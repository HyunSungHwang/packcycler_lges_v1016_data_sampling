// SelectingRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "SelectingRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectingRecordSet

IMPLEMENT_DYNAMIC(CSelectingRecordSet, CDaoRecordset)

CSelectingRecordSet::CSelectingRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CSelectingRecordSet)
	m_SelectID = 0;
	m_ProcID = 0;
	m_StepID = 0;
	m_No = 0;
	m_Content = _T("");
	m_Code = _T("");
	m_Query = _T("");
	m_nFields = 7;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CSelectingRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CSelectingRecordSet::GetDefaultSQL()
{
	return _T("[Select]");
}

void CSelectingRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CSelectingRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[SelectID]"), m_SelectID);
	DFX_Long(pFX, _T("[ProcID]"), m_ProcID);
	DFX_Long(pFX, _T("[StepID]"), m_StepID);
	DFX_Long(pFX, _T("[No]"), m_No);
	DFX_Text(pFX, _T("[Content]"), m_Content);
	DFX_Text(pFX, _T("[Code]"), m_Code);
	DFX_Text(pFX, _T("[Query]"), m_Query);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CSelectingRecordSet diagnostics

#ifdef _DEBUG
void CSelectingRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CSelectingRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
