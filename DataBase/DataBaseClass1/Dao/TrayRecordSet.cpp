// TrayRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "TrayRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrayRecordSet

IMPLEMENT_DYNAMIC(CTrayRecordSet, CDaoRecordset)

CTrayRecordSet::CTrayRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTrayRecordSet)
	m_ID = 0;
	m_TraySerial = 0;
	m_JigID = _T("");
	m_TeskID = 0;
	m_RegistedTime = (DATE)0;
	m_TrayNo = _T("");
	m_TrayName = _T("");
	m_UserName = _T("");
	m_Description = _T("");
	m_ModelKey = 0;
	m_TestKey = 0;
	m_LotNo = _T("");
	m_TestSerialNo = _T("");
	m_TestDateTime = (DATE)0;
	m_CellNo = 0;
	m_NormalCount = 0;
	m_FailCount = 0;
	m_CellCode = _T("");
	m_GradeCode = _T("");
	m_OperatorID = _T("");
	m_InputCellNo = 0;
	m_ModuleID = 0;
	m_GroupIndex = 0;
	m_StepIndex = 0;
	m_TrayType = 0;
	m_nFields = 25;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CTrayRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CTrayRecordSet::GetDefaultSQL()
{
	return _T("[Tray]");
}

void CTrayRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTrayRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ID]"), m_ID);
	DFX_Long(pFX, _T("[TraySerial]"), m_TraySerial);
	DFX_Text(pFX, _T("[JigID]"), m_JigID);
	DFX_Long(pFX, _T("[TeskID]"), m_TeskID);
	DFX_DateTime(pFX, _T("[RegistedTime]"), m_RegistedTime);
	DFX_Text(pFX, _T("[TrayNo]"), m_TrayNo);
	DFX_Text(pFX, _T("[TrayName]"), m_TrayName);
	DFX_Text(pFX, _T("[UserName]"), m_UserName);
	DFX_Text(pFX, _T("[Description]"), m_Description);
	DFX_Long(pFX, _T("[ModelKey]"), m_ModelKey);
	DFX_Long(pFX, _T("[TestKey]"), m_TestKey);
	DFX_Text(pFX, _T("[LotNo]"), m_LotNo);
	DFX_Text(pFX, _T("[TestSerialNo]"), m_TestSerialNo);
	DFX_DateTime(pFX, _T("[TestDateTime]"), m_TestDateTime);
	DFX_Long(pFX, _T("[CellNo]"), m_CellNo);
	DFX_Long(pFX, _T("[NormalCount]"), m_NormalCount);
	DFX_Long(pFX, _T("[FailCount]"), m_FailCount);
	DFX_Text(pFX, _T("[CellCode]"), m_CellCode);
	DFX_Text(pFX, _T("[GradeCode]"), m_GradeCode);
	DFX_Text(pFX, _T("[OperatorID]"), m_OperatorID);
	DFX_Long(pFX, _T("[InputCellNo]"), m_InputCellNo);
	DFX_Long(pFX, _T("[ModuleID]"), m_ModuleID);
	DFX_Long(pFX, _T("[GroupIndex]"), m_GroupIndex);
	DFX_Long(pFX, _T("[StepIndex]"), m_StepIndex);
	DFX_Long(pFX, _T("[TrayType]"), m_TrayType);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTrayRecordSet diagnostics

#ifdef _DEBUG
void CTrayRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CTrayRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
