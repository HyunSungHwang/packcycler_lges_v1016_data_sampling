#if !defined(AFX_SELECTINGRECORDSET_H__05B2FF91_44B4_45E3_9A76_60EDA567B21D__INCLUDED_)
#define AFX_SELECTINGRECORDSET_H__05B2FF91_44B4_45E3_9A76_60EDA567B21D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectingRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectingRecordSet DAO recordset

class CSelectingRecordSet : public CDaoRecordset
{
public:
	CSelectingRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSelectingRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CSelectingRecordSet, CDaoRecordset)
	long	m_SelectID;
	long	m_ProcID;
	long	m_StepID;
	long	m_No;
	CString	m_Content;
	CString	m_Code;
	CString	m_Query;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectingRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTINGRECORDSET_H__05B2FF91_44B4_45E3_9A76_60EDA567B21D__INCLUDED_)
