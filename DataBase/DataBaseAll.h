///////////////////////////////////////
//
//		ADPOWER Battery Charge/Discharge System
//		DataBase Header File
//
///////////////////////////////////////

#ifndef _ADPOWER_SCHEDULER_DATA_BASE_INCLUDE_H_
#define _ADPOWER_SCHEDULER_DATA_BASE_INCLUDE_H_

#ifdef _DATABASE_CONNECTION_ODBC
#include "./DataBaseClass/Odbc/BatteryModelRecordSet.h"
#include "./DataBaseClass/Odbc/GradeRecordSet.h"
#include "./DataBaseClass/Odbc/PreTestCheckRecordSet.h"
#include "./DataBaseClass/Odbc/StepRecordSet.h"
#include "./DataBaseClass/Odbc/TestListRecordSet.h"
#include "./DataBaseClass/Odbc/ProcTypeRecordSet.h"
#include "./DataBaseClass/Odbc/ChCodeRecordSet.h"
#include "./DataBaseClass/Odbc/SystemParamRecordSet.h"
#include "./DataBaseClass/Odbc/TrayRecordSet.h"
#include "./DataBaseClass/Odbc/CellStateRecordSet.h"
#include "./DataBaseClass/Odbc/UserRecordSet.h"

#else

#include "./DataBaseClass/Dao/BatteryModelRecordSet.h"
#include "./DataBaseClass/Dao/GradeRecordSet.h"
#include "./DataBaseClass/Dao/PreTestCheckRecordSet.h"
#include "./DataBaseClass/Dao/StepRecordSet.h"
#include "./DataBaseClass/Dao/TestListRecordSet.h"
#include "./DataBaseClass/Dao/ProcTypeRecordSet.h"
#include "./DataBaseClass/Dao/ChCodeRecordSet.h"
#include "./DataBaseClass/Dao/SystemParamRecordSet.h"
#include "./DataBaseClass/Dao/TrayRecordSet.h"
#include "./DataBaseClass/Dao/CellStateRecordSet.h"
#include "./DataBaseClass/Dao/UserRecordSet.h"

#endif	//_DATABASE_CONNECTION_ODBC

#endif	//_ADPOWER_SCHEDULER_DATA_BASE_INCLUDE_H_

