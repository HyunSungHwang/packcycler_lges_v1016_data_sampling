// GradeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "GradeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGradeRecordSet

IMPLEMENT_DYNAMIC(CGradeRecordSet, CDaoRecordset)

extern CString GetDataBaseName();

CGradeRecordSet::CGradeRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CGradeRecordSet)
	m_GradeID = 0;
	m_StepID = 0;
	m_GradeItem = 0;
	m_Value = 0.0f;
	m_Value1 = 0.0f;
	m_GradeCode = _T("");
	m_GradeIndex = 0;
	m_nFields = 7;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}

CString CGradeRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
//	return _T("C:\\My Documents\\Condition.mdb");
}

CString CGradeRecordSet::GetDefaultSQL()
{
	return _T("[Grade]");
}

void CGradeRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CGradeRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[GradeID]"), m_GradeID);
	DFX_Long(pFX, _T("[StepID]"), m_StepID);
	DFX_Long(pFX, _T("[GradeItem]"), m_GradeItem);
	DFX_Single(pFX, _T("[Value]"), m_Value);
	DFX_Single(pFX, _T("[Value1]"), m_Value1);
	DFX_Text(pFX, _T("[GradeCode]"), m_GradeCode);
	DFX_Long(pFX, _T("[GradeStep]"), m_GradeIndex);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CGradeRecordSet diagnostics

#ifdef _DEBUG
void CGradeRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CGradeRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
