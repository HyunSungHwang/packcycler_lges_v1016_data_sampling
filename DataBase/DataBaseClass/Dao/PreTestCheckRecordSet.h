#if !defined(AFX_PRETESTCHECKRECORDSET_H__CED5C4DF_3393_4EEC_A9A0_6259C0E77AB3__INCLUDED_)
#define AFX_PRETESTCHECKRECORDSET_H__CED5C4DF_3393_4EEC_A9A0_6259C0E77AB3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreTestCheckRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet DAO recordset

//Cell �˻� ���� 

class CPreTestCheckRecordSet : public CDaoRecordset
{
public:
	CPreTestCheckRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CPreTestCheckRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CPreTestCheckRecordSet, CDaoRecordset)
	long	m_CheckID;
	long	m_TestID;
	float	m_MaxV;
	float	m_MinV;
	float	m_CurrentRange;
	float	m_OCVLimit;
	float	m_TrickleCurrent;
	long	m_TrickleTime;
	float	m_DeltaVoltage;
	long	m_MaxFaultBattery;
	COleDateTime	m_AutoTime;
	BOOL	m_AutoProYN;
	BOOL	m_PreTestCheck;
	CString m_strCanCategory;
	CString	m_strCanCompareType;
	CString m_strCanDataType;
	CString	m_strCanValue;
	CString m_strAuxCategory;
	CString	m_strAuxCompareType;
	CString m_strAuxDataType;
	CString	m_strAuxValue;
	float	m_SafetyMaxV;
	float	m_SafetyMinV;
	float	m_SafetyMaxI;
	float	m_SafetyCellDeltaV;
	float	m_SafetyMaxT;
	float	m_SafetyMinT;
	float	m_SafetyMaxC;
	long	m_SafetyMaxW;
	long	m_SafetyMaxWh;
	float	m_DeltaStdMaxV;
	float	m_DeltaStdMinV;
	float	m_DeltaMaxV;
	float	m_DeltaMinV;
	BOOL	m_DeltaVent;

	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreTestCheckRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRETESTCHECKRECORDSET_H__CED5C4DF_3393_4EEC_A9A0_6259C0E77AB3__INCLUDED_)
