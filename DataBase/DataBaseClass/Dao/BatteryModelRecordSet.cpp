<<<<<<< .mine
// BatteryModelRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "BatteryModelRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBatteryModelRecordSet

IMPLEMENT_DYNAMIC(CBatteryModelRecordSet, CDaoRecordset)

extern CString GetDataBaseName();

CBatteryModelRecordSet::CBatteryModelRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CBatteryModelRecordSet)
	m_ModelID = 0;
	m_No = 0;
	m_ModelName = _T("");
	m_Description = _T("");
	m_CreatedTime = (DATE)0;
	m_Creator = "";

#ifdef _USE_MODEL_USER_
	m_nFields = 6;
#else
	m_nFields = 5;
#endif

	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CBatteryModelRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CBatteryModelRecordSet::GetDefaultSQL()
{
	return _T("[BatteryModel]");
}

void CBatteryModelRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CBatteryModelRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ModelID]"), m_ModelID);
	DFX_Long(pFX, _T("[No]"), m_No);
	DFX_Text(pFX, _T("[ModelName]"), m_ModelName);
	DFX_Text(pFX, _T("[Description]"), m_Description);

#ifdef _USE_MODEL_USER_
	DFX_Text(pFX, _T("[Creator]"), m_Creator);
#endif
	
	DFX_DateTime(pFX, _T("[CreatedTime]"), m_CreatedTime);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CBatteryModelRecordSet diagnostics

#ifdef _DEBUG
void CBatteryModelRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CBatteryModelRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
