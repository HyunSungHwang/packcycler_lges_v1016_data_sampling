#if !defined(AFX_STEPRECORDSET_H__4ADD84F5_3C0A_4EC4_A87E_DEF2DB6E58F5__INCLUDED_)
#define AFX_STEPRECORDSET_H__4ADD84F5_3C0A_4EC4_A87E_DEF2DB6E58F5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StepRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet DAO recordset

class CStepRecordSet : public CDaoRecordset
{
public:
	
	CStepRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CStepRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CStepRecordSet, CDaoRecordset)
	long	m_StepID;
	long	m_ModelID;
	long	m_TestID;
	long	m_StepNo;
	long	m_StepProcType;
	long	m_StepType;
	long	m_StepMode;
	float	m_Vref;
	float	m_Vref_DisCharge;
	float	m_Iref;
	float	m_Pref;
	float	m_Rref;
	long	m_EndTime;
	float	m_EndV;
	float	m_EndI;
	float	m_EndCapacity;
	float	m_End_dV;
	float	m_End_dI;
	long	m_CycleCount;
	float	m_OverV;
	float	m_LimitV;
	float	m_OverI;
	float	m_LimitI;
	float	m_OverCapacity;
	float	m_LimitCapacity;
	float	m_OverImpedance;
	float	m_LimitImpedance;
	long	m_DeltaTime;
	long	m_DeltaTime1;
	float	m_DeltaV;
	float	m_DeltaI;
	BOOL	m_Grade;
	long	m_CompTimeV1;
	long	m_CompTimeV2;
	long	m_CompTimeV3;
	float	m_CompVLow1;
	float	m_CompVLow2;
	float	m_CompVLow3;
	float	m_CompVHigh1;
	float	m_CompVHigh2;
	float	m_CompVHigh3;
	long	m_CompTimeI1;
	long	m_CompTimeI2;
	long	m_CompTimeI3;
	float	m_CompILow1;
	float	m_CompILow2;
	float	m_CompILow3;
	float	m_CompIHigh1;
	float	m_CompIHigh2;
	float	m_CompIHigh3;
	long	m_RecordTime;
	float	m_RecordDeltaV;
	float	m_RecordDeltaI;
	float	m_CapVLow;
	float	m_CapVHigh;
	float	m_EndCheckVLow;
	float	m_EndCheckVHigh;
	float	m_EndCheckILow;
	float	m_EndCheckIHigh;
	
	float	m_CellDeltaVStep;//yulee 20190531_5
	float	m_StepDeltaAuxTemp;
	float	m_StepDeltaAuxTh;
	float	m_StepDeltaAuxT;
	float	m_StepDeltaAuxVTime;
	float	m_StepAuxV;

	BOOL	m_StepDeltaVentAuxV;
	BOOL	m_StepDeltaVentAuxTemp;
	BOOL	m_StepDeltaVentAuxTh;
	BOOL	m_StepDeltaVentAuxT;
	BOOL	m_StepVentAuxV;

	CString	m_strGotoValue;
	CString	m_strReportTemp;
	CString	m_strSocEnd;
	CString	m_strEndWatt;
	CString	m_strTempLimit;
	CString m_strSimulFile;		
	CString m_ValueLimitLow;	//Add AccLoopInfoCount, AccLoopGroupID, strCVTime 20080724 : DB 필드도 모자르고 호환성도 살리기 위해 kjh
	CString m_ValueLimitHigh;
	CString	m_ValueOven;
	CString m_ValueBMSandAUX;		//2009-03 ljb edit	(m_Value9  -> m_ValueBMSandAUX )
	
	CString m_strCanCategory;		//ljb 20100819 for v100A
	CString m_strCanDataType;		//ljb 20100819 for v100A
	CString m_strCanCompareType;	//ljb 20100819 for v100A
	CString m_strCanValue;			//ljb 20100819 for v100A
	CString m_strCanGoto;			//ljb 20100819 for v100A
	
	CString m_strAuxCategory;		//ljb 20100819 for v100A
	CString m_strAuxDataType;	//ljb 20100819 for v100A
	CString m_strAuxCompareType;	//ljb 20100819 for v100A
	CString m_strAuxValue;			//ljb 20100819 for v100A
	CString m_strAuxGoto;			//ljb 20100819 for v100A
	
	//}}AFX_FIELD

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStepRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STEPRECORDSET_H__4ADD84F5_3C0A_4EC4_A87E_DEF2DB6E58F5__INCLUDED_)
