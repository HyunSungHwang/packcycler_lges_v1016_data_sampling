// StepRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "StepRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet

IMPLEMENT_DYNAMIC(CStepRecordSet, CDaoRecordset)

extern CString GetDataBaseName();

CStepRecordSet::CStepRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CStepRecordSet)

	m_StepID = 0;
	m_ModelID = 0;
	m_TestID = 0;
	m_StepNo = 0;
	m_StepProcType = 0;
	m_StepType = 0;
	m_StepMode = 0;
	m_Vref = 0.0f;
	m_Iref = 0.0f;
	m_Pref = 0.0f;				//10

	m_EndTime = 0;
	m_EndV = 0.0f;
	m_EndI = 0.0f;
	m_EndCapacity = 0.0f;
	m_End_dV = 0.0f;
	m_End_dI = 0.0f;
	m_CycleCount = 0;
	m_OverV = 0.0f;
	m_LimitV = 0.0f;
	m_OverI = 0.0f;				//20

	m_LimitI = 0.0f;
	m_OverCapacity = 0.0f;
	m_LimitCapacity = 0.0f;
	m_OverImpedance = 0.0f;
	m_LimitImpedance = 0.0f;
	m_DeltaTime = 0;
	m_DeltaTime1 = 0;
	m_DeltaV = 0.0f;
	m_DeltaI = 0.0f;
	m_Grade = FALSE;			//30

	m_CompTimeV1 = 0;
	m_CompTimeV2 = 0;
	m_CompTimeV3 = 0;
	m_CompVLow1 = 0.0f;
	m_CompVLow2 = 0.0f;
	m_CompVLow3 = 0.0f;
	m_CompVHigh1 = 0.0f;
	m_CompVHigh2 = 0.0f;
	m_CompVHigh3 = 0.0f;
	m_CompTimeI1 = 0;			//40

	m_CompTimeI2 = 0;
	m_CompTimeI3 = 0;
	m_CompILow1 = 0.0f;
	m_CompILow2 = 0.0f;
	m_CompILow3 = 0.0f;
	m_CompIHigh1 = 0.0f;
	m_CompIHigh2 = 0.0f;
	m_CompIHigh3 = 0.0f;
	m_RecordTime = 0;			
	m_RecordDeltaV = 0.0f;	//50

	m_RecordDeltaI = 0.0f;
	m_CapVLow = 0.0f;
	m_CapVHigh = 0.0f;
	m_EndCheckVLow = 0.0f;
	m_EndCheckVHigh = 0.0f;
	m_EndCheckILow = 0.0f;
	m_EndCheckIHigh = 0.0f;
	m_strGotoValue = _T("");
	m_strReportTemp = _T("");	
	m_strSocEnd = _T("");	//60

	m_strEndWatt = _T("");
	m_strTempLimit = _T("");
	m_strSimulFile = _T("");
	m_ValueLimitLow = _T("");
	m_ValueLimitHigh = _T("");
	m_ValueOven = _T("");
	m_ValueBMSandAUX = _T("");
	m_strCanCategory = _T("");		// ljb 20100819 for v100A
	m_strCanDataType = _T("");		//ljb 20100819 for v100A
	m_strCanCompareType = _T("");	//70 ljb 20100819 for v100A

	m_strCanValue = _T("");			//ljb 20100819 for v100A
	m_strCanGoto = _T("");			//ljb 20100819 for v100A
	m_strAuxCategory = _T("");		//ljb 20100819 for v100A
	m_strAuxDataType = _T("");	//ljb 20100819 for v100A
	m_strAuxCompareType = _T("");	//ljb 20100819 for v100A
	m_strAuxValue = _T("");			//ljb 20100819 for v100A
	m_strAuxGoto = _T("");;			//ljb 20100819 for v100A
	m_Vref_DisCharge = 0.0f;		
	m_Rref = 0.0f;
	m_CellDeltaVStep = 0.0f;			//80
		
	m_StepDeltaAuxTemp = 0.0f;
	m_StepDeltaAuxTh = 0.0f;
	m_StepDeltaAuxT = 0.0f;
	m_StepDeltaAuxVTime = 0.0f;
	m_StepAuxV = 0.0f;
	m_StepDeltaVentAuxV = FALSE;
	m_StepDeltaVentAuxTemp = FALSE;
	m_StepDeltaVentAuxTh = FALSE;	
	m_StepDeltaVentAuxT = FALSE;
	m_StepVentAuxV = FALSE;			//90

	m_nFields = 90;
	//m_nFields = 79;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CStepRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CStepRecordSet::GetDefaultSQL()
{
	return _T("[Step]");
}

void CStepRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CStepRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);

	DFX_Long(pFX, _T("[StepID]"), m_StepID);
	DFX_Long(pFX, _T("[ModelID]"), m_ModelID);
	DFX_Long(pFX, _T("[TestID]"), m_TestID);
	DFX_Long(pFX, _T("[StepNo]"), m_StepNo);
	DFX_Long(pFX, _T("[StepProcType]"), m_StepProcType);
	DFX_Long(pFX, _T("[StepType]"), m_StepType);
	DFX_Long(pFX, _T("[StepMode]"), m_StepMode);
	DFX_Single(pFX, _T("[Vref]"), m_Vref);
	DFX_Single(pFX, _T("[Iref]"), m_Iref);
	DFX_Single(pFX, _T("[Pref]"), m_Pref);

	DFX_Long(pFX, _T("[EndTime]"), m_EndTime);
	DFX_Single(pFX, _T("[EndV]"), m_EndV);
	DFX_Single(pFX, _T("[EndI]"), m_EndI);
	DFX_Single(pFX, _T("[EndCapacity]"), m_EndCapacity);
	DFX_Single(pFX, _T("[End_dV]"), m_End_dV);
	DFX_Single(pFX, _T("[End_dI]"), m_End_dI);
	DFX_Long(pFX, _T("[CycleCount]"), m_CycleCount);
	DFX_Single(pFX, _T("[OverV]"), m_OverV);
	DFX_Single(pFX, _T("[LimitV]"), m_LimitV);
	DFX_Single(pFX, _T("[OverI]"), m_OverI);

	DFX_Single(pFX, _T("[LimitI]"), m_LimitI);
	DFX_Single(pFX, _T("[OverCapacity]"), m_OverCapacity);
	DFX_Single(pFX, _T("[LimitCapacity]"), m_LimitCapacity);
	DFX_Single(pFX, _T("[OverImpedance]"), m_OverImpedance);
	DFX_Single(pFX, _T("[LimitImpedance]"), m_LimitImpedance);
	DFX_Long(pFX, _T("[DeltaTime]"), m_DeltaTime);
	DFX_Long(pFX, _T("[DeltaTime1]"), m_DeltaTime1);
	DFX_Single(pFX, _T("[DeltaV]"), m_DeltaV);
	DFX_Single(pFX, _T("[DeltaI]"), m_DeltaI);
	DFX_Bool(pFX, _T("[Grade]"), m_Grade);

	DFX_Long(pFX, _T("[CompTimeV1]"), m_CompTimeV1);
	DFX_Long(pFX, _T("[CompTimeV2]"), m_CompTimeV2);
	DFX_Long(pFX, _T("[CompTimeV3]"), m_CompTimeV3);
	DFX_Single(pFX, _T("[CompVLow1]"), m_CompVLow1);
	DFX_Single(pFX, _T("[CompVLow2]"), m_CompVLow2);
	DFX_Single(pFX, _T("[CompVLow3]"), m_CompVLow3);
	DFX_Single(pFX, _T("[CompVHigh1]"), m_CompVHigh1);
	DFX_Single(pFX, _T("[CompVHigh2]"), m_CompVHigh2);
	DFX_Single(pFX, _T("[CompVHigh3]"), m_CompVHigh3);
	DFX_Long(pFX, _T("[CompTimeI1]"), m_CompTimeI1);

	DFX_Long(pFX, _T("[CompTimeI2]"), m_CompTimeI2);
	DFX_Long(pFX, _T("[CompTimeI3]"), m_CompTimeI3);
	DFX_Single(pFX, _T("[CompILow1]"), m_CompILow1);
	DFX_Single(pFX, _T("[CompILow2]"), m_CompILow2);
	DFX_Single(pFX, _T("[CompILow3]"), m_CompILow3);
	DFX_Single(pFX, _T("[CompIHigh1]"), m_CompIHigh1);
	DFX_Single(pFX, _T("[CompIHigh2]"), m_CompIHigh2);
	DFX_Single(pFX, _T("[CompIHigh3]"), m_CompIHigh3);
	DFX_Long(pFX, _T("[RecordTime]"), m_RecordTime);
	DFX_Single(pFX, _T("[RecordDeltaV]"), m_RecordDeltaV);

	DFX_Single(pFX, _T("[RecordDeltaI]"), m_RecordDeltaI);
	DFX_Single(pFX, _T("[CapVLow]"), m_CapVLow);
	DFX_Single(pFX, _T("[CapVHigh]"), m_CapVHigh);
	DFX_Single(pFX, _T("[EndCheckVLow]"), m_EndCheckVLow);
	DFX_Single(pFX, _T("[EndCheckVHigh]"), m_EndCheckVHigh);
	DFX_Single(pFX, _T("[EndCheckILow]"), m_EndCheckILow);
	DFX_Single(pFX, _T("[EndCheckIHigh]"), m_EndCheckIHigh);
	DFX_Text(pFX, _T("[Value0]"), m_strGotoValue);
	DFX_Text(pFX, _T("[Value1]"), m_strReportTemp);
	DFX_Text(pFX, _T("[Value2]"), m_strSocEnd);

	DFX_Text(pFX, _T("[Value3]"), m_strEndWatt);
	DFX_Text(pFX, _T("[Value4]"), m_strTempLimit);
	DFX_Text(pFX, _T("[Value5]"), m_strSimulFile);
	DFX_Text(pFX, _T("[Value6]"), m_ValueLimitLow);
	DFX_Text(pFX, _T("[Value7]"), m_ValueLimitHigh);
	DFX_Text(pFX, _T("[Value8]"), m_ValueOven);
	DFX_Text(pFX, _T("[Value9]"), m_ValueBMSandAUX);	
	DFX_Text(pFX, _T("[CanCategory]"), m_strCanCategory);
	DFX_Text(pFX, _T("[CanDataType]"), m_strCanDataType);
	DFX_Text(pFX, _T("[CanCompare]"), m_strCanCompareType);

	DFX_Text(pFX, _T("[CanValue]"), m_strCanValue);
	DFX_Text(pFX, _T("[CanGoto]"), m_strCanGoto);
	DFX_Text(pFX, _T("[AuxCategory]"), m_strAuxCategory);
	DFX_Text(pFX, _T("[AuxDataType]"), m_strAuxDataType);
	DFX_Text(pFX, _T("[AuxCompare]"), m_strAuxCompareType);
	DFX_Text(pFX, _T("[AuxValue]"), m_strAuxValue);
	DFX_Text(pFX, _T("[AuxGoto]"), m_strAuxGoto);
	DFX_Single(pFX, _T("[Vref_DisCharge]"), m_Vref_DisCharge);
	DFX_Single(pFX, _T("[Rref]"), m_Rref);	
	DFX_Single(pFX, _T("[StepCellDeltaV]"), m_CellDeltaVStep);

	DFX_Single(pFX, _T("[StepDeltaAuxTemp]"), m_StepDeltaAuxTemp);
	DFX_Single(pFX, _T("[StepDeltaAuxTh]"), m_StepDeltaAuxTh);
	DFX_Single(pFX, _T("[StepDeltaAuxT]"), m_StepDeltaAuxT);
	DFX_Single(pFX, _T("[StepDeltaAuxVTime]"), m_StepDeltaAuxVTime);
	DFX_Single(pFX, _T("[StepAuxV]"), m_StepAuxV);
	DFX_Bool(pFX, _T("[StepDeltaVentAuxV]"), m_StepDeltaVentAuxV);
	DFX_Bool(pFX, _T("[StepDeltaVentAuxTemp]"), m_StepDeltaVentAuxTemp);
	DFX_Bool(pFX, _T("[StepDeltaVentAuxTh]"), m_StepDeltaVentAuxTh);
	DFX_Bool(pFX, _T("[StepDeltaVentAuxT]"), m_StepDeltaVentAuxT);
	DFX_Bool(pFX, _T("[StepVentAuxV]"), m_StepVentAuxV);

	//}}AFX_FIELD_MAP
}
 
/////////////////////////////////////////////////////////////////////////////
 // CStepRecordSet diagnostics

#ifdef _DEBUG
void CStepRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CStepRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
