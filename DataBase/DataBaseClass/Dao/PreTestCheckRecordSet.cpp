// PreTestCheckRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "PreTestCheckRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet

IMPLEMENT_DYNAMIC(CPreTestCheckRecordSet, CDaoRecordset)

extern CString GetDataBaseName();

CPreTestCheckRecordSet::CPreTestCheckRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CPreTestCheckRecordSet)
	m_CheckID = 0;
	m_TestID = 0;
	m_MaxV = 0.0f;
	m_MinV = 0.0f;
	m_CurrentRange = 0.0f;
	m_OCVLimit = 0.0f;
	m_TrickleCurrent = 0.0f;
	m_TrickleTime = 0;
	m_DeltaVoltage = 0.0f;
	m_MaxFaultBattery = 0;
	m_AutoTime = (DATE)0;
	m_AutoProYN = FALSE;
	m_PreTestCheck = FALSE;
	m_strCanCategory = _T("");
	m_strCanCompareType = _T("");
	m_strCanDataType = _T("");
	m_strCanValue = _T("");
	m_strAuxCategory = _T("");
	m_strAuxCompareType = _T("");
	m_strAuxDataType = _T("");
	m_strAuxValue = _T("");
	m_SafetyMaxV = 0.0f;
	m_SafetyMinV = 0.0f;
	m_SafetyMaxI = 0.0f;
	m_SafetyCellDeltaV = 0.0f;
	m_SafetyMaxT = 0.0f;
	m_SafetyMinT = 0.0f;
	m_SafetyMaxC = 0.0f;
	m_SafetyMaxW = 0.0f;
	m_SafetyMaxWh = 0.0f;
	m_DeltaStdMaxV = 0.0f;
	m_DeltaStdMinV = 0.0f;
	m_DeltaMinV = 0.0f;
	m_DeltaMaxV = 0.0f;
	m_DeltaVent = FALSE;

	m_nFields = 35;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}

CString CPreTestCheckRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
//	return _T("C:\\My Documents\\Condition.mdb");
}


CString CPreTestCheckRecordSet::GetDefaultSQL()
{
	return _T("[Check]");
}

void CPreTestCheckRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CPreTestCheckRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[CheckID]"), m_CheckID);
	DFX_Long(pFX, _T("[TestID]"), m_TestID);
	DFX_Single(pFX, _T("[MaxV]"), m_MaxV);
	DFX_Single(pFX, _T("[MinV]"), m_MinV);
	DFX_Single(pFX, _T("[CurrentRange]"), m_CurrentRange);
	DFX_Single(pFX, _T("[OCVLimit]"), m_OCVLimit);
	DFX_Single(pFX, _T("[TrickleCurrent]"), m_TrickleCurrent);
	DFX_Long(pFX, _T("[TrickleTime]"), m_TrickleTime);
	DFX_Single(pFX, _T("[DeltaVoltage]"), m_DeltaVoltage);
	DFX_Long(pFX, _T("[MaxFaultBattery]"), m_MaxFaultBattery);
	DFX_DateTime(pFX, _T("[AutoTime]"), m_AutoTime);
	DFX_Bool(pFX, _T("[AutoProYN]"), m_AutoProYN);
	DFX_Bool(pFX, _T("[PreTestCheck]"), m_PreTestCheck);
	DFX_Text(pFX, _T("[CanCategory]"), m_strCanCategory);
	DFX_Text(pFX, _T("[CanCompare]"), m_strCanCompareType);
	DFX_Text(pFX, _T("[CanDataType]"), m_strCanDataType);
	DFX_Text(pFX, _T("[CanValue]"), m_strCanValue);
	DFX_Text(pFX, _T("[AuxCategory]"), m_strAuxCategory);
	DFX_Text(pFX, _T("[AuxCompare]"), m_strAuxCompareType);
	DFX_Text(pFX, _T("[AuxDataType]"), m_strAuxDataType);
	DFX_Text(pFX, _T("[AuxValue]"), m_strAuxValue);
	DFX_Single(pFX, _T("[SafetyMaxV]"), m_SafetyMaxV);
	DFX_Single(pFX, _T("[SafetyMinV]"), m_SafetyMinV);
	DFX_Single(pFX, _T("[SafetyMaxI]"), m_SafetyMaxI);
	DFX_Single(pFX, _T("[SafetyMinI]"), m_SafetyCellDeltaV);
	DFX_Single(pFX, _T("[SafetyMaxT]"), m_SafetyMaxT);
	DFX_Single(pFX, _T("[SafetyMinT]"), m_SafetyMinT);
	DFX_Single(pFX, _T("[SafetyMaxC]"), m_SafetyMaxC);
	DFX_Long(pFX, _T("[SafetyMaxW]"), m_SafetyMaxW);
	DFX_Long(pFX, _T("[SafetyMaxWh]"), m_SafetyMaxWh);
	DFX_Single(pFX, _T("[STDDeltaVoltageMax]"), m_DeltaStdMaxV);
	DFX_Single(pFX, _T("[STDDeltaVoltageMin]"), m_DeltaStdMinV);
	DFX_Single(pFX, _T("[DeltaVoltageMax]"), m_DeltaMaxV);
	DFX_Single(pFX, _T("[DeltaVoltageMin]"), m_DeltaMinV);
	DFX_Bool(pFX, _T("[DeltaVoltageVent]"), m_DeltaVent);

	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet diagnostics

#ifdef _DEBUG
void CPreTestCheckRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CPreTestCheckRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
