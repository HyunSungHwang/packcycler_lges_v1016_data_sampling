#if !defined(AFX_USERRECORDSET_H__98FBAA56_F8B3_4287_A941_908BCE72FC14__INCLUDED_)
#define AFX_USERRECORDSET_H__98FBAA56_F8B3_4287_A941_908BCE72FC14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserRecordSet DAO recordset

class CUserRecordSet : public CDaoRecordset
{
public:
	CUserRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CUserRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CUserRecordSet, CDaoRecordset)
	long	m_Index;
	CString	m_UserID;
	CString	m_Password;
	CString	m_Name;
	long	m_Authority;
	COleDateTime	m_RegistedDate;
	CString	m_Description;
	BOOL	m_AutoLogOut;
	long	m_AutoLogOutTime;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERRECORDSET_H__98FBAA56_F8B3_4287_A941_908BCE72FC14__INCLUDED_)
