#if !defined(AFX_BATTERYMODELRECORDSET_H__3DB561C9_F755_4406_AE18_35F5ED8E405B__INCLUDED_)
#define AFX_BATTERYMODELRECORDSET_H__3DB561C9_F755_4406_AE18_35F5ED8E405B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BatteryModelRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBatteryModelRecordSet DAO recordset

class CBatteryModelRecordSet : public CDaoRecordset
{
public:
	CBatteryModelRecordSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CBatteryModelRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CBatteryModelRecordSet, CDaoRecordset)
	long	m_ModelID;
	long	m_No;
	CString	m_ModelName;
	CString	m_Description;
	COleDateTime	m_CreatedTime;
	CString	m_Creator;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBatteryModelRecordSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BATTERYMODELRECORDSET_H__3DB561C9_F755_4406_AE18_35F5ED8E405B__INCLUDED_)
