; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAboutDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Bin2Excel.h"
LastPage=0

ClassCount=9
Class1=CBin2ExcelApp
Class2=CBin2ExcelDoc
Class3=CBin2ExcelView
Class4=CMainFrame
Class9=CAboutDlg

ResourceCount=7
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Resource7=IDD_BIN2EXCEL_FORM

[CLS:CBin2ExcelApp]
Type=0
HeaderFile=Bin2Excel.h
ImplementationFile=Bin2Excel.cpp
Filter=N

[CLS:CBin2ExcelDoc]
Type=0
HeaderFile=Bin2ExcelDoc.h
ImplementationFile=Bin2ExcelDoc.cpp
Filter=N

[CLS:CBin2ExcelView]
Type=0
HeaderFile=Bin2ExcelView.h
ImplementationFile=Bin2ExcelView.cpp
Filter=D


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=Bin2Excel.cpp
ImplementationFile=Bin2Excel.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_APP_EXIT
Command6=ID_EDIT_UNDO
Command7=ID_EDIT_CUT
Command8=ID_EDIT_COPY
Command9=ID_EDIT_PASTE
Command10=ID_APP_ABOUT
CommandCount=10

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_BIN2EXCEL_FORM]
Type=1
Class=CBin2ExcelView
ControlCount=1
Control1=IDC_STATIC,static,1342308352

