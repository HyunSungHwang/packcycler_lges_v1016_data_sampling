// Bin2ExcelView.h : interface of the CBin2ExcelView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BIN2EXCELVIEW_H__18C275D9_91E0_42AA_995E_AA2F1A3E317E__INCLUDED_)
#define AFX_BIN2EXCELVIEW_H__18C275D9_91E0_42AA_995E_AA2F1A3E317E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBin2ExcelView : public CFormView
{
protected: // create from serialization only
	CBin2ExcelView();
	DECLARE_DYNCREATE(CBin2ExcelView)

public:
	//{{AFX_DATA(CBin2ExcelView)
	enum{ IDD = IDD_BIN2EXCEL_FORM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:
	CBin2ExcelDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBin2ExcelView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBin2ExcelView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBin2ExcelView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in Bin2ExcelView.cpp
inline CBin2ExcelDoc* CBin2ExcelView::GetDocument()
   { return (CBin2ExcelDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BIN2EXCELVIEW_H__18C275D9_91E0_42AA_995E_AA2F1A3E317E__INCLUDED_)
