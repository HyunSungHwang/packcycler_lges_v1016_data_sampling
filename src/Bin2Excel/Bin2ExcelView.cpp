// Bin2ExcelView.cpp : implementation of the CBin2ExcelView class
//

#include "stdafx.h"
#include "Bin2Excel.h"

#include "Bin2ExcelDoc.h"
#include "Bin2ExcelView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelView

IMPLEMENT_DYNCREATE(CBin2ExcelView, CFormView)

BEGIN_MESSAGE_MAP(CBin2ExcelView, CFormView)
	//{{AFX_MSG_MAP(CBin2ExcelView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelView construction/destruction

CBin2ExcelView::CBin2ExcelView()
	: CFormView(CBin2ExcelView::IDD)
{
	//{{AFX_DATA_INIT(CBin2ExcelView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here

}

CBin2ExcelView::~CBin2ExcelView()
{
}

void CBin2ExcelView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBin2ExcelView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BOOL CBin2ExcelView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CBin2ExcelView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

}

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelView diagnostics

#ifdef _DEBUG
void CBin2ExcelView::AssertValid() const
{
	CFormView::AssertValid();
}

void CBin2ExcelView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CBin2ExcelDoc* CBin2ExcelView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBin2ExcelDoc)));
	return (CBin2ExcelDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelView message handlers
