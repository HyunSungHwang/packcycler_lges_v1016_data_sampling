// Bin2Excel.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Bin2Excel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelApp

BEGIN_MESSAGE_MAP(CBin2ExcelApp, CWinApp)
	//{{AFX_MSG_MAP(CBin2ExcelApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelApp construction

CBin2ExcelApp::CBin2ExcelApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBin2ExcelApp object

CBin2ExcelApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelApp initialization

BOOL CBin2ExcelApp::InitInstance()
{
	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("ADP CTSPro"));

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	CString strArgu((char *)m_lpCmdLine);
	if(strArgu.IsEmpty())
	{
		CString str, strExt;
		strExt.Format(".%s", PS_RESULT_FILE_NAME_EXT);
		str.Format("CTSPro step end file(*.%s)|*.%s|CTSPro record file(*.%s)|*.%s|", 
					PS_RESULT_FILE_NAME_EXT, PS_RESULT_FILE_NAME_EXT,
					PS_RAW_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT
			);
		CFileDialog pDlg(TRUE, strExt , "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, str);
		pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
		if(IDOK != pDlg.DoModal())
		{
			return	TRUE;
		}
		strArgu = pDlg.GetPathName();
	}

	if(!strArgu.IsEmpty())
	{
		strArgu.TrimLeft("\"");
		strArgu.TrimRight("\"");
		CString strExt;
		int p1 = strArgu.ReverseFind('.');
		if(p1 > 0)								//파일 확장자 검색 
		{
			strExt =  strArgu.Mid(p1+1);
			strExt.MakeLower();
			if(strExt == PS_RESULT_FILE_NAME_EXT)
			{
				ConvertRptFile(strArgu);
			}
			else if(strExt == PS_RAW_FILE_NAME_EXT)
			{
				ConvertCycFile(strArgu);
			}
			else
			{
				AfxMessageBox("Not supported file format!!");
			}
		}
	}
	return TRUE;
}

BOOL CBin2ExcelApp::ConvertRptFile(CString strFile)
{
//	AfxMessageBox(strFile);

	CWaitCursor wait;
	int type  = GetProfileInt("Settings", "ConvertType", 0);

	FILE *fp = fopen(strFile, "rb");
	if(fp == NULL)	return FALSE;

	PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
	fread(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER),1 , fp);
	if(pFileHeader->fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
	{
		fclose(fp);
		delete pFileHeader;
		return FALSE;
	}
		
	PS_STEP_END_RECORD *pRecord = new PS_STEP_END_RECORD;

	FILE *fExcel;
	CString str, strExcel, strTemp1, strTemp;
	CString TABEL_FILE_COLUMN_HEAD("No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temperature,Press,VoltageAverage,CurrentAverage,Sequence,");

	if(type == 0)
	{
//		strExcel.Format("%s.csv", strFile.Left(strFile.ReverseFind('.')));
		strExcel = strFile + ".csv";
	}
	else
	{
		strExcel.Format("%s.rpt", strFile.Left(strFile.ReverseFind('.')));
	}

	fExcel = fopen(strExcel, "wt");
	if(fExcel)
	{
		if(type == 0)
		{
			str = "Ch,Step,Seq,From,To,CurCyc,TotCyc,State,Type,Code,Grade,Volt,Curr,Capa,Watt,WattH,StepT,TotT,Imp,Temp,Press,AvgVtg,AvgCrt,";
			fprintf(fExcel, "%s\n", str);	
		}
		else
		{
			str.Format("StartT=%s, EndT=%s, Serial=%s, User=%s, Descript=%s,", 
						pFileHeader->testHeader.szStartTime, 
						pFileHeader->testHeader.szEndTime,
						pFileHeader->testHeader.szSerial,
						pFileHeader->testHeader.szUserID,
						pFileHeader->testHeader.szDescript
						);
			fprintf(fExcel, "%s\n", str);	
			fprintf(fExcel, "%s\n", TABEL_FILE_COLUMN_HEAD);	
		}


		while(fread(pRecord, sizeof(PS_STEP_END_RECORD), 1, fp) > 0)
		{
			if(pRecord->chGradeCode == 0)	pRecord->chGradeCode = ' ';
			::PSCellCodeMsg(pRecord->chCode, strTemp, strTemp1);

			//type 1
			if(type == 0)
			{
				//str.Format("%d,%d,%d,%d,%d,%d,%d,%s,%s,%s,%c,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,",
				str.Format("%d,%d,%d,%d,%d,%d,%d,%s,%s,%s,%c,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,",
							pRecord->chNo,
							pRecord->chStepNo,
							pRecord->lSaveSequence,			// Expanded Field
							pRecord->nIndexFrom, 
							pRecord->nIndexTo,
							pRecord->nCurrentCycleNum,
							pRecord->nTotalCycleNum,
							::PSGetStateMsg(pRecord->chState),			// Run, Stop(Manual, Error), End
							::PSGetTypeMsg(pRecord->chStepType),			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
							strTemp,
							pRecord->chGradeCode,

							pRecord->fVoltage,				// Result Data...
							pRecord->fCurrent,
							pRecord->fCapacity,
							pRecord->fWatt,
							pRecord->fWattHour,
							pRecord->fStepTime,			// 이번 Step 진행 시간
							pRecord->fTotalTime,			// 시험 Total 진행 시간
							pRecord->fImpedance,			// Impedance (AC or DC)
							//pRecord->fTemparature,
							//pRecord->fPressure,
							pRecord->fAvgVoltage,
							pRecord->fAvgCurrent
							);
			}
			else
			{
				//type 2 호환되는 형태 
				//str.Format(	"%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,", 
				str.Format(	"%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,", 
							pRecord->chStepNo,			//StepNo
							pRecord->nIndexFrom,		//IndexFrom	
							pRecord->nIndexTo,			//IndexTo
							pRecord->nCurrentCycleNum,	//CurCycle
							pRecord->nTotalCycleNum,	//TotalCycle => Cycle별 파일 생서시는 FileNo로 사용됨
							pRecord->chStepType,		//Type
							pRecord->chState,			//State
							pRecord->fStepTime,			//Time
							pRecord->fTotalTime,		//TotTime
							pRecord->chGradeCode,		//Code
							pRecord->chGradeCode,		//Grade
							pRecord->fVoltage, 			//Voltage
							pRecord->fCurrent, 			//Current
							pRecord->fCapacity, 		//Capacity
							pRecord->fWattHour, 		//WattHour
							pRecord->fWattHour, 		//IR
							pRecord->fWattHour, 		//Temp
							//pRecord->fPressure, 		//Press
							pRecord->fAvgVoltage, 		//AvgVoltage
							pRecord->fAvgCurrent 		//AvgVoltage						
							);
			}
			fprintf(fExcel, "%s\n", str);	
		}
		fclose(fExcel);
	}

	delete pFileHeader;
	delete pRecord;
	fclose(fp);

	ExecuteExcel(strExcel);

	return TRUE;
}

BOOL CBin2ExcelApp::ConvertCycFile(CString strFile)
{
	CWaitCursor wait;
	
	//파일을 Open 한다.
	FILE *fp = fopen(strFile, "rb");
	if(fp == NULL)	return FALSE;

	PS_RAW_FILE_HEADER rawHeader;
	fread(&rawHeader, sizeof(PS_RAW_FILE_HEADER),1 , fp);
	if(rawHeader.fileHeader.nFileID != PS_ADP_RECORD_FILE_ID)
	{
		fclose(fp);
		return FALSE;
	}

	int nColumnSize = rawHeader.rsHeader.nColumnCount;		//save data setting

	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		fclose(fp);
		return FALSE;
	}
	
	float *pRecord;
	size_t rsSize;

	//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
	pRecord = new float[nColumnSize];
	rsSize = sizeof(float)*nColumnSize;

	FILE *fExcel;
	CString str, strExcel, strTemp;

//	strExcel.Format("%s.csv", strFile.Left(strFile.ReverseFind('.')));
	strExcel = strFile + ".csv";

	fExcel = fopen(strExcel, "wt");
	if(fExcel)
	{
		str.Empty();
		for(int i=0; i<nColumnSize; i++)
		{
			strTemp.Format("%s,", ::PSGetItemName(rawHeader.rsHeader.awColumnItem[i]));
			str += strTemp;
		}
		fprintf(fExcel, "%s\n", str);	

		while(fread(pRecord, rsSize, 1, fp) > 0)
		{
			str.Empty();
			for(int i=0; i<nColumnSize; i++)
			{
				strTemp.Format("%f,", pRecord[i]);
				str += strTemp;
			}
			fprintf(fExcel, "%s\n", str);	
		}
		fclose(fExcel);
	}
	delete pRecord;
	fclose(fp);

	ExecuteExcel(strExcel);
	return TRUE;
}

BOOL CBin2ExcelApp::ExecuteExcel(CString strFileName)
{
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	strTemp = GetExcelPath();
	if(strTemp.IsEmpty())	return FALSE;

	char buff1[_MAX_PATH];
	char buff2[_MAX_PATH];
	ZeroMemory(buff1, _MAX_PATH);
	ZeroMemory(buff2, _MAX_PATH);

	int aa =0;
	do {
		//존재 여부 확인
		aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
		if(aa <= 0)
		{
			if(AfxMessageBox("Excel 실행파일 경로가 잘못되었습니다. 경로를 설정 하십시요.", MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			{
				return FALSE;

			}
			strTemp = GetExcelPath();
			if(strTemp.IsEmpty())	return FALSE;
		}
		
	} while( aa <= 0 );	

	GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
	GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, _MAX_PATH);
	
	strTemp.Format("%s %s", buff2, buff1);
	
	BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		strTemp.Format("%s를 Open할 수 없습니다.", strFileName);
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
	}
	return TRUE;
}

CString CBin2ExcelApp::GetExcelPath()
{
	CString strExcelPath = GetProfileString("Settings", "Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		AfxMessageBox("Excel 위치가 설정되어 있지 않습니다. 위치를 지정하여 주십시요.");
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		WriteProfileString("Settings","Excel Path", strExcelPath);
	}
	return strExcelPath;
}