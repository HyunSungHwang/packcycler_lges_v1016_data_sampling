// Bin2Excel.h : main header file for the BIN2EXCEL application
//

#if !defined(AFX_BIN2EXCEL_H__0E130147_E325_4E11_B92B_27AC9E1D1C8F__INCLUDED_)
#define AFX_BIN2EXCEL_H__0E130147_E325_4E11_B92B_27AC9E1D1C8F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelApp:
// See Bin2Excel.cpp for the implementation of this class
//

class CBin2ExcelApp : public CWinApp
{
public:
	BOOL ConvertCycFile(CString strFile);
	BOOL ConvertRptFile(CString strFile);
	BOOL ExecuteExcel(CString strFileName);
	CString GetExcelPath();
	CBin2ExcelApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBin2ExcelApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CBin2ExcelApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BIN2EXCEL_H__0E130147_E325_4E11_B92B_27AC9E1D1C8F__INCLUDED_)
