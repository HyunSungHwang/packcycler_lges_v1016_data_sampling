// Bin2ExcelDoc.h : interface of the CBin2ExcelDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_BIN2EXCELDOC_H__2467BA78_12A1_4134_AF17_4553D6598E2A__INCLUDED_)
#define AFX_BIN2EXCELDOC_H__2467BA78_12A1_4134_AF17_4553D6598E2A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CBin2ExcelDoc : public CDocument
{
protected: // create from serialization only
	CBin2ExcelDoc();
	DECLARE_DYNCREATE(CBin2ExcelDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBin2ExcelDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBin2ExcelDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBin2ExcelDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BIN2EXCELDOC_H__2467BA78_12A1_4134_AF17_4553D6598E2A__INCLUDED_)
