// Bin2ExcelDoc.cpp : implementation of the CBin2ExcelDoc class
//

#include "stdafx.h"
#include "Bin2Excel.h"

#include "Bin2ExcelDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelDoc

IMPLEMENT_DYNCREATE(CBin2ExcelDoc, CDocument)

BEGIN_MESSAGE_MAP(CBin2ExcelDoc, CDocument)
	//{{AFX_MSG_MAP(CBin2ExcelDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelDoc construction/destruction

CBin2ExcelDoc::CBin2ExcelDoc()
{
	// TODO: add one-time construction code here

}

CBin2ExcelDoc::~CBin2ExcelDoc()
{
}

BOOL CBin2ExcelDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelDoc serialization

void CBin2ExcelDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelDoc diagnostics

#ifdef _DEBUG
void CBin2ExcelDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CBin2ExcelDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBin2ExcelDoc commands
