// CTSRawDataView.h : interface of the CCTSRawDataView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSRAWDATAVIEW_H__8F2CCF8D_610D_4EA5_8228_B07CC14E9B5E__INCLUDED_)
#define AFX_CTSRAWDATAVIEW_H__8F2CCF8D_610D_4EA5_8228_B07CC14E9B5E__INCLUDED_

#include "MyGridWnd.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCTSRawDataView : public CFormView
{
protected: // create from serialization only
	CCTSRawDataView();
	DECLARE_DYNCREATE(CCTSRawDataView)

public:
	//{{AFX_DATA(CCTSRawDataView)
	enum { IDD = IDD_CTSRAWDATA_FORM };
	int		m_nSelPage;
	//}}AFX_DATA

// Attributes
public:
	CCTSRawDataDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSRawDataView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL Fun_SaveData(CString strSavePath);
	BOOL UpdateCANData(CRawData *pRawData, int nPos);
	BOOL UpdateAuxData(CRawData * pRawData, int nPos);
	BOOL InsertRow();
	BOOL DeleteRow();
	BOOL IsSelectAllRow;
	
	BOOL IsEdit;
	
	int nCurPage;	//현재 페이지
	int nTotPage;	//전체 페이지
	BOOL bBarStepIt;
	int nProgressCount;
	
	void ProgressBarShow();
	void TestGrid();
	BOOL UpdateRecordRawData(CRawData * recordData, int nPos);	
	BOOL UpdateStepEndData(CRawData * pRawData);

	void MovePage();

	void UpdateGridData();
	void InitGrid();
	void MakeGrid(int nFileType, int nColCount);

	void EditStepDataToIndex(PS_STEP_END_RECORD * stepData, int nIndex, CString strData);
	void EditRecordDataToIndex(PS_RECORD_RAW_DATA * recData, int nIndex, CString strData);
	virtual ~CCTSRawDataView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	CMyGridWnd m_wndDataGrid;
	//{{AFX_MSG(CCTSRawDataView)
	afx_msg void OnFileOpen();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnStaticPrevious();
	afx_msg void OnStaticNext();
	afx_msg void OnStaticEnd();
	afx_msg void OnStaticStart();
	afx_msg void OnButtonMove();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnFileSave();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMenuInsert();
	afx_msg void OnMenuDelete();
	afx_msg void OnButSave();
	afx_msg void OnButSave2();
	//}}AFX_MSG

	afx_msg void OnGridBtnClicked(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridEndEdit(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridBeginEdit(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridTestMsg(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridRButtonClick(WPARAM wParam, LPARAM lParam);
	
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CTSRawDataView.cpp
inline CCTSRawDataDoc* CCTSRawDataView::GetDocument()
   { return (CCTSRawDataDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSRAWDATAVIEW_H__8F2CCF8D_610D_4EA5_8228_B07CC14E9B5E__INCLUDED_)
