// CTSRawDataView.cpp : implementation of the CCTSRawDataView class
//

#include "stdafx.h"
#include "CTSRawData.h"

#include "CTSRawDataDoc.h"
#include "CTSRawDataView.h"

#include "ProgressBar.h"

#define IDC_HAND MAKEINTRESOURCE(32649)
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataView

IMPLEMENT_DYNCREATE(CCTSRawDataView, CFormView)

BEGIN_MESSAGE_MAP(CCTSRawDataView, CFormView)
	//{{AFX_MSG_MAP(CCTSRawDataView)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_WM_SIZE()
	ON_WM_SETCURSOR()
	ON_BN_CLICKED(IDC_STATIC_PREVIOUS, OnStaticPrevious)
	ON_BN_CLICKED(IDC_STATIC_NEXT, OnStaticNext)
	ON_BN_CLICKED(IDC_STATIC_END, OnStaticEnd)
	ON_BN_CLICKED(IDC_STATIC_START, OnStaticStart)
	ON_BN_CLICKED(IDC_BUTTON_MOVE, OnButtonMove)
	ON_WM_CTLCOLOR()
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_MENU_INSERT, OnMenuInsert)
	ON_COMMAND(ID_MENU_DELETE, OnMenuDelete)
	ON_BN_CLICKED(IDC_BUT_SAVE, OnButSave)
	ON_BN_CLICKED(IDC_BUT_SAVE2, OnButSave2)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
	
	ON_MESSAGE(WM_GRID_CLICK, OnGridBtnClicked)
	ON_MESSAGE(WM_GRID_ENDEDIT, OnGridEndEdit)
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnGridBeginEdit)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnGridRButtonClick)
//	ON_MESSAGE(WM_GRID_CLICK, OnGridTestMsg)
	
END_MESSAGE_MAP()

BOOL PeekAndPump()
{
	static MSG msg;

	while (::PeekMessage(&msg,NULL,0,0,PM_NOREMOVE)) {
		if (!AfxGetApp()->PumpMessage()) {
			::PostQuitMessage(0);
			return FALSE;
		}	
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataView construction/destruction

CCTSRawDataView::CCTSRawDataView()
	: CFormView(CCTSRawDataView::IDD)
{
	//{{AFX_DATA_INIT(CCTSRawDataView)
	m_nSelPage = 0;
	//}}AFX_DATA_INIT
	// TODO: add construction code here

	nProgressCount =0;
	bBarStepIt = FALSE;
	nCurPage = 0;
	nTotPage = 0;
	IsEdit = FALSE;
	IsSelectAllRow = FALSE;
}

CCTSRawDataView::~CCTSRawDataView()
{
}

void CCTSRawDataView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTSRawDataView)
	DDX_Text(pDX, IDC_EDIT_SELECT_PAGE, m_nSelPage);
	//}}AFX_DATA_MAP
}

BOOL CCTSRawDataView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCTSRawDataView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();


	InitGrid();
}

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataView printing

BOOL CCTSRawDataView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCTSRawDataView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCTSRawDataView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CCTSRawDataView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataView diagnostics

#ifdef _DEBUG
void CCTSRawDataView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCTSRawDataView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSRawDataDoc* CCTSRawDataView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSRawDataDoc)));
	return (CCTSRawDataDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataView message handlers

void CCTSRawDataView::OnFileOpen() 
{
	// TODO: Add your command handler code here
	CString str;
	CString strTemp;
//	strTemp.Format("Step end data file(*.%s)|*.%s|Record raw data file(*.%s)|*.%s|Aux data file(*.%s)|*.%s|CAN data file(*.%s)|*.%s|All Files(*.*)|*.*|", 
//					PS_RESULT_FILE_NAME_EXT, PS_RESULT_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT,
//					PS_AUX_FILE_NAME_EXT,PS_AUX_FILE_NAME_EXT, PS_CAN_FILE_NAME_EXT, PS_CAN_FILE_NAME_EXT);

// 	strTemp.Format("Step end data file(*.%s)|*.%s|Record raw data file(*.%s)|*.%s|All Files(*.*)|*.*|", 
// 					PS_RESULT_FILE_NAME_EXT, PS_RESULT_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT);

	strTemp.Format("Step end data file(*.%s)|*.%s|Record raw data file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		PS_CAN_FILE_NAME_EXT, PS_CAN_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT);

	CFileDialog pDlg(TRUE, "cts", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	if(IDOK == pDlg.DoModal())
	{
		CString strFile = pDlg.GetPathName();
		GetDocument()->LoadRawFile(strFile);
		int nStart = strFile.ReverseFind('\\') + 1;
		int nEnd = strFile.ReverseFind('.');
		AfxGetMainWnd()->SetWindowText(strFile.Mid(nStart,nEnd));
	}
	nCurPage = 0;
	nTotPage = GetDocument()->nTotPage;
	CString strTotalPage;
	strTotalPage.Format("%d", nTotPage);
	GetDlgItem(IDC_STATIC_END)->SetWindowText(strTotalPage);
	
	UpdateGridData();
}



void CCTSRawDataView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	

	if(::IsWindow(this->GetSafeHwnd()))
	{
		CRect ctrlRect;
		RECT rect;	
		CString strMsg;
		::GetClientRect(m_hWnd, &rect);

		if(m_wndDataGrid.GetSafeHwnd())
		{
			m_wndDataGrid.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);			
			m_wndDataGrid.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right - ctrlRect.left, rect.bottom - ctrlRect.top, FALSE);
		}
	}
	
}

void CCTSRawDataView::UpdateGridData()
{
	int nFileType = 0;
	int nFiledCount = 0;
	
	CCTSRawDataDoc * pDoc = GetDocument();

	if(m_wndDataGrid.GetRowCount() >= 1)
		m_wndDataGrid.RemoveRows(1, m_wndDataGrid.GetRowCount());

	CRawData * pRawData = pDoc->m_rawDataList;
	if(pRawData != NULL)
	{
		MakeGrid(pRawData->GetFileType(), pRawData->GetColCount());

		if(pRawData->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
		{
			if(UpdateStepEndData(pRawData) == FALSE)
				return;
		}
		else if(pRawData->GetFileType() == PS_ADP_RECORD_FILE_ID)
		{
			if(UpdateRecordRawData(pRawData, 0) == FALSE)
				return;
		}
// 		else if(pRawData->GetFileType() == PS_PNE_AUX_FILE_ID || pRawData->GetFileType() == PS_PNE_AUX_RESULT_FILE_ID)
// 		{
// 			if(UpdateAuxData(pRawData, 0) == FALSE)
// 				return;
// 		}
		else if(pRawData->GetFileType() == PS_PNE_CAN_FILE_ID)
		{
			if(UpdateCANData(pRawData, 0) == FALSE)
				return;
		}
	}

}

BOOL CCTSRawDataView::UpdateStepEndData(CRawData * pRawData)
{
	long nRow = m_wndDataGrid.GetRowCount()+1;
	long nCount = pRawData->GetCount();
	
	for(nRow ; nRow <= nCount; nRow++)
	{
		PS_STEP_END_RECORD * stepData = (PS_STEP_END_RECORD *)pRawData->GetRawDataIndex(nRow-1);
		if(nRow <= MAX_ROW_COUNT)
		{	
			m_wndDataGrid.InsertRows(nRow, 1);
			short nTemp = stepData->chNo;
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 1), nTemp);
			nTemp = stepData->chStepNo;
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 2), nTemp);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 3), stepData->lSaveSequence);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 4), stepData->nIndexFrom);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 5), stepData->nIndexTo);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 6), stepData->nCurrentCycleNum);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 7), stepData->nTotalCycleNum);
			nTemp = stepData->chState;
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 8), nTemp);
			nTemp = stepData->chStepType;
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 9), nTemp);
			nTemp = stepData->chCode;
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 10), nTemp);
			nTemp = stepData->chGradeCode;
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 11), nTemp);		
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 12), stepData->fVoltage);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 13), stepData->fCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 14), stepData->fCapacity);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 15), stepData->fWatt);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 16), stepData->fWattHour);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 17), stepData->fStepTime);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 18), stepData->fTotalTime);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 19), stepData->fImpedance);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 20), stepData->fOvenTemperature);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 21), stepData->fAvgVoltage);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 22), stepData->fAvgCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 23), stepData->fChargeAh);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 24), stepData->fDisChargeAh);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 25), stepData->fCapacitance);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 26), stepData->fChargeWh);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 27), stepData->fDisChargeWh);

/*
float fVoltage;				// Result Data...
float fCurrent;
float fCapacity;
float fWatt;
float fWattHour;
float fStepTime;			// 이번 Step 진행 시간
float fTotalTime;			// 시험 Total 진행 시간
float fImpedance;			// Impedance (AC or DC)
float fOvenTemperature;			//ljb 20100722	Chamber Temparature
float fOvenHumidity;			//ljb 20100722	fAuxVoltage -> Chamber Humidity
float fAvgVoltage;
float fAvgCurrent;
float fChargeAh;
float fDisChargeAh;
float fCapacitance;
float fChargeWh;
float fDisChargeWh;

*/			
		}
		else
			return FALSE;
	}
	

	return TRUE;	//페이지당 아이템 갯수의 한계치를 넘었음
}

/*UINT ProgressThread(LPVOID pParam)
{
	CCTSRawDataView * pView = (CCTSRawDataView *)pParam;
	CProgressBar bar(_T("불러오는 중.."), 40, pView->nProgressCount);
	for(int i = 0; )
	bar.StepIt();
	PeekAndPump();
}*/

//그리드에 데이터를 올린다.
BOOL CCTSRawDataView::UpdateRecordRawData(CRawData * pRecordData, int nPos)
{
	int nCount = 0;
	long nRowCount = m_wndDataGrid.GetRowCount();
	if(nRowCount > MAX_ROW_COUNT || nPos < 0)
		return FALSE;

	if(nCurPage == nTotPage && nCurPage != 0)
		nCount = pRecordData->GetCount() - MAX_ROW_COUNT * nTotPage - 1;		
	else if(nCurPage < nTotPage)
		nCount = MAX_ROW_COUNT;
	else
		nCount = pRecordData->GetCount();
	
	CRecordRawData *  recordData = ((CRecordRawData *)pRecordData);
	
	for(int nRow = 1 ; nRow <= nCount; nRow++)
	{	
		
		if(nRow <= MAX_ROW_COUNT)
		{	
			m_wndDataGrid.InsertRows(nRow, 1);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 1), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fDataSeq);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 2), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fStepTime);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 3), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fVoltage);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 4), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 5), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCapacity);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 6), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fChargeCapacity);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 7), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fDisChargeCapacity);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 8), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fWattHour);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 9), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCapacitance);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 10), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fChargeWh);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 11), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 12), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgVoltage);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 13), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fOvenTemperature);
			
/*
float fDataSeq;
float fStepTime;			// 이번 Step 진행 시간
float fVoltage;				// Result Data...
float fCurrent;
float fCommState;			//ljb 201011
float fCanOutState;			//ljb 201011
float fCanInState;			//ljb 201011
float fVoltageInput;			//ljb 201011
float fVoltagePower;			//ljb 201011
float fVoltageBus;				//ljb 201011
float fCapacity;
float fChargeCapacity;
float fDisChargeCapacity;
float fAuxVolt;						//ljb 20120907
float fWattHour;
float fCapacitance;
float fChargeWh;
float fAvgCurrent;
float fAvgVoltage;
float fOvenTemperature;			//ljb 20100722	Chamber Temparature
float fOvenHumidity;			//ljb 20100722	fAuxVoltage -> Chamber Humidity
float fCVTime;
float fSyncDate;
	float fSyncTime;		
*/
		}
		else
			break;
	}
	
	nCurPage = nPos;
	return TRUE;	//페이지당 아이템 갯수의 한계치를 넘었음
}

//그리드 초기화
void CCTSRawDataView::InitGrid()
{
	m_wndDataGrid.SubclassDlgItem(IDC_CUSTOM_GRID, this);
	m_wndDataGrid.Initialize();
	m_wndDataGrid.GetParam()->EnableUndo(FALSE);
}

//nColCount 에 설정한 값에 따라 그리드를 그린다.
void CCTSRawDataView::MakeGrid(int nFileType, int nColCount)
{
	
	m_wndDataGrid.SetColCount(nColCount);
	//m_wndDataGrid.SetRowCount(MAX_ROW_COUNT);

	CRect rect;
	m_wndDataGrid.GetClientRect(&rect);
	float width = rect.Width()/100.0f;
	int i = 0;
	//if(nColCount == COL_STEP_END_DATA)

	for(int i = 0 ; i < nColCount; i++)
	{
		m_wndDataGrid.SetColWidth(i, i, (int)(width*10.0f));
	}

	m_wndDataGrid.GetParam()->EnableTrackColWidth(TRUE);	//no Resize Col Width
//	m_wndDataGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(RGB(192,192,192)));
	
	if(nFileType == PS_ADP_STEP_RESULT_FILE_ID)
	{
		m_wndDataGrid.SetStyleRange(CGXRange(0, 0), CGXStyle().SetValue("No"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("Ch"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 2), CGXStyle().SetValue("Step"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 3), CGXStyle().SetValue("Seq"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 4), CGXStyle().SetValue("From"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 5), CGXStyle().SetValue("To"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 6), CGXStyle().SetValue("CurCyc"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 7), CGXStyle().SetValue("TotCyc"));

		m_wndDataGrid.SetStyleRange(CGXRange(0, 8), CGXStyle().SetValue("State"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 9), CGXStyle().SetValue("Type"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 10), CGXStyle().SetValue("Code"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 11), CGXStyle().SetValue("Grade"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 12), CGXStyle().SetValue("Volt"));
		
		m_wndDataGrid.SetStyleRange(CGXRange(0, 13), CGXStyle().SetValue("Curr"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 14), CGXStyle().SetValue("Capa"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 15), CGXStyle().SetValue("Watt"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 16), CGXStyle().SetValue("WattH"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 17), CGXStyle().SetValue("StepT"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 18), CGXStyle().SetValue("TotalT"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 19), CGXStyle().SetValue("Imp"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 20), CGXStyle().SetValue("Temp"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 21), CGXStyle().SetValue("Press"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 22), CGXStyle().SetValue("AvgVtg"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 23), CGXStyle().SetValue("AvgCrt"));
	}
	else if(nFileType == PS_ADP_RECORD_FILE_ID)
	{
		m_wndDataGrid.SetStyleRange(CGXRange(0, 0), CGXStyle().SetValue("No"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("DataSeq"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 2), CGXStyle().SetValue("StepT"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 3), CGXStyle().SetValue("Volt"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 4), CGXStyle().SetValue("Curr"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 5), CGXStyle().SetValue("Capa"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 6), CGXStyle().SetValue("Charge Capa"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 7), CGXStyle().SetValue("Discharge Capa"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 8), CGXStyle().SetValue("WattH"));

		m_wndDataGrid.SetStyleRange(CGXRange(0, 9), CGXStyle().SetValue("Capacitance"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 10), CGXStyle().SetValue("ChargeWh"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 11), CGXStyle().SetValue("AvgCtr"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 12), CGXStyle().SetValue("AvgVtg"));
		m_wndDataGrid.SetStyleRange(CGXRange(0, 13), CGXStyle().SetValue("OvenTemp"));
	/*
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 1), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fDataSeq);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 2), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fStepTime);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 3), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fVoltage);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 4), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCurrent);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 5), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCapacity);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 6), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fChargeCapacity);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 7), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fDisChargeCapacity);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 8), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fWattHour);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 9), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCapacitance);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 10), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fChargeWh);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 11), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgCurrent);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 12), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgVoltage);
	m_wndDataGrid.SetValueRange(CGXRange(nRow, 13), ((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fOvenTemperature);

	*/
	}

	else if(nFileType == PS_PNE_AUX_FILE_ID  || PS_PNE_AUX_RESULT_FILE_ID)
	{
		int nIndex = 0;
		m_wndDataGrid.SetStyleRange(CGXRange(0, 0), CGXStyle().SetValue("No"));
//		m_wndDataGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("ChNo"));
		nIndex = 1;
		for(int i = 0 ; i < nColCount; i++)
		{
			m_wndDataGrid.SetStyleRange(CGXRange(0, nIndex++), CGXStyle().SetValue("Value"));
		}
	}

	for(i = 1; i <= nColCount; i++)		//각 데이터를 수정할 수 있도록 설정
		m_wndDataGrid.SetStyleRange(CGXRange().SetCols(i),CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
//	m_wndDataGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
}

//페이지 이동 STATIC에 마우스 커서가 오면 커서 모양을 변경
BOOL CCTSRawDataView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	CPoint pt;   // 마우스 커서 위치를 저장할 객체
    CRect rcNext, rcPrev, rcStart, rcEnd;   // 스태틱 컨트롤의 위치를 저장할 객체.

    // 마우스 커서의 위치를 찾아온다.
    GetCursorPos(&pt); 
    // 스태틱 컨트롤의 위치를 찾아온다.
    GetDlgItem(IDC_STATIC_NEXT)->GetWindowRect(rcNext);
	GetDlgItem(IDC_STATIC_PREVIOUS)->GetWindowRect(rcPrev);
	GetDlgItem(IDC_STATIC_END)->GetWindowRect(rcEnd);
	GetDlgItem(IDC_STATIC_START)->GetWindowRect(rcStart);

    // 만약 마우스가 스태틱 컨트롤 위에 와있으면..
    if(rcNext.PtInRect(pt))
    {
		if(nTotPage != 0 && nCurPage < nTotPage)
		{
        // IDC_HAND라는 스탠다드 커서를 읽어와서 커서를 변경시킨다.
        SetCursor(AfxGetApp()->LoadStandardCursor(MAKEINTRESOURCE(IDC_HAND)));
        return TRUE;
		}
	}

	// 만약 마우스가 스태틱 컨트롤 위에 와있으면..
    if( rcPrev.PtInRect(pt))
    {
		if(nTotPage != 0 && nCurPage > 0)
		{
        // IDC_HAND라는 스탠다드 커서를 읽어와서 커서를 변경시킨다.
        SetCursor(AfxGetApp()->LoadStandardCursor(MAKEINTRESOURCE(IDC_HAND)));
        return TRUE;
		}
	}

		// 만약 마우스가 스태틱 컨트롤 위에 와있으면..
    if( rcStart.PtInRect(pt))
    {
		if(nCurPage != 0)
		{
        // IDC_HAND라는 스탠다드 커서를 읽어와서 커서를 변경시킨다.
        SetCursor(AfxGetApp()->LoadStandardCursor(MAKEINTRESOURCE(IDC_HAND)));
        return TRUE;
		}
	}

		// 만약 마우스가 스태틱 컨트롤 위에 와있으면..
    if( rcEnd.PtInRect(pt))
    {
		if(nCurPage != nTotPage)
		{
        // IDC_HAND라는 스탠다드 커서를 읽어와서 커서를 변경시킨다.
        SetCursor(AfxGetApp()->LoadStandardCursor(MAKEINTRESOURCE(IDC_HAND)));
        return TRUE;
		}
	}

	return CFormView::OnSetCursor(pWnd, nHitTest, message);


}

//이전 페이지로 이동
void CCTSRawDataView::OnStaticPrevious() 
{
	if(nCurPage == 0)
		return;

	nCurPage--;
	MovePage();

}

//다음 페이지로 이동
void CCTSRawDataView::OnStaticNext() 
{
	if(nCurPage == nTotPage)
		return;

	nCurPage++;
	MovePage();
	
}

void CCTSRawDataView::TestGrid()
{
	CProgressBar bar(_T("Progress"), 40, 1000, TRUE);
	short nTemp = 1;
	for(int i = 1; i < 1000; i++)
	{
		bar.StepIt();
		PeekAndPump();
		nTemp++;
		m_wndDataGrid.InsertRows(i, 1);
		m_wndDataGrid.SetValueRange(CGXRange(i, 1), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 2), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 3), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 4), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 5), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 6), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 7), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 8), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 9), nTemp);
		m_wndDataGrid.SetValueRange(CGXRange(i, 10), nTemp);
	}
}


//프로그래스를 보여준다.
void CCTSRawDataView::ProgressBarShow()
{
	CProgressBar bar(_T("Progress"), 40, 5000, TRUE);

	for (int i = 0; i < 5000; i++) {
		bar.StepIt();
		PeekAndPump();
	}
}


//현재 설정된 페이지로 이동
void CCTSRawDataView::MovePage()
{
	CRawData * pRawData;
	CCTSRawDataDoc * pDoc = GetDocument();

	if(m_wndDataGrid.GetRowCount() > 1)
		m_wndDataGrid.RemoveRows(1, m_wndDataGrid.GetRowCount());

	pRawData = pDoc->m_rawDataList;

	if(pRawData != NULL)
	{

		if(pRawData->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
		{
			if(UpdateStepEndData(pRawData) == FALSE)
				return;
		}
		else if(pRawData->GetFileType() == PS_ADP_RECORD_FILE_ID)
		{
			if(UpdateRecordRawData(pRawData, nCurPage) == FALSE)
				return;
		}
	}
	
	m_nSelPage = nCurPage;

	UpdateData(FALSE);
}

//마지막 페이지로 이동
void CCTSRawDataView::OnStaticEnd() 
{
	if(nCurPage == nTotPage)
		return;

	nCurPage = nTotPage;
	MovePage();
	
}

//가장 처음 페이지로 이동
void CCTSRawDataView::OnStaticStart() 
{
	if(nCurPage == 0)
		return;

	nCurPage = 0;
	MovePage();
	
}

//선택한 페이지로 이동
void CCTSRawDataView::OnButtonMove() 
{
	UpdateData();
	if(m_nSelPage < 0 || m_nSelPage > nTotPage)
	{
		CString strError;
		strError.Format("선택범위를 넘었습니다.(%d~%d)", nCurPage, nTotPage);
		AfxMessageBox(strError);
		return;
	}

	if(m_nSelPage == nCurPage)
		return;
	nCurPage = m_nSelPage;
	MovePage();
	
}

HBRUSH CCTSRawDataView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);
	
	switch(nCtlColor)
    {
    case CTLCOLOR_STATIC:  pDC->SetTextColor(RGB(0, 0, 255)); break;
    }
	
	return hbr;
}

void CCTSRawDataView::OnFileSave() 
{
	CString str;
	CString strTemp;
//	strTemp.Format("Step end data file(*.%s)|*.%s|Record raw data file(*.%s)|*.%s|Aux data file(*.%s)|*.%s|CAN data file(*.%s)|*.%s|All Files(*.*)|*.*|", 
//					PS_RESULT_FILE_NAME_EXT, PS_RESULT_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT,
//					PS_AUX_FILE_NAME_EXT,PS_AUX_FILE_NAME_EXT, PS_CAN_FILE_NAME_EXT, PS_CAN_FILE_NAME_EXT);
	strTemp.Format("Step end data file(*.%s)|*.%s|Record raw data file(*.%s)|*.%s|All Files(*.*)|*.*|", 
					PS_RESULT_FILE_NAME_EXT, PS_RESULT_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT);

	CFileDialog pDlg(FALSE, "cts", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	if(IDOK == pDlg.DoModal())
	{
		GetDocument()->SaveRawFile(pDlg.GetPathName());
	}
	
}

void CCTSRawDataView::OnGridBtnClicked(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);	

	if(nCol == 0 && nRow > 0)	//Row만 선택되면 해당 Row 전체가 선택된것과 같다.
		IsSelectAllRow = TRUE;
	else
		IsSelectAllRow = FALSE;

}

void CCTSRawDataView::OnGridEndEdit(WPARAM wParam, LPARAM lParam)
{
	if(!IsEdit)
		return;
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);	

	CCTSRawDataDoc * pDoc = GetDocument();

	CRawData * pRawData = pDoc->m_rawDataList;

	if(pRawData != NULL)
	{
		if(pRawData->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
		{
			PS_STEP_END_RECORD * stepData = (PS_STEP_END_RECORD *)pRawData->GetRawDataIndex(nCurPage * MAX_ROW_COUNT + nRow - 1);
			EditStepDataToIndex(stepData, nCol, m_wndDataGrid.GetValueRowCol(nRow, nCol) );			
		}
		else if(pRawData->GetFileType() == PS_ADP_RECORD_FILE_ID)
		{
			PS_RECORD_RAW_DATA * recData = (PS_RECORD_RAW_DATA *)pRawData->GetRawDataIndex(nCurPage * MAX_ROW_COUNT + nRow - 1);
			EditRecordDataToIndex(recData, nCol, m_wndDataGrid.GetValueRowCol(nRow, nCol) );
		}
	}
	IsEdit = FALSE;
	
	TRACE("End ->Row : %d    Row : %d  Data : %s\n", nCol, nRow, m_wndDataGrid.GetValueRowCol(nRow, nCol));

}

void CCTSRawDataView::OnGridBeginEdit(WPARAM wParam, LPARAM lParam)
{
	IsEdit = TRUE;
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);	

	TRACE("Begin ->Row : %d    Row : %d  Data : %s\n", nCol, nRow, m_wndDataGrid.GetValueRowCol(nRow, nCol));
}

void CCTSRawDataView::EditStepDataToIndex(PS_STEP_END_RECORD * stepData, int nIndex, CString strData)
{
	if(stepData == NULL)
		return;
	float nData = atof(strData);
	switch(nIndex)
	{
	case 1:  stepData->chNo				= nData;	break;
	case 2:  stepData->chStepNo			= nData;	break;
	case 3:  stepData->lSaveSequence	= nData;	break;
	case 4:  stepData->nIndexFrom		= nData;	break;
	case 5:  stepData->nIndexTo			= nData;	break;
	case 6:  stepData->nCurrentCycleNum = nData;	break;
	case 7:  stepData->nTotalCycleNum	= nData;	break;
	case 8:  stepData->chState			= nData;	break;
	case 9:	 stepData->chStepType		= nData;	break;
	case 10: stepData->chCode			= nData;	break;
	case 11: stepData->chGradeCode		= nData;	break;
	case 12: stepData->fVoltage			= nData;	break;
	case 13: stepData->fCurrent			= nData;	break;
	case 14: stepData->fCapacity		= nData;	break;
	case 15: stepData->fWatt			= nData;	break;
	case 16: stepData->fWattHour		= nData;	break;
	case 17: stepData->fStepTime		= nData;	break;
	case 18: stepData->fTotalTime		= nData;	break;
	case 19: stepData->fImpedance		= nData;	break;
	case 20: stepData->fOvenTemperature		= nData;	break;
	case 21: stepData->fAvgVoltage		= nData;	break;
	case 22: stepData->fAvgCurrent		= nData;	break;
	case 23: stepData->fChargeAh		= nData;	break;
	case 24: stepData->fDisChargeAh		= nData;	break;
	case 25: stepData->fCapacitance		= nData;	break;
	case 26: stepData->fChargeWh		= nData;	break;
	case 27: stepData->fDisChargeWh		= nData;	break;
	}
	/*
	float fChargeAh;
	float fDisChargeAh;
	float fCapacitance;
	float fChargeWh;
	float fDisChargeWh;
*/
}

void CCTSRawDataView::EditRecordDataToIndex(PS_RECORD_RAW_DATA * recData, int nIndex, CString strData)
{
	if(recData == NULL)
		return;
	float nData = atof(strData);
	switch(nIndex)
	{
	case 1:  recData->fDataSeq		= nData;	break;
	case 2:  recData->fStepTime		= nData;	break;
	case 3:  recData->fVoltage		= nData;	break;
	case 4:  recData->fCurrent		= nData;	break;
	case 5:  recData->fCapacity		= nData;	break;
	case 6:  recData->fWattHour		= nData;	break;
	case 7:  recData->fAvgCurrent	= nData;	break;
	case 8:  recData->fAvgVoltage	= nData;	break;

	}
}

void CCTSRawDataView::OnGridTestMsg(WPARAM wParam, LPARAM lParam)
{
	AfxMessageBox("이건가?");
}

BOOL CCTSRawDataView::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if(GetFocus() == &m_wndDataGrid)
		{
			switch( pMsg->wParam )
			{
			case VK_INSERT:
				InsertRow();
				 break;

			case VK_DELETE:
				if(IsSelectAllRow)
					DeleteRow();
				break;
			}
		}
	}
	
	return CFormView::PreTranslateMessage(pMsg);
}

//선택된 Row 전체를 삭제한다.
BOOL CCTSRawDataView::DeleteRow()
{
	CCTSRawDataDoc * pDoc = GetDocument();
	CRawData * pRawData = pDoc->m_rawDataList;

	pRawData->GetRawDataIndex(0);

	if(pRawData != NULL)
	{
		CRowColArray ra;
		m_wndDataGrid.GetSelectedRows(ra);

		if(ra.GetSize() < 1)	return FALSE;

		for (int i = ra.GetSize()-1; i >=0 ; i--)
		{
			if(pRawData->DeleteRowDataIndex(ra[i] - 1))			
				m_wndDataGrid.RemoveRows(ra[i], ra[i]);
		}
		
	}	

	return TRUE;
}

//새로운 Row를 추가한다.
BOOL CCTSRawDataView::InsertRow()
{
	CRowColArray ra;
	m_wndDataGrid.GetSelectedRows(ra);
	m_wndDataGrid.InsertRows(ra[0] + 1, 1);

	CCTSRawDataDoc * pDoc = GetDocument();
	CRawData * pRawData = pDoc->m_rawDataList;

	if(pRawData != NULL)
	{
		if(pRawData->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
		{
			PS_STEP_END_RECORD * stepData = new PS_STEP_END_RECORD;
			ZeroMemory(stepData, sizeof(PS_STEP_END_RECORD));
			pRawData->InsertRawDataIndex(stepData, ra[0]);
		}
		else if(pRawData->GetFileType() == PS_ADP_RECORD_FILE_ID)
		{
			PS_RECORD_RAW_DATA * recData = new PS_RECORD_RAW_DATA;
			ZeroMemory(recData, sizeof(PS_RECORD_RAW_DATA));
			pRawData->InsertRawDataIndex(recData, ra[0]);
		}

	}
	return TRUE;
}

void CCTSRawDataView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	
	
}

void CCTSRawDataView::OnGridRButtonClick(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);

	if(nRow < 1)
		return;

	POINT point;		

	GetCursorPos(&point);

	CMenu menu;
    menu.LoadMenu(IDR_POPUP_MENU);
    
    CMenu *pContextMenu = menu.GetSubMenu(0);
	
    pContextMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON |
                  TPM_RIGHTBUTTON, point.x, point.y, AfxGetMainWnd());
}

void CCTSRawDataView::OnMenuInsert() 
{
	InsertRow();
	
}

void CCTSRawDataView::OnMenuDelete() 
{
	DeleteRow();
	
}

BOOL CCTSRawDataView::UpdateAuxData(CRawData *pRawData, int nPos)
{
	int nCount = 0;
	long nRowCount = m_wndDataGrid.GetRowCount();
	if(nRowCount > MAX_ROW_COUNT || nPos < 0)
		return FALSE;

	if(nCurPage == nTotPage && nCurPage != 0)
		nCount = pRawData->GetCount() - MAX_ROW_COUNT * nTotPage - 1;		
	else if(nCurPage < nTotPage)
		nCount = MAX_ROW_COUNT;
	else
		nCount = pRawData->GetCount();
	
	CAuxData *  pAuxData = ((CAuxData *)pRawData);
	
	for(int nRow = 1 ; nRow <= nCount; nRow++)
	{	
		
		if(nRow <= MAX_ROW_COUNT)	
		{
			m_wndDataGrid.InsertRows(nRow, 1);
			float * fArray = (float*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT));
			float data = 0.0f;
			for(int nCol = 0; nCol < pAuxData->GetColCount(); nCol++)
			{
				
				if(nCol > 0)
					data = ((float*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))[nCol];
				//	data = ((float*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))[nCol] / 1000000.0f;
				else
					data = ((float*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))[nCol];
				m_wndDataGrid.SetValueRange(CGXRange(nRow, nCol+1),data) ;
				

			}
			/*m_wndDataGrid.InsertRows(nRow, 1);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 1), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fDataSeq);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 2), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fStepTime);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 3), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fVoltage);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 4), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 5), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCapacity);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 6), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fWattHour);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 7), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 8), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgVoltage);*/
			
	/*		TRACE("%f,%f,%f,%f,%f,%f,%f\n", 	recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fDataSeq,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fStepTime,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fVoltage,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fCurrent,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fCapacity,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fWattHour,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fAvgCurrent,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fAvgVoltage);*/
		}
		else
			break;
	}
	
	nCurPage = nPos;
	return TRUE;
}

BOOL CCTSRawDataView::UpdateCANData(CRawData *pRawData, int nPos)
{
	int nCount = 0;
	long nRowCount = m_wndDataGrid.GetRowCount();
	if(nRowCount > MAX_ROW_COUNT || nPos < 0)
		return FALSE;

	if(nCurPage == nTotPage && nCurPage != 0)
		nCount = pRawData->GetCount() - MAX_ROW_COUNT * nTotPage - 1;		
	else if(nCurPage < nTotPage)
		nCount = MAX_ROW_COUNT;
	else
		nCount = pRawData->GetCount();
	
	CCANData *  pCanData = ((CCANData *)pRawData);
	
	for(int nRow = 1 ; nRow <= nCount; nRow++)
	{	
		
		if(nRow <= MAX_ROW_COUNT)	
		{
			m_wndDataGrid.InsertRows(nRow, 1);
			CAN_VALUE * canArray = (CAN_VALUE*)pCanData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT));
			CAN_VALUE canData;
			ZeroMemory(&canData, sizeof(CAN_VALUE));
			for(int nCol = 0; nCol < pCanData->GetColCount(); nCol++)
			{
				
				if(nCol > 0)
					canData = ((CAN_VALUE*)pCanData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))[nCol];
				//	data = ((float*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))[nCol] / 1000000.0f;
				else
					canData = ((CAN_VALUE*)pCanData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))[nCol];
				m_wndDataGrid.SetValueRange(CGXRange(nRow, nCol+1),canData.lVal[0]) ;
				

			}
			/*m_wndDataGrid.InsertRows(nRow, 1);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 1), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fDataSeq);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 2), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fStepTime);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 3), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fVoltage);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 4), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 5), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fCapacity);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 6), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fWattHour);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 7), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgCurrent);
			m_wndDataGrid.SetValueRange(CGXRange(nRow, 8), ((PS_RECORD_RAW_DATA*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fAvgVoltage);*/
			
	/*		TRACE("%f,%f,%f,%f,%f,%f,%f\n", 	recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fDataSeq,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fStepTime,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fVoltage,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fCurrent,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fCapacity,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fWattHour,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fAvgCurrent,
												recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT))->fAvgVoltage);*/
		}
		else
			break;
	}
	
	nCurPage = nPos;
	return TRUE;
}

void CCTSRawDataView::OnButSave() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");
	
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		GetDocument()->Fun_SaveCSVFile(pDlg.GetPathName());

// 		if(Fun_SaveData(pDlg.GetPathName()) == TRUE)
// 			AfxMessageBox("Save OK");
	}	
}

BOOL CCTSRawDataView::Fun_SaveData(CString strSavePath)
{
//	CCTSRawDataView *pDoc = GetDocument();

	CFile file;
	CString strData,strSaveData;
	CString strTitle, strTitle1;
	int i = 0;
	strSaveData.Empty();

	strTitle = "**Master Configuration**\r\nID(Hex),BaudRate,Ext ID,CV Value,User Filter Mode,Mask1(Hex),Filter1(Hex),Filter2(Hex),Mask2(Hex),Filter1(Hex),Filter2(Hex),Filter3(Hex),Filter4(Hex),Bms Type\r\n";
	TRY
	{
		if(strSavePath.Find('.') < 0) strSavePath += ".csv";
		if(file.Open(strSavePath, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			AfxMessageBox("파일을 생성할 수 없습니다.");
			return FALSE;
		}

		file.Write(strTitle, strTitle.GetLength());	//Title

// 		for(int i = 0; i<m_wndCanGrid.GetRowCount(); i++)
// 		{
// 			for(int j = 1; j <=m_wndCanGrid.GetColCount(); j++)
// 			{
// 				strData.Format("%s,",m_wndCanGrid.GetValueRowCol(i+1, j));
// 				file.Write(strData, strData.GetLength()); 
// 			}
// 			strData.Format("\r\n");
// 			file.Write(strData, strData.GetLength()); //줄바꿈
// 		}
// 		strData.Format("\r\n");
// 		file.Write(strData, strData.GetLength());	//줄바꿈
		
	///////////////////////////////////////////////////////////////////////////	
	
	}
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
	
	file.Close();
	return TRUE;
}

void CCTSRawDataView::OnButSave2() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");
	
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		GetDocument()->Fun_SaveCANCSVFile(pDlg.GetPathName());
		
		// 		if(Fun_SaveData(pDlg.GetPathName()) == TRUE)
		// 			AfxMessageBox("Save OK");
	}		
}
