// RawData2.h: interface for the CRawData2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RAWDATA2_H__34A07723_83B6_4760_816E_9E5207B5BEBE__INCLUDED_)
#define AFX_RAWDATA2_H__34A07723_83B6_4760_816E_9E5207B5BEBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_ROW_COUNT		1000
#define COL_STEP_END_DATA	23
#define COL_RECORD_RAW_DATA	8


typedef struct PS_RECORD_RAW_DATA		
{/*
	float fDataSeq;
	float fStepTime;			// 이번 Step 진행 시간
	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWattHour;
	float fAvgCurrent;
	float fAvgVoltage;	
*/
	float fDataSeq;
	float fStepTime;			// 이번 Step 진행 시간
	float fVoltage;				// Result Data...
	float fCurrent;
	float fCommState;			//ljb 201011
	float fCanOutState;			//ljb 201011
	float fCanInState;			//ljb 201011
	float fVoltageInput;			//ljb 201011
	float fVoltagePower;			//ljb 201011
	float fVoltageBus;				//ljb 201011
	float fCapacity;
	float fChargeCapacity;
	float fDisChargeCapacity;
	float fAuxVolt;						//ljb 20120907
	float fWattHour;
	float fCapacitance;
	float fChargeWh;
	float fAvgCurrent;
	float fAvgVoltage;
	float fOvenTemperature;			//ljb 20100722	Chamber Temparature
	float fOvenHumidity;			//ljb 20100722	fAuxVoltage -> Chamber Humidity
	float fCVTime;
	float fSyncDate;
	float fSyncTime;

// 	float fTotalTime;			// 시험 Total 진행 시간
// 	float fImpedance;			// Impedance (AC or DC)
// 	float fChargeAh;
// 	float fDisChargeAh;
//	float fDisChargeWh;
} PS_RECORD_RAW_DATA;

class CRawData : public CObject  
{
private:
	int nFileType;
	PS_FILE_ID_HEADER fileIDHeader;
	CPtrArray rawData;
protected:
	int nColCount;
public:
	CRawData()
	{
		ZeroMemory(&fileIDHeader, sizeof(PS_FILE_ID_HEADER));
		nColCount = 0;
	}
	~CRawData()
	{
		for(int i = 0 ; i < rawData.GetSize(); i++)
			delete rawData.GetAt(i);

		rawData.RemoveAll();
	}
	
	virtual BOOL GetRawData(void *){return FALSE;};
	virtual int GetFileType(void){return fileIDHeader.nFileID;}
	virtual int GetCount(){return rawData.GetSize();}
	virtual void SetFileHeader(void * pHeader){};
	virtual void * GetFileHeader(){return NULL;};
	virtual void SetFileIDHeader(void * pHeader)
	{
		memcpy(&fileIDHeader, pHeader, sizeof(PS_FILE_ID_HEADER));
	}
	virtual void * GetFileIDHeader()
	{
		return &fileIDHeader;
	}	
	virtual BOOL DeleteRowDataIndex(int nIndex)
	{
		if(nIndex >= 0)
		{
			rawData.RemoveAt(nIndex);
			return TRUE;
		}
		return FALSE;

	}

	virtual void * GetRawDataIndex(LONG nIndex)
	{		
		return rawData.GetAt(nIndex);
	}

	virtual void SetRawData(void * setData, int nType)
	{
		nFileType = nType;
		rawData.Add(setData);
	}

	virtual void InsertRawDataIndex(void * pRawData, int nIndex)
	{
		rawData.InsertAt(nIndex, pRawData);
	}
	virtual int GetColCount(){return nColCount;}
	virtual void SetColCount(int nCount)
	{
		this->nColCount = nCount;
	}

};

class CStepEndData : public CRawData
{
private :
	PS_TEST_FILE_HEADER testFileHeader;
public:
	CStepEndData()
	{
		ZeroMemory(&testFileHeader, sizeof(PS_RECORD_FILE_HEADER));

	}
	~CStepEndData()
	{ 
		
	}

	void SetFileHeader(void * pHeader)
	{
		memcpy(&testFileHeader, pHeader, sizeof(PS_TEST_FILE_HEADER));
	}

	void * GetFileHeader()
	{
		return &testFileHeader;
	}


};

class CRecordRawData : public CRawData
{
private :
	PS_RECORD_FILE_HEADER	recFileHeader;
	float * fRecord;
public:
	CRecordRawData()
	{
		ZeroMemory(&recFileHeader, sizeof(PS_RECORD_FILE_HEADER));
	}
	~CRecordRawData()
	{ 
		
	}

	void SetFileHeader(void * pHeader)
	{
		memcpy(&recFileHeader, pHeader, sizeof(PS_RECORD_FILE_HEADER));
	}

	void * GetFileHeader()
	{
		return &recFileHeader;
	}
	
	void SetColCount(int nCount)
	{
		recFileHeader.nColumnCount = nCount;
		nColCount = nCount;
	}


};

class CAuxData : public CRawData
{
private :
	PS_AUX_DATA_FILE_HEADER auxFileHeader;
public:
	CAuxData()
	{
		ZeroMemory(&auxFileHeader, sizeof(PS_AUX_DATA_FILE_HEADER));
		
	}
	~CAuxData()
	{ 
		
	}
	
	void SetFileHeader(void * pHeader)
	{
		memcpy(&auxFileHeader, pHeader, sizeof(PS_AUX_DATA_FILE_HEADER));
	}
	
	void * GetFileHeader()
	{
		return &auxFileHeader;
	}
	
};

class CCANData : public CRawData
{
private :
	PS_CAN_DATA_FILE_HEADER canFileHeader;
public:
	CCANData()
	{
		ZeroMemory(&canFileHeader, sizeof(PS_CAN_DATA_FILE_HEADER));
		
	}
	~CCANData()
	{ 
		
	}
	
	void SetFileHeader(void * pHeader)
	{
		memcpy(&canFileHeader, pHeader, sizeof(PS_CAN_DATA_FILE_HEADER));
	}
	
	void * GetFileHeader()
	{
		return &canFileHeader;
	}
	
};

#endif  !defined(AFX_RAWDATA2_H__34A07723_83B6_4760_816E_9E5207B5BEBE__INCLUDED_)
