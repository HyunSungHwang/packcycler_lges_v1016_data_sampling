; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCTSRawDataView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CTSRawData.h"
LastPage=0

ClassCount=5
Class1=CCTSRawDataApp
Class2=CCTSRawDataDoc
Class3=CCTSRawDataView
Class4=CMainFrame

ResourceCount=4
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class5=CAboutDlg
Resource3=IDR_POPUP_MENU
Resource4=IDD_CTSRAWDATA_FORM

[CLS:CCTSRawDataApp]
Type=0
HeaderFile=CTSRawData.h
ImplementationFile=CTSRawData.cpp
Filter=N

[CLS:CCTSRawDataDoc]
Type=0
HeaderFile=CTSRawDataDoc.h
ImplementationFile=CTSRawDataDoc.cpp
Filter=N

[CLS:CCTSRawDataView]
Type=0
HeaderFile=CTSRawDataView.h
ImplementationFile=CTSRawDataView.cpp
Filter=W
BaseClass=CFormView
VirtualFilter=VWC
LastObject=ID_MENU_DELETE


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=ID_FILE_OPEN




[CLS:CAboutDlg]
Type=0
HeaderFile=CTSRawData.cpp
ImplementationFile=CTSRawData.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
Command16=ID_APP_ABOUT
CommandCount=16

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_CTSRAWDATA_FORM]
Type=1
Class=CCTSRawDataView
ControlCount=11
Control1=IDC_CUSTOM_GRID,GXWND,1353777152
Control2=IDC_STATIC_PREVIOUS,static,1342308608
Control3=IDC_STATIC_NEXT,static,1342308608
Control4=IDC_STATIC_START,static,1342308608
Control5=IDC_STATIC_END,static,1342308608
Control6=IDC_EDIT_SELECT_PAGE,edit,1350639745
Control7=IDC_BUTTON_MOVE,button,1342242816
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_BUT_SAVE,button,1342242816
Control11=IDC_BUT_SAVE2,button,1342242816

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_APP_ABOUT
CommandCount=3

[MNU:IDR_POPUP_MENU]
Type=1
Class=?
Command1=ID_MENU_INSERT
Command2=ID_MENU_DELETE
CommandCount=2

