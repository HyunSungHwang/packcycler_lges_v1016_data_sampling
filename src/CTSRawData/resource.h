//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CTSRawData.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_CTSRAWDATA_FORM             101
#define IDR_MAINFRAME                   128
#define IDR_CTSRAWTYPE                  129
#define IDR_POPUP_MENU                  130
#define IDC_CUSTOM_GRID                 1000
#define IDC_STATIC_PREVIOUS             1001
#define IDC_STATIC_NEXT                 1002
#define IDC_STATIC_START                1004
#define IDC_STATIC_END                  1005
#define IDC_EDIT_SELECT_PAGE            1006
#define IDC_BUTTON_MOVE                 1007
#define IDC_BUT_SAVE                    1008
#define IDC_BUT_SAVE2                   1009
#define IDM_POPUP_INSERT                32772
#define ID_MENU_INSERT                  32773
#define ID_MENU_DELETE                  32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
