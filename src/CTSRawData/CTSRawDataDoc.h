// CTSRawDataDoc.h : interface of the CCTSRawDataDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSRAWDATADOC_H__F438A47D_6DFE_4255_A416_2C0711DE5520__INCLUDED_)
#define AFX_CTSRAWDATADOC_H__F438A47D_6DFE_4255_A416_2C0711DE5520__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_ROW_COUNT		1000

class CCTSRawDataDoc : public CDocument
{
protected: // create from serialization only
	CCTSRawDataDoc();
	DECLARE_DYNCREATE(CCTSRawDataDoc)
public: 
	CRawData * m_rawDataList;
// Attributes
public:

	//CObList RawList;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSRawDataDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL SaveRawFile(CString strFileName);
	BOOL Fun_SaveCSVFile(CString strFileName);
	BOOL Fun_SaveCANCSVFile(CString strFileName);
	void Fun_SaveRawFile(CString strFileName,CString strData);
	
	int nTotPage;
	void DeleteAllListItem();
	BOOL LoadRawFile(CString strName);
	virtual ~CCTSRawDataDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	
// Generated message map functions
protected:
	//{{AFX_MSG(CCTSRawDataDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSRAWDATADOC_H__F438A47D_6DFE_4255_A416_2C0711DE5520__INCLUDED_)
