// CTSRawDataDoc.cpp : implementation of the CCTSRawDataDoc class
//

#include "stdafx.h"
#include "CTSRawData.h"

#include "CTSRawDataDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataDoc

IMPLEMENT_DYNCREATE(CCTSRawDataDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSRawDataDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSRawDataDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataDoc construction/destruction

CCTSRawDataDoc::CCTSRawDataDoc()
{
	nTotPage = 0;
	m_rawDataList = NULL;

}

CCTSRawDataDoc::~CCTSRawDataDoc()
{
	DeleteAllListItem();
}

BOOL CCTSRawDataDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataDoc serialization

void CCTSRawDataDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataDoc diagnostics

#ifdef _DEBUG
void CCTSRawDataDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSRawDataDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG



/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataDoc commands

BOOL CCTSRawDataDoc::LoadRawFile(CString strName)
{
	PS_FILE_ID_HEADER fileHeader;
	CString str, strTemp, strTemp1;
	CString strCsvFile, strSaveFile;
	CString strFileName;
	
	DeleteAllListItem();

	FILE *fp = fopen(strName, "rb");
	if(fp)
	{
		while(fread(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) > 0)
		{
			if(fileHeader.nFileID == PS_ADP_RECORD_FILE_ID)  //cys 파일
			{
				
				PS_RECORD_FILE_HEADER rawHeader;
				if(fread(&rawHeader, sizeof(PS_RECORD_FILE_HEADER), 1, fp) > 0)
				{
					m_rawDataList = new CRecordRawData();
					m_rawDataList->SetFileIDHeader(&fileHeader);		//파일 ID 헤더 저장
					m_rawDataList->SetFileHeader(&rawHeader);			//파일 헤더 저장
					m_rawDataList->SetColCount(rawHeader.nColumnCount);

					int nColumnSize = rawHeader.nColumnCount;		//save data setting
					int lineSize = sizeof(float)*nColumnSize;
					float *pfBuff = new float[nColumnSize];					

					int nCount = 0;

					//ljb 20131128 add	start
// 				strTemp.Format("CSV data file(*.%s)|*.%s|", "csv","csv");
// 				
// 				CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
// 				pDlg.m_ofn.lpstrTitle = "csv file";
// 				if(IDOK == pDlg.DoModal())
// 				{
// 					strCsvFile = pDlg.GetPathName();
// 					strFileName.Format("%s_sum.csv",strCsvFile.Left(strCsvFile.ReverseFind('.')));
// 				}
				//ljb 20131128 add	end
				
					while(fread(pfBuff, lineSize, 1, fp) > 0)
					{
						PS_RECORD_RAW_DATA * pRecord = new PS_RECORD_RAW_DATA;
						ZeroMemory(pRecord, sizeof(PS_RECORD_RAW_DATA));
						memcpy(pRecord, pfBuff, sizeof(PS_RECORD_RAW_DATA));
						
						
						//m_rawDataList->SetRawData(pRecord, COL_RECORD_RAW_DATA);	//데이터 저장
						m_rawDataList->SetRawData(pRecord, 24);	//데이터 저장
						
						
						
					//	TRACE("%f,%f,%f,%f,%f,%f,%f,%fn",
					//		pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);

						nCount++;
					}
					nTotPage = nCount / MAX_ROW_COUNT;

					delete pfBuff;
					pfBuff = NULL;
					
				}
			}
			else if(fileHeader.nFileID == PS_ADP_STEP_RESULT_FILE_ID)	//CTS 파일
			{
				PS_TEST_FILE_HEADER rsHeader;
				m_rawDataList = new CStepEndData();
				while(fread(&rsHeader, sizeof(PS_TEST_FILE_HEADER), 1, fp) > 0)
				{
					m_rawDataList->SetFileIDHeader(&fileHeader);		//파일 ID 헤더 저장
					m_rawDataList->SetFileHeader(&rsHeader);			//파일 헤더 저장
						
					PS_STEP_END_RECORD Record;
					while(fread(&Record, sizeof(PS_STEP_END_RECORD), 1, fp) > 0)
					{
						if(Record.chGradeCode == 0)	Record.chGradeCode = ' ';
						::PSCellCodeMsg(Record.chCode, strTemp, strTemp1);
						
						PS_STEP_END_RECORD * pRecord = new PS_STEP_END_RECORD;
						ZeroMemory(pRecord, sizeof(PS_STEP_END_RECORD));
						memcpy(pRecord, &Record, sizeof(PS_STEP_END_RECORD));

					
					/*	str.Format("%d,%d,%d,%d,%d,%d,%d,%s,%s,%s,%c,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,",
									Record.chNo,
									Record.chStepNo,
									Record.lSaveSequence,			// Expanded Field
									Record.nIndexFrom, 
									Record.nIndexTo,
									Record.nCurrentCycleNum,
									Record.nTotalCycleNum,
									::PSGetStateMsg(Record.chState),			// Run, Stop(Manual, Error), End
									::PSGetTypeMsg(Record.chStepType),			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
									strTemp,
									Record.chGradeCode,

									Record.fVoltage,				// Result Data...
									Record.fCurrent,
									Record.fCapacity,
									Record.fWatt,
									Record.fWattHour,
									Record.fStepTime,			// 이번 Step 진행 시간
									Record.fTotalTime,			// 시험 Total 진행 시간
									Record.fImpedance,			// Impedance (AC or DC)
									Record.fTemparature,
									Record.fPressure,
									Record.fAvgVoltage,
									Record.fAvgCurrent
									);
						TRACE("%s\n", str);*/
						m_rawDataList->SetColCount(COL_STEP_END_DATA);
						m_rawDataList->SetRawData(pRecord, COL_STEP_END_DATA);	//데이터 저장
					}					
					
				}
			}

// 			else if(fileHeader.nFileID == PS_PNE_AUX_FILE_ID)	//Aux 파일
// 			{
// 				int		nOldFileNo= 1, nFileNo = 1;
// 				CString strFileName;
// 
// 				PS_AUX_DATA_FILE_HEADER auxHeader;
// 
// 				m_rawDataList = new CAuxData();
// 				while(fread(&auxHeader, sizeof(PS_AUX_DATA_FILE_HEADER), 1, fp) > 0)
// 				{
// 	//				m_rawDataList = new CRecordRawData();
// 					m_rawDataList->SetFileIDHeader(&fileHeader);		//파일 ID 헤더 저장
// 					m_rawDataList->SetFileHeader(&auxHeader);			//파일 헤더 저장
// 					m_rawDataList->SetColCount(auxHeader.nColumnCount);
// 					
// 					int nColumnSize = auxHeader.nColumnCount;		//save data setting
// 					int lineSize = sizeof(float)*nColumnSize;
// 					float *pfBuff = new float[nColumnSize];					
// 					
// 					int nCount = 0;
// 					int nSaveCount=0;
// 					CString strSaveData,strSaveTemp;
// 					strSaveData.Empty();
// 
// //
// 					strFileName.Format("c:\\save%05d.csv",nFileNo);		//file 번호 자동 
// 
// 					CFile file;
// 					if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite) == FALSE)
// 					{
// 						AfxMessageBox("파일을 생성할 수 없습니다.");
// 						return FALSE;
// 					}
// 					strSaveData = "NO,Charging Power(W),InterlackBatt,Pack Current,HVBatLevel1Failure,HVBatLevel2Failure,HVPowerConnection,SOC,Pack Voltage,SCH_WakeUpSleepCom,WakeUpType, EOC, HVBatUnderVoltage, BatInternalError, CellUnderVoltage, CellOverVoltage, HVBatOverVoltage, HVBatOverTemp";
// 					strTemp = 	"HVBatOverCurrent, Gen Power(W), Availabel Power(W), mean Temp, HVBatHealth(), Temp, LBC_RefuseToSleep, HvBattState, Available Energy(kWh), SafetyMode1Flag, IsolationImpedance, Max Voltage, Min Voltage, OperatingType, PowerRelayState, BSOC, UDS1, UDS2, UDS3, UDS4";
// 					strSaveData += strTemp;
// 					strTemp = 	"UDS5, UDS6, UDS7, UDS8, C_1, C_2, C_3, C_4, C_5, C_6, C_7, C_8, C_9, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17, C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28, C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39, C_40, C_41";
// 					strSaveData += strTemp;
// 					strTemp = 	"C_42, C_43, C_44, C_45, C_46, C_47, C_48, C_49, C_50, C_51, C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60, C_61, C_62, C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72, C_73, C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83, C_84, C_85";
// 					strSaveData += strTemp;
// 					strTemp = 	"C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94, C_95, C_96, Sum_Cell_V, Pack_V, Temp1, Temp 2, Temp 3, Temp 4, Temp 5, Temp 6, Temp 7, Temp 8, Temp 9, Temp 10, Temp 11, Temp 12, Temp 13, Temp 14, Temp 15, Temp 16, Temp 17, Temp 18, Temp 19, Temp 20, Temp 21";
// 					strSaveData += strTemp;
// 					strTemp = 	"Temp 22, Temp 23, Temp 24, Min_Bat_Temp, Avg_Bat_Temp, Max_Bat_Temp, Busbar_V1, Busbar_V2, Busbar_V3, Busbar_V4, Busbar_V5, Busbar_V6, Busbar_V7, Busbar_V8, Busbar_V9, Busbar_V10, Busbar_V11, Busbar_V12, Busbar_V13, Busbar_V14, Busbar_V15, Busbar_V16, Busbar_V17, Busbar_V18";
// 					strSaveData += strTemp;
// 					strTemp = 	"Busbar_V19, Busbar_V20, Busbar_V21, Busbar_V22, Busbar_V23, Busbar_Reserved \n";
// 					strSaveData += strTemp;
// 					file.Write(strSaveData, strSaveData.GetLength());	//Title
// 					
// 					
// 
// //
// 					ULONG lNo = 0;
// 					while(fread(pfBuff, lineSize, 1, fp) > 0)
// 					{
// 						if (nOldFileNo != nFileNo)
// 						{
// 							nOldFileNo = nFileNo;
// 							strFileName.Format("c:\\save%05d.csv",nFileNo);		//file 번호 자동 
// 							if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite) == FALSE)
// 							{
// 								AfxMessageBox("파일을 생성할 수 없습니다.");
// 								return FALSE;
// 							}
// 							strSaveData = "NO,Charging Power(W),InterlackBatt,Pack Current,HVBatLevel1Failure,HVBatLevel2Failure,HVPowerConnection,SOC,Pack Voltage,SCH_WakeUpSleepCom,WakeUpType, EOC, HVBatUnderVoltage, BatInternalError, CellUnderVoltage, CellOverVoltage, HVBatOverVoltage, HVBatOverTemp";
// 							strTemp = 	"HVBatOverCurrent, Gen Power(W), Availabel Power(W), mean Temp, HVBatHealth(), Temp, LBC_RefuseToSleep, HvBattState, Available Energy(kWh), SafetyMode1Flag, IsolationImpedance, Max Voltage, Min Voltage, OperatingType, PowerRelayState, BSOC, UDS1, UDS2, UDS3, UDS4";
// 							strSaveData += strTemp;
// 							strTemp = 	"UDS5, UDS6, UDS7, UDS8, C_1, C_2, C_3, C_4, C_5, C_6, C_7, C_8, C_9, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17, C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28, C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39, C_40, C_41";
// 							strSaveData += strTemp;
// 							strTemp = 	"C_42, C_43, C_44, C_45, C_46, C_47, C_48, C_49, C_50, C_51, C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60, C_61, C_62, C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72, C_73, C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83, C_84, C_85";
// 							strSaveData += strTemp;
// 							strTemp = 	"C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94, C_95, C_96, Sum_Cell_V, Pack_V, Temp1, Temp 2, Temp 3, Temp 4, Temp 5, Temp 6, Temp 7, Temp 8, Temp 9, Temp 10, Temp 11, Temp 12, Temp 13, Temp 14, Temp 15, Temp 16, Temp 17, Temp 18, Temp 19, Temp 20, Temp 21";
// 							strSaveData += strTemp;
// 							strTemp = 	"Temp 22, Temp 23, Temp 24, Min_Bat_Temp, Avg_Bat_Temp, Max_Bat_Temp, Busbar_V1, Busbar_V2, Busbar_V3, Busbar_V4, Busbar_V5, Busbar_V6, Busbar_V7, Busbar_V8, Busbar_V9, Busbar_V10, Busbar_V11, Busbar_V12, Busbar_V13, Busbar_V14, Busbar_V15, Busbar_V16, Busbar_V17, Busbar_V18";
// 							strSaveData += strTemp;
// 							strTemp = 	"Busbar_V19, Busbar_V20, Busbar_V21, Busbar_V22, Busbar_V23, Busbar_Reserved \n";
// 							strSaveData += strTemp;
// 							file.Write(strSaveData, strSaveData.GetLength());	//Title
// 							//file.Write(strSaveTemp, strSaveTemp.GetLength());	//Title
// 						}
// 
// 						strSaveTemp.Empty();
// 
// 						float * pAuxBuff = new float[nColumnSize];
// 						memcpy(pAuxBuff, pfBuff, lineSize);
// 						m_rawDataList->SetRawData(pAuxBuff, nColumnSize);	//데이터 저장					
// 						
// 						strSaveTemp.Empty();
// 						lNo++;
// 						strSaveTemp.Format("%d,",lNo);	//index
// 
// 						int i=0;
// 						for( int i=0; i<nColumnSize-1; i++)
// 						{
// 							strTemp.Format("%.3f,", pfBuff[i]);
// 							strSaveTemp += strTemp;
// 						}
// 						strTemp.Format("%.3f\n",pfBuff[i]);
// 						strSaveTemp += strTemp;
// 						file.Write(strSaveTemp, strSaveTemp.GetLength());	//Title
// 
// // 						TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
// // 								pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);
// 						
// 						nSaveCount++;
// 						if ((nSaveCount % 65536) == 0)
// 						{
// 							file.Close();
// 							nSaveCount = 0;
// 							nFileNo++;
// 						}
// 						delete pAuxBuff;
// 						pAuxBuff = NULL;
// 					}
// 
// 					file.Close();
// 
// 					AfxMessageBox(strFileName);
// 
// 					if (strSaveData.IsEmpty() == FALSE)
// 					{
// //						Fun_SaveRawFile("C:\\tt.txt",strSaveData);
// 						strSaveData.Empty();
// 					}
// 
// 					
// 					nTotPage = nCount / MAX_ROW_COUNT;
// 					
// 					delete pfBuff;
// 					pfBuff = NULL;
// 					
// 				}
// 			}
// 			else if(fileHeader.nFileID == PS_PNE_AUX_RESULT_FILE_ID)	//Aux 파일
// 			{
// 				PS_AUX_DATA_FILE_HEADER auxHeader;
// 				m_rawDataList = new CAuxData();
// 				while(fread(&auxHeader, sizeof(PS_AUX_DATA_FILE_HEADER), 1, fp) > 0)
// 				{
// 			//		m_rawDataList = new CRecordRawData();
// 					m_rawDataList->SetFileIDHeader(&fileHeader);		//파일 ID 헤더 저장
// 					m_rawDataList->SetFileHeader(&auxHeader);			//파일 헤더 저장
// 					m_rawDataList->SetColCount(auxHeader.nColumnCount);
// 					
// 					int nColumnSize = auxHeader.nColumnCount;		//save data setting
// 					int lineSize = sizeof(float)*nColumnSize;
// 					float *pfBuff = new float[nColumnSize];					
// 					
// 					int nCount = 0;
// 					
// 					while(fread(pfBuff, lineSize, 1, fp) > 0)
// 					{
// 						float * pAuxBuff = new float[nColumnSize];
// 						memcpy(pAuxBuff, pfBuff, lineSize);
// 						m_rawDataList->SetRawData(pAuxBuff, nColumnSize);	//데이터 저장
// 						
// 						
// 						
// 						
// 							TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
// 								pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);
// 						
// 						nCount++;
// 					}
// 					nTotPage = nCount / MAX_ROW_COUNT;
// 					
// 					delete pfBuff;
// 					pfBuff = NULL;
// 					
// 				}
// 			}
			else if(fileHeader.nFileID == PS_PNE_CAN_FILE_ID)	//CAN 파일
			{
				int		nOldFileNo= 1, nFileNo = 1;

				PS_CAN_DATA_FILE_HEADER canHeader;
				m_rawDataList = new CCANData();

				//0. strName 에서 CAN Title 명 얻기
				CString strCanT,strCanTile="", strCanTemp;
				strCanT.Format("%sCAN.tlu", strName.Left(strName.ReverseFind('.')));
				
				CStdioFile sourceFile;
				CFileException ex;
				//CString strFileName;
				//GetDlgItemText(IDC_EDIT,strFileName);     // 읽을 파일이름
				if(!sourceFile.Open(strCanT,CFile::modeRead,&ex))
				{
					CString strMsg;
					ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
					AfxMessageBox(strMsg);
					//return;
				}
				else
				{
					CString strText;
					BOOL bIsNotEOL;    // 라인이 존재하는지 확인.
					while(TRUE)
					{
						bIsNotEOL=sourceFile.ReadString(strText);    // 한 문자열씩 읽는다.
						if(!bIsNotEOL)	break;
						if(strText == "") break;
						strCanTemp = strText.Right(strText.GetLength() - (strText.ReverseFind(',')+1));
						strCanTile += strCanTemp +",";
					}
				}
				strCanTile = strCanTile.Left(strCanTile.GetLength() -1) + "\n";	// ,제거
				sourceFile.Close();


				//1. 출력된 결과 파일 선택 csv
// 				strTemp.Format("Step end data file(*.%s)|*.%s|Record raw data file(*.%s)|*.%s|All Files(*.*)|*.*|", 
// 					PS_RESULT_FILE_NAME_EXT, PS_RESULT_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT, PS_RAW_FILE_NAME_EXT);

				strTemp.Format("CSV data file(*.%s)|*.%s|", "csv","csv");
				
				CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
				pDlg.m_ofn.lpstrTitle = "csv file";
				if(IDOK == pDlg.DoModal())
				{
					strCsvFile = pDlg.GetPathName();
					strFileName.Format("%s_sum.csv",strCsvFile.Left(strCsvFile.ReverseFind('.')));
				}

				//2. strName 
				
				while(fread(&canHeader, sizeof(PS_CAN_DATA_FILE_HEADER), 1, fp) > 0)
				{
					m_rawDataList->SetFileIDHeader(&fileHeader);		//파일 ID 헤더 저장
					m_rawDataList->SetFileHeader(&canHeader);			//파일 헤더 저장
					m_rawDataList->SetColCount(canHeader.nColumnCount);
					
					int nColumnSize = canHeader.nColumnCount;		//save data setting
					int lineSize = sizeof(CAN_VALUE)*nColumnSize;
					CAN_VALUE *pTempBuff = new CAN_VALUE[nColumnSize];					
					
					int nCount = 0;
					ULONG ulSaveCount=0;
					CString strSaveData,strSaveTemp;
					strSaveData.Empty();
					
					
					//strFileName.Format("c:\\save%05d.csv",nFileNo);		//file 번호 자동 
					if (strFileName.IsEmpty())
					{
						AfxMessageBox("저장할 파일명이 없습니다.");
						break;
					}

					CFile file;
					if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite) == FALSE)
					{
						AfxMessageBox("파일을 생성할 수 없습니다.");
						return FALSE;
					}
					//file.Write(strCanTile, strCanTile.GetLength());	//Title write
					

					//3. 결과 파일에서 1줄씩 읽어와서 CAN 정보와 합쳐서 저장 한다.
					CString strText;
					BOOL bIsNotEOL;    // 라인이 존재하는지 확인.
					int i=0;
					if(!sourceFile.Open(strCsvFile,CFile::modeRead,&ex))
					{
						CString strMsg;
						ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
						AfxMessageBox(strMsg);
						//return;
					}
					else
					{
						bIsNotEOL=sourceFile.ReadString(strText);    // 타이틀은 SKIP
						strCanTile = strText + strCanTile;
						file.Write(strCanTile, strCanTile.GetLength());	//Title write

						while(TRUE)
						{
							bIsNotEOL=sourceFile.ReadString(strText);    // 한 문자열씩 읽는다.
							if(!bIsNotEOL)	break;
							if(strText == "") continue;
// 							strCanTemp = strText.Right(strText.GetLength() - (strText.ReverseFind(',')+1));
// 							strCanTile += strCanTemp +",";
							if(fread(pTempBuff, lineSize, 1, fp) > 0)
							{		
								strSaveTemp.Empty();
								
								CAN_VALUE * pCanBuff = new CAN_VALUE[nColumnSize];
								memcpy(pCanBuff, pTempBuff, lineSize);
								//m_rawDataList->SetRawData(pCanBuff, nColumnSize);	//데이터 저장						
								
								strSaveTemp.Empty();
								
								for( int i=0; i<nColumnSize-1; i++)
								{
									strTemp.Format("%.3f,", pCanBuff[i].fVal[0]);
									strSaveTemp += strTemp;
								}
								strTemp.Format("%.3f\n",pCanBuff[i].fVal[0]);
								strSaveTemp += strTemp;

								strSaveTemp = strText + strSaveTemp;	//csv 결과 데이터와 CAN 데이터 합치기

								file.Write(strSaveTemp, strSaveTemp.GetLength());	//Title
								
								ulSaveCount++;
								delete pCanBuff;
								pCanBuff = NULL;
							}
							else
							{
								file.Close();
							}
						}
					}
//					strCanTile = strCanTile.Left(strCanTile.GetLength() -1) + "\n";	// ,제거
					sourceFile.Close();




					strTemp.Format("%s save 완료.(%ul)",strFileName,ulSaveCount);
					AfxMessageBox(strTemp);

					delete pTempBuff;
					pTempBuff = NULL;
				}
			}
			else
			{
				fclose(fp);
				return FALSE;
			}
		}
		fclose(fp);
	}

	return TRUE;
}

void CCTSRawDataDoc::DeleteAllListItem()
{
	if(m_rawDataList != NULL)
		delete m_rawDataList;
}

BOOL CCTSRawDataDoc::SaveRawFile(CString strFileName)
{
	CFile file;

	TRY
	{
		if(strFileName.Find('.') < 0)
		{
			if(m_rawDataList->GetFileType() == PS_ADP_RECORD_FILE_ID)
			{
				strFileName += ".cyc";
			}
			else if(m_rawDataList->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
			{
				strFileName += ".cts";
			}
			else if(m_rawDataList->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
			{
				strFileName += ".aux";
			}
		}

		file.Open(strFileName, CFile::modeCreate|CFile::modeWrite);

		//1. write File ID Header
		file.Write(m_rawDataList->GetFileIDHeader(), sizeof(PS_FILE_ID_HEADER));

		//2. write File Header & Data
		if(m_rawDataList->GetFileType() == PS_ADP_RECORD_FILE_ID)
		{
			file.Write(m_rawDataList->GetFileHeader(), sizeof(PS_RECORD_FILE_HEADER));
			for(int i = 0 ; i < m_rawDataList->GetCount(); i++)
			{				
				file.Write(((CRecordRawData *)m_rawDataList)->GetRawDataIndex(i), sizeof(PS_RECORD_RAW_DATA));
			}
		}
		else if(m_rawDataList->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
		{
			file.Write(m_rawDataList->GetFileHeader(), sizeof(PS_TEST_FILE_HEADER));
			for(int i = 0 ; i < m_rawDataList->GetCount(); i++)
			{				
				file.Write(((CStepEndData *)m_rawDataList)->GetRawDataIndex(i), sizeof(PS_STEP_END_RECORD));
			}
			
		}
		else if(m_rawDataList->GetFileType() == PS_PNE_AUX_FILE_ID)
		{
			file.Write(m_rawDataList->GetFileHeader(), sizeof(PS_AUX_DATA_FILE_HEADER));
			CAuxData * pAuxData = (CAuxData *)m_rawDataList;
			for(int i = 0 ; i < m_rawDataList->GetCount(); i++)
			{					
				float * fArray = (float*)pAuxData->GetRawDataIndex(i);
				float fTemp[32] = {0.0f};
				int nIndex = 0;
				for(int a = 12 ; a < m_rawDataList->GetColCount() ; a++)
				{					
					fTemp[nIndex++] = fArray[a];
				}
				for(int b = 0; b < 12; b++)
					fTemp[nIndex++] = fArray[b];

				file.Write(fTemp, sizeof(float)*pAuxData->GetColCount());
				
				//file.Write(fArray, sizeof(float)*pAuxData->GetColCount());
			}
			
		}
		else if(m_rawDataList->GetFileType() == PS_PNE_CAN_RESULT_FILE_ID)
		{
			file.Write(m_rawDataList->GetFileHeader(), sizeof(PS_TEST_FILE_HEADER));
			for(int i = 0 ; i < m_rawDataList->GetCount(); i++)
			{				
				file.Write(((CStepEndData *)m_rawDataList)->GetRawDataIndex(i), sizeof(PS_STEP_END_RECORD));
			}
			
		}
	}
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
	
	file.Close();
	return TRUE;
}

BOOL CCTSRawDataDoc::Fun_SaveCSVFile(CString strFileName)
{
	CFile file;
	CString strTitle,strData,strTemp;
	float fSeq,fRSeq;
	fSeq=0.0f;
	TRY
	{
		strTitle = "No,StepTime,Volt,Current,Capacity,Charge Cap, Discharge Cap, WattHour, Capacitance, ChargeWh, Avg Curr, Avg Volt, Oven Temp\r\n";
		if(strFileName.Find('.') < 0)
		{
			if(m_rawDataList->GetFileType() == PS_ADP_RECORD_FILE_ID)
			{
				strFileName += "_cyc.csv";
				strTitle = "No,No2\r\n";
			}
			else if(m_rawDataList->GetFileType() == PS_ADP_STEP_RESULT_FILE_ID)
			{
				strFileName += "_cts.csv";
				strTitle = "Cts1,Cts2\r\n";
			}
			else if(m_rawDataList->GetFileType() == PS_PNE_CAN_FILE_ID)
			{
				strFileName += "_can.csv";
				strTitle = "CAN1,CAN2\r\n";
			}
		}


		if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			AfxMessageBox("파일을 생성할 수 없습니다.");
			return FALSE;
		}
		
		file.Write(strTitle, strTitle.GetLength());	//Title

		for(int i = 0 ; i < m_rawDataList->GetCount(); i++)
		{	
			strData.Empty();
// 				if(m_rawDataList->GetFileType() == PS_ADP_RECORD_FILE_ID)
// 				{
// 
// 				}
// 				if(m_rawDataList->GetFileType() == PS_PNE_CAN_RESULT_FILE_ID)
// 				{
// 					
// 				}
// 			((PS_RECORD_RAW_DATA*)recordData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))->fDataSeq
// 			(m_rawDataList->GetRawDataIndex(1)).f->>fDataSeq);


// 			fRSeq = ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fStepTime;
// 			if (fSeq < fRSeq)
// 			{
// 				fSeq =fRSeq;
// 				continue;
// 			}
// 			fSeq=0.0f;

			strData.Format("%.0f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fDataSeq);
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fStepTime );
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fVoltage / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fCurrent / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fCapacity / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fChargeCapacity / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fDisChargeCapacity / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fWattHour / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fCapacitance / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fChargeWh / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fAvgCurrent / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fAvgVoltage / 1000.0f);
			strData += strTemp;
			strTemp.Format(",%.3f \n", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fOvenTemperature / 1000.0f);
			strData += strTemp;
			
			file.Write(strData, strData.GetLength());	//줄바꿈
			//file.Write(((CRecordRawData *)m_rawDataList)->GetRawDataIndex(i), sizeof(PS_RECORD_RAW_DATA));
		}
		
		
	// 		for(int i = 0; i<m_wndCanGrid.GetRowCount(); i++)
	// 		{
	// 			for(int j = 1; j <=m_wndCanGrid.GetColCount(); j++)
	// 			{
	// 				strData.Format("%s,",m_wndCanGrid.GetValueRowCol(i+1, j));
	// 				file.Write(strData, strData.GetLength()); 
	// 			}
	// 			strData.Format("\r\n");
	// 			file.Write(strData, strData.GetLength()); //줄바꿈
	// 		}
	// 		strData.Format("\r\n");
	// 		file.Write(strData, strData.GetLength());	//줄바꿈
	}
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
		
		file.Close();
	return TRUE;
}

BOOL CCTSRawDataDoc::Fun_SaveCANCSVFile(CString strFileName)
{
	int nCount = 0;
	long nRowCount;// = m_wndDataGrid.GetRowCount();
	
	
	CFile file;
	CString strTitle,strData,strTemp;
	TRY
	{
		strTitle = "No,StepTime,Volt,Current,Capacity,Charge Cap, Discharge Cap, WattHour, Capacitance, ChargeWh, Avg Curr, Avg Volt, Oven Temp\r\n";

		if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			AfxMessageBox("파일을 생성할 수 없습니다.");
			return FALSE;
		}
		
		file.Write(strTitle, strTitle.GetLength());	//Title

		CAN_VALUE * canArray;
		CAN_VALUE canData;
		for(int i = 0 ; i < m_rawDataList->GetCount(); i++)
		{	
			strData.Empty();
		
//			CCANData *  pCanData = ((CCANData *)m_rawDataList);
			
//					canArray = (CAN_VALUE*)m_rawDataList->GetRawDataIndex(i);
					for(int nCol = 0; nCol < m_rawDataList->GetColCount(); nCol++)
					{
						ZeroMemory(&canData, sizeof(CAN_VALUE));
						

						if(nCol > 0)
						{
							canData = ((CAN_VALUE*)m_rawDataList->GetRawDataIndex(i))[nCol];
							strTemp.Format(",%f",canData.fVal[0] );
							strData += strTemp;

						//	data = ((float*)pAuxData->GetRawDataIndex(nRow-1 +(nPos*MAX_ROW_COUNT)))[nCol] / 1000000.0f;
						}
						else
						{
							canData = ((CAN_VALUE*)m_rawDataList->GetRawDataIndex(i))[nCol];
							strTemp.Format("%f",canData.fVal[0]);
							strData += strTemp;
						}
					}
					strData += "\n";
					file.Write(strData, strData.GetLength());	//줄바꿈
					
					
// 			strData.Format("%.0f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fDataSeq);
// 			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fStepTime);
// 			strData += strTemp;
// 			strTemp.Format(",%.3f", ((PS_RECORD_RAW_DATA*)m_rawDataList->GetRawDataIndex(i))->fVoltage);
// 			strData += strTemp;
			
//			file.Write(strData, strData.GetLength());	//줄바꿈
			//file.Write(((CRecordRawData *)m_rawDataList)->GetRawDataIndex(i), sizeof(PS_RECORD_RAW_DATA));
		}
	}
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
		
		file.Close();
	return TRUE;
}

void CCTSRawDataDoc::Fun_SaveRawFile(CString strFileName, CString strData)
{
	int nCount = 0;
	long nRowCount;// = m_wndDataGrid.GetRowCount();
	
//	CFileFind fFind;
	CFile file;
	TRY
	{
		//strTitle = "No,StepTime,Volt,Current,Capacity,Charge Cap, Discharge Cap, WattHour, Capacitance, ChargeWh, Avg Curr, Avg Volt, Oven Temp\r\n";
// 		BOOL bFind = fFind.FindFile(strFileName);
// 		if (bFind) fFind.FindNextFile();
// 		if (bFind)
// 		{
			if(file.Open(strFileName, CFile::modeWrite) == FALSE)
			{
				AfxMessageBox("파일을 write 수 없습니다.");
				return ;
			}
			file.SeekToEnd();
// 		}
// 		else
// 		{
// 			if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite) == FALSE)
// 			{
// 				AfxMessageBox("파일을 생성할 수 없습니다.");
// 				return ;
// 			}
// 		}
		
		file.Write(strData, strData.GetLength());	
	}
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
		
	file.Close();
}
