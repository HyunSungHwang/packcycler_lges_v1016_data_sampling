// CTSRawData.h : main header file for the CTSRAWDATA application
//

#if !defined(AFX_CTSRAWDATA_H__5260EC3B_80A4_4FBB_AA4D_D63A1BBBE39B__INCLUDED_)
#define AFX_CTSRAWDATA_H__5260EC3B_80A4_4FBB_AA4D_D63A1BBBE39B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCTSRawDataApp:
// See CTSRawData.cpp for the implementation of this class
//

class CCTSRawDataApp : public CWinApp
{
public:
	CCTSRawDataApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSRawDataApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCTSRawDataApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSRAWDATA_H__5260EC3B_80A4_4FBB_AA4D_D63A1BBBE39B__INCLUDED_)
