// UserRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "UserRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserRecordSet

IMPLEMENT_DYNAMIC(CUserRecordSet, CDaoRecordset)

CUserRecordSet::CUserRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CUserRecordSet)
	m_Index = 0;
	m_UserID = _T("");
	m_Password = _T("");
	m_Name = _T("");
	m_Authority = 0;
	m_RegistedDate = (DATE)0;
	m_Description = _T("");
	m_AutoLogOut = FALSE;
	m_AutoLogOutTime = 0;
	m_nFields = 9;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CUserRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CUserRecordSet::GetDefaultSQL()
{
	return _T("[User]");
}

void CUserRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CUserRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[Index]"), m_Index);
	DFX_Text(pFX, _T("[UserID]"), m_UserID);
	DFX_Text(pFX, _T("[Password]"), m_Password);
	DFX_Text(pFX, _T("[Name]"), m_Name);
	DFX_Long(pFX, _T("[Authority]"), m_Authority);
	DFX_DateTime(pFX, _T("[RegistedDate]"), m_RegistedDate);
	DFX_Text(pFX, _T("[Description]"), m_Description);
	DFX_Bool(pFX, _T("[AutoLogOut]"), m_AutoLogOut);
	DFX_Long(pFX, _T("[AutoLogOutTime]"), m_AutoLogOutTime);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CUserRecordSet diagnostics

#ifdef _DEBUG
void CUserRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CUserRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
