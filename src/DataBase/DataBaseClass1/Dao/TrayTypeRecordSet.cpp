// TrayTypeRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "TrayTypeRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrayTypeRecordSet

IMPLEMENT_DYNAMIC(CTrayTypeRecordSet, CDaoRecordset)

CTrayTypeRecordSet::CTrayTypeRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTrayTypeRecordSet)
	m_TrayType = 0;
	m_TypeName = _T("");
	m_Row = 0;
	m_Col = 0;
	m_nFields = 4;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CTrayTypeRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CTrayTypeRecordSet::GetDefaultSQL()
{
	return _T("[TrayType]");
}

void CTrayTypeRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTrayTypeRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[TrayType]"), m_TrayType);
	DFX_Text(pFX, _T("[TypeName]"), m_TypeName);
	DFX_Long(pFX, _T("[Row]"), m_Row);
	DFX_Long(pFX, _T("[Col]"), m_Col);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTrayTypeRecordSet diagnostics

#ifdef _DEBUG
void CTrayTypeRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CTrayTypeRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
