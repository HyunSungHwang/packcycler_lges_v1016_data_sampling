#if !defined(AFX_TRAYRECORDSET_H__6B51A757_460F_4FAA_A395_A3B07E1D957F__INCLUDED_)
#define AFX_TRAYRECORDSET_H__6B51A757_460F_4FAA_A395_A3B07E1D957F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TrayRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTrayRecordSet recordset

class CTrayRecordSet : public CRecordset
{
public:
	CTrayRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTrayRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CTrayRecordSet, CRecordset)
	long	m_ID;
	long	m_TraySerial;
	CString	m_JigID;
	long	m_TeskID;
	CTime	m_RegistedTime;
	CString	m_TrayNo;
	CString	m_TrayName;
	CString	m_UserName;
	CString	m_Description;
	long	m_ModelKey;
	long	m_TestKey;
	CString	m_LotNo;
	CString	m_TestSerialNo;
	CTime	m_TestDateTime;
	long	m_CellNo;
	long	m_InputCellNo;
	long	m_NormalCount;
	long	m_FailCount;
	CString	m_CellCode;
	CString	m_GradeCode;
	CString	m_OperatorID;
	long	m_ModuleID;
	long	m_GroupIndex;
	long	m_StepIndex;
	long	m_TrayType;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrayRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAYRECORDSET_H__6B51A757_460F_4FAA_A395_A3B07E1D957F__INCLUDED_)
