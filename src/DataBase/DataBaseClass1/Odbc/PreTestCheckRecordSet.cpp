// PreTestCheckRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "PreTestCheckRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet

IMPLEMENT_DYNAMIC(CPreTestCheckRecordSet, CRecordset)

CPreTestCheckRecordSet::CPreTestCheckRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CPreTestCheckRecordSet)
	m_CheckID = 0;
	m_TestID = 0;
	m_MaxV = 0.0f;
	m_MinV = 0.0f;
	m_CurrentRange = 0.0f;
	m_OCVLimit = 0.0f;
	m_TrickleCurrent = 0.0f;
	m_DeltaVoltage = 0.0f;
	m_MaxFaultBattery = 0;
	m_AutoProYN = FALSE;
	m_PreTestCheck = FALSE;
	m_nFields = 13;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CPreTestCheckRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CPreTestCheckRecordSet::GetDefaultSQL()
{
	return _T("[Check]");
}

void CPreTestCheckRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CPreTestCheckRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[CheckID]"), m_CheckID);
	RFX_Long(pFX, _T("[TestID]"), m_TestID);
	RFX_Single(pFX, _T("[MaxV]"), m_MaxV);
	RFX_Single(pFX, _T("[MinV]"), m_MinV);
	RFX_Single(pFX, _T("[CurrentRange]"), m_CurrentRange);
	RFX_Single(pFX, _T("[OCVLimit]"), m_OCVLimit);
	RFX_Single(pFX, _T("[TrickleCurrent]"), m_TrickleCurrent);
	RFX_Date(pFX, _T("[TrickleTime]"), m_TrickleTime);
	RFX_Single(pFX, _T("[DeltaVoltage]"), m_DeltaVoltage);
	RFX_Long(pFX, _T("[MaxFaultBattery]"), m_MaxFaultBattery);
	RFX_Date(pFX, _T("[AutoTime]"), m_AutoTime);
	RFX_Bool(pFX, _T("[AutoProYN]"), m_AutoProYN);
	RFX_Bool(pFX, _T("[PreTestCheck]"), m_PreTestCheck);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet diagnostics

#ifdef _DEBUG
void CPreTestCheckRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CPreTestCheckRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
