// RoutingRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "RoutingRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRoutingRecordSet

IMPLEMENT_DYNAMIC(CRoutingRecordSet, CDaoRecordset)

CRoutingRecordSet::CRoutingRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CRoutingRecordSet)
	m_RoutingID = 0;
	m_StepID = 0;
	m_ProcID = 0;
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CRoutingRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
}

CString CRoutingRecordSet::GetDefaultSQL()
{
	return _T("[Routing]");
}

void CRoutingRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CRoutingRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[RoutingID]"), m_RoutingID);
	DFX_Long(pFX, _T("[StepID]"), m_StepID);
	DFX_Long(pFX, _T("[ProcID]"), m_ProcID);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CRoutingRecordSet diagnostics

#ifdef _DEBUG
void CRoutingRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CRoutingRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
