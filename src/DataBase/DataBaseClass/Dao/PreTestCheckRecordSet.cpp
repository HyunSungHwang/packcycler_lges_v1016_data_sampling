// PreTestCheckRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "PreTestCheckRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet

IMPLEMENT_DYNAMIC(CPreTestCheckRecordSet, CDaoRecordset)

extern CString GetDataBaseName();

CPreTestCheckRecordSet::CPreTestCheckRecordSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CPreTestCheckRecordSet)
	m_CheckID = 0;
	m_TestID = 0;
	m_MaxV = 0.0f;
	m_MinV = 0.0f;
	m_CurrentRange = 0.0f;
	m_OCVLimit = 0.0f;
	m_TrickleCurrent = 0.0f;
	m_TrickleVoltage = 0.0f;
	m_TrickleTime = 0;
	m_DeltaVoltage = 0.0f;
	m_MaxFaultBattery = 0;
	m_AutoTime = (DATE)0;
	m_AutoProYN = FALSE;
	m_PreTestCheck = FALSE;
	m_nFields = 13;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}

CString CPreTestCheckRecordSet::GetDefaultDBName()
{
	return _T(GetDataBaseName());
//	return _T("C:\\My Documents\\Condition.mdb");
}


CString CPreTestCheckRecordSet::GetDefaultSQL()
{
	return _T("[Check]");
}

void CPreTestCheckRecordSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CPreTestCheckRecordSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[CheckID]"), m_CheckID);
	DFX_Long(pFX, _T("[TestID]"), m_TestID);
	DFX_Single(pFX, _T("[MaxV]"), m_MaxV);
	DFX_Single(pFX, _T("[MinV]"), m_MinV);
	DFX_Single(pFX, _T("[CurrentRange]"), m_CurrentRange);
	DFX_Single(pFX, _T("[OCVLimit]"), m_OCVLimit);
	DFX_Single(pFX, _T("[TrickleCurrent]"), m_TrickleCurrent);
	DFX_Long(pFX, _T("[TrickleTime]"), m_TrickleTime);
	DFX_Single(pFX, _T("[DeltaVoltage]"), m_DeltaVoltage);
	DFX_Long(pFX, _T("[MaxFaultBattery]"), m_MaxFaultBattery);
	DFX_DateTime(pFX, _T("[AutoTime]"), m_AutoTime);
	DFX_Bool(pFX, _T("[AutoProYN]"), m_AutoProYN);
	DFX_Bool(pFX, _T("[PreTestCheck]"), m_PreTestCheck);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CPreTestCheckRecordSet diagnostics

#ifdef _DEBUG
void CPreTestCheckRecordSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CPreTestCheckRecordSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
