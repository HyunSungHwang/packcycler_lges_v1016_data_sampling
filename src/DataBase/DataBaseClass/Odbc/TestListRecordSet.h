#if !defined(AFX_TESTLISTRECORDSET_H__7D0B896E_BCCB_415E_825D_E9EC85AA940C__INCLUDED_)
#define AFX_TESTLISTRECORDSET_H__7D0B896E_BCCB_415E_825D_E9EC85AA940C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestListRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestListRecordSet recordset

class CTestListRecordSet : public CRecordset
{
public:
	CTestListRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestListRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CTestListRecordSet, CRecordset)
	long	m_TestID;
	long	m_ModelID;
	CString	m_TestName;
	CString	m_Description;
	CString	m_Creator;
	CTime	m_ModifiedTime;
	BOOL	m_PreTestCheck;
	long	m_ProcTypeID;
	long	m_TestNo;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestListRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTLISTRECORDSET_H__7D0B896E_BCCB_415E_825D_E9EC85AA940C__INCLUDED_)
