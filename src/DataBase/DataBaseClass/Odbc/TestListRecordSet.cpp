// TestListRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "TestListRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestListRecordSet

IMPLEMENT_DYNAMIC(CTestListRecordSet, CRecordset)

CTestListRecordSet::CTestListRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestListRecordSet)
	m_TestID = 0;
	m_ModelID = 0;
	m_TestName = _T("");
	m_Description = _T("");
	m_Creator = _T("");
	m_PreTestCheck = FALSE;
	m_ProcTypeID = 0;
	m_ModifiedTime = CTime::GetCurrentTime();
	m_TestNo = 0;
	m_nFields = 9;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CTestListRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestCondition");
}

CString CTestListRecordSet::GetDefaultSQL()
{
	return _T("[TestName]");
}

void CTestListRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestListRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Long(pFX, _T("[TestID]"), m_TestID);
	RFX_Long(pFX, _T("[ModelID]"), m_ModelID);
	RFX_Text(pFX, _T("[TestName]"), m_TestName);
	RFX_Text(pFX, _T("[Description]"), m_Description);
	RFX_Text(pFX, _T("[Creator]"), m_Creator);
	RFX_Date(pFX, _T("[ModifiedTime]"), m_ModifiedTime);
	RFX_Bool(pFX, _T("[PreTestCheck]"), m_PreTestCheck);
	RFX_Long(pFX, _T("[ProcTypeID]"), m_ProcTypeID);
	RFX_Long(pFX, _T("[TestNo]"), m_TestNo);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestListRecordSet diagnostics

#ifdef _DEBUG
void CTestListRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestListRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
