///////////////////////////////////////
//
//		ADPOWER Battery Charge/Discharge System
//		Base define file
//
///////////////////////////////////////

#ifndef _ADPOWER_BASE_DEFINE_INCLUDE_H_
#define _ADPOWER_BASE_DEFINE_INCLUDE_H_

//#define _EDLC_TEST_SYSTEM

//Formation과 같이 사용되므로 Index를 변경하면 안됨 
#define		PS_MAX_STEP					100			//Max Step 	
#define		PS_MAX_SUB_STEP				10			//Max Sub Step
#define		PS_MAX_GRADE_STEP			10			//Max Grade Step
#define		PS_MAX_STEP_GRADE			4			//Max Grade item In One Step
#define		PS_MAX_COMP_POINT			3			//Voltage Compation Point
#define		PS_MAX_DELTA_POINT			3			//Delta Value Check Point

//Step Type 정의
//Step Type은 Max 31이하로 정의 하여야 한다.
#define		PS_STEP_NONE			0x00
#define		PS_STEP_CHARGE			0x01
#define		PS_STEP_DISCHARGE		0x02
#define		PS_STEP_REST			0x03
#define		PS_STEP_OCV				0x04
#define		PS_STEP_IMPEDANCE		0x05
#define		PS_STEP_END				0x06
#define		PS_STEP_ADV_CYCLE		0x07
#define		PS_STEP_LOOP			0x08
#define		PS_STEP_PATTERN			0x09		//2006/03/24 사용자가 입력한 pattern data를 실행
//#define		PS_STEP_LONG_REST		0x0A
#define		PS_STEP_EXT_CAN			0x0B		//ljb 2011318 이재복 ////////// 자부연 EXTERNAL CAN
#define		PS_STEP_USER_MAP		0x0C		//2014.09.02 UserMap 추가.

#define		PS_STEP_NEXT			251
#define		PS_STEP_PAUSE			252			//ljb 20100819
//Step의 Mode
#define		PS_MODE_CCCV			0x01
#define		PS_MODE_CC				0x02
#define		PS_MODE_CV				0x03
#define		PS_MODE_DCIMP			0x04
#define		PS_MODE_ACIMP			0x05
#define		PS_MODE_CP				0x06
#define		PS_MODE_PUSE			0x07
#define		PS_MODE_CR				0x08
#define		PS_MODE_CCCP			0x09		//ljb 201012

#define		PS_GOTO_NEXT_CYC		0x00

//#define		PS_CHARGE_MODE_STRING1		"CC/CV\nCC\nCV\nCP\nCR"
//#define		PS_CHARGE_MODE_STRING1		"CC/CV\nCC\nCV\nCP\nCC/CP" //ljb 20101214
//#define		PS_CHARGE_MODE_STRING1		"CC/CV\nCC\nCV\nCP\nCR" //ljb 2011322 이재복 //////////
#define		PS_CHARGE_MODE_STRING1		"CC/CV\nCC\nCV\nCP" //ksj 20210610 : CR모드 제거. 이미 기능이 제거되어있는데, 목록에 노출되어있어 제거함.
//#define		PS_CHARGE_MODE_STRING1		"CC/CV\nCC\nCV\nCP" //ljb 20120629 이재복 //////////
#define		PS_CHARGE_MODE_STRING2		"CC/CV\nCC\nCV"

//#define		PS_IMPEDANCE_MODE_STRING	"DC\nAC"
#define		PS_IMPEDANCE_MODE_STRING	"DC"	//ljb 20100928
#define		PS_RANGE_STRING				"Auto\nRange 1\nRange 2\nRange 3"	//khs 20081113

//Range
#define		PS_CURRENT_RANGE_HIGH		0
#define		PS_CURRENT_RANGE_LOW		1
#define		PS_CURRENT_RANGE_MICRO		2
#define		PS_RANGE1					0
#define		PS_RANGE2					1
#define		PS_RANGE3					2

#define		PS_SCHEDULE_DATABASE_NAME		"Pack_Schedule_2000.mdb"
#define		PS_CODE_DATABASE_NAME			"Pack_Code_2000.mdb" //ksj 20200804 : 스케쥴DB에서 코드 DB 분리.
// #define		PS_SCHEDULE_DATABASE_NAME_EN		"Pack_Schedule_2000_EN.mdb" 20190701
// #define		PS_SCHEDULE_DATABASE_NAME_PL		"Pack_Schedule_2000_PL.mdb"
#define		PS_WORKINFO_DATABASE_NAME		"Work_Info_2000.mdb"		//ljb 20160314 add

#define PMS_SUPERVISOR				0xFFFFFFFF		//모든 권한
#define PMS_MODULE_SHUT_DOWN		0x00000001		//모듈 종료 (Power Off)
#define PMS_MODULE_CAL_UPDATE		0x00000002		//보정 파일 Update
#define PMS_GROUP_CONTROL_CMD		0x00000004		//Control Command
#define PMS_TEST_CONDITION_SEND		0x00000008		//시험 조건 전송
#define PMS_FORM_START				0x00000010		//PowerForm 시작
#define PMS_FORM_CLOSE				0x00000020		//Powerform 종료
#define PMS_USER_SETTING_CHANGE		0x00000040		//사용자 설정 변경
#define PMS_MODULE_ADD_DELETE		0x00000080		//모듈 추가 삭제 
#define PMS_EDITOR_START			0x00000100		//시험 조건 편집기 시작
#define PMS_EDITOR_EDIT				0x00000200		//시험 조건 편집 
#define PMS_TRAY_REG				0x00000400		//Tray 등록
#define PMS_MODULE_MODE_CHANGE		0x00000800

#define PS_USER_SUPER		PMS_SUPERVISOR
#define PS_USER_ADMIN		PMS_SUPERVISOR
#define PS_USER_OPERATOR	(PMS_GROUP_CONTROL_CMD|PMS_TEST_CONDITION_SEND|PMS_FORM_START|PMS_FORM_CLOSE|PMS_EDITOR_START)
#define PS_USER_GUEST		(PMS_FORM_START|PMS_EDITOR_START|PMS_FORM_CLOSE)

#define		PS_MAX_FILE_ITEM_NUM			80		//ljb 20100313 32 -> 41, ljb 20110128 41 -> 80

#define		PS_MAX_FILE_SAVE_ITEM_NUM		    36		//ljb 20131204 24 -> 30 => 34 => 36  //yulee 20190318 check //yulee 20190514 cny work transfering
#define		PS_MAX_FILE_SAVE_ITEM_NUM_1011_v2	34		//ljb 20131204 24 -> 30 => 34 => 36  //yulee 20190318 check //yulee 20190514 cny work transfering
#define		PS_MAX_FILE_SAVE_ITEM_NUM_1011PRE	30		//yulee 20190221 PS_RECORD_FILE_HEADER -> PS_RECORD_FILE_HEADER_1011_PRE
#define		PS_MAX_FILE_SAVE_ITEM_NUM_100F  30		//yulee 20190221 PS_RECORD_FILE_HEADER -> PS_RECORD_FILE_HEADER_100F_READ
#define		PS_MAX_ITEM_NUM					100		//2008/7/24 =>32에서 40로 변경 RowFile Version 상승		//ljb 20150513 50 ->100
//#define		PS_RESTORE_MAX_ITEM_NUM			90		//ljb 201007 52 => 20100914 80	, 20131205 80 -> 90
#define		PS_RESTORE_MAX_ITEM_NUM			100		//ljb 201007 52 => 20100914 80	, 20131205 80 -> 90 //ksj 20200127 : 90 -> 100
#define		PS_MAX_ITEM_NUM_V1004			40		//2008/7/24 =>32에서 40로 변경 RowFile Version 상승 
#define		PS_MAX_ITEM_NUM_V1013			43		//yulee 20190627 //1013 CWA
#define		PS_MAX_ITEM_NUM_V1014			40		//yulee 20190627
#define		PS_MAX_ITEM_NUM_V1015			40			
#define		PS_MAX_ITEM_NUM_V1009			28		//2009/4/10 =>32에서 28로 변경 RowFile Version 상승 
#define		PS_MAX_ITEM_NUM_V100B			100		//ljb 20101227 27, 20110128 27 -> 80	20150513 80->100
#define		PS_MAX_AUX_FILE_ITEM			32
//#define		PS_MAX_AUX_SAVE_FILE_COLUMN		1024	//ljb 20181212 edit 520 -> 1024 //DAQ512 수정 포인트
#define		PS_MAX_AUX_SAVE_FILE_COLUMN		520//2007/10/17 130 -> 520으로 변경 (MAX 온도 : 256, MAX 전압 : 256, 기타 Column : 8)
#define		PS_MAX_CAN_SAVE_FILE_COLUMN		1030	//2007/10/17 130 -> 520으로 변경 (MAX Master : 256, MAX Slave : 256, 기타 Column : 8)

#define		PS_MAX_PATTERN_STEP_COUNT		100		//20081013 kjh Pattern Step의 최대 갯수는 10 -> 100(ljb 201007) 


//각 data 항목의 Index
#define		PS_STATE					0x00
#define		PS_VOLTAGE					0x01
#define		PS_CURRENT					0x02
#define		PS_CAPACITY					0x03
#define		PS_IMPEDANCE				0x04
#define		PS_CODE						0x05
#define		PS_STEP_TIME				0x06
#define		PS_TOT_TIME					0x07
#define		PS_GRADE_CODE				0x08
#define		PS_STEP_NO					0x09
#define		PS_WATT						0x0A
#define		PS_WATT_HOUR				0x0B
#define		PS_OVEN_TEMPERATURE			0x0C		// Chamber Temperature
#define		PS_OVEN_HUMIDITY			0x0D		// Chamber Humidity
#define		PS_STEP_TYPE				0x0E
#define		PS_CUR_CYCLE				0x0F
#define		PS_TOT_CYCLE				0x10
#define		PS_TEST_NAME				0x11
#define		PS_SCHEDULE_NAME			0x12
#define		PS_CHANNEL_NO				0x13
#define		PS_MODULE_NO				0x14
#define		PS_LOT_NO					0x15
#define		PS_DATA_SEQ					0x16
#define		PS_AVG_CURRENT				0x17
#define		PS_AVG_VOLTAGE				0x18
#define		PS_CAPACITY_SUM				0x19  
#define		PS_CHARGE_CAP				0x1a  
#define		PS_DISCHARGE_CAP			0x1b
#define		PS_METER_DATA				0x1c
#define		PS_START_TIME				0x1d
#define		PS_END_TIME					0x1e
#define		PS_CAN_DATA					0x1f

//Prococol Version 0x1005 20080724/////////////////////////////////////////
#define		PS_CV_TIME					0x20

//Prococol Version 0x1009 201010  /////////////////////////////////////////
#define		PS_AUX_TEMPERATURE			0x21//34
#define		PS_AUX_VOLTAGE				0x22//35			//ljb 20100726 추가
#define		PS_AUX_TEMPERATURE_TH		0x23//36
//#define	PS_AUX_HUMIDITY				0x34// //ksj 20200116 : 하단에 순서에 맞게 0x34로 추가
#define		PS_CAPACITANCE				0x24//37
#define		PS_CHARGE_WH				0x25//38
#define		PS_DISCHARGE_WH				0x26//39
#define		PS_SYNC_DATE				0x27//40
#define		PS_SYNC_TIME				0x28//41
#define		PS_ACC_CYCLE1				0x29//42
#define		PS_ACC_CYCLE2				0x2a//43
#define		PS_ACC_CYCLE3				0x2b//44
#define		PS_ACC_CYCLE4				0x2c//45
#define		PS_ACC_CYCLE5				0x2d//46
#define		PS_MULTI_CYCLE1				0x2e//
//#define		PS_MULTI_CYCLE1		0x2f//47
#define		PS_MULTI_CYCLE2				0x30//48
#define		PS_MULTI_CYCLE3				0x31//49
#define		PS_MULTI_CYCLE4				0x32//50
#define		PS_MULTI_CYCLE5				0x33//51
#define		PS_AUX_HUMIDITY				0x34// //ksj 20200116 : v1016 습도 센서 추가. 이거 확인해야됨.. 임시로 0x34로 해놓음.
//////////////////////////////////////////////////////////////////////////
#define		PS_AUX_DATA_COUNT			0x3f

//Prococol Version 0x100B 201012  /////////////////////////////////////////
#define		PS_AUX_COMM_TEMP			0x40	//ljb 201009 통신 상태
#define		PS_AUX_COMM_VOLT			0x41	//ljb 201009 통신 상태
#define		PS_CAN_COMM_MASTER			0x42	//ljb 201009 통신 상태
#define		PS_CAN_COMM_SLAVE			0x43	//ljb 201009 통신 상태
#define		PS_COMM_STATE				0x44	//ljb 201009 전체 통신 상태
#define		PS_CHAMBER_USING			0x45	//ljb 201010 v100b
#define		PS_RECORD_TIME_NO			0x46	//ljb 201010 v100b	//R1,R2,R3 Time 번호 SBC -> PC

#define		PS_VOLTAGE_INPUT			0x48	//ljb 201011 
#define		PS_VOLTAGE_POWER			0x49	//ljb 201011 
#define		PS_VOLTAGE_BUS				0x4a	//ljb 201011 
#define		PS_CAN_OUTPUT_STATE			0x4b	//ljb 201012 
#define		PS_CAN_INPUT_STATE			0x4c	//ljb 201012 
#define		PS_MUX_USE_STATE			0x4d	//ljb 20151111
#define		PS_MUX_BACKUP_INFO			0x4e	//ljb 20151111 

#define		PS_TOT_TIME_DAY				0x50	//ljb 20131125 add
#define		PS_STEP_TIME_DAY			0x51	//ljb 20131125 add
#define		PS_CV_TIME_DAY				0x52	//ljb 20131125 add

#define		PS_LOAD_VOLT				0x53		// Loader Voltage	20150427 add
#define		PS_LOAD_CURR				0x54		// Loader Current	20150427 add 

#define		PS_CHILLER_REF_TEMP			0x55		// Chiller Ref Temp		20170912 add
#define		PS_CHILLER_CUR_TEMP			0x56		// Chiller Cur Temp		20170912 add

#define		PS_CHILLER_REF_PUMP			0x57		// Chiller Ref Pump		20170912 add
#define		PS_CHILLER_CUR_PUMP			0x58		// Chiller Cur Pump		20170912 add

#define		PS_CELLBAL_CH_DATA			0x59		// Chiller CH DATA		20170912 add
#define		PS_CELLBAL_RES				0x5A		// Balancing Resistance

#define		PS_PWRSUPPLY_SETVOLT		0x5B		// PowerSupply SetVoltage//yulee 20190514 cny work transfering
#define		PS_PWRSUPPLY_CURVOLT		0x5C		// PowerSupply CurVoltage
#define		PS_CBANK					0x5D		// item94에 추가 됨
#define		PS_STEP_MODE				0x5F		//ksj 20200625 : 저장하는 항목은 아니고, 모니터링용으로 추가.

//Grading 항목 정의 
#define		PS_NONE					0
#define		PS_GRADE_VOLTAGE		1	
#define		PS_GRADE_CAPACITY		2
#define		PS_GRADE_IMPEDANCE		3
#define		PS_GRADE_CURRENT		4	
#define		PS_GRADE_TIME			5
#define		PS_GRADE_FARAD			6

//Aux or CAN Type 
#define ID_CAN_TYPE					1
#define ID_AUX_TYPE					2
#define ID_AUX_THERMISTOR_TYPE		3	//20160504 add
#define ID_AUX_HUMIDITY_TYPE		4	//ksj 20200207 : V1016 습도 센서 타입


#define ID_REG_USER_NOMAL			1
#define ID_REG_USER_MANAGE			2

//Aux Type 
#define		PS_AUX_TYPE_TEMPERATURE		0
#define		PS_AUX_TYPE_VOLTAGE			1
#define		PS_AUX_TYPE_TEMPERATURE_TH	2		//ljb 20160428 써미스터 추가
#define		PS_AUX_TYPE_HUMIDITY		3		//ksj 20200116 : 습도 센서 추가

#define		PS_AUX_TYPE_MAX				PS_AUX_TYPE_HUMIDITY+1	//ksj 20200521 : 센서 타입 개수 4개

//Aux Data 항목 Index
#define		PS_AUX_CHANNEL_NO		0x00
#define		PS_AUX_OWNER_CH_NO		0x01
#define		PS_AUX_TYPE				0x02
#define		PS_AUX_VALUE			0x03

//Can Type
#define		PS_CAN_TYPE_MASTER			1
#define		PS_CAN_TYPE_SLAVE			2

//Can Graph Set
#define		PS_CAN_Set1					1
#define		PS_CAN_Set2					2
#define		PS_CAN_Set3					3

//Can Data Type
#define		PS_CAN_DATA_TYPE_UNSIGNED	0
#define		PS_CAN_DATA_TYPE_SIGNED		1
#define		PS_CAN_DATA_TYPE_FLOAT		2
#define		PS_CAN_DATA_TYPE_STRING		3
#define		PS_CAN_DATA_TYPE_HEX		4

//CAN Data 항목 Index
#define		PS_CAN_CHANNEL_NO		0x00
#define		PS_CAN_OWNER_CH_NO		0x01
#define		PS_CAN_TYPE				0x02
#define		PS_CAN_VALUE			0x03

//Channel State
#define PS_STATE_IDLE			0x0000
#define PS_STATE_STANDBY		0x0001
#define PS_STATE_RUN			0x0002
#define PS_STATE_PAUSE			0x0003
#define PS_STATE_MAINTENANCE	0x0004

#define PS_STATE_PAUSE_CHAMBER_READY	0x0057		//ljb 201012 DEC : 87

//Run sub state(Step Type)
#define PS_STATE_OCV			0x0010
#define PS_STATE_CHARGE			0x0011
#define PS_STATE_DISCHARGE		0x0012
#define PS_STATE_REST			0x0013
#define PS_STATE_IMPEDANCE		0x0014
#define PS_STATE_CHANGING		0x0015
#define PS_STATE_CHECK			0x0016

//PC Part state
#define PS_STATE_LINE_OFF		0x0020
#define PS_STATE_LINE_ON		0x0021
#define PS_STATE_EMERGENCY		0x0022
#define PS_STATE_SELF_TEST		0x0023
#define PS_STATE_FAIL			0x0024
#define PS_STATE_STOP			0x0025
#define PS_STATE_END			0x0026
#define PS_STATE_FAULT			0x0027
#define PS_STATE_NONCELL		0x0028
#define PS_STATE_READY			0x0029
#define PS_STATE_ERROR			0xFFFF

//ljb 2011
#define PS_STATE_PAUSE_CHAMBER_LINK 0x0057		//87
#define PS_STATE_PAUSE_CYCLE_END	0x0058		//88
//////////////////////////////////////////////////////////////////////////

typedef struct floatData
{	
	float x; 
	float y; 
} fltPoint;

//define float point Num
#define PS_VTG_FLOAT	3
#define PS_CRT_FLOAT	3
#define PS_ETC_FLOAT	3
#define PS_VTG_EXP		1000.0
#define PS_CRT_EXP		1000.0
#define PS_ETC_EXP		1000.0

#define LONG2FLOAT(x)		(x == 0 ? 0.0f : float((double)x/PS_VTG_EXP))
//#define FLOAT2LONG(x)		(x == 0.0 ? 0 : long( x < 0 ? ((double)x*PS_VTG_EXP-0.5) : ((double)x*PS_VTG_EXP+0.5)))
//x 값을 double로 변환 하면서 3.1 값이 3.1000002 등으로 변환
// double dValue = _tcstod(strValue,NULL) => string을 더블로 변환 하면 손실 없음

#define FLOAT2LONG(x)		(x == 0.0 ? 0 : long( x < 0 ? ((double)x*PS_VTG_EXP-0.5) : ((double)x*PS_VTG_EXP+0.5)))


#define MAKE2LONG(x)		(x == 0.0 ? 0 : long( x < 0 ? (double)x-0.5 : (double)x+0.5))

#define PCTIME(x)			(x == 0 ? 0.0f : float((double)x/100.0f))			//sec 단위로 변경
#define MDTIME(x)			(x == 0.0f ? 0 : ULONG(MAKE2LONG((double)x*100.0f)))			//10m sec 단위로 변경

#define PS_EMG_NONE				0
#define PS_EMG_AC_FAIL			1
#define PS_EMG_UPS_BAT_FAIL		2
#define PS_EMG_MAIN_EMG_SWITCH	3
#define PS_EMG_SUB_EMG_SWITCH	4
#define PS_EMG_SMPS_FAIL		5
#define PS_EMG_OT_FAIL			6
#define PS_EMG_POWER_SWITCH_RUN	7

//progress ==> Scheudle.mdb의 TestType에 사용됨 
#define PS_PGS_NOWORK		0x00
#define PS_PGS_PREIMPEDANCE	0x01		//PI
#define	PS_PGS_PRECHARGE	0x02		//PC
#define PS_PGS_FORMATION	0x03		//FO
#define PS_PGS_IROCV		0x04		//IR
#define PS_PGS_FINALCHARGE	0x05		//FC
#define PS_PGS_AGING		0x06		//AG
#define	PS_PGS_GRADING		0x07		//GD
#define PS_PGS_SELECTOR		0x08		//SL
#define PS_PGS_OCV			0x09		//OV

#define PS_PGS_PREIMPEDANCE_CODE	"PI"
#define	PS_PGS_PRECHARGE_CODE		"PC"
#define PS_PGS_FORMATION_CODE		"FO"
#define PS_PGS_IROCV_CODE			"IR"
#define PS_PGS_FINALCHARGE_CODE		"FC"
#define PS_PGS_AGING_CODE			"AG"
#define	PS_PGS_GRADING_CODE			"GD"
#define PS_PGS_SELECTOR_CODE		"SL"
#define PS_PGS_OCV_CODE				"OV"

#define PS_REGISTRY_CTSMONPRO_NAME		 "SOFTWARE\\ADP CTSPro\\CTSMonPro"

#define PS_FILE_VERSION_0				0x1000		
#define PS_FILE_VERSION_1				0x1001		//결과데이터 파일 버전 20080801 kjh
#define PS_FILE_VERSION_2				0x1002		//결과데이터 파일 버전 20090408 ljb
#define PS_FILE_VERSION_3				0x1003		//ljb 20101201 Comm State 추가
#define PS_FILE_VERSION_4				0x1004		//ljb 20101222 Output State, Input State 추가
#define PS_FILE_VERSION_5				0x1005		//ljb 20131208 step day, total day, cv time day 추가
#define PS_FILE_VERSION_6				0x1006		//ljb 20170922 칠러연동 버전
#define PS_FILE_VERSION				0x1005		//ljb  v1011 SK 납품버전 4ch->2ch 표시 //power supply 이전과 power supply 버전 
//#define PS_FILE_VERSION_8				0x1006		//v1014 버전
#define PS_FILE_VERSION_9					0x1008		//LGC Poland CWA
#endif	//_ADPOWER_BASE_DEFINE_INCLUDE_H_

#define PS_PATTERN_TIME_ACUMULATE			0x10			//누적시간
#define PS_PATTERN_TIME_OPERATING					0x11			//동작시간
#define PS_PATTERN_TIME_TEMPERATURE					0x12			//동작온도

