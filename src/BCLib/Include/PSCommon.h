///////Power Supply System API Define/////////
//											//	
//	PSCommon.h								//	
//	ADP Cell Test System					//	
//	define of PSCommon.dll					//
//	Byung Hum - Kim		2005.8.10			//
//											//
//////////////////////////////////////////////

#ifndef _PS_COMMON_API_DEFINE_H_
#define _PS_COMMON_API_DEFINE_H_

#include "PowerSys.h"
#include "ChannelCode.h"

//#define PS_DATA_ANAL_PRO_CLASS_NAME		"PNEDataAnalyzerPro"
#define PS_DATA_ANAL_PRO_CLASS_NAME		"CCTSAnalProApp"
#define PS_DATA_REAL_GRAPH_CLASS_NAME	"PNERealGraph"

#define PS_MAX_STATE_COLOR				16	
#define PS_MAX_CELLBAL_COUNT    		32	


//각 상태별로 표시할 색상을 정의 
typedef struct tag_State_Display
{
	BOOL		bStateFlash;
	COLORREF	TStateColor;
	COLORREF	BStateColor;
	char		szMsg[32];
} _PS_STATE_CONFIG;

typedef struct tag_Over_Display
{
	float		fValue;			//0 : Voltage //1: Current
	BOOL		bFlash;
	COLORREF	TOverColor;
	COLORREF	BOverColor;	
} _PS_OVER_CONFIG;

typedef struct tag_Color_Config
{
	BYTE		bShowText;
	BYTE		bShowColor;
	BYTE		bShowOver;

	_PS_STATE_CONFIG	stateConfig[PS_MAX_STATE_COLOR];
	_PS_OVER_CONFIG		VOverConfig;
	_PS_OVER_CONFIG		IOverConfig;

} PS_COLOR_CONFIG;

//파일 저장시 ID Header
typedef struct PS_FILE_ID_HEADER {
	UINT	nFileID;
	UINT	nFileVersion;
	char	szCreateDateTime[64];
	char	szDescrition[128];
	char	szReserved[128];
}	PS_FILE_ID_HEADER, *LPPS_FILE_ID_HEADER;


typedef struct PS_RECORD_FILE_HEADER {
	int		nColumnCount;
	WORD	awColumnItem[PS_MAX_FILE_SAVE_ITEM_NUM];
}  PS_RECORD_FILE_HEADER;

typedef struct PS_RECORD_FILE_HEADER_1011_v2 {
	int		nColumnCount;
	WORD	awColumnItem[PS_MAX_FILE_SAVE_ITEM_NUM_1011_v2];
}  PS_RECORD_FILE_HEADER_1011_v2;

typedef struct PS_RECORD_FILE_HEADER_1011_PRE {
	int		nColumnCount;
	WORD	awColumnItem[PS_MAX_FILE_SAVE_ITEM_NUM_1011PRE];
}  PS_RECORD_FILE_HEADER_1011_PRE;

typedef struct PS_RECORD_FILE_HEADER_100F_READ { //yulee 20190221 PS_RECORD_FILE_HEADER -> PS_RECORD_FILE_HEADER_100F_READ
	int		nColumnCount;
	WORD	awColumnItem[PS_MAX_FILE_SAVE_ITEM_NUM_100F];
}  PS_RECORD_FILE_HEADER_100F_READ;

typedef struct PS_RAW_FILE_HEADER 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_RECORD_FILE_HEADER rsHeader;
} PS_RAW_FILE_HEADER;

typedef struct PS_AUX_DATA_FILE_HEADER {
	int		nColumnCount;
	WORD	awColumnItem[PS_MAX_AUX_SAVE_FILE_COLUMN];
}  PS_AUX_DATA_FILE_HEADER;

typedef struct PS_AUX_FILE_HEADER 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_AUX_DATA_FILE_HEADER  auxHeader;
} PS_AUX_FILE_HEADER;

typedef struct PS_CAN_DATA_FILE_HEADER {
	int		nColumnCount;
	WORD	awColumnItem[PS_MAX_CAN_SAVE_FILE_COLUMN];
}  PS_CAN_DATA_FILE_HEADER;

typedef struct PS_CAN_FILE_HEADER 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_CAN_DATA_FILE_HEADER  canHeader;
} PS_CAN_FILE_HEADER;


typedef struct PS_TEST_FILE_HEADER
{
	char szStartTime[64];
	char szEndTime[64];
	char szSerial[64];
	char szUserID[32];
	char szDescript[128];
	char szTray_CellBcr[128];			//ljb 20160404 szBuff[128]; -> szTray_CellBcr[128]
	int  nRecordSize;
	WORD wRecordItem[PS_MAX_ITEM_NUM_V1015];
} PS_TEST_FILE_HEADER;


typedef struct PS_TEST_FILE_HEADER_v13
{
	char szStartTime[64];
	char szEndTime[64];
	char szSerial[64];
	char szUserID[32];
	char szDescript[128];
	char szTray_CellBcr[128];			//ljb 20160404 szBuff[128]; -> szTray_CellBcr[128]
	int  nRecordSize;
	WORD wRecordItem[PS_MAX_ITEM_NUM_V1013];
} PS_TEST_FILE_HEADER_v13;

typedef struct PS_TEST_RESULT_FILE_HEADER 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_TEST_FILE_HEADER testHeader;
} PS_TEST_RESULT_FILE_HEADER;

typedef struct PS_TEST_RESULT_FILE_HEADER_v13 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_TEST_FILE_HEADER_v13 testHeader;
} PS_TEST_RESULT_FILE_HEADER_v13;

//ljb 201007 add
typedef struct PS_CHAM_FILE_HEADER 
{
	UINT	nFileID;
	UINT	nFileVersion;
	char	szCreateDateTime[64];
	char	szDescrition[128];
	char	szReserved[128];
} PS_CHAM_FILE_HEADER, *LPPS_CHAM_FILE_HEADER;

/*
//Version V1004
typedef struct PS_STEP_END_RECORD_V1004		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG lReserved;

	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			// 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fTemparature;
	float fPressure;
	float fAvgVoltage;
	float fAvgCurrent;
	float fReserved[8];
	
} PS_STEP_END_RECORD_V1004, *LPPS_STEP_END_RECORD_V1004;
*/
//2008/07/24 Version 0x1005 이후
typedef struct PS_STEP_END_RECORD_V8
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;
	
	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG nAccCycleNum;		//ljb v1009
	
	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			// 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fTemparature;
	float fPressure;
	float fAvgVoltage;
	float fAvgCurrent;
	float fChargeAh;
	float fDisChargeAh;
	float fCapacitance;
	float fChargeWh;
	float fDisChargeWh;
	float fCVTime;
	float fSyncDate;
	float fSyncTime;
} PS_STEP_END_RECORD_V8, *LPPS_STEP_END_RECORD_V8;

//2009/04/08 Version 0x1009 
typedef struct PS_STEP_END_RECORD_V10 //power supply X		
{
	BYTE chNo;					// Channel Number 1-start
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chCommState;			//ljb 20100909	Bit LSB(0:정상, 1:이상) 1:Aux 온도 통신 상태, 2: Aux 전압 통신 상태 
	// 3: CAN Master 통신상태, 4: CAN Slave 통신상태, 
	
	BYTE chOutputState;			//ljb 20101222	Bit LSB(0:정상, 1:이상) 1:Key On, 2: Charge On, 3: Relay On 
	BYTE chInputState;			//ljb 사용 안함 : 예약됨
	BYTE cReserved1;
	BYTE cReserved2;			
	
	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG nAccCycleGroupNum1;		//ljb v1009
	ULONG nAccCycleGroupNum2;
	ULONG nAccCycleGroupNum3;
	ULONG nAccCycleGroupNum4;
	ULONG nAccCycleGroupNum5;
	
	ULONG nMultiCycleGroupNum1;
	ULONG nMultiCycleGroupNum2;
	ULONG nMultiCycleGroupNum3;
	ULONG nMultiCycleGroupNum4;
	ULONG nMultiCycleGroupNum5;
	ULONG lSyncDate;				//ljb 20140116 add
	
	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			//ljb 20131202 add 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fOvenTemperature;			//ljb 20100722	Chamber Temparature
	float fOvenHumidity;			//ljb 20100722	fAuxVoltage -> Chamber Humidity
	float fAvgVoltage;
	float fAvgCurrent;
	float fChargeAh;
	float fDisChargeAh;
	float fCapacitance;
	float fChargeWh;
	float fDisChargeWh;
	float fCVTime;
	//float fSyncDate;
	float fSyncTime;
	float fVoltageInput;			//ljb 201011
	float fVoltagePower;			//ljb 201011
	float fVoltageBus;				//ljb 201011
	float fStepTime_Day;			//ljb 20131202 add  이번 Step 진행 시간
	float fTotalTime_Day;			//ljb 20131202 add  시험 Total 진행 시간
	float fCVTime_Day;				//ljb 20131202 add  이번 Step 진행 시간
	float fLoadVoltage;				//ljb 20150427 add
	float fLoadCurrent;				//ljb 20150427 add
	
	float  fChillerRefTemp;		    //cny 181106
	float  fChillerCurTemp;		    //cny 181106
	int    iChillerRefPump;		    //cny 181106
	int    iChillerCurPump;		    //cny 181106
	double dCellBALChData;          //cny 181106
	
} PS_STEP_END_RECORD_V10, *LPPS_STEP_END_RECORD_V10;

//20190707 Version 0x1011
typedef struct PS_STEP_END_RECORD_V11 //power supply O			
{
	BYTE chNo;					// Channel Number 1-start
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chCommState;			//ljb 20100909	Bit LSB(0:정상, 1:이상) 1:Aux 온도 통신 상태, 2: Aux 전압 통신 상태 
	// 3: CAN Master 통신상태, 4: CAN Slave 통신상태, 

	BYTE chOutputState;			//ljb 20101222	Bit LSB(0:정상, 1:이상) 1:Key On, 2: Charge On, 3: Relay On 
	BYTE chInputState;			//ljb 사용 안함 : 예약됨
	BYTE cReserved1;
	BYTE cReserved2;			

	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG nAccCycleGroupNum1;		//ljb v1009
	ULONG nAccCycleGroupNum2;
	ULONG nAccCycleGroupNum3;
	ULONG nAccCycleGroupNum4;
	ULONG nAccCycleGroupNum5;

	ULONG nMultiCycleGroupNum1;
	ULONG nMultiCycleGroupNum2;
	ULONG nMultiCycleGroupNum3;
	ULONG nMultiCycleGroupNum4;
	ULONG nMultiCycleGroupNum5;
	ULONG lSyncDate;				//ljb 20140116 add

	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			//ljb 20131202 add 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fOvenTemperature;			//ljb 20100722	Chamber Temparature
	float fOvenHumidity;			//ljb 20100722	fAuxVoltage -> Chamber Humidity
	float fAvgVoltage;
	float fAvgCurrent;
	float fChargeAh;
	float fDisChargeAh;
	float fCapacitance;
	float fChargeWh;
	float fDisChargeWh;
	float fCVTime;
	//float fSyncDate;
	float fSyncTime;
	float fVoltageInput;			//ljb 201011
	float fVoltagePower;			//ljb 201011
	float fVoltageBus;				//ljb 201011
	float fStepTime_Day;			//ljb 20131202 add  이번 Step 진행 시간
	float fTotalTime_Day;			//ljb 20131202 add  시험 Total 진행 시간
	float fCVTime_Day;				//ljb 20131202 add  이번 Step 진행 시간
	float fLoadVoltage;				//ljb 20150427 add
	float fLoadCurrent;				//ljb 20150427 add

	float  fChillerRefTemp;		    //cny 181106
	float  fChillerCurTemp;		    //cny 181106
	int    iChillerRefPump;		    //cny 181106
	int    iChillerCurPump;		    //cny 181106
	double dCellBALChData;          //cny 181106

	float	fPwrSetVoltage;		//cny 181106//yulee 20190514 cny work transfering //yulee 20190514_1//yulee 20190520
	float	fPwrCurVoltage;		//cny 181106

} PS_STEP_END_RECORD_V11, *LPPS_STEP_END_RECORD_V11;

//20190707 Version 0x1011 LGC CWA  V11과 동일
typedef struct PS_STEP_END_RECORD		
{
	BYTE chNo;					// Channel Number 1-start
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chCommState;			//ljb 20100909	Bit LSB(0:정상, 1:이상) 1:Aux 온도 통신 상태, 2: Aux 전압 통신 상태 
								// 3: CAN Master 통신상태, 4: CAN Slave 통신상태, 
	
	BYTE chOutputState;			//ljb 20101222	Bit LSB(0:정상, 1:이상) 1:Key On, 2: Charge On, 3: Relay On 
	BYTE chInputState;			//ljb 사용 안함 : 예약됨
	BYTE cReserved1;
	BYTE cReserved2;			

	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG nAccCycleGroupNum1;		//ljb v1009
	ULONG nAccCycleGroupNum2;
	ULONG nAccCycleGroupNum3;
	ULONG nAccCycleGroupNum4;
	ULONG nAccCycleGroupNum5;
	
	ULONG nMultiCycleGroupNum1;
	ULONG nMultiCycleGroupNum2;
	ULONG nMultiCycleGroupNum3;
	ULONG nMultiCycleGroupNum4;
	ULONG nMultiCycleGroupNum5;
	ULONG lSyncDate;				//ljb 20140116 add

	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			//ljb 20131202 add 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fOvenTemperature;			//ljb 20100722	Chamber Temparature
	float fOvenHumidity;			//ljb 20100722	fAuxVoltage -> Chamber Humidity
	float fAvgVoltage;
	float fAvgCurrent;
	float fChargeAh;
	float fDisChargeAh;
	float fCapacitance;
	float fChargeWh;
	float fDisChargeWh;
	float fCVTime;
	//float fSyncDate;
	float fSyncTime;
	float fVoltageInput;			//ljb 201011
	float fVoltagePower;			//ljb 201011
	float fVoltageBus;				//ljb 201011
	float fStepTime_Day;			//ljb 20131202 add  이번 Step 진행 시간
	float fTotalTime_Day;			//ljb 20131202 add  시험 Total 진행 시간
	float fCVTime_Day;				//ljb 20131202 add  이번 Step 진행 시간
	float fLoadVoltage;				//ljb 20150427 add
	float fLoadCurrent;				//ljb 20150427 add

	float  fChillerRefTemp;		    //cny 181106
	float  fChillerCurTemp;		    //cny 181106
// 	int    iChillerRefPump;		    //cny 181106
// 	int    iChillerCurPump;		    //cny 181106
	float    fChillerRefPump;		    //lyj 20201102 DES칠러 소숫점 
	float    fChillerCurPump;		    //lyj 20201102 DES칠러 소숫점 
	double dCellBALChData;          //cny 181106
		
	float	fPwrSetVoltage;		//cny 181106//yulee 20190514 cny work transfering //yulee 20190514_1//yulee 20190520
	float	fPwrCurVoltage;		//cny 181106

} PS_STEP_END_RECORD, *LPPS_STEP_END_RECORD;


//ksj 20201013
//기존 STEP_END_RECORD 구조체에 정보가 제한적이라 수정이 필요하나,
//파일에 저장할때도 사용하는 구조체라 함부로 바꾸기 부담스러우므로
//기존 정보를 포함하고, 추가 정보를 포함하는 구조체 추가
typedef struct PS_STEP_END_RECORD_EXTRA		
{
	ULONG ulStepEndRecordVersion;	
	long reserved1[3];
	long reserved2[10];

	long lSyncDate;
	long lSyncTime;
	long reserved3[38];
	//이하 기존 정보
	PS_STEP_END_RECORD stEndRecord;

} PS_STEP_END_RECORD_EXTRA, *LPPS_STEP_END_RECORD_EXTRA;


typedef struct PS_STEP_END_RECORD_DATA_V8	
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	ULONG lReserved;

	float fData[PS_MAX_ITEM_NUM_V1004];
} PS_STEP_END_RECORD_DATA_V8, *LPPS_STEP_END_RECORD_DATA_V8;

typedef struct PS_STEP_END_RECORD_DATA_V9		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;
	
	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	
	ULONG lAccCycleNum1;
	ULONG lAccCycleNum2;
	ULONG lAccCycleNum3;
	ULONG lAccCycleNum4;
	ULONG lAccCycleNum5;

	ULONG lMultiCycleNum1;
	ULONG lMultiCycleNum2;
	ULONG lMultiCycleNum3;
	ULONG lMultiCycleNum4;
	ULONG lMultiCycleNum5;
	
	float fData[PS_MAX_ITEM_NUM_V1009];
} PS_STEP_END_RECORD_DATA_V9, *LPPS_STEP_END_RECORD_DATA_V9;

typedef struct PS_STEP_END_RECORD_DATA_V100B		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	BYTE chOutputState;			//ljb 20101222	Bit LSB(0:정상, 1:이상) 1:Key On, 2: Charge On, 3: Relay On 
	BYTE chInputState;			//ljb 사용 안함 : 예약됨
	BYTE cReserved1;
	BYTE cReserved2;			
	
	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	
	ULONG lAccCycleNum1;
	ULONG lAccCycleNum2;
	ULONG lAccCycleNum3;
	ULONG lAccCycleNum4;
	ULONG lAccCycleNum5;
	
	ULONG lMultiCycleNum1;
	ULONG lMultiCycleNum2;
	ULONG lMultiCycleNum3;
	ULONG lMultiCycleNum4;
	ULONG lMultiCycleNum5;
	
	float fData[PS_MAX_ITEM_NUM_V100B];
} PS_STEP_END_RECORD_DATA_V100B, *LPPS_STEP_END_RECORD_DATA_V100B;

typedef struct PS_STEP_END_RECORD_DATA_V100C		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;
	
	BYTE chOutputState;			//ljb 20101222	Bit LSB(0:정상, 1:이상) 1:Key On, 2: Charge On, 3: Relay On 
	BYTE chInputState;			//ljb 사용 안함 : 예약됨
	BYTE cReserved1;
	BYTE cReserved2;			
	
	ULONG nIndexFrom;
	ULONG nIndexTo;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	
	ULONG lAccCycleNum1;
	ULONG lAccCycleNum2;
	ULONG lAccCycleNum3;
	ULONG lAccCycleNum4;
	ULONG lAccCycleNum5;
	
	ULONG lMultiCycleNum1;
	ULONG lMultiCycleNum2;
	ULONG lMultiCycleNum3;
	ULONG lMultiCycleNum4;
	ULONG lMultiCycleNum5;

	ULONG lSyncDate;	//ljb 20140128 add
	
	float fData[PS_MAX_ITEM_NUM_V100B];
} PS_STEP_END_RECORD_DATA_V100C, *LPPS_STEP_END_RECORD_DATA_V100C;

#define PS_ADP_RECORD_FILE_ID		5640
#define PS_ADP_STEP_RESULT_FILE_ID_OLD	20100715
#define PS_ADP_STEP_RESULT_FILE_ID		20100914		//ljb Comm State 추가

#define PS_RESULT_FILE_NAME_EXT		"cts"
#define PS_RAW_FILE_NAME_EXT		"cyc"
#define PS_SCHEDULE_FILE_NAME_EXT	"sch"		//20100430

//2007/08/09 kjh 추가
#define PS_AUX_FILE_NAME_EXT		"aux"
#define PS_AUX_RESULT_FILE_NAME_EXT	"ats"
#define PS_PNE_AUX_FILE_ID			20100715
#define PS_PNE_AUX_RESULT_FILE_ID	20100716
#define PS_MAX_AUX_INFO_ITEM		520
#define PS_MAX_AUX_FILE_COUNT		4
#define	_PNE_AUX_FILE_VER_V1		0x1001		//20080104 kjh
#define	_PNE_AUX_FILE_VER			0x1002		//ljb 201007

//2007/10/17 kjh 추가
#define PS_CAN_FILE_NAME_EXT		"can"
#define PS_CAN_FILE_NAME_EXT2		"can001"
#define PS_CAN_RESULT_FILE_NAME_EXT	"ctc"
#define PS_PNE_CAN_FILE_ID			20100715
#define PS_PNE_CAN_RESULT_FILE_ID	20100716
#define PS_MAX_CAN_INFO_ITEM		520
#define PS_MAX_CAN_FILE_COUNT		4

#define PS_CHAM_FILE_NAME_EXT		 "chm"
#define PS_CHAM_RESULT_FILE_NAME_EXT "ctm"

//ljb 2010-06-22 Anal에서 사용하는 AUX 정보
typedef struct PS_AUX_DISP_INFO
{
	short int	chNo;				//Aux Owner Channel No
	short int	auxType;			//Temp = 0, Voltage = 1;
	short int	auxDataType;		//센서 Type
	short int   nReserved;
	char		auxName[128];
} PS_AUX_DISP_INFO, *LPPS_AUX_DISP_INFO;

typedef struct PS_AUX_INFO
{
	short int	chNo;			//Aux Owner Channel No
	short int	chAuxNo;		//Aux Channel No
	short int	chType;			//Voltage or Temperature or CAN
	short int	chTempTableType;//20160428 add
	float		fValue;
	long		reserved;	
} PS_AUX_INFO, *LPPS_AUX_INFO;

typedef struct PS_AUX_RAW_DATA		
{
	//short int auxCount;				// Run, Stop(Manual, Error), End //DAQ512 수정 포인트
	//BYTE reserved1[2];												//DAQ512 수정 포인트
	BYTE auxCount;				// Run, Stop(Manual, Error), End
	BYTE reserved1[3];
	PS_AUX_INFO auxData[PS_MAX_AUX_INFO_ITEM];
} PS_AUX_RAW_DATA, *LPPS_AUX_RAW_DATA;

typedef struct PS_CAN_TITLE
{
	short int canType;
	short int dataType;
}PS_CAN_TITLE_INFO;

typedef union _uniCANVALUE{
	long	lVal[2];
	float	fVal[2];
	char	strVal[8];
}CAN_VALUE;

//Anal에서 사용하는 CAN 정보
typedef struct PS_CAN_DISP_INFO
{
	short int	chNo;				//Aux Owner Channel No
	short int	canType;			//Master = 1, Slave = 2;
	short int	canDataType;
	short int   nReserved;
	char		canName[128];
} PS_CAN_DISP_INFO, *LPPS_CAN_DISP_INFO;

//장비로 부터 저장되는 CAN 정보
typedef struct PS_CAN_INFO
{
	short int	chNo;			//Aux Owner Channel No
	short int	canType;			//Master = 1, Slave = 2;
	CAN_VALUE	canVal;
} PS_CAN_INFO, *LPPS_CAN_INFO;

typedef struct PS_CAN_RAW_DATA		
{
	// 	BYTE canCount;				
	// 	BYTE reserved1[3];	
	short int canCount;
	BYTE reserved1[2];			//ljb 20131217
	PS_CAN_INFO canData[PS_MAX_CAN_INFO_ITEM];
} PS_CAN_RAW_DATA, *LPPS_CAN_RAW_DATA;

//장비로 부터 저장되는 CAMBER 정보
typedef struct PS_OVEN_RAW_DATA
{
	//[0] : PV온도, [1] : SV온도, [2] : PV습도, [3] : SV습도
	float	fRefTemper;
	float	fCurTemper;
	float	fRefHumidity;
	float	fCurHumidity;

// 	int     iRefPump;
// 	int     iCurPump;
	float    fRefPump; //lyj 20201102 DES칠러 펌프 소숫점
	float    fCurPump; //lyj 20201102 DES칠러 펌프 소숫점

} PS_OVEN_RAW_DATA, *LPPS_OVEN_RAW_DATA;

typedef struct PS_CELLBAL_RAW_DATA
{
	char     ch_state[PS_MAX_CELLBAL_COUNT];

} PS_CELLBAL_RAW_DATA, *LPPS_CELLBAL_RAW_DATA;

typedef struct PS_PWRSUPPLY_RAW_DATA
{
	float	fSetVoltage;
	float	fCurVoltage;
	
} PS_PWRSUPPLY_RAW_DATA, *LPPS_PWRSUPPLY_RAW_DATA;

//장비로 부터 저장되는 DC LOAD 정보
typedef struct PS_LOAD_RAW_DATA
{
	float	fVoltage;
	float	fCurrent;
	float	fSetResis;
	float	fSetPower;
} PS_LOAD_RAW_DATA, *LPPS_LOAD_RAW_DATA;

#ifdef __cplusepluse
extern "C" {
#endif ///__cplusepluse

#ifndef __PS_COMMON_DLL__
#define __PS_COMMON_DLL__

#ifdef PS_COMMON_EXPORT_DLL_API
#define PS_DLL_API	__declspec(dllexport)
#else	//EXPORTSFT_DLL_API
#define PS_DLL_API	__declspec(dllimport)
#endif	//EXPORTSFT_DLL_API

#endif	//__SFT_FORM_DLL__

PS_DLL_API	void PSCellCodeMsg(BYTE code, CString &strMsg, CString &strDescript);

//Cell 양부 검사 함수
PS_DLL_API BOOL PSIsCellOk(BYTE code);
PS_DLL_API BOOL PSIsCellBad(BYTE code);

PS_DLL_API BOOL PSIsNonCell(BYTE code);
PS_DLL_API BOOL PSIsSysFail(BYTE code);
PS_DLL_API BOOL PSIsCellCheckFail(BYTE code);

//color 설정 함수 
PS_DLL_API PS_COLOR_CONFIG*  PSGetColorCfgData(BOOL bShowEditDlg = FALSE);
PS_DLL_API PS_COLOR_CONFIG*  PSGetColorCfgData2(); //ksj 20200202


//표기관련 
PS_DLL_API LPCTSTR PSGetTypeMsg(WORD type);
PS_DLL_API LPCTSTR PSGetStateMsg(WORD State, WORD type = PS_STEP_NONE);
PS_DLL_API void PSGetStateColor(WORD state, WORD type, COLORREF &textColor, COLORREF &bkColor);
PS_DLL_API void PSGetTypeColor(WORD type, COLORREF &textColor, COLORREF &bkColor);

//PS_DLL_API LPCTSTR PSGetItemName(int nCode);
PS_DLL_API LPSTR PSGetItemName(int nCode);

//dll inner ftn
BOOL	WriteColorCfgData();


#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif //_PS_COMMON_API_DEFINE_H_