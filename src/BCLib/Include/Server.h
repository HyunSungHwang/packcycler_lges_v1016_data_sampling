///////SOCKET_SERVER_DLL_API_HEADER_FILE///////
//											//	
//	Server.h								//	
//	Softech Charge/Discharge System API		//	
//  Define of Network Data
//	Byung Hum - Kim		2005.5.24			//
//											//
//////////////////////////////////////////////

#ifndef _SFT_SCHEDULER_MSG_DEFINE_H_
#define _SFT_SCHEDULER_MSG_DEFINE_H_


//Version Define
#define _SFT_PROTOCOL_VERSION_ELSE		0x0000		//Version Check -> else 
#define _SFT_PROTOCOL_VERSIONV1			0x1000		//Message Protocol Version
#define _SFT_PROTOCOL_VERSIONV2			0x1001		//SFT_CH_DATA에서 	//unsigned short int	nTotalCycleNum;	
																		//unsigned short int	nCurrentCycleNum;	
													//을 unsinged long으로 변경함 
#define _SFT_PROTOCOL_VERSIONV3			0x1002		//SFT_CMD_SAFETY_DATA command 추가	2006/3/24
#define _SFT_PROTOCOL_VERSION4			0x1003		//2006/6/8
#define _SFT_PROTOCOL_VERSION5			0x1004		//2007/5/18 병렬 모드/AUX Data /User Pattern 추가

//1005 SBC와 PC 시간 동기화/ Loop 누적 반복횟수/용량,전력 누적량/시각데이터 추가
#define _SFT_PROTOCOL_VERSION6			0x1005		//2008/07/23 

//Can TRAC, RECV factor_offset 추가 
#define _SFT_PROTOCOL_VERSION7			0x1006		//2008/11/12 

//Can Trans, receive 구조 변경 function_division 사용 
#define _SFT_PROTOCOL_VERSION8			0x1007		//2009/02/02 

//Step End 조건에서 BMS, Aux 온도 전압 에 의한 goto 추가  
#define _SFT_PROTOCOL_VERSION9			0x1008		//2009/03/04 

//Step End 조건에서 Loop in Loop , Multi GRADE , 누적 용량 , 누적 WattHour 추가  
#define _SFT_PROTOCOL_VERSION10			0x1009		//2009/03/26 

//Socket 분리 Mornitoring Data and Save Data 분리  
#define _SFT_PROTOCOL_VERSION_A			0x100A		//2010/08/16 

//Socket 분리 Mornitoring Data and Save Data 분리  
#define _SFT_PROTOCOL_VERSION_B			0x100B		//2010/12/08 (통신상태,전압 VInput, VPower, VBus 추가) 

//Socket 분리 Mornitoring Data and Save Data 분리  
#define _SFT_PROTOCOL_VERSION_C			0x100C		//2013/12/13 (CAN 설정 프로토콜 변경(동일아이디 수신가능), 스텝시간-총시간-CV시간(Day, Time 분리)  

#define _SFT_PROTOCOL_VERSION_D			0x100D

//#define _SFT_PROTOCOL_VERSION_E		0x100E		//SBC -> PC 데이터 저장 방식 변경.

#define _SFT_PROTOCOL_VERSION_F			0x100F		//20160427 Wait Time Goto 기능 추가, 온도(Aux 기능 추가)
#define _SFT_PROTOCOL_VERSION_1010		0x1010		//20170626 르크랑쉬 병렬
#define _SFT_PROTOCOL_VERSION_1011		0x1011		//20170626 4채널을 2채널로 벼시, 병렬기능 추가 수정
#define _SFT_PROTOCOL_VERSION_1013		0x1013		//20181119 스케쥴 공통 안전조건, CAN /LIN 플래그 추가
#define _SFT_PROTOCOL_VERSION_1014		0x1014		//20190719 CAN /LIN, RS232 추가, CELL 밸런스 수정, chiller 편차 수정

#define _SFT_PROTOCOL_VERSION_1015		0x1015		
#define _SFT_PROTOCOL_VERSION			0x1016		//ksj 20200121 : Aux Vent 안전기능 추가		
//#define _SFT_PROTOCOL_VERSION		0x1013		//20181119 스케쥴 공통 안전조건, CAN /LIN 플래그 추가


//Formation TCPIP Port
#define _SFT_FROM_SCHEDULER_TCPIP_PORT1	2001
#define _SFT_FROM_SCHEDULER_TCPIP_PORT2	2002
#define _SFT_FROM_SCHEDULER_TCPIP_PORT3	2003
#define _SFT_FROM_SCHEDULER_TCPIP_PORT4	2004
#define _SFT_FROM_SCHEDULER_TCPIP_PORT5	2005

//IROCV TCPIP Port
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT1	2051
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT2	2052
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT3	2053
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT4	2054
#define _SFT_IROCV_SCHEDULER_TCPIP_PORT5	2055

//System Max Define 
//#define _SFT_MAX_PACKET_LENGTH		256000		//최대 Packet 길이 
#define _SFT_MAX_PACKET_LENGTH		2000000		//최대 Packet 길이 
#define _SFT_MAX_PACKET_GRADE_STEP	10
#define _SFT_MAX_PACKET_COMP_POINT	3
#define _SFT_MAX_PACKET_SUB_STEP	10
#define _SFT_MAX_CAN_AUX_COMPARE_STEP	10
#define _SFT_MAX_PACKET_GRADE_ITEM_IN_STEP	2		//ljb 2009-03-26	4 -> 2;
#define _SFT_MAX_SIMULATION_LENGTH	30000

#if _SFT_MAX_PACKET_GRADE_STEP < PS_MAX_GRADE_STEP
#pragma message( "------------KBH Message-------------" )
#pragma message( "Max Grade Step define Error" )
#pragma message( "------------------------------------" )
#error Grade step Define Error
#endif

#if _SFT_MAX_PACKET_COMP_POINT < PS_MAX_COMP_POINT
#pragma message( "------------KBH Message-------------" )
#pragma message( "Max comperation point define Error" )
#pragma message( "------------------------------------" )
#error Comp. point Define Error
#endif

#if _SFT_MAX_PACKET_SUB_STEP < PS_MAX_SUB_STEP
#pragma message( "------------KBH Message-------------" )
#pragma message( "Maximum sub step define Error" )
#pragma message( "------------------------------------" )
#error Max sub step Define Error
#endif

//NetWork RX/TX Buffer Size
#define _SFT_TX_BUF_LEN				_SFT_MAX_PACKET_LENGTH
#define _SFT_RX_BUF_LEN				_SFT_MAX_PACKET_LENGTH
//#define _SFT_SYNC_WRITE_LEN			0x10000		//Packet 길이를 64K 나눠서 전송함 2008/01/10 kjh
#define _SFT_SYNC_WRITE_LEN			0x10000		//Packet 길이를 64K 나눠서 전송함 2008/01/10 kjh


//Time define
//#define SFT_MSG_TIMEOUT				2000		//Message Read Time Out 2014.08.12 테스트를 위해 10초로 변경. //yulee 20190911 2초로 다시 변경
/*#ifdef _DEBUG
#define SFT_MSG_TIMEOUT				5000		//ksj 20200202 : debug mode timeout
#define SFT_MSG_TIMEOUT_SCHEDULE_END	60000		//ksj 20200202 : debug mode timeout
#define SFT_MSG_TIMEOUT_CONNECTION_SEQ	10000		//ksj 20201209 : 커넥션 시퀀스는 타임아웃 넉넉하게 줘본다.
#else*/
//#define SFT_MSG_TIMEOUT				300		//yulee 20191016 DAQ AUXV 256 _TIMEOUT
//#define SFT_MSG_TIMEOUT_SCHEDULE_END	1500		//yulee 20191120 패턴 5만 줄에 관하여 SCHEDULE_END CMD의 TIMEOUT 시간은1.5초
#define SFT_MSG_TIMEOUT					10000		//ksj 20200206 : Timeout 을 왜 이렇게 타이트하게 줬는지 모르겠다 300ms -> 1초로 늘려봄. //ksj 20200702 : 2.5초로 늘림 //ksj 20201118 : 5초로 다시 늘림. 진천KCL Aux설정이 3~4초 정도 걸림;;;
#define SFT_MSG_TIMEOUT_SCHEDULE_END	100000		//ksj 20200206 : Timeout 을 왜 이렇게 타이트하게 줬는지 모르겠다 1500ms -> 5초로 늘려봄. //lyj 20201217 5->100
#define SFT_MSG_TIMEOUT_CONNECTION_SEQ	10000		//ksj 20201209 : 커넥션 시퀀스는 타임아웃 넉넉하게 줘본다.
//#endif

#define SFT_HEART_BEAT_INTERVAL		3000		//Heart beat check interval
//#define SFT_HEART_BEAT_INTERVAL		300000		//ljb 20130529 5분으로 변경 Heart beat check interval

#define SFT_HEART_BEAT_TIMEOUT		(SFT_HEART_BEAT_INTERVAL+5000)		//heart beat time out
//#define _SFT_MAX_WAIT_COUNT			2
#define _SFT_MAX_WAIT_COUNT			3		//lyj 20201217
//#define _SFT_MAX_WAIT_COUNT_SEND_SCHEDULE	7 //ksj 20201215 : 작업s 시작 중에는 타임아웃 처리 넉넉하게 준다 (8초*8회 = 64초) SFT_HEART_BEAT_TIMEOUT*_SFT_MAX_WAIT_COUNT_WORK_START
#define _SFT_MAX_WAIT_COUNT_SEND_SCHEDULE	10 //lyj 20201217

//Data Size Define
#define _SFT_DATE_TIME_LENGTH			32
//2007/10/16 128 => 512 로 변경
#define _SFT_MAX_MAPPING_AUX			512 	//Channel 당 최대 AUX 갯수 (온도(256) + 전압(256))
//#define _SFT_MAX_MAPPING_AUX			1024 	//ljb 20180920 DAQ volt만 512개로 확장 됨. //DAQ512 수정 포인트 //lyj 20200811 HLGP
#define _SFT_MAX_MAPPING_CAN			512		//Channel 당 최대 CAN 갯수 (Master(256) + Slave(256))
#define _SFT_MAX_MAPPING_AUX_A			128		//하위버전 호환용
#define _SFT_MAX_PARALLEL_CHANNEL_2		2			//병렬 인자 수 
#define _SFT_MAX_PARALLEL_CHANNEL_4		4			//병렬 인자 수 
#define _SFT_MAX_PARALLEL_COUNT			8			//병렬 가능한 최대 Channel 수
#define _SFT_MAX_INSTALL_CH_COUNT		8		//Max Channel 개수//

// SFT_CMD_CH_DATA 의 data 종류 
#define	SFT_SAVE_REPORT			0x00		//Monitoring용 Data
#define	SFT_SAVE_STEP_END		0x01		//Step End 저장용 data
#define SFT_SAVE_DELTA_TIME		0x02
#define SFT_SAVE_DELTA_V		0x03
#define SFT_SAVE_DELTA_I		0x04
#define SFT_SAVE_DELTA_T		0x05
#define SFT_SAVE_DELTA_P		0x06
#define SFT_SAVE_ETC			0x07

//AUX Type
#define SFT_AUX_TEMPERATURE		0
#define SFT_AUX_VOLTAGE			1
#define SFT_AUX_CAN				2

//////////////////////////////////////////////////////////////////////////
//Message structure Define
//////////////////////////////////////////////////////////////////////////

// SFT Server Message Header
typedef struct s_CMD_HEADER
{	
	unsigned long	nCommand;			// Packet Command 종류	(예 : 이 패킷은 컨트롤 패킷이다)
	unsigned long	lCmdSerial;			// Command Serial Number..(Command ID)
	unsigned short	wCmdType;			// 명령별 별도 정의
	unsigned short 	wNum;				// 이 Command에 해당 사항이 있는 채널의 수
	unsigned long	lChSelFlag[2];		// 이 시험조건이 전송될 Channel (Bit 당 Channel)
	unsigned long	nLength;			// Size of Body
} SFT_MSG_HEADER, *LPSFT_MSG_HEADER;


//Response Body
typedef struct s_SFT_CMD_RESPONSE{				//Command Response (NetWork St.)
	unsigned long nCmd;
	unsigned long nCode;
} SFT_RESPONSE, *LPSFT_RESPONSE;
//Response Packet

//Network Structure
typedef struct s_CommandResponse {
	SFT_MSG_HEADER		msgHeader;
	SFT_RESPONSE		msgBody;
} SFT_PACKET_RESPONSE, *LPSFT_PACKET_RESPONSE;

// SFTScheduler Group Information Data
// Network Structure
typedef struct s_SFT_CMD_INFO_DATA {
	unsigned int		nModuleID;							//Module에서는 GroupID, Host에서는 ModuleID
	unsigned int		nSystemType;						//Formation/IROCV/Aging/Grader/Selector
	unsigned int		nProtocolVersion;					//unsigned int :Protocol Version 
	char				szModelName[128];					//Module Model Name
	unsigned int		nOSVersion;							//unsigned int :System Version 
	unsigned short int	wVoltageRange;						//전압 Range 수
	unsigned short int	wCurrentRange;						//전류 Range 수
	unsigned int		nVoltageSpec[5];					//전압 Range 별 Spec
	unsigned int		nCurrentSpec[5];					//전류 Range 별 Spec
	unsigned char		byCanCommType;						//ljb  20180326 0: CAN 2.0, 1: CAN FD
	unsigned char		byTypeData[7];						//Module Location, Line No, Module Type, Etc...	
	unsigned short int	wInstalledBoard;					//unsigned short : Total Board Number in one Module
	unsigned short int	wChannelPerBoard;					//unsinged short : Total Channel Per Board
	unsigned int		nInstalledChCount;					// Installed channel in module
	unsigned int		nTotalJigNo;						//
	unsigned int		awBdInJig[16];						//Board in Each Jig
	unsigned int		reserved[4];
} SFT_MD_SYSTEM_DATA, *LPSFT_MD_SYSTEM_DATA;			

// SFTScheduler Group Information Data
// Network Structure
typedef struct s_SFT_CMD_MD_SET_DATA {
	unsigned char	bConnectionReTry;		//Module Connection retry
	unsigned char	bLineMode;				// N/A
	unsigned char	bControlMode;			// N/A
	unsigned char	bWorkingMode;			// N/A
	unsigned int	nAutoReportInterval;	//Auto Report Data Time Interval	ms
	unsigned int	nDataSaveInterval;		//Data Save Interval	ms
	unsigned int	nReserved[16];			//
} SFT_MD_SET_DATA;				

// SFTScheduler Group Monitoring Data 
typedef struct s_SFT_CMD_STATE_DATA {
	WORD	state;
	BYTE	jigState;				//    
	BYTE	trayState;				//  
	BYTE	doorState;				//  
	BYTE	failCode; 
	WORD	reserved1;				// 총 시간   
 	ULONG	sensorState;			//Bit Data -> Smoke, Fan, door, tray, 기타 
} SFT_MD_STATE_DATA, *LPSFT_MD_STATE_DATA;			

//SFTScheduler Step Header
typedef struct s_StepHeader {			//Step Data Header Structure
	INT			nStepTypeID;			//공정의 ID
	BYTE		stepNo;					//진행 Step 번호(1 Base)
	BYTE		mode;					// CC/CV, CP , CR ...
	BYTE		bTestEnd;
	BYTE		nSubStep;				//Formation 에서 사용
//	BYTE		reserved[4];
	BYTE		reserved;				//bUseSocFlag;			//Flag of soc rate end condition	-> 20090326 ljb 삭제
	BYTE		bUseCyclePause;			//ljb Cycle 반복횟수 후 Pause ( 0 : 사용안함, 1:사용)
	BYTE		reserved2[2];
} SFT_STEP_HEADER, *LPSFT_STEP_HEADER;	

//SFTScheduler Step Referance
//Protocol Ver 0x1005 이후 
typedef struct s_RefData_v1009 {
	long		lVref;					//Voltage Referance
    long		lIref;					//Current Referance
	BYTE		byRange;				// 0:Auto, 1: High 2: Middle 3: Low, 4: Micro	// v1009 : Cycle Step 일 경우 누적 용량 부호 (0:충방전, 1: 충전, 2:방전)
	BYTE		bValueStepNo;			//Soc Actual capa step no						// v1009 : Cycle Step 일 경우 누적 전력량 부호 (0:충방전, 1: 충전, 2:방전)
	unsigned short int wValueRate;		//Capa SOC Rate

	ULONG		ulEndTime;				//End time
    long		lEndV_H;				//End Voltage High
    long		lEndI;					//End Current			
    long		lEndC;					//End Capacity			// v1009 : Cycle Step 일 경우 누적 용량
	long		lGoto;					//Cycle goto step   	// v1009 : (Step 일 경우 Goto -> 전압 분기 High)	(LOOP 일 경우 Cycle 반복 완료 후 Goto Step) 
	long		lCycleCount;			//Cycle count			// v1009 : (Step 일 경우 Goto -> 전압 분기 Low)		(LOOP 일 경우 Cycle 반복 횟수)    
    long		lDeltaV;				//DeltaV (Vpeak - Delta V)													    
	
	long		lEndWatt;				//End Watt
	long		lEndWattHour;			//End WattHour			//v1009 : Cycle Step 일 경우 누적 전력량
	long		lAccCycleCount;			//누적 Loop Count		//v1009 : (Step 일 경우 Goto -> 종료시간 분기)		(LOOP 일 경우 누적 Cycle 반복 횟수)    
	long		lAccCycleGroupID;		//누적 Loop GroupID		//v1009 : (Step 일 경우 Goto -> CV시간 종료 분기)	(LOOP 일 경우 누적 Cycle 할 그룹 ID)    
	ULONG		ulCVTime;				//CV Time

	//20090304 ljb add -> bms 10byte + aux 온도, 전압 8byte + reserved 2byte -> 20090326 ljb 수정 
	long		lAuxEndV_H;			//Aux 전압 High 값
    long		lAuxEndV_L;					
	long		lAuxEndTemp_H;				
    long		lAuxEndTemp_L;

	BYTE		bAuxEndV_H_Goto;					
    BYTE		bAuxEndV_L_Goto;					
	BYTE		bAuxEndTemp_H_Goto;	
    BYTE		bAuxEndTemp_L_Goto;	
				
	long		lBmsEndVtg_H;					
    long		lBmsEndVtg_L;					
	long		lBmsEndTemp_H;					
    long		lBmsEndTemp_L;					
	long		lBmsEndSoc_H;					
    long		lBmsEndSoc_L;					
	long		lBmsEndFlt_H;					

	BYTE		bBmsEndVtg_H_Goto;					
    BYTE		bBmsEndVtg_L_Goto;					
	BYTE		bBmsEndTemp_H_Goto;					
    BYTE		bBmsEndTemp_L_Goto;					
	BYTE		bBmsEndSoc_H_Goto;					
    BYTE		bBmsEndSoc_L_Goto;					
	BYTE		bBmsEndFlt_H_Goto;					
	BYTE		bValueItem;				//v1009 : ( 0 : 사용안함 , 1 : SOC , 2 : WattHour)

	long		lEndV_L;				//v1009 : (Step 일 경우 End Voltage Low)			(LOOP 일 경우 멀티 Cycle 반복 횟수)
	BYTE		bEndC_Goto;				//v1009 : (Step 일 경우 Goto -> 종료 용량 분기)		(Cycle 또는 LOOP 일 경우 멀티 Cycle ID)
	BYTE		bEndWattHour_Goto;		//v1009 : (Step 일 경우 Goto -> 종료 전력량 분기)	(LOOP 일 경우 멀티 Cycle 완료시 Goto Step)
	BYTE		bValueRate_Goto;		//v1009 : (Step 일 경우 Goto -> bValueItem 종료 분기)
	BYTE		bAccCycleCount_Goto;	//v1009 : (LOOP 일 경우 Goto -> 누적 Cycle 완료시 Goto Step)
	long		lReserved[2];														
} SFT_STEP_REFERANCE_v1009, *LPSFT_STEP_REFERANCE_v1009;

//SFTScheduler Step Referance
//ljb 20100818Protocol Ver 0x100A 이후 
typedef struct s_RefData {
	// ljb 201011 변경할 내용 //////////////////////////////
	long		lVref_Charge;			//Charge Voltage Referance
	long		lVref_DisCharge;		//Discharge Voltage Referance
	long		lIref;					//Current Referance
	long		lPref;					//Powr Referance
	long		lRref;					//
	BYTE		byRange;				// 0:Auto, 1: High 2: Middle 3: Low, 4: Micro	// v1009 : Cycle Step 일 경우 누적 용량 부호 (0:충방전, 1: 충전, 2:방전)
	BYTE		bValueStepNo;			//Soc Actual capa step no						// v1009 : Cycle Step 일 경우 누적 전력량 부호 (0:충방전, 1: 충전, 2:방전)
	unsigned short int wValueRate;		//Capa SOC Rate
	
	ULONG		ulEndTimeDay;			//End time Day	ljb 20131212 add
	ULONG		ulEndTime;				//End time
    long		lEndV_H;				//End Voltage High
    long		lEndI;					//End Current			
    long		lEndC;					//End Capacity			// v1009 : Cycle Step 일 경우 누적 용량
	long		lGoto;					//Cycle goto step   	// v1009 : (Step 일 경우 Goto -> 전압 분기 High)	(LOOP 일 경우 Cycle 반복 완료 후 Goto Step) 
	long		lCycleCount;			//Cycle count			// v1009 : (Step 일 경우 Goto -> 전압 분기 Low)		(LOOP 일 경우 Cycle 반복 횟수)    
    long		lDeltaV;				//DeltaV (Vpeak - Delta V)													    
	
	long		lEndWatt;				//End Watt
	long		lEndWattHour;			//End WattHour			//v1009 : Cycle Step 일 경우 누적 전력량
	long		lAccCycleCount;			//누적 Loop Count		//v1009 : (Step 일 경우 Goto -> 종료시간 분기)		(LOOP 일 경우 누적 Cycle 반복 횟수)    
	long		lAccCycleGroupID;		//누적 Loop GroupID		//v1009 : (Step 일 경우 Goto -> CV시간 종료 분기)	(LOOP 일 경우 누적 Cycle 할 그룹 ID)    
	ULONG		ulCVTimeDay;			//CV Time Day	ljb 20131212 add
	ULONG		ulCVTime;				//CV Time
	
	BYTE		bChamberlinkOption;		// ljb 20150820 add 챔버 설정 값이 있으면 1 (챔버연동 대기 함), 없으면 0 (챔버연동 대기 없이 바로 진행)	삼성 하선준 요청 기능
	BYTE		bReserved[2];
	BYTE		bValueItem;				//v1009 : ( 0 : 사용안함 , 1 : SOC , 2 : WattHour)
	
	long		lEndV_L;				//v1009 : (Step 일 경우 End Voltage Low)			(LOOP 일 경우 멀티 Cycle 반복 횟수)
	BYTE		bEndC_Goto;				//v1009 : (Step 일 경우 Goto -> 종료 용량 분기)		(Cycle 또는 LOOP 일 경우 멀티 Cycle ID)
	BYTE		bEndWattHour_Goto;		//v1009 : (Step 일 경우 Goto -> 종료 전력량 분기)	(LOOP 일 경우 멀티 Cycle 완료시 Goto Step)
	BYTE		bValueRate_Goto;		//v1009 : (Step 일 경우 Goto -> bValueItem 종료 분기)
	BYTE		bAccCycleCount_Goto;	//v1009 : (LOOP 일 경우 Goto -> 누적 Cycle 완료시 Goto Step)

	//20100818 ljb add 
	short int	ican_function_division[10];		//분류 번호
	unsigned char ican_compare_type[10];			//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char cCan_data_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	short int	cCan_branch[10];					//Can 분기	Next : 251, Pause : 252 
	float		fcan_Value[10];

	
	short int	iaux_function_division[10];		//분류 번호
	unsigned char iaux_compare_type[10];			//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char cAux_data_type[10];				//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	short int	 cAux_branch[10];					//Aux 분기	Next : 251, Pause : 252
	float		faux_Value[10];
	UINT		iaux_conti_time[10];				//ljb 20170824 add

	long		lReserved[2];														
} SFT_STEP_REFERANCE, *LPSFT_STEP_REFERANCE;

//SFTScheduler Step Referance
//Protocol Ver 0x1002 이후 
typedef struct s_RefData_V2 {
	long		lVref;					//Voltage Referance
    long		lIref;					//Current Referance
	long		lRange;					//Range of current
    
	ULONG		ulEndTime;				//End time
    long		lEndV;					//End Voltage
    long		lEndI;					//End Current
    long		lEndC;					//End Capacity
	long		lGoto;					//Cycle goto step   //Rest High Goto
	long		lCycleCount;			//Cycle count		//Rest Low Goto
    long		lDeltaV;				//DeltaV (Vpeak - Delta V)
	unsigned short int wSocRate;		//Capa SOC Rate
	unsigned short int wSocCapStepNo;	//Soc Actual capa step no
	long		lEndWatt;				//End Watt
	long		lEndWattHour;			//End WattHour
	long		lReserved[3];			//Reserved
} SFT_STEP_REFERANCE_V2, *LPSFT_STEP_REFERANCE_V2;

//SFTScheduler Step Referance
//Protocal Ver 0x1000~0x1001
typedef struct s_RefData_V1 {
	long		lVref;					//Voltage Referance
    long		lIref;					//Current Referance
	long		lRange;					//Range of current
    
	ULONG		ulEndTime;				//End time
    long		lEndV;					//End Voltage
    long		lEndI;					//End Current
    long		lEndC;					//End Capacity
	long		lGoto;					//Cycle goto step
	long		lCycleCount;			//Cycle count
    long		lDeltaV;				//DeltaV (Vpeak - Delta V)
	unsigned short int wSocRate;		//Capa SOC Rate
	unsigned short int wSocCapStepNo;	//Soc Actual capa step no
	long		lEndData;				//End 
} SFT_STEP_REFERANCE_V1, *LPSFT_STEP_REFERANCE_V1;


//SFTScheduler Step Ramp Comparison Point 
typedef struct s_StepRampData {			//전압 상승 및 하강 기울기 검사 
	long			lLowData;
	long			lHighData;
	unsigned long	ulTime;
} SFT_COMP_CONDITION;

//SFTScheduler Step Delta condition
//Network Structure
typedef struct s_StepDeltaData {			//전압 전류 변화 검사  
	long			lMinData;
	long			lMaxData;
	unsigned long	ulTime;					//c
} SFT_DELTA_CONDITION;

//Grade Step Data 
typedef struct tag_Grade{			
	char	gradeCode;
	char	item;
	SHORT	reserved2;	
    long	lMinValue;
	long	lMaxValue;
} SFT_GRADE_STEP;


//Grade Condition 
//Network Structure
typedef struct s_Grade_Item {
	BYTE	item;		
	BYTE	stepCount;
	SHORT	gradeItemType;
	SFT_GRADE_STEP step[_SFT_MAX_PACKET_GRADE_STEP];
} SFT_GRADE_CONDITION;

typedef struct s_Record_Contition {
	unsigned long	lTime;
	long			lDeltaV;
	long			lDeltaI;
	long			lDeltaT;
	long			lDeltaP;
	long			lReserved;
}	SFT_RECORD_CONDITION;

typedef struct s_EDLC_Condition {
	long	lCapV1;
	long	lCapV2;
	ULONG	ulDCRStart;
	ULONG	ulDCREndT;
	ULONG	ulLCStartT;
	ULONG	ulLCEndT;

}	SFT_EDLC_CONDITION;

//ljb 201008 Protocal Ver 0x100A 이후 
typedef struct s_SFT_CMD_STEP_DATA_V2 {			//Charge/Discharge Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE stepReferance[_SFT_MAX_PACKET_SUB_STEP];
	SFT_COMP_CONDITION	vRamp[_SFT_MAX_PACKET_COMP_POINT];		//전압 상승 비교값 
	SFT_COMP_CONDITION	iRamp[_SFT_MAX_PACKET_COMP_POINT];		//전류 상승 비교값
	SFT_DELTA_CONDITION	vDelta;									//전압 변화 비교값
	SFT_DELTA_CONDITION	iDelta;									//전류 변화 비교값 
	SFT_RECORD_CONDITION	conREC;							//Record condition	
	SFT_EDLC_CONDITION		conEDLC;
	SFT_GRADE_CONDITION	gradeCondition[_SFT_MAX_PACKET_GRADE_ITEM_IN_STEP];	//Grading 조건 
	
	long	lHighV;									
    long	lLowV;
    long	lHighI;
    long	lLowI;
	long	lHighC;
	long	lLowC;
	long	lHighZ;
	long	lLowZ;
	long	lHighTemp;
	long	lLowTemp;
	long	lCellDeltaVStep;		//yulee 20190531_3 Cell Delta V step 사용
	BYTE	bPatternTime;   		//ljb 20140211 수정함 사용안함 = 0, T1 = 1, T2 = 2 
	BYTE	bStepCanCheckMode; 		//ljb 20150825 eid CAN 통신 체크 해제 (0:통신체크 함, 1: 통신체크 해제)  <= //ljb 20140211 수정함 사용안함 = 0, 전류 = 1, 파워 = 2  
	BYTE	bCanRxEndNoCheck;  		//이민규 대리가 사용  CAN Rx 안전, 종료 체크 안함  사용					 <= //ljb 20140211 수정함 사용안함 = 0, 사각파 = 1, 삼각파 = 2	
	BYTE	bCanTxOffMode;			//ljb 20150825 스텝별 CAN 통신체크 사용 여부		사용				 <= //ljb 20140211 수정함
	float	fPatternMaxValue;		//ljb 20140211 수정함 패턴데이터 절대값 중 MAX 값	사용
	long	lPatternFileSize;		//ljb 20140211 File size							사용
	long	lPatternChecksum;		//ljb 20140211 모든 byte합산						사용
	//long	lReserved[2];			//yulee 20190114
	BYTE	bUseCellBalancing;      //yulee 20190114 셀 밸런싱 사용 유무
	BYTE	bReserved[3];			//yulee 20190114
	//long	lreserved;				//yulee 20190114 //yulee 20190531_3 Cell Delta V step 사용
} SFT_STEP_CONDITION_V2, *LPSFT_STEP_CONDITION_V2; //lyj 20200214 LG v1001~105 미만



//ljb 201008 Protocal Ver 0x100A 이후 
typedef struct s_SFT_CMD_STEP_DATA_V3 {			//Charge/Discharge Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE stepReferance[_SFT_MAX_PACKET_SUB_STEP];
	SFT_COMP_CONDITION	vRamp[_SFT_MAX_PACKET_COMP_POINT];		//전압 상승 비교값 
	SFT_COMP_CONDITION	iRamp[_SFT_MAX_PACKET_COMP_POINT];		//전류 상승 비교값
	SFT_DELTA_CONDITION	vDelta;									//전압 변화 비교값
	SFT_DELTA_CONDITION	iDelta;									//전류 변화 비교값 
	SFT_RECORD_CONDITION	conREC;							//Record condition	
	SFT_EDLC_CONDITION		conEDLC;
	SFT_GRADE_CONDITION	gradeCondition[_SFT_MAX_PACKET_GRADE_ITEM_IN_STEP];	//Grading 조건 

	long	lHighV;									
	long	lLowV;
	long	lHighI;
	long	lLowI;
	long	lHighC;
	long	lLowC;
	long	lHighZ;
	long	lLowZ;
	long	lHighTemp;
	long	lLowTemp;

	long	lCellDeltaVStep;		//yulee 20190531_3 Cell Delta V step 사용
	long	faultcompAuxTemp;
	long	faultcompAuxTh;
	long	faultcompAuxT;
	long	faultcompAuxV;
	long	faultcompAuxV_Time;

	BYTE	bPatternTime;   		//ljb 20140211 수정함 사용안함 = 0, T1 = 1, T2 = 2 
	BYTE	bStepCanCheckMode; 		//ljb 20150825 eid CAN 통신 체크 해제 (0:통신체크 함, 1: 통신체크 해제)  <= //ljb 20140211 수정함 사용안함 = 0, 전류 = 1, 파워 = 2  
	BYTE	bCanRxEndNoCheck;  		//이민규 대리가 사용  CAN Rx 안전, 종료 체크 안함  사용					 <= //ljb 20140211 수정함 사용안함 = 0, 사각파 = 1, 삼각파 = 2	
	BYTE	bCanTxOffMode;			//ljb 20150825 스텝별 CAN 통신체크 사용 여부		사용				 <= //ljb 20140211 수정함
	float	fPatternMaxValue;		//ljb 20140211 수정함 패턴데이터 절대값 중 MAX 값	사용
	long	lPatternFileSize;		//ljb 20140211 File size							사용
	long	lPatternChecksum;		//ljb 20140211 모든 byte합산						사용
	
	BYTE	bUseCellBalancing;      //yulee 20190114 셀 밸런싱 사용 유무
	BYTE	faultCompAuxV_Vent_flag;
	BYTE	faultCompAuxTemp_Vent_flag;
	BYTE	faultCompAuxTh_Vent_flag;
	
	BYTE	faultCompAuxT_vent_flag;
	BYTE	faultDelta_AuxV_Vent_flag;
	BYTE	reserved1[2];			//yulee 20190114
	long	reserved2[2];
} SFT_STEP_CONDITION_V3, *LPSFT_STEP_CONDITION_V3 ; //lyj 20200214 LG v1015 이상

//Protocal Ver 0x100F Wait Time Goto 파일 생성시 사용 
typedef struct s_SFT_CMD_STEP_DATA_WAIT_TIME {
	short int		nStepNo;		//Step 번호
	unsigned char 	byTimieInit;	// 0: 초기화 안 함, 1:초기화 	 	
	unsigned char 	bReserved;
	short int 		nWaitDay;		//대기 Day
	short int 		nWaitHour;		//대기 Hour
	short int 		nWaitMin;		//대기 Minute
	short int 		nWaitSec;		//대기 Sec
	long 			lReserved[2];
} SFT_STEP_CONDITION_WAIT_TIME, *LPSFT_STEP_CONDITION_WAIT_TIME;


//Protocal Ver 0x1004 이후 
typedef struct s_SFT_CMD_STEP_SIMULATION_DATA {			//Charge/Discharge Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE stepReferance[_SFT_MAX_PACKET_SUB_STEP];
	
	SFT_RECORD_CONDITION	conREC;							//Record condition	
		
	long	lHighV;									
    long	lLowV;
    long	lHighI;
    long	lLowI;
	long	lHighC;
	long	lLowC;
	long	lHighZ;
	long	lLowZ;
	long	lHighTemp;
	long	lLowTemp;
	long	lCellDeltaVStep;//yulee 20190531_3
	long	lReserved[1];

//	char    *pPatternData;
} SFT_STEP_SIMUL_CONDITION, *LPSFT_STEP_SIMUL_CONDITION;


//Ptorocol ver 0x1000~0x1001
typedef struct s_SFT_CMD_STEP_DATA_V1 {			//Charge/Discharge Step Data Structure
	SFT_STEP_HEADER	stepHeader;
	SFT_STEP_REFERANCE_V1 stepReferance[_SFT_MAX_PACKET_SUB_STEP];

	SFT_COMP_CONDITION	vRamp[_SFT_MAX_PACKET_COMP_POINT];		//전압 상승 비교값 
	SFT_COMP_CONDITION	iRamp[_SFT_MAX_PACKET_COMP_POINT];		//전류 상승 비교값
	
	SFT_DELTA_CONDITION	vDelta;									//전압 변화 비교값
	SFT_DELTA_CONDITION	iDelta;									//전류 변화 비교값 

	SFT_RECORD_CONDITION	conREC;							//Record condition	
	SFT_EDLC_CONDITION		conEDLC;
	
	SFT_GRADE_CONDITION	gradeCondition[_SFT_MAX_PACKET_GRADE_ITEM_IN_STEP];	//Grading 조건 

	long	lHighV;									
    long	lLowV;
    long	lHighI;
    long	lLowI;
	long	lHighC;
	long	lLowC;
	long	lHighZ;
	long	lLowZ;
	long	lReserved[4];
} SFT_STEP_CONDITION_V1, *LPSFT_STEP_CONDITION_V1;

//Module에서 PC로 전송되는 채널의 Data 전송 Format
//2008/07/23 수정
typedef struct s_SFT_CMD_CH_DATA		
{
	unsigned char	chNo;					// Channel Number			//One Base
	unsigned char	chState;				// Run, Stop(Manual, Error), End
	unsigned char	chStepType;				// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	unsigned char	chMode;
	unsigned char	chDataSelect;			// For Display Data, For Saving Data
	unsigned char	chCode;
	unsigned char	chStepNo;
	unsigned char	chGradeCode;			
	long			lVoltage;				// Result Data...
	long			lCurrent;
//	long			lCapacity;				//20080723  lChargeAh,lDisChargeAh,lCapacitance 로 변경
	long			lChargeAh;			
	long			lDisChargeAh;
	long			lCapacitance;
	long			lWatt;					//+,- 값 표현
//	long			lWattHour;				//20080723 lChargeWh, lDisChargeWh 로 변경
	long			lChargeWh;
	long			lDisChargeWh;
	unsigned long	ulStepDay;				//ljb 20131125 add -> LG 적용
	unsigned long	ulStepTime;				// 이번 Step 진행 시간
	unsigned long	ulTotalDay;				//ljb 20131125 add -> LG 적용
	unsigned long	ulTotalTime;			// 시험 Total 진행 시간
	long			lImpedance;				// Impedance (AC or DC)
	//long			lTemparature;
	//long			lPressure;
	unsigned char	chReservedCmd;			//0:None, 1:Stop, 2:Pause
	unsigned char	chCommState;			//ljb 20100909	Bit LSB(0:정상, 1:이상) 1:Aux 온도 통신 상태, 2: Aux 전압 통신 상태 
											// 3: CAN Master 통신상태, 4: CAN Slave 통신상태, 
//	unsigned char	reserved1[2];			//2007/10/16 1byte -> 3byte
	unsigned char	chOutputState;			//ljb 201012	LSB적용 (0,0,0,0,0,PackRealyOn,ChargeOn,keyOn)  v100b
	unsigned char	chInputState;			//ljb 201012	한전 Pack Loader 적용	
	short	 int	nAuxCount;				//2007/07/31 
	short	 int	nCanCount;				//2007/10/16

	unsigned long	nTotalCycleNum;			//2005/12/29 unsigned short int => unsigned long으로 변경
	unsigned long	nCurrentCycleNum;		//2005/12/29 unsigned short int => unsigned long으로 변경
	unsigned long	nAccCycleGroupNum1;		//ljb v1009 그룹1 누적 Cycle 갯수
	unsigned long	nAccCycleGroupNum2;		//ljb v1009 그룹2 누적 Cycle 갯수
	unsigned long	nAccCycleGroupNum3;		//ljb v1009 그룹3 누적 Cycle 갯수
	unsigned long	nAccCycleGroupNum4;		//ljb v1009 그룹4 누적 Cycle 갯수
	unsigned long	nAccCycleGroupNum5;		//ljb v1009 그룹5 누적 Cycle 갯수
	unsigned long	nMultiCycleGroupNum1;	//ljb v1009 그룹1 Multi Cycle 갯수
	unsigned long	nMultiCycleGroupNum2;	//ljb v1009 그룹2 Multi Cycle 갯수
	unsigned long	nMultiCycleGroupNum3;	//ljb v1009 그룹3 Multi Cycle 갯수
	unsigned long	nMultiCycleGroupNum4;	//ljb v1009 그룹4 Multi Cycle 갯수
	unsigned long	nMultiCycleGroupNum5;	//ljb v1009 그룹5 Multi Cycle 갯수
	long			lAvgVoltage;			//Average Voltage of current step
	long			lAvgCurrent;			//Average current of current step
	long			lSaveSequence;			// Expanded Field 2005/12/29 모듈에서 저장하는 Data의 순서 번호 
	unsigned long	ulCVDay;				//ljb 20131125 add -> LG 적용
	unsigned long	ulCVTime;				//CV에 진입한 시간 20080723
	long			lSyncTime[2];			//현재 데이터에 대한 시간정보 YYYYMMDD HHMMSS.mmm 변환 가능한 시간 20080723 214055.123	

//  ljb 201011 추가 해야 할 내용 	
 	long			lVoltage_Input;			//ljb 201011 add 한전 & 울산꺼 부터
 	long			lVoltage_Power;			//ljb 201011 add 한전 & 울산꺼 부터
 	long			lVoltage_Bus;			//ljb 201011 add 한전 & 울산꺼 부터
	unsigned char	cUsingChamber;			//ljb 201010	챔버 연동 플래그	v100b
	unsigned char	cRecordTimeNo;			//ljb 201010	1 base : R1,R2,R3	v100b
	//unsigned char	cCanCommCheck;			//ljb 20151111 GUI 에는 없음 	
	unsigned char	cOutMuxUse;				//ljb 20151111 	0: not use, 1: use
	unsigned char	cOutMuxBackup;			//ljb 20151111 	0: open, 1: Mux A, 2: Mux B
	unsigned char   cCbank;
	unsigned char   creservedp[3];
	long			lreserved[5];				//ljb 201011 add 한전 & 울산꺼 부터

} SFT_CH_DATA, *LPSFT_CH_DATA;

typedef struct s_SFT_CMD_CH_DATA_B		
{
	unsigned char	chNo;					// Channel Number			//One Base
	unsigned char	chState;				// Run, Stop(Manual, Error), End
	unsigned char	chStepType;				// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	unsigned char	chMode;
	unsigned char	chDataSelect;			// For Display Data, For Saving Data
	unsigned char	chCode;
	unsigned char	chStepNo;
	unsigned char	chGradeCode;			
	long			lVoltage;				// Result Data...
	long			lCurrent;
	long			lCapacity;				
	long			lWatt;					//+,- 값 표현
	long			lWattHour;				//20080723 lChargeWh, lDisChargeWh 로 변경
	unsigned long	ulStepTime;				// 이번 Step 진행 시간
	unsigned long	ulTotalTime;			// 시험 Total 진행 시간
	long			lImpedance;				// Impedance (AC or DC)
	//long			lTemparature;
//	long			lPressure;
// =>	
	unsigned char	chReservedCmd;			//0:None, 1:Stop, 2:Pause
	unsigned char	reserved1[3];			//2007/10/16 1byte -> 3byte
	long			reserved2[4];			//2007/10/16 Loop-In-Loop 등 대비용
	short	 int	nAuxCount;			//2007/07/31 
	short	 int	nCanCount;			//2007/10/16

	unsigned long	nTotalCycleNum;			//2005/12/29 unsigned short int => unsigned long으로 변경
	unsigned long	nCurrentCycleNum;		//2005/12/29 unsigned short int => unsigned long으로 변경
	long			lAvgVoltage;			//Average Voltage of current step
	long			lAvgCurrent;			//Average current of current step
	long			lSaveSequence;			// Expanded Field 2005/12/29 모듈에서 저장하는 Data의 순서 번호 
} SFT_CH_DATA_B, *LPSFT_CH_DATA_B;

typedef struct s_SFT_CMD_CH_DATA_A		
{
	unsigned char	chNo;					// Channel Number			//One Base
	unsigned char	chState;				// Run, Stop(Manual, Error), End
	unsigned char	chStepType;				// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	unsigned char	chMode;
	unsigned char	chDataSelect;			// For Display Data, For Saving Data
	unsigned char	chCode;
	unsigned char	chStepNo;
	unsigned char	chGradeCode;			
	long			lVoltage;				// Result Data...
	long			lCurrent;
	long			lCapacity;
	long			lWatt;
	long			lWattHour;
	unsigned long	ulStepTime;				// 이번 Step 진행 시간
	unsigned long	ulTotalTime;			// 시험 Total 진행 시간
	long			lImpedance;				// Impedance (AC or DC)
	long			lTemparature;
//	long			lPressure;
// =>	
	unsigned char	chReservedCmd;			//0:None, 1:Stop, 2:Pause
	unsigned char	reserved1;			//2007/07/31 3byte -> 1byte
	short	 int	nAuxCount;			//2007/07/31 

	unsigned long	nTotalCycleNum;			//2005/12/29 unsigned short int => unsigned long으로 변경
	unsigned long	nCurrentCycleNum;		//2005/12/29 unsigned short int => unsigned long으로 변경
	long			lAvgVoltage;			//Average Voltage of current step
	long			lAvgCurrent;			//Average current of current step
	long			lSaveSequence;			// Expanded Field 2005/12/29 모듈에서 저장하는 Data의 순서 번호 
	
} SFT_CH_DATA_A, *LPSFT_CH_DATA_A;

//CAN////////////////////////////////////////////////////////////////////
typedef struct s_SFT_CAN_COMMON_DATA {
	unsigned char		can_baudrate;			//0:125k, 1: 250k, 2:500k, 3: 1M
	unsigned char		extended_id;			//0:unused, 1:used
	unsigned char		bms_type;				//ljb 201008 v100A -> default:0
	unsigned char		bms_sjw;				//ljb 20100909 default : 0 , select 1~4
	
	long				controller_canID;
	long				mask[2];
	long				filter[6];

	//ljb 20181115 S 구조체 변경
	//long				l_Reserved[3];
	unsigned char		can_fd_flag;  //0:can 2.0B, 1: can_fd
	unsigned char		can_datarate; //0:500K, 1: 833K, 2:1M, 3:1.5M, 4:2M, 5.3M
	unsigned char		terminal_r;   //0:open, 1:120ohm<default>
	unsigned char		crc_type;	  //0:non iso crc, 1:iso crc<default>

	unsigned char		can_lin_select;	  //0:can, 1:Lin
	unsigned char		c_Reserved[3];	  //0:non iso crc, 1:iso crc<default>
	long				l_Reserved;
	//ljb 20181115 E 구조체 변경

	short int can_function_division[10];		//분류 번호
	unsigned char can_compare_type[10];			//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char can_data_type[10];			//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	float	fcan_Value[10];
} SFT_CAN_COMMON_DATA;

typedef struct s_SFT_CAN_SET_DATA {
	unsigned char		canType;				//0: unused, 1:master, 2:slave
	unsigned char		byte_order;				//0:intel, 1:motolora
	unsigned char		data_type;				//0:signed, 1:unsigned, 2:string
	unsigned char		c_Reserved;				//20090202	ljb		cvFlag;			//0 : unused cell cv, 1: used cell cv
												//v100F 에서 LG 안전조건 값으로 GUI가 사용 하고 있음. 
	float				factor_multiply;		//+ Value : 곱하기, - Value : 나누기
	short int			startBit;
	short int			bitCount;
	long				canID;					//hex value
	char				name[128];
	float				fault_upper;
	float				fault_lower;
	float				end_upper;
	float				end_lower;
	float				default_fValue;			//기본 값
	float				factor_Offset;
	short int			function_division;		// 0: default	
												// 1~50 : Cell CV		51~100 : Invert Capacity voltage
												//101~150 : FAN Speed	151~200 : ECU ID
	//ljb 201008 Ver 0x100A
	short int			function_division2;	
	short int			function_division3;	
	short int			ui_Reserved;
	long				sentTime;
	long				l_Reserved[2];

	//ljb 20131213 add 동일 아이디 할당 가능하게 추가 Ver 0x100C *******
	short int			startBit2;
	short int			bitCount2;
	unsigned char		byte_order2;		//0:intel, 1:motolora
	unsigned char		data_type2;			//0:signed, 1:unsigned, 2:string
	unsigned char		c_Reserved2[2];
	float				default_fValue2;	//기본 값
	//********************************************************
} SFT_CAN_SET_DATA;

typedef struct s_SFT_CAN_SET_DATA1 {
	unsigned char		canType;		//0: unused, 1:master, 2:slave
	unsigned char		byte_order;		//0:intel, 1:motolora
	unsigned char		data_type;		//0:signed, 1:unsigned, 2:string
	unsigned char		cvFlag;			//0 : unused cell cv, 1: used cell cv
	float				factor_multiply;//+ Value : 곱하기, - Value : 나누기
	short int			startBit;
	short int			bitCount;
	long				canID;			//hex value
	char				name[128];
	float				fault_upper;
	float				fault_lower;
	float				end_upper;
	float				end_lower;
	long				l_Reserved;		//only sbc use
}	SFT_CAN_SET_DATA1;

typedef struct s_SFT_RCV_CMD_CAN_SET {
	SFT_CAN_COMMON_DATA	canCommonData[_SFT_MAX_INSTALL_CH_COUNT][2];	//MAX CH Num * (Master+Slave)
	SFT_CAN_SET_DATA	canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN];
} SFT_RCV_CMD_CAN_SET;

typedef struct s_SFT_RCV_CMD_CAN_SET1 {
	SFT_CAN_COMMON_DATA	canCommonData[_SFT_MAX_INSTALL_CH_COUNT][2];	//MAX CH Num * (Master+Slave)
	SFT_CAN_SET_DATA1	canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN];
} SFT_RCV_CMD_CAN_SET1;

//ljb 201008 CAN설정 분할(채널별)
typedef struct s_SFT_RCV_CMD_CHANNEL_CAN_SET {
	SFT_CAN_COMMON_DATA	canCommonData[2];	// Master+Slave
	SFT_CAN_SET_DATA	canSetData[_SFT_MAX_MAPPING_CAN];
} SFT_RCV_CMD_CHANNEL_CAN_SET;

typedef struct s_p1_send_cmd_can_info_reply_tag {
	short int installedCanCount[_SFT_MAX_INSTALL_CH_COUNT];		
	SFT_RCV_CMD_CAN_SET	canSetAllData;
} SFT_MD_CAN_INFO_DATA, *LPSFT_MD_CAN_INFO_DATA;

typedef struct s_p1_send_cmd_can_info_reply_tag1 {
	short int installedCanCount[_SFT_MAX_INSTALL_CH_COUNT];		
	SFT_RCV_CMD_CAN_SET1	canSetAllData;
} SFT_MD_CAN_INFO_DATA1, *LPSFT_MD_CAN_INFO_DATA1;

typedef union s_uniCANVALUE{
	long	lVal[2];
	float	fVal[2];
	char	strVal[8];
}SFT_CAN_VALUE;

typedef struct s_PWRSUPPLY_VALUE
{
	float	fSetVoltage;
	float	fCurVoltage;
	
}SFT_PWRSUPPLY_VALUE;

//★★ ljb 20091029
typedef struct s_CHAMBER_VALUE
{
	float	fRefTemper;
	float	fCurTemper;
	float	fRefHumidity;
	float	fCurHumidity;

// 	int     iRefPump;
// 	int     iCurPump;	
	float     fRefPump; //lyj 20201102 DES칠러 펌프 소숫점
	float     fCurPump;	//lyj 20201102 DES칠러 펌프 소숫점

}SFT_CHAMBER_VALUE;


//★★ ljb 20091029
typedef struct s_CELLBAL_VALUE{
	char	ch_state[32];	
}SFT_CELLBAL_VALUE;

//★★ ljb 20150427
typedef struct s_LOADER_VALUE{
	float	fVoltage;
	float	fCurrent;
	float	fSetResis;
	float	fSetPower;
}SFT_LOADER_VALUE;

typedef struct s_SFT_CAN_DATA {
	unsigned char		canType;		//0:master, 1:slave
	unsigned char		data_type;		//0:signed, 1:unsigned, 2:string
	short int			function_division;
	SFT_CAN_VALUE		canVal;
} SFT_CAN_DATA;

typedef struct s_CAN_DBC_DATA {
	//int			    can_id;			
	unsigned int	    uiCan_id;	//ksj 20200323 : Unsined int로 변경. 이름을 일부로 변경하여 연관되어있는 코드 모두 점검.
	char			signal_name[50];
	short int		startBit;
	short int		length;
	unsigned char	byteOrder;		//0:motorola, 1:intel
	unsigned char	valueType;		//0:signed (-), 1:unsigned (+)
	float			factor;
	float			offset;
	float			minValue;
	float			maxValue;
} S_CAN_DBC_DATA;

//CAN Transmit////////////////////////////////////////////////////////
typedef struct s_SFT_CAN_COMMON_TRANS_DATA {
	unsigned char		can_baudrate;	//0:125k, 1: 250k, 2:500k, 3: 1M
	unsigned char		extended_id;	//0:unused, 1:used
	unsigned char		bms_type;		//ljb 201008 v100A default : 0
	unsigned char		bms_sjw;			//ljb 20100909 default : 0 , select 1~4
	long				controller_canID;

	//ljb 20181115 S 구조체 변경
	//float				f_Reserved[2];
	unsigned char		can_fd_flag;  //0:can 2.0B, 1: can_fd
	unsigned char		can_datarate; //0:500K, 1: 833K, 2:1M, 3:1.5M, 4:2M, 5.3M
	unsigned char		terminal_r;   //0:open, 1:120ohm<default>
	unsigned char		crc_type;	  //0:non iso crc, 1:iso crc<default>

	unsigned char		can_lin_select;
	unsigned char		c_Reserved[3];
	//ljb 20181115 E 구조체 변경

	//ljb 201012 v100B
	short int can_function_division[10];		//분류 번호
	unsigned char can_compare_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char can_data_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	float	fcan_Value[10];
} SFT_CAN_COMMON_TRANS_DATA;

//ljb v100A CAN Transmit change ////////////////////////////////////////////////////////////////////
typedef struct s_SFT_CAN_CHANGE_DATA {
	unsigned char		canType;		//0: unused, 1:master, 2:slave
	unsigned char		cReserved[3];
	
	float				default_fValue;		//기본 값
	long				canID;			//hex value
	short int			startBit;
	
	short int			function_division;		// 0: default		1~50 : Capacity voltage
	// 51~100 MCU Temp		101~150 : ECU ID
	//ljb 201008 Ver 0x1010
	short int			function_division2;
	short int			function_division3;
	long				l_Reserved2[2];
} SFT_CAN_CHANGE_DATA;

typedef struct s_SFT_SEND_CMD_CAN_CHANGE_SET {
	SFT_CAN_CHANGE_DATA	canChangeData[10];
} SFT_SEND_CMD_CAN_CHANGE_SET;

//////////////////////////////////////////////////////////////////////////

typedef struct s_SFT_CAN_SET_TRANS_DATA {
	unsigned char		canType;		//0: unused, 1:master, 2:slave
	unsigned char		byte_order;		//0:intel, 1:motolora
	unsigned char		data_type;		//0:signed, 1:unsigned, 2:string, 3:float , 4: bit
	unsigned char		dlc;			//20090202 0x1007		0 bit : capvtg , 1 bit : mcu temp	//ljb 20180326 DLC로 변경 
	float				factor_multiply;	//+ Value : 곱하기, - Value : 나누기
	float				default_fValue;		//기본 값
	short int			startBit;
	short int			bitCount;
	long				canID;			//hex value
	long				send_time;		//전송속도(ms)
	char				name[128];
	long				l_Reserved;
	//20081112 Ver 0x1006
	float				factor_Offset;
	//20090202 Ver 0x1007
	short int			function_division;		// 0: default		1~50 : Capacity voltage
												// 51~100 MCU Temp		101~150 : ECU ID
	//ljb 201008 Ver 0x1010
	short int			function_division2;
	short int			function_division3;
	short int			ui_Reserved;
	long				l_Reserved2[2];
} SFT_CAN_SET_TRANS_DATA;

typedef struct s_SFT_CAN_SET_TRANS_DATA1 {
	unsigned char		canType;		//0: unused, 1:master, 2:slave
	unsigned char		byte_order;		//0:intel, 1:motolora
	unsigned char		data_type;		//0:signed, 1:unsigned, 2:string, 3:float , 4: bit
	unsigned char		CapVtgFlag;		//0 bit : capvtg , 1 bit : mcu temp 
	float				factor_multiply;	//+ Value : 곱하기, - Value : 나누기
	float				default_value;		//기본 값
	short int			startBit;
	short int			bitCount;
	long				canID;			//hex value
	long				send_time;		//전송속도(ms)
	char				name[128];
	long				reserved2;		//Only SBC USE
} SFT_CAN_SET_TRANS_DATA1;

typedef struct s_SFT_TRANS_CMD_CAN_SET {
	SFT_CAN_COMMON_TRANS_DATA	canCommonData[_SFT_MAX_INSTALL_CH_COUNT][2];	//MAX CH Num 
	SFT_CAN_SET_TRANS_DATA	canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN];
} SFT_TRANS_CMD_CAN_SET;

typedef struct s_SFT_TRANS_CMD_CAN_SET1 {
	SFT_CAN_COMMON_TRANS_DATA	canCommonData[_SFT_MAX_INSTALL_CH_COUNT][2];	//MAX CH Num 
	SFT_CAN_SET_TRANS_DATA1	canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN];
} SFT_TRANS_CMD_CAN_SET1;

//ljb 201008 채널별 설정
typedef struct s_SFT_TRANS_CMD_CHANNEL_CAN_SET {
	SFT_CAN_COMMON_TRANS_DATA	canCommonData[2];	//MAX CH Num 
	SFT_CAN_SET_TRANS_DATA	canSetData[_SFT_MAX_MAPPING_CAN];
} SFT_TRANS_CMD_CHANNEL_CAN_SET;

typedef struct s_p1_send_cmd_can_trans_info_reply_tag {
	short int installedCanCount[_SFT_MAX_INSTALL_CH_COUNT];		
	SFT_TRANS_CMD_CAN_SET	canSetAllData;
} SFT_MD_CAN_TRANS_INFO_DATA, *LPSFT_MD_CAN_TRANS_INFO_DATA;

typedef struct s_p1_send_cmd_can_trans_info_reply_tag1 {
	short int installedCanCount[_SFT_MAX_INSTALL_CH_COUNT];		
	SFT_TRANS_CMD_CAN_SET1	canSetAllData;
} SFT_MD_CAN_TRANS_INFO_DATA1, *LPSFT_MD_CAN_TRANS_INFO_DATA1;

//ljb add 2008-12
//BMS Request Ver 0x1007  (FAN TEST, CPU ID)   //////////////////////////////
//body of SFT_CAN_SET_DATA command		
typedef struct s_STF_MD_CAN_SET_DATA{
	short int	 iFunctionDivision;				//BMS respons Command 
	short int	 ui_Reserved;					
}STF_MD_CAN_SET_DATA;	
				
// iFunctionDivision 값 분류
// 0: default		1~50 : Capacity voltage
// 51~100 MCU Temp		101~150 : ECU ID
// 201~250 SOC Init		201~250 : SOC Init Result

#define		SFT_CMD_CAN_FAN_TEST			51;
#define		SFT_CMD_CAN_CPU_TEST			101;

typedef struct
{
	char	ModuleID[256];
	int		iFan1;
	int		iFan2;
	int		iFan3;
}	SFT_BMS_FAN_TEST_RESPONSE;

typedef struct
{
	char    bChannel;
	WORD	wState;
	WORD	wStepType;
	long	lVoltage;
	long	lCurrent;
	ULONG	ulTotalTime;
	ULONG	ulStepTime;
	int		nTotalCycle;
	int		nCurCycle;
}	SFT_BMA_CHANNEL_INFO, LPSFT_BMA_CHALLEL_INFO;

typedef struct
{
	char Cal_RepairShopCode_TesterSerialNum[10];
	char Cal_ProgrammingDate[4];
	char Vehicle_IdentificationNum[17];
	char Vehicle_ManufacturerECU_HardwareNum[12];
	char RepairShopCode_TesterSerialNum[10];
	char ProgrammingDate[4];
	char SubSystem_ECU_SoftwareNum[4];
	char SubSystem_Cal_Num[1];
}	SFT_BMS_CPU_ID_RESPONSE;


//END CAN////////////////////////////////////////////////////////////////////
//(SBC->PC) Aux Data Format	//2007/07/31
typedef struct s_SFT_CMD_AUX_DATA
{
	short int		auxChNo;
	unsigned char	auxChType;			//0:온도, 1:전압, 2:온도(써미스터)
	unsigned char	auxTempTableType;	//써미스터 온도 테이블	
	long			lValue;
} SFT_AUX_DATA, *LPSFT_AUX_DATA;

typedef struct s_SFT_CMD_AUX_DATA_A
{
	short int	chNo;
	short int	chType;		//V or I
	long		lValue;
	long		reserved;	//2007/10/16 Delete
} SFT_AUX_DATA_A, *LPSFT_AUX_DATA_A;




//Version 0x1005 이후
//ljb 201012 Version 0x100A 이후 사용
//전송 포맷 : SFT_MSG_HEADER + SFT_CH_DATA + SFT_AUX_DATA //2008/07/23 SFT_CH_DATA 수정
typedef struct s_SFT_CMD_VARIABLE_CH_DATA
{
	SFT_CH_DATA 	  chData;
	SFT_AUX_DATA	  auxData[_SFT_MAX_MAPPING_AUX];
	SFT_CAN_DATA  	  canData[_SFT_MAX_MAPPING_CAN];
	SFT_CHAMBER_VALUE ovenData;			//ljb
	SFT_LOADER_VALUE  loaderData;		//ljb 20150427 add
	SFT_CHAMBER_VALUE chillerData;		//ljb 20170906 add
	SFT_CELLBAL_VALUE cellBALData;		//ljb 20170906 add
	SFT_PWRSUPPLY_VALUE pwrSupplyData;	//ljb 20170906 add
} SFT_VARIABLE_CH_DATA, *LPSFT_VARIABLE_CH_DATA;

//////////////////////////////////////////////////////////////////////////
// + BW KIM 2014.03.10 100E 기능 추가

typedef struct s_SFT_CMD_VARIABLE_CH_DATA_100E
{
	short int ch_count;
	short int aux_count;
	short int can_count;
	short int resetved;				//ljb
	SFT_VARIABLE_CH_DATA Data;
} SFT_VARIABLE_CH_DATA_100E, *LPSFT_VARIABLE_CH_DATA_100E;

// -
//////////////////////////////////////////////////////////////////////////
//Version 0x1004 이후
//전송 포맷 : SFT_MSG_HEADER + SFT_CH_DATA + SFT_AUX_DATA //2007/07/31
typedef struct s_SFT_CMD_VARIABLE_CH_DATA_V1004
{
	SFT_CH_DATA_B 	chData;
	SFT_AUX_DATA	auxData[_SFT_MAX_MAPPING_AUX];
	SFT_CAN_DATA	canData[_SFT_MAX_MAPPING_CAN];
} SFT_VARIABLE_CH_DATA_V1004, *LPSFT_VARIABLE_CH_DATA_V1004;

//하위 버전 호환용 - CAN 사용 안함
typedef struct s_SFT_CMD_VARIABLE_CH_DATA_A
{
	SFT_CH_DATA_A 	chData;
	SFT_AUX_DATA_A	auxData[_SFT_MAX_MAPPING_AUX];
} SFT_VARIABLE_CH_DATA_A, *LPSFT_VARIABLE_CH_DATA_A;

//ksj 20200116 : V1015 이전까지 사용하던 구조체
//(SBC->PC) Installed Aux Data Format	//2007/08/06

//typedef struct s_SFT_AUX_INFO_DATA {
typedef struct s_SFT_AUX_INFO_DATA_V1015 {
	unsigned char chNo;
	unsigned char reserved[3];
	short int	auxChNo;
	short int	auxType;
	char		szName[128];
	long		upper_limit;
	long		lower_limit;
	long		reserved1[4];

//} SFT_AUX_INFO_DATA, *LPSFT_AUX_INFO_DATA;
} SFT_AUX_INFO_DATA_V1015, *LPSFT_AUX_INFO_DATA_V1015;


//ksj 20200116 : V1016 구조체 변경.
typedef struct s_SFT_AUX_INFO_DATA{
	unsigned char chNo;					//Channel Number (1 Base)
	unsigned char auxTempTableType;		//온도센서 타입(Thermistor)
	unsigned char vent_use_flag;		//ksj 20200116 : vent 연동 사용 여부
	unsigned char reserved1;
	short	auxChNo;					//AUX ID	(1 Base)
	short	auxType;					//AUX Type (0 : 온도센서, 1:Voltage  2:온도센서(Thermistor) 3:습도센서
	char	szName[128];				//Aux Name
	long	upper_limit;				//센서의 상한값		
	long	lower_limit;				//센서의 하한값
	long	lEndMaxData;				//종료 상한값
	long	lEndMinData;				//종료 하한값
	
	long	vent_upper;	//ksj 20200116 : v1016 vent 상한
	long	vent_lower; //ksj 20200116 : v1016 vent 하한

	short int	funtion_division1;		
	short int	funtion_division2;		
	short int	funtion_division3;		
	short int	reserved2;		
}SFT_AUX_INFO_DATA, *LPSFT_AUX_INFO_DATA;


//(SBC->PC) Installed Aux Data Format	//2007/08/06
typedef struct s_SFT_AUX_INFO_DATA_A {
	unsigned char chNo;
	unsigned char reserved[3];
	short int	auxChNo;
	short int	auxType;
	char		szName[32];
	long		upper_limit;
	long		lower_limit;
	long		reserved1[4];
} SFT_AUX_INFO_DATA_A, *LPSFT_AUX_INFO_DATA_A;



//ksj 20200116 : 주석처리. 기존 ~V1015 까지 사용하던 구조체.
/*typedef struct s_SFT_CMD_AUX_INFO_DATA
{
	short int	wInstalledTemp;
	short int	wInstalledVolt;
	short int	wInstalledTempTh;		//wInstalledCAN;
	short int	reserved;
	s_SFT_AUX_INFO_DATA auxData[_SFT_MAX_MAPPING_AUX];
}SFT_MD_AUX_INFO_DATA, *LPSFT_MD_AUX_INFO_DATA;
*/

//ksj 20200116 : 기존 ~V1015 까지 사용하던 구조체 이름 변경 SFT_MD_AUX_INFO_DATA->SFT_MD_AUX_INFO_DATA_V1015
typedef struct s_SFT_CMD_AUX_INFO_DATA_V1015
{
	short int	wInstalledTemp;
	short int	wInstalledVolt;
	short int	wInstalledTempTh;		//wInstalledCAN;
	short int	reserved;
	SFT_AUX_INFO_DATA_V1015 auxData[_SFT_MAX_MAPPING_AUX];
}SFT_MD_AUX_INFO_DATA_V1015, *LPSFT_MD_AUX_INFO_DATA_V1015;

//ksj 20200116 : V1016 Aux Thermistor Humidity 추가.
typedef struct s_SFT_CMD_AUX_INFO_DATA
{
	short int	wInstalledTemp; //온도 aux 개수
	short int	wInstalledVolt; //전압 aux 개수
	short int	wInstalledTempTh; //온도 써미스터 Aux 개수
	short int	wInstalledHumi; //습도 센서 Aux 개수
	s_SFT_AUX_INFO_DATA auxData[_SFT_MAX_MAPPING_AUX];
}SFT_MD_AUX_INFO_DATA, *LPSFT_MD_AUX_INFO_DATA;


typedef struct s_SFT_CMD_AUX_INFO_DATA_A
{
	short int	wInstalledTemp;
	short int	wInstalledVolt;
	short int	wInstalledCAN;
	short int	reserved;
	s_SFT_AUX_INFO_DATA_A auxData[_SFT_MAX_MAPPING_AUX];
}SFT_MD_AUX_INFO_DATA_A, *LPSFT_MD_AUX_INFO_DATA_A;


//ksj 20200116 : V1015 이전 버전 구조체 이름 변경 STF_MD_AUX_SET_DATA->STF_MD_AUX_SET_DATA_V1015
//body of SFT_CMD_AUX_SET_DATA command

//typedef struct s_STF_CMD_AUX_SET_DATA{
typedef struct s_STF_CMD_AUX_SET_DATA_V1015{
	unsigned char chNo;				//Channel Number (1 Base)
	unsigned char auxTempTableType;	//ljb 20160429  add 온도센서 타입(Thermistor)
	unsigned char reserved[2];
	short	auxChNo;				//AUX ID	(1 Base)
	short	auxType;				//AUX Type (0 : 온도센서, 1:Voltage) //ljb 20140429 add 2:온도센서(Thermistor)	
	char	szName[128];			//Aux Name
	long	lMaxData;				//센서의 상한값		
	long	lMinData;				//센서의 하한값
	long	lEndMaxData;			//종료 상한값
	long	lEndMinData;			//종료 하한값
	//20090304 ljb add
	short int	funtion_division1;		//ljb 20090304 
	short int	funtion_division2;		//ljb 2010-06-23  온도 써미스터 Type 추가  : 201~250
	short int	funtion_division3;		//ljb 2010-06-23  온도 써미스터 Type 추가  : 201~250
	short int	reserved2;		
//}STF_MD_AUX_SET_DATA;
}STF_MD_AUX_SET_DATA_V1015;


//ksj 20200116 : V1016 구조체 추가.
typedef struct s_STF_CMD_AUX_SET_DATA{
//typedef struct s_STF_CMD_AUX_SET_DATA_V1016{
	unsigned char chNo;				//Channel Number (1 Base)
	unsigned char auxTempTableType;	//ljb 20160429  add 온도센서 타입(Thermistor)
	unsigned char vent_use_flag;
	unsigned char reserved1;
	short	auxChNo;				//AUX ID	(1 Base)
	short	auxType;				//AUX Type (0 : 온도센서, 1:Voltage) //ljb 20140429 add 2:온도센서(Thermistor)	
	char	szName[128];			//Aux Name
	long	lMaxData;				//센서의 상한값		
	long	lMinData;				//센서의 하한값
	long	lEndMaxData;			//종료 상한값
	long	lEndMinData;			//종료 하한값

	long	vent_upper;	//ksj 20200116 : v1016 vent 상한
	long	vent_lower; //ksj 20200116 : v1016 vent 하한
	short int	funtion_division1;		
	short int	funtion_division2;		
	short int	funtion_division3;		
	short int	reserved2;		
}STF_MD_AUX_SET_DATA;
//}STF_MD_AUX_SET_DATA_V1016;

//body of SFT_CMD_AUX_SET_DATA command
typedef struct s_STF_CMD_AUX_SET_DATA_A{
	
	unsigned char chNo;			//Channel Number (1 Base)
	unsigned char reserved[3];
	short	auxChNo;		//AUX ID	(1 Base)
	short	auxType;		//AUX Type (0 : 온도센서, 1:Voltage)	
	char	szName[32];		//Aux Name
	long	lMaxData;		//센서의 상한값
	long	lMinData;		//센서의 하한값
	long	lEndMaxData;	//종료 상한값
	long	lEndMinData;	//종료 하한값
	long	reserved1[2];
}STF_MD_AUX_SET_DATA_A;

#define SizeofNetChData()			(sizeof(SFT_CH_DATA))
#define SizeofNetChData_V1004()		(sizeof(SFT_CH_DATA_B))
#define SizeofNetAuxData()			(sizeof(SFT_AUX_DATA))
#define SizeofNetCanData()			(sizeof(SFT_CAN_DATA))
#define SizeofNetOvenData()			(sizeof(SFT_CHAMBER_VALUE))

//교정 Point 설정 
typedef struct s_CaliPointSet
{
	BYTE byValidPointNum;
	BYTE byCheckPointNum;
	BYTE byReserved[2];
	long lCaliPoint[15];
	long lCheckPoint[15];
} SFT_CALI_POINT; 

typedef struct s_SFT_CMD_CALI_START
{
	int				nType;				//voltage or current
	int				nRange;				//range 0,1,2...
	int				nMode;
	int				nSel;				//lmh 20120505 add CALI 용 충전 방전 모드 선택 flag(충전 : 0, 방전 : 4)
	SFT_CALI_POINT	pointData;
} SFT_CALI_START_INFO, *LPSFT_CALI_START_INFO;

typedef struct s_SFT_CMD_CALI_CHECK_RESULT
{
	int		nType;				//voltage or current
	BYTE	byRange;
	BYTE	reserved;
	BYTE	byValidCheckPointNum;
	BYTE	byChannelID;
	long	lCheckAD[15];
	long	lCheckDVM[15];
} SFT_CALI_CHECK_DATA;

//교정 완료후 수신되는 Data
typedef struct s_SFT_CMD_CALI_RESULT
{
	int		nType;				//voltage or current
	int		nSelType;			//lmh 20120504 충전:0	방전:4
	BYTE	byRange;	
	BYTE	byValidCalPointNum;
	BYTE	byValidCheckPointNum;
	BYTE	byChannelID;		//One Base
	long	lCaliAD[15];
	long	lCaliDVM[15];
	long	lCheckAD[15];
	long	lCheckDVM[15];
} SFT_CALI_END_DATA, *LPSFT_CALI_END_DATA;

typedef struct s_SFT_CMD_CALI_READY_DATA
{
	long	nCode;
}	SFT_CALI_READY_RESPONSE;

typedef struct s_SFT_CMD_CALI_READY
{
	long	nCode;
}	SFT_CALI_SET_READY_DATA;

typedef struct s_SFT_CMD_SCHEDULE_START
{
	short	int		byTotalStep;
	short	int		nPatternStepCount;	//ljb 20130404 add
	short	int		bWaitTimeGotoCount;	//ljb 20160427 add 지정시간 후 실행 Step 갯수
	short	int		reserved1;	
	long			reserved2[2];
}	SFT_STEP_START_INFO;

typedef struct s_SFT_CMD_EMERGENCY
{
	short int	lCode;
	short int	lValue;
	long		reserved;
	char		szName[128];
}	SFT_EMG_DATA;

//ljb 20140207 add for v100D
typedef struct s_SFT_CMD_SCHEDULE_END
{
	long lTestCond_File_Size;
	long lTestCond_File_CheckSum;
	long lWaitTimeGoto_File_Size;	//20160427 add	
	long lWaitTimeGoto_CheckSum;	//20160427 add
}	SFT_STEP_END_BODY;					


typedef struct tag_AutoReportSet {
	UINT nInterval;			//자동 보고 간격 
	BYTE reserved[4];		
} SFT_AUTO_REPORT_SET;


typedef struct tag_LineModeSet {
	BOOL bOnLineMode;	//0: OffLine,  1: OnLine 
	BOOL bControlMode;	//0: Maintenance Mode, 1: Control Mode 
	BYTE reserved[4];		
} SFT_LINE_MODE_SET;

//ljb 20100818 test safety data
typedef struct tag_SFT_CMD_SAFETY_DATA_V1 {
	long	lVtgLow;
	long	lVtgHigh;
	long	lVtgCellDelta;				//ljb 셀 전압편차 20150730 edit , 최소 전류 -> 2010-09-09 사용 하지 않음 추후 업그레이드 이용시 써도 됨
	long	lVtgCellHigh;				//yulee 20181114 Protocol 1013
	long	lCrtHigh;					//최대 전류
	long	lTempLow;	
	long	lTempHigh;                  //yulee 20181114 Protocol 1013
	long	lCapHigh;					//용량초과
	long	lWattHigh;					//전력초과
	long	lWattHourHigh;				//전력량초과

	short int can_function_division[10];		//분류 번호
	unsigned char can_compare_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char can_data_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	float	fcan_Value[10];

	short int aux_function_division[10];		//분류 번호
	unsigned char aux_compare_type[10];				//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char aux_data_type[10];				//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	float	faux_Value[10];

	long	lReserved[4];
} SFT_TEST_SAFETY_SET_V1; // LG v1015미만 (3구간 안전조건 미포함)


//ljb 20100818 test safety data
typedef struct tag_SFT_CMD_SAFETY_DATA_V2 {
	long	lVtgLow;
	long	lVtgHigh;
	long	faultCompAuxV;				//ljb 셀 전압편차 20150730 edit , 최소 전류 -> 2010-09-09 사용 하지 않음 추후 업그레이드 이용시 써도 됨
	long	faultCompAuxV2;
	long	faultCompAuxV3;		
	long	faultCompAux_Refv1;		
	long	faultCompAux_Refv2;	

	unsigned char bfaultcompAuxV_Vent_Flag;
	unsigned char reserved1[3];

	long	lVtgCellHigh;				//yulee 20181114 Protocol 1013
	long	lVtgCellLow;				//yulee 20181114 Protocol 1013
	long	lCrtHigh;					//최대 전류
	long	lTempLow;	
	long	lTempHigh;                  //yulee 20181114 Protocol 1013
	long	lCapHigh;					//용량초과
	long	lWattHigh;					//전력초과
	long	lWattHourHigh;				//전력량초과

	short int can_function_division[10];		//분류 번호
	unsigned char can_compare_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char can_data_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	float	fcan_Value[10];

	short int aux_function_division[10];		//분류 번호
	unsigned char aux_compare_type[10];				//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char aux_data_type[10];				//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : signed, 2: float
	float	faux_Value[10];

	long	lReserved[4];
} SFT_TEST_SAFETY_SET_V2; // LG v1015이상 안전조건 추가

//msec 저장용 channel data 구조 
typedef struct tag_SFT_CMD_MSEC_CH_DATA_HEADER {
	long	lTotCycNo;
	long	lStepNo;
	long	lDataCount;
} SFT_MSEC_CH_DATA_INFO;

//body = > { SFT_MSEC_CH_DATA * data 수 }
//Protocol Ver 0x1002
typedef struct tag_SFT_CMD_MSEC_CH_DATA_BODY {
	long	lSec;		//time
	long	lData1;		//Voltage
	long	lData2;		//current
	long	lData3;		//Capacity
	long	lData4;		//WattHour
} SFT_MSEC_CH_DATA, *LPSFT_MSEC_CH_DATA;

//Protocol Ver 0x1003
typedef struct tag_SFT_CMD_CONTROL {
	long	lStepNo;		// 0 or 현재 Step보다 작은값이면 즉시 Stop
	long	lCycleNo;		// 0 or 현재 Cycle보다 작은값이면 즉시 Stop
	long	lData1;
	long	lData2;
	long	lReserved[4];
} SFT_CONTROL_INFO;

//body of SFT_CMD_STEP_DATA_REQUEST command
typedef struct tag_SFT_CMD_STEP_DATA_REQUEST {
	long	lStepNo;
	long	lReserved[3];
} SFT_STEP_NO_INFO;

//#pragma push(pack:1)	//2byte 또는 1byte로 끊기는 것은 OS 마다 다르다... 좀 알아보자.. kjh
//body of SFT_CMD_PARALLEL_DATA command
typedef struct s_SFT_CMD_PARALLEL_DATA{

	unsigned char chNoMaster;	//Master Channel		(1 Base)
	unsigned char chNoSlave[3];	//3개까지 Slave 가능	(1 Base)
	unsigned char bParallel;	//0:단독, 1:병렬
	unsigned char reserved[3];	
	//reserved
	//[0]: chamber_control, 0:None 1:PC control
	//[1]can_comm_check, 0:non-check, 1:M+Scheck, 2:M, 3:S
	//[2]chamber_standby, 0:non-standby, 1:standby
	unsigned char reserved2[4];	//ljb 20151112 김재호 추가 요청 (MUX 개발 중)
	//reserved2
	//[0]: out_mux_use
	//[1]chiller_control, 0:none, 1:pc_control
	//[2]~[3] reserved
}SFT_MD_PARALLEL_DATA;

typedef struct s_STF_CMD_USERPATTERN_DATA{
	long lStepNo;			//User Pattern 이 사용된 스텝 번호 
	long lLength;			//동적으로 길이가 변한다.
	char * pPatternData;	//패턴 데이터는 동적이다.
	char reserved[4];
}STF_MD_USERPATTERN_DATA;

typedef struct s_SFT_CMD_SYNC_TIME_RESPONE{
	char	szSyncTime[24];				//현재 데이터에 대한 시간정보 YYYYMMDD HHMMSS.mmm 변환 가능한 시간 20080723 214055.123	
}SFT_MD_SYNC_TIME_RESPONE;
/*
//2014.12.23 챔버 연동 대기 모드 추가로 변경.
typedef struct s_USING_CHAMBER{
	BYTE	bfulseMode;			// 0:챔버연동 안함 , 1:챔버연동
	BYTE	bChamberStand;		// 1 :챔대기 모드  0 : 챔버 대기 하지않음.
	BYTE	bReserved[2];
}SFT_USING_CHAMBER;
*/
//2019.01.14 칠러연동 Flag전송을 위해 변경.
typedef struct s_USING_CHAMBER{
	BYTE	bfulseMode;			// 0:챔버연동 안함 , 1:챔버연동
	BYTE	bChamberStand;		// 1 :챔대기 모드  0 : 챔버 대기 하지않음.
	BYTE	bChillerControl;	// 0: 연동 안함, 1:칠러 연동
	BYTE	bReserved;
}SFT_USING_CHAMBER;

//body of SFT_CMD_DAQ_ISOLATION_REQUEST command
// typedef struct tag_SFT_CMD_DAQ_ISOLATION_REQUEST {
// 	BYTE	bISO;		// 0: 비절연 실행,   1: 절연 실행
// 	BYTE	bReserved[3];
// } SFT_DAQ_ISOLATION;

typedef struct tag_SFT_CMD_DAQ_ISOLATION_REQUEST {
	BYTE bISO;  // 0: 비절연 실행,   1: 절연 실행
	BYTE div_ch;     // 출력 채널 정보 1,2,3   //commented by ksj 20210105 : (일반 절연시에는 충방전기 채널을 뜻함, 분기절연시에는 a,b 절연 구분시 사용)
	BYTE div_MainCh;    //채널이 2개 일 때에 채널을 구분 //commented by ksj 20210105 : (일반 절연시에는 미사용, 분기절연시에는 충방전기 채널을 뜻함)
	BYTE bReserved;
} SFT_DAQ_ISOLATION;

//body of SFT_CMD_CHANNEL_IO_SET command
typedef struct tag_SFT_CMD_CHANNEL_IO_SET_REQUEST {
	BYTE	bType;		// 0: none,   1: key on, 2: charge on, 3:Pack Relay
	BYTE	bValue;		// 0: OFF, 1: ON
	BYTE	bReserved[2];
} SFT_CHANNEL_IO_SET;

//yulee 20181120 RT_TABLE_UPLOAD_END
//body of SFT_LIN_CAN_SELECT_REQUEST command
typedef struct tag_SFT_CMD_RT_TABLE_UPLOAD_END_REQUEST {
	unsigned char TableNum;    //0: 1:END
	unsigned char reserved[3]; //reserved
} SFT_RT_TABLE_SEND_END;

//yulee 20181120 RT_TABLE_UPLOAD_RESPONSE
//body of SFT_LIN_CAN_SELECT_RESPONSE command
typedef struct tag_SFT_CMD_RT_TABLE_UPLOAD_END_RESPONSE {
	unsigned char nRetFlag;    //0: 1:OK
	unsigned char reserved[3]; //reserved
} SFT_RT_TABLE_SEND_RESPONSE;

//ksj 20200207
typedef struct tag_SFT_CMD_RT_HUMI_TABLE_UPLOAD_END_REQUEST {
	unsigned char TableNum;    //0: 1:END
	unsigned char reserved[3]; //reserved
} SFT_RT_HUMI_TABLE_SEND_END;
//ksj 20200207
typedef struct tag_SFT_CMD_RT_HUMI_TABLE_UPLOAD_END_RESPONSE {
	unsigned char nRetFlag;    //0: 1:OK
	unsigned char reserved[3]; //reserved
} SFT_RT_HUMI_TABLE_SEND_RESPONSE;

//ljb 20151023 MUX command
//body of SFT_CMD_MUX_SELECT_COMM_REQUEST command
typedef struct tag_SFT_CMD_MUX_SELECT_COM_REQUEST {
	BYTE	out_mux; //0: all_off, 1:mux_a, 2:mux_b
	BYTE	bReserved[2];
	BYTE    command;	//ljb 20160122 add => 1:MUX, 2:LOAD
} SFT_MUX_SELECT;

//yulee 20181114 LIN/CAN command
//body of SFT_LIN_CAN_SELECT_REQUEST command
// typedef struct tag_SFT_LIN_CAN_SELECT_REQUEST {
// 	unsigned char SelectMode;    //0: CAN 1:LIN
// 	unsigned char reserved[3]; //reserved
// } SFT_LINCAN_SELECT;

//body of SFT_CMD_MUX_SELECT_COMM_REQUEST command
typedef struct tag_SFT_CMD_MUX_SELECT_COMM_RESPONE {
	UCHAR	chNum;		//cha1, ch2 ~
	UCHAR	response;	//0: NG, 1:OK
	UCHAR	setting;
	UCHAR	state;
	UCHAR	fault_flag;
	UCHAR	reserved[3];
} SFT_MUX_SELECT_RESPONSE;

//ljb 20151214 GOTO command
//body of SFT_CMD_GOTO_COMM_REQUEST command
typedef struct tag_SFT_CMD_GOTO_COMM_REQUEST {
	unsigned short	user_branch_stepNo; //1 base
	unsigned short	reserved;
	ULONG			lReserved[3];
} SFT_GOTO_STEP_COMM;

//ljb 20170731 add
//body of SFT_CMD_DAQ_ISOLATION_REQUEST command
typedef struct tag_SFT_CMD_STEP_INFO_REQUEST {
	long	lStpepNo;		// 0 base
	long	lReserved[3];
} SFT_STEP_INFO_REQUEST;

//업데이트 response와 request 할때의 body
typedef struct s_SFT_CMD_SAFETY_UPDATE_REPLY
{
	SFT_STEP_START_INFO		sStepStartInfo;
	SFT_TEST_SAFETY_SET_V2		sTestSafetySet; 
} SFT_CMD_SAFETY_UPDATE_REPLY;

typedef struct s_SFT_CMD_SCHEDULE_UPDATE_BODY
{
	SFT_STEP_INFO_REQUEST	sStepInfo;
	SFT_STEP_CONDITION_V3		sTestSchedule; //lyj 20200214 LG v1015 이상
} SFT_CMD_SCHEDULE_UPDATE_BODY;

typedef struct CanReceiveIndexDecimaltoHex{
	short usIndex;
	bool isChecked;
} CAN_RECEIVE_INDEX_DE_TO_HEX;


/*typedef struct tag_SFT_CMD_VENT_CLEAR {
	BYTE bReserve0;  
	BYTE bReserve1;    
	BYTE bReserve2;    
	BYTE bReserve3;
} SEND_CMD_VENT_CLEAR;*/

//ksj 20200122 : v1016. vent clear 명령 변경. open 기능 추가.
typedef struct tag_SFT_CMD_VENT_CLEAR {
	BYTE bCh; //미사용  
	BYTE bCloseOpenFlag; // 0:close, 1:open    
	BYTE bReserve2;    
	BYTE bReserve3;
} SEND_CMD_VENT_CLEAR;
//ksj end

typedef struct tag_SFT_CMD_CBANK_ON {
	BYTE flag;     
	BYTE breserved[3];
} SEND_CMD_CBANK_ON;

//ksj 20201222 : EMG 구조체 추가.
typedef struct tag_UserEMG {
	CHAR	command;			
	CHAR	reserve[3];			
} SFT_USER_EMG, *LPSFT_USER_EMG;

//////////////////////////////////////////////////////////////////////////
// Command Define start
//////////////////////////////////////////////////////////////////////////

//common command(0x0000~0xFFF)
//System Command
#define SFT_CMD_NONE				0x00000000
#define SFT_CMD_RESPONSE			0x00000001		//Command response 
#define SFT_CMD_SHUTDOWN			0x00000002		//Module shutdown Command
#define SFT_CMD_SYSTEM_INIT			0x00000003		//(PC <- SBC)   Module Init
#define SFT_CMD_HEARTBEAT			0x00000004		//(PC <- SBC)	Network State Check  Send Ack 
#define SFT_CMD_EMERGENCY			0x00000005

#define SFT_CMD_INFO_REQUEST		0x00000006		//(PC -> SBC) Version Data request	//Ack SFT_AMD_VER_DATA
#define SFT_CMD_INFO_DATA			0x00010006		//(PC <- SBC) Version Data Response

#define SFT_CMD_AUX_INFO_REQUEST	0x00000007		//(PC -> SBC) Installed Aux Data request	//Ack SFT_AMD_VER_DATA
#define SFT_CMD_AUX_INFO_DATA		0x00010007		//(PC <- SBC) Version Data Response

#define SFT_CMD_AUX_SET_DATA		0x00000008	//(PC -> SBC)
#define SFT_CMD_PARALLEL_DATA		0x00000009	//(PC -> SBC)

#define SFT_CMD_CH_ATTRIBUTE_REQUEST	0x0000000A //(PC -> SBC)		
#define SFT_CMD_CH_ATTRIBUTE_REPLY		0x0001000A //(PC <- SBC)

#define SFT_CMD_CAN_SET_DATA		0x0000000B		//(PC -> SBC)	CAN Receive setting Send
#define SFT_CMD_CAN_INFO_REQUEST	0x0000000C
#define SFT_CMD_CAN_INFO_RESPONE	0x0001000C
#define SFT_CMD_CAN_SET_TRANS_DATA	0x0000000D		//(PC -> SBC)	CAN Transmit setting Send
#define SFT_CMD_CAN_TRANS_INFO_REQUEST	0x0000000E
#define SFT_CMD_CAN_TRANS_INFO_RESPONE	0x0001000E

#define SFT_CMD_CHANNEL_CAN_SET_DATA		0x0000000F		//(PC -> SBC)	CAN Receive setting Send
#define SFT_CMD_CHANNEL_CAN_SET_TRANS_DATA	0x00000010		//(PC -> SBC)	CAN Transmit setting Send

//special command(0x1000~)
//Control command	
#define SFT_CMD_RUN						0x00001000		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_STOP					0x00001001		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_PAUSE					0x00001002		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_CONTINUE				0x00001003		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_CLEAR					0x00001004		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_NEXTSTEP				0x00001005		//(PC -> SBC)	//Receive Ack

//special command(0x100B)
//Control command	
#define SFT_CMD_CHAMBER_USING			0x00001006		//(PC -> SBC)	//Receive Ack	//ljb 20100322 챔버 연동 
#define SFT_CMD_CHAMBER_CONTINUE		0x00001007		//(PC -> SBC)	//Receive Ack	//ljb 20100322 챔버 연동 
#define SFT_CMD_CABLE_CHECK_H			0x00001008		//(PC -> SBC)	//Receive Ack	//ljb 20100430 셀체크 (전류 인가)				//Ack
#define SFT_CMD_CABLE_CHECK_H_REPLY		0x00011008		//(PC <- SBC)	//Receive Ack	//ljb 20100430 셀체크 (전류 인가) Reply
#define SFT_CMD_CABLE_CHECK_S			0x00001009		//(PC -> SBC)	//Receive Ack	//ljb 20100430 셀체크 (전류 인가 안함)			//Ack
#define SFT_CMD_CABLE_CHECK_S_REPLY		0x00011009		//(PC <- SBC)	//Receive Ack	//ljb 20100430 셀체크 (전류 인가 안함) Reply
#define SFT_CMD_CYCLE_PAUSE_CONTINUE	0x0000100A		//(PC -> SBC)	//Receive Ack	//ljb 20100509 Cycle 반복 후 Pause한 채널 Continue	//Ack
#define SFT_CMD_CAN_MODE_CHECK			0x0000100B		//(PC -> SBC)	//SEND			//2014.12.03   캔 모드 연속 진행 
#define SFT_CMD_MUX_MODE_CHECK			0x0000100C		//(PC -> SBC)	//SEND			//2015.11.10   MUX 연동 체크 (0:non-use, 1:use)
#define SFT_CMD_GOTOSTEP				0x0000100D		//(PC -> SBC)	//Receive Ack	//20151212 ADD  이동 STEP

//Data Sending command
#define SFT_CMD_CH_DATA							0x00001010		//(PC <- SBC)	Auto Group Data	(Send ACK) 
#define SFT_CMD_STATE_DATA						0x00001011		//(PC <- SBC)	Auto Group State Data	(Send ACK) 
#define SFT_CMD_MD_SET_DATA						0x00001012		//(PC -> SBC)	//SFT_MD_SET_DATA
#define SFT_CMD_USER_EMG						0x0000101D				//ksj 20201222 : EMG 커맨드 전송.

//Schedule data sending command
#define SFT_CMD_SCHEDULE_START					0x00001020		//(PC -> SBC)	send Test Header Data
#define SFT_CMD_STEP_DATA						0x00001021		//(PC -> SBC)	send Test Step Data
#define SFT_CMD_SCHEDULE_END					0x00001022		//(PC -> SBC)	send Test Header Data

//2006/3/24 ver 0x1002 cmd
#define SFT_CMD_SAFETY_DATA						0x00001023		//(PC -> SBC)	send test safty condition
#define SFT_CMD_MSEC_CH_DATA					0x00001013		//(PC <- SBC)	Auto msec data	(Send ACK) 

//2006/6/9 Ver 0x1003 CMD	-> 20170728 권준구 다시 구현
#define SFT_CMD_STEP_DATA_REQUEST				0x00001024	//(PC ->SBC)	
#define SFT_CMD_STEP_INFO						0x00011024	//(PC <- SBC)	response of SFT_CMD_STEP_DATA_REQUEST

//2006/6/9 Ver 0x1003 CMD	-> 20170728 권준구 다시 구현
#define SFT_CMD_STEP_DATA_UPDATE				0x00001025	//(PC ->SBC )	//ACK


//2006/6/9 Ver 0x1003 CMD	-> 20170728 권준구 다시 구현
#define SFT_CMD_SAFETY_DATA_REQUEST				0x00001026	//(PC ->SBC)	
#define SFT_CMD_SAFETY_INFO						0x00011026	//(PC ->SBC)	response of SFT_CMD_SAFETY_DATA_REQUEST
#define SFT_CMD_SAFETY_DATA_UPDATE				0x00001027	//(PC ->SBC)	//ACK

#define SFT_CMD_RESET_RESERVED_CMD				0x00001028	//(PC ->SBC)	//ACK

//ljb 20170728 add
#define SFT_CMD_TESTCOND_UPDATE_START			0x00001029	//(PC ->SBC)	
#define SFT_CMD_TESTCOND_UPDATE_END				0x0000102A	//(PC ->SBC)	

#define SFT_CMD_RUN_QUEUE						0x00001099 //ksj 20160613 실제 전송하는 명령어가 아님. 작업 예약시 조건 체크에만 사용. 값이 다른 명령어랑 중복된다면 변경해도 된다.
//2007/5/18 Ver 0x1004CMD
#define SFT_CMD_USERPATTERN_DATA				0x00001040	//(PC -> SBC)
#define SFT_CMD_TESTCOND_UPDATE_END_CONFIRM		0x00001041	//(SBC -> PC)	스케쥴 업데이트 END 명령 수신 후 SBC 시랭 여부 확인

//2007/7/31 ver 0x1004 cmd
#define SFT_CMD_VARIABLE_CH_DATA				0x00001014		//(PC <- SBC)	send Variable data ->Add Aux Data

//20140308 ver 0x100E cmd
#define SFT_CMD_VARIABLE_CH_DATA_SAVE			0x00001015		//(PC <- SBC)	save 전용 데이터 1초 단위로 전송 send Variable data ->Add Aux Data

//2008/7/23 Ver 0x1005CMD
#define SFT_CMD_SYNC_TIME_RESPONE				0x00001050	//(PC -> SBC)	PC Time을  SBC로 전송
#define SFT_CMD_SYNC_TIME_REQUEST				0x00011050	//(PC <- SBC)	Time 동기화 요청

//2008/9/03 BMS MCU Temperature	-> 20090202 BMS command command		ljb
#define SFT_CMD_BMS_COMM_REQUEST				0x00001060		//( PC-> SBC) command Send  SFT_MD_CAN_TRANS_INFO_DATA
#define SFT_CMD_BMS_COMM_RESPONE				0x00011060		//( PC <- SBC) Command response  SFT_MD_CAN_TRANS_INFO_DATA

#define SFT_CMD_BMS_COMM_RESULT					0x00001061		//20090209 ljb SBC -> PC (ACK 안보냄)
#define SFT_CMD_BMS_CHANGE						0x00001062		//ljb 20090910 ljb SBC -> PC (ACK 보냄)

#define SFT_CMD_DAQ_ISOLATION_COMM_REQUEST		0x00001070		//( PC-> SBC) command Send  SFT_MD_CAN_TRANS_INFO_DATA
#define SFT_CMD_DAQ_ISOLATION_COMM_RESPONE		0x00011070		//( PC <- SBC) Command response  SFT_MD_CAN_TRANS_INFO_DATA

#define SFT_CMD_OUTPUT_CH_CHANGE_REQUEST		0x00001072		//lyj 20201203 A,B분기 요청
#define SFT_CMD_OUTPUT_CH_CHANGE_RESPONE		0x00011072		//lyj 20201203 A,B분기 요청

//ljb 20151023 MUX command
#define SFT_CMD_MUX_SELECT_COMM_REQUEST			0x00001073
#define SFT_CMD_MUX_SELECT_COMM_RESPONE			0x00011073

//yulee 20181114 LIN CAN Select command
//#define SFT_CMD_LIN_CAN_SELECT_REQUEST			0x00001075      //( PC-> SBC) command Send LIN(check), CAN(uncheck)

//yulee 20181120
#define SFT_CMD_RT_TABLE_UPLOAD_END_REQUEST		0x00001076		//( PC-> SBC) command Send
#define SFT_CMD_RT_TABLE_UPLOAD_END_RESPONSE	0x00011076		//( PC-> SBC) command response

//ksj 20200207
#define SFT_CMD_RT_HUMI_TABLE_UPLOAD_END_REQUEST		0x00001077		//( PC-> SBC) command Send
#define SFT_CMD_RT_HUMI_TABLE_UPLOAD_END_RESPONSE		0x00011077		//( PC-> SBC) command response

//ljb 201012
#define SFT_CMD_CHANNEL_IO_SET					0x00001080		//( PC-> SBC) command Send  (ACK 보냄)

//ljb 20110101
#define SFT_CMD_STOP_BUZZER						0x00001090		//( PC-> SBC) command Send  (ACK 보냄)
#define SFT_CMD_RESET_ALARM						0x00001091		//( PC-> SBC) command Send  (ACK 보냄)
//ljb 20130130
#define SFT_CMD_CALI_START_END					0x00001092		//( PC-> SBC) command Send  (ACK ??) //ksj 20200819 : 커맨드 이식

#define SFT_CMD_LOAD_TYPE_SET_COMM_REQUEST		0x00001093		//ljb 20160122 add LOAD TYPE SET

#define SFT_CMD_VENT_CLEAR						0x00001094		

#define SFT_CMD_CBANK							0x00001095		//LJH 20191014 김재호 차장님 요청사항 CBANK 기능 추가

//#define SFT_CMD_RT_TABLE_UPLOAD_END_RESPONSE	0x00011077		//LJH 20191014 김재호 차장님 요청사항 CBANK 기능 추가(response)
#define SFT_CMD_CBANK_RESPONSE					0x00011095		//ksj 20200123 : CBANK Reply 오류 수정. 확인 필요.. 이상함



//Calibration command
#define SFT_CMD_CALI_READY			0x00001030		//20051123 삭제됨 KBH
#define SFT_CMD_CALI_READY_DATA		0x00011030		//20051123 삭제됨 KBH
#define SFT_CMD_CALI_POINT			0x00001031		//20051123 삭제됨 KBH
#define SFT_CMD_CALI_START			0x00001032		//(PC -> SBC)	//Receive Ack
#define SFT_CMD_CALI_RESULT			0x00001033		//(PC <- SBC)	//Receive Ack
#define SFT_CMD_CALI_CHECK_RESULT	0x00001034
#define SFT_CMD_CALI_UPDATE			0x00001035		//(PC -> SBC)	//Receive Ack

#define SFT_CMD_SET_AUTO_REPORT		0x00002000		//(PC -> SBC)	AutoReport Setting //2003/1/14 Receive Ack
#define SFT_CMD_SET_LINE_MODE		0x00002001		//(PC -> SBC)
#define SFT_CMD_LINE_MODE_REQUEST	0x00002002		//(PC -> SBC)
#define SFT_CMD_LINE_MODE_DATA		0x00012002		//(PC <- SBC)	Response of SFT_CMD_LINE_MODE_REQUEST


//20140211 ver 0x100D CMD add
#define SFT_CMD_FTP_END_RESPONSE			0x00001040		
#define SFT_CMD_FTP_END_OK_RESPONSE			0x00001042	//ljb 20140211 add PSServer.dll -> Monitor		
#define SFT_CMD_FTP_END_NG_RESPONSE			0x00001043	//ljb 20140211 add PSServer.dll -> Monitor


// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지
#define SFT_CMD_CH_OUT_CABLE_ERROR			0x00000204	// 작업 시작 전 파워케이블 체크 기능이 켜져있을 때 조건 감지되면 해당 ERROR 수신(공정 시작 실패)
#define SFT_CMD_CD_FAULT_OUT_CABLE_CHECK	0x00000101	// 공정 중 파워케이블 조건 감지시 해당 코드 수신(일시정지)

//////////////////////////////////////////////////////////////////////////
// Command Define end
//////////////////////////////////////////////////////////////////////////


//Define of module kind
#define SFT_ID_FORM_MODULE		0x01
#define SFT_ID_FORM_OP_SYSTEM	0x02
#define SFT_ID_IROCV_SYSTEM		0x03
#define SFT_ID_GRADING_SYSTEM	0x04
#define SFT_ID_DATA_SERVER		0x05
#define SFT_ID_CYCLER			0x06

//response of command code
#define SFT_NACK				0x00000000
#define SFT_ACK					0x00000001
#define SFT_TIMEOUT				0x00000002
#define SFT_SIZE_MISMATCH		0x00000003
#define SFT_RX_BUFF_OVER_FLOW	0x00000004
#define SFT_TX_BUFF_OVER_FLOW	0x00000005
#define SFT_CONNECTED			0x00000006	
#define SFT_FAIL				0xFFFFFFFF


//Size 함수 
#define SizeofCHData()	sizeof(SFT_CH_DATA)
#define SizeofHeader()	sizeof(SFT_MSG_HEADER)

#endif //_SFT_SCHEDULER_MSG_DEFINE_H_



