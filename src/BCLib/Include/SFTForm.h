///////SFT SCHEDULER_API Define//////////////
//											//	
//	SFTForm.h								//	
//	Softech Formation System API			//	
//	define of Scheduler.dll					//
//	Byung Hum - Kim		2004.1.13			//
//											//
//////////////////////////////////////////////

#ifndef _SFT_SCHEDULER_API_DEFINE_H_
#define _SFT_SCHEDULER_API_DEFINE_H_
#include "PowerSys.h"						
#include "Scheduler.h"				//server.h포함 
#include "../src/PSServer/Module.h"		//channel.h 포함 

#ifdef __cplusepluse
extern "C" {
#endif ///__cplusepluse

#ifndef __SFT_FORM_DLL__
#define __SFT_FORM_DLL__

#ifdef EXPORT_DLL_API
#define SFT_DLL_APT	__declspec(dllexport)
#else	//EXPORTSFT_DLL_API
#define SFT_DLL_APT	__declspec(dllimport)
#endif	//EXPORTSFT_DLL_API

#endif	//__SFT_FORM_DLL__

SFT_DLL_APT BYTE	SFTGetReservedCmd(int nModuleID, int nChIndex);						//2006/06/08	KBH

SFT_DLL_APT	int		SFTGetInstalledModuleNum();											//2005/06/08	KBH
SFT_DLL_APT	long	SFTGetChannelValue(int nModuleID, int nChannelIndex, int nItem);	//2005/06/08	KBH
SFT_DLL_APT	int		SFTGetModuleIndex(int nModuleNumber);								//2005/06/08	KBH
SFT_DLL_APT	CChannel	* SFTGetChannelData(int nModuleID, int nChannelIndex);			//2005/06/08	KBH
SFT_DLL_APT	BOOL	SFTServerStart();
	
//SFT_DLL_APT	SFT_STR_MODULE_DATA* SFTGetModuleData(int nModuleIndex);
SFT_DLL_APT	WORD	SFTGetChannelState(int nModuleID, int nChannelIndex);
SFT_DLL_APT	WORD	SFTGetGroupState(int nModuleID, int nGroupIndex);
SFT_DLL_APT	LPSTR	SFTGetIPAddress(int nModuleID);
SFT_DLL_APT	BOOL	SFTSetSysParam(int nModuleIndex/*ZeroBase of Array*/, int nModuleID/*One Base*/, SFT_SYSTEM_PARAM *pParam);
SFT_DLL_APT	SFT_SYSTEM_PARAM SFTGetSysParam(int nModuleIndex);
//★★ ljb 20091112 S	★★★★★★★★
SFT_DLL_APT BOOL	SFTSetChamberData(int nModuleID /*Zero Base*/,  int nChannelIndex /*One Base*/,SFT_CHAMBER_VALUE *pChamberData);
SFT_DLL_APT SFT_CHAMBER_VALUE * SFTGetChamberData(int nModuleID, int nChannelIndex);

//★★ ljb 20150427 S	★★★★★★★★
SFT_DLL_APT BOOL	SFTSetLoaderData(int nModuleID /*Zero Base*/,  int nChannelIndex /*One Base*/,SFT_LOADER_VALUE *pLoaderData);
SFT_DLL_APT SFT_LOADER_VALUE * SFTGetLoaderData(int nModuleID, int nChannelIndex);

//★★ ljb 20170906 S	★★★★★★★★
SFT_DLL_APT BOOL	SFTSetPwrSupplyData(int nModuleID /*Zero Base*/,  int nChannelIndex /*One Base*/,SFT_PWRSUPPLY_VALUE *pPwrSupplyData);
SFT_DLL_APT SFT_PWRSUPPLY_VALUE * SFTGetPwrSupplyData(int nModuleID, int nChannelIndex);

//★★ ljb 20170906 S	★★★★★★★★
SFT_DLL_APT BOOL	SFTSetChillerData(int nModuleID /*Zero Base*/,  int nChannelIndex /*One Base*/,SFT_CHAMBER_VALUE *pChillerData);
SFT_DLL_APT SFT_CHAMBER_VALUE * SFTGetChillerData(int nModuleID, int nChannelIndex);

//★★ ljb 20170906 S	★★★★★★★★
SFT_DLL_APT BOOL	SFTSetCellBALData(int nModuleID /*Zero Base*/,  int nChannelIndex /*One Base*/,SFT_CELLBAL_VALUE *pCellBALData);
SFT_DLL_APT SFT_CELLBAL_VALUE * SFTGetCellBALData(int nModuleID, int nChannelIndex);

SFT_DLL_APT SFT_VARIABLE_CH_DATA * SFTGetRealChData(int nModule, int nChannelIndex /*One Base*/);
SFT_DLL_APT SFT_CHAMBER_VALUE * SFTGetRealChamberData(int nModule, int nChannelIndex /*One Base*/);
//★★ ljb 20091112 E ★★★★★★★★
SFT_DLL_APT	BYTE	SFTGetChCode(int nModuleID, int nChannelIndex);
SFT_DLL_APT	BOOL	SFTInitSysParam(int nModuleIndex,  SFT_SYSTEM_PARAM *pParam);
SFT_DLL_APT	int		SFTGetGroupCount(int nModuleID);
SFT_DLL_APT	WORD	SFTGetChInGroup(int nModuleID, int nGroupIndex);
SFT_DLL_APT	SFT_MD_SYSTEM_DATA * SFTGetModuleSysData(int nModuleIndex);

SFT_DLL_APT	SFT_MD_AUX_INFO_DATA * SFTGetAuxInfoData(int nModuleIndex);				//2007/08/08	KJH
SFT_DLL_APT SFT_AUX_DATA * SFTGetAuxData(int nModuleID, int nChannelIndex);			//2007/08/08	KJH
SFT_DLL_APT SFT_MD_PARALLEL_DATA * SFTGetParallelData(int nModuleID);				//2007/08/08	KJH
SFT_DLL_APT	SFT_MD_CAN_INFO_DATA * SFTGetCANInfoData(int nModuleIndex);				//2007/10/16	KJH
SFT_DLL_APT	SFT_MD_CAN_TRANS_INFO_DATA * SFTGetCANTransInfoData(int nModuleIndex);	//2008/08/13	LJB

//SFT_DLL_APT	SFT_GP_DATA SFTGetGroupData(int nModuleID, int nGroupIndex);
SFT_DLL_APT	WORD	SFTTrayState(int nModuleID, int nGroupIndex);
SFT_DLL_APT	WORD	SFTJigState(int nModuleID, int nGroupIndex);
SFT_DLL_APT	WORD	SFTDoorState(int nModuleID, int nGroupIndex);

SFT_DLL_APT	void	SFTSetLogFileName(char *szFileName);
SFT_DLL_APT	int		SFTGetInstalledBoard(int nModuleID);
SFT_DLL_APT	int		SFTGetChPerBoard(int nModuleID);
SFT_DLL_APT	UINT	SFTInstledlTotalGroup();
SFT_DLL_APT	BOOL	SFTSetAutoProcess(int nModuleID, BOOL bEnable);

//SFT_DLL_APT	BOOL	SFTGetAutoProcess(int nModuleID);
SFT_DLL_APT	BYTE	SFTGetGroupFailCode(int nModuleID, int nGroupIndex);
SFT_DLL_APT	BOOL	SFTSetGroupState(int nModuleID, int nGroupIndex, WORD newState);
SFT_DLL_APT	void	SFTSetChMappingType(int nType);
SFT_DLL_APT	CString SFTCmdFailMsg(int nCode);
SFT_DLL_APT	UINT	SFTGetHWChIndex(UINT nIndex);


SFT_DLL_APT int		SFTDllGetVersion();
SFT_DLL_APT BOOL	SFTSetAutoReport(int nModuleID, int nGroupNo, UINT nInterval);
SFT_DLL_APT BOOL	SFTSetLineMode(int nModuleID, int nGroupNo, int nOnLineMode, int nControlMode);
SFT_DLL_APT BOOL	SFTGetLineMode(int nModuleID, int nGroupNo, int &nOnLineMode, int &nControlMode);
SFT_DLL_APT UINT	SFTGetProtocolVer(int nModuleID);
SFT_DLL_APT WORD	SFTGroupCode(int nModuleID, int nGroupIndex);

//
SFT_DLL_APT char	*SFTGetErrorString(int nErrorCode);
SFT_DLL_APT	int		SFTGetLastError();
SFT_DLL_APT	CModule * SFTGetModule(int nModuleID);
SFT_DLL_APT int		SFTSendCommand(UINT nModuleID, UINT nCmd, LPSFT_CH_SEL_DATA lpChSelData = NULL, LPVOID lpData = NULL, UINT nSize = 0, UINT nWaitTime = 0);
SFT_DLL_APT LPVOID	SFTSendDataCmd(UINT nModuleID, UINT nCmd, LPSFT_CH_SEL_DATA lpChSelData, UINT nReadSize, LPVOID lpData = NULL, UINT nSize = 0);
SFT_DLL_APT	BOOL	SFTOpenFormServer(int nInstalledModuleNo = 5, HWND hMsgWnd = NULL, int iPType = 0);//yulee 20190705 강제병렬 옵션 처리 Mark
SFT_DLL_APT	BOOL	SFTCloseFormServer();
SFT_DLL_APT	int		SFTGetModuleID(int nModuleIndex);
SFT_DLL_APT	WORD	SFTGetModuleState(int nModuleID);
SFT_DLL_APT	SYSTEMTIME * SFTGetConnectedTime(int nModuleID);
SFT_DLL_APT void	SFTInitChannel(int nModuleID, int nChIndex);

#ifdef __cplusepluse
}	// extern "C" {
#endif //__cplusepluse

#endif //_SFT_SCHEDULER_API_DEFINE_H_