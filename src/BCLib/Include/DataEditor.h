#ifndef _DATAFORM_
#define _DATAFORM_

#include "PowerSys.h"

#define SCH_MAX_COMP_POINT	3
#define SCH_MAX_END_CAN_AUX_POINT	10		//ljb 2011221 이재복 //////////

#define SCH_MAX_GRADE_STEP	10
#define SCH_STEP_ALL		-1
#define SCH_MAX_STEP		200

//#define SCH_UNIT_MILI		0

#define SCH_MAX_ID_SIZE		16
#define SCH_MAX_PWD_SIZE	16
#define SCH_DATE_TIME_SIZE		32
#define SCH_USER_NAME_SIZE		32
#define SCH_DESCRIPTION_SIZE	128	

#define SCH_MAX_CODE_SIZE	64
#define SCH_MAX_MSEC_DATA_POINT	600

typedef struct s_LoginInformation 
{
	char	szLoginID[SCH_MAX_ID_SIZE];
	char	szPassword[SCH_MAX_PWD_SIZE];
	long	nPermission;
	char	szRegistedDate[SCH_DATE_TIME_SIZE];
	char	szUserName[SCH_USER_NAME_SIZE];
	char	szDescription[SCH_DESCRIPTION_SIZE];
	BOOL	bUseAutoLogOut;	
	ULONG	lAutoLogOutTime;
} SCH_LOGIN_INFO, STR_LOGIN;

typedef struct s_ChannelCodeMessage {
	int nCode;
	int nData;
	char szMessage[SCH_MAX_CODE_SIZE];
} SCH_CODE_MSG;

typedef struct s_TestInformation {
	LONG	lID;
	char	szName[64];
	char	szDescription[128];
	char	szCreator[64];
	char	szModifiedTime[32];
} SCH_TEST_INFORMATION;



#if SCH_MAX_COMP_POINT < 3
#pragma message( "------------KBH Message-------------" )
#pragma message( "comparison Voltage Data Point Define Error" )
#pragma message( "------------------------------------" )
#error Least 3 Point
#endif


typedef struct tag_GRADE_CON {
//20081208 KHS
long	lGradeItem[SCH_MAX_GRADE_STEP*2];
BYTE	chTotalGrade;
float	faValue1[SCH_MAX_GRADE_STEP*2];
float	faValue2[SCH_MAX_GRADE_STEP*2];
char	aszGradeCode[SCH_MAX_GRADE_STEP*2];
UINT	iGradeCount;
}	GRADE;

typedef struct tag_STEP_CON {
	//Step Header
	unsigned char	chStepNo;	//ljb 20150609 edit	char => unsigned char
	char	chType;
	int		nProcType;
	char	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;				//ljb 20100820 for v100A
	float	fRref;				//ljb 20100820 for v100A

	//Chamber 조건
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	BOOL	bRunProgMode;		//ljb 201010   챔버 프로그램 모드로 운영
	BOOL    bStepChamberStop;   //yulee 20180828
	
	//End 조건
	ULONG	ulEndTimeDay;			//ljb 20131212 add Day 단위 입력 
	ULONG	ulEndTime;			//10msec 단위 입력 
	float	fEndV_H;			//ljb v1009
	float	fEndV_L;			//ljb v1009
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fEndW;
	float	fEndWh;
	float	fEndTemp;					//ljb SBC가 챔버와 연동할때 종료 챔버온도
	float	fStartTemp;					//ljb SBC가 챔버와 연동할때 시작 챔버온도
	ULONG	ulCVTimeDay;			//ljb 20131212 add Day 단위 입력 
	ULONG	ulCVTime;
	
	//SOC 종료 조건//20051208
	int		nValueRateItem;		//0: 사용안함 , 1: SOC,	 2: WhattHour  //v1009 : Cycle 누적 용량 부호 (0:모두, 1:충전, 2:방전)
	int		nValueStepNo;		//비교할 기준 Step번호					//v1009 : Cycle 누적 와트량 부호 (0:모두, 1:충전, 2:방전)
	float	fValueRate;			//비교 값 (Ah , WhattHour...) SOC

	//Cycle  Loop in Loop 조건
	//ljb v1009 ///////////////////////////////////////////////////////////////
	UINT nLoopInfoCycle;
	UINT nLoopInfoGotoStep;

	UINT nMultiLoopInfoCycle;
	UINT nMultiLoopInfoGotoStep;
	UINT nMultiLoopGroupID;

	UINT nAccLoopInfoCycle;
	UINT nAccLoopInfoGotoStep;
	UINT nAccLoopGroupID;

	UINT nGotoStepEndV_H;
	UINT nGotoStepEndV_L;
	UINT nGotoStepEndTime;
	UINT nGotoStepEndCVTime;
	UINT nGotoStepEndC;				//EndV,EndT,EndC를 위한 임시 Step ID -> 이동 스텝은 이 ID를 참조하게 된다. 20080201 kjh
	UINT nGotoStepEndWh;
	UINT nGotoStepEndValue;
	//////////////////////////////////////////////////////////////////////////

	//ljb 20100818 for v100A 
	short int	ican_function_division[SCH_MAX_END_CAN_AUX_POINT];		//분류 번호
	unsigned char	cCan_compare_type[SCH_MAX_END_CAN_AUX_POINT];		//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char	cCan_data_type[SCH_MAX_END_CAN_AUX_POINT];			//0:None , 1: Signed, 2: Float
	short int	ican_branch[SCH_MAX_END_CAN_AUX_POINT];				//Can 분기	Next : 251, Pause : 252 
	float		fcan_Value[SCH_MAX_END_CAN_AUX_POINT];

	short int	iaux_function_division[SCH_MAX_END_CAN_AUX_POINT];		//분류 번호
	unsigned char	cAux_compare_type[SCH_MAX_END_CAN_AUX_POINT];		//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char	cAux_data_type[SCH_MAX_END_CAN_AUX_POINT];			//0:None , 1: Signed, 2: Float
	short int	iaux_branch[SCH_MAX_END_CAN_AUX_POINT];					//Aux 분기	Next : 251, Pause : 252
	float		faux_Value[SCH_MAX_END_CAN_AUX_POINT];
	UINT		iaux_conti_time[SCH_MAX_END_CAN_AUX_POINT];				//ljb 20170824 add Aux 지속시간 & 연산 함. 기능추가


	int		bUseCyclePause;			//ljb 201010 Cycle 반복 후 Pause 종료 조건
	int		bUseLinkStep;			//ljb 201010 CP-CC 연동 STEP Flag
	int		bUseChamberProg;		//ljb 201010 챔버 Prog 모드 실행
	int     bUseChamberStop;		//yulee 20180828 스텝별 챔버 정지 기능(챔버 연동 시)
	
	//안전 조건 
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	LONG	lDeltaTime;
	LONG	lDeltaTime1;				 
	float	fDeltaV;							
	float	fDeltaI;
	float	fTempHigh;				//ljb SBC가 챔버와 연동할때 챔버 온도 안전조건 HIGH
	float	fTempLow;				//ljb SBC가 챔버와 연동할때 챔버 온도 안전조건 Low
	float	fCapacitanceHigh;
	float	fCapacitanceLow;

	//Grading
	BOOL	bGrade;
	GRADE	sGrading_Val;

	//전압 상승 비교 설정
	float	fCompVHigh[SCH_MAX_COMP_POINT];
	float	fCompVLow[SCH_MAX_COMP_POINT];
	ULONG	ulCompTimeV[SCH_MAX_COMP_POINT];

	//전류 상승 비교 설정
	float	fCompIHigh[SCH_MAX_COMP_POINT];
	float	fCompILow[SCH_MAX_COMP_POINT];
	ULONG	ulCompTimeI[SCH_MAX_COMP_POINT];

	//종지 전류 전압비교 설정
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				
	float	fVEndHigh;				
	float	fVEndLow;	
	
	//Report 설정
	float	fReportV;
	float	fReportI;
	ULONG	ulReportTime;
	float	fReportTemp;

	//EDLC 용//////2003/11/28 KBH	for EDLC	///////////////////////////////////
	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1명
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	ULONG	fLCStartTime;			//EDLC LC 측정 시작 시간
	ULONG	fLCEndTime;				//EDLC LC 측정 종료 시간
	LONG	lRange;					//Range 선택
	///////////////////////////////////////////////////////////////////////
	
	//copy paste 기능시 사용
	INT		nEditState;

	//Simulation 용/////2007/07/27 KJH  for Simulation Mode //////////////////////
	char	szSimulFile[128];		//Simulation 파일
	long	lValueLimitHigh;		//최대 (전류 또는 전력) 값
	long	lValueLimitLow;			//최소 (전류 또는 전력) 값
	//////////////////////////////////////////////////////////////////////////////

	//2014.09.02 UserMap 파일 추가.
	char    szUserMapFile[128];
	char	UserMap_Time_Type;		//2014.09.15 시간 설정 방법
	char	UserMap_Mode_Type;		//2014.09.15 동작 모드
	long    UserMap_Apply_Period;   //2014.09.15 동작 시간
	
	//2014.12.08 데이터 체크(can,aux) 무시모드
	int    nNoCheckMode;
	int    nCanTxOffMode; // 2014.12.09 캔 송신 off 플러그 추가.
	int    nCanCheckMode; //20150828 add

	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// Wait Time 조건 추가  20160422
	int		nWaitTimeInit;		
	int		nWaitTimeDay;		//대기 일
	int		nWaitTimeHour;	//대기 시간
	int		nWaitTimeMin;		//대기  분
	int		nWaitTimeSec;		//대기  초

	//cny CellBalance
	int     nCellBal_CircuitRes;
	int     nCellBal_WorkVolt;//mV
	int     nCellBal_WorkVoltUpper;//mV
	int     nCellBal_StartVolt;//mV
	int     nCellBal_EndVolt;//mV
	int     nCellBal_ActiveTime;
	int     nCellBal_AutoStopTime;
	int     nCellBal_AutoNextStep;
	
	//cny PowerSupply
	int     nPwrSupply_Cmd;//1-ON,2-OFF,0-NONE
	int     nPwrSupply_Volt;//mV
	
	//cny Chiller
	int     nChiller_Cmd;//1-USE
	float   fChiller_RefTemp;
    int     nChiller_RefCmd;//1-Run,2-Stop
    float   fChiller_RefPump;
	int     nChiller_PumpCmd;//1-Run,2-Stop
	int     nChiller_TpData;//1-Aux,2-Can
	float   fChiller_ONTemp1;//Max
    int     nChiller_ONTemp1_Cmd;//1-ON
	int     nChiller_ONPump1_Cmd;//1-ON
	float   fChiller_ONTemp2;
	int     nChiller_ONTemp2_Cmd;//1-ON
	int     nChiller_ONPump2_Cmd;//1-ON
	float   fChiller_OFFTemp1;
	int     nChiller_OFFTemp1_Cmd;//1-ON
	int     nChiller_OFFPump1_Cmd;//1-ON
	float   fChiller_OFFTemp2;
	int     nChiller_OFFTemp2_Cmd;//1-ON
	int     nChiller_OFFPump2_Cmd;//1-ON

	float	fCellDeltaVStep;//yulee 20190531_3
	float	fStepDeltaAuxTemp;
	float	fStepDeltaAuxTh;
	float	fStepDeltaAuxT;
	float	fStepDeltaAuxVTime;
	float	fStepAuxV;

	BOOL	bStepDeltaVentAuxV;
	BOOL	bStepDeltaVentAuxTemp;
	BOOL	bStepDeltaVentAuxTh;
	BOOL	bStepDeltaVentAuxT;
	BOOL	bStepVentAuxV;

}	STEP;	

typedef struct tag_GROUP_PARAMETER {
// 	float	fMaxVoltage;
// 	float	fMinVoltage;
// 	float	fMaxCurrent;
// 	float	fOCVLimitVal;			//Vref
// 	float	fTrickleCurrent;
// 	float	fDeltaVoltage;
// 	long	lTrickleTime;
// 	int		nMaxFaultNo;
// 	BOOL	bPreTest;
	float	fMaxV;
	float	fMinV;
	float	fMaxI;
	float	fCellDataV;
	float	fMaxT;
	float	fMinT;
	float	fMaxC;
	long	lMaxW;
	long	lMaxWh;
	float	fStdMaxV;
	float	fStdMinV;
	float	fDataMaxV;
	float	fDataMinV;
	BOOL	bDataVent;

	//ljb 20100818 for v100A 
	short int	ican_function_division[SCH_MAX_END_CAN_AUX_POINT];		//분류 번호
	unsigned char	cCan_compare_type[SCH_MAX_END_CAN_AUX_POINT];		//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char	cCan_data_type[SCH_MAX_END_CAN_AUX_POINT];			//can data type -> 비교 type 0:사용 안한 1 : Signed, 2: Float
	float		fcan_Value[SCH_MAX_END_CAN_AUX_POINT];
	short int	iaux_function_division[SCH_MAX_END_CAN_AUX_POINT];		//분류 번호
	unsigned char	cAux_compare_type[SCH_MAX_END_CAN_AUX_POINT];			//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char	cAux_data_type[SCH_MAX_END_CAN_AUX_POINT];			//can data type -> 비교 type 0:사용 안한 1 : Signed, 2: Float
	float		faux_Value[SCH_MAX_END_CAN_AUX_POINT];

	//cny 201809
	char szSocTableFile[128];

}	TEST_PARAM;

typedef struct tag_ConditionFile_Header {
	char	szFileIdentify[16];
	char	szDateTime[32];
}	CONDITION_FILE_HEADER;

//#define V_UNIT_FACTOR	1000.0f
//#define I_UNIT_FACTOR	1000.0f
#define T_UNIT_FACTOR	100.0F
#define Z_UNIT_FACTOR	1
//#define C_UNIT_FACTOR	1000.0f

typedef struct tag_UnitInfo {
	float fTransfer;
	char szUnit[8];
	int  nFloatPoint;
} UNIT_INFO;

#define COPY_DATA	0
#define INSERT_DATA 1
#define DELETE_DATA 2

#endif	//_DATAFORM_