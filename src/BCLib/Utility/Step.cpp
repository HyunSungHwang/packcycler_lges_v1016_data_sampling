// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Step.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStep::CStep(int nStepType /*=0*/)
{
	ClearStepData();
	m_type = (BYTE)nStepType;
}

CStep::CStep()
{
	ClearStepData();
}

CStep::~CStep()
{

}

BOOL CStep::ClearStepData()
{
	m_type = 0;
	m_StepIndex = 0;
	m_mode = 0;
	m_bGrade = FALSE;
	m_lProcType = 0;				
	m_fVref = 0;
    m_fIref = 0;
    
	m_ulEndTime = 0;
    m_fEndV = 0;
    m_fEndI = 0;
    m_fEndC = 0;
    m_fEndDV = 0;
    m_fEndDI = 0;

	m_nLoopInfoGoto = 0;	
	m_nLoopInfoCycle = 0;	

	m_fHighLimitV = 0;
    m_fLowLimitV = 0;
    m_fHighLimitI = 0;
    m_fLowLimitI = 0;
    m_fHighLimitC = 0;
    m_fLowLimitC = 0;
	m_fHighLimitImp = 0;
    m_fLowLimitImp = 0;
	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_lCompTimeV[i] = 0;			
		m_fCompHighV[i] = 0.0f;
		m_fCompLowV[i] = 0.0f;
		m_lCompTimeI[i] = 0;			
		m_fCompHighI[i] = 0.0f;
		m_fCompLowI[i] = 0.0f;
	}
    m_lDeltaTimeV = 0;
    m_fDeltaV = 0.0f;
    m_lDeltaTimeI = 0;
    m_fDeltaI = 0.0f;

	m_fReportV = 0.0f;
	m_fReportI = 0.0f;
	m_ulReportTime = 0;


	m_fCapaVoltage1		=0;	
	m_fCapaVoltage2		=0;	

	m_ulDCRStartTime	=0;	
	m_ulDCREndTime		=0;

	m_ulLCStartTime		=0;	
	m_ulLCEndTime		=0;		

	m_lRange		=0;			


	m_Grading.ClearStep();
	m_StepID = 0;
	return TRUE;
}

void CStep::operator=(CStep &step) 
{
	m_type = step.m_type;
	m_StepIndex = step.m_StepIndex;
	m_mode = step.m_mode;
	m_bGrade = step.m_bGrade;
	m_lProcType = step.m_lProcType;				
	m_fVref = step.m_fVref;
    m_fIref = step.m_fIref;
    
	m_ulEndTime = step.m_ulEndTime;
    m_fEndV = step.m_fEndV;
    m_fEndI = step.m_fEndI;
    m_fEndC = step.m_fEndC;
    m_fEndDV = step.m_fEndDV;
    m_fEndDI = step.m_fEndDI;

	m_nLoopInfoGoto = step.m_nLoopInfoGoto;	
	m_nLoopInfoCycle = step.m_nLoopInfoCycle;	

	m_fHighLimitV = step.m_fHighLimitV;
    m_fLowLimitV = step.m_fLowLimitV;
    m_fHighLimitI = step.m_fHighLimitI;
    m_fLowLimitI = step.m_fLowLimitI;
    m_fHighLimitC = step.m_fHighLimitC;
    m_fLowLimitC = step.m_fLowLimitC;
	m_fHighLimitImp = step.m_fHighLimitImp;
    m_fLowLimitImp = step.m_fLowLimitImp;
	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_lCompTimeV[i] = step.m_lCompTimeV[i];			
		m_fCompHighV[i] = step.m_fCompHighV[i];
		m_fCompLowV[i] = step.m_fCompLowV[i];
		m_lCompTimeI[i] = step.m_lCompTimeI[i];			
		m_fCompHighI[i] = step.m_fCompHighI[i];
		m_fCompLowI[i] = step.m_fCompLowI[i];
	}
    m_lDeltaTimeV = step.m_lDeltaTimeV;
    m_fDeltaV = step.m_fDeltaV;
    m_lDeltaTimeI = step.m_lDeltaTimeI;
    m_fDeltaI = step.m_fDeltaI;

	m_fReportV	=	step.m_fReportV;
	m_fReportI	=	step.m_fReportI;
	m_ulReportTime	=	step.m_ulReportTime;

	m_fCapaVoltage1		=step.m_fCapaVoltage1;	
	m_fCapaVoltage2		=step.m_fCapaVoltage2;	

	m_ulDCRStartTime	=step.m_ulDCRStartTime;	
	m_ulDCREndTime		=step.m_ulDCREndTime;

	m_ulLCStartTime		=step.m_ulLCStartTime;	
	m_ulLCEndTime		=step.m_ulLCEndTime;		

	m_lRange		=step.m_lRange;			


	m_Grading.SetGradingData(step.m_Grading);

	m_StepID = step.m_StepID;

	GRADE_STEP data;
	m_Grading.ClearStep();
	for(int i = 0; i<step.m_Grading.GetGradeStepSize(); i++)
	{
		data = step.m_Grading.GetStepData(i);
		m_Grading.AddGradeStep(data.fMin, data.fMax, data.strCode);
	}
}

FILE_STEP_PARAM		CStep::GetFileStep()
{
	FILE_STEP_PARAM stepData;
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM));

	stepData.chStepNo	= m_StepIndex;			
	stepData.nProcType	= m_lProcType;		
	stepData.chType		= m_type;
	stepData.chMode		= m_mode;
	stepData.fVref		= m_fVref;
	stepData.fIref		= m_fIref;
				
	stepData.lEndTime	= m_ulEndTime;	
	stepData.fEndV		= m_fEndV;
	stepData.fEndI		= m_fEndI;
	stepData.fEndC		= m_fEndC;
	stepData.fEndDV		= m_fEndDV;		
	stepData.fEndDI		= m_fEndDI;	
	
	stepData.nLoopInfoGoto  = m_nLoopInfoGoto;	
	stepData.nLoopInfoCycle = m_nLoopInfoCycle;	


	stepData.fVLimitHigh	=	m_fHighLimitV;
	stepData.fVLimitLow		=	m_fLowLimitV;
	stepData.fILimitHigh	=	m_fHighLimitI;	
	stepData.fILimitLow		=	m_fLowLimitI;
	stepData.fCLimitHigh	=	m_fHighLimitC;
	stepData.fCLimitLow		=	m_fLowLimitC;
	stepData.fImpLimitHigh	=	m_fHighLimitImp;
	stepData.fImpLimitLow	=	m_fLowLimitImp;
	
	stepData.lDeltaTime		=	m_lDeltaTimeV; 	
	stepData.lDeltaTime1	=	m_lDeltaTimeI;	
	stepData.fDeltaV		=	m_fDeltaV;		
	stepData.fDeltaI		=	m_fDeltaI;

	stepData.bGrade			=	m_bGrade;
	stepData.sGrading_Val.chTotalGrade			= m_Grading.GetGradeStepSize();
	stepData.sGrading_Val.lGradeItem = m_Grading.m_lGradingItem;

	GRADE_STEP	grade_step;
	for(int i =0; i<m_Grading.GetGradeStepSize(); i++)
	{
		grade_step = m_Grading.GetStepData(i);
		stepData.sGrading_Val.faValue1[i]	=	grade_step.fMin;
		stepData.sGrading_Val.faValue2[i]	=	grade_step.fMax;
		stepData.sGrading_Val.aszGradeCode[i]	=	grade_step.strCode[0];
	}

	
	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		stepData.fCompVLow[i]	=	m_fCompLowV[i];
		stepData.fCompVHigh[i]	=	m_fCompHighV[i];	
		stepData.ulCompTimeV[i] =	m_lCompTimeV[i];
		stepData.fCompILow[i]	=	m_fCompLowI[i];
		stepData.fCompIHigh[i]	=	m_fCompHighI[i];
		stepData.ulCompTimeI[i]	=	m_lCompTimeI[i];
		i++;
	}

	//Not Use (Time 비교값으로 대치 가능 하므로 사용 안함
//	stepData.fIEndHigh;		
//	stepData.fIEndLow;			
//	stepData.fVEndHigh;			
//	stepData.fVEndLow;	
	
	stepData.fReportV	=	m_fReportV;
	stepData.fReportI	=	m_fReportI;
	stepData.ulReportTime	=	m_ulReportTime;

	stepData.fCapaVoltage1 = m_fCapaVoltage1;	
	stepData.fCapaVoltage2 = m_fCapaVoltage2;	
	
	stepData.ulDCRStartTime = m_ulDCRStartTime;	
	stepData.ulDCREndTime = m_ulDCREndTime;
	
	stepData.ulLCStartTime = m_ulLCStartTime;	
	stepData.ulLCEndTime = m_ulLCEndTime;		

	stepData.lRange = m_lRange;			

	//m_StepID;

	return stepData;
}

//
void	CStep::SetStepData(FILE_STEP_PARAM const &stepData)
{
	ClearStepData();
	
	m_StepIndex		= stepData.chStepNo	;
	m_lProcType		= stepData.nProcType;	
	m_type			= stepData.chType;		
	m_mode			= stepData.chMode;		
	m_fVref			= stepData.fVref;		
	m_fIref			= stepData.fIref;		
				
	m_ulEndTime			= stepData.lEndTime;	
	m_fEndV				= stepData.fEndV	;
	m_fEndI				= stepData.fEndI	;
	m_fEndC				= stepData.fEndC	;
	m_fEndDV			= stepData.fEndDV	;		
	m_fEndDI			= stepData.fEndDI	;	
	
	m_nLoopInfoGoto		=	stepData.nLoopInfoGoto;	
	m_nLoopInfoCycle	=	stepData.nLoopInfoCycle;	

	m_fHighLimitV	=	stepData.fVLimitHigh	;
	m_fLowLimitV	=	stepData.fVLimitLow		;
	m_fHighLimitI	=	stepData.fILimitHigh	;	
	m_fLowLimitI	=	stepData.fILimitLow		;	
	m_fHighLimitC	=	stepData.fCLimitHigh	;	
	m_fLowLimitC	=	stepData.fCLimitLow		;	
	m_fHighLimitImp	=	stepData.fImpLimitHigh	;
	m_fLowLimitImp	=	stepData.fImpLimitLow	;	
	
	m_lDeltaTimeV	=	stepData.lDeltaTime		; 	
	m_lDeltaTimeI	=	stepData.lDeltaTime1	;	
	m_fDeltaV		=	stepData.fDeltaV		;		
	m_fDeltaI		=	stepData.fDeltaI		;

	m_bGrade = stepData.bGrade;
	m_Grading.ClearStep();
	m_Grading.m_lGradingItem = stepData.sGrading_Val.lGradeItem;

//	GRADE_STEP	grade_step;
	for(int i =0; i<stepData.sGrading_Val.chTotalGrade; i++)
	{	
		m_Grading.AddGradeStep(stepData.sGrading_Val.faValue1[i], stepData.sGrading_Val.faValue2[i], stepData.sGrading_Val.aszGradeCode[i]);
	}

	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		m_fCompLowV[i]	=stepData.fCompVLow[i]	;
		m_fCompHighV[i]	=stepData.fCompVHigh[i]	;	
		m_lCompTimeV[i]	=stepData.ulCompTimeV[i] 	;
		m_fCompLowI[i]	=stepData.fCompILow[i]	;
		m_fCompHighI[i]	=stepData.fCompIHigh[i]	;
		m_lCompTimeI[i]	=stepData.ulCompTimeI[i]		;
		i++;
	}

	//Not Use (Time 비교값으로 대치 가능 하므로 사용 안함
//	stepData.fIEndHigh;		
//	stepData.fIEndLow;			
//	stepData.fVEndHigh;			
//	stepData.fVEndLow;	

	m_fReportV = stepData.fReportV;
	m_fReportI = stepData.fReportI;
	m_ulReportTime = stepData.ulReportTime;

	
	m_fCapaVoltage1	 = stepData.fCapaVoltage1;	
	m_fCapaVoltage2	 = stepData.fCapaVoltage2;	
	
	m_ulDCRStartTime = stepData.ulDCRStartTime ;	
	m_ulDCREndTime = stepData.ulDCREndTime ;
	
	m_ulLCStartTime = stepData.ulLCStartTime ;	
	m_ulLCEndTime = stepData.ulLCEndTime ;		

	m_lRange = stepData.lRange;			

	//m_StepID;

}
