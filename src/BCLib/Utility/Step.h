// Step.h: interface for the CStep class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
#define AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Grading.h"

class CStep  
{
public:
	FILE_STEP_PARAM		GetFileStep();
	void	SetStepData(FILE_STEP_PARAM const &stepData);
	
	BOOL ClearStepData();
	CStep(int nStepType /*=0*/);
	CStep();
	virtual ~CStep();
	void operator = (CStep &step); 

	BYTE	m_type;			//Step Type
	BYTE	m_StepIndex;	//StepNo
	BYTE	m_mode;
	BYTE	m_bGrade;
	long	m_lProcType;				
	float	m_fVref;
    float	m_fIref;
    
	/* Step End Value */
	ULONG	m_ulEndTime;
    float	m_fEndV;
    float	m_fEndI;
    float	m_fEndC;
    float	m_fEndDV;
    float	m_fEndDI;

	/*Step Fail Value */
	float	m_fHighLimitV;
    float	m_fLowLimitV;
    float	m_fHighLimitI;
    float	m_fLowLimitI;
    float	m_fHighLimitC;
    float	m_fLowLimitC;
	float 	m_fHighLimitImp;
    float	m_fLowLimitImp;
	
	//Cycle Check
	long	m_nLoopInfoGoto;	
	long	m_nLoopInfoCycle;	

	//V/I Ramp check
	ULONG	m_lCompTimeV[PS_MAX_COMP_POINT];			
	float	m_fCompHighV[PS_MAX_COMP_POINT];
 	float	m_fCompLowV[PS_MAX_COMP_POINT];
	ULONG	m_lCompTimeI[PS_MAX_COMP_POINT];			
	float	m_fCompHighI[PS_MAX_COMP_POINT];
	float	m_fCompLowI[PS_MAX_COMP_POINT];

	//Delta Check
	ULONG	m_lDeltaTimeV;
    float	m_fDeltaV;
    ULONG	m_lDeltaTimeI;
    float	m_fDeltaI;

	//Recording 
	float	m_fReportV;
	float	m_fReportI;
	ULONG	m_ulReportTime;



	float	m_fCapaVoltage1;		
	float	m_fCapaVoltage2;	
	ULONG	m_ulDCRStartTime;		
	ULONG	m_ulDCREndTime;
	ULONG	m_ulLCStartTime;		
	ULONG	m_ulLCEndTime;		

	ULONG	m_lRange;			
	long	m_StepID;
	
	CGrading	m_Grading;

};

#endif // !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
