VB/Access programmers can study this example to see how
to replace using the slow YData property array with the
fast PEvset DLL call. 

Using the current Access 7 example code, paste this
code into the FormLoad event to replace the commented
code below.

    '** Fill YData() with actual Sales data **'
    'ss_Sales.MoveFirst
    'For col = 1 To ColumnCount
    '    i = 0
    '    Do Until ss_Sales.EOF
->  '        PEGraph1.YData(col - 1, i) = ss_Sales.Fields(col)
    '        ss_Sales.MoveNext
    '        i = i + 1
    '    Loop
    '     ss_Sales.MoveFirst
    'Next col
    
    '** Fill YData with DLL call, much faster **'
    '** This logic stores all of the first subset's data and
    '** then the second subsets's data and so on into the TmpData array.
    '** Be sure to load VB4PEAPI.TXT into a module via the Insert..Module menu item.
    '** If using VB3, VB4-16, or Access 2 load VB3PEAPI.TXT.

    Dim test As Long
->  ReDim TmpData(PEGraph1.Subsets * PEGraph1.Points) As Single
    ss_Sales.MoveFirst
    i = 0
    For col = 1 To ColumnCount
        Do Until ss_Sales.EOF
->          TmpData(i) = ss_Sales.Fields(col)
            ss_Sales.MoveNext
            i = i + 1
        Loop
        ss_Sales.MoveFirst
    Next col
->  test = PEvset(PEGraph1, PEP_faYDATA, TmpData(0), PEGraph1.Subsets * PEGraph1.Points)

