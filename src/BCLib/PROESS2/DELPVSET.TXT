Information on how to use PEvset function within Delphi to
speed data transfer.

unit Unit2;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Pegvcl, StdCtrls, Pegrpapi;

type
  TForm2 = class(TForm)
    PEGraph1: PEGraph;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  MyTempData = array[0..3, 0..11] of Single;
  MyTempData2 = array[0..16000] of Single;

var
  Form2: TForm2;

implementation

{$R *.DFM}

procedure TForm2.FormCreate(Sender: TObject);
var
   s: Integer;
   p: Integer;
   o: Integer;
   f: ^MyTempData;
   pt: ^MyTempData2;
begin

{** Make Random Data for Graph Object}
PEGraph1.Subsets := 4;
PEGraph1.Points := 12;

{** Pass data via YData VCL property array **}
For s := 0 To 3 do       {4 subsets}
    For p := 0 To 11 do  {12 points}
        PEGraph1.YData[s, p] := (p + 1) * 5 + Random(25);

{** Pass data via dynamically allocated two dimensional array **}
{** Good when you have lots of data and DO know size ahead of time **}
New(f);
For s := 0 To 3 do       {4 subsets}
    For p := 0 To 11 do  {12 points}
        f^[s, p] := (p + 1) * 5 + Random(25);
PEvset(PEGraph1.hObject, PEP_faYDATA, f, 48);
Dispose(f);

{** Pass data via dynamically allocated and dynamically sized array **}
{** Good when you have lots of data and DON'T know size ahead of time **}
GetMem(pt, SizeOf(Single) * 48);
For s := 0 To 3 do       {4 subsets}
begin;
    For p := 0 To 11 do  {12 points}
    begin
        {convert two dimensional index to one dimension}
        o := (s * 12{12 points per subset}) + p;
        pt^[o] := (p + 1) * 5 + Random(25);
    end;
end;
PEvset(PEGraph1.hObject, PEP_faYDATA, pt, 48);
FreeMem(pt, SizeOf(Single) * 48);
