/* OWL Example code for ProEssentials Implementation */

void PEWindow::SetupWindow()
{
    RECT	rect;
    HWND	hWnd;
    int		i, j;
    float	data[4][24];	/* 4 SUBSETS by 24 POINTS */
    char	subsetlabels[] = "Texas\tCalifornia\tFlorida\tWashington\t";
    UINT	intarray[2];

    /* hWndPE is a global variable to store handle of object */
    /* You should define hWndPE as a member variable of your PEWindow class */
    hWndPE = NULL;
    
    TWindow::SetupWindow();
    
    /********************************************************************
    *********************************************************************
    ** Implement ProEssential Object **
    **********************************/
    ::GetClientRect(HWindow, (LPRECT) &rect);
    hWndPE = PEcreate(PECONTROL_GRAPH, WS_VISIBLE, (LPRECT) &rect, HWindow, 10000);

    /* set amount of data */
    PEnset(hWndPE, PEP_nSUBSETS, 4);	/* Object will manage 4 subsets */
    PEnset(hWndPE, PEP_nPOINTS, 24);	/* Object will manage 24 points */

    /* set various other properties */
    PEnset(hWndPE, PEP_bPREPAREIMAGES, 1);		/* Prepare images in memory */
    PEnset(hWndPE, PEP_nSCROLLINGSUBSETS, 1);	/* Scroll subsets 1 at a time */
    PEnset(hWndPE, PEP_nDATAPRECISION, 1);      /* Data in table has 1 decimal */
    PEnset(hWndPE, PEP_nPOINTSTOGRAPH, 10);     /* Only show 10 of the 20 points */
    PEnset(hWndPE, PEP_nGRAPHPLUSTABLE, PEGPT_BOTH);	/* Include Table with Graph */ 
    PEnset(hWndPE, PEP_nTABLEWHAT, PETW_ALLSUBSETS);    /* Table all subsets */
    PEnset(hWndPE, PEP_nPLOTTINGMETHOD, PEGPM_AREA);    /* Show as an Area graph */
    PElset(hWndPE, PEP_dwGRAPHBACKCOLOR, RGB(192, 192, 192)); /* Graph background color is white */
	
    /* prepare some random data, send as subsets x points one dimension */
    for (i=0; i<4; i++)
    {
        for (j=0; j<24; j++)
            data[i][j] = (float) GetRandom(100, 1000);
    }    	
    PEvset(hWndPE, PEP_faYDATA, (LPVOID) data, 96);
    
    /* tab delimited subset labels */
    PEvset(hWndPE, PEP_szaSUBSETLABELS, (LPVOID) subsetlabels, 4);
    
    /* tell object to generate statistical comparison subsets */
    intarray[0] = PEAS_AVGAP;	/* average of all subsets all points */
    intarray[1] = PEAS_AVGPP;	/* average of all subsets per point */
    PEvset(hWndPE, PEP_naAUTOSTATSUBSETS, (LPVOID) intarray, 2);
    
    /* set autostats as permanent subsets 
       user can compare other subsets to statistical subsets */
    intarray[0] = 4;  /* zero based index of PEAS_AVGAP */
    intarray[1] = 5;  /* zero based index of PEAS_AVGPP */
    PEvset(hWndPE, PEP_naRANDOMSUBSETSTOGRAPH, (LPVOID) intarray, 2);

    /* reinitialize and reset image */
    PEreinitialize(hWndPE);
    PEresetimage(hWndPE, 0, 0);
    
    /* After reinitialization the Object contains 6 subsets, thus  
       PEP_nSUBSETS equals 6. */
    
    /********************************************************************
    ********************************************************************/
}

void PEWindow::CleanupWindow()
{
    /* Delete ProEssentials Object if exists */
    if (hWndPE)
    {
    	PEdestroy(hWndPE);
    	hWndPE = NULL;
    }
    
    TWindow::CleanupWindow();
}