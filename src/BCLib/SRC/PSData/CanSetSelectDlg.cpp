// CanSetSelectDlg.cpp : implementation file
//

#include "stdafx.h"
//#include "psdata.h"
#include "CanSetSelectDlg.h"
#include "DataQueryDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCanSetSelectDlg dialog


CCanSetSelectDlg::CCanSetSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCanSetSelectDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCanSetSelectDlg::IDD2):
	(CCanSetSelectDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CCanSetSelectDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_nSelectedItemNum = 0;
}


void CCanSetSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCanSetSelectDlg)
	DDX_Control(pDX, IDC_CAN_SET_SELECT_LIST, m_ctrlCanSetSelectList);
	//}}AFX_DATA_MAP


}


BEGIN_MESSAGE_MAP(CCanSetSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CCanSetSelectDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CCanSetSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	init_Language();
	UpdateCanSetSelectList();
	return 1;
}
/////////////////////////////////////////////////////////////////////////////
// CCanSetSelectDlg message handlers

void CCanSetSelectDlg::UpdateCanSetSelectList()
{
//	int* SelectedCAN;
//	SelectedCAN = ((CDataQueryDialog*)GetParent())->m_CanListCheckNo;
	CDataQueryDialog *pDataQueryDlg = ((CDataQueryDialog*)GetParent());
	CString strItemName;
	for(int nIndex = 0; nIndex < pDataQueryDlg->m_ctrlCanList.GetCount(); nIndex++)
	{
		if(pDataQueryDlg->m_ctrlCanList.GetCheck(nIndex))
		{
			CString tmpItemText;
			pDataQueryDlg->m_ctrlCanList.GetText(nIndex, tmpItemText);
			m_ctrlCanSetSelectList.AddString(tmpItemText);
		}	
	}

	if(pDataQueryDlg->m_bBtnCANSetA)
	{
		for(int nIndex = 0; nIndex<pDataQueryDlg->m_ctrlCanListSetA.GetCount(); nIndex++)
		{
			if(pDataQueryDlg->m_ctrlCanList.GetCheck(nIndex))
			{
				for(int i=0; i<m_ctrlCanSetSelectList.GetCount(); i++)
				{
					CString tmpItemText;
					pDataQueryDlg->m_ctrlCanList.GetText(i, tmpItemText);
					CString tmpSetAItemText;
					pDataQueryDlg->m_ctrlCanListSetA.GetText(nIndex, tmpSetAItemText);
					if(tmpItemText.Mid(0,5).CompareNoCase(tmpSetAItemText.Mid(0,5))==0)
					{
						int tmpNo;
						tmpNo = (atoi(tmpSetAItemText.Mid(tmpSetAItemText.Find('[')+1,3)))-1;
						m_ctrlCanSetSelectList.SetCheck(tmpNo,TRUE);
					}
				}
			}
		}
	}
	else if(pDataQueryDlg->m_bBtnCANSetB)
	{
		for(int nIndex = 0; nIndex<pDataQueryDlg->m_ctrlCanListSetB.GetCount(); nIndex++)
		{
			if(pDataQueryDlg->m_ctrlCanList.GetCheck(nIndex))
			{
				for(int i=0; i<m_ctrlCanSetSelectList.GetCount(); i++)
				{
					CString tmpItemText;
					pDataQueryDlg->m_ctrlCanList.GetText(i, tmpItemText);
					CString tmpSetBItemText;
					pDataQueryDlg->m_ctrlCanListSetB.GetText(nIndex, tmpSetBItemText);
					if(tmpItemText.Mid(0,5).CompareNoCase(tmpSetBItemText.Mid(0,5))==0)
					{
						int tmpNo;
						tmpNo = (atoi(tmpSetBItemText.Mid(tmpSetBItemText.Find('[')+1,3)))-1;
						m_ctrlCanSetSelectList.SetCheck(tmpNo,TRUE);
					}
				}
			}
		}
	}
	else if(pDataQueryDlg->m_bBtnCANSetC)
	{
		for(int  nIndex = 0; nIndex<pDataQueryDlg->m_ctrlCanListSetC.GetCount(); nIndex++)
		{
			if(pDataQueryDlg->m_ctrlCanList.GetCheck(nIndex))
			{
				for(int i=0; i<m_ctrlCanSetSelectList.GetCount(); i++)
				{
					CString tmpItemText;
					pDataQueryDlg->m_ctrlCanList.GetText(i, tmpItemText);
					CString tmpSetCItemText;
					pDataQueryDlg->m_ctrlCanListSetC.GetText(nIndex, tmpSetCItemText);
					if(tmpItemText.Mid(0,5).CompareNoCase(tmpSetCItemText.Mid(0,5))==0)
					{
						int tmpNo;
						tmpNo = (atoi(tmpSetCItemText.Mid(tmpSetCItemText.Find('[')+1,3)))-1;
						m_ctrlCanSetSelectList.SetCheck(tmpNo,TRUE);
					}
				}
			}
		}
	}


}

void CCanSetSelectDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString strItemName;
	for(int nIndex = 0; nIndex < m_ctrlCanSetSelectList.GetCount(); nIndex++)
	{
		if(m_ctrlCanSetSelectList.GetCheck(nIndex))
		{
			CString strTmpItemText;
			int tmpInt;
			m_ctrlCanSetSelectList.GetText(nIndex, strTmpItemText);
			tmpInt = atoi(strTmpItemText.Mid(strTmpItemText.Find('[')+1,3));//strTmpItemText.Find('['),strTmpItemText.Find(']')-1));
			m_nSelectedItemArr[m_nSelectedItemNum] = tmpInt;
			m_nSelectedItemNum++;
		}	
	}

	CDialog::OnOK();
}

int* CCanSetSelectDlg::GetSelectedItemArr()
{
	return m_nSelectedItemArr;
}

int CCanSetSelectDlg::GetSelectedItemNum()
{
	return m_nSelectedItemNum;
}