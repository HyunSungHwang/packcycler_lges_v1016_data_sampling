// Line.cpp: implementation of the CLine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Line.h"
//#include "Table.h"
#include "Axis.h"
#include "Data.h"
#include "DataQueryCdn.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLine::CLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, COLORREF color, LPCTSTR strAxisName)
: m_strChPath(strChPath), m_lCycle(lCycle), m_dwMode(dwMode), m_Color(color), m_strAxisName(strAxisName)
{
	m_LineType = PS_SOLID;
	m_Width    = 2;
	m_bSelect = FALSE;
	m_bDataPointCheck = FALSE;
	m_bInterLine = TRUE;
	m_nShowLine = TRUE;	//20090911 KBH
}

CLine::~CLine()
{

}

BOOL CLine::GetMinMaxValue(fltPoint& MinValue, fltPoint& MaxValue, BOOL bInit)
{
	//
	POSITION pos = m_DataList.GetHeadPosition();
	if(pos==NULL) return FALSE;
	//
	fltPoint Data;
	if(bInit)
	{
		Data = m_DataList.GetNext(pos);
		MinValue.x = MaxValue.x = Data.x;
		MinValue.y = MaxValue.y = Data.y;
	}
	//
	while(pos)
	{
		Data = m_DataList.GetNext(pos);
		if(Data.x>MaxValue.x) MaxValue.x = Data.x;
		if(Data.x<MinValue.x) MinValue.x = Data.x;
		if(Data.y>MaxValue.y) MaxValue.y = Data.y;
		if(Data.y<MinValue.y) MinValue.y = Data.y;
	}
	//
	return TRUE;
}

CString CLine::GetName()
{
	CString strName;
	// 영점시작
	if     (m_lCycle!=0L&& m_dwMode==CDataQueryCondition::MODE_NONE)
	{
		strName.Format("%s Cyc%d", 
			           m_strChPath.Mid(m_strChPath.ReverseFind('\\')+1),
					   m_lCycle);
	}
	// Cycle이 x축
	else if(m_lCycle==0L&& m_dwMode!=CDataQueryCondition::MODE_NONE)
	{
		strName = m_strChPath.Mid(m_strChPath.ReverseFind('\\')+1);
		if(m_strAxisName.CollateNoCase("OCV") != 0)
		{
			strName+= CDataQueryCondition::GetModeName(m_dwMode);
		}
	}
	// 연속, Cycle이 X축이 아님
	else
	{
		strName = m_strChPath.Mid(m_strChPath.ReverseFind('\\')+1);
	}

	//
	if     (m_strAxisName.CollateNoCase("OCV")==0)          strName+=" OCV";
	else if(m_strAxisName.CollateNoCase("Voltage1")==0)     strName+=" V1";
	else if(m_strAxisName.CollateNoCase("Voltage2")==0)     strName+=" V2";
	else if(m_strAxisName.CollateNoCase("Voltage3")==0)     strName+=" V3";
	else if(m_strAxisName.CollateNoCase("Voltage4")==0)     strName+=" V4";
	else if(m_strAxisName.CollateNoCase("V_MaxDiff")==0)    strName+=" △V";
//ljb
// 	else if(m_strAxisName.CollateNoCase("AuxTemperature[000]")==0) 
	else if(m_strAxisName.Find("AuxT") > 0)// && m_strAxisName.Find("[000]") > 0) 
	{//20180620 yulee
// 		int nIndex = m_strAxisName.Find('[')+1;
// 		CString strTemp;
// 		strTemp.Format("AuxT_[%s]", m_strAxisName.Mid(nIndex, m_strAxisName.ReverseFind(']') - nIndex));
// 		strName+=strTemp;
	}
	else if(m_strAxisName.CollateNoCase("Temperature2")==0) strName+=" T2";
	else if(m_strAxisName.CollateNoCase("T_MaxDiff")==0)    strName+=" △T";
//ljb
// 	else if(m_strAxisName.CollateNoCase("AuxVoltage[000]") == 0)
	else if(m_strAxisName.Find("AuxV") > 0 && m_strAxisName.Find("[000]") > 0) 
	{
		int nIndex = m_strAxisName.Find('[')+1;
		CString strTemp;
		strTemp.Format("AuxV_[%s]", m_strAxisName.Mid(nIndex, m_strAxisName.ReverseFind(']') - nIndex));
		strName+=strTemp;
	}


	//////////////////////////////////////////////
	//ljb 201127 이재복]
	//ljb 2011223 이재복 //////////
	//yulee 20180831-2 
	if(m_strAxisName.Mid(5,3) == "CAN") //yulee 20180831-2 if(m_strAxisName.Mid(5,4) == "CAN[") //20180620 yulee Left(5) -> Mid(5,5)
	{
		//		int nIndex = m_strAxisName.Find('[')+1;
		//		CString strTemp;
		//		strTemp.Format("AuxV_[%s]", m_strAxisName.Mid(nIndex, m_strAxisName.ReverseFind(']') - nIndex));
		//strTemp = m_strAxisName.Left(8);
		strName+=m_strAxisName;//yulee 20180831-2
//		CString tmpStr;
//		tmpStr = strName;
//		strName.Format("%s%s", tmpStr.Mid(1, tmpStr.Find('[')-1), m_strAxisName.Mid(m_strAxisName.Find('['),31));
	//	strName+=m_strAxisName.Mid(m_strAxisName.Find(']')+1,31); //yulee 20180931-2
	}//////////////////////////////////////////////////////////////////////////
	else
	{
		//	if(m_strAxisName.Left(14) == "AuxTemperature")
		if((m_strAxisName.Mid(5,4) == "AuxT") && (m_strAxisName.Mid(5,5) != "AuxTH")) //20180611 yulee
		{
			// 		int nIndex = m_strAxisName.Find('[')+1;
			// 		CString strTemp;
			// 		strTemp.Format("AuxT_[%s]", m_strAxisName.Mid(nIndex, m_strAxisName.ReverseFind(']') - nIndex));
			// 		strName+=strTemp;
			strName+=m_strAxisName;//yulee 20180831-2
//			CString tmpStr;
//			tmpStr = strName;
//			strName.Format("%s%s", tmpStr.Mid(1, tmpStr.Find('[')-1), m_strAxisName.Mid(m_strAxisName.Find('['),31));
			//strName+=m_strAxisName.Mid(m_strAxisName.Find(']')+1,31);//ljb 2011223 이재복
		}
		
		if(m_strAxisName.Mid(5,5) == "AuxTH")//20180611 yulee 추가
		{
			// 		int nIndex = m_strAxisName.Find('[')+1;
			// 		CString strTemp;
			// 		strTemp.Format("AuxT_[%s]", m_strAxisName.Mid(nIndex, m_strAxisName.ReverseFind(']') - nIndex));
			// 		strName+=strTemp;
		strName+=m_strAxisName;//yulee 20180831-2
	//		CString tmpStr;
	//		tmpStr = strName;
	//		strName.Format("%s%s", tmpStr.Mid(1, tmpStr.Find('[')-1), m_strAxisName.Mid(m_strAxisName.Find('['),31));
		//	strName+=m_strAxisName.Mid(m_strAxisName.Find(']')+1,31);;//ljb 2011223 이재복
		}
		
		//ljb
		// 	if(m_strAxisName.Left(10) == "AuxVoltage")
		if(m_strAxisName.Mid(5,4) == "AuxV")
		{
			// 		int nIndex = m_strAxisName.Find('[')+1;
			// 		CString strTemp;
			// 		strTemp.Format("AuxV_[%s]", m_strAxisName.Mid(nIndex, m_strAxisName.ReverseFind(']') - nIndex));
			// 		strName+=strTemp;
			strName+=m_strAxisName;//yulee 20180831-2
	//		CString tmpStr;
	//		tmpStr = strName;
	//		strName.Format("%s%s", tmpStr.Mid(1, tmpStr.Find('[')-1), m_strAxisName.Mid(m_strAxisName.Find('['),31));
			//strName+=m_strAxisName.Mid(m_strAxisName.Find(']')+1,31);;//ljb 2011223 이재복
		}
		
		//ljb 201008 추가
		if(m_strAxisName.Mid(5,5) == "OvenT") //20180620 yulee Left(5) -> Mid(5,5)
		{
			strName += "OvenT";
		}
		if(m_strAxisName.Mid(5,5) == "OvenH") //20180620 yulee Left(5) -> Mid(5,5)
		{
			strName += "OvenH";
		}
	}

//	strName.MakeUpper();
	return strName;
}

int CLine::GetApproximatePos(fltPoint value)
{
	POSITION pos = m_DataList.GetHeadPosition();
	int i=0;
	fltPoint point;
	while(pos){
		point = m_DataList.GetNext(pos);
		if(point.x>=value.x) return i;
		i++;
	}
	return 0;
}

int CLine::GetIndexValByXAxisVal(fltPoint value) //20180710 yulee
{
	POSITION pos = m_DataList.GetHeadPosition();
	int i=0;
	fltPoint point;
	while(pos){
		point = m_DataList.GetNext(pos);
		if(point.x==value.x) 
			return i;
		i++;
	}
	return 0;
}

fltPoint CLine::GetYAxisValByIndex(int nIndex)
{
	POSITION pos = m_DataList.GetHeadPosition();
	int i=0;
	fltPoint point;
	while(pos){
		point = m_DataList.GetNext(pos);
		if(i == nIndex)
			return point;
		i++;
	}
	point.x=0;
	point.y=0;
	return point;
}

/*
//속도 향상을 위해 Memory 배열 사용 
fltPoint * CLine::GetDataArray(int &nSize)
{
	nSize = m_DataList.GetCount();
	return m_pData;
}

BOOL CLine::MakeDataArray()
{
	if(m_pData != NULL)
	{
		delete [] m_pData;
		m_pData = NULL;
	}

	int nCount = m_DataList.GetCount();
	if(nCount < 1)	return FALSE;

	m_pData = new fltPoint[nCount];
	memset(m_pData, 0, sizeof(fltPoint)*nCount);

	POSITION pos = m_DataList.GetHeadPosition();
	fltPoint fltData;
	for(int i=0; i<nCount; i++)
	{
		if(pos == NULL)	break;

		fltData = m_DataList.GetNext(pos);
		m_pData[i] = fltData;
	}

	return TRUE;
}
*/