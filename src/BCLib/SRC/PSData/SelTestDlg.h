/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: SelTestDlg.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CSelTestDialog class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_SELTESTDLG_H__BA067C5A_EE18_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_SELTESTDLG_H__BA067C5A_EE18_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelTestDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelTestDialog dialog

// #include

#include "resource.h"

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CSelTestDialog
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CSelTestDialog : public CDialog
{
// Construction
public:
	CSelTestDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelTestDialog)
	enum { IDD = IDD_TEST_LIST_DLG , IDD2 = IDD_TEST_LIST_DLG_ENG , IDD3 = IDD_TEST_LIST_DLG_PL };
	CListBox	m_NetworkErrorList;
	CListCtrl	m_TestList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelTestDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelTestDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnColumnclickTestList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkTestList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDataBring();
	afx_msg void OnDataDelete();
	afx_msg void OnDataMove();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDataDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
public:
	CDaoDatabase* m_pDatabase;
	CStringList   m_TestPathList;
	CStringList   m_RunningTestNameList;
	BYTE          m_byMode;
	CString		  m_strSearchKey;
private:
	CImageList * m_pLargeImage;
	CImageList * m_pSmallImage;
	BOOL         m_bNameSortToggle;
	BOOL         m_bDateSortToggle;
	CStringList  m_UnitPathList;

private:
	BOOL BringTestDataIntoDataPC(CString& strTestName, CStringList* pList);
	COleDateTime GetTestStartTime(LPCTSTR path);
	static int CALLBACK TestNameASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK TestDateASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK TestNameDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK TestDateDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

public:
	enum{
		MODE_MANAGEMENT = 0x01,
		MODE_SELECTION  = 0x02,
		MODE_AUTOLOAD   = 0x04,
		MODE_SEARCH		= 0x08
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELTESTDLG_H__BA067C5A_EE18_11D4_88DF_006008CEDA07__INCLUDED_)
