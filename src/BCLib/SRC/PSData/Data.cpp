/***********************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Data.cpp

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: Member functions of CData class are defined
***********************************************************************/

// Data.cpp: implementation of the CData class.
//
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// #include

#include "stdafx.h"
#include "Data.h"
//#include "ChData.h"
#include "Plane.h"
#include "Line.h"
#include "SortTypeDlg.h"
#include "SelTestDlg.h"
#include "QuerySaveDlg.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
	Name:    CData
	Comment: Constructor of CData class
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CData::CData()
{
	m_BackColor = RGB(255,255,255);
	m_bShowTitle = TRUE;
	m_bFirstTitle = TRUE; //lyj 20210203
}

/*
	Name:    ~CData
	Comment: Destructor of CData class
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CData::~CData()
{
	// Plane list를 비우면서 각 plane에 할당된 memory를 해제한다.
	while(!m_PlaneList.IsEmpty())
	{
		CPlane* pPlane = m_PlaneList.RemoveTail();
		delete pPlane;
	}

	// Channel Data list를 비우면서 각 data에 할당된 memory를 해제한다.
	while(!m_ChDataList.IsEmpty())
	{
		CChData* pChData = m_ChDataList.RemoveTail();
		delete pChData;
	}
}

/*
	Name:    QueryData
	Comment: User로부터 Data의 Query조건을 받아서 조건에 맞게 Data를 가져와
	         Plane을 구성하고 각 Plane의 Line을 만들어 준다.
	Input:   Data가 위치한 folder들의 Path
	         -> 여러 folder인 경우, '\n'구분
			 -> 없는 경우, NULL
	Output:  사용자로부터 Query조건을 받았는지 여부
	         -> User가 "데이터조건 대화상자"를 "OK"버튼으로 닫으면 TRUE
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoo n  2001.06.01  The first settlement
*/
//20100430
BOOL CData::QueryData(LPCTSTR strPaths, BOOL bReload, CDWordArray *pSelRange, long lQueryModel)  // 20090909  KHM 데이터 reload 수정 for SDI ELAB 요청사항.
{
	//////////////////////////////////////////////////////
	// Step 1. Data가 위치한 folder들을 Drag & Drop으로 //
	//         Main Frame으로 가져왔을 때,              //
	//////////////////////////////////////////////////////

    //--------------------------------------------------------------------
	if (strPaths != NULL)
	{
		CString str(strPaths);
		int p1 = 0, p2 = 0;
		int s1;
		while (TRUE)
		{
			// '\n'로 분리된 각 folder의 Path를 얻어서
			p2 = str.Find('\n', p1);
			if (p2 < 0)
			{
				break;
			}

			CString strPath = str.Mid(p1, p2 - p1);
			p1 = p2 + 1;
			
			m_QueryCondition.AddChPath(strPath);
			CString strTestName;
			BOOL bFind = FALSE;
			
			s1 = strPath.ReverseFind('\\');
			strTestName = strPath.Left(s1);
			POSITION pos = m_QueryCondition.m_dlgQuery.m_strlistTestPath.GetHeadPosition();

			while (pos)
			{	
				CString strTemp;
				strTemp = m_QueryCondition.m_dlgQuery.m_strlistTestPath.GetNext(pos);
				if (strTemp == strTestName)	//이미 추가된 시험명 
				{
					bFind = TRUE;
					break;
				}
			}

			if (bFind == FALSE)
			{
				m_QueryCondition.m_dlgQuery.m_strlistTestPath.AddTail(strTestName);
			}
		}
	}

//--------------------------------------------------------------------

	int nNewQuery = m_QueryCondition.m_dlgQuery.m_strlistTestPath.GetCount();

	//////////////////////////////////////////////////////
	// Step 2. 사용자로부터 데이터 Query 조건을 받는다. //
	//////////////////////////////////////////////////////
	//Reload가 아니면 사용자 설정을 받아 들인다.
	if (!bReload)
	{
		//ljb 20101228 수정 해야 함
		m_strTestName = m_QueryCondition.m_strTestName;						//가장 마지막 선택 작업명 

		//20100430
		if (!m_QueryCondition.GetUserSelection(pSelRange, lQueryModel))
		{
			return FALSE;	//사용자 취소 
		}

		// Added by 224 (2013/12/24) : 주어진 범위의 레코드 수를 산정하여 전체 레코드수를 보여준다.
		// .1. 주어진 STEP END 의 수

		// .2. STEP END 의 수 별 레코드의 수

		// .3. 수정한 STEP END

		// .4. Y 축 수
		INT nYAxis = m_QueryCondition.GetYAxisCount() ;

	}

// 사용자에게 채널 및 구간을 선택받았다.
// "그리기" 를 선택까지 마쳤다.
//--------------------------------------------------------------------

// Plane 정리.........
	////////////////////////////////////
	// Step 3. Plane List를 작성한다. //
	////////////////////////////////////
	// 기존의 Plane이 있는 경우,
	CPlane* pPlane;
	POSITION pos;
	if (!m_PlaneList.IsEmpty())
	{
		// 첫 Plane의 X축 Title과 User가 선택한 X축 Title이 다르면
		CString strTitle = m_PlaneList.GetHead()->GetXAxis()->GetTitle();
		if (m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase(strTitle)!=0)
		{
			// X축이 변경된 경우는 모두 삭제 하고 새로 그린다.
			//기존의 Plane을 모두 지운다.
			while (!m_PlaneList.IsEmpty())
			{
				pPlane = m_PlaneList.RemoveTail();
				delete pPlane;
				pPlane = NULL;
			}
		}
		else
		{
			// User가 선택한 X축이 이전 Plane의 것과 같다면
			// Y축이 다른 Plane을 일단 속아내어 지운다.
			pos = m_PlaneList.GetHeadPosition();
			while (pos)
			{
				POSITION PrePos = pos;
				// Plane을 한개씩 꺼내어
				pPlane = m_PlaneList.GetNext(pos);

				// 그 Y Axis가 User의 선택에 있으면,
				CAxis* pAxis = m_QueryCondition.IsThereThisYAxis(pPlane->GetYAxis()->GetPropertyTitle());
				if (pAxis != NULL)
				{
					// X Axis와 Y Axis의 Unit를 새로 조정한다.(Unit이 변경 되었을 수도 있을므로)
					*pPlane->GetXAxis() = *m_QueryCondition.GetXAxis();
					*pPlane->GetYAxis() = *pAxis;
				}
				else
				{
					// Y Axis가 User의 선택에 없으면,
					// 해당 Plane을 리스트에서 지운다.
					pPlane = m_PlaneList.GetAt(PrePos);
					m_PlaneList.RemoveAt(PrePos);
					delete pPlane;
				}
			}
		}
	}

	// 기존에 없는 Plane이면 사용자의 선택에 따라 새 Plane을 만든다.
	pos = m_QueryCondition.GetYAxisHead();
	while (pos)
	{
		// 사용자가 선택한 Y축 중에서 기존의 Plane에 없는 것을 찾아
		CAxis* pYAxis = m_QueryCondition.GetNextYAxis(pos);
		if (!IsTherePlaneWithThisYAxis(pYAxis->GetPropertyTitle()))
		{
			// 새 Plane을 만들고 list에 넣는다.
			pPlane = new CPlane;
			*pPlane->GetXAxis() = *m_QueryCondition.GetXAxis();
			*pPlane->GetYAxis() = *pYAxis;
			if (!pPlane->SetColorOption(m_PlaneList.GetCount() % 2 == 0))
			{
				delete pPlane;		//database에서 색상 정보를 Loading한다.
			}
			else
			{
				m_PlaneList.AddTail(pPlane);
			}
		}
	}

// Plane list 정리
//--------------------------------------------------------------------

	// 각 Plane의 Serial No를 다시 부여한다.
	for (int i = 0; i < m_PlaneList.GetCount(); i++)
	{
		pPlane = m_PlaneList.GetAt(m_PlaneList.FindIndex(i));
		pPlane->SetSerialNo(i);
		
		//새로 그리기일 경우는 첫번째 Plane의 Y축 Grid를 표시하고 나머지는 표시하지 않는다.
		if (nNewQuery <= 0 || bReload == FALSE)
		{
			if (i == 0)
			{
				pPlane->GetYAxis()->SetShowGrid(TRUE);
			}
			else
			{
				pPlane->GetYAxis()->SetShowGrid(FALSE);
			}
		}
		
		//새로 그리기, Reload, 사용자 Option 변경시 Zoom되어 있는 상태이면 Zoom Off시켜야 한다.
		pPlane->ZoomOff();
	}

//--------------------------------------------------------------------

	////////////////////////////////////////////
	// Step 4. 각 Plane에 Line을 만들어 준다. //
	////////////////////////////////////////////
	// Added by 224 (2014/02/13) : 기존 코드는 마직막 pPlane의 List 만 제거됨
	// 모든 Plane 의 라인을 제거
	for (int i = 0; i < m_PlaneList.GetCount(); i++)
	{
		pPlane = m_PlaneList.GetAt(m_PlaneList.FindIndex(i));
		
		if (!m_QueryCondition.IsModifyingMode()) 
		{
			pPlane->DeleteAllLines();
		}
		// Modify 해야 되는 상황이면 일단 현 User선택에 없는 Line들을 삭제한다.
		else
		{
			POSITION LinePos = pPlane->GetLineHead();
			while (LinePos)
			{
				POSITION PrePos = LinePos;
				CLine* pLine = pPlane->GetNextLine(LinePos);
				if (!m_QueryCondition.IsThereThisLine(pLine->GetChPath()
					, pLine->GetCycle()
					, pLine->GetMode()
					, pLine->GetAxisName()))
				{
					pPlane->RemoveLineAt(PrePos);
				}
			}
		}
	}
/*
: 기존코드
	if (!m_QueryCondition.IsModifyingMode()) 
	{
		pPlane->DeleteAllLines();
	}
	// Modify 해야 되는 상황이면 일단 현 User선택에 없는 Line들을 삭제한다.
	else
	{
		POSITION LinePos = pPlane->GetLineHead();
		while (LinePos)
		{
			POSITION PrePos = LinePos;
			CLine* pLine = pPlane->GetNextLine(LinePos);
			if (!m_QueryCondition.IsThereThisLine(pLine->GetChPath()
												, pLine->GetCycle()
												, pLine->GetMode()
												, pLine->GetAxisName()))
			{
				pPlane->RemoveLineAt(PrePos);
			}
		}
	}
*/

//----------------- ljb add start 20130215
	POSITION ChPos;
	CString strChPath;

	// Added by 224 : For debugging...
	// 선택한 작업의 수 만큼 반환 (일반적으로 하나?)
	INT nNumberOfChPath = m_QueryCondition.GetChPathCount() ;

	INT nCnt = 0 ;
	ChPos = m_QueryCondition.GetChPathHead();
	while (ChPos)
	{
		nCnt++ ;
		strChPath = m_QueryCondition.GetNextChPath(ChPos);

		// Added by 224 : For debugging...
		// 화면 좌측에 그려줄 Y 축 수
		// 각 축별로 RAW 파일을 읽어서 그래프를 그려준다.
		// 호출 ROW = STEP END Record 수 * nNumberOfPlane
		INT nNumberOfPlane = m_PlaneList.GetCount() ;

//----------------- ljb add end 20130215
		pos = m_PlaneList.GetHeadPosition();
		while (pos)
		{
			pPlane = m_PlaneList.GetNext(pos);

			// Modify 해야 되는 상황이 아니면, 모든 Line들을 삭제한다.
// 			if (bReload)
// 			{
// 				ReloadDataAtPlane(pPlane);
// 			}
// 			else	//새로 그리기, 사용자 Option일 경우 
// 			{
				//////////////////////////////////////////////////////////////////////////
				// + BW KIM 2014.03.12
				pPlane->SetCurLineColorIndex(0);
				// -
				//////////////////////////////////////////////////////////////////////////
				// 이제 새 Line들을 Plane에 추가한다.
				AddLinesToPlane(strChPath, pPlane); //yulee 20180903 확인 

//			}
			
			//X, Y축을 Scale에 맞게 재조정 
			pPlane->AutoScaleYAxis(m_PlaneList.GetCount());
		}
	}

//	TRACE("DrawTime elapsed %d msec\n", GetTickCount()- nStartTime);
	TRACE("nCnt = %d\n", nCnt) ;

	return TRUE;		
}

/*
	Name:    LongCompare
	Comment: long형의 두 값의 크기를 비교
	Input:   한 수의 포인터, 다른 한 수의 포인터
	Output:  크기를 비교한 결과
	         +1: 앞수>뒷수
			  0: 앞수=뒷수
			 -1: 압수<뒷수
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
int CData::LongCompare(const void *arg1, const void *arg2)
{
	if (*((long*)arg1)> *((long*)arg2)) return 1;
	if (*((long*)arg1)==*((long*)arg2)) return 0;
    return -1;
}

/*
	Name:    FloatCompare
	Comment: float형의 두 값의 크기를 비교
	Input:   한 수의 포인터, 다른 한 수의 포인터
	Output:  크기를 비교한 결과
	         +1: 앞수>뒷수
			  0: 앞수=뒷수
			 -1: 압수<뒷수
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
int CData::FloatCompare(const void *arg1, const void *arg2)
{
	if (*((float*)arg1)> *((float*)arg2)) return 1;
	if (*((float*)arg1)==*((float*)arg2)) return 0;
    return -1;
}

/*
	Name:    IsTherePlaneWithThisYAxis
	Comment: long형의 두 값의 크기를 비교
	Input:   LPCTSTR strTitle - YAxis의 Propety 이름
	Output:  Plane List에 입력으로 들어온 Property 이름을 가진
	         Plane이 있는지 여부
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
BOOL CData::IsTherePlaneWithThisYAxis(LPCTSTR strTitle)
{
	POSITION pos = m_PlaneList.GetHeadPosition();
	while(pos)
	{
		if (m_PlaneList.GetNext(pos)->GetYAxis()->GetPropertyTitle().CompareNoCase(strTitle)==0)
		{
			return TRUE;
		}
	}

	return FALSE;
}

/*
	Name:    AddLinesToPlane
	Comment: 데이터 Query 조건에 따라 데이터를 불러와 Line을 만들어 Plane에 넣어줌
	Input:   Plane의 pointer
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::AddLinesToPlane(CString strChPath1, CPlane* pPlane)
{
	////////////////////////////////////////////////////////////////////
	//	Function 구성                                                 //
	//                                                                //
	//	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();          //
	//	while(YAxisPos)                                               //
	//	{                                                             //
	//		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		/////////////////                                         //
	//		// CASE 1. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 2. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 3. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//	}                                                             //
	////////////////////////////////////////////////////////////////////

	// Data Query조건에서 User가 선택한 Y축을 하나씩 꺼내어

	CAxis* pYAxis;
	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
	while (YAxisPos)
	{
		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		// 그 Property 이름이 pPlane이 가르키는 plane object의
		// Y축의 property 이름과 다르면, Loop 마지막으로 continue!
		pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
		TRACE("pYAxis = %s, pPlane = %s \n",pYAxis->GetPropertyTitle(),pPlane->GetYAxis()->GetPropertyTitle());	//ljb add 20131015

		CString strYAxisTitle = pYAxis->GetPropertyTitle() ;
		CString strPlaneTitle = pPlane->GetYAxis()->GetPropertyTitle() ;
		if (pYAxis->GetPropertyTitle().CompareNoCase(pPlane->GetYAxis()->GetPropertyTitle()) != 0)
		{
			continue;
		}

		// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
		/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
		/*♠*/ CProgressCtrl ProgressCtrl;
		/*♠*/ CRect rect;
		/*♠*/ pStatusBar->GetItemRect(0,rect);
		/*♠*/ rect.left = rect.Width()/2;
		/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
		/*♠*/ ProgressCtrl.SetRange(0,m_QueryCondition.GetChPathCount());

		// 다음의 3가지 경우에 따라 데이터를 loading하여 line을 만들어
		// 새 라인의 List를 작성한다.
//		CTypedPtrList<CPtrList, CLine*> NewLineList;

		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		////////////////////////////////
		// CASE 1. X축이 cycle인 경우 //
		////////////////////////////////

		//CString strChPathTemp;
		CString strMsg;
		POSITION ChPos, ModePos, LinePos, CyclePos;

		if (m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("Cycle") == 0)
		{
			DWORD dwMode;

// 			ChPos = m_QueryCondition.GetChPathHead();
// 			while(ChPos)
			{
				CList<DWORD, DWORD&> TempList;
//				strChPath = m_QueryCondition.GetNextChPath(ChPos);
				//
				//lmh 20111019 추가
				CList<DWORD, DWORD&> ModeList1;
				m_QueryCondition.GetModeList(&ModeList1, strChPath1, pYAxis->GetTitle());
								
				CList<DWORD, DWORD&> ModeList;
				//				m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());
				ModePos = ModeList1.GetHeadPosition();
				DWORD dwFlagMode = ModeList1.GetNext(ModePos);
				for(int i=0; i<16; i++) //lmh 20111019 선택된 모드별로 리스트에 추가되게 한다.
				{
					dwMode = 0x01<<i;
					if (dwMode & dwFlagMode)
					{
						ModeList.AddTail(dwMode);
					}
				}
				//lmh////////////////////////////////////////////////////////////////////////
				
				ModePos = ModeList.GetHeadPosition();
				
				while (ModePos)
				{
					dwMode = ModeList.GetNext(ModePos);
					if (!pPlane->IsThereThisLine(strChPath1, 0, dwMode, pYAxis->GetTitle())) 
					{
						TempList.AddTail(dwMode);
					}
///************************************************* ljb 20131204 add start
// 				}
// 				if(pYAxis->GetTitle().Left(4) == "AuxT" || pYAxis->GetTitle().Left(4) == "AuxV")//lmh 20120726
// 				{
// 					POSITION auxPos = m_QueryCondition.GetYAuxAxisHead();
// 					while(auxPos)
// 					{
// 						CAxis * pAuxAxis = m_QueryCondition.GetNextYAuxAxis(auxPos);
// 						if(pAuxAxis->GetTitle().Left(4) != pAuxAxis->GetTitle().Left(4)) continue;
// 						/*♠*/ strMsg.Format("Loading data %s-%s %s",
// 						/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
// 						/*♠*/ 			  pAuxAxis->GetTitle(),
// 						/*♠*/ 			  strChPath1.Mid(strChPath.ReverseFind('\\')+1));
// 						/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
// 						/*♠*/ ProgressCtrl.SetRange(0,(short)(TempList.GetCount()*m_QueryCondition.GetCycleCount()));
// 						/*♠*/ int nPos = 0;
// 						
// 						LinePos = TempList.GetHeadPosition();
// 						LONG lCycle;
// 						fltPoint pt;
// 						while(LinePos)
// 						{
// 							dwMode = TempList.GetNext(LinePos);
// 							CLine* pLine = new CLine(strChPath1, 0, dwMode, pPlane->GetNextLineColor(), pAuxAxis->GetTitle());
// 							//
// 							CyclePos = m_QueryCondition.GetCycleHead();
// 							while(CyclePos)
// 							{
// 								lCycle = m_QueryCondition.GetNextCycle(CyclePos);
// 								//해당 Cycle에서 dwMode 조건에 맞는 모든 data Point 추가
// 								if(GetLastDataOfTable(pt, strChPath1, lCycle, dwMode, pAuxAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
// 								{
// 									if(pYAxis->GetTitle().Left(4) == "AuxT" && pt.y > 1000)
// 										pt.y = pt.y/1000;
// 									pLine->AddData(pt);
// 								}	
// 								
// 								/*♠*/ nPos++;
// 								/*♠*/ ProgressCtrl.SetPos(nPos);
// 							}
// 							
// 							//
// 							if(pLine->GetDataCount()>0) NewLineList.AddTail(pLine);
// 							else                        delete pLine;
// 							
// 						}
// 					}
// 				}
// 				else
// 				{
//********************************************************************************************* ljb 20131204 add end

					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath1.Mid(strChPath1.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(TempList.GetCount()*m_QueryCondition.GetCycleCount()));
					/*♠*/ int nPos = 0;
					
					TRACE("%s", strMsg);	//20130215 add

					LinePos = TempList.GetHeadPosition();
					LONG lCycle;
					fltPoint pt;
					while (LinePos)
					{
						dwMode = TempList.GetNext(LinePos);
						CLine* pLine = new CLine(strChPath1, 0, dwMode, pPlane->GetNextLineColor(), pYAxis->GetTitle());
						//
						CyclePos = m_QueryCondition.GetCycleHead();
						while (CyclePos)
						{
							lCycle = m_QueryCondition.GetNextCycle(CyclePos);
							//해당 Cycle에서 dwMode 조건에 맞는 모든 data Point 추가
							if (GetLastDataOfTable(pt, strChPath1, lCycle, dwMode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
							{
								pLine->AddData(pt);
							}	
							
							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}
						
						//
						if (pLine->GetDataCount()>0) 
						{
							//pPlane->AddLine(pLine);
							if (pPlane->IsThereThisLine(strChPath1, 0, dwMode, pYAxis->GetTitle()) == FALSE)	//ljb 20170817 edit
								pPlane->AddLine(pLine);
							else
								delete pLine;

						}
						else
						{
							delete pLine;
						}						
					}
				}

				ModeList.RemoveAll();
			}
		}
		else // <- if (m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("cycle")==0)
		{

			///////////////////////////////////////////////////////////
			// CASE 2. X축이 cycle이 아니고 시작type이 '연속'인 경우 //
			///////////////////////////////////////////////////////////
			if (m_QueryCondition.GetStartPointOption() == 0)
			{
				LONG lCycle;
				
				
				/*CString strTemp = pPlane->GetYAxis()->GetPropertyTitle().Left(4);*/ //20180609 yulee
				CString strTemp = pPlane->GetYAxis()->GetPropertyTitle().Left(5); //20180609 yulee
				CLine* pLine;
				TRACE("%s,%s \n",pPlane->GetYAxis()->GetPropertyTitle(),strTemp);
				/*if (pYAxis->GetTitle() == "AuxT" || pYAxis->GetTitle() == "AuxV")*/ //20180609 yulee
				//if (pYAxis->GetTitle() == "AuxT" || pYAxis->GetTitle() == "AuxV" || pYAxis->GetTitle() == "AuxTH") //20180609 yulee
				if (pYAxis->GetTitle() == "AuxT"	||
					pYAxis->GetTitle() == "AuxV"	||
					pYAxis->GetTitle() == "AuxTH"	||
					pYAxis->GetTitle() == "AuxH"	|| //ksj 20200206 : v1016 습도 센서 추가
					pYAxis->GetTitle() == "CAN"		||
					pYAxis->GetTitle() == "CANs1"	||
					pYAxis->GetTitle() == "CANs2"	||
					pYAxis->GetTitle() == "CANs3"	||
					pYAxis->GetTitle() == "CANs4"	) //yulee 20180831-2
				{
				
					//if (pYAxis->GetTitle() == "AuxT" || pYAxis->GetTitle() == "AuxV" || pYAxis->GetTitle() == "AuxTH" || pYAxis->GetTitle() == "CAN") //yulee 20180831-2
					//{
					//yulee 20180729 그리드에 단위 이상 수정차 주석처리
					//BOOL bIsAuxT = 	(pYAxis->GetTitle() == "AuxT")? TRUE : FALSE; //ksj 20170832 : AUXT 단위 버그 수정

					TRACE("%s,%s \n",pPlane->GetYAxis()->GetPropertyTitle(),strTemp);
					//ljb 20131213 AuxV, AuxT 라인 그리기
					POSITION YAxisAuxPos;
					if (pYAxis->GetTitle() == "AuxV") YAxisAuxPos= m_QueryCondition.GetYAuxAxisVoltHead();
					if (pYAxis->GetTitle() == "AuxT") YAxisAuxPos= m_QueryCondition.GetYAuxAxisTempHead();
					if (pYAxis->GetTitle() == "AuxTH") YAxisAuxPos= m_QueryCondition.GetYAuxAxisTempTHHead(); //20180609 yulee
					if (pYAxis->GetTitle() == "AuxH") YAxisAuxPos= m_QueryCondition.GetYAuxAxisHumiHead(); //ksj 20200206 : v1016 습도 센서
					if ((pYAxis->GetTitle() == "CAN")&&
						((pYAxis->GetTitle() != "CANs1")	||
						(pYAxis->GetTitle() != "CANs2")		||
						(pYAxis->GetTitle() != "CANs3")		||
						(pYAxis->GetTitle() != "CANs4"))) //yulee 20181009
						YAxisAuxPos= m_QueryCondition.GetYAuxAxisCANHead(); //20180609 yulee 20180903
				
					if (pYAxis->GetTitle() == "CANs1") YAxisAuxPos = m_QueryCondition.GetYAuxAxisCANS1Head(); //yulee 20180831-2
					if (pYAxis->GetTitle() == "CANs2") YAxisAuxPos = m_QueryCondition.GetYAuxAxisCANS2Head(); //yulee 20180831-2
					if (pYAxis->GetTitle() == "CANs3") YAxisAuxPos = m_QueryCondition.GetYAuxAxisCANS3Head(); //yulee 20180831-2
					if (pYAxis->GetTitle() == "CANs4") YAxisAuxPos = m_QueryCondition.GetYAuxAxisCANS4Head(); //yulee 20181009
					
					
					//if (pYAxis->GetTitle() == "CAN") YAxisAuxPos= m_QueryCondition.GetYAuxAxisCANHead(); //20180609 yulee
					CAxis* pYAxisAux;
					while(YAxisAuxPos)
					{
						if (pYAxis->GetTitle() == "AuxV") pYAxisAux = m_QueryCondition.GetNextYAuxAxisVolt(YAxisAuxPos);
						if (pYAxis->GetTitle() == "AuxT") pYAxisAux = m_QueryCondition.GetNextYAuxAxisTemp(YAxisAuxPos);
						if (pYAxis->GetTitle() == "AuxTH") pYAxisAux = m_QueryCondition.GetNextYAuxAxisTempTH(YAxisAuxPos); //20180609 yulee
						if (pYAxis->GetTitle() == "AuxH") pYAxisAux = m_QueryCondition.GetNextYAuxAxisHumi(YAxisAuxPos); //ksj 20200206 : v1016 습도 센서
						if ((pYAxis->GetTitle() == "CAN")&&
							((pYAxis->GetTitle() != "CANs1")||
							(pYAxis->GetTitle() != "CANs2")||
							(pYAxis->GetTitle() != "CANs3") ||
							(pYAxis->GetTitle() != "CANs4")))pYAxisAux= m_QueryCondition.GetNextYAuxAxisCAN(YAxisAuxPos); //20180609 yulee
						if (pYAxis->GetTitle() == "CANs1") pYAxisAux = m_QueryCondition.GetNextYAuxAxisCANS1(YAxisAuxPos); //yulee 20180831-2
						if (pYAxis->GetTitle() == "CANs2") pYAxisAux = m_QueryCondition.GetNextYAuxAxisCANS2(YAxisAuxPos); //yulee 20180831-2
						if (pYAxis->GetTitle() == "CANs3") pYAxisAux = m_QueryCondition.GetNextYAuxAxisCANS3(YAxisAuxPos); //yulee 20180831-2
						if (pYAxis->GetTitle() == "CANs4") pYAxisAux = m_QueryCondition.GetNextYAuxAxisCANS4(YAxisAuxPos); //yulee 20181009
					
					//	if (pYAxis->GetTitle() == "CAN") pYAxisAux = m_QueryCondition.GetNextYAuxAxisCAN(YAxisAuxPos); //yulee 20180831-2
					
						TRACE("pYAxis = %s, pPlane = %s \n",pYAxisAux->GetPropertyTitle(),pPlane->GetYAxis()->GetPropertyTitle());	//ljb add 20131015 //yulee 20180903 확인 

						pLine = new CLine(strChPath1, 0, 0, pPlane->GetNextLineColor(), pYAxisAux->GetTitle());
						float fltSum = 0.0f;
						//
						CList<DWORD, DWORD&> ModeList;
						m_QueryCondition.GetModeList(&ModeList, strChPath1, pYAxisAux->GetTitle());
						
						/*♠*/ strMsg.Format("Loading data %s-%s %s",
						/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
						/*♠*/ 			  pYAxisAux->GetTitle(),
						/*♠*/ 			  strChPath1.Mid(strChPath1.ReverseFind('\\')+1));
						/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
						/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*m_QueryCondition.GetCycleCount()));
						/*♠*/ int nPos = 0;

						INT nLoopOfCycle = 0 ;
						LONG lCycleCount = m_QueryCondition.GetCycleCount() ;
						CyclePos = m_QueryCondition.GetCycleHead();
						while(CyclePos)
						{
// Added by 224 (2013/12/24) : 디버깅을 위해 추가
//--------------------------------------------
							nLoopOfCycle++ ;
							TRACE("nLoopOfCycle = %d\n", nLoopOfCycle) ;
							
							CString str ;
							str.Format(" (%d / %d)", nLoopOfCycle, lCycleCount) ;
							
							((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg + str);
//--------------------------------------------

							lCycle = m_QueryCondition.GetNextCycle(CyclePos);
							
							ModePos = ModeList.GetHeadPosition();
							while(ModePos)
							{
								DWORD dwMode = ModeList.GetNext(ModePos);
								LONG      lDataNum = 0L;

								fltPoint* pfltPt = GetDataOfTable(lDataNum,
									strChPath1, lCycle, dwMode, 
									pPlane->GetXAxis()->GetTitle(),
									pYAxisAux->GetTitle());

// 								fltPoint* pfltPt_day = GetDataOfTable(lDataNum, 
// 									strChPath1, lCycle, dwMode, 
// 									pPlane->GetXAxis()->GetTitle(),
// 									RS_COL_TIME_DAY);

								TRACE("%s\n",pPlane->GetXAxis()->GetTitle());
								
								TRACE("2: %s Draw data size is %d\n", strChPath1, lDataNum);
								if(lDataNum>0 && pfltPt!=NULL)
								{
									float pre_pt = 0.0f;
									for(int i=0; i<lDataNum; i++)
									{
										fltPoint pt = pfltPt[i];
										pt.x += fltSum;

										pLine->AddData(pt);
										
//										TRACE("day: %f, time : %f\n",pt2.y, pt2.x);


//										pLine->AddData(pt);
										//TRACE("Add %d :: %f\n", i+1, pfltPt[i].x);
									}
									
									//20090911 Mode가 바뀌는 부분 Indexing
									pLine->AddModeDataCount(pLine->GetDataCount());
									
									if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
										fltSum += pfltPt[lDataNum-1].x;
									
									delete [] pfltPt;
//									delete [] pfltPt_day;
								}
								/*♠*/ nPos++;
								/*♠*/ ProgressCtrl.SetPos(nPos);
							}
						}
						if(pLine->GetDataCount()>0)
						{
							pPlane->AddLine(pLine);
						}
						else
						{
							delete pLine;
						}
					}
				}
				else
				{
					pLine = new CLine(strChPath1, 0, 0, pPlane->GetNextLineColor(), pYAxis->GetTitle());
					float fltSum = 0.0f;
					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath1, pYAxis->GetTitle());
					
					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath1.Mid(strChPath1.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*m_QueryCondition.GetCycleCount()));
					/*♠*/ int nPos = 0;
					
					CyclePos = m_QueryCondition.GetCycleHead();
					while(CyclePos)
					{
						lCycle = m_QueryCondition.GetNextCycle(CyclePos);
						
						ModePos = ModeList.GetHeadPosition();
						while(ModePos)
						{
							DWORD dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt = GetDataOfTable(lDataNum,
								strChPath1, lCycle, dwMode, 
								pPlane->GetXAxis()->GetTitle(),
								pYAxis->GetTitle());

// 							fltPoint* pfltPt_day = GetDataOfTable(lDataNum, 
// 								strChPath1, lCycle, dwMode, 
// 								pPlane->GetXAxis()->GetTitle(),
// 								RS_COL_TIME_DAY);

							TRACE("2: %s Draw data size is %d\n", strChPath1, lDataNum);
							if(lDataNum>0 && pfltPt!=NULL)
							{
								float pre_pt = 0.0f;
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;

									pLine->AddData(pt);
									//TRACE("Add %d :: %f\n", i+1, pfltPt[i].x);
								}
								
								//20090911 Mode가 바뀌는 부분 Indexing
								pLine->AddModeDataCount(pLine->GetDataCount());
								
								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;
								
								delete [] pfltPt;
//								delete [] pfltPt_day;
							}
							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}
					}
					if(pLine->GetDataCount()>0)
					{
						pPlane->AddLine(pLine);
					}
					else
					{
						delete pLine;
					}
				}
				
			}

			///////////////////////////////////////////////////////////////
			// CASE 3. X축이 cycle이 아니고 시작type이 '영점시작'인 경우 //
			///////////////////////////////////////////////////////////////
			else if (m_QueryCondition.GetStartPointOption() == 1)
			{
				ChPos = m_QueryCondition.GetChPathHead();			
				bool bCapa = false;
				if (pPlane->GetXAxis()->GetTitle() == "Capacity")
				{
					bCapa = true;
				}

				while (ChPos)
				{
					strChPath1 = m_QueryCondition.GetNextChPath(ChPos);
					//
					CList<LONG, LONG&> TempList;
					CyclePos = m_QueryCondition.GetCycleHead();
					while (CyclePos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CyclePos);
						if (!pPlane->IsThereThisLine(strChPath1, lCycle, 0, pYAxis->GetTitle()))
						{
							TempList.AddTail(lCycle);
						}
					}

					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath1, pYAxis->GetTitle());
					
					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath1.Mid(strChPath1.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*TempList.GetCount()));
					/*♠*/ int nPos = 0;
					
					LinePos = TempList.GetHeadPosition();
					while (LinePos)
					{
						LONG lCycle = TempList.GetNext(LinePos);
						//
						CLine* pLine = new CLine(strChPath1, lCycle, 0, pPlane->GetNextLineColor(), pYAxis->GetTitle());
						float fltSum = 0.0f;
						//
						ModePos = ModeList.GetHeadPosition();
						DWORD dwMode;
						while (ModePos)
						{
							dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt =GetDataOfTable(lDataNum,
								strChPath1, lCycle, dwMode, 
								pPlane->GetXAxis()->GetTitle(),
								pYAxis->GetTitle());
// 								GetAuxTitleOfIndex(int nAuxIndex));
// 
// 
// 							bWork          = afinder.FindNextFile();
// 							
// 							if(afinder.IsDirectory()&&!afinder.IsDots())
// 							{
// 								long lLastTableSize = 0;	//ljb 20101228
// 								
// 								// Added by 224 (2014/02/03) :
// 								CChData chData(afinder.GetFilePath()) ;
// 								
// 				CTable *record = CChData::GetLastDataRecord(afinder.GetFilePath(), lLastTableSize, &chData);
// 
// 
// CTable *record = CChData::GetLastDataRecord(afinder.GetFilePath(), lLastTableSize, &chData);
// CString strTitle = record->GetAuxTitleOfIndex(i);




							TRACE("3: %s Draw data size is %d\n", strChPath1, lDataNum);
							if (lDataNum>0 && pfltPt!=NULL)
							{
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;
									pLine->AddData(pt);
								}
								
								// 								if (pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
								// 									fltSum += pfltPt[lDataNum-1].x;
								//20090911 Mode가 바뀌는 부분 Indexing
								pLine->AddModeDataCount(pLine->GetDataCount());
								
								if (pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;
								
								delete [] pfltPt;
							}
							
							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}
						
						// Added by 224 (2014/02/09) : Cycle 구간 영점시작 이상 현상 수정 시도
//--------------------------------------------------------
						if (pLine->GetDataCount()>0)
						{
							//pPlane->AddLine(pLine);
							if (pPlane->IsThereThisLine(strChPath1, lCycle, 0, pYAxis->GetTitle()) == FALSE)	//ljb 20170817 edit
								pPlane->AddLine(pLine);
							else
								delete pLine;

						}
						else
						{
							delete pLine;
						}
//--------------------------------------------------------

						//
// 						if (pLine->GetDataCount()>0) 
// 							NewLineList.AddTail(pLine);
// 						else
// 							delete pLine;
					}	// end of while (LinePos)
				}	// end of while (ChPos)
			} // end of else if (m_QueryCondition.GetStartPointOption() == 1)
		} // end of else

		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

// 		CLine *pLine;
// 		while(!NewLineList.IsEmpty())
// 		{
// 			pLine = NewLineList.RemoveHead();			
// 			pPlane->AddLine(pLine);
// 		}

		/*♠*/ ProgressCtrl.DestroyWindow();
		//Added by KBH 2005/10/31  
		/*♠*/ strMsg.Empty();
		/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
	}
}

//파일에서 data를 새로 읽은 data로 Line을 새로 갱신한다.
void CData::ReloadDataAtPlane(CPlane* pPlane)
{
	////////////////////////////////////////////////////////////////////
	//	Function 구성                                                 //
	//                                                                //
	//	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();          //
	//	while(YAxisPos)                                               //
	//	{                                                             //
	//		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		/////////////////                                         //
	//		// CASE 1. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 2. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/////////////////                                         //
	//		// CASE 3. ... //                                         //
	//		/////////////////                                         //
	//		:                                                         //
	//		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/    //
	//		:                                                         //
	//	}                                                             //
	////////////////////////////////////////////////////////////////////

	// Data Query조건에서 User가 선택한 Y축을 하나씩 꺼내어
	POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
	CAxis* pYAxis;
	while(YAxisPos)
	{
		/*♣♣♣ Part I   ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		// 그 Property 이름이 pPlane이 가르키는 plane object의
		// Y축의 property 이름과 다르면, Loop 마지막으로 continue!
		pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
		if(pYAxis->GetPropertyTitle().CompareNoCase(pPlane->GetYAxis()->GetPropertyTitle())!=0) continue;

		// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
		/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
		/*♠*/ CProgressCtrl ProgressCtrl;
		/*♠*/ CRect rect;
		/*♠*/ pStatusBar->GetItemRect(0,rect);
		/*♠*/ rect.left = rect.Width()/2;
		/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
		/*♠*/ ProgressCtrl.SetRange(0,m_QueryCondition.GetChPathCount());


		/*♣♣♣ Part II  ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		////////////////////////////////
		// CASE 1. X축이 cycle인 경우 //
		////////////////////////////////

		CString strChPath;
		CString strMsg;
		POSITION ChPos, ModePos, LinePos, CyclePos;

		if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("Cycle")==0)
		{
			DWORD dwMode;

			ChPos = m_QueryCondition.GetChPathHead();
			while(ChPos)
			{
				CList<DWORD, DWORD&> TempList;
				strChPath = m_QueryCondition.GetNextChPath(ChPos);
				//
				CList<DWORD, DWORD&> ModeList;
				m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());
				ModePos = ModeList.GetHeadPosition();
				
				while(ModePos)
				{
					dwMode = ModeList.GetNext(ModePos);
					if(pPlane->IsThereThisLine(strChPath, 0, dwMode, pYAxis->GetTitle())) 
					{
						TempList.AddTail(dwMode);
					}
				}

				/*♠*/ strMsg.Format("Loading data %s-%s %s",
				/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
				/*♠*/ 			  pYAxis->GetTitle(),
				/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
				/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
				/*♠*/ ProgressCtrl.SetRange(0,(short)(TempList.GetCount()*m_QueryCondition.GetCycleCount()));
				/*♠*/ int nPos = 0;

				LinePos = TempList.GetHeadPosition();
				LONG lCycle;
				fltPoint pt;
				while(LinePos)
				{
					dwMode = TempList.GetNext(LinePos);
					//Reload이면 이전 속성을 내려 받야야 함 
					CLine* pLine = pPlane->FindLineOf(strChPath, 0, dwMode);
					if(pLine)
					{
						pLine->RemoveAllData();
						//
						CyclePos = m_QueryCondition.GetCycleHead();
						while(CyclePos)
						{
							lCycle = m_QueryCondition.GetNextCycle(CyclePos);
							if(GetLastDataOfTable(pt, strChPath, lCycle, dwMode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
								pLine->AddData(pt);

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}

					}
				}
			}
		}
		else // <- if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("cycle")==0)
		{

			///////////////////////////////////////////////////////////
			// CASE 2. X축이 cycle이 아니고 시작type이 '연속'인 경우 //
			///////////////////////////////////////////////////////////
			if(m_QueryCondition.GetStartPointOption() ==0)
			{
				//
				CStringList TempList;
				LONG lCycle;
				
				//현재 채널이 이미 표기되어 있는지 확인 
				ChPos = m_QueryCondition.GetChPathHead();
				while(ChPos)
				{
					strChPath = m_QueryCondition.GetNextChPath(ChPos);
					if(pPlane->IsThereThisLine(strChPath, 0, 0, pYAxis->GetTitle())) 
						TempList.AddTail(strChPath);
				}
				
				//새로 그려야할 채널이름 리스트 
				LinePos = TempList.GetHeadPosition();
				while(LinePos)
				{
					strChPath = TempList.GetNext(LinePos);
					//
					CLine* pLine = pPlane->FindLineOf(strChPath, 0, 0);
					if(pLine == NULL)	continue;

					pLine->RemoveAllData();

					float fltSum = 0.0f;
					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());

					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*m_QueryCondition.GetCycleCount()));
					/*♠*/ int nPos = 0;

					CyclePos = m_QueryCondition.GetCycleHead();
					while(CyclePos)
					{
						lCycle = m_QueryCondition.GetNextCycle(CyclePos);

						ModePos = ModeList.GetHeadPosition();
						while(ModePos)
						{
							DWORD dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt = GetDataOfTable(lDataNum,
								                              strChPath, lCycle, dwMode, 
															  pPlane->GetXAxis()->GetTitle(),
															  pYAxis->GetTitle());

							TRACE("2: %s Draw data size is %d\n", strChPath, lDataNum);
							if(lDataNum>0 && pfltPt!=NULL)
							{
								
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;
									pLine->AddData(pt);
								}

// 								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
// 									fltSum += pfltPt[lDataNum-1].x;
								//20090911 Mode가 바뀌는 부분 Indexing
								pLine->AddModeDataCount(pLine->GetDataCount());
								
								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;

								delete [] pfltPt;
							}

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}
					}

					//
				}
			}

			///////////////////////////////////////////////////////////////
			// CASE 3. X축이 cycle이 아니고 시작type이 '영점시작'인 경우 //
			///////////////////////////////////////////////////////////////

			else if(m_QueryCondition.GetStartPointOption() ==1)
			{
				ChPos = m_QueryCondition.GetChPathHead();			
				while(ChPos)
				{
					strChPath = m_QueryCondition.GetNextChPath(ChPos);
					//
					CList<LONG, LONG&> TempList;
					CyclePos = m_QueryCondition.GetCycleHead();
					while(CyclePos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CyclePos);
						if(pPlane->IsThereThisLine(strChPath, lCycle, 0, pYAxis->GetTitle())) TempList.AddTail(lCycle);
					}
					//
					CList<DWORD, DWORD&> ModeList;
					m_QueryCondition.GetModeList(&ModeList, strChPath, pYAxis->GetTitle());

					/*♠*/ strMsg.Format("Loading data %s-%s %s",
					/*♠*/ 	          pPlane->GetXAxis()->GetTitle(),
					/*♠*/ 			  pYAxis->GetTitle(),
					/*♠*/ 			  strChPath.Mid(strChPath.ReverseFind('\\')+1));
					/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
					/*♠*/ ProgressCtrl.SetRange(0,(short)(ModeList.GetCount()*TempList.GetCount()));
					/*♠*/ int nPos = 0;

					LinePos = TempList.GetHeadPosition();
					while(LinePos)
					{
						LONG lCycle = TempList.GetNext(LinePos);
						//
						CLine* pLine = pPlane->FindLineOf(strChPath, 0, 0);
						if(pLine == NULL)	continue;

						pLine->RemoveAllData();

						float fltSum = 0.0f;
						//
						ModePos = ModeList.GetHeadPosition();
						DWORD dwMode;
						while(ModePos)
						{
							dwMode = ModeList.GetNext(ModePos);
							LONG      lDataNum = 0L;
							fltPoint* pfltPt =GetDataOfTable(lDataNum,
								                             strChPath, lCycle, dwMode, 
															 pPlane->GetXAxis()->GetTitle(),
															 pYAxis->GetTitle());
							TRACE("3: %s Draw data size is %d\n", strChPath, lDataNum);
							if(lDataNum>0 && pfltPt!=NULL)
							{
								for(int i=0; i<lDataNum; i++)
								{
									fltPoint pt = pfltPt[i];
									pt.x += fltSum;
									pLine->AddData(pt);
								}

// 								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
// 									fltSum += pfltPt[lDataNum-1].x;
								//20090911 Mode가 바뀌는 부분 Indexing
								pLine->AddModeDataCount(pLine->GetDataCount());
								
								if(pPlane->GetXAxis()->GetTitle().CompareNoCase("Time")==0)
									fltSum += pfltPt[lDataNum-1].x;

								delete [] pfltPt;
							}

							/*♠*/ nPos++;
							/*♠*/ ProgressCtrl.SetPos(nPos);
						}

						//
					}
				}
			}
		}

		/*♣♣♣ Part III ♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣♣*/ 

		/*♠*/ ProgressCtrl.DestroyWindow();
		//Added by KBH 2005/10/31  
		/*♠*/ strMsg.Empty();
		/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText(strMsg);
	}
}

/*
	Name:    SaveDataAsFile
	Comment: 데이터 Query 조건에 따라 데이터를 불러와
	         *.csv형식의 file로 저장한다.
	Input:   저장할 file path
	Output:  저장성공유무
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
BOOL CData::SaveDataAsFile(LPCTSTR strPath)
{
	////////////////////////////
	//	Function 구성         //
	//                        //
	//	Step 1. ...           //
	//	:                     //
	//	Step 2. ...           //
	//	// CASE I. ...        //
	//		// case 1. ...    //
	//		:                 //
	//		// case 2. ...    //
	//		:                 //
	//		// case 3. ...    //
	//		:                 //
	//		// case 4. ...    //
	//		:                 //
	//	// CASE II. ...       //
	//	:                     //
	//	// CASE II. ...       //
	//	:                     //
	//	Step 3. ...           //
	//	:                     //
	////////////////////////////

	//
	// Step 1. 'strPath'의 Path로 file을 만든다.
	BOOL bVerticalMode = FALSE;		//20110321 KHS

	//
	CStdioFile afile;
	if(!afile.Open(strPath,CFile::modeCreate|CFile::typeText|CFile::modeWrite))		return FALSE;

	//
	// Step 2. 다음의 각 경우에 따라서 file내용을 기록한다.
	//

	////////////////////////////////
	// CASE I. X축이 cycle인 경우 //
	////////////////////////////////

	if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("cycle")==0)
	{
		CSortTypeDialog aDlg;
		if(IDOK==aDlg.DoModal())
		{
			////////////////////////////////
			// case 1. Channel - Vertical //
			////////////////////////////////

			if     (aDlg.m_iType==1)
			{
				POSITION ChPos = m_QueryCondition.GetChPathHead();
				while(ChPos)
				{
					CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
					//
					CString str;
					str.Format("\n%s,\n\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
					afile.WriteString(str);

					// Column Head의 Title을 쓴다.

					// 1째 줄 Title
					afile.WriteString(",");
					POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
					while(YAxisPos)
					{
						CAxis* pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
						CList<DWORD, DWORD&> ModeList;
						if(m_QueryCondition.GetModeList(&ModeList, ChPath, pYAxis->GetTitle()))
						{
							str.Format("%s(%s)",pYAxis->GetTitle(),pYAxis->GetUnitNotation());
							afile.WriteString(str);
							for(int i=0; i<ModeList.GetCount(); i++)
							{
								afile.WriteString(",");
							}
						}
					}
					afile.WriteString("\n");

					// 2째 줄 Title
					afile.WriteString("CYCLE,");
					YAxisPos = m_QueryCondition.GetYAxisHead();
					while(YAxisPos)
					{
						CAxis* pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
						CList<DWORD, DWORD&> ModeList;
						if(m_QueryCondition.GetModeList(&ModeList, ChPath, pYAxis->GetTitle()))
						{
							POSITION ModePos = ModeList.GetHeadPosition();
							while(ModePos)
							{
								DWORD mode = ModeList.GetNext(ModePos);
								afile.WriteString(CDataQueryCondition::GetModeName(mode)+",");
							}
						}
					}
					afile.WriteString("\n");

					// Cycle별 Data를 기록한다.
					POSITION CycPos = m_QueryCondition.GetCycleHead();
					while(CycPos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
						str.Format("%d,", lCycle);
						afile.WriteString(str);
						//
						POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
						while(YAxisPos)
						{
							CAxis* pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
							CList<DWORD, DWORD&> ModeList;
							if(m_QueryCondition.GetModeList(&ModeList, ChPath, pYAxis->GetTitle()))
							{
								POSITION ModePos = ModeList.GetHeadPosition();
								while(ModePos)
								{
									DWORD mode = ModeList.GetNext(ModePos);
									fltPoint pt;
									if(GetLastDataOfTable(pt, ChPath, lCycle, mode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
									{
										if(pt.x==0.0f) afile.WriteString(",");
										else
										{
											str.Format("%.3f,", pYAxis->ConvertUnit(pt.y));
											afile.WriteString(str);
										}
									}
									else afile.WriteString(",");
								}
							}
						}
						afile.WriteString("\n");
					}
				}
			}

			//////////////////////////////////
			// case 2. Channel - Horizontal //
			//////////////////////////////////

			else if(aDlg.m_iType==0)
			{
				POSITION ChPos = m_QueryCondition.GetChPathHead();
				while(ChPos)
				{
					CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
					//
					CString str;
					str.Format("\n%s,\n\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
					afile.WriteString(str);
					//
					POSITION CycPos = m_QueryCondition.GetCycleHead();
					afile.WriteString("CYCLE,,");
					while(CycPos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
						str.Format("%d,",lCycle);
						afile.WriteString(str);
					}
					afile.WriteString("\n");
					//
					POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
					while(YAxisPos)
					{
						CAxis* pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
						CList<DWORD, DWORD&> ModeList;
						if(m_QueryCondition.GetModeList(&ModeList, ChPath, pYAxis->GetTitle()))
						{
							POSITION ModePos = ModeList.GetHeadPosition();
							while(ModePos)
							{
								DWORD mode = ModeList.GetNext(ModePos);
								str.Format("%s(%s),%s,",
									       pYAxis->GetTitle(),
										   pYAxis->GetUnitNotation(),
										   CDataQueryCondition::GetModeName(mode));
								afile.WriteString(str);
								POSITION CycPos = m_QueryCondition.GetCycleHead();
								while(CycPos)
								{
									LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
									fltPoint pt;
									if(GetLastDataOfTable(pt, ChPath, lCycle, mode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
									{
										if(pt.x==0.0f) afile.WriteString(",");
										else
										{
											str.Format("%.3f,", pYAxis->ConvertUnit(pt.y));
											afile.WriteString(str);
										}
									}
									else afile.WriteString(",");
								}
								afile.WriteString("\n");
							}
						}
					}
				}
			}

			/////////////////////////////////
			// case 3. Y-Axis - Horizontal //
			/////////////////////////////////

			else if(aDlg.m_iType==10)
			{
				//
				POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
				while(YAxisPos)
				{
					CAxis* pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
					//
					CString str;
					str.Format("\n%s(%s),\n\n",
						       pYAxis->GetTitle(),
							   pYAxis->GetUnitNotation());
					afile.WriteString(str);
					//
					POSITION CycPos = m_QueryCondition.GetCycleHead();
					afile.WriteString("CYCLE,,");
					while(CycPos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
						str.Format("%d,",lCycle);
						afile.WriteString(str);
					}
					afile.WriteString("\n");
					//
					CList<BYTE, BYTE&> ModeList;
					POSITION PlanePos = m_PlaneList.GetHeadPosition();
					while(PlanePos)
					{
						CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
						pPlane->GetModeListOfLines(&ModeList, pYAxis->GetTitle());
					}
					//
					POSITION ModePos = ModeList.GetHeadPosition();
					while(ModePos)
					{
						DWORD mode = ModeList.GetNext(ModePos);
						POSITION ChPos = m_QueryCondition.GetChPathHead();
						while(ChPos)
						{
							CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
							str.Format("%s,%s,",
									   CDataQueryCondition::GetModeName(mode),
								       ChPath.Mid(ChPath.ReverseFind('\\')+1));
							afile.WriteString(str);
							POSITION CycPos = m_QueryCondition.GetCycleHead();
							while(CycPos)
							{
								LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
								fltPoint pt;
								if(GetLastDataOfTable(pt, ChPath, lCycle, mode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
								{
									if(pt.x==0.0f) afile.WriteString(",");
									else
									{
										str.Format("%.3f,", pYAxis->ConvertUnit(pt.y));
										afile.WriteString(str);
									}
								}
								else afile.WriteString(",");
							}
							afile.WriteString("\n");
						}
					}
				}
			}

			///////////////////////////////
			// case 4. Y-Axis - Vertical //
			///////////////////////////////

			else if(aDlg.m_iType==11)
			{
				//
				POSITION YAxisPos = m_QueryCondition.GetYAxisHead();
				while(YAxisPos)
				{
					CAxis* pYAxis = m_QueryCondition.GetNextYAxis(YAxisPos);
					//
					CString str;
					str.Format("\n%s(%s),\n\n",pYAxis->GetTitle(),pYAxis->GetUnitNotation());
					afile.WriteString(str);

					// Column Head의 Title을 쓴다.

					// 1째 줄 Title
					afile.WriteString(",");
					CList<BYTE, BYTE&> ModeList;
					POSITION PlanePos = m_PlaneList.GetHeadPosition();
					while(PlanePos)
					{
						CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
						pPlane->GetModeListOfLines(&ModeList, pYAxis->GetTitle());
					}
					//
					POSITION ModePos = ModeList.GetHeadPosition();
					while(ModePos)
					{
						DWORD mode = ModeList.GetNext(ModePos);
						str.Format("%s",CDataQueryCondition::GetModeName(mode));
						afile.WriteString(str);
						for(int i=0; i<m_QueryCondition.GetChPathCount(); i++)
						{
							afile.WriteString(",");
						}
					}
					afile.WriteString("\n");
					//
					afile.WriteString("CYCLE,");
					ModePos = ModeList.GetHeadPosition();

					DWORD dwMode;	//ljb 20131011 add
					while(ModePos)
					{
//						BYTE mode = ModeList.GetNext(ModePos);
						POSITION ChPos = m_QueryCondition.GetChPathHead();
						while(ChPos)
						{
							CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
							afile.WriteString(ChPath.Mid(ChPath.ReverseFind('\\')+1)+",");
						}
						dwMode = ModeList.GetNext(ModePos);	//ljb 20131011 add

					}
					afile.WriteString("\n");
					
					// Cycle별 Data를 기록한다.
					POSITION CycPos = m_QueryCondition.GetCycleHead();
					while(CycPos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
						str.Format("%d,", lCycle);
						afile.WriteString(str);
						//
						CList<BYTE, BYTE&> ModeList;
						POSITION PlanePos = m_PlaneList.GetHeadPosition();
						while(PlanePos)
						{
							CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
							pPlane->GetModeListOfLines(&ModeList, pYAxis->GetTitle());
						}
						//
						POSITION ModePos = ModeList.GetHeadPosition();
						while(ModePos)
						{
							BYTE mode = ModeList.GetNext(ModePos);
							POSITION ChPos = m_QueryCondition.GetChPathHead();
							while(ChPos)
							{
								CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
								fltPoint pt;
								if(GetLastDataOfTable(pt, ChPath, lCycle, mode, pYAxis->GetTitle(), m_QueryCondition.GetVTMeasurePoint()))
								{
									if(pt.x==0.0f) afile.WriteString(",");
									else
									{
										str.Format("%.3f,", pYAxis->ConvertUnit(pt.y));
										afile.WriteString(str);
									}
								}
								else afile.WriteString(",");
							}
						}
						//
						afile.WriteString("\n");
					}
				}
			}
		}
	}
	else // <- if(m_QueryCondition.GetXAxis()->GetTitle().CompareNoCase("cycle")==0)
	{
		ULONG ulTotalDataNum = 0;
		ULONG ulMaxDataNum = 0;
		long lTmpCount = 0;
		int nSaveInterval = 0;
//		int nSaveCount;

		int MAX_HORIZONTAL_BOUND = 254;
		POSITION ChPos = m_QueryCondition.GetChPathHead();
		bool bIgnore = false;
		while(ChPos)
		{
			CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
			//
			POSITION PlanePos = m_PlaneList.GetHeadPosition();
			while (PlanePos)
			{
				CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
				CLine* pLine = pPlane->FindLineOf(ChPath, 0, 0);
				if(pLine!=NULL)
				{
					lTmpCount = pLine->GetDataCount();
					ulTotalDataNum += lTmpCount;
					if( lTmpCount>ulMaxDataNum)
					{
						ulMaxDataNum = lTmpCount;
					}
					if( lTmpCount>MAX_HORIZONTAL_BOUND )
					{
						bIgnore = true;
						break;
					}
				}
			}
		}

		// 데이터의 개수가 254개를 넘지 않으면 Horizontal로도 저장이 가능토록..
		if ( bIgnore == false )
		{
			CSortTypeDialog aDlg (CSortTypeDialog::modeSimple);
			if( IDOK == aDlg.DoModal() )
			{
				//////////////////////
				// case 1. Vertical //
				//////////////////////
				if ( (aDlg.m_iType%10) == 1 )
				{
					nSaveInterval=1;
					goto SAVE_MODE_VERTICAL;
				}

				////////////////////////
				// case 2. Horizontal //
				////////////////////////
				else
				{
					////////////////////////////////////////////////////////////
					// CASE II. X축이 cycle이 아니고 시작type이 '연속'인 경우 //
					////////////////////////////////////////////////////////////
					if(m_QueryCondition.GetStartPointOption() ==0)
					{
						/* (Example)
							M24CH15               => 채널 Loop
							시간	1		20		40		...
							전압	3.0		3.1		3.2		...
							전류	1000.0	999.5	999.2	...

							M24CH16
							시간	1		20		40		...
							전압	3.0		3.1		3.2		...
							전류	1000.0	999.5	999.2	...
						*/
						POSITION ChPos = m_QueryCondition.GetChPathHead();
						while(ChPos)
						{
							CString str;
							//
							CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
							str.Format("\n%s,\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
							afile.WriteString(str);

							// 
							POSITION PlanePos = m_PlaneList.GetHeadPosition();
							if (PlanePos)
							{
								CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
								str.Format("%s(%s),", pPlane->GetXAxis()->GetTitle(),
									pPlane->GetXAxis()->GetUnitNotation());
								afile.WriteString(str);
															
								CLine* pLine = pPlane->FindLineOf(ChPath, 0, 0);

								if(pLine!=NULL)
								{
									int N = pLine->GetDataCount();
									for ( int i = 0; i < N; i++ )
									{
										str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(0))->GetXAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).x));
										afile.WriteString(str);
									}
								}
								afile.WriteString("\n");
							}

							int n = 0;
							PlanePos = m_PlaneList.GetHeadPosition();
							while(PlanePos)
							{
								CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
								str.Format("%s(%s),", pPlane->GetYAxis()->GetTitle(),
													 pPlane->GetYAxis()->GetUnitNotation());
								afile.WriteString(str);
								CLine* pLine = pPlane->FindLineOf(ChPath, 0, 0);

								if(pLine!=NULL)
								{
									int N = pLine->GetDataCount();
									for ( int i = 0; i < N; i++ )
									{
										str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(n))->GetYAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).y));
										afile.WriteString(str);
									}
								}
								n++;
								afile.WriteString("\n");
							}
						}
					}

				////////////////////////////////////////////////////////////////
				// CASE III. X축이 cycle이 아니고 시작type이 '영점시작'일 때, //
				////////////////////////////////////////////////////////////////

					else if(m_QueryCondition.GetStartPointOption()==1)
					{
						/* (Example)

							M24CH15               => 채널 Loop

							CYCLE 1               => Cycle Loop
							시간	1		20		40		...
							전압	3.0		3.1		3.2		...
							전류	1000.0	999.5	999.2	...

							CYCLE 2
							시간	1		20		40		...
							전압	3.0		3.1		3.2		...
							전류	1000.0	999.5	999.2	...
						*/
						POSITION ChPos = m_QueryCondition.GetChPathHead();
						while(ChPos)
						{
							CString str;
							//
							CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
							str.Format("\n%s,\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
							afile.WriteString(str);

							POSITION CycPos = m_QueryCondition.GetCycleHead();
							while(CycPos)
							{
								LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
								str.Format("\nCYCLE %d\n", lCycle);
								afile.WriteString(str);

								// 
								POSITION PlanePos = m_PlaneList.GetHeadPosition();
								if (PlanePos)
								{
									CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
									str.Format("%s(%s),", pPlane->GetXAxis()->GetTitle(),
										pPlane->GetXAxis()->GetUnitNotation());
									afile.WriteString(str);
																
									CLine* pLine = pPlane->FindLineOf(ChPath, lCycle, 0);

									if(pLine!=NULL)
									{
										int N = pLine->GetDataCount();
										for ( int i = 0; i < N; i++ )
										{
											str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(0))->GetXAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).x));
											afile.WriteString(str);
										}
									}
									afile.WriteString("\n");
								}

								int n = 0;
								PlanePos = m_PlaneList.GetHeadPosition();
								while(PlanePos)
								{
									CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
									str.Format("%s(%s),", pPlane->GetYAxis()->GetTitle(),
														 pPlane->GetYAxis()->GetUnitNotation());
									afile.WriteString(str);
									CLine* pLine = pPlane->FindLineOf(ChPath, lCycle, 0);

									if(pLine!=NULL)
									{
										int N = pLine->GetDataCount();
										for ( int i = 0; i < N; i++ )
										{
											str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(n))->GetYAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).y));
											afile.WriteString(str);
										}
									}
									n++;
									afile.WriteString("\n");
								}
							}
						}
					}
				}
			}
		}

		else
		{
			{
				CQuerySaveDlg aQueryDlg(ulMaxDataNum, ulTotalDataNum);
				if(aQueryDlg.DoModal()==IDOK)
				{
					nSaveInterval = aQueryDlg.m_ulSaveInterval;
				}
			}

SAVE_MODE_VERTICAL:
			bVerticalMode = TRUE;		//20110321 KHS
			////////////////////////////////////////////////////////////
			// CASE II. X축이 cycle이 아니고 시작type이 '연속'인 경우 //
			////////////////////////////////////////////////////////////
			if(m_QueryCondition.GetStartPointOption()==0)
			{
				/* (Example)

					M24CH15               => 채널 Loop

					시간 전압 전류        => Plane Loop
					1    3.0  1000.0
					20   3.1   999.5
					:    :     :

					M24CH16
					시간 전압 전류
					1    3.0  1000.0
					20   3.1   999.5
					:    :     :

				*/
				CString strModuleChannelName; //ksj 20200904;
				POSITION ChPos = m_QueryCondition.GetChPathHead();
				while(ChPos)
				{
					CString str;
					//
					CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
					str.Format("\n%s,\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
					afile.WriteString(str);

					strModuleChannelName = ChPath.Mid(ChPath.ReverseFind('\\')+1); //ksj 20200904

					// 해당채널의 라인을 각 Plane에서 찾아서 List를 만든다.
					CTypedPtrList<CPtrList, CLine*> TempLineList;
					CTypedPtrList<CPtrList, CAxis*> TempAxisList;
					POSITION PlanePos = m_PlaneList.GetHeadPosition();
					int n=0;
					//kjh 20080911
					while(PlanePos)
					{
						CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
						if(n == 0)
						{
							str.Format("%s(%s),", pPlane->GetXAxis()->GetTitle(),
												 pPlane->GetXAxis()->GetUnitNotation());
							afile.WriteString(str);
						}

// 						str.Format("%s(%s),", pPlane->GetYAxis()->GetTitle(),
// 											 pPlane->GetYAxis()->GetUnitNotation());
// 						afile.WriteString(str);
// 						//
// 						TempLineList.AddTail(pPlane->FindLineOf(ChPath, 0, 0));

						//2008/12/17 Aux Data 저장 수정 KBH
						POSITION linePos = pPlane->GetLineHead();
						while(linePos)
						{
							CLine *pLine = pPlane->GetNextLine(linePos);	
							//ksj 20200904 : 해당 채널 Line값만 출력하도록 변경
							//기존에는 해당 채널 Line 이 아닌 것도 출력해서 불필요하게 반복 출력이됨.
							BOOL bChMatch = TRUE;
							CString strTemp = pLine->GetChPath().Mid(pLine->GetChPath().ReverseFind('\\')+1);
							if(strModuleChannelName.Compare(strTemp))
								bChMatch = FALSE;							
							//ksj end
							//if(pLine!=NULL)
							if(pLine!=NULL && bChMatch) //ksj 20200904
							{
								str.Format("%s(%s),", pLine->GetName(),  pPlane->GetYAxis()->GetUnitNotation());
								afile.WriteString(str);
								//////////////////////////////////////////////////////////////////////////
								// + BW KIM 2014.02.07
		 						//TempLineList.AddTail(pPlane->FindLineOf(ChPath, 0, 0));
								TempLineList.AddTail(pLine);
								// -
								//////////////////////////////////////////////////////////////////////////
								TempAxisList.AddTail(pPlane->GetYAxis());
							}
						}
						n++;
					}
					afile.WriteString("\n");

					// Line 내 Data 개수 검증
					int N = 0;
					BOOL bCheck = TRUE;
					POSITION TempPos = TempLineList.GetHeadPosition();
					n = 0;
					while(TempPos)
					{
						CLine* pLine = TempLineList.GetNext(TempPos);
						if(pLine!=NULL)
						{
							if(n==0) N = pLine->GetDataCount();
							else
							{
								if(N!=pLine->GetDataCount())
								{
									bCheck = FALSE;
									break;
								}
							}
							n++;
						}
					}
					//
					if(N>0&&bCheck)
					{

						//2008/12/17 Progress 수정 KBH
						// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
						/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
						/*♠*/ CProgressCtrl ProgressCtrl;
						/*♠*/ CRect rect;
						/*♠*/ pStatusBar->GetItemRect(0,rect);
						/*♠*/ rect.left = rect.Width()/2;
						/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
						/*♠*/ ProgressCtrl.SetRange(0, 100);
						/*♠*/ int nPos = 0;
						/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("Data saving...");

						for(int i=0; i<N; i++)
						{
							//2008/12/17 Progress 수정 KBH
							ProgressCtrl.SetPos(int(((float)i/(float)N)*100.0f));

							if( (i % nSaveInterval != 0) && i )
								continue;

							// X축 값 적기
							TempPos = TempLineList.GetHeadPosition();
//							n=0;
//							while(TempPos)
//							{
								CLine* pLine = TempLineList.GetNext(TempPos);
								if(pLine!=NULL)
								{
//									str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(n))->GetXAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).x));
									str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(0))->GetXAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).x));
									afile.WriteString(str);
//									break;
								}
//								n++;
//							}
							// 각 Plane의 Y축 값 적기
							TempPos = TempLineList.GetHeadPosition();
							POSITION TempPos1 = TempAxisList.GetHeadPosition();
//							n=0;
							while(TempPos)
							{
								CLine* pLine = TempLineList.GetNext(TempPos);
								CAxis* pAxis = TempAxisList.GetNext(TempPos1);
								if(pLine!=NULL && pAxis)
								{
									float fYData = pLine->GetDataAt(pLine->FindDataIndex(i)).y;
									//2008/12/17 Aux Data 저장 수정 KBH
//									str.Format("%.3f,", m_PlaneList.GetAt(m_PlaneList.FindIndex(n))->GetYAxis()->ConvertUnit(fYData));

									str.Format("%.3f,", pAxis->ConvertUnit(fYData));
									afile.WriteString(str);
								}
								else 
								{
									afile.WriteString(",");
								}
//								n++;
							}
							afile.WriteString("\n");
						}
					}
				}
			}

		////////////////////////////////////////////////////////////////
		// CASE III. X축이 cycle이 아니고 시작type이 '영점시작'일 때, //
		////////////////////////////////////////////////////////////////

			else if(m_QueryCondition.GetStartPointOption()==1)
			{
				/* (Example)

					M24CH15               => 채널 Loop

					CYCLE 1               => Cycle Loop
					시간 전압 전류        => Plane Loop
					1    3.0  1000.0
					20   3.1   999.5
					:    :     :

					CYCLE 1
					시간 전압 전류
					1    3.0  1000.0
					20   3.1   999.5
					:    :     :

				*/
// 				POSITION ChPos = m_QueryCondition.GetChPathHead();
// 				while(ChPos)
// 				{
// 					CString str;
// 					//
// 					CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
// 					str.Format("\n%s,\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
// 					afile.WriteString(str);
// 					//
// 					POSITION CycPos = m_QueryCondition.GetCycleHead();
// 					while(CycPos)
// 					{
// 						LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
// 						str.Format("\nCYCLE %d\n", lCycle);
// 						afile.WriteString(str);
// 						// lCycle에 해당하는 라인을 각 Plane에서 찾아서 List를 만든다.
// 						CTypedPtrList<CPtrList, CLine*> TempLineList;
// 						CTypedPtrList<CPtrList, CAxis*> TempAxisList;
// 						POSITION PlanePos = m_PlaneList.GetHeadPosition();
// 						int n=0;
// 						while(PlanePos)
// 						{
// 							CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
// 							if(n==0)
// 							{
// 								str.Format("%s(%s),", pPlane->GetXAxis()->GetTitle(),
// 													 pPlane->GetXAxis()->GetUnitNotation());
// 								afile.WriteString(str);
// 							}
// // 							str.Format("%s(%s),", pPlane->GetYAxis()->GetTitle(),
// // 												 pPlane->GetYAxis()->GetUnitNotation());
// // 							afile.WriteString(str);
// // 							//
// // 							TempLineList.AddTail(pPlane->FindLineOf(ChPath, lCycle, 0));
// 
// 							//2008/12/17 Aux Data 저장 수정 KBH
// 							POSITION linePos = pPlane->GetLineHead();
// 							while(linePos)
// 							{
// 								CLine *pLine = pPlane->GetNextLine(linePos);		
// 								if(pLine!=NULL)
// 								{
// 									str.Format("%s(%s),", pLine->GetName(),  pPlane->GetYAxis()->GetUnitNotation());
// 									afile.WriteString(str);
// 		 							TempLineList.AddTail(pPlane->FindLineOf(ChPath, 0, 0));
// 									TempAxisList.AddTail(pPlane->GetYAxis());
// 								}
// 							}
// 
// 							n++;
// 						}
// 						afile.WriteString("\n");
// 						// Line 내 Data 개수 검증
// 						int N = 0;
// 						BOOL bCheck = TRUE;
// 						POSITION TempPos = TempLineList.GetHeadPosition();
// 						n = 0;
// 						while(TempPos)
// 						{
// 							CLine* pLine = TempLineList.GetNext(TempPos);
// 							if(pLine!=NULL)
// 							{
// 								if(n==0) N = pLine->GetDataCount();
// 								else
// 								{
// 									if(N!=pLine->GetDataCount())
// 									{
// 										bCheck = FALSE;
// 										break;
// 									}
// 								}
// 								n++;
// 							}
// 						}
// 						if(N>0&&bCheck)
// 						{
// 							//2008/12/17 Progress 수정 KBH
// 							// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
// 							/*♠*/ CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
// 							/*♠*/ CProgressCtrl ProgressCtrl;
// 							/*♠*/ CRect rect;
// 							/*♠*/ pStatusBar->GetItemRect(0,rect);
// 							/*♠*/ rect.left = rect.Width()/2;
// 							/*♠*/ ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
// 							/*♠*/ ProgressCtrl.SetRange(0, 100);
// 							/*♠*/ int nPos = 0;
// 							/*♠*/ ((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("Data saving...");
// 							for(int i=0; i<N; i++)
// 							{
// 								//2008/12/17 Progress 수정 KBH
// 								ProgressCtrl.SetPos(int(((float)i/(float)N)*100.0f));
// 
// 								if( (i % nSaveInterval != 0) && i )
// 									continue;
// 
// 								// X축 값 적기
// 								TempPos = TempLineList.GetHeadPosition();
// 								n=0;
// 								while(TempPos)
// 								{
// 									CLine* pLine = TempLineList.GetNext(TempPos);
// 									if(pLine!=NULL)
// 									{
// 										str.Format("%.3f,",m_PlaneList.GetAt(m_PlaneList.FindIndex(n))->GetXAxis()->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).x));
// 										afile.WriteString(str);
// 										break;
// 									}
// 									n++;
// 								}
// 								// 각 Plane의 Y축 값 적기
// 								TempPos = TempLineList.GetHeadPosition();
// 								POSITION TempPos1 = TempAxisList.GetHeadPosition();
// 								n=0;
// 								while(TempPos)
// 								{
// 									CLine* pLine = TempLineList.GetNext(TempPos);
// 
// 									//2008/12/17 Aux Data 저장 수정 KBH
// 									CAxis* pAxis = TempAxisList.GetNext(TempPos1);
// 									if(pLine!=NULL && pAxis)
// 									{
// 										str.Format("%.3f,",pAxis->ConvertUnit(pLine->GetDataAt(pLine->FindDataIndex(i)).y));
// 										afile.WriteString(str);
// 									}
// 									else afile.WriteString(",");
// 									n++;
// 								}
// 								//
// 								afile.WriteString("\n");
// 							}
// 						}
// 					}
// 				}
				//////////////////////////////////////////////////////////////////////////
				//20090911 KBH	
				// cycle 별 충전/방전을 횡으로 표시	
				//1. Channel for
				DWORD dwMode = m_QueryCondition.GetModeFlag();

				ChPos = m_QueryCondition.GetChPathHead();
				while(ChPos)
				{
					CString str;
					//
					CString ChPath = m_QueryCondition.GetNextChPath(ChPos);
					str.Format("\n%s,\n",ChPath.Mid(ChPath.ReverseFind('\\')+1));
					afile.WriteString(str);
					CChData* pChData = GetChData(ChPath);
					if(pChData == NULL)		continue;


					//2. Cycle for
					CString strTitle, strSubTitle, strCycleTitle;
					CStringList strListTemp[2];
					int nToggleCnt = 0;		//strListTemp 0,1을 toggle하기 위한 count
					POSITION CycPos = m_QueryCondition.GetCycleHead();
					int nCurColumnCnt = 0;

					while(CycPos)
					{
						LONG lCycle = m_QueryCondition.GetNextCycle(CycPos);
						str.Format("CYCLE %d", lCycle);
						strCycleTitle += str;

						//3. Cycle내 Table List(Step) 검색
						CTypedPtrList <CPtrList, CTable*> TableList;
						GetTableListOfCycle(&TableList, ChPath, lCycle);
						POSITION TbPos = TableList.GetHeadPosition();
						while(TbPos)
						{
							//4. 선택 Step일치 여부 검사 
							CTable *pTable = (CTable *)TableList.GetNext(TbPos);
							
							if( (0x01<<pTable->GetType()) & dwMode)		//Type selection
							{
								//Rest는 이전 step의 하단에 연속해서 저장하므로 생략
								if(pTable->GetType() == PS_STEP_REST)	continue;
								

								//5. 선택 Y축 항목만 저장
								long lDataCnt = -1;
								POSITION PlanePos = m_PlaneList.GetHeadPosition();
								while(PlanePos)
								{
									long dataSize = 0;
									CString str;
									//2개의 StringList Bufer를 이용한다.
									int nSrcIndex = nToggleCnt%2;			//nSrcIndex = 0;
									int nDestIndex = (nToggleCnt+1)%2;		//
									
									CPlane* pPlane = m_PlaneList.GetNext(PlanePos);
//									fltPoint *fltData = pTable->GetData(dataSize, pPlane->GetXAxis()->GetTitle(), pPlane->GetYAxis()->GetTitle());
//									fltPoint *fltData = pChData->GetDataOfTable(pChData->GetTableIndex(pTable->GetStepNo(), lCycle), dataSize, pPlane->GetXAxis()->GetTitle(), pPlane->GetYAxis()->GetTitle(), dwMode&(0x01<<PS_STEP_REST));
									fltPoint *fltData = pChData->GetDataOfTable(pChData->GetTableIndex(pTable->GetStepNo(), lCycle), dataSize, pPlane->GetXAxis()->GetTitle(), pPlane->GetYAxis()->GetTitle(), dwMode&(0x01<<PS_STEP_REST));
								
									if(pPlane == NULL || fltData == NULL)	continue;
									
									//6. make title
									if(lDataCnt == -1)			//각 Step의 Item 처음일 경우 X축도 기록 	
									{
										strCycleTitle += ",";			//cycle title
										str.Format("STEP %d : %s,", pTable->GetStepNo(), PSGetTypeMsg(pTable->GetType()));
										strSubTitle += str;				//Step No & Type Title
										str.Format("%s(%s),", pPlane->GetXAxis()->GetTitle(), pPlane->GetXAxis()->GetUnitNotation());
										strTitle += str;				//Axis title
									}
									//축 Data 용 Title
									str.Format("%s(%s),", pPlane->GetYAxis()->GetTitle(), pPlane->GetYAxis()->GetUnitNotation());
									strTitle += str;
									strSubTitle += ",";
									strCycleTitle += ",";
								

									CString st;
									strListTemp[nDestIndex].RemoveAll();
									
									//이전 Data수와  현재 추가할 Data수를 비교한다. 
									if(strListTemp[nSrcIndex].GetCount() > dataSize)	//이전 Data가 길 경우
									{
										POSITION dataPos= strListTemp[nSrcIndex].GetHeadPosition();
										int nRecordCnt = 0;
										while(dataPos)
										{
											str = strListTemp[nSrcIndex].GetNext(dataPos);

											if(lDataCnt == -1)			//각 Step의 Item 처음일 경우 X축도 기록 	
											{
												if(nRecordCnt< dataSize)
												{
													st.Format("%.3f,%.3f,", pPlane->GetXAxis()->ConvertUnit(fltData[nRecordCnt].x), pPlane->GetYAxis()->ConvertUnit(fltData[nRecordCnt].y));
												}
												else
												{
													st.Format(",,");
												}
											}
											else
											{
												if(nRecordCnt< dataSize)
												{
													st.Format("%.3f,", pPlane->GetYAxis()->ConvertUnit(fltData[nRecordCnt].y));
												}
												else
												{
													st.Format(",");
												}
											}

											str += st;
											strListTemp[nDestIndex].AddTail(str);
											nRecordCnt++;
										}
									}
									else //if(strListTemp[nSrcIndex].GetCount() <= dataSize)	//현재 data가 가장 길 경우 
									{
										POSITION dataPos= strListTemp[nSrcIndex].GetHeadPosition();
										int nRecordCnt = 0;
										while(dataPos || nRecordCnt < dataSize)
										{
											if(dataPos != NULL)
											{
												str = strListTemp[nSrcIndex].GetNext(dataPos);
											}
											else
											{
												str.Empty();
												for(int a = 0; a < nCurColumnCnt; a++)
												{
													str += ",";
												}
											}

											if(lDataCnt == -1)			//각 Step의 Item 처음일 경우 X축도 기록 	
											{
												st.Format("%.3f,%.3f,", pPlane->GetXAxis()->ConvertUnit(fltData[nRecordCnt].x), pPlane->GetYAxis()->ConvertUnit(fltData[nRecordCnt].y));
											}
											else
											{
												st.Format("%.3f,", pPlane->GetYAxis()->ConvertUnit(fltData[nRecordCnt].y));
											}
											str += st;
											strListTemp[nDestIndex].AddTail(str);
											nRecordCnt++;
										}
									}

									if(lDataCnt == -1)			//각 Step의 Item 처음일 경우 X축도 기록 	
									{
										nCurColumnCnt++;
									}
									nCurColumnCnt++;

									lDataCnt = dataSize;
									delete [] fltData;


									strListTemp[nSrcIndex].RemoveAll();
									nToggleCnt++;
								}
							}							
							//이전 step의 data와 길이가 다를 경우 처리
						}
					}
					int ndd = 0;
					if(strListTemp[1].GetCount() > 0)
					{
						ndd = 1;
					}
					afile.WriteString(strCycleTitle);
					afile.WriteString("\n");
					afile.WriteString(strSubTitle);
					afile.WriteString("\n");
					afile.WriteString(strTitle);
					afile.WriteString("\n");
					POSITION linePos = strListTemp[ndd].GetHeadPosition();
					while(linePos)
					{
						CString str = strListTemp[ndd].GetNext(linePos);
						afile.WriteString(str);
						afile.WriteString("\n");
					}
				}
				//////////////////////////////////////////////////////////////////////////
			}
		}
	}
	//
	// Step 3. file을 닫는다.
	//
	afile.Close();

	//20110321 KHS
	//if(bVerticalMode)
	if(!bVerticalMode) //ksj 20210908 : 수직 저장인데 왜 수평 컨버팅?.. 오류로 보이므로 수정함. 데이터 양이 많아지면 굉장히 느림
	{
		ConvertHorizonCH(strPath);
	}
	//
	return TRUE;
}

/*
	Name:    GetLastDataOfTable
	Comment: Table의 마지막 data를 가져온다.
	Input:   fltPoint& pt          - Table 마지막 data를 가져갈 변수
	         LPCTSTR   strChPath   - Channel data가 위치한 folder path
			 LONG      lCycle      - Cycle번호      (Table index를 구하기 위한 ...)
			 BYTE      byMode      - 충/방/충휴/방휴(Table index를 구하기 위한 ...)
			 LPCTSTR   strTitle    - data를 가져올 transducer의 이름
			 WORD      wPoint      - Pack용에서 'V_MaxDiff'와 'T_MaxDiff'를 계산하기 위해
			                         사용할 point의 정보를 담고 있는 변수
	Output:  Data를 얻는데 성공했는지 여부
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
BOOL CData::GetLastDataOfTable(fltPoint& pt, LPCTSTR strChPath, LONG lCycle,  DWORD dwMode, LPCTSTR strTitle, WORD wPoint)
{
	// 입력된 Channel data가 위치한 folder path의 CChData object의 pointer를 구한다.
	CChData* pChData = GetChData(strChPath);
	if(pChData==NULL) return FALSE;

	// Efficiency를 구하는 경우,
	pt.x = (float) lCycle;
	
	//20100430--------------------------
	CList<LONG, LONG&> stepNoList;
	m_QueryCondition.GetStepNoList(&stepNoList);
	//--------------------------------
	
	if(CString(strTitle).CompareNoCase("Efficiency")==0)
	{
		// x축은 cycle이므로 pt.x는 cycle번호

		//충방전 순서를 관계없이 계산한다.
		//충방전 순서는 사용자가 맞게 사용해야 한다.
		//동일 step이 여러개 존재할 경우 가장 마지막 Step이 Data를 사용한다.
		//현재 Cycle에서 충전용량을 구한다.
		float fChargeCap = pChData->GetLastDataOfCycle(lCycle, 0x01<<PS_STEP_CHARGE, "Capacity");
		//현재 Cycle에서 방전 용량을 구한다.
		float fDischrageCap = pChData->GetLastDataOfCycle(lCycle, 0x01<<PS_STEP_DISCHARGE, "Capacity");

		if(fChargeCap < 0.00001)		pt.y = 0.0f;
		else							pt.y = fDischrageCap/fChargeCap*100.0f;

		TRACE("Cycle %d : %.1f= %.3f/%.3f\n", lCycle, pt.y, fDischrageCap, fChargeCap);
	}
	if(CString(strTitle).CompareNoCase("IR") == 0)
	{
		pt.y = pChData->GetLastDataOfCycle(lCycle, 0x01<<PS_STEP_IMPEDANCE, "IR");
	}
	else 
	{	
		//pt.y = pChData->GetLastDataOfTable(GetTableIndex(lCycle, dwMode), strTitle, wPoint);
		pt.y = pChData->GetLastDataOfCycle(lCycle, dwMode, strTitle);

		if(pt.y==FLT_MAX)
		{
			pt.y=0.0f;
			return FALSE;
		}
	}

	return TRUE;
}

/*
	Name:    GetDataOfTable
	Comment: Table의 data를 가져온다.
	Input:   LONG&     lDataNum      - 획득된 data수를 가져갈 변수
	         LPCTSTR   strChPath     - Channel data가 위치한 folder path
			 LONG      lCycle        - Cycle번호      (Table index를 구하기 위한 ...)
			 BYTE      byMode        - 충/방/충휴/방휴(Table index를 구하기 위한 ...)
			 LPCTSTR   strXAxisTitle - data를 가져올 x축 transducer의 이름
			 LPCTSTR   strYAxisTitle - data를 가져올 y축 transducer의 이름
	Output:  획득된 data 배열의 point
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
fltPoint* CData::GetDataOfTable(LONG& lDataNum, LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle)
{
	CChData* pChData = GetChData(strChPath);
	if (pChData==NULL)
	{
		lDataNum = 0L;
		return NULL;
	}

/*	DWORD dwMode =0;
	dwMode |= (0x01<<PS_STEP_CHARGE);
//	dwMode |= (0x01<<PS_STEP_DISCHARGE);
*/
//	return pChData->GetDataOfTable(GetTableIndex(lCycle,byMode), lDataNum, strXAxisTitle, strYAxisTitle, m_QueryCondition.GetVTMeasurePoint());

	//20100430
	CList<LONG, LONG&> stepNoList;
	m_QueryCondition.GetStepNoList(&stepNoList);
	
	//return pChData->GetDataOfCycle(lCycle, lDataNum, dwMode, strXAxisTitle, strYAxisTitle, m_QueryCondition.GetVTMeasurePoint());
	return pChData->GetDataOfCycle(lCycle, lDataNum, dwMode, m_QueryCondition.GetQueryMode(), &stepNoList, strXAxisTitle, strYAxisTitle, m_QueryCondition.GetVTMeasurePoint());
	//------------------------------------------

}

/*
	Name:    GetTableIndex
	Comment: Table Index를 구한다.
	Input:	 LONG      lCycle        - Cycle번호
			 BYTE      byMode        - 충/방/충휴/방휴
	Output:  Table index
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
LONG CData::GetTableIndex(LONG /*lCycle*/, DWORD /*dwMode*/)
{
//	if(dwMode==CDataQueryCondition::MODE_NONE) return 0L;
//	else return (lCycle-1L)*4L+(LONG)dwMode;

	return 0;
}

/*
	Name:    GetTableIndex
	Comment: Table Index를 구한다.
	Input:	 LPCTSTR strChPath - Channel data가 위치한 folder path
	Output:  해당 CChData object의 pointer
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CChData* CData::GetChData(LPCTSTR strChPath)
{
	CChData* pChData = NULL;

	// 1. m_ChDataList에서 값을 찾아본다.
	POSITION pos = m_ChDataList.GetHeadPosition();
	while (pos)
	{
		pChData = m_ChDataList.GetNext(pos);
		if(pChData->GetPath().CompareNoCase(strChPath)==0) break;
		else pChData = NULL;
	}

	// 2. m_ChDataList에서 값이 없으면 새로 생성한다.
	if (pChData == NULL)
	{
		pChData = new CChData(strChPath);
		// Added by 224 (2014/02/05) : Index를 제외한 값들은 cycle 수이다.
		// 주어진 범위에 맞는 테이블 수를 지정해야 한다.
		if (m_QueryCondition.GetQueryMode() == QUERY_MODE_INDEX)
		{
			
			
			// Added by 224 (2014/01/23) : 그래프 자료 로딩 개선
			pChData->m_nFrom = m_QueryCondition.m_dlgQuery.m_lFromCycle ;
			pChData->m_nTo   = m_QueryCondition.m_dlgQuery.m_lToCycle ;
		}
		else
		{
// 			pChData->m_nFrom = m_QueryCondition.m_dlgQuery.m_lFromCycle ;
// 			pChData->m_nTo   = m_QueryCondition.m_dlgQuery.m_lToCycle ;
			pChData->m_nFrom = m_QueryCondition.m_dlgQuery.m_lFromTable ;
			pChData->m_nTo   = m_QueryCondition.m_dlgQuery.m_lToTable ;
		}

		if (pChData->Create())
		{
			// 2.1. 새로 생성한 채널데이터를 m_chDataList에 추가한다.
			// 채널은 STEP END 의 값을 로드한다.
			m_ChDataList.AddTail(pChData);
		}
		else
		{
			delete pChData;
			pChData = NULL;
		}
	}

	//
	return pChData;
}

/*
	Name:    DrawPlaneOnView
	Comment: 각 Plane을 View에 그려줌
	Input:   CDC *pDC        - View의 CDC object pointer
	         RECT ClientRect - View의 Client영역 Rect
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::DrawPlaneOnView(CDC* pDC, CRect ClientRect)
{
	if(m_PlaneList.IsEmpty())
	{
		return;
	}
	//
	/*
	      ============================
		  ||         TopSpace
		  ||         ---------------      
		  ||LeftSpace|
		  ||         |    Scope
		  ||         |
	*/


	//
	// Fill BackGround color
	//////////////////////////////////////////////////////////////////////////
	CBrush aBrush, *oldBrush;
	aBrush.CreateSolidBrush(m_BackColor);
	oldBrush = pDC->SelectObject(&aBrush);
	pDC->Rectangle(&ClientRect); // Client영역의 전체바탕화면을 m_BackColor로칠한다.
	pDC->SelectObject(oldBrush);
	aBrush.DeleteObject();
	//////////////////////////////////////////////////////////////////////////

	CPlane *pPlane;
	POSITION pos = m_PlaneList.GetHeadPosition();
	
	CRect scopeRect = GetScopeRect(pDC, ClientRect);
	int LabelSpace = ClientRect.left;
	//
	/*								
	      ================================================================
		  || LeftSpace										Title
		  ||<--------------------->|
		  || LabelSpace for Plane[1]
		  ||<----------->|
		  || LabelSpace for Plane[0]
		  ||<->| 
		  ||    Volt(mV)  Curr(mA)  ---------------      
		  ||                       |
		  ||                       |    Scope
		  ||    1.0       2.2      |
	*/
	
	//////////////////////////////////////////////////////////////////////////
	//X, Y축을 그리고 축의 눈금을 표시한다.
	pos = m_PlaneList.GetHeadPosition();
	while (pos)
	{
		// 각 Plane에
		pPlane = m_PlaneList.GetNext(pos);
		// 좌측에 Label을 줄 위치와 Scope까지의 좌측여백, 상단여백,
		// 그리고 현재 Client영역의 Rect정보를 가지고
		// Scope의 테두리, Grid Line을 그린다.
	

		//LabelSpace = pPlane->DrawScopeInView(pDC, LabelSpace, scopeRect, ClientRect);

		//20070906 KJH
		//Aux 는 데이터가 몇개이던지 Plane은 1개만 그려지도록 한다. 
/*		if((pPlane->GetYAxis()->GetPropertyTitle().Left(3) == "Tem" && pPlane->IsDrawTemperature == TRUE) ||
			(pPlane->GetYAxis()->GetPropertyTitle().Left(3) == "Aux" && pPlane->IsDrawAuxVoltage == TRUE))
			LabelSpace = pPlane->DrawScopeInView(pDC, LabelSpace, scopeRect, ClientRect);			
		else if(pPlane->GetYAxis()->GetPropertyTitle().Left(3) != "Tem" && pPlane->GetYAxis()->GetPropertyTitle().Left(3) != "Aux")*/
		LabelSpace = pPlane->DrawScopeInView(pDC, LabelSpace, scopeRect, ClientRect);
	}

	//////////////////////////////////////////////////////////////////////////
	//Title Window 표시 
	if(m_bFirstTitle) //lyj 20210203 
	{
		SetTestName(m_QueryCondition.m_strTestName); //yulee 20190123
		m_bFirstTitle = FALSE;
	}
	if(m_bShowTitle)
	{
		//SetTestName(m_QueryCondition.m_strTestName); //yulee 20190123
		DrawTitle(pDC, scopeRect);
	}
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// 그리고, 각 Plan의 Data Line을 그린다.
	pos = m_PlaneList.GetHeadPosition();
	while(pos)
	{
		// 각 Plane에
		pPlane = m_PlaneList.GetNext(pos);
		// 좌측여백, 상단여백, 그리고 현재 Client영역의 Rect정보를 가지고 Line을 그린다.
		pPlane->DrawLineInView(pDC, scopeRect, m_QueryCondition.m_dlgQuery.m_chkDrawPauseData); //yulee 20190814
	}
	//////////////////////////////////////////////////////////////////////////

}

void CData::DrawTitle(CDC *pDC, CRect scopeRect)
{
	CFont afont, *oldfont;
	afont.CreateFont(
					   25,                        // nHeight
					   0,                         // nWidth
					   0,                         // nEscapement
					   0,                         // nOrientation
					   FW_NORMAL,                 // nWeight
					   FALSE,                     // bItalic
					   FALSE,                     // bUnderline
					   0,                         // cStrikeOut
					   ANSI_CHARSET,              // nCharSet
					   OUT_DEFAULT_PRECIS,        // nOutPrecision
					   CLIP_DEFAULT_PRECIS,       // nClipPrecision
					   DEFAULT_QUALITY,           // nQuality
					   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
					   "Arial"						// lpszFacename
					  );                 

	oldfont=pDC->SelectObject(&afont);
	pDC->SetTextAlign(TA_BASELINE|TA_CENTER);
	pDC->TextOut(scopeRect.left + scopeRect.Width()/2, scopeRect.top - scopeRect.top/2+8,  m_strTestName);
	pDC->SelectObject(oldfont);
	afont.DeleteObject();
}


/*
	Name:    GetScopeRect
	Comment: 그래프가 그려질 Scope영역의 rect를 구함
	Input:   CDC *pDC        - View의 CDC object pointer
	         RECT ClientRect - View의 Client영역 Rect
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
//그래프 영역을 구한다.
RECT CData::GetScopeRect(CDC *pDC, RECT ClientRect)
{
	//
	int LeftSpace=0;
	int MaxTopSpace=0;
	TEXTMETRIC Metrics;
	POSITION pos = m_PlaneList.GetHeadPosition();
	CFont afont,*oldfont;
	CPlane *pPlane = NULL;
	BOOL IsAuxTemp = FALSE;
	BOOL ISAuxVolt	= FALSE;
	RECT rtnError = {0,0,0,0};
	
	//20100430		Error 처리
	if(m_PlaneList.GetCount() <= 0)
	{
		return ClientRect;
	}
	//-------------------

	while(pos)
	{
		pPlane = m_PlaneList.GetNext(pos);
		afont.CreateFontIndirect(pPlane->GetYAxis()->GetFont());
		oldfont=pDC->SelectObject(&afont);
		pDC->GetTextMetrics(&Metrics);

		//각 Y축 별로 라벨 공간을 13문자 할당한다.
// 		if(pPlane->GetYAxis()->GetPropertyTitle().Left(3) == "Tem" && pPlane->IsDrawTemperature == TRUE && IsAuxTemp == FALSE)
		if(pPlane->GetYAxis()->GetPropertyTitle().Left(4) == "AuxT" && pPlane->IsDrawAuxTemperature == TRUE && IsAuxTemp == FALSE)
		{
			IsAuxTemp = TRUE;
			LeftSpace+=Metrics.tmAveCharWidth*_YAXIS_LABEL_WIDTH;
		}
//		else if(pPlane->GetYAxis()->GetPropertyTitle().Left(3) == "Aux" && pPlane->IsDrawAuxVoltage == TRUE && ISAuxVolt == FALSE)
		else if(pPlane->GetYAxis()->GetPropertyTitle().Left(4) == "AuxV" && pPlane->IsDrawAuxVoltage == TRUE && ISAuxVolt == FALSE)
		{
			ISAuxVolt = TRUE;
			LeftSpace+=Metrics.tmAveCharWidth*_YAXIS_LABEL_WIDTH;
		}
// 		else if((pPlane->GetYAxis()->GetPropertyTitle().Left(3) == "Tem" && pPlane->IsDrawTemperature == FALSE && IsAuxTemp == TRUE)||
// 			(pPlane->GetYAxis()->GetPropertyTitle().Left(3) == "Aux" && pPlane->IsDrawAuxVoltage == FALSE && ISAuxVolt == TRUE));
		else if((((pPlane->GetYAxis()->GetPropertyTitle().Left(4) == "AuxT") && (pPlane->GetYAxis()->GetPropertyTitle().Left(5) != "AuxTH")) && pPlane->IsDrawAuxTemperature == FALSE && IsAuxTemp == TRUE)||
			(pPlane->GetYAxis()->GetPropertyTitle().Left(4) == "AuxV" && pPlane->IsDrawAuxVoltage == FALSE && ISAuxVolt == TRUE));
		else
			LeftSpace+=Metrics.tmAveCharWidth*_YAXIS_LABEL_WIDTH;		

		//Y축 중 가장 큰 폰트의 1글자 공간을 위에서 뛰다.
		if((Metrics.tmHeight+Metrics.tmExternalLeading*2) > MaxTopSpace)
			MaxTopSpace = Metrics.tmHeight+Metrics.tmExternalLeading*2;
		
		pDC->SelectObject(oldfont);
		afont.DeleteObject();
	}

	//Title Space로 전체 높이의 5% 할당
	if(m_bShowTitle)
	{
		MaxTopSpace += int((ClientRect.bottom-ClientRect.top) *0.05f);
	}

	//
	if(pPlane == NULL) return rtnError;
	
	pPlane = m_PlaneList.GetHead();
	// View에서 Scope의 크기를 정한다.
	TEXTMETRIC XAxisTextMetric;
	{
		CFont afont, *oldfont;
		afont.CreateFontIndirect(pPlane->GetXAxis()->GetFont());
		oldfont=pDC->SelectObject(&afont);
		pDC->GetTextMetrics(&XAxisTextMetric);
		pDC->SelectObject(oldfont);
		afont.DeleteObject();
	}

	//
	RECT ScopeRect = { ClientRect.left + LeftSpace,
					   ClientRect.top  + MaxTopSpace,
					   //ClientRect.right  - 260/*XAxisTextMetric.tmAveCharWidth * 6*/,		//List control 표시 영역
					   ClientRect.right  - XAxisTextMetric.tmAveCharWidth * 6,		//List control 표시 영역
					   ClientRect.bottom - (XAxisTextMetric.tmHeight+XAxisTextMetric.tmExternalLeading)*3 };
	return ScopeRect;
	

	
	
}

/*
	Name:    ManageData
	Comment: Data관리를 위한 대화상자를 나타냄
	Input:   CStringList* pRunningTestList - 현재 시스템에서 실행 중인 실험들의 이름 List
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::ManageData(CStringList* pRunningTestList)
{
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString path(szBuff);
//	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

// 	switch(nSelLanguage) 20190701
// 	{
// 	case 1: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME; break;
// 	case 2: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	case 3: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_PL; break;
// 	default : path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	}
	path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME;
	CDaoDatabase* pDatabase = new CDaoDatabase;
	pDatabase->Open(path);
		//
		CSelTestDialog aDlg;
		aDlg.m_byMode = CSelTestDialog::MODE_MANAGEMENT;
		aDlg.m_pDatabase = pDatabase;
		POSITION pos = pRunningTestList->GetHeadPosition();
		while(pos)
		{
			CString str = pRunningTestList->GetNext(pos);
			aDlg.m_RunningTestNameList.AddTail(str);
		}
		aDlg.DoModal();
	pDatabase->Close();
	delete pDatabase;
	//
	return;
}

/*
	Name:    AutoLoad
	Comment: 자동으로 각 Unit에서 data를 사용자 PC로 가져옴
	Input:   CStringList* pRunningTestList - 현재 시스템에서 실행 중인 실험들의 이름 List
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::AutoLoad(CStringList* pRunningTestList)
{
	//

	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString path(szBuff);
//	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

// 	switch(nSelLanguage) 20190701
// 	{
// 	case 1: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME;break;
// 	case 2: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	case 3: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_PL; break;
// 	default : path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	}
	path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME;
	CDaoDatabase* pDatabase = new CDaoDatabase;
	pDatabase->Open(path);
		//
		CSelTestDialog aDlg;
		aDlg.m_byMode = CSelTestDialog::MODE_AUTOLOAD;
		aDlg.m_pDatabase = pDatabase;
		POSITION pos = pRunningTestList->GetHeadPosition();
		while(pos)
		{
			CString str = pRunningTestList->GetNext(pos);
			aDlg.m_RunningTestNameList.AddTail(str);
		}
		aDlg.DoModal();
	pDatabase->Close();
	delete pDatabase;
	//
	return;
}

/*
	Name:    ShowPatternOfChData
	Comment: 적용된 패턴의 내용을 보여줌
	Input:   Channel Data가 위치한 folder의 path
	Output:  
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CData::ShowPatternOfChData(LPCTSTR strChPath)
{
	CChData* pChData = GetChData(strChPath);
	if(pChData!=NULL)
	{
		CScheduleData* pPattern = pChData->GetPattern();
		if(pPattern!=NULL) 
			pPattern->ShowContent();
//			pPattern->ShowContent(0L,0L,CMode::MODE_NO_OP);
	}
}

int CData::CheckPointInWhichChannel(CRect ClientRect, POINT destPoint)
{
	POSITION pos = m_PlaneList.GetHeadPosition();

	int nLineIndex = -1;
	CPlane *pPlane;
	while(pos)
	{
		//각 Plane의 모든 라인을 검사하여 해당 지점에 속하는 Data가 존재 하는지 검사
		pPlane = m_PlaneList.GetNext(pos);
		nLineIndex = pPlane->IsPointInLine(ClientRect, destPoint);

		if(nLineIndex >= 0)
			return nLineIndex;
	}
	return -1;
}

BOOL CData::ReLoadData()
{
	CWaitCursor wait;
	CChData* pChData = NULL;
	POSITION pos = m_ChDataList.GetHeadPosition();
	while(pos)
	{
		pChData = m_ChDataList.GetNext(pos);
		if(pChData)
		{
			pChData->ReLoadData();
		}
	}

	// Commented by 224 (2014/02/13) :
	// 기존 Record sheet 를 지우고 다시 불러오기...
	QueryData(NULL, TRUE);
	
	return TRUE;
}

//주어진 viewPoint(destPoint)에 가장 가까운 datapoint를 구하고 그 data가 어느 항목 data인지 검사 
BOOL CData::CheckPointWhichPlane(CRect rect, POINT destPoint, int &nPlanIndex, int &nLineIndex, fltPoint &fltDataPoint)
{
	POSITION pos = m_PlaneList.GetHeadPosition();

	int nLIndex = -1;
	int nPIndex = 0;
	fltPoint dataPoint;

	CPlane *pPlane;
	while(pos)
	{
		//각 Plane의 모든 라인을 검사하여 해당 지점에 속하는 Data가 존재 하는지 검사
		pPlane = m_PlaneList.GetNext(pos);
		nLIndex = pPlane->IsPointInPoint(rect, destPoint, dataPoint);
		
		if(nLIndex >= 0)
		{
			nPlanIndex = nPIndex;
			nLineIndex = nLIndex;
			fltDataPoint.x = dataPoint.x;
			fltDataPoint.y = dataPoint.y;
			return TRUE;
		}
		nPIndex++;
	}
	return FALSE;
}

//lCycle에 해당하는 Table list 구함
BOOL CData::GetTableListOfCycle(CTypedPtrList <CPtrList, CTable*>* TableList,LPCTSTR strChPath, long lCycleNo)
{
	CChData* pChData = GetChData(strChPath);
	for(int i=0; i<pChData->GetTableSize(); i++)
	{
		CTable *pTable = (CTable*)pChData->GetTableAt(i);
		if(pTable->GetCycleNo() == lCycleNo)
		{
			TableList->AddTail(pTable);
		}
	}
	return TRUE;
}


void CData::ConvertHorizonCH(CString strPath)
{
	CString strData, strDataSavePath, strFileName;
	CStdioFile aFile, aTemp;
	int nIndex, nPos = 0;
	CFileException e;
	int nCount = 0;
	int nMaxCount = 0;
	int nCommaCount = 0;

	init_Language();

	strDataSavePath.Format("%s",strPath.Left(strPath.ReverseFind('\\')), NULL);
	strFileName.Format("\\Temp.csv");

	if( !aFile.Open( strPath, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		//AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("Data_ConvertHorizonCH_msg1","PSDATA"));//&&
		return;
	}

	if( !aTemp.Open( strDataSavePath+strFileName, CFile::modeCreate|CFile::modeWrite|CFile::modeNoTruncate, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		//AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("Data_ConvertHorizonCH_msg1","PSDATA"));//&&
		return;
	}

	//Data Counting
	aFile.ReadString(strData);	//공백
	while(strData.IsEmpty())
	{
		nCount = 0;
		aFile.ReadString(strData);	//채널 정보 Check
		if(strData.IsEmpty())		break;

		while(!strData.IsEmpty())
		{
			aFile.ReadString(strData);
			nCount++;
		}

		if(nMaxCount < nCount)		nMaxCount = nCount;
	}

	//Data 기록
	for(int i=0;i<nMaxCount+1;i++)
	{
		aFile.SeekToBegin();
		aFile.ReadString(strData);	//공백
		aFile.ReadString(strData);	//채널 정보
		aFile.ReadString(strData);	//Cycle
		aFile.ReadString(strData);	//Step
		aFile.ReadString(strData);	//첫 Data
		nCommaCount = 0;
		nPos = 0;
		nIndex = 0;
		while(nIndex != -1)
		{
			nIndex = strData.Find(",", nPos);
			nPos = nIndex + 1;
			nCommaCount++;
		}

		aFile.SeekToBegin();
		aFile.ReadString(strData);	//공백
		while(strData.IsEmpty())
		{
			nCount = 0;
			aFile.ReadString(strData);	//채널 정보 Check
			if(strData.IsEmpty())		break;
			
			if(i == nCount)
			{
				for(int j=0;j<nCommaCount-2;j++)
				{
					strData += ",";
				}
				aTemp.WriteString(strData);
			}

			aFile.ReadString(strData);				
			nCount++;
			while(!strData.IsEmpty())
			{
				if(i == nCount)		aTemp.WriteString(strData);
				aFile.ReadString(strData);
				nCount++;

				if(strData.IsEmpty() && nCount < nMaxCount)
				{
					for(int j=0;j<nCommaCount-1;j++)
					{
						strData += ",";
					}

					while(nCount < nMaxCount)
					{
						if(i == nCount)		aTemp.WriteString(strData);
						nCount++;
					}
				}

				if(nCount == nMaxCount)
				{
					strData = "";
					nCount = 0;
				}
			}
		}
		aTemp.WriteString("\n");
	}

	aFile.Close();
	aTemp.Close();
 	aFile.Remove(strPath);
 	aTemp.Rename(strDataSavePath+strFileName, strPath);
}