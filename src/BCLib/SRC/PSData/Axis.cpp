/***********************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Axis.cpp

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: Member functions of CAxis class are defined
***********************************************************************/

// Axis.cpp: implementation of the CAxis class.
//
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// #include

#include "stdafx.h"
#include "Axis.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
	Name:    CAxis
	Comment: Constructor of CAxis class
	Input:   Title, notation, transfer value
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CAxis::CAxis(LPCTSTR title, LPCTSTR notation, float transfer)
: m_strTitle(title), m_strUnitNotation(notation), m_fltUnitTransfer(transfer)
{
	// Initial values of min/max value and grid interval
	m_fltMax  = 0.0f;
	m_fltMin  = 0.0f;
	m_fltGrid = 1.0f;

	// Initial label font -> Main Frame의 상태바의 font로 초기화 한다.
	//                       즉, CAxis의 object는 상태바가 있는 Main Frame을
	//                       가지는 Application에서 상태바가 create된 후 생성할 수 있다.
	((CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR)))->GetFont()->GetLogFont(&m_Font);

	// Initial label color is black
	m_LabelColor = RGB(0,0,0);
	m_bShowGrid = TRUE;
	init_Language();
}

/*
	Name:    ~CAxis
	Comment: Destructor of CAxis class
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CAxis::~CAxis()
{
}

/*
	Name:    Operator =
	Comment: Title, Unit notation, and Unit transfer value의
	         3가지 항목만 일치시킨다.
	Input:   Reference of a CAxis object
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CAxis::operator =(const CAxis& OrgAxis)
{
	m_strTitle        = OrgAxis.m_strTitle;
	m_fltUnitTransfer = OrgAxis.m_fltUnitTransfer;
	m_strUnitNotation = OrgAxis.m_strUnitNotation;
}

/*
	Name:    AddMinMaxBuffer
	Comment: Zoom operation에서 Zoom In 동작 시
	         Scope의 이전 min/max값을 list에 저장한다.
	Input:   Min/Max value of Scope
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
void CAxis::AddMinMaxBuffer(float min, float max)
{
	// 입력된 min/max value를 fltPoint형의 object에 넣어서
	fltPoint min_max;
	min_max.x = m_fltMin;
	min_max.y = m_fltMax;
	// List에 저장하고
	m_MinMaxBuffer.AddTail(min_max);
	// 현 Min/Max값을 바꾼다.
	m_fltMin  = min;
	m_fltMax  = max;
}


/*
	Name:    GetPropertyTitle
	Comment: 축의 title은 다르나 같은 평면 상에 데이터를 그려야 하는
	         경우를 위하여 Property의 Title을 정의하여 return한다.
	Input:   
	Output:  Property title
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CString CAxis::GetPropertyTitle()
{
	// 다음의 경우에는 "Voltage"와 같은 Plane에 데이터를 그린다.
	if(m_strTitle.CompareNoCase("Voltage1")==0)     return "Voltage";
	if(m_strTitle.CompareNoCase("Voltage2")==0)     return "Voltage";
	if(m_strTitle.CompareNoCase("Voltage3")==0)     return "Voltage";
	if(m_strTitle.CompareNoCase("Voltage4")==0)     return "Voltage";
	if(m_strTitle.CompareNoCase("V_MaxDiff")==0)    return "Voltage";
	if(m_strTitle.CompareNoCase("OCV")==0)          return "Voltage";
	// 다음의 경우에는 "Temperature"와 같은 Plane에 데이터를 그린다.
	if(m_strTitle.CompareNoCase("Temperature1")==0) return "Temperature";
	if(m_strTitle.CompareNoCase("Temperature2")==0) return "Temperature";
	if(m_strTitle.CompareNoCase("T_MaxDiff")==0)    return "Temperature";

	//ljb 20131204 다음의 경우에는 "AuxV"와 같은 Plane에 데이터를 그린다.
//	if(m_strTitle.Left(3)== "AuxV") return "Voltage";
	
	
	// 그 외의 경우에는 Axis title을 그대로 사용한다.
	return m_strTitle;
}