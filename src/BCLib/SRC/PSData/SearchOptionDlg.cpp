// SearchOptionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SearchOptionDlg.h"
//#include "../CTSAnal/CTSAnal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchOptionDlg dialog


CSearchOptionDlg::CSearchOptionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSearchOptionDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSearchOptionDlg::IDD2):
	(CSearchOptionDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSearchOptionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSearchOptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchOptionDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSearchOptionDlg, CDialog)
	//{{AFX_MSG_MAP(CSearchOptionDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchOptionDlg message handlers

BOOL CSearchOptionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSearchOptionDlg::OnOK() 
{
	m_nFindTestNum = 0;		

	CString strTestName, strTemp;
	GetDlgItemText(IDC_EDIT_TEST_LIKE_NAME, strTemp);
	
	if(strTemp.IsEmpty() == FALSE)
	{
/*		CEPTesterApp* pApp = (CEPTesterApp *)AfxGetApp();
		CString path(pApp->m_szCurrentDirectory);

		strTestName.Format("%s\\%s*", path, strTemp);

		CFileFind aFinder;
		BOOL bFound = aFinder.FindFile(strTestName);
		while(bFound)
		{
			bFound = aFinder.FindNextFile();
			if(aFinder.IsDirectory() && !aFinder.IsDots())
			{
				m_strlistFindTestList.AddTail(aFinder.GetFileName());
				m_nFindTestNum++;
			}			
		} */
		m_nFindTestNum = 1;
		m_strSearchKey = strTemp;
	}
	CDialog::OnOK();
}