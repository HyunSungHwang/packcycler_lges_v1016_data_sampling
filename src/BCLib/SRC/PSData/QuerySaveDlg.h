#if !defined(AFX_QUERYSAVEDLG_H__AAA14142_118A_41DE_8FB8_B3DF2D7C975D__INCLUDED_)
#define AFX_QUERYSAVEDLG_H__AAA14142_118A_41DE_8FB8_B3DF2D7C975D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// QuerySaveDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CQuerySaveDlg dialog
#include "resource.h"
class CQuerySaveDlg : public CDialog
{
// Construction
public:
	ULONG m_ulMaxNumOfData;
	ULONG m_ulSaveInterval;
	ULONG m_ulTotalNumOfData;
	ULONG m_ulSavedNumOfResult;
	CQuerySaveDlg(ULONG ulMaxNumOfData, ULONG ulTotalNumOfData, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CQuerySaveDlg)
	enum { IDD = IDD_SAVE_QUERY_DLG , IDD2 = IDD_SAVE_QUERY_DLG_ENG , IDD3 = IDD_SAVE_QUERY_DLG_PL  };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CQuerySaveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CQuerySaveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditInterval();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_QUERYSAVEDLG_H__AAA14142_118A_41DE_8FB8_B3DF2D7C975D__INCLUDED_)
