/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: SortTypeDlg.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CSortTypeDialog class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_SORTTYPEDLG_H__D9C90144_047F_457F_8D25_180C687B2298__INCLUDED_)
#define AFX_SORTTYPEDLG_H__D9C90144_047F_457F_8D25_180C687B2298__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// SortTypeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSortTypeDialog dialog

// #include

#include "resource.h"

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CSortTypeDialog
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CSortTypeDialog : public CDialog
{
// Construction
public:
	CSortTypeDialog(int nMode = modeBasic, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSortTypeDialog)
	enum { IDD = IDD_SORT_TYPE_DLG , IDD2 = IDD_SORT_TYPE_DLG_ENG , IDD3 = IDD_SORT_TYPE_DLG_PL };
	int		m_iFirst;
	int		m_iSecond;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSortTypeDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	int m_nMode;
	// Generated message map functions
	//{{AFX_MSG(CSortTypeDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
public:
	enum { modeBasic = 0, modeSimple = 1};
	int m_iType;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SORTTYPEDLG_H__D9C90144_047F_457F_8D25_180C687B2298__INCLUDED_)
