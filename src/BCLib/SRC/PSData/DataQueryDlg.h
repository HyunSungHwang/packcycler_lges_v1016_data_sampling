/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: DataQueryDlg.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CDataQueryDialog class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_DATAQUERYDLG_H__90DAB181_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_DATAQUERYDLG_H__90DAB181_E17D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataQueryDlg.h : header file		
//

/////////////////////////////////////////////////////////////////////////////
// CDataQueryDialog dialog

// #include

#include "resource.h"
class CDataQueryCondition;
class CDaoDatabase;

#define REG_CONFIG_SECTION	"Config"
#define QUERY_REG_SECTION	"Query"
// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CDataQueryDialog
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CDataQueryDialog : public CDialog
{
// Construction
public:
	CDataQueryDialog(CWnd* pParent = NULL);   // standard constructor
	
////////////////////////////////////
// 2003-02-25 추가 요구사항
	CString m_strTestName;
////////////////////////////////////

// Dialog Data
	//{{AFX_DATA(CDataQueryDialog)
	enum { IDD = IDD_DATA_QUERY_DIALOG , IDD2 = IDD_DATA_QUERY_DIALOG_ENG, IDD3 = IDD_DATA_QUERY_DIALOG_PL };
	CListBox	m_ctrlCanListSetD;
	CListBox	m_ctrlCanListSetC;
	CListBox	m_ctrlCanListSetB;
	CListBox	m_ctrlCanListSetA;
	CCheckListBox	m_ctrlCanList;
	CListBox	m_SelStepList;
	CListCtrl	m_ctrlYAxisList;
	CCheckListBox	m_ctrlAuxList;
	CListCtrl	m_TestNameListCtrl;
	CCheckListBox	m_YAxisList;
	CComboBox	m_YUnitCombo;
	CComboBox	m_XUnitCombo;
	CComboBox	m_XAxisCombo;
	CListBox	m_SelCycleList;
	CListCtrl	m_ChannelListCtrl;
	long	m_lFromCycle;
	long	m_lIntervalCycle;
	BOOL	m_bCycleStringCheck;
	CString	m_strCycle;
	long	m_lToCycle;
	int		m_iMode;
	int		m_iStartType;
	BOOL	m_chkCharge;
	BOOL	m_chkDisCharge;
	BOOL	m_chkRest;
	BOOL	m_chkPattern;
	BOOL	m_chkImpedance;
	BOOL	m_chkExtCAN;
	//}}AFX_DATA
	long	m_lFromTable;
	long	m_lToTable;
	BOOL	m_bBtnCANSetA;
	BOOL	m_bBtnCANSetB;
	BOOL	m_bBtnCANSetC;
	BOOL	m_bBtnCANSetD;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataQueryDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	INT GetNumberOfStepEndData(CString strPath, INT nBegin, INT nEnd, INT nQueryMode);

	// Generated message map functions
	//{{AFX_MSG(CDataQueryDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddSelCycleBtn();
	afx_msg void OnDelSelCycleBtn();
	afx_msg void OnCycleStringCheck();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeXAxisCombo();
	afx_msg void OnSelchangeYAxisList();
	afx_msg void OnSelchangeYUnitCombo();
	afx_msg void OnTestFromFolderBtn();
	afx_msg void OnTestFromListBtn();
	afx_msg void OnTestFromSearch();
	afx_msg void OnItemchangedChannelList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedYAxisCtrlList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBtnAllSelect();
	afx_msg void OnBtnAllUnSelect();
	afx_msg void OnButtonAll();
	afx_msg void OnButtonCancel();
	afx_msg void OnChargeCheck();
	afx_msg void OnDischargeCheck();
	afx_msg void OnRestCheck();
	afx_msg void OnImpCheck();
	afx_msg void OnPatternCheck();
	afx_msg void OnChangeEditMaxrow();
	afx_msg void OnBtnAllCanSelect();
	afx_msg void OnBtnAllCanUnSelect();
	afx_msg void OnBtnCanSelectsetA();
	afx_msg void OnBtnCanSelectsetB();
	afx_msg void OnBtnCanSelectsetC();
	afx_msg void OnBtnCanSelectsetD();
	afx_msg void OnBtnCanSelectsetAClr();
	afx_msg void OnBtnCanSelectsetBClr();
	afx_msg void OnBtnCanSelectsetCClr();
	afx_msg void OnBtnCanSelectsetDClr();
	afx_msg void OnSelchangeCanSelectList();
	afx_msg void OnSelchangeAuxSelectList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

////////////////
// Attributes //
////////////////
private:

	CByteArray yAuxArray;
	int nAuxListCheck;
	// 속해 있는 CDataQueryCondition object의 Pointer
	CDataQueryCondition*   m_pQueryCondition;

	// Dialog Box가 열려 있는 동안 사용할 변수들
	CDaoDatabase*          m_pDatabase;
	BYTE*                  m_pbyXAxisIndex;			//X 축으로 사용 가능한 리스트 저장 

	// Dialog Box를 Reload할 때 ListCtrl이나 ListBox의 내용을
	// 채우기 위해 필요한 내용을 저장하는 String List
public:
	CStringList            m_strlistTestPath;		//선택 시험명 리스트 
	BYTE                   m_byVoltagePoint;
	BYTE                   m_byTemperaturePoint;
	BOOL					m_bCheck[256]; //yulee 20180905
private:
	
	long				m_bQueryMode;				//20100430

	CStringList            m_strlistSelCycle;		//Cycle 구간 선택 리스트 
	CStringList				*m_pstrPathList;		//lmh 20120727
	CList<CPoint, CPoint&> m_YAxisPropertyIndex;
	
////////////////
// Operations //
////////////////
public:
	//20104030 //
//	BOOL SetRangeList(CDWordArray *pSelRangeArray);
	BOOL SetRangeList2(CDWordArray *pSelRangeArray);
	void SetQueryMode(long lQueryMode)	{	m_bQueryMode = lQueryMode; }
	LONG GetQueryMode()					{	return	m_bQueryMode;		}
	void UpdateStepNoList();
	UINT GetCtsFileVersion(CString strCtsFilePath); //ksj 20210326
	
	//BOOL bAuxListFlag;
	BOOL bCanListFlag;
	BOOL IsClickedAuxItem;
	void UpdateAuxList(CString strAuxName);
	void UpdateCanList(CString strCanName);		//ljb 2011210 이재복 //////////
	void WriteRegUserSet();
	void AddTestPath(LPCTSTR path, int nIndex);
	void SetQueryCondition(CDataQueryCondition* pCondition) { m_pQueryCondition = pCondition;};
	void InitCtrlListAuxBtn(); //yulee 20181008
	void InitCtrlListCANBtn(); //yulee 20181008
	int* GetSelectedItemArr(); //yulee 20181008
	int m_nSelectedItemArr[127]; //yulee 20181008
	int m_nSelectedItemNum;		//yulee 20181008
private:
	//20104030 ------------------
	int UpdateStepTypeSelect();
	int GetFistTestSchedule(CString strTestPath, CScheduleData *pScheule); 
	enum {
		QUERY_MODE_CYCLE = 0,
			QUERY_MODE_INDEX = 1
	};
	//----------------------------
	
	void SortChannelListCtrl();
	static int CALLBACK ChannelCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

public:
	BOOL m_chkDrawPauseData; //yulee 20190814
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAQUERYDLG_H__90DAB181_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
