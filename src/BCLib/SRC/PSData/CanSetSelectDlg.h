#if !defined(AFX_CANSETSELECTDLG_H__1730F384_80E4_4006_8383_5E7AA3E078DD__INCLUDED_)
#define AFX_CANSETSELECTDLG_H__1730F384_80E4_4006_8383_5E7AA3E078DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CanSetSelectDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCanSetSelectDlg dialog
#include "resource.h"
class CCanSetSelectDlg : public CDialog
{
// Construction
public:
	CCanSetSelectDlg(CWnd* pParent = NULL);   // standard constructor
	void UpdateCanSetSelectList();		//yulee 20180902



// Dialog Data
	//{{AFX_DATA(CCanSetSelectDlg)
	enum { IDD = IDD_CAN_SET_SELECT_LIST , IDD2 = IDD_CAN_SET_SELECT_LIST_ENG };
	CCheckListBox	m_ctrlCanSetSelectList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCanSetSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL
public:
	int* GetSelectedItemArr();
	int  GetSelectedItemNum();
	int m_nSelectedItemArr[127];
	int m_nSelectedItemNum;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCanSetSelectDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CANSETSELECTDLG_H__1730F384_80E4_4006_8383_5E7AA3E078DD__INCLUDED_)
