// SelTestDlg.cpp : implementation file
//

#include "stdafx.h"
//#include "../CTSAnal/CTSAnal.h"
#include "SelTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define AUTO_LOAD_TIMER 500

/////////////////////////////////////////////////////////////////////////////
// CSelTestDialog dialog


CSelTestDialog::CSelTestDialog(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSelTestDialog::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSelTestDialog::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSelTestDialog::IDD3):
	(CSelTestDialog::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSelTestDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pLargeImage=NULL;
	m_pSmallImage=NULL;
	m_bNameSortToggle=FALSE;
	m_bDateSortToggle=TRUE;
}

void CSelTestDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelTestDialog)
	DDX_Control(pDX, IDC_NETWORK_ERROR_LIST, m_NetworkErrorList);
	DDX_Control(pDX, IDC_TEST_LIST, m_TestList);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate&&(m_byMode==MODE_SELECTION||m_byMode==MODE_SEARCH))
	{
		CWaitCursor cursor;

		// 선택한 Test이름의 List를 만든다.
		CStringList strlistTestName;
		POSITION pos = m_TestList.GetFirstSelectedItemPosition();
		if(pos==NULL){
			MessageBox("Select the test.", "Test Selection", MB_ICONEXCLAMATION);
			pDX->Fail();
		}
		while(pos)
		{
			int nItem = m_TestList.GetNextSelectedItem(pos);
			CString strTestName = m_TestList.GetItemText(nItem,0);
			strTestName.MakeLower();
			strlistTestName.AddTail(strTestName);
		}
		// m_TestPathList 중 선택한 Test이름의 경로만 남긴다.
		pos = m_TestPathList.GetHeadPosition();
		while(pos)
		{
			POSITION PrePos = pos;
			CString strPath = m_TestPathList.GetNext(pos);
			CString strName = strPath.Mid(strPath.ReverseFind('\\')+1);
			if(strlistTestName.Find(strName)==NULL)
			{
				m_TestPathList.RemoveAt(PrePos);
			}
		}
	}
}


BEGIN_MESSAGE_MAP(CSelTestDialog, CDialog)
	//{{AFX_MSG_MAP(CSelTestDialog)
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_TEST_LIST, OnColumnclickTestList)
	ON_NOTIFY(NM_DBLCLK, IDC_TEST_LIST, OnDblclkTestList)
	ON_COMMAND(IDM_DATA_BRING, OnDataBring)	
	ON_COMMAND(IDM_DATA_DELETE, OnDataDelete)
	ON_COMMAND(IDM_DATA_MOVE, OnDataMove)
	ON_WM_CONTEXTMENU()
	ON_WM_TIMER()
	ON_COMMAND(IDM_DATA_DESTROY, OnDataDestroy)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelTestDialog message handlers

BOOL CSelTestDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();	
	CWaitCursor acursor;

	//if     (m_byMode==MODE_MANAGEMENT) SetWindowText("시험 데이터 탐색기");
	if     (m_byMode==MODE_MANAGEMENT) SetWindowText(Fun_FindMsg("SelTestDialog_OnInitDialog_msg1","IDD_TEST_LIST_DLG"));//&&
	else if(m_byMode==MODE_SELECTION)  SetWindowText("Select Data");
	//else if(m_byMode==MODE_AUTOLOAD)   SetWindowText("결과 데이터 Downloading");
	else if(m_byMode==MODE_AUTOLOAD)   SetWindowText(Fun_FindMsg("SelTestDialog_OnInitDialog_msg2","IDD_TEST_LIST_DLG"));//&&
	//else if(m_byMode==MODE_SEARCH)	   SetWindowText("Data 검색 결과");
	else if(m_byMode==MODE_SEARCH)	   SetWindowText(Fun_FindMsg("SelTestDialog_OnInitDialog_msg3","IDD_TEST_LIST_DLG"));//&&

	//
	// 1. ListCtrl의 Image등록
	//
	m_pLargeImage = new CImageList;
	m_pSmallImage = new CImageList;
	m_pLargeImage->Create(16,16,ILC_COLOR4,3,3);
	m_pSmallImage->Create(16,16,ILC_COLOR4,3,3);
	CBitmap cBit;
	cBit.LoadBitmap(IDB_HDD);
	m_pLargeImage->Add(&cBit,RGB(0,0,0));
	m_pSmallImage->Add(&cBit,RGB(0,0,0));
	cBit.DeleteObject();
	cBit.LoadBitmap(IDB_NETWORK);
	m_pLargeImage->Add(&cBit,RGB(0,0,0));
	m_pSmallImage->Add(&cBit,RGB(0,0,0));
	cBit.DeleteObject();
	cBit.LoadBitmap(IDB_RUN);
	m_pLargeImage->Add(&cBit,RGB(0,0,0));
	m_pSmallImage->Add(&cBit,RGB(0,0,0));
	cBit.DeleteObject();
	m_TestList.SetImageList(m_pLargeImage,LVSIL_NORMAL);
	m_TestList.SetImageList(m_pSmallImage,LVSIL_SMALL);

	//
	// 2. ListCtrl의 Column설정
	//
	m_TestList.InsertColumn(1, "Name", LVCFMT_LEFT, 132, 0);
	m_TestList.InsertColumn(2, "Date", LVCFMT_LEFT, 120, 0);
	m_TestList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	//
	CStringList tempList;
	int iItem = 0;

#ifdef __VERSION_LG__
	CStringList strList ;
	//
	// 3. 연결된 Unit들의 Path를 Database에서 읽는다.
	//

	CDaoRecordset aRecordset(m_pDatabase);
	aRecordset.Open(dbOpenTable, "UnitInfo");
		//
		while(!aRecordset.IsEOF())
		{
			//
			COleVariant var;
			aRecordset.GetFieldValue("Path", var);
			CString strPath = (LPCTSTR)(LPTSTR)var.bstrVal;
			var.Clear();
			//
			aRecordset.GetFieldValue("Comm", var);
			if(var.boolVal)	m_UnitPathList.AddTail(strPath);
			else            m_NetworkErrorList.AddString(strPath);
			//
			aRecordset.MoveNext();
		}
	aRecordset.Close();

	//
	// 4. 각 Path에서 Test이름을 얻는다.
	//

	tempList.RemoveAll();
	int iNumOfUnit = m_UnitPathList.GetCount();
	for(int i=0; i<iNumOfUnit; i++)
	{
		CString strUnitPath = m_UnitPathList.GetAt(m_UnitPathList.FindIndex(i));
		CFileFind afinder;
		BOOL bWork;

		if(m_byMode==MODE_SEARCH)
			bWork = afinder.FindFile(strUnitPath+CString("\\data\\*")+m_strSearchKey+CString("*"));
		else
			bWork = afinder.FindFile(strUnitPath+"\\data\\*.*");
		if(!bWork){
			LPVOID lpMsgBuf;
			FormatMessage( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL 
			);
			m_NetworkErrorList.AddString(strUnitPath);
			m_NetworkErrorList.AddString(CString(" : ")+CString((LPCTSTR)lpMsgBuf));
			LocalFree( lpMsgBuf );
		}
		while(bWork)
		{
			bWork = afinder.FindNextFile();
			if(!afinder.IsDots()&&afinder.IsDirectory())
			{
				// Test Path 저장
				CString strTestPath = afinder.GetFilePath();
				strTestPath.MakeLower();
				m_TestPathList.AddTail(strTestPath);

				// 모든 Path를 임시 리스트에 저장한다.
				strList.AddTail(strTestPath);
				
				// Test Name을 ListCtrl에 등록
				CString strTestName = afinder.GetFileName();

				if(tempList.Find(strTestName)==NULL)
				{
					tempList.AddTail(strTestName);
					LVITEM lvitem;
					lvitem.iItem  = iItem;
					if(m_RunningTestNameList.Find(strTestPath.Mid(strTestPath.ReverseFind('\\')+1))==NULL)
						 lvitem.iImage = 1;
					else lvitem.iImage = 2;
					//
					lvitem.iSubItem = 0;
					lvitem.mask   = LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
					lvitem.lParam = iItem;
					lvitem.pszText  = (LPTSTR)(LPCTSTR) strTestName;
					m_TestList.InsertItem(&lvitem);
					//
					lvitem.iSubItem = 1;
					lvitem.mask   = LVIF_TEXT;
					FILETIME FileTime;
					afinder.GetCreationTime(&FileTime);
					CString str;
					str = GetTestStartTime(strTestPath).Format("%m/%d/%Y %H:%M:%S");
					lvitem.pszText  = (LPTSTR)(LPCTSTR) str;
					m_TestList.SetItem(&lvitem);
					//
					if(m_byMode==MODE_AUTOLOAD&&lvitem.iImage==1) m_TestList.SetItemState(iItem,LVIS_SELECTED,LVIS_SELECTED);
					iItem++;
				}
			}
		}
	}
#endif

	//
	// 5. 다운로드 완료된 Data를 ListCtrl에 넣는다.
	//

//	CCTSAnalApp* pApp = (CCTSAnalApp *)AfxGetApp();
//	CString path(pApp->m_szCurrentDirectory);
	CFileFind afinder;

	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString path(szBuff);

	BOOL bWork;
	if(m_byMode==MODE_SEARCH)
		bWork = afinder.FindFile(path + CString("\\*") + m_strSearchKey + CString("*"));
	else
		bWork = afinder.FindFile(path + "\\*.*");

	while(bWork)
	{
		bWork = afinder.FindNextFile();
		//
		if(!afinder.IsDots()&&afinder.IsDirectory())
		{
			// Test Path 저장
			CString strTestPath = afinder.GetFilePath();
			strTestPath.MakeLower();
			m_TestPathList.AddTail(strTestPath);

			// Test Name을 ListCtrl에 등록
			CString strTestName = afinder.GetFileName();
			
			// 이미 각 Unit에서 가져온 결과파일의 이름과 같은지 조사하기 위한 구조체
			LVFINDINFO lvFind;
			lvFind.psz = (LPSTR)(LPCTSTR)strTestName;

			// 같은 아이템이 존재하지 않는다면 리스트 컨트롤에 삽입
			if(tempList.Find(strTestName)==NULL && m_TestList.FindItem(&lvFind, 0) == -1)
			{
				tempList.AddTail(strTestName);
				LVITEM lvitem;
				lvitem.iItem  = iItem;
				lvitem.iImage = 0;
					//
					lvitem.iSubItem = 0;
					lvitem.mask   = LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
					lvitem.lParam = iItem;
					lvitem.pszText  = (LPTSTR)(LPCTSTR) strTestName;
					m_TestList.InsertItem(&lvitem);
					//
					lvitem.iSubItem = 1;
					lvitem.mask   = LVIF_TEXT;
					CString str;
					str = GetTestStartTime(strTestPath).Format("%m/%d/%Y %H:%M:%S");
					lvitem.pszText  = (LPTSTR)(LPCTSTR) str;
					m_TestList.SetItem(&lvitem);
				iItem++;
			}
			// 같은 아이템이 존재한다면 ..
			//	1. 데이터 다운로딩과 관련있는 항목이면 
			//	   아이템의 경로를 Unit의 경로로 설정하고
			//	2. 결과 분석(그래프)과 관련있는 항목이면
			//	   아이템의 경로를 다운로드 된 경로로 설정한다. 
			else
			{
#ifdef __VERSION_LG__
				if (m_byMode==MODE_MANAGEMENT || m_byMode==MODE_AUTOLOAD) 
				{
					CString strReplaceItem = m_TestPathList.RemoveTail();
				}		
				else
				{
					POSITION pos = strList.GetHeadPosition();
					while(pos)
					{
						CString strTmp = strList.GetAt(pos);
						strTestName.MakeLower();
						if( strTmp.Find(strTestName) != -1 )
						{
							POSITION ps = m_TestPathList.Find(strTmp);
							if (ps)
							{
								CString strReplaceItem = m_TestPathList.RemoveTail();
								CString strRemoveItem = m_TestPathList.GetAt(ps);
								m_TestPathList.SetAt(ps, strReplaceItem);
							}
							break;
						}
						strList.GetNext(pos);
					}
				}
#endif
			}
		}
	}

#ifdef __VERSION_LG__
	//
	// Network Error가 있는 경우 알려준다.
	//
	if(m_NetworkErrorList.GetCount()<=0)
	{
		CRect rect1, rect2;
		GetWindowRect(rect1);
		((CStatic*)GetDlgItem(IDC_STATIC_SEPE_LINE))->GetWindowRect(rect2);
		MoveWindow(rect1.left,rect1.top,rect1.Width(),rect2.bottom-rect1.top);
	}
	else if(m_byMode==MODE_MANAGEMENT) 
		CDialog::OnOK();
#else//__VERSION_EP__
	CRect rect1, rect2;
	GetWindowRect(rect1);
	((CStatic*)GetDlgItem(IDC_STATIC_SEPE_LINE))->GetWindowRect(rect2);
	MoveWindow(rect1.left,rect1.top,rect1.Width(),rect2.bottom-rect1.top);
#endif

	if(m_byMode&(MODE_MANAGEMENT|MODE_AUTOLOAD))
	{
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);
		if(m_byMode==MODE_AUTOLOAD) GetDlgItem(IDCANCEL)->ShowWindow(SW_HIDE);
		else
		{
			GetDlgItem(IDCANCEL)->SetWindowText("End");
			CRect rect1, rect2;
			GetClientRect(rect1);
			GetDlgItem(IDCANCEL)->GetClientRect(rect2);
			GetDlgItem(IDCANCEL)->MoveWindow((rect1.Width()-rect2.Width())/2, rect1.bottom-rect2.Height()-5, rect2.Width(), rect2.Height());
		}
	}

	//
	// 시간 순서로 List를 Sorting 한다.
	//
	m_TestList.SortItems(TestDateASCCompareProc,(LPARAM)(&m_TestList));
	for (int j=0;j < m_TestList.GetItemCount();j++)
	{
	   m_TestList.SetItemData(j, j);
	}
	m_bDateSortToggle=!m_bDateSortToggle;

	//
	if(m_byMode==MODE_AUTOLOAD) SetTimer(AUTO_LOAD_TIMER,AUTO_LOAD_TIMER,NULL);

	//
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelTestDialog::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if(m_pLargeImage!=NULL) delete m_pLargeImage;	
	if(m_pSmallImage!=NULL) delete m_pSmallImage;
}

COleDateTime CSelTestDialog::GetTestStartTime(LPCTSTR path)
{
	CString strPath(path);
	strPath += "\\m*ch*";
	CFileFind afinder;
//	BOOL bFail = TRUE;
	BOOL bWork = afinder.FindFile(strPath);
	while(bWork)
	{
		bWork = afinder.FindNextFile();
		if(afinder.IsDirectory())
		{
			strPath = afinder.GetFilePath();
			break;
		}
	}

	COleDateTime date(100,1,1,0,0,0);

	strPath += "\\TableList";
	CStdioFile afile;
	if(afile.Open(strPath, CFile::modeRead|CFile::typeText|
						   CFile::shareDenyNone))
	{
		//
		CString strTemp;
		afile.ReadString(strTemp);
		afile.Close();
		int p1 = strTemp.Find('=');
		int p2 = strTemp.Find(',');
		date.ParseDateTime(strTemp.Mid(p1+1,p2-p1-1));
	}

	//
	return date;
}

void CSelTestDialog::OnColumnclickTestList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	CWaitCursor acursor;

	if(pNMListView->iSubItem==0)
	{
		if(m_bNameSortToggle) m_TestList.SortItems(TestNameASCCompareProc,(LPARAM)(&m_TestList));
		else                  m_TestList.SortItems(TestNameDECCompareProc,(LPARAM)(&m_TestList));
		for (int i=0;i < m_TestList.GetItemCount();i++)
		{
		   m_TestList.SetItemData(i, i);
		}
		m_bNameSortToggle=!m_bNameSortToggle;
	}
	else if(pNMListView->iSubItem==1)
	{
		if(m_bDateSortToggle) m_TestList.SortItems(TestDateASCCompareProc,(LPARAM)(&m_TestList));
		else                  m_TestList.SortItems(TestDateDECCompareProc,(LPARAM)(&m_TestList));
		for (int i=0;i < m_TestList.GetItemCount();i++)
		{
		   m_TestList.SetItemData(i, i);
		}
		m_bDateSortToggle=!m_bDateSortToggle;
	}
	*pResult = 0;
}

int CALLBACK CSelTestDialog::TestNameASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam2, 0);
   CString    strItem2 = pListCtrl->GetItemText(lParam1, 0);

   return strcmp(strItem2, strItem1);
}

int CALLBACK CSelTestDialog::TestNameDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam2, 0);
   CString    strItem2 = pListCtrl->GetItemText(lParam1, 0);

   return strcmp(strItem1, strItem2);
}

int CALLBACK CSelTestDialog::TestDateASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam1, 1);
   CString    strItem2 = pListCtrl->GetItemText(lParam2, 1);

   COleDateTime date1; date1.ParseDateTime(strItem1);
   COleDateTime date2; date2.ParseDateTime(strItem2);

   if     (date1>date2) return -1;
   else if(date1<date2) return  1;
   else                 return  0;
}

int CALLBACK CSelTestDialog::TestDateDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString    strItem1 = pListCtrl->GetItemText(lParam1, 1);
   CString    strItem2 = pListCtrl->GetItemText(lParam2, 1);

   COleDateTime date1; date1.ParseDateTime(strItem1);
   COleDateTime date2; date2.ParseDateTime(strItem2);

   if     (date1>date2) return  1;
   else if(date1<date2) return -1;
   else                 return  0;
}

void CSelTestDialog::OnDblclkTestList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CDialog::OnOK();	
	*pResult = 0;
}

void CSelTestDialog::OnDataBring() 
{
	CWaitCursor cursor;

	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString OriginPath(szBuff);
	OriginPath = OriginPath.Left(OriginPath.ReverseFind('\\'));
	OriginPath.MakeLower();

//	CCTSAnalApp *pApp = (CCTSAnalApp*)AfxGetApp();
//	CString OriginPath(pApp->m_szCurrentDirectory);
//	OriginPath = OriginPath.Left(OriginPath.ReverseFind('\\'));
// 	OriginPath.MakeLower();

	// StatusBar에 Progress 준비
	CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
	CProgressCtrl ProgressCtrl;
	CRect rect;
	pStatusBar->GetItemRect(0,rect);
	rect.left = rect.Width()/2;
	ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
	ProgressCtrl.SetRange(0,m_TestList.GetSelectedCount());

	// 선택한 Item을
	int nPos=0;
	POSITION pos = 	m_TestList.GetFirstSelectedItemPosition();
	while(pos)
	{
		// 하나씩 가져와서 그 Test이름을 얻는다.
		int nItem = m_TestList.GetNextSelectedItem(pos);
		CString strTestName = m_TestList.GetItemText(nItem,0);
		strTestName.MakeLower();

		// 현재 Running 중인 Test가 아니면
		if(m_RunningTestNameList.Find(strTestName)==NULL)
		{
			// Network 상의 그 Path를 모두 모아서
			CStringList strPathList;
			CList<POSITION, POSITION&> posPathList;
			POSITION ps = m_TestPathList.GetHeadPosition();
			while(ps)
			{
				POSITION PrePos = ps;
				CString strPath = m_TestPathList.GetNext(ps);
				if(strPath.Find(OriginPath)==-1)
				{
					CString strName = strPath.Mid(strPath.ReverseFind('\\')+1);
					if(strName.CompareNoCase(strTestName)==0)
					{
						strPathList.AddTail(strPath);
						posPathList.AddTail(PrePos);
					}
				}
			}

			// Data PC로 가져온다.
			if(!strPathList.IsEmpty())
			{
				((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("Bring Data:"+strTestName);
				if(BringTestDataIntoDataPC(strTestName,&strPathList))
				{
					// ListCtrl 상에서 상태를 바꾸고
					m_TestList.EnsureVisible(nItem, FALSE);
					LVITEM lvitem;
					lvitem.mask     = LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
					lvitem.iItem    = nItem;
					lvitem.iSubItem = 0;
					lvitem.iImage   = 0;
					lvitem.lParam   = nItem;
					lvitem.pszText  = (LPTSTR)(LPCTSTR) strTestName;
					m_TestList.SetItem(&lvitem);

					// Path List에서 Path를 바꾼다.
					POSITION TempPos = posPathList.GetHeadPosition();
					m_TestPathList.GetAt(posPathList.GetNext(TempPos)).Format("%s\\data\\%s", OriginPath, strTestName);
					while(TempPos)
					{
						m_TestPathList.RemoveAt(posPathList.GetNext(TempPos));
					}
				}
			}
		}
		//
		nPos++;
		ProgressCtrl.SetPos(nPos);
		m_TestList.Invalidate();
	}

	// 상태바에서 Progress 지움
	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
	ProgressCtrl.DestroyWindow();
}

void CSelTestDialog::OnDataDelete() 
{
	//if( IDYES != MessageBox("선택된 데이터를 모두 삭제 하시겠습니까?", "삭제 확인", MB_ICONQUESTION | MB_YESNO)) return;
	if( IDYES != MessageBox(Fun_FindMsg("SelTestDialog_OnDataDelete_msg1","IDD_TEST_LIST_DLG"), Fun_FindMsg("SelTestDialog_OnDataDelete_msg2","IDD_TEST_LIST_DLG"), MB_ICONQUESTION | MB_YESNO)) return;//&&

	CWaitCursor cursor;


	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString OriginPath(szBuff);
	OriginPath = OriginPath.Left(OriginPath.ReverseFind('\\'));
	OriginPath.MakeLower();


//	CCTSAnalApp *pApp = (CCTSAnalApp*)AfxGetApp();
//	CString OriginPath(pApp->m_szCurrentDirectory);
//	OriginPath = OriginPath.Left(OriginPath.ReverseFind('\\'));
//	OriginPath.MakeLower();

	// StatusBar에 Progress 준비
	CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
	CProgressCtrl ProgressCtrl;
	CRect rect;
	pStatusBar->GetItemRect(0,rect);
	rect.left = rect.Width()/2;
	ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
	ProgressCtrl.SetRange(0,m_TestList.GetSelectedCount());

	// 선택한 Item을
	int nPos=0;
	while(m_TestList.GetSelectedCount()>0)
	{
		// 하나씩 가져와서 그 Test이름을 얻는다.
		POSITION pos = 	m_TestList.GetFirstSelectedItemPosition();
		int nItem = m_TestList.GetNextSelectedItem(pos);
		CString strTestName = m_TestList.GetItemText(nItem,0);
		strTestName.MakeLower();

		// Test의 Data PC로 넘어온 Path를 모두 모아서
		CStringList strPathList;
		CList<POSITION, POSITION&> posPathList;
		POSITION ps = m_TestPathList.GetHeadPosition();
		while(ps)
		{
			POSITION PrePos = ps;
			CString strPath = m_TestPathList.GetNext(ps);
			if((m_RunningTestNameList.Find(strTestName)==NULL)&&(strPath.Find(OriginPath)!=-1))
			{
				CString strName = strPath.Mid(strPath.ReverseFind('\\')+1);
				if(strName.CompareNoCase(strTestName)==0)
				{
					strPathList.AddTail(strPath);
					posPathList.AddTail(PrePos);
				}
			}
		}

		// 삭제한다.
		if(!strPathList.IsEmpty())
		{
			BOOL bFail = FALSE;
			POSITION ps = strPathList.GetHeadPosition();
			while(ps)
			{
				CString strSourcePath = strPathList.GetNext(ps);
				//
				char *pFrom = new char[strSourcePath.GetLength()+2];
				for(int j=0; j<strSourcePath.GetLength(); j++) pFrom[j]=strSourcePath.GetAt(j);
				pFrom[strSourcePath.GetLength()]='\0'; pFrom[strSourcePath.GetLength()+1]='\0';

				//
				SHFILEOPSTRUCT FileOp;
				memset(&FileOp,0,sizeof(FileOp));
				FileOp.wFunc=FO_DELETE;
				FileOp.pFrom=pFrom;
				FileOp.fFlags = FOF_NOCONFIRMATION;
				((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("Delete Data:"+strTestName);
				if(0!=SHFileOperation(&FileOp)) bFail = TRUE;
				//
				delete [] pFrom;
				//
				if(bFail) break;
			}
			//
			if(!bFail)
			{
				m_TestList.DeleteItem(nItem);
				POSITION ps = posPathList.GetHeadPosition();
				while(ps)
				{
					m_TestPathList.RemoveAt(posPathList.GetNext(ps));
				}
				for (int i=0;i < m_TestList.GetItemCount();i++)
				{
				   m_TestList.SetItemData(i, i);
				}
			}
			else m_TestList.SetItemState(nItem,~LVIS_SELECTED,LVIS_SELECTED);
		}
		else m_TestList.SetItemState(nItem,~LVIS_SELECTED,LVIS_SELECTED);
		//
		nPos++;
		ProgressCtrl.SetPos(nPos);
		m_TestList.Invalidate();
	}

	// 상태바에서 Progress 지움
	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
	ProgressCtrl.DestroyWindow();
}

void CSelTestDialog::OnDataMove() 
{
	CString strDestinationPath;
	//
	char buffer[_MAX_PATH] = {'\0'};
	BROWSEINFO browseInfo;
	browseInfo.hwndOwner = m_hWnd;
	browseInfo.pidlRoot  = NULL;
	//browseInfo.lpszTitle = "시험결과 데이터를 이동할 폴더를 선택해 주십시오";
	//browseInfo.lpszTitle = "Please select the folder for moving the test result";
	browseInfo.lpszTitle = Fun_FindMsg("SelTestDialog_OnDataMove_msg1","IDD_TEST_LIST_DLG");//&&
	browseInfo.pszDisplayName = buffer;
	browseInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	browseInfo.lpfn=NULL;
	browseInfo.lParam=0;
	LPITEMIDLIST pDirList = SHBrowseForFolder(&browseInfo);
	if(pDirList!=NULL)
	{
		SHGetPathFromIDList(pDirList, buffer);
		strDestinationPath = CString(buffer);
		strDestinationPath.MakeLower();
		LPMALLOC lpMalloc;
		VERIFY(::SHGetMalloc(&lpMalloc) == NOERROR);
		lpMalloc->Free(pDirList);
		lpMalloc->Release();
	}
	else return;

	//
	CWaitCursor cursor;


	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString OriginPath(szBuff);
	OriginPath = OriginPath.Left(OriginPath.ReverseFind('\\'));
	OriginPath.MakeLower();

//	CCTSAnalApp *pApp = (CCTSAnalApp*)AfxGetApp();
//	CString OriginPath(pApp->m_szCurrentDirectory);
//	OriginPath = OriginPath.Left(OriginPath.ReverseFind('\\'));
	OriginPath.MakeLower();

	// StatusBar에 Progress 준비
	CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
	CProgressCtrl ProgressCtrl;
	CRect rect;
	pStatusBar->GetItemRect(0,rect);
	rect.left = rect.Width()/2;
	ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
	ProgressCtrl.SetRange(0,m_TestList.GetSelectedCount());

	// 선택한 Item을
	int nPos=0;
	while(m_TestList.GetSelectedCount()>0)
	{
		// 하나씩 가져와서 그 Test이름을 얻는다.
		POSITION pos = 	m_TestList.GetFirstSelectedItemPosition();
		int nItem = m_TestList.GetNextSelectedItem(pos);
		CString strTestName = m_TestList.GetItemText(nItem,0);
		strTestName.MakeLower();

		// Test의 Data PC로 넘어온 Path를 모두 모아서
		CStringList strPathList;
		CList<POSITION, POSITION&> posPathList;
		POSITION ps = m_TestPathList.GetHeadPosition();
		while(ps)
		{
			POSITION PrePos = ps;
			CString strPath = m_TestPathList.GetNext(ps);
			if((m_RunningTestNameList.Find(strTestName)==NULL)&&(strPath.Find(OriginPath)!=-1))
			{
				CString strName = strPath.Mid(strPath.ReverseFind('\\')+1);
				if(strName.CompareNoCase(strTestName)==0)
				{
					strPathList.AddTail(strPath);
					posPathList.AddTail(PrePos);
				}
			}
		}

		// 이동한다.
		if(!strPathList.IsEmpty())
		{
			BOOL bFail = FALSE;
			POSITION ps = strPathList.GetHeadPosition();
			while(ps)
			{
				CString strSourcePath = strPathList.GetNext(ps);
				//
				char *pFrom = new char[strSourcePath.GetLength()+2];
				for(int j=0; j<strSourcePath.GetLength(); j++) pFrom[j]=strSourcePath.GetAt(j);
				pFrom[strSourcePath.GetLength()]='\0'; pFrom[strSourcePath.GetLength()+1]='\0';

				//
				char *pTo   = new char[strDestinationPath.GetLength()+2];
				for(int j=0; j<strDestinationPath.GetLength(); j++) pTo[j]=strDestinationPath.GetAt(j);
				pTo[strDestinationPath.GetLength()]='\0'; pTo[strDestinationPath.GetLength()+1]='\0';

				//
				SHFILEOPSTRUCT FileOp;
				memset(&FileOp,0,sizeof(FileOp));
				FileOp.wFunc=FO_MOVE;
				FileOp.pFrom=pFrom;
				FileOp.pTo=pTo;
				((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("Move Data:"+strTestName);
				if(0 != SHFileOperation(&FileOp)) bFail=TRUE;

				//
				delete [] pFrom;
				delete [] pTo;

				//
				if(bFail) break;
			}
			//
			if(!bFail)
			{
				m_TestList.DeleteItem(nItem);
				POSITION ps = posPathList.GetHeadPosition();
				while(ps)
				{
					m_TestPathList.RemoveAt(posPathList.GetNext(ps));
				}
				//
				for (int i=0;i < m_TestList.GetItemCount();i++)
				{
				   m_TestList.SetItemData(i, i);
				}
			}
			else m_TestList.SetItemState(nItem,~LVIS_SELECTED,LVIS_SELECTED);
		}
		else m_TestList.SetItemState(nItem,~LVIS_SELECTED,LVIS_SELECTED);
		//
		nPos++;
		ProgressCtrl.SetPos(nPos);
		m_TestList.Invalidate();
	}

	// 상태바에서 Progress 지움
	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
	ProgressCtrl.DestroyWindow();
}

void CSelTestDialog::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	//
	if(pWnd->IsKindOf(RUNTIME_CLASS(CListCtrl))&&m_byMode==MODE_MANAGEMENT)
	{
		CMenu aMenu;
		aMenu.LoadMenu(
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDR_POPUP_MENU):
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDR_POPUP_MENU_ENG):
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDR_POPUP_MENU_PL):
			IDR_POPUP_MENU);
			aMenu.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this);
	}
}

BOOL CSelTestDialog::BringTestDataIntoDataPC(CString& strTestName, CStringList* pList)
{
	//
	// "...\\data\\Test이름" 폴더로 데이터를 이동하게 된다.
	//
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString path(szBuff);
	CString strDestinationPath = path.Left(path.ReverseFind('\\'))+"\\data\\"+strTestName;

//	CCTSAnalApp* pApp = (CCTSAnalApp *)AfxGetApp();
//	CString path(pApp->m_szCurrentDirectory);
//	CString strDestinationPath = path + "\\" + strTestName;
	//
	// 혹 "...\\data" 폴더에 Test이름의 폴더가 이미 존재 하는 경우
	// 데이터를 이동할 폴더의 이름은 위의 이름에 "(0)"/"(1)"... 등을
	// 붙여준다.
	//

	CFileFind afinder;
	if ( !afinder.FindFile(strDestinationPath) )
	{
		if(!CreateDirectory(strDestinationPath,NULL)) return FALSE;
	}
	strTestName = strDestinationPath.Mid(strDestinationPath.ReverseFind('\\')+1);
/*
	int iTemp = 0;
	CFileFind afinder;
	while(afinder.FindFile(strDestinationPath))
	{
		strDestinationPath.Format("%s\\data\\%s(%d)", path.Left(path.ReverseFind('\\')), strTestName, iTemp);
		iTemp++;
	}

	strTestName = strDestinationPath.Mid(strDestinationPath.ReverseFind('\\')+1);

	//
	// 데이터를 이동할 폴더를 Create한다.
	//

	if(!CreateDirectory(strDestinationPath,NULL)) return FALSE;
*/
	//
	// 각 Unit에서 데이터가 있으면 가져온다.
	//

	afinder.Close();

	BOOL bFail = FALSE;
	POSITION ps = pList->GetHeadPosition();

	//while(ps)
	if(ps)
	{
		CString strSourcePath = pList->GetNext(ps);
		strSourcePath+="\\*.*";

		//
		char *pFrom = new char[strSourcePath.GetLength()+2];
		for(int j=0; j<strSourcePath.GetLength(); j++) pFrom[j]=strSourcePath.GetAt(j);
		pFrom[strSourcePath.GetLength()]='\0'; pFrom[strSourcePath.GetLength()+1]='\0';

		//
		char *pTo   = new char[strDestinationPath.GetLength()+2];
		for(int j=0; j<strDestinationPath.GetLength(); j++) pTo[j]=strDestinationPath.GetAt(j);
		pTo[strDestinationPath.GetLength()]='\0'; pTo[strDestinationPath.GetLength()+1]='\0';

		//
		SHFILEOPSTRUCT FileOp;
		memset(&FileOp,0,sizeof(FileOp));
		
		FileOp.fFlags = FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS;
		
		FileOp.wFunc=FO_MOVE;
		FileOp.pFrom=pFrom;
		FileOp.pTo=pTo;
		if(0 != SHFileOperation(&FileOp)) bFail=TRUE;

		if(!bFail)
		{
			memset(&FileOp,0,sizeof(FileOp));
			FileOp.wFunc=FO_DELETE;
			pFrom[strSourcePath.GetLength()-4]='\0'; pFrom[strSourcePath.GetLength()-3]='\0';
			FileOp.pFrom=pFrom;
			FileOp.fFlags = FOF_NOCONFIRMATION;
			SHFileOperation(&FileOp);
		}

		//
		delete [] pFrom;
		delete [] pTo;

		//
//		if(bFail) break;
	}

	if(bFail) return FALSE;

	//
	return TRUE;
}


void CSelTestDialog::OnTimer(UINT_PTR nIDEvent) 
{
	if(nIDEvent==AUTO_LOAD_TIMER)
	{
		KillTimer(nIDEvent);
		OnDataBring();
		CDialog::OnCancel();
	}
	CDialog::OnTimer(nIDEvent);
}


void CSelTestDialog::OnDataDestroy() 
{
	OnDataDelete();	
}