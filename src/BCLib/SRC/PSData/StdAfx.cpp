// stdafx.cpp : source file that includes just the standard includes
//	Ba_Data.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"


CString g_strpathIni;
int gi_Language;


DWORD ReadDwordPSDataReg(CString ValueKey, DWORD dwDefault) 
{ 
	DWORD dwValue = (DWORD)dwDefault; 
	HKEY hSubKey = NULL; 
	// 키 열기 
	if ( ::RegOpenKeyEx( HKEY_CURRENT_USER, "Software\\PNE CTSPack\\CTSMonPro\\Config", 0, KEY_READ, &hSubKey ) == ERROR_SUCCESS ) 
	{ 
		DWORD buf_size = sizeof(DWORD);	
		// DWORD 값 읽어오기 
		if ( ::RegQueryValueEx( hSubKey, ValueKey, NULL, NULL, (LPBYTE)&dwValue, &buf_size ) == ERROR_SUCCESS ) 
		{ 
			ASSERT( buf_size == sizeof(DWORD) ); 
		} 
		// 키 닫기 
		::RegCloseKey( hSubKey ); 
	} 

	return dwValue; 
}

void init_Language() 
{
	gi_Language=0;
	g_strpathIni = "";
	CString strDefault, getPath;
	strDefault.Format("Don't Read");
	gi_Language = ReadDwordPSDataReg("Language", 1);
	char path[MAX_PATH];
	GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	getPath=(LPSTR)path;
	int i = getPath.ReverseFind('\\');
	getPath = getPath.Left(i);
	getPath.Replace("\\","\\\\");

	switch(gi_Language)	
	{
	case 1	:	{

		g_strpathIni.Format("%s\\Language_PackCycler\\PSData\\Korean.ini", getPath);
				}break;

	case 2	:	{
		g_strpathIni.Format("%s\\Language_PackCycler\\PSData\\English.ini", getPath);
				}break;

	case 3	:	{
		g_strpathIni.Format("%s\\Language_PackCycler\\PSData\\Polish.ini", getPath);
				}break;
	default:
		g_strpathIni.Format("%s\\Language_PackCycler\\PSData\\Korean.ini", getPath);
		break;
	}
}

CString Fun_FindMsg(CString strMsg, CString strFindMsg)
{
	CString strDefault;
	CString strReturn;
	strDefault.Format("Don't Read");
	strReturn = GIni::ini_GetFile(g_strpathIni,_T(strFindMsg),_T(strMsg),strDefault);
	if((strReturn.IsEmpty() == FALSE)||(strReturn != _T(""))) //yulee 20190610_1
		return strReturn;
	else
		return strDefault;
}