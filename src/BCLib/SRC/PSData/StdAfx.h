// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__26B8E346_D902_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_STDAFX_H__26B8E346_D902_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT


#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#include <afxtempl.h>
#endif // _AFX_NO_AFXCMN_SUPPORT

#pragma warning(disable:4146)//unary minus operator applied to unsigned type

#include <float.h>
#include "../../Include/PSCommonAll.h"

#ifdef _DEBUG
#pragma comment(lib, "../../../../lib/PSCommonD.lib ")
#pragma message("Automatically linking with PSCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../../lib/PSCommon.lib")
#pragma message("Automatically linking with PSCommon.lib By K.B.H")
#endif	//_DEBUG

#include "./Global/IniParse/IniParser.h"

extern CString g_strpathIni;
extern int gi_Language;
CString Fun_FindMsg(CString strMsg, CString strFindMsg);
void init_Language();

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#ifdef TRACE
#undef TRACE
#define TRACE
#endif

#endif // !defined(AFX_STDAFX_H__26B8E346_D902_11D4_88DF_006008CEDA07__INCLUDED_)


