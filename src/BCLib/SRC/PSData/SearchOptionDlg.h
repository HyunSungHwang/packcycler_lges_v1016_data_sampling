#if !defined(AFX_SEARCHOPTIONDLG_H__0FAC107D_029A_4065_A118_25387B1E8B5F__INCLUDED_)
#define AFX_SEARCHOPTIONDLG_H__0FAC107D_029A_4065_A118_25387B1E8B5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SearchOptionDlg.h : header file
//

#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// CSearchOptionDlg dialog

class CSearchOptionDlg : public CDialog
{
// Construction
public:
	CSearchOptionDlg(CWnd* pParent = NULL);   // standard constructor
	int m_nFindTestNum;
	CString m_strSearchKey;
	CStringList m_strlistFindTestList;
// Dialog Data
	//{{AFX_DATA(CSearchOptionDlg)
	enum { IDD = IDD_SEARCH_DLG , IDD2 = IDD_SEARCH_DLG_ENG};
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchOptionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSearchOptionDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHOPTIONDLG_H__0FAC107D_029A_4065_A118_25387B1E8B5F__INCLUDED_)
