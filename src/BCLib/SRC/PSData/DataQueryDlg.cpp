// DataQueryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DataQueryDlg.h"
#include "Data.h"
#include "SelTestDlg.h"
#include "FolderDialog.h"
#include "ResultTestSelDlg.h"
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Begin ////////////
#define MAX_CYCLE_NUM	2147483647	//
// End ///////////////////////////////

// Added by 224 (2013/12/24) : utility
CString AddComma(int nNum)
{
	CString strTmp ;
	strTmp.Format("%d", nNum) ;
	
	int Count = 1 ;
	for (int i=strTmp.GetLength() - 1; i >= 0; --i, ++Count)
	{ 
		if ((Count % 3 == 0) && (i != 0))  // 3자리 마다 콤마를 삽입하되, 맨 앞자리 에는 삽입 안 함...
		{
			strTmp.Insert(i, ',');  // 주어진 인덱스에 콤마 삽입
		}
	}

	return strTmp ;
}

/////////////////////////////////////////////////////////////////////////////
// CDataQueryDialog dialog


CDataQueryDialog::CDataQueryDialog(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CDataQueryDialog::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CDataQueryDialog::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CDataQueryDialog::IDD3):
	CDataQueryDialog::IDD, pParent)
	, m_chkDrawPauseData(FALSE) //yulee 20190814
{
	//{{AFX_DATA_INIT(CDataQueryDialog)
	m_lFromCycle = 1;
	m_lIntervalCycle = 1;
	m_bCycleStringCheck = FALSE;
	m_strCycle = _T("");
	m_lToCycle = 1;//MAX_CYCLE_NUM;		//
	m_iMode = 0x00;
	m_iStartType = 0;
	m_chkCharge = TRUE;
	m_chkDisCharge = TRUE;
	m_chkRest = TRUE;
	m_chkPattern = TRUE;
	m_chkImpedance = TRUE;
	m_chkExtCAN = FALSE;
	//}}AFX_DATA_INIT

	m_lFromTable = 1 ;
	m_lToTable   = 1 ;
	
	m_pDatabase          = NULL;
	m_pbyXAxisIndex      = NULL;
	m_byVoltagePoint     = 0x00;
	m_byTemperaturePoint = 0x00;
	IsClickedAuxItem = TRUE;
	//bAuxListFlag = FALSE;
	bCanListFlag = FALSE;
	m_pstrPathList = NULL;
	m_bQueryMode = -1;	//ljb 201132 이재복 //////////	Release 모드에서 CAN 그래프 안 그려지던 문제 해결

	m_bBtnCANSetA = FALSE; //yulee 20180902
	m_bBtnCANSetB = FALSE;
	m_bBtnCANSetC = FALSE;
	m_bBtnCANSetD = FALSE; //yulee 20181008

	for(int i =0; i< 256; i++)
		m_bCheck[i] = 0;

	m_nSelectedItemNum = 0;

}


void CDataQueryDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataQueryDialog)
	DDX_Control(pDX, IDC_CAN_SELECT_SET_LIST_D, m_ctrlCanListSetD);
	DDX_Control(pDX, IDC_CAN_SELECT_SET_LIST_C, m_ctrlCanListSetC);
	DDX_Control(pDX, IDC_CAN_SELECT_SET_LIST_B, m_ctrlCanListSetB);
	DDX_Control(pDX, IDC_CAN_SELECT_SET_LIST_A, m_ctrlCanListSetA);
	DDX_Control(pDX, IDC_CAN_SELECT_LIST, m_ctrlCanList);
	DDX_Control(pDX, IDC_SEL_STEP_LIST, m_SelStepList);
	DDX_Control(pDX, IDC_Y_AXIS_CTRL_LIST, m_ctrlYAxisList);
	DDX_Control(pDX, IDC_AUX_SELECT_LIST, m_ctrlAuxList);
	DDX_Control(pDX, IDC_TEST_NAME_LIST, m_TestNameListCtrl);
	DDX_Control(pDX, IDC_Y_AXIS_LIST, m_YAxisList);
	DDX_Control(pDX, IDC_Y_UNIT_COMBO, m_YUnitCombo);
	DDX_Control(pDX, IDC_X_UNIT_COMBO, m_XUnitCombo);
	DDX_Control(pDX, IDC_X_AXIS_COMBO, m_XAxisCombo);
	DDX_Control(pDX, IDC_SEL_CYCLE_LIST, m_SelCycleList);
	DDX_Control(pDX, IDC_CHANNEL_LIST, m_ChannelListCtrl);
	DDX_Text(pDX, IDC_CYCLE_FROM_EDIT, m_lFromCycle);
	DDV_MinMaxLong(pDX, m_lFromCycle, 1, 2147483647);
	DDX_Text(pDX, IDC_CYCLE_INTERVAL_EDIT, m_lIntervalCycle);
	DDV_MinMaxLong(pDX, m_lIntervalCycle, 1, 2147483647);
	DDX_Check(pDX, IDC_CYCLE_STRING_CHECK, m_bCycleStringCheck);
	DDX_Text(pDX, IDC_CYCLE_STRING_EDIT, m_strCycle);
	DDX_Text(pDX, IDC_CYCLE_TO_EDIT, m_lToCycle);
	DDV_MinMaxLong(pDX, m_lToCycle, 1, 2147483647);
	DDX_Radio(pDX, IDC_START_TYPE_RADIO, m_iStartType);
	DDX_Check(pDX, IDC_CHARGE_CHECK, m_chkCharge);
	DDX_Check(pDX, IDC_DISCHARGE_CHECK, m_chkDisCharge);
	DDX_Check(pDX, IDC_REST_CHECK, m_chkRest);
	DDX_Check(pDX, IDC_PATTERN_CHECK, m_chkPattern);
	DDX_Check(pDX, IDC_IMP_CHECK, m_chkImpedance);
	DDX_Check(pDX, IDC_EXT_CAN_CHECK, m_chkExtCAN);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate)
	{
		///////////
		// Check //
		///////////

		if(m_TestNameListCtrl.GetItemCount()<=0)
		{//$1013
			MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg16","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg17","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		if(m_ChannelListCtrl.GetSelectedCount()<=0)
		{
			MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg18","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg19","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);
			pDX->Fail();
		}

		CList<LONG, LONG&> CycleList;
		POSITION pos = m_strlistSelCycle.GetHeadPosition();
		while(pos)
		{
			//
			CString strCycle = m_strlistSelCycle.GetNext(pos);
			long from=0L, to=0L, interval=0L;
			int p1=0, p2=0;
			p2=strCycle.Find('~',p1);
			from =      atoi(strCycle.Mid(p1,p2-p1));
			p1=p2+1;
			p2=strCycle.Find(':',p1);
			to   =     atoi(strCycle.Mid(p1,p2-p1));
			interval = atoi(strCycle.Mid(p2+1));
			//
			for(long cyc=from; cyc<=to; cyc+=interval)
			{
				// Commented by 224 (2014/01/23) : 속도 개선을 위해 주석처리
				// 바로 위에서 선언을 했으므로 자료는 없는 것으로 한다.
				// 많은 량의 데이터를 Find하고 Add 시에 부하가 많다.
//				if(CycleList.Find(cyc)==NULL)
					CycleList.AddTail(cyc);
			}
		}
		if(m_bCycleStringCheck)
		{
			int p1=0, p2=0, p3=0;
			while(TRUE && !m_strCycle.IsEmpty())
			{
				p2=m_strCycle.Find(',',p1);
				p3=m_strCycle.Find('-',p1);
				if     (p2!=-1&&p3==-1)
				{
					long cyc = atoi(m_strCycle.Mid(p1,p2-p1));
					if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
				}
				else if(p2!=-1&&p3!=-1)
				{
					if(p3<p2)
					{
						long from = atoi(m_strCycle.Mid(p1, p3-p1));
						long to   = atoi(m_strCycle.Mid(p3+1, p2-p3-1));
						for(long cyc=from; cyc<=to; cyc++)
						{
							if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
						}
					}
				}
				else if(p2==-1&&p3==-1)
				{
					CString str = m_strCycle.Mid(p1);
					if(!str.IsEmpty())
					{
						long cyc = atoi(str);
						if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
					}
					break;
				}
				else if(p2==-1&&p3!=-1)
				{
					long from = atoi(m_strCycle.Mid(p1, p3-p1));
					long to   = atoi(m_strCycle.Mid(p3+1));
					for(long cyc=from; cyc<=to; cyc++)
					{
						if(CycleList.Find(cyc)==NULL) CycleList.AddTail(cyc);
					}
					break;
				}
			
				p1=p2+1;
			}
		}
		if(CycleList.IsEmpty())
		{//$1013
			//MessageBox("Cycle 구간을 선택해 주십시오.", "Cycle 구간 선택", MB_ICONEXCLAMATION);
			MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg1","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg2","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
			pDX->Fail();
		}

		if(m_XAxisCombo.GetCurSel()==CB_ERR)
		{
			//MessageBox("X-축에 표현할 아이템을 선택해 주십시오", "X-축 아이템 선택", MB_ICONEXCLAMATION);
			MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg3","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg4","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
			pDX->Fail();
		}

		int j;
		BOOL bCheck = FALSE;
		for(int i=0; i<m_ctrlYAxisList.GetItemCount(); i++)
		{
			if(m_ctrlYAxisList.GetCheck(i)==1)
			{
				CString strName = m_ctrlYAxisList.GetItemText(i, 0);
				BOOL bAuxCheck = FALSE;
				BOOL bCanCheck = FALSE;		//ljb 2011223 이재복 //////////

				//if(strName == RS_COL_AUX_TEMP || strName == RS_COL_AUX_VOLT || strName == RS_COL_AUX_TEMPTH) //20180619 yulee
				if(strName == RS_COL_AUX_TEMP || strName == RS_COL_AUX_VOLT
					|| strName == RS_COL_AUX_TEMPTH || strName == RS_COL_AUX_HUMI) //ksj 20200116 : v1016 습도 추가
				{
					for(int j = 0 ; j < m_ctrlAuxList.GetCount(); j++)
					{
						if(m_ctrlAuxList.GetCheck(j) == 1)
						{
							bAuxCheck = TRUE;
							break;
						}
					}
					if(!bAuxCheck)
					{
						//MessageBox("외부데이터를  선택해 주십시오", "외부데이터 선택", MB_ICONEXCLAMATION);
						MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg5","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg6","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
						pDX->Fail();
					}
				}
				
				//ljb 2011223 이재복  S /////////////////////////////////////////////
				if(strName == RS_COL_CAN)
				{
					for(j = 0 ; j < m_ctrlCanList.GetCount(); j++)
					{
						if(m_ctrlCanList.GetCheck(j) == 1)
						{
							bCanCheck = TRUE;
							break;
						}
					}
					if(!bCanCheck)
					{//$1013
						//MessageBox("CAN 데이터를  선택해 주십시오", "CAN 데이터 선택", MB_ICONEXCLAMATION);
						MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg7","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg8","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
						pDX->Fail();
					}
				}
				else if(strName == RS_COL_CAN_S1)
				{
					for(j = 0 ; j < m_ctrlCanList.GetCount(); j++)
					{
						if(m_ctrlCanList.GetCheck(j) == 1)
						{
							bCanCheck = TRUE;
							break;
						}
					}
					if(!bCanCheck)
					{
						//MessageBox("CAN 데이터를  선택해 주십시오", "CAN 데이터 선택", MB_ICONEXCLAMATION);
						MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg7","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg8","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
						pDX->Fail();
					}
				}
				else if(strName == RS_COL_CAN_S2)
				{
					for(j = 0 ; j < m_ctrlCanList.GetCount(); j++)
					{
						if(m_ctrlCanList.GetCheck(j) == 1)
						{
							bCanCheck = TRUE;
							break;
						}
					}
					if(!bCanCheck)
					{
						//MessageBox("CAN 데이터를  선택해 주십시오", "CAN 데이터 선택", MB_ICONEXCLAMATION);
						MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg7","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg8","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
						pDX->Fail();
					}
				}
				else if(strName == RS_COL_CAN_S3)
				{
					for(j = 0 ; j < m_ctrlCanList.GetCount(); j++)
					{
						if(m_ctrlCanList.GetCheck(j) == 1)
						{
							bCanCheck = TRUE;
							break;
						}
					}
					if(!bCanCheck)
					{
						//MessageBox("CAN 데이터를  선택해 주십시오", "CAN 데이터 선택", MB_ICONEXCLAMATION);
						MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg7","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg8","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
						pDX->Fail();
					}
				}
				//////////////////////////////////////////////////////////////////////////
				
				bCheck = TRUE;
				break;
			}
		}
		if(!bCheck)
		{
			//MessageBox("Y-축에 표현할 아이템을 선택해 주십시오", "Y-축 아이템 선택", MB_ICONEXCLAMATION);
			MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg9","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg10","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
			pDX->Fail();
		}

		//20100430 step select
		CList<LONG, LONG&> StepList;
		LONG selStep=0;
		int nCount = m_SelStepList.GetSelCount();
		CArray<int,int> aryListBoxSel;
		aryListBoxSel.SetSize(nCount);
		m_SelStepList.GetSelItems(nCount, aryListBoxSel.GetData()); 
		for(int s = 0; s<nCount; s++)
		{
			selStep = m_SelStepList.GetItemData(aryListBoxSel[s]);
			StepList.AddTail(selStep);	
		}
		/*if(StepList.IsEmpty())
		{
			//MessageBox("선택된 Step 번호가 없습니다.", "Step 번호 선택", MB_ICONEXCLAMATION);
			MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg11"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg12"), MB_ICONEXCLAMATION);//&&
			pDX->Fail();
		}*/
// 		CString strCycle;
// 		GetDlgItem(IDC_CYCLE_STRING_EDIT2)->GetWindowText(strCycle);
		
		/////////////
		// Channel //
		/////////////
		
		m_pQueryCondition->ResetChPath();
		
		int     nItem, nIndex;
		CString strChPath;
		CString strTestName;

		pos = m_ChannelListCtrl.GetFirstSelectedItemPosition();
		while(pos)
		{
			nIndex = 0;
			nItem     = m_ChannelListCtrl.GetNextSelectedItem(pos);
			nIndex = m_ChannelListCtrl.GetItemData(nItem);
			strChPath = m_strlistTestPath.GetAt(m_strlistTestPath.FindIndex(nIndex))+"\\"+m_ChannelListCtrl.GetItemText(nItem,1);
			strChPath.MakeLower();
			m_pQueryCondition->AddChPath(strChPath);
		}

		///////////
		// Cycle //
		///////////

		int NumOfCycle = CycleList.GetCount();
		long* plCycle = new long[NumOfCycle];
		int n=0;
		pos = CycleList.GetHeadPosition();
		while(pos)
		{
			plCycle[n]=CycleList.GetNext(pos);
			n++;
		}
		qsort( (void *)plCycle, (size_t)NumOfCycle, sizeof(long ), CData::LongCompare);

		BOOL bChangeCycleSelection = m_pQueryCondition->UpdateCycleList(plCycle, NumOfCycle);

		delete [] plCycle;

		//20100430 step select
		//////////
		// Step //
		//////////
		int NumOfStep = StepList.GetCount();
		long* plStep = new long[NumOfStep];
		n=0;
		pos = StepList.GetHeadPosition();
		while(pos)
		{
			plStep[n]=StepList.GetNext(pos);
			n++;
		}
		qsort( (void *)plStep, (size_t)NumOfStep, sizeof(long ), CData::LongCompare);
		
		BOOL bChangeStepSelection = m_pQueryCondition->UpdateStepNoList(plStep, NumOfStep);
		
		delete [] plStep;
		//////////////////////////////////////////////////////////////////////////

		//////////
		// Axis //
		//////////

		CString strTitle, strUnitNotation;
		float fltUnitTransfer=0.0f;
		CDaoRecordset aRecordset(m_pDatabase);
		CString strQuery;

		////////////
		// X-Axis //
		////////////

		// Title
		m_XAxisCombo.GetLBText(m_XAxisCombo.GetCurSel(), strTitle);
		// Unit Notation
		m_XUnitCombo.GetLBText(m_XUnitCombo.GetCurSel(), strUnitNotation);
		// Unit Transfer
		strQuery.Format("SELECT PropertyIndex FROM AxisInfo WHERE index = %d",
			            m_pbyXAxisIndex[m_XAxisCombo.GetCurSel()]);
		aRecordset.Open(dbOpenDynaset, strQuery);
		COleVariant var;
		aRecordset.GetFieldValue("PropertyIndex", var);
		BYTE PropertyIndex = var.bVal;
		var.Clear();
		aRecordset.Close();
		//
		strQuery.Format("SELECT UnitTransfer FROM Property WHERE index = %d",
			            PropertyIndex);
		aRecordset.Open(dbOpenDynaset, strQuery);
		aRecordset.Move(m_XUnitCombo.GetCurSel());
		aRecordset.GetFieldValue("UnitTransfer", var);
		fltUnitTransfer = var.fltVal;
		var.Clear();
		aRecordset.Close();

		//20100430
		int nModeCnt = UpdateStepTypeSelect();

		if(m_iMode == 0 )
		{
			//MessageBox("표기 Step종류가 선택되지 않았습니다.", "Step 종류 선택", MB_ICONEXCLAMATION);
			MessageBox(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg13","IDD_DATA_QUERY_DIALOG"), Fun_FindMsg("DataQueryDialog_DoDataExchange_msg14","IDD_DATA_QUERY_DIALOG"), MB_ICONEXCLAMATION);//&&
			pDX->Fail();
		}
		
// 		//X축이 Cycle 선택되었으면 Y축으로 사용되는 Step의 종류는 1개만 선택되어야 한다.
// 		if(strTitle.CompareNoCase("Cycle") == 0 && nModeCnt != 1)
// 		{
// 			MessageBox("X축이 Cycle이면 표기 Step은 1개만 선택 가능합니다.", "Step 종류 선택", MB_ICONEXCLAMATION);
// 			pDX->Fail();
// 		}

		// Query Condition의 XAxis member변수의 내용에
		// 사용자의 선택을 반영한다.
		CAxis* pXAxis = m_pQueryCondition->GetXAxis();

			//
			// 이번 User선택으로 Query Condition이 Modifying Mode로 되는지 결정.
			//
			m_pQueryCondition->SetModifyingMode(FALSE);
			if((strTitle.CompareNoCase("cycle")==0 ) && (pXAxis->GetTitle().CompareNoCase("cycle") == 0))
			{
				if(!bChangeCycleSelection) 
					m_pQueryCondition->SetModifyingMode(TRUE);
			}
			else if((strTitle.CompareNoCase("cycle") != 0 ) && (pXAxis->GetTitle().CompareNoCase("cycle") != 0))
			{
/*				if(m_iStartType == m_pQueryCondition->GetStartPointOption())
				{
					//Step selection만 변경한 경우 
					if(!bChangeCycleSelection && m_iMode != m_pQueryCondition->GetOutputOption())
					{
						m_pQueryCondition->SetModifyingMode(TRUE);
					}
				}
*/
				if((m_iStartType == 0) && (m_pQueryCondition->GetStartPointOption()==0))
				{
					if(!bChangeCycleSelection && (m_iMode == m_pQueryCondition->GetOutputOption())) 
						m_pQueryCondition->SetModifyingMode(TRUE);	
				}
				else if((m_iStartType==1)&&(m_pQueryCondition->GetStartPointOption()==1))
				{
					if(m_iMode == m_pQueryCondition->GetOutputOption()) 
						m_pQueryCondition->SetModifyingMode(TRUE);
				}
			}
	
		pXAxis->SetTitle(strTitle);
		pXAxis->SetUnitNotation(strUnitNotation);
		pXAxis->SetUnitTransfer(fltUnitTransfer);

		////////////
		// Y-Axis //
		////////////

		// Query Condition의 YAxis member변수의 내용에
		// 사용자의 선택을 반영한다.
		m_pQueryCondition->ResetYAxisList();
		m_pQueryCondition->ResetYAuxAxisList();
		m_pQueryCondition->ResetYCanAxisList();		//ljb 2011215 이재복 //////////

		for (int i = 0; i < m_ctrlYAxisList.GetItemCount(); i++)
		{
			if (m_ctrlYAxisList.GetCheck(i) == 1)
			{
				POSITION pos = m_YAxisPropertyIndex.FindIndex(i);
				CPoint pt = m_YAxisPropertyIndex.GetAt(pos);
				// Title
				//m_YAxisList.GetText(i, strTitle);
				strTitle = m_ctrlYAxisList.GetItemText(i, 0);

				// UnitNotation, UnitTransfer
				strQuery.Format("SELECT UnitNotation, UnitTransfer FROM Property WHERE index = %d",
					            pt.x);
				aRecordset.Open(dbOpenDynaset, strQuery);
				aRecordset.Move(pt.y);
				aRecordset.GetFieldValue("UnitNotation", var);
				strUnitNotation = (LPCTSTR)(LPTSTR)var.bstrVal;
				var.Clear();
				aRecordset.GetFieldValue("UnitTransfer", var);
				fltUnitTransfer = var.fltVal;
				var.Clear();
				aRecordset.Close();
//**********************************************
				//Aux가 체크되면 선택된 작업결과 DATA에 포함된 해당 Aux를 모두 화면에 그려준다.////
				CString strItem;
				CString strAux;
				int i;
				BOOL bDrawAux(FALSE);
				if(strTitle == RS_COL_AUX_VOLT)
				{
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{
						m_ctrlAuxList.GetText(i, strItem);
						if (m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,4)== strTitle)
						{
							bDrawAux = TRUE;
							break;
						}
					}
					if (bDrawAux) 
					{
						CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
						m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add  AuxV 축 추가
					}
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{
						m_ctrlAuxList.GetText(i, strItem);
						if(m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,4) == strTitle)
						{							
							m_ctrlAuxList.GetText(i, strAux);
							CAxis* pYAxis = new CAxis(strAux, strUnitNotation, fltUnitTransfer);
							
							m_pQueryCondition->AddYAuxAxisVoltList(pYAxis);			//ljb 20131204 Y축 하나에 AuxV 추가 함.
							//m_pQueryCondition->AddYAxisList(pYAxis);			//20130215 add
						}						
					}					
					continue;
				}
				
				if(strTitle == RS_COL_AUX_TEMP)
				{
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{
						m_ctrlAuxList.GetText(i, strItem);
						if (m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,4) == strTitle && strItem.Mid(5,5).CompareNoCase(RS_COL_AUX_TEMPTH) != 0)
						{
							bDrawAux = TRUE;
							break;
						}
					}
					if (bDrawAux) 
					{
						CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
						m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add  AuxV 축 추가
					}					
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{				
						m_ctrlAuxList.GetText(i, strItem);
						if(m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,4) == strTitle && strItem.Mid(5,5).CompareNoCase(RS_COL_AUX_TEMPTH) != 0) //180620 yulee
						{							
							m_ctrlAuxList.GetText(i, strAux);
							//ljb 2011222 이재복 AUX 단위 //////////
							fltUnitTransfer = -1;
							
							CAxis* pYAxis = new CAxis(strAux, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAuxAxisTempList(pYAxis);			//ljb 20131204 Y축 하나에 AuxT 추가 함.
							//m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add
						}						
					}					
					continue;
				}

				if(strTitle == RS_COL_AUX_TEMPTH)
				{
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{
						m_ctrlAuxList.GetText(i, strItem);
						if (m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,5) == strTitle && strItem.Mid(5,5).CompareNoCase(RS_COL_AUX_TEMPTH) == 0)
						{
							bDrawAux = TRUE;
							break;
						}
					}
					if (bDrawAux) 
					{
						CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
						m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add  AuxV 축 추가
					}					
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{				
						m_ctrlAuxList.GetText(i, strItem);
						if(m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,5) == strTitle && strItem.Mid(5,5).CompareNoCase(RS_COL_AUX_TEMPTH) == 0)
						{							
							m_ctrlAuxList.GetText(i, strAux);
							//ljb 2011222 이재복 AUX 단위 //////////
							//fltUnitTransfer = -1;
							
							CAxis* pYAxis = new CAxis(strAux, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAuxAxisTempTHList(pYAxis);			//20180608 yulee Y축 하나에 AuxTH 추가 함.
							//m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add
						}						
					}					
					continue;
				}
				//ksj 20200116 : v1016 습도 추가.
				if(strTitle == RS_COL_AUX_HUMI)
				{
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{
						m_ctrlAuxList.GetText(i, strItem);
						if (m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,4) == strTitle && strItem.Mid(5,4).CompareNoCase(RS_COL_AUX_HUMI) == 0)
						{
							bDrawAux = TRUE;
							break;
						}
					}
					if (bDrawAux) 
					{
						CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
						m_pQueryCondition->AddYAxisList(pYAxis);			
					}					
					for(int i = 0; i < m_ctrlAuxList.GetCount(); i++)
					{				
						m_ctrlAuxList.GetText(i, strItem);
						if(m_ctrlAuxList.GetCheck(i) == 1 && strItem.Mid(5,4) == strTitle && strItem.Mid(5,4).CompareNoCase(RS_COL_AUX_HUMI) == 0)
						{							
							m_ctrlAuxList.GetText(i, strAux);

							CAxis* pYAxis = new CAxis(strAux, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAuxAxisHumiList(pYAxis);			
						}						
					}					
					continue;
				}
//************************************				
				//ljb CAN 체크되면 선택된 작업결과 DATA에 포함된 해당 CAN를 모두 화면에 그려준다.////					
				if (strTitle == RS_COL_CAN)
				{


					//원래 소스 
					/*
					CString strCan;
					for(int i = 0; i < m_ctrlCanList.GetCount(); i++)
					{
						CString strItem;
						m_ctrlCanList.GetText(i, strItem);
						
						if(m_ctrlCanList.GetCheck(i) == 1 && strItem.Left(strItem.Find('[')) == strTitle)
						{							
							m_ctrlCanList.GetText(i, strCan);
							//ljb 2011222 이재복 CAN 단위 //////////
							strUnitNotation.Empty();
							fltUnitTransfer = 1;
							CAxis* pYAxis = new CAxis(strCan, strUnitNotation, fltUnitTransfer);
							//m_pQueryCondition->AddYCanAxisList(pYAxis);			//ljb 20131104 remove
							m_pQueryCondition->AddYAxisList(pYAxis);				//ljb 20131104 add
							
						}						
					}
					*/


//************************************	
					/*
					//수정 소스 //yulee 20180831-2
					CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
					m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add  AuxCan 축 추가
					CString strCan;
					for(int i = 0; i < m_ctrlCanList.GetCount(); i++)
					{				
						m_ctrlCanList.GetText(i, strItem);
						if(m_ctrlCanList.GetCheck(i) == 1 && strItem.Mid(5,3) == strTitle) //180620 yulee
						{							
							m_ctrlCanList.GetText(i, strCan);
							//ljb 2011222 이재복 AUX 단위 //////////
							fltUnitTransfer = 1;
							
							CAxis* pYAxis = new CAxis(strCan, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAuxAxisCANList(pYAxis);			//ljb 20131204 Y축 하나에 Aux 추가 함.
							//m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add
						}						
					}	

					continue;*/

							//수정 소스 //yulee 20180831-2
							 //yulee 20181009 주석 처리
					/*
							int nCanS0CheckNo=0, nIsInS123=0;
							BOOL bCanS0Skip=FALSE;
							for(int i=0; i<m_ctrlCanList.GetCount(); i++)
							{
								if(m_ctrlCanList.GetCheck(i) == TRUE)
									nCanS0CheckNo++;
							}

							for(int i = 0; i<nCanS0CheckNo; i++)
							{
								CString tmpStrItem;
								BOOL	bIsInS123=FALSE;
								m_ctrlCanList.GetText(i, tmpStrItem);
								for(int j=0; j<m_ctrlCanListSetA.GetCount(); j++)
								{
									CString tmpStrSetItem, tmpStr;
									m_ctrlCanListSetA.GetText(j, tmpStrSetItem);
									tmpStr.Format("%s", tmpStrSetItem.Mid(tmpStrSetItem.Find('s')+2, 32));
									tmpStrSetItem = tmpStrSetItem.Left(8) + tmpStr;
									if(tmpStrItem.CompareNoCase(tmpStrSetItem)==0)
									{
										nIsInS123++;
										bIsInS123 = TRUE;
										break;
									}
								}
								if(bIsInS123 == TRUE)
										continue;
								for(j=0; j<m_ctrlCanListSetB.GetCount(); j++)
								{
									CString tmpStrSetItem, tmpStr;
									m_ctrlCanListSetB.GetText(j, tmpStrSetItem);
									tmpStr.Format("%s", tmpStrSetItem.Mid(tmpStrSetItem.Find('s')+2, 32));
									tmpStrSetItem = tmpStrSetItem.Left(8) + tmpStr;
									if(tmpStrItem.CompareNoCase(tmpStrSetItem)==0)
									{
										nIsInS123++;
										bIsInS123 = TRUE;
										break;
									}
								}
								if(bIsInS123 == TRUE)
									continue;
								for(j=0; j<m_ctrlCanListSetC.GetCount(); j++)
								{
									CString tmpStrSetItem, tmpStr;
									m_ctrlCanListSetC.GetText(j, tmpStrSetItem);
									tmpStr.Format("%s", tmpStrSetItem.Mid(tmpStrSetItem.Find('s')+2, 32));
									tmpStrSetItem = tmpStrSetItem.Left(8) + tmpStr;
									if(tmpStrItem.CompareNoCase(tmpStrSetItem)==0)
									{
										nIsInS123++;
										bIsInS123 = TRUE;
										break;
									}
								}


							}
							////////////////////////////////////////////////////////////////////////
							if(nCanS0CheckNo == nIsInS123)
									bCanS0Skip = TRUE;
							if(nCanS0CheckNo == 0) 
									bCanS0Skip = TRUE;
							if(bCanS0Skip == FALSE)
							{
								strUnitNotation.Format("");
								CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);

								m_pQueryCondition->AddYAxisList(pYAxis);				//20130215 add  AuxCan 축 추가
								CString strCan;
								BOOL bSelectedItem = FALSE;
								for(int i = 0; i < m_ctrlCanList.GetCount(); i++)
								{	
									CString tmpStr, tmpStrSetABC;
									int nIndex;

									


									m_ctrlCanList.GetText(i,tmpStr);
									nIndex = (atoi(tmpStr.Mid(tmpStr.Find('[')+1,3)))-1;
									
									for(int j=0; j<m_ctrlCanListSetA.GetCount(); j++)
									{
										m_ctrlCanListSetA.GetText(j,tmpStrSetABC);
										if((atoi(tmpStrSetABC.Mid(tmpStr.Find('[')+1,3))-1) == nIndex)
											bSelectedItem = TRUE;
									}
									
									if(bSelectedItem != TRUE)
									{
										for(j=0; j<m_ctrlCanListSetB.GetCount(); j++)
										{
											m_ctrlCanListSetB.GetText(j,tmpStrSetABC);
											if((atoi(tmpStrSetABC.Mid(tmpStr.Find('[')+1,3))-1) == nIndex)
												bSelectedItem = TRUE;
										}
									}

									if(bSelectedItem != TRUE)
									{
										for(j=0; j<m_ctrlCanListSetC.GetCount(); j++)
										{
											m_ctrlCanListSetC.GetText(j,tmpStrSetABC);
											if((atoi(tmpStrSetABC.Mid(tmpStr.Find('[')+1,3))-1) == nIndex)
												bSelectedItem = TRUE;
										}
									}


									if(bSelectedItem == TRUE)
									{
										bSelectedItem = FALSE;
										continue;
									}

									m_ctrlCanList.GetText(i, strItem);
									if(m_ctrlCanList.GetCheck(i) == 1 && strItem.Mid(5,3) == strTitle) //180620 yulee
									{							
										m_ctrlCanList.GetText(i, strCan);
										//ljb 2011222 이재복 AUX 단위 //////////
										fltUnitTransfer = 1;
										CAxis* pYAxis = new CAxis(strCan, strUnitNotation, fltUnitTransfer);
										m_pQueryCondition->AddYAuxAxisCANList(pYAxis);			//ljb 20131204 Y축 하나에 Aux 추가 함.
										
									}						
								}
							}

							*/
						  //수정 소스 //yulee 20180903

						fltUnitTransfer = 1; //ksj 20201118 : CAN 단위는 1로 고정

						if(m_ctrlCanListSetA.GetCount() >0)
						{
							CString tmpSetAItemText;
							m_ctrlCanListSetA.GetText(0, tmpSetAItemText);
							strTitle = tmpSetAItemText.Mid(tmpSetAItemText.Find(']')+1,5);
							fltUnitTransfer = 1; //ksj 20200511 : 단위 초기화 추가. //ksj 20201120 : 1015 소스에서 가져와서 다시 한번 추가.
							CAxis* pYAxisS1 = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAxisList(pYAxisS1);				//20130215 add  AuxCan 축 추가

							CString strCan;
							for(int i = 0; i < m_ctrlCanListSetA.GetCount(); i++)
							{				
								m_ctrlCanListSetA.GetText(i, strCan);
								//ljb 2011222 이재복 AUX 단위 //////////
								fltUnitTransfer = 1;
								
								CAxis* pYAxisS1 = new CAxis(strCan, strUnitNotation, fltUnitTransfer);
								m_pQueryCondition->AddYAuxAxisCANS1List(pYAxisS1);			//ljb 20131204 Y축 하나에 Aux 추가 함.
							}
						}
						if(m_ctrlCanListSetB.GetCount() >0)
						{
							CString tmpSetBItemText;
							m_ctrlCanListSetB.GetText(0, tmpSetBItemText);
							strTitle = tmpSetBItemText.Mid(tmpSetBItemText.Find(']')+1,5);
							fltUnitTransfer = 1; //ksj 20200511 : 단위 초기화 추가. //ksj 20201120 : 1015 소스에서 가져와서 다시 한번 추가.
								CAxis* pYAxisS2 = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAxisList(pYAxisS2);				//20130215 add  AuxCan 축 추가
							
							CString strCan;
							for(int i = 0; i < m_ctrlCanListSetB.GetCount(); i++)
							{				
								m_ctrlCanListSetB.GetText(i, strCan);
								//ljb 2011222 이재복 AUX 단위 //////////
								fltUnitTransfer = 1;
								
								CAxis* pYAxisS2 = new CAxis(strCan, strUnitNotation, fltUnitTransfer);
								m_pQueryCondition->AddYAuxAxisCANS2List(pYAxisS2);			//ljb 20131204 Y축 하나에 Aux 추가 함.
							}
						}
						if(m_ctrlCanListSetC.GetCount() >0)
						{
							CString tmpSetCItemText;
							m_ctrlCanListSetC.GetText(0, tmpSetCItemText);
							strTitle = tmpSetCItemText.Mid(tmpSetCItemText.Find(']')+1,5);
							fltUnitTransfer = 1; //ksj 20200511 : 단위 초기화 추가. //ksj 20201120 : 1015 소스에서 가져와서 다시 한번 추가.
								CAxis* pYAxisS3 = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAxisList(pYAxisS3);				//20130215 add  AuxCan 축 추가
							
							CString strCan;
							for(int i = 0; i < m_ctrlCanListSetC.GetCount(); i++)
							{				
								m_ctrlCanListSetC.GetText(i, strCan);
								//ljb 2011222 이재복 AUX 단위 //////////
								fltUnitTransfer = 1;
								
								CAxis* pYAxisS3 = new CAxis(strCan, strUnitNotation, fltUnitTransfer);
								m_pQueryCondition->AddYAuxAxisCANS3List(pYAxisS3);			//ljb 20131204 Y축 하나에 Aux 추가 함.
					
								}
						}
						if(m_ctrlCanListSetD.GetCount() >0)//yulee 20181009
						{
							CString tmpSetCItemText;
							m_ctrlCanListSetD.GetText(0, tmpSetCItemText);
							strTitle = tmpSetCItemText.Mid(tmpSetCItemText.Find(']')+1,5);
							fltUnitTransfer = 1; //ksj 20200511 : 단위 초기화 추가. //ksj 20201120 : 1015 소스에서 가져와서 다시 한번 추가.
							CAxis* pYAxisS4 = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);
							m_pQueryCondition->AddYAxisList(pYAxisS4);				//20130215 add  AuxCan 축 추가
							
							CString strCan;
							for(int i = 0; i < m_ctrlCanListSetD.GetCount(); i++)
							{				
								m_ctrlCanListSetD.GetText(i, strCan);
								//ljb 2011222 이재복 AUX 단위 //////////
								fltUnitTransfer = 1;
								
								CAxis* pYAxisS4 = new CAxis(strCan, strUnitNotation, fltUnitTransfer);
								m_pQueryCondition->AddYAuxAxisCANS4List(pYAxisS4);			//ljb 20131204 Y축 하나에 Aux 추가 함.
								
							}
						}
					continue;
				}
				//ljb 20110201 챔버 온도 단위 UnitTransfer
				if(strTitle == RS_COL_OVEN_TEMP)
				{
					//strUnitNotation = ""
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_OVEN_HUMI)
				{
					strUnitNotation = "%";
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_CHILLER_REF_TEMP) //yulee 20190710
				{
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_CHILLER_CUR_TEMP)
				{
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_CHILLER_REF_PUMP)
				{
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_CHILLER_CUR_PUMP)
				{
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_PWRSUPPLY_SETVOLT)
				{
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_PWRSUPPLY_CURVOLT)
				{
					fltUnitTransfer = 1.0;
				}
				else if(strTitle == RS_COL_AUX_HUMI) //ksj 20200206 : v1016 습도 센서 추가
				{
					strUnitNotation = "%";
					fltUnitTransfer = 1.0;
				}


				CAxis* pYAxis = new CAxis(strTitle, strUnitNotation, fltUnitTransfer);	
				m_pQueryCondition->AddYAxisList(pYAxis);									
			}
		}

		///////////////////
		// Output Option //
		///////////////////

		m_pQueryCondition->SetOutputOption(m_iMode);
		m_pQueryCondition->SetStartPointOption(m_iStartType);
		AfxGetApp()->WriteProfileInt(QUERY_REG_SECTION, "Step Option", m_iMode);
		AfxGetApp()->WriteProfileInt(QUERY_REG_SECTION, "Start Option", m_iStartType);
		

		///////////////////////
		// Measurement Point //
		///////////////////////
		if(((CButton*)GetDlgItem(IDC_VOLTAGE1_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x01;
		else                                                          m_byVoltagePoint&=~0x01;

		if(((CButton*)GetDlgItem(IDC_VOLTAGE2_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x02;
		else                                                          m_byVoltagePoint&=~0x01;

		if(((CButton*)GetDlgItem(IDC_VOLTAGE3_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x04;
		else                                                          m_byVoltagePoint&=~0x01;
		
		if(((CButton*)GetDlgItem(IDC_VOLTAGE4_CHECK))->GetCheck()==1) m_byVoltagePoint|= 0x08;
		else                                                          m_byVoltagePoint&=~0x01;
		
		if(((CButton*)GetDlgItem(IDC_TEMPERATURE1_CHECK))->GetCheck()==1) m_byTemperaturePoint|= 0x01;
		else                                                              m_byTemperaturePoint&=~0x01;
		
		if(((CButton*)GetDlgItem(IDC_TEMPERATURE1_CHECK))->GetCheck()==1) m_byTemperaturePoint|= 0x02;
		else                                                              m_byTemperaturePoint&=~0x01;

		WriteRegUserSet();



// Added by 224 (2013/12/24) : 주어진 범위의 레코드 수를 산정하여 전체 레코드수를 보여준다.
//------------------------------------------------------------------
		// .1. 주어진 STEP END 의 수
		NumOfCycle ;

		// .2. 주어진 범위내의 STEP END 별 레코드의 수
		INT nRows = 0 ;
		INT nTimes = m_pQueryCondition->GetYAxisCount() ;

		// .3. 주어진 Y축의 수
		pos = m_pQueryCondition->GetChPathHead();
		while (pos)
		{
			CString strPath = m_pQueryCondition->GetNextChPath(pos);

			//long from = 0L, to=0L, interval = 0L;
			long from = 0, to=0, interval = 0; //ksj 20210223 : 초기 값 수정.
			POSITION sel_pos = m_strlistSelCycle.GetHeadPosition();
			while (sel_pos)
			{
				CString str = m_strlistSelCycle.GetNext(sel_pos);
				int p1 = 0 ;
				int p2 = str.Find('~',p1);
				from = atoi(str.Mid(p1, p2-p1));
				p1 = p2 + 1;
				p2 = str.Find(':', p1);
				to =     atoi(str.Mid(p1,p2-p1));
				interval = atoi(str.Mid(p2+1));
			}
			
			if(from != 0 && to != 0 && interval != 0) //ksj 20210223 : 값이 0이 아닐때만 진행하도록. 조건 추가.
			{
				if (GetQueryMode() == QUERY_MODE_INDEX)			
				{
					nRows += GetNumberOfStepEndData(strPath, from, to, QUERY_MODE_INDEX) / interval ;
				}
				else if (GetQueryMode() == QUERY_MODE_CYCLE)
				{
					nRows += GetNumberOfStepEndData(strPath, from, to, QUERY_MODE_CYCLE) / interval ;
				}
				else
				{
					ASSERT(FALSE) ;
				}
			}		

		}

		// .4. 요청한 총 레코드의 수
		CString strMaxRow ;
		GetDlgItem(IDC_EDIT_MAXROW)->GetWindowText(strMaxRow) ;

		// .4.1. 총 레코드의 수가 설정한 값보다 크면
		if (nRows > atoi(strMaxRow))
		{
			CString str ;
			/*str.Format("조회하실 자료는\n"
					   "Y축: %d개\n"
					   "레코드 수: %s 건 입니다.\n"
					   "총 조회건수는 총 %s 건으로 설정한 조회수(%s)를 초과하였습니다."
			 */
			 			 str.Format(Fun_FindMsg("DataQueryDialog_DoDataExchange_msg15","IDD_DATA_QUERY_DIALOG") //&&
					 , nTimes, AddComma(nRows), AddComma(nRows * nTimes), AddComma(atoi(strMaxRow))) ;
					 
			MessageBox(str, "Error", MB_OK|MB_ICONEXCLAMATION) ;

			pDX->Fail();

		}

//------------------------------------------------------------------

		// Data가 너무 많은 경우 경고를 띠움 
		// 선택 채널 수, cycle 갯수
		int a = m_pQueryCondition->GetChPathCount();
		int b = a * NumOfCycle;

		//memory 약 80M 사용
		if (b > AfxGetApp()->GetProfileInt(QUERY_REG_SECTION, "MaxWarningCycle", 128))
		{
			//if (IDCANCEL == MessageBox("선택한 채널과 Cycle 구간이 너무 많아 속도가 저하될 수 있습니다.\n계속 실행하시겠습니까?", "Warning", MB_OKCANCEL|MB_ICONEXCLAMATION))
			if (IDCANCEL == MessageBox("선택한 채널과 Cycle 구간이 너무 많아 속도가 저하될 수 있습니다.\n계속 실행하시겠습니까?", "Warning", MB_OKCANCEL|MB_ICONEXCLAMATION))//&&
			{
				pDX->Fail();
			}
		}

	} // if(pDX->m_bSaveAndValidate)
	DDX_Check(pDX, IDC_CHK_DRAWPAUSEDATA, m_chkDrawPauseData); //yulee 20190814
}


BEGIN_MESSAGE_MAP(CDataQueryDialog, CDialog)
	//{{AFX_MSG_MAP(CDataQueryDialog)
	ON_BN_CLICKED(IDC_ADD_SEL_CYCLE_BTN, OnAddSelCycleBtn)
	ON_BN_CLICKED(IDC_DEL_SEL_CYCLE_BTN, OnDelSelCycleBtn)
	ON_BN_CLICKED(IDC_CYCLE_STRING_CHECK, OnCycleStringCheck)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_X_AXIS_COMBO, OnSelchangeXAxisCombo)
	ON_LBN_SELCHANGE(IDC_Y_AXIS_LIST, OnSelchangeYAxisList)
	ON_CBN_SELCHANGE(IDC_Y_UNIT_COMBO, OnSelchangeYUnitCombo)
	ON_BN_CLICKED(IDC_TEST_FROM_FOLDER_BTN, OnTestFromFolderBtn)
	ON_BN_CLICKED(IDC_TEST_FROM_LIST_BTN, OnTestFromListBtn)
	ON_BN_CLICKED(IDC_BTN_SEARCH, OnTestFromSearch)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_CHANNEL_LIST, OnItemchangedChannelList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_Y_AXIS_CTRL_LIST, OnItemchangedYAxisCtrlList)
	ON_BN_CLICKED(IDC_BTN_ALL_SELECT, OnBtnAllSelect)
	ON_BN_CLICKED(IDC_BTN_ALL_UNSELECT, OnBtnAllUnSelect)
	ON_BN_CLICKED(IDC_BUTTON_ALL, OnButtonAll)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, OnButtonCancel)
	ON_BN_CLICKED(IDC_CHARGE_CHECK, OnChargeCheck)
	ON_BN_CLICKED(IDC_DISCHARGE_CHECK, OnDischargeCheck)
	ON_BN_CLICKED(IDC_REST_CHECK, OnRestCheck)
	ON_BN_CLICKED(IDC_IMP_CHECK, OnImpCheck)
	ON_BN_CLICKED(IDC_PATTERN_CHECK, OnPatternCheck)
	ON_EN_CHANGE(IDC_EDIT_MAXROW, OnChangeEditMaxrow)
	ON_BN_CLICKED(IDC_BTN_ALL_CAN_SELECT, OnBtnAllCanSelect)
	ON_BN_CLICKED(IDC_BTN_ALL_CAN_UNSELECT, OnBtnAllCanUnSelect)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_A, OnBtnCanSelectsetA)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_B, OnBtnCanSelectsetB)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_C, OnBtnCanSelectsetC)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_D, OnBtnCanSelectsetD)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_A_CLR, OnBtnCanSelectsetAClr)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_B_CLR, OnBtnCanSelectsetBClr)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_C_CLR, OnBtnCanSelectsetCClr)
	ON_BN_CLICKED(IDC_BTN_CAN_SELECTSET_D_CLR, OnBtnCanSelectsetDClr)
	ON_LBN_SELCHANGE(IDC_CAN_SELECT_LIST, OnSelchangeCanSelectList)
	ON_LBN_SELCHANGE(IDC_AUX_SELECT_LIST, OnSelchangeAuxSelectList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataQueryDialog message handlers

BOOL CDataQueryDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//
	// 1. 모양만들기
	//

	// Test Path ListCtrl
	m_TestNameListCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);
//	m_TestNameListCtrl.SetHoverTime(1000);
	m_TestNameListCtrl.InsertColumn(1, "No",	LVCFMT_LEFT, 60,  0);
	m_TestNameListCtrl.InsertColumn(2, "Name", LVCFMT_LEFT, 165, 1);
	m_TestNameListCtrl.InsertColumn(3, "Date", LVCFMT_LEFT, 165, 2);

	// Test Path Button
//	m_TestFromListBtn.LoadBitmaps(IDB_LIST_UP,IDB_LIST_DOWN,IDB_LIST_FOCUS);
//	m_TestFromFolderBtn.LoadBitmaps(IDB_FOLDER_UP,IDB_FOLDER_DOWN,IDB_FOLDER_FOCUS);

	// Channel ListCtrl
	m_ChannelListCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);
//	m_ChannelListCtrl.SetHoverTime(10000);
	m_ChannelListCtrl.InsertColumn(1, "Name", LVCFMT_LEFT, 110,  0);
	m_ChannelListCtrl.InsertColumn(2, "Channel", LVCFMT_LEFT, 100, 1);
//	m_ChannelListCtrl.InsertColumn(3, "Cycle",   LVCFMT_LEFT, 55, 2);
	if(GetQueryMode() == QUERY_MODE_INDEX)
	{
		m_ChannelListCtrl.InsertColumn(3, "Step",   LVCFMT_LEFT, 50, 2);
	}
	else
	{
		m_ChannelListCtrl.InsertColumn(3, "Cycle",   LVCFMT_LEFT, 50, 2);
	}
	m_ChannelListCtrl.InsertColumn(4, "Aux Num",   LVCFMT_LEFT, 71, 2);

	// Y_AXIS_ListCtrl
	m_ctrlYAxisList.SetExtendedStyle(LVS_EX_FULLROWSELECT  | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);
	CRect rect;
	m_ctrlYAxisList.GetClientRect(&rect);
	m_ctrlYAxisList.InsertColumn(1, "Select", LVCFMT_LEFT, rect.Width()-17, 0);
	//
	// 2. "sysinfo.mdb" database를 연다.
	//

	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	CString path(szBuff);
//	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

// 	switch(nSelLanguage) 20190701
// 	{
// 	case 1: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME;break;
// 	case 2: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	case 3: path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_PL; break;
// 	default : path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	}
	path = path.Left(path.ReverseFind('\\'))+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME;
	m_pDatabase = new CDaoDatabase;
	m_pDatabase->Open(path);

	//
	// 3. TestPathListCtrl 및 ChannelListCtrl의 내용을 넣는다.
	//
	POSITION pos = m_strlistTestPath.GetHeadPosition();
	int aa =0;
	while(pos)
	{
		AddTestPath(m_strlistTestPath.GetNext(pos), aa++);
	}
	SortChannelListCtrl();

	//
	// Measurement Points
	//
	CFileFind afinder;
	if(!afinder.FindFile(path.Left(path.ReverseFind('\\'))+"\\PackType.txt"))
	{
/*		CRect rect1, rect2;
		GetWindowRect(rect1);
		((CStatic*)GetDlgItem(IDC_STATIC_LINE))->GetWindowRect(rect2);
		MoveWindow(rect1.left,rect1.top,rect1.Width(),rect2.bottom-rect1.top);
*/	}
	else
	{
		if(m_byVoltagePoint&0x01) ((CButton*)GetDlgItem(IDC_VOLTAGE1_CHECK))->SetCheck(1);
		if(m_byVoltagePoint&0x02) ((CButton*)GetDlgItem(IDC_VOLTAGE2_CHECK))->SetCheck(1);
		if(m_byVoltagePoint&0x04) ((CButton*)GetDlgItem(IDC_VOLTAGE3_CHECK))->SetCheck(1);
		if(m_byVoltagePoint&0x08) ((CButton*)GetDlgItem(IDC_VOLTAGE4_CHECK))->SetCheck(1);
		if(m_byTemperaturePoint&0x01) ((CButton*)GetDlgItem(IDC_TEMPERATURE1_CHECK))->SetCheck(1);
		if(m_byTemperaturePoint&0x02) ((CButton*)GetDlgItem(IDC_TEMPERATURE2_CHECK))->SetCheck(1);
	}
	
	m_iStartType = AfxGetApp()->GetProfileInt(QUERY_REG_SECTION, "Start Option", 0);
	m_iMode = AfxGetApp()->GetProfileInt(QUERY_REG_SECTION, "Step Option", (0x01 << PS_STEP_CHARGE)|(0x01 << PS_STEP_DISCHARGE)|(0x01 << PS_STEP_REST|(0x01<<PS_STEP_IMPEDANCE)));
// 	if(m_iMode&(0x01 << PS_STEP_CHARGE)) ((CButton*)GetDlgItem(IDC_CHARGE_CHECK))->SetCheck(1);
// 	if(m_iMode&(0x01 << PS_STEP_DISCHARGE)) ((CButton*)GetDlgItem(IDC_DISCHARGE_CHECK))->SetCheck(1);
// 	if(m_iMode&(0x01 << PS_STEP_REST)) ((CButton*)GetDlgItem(IDC_REST_CHECK))->SetCheck(1);
// 	if(m_iMode&(0x01 << PS_STEP_IMPEDANCE)) ((CButton*)GetDlgItem(IDC_IMP_CHECK))->SetCheck(1);
// 	if(m_iMode&(0x01 << PS_STEP_PATTERN)) ((CButton*)GetDlgItem(IDC_PATTERN_CHECK))->SetCheck(1);
	if(m_iMode&(0x01 << PS_STEP_CHARGE)) m_chkCharge = TRUE;
	if(m_iMode&(0x01 << PS_STEP_DISCHARGE)) m_chkDisCharge = TRUE;
	if(m_iMode&(0x01 << PS_STEP_REST)) m_chkRest = TRUE;
	if(m_iMode&(0x01 << PS_STEP_IMPEDANCE)) m_chkImpedance = TRUE;
	if(m_iMode&(0x01 << PS_STEP_PATTERN)) m_chkPattern = TRUE;
	if(m_iMode&(0x01 << PS_STEP_EXT_CAN)) m_chkExtCAN = TRUE;		//ljb 201145 이재복 //////////

	if(GetDlgItem(IDC_EXT_CAN_CHECK)->IsWindowVisible() == FALSE) m_chkExtCAN = FALSE;//lmh 20120727 윈도우의 상태가 보이지 않음이면 체크안함으로 변경
	if(GetDlgItem(IDC_IMP_CHECK)->IsWindowVisible() == FALSE) m_chkImpedance = FALSE;//lmh 20120726 윈도우의 상태가 보이지 않음이면 체크안함으로 변경


	// Query Condition의 내용에 따라 이전에 선택했던 Channel들을
	// 선택한 상태로 표시해 준다.
	if(m_pQueryCondition->GetChPathCount() > 0)
	{
		int N = m_ChannelListCtrl.GetItemCount();
		for(int nItem=0; nItem<N; nItem++)
		{
			//
	//		int nIndex        = atoi(m_ChannelListCtrl.GetItemText(nItem,0))-1;
			int nIndex        = m_ChannelListCtrl.GetItemData(nItem);
			CString strChPath = m_strlistTestPath.GetAt(m_strlistTestPath.FindIndex(nIndex))
							   +"\\"+m_ChannelListCtrl.GetItemText(nItem,1);
			strChPath.MakeLower();

			m_ChannelListCtrl.SetItemState(nItem, ~LVIS_SELECTED, LVIS_SELECTED);
			//
			POSITION pos = m_pQueryCondition->GetChPathHead();
			while(pos)
			{
				CString strPath   = m_pQueryCondition->GetNextChPath(pos);
				if(strPath.CompareNoCase(strChPath)==0){
					m_ChannelListCtrl.SetItemState(nItem,LVIS_SELECTED,LVIS_SELECTED);
					m_ChannelListCtrl.Update(nItem);
					break;
				}
			}
		}
	}

	//
	// 4. Cycle Selection List
	//

	pos = m_strlistSelCycle.GetHeadPosition();
	while(pos)
	{
		CString str = m_strlistSelCycle.GetNext(pos) ;
		m_SelCycleList.AddString(str);

//		m_SelCycleList.AddString(m_strlistSelCycle.GetNext(pos));
	}

	//
	// 5. Cycle Selection String
	//

	OnCycleStringCheck();

	//
	// 5. X Axis Combo Box
	//
	CDaoRecordset aRecordset(m_pDatabase);
	aRecordset.Open(dbOpenDynaset, "SELECT Index, Name FROM AxisInfo WHERE IsUsedForXAxis=-1");
	aRecordset.MoveLast();
	m_pbyXAxisIndex = new BYTE[aRecordset.GetRecordCount()];
	aRecordset.MoveFirst();

	int i=0;
	//기본 선택 축 Item
//	int nXAxisDefaultIndex = 0;		//Time
//	int nYAxisDefaultIndex = 3;		//Capacity

	CString strXName;
	CString strDefaultXName("Time");
	strDefaultXName = AfxGetApp()->GetProfileString(QUERY_REG_SECTION, "X Axis", "Time");
	
	COleVariant var;
	while(!aRecordset.IsEOF())
	{
		aRecordset.GetFieldValue("Name",var);
		strXName = (LPCTSTR)(LPTSTR)var.bstrVal;
		m_XAxisCombo.AddString(strXName);

//		//X축을 기본으로 cycle 항목을 선택한다.
		var.Clear();
		aRecordset.GetFieldValue("Index",var);
		m_pbyXAxisIndex[i] = var.bVal;
		
		aRecordset.MoveNext();
		i++;
	}
	aRecordset.Close();

	//
	// 6. X Unit Combo Box
	//
	//X 축을 설정 한다.
	CAxis* pXAxis = m_pQueryCondition->GetXAxis();

	if(!pXAxis->GetTitle().IsEmpty())
	{
		strDefaultXName = pXAxis->GetTitle();
	}

	if(m_XAxisCombo.SelectString(0, strDefaultXName) == CB_ERR)
	{
		m_XAxisCombo.SetCurSel(0);	//
	}
//	m_XAxisCombo.GetLBText(m_XAxisCombo.GetCurSel(), strDefaultXName);
	OnSelchangeXAxisCombo();
	

	
	 // Y Axis ListBox
	pos = m_pQueryCondition->GetYAxisHead();
	if(pos)		////기존 그래프 조건 재설정시에는 기본 설정을 Reset 시키고 이전 조건을 Diaplay한다.
	{	
		//현재 선택을 Reset 시킨다.
		for(int index =0; index< m_ctrlYAxisList.GetItemCount(); index++)
		{
			m_ctrlYAxisList.SetCheck(index,0);
		}

		while(pos)		
		{
			//Y축과 같은 Item이면 선택 표시한다.
			CAxis* pYAxis = m_pQueryCondition->GetNextYAxis(pos);
			int index =0;
			for(int index =0; index< m_ctrlYAxisList.GetItemCount(); index++)
			{
				CString str;
				str = m_ctrlYAxisList.GetItemText(index, 0);
				if(str.CompareNoCase(pYAxis->GetTitle())==0) break;
				if(str.Left(3) == pYAxis->GetTitle().Left(3))
				{
					//bAuxListFlag = FALSE;
					bCanListFlag = FALSE;
					break;
				}
			}
			//m_YAxisList.SetCheck(index, 1);
			m_ctrlYAxisList.SetCheck(index,1);

			CString strQuery;
			strQuery.Format("SELECT UnitTransfer From Property WHERE Index = %d",
							m_YAxisPropertyIndex.GetAt(m_YAxisPropertyIndex.FindIndex(index)).x);
			aRecordset.Open(dbOpenDynaset, strQuery);
			int n=0;
			while(!aRecordset.IsEOF())
			{
				COleVariant var;
				aRecordset.GetFieldValue("UnitTransfer", var);
				if(var.fltVal == pYAxis->GetUnitTransfer()) break;
				aRecordset.MoveNext();
				n++;
			}
			aRecordset.Close();

			//Unit 선택
			//m_YAxisList.SetCurSel(index);
			m_ctrlYAxisList.SetItemState(i, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED| LVIS_FOCUSED); 

			m_YAxisPropertyIndex.GetAt(m_YAxisPropertyIndex.FindIndex(index)).y = n;		//Unit의 Combo Index 저장

			OnSelchangeYAxisList();							//
		}
	}
/*
	//X축 선택에 맞추어 기본적인 표시한다.
////////////////////////////
	if( m_XAxisCombo.GetCurSel() == 1  ) {			//
		m_YAxisList.SetCheck(nYAxisDefaultIndex, 1);//
		m_YAxisList.SetCurSel(nYAxisDefaultIndex);	//
	}												//
	OnSelchangeYAxisList();							//
///////////////////////////////////////////
*/	
	//

	UpdateData(FALSE);

	//CTSMonPro에서 호출시
	//파일은 선택되었지만 cycle 구간이 선택이 안되어 있으면 
	//기존적으로 현재 구간 적용
	if(m_SelCycleList.GetCount() < 1 && m_ChannelListCtrl.GetItemCount() > 0)
	{
		OnAddSelCycleBtn();
	}
	
	//구간을 1개만 선택 하면 구간 그리기 옵션을 Disable 시킴 
/*	if(m_SelCycleList.GetCount() < 2)
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(TRUE);
		
	}
*/
/*	//////////////////////////////////////////////////////////////////////////
	CString str;							//
	if ( m_lToCycle == MAX_CYCLE_NUM )		//
		str.Format("%d", 1);				//
	else									//
		str.Format("%d",m_lToCycle);		//
	SetDlgItemText(IDC_CYCLE_TO_EDIT, str);	//
//////////////////////////////////////////////////////////////////////////
*/

	// Added by 224 (2013/12/27) : 최대 허용 ROW 갯수
	CString str ;
	str = AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "MAXROW", "1000000");
	GetDlgItem(IDC_EDIT_MAXROW)->SetWindowText(str) ;

//	OnTestFromFolderBtn();
	//20100403  //Query Mode에 따라 화면 모양 변경 
	if(GetQueryMode() == QUERY_MODE_INDEX)
	{
		GetDlgItem(IDC_RANGE_STATIC)->SetWindowText("Result STEP Range");
		// 		LVCOLUMN col;
		// 		col.mask = LVCF_TEXT;
		// 		char szTitle[32];
		// 		col.pszText = szTitle;
		// 		col.cchTextMax = 31;
		// 		if (m_ChannelListCtrl.GetColumn(3, &col))
		// 		{
		// 			sprintf(szTitle, "STEP");
		// 			m_ChannelListCtrl.SetColumn(3, &col);
		// 		}
	}

	InitCtrlListAuxBtn(); //yulee 20181008
	InitCtrlListCANBtn(); //yulee 20181008
	
	if (AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "ProcessGraphReload", 0) == 1)		CDialog::OnOK();	// 210805 HKH Reload 관련 추가
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDataQueryDialog::InitCtrlListAuxBtn() //yulee 20181008
{
	BOOL bChk = TRUE, bUnChk = FALSE;
	
	for(int i = 0 ; i < m_ctrlAuxList.GetCount(); i++)
	{
		if(m_ctrlAuxList.GetCheck(i) == 0)
			bChk = FALSE;
		if(m_ctrlAuxList.GetCheck(i) == 1)
			bUnChk = TRUE;
	}
	
	if(bChk == TRUE)
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(TRUE);
		return;
	}
	if(bUnChk == FALSE)
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(FALSE);
		return;
	}
	if((bChk == FALSE)&&(bUnChk == TRUE))
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(TRUE);
		return;
	}


}

void CDataQueryDialog::InitCtrlListCANBtn() //yulee 20181008
{
	BOOL bChk = TRUE, bUnChk = FALSE;
	
	for(int i = 0 ; i < m_ctrlCanList.GetCount(); i++)
	{
		if(m_ctrlCanList.GetCheck(i) == 0)
			bChk = FALSE;
		if(m_ctrlCanList.GetCheck(i) == 1)
			bUnChk = TRUE;
	}
	
	if(bChk == TRUE)
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(TRUE);
		return;
	}
	if(bUnChk == FALSE)
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(FALSE);
		return;
	}
	
	if((bChk == FALSE)&&(bUnChk == TRUE))
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(TRUE);
		return;
	}
}

//
void CDataQueryDialog::OnAddSelCycleBtn() 
{
	int iFrom=0, iTo=0, iInterval=0;
	CString strTemp;

	//
	GetDlgItem(IDC_CYCLE_FROM_EDIT)->GetWindowText(strTemp);
	iFrom     = atoi(strTemp);
	GetDlgItem(IDC_CYCLE_TO_EDIT)->GetWindowText(strTemp);
	iTo       = atoi(strTemp);
	GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->GetWindowText(strTemp);
	iInterval = atoi(strTemp);

	//m_SelCycleList.ResetContent(); //ksj 20210223 : 주석처리. 왜 add 하는데 기존 목록을 날리지?
	//m_strlistSelCycle.RemoveAll(); //ksj 20210223 : 주석처리. 왜 add 하는데 기존 목록을 날리지?

	if(iFrom < 1 )//$1013
	{
		//MessageBox("시작 구간 설정이 잘못되었습니다.(0 이상값 입력)", "Error", MB_OK|MB_ICONSTOP);
		MessageBox(Fun_FindMsg("DataQueryDialog_OnAddSelCycleBtn_msg1","IDD_DATA_QUERY_DIALOG"), "Error", MB_OK|MB_ICONSTOP);//&&
		GetDlgItem(IDC_CYCLE_FROM_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_FROM_EDIT))->SetSel(0,-1);
		return;
	}

	if( iTo < 1 )
	{//$1013
		//MessageBox("구간 설정이 잘못되었습니다.(0 이상값 입력)", "Error", MB_OK|MB_ICONSTOP);
		MessageBox(Fun_FindMsg("DataQueryDialog_OnAddSelCycleBtn_msg2","IDD_DATA_QUERY_DIALOG"), "Error", MB_OK|MB_ICONSTOP);//&&
		GetDlgItem(IDC_CYCLE_TO_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_TO_EDIT))->SetSel(0,-1);
		return;
	}

	if( iInterval < 1 )
	{//$1013
		//MessageBox("Interval 설정이 잘못되었습니다.(0 이상값 입력)", "Error", MB_OK|MB_ICONSTOP);
		MessageBox(Fun_FindMsg("DataQueryDialog_OnAddSelCycleBtn_msg3","IDD_DATA_QUERY_DIALOG"), "Error", MB_OK|MB_ICONSTOP);//&&
		GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_INTERVAL_EDIT))->SetSel(0,-1);
		return;
	}


	if(iFrom > iTo )
	{//$1013
		//MessageBox("구간 설정이 잘못되었습니다.", "Error", MB_OK|MB_ICONSTOP);
		MessageBox(Fun_FindMsg("DataQueryDialog_OnAddSelCycleBtn_msg4","IDD_DATA_QUERY_DIALOG"), "Error", MB_OK|MB_ICONSTOP);//&&
		GetDlgItem(IDC_CYCLE_TO_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_TO_EDIT))->SetSel(0,-1);
		return;
	}
	
	// 1~1 : 1 형태 입력
	if(iTo == iFrom && iInterval != 1) 
	{//$1013
		//MessageBox("Interval 설정이 잘못되었습니다.", "Error", MB_OK|MB_ICONSTOP);
		MessageBox(Fun_FindMsg("DataQueryDialog_OnAddSelCycleBtn_msg5","IDD_DATA_QUERY_DIALOG"), "Error", MB_OK|MB_ICONSTOP);//&&
		GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->SetFocus();
		((CEdit*)GetDlgItem(IDC_CYCLE_INTERVAL_EDIT))->SetSel(0,-1);
		return;
	}
	else if(iTo > iFrom)
	{//$1013
		if((iTo-iFrom) < iInterval)
		{
			//MessageBox("Interval 설정이 잘못되었습니다.", "Error", MB_OK|MB_ICONSTOP);
			MessageBox(Fun_FindMsg("DataQueryDialog_OnAddSelCycleBtn_msg5","IDD_DATA_QUERY_DIALOG"), "Error", MB_OK|MB_ICONSTOP);//&&
			GetDlgItem(IDC_CYCLE_INTERVAL_EDIT)->SetFocus();
			((CEdit*)GetDlgItem(IDC_CYCLE_INTERVAL_EDIT))->SetSel(0,-1);
			return;
		}
	}

	//
	strTemp.Format("%d~%d:%d", iFrom, iTo, iInterval);
	
	POSITION pos = m_strlistSelCycle.GetHeadPosition();
	while(pos)
	{
		if(strTemp == m_strlistSelCycle.GetNext(pos))
		{
//			MessageBox("이미 추가된 구간입니다.", "Error", MB_OK|MB_ICONWARNING);
			return;
		}
	}

	m_SelCycleList.AddString(strTemp);

	//20090829 Bug //20090911
	//m_SelCycleList Sort 속성 없애야 함
	//m_strlistSelCycle.AddHead(strTemp);
	m_strlistSelCycle.AddTail(strTemp);

	//
	GetDlgItem(IDC_CYCLE_FROM_EDIT)->SetFocus();
	((CEdit*)GetDlgItem(IDC_CYCLE_FROM_EDIT))->SetSel(0,-1);

	//구간을 1개만 선택 하면 구간 그리기 옵션을 Disable 시킴 
/*	if(m_SelCycleList.GetCount() < 2)
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(TRUE);
		
	}
*/
}

void CDataQueryDialog::OnDelSelCycleBtn()
{
	int iCurSel = m_SelCycleList.GetCurSel();
	if(iCurSel!=LB_ERR)
	{
		POSITION pos = m_strlistSelCycle.FindIndex(iCurSel);
		ASSERT(pos);
		m_strlistSelCycle.RemoveAt(pos);
		m_SelCycleList.DeleteString(iCurSel);
		m_SelCycleList.SetCurSel(iCurSel);
		GetDlgItem(IDC_DEL_SEL_CYCLE_BTN)->SetFocus();
	}
	
	//구간을 1개만 선택 하면 구간 그리기 옵션을 Disable 시킴 
/*	if(m_SelCycleList.GetCount() < 2)
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_START_POINT_GROUP_STATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_START_TYPE_RADIO1)->EnableWindow(TRUE);
		
	}
*/

}

void CDataQueryDialog::OnCycleStringCheck() 
{
	GetDlgItem(IDC_CYCLE_STRING_EDIT)->EnableWindow(((CButton*)GetDlgItem(IDC_CYCLE_STRING_CHECK))->GetCheck());
}

void CDataQueryDialog::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if(m_pDatabase!=NULL)
	{
		if(m_pDatabase->IsOpen()) m_pDatabase->Close();
		delete m_pDatabase;
		m_pDatabase=NULL;
	}

	if(m_pbyXAxisIndex!=NULL)
	{
		delete [] m_pbyXAxisIndex;
		m_pbyXAxisIndex = NULL;
	}
	if(m_pstrPathList != NULL)//lmh 20120727
	{
		m_pstrPathList->RemoveAll();
		delete m_pstrPathList;
		m_pstrPathList = NULL;
	}
}

void CDataQueryDialog::OnSelchangeXAxisCombo() 
{
	int iCurSel = m_XAxisCombo.GetCurSel();
	if(iCurSel != CB_ERR)
	{
		CDaoRecordset aRecordset(m_pDatabase);
		CString strQuery;
		strQuery.Format("SELECT YAxisIndexList, PropertyIndex FROM AxisInfo WHERE Index = %d" ,
			            m_pbyXAxisIndex[iCurSel]);
		aRecordset.Open(dbOpenDynaset,strQuery);
			//
			COleVariant var;
			aRecordset.GetFieldValue("YAxisIndexList", var);
			CString strYAxisIndex   = (LPCTSTR)(LPTSTR)var.bstrVal;
			var.Clear();
			//
			aRecordset.GetFieldValue("PropertyIndex", var);
			BYTE    byPropertyIndex = var.bVal;
			var.Clear();
		aRecordset.Close();

		// X-Unit ComboBox
		m_XUnitCombo.ResetContent();
		strQuery.Format("SELECT UnitNotation FROM Property WHERE Index = %d", byPropertyIndex);
		aRecordset.Open(dbOpenDynaset, strQuery);
			while(!aRecordset.IsEOF())
			{
				COleVariant var;
				aRecordset.GetFieldValue("UnitNotation", var);
				m_XUnitCombo.AddString((LPCTSTR)(LPTSTR)var.bstrVal);
				aRecordset.MoveNext();
			}
		aRecordset.Close();
//		m_XUnitCombo.SetCurSel(0);

//////////////////////////////////////////////////////////////////////////
//	사용자가 이전에 사용한 설정을 Registry에서 read하여 기본적으로 표시해준다.
//////////////////////////////////////////////////////////////////////////

		LVCOLUMN col;
		col.mask = LVCF_TEXT;
		char szTitle[32];
		col.pszText = szTitle;
		col.cchTextMax = 31;

		//선택 X축 저장 (다음 시작시 기본적으로 이축을 선택하도록 하기 위해)		
		CString strDefaultXName;
		m_XAxisCombo.GetLBText(iCurSel, strDefaultXName);
		if(strDefaultXName.CompareNoCase("Cycle") == 0 || strDefaultXName.CompareNoCase("Capacity") == 0
			 || strDefaultXName.CompareNoCase("Current") == 0 || strDefaultXName.CompareNoCase("Voltage") == 0)
		{
			GetDlgItem(IDC_CYCLE_WARRING_STATIC)->ShowWindow(SW_SHOW);//$1013
			//GetDlgItem(IDC_FRAM_TITLE_1)->SetWindowText("Cycle 구간 및 Data 선택");		//ljb 201156 
			GetDlgItem(IDC_FRAM_TITLE_1)->SetWindowText(Fun_FindMsg("DataQueryDialog_OnSelchangeXAxisCombo_msg1","IDD_DATA_QUERY_DIALOG"));		//ljb 201156 //&&
			//GetDlgItem(IDC_RANGE_STATIC)->SetWindowText("Cycle 구간 선택");				//ljb 201156 
			GetDlgItem(IDC_RANGE_STATIC)->SetWindowText(Fun_FindMsg("DataQueryDialog_OnSelchangeXAxisCombo_msg2","IDD_DATA_QUERY_DIALOG"));				//ljb 201156 //&&
			if (m_ChannelListCtrl.GetColumn(2, &col))
			{
				sprintf(szTitle, "Cycle");
				m_ChannelListCtrl.SetColumn(2, &col);
			}
// 			GetDlgItem(IDC_CYCLE_STRING_EDIT2)->SetWindowText("");
// 			GetDlgItem(IDC_CYCLE_STRING_EDIT2)->EnableWindow(FALSE);
			SetQueryMode(FALSE);
		}
		else
		{
			GetDlgItem(IDC_CYCLE_WARRING_STATIC)->ShowWindow(SW_HIDE);	//$1013		
			//GetDlgItem(IDC_FRAM_TITLE_1)->SetWindowText("STEP 구간 및 Data 선택");		//ljb 201156 
			GetDlgItem(IDC_FRAM_TITLE_1)->SetWindowText(Fun_FindMsg("DataQueryDialog_OnSelchangeXAxisCombo_msg3","IDD_DATA_QUERY_DIALOG"));		//ljb 201156 //&&
			//GetDlgItem(IDC_RANGE_STATIC)->SetWindowText("STEP 구간 선택");				//ljb 201156 
			GetDlgItem(IDC_RANGE_STATIC)->SetWindowText(Fun_FindMsg("DataQueryDialog_OnSelchangeXAxisCombo_msg4","IDD_DATA_QUERY_DIALOG"));				//ljb 201156 //&&
			if (m_ChannelListCtrl.GetColumn(2, &col))
			{
				sprintf(szTitle, "Step");
				m_ChannelListCtrl.SetColumn(2, &col);
			}
//			GetDlgItem(IDC_CYCLE_STRING_EDIT2)->EnableWindow(TRUE);
			
			SetQueryMode(TRUE);
 		}

		int p1=0, p2=0;
		//X축 선택에 따른 기본 설정 Setting
		CString strTemp, strDefaultXSet, strDefaultYSet, strDefaultAuxSet;
		strTemp = AfxGetApp()->GetProfileString(QUERY_REG_SECTION, strDefaultXName, "sec,0,1,:0,0,");
		int nFindIndex = strTemp.Find(':');
		strDefaultXSet = strTemp.Left(nFindIndex);		//X축 이전 설정 상태 
		nFindIndex+=1;
		strDefaultYSet = strTemp.Mid(nFindIndex, strTemp.ReverseFind(':') - nFindIndex);		//Y축 이전 설정 Unit
		strDefaultAuxSet = strTemp.Mid(strTemp.ReverseFind(':')+1);	//Aux List 설정 상태

//		strDefaultXSet = strTemp.Left(strTemp.Find(':'));		//X축 이전 설정 상태 
//		strDefaultYSet = strTemp.Mid(strTemp.Find(':')+1);		//Y축 이전 설정 Unit

		//X축 Unit 설정
		CAxis* pXAxis = m_pQueryCondition->GetXAxis();
		CString strXUnit, strDefaultXUnit;
		strXUnit = pXAxis->GetUnitNotation();

		//X축 default Unit 설정
		p2=strDefaultXSet.Find(',',p1);
		strDefaultXUnit = strDefaultXSet.Mid(p1,p2-p1);
		if(strXUnit.IsEmpty())
		{
			strXUnit = strDefaultXUnit;
		}
		if(m_XUnitCombo.SelectString(0, strXUnit) == CB_ERR)
		{
			m_XUnitCombo.SetCurSel(0);
		}
		
		//default y축 선택 Item
		CByteArray	yAxisArray, yUnitArray;
		while(p2 >= 0)
		{
			p1=p2+1;		
			p2=strDefaultXSet.Find(',',p1);
			if(p2 < 0)	break;
			yAxisArray.Add((BYTE)atoi(strDefaultXSet.Mid(p1,p2-p1)));
		}

		//Y축 default Unit을 구한다.
		p1=0, p2=0;
		while(p2 >= 0)
		{
			p2=strDefaultYSet.Find(',',p1);
			if(p2 < 0)	break;
			CString s = strDefaultYSet.Mid(p1,p2-p1);
			yUnitArray.Add((BYTE)atoi(strDefaultYSet.Mid(p1,p2-p1)));
			p1=p2+1;		
		}

		//Aux default
		p1 = 0; p2 = 0;
		while(p2 >=0)
		{
			p2=strDefaultAuxSet.Find(',',p1);
			if(p2 < 0)	break;
			CString s = strDefaultAuxSet.Mid(p1,p2-p1);
			yAuxArray.Add((BYTE)atoi(strDefaultAuxSet.Mid(p1,p2-p1)));
			p1=p2+1;	
		}
//////////////////////////////////////////////////////////////////////////
		

		// Y-Axis ListBox
		//m_YAxisList.ResetContent();
		m_ctrlYAxisList.DeleteAllItems();
		
		m_YAxisPropertyIndex.RemoveAll();
		p1=0;
		p2=0;
		int cnt = 0;
		while(TRUE)
		{
			p2=strYAxisIndex.Find(',',p1);
			if(p2 == -1) break;
			BYTE index = (BYTE) atoi(strYAxisIndex.Mid(p1,p2-p1));
			strQuery.Format("SELECT Name, PropertyIndex From AxisInfo WHERE Index = %d", index);
			aRecordset.Open(dbOpenDynaset, strQuery);
			COleVariant var;
			aRecordset.GetFieldValue("Name",var);
			//m_YAxisList.AddString((LPCTSTR)(LPTSTR)var.bstrVal);
			strTemp.Format("%s",(LPCTSTR)(LPTSTR)var.bstrVal);
			if( strTemp.Left(3) == "Aux" ) //lmh 20111012
			{
				if(strTemp.Right(2) == "TH") //20180607 yulee 
					m_ctrlYAxisList.InsertItem(m_ctrlYAxisList.GetItemCount(), strTemp.Left(5)); 
				else 
					m_ctrlYAxisList.InsertItem(m_ctrlYAxisList.GetItemCount(), strTemp.Left(4)); 
			}
			else 
				m_ctrlYAxisList.InsertItem(m_ctrlYAxisList.GetItemCount(), (LPCTSTR)(LPTSTR)var.bstrVal);
//			m_ctrlYAxisList.InsertItem(m_ctrlYAxisList.GetItemCount(), (LPCTSTR)(LPTSTR)var.bstrVal);
			TRACE("%s\r\n", (LPCTSTR)(LPTSTR)var.bstrVal);
			var.Clear();
			aRecordset.GetFieldValue("PropertyIndex", var);
			
			//기본적으로 각 축의 Unit은 Combo 0을 선택하고 이전 설정이 있으면 이전 설정을 기본으로 선택한다.
			CPoint pt(var.bVal, 0);
			if(cnt < yUnitArray.GetSize())
			{
				pt.y = yUnitArray[cnt];			//(Propoerty Index, Unit Combo Index)
			}
			cnt++;
			m_YAxisPropertyIndex.AddTail(pt);
			aRecordset.Close();
			p1=p2+1;
		}

		//Y축 기본 설정
		for(int i=0; i<yAxisArray.GetSize(); i++)
		{
			//m_YAxisList.SetCheck(yAxisArray[i], 1);
			m_ctrlYAxisList.SetCheck(yAxisArray[i], 1);
		}

		// Y-Unit Combobox
		// m_YUnitCombo.ResetContent();
		if(yAxisArray.GetSize() > 0)
		{
			//m_YAxisList.SetCurSel(yAxisArray[yAxisArray.GetSize()-1]);
			m_ctrlYAxisList.SetItemState(yAxisArray[yAxisArray.GetSize()-1], LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED| LVIS_FOCUSED); 
			OnSelchangeYAxisList();							//
		}


	}

	//lmh 20120727 밑에 새로 추가

	if(m_pstrPathList == NULL) return;//lmh 20120731
	m_strlistTestPath.RemoveAll();
	m_TestNameListCtrl.DeleteAllItems();
	m_ChannelListCtrl.DeleteAllItems();
	

	
	CString strPath;
	POSITION pos = m_pstrPathList->GetHeadPosition();
	
	//	int aa = dlg.GetSelList()->GetCount();
	//	if(aa > 0)	aa--;
	
	int aa = 0;
	while(pos)
	{	
		strPath = m_pstrPathList->GetNext(pos);
		//		strFileName.MakeLower();
		m_strlistTestPath.AddTail(strPath);
		
		TRACE("Data Index %d\n", aa);
		AddTestPath(strPath, aa++);	//시험목록 리스트와 채널 리스트에 선택 폴더 내용 추가 
		
	}
	
	SortChannelListCtrl();	//표기된 채널 리스트 목록을 sort 시킨다.
	OnAddSelCycleBtn();		//cycle List 추가 
	
	//20100430
	UpdateStepNoList();
}



void CDataQueryDialog::OnSelchangeYUnitCombo() 
{
	//int iCurYAxisSel = m_YAxisList.GetCurSel();
	int iCurYAxisSel=0;
	POSITION pos = m_ctrlYAxisList.GetFirstSelectedItemPosition();
	
	while (pos)
	{
	  iCurYAxisSel = m_ctrlYAxisList.GetNextSelectedItem(pos);
	}

	if(iCurYAxisSel!=LB_ERR)
	{
		int iCurYUnitSel = m_YUnitCombo.GetCurSel();
		if(iCurYUnitSel!=CB_ERR)
		{
			POSITION pos = m_YAxisPropertyIndex.FindIndex(iCurYAxisSel);
			m_YAxisPropertyIndex.GetAt(pos).y= iCurYUnitSel;
		}
	}
}

// 2005/4/20 KBH
// Data Folder를 선택시
//
void CDataQueryDialog::OnTestFromFolderBtn() 
{
	CResultTestSelDlg	dlg;
	
	dlg.SetTestList(&m_strlistTestPath);
	if( dlg.DoModal() == IDCANCEL)
	{
		return;
	}
	
	//기존 선택에 Add or 새롭게 추가 
	m_strlistTestPath.RemoveAll();
	m_TestNameListCtrl.DeleteAllItems();
	m_ChannelListCtrl.DeleteAllItems();
	
	CString strPath;
	POSITION pos = dlg.GetSelList()->GetHeadPosition();
	if(m_pstrPathList != NULL)
	{
		m_pstrPathList->RemoveAll();
		delete m_pstrPathList;
		m_pstrPathList = NULL;
	}
	m_pstrPathList = new CStringList;
//	memcpy(m_pstrPathList,dlg.GetSelList(),dlg.GetSelList()->GetCount()*sizeof(CStringList));
	
	//	int aa = dlg.GetSelList()->GetCount();
	//	if(aa > 0)	aa--;
	
	int aa = 0;
	while(pos)
	{	
		strPath = dlg.GetSelList()->GetNext(pos);
		m_pstrPathList->AddTail(strPath);//lmh 20120727
		//		strFileName.MakeLower();
		m_strlistTestPath.AddTail(strPath);
		
		TRACE("Data Index %d\n", aa);

		// Commented by 224 (2013/12/23) : m_ChannelListCtrl 에 STEP END 정보를 표시한다.
		AddTestPath(strPath, aa++);	//시험목록 리스트와 채널 리스트에 선택 폴더 내용 추가 
		
	}
	
	SortChannelListCtrl();	//표기된 채널 리스트 목록을 sort 시킨다.
	OnAddSelCycleBtn();		//cycle List 추가 
	
	//20100430
	UpdateStepNoList();
	
	return;
}

void CDataQueryDialog::OnTestFromListBtn() 
{
	//
	CSelTestDialog aDlg;
	aDlg.m_byMode = CSelTestDialog::MODE_SELECTION;
	aDlg.m_pDatabase = m_pDatabase;
	//
	if(aDlg.DoModal()==IDOK)
	{
		POSITION pos = aDlg.m_TestPathList.GetHeadPosition();
//		int aa = aDlg.m_TestPathList.GetCount();
		int aa = 0;
		while(pos)
		{
			CString strPath = aDlg.m_TestPathList.GetNext(pos);
			if(m_strlistTestPath.Find(strPath)==NULL)
			{
				m_strlistTestPath.AddTail(strPath);
				AddTestPath(strPath, aa++);
			}
		}
		SortChannelListCtrl();

		OnAddSelCycleBtn();
	}
}

// 2005/04/20 
// 선택 폴더를 시험명 리스트에 추가 하고 
// 시험을 실시한 채널 리스트를 채널 리스트 폴더에 추가 한다.
void CDataQueryDialog::AddTestPath(LPCTSTR path, int nIndex)
{
	int nItem = m_TestNameListCtrl.GetItemCount();
	CString strLabel,strTemp;
	strLabel.Format("%d", nItem+1);

	CFileFind afinder;
	//파일 수를 센다.
	int nFileTotCount =0;
	int nFilecount = 0;
	CString str;
	str.Format("%s\\%s", path, FILE_FIND_FORMAT);
	
	BOOL bWork = afinder.FindFile(str);
	while (bWork)
	{
		bWork = afinder.FindNextFile();
		nFileTotCount++;
	}

	//파일이 하나도 없으면 return
	if (nFileTotCount <= 0)
	{
		return;
	}

	LVITEM lvitem;
	//1 컬럼에 Count 입기
	lvitem.mask     = LVIF_TEXT;
	lvitem.iItem    = nItem;
	lvitem.iSubItem = 0;
	lvitem.pszText  = (LPTSTR)(LPCTSTR)strLabel;
	m_TestNameListCtrl.InsertItem(&lvitem);

	//컬럼 2에 Test명(폴더명) 표기 
	CString strPath(path);
	CString strName = strPath.Mid(strPath.ReverseFind('\\')+1);
	m_TestNameListCtrl.SetItemText(nItem, 1, strName);

//////////////////////////////////////////////////////////
// 2003-02-25 추가 요구 사항
	m_strTestName = strName;
//////////////////////////////////////////////////////////

	//선택 폴더 채널 파일 검색 
	int iStartItem = m_ChannelListCtrl.GetItemCount();
	int iMaxCycle  = 0;
	int nMinCycle  = m_lToCycle;		//

	CProgressWnd *pProgressWnd = new CProgressWnd;
	pProgressWnd->Create(this, "Loading...", TRUE);
	//pProgressWnd->SetText("결과 목록을 검색중입니다.\n\n");//$1013
	pProgressWnd->SetText(Fun_FindMsg("DataQueryDialog_AddTestPath_msg1","IDD_DATA_QUERY_DIALOG"));//$1013//&&
	pProgressWnd->Show();

	int i = 0;
	bWork = afinder.FindFile(str);
	while (bWork)
	{
		bWork = afinder.FindNextFile();

		pProgressWnd->SetPos(int((float)nFilecount++/(float)nFileTotCount*100.0f));
		if (pProgressWnd->PeekAndPump(FALSE) == FALSE)
		{
			break;
		}

		CString ChPath = afinder.GetFileName();
		//선택 Test를 실시한 채널 List 폴더를 List에 추가 시킨다. 
		if (afinder.IsDirectory() && !afinder.IsDots())
		{
			//Total cycle을 얻기 위해 모든 data를 Loading하는게 비효율적이다.
			//최종 data를 얻기위한 별도의 함수를 사용하도록 수정
			//2006/8/22 By KBH
//			CChData chData(afinder.GetFilePath());
//			if(chData.Create())
			{
				long lEndTableSize = 0;
				// Added by 224 (2014/02/03) : 호환성을 위하여
				CChData chData(afinder.GetFilePath());
				CTable* record = CChData::GetLastDataRecord(afinder.GetFilePath(), lEndTableSize, &chData);
				if (record == NULL)
				{
					break;
				}
				
				//
				if (afinder.GetFileName().GetLength() > 512)
				{
					strTemp = afinder.GetFileName().Left(511);   //ljb 2011517  release 모드에서 죽는다
				}

//------------------------------------------------------
				//pProgressWnd->SetText("결과 목록을 검색중입니다.\n\n"+strTemp);	
				pProgressWnd->SetText(Fun_FindMsg("DataQueryDialog_AddTestPath_msg1","IDD_DATA_QUERY_DIALOG")+strTemp);	//&&
				//pProgressWnd->SetText("결과 목록을 검색중입니다.\n\n"+afinder.GetFileName());

//------------------------------------------------------

				lvitem.iItem = iStartItem+i;

				//번호 표기 
				lvitem.iSubItem = 0;
				lvitem.mask     = LVIF_TEXT|LVIF_PARAM;
				lvitem.pszText  = (LPTSTR)(LPCTSTR)m_strTestName;
				m_ChannelListCtrl.InsertItem(&lvitem);
				m_ChannelListCtrl.SetItemData(lvitem.iItem, nIndex);

				//채널명 입력
				ChPath.MakeUpper();
				m_ChannelListCtrl.SetItemText(lvitem.iItem, 1, ChPath);

//------------------------------------------------------

				//진행 TotalCycle 입력 
//				long cycle = (long)chData.GetLastDataOfTable(chData.GetTableSize()-1, RS_COL_TOT_CYCLE);
				long cycle = 0;
				if (record != NULL)
				{
					//cycle = (long)record->GetLastData(RS_COL_TOT_CYCLE);
					//20131015 ljb 주석처리
					if (GetQueryMode() == QUERY_MODE_CYCLE)
					{
						cycle = (long)record->GetLastData(RS_COL_TOT_CYCLE);
					}
					else
					{	//query가 index mode일 경우
						cycle = lEndTableSize;
 					}
					
					if (cycle < nMinCycle)	nMinCycle = cycle;
					if (cycle > iMaxCycle)	iMaxCycle = cycle;
				}

// 				long cycle = (long)record->GetLastData(RS_COL_TOT_CYCLE);
// 				if(cycle < nMinCycle)	nMinCycle = cycle;
// 				if(cycle > iMaxCycle)	iMaxCycle = cycle;
				
				str.Format("%d", cycle);
				m_ChannelListCtrl.SetItemText(lvitem.iItem, 2, str);
//------------------------------------------------------

				// Aux 정보 표시
				int nAuxCount = record->LoadAuxData(afinder.GetFilePath());
				if (nAuxCount > 0)
				{
					str.Format("%d", nAuxCount);
					m_ChannelListCtrl.SetItemText(lvitem.iItem, 3, str);

					//AuxList를 다시 그려줌
					m_ctrlAuxList.ShowWindow(SW_SHOW);
					m_ctrlAuxList.ResetContent();
					GetDlgItem(IDC_BTN_ALL_SELECT)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_BTN_ALL_UNSELECT)->ShowWindow(SW_SHOW);

					int index = m_ctrlYAxisList.GetItemCount();
					for (int i = index-1; i >= 0; i--)
					{
						 if (m_ctrlYAxisList.GetCheck(i))
						 {
							 CString strSelect;
							 strSelect = m_ctrlYAxisList.GetItemText(i, 0);

							//Aux가 체크되면 선택된 작업결과 DATA에 포함된 해당 Aux를 모두 화면에 그려준다.////
//20130215 remove
							 if (strSelect == RS_COL_AUX_TEMP)
							 {
								 UpdateAuxList(RS_COL_AUX_TEMP);
							 }
							 else if(strSelect == RS_COL_AUX_VOLT)
							 {
								 UpdateAuxList(RS_COL_AUX_VOLT);
							 }
							 else if(strSelect == RS_COL_AUX_TEMPTH)
							 {
								 UpdateAuxList(RS_COL_AUX_TEMPTH);
							 }
							 //ksj 20200116
							 else if(strSelect == RS_COL_AUX_HUMI)
							 {
								 UpdateAuxList(RS_COL_AUX_HUMI);
							 }
						 }
					}
				}

//------------------------------------------------------

				//CAN 정보 표시
				int nCanCount = record->LoadCanData(afinder.GetFilePath());
				if (nCanCount > 0)
				{
					str.Format("%d", nCanCount);
					m_ChannelListCtrl.SetItemText(lvitem.iItem, 3, str);
					
					//AuxList를 다시 그려줌
					m_ctrlCanList.ShowWindow(SW_SHOW);
					m_ctrlCanList.ResetContent(); 
					/*GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->ShowWindow(SW_SHOW); //yulee 20180803
					GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->ShowWindow(SW_SHOW); //yulee 20181008
					GetDlgItem(IDC_BTN_CAN_SELECTSET_A)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_BTN_CAN_SELECTSET_B)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_BTN_CAN_SELECTSET_C)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_BTN_CAN_SELECTSET_D)->ShowWindow(SW_SHOW); //yulee 20181008
					GetDlgItem(IDC_BTN_CAN_SELECTSET_A_CLR)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_BTN_CAN_SELECTSET_B_CLR)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_BTN_CAN_SELECTSET_C_CLR)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_BTN_CAN_SELECTSET_D_CLR)->ShowWindow(SW_SHOW); //yulee 20181008
					GetDlgItem(IDC_CAN_SELECT_SET_LIST_A)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_CAN_SELECT_SET_LIST_B)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_CAN_SELECT_SET_LIST_C)->ShowWindow(SW_SHOW); //yulee 20180902
					GetDlgItem(IDC_CAN_SELECT_SET_LIST_D)->ShowWindow(SW_SHOW); //yulee 20181008*/
					int index = m_ctrlYAxisList.GetItemCount();
					for (int i = index-1; i >= 0; i--)
					{
						if (m_ctrlYAxisList.GetCheck(i))
						{
							CString strSelect;
							strSelect = m_ctrlYAxisList.GetItemText(i, 0);
							
							//CAN가 체크되면 선택된 작업결과 DATA에 포함된 해당 Aux를 모두 화면에 그려준다.////
							UpdateCanList(RS_COL_CAN);
						}
					}
				}
				
//------------------------------------------------------
				i++;
				if (record)
				{
					// m_ChannelListCtrl 에만 보여지고 지우려고 만들었다???
					delete record;
				}
//------------------------------------------------------

			}
		}	//  end of if (afinder.IsDirectory() && !afinder.IsDots())
	} // end of while (bWork)

	pProgressWnd->SetPos(100);
	pProgressWnd->Hide();
	delete pProgressWnd;
	pProgressWnd = NULL;

 	m_lToCycle = iMaxCycle;
	str.Format("%d", m_lToCycle);
	GetDlgItem(IDC_CYCLE_TO_EDIT)->SetWindowText(str);
// 	SortChannelListCtrl();	//표기된 채널 리스트 목록을 sort 시킨다.
// 	OnAddSelCycleBtn();		//cycle List 추가 
	
	//20100430
	UpdateStepNoList();
}

int CALLBACK CDataQueryDialog::ChannelCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;

   if(pListCtrl->GetItemCount()<2) return 0;

   CString strItem1 = pListCtrl->GetItemText(lParam1, 0);
   CString strItem2 = pListCtrl->GetItemText(lParam2, 0);

   int result = strcmp(strItem1, strItem2);
   if(result==0)
   {
		strItem1 = pListCtrl->GetItemText(lParam1, 1);
		strItem2 = pListCtrl->GetItemText(lParam2, 1);
		result = strcmp(strItem1, strItem2);
   }

   return result;
}

void CDataQueryDialog::SortChannelListCtrl()
{
	
/*	for (int i=0;i < m_ChannelListCtrl.GetItemCount();i++)
	{
	   m_ChannelListCtrl.SetItemData(i, i);
	}
*/	m_ChannelListCtrl.SortItems(ChannelCompareProc, (LPARAM)(&m_ChannelListCtrl));

	//모두 선택 상태로 만든다.
	for (int i = 0; i < m_ChannelListCtrl.GetItemCount(); i++)
	{
		m_ChannelListCtrl.SetItemState(i,LVIS_SELECTED,LVIS_SELECTED);
	}
}

#include "SearchOptionDlg.h"
void CDataQueryDialog::OnTestFromSearch()
{

		LVCOLUMN col;
		col.mask = LVCF_TEXT;
		char szTitle[32];
		col.pszText = szTitle;
		col.cchTextMax = 31;
		if (m_ChannelListCtrl.GetColumn(2, &col))
		{
			sprintf(szTitle, "lee");
			m_ChannelListCtrl.SetColumn(2, &col);
		}
	
//	m_ChannelListCtrl.SetColumn(3, "lee",   LVCFMT_LEFT, 50, 2);
	
	return;

	CString strSearchKey;
	GetDlgItemText(IDC_EDIT_SEARCH_KEY, strSearchKey);

	if( strSearchKey.IsEmpty() == FALSE )
	{
		CSelTestDialog aDlg;
		aDlg.m_byMode = CSelTestDialog::MODE_SEARCH;
		aDlg.m_pDatabase = m_pDatabase;
		aDlg.m_strSearchKey = strSearchKey;
		//
		if(aDlg.DoModal()==IDOK)
		{
			POSITION pos = aDlg.m_TestPathList.GetHeadPosition();
//			int aa = aDlg.m_TestPathList.GetCount();
//			if(aa > 0)	aa--;
			int aa = 0;

			while(pos)
			{
				CString strPath = aDlg.m_TestPathList.GetNext(pos);
				if(m_strlistTestPath.Find(strPath)==NULL)
				{
					m_strlistTestPath.AddTail(strPath);
					AddTestPath(strPath, aa++);
					OnAddSelCycleBtn();
				}
			}
			SortChannelListCtrl();

		}
	}
	else
	{
		OnTestFromListBtn();
	}
}

void CDataQueryDialog::OnItemchangedChannelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	int a = m_ChannelListCtrl.GetSelectedCount();
	CString msg;
	msg.Format(Fun_FindMsg("DataQueryDialog_OnItemchangedChannelList_msg1","IDD_DATA_QUERY_DIALOG"), a);
	GetDlgItem(IDC_SEL_COUNT_STATIC)->SetWindowText(msg);
	*pResult = 0;
}

//사용자가 선택한 축의 설정을 저장한다.
void CDataQueryDialog::WriteRegUserSet()
{
	//축 선택 저장 
	int iCurSel = m_XAxisCombo.GetCurSel();
	if(iCurSel != CB_ERR)
	{
		CString strDefaultXName;
		m_XAxisCombo.GetLBText(iCurSel, strDefaultXName);
		AfxGetApp()->WriteProfileString(QUERY_REG_SECTION, "X Axis", strDefaultXName);

		//y축 설정 저장
		CString strTemp, strMsg;
		m_XUnitCombo.GetLBText(m_XUnitCombo.GetCurSel(), strTemp);
		strMsg.Format("%s,", strTemp);
		
		//for(int i = 0; i < m_YAxisList.GetCount(); i++)
		for(int i = 0; i < m_ctrlYAxisList.GetItemCount(); i++)
		{
			//if(m_YAxisList.GetCheck(i))
			if(m_ctrlYAxisList.GetCheck(i))
			{
				strTemp.Format("%d,", i);
				strMsg += strTemp;
			}
		}
		strMsg += ":";
	
		POSITION pos = m_YAxisPropertyIndex.GetHeadPosition();
		while(pos)
		{
			CPoint pt = m_YAxisPropertyIndex.GetNext(pos);
			strTemp.Format("%d,", pt.y);
			strMsg += strTemp;
		}
		//Write Aux Info
		strMsg += ":";

		for(int i = 0 ; i < m_ctrlAuxList.GetCount(); i++)
		{
			if(m_ctrlAuxList.GetCheck(i))
			{
				strTemp.Format("%d,", i);
				strMsg += strTemp;
			}
		}

		AfxGetApp()->WriteProfileString(QUERY_REG_SECTION, strDefaultXName, strMsg);
	}
}

void CDataQueryDialog::UpdateAuxList(CString strAuxName)
{
	CFileFind afinder;
	POSITION pos = m_strlistTestPath.GetHeadPosition();
	//lyj 20201012 세인 s ====================
	int nChannelSel = m_ChannelListCtrl.GetSelectedCount();
	bool bChSel = FALSE;
	if (nChannelSel == 1)
	{
		bChSel = TRUE;
	}
	POSITION pos1;
	int nItem;
	int nIndex;;
	CString strChPath;
	//lyj 20201012 세인 e ====================

	while(pos)
	{
		CString strPath = m_strlistTestPath.GetAt(pos);
		CString str;
		str.Format("%s\\%s", strPath, FILE_FIND_FORMAT);

		BOOL bWork = afinder.FindFile(str);
		while(bWork)
		{
			bWork          = afinder.FindNextFile();
		
			if(afinder.IsDirectory()&&!afinder.IsDots())
			{
				long lLastTableSize = 0;	//ljb 20101228

				// Added by 224 (2014/02/03) :
				CChData chData(afinder.GetFilePath()) ;
				
				CTable *record = CChData::GetLastDataRecord(afinder.GetFilePath(), lLastTableSize, &chData);
				int nAuxCount = record->LoadAuxData(afinder.GetFilePath()); //2014.11.10 nAuxCount수정
				if(record == NULL) return;
				CString strTemp = afinder.GetFilePath();
				
				//lyj 20201012 세인 s ====================
				if(bChSel)
				{
					pos1 = m_ChannelListCtrl.GetFirstSelectedItemPosition();
					nItem     = m_ChannelListCtrl.GetNextSelectedItem(pos1);
					nIndex = m_ChannelListCtrl.GetItemData(nItem);
					strChPath = m_strlistTestPath.GetAt(m_strlistTestPath.FindIndex(nIndex))+"\\"+m_ChannelListCtrl.GetItemText(nItem,1);

					if(strChPath.CompareNoCase(strTemp) !=0 ) continue;

				}
				//lyj 20201012 세인 e ====================

				CString strDataName = strTemp.Mid(strTemp.ReverseFind('\\')+1, 7);				

				for(int i = 0 ; i < nAuxCount; i++)	 //2014.11.10 nAuxCount수정
				{
					CString strTitle = record->GetAuxTitleOfIndex(i);
					//if(strTitle.Mid(0, strTitle.Find('(')) == strAuxName)
					bool bStrCmp = FALSE;
					if(strTitle.Find(RS_COL_AUX_TEMPTH)>-1)
					{
						if(strTitle.Mid(5,5) == strAuxName.Left(5))
							bStrCmp = TRUE;
						else 
							bStrCmp = FALSE;
					}
					//ksj 20200116 : v1016 습도 추가
					if(strTitle.Find(RS_COL_AUX_HUMI)>-1)
					{
						if(strTitle.Mid(5,4) == strAuxName.Left(9))
							bStrCmp = TRUE;
						else 
							bStrCmp = FALSE;
					}
					//ksj end
					else if(strAuxName.CompareNoCase(RS_COL_AUX_TEMPTH) != 0)
					{
						if(strTitle.Mid(5,4) == RS_COL_AUX_TEMP)
						{
							if(strTitle.Mid(5,4) == strAuxName.Left(4))
								bStrCmp = TRUE;
							else 
								bStrCmp = FALSE;
						}
						else if(strTitle.Mid(5,4) == RS_COL_AUX_VOLT)
						{
							if(strTitle.Mid(5,4) == strAuxName.Left(4))
								bStrCmp = TRUE;
							else 
								bStrCmp = FALSE;
						}


// 						if(strTitle.Mid(5,4) == strAuxName.Left(4))
// 							bStrCmp = TRUE;
// 						else 
// 							bStrCmp = FALSE;
					}
					//if(strTitle.Left(4) == strAuxName.Left(4))	//ljb 201105 AuxV
					if(bStrCmp)	//20180608 yulee 
					{
						//strTemp.Format("%s[%s]", strTitle, strDataName);						
						//m_ctrlAuxList.AddString(strTemp);

						CString strItemName;
						BOOL bFindItem = FALSE;
						for(int nIndex = 0; nIndex < m_ctrlAuxList.GetCount(); nIndex++)
						{
							m_ctrlAuxList.GetText(nIndex, strItemName);
							if(strItemName == strTitle)
								bFindItem = TRUE;
						}
						if(bFindItem==FALSE)
							m_ctrlAuxList.AddString(strTitle);
					}
				}
				delete record;
			}
		}
		m_strlistTestPath.GetNext(pos);
	}

	//Aux 기본 설정
	for(int i=0; i<yAuxArray.GetSize(); i++)
	{
		m_ctrlAuxList.SetCheck(yAuxArray[i], 1);
	}

}

void CDataQueryDialog::UpdateCanList(CString strCanName)
{
	CFileFind afinder;
	POSITION pos = m_strlistTestPath.GetHeadPosition();
	while(pos)
	{
		CString strPath = m_strlistTestPath.GetAt(pos);
		CString str;
		str.Format("%s\\%s", strPath, FILE_FIND_FORMAT);
		
		BOOL bWork = afinder.FindFile(str);
		while(bWork)
		{
			bWork          = afinder.FindNextFile();
			
			if(afinder.IsDirectory()&&!afinder.IsDots())
			{
				long lLastTableSize = 0;	//ljb 20101228
				
				// Added by 224 (2014/02/03) :
				CChData chData(afinder.GetFilePath()) ;
				CTable *record = CChData::GetLastDataRecord(afinder.GetFilePath(), lLastTableSize, &chData);
				if(record == NULL) return;
				CString strTemp = afinder.GetFilePath();
				CString strDataName = strTemp.Mid(strTemp.ReverseFind('\\')+1, 7);
				record->LoadCanData(afinder.GetFilePath());

				for(int i = 0 ; i < record->GetCanColumnCount(); i++)	
				{
					CString strTitle = record->GetCanTitleOfIndex(i);
					if (strTitle.Find("$") > 0) continue;
//					if(strTitle.Left(8) == strCanName)
					{
						//strTemp.Format("%s[%s]", strTitle, strDataName);						
						//m_ctrlCanList.AddString(strTemp); 
						
						CString strItemName;
						BOOL bFindItem = FALSE;
						for(int nIndex = 0; nIndex < m_ctrlCanList.GetCount(); nIndex++)
						{
							m_ctrlCanList.GetText(nIndex, strItemName);
							if(strItemName.Left(8) == strTitle.Left(8))
							//if(strItemName.Compare(strTitle) == 0) // 20210803 hhs 추가 : strItemName과 strTitle 문자열 비교
								bFindItem = TRUE; // strItemName과 strTitle 문자열이 같으면 bFindItem = TRUE
						}
						if(bFindItem==FALSE) // 위의 for문에서 같은 문자열 없을 시
						{
							m_ctrlCanList.AddString(strTitle); // strTitle 문자열을 CAN 리스트에 추가
							// 							strTemp = strTitle.Mid(0, strTitle.Find('['));
							// 							m_ctrlCanList.AddString(strTemp);
						}
					}
				}
				delete record;
			}
			break;	//ljb 2011215 이재복 //////////
		}
//		m_strlistTestPath.GetNext(pos);
		break;
	}
	
	//can 기본 설정
// 	for(int i=0; i<yAuxArray.GetSize(); i++)
// 	{
// 		m_ctrlCanList.SetCheck(yAuxArray[i], 1);
// 	}
	//m_ctrlCanList.AddString("ljb");
}

void CDataQueryDialog::OnSelchangeYAxisList() 
{
	int iCurSel = m_YAxisList.GetCurSel();
	if(iCurSel!=LB_ERR)
	{
		TRACE("GetCheck : %d\r\n",m_YAxisList.GetCheck(iCurSel));
		if(nAuxListCheck == m_YAxisList.GetCheck(iCurSel))
			return;
		//Item이 선택되지 않았을시 
		m_YUnitCombo.ResetContent();
		//Item이 선택되었으면
		if(1 == m_YAxisList.GetCheck(iCurSel))
		{
			nAuxListCheck = 1;
			POSITION pos = m_YAxisPropertyIndex.FindIndex(iCurSel);
			CPoint pt    = m_YAxisPropertyIndex.GetAt(pos);
			BYTE index   = (BYTE) pt.x;

			//
			CDaoRecordset aRecordset(m_pDatabase);
			CString strQuery;
			strQuery.Format("SELECT UnitNotation FROM Property WHERE Index = %d", index);
			aRecordset.Open(dbOpenDynaset, strQuery);
				while(!aRecordset.IsEOF())
				{
					COleVariant var;
					aRecordset.GetFieldValue("UnitNotation", var);
					m_YUnitCombo.AddString((LPCTSTR)(LPTSTR)var.bstrVal);
					aRecordset.MoveNext();
				}
			aRecordset.Close();

			//
			m_YUnitCombo.SetCurSel(pt.y);

			CString strSelect;
			m_YAxisList.GetText(iCurSel, strSelect);

			//Aux가 체크되면 선택된 작업결과 DATA에 포함된 해당 Aux를 모두 화면에 그려준다.////
//20130215 remove
			if(strSelect == RS_COL_AUX_TEMP)
			{
				UpdateAuxList(RS_COL_AUX_TEMP);
			}
			else if(strSelect == RS_COL_AUX_VOLT)
			{
				UpdateAuxList(RS_COL_AUX_VOLT);
			}
			else if(strSelect == RS_COL_AUX_TEMPTH)
			{
				UpdateAuxList(RS_COL_AUX_TEMPTH);
			}
			else if(strSelect == RS_COL_AUX_HUMI)
			{
				UpdateAuxList(RS_COL_AUX_HUMI);
			}

		///////////////////////////////////////////////////////////////////////////////////
		}
		else
		{
			nAuxListCheck = 0;
			CString strSelect;
			m_YAxisList.GetText(iCurSel, strSelect);

			//Aux가 체크되면 선택된 작업결과 DATA에 포함된 해당 Aux를 모두 화면에 그려준다.////
//ljb
			if(strSelect == RS_COL_AUX_TEMP)
			{
				CString strItem;
				for(int i = m_ctrlAuxList.GetCount() -1; i >= 0; i--)
				{
					 m_ctrlAuxList.GetText(i, strItem);
					 if(strItem.Left(strItem.Find('[')) == RS_COL_AUX_TEMP)
						 m_ctrlAuxList.DeleteString(i);
				}
			}
			else if(strSelect == RS_COL_AUX_VOLT)
			{
				CString strItem;
				for(int i = m_ctrlAuxList.GetCount() -1; i >= 0; i--)
				{
					 m_ctrlAuxList.GetText(i, strItem);
					 if(strItem.Left(strItem.Find('[')) == RS_COL_AUX_VOLT)
						 m_ctrlAuxList.DeleteString(i);
				}
			}
			//ljb 2011223 이재복 //////////
			else if(strSelect == RS_COL_CAN)
			{
				CString strItem;
				for(int i = m_ctrlCanList.GetCount() -1; i >= 0; i--)
				{
					m_ctrlCanList.GetText(i, strItem);
					if(strItem.Left(strItem.Find('[')) == RS_COL_CAN)
						m_ctrlCanList.DeleteString(i);
				}
			}
		}

	
	}
}

void CDataQueryDialog::OnItemchangedYAxisCtrlList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	int iCurSel = pNMListView->iItem;

	//Item 체크 버튼이 바뀌었는지를 확인한다.
	//메시지가 1~3번 날라오기 때문에 변경되지 않은 경우는 리턴 시킨다.
	if (pNMListView->uOldState == 0 && pNMListView->uNewState == 0)
		return;	// No change

	BOOL bPrevState = (BOOL)(((pNMListView->uOldState &
				LVIS_STATEIMAGEMASK)>>12)-1);   // Old check box state
	if (bPrevState < 0)	// On startup there's no previous state 
		bPrevState = 0; // so assign as false (unchecked)

	// New check box state
	BOOL bChecked=(BOOL)(((pNMListView->uNewState & LVIS_STATEIMAGEMASK)>>12)-1);
	if (bChecked < 0) // On non-checkbox notifications assume false
		bChecked = 0;	

	
	
	if(iCurSel!=LB_ERR)
	{
		//Item이 선택되지 않았을시 
		m_YUnitCombo.ResetContent();
	
		//Item이 선택되었으면
		if(1 == m_ctrlYAxisList.GetCheck(iCurSel))
		{
			POSITION pos = m_YAxisPropertyIndex.FindIndex(iCurSel);
			CPoint pt    = m_YAxisPropertyIndex.GetAt(pos);
			BYTE index   = (BYTE) pt.x;

			//
			CDaoRecordset aRecordset(m_pDatabase);
			CString strQuery;
			strQuery.Format("SELECT UnitNotation FROM Property WHERE Index = %d", index);
			aRecordset.Open(dbOpenDynaset, strQuery);
			while(!aRecordset.IsEOF())
			{
				COleVariant var;
				aRecordset.GetFieldValue("UnitNotation", var);
				m_YUnitCombo.AddString((LPCTSTR)(LPTSTR)var.bstrVal);
				aRecordset.MoveNext();
			}
			aRecordset.Close();

			//
			m_YUnitCombo.SetCurSel(pt.y);
			TRACE("SetCurSel : %d\n", pt.y);

			CString strSelect;
			strSelect = m_ctrlYAxisList.GetItemText(iCurSel, 0);

			if(strSelect == RS_COL_CAN)  //lyj 20200605 Y축 선택이 CAN이면 단위선택 안되게 끔 수정
			{
				m_YUnitCombo.ResetContent();
				m_YUnitCombo.SetCurSel(0);
			}

			if (bPrevState == bChecked) // No change in check box
				return;	

			//Aux가 체크되면 선택된 작업결과 DATA에 포함된 해당 Aux를 모두 화면에 그려준다.////
//20130215 remove
			if(strSelect == RS_COL_AUX_TEMP)
			{
				m_ctrlAuxList.ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_SELECT)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_UNSELECT)->ShowWindow(SW_SHOW);
				UpdateAuxList(RS_COL_AUX_TEMP);
			}
			else if(strSelect == RS_COL_AUX_VOLT)
			{
				m_ctrlAuxList.ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_SELECT)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_UNSELECT)->ShowWindow(SW_SHOW);
				UpdateAuxList(RS_COL_AUX_VOLT);
			}
			else if(strSelect == RS_COL_AUX_TEMPTH)
			{
				m_ctrlAuxList.ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_SELECT)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_UNSELECT)->ShowWindow(SW_SHOW);
				UpdateAuxList(RS_COL_AUX_TEMPTH);
			}
			//ksj 20200116 : v1016 습도 추가
			else if(strSelect == RS_COL_AUX_HUMI)
			{
				m_ctrlAuxList.ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_SELECT)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_UNSELECT)->ShowWindow(SW_SHOW);
				UpdateAuxList(RS_COL_AUX_HUMI);
			}
			//ksj end
			else if(strSelect == RS_COL_CAN)
			{
 				m_ctrlCanList.ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->ShowWindow(SW_SHOW); //yulee 20180803
 				GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->ShowWindow(SW_SHOW); //yulee 20181008
				GetDlgItem(IDC_BTN_CAN_SELECTSET_A)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_BTN_CAN_SELECTSET_B)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_BTN_CAN_SELECTSET_C)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_BTN_CAN_SELECTSET_D)->ShowWindow(SW_SHOW); //yulee 20181008
				GetDlgItem(IDC_BTN_CAN_SELECTSET_A_CLR)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_BTN_CAN_SELECTSET_B_CLR)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_BTN_CAN_SELECTSET_C_CLR)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_BTN_CAN_SELECTSET_D_CLR)->ShowWindow(SW_SHOW); //yulee 20181008
				GetDlgItem(IDC_CAN_SELECT_SET_LIST_A)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_CAN_SELECT_SET_LIST_B)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_CAN_SELECT_SET_LIST_C)->ShowWindow(SW_SHOW); //yulee 20180902
				GetDlgItem(IDC_CAN_SELECT_SET_LIST_D)->ShowWindow(SW_SHOW); //yulee 20181008
 				UpdateCanList(RS_COL_CAN);
			}
			
		///////////////////////////////////////////////////////////////////////////////////
		}
		else
		{
			
			CString strSelect;
			strSelect = m_ctrlYAxisList.GetItemText(iCurSel, 0);

			if (bPrevState == bChecked) // No change in check box
				return;	

			//ljb
			if(strSelect == RS_COL_AUX_TEMP)
			{
				CString strItem;
				for(int i = m_ctrlAuxList.GetCount() -1; i >= 0; i--)
				{
					 m_ctrlAuxList.GetText(i, strItem);
//					 if(strItem.Left(strItem.Find('[')) == RS_COL_AUX_TEMP)
					 if((strItem.Mid(5,4) == RS_COL_AUX_TEMP) && (strItem.Find(RS_COL_AUX_TEMPTH) == -1))//lmh 20111012 //20180619 yulee
						 m_ctrlAuxList.DeleteString(i);
					 else
						 continue;
				}
			}
			else if(strSelect == RS_COL_AUX_VOLT)
			{
				CString strItem;
				for(int i = m_ctrlAuxList.GetCount() -1; i >= 0; i--)
				{
					 m_ctrlAuxList.GetText(i, strItem);
//					 if(strItem.Left(strItem.Find('[')) == RS_COL_AUX_VOLT)
					 if(strItem.Mid(5,4) == RS_COL_AUX_VOLT)//lmh 20111012
						 m_ctrlAuxList.DeleteString(i);
				}
			}
			else if(strSelect == RS_COL_AUX_TEMPTH)
			{
				CString strItem;
				for(int i = m_ctrlAuxList.GetCount() -1; i >= 0; i--)
				{
					m_ctrlAuxList.GetText(i, strItem);
					//if(strItem.Left(strItem.Find('[')) == RS_COL_AUX_VOLT)
					if(strItem.Find(RS_COL_AUX_TEMPTH) > -1)//lmh 20111012
						m_ctrlAuxList.DeleteString(i);
				}
			}
			//ksj 20200116 : v1016 습도 추가
			else if(strSelect == RS_COL_AUX_HUMI)
			{
				CString strItem;
				for(int i = m_ctrlAuxList.GetCount() -1; i >= 0; i--)
				{
					m_ctrlAuxList.GetText(i, strItem);
					if(strItem.Find(RS_COL_AUX_HUMI) > -1)
						m_ctrlAuxList.DeleteString(i);
				}
			}
			//ksj end

			//ljb 2011223 이재복 //////////
			else if(strSelect == RS_COL_CAN)
			{
				CString strItem;
				for(int i = m_ctrlCanList.GetCount() -1; i >= 0; i--)
				{
					m_ctrlCanList.GetText(i, strItem);
					if(strItem.Mid(strItem.Find(']'), strItem.GetLength()).Find(RS_COL_CAN) > -1)
						m_ctrlCanList.DeleteString(i);
				}
			}
		}

	
	}

	CString strTmp = ""; //yulee 20180729
	if(m_ctrlAuxList.GetCount() != 0)
		m_ctrlAuxList.GetText(0, strTmp);
	
	if(strTmp.IsEmpty())
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(TRUE);
	}
	strTmp = ""; 
	if(m_ctrlCanList.GetCount() != 0)
		m_ctrlCanList.GetText(0, strTmp);
	
	if(strTmp.IsEmpty())
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(FALSE); //yulee 20180803
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(FALSE); //yulee 20181008
		GetDlgItem(IDC_BTN_CAN_SELECTSET_A)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_B)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_C)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_D)->EnableWindow(FALSE); //yulee 20181008
		GetDlgItem(IDC_BTN_CAN_SELECTSET_A_CLR)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_B_CLR)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_C_CLR)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_D_CLR)->EnableWindow(FALSE); //yulee 20181008
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_A)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_B)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_C)->EnableWindow(FALSE); //yulee 20180902
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_D)->EnableWindow(FALSE); //yulee 20181008
	}
	else
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(TRUE);//yulee 20181008
		GetDlgItem(IDC_BTN_CAN_SELECTSET_A)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_B)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_C)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_D)->EnableWindow(TRUE); //yulee 20180908
		GetDlgItem(IDC_BTN_CAN_SELECTSET_A_CLR)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_B_CLR)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_C_CLR)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_BTN_CAN_SELECTSET_D_CLR)->EnableWindow(TRUE); //yulee 20180908
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_A)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_B)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_C)->EnableWindow(TRUE); //yulee 20180902
		GetDlgItem(IDC_CAN_SELECT_SET_LIST_D)->EnableWindow(TRUE); //yulee 20180908
	}

	*pResult = 0;
}

void CDataQueryDialog::OnBtnAllSelect() 
{
	BOOL bChk = TRUE;
	for(int i = 0 ; i < m_ctrlAuxList.GetCount(); i++)
	{
		if(m_ctrlAuxList.GetCheck(i) == 0)
			bChk = FALSE;
	}
	if(bChk == TRUE)
	{
		//AfxMessageBox("이미 모두 체크되어 있습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("DataQueryDialog_OnBtnAllSelect_msg1","IDD_DATA_QUERY_DIALOG"));//$1013//&&
		return;
	}

	for(int i = 0 ; i < m_ctrlAuxList.GetCount(); i++)
		m_ctrlAuxList.SetCheck(i, 1);
	
	GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(TRUE);
// 	if(bAuxListFlag)
// 	{
// 		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(TRUE);
// 		//GetDlgItem(IDC_BTN_ALL_SELECT)->SetWindowText("Aux 전체선택");
// 	}
// 	else
// 	{
// 		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(TRUE);
// 		//GetDlgItem(IDC_BTN_ALL_SELECT)->SetWindowText("Aux 전체해제");
// 	}
// 
// 	bAuxListFlag  = !bAuxListFlag;	
}

void CDataQueryDialog::OnBtnAllUnSelect() 
{
	BOOL bChk = FALSE;
	for(int i = 0 ; i < m_ctrlAuxList.GetCount(); i++)
	{
		if(m_ctrlAuxList.GetCheck(i) == 1)
			bChk = TRUE;
	}
	if(bChk == FALSE)
	{
		//AfxMessageBox("이미 모두 체크 해제되어 있습니다.");
		AfxMessageBox(Fun_FindMsg("DataQueryDialog_OnBtnAllSelect_msg1","IDD_DATA_QUERY_DIALOG"));//&&
		return;
	}

	for(int i = 0 ; i < m_ctrlAuxList.GetCount(); i++)
		m_ctrlAuxList.SetCheck(i, 0);
	
	GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(FALSE);
}

void CDataQueryDialog::OnButtonAll() 
{
	m_SelStepList.SelItemRange(TRUE, 0, m_SelStepList.GetCount());	
	
}

void CDataQueryDialog::OnButtonCancel() 
{
	m_SelStepList.SelItemRange(FALSE, 0, m_SelStepList.GetCount());
	
}

BOOL CDataQueryDialog::SetRangeList2(CDWordArray *pSelRangeArray)
{
	if (pSelRangeArray == NULL)
		return FALSE;
	
	CString str;
	DWORD start, End;
	if (pSelRangeArray->GetSize() > 0)
	{
// 		start = pSelRangeArray->GetAt(0);
// 		End   = pSelRangeArray->GetAt(0);

// 연속된 값은 "2~3:1" 형태로 보이고,
// 분리된 값은 따로 구간을 정한다.
		INT nPrevStart = -1 ;
		INT nSize = pSelRangeArray->GetSize() ;
		for (int nIdx = 0; nIdx < nSize; nIdx++)
		{
			INT Curr = pSelRangeArray->GetAt(nIdx);
			if (nIdx == 0 || pSelRangeArray->GetAt(nIdx-1)+1 != pSelRangeArray->GetAt(nIdx))
			{
				start = Curr ;

				str.Format("%d~%d:1", start, Curr);
				TRACE("%s\n", str);
				m_strlistSelCycle.AddTail(str);	//"2~3:1" 형태 구간 선택 String 입력 

				nPrevStart = Curr ;
			}
			else // if (nIdx != 0)
			{
//				if (pSelRangeArray->GetAt(nIdx-1) + 1 == pSelRangeArray->GetAt(nIdx))
				{
					m_strlistSelCycle.RemoveTail();

					str.Format("%d~%d:1", nPrevStart, Curr);
					TRACE("%s\n", str);
					m_strlistSelCycle.AddTail(str);	//"2~3:1" 형태 구간 선택 String 입력 
				}
			}
		}

// 		str.Format("%d~%d:1", start, End);
// 		TRACE("%s\n", str);
//		m_strlistSelCycle.AddTail(str);	//"2~3:1" 형태 구간 선택 String 입력 
		
		return TRUE;
	}
	
	return FALSE;
}

#if (1==0)
//Range 선택 dialog 이전에 선택 구간 미리 지정시
BOOL CDataQueryDialog::SetRangeList(CDWordArray *pSelRangeArray)
{
	if (pSelRangeArray == NULL)
		return FALSE;
	
	CString str;
	DWORD start, End;
	if(pSelRangeArray->GetSize() > 0)
	{
		start = pSelRangeArray->GetAt(0);
		End   = pSelRangeArray->GetAt(0);
		
		for (int a = 1; a < pSelRangeArray->GetSize(); a++)
		{
			if ((End+1) == pSelRangeArray->GetAt(a))
			{
				End = pSelRangeArray->GetAt(a);
			}
			else
			{
				str.Format("%d~%d:1", start+1, End+1);
				TRACE("%s\n", str);
				m_strlistSelCycle.AddTail(str);	//"2~3:1" 형태 구간 선택 String 입력 
				
				start = pSelRangeArray->GetAt(a);
				End = pSelRangeArray->GetAt(a);
			}
		}

		str.Format("%d~%d:1", start+1, End+1);
		TRACE("%s\n", str);
		m_strlistSelCycle.AddTail(str);	//"2~3:1" 형태 구간 선택 String 입력 
		
		return TRUE;
	}

	return FALSE;
}
#endif

void CDataQueryDialog::UpdateStepNoList()
{
	//Update Step type select
	UpdateStepTypeSelect();
	m_SelStepList.ResetContent();
	
	//조건이 다른 시험을 동시에 선택 하였음
	CScheduleData schData;
	int nTotStep = 0;
	if (m_strlistTestPath.GetCount() > 1)
	{
		//MAX Step  번호를 display 한다.
		CString strPath;
		POSITION pos = m_strlistTestPath.GetHeadPosition();
		while (pos)
		{
			strPath = m_strlistTestPath.GetNext(pos);
			int nStepCnt = GetFistTestSchedule(strPath, &schData);
			if(nTotStep < nStepCnt)
			{
				nTotStep = nStepCnt;
			}
		}
		
		CList <LONG, LONG&>     listStepNo;    // StepNo List
		m_pQueryCondition->GetStepNoList(&listStepNo);
		CString str;
		for (int s = 0; s<nTotStep; s++)
		{
			LONG stepNo = s+1;
			//str.Format("%03d :: 다중선택", stepNo);
			str.Format(Fun_FindMsg("DataQueryDialog_UpdateStepNoList_msg1","IDD_DATA_QUERY_DIALOG"), stepNo);//&&
			m_SelStepList.AddString(str);
			m_SelStepList.SetItemData(s, s+1);
			
			if(listStepNo.Find(stepNo) || listStepNo.GetCount() < 1)
			{
				m_SelStepList.SetSel(s);
			}
		}

		if (m_SelStepList.GetCount() > 0)
		{
			m_SelStepList.SetCaretIndex(0);
		}
		//////////////////////////////////////////////////////////////////////////
	}
	else if (m_strlistTestPath.GetCount() == 1)	//조건이 같은 시험을 Loading 하였음
	{
		nTotStep = GetFistTestSchedule(m_strlistTestPath.GetAt(m_strlistTestPath.GetHeadPosition()), &schData);
		
		CList <LONG, LONG&>     listStepNo;    // StepNo List
		m_pQueryCondition->GetStepNoList(&listStepNo);		//기존에 Select list
		CString str;
		int selcnt = 0;
		for (int s = 0; s < nTotStep; s++)
		{
			CStep *pStep = schData.GetStepData(s);
			LONG stepNo = pStep->m_StepIndex+1;
			str.Format("%03d :: %s", stepNo, CScheduleData::GetTypeString(pStep->m_type));
			
			//선택 Step 만 Display
			if (m_iMode & (0x01<<pStep->m_type))
			{
				m_SelStepList.AddString(str);
				m_SelStepList.SetItemData(selcnt++, stepNo);
				
				if (listStepNo.Find(stepNo) || listStepNo.GetCount() < 1)
				{
					m_SelStepList.SetSel(selcnt-1);
					TRACE("Select StepNo %d\n", stepNo);
				}
			}
		}

		if (m_SelStepList.GetCount() > 0)
		{
			m_SelStepList.SetCaretIndex(0);
		}
		//////////////////////////////////////////////////////////////////////////
	}
}

int CDataQueryDialog::GetFistTestSchedule(CString strTestPath, CScheduleData *pScheule) 
{
	int nStepMaxNo = 0;
	CFileFind afinder, aSchfinder;
	CString str;
	str.Format("%s\\%s", strTestPath, FILE_FIND_FORMAT);
	
	BOOL bWork = afinder.FindFile(str);
	while(bWork)
	{
		bWork          = afinder.FindNextFile();
		if(afinder.IsDirectory() && !afinder.IsDots())
		{
			str.Format("%s\\*.%s", afinder.GetFilePath(), PS_SCHEDULE_FILE_NAME_EXT);
			
			BOOL bSchedule = aSchfinder.FindFile(str);
			if(bSchedule)
			{
				bSchedule     = aSchfinder.FindNextFile();
				str = aSchfinder.GetFilePath();
				if(pScheule)
				{
					pScheule->ResetData();
					pScheule->SetSchedule(str);
					nStepMaxNo = pScheule->GetStepSize();
				}
			}
		}
	}
	return nStepMaxNo;
}

int CDataQueryDialog::UpdateStepTypeSelect()
{
	m_iMode = 0;
	int nModeCnt = 0;
// 	if(((CButton*)GetDlgItem(IDC_CHARGE_CHECK))->GetCheck()==1)	
// 	{
// 		m_iMode|= (0x01 << PS_STEP_CHARGE);
// 		nModeCnt++;
// 	}
// 	if(((CButton*)GetDlgItem(IDC_DISCHARGE_CHECK))->GetCheck()==1)
// 	{
// 		m_iMode|= (0x01 << PS_STEP_DISCHARGE);
// 		nModeCnt++;
// 	}
// 	if(((CButton*)GetDlgItem(IDC_REST_CHECK))->GetCheck()==1)
// 	{
// 		m_iMode|= (0x01 << PS_STEP_REST);
// 		nModeCnt++;
// 	}
// 	if(((CButton*)GetDlgItem(IDC_IMP_CHECK))->GetCheck()==1)
// 	{
// 		m_iMode|= (0x01 << PS_STEP_IMPEDANCE);
// 		nModeCnt++;
// 	}
// 	if(((CButton*)GetDlgItem(IDC_PATTERN_CHECK))->GetCheck()==1)
// 	{
// 		m_iMode|= (0x01 << PS_STEP_PATTERN);
// 		nModeCnt++;
// 	}
	if(m_chkCharge)	
	{
		m_iMode |= (0x01 << PS_STEP_CHARGE);
		nModeCnt++;
	}
	if(m_chkDisCharge)
	{
		m_iMode |= (0x01 << PS_STEP_DISCHARGE);
		nModeCnt++;
	}
	if(m_chkRest)
	{
		m_iMode |= (0x01 << PS_STEP_REST);
		nModeCnt++;
	}
	if(m_chkImpedance)
	{
		m_iMode |= (0x01 << PS_STEP_IMPEDANCE);
		nModeCnt++;
	}
	if(m_chkPattern)
	{
		m_iMode |= (0x01 << PS_STEP_PATTERN);
		nModeCnt++;
	}
	if(m_chkExtCAN)
	{
		m_iMode |= (0x01 << PS_STEP_EXT_CAN);
		nModeCnt++;
	}
	return nModeCnt;
}

void CDataQueryDialog::OnChargeCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateStepNoList();	
	m_SelStepList.SelItemRange(TRUE, 0, m_SelStepList.GetCount());
	
}

void CDataQueryDialog::OnDischargeCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateStepNoList();	
	m_SelStepList.SelItemRange(TRUE, 0, m_SelStepList.GetCount());
	
}

void CDataQueryDialog::OnRestCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateStepNoList();	
	m_SelStepList.SelItemRange(TRUE, 0, m_SelStepList.GetCount());
	
}

void CDataQueryDialog::OnImpCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateStepNoList();	
	m_SelStepList.SelItemRange(TRUE, 0, m_SelStepList.GetCount());
	
}

void CDataQueryDialog::OnPatternCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateStepNoList();	
	m_SelStepList.SelItemRange(TRUE, 0, m_SelStepList.GetCount());
	
}

// nFrom, nTo 는 1 베이스값이다. (1~2568)
// 
INT CDataQueryDialog::GetNumberOfStepEndData(CString strChPath, INT nFrom, INT nTo, INT nQueryMode)
{
	INT nRows = 0 ;

	CFileFind afinder;
	FILE* fp;
	CString strTemp;
	
    int FileSize = 0;	
    char* rltBuffer = NULL;

	try
	{
		// Commented by 224(2013/12/21) : 
		// cts - step end
		if (afinder.FindFile(strChPath + "\\*." + PS_RESULT_FILE_NAME_EXT))
		{
			afinder.FindNextFile();
			strTemp = afinder.GetFilePath();
			
			int nCtsFileVer = GetCtsFileVersion(strTemp); //ksj 20210326

			//lmh cts file open
			fp = fopen( strTemp, "rb" );			
			if (!fp)
			{
				throw "File Not Found!";			
			}
			
			// Commented by 224(2013/12/21) :
			// afinder 는 재활용. *.cts 파일은 하나만 있다고 본다.ㄴ
			// 없으면 exception 처리 됨

			FileSize = filelength( fileno(fp) );
			rltBuffer = new char [FileSize+1];			
			
			// Commented by 224(2013/12/21) : 
			// cts 파일을 rltBuffer 로 복사
			fread( rltBuffer, FileSize, 1, fp );			
			*(rltBuffer + FileSize) = 0;			
			fclose( fp );
			afinder.Close();
			
			///////////////////////////////////////////////////////////////////////////////
			if(nCtsFileVer == PS_FILE_VERSION_9) //v1013
			{
				PS_TEST_RESULT_FILE_HEADER_v13 header ;
				int nHdrSize = sizeof(PS_TEST_RESULT_FILE_HEADER_v13);
				memcpy(&header, rltBuffer, nHdrSize);

				// Commented by 224 (2013/12/21) :
				// ctc 파일의 레코드 수 = nTableCount
				int nTableCount = (FileSize - nHdrSize) / sizeof(PS_STEP_END_RECORD) + 1 ;

				long nStepNo;
				WORD type;

				// Modified by 224 (2013/12/30) : 갯수 이상으로 수정
				//			int nRow = 0;
				int nIdx = nHdrSize ;

				ASSERT(nFrom > 0) ;
				int nRecSize = sizeof(PS_STEP_END_RECORD);

				//			int totCycle = 0;
				if (nQueryMode == QUERY_MODE_INDEX)
				{
					nIdx = nIdx + sizeof(PS_STEP_END_RECORD) * (nFrom-1) ;
					int nRow = nFrom-1 ;

					while (nIdx < FileSize && nRow < nTo)
					{

						PS_STEP_END_RECORD record ;
						memcpy(&record, &rltBuffer[nIdx], nRecSize);

						//					totCycle = record.nTotalCycleNum;
						type     = record.chStepType;
						nStepNo  = record.chStepNo;

						nRows += (record.nIndexTo - record.nIndexFrom) + 1;
						//---------------------------------------------------

						nRow++;
						//				nIdx += (record.nIndexTo - record.nIndexFrom) + 1;
						nIdx += nRecSize ;
					}

				}
				else
				{
					// Added by 224 (2014/02/05) : Cycle 인 경우 처리 추가
					// Cycle로 검색한 경우는 처음부터 해당 Cycle 을 만날때 까지 순환해야
					// 한다.
					nIdx = nIdx + sizeof(PS_STEP_END_RECORD) * (0) ;
					INT nTableIdx = 0 ;
					INT nCycle = 0 ;
					BOOL bFirst = TRUE ;
					while (nIdx < FileSize && nCycle < nTo+1)
					{

						PS_STEP_END_RECORD record ;
						memcpy(&record, &rltBuffer[nIdx], nRecSize);

						// One base 인덱스로 한다.
						nTableIdx++ ;

						// 다음 Record 포인터로 이동
						nIdx += nRecSize ;

						// 레코드의 사이클이 구간내에 있는지 확인
						nCycle = record.nTotalCycleNum ;
						if (nCycle < nFrom)
						{
							continue ;
						}

						if (bFirst)
						{
							m_lFromTable = nTableIdx ;
							bFirst = !bFirst ;
						}

						if (nCycle > nTo)
						{
							break ;
						}

						// 처음 레코드의 인덱스를 보관
						//					totCycle = record.nTotalCycleNum;
						type     = record.chStepType;
						nStepNo  = record.chStepNo;

						nRows += (record.nIndexTo - record.nIndexFrom) + 1;
						//---------------------------------------------------

						// 마지막 cycle 값의 인덱스를 보관
						m_lToTable = nTableIdx ;
					}

					CFileFind finder ;
					if (finder.FindFile(strChPath + "\\*.rp$"))
					{
						m_lToTable++ ;
					}
				}

				if (rltBuffer != NULL)
				{
					delete rltBuffer;
					rltBuffer = NULL;
				}

			}
			else //if(nCtsFileVer == PS_FILE_VERSION_7) //v1015~
			{
				PS_TEST_RESULT_FILE_HEADER header ;
				int nHdrSize = sizeof(PS_TEST_RESULT_FILE_HEADER);
				memcpy(&header, rltBuffer, nHdrSize);

				// Commented by 224 (2013/12/21) :
				// ctc 파일의 레코드 수 = nTableCount
				int nTableCount = (FileSize - nHdrSize) / sizeof(PS_STEP_END_RECORD) + 1 ;

				long nStepNo;
				WORD type;

				// Modified by 224 (2013/12/30) : 갯수 이상으로 수정
				//			int nRow = 0;
				int nIdx = nHdrSize ;

				ASSERT(nFrom > 0) ;
				int nRecSize = sizeof(PS_STEP_END_RECORD);

				//			int totCycle = 0;
				if (nQueryMode == QUERY_MODE_INDEX)
				{
					nIdx = nIdx + sizeof(PS_STEP_END_RECORD) * (nFrom-1) ;
					int nRow = nFrom-1 ;

					while (nIdx < FileSize && nRow < nTo)
					{

						PS_STEP_END_RECORD record ;
						memcpy(&record, &rltBuffer[nIdx], nRecSize);

						//					totCycle = record.nTotalCycleNum;
						type     = record.chStepType;
						nStepNo  = record.chStepNo;

						nRows += (record.nIndexTo - record.nIndexFrom) + 1;
						//---------------------------------------------------

						nRow++;
						//				nIdx += (record.nIndexTo - record.nIndexFrom) + 1;
						nIdx += nRecSize ;
					}

				}
				else
				{
					// Added by 224 (2014/02/05) : Cycle 인 경우 처리 추가
					// Cycle로 검색한 경우는 처음부터 해당 Cycle 을 만날때 까지 순환해야
					// 한다.
					nIdx = nIdx + sizeof(PS_STEP_END_RECORD) * (0) ;
					INT nTableIdx = 0 ;
					INT nCycle = 0 ;
					BOOL bFirst = TRUE ;
					while (nIdx < FileSize && nCycle < nTo+1)
					{

						PS_STEP_END_RECORD record ;
						memcpy(&record, &rltBuffer[nIdx], nRecSize);

						// One base 인덱스로 한다.
						nTableIdx++ ;

						// 다음 Record 포인터로 이동
						nIdx += nRecSize ;

						// 레코드의 사이클이 구간내에 있는지 확인
						nCycle = record.nTotalCycleNum ;
						if (nCycle < nFrom)
						{
							continue ;
						}

						if (bFirst)
						{
							m_lFromTable = nTableIdx ;
							bFirst = !bFirst ;
						}

						if (nCycle > nTo)
						{
							break ;
						}

						// 처음 레코드의 인덱스를 보관
						//					totCycle = record.nTotalCycleNum;
						type     = record.chStepType;
						nStepNo  = record.chStepNo;

						nRows += (record.nIndexTo - record.nIndexFrom) + 1;
						//---------------------------------------------------

						// 마지막 cycle 값의 인덱스를 보관
						m_lToTable = nTableIdx ;
					}

					CFileFind finder ;
					if (finder.FindFile(strChPath + "\\*.rp$"))
					{
						m_lToTable++ ;
					}
				}

				if (rltBuffer != NULL)
				{
					delete rltBuffer;
					rltBuffer = NULL;
				}

			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
	}
	catch (char* pMsg)
	{
		TRACE("ErrorMsg = %s\n",pMsg);
	}

	return nRows;
}

// Added by 224 (2013/12/27) : 최대 허용 ROW 갯수 설정
void CDataQueryDialog::OnChangeEditMaxrow() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString str ;
	CString strRead;
	int nMaxrow;
	GetDlgItem(IDC_EDIT_MAXROW)->GetWindowText(str) ;

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.03.20 레지스트리의 MAXROW 를 변경하면 그래프 시 최대 값을 변경할 수 있다.
	// Added by 224 (2014/01/23)
	//if (atoi(str) > 65536) 

	strRead = AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "MAXROW", "1000000");
	nMaxrow = atoi(strRead);
	if (atoi(str) > nMaxrow)	
	{
		str.Format("%d",nMaxrow);
		//str = "65536" ;
		GetDlgItem(IDC_EDIT_MAXROW)->SetWindowText(str) ;
	}
	// 굳이 안해도 될듯...
	//AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "MAXROW", str) ;
	// -
	//////////////////////////////////////////////////////////////////////////

	str = AddComma(atoi(str)) ;
	str += " 건" ;//$1013
	GetDlgItem(IDC_STATIC_MAXROW)->SetWindowText(str) ;
}


void CDataQueryDialog::OnBtnAllCanSelect() 
{
	BOOL bChk = TRUE;
	for(int i = 0 ; i < m_ctrlCanList.GetCount(); i++)
	{
		if(m_ctrlCanList.GetCheck(i) == 0)
			bChk = FALSE;
	}
	if(bChk == TRUE)
	{
		//AfxMessageBox("이미 모두 체크되어 있습니다.");
		AfxMessageBox(Fun_FindMsg("DataQueryDialog_OnBtnAllSelect_msg1","IDD_DATA_QUERY_DIALOG"));//&&
		return;
	}
	
	for(int i = 0 ; i < m_ctrlCanList.GetCount(); i++)
		m_ctrlCanList.SetCheck(i, 1);
	
	GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(TRUE);



// 	for(int i = 0 ; i < m_ctrlCanList.GetCount(); i++)
// 		m_ctrlCanList.SetCheck(i, !bCanListFlag);
// 	
// 	if(bCanListFlag)
// 		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->SetWindowText("CAN 전체선택"); //yulee 20180803
// 	else
// 		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->SetWindowText("CAN 전체해제");
// 	
// 	bCanListFlag  = !bCanListFlag;	
}

void CDataQueryDialog::OnBtnAllCanUnSelect() 
{
	BOOL bChk = FALSE;
	for(int i = 0 ; i < m_ctrlCanList.GetCount(); i++)
	{
		if(m_ctrlCanList.GetCheck(i) == 1)
			bChk = TRUE;
	}
	if(bChk == FALSE)
	{
		//AfxMessageBox("이미 모두 체크 해제되어 있습니다.");
		AfxMessageBox(Fun_FindMsg("DataQueryDialog_OnBtnAllSelect_msg1","IDD_DATA_QUERY_DIALOG"));//&&
		return;
	}
	
	for(int i = 0 ; i < m_ctrlCanList.GetCount(); i++)
		m_ctrlCanList.SetCheck(i, 0);
	
	GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(FALSE);
}

void CDataQueryDialog::OnBtnCanSelectsetA() //yulee 20180902
{

	
	for(int i=0; i<m_ctrlCanList.GetCount(); i++)
	{
		m_bCheck[i] = (m_ctrlCanList.GetCheck(i));
	}


//	CCanSetSelectDlg dlg;
	int* pArrSelectedItem1;
	int  nSelectedCanSetNum;
	m_bBtnCANSetA = TRUE;
// 	if(IDOK == dlg.DoModal())
// 	{
	m_nSelectedItemNum = 0;
	pArrSelectedItem1 = GetSelectedItemArr();
	nSelectedCanSetNum = m_nSelectedItemNum;
//	}
//	else
//	{
//		return;
//	}
	m_bBtnCANSetA = FALSE;

	OnBtnCanSelectsetAClr();

	CString strItemName;
	int SetAState = -1;
	for(int nIndex = 0; nIndex < nSelectedCanSetNum; nIndex++)
	{
		CString tmpItemText;
		int tmpInt = pArrSelectedItem1[nIndex]-1;
		m_ctrlCanList.GetText(tmpInt, tmpItemText);

		tmpItemText.Insert(tmpItemText.Find(']')+4, "s1");
		m_ctrlCanListSetA.AddString(tmpItemText);

		//if tmpInt is in SetB or C, Release them from them
		for(int i=0; i<m_ctrlCanListSetB.GetCount(); i++)
		{
			CString tmpSetBItemText;
			m_ctrlCanListSetB.GetText(i, tmpSetBItemText);
			if(tmpItemText.CompareNoCase(tmpSetBItemText)==0)
			{
				m_ctrlCanListSetB.DeleteString(i);
			}
		}

		for(int i=0; i<m_ctrlCanListSetC.GetCount(); i++)
		{
			CString tmpSetCItemText;
			m_ctrlCanListSetC.GetText(i, tmpSetCItemText);
			if(tmpItemText.CompareNoCase(tmpSetCItemText)==0)
			{
				m_ctrlCanListSetC.DeleteString(i);
			}
		}	

		for(int i=0; i<m_ctrlCanListSetD.GetCount(); i++)
		{
			CString tmpSetDItemText;
			m_ctrlCanListSetD.GetText(i, tmpSetDItemText);
			if(tmpItemText.CompareNoCase(tmpSetDItemText)==0)
			{
				m_ctrlCanListSetD.DeleteString(i);
			}
		}
	}
}

int* CDataQueryDialog::GetSelectedItemArr()
{
	CString strItemName;
	for(int nIndex = 0; nIndex < m_ctrlCanList.GetCount(); nIndex++)
	{
		if(m_ctrlCanList.GetCheck(nIndex))
		{
			CString strTmpItemText;
			int tmpInt;
			m_ctrlCanList.GetText(nIndex, strTmpItemText);
			tmpInt = atoi(strTmpItemText.Mid(strTmpItemText.Find('[')+1,3));
			m_nSelectedItemArr[m_nSelectedItemNum] = tmpInt; // m_nSelectedItemNum = 0
			//m_nSelectedItemArr[m_nSelectedItemNum] = nIndex + 1; // 20210805 hhs 추가
			m_nSelectedItemNum++;
		}
	}
	return m_nSelectedItemArr;
}

void CDataQueryDialog::OnBtnCanSelectsetB() //yulee 20180902
{
	for(int i=0; i<m_ctrlCanList.GetCount(); i++)
	{
		m_bCheck[i] = (m_ctrlCanList.GetCheck(i));
	}
	
	//	CCanSetSelectDlg dlg;
	int* pArrSelectedItem2;
	int  nSelectedCanSetNum;
	m_bBtnCANSetB = TRUE;
	// 	if(IDOK == dlg.DoModal())
	// 	{
	m_nSelectedItemNum = 0;
	pArrSelectedItem2 = GetSelectedItemArr();
	nSelectedCanSetNum = m_nSelectedItemNum;
	//	}
	//	else
	//	{
	//		return;
//	}
// 	CCanSetSelectDlg dlg;
// 	int* pArrSelectedItem2;
// 	int  nSelectedCanSetNum;
// 	m_bBtnCANSetB = TRUE;
// 	if(IDOK == dlg.DoModal())
// 	{
// 		pArrSelectedItem2 = dlg.GetSelectedItemArr();
// 		nSelectedCanSetNum = dlg.GetSelectedItemNum();
// 	} 
// 	else
// 	{
// 		return;
// 	}
	m_bBtnCANSetB = FALSE;

	OnBtnCanSelectsetBClr();
	
	CString strItemName;
	for(int nIndex = 0; nIndex < nSelectedCanSetNum; nIndex++)
	{
		CString tmpItemText;
		int tmpInt = pArrSelectedItem2[nIndex]-1;
		m_ctrlCanList.GetText(tmpInt, tmpItemText);

		tmpItemText.Insert(tmpItemText.Find(']')+4, "s2");
		m_ctrlCanListSetB.AddString(tmpItemText);

		//if tmpInt is in SetB or C, Release them from them		
		
		for(int i=0; i<m_ctrlCanListSetA.GetCount(); i++)
		{
			CString tmpSetAItemText;
			m_ctrlCanListSetA.GetText(i, tmpSetAItemText);
			if(tmpItemText.CompareNoCase(tmpSetAItemText)==0)
			{
				m_ctrlCanListSetA.DeleteString(i);
			}
		}
		
		for(int i=0; i<m_ctrlCanListSetC.GetCount(); i++)
		{
			CString tmpSetCItemText;
			m_ctrlCanListSetC.GetText(i, tmpSetCItemText);
			if(tmpItemText.CompareNoCase(tmpSetCItemText)==0)
			{
				m_ctrlCanListSetC.DeleteString(i);
			}
		}

		for(int i=0; i<m_ctrlCanListSetD.GetCount(); i++)
		{
			CString tmpSetDItemText;
			m_ctrlCanListSetD.GetText(i, tmpSetDItemText);
			if(tmpItemText.CompareNoCase(tmpSetDItemText)==0)
			{
				m_ctrlCanListSetD.DeleteString(i);
			}
		}
	}
}

void CDataQueryDialog::OnBtnCanSelectsetC() //yulee 20180902
{
	for(int i=0; i<m_ctrlCanList.GetCount(); i++)
	{
		m_bCheck[i] = (m_ctrlCanList.GetCheck(i));
	}

	//	CCanSetSelectDlg dlg;
	int* pArrSelectedItem3;
	int  nSelectedCanSetNum;
	m_bBtnCANSetC = TRUE;
	m_nSelectedItemNum = 0;
	// 	if(IDOK == dlg.DoModal())
	// 	{
	pArrSelectedItem3 = GetSelectedItemArr();
	nSelectedCanSetNum = m_nSelectedItemNum;
	//	}
	//	else
	//	{
	//		return;
//	}
// 	CCanSetSelectDlg dlg;
// 	int* pArrSelectedItem3;
// 	int  nSelectedCanSetNum;
// 	m_bBtnCANSetC = TRUE;
// 	if(IDOK == dlg.DoModal())
// 	{
// 		pArrSelectedItem3 = dlg.GetSelectedItemArr();
// 		nSelectedCanSetNum = dlg.GetSelectedItemNum();
// 	}
// 	else
// 	{
// 		return;
// 	}
	m_bBtnCANSetC = FALSE;
	
	OnBtnCanSelectsetCClr();
	
	CString strItemName;
	for(int nIndex = 0; nIndex < nSelectedCanSetNum; nIndex++)
	{
		CString tmpItemText;
		int tmpInt = pArrSelectedItem3[nIndex]-1;
		m_ctrlCanList.GetText(tmpInt, tmpItemText);
	
		tmpItemText.Insert(tmpItemText.Find(']')+4, "s3");
		m_ctrlCanListSetC.AddString(tmpItemText);
		//if tmpInt is in SetB or C, Release them from them
		
		for(int i=0; i<m_ctrlCanListSetB.GetCount(); i++)
		{
			CString tmpSetBItemText;
			m_ctrlCanListSetB.GetText(i, tmpSetBItemText);
			if(tmpItemText.CompareNoCase(tmpSetBItemText)==0)
			{
				m_ctrlCanListSetB.DeleteString(i);
			}
		}
		
		for(int i=0; i<m_ctrlCanListSetA.GetCount(); i++)
		{
			CString tmpSetAItemText;
			m_ctrlCanListSetA.GetText(i, tmpSetAItemText);
			if(tmpItemText.CompareNoCase(tmpSetAItemText)==0)
			{
				m_ctrlCanListSetA.DeleteString(i);
			}
		}
		
		for(int i=0; i<m_ctrlCanListSetD.GetCount(); i++)
		{
			CString tmpSetDItemText;
			m_ctrlCanListSetD.GetText(i, tmpSetDItemText);
			if(tmpItemText.CompareNoCase(tmpSetDItemText)==0)
			{
				m_ctrlCanListSetD.DeleteString(i);
			}
		}
	}
}

void CDataQueryDialog::OnBtnCanSelectsetD() //yulee 201801008
{
	for(int i=0; i<m_ctrlCanList.GetCount(); i++)
	{
		m_bCheck[i] = (m_ctrlCanList.GetCheck(i));
	}
	
	//	CCanSetSelectDlg dlg;
	int* pArrSelectedItem4;
	int  nSelectedCanSetNum;
	m_bBtnCANSetD = TRUE;
	m_nSelectedItemNum = 0;
	// 	if(IDOK == dlg.DoModal())
	// 	{
	pArrSelectedItem4 = GetSelectedItemArr();
	nSelectedCanSetNum = m_nSelectedItemNum;
	//	}
	//	else
	//	{
	//		return;
	//	}
	// 	CCanSetSelectDlg dlg;
	// 	int* pArrSelectedItem3;
	// 	int  nSelectedCanSetNum;
	// 	m_bBtnCANSetC = TRUE;
	// 	if(IDOK == dlg.DoModal())
	// 	{
	// 		pArrSelectedItem3 = dlg.GetSelectedItemArr();
	// 		nSelectedCanSetNum = dlg.GetSelectedItemNum();
	// 	}
	// 	else
	// 	{
	// 		return;
	// 	}
	m_bBtnCANSetD = FALSE;
	
	OnBtnCanSelectsetDClr();
	
	CString strItemName;
	for(int nIndex = 0; nIndex < nSelectedCanSetNum; nIndex++)
	{
		CString tmpItemText;
		int tmpInt = pArrSelectedItem4[nIndex]-1;
		m_ctrlCanList.GetText(tmpInt, tmpItemText);
		
		tmpItemText.Insert(tmpItemText.Find(']')+4, "s4");
		m_ctrlCanListSetD.AddString(tmpItemText);
		//if tmpInt is in SetB or C, Release them from them
		
		for(int i=0; i<m_ctrlCanListSetA.GetCount(); i++)
		{
			CString tmpSetAItemText;
			m_ctrlCanListSetA.GetText(i, tmpSetAItemText);
			if(tmpItemText.CompareNoCase(tmpSetAItemText)==0)
			{
				m_ctrlCanListSetA.DeleteString(i);
			}
		}	
		
		for(int i=0; i<m_ctrlCanListSetB.GetCount(); i++)
		{
			CString tmpSetBItemText;
			m_ctrlCanListSetB.GetText(i, tmpSetBItemText);
			if(tmpItemText.CompareNoCase(tmpSetBItemText)==0)
			{
				m_ctrlCanListSetB.DeleteString(i);
			}
		}
		
		for(int i=0; i<m_ctrlCanListSetC.GetCount(); i++)
		{
			CString tmpSetCItemText;
			m_ctrlCanListSetC.GetText(i, tmpSetCItemText);
			if(tmpItemText.CompareNoCase(tmpSetCItemText)==0)
			{
				m_ctrlCanListSetC.DeleteString(i);
			}
		}
		
	}
}

void CDataQueryDialog::OnBtnCanSelectsetAClr()//yulee 20180902
{
	for(int i = m_ctrlCanListSetA.GetCount()*2 -1; i >= 0; i--)
	{
		m_ctrlCanListSetA.DeleteString(i);
	}
}
void CDataQueryDialog::OnBtnCanSelectsetBClr()//yulee 20180902
{
	for(int i = m_ctrlCanListSetB.GetCount()*2 -1; i >= 0; i--)
	{
		m_ctrlCanListSetB.DeleteString(i);
	}
}
void CDataQueryDialog::OnBtnCanSelectsetCClr()//yulee 20180902
{
	for(int i = m_ctrlCanListSetC.GetCount()*2 -1; i >= 0; i--)
	{
		m_ctrlCanListSetC.DeleteString(i);
	}
}
void CDataQueryDialog::OnBtnCanSelectsetDClr()//yulee 201801008
{
	for(int i = m_ctrlCanListSetD.GetCount()*2 -1; i >= 0; i--)
	{
		m_ctrlCanListSetD.DeleteString(i);
	}
}


void CDataQueryDialog::OnSelchangeCanSelectList() //yulee 20180905
{
	// TODO: Add your control notification handler code here	
	GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(TRUE);//yulee 20181008
	GetDlgItem(IDC_BTN_CAN_SELECTSET_A)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_BTN_CAN_SELECTSET_B)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_BTN_CAN_SELECTSET_C)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_BTN_CAN_SELECTSET_D)->EnableWindow(TRUE); //yulee 20180908
	GetDlgItem(IDC_BTN_CAN_SELECTSET_A_CLR)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_BTN_CAN_SELECTSET_B_CLR)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_BTN_CAN_SELECTSET_C_CLR)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_BTN_CAN_SELECTSET_D_CLR)->EnableWindow(TRUE); //yulee 20180908
	GetDlgItem(IDC_CAN_SELECT_SET_LIST_A)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_CAN_SELECT_SET_LIST_B)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_CAN_SELECT_SET_LIST_C)->EnableWindow(TRUE); //yulee 20180902
	GetDlgItem(IDC_CAN_SELECT_SET_LIST_D)->EnableWindow(TRUE); //yulee 20180908
	
	BOOL bChk = TRUE, bUnChk = FALSE;
	
	for(int i = 0 ; i < m_ctrlCanList.GetCount(); i++)
	{
		if(m_ctrlCanList.GetCheck(i) == 0)
			bChk = FALSE;
		if(m_ctrlCanList.GetCheck(i) == 1)
			bUnChk = TRUE;
	}
	
	if(bChk == TRUE)
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(TRUE);
		return;
	}
	if(bUnChk == FALSE)
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(FALSE);
		return;
	}
	
	if((bChk == FALSE)&&(bUnChk == TRUE))
	{
		GetDlgItem(IDC_BTN_ALL_CAN_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_CAN_UNSELECT)->EnableWindow(TRUE);
		return;
	}

	// 체크 상태가 바뀔 때 마다 Set A, B, C의 상태도 같이 바뀐다. 

	if((m_ctrlCanListSetA.GetCount() == 0)&&
		(m_ctrlCanListSetB.GetCount() == 0)&&
		(m_ctrlCanListSetC.GetCount() == 0))
		return;

	BOOL bCheckNew[256] = {0,};

	for(int i=0; i<m_ctrlCanList.GetCount(); i++)
	{
		bCheckNew[i] = (m_ctrlCanList.GetCheck(i));
	}

	for(int Index = 0; Index<m_ctrlCanList.GetCount(); Index++)
	{
		if((m_bCheck[Index] == 1) && (bCheckNew[Index] == 0))
		{
			CString tmpStrItem;
			m_ctrlCanList.GetText(Index, tmpStrItem);
			for(int j=0; j<m_ctrlCanListSetA.GetCount(); j++)
			{
				CString tmpStrSetItem, tmpStr;
				m_ctrlCanListSetA.GetText(j, tmpStrSetItem);
				tmpStr.Format("%s", tmpStrSetItem.Mid(tmpStrSetItem.Find('s')+2, 32));
				tmpStrSetItem = tmpStrSetItem.Left(8) + tmpStr;
				if(tmpStrItem.CompareNoCase(tmpStrSetItem)==0)
				{
					m_ctrlCanListSetA.DeleteString(j);
					break;
				}
			}
			for(int j=0; j<m_ctrlCanListSetB.GetCount(); j++)
			{
				CString tmpStrSetItem, tmpStr;
				m_ctrlCanListSetB.GetText(j, tmpStrSetItem);
				tmpStr.Format("%s", tmpStrSetItem.Mid(tmpStrSetItem.Find('s')+2, 32));
				tmpStrSetItem = tmpStrSetItem.Left(8) + tmpStr;
				if(tmpStrItem.CompareNoCase(tmpStrSetItem)==0)
				{
					m_ctrlCanListSetB.DeleteString(j);
					break;
				}
			}
			for(int j=0; j<m_ctrlCanListSetC.GetCount(); j++)
			{
				CString tmpStrSetItem, tmpStr;
				m_ctrlCanListSetC.GetText(j, tmpStrSetItem);
				tmpStr.Format("%s", tmpStrSetItem.Mid(tmpStrSetItem.Find('s')+2, 32));
				tmpStrSetItem = tmpStrSetItem.Left(8) + tmpStr;
				if(tmpStrItem.CompareNoCase(tmpStrSetItem)==0)
				{
					m_ctrlCanListSetC.DeleteString(j);
					break;
				}
			}
		}
	}
}

void CDataQueryDialog::OnSelchangeAuxSelectList() //yulee 20181008
{
	// TODO: Add your control notification handler code here

	BOOL bChk = TRUE, bUnChk = FALSE;

	for(int i = 0 ; i < m_ctrlAuxList.GetCount(); i++)
	{
		if(m_ctrlAuxList.GetCheck(i) == 0)
			bChk = FALSE;
		if(m_ctrlAuxList.GetCheck(i) == 1)
			bUnChk = TRUE;
	}

	if(bChk == TRUE)
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(TRUE);
		return;
	}
	if(bUnChk == FALSE)
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(FALSE);
		return;
	}
	if((bChk == FALSE)&&(bUnChk == TRUE))
	{
		GetDlgItem(IDC_BTN_ALL_SELECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ALL_UNSELECT)->EnableWindow(TRUE);
		return;
	}
}

//ksj 20210324 : CTS 파일 버전 정보 확인 함수 추가.
//해당 파일이 이미 오픈되어있으면 안될듯?
UINT CDataQueryDialog::GetCtsFileVersion(CString strCtsFilePath)
{
	CFile afile;	
	PS_FILE_ID_HEADER	fileHeader;
	if (afile.Open(strCtsFilePath, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
	{
		afile.Read(&fileHeader, sizeof(PS_FILE_ID_HEADER));
		if (fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
		{			
			afile.Close();
			return 0;
		}		

		afile.Close();
		return fileHeader.nFileVersion;
	}

	return 0;
}
