// DataQueryCdn.cpp: implementation of the CDataQueryCondition class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataQueryCdn.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDataQueryCondition::CDataQueryCondition()
{
	m_iOutputOption = 0;
	m_iStartOption = 0;
	m_bModify       = FALSE;

	init_Language();
//	m_dlgQuery.Create(IDD_DATA_QUERY_DIALOG);
}

CDataQueryCondition::~CDataQueryCondition()
{
	while(!m_YAxisList.IsEmpty())
	{
		CAxis* pAxis = m_YAxisList.RemoveTail();
		delete pAxis;
	}

	while(!m_YAuxAxisVoltList.IsEmpty())
	{
		CAxis* pAxis = m_YAuxAxisVoltList.RemoveTail();
		delete pAxis;
	}
	while(!m_YAuxAxisTempList.IsEmpty())
	{
		CAxis* pAxis = m_YAuxAxisTempList.RemoveTail();
		delete pAxis;
	}
	while(!m_YAuxAxisTempTHList.IsEmpty())
	{
		CAxis* pAxis = m_YAuxAxisTempTHList.RemoveTail();
		delete pAxis;
	}
	while(!m_YCanAxisList.IsEmpty())
	{
		CAxis* pAxis = m_YCanAxisList.RemoveTail();
		delete pAxis;
	}

}

BOOL CDataQueryCondition::GetUserSelection(CDWordArray *pdwSelRange, BOOL bQuertyIndexMode)
{
	//
	m_dlgQuery.SetQueryCondition(this);
	
	//20100430 Query mode settting
	//Query Mode가 Index일 경우는 m_llistCycle에 선택된 Table Index가 보관된다.
	if(pdwSelRange != NULL)
	{
		m_dlgQuery.SetQueryMode(bQuertyIndexMode);
//		m_dlgQuery.SetRangeList(pdwSelRange);
		m_dlgQuery.SetRangeList2(pdwSelRange);
	}
	//------------------
	
	if(IDOK == m_dlgQuery.DoModal()) 
	{
		m_strTestName = m_dlgQuery.m_strTestName;
		return TRUE;
	}
	
	return FALSE;
}

void CDataQueryCondition::ResetYAxisList()
{
	while(!m_YAxisList.IsEmpty())
	{
		CAxis* pAxis = m_YAxisList.RemoveTail();
		delete pAxis;
	}
}

BOOL CDataQueryCondition::UpdateCycleList(long* pCycle, int NumOfCycle)
{
	BOOL bChange = FALSE;

	if(m_llistCycle.GetCount()==NumOfCycle)
	{
		POSITION pos = m_llistCycle.GetHeadPosition();
		for(int i=0; i<NumOfCycle; i++)
		{
			if(pCycle[i]!=m_llistCycle.GetNext(pos))
			{
				bChange = TRUE;
				break;
			}
		}
	}
	else bChange = TRUE;

	if(bChange)
	{
		m_llistCycle.RemoveAll();
		for(int i=0; i<NumOfCycle; i++)
		{
			LONG cyc = pCycle[i];
			m_llistCycle.AddTail(cyc);
		}
	}

	return bChange;
}

//20100430 //update selected step no list
BOOL CDataQueryCondition::UpdateStepNoList(long* pStepNo, int NumOfStep)
{
	BOOL bChange = FALSE;
	
	if(m_llistStepNo.GetCount()==NumOfStep)
	{
		POSITION pos = m_llistStepNo.GetHeadPosition();
		for(int i=0; i<NumOfStep; i++)
		{
			if(pStepNo[i]!=m_llistStepNo.GetNext(pos))
			{
				bChange = TRUE;
				break;
			}
		}
	}
	else bChange = TRUE;
	
	if(bChange)
	{
		m_llistStepNo.RemoveAll();
		for(int i=0; i<NumOfStep; i++)
		{
			LONG cyc = pStepNo[i];
			m_llistStepNo.AddTail(cyc);
		}
	}
	
	return bChange;
}

//20100430
BOOL CDataQueryCondition::GetStepNoList(CList<LONG, LONG&>* pdwListStep)
{
	LONG lStepNo = 0;
	POSITION pos = m_llistStepNo.GetHeadPosition();
	while(pos)
	{
		lStepNo = m_llistStepNo.GetNext(pos);
		pdwListStep->AddTail(lStepNo);
	}
	return TRUE;
}

CAxis* CDataQueryCondition::IsThereThisYAxis(LPCTSTR strTitle)
{
	POSITION pos = m_YAxisList.GetHeadPosition();
	while(pos)
	{
		CAxis* pAxis = m_YAxisList.GetNext(pos);
		if(pAxis->GetPropertyTitle().CompareNoCase(strTitle)==0) return pAxis;
	}
	return NULL;
}

BOOL CDataQueryCondition::GetModeList(CList<DWORD, DWORD&>* pdwListMode, LPCTSTR strChPath, LPCTSTR strYAxisTitle)
{

	DWORD dwMode = 0;

	dwMode = m_iOutputOption;
	pdwListMode->AddTail(dwMode);
	return TRUE;
}

BOOL CDataQueryCondition::IsThereThisLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strYAxisTitle)
{
	BOOL bExit = FALSE;
	POSITION pos = m_YAxisList.GetHeadPosition();
	while(pos)
	{
		if(m_YAxisList.GetNext(pos)->GetTitle().CompareNoCase(strYAxisTitle)==0)
		{
			bExit = TRUE;
			break;
		}
	}
	if(!bExit) return FALSE;

	//
	if(m_strlistChPath.Find(strChPath) != NULL)
	{
		// X축이 Cycle인 경우,
/*		if (lCycle==0 && dwMode!=0)
		{
			switch(dwMode)
			{
			case MODE_CHARGE:
				if(((m_iOutputOption%100)/10)==0) return TRUE;
				if(((m_iOutputOption%100)/10)==1) return TRUE;
				break;
			case MODE_CHARGE_OPEN:
				if((CString(strYAxisTitle).CompareNoCase("IR")==0)&&
				   (CString(strYAxisTitle).CompareNoCase("OCV")==0))
				{
					if(((m_iOutputOption%100)/10)==0) return TRUE;
					if(((m_iOutputOption%100)/10)==1) return TRUE;
				}
				else
				{
					if((((m_iOutputOption%100)/10)==0)&&(m_iOutputOption%10==0)) return TRUE;
					if((((m_iOutputOption%100)/10)==1)&&(m_iOutputOption%10==0)) return TRUE;
				}
				break;
			case MODE_DISCHARGE:
				if(((m_iOutputOption%100)/10)==0) return TRUE;
				if(((m_iOutputOption%100)/10)==2) return TRUE;
				break;
			case MODE_DISCHARGE_OPEN:
				if((CString(strYAxisTitle).CompareNoCase("IR")==0)&&
				   (CString(strYAxisTitle).CompareNoCase("OCV")==0))
				{
					if(((m_iOutputOption%100)/10)==0) return TRUE;
					if(((m_iOutputOption%100)/10)==2) return TRUE;
				}
				else
				{
					if((((m_iOutputOption%100)/10)==0)&&(m_iOutputOption%10==0)) return TRUE;
					if((((m_iOutputOption%100)/10)==2)&&(m_iOutputOption%10==0)) return TRUE;
				}
				break;
			}
		}

		// X축이 Cycle이 아니고 Start Type이 "연속"인 경우,
		// 또는 X축이 Cycle이고 Y축이 Efficiency인 경우
		else if(lCycle == 0 && dwMode==0)
		{
			return TRUE;
		}
		// X축이 Cycle이 아니고 Start Type이 "영점시작"인 경우,
		
*/
		if(lCycle == 0)
		{
			TRACE("aa\n");
		}
		else //if(lCycle != 0)
		{
			if(m_llistCycle.Find(lCycle) != NULL)	return TRUE;
		}
	}
	return FALSE;
}

CString CDataQueryCondition::GetModeName(DWORD mode)
{
	CString strName;
	//lmh 20120726 충방전 한글표시
	if(mode&(0x01 << PS_STEP_CHARGE))		strName = PSGetTypeMsg(PS_STEP_CHARGE);
	if(mode&(0x01 << PS_STEP_DISCHARGE))	strName = PSGetTypeMsg(PS_STEP_DISCHARGE);
	if(mode&(0x01 << PS_STEP_REST))			strName = PSGetTypeMsg(PS_STEP_REST);
	if(mode&(0x01 << PS_STEP_IMPEDANCE))	strName = PSGetTypeMsg(PS_STEP_IMPEDANCE);
	if(mode&(0x01 << PS_STEP_PATTERN))		strName = PSGetTypeMsg(PS_STEP_PATTERN);
	return strName;
}

WORD CDataQueryCondition::GetVTMeasurePoint()
{
	return MAKEWORD(m_dlgQuery.m_byVoltagePoint, m_dlgQuery.m_byTemperaturePoint);
}

void CDataQueryCondition::ResetYAuxAxisList()
{
	while(!m_YAuxAxisVoltList.IsEmpty())
	{
		CAxis* pAxis = m_YAuxAxisVoltList.RemoveTail();
		delete pAxis;
	}
	while(!m_YAuxAxisTempList.IsEmpty())
	{
		CAxis* pAxis = m_YAuxAxisTempList.RemoveTail();
		delete pAxis;
	}
	while(!m_YAuxAxisTempTHList.IsEmpty())
	{
		CAxis* pAxis = m_YAuxAxisTempTHList.RemoveTail();
		delete pAxis;
	}
}

//ljb 2011215 이재복 //////////
void CDataQueryCondition::ResetYCanAxisList()
{
	while(!m_YCanAxisList.IsEmpty())
	{
		CAxis* pAxis = m_YCanAxisList.RemoveTail();
		delete pAxis;
	}

	while(!m_YAuxAxisCANS1List.IsEmpty()) //lyj 20200605 DATA 재설정 시 CAN 중복 그리기 방지
	{
		CAxis* pAxis = m_YAuxAxisCANS1List.RemoveTail();
		delete pAxis;
	}

	while(!m_YAuxAxisCANS2List.IsEmpty()) //lyj 20200605 DATA 재설정 시 CAN 중복 그리기 방지
	{
		CAxis* pAxis = m_YAuxAxisCANS2List.RemoveTail();
		delete pAxis;
	}

	while(!m_YAuxAxisCANS3List.IsEmpty()) //lyj 20200605 DATA 재설정 시 CAN 중복 그리기 방지
	{
		CAxis* pAxis = m_YAuxAxisCANS3List.RemoveTail();
		delete pAxis;
	}

	while(!m_YAuxAxisCANS4List.IsEmpty()) //lyj 20200605 DATA 재설정 시 CAN 중복 그리기 방지
	{
		CAxis* pAxis = m_YAuxAxisCANS4List.RemoveTail();
		delete pAxis;
	}

}

//20090911
DWORD CDataQueryCondition::GetModeFlag()
{
	return m_iOutputOption;
}
