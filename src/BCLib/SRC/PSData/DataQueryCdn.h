// DataQueryCdn.h: interface for the CDataQueryCondition class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATAQUERYCDN_H__90DAB180_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_DATAQUERYCDN_H__90DAB180_E17D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

#include "DataQueryDlg.h"	// Added by ClassView
#include "Axis.h"

class CDataQueryCondition  
{
////////////////
// Attributes //
////////////////
private:
	//20100430
	CList <LONG, LONG&>             m_llistStepNo;    // StepNo List

	CStringList                     m_strlistChPath; // List of Channel Path
	CList <LONG, LONG&>             m_llistCycle;    // Cycle List
	CAxis                           m_XAxis;         // Selected X-Axis
	CTypedPtrList<CPtrList, CAxis*> m_YAxisList;     // List of Selected Y-Axis
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisVoltList;     // List of Selected Y-AuxAxis
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisTempList;     // List of Selected Y-AuxAxis
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisTempTHList;   // List of Selected Y-AuxAxis 20180608 yulee
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisHumiList;   //ksj 20200206 : v1016 습도 센서
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisCANList;			// List of Selected Y-AuxAxis yulee 20180831-2
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisCANS1List;		// List of Selected Y-AuxAxis yulee 20180831-2
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisCANS2List;		// List of Selected Y-AuxAxis yulee 20180831-2
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisCANS3List;		// List of Selected Y-AuxAxis yulee 20180831-2
	CTypedPtrList<CPtrList, CAxis*> m_YAuxAxisCANS4List;		// List of Selected Y-AuxAxis yulee 20181009
	CTypedPtrList<CPtrList, CAxis*> m_YCanAxisList;     // List of Selected Y-CanAxis	//ljb 2011215 이재복 //////////

	int                             m_iOutputOption;	// Step Output Option
	int								m_iStartOption;		//Start Type
	BOOL                            m_bModify;       // Whether it is Modifying Mode
public:
	CDataQueryDialog                m_dlgQuery;      // Dialog Box where Data Querying Condition is getted.
	CString							m_strTestName;
///////////////////////////////////////////////////////////
// Operations: "Construction, Destruction, and Creation" //
///////////////////////////////////////////////////////////
public:

	CDataQueryCondition();
	virtual ~CDataQueryCondition();

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:
	// m_strlistChPath
	POSITION GetChPathHead() { return m_strlistChPath.GetHeadPosition(); };
	CString  GetNextChPath(POSITION& pos) { return m_strlistChPath.GetNext(pos); };
	void     ResetChPath() { m_strlistChPath.RemoveAll(); };
	void     AddChPath(LPCTSTR path) { m_strlistChPath.AddTail(path); };
	int      GetChPathCount() { return m_strlistChPath.GetCount(); };

	// m_llistCycle
	BOOL     UpdateCycleList(long* pCycle, int NumOfCycle);
	POSITION GetCycleHead() { return m_llistCycle.GetHeadPosition(); };
	LONG     GetNextCycle(POSITION& pos) { return m_llistCycle.GetNext(pos); };
	LONG     GetCycleCount() { return m_llistCycle.GetCount(); };

	// m_XAxisList
	CAxis*   GetXAxis() { return &m_XAxis; };

	// m_YAxisList
	void     ResetYAxisList();
	INT      GetYAxisCount() { return m_YAxisList.GetCount() ;}
	void     AddYAxisList(CAxis* pAxis) { m_YAxisList.AddTail(pAxis); };
	POSITION GetYAxisHead() { return m_YAxisList.GetHeadPosition(); };
	CAxis*   GetNextYAxis(POSITION& pos) { return m_YAxisList.GetNext(pos); };
	CAxis*   IsThereThisYAxis(LPCTSTR strTitle);

	//m_YAuxAxisList Volt	ljb 20131213 add
	void     AddYAuxAxisVoltList(CAxis* pAxis) { m_YAuxAxisVoltList.AddTail(pAxis); };
	POSITION GetYAuxAxisVoltHead() { return m_YAuxAxisVoltList.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisVolt(POSITION& pos) { return m_YAuxAxisVoltList.GetNext(pos); };

	//m_YAuxAxisList Temp	ljb 20131213 add
	void     AddYAuxAxisTempList(CAxis* pAxis) { m_YAuxAxisTempList.AddTail(pAxis); };
	POSITION GetYAuxAxisTempHead() { return m_YAuxAxisTempList.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisTemp(POSITION& pos) { return m_YAuxAxisTempList.GetNext(pos); };

	//m_YAuxAxisList Temp Thermistor	20180608 yulee add
	void     AddYAuxAxisTempTHList(CAxis* pAxis) { m_YAuxAxisTempTHList.AddTail(pAxis); };
	POSITION GetYAuxAxisTempTHHead() { return m_YAuxAxisTempTHList.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisTempTH(POSITION& pos) { return m_YAuxAxisTempTHList.GetNext(pos); };

	//ksj 20200206 : v1016 습도 센서 추가.
	void     AddYAuxAxisHumiList(CAxis* pAxis) { m_YAuxAxisHumiList.AddTail(pAxis); };
	POSITION GetYAuxAxisHumiHead() { return m_YAuxAxisHumiList.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisHumi(POSITION& pos) { return m_YAuxAxisHumiList.GetNext(pos); };

	//m_YAuxAxisList CAN yulee 20180831-2 add
	void     AddYAuxAxisCANList(CAxis* pAxis) { m_YAuxAxisCANList.AddTail(pAxis); };
	POSITION GetYAuxAxisCANHead() { return m_YAuxAxisCANList.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisCAN(POSITION& pos) { return m_YAuxAxisCANList.GetNext(pos); };

	//m_YAuxAxisList CAN yulee 20180831-2 add
	void     AddYAuxAxisCANS1List(CAxis* pAxis) { m_YAuxAxisCANS1List.AddTail(pAxis); };
	POSITION GetYAuxAxisCANS1Head() { return m_YAuxAxisCANS1List.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisCANS1(POSITION& pos) { return m_YAuxAxisCANS1List.GetNext(pos); };
	
	//m_YAuxAxisList CAN yulee 20180831-2 add
	void     AddYAuxAxisCANS2List(CAxis* pAxis) { m_YAuxAxisCANS2List.AddTail(pAxis); };
	POSITION GetYAuxAxisCANS2Head() { return m_YAuxAxisCANS2List.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisCANS2(POSITION& pos) { return m_YAuxAxisCANS2List.GetNext(pos); };

	//m_YAuxAxisList CAN yulee 20180831-2 add
	void     AddYAuxAxisCANS3List(CAxis* pAxis) { m_YAuxAxisCANS3List.AddTail(pAxis); };
	POSITION GetYAuxAxisCANS3Head() { return m_YAuxAxisCANS3List.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisCANS3(POSITION& pos) { return m_YAuxAxisCANS3List.GetNext(pos); };

	//m_YAuxAxisList CAN yulee 20181009 add
	void     AddYAuxAxisCANS4List(CAxis* pAxis) { m_YAuxAxisCANS4List.AddTail(pAxis); };
	POSITION GetYAuxAxisCANS4Head() { return m_YAuxAxisCANS4List.GetHeadPosition(); };
	CAxis*   GetNextYAuxAxisCANS4(POSITION& pos) { return m_YAuxAxisCANS4List.GetNext(pos); };

	//m_YCanAxisList
	void     AddYCanAxisList(CAxis* pAxis) { m_YCanAxisList.AddTail(pAxis); };	//ljb 2011215 이재복 //////////
	POSITION GetYCanAxisHead() { return m_YCanAxisList.GetHeadPosition(); };	//ljb 2011215 이재복 //////////
	CAxis*   GetNextYCanAxis(POSITION& pos) { return m_YCanAxisList.GetNext(pos); };		//ljb 2011215 이재복 //////////

	// m_iOutputOption
	void SetOutputOption(int iOption) { m_iOutputOption = iOption; };		//Step Type있는Flag
	int  GetOutputOption() { return m_iOutputOption; };

	// m_bModify
	BOOL IsModifyingMode() { return m_bModify; };
	void SetModifyingMode(BOOL bMode) { m_bModify = bMode; };

	//
	void	SetStartPointOption(int iOption)	{ m_iStartOption = iOption;	};
	int	GetStartPointOption()				{	return m_iStartOption;	};


///////////////////////////////////
// Operations: "Other functions" //
///////////////////////////////////
public:
	//20100430	---------
	BOOL UpdateStepNoList(long* pStepNo, int NumOfStep);
	BOOL GetStepNoList(CList<LONG, LONG&>* pdwListStep);
	//-------------------

	BOOL GetModeList(CList<DWORD, DWORD&>* pdwListMode,LPCTSTR strChPath, LPCTSTR strYAxisTitle);
	BOOL IsThereThisLine(LPCTSTR strChPath, LONG lCycle, DWORD dwMode, LPCTSTR strYAxisTitle);
	WORD GetVTMeasurePoint();
//	BOOL GetUserSelection();
	//20100430
	BOOL GetUserSelection(CDWordArray *pdwSelRange, BOOL bQuertyIndexMode = FALSE);

///////////////////////////////
// Operations: "Common used" //
///////////////////////////////
public:

	static CString GetModeName(DWORD mode);

//////////////////////////
// Enumerated Constants //
//////////////////////////
public:
	//20100430
	long							GetQueryMode()	{	return m_dlgQuery.GetQueryMode();	}
	DWORD GetModeFlag();

	void ResetYAuxAxisList();
	void ResetYCanAxisList();	//ljb 2011215 이재복 //////////

	enum{
		MODE_NONE           = 0,
		MODE_CHARGE         = 1,
		MODE_CHARGE_OPEN    = 2,
		MODE_DISCHARGE      = 3,
		MODE_DISCHARGE_OPEN = 4
	};

};

#endif // !defined(AFX_DATAQUERYCDN_H__90DAB180_E17D_11D4_88DF_006008CEDA07__INCLUDED_)
