#if !defined(AFX_RESULTTESTSELDLG_H__B227B318_5841_4C09_839B_BFA06026748C__INCLUDED_)
#define AFX_RESULTTESTSELDLG_H__B227B318_5841_4C09_839B_BFA06026748C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ResultTestSelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CResultTestSelDlg dialog

#include "resource.h"

#define FILE_FIND_FORMAT	"M??Ch??*"

class AFX_EXT_CLASS CResultTestSelDlg : public CDialog
{
// Construction
public:
	void SetTestList(CStringList *pstrList);
	CStringList * GetSelList();
	BOOL UpdateSelTestList();
	void Refresh();
	void InitTestListCtrl();
	CResultTestSelDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CResultTestSelDlg)
	enum { IDD = IDD_RESTULT_SEL_DLG , IDD2 = IDD_RESTULT_SEL_DLG_ENG };
	CListCtrl	m_ctrlTestList;
	CString	m_strFileFolder;
	CString m_strFindFile;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CResultTestSelDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CStringList m_strSelTestList;		//List of Full Path Test Name 
	BOOL LoadTestList();
	BOOL LoadSearchList();
	BOOL m_bDateSortToggle;
	BOOL m_bNameSortToggle;
//	CProgressWnd m_progressWnd;

private:
	static int CALLBACK TestNameASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK TestDateASCCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK TestNameDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK TestDateDECCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

	// Generated message map functions
	//{{AFX_MSG(CResultTestSelDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDataFolerSelButton();
	afx_msg void OnColumnclickResultTestList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkResultTestList(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESULTTESTSELDLG_H__B227B318_5841_4C09_839B_BFA06026748C__INCLUDED_)
