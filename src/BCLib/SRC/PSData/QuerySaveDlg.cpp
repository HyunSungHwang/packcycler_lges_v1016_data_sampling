// QuerySaveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "QuerySaveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CQuerySaveDlg dialog


CQuerySaveDlg::CQuerySaveDlg(ULONG ulMaxNumOfData, ULONG ulTotalNumOfData, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CQuerySaveDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CQuerySaveDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CQuerySaveDlg::IDD3):
	(CQuerySaveDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CQuerySaveDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	
	m_ulSaveInterval = 1;
	m_ulMaxNumOfData = ulMaxNumOfData;
	m_ulTotalNumOfData = ulTotalNumOfData;
	m_ulSavedNumOfResult = m_ulMaxNumOfData;
}


void CQuerySaveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CQuerySaveDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CQuerySaveDlg, CDialog)
	//{{AFX_MSG_MAP(CQuerySaveDlg)
	ON_EN_CHANGE(IDC_EDIT_INTERVAL, OnChangeEditInterval)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQuerySaveDlg message handlers

BOOL CQuerySaveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetDlgItemInt(IDC_EDIT_NUMBER, m_ulMaxNumOfData);
	SetDlgItemInt(IDC_EDIT_TOTAL, m_ulTotalNumOfData);
	SetDlgItemInt(IDC_EDIT_INTERVAL, m_ulSaveInterval);
	SetDlgItemInt(IDC_EDIT_EXPECT_NUM, m_ulSavedNumOfResult);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CQuerySaveDlg::OnChangeEditInterval() 
{
	m_ulSaveInterval = GetDlgItemInt(IDC_EDIT_INTERVAL);
	if(m_ulSaveInterval<=0 || m_ulSaveInterval>m_ulTotalNumOfData)
		return;
	
	m_ulSavedNumOfResult = m_ulTotalNumOfData/m_ulSaveInterval;
	SetDlgItemInt(IDC_EDIT_EXPECT_NUM, m_ulSavedNumOfResult);
}