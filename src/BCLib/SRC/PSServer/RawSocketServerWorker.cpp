/////////////////////////////////////////////////////////////////////
// Class Creator Version 2.0.000 Copyrigth (C) Poul A. Costinsky 1994
///////////////////////////////////////////////////////////////////
// Implementation File RawSocketServerWorker.cpp
// class CWizRawSocketServerWorker
//
// 16/07/1996 17:53                             Author: Poul
///////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RawSocketServerWorker.h"
#include "Module.h"

#ifdef _DEBUG
//#include "Mmsystem.h"
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__; 
#define new DEBUG_NEW
#endif

/*
//Message Direction
#define _HOST_TO_MODULE		0x00
#define _MODULE_TO_HOST		0x01

//#define CheckMsgDirection(x)	((x) == _MODULE_TO_HOST ? TRUE : FALSE)	
*/

extern CModule	*m_lpModule;
//extern BOOL	InitGroupData (int nModuleIndex);	//Module Data
extern BOOL WriteLog(char *szLog);



//Channel Mapping FTN
extern int	g_nChMappingType;	
extern int g_anChannelMap[SFT_MAX_CH_PER_MD];

// #ifdef _PARALLEL_FORCE 전처리기 주석처리
 static int g_iPType=0;  //yulee 20190705 강제병렬 옵션 처리 Mark //yulee 20190706
// #endif

void ModuleClosed (int nModuleIndex , HWND hMsgWnd, int nPort);
void ModuleConnected(int nModuleIndex, HWND hMsgWnd, int nPort);
void ModuleConnecting(int nModuleIndex, HWND hMsgWnd, int nProgress);

void ParsingCommand(CWizReadWriteSocket *pSock, int nGroupIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd);
void ParsingCommand2(CWizReadWriteSocket *pSock, int nGroupIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd);
BOOL ParsingCommandA(CWizReadWriteSocket *pSock, int nGroupIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd);

//int ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hShutDownEvent);
int ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hShutDownEvent, HWND hMsgWnd); //ksj 20201209 : 메세지 핸들 추가.
int ConnectionSequencePort2(CWizReadWriteSocket *pSocket, HANDLE *hShutDownEvent);

//void ProcessingMonitoringData(UINT nModuleIndex, void *pReceivedBuffer, int nSize, HWND hMsgWnd);
void ProcessingMonitoringVariableData(UINT nModuleIndex, void *pReceivedBuffer, int nSize, HWND hMsgWnd);
void ProcessingSaveVariableData(UINT nModuleIndex, void *pReceivedBuffer, int nSize, HWND hMsgWnd);			//v100E save


//BYTE GetNextPacketID();

///////////////////////////////////////////////////////////////////
//*****************************************************************
inline void ThrowIfNull(void* p)
{
	if (p == NULL)	AfxThrowMemoryException();
}

///////////////////////////////////////////////////////////////////
// class CWizRawSocketListener
///////////////////////////////////////////////////////////////////
//**********************************************************//
// Default Constructor										//
CWizRawSocketListener::CWizRawSocketListener(int nPort)     //
	: m_pListenSocket (NULL),								//
	  m_nPort         (nPort)								//
{															//
//	m_pDoc = NULL;
	m_nModuleIndex = 0;
	m_hMessageWnd = NULL;									//
}															//
//**********************************************************//
// Destructor												//
CWizRawSocketListener::~CWizRawSocketListener()				//
{															//
	if (m_pListenSocket != NULL)							//
	{														//
		TRACE("You Must call SFTCloseFormServer() before your application is closed\n");
		//ASSERT(0);											//
		delete m_pListenSocket;								//
	}														//
}															//
//**********************************************************//

void CWizRawSocketListener::SetMsgWnd(HWND hMessageWnd,int iPType) //yulee 20190706 iPtype추
{
	m_hMessageWnd = hMessageWnd;
	g_iPType=iPType;
}

// Method called from dispath thread.
void CWizRawSocketListener::Prepare ()
{
	// Create listening socket
	if (m_pListenSocket != NULL)
	{
		ASSERT(0);
		delete m_pListenSocket;
	}
	m_pListenSocket = new CWizSyncSocket(m_nPort);
	ThrowIfNull (m_pListenSocket);

#ifdef _DEBUG
	TCHAR buff[100];
	unsigned int nP;
	VERIFY(m_pListenSocket->GetHostName (buff,100,nP));
	TRACE(_T("Control Server Listening at %s:%d\n"), buff, nP);
#endif
}

//*****************************************************************
// Method called from dispath thread.
void CWizRawSocketListener::CleanUp()
{
	// close and destroy listening socket
	delete m_pListenSocket;
	m_pListenSocket = NULL;
}

//*****************************************************************
// Method called from dispath thread.
BOOL CWizRawSocketListener::WaitForData (HANDLE hShutDownEvent)
{
	HANDLE *lpHandles;
	lpHandles = new HANDLE[2];

	WSAEVENT myObjectEvent = WSACreateEvent();
	lpHandles[0]= myObjectEvent;
	lpHandles[1]= hShutDownEvent;

	WSAEventSelect( m_pListenSocket->H(), myObjectEvent, FD_ACCEPT);
	
	DWORD rtn;
	SOCKET h;

	while (1)
	{
		rtn = ::WaitForMultipleObjects(2, lpHandles, FALSE,   INFINITE);
		if( (rtn - WAIT_OBJECT_0) == 0 )
			 h = m_pListenSocket->Accept();
		else 
		{
			 WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return FALSE;
		}

		// Get accepted socket.	 If it's connected client, go to serve it.
		if (h != INVALID_SOCKET)
		{
//			WriteLog("A socket accepted");
			m_hAcceptedSocket = h;
			WSACloseEvent(myObjectEvent);
			delete [] lpHandles;
			return TRUE;
		}
	} // while 1
	return TRUE;
}

//*****************************************************************
// Method called from dispath thread.
// return TRUE : Wait clinet connection
// return FALSE:  Server Close
//****************************************************************
BOOL CWizRawSocketListener::TreatData   (HANDLE hShutDownEvent, HANDLE hDataTakenEvent)
{
	char	msg[128];								//Display Message Buffer
	UINT	nPortNo = 0;								//Receive Port No
	int		msgReadState  = _MSG_ST_CLEAR;			//Read Done Flag
	int		reqSize = 0, offset = 0, nRtn =0;		//Data Point Indicator

	LPSFT_MSG_HEADER	 lpMsgHeader;				//Message Header	
	lpMsgHeader = new SFT_MSG_HEADER;
	ASSERT(lpMsgHeader);

	LPVOID lpReadBuff = NULL;						//Body read Buffer
	int		nBuffSize = 0;							//size of buffer
	
	HANDLE 	*lpHandles;								//Handle
	if (m_nPort == _SFT_FROM_SCHEDULER_TCPIP_PORT1)
	{
		TRACE("connect port 1 \n");
		lpHandles = new HANDLE[3];
	}
	else
	{
		TRACE("connect port 2 \n");
//		lpHandles = new HANDLE[2];
		lpHandles = new HANDLE[3];		//ljb 20170914

	}
	ASSERT(lpHandles);

	INT	nModuleID = 0, nModuleIndex =0;			//Return Value, Module ID
	long nWaitCount = 0;
	
	// Create client side socket to communicate with client.
	CWizReadWriteSocket clientSide (m_hAcceptedSocket);
	// Signal dispather to continue waiting.
	::SetEvent(hDataTakenEvent);

	WSAEVENT myObjectEvent = WSACreateEvent();		//Event
	lpHandles[0] = myObjectEvent;					//Read 
	lpHandles[1] = hShutDownEvent;					//Server Side Shut down
	
	WSAEventSelect(clientSide.H(), myObjectEvent, FD_READ|FD_CLOSE);		//select Read and close event
	LPWSANETWORKEVENTS lpEvents = new WSANETWORKEVENTS;
	ASSERT(lpEvents);
	
	
	//Start Connection Sequence
	if (m_nPort == _SFT_FROM_SCHEDULER_TCPIP_PORT1)
	{	
		TRACE("connect 1 sequence \n");
		//if((nModuleIndex = ConnectionSequence(&clientSide, lpHandles)) < 0)
		if((nModuleIndex = ConnectionSequence(&clientSide, lpHandles, m_hMessageWnd)) < 0) //ksj 20201209 : 메세지 핸들 인자 추가.
		{
			sprintf(msg, "Module %d Connection Error. (Code %d)", nModuleID, nModuleIndex);
			goto _CLOSE_MODULE;
		}
		lpHandles[2] = m_lpModule[nModuleIndex].GetWriteEventHandle();
		nModuleID = m_lpModule[nModuleIndex].GetModuleID();
		//----------------Module Connection Complited-----------------------------//
		
		clientSide.GetPeerName(m_lpModule[nModuleIndex].m_szIpAddress, sizeof(m_lpModule[nModuleIndex].m_szIpAddress), nPortNo);	//Get Client Side Ip Address and Port Number
		sprintf(msg, "Module %d Connected From %s :: %d", nModuleID, m_lpModule[nModuleIndex].m_szIpAddress, nPortNo);
		//TRACE(msg)
		WriteLog(msg);
		
		ModuleConnected(nModuleIndex, m_hMessageWnd, m_nPort);	//Send Message to Main Wnd
	}
	else if (m_nPort == _SFT_FROM_SCHEDULER_TCPIP_PORT2)
	{
		//Port2 도 접속한 모듈 정보를 분석해서 모듈 아이디 계산한다.
		TRACE("connect 2 sequence \n");
		nModuleIndex=1;		//ljb 임시
		if((nModuleIndex = ConnectionSequencePort2(&clientSide, lpHandles)) < 0)
		{
			sprintf(msg, "Module %d Connection Error. (Code %d)", nModuleID, nModuleIndex);
			goto _CLOSE_MODULE;
		}
		lpHandles[2] = m_lpModule[nModuleIndex].GetWriteEventHandle2();
		sprintf(msg, "Module %d Port(%d) Number Error. (Code %d)", nModuleID, nPortNo, nModuleIndex);
	}
	else
	{
		sprintf(msg, "Module %d Port(%d) Number Error. (Code %d)", nModuleID, nPortNo, nModuleIndex);
		goto _CLOSE_MODULE;
	}


	//-----------------start Socket Read/Write--------------------------------//

	int idx;
	while(1)
	{
		if (m_nPort == _SFT_FROM_SCHEDULER_TCPIP_PORT1)
		{
			nRtn = ::WaitForMultipleObjects(3, lpHandles, FALSE, SFT_HEART_BEAT_TIMEOUT);		//Wait for Read or Write Event
		}
		else
		{	
			nRtn = ::WaitForMultipleObjects(3, lpHandles, FALSE, SFT_HEART_BEAT_TIMEOUT);		//Wait for Read or Write Event 20170914
			//nRtn = ::WaitForMultipleObjects(3, lpHandles, FALSE, INFINITE);		//ljb 20140116 Wait for Read or Write Event
		}
	
		if (nRtn == WAIT_FAILED)
		{
			sprintf(msg, "Module %d message read failed %d, Disconnect module %d", nModuleID, GetLastError(), nModuleID);
			goto _CLOSE_MODULE;
		}
		else if (nRtn == WAIT_TIMEOUT)
		{
			/*char aaa[50];
			if(!(m_lpModule->bSendScheduleEND)) //201912 LG 오창2공장 106호 15번 작업시작 안됨 이슈
			{
				nWaitCount++;
				sprintf(aaa, "3.Socket bSendScheduleEND = %i, nWaitCount = %i",m_lpModule->bSendScheduleEND,nWaitCount);
				WriteLog(aaa);
				sprintf(msg, "Module %d data read time out, Count %d", nModuleID, nWaitCount);
				if(nWaitCount > _SFT_MAX_WAIT_COUNT) //SFT_HEART_BEAT_TIMEOUT*_SFT_MAX_WAIT_COUNT
				{
					sprintf(msg, "Module %d Data Read Time Out. Disconnect Module %d", nModuleID, nModuleID);
					sprintf(msg, "goto _CLOSE HIDE");
					goto _CLOSE_MODULE;		//ljb 20140115 add
				}
			}*/


			//아래가 오리지널 코드
			/*nWaitCount++;
			sprintf(msg, "Module %d data read time out, Count %d", nModuleID, nWaitCount);
			if(nWaitCount > _SFT_MAX_WAIT_COUNT)
			{
				sprintf(msg, "Module %d Data Read Time Out. Disconnect Module %d", nModuleID, nModuleID);
				goto _CLOSE_MODULE;		//ljb 20140115 add
			}*/

			//ksj 20201215 : 스케쥴 전송중에는 timeout 예외처리////////////////////////////////////////////////////////////
			nWaitCount++;

			if(m_lpModule->GetSendScheduleState() == TRUE) //스케쥴 전송중일때 heartbit 체크
			{	
				sprintf(msg, "Module %d data read time out, Count %d [TYPE1]", nModuleID, nWaitCount);
				WriteLog(msg);
				//if(nWaitCount > _SFT_MAX_WAIT_COUNT) //8*3 = 24초까지 하트비트 없어도 접속 안끊고 기다림.
				if(nWaitCount > _SFT_MAX_WAIT_COUNT_SEND_SCHEDULE) //8*8 = 64초까지 하트비트 없어도 접속 안끊고 기다림. //lyj 20201217
				{
					sprintf(msg, "Module %d Data Read Time Out. Disconnect Module %d [TYPE1]", nModuleID, nModuleID);
					goto _CLOSE_MODULE;		//ljb 20140115 add
				}
			}
			else //시험 전송중이 아닐때 heartbit 체크
			{			
				sprintf(msg, "Module %d data read time out, Count %d [TYPE2]", nModuleID, nWaitCount);
				WriteLog(msg);
				//if(nWaitCount > _SFT_MAX_WAIT_COUNT_SEND_SCHEDULE) //8*8 = 64초까지 하트비트 없어도 접속 안끊고 기다림.
				if(nWaitCount > _SFT_MAX_WAIT_COUNT) //8*3 = 24초까지 하트비트 없어도 접속 안끊고 기다림.
				{
					sprintf(msg, "Module %d Data Read Time Out. Disconnect Module %d [TYPE2]", nModuleID, nModuleID);
					goto _CLOSE_MODULE;		//ljb 20140115 add
				}
			}
			//ksj end///////////////////////////////////////////////////////////////////////////////////////////////////////

		}
		else if (nRtn >= WAIT_ABANDONED_0 && nRtn < WAIT_ABANDONED_0 + 3)
		{
			idx = nRtn - WAIT_ABANDONED_0;
			ResetEvent(lpHandles[idx]);			
			sprintf(msg, "Module %d something error event detected %d. Disconnect module %d", nModuleID, nRtn, nModuleID);
			WriteLog(msg);		
			continue;
		}
		else
		{
			idx = nRtn -WAIT_OBJECT_0;			
			//작업 처리
			switch(nRtn)
			{
			case WAIT_OBJECT_0:		// Client socket event FD_READ
				if(SOCKET_ERROR == ::WSAEnumNetworkEvents(clientSide.H(), myObjectEvent, lpEvents))		//Get Client side Socket Event
				{ 
					sprintf(msg, "Module %d socket error. (Code %d)", nModuleID, WSAGetLastError());
					goto _CLOSE_MODULE;
				}	
				if (lpEvents->lNetworkEvents & FD_READ)		//Read Event
				{
					nWaitCount = 0;		//Reset Wait Count;
					//--------------------Message Head Read -----------------//
					if( msgReadState & (_MSG_ST_CLEAR | _MSG_ST_HEADER_REMAIN))				//Read Message Header
					{
						if(msgReadState & _MSG_ST_CLEAR)	reqSize = SizeofHeader();		
						offset = SizeofHeader() - reqSize;
						nRtn = clientSide.Read((char *)lpMsgHeader+offset, reqSize);		//Read Message Header
						
						if (nRtn != reqSize) 
						{
							if(nRtn == SOCKET_ERROR)	//Socket Error
							{
								msgReadState = _MSG_ST_CLEAR;
								sprintf(msg, "Module %d message header read fail. (Code %d)\n", nModuleID, WSAGetLastError());
								WriteLog(msg);
								break;
							}
							else
							{
								msgReadState = _MSG_ST_HEADER_REMAIN;
								TRACE("Module %d Message Header Data %d Byte Read %d Byte Remain\n", nModuleID, nRtn, reqSize);
							}
							reqSize -= nRtn;
						}
						else		//Read Done
						{	
							if(lpMsgHeader->nLength <= 0 )	//
							{
								msgReadState = _MSG_ST_CLEAR;
							}
							else
							{
								msgReadState = _MSG_ST_HEAD_READED;
							}
						}
					} 
					else	//--------------------Message Body Read -----------------////Read Body And Command Parsing	
					{
						if (msgReadState & _MSG_ST_HEAD_READED)
						{
							reqSize = lpMsgHeader->nLength;		
							//too long packet 
							if(reqSize > _SFT_MAX_PACKET_LENGTH)
							{
								msgReadState = _MSG_ST_CLEAR;
								sprintf(msg, "Module %d message packet length error. receive %d/Max %d\n", nModuleID, reqSize, _MSG_ST_CLEAR);
								WriteLog(msg);
								goto _CLOSE_MODULE;
								break;
							}
							
							//if size is mismatch then release preallocated memory
							if(nBuffSize != reqSize && reqSize > 0)
							{
								if(lpReadBuff)		delete[] lpReadBuff;		

								//new allocate memory
								lpReadBuff = new char[reqSize];
								ASSERT(lpReadBuff);
								nBuffSize = reqSize;
							}
						}

						offset = lpMsgHeader->nLength - reqSize;
						nRtn = clientSide.Read((char *)lpReadBuff+offset, reqSize);
						if (nRtn != reqSize) 
						{
							if(nRtn == SOCKET_ERROR)	//Socket Error
							{
								msgReadState = _MSG_ST_CLEAR;
								sprintf(msg, "Module %d message body read fail. (Code %d)\n", nModuleID, WSAGetLastError());
								WriteLog(msg);
								break;
							}
							else
							{
								msgReadState = _MSG_ST_REMAIN;	
							}
							reqSize -= nRtn;
						}
						else		//Read Done
						{	
							msgReadState = _MSG_ST_CLEAR;
						}
						
					}
					
					///---------------Parsing Command---------------------///
					if(	msgReadState & _MSG_ST_CLEAR)
					{
						//ljb PasingCommand 리턴값 없음으로 수정
						if (m_nPort == _SFT_FROM_SCHEDULER_TCPIP_PORT1)
						{
							ParsingCommand(&clientSide, nModuleIndex, lpMsgHeader, lpReadBuff, m_hMessageWnd);	//Parsing Recevied Command 				
						}
						else if (m_nPort == _SFT_FROM_SCHEDULER_TCPIP_PORT2)
						{
							//모듈번호를 알수가 없다. ㅡㅡ;
							ParsingCommand2(&clientSide, nModuleIndex, lpMsgHeader, lpReadBuff, m_hMessageWnd);	//Parsing Recevied Command 				
						}
					}
				}

				else if(lpEvents->lNetworkEvents == FD_CLOSE)		//Client Disconnected
				{
					sprintf(msg, "Disconnect event detected from Module %d.(Code %d)", nModuleID, lpEvents->lNetworkEvents);
					goto _CLOSE_MODULE;
				}
				else 
				{
					if (lpEvents->lNetworkEvents != 0) 
					{
						sprintf(msg, "Module %d UnKnown event %d detected. Disconnect Module %d", nModuleID, lpEvents->lNetworkEvents, nModuleID);
						goto _CLOSE_MODULE;
					}
				}				
			break;
			
			case WAIT_OBJECT_0 + 1: // Server side shutdown event
				{
					TRACE("===================>Write event\n");
					sprintf(msg, "Server shutdown event event detected. Disconnect Module %d", nModuleID);
					WriteLog(msg);
					clientSide.Close();
					if(lpReadBuff)
					{
						delete [] lpReadBuff;			lpReadBuff = NULL;
					}
					delete lpMsgHeader;
					delete [] lpHandles;
					delete lpEvents;
					return FALSE;			
				}
			
			case WAIT_OBJECT_0 + 2:	// client side write event
				{
					ASSERT(m_lpModule[nModuleIndex].m_nTxLength < _SFT_TX_BUF_LEN);
					nRtn = clientSide.Write(m_lpModule[nModuleIndex].m_szTxBuffer, m_lpModule[nModuleIndex].m_nTxLength);
					LPSFT_MSG_HEADER lpHeader = (LPSFT_MSG_HEADER)m_lpModule[nModuleIndex].m_szTxBuffer;
					sprintf(msg, "Send command 0x%08x to Module %d write %d byte,  %d/%d Byte ==>>", 
						lpHeader->nCommand, nModuleID, lpHeader->nLength, nRtn, m_lpModule[nModuleIndex].m_nTxLength);
					WriteLog(msg);
					#ifdef _DEBUG			
					TRACE(msg);
					int nSize1 = m_lpModule[nModuleIndex].m_nTxLength;
					int i;
					if(nSize1 > 40)
						nSize1 = 40;
					for(int i = 0 ; i > nSize1; i++)
						TRACE("%x ", m_lpModule[nModuleIndex].m_szTxBuffer[nSize1]);
					TRACE("\n");
					for(int i = 0 ; i > 40; i++)
						TRACE("%x ", m_lpModule[nModuleIndex].m_szTxBuffer[nSize1-i]);

					TRACE("\n============완료===========\n");
					#endif
					
					if(nRtn != m_lpModule[nModuleIndex].m_nTxLength)
					{
						sprintf(msg, "Module %d socket write error, %d Byte write - %d Byte remain", nModuleID, nRtn, m_lpModule[nModuleIndex].m_nTxLength - nRtn);
						WriteLog(msg);
						m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_SIZE_MISMATCH; //ljb 2013 가끔 write 실패 할때 처리
						m_lpModule[nModuleIndex].SetWriteEvent(); //ljb 2013 가끔 write 실패 할때 처리
					}					

					m_lpModule[nModuleIndex].ResetWriteEvent();
					m_lpModule[nModuleIndex].SetSyncWriteEvent();
					break;
				}			
			}	//switch
		}
	}	//while(1)

	_CLOSE_MODULE:				//Release Memory and close client socket

	WriteLog(msg);	
	//CleanUp(); //2014.08.12
	clientSide.Close();

	if (m_nPort == _SFT_FROM_SCHEDULER_TCPIP_PORT1) ModuleClosed(nModuleIndex, m_hMessageWnd,m_nPort);	//ljb 20171024 edit


	if(lpReadBuff)
	{
		delete [] lpReadBuff;		lpReadBuff = NULL;
	}
	delete lpMsgHeader;			lpMsgHeader = NULL;
	delete [] lpHandles;		lpHandles = NULL;
	delete lpEvents;			lpEvents = NULL;

	return TRUE;
}

void ModuleConnected(int nModuleIndex, HWND hMsgWnd, int nPort)
{
	if (nPort != _SFT_FROM_SCHEDULER_TCPIP_PORT1)
	{
		return;
	}
	if(m_lpModule == NULL || nModuleIndex < 0)	return ;					//Form Server is closed
	m_lpModule[nModuleIndex].SetState(PS_STATE_LINE_ON);					//Module Line On State
	
	for(int i = 0; i < m_lpModule[nModuleIndex].GetChannelCount(); i++)
	{
		CChannel * pChannel = m_lpModule[nModuleIndex].GetChannelData(i);
		pChannel->SetState(PS_STATE_LINE_ON);
		Sleep(300);	//ljb 201012 모듈 끊김
	}

	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_CONNECTED, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)m_lpModule[nModuleIndex].GetModuleSystem());		//Send to Parent Wnd to Module Conntected 

	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_STATE_CHANGE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)0);		//Send to Parent Wnd To Module State Change
}

void ModuleClosed (int nModuleIndex , HWND hMsgWnd, int nPort)
{
	if(m_lpModule == NULL || nModuleIndex < 0)	return ;		//Form Server is closed
	m_lpModule[nModuleIndex].SetState(PS_STATE_LINE_OFF);		//Module Line Off State
	m_lpModule[nModuleIndex].IsSyncWrite = FALSE;				//ljb 20150312 add 재접속을 위한 Heart bit 초기화
	
	for(int i = 0; i < m_lpModule[nModuleIndex].GetChannelCount(); i++)
	{
		CChannel * pChannel = m_lpModule[nModuleIndex].GetChannelData(i);
		pChannel->SetState(PS_STATE_LINE_OFF);		
		Sleep(300);	//ljb 201012 모듈 끊김 
	}
//	int nModuleID = m_lpModule[nModuleIndex].sysData.nModuleID;				//Get Module ID
	
	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_CLOSED, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)0);	//Send to Parent Wnd to Module Close Message

	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_STATE_CHANGE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)0);			//Send to Parent Wnd to Module State change
}

//ksj 20201209 : 모듈 접속중인 상태 통지 함수 추가.
void ModuleConnecting(int nModuleIndex, HWND hMsgWnd, int nProgress)
{
	if(m_lpModule == NULL || nModuleIndex < 0)	return ;


	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_CONNECTING, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), nProgress);		//Send to Parent Wnd to Module Conntected 

	if(hMsgWnd)
		::PostMessage(hMsgWnd, SFTWM_MODULE_STATE_CHANGE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)0);		//Send to Parent Wnd To Module State Change
}


inline int CheckMsgHeader(LPSFT_MSG_HEADER lpHeader)
{
	ASSERT(lpHeader);

#ifdef _DEBUG
	/*
	char msg[128];
	sprintf(msg, "==> CMD [0x%x] Serial [%03d] :: CH [0x%x/0x%x], Type [%d], Num [%d], Size [%d]\n", lpHeader->nCommand, lpHeader->lCmdSerial, 
				lpHeader->lChSelFlag[1], lpHeader->lChSelFlag[0], 
				lpHeader->wCmdType, lpHeader->wNum, lpHeader->nLength);
	if(lpHeader->nCommand != SFT_CMD_CH_DATA && lpHeader->nCommand != SFT_CMD_HEARTBEAT)
	{
		WriteLog(msg);
//		TRACE(msg);
	}
	*/
#endif

//	if(lpHeader->wGroupNum < 0 || lpHeader->wGroupNum > m_stModule[nModuleIndex].sysData.nTotalGroupNo)	return -2;
//	if(lpHeader->wChannelNum < 0 || lpHeader->wChannelNum > m_stModule[nModuleIndex].sysData.awChInGroup[lpHeader->wGroupNum-1])	return -3;
//	if(GetCmdDataSize(lpHeader->nCommand) != (lpHeader->nLength - SizeofHeader()))	return -4;

	for(int i=0; i<32; i++)
	{
		//최하위 채널을 return 한다. 
		if(0x01 << i & lpHeader->lChSelFlag[0])		
		{
			return i+1;
		}

		if(0x01 << i & lpHeader->lChSelFlag[1])		
		{
			return i+33;
		}
	}
	
	return 0;
}


//Parsing Response Command 
//Ack 가 필요 하지 않은 명령은 반드시 return 하여야 한다.
void ParsingCommand(CWizReadWriteSocket *pSock, int nModuleIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd)
{
	ASSERT(pSock);
	ASSERT(lpMsgHeader);
//	ASSERT(lpReadBuff);		//최초 Command가 Body 없는 명령일 경우 lpReadBuff가 NULL이다.

	char				msg[128];
	unsigned long		wrSize = 0;
	int					nChannel = 0;
	INT					nResponseCode = SFT_ACK;

	LPSFT_MSG_HEADER lpHeader = (LPSFT_MSG_HEADER)lpMsgHeader;

	if((nChannel = CheckMsgHeader(lpHeader)) < 0)		//Header Check
	{
		sprintf(msg, "Module %d Message Header Error. Cmd = 0x%x [Code= %d]", m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand, nChannel);
		WriteLog(msg);
		return ;
	}

	

	

	//ksj 20201115 : 최종 수신한 cmdID와 시간 저장.
	SYSTEMTIME tm;
	GetLocalTime(&tm);
	if(lpHeader->nCommand == SFT_CMD_RESPONSE)
	{
		int nAckCmd = m_lpModule[nModuleIndex].m_CommandAck.nCmd;
		m_lpModule[nModuleIndex].SetRecentRecvAck1(nAckCmd,tm); //최종 수신한 Response (Ack) 명령어 시간 저장

		//ksj 20201118 : TEST 수신한 RESPONSE 전부 로그 남기기
		sprintf(msg, "MD:%d CH:%d CMD:0x%x(0x%x) RESPONSE : %d", m_lpModule[nModuleIndex].GetModuleID(), nChannel, lpHeader->nCommand, nAckCmd, m_lpModule[nModuleIndex].m_CommandAck.nCode);
		WriteLog(msg);
		//ksj end 
	}
	else
	{
		m_lpModule[nModuleIndex].SetRecentRecvCmd1(lpHeader->nCommand,tm); //최종 수신한 cmd id 시간 저장
	}
	//ksj end

//	sprintf(msg, "커맨드 : %d", lpHeader->nCommand);
//	WriteLog(msg);

	switch ( lpHeader->nCommand )
	{
	
	case SFT_CMD_MUX_SELECT_COMM_RESPONE:	//ljb 20151102 add
		{
			// Send to Main Window
			if(lpHeader->nLength == sizeof(SFT_MUX_SELECT_RESPONSE) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					SFT_MUX_SELECT_RESPONSE *sMux = new SFT_MUX_SELECT_RESPONSE;
					//LPVOID lpData = new char[sizeof(SFT_MUX_SELECT_RESPONSE)];
					//m_lpModule[nModuleIndex].SetBuffer(lpData);
					//memcpy(lpData, lpReadBuff, sizeof(SFT_MUX_SELECT_RESPONSE));
					memcpy(sMux, lpReadBuff, sizeof(SFT_MUX_SELECT_RESPONSE));
					
					TRACE("channel : %d, fault %d \n",sMux->chNum,sMux->fault_flag);
					//::PostMessage(hMsgWnd, SFTWM_MUX_CHANGE_RESULT, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
					::PostMessage(hMsgWnd, SFTWM_MUX_CHANGE_RESULT, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)sMux);
				}
			}
			else
			{
				nResponseCode = SFT_NACK;
			}

		}
		return;
		
	case SFT_CMD_RT_TABLE_UPLOAD_END_RESPONSE:	//yulee	20181120
		{
			// Send to Main Window
			//if(lpHeader->nLength == sizeof(SFT_CMD_RT_TABLE_UPLOAD_END_RESPONSE) && lpReadBuff!= NULL)
			if(lpHeader->nLength == sizeof(SFT_RT_TABLE_SEND_RESPONSE) && lpReadBuff!= NULL) //ksj 20200207
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					SFT_RT_TABLE_SEND_RESPONSE *sRTtable = new SFT_RT_TABLE_SEND_RESPONSE;
					memcpy(sRTtable, lpReadBuff, sizeof(SFT_RT_TABLE_SEND_RESPONSE));
					
					TRACE("RT table receive check : %d(1:OK)", sRTtable->nRetFlag);
					::PostMessage(hMsgWnd, SFTWM_RT_TABLE_REC, (WPARAM)sRTtable->nRetFlag, (LPARAM)sRTtable->nRetFlag);
				}
			}
			else
			{
				nResponseCode = SFT_NACK;
			}
			
		}
		return;

	case SFT_CMD_RT_HUMI_TABLE_UPLOAD_END_RESPONSE:	//ksj 20200207
		{
			// Send to Main Window
			if(lpHeader->nLength == sizeof(SFT_RT_HUMI_TABLE_SEND_RESPONSE) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					SFT_RT_HUMI_TABLE_SEND_RESPONSE *sRTtable = new SFT_RT_HUMI_TABLE_SEND_RESPONSE;
					memcpy(sRTtable, lpReadBuff, sizeof(SFT_RT_HUMI_TABLE_SEND_RESPONSE));

					TRACE("RT table receive check : %d(1:OK)", sRTtable->nRetFlag);
					::PostMessage(hMsgWnd, SFTWM_RT_HUMI_TABLE_REC, (WPARAM)sRTtable->nRetFlag, (LPARAM)sRTtable->nRetFlag);
				}
			}
			else
			{
				nResponseCode = SFT_NACK;
			}

		}
		return;
		
	case SFT_CMD_FTP_END_RESPONSE :
		{
			if((lpHeader->nLength == sizeof(SFT_RESPONSE)) && lpReadBuff != NULL)
			{
				memcpy(&m_lpModule[nModuleIndex].m_CommandAck, lpReadBuff, sizeof(SFT_RESPONSE));
			}
			else
			{
				m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_SIZE_MISMATCH;
			}
			//LPVOID lpData = new char[sizeof(SFT_CMD_FTP_END_RESPONSE)];
			LPVOID lpData = NULL;
			
			//			2013-08-21			bung			:			study
			//ljb 20130621 

			if (m_lpModule[nModuleIndex].m_CommandAck.nCode == SFT_ACK)
			{
				::PostMessage(hMsgWnd, SFT_CMD_FTP_END_OK_RESPONSE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
			}
			else{
				::PostMessage(hMsgWnd, SFT_CMD_FTP_END_NG_RESPONSE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
			}
			
			
			sprintf(msg, "FTP END Response From Module %d : Command 0x%x, Code %d", 
				m_lpModule[nModuleIndex].GetModuleID(), 
				m_lpModule[nModuleIndex].m_CommandAck.nCmd, 
				m_lpModule[nModuleIndex].m_CommandAck.nCode); 			
			WriteLog(msg);
			return ;		//return TRUE;
		}
		
		// 모듈이 최초 접속시에 보내는 모듈 아이디	//
	case SFT_CMD_INFO_DATA :			// SBC -> HOST
		{
			return ;
		}
	
		// 최소 100ms 단위로 저장 되는 Data
	case SFT_CMD_MSEC_CH_DATA:
		return;		//ljb 201008 추가

	case SFT_CMD_BMS_COMM_RESPONE :			// 20090209 ljb add		SBC -> BMS TEST 메시지 수신 완료
	case SFT_CMD_BMS_COMM_RESULT :			//20090209 ljb add SBC -> BMS TEST 결과 값 전송했다는 메시지
			return;		//ljb 201008 추가

	// 모니터링 결과 데이터	매 1초마다 보고되는 Data//
	case SFT_CMD_CH_DATA :
		{
			return;		//ljb 201008 추가			
		}

	// 전송한 Command에 대한 Acknowledge //
	case SFT_CMD_RESPONSE :
		{
			if((lpHeader->nLength == sizeof(SFT_RESPONSE)) && lpReadBuff != NULL)
			{
				memcpy(&m_lpModule[nModuleIndex].m_CommandAck, lpReadBuff, sizeof(SFT_RESPONSE));
			}
			else
			{
				m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_SIZE_MISMATCH;
			}

			sprintf(msg, "Response From Module %d : Command 0x%x, Code %d", 
							m_lpModule[nModuleIndex].GetModuleID(), 
							m_lpModule[nModuleIndex].m_CommandAck.nCmd, 
							m_lpModule[nModuleIndex].m_CommandAck.nCode); 	
			TRACE("%s\r\n",msg);
			m_lpModule[nModuleIndex].SetReadEvent();
//			WriteLog(msg);
		}
		return ;

	case SFT_CMD_VARIABLE_CH_DATA:		//ljb 20140308 
//		if(lpReadBuff != NULL) ProcessingMonitoringVariableData(nModuleIndex, lpReadBuff, lpHeader->nLength, hMsgWnd); 
		return;

		//ljb 20170731 Step update Request 응답 
	case SFT_CMD_STEP_INFO:
		{
			// Send to Main Window
			if(lpHeader->nLength == sizeof(SFT_STEP_CONDITION_V3) && lpReadBuff!= NULL) //lyj 20200214 LG v1015 이상
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					LPVOID lpData = new char[sizeof(SFT_STEP_CONDITION_V3)]; //lyj 20200214 LG v1015 이상
					m_lpModule[nModuleIndex].SetBuffer(lpData);
					memcpy(lpData, lpReadBuff, sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상
					//::PostMessage(hMsgWnd, SFTWM_STEP_INFO_DATA, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);

					//ksj 20170808 : 채널 정보도 같이 올리기 , 한 채널만 처리 가능한 명령이므로 선택된 채널 하나 발견하면 해당 채널 번호 사용.										
					int nSelCh = -1;
					int nCh, nCnt;
					for(nCnt = 0; nCnt < 2; nCnt++)
					{
						for(nCh = 0; nCh < 32; nCh++)
						{
							if(lpHeader->lChSelFlag[0] >> nCh & 0x01)
							{			
								nSelCh = nCh + (nCnt*32);
								break;
							}
						}
						if(nSelCh != -1)
							break;
					}					

					WPARAM wparam = MAKEWPARAM(m_lpModule[nModuleIndex].GetModuleID(), nSelCh);
					::PostMessage(hMsgWnd, SFTWM_STEP_INFO_DATA, wparam, (LPARAM)lpData);
					//ksj end
					
				}
			}			
		}
		return;

		//ljb 20170731 안전조건 Request 응답 
	case SFT_CMD_SAFETY_INFO:
		{
			COPYDATASTRUCT		copymsg;
			HANDLE	handle = AfxGetInstanceHandle();			// 자신의 핸들 얻기

			// Send to Main Window
			if(lpHeader->nLength == sizeof(SFT_CMD_SAFETY_UPDATE_REPLY) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
// 				SFT_CMD_SAFETY_UPDATE_REPLY sReply;
// 				memcpy(&sReply, lpReadBuff, sizeof(SFT_CMD_SAFETY_UPDATE_REPLY));

				
// 				HANDLE	handle = AfxGetInstanceHandle();			// 자신의 핸들 얻기 20170803
// 				HWND hWnd = ::FindWindow(NULL, "CTSMonPro");
// 				if(hMsgWnd)
// 				{
// 					//copymsg.cbData = sizeof(CTSWORKVALUE);
// 					copymsg.cbData = 0;
// 					copymsg.dwData = SFTWM_SAFETY_INFO_DATA;
// 					//msg.lpData = (PVOID)m_pInsulation;
// 					copymsg.lpData = NULL;
// 					//CTSPro 전달 인수 확인
// 					//::SendMessage(hMsgWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&copymsg);				
// 					::SendMessage(hMsgWnd, WM_COPYDATA, 0, (LPARAM)&copymsg);				
// 					
// 					//ljb	log file 쓰기
// // 					if (bFlag) strData.Format("\t Send SBC --> 절연 실행\n");
// // 					else strData.Format("\t Send SBC --> 비절연 실행\n");
// // 					mainArea->Fun_FileWriteLog(0,strData);
// 				}
// 				COPYDATASTRUCT		msgcopy;
// 				ZeroMemory(&msgcopy,sizeof(COPYDATASTRUCT));
// 				HANDLE	handle = AfxGetInstanceHandle();			// 자신의 핸들 얻기
// 				HWND hWnd = ::FindWindow(NULL, "CTSMonitorPro");
// 				if(hWnd)
// 				{
// 					//msgcopy.cbData = sizeof(CTSWORKVALUE);
// 					msgcopy.dwData = SFTWM_SAFETY_INFO_DATA;
// 					//msgcopy.lpData = (PVOID)m_pInsulation;
// 					//CTSPro 전달 인수 확인
// 					::SendMessage(hWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&msgcopy);				
// 					
// 
// 				}				
// 				

				if(hMsgWnd)
				{
					LPVOID lpData = new char[sizeof(SFT_CMD_SAFETY_UPDATE_REPLY)];
					m_lpModule[nModuleIndex].SetBuffer(lpData);
					memcpy(lpData, lpReadBuff, sizeof(SFT_CMD_SAFETY_UPDATE_REPLY));
					
					//::PostMessage(hMsgWnd, SFTWM_SAFETY_INFO_DATA, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
					::SendMessageA(hMsgWnd, SFTWM_SAFETY_INFO_DATA, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
					
				}
			}			
		}
		return;
	case SFT_CMD_TESTCOND_UPDATE_END_CONFIRM:
		sprintf(msg, "Schedule Update END Confirm (SBC) From Module %d : Command 0x%x, Code %d", 
			m_lpModule[nModuleIndex].GetModuleID(), 
			m_lpModule[nModuleIndex].m_CommandAck.nCmd, 
			m_lpModule[nModuleIndex].m_CommandAck.nCode); 			
		WriteLog(msg);
		return;

		//교정 완료 명령 수신 
	case SFT_CMD_CALI_RESULT:
		{
			// Send to Main Window
			if(lpHeader->nLength == sizeof(SFT_CALI_END_DATA) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					LPVOID lpData = new char[sizeof(SFT_CALI_END_DATA)];
					m_lpModule[nModuleIndex].SetBuffer(lpData);
					memcpy(lpData, lpReadBuff, sizeof(SFT_CALI_END_DATA));

					::PostMessage(hMsgWnd, SFTWM_CALI_RESULT_DATA, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
				}
			}
			else
			{
				nResponseCode = SFT_NACK;
			}

			break;	//send response	//SFTWM_CALI_RESULT_DATA
		}
	case SFT_CMD_HEARTBEAT:		//Network Check Command
		{
			if(m_lpModule[nModuleIndex].IsSyncWrite == FALSE)
				break;	//Send Ack
			else 
				return ; //return TRUE;
		}

	case SFT_CMD_EMERGENCY:
		{
			if(lpHeader->nLength == sizeof(SFT_EMG_DATA) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					LPVOID lpData = new char[sizeof(SFT_EMG_DATA)];
					m_lpModule[nModuleIndex].SetBuffer(lpData);
					memcpy(lpData, lpReadBuff, sizeof(SFT_EMG_DATA));
					::PostMessage(hMsgWnd, SFTWM_MODULE_EMG, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)lpData);
				}
			}
			return ;	//return TRUE;
		}

	case SFT_CMD_SYNC_TIME_REQUEST:
		{
			SFT_MD_SYNC_TIME_RESPONE syncTime;
			ZeroMemory(&syncTime, sizeof(SFT_MD_SYNC_TIME_RESPONE));

			SYSTEMTIME st;
			::GetLocalTime(&st);  
			sprintf(syncTime.szSyncTime, "%d-%02d-%02d %02d:%02d:%02d.%03d" , st.wYear, st.wMonth, st.wDay,
				st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
			
			Sleep(50);	//==> Process Handle을 넘기기 위한 잠깐의 Delay 반드시 필요

			//Make Header
			int nTimeSize = SizeofHeader()+sizeof(SFT_MD_SYNC_TIME_RESPONE);
			char *sbuff = new char[nTimeSize];
			ZeroMemory(sbuff, nTimeSize);
			LPSFT_MSG_HEADER lpMsgHeader = (LPSFT_MSG_HEADER)sbuff;
			lpMsgHeader->nCommand = SFT_CMD_SYNC_TIME_RESPONE;			// Packet Command 종류	(예 : 이 패킷은 컨트롤 패킷이다)
			lpMsgHeader->lCmdSerial = lpHeader->lCmdSerial;				// Command Serial Number..(Command ID)
			lpMsgHeader->nLength = sizeof(SFT_MD_SYNC_TIME_RESPONE);	// Size of Body
			memcpy((char *)sbuff+SizeofHeader(), &syncTime, sizeof(SFT_MD_SYNC_TIME_RESPONE));
		
			wrSize = pSock->Write(sbuff, nTimeSize);
			if(wrSize != nTimeSize)
			{
				sprintf(msg, "Module %d time sync. response send fail(%d/%d). WSAGetLastError %d\n", 
								m_lpModule[nModuleIndex].GetModuleID(), wrSize,  nTimeSize, WSAGetLastError()); 			
			}
			else
			{
				sprintf(msg, "Module %d time sync. response send.", m_lpModule[nModuleIndex].GetModuleID()); 	

				//ksj 20201115 : 최종 전송한 명령ID와, 시간 저장 추가.
				m_lpModule[nModuleIndex].SetRecentSendCmd(lpMsgHeader->nCommand,st);
				//ksj end
			}

			WriteLog(msg);
			delete [] sbuff;
			return ;	//return TRUE;
		}
	//ksj 20200122 : Ch Attribute 갱신 코드 추가.
	//기존에는 접속시 1회만 수신하여 처리했으나
	//병렬모드등 채널 attribute 변경시에 한번 호출해서 갱신 받도록 처리.
	case SFT_CMD_CH_ATTRIBUTE_REPLY:
		{
			if(lpHeader->nLength == (sizeof(SFT_MD_PARALLEL_DATA)*_SFT_MAX_PARALLEL_COUNT) && lpReadBuff!= NULL)
			{
				//Memory 새로 할당 받아야 한다.
				if(hMsgWnd)
				{
					SFT_MD_PARALLEL_DATA ParallelInfo[_SFT_MAX_PARALLEL_COUNT];
					memcpy(&ParallelInfo, lpReadBuff, sizeof(SFT_MD_PARALLEL_DATA)*_SFT_MAX_PARALLEL_COUNT);
					m_lpModule[nModuleIndex].SetParallelData(ParallelInfo); // 새 데이터로 갱신
					::PostMessage(hMsgWnd, SFTWM_CH_ATTRIBUTE_RECEIVE, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), 0); //CTSMon에 통지.					
				}
			}
			return;
		}
	//ksj end

	// Hardware Error Code						//
//	case CMD_HARDWARE_ERROR :
//		{
////			SFT_RESPONSE *lpRespnse = (SFT_RESPONSE *)lpReadBuff;
////			PostMessage(m_hMsgHwnd, EP_WM_SEND_EMERGENCY_CODE, m_nModuleID, lpRespnse->nCode);
////			TRACE("\t\t\t▷ Module %d H/W ERROR Receive Size : %d\n", m_nModuleID, m_sRxBuffer.dwDataSize);
//			return TRUE;
//		}
	// Calibration 실시에 따른 결과 데이터		//
	case SFT_CMD_CALI_READY_DATA:	//교정명령에 대한 완료 보고 
	case SFT_CMD_CALI_CHECK_RESULT:	//현재 정밀도 검사 수신 
		break;
	// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지 =============================
	case SFT_CMD_CH_OUT_CABLE_ERROR:
		{

			break;
		}
	case SFT_CMD_CD_FAULT_OUT_CABLE_CHECK:
		{

			break;
		}
	// ==============================================================================
	default: 
		{
			//	CCriticalSection m_RxBuffCritic;		//Rx Buffer Critical Section
			if(lpHeader->nLength > 0 && lpReadBuff != NULL)		//Body Data Length Check
			{
				wrSize = lpHeader->nLength;
				if(wrSize <= _SFT_RX_BUF_LEN)			//Rx Buffer Size Check
				{
					memcpy((char *)m_lpModule[nModuleIndex].m_szRxBuffer, lpReadBuff, wrSize);
					m_lpModule[nModuleIndex].m_nRxLength = wrSize;
					m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_ACK;
//					TRACE("Response Data 0x%08x ,%d\n", lpHeader->nCommand, m_stModule[nModuleIndex].nRxLength);
				}
				else									//Size Error
				{
					m_lpModule[nModuleIndex].m_nRxLength = 0;
					m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_RX_BUFF_OVER_FLOW;
//					sprintf(msg, "Module %d Group %d Received Data Length is too Long %d/%d", 
//								  m_stModule[nModuleIndex].sysParam.nModuleID, lpHeader->wGroupNum, wrSize, _EP_RX_BUF_LEN);
//					WriteLog(msg);
				}
				m_lpModule[nModuleIndex].SetReadEvent();
			}
		}
		return ;	//return TRUE;
	}
	

	//응답용 Message 
//	m_lpModule[nModuleIndex].SendResponse(lpHeader, nResponseCode);
	
	SFT_PACKET_RESPONSE	response;
	ZeroMemory(&response, sizeof(SFT_PACKET_RESPONSE));
	response.msgHeader.nCommand = SFT_CMD_RESPONSE;			// Packet Command 종류	(예 : 이 패킷은 컨트롤 패킷이다)
	response.msgHeader.lCmdSerial = lpHeader->lCmdSerial;			// Command Serial Number..(Command ID)
//	response.msgHeader.wCmdType;	
//	response.msgHeader.wNum;				// 이 Command에 해당 사항이 있는 채널의 수
//	response.msgHeader.lChSelFlag[2];		// 이 시험조건이 전송될 Channel (Bit 당 Channel)
	response.msgHeader.nLength = sizeof(SFT_RESPONSE);			// Size of Body
	response.msgBody.nCmd = lpHeader->nCommand;		//default Reponse Code is Ack
	response.msgBody.nCode = nResponseCode;
	
	Sleep(50);	//==> Process Handle을 넘기기 위한 잠깐의 Delay 반드시 필요
	//wrSize = pSock->Write(&response, sizeof(SFT_PACKET_RESPONSE));
	wrSize = pSock->Write(&response, sizeof(SFT_PACKET_RESPONSE), 0); //ksj 20210107 : 로그 레벨 추가.

//	TRACE("RESPONE CMD %x\n", lpHeader->nCommand);

	if(wrSize != sizeof(SFT_PACKET_RESPONSE))
	{
		sprintf(msg, "▶▶▶▶▶▶▶▶▶▶▶▶ Module %d :: Commnad 0x%x, ACK send fail. WSAGetLastError %d\n", 
						m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand,  
						WSAGetLastError()); 			
		WriteLog(msg);
		
		TRACE("\n▶▶▶▶▶▶▶▶▶▶▶▶\n");
		TRACE("Module %d :: Commnad 0x%x, ACK send fail(%d). WSAGetLastError %d\n", 
				m_lpModule[nModuleIndex].GetModuleID(), 
				lpHeader->nCommand, 
				wrSize,
				WSAGetLastError());
		TRACE("▶▶▶▶▶▶▶▶▶▶▶▶\n\n");
	} 
	//ksj 20201115 : 최종 전송한 response cmdID와 시간 저장.
	else
	{
		SYSTEMTIME tm;
		GetLocalTime(&tm);
		int nAckCmd = response.msgBody.nCmd;

		if(nAckCmd == SFT_CMD_HEARTBEAT)
		{
			m_lpModule[nModuleIndex].SetRecentSendHeartBeat(nAckCmd,tm); //최종 수신한 Heartbit에 대한 Response (Ack) 명령어 시간 저장	
		}
		else
		{
			m_lpModule[nModuleIndex].SetRecentSendAck1(nAckCmd,tm); //최종 수신한 Response (Ack) 명령어 시간 저장	
		}
		
	}	
	//ksj end

#ifdef _DEBUG	
	if(nResponseCode != SFT_ACK)
	{
		TRACE("Module %d :: Nack send. Command 0x0xd.\n", m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand);
	}

	if(wrSize != sizeof(SFT_PACKET_RESPONSE))
	{
//		TRACE("\n▶▶▶▶▶▶▶▶▶▶▶▶\n");
//		TRACE("Module %d :: Ack send fail. %d\n", m_lpModule[nModuleIndex].GetModuleID(), wrSize);
//		TRACE("▶▶▶▶▶▶▶▶▶▶▶▶\n\n");
		AfxMessageBox("Ack send fail...");
	}
	else
	{
//		if(lpHeader->nCommand != SFT_CMD_CH_DATA && lpHeader->nCommand != SFT_CMD_HEARTBEAT)
		if(lpHeader->nCommand == SFT_CMD_HEARTBEAT)
		{
			static DWORD dTime = GetTickCount();
			SYSTEMTIME systime;
			::GetLocalTime(&systime);
			long lElTime = GetTickCount()-dTime;
			if(lElTime > 5000)
			{
				TRACE("\n★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★\n");
				TRACE("<===%02d:%02d:%02d.%03d(%03d) :: Module %d send response code %d.\n", systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds, lElTime, m_lpModule[nModuleIndex].GetModuleID(), nResponseCode);
				TRACE("★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★\n\n");
			}
			else
			{
//				TRACE("<===%02d:%02d:%02d.%03d(%03d) :: Module %d Commad serial %d\n", 
//						systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds, lElTime, 
//						m_lpModule[nModuleIndex].GetModuleID(), response.msgHeader.lCmdSerial);
			}
			dTime = GetTickCount();
		}
	}
#endif

	return ;	//return TRUE;
}

//Parsing Response Command 
//Ack 가 필요 하지 않은 명령은 반드시 return 하여야 한다.
void ParsingCommand2(CWizReadWriteSocket *pSock, int nModuleIndex, LPVOID lpMsgHeader, LPVOID lpReadBuff, HWND hMsgWnd)
{
	ASSERT(pSock);
	ASSERT(lpMsgHeader);
//	ASSERT(lpReadBuff);		//최초 Command가 Body 없는 명령일 경우 lpReadBuff가 NULL이다.

	char				msg[128];
	unsigned long		wrSize = 0;
	int					nChannel = 0;
	INT					nResponseCode = SFT_ACK;

	LPSFT_MSG_HEADER lpHeader = (LPSFT_MSG_HEADER)lpMsgHeader;

	if((nChannel = CheckMsgHeader(lpHeader)) < 0)		//Header Check
	{
		sprintf(msg, "Module %d Message Header Error. Cmd = 0x%x [Code= %d]", m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand, nChannel);
		WriteLog(msg);
		//return FALSE;
		return ;
	}

	//ksj 20201115 : 최종 수신한 cmdID와 시간 저장.
	SYSTEMTIME tm;
	GetLocalTime(&tm);
	if(lpHeader->nCommand == SFT_CMD_RESPONSE)
	{
		int nAckCmd = m_lpModule[nModuleIndex].m_CommandAck.nCmd;
		m_lpModule[nModuleIndex].SetRecentRecvAck2(nAckCmd,tm); //최종 수신한 Response (Ack) 명령어 시간 저장
	}
	else if(lpHeader->nCommand == SFT_CMD_HEARTBEAT)
	{
		m_lpModule[nModuleIndex].SetRecentRecvHeartBeat(lpHeader->nCommand,tm); //최종 수신한 cmd id 시간 저장
	}
	else
	{
		m_lpModule[nModuleIndex].SetRecentRecvCmd2(lpHeader->nCommand,tm); //최종 수신한 cmd id 시간 저장
	}
	//ksj end


	//////////////////////////////////////////////////////////////////////////
	// CASE 에서 break 하면 SBC로 Send Ack 전송,return 은 Ack 없다
	switch ( lpHeader->nCommand )
	{
	case SFT_CMD_MSEC_CH_DATA:
	// 모니터링 결과 데이터	매 1초마다 보고되는 Data//
	case SFT_CMD_CH_DATA :
		return;		//ljb 201008 추가 Ack 안보냄

	// 전송한 Command에 대한 Acknowledge //
	case SFT_CMD_RESPONSE :
		{
			if((lpHeader->nLength == sizeof(SFT_RESPONSE)) && lpReadBuff != NULL)
			{
				memcpy(&m_lpModule[nModuleIndex].m_CommandAck, lpReadBuff, sizeof(SFT_RESPONSE));
			}
			else
			{
				m_lpModule[nModuleIndex].m_CommandAck.nCode = SFT_SIZE_MISMATCH;
			}

			sprintf(msg, "Response From Module %d : Command 0x%x, Code %d", 
							m_lpModule[nModuleIndex].GetModuleID(), 
							m_lpModule[nModuleIndex].m_CommandAck.nCmd, 
							m_lpModule[nModuleIndex].m_CommandAck.nCode); 	
			TRACE("%s\r\n",msg);
			m_lpModule[nModuleIndex].SetReadEvent();
		}
		return ;

	//2014.11.21 100D, 100F, 1010, 1011 버전 저장 방식	
	case SFT_CMD_VARIABLE_CH_DATA:
		{
			if(lpReadBuff != NULL) 
			{
				ProcessingMonitoringVariableData(nModuleIndex, lpReadBuff, lpHeader->nLength, hMsgWnd); 
			}
			return;		
		}

	//2014.11.21 100E 버전 저장 방식	
	case SFT_CMD_VARIABLE_CH_DATA_SAVE:
		{
			if(lpReadBuff != NULL){
				ProcessingSaveVariableData(nModuleIndex, lpReadBuff, lpHeader->nLength, hMsgWnd); 				
			}
			return;		
		}

	case SFT_CMD_HEARTBEAT:		//Network Check Command
		{
		}
	}	//END switch ( lpHeader->nCommand )
	//////////////////////////////////////////////////////////////////////////
	
	//응답용 Message 
	SFT_PACKET_RESPONSE	response;
	ZeroMemory(&response, sizeof(SFT_PACKET_RESPONSE));
	response.msgHeader.nCommand = SFT_CMD_RESPONSE;			// Packet Command 종류	(예 : 이 패킷은 컨트롤 패킷이다)
	response.msgHeader.lCmdSerial = lpHeader->lCmdSerial;			// Command Serial Number..(Command ID)
//	response.msgHeader.wCmdType;	
//	response.msgHeader.wNum;				// 이 Command에 해당 사항이 있는 채널의 수
//	response.msgHeader.lChSelFlag[2];		// 이 시험조건이 전송될 Channel (Bit 당 Channel)
	response.msgHeader.nLength = sizeof(SFT_RESPONSE);			// Size of Body
	response.msgBody.nCmd = lpHeader->nCommand;		//default Reponse Code is Ack
	response.msgBody.nCode = nResponseCode;
	
	Sleep(50);	//==> Process Handle을 넘기기 위한 잠깐의 Delay 반드시 필요
	//wrSize = pSock->Write(&response, sizeof(SFT_PACKET_RESPONSE));
	wrSize = pSock->Write(&response, sizeof(SFT_PACKET_RESPONSE), 0); //ksj 20210107 : 로그 레벨 추가.
//	TRACE("RESPONE CMD %x\n", lpHeader->nCommand);

	if(wrSize != sizeof(SFT_PACKET_RESPONSE))
	{
		sprintf(msg, "▶▶▶▶▶▶▶▶▶▶▶▶ Module %d :: Commnad 0x%x, ACK send fail. WSAGetLastError %d\n", 
						m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand,  
						WSAGetLastError()); 			
		WriteLog(msg);
		
		TRACE("\n▶▶▶▶▶▶▶▶▶▶▶▶\n");
		TRACE("Module %d :: Commnad 0x%x, ACK send fail(%d). WSAGetLastError %d\n", 
				m_lpModule[nModuleIndex].GetModuleID(), 
				lpHeader->nCommand, 
				wrSize,
				WSAGetLastError());
		TRACE("▶▶▶▶▶▶▶▶▶▶▶▶\n\n");
	} 
	//ksj 20201115 : 최종 전송한 response cmdID와 시간 저장.
	else
	{
		SYSTEMTIME tm;
		GetLocalTime(&tm);
		int nAckCmd = response.msgBody.nCmd;
		m_lpModule[nModuleIndex].SetRecentSendAck2(nAckCmd,tm); //최종 수신한 Response (Ack) 명령어 시간 저장		
		
	}	
	//ksj end

#ifdef _DEBUG	
	if(nResponseCode != SFT_ACK)
	{
		TRACE("Module %d :: Nack send. Command 0x0xd.\n", m_lpModule[nModuleIndex].GetModuleID(), lpHeader->nCommand);
	}

	if(wrSize != sizeof(SFT_PACKET_RESPONSE))
	{
		AfxMessageBox("Ack send fail...");
	}
	else
	{
		if(lpHeader->nCommand == SFT_CMD_HEARTBEAT)
		{
			static DWORD dTime = GetTickCount();
			SYSTEMTIME systime;
			::GetLocalTime(&systime);
			long lElTime = GetTickCount()-dTime;
			if(lElTime > 5000)
			{
				TRACE("\n★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★\n");
				TRACE("<===%02d:%02d:%02d.%03d(%03d) :: Module %d send response code %d.\n", systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds, lElTime, m_lpModule[nModuleIndex].GetModuleID(), nResponseCode);
				TRACE("★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★\n\n");
			}
//			else
//			{
//				TRACE("<===%02d:%02d:%02d.%03d(%03d) :: Module %d Commad serial %d\n", 
//						systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds, lElTime, 
//						m_lpModule[nModuleIndex].GetModuleID(), response.msgHeader.lCmdSerial);
//			}
			dTime = GetTickCount();
		}
	}
#endif

	return;
}


/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	결과 데이터를 SBC -> PC 데이터를 모아서 전송한다.
-- Author		:   이재복
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void ProcessingSaveVariableData(UINT nModuleIndex, void* pReceivedBuffer, int nSize, HWND hMsgWnd)
{
		
	CChannel *pChData;	
	int nChSize = 0, nAuxSize = 0, nCanSize = 0, nOvenSize = 0;
	
	int nMappedCh = 0;
	
	static unsigned char szDataMode[SFT_MAX_CH_PER_MD] = {0,};
	memset(szDataMode, 0, sizeof(szDataMode));
				
	int nIndex;
	int nBasicSize;
	short int ch_count;
	short int aux_count;
	short int can_count;
	short int resetved;
	
	nBasicSize = sizeof(short int)*4;
	nChSize = SizeofNetChData();
	nAuxSize = SizeofNetAuxData();
	nCanSize = SizeofNetCanData();
	nOvenSize = SizeofNetOvenData();
	
	char* pDataBuffer = (char *)pReceivedBuffer;
				
	//ver 100E 수정 해야 하는 부분 (아래)
	SFT_VARIABLE_CH_DATA_100E sOrigianlData;
	SFT_VARIABLE_CH_DATA sVarChResultData;
	
	memcpy(&sOrigianlData.ch_count, pDataBuffer, sizeof(short int));
	memcpy(&sOrigianlData.aux_count, pDataBuffer + (1*sizeof(short int)), sizeof(short int));
	memcpy(&sOrigianlData.can_count, pDataBuffer + (2*sizeof(short int)), sizeof(short int));
	memcpy(&sOrigianlData.resetved, pDataBuffer + (3*sizeof(short int)), sizeof(short int));
	memcpy(&sOrigianlData.Data, pDataBuffer + (4*sizeof(short int)), nChSize);
	sVarChResultData =  sOrigianlData.Data;
	
	//////////////////////////////////////////////////////////////////////////
	// 기본 데이터 넣기 
	ch_count = sOrigianlData.ch_count;
	aux_count = sOrigianlData.aux_count;
	can_count = sOrigianlData.can_count;
	resetved = sOrigianlData.resetved;
	
	#ifdef _DEBUG
		//TRACE("nChSize : %d\n",nSize);	
	#endif // _DEBUG												

	// 시작시 길이
	int nLength = nBasicSize;	

	//char msg[255];
	//sprintf(msg, " DataSize : %d, ch_count : %d / aux_count : %d / can_count : %d / resetved : %d  \n",nSize ,ch_count, aux_count, can_count, resetved);	
	//WriteLog(msg);
	
	int TotalSize = nChSize + nAuxSize*sVarChResultData.chData.nAuxCount + nCanSize*sVarChResultData.chData.nCanCount;
	int modNo = (nSize % TotalSize) - nBasicSize;
	int DataCnt = nSize / TotalSize;

	if(modNo != 0){
		return;
	}

	//sprintf(msg, " TotalSize : %d, DataCnt : %d / modNo : %d \n",TotalSize ,DataCnt, modNo);	
	//WriteLog(msg);
	
	for( nIndex = 0; nIndex < DataCnt; nIndex++ )
	{				
		if(nMappedCh >= 0 && nMappedCh < SFT_MAX_CH_PER_MD)
		{
			szDataMode[nMappedCh] = sVarChResultData.chData.chDataSelect;
		}
		
		if(nSize >= nChSize )
		{
			memcpy(&sVarChResultData.chData, pDataBuffer + nLength, nChSize);	
		}		
		else
		{					
			continue;
		}
		
		nMappedCh = sVarChResultData.chData.chNo - 1;
		
		if(nModuleIndex < SFT_MAX_BD_PER_MD && nModuleIndex >= 0 && nMappedCh >= 0 && nMappedCh < SFT_MAX_CH_PER_MD)
		{			
			memcpy(&g_variableChdata[nModuleIndex][nMappedCh].chData, pDataBuffer + nLength, nChSize);		
			nLength += nChSize;
		}
		else
		{				
			continue;
		}
		
		
		
		ZeroMemory(sVarChResultData.auxData, sizeof(SFT_AUX_DATA)*_SFT_MAX_MAPPING_AUX);
		if(sVarChResultData.chData.nAuxCount > 0 && sVarChResultData.chData.nAuxCount <= _SFT_MAX_MAPPING_AUX)
		{
			int nLen_aux;
			nLen_aux = nAuxSize*sVarChResultData.chData.nAuxCount;					
			memcpy(sVarChResultData.auxData, pDataBuffer+nLength, nLen_aux);					
			memcpy(&g_variableChdata[nModuleIndex][nMappedCh].auxData, pDataBuffer+nLength, nLen_aux);	
			nLength +=nLen_aux;
		}
		
		ZeroMemory(sVarChResultData.canData, sizeof(SFT_CAN_DATA)*_SFT_MAX_MAPPING_CAN);
		if(sVarChResultData.chData.nCanCount > 0 && sVarChResultData.chData.nCanCount <= _SFT_MAX_MAPPING_CAN)
		{				
			int nLen_can;
			nLen_can = nCanSize*sVarChResultData.chData.nCanCount;					
			memcpy(sVarChResultData.canData, pDataBuffer+nLength, nLen_can); 
			memcpy(&g_variableChdata[nModuleIndex][nMappedCh].canData, pDataBuffer+nLength, nLen_can); 
			nLength +=nLen_can;
		}
		
		//채널 참조 포인트 구함
		pChData = m_lpModule[nModuleIndex].GetChannelData(nMappedCh);
		if(pChData != NULL)
		{					
			ZeroMemory(&sVarChResultData.ovenData, sizeof(SFT_CHAMBER_VALUE));
			memcpy(&sVarChResultData.ovenData, pChData->GetChamberData(), sizeof(SFT_CHAMBER_VALUE)); 
			memcpy(&sVarChResultData.loaderData, pChData->GetLoaderData(), sizeof(SFT_LOADER_VALUE));	 //ljb 20150427 add Loader data save
			memcpy(&sVarChResultData.chillerData, pChData->GetChillerData(), sizeof(SFT_CHAMBER_VALUE)); //ljb 20170906 add chiller data save
			memcpy(&sVarChResultData.cellBALData, pChData->GetCellBALData(), sizeof(SFT_CELLBAL_VALUE)); //ljb 20170906 add cellBalance data save
			memcpy(&sVarChResultData.pwrSupplyData, pChData->GetPwrSupplyData(), sizeof(SFT_PWRSUPPLY_VALUE)); //ljb 20170906 add cellBalance data save
			memcpy(&g_chamberData[nModuleIndex][nMappedCh],pChData->GetChamberData(), sizeof(SFT_CHAMBER_VALUE));
			
			//ljb 20140310 비교 없이 저장 한다.
			pChData->PushSaveData(sVarChResultData);	
			
		}
		else
		{
			TRACE("Channel Monitoring Data의 Channel No Error\n");
		}
		
	}
	
	
	// Send Message to Main Window
	::PostMessage(hMsgWnd,   SFTWM_TEST_RESULT, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)szDataMode);	//ljb 20140308 SAVE 데이터 전송			
	
}

//Aux Data를 포함한 데이터 //2007/07/31 kjh
//CAN Data를 포함한 데이터 //2007/10/18 kjh
void ProcessingMonitoringVariableData(UINT nModuleIndex, void* pReceivedBuffer, int /*nSize*/, HWND hMsgWnd)
{
	DWORD dwtime = GetTickCount();
	Sleep(1);

	CChannel *pChData;	
	int nChSize = 0, nAuxSize = 0, nCanSize = 0, nOvenSize = 0;
	//Version check
	int nMappedCh = 0;

	static unsigned char szDataMode[SFT_MAX_CH_PER_MD] = {0,};
	memset(szDataMode, 0, sizeof(szDataMode));

	SFT_MD_SYSTEM_DATA * pSysData =  m_lpModule[nModuleIndex].GetModuleSystem();

	//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION)
	if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20200121
	{
	}
	else
	{
		// _SFT_PROTOCOL_VERSION 이상 버전 처리 루틴
		SFT_VARIABLE_CH_DATA sVarChResultData;
		nChSize = SizeofNetChData();
		nAuxSize = SizeofNetAuxData();
		nCanSize = SizeofNetCanData();
		nOvenSize = SizeofNetOvenData();

		memcpy(&sVarChResultData.chData, (BYTE *)pReceivedBuffer, nChSize);
//#ifdef _PARALLEL_FORCE 전처리기 주석처리
//yulee 20190705 강제병렬 옵션 처리 Mark

		if(g_iPType==1)//yulee 20190706
		{
			//ksj 20171025 : 주석처리.
			if (sVarChResultData.chData.chNo == 2 || sVarChResultData.chData.chNo == 4)	//ljb 20170626 1,3 채널 데이터 버림.
			{
				return;
			}
			if (sVarChResultData.chData.chNo == 3) sVarChResultData.chData.chNo = 2; 	//ljb 20170626 3채널 데이터를 2번 채널 데이터로 변경.
		}
//#endif	
		nMappedCh = sVarChResultData.chData.chNo-1;
		memcpy(&g_variableChdata[nModuleIndex][nMappedCh].chData, (BYTE *)pReceivedBuffer, nChSize);//★★ ljb 20091111  ★★★★★★★★
		
		ZeroMemory(sVarChResultData.auxData, sizeof(SFT_AUX_DATA)*_SFT_MAX_MAPPING_AUX);
		if(sVarChResultData.chData.nAuxCount > 0 && sVarChResultData.chData.nAuxCount <= _SFT_MAX_MAPPING_AUX)
		{
			memcpy(sVarChResultData.auxData, (BYTE *)pReceivedBuffer+nChSize, nAuxSize*sVarChResultData.chData.nAuxCount);
			memcpy(&g_variableChdata[nModuleIndex][nMappedCh].auxData, (BYTE *)pReceivedBuffer+nChSize, nAuxSize*sVarChResultData.chData.nAuxCount);
		}

		ZeroMemory(sVarChResultData.canData, sizeof(SFT_CAN_DATA)*_SFT_MAX_MAPPING_CAN);
		if(sVarChResultData.chData.nCanCount > 0 && sVarChResultData.chData.nCanCount <= _SFT_MAX_MAPPING_CAN)
		{
			memcpy(sVarChResultData.canData, (BYTE*)pReceivedBuffer+nChSize+nAuxSize*sVarChResultData.chData.nAuxCount, nCanSize*sVarChResultData.chData.nCanCount); 
			memcpy(&g_variableChdata[nModuleIndex][nMappedCh].canData, (BYTE*)pReceivedBuffer+nChSize+nAuxSize*sVarChResultData.chData.nAuxCount, nCanSize*sVarChResultData.chData.nCanCount); 
		}

/*#ifdef _DEBUG

 		//ksj 20200818 : 가상 CAN 값 디버깅용
 		for(int x=0;x<sVarChResultData.chData.nCanCount;x++)
 		{
 			float fTest[2];
 			fTest[0] = 4.224+x;
 			fTest[1] = 0;
 			memcpy(&sVarChResultData.canData[x].canVal,fTest,sizeof(SFT_CAN_VALUE));			
 		}
#endif*/

		//TRACE("=====>save =%d, ch = %d ,current %d, watt %d  \n",sVarChResultData.chData.chDataSelect, nMappedCh,g_variableChdata[nMappedCh].chData.lCurrent,g_variableChdata[nMappedCh].chData.lWatt);
		//채널 참조 포인트 구함
		pChData = m_lpModule[nModuleIndex].GetChannelData(nMappedCh);
		if(pChData != NULL)
		{
			//★★ ljb 20091112  챔버 값을 가져옴★★★★★★★★
			ZeroMemory(&sVarChResultData.ovenData, sizeof(SFT_CHAMBER_VALUE));
			memcpy(&sVarChResultData.ovenData, pChData->GetChamberData(), sizeof(SFT_CHAMBER_VALUE)); 
			
			//★★ ljb 20150427 add Loader data save ★★★★★★★★
			memcpy(&sVarChResultData.loaderData, pChData->GetLoaderData(), sizeof(SFT_LOADER_VALUE));	//ljb 20150427 add Loader data save
			
			//★★ ljb 20170906  칠러 값을 가져옴★★★★★★★★
			ZeroMemory(&sVarChResultData.chillerData, sizeof(SFT_CHAMBER_VALUE));
			memcpy(&sVarChResultData.chillerData, pChData->GetChillerData(), sizeof(SFT_CHAMBER_VALUE)); 
			
			//★★ ljb 20170906  CellBalance 값을 가져옴★★★★★★★★
			ZeroMemory(&sVarChResultData.cellBALData, sizeof(SFT_CELLBAL_VALUE));
			memcpy(&sVarChResultData.cellBALData, pChData->GetCellBALData(), sizeof(SFT_CELLBAL_VALUE)); 
			
			//★★ ljb 20170906  PwrSupply 값을 가져옴★★★★★★★★
			ZeroMemory(&sVarChResultData.pwrSupplyData, sizeof(SFT_PWRSUPPLY_VALUE));
			memcpy(&sVarChResultData.pwrSupplyData, pChData->GetPwrSupplyData(), sizeof(SFT_PWRSUPPLY_VALUE)); 
			
			memcpy(&g_chamberData[nModuleIndex][nMappedCh],pChData->GetChamberData(), sizeof(SFT_CHAMBER_VALUE));	//실시간 그래프용 데이터 복사

			//현재 채널 data를 갱신 한다.
			//pChData->SetChannelVarData(sVarChResultData);
			pChData->SetChannelVarData(sVarChResultData, hMsgWnd); //ksj 20160728 메세지를 보내기 위한 윈도우 핸들 인자 추가.
			
			//sVarChResultData.auxData[4].lValue = 123456789;

			//저장해야 할 Data일 경우 채널의 저장 stack에 저장한다.
			if(sVarChResultData.chData.chDataSelect != SFT_SAVE_REPORT)
			{
				pChData->PushSaveData(sVarChResultData);	
			}
		}
		else
		{
			TRACE("Channel Monitoring Data의 Channel No Error\n");
		}
		
		if(nMappedCh >= 0 && nMappedCh <SFT_MAX_CH_PER_MD)
		{
			szDataMode[nMappedCh] = sVarChResultData.chData.chDataSelect;
		}
	}
	
	// Send Message to Main Window
	::PostMessage(hMsgWnd,   SFTWM_TEST_RESULT, (WPARAM)m_lpModule[nModuleIndex].GetModuleID(), (LPARAM)szDataMode);
}

//Moduel ID duplication, Version, registration Check
int CheckModuleIDValidate(LPSFT_MD_SYSTEM_DATA pSysData)
{
	int nModuleIndex;

	//Version Check
	//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION)			return -1;		//Version Mismatch
	if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015)			return -1;		//ksj 20201013 : V1015~V1016 호환
	
	//DataBase에 등록여부 확인 현재 사용하지않느듯
	if((nModuleIndex = SFTGetModuleIndex(pSysData->nModuleID)) < 0)	return -2;		//사용하지않음.
	
	//변수 확인 
	if(pSysData->wChannelPerBoard < 1  || pSysData->wInstalledBoard >SFT_MAX_CH_PER_BD)		return -3;
	if(pSysData->nInstalledChCount < 1  || pSysData->nInstalledChCount > SFT_MAX_CH_PER_MD)	return -4;

	//이미 연결된 상태인지 검사 
	if(SFTGetModuleState(pSysData->nModuleID) != PS_STATE_LINE_OFF)						return -5;		//ID duplication

	return nModuleIndex;
}

//Module이 접속 시도시 접속 절차에 의해 받아 들이고 Module 정보 확인
// 1. Host  => Module : Module Information Request명령 전송
// 2. Host <=  Module : Module 정보 전송
// 3. Host  => Module : Module Setting 정보 전송
// 4. Host <=  Module : Ack/Nack
// 5. Host =>  Module : Aux Information Request 명령 전송
// 6. Host <=  Module : Aux 정보 전송
// 7. Host =>  Module : Can Information Request 명령 전송
// 8. Host <=  Module : Can 정보 전송
// 9. Host =>  Module : Can Trans Information Request 명령 전송
// 10. Host <=  Module : Can Trans 정보 전송
//int	ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hEvent)
int	ConnectionSequence(CWizReadWriteSocket *pSocket, HANDLE *hEvent, HWND hMsgWnd) //ksj 20201209 : 메세지 핸들 인자 추가
{
	int nRtn = 0;
	int nModuleIndex = 0;
	char msg[128];
	char szPacketLogType[128]={0,}; //ksj 20210107
	BYTE * Buffer = new BYTE[3000000];		//ljb 임시 3000000 -> 원래 1000000
	

	SFT_MSG_HEADER msgHeader;

	WSANETWORKEVENTS wsEvents;
	SFT_MD_SYSTEM_DATA sysData;
	ZeroMemory(&sysData, sizeof(sysData));

	ModuleConnecting(nModuleIndex, hMsgWnd, 1); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 1단계로 통지

	SFT_RESPONSE	response;	
	TRACE("\n\r//------------[모듈접속정보] Send Version Information Request Command--------------------//\n\r");		
	//////////////////// [모듈접속정보] 1. 시스템 정보 요청   /////////////////////
	ZeroMemory(&msgHeader, sizeof(msgHeader));
	msgHeader.nCommand = SFT_CMD_INFO_REQUEST;
	nRtn = pSocket->Write(&msgHeader, SizeofHeader());			//Modlule Version Information Request
	sprintf(msg, "--- 1.모듈 정보 요청 Command---");
	WriteLog(msg);
		
//	WritePacketLog(1, (BYTE*)&msgHeader,sizeof(msgHeader),nRtn); //ksj 20210106
	
	if(nRtn != SizeofHeader())
	{ 
		sprintf(msg, "Module Socket Error (모듈 정보 요청 헤더 사이즈 Error) %d\n", WSAGetLastError());
		nRtn = -1;
		goto CONNECTION_FAIL;
	}

	// 응답대기
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT_CONNECTION_SEQ );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)													// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{			
			//nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			nRtn = pSocket->Read(&msgHeader, SizeofHeader(), 1); //ksj 20210107 : 로그 추가

			if(nRtn != SizeofHeader())	//Header Read Fail;
			{
				sprintf(msg, "Module information cmd header read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -2;
				goto CONNECTION_FAIL;
			}

			//Command validate Checking
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "Module information data cmd header fail.(%d)\n", nRtn);
				nRtn = -3;
				goto CONNECTION_FAIL;
			}

			Sleep(200);
			//nRtn = pSocket->Read(&sysData , sizeof(sysData));				//Module Version Info Read
			nRtn = pSocket->Read(&sysData , sizeof(sysData), 1);	 //ksj 20210107 : 로그 추가

			if(nRtn != msgHeader.nLength)	//MsgBody Read Fail
			{
				sprintf(msg, "Module iInformation data read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -4;
				goto CONNECTION_FAIL;
			}
			
// 			UINT	nPortNo;
// 			char	szIP[36];	
// 			pSocket->GetPeerName(szIP, sizeof(szIP), nPortNo);	//Get Client Side Ip Address and Port Number
// 			sprintf(msg, "IP %s에서 Module %d로 접속이 시도 되었습니다.(Protocol Version: 0x%x) /n/r 모듈이름:%s  채널수:%d ", 
// 						  szIP, sysData.nModuleID, sysData.nProtocolVersion
// 						  ,sysData.szModelName,sysData.nInstalledChCount
// 						  );
// 			TRACE("%s",msg);
// 			WriteLog(msg);
			
			nModuleIndex = CheckModuleIDValidate(&sysData);		//Accepted Module Number and Version Check
//#ifdef _PARALLEL_FORCE 전처리기 주석처리		
//yulee 20190705 강제병렬 옵션 처리 Mark //yulee 20190706
if(g_iPType==1)
			{
				//[20201123]Edited By Skh 강제병렬시 2ch -> 1ch or 4ch -> 2ch 표시
				sysData.nInstalledChCount = sysData.nInstalledChCount / 2;	//ljb 20170626 강제 변경
			}
//#endif		
			if(nModuleIndex< 0)									//Accepted Module information error			
			{
				sprintf(msg, "Accepted module does not support. Code(%d).\n", nModuleIndex);
				WriteLog(msg);
				sprintf(msg, "제공 하지 않는 Module이 접속을 시도 합니다. code %d", nModuleIndex);
				nRtn = -5;
				goto CONNECTION_FAIL;
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -6;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{			
			sprintf(msg, "Module Disconnected ::Wrong event detected0 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -7;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{		
		sprintf(msg, "모듈 정보 요청 접속 실패 또는 시간 초과.");
		WriteLog(msg);
		sprintf(msg, " Module Disconnected :: Shutdown befor read module information.\n");
		nRtn = -8;
		goto CONNECTION_FAIL;
	}

	ModuleConnecting(nModuleIndex, hMsgWnd, 2); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 2단계로 통지

	TRACE("	//------------[모듈접속정보] Send Module Set Command --------------------//\n\r");
	//////////////////// [모듈접속정보] 2. 모듈 정보 요청   /////////////////////	
	m_lpModule[nModuleIndex].MakeCmdSerial(&msgHeader);		//serial No 1 Command
	msgHeader.nCommand = SFT_CMD_MD_SET_DATA;
	msgHeader.nLength = sizeof(SFT_MD_SET_DATA);
	TRACE("Command : %d, Serial : %d\r\n", msgHeader.nCommand, msgHeader.lCmdSerial);

	SFT_MD_SET_DATA mdSetData;
	ZeroMemory(&mdSetData, sizeof(mdSetData));

	//nRtn = pSocket->Write(&msgHeader, SizeofHeader());		
	//nRtn += pSocket->Write(&mdSetData, sizeof(SFT_MD_SET_DATA));

	nRtn = pSocket->Write(&msgHeader, SizeofHeader(), 1);  //ksj 20210107 : 로그 추가		
	nRtn += pSocket->Write(&mdSetData, sizeof(SFT_MD_SET_DATA), 1); //ksj 20210107 : 로그 추가

	if(nRtn != msgHeader.nLength+SizeofHeader())
	{
		sprintf(msg, "Module %d :: Write module set data fail. (Code %d)\n", sysData.nModuleID, WSAGetLastError());
		nRtn = -11;
		goto CONNECTION_FAIL;
	}
	//Sleep(500); //<----- ksj 20201209 : 주석처리함. 이거 슬립 위험함. SBC 응답 빠르면 처리 안될 수 있음. 체크해야됨.
	//응답대기
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT_CONNECTION_SEQ);		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)						// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if(nRtn == SOCKET_ERROR)
		{
			sprintf(msg, "Module %d :: Socket Error%d\n", sysData.nModuleID, WSAGetLastError());
			nRtn = -12;
			goto CONNECTION_FAIL;
		}
		TRACE("WSAEnumNetworkEvents() returns %d\n", nRtn);

		if (wsEvents.lNetworkEvents & FD_READ)					//Read Event
		{			
			//nRtn = pSocket->Read(&msgHeader, SizeofHeader());			//Message Head Read
			nRtn = pSocket->Read(&msgHeader, SizeofHeader(), 1);	 //ksj 20210107 : 로그 추가

			if(nRtn != SizeofHeader())
			{
				sprintf(msg, "Module %d :: Module set command response read fail.(%d) (Read=%d/Request=%d)\n", sysData.nModuleID, WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -13;
				goto CONNECTION_FAIL;
			}						
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "Module %d :: Module set command response header fail.(%d)\n", sysData.nModuleID, nRtn);
				nRtn = -14;
				goto CONNECTION_FAIL;
			}

			Sleep(200);			
			nRtn = 0;
			while(nRtn < msgHeader.nLength)
			{
				//nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn);				//Module Version Info Read
				nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn, 1);	 //ksj 20210107 : 로그 추가
			}

			if(nRtn != msgHeader.nLength)
			{
				sprintf(msg, "Module %d :: Receive module set command response fail(%d/%d). (Code %d)\n", sysData.nModuleID, nRtn, msgHeader.nLength, WSAGetLastError());
				nRtn = -15;
				goto CONNECTION_FAIL;
			}
			memcpy(&response, Buffer, sizeof(SFT_RESPONSE));

			if(msgHeader.nCommand == SFT_CMD_RESPONSE && response.nCode == SFT_ACK)
			{ 
				if(m_lpModule[nModuleIndex].SetModuleSystem(&sysData) == FALSE)			//ljb 20150106 모듈정보 설정
				{
					sprintf(msg, "Module %d :: Module setting error.\n", sysData.nModuleID);
					return -16;
				}			
			}
			else
			{
				sprintf(msg, "Module %d :: Module set command receive NACK(%d).\n", sysData.nModuleID, response.nCode);
				nRtn = -17;
				goto CONNECTION_FAIL;
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected1 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -18;
		}
		else	//wrong event or client Close Event
		{
			//sprintf(msg, "Module %d Disconnected :: Wrong event detected1 (Code %d)\n", sysData.nModuleID, wsEvents.lNetworkEvents);

			//ksj 20201110 : 로그 추가
			sprintf(msg, "Module %d Disconnected :: Wrong event detected1 (Code %d,%d)\n", sysData.nModuleID, wsEvents.lNetworkEvents, wsEvents.iErrorCode[0]);
			//ksj end

			nRtn = -19;
			goto CONNECTION_FAIL;
		}
	}
	else	//wrong event or Server Close Event
	{
		sprintf(msg, "Server Disconnected :: Wrong event detected (Code %d)\n", wsEvents.lNetworkEvents);
		nRtn = -20;
		goto CONNECTION_FAIL;
	}

	ModuleConnecting(nModuleIndex, hMsgWnd, 3); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 3단계로 통지

	TRACE("	//------------[모듈접속정보] Send Parallel Info Request Command --------------------//\n\r");
	//////////////////// [모듈접속정보] 3. 병렬 정보 요청   /////////////////////	
	SFT_MD_PARALLEL_DATA ParallelInfo[_SFT_MAX_PARALLEL_COUNT];
	m_lpModule[nModuleIndex].MakeCmdSerial(&msgHeader);		
	msgHeader.nCommand = SFT_CMD_CH_ATTRIBUTE_REQUEST;
	msgHeader.nLength = 0;	
	TRACE("Command : %d, Serial : %d\r\n", msgHeader.nCommand, msgHeader.lCmdSerial);
	
	//nRtn = pSocket->Write(&msgHeader, SizeofHeader());		
	nRtn = pSocket->Write(&msgHeader, SizeofHeader(), 1);  //ksj 20210107 : 로그 추가		

	if(nRtn != SizeofHeader())
	{ 
		sprintf(msg, "Parallel Info Request Error %d\n", WSAGetLastError());
		nRtn = -21;
		goto CONNECTION_FAIL;
	}
	
	//Sleep(500); //<----- ksj 20201209 : 주석처리함. 이거 슬립 위험함. SBC 응답 빠르면 처리 안될 수 있음. 체크해야됨.
	//응답대기
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT_CONNECTION_SEQ );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)													// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{			
			//nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			nRtn = pSocket->Read(&msgHeader, SizeofHeader(), 1);  //ksj 20210107 : 로그 추가
			if(nRtn != SizeofHeader())	//Header Read Fail;
			{
				sprintf(msg, "Parallel information cmd header read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -22;
				goto CONNECTION_FAIL;
			}
			
			//Command validate Checking
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "Parallel information data cmd header fail.(%d)\n", nRtn);
				nRtn = -23;
				goto CONNECTION_FAIL;
			}
			
			Sleep(200);
			//nRtn = pSocket->Read(&ParallelInfo , sizeof(ParallelInfo) * _SFT_MAX_PARALLEL_COUNT);			
			nRtn = pSocket->Read(&ParallelInfo , sizeof(ParallelInfo) * _SFT_MAX_PARALLEL_COUNT, 1);  //ksj 20210107 : 로그 추가			

			if(nRtn != msgHeader.nLength)	//MsgBody Read Fail
			{
				sprintf(msg, "Module Information data read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -24;
				goto CONNECTION_FAIL;
			}					
			if(msgHeader.nCommand == SFT_CMD_CH_ATTRIBUTE_REPLY)
			{ 
				if(m_lpModule[nModuleIndex].SetParallelData(ParallelInfo) == FALSE)
				{
					sprintf(msg, "Module %d :: Parallel setting error.\n", sysData.nModuleID);
					return -25;
				}
			}
			else
			{
				sprintf(msg, "Module %d :: Parallel set command receive error.\n", sysData.nModuleID);
				nRtn = -26;
				goto CONNECTION_FAIL;
			}
		
			
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -27;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong event detected0 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -28;
			goto CONNECTION_FAIL;
		}
	}

	ModuleConnecting(nModuleIndex, hMsgWnd, 4); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 4단계로 통지
	TRACE("	//------------[모듈접속정보] Send Aux Info Request Command --------------------//\n\r");
	//////////////////// [모듈접속정보] 4. 외부 정보 요청   /////////////////////	
	SFT_MD_AUX_INFO_DATA auxInfo;
	SFT_MD_AUX_INFO_DATA_V1015 auxInfo_Ver1015; //ksj 20201013
	SFT_MD_AUX_INFO_DATA_A auxInfo_Ver1003;	
	m_lpModule[nModuleIndex].MakeCmdSerial(&msgHeader);		//serial No 1 Command
	msgHeader.nCommand = SFT_CMD_AUX_INFO_REQUEST;
	msgHeader.nLength = 0;
	TRACE("Command : %d, Serial : %d\r\n", msgHeader.nCommand, msgHeader.lCmdSerial);

	//nRtn = pSocket->Write(&msgHeader, SizeofHeader());		
	nRtn = pSocket->Write(&msgHeader, SizeofHeader(), 1);	 //ksj 20210107 : 로그 추가	

	if(nRtn != SizeofHeader())
	{ 
		sprintf(msg, "Aux Info Request Error %d\n", WSAGetLastError());
		nRtn = -31;
		goto CONNECTION_FAIL;
	}

	//Sleep(500); //<----- ksj 20201209 : 주석처리함. 이거 슬립 위험함. SBC 응답 빠르면 처리 안될 수 있음. 체크해야됨.
	//응답대기
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT_CONNECTION_SEQ );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)													// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{			
			//nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			nRtn = pSocket->Read(&msgHeader, SizeofHeader(), 1);  //ksj 20210107 : 로그 추가

			if(nRtn != SizeofHeader())	//Header Read Fail;
			{
				sprintf(msg, "Aux information cmd header read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -32;
				goto CONNECTION_FAIL;
			}
				
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "Aux information data cmd header fail.(%d)\n", nRtn);
				nRtn = -33;
				goto CONNECTION_FAIL;
			}
			
			Sleep(200);
			nRtn = 0;
			while(nRtn < msgHeader.nLength)
			{
				//nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn);				//Module Version Info Read
				nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn, 1); //ksj 20210107 : 로그 추가
			}			

			if(nRtn != msgHeader.nLength)	//MsgBody Read Fail
			{
				sprintf(msg, "Module iInformation data read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -34;
				goto CONNECTION_FAIL;
			}
			
			SFT_MD_SYSTEM_DATA* pSys = m_lpModule[nModuleIndex].GetModuleSystem();
			//if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION) //100D 버전 이하.
			if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //1015 버전 이하. //ksj 20200121
				memcpy(&auxInfo_Ver1003, Buffer, sizeof(auxInfo_Ver1003));
			else if(pSys->nProtocolVersion == _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : V1015 호환기능 추가
				memcpy(&auxInfo_Ver1015, Buffer, sizeof(auxInfo_Ver1015));
			else//v1016
				memcpy(&auxInfo, Buffer, sizeof(auxInfo));
			
			
			if(msgHeader.nCommand == SFT_CMD_AUX_INFO_DATA )
			{ 
				//if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION) //100D 버전 이하.
				if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //1015 버전 이하. //ksj 20200121.
				{
					if(m_lpModule[nModuleIndex].SetInstalledAuxDataA(&auxInfo_Ver1003) == FALSE)
					{
						sprintf(msg, "Module %d :: Aux setting error.\n", sysData.nModuleID);
						delete [] Buffer;
						return -35;
					}
					delete [] Buffer;
					CString strVer;
					strVer.Format("%x", pSys->nProtocolVersion);
		
					return nModuleIndex;
				}
				else if (pSys->nProtocolVersion == _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : V1015 버전 호환 처리 추가.
				{
					if(g_iPType==1)
					{
						//ljb 20171013  Aux 할당 채널을 변경 3ch -> 2ch  start /////////////////
						for (int nPos=0; nPos < _SFT_MAX_MAPPING_AUX; nPos++)
						{
							if (auxInfo_Ver1015.auxData[nPos].chNo == 3) auxInfo_Ver1015.auxData[nPos].chNo = 2;
						}
						//ljb 20171013  Aux 할당 채널을 변경 3ch -> 2ch  end /////////////////
					}
					if(m_lpModule[nModuleIndex].SetInstalledAuxData1015(&auxInfo_Ver1015) == FALSE) 
					{
						sprintf(msg, "Module %d :: Aux setting error.\n", sysData.nModuleID);
						delete [] Buffer;
						return -35;
					}			
				}
				else //v1016
				{
					if(g_iPType==1)
					{
						//ljb 20171013  Aux 할당 채널을 변경 3ch -> 2ch  start /////////////////
						for (int nPos=0; nPos < _SFT_MAX_MAPPING_AUX; nPos++)
						{
							if (auxInfo.auxData[nPos].chNo == 3) auxInfo.auxData[nPos].chNo = 2;
						}
						//ljb 20171013  Aux 할당 채널을 변경 3ch -> 2ch  end /////////////////
					}
					if(m_lpModule[nModuleIndex].SetInstalledAuxData(&auxInfo) == FALSE) 
					{
						sprintf(msg, "Module %d :: Aux setting error.\n", sysData.nModuleID);
						delete [] Buffer;
						return -35;
					}			
				}
					
			}
			else
			{
				sprintf(msg, "Module %d :: Aux set command receive error.\n", sysData.nModuleID);
				nRtn = -36;
				goto CONNECTION_FAIL;
			}			
			
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -37;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong event detected0 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -38;
			goto CONNECTION_FAIL;
		}
	}

	ModuleConnecting(nModuleIndex, hMsgWnd, 5); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 5단계로 통지
	TRACE("//------------[모듈접속정보] Send Can Info Request Command --------------------//\n\r");
	//////////////////// [모듈접속정보] 5. 캔정보 정보 요청   /////////////////////	
	m_lpModule[nModuleIndex].MakeCmdSerial(&msgHeader);			
	msgHeader.nCommand = SFT_CMD_CAN_INFO_REQUEST;
	msgHeader.nLength = 0;
	TRACE("Command : %d, Serial : %d\r\n", msgHeader.nCommand, msgHeader.lCmdSerial);

	//nRtn = pSocket->Write(&msgHeader, SizeofHeader());		
	nRtn = pSocket->Write(&msgHeader, SizeofHeader(), 1);  //ksj 20210107 : 로그 추가		
	TRACE("Can Write nRtn = %d\r\n",nRtn);

	if(nRtn != SizeofHeader())
	{ 
		sprintf(msg, "CAN Info Request Error %d\n", WSAGetLastError());
		nRtn = -41;
		goto CONNECTION_FAIL;
	}

	//Sleep(500); //<----- ksj 20201209 : 주석처리함. 이거 슬립 위험함. SBC 응답 빠르면 처리 안될 수 있음. 체크해야됨.
	//응답대기
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT_CONNECTION_SEQ );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)													// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{			
			//nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			nRtn = pSocket->Read(&msgHeader, SizeofHeader(), 1);	 //ksj 20210107 : 로그 추가
			if(nRtn != SizeofHeader())	//Header Read Fail;
			{
				sprintf(msg, "CAN information cmd header read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -42;
				goto CONNECTION_FAIL;
			}						
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "CAN information data cmd header fail.(%d)\n", nRtn);
				nRtn = -43;
				goto CONNECTION_FAIL;
			}			
			Sleep(200);			
			nRtn = 0;
			int Cnt = 0;
			while(nRtn < msgHeader.nLength)
			{
				//nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn);				//Module Version Info Read
				nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn, 1);	 //ksj 20210107 : 로그 추가
				Cnt++;
			}
								
			TRACE("pSocket read nRtn = %d\r\n",nRtn);
			TRACE("msgHeader length = %d\r\n",msgHeader.nLength);
			
			if(nRtn != msgHeader.nLength)	//MsgBody Read Fail
			{
				sprintf(msg, "Module Information data read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -44;
				goto CONNECTION_FAIL;
			}
			sprintf(msg, "Can recevive data read.(Read=%d/Request=%d) V %d, I %d, Module ID =  %d, version 0x%x\n", nRtn, msgHeader.nLength, 
				sysData.nCurrentSpec, sysData.nVoltageSpec,
				sysData.nModuleID ,
				sysData.nProtocolVersion);
			WriteLog(msg);

			TRACE("%s",msg);

			SFT_MD_CAN_INFO_DATA *pCanRecvInfo = new SFT_MD_CAN_INFO_DATA;			

			if(sysData.nProtocolVersion <= _SFT_PROTOCOL_VERSION6)	//0x1005이하 20081112
			{
				SFT_MD_CAN_INFO_DATA1 *pCanData = new SFT_MD_CAN_INFO_DATA1;
				memcpy(pCanData, Buffer, sizeof(SFT_MD_CAN_INFO_DATA1));

				memcpy(pCanRecvInfo->installedCanCount, pCanData->installedCanCount, sizeof(pCanData->installedCanCount));
				memcpy(pCanRecvInfo->canSetAllData.canCommonData, pCanData->canSetAllData.canCommonData, sizeof(pCanData->canSetAllData.canCommonData));
				for(int a= 0; a<_SFT_MAX_INSTALL_CH_COUNT; a++)
				{
					for(int b=0; b<_SFT_MAX_MAPPING_CAN; b++)
					{
						pCanRecvInfo->canSetAllData.canSetData[a][b].canType			= pCanData->canSetAllData.canSetData[a][b].canType			;				//0: unused, 1:master, 2:slave
						pCanRecvInfo->canSetAllData.canSetData[a][b].byte_order			= pCanData->canSetAllData.canSetData[a][b].byte_order		;			//0:intel, 1:motolora
						pCanRecvInfo->canSetAllData.canSetData[a][b].data_type			= pCanData->canSetAllData.canSetData[a][b].data_type		;		//0:signed, 1:unsigned, 2:string
						pCanRecvInfo->canSetAllData.canSetData[a][b].factor_multiply	= pCanData->canSetAllData.canSetData[a][b].factor_multiply;		//+ Value : 곱하기, - Value : 나누기
						pCanRecvInfo->canSetAllData.canSetData[a][b].startBit			= pCanData->canSetAllData.canSetData[a][b].startBit		;
						pCanRecvInfo->canSetAllData.canSetData[a][b].bitCount			= pCanData->canSetAllData.canSetData[a][b].bitCount		;
						pCanRecvInfo->canSetAllData.canSetData[a][b].canID				= pCanData->canSetAllData.canSetData[a][b].canID			;			//hex value
						memcpy(pCanRecvInfo->canSetAllData.canSetData[a][b].name, pCanData->canSetAllData.canSetData[a][b].name, sizeof(pCanData->canSetAllData.canSetData[a][b].name));	
						pCanRecvInfo->canSetAllData.canSetData[a][b].fault_upper		= pCanData->canSetAllData.canSetData[a][b].fault_upper	;
						pCanRecvInfo->canSetAllData.canSetData[a][b].fault_lower		= pCanData->canSetAllData.canSetData[a][b].fault_lower	;
						pCanRecvInfo->canSetAllData.canSetData[a][b].end_upper			= pCanData->canSetAllData.canSetData[a][b].end_upper		;
						pCanRecvInfo->canSetAllData.canSetData[a][b].end_lower			= pCanData->canSetAllData.canSetData[a][b].end_lower		;
						pCanRecvInfo->canSetAllData.canSetData[a][b].factor_Offset		= 0.0f	;
						pCanRecvInfo->canSetAllData.canSetData[a][b].c_Reserved			= 0;
						pCanRecvInfo->canSetAllData.canSetData[a][b].ui_Reserved		= 0;		//only sbc use
					}
				}
				delete pCanData;
			}
			else //0x1005이상
			{				
				sprintf(msg, "*******Can 정보 수신 완료 ver 0x%x\n", sysData.nProtocolVersion);
				WriteLog(msg);
				memcpy(pCanRecvInfo, Buffer, sizeof(SFT_MD_CAN_INFO_DATA));	
//#ifdef _PARALLEL_FORCE 전처리기 주석처리			
//yulee 20190705 강제병렬 옵션 처리 Mark //yulee 20190706
if(g_iPType==1)
				{
					//ksj 20171025 : 주석처리
					//ljb 20170825 add ch3 CAN info 를 ch2로 복사 함. start
					pCanRecvInfo->installedCanCount[1] = pCanRecvInfo->installedCanCount[2];
					memcpy(pCanRecvInfo->canSetAllData.canCommonData[1], pCanRecvInfo->canSetAllData.canCommonData[2], sizeof(SFT_CAN_COMMON_DATA)*2);	
					memcpy(pCanRecvInfo->canSetAllData.canSetData[1], pCanRecvInfo->canSetAllData.canSetData[2], sizeof(SFT_CAN_SET_DATA)*512);	
					//ljb 20170825 add ch3 CAN info 를 ch2로 복사 함. end
}
//#endif				
				sprintf(msg, "can installedCanCount %d,%d,%d,%d",pCanRecvInfo->installedCanCount[0],pCanRecvInfo->installedCanCount[1],pCanRecvInfo->installedCanCount[2],pCanRecvInfo->installedCanCount[3] );
				WriteLog(msg);

			}

			TRACE("msgHeader nCommand = %d\r\n",msgHeader.nCommand);			
			if(msgHeader.nCommand == SFT_CMD_CAN_INFO_RESPONE)
			{ 
				if(m_lpModule[nModuleIndex].SetInstalledCanData(pCanRecvInfo) == FALSE)
				{
					sprintf(msg, "Module %d :: CAN setting error.\n", sysData.nModuleID);
					delete pCanRecvInfo;
					nRtn =  -45;
					goto CONNECTION_FAIL;
				}

				TRACE("모듈 %d CAN(SetInstalledCanData) 접속 완료\r\n ",sysData.nModuleID);
				delete pCanRecvInfo;			
			}
			else
			{
				TRACE("fail Command : %d\r\n", msgHeader.nCommand);
				sprintf(msg, "Module %d :: CAN set command receive error.\n", sysData.nModuleID);
				nRtn = -46;
				delete pCanRecvInfo;
				goto CONNECTION_FAIL;
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -47;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong event detected0 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -48;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{
		sprintf(msg, "Module Disconnected :: Shutdown befor read module information.\n");
		nRtn = -49;
		goto CONNECTION_FAIL;
	}

	ModuleConnecting(nModuleIndex, hMsgWnd, 6); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 6단계로 통지
	TRACE("//------------[모듈접속정보] Send Can Trans Info Request Command --------------------//\n\r");
	//////////////////// [모듈접속정보] 6. 캔 전송? 정보 요청   /////////////////////	
	m_lpModule[nModuleIndex].MakeCmdSerial(&msgHeader);		//serial No 1 Command
	msgHeader.nCommand = SFT_CMD_CAN_TRANS_INFO_REQUEST;
	msgHeader.nLength = 0;
	TRACE("Trans Command : %d, Serial : %d\r\n", msgHeader.nCommand, msgHeader.lCmdSerial);

	//nRtn = pSocket->Write(&msgHeader, SizeofHeader());
	nRtn = pSocket->Write(&msgHeader, SizeofHeader(), 1);  //ksj 20210107 : 로그 추가
	TRACE("Can trans Write nRtn = %d\r\n",nRtn);

	if(nRtn != SizeofHeader())
	{ 
		TRACE("sizeofheader error");
		sprintf(msg, "CAN Trans Info Request Error %d\n", WSAGetLastError());
		nRtn = -51;
		goto CONNECTION_FAIL;
	}
	//Sleep(500);	//<----- ksj 20201209 : 주석처리함. 이거 슬립 위험함. SBC 응답 빠르면 처리 안될 수 있음. 체크해야됨.
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT_CONNECTION_SEQ );		//Waiting Data Read or Shutdowm	
	if(nRtn == WAIT_OBJECT_0)													// Client socket event FD_READ or Shutdown
	{
		Sleep(200);		
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event		
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{						
			//nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read		
			nRtn = pSocket->Read(&msgHeader, SizeofHeader(), 1); //ksj 20210107 : 로그 추가	
			if(nRtn != SizeofHeader())	//Header Read Fail;
			{
				sprintf(msg, "CAN Trans information cmd header read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -52;
				goto CONNECTION_FAIL;
			}
			
			//Command validate Checking
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "CAN Trans information data cmd header fail.(%d)\n", nRtn);
				nRtn = -53;
				goto CONNECTION_FAIL;
			}
						
			Sleep(200);									
			nRtn = 0;
			TRACE("nLength = %d \n",msgHeader.nLength);
			while(nRtn < msgHeader.nLength)
			{
				//nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn);				//Module Version Info Read
				nRtn += pSocket->Read(Buffer+nRtn , msgHeader.nLength - nRtn, 1);	 //ksj 20210107 : 로그 추가
			}
						
			if(nRtn != msgHeader.nLength)	//MsgBody Read Fail
			{
				sprintf(msg, "Module iInformation data read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -54;
				goto CONNECTION_FAIL;
			}
			
			SFT_MD_CAN_TRANS_INFO_DATA * pCanTransInfo = NULL;
			pCanTransInfo = new SFT_MD_CAN_TRANS_INFO_DATA;
			if(sysData.nProtocolVersion <= _SFT_PROTOCOL_VERSION6)	//0x1005이하 20081112
			{
				SFT_MD_CAN_TRANS_INFO_DATA1 *pCanTransData = new SFT_MD_CAN_TRANS_INFO_DATA1;
				memcpy(pCanTransData, Buffer, sizeof(SFT_MD_CAN_TRANS_INFO_DATA1));

				memcpy(pCanTransInfo->installedCanCount, pCanTransData->installedCanCount, sizeof(pCanTransData->installedCanCount));
				memcpy(pCanTransInfo->canSetAllData.canCommonData, pCanTransData->canSetAllData.canCommonData, sizeof(pCanTransData->canSetAllData.canCommonData));
				for(int a= 0; a<_SFT_MAX_INSTALL_CH_COUNT; a++)
				{
					for(int b=0; b<_SFT_MAX_MAPPING_CAN; b++)
					{
						pCanTransInfo->canSetAllData.canSetData[a][b].canType			= pCanTransData->canSetAllData.canSetData[a][b].canType		;		//0: unused, 1:master, 2:slave
						pCanTransInfo->canSetAllData.canSetData[a][b].byte_order		= pCanTransData->canSetAllData.canSetData[a][b].byte_order	;		//0:intel, 1:motolora
						pCanTransInfo->canSetAllData.canSetData[a][b].data_type			= pCanTransData->canSetAllData.canSetData[a][b].data_type		;		//0:signed, 1:unsigned, 2:string, 3:float , 4: bit
						pCanTransInfo->canSetAllData.canSetData[a][b].factor_multiply	= pCanTransData->canSetAllData.canSetData[a][b].factor_multiply;		//+ Value : 곱하기, - Value : 나누기
						pCanTransInfo->canSetAllData.canSetData[a][b].startBit			= pCanTransData->canSetAllData.canSetData[a][b].startBit		;
						pCanTransInfo->canSetAllData.canSetData[a][b].bitCount			= pCanTransData->canSetAllData.canSetData[a][b].bitCount		;
						pCanTransInfo->canSetAllData.canSetData[a][b].canID				= pCanTransData->canSetAllData.canSetData[a][b].canID			;		//hex value
						pCanTransInfo->canSetAllData.canSetData[a][b].send_time			= pCanTransData->canSetAllData.canSetData[a][b].send_time		;		//전송속도(ms)
						memcpy(pCanTransInfo->canSetAllData.canSetData[a][b].name, pCanTransData->canSetAllData.canSetData[a][b].name, sizeof(pCanTransData->canSetAllData.canSetData[a][b].name));	
						pCanTransInfo->canSetAllData.canSetData[a][b].l_Reserved		= 0.0f	;		
						pCanTransInfo->canSetAllData.canSetData[a][b].factor_Offset		= 0.0f	;
						pCanTransInfo->canSetAllData.canSetData[a][b].dlc				= 0;		//ljb 20181119
						pCanTransInfo->canSetAllData.canSetData[a][b].ui_Reserved		= 0;
					}

				}
				delete pCanTransData;
			}
			else
			{				
				memcpy(pCanTransInfo, Buffer, sizeof(SFT_MD_CAN_TRANS_INFO_DATA));
//#ifdef _PARALLEL_FORCE 전처리기 주석처리
//yulee 20190705 강제병렬 옵션 처리 Mark

				if(g_iPType==1)
				{
					//ksj 20171025 : 주석처리
					//ljb 20170825 add ch3 CAN info 를 ch2로 복사 함. start
					pCanTransInfo->installedCanCount[1] = pCanTransInfo->installedCanCount[2];
					memcpy(pCanTransInfo->canSetAllData.canCommonData[1], pCanTransInfo->canSetAllData.canCommonData[2], sizeof(SFT_CAN_COMMON_DATA)*2);	
					memcpy(pCanTransInfo->canSetAllData.canSetData[1], pCanTransInfo->canSetAllData.canSetData[2], sizeof(SFT_CAN_SET_DATA)*512);	
					//ljb 20170825 add ch3 CAN info 를 ch2로 복사 함. end
				}	
//#endif
			}
			
			if(msgHeader.nCommand == SFT_CMD_CAN_TRANS_INFO_RESPONE)
			{ 
				TRACE("msgHeader nCommand \r\n",nRtn);
				if(m_lpModule[nModuleIndex].SetInstalledTransCanData(pCanTransInfo) == FALSE)
				{
					TRACE("setinstalled transcan data 실패\r\n");
					sprintf(msg, "Module %d :: CAN Trans setting error.\n", sysData.nModuleID);
					nRtn = -55;
					delete pCanTransInfo;
					goto CONNECTION_FAIL;
				}				
				m_lpModule->RestCmdSequenceNo();
				
				TRACE("TRANS 처리 완료\r\n");
				delete [] Buffer;
				delete pCanTransInfo;	

				ModuleConnecting(nModuleIndex, hMsgWnd, 0); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 성공으로 0 리턴.
				return nModuleIndex;
			}
			else
			{
				TRACE("msgHeader nCommand not ack \r\n",nRtn);
				sprintf(msg, "Module %d :: CAN set command receive error.\n", sysData.nModuleID);
				nRtn = -56;
				delete pCanTransInfo;
				goto CONNECTION_FAIL;
			}			
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -57;

			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong event detected0 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -58;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{
		sprintf(msg, "Module Disconnected :: Shutdown befor read module information.\n");
		nRtn = -59;
		goto CONNECTION_FAIL;
	}

	delete [] Buffer; //2014.08.21
	TRACE("//-----------[모듈접속정보] ConnectionSequence 처리 완료---------------//\r\n");
//	delete pCanTransInfo;

	ModuleConnecting(nModuleIndex, hMsgWnd, 0); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 성공으로 0 리턴.
	
CONNECTION_FAIL:
	WriteLog(msg);
	TRACE("connection fail =%s\r\n",msg);
	delete [] Buffer;
//	if(pCanTransInfo != NULL)
//		delete pCanTransInfo;
//	if(pCanRecvInfo)
//		delete pCanRecvInfo;
	TRACE(msg);


	ModuleConnecting(nModuleIndex, hMsgWnd, nRtn); //ksj 20201209 : 현재 ConnectionSeq 진행 상태 최종 rtn 값 통지 (minus 값은 실패)

	return nRtn;
}

int	ConnectionSequencePort2(CWizReadWriteSocket *pSocket, HANDLE *hEvent)
{
	int nRtn;
	int nModuleIndex = 0;
	char msg[128];
	BYTE * Buffer = new BYTE[3000000];		//ljb 임시 3000000 -> 원래 1000000

	SFT_MSG_HEADER msgHeader;

	WSANETWORKEVENTS wsEvents;
	SFT_MD_SYSTEM_DATA sysData;
	ZeroMemory(&sysData, sizeof(sysData));

//	SFT_RESPONSE	response;
TRACE("\n\r//------------Send Version Information Request Command--------------------//\n\r");

	//Serial No 0 Command is => SFT_CMD_INFO_REQUEST
// 	ZeroMemory(&msgHeader, sizeof(msgHeader));
// 	msgHeader.nCommand = SFT_CMD_INFO_REQUEST;
// 	nRtn = pSocket->Write(&msgHeader, SizeofHeader());			//Modlule Version Information Request
// 	if(nRtn != SizeofHeader())
// 	{ 
// 		sprintf(msg, "Module Socket Error %d\n", WSAGetLastError());
// 		nRtn = -1;
// 		goto CONNECTION_FAIL;
// 	}

	//------------wait Version Information Response Header--------------------//
	nRtn = ::WaitForMultipleObjects(2, hEvent, FALSE, SFT_MSG_TIMEOUT_CONNECTION_SEQ );		//Waiting Data Read or Shutdowm
	if(nRtn == WAIT_OBJECT_0)													// Client socket event FD_READ or Shutdown
	{
		Sleep(200);
		nRtn = ::WSAEnumNetworkEvents(pSocket->H(), hEvent[0], &wsEvents);	//Client side Socket Event
		if (wsEvents.lNetworkEvents & FD_READ)								//Read Event
		{
			//-----------------Read Module Information and Check validate ---------------------//
			//nRtn = pSocket->Read(&msgHeader, SizeofHeader());				//Message Head Read
			nRtn = pSocket->Read(&msgHeader, SizeofHeader(), 0);	//ksj 20210107 : 로그 레벨 추가
			if(nRtn != SizeofHeader())	//Header Read Fail;
			{
				sprintf(msg, "Module information cmd header read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, SizeofHeader());
				nRtn = -2;
				goto CONNECTION_FAIL;
			}

			//Command validate Checking
			if((nRtn = CheckMsgHeader( &msgHeader)) < 0)
			{
				sprintf(msg, "Module information data cmd header fail.(%d)\n", nRtn);
				nRtn = -3;
				goto CONNECTION_FAIL;
			}

			Sleep(200);
			//nRtn = pSocket->Read(&sysData , sizeof(sysData));				//Module Version Info Read
			nRtn = pSocket->Read(&sysData , sizeof(sysData), 0);	//ksj 20210107 : 로그 레벨 추가
			if(nRtn != msgHeader.nLength)	//MsgBody Read Fail
			{
				sprintf(msg, "Module iInformation data read fail.(%d) (Read=%d/Request=%d)\n", WSAGetLastError(), nRtn, msgHeader.nLength);
				nRtn = -4;
				goto CONNECTION_FAIL;
			}

// 			UINT	nPortNo;
// 			char	szIP[36];	
// 			pSocket->GetPeerName(szIP, sizeof(szIP), nPortNo);	//Get Client Side Ip Address and Port Number
// 			sprintf(msg, "IP %s에서 Module %d로 접속이 시도 되었습니다.(Protocol Version: 0x%x)", 
// 						  szIP, sysData.nModuleID, sysData.nProtocolVersion);
// 			WriteLog(msg);
			
			nModuleIndex = CheckModuleIDValidate(&sysData);		//Accepted Module Number and Version Check
//			sprintf(msg, "***** Module version. 0x%x.\n", sysData.nProtocolVersion);
//			WriteLog(msg);
			if(nModuleIndex< 0)									//Accepted Module information error			
			{
				sprintf(msg, "Accepted module does not support. Code(%d).\n", nModuleIndex);
				WriteLog(msg);
				sprintf(msg, "제공 하지 않는 Module이 접속을 시도 합니다. code %d", nModuleIndex);
				nRtn = -5;
				goto CONNECTION_FAIL;
				return nModuleIndex;	//ljb 20171201 add
			}
		}
		else if(wsEvents.lNetworkEvents & FD_CLOSE)		//Client Disconnected
		{
			sprintf(msg, "Module Closed ::Module close event detected %d.\n", wsEvents.lNetworkEvents);
			nRtn = -6;
			goto CONNECTION_FAIL;
		}
		else	//wrong event or client Close Event
		{
			sprintf(msg, "Module Disconnected ::Wrong event detected0 %d.\n", wsEvents.lNetworkEvents);
			nRtn = -7;
			goto CONNECTION_FAIL;
		}
	}
	else	//Wrong Event or Time Out
	{
		sprintf(msg, "Module Disconnected :: Shutdown befor read module information.\n");
		nRtn = -8;
		goto CONNECTION_FAIL;
	}

	delete [] Buffer; //2014.08.06
	return nModuleIndex;

CONNECTION_FAIL:
	WriteLog(msg);
	TRACE("connection fail =%s\r\n",msg);
	delete [] Buffer;
	//	if(pCanTransInfo != NULL)
	//		delete pCanTransInfo;
	//	if(pCanRecvInfo)
	//		delete pCanRecvInfo;
	
	TRACE(msg);
	return nRtn;
}

int CWizRawSocketListener::SetListenPort(int nPort)
{
	m_nPort = nPort;
	return TRUE;
}

/*
void CClientThread::ProcessingCalResultData(BYTE* pReceivedBuffer, 
											int nSize, 
											ULONG ulReceivedCmdSerial)
{
	// 수신한 Calibration Result Packet에 대한 Acknowledge를 전송함
	S_CMD_ACK_HEADER cmdHeader;
	ZeroMemory(&cmdHeader, sizeof(S_CMD_ACK_HEADER));

	cmdHeader.chCmd					= PACKET_ACK_RESULT;
	cmdHeader.lDataSize				= sizeof(int) * 2;
	cmdHeader.nResultCode			= ACK_CODE_ACKNOWLEDGE ;
	cmdHeader.lCmdSendSerial		= m_AckID.SetID();
	cmdHeader.lCmdResponseSerial	= ulReceivedCmdSerial;

	int nSendSize = m_pSocket->Send(&cmdHeader, sizeof(S_CMD_ACK_HEADER));
	if( nSendSize < sizeof(S_CMD_ACK_HEADER) )
	{
		CString strLogMsg;
		strLogMsg.Format("Module %d::Send Calibration Result Data Ack Error", m_nModuleID);
		PrintLog(strLogMsg);
	}

	// 새로운 버퍼를 생성함. (추후 Main Window에서 꼭 삭제해야 함)
	BYTE *pTempBuffer = new BYTE [nSize];
	memcpy(pTempBuffer, pReceivedBuffer, nSize);

	// Send to Main Window
	::PostMessage(m_hMsgHwnd, 
				  EP_WM_SET_CALRESULT_DATA, 
				  (WPARAM)pTempBuffer, 
				  (LPARAM)m_nModuleID);

	TRACE("\t\t\t\t▷ Module %d::Send Acknowledge (S/N:%d)\n", m_nModuleID, cmdHeader.lCmdSendSerial);
}

*/
