/////////////////////////////////////////////////////////////////////
// Class Creator Version 2.0.000 Copyrigth (C) Poul A. Costinsky 1994
///////////////////////////////////////////////////////////////////
// Implementation File ThreadDispatcher.cpp
// class CWizThreadDispatcher
//
// 15/07/1996 11:54                             Author: Poul
///////////////////////////////////////////////////////////////////


#include "stdafx.h"

#include "RawSocketServerWorker.h"
#include "ThreadDispatcher.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif
#define new DEBUG_NEW

///////////////////////////////////////////////////////////////////
// class CWizThreadDispatcher
//*****************************************************************
// Default Constructor
CWizThreadDispatcher::CWizThreadDispatcher(CWizMultiThreadedWorker &rWorker, int MaxThreads)
	: m_nMaxThreads(MaxThreads),
	  m_rWorker(rWorker),
	  m_ShutDownEvent(TRUE),
	  m_HasDataEvent (FALSE),
	  m_StartedDataTreatEvent(FALSE),
	  m_ahWorkersHandles(NULL),
	  m_hThisThread(NULL),
	  m_pWnd (NULL),
	  m_nMessage(0)
{
	m_ahStartedTreatmentEvents[0] = m_ShutDownEvent.m_h;
	m_ahStartedTreatmentEvents[1] = m_StartedDataTreatEvent.m_h;
	m_ahWorkersHandles = new HANDLE[m_nMaxThreads + 1];
	if (m_ahWorkersHandles == NULL)
		AfxThrowMemoryException();

	for (register int i = 0; i < m_nMaxThreads; i++)
		m_ahWorkersHandles[i] = NULL;
}

//*****************************************************************
// Destructor
CWizThreadDispatcher::~CWizThreadDispatcher()
{
	delete [] m_ahWorkersHandles;
	Stop(FALSE);
}

//*****************************************************************
void	CWizThreadDispatcher::Start()
{
	CWinThread* pThr = AfxBeginThread(CWizThreadDispatcherFun, this);
	if (pThr == NULL)	AfxThrowMemoryException();
}

//*****************************************************************
void	CWizThreadDispatcher::Stop(BOOL bWait)
{
	m_ShutDownEvent.Set();
	if (bWait)	WaitShutDown();
}

//*****************************************************************
void	CWizThreadDispatcher::MsgStop (CWnd* pWnd, UINT Message)
{
	m_pWnd = pWnd;
	m_nMessage = Message;
	Stop(FALSE);
}

//*****************************************************************
void	CWizThreadDispatcher::WaitShutDown()
{
	register int i;
	DWORD ExitCode;
	
	for( int i = 0; i < m_nMaxThreads; i++)
	{
		ExitCode = ::WaitForSingleObject(m_ahWorkersHandles[i], 3000/*INFINITE*/);

		if(ExitCode == WAIT_TIMEOUT)
		{
			TRACE("Thread Exit Wait Time Out\n");
		}
		else if(ExitCode == WAIT_OBJECT_0)
		{
			TRACE("Thread Exited\n");
		}
		else
		{
			TRACE("Thread Exit Fail (%d)\n", ExitCode);
		}

	}
	const int hMax = m_nMaxThreads + 1;
	for( int i = 0; i < hMax; i++)
	{
		while(1) {
			if (!GetExitCodeThread(m_ahWorkersHandles[i], &ExitCode) ||	ExitCode != STILL_ACTIVE)
			{
				break; // from while
			}
		}
	}

	/*
		const int hMax = m_nMaxThreads + 1;
	for (INDEX i = 0; i < hMax; i++)
		{
		while (1)
			{
			const DWORD dwWaitRes = ::MsgWaitForMultipleObjects(1, &(m_ahWorkersHandles[i]), TRUE, INFINITE,QS_ALLINPUT);
			if (dwWaitRes == WAIT_OBJECT_0 + 1)
				{
				MSG msg;
				while (::PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE))
					if(!AfxGetApp()->PumpMessage())
						{
						ASSERT(0);
						ExitProcess(1);
						}
				}
			else
				{
				DWORD ExitCode;
				if (!GetExitCodeThread(m_ahWorkersHandles[i], &ExitCode) ||
					 ExitCode != STILL_ACTIVE)
					break; // from while
				}
			} // while
		} // for
		*/
}

//해당 Port Wait(Listen)하기 위한 Thread 
//*****************************************************************
UINT AFX_CDECL CWizThreadDispatcherFun(LPVOID pParam)
{
	ASSERT(pParam != NULL);
	return ((CWizThreadDispatcher *)pParam)->Run();
}

//Server에서 Port 감시 Thread
//*****************************************************************
UINT 	CWizThreadDispatcher::Run()
{
	
	CWizMultiThreadedWorker::Stack stack(m_rWorker);

	m_hThisThread = AfxGetThread()->m_hThread;
	
	//가장 마지막 Handel은 Server 자체 Thread에 대한 Handle
	m_ahWorkersHandles[m_nMaxThreads] = m_hThisThread;	

	//---------------------------------------------------------
	// Start all working threads
	for (register int i = 0; i < m_nMaxThreads; i++)
	{
		WorkerThread* pTr = new WorkerThread(m_rWorker, 
											 m_HasDataEvent.m_h,
											 m_StartedDataTreatEvent.m_h,
											 m_ShutDownEvent.m_h);
		if (pTr == NULL)	AfxThrowMemoryException();

		CWinThread* pThr = AfxBeginThread(CWizThreadDispatcher_WorkerThread_Fun, pTr);
		if (pThr == NULL)	AfxThrowMemoryException();

		m_ahWorkersHandles[i] = pThr->m_hThread;
	}

	//---------------------------------------------------------
	while (1)
	{
		if (!m_rWorker.WaitForData(m_ShutDownEvent.m_h))
		{
			m_ShutDownEvent.Set();	//goto end;
			return 0;
		}
		m_HasDataEvent.Set();		//Port에 접속이 감지 되었을시 Handle 처리를 위해 TreateData가 호출 되도록 Evnet Set

		const DWORD res = 
			::WaitForMultipleObjects(StartedTreatmentEventsCount, m_ahStartedTreatmentEvents, FALSE, INFINITE);

		switch (res)
		{
			case WAIT_OBJECT_0:				// Server Shut down!  goto end;
				return 0;
			case (WAIT_OBJECT_0 + 1):		// Worker thread started to treat data
				break;						// 현재 받아들인 Socket Handle을 Accept처리를 완료 하고 다시 Port Listen 하였다 
			case WAIT_FAILED: // something wrong!
				throw XWaitFailed();
			default:
				ASSERT(0);
		}
	} 
	return 0;
}
//*****************************************************************
///////////////////////////////////////////////////////////////////
CWizThreadDispatcher::WorkerThread::WorkerThread(CWizMultiThreadedWorker &rWorker, 
				HANDLE hDataReadyEvent,
				HANDLE hStartedTreatEvent,
				HANDLE hShutDownEvent)
	: m_rWorker (rWorker),
	  m_hStartedTreatEvent (hStartedTreatEvent)
{
	m_hDataWait[0] = hShutDownEvent;
	m_hDataWait[1] = hDataReadyEvent;
}

//*****************************************************************
UINT CWizThreadDispatcher::WorkerThread::Run()
{
	while (1)	//CWizThreadDispatcher::Run() 에서 Port에 접속이 시도된 Handle에 대한 처리 를 하기 위한 thread
	{
		const DWORD res = 
			::WaitForMultipleObjects(DataWaitHCount, m_hDataWait, FALSE, INFINITE);

		switch (res)
		{
			case WAIT_FAILED: // something wrong!
				throw CWizThreadDispatcher::XWaitFailed();
			case WAIT_OBJECT_0: // Shut down!
				return 0;
			case (WAIT_OBJECT_0 + 1): // Has data to treat
				TRACE("Module connect event \n");
				if (!m_rWorker.TreatData(m_hDataWait[0], m_hStartedTreatEvent))
					return 0;
				break;
			default:
				ASSERT(0);
		}
	} // while 1
	return 0;
}

//Port에 접속 시도된 Socket Handle 처리를 위한 Thread
//*****************************************************************
UINT AFX_CDECL CWizThreadDispatcher_WorkerThread_Fun(LPVOID pParam)
{
	CWizThreadDispatcher::WorkerThread* pWorker = (CWizThreadDispatcher::WorkerThread*)pParam;
	ASSERT(pWorker != NULL);
	UINT res = 0;

	try
	{
		res = pWorker->Run();
	}
	catch(...)
	{
		delete pWorker;
		throw;
	}
	delete pWorker;
	return res;
}
//*****************************************************************
