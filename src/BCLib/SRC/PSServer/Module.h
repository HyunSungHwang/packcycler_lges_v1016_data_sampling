// Module.h: interface for the CModule class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODULE_H__500F2D05_C0A1_4EF0_858D_46B988FD42F3__INCLUDED_)
#define AFX_MODULE_H__500F2D05_C0A1_4EF0_858D_46B988FD42F3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Channel.h"
#include "MinMaxData.h"
#include "Server.h"	// Added by ClassView

class CModule  
{
public:
	//ksj 20201115 : 타임아웃 통신 두절 이슈 관련 로그 함수 추가
	//타임아웃으로 SBC가 자꾸 접속을 끊는데 실제로 타임아웃이 일어날정도로 통신이 없었는지 로깅을 위해 통신 시간 기록.
	//통신 두절 발생했을때 최종 통신 기록을 토대로 로그를 남긴다.

	SYSTEMTIME m_tmStdDisconnectTime; //통신 두절 기준 시간
	SYSTEMTIME GetStdDisconnectTime(){return m_tmStdDisconnectTime;;} //통신 두절 기준 시간

	int m_nRecentRecvAckCmdID1;
	SYSTEMTIME m_tmRecentRecvAckTime1;
	void SetRecentRecvAck1(int nCmdID, SYSTEMTIME& time){ m_nRecentRecvAckCmdID1=nCmdID; m_tmRecentRecvAckTime1=time; }
	SYSTEMTIME	GetRecentRecvAckTime1(){return m_tmRecentRecvAckTime1;} //ksj 20201115 : 가장 마지막으로 수신한 Ack 시간
	int	GetRecentRecvAckCmdID1(){return m_nRecentRecvAckCmdID1;} //ksj 20201115 : 가장 마지막으로 Ack 수신한 Command ID
	int m_nRecentRecvAckCmdID2;
	SYSTEMTIME m_tmRecentRecvAckTime2;
	void SetRecentRecvAck2(int nCmdID, SYSTEMTIME& time){ m_nRecentRecvAckCmdID2=nCmdID; m_tmRecentRecvAckTime2=time; }
	SYSTEMTIME	GetRecentRecvAckTime2(){return m_tmRecentRecvAckTime2;} //ksj 20201115 : 가장 마지막으로 수신한 Ack 시간
	int	GetRecentRecvAckCmdID2(){return m_nRecentRecvAckCmdID2;} //ksj 20201115 : 가장 마지막으로 Ack 수신한 Command ID

	int m_nRecentSendAckCmdID1;
	SYSTEMTIME m_tmRecentSendAckTime1;
	void SetRecentSendAck1(int nCmdID, SYSTEMTIME& time){ m_nRecentSendAckCmdID1=nCmdID; m_tmRecentSendAckTime1=time; }
	SYSTEMTIME	GetRecentSendAckTime1(){return m_tmRecentSendAckTime1;} //ksj 20201115 : 가장 마지막으로 응답한 Ack 시간
	int	GetRecentSendAckCmdID1(){return m_nRecentSendAckCmdID1;} //ksj 20201115 : 가장 마지막으로 Ack 응답한 Command ID
	int m_nRecentSendAckCmdID2;
	SYSTEMTIME m_tmRecentSendAckTime2;
	void SetRecentSendAck2(int nCmdID, SYSTEMTIME& time){ m_nRecentSendAckCmdID2=nCmdID; m_tmRecentSendAckTime2=time; }
	SYSTEMTIME	GetRecentSendAckTime2(){return m_tmRecentSendAckTime2;} //ksj 20201115 : 가장 마지막으로 응답한 Ack 시간
	int	GetRecentSendAckCmdID2(){return m_nRecentSendAckCmdID2;} //ksj 20201115 : 가장 마지막으로 Ack 응답한 Command ID


	int m_nRecentRecvCmdID1;
	SYSTEMTIME m_tmRecentRecvTime1;
	void SetRecentRecvCmd1(int nCmdID, SYSTEMTIME& time){ m_nRecentRecvCmdID1=nCmdID; m_tmRecentRecvTime1=time; }
	SYSTEMTIME GetRecentRecvCmdTime1(){return m_tmRecentRecvTime1;} //ksj 20201115 : 가장 마지막으로 수신한 Cmd 시간
	int	GetRecentRecvCmdID1(){return m_nRecentRecvCmdID1;} //ksj 20201115 : 가장 마지막으로 수신한 Command ID
	int m_nRecentRecvCmdID2;
	SYSTEMTIME m_tmRecentRecvTime2;
	void SetRecentRecvCmd2(int nCmdID, SYSTEMTIME& time){ m_nRecentRecvCmdID2=nCmdID; m_tmRecentRecvTime2=time; }
	SYSTEMTIME GetRecentRecvCmdTime2(){return m_tmRecentRecvTime2;} //ksj 20201115 : 가장 마지막으로 수신한 Cmd 시간
	int	GetRecentRecvCmdID2(){return m_nRecentRecvCmdID2;} //ksj 20201115 : 가장 마지막으로 수신한 Command ID

	int m_nRecentSendCmdID;
	SYSTEMTIME m_tmRecentSendTime;
	void SetRecentSendCmd(int nCmdID, SYSTEMTIME& time){ m_nRecentSendCmdID=nCmdID; m_tmRecentSendTime=time; }
	SYSTEMTIME GetRecentSendCmdTime(){return m_tmRecentSendTime;} //ksj 20201115 : 가장 마지막으로 Command 전송한 시간
	int	GetRecentSendCmdID(){return m_nRecentSendCmdID;} //ksj 20201115 : 가장 마지막으로 전송한 Command ID

	//최종 heartbeat 응답 시간 저장
	int m_nRecentSendHeartBeat;
	SYSTEMTIME m_tmRecentSendHeartBeatTime;
	void SetRecentSendHeartBeat(int nCmdID, SYSTEMTIME& time){ m_nRecentSendHeartBeat=nCmdID; m_tmRecentSendHeartBeatTime=time; }
	SYSTEMTIME GetRecentSendHeartBeatTime(){return m_tmRecentSendHeartBeatTime;} //ksj 20201115 : 가장 마지막으로 Heartbit 응답한 시간
	int	GetRecentSendHeartBeatID(){return m_nRecentSendHeartBeat;} //ksj 20201115 : 가장 마지막으로 전송한 Command ID

	//최종 Heartbeat 수신 시간 저장
	int m_nRecentRecvHeartBeat;
	SYSTEMTIME m_tmRecentRecvHeartBeatTime;
	void SetRecentRecvHeartBeat(int nCmdID, SYSTEMTIME& time){ m_nRecentRecvHeartBeat=nCmdID;  m_tmRecentRecvHeartBeatTime=time; }
	SYSTEMTIME GetRecentRecvHeartBeatTime(){return  m_tmRecentRecvHeartBeatTime;} //ksj 20201115 : 가장 마지막으로 Heartbit 수신한 시간
	int	GetRecentRecvHeartBeatID(){return m_nRecentRecvHeartBeat;} //ksj 20201115 : 가장 마지막으로 전송한 Command ID

	//ksj end//////////////////////////////////////////////////////////////////////////////////////////////////////


	//BOOL bSendScheduleEND; //201912 LG 오창2공장 106호 15번 작업시작 안됨 이슈		
	void SetSendScheduleState(BOOL bState){m_bSendSchedule = bState; } //ksj 20201215
	BOOL GetSendScheduleState(){return m_bSendSchedule; } //ksj 20201215

	BOOL SetInstalledTransCanData(SFT_MD_CAN_TRANS_INFO_DATA * pCANTransInfo);
	SFT_MD_CAN_INFO_DATA * GetInstalledCANData();
	SFT_MD_CAN_TRANS_INFO_DATA * GetInstalledTransCANData();
	BOOL SetInstalledCanData(SFT_MD_CAN_INFO_DATA * pCANInfo);
	SFT_MD_CAN_INFO_DATA m_CANInfoData;
	SFT_MD_CAN_TRANS_INFO_DATA m_CANTransInfoData;
	SFT_MD_PARALLEL_DATA * GetParallelData();
	SFT_MD_PARALLEL_DATA m_ParallelData[_SFT_MAX_PARALLEL_COUNT];
	BOOL SetParallelData(SFT_MD_PARALLEL_DATA * pParallel);
	SFT_MD_AUX_INFO_DATA * GetInstalledAuxData();
	SFT_MD_AUX_INFO_DATA m_AuxInfoData;
	BOOL SetInstalledAuxData(SFT_MD_AUX_INFO_DATA * pAuxInfo);
	BOOL SetInstalledAuxDataA(SFT_MD_AUX_INFO_DATA_A * pAuxInfo);
	BOOL SetInstalledAuxData1015(SFT_MD_AUX_INFO_DATA_V1015 * pAuxInfo); //ksj 20201013
	SYSTEMTIME * GetConnectedTime();
	void SetBuffer(LPVOID lpBuff);
	LPVOID GetBuffer();
	BOOL	SendResponse(LPSFT_MSG_HEADER lpReceivedCmd, int nCode = SFT_ACK);
	BOOL	GetHWChCount();
	int		GetChannelCount();
	void	SetStateData(const SFT_MD_STATE_DATA &stateData);
	void	RestCmdSequenceNo();
	BOOL	MakeCmdSerial(SFT_MSG_HEADER *pMsgHeader);
	LPVOID	ReadData(int nSize);
	BOOL	SendCommand(UINT nCmd, LPSFT_CH_SEL_DATA pChData = NULL, LPVOID lpData = NULL, UINT nSize = 0);
	CChannel *GetChannelData(int nChIndex);		
	WORD	GetChannelState(int nChIndex);
	HANDLE	GetWriteEventHandle();
	HANDLE	GetWriteEventHandle2();
	int		ReadAck();
	//int		ReadAckByCmd(UINT nCmd);
	int		ReadAckByCmd(UINT nCmd, UINT nWaitTime = 0); //ksj 20201215
	void	SetChannelChamberData(int nChIndex, SFT_CHAMBER_VALUE *pChamberData);	//ljb 20091101
	void	SetChannelLoaderData(int nChIndex, SFT_LOADER_VALUE *pLoaderData);	    //ljb 20150427
	void	SetChannelChillerData(int nChIndex, SFT_CHAMBER_VALUE *pChillerData);	//ljb 20170906
	void	SetChannelCellBALData(int nChIndex, SFT_CELLBAL_VALUE *pCellBALData);	//ljb 20170906
	void	SetChannelPwrSupplyData(int nChIndex, SFT_PWRSUPPLY_VALUE *pPwrSupplyData);	//ljb 20170906

	INT		GetModuleID()		{	return m_nModuleID;				}
	char *	GetModuleIP()		{	return m_szIpAddress;			}

	//state treat
	void	SetState(WORD state);
	WORD	GetState()			{	return m_state;					}

	//Event treat
	void	SetReadEvent()		{	::SetEvent(m_hReadEvent);		}
	void	ResetReadEvent()	{ 	m_nRxLength = 0;	::ResetEvent(m_hReadEvent);		}
	void	SetWriteEvent()		{	::SetEvent(m_hWriteEvent);		}
	void	SetWriteEvent2()		{	::SetEvent(m_hWriteEvent2);		}
	void	ResetWriteEvent()	{	m_nTxLength = 0;	::ResetEvent(m_hWriteEvent);	}
	void	ResetWriteEvent2()	{	m_nTxLength = 0;	::ResetEvent(m_hWriteEvent2);	}

	void	SetSyncWriteEvent()		{	::SetEvent(m_hSyncWriteEvent);		}
	void	ResetSyncWriteEvent()	{ 	m_nTxLength = 0;	::ResetEvent(m_hSyncWriteEvent);		}

	//Module set Data treat
	SFT_MD_SYSTEM_DATA	m_sysData;			//Module System Data (Module에서 올라오는 설정정보)
	BOOL SetModuleSystem(SFT_MD_SYSTEM_DATA *lpSysData);
	SFT_MD_SYSTEM_DATA *GetModuleSystem();

	//Module Config data treat
//	SFT_SYSTEM_PARAM	m_sysParam;			//System Parameter (Module로 하달하는 정보)
	BOOL SetModuleParam(int nModuleID, SFT_SYSTEM_PARAM *lpSysParam);
	SFT_SYSTEM_PARAM GetModuleParam();
	
	//construction,distruction
	CModule();
	virtual ~CModule();

	//Tx Buff
	char			m_szTxBuffer[_SFT_TX_BUF_LEN];		//Socket Write Buffer
	int				m_nTxLength;						//Tx Data Length

	//Rx Buff
	char			m_szRxBuffer[_SFT_RX_BUF_LEN];		//Socket Read Buffer	
	int				m_nRxLength;						//Rx Data Length

	//Command response
	SFT_RESPONSE	m_CommandAck;					//Response of Command

	//Ip Address
	char	m_szIpAddress[32];

	BOOL	IsSyncWrite;

	BOOL WriteLog(char *szLog); //ksj 20201118
protected:
	int m_nChCount;
	SYSTEMTIME m_timeConnect;

	LPVOID	m_lpBuff;

	//Read/Write Event Handle
	HANDLE			m_hWriteEvent;						//Module별 Write Event	
	HANDLE			m_hWriteEvent2;						//Module별 Write Event	
	HANDLE			m_hReadEvent;						//Module별 Read Event
	HANDLE			m_hSyncWriteEvent;					//64K 이상 데이터를 쓸때 동기화하는 Event

	char m_szConnectedTime[64];							//모듈과 통신이 연결된 시각
	void SetChannelCount(int nCount);

	UINT m_nCmdSequence;
	UINT m_nModuleID;						//ModuleID

	CMinMaxData		m_TempSensor;			//온도 기록용
	CMinMaxData		m_GasSensor;			//Gas Sensor 기록용

	BOOL	m_bSendSchedule; //ksj 20201215 : 스케쥴 전송중일때는 Heartbit 타임아웃 처리등을 여유있게 하도록 한다.
	WORD	m_state;						//Module State
	BYTE	m_jigState;						//Jig State 
	BYTE	m_trayState;					//Tray State
	BYTE	m_doorState;					//Door State
	BYTE	m_failCode;						//Module Fail Code

	//Channel Data
	CChannel	*m_pChannel;							
};

#endif // !defined(AFX_MODULE_H__500F2D05_C0A1_4EF0_858D_46B988FD42F3__INCLUDED_)
