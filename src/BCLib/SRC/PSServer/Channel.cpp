// Channel.cpp: implementation of the CChannel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Channel.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BOOL WriteALog(char *szLog)
{
	char szLogFileName[512];

	if(szLog == NULL)					return FALSE;

	SYSTEMTIME systemTime;   // system time
	::GetLocalTime(&systemTime);
	
	//주어진 Log File 명이 없으면 기본 파일명 생성
	//if(strlen(szLogFileName) == 0)
	//{
		sprintf(szLogFileName, ".\\log\\Debug.log", systemTime.wYear, systemTime.wMonth, systemTime.wDay);
	//}

	FILE *fp = fopen(szLogFileName, "ab+");
	if(fp == NULL)		return FALSE;
		
	TRACE("%s\n", szLog);
	fprintf(fp, "%02d/%02d/%02d %02d:%02d:%02d :: %s\r\n",	systemTime.wYear, systemTime.wMonth, systemTime.wDay,
												systemTime.wHour, systemTime.wMinute, systemTime.wSecond, 
												szLog);
	fclose(fp);
	return TRUE;
}

CChannel::CChannel()
{
    m_wChIndex = 0;
	m_state = PS_STATE_LINE_OFF;
    ZeroMemory(m_grade, sizeof(m_grade));
    m_cellCode  = 0;
	m_nStepNo = 0;
	m_nMode = 0; //ksj 20200625
    m_ulStepDay = 0;
    m_ulTotalDay = 0;
    m_ulStepTime = 0;
    m_ulTotalTime = 0;

    m_ulCVDay = 0;
	m_ulCVTime = 0;
 
	m_lVoltage = 0;
    m_lCurrent = 0;
    m_lWatt = 0;
    m_lWattHour = 0;
    m_lCapacity = 0;
    m_lImpedance = 0;
	m_lAvgVoltage = 0;
	m_lAvgCurrent = 0;
	m_wStepType = 0;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	m_lOvenCurTemparature = 0;
	m_lOvenRefTemparature = 0;
	m_lOvenCurHumidity = 0;
	m_lOvenRefHumidity = 0;
	m_lTemparature = 0;
	m_lAuxVoltage = 0;
	m_lCommState = 0;
	m_lOutputState = 0;				//ljb 20101222
	m_lInputState = 0;				//ljb 20101222
	m_nCurrentCycleNum = 0;
	m_nTotalCycleNum = 0;
	m_nAccCycleGroupNum1 = 0;
	m_nAccCycleGroupNum2 = 0;
	m_nAccCycleGroupNum3 = 0;
	m_nAccCycleGroupNum4 = 0;
	m_nAccCycleGroupNum5 = 0;
	m_lCapacitySum = 0;
	m_nAuxCount = 0;
	pAuxData = NULL;
	pCanData = NULL;
	m_nCanCount = 0;
	m_lCapacitance = 0;
	m_lChargeAh = 0;
	m_lDisChargeAh = 0;
	m_lChargeWh = 0;
	m_lDisChargeWh = 0;
	m_lSyncDate = 0;
	m_lSyncTime = 0;
	m_wChamberUsing = 0;
	m_wRecordTimeNo = 0;

	m_lVoltage_Input = 0;				//ljb 201011 add 한전 & 울산꺼 부터
	m_lVoltage_Power = 0;				//ljb 201011 add 한전 & 울산꺼 부터
	m_lVoltage_Bus = 0;					//ljb 201011 add 한전 & 울산꺼 부터

	m_nMuxUse = 0;						//ljb 20151111
	m_nMuxBackup = 0;					//ljb 20151111

	m_cbank = 0;

	//ljb 20091101
	m_pChamberData = new SFT_CHAMBER_VALUE;
	memset(m_pChamberData, 0, sizeof(SFT_CHAMBER_VALUE));

	//ljb 20150427
	m_pLoaderData = new SFT_LOADER_VALUE;
	memset(m_pLoaderData, 0, sizeof(SFT_LOADER_VALUE));

	//ljb 20170906
	m_pChillerData = new SFT_CHAMBER_VALUE;
	memset(m_pChillerData, 0, sizeof(SFT_CHAMBER_VALUE));

	m_pCellBALData = new SFT_CELLBAL_VALUE;
	memset(m_pCellBALData, 0, sizeof(SFT_CELLBAL_VALUE));

	m_pPwrSupplyData = new SFT_PWRSUPPLY_VALUE;
	memset(m_pPwrSupplyData, 0, sizeof(SFT_PWRSUPPLY_VALUE));

	memset(m_aStepStartData, 0, sizeof(SFT_MSEC_CH_DATA) * MAX_MSEC_DATA_POINT);	//ljb 2011524 
	memset(&m_StepStartDataInfo, 0, sizeof(SFT_MSEC_CH_DATA_INFO));

	/*m_nRunMode = AfxGetApp()->GetProfileInt("Config", "RunMode", 1);

	m_lStartCellCheckVoltageLimit = AfxGetApp()->GetProfileInt("Config", "NonCellLimit1", 500)*1000;
	m_lEndCellCheckVoltageLimit = AfxGetApp()->GetProfileInt("Config", "NonCellLimit2", 500)*1000;*/

	m_nRunMode = GetProfileIntExt("Config", "RunMode", 1);
	m_lStartCellCheckVoltageLimit = GetProfileIntExt("Config", "NonCellLimit1", 500)*1000;
	m_lEndCellCheckVoltageLimit = GetProfileIntExt("Config", "NonCellLimit2", 500)*1000;
}

CChannel::~CChannel()
{
	if(m_pChamberData != NULL)
	{		
		delete m_pChamberData;
		m_pChamberData = NULL;
	}
	
	if(m_pLoaderData != NULL)
	{		
		delete m_pLoaderData;
		m_pLoaderData = NULL;
	}

	// 200313 HKH Memory Leak 수정 =======
	if(m_pChillerData != NULL)
	{		
		delete m_pChillerData;
		m_pChillerData = NULL;
	}

	if(m_pCellBALData!=NULL)
	{
		delete m_pCellBALData;
		m_pCellBALData = NULL;
	}

	if(m_pPwrSupplyData!=NULL)
	{
		delete m_pPwrSupplyData;
		m_pPwrSupplyData = NULL;
	}
	// ====================================

	if(pAuxData != NULL)
	{		
		delete [] pAuxData;
		pAuxData = NULL;
	}

	if(pCanData != NULL)
	{
		delete [] pCanData;
		pCanData = NULL;
	}
}

void CChannel::operator =(const CChannel& chData)
{
	m_wChIndex =  chData.m_wChIndex;
	m_state = chData.m_state;
}

SFT_CHAMBER_VALUE * CChannel::GetChamberData()	//ljb 20091101
{
	return this->m_pChamberData;
}

SFT_LOADER_VALUE * CChannel::GetLoaderData()	//ljb 20150427
{
	return this->m_pLoaderData;
}
SFT_CHAMBER_VALUE * CChannel::GetChillerData()	//ljb 20170906
{
	return this->m_pChillerData;
}
SFT_CELLBAL_VALUE * CChannel::GetCellBALData()	//ljb 20170906
{
	return this->m_pCellBALData;
}
SFT_PWRSUPPLY_VALUE * CChannel::GetPwrSupplyData()	//ljb 20170906
{
	return this->m_pPwrSupplyData;
}
void CChannel::SetChamberData(SFT_CHAMBER_VALUE *pChamberData)	//ljb 20091101
{
	memcpy(m_pChamberData,pChamberData,sizeof(SFT_CHAMBER_VALUE));
}
void CChannel::SetLoaderData(SFT_LOADER_VALUE *pLoaderData)	//ljb 20150427
{
	memcpy(m_pLoaderData,pLoaderData,sizeof(SFT_LOADER_VALUE));
}
void CChannel::SetChillerData(SFT_CHAMBER_VALUE *pChillerData)	//ljb 20170906
{
	memcpy(m_pChillerData,pChillerData,sizeof(SFT_CHAMBER_VALUE));
}
void CChannel::SetCellBALData(SFT_CELLBAL_VALUE *pCellBALData)	//ljb 20170906
{
	memcpy(m_pCellBALData,pCellBALData,sizeof(SFT_CELLBAL_VALUE));
}
void CChannel::SetPwrSupplyData(SFT_PWRSUPPLY_VALUE *pPwrSupplyData)	//ljb 20170906
{
	memcpy(m_pPwrSupplyData,pPwrSupplyData,sizeof(SFT_PWRSUPPLY_VALUE));
}

void CChannel::SetChannelData(SFT_CH_DATA chData)
{
//	char szLog[128];
//	sprintf(szLog, "befor data I:%d, C:%d :: Flag %d\n", chData.lCurrent, chData.lCapacity, m_nRunMode);
//	WriteALog(szLog);
	ConverChData(chData);	
//	sprintf(szLog, "befor data I:%d, C:%d\n", chData.lCurrent, chData.lCapacity);
//	WriteALog(szLog);

	//PC에서 Ready, End 상태를 만듬 2006/12/15
	//	m_state = chData.chState;
	//Ready 상태	:  Standby 상태에서 전압이 있을 경우 
	//End 상태		: 작업이 완료된 상태에서 전압이 있을 경우 
	//Run => Standby로 전이시 완료 상태로 만듬 
	if(m_state == PS_STATE_RUN  && chData.chState == PS_STATE_STANDBY)
	{
		m_state = PS_STATE_END;
	}

	//완료 상태에서 
	if(m_state == PS_STATE_END && chData.chState == PS_STATE_STANDBY)
	{
		//전압이 판단값보다 작으면 
		if(m_lVoltage < m_lStartCellCheckVoltageLimit)
		{
			//모듈 상태를 이용(Stanby)
			m_state = chData.chState;
		}
		else
		{
			//이전에 전압값이 판단값이상 이었는데 전압이 사라진 경우 
			if(chData.lVoltage < m_lStartCellCheckVoltageLimit)
			{
				//모듈 상태를 이용(Standby)
				m_state = chData.chState;
			}
		}
	}
	else
	{
		//모듈의 Channel 상태가 Standby가 아니면 현재 상태를 Update
		m_state = chData.chState;
	}

	//Standby 상태에서 
	if(m_state == PS_STATE_STANDBY)
	{
		//전압이 판단값 이상이면 Ready 상태로 변경
		if(m_lVoltage >= m_lEndCellCheckVoltageLimit)
		{
			m_state = PS_STATE_READY;
		}
	}
	//////////////////////////////////////////////////////////////////////////

	if(chData.chDataSelect == SFT_SAVE_STEP_END)
	{
		m_grade[0] = chData.chGradeCode;
	}
    m_cellCode = chData.chCode;
	m_nStepNo = chData.chStepNo;
    m_ulStepDay = chData.ulStepDay;
    m_ulTotalDay = chData.ulTotalDay;
    m_ulStepTime = chData.ulStepTime;
    m_ulTotalTime = chData.ulTotalTime;
    //m_lVoltage = chData.lVoltage; //20180418 yulee 주석 처리 -> Voltage값이 0으로 내려오나 이것을 마지막 Endstep의 Voltage값으로 대체 

	if(	chData.chDataSelect == SFT_SAVE_STEP_END && 
		(chData.chStepType == PS_STEP_CHARGE || chData.chStepType == PS_STEP_DISCHARGE))	
	{
		m_lVoltage = chData.lVoltage;
	}

    m_ulCVDay = chData.ulCVDay;
    m_ulCVTime = chData.ulCVTime;

    m_lVoltage_Input = chData.lVoltage_Input;
    m_lVoltage_Power = chData.lVoltage_Power;
    m_lVoltage_Bus = chData.lVoltage_Bus;

	m_lCurrent = chData.lCurrent;
	m_lWatt = chData.lWatt;

//	m_lWattHour = chData.lWattHour;
	m_lCapacitance	= chData.lCapacitance;
	m_lChargeWh	= chData.lChargeWh;
	m_lDisChargeWh	= chData.lDisChargeWh;

	m_lAvgVoltage = chData.lAvgVoltage;
	m_lAvgCurrent = chData.lAvgCurrent;
	m_wStepType = chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest


	
	//Impedance값은 최종값을 유지하도록 한다. 
	if(chData.chStepType == PS_STEP_CHARGE || chData.chStepType == PS_STEP_DISCHARGE || chData.chStepType == PS_STEP_IMPEDANCE)
	{
		//chData.chDataSelect == SFT_SAVE_STEP_END;
		m_lImpedance = chData.lImpedance;
	}
	//2006/12/14 SBC에서 값을 유지 하도록 수정
/*
#ifdef _EDLC_TEST_SYSTEM
	if(	chData.chDataSelect == SFT_SAVE_STEP_END && 
		(chData.chStepType == PS_STEP_CHARGE || chData.chStepType == PS_STEP_DISCHARGE))	
	{
		m_lCapacity = chData.lCapacity;
	}
#else
*/
	//m_lCapacity = chData.lCapacity;
	m_lChargeAh	= chData.lChargeAh;
	m_lDisChargeAh	= chData.lDisChargeAh;
//#endif
	
	//m_lTemparature = chData.lTemparature;
	m_CmdReserved = chData.chReservedCmd;
//	m_lPressure = chData.lPressure;
//	chData.reserved
	m_nCurrentCycleNum = (WORD)chData.nCurrentCycleNum;
	m_nTotalCycleNum = (WORD)chData.nTotalCycleNum;
	m_nAccCycleGroupNum1 = (WORD)chData.nAccCycleGroupNum1;		//ljb v1009
	m_nAccCycleGroupNum2 = (WORD)chData.nAccCycleGroupNum2;		//ljb v1009
	m_nAccCycleGroupNum3 = (WORD)chData.nAccCycleGroupNum3;		//ljb v1009
	m_nAccCycleGroupNum4 = (WORD)chData.nAccCycleGroupNum4;		//ljb v1009
	m_nAccCycleGroupNum5 = (WORD)chData.nAccCycleGroupNum5;		//ljb v1009
	m_nMultiCycleGroupNum1 = (WORD)chData.nMultiCycleGroupNum1;		//ljb v1009
	m_nMultiCycleGroupNum2 = (WORD)chData.nMultiCycleGroupNum2;		//ljb v1009
	m_nMultiCycleGroupNum3 = (WORD)chData.nMultiCycleGroupNum3;		//ljb v1009
	m_nMultiCycleGroupNum4 = (WORD)chData.nMultiCycleGroupNum4;		//ljb v1009
	m_nMultiCycleGroupNum5 = (WORD)chData.nMultiCycleGroupNum5;		//ljb v1009

	m_nMuxUse = (WORD)chData.cOutMuxUse;			//ljb 20151111
	m_nMuxBackup = (WORD)chData.cOutMuxBackup;		//ljb 20151111

	m_cbank = chData.cCbank;

	//TRACE("Mux use : %d, Mux Backup :%d \n",m_nMuxUse,m_nMuxBackup);
	//TRACE("m_cbank: %d, chData num :%d \n",m_cbank, chData.chNo);
}

long CChannel::GetVoltage()
{
	return m_lVoltage;
}

long CChannel::GetCurrent()
{
	return m_lCurrent;
}

long CChannel::GetCapacity()
{
	return m_lCapacity;
}

long CChannel::GetImpedance()
{
	return m_lImpedance;
}

WORD CChannel::GetState()
{
	return m_state;
}

UINT CChannel::GetStepDay()
{
	return m_ulStepDay;
}

UINT CChannel::GetStepTime()
{
	return m_ulStepTime;
}

BYTE CChannel::GetCode()
{
	return m_cellCode;
}


BYTE CChannel::GetGradeCode(int nItem)
{
	if(nItem < 0 && nItem >= PS_MAX_STEP_GRADE)
		return 0;

	return m_grade[nItem];
}

long CChannel::GetWatt()
{
	return m_lWatt;
}

long CChannel::GetWattHour()
{
	return m_lWattHour;
}

WORD CChannel::GetStepNo()
{
	return m_nStepNo;
}

//ksj 20200625 : mode 추가.
WORD CChannel::GetMode()
{
	return m_nMode;
}

UINT CChannel::GetTotalDay()
{
	return m_ulTotalDay;
}

UINT CChannel::GetTotalTime()
{
	return m_ulTotalTime;
}

long	CChannel::GetAvgCurrent()
{
	return m_lAvgCurrent;
}

long	CChannel::GetAvgVoltage()
{
	return m_lAvgVoltage;
}

long	CChannel::GetCapacitySum()
{
	return m_lCapacitySum;
}

long	CChannel::GetTemperature()
{
	return m_lTemparature;
}

long	CChannel::GetAuxVoltage()
{
	return m_lAuxVoltage;
}

long	CChannel::GetCommState()
{
	return m_lCommState;
}

WORD CChannel::GetChOutputState()
{
	return (WORD)m_lOutputState;
}

WORD CChannel::GetChInputState()
{
	return (WORD)m_lInputState;
}

WORD CChannel::GetChamberUsing()
{
	return m_wChamberUsing;
}

WORD CChannel::GetRecordTimeNo()
{
	return m_wRecordTimeNo;
}

long CChannel::GetVoltage_Input()
{
	return m_lVoltage_Input;
}

long CChannel::GetVoltage_Power()
{
	return m_lVoltage_Power;
}

long CChannel::GetVoltage_Bus()
{
	return m_lVoltage_Bus;
}


/*
void CChannel::PushSaveData(SFT_CH_DATA chData)
{
	ConverChData(chData);
	
	//Critcal section이 필요 할 것으로 생각됨 
	cs.Lock();

	if( GetSaveDataStackSize() < MAX_DATA_STACK_SIZE)
	{
		m_aSaveDataList.AddTail(chData);
//		TRACE("Pushed data to stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
	}

#ifdef _DEBUG
	else
	{
		TRACE("Channel %d data stack overflow!!!!!!!!!!!!\n", m_wChIndex+1);
	}
#endif

	cs.Unlock();
}

BOOL CChannel::PopSaveData(SFT_CH_DATA &chData)
{
	int nRtn = FALSE;
	
	//critical section
	cs.Lock();
	if(GetSaveDataStackSize() > 0 )
	{
		POSITION pos = m_aSaveDataList.GetHeadPosition();
		if(pos)
		{
			chData = m_aSaveDataList.GetAt(pos);
			m_aSaveDataList.RemoveAt(pos);
			nRtn = TRUE;

//			TRACE("Poped data form stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
		}
	}
	cs.Unlock();

	return nRtn;
}*/

//2007/08/09 kjh 수정 -> SFT_CH_DATA를 SFT_VARIABLE_CH_DATA로 변경
void CChannel::PushSaveData(SFT_VARIABLE_CH_DATA varData)
{
	ConverChData(varData.chData);
	
	//Critcal section이 필요 할 것으로 생각됨 
	cs.Lock();
	
	if( GetSaveDataStackSize() < MAX_DATA_STACK_SIZE)
	{
		m_aSaveDataList.AddTail(varData);
//		TRACE("Pushed data to stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
	}
	
#ifdef _DEBUG
	else
	{
		TRACE("Channel %d data stack overflow!!!!!!!!!!!!\n", m_wChIndex+1);
	}
#endif
	
	cs.Unlock();
}

//2007/08/09 kjh 수정 -> SFT_CH_DATA를 SFT_VARIABLE_CH_DATA로 변경
BOOL CChannel::PopSaveData(SFT_VARIABLE_CH_DATA &varData)
{
	int nRtn = FALSE;
	
	//critical section
	cs.Lock();
	if(GetSaveDataStackSize() > 0 )
	{
		POSITION pos = m_aSaveDataList.GetHeadPosition();
		if(pos)
		{
			varData = m_aSaveDataList.GetAt(pos);
			m_aSaveDataList.RemoveAt(pos);
			nRtn = TRUE;
			
			//TRACE("Poped data form stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
		}
	}
	cs.Unlock();
	
	return nRtn;
}

//2007/11/09 kjh 수정 -> 하위 버전 호환용 - CAN 사용 안함
void CChannel::PushSaveDataA(SFT_VARIABLE_CH_DATA_A varData)
{
	ConverChDataA(varData.chData);
	
	//Critcal section이 필요 할 것으로 생각됨 
	cs.Lock();
	
	if( GetSaveDataStackSize() < MAX_DATA_STACK_SIZE)
	{
		m_aSaveDataListA.AddTail(varData);
//		TRACE("Pushed data to stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
	}
	
#ifdef _DEBUG
	else
	{
		TRACE("Channel %d data stack overflow!!!!!!!!!!!!\n", m_wChIndex+1);
	}
#endif
	
	cs.Unlock();
}

//2007/11/09 kjh 수정 -> 하위 버전 호환용 - CAN 사용 안함
BOOL CChannel::PopSaveDataA(SFT_VARIABLE_CH_DATA_A &varData)
{
	int nRtn = FALSE;
	
	//critical section
	cs.Lock();
	if(GetSaveDataStackSizeA() > 0 )
	{
		POSITION pos = m_aSaveDataListA.GetHeadPosition();
		if(pos)
		{
			varData = m_aSaveDataListA.GetAt(pos);
			m_aSaveDataListA.RemoveAt(pos);
			nRtn = TRUE;
			
		//	TRACE("Poped data form stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
		}
	}
	cs.Unlock();
	
	return nRtn;
}

UINT CChannel::GetSaveDataStackSize()
{
	return m_aSaveDataList.GetCount();
}

WORD CChannel::GetTotalCycleCount()
{
	return m_nTotalCycleNum;
}

WORD CChannel::GetCurCycleCount()
{
	return m_nCurrentCycleNum;
}

WORD CChannel::GetStepType()
{
	return m_wStepType;
}

void CChannel::ResetData()
{
	//m_state = PS_STATE_IDLE;
	m_state = PS_STATE_LINE_OFF;
    ZeroMemory(m_grade, sizeof(m_grade));
    m_cellCode  = 0;
	m_nStepNo = 0;

    m_ulStepDay = 0;
    m_ulTotalDay = 0;
    m_ulStepTime = 0;
    m_ulTotalTime = 0;

	m_ulCVDay = 0;
    m_ulCVTime = 0;

    m_lVoltage = 0;
    m_lCurrent = 0;
    m_lWatt = 0;
    m_lWattHour = 0;
    m_lCapacity = 0;
    m_lImpedance = 0;
	m_lAvgVoltage = 0;
	m_lAvgCurrent = 0;
	m_wStepType = 0;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	m_lOvenCurTemparature = 0;
	m_lOvenRefTemparature = 0;
	m_lOvenCurHumidity = 0;
	m_lOvenRefHumidity = 0;
	m_lTemparature = 0;
	m_lAuxVoltage = 0;
	m_lCommState = 0;
	m_lOutputState = 0;
	m_lInputState = 0;
	m_nCurrentCycleNum = 0;
	m_nTotalCycleNum = 0;
	m_nAccCycleGroupNum1 = 0;
	m_nAccCycleGroupNum2 = 0;
	m_nAccCycleGroupNum3 = 0;
	m_nAccCycleGroupNum4 = 0;
	m_nAccCycleGroupNum5 = 0;
	m_lCapacitySum = 0;

    m_lVoltage_Input = 0;
	m_lVoltage_Power = 0;
    m_lVoltage_Bus = 0;

	m_nMuxUse = 0;
	m_nMuxBackup = 0;

	m_cbank = 0;

	m_aSaveDataList.RemoveAll();   // List of Save data
	m_aSaveDataListA.RemoveAll();   // List of Save data

}

//최대 100msec로 1분가능 
BOOL CChannel::SetStepStartData(SFT_MSEC_CH_DATA_INFO *pInfo, LPVOID lpData)
{
	if(pInfo->lDataCount < 1)
	{
		return TRUE;
	}

	if(pInfo->lDataCount > MAX_MSEC_DATA_POINT)
	{
		pInfo->lDataCount = MAX_MSEC_DATA_POINT;
	}
	int nBuffSize = pInfo->lDataCount * sizeof(SFT_MSEC_CH_DATA);

	memcpy(m_aStepStartData, lpData, nBuffSize);
	memcpy(&m_StepStartDataInfo, pInfo, sizeof(SFT_MSEC_CH_DATA_INFO));
	
	return TRUE;
}

//convert data to run mode
void CChannel::ConverChData(SFT_CH_DATA &chData)
{
	if(m_nRunMode == 0 )	//
	{
		if( chData.chStepType == PS_STEP_CHARGE		|| 
			chData.chStepType == PS_STEP_DISCHARGE	|| 
			chData.chStepType == PS_STEP_PATTERN	||
			chData.chStepType == PS_STEP_EXT_CAN	||			//20121119 add		
			(chData.chStepType == PS_STEP_IMPEDANCE && chData.chMode == PS_MODE_DCIMP)
		)
		{
		}
		else
		{
//			chData.lCapacity = 0;
			chData.lChargeAh	= 0;
			chData.lDisChargeAh	= 0;
			chData.lCapacitance	= 0;

			chData.lCurrent = 0;
			chData.lWatt = 0;

//			chData.lWattHour = 0;
			chData.lChargeWh = 0;
			chData.lDisChargeWh = 0;

			chData.lAvgVoltage = 0;
			chData.lAvgCurrent = 0;
		}

		if(chData.chState == PS_STATE_IDLE || chData.chState == PS_STATE_STANDBY)
		{
			chData.lCurrent = 0;
			chData.lWatt = 0;
		}

		if(chData.lImpedance > 1000000000 || chData.lImpedance < 0)	//0 ~ 1KOhm
			chData.lImpedance = 0;
	}
}

//convert data to run mode
void CChannel::ConverChDataA(SFT_CH_DATA_A &chData)
{
	if(m_nRunMode == 0 )	//
	{
		if( chData.chStepType == PS_STEP_CHARGE		|| 
			chData.chStepType == PS_STEP_DISCHARGE	|| 
			chData.chStepType == PS_STEP_PATTERN	||
			(chData.chStepType == PS_STEP_IMPEDANCE && chData.chMode == PS_MODE_DCIMP)
		)
		{
		}
		else
		{
			chData.lCapacity = 0;
			chData.lCurrent = 0;
			chData.lWatt = 0;
			chData.lWattHour = 0;
			chData.lAvgVoltage = 0;
			chData.lAvgCurrent = 0;
		}

		if(chData.chState == PS_STATE_IDLE || chData.chState == PS_STATE_STANDBY)
		{
			chData.lCurrent = 0;
			chData.lWatt = 0;
		}

		if(chData.lImpedance > 1000000000 || chData.lImpedance < 0)	//0 ~ 1KOhm
			chData.lImpedance = 0;
	}
}


//void CChannel::SetChannelVarData(SFT_VARIABLE_CH_DATA varData)
void CChannel::SetChannelVarData(SFT_VARIABLE_CH_DATA varData, HWND hMsgWnd) //ksj 20160728 윈도우 핸들 인자 추가.
{
//	char szLog[128];
//	sprintf(szLog, "befor data I:%d, C:%d :: Flag %d\n", chData.lCurrent, chData.lCapacity, m_nRunMode);
//	WriteALog(szLog);

	ConverChData(varData.chData);	


//	sprintf(szLog, "befor data I:%d, C:%d\n", chData.lCurrent, chData.lCapacity);
//	WriteALog(szLog);

	//PC에서 Ready, End 상태를 만듬 2006/12/15
	//	m_state = chData.chState;
	//Ready 상태	:  Standby 상태에서 전압이 있을 경우 
	//End 상태		: 작업이 완료된 상태에서 전압이 있을 경우 
	//Run => Standby로 전이시 완료 상태로 만듬 
	if(m_state == PS_STATE_RUN  && varData.chData.chState == PS_STATE_STANDBY)
	{
		m_state = PS_STATE_END;

		::SendMessage(hMsgWnd, SFTWM_TEST_COMPLETE, (WPARAM)varData.chData.chNo , (LPARAM) this); //ksj 20160728 작업 완료 통지 메세지 보내기. (받은 쪽은 채널번호만 알수있고 어떤 모듈인지 모른다....)
	}

	//ksj 20200601 : 작업중 PAUSE로 변할때 통지 보내기
	if(m_state == PS_STATE_RUN  && varData.chData.chState == PS_STATE_PAUSE)
	{
		::SendMessage(hMsgWnd, SFTWM_TEST_PAUSE, (WPARAM)varData.chData.chNo , (LPARAM) this); //ksj 20200601 작업 일시정지 통지 메세지 보내기. (받은 쪽은 채널번호만 알수있고 어떤 모듈인지 모른다....)
	}
#ifdef _DEBUG
	if(m_state != varData.chData.chState)
	{
		TRACE("STATE(%d) : %d -> %d\n",varData.chData.chNo,m_state,varData.chData.chState);
	}
#endif
	//ksj end
	
#ifdef _DEBUG
	//TRACE("SBC - > PC 전송한 채널 상태 : %X \n",varData.chData.chState);
#endif // _DEBUG

	//완료 상태에서 
	if(m_state == PS_STATE_END && varData.chData.chState == PS_STATE_STANDBY)
	{
		//전압이 판단값보다 작으면 
		if(m_lVoltage < m_lStartCellCheckVoltageLimit)
		{
			//모듈 상태를 이용(Stanby)
			m_state = varData.chData.chState;
		}
		else
		{
			//이전에 전압값이 판단값이상 이었는데 전압이 사라진 경우 
			if(varData.chData.lVoltage < m_lStartCellCheckVoltageLimit)
			{
				//모듈 상태를 이용(Standby)
				m_state = varData.chData.chState;
			}
		}
	}
	else
	{
		//모듈의 Channel 상태가 Standby가 아니면 현재 상태를 Update
		m_state = varData.chData.chState;
	}

	//Standby 상태에서 
	if(m_state == PS_STATE_STANDBY)
	{
		//전압이 판단값 이상이면 Ready 상태로 변경
		if(m_lVoltage >= m_lEndCellCheckVoltageLimit)
		{
			m_state = PS_STATE_READY;
		}
	}
	//////////////////////////////////////////////////////////////////////////

	if(varData.chData.chDataSelect == SFT_SAVE_STEP_END)
	{
		m_grade[0] = varData.chData.chGradeCode;
	}
    m_cellCode = varData.chData.chCode;
	m_nStepNo = varData.chData.chStepNo;
	m_nMode = varData.chData.chMode; //ksj 20200625 : mode 정보 복사 추가.

    m_ulStepDay = varData.chData.ulStepDay;
    m_ulTotalDay = varData.chData.ulTotalDay;
    m_ulStepTime = varData.chData.ulStepTime;
    m_ulTotalTime = varData.chData.ulTotalTime;

    m_lVoltage = varData.chData.lVoltage;
	m_lCurrent = varData.chData.lCurrent;
	m_lWatt = varData.chData.lWatt;
	m_lCommState = varData.chData.chCommState;				//ljb 201009
	m_lOutputState = varData.chData.chOutputState;		//ljb 201012 v100b
	m_lInputState = varData.chData.chInputState;			//ljb 201012 v100b
	m_wChamberUsing = varData.chData.cUsingChamber;			//ljb 201010 v100b
	m_wRecordTimeNo = varData.chData.cRecordTimeNo;			//ljb 201010 v100b

	m_lWattHour = varData.chData.lChargeWh+varData.chData.lDisChargeWh;
	m_lChargeWh	= varData.chData.lChargeWh;
	m_lDisChargeWh	= varData.chData.lDisChargeWh;

	m_lAvgVoltage = varData.chData.lAvgVoltage;
	m_lAvgCurrent = varData.chData.lAvgCurrent;
	m_wStepType = varData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest

	m_lSyncDate	= varData.chData.lSyncTime[0];
	m_lSyncTime	= varData.chData.lSyncTime[1];

	m_ulCVDay = varData.chData.ulCVDay;
	m_ulCVTime	= varData.chData.ulCVTime;

    m_lVoltage_Input = varData.chData.lVoltage_Input;
    m_lVoltage_Power = varData.chData.lVoltage_Power;
    m_lVoltage_Bus = varData.chData.lVoltage_Bus;
	
	//Impedance값은 최종값을 유지하도록 한다. 
	if(varData.chData.chStepType == PS_STEP_CHARGE || varData.chData.chStepType == PS_STEP_DISCHARGE 
		|| varData.chData.chStepType == PS_STEP_IMPEDANCE)
	{
		//chData.chDataSelect == SFT_SAVE_STEP_END;
		m_lImpedance = varData.chData.lImpedance;
	}
	//2006/12/14 SBC에서 값을 유지 하도록 수정
	m_lCapacity = varData.chData.lChargeAh+varData.chData.lDisChargeAh;	//kjh Charge+disCharge 로 변경해야
	m_lChargeAh = varData.chData.lChargeAh;
	m_lDisChargeAh	= varData.chData.lDisChargeAh;
	m_lCapacitance	= varData.chData.lCapacitance;
	
	//m_lTemparature = varData.chData.lTemparature;
	m_CmdReserved = varData.chData.chReservedCmd;
	m_nCurrentCycleNum = (WORD)varData.chData.nCurrentCycleNum;
	m_nTotalCycleNum = (WORD)varData.chData.nTotalCycleNum;
	m_nAccCycleGroupNum1 = (WORD)varData.chData.nAccCycleGroupNum1;		//ljb v1009
	m_nAccCycleGroupNum2 = (WORD)varData.chData.nAccCycleGroupNum2;		//ljb v1009
	m_nAccCycleGroupNum3 = (WORD)varData.chData.nAccCycleGroupNum3;		//ljb v1009
	m_nAccCycleGroupNum4 = (WORD)varData.chData.nAccCycleGroupNum4;		//ljb v1009
	m_nAccCycleGroupNum5 = (WORD)varData.chData.nAccCycleGroupNum5;		//ljb v1009

	m_nMuxUse = varData.chData.cOutMuxUse;			//ljb 20151111
	m_nMuxBackup = varData.chData.cOutMuxBackup;	//ljb 20151111 0: open, 1: Mux A, 2: Mux B

	if (varData.chData.chDataSelect == 0)
	{
		m_cbank = varData.chData.cCbank;
	}

	//TRACE("m_cbank: %d, chData num :%d \n",m_cbank, varData.chData.cCbank);

	//CAN 데이터
	SetCanVarData(varData.chData.nCanCount, varData.canData);

	//Aux 데이터
	if(varData.chData.chNo > 0) //yulee 20190610_2
		SetAuxVarData(varData.chData.nAuxCount, varData.auxData);

	//챔버 데이터
	m_lOvenCurTemparature = FLOAT2LONG(varData.ovenData.fCurTemper);
	m_lOvenRefTemparature = FLOAT2LONG(varData.ovenData.fRefTemper);
	m_lOvenCurHumidity = FLOAT2LONG(varData.ovenData.fCurHumidity);
	m_lOvenRefHumidity = FLOAT2LONG(varData.ovenData.fRefHumidity);

}

void CChannel::SetChannelVarDataA(SFT_VARIABLE_CH_DATA_A varData)
{
//	char szLog[128];
//	sprintf(szLog, "befor data I:%d, C:%d :: Flag %d\n", chData.lCurrent, chData.lCapacity, m_nRunMode);
//	WriteALog(szLog);
	ConverChDataA(varData.chData);	
//	sprintf(szLog, "befor data I:%d, C:%d\n", chData.lCurrent, chData.lCapacity);
//	WriteALog(szLog);

	//PC에서 Ready, End 상태를 만듬 2006/12/15
	//	m_state = chData.chState;
	//Ready 상태	:  Standby 상태에서 전압이 있을 경우 
	//End 상태		: 작업이 완료된 상태에서 전압이 있을 경우 
	//Run => Standby로 전이시 완료 상태로 만듬 
	if(m_state == PS_STATE_RUN  && varData.chData.chState == PS_STATE_STANDBY)
	{
		m_state = PS_STATE_END;
	}

	//완료 상태에서 
	if(m_state == PS_STATE_END && varData.chData.chState == PS_STATE_STANDBY)
	{
		//전압이 판단값보다 작으면 
		if(m_lVoltage < m_lStartCellCheckVoltageLimit)
		{
			//모듈 상태를 이용(Stanby)
			m_state = varData.chData.chState;
		}
		else
		{
			//이전에 전압값이 판단값이상 이었는데 전압이 사라진 경우 
			if(varData.chData.lVoltage < m_lStartCellCheckVoltageLimit)
			{
				//모듈 상태를 이용(Standby)
				m_state = varData.chData.chState;
			}
		}
	}
	else
	{
		//모듈의 Channel 상태가 Standby가 아니면 현재 상태를 Update
		m_state = varData.chData.chState;
	}

	//Standby 상태에서 
	if(m_state == PS_STATE_STANDBY)
	{
		//전압이 판단값 이상이면 Ready 상태로 변경
		if(m_lVoltage >= m_lEndCellCheckVoltageLimit)
		{
			m_state = PS_STATE_READY;
		}
	}
	//////////////////////////////////////////////////////////////////////////

	if(varData.chData.chDataSelect == SFT_SAVE_STEP_END)
	{
		m_grade[0] = varData.chData.chGradeCode;
	}
    m_cellCode = varData.chData.chCode;
	m_nStepNo = varData.chData.chStepNo;
    m_ulStepTime = varData.chData.ulStepTime;
    m_ulTotalTime = varData.chData.ulTotalTime;
    m_lVoltage = varData.chData.lVoltage;

	m_lCurrent = varData.chData.lCurrent;
	m_lWatt = varData.chData.lWatt;
	m_lWattHour = varData.chData.lWattHour;
	m_lAvgVoltage = varData.chData.lAvgVoltage;
	m_lAvgCurrent = varData.chData.lAvgCurrent;
	m_wStepType = varData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest

	
	//Impedance값은 최종값을 유지하도록 한다. 
	if(varData.chData.chStepType == PS_STEP_CHARGE || varData.chData.chStepType == PS_STEP_DISCHARGE 
		|| varData.chData.chStepType == PS_STEP_IMPEDANCE)
	{
		//chData.chDataSelect == SFT_SAVE_STEP_END;
		m_lImpedance = varData.chData.lImpedance;
	}
	//2006/12/14 SBC에서 값을 유지 하도록 수정
/*
#ifdef _EDLC_TEST_SYSTEM
	if(	chData.chDataSelect == SFT_SAVE_STEP_END && 
		(chData.chStepType == PS_STEP_CHARGE || chData.chStepType == PS_STEP_DISCHARGE))	
	{
		m_lCapacity = chData.lCapacity;
	}
#else
*/
	m_lCapacity = varData.chData.lCapacity;
//#endif
	
	//m_lTemparature = varData.chData.lTemparature;
	m_CmdReserved = varData.chData.chReservedCmd;
//	m_lPressure = chData.lPressure;
//	chData.reserved
	m_nCurrentCycleNum = (WORD)varData.chData.nCurrentCycleNum;
	m_nTotalCycleNum = (WORD)varData.chData.nTotalCycleNum;


	//Aux 데이터
	SetAuxVarDataA(varData);	
}

int CChannel::GetAuxCount()
{
	return this->m_nAuxCount;
}

SFT_AUX_DATA * CChannel::GetAuxData()
{
	return this->pAuxData;
}

void CChannel::SetState(WORD state)
{
	this->m_state = state;
}

SFT_CAN_DATA * CChannel::GetCanData()
{
	return this->pCanData;
}

int CChannel::GetCanCount()
{
	return this->m_nCanCount;
}

void CChannel::SetCanVarData(int nCanCount , SFT_CAN_DATA * pRCanData)
{
	char szMsg[128];
	ZeroMemory(szMsg, 128);

	if(pRCanData == NULL) return;

	if(nCanCount <= _SFT_MAX_MAPPING_CAN && nCanCount > 0)
	{
		if(m_nCanCount == 0 && nCanCount > 0)		//CAN Data 새로 생성
		{
			
			m_nCanCount = nCanCount;
			pCanData = new SFT_CAN_DATA[m_nCanCount];
			memcpy(pCanData, pRCanData, sizeof(SFT_CAN_DATA)*m_nCanCount);
			sprintf(szMsg,"Create Monitoring Can Infomation => Can Count : %d", m_nCanCount);
			WriteALog(szMsg);
		}
		else if(m_nCanCount == nCanCount)			//CAN 정보
		{
			memcpy(pCanData, pRCanData, sizeof(SFT_CAN_DATA)*m_nCanCount);
			/*TRACE("Count : %d can Type : %d, can Val : %f\r\n", 
				varData.chData.nCanCount,
				varData.canData[m_nCanCount-1].canType,
				varData.canData[m_nCanCount-1].canVal.fVal[0]
				);*/
		}
		else if(m_nCanCount > 0 && m_nCanCount != nCanCount)		//CAN 갯수 변경
		{
			sprintf(szMsg,"Installed CAN : %d, Monitoring CAN : %d", m_nCanCount, nCanCount);
			WriteALog(szMsg);
			delete [] pCanData;
			m_nCanCount = nCanCount;
			pCanData = new SFT_CAN_DATA[m_nCanCount];
			memcpy(pCanData, pRCanData, sizeof(SFT_CAN_DATA)*m_nCanCount);
		}
		else
		{
			sprintf(szMsg,"Installed CAN : %d, Monitoring CAN : %d", m_nCanCount, nCanCount);
			WriteALog(szMsg);
			if(pCanData != NULL)
				delete pCanData;
			pCanData = NULL;
			m_nCanCount = 0;
		}
	}
	else
	{		
		//ksj 20200513 : 주석처리. 불필요한 로그 찍힘.
		/*sprintf(szMsg,"Installed CAN : %d, Monitoring CAN : %d", m_nCanCount, nCanCount);
		WriteALog(szMsg);*/
		if(m_nCanCount > 0)
		{
			if(pCanData != NULL)
				delete pCanData;
			pCanData = NULL;
			m_nCanCount = 0;
		}
	}
}

void CChannel::SetAuxVarData(int nAuxCount, SFT_AUX_DATA * pRAuxData)
{
	if(pRAuxData == NULL) return;

	if(nAuxCount <= _SFT_MAX_MAPPING_AUX && nAuxCount > 0)
	{
		if(m_nAuxCount == 0 && nAuxCount > 0)		//Aux Data 새로 생성
		{
			m_nAuxCount = nAuxCount;
			pAuxData = new SFT_AUX_DATA[m_nAuxCount];
			memcpy(pAuxData, pRAuxData, sizeof(SFT_AUX_DATA)*m_nAuxCount);
			TRACE("Create Monitoring Aux Info => Aux Count : %d \n", m_nAuxCount);
		}
		else if(m_nAuxCount == nAuxCount)			//Aux 정보
		{
			memcpy(pAuxData, pRAuxData, sizeof(SFT_AUX_DATA)*m_nAuxCount);
	//		TRACE("Update Monitoring Aux Data => Aux Count : %d", m_nAuxCount);
		}
		else if(m_nAuxCount > 0 && m_nAuxCount != nAuxCount)		//Aux 갯수 변경
		{
			delete [] pAuxData;
			m_nAuxCount = nAuxCount;
			pAuxData = new SFT_AUX_DATA[m_nAuxCount];
			memcpy(pAuxData, pRAuxData, sizeof(SFT_AUX_DATA)*m_nAuxCount);
	//		TRACE("New Monitoring Aux Info => Aux Count : %d\r\n", m_nAuxCount);
		}
		else
		{
	//		TRACE("Error : 올바른 Aux 정보가 아님 => Current Aux Count : %d, InPut Aux Count : %d\r\n",
	//			m_nAuxCount, varData.chData.nAuxCount);

			if(pAuxData != NULL)
				delete pAuxData;
			pAuxData = NULL;
			m_nAuxCount = 0;
		}

	}
	//ksj 20200703 : 조건추가. aux 할당했다가 전부 해제했을때 0으로 리셋 안되는 현상 수정. (비정상적으로 로그 생성됨)
	else if(nAuxCount == 0)
	{
		if(pAuxData != NULL)
			delete pAuxData;
		pAuxData = NULL;
		m_nAuxCount = 0;
	}
	//ksj end;
}
void CChannel::SetAuxVarDataA(SFT_VARIABLE_CH_DATA_A varData)
{
	if(varData.chData.nAuxCount <= _SFT_MAX_MAPPING_AUX && varData.chData.nAuxCount > 0)
	{
		if(m_nAuxCount == 0 && varData.chData.nAuxCount > 0)		//Aux Data 새로 생성
		{
			m_nAuxCount = varData.chData.nAuxCount;
			pAuxData = new SFT_AUX_DATA[m_nAuxCount];
			//memcpy(pAuxData, varData.auxData, sizeof(SFT_AUX_DATA)*m_nAuxCount);
			for(int i = 0 ; i < m_nAuxCount; i++)
			{
				pAuxData[i].auxChNo     = varData.auxData[i].chNo;
				pAuxData[i].auxChType	= (UCHAR)varData.auxData[i].chType;
				pAuxData[i].lValue	    = varData.auxData[i].lValue;
			}
			
			TRACE("Create Monitoring Aux Info A => Aux Count : %d \n", m_nAuxCount);
		}
		else if(m_nAuxCount == varData.chData.nAuxCount)			//Aux 정보
		{
			//memcpy(pAuxData, varData.auxData, sizeof(SFT_AUX_DATA)*m_nAuxCount);
			for(int i = 0 ; i < m_nAuxCount; i++)
			{
				pAuxData[i].auxChNo     = varData.auxData[i].chNo;
				pAuxData[i].auxChType	= (UCHAR)varData.auxData[i].chType;
				pAuxData[i].lValue	    = varData.auxData[i].lValue;
			}
			//TRACE("Update Monitoring Aux Data => Aux Count : %d", m_nAuxCount);
		}
		else if(m_nAuxCount > 0 && m_nAuxCount != varData.chData.nAuxCount)		//Aux 갯수 변경
		{
			delete [] pAuxData;
			m_nAuxCount = varData.chData.nAuxCount;
			pAuxData = new SFT_AUX_DATA[m_nAuxCount];
			//memcpy(pAuxData, varData.auxData, sizeof(SFT_AUX_DATA)*m_nAuxCount);
			for(int i = 0 ; i < m_nAuxCount; i++)
			{
				pAuxData[i].auxChNo     = varData.auxData[i].chNo;
				pAuxData[i].auxChType	= (UCHAR)varData.auxData[i].chType;
				pAuxData[i].lValue	    = varData.auxData[i].lValue;
			}
			//TRACE("New Monitoring Aux Info => Aux Count : %d\r\n", m_nAuxCount);
		}
		else
		{
			//TRACE("Error : 올바른 Aux 정보가 아님 => Current Aux Count : %d, InPut Aux Count : %d\r\n",
			//	m_nAuxCount, varData.chData.nAuxCount);

			if(pAuxData != NULL)
				delete pAuxData;
			pAuxData = NULL;
			m_nAuxCount = 0;
		}

	}
}

UINT CChannel::GetSaveDataStackSizeA()
{
	return m_aSaveDataListA.GetCount();
}

int CChannel::GetProfileIntExt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault)
{
//	ULONG keySize=4;
//    unsigned char bszStr =0 ;
    LONG lreturn;
    HKEY hKey;
    CString SubFolder;
    CString Folder;
    DWORD temp;

	Folder = PS_REGISTRY_CTSMONPRO_NAME;

    SubFolder.Format("%s%s%s", Folder, _T("\\"), lpszSection);
    
    if(Folder != "")
    {
        lreturn=RegOpenKey(HKEY_CURRENT_USER, Folder, &hKey);
        if(lreturn!=ERROR_SUCCESS)    

        {
            return nDefault;
        }
    }
    
    if(SubFolder != "")
    {
        //HKEY_CURRENT_USER 에서 "SOFTWARE\\aaa\\(입력 SUB폴더값) " 열기
        lreturn=RegOpenKey(HKEY_CURRENT_USER, SubFolder, &hKey);
        if(lreturn!=ERROR_SUCCESS)    
        {
            return nDefault;
        }
    }

    // "SOFTWARE\\aaa\\(입력 폴더값) " 에 입력key 에 대한  출력 value 값 읽기 
    //lreturn = RegQueryValueEx(hKey, (LPSTR)lpszEntry, NULL, NULL, &bszStr, &keySize);
	DWORD dwValue;
	DWORD dwType;
	DWORD dwCount = sizeof(DWORD);
	lreturn = RegQueryValueEx(hKey, (LPTSTR)lpszEntry, NULL, &dwType,
			(LPBYTE)&dwValue, &dwCount);
    if(lreturn!=ERROR_SUCCESS)
    {
        return nDefault;
    }
    
    temp = dwValue;
    
 //닫기
    RegCloseKey(hKey);
    return temp;    
}

void CChannel::SetChannelVarData(SFT_VARIABLE_CH_DATA_V1004 varData)
{
//	char szLog[128];
//	sprintf(szLog, "befor data I:%d, C:%d :: Flag %d\n", chData.lCurrent, chData.lCapacity, m_nRunMode);
//	WriteALog(szLog);
	ConverChData(varData.chData);	
//	sprintf(szLog, "befor data I:%d, C:%d\n", chData.lCurrent, chData.lCapacity);
//	WriteALog(szLog);

	//PC에서 Ready, End 상태를 만듬 2006/12/15
	//	m_state = chData.chState;
	//Ready 상태	:  Standby 상태에서 전압이 있을 경우 
	//End 상태		: 작업이 완료된 상태에서 전압이 있을 경우 
	//Run => Standby로 전이시 완료 상태로 만듬 
	if(m_state == PS_STATE_RUN  && varData.chData.chState == PS_STATE_STANDBY)
	{
		m_state = PS_STATE_END;
	}

	//완료 상태에서 
	if(m_state == PS_STATE_END && varData.chData.chState == PS_STATE_STANDBY)
	{
		//전압이 판단값보다 작으면 
		if(m_lVoltage < m_lStartCellCheckVoltageLimit)
		{
			//모듈 상태를 이용(Stanby)
			m_state = varData.chData.chState;
		}
		else
		{
			//이전에 전압값이 판단값이상 이었는데 전압이 사라진 경우 
			if(varData.chData.lVoltage < m_lStartCellCheckVoltageLimit)
			{
				//모듈 상태를 이용(Standby)
				m_state = varData.chData.chState;
			}
		}
	}
	else
	{
		//모듈의 Channel 상태가 Standby가 아니면 현재 상태를 Update
		m_state = varData.chData.chState;
	}

	//Standby 상태에서 
	if(m_state == PS_STATE_STANDBY)
	{
		//전압이 판단값 이상이면 Ready 상태로 변경
		if(m_lVoltage >= m_lEndCellCheckVoltageLimit)
		{
			m_state = PS_STATE_READY;
		}
	}
	//////////////////////////////////////////////////////////////////////////

	if(varData.chData.chDataSelect == SFT_SAVE_STEP_END)
	{
		m_grade[0] = varData.chData.chGradeCode;
	}
    m_cellCode = varData.chData.chCode;
	m_nStepNo = varData.chData.chStepNo;
    m_ulStepTime = varData.chData.ulStepTime;
    m_ulTotalTime = varData.chData.ulTotalTime;
    m_lVoltage = varData.chData.lVoltage;

	m_lCurrent = varData.chData.lCurrent;
	m_lWatt = varData.chData.lWatt;
	m_lWattHour = varData.chData.lWattHour;
	m_lAvgVoltage = varData.chData.lAvgVoltage;
	m_lAvgCurrent = varData.chData.lAvgCurrent;
	m_wStepType = varData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest

	
	//Impedance값은 최종값을 유지하도록 한다. 
	if(varData.chData.chStepType == PS_STEP_CHARGE || varData.chData.chStepType == PS_STEP_DISCHARGE 
		|| varData.chData.chStepType == PS_STEP_IMPEDANCE)
	{
		//chData.chDataSelect == SFT_SAVE_STEP_END;
		m_lImpedance = varData.chData.lImpedance;
	}
	//2006/12/14 SBC에서 값을 유지 하도록 수정
/*
#ifdef _EDLC_TEST_SYSTEM
	if(	chData.chDataSelect == SFT_SAVE_STEP_END && 
		(chData.chStepType == PS_STEP_CHARGE || chData.chStepType == PS_STEP_DISCHARGE))	
	{
		m_lCapacity = chData.lCapacity;
	}
#else
*/
	m_lCapacity = varData.chData.lCapacity;
//#endif
	
	//m_lTemparature = varData.chData.lTemparature;
	m_CmdReserved = varData.chData.chReservedCmd;
//	m_lPressure = chData.lPressure;
//	chData.reserved
	m_nCurrentCycleNum = (WORD)varData.chData.nCurrentCycleNum;
	m_nTotalCycleNum = (WORD)varData.chData.nTotalCycleNum;

	//CAN 데이터
	SetCanVarData(varData.chData.nCanCount, varData.canData);

	//Aux 데이터
	if(varData.chData.chNo > 0) //yulee 20190610_2
		SetAuxVarData(varData.chData.nAuxCount, varData.auxData);	
}

void CChannel::PushSaveData(SFT_VARIABLE_CH_DATA_V1004 varData)
{
	ConverChData(varData.chData);
	
	//Critcal section이 필요 할 것으로 생각됨 
	cs.Lock();
	
	if( GetSaveDataStackSize() < MAX_DATA_STACK_SIZE)
	{
		m_aSaveDataList_V1004.AddTail(varData);
//		TRACE("Pushed data to stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
	}
	
#ifdef _DEBUG
	else
	{
		TRACE("Channel %d data stack overflow!!!!!!!!!!!!\n", m_wChIndex+1);
	}
#endif
	
	cs.Unlock();
}

void CChannel::ConverChData(SFT_CH_DATA_B &chData)
{
	if(m_nRunMode == 0 )	//
	{
		if( chData.chStepType == PS_STEP_CHARGE		|| 
			chData.chStepType == PS_STEP_DISCHARGE	|| 
			chData.chStepType == PS_STEP_PATTERN	||
			chData.chStepType == PS_STEP_EXT_CAN	||			//20121119 add		
			(chData.chStepType == PS_STEP_IMPEDANCE && chData.chMode == PS_MODE_DCIMP)
		)
		{
		}
		else
		{
			chData.lCapacity = 0;
			chData.lCurrent = 0;
			chData.lWatt = 0;
			chData.lWattHour = 0;
			chData.lAvgVoltage = 0;
			chData.lAvgCurrent = 0;
		}

		if(chData.chState == PS_STATE_IDLE || chData.chState == PS_STATE_STANDBY)
		{
			chData.lCurrent = 0;
			chData.lWatt = 0;
		}

		if(chData.lImpedance > 1000000000 || chData.lImpedance < 0)	//0 ~ 1KOhm
			chData.lImpedance = 0;
	}
}

BOOL CChannel::PopSaveData(SFT_VARIABLE_CH_DATA_V1004 &varData)
{
	int nRtn = FALSE;
	
	//critical section
	cs.Lock();
	if(GetSaveDataStackSize_V1004() > 0 )
	{
		POSITION pos = m_aSaveDataList_V1004.GetHeadPosition();
		if(pos)
		{
			varData = m_aSaveDataList_V1004.GetAt(pos);
			m_aSaveDataList_V1004.RemoveAt(pos);
			nRtn = TRUE;
			
		//	TRACE("Poped data form stack (channel %d) Stack size is %d\n", m_wChIndex+1, m_aSaveDataList.GetCount());
		}
	}
	cs.Unlock();
	
	return nRtn;
}

UINT CChannel::GetSaveDataStackSize_V1004()
{
	return m_aSaveDataList_V1004.GetCount();
}

UINT CChannel::GetCVDay()
{
	return m_ulCVDay;
}

UINT CChannel::GetCVTime()
{
	return m_ulCVTime;
}

LONG CChannel::GetChargeAh()
{
	return m_lChargeAh;
}

LONG CChannel::GetDisChargeAh()
{
	return m_lDisChargeAh;
}

LONG CChannel::GetCapacitance()
{
	return m_lCapacitance;
}

LONG CChannel::GetChargeWh()
{
	return m_lChargeWh;
}

LONG CChannel::GetDisChargeWh()
{
	return m_lDisChargeWh;
}

LONG CChannel::GetSyncDate()
{
	return m_lSyncDate;
}

LONG CChannel::GetSyncTime()
{
	return m_lSyncTime;
}

WORD CChannel::GetAccCycleCount1()
{
	return m_nAccCycleGroupNum1;
}
WORD CChannel::GetAccCycleCount2()
{
	return m_nAccCycleGroupNum2;
}
WORD CChannel::GetAccCycleCount3()
{
	return m_nAccCycleGroupNum3;
}
WORD CChannel::GetAccCycleCount4()
{
	return m_nAccCycleGroupNum4;
}
WORD CChannel::GetAccCycleCount5()
{
	return m_nAccCycleGroupNum5;
}

WORD CChannel::GetMultiCycleCount1()
{
	return m_nMultiCycleGroupNum1;
}
WORD CChannel::GetMultiCycleCount2()
{
	return m_nMultiCycleGroupNum2;
}
WORD CChannel::GetMultiCycleCount3()
{
	return m_nMultiCycleGroupNum3;
}
WORD CChannel::GetMultiCycleCount4()
{
	return m_nMultiCycleGroupNum4;
}
WORD CChannel::GetMultiCycleCount5()
{
	return m_nMultiCycleGroupNum5;
}

WORD CChannel::GetMuxUse()
{
	return m_nMuxUse;
}

WORD CChannel::GetMuxBackup()
{
	return m_nMuxBackup;
}

BYTE CChannel::GetCbank()
{
	return m_cbank;
}