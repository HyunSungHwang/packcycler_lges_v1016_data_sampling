// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__5B1D46D8_D4CA_4C63_9457_BEC639FCBAE6__INCLUDED_)
#define AFX_STDAFX_H__5B1D46D8_D4CA_4C63_9457_BEC639FCBAE6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT


#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#ifndef EXPORT_DLL_API
#define EXPORT_DLL_API
#endif

#include <winsock2.h>
#include "../../Include/SFTForm.h"

//�ڡ� ljb 20091029 
extern int		nInstanceCount;			//Module Struct
extern int		m_Initialized;
extern BOOL		m_bServerOpen;
extern int		m_nInstalledModuleNum;
extern SFT_VARIABLE_CH_DATA g_variableChdata[SFT_MAX_BD_PER_MD][SFT_MAX_CH_PER_MD];
extern SFT_CHAMBER_VALUE g_chamberData[SFT_MAX_BD_PER_MD][SFT_MAX_CH_PER_MD];

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__5B1D46D8_D4CA_4C63_9457_BEC639FCBAE6__INCLUDED_)
