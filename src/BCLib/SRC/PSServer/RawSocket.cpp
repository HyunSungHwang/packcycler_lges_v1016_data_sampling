/////////////////////////////////////////////////////////////////////
// Class Creator Version 2.0.000 Copyrigth (C) Poul A. Costinsky 1994
///////////////////////////////////////////////////////////////////
// Implementation File RawSocket.cpp
// class CWizRawSocket
//
// 23/07/1996 14:54                       Author: Poul, Hadas & Oren
///////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RawSocket.h"

#ifdef _UNICODE
#include <winnls.h>
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

extern BOOL WriteLog(char *szLog);
//*****************************************************************
void CWizSyncSocket::Init(SOCKET h)
{
	m_hSocket = h;


	m_nPacketLogLevel = 0; //ksj 20210107
	InitPacketLogConfig(); //ksj 20210107
}

//*****************************************************************
void CWizSyncSocket::Close()
{
	ASSERT(m_hSocket != INVALID_SOCKET);
	::closesocket(m_hSocket);
	m_hSocket = INVALID_SOCKET;
}

//*****************************************************************
int CWizSyncSocket::SetIntOption(int level, int optname, int val)
{
	return ::setsockopt (m_hSocket, level, optname, (char *)&val, sizeof(val));
}

//*****************************************************************
BOOL CWizSyncSocket::InitializeSocket(int nPort)
{
	// Socket must be created with socket()
	ASSERT(m_hSocket != INVALID_SOCKET);

	// Make up address
	SOCKADDR_IN	SockAddr;
	memset(&SockAddr,0, sizeof(SockAddr));
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = INADDR_ANY;
	SockAddr.sin_port   = ::htons((unsigned short)nPort);

	// Bind to the address and port
	int r = ::bind(m_hSocket, (SOCKADDR*)&SockAddr, sizeof(SockAddr));
	if (r == 0)
	{
		SetIntOption (SOL_SOCKET, SO_DONTLINGER,1);
		SetIntOption (SOL_SOCKET, SO_KEEPALIVE,1);

		int optVal;
		 int optLen = sizeof(int);

		::getsockopt(m_hSocket, SOL_SOCKET, SO_SNDBUF, (char *)&optVal, &optLen);
		SetIntOption (SOL_SOCKET, SO_SNDBUF,optVal*2);
		// establishes a socket to listen for incoming connection so Accept can be called
		r = ::listen (m_hSocket, 5);
		if (r == 0)
			return TRUE;
	}
	return FALSE;
}

//*****************************************************************
BOOL CWizSyncSocket::Create(int nPort)
{
	// creates a socket
	m_hSocket = ::socket(PF_INET, SOCK_STREAM, 0);
	if (m_hSocket == INVALID_SOCKET)
		return FALSE;

	// Bind to the port
	if (!InitializeSocket(nPort))
	{
		Close();
		return FALSE;
	}
	return TRUE;
}

//*****************************************************************
// Create an invalid socket
CWizSyncSocket::CWizSyncSocket(SOCKET h /* = INVALID_SOCKET */)
{
	Init(h);
}

//*****************************************************************
// Create a listening socket
CWizSyncSocket::CWizSyncSocket(int nPort)
{
	Init(INVALID_SOCKET);
	if(!Create(nPort))
	{
		MessageBox(NULL, "Port is Used By Another Application", "Port Error", MB_ICONSTOP|MB_OK);
		throw XCannotCreate();
	}
}

//*****************************************************************
CWizSyncSocket::~CWizSyncSocket()
{
	if (m_hSocket != INVALID_SOCKET)
		Close();
}

//*****************************************************************
// Waits for pending connections on the port, 
// creates a new socket with the same properties as this and returns a handle to the new socket
SOCKET CWizSyncSocket::Accept()
{
	SOCKET h = ::accept (m_hSocket, NULL, NULL);
	return h;
}

//*****************************************************************
// Cerates a socket and establishes a connection to a peer
// on lpszHostAddress:nHostPort 
BOOL CWizSyncSocket::Connect(LPCTSTR lpszHostAddress, UINT nHostPort )
{
	ASSERT(lpszHostAddress != NULL);

	// Create ? socket
	if (m_hSocket == INVALID_SOCKET)
	{
		m_hSocket = ::socket(PF_INET, SOCK_STREAM, 0);
		if (m_hSocket == INVALID_SOCKET)
			return FALSE;
	}

	// Fill address machinery of sockets.
	SOCKADDR_IN sockAddr;
	memset(&sockAddr,0,sizeof(sockAddr));

#ifdef _UNICODE
	char buff[500];
	WideCharToMultiByte(CP_ACP, 0, lpszHostAddress, -1, buff, 500, NULL, NULL);
	LPSTR lpszAscii = buff;
#else
	LPSTR lpszAscii = (LPSTR)lpszHostAddress;
#endif

	sockAddr.sin_family = AF_INET;
	sockAddr.sin_addr.s_addr = inet_addr(lpszAscii);
	if (sockAddr.sin_addr.s_addr == INADDR_NONE)
	{
		LPHOSTENT lphost;
		lphost = gethostbyname(lpszAscii);
		if (lphost != NULL)
			sockAddr.sin_addr.s_addr = ((LPIN_ADDR)lphost->h_addr)->s_addr;
		else
		{
			WSASetLastError(WSAEINVAL);
			return FALSE;
		}
	}
	sockAddr.sin_port = htons((u_short)nHostPort);
	// connects to peer
	int r = ::connect(m_hSocket, (SOCKADDR*)&sockAddr, sizeof(sockAddr));
	if (r != SOCKET_ERROR)
	{
		ASSERT(r == 0);
		return TRUE;
	}

	int e = ::WSAGetLastError();
	ASSERT(e != WSAEWOULDBLOCK);

	return FALSE;
}

//*****************************************************************
// read raw data
//int	CWizReadWriteSocket::Read(void *pData, int nLen) 
int	CWizReadWriteSocket::Read(void *pData, int nLen, int nWritePacketLogLevel) 
{
	char* pcData = (char* )pData;
	int	n = nLen;

	// if data size is bigger then network buffer handle it nice
	do
	{
		int r1 = ::recv (m_hSocket, pcData, n, 0);
		if (r1 == SOCKET_ERROR)
		{
			int e = WSAGetLastError();
			if( e == WSAEWOULDBLOCK) return (nLen - n);
			else {
				return -1;
			}
// 			if( e == WSAECONNRESET) return (nLen - n);
// 			else {
// 				return -1;
// 			}
			//ASSERT(e == WSAEWOULDBLOCK);
//			return (nLen - n);
			return -1; 
		}
		else if (r1 == 0)
			return -1; 
//			return (nLen - n);
		else if (r1 < 0)
		{
//			ASSERT(0);
			return -1;
//			return (nLen - n);
		}
		pcData += r1;
		n -= r1;
	} while (n > 0);

	ASSERT(n == 0);


	WritePacketLog(0,(BYTE*)pData,nLen,nWritePacketLogLevel); //ksj 20210107 : 통신 패킷 로그 추가

	return nLen;
}

//*****************************************************************
// write raw data

//int	CWizReadWriteSocket::Write(const void *pData, int nLen) 
int	CWizReadWriteSocket::Write(const void *pData, int nLen, int nWritePacketLogLevel) 
{
	const char* pcData = (const char* )pData;
	int	n = nLen;
	//char msg[128];
	//TRACE("pData = %c\n",pData);

	// if data size is bigger then network buffer handle it nice
	int nTimeout = 0;
	do
	{
		//ksj 20201118
 		char msg[128];
// 		sprintf(msg, "socket Send : size: %d",n);
// 		WriteLog(msg);
		//ksj end

		int r1 = ::send (m_hSocket, pcData, n, 0);

		//ksj 20201118
// 		sprintf(msg, "socket Send result : %d",r1);
// 		WriteLog(msg);
		//ksj end

		if (r1 == SOCKET_ERROR)
		{
			int e = WSAGetLastError();
//			ASSERT(e != WSAEWOULDBLOCK);
			TRACE("Here (WSAGetLastError %d)\n", e);

			//ksj 20201118
			sprintf(msg, "Here (WSAGetLastError %d) timeout cnt:%d",e,nTimeout); 
			WriteLog(msg);
			//ksj end

			Sleep(10);
			nTimeout++;
			if(nTimeout > 100)
			{
				//ksj 20201118
				sprintf(msg, "fail 1"); 
				WriteLog(msg);
				//ksj end

				return 0;
			}
//			return 0;
		}
		else if (r1 == 0)
		{
			//ksj 20201118
			sprintf(msg, "fail 2"); 
			WriteLog(msg);
			//ksj end

			return 0;
		}	
		else if (r1 < 0)
		{
			ASSERT(0);

			//ksj 20201118
			sprintf(msg, "fail 3"); 
			WriteLog(msg);
			//ksj end

			return 0;
		}
		pcData += r1;
		n -= r1;

		//ksj 20201118
// 		sprintf(msg, "while %d",n); 
// 		WriteLog(msg);
		//ksj end
	} while (n > 0);

	ASSERT(n == 0);


	WritePacketLog(1,(BYTE*)pData,nLen,nWritePacketLogLevel); //ksj 20210107 : 통신 패킷 로그 추가

	return nLen;
}

//*****************************************************************
// Reads UNICODE string from socket.
// Converts string from ANSI to UNICODE.
int CWizReadWriteSocket::ReadString  (WCHAR* lpszString, int nMaxLen)
{
	// read string length
	u_long nt_Len;
	if (Read(&nt_Len, sizeof(nt_Len)) < sizeof(nt_Len))
		return 0;

	int Len = ntohl(nt_Len);
	if (Len == 0 || Len >= nMaxLen)
		return 0;

	static const int BUFF_SIZE = 2000;
	if (Len >= BUFF_SIZE)
		return 0;

	char buff[BUFF_SIZE];
	// Read ANSI string to the buffer
	if (Read(buff, Len) < Len)
		return 0;
	buff[Len] = 0;

	// Convert ANSI string to the UNICODE
	VERIFY(::MultiByteToWideChar(CP_ACP, 0, buff, Len, lpszString, nMaxLen*sizeof(lpszString[0])));
	return Len;
}

//*****************************************************************
// Reads ANSI string from socket.
int CWizReadWriteSocket::ReadString  (char* lpszString, int nMaxLen)
{
	// read string length
	u_long nt_Len;
	if (Read(&nt_Len, sizeof(nt_Len)) < sizeof(nt_Len))
		return 0;
	int Len = ntohl(nt_Len);
	if (Len == 0 || Len >= nMaxLen)
		return 0;

	// Read ANSI string
	if (Read(lpszString, Len) < Len)
		return 0;
	lpszString[Len] = 0;
	return Len;
}

//*****************************************************************
inline int Length(const char* p)
{
	return strlen(p);
}

//*****************************************************************
inline int Length(const WCHAR* p)
{
	return wcslen(p);
}

//*****************************************************************
// Writes UNICODE string to socket, converts UNICODE string to ANSI.
BOOL CWizReadWriteSocket::WriteString (const WCHAR*  lpszString, int nLen /* = -1*/)
{
	if (nLen < 0)
		nLen = Length(lpszString);
	static const int BUFF_SIZE = 2000;
	if (nLen >= BUFF_SIZE*sizeof(lpszString) + sizeof(u_long))
		return FALSE;
	char buff[BUFF_SIZE];
	u_long nt_Len = htonl(nLen);
	int nSize = sizeof(nt_Len);
	memcpy(buff, &nt_Len, nSize);

	// To make one call less, the length of the string
	// copied to the buffer before the string itself and the buffer sent once.
	char* ptr = buff + nSize;
	if (nLen > 0)
	{
		// Convert ANSI to UNICODE
		int s = WideCharToMultiByte(CP_ACP, 0, lpszString, nLen, ptr, BUFF_SIZE - sizeof(u_long), NULL, NULL);
		ASSERT(s > 0);
		nSize += s;
	}
	return Write(buff, nSize);
}

//*****************************************************************
// Writes ANSI string to socket.
BOOL CWizReadWriteSocket::WriteString (const char* lpszString, int nLen /* = -1*/)
{
	if (nLen < 0)
		nLen = strlen(lpszString);
	static const int BUFF_SIZE = 2000;
	if (nLen >= BUFF_SIZE*sizeof(lpszString) + sizeof(u_long))
		return FALSE;
	char buff[BUFF_SIZE];
	u_long nt_Len = htonl(nLen);
	int nSize = sizeof(nt_Len);
	memcpy(buff, &nt_Len, nSize);
	// To make one call less, the length of the string
	// copied to the buffer before the string itself and the buffer sent once.
	char* ptr = buff + nSize;
	if (nLen > 0)
	{
		memcpy(ptr, lpszString, nLen);
		nSize += nLen;
	}
	return Write(buff, nSize);
}

//*****************************************************************
BOOL CWizSyncSocket::GetHostName(LPTSTR lpszAddress, size_t nAddrSize, UINT& rSocketPort)
{
	if (nAddrSize < 1)
		return FALSE;

	*lpszAddress = TCHAR(0);
	SOCKADDR_IN sockAddr;
	memset(&sockAddr, 0, sizeof(sockAddr));
	int nSockAddrLen = sizeof(sockAddr);
	int r;

	while ((r = ::getsockname(m_hSocket, (SOCKADDR*)&sockAddr, &nSockAddrLen)) == SOCKET_ERROR)
	{
		const int e = ::WSAGetLastError();
		if (e != WSAEINPROGRESS)
			return FALSE;
	}

	ASSERT(r == 0);
	rSocketPort = ::ntohs(sockAddr.sin_port);

	char    szName[64];
	struct  hostent *h;
	DWORD	dwMyAddress;

	int r1;
	while((r1 = ::gethostname(szName,sizeof(szName))) == SOCKET_ERROR)
	{
		const int e = ::WSAGetLastError();
		if (e != WSAEINPROGRESS)
			return FALSE;
	}

	ASSERT(r1 == 0);
	h = (struct hostent *) ::gethostbyname(szName);
	memcpy(&dwMyAddress,h->h_addr_list[0],sizeof(DWORD));
	if (dwMyAddress == INADDR_NONE)
		return FALSE;
	struct   in_addr     tAddr;
	memcpy(&tAddr,&dwMyAddress,sizeof(DWORD));
	char    *ptr = ::inet_ntoa(tAddr);

#ifdef _UNICODE
	return ::MultiByteToWideChar(CP_ACP, 0, ptr, -1, lpszAddress, nAddrSize*sizeof(lpszAddress[0]));
#else
	if (size_t(lstrlen(ptr)) >= nAddrSize)
		return FALSE;
	lstrcpy(lpszAddress, ptr);
#endif

	return TRUE;
}

//*****************************************************************
BOOL CWizSyncSocket::GetPeerName(LPTSTR lpszAddress, size_t nAddrSize, UINT& rPeerPort)
{
	if (nAddrSize < 1)
		return FALSE;
	*lpszAddress = TCHAR(0);
	SOCKADDR_IN sockAddr;
	memset(&sockAddr, 0, sizeof(sockAddr));

	int nSockAddrLen = sizeof(sockAddr);
	int r;
	while ((r = ::getpeername(m_hSocket, (SOCKADDR*)&sockAddr, &nSockAddrLen)) == SOCKET_ERROR)
	{
		const int e = ::WSAGetLastError();
		if (e != WSAEINPROGRESS)
			return FALSE;
	}

	ASSERT(r == 0);
	rPeerPort = ntohs(sockAddr.sin_port);
	char * pAddr = inet_ntoa(sockAddr.sin_addr);
	int len = strlen(pAddr);
#ifdef _UNICODE
	char buff[100];
	if (len >= 100 || len >= int(nAddrSize))
		return FALSE;
	memcpy(buff, pAddr,100);
	return ::MultiByteToWideChar(CP_ACP, 0, buff, len, lpszAddress, nAddrSize*sizeof(lpszAddress[0]));
#else
	if (size_t(len) >= nAddrSize)
		return FALSE;
	memcpy(lpszAddress, pAddr,len + 1);
#endif
	return TRUE;
}

//*****************************************************************
/*
#ifdef _DEBUG
void Foo()
{
	CWizReadWriteSocket s;
	int i;
	short j;
	char k;
	double d;

	s << i << j << k << d;
	s >> i >> j >> k >> d;
}

#endif
*/

//ksj 20210106 : packet 로그용 함수 추가.
//int nType  0:recv 1:send
//BYTE* pPacket 로그 남길 패킷 기록
//int nTotalSize 패킷 사이즈
//**주의** 호출될때마다 파일을 Open/Close 하는 방식이므로,
//통신 패킷 발생할때마다 파일 액세스 발생함.
//파일 기록에 걸리는 시간까지 포함하여 통신 속도 및 안정성에 영향 줄 수 있으므로 주의
//특히 풀로그는 디버그시에만 사용!!!!!
int CWizSyncSocket::WritePacketLog(int nType, BYTE* pPacket, int nTotalSize, int nWritePacketLogLevel)
{	
	//로그 허용 수준에 따라 로그 남김.
	//0 : 모든 로그 남김 (full log)
	//1 : Connection Sequence 및 Send 로그만 남김
	//2이상 : 로그 안남김.
	if(nWritePacketLogLevel >= m_nPacketLogLevel)
	{
		return 0;
	}

	if(pPacket == NULL) return 0;	
	if(nTotalSize == 0) return 0;

	int nCmdID = 0;
	CString strLog, str;
	CString strType;

	if(nTotalSize>=4) //커맨드 ID 추출
		memcpy(&nCmdID,pPacket,sizeof(int));
	
	if(m_nPacketLogLevel !=0 )
	{
		//Full Log가 아니면 남기지 않는 CMD ID 블랙 리스트
		int nCmdIdBlackList[]=
						{
						SFT_CMD_RESPONSE,
						SFT_CMD_HEARTBEAT,
						SFT_CMD_VARIABLE_CH_DATA
						};

		int nBlackListCnt = sizeof(nCmdIdBlackList)/sizeof(int);

		for(int i=0;i<nBlackListCnt;i++)
		{
			if(nCmdID == nCmdIdBlackList[i])
			{
				return 0; //블랙리스트에 포함된 CMD ID면 로그 남기지 않고 리턴한다.
			}
		}
	}

	if(nType == 0) strType.Format("recv");
	else strType.Format("send");

	strLog.Format("[%s][%dbytes]: ",strType,nTotalSize);
	int nLength = strLog.GetLength();

	for(int i=0;i<nTotalSize;i++)
	{
		str.Format("%02x ",pPacket[i]);	
		strLog += str;
	}

	//WriteLog(strLog.GetBuffer());

	SYSTEMTIME systemTime;   // system time
	::GetLocalTime(&systemTime);

	char	szPacketLogFileName[512] = {""};
	sprintf(szPacketLogFileName, "C:\\Program Files (x86)\\PNE CTSPack\\log\\%d%02d%02d_packet.log", systemTime.wYear, systemTime.wMonth, systemTime.wDay);


	FILE *fp = fopen(szPacketLogFileName, "ab+");
	if(fp == NULL)		return FALSE;

	//	TRACE("%s\n", szLog);		//ljb remove 20090604
	fprintf(fp, "%02d/%02d/%02d %02d:%02d:%02d :: %s\r\n",	systemTime.wYear, systemTime.wMonth, systemTime.wDay,
		systemTime.wHour, systemTime.wMinute, systemTime.wSecond, 
		strLog.GetBuffer());
	fclose(fp);
	return TRUE;

	return 1;
}

//ksj 20210107 : 레지스트리에서 패킷 로그 수준 설정을 가져온다
//Software\\PNE CTSPack\\CTSMonPro\\Config "PacketLogLevel"
//0: 로그 안남김, 1: 커넥션 시퀀스 로그와, Write 로그만 남기기 2: Read/Write 모든 로그 남김
int CWizSyncSocket::InitPacketLogConfig(void)
{
	CRegKey regKey; 
	DWORD dwValue;	
	m_nPacketLogLevel = 0;
		
	if(regKey.Open(HKEY_CURRENT_USER, _T("Software\\PNE CTSPack\\CTSMonPro\\Config")) == ERROR_SUCCESS){
		if(regKey.QueryValue(dwValue,_T("PacketLogLevel")) == ERROR_SUCCESS){
			regKey.Close();
			m_nPacketLogLevel = dwValue;
		}
		regKey.Close();
	}

	return 0;
}
