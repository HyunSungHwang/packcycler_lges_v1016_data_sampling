// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Step.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStep::CStep(int nStepType /*=0*/)
{
	ClearStepData();
	m_type = (BYTE)nStepType;
}

CStep::CStep()
{
	ClearStepData();
}

CStep::~CStep()
{

}

BOOL CStep::ClearStepData()
{
	m_type = 0;
	m_StepIndex = 0;
	m_mode = 0;
	m_bGrade = FALSE;
	m_lProcType = 0;				
	m_fVref_Charge = 0;
	m_fVref_DisCharge = 0;
    m_fIref = 0;
    m_fPref = 0;
	m_fRref = 0;

	m_fStartT = 0.0f;
	m_fEndT = 0.0f;
	m_fTref = 0.0f;
	m_fTrate = 0.0f;
	m_fHref = 0.0f;
	m_fHrate = 0.0f;
    
	m_fEndTime = 0;
    m_fEndV_H = 0;
	m_fEndV_L = 0;
    m_fEndI = 0;
    m_fEndC = 0;
    m_fEndDV = 0;
    m_fEndDI = 0;
	m_fEndW = 0.0f;
	m_fEndWh = 0.0f;

	m_bValueItem = 0;					//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	m_bValueStepNo = 0;					//비교할 기준 용량이 속한 Step번호
	m_fValueRate = 0;						//비교 SOC Rate


	m_nLoopInfoCycle = 0;	
	m_nLoopInfoGotoStep = 0;	

	m_nMultiLoopGroupID = 0;
	m_nMultiLoopInfoCycle = 0;
	m_nMultiLoopInfoGotoStep = 0;

	m_nAccLoopGroupID = 0;
	m_nAccLoopInfoCycle = 0;
	m_nAccLoopInfoGotoStep = 0;

	m_nGotoStepEndV_H = 0;
	m_nGotoStepEndV_L = 0;
	m_nGotoStepEndTime = 0;
	m_nGotoStepEndCVTime = 0;
	m_nGotoStepEndC = 0;
	m_nGotoStepEndWh = 0;
	m_nGotoStepEndValue = 0;

	m_fHighLimitV = 0;
    m_fLowLimitV = 0;
    m_fHighLimitI = 0;
    m_fLowLimitI = 0;
    m_fHighLimitC = 0;
    m_fLowLimitC = 0;
	m_fHighCapacitance = 0;
	m_fLowCapacitance = 0;
	m_fHighLimitImp = 0;
    m_fLowLimitImp = 0;
	m_fHighLimitTemp = 0.0f;
	m_fLowLimitTemp = 0.0f; 
	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = 0;			
		m_fCompHighV[i] = 0.0f;
		m_fCompLowV[i] = 0.0f;
		m_fCompTimeI[i] = 0;			
		m_fCompHighI[i] = 0.0f;
		m_fCompLowI[i] = 0.0f;
	}
    m_fDeltaTimeV = 0;
    m_fDeltaV = 0.0f;
    m_fDeltaTimeI = 0;
    m_fDeltaI = 0.0f;

	m_fReportTemp = 0.0f;
	m_fReportV = 0.0f;
	m_fReportI = 0.0f;
	m_fReportTime = 0;

	m_fCapaVoltage1		=0;	
	m_fCapaVoltage2		=0;	

	m_fDCRStartTime	=0;	
	m_fDCREndTime		=0;

	m_fLCStartTime		=0;	
	m_fLCEndTime		=0;		

	m_lRange		=0;			

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	m_lEndTimeDay = 0;
	m_lCVTimeDay = 0;
	// -
	//////////////////////////////////////////////////////////////////////////
	m_Grading.ClearStep();
	m_StepID = 0;

	m_strPatternFileName.Empty();
	m_lValueLimitLow=0;
	m_lValueLimitHigh=0;
	
	m_lEndTimeDay=0;		//ljb 20131212 add
	m_lCVTimeDay=0;		//ljb 20131212 add
	
	m_fValueMax=0;		//20140207 add Pattern data 중 최대값
	m_fValueMin=0;		//20140207 add Pattern data 중 최소값 
	
	m_nNoCheckMode=0;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	m_nCanTxOffMode=0;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	m_nCanCheckMode=0;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	nValueLoaderItem=0;		//0: 사용안함 , 1: ON,	 2: OFF
	nValueLoaderMode=0;		//0: CP, 1: CC, 2: CV, 3: CR
	fValueLoaderSet=0.0f;		//설정값 W,A,V,Ohm

	m_nWaitTimeInit=0;		
	m_nWaitTimeDay=0;		//대기 일
	m_nWaitTimeHour=0;	//대기 시간
	m_nWaitTimeMin=0;		//대기  분
	m_nWaitTimeSec=0;		//대기  초

	m_bStepChamberStop=0; //yulee 20180829

	m_nCellBal_CircuitRes=0;
	m_nCellBal_WorkVoltUpper=0;//mV
	m_nCellBal_WorkVolt=0;//mV
	m_nCellBal_StartVolt=0;//mV
	m_nCellBal_EndVolt=0;//mV
	m_nCellBal_AutoStopTime=0;
	m_nCellBal_State=0;//Use_State
	
	//cny PowerSupply
	m_nPwrSupply_Cmd=0;//1-ON,2-OFF,0-NONE
	m_nPwrSupply_Volt=0;//mV
	m_nPwrSupply_State=0;//Use_State
	
	//cny Chiller
	m_nChiller_Cmd=0;
	m_fChiller_RefTemp=0;
	m_nChiller_RefCmd=0;
	m_nChiller_PumpCmd=0;
	m_fChiller_RefPump=0;
	m_nChiller_TpData=0;
	m_fChiller_ONTemp1=0;
	m_nChiller_ONTemp1_Cmd=0;
	m_fChiller_ONTemp2=0;
	m_nChiller_ONTemp2_Cmd=0;
	m_fChiller_OFFTemp1=0;
	m_nChiller_OFFTemp1_Cmd=0;
	m_fChiller_OFFTemp2=0;
	m_nChiller_OFFTemp2_Cmd=0;
	m_nChiller_State=0;//Use_State
	m_nChiller_Log=0;


	m_fCellDeltaVStep=0.0f;
	m_fStepDeltaAuxTemp=0.0f;
	m_fStepDeltaAuxTH=0.0f;
	m_fStepDeltaAuxT=0.0f;
	m_fStepDeltaAuxVTime=0.0f;
	m_fStepAuxV=0.0f;

	m_bStepDeltaVentAuxV=FALSE;
	m_bStepDeltaVentAuxTemp=FALSE;
	m_bStepDeltaVentAuxTh=FALSE;
	m_bStepDeltaVentAuxT=FALSE;
	m_bStepVentAuxV=FALSE;

	return TRUE;
}

void CStep::operator=(CStep &step) 
{
	m_type = step.m_type;
	m_StepIndex = step.m_StepIndex;
	m_mode = step.m_mode;
	m_bGrade = step.m_bGrade;
	m_lProcType = step.m_lProcType;				
	m_fVref_Charge = step.m_fVref_Charge;
	m_fVref_DisCharge = step.m_fVref_DisCharge;
    m_fIref = step.m_fIref;
    m_fPref = step.m_fPref;
	m_fRref = step.m_fRref;

	m_fStartT	=step.m_fStartT;
	m_fEndT		= step.m_fEndT;
	m_fTref		= step.m_fTref;
	m_fTrate	= step.m_fTrate;
	m_fHref		= step.m_fHref;
	m_fHrate	= step.m_fHrate;
    
	m_fEndTime = step.m_fEndTime;
    m_fEndV_H = step.m_fEndV_H;
	m_fEndV_L = step.m_fEndV_L;
    m_fEndI = step.m_fEndI;
    m_fEndC = step.m_fEndC;
    m_fEndDV = step.m_fEndDV;
    m_fEndDI = step.m_fEndDI;
	m_fEndW	 = step.m_fEndW;
	m_fEndWh = step.m_fEndWh;

	m_bValueItem = step.m_bValueItem;
	m_bValueStepNo = step.m_bValueStepNo;
	m_fValueRate = step.m_fValueRate;

	m_nLoopInfoCycle = step.m_nLoopInfoCycle;	
	m_nLoopInfoGotoStep = step.m_nLoopInfoGotoStep;	

	m_nMultiLoopGroupID	= step.m_nMultiLoopGroupID;
	m_nMultiLoopInfoCycle	= step.m_nMultiLoopInfoCycle;
	m_nMultiLoopInfoGotoStep	= step.m_nMultiLoopInfoGotoStep;

	m_nAccLoopGroupID	= step.m_nAccLoopGroupID;
	m_nAccLoopInfoCycle	= step.m_nAccLoopInfoCycle;
	m_nAccLoopInfoGotoStep	= step.m_nAccLoopInfoGotoStep;

	m_nGotoStepEndV_H = step.m_nGotoStepEndV_H;
	m_nGotoStepEndV_L = step.m_nGotoStepEndV_L;
	m_nGotoStepEndTime = step.m_nGotoStepEndTime;
	m_nGotoStepEndCVTime = step.m_nGotoStepEndCVTime;
	m_nGotoStepEndC = step.m_nGotoStepEndC;
	m_nGotoStepEndWh = step.m_nGotoStepEndWh;
	m_nGotoStepEndValue = step.m_nGotoStepEndValue;

	m_fHighLimitV = step.m_fHighLimitV;
    m_fLowLimitV = step.m_fLowLimitV;
    m_fHighLimitI = step.m_fHighLimitI;
    m_fLowLimitI = step.m_fLowLimitI;
    m_fHighLimitC = step.m_fHighLimitC;
	m_fHighCapacitance	= step.m_fHighCapacitance;
	m_fLowCapacitance	= step.m_fLowCapacitance;
    m_fLowLimitC = step.m_fLowLimitC;
	m_fHighLimitImp = step.m_fHighLimitImp;
    m_fLowLimitImp = step.m_fLowLimitImp;
	m_fHighLimitTemp = step.m_fHighLimitTemp;
	m_fLowLimitTemp = step.m_fLowLimitTemp;

	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = step.m_fCompTimeV[i];			
		m_fCompHighV[i] = step.m_fCompHighV[i];
		m_fCompLowV[i] = step.m_fCompLowV[i];
		m_fCompTimeI[i] = step.m_fCompTimeI[i];			
		m_fCompHighI[i] = step.m_fCompHighI[i];
		m_fCompLowI[i] = step.m_fCompLowI[i];
	}
    m_fDeltaTimeV = step.m_fDeltaTimeV;
    m_fDeltaV = step.m_fDeltaV;
    m_fDeltaTimeI = step.m_fDeltaTimeI;
    m_fDeltaI = step.m_fDeltaI;

	m_fReportTemp = step.m_fReportTemp;
	m_fReportV	=	step.m_fReportV;
	m_fReportI	=	step.m_fReportI;
	m_fReportTime	=	step.m_fReportTime;

	m_fCapaVoltage1		=step.m_fCapaVoltage1;	
	m_fCapaVoltage2		=step.m_fCapaVoltage2;	

	m_fDCRStartTime	=step.m_fDCRStartTime;	
	m_fDCREndTime		=step.m_fDCREndTime;

	m_fLCStartTime		=step.m_fLCStartTime;	
	m_fLCEndTime		=step.m_fLCEndTime;		

	m_lRange		=step.m_lRange;		
	
	m_bStepChamberStop	= step.m_bStepChamberStop; //yulee 20180828


	m_Grading.SetGradingData(step.m_Grading);

	m_StepID = step.m_StepID;

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	m_lEndTimeDay = step.m_lEndTimeDay;
	m_lCVTimeDay = step.m_lCVTimeDay;
	// -
	//////////////////////////////////////////////////////////////////////////

	m_fCellDeltaVStep			= step.m_fCellDeltaVStep;
	m_fStepDeltaAuxTemp			= step.m_fStepDeltaAuxTemp;
	m_fStepDeltaAuxTH			= step.m_fStepDeltaAuxTH;
	m_fStepDeltaAuxT			= step.m_fStepDeltaAuxT;
	m_fStepDeltaAuxVTime		= step.m_fStepDeltaAuxVTime;
	m_fStepAuxV					= step.m_fStepAuxV;

	m_bStepDeltaVentAuxV		= step.m_bStepDeltaVentAuxV;
	m_bStepDeltaVentAuxTemp		= step.m_bStepDeltaVentAuxTemp;
	m_bStepDeltaVentAuxTh		= step.m_bStepDeltaVentAuxTh;
	m_bStepDeltaVentAuxT		= step.m_bStepDeltaVentAuxT;
	m_bStepVentAuxV				= step.m_bStepVentAuxV;

	GRADE_STEP data;
	m_Grading.ClearStep();
	for(int i=0; i<step.m_Grading.GetGradeStepSize(); i++)
	{
		data = step.m_Grading.GetStepData(i);
		m_Grading.AddGradeStep(data.lGradeItem, data.fMin, data.fMax, data.strCode);	//20081210 KHS
	}

	m_strPatternFileName = step.m_strPatternFileName;
	m_lValueLimitLow = step.m_lValueLimitLow;
	m_lValueLimitHigh = step.m_lValueLimitHigh;
	
	m_lEndTimeDay = step.m_lEndTimeDay;		//ljb 20131212 add
	m_lCVTimeDay = step.m_lCVTimeDay;		//ljb 20131212 add
	
	m_fValueMax = step.m_fValueMax;		//20140207 add Pattern data 중 최대값
	m_fValueMin = step.m_fValueMin;		//20140207 add Pattern data 중 최소값 
	
	m_nNoCheckMode = step.m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	m_nCanTxOffMode = step.m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	m_nCanCheckMode = step.m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	nValueLoaderItem = step.nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	nValueLoaderMode = step.nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	fValueLoaderSet = step.fValueLoaderSet;		//설정값 W,A,V,Ohm
	
	m_nWaitTimeInit = step.m_nWaitTimeInit;		
	m_nWaitTimeDay = step.m_nWaitTimeDay;		//대기 일
	m_nWaitTimeHour = step.m_nWaitTimeHour;		//대기 시간
	m_nWaitTimeMin = step.m_nWaitTimeMin;		//대기  분
	m_nWaitTimeSec = step.m_nWaitTimeSec;		//대기  초
}

BOOL CStep::Fun_SaveCellBALFileToSch(FILE *pFile,CStep *pStep)
{
	FILE_CELLBAL_INFO fileCellBAL;
	ZeroMemory(&fileCellBAL,sizeof(FILE_CELLBAL_INFO));
	fileCellBAL.nStepIndex              = pStep->m_StepIndex;
	fileCellBAL.nCellBal_CircuitRes     = pStep->m_nCellBal_CircuitRes;
	fileCellBAL.nCellBal_WorkVoltUpper  = pStep->m_nCellBal_WorkVoltUpper;
	fileCellBAL.nCellBal_WorkVolt       = pStep->m_nCellBal_WorkVolt;
	fileCellBAL.nCellBal_StartVolt      = pStep->m_nCellBal_StartVolt;
	fileCellBAL.nCellBal_EndVolt        = pStep->m_nCellBal_EndVolt;
	fileCellBAL.nCellBal_ActiveTime	    = pStep->m_nCellBal_ActiveTime;
	fileCellBAL.nCellBal_AutoStopTime   = pStep->m_nCellBal_AutoStopTime;
	fileCellBAL.nCellBal_AutoNextStep   = pStep->m_nCellBal_AutoNextStep;

	fwrite(&fileCellBAL,sizeof(FILE_CELLBAL_INFO),1,pFile);
	return TRUE;
}

BOOL CStep::Fun_SavePwrSupplyFileToSch(FILE *pFile,CStep *pStep)
{
	FILE_PWRSUPPLY_INFO filePwrSupply;
	ZeroMemory(&filePwrSupply,sizeof(FILE_PWRSUPPLY_INFO));
	filePwrSupply.nStepIndex      = pStep->m_StepIndex;
	filePwrSupply.nPwrSupply_Cmd  = pStep->m_nPwrSupply_Cmd;
	filePwrSupply.nPwrSupply_Volt = pStep->m_nPwrSupply_Volt;

	fwrite(&filePwrSupply,sizeof(FILE_PWRSUPPLY_INFO),1,pFile);
	return TRUE;
}

BOOL CStep::Fun_SaveChillerFileToSch(FILE *pFile,CStep *pStep)
{
	FILE_CHILLER_INFO fileChiller;
	ZeroMemory(&fileChiller,sizeof(FILE_CHILLER_INFO));
	fileChiller.nStepIndex           =pStep->m_StepIndex;
	fileChiller.nChiller_Cmd         =pStep->m_nChiller_Cmd;
	fileChiller.fChiller_RefTemp     =pStep->m_fChiller_RefTemp;
	fileChiller.nChiller_RefCmd      =pStep->m_nChiller_RefCmd;
	fileChiller.fChiller_RefPump     =pStep->m_fChiller_RefPump;
	fileChiller.nChiller_PumpCmd     =pStep->m_nChiller_PumpCmd;
	fileChiller.nChiller_TpData      =pStep->m_nChiller_TpData;
	fileChiller.fChiller_ONTemp1     =pStep->m_fChiller_ONTemp1;
	fileChiller.nChiller_ONTemp1_Cmd =pStep->m_nChiller_ONTemp1_Cmd;
	fileChiller.nChiller_ONPump1_Cmd =pStep->m_nChiller_ONPump1_Cmd;
	fileChiller.fChiller_ONTemp2     =pStep->m_fChiller_ONTemp2;
	fileChiller.nChiller_ONTemp2_Cmd =pStep->m_nChiller_ONTemp2_Cmd;
	fileChiller.nChiller_ONPump2_Cmd =pStep->m_nChiller_ONPump2_Cmd;
	fileChiller.fChiller_OFFTemp1    =pStep->m_fChiller_OFFTemp1;
	fileChiller.nChiller_OFFTemp1_Cmd=pStep->m_nChiller_OFFTemp1_Cmd;
	fileChiller.nChiller_OFFPump1_Cmd=pStep->m_nChiller_OFFPump1_Cmd;
	fileChiller.fChiller_OFFTemp2    =pStep->m_fChiller_OFFTemp2;
	fileChiller.nChiller_OFFTemp2_Cmd=pStep->m_nChiller_OFFTemp2_Cmd;
	fileChiller.nChiller_OFFPump2_Cmd=pStep->m_nChiller_OFFPump2_Cmd;

	fwrite(&fileChiller,sizeof(FILE_CHILLER_INFO),1,pFile);
	return TRUE;
}

//ksj 20180528 : 스케쥴 파일에 패턴 저장. (울산 SDI 고속형 Cycler 발췌)
BOOL CStep::Fun_SavePatternFileToSch(FILE *pFile,int nStepNo, CString strFileName)
{
	FILE_SIMULATION_INFO filePattern;
	ZeroMemory(&filePattern,sizeof(FILE_SIMULATION_INFO));
	filePattern.iStepNo = nStepNo;
	sprintf(filePattern.szFileName, "%s", strFileName);

	//ljb 20130514 메모리 맵을 이용한 파일정보 가져 오기
	HANDLE hFile, hFileMap;
	DWORD dwFileSize;
	//DWORD dwCnt;
	char *szbuff = NULL;

	// 읽기 전용으로 lpszPathName인 파일을 읽는다.
	hFile = CreateFile((LPSTR)(LPCSTR)strFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if( hFile == INVALID_HANDLE_VALUE )
	{
		//AfxMessageBox("File Handle invalid value. Failed file open\n", MB_OK | MB_ICONERROR);
		AfxMessageBox(Fun_FindMsg("Step_Fun_SavePatternFileToSch_msg1","STEP"), MB_OK | MB_ICONERROR);//&&
		return FALSE;
	}

	filePattern.iFileSize = dwFileSize = GetFileSize(hFile, NULL);

	// dwFileSize 만큼 메모리에 쓴다.
	hFileMap = CreateFileMapping(hFile, NULL, PAGE_WRITECOPY, 0, dwFileSize, NULL);
	if( hFileMap == NULL )
	{
		//AfxMessageBox("FileMap Handle is NULL. Failed file open\n", MB_OK | MB_ICONERROR );
		AfxMessageBox(Fun_FindMsg("Step_Fun_SavePatternFileToSch_msg2","STEP"), MB_OK | MB_ICONERROR );//&&
		CloseHandle(hFile);
		return FALSE;
	}

	szbuff = (char *)MapViewOfFile(hFileMap, FILE_MAP_COPY, 0, 0, 0);

	fwrite(&filePattern,sizeof(FILE_SIMULATION_INFO),1,pFile); //lmh 201109 패턴 정보를 먼저 저장하고 파일을 세이브한다.
	fwrite(szbuff,filePattern.iFileSize,1,pFile);

	CloseHandle(hFileMap);
	CloseHandle(hFile);
	return TRUE;
}