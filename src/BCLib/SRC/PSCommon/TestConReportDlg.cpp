// TestConReportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TestConReportDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <math.h>


/////////////////////////////////////////////////////////////////////////////
// CTestConReportDlg dialog

CTestConReportDlg::CTestConReportDlg(CScheduleData* sData, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CTestConReportDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CTestConReportDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CTestConReportDlg::IDD3):
	(CTestConReportDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CTestConReportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pData = sData;
	m_nCurStepIndex = 0;
	ASSERT(m_pData);
	m_bUseHLGP = 0;

/*	char szBuff[32], szUnit[16], szDecimalPoint[16];
	m_nCurrentUnitMode = AfxGetApp()->GetProfileInt("Config", "Crt Unit Mode", 0);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWUnit = szUnit;
	m_nWDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "Wh Unit", "mWh 0"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWhUnit = szUnit;
	m_nWhDecimal = atoi(szDecimalPoint);


	sprintf(szBuff, AfxGetApp()->GetProfileString("Config", "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);
*/

}


void CTestConReportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestConReportDlg)
	DDX_Control(pDX, IDC_END_ETC_EDITBOX, m_EditEndEtc);
	DDX_Control(pDX, IDC_GRADE_LIST, m_ctrlGrade);
	DDX_Control(pDX, IDC_STEP_LIST, m_ctrlList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestConReportDlg, CDialog)
	//{{AFX_MSG_MAP(CTestConReportDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_STEP_LIST, OnItemchangedStepList)
	ON_BN_CLICKED(IDC_EXCEL_BUTTON, OnExcelButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestConReportDlg message handlers

BOOL CTestConReportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	Init();	
	
	GetDlgItem(IDC_EDIT_MODELNAME)->SetWindowText(m_pData->GetModelName());
	GetDlgItem(IDC_EDIT_MODEL_DEACRIPT)->SetWindowText(m_pData->GetModelDescript());

	GetDlgItem(IDC_EDIT_SCHNAME)->SetWindowText(m_pData->GetScheduleName());
	GetDlgItem(IDC_EDIT_AUTHOR)->SetWindowText(m_pData->GetScheduleCreator());
	GetDlgItem(IDC_EDIT_DEACRIPT)->SetWindowText(m_pData->GetScheduleDescript());
	GetDlgItem(IDC_EDIT_DATE)->SetWindowText(m_pData->GetScheduleEditTime());
	m_bUseHLGP = AfxGetApp()->GetProfileInt("Config","UseHLGP",0); //lyj 20200803 HLGP

	//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
	UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER);  //ksj 20200825
	if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
	{

	}
	else
	{
		//공통 안전 조건
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_MAX_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_MIN_MAX_STATIC)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V_YELLOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_MIN_V)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_IDC_SAFETY_CELL_DELTA_V_YELLOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_IDC_SAFETY_CELL_DELTA_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_V)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V_YELLOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_MAX_V)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC)->ShowWindow(SW_HIDE); //lyj 20200625
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC2)->ShowWindow(SW_HIDE); //lyj 20200625
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC3)->ShowWindow(SW_HIDE); //lyj 20200625
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC5)->ShowWindow(SW_HIDE); //lyj 20200625
		

		GetDlgItem(IDC_CHK_DELTA_CHAMBER)->ShowWindow(SW_HIDE);

		//스텝 안전 조건
		GetDlgItem(IDC_SAFETY_MAX_CELL_DELTA_V_STEP_YELLOW)->ShowWindow(SW_HIDE);//yulee 20190531_3
		GetDlgItem(IDC_STEP_AUXT_DELTA_YELLOW)->ShowWindow(SW_HIDE);//yulee 20190531_3
		GetDlgItem(IDC_STATIC_STEP_AUXTH_DELTA_YELLOW)->ShowWindow(SW_HIDE);//yulee 20190531_3
		GetDlgItem(IDC_STATIC_STEP_AUXT_AUXTH_DELTA_YELLOW)->ShowWindow(SW_HIDE);//yulee 20190531_3
		GetDlgItem(IDC_STATIC_STEP_AUXV_TIME_YELLOW)->ShowWindow(SW_HIDE);//yulee 20190531_3
		GetDlgItem(IDC_STATIC_STEP_AUXV_DELTA_YELLOW)->ShowWindow(SW_HIDE);//yulee 20190531_3

		GetDlgItem(IDC_CELL_DELTA_V_STEP)->ShowWindow(SW_HIDE);//yulee 20190531_3
		GetDlgItem(IDC_IDC_CELL_DELTA_V_STATIC_STATIC)->ShowWindow(SW_HIDE);//yulee 20190625
		GetDlgItem(IDC_STEP_AUXT_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_STEP_AUXT_DELTA_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_DELTA_STATIC)->ShowWindow(SW_HIDE);
	}

	BOOL bUseVentSafety = AfxGetApp()->GetProfileInt("Config", "Use Vent Safety", 1); //ksj 20200212

	if(bUseVentSafety == 0) //ksj 20200212 : vent 사용 버튼 숨김.
	{
		GetDlgItem(IDC_CHK_DELTA_CHAMBER)->ShowWindow(SW_HIDE);		
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_HIDE);	
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTestConReportDlg::Init()
{
	DWORD style = 	m_ctrlList.GetExtendedStyle();
	char szBuff[32];
	
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_ctrlList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

	m_imgList.Create(IDB_CELL_STATE_ICON_P, 19, 14,RGB(255,255,255));
	m_ctrlList.SetImageList(&m_imgList, LVSIL_SMALL);

	// Column 삽입
	m_ctrlList.InsertColumn(0, "No",  LVCFMT_RIGHT,  40,  0);
	m_ctrlList.InsertColumn(1, "Type",  LVCFMT_CENTER,  90,  1);
	sprintf(szBuff, "Mode");
	m_ctrlList.InsertColumn(2, szBuff,  LVCFMT_RIGHT,  60,  2);
	//sprintf(szBuff, "????(%s)", m_UnitTrans.GetUnitString(PS_VOLTAGE));
	sprintf(szBuff, "Charge Voltage(%s)", m_UnitTrans.GetUnitString(PS_VOLTAGE));//ENG_ysy
	m_ctrlList.InsertColumn(3, szBuff,  LVCFMT_RIGHT,  80,  3);
	//sprintf(szBuff, "????(%s)", m_UnitTrans.GetUnitString(PS_VOLTAGE));
	sprintf(szBuff, "DisCharge Voltage(%s)", m_UnitTrans.GetUnitString(PS_VOLTAGE));//ENG_ysy
	m_ctrlList.InsertColumn(4, szBuff,  LVCFMT_RIGHT,  80,  3);
	//sprintf(szBuff, "??(%s)", m_UnitTrans.GetUnitString(PS_CURRENT));
	sprintf(szBuff, "Current(%s)", m_UnitTrans.GetUnitString(PS_CURRENT));//ENG_ysy
	m_ctrlList.InsertColumn(5, szBuff,  LVCFMT_RIGHT,  60,  4);
	//sprintf(szBuff, "??(%s)", m_UnitTrans.GetUnitString(PS_WATT));
	sprintf(szBuff, "Electricity(%s)", m_UnitTrans.GetUnitString(PS_WATT));//ENG_ysy
	m_ctrlList.InsertColumn(6, szBuff,  LVCFMT_RIGHT,  60,  5);
	// 	sprintf(szBuff, "??(?)");
	// 	m_ctrlList.InsertColumn(5, szBuff,  LVCFMT_RIGHT,  60,  5); 
	//sprintf(szBuff, "챔버온도(℃)");//%1013
	sprintf(szBuff, Fun_FindMsg("TestConReportDlg_Init_msg1","IDD_TESTCON_VIEW_DLG")); //&&
	m_ctrlList.InsertColumn(7, szBuff,  LVCFMT_RIGHT,  60,  6);
	//sprintf(szBuff, "챔버습도(%)");									//yulee 20181212 추가
	sprintf(szBuff, Fun_FindMsg("TestConReportDlg_Init_msg2","IDD_TESTCON_VIEW_DLG"));							//&&
	m_ctrlList.InsertColumn(8, szBuff,  LVCFMT_RIGHT,  60,  7);
	sprintf(szBuff, "Balancing(Ω)");
	m_ctrlList.InsertColumn(9, szBuff,  LVCFMT_RIGHT,  60,  8);
	sprintf(szBuff, "Power Supply(V)");
	m_ctrlList.InsertColumn(10, szBuff,  LVCFMT_RIGHT,  60,  9);
	sprintf(szBuff, "Chiller(℃)");
	m_ctrlList.InsertColumn(11, szBuff,  LVCFMT_RIGHT,  60,  10);

	m_ctrlGrade.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);
	// Column 삽입
	m_ctrlGrade.InsertColumn(0, "No",  LVCFMT_RIGHT,  30,  0);
	//m_ctrlGrade.InsertColumn(1, "하한값",  LVCFMT_RIGHT,  80,  1);
	m_ctrlGrade.InsertColumn(1, Fun_FindMsg("TestConReportDlg_Init_msg3","IDD_TESTCON_VIEW_DLG"),  LVCFMT_RIGHT,  80,  1);//&&
	//m_ctrlGrade.InsertColumn(2, "상한값",  LVCFMT_RIGHT,  80,  2);
	m_ctrlGrade.InsertColumn(2, Fun_FindMsg("TestConReportDlg_Init_msg4","IDD_TESTCON_VIEW_DLG"),  LVCFMT_RIGHT,  80,  2);//&&
	m_ctrlGrade.InsertColumn(3, "Code",  LVCFMT_RIGHT,  40,  3);
	

	//ljb 안전조건 표시
	m_pCommSafetyData = m_pData->GetCellCheckParam();
	SetDlgItemText(IDC_SAFETY_MIN_V, ValueString(m_pCommSafetyData->fMinV, PS_VOLTAGE, TRUE));
	SetDlgItemText(IDC_SAFETY_MAX_V, ValueString(m_pCommSafetyData->fMaxV, PS_VOLTAGE, TRUE));
	SetDlgItemText(IDC_SAFETY_MAX_CELL_DELTA_V, ValueString(m_pCommSafetyData->fCellDeltaV, PS_VOLTAGE, TRUE));
	
	SetDlgItemText(IDC_SAFETY_MAX_C, ValueString(m_pCommSafetyData->fMaxC, PS_CAPACITY, TRUE));
	SetDlgItemText(IDC_SAFETY_MAX_I, ValueString(m_pCommSafetyData->fMaxI, PS_CURRENT, TRUE));
	
	SetDlgItemText(IDC_SAFETY_MIN_T, ValueString(m_pCommSafetyData->fMinT, PS_AUX_TEMPERATURE, TRUE));
	SetDlgItemText(IDC_SAFETY_MAX_T, ValueString(m_pCommSafetyData->fMaxT, PS_AUX_TEMPERATURE, TRUE));
	
	SetDlgItemText(IDC_SAFETY_MAX_W, ValueString(m_pCommSafetyData->lMaxW, PS_WATT, TRUE));
	SetDlgItemText(IDC_SAFETY_MAX_WH, ValueString(m_pCommSafetyData->lMaxWh, PS_WATT_HOUR, TRUE));

	//INT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
	INT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
	if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
	{
		SetDlgItemText(IDC_SAFETY_CELL_DELTA_STD_MIN_V, ValueString(m_pCommSafetyData->fSTDCellDeltaMinV, PS_VOLTAGE, TRUE));
		SetDlgItemText(IDC_SAFETY_CELL_DELTA_MIN_V, ValueString(m_pCommSafetyData->fCellDeltaMinV, PS_VOLTAGE, TRUE));
		SetDlgItemText(IDC_SAFETY_CELL_DELTA_V, ValueString(m_pCommSafetyData->fCellDeltaV, PS_VOLTAGE, TRUE));
		SetDlgItemText(IDC_SAFETY_CELL_DELTA_STD_MAX_V, ValueString(m_pCommSafetyData->fSTDCellDeltaMaxV, PS_VOLTAGE, TRUE));
		SetDlgItemText(IDC_SAFETY_CELL_DELTA_MAX_V, ValueString(m_pCommSafetyData->fCellDeltaMaxV, PS_VOLTAGE, TRUE));
		((CButton*)GetDlgItem(IDC_CHK_DELTA_CHAMBER))->SetCheck((BOOL)m_pCommSafetyData->bfaultcompAuxV_Vent_Flag);
	}

	//IDC_EDIT_SCHNAME
	CString strMode;
	CStep *pStep;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	for(int step =0; step < (int)m_pData->GetStepSize(); step++)
	{
		lvItem.iItem = step;
		lvItem.iSubItem = 0;
		lvItem.iImage = -1;
		sprintf(szBuff,"%d", step+1);
		lvItem.pszText = szBuff; 
		
		//20080821 Added by KBH
		//현재 Step 표시
		if(m_nCurStepIndex == step)
		{
			lvItem.iImage = 9;
		}
		m_ctrlList.InsertItem(&lvItem);
		m_ctrlList.SetItemData(lvItem.iItem, step);
		
		pStep = m_pData->GetStepData(step);
		ASSERT(pStep);
		
		lvItem.iSubItem = 1;
		if(pStep->m_type == PS_STEP_CHARGE)				lvItem.iImage = 2;
		else if(pStep->m_type == PS_STEP_DISCHARGE)		lvItem.iImage = 1;
		else if(pStep->m_type == PS_STEP_REST)			lvItem.iImage = 11;
		else if(pStep->m_type == PS_STEP_OCV)			lvItem.iImage = 7;
		else if(pStep->m_type == PS_STEP_IMPEDANCE)		lvItem.iImage = 10;
		else if(pStep->m_type == PS_STEP_END)			lvItem.iImage = 4;
		else if(pStep->m_type == PS_STEP_PATTERN)		lvItem.iImage = 12;
		else if(pStep->m_type == PS_STEP_USER_MAP)		lvItem.iImage = 12;
		else lvItem.iImage = 8;

		//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
		UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
		if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
		{
			//SetDlgItemText(IDC_SAFETY_MAX_CELL_DELTA_V_STEP, ValueString(pStep->m_fCellDeltaVStep, PS_VOLTAGE, TRUE)); //yulee 20190531_6
			SetDlgItemText(IDC_CELL_DELTA_V_STEP, ValueString(pStep->m_fCellDeltaVStep, PS_VOLTAGE, TRUE)); //lyj 20200625
		}
		else
		{
			//SetDlgItemText(IDC_CELL_DELTA_V_STEP, ValueString(pStep->m_fCellDeltaVStep, PS_VOLTAGE, TRUE)); //lyj 20200625
			GetDlgItem(IDC_CELL_DELTA_V_STEP)->ShowWindow(SW_HIDE); //lyj 20200625
			//GetDlgItem(IDC_SAFETY_MAX_CELL_DELTA_V_STEP)->ShowWindow(SW_HIDE);
 		}		

		sprintf(szBuff,"%s", 	m_pData->GetTypeString(pStep->m_type));
		lvItem.pszText = szBuff; 
		m_ctrlList.SetItem(&lvItem);
		
		if( pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE || 
			(pStep->m_type == PS_STEP_IMPEDANCE && pStep->m_mode == PS_MODE_DCIMP))
		{
			m_ctrlList.SetItemText(lvItem.iItem, 3, ValueString(pStep->m_fVref_Charge, PS_VOLTAGE));
			m_ctrlList.SetItemText(lvItem.iItem, 4, ValueString(pStep->m_fVref_DisCharge, PS_VOLTAGE));

			if (pStep->m_mode == PS_MODE_CP)
			{
				m_ctrlList.SetItemText(lvItem.iItem, 6, ValueString(pStep->m_fPref, PS_WATT));
			}
// 			else if (pStep->m_mode == PS_MODE_CR)
// 			{
// 				m_ctrlList.SetItemText(lvItem.iItem, 5, ValueString(pStep->m_fRref, PS_WATT));
// 			}
			else
			{
				m_ctrlList.SetItemText(lvItem.iItem, 5, ValueString(pStep->m_fIref, PS_CURRENT));
			}

			strMode.Empty();
			if (pStep->m_mode == PS_MODE_CCCV) strMode = "CC/CV";
			else if (pStep->m_mode == PS_MODE_CC) strMode = "CC";
			else if (pStep->m_mode == PS_MODE_CP) strMode = "CP";
			else if (pStep->m_mode == PS_MODE_CCCP) strMode = "CC/CP";
			else if (pStep->m_mode == PS_MODE_CR) strMode = "CR";
			//모드표시
			m_ctrlList.SetItemText(lvItem.iItem, 2, strMode);
			//챔버 온도 표시
			m_ctrlList.SetItemText(lvItem.iItem, 7, ValueString(pStep->m_fTref, PS_OVEN_TEMPERATURE));
			if(pStep->m_fHref != 0.0)  //yulee 20181212 추가 
				m_ctrlList.SetItemText(lvItem.iItem, 8, ValueString(pStep->m_fHref, PS_OVEN_HUMIDITY)); //습도 표시
			else
			{
				CString strTmpValue;
				strTmpValue.Format("-");
				m_ctrlList.SetItemText(lvItem.iItem, 8, strTmpValue); //습도 표시
			}
			m_ctrlList.SetItemText(lvItem.iItem, 9, ValueString(pStep->m_nCellBal_CircuitRes, PS_CELLBAL_RES)); //Cell Balancing 표시
			m_ctrlList.SetItemText(lvItem.iItem, 10, ValueString(pStep->m_nPwrSupply_Volt, PS_VOLTAGE)); //Power Supply 표시
			m_ctrlList.SetItemText(lvItem.iItem, 11, ValueString(pStep->m_fChiller_RefTemp, PS_CHILLER_REF_TEMP)); //Chiller 표시
		}
		//if( pStep->m_type == PS_STEP_REST )
		if( pStep->m_type == PS_STEP_REST ||
			pStep->m_type == PS_STEP_PATTERN //ksj 20210823 : 패턴 스텝에서도 조건 표시 추가.
			)


		{
			//챔버 온도 표시
			m_ctrlList.SetItemText(lvItem.iItem, 7, ValueString(pStep->m_fTref, PS_OVEN_TEMPERATURE));
			if(pStep->m_fHref != 0.0)
				m_ctrlList.SetItemText(lvItem.iItem, 8, ValueString(pStep->m_fHref, PS_OVEN_HUMIDITY)); //습도 표시 
			else
			{
				CString strTmpValue;
				strTmpValue.Format("-");
				m_ctrlList.SetItemText(lvItem.iItem, 8, strTmpValue); //습도 표시
			}
			m_ctrlList.SetItemText(lvItem.iItem, 9, ValueString(pStep->m_nCellBal_CircuitRes, PS_CELLBAL_RES)); //Cell Balancing 표시
			//m_ctrlList.SetItemText(lvItem.iItem, 10, ValueString(pStep->m_nPwrSupply_Volt, PS_VOLTAGE)); //Power Supply 표시
			
			//lyj 20200806 s 파워서플라이 화면 출력 개선 ======================
			if(pStep->m_nPwrSupply_Cmd == 2) 
			{
				m_ctrlList.SetItemText(lvItem.iItem, 10, "OFF"); //Power Supply 표시
			}
			else
			{
				m_ctrlList.SetItemText(lvItem.iItem, 10, ValueString(pStep->m_nPwrSupply_Volt, PS_VOLTAGE)); //Power Supply 표시
			}
			//lyj 20200806 e ==================================================

			m_ctrlList.SetItemText(lvItem.iItem, 11, ValueString(pStep->m_fChiller_RefTemp, PS_CHILLER_REF_TEMP)); //Chiller 표시
		}

	}

	if(m_pData->GetStepSize() > 0)
	{
		m_ctrlList.SetItemState(m_nCurStepIndex, LVIS_SELECTED, LVIS_SELECTED);
		SetStepData(m_nCurStepIndex); 
	}
}


void CTestConReportDlg::SetStepData(int nIndex)
{
	CStep *pStep;

	pStep = m_pData->GetStepData(nIndex);
	if(pStep == NULL)	return;

	CString strCmd,strWaitTime,strLoader,strTemp;
	strWaitTime.Empty();

	//strCmd.Format("Step %d 적용 조건", nIndex+1);
	strCmd.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg1","IDD_TESTCON_VIEW_DLG"), nIndex+1);//&&
	SetDlgItemText(IDC_STEP_STATIC, strCmd);
	
	//safty condition
	SetDlgItemText(IDC_EDIT_SAFEVTGMIN, ValueString(pStep->m_fLowLimitV, PS_VOLTAGE, TRUE));
	SetDlgItemText(IDC_EDIT_SAFEVTGMAX, ValueString(pStep->m_fHighLimitV, PS_VOLTAGE, TRUE));

	SetDlgItemText(IDC_EDIT_SAFECRTMIN, ValueString(pStep->m_fLowLimitI, PS_CURRENT, TRUE));
	SetDlgItemText(IDC_EDIT_SAFECRTMAX, ValueString(pStep->m_fHighLimitI, PS_CURRENT, TRUE));

	SetDlgItemText(IDC_EDIT_SAFECAPMIN, ValueString(pStep->m_fLowLimitC, PS_CAPACITY, TRUE));
	SetDlgItemText(IDC_EDIT_SAFECAPMAX, ValueString(pStep->m_fHighLimitC, PS_CAPACITY, TRUE));

	SetDlgItemText(IDC_EDIT_SAFEIMPMIN, ValueString(pStep->m_fHighLimitImp, PS_IMPEDANCE, TRUE));
	SetDlgItemText(IDC_EDIT_SAFEIMPMAX, ValueString(pStep->m_fLowLimitImp, PS_IMPEDANCE, TRUE));

	SetDlgItemText(IDC_EDIT_SAFEWATTMIN, ValueString(pStep->m_fHighLimitTemp, PS_AUX_TEMPERATURE, TRUE));
	SetDlgItemText(IDC_EDIT_SAFEWATTMAX, ValueString(pStep->m_fLowLimitTemp, PS_AUX_TEMPERATURE, TRUE));
	
	//Report Condition
//	strCmd.Format("%3.1f", pStep->m_fReportV);
	SetDlgItemText(IDC_EDIT_C_SAVEV, ValueString(pStep->m_fReportV, PS_VOLTAGE, TRUE));
	SetDlgItemText(IDC_EDIT_C_SAVEI, ValueString(pStep->m_fReportI, PS_CURRENT, TRUE));
	//SetDlgItemText(IDC_EDIT_C_SAVET, ValueString(pStep->m_fReportTime, PS_STEP_TIME, TRUE));
	SetDlgItemText(IDC_EDIT_C_SAVET, ValueString(pStep->m_fReportTime, PS_STEP_TIME, TRUE));	// BW KIM 20140107 -> ljb 20160428 edit ,pStep->m_lEndTimeDay 삭제
	SetDlgItemText(IDC_EDIT_T_SAVET, ValueString(pStep->m_fReportTemp, PS_OVEN_TEMPERATURE, TRUE));					//ljb 20100726
	
	//End Condition
//	strCmd.Format("%3.1f", pStep->m_fEndV);
	SetDlgItemText(IDC_EDIT_C_ENDV_H, ValueString(pStep->m_fEndV_H, PS_VOLTAGE, TRUE));		//ljb v1009
	SetDlgItemText(IDC_EDIT_C_ENDV_L, ValueString(pStep->m_fEndV_L, PS_VOLTAGE, TRUE));		//ljb v1009
	if (pStep->m_nGotoStepEndV_H == 0)
		strTemp = "";
	else if (pStep->m_nGotoStepEndV_H == PS_STEP_NEXT)
		strTemp = "NEXT";
	else
		strTemp.Format("%d",pStep->m_nGotoStepEndV_H);
	SetDlgItemText(IDC_EDIT_C_ENDV_H_GOTO, strTemp);		//ljb v1009
	if (pStep->m_nGotoStepEndV_L == 0)
		strTemp = "";
	else if (pStep->m_nGotoStepEndV_L == PS_STEP_NEXT)
		strTemp = "NEXT";
	else
		strTemp.Format("%d",pStep->m_nGotoStepEndV_L);
	SetDlgItemText(IDC_EDIT_C_ENDV_L_GOTO, strTemp);		//ljb v1009
	SetDlgItemText(IDC_EDIT_C_ENDI, ValueString(pStep->m_fEndI, PS_CURRENT, TRUE));
	strCmd.Format("%3.1f", pStep->m_fEndC);
	SetDlgItemText(IDC_EDIT_C_ENDCAP, ValueString(pStep->m_fEndC, PS_CAPACITY, TRUE));
	if (pStep->m_nGotoStepEndC == 0)
		strTemp = "";
	else if (pStep->m_nGotoStepEndC == PS_STEP_NEXT)
		strTemp = "NEXT";
	else
		strTemp.Format("%d",pStep->m_nGotoStepEndC);
	SetDlgItemText(IDC_EDIT_C_ENDCAP_GOTO, strTemp);	//ljb v1009
/*	CTimeSpan endT( pStep->m_ulEndTime/100);
	if(endT.GetDays() > 0)
	{
		strCmd.Format("%s.%02d", endT.Format("%dD %H:%M:%S"), pStep->m_ulEndTime%100);
	}
	else
	{
		strCmd.Format("%s.%02d", endT.Format("%H:%M:%S"), pStep->m_ulEndTime%100);
	}
*/	//SetDlgItemText(IDC_EDIT_C_ENDTIME, ValueString(pStep->m_fEndTime, PS_STEP_TIME, TRUE));
	SetDlgItemText(IDC_EDIT_C_ENDTIME, ValueString(pStep->m_fEndTime, PS_STEP_TIME, TRUE,pStep->m_lEndTimeDay)); // BW KIM 2014.01.07
	if (pStep->m_nGotoStepEndTime == 0)
		strTemp = "";
	else if (pStep->m_nGotoStepEndTime == PS_STEP_NEXT)
		strTemp = "NEXT";
	else
		strTemp.Format("%d",pStep->m_nGotoStepEndTime);
	SetDlgItemText(IDC_EDIT_C_ENDTIME_GOTO, strTemp);	//ljb v1009

	strCmd.Format("%.3f", pStep->m_fEndDV);
	SetDlgItemText(IDC_EDIT_C_ENDDELTAV, ValueString(pStep->m_fEndDV, PS_VOLTAGE, TRUE));
	strCmd.Format("%.3f", pStep->m_fEndW);
	SetDlgItemText(IDC_EDIT_C_ENDWATT, ValueString(pStep->m_fEndW, PS_WATT, TRUE));
	
	strCmd.Format("%.3f", pStep->m_fEndWh);
	SetDlgItemText(IDC_EDIT_C_ENDWATTHOUR, ValueString(pStep->m_fEndWh, PS_WATT_HOUR, TRUE));
	if (pStep->m_nGotoStepEndWh == 0)
		strTemp = "";
	else if (pStep->m_nGotoStepEndWh == PS_STEP_NEXT)
		strTemp = "NEXT";
	else
		strTemp.Format("%d",pStep->m_nGotoStepEndWh);
	SetDlgItemText(IDC_EDIT_C_ENDWATTHOUR_GOTO, strTemp);	//ljb v1009

	//ljb v1009 20090326 ////////////////////////////////////////////////////////////////////////
// 	switch (pStep->m_bValueItem)
// 	{
// 	case 0 :
// 		  strCmd="사용안함";
// 		  break;
// 	case 1:
// 		  strCmd.Format("SOC 기준값으로 사용");
// 		  break;
// 	case 2:
// 		  strCmd.Format("WattHour 기준값으로 사용");
// 		  break;
// 	}
	
	//20170519 YULEE 종료조건 기타 출력의 시작
	if(pStep->m_bValueStepNo > 0 && pStep->m_fValueRate > 0)
	{
		if(pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE)
		{
			if (pStep->m_bValueItem == 1) //ah
			{
				//strCmd.Format("Step %d 의 %.1f%% (Ah)", pStep->m_bValueStepNo, pStep->m_fValueRate);
				strCmd.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg12","IDD_TESTCON_VIEW_DLG"), pStep->m_bValueStepNo, pStep->m_fValueRate);//&&
			}
			else
			{
				//strCmd.Format("Step %d 의 %.1f%% (Wh)", pStep->m_bValueStepNo, pStep->m_fValueRate);
				strCmd.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg13","IDD_TESTCON_VIEW_DLG"), pStep->m_bValueStepNo, pStep->m_fValueRate);//&&
			}
			
		}
// 		else if(pStep->m_type == PS_STEP_DISCHARGE)
// 		{
// 			strCmd.Format("Step %d 의 %.1f%%", pStep->m_bValueStepNo, pStep->m_fValueRate);
// 		}
		else
			strCmd.Empty();
	}
	//////////////////////////////////////////////////////////////////////////
	else if(pStep->m_nLoopInfoCycle > 0 && pStep->m_nLoopInfoGotoStep >= 0)
	{
		if(pStep->m_nLoopInfoGotoStep <= 0 || pStep->m_nLoopInfoGotoStep == PS_STEP_NEXT)
		{
			//strCmd.Format("반복 %d회 후 Next로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);
			//strCmd.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg2","IDD_TESTCON_VIEW_DLG"), pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);//&&
			strCmd.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg2","IDD_TESTCON_VIEW_DLG"), pStep->m_nLoopInfoCycle);//&&
		}
		else
		{
			//strCmd.Format("반복 %d회 후 %d로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);
			strCmd.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg3","IDD_TESTCON_VIEW_DLG"), pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);//&&
		}
	}
	else 
		strCmd.Empty();


	//ksj 20201012 : 누적 사이클 정보 추가
	if(pStep->m_nAccLoopGroupID > 0 && pStep->m_nAccLoopInfoGotoStep >= 0)
	{
		if(pStep->m_nAccLoopInfoGotoStep <= 0 || pStep->m_nAccLoopInfoGotoStep == PS_STEP_NEXT)
		{
			//strCmd.Format("누적 사이클 반복 %d회 후 Next로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);
			strTemp.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg14","IDD_TESTCON_VIEW_DLG"), pStep->m_nAccLoopGroupID, pStep->m_nAccLoopInfoCycle);//&&
		}
		else
		{
			//strCmd.Format("누적 사이클 반복 %d회 후 %d로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);
			strTemp.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg15","IDD_TESTCON_VIEW_DLG"), pStep->m_nAccLoopGroupID, pStep->m_nAccLoopInfoCycle, pStep->m_nAccLoopInfoGotoStep);//&&
		}
		strCmd += _T(",");
		strCmd += strTemp;
	}
	//ksj end

	
	//ljb v1009 20090326 ////////////////////////////////////////////////////////////////////////
	if (pStep->m_nWaitTimeInit >0)
	{
		//strWaitTime = "시간 초기화";
		strWaitTime = Fun_FindMsg("TestConReportDlg_SetStepData_msg4","IDD_TESTCON_VIEW_DLG");//&&
	}
	if (pStep->m_nWaitTimeDay >0 || pStep->m_nWaitTimeHour >0 || pStep->m_nWaitTimeMin >0 || pStep->m_nWaitTimeSec >0)
	{
		//strWaitTime.Format(" D%d %02d:%02d:%02d 시간 후 NEXT",pStep->m_nWaitTimeDay,pStep->m_nWaitTimeHour,pStep->m_nWaitTimeMin,pStep->m_nWaitTimeSec);
		strWaitTime.Format(Fun_FindMsg("TestConReportDlg_SetStepData_msg5","IDD_TESTCON_VIEW_DLG"),pStep->m_nWaitTimeDay,pStep->m_nWaitTimeHour,pStep->m_nWaitTimeMin,pStep->m_nWaitTimeSec);//&&
	}

	strCmd = strWaitTime + strCmd;

	
	//yulee 20180910 챔버 동작에 관한 내용 출력의 시작
	if(pStep->m_bStepChamberStop > 0)
	{
		CString tmpStr;
		tmpStr.Format("Chamber Step Stop\r\n");
		strCmd += tmpStr;
	}
	//yulee 20180910 챔버 동작에 관한 내용 출력의 끝 


	//20170519 YULEE 종료조건 옵션 처리 추가의 사작 
	CString StrEndCondOption = _T("");


	//Pattern MAX and MIN 처리
	/*
	PS_STEP_END				0x06
	PS_STEP_ADV_CYCLE		0x07
	PS_STEP_LOOP			0x08
	위 스텝에서는 표시 안함 if((pStep->m_fValueMax =! 0) && ((nStepMtype < 6) || (nStepMtype > 8)))
	*/
	int nStepMtype = pStep->m_type;
	if((pStep->m_fValueMax =! 0) && ((nStepMtype < 6) || (nStepMtype > 8)))
	{
		StrEndCondOption.Format(_T("\nPattern MAX: %.2f"), pStep->m_fValueMax);
		strCmd = strCmd + StrEndCondOption;
		StrEndCondOption = _T(""); 
	}
	if((pStep->m_fValueMin =! 0) && ((nStepMtype < 6) || (nStepMtype > 8)))
	{
		StrEndCondOption.Format(_T("Pattern MIN: %.2f"), pStep->m_fValueMin);
		strCmd = strCmd + _T(", ");
		strCmd = strCmd + StrEndCondOption;
		StrEndCondOption = _T("");
	}

	
	//CAN 통신 모드의 처리
	
	//m_nCanCheckMode CBOBOX 
	//1. CAN 통신체크 Master + Slave
	//2. CAN 통신체크 해제
	//3. CAN 통신체크 Master
	//4. CAN 통신체크 Slave
	/*
	PS_STEP_END				0x06
	PS_STEP_ADV_CYCLE		0x07
	PS_STEP_LOOP			0x08
	위 스텝에서는 표시 안함 if((pStep->m_fValueMax =! 0) && ((nStepMtype < 6) || (nStepMtype > 8)))
	*/
	if((nStepMtype < 6) || (nStepMtype > 8))
	{
		switch(pStep->m_nCanCheckMode)
		{
		case 0:
			{
				//StrEndCondOption = _T("\nCAN MODE : CAN 통신체크 Master + Slave");
				StrEndCondOption = Fun_FindMsg("TestConReportDlg_SetStepData_msg6","IDD_TESTCON_VIEW_DLG");//&&
				strCmd = strCmd + _T(", ");
				strCmd = strCmd + StrEndCondOption;
				break;
			}
		case 1:
			{
				//StrEndCondOption = _T("\nCAN MODE : CAN 통신체크 해제");
				StrEndCondOption = Fun_FindMsg("TestConReportDlg_SetStepData_msg7","IDD_TESTCON_VIEW_DLG");//&&
				strCmd = strCmd + _T(", ");
				strCmd = strCmd + StrEndCondOption;
				break;
			}
		case 2:
			{
				//StrEndCondOption = _T("\nCAN MODE : CAN 통신체크 Master");
				StrEndCondOption = Fun_FindMsg("TestConReportDlg_SetStepData_msg8","IDD_TESTCON_VIEW_DLG");//&&
				strCmd = strCmd + _T(", ");
				strCmd = strCmd + StrEndCondOption;
				break;
			}
		case 3:
			{
				//StrEndCondOption = _T("\nCAN MODE : CAN 통신체크 Slave");
				StrEndCondOption = Fun_FindMsg("TestConReportDlg_SetStepData_msg9","IDD_TESTCON_VIEW_DLG");//&&
				strCmd = strCmd + _T(", ");
				strCmd = strCmd + StrEndCondOption;
				break;
			}
		default:
			{	
				break;
			}
		}
	}

	//CAN 송신 OFF
	if(pStep->m_nCanTxOffMode)
	{
		//StrEndCondOption = _T("\nCAN 송신 OFF");
		StrEndCondOption = Fun_FindMsg("TestConReportDlg_SetStepData_msg10","IDD_TESTCON_VIEW_DLG");//&&
		strCmd = strCmd + _T(", ");
		strCmd = strCmd + StrEndCondOption;
	}
	
	//CAN 데이터 조건 무시
	if(pStep->m_nNoCheckMode)
	{
		//StrEndCondOption = _T("\nCAN 데이터 조건 무시");
		StrEndCondOption = Fun_FindMsg("TestConReportDlg_SetStepData_msg11","IDD_TESTCON_VIEW_DLG");//&&
		strCmd = strCmd + _T(", ");
		strCmd = strCmd + StrEndCondOption;
	}

	//YULEE 종료조건 옵션 처리 추가의 끝 

	//lyj 20200803 s [18PSCSA012] HL그린파워 1000V 600A 2CH 600kW_PACKcycler 대응Loop 조건 ----------------
	if(pStep->m_type == PS_STEP_LOOP && m_bUseHLGP)
	{
		GetDlgItem(IDC_STATIC_VTG_H)->SetWindowText("Ah_Upper");
		GetDlgItem(IDC_STATIC_VTG_L)->SetWindowText("Ah_Lower");
		SetDlgItemText(IDC_EDIT_C_ENDV_H, ValueString(pStep->m_fEndV_H, PS_CAPACITY, TRUE));
		SetDlgItemText(IDC_EDIT_C_ENDV_L, ValueString(pStep->m_fEndV_L, PS_CAPACITY, TRUE));
		GetDlgItem(IDC_STATIC_CAP)->SetWindowText("Wh_Upper");
		GetDlgItem(IDC_STATIC_WH)->SetWindowText("Wh_Lower");
		SetDlgItemText(IDC_EDIT_C_ENDCAP, ValueString(pStep->m_fEndV_H, PS_WATT_HOUR, TRUE));
		SetDlgItemText(IDC_EDIT_C_ENDWATTHOUR, ValueString(pStep->m_fEndV_L, PS_WATT_HOUR, TRUE));

		if (pStep->m_fEndV_H != 0)
		{
			StrEndCondOption.Format("\n Cycle Ah >= %s Goto %d",ValueString(pStep->m_fEndV_H, PS_CAPACITY, TRUE),pStep->m_nGotoStepEndV_H);
			strCmd = strCmd + _T(", ");
			strCmd = strCmd + StrEndCondOption;
		}
		if (pStep->m_fEndV_L != 0)
		{
			StrEndCondOption.Format("\nCycle Ah <= %s Goto %d",ValueString(pStep->m_fEndV_L, PS_CAPACITY, TRUE),pStep->m_nGotoStepEndV_L);
			strCmd = strCmd + _T(", ");
			strCmd = strCmd + StrEndCondOption;
		}
		if (pStep->m_fEndC != 0)
		{
			StrEndCondOption.Format("\nCycle Wh >= %s Goto %d",ValueString(pStep->m_fEndC, PS_WATT_HOUR, TRUE),pStep->m_nGotoStepEndC);
			strCmd = strCmd + _T(", ");
			strCmd = strCmd + StrEndCondOption;
		}
		if (pStep->m_fEndWh != 0)
		{
			StrEndCondOption.Format("\nCycle Wh >= %s Goto %d",ValueString(pStep->m_fEndWh, PS_WATT_HOUR, TRUE),pStep->m_nGotoStepEndWh);
			strCmd = strCmd + _T(", ");
			strCmd = strCmd + StrEndCondOption;
		}
	}
	else
	{
		GetDlgItem(IDC_STATIC_VTG_H)->SetWindowText("Voltage H");
		GetDlgItem(IDC_STATIC_VTG_L)->SetWindowText("Voltage L");
		GetDlgItem(IDC_STATIC_CAP)->SetWindowText("Capacity");
		GetDlgItem(IDC_STATIC_WH)->SetWindowText("WattHour");
	}
	//lyj 20200803 e ----------------------------------------------------------------------------------------------------------------

	//yulee 20180803 확장 종료조건 추가의 시작
	//코드 + 수학기호 + 설정 값 + 다음 스텝 

	bool bAuxCanExtEndCond = FALSE;

	for(int i=0; i<10; i++)
	{
		if(pStep->m_iaux_function_division[0] != NULL)
			bAuxCanExtEndCond = TRUE;
	}

	for(int i=0; i<10; i++)
	{
		if(pStep->m_ican_function_division[0] != NULL)
			bAuxCanExtEndCond = TRUE;
	}

	strCmd = strCmd + "\r\n";

	if(bAuxCanExtEndCond)
		strCmd = strCmd + _T("[Extend End Cond.]");

	strCmd = strCmd + "\r\n";

	CString strTmp="";
	for(int i=0; i<10; i++)
	{
		if(pStep->m_ican_function_division[i] != 0)
		{
			strTmp.Format("%d.%d:%s%.3fStep:%s",
				i+1,
				pStep->m_ican_function_division[i],
				ExtEndConCompareTypeConvertor(pStep->m_ican_compare_type[i]),	//compare type
				pStep->m_fcan_Value[i],											//value
				GetExtEndCondNext(pStep->m_ican_branch[i]));					//branch
			strTmp = strTmp + "\r\n";
			if(strTmp.IsEmpty() == FALSE && strTmp != "")
			{
				strCmd = strCmd + strTmp;
				strTmp="";
			}
		}
		else 
			continue;
	}

	strTmp="";
	for(int i=0; i<10; i++)
	{
		if(pStep->m_iaux_function_division[i] != 0)
		{
			strTmp.Format("%d.%d:%s%.3fStep:%s,%d",
			i+1,
			pStep->m_iaux_function_division[i],
			ExtEndConCompareTypeConvertor(pStep->m_iaux_compare_type[i]),	//compare type
			pStep->m_faux_Value[i],											//value
			GetExtEndCondNext(pStep->m_iaux_branch[i]),
			pStep->m_iaux_conti_time[i]);										//yulee 20180905
			strTmp = strTmp + "\r\n";

			TRACE("No %d", i+1);
			TRACE("Division %d", pStep->m_iaux_function_division[i]);
			TRACE("CompareType %s", ExtEndConCompareTypeConvertor(pStep->m_iaux_compare_type[i]));
			TRACE("Value %f", pStep->m_faux_Value[i]);
			TRACE("Next %s", GetExtEndCondNext(pStep->m_iaux_branch[i]));
			if(strTmp.IsEmpty() == FALSE && strTmp != "")
			{
				strCmd = strCmd + strTmp;
				strTmp="";
			}
		}
		else 
			continue;
	}
	
	//yulee 20180803 확장 종료조건 추가의 끝 
	//SetDlgItemText(IDC_END_ETC_STATIC, strCmd);
	SetDlgItemText(IDC_END_ETC_EDITBOX, strCmd); //yulee 20180803
	//20170519 YULEE 종료조건 기타 출력의 끝 



	//ljb 20150604 add
// 	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
// 	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
// 	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	if(pStep->nValueLoaderItem == 0)	strLoader = "LOAD 사용안함";
	else if(pStep->nValueLoaderItem == 1)
	{
		strLoader = "LOAD ON";

		if(pStep->nValueLoaderMode == 0)		strTemp = "  MODE : CP   ";
		else if(pStep->nValueLoaderMode == 1)	strTemp = "  MODE : CC   ";
		else if(pStep->nValueLoaderMode == 2)	strTemp = "  MODE : CV   ";
		else if(pStep->nValueLoaderMode == 3)	strTemp = "  MODE : CR   ";
		else strTemp ="";
		strLoader += strTemp;

		if(pStep->nValueLoaderMode == 0) strTemp.Format("%.3f W",pStep->fValueLoaderSet);
		else if(pStep->nValueLoaderMode == 1) strTemp.Format("%.3f A",pStep->fValueLoaderSet);
		else if(pStep->nValueLoaderMode == 2) strTemp.Format("%.3f V",pStep->fValueLoaderSet);
		else if(pStep->nValueLoaderMode == 3) strTemp.Format("%.3f ohm",pStep->fValueLoaderSet);
		else strTemp ="";
		strLoader += strTemp;
	}
	else if(pStep->nValueLoaderItem == 2)	strLoader = "LOAD OFF";
	else strLoader = "";

	if (pStep->m_type == PS_STEP_ADV_CYCLE || pStep->m_type == PS_STEP_LOOP || pStep->m_type == PS_STEP_END) strLoader = "";
	SetDlgItemText(IDC_END_LOAD_STATIC, strLoader);

	//Grading condition
	m_ctrlGrade.DeleteAllItems();

	char szBuff[32];
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;//|LVIF_IMAGE;
	GRADE_STEP gStep;
	for(int step =0; step < pStep->m_Grading.GetGradeStepSize(); step++)
	{
		lvItem.iItem = step;
		lvItem.iSubItem = 0;
		sprintf(szBuff,"%d", step+1);
		lvItem.pszText = szBuff; 
		m_ctrlGrade.InsertItem(&lvItem);
		
		gStep = pStep->m_Grading.GetStepData(step);	
		if(/*pStep->m_Grading.m_lGradingItem*/ gStep.lGradeItem == PS_GRADE_CAPACITY)		//EDLC 용량은 F단위로 표시한다.
		{
			//			sprintf(szBuff,"%.1f", gStep.fMin);
			m_ctrlGrade.SetItemText(lvItem.iItem, 1, ValueString(gStep.fMin, PS_CAPACITY));
			//			sprintf(szBuff,"%.1f", gStep.fMax);
			m_ctrlGrade.SetItemText(lvItem.iItem, 2, ValueString(gStep.fMax, PS_CAPACITY));
		}
		else if(gStep.lGradeItem == PS_GRADE_IMPEDANCE)
		{
			//			sprintf(szBuff,"%.1f", gStep.fMin/1000.0);
			m_ctrlGrade.SetItemText(lvItem.iItem, 1, ValueString(gStep.fMin, PS_IMPEDANCE));
			//			sprintf(szBuff,"%.1f", gStep.fMax/1000.0);
			m_ctrlGrade.SetItemText(lvItem.iItem, 2, ValueString(gStep.fMax, PS_IMPEDANCE));
		}
		else
		{
			//			sprintf(szBuff,"%.1f", gStep.fMin);
			m_ctrlGrade.SetItemText(lvItem.iItem, 1, ValueString(gStep.fMin, PS_VOLTAGE));
			//			sprintf(szBuff,"%.1f", gStep.fMax);
			m_ctrlGrade.SetItemText(lvItem.iItem, 2, ValueString(gStep.fMax, PS_VOLTAGE));
		}
		
		if(!gStep.strCode.IsEmpty())
		{
			sprintf(szBuff,"%c", gStep.strCode[0]);	
		}
		else
		{
			sprintf(szBuff,"");	
		}
		m_ctrlGrade.SetItemText(lvItem.iItem, 3, szBuff);
	}

	//cny 201809
	GetDlgItem(IDC_EDIT_TV_PWRSUPPLY_CMD)->SetWindowText(_T(""));
	//GetDlgItem(IDC_EDIT_TV_PWRSUPPLY_VOLT)->SetWindowText(_T(""));
	
	if(pStep->m_nPwrSupply_Cmd==1)
	{
		GetDlgItem(IDC_EDIT_TV_PWRSUPPLY_CMD)->SetWindowText(_T("ON"));
		
		//strTemp.Format(_T("%.2f"),(float)pStep->m_nPwrSupply_Volt/1000.f);
		//GetDlgItem(IDC_EDIT_TV_PWRSUPPLY_VOLT)->SetWindowText(strTemp);
	}
	else if(pStep->m_nPwrSupply_Cmd==2)
	{
		GetDlgItem(IDC_EDIT_TV_PWRSUPPLY_CMD)->SetWindowText(_T("OFF"));
	}
	else 
	{
		GetDlgItem(IDC_EDIT_TV_PWRSUPPLY_CMD)->SetWindowText(_T("NONE"));
	}

	//cny 201809
	GetDlgItem(IDC_EDIT_TV_CHILLER_CMD)->SetWindowText(_T(""));
	if(pStep->m_nChiller_Cmd==1)
	{
		GetDlgItem(IDC_EDIT_TV_CHILLER_CMD)->SetWindowText(_T("ON"));
	}

	//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
	UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER);  //ksj 20200825
	if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
	{
		if (pStep->m_type == PS_STEP_LOOP || pStep->m_type == PS_STEP_ADV_CYCLE || pStep->m_type == PS_STEP_END)
		{
			SetDlgItemText(IDC_CELL_DELTA_V_STEP, "0.000 V");
			SetDlgItemText(IDC_STEP_AUXT_DELTA, "0.000 'C");
			SetDlgItemText(IDC_STEP_AUXTH_DELTA, "0.000 'C");
			SetDlgItemText(IDC_STEP_AUXT_AUXTH_DELTA, "0.000 'C");
			CString strOut;
			strOut.Format("%.2lf s", 0.0);
			SetDlgItemText(IDC_STEP_AUXV_TIME, strOut);
			SetDlgItemText(IDC_STEP_AUXV_DELTA, "0.000 V");

			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V))->SetCheck(FALSE);
			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP))->SetCheck(FALSE);
			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH))->SetCheck(FALSE);
			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T))->SetCheck(FALSE);
			((CButton*)GetDlgItem(IDC_CHK_STEP_AUX_V))->SetCheck(FALSE);
		}
		else
		{
			//20191206 lyj AuxT 편차, AuxTH 편차, AuxT + AuxTH 편차 단위 조정
			SetDlgItemText(IDC_CELL_DELTA_V_STEP, ValueString(pStep->m_fCellDeltaVStep, PS_VOLTAGE, TRUE));
			if(pStep->m_fStepDeltaAuxTemp != 0.0f)
			{
				SetDlgItemText(IDC_STEP_AUXT_DELTA, ValueString(pStep->m_fStepDeltaAuxTemp/1000, PS_OVEN_TEMPERATURE, TRUE));
			}
			else
			{
				SetDlgItemText(IDC_STEP_AUXT_DELTA, ValueString(pStep->m_fStepDeltaAuxTemp, PS_OVEN_TEMPERATURE, TRUE));
			}
			
			if (pStep->m_fStepDeltaAuxTH != 0.0f)
			{
				SetDlgItemText(IDC_STEP_AUXTH_DELTA, ValueString(pStep->m_fStepDeltaAuxTH/1000, PS_OVEN_TEMPERATURE, TRUE));
			}
			else
			{
				SetDlgItemText(IDC_STEP_AUXTH_DELTA, ValueString(pStep->m_fStepDeltaAuxTH, PS_OVEN_TEMPERATURE, TRUE));
			}
			if (pStep->m_fStepDeltaAuxT != 0.0f)
			{
				SetDlgItemText(IDC_STEP_AUXT_AUXTH_DELTA, ValueString(pStep->m_fStepDeltaAuxT/1000, PS_OVEN_TEMPERATURE, TRUE));
			}
			else
			{
				SetDlgItemText(IDC_STEP_AUXT_AUXTH_DELTA, ValueString(pStep->m_fStepDeltaAuxT, PS_OVEN_TEMPERATURE, TRUE));
			}
			
			CString strOut;
			strOut.Format("%.2lf s", pStep->m_fStepDeltaAuxVTime/100);
			SetDlgItemText(IDC_STEP_AUXV_TIME, strOut);
			SetDlgItemText(IDC_STEP_AUXV_DELTA, ValueString(pStep->m_fStepAuxV, PS_VOLTAGE, TRUE));

// 			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V))->SetCheck(pStep->m_bStepDeltaVentAuxV);
// 			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP))->SetCheck(pStep->m_bStepDeltaVentAuxV);
// 			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH))->SetCheck(pStep->m_bStepDeltaVentAuxV);
// 			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T))->SetCheck(pStep->m_bStepDeltaVentAuxV);
// 			((CButton*)GetDlgItem(IDC_CHK_STEP_AUX_V))->SetCheck(pStep->m_bStepDeltaVentAuxV);

			//ksj 20200216 : 버그 수정
			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V))->SetCheck(pStep->m_bStepDeltaVentAuxV);
			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP))->SetCheck(pStep->m_bStepDeltaVentAuxTemp);
			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH))->SetCheck(pStep->m_bStepDeltaVentAuxTh);
			((CButton*)GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T))->SetCheck(pStep->m_bStepDeltaVentAuxT);
			((CButton*)GetDlgItem(IDC_CHK_STEP_AUX_V))->SetCheck(pStep->m_bStepVentAuxV);
			//ksj end
		}
	}
}

CString CTestConReportDlg::ExtEndConCompareTypeConvertor(int iSel) //yulee 20180803
{
	CString strCompType="";
	switch(iSel)
	{
		case 1:
		{
			strCompType.Format("<");
			return strCompType;
		}
		case 2:
		{
			strCompType.Format("<=");	
			return strCompType;
		}
		case 3:
		{
			strCompType.Format("==");	
			return strCompType;
		}
		case 4:
		{
			strCompType.Format(">");	
			return strCompType;
		}
		case 5:
		{
			strCompType.Format(">=");	
			return strCompType;
		}
		case 6:
			{
				strCompType.Format("!=");	
				return strCompType;
		}
		default:
			return strCompType;
	}

}


CString CTestConReportDlg::GetExtEndCondNext(int iSelNum) //yulee 20180803
{
	CString strTmp;

	switch(iSelNum)
	{
	case 251:
		{
			strTmp.Format("Next");
			return strTmp;
		}
	case 252:
		{
			strTmp.Format("Pause");
			return strTmp;
		}
	default:
		{
			strTmp.Format("%d", iSelNum);
			return strTmp;
		}
	}
}

void CTestConReportDlg::OnItemchangedStepList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	POSITION pos = m_ctrlList.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_ctrlList.GetNextSelectedItem(pos);
		int nStepIndex = m_ctrlList.GetItemData(nItem);
		SetStepData(nStepIndex);	
	}
	*pResult = 0;
}

void CTestConReportDlg::OnExcelButton() 
{
	// TODO: Add your control notification handler code here
	CString strFileName;

	strFileName.Format("%s_%s.csv", m_pData->GetModelName(), m_pData->GetScheduleName());

	CFileDialog pDlg(FALSE, "csv", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|");
	if(IDOK != pDlg.DoModal())
	{
		return;
	}

	strFileName = pDlg.GetPathName();
	if(m_pData->SaveToExcelFile(strFileName) == FALSE)
	{
		//AfxMessageBox(strFileName+" 저장 실패!!!");
		AfxMessageBox(strFileName+Fun_FindMsg("TestConReportDlg_OnExcelButton_msg1","IDD_TESTCON_VIEW_DLG"));//&&
	}
}

CString CTestConReportDlg::ValueString(double dData, int item, BOOL bUnit, double dData2)
{
	return m_UnitTrans.ValueString(dData, item, bUnit, dData2);
}

//설정된 종료 조건을 String으로 만든다.
CString CTestConReportDlg::MakeEndString(CStep *pStep)
{
	if(pStep == NULL)	"Error";
	
	CString strTemp, strTemp1;
	switch(pStep->m_type)
	{
	case PS_STEP_CHARGE:			//Charge
		if(fabs(pStep->m_fEndV_H) > 0.0f)	
		{
			strTemp.Format("High V > %s", ValueString(pStep->m_fEndV_H, PS_VOLTAGE));		//ljb v1009
		}
		if(fabs(pStep->m_fEndV_L) > 0.0f)	
		{
			strTemp1.Format(" or Low V > %s", ValueString(pStep->m_fEndV_L, PS_VOLTAGE));		//ljb v1009
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("I < %s", ValueString(pStep->m_fEndI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(pStep->m_fEndTime > 0)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("t > %s", ValueString(pStep->m_fEndTime, PS_STEP_TIME));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndC) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("C > %.1f", ValueString(pStep->m_fEndC, PS_CAPACITY));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDV) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dVp > %s", ValueString(pStep->m_fEndDV, PS_VOLTAGE));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dI > %s", ValueString(pStep->m_fEndDI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndW) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("W > %s", ValueString(pStep->m_fEndW, PS_WATT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndWh) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("Wh > %s", ValueString(pStep->m_fEndWh, PS_WATT_HOUR));
			strTemp += strTemp1;
		}


		break;
	
	case PS_STEP_DISCHARGE:		//Discharge
		if(fabs(pStep->m_fEndV_H) > 0.0f)	
		{
			strTemp.Format("High V < %s", ValueString(pStep->m_fEndV_H, PS_VOLTAGE));		//ljb v1009
		}
		if(fabs(pStep->m_fEndV_L) > 0.0f)	
		{
			strTemp1.Format("or Low V < %s", ValueString(pStep->m_fEndV_L, PS_VOLTAGE));	//ljb v1009
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("I < %s", ValueString(pStep->m_fEndI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(pStep->m_fEndTime > 0)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("t > %s", ValueString(pStep->m_fEndTime, PS_STEP_TIME));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndC) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("C > %s", ValueString(pStep->m_fEndC, PS_CAPACITY));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDV) < 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dVp < %s", ValueString(pStep->m_fEndDV, PS_VOLTAGE));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndDI) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("dI > %s", ValueString(pStep->m_fEndDI, PS_CURRENT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndW) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("W > %s", ValueString(pStep->m_fEndW, PS_WATT));
			strTemp += strTemp1;
		}
		if(fabs(pStep->m_fEndWh) > 0.0f)
		{
			if(!strTemp.IsEmpty())
			{
				strTemp += " or "; 
			}
			strTemp1.Format("Wh > %s", ValueString(pStep->m_fEndWh, PS_WATT_HOUR));
			strTemp += strTemp1;
		}
		break;
	
	case PS_STEP_REST:			//Rest
	case PS_STEP_IMPEDANCE:		//Impedance
		{
			strTemp.Format("t > %s", ValueString(pStep->m_fEndTime, PS_STEP_TIME));
			break;
		}
	
	case PS_STEP_OCV:				//Ocv
	case PS_STEP_END:				//End
	case PS_STEP_ADV_CYCLE:
		break;
	case PS_STEP_LOOP:
		if(pStep->m_nLoopInfoGotoStep <= 0)
		{
			strTemp.Format("반복 %d회 후 다음 Cycle로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);
		}
		else
		{
			strTemp.Format("반복 %d회 후 Step %d로 이동", pStep->m_nLoopInfoCycle, pStep->m_nLoopInfoGotoStep);
		}
		break;
	default:
		strTemp = "";
	}

	return strTemp;
}