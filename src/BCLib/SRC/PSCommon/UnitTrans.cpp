// UnitTrans.cpp: implementation of the CUnitTrans class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UnitTrans.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define CT_CONFIG_REG_SEC "Config"

CUnitTrans::CUnitTrans()
{
	UpdateUnitSetting();

	m_nVoltageUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0);
	m_nCurrentUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0);
	m_nSyncTimeOpt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SyncTime Opt", 0); //ksj 20210308 : Data Sync Time 길이 옵션. 0: 사용안함. 6: 6자리 9: 9자리
}

CUnitTrans::~CUnitTrans()
{

}

void CUnitTrans::UpdateUnitSetting()
{
	char szBuff[32], szUnit[16], szDecimalPoint[16];

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "CAPA Unit", "mF 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCapaUnit = szUnit;
	m_nCapaDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWUnit = szUnit;
	m_nWDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWhUnit = szUnit;
	m_nWhDecimal = atoi(szDecimalPoint);


	//ljb [9/30/2011 XNOTE]
// 	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "F Unit", "F 3"));
// 	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
// 	m_strVUnit = szUnit;
// 	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);

}

CString CUnitTrans::ValueString(double dData, int item, BOOL bUnit,double dData2)
{
	CString strMsg, strTemp;
	double dTemp;
	long lTemp;		//ljb 날자 받기
	//ULONG ulTemp;
	char szTemp[8];

	dTemp = dData;
	lTemp = dData2; //ljb 20131202 add STEP, TOTAL, CV => day

	switch(item)
	{
	case PS_STEP_TYPE:	strMsg = ::PSGetTypeMsg((WORD)dData);
		break;

	case PS_STATE:		strMsg = ::PSGetStateMsg((WORD)dData);
		break;
		
	case PS_VOLTAGE:		//voltage

		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		if(m_strVUnit == "uV")
		{
			dTemp = dTemp * 1000.0f;	//LONG2FLOAT(Value);
		}
		//else //if(m_strVUnit == "mV")
		//{
		//}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg+= (" "+m_strVUnit);
		
		break;

	case PS_CURRENT:		//current
		//if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;
		
		//current
		if(m_strIUnit == "A")
		{
			dTemp = dTemp/1000.0f;
		}  
		else if(m_strIUnit == "uA")
		{
			dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
		}
		//else if(m_strIUnit == "mA")	//mA
		//{
		//}

		if(m_nIDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nIDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strIUnit);

		break;
			
	case PS_WATT	:
		//if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;

		if(m_strWUnit == "W")
		{
			dTemp = dTemp/1000.0f;
		}
		else if(m_strWUnit == "KW")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWUnit == "uW")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWUnit);
		break;	
	
	case PS_CHARGE_WH:
	case PS_DISCHARGE_WH:
	case PS_WATT_HOUR:			
		//if(dTemp < 0)	dTemp = -dTemp;			//양수 표기

		//m단위로 변경
		if(m_nCurrentUnitMode)		dTemp = dTemp /1000.0f;

		if(m_strWhUnit == "Wh")
		{
			dTemp = dTemp/1000.0f;
			strTemp = " Wh";
		}
		else if(m_strWhUnit == "KWh")
		{
			dTemp = dTemp/1000000.0f;
		}
		else if(m_strWhUnit == "uWh")
		{
			dTemp = dTemp*1000.0f;
		}

		if(m_nWhDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nWhDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strWhUnit);
		
		break;
		
	case PS_CHARGE_CAP:
	case PS_DISCHARGE_CAP:
	case PS_CAPACITANCE:
		if(m_nCurrentUnitMode)		dTemp = dTemp / 1000.0f;

		if(m_strCapaUnit == "F") dTemp = dTemp / 1000.0f;
		else if(m_strCapaUnit == "uF") dTemp = dTemp * 1000.0f;

		if(m_nCapaDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nCapaDecimal);
			strMsg.Format(szTemp, dTemp);	
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		if(bUnit) strMsg+= (" "+m_strCapaUnit);
		break;
//20180308 yulee EDLC와 일반 셀의 분리 
#ifdef _EDLC_CELL_
	case PS_CAPACITY:
		{
			//sbc에서 Battery일 경우 uAh로 전송되어 온다.
			//sbc에서 EDLC 경우 mF로 전송되어 온다.
			//dTemp = (double)Value;
			
			// 			if(m_CurrentUnitMode)
			// 			{
			// 				//sbc에서 nAh로 전송되어 온다.
			// 				dTemp = dTemp / 1000.0f;		//uAh 단위로 변환 
			// 			}
			
			if(m_strCUnit == "kF")	//ljb 20151212 add m_strCUnit == "A" 
			{
				dTemp = dTemp/1000000.0f;	//LONG2FLOAT(Value);
			}
			else if(m_strCUnit == "F")
			{
				dTemp = dTemp/1000.0f;
			}
			else if(m_strCUnit == "uF")
			{
				dTemp = dTemp*1000.0f;
			}
			else if(m_strCUnit == "uAh" || m_strCUnit == "uF")
			{
				dTemp = dTemp;
			}
			
			if(m_nCDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nCDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
		}
			break;
#else
	case PS_CAPACITY:
		{
			//sbc에서 Battery일 경우 uAh로 전송되어 온다.
			//sbc에서 EDLC 경우 mF로 전송되어 온다.
			//dTemp = (double)Value;
			
			// 			if(m_CurrentUnitMode)
			// 			{
			// 				//sbc에서 nAh로 전송되어 온다.
			// 				dTemp = dTemp / 1000.0f;		//uAh 단위로 변환 
			// 			}
			
			if(m_strCUnit == "A" || m_strCUnit == "Ah")	//ljb 20151212 add m_strCUnit == "A" 
			{
				dTemp = dTemp/1000.0f;	//LONG2FLOAT(Value);
			}
			else if(m_strCUnit == "mAh")
			{
				dTemp = dTemp;
			}
			else if(m_strCUnit == "nAh")
			{
				dTemp = dTemp*1000000.0f;
			}
			else if(m_strCUnit == "uAh")
			{
				dTemp = dTemp*1000;
			}
			
			if(m_nCDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nCDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
			if(bUnit) strMsg+= (" "+m_strCUnit); //lyj 20200803
		}
		break;
#endif
// 	case PS_CAPACITY:		//capacity 충전C 방전C
// 
// 		//m단위로 변경
// 		if(m_nCurrentUnitMode)		dTemp = dTemp / 1000.0f;
// 
// 		if(m_strCUnit == "Ah" || m_strCapaUnit == "F")
// 		{
// 			dTemp = dTemp/1000.0f;
// 		}
// 		else if(m_strCUnit == "uAh" || m_strCapaUnit == "uF")
// 		{
// 			dTemp = dTemp*1000.0f;
// 		}
// // 		else if(m_strCUnit == "uAh" || m_strCapaUnit == "uF") //yulee 20180306 add
// // 		{
// // 			dTemp = dTemp*1000.0f;
// // 		}
// 
// 		if(m_nCDecimal > 0)	//소수점 표기 
// 		{
// 			sprintf(szTemp, "%%.%df", m_nCDecimal);
// 			strMsg.Format(szTemp, dTemp);	
// 		}
// 		else	//정수 표기 
// 		{
// 			strMsg.Format("%d", (LONG)dTemp);
// 		}
// 		if(bUnit) strMsg+= (" "+m_strCUnit);
// 		break;

	case PS_IMPEDANCE:	
#ifdef _EDLC_TEST_SYSTEM
		strMsg.Format("%.3f", dTemp);
#else
		strMsg.Format("%.1f", dTemp);
#endif
		if(bUnit) strMsg+= " MOhm";
		break;

	case PS_CODE:	//failureCode
		::PSCellCodeMsg((BYTE)dData, strMsg, strTemp);	//Pscommon.dll API
		break;

	case PS_CV_TIME:
	case PS_TOT_TIME:
	case PS_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			if (lTemp > 0)
			{
				dTemp = dTemp + (lTemp * 86400 ); //ljb 날자를 곱한다.
			}
#ifdef _EDLC_TEST_SYSTEM
			strMsg.Format("%.3f", dTemp);
#else
			strMsg.Format("%.2f", dTemp);
#endif
			if(bUnit) strMsg+= " sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			if (lTemp > 0)
			{
				//////////////////////////////////////////////////////////////////////////
				// + BW KIM 2014.02.20
				//dTemp = dTemp + (lTemp * 1440 ); //ljb 날자를 곱한다.
				dTemp = dTemp + (lTemp * 86400 ); //ljb 날자를 곱한다.
				// -
				//////////////////////////////////////////////////////////////////////////
			}
			strMsg.Format("%.2f", dTemp/60.0f);
			if(bUnit) strMsg+= " Min";
		}
		else
		{
			CTimeSpan timeSpan((ULONG)dTemp);
 			float aa = dTemp-(long)dTemp;
// 			if(timeSpan.GetDays() > 0)
// 			{
// 				strMsg.Format("%s.%02d", timeSpan.Format("%Dd %H:%M:%S"), MAKE2LONG(aa*100.0f));
// 			}
// 			else
// 			{
// 				strMsg.Format("%s.%02d", timeSpan.Format("%H:%M:%S"), MAKE2LONG(aa*100.0f));
// 			}
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.20
// 			if (lTemp > 0)
// 			{
// 				strMsg.Format("D%d %s.%02d",lTemp, timeSpan.Format("%H:%M:%S"), MAKE2LONG(aa*100.0f));
// 			}
// 			else
// 			{
// 				strMsg.Format("%s.%02d", timeSpan.Format("%H:%M:%S"), MAKE2LONG(aa*100.0f));
// 			}
			strMsg.Format("D%d %s.%02d",lTemp, timeSpan.Format("%H:%M:%S"), MAKE2LONG(aa*100.0f));
			// -
			//////////////////////////////////////////////////////////////////////////

		}

/*		{
			char szTemp[32];
			ConvertTime(szTemp, Value);
			strMsg = szTemp;
		}
*/		break;

	case PS_GRADE_CODE:	
		strMsg.Format("%c", (BYTE)dData);
		break;
	case PS_AUX_VOLTAGE:
		//mV단위로 변경 
		if(m_strVUnit == "V")
		{
			dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);	ljb 20170403 써미스터 음수(-)처리 위해 수정
			//if ((dTemp / 1000.0f) > 1) dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
		}
		if(m_strVUnit == "uV")
		{
			dTemp = dTemp * 1000.0f;	//LONG2FLOAT(Value);
		}
		//else //if(m_strVUnit == "mV")
		//{
		//}
		
		if(m_nVDecimal > 0)	//소수점 표기 
		{
			sprintf(szTemp, "%%.%df", m_nVDecimal);
			strMsg.Format(szTemp, dTemp);
		}
		else	//정수 표기 
		{
			strMsg.Format("%d", (LONG)dTemp);
		}		
		if(bUnit) strMsg+= (" "+m_strVUnit);
		break;

	case PS_COMM_STATE:
	case PS_CAN_OUTPUT_STATE:
	case PS_CAN_INPUT_STATE:
		if(0xFFFFFFFF == (float)dTemp)
		{
				//strMsg = "미사용";
				strMsg = Fun_FindMsg("UnitTrans_ValueString_msg1","UNITTRANS");//
		}
		else
		{
			strMsg.Format("%d", (LONG)dTemp);
		}
		
		break;
	case PS_AUX_TEMPERATURE:
	case PS_AUX_TEMPERATURE_TH: //201806012 yulee add
	case PS_AUX_HUMIDITY: //ksj 20200120 : 습도 추가.
	case PS_OVEN_TEMPERATURE:
	case PS_CHILLER_REF_TEMP:
	case PS_CHILLER_CUR_TEMP:
		{
			if(0xFFFFFFFF == (float)dTemp)
			{
				//strMsg = "미사용";
				strMsg = Fun_FindMsg("UnitTrans_ValueString_msg1","UNITTRANS");//&&
			}
			else
			{
				//strMsg.Format("%.1f 'C", dTemp);
				strMsg.Format("%.1f", dTemp);
				//strMsg = "미사용";
				//strMsg = Fun_FindMsg("UnitTrans_ValueString_msg1");//
			}
			break;
		}
	case PS_CHILLER_REF_PUMP:
    case PS_CHILLER_CUR_PUMP:
		{
			if(0xFFFFFFFF == (float)dTemp)
			{
				//strMsg = "미사용";
				strMsg = Fun_FindMsg("UnitTrans_ValueString_msg1","UNITTRANS");//&&
			}
			else
			{
				strMsg.Format("%.1f", dTemp);
				if(bUnit) strMsg+= "";
			}
			break;
		}    
	case PS_CELLBAL_CH_DATA:
		{
			//if(0xFFFFFFFF == (float)dTemp)
			//{
			//	strMsg = "미사용";
			//}
			//else
			{
				strTemp.Format(_T("%f"),dData);
				int nPos=strTemp.Find(_T("."));
				strTemp=strTemp.Mid(0,nPos);
				INT64 lVal=_atoi64(strTemp);
				lVal=lVal>>1;

				strMsg=_T("");
				strTemp=_T("");
				for(int iC=0;iC<PS_MAX_CELLBAL_COUNT;iC++)
				{
					lTemp=lVal>>iC;

					lTemp = lTemp & 1;
					
					if(lTemp==1) strMsg+=_T("R");
					else         strMsg+=_T("S");
				}
				if(bUnit) strMsg+= "";
			}
			break;
		}
	case PS_PWRSUPPLY_SETVOLT://yulee 20190514 cny work transfering
	case PS_PWRSUPPLY_CURVOLT:
		{
			if(0xFFFFFFFF == (float)dTemp)
			{
				//strMsg = "미사용";
				strMsg = Fun_FindMsg("UnitTrans_ValueString_msg1","UNITTRANS");//
			}
			else
			{
				strMsg.Format("%.1f", dTemp);
				if(bUnit) strMsg+= " `C";
			}
			break;
		}
	case PS_OVEN_HUMIDITY:
		if(0xFFFFFFFF == (float)dTemp)
		{
				//strMsg = "미사용";
				strMsg = Fun_FindMsg("UnitTrans_ValueString_msg1","UNITTRANS");//
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " %";
		}
		
		break;
	case PS_SYNC_DATE:	//20130604 add
		{

			CString strT;
//			char buffer[24];
//			CString strSyncTime;
			float fSyncDate;
			fSyncDate = (float)dTemp;		//ljb 20131210 edit *1000 delete

			strT.Format("%.0f",fSyncDate);
			if (strT.GetLength() >= 8 ) strMsg.Format("%s-%s-%s", strT.Left(4), strT.Mid(4, 2), strT.Right(2));
			else strMsg ="";

// 			if( fSyncDate > 0.0f)
// 			{
// 				ltoa((long)fSyncDate, buffer, 10);
// 				strT.Format("%s", buffer);
// 				if(!strT.IsEmpty())
// 				{
// 					strMsg.Format("%s-%s-%s", strT.Left(4), strT.Mid(4, 2), strT.Right(2));
// 				}
// 			}
			break;
		}
	case PS_CELLBAL_RES: //yulee 20181212
		if(0xFFFFFFFF == (float)dTemp)
		{
			//strMsg = "미사용";
			strMsg = Fun_FindMsg("UnitTrans_ValueString_msg1","UNITTRANS");//&&
		}
		else
		{
			strMsg.Format("%.1f", dTemp);
			if(bUnit) strMsg+= " Ohm";
		}
		
		break;
	case PS_SYNC_TIME:	//20130604 add
		{			
			//2014.11.03 작업 시간 버그 변경
			CString strT;
			float fSyncDate;
			
			if(m_nSyncTimeOpt == 0) //ksj 20210308 : 옵션 추가.
			{
				//기존 코드
				strT.Format("%.0f",dTemp);

				if(strT.GetLength() > 6){
					fSyncDate = (float)dTemp / 1000; 
					//strT.Format("%d",(int)fSyncDate); //lyj 20201230
				}else{
					fSyncDate = (float)dTemp;
					//strT.Format("%09d",(int)fSyncDate); //lyj 20201230
				}

				//strT.Format("%.0f",fSyncDate);
				strT.Format("%d",(int)fSyncDate); //ksj 20201230


				if (strT.GetLength() > 5 ) {
					strMsg.Format("%s:%s:%s", strT.Left(2), strT.Mid(2, 2), strT.Mid(4,2));
				}			
				else if(strT.GetLength() == 5)
				{				
					CString strTemp;
					//strT="";
					strTemp="0";
					strTemp+=strT;
					strMsg.Format("%s:%s:%s", strTemp.Left(2), strTemp.Mid(2, 2), strTemp.Mid(4,2));				
				}else if(strT.GetLength() == 0){
					strMsg.Format("%s:%s:%s", "00", "00", "00");				
				}else if(strT.GetLength() == 1){
					strMsg.Format("%s:%s:%s", "00", "00", "0" + strT);				
				}else if(strT.GetLength() == 2){
					strMsg.Format("%s:%s:%s", "00", "00", strT);				
				}else if(strT.GetLength() == 3){
					strMsg.Format("%s:%s:%s", "00", "0" + strT.Left(1), strT.Mid(1, 2));
				}else if(strT.GetLength() == 4){
					strMsg.Format("%s:%s:%s", "00", strT.Left(2), strT.Mid(2, 2));
				}				
			}
			else
			{
				//lyj 20201230
				/*fSyncDate = (float)dTemp / 1000;  
				strT.Format("%06d",(int)fSyncDate);			
				strMsg.Format("%s:%s:%s", strT.Left(2), strT.Mid(2, 2), strT.Mid(4,2));*/

				//ksj 20210209			
				/*fSyncDate = (float)dTemp;
				strT.Format("%09d",(int)fSyncDate);
				strMsg.Format("%s:%s:%s", strT.Left(2), strT.Mid(2, 2), strT.Mid(4,2));*/

				//ksj 20210308 : 옵션에 따라 길이 변경 //6자리 또는 9자리
				fSyncDate = (float)dTemp;
				strTemp.Format("%%0%dd",m_nSyncTimeOpt); //6 or 9
				strT.Format(strTemp,(int)fSyncDate);
				strMsg.Format("%s:%s:%s", strT.Left(2), strT.Mid(2, 2), strT.Mid(4,2));

			}		
			break;
		}

		break;
	case PS_STEP_NO:
	default:
		strMsg.Format("%d", (int)dTemp);
		break;
	}
	
	return strMsg;
}

CString CUnitTrans::GetUnitString(int nItem)
{
	CString	strTemp;

	switch(nItem)
	{
	case PS_VOLTAGE:
	case PS_AUX_VOLTAGE:	//=> Aux Voltage로 사용
		return	m_strVUnit;

	case PS_CURRENT:
		{
			strTemp = m_strIUnit;
/*			if(m_nCurrentUnitMode)
			{
				strTemp = "uA";			//11. Current	
			}
*/			return	strTemp;						
		}
			
	//capa 단위를 따라감
	case PS_WATT	:	return m_strWUnit;

/*		strTemp = " mW";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			strTemp = "W";
		}
		if(m_nCurrentUnitMode)
		{
			strTemp = "uW";			//11. Current	
		}
		return strTemp;
*/
	
	//capa 단위를 따라감
	case PS_CHARGE_WH:
	case PS_DISCHARGE_WH:
	case PS_WATT_HOUR:			return m_strWhUnit;
/*		strTemp = " mWh";
		if(m_strCUnit == "Ah" || m_strCUnit == "F")
		{
			strTemp = "Wh";
		}
		if(m_nCurrentUnitMode)
		{
			strTemp = "uWh";			//11. Current	
		}
		return strTemp;
*/		
	case PS_CAPACITANCE:
		return m_strCapaUnit;
	case PS_CHARGE_CAP:
	case PS_DISCHARGE_CAP:
	case PS_CAPACITY:
		strTemp = m_strCUnit;
/*		if(m_nCurrentUnitMode)	
		{
#ifdef EDLC_TEST_SYSTEM
			strTemp = "mF";		//12. Capacity,					
#else
			strTemp = "uAh";		//12. Capacity,					
#endif
		}
*/		return strTemp;

	case PS_IMPEDANCE:		strTemp = "MOhm";
		return strTemp;

	case PS_SYNC_DATE:		strTemp = "Work Time";
		return strTemp;
	case PS_CV_TIME:
	case PS_TOT_TIME:
	case PS_STEP_TIME:

		//sbc에서 100msec단위 전송되어 온다.
		if(m_nTimeUnit == 1)	//sec 표시
		{
			strTemp = "sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			strTemp = "Min";
		}
		return strTemp;

	case PS_AUX_TEMPERATURE:
	case PS_AUX_TEMPERATURE_TH: //20180612 yulee
	case PS_OVEN_TEMPERATURE:
		strTemp = "`C";
		return strTemp;
	case PS_OVEN_HUMIDITY:	//ljb 20100726 add
	case PS_AUX_HUMIDITY: //ksj 20200210
		strTemp = "%";
		return strTemp;

	case PS_STEP_TYPE:
	case PS_STATE:	
	case PS_CODE:	//failureCode
	case PS_GRADE_CODE:	
	case PS_STEP_NO:
	default:
		break;
	}

	return strTemp;
}