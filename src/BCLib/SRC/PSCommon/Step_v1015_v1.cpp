// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Step.h"

#include "ScheduleData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

FILE_STEP_PARAM_V1015_v1 CStep::GetfileStep_v1015_v1()
{
	int i;
	FILE_STEP_PARAM_V1015_v1 stepData;
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V1015_v1));

	stepData.chStepNo	= m_StepIndex;			
	stepData.nProcType	= m_lProcType;		
	stepData.chType		= m_type;
	stepData.chMode		= m_mode;
	stepData.fVref_Charge = m_fVref_Charge;
	stepData.fVref_DisCharge = m_fVref_DisCharge;
	stepData.fIref		= m_fIref;
	stepData.fPref		= m_fPref;
	stepData.fRref		= m_fRref;

	stepData.fStartT	= m_fStartT;	
	stepData.fEndT		= m_fEndT;		
	stepData.fTref		= m_fTref;		
	stepData.fTrate		= m_fTrate;	
	stepData.fHref		= m_fHref;		
	stepData.fHrate		= m_fHrate;	

	stepData.fEndTime	= m_fEndTime;	
	stepData.fCVTime	= m_fCVTime;//yulee 20190715
	stepData.fEndV		= m_fEndV_H;
	stepData.fEndV_L	= m_fEndV_L;
	stepData.fEndI		= m_fEndI;
	stepData.fEndC		= m_fEndC;
	stepData.fEndDV		= m_fEndDV;		
	stepData.fEndDI		= m_fEndDI;	
	stepData.fEndW		= m_fEndW;
	stepData.fEndWh		= m_fEndWh;

	stepData.bValueItem = m_bValueItem	;			
	stepData.bValueStepNo = m_bValueStepNo ;		
	stepData.fValueRate	= m_fValueRate ;			

	stepData.nLoopInfoCycle = m_nLoopInfoCycle;	
	stepData.nLoopInfoGotoStep  = m_nLoopInfoGotoStep;	

	stepData.nMultiLoopGroupID	= m_nMultiLoopGroupID;
	stepData.nMultiLoopInfoCycle	= m_nMultiLoopInfoCycle;
	stepData.nMultiLoopInfoGotoStep	= m_nMultiLoopInfoGotoStep;

	stepData.nAccLoopGroupID	= m_nAccLoopGroupID;
	stepData.nAccLoopInfoCycle	= m_nAccLoopInfoCycle;
	stepData.nAccLoopInfoGotoStep	= m_nAccLoopInfoGotoStep;

	stepData.nGotoStepEndV_H = m_nGotoStepEndV_H;
	stepData.nGotoStepEndV_L = m_nGotoStepEndV_L;
	stepData.nGotoStepEndTime = m_nGotoStepEndTime;
	stepData.nGotoStepEndCVTime = m_nGotoStepEndCVTime;
	stepData.nGotoStepEndC = m_nGotoStepEndC;
	stepData.nGotoStepEndWh = m_nGotoStepEndWh;
	stepData.nGotoStepEndValue = m_nGotoStepEndValue;

	stepData.fVLimitHigh	=	m_fHighLimitV;
	stepData.fVLimitLow		=	m_fLowLimitV;
	stepData.fILimitHigh	=	m_fHighLimitI;	
	stepData.fILimitLow		=	m_fLowLimitI;
	stepData.fCLimitHigh	=	m_fHighLimitC;
	stepData.fCLimitLow		=	m_fLowLimitC;
	stepData.fCapacitanceHigh	= m_fHighCapacitance;
	stepData.fCapacitanceLow	= m_fLowCapacitance;
	stepData.fImpLimitHigh	=	m_fHighLimitImp;
	stepData.fImpLimitLow	=	m_fLowLimitImp;
	stepData.fHighLimitTemp = m_fHighLimitTemp;
	stepData.fLowLimitTemp = m_fLowLimitTemp;

	stepData.fDeltaTime		=	m_fDeltaTimeV; 	
	stepData.fDeltaTime1	=	m_fDeltaTimeI;	
	stepData.fDeltaV		=	m_fDeltaV;		
	stepData.fDeltaI		=	m_fDeltaI;

	stepData.bGrade			=	m_bGrade;
	stepData.sGrading_Val.chTotalGrade			= (BYTE)m_Grading.GetGradeStepSize();
	//	stepData.sGrading_Val.lGradeItem = m_Grading.m_lGradingItem;

	GRADE_STEP	grade_step;
	for(int i = 0; i<m_Grading.GetGradeStepSize(); i++)
	{
		grade_step = m_Grading.GetStepData(i);
		stepData.sGrading_Val.faValue1[i]	=	grade_step.fMin;
		stepData.sGrading_Val.faValue2[i]	=	grade_step.fMax;
		stepData.sGrading_Val.aszGradeCode[i]	=	grade_step.strCode[0];
	}


	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		stepData.fCompVLow[i]	=	m_fCompLowV[i];
		stepData.fCompVHigh[i]	=	m_fCompHighV[i];	
		stepData.fCompTimeV[i] =	m_fCompTimeV[i];
		stepData.fCompILow[i]	=	m_fCompLowI[i];
		stepData.fCompIHigh[i]	=	m_fCompHighI[i];
		stepData.fCompTimeI[i]	=	m_fCompTimeI[i];
		i++;
	}

	//Not Use (Time 비교값으로 대치 가능 하므로 사용 안함
	//	stepData.fIEndHigh;		
	//	stepData.fIEndLow;			
	//	stepData.fVEndHigh;			
	//	stepData.fVEndLow;	

	stepData.fReportTemp = m_fReportTemp;
	stepData.fReportV	=	m_fReportV;
	stepData.fReportI	=	m_fReportI;
	stepData.fReportTime	=	m_fReportTime;

	stepData.fCapaVoltage1 = m_fCapaVoltage1;	
	stepData.fCapaVoltage2 = m_fCapaVoltage2;	

	stepData.fDCRStartTime = m_fDCRStartTime;	
	stepData.fDCREndTime = m_fDCREndTime;

	stepData.fLCStartTime = m_fLCStartTime;	
	stepData.fLCEndTime = m_fLCEndTime;		

	stepData.lRange = m_lRange;	

	//ljb 201012
	stepData.bUseCyclePause = m_bUseCyclePause;
	stepData.bUseLinkStep = m_bUseLinkStep;
	stepData.bUseChamberProg = m_bUseChamberProg;

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	stepData.lEndTimeDay = m_lEndTimeDay;
	stepData.lCVTimeDay = m_lCVTimeDay;
	// -
	//////////////////////////////////////////////////////////////////////////
	//ljb 20101230
	for( int i=0; i < MAX_STEP_CAN_AUX_COMPARE_SIZE; i++)
	{
		stepData.iBranchCanDivision[i] = m_ican_function_division[i];
		stepData.fBranchCanValue[i] = m_fcan_Value[i];
		stepData.cBranchCanDataType[i] = m_ican_data_type[i];
		stepData.cBranchCanCompareType[i] = m_ican_compare_type[i];
		stepData.wBranchCanStep[i] = m_ican_branch[i];

		stepData.iBranchAuxDivision[i] = m_iaux_function_division[i];
		stepData.fBranchAuxValue[i] = m_faux_Value[i];
		stepData.cBranchAuxDataType[i] = m_iaux_data_type[i];
		stepData.cBranchAuxCompareType[i] = m_iaux_compare_type[i];
		stepData.wBranchAuxStep[i] = m_iaux_branch[i];
		stepData.uaux_conti_time[i] = m_iaux_conti_time[i]; //yulee 20180905
	}


	//////////////////////////////////////////////////////////////////////////
	// ljb 20150512 add
	stepData.nValueLoaderItem = nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	stepData.nValueLoaderMode = nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	stepData.fValueLoaderSet = fValueLoaderSet;			//설정값 W,A,V,Ohm
	// -
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// ljb 20160427 add
	stepData.m_nWaitTimeInit = m_nWaitTimeInit;		//0: 사용안함 , 1: ON,	 2: OFF
	stepData.m_nWaitTimeDay = m_nWaitTimeDay;		//0: CP, 1: CC, 2: CV, 3: CR
	stepData.m_nWaitTimeHour = m_nWaitTimeHour;			//설정값 W,A,V,Ohm
	stepData.m_nWaitTimeMin = m_nWaitTimeMin;			//설정값 W,A,V,Ohm
	stepData.m_nWaitTimeSec = m_nWaitTimeSec;			//설정값 W,A,V,Ohm
	// -
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// ljb 20170517 add
	stepData.m_fValueMax =	 m_fValueMax;		//20140207 add Pattern data 중 최대값
	stepData.m_fValueMin =	 m_fValueMin;		//20140207 add Pattern data 중 최소값 

	stepData.m_nNoCheckMode =	m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	stepData.m_nCanTxOffMode =  m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	stepData.m_nCanCheckMode =  m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 
	// -
	//////////////////////////////////////////////////////////////////////////

	//yulee 20180828 add
	stepData.m_bStepChamberStop	= m_bStepChamberStop;

	stepData.m_fCellDeltaVStep			= m_fCellDeltaVStep;
	stepData.m_fStepDeltaAuxTemp		= m_fStepDeltaAuxTemp;
	stepData.m_fStepDeltaAuxTh			= m_fStepDeltaAuxTH;
	stepData.m_fStepDeltaAuxT			= m_fStepDeltaAuxT;
	stepData.m_fStepDeltaAuxVTime		= m_fStepDeltaAuxVTime;
	stepData.m_fStepAuxV			=	 m_fStepAuxV;

	stepData.m_bStepDeltaVentAuxV		= m_bStepDeltaVentAuxV;
	stepData.m_bStepDeltaVentAuxTemp	= m_bStepDeltaVentAuxTemp;
	stepData.m_bStepDeltaVentAuxTH		= m_bStepDeltaVentAuxTh;
	stepData.m_bStepDeltaVentAuxT		= m_bStepDeltaVentAuxT;
	stepData.m_bStepVentAuxV			= m_bStepVentAuxV;

	sprintf(stepData.szSimulationFile,"%s",m_strPatternFileName.Left(256)); //ksj 20180528

	return stepData;
}

void CStep::SetStepData_v1015_v1(FILE_STEP_PARAM_V1015_v1 *pStepData)
{
	int i;
	ClearStepData();

	m_StepIndex		= pStepData->chStepNo;
	m_lProcType		= pStepData->nProcType;	
	m_type			= pStepData->chType;		
	m_mode			= pStepData->chMode;		
	m_fVref_Charge	= pStepData->fVref_Charge;		
	m_fVref_DisCharge = pStepData->fVref_DisCharge;		
	m_fIref			= pStepData->fIref;		
	m_fPref			= pStepData->fPref;		
	m_fRref			= pStepData->fRref;

	m_fStartT		= pStepData->fStartT;
	m_fEndT			= pStepData->fEndT;
	m_fTref			= pStepData->fTref;
	m_fTrate		= pStepData->fTrate;
	m_fHref			= pStepData->fHref;
	m_fHrate		= pStepData->fHrate;

	m_fEndTime			= pStepData->fEndTime;	
	m_fEndV_H			= pStepData->fEndV	;
	m_fEndV_L			= pStepData->fEndV_L;
	m_fEndI				= pStepData->fEndI	;
	m_fEndC				= pStepData->fEndC	;
	m_fEndDV			= pStepData->fEndDV	;		
	m_fEndDI			= pStepData->fEndDI	;
	m_fEndW				= pStepData->fEndW	;
	m_fEndWh			= pStepData->fEndWh	;
	m_fCVTime			= pStepData->fCVTime;

	//ljb v1009 아이템별 Rate 로 분기     ///////////////////////////////////
	m_bValueItem	= pStepData->bValueItem	;			//사용할 ITEM  ( 0: 사용안함 , 1 : SOC, 2 : WattHour)
	m_bValueStepNo	= pStepData->bValueStepNo;			//비교할 기준 용량이 속한 Step번호
	m_fValueRate	= pStepData->fValueRate;			//비교 SOC Rate	
	//////////////////////////////////////////////////////////////////////////
	//ljb v100B 챔버 Prog모드, 챔버연동, Cycle pause,      ///////////////////////////////////
	m_bUseCyclePause	= pStepData->bUseCyclePause	;	//Cycle 완료 후 대기
	m_bUseChamberProg	= pStepData->bUseChamberProg;	//챔버 프로그램 모드로 운영
	m_bUseLinkStep		= pStepData->bUseLinkStep;		//다음 스텝과 연동 (v100B -> 사용 안함)
	//////////////////////////////////////////////////////////////////////////

	m_nLoopInfoCycle	=	pStepData->nLoopInfoCycle;	
	m_nLoopInfoGotoStep		=	pStepData->nLoopInfoGotoStep;	

	m_nMultiLoopGroupID	=	pStepData->nMultiLoopGroupID;
	m_nMultiLoopInfoCycle	=	pStepData->nMultiLoopInfoCycle;
	m_nMultiLoopInfoGotoStep	=	pStepData->nMultiLoopInfoGotoStep;

	m_nAccLoopGroupID	=	pStepData->nAccLoopGroupID;
	m_nAccLoopInfoCycle	=	pStepData->nAccLoopInfoCycle;
	m_nAccLoopInfoGotoStep	=	pStepData->nAccLoopInfoGotoStep;

	m_nGotoStepEndV_H = pStepData->nGotoStepEndV_H;
	m_nGotoStepEndV_L = pStepData->nGotoStepEndV_L;
	m_nGotoStepEndTime = pStepData->nGotoStepEndTime;
	m_nGotoStepEndCVTime = pStepData->nGotoStepEndCVTime;
	m_nGotoStepEndC = pStepData->nGotoStepEndC;
	m_nGotoStepEndWh = pStepData->nGotoStepEndWh;
	m_nGotoStepEndValue = pStepData->nGotoStepEndValue;

	m_fHighCapacitance	= pStepData->fCapacitanceHigh;
	m_fLowCapacitance	= pStepData->fCapacitanceLow;
	m_fHighLimitV	=	pStepData->fVLimitHigh	;
	m_fLowLimitV	=	pStepData->fVLimitLow		;
	m_fHighLimitI	=	pStepData->fILimitHigh	;	
	m_fLowLimitI	=	pStepData->fILimitLow		;	
	m_fHighLimitC	=	pStepData->fCLimitHigh	;	
	m_fLowLimitC	=	pStepData->fCLimitLow		;	
	m_fHighLimitImp	=	pStepData->fImpLimitHigh	;
	m_fLowLimitImp	=	pStepData->fImpLimitLow	;	
	m_fHighLimitTemp	=	pStepData->fHighLimitTemp	;
	m_fLowLimitTemp	=	pStepData->fLowLimitTemp	;	

	m_fDeltaTimeV	=	pStepData->fDeltaTime		; 	
	m_fDeltaTimeI	=	pStepData->fDeltaTime1	;	
	m_fDeltaV		=	pStepData->fDeltaV		;		
	m_fDeltaI		=	pStepData->fDeltaI		;

	m_bGrade = (BYTE)pStepData->bGrade;
	m_Grading.ClearStep();
	//	m_Grading.m_lGradingItem = pStepData->sGrading_Val.lGradeItem;

	//	GRADE_STEP	grade_step;
	for(int i = 0; i<pStepData->sGrading_Val.chTotalGrade; i++)
	{	
		m_Grading.AddGradeStep(pStepData->sGrading_Val.lGradeItem, pStepData->sGrading_Val.faValue1[i], pStepData->sGrading_Val.faValue2[i], pStepData->sGrading_Val.aszGradeCode[i]);	//20081210 KHS
	}

	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		m_fCompLowV[i]	=pStepData->fCompVLow[i]	;
		m_fCompHighV[i]	=pStepData->fCompVHigh[i]	;	
		m_fCompTimeV[i]	=pStepData->fCompTimeV[i] 	;
		m_fCompLowI[i]	=pStepData->fCompILow[i]	;
		m_fCompHighI[i]	=pStepData->fCompIHigh[i]	;
		m_fCompTimeI[i]	=pStepData->fCompTimeI[i]	;
		i++;
	}

	//Not Use (Time 비교값으로 대치 가능 하므로 사용 안함
	//	stepData.fIEndHigh;		
	//	stepData.fIEndLow;			
	//	stepData.fVEndHigh;			
	//	stepData.fVEndLow;	

	m_fReportTemp = pStepData->fReportTemp;
	m_fReportV = pStepData->fReportV;
	m_fReportI = pStepData->fReportI;
	m_fReportTime = pStepData->fReportTime;


	m_fCapaVoltage1	 = pStepData->fCapaVoltage1;	
	m_fCapaVoltage2	 = pStepData->fCapaVoltage2;	

	m_fDCRStartTime = pStepData->fDCRStartTime ;	
	m_fDCREndTime = pStepData->fDCREndTime ;

	m_fLCStartTime = pStepData->fLCStartTime ;	
	m_fLCEndTime = pStepData->fLCEndTime ;		

	m_lRange = pStepData->lRange;			

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	m_lEndTimeDay = pStepData->lEndTimeDay; 
	m_lCVTimeDay = pStepData->lCVTimeDay;
	// -
	//////////////////////////////////////////////////////////////////////////
	//ljb 20101230
	for( int i=0; i < MAX_STEP_CAN_AUX_COMPARE_SIZE; i++)
	{
		m_ican_function_division[i] = pStepData->iBranchCanDivision[i];
		m_fcan_Value[i] = pStepData->fBranchCanValue[i];
		m_ican_data_type[i] = pStepData->cBranchCanDataType[i];
		m_ican_compare_type[i] = pStepData->cBranchCanCompareType[i];
		m_ican_branch[i] = pStepData->wBranchCanStep[i];

		m_iaux_function_division[i] = pStepData->iBranchAuxDivision[i];
		m_faux_Value[i] = pStepData->fBranchAuxValue[i];
		m_iaux_data_type[i] = pStepData->cBranchAuxDataType[i];
		m_iaux_compare_type[i] = pStepData->cBranchAuxCompareType[i];
		m_iaux_branch[i] = pStepData->wBranchAuxStep[i];
		m_iaux_conti_time[i] = pStepData->uaux_conti_time[i]; //yulee 20180905 
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	nValueLoaderItem = pStepData->nValueLoaderItem; 	//0: 사용안함 , 1: ON,	 2: OFF
	nValueLoaderMode = pStepData->nValueLoaderMode;	//0: CP, 1: CC, 2: CV, 3: CR
	fValueLoaderSet = pStepData->fValueLoaderSet;	//설정값 W,A,V,Ohm
	// -
	//////////////////////////////////////////////////////////////////////////
	m_nWaitTimeInit = pStepData->m_nWaitTimeInit;
	m_nWaitTimeDay = pStepData->m_nWaitTimeDay;
	m_nWaitTimeHour = pStepData->m_nWaitTimeHour;
	m_nWaitTimeMin = pStepData->m_nWaitTimeMin;
	m_nWaitTimeSec = pStepData->m_nWaitTimeSec;
	//////////////////////////////////////////////////////////////////////////
	//ljb 20170517 add
	m_fValueMax  = pStepData->m_fValueMax;		//20140207 add Pattern data 중 최대값
	m_fValueMin  = pStepData->m_fValueMin;		//20140207 add Pattern data 중 최소값 

	m_nNoCheckMode  = pStepData->m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	m_nCanTxOffMode  = pStepData->m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	m_nCanCheckMode  = pStepData->m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	m_bStepChamberStop	=	pStepData->m_bStepChamberStop;	//yulee 20180828

	m_strPatternFileName = pStepData->szSimulationFile; //ksj 20180528

	m_fCellDeltaVStep = pStepData->m_fCellDeltaVStep;
	m_fStepDeltaAuxTemp = pStepData->m_fStepDeltaAuxTemp;
	m_fStepDeltaAuxTH = pStepData->m_fStepDeltaAuxTh;
	m_fStepDeltaAuxT = pStepData->m_fStepDeltaAuxT;
	m_fStepDeltaAuxVTime = pStepData->m_fStepDeltaAuxVTime;
	m_fStepAuxV = pStepData->m_fStepAuxV;

	m_bStepDeltaVentAuxV = pStepData->m_bStepDeltaVentAuxV;
	m_bStepDeltaVentAuxTemp = pStepData->m_bStepDeltaVentAuxTemp;
	m_bStepDeltaVentAuxTh = pStepData->m_bStepDeltaVentAuxTH;
	m_bStepDeltaVentAuxT = pStepData->m_bStepDeltaVentAuxT;
	m_bStepVentAuxV = pStepData->m_bStepVentAuxV;
}

BOOL CStep::SaveStep_v1015_v1(CString strFileName, void *pSch, void* pSchAddInfo)
{
	FILE *fp = fopen(strFileName, "wb");
	if(fp == NULL){
		return FALSE;
	}

	CScheduleData* pSchedule = (CScheduleData*)pSch;
	CScheduleData* pScheduleAddInfo = (CScheduleData*)pSchAddInfo;

	//파일 Header 기록 
	PS_FILE_ID_HEADER	FileHeader;
	ZeroMemory(&FileHeader, sizeof(PS_FILE_ID_HEADER));

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(FileHeader.szCreateDateTime, "%s", dateTime.Format());			//기록 시간
	FileHeader.nFileID = SCHEDULE_FILE_ID;									//파일 ID
	FileHeader.nFileVersion = SCHEDULE_FILE_VER_v1015_v1;
	sprintf(FileHeader.szDescrition, "PNE power supply schedule file.");	//파일 서명 

	fwrite(&FileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);												//기록 실시 
	TRACE("FileHeader Size %d\n", sizeof(FileHeader));

	//모델 정보 기록 
	fwrite(&pScheduleAddInfo->m_ModelData, sizeof(pScheduleAddInfo->m_ModelData), 1, fp);				//기록 실시 

	//공정 정보 기록 
	fwrite(&pScheduleAddInfo->m_ProcedureData, sizeof(pScheduleAddInfo->m_ProcedureData), 1, fp);		//기록 실시 

	//cell check 조건 기록 
	fwrite(&pScheduleAddInfo->m_CellCheck, sizeof(pScheduleAddInfo->m_CellCheck), 1, fp);				//기록 실시 

	//Step Size 기록 //ksj 20180528
	PS_RECORD_FILE_HEADER stepSize;	
	ZeroMemory(&stepSize, sizeof(PS_RECORD_FILE_HEADER));
	stepSize.nColumnCount = pScheduleAddInfo->m_apStepArray.GetSize();
	fwrite(&stepSize,sizeof(PS_RECORD_FILE_HEADER), 1, fp);

	CStep *pStep;
	int nWrite = 0;		

	FILE_STEP_PARAM_V1015_v1 fileStep_V1015_v1;		

	//ljb 2011415 이재복 ////////// s
	if (pSchedule != NULL)
	{
		//Simple test 스케쥴 저장
		for(int i = 0; i< (int)pSchedule->GetStepSize(); i++)
		{
			pStep = (CStep *)pSchedule->GetStepData(i);
			fileStep_V1015_v1 = pStep->GetfileStep_v1015_v1(); //ksj 20180528 //yulee 20180828
			nWrite += fwrite(&fileStep_V1015_v1, sizeof(FILE_STEP_PARAM_V1015_v1), 1, fp);					//기록 실시 //yulee 20180828
		}
	}
	else
	{
		for(int i = 0; i< pScheduleAddInfo->m_apStepArray.GetSize(); i++)
		{
			pStep = (CStep *)pScheduleAddInfo->m_apStepArray.GetAt(i);
			fileStep_V1015_v1 = pStep->GetfileStep_v1015_v1();	//yulee 20180828 
			nWrite += fwrite(&fileStep_V1015_v1, sizeof(FILE_STEP_PARAM_V1015_v1), 1, fp);					//기록 실시 //yulee 20180828			
		}
	}

	//ksj 20180528 : 스케쥴 파일에 패턴 저장하기
	for(int i = 0; i< pScheduleAddInfo->m_apStepArray.GetSize(); i++)	
	{
		pStep = (CStep *)pScheduleAddInfo->m_apStepArray.GetAt(i);
		if(pStep->m_type == PS_STEP_PATTERN)	
		{
			Fun_SavePatternFileToSch(fp, pStep->m_StepIndex,pStep->m_strPatternFileName);
		}
	}

	int iTmpCnt = 0;
	//cny 201809----------------------------------------
	for(int iB = 0; iB< pScheduleAddInfo->m_apStepArray.GetSize(); iB++)	
	{
		pStep = (CStep *)pScheduleAddInfo->m_apStepArray.GetAt(iB);
		if( pStep->m_type != PS_STEP_ADV_CYCLE &&
			pStep->m_type != PS_STEP_LOOP &&
			pStep->m_type != PS_STEP_END)	
		{
			Fun_SaveCellBALFileToSch(fp,pStep);
			iTmpCnt++;
		}
	}
	TRACE("Sch CellBalance Make Cnt, iTmpCnt : %d", iTmpCnt);
	iTmpCnt = 0;

	for(int iP = 0; iP< pScheduleAddInfo->m_apStepArray.GetSize(); iP++)	
	{
		pStep = (CStep *)pScheduleAddInfo->m_apStepArray.GetAt(iP);
		if(pStep->m_type == PS_STEP_REST)	
		{
			Fun_SavePwrSupplyFileToSch(fp,pStep);
			iTmpCnt++;
		}
	}
	TRACE("Sch PS Make Cnt, iTmpCnt : %d", iTmpCnt);
	iTmpCnt = 0;

	for(int iC = 0; iC< pScheduleAddInfo->m_apStepArray.GetSize(); iC++)	
	{
		pStep = (CStep *)pScheduleAddInfo->m_apStepArray.GetAt(iC);
		if( pStep->m_type != PS_STEP_ADV_CYCLE &&
			pStep->m_type != PS_STEP_LOOP &&
			pStep->m_type != PS_STEP_END)	
		{
			Fun_SaveChillerFileToSch(fp,pStep);
			iTmpCnt++;
		}
	}
	TRACE("Sch Chiller Make Cnt, iTmpCnt : %d", iTmpCnt);

	fclose(fp);

	return TRUE;
}