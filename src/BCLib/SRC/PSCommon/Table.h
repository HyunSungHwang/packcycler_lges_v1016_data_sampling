/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: Table.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CTable class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// Table.h: interface for the CTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#include "..\..\INCLUDE\PSCommon.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CTable
//
//	Comment: 
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
#define RS_COL_VOLTAGE		"Voltage"		//PS_VOLTAGE
#define RS_COL_VOLTAGE1		"Voltage1"
#define RS_COL_VOLTAGE2		"Voltage2"
#define RS_COL_VOLTAGE3		"Voltage3"
#define RS_COL_VOLTAGE4		"Voltage4"
#define RS_COL_CURRENT		"Current"			//PS_CURRENT
#define RS_COL_CAPACITY		"Capacity"			//PS_CAPACITY
#define RS_COL_TIME			"Time"				//PS_STEP_TIME
#define RS_COL_WATT			"Power"				//PS_WATT
#define RS_COL_WATT_HOUR	"WattHour"			//PS_WATT_HOUR
//#define RS_COL_TEMPERATURE	"Temperature"		//PS_TEMPERATURE
//#define RS_COL_TEMPERATURE2	"Temperature2"
#define RS_COL_AVG_VTG		"VoltageAverage"	//PS_AVG_VOLTAGE
#define RS_COL_AVG_CRT		"CurrentAverage"	//PS_AVG_CURRENT
#define RS_COL_STEP_NO		"No"				//PS_STEP_NO
#define RS_COL_INDEX_FROM	"IndexFrom"
#define RS_COL_INDEX_TO		"IndexTo"
#define RS_COL_CUR_CYCLE	"CurCycle"			//PS_CUR_CYCLE
#define RS_COL_TOT_CYCLE	"TotalCycle"		//PS_TOT_CYCLE
#define RS_COL_TYPE			"Type"				//PS_STEP_TYPE
#define RS_COL_STATE		"State"				//PS_STATE
#define RS_COL_TOT_TIME		"TotTime"			//PS_TOT_TIME
#define RS_COL_CODE			"Code"				//PS_CODE
#define RS_COL_GRADE		"Grade"				//PS_GRADE_CODE
#define RS_COL_IR			"IR"				//PS_IMPEDANCE
#define RS_COL_CH_NO		"ChNo"				//PS_CHANNEL_NO
#define RS_COL_SAVE_ACCU	"Select"
#define RS_COL_SEQ			"Sequence"			//PS_DATA_SEQ
#define RS_COL_OCV			"OCV"
//#define RS_COL_AUX_CAN		"AuxCAN"
//#define RS_COL_ACC_CYCLE		"AccCycle"			//PS_ACC_CYCLE
//#define RS_COL_TEMPERATURE1	"AuxTemperature"			//PS_TEMPERATURE1
//#define RS_COL_AUX_VOLTAGE	"AuxVoltage"
#define RS_COL_AUX_TEMP		"AuxT"				//ljb 2010-06-23 
#define RS_COL_AUX_VOLT		"AuxV"				//ljb 2010-06-23 

#define RS_COL_OVEN_TEMP	"OvenT"				//ljb 2010-07-26
#define RS_COL_OVEN_HUMI	"OvenH"				//ljb 2010-07-26

#define RS_COL_CAN			"CAN"				//ljb 2011210 이재복 //////////
#define RS_COL_CAN_S1		"CANs1"				//yulee 20180903
#define RS_COL_CAN_S2		"CANs2"				//yulee 20180903
#define RS_COL_CAN_S3		"CANs3"				//yulee 20180903
#define RS_COL_CAN_S4		"CANs4"				//yulee 20181009

#define RS_COL_AUX_TEMPTH	"AuxTH"				//20180607 yulee 추가
#define RS_COL_AUX_HUMI		"AuxH"				//ksj 20200116 : 습도 센서 추가.

#define RS_COL_COMM_STATE	"CommState"			//ljb 201009 통신상태
#define RS_COL_OUTPUT_STATE	"OutputState"			//ljb 201012 CAN 통신상태
#define RS_COL_INPUT_STATE	"InputState"			//ljb 201012 CAN 통신상태
#define RS_COL_VOLTAGE_INPUT	"Voltage Input"			//ljb 201012 
#define RS_COL_VOLTAGE_POWER	"Voltage Power"			//ljb 201012 
#define RS_COL_VOLTAGE_BUS		"Voltage Bus"			//ljb 201012 

#define RS_COL_ACC_CYCLE1	"AccCycle1"			//PS_ACC_CYCLE
#define RS_COL_ACC_CYCLE2	"AccCycle2"			//PS_ACC_CYCLE
#define RS_COL_ACC_CYCLE3	"AccCycle3"			//PS_ACC_CYCLE
#define RS_COL_ACC_CYCLE4	"AccCycle4"			//PS_ACC_CYCLE
#define RS_COL_ACC_CYCLE5	"AccCycle5"			//PS_ACC_CYCLE

#define RS_COL_MULTI_CYCLE1	"MultiCycle1"			//PS_MULTI_CYCLE
#define RS_COL_MULTI_CYCLE2	"MultiCycle2"			//PS_MULTI_CYCLE
#define RS_COL_MULTI_CYCLE3	"MultiCycle3"			//PS_MULTI_CYCLE
#define RS_COL_MULTI_CYCLE4	"MultiCycle4"			//PS_MULTI_CYCLE
#define RS_COL_MULTI_CYCLE5	"MultiCycle5"			//PS_MULTI_CYCLE

#define RS_COL_CHARGE_AH	"ChargeAh"			//PS_CHARGE_CAP
#define RS_COL_DISCHARGE_AH	"DisChargeAh"		//PS_DISCHARGE_CAP
#define RS_COL_CAPACITANCE	"Capacitance"		//PS_CAPACITANCE
#define RS_COL_CHARGE_WH	"ChargeWh"			//PS_CHARGE_WH
#define RS_COL_DISCHARGE_WH	"DisChargeWh"		//PS_DISCHARGE_WH
#define RS_COL_CV_TIME		"CVTime"			//PS_CV_TIME
#define RS_COL_SYNC_DATE	"SyncDate"			//PS_SYNC_DATE
#define RS_COL_SYNC_TIME	"SyncTime"			//PS_SYNC_TIME
#define RS_COL_METER		"Meter1"			//PS_METER_DATA

#define RS_COL_TIME_DAY			"TimeDay"			//PS_STEP_TIME DAY
#define RS_COL_TOT_TIME_DAY		"TotTimeDay"		//PS_TOT_TIME DAY
#define RS_COL_CV_TIME_DAY		"CVTimeDay"			//PS_CV_TIME DAY

#define RS_COL_VOLTAGE_LOAD    	"Load Voltage"			//ljb 20150430 
#define RS_COL_CURRENT_LOAD	    "Load urrent"			//ljb 20150430 

#define RS_COL_CHILLER_REF_TEMP	"ChillerRefT"    //ljb 20170912
#define RS_COL_CHILLER_CUR_TEMP	"ChillerCurT"	//ljb 20170912

#define RS_COL_CHILLER_REF_PUMP	"ChillerRefF"    //ljb 20170912
#define RS_COL_CHILLER_CUR_PUMP	"ChillerCurF"	//ljb 20170912

#define RS_COL_CELLBAL_CH_DATA	"CellBALChData"	//ljb 20170912

#define RS_COL_PWRSUPPLY_SETVOLT "P/SSetV" //ljb 20170912 //yulee 20190514 cny work transfering
#define RS_COL_PWRSUPPLY_CURVOLT "P/SCurV" //ljb 20170912

//TABEL_FILE_COLUMN_HEAD 의 이름과 같이 정의해야함
//#define TABEL_FILE_COLUMN_HEAD 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temperature,Press,VoltageAverage,CurrentAverage,Sequence,ChargeAh,DisChargeAh,Capacitance,ChargeWh,DisChargeWh,CVTime,SyncDate,SyncTime,AccCycle"


// #define		PS_TEST_NAME		0x11
// #define		PS_SCHEDULE_NAME	0x12
// #define		PS_MODULE_NO		0x14
// #define		PS_LOT_NO			0x15
// #define		PS_CAPACITY_SUM		0x19  
// #define		PS_START_TIME		0x1d
// #define		PS_END_TIME			0x1e
// #define		PS_CAN_DATA			0x1f


class CChData ;
class AFX_EXT_CLASS CTable  
{
////////////////
// Attributes //
////////////////

private:
	int FindItemIndex(WORD nItem);
	void				ReleaseRawDataMemory();
	LONG				m_lLastRecordIndex;
	LONG				m_lRecordIndex;
	LONG                m_lIndex;

	BOOL                m_bLoadData;
	BOOL                m_bLoadLastData;

	COleDateTime        m_StartTime;
	LONG                m_lStepNo;

	CStringList         m_strlistTitle;
	CWordArray			m_awlistItem;

	float*              m_pfltLastData;
	float*				m_pfltAuxLastData;
	CAN_VALUE*			m_pCanLastData;

	float**             m_ppfltData;
	float**				m_ppfTempChData;
	float**				m_ppfTempAuxData;
	CAN_VALUE**			m_ppCanData;

	//------------------------------------------------------------------------
	// Added by 224 (2014/02/02) : CTable 생성 시 메모리가 많이 설정하여
	// 중복성으로 의심 되어 CChData 에 공통으로 저장되게 바꿈
	CChData* m_pParent ; 
	//PS_AUX_DISP_INFO*	 m_ArrayAuxInfo[512];   //복구에서 사용. //2014.08.14
	//PS_CAN_DISP_INFO*	 m_ArrayCanInfo[512];	//ljb 2011210 이재복 복구에서 사용.
	// 2014.10.22 Aux 정보값을 불러오지못해서  수정.
	PS_AUX_DISP_INFO* GetArrayAuxInfo(INT nIdx)
	{ 
		ASSERT(this);
		ASSERT(nIdx < 512);
		ASSERT(m_pParent) ;
		
		return &m_pParent->m_ArrayAuxInfo[nIdx] ; //2014.10.22 수정
		//return &m_pParent->m_TableArrayAuxInfo[nIdx] ;
	}
	
	// 2014.10.22 Can 정보값을 불러오지못해서  수정.
	PS_CAN_DISP_INFO* GetArrayCanInfo(INT nIdx)
	{ 
		ASSERT(this);
		ASSERT(nIdx < 512);
		ASSERT(m_pParent) ;
		
		return &m_pParent->m_ArrayCanInfo[nIdx] ; //2014.10.22 수정
		//return &m_pParent->m_TableArrayCanInfo[nIdx] ;
	}
	//------------------------------------------------------------------------

	LONG				m_lTotCycle;
	WORD				m_wType;
	CWordArray			m_DataItemList;				// 컬럼값(아이템)들을 보관한다.

	LONG                m_lNumOfData;				//.cyc에 저장된 data 크기 
	long				m_lLoadedDataCount;			//step start data까지 포함된 크기

	BYTE	m_chNo;			
	BYTE	m_chState;		
	BYTE	m_chDataSelect; 
	BYTE	m_chCode;		
	BYTE	m_chGradeCode;	

	ULONG	m_nCurrentCycleNum;
	ULONG	m_lSaveSequence;

	ULONG	m_lAccCycleNum;	//v1008

	ULONG	m_lSyncDate;	//ljb 20140116 add v100C
	float   m_fSyncTime;

	ULONG	m_lAccCycleGroupNum1;
	ULONG	m_lAccCycleGroupNum2;
	ULONG	m_lAccCycleGroupNum3;
	ULONG	m_lAccCycleGroupNum4;
	ULONG	m_lAccCycleGroupNum5;

	ULONG	m_lMultiCycleGroupNum1;
	ULONG	m_lMultiCycleGroupNum2;
	ULONG	m_lMultiCycleGroupNum3;
	ULONG	m_lMultiCycleGroupNum4;
	ULONG	m_lMultiCycleGroupNum5;

	ULONG	m_lchCommState;		//ljb 20110101
	ULONG	m_lchOutputState;	//ljb 20110101
	ULONG	m_lchInputState;	//ljb 20110101
	
	ULONG	m_lstepDay;		//ljb 20131208 add
	ULONG	m_ltotalDay;	//ljb 20131208 add
	
///////////////////////////////////////////////
// Operations: "Construction and destruction //
///////////////////////////////////////////////
public:
//	CTable(LONG lIndex);
	CTable(LPCTSTR strTitle, LPCTSTR strFromList, CChData* pParent = NULL);
	CTable(CWordArray *paItem, LPPS_STEP_END_RECORD lpRecord, CChData* pParent = NULL);
	CTable(CWordArray *paItem, LPPS_STEP_END_RECORD_V8 lpRecord, CChData* pParent = NULL);
	virtual ~CTable();
	float CTable::fltPlaceFix(float Num, int Place); //yulee 20181008
//  	void* operator new(size_t nSize);
//     void operator delete(void *pDelete, size_t nSize);

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////
public:
	BOOL EditRecordIndex(int nFrom, int nTo);

	CAN_VALUE GetLastCanData(int nCanIndex);
	void SetCanLastData(CAN_VALUE * pLastCan, int nCount);
	CAN_VALUE* GetCanData(LONG &lDataNum, int nCanCol);
	CAN_VALUE GetCanDatum(INT row, int nCanCol) ;

	int LoadCanData(CString strCanPath);
	int LoadCanData_ORIGIN(LPCTSTR strCanPath);
	
	// Added by 224 (2013/12/30) : 소스 호환을 위해 함수 추가
	int LoadRawData(CString strRawPath);
	
	int LoadAuxData(CString strAuxPath);
	int LoadAuxData_ORIGIN(LPCTSTR strAuxPath);
	
	void LoadAuxTitle(CString strAuxConfigPath);
	void LoadCanTitle(CString strCanConfigPath);	//ljb 2011210 이재복 //////////
	void SetAuxLastData(float * pfltLastAux, int nCount);
	int GetAuxIndexToTitle(CString strTitle, WORD wPoint);
	int GetCanIndexToTitle(CString strTitle, WORD wPoint);	//ljb 2011215 이재복 //////////
	fltPoint * GetAuxData(LONG& lDataNum, int nAuxCol);
	CString GetAuxTitleOfIndex(int nIndex);
	CString GetCanTitleOfIndex(int nIndex);		//ljb 2011210 이재복 //////////
	int m_nAuxColumnCount;
	int m_nCanColumnCount;
	int GetAuxColumnCount();
	int GetCanColumnCount();	//ljb 2011210 이재복 //////////
	CString GetItemName(WORD wItem);
	WORD GetItemID(CString strTitle);
	CWordArray *	GetRecordItemList()	{	return &m_DataItemList;		}
	LONG	GetRecordItemSize()		{	return m_DataItemList.GetSize();	}
	LONG	GetRecordCount();
	WORD	GetType()			{	return m_wType;		}
	LONG	GetRecordIndex()	{	return m_lRecordIndex;	}
	LONG	GetLastRecordIndex()	{	return	m_lLastRecordIndex	; 	}
	LONG    GetStepNo()				{	return m_lStepNo;		};
	LONG    GetCycleNo()			{	return m_lTotCycle;		};
	void	LoadData_v100B_ORIGIN(LPCTSTR strChPath);
	void	LoadData_v100B(CString strChPath);
	void	LoadDataB(LPCTSTR strChPath);
	void	LoadDataC(LPCTSTR strChPath);
//	void    LoadData(LPCTSTR strChPath);
	float   GetLastData(LPCTSTR strTitle, WORD wPoint=0x0000);
//	float   GetDCIR()			{	return m_fltDCIR;		};
//	float   GetOCV()			{	return m_fltOCV;		};
	//fltPoint* GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);
	fltPoint* GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000, int nType = 0);


	// Added by 224 (2014/07/28) : 대용량 데이터의 과도한 메모리 할당으로 인한
	fltPoint GetDatum(LPCTSTR strYAxisTitle, INT row, WORD wPoint=0x0000);
	INT GetDataCount() { ASSERT(this) ; return (m_bLoadData) ? m_lLoadedDataCount : 0 ; }

	double  m_dCellBALChData;

protected:
	long FindRsColumnIndex(LPCTSTR szTitle);
	long FindRsColumnIndex(WORD nItem);
	int  FindIndexOfTitle(LPCTSTR title);
	int FindIndexOfTitle(WORD wItem);

};

#endif // !defined(AFX_TABLE_H__40068524_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
