/***********************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: ChData.cpp

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: Member functions of CChData class are defined
***********************************************************************/

// ChData.cpp: implementation of the CChData class.
//
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// #include

#include "stdafx.h"
#include "ChData.h"
#include "Table.h"
#include "ScheduleData.h"
#include <float.h>
#include <io.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
	Name:    CChData
	Comment: Constructor of CChData class
	Input:   LPCTSTR strPath - Path where channel data is located
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CChData::CChData(LPCTSTR strPath) : m_strPath(strPath)
{
	// Initial point of pattern object is NULL
	m_pSchedule = NULL;
	m_nTotalRecordCount = 0;
	m_nAuxColumnCount = 0;
	m_nAuxTempCount = 0;
	m_nAuxVoltCount = 0;
	m_nAuxTempTHCount = 0; //20180607 yulee 
	m_nAuxHumiCount = 0; //ksj 20200116 : 습도 추가
	m_nCanMasterCount = 0; 
	m_nCanSlaveCount = 0;
	m_nCanColumnCount =0;
	ZeroMemory(m_ArrayCanInfo, sizeof(PS_CAN_DISP_INFO)*512);
	ZeroMemory(m_ArrayAuxInfo, sizeof(PS_AUX_DISP_INFO)*512);
	m_TableArrayCanInfo = new PS_CAN_DISP_INFO[512] ;
	ZeroMemory(m_TableArrayCanInfo, sizeof(PS_CAN_DISP_INFO)*512);
	
	m_TableArrayAuxInfo = new PS_AUX_DISP_INFO[512] ;
	ZeroMemory(m_TableArrayAuxInfo, sizeof(PS_AUX_DISP_INFO)*512);
	
	// Added by 224 (2014/01/28) : CChData 의 구간을 반드시 선택해야함
	// 초기값을 -1로 설정
	m_nFrom = -1 ;
	m_nTo   = -1 ;
	init_Language();
}

/*
	Name:    ~CChData
	Comment: Destructor of CChData class
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
CChData::~CChData()
{
	// Pattern object에 할당된 memory가 있으면 지운다.
	if(m_pSchedule!=NULL) 
	{
		delete m_pSchedule;
		m_pSchedule = NULL;
	}

	// Table의 list를 비우면서 각 Table에 할당된 memeory를 지운다.
	while(!m_TableList.IsEmpty())
	{
		CTable* pTable = m_TableList.RemoveTail();
		delete pTable;
	}
	// Added by 224 (2014/02/02) : 
	// AUX_DISP_INFO[512], CAN_DISP_INFO[512] 를 CTable 에서 제거하고
	// CChData에 값을 이동.
	//------------------------------------------------
	if (m_TableArrayAuxInfo)
	{
		delete[] m_TableArrayAuxInfo ; //2014.08.14
		m_TableArrayAuxInfo = NULL ;
	}
	
	if (m_TableArrayCanInfo)
	{
		delete[] m_TableArrayCanInfo ; //2014.08.14
		m_TableArrayCanInfo = NULL ;
	}
	//------------------------------------------------
	
	
}

/*
---------------------------------------------------------
---------
-- Filename		:	ChData.cpp 
-- Description	:	StepEnd 테이블 생성.
-- Author		:	2014.11.05 
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/

BOOL CChData::CreateTable(BOOL bIncludeWorkingStep)
{
	CFileFind afinder;
	ResetData();
		
	if (afinder.FindFile(m_strPath + "\\*." + PS_RESULT_FILE_NAME_EXT))
	{		
		return CreateDataTable(bIncludeWorkingStep);
	}
			
	return TRUE;
}

/*
	Name:    Create
	Comment: 생성자에서 지정된 path에서
	         1. "pattern file"을 열어 pattern object를 생성하고
			 2. "Table list file"을 열어 table object의 list를 만든다. 
	Input:   
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/

//bIncludeWorkingStep : 현재 진행중에 중단한 Step이 있을 경우 중지된 최종 정보를 포함할 것인지 여부 
BOOL CChData::Create(BOOL bIncludeWorkingStep)
{
	CFileFind afinder;
	ResetData();

	//
	// Step 1. Pattern Object를 만든다.
	ASSERT(m_pSchedule == NULL);

	//필요시(GetPattern()) Laoding 하도록 수정 2005/12/07 By KBH
/*	if(afinder.FindFile(m_strPath+"\\*.sch"))
	{
		afinder.FindNextFile();
		m_pSchedule = new CScheduleData;		
		if(!m_pSchedule->SetSchedule(afinder.GetFilePath()))
		{
			delete m_pSchedule;
			m_pSchedule = NULL;
			// Pattern object를 create 못하면 return FALSE.
			return FALSE;
		}
	}
	// Pattern file이 없으면 return FALSE.
	else return FALSE;
*/


	//
	// Step 2. Table object의 list를 만든다.
	//
	
	//New version of result file. Binary format
	//2006/8/4
	//	int nType = 0;
#ifdef _DEBUG
	{
		CString str = m_strPath + "\\*." + PS_RESULT_FILE_NAME_EXT ;

		WIN32_FIND_DATA FindFileData ;
		HANDLE hFind = FindFirstFile(str, &FindFileData);

		if (hFind == INVALID_HANDLE_VALUE) 
		{
			TRACE("Invalid File Handle. GetLastError reports %d\n", GetLastError ());
			
			return (0);
		} 
		else 
		{
			while (TRUE)
			{
				TRACE("filename = %s\n", FindFileData.cFileName) ;

				if (FindNextFile( hFind, &FindFileData ) == false )
					break;
			}       
			
			FindClose(hFind);
			
		}


		
		
		CFileFind finder ;
		
		BOOL bWorking = finder.FindFile(m_strPath + "\\*.*");
		while (bWorking)
		{
			bWorking = finder.FindNextFile();
			
			TRACE("filename = %s\n", finder.GetFilePath()) ;
			
		}
	}

#endif
	if (afinder.FindFile(m_strPath + "\\*." + PS_RESULT_FILE_NAME_EXT))
	{
//		return CreateA(bIncludeWorkingStep);
		return CreateA2(bIncludeWorkingStep);
	}

	if (afinder.FindFile(m_strPath + "\\*.rp$") && bIncludeWorkingStep)
	{
//		return CreateA(bIncludeWorkingStep);
		return CreateA2(bIncludeWorkingStep);
	}

	//Old version result file. Text format
	if (afinder.FindFile(m_strPath + "\\*.rpt"))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		// Table List File을 열어서
		CStdioFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CStringList strlist;
			CString strContent;
			while (afile.ReadString(strContent))
			{
				if(!strContent.IsEmpty())
				{
					strlist.AddTail(strContent);
				}
			}
			afile.Close();

			// 내용이 2줄 이상이면 Table이 생성되어 있는 것임
			if (strlist.GetCount()>2)
			{
				// 읽은 내용에서
				POSITION pos = strlist.GetHeadPosition();
				// 첫번째는 StartT= ,EndT=, Serial=,	기록
				strContent = strlist.GetNext(pos);
				//Start Time and End Time Parsing
				int p1, p2, s;
				// Start Time 정보 추출 
				s  = strContent.Find("StartT");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_strStartT = strContent.Mid(p1+1,p2-p1-1);
				
				// End Time 정보 추출 
				s  = strContent.Find("EndT");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_strEndT = strContent.Mid(p1+1,p2-p1-1);
				
				// Test Serial 정보 추출 
				s  = strContent.Find("Serial");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_strSerial = strContent.Mid(p1+1,p2-p1-1);
				
				// UserID 정보 추출 
				s  = strContent.Find("User");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_strUserID = strContent.Mid(p1+1,p2-p1-1);
				
				// Comment 정보 추출 
				s  = strContent.Find("Descript");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_strDescript = strContent.Mid(p1+1,p2-p1-1);

				//2번째 Row는 Title 기록 
				CString strTitle = strlist.GetNext(pos);		//Title
				// 한줄씩
				while(pos)
				{
					// Table을 만든다.
					strContent = strlist.GetNext(pos);
//					LONG lIndex = atoi(strContent.Left(strContent.Find(',')));	//Step No
					CTable* pTable = new CTable(strTitle,strContent,this); //2014.08.14
				

					m_nTotalRecordCount += pTable->GetRecordCount();
					//Step번호가 중복되더라도 표시하도록 수정 //2005/8/17

					// 같은 인덱스에 대한 중복된 기록이 아닌 경우에는
//					if(IndexList.Find(lIndex)==NULL)
//					{
						// 생성한 Table object를 리스트에 넣고
						m_TableList.AddTail(pTable);
//						IndexList.AddTail(lIndex);
//					}
					// 같은 인덱스에 대한 중복된 기록인 경우에는
/*					else
					{
						// 이미 리스트에 들어가 있는 Table object를 찾아 지우고
						POSITION TempPos = m_TableList.GetHeadPosition();
						while(TempPos)
						{
							POSITION PrePos = TempPos;
							if(m_TableList.GetNext(TempPos)->GetNo()==lIndex)
							{
								CTable* pTempTable = m_TableList.GetAt(PrePos);
								m_TableList.RemoveAt(PrePos);
								delete pTempTable;
								break;
							}
						}
						// 새로 만든 Table object를 리스트에 넣는다.
						m_TableList.AddTail(pTable);
					}
*/				}
			}
		}	// end of (afile.Open())
	}	// end of if (afinder.FindFile())

	afinder.Close();

	//2006/3/28
	//현재 최종 step임시 파일에서 최종 data(현재 진행중인 곳)를 loading 한다.
	//Step 번호 and TotCycle번호가 같거나 Index가 정상이며 추가 한다.
	if (afinder.FindFile(m_strPath + "\\*.rp$") && bIncludeWorkingStep)
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		// Table List File을 열어서
		CStdioFile afile;
		if(afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if(!strContent.IsEmpty()) strlist.AddTail(strContent);
			}
			afile.Close();

			// 내용이 2줄 이상이면 Table이 생성되어 있는 것임
			if(strlist.GetCount()>2)
			{
				// 첫번째는 StartT= ,EndT=, Serial=,	기록 에서 End Time 만 추출 

				POSITION pos = strlist.GetHeadPosition();

				// 첫번째는 StartT= ,EndT=, Serial=,	기록
				strContent = strlist.GetNext(pos);
				int p1, p2, s;
				
				CString strStartTime, strSerial, strUser, strDes;
					//Start Time and End Time Parsing
					// Start Time 정보 추출 
					s  = strContent.Find("StartT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strStartTime= strContent.Mid(p1+1,p2-p1-1);
					
					// End Time 정보 추출 
					s  = strContent.Find("EndT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					m_strEndT= strContent.Mid(p1+1,p2-p1-1);
					
					// Test Serial 정보 추출 
					s  = strContent.Find("Serial");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strSerial = strContent.Mid(p1+1,p2-p1-1);
					
					// UserID 정보 추출 
					s  = strContent.Find("User");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strUser = strContent.Mid(p1+1,p2-p1-1);
					
					// Comment 정보 추출 
					s  = strContent.Find("Descript");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strDes = strContent.Mid(p1+1,p2-p1-1);

				if(m_TableList.GetCount() < 1)
				{
					m_strStartT = strStartTime;
					m_strSerial = strSerial;
					m_strUserID = strUser;
					m_strDescript = strDes;
				}
				
				//2번째 Row는 Title 기록 
				CString strTitle =  strlist.GetNext(pos);		//Title
				
				// Table을 만든다.
				strContent = strlist.GetTail();
				
				CTable* pTable = new CTable(strTitle,strContent);
				//결과 data가 하나도 없거나(최초 step) step이나
				if(	m_TableList.GetCount() <= 0 )
				{
					m_TableList.AddTail(pTable);
					m_nTotalRecordCount += pTable->GetRecordCount();
				}
				else 
				{	//index가 정상인 경우  
					if(pTable->GetRecordCount() > 0)
					{
						CTable *pLastT = m_TableList.GetTail();						
						if(pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
						}
						else
						{
							delete pTable;
						}
					}
					else
					{
						delete pTable;
					}
				}
				LoadAuxTitle(m_strPath);
				LoadCANTitle(m_strPath);
			}
		}
	}

	if (m_TableList.GetCount() < 1)
	{
		return FALSE;
	}

	afinder.Close();
	TRACE("CChData::Create(): Table Size is %d\n", m_TableList.GetCount());
	//
	return TRUE;
}

#if (1 == 0)
//Binary 형태의 결과 file loading
//2006/8/3
BOOL CChData::CreateA(BOOL bIncludeWorkingStep)
{
	// Added by 224 (2014/02/02) 
	ASSERT(m_nFromCycle != -1 && m_nToCycle != -1) ;
	
	CFileFind afinder;
	ResetData();
	//
	// Step 1. Pattern Object를 만든다.
	ASSERT(m_pSchedule == NULL);
	
	//
	// Step 2. Table object의 list를 만든다.
//	#define PS_RESULT_FILE_NAME_EXT		"cts"
	if (afinder.FindFile(m_strPath + "\\*." + PS_RESULT_FILE_NAME_EXT))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		// Table List File을 열어서
		CFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
		{
			PS_TEST_RESULT_FILE_HEADER* pHeader = new PS_TEST_RESULT_FILE_HEADER;

			afile.Read(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
			if (pHeader->fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
			{
				//AfxMessageBox("CTSAnalyzerPro 버전이 안 맞습니다.\n 최신 CTSAnalyzerPro.exe를 실행해 주세요.");
				//AfxMessageBox("결과 데이터 File ID가 맞지 않습니다.\n CTSMonitorPro일 경우 Temp 폴더를 삭제 하세요.\n CTSAnalyzer일 경우 최신 CTSAnalyzerPro.exe를 실행해 주세요.");
				AfxMessageBox(Fun_FindMsg("ChData_CreateA_msg1","CHDATA"));//&&
				//afile.Close();
				//return FALSE;
			}

			m_wArrySaveItem.RemoveAll();
			for (int i = 0; i < pHeader->testHeader.nRecordSize && i<PS_MAX_ITEM_NUM; i++)
			{
				m_wArrySaveItem.Add(pHeader->testHeader.wRecordItem[i]);
			}

			m_strStartT    = pHeader->testHeader.szStartTime;
			m_strEndT	   = pHeader->testHeader.szEndTime;
			m_strSerial	   = pHeader->testHeader.szSerial;
			m_strUserID    = pHeader->testHeader.szUserID;
			m_strDescript  = pHeader->testHeader.szDescript;
			m_lFileVersion = pHeader->fileHeader.nFileVersion;

			// 한줄씩
			PS_STEP_END_RECORD*    pRecord  = new PS_STEP_END_RECORD;
			PS_STEP_END_RECORD_V8* pRecord8 = new PS_STEP_END_RECORD_V8;
			// Added by 224 (2014/01/23) : 그래프 자료 로딩 개선
			afile.Seek(sizeof(PS_STEP_END_RECORD)*(m_nFromCycle-1), SEEK_CUR) ;
			
			// 기존 코드에서는 영역에 상관없이 모든 데이터를 로드했다.
			if (m_lFileVersion < PS_FILE_VERSION)
			{
				// Commented by 224 (2014/01/23)
//				while (afile.Read(pRecord, sizeof(PS_STEP_END_RECORD_V8)) > 0)
				for (int nLoop = m_nFromCycle; nLoop < m_nToCycle+1; nLoop++)
				{
					if (afile.Read(pRecord, sizeof(PS_STEP_END_RECORD_V8)) > 0)
					{
						break ;
					}

					CTable* pTable = new CTable(&m_wArrySaveItem, pRecord);
					m_nTotalRecordCount += pTable->GetRecordCount();
					
					m_TableList.AddTail(pTable);
				}
			}
			else
			{
				// Commented by 224 (2014/01/23)
				// 기존 코드는 파일의 범위와 상관없이 레코드를 모두 읽는다.
				// 필요한 갯수만큼만 읽는 방법으로 수정

//				while (afile.Read(pRecord, sizeof(PS_STEP_END_RECORD)) > 0)
				for (int nLoop = m_nFromCycle; nLoop < m_nToCycle+1; nLoop++)
				{
					if (afile.Read(pRecord, sizeof(PS_STEP_END_RECORD)) <= 0)
					{
						break ;
					}

					pRecord->fCapacity = pRecord->fChargeAh + pRecord->fDisChargeAh;	//ljb20150326 add x축 cycle, y축 용량으로 그래프 그리기 문제 해결
					CTable* pTable = new CTable(&m_wArrySaveItem, pRecord);
// 					CTable* pTable2 = (CTable*)::VirtualAlloc(NULL,sizeof(*pTable),MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
// 					memcpy(pTable2,pTable,sizeof(*pTable));
					m_nTotalRecordCount += pTable->GetRecordCount();
					
					m_TableList.AddTail(pTable);
					TRACE("Table Size is %d\n", m_TableList.GetCount());
// 					delete pTable;
 //					pTable = NULL;
				}
			}

//Load Aux Last Data////////////////////////////////////////////////////////////////////////////
			LoadAuxTitle(m_strPath);
			if (m_TableList.GetCount() > 0)
			{
				POSITION pos = m_TableList.GetHeadPosition();
				if (pos)
				{					
					CFileFind auxFinder;
					if (auxFinder.FindFile(m_strPath + "\\*." + PS_AUX_RESULT_FILE_NAME_EXT))
					{
						auxFinder.FindNextFile();
						CString auxFileName = auxFinder.GetFilePath();
						CFile auxFile;
						if (auxFile.Open(auxFileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
						{
							PS_AUX_FILE_HEADER *pAuxHeader = new PS_AUX_FILE_HEADER;
							if (auxFile.Read(pAuxHeader, sizeof(PS_AUX_FILE_HEADER)))
							{
								if(pAuxHeader->fileHeader.nFileID !=PS_PNE_AUX_RESULT_FILE_ID)
								{
									auxFile.Close();
									return FALSE;
								}
							}
							ASSERT(pAuxHeader->auxHeader.nColumnCount > 0);

							float* pfBuff= new float[pAuxHeader->auxHeader.nColumnCount];
							while (auxFile.Read(pfBuff, sizeof(float) * pAuxHeader->auxHeader.nColumnCount))
							{
								if(pos == NULL)
								{
									break;
								}

								CTable * pTable = m_TableList.GetAt(pos);

								//if(pAuxHeader->fileHeader.nFileVersion < _PNE_AUX_FILE_VER)
								//{
								for (int i = 0 ; i < pAuxHeader->auxHeader.nColumnCount; i++)
								{
									if(m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_VOLTAGE)
									{
										pfBuff[i] = pfBuff[i] / 1000;
									}
									else if(m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_TEMPERATURE_TH)
									{
										pfBuff[i] = pfBuff[i] / 1000;
									}
									//ksj 20200116 : v1016 습도 센서 추가. 향후 단위 확인 필요.
									else if(m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_HUMIDITY)
									{
										pfBuff[i] = pfBuff[i] / 1000;
									}
								}
										
								//}
								pTable->SetAuxLastData(pfBuff, pAuxHeader->auxHeader.nColumnCount);
								
								m_TableList.GetNext(pos);								
							}

							TRACE("Save Aux Last Data => Count : %d\r\n", pAuxHeader->auxHeader.nColumnCount);
							delete pAuxHeader;
							delete [] pfBuff;
							auxFile.Close();
						}
					}
				}
			}
// end of Load Aux Last Data////////////////////////////////////////////////////////////////////////////
			
				
/////////////////////////////////////////////////////////////////////////////////////////////

//Load Can Last Data////////////////////////////////////////////////////////////////////////////
			LoadCANTitle(m_strPath);
			if (m_TableList.GetCount() > 0)
			{
				POSITION pos = m_TableList.GetHeadPosition();
				if (pos)
				{					
					CFileFind canFinder;
					if(canFinder.FindFile(m_strPath+"\\*."+PS_CAN_RESULT_FILE_NAME_EXT))
					{
						canFinder.FindNextFile();
						CString canFileName = canFinder.GetFilePath();
						CFile canFile;
						if(canFile.Open(canFileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
						{
							PS_CAN_FILE_HEADER *pCanHeader = new PS_CAN_FILE_HEADER;
							if(canFile.Read(pCanHeader, sizeof(PS_CAN_FILE_HEADER)))
							if(pCanHeader->fileHeader.nFileID !=PS_PNE_CAN_RESULT_FILE_ID)
							{
								canFile.Close();
								return FALSE;
							}
							ASSERT(pCanHeader->canHeader.nColumnCount > 0);
							CAN_VALUE * pBuff= new CAN_VALUE[pCanHeader->canHeader.nColumnCount];
							UINT lCount=0;
							while(canFile.Read(pBuff, sizeof(CAN_VALUE)*pCanHeader->canHeader.nColumnCount))
							{
								if(pos == NULL) break;
								CTable * pTable = m_TableList.GetAt(pos);
								pTable->SetCanLastData(pBuff, pCanHeader->canHeader.nColumnCount);
								TRACE("Set can Data ==== \n");
								m_TableList.GetNext(pos);								
								lCount++;
							}
							TRACE("Save CAN Last Data => Count : %d\r\n", pCanHeader->canHeader.nColumnCount);

							delete pCanHeader;
							delete [] pBuff;
							canFile.Close();
						}
					}
				}
			}
// end of Load Can Last Data////////////////////////////////////////////////////////////////////////////

			delete 	pHeader;
			delete	pRecord;
			delete	pRecord8;

			afile.Close();
		}
	}

	afinder.Close();
//	LoadAuxTitle(m_strPath);	//2014.08.08 잘못된 코드 
//	LoadCANTitle(m_strPath);	//2014.08.08 잘못된 코드
	//TRACE("Table Size is %d\n", m_TableList.GetCount());
	TRACE("Table Size is %d\n", m_TableList.GetCount());

	//2006/3/28
	//현재 최종 step임시 파일에서 최종 data(현재 진행중인 곳)를 loading 한다.
	//Step 번호 and TotCycle번호가 같거나 Index가 정상이며 추가 한다.
	if (afinder.FindFile(m_strPath+"\\*.rp$") && bIncludeWorkingStep)
	{
		m_lFileVersion = 4100;	//PS_FILE_VERSION;		//ljb 임시 20101227

		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();

		// Table List File을 열어서
		CStdioFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if (!strContent.IsEmpty())
				{
					strlist.AddTail(strContent);
				}
			}
			afile.Close();

			// 내용이 2줄 이상이면 Table이 생성되어 있는 것임
			if (strlist.GetCount() > 2)
			{
				// 첫번째는 StartT= ,EndT=, Serial=,	기록 에서 End Time 만 추출 

				POSITION pos = strlist.GetHeadPosition();

				// 첫번째는 StartT= ,EndT=, Serial=,	기록
				strContent = strlist.GetNext(pos);
				int p1, p2, s;
				
				CString strStartTime, strSerial, strUser, strDes;
					//Start Time and End Time Parsing
					// Start Time 정보 추출 
					s  = strContent.Find("StartT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strStartTime= strContent.Mid(p1+1,p2-p1-1);
					
					// End Time 정보 추출 
					s  = strContent.Find("EndT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					//End time 갱신
					m_strEndT= strContent.Mid(p1+1,p2-p1-1);
					
					// Test Serial 정보 추출 
					s  = strContent.Find("Serial");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strSerial = strContent.Mid(p1+1,p2-p1-1);
					
					// UserID 정보 추출 
					s  = strContent.Find("User");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strUser = strContent.Mid(p1+1,p2-p1-1);
					
					// Comment 정보 추출 
					s  = strContent.Find("Descript");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strDes = strContent.Mid(p1+1,p2-p1-1);

				if (m_TableList.GetCount() < 1)
				{
					m_strStartT   = strStartTime;
					m_strSerial   = strSerial;
					m_strUserID   = strUser;
					m_strDescript = strDes;
				}
				
				//2번째 Row는 Title 기록 
				CString strTitle =  strlist.GetNext(pos);		//Title
				
				// Table을 만든다.
				strContent = strlist.GetTail();
				
				CTable* pTable = new CTable(strTitle,strContent);
				// Added by 224 (2014/01/28) : 메모리 리크 수정
//				m_TableList.AddTail(pTable) ; - 아래 코드에서 판단하는 곳 있음.
				TRACE("Title : %s \n", strTitle);

				//Load Aux Last/////////////////////////////////////////////////////////
				CFileFind auxFinder;

				// Added by 224 (2013/12/29) : 분할 파일의 처리
				if (auxFinder.FindFile(m_strPath + "\\*." + PS_AUX_FILE_NAME_EXT + "*"))
				{
					LoadAuxTitle(m_strPath);
					CString strPath;					

/*
// Commented by 224 (2013/12/29)
					auxFinder.FindNextFile();
					strPath = auxFinder.GetFilePath();
*/
					// Commented by 224 (2013/12/29) :
					// 분할된 맨 마지막의 파일을 얻는다.
					BOOL bContinue = TRUE ;
					while (bContinue)
					{
						bContinue = auxFinder.FindNextFile() ;
						strPath = auxFinder.GetFilePath() ;
					} ;
					
					// Data를 읽어들임
					FILE* fp = fopen(strPath, "rb");
					if (fp == NULL)		return 0;

					// File Header를 읽음
					PS_FILE_ID_HEADER fileID;
					if (fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
					{
						fclose(fp);
						return 0;
					}
					if (fileID.nFileID != PS_PNE_AUX_FILE_ID)
					{
						fclose(fp);
						return 0;
					}
					

					// Commented by 224 (2013/12/29) :
// 					PS_AUX_DATA_FILE_HEADER* pAuxHeader = new PS_AUX_DATA_FILE_HEADER;
// 					ZeroMemory(pAuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER));
					PS_AUX_DATA_FILE_HEADER AuxHeader ;
					ZeroMemory(&AuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER));

					//File Header를 읽음
					if (fread(&AuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER), 1, fp) < 1)	
					{
// 						delete pAuxHeader;
						fclose(fp);
						return 0;
					}

					int nColAuxCount = AuxHeader.nColumnCount;
//					int nColAuxCount = pAuxHeader->nColumnCount;
//					delete pAuxHeader;

					if (nColAuxCount > 0)
					{
						int nHeaderSize = sizeof(PS_FILE_ID_HEADER) + sizeof(PS_AUX_DATA_FILE_HEADER);
						//저장된 Data의 크기 
						size_t nRecordSize = sizeof(float)*nColAuxCount;

						//주어진 Index로 이동한다.
						fseek(fp, nRecordSize+nHeaderSize, SEEK_SET);

//----------------------------------------
#ifdef _DEBUG
						{
							int nAuxFileSize = filelength(fileno(fp));		
							ASSERT(nAuxFileSize >= sizeof(PS_AUX_FILE_HEADER)) ;
							
							// . 파일에 포함된 레코드는 최소 1개 이상이어야 한다.
							int nAuxRecordSize = sizeof(float) * AuxHeader.nColumnCount;
							int nFileNumOfRecord = (nAuxRecordSize == 0) ? 0 : (nAuxFileSize - sizeof(PS_AUX_FILE_HEADER)) / nAuxRecordSize ;
							ASSERT(nFileNumOfRecord > 1) ;
						}
#endif
//----------------------------------------
						
						float *fpBuff = new float[nColAuxCount];
						ZeroMemory(fpBuff, sizeof(float)*nColAuxCount);
						int rsCount = 0;

						// 224 (2013/12/29) : 맨 마지막의 레코드를 읽으려는 의도?
						// 왜 파일을 모두 읽지? fseek 해서 맨 마지막에서 한 레코드만 가져오면 되지 않나?
						// Added by 224 (2013/12/29)
						fseek(fp, -nRecordSize, SEEK_END) ;

						while (fread(fpBuff, nRecordSize, 1, fp) > 0)
						{	
							rsCount++;
						}

						pTable->SetAuxLastData(fpBuff, nColAuxCount);
						delete [] fpBuff;
					}

					fclose(fp);
				}
				//End Load Aux Last/////////////////////////////////////////////////////////

				//Load CAN Last/////////////////////////////////////////////////////////
				CFileFind canFinder;
				if (canFinder.FindFile(m_strPath + "\\*." + PS_CAN_FILE_NAME_EXT + "*"))
				{
					LoadCANTitle(m_strPath);
					CString strPath;
					
/*
// Commented by 224 (2013/12/29)
					canFinder.FindNextFile();
					strPath = canFinder.GetFilePath();
*/
					
					// Commented by 224 (2013/12/29) :
					// 분할된 맨 마지막의 파일을 얻는다.
					BOOL bContinue = TRUE ;
					while (bContinue)
					{
						bContinue = canFinder.FindNextFile() ;
						strPath = canFinder.GetFilePath() ;
					} ;

					//Data를 읽어들임
					FILE *fp = fopen(strPath, "rb");
					if(fp == NULL)		return 0;

					//File Header를 읽음
					PS_FILE_ID_HEADER fileID;
					if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
					{
						fclose(fp);
						return 0;
					}
					if (fileID.nFileID != PS_PNE_CAN_FILE_ID)
					{
						fclose(fp);
						return 0;
					}
					

					// Commented by 224 (2013/12/29) :
//					PS_CAN_DATA_FILE_HEADER* pCANHeader = new PS_CAN_DATA_FILE_HEADER;
//					ZeroMemory(pCANHeader, sizeof(PS_CAN_DATA_FILE_HEADER));
					PS_CAN_DATA_FILE_HEADER CanHeader ;
					ZeroMemory(&CanHeader, sizeof(PS_CAN_DATA_FILE_HEADER));

					//File Header를 읽음
					if (fread(&CanHeader, sizeof(PS_CAN_DATA_FILE_HEADER), 1, fp) < 1)	
					{
//						delete pCANHeader;
						fclose(fp);
						return 0;
					}

					int nColCANCount = CanHeader.nColumnCount;
//					delete pCANHeader;

					if (nColCANCount > 0)
					{
						int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_CAN_DATA_FILE_HEADER);
						//저장된 Data의 크기 
						size_t nRecordSize = sizeof(CAN_VALUE) * nColCANCount;

						//주어진 Index로 이동한다.
						fseek(fp, nRecordSize + nHeaderSize, SEEK_SET);
//----------------------------------------
#ifdef _DEBUG
						{
							int nCanFileSize = filelength(fileno(fp));		
							ASSERT(nCanFileSize >= sizeof(PS_CAN_FILE_HEADER)) ;
							
							// . 파일에 포함된 레코드는 최소 1개 이상이어야 한다.
							int nCanRecordSize = sizeof(CAN_VALUE) * CanHeader.nColumnCount;
							int nFileNumOfRecord = (nCanRecordSize == 0) ? 0 : (nCanFileSize - sizeof(PS_CAN_FILE_HEADER)) / nCanRecordSize ;
							ASSERT(nFileNumOfRecord > 1) ;
						}
#endif
//----------------------------------------
						
						CAN_VALUE* pBuff = new CAN_VALUE[nColCANCount];
						ZeroMemory(pBuff, sizeof(CAN_VALUE) * nColCANCount);
						int rsCount = 0;

						// 224 (2013/12/29) : 맨 마지막의 레코드를 읽으려는 의도?
						// 왜 파일을 모두 읽지? fseek 해서 맨 마지막에서 한 레코드만 가져오면 되지 않나?
						// Added by 224 (2013/12/29)
						fseek(fp, -nRecordSize, SEEK_END) ;

						while (fread(pBuff, nRecordSize, 1, fp) > 0)
						{	
							rsCount++;
						}

						pTable->SetCanLastData(pBuff, nColCANCount);
						delete [] pBuff;
					}

					
					fclose(fp);

				}
				//End Load CAN Last/////////////////////////////////////////////////////////
				
				//결과 data가 하나도 없거나(최초 step) step이나
				if (m_TableList.GetCount() <= 0)
				{
					m_TableList.AddTail(pTable);
					m_nTotalRecordCount += pTable->GetRecordCount();
				}
				else 
				{
					//index가 정상인 경우  
					if (pTable->GetRecordCount() > 0)
					{
						CTable* pLastT = m_TableList.GetTail();	

						//20081103
						//같은 Step의 결과 Data가 여러번 올라오더라도 1개의 Step으로 Display 되도록 수정 
						if (pLastT->GetStepNo() == pTable->GetStepNo() && pLastT->GetCycleNo() == pTable->GetCycleNo()
							&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							pTable->EditRecordIndex( pLastT->GetRecordIndex(), pTable->GetLastRecordIndex());
						
							m_TableList.RemoveTail();
							delete pLastT;
							
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
						}
// 						else
// 						{
// 							delete pTable;
// 						}
						//20121228
						//Step END가 아닌 rp$ 이면 마지막 테이블에 붙인다
						else if(pLastT->GetStepNo() != pTable->GetStepNo() //&& pLastT->GetCycleNo() != pTable->GetCycleNo()
							&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
							afinder.Close();
							TRACE("Table Size is %d\n", m_TableList.GetCount());
							return TRUE;
						}
						else
						{
							delete pTable;
						}
					}
// 					else
// 					{
// 						delete pTable;
// 					}
				}
			}
		}
	}

	if (m_TableList.GetCount() < 1)
	{
		return FALSE;
	}

	afinder.Close();
	TRACE("CChData::CreateA(): Table Size is %d\n", m_TableList.GetCount());
	
	return TRUE;
}
#endif 

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	CreateA2 손실 데이터 검색시 동일하게 사용되 검색이 
                    올바르게 되지 않는 문제 수정.
					stepend 테이블을 만든다. 					
-- Author		:	2014.11.05 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CChData::CreateDataTable(BOOL bIncludeWorkingStep)
{	
	CFileFind afinder;
	FILE* fp;
	ResetData();

	// Step 1. Table object의 list를 만든다.
	if (afinder.FindFile(m_strPath + "\\*." + PS_RESULT_FILE_NAME_EXT))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();

		CFile afile;

		//2014.10.15  m_nTo,m_nFrom 없을시 기본값을 설정해준다.
		fp = fopen( fileName, "rb");

		long FileSize = filelength( fileno(fp) );
		int iRawSize = sizeof(PS_TEST_RESULT_FILE_HEADER);
		int nTableCount = (FileSize-iRawSize) / sizeof(PS_STEP_END_RECORD) ;
		
		if(m_nTo < 0){
			m_nTo = nTableCount;
		}
		if(m_nFrom < 0){
			m_nFrom = 1;
		}
		
		fclose(fp);

		//1.1 cts 스텝 데이터 파일을 읽음
		if (afile.Open(fileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
		{						
			PS_TEST_RESULT_FILE_HEADER header ;
			
			afile.Read(&header, sizeof(PS_TEST_RESULT_FILE_HEADER));
			if (header.fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
			{				
				//AfxMessageBox("결과 데이터 File ID가 맞지 않습니다.\n CTSMonitorPro일 경우 Temp 폴더를 삭제 하세요.\n CTSAnalyzer일 경우 최신 CTSAnalyzerPro.exe를 실행해 주세요.");
				AfxMessageBox(Fun_FindMsg("ChData_CreateA_msg1","CHDATA"));//&&
				afile.Close();
				return FALSE;
			}
			
			//스텝 해더 값을 저장.
			m_wArrySaveItem.RemoveAll();
			for (int i = 0; i < header.testHeader.nRecordSize && i<PS_MAX_ITEM_NUM; i++)
			{
				m_wArrySaveItem.Add(header.testHeader.wRecordItem[i]);
			}
			
			// Monitoring 에서 A버전인 경우 nRecordSize 가 12인경우
			// 엑셀파일로 변환 시에 마지막 StepEnd 값에 채워지지 않는 
			// 현상이 있음. 임의로 컬럼을 추가
			m_wArrySaveItem.Add(PS_CHARGE_CAP);		//ljb 20140204 add
			m_wArrySaveItem.Add(PS_DISCHARGE_CAP);	
			m_wArrySaveItem.Add(PS_CAPACITANCE);	
			m_wArrySaveItem.Add(PS_CHARGE_WH);		
			m_wArrySaveItem.Add(PS_DISCHARGE_WH);	
			
			m_strStartT    = header.testHeader.szStartTime;
			m_strEndT	   = header.testHeader.szEndTime;
			m_strSerial	   = header.testHeader.szSerial;
			m_strUserID    = header.testHeader.szUserID;
			m_strDescript  = header.testHeader.szDescript;
			m_lFileVersion = header.fileHeader.nFileVersion;
			
			// 한줄씩
			PS_STEP_END_RECORD rec ;
			
			// 기존 코드는 파일의 범위와 상관없이 레코드를 모두 읽는다.
			// 필요한 갯수만큼만 읽는 방법으로 수정
			for (int nLoop = m_nFrom; nLoop < m_nTo+1; nLoop++)
			{
				if (afile.Read(&rec, sizeof(PS_STEP_END_RECORD)) <= 0)
				{
					break ;
				}
				
				CTable* pTable = new CTable(&m_wArrySaveItem, &rec, this);
				m_nTotalRecordCount += pTable->GetRecordCount();
				
				m_TableList.AddTail(pTable);				
			}
			
			afile.Close();
		}
	}

	afinder.Close();
	TRACE("Table Size is %d\n", m_TableList.GetCount());
	
	//2. 임시 파일 스탭엔드 데이터를 가져온다.
	//현재 최종 step임시 파일에서 최종 data(현재 진행중인 곳)를 loading 한다.
	//Step 번호 and TotCycle번호가 같거나 Index가 정상이며 추가 한다.
	if (afinder.FindFile(m_strPath+"\\*.rp$") && bIncludeWorkingStep)
	{				
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();

		// Table List File을 열어서
		CStdioFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if (!strContent.IsEmpty())
				{
					strlist.AddTail(strContent);
				}
			}
			afile.Close();

			// 내용이 2줄 이상이면 Table이 생성되어 있는 것임
			if (strlist.GetCount() > 2)
			{
				
				POSITION pos = strlist.GetHeadPosition();
				// 첫번째는 StartT= ,EndT=, Serial=,
				strContent = strlist.GetNext(pos);												
				
				//2번째 Row는 Title 기록 
				CString strTitle =  strlist.GetNext(pos);		//Title
				
				// Table을 만든다.
				strContent = strlist.GetTail();
				
				CTable* pTable = new CTable(strTitle,strContent, this);
				
				// 제거... 아래 코드에서 delete 할 수 있음
				TRACE("Title : %s \n", strTitle);
				
				//결과 data가 하나도 없거나(최초 step) step이나
				if (m_TableList.GetCount() <= 0)
				{
					m_TableList.AddTail(pTable);
					m_nTotalRecordCount += pTable->GetRecordCount();
				}
				else 
				{
					//index가 정상인 경우  
					if (pTable->GetRecordCount() > 0)
					{
						CTable* pLastT = m_TableList.GetTail();							
						//같은 Step의 결과 Data가 여러번 올라오더라도 1개의 Step으로 Display 되도록 수정 
						if (pLastT->GetStepNo() == pTable->GetStepNo() && pLastT->GetCycleNo() == pTable->GetCycleNo()
							&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							pTable->EditRecordIndex( pLastT->GetRecordIndex(), pTable->GetLastRecordIndex());
						
							m_TableList.RemoveTail();
							delete pLastT;
							
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
						}						
						//Step END가 아닌 rp$ 이면 마지막 테이블에 붙인다
						else if(pLastT->GetStepNo() != pTable->GetStepNo() 
							&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
							afinder.Close();
							
							return TRUE;
						}
						else
						{							
							// 메모리 리크나는 경우 있었음. 재현 (2014/02/07)에 재현 불가능
							delete pTable;
						}
					}
					else
					{												
						// 기존 주석처리 풀음
						delete pTable;
					}
				}
			}
		}
	}

	if (m_TableList.GetCount() < 1)
	{
		return FALSE;
	}

	afinder.Close();
	TRACE("CChData::CreateA(): Table Size is %d\n", m_TableList.GetCount());

	return TRUE;
}

// Added by 224 (2014/02/02) :
BOOL CChData::CreateA2(BOOL bIncludeWorkingStep)
{
	// Added by 224 (2014/02/02) 
	ASSERT(m_nFrom != -1 && m_nTo != -1) ;

	CFileFind afinder;
	FILE* fp;
	ResetData();

	// Step 1. Pattern Object를 만든다.
	ASSERT(m_pSchedule == NULL);

	// Step 2. Table object의 list를 만든다.
//	#define PS_RESULT_FILE_NAME_EXT		"cts"
	if (afinder.FindFile(m_strPath + "\\*." + PS_RESULT_FILE_NAME_EXT))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();

		CFile afile;

		//2014.10.15  m_nTo,m_nFrom 없을시 기본값을 설정해준다.
		fp = fopen( fileName, "rb");

		long FileSize = filelength( fileno(fp) );
		int iRawSize = sizeof(PS_TEST_RESULT_FILE_HEADER);
		int nTableCount = (FileSize-iRawSize) / sizeof(PS_STEP_END_RECORD) ;
		
		if(m_nTo < 0){
			m_nTo = nTableCount;
		}
		if(m_nFrom < 0){
			m_nFrom = 1;
		}
		
		fclose(fp);

		// Table List File을 열어서
		
		if (afile.Open(fileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
		{
			
			
//			PS_TEST_RESULT_FILE_HEADER* pHeader = new PS_TEST_RESULT_FILE_HEADER;
			PS_TEST_RESULT_FILE_HEADER header ;
			
			afile.Read(&header, sizeof(PS_TEST_RESULT_FILE_HEADER));
			if (header.fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
			{
				//AfxMessageBox("CTSAnalyzerPro 버전이 안 맞습니다.\n 최신 CTSAnalyzerPro.exe를 실행해 주세요.");
				//AfxMessageBox("결과 데이터 File ID가 맞지 않습니다.\n CTSMonitorPro일 경우 Temp 폴더를 삭제 하세요.\n CTSAnalyzer일 경우 최신 CTSAnalyzerPro.exe를 실행해 주세요.");
				AfxMessageBox(Fun_FindMsg("ChData_CreateA_msg1","CHDATA"));//&&
				//afile.Close();
				//return FALSE;
			}

			m_wArrySaveItem.RemoveAll();
			for (int i = 0; i < header.testHeader.nRecordSize && i<PS_MAX_ITEM_NUM; i++)
			{
				m_wArrySaveItem.Add(header.testHeader.wRecordItem[i]);
			}

//----------------------------------------------------------------------
			// Monitoring 에서 A버전인 경우 nRecordSize 가 12인경우
			// 엑셀파일로 변환 시에 마지막 StepEnd 값에 채워지지 않는 
			// 현상이 있음. 임의로 컬럼을 추가
			m_wArrySaveItem.Add(PS_CHARGE_CAP);		//ljb 20140204 add
			m_wArrySaveItem.Add(PS_DISCHARGE_CAP);	//ljb 20140204 add
			m_wArrySaveItem.Add(PS_CAPACITANCE);	//ljb 20140204 add
			m_wArrySaveItem.Add(PS_CHARGE_WH);		//ljb 20140204 add
			m_wArrySaveItem.Add(PS_DISCHARGE_WH);	//ljb 20140204 add
//----------------------------------------------------------------------
			m_wArrySaveItem.Add(PS_LOAD_VOLT);	//ljb 20150513 add
			m_wArrySaveItem.Add(PS_LOAD_CURR);	//ljb 20150513 add

			m_strStartT    = header.testHeader.szStartTime;
			m_strEndT	   = header.testHeader.szEndTime;
			m_strSerial	   = header.testHeader.szSerial;
			m_strUserID    = header.testHeader.szUserID;
			m_strDescript  = header.testHeader.szDescript;
			m_lFileVersion = header.fileHeader.nFileVersion;

			// 한줄씩
// 			PS_STEP_END_RECORD*    pRecord  = new PS_STEP_END_RECORD;
// 			PS_STEP_END_RECORD_V8* pRecord8 = new PS_STEP_END_RECORD_V8;
			PS_STEP_END_RECORD rec ;
//			PS_STEP_END_RECORD_V8 recV8 ;

			// Added by 224 (2014/01/23) : 그래프 자료 로딩 개선
			afile.Seek(sizeof(PS_STEP_END_RECORD)*(m_nFrom-1), SEEK_CUR) ;						
			// 기존 코드에서는 영역에 상관없이 모든 데이터를 로드했다.
			if (m_lFileVersion < PS_FILE_VERSION)
			{
				// Added by 224 (2014/01/23) : 그래프 자료 로딩 개선
				//afile.Seek(sizeof(PS_STEP_END_RECORD_V8)*(m_nFrom-1), SEEK_CUR) ;

				// Commented by 224 (2014/01/23)
//				while (afile.Read(pRecord, sizeof(PS_STEP_END_RECORD_V8)) > 0)
				for (int nLoop = m_nFrom ; nLoop < m_nTo+1; nLoop++)
				{
					if (afile.Read(&rec, sizeof(PS_STEP_END_RECORD_V8)) > 0)
					{
						break ;
					}

					CTable* pTable = new CTable(&m_wArrySaveItem, &rec, this);
					m_nTotalRecordCount += pTable->GetRecordCount();
									
					m_TableList.AddTail(pTable);
				}
			}
			else
			{
				// Commented by 224 (2014/01/23)
				// 기존 코드는 파일의 범위와 상관없이 레코드를 모두 읽는다.
				// 필요한 갯수만큼만 읽는 방법으로 수정

				//afile.Seek(sizeof(PS_STEP_END_RECORD)*(m_nFrom-1), SEEK_CUR) ;

				//while (afile.Read(pRecord, sizeof(PS_STEP_END_RECORD)) > 0)
				for (int nLoop = m_nFrom; nLoop < m_nTo+1; nLoop++)
				{
					if (afile.Read(&rec, sizeof(PS_STEP_END_RECORD)) <= 0)
					{
						break ;
					}

					CTable* pTable = new CTable(&m_wArrySaveItem, &rec, this);
 					//CTable* pTable2 = (CTable*)::VirtualAlloc(NULL,sizeof(*pTable),MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
 					//memcpy(pTable2,pTable,sizeof(*pTable));
					m_nTotalRecordCount += pTable->GetRecordCount();
					
					m_TableList.AddTail(pTable);
					TRACE("Table Size is %d\n", m_TableList.GetCount());

 					//delete pTable;
					//pTable = NULL;
				}
			}

//Load Aux Last Data////////////////////////////////////////////////////////////////////////////
			LoadAuxTitle(m_strPath);
			if (m_TableList.GetCount() > 0)
			{
				POSITION pos = m_TableList.GetHeadPosition();
				if (pos)
				{					
					CFileFind auxFinder;
					if (auxFinder.FindFile(m_strPath + "\\*." + PS_AUX_RESULT_FILE_NAME_EXT))
					{
						auxFinder.FindNextFile();
						CString auxFileName = auxFinder.GetFilePath();
						CFile auxFile;
						if (auxFile.Open(auxFileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
						{
							PS_AUX_FILE_HEADER *pAuxHeader = new PS_AUX_FILE_HEADER;
							if (auxFile.Read(pAuxHeader, sizeof(PS_AUX_FILE_HEADER)))
							{
								if(pAuxHeader->fileHeader.nFileID !=PS_PNE_AUX_RESULT_FILE_ID)
								{
									auxFile.Close();
									return FALSE;
								}
							}
							ASSERT(pAuxHeader->auxHeader.nColumnCount > 0);

							float* pfBuff= new float[pAuxHeader->auxHeader.nColumnCount];
							while (auxFile.Read(pfBuff, sizeof(float) * pAuxHeader->auxHeader.nColumnCount))
							{
								if(pos == NULL)
								{
									break;
								}

								CTable * pTable = m_TableList.GetAt(pos);

								//if(pAuxHeader->fileHeader.nFileVersion < _PNE_AUX_FILE_VER)
								//{
								for (int i = 0 ; i < pAuxHeader->auxHeader.nColumnCount; i++)
								{
									if(m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_VOLTAGE)
									{
										pfBuff[i] = pfBuff[i] / 1000;
									}
									else if(m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_TEMPERATURE_TH)
									{
										pfBuff[i] = pfBuff[i] / 1000;
									}
									//ksj 20200116 : v1016 습도 추가. 단위 확인 필요.
									else if(m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_HUMIDITY)
									{
										pfBuff[i] = pfBuff[i] / 1000;
									}
								}
										
								//}
								pTable->SetAuxLastData(pfBuff, pAuxHeader->auxHeader.nColumnCount);
								
								m_TableList.GetNext(pos);								
							}

							TRACE("Save Aux Last Data => Count : %d\r\n", pAuxHeader->auxHeader.nColumnCount);
							delete pAuxHeader;
							delete [] pfBuff;
							auxFile.Close();
						}
					}
				}
			}
// end of Load Aux Last Data////////////////////////////////////////////////////////////////////////////
			
				
/////////////////////////////////////////////////////////////////////////////////////////////

//Load Can Last Data////////////////////////////////////////////////////////////////////////////
			LoadCANTitle(m_strPath);
			if (m_TableList.GetCount() > 0)
			{
				POSITION pos = m_TableList.GetHeadPosition();
				if (pos)
				{					
					CFileFind canFinder;
					if(canFinder.FindFile(m_strPath+"\\*."+PS_CAN_RESULT_FILE_NAME_EXT))
					{
						canFinder.FindNextFile();
						CString canFileName = canFinder.GetFilePath();
						CFile canFile;
						if(canFile.Open(canFileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
						{
							PS_CAN_FILE_HEADER *pCanHeader = new PS_CAN_FILE_HEADER;
							if(canFile.Read(pCanHeader, sizeof(PS_CAN_FILE_HEADER)))
							if(pCanHeader->fileHeader.nFileID !=PS_PNE_CAN_RESULT_FILE_ID)
							{
								canFile.Close();
								return FALSE;
							}
							ASSERT(pCanHeader->canHeader.nColumnCount > 0);
							CAN_VALUE * pBuff= new CAN_VALUE[pCanHeader->canHeader.nColumnCount];
							UINT lCount=0;
							while(canFile.Read(pBuff, sizeof(CAN_VALUE)*pCanHeader->canHeader.nColumnCount))
							{
								if(pos == NULL) break;
								CTable * pTable = m_TableList.GetAt(pos);
								pTable->SetCanLastData(pBuff, pCanHeader->canHeader.nColumnCount);
								TRACE("Set can Data ==== \n");
								m_TableList.GetNext(pos);								
								lCount++;
							}
							TRACE("Save CAN Last Data => Count : %d\r\n", pCanHeader->canHeader.nColumnCount);

							delete pCanHeader;
							delete [] pBuff;
							canFile.Close();
						}
					}
				}
			}
// end of Load Can Last Data////////////////////////////////////////////////////////////////////////////

//			delete 	pHeader;
//			delete	pRecord;
//			delete	pRecord8;

			afile.Close();
		}
	}

	afinder.Close();
//	LoadAuxTitle(m_strPath);
//	LoadCANTitle(m_strPath);
	//TRACE("Table Size is %d\n", m_TableList.GetCount());
	TRACE("Table Size is %d\n", m_TableList.GetCount());
	//2006/3/28
	//현재 최종 step임시 파일에서 최종 data(현재 진행중인 곳)를 loading 한다.
	//Step 번호 and TotCycle번호가 같거나 Index가 정상이며 추가 한다.
	if (afinder.FindFile(m_strPath+"\\*.rp$") && bIncludeWorkingStep)
	{
		//m_lFileVersion = 4100;	//PS_FILE_VERSION;		//ljb 임시 20101227

		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();

		// Table List File을 열어서
		CStdioFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if (!strContent.IsEmpty())
				{
					strlist.AddTail(strContent);
				}
			}
			afile.Close();

			// 내용이 2줄 이상이면 Table이 생성되어 있는 것임
			if (strlist.GetCount() > 2)
			{
				// 첫번째는 StartT= ,EndT=, Serial=,	기록 에서 End Time 만 추출 

				POSITION pos = strlist.GetHeadPosition();

				// 첫번째는 StartT= ,EndT=, Serial=,	기록
				strContent = strlist.GetNext(pos);
				int p1, p2, s;
				
				CString strStartTime, strSerial, strUser, strDes;
					//Start Time and End Time Parsing
					// Start Time 정보 추출 
					s  = strContent.Find("StartT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strStartTime= strContent.Mid(p1+1,p2-p1-1);
					
					// End Time 정보 추출 
					s  = strContent.Find("EndT");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					//End time 갱신
					m_strEndT= strContent.Mid(p1+1,p2-p1-1);
					
					// Test Serial 정보 추출 
					s  = strContent.Find("Serial");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strSerial = strContent.Mid(p1+1,p2-p1-1);
					
					// UserID 정보 추출 
					s  = strContent.Find("User");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strUser = strContent.Mid(p1+1,p2-p1-1);
					
					// Comment 정보 추출 
					s  = strContent.Find("Descript");
					p1 = strContent.Find('=', s);
					p2 = strContent.Find(',', s);
					strDes = strContent.Mid(p1+1,p2-p1-1);

				if (m_TableList.GetCount() < 1)
				{
					m_strStartT   = strStartTime;
					m_strSerial   = strSerial;
					m_strUserID   = strUser;
					m_strDescript = strDes;
				}
				
				//2번째 Row는 Title 기록 
				CString strTitle =  strlist.GetNext(pos);		//Title
				
				// Table을 만든다.
				strContent = strlist.GetTail();
				
				CTable* pTable = new CTable(strTitle,strContent, this);

				// Added by 224 (2014/01/28) : 메모리 리크 수정
				// 제거... 아래 코드에서 delete 할 수 있음
//				m_TableList.AddTail(pTable) ;
				TRACE("Title : %s \n", strTitle);

				//Load Aux Last/////////////////////////////////////////////////////////
				CFileFind auxFinder;

				// Added by 224 (2013/12/29) : 분할 파일의 처리
				if (auxFinder.FindFile(m_strPath + "\\*." + PS_AUX_FILE_NAME_EXT + "*"))
				{
					LoadAuxTitle(m_strPath);
					CString strPath;					

/*
// Commented by 224 (2013/12/29)
					auxFinder.FindNextFile();
					strPath = auxFinder.GetFilePath();
*/
					// Commented by 224 (2013/12/29) :
					// 분할된 맨 마지막의 파일을 얻는다.
					BOOL bContinue = TRUE ;
					while (bContinue)
					{
						bContinue = auxFinder.FindNextFile() ;
						//if(auxFinder.GetFilePath().Find('.AUX')>0) continue; //lyj 20201109 작업명끝에 '.' 있을 경우 //ksj 20201120 : 주석처리함. aux 저장이안됨.	
						strPath = auxFinder.GetFilePath() ;
					} ;
					
					// Data를 읽어들임
					FILE* fp = fopen(strPath, "rb");
					if (fp == NULL)		return 0;

					// File Header를 읽음
					PS_FILE_ID_HEADER fileID;
					if (fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
					{
						fclose(fp);
						return 0;
					}
					if (fileID.nFileID != PS_PNE_AUX_FILE_ID)
					{
						fclose(fp);
						return 0;
					}
					

					// Commented by 224 (2013/12/29) :
// 					PS_AUX_DATA_FILE_HEADER* pAuxHeader = new PS_AUX_DATA_FILE_HEADER;
// 					ZeroMemory(pAuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER));
					PS_AUX_DATA_FILE_HEADER AuxHeader ;
					ZeroMemory(&AuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER));

					//File Header를 읽음
					if (fread(&AuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER), 1, fp) < 1)	
					{
// 						delete pAuxHeader;
						fclose(fp);
						return 0;
					}

					int nColAuxCount = AuxHeader.nColumnCount;
//					int nColAuxCount = pAuxHeader->nColumnCount;
//					delete pAuxHeader;

					if (nColAuxCount > 0)
					{
						int nHeaderSize = sizeof(PS_FILE_ID_HEADER) + sizeof(PS_AUX_DATA_FILE_HEADER);
						//저장된 Data의 크기 
						size_t nRecordSize = sizeof(float)*nColAuxCount;

						//주어진 Index로 이동한다.
						fseek(fp, nRecordSize+nHeaderSize, SEEK_SET);

//----------------------------------------
#ifdef _DEBUG
						{
							int nAuxFileSize = filelength(fileno(fp));		
							ASSERT(nAuxFileSize >= sizeof(PS_AUX_FILE_HEADER)) ;
							
							// . 파일에 포함된 레코드는 최소 1개 이상이어야 한다.
							int nAuxRecordSize = sizeof(float) * AuxHeader.nColumnCount;
							int nFileNumOfRecord = (nAuxRecordSize == 0) ? 0 : (nAuxFileSize - sizeof(PS_AUX_FILE_HEADER)) / nAuxRecordSize ;
							//ASSERT(nFileNumOfRecord > 1) ;
						}
#endif
//----------------------------------------
						
						float *fpBuff = new float[nColAuxCount];
						ZeroMemory(fpBuff, sizeof(float)*nColAuxCount);
						int rsCount = 0;

						// 224 (2013/12/29) : 맨 마지막의 레코드를 읽으려는 의도?
						// 왜 파일을 모두 읽지? fseek 해서 맨 마지막에서 한 레코드만 가져오면 되지 않나?
						// Added by 224 (2013/12/29)
						fseek(fp, -nRecordSize, SEEK_END) ;

						while (fread(fpBuff, nRecordSize, 1, fp) > 0)
						{	
							rsCount++;
						}

						pTable->SetAuxLastData(fpBuff, nColAuxCount);
						delete [] fpBuff;
					}

					fclose(fp);
				}
				//End Load Aux Last/////////////////////////////////////////////////////////

				//Load CAN Last/////////////////////////////////////////////////////////
				CFileFind canFinder;
				if (canFinder.FindFile(m_strPath + "\\*." + PS_CAN_FILE_NAME_EXT + "*"))
				{
					LoadCANTitle(m_strPath);
					CString strPath;
					
/*
// Commented by 224 (2013/12/29)
					canFinder.FindNextFile();
					strPath = canFinder.GetFilePath();
*/
					
					// Commented by 224 (2013/12/29) :
					// 분할된 맨 마지막의 파일을 얻는다.
					BOOL bContinue = TRUE ;
					while (bContinue)
					{
						bContinue = canFinder.FindNextFile() ;
						//if(canFinder.GetFilePath().Find('.CAN')>0) continue; //lyj 20201109 작업명끝에 '.' 있을 경우  //ksj 20201120 : 주석처리함. can 저장이안됨.	
						strPath = canFinder.GetFilePath() ;
					} ;

					//Data를 읽어들임
					FILE *fp = fopen(strPath, "rb");
					if(fp == NULL)		return 0;

					//File Header를 읽음
					PS_FILE_ID_HEADER fileID;
					if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
					{
						fclose(fp);
						return 0;
					}
					if (fileID.nFileID != PS_PNE_CAN_FILE_ID)
					{
						fclose(fp);
						return 0;
					}
					

					// Commented by 224 (2013/12/29) :
//					PS_CAN_DATA_FILE_HEADER* pCANHeader = new PS_CAN_DATA_FILE_HEADER;
//					ZeroMemory(pCANHeader, sizeof(PS_CAN_DATA_FILE_HEADER));
					PS_CAN_DATA_FILE_HEADER CanHeader ;
					ZeroMemory(&CanHeader, sizeof(PS_CAN_DATA_FILE_HEADER));

					//File Header를 읽음
					if (fread(&CanHeader, sizeof(PS_CAN_DATA_FILE_HEADER), 1, fp) < 1)	
					{
//						delete pCANHeader;
						fclose(fp);
						return 0;
					}

					int nColCANCount = CanHeader.nColumnCount;
//					delete pCANHeader;

					if (nColCANCount > 0)
					{
						int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_CAN_DATA_FILE_HEADER);
						//저장된 Data의 크기 
						size_t nRecordSize = sizeof(CAN_VALUE) * nColCANCount;

						//주어진 Index로 이동한다.
						fseek(fp, nRecordSize + nHeaderSize, SEEK_SET);
//----------------------------------------
#ifdef _DEBUG
						{
							int nCanFileSize = filelength(fileno(fp));		
							ASSERT(nCanFileSize >= sizeof(PS_CAN_FILE_HEADER)) ;
							
							// . 파일에 포함된 레코드는 최소 1개 이상이어야 한다.
							int nCanRecordSize = sizeof(CAN_VALUE) * CanHeader.nColumnCount;
							int nFileNumOfRecord = (nCanRecordSize == 0) ? 0 : (nCanFileSize - sizeof(PS_CAN_FILE_HEADER)) / nCanRecordSize ;
							//ASSERT(nFileNumOfRecord > 1) ;
						}
#endif
//----------------------------------------
						
						CAN_VALUE* pBuff = new CAN_VALUE[nColCANCount];
						ZeroMemory(pBuff, sizeof(CAN_VALUE) * nColCANCount);
						int rsCount = 0;

						// 224 (2013/12/29) : 맨 마지막의 레코드를 읽으려는 의도?
						// 왜 파일을 모두 읽지? fseek 해서 맨 마지막에서 한 레코드만 가져오면 되지 않나?
						// Added by 224 (2013/12/29)
						fseek(fp, -nRecordSize, SEEK_END) ;

						while (fread(pBuff, nRecordSize, 1, fp) > 0)
						{	
							rsCount++;
						}

						pTable->SetCanLastData(pBuff, nColCANCount);
						delete [] pBuff;
					}

					
					fclose(fp);

				}
				//End Load CAN Last/////////////////////////////////////////////////////////
				
				//결과 data가 하나도 없거나(최초 step) step이나
				if (m_TableList.GetCount() <= 0)
				{
					m_TableList.AddTail(pTable);
					m_nTotalRecordCount += pTable->GetRecordCount();
				}
				else 
				{
					//index가 정상인 경우  
					if (pTable->GetRecordCount() > 0)
					{
						CTable* pLastT = m_TableList.GetTail();	

						//20081103
						//같은 Step의 결과 Data가 여러번 올라오더라도 1개의 Step으로 Display 되도록 수정 
						if (pLastT->GetStepNo() == pTable->GetStepNo() && pLastT->GetCycleNo() == pTable->GetCycleNo()
							&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							pTable->EditRecordIndex( pLastT->GetRecordIndex(), pTable->GetLastRecordIndex());
						
							m_TableList.RemoveTail();
							delete pLastT;
							
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
						}
// 						else
// 						{
// 							delete pTable;
// 						}
						//20121228
						//Step END가 아닌 rp$ 이면 마지막 테이블에 붙인다
// 						else if(pLastT->GetStepNo() != pTable->GetStepNo() //&& pLastT->GetCycleNo() != pTable->GetCycleNo()
// 							&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						//else if(pLastT->GetStepNo() != pTable->GetStepNo() //&& pLastT->GetCycleNo() != pTable->GetCycleNo() //yulee 20180718 주석처리
						//	&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
							
	//else if(/*pLastT->GetStepNo() != pTable->GetStepNo() &&*/ pLastT->GetCycleNo() != pTable->GetCycleNo()	//ljb 20170824 edit 그래프 자동갱신 수정
						else if(pLastT->GetStepNo() != pTable->GetStepNo() //&& pLastT->GetCycleNo() != pTable->GetCycleNo() 	//yulee 20180905-1 //2번 스텝 진행 중 그래프 그려 질 수 있도록 변경 
						
							&&  pLastT->GetLastRecordIndex() < pTable->GetRecordIndex())
						{
							m_TableList.AddTail(pTable);
							m_nTotalRecordCount += pTable->GetRecordCount();
							m_nTo++; //lyj 20200528 rp$ 선택 추출 버그 수정
							afinder.Close();
							TRACE("Table Size is %d\n", m_TableList.GetCount());
							return TRUE;
						}
						else
						{
							// Added by 224 (2014/01/28) : 조건에 안맞으면, m_TableList에 넣지 않고 바로 삭제
							// 메모리 리크나는 경우 있었음. 재현 (2014/02/07)에 재현 불가능
							delete pTable;
						}
					}
					else
					{
						// Added by 224 (2014/02/09) :
						// (pTable->GetRecordCount() > 0) 조건에 안맞으면 메모리 리크발생.
						// 기존 주석처리 풀음
						delete pTable;
					}
				}
			}
		}
	}

	if (m_TableList.GetCount() < 1)
	{
		return FALSE;
	}

	afinder.Close();
	TRACE("CChData::CreateA(): Table Size is %d\n", m_TableList.GetCount());

	return TRUE;
}

CScheduleData* CChData::GetPattern()
{
	// Pattern file이 없으면 return NULL.
	if(m_pSchedule == NULL)
	{
		CFileFind afinder;
		if(afinder.FindFile(m_strPath+"\\*.sch"))
		{
			afinder.FindNextFile();
			m_pSchedule = new CScheduleData;		
			if(!m_pSchedule->SetSchedule(afinder.GetFilePath()))
			{
				delete m_pSchedule;
				m_pSchedule = NULL;
			}
		}
	}
	return m_pSchedule; 
}

CString CChData::GetLogFileName()
{
	CString strFileName;
	CFileFind afinder;
	if(afinder.FindFile(m_strPath+"\\*.log"))
	{
		afinder.FindNextFile();
		strFileName =  afinder.GetFilePath();
	}
	return strFileName;
}


/*
	Name:    GetLastDataOfTable
	Comment: 지정된 Table에서 지정된 Transducer의 마지막 값을 가져온다.
	Input:   LONG    lIndex   - Table의 번호
	         LPCTSTR strTitle - data를 가져올 transducer의 이름
			 WORD    wPoint   - Pack용에서 'V_MaxDiff'와 'T_MaxDiff'를 계산하기 위해
			                    사용할 point의 정보를 담고 있는 변수
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
float CChData::GetLastDataOfTable(LONG lIndex, LPCTSTR strTitle, WORD wPoint)
{
	CTable* pTable = NULL;

	// Table list에서 lIndex의 index를 가지는 table을 찾는다.

	//Table에서 No가 아닌 순서 Index로 찾도록 수정 //20050818 KBH
/*	POSITION pos = m_TableList.GetHeadPosition();
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable->GetNo()==lIndex) break;
		else pTable = NULL;
	}
*/
	POSITION pos = m_TableList.FindIndex(lIndex);
	if(pos == NULL)		return 0.0f;
	pTable = m_TableList.GetAt(pos);

//////////////////////////////////////////////////////////////////////////
//	Data를 요청하면 만약 Index가 없을 경우 전체 Size가 증가 하므로 적절치 않음
//  Commented By KBH 2005/7/22
//////////////////////////////////////////////////////////////////////////
	// 혹 "TableList" file이 없거나 이 file에 index가 누락되어
	// table이 생성되어 있지 않아서 해당 object가 없는 경우,
//	if(pTable==NULL)
//	{
//		// lIndex를 index로 가지는 table object를 하나 생성하여 list에 넣는다.
//		pTable = new CTable(lIndex);
//		m_TableList.AddTail(pTable);
//		// 그리고 Table의 data를 loading한다.
//		pTable->LoadData(m_strPath);
//	}
//////////////////////////////////////////////////////////////////////////

	if(pTable == NULL)
		return 0.0f;

	// Table에서 strTitle을 이름으로 하는 transducer의 마지막 데이터를 얻는다.
	//OCV는 첫번재 Data에 기록되어 있으므로 실시간 data를 load후 호출 
	if(CString(strTitle).CompareNoCase(RS_COL_OCV) == 0)
	{
		pTable->LoadData_v100B(m_strPath);
	}

 	float fltVal = pTable->GetLastData(strTitle,wPoint);

	// 만약 마지막 데이터를 얻지 못한 상태이면 0.0 이 return 되도록 한다.
	if(fltVal==FLT_MAX) fltVal=0.0f;

	//
	return fltVal; 
}

//지정 Cycle에서 type의 strYAxisTitle 값을 return
float CChData::GetLastDataOfCycle(LONG lCycleID, DWORD dwMode, LPCTSTR strYAxisTitle)
{
	CTable* pTable = NULL;
	float fltRtn =0.0f;
	LONG lIndex = GetFirstTableIndexOfCycle(lCycleID);

	POSITION pos = m_TableList.FindIndex(lIndex);
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable)
		{
			if(pTable->GetCycleNo() == lCycleID)
			{
				//여러개 Step조건을 선택하면 가장 처음 Maching된는 조건을 사용(Charge => Discharge => Reset)
				if(dwMode & (0x01<<pTable->GetType()))
				{
					//요청 항목이 OCV이면 실시간 data를 Loading 후 return
					if(CString(strYAxisTitle).CompareNoCase(RS_COL_OCV) == 0)
					{
						pTable->LoadData_v100B(m_strPath);
					}
					TRACE("**** %s %s \n",m_strPath, strYAxisTitle);		//20131015 ljb 그래프에서 데이터 읽기
					fltRtn = pTable->GetLastData(strYAxisTitle);
				}
			}
			//1 Cycle에 같은 종류의 Step이 여러개 존재할 경우 가장 마지막 Step의 Data값 사용
			else if(pTable->GetCycleNo() > lCycleID)				
			{
				break;
			}
		}
	}

	return fltRtn;
}


/*
	Name:    GetDataOfTable
	Comment: 지정된 Table에서 지정된 Transducer의 데이터를 가져온다.
	Input:   LONG    lIndex        - Table의 번호
	         LONG&   lDataNum      - Table에서 가져가는 데이터의 개수를 돌려받을 reference 변수
	         LPCTSTR strXAxisTitle - X축 data의 transducer 이름
	         LPCTSTR strYAxisTitle - Y축 data의 transducer 이름
			 WORD    wPoint        - Pack용에서 'V_MaxDiff'와 'T_MaxDiff'를 계산하기 위해
			                         사용할 point의 정보를 담고 있는 변수
	Output:
	Revision History:
	Version  Name            Date        Reason
	    1.0  Kim, Sung-Hoon  2001.06.01  The first settlement
*/
fltPoint* CChData::GetDataOfTable(LONG lIndex, LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
{
	//2014.10.09 예외처리추가.
	try
	{
		CTable* pTable = NULL;

		//Table에서 No가 아닌 순서 Index로 찾도록 수정 //20050818 KBH
		// Table list에서 lIndex의 index를 가지는 table을 찾는다.
		/*	
		POSITION pos = m_TableList.GetHeadPosition();
		while(pos)
		{
			pTable = m_TableList.GetNext(pos);
			if(pTable->GetNo()==lIndex) break;
			else pTable = NULL;
		}
		*/
		POSITION pos = m_TableList.FindIndex(lIndex);
		if(pos == NULL)		return NULL;
		pTable = m_TableList.GetAt(pos);

		//////////////////////////////////////////////////////////////////////////
		//	Data를 요청 하면 서 Size가 변하므로 적절치 않음
		//  Commented By KBH 2005/7/22
		//////////////////////////////////////////////////////////////////////////
		// 혹 "TableList" file이 없거나 이 file에 index가 누락되어
		// table이 생성되어 있지 않아서 해당 object가 없는 경우,
		//	if(pTable==NULL)
		//	{
		//		// lIndex를 index로 가지는 table object를 하나 생성하여 list에 넣는다.
		//		pTable = new CTable(lIndex);
		//		m_TableList.AddTail(pTable);
		//	}
		//////////////////////////////////////////////////////////////////////////

		if(pTable == NULL)
			return NULL;
		//////////////////////////////////////////////////////////////////////////

		//Table의 data를 loading한다.
	    //pTable->LoadData(m_strPath);
		pTable->LoadData_v100B(m_strPath);
		
		return pTable->GetData(lDataNum, strXAxisTitle, strYAxisTitle, wPoint);
	}
	catch (...)
	{
		return NULL;
	}
	
}

// Added by 224 (2014/07/28) : 대용량 데이터의 과도한 메모리 할당으로 인한
// 메모리 오류로 인하여, 로드된 자료에서 해당하는 행의 값만 가져오는 방법 추가

INT CChData::GetDataCount(LONG tbIdx)
{
	POSITION pos = m_TableList.FindIndex(tbIdx);
	if (pos == NULL)
	{
		ASSERT(FALSE) ;
		return 0 ;
	}
	
	CTable* pTable = m_TableList.GetAt(pos);
	if (pTable == NULL)
	{
		ASSERT(pTable) ;
		return 0 ;
	}
	
	
	pTable->LoadData_v100B(m_strPath);

	return pTable->GetDataCount() ; 
}

fltPoint CChData::GetDatumOfTable(LONG tbIdx, LPCTSTR strYAxisTitle, INT row, WORD wPoint)
{
//	CTable* pTable = NULL;
	fltPoint fltRet = {0.0f, 0.0f} ;

	POSITION pos = m_TableList.FindIndex(tbIdx);
	if (pos == NULL)
	{
		ASSERT(FALSE) ;
		return fltRet ;
	}

	CTable* pTable = m_TableList.GetAt(pos);
	if (pTable == NULL)
	{
		ASSERT(pTable) ;
		return fltRet ;
	}
//////////////////////////////////////////////////////////////////////////

	// Table의 data를 loading한다.
	pTable->LoadData_v100B(m_strPath);

	//
	fltRet = pTable->GetDatum(strYAxisTitle, row, wPoint);
	return fltRet ;
}

//해당 cycle 번호를 갖는 Step Data
//20100430
//fltPoint* CChData::GetDataOfCycle(LONG lCycleID, LONG& lDataNum, DWORD dwMode, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
fltPoint* CChData::GetDataOfCycle(LONG lCycleID, LONG& lDataNum, DWORD dwMode, LONG lQueryMode, CList<LONG, LONG&>* pdwListStep, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
{
	CTable* pTable = NULL;
	floatData *fTempData = NULL;
	LONG lTotSize = 0, lSize;
	fltPoint *fData = NULL;

	//20100430 cycle query일 경우
	if (lQueryMode == 0)	// QUERY_MODE_CYCLE
	{
	//1. Cycle로 선택구간 Query시 처리 
		LONG lIndex = GetFirstTableIndexOfCycle(lCycleID);
		POSITION pos = m_TableList.FindIndex(lIndex);
		
		//1. 전체 Data의 크기를 구한다.
		while(pos)
		{
			pTable = m_TableList.GetNext(pos);
			if(pTable)
			{
				lSize = 0;
				fTempData = NULL;
				//해당 Cycle 번호를 갖는 모든 data를 합하여 return한다.
				//Mode에 따라 해당 Type의 Step 포함 여부 Check
				if(pTable->GetCycleNo() == lCycleID)
				{
					if(dwMode & (0x01<<pTable->GetType()))
					{
						//20104030
						LONG lStepNo = (LONG)pTable->GetStepNo();
						if(pdwListStep->Find(lStepNo))
						//////////////////////////////////////////////////////////////////////////
						{
							// Table의 data를 loading한다.
							pTable->LoadData_v100B(m_strPath);
							//ljb
							CString strFindAux(strYAxisTitle);
							if (strFindAux.Mid(5,4) == RS_COL_AUX_TEMP || strFindAux.Mid(5,4) == RS_COL_AUX_VOLT)	//ljb 2010-06-23
							{
								CString strAuxTitle = strFindAux.Right(strFindAux.Find(']'));
								DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
								fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
							}
							else
								fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint);
							//////////////////////////////////////////////////////////////////////////
//							fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint);
							if(fTempData != NULL)
							{
								lTotSize += lSize;
								delete fTempData;
							}
						}
					}
				}
				else
					break;
			}
		}
		if(lTotSize <= 0)	return NULL;


		//2. Data를 합한다.
		CString strXAxisTemp(strXAxisTitle);
		LONG lOffset = 0;
		float fltSum =0.0f;
		fData = new fltPoint[lTotSize]; 
		pos = m_TableList.FindIndex(lIndex);
		//전체 Data의 크기를 구한다.
		while(pos)
		{
			pTable = m_TableList.GetNext(pos);
			if(pTable)
			{
				lSize = 0;
				fTempData = NULL;
				//해당 Cycle 번호를 갖는 모든 data를 합하여 return한다.
				//Mode에 따라 해당 Type의 Step 포함 여부 Check
				if(pTable->GetCycleNo() == lCycleID)
				{
					if(dwMode & (0x01<<pTable->GetType()))
					{
						//20104030
						LONG lStepNo = (LONG)pTable->GetStepNo();
						if(pdwListStep->Find(lStepNo))
						//////////////////////////////////////////////////////////////////////////
						{
							TRACE("Cycle data added. Type %d, Time Sum :%f\n", pTable->GetType(), fltSum);
							// Table의 data를 loading한다.
							pTable->LoadData_v100B(m_strPath);
							//ljb
							CString strFindAux(strYAxisTitle);
							if (strFindAux.Mid(5,4) == RS_COL_AUX_TEMP || strFindAux.Mid(5,4) == RS_COL_AUX_VOLT)	//ljb 2010-06-23
							{
								CString strAuxTitle = strFindAux.Right(strFindAux.Find(']'));
								DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
								fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
							}
							else
								fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint);
							//////////////////////////////////////////////////////////////////////////

							//fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint);

							if(fTempData != NULL)
							{
								//1 Cycle
								if(strXAxisTemp.CompareNoCase("Time")==0 )
								{
									for(int i=0; i<lSize; i++)
									{
										fltPoint pt = fTempData[i];
										pt.x += fltSum;
										fTempData[i] = pt;
									}
									fltSum = fTempData[lSize-1].x;
								}							
								memcpy((char *)fData+sizeof(floatData)*lOffset, fTempData, sizeof(floatData)*lSize);
								lOffset += lSize;
								delete fTempData;
							}
						}
					}
				}
				else
					break;
			}
		}
		lDataNum = lTotSize;
	}
	else //if(lQueryMode == QUERY_MODE_INDEX)
	{
		//2. Index로 선택 구간 Query시 처리 
		//20100430 table index query일 경우
		// Added by 224 (2014/01/23) : 그래프 자료 로딩 개선
		INT nIdx = lCycleID - m_nFrom ;
		pTable = GetTableAt(nIdx);		//lCycleID => lIndexID로 사용됨
//		pTable = GetTableAt(lCycleID-1);		//lCycleID => lIndexID로 사용됨
		
		if(pTable)
		{
			lSize = 0;
			fTempData = NULL;
			//해당 Cycle 번호를 갖는 모든 data를 합하여 return한다.
			//Mode에 따라 해당 Type의 Step 포함 여부 Check
			CString strFindAux(strYAxisTitle);

			if ((strFindAux.Mid(5,3) == RS_COL_CAN )&&(strFindAux.Mid(5,4).CompareNoCase("CANs") != 0))//ljb 2011216 이재복 ////////// //yulee 20180831-2 수정  
			{
				LONG lStepNo = (LONG)pTable->GetStepNo();
				if(pdwListStep->Find(lStepNo))
				{
					// Table의 data를 loading한다.
					pTable->LoadData_v100B(m_strPath);
					//ljb
					CString strFindAux(strYAxisTitle);
					CString strAuxTitle = strFindAux.Left(8);
					DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
					//DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, 7));

//					TRACE("\n ***** %s \n",strFindAux.Mid(strFindAux.Find('[')+1, 7));
					//ljb 임시
// 					CString strtemp;
// 					strtemp.Format("strCANTitle= %s wAuxPoint= %d" ,strAuxTitle, wAuxPoint);
// 					AfxMessageBox(strtemp);

					fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
					//////////////////////////////////////////////////////////////////////////
					
					if(fTempData != NULL)
					{
						lTotSize = lSize;
						fData = new fltPoint[lSize]; 
						memcpy((char *)fData, fTempData, sizeof(floatData)*lSize);
						
						delete fTempData;
					}
					else
					{
						return NULL;
					}
				}
			}
			else if (strFindAux.Mid(5,5) == RS_COL_CAN_S1 )//ljb 2011216 이재복 ////////// //yulee 20180903 수정  
			{
				LONG lStepNo = (LONG)pTable->GetStepNo();
				if(pdwListStep->Find(lStepNo))
				{
					// Table의 data를 loading한다.
					pTable->LoadData_v100B(m_strPath);
					//ljb
					CString strFindAux(strYAxisTitle);
					CString strAuxTitle = strFindAux.Left(8);
					DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
					//DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, 7));
					
					//					TRACE("\n ***** %s \n",strFindAux.Mid(strFindAux.Find('[')+1, 7));
					//ljb 임시
					// 					CString strtemp;
					// 					strtemp.Format("strCANTitle= %s wAuxPoint= %d" ,strAuxTitle, wAuxPoint);
					// 					AfxMessageBox(strtemp);
					
					fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
					//////////////////////////////////////////////////////////////////////////
					
					if(fTempData != NULL)
					{
						lTotSize = lSize;
						fData = new fltPoint[lSize]; 
						memcpy((char *)fData, fTempData, sizeof(floatData)*lSize);
						
						delete fTempData;
					}
					else
					{
						return NULL;
					}
				}
			}
			else if (strFindAux.Mid(5,5) == RS_COL_CAN_S2)//ljb 2011216 이재복 ////////// //yulee 20180903 수정  
			{
				LONG lStepNo = (LONG)pTable->GetStepNo();
				if(pdwListStep->Find(lStepNo))
				{
					// Table의 data를 loading한다.
					pTable->LoadData_v100B(m_strPath);
					//ljb
					CString strFindAux(strYAxisTitle);
					CString strAuxTitle = strFindAux.Left(8);
					DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
					//DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, 7));
					
					//					TRACE("\n ***** %s \n",strFindAux.Mid(strFindAux.Find('[')+1, 7));
					//ljb 임시
					// 					CString strtemp;
					// 					strtemp.Format("strCANTitle= %s wAuxPoint= %d" ,strAuxTitle, wAuxPoint);
					// 					AfxMessageBox(strtemp);
					
					fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
					//////////////////////////////////////////////////////////////////////////
					
					if(fTempData != NULL)
					{
						lTotSize = lSize;
						fData = new fltPoint[lSize]; 
						memcpy((char *)fData, fTempData, sizeof(floatData)*lSize);
						
						delete fTempData;
					}
					else
					{
						return NULL;
					}
				}
			}
			else if(strFindAux.Mid(5,5) == RS_COL_CAN_S3)//ljb 2011216 이재복 ////////// //yulee 20180903 수정  
			{
				LONG lStepNo = (LONG)pTable->GetStepNo();
				if(pdwListStep->Find(lStepNo))
				{
					// Table의 data를 loading한다.
					pTable->LoadData_v100B(m_strPath);
					//ljb
					CString strFindAux(strYAxisTitle);
					CString strAuxTitle = strFindAux.Left(8);
					DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
					//DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, 7));
					
					//					TRACE("\n ***** %s \n",strFindAux.Mid(strFindAux.Find('[')+1, 7));
					//ljb 임시
					// 					CString strtemp;
					// 					strtemp.Format("strCANTitle= %s wAuxPoint= %d" ,strAuxTitle, wAuxPoint);
					// 					AfxMessageBox(strtemp);
					
					fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
					//////////////////////////////////////////////////////////////////////////
					
					if(fTempData != NULL)
					{
						lTotSize = lSize;
						fData = new fltPoint[lSize]; 
						memcpy((char *)fData, fTempData, sizeof(floatData)*lSize);
						
						delete fTempData;
					}
					else
					{
						return NULL;
					}
				}
			}
			else if(strFindAux.Mid(5,5) == RS_COL_CAN_S4)//ljb 2011216 이재복 ////////// //yulee 20181009 수정  
			{
				LONG lStepNo = (LONG)pTable->GetStepNo();
				if(pdwListStep->Find(lStepNo))
				{
					// Table의 data를 loading한다.
					pTable->LoadData_v100B(m_strPath);
					//ljb
					CString strFindAux(strYAxisTitle);
					CString strAuxTitle = strFindAux.Left(8);
					DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
					//DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, 7));
					
					//					TRACE("\n ***** %s \n",strFindAux.Mid(strFindAux.Find('[')+1, 7));
					//ljb 임시
					// 					CString strtemp;
					// 					strtemp.Format("strCANTitle= %s wAuxPoint= %d" ,strAuxTitle, wAuxPoint);
					// 					AfxMessageBox(strtemp);
					
					fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
					//////////////////////////////////////////////////////////////////////////
					
					if(fTempData != NULL)
					{
						lTotSize = lSize;
						fData = new fltPoint[lSize]; 
						memcpy((char *)fData, fTempData, sizeof(floatData)*lSize);
						
						delete fTempData;
					}
					else
					{
						return NULL;
					}
				}
			}
			else
			{
				if (dwMode & (0x01 << pTable->GetType()))
				{
					LONG lStepNo = (LONG)pTable->GetStepNo();
					if (pdwListStep->Find(lStepNo))
					{
						// Table의 data를 loading한다.
						// Table(STEP END)의 raw data를 loading한다.
						pTable->LoadData_v100B(m_strPath);

						//ljb
// 						CString strFindAux(strYAxisTitle);
						//if (strFindAux.Left(4) == RS_COL_AUX_TEMP || strFindAux.Left(4) == RS_COL_AUX_VOLT)	//ljb 2010-06-23 //20180610 YULEE 주석처리
						//if (strFindAux.Mid(5,4) == RS_COL_AUX_TEMP || strFindAux.Mid(5,4) == RS_COL_AUX_VOLT || strFindAux.Mid(5,5) == RS_COL_AUX_TEMPTH)	//20180610 YULEE
						if (strFindAux.Mid(5,4) == RS_COL_AUX_TEMP 
							|| strFindAux.Mid(5,4) == RS_COL_AUX_VOLT 
							|| strFindAux.Mid(5,5) == RS_COL_AUX_TEMPTH
							|| strFindAux.Mid(5,4) == RS_COL_AUX_HUMI)	//ksj 20200116 : v1016 습도 추가
						{
							//CString strAuxTitle = strFindAux.Left(strFindAux.Find('['));
							CString strAuxTitle;
							/*if(strFindAux.Mid(5,5) != RS_COL_AUX_TEMPTH) //20180620 yulee
								strAuxTitle = strFindAux.Mid(5,4);
							else if(strFindAux.Mid(5,5) == RS_COL_AUX_TEMPTH)
								strAuxTitle = strFindAux.Mid(5,5);
							*/
							//ksj 20200116
							if(strFindAux.Mid(5,5) == RS_COL_AUX_TEMPTH)
								strAuxTitle = strFindAux.Mid(5,5);
							//if(strFindAux.Mid(5,4) == RS_COL_AUX_HUMI)
							else if(strFindAux.Mid(5,4) == RS_COL_AUX_HUMI) //ksj 20200217 : AuxTH 그래프 안그려지는 현상 수정
								strAuxTitle = strFindAux.Mid(5,4);
							else
								strAuxTitle = strFindAux.Mid(5,4);
							//ksj end

							DWORD wAuxPoint = atoi(strFindAux.Mid(strFindAux.Find('[')+1, strFindAux.ReverseFind(']')));
							fTempData = pTable->GetData(lSize, strXAxisTitle, strAuxTitle, wAuxPoint);
						}
						else
						{
							//fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint);
							fTempData = pTable->GetData(lSize, strXAxisTitle, strYAxisTitle, wPoint, pTable->GetType()); //20180710 yulee pTable->GetType() 추가
						}
						//////////////////////////////////////////////////////////////////////////

						if(fTempData != NULL)
						{
							lTotSize = lSize;
							fData = new fltPoint[lSize]; 
							memcpy((char *)fData, fTempData, sizeof(floatData)*lSize);

							delete fTempData;
						}
						else
						{
							return NULL;
						}
					}
				}//end if(dwMode & (0x01<<pTable->GetType()))
			}
		}

		lDataNum = lTotSize;
	}

	return fData;
}


void CChData::ResetData()
{
	// Pattern object에 할당된 memory가 있으면 지운다.
	if(m_pSchedule!= NULL) 
	{
		delete m_pSchedule;
		m_pSchedule = NULL;
	}

	// Table의 list를 비우면서 각 Table에 할당된 memeory를 지운다.
	while(!m_TableList.IsEmpty())
	{
		CTable* pTable = m_TableList.RemoveTail();
		delete pTable;
	}

	m_nTotalRecordCount = 0;
//	m_fCapacitySum = 0;

	m_nTo = -1; //ksj 20200720 : 초기화 추가
	m_nFrom = -1;  //ksj 20200720 : 초기화 추가
}
//ksj 20210419 : 데이터 복구시 과도한 메모리 사용을 피하도록 이미 스캔한 데이터는 데이터를 메모리에서 비워준다.
BOOL CChData::DeleteTableData(int tbIdx)
{
	POSITION pos = m_TableList.FindIndex(tbIdx);
	if (pos == NULL)
	{
		ASSERT(pos);
		return FALSE;
	}

	CTable* pTable = m_TableList.GetAt(pos);
	if (pTable == NULL)
	{
		ASSERT(pTable);
		return FALSE;
	}

	delete pTable;
	m_TableList.SetAt(pos,NULL);

	return TRUE;
}

BOOL CChData::IsOKCell()
{
	//상태 정보 
	float fData = GetLastDataOfTable(m_TableList.GetCount(), "Code");
	BYTE code = (BYTE)fData;
	
	return PSIsCellOk(code);
}

LONG	CChData::GetTableNo(int nIndex)
{
/*	POSITION pos = m_TableList.GetHeadPosition();
	int nCount = 0;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(nIndex == nCount++)
		{
			CString strName;
			strName.Format("%d", nIndex);
			AfxMessageBox(strName);

		}
	}
*/
	CTable* pTable = NULL;
	// Modified by 224 (2014/02/07) : POSITION 값 이상으로 수정
	pTable = GetTableAt(nIndex) ;
// 	pTable = (CTable*)m_TableList.GetAt(m_TableList.FindIndex(nIndex));
	if (pTable == NULL)
	{
		return -1;
	}

	return pTable->GetStepNo();
}

LONG CChData::GetTableIndex(int nStepNo, int lCycleNo)
{
	POSITION pos = m_TableList.GetHeadPosition();
	int nIndex = 0;
	CTable* pTable = NULL;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		//		int aa = pTable->GetStepNo();
		if(pTable->GetStepNo() == nStepNo && pTable->GetCycleNo() == lCycleNo) return nIndex;
		
		nIndex++;
	}
	return -1;
}

long CChData::GetFirstTableIndexOfCycle(LONG lCycleID)
{
	POSITION pos = m_TableList.GetHeadPosition();
	int nIndex = 0;
	CTable* pTable = NULL;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable->GetCycleNo() == lCycleID) return nIndex;
		
		nIndex++;
	}
	return 0;
}

BOOL CChData::ReLoadData()
{
	//Create();	
	CreateA2();	//ljb 20150123 add
	return TRUE;
}

//nStepIndex까지의 용량 합산을 return 한다.
//nStepIndex = -1 =>모든 step
float CChData::GetCapacitySum(int nStepIndex)
{
	CTable *pTable = NULL;
	POSITION pos = m_TableList.GetHeadPosition();

	float fCapaSum = 0.0f;
	int nIndex = 0;
	int nType = 0;
	
	//All step data
	if(nStepIndex < 1)	nStepIndex = 0x7FFFFFFF;

	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable && nIndex<nStepIndex)
		{
			nType = (int)pTable->GetLastData("Type");
			if(nType == PS_STEP_CHARGE)
			{
				fCapaSum += pTable->GetLastData("Capacity");
			}
			else if(nType == PS_STEP_DISCHARGE || nType == PS_STEP_IMPEDANCE)
			{
				fCapaSum -= pTable->GetLastData("Capacity");
			}
		}
		nIndex++;
	}
	return fCapaSum;
}

CWordArray * CChData::GetRecordItemList(LONG lIndex)
{
	CTable *pTable = NULL;
	POSITION pos = m_TableList.GetHeadPosition();

	int nCnt = 0;
	while(pos)
	{
		pTable = m_TableList.GetNext(pos);
		if(pTable && nCnt++ == lIndex)
		{
			return pTable->GetRecordItemList();
		}
	}
	return NULL;
}

CWordArray * CChData::GetSaveRecordItemList()
{
	return &m_wArrySaveItem;
}

//많은양의 table이 있을시 GetLastDataOfTable()과 같은 함수에서 
//CList::Find() 함수를 여러번 호출하여 속도가 느려지는 현상을 없에기 위해 
//Table을 직접 호출하는 함수 추가 
//2006/8/7 KBH
POSITION CChData::GetFirstTablePosition()
{
	return m_TableList.GetHeadPosition();
}

CTable * CChData::GetNextTable(POSITION &pos)
{
	return (CTable *)m_TableList.GetNext(pos);
}

CTable * CChData::GetTableAt(LONG lIndex)
{
	POSITION pos = m_TableList.FindIndex(lIndex);
	if(pos)
	{
		return (CTable*)m_TableList.GetAt(pos);
	}
	return NULL;
}	

//속도 향상을 위해 현재 결과의 최종 data table을 Loading하는 함수를 별도로 만든다.
//Static 함수이고 반환된 CTable *은 호출자가 Delete해야 한다.
CTable* CChData::GetLastDataRecord(CString strFileName, long &lEndTableSize, CChData* pParent)
{
	// Added by 224 (2014/02/03) :
	ASSERT(pParent) ;
	
	CTable* pTable = NULL;
	CFileFind afinder, afinder1;
	int nTotalStepEndSize = 0;

	//1. 현재 진행중인 step 파일 검색 
	if (afinder.FindFile(strFileName + "\\*.rp$"))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		// Table List File을 열어서
		CStdioFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if (!strContent.IsEmpty()) strlist.AddTail(strContent);
			}
			afile.Close();

			// 내용이 2줄 이상이면 Table이 생성되어 있는 것임
			if (strlist.GetCount() > 2)
			{
				// 첫번째는 StartT= ,EndT=, Serial=,	기록 에서 End Time 만 추출 
				POSITION pos;
				pos = strlist.GetHeadPosition();

				// 첫번째는 StartT= ,EndT=, Serial=,	기록
				strContent = strlist.GetNext(pos);
		
				//2번째 Row는 Title 기록 
				CString strTitle =  strlist.GetNext(pos);		//Title
				
				// Table을 만든다.
				strContent = strlist.GetTail();
				
				// Added by 224 (2014/02/03) :
//				pTable = new CTable(strTitle, strContent);
				pTable = new CTable(strTitle, strContent, pParent);

				//20090911 KBH
				//파일이 깨졋는지 확인
				if ( pTable->GetCycleNo() > 0 && pTable->GetRecordCount() > 0 
					&& pTable->GetType() > 0 && pTable->GetLastRecordIndex() >0)
				{
					//return pTable;

					//20100430 Table size 구함
					if (afinder1.FindFile(strFileName+"\\*."+PS_RESULT_FILE_NAME_EXT))
					{
						afinder1.FindNextFile();
						CString fileName = afinder1.GetFilePath();
						CFile afile;
						if (afile.Open(fileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
						{
							long lSize = afile.SeekToEnd();
							nTotalStepEndSize = (lSize - sizeof(PS_TEST_RESULT_FILE_HEADER))/sizeof(PS_STEP_END_RECORD);
							if ((lSize - sizeof(PS_TEST_RESULT_FILE_HEADER))%sizeof(PS_STEP_END_RECORD) != 0)
							{
								TRACE("Data file size error!!!!!!!\n");
							}
						}
					}
					nTotalStepEndSize = nTotalStepEndSize+1;		//임시파일에 저장된 STEP값도 포함함
					afinder1.Close();
				}
				else
				{
					delete pTable;
					pTable = NULL;
				}
			}
		}
	}

	afinder.Close();

	if (pTable != NULL)
	{
		lEndTableSize = nTotalStepEndSize;	//20100430
		return pTable;
	}

	//2.결과 파일에서 찾음
	if (afinder.FindFile(strFileName + "\\*." + PS_RESULT_FILE_NAME_EXT))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		CFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone))
		{
			PS_TEST_RESULT_FILE_HEADER *pHeader = new PS_TEST_RESULT_FILE_HEADER;

			afile.Read(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
			if (pHeader->fileHeader.nFileID != PS_ADP_STEP_RESULT_FILE_ID)
			{
				afile.Close();
			}
			
			CWordArray aItem;
			for(int i=0; i<pHeader->testHeader.nRecordSize && i<PS_MAX_FILE_ITEM_NUM; i++)
			{
				aItem.Add(pHeader->testHeader.wRecordItem[i]);
			}

			long lSize = afile.SeekToEnd();
			afile.Seek(lSize - sizeof(PS_STEP_END_RECORD), CFile::begin);

			PS_STEP_END_RECORD *pRecord = new PS_STEP_END_RECORD;
			if (afile.Read(pRecord, sizeof(PS_STEP_END_RECORD)) > 0)
			{
				// Added by 224 (2014/02/03) ;
//				pTable = new CTable(&aItem, pRecord);
				pTable = new CTable(&aItem, pRecord, pParent);
			}
			delete 	pHeader;
			delete pRecord;
			afile.Close();

			//20100430
			//Record size 계산
			nTotalStepEndSize = (lSize - sizeof(PS_TEST_RESULT_FILE_HEADER)) / sizeof(PS_STEP_END_RECORD);
			if ((lSize - sizeof(PS_TEST_RESULT_FILE_HEADER))%sizeof(PS_STEP_END_RECORD) != 0)
			{
				TRACE("Data file size error!!!!!!!\n");
			}
		}
	}
	afinder.Close();

	if (pTable != NULL)
	{
		lEndTableSize = nTotalStepEndSize;	//20100430
		return pTable;
	}

	//3. Old version of result file
	if (afinder.FindFile(strFileName + "\\*.rpt"))
	{
		afinder.FindNextFile();
		CString fileName = afinder.GetFilePath();
		// Table List File을 열어서
		CStdioFile afile;
		if (afile.Open(fileName, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			// 그 내용을 읽어온다.
			CString strContent;
			CString strLine1, strLine2, strLine;
			afile.ReadString(strLine1);
			afile.ReadString(strLine2);
			while(afile.ReadString(strContent))
			{
				if (!strContent.IsEmpty()) strLine = strContent;
				
				//20100430
				nTotalStepEndSize++;
			}
			afile.Close();

			//2번째 Row는 Title 기록 
			CString strTitle = strLine2;		//Title
			// Added by 224 (2014/02/03) :
//			pTable = new CTable(strTitle, strLine);
			pTable = new CTable(strTitle, strLine, pParent);
		}
	}
	afinder.Close();	

	lEndTableSize = nTotalStepEndSize;		//20100430
	
	return pTable;
}

//c:\\Test_Name\\M01C01[001] 형태
CString CChData::GetTestName()
{
	if(m_strPath.IsEmpty())	return "";

	int a = m_strPath.ReverseFind('\\');
	if(a < 1)	return "";

	CString strName(m_strPath.Left(a));

	a = strName.ReverseFind('\\');
	if(a < 1)	return "";

	return strName.Mid(a+1);
}


int CChData::GetModuleID()
{
	if(m_strPath.IsEmpty())	return -1;

	int a = m_strPath.ReverseFind('\\');
	if(a < 1)	return -1;

	CString strName(m_strPath.Mid(a+1));
	strName.MakeUpper();

	int indexS = strName.Find('M');
	int indexE = strName.Find('C');
	
	if(indexS >= indexE)	return -1;

	return atol(strName.Mid(indexS+1, (indexE-indexS)-1));
}

int CChData::GetChannelNo()
{
	if(m_strPath.IsEmpty())	return -1;

	int a = m_strPath.ReverseFind('\\');
	if(a < 1)	return -1;

	CString strName(m_strPath.Mid(a+1));
	strName.MakeUpper();

	int indexS = strName.Find('C');
	int indexE = strName.Find('[');

	//[]를 발견하지 못하였을 경우 
	if(indexE < 0)
	{
		return atol(strName.Mid(indexS+1));
	}
		
	if(indexS >= indexE)	return -1;

	return atol(strName.Mid(indexS+1, (indexE-indexS)-1));
}

int CChData::GetAuxColumnCount()
{	
	return this->m_nAuxColumnCount;
}

void CChData::LoadAuxTitle(CString strAuxConfigPath)
{
	CString strPath;

	char path[MAX_PATH];
    GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	CString strTemp(path);
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strAuxConfigPath.Mid(strAuxConfigPath.ReverseFind('\\')+1, 7), PS_AUX_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("*AUX.%s", "tlt"); 
	CString strNew = strAuxConfigPath+"\\"+strTemp;
	
		
   	CFileFind afinder;
	int nIndex = 0;
	m_nAuxTempCount = 0;
	m_nAuxVoltCount = 0;
	m_nAuxTempTHCount = 0; //20180607 yulee
	m_nAuxHumiCount = 0; //ksj 20200116

	if(afinder.FindFile(strNew) ==  TRUE)
	{
		afinder.FindNextFile();
		strPath = afinder.GetFilePath();


		//Data를 읽어들임
		FILE *fp = fopen(strPath, "rb");
		if(fp == NULL)		return;

		char buff[10000] = {0,};
		fread(&buff, 10000, 1, fp);

		CString strBuff;
		strBuff.Format("%s", buff);

		int nFind = strBuff.Find('\n');
		int nPos = 0;
//ljb 2010-06-22 Start-----------------------------------
		while(nFind > 0)
		{
			strTemp = strBuff.Mid(nPos,nFind - nPos -1);
			
			char *Buf = new char[strTemp.GetLength() + 1];//lmh 20120319 문장의 길이가 50이상일때를 대비해서 포인터형으로 변경
			char seps[] = ",";
			char * token;
			sprintf(Buf, strTemp);

			
			//ljb AUX정보의 이름을 추가하도록 변경
			token = strtok(Buf, seps);				// 1.chNo
			//m_ArrayAuxInfo[nIndex].chNo = atoi(token);
			m_ArrayAuxInfo[nIndex].chNo = nIndex+1;				//ljb 20131118 add

			token = strtok(NULL, seps);				// 2.auxType
			m_ArrayAuxInfo[nIndex].auxType = atoi(token);
			if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_TEMPERATURE)
				m_nAuxTempCount++;
			else if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_VOLTAGE)
				m_nAuxVoltCount++;
			else if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_TEMPERATURE_TH)
				m_nAuxTempTHCount++;
			//ksj 20200117 : 습도 센서 추가
			else if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_HUMIDITY)
				m_nAuxHumiCount++;
			
			token = strtok(NULL, seps);				// 4.auxName
			if(token != NULL)
			{
				if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_TEMPERATURE)
					sprintf(m_ArrayAuxInfo[nIndex].auxName, "%s_%03d", token, m_nAuxTempCount);
				else if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_VOLTAGE)
					sprintf(m_ArrayAuxInfo[nIndex].auxName, "%s_%03d", token, m_nAuxVoltCount);
				else if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_TEMPERATURE_TH)
					sprintf(m_ArrayAuxInfo[nIndex].auxName, "%s_%03d", token, m_nAuxTempTHCount);
				//ksj 20200116 : 습도 센서 추가
				else if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_HUMIDITY)
					sprintf(m_ArrayAuxInfo[nIndex].auxName, "%s_%03d", token, m_nAuxHumiCount);
			}
			
			nIndex++;
			
			token = strtok(NULL, seps);
			nPos = nFind+1;
			nFind = strBuff.Find('\n', nFind+1);
			
			
			
			delete Buf;
			Buf = NULL;
		}	
//ljb 2010-06-22 End -----------------------------------
		
		fclose(fp);
	}

//	m_TableArrayAuxInfo = m_ArrayAuxInfo;

	m_nAuxColumnCount = nIndex;
}

CString CChData::GetAuxTitleOfIndex(int nAuxIndex)
{
	if(nAuxIndex < 0 || nAuxIndex > this->GetAuxColumnCount()) return "";
	CString strTitle;

	switch(m_ArrayAuxInfo[nAuxIndex].auxType)
	{
		//20180619 yulee sort를 위한 변경
		case PS_AUX_TYPE_TEMPERATURE:		strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nAuxIndex].chNo, RS_COL_AUX_TEMP,   m_ArrayAuxInfo[nAuxIndex].auxName); break;
		case PS_AUX_TYPE_VOLTAGE:			strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nAuxIndex].chNo, RS_COL_AUX_VOLT,   m_ArrayAuxInfo[nAuxIndex].auxName); break;
		case PS_AUX_TYPE_TEMPERATURE_TH:	strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nAuxIndex].chNo, RS_COL_AUX_TEMPTH, m_ArrayAuxInfo[nAuxIndex].auxName); break;
		case PS_AUX_TYPE_HUMIDITY:		strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nAuxIndex].chNo, RS_COL_AUX_HUMI, m_ArrayAuxInfo[nAuxIndex].auxName); break; //ksj 20200116 : 습도
	/*
		case PS_AUX_TYPE_TEMPERATURE:		strTitle.Format("%s(%s)[%003d]",RS_COL_AUX_TEMP, m_ArrayAuxInfo[nAuxIndex].auxName, m_ArrayAuxInfo[nAuxIndex].chNo);break;
		case PS_AUX_TYPE_VOLTAGE:			strTitle.Format("%s(%s)[%003d]",RS_COL_AUX_VOLT, m_ArrayAuxInfo[nAuxIndex].auxName, m_ArrayAuxInfo[nAuxIndex].chNo); break;
		case PS_AUX_TYPE_TEMPERATURE_TH:	strTitle.Format("%s(%s)[%003d]",RS_COL_AUX_TEMPTH, m_ArrayAuxInfo[nAuxIndex].auxName, m_ArrayAuxInfo[nAuxIndex].chNo); break;
	*/
	}
	return strTitle;
}

fltPoint * CChData::GetAuxDataOfTable(LONG lIndex, LONG lDataNum, int nAuxIndex)
{
	CTable* pTable = NULL;

	POSITION pos = m_TableList.FindIndex(lIndex);
	if(pos == NULL)		return NULL;
	pTable = m_TableList.GetAt(pos);


	if(pTable == NULL)

	pTable->LoadData_v100B(m_strPath);

	return pTable->GetAuxData(lDataNum, nAuxIndex);
}

void CChData::LoadCANTitle(CString strCanConfigPath)
{
	CString strPath;

	char path[MAX_PATH];
    GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	CString strTemp(path);
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strCanConfigPath.Mid(strCanConfigPath.ReverseFind('\\')+1, 7), PS_CAN_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("*%s.%s", "CAN", "tlt"); 
	CString strNew = strCanConfigPath+"\\"+strTemp;
			
   	CFileFind afinder;
	int nIndex = 0;

	m_nCanMasterCount = m_nCanSlaveCount = 0;

	if(afinder.FindFile(strNew) ==  TRUE)
	{
		afinder.FindNextFile();
		
		strPath = afinder.GetFilePath();
		
		//Data를 읽어들임
		FILE *fp = fopen(strPath, "rb");
		if(fp == NULL)		return;

		char buff[30000] = {0,};
		
		fread(&buff, 30000, 1, fp);

		CString strBuff;
		strBuff.Format("%s", buff);

		int nFind = strBuff.Find('\n');
		int nPos = 0;
		while(nFind > 0)
		{
			strTemp = strBuff.Mid(nPos,nFind - nPos -1);
			
			//ljb 20110127 CAN Title에 $ 있으면 데이터 끝으로 처리 ///////////
			strTemp.TrimRight();
			if (strTemp.IsEmpty() || strTemp.Find("$") > 0) break;

			char *Buf = new char[strTemp.GetLength()+1];//lmh 20120319 문장의 길이가 50바이트 이상일경우를 대비해서 포인터형으로 선언
			char seps[] = ",";
			char * token;
			sprintf(Buf, strTemp);
			token = strtok(Buf, seps);

			//ljb 201012 CAN Title에 $ 오면 데이터 끝으로 처리 ///////////
			strTemp.Format("%s",token);
			strTemp.TrimLeft();
			strTemp.TrimRight();
			if (strTemp == "$") break;

			//////////////////////////////////////////////////////////////
			//20080407 CAN정보의 이름을 추가하도록 변경 kjh
			m_ArrayCanInfo[nIndex].chNo = atoi(token);
			token = strtok(NULL, seps);

			m_ArrayCanInfo[nIndex].canType = atoi(token);
			token = strtok(NULL, seps);

			if(m_ArrayCanInfo[nIndex].canType == PS_CAN_TYPE_MASTER)
				m_nCanMasterCount++;
			else if(m_ArrayCanInfo[nIndex].canType == PS_CAN_TYPE_SLAVE)
				m_nCanSlaveCount++;

			m_ArrayCanInfo[nIndex].canDataType = atoi(token);
			token = strtok(NULL, seps);
			
			if(token != NULL)
			{
				if(m_ArrayCanInfo[nIndex].canType == PS_CAN_TYPE_MASTER){
					//2014.09.05 캔이름 타입 수정
					//sprintf(m_ArrayCanInfo[nIndex].canName, "CAN[%03d]%s", m_ArrayCanInfo[nIndex].chNo, token);
					sprintf(m_ArrayCanInfo[nIndex].canName, "%s", token);
				}
				else{
					//2014.09.05 캔이름 타입 수정
					//sprintf(m_ArrayCanInfo[nIndex].canName, "CAN[%03d]%s", m_ArrayCanInfo[nIndex].chNo  + m_nCanMasterCount, token);
					sprintf(m_ArrayCanInfo[nIndex].canName, "%s", token);
				}					
			}
			else
			{
				if(m_ArrayCanInfo[nIndex].canType == PS_CAN_TYPE_MASTER)
					//2014.09.05 캔이름 타입 수정
					//sprintf(m_ArrayCanInfo[nIndex].canName, "CAN[%03d] Can_Name", m_ArrayCanInfo[nIndex].chNo);
					sprintf(m_ArrayCanInfo[nIndex].canName, "CAN[%03d]", m_ArrayCanInfo[nIndex].chNo);
				else
					//2014.09.05 캔이름 타입 수정
					//sprintf(m_ArrayCanInfo[nIndex].canName, "CAN[%03d] Can_Name", m_ArrayCanInfo[nIndex].chNo + m_nCanMasterCount);
					sprintf(m_ArrayCanInfo[nIndex].canName, "CAN[%03d]", m_ArrayCanInfo[nIndex].chNo + m_nCanMasterCount);
			}


			nIndex++;

			token = strtok(NULL, seps);
			nPos = nFind+1;
			nFind = strBuff.Find('\n', nFind+1);
			delete Buf;	
			Buf = NULL;
		}	
		
		fclose(fp);
	}
	m_nCanColumnCount = nIndex;	
}

int CChData::GetCanChNoOfIndex(int nCanIndex, int &nCanType)
{
	if(nCanIndex < 0 || nCanIndex > this->GetCanColumnCount()) return -1;
	nCanType = m_ArrayCanInfo[nCanIndex].canType;
	return m_ArrayCanInfo[nCanIndex].chNo;
}
/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 2014.08.11	이민규		canType - > canDataType 수정.
----------------------------------------------------------
*/
int CChData::GetCanTypeOfIndex(int nCanIndex)
{
	if(nCanIndex < 0 || nCanIndex > this->GetCanColumnCount()) return -1;

	return m_ArrayCanInfo[nCanIndex].canDataType;
}

//ljb start ------------------------------------------------------------------
int CChData::GetAuxChNoOfIndex(int nAuxIndex, int &nAuxType)
{
	if(nAuxIndex < 0 || nAuxIndex > this->GetAuxColumnCount()) return -1;
	nAuxType = m_ArrayAuxInfo[nAuxIndex].auxType;
	return m_ArrayAuxInfo[nAuxIndex].chNo;
}

int CChData::GetAuxTypeOfIndex(int nAuxIndex)
{
	if(nAuxIndex < 0 || nAuxIndex > this->GetAuxColumnCount()) return -1;
	
	return m_ArrayAuxInfo[nAuxIndex].auxType;
}

//ljb end ------------------------------------------------------------------

int CChData::GetCanColumnCount()
{
	return this->m_nCanColumnCount;
}

CString CChData::GetCanTitleOfIndex(int nCanIndex)
{
	CString strTitle = "";
//	strTitle.Format("%s", m_ArrayCanInfo[nCanIndex].canName); 
	//yulee 20180831-2
	//strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nCanIndex].chNo, RS_COL_CAN,   m_ArrayCanInfo[nCanIndex].canName); //yulee 20180831-2
//	switch(m_ArrayAuxInfo[nCanIndex].auxType) //yulee 20180903
//	{
//	case PS_CAN_Set1:		strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nCanIndex].chNo, RS_COL_CAN_S1,   m_ArrayCanInfo[nCanIndex].canName); break;//yulee 20180903
//	case PS_CAN_Set2:		strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nCanIndex].chNo, RS_COL_CAN_S2,   m_ArrayCanInfo[nCanIndex].canName); break;//yulee 20180903
//	case PS_CAN_Set3:		strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nCanIndex].chNo, RS_COL_CAN_S3,   m_ArrayCanInfo[nCanIndex].canName); break;//yulee 20180903
//	default :
//	strTitle.Format("[%003d]%s(%s)",m_ArrayAuxInfo[nCanIndex].chNo, RS_COL_CAN,   m_ArrayCanInfo[nCanIndex].canName); //yulee 20180831-2
	
	strTitle.Format("[%003d]%s(%s)",nCanIndex+1, RS_COL_CAN,   m_ArrayCanInfo[nCanIndex].canName); //yulee 20180831-2
	TRACE("%s", strTitle);
	//} 

	return strTitle;
}

CAN_VALUE * CChData::GetCanDataOfTable(LONG lIndex, LONG &lDataNum, int nCanIndex)
{
	CTable* pTable = NULL;

	POSITION pos = m_TableList.FindIndex(lIndex);
	if(pos == NULL)		return NULL;
	pTable = m_TableList.GetAt(pos);


	if(pTable == NULL)
		pTable->LoadData_v100B(m_strPath);

	return pTable->GetCanData(lDataNum, nCanIndex);
}

CAN_VALUE CChData::GetCanDatumOfTable(LONG tbIdx, INT row, int nCanIndex)
{
	CAN_VALUE value ;
	value.fVal[0] = 0.0f ;

	POSITION pos = m_TableList.FindIndex(tbIdx);
	if (pos == NULL)
	{
		ASSERT(FALSE) ;
		return value ;
	}

	CTable* pTable = m_TableList.GetAt(pos);
	if (pTable == NULL)
	{
		ASSERT(FALSE) ;
		// 뭐지??? NULL인데.. 함수를 호출하는건????
		pTable->LoadData_v100B(m_strPath);
	}
	
	return pTable->GetCanDatum(row, nCanIndex);
}