// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Step.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


void CStep::SetStepData_1014_V1_SCHto1013_V2_SCH(FILE_STEP_PARAM_V1014_v1_SCH *pStepData) //20180628 yulee
{
	int i;
	ClearStepData();

	m_StepIndex		= pStepData->chStepNo;
	m_lProcType		= pStepData->nProcType;	
	m_type			= pStepData->chType;		
	m_mode			= pStepData->chMode;		
	m_fVref_Charge	= pStepData->fVref_Charge;		
	m_fVref_DisCharge = pStepData->fVref_DisCharge;		
	m_fIref			= pStepData->fIref;		
	m_fPref			= pStepData->fPref;		
	m_fRref			= pStepData->fRref;

	m_fStartT		= pStepData->fStartT;
	m_fEndT			= pStepData->fEndT;
	m_fTref			= pStepData->fTref;
	m_fTrate		= pStepData->fTrate;
	m_fHref			= pStepData->fHref;
	m_fHrate		= pStepData->fHrate;

	m_fEndTime			= pStepData->fEndTime;	
	m_fEndV_H			= pStepData->fEndV	;
	m_fEndV_L			= pStepData->fEndV_L;
	m_fEndI				= pStepData->fEndI	;
	m_fEndC				= pStepData->fEndC	;
	m_fEndDV			= pStepData->fEndDV	;		
	m_fEndDI			= pStepData->fEndDI	;
	m_fEndW				= pStepData->fEndW	;
	m_fEndWh			= pStepData->fEndWh	;
	m_fCVTime			= pStepData->fCVTime;

	//ljb v1009 아이템별 Rate 로 분기     ///////////////////////////////////
	m_bValueItem	= pStepData->bValueItem	;			//사용할 ITEM  ( 0: 사용안함 , 1 : SOC, 2 : WattHour)
	m_bValueStepNo	= pStepData->bValueStepNo;			//비교할 기준 용량이 속한 Step번호
	m_fValueRate	= pStepData->fValueRate;			//비교 SOC Rate	
	//////////////////////////////////////////////////////////////////////////
	//ljb v100B 챔버 Prog모드, 챔버연동, Cycle pause,      ///////////////////////////////////
	m_bUseCyclePause	= pStepData->bUseCyclePause	;	//Cycle 완료 후 대기
	m_bUseChamberProg	= pStepData->bUseChamberProg;	//챔버 프로그램 모드로 운영
	m_bUseLinkStep		= pStepData->bUseLinkStep;		//다음 스텝과 연동 (v100B -> 사용 안함)
	//////////////////////////////////////////////////////////////////////////

	m_nLoopInfoCycle	=	pStepData->nLoopInfoCycle;	
	m_nLoopInfoGotoStep		=	pStepData->nLoopInfoGotoStep;	

	m_nMultiLoopGroupID	=	pStepData->nMultiLoopGroupID;
	m_nMultiLoopInfoCycle	=	pStepData->nMultiLoopInfoCycle;
	m_nMultiLoopInfoGotoStep	=	pStepData->nMultiLoopInfoGotoStep;

	m_nAccLoopGroupID	=	pStepData->nAccLoopGroupID;
	m_nAccLoopInfoCycle	=	pStepData->nAccLoopInfoCycle;
	m_nAccLoopInfoGotoStep	=	pStepData->nAccLoopInfoGotoStep;

	m_nGotoStepEndV_H = pStepData->nGotoStepEndV_H;
	m_nGotoStepEndV_L = pStepData->nGotoStepEndV_L;
	m_nGotoStepEndTime = pStepData->nGotoStepEndTime;
	m_nGotoStepEndCVTime = pStepData->nGotoStepEndCVTime;
	m_nGotoStepEndC = pStepData->nGotoStepEndC;
	m_nGotoStepEndWh = pStepData->nGotoStepEndWh;
	m_nGotoStepEndValue = pStepData->nGotoStepEndValue;

	m_fHighCapacitance	= pStepData->fCapacitanceHigh;
	m_fLowCapacitance	= pStepData->fCapacitanceLow;
	//항목 없음 			//fCellDeltaVStep;//yulee 20190531_4 //yulee 20190707
	m_fHighLimitV	=	pStepData->fVLimitHigh	;
	m_fLowLimitV	=	pStepData->fVLimitLow		;
	m_fHighLimitI	=	pStepData->fILimitHigh	;	
	m_fLowLimitI	=	pStepData->fILimitLow		;	
	m_fHighLimitC	=	pStepData->fCLimitHigh	;	
	m_fLowLimitC	=	pStepData->fCLimitLow		;	
	m_fHighLimitImp	=	pStepData->fImpLimitHigh	;
	m_fLowLimitImp	=	pStepData->fImpLimitLow	;	
	m_fHighLimitTemp	=	pStepData->fHighLimitTemp	;
	m_fLowLimitTemp	=	pStepData->fLowLimitTemp	;	

	m_fDeltaTimeV	=	pStepData->fDeltaTime		; 	
	m_fDeltaTimeI	=	pStepData->fDeltaTime1	;	
	m_fDeltaV		=	pStepData->fDeltaV		;		
	m_fDeltaI		=	pStepData->fDeltaI		;

	m_bGrade = (BYTE)pStepData->bGrade;
	m_Grading.ClearStep();
	//	m_Grading.m_lGradingItem = pStepData->sGrading_Val.lGradeItem;

	//	GRADE_STEP	grade_step;
	for(int i = 0; i<pStepData->sGrading_Val.chTotalGrade; i++)
	{	
		m_Grading.AddGradeStep(pStepData->sGrading_Val.lGradeItem, pStepData->sGrading_Val.faValue1[i], pStepData->sGrading_Val.faValue2[i], pStepData->sGrading_Val.aszGradeCode[i]);	//20081210 KHS
	}

	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		m_fCompLowV[i]	=pStepData->fCompVLow[i]	;
		m_fCompHighV[i]	=pStepData->fCompVHigh[i]	;	
		m_fCompTimeV[i]	=pStepData->fCompTimeV[i] 	;
		m_fCompLowI[i]	=pStepData->fCompILow[i]	;
		m_fCompHighI[i]	=pStepData->fCompIHigh[i]	;
		m_fCompTimeI[i]	=pStepData->fCompTimeI[i]	;
		i++;
	}

	//Not Use (Time 비교값으로 대치 가능 하므로 사용 안함
	//	stepData.fIEndHigh;		
	//	stepData.fIEndLow;			
	//	stepData.fVEndHigh;			
	//	stepData.fVEndLow;	

	m_fReportTemp = pStepData->fReportTemp;
	m_fReportV = pStepData->fReportV;
	m_fReportI = pStepData->fReportI;
	m_fReportTime = pStepData->fReportTime;


	m_fCapaVoltage1	 = pStepData->fCapaVoltage1;	
	m_fCapaVoltage2	 = pStepData->fCapaVoltage2;	

	m_fDCRStartTime = pStepData->fDCRStartTime ;	
	m_fDCREndTime = pStepData->fDCREndTime ;

	m_fLCStartTime = pStepData->fLCStartTime ;	
	m_fLCEndTime = pStepData->fLCEndTime ;		

	m_lRange = pStepData->lRange;			

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	m_lEndTimeDay = pStepData->lEndTimeDay; 
	m_lCVTimeDay = pStepData->lCVTimeDay;
	// -
	//////////////////////////////////////////////////////////////////////////
	//ljb 20101230
	for( int i=0; i < MAX_STEP_CAN_AUX_COMPARE_SIZE; i++)
	{
		m_ican_function_division[i] = pStepData->iBranchCanDivision[i];
		m_fcan_Value[i] = pStepData->fBranchCanValue[i];
		m_ican_data_type[i] = pStepData->cBranchCanDataType[i];
		m_ican_compare_type[i] = pStepData->cBranchCanCompareType[i];
		m_ican_branch[i] = pStepData->wBranchCanStep[i];

		m_iaux_function_division[i] = pStepData->iBranchAuxDivision[i];
		m_faux_Value[i] = pStepData->fBranchAuxValue[i];
		m_iaux_data_type[i] = pStepData->cBranchAuxDataType[i];
		m_iaux_compare_type[i] = pStepData->cBranchAuxCompareType[i];
		m_iaux_branch[i] = pStepData->wBranchAuxStep[i];
		m_iaux_conti_time[i] = pStepData->uaux_conti_time[i]; //yulee 20180905 
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	nValueLoaderItem = pStepData->nValueLoaderItem; 	//0: 사용안함 , 1: ON,	 2: OFF
	nValueLoaderMode = pStepData->nValueLoaderMode;	//0: CP, 1: CC, 2: CV, 3: CR
	fValueLoaderSet = pStepData->fValueLoaderSet;	//설정값 W,A,V,Ohm
	// -
	//////////////////////////////////////////////////////////////////////////
	m_nWaitTimeInit = pStepData->m_nWaitTimeInit;
	m_nWaitTimeDay = pStepData->m_nWaitTimeDay;
	m_nWaitTimeHour = pStepData->m_nWaitTimeHour;
	m_nWaitTimeMin = pStepData->m_nWaitTimeMin;
	m_nWaitTimeSec = pStepData->m_nWaitTimeSec;
	//////////////////////////////////////////////////////////////////////////
	//ljb 20170517 add
	m_fValueMax  = pStepData->m_fValueMax;		//20140207 add Pattern data 중 최대값
	m_fValueMin  = pStepData->m_fValueMin;		//20140207 add Pattern data 중 최소값 

	m_nNoCheckMode  = pStepData->m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	m_nCanTxOffMode  = pStepData->m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	m_nCanCheckMode  = pStepData->m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	m_bStepChamberStop	=	pStepData->m_bStepChamberStop;	//yulee 20180828

	CString tmpStr;
	tmpStr.Format("");

	m_strPatternFileName = pStepData->szSimulationFile; //ksj 20180528
}