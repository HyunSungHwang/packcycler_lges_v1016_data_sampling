/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: ChData.h

	Written out by 
	Evaluation Thechnology Group
	Kim, Sung-Hoon

	Comment: CChData class is defined
*************************************************************/

// ChData.h: interface for the CChData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHDATA_H__40068523_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CHDATA_H__40068523_EC8D_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include
class CTable;
class CScheduleData;
//class CPattern;

// #define

// Class definition
//---------------------------------------------------------------------------
//	Class Name: CChData
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  The first settlement
//---------------------------------------------------------------------------
class AFX_EXT_CLASS CChData  
{
////////////////
// Attributes //
////////////////
public:
	CString                          m_strPath;   // Path of Channel Data
//	CPattern*                        m_pPattern;  // Applied Pattern
	CScheduleData*                   m_pSchedule;  // Applied Pattern
	CTypedPtrList<CPtrList, CTable*> m_TableList; // Table List
	CWordArray m_wArrySaveItem;		//ljb 201012 저장된 항목 아이템
	
private:
	CString	m_strUserID;
	CString m_strDescript;
	CString m_strSerial;
	CString m_strEndT;
	CString m_strStartT;

//	char m_szEndT[64];
//	char m_szStartT[64];


///////////////////////////////////////////////////////////
// Operations: "Construction, Destruction, and Creation" //
///////////////////////////////////////////////////////////

public:
	CString		GetStartTime()	{		return m_strStartT;	}
	CString		GetEndTime()	{		return m_strEndT;	}
	CString		GetTestSerial()	{		return m_strSerial;	}
	CString		GetUserID()		{		return m_strUserID;	}
	CString		GetDescript()	{		return m_strDescript;	}
	long		GetFileVersion(){		return m_lFileVersion;}
	CChData()	{		
		m_pSchedule = NULL;	m_nAuxColumnCount = 0; m_nCanColumnCount = 0;
		m_nAuxTempCount = 0;m_nAuxVoltCount = 0; m_nCanMasterCount = 0; m_nCanSlaveCount = 0;
		m_TableArrayAuxInfo = NULL ;
		m_TableArrayCanInfo = NULL ;
	};
	CChData(LPCTSTR strPath);
	virtual ~CChData();
	BOOL Create(BOOL bIncludeWorkingStep = TRUE);
#if (1 == 0)
	BOOL CreateA(BOOL bIncludeWorkingStep = TRUE);
#endif
	BOOL CreateA2(BOOL bIncludeWorkingStep = TRUE);	// Added by 224 (2014/02/02)
	
	// Added by 224 (2014/01/23) : 그래프 자료 로딩 개선
	INT m_nFrom ;	// 1 based
	INT m_nTo ;

////////////////////////////////////////////////////////
// Operations: "Functions to access member variables" //
////////////////////////////////////////////////////////

public:

	// m_strPath
	CString GetPath() { return m_strPath; };

	// m_pPattern
	CScheduleData* GetPattern();

///////////////////////////////////
// Operations: "Other functions" //
///////////////////////////////////

public:

	int m_nAuxVoltCount;
	int m_nAuxTempCount;
	int m_nAuxTempTHCount;
	int m_nAuxHumiCount; //ksj 20200116 : V1016 습도 센서 추가
	int m_nCanMasterCount;
	int m_nCanSlaveCount;
	int m_nCanColumnCount;
	int m_nAuxColumnCount;
	long	m_lFileVersion;
	CAN_VALUE* GetCanDataOfTable(LONG lIndex, LONG &lDataNum, int nCanIndex);
	CAN_VALUE GetCanDatumOfTable(LONG tbIdx, INT row, int nCanIndex) ;
	CString GetCanTitleOfIndex(int nCanIndex);
	int GetCanColumnCount();
	int GetCanTypeOfIndex(int nCanIndex);
	int GetCanChNoOfIndex(int nCanIndex, int &nCanType);
	int GetAuxColumnCount();
	int GetAuxTypeOfIndex(int nAuxIndex);
	int GetAuxChNoOfIndex(int nAuxIndex, int &nAuxType);
	void LoadCANTitle(CString strCanConfigPath);
	void LoadAuxTitle(CString strAuxConfigPath);
	fltPoint * GetAuxDataOfTable(LONG lIndex, LONG lDataNum, int nAuxIndex);
	CString GetAuxTitleOfIndex(int nAuxIndex);
	int GetChannelNo();
	int GetModuleID();
	CString GetTestName();
	
	//20100430
	//	static CTable * GetLastDataRecord(CString strFileName);
	static CTable * GetLastDataRecord(CString strFileName, long &lEndTableSize, CChData* pParent);

	CTable * GetTableAt(LONG lIndex);
	CTable * GetNextTable(POSITION &pos);
	POSITION GetFirstTablePosition();
	CWordArray *	GetRecordItemList(LONG lIndex);
	CWordArray *	GetSaveRecordItemList();	//ljb 201012
	CString GetLogFileName();
	float	GetCapacitySum(int nStepIndex = -1);
	long	GetTotalRecordCount()	{	return	m_nTotalRecordCount;	}	//Step start data가 포함이 안된 record의 크기
	BOOL ReLoadData();
	long GetFirstTableIndexOfCycle(LONG lCycleID);
	LONG GetTableIndex(int nStepNo, int lCycleNo);	//ljb 201012
	LONG	GetTableNo(int nIndex);
	int		GetTableSize()	{	return m_TableList.GetCount();	};
	BOOL	IsOKCell();
	void	SetPath(CString strPath)	{ m_strPath = strPath;	};
	BOOL	IsLoadedData()	{	if(m_strStartT.IsEmpty())	return FALSE;		return TRUE; };

	float	  GetLastDataOfCycle(LONG lCycleID, DWORD dwMode, LPCTSTR strYAxisTitle);
	float     GetLastDataOfTable(LONG lIndex, LPCTSTR strTitle, WORD wPoint=0x0000);

	fltPoint* GetDataOfTable(LONG lIndex, LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);

	// Added by 224 (2014/07/28) : 지정된 테이블에서, 지정한 컬럼의 값을 가져온다.
	// 채널데이터(data)에는 메모리에 모든 데이터를 보관하고 있다. (없으면 로딩)
	// 보관된 데이터에 컬럼별로 또다른 메모리를 할당하고, 해당 컬럼값에 어레이로 보관한다.
	// 채널데이터 클래스에 GetDatumOfTable(cnt, column, index)를 구현하고 한 row만 가져오는 방법 시도
	// 프로그래시브 에도 각 행/전체행 에 대한 표시
	// lMaxDataCount 값은 따로 얻는다. data.GetMaxDataCount(nTable) ;
	INT GetDataCount(LONG tbIdx) ;
	fltPoint GetDatumOfTable(LONG tbIdx, LPCTSTR strYAxisTitle, INT index, WORD wPoint=0x0000);

	//20100430	KBH 선택 Step만 Dsiplay 기능
	//	fltPoint* GetDataOfCycle(LONG lCycleID, LONG& lDataNum, DWORD dwMode, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);
	fltPoint* GetDataOfCycle(LONG lCycleID, LONG& lDataNum, DWORD dwMode, LONG lQueryMode, CList<LONG, LONG&>* pdwListStep, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint=0x0000);

protected:
//	float m_fCapacitySum;
	long m_nTotalRecordCount;
	int	 m_AuxInfo[512][2];		//[0] : Aux Ch Num		[1] : Aux Type
	int  m_CanInfo[512][3];		//[0] ; CAN Ch Num		[1] : Can type		[2] : Data Type		-> 사용 안함

	
	void ResetData();

public:
	BOOL CreateTable(BOOL bIncludeWorkingStep);

	BOOL CreateDataTable(BOOL bIncludeWorkingStep);
	// Added by 224 (2014/02/02) : CTable 의 ArrayAuxInfo, CanInfo 를 중복하여
	// 사용하는것으로 판단하여 Channel 로 이동함.

	// 아래를 사용하면 Stack Overflow 발생
// 	PS_AUX_DISP_INFO	 m_TableArrayAuxInfo[1];
// 	PS_CAN_DISP_INFO	 m_TableArrayCanInfo[1];
	PS_AUX_DISP_INFO* m_TableArrayAuxInfo;	
	PS_CAN_DISP_INFO* m_TableArrayCanInfo;
	
	PS_AUX_DISP_INFO	 m_ArrayAuxInfo[512];	//2014.10.22 protected - > public변경 
	PS_CAN_DISP_INFO	 m_ArrayCanInfo[512];

	
//	friend class CTable ;

	BOOL DeleteTableData(int tbIdx); //ksj 20210419
};

#endif // !defined(AFX_CHDATA_H__40068523_EC8D_11D4_88DF_006008CEDA07__INCLUDED_)
