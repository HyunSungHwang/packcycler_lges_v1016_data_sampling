// ScheduleData.h: interface for the CScheduleData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_)
#define AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Step.h"
#include <afxdao.h>



#define CT_CONFIG_REG_SEC "Config"

class AFX_EXT_CLASS CScheduleData  
{
public:
	int Fun_ISEmptyCommSafety();			//  [9/28/2011 XNOTE]
	//BOOL AddStep(FILE_STEP_PARAM_V100D *pStepData);
	BOOL AddStep_vD(FILE_STEP_PARAM_V100D *pStepData);
	BOOL AddStep_vDL(FILE_STEP_PARAM_V100D_LOAD *pStepData);
	BOOL AddStep_vDL_SCH(FILE_STEP_PARAM_V100D_LOAD_SCH *pStepData); //20180627 yulee
	BOOL AddStep_vF(FILE_STEP_PARAM_V100F *pStepData);
	BOOL AddStep_vFv2(FILE_STEP_PARAM_V100F_v2 *pStepData); //ksj 20171123
	BOOL AddStep_vF_v2_SCH(FILE_STEP_PARAM_V100F_v2_SCH *pStepData); //ksj 20180528
	BOOL AddStep_v1011_v1_SCH(FILE_STEP_PARAM_V1011_v1_SCH *pStepData); //yulee 20180828
	BOOL AddStep_v1011_v2_SCH(FILE_STEP_PARAM_V1011_v2_SCH *pStepData); //yulee 20181002
	BOOL AddStep_v1013_v1_SCH(FILE_STEP_PARAM_V1013_v1_SCH *pStepData); //yulee 20190707
	BOOL AddStep_v1013_v2_SCH(FILE_STEP_PARAM_V1013_v2_SCH *pStepData); //yulee 20190707
	BOOL AddStep_v1014_v1_SCH(FILE_STEP_PARAM_V1014_v1_SCH *pStepData); //yulee 20190707

	void CellCheck_v1013_v2_SCH(FILE_CELL_CHECK_PARAM_V1013_v2_SCH *pStepData);


	int GetCurrentStepCycle(int nStepIndex);
	BOOL IsThereGradingStep();
	BOOL IsThereImpStep();
	BOOL SaveToExcelFile(CString strFileName, BOOL bOpenExcel = TRUE, BOOL bCheckMode = FALSE);
	void ShowContent(int nStepIndex = 0);
	CStep* GetStepData(UINT nStepIndex);
	void ResetData();
	UINT GetStepSize();
	BOOL SaveToFile(CString strFileName, CScheduleData *pSchedule = NULL);
	BOOL SaveToFile_v1013(CString strFileName, CScheduleData *pSchedule = NULL); //ksj 20210113
	BOOL SaveToFile_v1015(CString strFileName, CScheduleData *pSchedule = NULL); //ksj 20210113
	
	/*BOOL Fun_SavePatternFileToSch(FILE *pFile, int nStepNo,CString strFileName);	//ksj 20180528 : 패턴파일 sch 파일에 같이 저장 (울산 SDI 고속형 Cycler 발췌)
	BOOL Fun_SavePwrSupplyFileToSch(FILE *pFile, CStep *pStep);
	BOOL Fun_SaveChillerFileToSch(FILE *pFile, CStep *pStep);
	BOOL Fun_SaveCellBALFileToSch(FILE *pFile, CStep *pStep);*/
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.21
	BOOL SaveToFile_ForFTP(CString strFileName, CScheduleData *pSchedule);
	// -
	//////////////////////////////////////////////////////////////////////////
	//BOOL SaveToFile(CString strFileName,CScheduleData *pSchedule);	//ljb 2011415 이재복 //////////
	BOOL SetSchedule(CString strDBName, long lModelPK, long lTestPK);
	BOOL SetSchedule(CString strFileName);
	BOOL Fun_SetScheduleFromSch(CString strFileName);			//ksj 20180528 : 저장되어 있는 pattern 파일까지 가져오기
	CScheduleData();
	CScheduleData(CString strFileName);
	virtual ~CScheduleData();

	static BOOL	   ExecuteEditor(long lModelPK, long lTestPK);
	static CString GetTypeString(int nType);
	static CString GetModeString(int nType, int nMode);

	//Model Information
	LPCTSTR GetModelName()		{	return m_ModelData.szName;			}
	LPCTSTR	GetModelEditTime()	{	return m_ModelData.szModifiedTime;	}
	long	GetModelPK()		{	return m_ModelData.lID;				}
	LPCTSTR GetModelDescript()	{	return m_ModelData.szDescription;	}
//	LPCTSTR	GetModelCreator()	{	return m_ModelData.szCreator;		}

	//Schedule Information
	LPCTSTR GetScheduleName()	{	return m_ProcedureData.szName;		}
	LPCTSTR	GetScheduleEditTime()	{	return m_ProcedureData.szModifiedTime;		}
	long	GetSchedulePK()		{	return m_ProcedureData.lID;						}
	LPCTSTR GetScheduleDescript()	{	return	m_ProcedureData.szDescription;		}
	LPCTSTR GetScheduleCreator()	{	return m_ProcedureData.szCreator;			}
	long	GetScheduleType()	{	return m_ProcedureData.lType;			}

	BOOL ExecuteExcel(CString strFileName);
	CELL_CHECK_DATA * GetCellCheckParam()	{	return &m_CellCheck;	}
	float GetCellDeltaVolt() 	{	return m_CellCheck.fCellDeltaV;	}

	CELL_CHECK_DATA				m_CellCheck;
	//////////////////////////////////////////////////////////////////////////
	// + BW KIm 2014.02.20 추가 m_CellCheck 안의 구조체 사용 부분을 함부로 건드릴 수 없어서
	CELL_CHECK_DATA_SMALL		m_CellCheck_Small;
	// - 
	//////////////////////////////////////////////////////////////////////////


	SCHDULE_INFORMATION_DATA	m_ModelData;
	SCHDULE_INFORMATION_DATA	m_ProcedureData;
	CPtrArray	m_apStepArray;
	BOOL	m_bContinueCellCode;

protected:
	CString GetExcelPath();
	BOOL LoadGradeInfo(CDaoDatabase &db, CStep *pStepData);
	BOOL LoadCellCheckInfo(CDaoDatabase &db, long lProcPK);
	BOOL LoadStepInfo(CDaoDatabase &db, long lProcPK);
	BOOL LoadProcedureInfo(CDaoDatabase &db, long lModelPK, long lProcPK);
	BOOL LoadModelInfo(CDaoDatabase &db, long lPK);

	


};

#endif // !defined(AFX_SCHEDULEDATA_H__707CB6FF_2156_4365_87AD_1D2295496A0E__INCLUDED_)
