// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Step.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStep::CStep(int nStepType /*=0*/)
{
	ClearStepData();
	m_type = (BYTE)nStepType;
}

CStep::CStep()
{
	ClearStepData();
}

CStep::~CStep()
{

}

BOOL CStep::ClearStepData()
{
	m_type = 0;
	m_StepIndex = 0;
	m_mode = 0;
	m_bGrade = FALSE;
	m_lProcType = 0;				
	m_fVref_Charge = 0;
	m_fVref_DisCharge = 0;
    m_fIref = 0;
    m_fPref = 0;
	m_fRref = 0;

	m_fStartT = 0.0f;
	m_fEndT = 0.0f;
	m_fTref = 0.0f;
	m_fTrate = 0.0f;
	m_fHref = 0.0f;
	m_fHrate = 0.0f;
    
	m_fEndTime = 0;
    m_fEndV_H = 0;
	m_fEndV_L = 0;
    m_fEndI = 0;
    m_fEndC = 0;
    m_fEndDV = 0;
    m_fEndDI = 0;
	m_fEndW = 0.0f;
	m_fEndWh = 0.0f;

	m_bValueItem = 0;					//현재 Step의 용량값을 기준 용량으로 사용 할지 여부 
	m_bValueStepNo = 0;					//비교할 기준 용량이 속한 Step번호
	m_fValueRate = 0;						//비교 SOC Rate


	m_nLoopInfoCycle = 0;	
	m_nLoopInfoGotoStep = 0;	

	m_nMultiLoopGroupID = 0;
	m_nMultiLoopInfoCycle = 0;
	m_nMultiLoopInfoGotoStep = 0;

	m_nAccLoopGroupID = 0;
	m_nAccLoopInfoCycle = 0;
	m_nAccLoopInfoGotoStep = 0;

	m_nGotoStepEndV_H = 0;
	m_nGotoStepEndV_L = 0;
	m_nGotoStepEndTime = 0;
	m_nGotoStepEndCVTime = 0;
	m_nGotoStepEndC = 0;
	m_nGotoStepEndWh = 0;
	m_nGotoStepEndValue = 0;

	m_fHighLimitV = 0;
    m_fLowLimitV = 0;
    m_fHighLimitI = 0;
    m_fLowLimitI = 0;
    m_fHighLimitC = 0;
    m_fLowLimitC = 0;
	m_fHighCapacitance = 0;
	m_fLowCapacitance = 0;
	m_fHighLimitImp = 0;
    m_fLowLimitImp = 0;
	m_fHighLimitTemp = 0.0f;
	m_fLowLimitTemp = 0.0f; 
	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = 0;			
		m_fCompHighV[i] = 0.0f;
		m_fCompLowV[i] = 0.0f;
		m_fCompTimeI[i] = 0;			
		m_fCompHighI[i] = 0.0f;
		m_fCompLowI[i] = 0.0f;
	}
    m_fDeltaTimeV = 0;
    m_fDeltaV = 0.0f;
    m_fDeltaTimeI = 0;
    m_fDeltaI = 0.0f;

	m_fReportTemp = 0.0f;
	m_fReportV = 0.0f;
	m_fReportI = 0.0f;
	m_fReportTime = 0;

	m_fCapaVoltage1		=0;	
	m_fCapaVoltage2		=0;	

	m_fDCRStartTime	=0;	
	m_fDCREndTime		=0;

	m_fLCStartTime		=0;	
	m_fLCEndTime		=0;		

	m_lRange		=0;			

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	m_lEndTimeDay = 0;
	m_lCVTimeDay = 0;
	// -
	//////////////////////////////////////////////////////////////////////////
	m_Grading.ClearStep();
	m_StepID = 0;

	m_strPatternFileName.Empty();
	m_lValueLimitLow=0;
	m_lValueLimitHigh=0;
	
	m_lEndTimeDay=0;		//ljb 20131212 add
	m_lCVTimeDay=0;		//ljb 20131212 add
	
	m_fValueMax=0;		//20140207 add Pattern data 중 최대값
	m_fValueMin=0;		//20140207 add Pattern data 중 최소값 
	
	m_nNoCheckMode=0;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	m_nCanTxOffMode=0;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	m_nCanCheckMode=0;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	nValueLoaderItem=0;		//0: 사용안함 , 1: ON,	 2: OFF
	nValueLoaderMode=0;		//0: CP, 1: CC, 2: CV, 3: CR
	fValueLoaderSet=0.0f;		//설정값 W,A,V,Ohm

	m_nWaitTimeInit=0;		
	m_nWaitTimeDay=0;		//대기 일
	m_nWaitTimeHour=0;	//대기 시간
	m_nWaitTimeMin=0;		//대기  분
	m_nWaitTimeSec=0;		//대기  초

	m_bStepChamberStop=0; //yulee 20180829

	m_nCellBal_CircuitRes=0;
	m_nCellBal_WorkVoltUpper=0;//mV
	m_nCellBal_WorkVolt=0;//mV
	m_nCellBal_StartVolt=0;//mV
	m_nCellBal_EndVolt=0;//mV
	m_nCellBal_AutoStopTime=0;
	m_nCellBal_State=0;//Use_State
	
	//cny PowerSupply
	m_nPwrSupply_Cmd=0;//1-ON,2-OFF,0-NONE
	m_nPwrSupply_Volt=0;//mV
	m_nPwrSupply_State=0;//Use_State
	
	//cny Chiller
	m_nChiller_Cmd=0;
	m_fChiller_RefTemp=0;
	m_nChiller_RefCmd=0;
	m_nChiller_PumpCmd=0;
	m_fChiller_RefPump=0;
	m_nChiller_TpData=0;
	m_fChiller_ONTemp1=0;
	m_nChiller_ONTemp1_Cmd=0;
	m_fChiller_ONTemp2=0;
	m_nChiller_ONTemp2_Cmd=0;
	m_fChiller_OFFTemp1=0;
	m_nChiller_OFFTemp1_Cmd=0;
	m_fChiller_OFFTemp2=0;
	m_nChiller_OFFTemp2_Cmd=0;
	m_nChiller_State=0;//Use_State
	m_nChiller_Log=0;
	
	return TRUE;
}

void CStep::operator=(CStep &step) 
{
	m_type = step.m_type;
	m_StepIndex = step.m_StepIndex;
	m_mode = step.m_mode;
	m_bGrade = step.m_bGrade;
	m_lProcType = step.m_lProcType;				
	m_fVref_Charge = step.m_fVref_Charge;
	m_fVref_DisCharge = step.m_fVref_DisCharge;
    m_fIref = step.m_fIref;
    m_fPref = step.m_fPref;
	m_fRref = step.m_fRref;

	m_fStartT	=step.m_fStartT;
	m_fEndT		= step.m_fEndT;
	m_fTref		= step.m_fTref;
	m_fTrate	= step.m_fTrate;
	m_fHref		= step.m_fHref;
	m_fHrate	= step.m_fHrate;
    
	m_fEndTime = step.m_fEndTime;
    m_fEndV_H = step.m_fEndV_H;
	m_fEndV_L = step.m_fEndV_L;
    m_fEndI = step.m_fEndI;
    m_fEndC = step.m_fEndC;
    m_fEndDV = step.m_fEndDV;
    m_fEndDI = step.m_fEndDI;
	m_fEndW	 = step.m_fEndW;
	m_fEndWh = step.m_fEndWh;

	m_bValueItem = step.m_bValueItem;
	m_bValueStepNo = step.m_bValueStepNo;
	m_fValueRate = step.m_fValueRate;

	m_nLoopInfoCycle = step.m_nLoopInfoCycle;	
	m_nLoopInfoGotoStep = step.m_nLoopInfoGotoStep;	

	m_nMultiLoopGroupID	= step.m_nMultiLoopGroupID;
	m_nMultiLoopInfoCycle	= step.m_nMultiLoopInfoCycle;
	m_nMultiLoopInfoGotoStep	= step.m_nMultiLoopInfoGotoStep;

	m_nAccLoopGroupID	= step.m_nAccLoopGroupID;
	m_nAccLoopInfoCycle	= step.m_nAccLoopInfoCycle;
	m_nAccLoopInfoGotoStep	= step.m_nAccLoopInfoGotoStep;

	m_nGotoStepEndV_H = step.m_nGotoStepEndV_H;
	m_nGotoStepEndV_L = step.m_nGotoStepEndV_L;
	m_nGotoStepEndTime = step.m_nGotoStepEndTime;
	m_nGotoStepEndCVTime = step.m_nGotoStepEndCVTime;
	m_nGotoStepEndC = step.m_nGotoStepEndC;
	m_nGotoStepEndWh = step.m_nGotoStepEndWh;
	m_nGotoStepEndValue = step.m_nGotoStepEndValue;

	m_fHighLimitV = step.m_fHighLimitV;
    m_fLowLimitV = step.m_fLowLimitV;
    m_fHighLimitI = step.m_fHighLimitI;
    m_fLowLimitI = step.m_fLowLimitI;
    m_fHighLimitC = step.m_fHighLimitC;
	m_fHighCapacitance	= step.m_fHighCapacitance;
	m_fLowCapacitance	= step.m_fLowCapacitance;
    m_fLowLimitC = step.m_fLowLimitC;
	m_fHighLimitImp = step.m_fHighLimitImp;
    m_fLowLimitImp = step.m_fLowLimitImp;
	m_fHighLimitTemp = step.m_fHighLimitTemp;
	m_fLowLimitTemp = step.m_fLowLimitTemp;

	for(int i=0; i<PS_MAX_COMP_POINT; i++)
	{
		m_fCompTimeV[i] = step.m_fCompTimeV[i];			
		m_fCompHighV[i] = step.m_fCompHighV[i];
		m_fCompLowV[i] = step.m_fCompLowV[i];
		m_fCompTimeI[i] = step.m_fCompTimeI[i];			
		m_fCompHighI[i] = step.m_fCompHighI[i];
		m_fCompLowI[i] = step.m_fCompLowI[i];
	}
    m_fDeltaTimeV = step.m_fDeltaTimeV;
    m_fDeltaV = step.m_fDeltaV;
    m_fDeltaTimeI = step.m_fDeltaTimeI;
    m_fDeltaI = step.m_fDeltaI;

	m_fReportTemp = step.m_fReportTemp;
	m_fReportV	=	step.m_fReportV;
	m_fReportI	=	step.m_fReportI;
	m_fReportTime	=	step.m_fReportTime;

	m_fCapaVoltage1		=step.m_fCapaVoltage1;	
	m_fCapaVoltage2		=step.m_fCapaVoltage2;	

	m_fDCRStartTime	=step.m_fDCRStartTime;	
	m_fDCREndTime		=step.m_fDCREndTime;

	m_fLCStartTime		=step.m_fLCStartTime;	
	m_fLCEndTime		=step.m_fLCEndTime;		

	m_lRange		=step.m_lRange;		
	
	m_bStepChamberStop	= step.m_bStepChamberStop; //yulee 20180828


	m_Grading.SetGradingData(step.m_Grading);

	m_StepID = step.m_StepID;

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	m_lEndTimeDay = step.m_lEndTimeDay;
	m_lCVTimeDay = step.m_lCVTimeDay;
	// -
	//////////////////////////////////////////////////////////////////////////

	GRADE_STEP data;
	m_Grading.ClearStep();
	for(int i=0; i<step.m_Grading.GetGradeStepSize(); i++)
	{
		data = step.m_Grading.GetStepData(i);
		m_Grading.AddGradeStep(data.lGradeItem, data.fMin, data.fMax, data.strCode);	//20081210 KHS
	}

	m_strPatternFileName = step.m_strPatternFileName;
	m_lValueLimitLow = step.m_lValueLimitLow;
	m_lValueLimitHigh = step.m_lValueLimitHigh;
	
	m_lEndTimeDay = step.m_lEndTimeDay;		//ljb 20131212 add
	m_lCVTimeDay = step.m_lCVTimeDay;		//ljb 20131212 add
	
	m_fValueMax = step.m_fValueMax;		//20140207 add Pattern data 중 최대값
	m_fValueMin = step.m_fValueMin;		//20140207 add Pattern data 중 최소값 
	
	m_nNoCheckMode = step.m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	m_nCanTxOffMode = step.m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	m_nCanCheckMode = step.m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	nValueLoaderItem = step.nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	nValueLoaderMode = step.nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	fValueLoaderSet = step.fValueLoaderSet;		//설정값 W,A,V,Ohm
	
	m_nWaitTimeInit = step.m_nWaitTimeInit;		
	m_nWaitTimeDay = step.m_nWaitTimeDay;		//대기 일
	m_nWaitTimeHour = step.m_nWaitTimeHour;		//대기 시간
	m_nWaitTimeMin = step.m_nWaitTimeMin;		//대기  분
	m_nWaitTimeSec = step.m_nWaitTimeSec;		//대기  초
}

















void CStep::SetStepData_1011_V1_SCHto1013_V1_SCH(FILE_STEP_PARAM_V1011_v1_SCH *pStepData) //20180628 yulee
{
	int i;
	ClearStepData();

	m_StepIndex		= pStepData->chStepNo;
	m_lProcType		= pStepData->nProcType;	
	m_type			= pStepData->chType;		
	m_mode			= pStepData->chMode;		
	m_fVref_Charge	= pStepData->fVref_Charge;		
	m_fVref_DisCharge = pStepData->fVref_DisCharge;		
	m_fIref			= pStepData->fIref;		
	m_fPref			= pStepData->fPref;		
	m_fRref			= pStepData->fRref;

	m_fStartT		= pStepData->fStartT;
	m_fEndT			= pStepData->fEndT;
	m_fTref			= pStepData->fTref;
	m_fTrate		= pStepData->fTrate;
	m_fHref			= pStepData->fHref;
	m_fHrate		= pStepData->fHrate;

	m_fEndTime			= pStepData->fEndTime;	
	m_fEndV_H			= pStepData->fEndV	;
	m_fEndV_L			= pStepData->fEndV_L;
	m_fEndI				= pStepData->fEndI	;
	m_fEndC				= pStepData->fEndC	;
	m_fEndDV			= pStepData->fEndDV	;		
	m_fEndDI			= pStepData->fEndDI	;
	m_fEndW				= pStepData->fEndW	;
	m_fEndWh			= pStepData->fEndWh	;
	m_fCVTime			= pStepData->fCVTime;

	//ljb v1009 아이템별 Rate 로 분기     ///////////////////////////////////
	m_bValueItem	= pStepData->bValueItem	;			//사용할 ITEM  ( 0: 사용안함 , 1 : SOC, 2 : WattHour)
	m_bValueStepNo	= pStepData->bValueStepNo;			//비교할 기준 용량이 속한 Step번호
	m_fValueRate	= pStepData->fValueRate;			//비교 SOC Rate	
	//////////////////////////////////////////////////////////////////////////
	//ljb v100B 챔버 Prog모드, 챔버연동, Cycle pause,      ///////////////////////////////////
	m_bUseCyclePause	= pStepData->bUseCyclePause	;	//Cycle 완료 후 대기
	m_bUseChamberProg	= pStepData->bUseChamberProg;	//챔버 프로그램 모드로 운영
	m_bUseLinkStep		= pStepData->bUseLinkStep;		//다음 스텝과 연동 (v100B -> 사용 안함)
	//////////////////////////////////////////////////////////////////////////

	m_nLoopInfoCycle	=	pStepData->nLoopInfoCycle;	
	m_nLoopInfoGotoStep		=	pStepData->nLoopInfoGotoStep;	

	m_nMultiLoopGroupID	=	pStepData->nMultiLoopGroupID;
	m_nMultiLoopInfoCycle	=	pStepData->nMultiLoopInfoCycle;
	m_nMultiLoopInfoGotoStep	=	pStepData->nMultiLoopInfoGotoStep;

	m_nAccLoopGroupID	=	pStepData->nAccLoopGroupID;
	m_nAccLoopInfoCycle	=	pStepData->nAccLoopInfoCycle;
	m_nAccLoopInfoGotoStep	=	pStepData->nAccLoopInfoGotoStep;

	m_nGotoStepEndV_H = pStepData->nGotoStepEndV_H;
	m_nGotoStepEndV_L = pStepData->nGotoStepEndV_L;
	m_nGotoStepEndTime = pStepData->nGotoStepEndTime;
	m_nGotoStepEndCVTime = pStepData->nGotoStepEndCVTime;
	m_nGotoStepEndC = pStepData->nGotoStepEndC;
	m_nGotoStepEndWh = pStepData->nGotoStepEndWh;
	m_nGotoStepEndValue = pStepData->nGotoStepEndValue;

	m_fHighCapacitance	= pStepData->fCapacitanceHigh;
	m_fLowCapacitance	= pStepData->fCapacitanceLow;
	m_fCellDeltaVStep	= 0.0;//yulee 20190531_4
	m_fHighLimitV	=	pStepData->fVLimitHigh	;
	m_fLowLimitV	=	pStepData->fVLimitLow		;
	m_fHighLimitI	=	pStepData->fILimitHigh	;	
	m_fLowLimitI	=	pStepData->fILimitLow		;	
	m_fHighLimitC	=	pStepData->fCLimitHigh	;	
	m_fLowLimitC	=	pStepData->fCLimitLow		;	
	m_fHighLimitImp	=	pStepData->fImpLimitHigh	;
	m_fLowLimitImp	=	pStepData->fImpLimitLow	;	
	m_fHighLimitTemp	=	pStepData->fHighLimitTemp	;
	m_fLowLimitTemp	=	pStepData->fLowLimitTemp	;	

	m_fDeltaTimeV	=	pStepData->fDeltaTime		; 	
	m_fDeltaTimeI	=	pStepData->fDeltaTime1	;	
	m_fDeltaV		=	pStepData->fDeltaV		;		
	m_fDeltaI		=	pStepData->fDeltaI		;

	m_bGrade = (BYTE)pStepData->bGrade;
	m_Grading.ClearStep();
	//	m_Grading.m_lGradingItem = pStepData->sGrading_Val.lGradeItem;

	//	GRADE_STEP	grade_step;
	for(i =0; i<pStepData->sGrading_Val.chTotalGrade; i++)
	{	
		m_Grading.AddGradeStep(pStepData->sGrading_Val.lGradeItem, pStepData->sGrading_Val.faValue1[i], pStepData->sGrading_Val.faValue2[i], pStepData->sGrading_Val.aszGradeCode[i]);	//20081210 KHS
	}

	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		m_fCompLowV[i]	=pStepData->fCompVLow[i]	;
		m_fCompHighV[i]	=pStepData->fCompVHigh[i]	;	
		m_fCompTimeV[i]	=pStepData->fCompTimeV[i] 	;
		m_fCompLowI[i]	=pStepData->fCompILow[i]	;
		m_fCompHighI[i]	=pStepData->fCompIHigh[i]	;
		m_fCompTimeI[i]	=pStepData->fCompTimeI[i]	;
		i++;
	}

	//Not Use (Time 비교값으로 대치 가능 하므로 사용 안함
	//	stepData.fIEndHigh;		
	//	stepData.fIEndLow;			
	//	stepData.fVEndHigh;			
	//	stepData.fVEndLow;	

	m_fReportTemp = pStepData->fReportTemp;
	m_fReportV = pStepData->fReportV;
	m_fReportI = pStepData->fReportI;
	m_fReportTime = pStepData->fReportTime;


	m_fCapaVoltage1	 = pStepData->fCapaVoltage1;	
	m_fCapaVoltage2	 = pStepData->fCapaVoltage2;	

	m_fDCRStartTime = pStepData->fDCRStartTime ;	
	m_fDCREndTime = pStepData->fDCREndTime ;

	m_fLCStartTime = pStepData->fLCStartTime ;	
	m_fLCEndTime = pStepData->fLCEndTime ;		

	m_lRange = pStepData->lRange;			

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	m_lEndTimeDay = pStepData->lEndTimeDay; 
	m_lCVTimeDay = pStepData->lCVTimeDay;
	// -
	//////////////////////////////////////////////////////////////////////////
	//ljb 20101230
	for (i=0; i < MAX_STEP_CAN_AUX_COMPARE_SIZE; i++)
	{
		m_ican_function_division[i] = pStepData->iBranchCanDivision[i];
		m_fcan_Value[i] = pStepData->fBranchCanValue[i];
		m_ican_data_type[i] = pStepData->cBranchCanDataType[i];
		m_ican_compare_type[i] = pStepData->cBranchCanCompareType[i];
		m_ican_branch[i] = pStepData->wBranchCanStep[i];

		m_iaux_function_division[i] = pStepData->iBranchAuxDivision[i];
		m_faux_Value[i] = pStepData->fBranchAuxValue[i];
		m_iaux_data_type[i] = pStepData->cBranchAuxDataType[i];
		m_iaux_compare_type[i] = pStepData->cBranchAuxCompareType[i];
		m_iaux_branch[i] = pStepData->wBranchAuxStep[i];
		m_iaux_conti_time[i] = pStepData->uaux_conti_time[i]; //yulee 20180905 
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	nValueLoaderItem = pStepData->nValueLoaderItem; 	//0: 사용안함 , 1: ON,	 2: OFF
	nValueLoaderMode = pStepData->nValueLoaderMode;	//0: CP, 1: CC, 2: CV, 3: CR
	fValueLoaderSet = pStepData->fValueLoaderSet;	//설정값 W,A,V,Ohm
	// -
	//////////////////////////////////////////////////////////////////////////
	m_nWaitTimeInit = pStepData->m_nWaitTimeInit;
	m_nWaitTimeDay = pStepData->m_nWaitTimeDay;
	m_nWaitTimeHour = pStepData->m_nWaitTimeHour;
	m_nWaitTimeMin = pStepData->m_nWaitTimeMin;
	m_nWaitTimeSec = pStepData->m_nWaitTimeSec;
	//////////////////////////////////////////////////////////////////////////
	//ljb 20170517 add
	m_fValueMax  = pStepData->m_fValueMax;		//20140207 add Pattern data 중 최대값
	m_fValueMin  = pStepData->m_fValueMin;		//20140207 add Pattern data 중 최소값 

	m_nNoCheckMode  = pStepData->m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	m_nCanTxOffMode  = pStepData->m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	m_nCanCheckMode  = pStepData->m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	m_bStepChamberStop	=	pStepData->m_bStepChamberStop;	//yulee 20180828

	CString tmpStr;
	tmpStr.Format("");

	m_strPatternFileName = pStepData->szSimulationFile; //ksj 20180528
}