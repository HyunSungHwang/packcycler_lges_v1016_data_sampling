// Grading.h: interface for the CGrading class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_)
#define AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Afxtempl.h>

typedef struct GradeStep{
	long  lGradeItem;		//20081208 KHS
	float fMin;
	float fMax;
	CString strCode;
} GRADE_STEP;
/*
#ifdef _DEBUG
	typedef struct s_FileTestInformation {
		WORD	lID;
		WORD	lType;			//Added 2005/08/31
		char	szName[64];
		char	szDescription[128];
		char	szCreator[64];
		char	szModifiedTime[64];
	} FILE_TEST_INFORMATION, SCHDULE_INFORMATION_DATA;
#else
*/	typedef struct s_FileTestInformation {
		LONG	lID;
		LONG	lType;			//Added 2005/08/31
		char	szName[64];
		char	szDescription[128];
		char	szCreator[64];
		char	szModifiedTime[64];
	} FILE_TEST_INFORMATION, SCHDULE_INFORMATION_DATA;
//#endif

typedef struct tag_FILE_CELL_CHECK_PARAMETER_V1013_v2_SCH {
	float	fMaxV;
	float	fMinV;
	float	fMaxI;			//최대 전류
	float	fCellDeltaV;	//ljb 20150730 edit 셀전압편차 값 , 최소 전류 -> 2010-09-09 사용 하지 않음 추후 업그레이드 이용시 써도 됨

	//1.15.1 부터 추가
// 	float	fSTDCellDeltaMaxV;	
// 	float	fSTDCellDeltaMinV;	
// 	float	fCellDeltaMaxV;	
// 	float	fCellDeltaMinV;	
// 
// 	unsigned char bfaultcompAuxV_Vent_Flag;
// 	unsigned char breserved1[3];
	//1.15.1 부터 추가 끝
	float	fMaxT;
	float	fMinT;
	float	fMaxC;			//용량초과
	long	lMaxW;			//전력초과
	long	lMaxWh;			//전력량초과

	//ljb 20100819	for v100A

	short int ican_function_division[10];		//분류 번호
	unsigned char cCan_compare_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char cCan_data_type[10];				//can Data Type -> 0:사용 안한 1 : Signed, 2 : Float
	float	fcan_Value[10];


	short int iaux_function_division[10];		//분류 번호
	unsigned char cAux_compare_type[10];				//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char cAux_data_type[10];				//can Data Type -> 0:사용 안한 1 : Signed, 2 : Float
	float	faux_Value[10];

}	FILE_CELL_CHECK_PARAM_V1013_v2_SCH, CELL_CHECK_DATA_V1013_v2_SCH;

typedef struct tag_FILE_CELL_CHECK_PARAMETER {
// 	float	fMaxVoltage;
// 	float	fMinVoltage;
// 	float	fMaxCurrent;
// 	float	fVrefVal;			//Vref
// 	float	fIrefVal;			//Iref
// 	float	fDeltaVoltage;
// 	ULONG	lTrickleTime;
// 	int		nMaxFaultNo;
// 	BOOL	bPreTest;
	float	fMaxV;
	float	fMinV;
	float	fMaxI;			//최대 전류
	float	fCellDeltaV;	//ljb 20150730 edit 셀전압편차 값 , 최소 전류 -> 2010-09-09 사용 하지 않음 추후 업그레이드 이용시 써도 됨
//1.15.1 부터 추가
	float	fSTDCellDeltaMaxV;	
	float	fSTDCellDeltaMinV;	
	float	fCellDeltaMaxV;	
	float	fCellDeltaMinV;	

	unsigned char bfaultcompAuxV_Vent_Flag;
	unsigned char breserved1[3];
//1.15.1 부터 추가 끝
	float	fMaxT;
	float	fMinT;
	float	fMaxC;			//용량초과
	long	lMaxW;			//전력초과
	long	lMaxWh;			//전력량초과

	//ljb 20100819	for v100A

	short int ican_function_division[10];		//분류 번호
	unsigned char cCan_compare_type[10];				//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char cCan_data_type[10];				//can Data Type -> 0:사용 안한 1 : Signed, 2 : Float
	float	fcan_Value[10];
	

	short int iaux_function_division[10];		//분류 번호
	unsigned char cAux_compare_type[10];				//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	unsigned char cAux_data_type[10];				//can Data Type -> 0:사용 안한 1 : Signed, 2 : Float
	float	faux_Value[10];

}	FILE_CELL_CHECK_PARAM, CELL_CHECK_DATA;

//////////////////////////////////////////////////////////////////////////
// + BW KIM 2014.02.20 
typedef struct tag_FILE_CELL_CHECK_PARAMETER_SMALL {

	float	fMaxV;
	float	fMinV;
	float	fMaxI;			//최대 전류
	float	fMinI;			//최소 전류 -> 2010-09-09 사용 하지 않음 추후 업그레이드 이용시 써도 됨
	float	fMaxT;
	float	fMinT;
	float	fMaxC;			//용량초과
	long	lMaxW;			//전력초과
	long	lMaxWh;			//전력량초과	
	
}CELL_CHECK_DATA_SMALL;
// -
//////////////////////////////////////////////////////////////////////////

typedef struct tag_File_GRADE_CON {
	long	lGradeItem;
	BYTE	chTotalGrade;
	float	faValue1[10];
	float	faValue2[10];
	char	aszGradeCode[10];
}	FILE_GRADE;

typedef struct tag_FILE_STEP_CONDITION_V1000 {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref;
	float	fIref;

	ULONG	lEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	UINT	nLoopInfoGoto;
	UINT	nLoopInfoCycle;

	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	LONG	lDeltaTime;
	LONG	lDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	ULONG	ulCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	ULONG	ulCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	ULONG	ulReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	ULONG	ulDCRStartTime;			//EDLC DCR 검사시 Start Time
	ULONG	ulDCREndTime;			//EDLC DCR 검사시 End Time
	ULONG	ulLCStartTime;			//EDLC LC 측정 시작 시간
	ULONG	ulLCEndTime;			//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택

}	FILE_STEP_PARAM_10000;	

typedef struct tag_FILE_STEP_CONDITION_V100A {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;
	long	lReserved[17];			//ljb 18->17
	//////////////////////////////////////////////////////////////////////////
}	FILE_STEP_PARAM_V100A;

typedef struct tag_FILE_STEP_CONDITION_V100B {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb
	//////////////////////////////////////////////////////////////////////////
}	FILE_STEP_PARAM_V100B;


typedef struct tag_FILE_STEP_CONDITION_V100C {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
}	FILE_STEP_PARAM_V100C;

typedef struct tag_FILE_STEP_CONDITION_V100D {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

}	FILE_STEP_PARAM_V100D;

typedef struct tag_FILE_STEP_CONDITION_V100D_LOAD {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];				//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];
	UINT	iBranchAuxContiTime[10];			//ljb 20170824 for v1011

	long	lReserved[10];						//ljb iBranchAuxContiTime[10] 추가로 20 ->10 으로 변경

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////

	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	//cny CellBal
	int     nCellBal_CircuitRes;//RES
	int		nCellBal_WorkVoltUpper;//mV
	int     nCellBal_WorkVolt;//mV
	int     nCellBal_StartVolt;//mV
	int     nCellBal_EndVolt;//mV
	int     nCellBal_ActiveTime;//SEC
	int     nCellBal_AutoStopTime;//SEC
	
	//cny PowerSupply
	int     nPwrSupply_Cmd;//1-ON,2-OFF,0-NONE
	int     nPwrSupply_Volt;//mV
	
	//cny Chiller
	int     nChiller_Cmd;//1-USE              
	float   fChiller_RefTemp;                 
	int     nChiller_RefCmd;//1-Run,2-Stop  
	int     nChiller_PumpCmd;//1-Run,2-Stop 
	float   fChiller_RefPump;                 
	int     nChiller_TpData;//1-Aux,2-Can     
	float   fChiller_ONTemp1;//Max            
	int     nChiller_ONTemp1_Cmd;//1-ON     
	int     nChiller_ONPump1_Cmd;//1-ON     
	float   fChiller_ONTemp2;                 
	int     nChiller_ONTemp2_Cmd;//1-ON       
	int     nChiller_ONPump2_Cmd;//1-ON     
	float   fChiller_OFFTemp1;                
	int     nChiller_OFFTemp1_Cmd;//1-ON      
	int     nChiller_OFFPump1_Cmd;//1-ON      
	float   fChiller_OFFTemp2;                
	int     nChiller_OFFTemp2_Cmd;//1-ON      
	int     nChiller_OFFPump2_Cmd;//1-ON      

}	FILE_STEP_PARAM_V100D_LOAD;

typedef struct tag_FILE_STEP_CONDITION_V100D_LOAD_SCH {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////

// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	char	szSimulationFile[256];	//ksj 20180528
}	FILE_STEP_PARAM_V100D_LOAD_SCH; //20180627 yulee : 스케쥴 호환

typedef struct tag_FILE_STEP_CONDITION_V100F {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

}	FILE_STEP_PARAM_V100F;

typedef struct tag_FILE_STEP_CONDITION_V100F_v2 {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

}	FILE_STEP_PARAM_V100F_v2;

typedef struct tag_FILE_STEP_CONDITION_V100F_LOAD {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////

// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	int		nWaitTimeInit;		
	int		nWaitTimeDay;		//대기 일
	int		nWaitTimeHour;	//대기 시간
	int		nWaitTimeMin;		//대기  분
	int		nWaitTimeSec;		//대기  초
}	FILE_STEP_PARAM_V100F_LOAD;

//ksj 20180528
typedef struct tag_FILE_STEP_CONDITION_V100F_v2_SCH {
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];			//ljb 20101230 for v100B
	float	fBranchCanValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];			//ljb 20101230 for v100B
	float	fBranchAuxValue[10];		//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	char	szSimulationFile[256];	//ksj 20180528
	
}	FILE_STEP_PARAM_V100F_v2_SCH;

//yulee 20180828
typedef struct tag_FILE_STEP_CONDITION_V1011_v1_SCH {//yulee 20180828 스텝별 챔버 동작 정지를 위한 새 스케쥴 구조체
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];		//ljb 20101230 for v100B
	float	fBranchCanValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];		//ljb 20101230 for v100B
	float	fBranchAuxValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];
	UINT    uaux_conti_time[10];			//yulee 20180905 

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	int		m_bStepChamberStop; //yulee 20180828

	char	szSimulationFile[256];	//ksj 20180528
	
}	FILE_STEP_PARAM_V1011_v1_SCH;//FILE_STEP_PARAM_V100F_v2_1_SCH;

//yulee 20180828
typedef struct tag_FILE_STEP_CONDITION_V1011_v2_SCH {//yulee 20180828 스텝별 챔버 동작 정지를 위한 새 스케쥴 구조체
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택
	
	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];
	
	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];		//ljb 20101230 for v100B
	float	fBranchCanValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];		//ljb 20101230 for v100B
	float	fBranchAuxValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];
	UINT    uaux_conti_time[10];			//yulee 20180905 

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	int		m_bStepChamberStop; //yulee 20180828

	char	szSimulationFile[256];	//ksj 20180528
	
}	FILE_STEP_PARAM_V1011_v2_SCH; 

//yulee 20190707
typedef struct tag_FILE_STEP_CONDITION_V1013_v1_SCH {//yulee 20180828 스텝별 챔버 동작 정지를 위한 새 스케쥴 구조체 //yulee 20190707
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fCellDeltaVStep;//yulee 20190531_4
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택

	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];

	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];		//ljb 20101230 for v100B
	float	fBranchCanValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];		//ljb 20101230 for v100B
	float	fBranchAuxValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];
	UINT    uaux_conti_time[10];			//yulee 20180905 

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	int		m_bStepChamberStop; //yulee 20180828

	char	szSimulationFile[256];	//ksj 20180528

}	FILE_STEP_PARAM_V1013_v1_SCH;

//yulee 20190707
typedef struct tag_FILE_STEP_CONDITION_V1013_v2_SCH {//yulee 20180828 스텝별 챔버 동작 정지를 위한 새 스케쥴 구조체 //yulee 20190707
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택

	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];

	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];		//ljb 20101230 for v100B
	float	fBranchCanValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];		//ljb 20101230 for v100B
	float	fBranchAuxValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];
	UINT    uaux_conti_time[10];			//yulee 20180905 

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	int		m_bStepChamberStop; //yulee 20180828

	char	szSimulationFile[256];	//ksj 20180528

}	FILE_STEP_PARAM_V1013_v2_SCH;



//yulee 20190707
typedef struct tag_FILE_STEP_CONDITION_V1015_v1 {//yulee 20180828 스텝별 챔버 동작 정지를 위한 새 스케쥴 구조체 //yulee 20190707
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택

	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];

	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];		//ljb 20101230 for v100B
	float	fBranchCanValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];		//ljb 20101230 for v100B
	float	fBranchAuxValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];
	UINT    uaux_conti_time[10];			//yulee 20180905 

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	int		m_bStepChamberStop; //yulee 20180828

	char	szSimulationFile[256];	//ksj 20180528

	float	m_fCellDeltaVStep;
	float	m_fStepDeltaAuxTemp;
	float	m_fStepDeltaAuxTh;
	float	m_fStepDeltaAuxT;
	float	m_fStepDeltaAuxVTime;
	float	m_fStepAuxV;

	BYTE	m_bStepDeltaVentAuxV;
	BYTE	m_bStepDeltaVentAuxTemp;
	BYTE	m_bStepDeltaVentAuxTH;
	BYTE	m_bStepDeltaVentAuxT;
	BYTE	m_bStepVentAuxV;

}	FILE_STEP_PARAM_V1015_v1;

//yulee 20190531_4
typedef struct tag_FILE_STEP_CONDITION_V1014_v1_SCH 
{
	//yulee 20190531_4 스텝 안전조건 중 셀 전압편차 포함 
	BYTE	chStepNo;
	int		nProcType;
	BYTE	chType;
	BYTE	chMode;
	float	fVref_Charge;		//ljb 20101129
	float	fVref_DisCharge;	//ljb 20101129
	float	fIref;
	float	fPref;
	float	fRref;				//ljb 20101129

	float	fEndTime;
	float	fEndV;
	float	fEndI;
	float	fEndC;
	float	fEndDV;						//Step Charge/Discharge Flag
	float	fEndDI;						//Step 충방전시 2번째 I ref 
	float	fCVTime;

	UINT	nLoopInfoCycle;
	UINT	nLoopInfoGotoStep;

	UINT	nAccLoopInfoCycle;
	UINT	nAccLoopGroupID;

	float	fCapacitanceHigh;
	float	fCapacitanceLow;
	float	fCellDeltaVStep;//yulee 20190531_4
	float	fVLimitHigh;
	float	fVLimitLow;
	float	fILimitHigh;
	float	fILimitLow;
	float	fCLimitHigh;
	float	fCLimitLow;
	float	fImpLimitHigh;
	float	fImpLimitLow;
	float	fDeltaTime;
	float	fDeltaTime1;			//Step 충방전시 2번째 End Cap으로 사용 
	float	fDeltaV;				//Step 충방전시 2번째 End V
	float	fDeltaI;				//Step 충방전시 2번째 End I
	BOOL	bGrade;
	FILE_GRADE	sGrading_Val;

	float	fCompVLow[3];
	float	fCompVHigh[3];
	float	fCompTimeV[3];
	float	fCompILow[3];
	float	fCompIHigh[3];
	float	fCompTimeI[3];
	float	fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	float	fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교

	float	fReportV;
	float	fReportI;
	float	fReportTime;

	float	fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
	float	fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
	float	fDCRStartTime;			//EDLC DCR 검사시 Start Time
	float	fDCREndTime;			//EDLC DCR 검사시 End Time
	float	fLCStartTime;			//EDLC LC 측정 시작 시간
	float	fLCEndTime;				//EDLC LC 측정 종료 시간

	LONG	lRange;					//Range 선택

	//////////////////////////////////////////////////////////////////////////
	//Added 2005/11/08
	float	fReportTemp;			//온도 보고 선택
	float	fValueRate;				//비교 Value Rate  ( SOC, WattHour )

	//Added 2006/5/1
	float	fEndW;
	float	fEndWh;

	//Added 2006/6/8
	float	fHighLimitTemp;
	float	fLowLimitTemp;

	//Added 2008/07/23
	float	fTref;
	float	fHref;
	float	fTrate;
	float	fHrate;
	float	fStartT;
	float	fEndT;
	float	fReserved[20];

	//Added 2005/11/08	-> 20090326 ljb
	BYTE	bValueItem;			//현재 Step의 용량값을 기준 용량으로 사용 할지 여부	 -> 20090326  ljb 사용 안함
	BYTE	bValueStepNo;		//비교할 기준 용량이 속한 Step번호
	WORD	wReserved;			

	//Added 2009/03/26	ljb
	float	fEndV_L;

	UINT	nAccLoopInfoGotoStep;
	UINT	nMultiLoopGroupID;
	UINT	nMultiLoopInfoCycle;
	UINT	nMultiLoopInfoGotoStep;

	UINT	nGotoStepEndV_H;
	UINT	nGotoStepEndV_L;
	UINT	nGotoStepEndTime;
	UINT	nGotoStepEndCVTime;
	UINT	nGotoStepEndC;
	UINT	nGotoStepEndWh;
	UINT	nGotoStepEndValue;

	BYTE	bUseCyclePause;			//Cycle 완료 후 Pause	//★★ ljb 201059  ★★★★★★★★
	BYTE	bUseLinkStep;			//ljb2 CP-CC Step 연동	//ljb 2010-06-25
	BYTE	bUseChamberProg;		//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	cReserved;

	short int	iBranchCanDivision[10];		//ljb 20101230 for v100B
	float	fBranchCanValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchCanDataType[10];
	BYTE	cBranchCanCompareType[10];
	WORD	wBranchCanStep[10];

	short int	iBranchAuxDivision[10];		//ljb 20101230 for v100B
	float	fBranchAuxValue[10];			//ljb 20101230 for v100B
	BYTE	cBranchAuxDataType[10];
	BYTE	cBranchAuxCompareType[10];
	WORD	wBranchAuxStep[10];
	UINT    uaux_conti_time[10];			//yulee 20180905 

	long	lReserved[20];				//ljb

	ULONG	lEndTimeDay;		//ljb 20131212 add
	ULONG	lCVTimeDay;			//ljb 20131212 add

	//////////////////////////////////////////////////////////////////////////
	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	// 종료 옵션 추가 20170517 add
	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 	
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	int		m_bStepChamberStop; //yulee 20180828

	char	szSimulationFile[256];	//ksj 20180528

}	FILE_STEP_PARAM_V1014_v1_SCH;

//ksj 20180528 : 패턴 파일 정보.
typedef struct tag_File_SAVE_SIMLATION_INFO {
	int iStepNo;
	int iFileSize;
	char szFileName[256];
}FILE_SIMULATION_INFO;

//cny 201809 Cell Balance
typedef struct tag_File_SAVE_CELLBAL_INFO {
	int  nStepIndex;
	int  nCellBal_CircuitRes;//RES
	int  nCellBal_WorkVoltUpper;//mV
	int  nCellBal_WorkVolt;//mV
	int  nCellBal_StartVolt;//mV
	int  nCellBal_EndVolt;//mV
	int  nCellBal_ActiveTime;//SEC
	int  nCellBal_AutoStopTime;//SEC
	int  nCellBal_AutoNextStep;
}FILE_CELLBAL_INFO;

//cny 201809 Powr Supply
typedef struct tag_File_SAVE_PWRSUPPLY_INFO {
	int nStepIndex;
	int nPwrSupply_Cmd;
	int nPwrSupply_Volt;
}FILE_PWRSUPPLY_INFO;

//cny 201809 Chiller
typedef struct tag_File_SAVE_CHILLER_INFO 
{
	int     nStepIndex;
	int     nChiller_Cmd;//1-USE               
	float   fChiller_RefTemp;                  
    int     nChiller_RefCmd;//1-Run,2-Stop 
	float   fChiller_RefPump;                  
    int     nChiller_PumpCmd;//1-Run,2-Stop  	
	int     nChiller_TpData;//1-Aux,2-Can      
	float   fChiller_ONTemp1;            
    int     nChiller_ONTemp1_Cmd;//1-ON      
	int     nChiller_ONPump1_Cmd;//1-ON      
	float   fChiller_ONTemp2;                  
	int     nChiller_ONTemp2_Cmd;//1-ON        
	int     nChiller_ONPump2_Cmd;//1-ON        
	float   fChiller_OFFTemp1;                 
	int     nChiller_OFFTemp1_Cmd;//1-ON       
	int     nChiller_OFFPump1_Cmd;//1-ON       
	float   fChiller_OFFTemp2;                 
	int     nChiller_OFFTemp2_Cmd;//1-ON  
	int     nChiller_OFFPump2_Cmd;//1-ON  	
	
}FILE_CHILLER_INFO;




class AFX_EXT_CLASS CGrading  
{
public:
//	WORD GetGradeItem()	{	return (WORD)m_lGradingItem;	}
//	long m_lGradingItem;
	BOOL SetGradingData(CGrading &grading);
	GRADE_STEP GetStepData(int nIndex);
	int GetGradeStepSize();
	BOOL ClearStep();
	BOOL AddGradeStep(long litem, float fMin, float fMax, CString Code);
	CString GetGradeCode(float fData);
	CGrading();
	virtual ~CGrading();
//	void operator = (const CGrading grading); 

private:
	CArray<GRADE_STEP ,GRADE_STEP> m_ptArray;

};

#endif // !defined(AFX_GRADING_H__90C5326F_1905_4D08_BC4D_F173AE68D5FC__INCLUDED_)
