// Step.h: interface for the CStep class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
#define AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Grading.h"

#define SCHEDULE_FILE_ID					740721		//스케쥴 파일 ID
#define SCHEDULE_FILE_VER_v100A				0x00010001	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No
#define SCHEDULE_FILE_VER_v100B		 		0x00020001	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No
#define SCHEDULE_FILE_VER_v100C		 		0x00030001	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No
#define SCHEDULE_FILE_VER_v100D				0x00040001	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No
#define SCHEDULE_FILE_VER_v100D_LOAD		0x00040002	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No
#define SCHEDULE_FILE_VER_v100D_LOAD_SCH	0x00040003	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No	//20180627 yulee : 스케쥴 호환
#define SCHEDULE_FILE_VER_v100F				0x00040004	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No
#define SCHEDULE_FILE_VER_v100F_v2			0x00040005	//상위 2Byte는 Majer Version No, 하위 2Byte는 Minu Version No //20180629 yulee 1011 사용
#define SCHEDULE_FILE_VER_v100F_v2_SCH		0x00040006	//ksj 20180528 	//20180629 yulee 1011 패턴 포함에 사용 
#define SCHEDULE_FILE_VER_v1011_v1_SCH		0x00040007	//ksj 20180528 	//yulee 20180828 스텝별 챔버 정지 기능
#define SCHEDULE_FILE_VER_v1011_v2_SCH		0x00040008	//ksj 20180528 	//yulee 20180828 스텝별 챔버 정지 기능 //LGC 오창 2공장 20190322 버전 
#define SCHEDULE_FILE_VER_v1013_v1_SCH		0x00050001	//1013 protocol PowerSupply //LGC 오창 2공장 20190520 버전
#define SCHEDULE_FILE_VER_v1013_v2_SCH		0x00050002	//1013 VS2010 CWA
#define SCHEDULE_FILE_VER_v1014_v1_SCH		0x00060001	//1014 Step별 셀 전압편차 안전 조건
#define SCHEDULE_FILE_VER_v1014_v2_SCH		0x00060002	
#define SCHEDULE_FILE_VER_v1015_v1			0x00070001	

#define SCHEDULE_FILE_VER	SCHEDULE_FILE_VER_v1015_v1 //ksj 20200825 : 최종버전을 SCHEDULE_FILE_VER로 재정의. 버전업하는 경우 이것도 변경.

#define MAX_STEP_COMP_SIZE	3
#define MAX_STEP_CAN_AUX_COMPARE_SIZE	10

class AFX_EXT_CLASS CStep  
{
public:

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get Step

	FILE_STEP_PARAM_V100D_LOAD_SCH	GetFileStep_LOAD_SCH(); //ksj 20180528

	FILE_STEP_PARAM_V100D			GetFileStep_vD();
	FILE_STEP_PARAM_V100D			GetFileStep_vDLto1011_V1_SCH();
	FILE_STEP_PARAM_V100D_LOAD		GetFileStep_vD_load();
	FILE_STEP_PARAM_V100D_LOAD_SCH	GetFileStep_vDL_SCH();

	FILE_STEP_PARAM_V100F			GetFileStep_vF();
	FILE_STEP_PARAM_V100F_v2		GetFileStep_vF2();
	FILE_STEP_PARAM_V100F_v2_SCH	GetFileStep_vF2_SCH(); //ksj 20180528

	FILE_STEP_PARAM_V1011_v1_SCH	GetFileStep_v1011_v1_SCH(); //yulee 20180828 
	FILE_STEP_PARAM_V1011_v2_SCH	GetFileStep_v1011_v2_SCH(); //yulee 20181002

	FILE_STEP_PARAM_V1013_v1_SCH	GetFileStep_1013_V1_SCHto1013_V2_SCH();
	FILE_STEP_PARAM_V1013_v1_SCH	GetFileStep_v1013_v1_SCH(); //yulee 20181002
	FILE_STEP_PARAM_V1013_v2_SCH	GetFileStep_v1013_v2_SCH(); //yulee 20181002

	FILE_STEP_PARAM_V1014_v1_SCH	GetFileStep_v1014_v1_SCH(); //yulee 20181002

	FILE_STEP_PARAM_V1015_v1		GetfileStep_v1015_v1(); 
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// Set Step
	void	SetStepData(FILE_STEP_PARAM_V100D_LOAD_SCH *pStepData); //ksj 20180528

	void	SetStepData_vB(FILE_STEP_PARAM_V100B *pStepData);

	void	SetStepData_vD(FILE_STEP_PARAM_V100D *pStepData); //ksj 20180528
	void	SetStepData_vDL(FILE_STEP_PARAM_V100D_LOAD *pStepData);
	void	SetStepData_vDL_SCH(FILE_STEP_PARAM_V100D_LOAD_SCH *pStepData); //ksj 20180528	
	void	SetStepData_vDto1011_V1_SCH(FILE_STEP_PARAM_V100D *pStepData); //20180628 yulee
	void	SetStepData_vDLto1011_V1_SCH(FILE_STEP_PARAM_V100D_LOAD *pStepData); //20180628 yulee
	void	SetStepData_vDL_SCHto1011_V1_SCH(FILE_STEP_PARAM_V100D_LOAD_SCH *pStepData); //20180628 yulee

	void	SetStepData_vF(FILE_STEP_PARAM_V100F *pStepData);
	void	SetStepData_vF2(FILE_STEP_PARAM_V100F_v2 *pStepData);
	void	SetStepData_vFto1011_V1_SCH(FILE_STEP_PARAM_V100F *pStepData); //20180628 yulee
	void	SetStepData_vF2_SCH(FILE_STEP_PARAM_V100F_v2_SCH *pStepData); //ksj 20180528
	void	SetStepData_vF2to1011_V1_SCH(FILE_STEP_PARAM_V100F_v2 *pStepData); //20180628 yulee
	void	SetStepData_vF_V2_SCHto1013_V1_SCH(FILE_STEP_PARAM_V100F_v2_SCH *pStepData); //yulee 20190116
	void	SetStepData_vFto1013_V2_SCH(FILE_STEP_PARAM_V100F *pStepData); //20180628 yulee
	void	SetStepData_vF2to1013_V2_SCH(FILE_STEP_PARAM_V100F_v2 *pStepData); //20180628 yulee
	void	SetStepData_vF_V2_SCHto1013_V2_SCH(FILE_STEP_PARAM_V100F_v2_SCH *pStepData); //yulee 20190116

	void	SetStepData_v1011_v1_SCH(FILE_STEP_PARAM_V1011_v1_SCH *pStepData); //yulee 20180828 
	void	SetStepData_v1011_v2_SCH(FILE_STEP_PARAM_V1011_v2_SCH *pStepData); //yulee 20181002
	void	SetStepData_1011_V1_SCHto1013_V1_SCH(FILE_STEP_PARAM_V1011_v1_SCH *pStepData);
	void	SetStepData_1011_V2_SCHto1013_V1_SCH(FILE_STEP_PARAM_V1011_v2_SCH *pStepData);//yulee 20190531_4
	void	SetStepData_1011_V1_SCHto1013_V2_SCH(FILE_STEP_PARAM_V1011_v1_SCH *pStepData);
	void	SetStepData_1011_V2_SCHto1013_V2_SCH(FILE_STEP_PARAM_V1011_v2_SCH *pStepData); //LGC 오창 2공장 20190322 버전 

	void	SetStepData_v1013_v1_SCH(FILE_STEP_PARAM_V1013_v1_SCH *pStepData); //yulee 20190707
	void	SetStepData_v1013_v2_SCH(FILE_STEP_PARAM_V1013_v2_SCH *pStepData); //yulee 20190707
	void	SetStepData_1013_V1_SCHto1013_V2_SCH(FILE_STEP_PARAM_V1013_v1_SCH *pStepData); //1013 protocol PowerSupply //LGC 오창 2공장 20190520 버전
	void	SetStepData_1014_V1_SCHto1013_V2_SCH(FILE_STEP_PARAM_V1014_v1_SCH *pStepData); //1014 Step별 셀 전압편차 안전 조건

	void    SetStepData_v1014_v1_SCH(FILE_STEP_PARAM_V1014_v1_SCH *pStepData);
	void	SetStepData_v1014_v2_SCH(FILE_STEP_PARAM_V1014_v1_SCH *pStepData); //yulee 20190707

	void	SetStepData_v1015_v1(FILE_STEP_PARAM_V1015_v1 *pStepData); 
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// Save Step
	BOOL SaveStep_vF_V2_SCHto1013_V1_SCH(CString strFileName, void *pSch, void* pSchAddInfo);

	BOOL SaveStep_v1011_v2_SCH(CString strFileName, void *pSch, void* pSchAddInfo);

	BOOL SaveStep_v1014_v1_SCH(CString strFileName, void* pSch, void* pSchAddInfo);

	BOOL SaveStep_v1013_v1_SCHto1013_V2_SCH(CString strFileName, void *pSch, void* pSchAddInfo);

	BOOL SaveStep_v1015_v1(CString strFileName, void *pSch, void* pSchAddInfo);
	BOOL SaveStep_v1013(CString strFileName, void *pSch, void* pSchAddInfo); //ksj 20210114
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	BOOL ClearStepData();
	CStep(int nStepType /*=0*/);
	CStep();
	virtual ~CStep();
	void operator = (CStep &step); 

	//DB PK
	long	m_StepID;

	/*Step Header */
	BYTE	m_StepIndex;	//StepNo
	long	m_lProcType;				
	BYTE	m_type;			//Step Type
	BYTE	m_mode;
	ULONG	m_lRange;			
	float	m_fVref_Charge;		//20101129
	float	m_fVref_DisCharge;	//20101129
    float	m_fIref;
    float	m_fPref;		//ljb 20100820	파워
	float	m_fRref;		//ljb 20101129	저항
	
	float	m_fEndT;		//종료온도
	float	m_fStartT;		//시작온도
	float	m_fTref;		//Referance Temperature
	float	m_fHref;		//Referance humidity
	float	m_fTrate;		//온도변화율
	float	m_fHrate;		//습도변화율
    
	/* Step End Value */
//	ULONG	m_ulEndTime;
	float	m_fEndTime;
    float	m_fEndV_H;		//ljb 2009-03-26  m_fEndV -> m_fEndV_H
	float	m_fEndV_L;		//ljb 2009-03-26 추가
    float	m_fEndI;
    float	m_fEndC;
    float	m_fEndDV;
    float	m_fEndDI;
	float	m_fEndW;
	float	m_fEndWh;
	float	m_fCVTime;

	/* Soc End Condition */
	BYTE	m_bValueItem;				//사용할 Item ( 0 : 사용 안함 , 1 : SOC , 2 : WattHour)
	BYTE	m_bValueStepNo;				//비교할 기준 용량이 속한 Step번호
	float	m_fValueRate;				//비교 SOC Rate
	/* Cycle End Condition */
	BYTE	m_bUseCyclePause;	//ljb Cycle 반복 후 Pause 상태 유지 0:사용안함, 1:Pause 사용
	BYTE	m_bUseLinkStep;		//ljb CP-CC 연동 STEP
	BYTE	m_bUseChamberProg;	//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정
	BYTE	m_bStepChamberStop;  //yulee 20180828 챔버 stop 기능

	/*Step Fail Value */
	float	m_fHighLimitV;
    float	m_fLowLimitV;
    float	m_fHighLimitI;
    float	m_fLowLimitI;
    float	m_fHighLimitC;
    float	m_fLowLimitC;
	float 	m_fHighLimitImp;
    float	m_fLowLimitImp;
	float	m_fHighLimitTemp;
	float	m_fLowLimitTemp;
	float	m_fHighCapacitance;
	float	m_fLowCapacitance;
	
	//ljb 2009-03 add -> BMS goto value
	float m_fEndBmsVtgHigh;
	float m_fEndBmsVtgLow;
	float m_fEndBmsTempHigh;
	float m_fEndBmsTempLow;
	float m_fEndBmsSocHigh;
	float m_fEndBmsSocLow;
	float m_fEndBmsFltHigh;
	float m_fEndAuxVtgHigh;
	float m_fEndAuxVtgLow;
	float m_fEndAuxTempHigh;
	float m_fEndAuxTempLow;

	//ljb 2009-03 add -> BMS goto info	
	unsigned short int m_nLoopGotoStepBmsVtgH;
	unsigned short int m_nLoopGotoStepBmsVtgL;
	unsigned short int m_nLoopGotoStepBmsTempH;
	unsigned short int m_nLoopGotoStepBmsTempL;
	unsigned short int m_nLoopGotoStepBmsSocH;
	unsigned short int m_nLoopGotoStepBmsSocL;
	unsigned short int m_nLoopGotoStepBmsFltH;
	unsigned short int m_nLoopGotoStepAuxVtgH;
	unsigned short int m_nLoopGotoStepAuxVtgL;
	unsigned short int m_nLoopGotoStepAuxTempH;
	unsigned short int m_nLoopGotoStepAuxTempL;

	/*Grading*/
	BYTE	m_bGrade;
	CGrading	m_Grading;

	//Cycle End Goto 
	unsigned short int	m_nLoopInfoCycle;	
	unsigned short int	m_nLoopInfoGotoStep;

	unsigned short int	m_nMultiLoopGroupID;
	unsigned short int	m_nMultiLoopInfoCycle;
	unsigned short int	m_nMultiLoopInfoGotoStep;

	unsigned short int	m_nAccLoopGroupID;
	unsigned short int	m_nAccLoopInfoCycle;
	unsigned short int	m_nAccLoopInfoGotoStep;

	//ljb 2009-03-26 Goto 변수 추가///////////////////////////////////////////
//	long	m_nGotoStepID;		//EndV, EndT, EndC용 Step ID -> 이동스텝은 Step ID를 참조한다.
	unsigned short int m_nGotoStepEndV_H;
	unsigned short int m_nGotoStepEndV_L;
	unsigned short int m_nGotoStepEndTime;
	unsigned short int m_nGotoStepEndCVTime;
	unsigned short int m_nGotoStepEndC;
	unsigned short int m_nGotoStepEndWh;
	unsigned short int m_nGotoStepEndValue;
	//////////////////////////////////////////////////////////////////////////

	//ljb 20100819 변수 추가 for v100A ///////////////////////////////////////
	short int	m_ican_function_division[MAX_STEP_CAN_AUX_COMPARE_SIZE];	//분류 번호
	short int	m_ican_compare_type[MAX_STEP_CAN_AUX_COMPARE_SIZE];			//can 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	short int	m_ican_data_type[MAX_STEP_CAN_AUX_COMPARE_SIZE];			//Can Data Type 0: None , 1: Signed, 2: Float
	short int	m_ican_branch[MAX_STEP_CAN_AUX_COMPARE_SIZE];				//Can 분기	Next : 251, Pause : 252 
	float		m_fcan_Value[MAX_STEP_CAN_AUX_COMPARE_SIZE];
	
	short int	m_iaux_function_division[MAX_STEP_CAN_AUX_COMPARE_SIZE];	//분류 번호
	short int	m_iaux_compare_type[MAX_STEP_CAN_AUX_COMPARE_SIZE];			//aux 값이 비교대상 -> 비교 type 0:사용 안한 1 : < ,2: <=, 3 : == , 4: >, 5: =>
	short int	m_iaux_data_type[MAX_STEP_CAN_AUX_COMPARE_SIZE];			//Aux Data Type 0: None , 1: Signed, 2: Float
	short int	m_iaux_branch[MAX_STEP_CAN_AUX_COMPARE_SIZE];				//Aux 분기	Next : 251, Pause : 252
	float		m_faux_Value[MAX_STEP_CAN_AUX_COMPARE_SIZE];
	UINT		m_iaux_conti_time[MAX_STEP_CAN_AUX_COMPARE_SIZE];			//ljb 20170824 add 
	//////////////////////////////////////////////////////////////////////////

	//V/I Ramp check
//	ULONG	m_lCompTimeV[MAX_STEP_COMP_SIZE];			
	float	m_fCompTimeV[MAX_STEP_COMP_SIZE];			
	float	m_fCompHighV[MAX_STEP_COMP_SIZE];
 	float	m_fCompLowV[MAX_STEP_COMP_SIZE];
//	ULONG	m_lCompTimeI[MAX_STEP_COMP_SIZE];			
	float	m_fCompTimeI[MAX_STEP_COMP_SIZE];			
	float	m_fCompHighI[MAX_STEP_COMP_SIZE];
	float	m_fCompLowI[MAX_STEP_COMP_SIZE];

	//Delta Check
	float	m_fDeltaTimeV;
    float	m_fDeltaV;
    float	m_fDeltaTimeI;
    float	m_fDeltaI;

	//Recording 
	float	m_fReportTemp;
	float	m_fReportV;
	float	m_fReportI;
	float	m_fReportTime;


	//EDLC
	float	m_fCapaVoltage1;		
	float	m_fCapaVoltage2;	
	float	m_fDCRStartTime;		
	float	m_fDCREndTime;
	float	m_fLCStartTime;		
	float	m_fLCEndTime;		

	//User Pattern
	CString	m_strPatternFileName;
	long	m_lValueLimitLow;
	long	m_lValueLimitHigh;

	long	m_lEndTimeDay;	//ljb 20131212 add
	long	m_lCVTimeDay;	//ljb 20131212 add

	float	m_fValueMax;		//20140207 add Pattern data 중 최대값
	float	m_fValueMin;		//20140207 add Pattern data 중 최소값 
		
	int		m_nNoCheckMode;		//2014.12.08 해당 STEP CAN RX 안전, 종료 조건 무시.(RX 설정에 있는 안전상하한, 종료상하한이 무시됨)
	int     m_nCanTxOffMode;	//2014.12.09 해당 STEP CAN TX OFF 됨.
	int     m_nCanCheckMode;	//ljb 20150825 add, 해당 STEP CAN 통신 체크 해제. (1:CAN 통신 체크를 하지 않음) 

	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	float	fValueLoaderSet;		//설정값 W,A,V,Ohm

	// WAIT TIME GOTO  조건 추가  20160414 add 이재복
	int		m_nWaitTimeInit;		
	int		m_nWaitTimeDay;		//대기 일
	int		m_nWaitTimeHour;	//대기 시간
	int		m_nWaitTimeMin;		//대기  분
	int		m_nWaitTimeSec;		//대기  초

	CString		m_strStepMemo;

	//cny CellBal
	int   m_nCellBal_CircuitRes;//RES
	int     m_nCellBal_WorkVoltUpper;//mV
	int     m_nCellBal_WorkVolt;//mV
	int     m_nCellBal_StartVolt;//mV
	int     m_nCellBal_EndVolt;//mV
	int     m_nCellBal_ActiveTime;
	int     m_nCellBal_AutoStopTime;
	int     m_nCellBal_AutoNextStep;
	int     m_nCellBal_State;//Use_State
	int     m_nCellBal_Log;
	int     m_nCellBal_Count;
	int     m_nCellBal_SeqNo;
	
	//cny PowerSupply
	int     m_nPwrSupply_Cmd;//1-ON,2-OFF,0-NONE
	int     m_nPwrSupply_Volt;//mV
	int     m_nPwrSupply_State;//Use_State
	int     m_nPwrSupply_Log;
	int     m_nPwrSupply_SeqNo;
	
	//cny Chiller
	int     m_nChiller_Cmd;//1-USE               
	float   m_fChiller_RefTemp;
	int     m_nChiller_RefCmd;//1-ON,2-OFF 0-NOTUSE
	float   m_fChiller_RefPump;
	int     m_nChiller_PumpCmd;//1-ON,2-OFF
	int     m_nChiller_TpData;//1-Aux,2-Can      
	float   m_fChiller_ONTemp1;//Max             
	int     m_nChiller_ONTemp1_Cmd;//1-ON  
	int     m_nChiller_ONPump1_Cmd;//1-ON  
	float   m_fChiller_ONTemp2;                  
	int     m_nChiller_ONTemp2_Cmd;//1-ON  
	int     m_nChiller_ONPump2_Cmd;//1-ON
	float   m_fChiller_OFFTemp1;                 
	int     m_nChiller_OFFTemp1_Cmd;//1-ON  
	int     m_nChiller_OFFPump1_Cmd;//1-ON
	float   m_fChiller_OFFTemp2;                 
	int     m_nChiller_OFFTemp2_Cmd;//1-ON  
	int     m_nChiller_OFFPump2_Cmd;//1-ON
	int     m_nChiller_State;//Use_State
	int     m_nChiller_Log;
	int     m_nChiller_Count;
	int     m_nChiller_SeqNo;

	float   m_fCellDeltaVStep;//yulee 20190531_3
	float	m_fStepDeltaAuxTemp;
	float	m_fStepDeltaAuxTH;
	float	m_fStepDeltaAuxT;
	float	m_fStepDeltaAuxVTime;
	float	m_fStepAuxV;

	BOOL	m_bStepDeltaVentAuxV;
	BOOL	m_bStepDeltaVentAuxTemp;
	BOOL	m_bStepDeltaVentAuxTh;
	BOOL	m_bStepDeltaVentAuxT;
	BOOL	m_bStepVentAuxV;

	BOOL Fun_SavePatternFileToSch(FILE *pFile, int nStepNo,CString strFileName);	//ksj 20180528 : 패턴파일 sch 파일에 같이 저장 (울산 SDI 고속형 Cycler 발췌)
	BOOL Fun_SavePwrSupplyFileToSch(FILE *pFile, CStep *pStep);
	BOOL Fun_SaveChillerFileToSch(FILE *pFile, CStep *pStep);
	BOOL Fun_SaveCellBALFileToSch(FILE *pFile, CStep *pStep);

};

#endif // !defined(AFX_STEP_H__B1C39ECF_2E5D_4089_85F8_D596E36E94A0__INCLUDED_)
