// Table.cpp: implementation of the CTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ChData.h"
#include "Table.h"
#include <math.h>
#include <float.h>
#include <io.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
CTable::CTable(LONG lIndex) : m_lIndex(lIndex)
{
	// 각 Transducer의 마지막 데이터를 얻지 못하였고
	m_bLoadLastData  = FALSE;
	m_pfltLastData   = NULL;
//	m_fltDCIR        = 0.0f;
//	m_fltOCV         = 0.0f;

	// 전체 데이터도 loading되지 않았음을 표시
	m_bLoadData      = FALSE;
	m_lNumOfData     = 0L;
	m_lLoadedDataCount = 0L;
	m_ppfltData      = NULL;
	m_lStepNo        = 0L;
	m_lLastRecordIndex = 0L;
	m_lRecordIndex	= 0L;
	m_lTotCycle = 0L;
	m_wType = PS_STEP_NONE;
}
*/
CTable::CTable(LPCTSTR strTitle, LPCTSTR strFromList, CChData* pParent) : m_pParent(pParent)
{
	CString strName(strTitle);		//TableList 파일의 Column Header 목록  
	CString strValue(strFromList);	//현재 Table Index의 값

	// (1) m_strlistTitle
	// -> strName("Table List의 둘째 줄")에서 저장된 각 Transducer의 Name을 List에 넣는다.
	int s=0, p1=0;
	CString strItem;
	while(p1!=-1)
	{
		p1 = strName.Find(',', s);
		if(p1!=-1)
		{
			strItem = strName.Mid(s, p1-s);
			strItem.TrimLeft();
			strItem.TrimRight();
			if (strItem.IsEmpty()) break;
			s  = p1+1;
			//No(StepNo), DCIR, OCV는 실처리 정보가 아니다.(Step에서 오직 1개의 값만을 갖는다.)
//			if( Title.CompareNoCase("No") != 0 && Title.CompareNoCase("IR")!= 0 && Title.CompareNoCase("OCV") != 0)
			m_strlistTitle.AddTail(strItem);

			//////////////////////////////////////////////////////////////////////////
			m_awlistItem.Add(GetItemID(strItem));
			//////////////////////////////////////////////////////////////////////////
		}
	}

	// strValue에서 Table의 Index와 각 Transducer의 마지막 값을 얻는다.
	s=0, p1=0;

//	// (2) m_lIndex: Table No
//	p1 = strValue.Find(',', s);
//	m_lIndex = atoi(strValue.Mid(s, p1-s));
//	s  = p1+1;

	// (3) m_pfltLastData: 각 Transducer들의 마지막 값
//	ASSERT(m_pfltLastData == NULL) ;
	int a = m_strlistTitle.GetCount();
	m_pfltLastData = new float[a];

	//lmh 20120607 가상메모리화
//	m_pfltLastData = (float*)::VirtualAlloc(NULL,sizeof(float)*a,MEM_COMMIT,PAGE_EXECUTE_READWRITE);
	for(int i=0;i<a;i++)
	{
		p1 = strValue.Find(',', s);
		if(p1!=-1) 
			m_pfltLastData[i] = (float) atof(strValue.Mid(s, p1-s));
		else
			m_pfltLastData[i] = 0;
		s  = p1+1;
	}
	m_bLoadLastData = TRUE;

	//Step No
	int nIndex1;
//	nIndex1 = FindIndexOfTitle(PS_CHANNEL_NO);

	nIndex1 = FindIndexOfTitle((WORD)PS_STATE);
	if(nIndex1 < 0 )		m_chState = 0;
	else					m_chState = (BYTE)m_pfltLastData[nIndex1];
	
	m_chDataSelect = 0x01;	//SFT_SAVE_STEP_END;

	nIndex1 = FindIndexOfTitle(PS_CUR_CYCLE);
	if(nIndex1 < 0 )		m_nCurrentCycleNum = 0;
	else					m_nCurrentCycleNum = (ULONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_DATA_SEQ);
	if(nIndex1 < 0 )		m_lSaveSequence = 0;
	else					m_lSaveSequence = (ULONG)m_pfltLastData[nIndex1];


	nIndex1 = FindIndexOfTitle(PS_STEP_NO);
	if(nIndex1 < 0 )		m_lStepNo = 0;
	else					m_lStepNo = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_TOT_CYCLE);
	if(nIndex1 < 0 )		m_lTotCycle = 0;
	else					m_lTotCycle = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_STEP_TYPE);
	if(nIndex1 < 0 )		m_wType = 0;
	else					m_wType = (WORD)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle("IndexFrom");
	if(nIndex1 < 0 )		m_lRecordIndex = 0;
	else					m_lRecordIndex = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle("IndexTo");
	if(nIndex1 < 0 )		m_lLastRecordIndex = 0;
	else					m_lLastRecordIndex = (LONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_SYNC_DATE);				//ljb v1009
	nIndex1 = FindIndexOfTitle(PS_ACC_CYCLE1);				//ljb v1009
	if(nIndex1 < 0)			m_lAccCycleGroupNum1 = 0;
	else					m_lAccCycleGroupNum1	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_ACC_CYCLE2);				//ljb v1009
	if(nIndex1 < 0)			m_lAccCycleGroupNum2 = 0;
	else					m_lAccCycleGroupNum2	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_ACC_CYCLE3);				//ljb v1009
	if(nIndex1 < 0)			m_lAccCycleGroupNum3 = 0;
	else					m_lAccCycleGroupNum3	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_ACC_CYCLE4);				//ljb v1009
	if(nIndex1 < 0)			m_lAccCycleGroupNum4 = 0;
	else					m_lAccCycleGroupNum4	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_ACC_CYCLE5);				//ljb v1009
	if(nIndex1 < 0)			m_lAccCycleGroupNum5 = 0;
	else					m_lAccCycleGroupNum5	= (ULONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_MULTI_CYCLE1);				//ljb v1009
	if(nIndex1 < 0)			m_lMultiCycleGroupNum1 = 0;
	else					m_lMultiCycleGroupNum1	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_MULTI_CYCLE2);				//ljb v1009
	if(nIndex1 < 0)			m_lMultiCycleGroupNum2 = 0;
	else					m_lMultiCycleGroupNum2	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_MULTI_CYCLE3);				//ljb v1009
	if(nIndex1 < 0)			m_lMultiCycleGroupNum3 = 0;
	else					m_lMultiCycleGroupNum3	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_MULTI_CYCLE4);				//ljb v1009
	if(nIndex1 < 0)			m_lMultiCycleGroupNum4 = 0;
	else					m_lMultiCycleGroupNum4	= (ULONG)m_pfltLastData[nIndex1];
	nIndex1 = FindIndexOfTitle(PS_MULTI_CYCLE5);				//ljb v1009
	if(nIndex1 < 0)			m_lMultiCycleGroupNum5 = 0;
	else					m_lMultiCycleGroupNum5	= (ULONG)m_pfltLastData[nIndex1];

	nIndex1 = FindIndexOfTitle(PS_CODE);
	if(nIndex1 < 0 )		m_chCode = 0;
	else					m_chCode = (BYTE)m_pfltLastData[nIndex1];
	
	nIndex1 = FindIndexOfTitle(PS_GRADE_CODE);
	if(nIndex1 < 0 )		m_chGradeCode = 0;
	else					m_chGradeCode = (BYTE)m_pfltLastData[nIndex1];


	m_lNumOfData = m_lLastRecordIndex-m_lRecordIndex+1;
	m_lLoadedDataCount = m_lNumOfData;

	// 각 Transducer의 마지막 데이터는 얻었고

	// 전체 데이터는 아직 loading하지 않았음을 표시한다.
	m_bLoadData     = FALSE;
	m_ppfltData     = NULL;
	m_ppfTempAuxData	= NULL;
	m_ppfTempChData		= NULL;
	m_ppCanData			= NULL;
	m_pfltAuxLastData	= NULL;
	m_pCanLastData		= NULL;
	m_nAuxColumnCount = 0;
	m_nCanColumnCount = 0;
}


// 
//  void CTable::operator delete(void *pDelete, size_t nSize)
// {
//     VirtualUnlock(pDelete, nSize);
//     VirtualFree(pDelete, nSize, MEM_RELEASE);    
// }
// void* CTable::operator new(size_t nSize)
// {
// 	void *pNew = NULL;
// 	pNew = VirtualAlloc(NULL, nSize, MEM_RESERVE, PAGE_EXECUTE_READWRITE);
// 	return pNew;
// }

// void *CTable::operator new(size_t nSize)
// {
//     void *pNew = NULL;
// 	
//     pNew = VirtualAlloc(NULL, nSize, MEM_RESERVE, PAGE_EXECUTE_READWRITE);
// 	
//     if(pNew)    // if reserve suceed
// 		pNew = VirtualAlloc(pNew, nSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
// 	
//     if(pNew)    // if commit suceed
// 		return VirtualLock(pNew, nSize) ? pNew : NULL;
//     
//     return pNew;
// }

CTable::CTable(CWordArray *paItem, LPPS_STEP_END_RECORD_V8 lpRecord, CChData* pParent) : m_pParent(pParent)
{
	ASSERT(lpRecord);
	ASSERT(paItem);
	ASSERT(paItem->GetSize() > 0);
	
	ASSERT(m_pfltLastData == NULL) ;
	m_awlistItem.Copy(*paItem);
	m_pfltLastData = new float[m_awlistItem.GetSize()];
	//lmh 20120607 가상메모리화
	//m_pfltLastData = (float*)::VirtualAlloc(NULL,sizeof(float)*m_awlistItem.GetSize(),MEM_COMMIT,PAGE_EXECUTE_READWRITE);
	
	LPPS_STEP_END_RECORD_DATA_V8 pData = (LPPS_STEP_END_RECORD_DATA_V8)lpRecord;
	
	CString strItemName;
	for(int i = 0; i<m_awlistItem.GetSize(); i++)
	{
		m_pfltLastData[i] = (float)pData->fData[i];
		
		//////////////////////////////////////////////////////////////////////////
		//TRACE("Title = %s \n",GetItemName(m_awlistItem.GetAt(i)));
		m_strlistTitle.AddTail(GetItemName(m_awlistItem.GetAt(i)));
		//////////////////////////////////////////////////////////////////////////
	}
	
	m_bLoadLastData = TRUE;
	
	//////////////////////////////////////////////////////////////////////////
	m_chNo			= lpRecord->chNo;					// Channel Number
	m_chState		= lpRecord->chState;				// Run, Stop(Manual, Error), End
	m_chDataSelect = lpRecord->chDataSelect;			// For Display Data, For Saving Data
	m_chCode		= lpRecord->chCode;
	m_chGradeCode	= lpRecord->chGradeCode;
	
	m_nCurrentCycleNum = lpRecord->nCurrentCycleNum;
	m_lSaveSequence = lpRecord->lSaveSequence;			// Expanded Field
	
	m_lStepNo = lpRecord->chStepNo;
	m_lTotCycle = lpRecord->nTotalCycleNum;
	m_wType = lpRecord->chStepType;
	
	m_lAccCycleNum = lpRecord->nAccCycleNum;	//ljb v1009
		
	m_lRecordIndex = lpRecord->nIndexFrom;
	m_lLastRecordIndex = lpRecord->nIndexTo;
	m_lNumOfData = m_lLastRecordIndex-m_lRecordIndex+1;
	m_lLoadedDataCount = m_lNumOfData;
	//////////////////////////////////////////////////////////////////////////
	
	// 전체 데이터는 아직 loading하지 않았음을 표시한다.
	m_bLoadData     = FALSE;
	m_ppfltData     = NULL;
	m_ppfTempAuxData	= NULL;
	m_ppfTempChData		= NULL;
	m_nAuxColumnCount = 0;
	m_pfltAuxLastData = NULL;
	m_pCanLastData		= NULL;
	m_ppCanData = NULL;
	m_nCanColumnCount = 0;	
}

CTable::CTable(CWordArray *paItem, LPPS_STEP_END_RECORD lpRecord, CChData* pParent) : m_pParent(pParent) 
{
	ASSERT(lpRecord);
	ASSERT(paItem);
	ASSERT(paItem->GetSize() > 0);
	
	//	ASSERT(m_pfltLastData == NULL) ;
	m_awlistItem.Copy(*paItem);
	m_pfltLastData = new float[m_awlistItem.GetSize()];//lmh 20120607 가상메모리화

	//m_pfltLastData = (float*)::VirtualAlloc(NULL,sizeof(float)*m_awlistItem.GetSize(),MEM_RESERVE,PAGE_EXECUTE_READWRITE);
	// 
	//if(m_pfltLastData)
	// 	m_pfltLastData = (float*)::VirtualAlloc(m_pfltLastData, sizeof(float)*m_awlistItem.GetSize(), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	// 
	//if(m_pfltLastData)
	// VirtualLock(m_pfltLastData,sizeof(float)*m_awlistItem.GetSize());

	//LPPS_STEP_END_RECORD_DATA_V100B pData = (LPPS_STEP_END_RECORD_DATA_V100B)lpRecord;
	//////////////////////////////////////////////////////////////////////////
	//ljb 20110128 수정
	LPPS_STEP_END_RECORD_DATA_V100C pData = new PS_STEP_END_RECORD_DATA_V100C;

	//lmh 20120607 가상메모리화
	//LPPS_STEP_END_RECORD_DATA_V100B pData = (LPPS_STEP_END_RECORD_DATA_V100B)::VirtualAlloc(NULL,sizeof(PS_STEP_END_RECORD_DATA_V100B),MEM_RESERVE,PAGE_EXECUTE_READWRITE);
	// 
	//if(pData)
	// pData = (LPPS_STEP_END_RECORD_DATA_V100B)::VirtualAlloc(pData, sizeof(PS_STEP_END_RECORD_DATA_V100B), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	// 
	//if(pData)
	// VirtualLock(pData,sizeof(PS_STEP_END_RECORD_DATA_V100B));

	ZeroMemory(pData,sizeof(PS_STEP_END_RECORD_DATA_V100C));
	
	memcpy(pData,lpRecord,sizeof(PS_STEP_END_RECORD));
	//////////////////////////////////////////////////////////////////////////
		
	CString strItemName;
	for(int i = 0; i<m_awlistItem.GetSize(); i++)
	{
		m_pfltLastData[i] = (float)pData->fData[i];

		//////////////////////////////////////////////////////////////////////////
		TRACE("Title = %s, i=%d \n",GetItemName(m_awlistItem.GetAt(i)),i);
		m_strlistTitle.AddTail(GetItemName(m_awlistItem.GetAt(i)));
		//////////////////////////////////////////////////////////////////////////
	}

	m_bLoadLastData = TRUE;

	//////////////////////////////////////////////////////////////////////////
	m_chNo			= lpRecord->chNo;					// Channel Number
	m_chState		= lpRecord->chState;				// Run, Stop(Manual, Error), End
	m_chDataSelect = lpRecord->chDataSelect;			// For Display Data, For Saving Data
	m_chCode		= lpRecord->chCode;
	m_chGradeCode	= lpRecord->chGradeCode;

	//lpRecord->chCommState;			//ljb 20100909	Bit LSB(0:정상, 1:이상) 1:Aux 온도 통신 상태, 2: Aux 전압 통신 상태 
	//lpRecord->chOutputState;
	//lpRecord->chInputState;

	m_nCurrentCycleNum = lpRecord->nCurrentCycleNum;
	m_lSaveSequence = lpRecord->lSaveSequence;			// Expanded Field

	m_lStepNo = lpRecord->chStepNo;
	m_lTotCycle = lpRecord->nTotalCycleNum;
	m_wType = lpRecord->chStepType;

	m_lSyncDate = lpRecord->lSyncDate;	//ljb 20130610 add v100C
	m_fSyncTime = lpRecord->fSyncTime;
	TRACE("SyncDate = %d, SysncTime = %f \n",m_lSyncDate,m_fSyncTime);

	m_lAccCycleGroupNum1 = lpRecord->nAccCycleGroupNum1;	//ljb v1009
	m_lAccCycleGroupNum2 = lpRecord->nAccCycleGroupNum2;
	m_lAccCycleGroupNum3 = lpRecord->nAccCycleGroupNum3;
	m_lAccCycleGroupNum4 = lpRecord->nAccCycleGroupNum4;
	m_lAccCycleGroupNum5 = lpRecord->nAccCycleGroupNum5;

	m_lMultiCycleGroupNum1 = lpRecord->nMultiCycleGroupNum1;
	m_lMultiCycleGroupNum2 = lpRecord->nMultiCycleGroupNum2;
	m_lMultiCycleGroupNum3 = lpRecord->nMultiCycleGroupNum3;
	m_lMultiCycleGroupNum4 = lpRecord->nMultiCycleGroupNum4;
	m_lMultiCycleGroupNum5 = lpRecord->nMultiCycleGroupNum5;
	
	m_lRecordIndex = lpRecord->nIndexFrom;
	m_lLastRecordIndex = lpRecord->nIndexTo;
	m_lNumOfData = m_lLastRecordIndex-m_lRecordIndex+1;
	m_lLoadedDataCount = m_lNumOfData;
	
	m_dCellBALChData=lpRecord->dCellBALChData;	
	//////////////////////////////////////////////////////////////////////////

	m_lstepDay = lpRecord->fStepTime_Day;	//ljb 20131208 add
	m_ltotalDay = lpRecord->fTotalTime_Day;	//ljb 20131208 add
	
	// 전체 데이터는 아직 loading하지 않았음을 표시한다.
	m_bLoadData     = FALSE;
	m_ppfltData     = NULL;
	m_ppfTempAuxData	= NULL;
	m_ppfTempChData		= NULL;
	m_nAuxColumnCount = 0;
	m_pfltAuxLastData = NULL;
	m_pCanLastData		= NULL;
	m_ppCanData = NULL;
	m_nCanColumnCount = 0;

	delete pData;		//ljb 20110128

	//if(pData)
	//{
	//	VirtualUnlock(pData,sizeof(PS_STEP_END_RECORD_DATA_V100B));
	//	VirtualFree(pData,NULL,MEM_RELEASE);
	//}
}

CTable::~CTable()
{
	ReleaseRawDataMemory();

	if(m_pfltLastData!=NULL)
	{
 //		VirtualUnlock(m_pfltLastData, sizeof(m_pfltLastData));
// 		VirtualFree(m_pfltLastData,NULL,MEM_RELEASE);
 		delete [] m_pfltLastData;
		m_pfltLastData = NULL;
	}

	if(m_ppfTempAuxData != NULL)
	{
		for(int i = 0 ; i < m_nAuxColumnCount; i++)
		{
			delete m_ppfTempAuxData[i];
			m_ppfTempAuxData[i] = NULL;
		}
		delete [] m_ppfTempAuxData;
		m_ppfTempAuxData = NULL;
	}
	if(m_pfltAuxLastData != NULL)
	{
		delete [] m_pfltAuxLastData;
		m_pfltAuxLastData = NULL;
	}

	if(m_ppCanData != NULL)
	{
		for(int i = 0 ; i < m_nCanColumnCount; i++)
		{
			delete  m_ppCanData[i];
			m_ppCanData[i] = NULL;
		}
		delete [] m_ppCanData;
		m_ppCanData = NULL;	

	}
	if(m_pCanLastData != NULL)
	{
		delete [] m_pCanLastData;
		m_pCanLastData = NULL;
	}
}

/*
void CTable::LoadData(LPCTSTR strChPath)
{
	if(m_bLoadData) return;

	// Table file path
	CString strPath;
	strPath.Format("%s\\ADP%04d.cyc", strChPath, m_lIndex);	//각 Step의 Data가 저장되어 있는 파일 

	CFileFind afinder;
	if(afinder.FindFile(strPath))
	{
		afinder.FindNextFile();
		CStdioFile afile;
		if(afile.Open(strPath, CFile::modeRead|CFile::typeText|CFile::shareDenyNone))
		{
			//
			CStringList strlist;
			CString strContent;
			while(afile.ReadString(strContent))
			{
				if(!strContent.IsEmpty()) strlist.AddTail(strContent);
			}
			afile.Close();

			//
			POSITION pos = strlist.GetHeadPosition();
			int s=0,p1=0,p2=0;

			// 첫줄에서
			strContent   = strlist.GetNext(pos);
			s  = strContent.Find("StartT");
			p1 = strContent.Find('=', s);
			p2 = strContent.Find(',', s);
			m_StartTime.ParseDateTime(strContent.Mid(p1+1,p2-p1-1));
			s  = strContent.Find("Step");
			p1 = strContent.Find('=', s);
			p2 = strContent.Find(',', s);
			m_lStepNo = atoi(strContent.Mid(p1+1,p2-p1-1));
			if(!m_bLoadLastData)
			{
				s  = strContent.Find("IR");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_fltDCIR = (float) atof(strContent.Mid(p1+1,p2-p1-1));
				s  = strContent.Find("OCV");
				p1 = strContent.Find('=', s);
				p2 = strContent.Find(',', s);
				m_fltOCV = (float) atof(strContent.Mid(p1+1,p2-p1-1));
			}

			// 둘째줄에서
			strContent = strlist.GetNext(pos);
			if(!m_bLoadLastData)
			{
				s=0, p1=0;
				while(p1!=-1)
				{
					p1 = strContent.Find(',', s);
					m_strlistTitle.AddTail(strContent.Mid(s, p1-s));
					s  = p1+1;
				}
			}

			// 다음줄부터 데이터를 Loading함
			int N        = m_strlistTitle.GetCount();
			m_lNumOfData = strlist.GetCount()-2L;
			if(m_lNumOfData>0)
			{
				m_ppfltData = new float*[N];
				for(int i=0;i<N;i++) m_ppfltData[i] = new float[m_lNumOfData];

				//
				int j = 0;
				while(pos)
				{
					strContent = strlist.GetNext(pos);
					//
					s=0, p1=0;
					for(int i=0; i<N; i++)
					{
						p1 = strContent.Find(',', s);
						float fltData = (float)atof(strContent.Mid(s, p1-s));
						s  = p1+1;
						m_ppfltData[i][j]=fltData;
					}
					//
					j++;
				}
				//

				//TableList 라는 파일에서 읽은 Step의 가장 마지막값을 무시 하고 Step의 가장 마지막값을 Load 한다.
//ksh				if(!m_bLoadLastData)
				{
						if(m_bLoadLastData) delete [] m_pfltLastData;
						m_pfltLastData = new float[N];
//ksh					for(int i=0; i<N; i++) m_pfltLastData[i] = m_ppfltData[i][m_lNumOfData-1];
						for(int i=0; i<N; i++) m_pfltLastData[i] = m_ppfltData[i][m_lNumOfData-2];
				}
				//
				m_bLoadData     = TRUE;
				m_bLoadLastData = TRUE;
			}
		}
	}
}
*/


//ADPower Style data

// Added by 224 (2013/12/29) : 분할된 파일들을 처리하는 함수 구현
// 수정전 파일은 LoadData_v100B_ORIGIN()으로 보관함.
// 레코드 시작 위치 : m_lRecordIndex
// 레코드 사이즈    : nRecordSize (== sizeof(float) * lRecItemSize)
// 레코드 갯수      : m_lNumOfData
// m_ppfTempChData는 2차원 배열이다.
// m_ppfTempChData[i][j] : i는 레코드데이터, j는 데이터수이다.
// i,j행이 뒤바뀌어 있는 것에 조심 !!!
void CTable::LoadData_v100B(CString strChPath)
{
	if (m_bLoadData)
	{
		return;
	}

	//저장된 Record수를 검색
	if (m_lNumOfData <= 0)
	{
		return;		//실처리로 저장된 Data가 없음 
	}

	CString strTemp;

	// 컬럼을 모두 지우기.
	m_DataItemList.RemoveAll();

	// Added by 224 (2013/12/30) : Aux, Can 과 같은 방식으로의 데이터 로딩
	INT nChColCount	= LoadRawData(strChPath) ;

	//----------------------------------------------------------------
	//Data를 읽어들임

	ASSERT(m_ppfltData == NULL);
	//----------------------------------------------------------------

	//----------------------------------------------------------------
	int i ;
	//----------------------------------------------------------------

	int nAuxColCount = LoadAuxData(strChPath);

	m_ppfltData = new float*[GetRecordItemSize()];
	for( int i = 0; i < GetRecordItemSize(); i++)
	{
		m_ppfltData[i] = new float[m_lNumOfData];
		ZeroMemory(m_ppfltData[i], sizeof(float)*m_lNumOfData);
	}

	long lSizeChData = sizeof(float)*m_lNumOfData;
   	for(int cp = 0 ; cp < nChColCount; cp++)
	{
		memcpy(m_ppfltData[cp], m_ppfTempChData[cp], lSizeChData);
	}

	if (nAuxColCount > 0)
	{
		int nIndex = 0;
		for(int cp = nChColCount ; cp < GetRecordItemSize();cp++)
		{
			memcpy(m_ppfltData[cp], m_ppfTempAuxData[nIndex++], sizeof(float)* m_lNumOfData);	
		}
	}
	//----------------------------------------------------------------

	int ri = 0;
	for (ri = 0; ri < nChColCount; ri++)
	{
		if (m_ppfTempChData[ri] != NULL)
		{
			delete [] m_ppfTempChData[ri];
			m_ppfTempChData[ri] = NULL;
		}
	}

	for (ri = 0; ri < nAuxColCount; ri++)
	{
		if (m_ppfTempAuxData[ri] != NULL)
		{
			delete [] m_ppfTempAuxData[ri];
			m_ppfTempAuxData[ri] = NULL;
		}
	}
	//----------------------------------------------------------------

	delete [] m_ppfTempChData;
	m_ppfTempChData = NULL;

	delete [] m_ppfTempAuxData;
	m_ppfTempAuxData = NULL;

	//----------------------------------------------------------------

	LoadCanData(strChPath);
	m_bLoadData     = TRUE;
}

// Added by 224 (2013/12/29) : 기존 함수명을 LoadData_v100B()에서 LoadData_v100B_ORIGIN()으로 변경
// void CTable::LoadData_v100B(LPCTSTR strChPath)
void CTable::LoadData_v100B_ORIGIN(LPCTSTR strChPath)
{
	if (m_bLoadData)
	{
		return;
	}

	//저장된 Record수를 검색
	if (m_lNumOfData <= 0)
	{
		return;		//실처리로 저장된 Data가 없음 
	}
	

	// Table file path
//	strPath.Format("%s\\*_%06d.cyc", strChPath, 0/*m_lIndex*/);	//각 Step의 Data가 저장되어 있는 파일 
//자기 Table Data만 Loading 하도록 수정 필요 	
	CString strPath;
	strPath.Format("%s\\*.%s", strChPath, PS_RAW_FILE_NAME_EXT);	//각 Step의 Data가 저장되어 있는 파일 

	CFileFind afinder;
	if (afinder.FindFile(strPath) == FALSE)
	{
		return;
	}
	
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
//----------------------------------------------------------------
	//Data를 읽어들임
	FILE* fp = fopen(strPath, "rb");
	if (fp == NULL)
	{
		return;
	}

	//File Header를 읽음
	PS_FILE_ID_HEADER fileID;
	if (fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
	{
		fclose(fp);
		return;
	}

	if (fileID.nFileID != 5640)
	{
		fclose(fp);
		return;
	}

	PS_RECORD_FILE_HEADER *pRSHeader = new PS_RECORD_FILE_HEADER;
	ZeroMemory(pRSHeader, sizeof(PS_RECORD_FILE_HEADER));

	//File Header를 읽음
	if (fread(pRSHeader, sizeof(PS_RECORD_FILE_HEADER), 1, fp) < 1)	
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}

	//저장된 Column수를 구함 
	if (pRSHeader->nColumnCount < 1)		
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}
	
	int nChColCount = pRSHeader->nColumnCount;
	m_DataItemList.RemoveAll();
	for (int i = 0; i < nChColCount; i++)
	{
		m_DataItemList.Add(pRSHeader->awColumnItem[i]);
	}
	delete pRSHeader;

	int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_RECORD_FILE_HEADER);
	//저장된 Data의 크기 
	size_t nRecordSize = sizeof(float) * m_DataItemList.GetSize();

	ASSERT(m_ppfltData == NULL);

	//2차원 동적 배열 
//	m_ppfTempChData = new float*[GetRecordItemSize()];
//	for( int i = 0; i < GetRecordItemSize(); i++)

	// 컬럼별로 데이터의 값을 수집한다.
	LONG lRecItemSize = GetRecordItemSize() ;
	m_ppfTempChData = new float* [lRecItemSize];
	for (int i = 0; i < lRecItemSize; i++)
	{
		m_ppfTempChData[i] = new float[m_lNumOfData];
	}

	//----------------------------------------------------------------
	// 주어진 Index로 이동한다.
	fseek(fp, m_lRecordIndex * nRecordSize + nHeaderSize, SEEK_SET);
	//----------------------------------------------------------------
	
//	float *fpBuff = new float[GetRecordItemSize()];
	// Added by 224 (2013/12/29) :
	// lRecItemSize 는 단지 숫자만이고, nRecordSize는 실제 메모리 할당 사이즈이다.
	// 즉, fpBuff의 실제 메모리 사이즈는 sizeof(float) * lRecItemSize == nRecordSize 이다.
	float* fpBuff = new float[lRecItemSize];
	int rsCount = 0;
	while (fread(fpBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lNumOfData)
	{
//		for (int i = 0; i < GetRecordItemSize(); i++)
		for (int i = 0; i < lRecItemSize; i++)
		{
			if (i == 22)
			{
//				TRACE("buff = %f \n", fpBuff[i]);

				CString strTemp;
				strTemp.Format("%.05f",fpBuff[i]);
   				fpBuff[i] = atof(strTemp);
//				TRACE("buff = %f \n" , fpBuff[i]);

				//m_ppfTempChData[i][rsCount] = atof(strTemp);
			}

			m_ppfTempChData[i][rsCount] = fpBuff[i];
		}

		rsCount++;
	}

	delete [] fpBuff;
	fclose(fp);


//----------------------------------------------------------------
//----------------------------------------------------------------

	int nAuxColCount = LoadAuxData(strChPath);

	m_ppfltData = new float*[GetRecordItemSize()];
	for (int i = 0; i < GetRecordItemSize(); i++)
	{
		m_ppfltData[i] = new float[m_lNumOfData];
		ZeroMemory(m_ppfltData[i], sizeof(float)*m_lNumOfData);
	}

	long lSizeChData = sizeof(float)*m_lNumOfData;
   	for(int cp = 0 ; cp < nChColCount; cp++)
	{
		memcpy(m_ppfltData[cp], m_ppfTempChData[cp], lSizeChData);
	}

	if (nAuxColCount > 0)
	{
		int nIndex = 0;
		for(int cp = nChColCount ; cp < GetRecordItemSize();cp++)
		{
			memcpy(m_ppfltData[cp], m_ppfTempAuxData[nIndex++], sizeof(float)* m_lNumOfData);	
		}
	}

//----------------------------------------------------------------

	int ri = 0;
	for (ri = 0; ri < nChColCount; ri++)
	{
		if (m_ppfTempChData[ri] != NULL)
		{
			delete [] m_ppfTempChData[ri];
			m_ppfTempChData[ri] = NULL;
		}
	}

	for (ri = 0; ri < nAuxColCount; ri++)
	{
		if (m_ppfTempAuxData[ri] != NULL)
		{
			delete [] m_ppfTempAuxData[ri];
			m_ppfTempAuxData[ri] = NULL;
		}
	}

//----------------------------------------------------------------

	delete [] m_ppfTempChData;
	m_ppfTempChData = NULL;

	delete [] m_ppfTempAuxData;
	m_ppfTempAuxData = NULL;

//----------------------------------------------------------------

	LoadCanData(strChPath);
	m_bLoadData     = TRUE;
}

//ADP Style data
//1. 실처리 Data Loading
//2. Step start data loading

//=> Step 시작 data 포함과 미포함 data 요청 명령이 분리되어야 한다.
//Data 복구시등에서는 Step 시작 data가 포함되지 않은 data를 return하여야 한다.

/*
Commented by 224 (2013/12/27) : 호출하는 곳이 없음.
void CTable::LoadDataB(LPCTSTR strChPath)
{
	//이미 Loading하였거나 저장된 Record가 없음 
	if(m_bLoadData || m_lNumOfData <=0 )	return;		//실처리로 저장된 Data가 없음 

	// Table file path
	CString strTemp;
	CString strPath, strStepStartPath;
	CFileFind afinder;
	FILE *fp = NULL;

	//Raw data file
	strTemp.Format("%s\\*.%s", strChPath, PS_RAW_FILE_NAME_EXT);		//각 Step의 Data가 저장되어 있는 파일 
	if(afinder.FindFile(strTemp) == FALSE)	return;
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	afinder.Close();

	//msec save file
	float fStartData[5][600];
	int nStepStartDataCount = 0;	
	strTemp.Format("%s\\StepStart\\*_C%06d_S%02d.csv", strChPath, GetCycleNo(), GetStepNo());
	if(afinder.FindFile(strTemp))
	{
		afinder.FindNextFile();
		strStepStartPath = afinder.GetFilePath();
		afinder.Close();
		
		fp = fopen(strStepStartPath, "rb");
		if(fp)
		{
			float fStepTime, fDataVtg, fDataCrt, fDataCap, fWattHour;
			//모든 Data를 Laoding한다.
			char szBuff[64];
			if(fscanf(fp, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
			{
				//Line 수 검사 
				while(fscanf(fp, "%f,%f,%f,%f,%f", &fStepTime, &fDataVtg, &fDataCrt, &fDataCap, &fWattHour) > 0)
				{
					fStartData[0][nStepStartDataCount] = fStepTime;
					fStartData[1][nStepStartDataCount] = fDataVtg;
					fStartData[2][nStepStartDataCount] = fDataCrt;
					fStartData[3][nStepStartDataCount] = fDataCap;
					fStartData[4][nStepStartDataCount] = fWattHour;
					nStepStartDataCount++;
					if(nStepStartDataCount >= 600)	break;
				}
			}

			if(fp) fclose(fp);
		}
	}

	//step data를 포함한다.
	m_lLoadedDataCount = m_lNumOfData + nStepStartDataCount;
	
	//Data를 읽어들임
	fp = fopen(strPath, "rb");
	if(fp)
	{
		//File Header를 읽음
		PS_FILE_ID_HEADER fileID;
		if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
		{
			fclose(fp);
			return;
		}
		if(fileID.nFileID != 5640)
		{
			fclose(fp);
			return;
		}

		PS_RECORD_FILE_HEADER *pRSHeader = new PS_RECORD_FILE_HEADER;
		ZeroMemory(pRSHeader, sizeof(PS_RECORD_FILE_HEADER));

		//File Header를 읽음
		if(fread(pRSHeader, sizeof(PS_RECORD_FILE_HEADER), 1, fp) < 1)	
		{
			delete pRSHeader;
			fclose(fp);
			return;
		}

		//저장된 Column수를 구함 
		if(pRSHeader->nColumnCount < 1)		
		{
			delete pRSHeader;
			fclose(fp);
			return;
		}	

		m_DataItemList.RemoveAll();
		for(int i=0; i<pRSHeader->nColumnCount; i++)
		{
			m_DataItemList.Add(pRSHeader->awColumnItem[i]);
		}
		delete pRSHeader;

		int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_RECORD_FILE_HEADER);
		//저장된 Data의 크기 
		size_t nRecordSize = sizeof(float)*m_DataItemList.GetSize();

		//주어진 Index로 이동한다.
		fseek(fp, m_lRecordIndex*nRecordSize+nHeaderSize,SEEK_SET);
		
		ASSERT(m_ppfltData == NULL);
		//2차원 동적 배열 
		m_ppfltData = new float*[GetRecordItemSize()];
		for(int i = 0; i<GetRecordItemSize(); i++) 
		{
			m_ppfltData[i] = new float[m_lLoadedDataCount];
		}

		float *fpBuff = new float[GetRecordItemSize()];
		int rsCount = 0;
		int nIndicator = 0;
		
		int time_index     = FindItemIndex(PS_STEP_TIME);
		int vtg_index      = FindItemIndex(PS_VOLTAGE);
		int crt_index      = FindItemIndex(PS_CURRENT);
		int cap_Index      = FindItemIndex(PS_CAPACITY);
		int watthour_Index = FindItemIndex(PS_WATT_HOUR);
		int data_Index     = FindItemIndex(PS_DATA_SEQ);

		while (fread(fpBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lLoadedDataCount)
		{	
			//Step Start data를 추가한다.
			//시간이 중복되더라도 추가한다.(그래프 그리시 생략)
			while (fStartData[0][nIndicator] <= fpBuff[time_index] &&  nIndicator < nStepStartDataCount)
			{
				if(data_Index >= 0)	m_ppfltData[0][rsCount] = 0;		//Serial을 0으로 한다.
				if(time_index >= 0)	m_ppfltData[time_index][rsCount] = fStartData[0][nIndicator];
				if(vtg_index >= 0)	m_ppfltData[vtg_index][rsCount] = fStartData[1][nIndicator];
				if(crt_index >= 0)	m_ppfltData[crt_index][rsCount] = fStartData[2][nIndicator];
				if(cap_Index >= 0)	m_ppfltData[cap_Index][rsCount] = fStartData[3][nIndicator];
				if(watthour_Index >= 0)	m_ppfltData[watthour_Index][rsCount] = fStartData[4][nIndicator];
				nIndicator++;
				rsCount++;
			}

			for (int i = 0; i < GetRecordItemSize(); i++)
			{
				m_ppfltData[i][rsCount] = fpBuff[i];
			}

			rsCount++;
		}
		
		delete [] fpBuff;
		fclose(fp);

		m_bLoadData = TRUE;
	}
}
*/


/*
Commented by 224 (2013/12/27) : 호출하는 곳이 없음.

//해당 table의 Data 시작 위치를 찾을 때 
//Index를 활용하지 않고 처음 부터 Scan한다.
void CTable::LoadDataC(LPCTSTR strChPath)
{
	if(m_bLoadData)		return;

	// Table file path
	CString strPath;
//	strPath.Format("%s\\*_%06d.cyc", strChPath, 0);	//각 Step의 Data가 저장되어 있는 파일 
//자기 Table Data만 Loading 하도록 수정 필요 
	
	strPath.Format("%s\\*.%s", strChPath, PS_RAW_FILE_NAME_EXT);	//각 Step의 Data가 저장되어 있는 파일 

	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return;
	
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	//저장된 Record수를 검색
	if(m_lNumOfData <=0 )	return;		//실처리로 저장된 Data가 없음 

	//Data를 읽어들임
	FILE *fp = fopen(strPath, "rb");
	if(fp == NULL)		return;

	//File Header를 읽음
	PS_FILE_ID_HEADER fileID;
	if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
	{
		fclose(fp);
		return;
	}
	if(fileID.nFileID != 5640)
	{
		fclose(fp);
		return;
	}

	PS_RECORD_FILE_HEADER *pRSHeader = new PS_RECORD_FILE_HEADER;
	ZeroMemory(pRSHeader, sizeof(PS_RECORD_FILE_HEADER));

	//File Header를 읽음
	if(fread(pRSHeader, sizeof(PS_RECORD_FILE_HEADER), 1, fp) < 1)	
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}

	//저장된 Column수를 구함 
	if(pRSHeader->nColumnCount < 1)		
	{
		delete pRSHeader;
		fclose(fp);
		return;
	}	
	m_DataItemList.RemoveAll();
	for(int i=0; i<pRSHeader->nColumnCount; i++)
	{
		m_DataItemList.Add(pRSHeader->awColumnItem[i]);
	}
	delete pRSHeader;

	int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_RECORD_FILE_HEADER);

	//저장된 Data의 크기 
	size_t nRecordSize = sizeof(float)*m_DataItemList.GetSize();

	//주어진 Index로 이동한다.
	fseek(fp, m_lRecordIndex*nRecordSize+nHeaderSize,SEEK_SET);
	
	//2차원 동적 배열 
	ASSERT(m_ppfltData == NULL);
	m_ppfltData = new float*[GetRecordItemSize()];
	for( i=0; i<GetRecordItemSize(); i++) 
	{
		m_ppfltData[i] = new float[m_lNumOfData];
	}

	//File read buffer
	float *fpBuff = new float[GetRecordItemSize()];
	
	int rsCount = 0;
	//read file
	while(fread(fpBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lNumOfData)
	{	
		for(int i=0; i<GetRecordItemSize(); i++)
		{
			m_ppfltData[i][rsCount] = fpBuff[i];
		}
		rsCount++;
	}

	//release buffer
	delete [] fpBuff;

	//close file
	fclose(fp);

	m_bLoadData     = TRUE;
}
*/

int CTable::FindItemIndex(WORD nItem)
{
	for(int a=0; a<m_DataItemList.GetSize(); a++)
	{
		if(m_DataItemList.GetAt(a) == nItem)
		{
			return a;
			break;
		}
	}
	return -1;
}

		
float CTable::GetLastData(LPCTSTR strTitle, WORD wPoint)
{
	if (!m_bLoadLastData)
	{
		return FLT_MAX;
	}

	int nIndex1 =0, nIndex2 = 0, nIndex3 = 0, nIndex4 = 0;

	CString strTitleX(strTitle);	//ljb
	if(strTitleX.CompareNoCase("V_MaxDiff")==0)
	{
		nIndex1 = FindIndexOfTitle(RS_COL_VOLTAGE1);
		nIndex2 = FindIndexOfTitle(RS_COL_VOLTAGE2);
		nIndex3 = FindIndexOfTitle(RS_COL_VOLTAGE3);
		nIndex4 = FindIndexOfTitle(RS_COL_VOLTAGE4);

		if(nIndex1 < 0 || nIndex2 < 0 || nIndex3< 0 || nIndex4 < 0)		return 0.0f;

		float fltV1 = m_pfltLastData[nIndex1];
		float fltV2 = m_pfltLastData[nIndex2];
		float fltV3 = m_pfltLastData[nIndex3];
		float fltV4 = m_pfltLastData[nIndex4];
		//
		BYTE byVPoint = LOBYTE(wPoint);
		if     (byVPoint==0x03) // 1번-2번
		{
			return (float)fabs(fltV1-fltV2);
		}
		else if(byVPoint==0x05) // 1번-3번
		{
			return (float)fabs(fltV1-fltV3);
		}
		else if(byVPoint==0x09) // 1번-4번
		{
			return (float)fabs(fltV1-fltV4);
		}
		else if(byVPoint==0x06) // 2번-3번
		{
			return (float)fabs(fltV2-fltV3);
		}
		else if(byVPoint==0x0A) // 2번-4번
		{
			return (float)fabs(fltV2-fltV4);
		}
		else if(byVPoint==0x0C) // 3번-4번
		{
			return (float)fabs(fltV3-fltV4);
		}
		else if(byVPoint==0x07) // 1번-2번-3번
		{
			return __max(__max(fltV1,fltV2),fltV3)-__min(__min(fltV1,fltV2),fltV3);
		}
		else if(byVPoint==0x0B) // 1번-2번-4번
		{
			return __max(__max(fltV1,fltV2),fltV4)-__min(__min(fltV1,fltV2),fltV4);
		}
		else if(byVPoint==0x0D) // 1번-3번-4번
		{
			return __max(__max(fltV1,fltV3),fltV4)-__min(__min(fltV1,fltV3),fltV4);
		}
		else if(byVPoint==0x0E) // 2번-3번-4번
		{
			return __max(__max(fltV2,fltV3),fltV4)-__min(__min(fltV2,fltV3),fltV4);
		}
		else if(byVPoint==0x0F) // 1번-2번-3번-4번
		{
			return __max(__max(__max(fltV1,fltV2),fltV3),fltV4)-__min(__min(__min(fltV1,fltV2),fltV3),fltV4);
		}
		else                    // 한개 선택 or 선택하지 않음
		{
			return 0.0f;
		}
	}
	else if(strTitleX.CompareNoCase("T_MaxDiff")==0)
	{
		//nIndex1 = FindIndexOfTitle(RS_COL_TEMPERATURE1);
		nIndex1 = FindIndexOfTitle(RS_COL_AUX_TEMP);
		//nIndex2 = FindIndexOfTitle(RS_COL_TEMPERATURE2);
		if(nIndex1 < 0 || nIndex2 < 0 || nIndex3< 0 || nIndex4 < 0)		return 0.0f;

		float fltT1 = m_pfltLastData[nIndex1];
		float fltT2 = m_pfltLastData[nIndex2];
		//
		BYTE byTPoint = HIBYTE(wPoint);
		if(byTPoint==0x03) // 1번-2번
		{
			return (float)fabs(fltT1-fltT2);
		}
		else               // 한개 선택 or 선택하지 않음
		{
			return 0.0f;
		}
	}
	else if(strTitleX.CompareNoCase(RS_COL_OCV)==0)	//첫번째 전압이 OCV임 
	{
		//OCV 항목을 그리기 이전에 반드시 LoadDataA()를 먼저 호출 하여야 한다.
		if(m_bLoadData == FALSE)
		{
			return 0.0f;
		}
		nIndex1 = FindIndexOfTitle(PS_VOLTAGE);
		if(nIndex1 < 0)	return 0.0f;
		else 		return m_ppfltData[nIndex1][0];		//제일 첫번째 전압이 OCV이다.
	}
	else if(strTitleX.CompareNoCase(RS_COL_TYPE)==0)
	{
		return (float)GetType();
	}
	else if(strTitleX.CompareNoCase(RS_COL_STEP_NO)==0)
	{
		return (float)GetStepNo();
	}
	else if(strTitleX.CompareNoCase(RS_COL_TOT_CYCLE)==0)
	{
		return (float)GetCycleNo();
	}
	else if(strTitleX.CompareNoCase(RS_COL_STATE)==0)
	{
		return (float)m_chState;
	}
	else if(strTitleX.CompareNoCase(RS_COL_CUR_CYCLE)==0)
	{
		return (float)m_nCurrentCycleNum;
	}
	else if(strTitleX.CompareNoCase(RS_COL_CODE)==0)
	{
		return (float)m_chCode;
	}
	else if(strTitleX.CompareNoCase(RS_COL_GRADE)==0)
	{
		return (float)m_chGradeCode;
	}
	else if(strTitleX.CompareNoCase(RS_COL_CH_NO)==0)
	{
		return (float)m_chNo;
	}
	else if(strTitleX.CompareNoCase(RS_COL_SAVE_ACCU)==0)
	{
		return (float)m_chDataSelect;
	}
	else if(strTitleX.CompareNoCase(RS_COL_SEQ)==0)
	{
		return (float)m_lSaveSequence;
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.18
	else if(strTitleX.CompareNoCase(RS_COL_TOT_TIME_DAY)==0) 
	{	
 		//if(m_ltotalDay>0)
 		//{
 		//	TRACE("Common Table - Day > 0\n");
 		//}
		return (float)m_ltotalDay;
	}
	else if(strTitleX.CompareNoCase(RS_COL_TIME_DAY)==0) 
	{	
 		//if(m_lstepDay>0)
 		//{
 		//	TRACE("Common Table - Step Day > 0\n");
 		//}
		return (float)m_lstepDay;
	}	
	// -
	//////////////////////////////////////////////////////////////////////////
 	//else if(strTitleX.CompareNoCase(RS_COL_TEMPERATURE1) == 0)
	else if((strTitleX.Left(4).CompareNoCase(RS_COL_AUX_TEMP) == 0) && (strTitleX.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) != 0)) //20180620 yulee
	{
		//Get Temperature from wPoint Index
		if(m_pfltLastData == NULL) return 0.0f;
		if(m_pfltAuxLastData == NULL) return 0.0f;

		int nFirst = strTitleX.ReverseFind('[');
		int nEnd = strTitleX.ReverseFind(']');
		//AfxMessageBox(strTitleX);
		CString strTemp;
		strTemp = strTitleX.Mid(nFirst+1,nEnd-(nFirst+1));
		//AfxMessageBox(strTemp);
		if (strTemp.IsEmpty())
		{
			strTemp.Format("%d",wPoint+1);		//ljb 20131118 add
			//return (float)m_pfltAuxLastData[atoi(strTemp)-1];//ljb 20131118 	Aux 데이터를 리턴하는 위치 변경 / 1000
		}
		return (float)m_pfltAuxLastData[atoi(strTemp)-1] / 1000;//ljb 20131015 	Aux 데이터를 리턴하는 위치 변경 / 1000

		//return (float)m_pfltAuxLastData[wPoint];
	}
	else if(strTitleX.Left(4).CompareNoCase(RS_COL_AUX_VOLT) == 0)
	{
		if(m_pfltLastData == NULL) return 0.0f;
		if(m_pfltAuxLastData == NULL) return 0.0f;

		int nFirst = strTitleX.ReverseFind('[');
		int nEnd = strTitleX.ReverseFind(']');
		//		AfxMessageBox(strTitleX);
		CString strTemp;
		strTemp = strTitleX.Mid(nFirst+1,nEnd-(nFirst+1));
		//		AfxMessageBox(strTemp); 
		if (strTemp.IsEmpty())
		{
			strTemp.Format("%d",wPoint+1);		//ljb 20131118 add
			return (float)m_pfltAuxLastData[atoi(strTemp)-1];//ljb 20131118 Aux 데이터를 리턴하는 위치 변경 / 1000
		}
		return (float)m_pfltAuxLastData[atoi(strTemp)-1] / 1000;//ljb 20131015 Aux 데이터를 리턴하는 위치 변경 / 1000

		//return (float)m_pfltAuxLastData[wPoint] / 1000;//ljb 0131015 Aux 데이터를 리턴하는 위치 변경 / 1000
	}
	else if(strTitleX.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) == 0) //20180620 yulee
	{
		//Get Temperature from wPoint Index
		if(m_pfltLastData == NULL) return 0.0f;
		if(m_pfltAuxLastData == NULL) return 0.0f;
		
		int nFirst = strTitleX.ReverseFind('[');
		int nEnd = strTitleX.ReverseFind(']');
		//		AfxMessageBox(strTitleX);
		CString strTemp;
		strTemp = strTitleX.Mid(nFirst+1,nEnd-(nFirst+1));
		//		AfxMessageBox(strTemp);
		if (strTemp.IsEmpty())
		{
			strTemp.Format("%d",wPoint+1);		//ljb 20131118 add
			//return (float)m_pfltAuxLastData[atoi(strTemp)-1];//ljb 20131118 	Aux 데이터를 리턴하는 위치 변경 / 1000
		}
		return (float)m_pfltAuxLastData[atoi(strTemp)-1];//ljb 20131015 	Aux 데이터를 리턴하는 위치 변경 / 1000
		
		//		return (float)m_pfltAuxLastData[wPoint];
	}
	else if(strTitleX.Left(9).CompareNoCase(RS_COL_AUX_HUMI) == 0) //ksj 20200116
	{
		//Get Temperature from wPoint Index
		if(m_pfltLastData == NULL) return 0.0f;
		if(m_pfltAuxLastData == NULL) return 0.0f;

		int nFirst = strTitleX.ReverseFind('[');
		int nEnd = strTitleX.ReverseFind(']');
		//		AfxMessageBox(strTitleX);
		CString strTemp;
		strTemp = strTitleX.Mid(nFirst+1,nEnd-(nFirst+1));
		//		AfxMessageBox(strTemp);
		if (strTemp.IsEmpty())
		{
			strTemp.Format("%d",wPoint+1);		//ljb 20131118 add
			//return (float)m_pfltAuxLastData[atoi(strTemp)-1];//ljb 20131118 	Aux 데이터를 리턴하는 위치 변경 / 1000
		}
		return (float)m_pfltAuxLastData[atoi(strTemp)-1];//ljb 20131015 	Aux 데이터를 리턴하는 위치 변경 / 1000

		//		return (float)m_pfltAuxLastData[wPoint];
	}
	else if(strTitleX.CompareNoCase(RS_COL_ACC_CYCLE1) == 0)		//ljb v1009
	{
		return (float)m_lAccCycleGroupNum1;
	}
	else if(strTitleX.CompareNoCase(RS_COL_ACC_CYCLE2) == 0)
	{
		return (float)m_lAccCycleGroupNum2;
	}
	else if(strTitleX.CompareNoCase(RS_COL_ACC_CYCLE3) == 0)
	{
		return (float)m_lAccCycleGroupNum3;
	}
	else if(strTitleX.CompareNoCase(RS_COL_ACC_CYCLE4) == 0)
	{
		return (float)m_lAccCycleGroupNum4;
	}
	else if(strTitleX.CompareNoCase(RS_COL_ACC_CYCLE5) == 0)
	{
		return (float)m_lAccCycleGroupNum5;
	}

	else if(strTitleX.CompareNoCase(RS_COL_MULTI_CYCLE1) == 0)		//ljb v1009
	{
		return (float)m_lMultiCycleGroupNum1;
	}
	else if(strTitleX.CompareNoCase(RS_COL_MULTI_CYCLE2) == 0)
	{
		return (float)m_lMultiCycleGroupNum2;
	}
	else if(strTitleX.CompareNoCase(RS_COL_MULTI_CYCLE3) == 0)
	{
		return (float)m_lMultiCycleGroupNum3;
	}
	else if(strTitleX.CompareNoCase(RS_COL_MULTI_CYCLE4) == 0)
	{
		return (float)m_lMultiCycleGroupNum4;
	}
	else if(strTitleX.CompareNoCase(RS_COL_MULTI_CYCLE5) == 0)
	{
		return (float)m_lMultiCycleGroupNum5;
	}

	else if(strTitleX.CompareNoCase(RS_COL_SYNC_DATE) == 0)
	{
		TRACE("%d \n", m_lSyncDate);		//제일 첫번째 전압이 OCV이다.)
		return m_lSyncDate;
	}
	else if(strTitleX.CompareNoCase(RS_COL_SYNC_TIME) == 0)
	{
		TRACE("%f \n", m_fSyncTime);		//제일 첫번째 전압이 OCV이다.)
		return m_fSyncTime;
	}
	else if(strTitleX.CompareNoCase(RS_COL_CELLBAL_CH_DATA) == 0)
	{		
		return m_dCellBALChData;
	}
	//ljb 20131208 add
	//else if(strTitleX.CompareNoCase(RS_COL_TIME) == 0)
	//{
	//	TRACE("%f \n", m_fSyncTime);		//제일 첫번째 전압이 OCV이다.)
	//	ULONG	m_lstepDay;		//ljb 20131208 add
	//	ULONG	m_ltotalDay;	//ljb 20131208 add
	//	return m_fSyncTime;
	//}
	//else if(strTitleX.CompareNoCase(RS_COL_TOT_TIME) == 0)
	//{
	//	TRACE("%f \n", m_fSyncTime);		//제일 첫번째 전압이 OCV이다.)
	//	return m_fSyncTime;
	//}
	
	////ljb 2011412 이재복 ////////// s
	//else if(strTitleX.CompareNoCase(RS_COL_COMM_STATE) == 0)
	//{
	//	return (float)m_lchCommState;
	//}
	//else if(strTitleX.CompareNoCase(RS_COL_OUTPUT_STATE) == 0)
	//{
	//	return (float)m_lchOutputState;
	//}
	//else if(strTitleX.CompareNoCase(RS_COL_INPUT_STATE) == 0)
	//{
	//	return (float)m_lchInputState;
	//}
	//else if(strTitleX.CompareNoCase(RS_COL_VOLTAGE_INPUT) == 0)
	//{
	//	return (float)m_lMultiCycleGroupNum5;
	//}
	//else if(strTitleX.CompareNoCase(RS_COL_VOLTAGE_POWER) == 0)
	//{
	//	return (float)m_lMultiCycleGroupNum5;
	//}
	//else if(strTitleX.CompareNoCase(RS_COL_VOLTAGE_BUS) == 0)
	//{
	//	return (float)m_lMultiCycleGroupNum5;
	//}
	////ljb 2011412 이재복 ////////// e

	//default data
	nIndex1 = FindIndexOfTitle(strTitle);	
	if (nIndex1 < 0)
	{
		return 0.0f;
	}

	return m_pfltLastData[nIndex1];
}

//fltPoint* CTable::GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
fltPoint* CTable::GetData(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint, int nType) //20180710 yulee nType 추가
{
	int nDay = 0;//yulee 20180824 //yulee 20191017 OverChargeDischarger Mark
	//2014.10.09 예외처리추가.
	try
	{
		if(!m_bLoadData)
			return NULL;

		int iXIndex=0, iXindex_day=0, iYIndex1=0, iYIndex2=0, iYIndex3=0, iYIndex4=0;
		
		iXIndex = FindRsColumnIndex(strXAxisTitle);
		iXindex_day = FindRsColumnIndex(RS_COL_TIME_DAY); //lyj 20201208
		CString strYAxisT(strYAxisTitle);
		
		if(strYAxisT.CompareNoCase(RS_COL_WATT)==0)
		{
	 		//iYIndex1 = FindRsColumnIndex(PS_VOLTAGE);
	 		//iYIndex2 = FindRsColumnIndex(PS_CURRENT);
			iYIndex1 = FindRsColumnIndex(PS_WATT);
		}
		else if(strYAxisT.CompareNoCase("V_MaxDiff")==0)
		{
			iYIndex1 = FindRsColumnIndex(RS_COL_VOLTAGE1);
			iYIndex2 = FindRsColumnIndex(RS_COL_VOLTAGE2);
			iYIndex3 = FindRsColumnIndex(RS_COL_VOLTAGE3);
			iYIndex4 = FindRsColumnIndex(RS_COL_VOLTAGE4);
		}
		else if(strYAxisT.CompareNoCase("T_MaxDiff")==0)
		{
	 		//iYIndex1 = FindRsColumnIndex(RS_COL_TEMPERATURE1);
			iYIndex1 = FindRsColumnIndex(RS_COL_AUX_TEMP);
			//iYIndex2 = FindRsColumnIndex(RS_COL_TEMPERATURE2);
		}
	 	//else if(CString(strYAxisTitle).CompareNoCase(RS_COL_TEMPERATURE1) == 0)
		else if((strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_TEMP) == 0) && (strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) != 0)) //20180702
		{
			//Aux Temperature는 아래에서 직접 처리
			iYIndex1 = 0;
		}
		else if(strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) == 0) //20180702
		{
			//Aux Temperature는 아래에서 직접 처리
			iYIndex1 = 0;
		}
		else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_HUMI) == 0) //ksj 20200116
		{
			//Aux Temperature는 아래에서 직접 처리
			iYIndex1 = 0;
		}
		//else if(strYAxisT.CompareNoCase(RS_COL_AUX_VOLTAGE) == 0)
		else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_VOLT) == 0)
		{
			//Aux Voltage는 아래에서 직접 처리
			iYIndex1 = 0; 
		}
		else if((strYAxisT.GetLength() >= 5) && 
			(strYAxisT.Left(3).Compare("Vol") !=0) &&
			(strYAxisT.Left(3).Compare("Cur") !=0) &&
			(strYAxisT.Left(1).Compare("[") == 0))//yulee 20180911 //yulee 20180911-1-2
		{
			if(strYAxisT.Mid(5,3).CompareNoCase(RS_COL_CAN) == 0)	//ljb 20131104 add
			{
				iYIndex1 = 0; 
			}
		}
		else 
		{
			iYIndex1 = FindRsColumnIndex(strYAxisTitle);
		}

		fltPoint* pfltPt = NULL;
		fltPoint* pfltPt_day = NULL; //lyj 20201208
		//Data가 저장되어 있지 않을 경우 
		if(iXIndex < 0 || iYIndex1 < 0 || iYIndex2 < 0 || iYIndex3 < 0 || iYIndex4 < 0)
		{
			lDataNum = 0;
			return pfltPt;
		}

		bool bTimeX = FALSE;
		bool bSequenceX = FALSE;

		if(CString(strXAxisTitle).CompareNoCase(RS_COL_SEQ)==0) //lyj 20200520 X축 Sequence 일때 값-1
		{
			bSequenceX = TRUE;
		}
		else
		{
			bSequenceX = FALSE;
		}

		if(CString(strXAxisTitle).CompareNoCase(RS_COL_TIME)==0) //lyj 20201208
		{
			bTimeX = TRUE;
		}
		else
		{
			bTimeX = FALSE;
		}


		BOOL bDetailData;
		bDetailData = AfxGetApp()->GetProfileInt("Config", "OnDetailData", 1);
		//
		lDataNum = m_lLoadedDataCount;
		pfltPt   = new fltPoint[lDataNum];
		pfltPt_day = new fltPoint[lDataNum]; //lyj 20201208
		float fTimePtX=0.0f , fDayPtX = 0.0 , pre_fTimePtX = 0.0f , fRecodeTime = 0.0f;
		//
		for(int i= 0; i<m_lLoadedDataCount; i++)
		{
			//
			// X축값
			//yulee 20181008-2
			if(bDetailData == 1)
			{
				pfltPt[i].x = m_ppfltData[iXIndex][i];
				pfltPt_day[i].x = m_ppfltData[iXindex_day][i]; //lyj 20201208
			}
			else
			{
				pfltPt[i].x = floor(m_ppfltData[iXIndex][i]*10.f + 0.5)/10.f;
				pfltPt_day[i].x = m_ppfltData[iXindex_day][i]; //lyj 20201208
			}

			if(bSequenceX)
			{
				pfltPt[i].x--;
			}

			//yulee 20180824 날짜 넘어가는 것에 대한 수정 //yulee 20191017 OverChargeDischarger Mark 
  			/*if((i%86400 == 0) && (i != 0))
 			{
 				nDay = nDay +1;
 			}
 
 			if(nDay != 0)
 			{
 				pfltPt[i].x += 86400*nDay;
 			}*/

			if(bTimeX) //lyj 20201208
			{
				fTimePtX = pfltPt[i].x;
				fDayPtX = pfltPt_day[i].x;
				
				if(fDayPtX>0)
				{
					pfltPt[i].x = pfltPt[i].x + (pre_fTimePtX * fDayPtX) + fRecodeTime; //lyj 20201208 현재시간 + (직전시간*날짜) + 기록조건
				}
				else
				{
					if(pfltPt[i].x == 120.0f) //120초일때 기록조건 구한다
					{
						fRecodeTime = fTimePtX - pre_fTimePtX;
					}
					pre_fTimePtX = fTimePtX; //lyj 20201208 마지막 시간 저장
				}
			}
			//
			// Y축값
			//
			if(CString(strYAxisTitle).CompareNoCase(RS_COL_WATT)==0)
			{
				//Bug Fixed 2006/5/8  
				//Watt / 1000.0f
				//kjh - 2007/09/20 Watt가 음수로 표시되던 것을 양수로 변환
				//pfltPt[i].y = m_ppfltData[iYIndex1][i]*m_ppfltData[iYIndex2][i]/1000.0f;
				//pfltPt[i].y = fabs(m_ppfltData[iYIndex1][i]*m_ppfltData[iYIndex2][i]/1000.0f);

				//pfltPt[i].y = fabs(m_ppfltData[iYIndex1][i]);
				if(bDetailData == 1)
					pfltPt[i].y = m_ppfltData[iYIndex1][i];			//ljb 20130507 add HL 정보옥 요청 절대값을 하지 않는다
				else
				{
					float fltTmp;
					fltTmp =  m_ppfltData[iYIndex1][i];	;
					pfltPt[i].y = fltPlaceFix(fltTmp, 1);//floor(fltTmp*10.f + 0.5)/10.f; //yulee 20181008-2
				}
			}
			else if(strYAxisT.CompareNoCase("V_MaxDiff")==0)
			{
				BYTE byVPoint = LOBYTE(wPoint);
				if     (byVPoint==0x03) // 1번-2번
				{
					pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex2][i]);
				}
				else if(byVPoint==0x05) // 1번-3번
				{
					pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex3][i]);
				}
				else if(byVPoint==0x09) // 1번-4번
				{
					pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex4][i]);
				}
				else if(byVPoint==0x06) // 2번-3번
				{
					pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex2][i]-m_ppfltData[iYIndex3][i]);
				}
				else if(byVPoint==0x0A) // 2번-4번
				{
					pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex2][i]-m_ppfltData[iYIndex4][i]);
				}
				else if(byVPoint==0x0C) // 3번-4번
				{
					pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex3][i]-m_ppfltData[iYIndex4][i]);
				}
				else if(byVPoint==0x07) // 1번-2번-3번
				{
					pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i])
							   -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]);
				}
				else if(byVPoint==0x0B) // 1번-2번-4번
				{
					pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex4][i])
							   -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex4][i]);
				}
				else if(byVPoint==0x0D) // 1번-3번-4번
				{
					pfltPt[i].y=__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
							   -__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
				}
				else if(byVPoint==0x0E) // 2번-3번-4번
				{
					pfltPt[i].y=__max(__max(m_ppfltData[iYIndex2][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
							   -__min(__min(m_ppfltData[iYIndex2][i],m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
				}
				else if(byVPoint==0x0F) // 1번-2번-3번-4번
				{
					pfltPt[i].y=__max(__max(__max(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i])
							   -__min(__min(__min(m_ppfltData[iYIndex1][i],m_ppfltData[iYIndex2][i]),m_ppfltData[iYIndex3][i]),m_ppfltData[iYIndex4][i]);
				}
				else                    // 한개 선택 or 선택하지 않음
				{
					pfltPt[i].y=0.0f;
				}
			}
			else if(strYAxisT.CompareNoCase("T_MaxDiff")==0)
			{
				BYTE byTPoint = HIBYTE(wPoint);
				if(byTPoint==0x03) // 1번-2번
				{
					pfltPt[i].y=(float)fabs(m_ppfltData[iYIndex1][i]-m_ppfltData[iYIndex2][i]);
				}
				else               // 한개 선택 or 선택하지 않음
				{
					pfltPt[i].y=0.0f;
				}
			}
			//else if(strYAxisT.CompareNoCase(RS_COL_TEMPERATURE1) == 0)
			//else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_TEMP) == 0) //20180611 주석처리 
			else if((strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_TEMP) == 0)&&((strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) != 0)))
			{
				int nIndex = GetAuxIndexToTitle(RS_COL_AUX_TEMP, wPoint);
				if(nIndex < 0)
				{
					lDataNum = 0;
					return pfltPt;
				}
				if(m_nAuxColumnCount < nIndex)
				{
					TRACE("Error Record Item Size(RecordItemSize : %d/ AuxColumnCount : %d)\n", GetRecordItemSize(), m_nAuxColumnCount);
					return NULL;
				}
				//pfltPt[i].y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i];
				
				if(bDetailData == 1)
					pfltPt[i].y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i] / 1000;	//ljb 2011222 이재복 AUX 단위//////////
				else
				{
					float fltTmp;
					fltTmp = (m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i])/1000;
					pfltPt[i].y = fltPlaceFix(fltTmp, 1);//floor(fltTmp*10.f + 0.5)/10.f; //yulee 20181008-2
				}
			}
			//else if(strYAxisT.CompareNoCase(RS_COL_AUX_VOLTAGE) == 0)
			else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_VOLT) == 0)
			{
				int nIndex = GetAuxIndexToTitle(RS_COL_AUX_VOLT, wPoint);
				if(nIndex < 0)
				{
					lDataNum = 0;
					return pfltPt;
				}
				//pfltPt[i].y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i];

				if(bDetailData == 1)
					pfltPt[i].y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i];
					else
				{
					float fltTmp;
					fltTmp = (m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i])/1000;
					pfltPt[i].y = fltPlaceFix(fltTmp, 3)*1000;//floor(fltTmp*1000.f+0.5)/1000.f; //yulee 20181008-2
				}
			}
			else if(strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) == 0)
			{
				int nIndex = GetAuxIndexToTitle(RS_COL_AUX_TEMPTH, wPoint);
				if(nIndex < 0)
				{
					lDataNum = 0;
					return pfltPt;
				}
				//pfltPt[i].y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i];

				if(bDetailData == 1)
					pfltPt[i].y = (m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i])/1000.0;
				else
				{
					float fltTmp;
					fltTmp = (m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i])/1000;
					pfltPt[i].y = fltPlaceFix(fltTmp, 1);//floor(fltTmp*10.f + 0.5)/10.f; //yulee 20181008-2
				}
			}
			//ksj 20200206 : v1016 습도 센서 추가
			else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_HUMI) == 0)
			{
				int nIndex = GetAuxIndexToTitle(RS_COL_AUX_HUMI, wPoint);
				if(nIndex < 0)
				{
					lDataNum = 0;
					return pfltPt;
				}
				//pfltPt[i].y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i];

				if(bDetailData == 1)
					pfltPt[i].y = (m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i])/1000.0;
				else
				{
					float fltTmp;
					fltTmp = (m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][i])/1000;
					pfltPt[i].y = fltPlaceFix(fltTmp, 1);//floor(fltTmp*10.f + 0.5)/10.f; //yulee 20181008-2
				}
			}
			//////////////////////////////////////////////////////////////////////////
			//ljb 2011215 이재복 S //////////
			else if((strYAxisT.GetLength() >= 5) && 
				(strYAxisT.Left(3).Compare("Vol") !=0) &&
				(strYAxisT.Left(3).Compare("Cur") !=0) &&
				(strYAxisT.Left(1).Compare("[") == 0))   //yulee 20180911 //yulee 20180911-1-2
			{
				if(strYAxisT.Mid(5,3).CompareNoCase(RS_COL_CAN) == 0)
				{
					int nIndex = GetCanIndexToTitle(RS_COL_CAN, wPoint);
					if(nIndex < 0)
					{
						lDataNum = 0;
						return pfltPt;
					}
					if(m_nCanColumnCount < nIndex)
					{
						TRACE("Error Record Item Size(RecordItemSize : %d/ CanColumnCount : %d)\n", GetRecordItemSize(), m_nCanColumnCount);
						return NULL;
					}
					//ljb 임시 찾기
					//pfltPt[i].y = m_ppCanData[GetRecordItemSize()- m_nCanColumnCount+nIndex][i].fVal[0];
					pfltPt[i].y = m_ppCanData[nIndex][i].fVal[0];			
				}
			}
			//////////////////////////////////////////////////////////////////////////
			else
			{
				if(bDetailData == 1)
					pfltPt[i].y = m_ppfltData[iYIndex1][i];
				else
				{
					float fltTmp;
					fltTmp = m_ppfltData[iYIndex1][i]/1000;
					pfltPt[i].y = fltPlaceFix(fltTmp, 3)*1000;//floor(fltTmp*1000.f+0.5)/1000.f; //yulee 20181008-2
				}
			
				//ljb 20180430 add start  /////////////////////////////////////////////////////#과충방전기#rest 스텝#오차 범위는 0
				if((CString(strYAxisTitle).CompareNoCase(RS_COL_CURRENT)==0) && (nType == 3)) //20180710 yulee nType 추가
				{
					if (pfltPt[i].y > -1600 && pfltPt[i].y < 1600) //스펙 전류 X 채널 수 
					{
						pfltPt[i].y = 0.0f;
					}
				}
				//ljb 20180430 add end	/////////////////////////////////////////////////////
			}
		}
		return pfltPt;
	}
	catch (...)
	{
		return NULL;
	}
	
}

float CTable::fltPlaceFix(float Num, int Place)
{
	return ((int)(Num*pow(10.0, Place)))/pow(10.0, Place);
}

// Added by 224 (2014/07/28) : 대용량 데이터의 과도한 메모리 할당으로 인한
// 메모리 오류로 인하여, 로드된 자료에서 해당하는 행의 값만 가져오는 방법 추가
//fltPoint* CTable::GetDatum(LONG& lDataNum, LPCTSTR strXAxisTitle, LPCTSTR strYAxisTitle, WORD wPoint)
fltPoint CTable::GetDatum(LPCTSTR strYAxisTitle, INT row, WORD wPoint)
{
	fltPoint fltRet = { 0.0f, 0.0f } ;

	if (!m_bLoadData)
	{
		return fltRet ;
	}

	int iXIndex=0, iYIndex1=0, iYIndex2=0, iYIndex3=0, iYIndex4=0;
	
	CString strXAxisTitle("Time") ;
	iXIndex = FindRsColumnIndex(strXAxisTitle);
	CString strYAxisT(strYAxisTitle);
	
	if (strYAxisT.CompareNoCase(RS_COL_WATT)==0)
	{
		iYIndex1 = FindRsColumnIndex(PS_WATT);
	}
	else if(strYAxisT.CompareNoCase("V_MaxDiff")==0)
	{
		iYIndex1 = FindRsColumnIndex(RS_COL_VOLTAGE1);
		iYIndex2 = FindRsColumnIndex(RS_COL_VOLTAGE2);
		iYIndex3 = FindRsColumnIndex(RS_COL_VOLTAGE3);
		iYIndex4 = FindRsColumnIndex(RS_COL_VOLTAGE4);
	}
	else if(strYAxisT.CompareNoCase("T_MaxDiff")==0)
	{
		iYIndex1 = FindRsColumnIndex(RS_COL_AUX_TEMP);
	}
	else if((strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_TEMP) == 0) && (strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) != 0))
	{
		//Aux Temperature는 아래에서 직접 처리
		iYIndex1 = 0;
	}
	else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_VOLT) == 0)
	{
		//Aux Voltage는 아래에서 직접 처리
		iYIndex1 = 0; 
	}
	else if(strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) == 0)
	{
		//Aux Voltage는 아래에서 직접 처리
		iYIndex1 = 0; 
	}
	else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_HUMI) == 0) //ksj 20200116
	{
		//Aux Voltage는 아래에서 직접 처리
		iYIndex1 = 0; 
	}

	else if(strYAxisT.Left(3).CompareNoCase(RS_COL_CAN) == 0)	//ljb 20131104 add
	{
		iYIndex1 = 0; 
	}
	else 
	{
		iYIndex1 = FindRsColumnIndex(strYAxisTitle);
	}

	//fltPoint* pfltPt = NULL;
	//Data가 저장되어 있지 않을 경우 
	if (iXIndex < 0 || iYIndex1 < 0 || iYIndex2 < 0 || iYIndex3 < 0 || iYIndex4 < 0)
	{
		//lDataNum = 0;
		return fltRet ;
	}

	//
	//lDataNum = m_lLoadedDataCount;
	//pfltPt   = new fltPoint[lDataNum];

	if (row < 0 || row >= m_lLoadedDataCount)
	{
		ASSERT(FALSE) ;
		return fltRet ;
	}

	//
	//for (int i = 0; i < m_lLoadedDataCount; i++)
	{
		//
		// X축값
		//
		fltRet.x = m_ppfltData[iXIndex][row];

		//
		// Y축값
		//
		if(CString(strYAxisTitle).CompareNoCase(RS_COL_WATT)==0)
		{
			//Bug Fixed 2006/5/8  
			//Watt / 1000.0f
			//kjh - 2007/09/20 Watt가 음수로 표시되던 것을 양수로 변환
			//fltRet.y = m_ppfltData[iYIndex1][row]*m_ppfltData[iYIndex2][row]/1000.0f;
			//fltRet.y = fabs(m_ppfltData[iYIndex1][row]*m_ppfltData[iYIndex2][row]/1000.0f);

			//fltRet.y = fabs(m_ppfltData[iYIndex1][row]);
			fltRet.y = m_ppfltData[iYIndex1][row];			//ljb 20130507 add HL 정보옥 요청 절대값을 하지 않는다
			//TRACE("watt = %.3f \n",fabs(m_ppfltData[iYIndex1][row]));
		}
		else if(strYAxisT.CompareNoCase("V_MaxDiff")==0)
		{
			BYTE byVPoint = LOBYTE(wPoint);
			if     (byVPoint==0x03) // 1번-2번
			{
				fltRet.y=(float)fabs(m_ppfltData[iYIndex1][row]-m_ppfltData[iYIndex2][row]);
			}
			else if(byVPoint==0x05) // 1번-3번
			{
				fltRet.y=(float)fabs(m_ppfltData[iYIndex1][row]-m_ppfltData[iYIndex3][row]);
			}
			else if(byVPoint==0x09) // 1번-4번
			{
				fltRet.y=(float)fabs(m_ppfltData[iYIndex1][row]-m_ppfltData[iYIndex4][row]);
			}
			else if(byVPoint==0x06) // 2번-3번
			{
				fltRet.y=(float)fabs(m_ppfltData[iYIndex2][row]-m_ppfltData[iYIndex3][row]);
			}
			else if(byVPoint==0x0A) // 2번-4번
			{
				fltRet.y=(float)fabs(m_ppfltData[iYIndex2][row]-m_ppfltData[iYIndex4][row]);
			}
			else if(byVPoint==0x0C) // 3번-4번
			{
				fltRet.y=(float)fabs(m_ppfltData[iYIndex3][row]-m_ppfltData[iYIndex4][row]);
			}
			else if(byVPoint==0x07) // 1번-2번-3번
			{
				fltRet.y=__max(__max(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex2][row]),m_ppfltData[iYIndex3][row])
					       -__min(__min(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex2][row]),m_ppfltData[iYIndex3][row]);
			}
			else if(byVPoint==0x0B) // 1번-2번-4번
			{
				fltRet.y=__max(__max(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex2][row]),m_ppfltData[iYIndex4][row])
					       -__min(__min(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex2][row]),m_ppfltData[iYIndex4][row]);
			}
			else if(byVPoint==0x0D) // 1번-3번-4번
			{
				fltRet.y=__max(__max(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex3][row]),m_ppfltData[iYIndex4][row])
					       -__min(__min(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex3][row]),m_ppfltData[iYIndex4][row]);
			}
			else if(byVPoint==0x0E) // 2번-3번-4번
			{
				fltRet.y=__max(__max(m_ppfltData[iYIndex2][row],m_ppfltData[iYIndex3][row]),m_ppfltData[iYIndex4][row])
					       -__min(__min(m_ppfltData[iYIndex2][row],m_ppfltData[iYIndex3][row]),m_ppfltData[iYIndex4][row]);
			}
			else if(byVPoint==0x0F) // 1번-2번-3번-4번
			{
				fltRet.y=__max(__max(__max(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex2][row]),m_ppfltData[iYIndex3][row]),m_ppfltData[iYIndex4][row])
					       -__min(__min(__min(m_ppfltData[iYIndex1][row],m_ppfltData[iYIndex2][row]),m_ppfltData[iYIndex3][row]),m_ppfltData[iYIndex4][row]);
			}
			else                    // 한개 선택 or 선택하지 않음
			{
				fltRet.y=0.0f;
			}
		}
		else if(strYAxisT.CompareNoCase("T_MaxDiff")==0)
		{
			BYTE byTPoint = HIBYTE(wPoint);
			if(byTPoint==0x03) // 1번-2번
			{
				fltRet.y=(float)fabs(m_ppfltData[iYIndex1][row]-m_ppfltData[iYIndex2][row]);
			}
			else               // 한개 선택 or 선택하지 않음
			{
				fltRet.y=0.0f;
			}
		}
		//else if(strYAxisT.CompareNoCase(RS_COL_TEMPERATURE1) == 0)
		else if((strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_TEMP) == 0) && (strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) != 0))
		{		
			int nIndex = GetAuxIndexToTitle(RS_COL_AUX_TEMP, wPoint);
			if(nIndex < 0)
			{
				//lDataNum = 0;
				//return pfltPt;
				return fltRet;
			}

			if (m_nAuxColumnCount < nIndex)
			{
				TRACE("Error Record Item Size(RecordItemSize : %d/ AuxColumnCount : %d)\n", GetRecordItemSize(), m_nAuxColumnCount);
				return fltRet ;
			}

			//fltRet.y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][row];
			fltRet.y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][row] / 1000;	//ljb 2011222 이재복 AUX 단위//////////
		}
		//else if(strYAxisT.CompareNoCase(RS_COL_AUX_VOLTAGE) == 0)
		else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_VOLT) == 0)
		{
			int nIndex = GetAuxIndexToTitle(RS_COL_AUX_VOLT, wPoint);
			if(nIndex < 0)
			{
				return fltRet ;
			}

			fltRet.y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][row];
		}
		else if(strYAxisT.Left(5).CompareNoCase(RS_COL_AUX_TEMPTH) == 0)
		{		
			int nIndex = GetAuxIndexToTitle(RS_COL_AUX_TEMPTH, wPoint);
			if(nIndex < 0)
			{
				//lDataNum = 0;
				//return pfltPt;
				return fltRet;
			}
			
			if (m_nAuxColumnCount < nIndex)
			{
				TRACE("Error Record Item Size(RecordItemSize : %d/ AuxColumnCount : %d)\n", GetRecordItemSize(), m_nAuxColumnCount);
				return fltRet ;
			}
			
			//fltRet.y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][row];
			fltRet.y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][row] / 1000;	//ljb 2011222 이재복 AUX 단위//////////
		}
		else if(strYAxisT.Left(4).CompareNoCase(RS_COL_AUX_HUMI) == 0) //ksj 20200116
		{		
			int nIndex = GetAuxIndexToTitle(RS_COL_AUX_HUMI, wPoint);
			if(nIndex < 0)
			{
				//lDataNum = 0;
				//return pfltPt;
				return fltRet;
			}

			if (m_nAuxColumnCount < nIndex)
			{
				TRACE("Error Record Item Size(RecordItemSize : %d/ AuxColumnCount : %d)\n", GetRecordItemSize(), m_nAuxColumnCount);
				return fltRet ;
			}

			//fltRet.y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][row];
			fltRet.y = m_ppfltData[GetRecordItemSize()- m_nAuxColumnCount+nIndex][row] / 1000;	//ljb 2011222 이재복 AUX 단위//////////
		}
		//////////////////////////////////////////////////////////////////////////
		//ljb 2011215 이재복 S //////////
		else if(strYAxisT.Left(3).CompareNoCase(RS_COL_CAN) == 0)
		{
			int nIndex = GetCanIndexToTitle(RS_COL_CAN, wPoint);
			if(nIndex < 0)
			{
				ASSERT(FALSE) ;
				//lDataNum = 0;
				return fltRet ;
			}

			if (m_nCanColumnCount < nIndex)
			{
				ASSERT(FALSE) ;
				TRACE("Error Record Item Size(RecordItemSize : %d/ CanColumnCount : %d)\n", GetRecordItemSize(), m_nCanColumnCount);
				return fltRet ;
			}

			//ljb 임시 찾기
			//fltRet.y = m_ppCanData[GetRecordItemSize()- m_nCanColumnCount+nIndex][row].fVal[0];
			fltRet.y = m_ppCanData[nIndex][row].fVal[0];
			
		}
		//////////////////////////////////////////////////////////////////////////
		else
		{
			fltRet.y = m_ppfltData[iYIndex1][row];
		}
	}
	//
//	return pfltPt;
	return fltRet;
}
//최종 결과의 Column의 Index를 구한다.
int CTable::FindIndexOfTitle(LPCTSTR title)
{
	// int i = 0  // 2020.01.03 dhkim for 문 내 외에서 동일 변수명 선언시 return에 바로 해당 변수를 사용할 경우 문제가 발생할 수 있음..
	CString stTemp;
	BOOL bCheck = FALSE;
	int retValue = -1;

	for(int i = 0; i<m_strlistTitle.GetCount(); i++)   // 20200103
	{
		stTemp.Format("%s",m_strlistTitle.GetAt(m_strlistTitle.FindIndex(i)));
		TRACE("%s \n",stTemp);
				
		if(m_strlistTitle.GetAt(m_strlistTitle.FindIndex(i)).CompareNoCase(title)==0)
		{
			//bCheck = TRUE;
			retValue = i;
			break;
		}
	}

	/*if(bCheck)
	{
		retValue = retValue;
	}*/
	
	return retValue;
}


//최종 결과의 Column의 Index를 구한다.
int CTable::FindIndexOfTitle(WORD wItem)
{
	//BOOL bCheck = FALSE;
	for(int i=0; i<m_awlistItem.GetSize(); i++)
	{
		if(m_awlistItem.GetAt(i) == wItem)
		{
			return i;
		}
	}
	return -1;
}

// 실처리 Data의 Column Index를 구한다.
long CTable::FindRsColumnIndex(WORD nItem)
{
	for(int i= 0; i<GetRecordItemSize(); i++)
	{
		if (nItem == m_DataItemList.GetAt(i))
		{
			return i;
		}
	}

	return -1;
	//return 0;
}


long CTable::FindRsColumnIndex(LPCTSTR szTitle)
{
	if(!CString(szTitle).CompareNoCase(RS_COL_VOLTAGE))
	{
		return FindRsColumnIndex(PS_VOLTAGE);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_CURRENT))
	{
		return FindRsColumnIndex(PS_CURRENT);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_CAPACITY))
	{
		return FindRsColumnIndex(PS_CAPACITY);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_TIME_DAY))	//ljb 20131206 add
	{
		return FindRsColumnIndex(PS_STEP_TIME_DAY);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_TIME))
	{
		return FindRsColumnIndex(PS_STEP_TIME);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_SEQ))
	{
		//int a = FindRsColumnIndex(PS_DATA_INDEX);
		return FindRsColumnIndex(PS_DATA_SEQ);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_WATT))
	{
		return FindRsColumnIndex(PS_WATT);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_WATT_HOUR))
	{
		return FindRsColumnIndex(PS_WATT_HOUR);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_AUX_TEMP))
	{
		return FindRsColumnIndex(PS_AUX_TEMPERATURE);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_AUX_VOLT))
	{
		return FindRsColumnIndex(PS_AUX_VOLTAGE);
	}

	//ljb 2011215 이재복 //////////
	if(!CString(szTitle).CompareNoCase(RS_COL_CAN))
	{
		return FindRsColumnIndex(PS_CAN_DATA);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CAN_S1))
	{
		return FindRsColumnIndex(PS_CAN_DATA);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CAN_S2))
	{
		return FindRsColumnIndex(PS_CAN_DATA);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CAN_S3))
	{
		return FindRsColumnIndex(PS_CAN_DATA);
	}

	//ljb 20100726 add start
	if(!CString(szTitle).CompareNoCase(RS_COL_OVEN_TEMP))
	{
		return FindRsColumnIndex(PS_OVEN_TEMPERATURE);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_OVEN_HUMI))
	{
		return FindRsColumnIndex(PS_OVEN_HUMIDITY);
	}
	//ljb 20100726 add end

	if(!CString(szTitle).CompareNoCase(RS_COL_AVG_VTG))
	{
		return FindRsColumnIndex(PS_AVG_VOLTAGE);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_AVG_CRT))
	{
		return FindRsColumnIndex(PS_AVG_CURRENT);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CV_TIME_DAY))		//ljb 20131206 add
	{
		return FindRsColumnIndex(PS_CV_TIME_DAY);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CV_TIME))
	{
		return FindRsColumnIndex(PS_CV_TIME);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CHARGE_AH))
	{
		return FindRsColumnIndex(PS_CHARGE_CAP);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_DISCHARGE_AH))
	{
		return FindRsColumnIndex(PS_DISCHARGE_CAP);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CAPACITANCE))
	{
		return FindRsColumnIndex(PS_CAPACITANCE);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CHARGE_WH))
	{
		return FindRsColumnIndex(PS_CHARGE_WH);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_DISCHARGE_WH))
	{
		return FindRsColumnIndex(PS_DISCHARGE_WH);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_SYNC_DATE))
	{
		return FindRsColumnIndex(PS_SYNC_DATE);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_SYNC_TIME))
	{
		return FindRsColumnIndex(PS_SYNC_TIME);
	}
	
	//ljb 201009 v100B
	if(!CString(szTitle).CompareNoCase(RS_COL_COMM_STATE))
	{
		return FindRsColumnIndex(PS_COMM_STATE);
	}
	//ljb 201012 v100B
	if(!CString(szTitle).CompareNoCase(RS_COL_OUTPUT_STATE))
	{
		return FindRsColumnIndex(PS_CAN_OUTPUT_STATE);
	}
	//ljb 201012 v100B
	if(!CString(szTitle).CompareNoCase(RS_COL_INPUT_STATE))
	{
		return FindRsColumnIndex(PS_CAN_INPUT_STATE);
	}
	//ljb 201012 v100B
	if(!CString(szTitle).CompareNoCase(RS_COL_VOLTAGE_INPUT))
	{
		return FindRsColumnIndex(PS_VOLTAGE_INPUT);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_VOLTAGE_POWER))
	{
		return FindRsColumnIndex(PS_VOLTAGE_POWER);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_VOLTAGE_BUS))
	{
		return FindRsColumnIndex(PS_VOLTAGE_BUS);
	}

	// Added by ljb 20150311 : TOT_TIME 추가
	if (!CString(szTitle).CompareNoCase(RS_COL_TOT_TIME))
	{
		return FindRsColumnIndex(PS_TOT_TIME);
	}

	// Added by 224 (2014/07/22) : TOT_DAY_TIME 추가
	if (!CString(szTitle).CompareNoCase(RS_COL_TOT_TIME_DAY))
	{
		return FindRsColumnIndex(PS_TOT_TIME_DAY);
	}
	//ljb 20150514 add start
	if(!CString(szTitle).CompareNoCase(RS_COL_VOLTAGE_LOAD))
	{
		return FindRsColumnIndex(PS_LOAD_VOLT);
	}
	
	if(!CString(szTitle).CompareNoCase(RS_COL_CURRENT_LOAD))
	{
		return FindRsColumnIndex(PS_LOAD_CURR);
	}
	//ljb 20150514 add end

	if(!CString(szTitle).CompareNoCase(RS_COL_CHILLER_REF_TEMP))
	{
		return FindRsColumnIndex(PS_CHILLER_REF_TEMP);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_CHILLER_CUR_TEMP))
	{
		return FindRsColumnIndex(PS_CHILLER_CUR_TEMP);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_CHILLER_REF_PUMP))
	{
		return FindRsColumnIndex(PS_CHILLER_REF_PUMP);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_CHILLER_CUR_PUMP))
	{
		return FindRsColumnIndex(PS_CHILLER_CUR_PUMP);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_CELLBAL_CH_DATA))
	{
		return FindRsColumnIndex(PS_CELLBAL_CH_DATA);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_PWRSUPPLY_SETVOLT)) //yulee 20190514 cny work transfering
	{
		return FindRsColumnIndex(PS_PWRSUPPLY_SETVOLT);
	}
	if(!CString(szTitle).CompareNoCase(RS_COL_PWRSUPPLY_CURVOLT))
	{
		return FindRsColumnIndex(PS_PWRSUPPLY_CURVOLT);
	}

	if(!CString(szTitle).CompareNoCase(RS_COL_CODE)) //ksj 20200601
	{
		return FindRsColumnIndex(PS_CODE);
	}

	//return 0;
	return -1;
}


void CTable::ReleaseRawDataMemory()
{
	//
	if(m_ppfltData!=NULL)
	{
//		for(int i=0; i<m_strlistTitle.GetCount(); i++)
//		{
//			if(m_ppfltData[i]!=NULL) delete [] m_ppfltData[i];
//		}
//		delete [] m_ppfltData;
		
		for(int i=0; i<GetRecordItemSize(); i++)
		{
			if(m_ppfltData[i]!=NULL)
			{
				delete [] m_ppfltData[i];
				m_ppfltData[i] = NULL;
			}
		}
		delete [] m_ppfltData;
		m_ppfltData = NULL;

	}
}

LONG CTable::GetRecordCount()
{
	return m_lNumOfData;
}

WORD CTable::GetItemID(CString strTitle)
{
//	if(str)
//	No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temp,Press,AvgVoltage,AvgCurrent,
	if(!strTitle.CompareNoCase(RS_COL_VOLTAGE))
	{
		return PS_VOLTAGE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CURRENT))
	{
		return PS_CURRENT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CAPACITY))
	{
		return PS_CAPACITY;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TIME_DAY))	//ljb 20131206 add
	{
		return PS_STEP_TIME_DAY;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TIME))
	{
		return PS_STEP_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_WATT))
	{
		return PS_WATT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_WATT_HOUR))
	{
		return PS_WATT_HOUR;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AUX_TEMP))
	{
		return PS_AUX_TEMPERATURE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AUX_VOLT))
	{
		return PS_AUX_VOLTAGE;
	}

	//ljb 20100726 start
	else if(!strTitle.CompareNoCase(RS_COL_OVEN_TEMP))
	{
		return PS_OVEN_TEMPERATURE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_OVEN_HUMI))
	{
		return PS_OVEN_HUMIDITY;
	}
	//ljb 20100726 end

	else if(!strTitle.CompareNoCase(RS_COL_SEQ))
	{
		return PS_DATA_SEQ;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AVG_VTG))
	{
		return PS_AVG_VOLTAGE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_AVG_CRT))
	{
		return PS_AVG_CURRENT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_STEP_NO))
	{
		return PS_STEP_NO;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CUR_CYCLE))
	{
		return PS_CUR_CYCLE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TOT_CYCLE))
	{
		return PS_TOT_CYCLE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TYPE))
	{
		return PS_STEP_TYPE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_STATE))
	{
		return PS_STATE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TOT_TIME))
	{
		return PS_TOT_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_TOT_TIME_DAY))	//ljb 20131206 add
	{
		return PS_TOT_TIME_DAY;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CODE))
	{
		return PS_CODE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_GRADE))
	{
		return PS_GRADE_CODE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_IR))
	{
		return PS_IMPEDANCE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CH_NO))
	{
		return PS_CHANNEL_NO;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CV_TIME_DAY))
	{
		return  PS_CV_TIME_DAY;
	}
	//added by KBH 20080813
	else if(!strTitle.CompareNoCase(RS_COL_CV_TIME))
	{
		return  PS_CV_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_ACC_CYCLE1))
	{
		return  PS_ACC_CYCLE1;
	}
	else if(!strTitle.CompareNoCase(RS_COL_ACC_CYCLE2))
	{
		return  PS_ACC_CYCLE2;
	}
	else if(!strTitle.CompareNoCase(RS_COL_ACC_CYCLE3))
	{
		return  PS_ACC_CYCLE3;
	}
	else if(!strTitle.CompareNoCase(RS_COL_ACC_CYCLE4))
	{
		return  PS_ACC_CYCLE4;
	}
	else if(!strTitle.CompareNoCase(RS_COL_ACC_CYCLE5))
	{
		return  PS_ACC_CYCLE5;
	}
	else if(!strTitle.CompareNoCase(RS_COL_MULTI_CYCLE1))
	{
		return  PS_MULTI_CYCLE1;
	}
	else if(!strTitle.CompareNoCase(RS_COL_MULTI_CYCLE2))
	{
		return  PS_MULTI_CYCLE2;
	}
	else if(!strTitle.CompareNoCase(RS_COL_MULTI_CYCLE3))
	{
		return  PS_MULTI_CYCLE3;
	}
	else if(!strTitle.CompareNoCase(RS_COL_MULTI_CYCLE4))
	{
		return  PS_MULTI_CYCLE4;
	}
	else if(!strTitle.CompareNoCase(RS_COL_MULTI_CYCLE5))
	{
		return  PS_MULTI_CYCLE5;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CHARGE_AH))
	{
		return  PS_CHARGE_CAP;
	}
	else if(!strTitle.CompareNoCase(RS_COL_DISCHARGE_AH))
	{
		return  PS_DISCHARGE_CAP;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CAPACITANCE))
	{
		return  PS_CAPACITANCE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CHARGE_WH))
	{
		return  PS_CHARGE_WH;
	}
	else if(!strTitle.CompareNoCase(RS_COL_DISCHARGE_WH))
	{
		return  PS_DISCHARGE_WH;
	}
	else if(!strTitle.CompareNoCase(RS_COL_SYNC_DATE))
	{
		return  PS_SYNC_DATE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_SYNC_TIME))
	{
		return  PS_SYNC_TIME;
	}
	else if(!strTitle.CompareNoCase(RS_COL_METER))
	{
		return  PS_METER_DATA;
	}

	//ljb 201009 v100B
	else if(!strTitle.CompareNoCase(RS_COL_COMM_STATE))
	{
		return PS_COMM_STATE;
	}
	//ljb 201012 v100B
	else if(!strTitle.CompareNoCase(RS_COL_OUTPUT_STATE))
	{
		return PS_CAN_OUTPUT_STATE;
	}
	//ljb 201012 v100B
	else if(!strTitle.CompareNoCase(RS_COL_INPUT_STATE))
	{
		return PS_CAN_INPUT_STATE;
	}
	else if(!strTitle.CompareNoCase(RS_COL_VOLTAGE_INPUT))
	{
		return PS_VOLTAGE_INPUT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_VOLTAGE_POWER))
	{
		return PS_VOLTAGE_POWER;
	}
	else if(!strTitle.CompareNoCase(RS_COL_VOLTAGE_BUS))
	{
		return PS_VOLTAGE_BUS;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CHILLER_REF_TEMP)) //yulee 20190710
	{
		return PS_CHILLER_REF_TEMP;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CHILLER_CUR_TEMP)) //yulee 20190710
	{
		return PS_CHILLER_CUR_TEMP;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CHILLER_REF_PUMP)) //yulee 20190710
	{
		return PS_CHILLER_REF_PUMP;
	}
	else if(!strTitle.CompareNoCase(RS_COL_CHILLER_CUR_PUMP)) //yulee 20190710
	{
		return PS_CHILLER_CUR_PUMP;
	}
	else if(!strTitle.CompareNoCase(RS_COL_PWRSUPPLY_SETVOLT)) //yulee 20190710
	{
		return PS_PWRSUPPLY_SETVOLT;
	}
	else if(!strTitle.CompareNoCase(RS_COL_PWRSUPPLY_CURVOLT)) //yulee 20190710
	{
		return PS_PWRSUPPLY_CURVOLT;
	}

	return 0xffff;
}

CString CTable::GetItemName(WORD wItem)
{
	switch (wItem)
	{
	case PS_VOLTAGE:		return RS_COL_VOLTAGE;
	case PS_CURRENT:		return RS_COL_CURRENT;
	case PS_CAPACITY:		return RS_COL_CAPACITY;
	case PS_STEP_TIME_DAY:	return RS_COL_TIME_DAY;			//ljb 20131206 add
	case PS_STEP_TIME:		return RS_COL_TIME;
	case PS_WATT:			return RS_COL_WATT;
	case PS_WATT_HOUR:		return RS_COL_WATT_HOUR;

	case PS_AUX_TEMPERATURE:	return RS_COL_AUX_TEMP;		//ljb 20100726
	case PS_AUX_VOLTAGE:		return RS_COL_AUX_VOLT;		//ljb 20100726
	case PS_AUX_TEMPERATURE_TH: return RS_COL_AUX_TEMPTH; //20180612 yulee
	case PS_AUX_HUMIDITY:		return RS_COL_AUX_HUMI; //ksj 20200116 : v1016 습도 추가.
	case PS_OVEN_TEMPERATURE:	return RS_COL_OVEN_TEMP;		//ljb 20100726
	case PS_OVEN_HUMIDITY:		return RS_COL_OVEN_HUMI;		//ljb 20100726
	
	case PS_COMM_STATE:			return RS_COL_COMM_STATE;		//ljb 201009
	case PS_CAN_OUTPUT_STATE:	return RS_COL_OUTPUT_STATE;		//ljb 201012
	case PS_CAN_INPUT_STATE:	return RS_COL_INPUT_STATE;		//ljb 201012
	
	case PS_VOLTAGE_INPUT:	return RS_COL_VOLTAGE_INPUT;		//ljb 20101201
	case PS_VOLTAGE_POWER:	return RS_COL_VOLTAGE_POWER;		//ljb 20101201
	case PS_VOLTAGE_BUS:	return RS_COL_VOLTAGE_BUS;			//ljb 20101201

	case PS_DATA_SEQ:		return RS_COL_SEQ;
	case PS_AVG_VOLTAGE:	return RS_COL_AVG_VTG;
	case PS_AVG_CURRENT:	return RS_COL_AVG_CRT;
	case PS_STEP_NO:		return RS_COL_STEP_NO;

	case PS_CUR_CYCLE:		return RS_COL_CUR_CYCLE;
	case PS_TOT_CYCLE:		return RS_COL_TOT_CYCLE;
	case PS_STEP_TYPE:		return RS_COL_TYPE;
	case PS_STATE:			return RS_COL_STATE;
	case PS_TOT_TIME_DAY:	return RS_COL_TOT_TIME_DAY;		//ljb 20131206 add
	case PS_TOT_TIME:		return RS_COL_TOT_TIME;
	case PS_CODE:			return RS_COL_CODE;
	case PS_GRADE_CODE:		return RS_COL_GRADE;
	case PS_IMPEDANCE:		return RS_COL_IR;

	case PS_CHANNEL_NO:		return RS_COL_CH_NO;

	//added by KBH 20080813
	case PS_CV_TIME_DAY:	return RS_COL_CV_TIME_DAY;		//ljb 20131206
	case PS_CV_TIME:		return RS_COL_CV_TIME;
	case PS_ACC_CYCLE1:		return RS_COL_ACC_CYCLE1;		//ljb v1009
	case PS_ACC_CYCLE2:		return RS_COL_ACC_CYCLE2;
	case PS_ACC_CYCLE3:		return RS_COL_ACC_CYCLE3;
	case PS_ACC_CYCLE4:		return RS_COL_ACC_CYCLE4;
	case PS_ACC_CYCLE5:		return RS_COL_ACC_CYCLE5;
	case PS_MULTI_CYCLE1:		return RS_COL_MULTI_CYCLE1;
	case PS_MULTI_CYCLE2:		return RS_COL_MULTI_CYCLE2;
	case PS_MULTI_CYCLE3:		return RS_COL_MULTI_CYCLE3;
	case PS_MULTI_CYCLE4:		return RS_COL_MULTI_CYCLE4;
	case PS_MULTI_CYCLE5:		return RS_COL_MULTI_CYCLE5;
	case PS_CHARGE_CAP:		return RS_COL_CHARGE_AH;
	case PS_DISCHARGE_CAP:	return RS_COL_DISCHARGE_AH;
	case PS_CAPACITANCE:	return RS_COL_CAPACITANCE;
	case PS_CHARGE_WH:		return RS_COL_CHARGE_WH;
	case PS_DISCHARGE_WH:	return RS_COL_DISCHARGE_WH;
	case PS_SYNC_DATE:		return RS_COL_SYNC_DATE;
	case PS_SYNC_TIME:		return RS_COL_SYNC_TIME;
	case PS_METER_DATA:		return RS_COL_METER;
	case PS_LOAD_VOLT:		return RS_COL_VOLTAGE_LOAD;		//ljb 20150513
	case PS_LOAD_CURR:		return RS_COL_CURRENT_LOAD;		//ljb 20150513
	case PS_CHILLER_REF_TEMP:	return RS_COL_CHILLER_REF_TEMP;	
	case PS_CHILLER_CUR_TEMP:	return RS_COL_CHILLER_CUR_TEMP;	
	case PS_CHILLER_REF_PUMP:	return RS_COL_CHILLER_REF_PUMP;	
	case PS_CHILLER_CUR_PUMP:	return RS_COL_CHILLER_CUR_PUMP;	
	case PS_CELLBAL_CH_DATA:	return RS_COL_CELLBAL_CH_DATA;	
	case PS_PWRSUPPLY_SETVOLT:	return RS_COL_PWRSUPPLY_SETVOLT;//yulee 20190514 cny work transfering	
	case PS_PWRSUPPLY_CURVOLT:	return RS_COL_PWRSUPPLY_CURVOLT;	

	}
	return "Unknown";
}

int CTable::LoadRawData(CString strChPath)
{
	//----------------------------------------------------------------
	// Added by 224 (2013/12/29) : DataView와 소스 호환을 위해 변수명 조정
	// 1. 레코드의 종료 행 인덱스
	int nFrom = m_lRecordIndex ;

	// 2. 레코드의 시작 행 인덱스
	int nTo   = m_lRecordIndex + m_lNumOfData - 1 ;
	ASSERT(nTo >= nFrom) ;

	CString strTemp ;
	//----------------------------------------------------------------

	// 3. 파일관련 변수
	CFileFind afinder;
	FILE* fp;
	
	// 4. RAW 데이터 처리. PS_RAW_FILE_NAME_EXT	"cyc"
	// 4.1. 헤더초기화
	PS_RAW_FILE_HEADER RawHeader ;
	ZeroMemory(&RawHeader, sizeof(RawHeader)) ;
	
	// 4.2. cyc 파일은 여러개 있는 것으로 간주함
	CStringArray arRawFiles ;
	if (afinder.FindFile(strChPath + "\\*." + PS_RAW_FILE_NAME_EXT + "*"))
	{
		int nLoop = 0 ;
		
		BOOL bContinue = TRUE ;
		while (bContinue)
		{
			nLoop++ ;
			bContinue = afinder.FindNextFile() ;
			arRawFiles.Add(afinder.GetFilePath()) ;
		} ;
	}

	afinder.Close() ;

	// 4.3. 파일속성, 레코드크기 및 버퍼 초기화
	// 	int nRawFileSize   = 0 ;
	// 	int nRawRecordSize = 0 ;
	long long llRawFileSize   = 0 ; //lyj 20210729 int -> long long
	long long llRawRecordSize = 0 ; //lyj 20210729 int -> long long

	// 4.4. String array 에 값이 있으면(파일들이 존재하면) 진행한다.
	int nColRawCount = 0 ;
	if (arRawFiles.GetSize() > 0)
	{
		// 4.4.1. RAW 파일의 헤더를 얻는다.
		strTemp = arRawFiles.GetAt(0) ;
		fp = fopen(strTemp, "rb");		
		if (fp)
		{
			// .1. 파일의 길이 계산
			//nRawFileSize = filelength(fileno(fp));
			llRawFileSize = _filelengthi64(fileno(fp)); //lyj 20210729

			//ljb 20140115 add start
			if (llRawFileSize <= sizeof(PS_RAW_FILE_HEADER))
			{
				llRawRecordSize = 0;
				fclose( fp );
				return FALSE;
			}
			//ljb 20140115 add end

			//ASSERT(nRawFileSize > sizeof(PS_RAW_FILE_HEADER)) ;
			ASSERT(llRawFileSize > sizeof(PS_RAW_FILE_HEADER)) ;
			
			// .2. header 는 따로 파일에서 읽어와서 저장한다.
			fread(&RawHeader, sizeof(RawHeader), 1, fp );
			
			// .3. 레코드의 갯수 계산
			llRawRecordSize = sizeof(float) * RawHeader.rsHeader.nColumnCount;
			fclose( fp );
		}

		fp = NULL ;

		// 4.4.2. 컬럼수를 멤버변수에 설정한다.
		nColRawCount = RawHeader.rsHeader.nColumnCount;
		for (int i = 0; i < nColRawCount; i++)
		{
			m_DataItemList.Add(RawHeader.rsHeader.awColumnItem[i]);
		}

		// 4.4.3. 컬럼별로 데이터의 값을 수집한다.
		// 2차원 동적 배열 
		LONG lRecItemSize = GetRecordItemSize() ;
		m_ppfTempChData = new float* [lRecItemSize];
		for (int i = 0; i < lRecItemSize; i++)
		{
			m_ppfTempChData[i] = new float[m_lNumOfData];
			ZeroMemory(m_ppfTempChData[i], sizeof(float) * m_lNumOfData);
		}
		
		// 4.4.4. 결과 버퍼의 크기를 정한다.
		int num             = nTo - nFrom + 1;		
		int nStartingRecord = 0 ;		// 파일별 시작 레코드의 번호
		int nBegin          = nFrom ;	// 파일별 중첩시 가변적이다.
		int nIdx            = 0 ;		// 버퍼인덱스
		for (int nLoop = 0; nLoop < arRawFiles.GetSize(); nLoop++)
		{
			// .1. 파일오픈.
			strTemp = arRawFiles.GetAt(nLoop) ;
			fp = fopen(strTemp, "rb");
			
			// .2. 파일이 보유한 레코드 갯수를 읽는다.
			//nRawFileSize = filelength(fileno(fp));		
			llRawFileSize = _filelengthi64(fileno(fp));		//lyj 20210729
			//int nFileNumOfRecord = (nRawFileSize - sizeof(PS_RAW_FILE_HEADER)) / nRawRecordSize ;
			int nFileNumOfRecord = (llRawFileSize - sizeof(PS_RAW_FILE_HEADER)) / llRawRecordSize ;
			
			// .3. 현재 파일에 검색하려는 행이 포함됐는지 확인
			if (nBegin < (nStartingRecord + nFileNumOfRecord))
			{
				//int nOffset = sizeof(PS_RAW_FILE_HEADER) + (nBegin-nStartingRecord) * nRawRecordSize ;
				long long nOffset = sizeof(PS_RAW_FILE_HEADER) + (nBegin-nStartingRecord) * llRawRecordSize; //lyj 20210729
				_fseeki64(fp, nOffset, SEEK_SET) ; //lyj 20210729

				//-----------------------------------------------------
				float* fpBuff = new float[lRecItemSize];
				//-----------------------------------------------------[

				// 이 파일에 레코드가 모두 있는지 여부
				if (nTo < (nStartingRecord + nFileNumOfRecord))
				{
					int num = nTo - nBegin + 1 ;
					ASSERT(num <= m_lNumOfData) ;
					
					for (int nLoop = 0; nLoop < num && !feof(fp); nLoop++)
					{
						VERIFY(fread(fpBuff, llRawRecordSize, 1, fp) > 0) ;

						for (int i = 0; i < lRecItemSize; i++)
						{
							if (i == 22)
							{
								strTemp.Format("%.05f",fpBuff[i]);
								fpBuff[i] = atof(strTemp);
							}							
							//TRACE("%.0f\n",fpBuff[i]);							

							m_ppfTempChData[i][nIdx+nLoop] = fpBuff[i];							
						}
					}
					
					delete [] fpBuff;

					fclose(fp) ;
					break ;
				}
				else
				{
					// 다음 파일에 레코드가 중첩된 경우
					// 파일의 마지막 까지는 배열에 쓴다.
					int num = nStartingRecord + nFileNumOfRecord - nBegin ;
					for (int nLoop = 0; nLoop < num && !feof(fp); nLoop++)
					{
						VERIFY(fread(fpBuff, llRawRecordSize, 1, fp) > 0) ;
						
						for (int i = 0; i < lRecItemSize; i++)
						{
							if (i == 22)
							{
								strTemp.Format("%.05f",fpBuff[i]);
								fpBuff[i] = atof(strTemp);
							}
							
							//m_ppfTempChData[i][rsCount] = fpBuff[i];
							m_ppfTempChData[i][nIdx+nLoop] = fpBuff[i];
						}
						
						//rsCount++;
					}

					// nIdx 의 위치를 변경시킨다.
					nIdx += num ;
					
					// nBegin 의 위치를 변경한다.
					nBegin = nStartingRecord + nFileNumOfRecord;

					delete [] fpBuff;
				}
				
			}
			
			nStartingRecord += nFileNumOfRecord ;
			fclose(fp) ;
		}
		
		afinder.Close();
	}

	return nColRawCount ;
}

int CTable::LoadAuxData(CString strChPath)
{
	//2014.11.10 Aux 타이틀을 불러온다.
	LoadAuxTitle(strChPath);

	//----------------------------------------------------------------
	// Added by 224 (2013/12/29) : DataView와 소스 호환을 위해 변수명 조정
	// 1. 레코드의 종료 행 인덱스
	int nFrom = m_lRecordIndex ;
	
	// 2. 레코드의 시작 행 인덱스
	int nTo   = m_lRecordIndex + m_lNumOfData - 1 ;
	ASSERT(nTo >= nFrom) ;
	
	CString strTemp ;
	//----------------------------------------------------------------
	
	// 3. 파일관련 변수
	CFileFind afinder;
	FILE* fp;
	
	// 4. AUX 데이터 처리. PS_AUX_FILE_NAME_EXT	"aux"
	// 4.1. 헤더초기화
	PS_AUX_FILE_HEADER AuxHeader ;
	ZeroMemory(&AuxHeader, sizeof(AuxHeader)) ;

	// 4.2. cyc 파일은 여러개 있는 것으로 간주함
	CStringArray arAuxFiles ;
	if (afinder.FindFile(strChPath + "\\*." + PS_AUX_FILE_NAME_EXT + "*"))
	{
		int nLoop = 0 ;
		
		BOOL bContinue = TRUE ;
		while (bContinue)
		{
			nLoop++ ;
			bContinue = afinder.FindNextFile() ;
			arAuxFiles.Add(afinder.GetFilePath()) ;
		} ;
	}
	
	afinder.Close() ;

	// 4.3. 파일속성, 레코드크기 및 버퍼 초기화
	// 	int nAuxFileSize   = 0 ;
	// 	int nAuxRecordSize = 0 ;
	long long llAuxFileSize   = 0 ; //lyj 20210729 int -> long long
	long long llAuxRecordSize = 0 ; //lyj 20210729 int -> long long

	// 4.4. String array 에 값이 있으면(파일들이 존재하면) 진행한다.
	int nColAuxCount = 0 ;
	if (arAuxFiles.GetSize() > 0)
	{
		// 4.4.1. AUX 파일의 헤더를 얻는다.
		strTemp = arAuxFiles.GetAt(0) ;
		fp = fopen(strTemp, "rb");		
		if (fp)
		{
			// .1. 파일의 길이 계산
			//nAuxFileSize = filelength(fileno(fp));		
			llAuxFileSize = _filelengthi64(fileno(fp));	//lyj 20210729
			ASSERT(llAuxFileSize >= sizeof(PS_AUX_FILE_HEADER)) ;

			// .2. header 는 따로 파일에서 읽어와서 저장한다.
			fread(&AuxHeader, sizeof(AuxHeader), 1, fp );
			
			// .3. 레코드의 갯수 계산
			llAuxRecordSize = sizeof(float) * AuxHeader.auxHeader.nColumnCount;
			fclose( fp );
		}

		fp = NULL ;

		// 4.4.2. 컬럼수를 멤버변수에 설정한다.
		nColAuxCount = AuxHeader.auxHeader.nColumnCount;
		for (int i = 0; i < nColAuxCount; i++)
		{
			m_DataItemList.Add(AuxHeader.auxHeader.awColumnItem[i]);
		}

		// 4.4.3. 컬럼별로 데이터의 값을 수집한다.
		// 2차원 동적 배열 
		// 
		m_ppfTempAuxData = new float* [nColAuxCount];
		
		for (int i = 0; i < nColAuxCount; i++) 
		{
			m_ppfTempAuxData[i] = new float[m_lNumOfData];
			ZeroMemory(m_ppfTempAuxData[i], sizeof(float) * m_lNumOfData);
		}
				
		// 4.4.4. 결과 버퍼의 크기를 정한다.
		int num             = nTo - nFrom + 1;		
		int nStartingRecord = 0 ;		// 파일별 시작 레코드의 번호
		int nBegin          = nFrom ;	// 파일별 중첩시 가변적이다.
		int nIdx            = 0 ;		// 버퍼인덱스
		for (int nLoop = 0; nLoop < arAuxFiles.GetSize(); nLoop++)
		{
			if (llAuxRecordSize == 0)
			{
				break ;
			}

			// .1. 파일오픈.
			strTemp = arAuxFiles.GetAt(nLoop) ;
			fp = fopen(strTemp, "rb");
			
			// .2. 파일이 보유한 레코드 갯수를 읽는다.
			//nAuxFileSize = filelength(fileno(fp));		
			llAuxFileSize = _filelengthi64(fileno(fp));	//lyj 20210729
			int nFileNumOfRecord = (llAuxFileSize - sizeof(PS_AUX_FILE_HEADER)) / llAuxRecordSize ;

			// .3. 현재 파일에 검색하려는 행이 포함됐는지 확인
			if (nBegin < (nStartingRecord + nFileNumOfRecord))
			{
				//int nOffset = sizeof(PS_AUX_FILE_HEADER) + (nBegin-nStartingRecord) * llAuxRecordSize ;
				//fseek(fp, nOffset, SEEK_SET) ;
				long long  nOffset = sizeof(PS_AUX_FILE_HEADER) + (nBegin-nStartingRecord) * llAuxRecordSize ; //lyj 20210729
				_fseeki64(fp, nOffset, SEEK_SET) ; //lyj 20210729

//-----------------------------------------------------
				float* fpBuff = new float[nColAuxCount];
				ZeroMemory(fpBuff, sizeof(float)*nColAuxCount);
//-----------------------------------------------------[

				// 이 파일에 레코드가 모두 있는지 여부
				if (nTo < (nStartingRecord + nFileNumOfRecord))
				{
					int num = nTo - nBegin + 1 ;
					ASSERT(num <= m_lNumOfData) ;
					
					for (int nLoop = 0; nLoop < num && !feof(fp); nLoop++)
					{
						VERIFY(fread(fpBuff, llAuxRecordSize, 1, fp) > 0) ;

						for (int i = 0; i < nColAuxCount; i++)
						{
							PS_AUX_DISP_INFO* pAux = GetArrayAuxInfo(i) ;
//							if (AuxHeader.fileHeader.nFileVersion < _PNE_AUX_FILE_VER && m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_VOLTAGE)
							if (AuxHeader.fileHeader.nFileVersion < _PNE_AUX_FILE_VER && pAux->auxType == PS_AUX_TYPE_VOLTAGE)
							{
								m_ppfTempAuxData[i][nIdx+nLoop] = fpBuff[i] / 1000;	
							}
							else
							{
								m_ppfTempAuxData[i][nIdx+nLoop] = fpBuff[i];
							}
						}
					}
					
					delete [] fpBuff;

					fclose(fp) ;
					break ;
				}
				else
				{
					// 다음 파일에 레코드가 중첩된 경우
					// 파일의 마지막 까지는 배열에 쓴다.
					int num = nStartingRecord + nFileNumOfRecord - nBegin ;
					for (int nLoop = 0; nLoop < num && !feof(fp); nLoop++)
					{
						VERIFY(fread(fpBuff, llAuxRecordSize, 1, fp) > 0) ;
						
						for (int i = 0; i < nColAuxCount; i++)
						{
							PS_AUX_DISP_INFO* pAux = GetArrayAuxInfo(i) ;
//							if (AuxHeader.fileHeader.nFileVersion < _PNE_AUX_FILE_VER && m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_VOLTAGE)
							if (AuxHeader.fileHeader.nFileVersion < _PNE_AUX_FILE_VER && pAux->auxType == PS_AUX_TYPE_VOLTAGE)
							{
								m_ppfTempAuxData[i][nIdx+nLoop] = fpBuff[i] / 1000;	
							}
							else
							{
								m_ppfTempAuxData[i][nIdx+nLoop] = fpBuff[i];
							}
						}
						
						//						rsCount++;
					}

					// nIdx 의 위치를 변경시킨다.
					nIdx += num ;
					
					// nBegin 의 위치를 변경한다.
					nBegin = nStartingRecord + nFileNumOfRecord;

					delete [] fpBuff;
				}
				
			}
			
			nStartingRecord += nFileNumOfRecord ;
			fclose(fp) ;
		}
		
		afinder.Close();
	}

//--------------------------------------------------------------------


	if (GetRecordItemSize() >= nColAuxCount)
	{
		m_nAuxColumnCount = nColAuxCount;
	}
	else
	{
		m_nAuxColumnCount = 0;
	}

	return m_nAuxColumnCount;
	
}

int CTable::LoadAuxData_ORIGIN(LPCTSTR strAuxPath)
{
	LoadAuxTitle(strAuxPath);
	CString strPath;
	strPath.Format("%s\\*.%s", strAuxPath, PS_AUX_FILE_NAME_EXT);	//각 Step의 Data가 저장되어 있는 파일 

	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return 0;
	
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();

	//Data를 읽어들임
	FILE *fp = fopen(strPath, "rb");
	if(fp == NULL)		return 0;

	//File Header를 읽음
	PS_FILE_ID_HEADER fileID;
	if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
	{
		fclose(fp);
		return 0;
	}
	if(fileID.nFileID != PS_PNE_AUX_FILE_ID)
	{
		fclose(fp);
		return 0;
	}
	

	PS_AUX_DATA_FILE_HEADER * pAuxHeader = new PS_AUX_DATA_FILE_HEADER;
	ZeroMemory(pAuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER));

	//File Header를 읽음
	if(fread(pAuxHeader, sizeof(PS_AUX_DATA_FILE_HEADER), 1, fp) < 1)	
	{
		delete pAuxHeader;
		fclose(fp);
		return 0;
	}

	//저장된 Column수를 구함 
	if(pAuxHeader->nColumnCount < 1)		
	{
		delete pAuxHeader;
		fclose(fp);
		return 0;
	}	

	int nColAuxCount = pAuxHeader->nColumnCount;
	for(int i=0; i < nColAuxCount; i++)
	{
		m_DataItemList.Add(pAuxHeader->awColumnItem[i]);
	}

	delete pAuxHeader;

	int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_AUX_DATA_FILE_HEADER);
	//저장된 Data의 크기 
	size_t nRecordSize = sizeof(float)*nColAuxCount;

	//주어진 Index로 이동한다.
	fseek(fp, m_lRecordIndex*nRecordSize+nHeaderSize,SEEK_SET);
	
	ASSERT(m_ppfTempAuxData == NULL);
	//2차원 동적 배열 
	m_ppfTempAuxData = new float*[nColAuxCount];
	
	for(int i=0;i<nColAuxCount;i++) 
	{
		m_ppfTempAuxData[i] = new float[m_lNumOfData];
		ZeroMemory(m_ppfTempAuxData[i], sizeof(float)*m_lNumOfData);
	}

//	ZeroMemory(m_ppfTempAuxData, sizeof(float)*nColAuxCount*m_lNumOfData);
	float *fpBuff = new float[nColAuxCount];
	ZeroMemory(fpBuff, sizeof(float)*nColAuxCount);
	int rsCount = 0;
	while(fread(fpBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lNumOfData)
	{	
		for(int i=0; i<nColAuxCount; i++)
		{

// 			if(fileID.nFileVersion < _PNE_AUX_FILE_VER && m_AuxInfo[i][1] == PS_AUX_TYPE_VOLTAGE)
			PS_AUX_DISP_INFO* pAux = GetArrayAuxInfo(i) ;
//			if(fileID.nFileVersion < _PNE_AUX_FILE_VER && m_ArrayAuxInfo[i].auxType == PS_AUX_TYPE_VOLTAGE)
			//if(fileID.nFileVersion < _PNE_AUX_FILE_VER && pAux->auxType == PS_AUX_TYPE_VOLTAGE)
			if (fileID.nFileVersion < _PNE_AUX_FILE_VER && ((pAux->auxType == PS_AUX_TYPE_VOLTAGE)||(pAux->auxType == PS_AUX_TYPE_TEMPERATURE_TH))) //180607 yulee
			{
				m_ppfTempAuxData[i][rsCount] = fpBuff[i] / 1000;	
			}
			else
				m_ppfTempAuxData[i][rsCount] = fpBuff[i];
		}
		rsCount++;
	}
	delete [] fpBuff;
	fclose(fp);

	if(GetRecordItemSize() >= nColAuxCount)
		m_nAuxColumnCount = nColAuxCount;
	else
		m_nAuxColumnCount = 0;

	return m_nAuxColumnCount;
	
}

int CTable::GetAuxColumnCount()
{
	return this->m_nAuxColumnCount;
}

int CTable::GetCanColumnCount()
{
	return this->m_nCanColumnCount;
}

CString CTable::GetAuxTitleOfIndex(int nAuxIndex)
{
	if(nAuxIndex < 0 || nAuxIndex > this->GetAuxColumnCount()) return "";
	CString strTitle;

	PS_AUX_DISP_INFO* pAux = GetArrayAuxInfo(nAuxIndex) ;

	switch(pAux->auxType)
	{
		case PS_AUX_TYPE_TEMPERATURE:	strTitle.Format("[%003d]AuxT(%s)", pAux->chNo, pAux->auxName ); break;
		case PS_AUX_TYPE_VOLTAGE:		strTitle.Format("[%003d]AuxV(%s)", pAux->chNo, pAux->auxName); break;
		case PS_AUX_TYPE_TEMPERATURE_TH:strTitle.Format("[%003d]AuxTH(%s)", pAux->chNo, pAux->auxName); break; //20180619 yulee sort를 위해 변경 
		case PS_AUX_TYPE_HUMIDITY:	strTitle.Format("[%003d]AuxH(%s)", pAux->chNo, pAux->auxName); break; //ksj 20200116 : v1016 습도 추가.
	/*	case PS_AUX_TYPE_TEMPERATURE:	strTitle.Format("AuxT(%s)[%003d]", pAux->auxName, pAux->chNo); break;
		case PS_AUX_TYPE_VOLTAGE:		strTitle.Format("AuxV(%s)[%003d]", pAux->auxName, pAux->chNo); break;
		case PS_AUX_TYPE_TEMPERATURE_TH:strTitle.Format("AuxTH(%s)[%003d]", pAux->auxName, pAux->chNo); break; //20180619 yulee
	*/}
	return strTitle;
}

CString CTable::GetCanTitleOfIndex(int nCanIndex)
{
	if(nCanIndex < 0 || nCanIndex > this->GetCanColumnCount()) return "";

	CString strTitle;	
	PS_CAN_DISP_INFO* pCan = GetArrayCanInfo(nCanIndex) ;
//	strTitle.Format("CAN[%03d]%s", m_ArrayCanInfo[nCanIndex].chNo ,m_ArrayCanInfo[nCanIndex].canName);
//	strTitle.Format("CAN[%03d]%s", pCan->chNo, pCan->canName);
	strTitle.Format("[%03d]CAN%s", pCan->chNo+1, pCan->canName); //yulee 20180831-2
	return strTitle;
}

void CTable::LoadAuxTitle(CString strAuxConfigPath)
{
	CString strPath;

	char path[MAX_PATH];
    GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	CString strTemp(path);
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strAuxConfigPath.Mid(strAuxConfigPath.ReverseFind('\\')+1, 7), PS_AUX_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("*AUX.%s", "tlt"); 
	CString strNew = strAuxConfigPath+"\\"+strTemp;
		
   	CFileFind afinder;
	int nIndex = 0;

	if(afinder.FindFile(strNew) ==  TRUE)
	{
		afinder.FindNextFile();
		strPath = afinder.GetFilePath();

		//Data를 읽어들임
		FILE *fp = fopen(strPath, "rb");
		if(fp == NULL)		return;

// 		char buff[10000] = {0,};
// 		fread(&buff, 10000, 1, fp);

		char buff[20000] = {0,};	//ljb 20180921 10000 -> 20000 Aux 할당 가능 갯수 변경에 따른 증가 //DAQ512 수정 포인트
		fread(&buff, 20000, 1, fp);	//ljb 20180921 10000 -> 20000 Aux 할당 가능 갯수 변경에 따른 증가 //DAQ512 수정 포인트

		CString strBuff;
		strBuff.Format("%s", buff);

		int nFind = strBuff.Find('\n');
		int nPos = 0;
		//ljb 2010-06-22 Start-----------------------------------
		while(nFind > 0)
		{
			strTemp = strBuff.Mid(nPos,nFind - nPos -1);
			
			char Buf[50];
			char seps[] = ",";
			char * token;
			sprintf(Buf, strTemp);
			
			
			//ljb AUX정보의 이름을 추가하도록 변경
			token = strtok(Buf, seps);				// 1.chNo
			
			//m_ArrayAuxInfo[nIndex].chNo = atoi(token);
			PS_AUX_DISP_INFO* pAux = GetArrayAuxInfo(nIndex) ;
//			m_ArrayAuxInfo[nIndex].chNo = nIndex+1;	//ljb 20131015 add	Aux 데이터 순서를 무조건 1번 부터로 변경
			pAux->chNo = nIndex+1;	//ljb 20131015 add	Aux 데이터 순서를 무조건 1번 부터로 변경
			
			token = strtok(NULL, seps);				// 2.auxType
//			m_ArrayAuxInfo[nIndex].auxType = atoi(token);
			pAux->auxType = atoi(token);
			
			token = strtok(NULL, seps);				// 4.auxName
			if(token != NULL)
			{
//				sprintf(m_ArrayAuxInfo[nIndex].auxName, "%s", token);
				sprintf(pAux->auxName, "%s", token);
			}
			else
			{
//				if(m_ArrayAuxInfo[nIndex].auxType == PS_AUX_TYPE_TEMPERATURE)
				if(pAux->auxType == PS_AUX_TYPE_TEMPERATURE)
				{
//					sprintf(m_ArrayAuxInfo[nIndex].auxName, "AuxT_%d", nIndex+1);
					sprintf(pAux->auxName, "AuxT_%d", nIndex+1);
				}
				//else //20180607 yulee 주석처리
				else if(pAux->auxType == PS_AUX_TYPE_VOLTAGE)
				{
//					sprintf(m_ArrayAuxInfo[nIndex].auxName, "AuxV_%d", nIndex+1);
					sprintf(pAux->auxName, "AuxV_%d", nIndex+1);
				}
				else if(pAux->auxType == PS_AUX_TYPE_TEMPERATURE_TH) //20180607 yulee
				{
					sprintf(pAux->auxName, "AuxTH_%d", nIndex+1);
				}
				else if(pAux->auxType == PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : v1016 습도 추가.
				{
					sprintf(pAux->auxName, "AuxH_%d", nIndex+1);
				}
				else 
				{
//					sprintf(m_ArrayAuxInfo[nIndex].auxName, "AuxV_%d", nIndex+1);
					sprintf(pAux->auxName, "AuxV_%d", nIndex+1);
				}
			}
			
			nIndex++;
			
			token = strtok(NULL, seps);
			nPos = nFind+1;
			nFind = strBuff.Find('\n', nFind+1);			
		}	
		//ljb 2010-06-22 End -----------------------------------
	
		fclose(fp);
	}

	if(GetRecordItemSize() >= nIndex)
		m_nAuxColumnCount = nIndex;
	else
		m_nAuxColumnCount = 0;
}

void CTable::LoadCanTitle(CString strCanConfigPath)
{
	CString strPath;

	char path[MAX_PATH];
    GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	CString strTemp(path);
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strCanConfigPath.Mid(strCanConfigPath.ReverseFind('\\')+1, 7), PS_CAN_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("*CAN.%s", "tlt"); 
	CString strNew = strCanConfigPath+"\\"+strTemp;
	
		
   	CFileFind afinder;
	int nIndex = 0;

	if(afinder.FindFile(strNew) ==  TRUE)
	{
		afinder.FindNextFile();
		strPath = afinder.GetFilePath();

		//Data를 읽어들임
		FILE *fp = fopen(strPath, "rb");
		if(fp == NULL)		return;

		char buff[10000] = {0,};
		fread(&buff, 10000, 1, fp);

		CString strBuff;
		strBuff.Format("%s", buff);

		int nFind = strBuff.Find('\n');
		int nPos = 0;
		//ljb 2010-06-22 Start-----------------------------------
		while(nFind > 0)
		{
			strTemp = strBuff.Mid(nPos,nFind - nPos -1);
			
			//char Buf[50];
			char Buf[100]; //20191206 lyj CanTitle에서 저장되는 문자열이 50byte 초과하여 수정
			char seps[] = ",";
			char * token;
			sprintf(Buf, strTemp);
			
			PS_CAN_DISP_INFO* pCan = GetArrayCanInfo(nIndex) ;
			
			//ljb CAN 정보의 이름을 추가하도록 변경
			token = strtok(Buf, seps);				// 1.순서
//			m_ArrayCanInfo[nIndex].chNo = atoi(token);		//ljb 2011214 이재복 //////////
			//pCan->chNo = atoi(token);		//ljb 2011214 이재복 //////////
			pCan->chNo = nIndex;		//ksj 20210826 : 파일에서 읽어온 index를 쓸필요는 없을듯. 읽어온 순서대로 0-base로 지정한다.

			token = strtok(NULL, seps);				// 2.canType	master,slave
//			m_ArrayCanInfo[nIndex].canType = atoi(token);
			pCan->canType = atoi(token);
			
			token = strtok(NULL, seps);				// 3.canDataType
//			m_ArrayCanInfo[nIndex].canDataType = atoi(token);
			pCan->canDataType = atoi(token);

			token = strtok(NULL, seps);				// 4.canName
			if(token != NULL)
			{
//				sprintf(m_ArrayCanInfo[nIndex].canName, "%s", token);
				sprintf(pCan->canName, "%s", token);
			}
			else
			{
//				sprintf(m_ArrayCanInfo[nIndex].canName, "CAN_%d", nIndex+1);
				sprintf(pCan->canName, "CAN_%d", nIndex+1);
			}
			
			nIndex++;
			
			token = strtok(NULL, seps);
			nPos = nFind+1;
			nFind = strBuff.Find('\n', nFind+1);			
		}	
		//ljb 2010-06-22 End -----------------------------------	
		fclose(fp);
	}

	if(GetRecordItemSize() >= nIndex)
		m_nCanColumnCount = nIndex;
	else
		m_nCanColumnCount = 0;
}

fltPoint * CTable::GetAuxData(LONG &lDataNum, int nAuxCol)
{
	lDataNum = m_lLoadedDataCount;

	fltPoint* pfltPt = NULL;
	pfltPt = new fltPoint[lDataNum];
	for(int i = 0 ; i < lDataNum; i++)
	{
		pfltPt[i].x = m_ppfltData[GetRecordItemSize() - m_nAuxColumnCount + nAuxCol][i];
	}
	return pfltPt;
}

int CTable::GetAuxIndexToTitle(CString strTitle, WORD wPoint)
{
	int nType = 0;
//	int nIndex = 0;

	if(strTitle == RS_COL_AUX_TEMP)
		nType= PS_AUX_TYPE_TEMPERATURE;
	else if(strTitle == RS_COL_AUX_VOLT)
		nType= PS_AUX_TYPE_VOLTAGE;
	else if(strTitle == RS_COL_AUX_TEMPTH) //20180607 yulee
		nType= PS_AUX_TYPE_TEMPERATURE_TH;
	else if(strTitle == RS_COL_AUX_HUMI) //ksj 20200116 : 습도 추가.
		nType= PS_AUX_TYPE_HUMIDITY;

	for(int i = 0 ; i < this->GetAuxColumnCount(); i++)
	{
//		if(m_AuxInfo[i][0] == wPoint && m_AuxInfo[i][1] == nType)
//		if(m_ArrayAuxInfo[i].chNo == wPoint && m_ArrayAuxInfo[i].auxType == nType)
				
		PS_AUX_DISP_INFO* pAux = GetArrayAuxInfo(i) ;
		if (pAux->chNo == wPoint && pAux->auxType == nType)
		{
			return i;
		}
	}
	
	return -1;
}

//////////////////////////////////////////////////////////////////////////
//ljb 2011215 이재복 S //////////
int CTable::GetCanIndexToTitle(CString strTitle, WORD wPoint)
{
	for(int i = 0 ; i < this->GetCanColumnCount(); i++) //yulee 20180831-2
	{
//		if(m_ArrayCanInfo[i].chNo == wPoint)	// && m_ArrayAuxInfo[i].auxType == nType)
		PS_CAN_DISP_INFO* pCan = GetArrayCanInfo(i) ;
		if ((pCan->chNo)+1 == wPoint)	// && m_ArrayAuxInfo[i].auxType == nType) 
		{
			return i;
		}
	}
	
	return -1;
}
//////////////////////////////////////////////////////////////////////////

void CTable::SetAuxLastData(float * pfltLastAux, int nCount)
{
	if(pfltLastAux == NULL) return;
	m_pfltAuxLastData = new float[nCount];
	memcpy(m_pfltAuxLastData, pfltLastAux, sizeof(float)*nCount);
}

int CTable::LoadCanData(CString strChPath)
{

	LoadCanTitle(strChPath);

	//----------------------------------------------------------------
	// Added by 224 (2013/12/29) : DataView와 소스 호환을 위해 변수명 조정
	// 1. 레코드의 종료 행 인덱스
	int nFrom = 0;
	
	nFrom = m_lRecordIndex ;
	
	// 2. 레코드의 시작 행 인덱스
	int nTo   = m_lRecordIndex + m_lNumOfData - 1 ;
	ASSERT(nTo >= nFrom) ;
	
	CString strTemp ;
	//----------------------------------------------------------------
	
	// 3. 파일관련 변수
	CFileFind afinder;
	FILE* fp;
	
	// 4. CAN 데이터 처리. PS_CAN_FILE_NAME_EXT	"can"
	// 4.1. 헤더초기화
	PS_CAN_FILE_HEADER CanHeader ;
	ZeroMemory(&CanHeader, sizeof(CanHeader)) ;

	// 4.2. cyc 파일은 여러개 있는 것으로 간주함
	CStringArray arCanFiles ;
	if (afinder.FindFile(strChPath + "\\*." + PS_CAN_FILE_NAME_EXT + "*"))
	{
		int nLoop = 0 ;
		
		BOOL bContinue = TRUE ;
		while (bContinue)
		{
			nLoop++ ;
			bContinue = afinder.FindNextFile() ;
			arCanFiles.Add(afinder.GetFilePath()) ;
		} ;
	}
	
	afinder.Close() ;

	// 4.3. 파일속성, 레코드크기 및 버퍼 초기화
	// 	int nCanFileSize   = 0 ;
	// 	int nCanRecordSize = 0 ;
	long long llCanFileSize   = 0 ; //lyj 20200703 CAN파일 사이즈 int 범위 넘어감. //lyj 20210729 
	long long llCanRecordSize = 0 ; //lyj 20200703 CAN파일 사이즈 int 범위 넘어감. //lyj 20210729

	// 4.4. String array 에 값이 있으면(파일들이 존재하면) 진행한다.
	int nColCanCount = 0 ;
	if (arCanFiles.GetSize() > 0)
	{
		// 4.4.1. CAN 파일의 헤더를 얻는다.
		strTemp = arCanFiles.GetAt(0) ;
		fp = fopen(strTemp, "rb");		
		if (fp)
		{
			// .1. 파일의 길이 계산
			//nCanFileSize = filelength(fileno(fp));		
			llCanFileSize = _filelengthi64(fileno(fp)); //lyj 202100729
			ASSERT(llCanFileSize >= sizeof(PS_CAN_FILE_HEADER)) ;

			// .2. header 는 따로 파일에서 읽어와서 저장한다.
			fread(&CanHeader, sizeof(CanHeader), 1, fp );
			
			// .3. 레코드의 갯수 계산
			llCanRecordSize = sizeof(CAN_VALUE) * CanHeader.canHeader.nColumnCount;
			fclose( fp );
		}

		fp = NULL ;

		// 4.4.2. 컬럼수를 멤버변수에 설정한다.
		nColCanCount = CanHeader.canHeader.nColumnCount;
// 		for (int i = 0; i < nColCanCount; i++)
// 		{
// 			m_DataItemList.Add(CanHeader.canHeader.awColumnItem[i]);
// 		}

		// 4.4.3. 컬럼별로 데이터의 값을 수집한다.
		// 2차원 동적 배열 
		// 
		m_ppCanData = new CAN_VALUE* [nColCanCount];
		
		for (int i = 0; i < nColCanCount; i++) 
		{
			m_ppCanData[i] = new CAN_VALUE[m_lNumOfData];
			ZeroMemory(m_ppCanData[i], sizeof(CAN_VALUE) * m_lNumOfData);
		}
				
		// 4.4.4. 결과 버퍼의 크기를 정한다.
		int num             = nTo - nFrom + 1;		
		int nStartingRecord = 0 ;		// 파일별 시작 레코드의 번호
		int nBegin          = nFrom ;	// 파일별 중첩시 가변적이다.
		int nIdx            = 0 ;		// 버퍼인덱스
		
		for (int nLoop = 0; nLoop < arCanFiles.GetSize(); nLoop++)
		{
			if (llCanRecordSize == 0)
			{
				break ;
			}

			// .1. 파일오픈.
			strTemp = arCanFiles.GetAt(nLoop) ;
			fp = fopen(strTemp, "rb");
			
			// .2. 파일이 보유한 레코드 갯수를 읽는다.
			//nCanFileSize = filelength(fileno(fp));		
			llCanFileSize = _filelengthi64(fileno(fp));		//lyj 20210729
			int nFileNumOfRecord = (llCanFileSize - sizeof(PS_CAN_FILE_HEADER)) / llCanRecordSize ;

			// .3. 현재 파일에 검색하려는 행이 포함됐는지 확인
			if (nBegin < (nStartingRecord + nFileNumOfRecord))
			{
				//int nOffset = sizeof(PS_CAN_FILE_HEADER) + (nBegin-nStartingRecord) * nCanRecordSize ;
				long long nOffset = sizeof(PS_CAN_FILE_HEADER) + (nBegin-nStartingRecord) * llCanRecordSize ; //lyj 20200703 CAN파일 사이즈 int 범위 넘어감.
				//fseek(fp, nOffset, SEEK_SET) ; //lyj 20200703
				_fseeki64(fp, nOffset, SEEK_SET) ; //lyj 20200703 CAN파일 사이즈 int 범위 넘어감.

//-----------------------------------------------------
				CAN_VALUE* fpBuff = new CAN_VALUE[nColCanCount];
				ZeroMemory(fpBuff, sizeof(CAN_VALUE) * nColCanCount);
//-----------------------------------------------------[

				// 이 파일에 레코드가 모두 있는지 여부
				if (nTo < (nStartingRecord + nFileNumOfRecord))
				{
					int num = nTo - nBegin + 1 ;
					ASSERT(num <= m_lNumOfData) ;
					
					for (int nLoop = 0; nLoop < num && !feof(fp); nLoop++)
					{
						VERIFY(fread(fpBuff, llCanRecordSize, 1, fp) > 0) ;

						for (int i = 0; i < nColCanCount; i++)
						{
							m_ppCanData[i][nIdx+nLoop] = fpBuff[i];
						}
					}
					
					delete [] fpBuff;

					fclose(fp) ;
					break ;
				}
				else
				{
					// 다음 파일에 레코드가 중첩된 경우
					// 파일의 마지막 까지는 배열에 쓴다.
					int num = nStartingRecord + nFileNumOfRecord - nBegin ;
					for (int nLoop = 0; nLoop < num && !feof(fp); nLoop++)
					{
						VERIFY(fread(fpBuff, llCanRecordSize, 1, fp) > 0) ;

						for (int i = 0; i < nColCanCount; i++)
						{
							m_ppCanData[i][nIdx+nLoop] = fpBuff[i];
						}
						
						//						rsCount++;
					}

					// nIdx 의 위치를 변경시킨다.
					nIdx += num ;
					
					// nBegin 의 위치를 변경한다.
					nBegin = nStartingRecord + nFileNumOfRecord;

					delete [] fpBuff;
				}
				
			}
			
			nStartingRecord += nFileNumOfRecord ;
			fclose(fp) ;
		}
		
		afinder.Close();
	}

//--------------------------------------------------------------------


	m_nCanColumnCount = nColCanCount;

	return nColCanCount;
	
}

int CTable::LoadCanData_ORIGIN(LPCTSTR strCanPath)
{
	LoadCanTitle(strCanPath);	//ljb 20131105 add
	CString strPath;
	strPath.Format("%s\\*.%s", strCanPath, PS_CAN_FILE_NAME_EXT);	//각 Step의 Data가 저장되어 있는 파일 
	
	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return 0;
	
	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	//Data를 읽어들임
	FILE *fp = fopen(strPath, "rb");
	if(fp == NULL)		return 0;
	
	//File Header를 읽음
	PS_FILE_ID_HEADER fileID;
	if(fread(&fileID, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)	
	{
		fclose(fp);
		return 0;
	}
	if(fileID.nFileID != PS_PNE_CAN_FILE_ID)
	{
		fclose(fp);
		return 0;
	}
	
	PS_CAN_DATA_FILE_HEADER * pCanHeader = new PS_CAN_DATA_FILE_HEADER;
	ZeroMemory(pCanHeader, sizeof(PS_CAN_DATA_FILE_HEADER));
	
	//File Header를 읽음
	if(fread(pCanHeader, sizeof(PS_CAN_DATA_FILE_HEADER), 1, fp) < 1)	
	{
		delete pCanHeader;
		fclose(fp);
		return 0;
	}
	
	//저장된 Column수를 구함 
	if(pCanHeader->nColumnCount < 1)		
	{
		delete pCanHeader;
		fclose(fp);
		return 0;
	}	
	
	int nColCanCount = pCanHeader->nColumnCount;
	//	for(int i=0; i < nColCanCount; i++)
	//	{
	//		m_DataItemList.Add(pCanHeader->awColumnItem[i]);
	//	}
	
	delete pCanHeader;
	
	int nHeaderSize = sizeof(PS_FILE_ID_HEADER)+sizeof(PS_CAN_DATA_FILE_HEADER);
	//저장된 Data의 크기 
	size_t nRecordSize = sizeof(CAN_VALUE)*nColCanCount;
	
	//주어진 Index로 이동한다.
	fseek(fp, m_lRecordIndex*nRecordSize+nHeaderSize,SEEK_SET);
	
	ASSERT(m_ppCanData == NULL);
	//2차원 동적 배열 

	//ljb 201311 여기수정하자

	m_ppCanData = new CAN_VALUE*[nColCanCount];
	
	for(int i=0;i<nColCanCount;i++) 
	{
		m_ppCanData[i] = new CAN_VALUE[m_lNumOfData];
		ZeroMemory(m_ppCanData[i], sizeof(CAN_VALUE) * m_lNumOfData);
	}

	CAN_VALUE *pBuff = new CAN_VALUE[nColCanCount];
	int rsCount = 0;
	while(fread(pBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lNumOfData)
	{	
		for(int i=0; i<nColCanCount; i++)
		{
			m_ppCanData[i][rsCount] = pBuff[i];
		}
		rsCount++;
	}
	delete [] pBuff;
	fclose(fp);

	m_nCanColumnCount = nColCanCount;

	return nColCanCount;
// 	//2차원 동적 배열 
// 	m_ppCanData = new CAN_VALUE*[nColCanCount];
// 	
// 	for(int i=0;i<nColCanCount;i++) 
// 	{
// 		m_ppCanData[i] = new CAN_VALUE[m_lNumOfData];
// 		ZeroMemory(m_ppCanData[i], sizeof(CAN_VALUE) * m_lNumOfData);
// 	}
// 	
// 	CAN_VALUE *pBuff = new CAN_VALUE[nColCanCount];
// 	int rsCount = 0;
// 	while(fread(pBuff, nRecordSize, 1, fp) > 0 && rsCount < m_lNumOfData)
// 	{	
// 		for(int i=0; i<nColCanCount; i++)
// 		{
// 			m_ppCanData[i][rsCount] = pBuff[i];
// 		}
// 		rsCount++;
// 	}
// 	delete [] pBuff;
// 	fclose(fp);
// 	
// 	m_nCanColumnCount = nColCanCount;
// 	
// 	return nColCanCount;
}

CAN_VALUE* CTable::GetCanData(LONG &lDataNum, int nCanCol)
{
	if(m_ppCanData == NULL) return NULL;
	lDataNum = m_lLoadedDataCount;

	CAN_VALUE* pCanVal = NULL;
	pCanVal = new CAN_VALUE[lDataNum];
	for(int i = 0 ; i < lDataNum; i++)
	{
		pCanVal[i] = m_ppCanData[nCanCol][i];
	}
	return pCanVal;
}
CAN_VALUE CTable::GetCanDatum(INT row, int nCanCol)
{
	CAN_VALUE value ;
	value.fVal[0] = 0.0f ;

	if (m_ppCanData == NULL)
	{
		return value ;
	}

	ASSERT(row < m_lLoadedDataCount) ;
		
	return m_ppCanData[nCanCol][row] ;
}

void CTable::SetCanLastData(CAN_VALUE *pLastCan, int nCount)
{
	if(pLastCan == NULL) return;
	m_pCanLastData = new CAN_VALUE[nCount];
	memcpy(m_pCanLastData, pLastCan, sizeof(CAN_VALUE)*nCount);	
}

CAN_VALUE CTable::GetLastCanData(int nCanIndex)
{
/*	CAN_VALUE retValue;
	retValue.fVal[0] = 0.0f;
	if(m_pCanLastData == NULL)
		return retValue;*/
	return m_pCanLastData[nCanIndex];
}

BOOL CTable::EditRecordIndex(int nFrom, int nTo)
{
	m_lRecordIndex = nFrom ;
	m_lLastRecordIndex = nTo;
	m_lNumOfData = m_lLastRecordIndex-m_lRecordIndex+1;
	m_lLoadedDataCount = m_lNumOfData;
	return TRUE;
}
