// Step.cpp: implementation of the CStep class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Step.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
FILE_STEP_PARAM_V100D_LOAD	CStep::GetFileStep_vD_load()
{
	int i;
	FILE_STEP_PARAM_V100D_LOAD stepData;
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100D_LOAD));

	stepData.chStepNo	= m_StepIndex;			
	stepData.nProcType	= m_lProcType;		
	stepData.chType		= m_type;
	stepData.chMode		= m_mode;
	stepData.fVref_Charge = m_fVref_Charge;
	stepData.fVref_DisCharge = m_fVref_DisCharge;
	stepData.fIref		= m_fIref;
	stepData.fPref		= m_fPref;
	stepData.fRref		= m_fRref;

	stepData.fStartT	= m_fStartT;	
	stepData.fEndT		= m_fEndT;		
	stepData.fTref		= m_fTref;		
	stepData.fTrate		= m_fTrate;	
	stepData.fHref		= m_fHref;		
	stepData.fHrate		= m_fHrate;	

	stepData.fEndTime	= m_fEndTime;	
	stepData.fEndV		= m_fEndV_H;
	stepData.fEndV_L	= m_fEndV_L;
	stepData.fEndI		= m_fEndI;
	stepData.fEndC		= m_fEndC;
	stepData.fEndDV		= m_fEndDV;		
	stepData.fEndDI		= m_fEndDI;	
	stepData.fEndW		= m_fEndW;
	stepData.fEndWh		= m_fEndWh;

	stepData.bValueItem = m_bValueItem	;			
	stepData.bValueStepNo = m_bValueStepNo ;		
	stepData.fValueRate	= m_fValueRate ;			

	stepData.nLoopInfoCycle = m_nLoopInfoCycle;	
	stepData.nLoopInfoGotoStep  = m_nLoopInfoGotoStep;	

	stepData.nMultiLoopGroupID	= m_nMultiLoopGroupID;
	stepData.nMultiLoopInfoCycle	= m_nMultiLoopInfoCycle;
	stepData.nMultiLoopInfoGotoStep	= m_nMultiLoopInfoGotoStep;

	stepData.nAccLoopGroupID	= m_nAccLoopGroupID;
	stepData.nAccLoopInfoCycle	= m_nAccLoopInfoCycle;
	stepData.nAccLoopInfoGotoStep	= m_nAccLoopInfoGotoStep;

	stepData.nGotoStepEndV_H = m_nGotoStepEndV_H;
	stepData.nGotoStepEndV_L = m_nGotoStepEndV_L;
	stepData.nGotoStepEndTime = m_nGotoStepEndTime;
	stepData.nGotoStepEndCVTime = m_nGotoStepEndCVTime;
	stepData.nGotoStepEndC = m_nGotoStepEndC;
	stepData.nGotoStepEndWh = m_nGotoStepEndWh;
	stepData.nGotoStepEndValue = m_nGotoStepEndValue;

	stepData.fVLimitHigh	=	m_fHighLimitV;
	stepData.fVLimitLow		=	m_fLowLimitV;
	stepData.fILimitHigh	=	m_fHighLimitI;	
	stepData.fILimitLow		=	m_fLowLimitI;
	stepData.fCLimitHigh	=	m_fHighLimitC;
	stepData.fCLimitLow		=	m_fLowLimitC;
	stepData.fCapacitanceHigh	= m_fHighCapacitance;
	stepData.fCapacitanceLow	= m_fLowCapacitance;
	stepData.fImpLimitHigh	=	m_fHighLimitImp;
	stepData.fImpLimitLow	=	m_fLowLimitImp;
	stepData.fHighLimitTemp = m_fHighLimitTemp;
	stepData.fLowLimitTemp = m_fLowLimitTemp;

	stepData.fDeltaTime		=	m_fDeltaTimeV; 	
	stepData.fDeltaTime1	=	m_fDeltaTimeI;	
	stepData.fDeltaV		=	m_fDeltaV;		
	stepData.fDeltaI		=	m_fDeltaI;

	stepData.bGrade			=	m_bGrade;
	stepData.sGrading_Val.chTotalGrade			= (BYTE)m_Grading.GetGradeStepSize();
	//	stepData.sGrading_Val.lGradeItem = m_Grading.m_lGradingItem;

	GRADE_STEP	grade_step;
	for(int i = 0; i<m_Grading.GetGradeStepSize(); i++)
	{
		grade_step = m_Grading.GetStepData(i);
		stepData.sGrading_Val.faValue1[i]	=	grade_step.fMin;
		stepData.sGrading_Val.faValue2[i]	=	grade_step.fMax;
		stepData.sGrading_Val.aszGradeCode[i]	=	grade_step.strCode[0];
	}


	i=0;
	while(i<3 && i<PS_MAX_COMP_POINT)
	{
		stepData.fCompVLow[i]	=	m_fCompLowV[i];
		stepData.fCompVHigh[i]	=	m_fCompHighV[i];	
		stepData.fCompTimeV[i] =	m_fCompTimeV[i];
		stepData.fCompILow[i]	=	m_fCompLowI[i];
		stepData.fCompIHigh[i]	=	m_fCompHighI[i];
		stepData.fCompTimeI[i]	=	m_fCompTimeI[i];
		i++;
	}

	//Not Use (Time 비교값으로 대치 가능 하므로 사용 안함
	//	stepData.fIEndHigh;		
	//	stepData.fIEndLow;			
	//	stepData.fVEndHigh;			
	//	stepData.fVEndLow;	

	stepData.fReportTemp = m_fReportTemp;
	stepData.fReportV	=	m_fReportV;
	stepData.fReportI	=	m_fReportI;
	stepData.fReportTime	=	m_fReportTime;

	stepData.fCapaVoltage1 = m_fCapaVoltage1;	
	stepData.fCapaVoltage2 = m_fCapaVoltage2;	

	stepData.fDCRStartTime = m_fDCRStartTime;	
	stepData.fDCREndTime = m_fDCREndTime;

	stepData.fLCStartTime = m_fLCStartTime;	
	stepData.fLCEndTime = m_fLCEndTime;		

	stepData.lRange = m_lRange;	

	//ljb 201012
	stepData.bUseCyclePause = m_bUseCyclePause;
	stepData.bUseLinkStep = m_bUseLinkStep;
	stepData.bUseChamberProg = m_bUseChamberProg;

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.07
	stepData.lEndTimeDay = m_lEndTimeDay;
	stepData.lCVTimeDay = m_lCVTimeDay;
	// -
	//////////////////////////////////////////////////////////////////////////
	//ljb 20101230
	for( int i=0; i < MAX_STEP_CAN_AUX_COMPARE_SIZE; i++)
	{
		stepData.iBranchCanDivision[i] = m_ican_function_division[i];
		stepData.fBranchCanValue[i] = m_fcan_Value[i];
		stepData.cBranchCanDataType[i] = m_ican_data_type[i];
		stepData.cBranchCanCompareType[i] = m_ican_compare_type[i];
		stepData.wBranchCanStep[i] = m_ican_branch[i];

		stepData.iBranchAuxDivision[i] = m_iaux_function_division[i];
		stepData.fBranchAuxValue[i] = m_faux_Value[i];
		stepData.cBranchAuxDataType[i] = m_iaux_data_type[i];
		stepData.cBranchAuxCompareType[i] = m_iaux_compare_type[i];
		stepData.wBranchAuxStep[i] = m_iaux_branch[i];
	}

	//////////////////////////////////////////////////////////////////////////
	// ljb 20150512 add
	stepData.nValueLoaderItem = nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
	stepData.nValueLoaderMode = nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	stepData.fValueLoaderSet = fValueLoaderSet;			//설정값 W,A,V,Ohm
	// -
	//////////////////////////////////////////////////////////////////////////
	return stepData;
}
