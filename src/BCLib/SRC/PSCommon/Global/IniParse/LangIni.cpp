// ORIGIONAL FILE INFO:
// IniFile.cpp: implementation of the CIniFile class.
// Written by: Adam Clauss
//
// ADAPTED FILE INFO:
// LangIni.cpp: implementation of the CLangIni class.
// Written by: OMOSHIMA
//     CODE ADAPTED FROM: Adam Clauss
//
// NOTES:
//    I removed some functions and changed some functions to more
//  precicely fit the needs of a Language .INI File..... Hope everyone
//  understands it. I also changed it to conform with VC++.NET specs.
//////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "LangIni.h"

#include <fstream>
using namespace std;

/////////////////////////////////////////////////////////////////////
// Construction/Destruction
/////////////////////////////////////////////////////////////////////

//default constructor
CLangIni::CLangIni(void)
	: m_path(_T("")), m_errorMessage(_T(""))
{
}

//default destructor
CLangIni::~CLangIni(void)
{
}

/////////////////////////////////////////////////////////////////////
// Private Functions
/////////////////////////////////////////////////////////////////////

// Returns index of specified key, or -1 if not found
int CLangIni::FindKey(CString keyname)
{
	int keynum = 0;
	
	while (keynum < this->keys.GetSize() && names[keynum] != keyname)
		keynum++;
	
	if (keynum == keys.GetSize())
		return -1;
	
	return keynum;
}

// Returns index of specified value in the specified key, or -1 if not found
int CLangIni::FindValue(int keynum, CString valuename)
{
	if (keynum == -1)
		return -1;
	
	int valuenum = 0;
	
	while (valuenum < keys[keynum].names.GetSize() && keys[keynum].names[valuenum] != valuename)
		valuenum++;
	
	if (valuenum == keys[keynum].names.GetSize())
		return -1;
	
	return valuenum;
}

//overloaded from original getline to take CString
istream& CLangIni::getline(istream &is, CString &str)
{
	char buf[2048];
    is.getline(buf,2048);
    str = buf;
    return is;
}


/////////////////////////////////////////////////////////////////////
// Public Functions
/////////////////////////////////////////////////////////////////////

bool CLangIni::LoadFile(CString path)
{
	m_path = path;
	return LoadFile();
}

// Reads ini file specified using CIniFile::SetPath()
// Returns true if successful, false otherwise
bool CLangIni::LoadFile(void)
{
	CFile file;
	CFileStatus status;
	
	if (!file.GetStatus(m_path,status))
	{
		return false;
	}
	
	ifstream inifile;
	CString readinfo;
	
	inifile.open(m_path);
	
	int curkey = -1, curval = -1;
	
	if (inifile.fail())
	{
		m_errorMessage.Format(_T("Unable to open file: \"%s\"!!"), m_path);
		return false;
	}
	
	CString keyname, valuename, value;
	CString temp;
	
	while (getline(inifile,readinfo))
	{
		if (readinfo != _T("") && readinfo[0] != ';')
		{
			if (readinfo[0] == '[' && readinfo[readinfo.GetLength()-1] == ']') //if a section heading
			{
				keyname = readinfo;
				keyname.TrimLeft('[');
				keyname.TrimRight(']');
			}
			
			else//if a value
			{
				valuename = readinfo.Left(readinfo.Find(_T("=")));
				value = readinfo.Right(readinfo.GetLength()-valuename.GetLength()-1);
				SetValue(keyname,valuename,value);
			}
		}
	}
	
	inifile.close();	
	return true;
}

// Sets value of:
//     [keyname]
//     valuename=_____
//
// NOTE: Specify the optional paramter as false if you do not want it to create
// the key if it doesn't exist. Returns true if data entered, otherwise false
bool CLangIni::SetValue(CString keyname, CString valuename, CString value, bool create)
{
	int keynum = FindKey(keyname), valuenum = 0;
	//find key
	if (keynum == -1) //if key doesn't exist
	{
		if (!create)
		{//and user does not want to create it,
			return 0; //stop entering this key
		}
		names.SetSize(names.GetSize()+1);
		keys.SetSize(keys.GetSize()+1);
		keynum = names.GetSize()-1;
		names[keynum] = keyname;
	}

	//find value
	valuenum = FindValue(keynum,valuename);
	if (valuenum == -1)
	{
		if (!create) return 0;

		keys[keynum].names.SetSize(keys[keynum].names.GetSize()+1);
		keys[keynum].values.SetSize(keys[keynum].names.GetSize()+1);
		valuenum = keys[keynum].names.GetSize()-1;
		keys[keynum].names[valuenum] = valuename;
	}
	keys[keynum].values[valuenum] = value;

	SetFile(keyname,valuename,value);
	return 1;
}

// Gets value of:
//     [keyname]
//     valuename=_____
CString CLangIni::GetValue(CString keyname, CString valuename, CString sdefault)
{
	int keynum = FindKey(keyname), valuenum = FindValue(keynum,valuename);

	if (keynum == -1)
	{
		m_errorMessage.Format(_T("Unable to locate specified key: [%s]"), keyname);
		return sdefault;
	}

	if (valuenum == -1)
	{
		m_errorMessage.Format(_T("Unable to locate specified value: %s."), valuename);
		return sdefault;
	}
	
	return keys[keynum].values[valuenum];
}

void CLangIni::SetFile(CString keyname,CString valuename,CString value)
{
	WritePrivateProfileString(keyname,valuename,value,m_path);
}

void CLangIni::SetEmpty(CString keyname)
{
	WritePrivateProfileString(keyname, NULL, NULL, m_path); //delete
}

void CLangIni::GetMuldata(CString keyname,CString valuename,CStringArray& Data)
{//파일에서 멀티 구하기
	CString scount=_T("");
	CString sdata=_T("");
	Data.RemoveAll();
	int count=0;

	for(int i=0;;i++)
	{
	  scount.Format(_T("%s%d"),valuename,i);
	  sdata=GetValue(keyname, valuename, _T(""));

	  if(sdata!=_T("")) Data.Add(sdata);
	  else count=count+1;

	  if(count>10) break;
	}
}