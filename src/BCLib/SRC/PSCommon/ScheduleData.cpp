// ScheduleData.cpp: implementation of the CScheduleData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ScheduleData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "TestConReportDlg.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



CScheduleData::CScheduleData()
{
	ResetData();

}

CScheduleData::CScheduleData(CString strFileName)
{
	SetSchedule(strFileName);
}


CScheduleData::~CScheduleData()
{
	CStep *pStep;
	for(int i = m_apStepArray.GetSize()-1; i>=0; i--)
	{
		pStep = (CStep *)m_apStepArray[i];
		delete pStep;
		pStep = NULL;
		m_apStepArray.RemoveAt(i);
	}
	m_apStepArray.RemoveAll();
}

//Schedule File로 부터 공정 조건을 Load한다.
BOOL CScheduleData::SetSchedule(CString strFileName)
{
	if(strFileName.IsEmpty())	return FALSE;
	
	CString strTemp;
	int iPatternCnt = 0; //ksj 20180528
	int iCellBALCnt=0;//cny 201809
	int iPwrSupplyCnt=0;//cny 201809
	int iChillerCnt=0;//cny 201809

	ResetData();

	//File Open
	FILE *fp = fopen(strFileName, "rb");
	if(fp == NULL){
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	fileHeader;
	if(fread(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	//Header 정보 검사
	//2014.09.26 파일아이디가 없거나 비정상적인 경우 그래프가 출력되지않음. 일단 주석처리.
	/*
	if(fileHeader.nFileID != SCHEDULE_FILE_ID)
	{
		fclose(fp);
		return FALSE;
	}
	*/
	//Header 정보 검사
	if(fileHeader.nFileVersion  > SCHEDULE_FILE_VER_v1015_v1) //yulee 20180828
	{
		fclose(fp);
		return FALSE;
	}

	//모델 정보 기록 
	FILE_TEST_INFORMATION modelData;
	if(fread(&modelData, sizeof(modelData), 1, fp) < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ModelData = modelData;

	//공정 정보 기록 
	FILE_TEST_INFORMATION testData;
	if(fread(&testData, sizeof(testData), 1, fp)  < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ProcedureData = testData;

	//지원하지 않는 문자를 변경해준다. 예) \ / : * ? " < > |
	CString strScheduleName;
	strScheduleName.Format("%s", testData.szName);
	strScheduleName.Replace('\\', '_');
	strScheduleName.Replace('/', '_');
	strScheduleName.Replace(':', '_');
	strScheduleName.Replace('*', '_');
	strScheduleName.Replace('?', '_');
	strScheduleName.Replace('"', '_');
	strScheduleName.Replace('<', '_');
	strScheduleName.Replace('>', '_');
	strScheduleName.Replace('|', '_');	
	sprintf(m_ProcedureData.szName, "%s", strScheduleName);

	//cell check 조건 기록 //yulee 20191104
	//if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v2_SCH)
	if(fileHeader.nFileVersion <= SCHEDULE_FILE_VER_v1013_v2_SCH) //하위버전 호환
	{
		FILE_CELL_CHECK_PARAM_V1013_v2_SCH cellCheck_v1013_v2;
		if(fread(&cellCheck_v1013_v2, sizeof(cellCheck_v1013_v2), 1, fp)  < 1)				//기록 실시 
		{
			fclose(fp);
			return FALSE;
		}
		CellCheck_v1013_v2_SCH(&cellCheck_v1013_v2);
		//m_CellCheck = cellCheck;
	}
	else
	{
		FILE_CELL_CHECK_PARAM cellCheck;
		if(fread(&cellCheck, sizeof(cellCheck), 1, fp)  < 1)				//기록 실시 
		{
			fclose(fp);
			return FALSE;
		}
		m_CellCheck = cellCheck;
	}

	//ksj 20180528	
	//step Size 정보 가져오기 
	PS_RECORD_FILE_HEADER stepSize;
	ZeroMemory(&stepSize, sizeof(PS_RECORD_FILE_HEADER));
	//ksj end


	
	//FILE_STEP_PARAM_10000	oldFileStep;
	//FILE_STEP_PARAM_V100A	stepData_v100A;
	FILE_STEP_PARAM_V100B			stepData_v100B;
	FILE_STEP_PARAM_V100C			stepData_v100C;
	FILE_STEP_PARAM_V100D			stepData_v100D;			//20150908
	FILE_STEP_PARAM_V100D_LOAD		stepData_v100D_LOAD;	//20150908
	FILE_STEP_PARAM_V100D_LOAD_SCH	stepData_v100D_LOAD_SCH; //ksj 20180528
	FILE_STEP_PARAM_V100F			stepData_v100F;			//20160427
	FILE_STEP_PARAM_V100F_v2		stepData_v100F_v2;		//20170518
	FILE_STEP_PARAM_V100F_v2_SCH	stepData_v100F_v2_SCH; //ksj 20180528
	FILE_STEP_PARAM_V1011_v1_SCH	stepData_v1011_v1_SCH; //yulee 20180828
	FILE_STEP_PARAM_V1011_v2_SCH	stepData_v1011_v2_SCH; //yulee 20180828

	FILE_STEP_PARAM_V1013_v1_SCH	stepData_v1013_v1_SCH; //yulee 20190707 //yulee 20190707 1013 protocol PowerSupply //LGC 오창 2공장 20190520 버전 
	FILE_STEP_PARAM_V1013_v2_SCH	stepData_v1013_v2_SCH; //yulee 20190707 //yulee 20190707 1013 VS2010 CWA
	FILE_STEP_PARAM_V1014_v1_SCH	stepData_v1014_v1_SCH; //yulee 20190707 //yulee 20190707 1014 Step별 셀 전압편차 안전 조건
	FILE_STEP_PARAM_V1015_v1		stepData_v1015_v1; 

	//if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v100B || fileHeader.nFileVersion > SCHEDULE_FILE_VER_v100D_LOAD)
	//{
	//	AfxMessageBox("하위버전의 File 입니다. 모든 STEP의 END 조건을 확인 하세요.");
	//}

	//step 정보 기록 
	CStep *pStep;
	if((fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v100D) && (fileHeader.nFileVersion <= SCHEDULE_FILE_VER_v100D_LOAD))////20180627 yulee : 스케쥴 호환
	{
		while(1)
		{
			ZeroMemory(&stepData_v100B,sizeof(FILE_STEP_PARAM_V100B));
			ZeroMemory(&stepData_v100C,sizeof(FILE_STEP_PARAM_V100C));
			ZeroMemory(&stepData_v100D,sizeof(FILE_STEP_PARAM_V100D));
			ZeroMemory(&stepData_v100D_LOAD,sizeof(FILE_STEP_PARAM_V100D_LOAD));
			
			if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100B)
			{
				if(fread(&stepData_v100B, sizeof(FILE_STEP_PARAM_V100B), 1, fp) < 1) break;			//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vB(&stepData_v100B);
				m_apStepArray.Add(pStep);
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100C || fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D)
			{
				if(fread(&stepData_v100D, sizeof(FILE_STEP_PARAM_V100D), 1, fp) < 1) break;			//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDto1011_V1_SCH(&stepData_v100D);
				m_apStepArray.Add(pStep);
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D_LOAD)
			{
				if(fread(&stepData_v100D_LOAD, sizeof(FILE_STEP_PARAM_V100D_LOAD), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDLto1011_V1_SCH(&stepData_v100D_LOAD);
				m_apStepArray.Add(pStep);
			}					
		}
	}
	else if((fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v100F) && (fileHeader.nFileVersion <= SCHEDULE_FILE_VER_v100F_v2)) //ksj 20180528 : 구버전 처리.
	{
		while(1)
		{
			ZeroMemory(&stepData_v100B,sizeof(FILE_STEP_PARAM_V100B));
			ZeroMemory(&stepData_v100D,sizeof(FILE_STEP_PARAM_V100D));
			ZeroMemory(&stepData_v100D_LOAD,sizeof(FILE_STEP_PARAM_V100D_LOAD));
			ZeroMemory(&stepData_v100F,sizeof(FILE_STEP_PARAM_V100F));
			ZeroMemory(&stepData_v100F_v2,sizeof(FILE_STEP_PARAM_V100F_v2));
		
			if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v100B)
			{
				fclose(fp);
				return FALSE;
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100B)
			{
				fclose(fp);
				AfxMessageBox("SCHEDULE version error");
				return FALSE;

			}
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F-1)//SCHEDULE_FILE_VER_v100F_v2) //YULEE 20170522 
			{
				if(fread(&stepData_v100F, sizeof(FILE_STEP_PARAM_V100F), 1, fp) < 1) break;	//기록 실시 
				pStep->SetStepData_vFto1013_V2_SCH(&stepData_v100F);
				m_apStepArray.Add(pStep);
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F_v2-1)//SCHEDULE_FILE_VER_v100F_v2) //YULEE 20170522 
			{
				if(fread(&stepData_v100F_v2, sizeof(FILE_STEP_PARAM_V100F_v2), 1, fp) < 1) break;	//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vF2to1013_V2_SCH(&stepData_v100F_v2);
				m_apStepArray.Add(pStep);
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D_LOAD_SCH) //20180627 yulee : 스케쥴 호환
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	
		{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;
		
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v100D_LOAD_SCH,sizeof(FILE_STEP_PARAM_V100D_LOAD_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D_LOAD_SCH) //ksj 20180528
			{
				if(fread(&stepData_v100D_LOAD_SCH, sizeof(FILE_STEP_PARAM_V100D_LOAD_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDL_SCHto1011_V1_SCH(&stepData_v100D_LOAD_SCH);
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F_v2_SCH) //ksj 20180528 : SCH 파일에 패턴 포함 처리. //20180627 yulee : 스케쥴 호환
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER_100F_READ), 1, fp)  < 1)	{ //yulee 20190221 PS_RECORD_FILE_HEADER -> PS_RECORD_FILE_HEADER_100F_READ
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;
		
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v100F_v2_SCH,sizeof(FILE_STEP_PARAM_V100F_v2_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F_v2_SCH) //ksj 20180528
			{
				if(fread(&stepData_v100F_v2_SCH, sizeof(FILE_STEP_PARAM_V100F_v2_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vF_V2_SCHto1013_V1_SCH(&stepData_v100F_v2_SCH);
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN) iPatternCnt++; //ksj 20180528
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v1_SCH) //yulee 20180828
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER_1011_v2), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;
		
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1011_v1_SCH,sizeof(FILE_STEP_PARAM_V1011_v1_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v1_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1011_v1_SCH, sizeof(FILE_STEP_PARAM_V1011_v1_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_1011_V1_SCHto1013_V1_SCH(&stepData_v1011_v1_SCH); //yulee 201801002
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v2_SCH) //yulee 20180828
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1011_v2_SCH,sizeof(FILE_STEP_PARAM_V1011_v2_SCH)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v2_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1011_v2_SCH, sizeof(FILE_STEP_PARAM_V1011_v2_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_v1011_v2_SCH(&stepData_v1011_v2_SCH); //yulee 201801002
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v1_SCH) //yulee 201801002
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1013_v1_SCH,sizeof(FILE_STEP_PARAM_V1013_v1_SCH)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v1_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1013_v1_SCH, sizeof(FILE_STEP_PARAM_V1013_v1_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_1013_V1_SCHto1013_V2_SCH(&stepData_v1013_v1_SCH); //yulee 201801002
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1014_v1_SCH) //yulee 201801002
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1014_v1_SCH,sizeof(FILE_STEP_PARAM_V1014_v1_SCH)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1014_v1_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1014_v1_SCH, sizeof(FILE_STEP_PARAM_V1014_v1_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_v1014_v1_SCH(&stepData_v1014_v1_SCH); //yulee 201801002
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v2_SCH) //yulee 201801002
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;
		
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1013_v2_SCH,sizeof(FILE_STEP_PARAM_V1013_v2_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v2_SCH) //yulee 201801002
			{
				if(fread(&stepData_v1013_v2_SCH, sizeof(FILE_STEP_PARAM_V1013_v2_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_v1013_v2_SCH(&stepData_v1013_v2_SCH); //yulee 201801002
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else  //if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1015_v1)
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1015_v1,sizeof(FILE_STEP_PARAM_V1015_v1)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1015_v1) //yulee 20180828
			{
				if(fread(&stepData_v1015_v1, sizeof(FILE_STEP_PARAM_V1015_v1), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_v1015_v1(&stepData_v1015_v1); //yulee 201801002
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}

	//ksj 20180528 //////////////////////////////////////////////////
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(NULL,szBuff,_MAX_PATH);
	CString strPath = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'))+"\\DataBase";
	CString strSavePatternFileName;
	
	for(int iCnt = 0; iCnt<iPatternCnt;iCnt++)
	{
		FILE_SIMULATION_INFO filePattern;
		ZeroMemory(&filePattern,sizeof(FILE_SIMULATION_INFO));
		if(fread(&filePattern, sizeof(FILE_SIMULATION_INFO), 1, fp) < 1)			//기록 실시 
		{
			fclose(fp);
			//return true;
			return FALSE;
		}
		else
		{
			//ljb 20130514 add 파일 있는지 체크 해서 있으면 삭제 할지 물어 본다.
			strTemp.Format("%s",filePattern.szFileName);	
			int nPos = strTemp.ReverseFind('\\');
			strTemp = strTemp.Right(strTemp.GetLength() - (nPos+1));
			strSavePatternFileName.Format("%s\\%s", strPath, strTemp);
			ZeroMemory(filePattern.szFileName,256);
			sprintf(filePattern.szFileName, "%s",strSavePatternFileName);
			
			CFileFind aFind;
			BOOL bFind = aFind.FindFile(filePattern.szFileName);
			BOOL bDeleteFile(TRUE);
			while(bFind)
			{
				bFind = aFind.FindNextFile();
				//strTemp.Format("Pattern File 동일한 파일이 있습니다. 덮어쓰기하겠습니다.");
				strTemp.Format(Fun_FindMsg("ScheduleData_SetSchedule_msg1","SCHEDULEDATA"));
				
				if(bFind == TRUE)
				{
					bDeleteFile = TRUE;
					DeleteFile(aFind.GetFilePath());
				}
				else
				{
					bDeleteFile = FALSE; 
				}
				
			}
			
			if (bDeleteFile)
			{
				//create
				FILE *fp2 = fopen(filePattern.szFileName,"wb");
				BYTE* pBuffer = new BYTE[filePattern.iFileSize];
				fread(pBuffer,filePattern.iFileSize,1,fp);	//sch 에서 읽기
				fwrite(pBuffer,filePattern.iFileSize,1,fp2);	//csv로 쓰기
				fclose(fp2);
				pStep = (CStep *)m_apStepArray.GetAt(filePattern.iStepNo);
				pStep->m_strPatternFileName = filePattern.szFileName;
				delete[] pBuffer;
			}
			else
			{
				//skip
				BYTE* pBuffer = new BYTE[filePattern.iFileSize];
				fread(pBuffer,filePattern.iFileSize,1,fp);	//sch 에서 읽기
				pStep = (CStep *)m_apStepArray.GetAt(filePattern.iStepNo);
				pStep->m_strPatternFileName = filePattern.szFileName;
				delete[] pBuffer;
			}
		}
	}
	//ksj end ///////////////////////////////////////////////////////

	//cny Cell Balance start///////////////////////////////////////////////////////
	if(iCellBALCnt>0)
	{
		for(int iCnt = 0; iCnt<iCellBALCnt;iCnt++)
		{
			FILE_CELLBAL_INFO fileCellBAL;
			ZeroMemory(&fileCellBAL,sizeof(FILE_CELLBAL_INFO));
			if((fread(&fileCellBAL, sizeof(FILE_CELLBAL_INFO), 1, fp) < 1) && (fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH))
			{
				fclose(fp);
				return FALSE;
			}
			else
			{
				pStep = (CStep *)m_apStepArray.GetAt(fileCellBAL.nStepIndex);
				if(!pStep) continue;
				
				if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v1011_v2_SCH)
				{
					pStep->m_nCellBal_CircuitRes     = 0;
					pStep->m_nCellBal_WorkVoltUpper  = 0;
					pStep->m_nCellBal_WorkVolt       = 0;
					pStep->m_nCellBal_StartVolt      = 0;
					pStep->m_nCellBal_EndVolt        = 0;
					pStep->m_nCellBal_ActiveTime     = 0;
					pStep->m_nCellBal_AutoStopTime   = 0;
					pStep->m_nCellBal_AutoNextStep   = 0;					
				}
				else if(fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH)
				{
					pStep->m_nCellBal_CircuitRes     = fileCellBAL.nCellBal_CircuitRes;
					pStep->m_nCellBal_WorkVoltUpper  = fileCellBAL.nCellBal_WorkVoltUpper;
					pStep->m_nCellBal_WorkVolt       = fileCellBAL.nCellBal_WorkVolt;
					pStep->m_nCellBal_StartVolt      = fileCellBAL.nCellBal_StartVolt;
					pStep->m_nCellBal_EndVolt        = fileCellBAL.nCellBal_EndVolt;
					pStep->m_nCellBal_ActiveTime     = fileCellBAL.nCellBal_ActiveTime;
					pStep->m_nCellBal_AutoStopTime   = fileCellBAL.nCellBal_AutoStopTime;
					pStep->m_nCellBal_AutoNextStep   = fileCellBAL.nCellBal_AutoNextStep;
				}
			}
		}
	}
	//cny Power Supply start///////////////////////////////////////////////////////
	if(iPwrSupplyCnt>0)
	{
		for(int iCnt = 0; iCnt<iPwrSupplyCnt;iCnt++)
		{
			FILE_PWRSUPPLY_INFO filePwrSupply;
			ZeroMemory(&filePwrSupply,sizeof(FILE_PWRSUPPLY_INFO));
			if((fread(&filePwrSupply, sizeof(FILE_PWRSUPPLY_INFO), 1, fp) < 1) && (fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH))
			{
				fclose(fp);
				return FALSE;
			}
			else
			{
				pStep = (CStep *)m_apStepArray.GetAt(filePwrSupply.nStepIndex);
				if(!pStep) continue;
				
				if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v1011_v2_SCH)
				{
					pStep->m_nPwrSupply_Cmd  =0;
					pStep->m_nPwrSupply_Volt =0;					
				}
				else if(fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH)
				{
					pStep->m_nPwrSupply_Cmd  =filePwrSupply.nPwrSupply_Cmd;
					pStep->m_nPwrSupply_Volt =filePwrSupply.nPwrSupply_Volt;
				}				
			}
		}
	}
	//cny Chiller start///////////////////////////////////////////////////////
	if(iChillerCnt>0)
	{
		for(int iCnt = 0; iCnt<iChillerCnt;iCnt++)
		{
			FILE_CHILLER_INFO fileChiller;
			ZeroMemory(&fileChiller,sizeof(FILE_CHILLER_INFO));
			if((fread(&fileChiller, sizeof(FILE_CHILLER_INFO), 1, fp) < 1) && (fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH))
			{
				fclose(fp);
				return FALSE;
			}
			else
			{
				pStep = (CStep *)m_apStepArray.GetAt(fileChiller.nStepIndex);
				if(!pStep) continue;
				
				if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v1011_v2_SCH)
				{
					pStep->m_nChiller_Cmd          =0;
					pStep->m_fChiller_RefTemp      =0;
					pStep->m_nChiller_RefCmd       =0;
					pStep->m_nChiller_PumpCmd      =0;
					pStep->m_fChiller_RefPump      =0;
					pStep->m_nChiller_TpData       =0;
					pStep->m_fChiller_ONTemp1      =0;
					pStep->m_nChiller_ONTemp1_Cmd  =0;
					pStep->m_fChiller_ONTemp2      =0;
					pStep->m_nChiller_ONTemp2_Cmd  =0;
					pStep->m_fChiller_OFFTemp1     =0;
					pStep->m_nChiller_OFFTemp1_Cmd =0;
					pStep->m_fChiller_OFFTemp2     =0;
					pStep->m_nChiller_OFFTemp2_Cmd =0; 				
				}
				else if(fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH)
				{
					pStep->m_nChiller_Cmd          =fileChiller.nChiller_Cmd;
					pStep->m_fChiller_RefTemp      =fileChiller.fChiller_RefTemp;
					pStep->m_nChiller_RefCmd       =fileChiller.nChiller_RefCmd;
					pStep->m_nChiller_PumpCmd      =fileChiller.nChiller_PumpCmd;
					pStep->m_fChiller_RefPump      =fileChiller.fChiller_RefPump;
					pStep->m_nChiller_TpData       =fileChiller.nChiller_TpData;
					pStep->m_fChiller_ONTemp1      =fileChiller.fChiller_ONTemp1;
					pStep->m_nChiller_ONTemp1_Cmd  =fileChiller.nChiller_ONTemp1_Cmd;
					pStep->m_fChiller_ONTemp2      =fileChiller.fChiller_ONTemp2;
					pStep->m_nChiller_ONTemp2_Cmd  =fileChiller.nChiller_ONTemp2_Cmd;
					pStep->m_fChiller_OFFTemp1     =fileChiller.fChiller_OFFTemp1;
					pStep->m_nChiller_OFFTemp1_Cmd =fileChiller.nChiller_OFFTemp1_Cmd;
					pStep->m_fChiller_OFFTemp2     =fileChiller.fChiller_OFFTemp2;
					pStep->m_nChiller_OFFTemp2_Cmd =fileChiller.nChiller_OFFTemp2_Cmd;    
				}	
			}
		}
	}

	fclose(fp);
	return TRUE;
}

void CScheduleData::CellCheck_v1013_v2_SCH(FILE_CELL_CHECK_PARAM_V1013_v2_SCH *pStepData)
{

	m_CellCheck.fMaxV = pStepData->fMaxV;
	m_CellCheck.fMinV = pStepData->fMinV;
	m_CellCheck.fMaxI = pStepData->fMaxI;			//최대 전류
	m_CellCheck.fCellDeltaV = pStepData->fCellDeltaV;	//ljb 20150730 edit 셀전압편차 값 , 최소 전류 -> 2010-09-09 사용 하지 않음 추후 업그레이드 이용시 써도 됨

	m_CellCheck.fSTDCellDeltaMaxV = 0.0;	
	m_CellCheck.fSTDCellDeltaMinV = 0.0;	
	m_CellCheck.fCellDeltaMaxV	  = 0.0;	
	m_CellCheck.fCellDeltaMinV	  = 0.0;	

	m_CellCheck.bfaultcompAuxV_Vent_Flag = FALSE;
	m_CellCheck.breserved1[3];

	m_CellCheck.fMaxT = pStepData->fMaxT;
	m_CellCheck.fMinT = pStepData->fMinT;
	m_CellCheck.fMaxC = pStepData->fMaxC;			//용량초과
	m_CellCheck.lMaxW = pStepData->lMaxW;			//전력초과
	m_CellCheck.lMaxWh= pStepData->lMaxWh;			//전력량초과

	//ljb 20100819	for v100A
	for(int i= 0; i<10; i++){
		m_CellCheck.ican_function_division[i] = pStepData->ican_function_division[i];}
	for(int i= 0; i<10; i++){
		m_CellCheck.cCan_compare_type[i] = pStepData->cCan_compare_type[i];}
	for(int i= 0; i<10; i++){
		m_CellCheck.cCan_data_type[i] = pStepData->cCan_data_type[i];}
	for(int i= 0; i<10; i++){
		m_CellCheck.fcan_Value[i] = pStepData->fcan_Value[i];}

	for(int i= 0; i<10; i++){
		m_CellCheck.iaux_function_division[i] = pStepData->iaux_function_division[i];}
	for(int i= 0; i<10; i++){
		m_CellCheck.cAux_compare_type[i] = pStepData->cAux_compare_type[i];}
	for(int i= 0; i<10; i++){
		m_CellCheck.cAux_data_type[i] = pStepData->cAux_data_type[i];}
	for(int i= 0; i<10; i++){
		m_CellCheck.faux_Value[i] = pStepData->faux_Value[i];}
	/*
	//1.15.1 부터 추가
	float	fSTDCellDeltaMaxV;	
	float	fSTDCellDeltaMinV;	
	float	fCellDeltaMaxV;	
	float	fCellDeltaMinV;	

	unsigned char bfaultcompAuxV_Vent_Flag;
	unsigned char breserved1[3];
	//1.15.1 부터 추가 끝
	*/
}

//ksj 20180528 : sch 파일 불러와 추가.
BOOL CScheduleData::Fun_SetScheduleFromSch(CString strFileName)
{
	if(strFileName.IsEmpty())	return FALSE;
	
	CString strTemp;
	int iPatternCnt = 0; //ksj 20180528
	int iCellBALCnt=0, iCellBALCntNum=0;//cny 201809 //20190318
	int iPwrSupplyCnt=0, iPwrSupplyCntNum=0;//cny 201809
	int iChillerCnt=0, iChillerCntNum=0;//cny 201809

	ResetData();

	//File Open
	FILE *fp = fopen(strFileName, "rb");
	if(fp == NULL){
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	fileHeader;
	if(fread(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}

	//Header 정보 검사
	if(fileHeader.nFileVersion  > SCHEDULE_FILE_VER_v1015_v1) //yulee 20180828
	{
		fclose(fp);
		AfxMessageBox("nFileVersion Error");
		return FALSE;
	}

	//모델 정보 기록 
	FILE_TEST_INFORMATION modelData;
	if(fread(&modelData, sizeof(modelData), 1, fp) < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ModelData = modelData;

	//공정 정보 기록 
	FILE_TEST_INFORMATION testData;
	if(fread(&testData, sizeof(testData), 1, fp)  < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	m_ProcedureData = testData;

	//지원하지 않는 문자를 변경해준다. 예) \ / : * ? " < > |
	CString strScheduleName;
	strScheduleName.Format("%s", testData.szName);
	strScheduleName.Replace('\\', '_');
	strScheduleName.Replace('/', '_');
	strScheduleName.Replace(':', '_');
	strScheduleName.Replace('*', '_');
	strScheduleName.Replace('?', '_');
	strScheduleName.Replace('"', '_');
	strScheduleName.Replace('<', '_');
	strScheduleName.Replace('>', '_');
	strScheduleName.Replace('|', '_');	
	sprintf(m_ProcedureData.szName, "%s", strScheduleName);

//cell check 조건 기록 //yulee 20191104
	if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v2_SCH)
	{
		FILE_CELL_CHECK_PARAM_V1013_v2_SCH cellCheck_v1013_v2;
		if(fread(&cellCheck_v1013_v2, sizeof(cellCheck_v1013_v2), 1, fp)  < 1)				//기록 실시 
		{
			fclose(fp);
			return FALSE;
		}
		CellCheck_v1013_v2_SCH(&cellCheck_v1013_v2);
		//m_CellCheck = cellCheck;
	}
	else
	{
		FILE_CELL_CHECK_PARAM cellCheck;
		if(fread(&cellCheck, sizeof(cellCheck), 1, fp)  < 1)				//기록 실시 
		{
			fclose(fp);
			return FALSE;
		}
		m_CellCheck = cellCheck;
	}

	//step Size 정보 가져오기 
	PS_RECORD_FILE_HEADER stepSize;
	ZeroMemory(&stepSize, sizeof(PS_RECORD_FILE_HEADER));
	//ksj end


	//step 정보 기록 
	//FILE_STEP_PARAM_10000	oldFileStep;
	//FILE_STEP_PARAM_V100A	stepData_v100A;
	FILE_STEP_PARAM_V100B			stepData_v100B;
	
	FILE_STEP_PARAM_V100C			stepData_v100C;
	
	FILE_STEP_PARAM_V100D			stepData_v100D;			//20150908
	FILE_STEP_PARAM_V100D_LOAD		stepData_v100D_LOAD;	//20150908
	FILE_STEP_PARAM_V100D_LOAD_SCH	stepData_v100D_LOAD_SCH; //ksj 20180528
	
	FILE_STEP_PARAM_V100F			stepData_v100F;			//20160427
	FILE_STEP_PARAM_V100F_v2		stepData_v100F_v2;		//20170518
	FILE_STEP_PARAM_V100F_v2_SCH	stepData_v100F_v2_SCH; //ksj 20180528
	
	FILE_STEP_PARAM_V1011_v1_SCH	stepData_v1011_v1_SCH; //yulee 20180828
	FILE_STEP_PARAM_V1011_v2_SCH	stepData_v1011_v2_SCH; //yulee 20180828

	FILE_STEP_PARAM_V1013_v1_SCH	stepData_v1013_v1_SCH; //yulee 20190707
	FILE_STEP_PARAM_V1013_v2_SCH	stepData_v1013_v2_SCH; //yulee 20190707

	FILE_STEP_PARAM_V1014_v1_SCH	stepData_v1014_v1_SCH; //yulee 20190707

	FILE_STEP_PARAM_V1015_v1		stepData_v1015_v1; 

	CStep *pStep;
	if((fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v100D) && (fileHeader.nFileVersion <= SCHEDULE_FILE_VER_v100D_LOAD))////20180627 yulee : 스케쥴 호환
	{
		while(1)
		{
			ZeroMemory(&stepData_v100B,sizeof(FILE_STEP_PARAM_V100B));
			ZeroMemory(&stepData_v100C,sizeof(FILE_STEP_PARAM_V100C));
			ZeroMemory(&stepData_v100D,sizeof(FILE_STEP_PARAM_V100D));
			ZeroMemory(&stepData_v100D_LOAD,sizeof(FILE_STEP_PARAM_V100D_LOAD));
			
			if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100B)
			{
				if(fread(&stepData_v100B, sizeof(FILE_STEP_PARAM_V100B), 1, fp) < 1) break;			//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vB(&stepData_v100B);
				m_apStepArray.Add(pStep);
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100C || fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D)
			{
				if(fread(&stepData_v100D, sizeof(FILE_STEP_PARAM_V100D), 1, fp) < 1) break;			//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDto1011_V1_SCH(&stepData_v100D);
				m_apStepArray.Add(pStep);
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D_LOAD)
			{
				if(fread(&stepData_v100D_LOAD, sizeof(FILE_STEP_PARAM_V100D_LOAD), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDLto1011_V1_SCH(&stepData_v100D_LOAD);
				m_apStepArray.Add(pStep);
			}					
		}
	}
	else if((fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v100F) && (fileHeader.nFileVersion <= SCHEDULE_FILE_VER_v100F_v2)) //ksj 20180528 : 구버전 처리.
	{
		while(1)
		{
			ZeroMemory(&stepData_v100B,sizeof(FILE_STEP_PARAM_V100B));
			ZeroMemory(&stepData_v100D,sizeof(FILE_STEP_PARAM_V100D));
			ZeroMemory(&stepData_v100D_LOAD,sizeof(FILE_STEP_PARAM_V100D_LOAD));
			ZeroMemory(&stepData_v100F,sizeof(FILE_STEP_PARAM_V100F));
			ZeroMemory(&stepData_v100F_v2,sizeof(FILE_STEP_PARAM_V100F_v2));
		
			if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v100B)
			{
				fclose(fp);
				return FALSE;
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100B)
			{
				fclose(fp);
				AfxMessageBox("SCHEDULE version error");
				return FALSE;

			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100C || fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D)
			{
				if(fread(&stepData_v100D, sizeof(FILE_STEP_PARAM_V100D), 1, fp) < 1) break;			//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDto1011_V1_SCH(&stepData_v100D);
				m_apStepArray.Add(pStep);
			}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D_LOAD)
			{
				if(fread(&stepData_v100D_LOAD, sizeof(FILE_STEP_PARAM_V100D_LOAD), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDLto1011_V1_SCH(&stepData_v100D_LOAD);
				m_apStepArray.Add(pStep);
			}
	 		else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F-1)
	 		{
	 			if(fread(&stepData_v100F, sizeof(FILE_STEP_PARAM_V100F), 1, fp) < 1) break;	//기록 실시 
	 			pStep = new CStep;
				pStep->SetStepData_vFto1011_V1_SCH(&stepData_v100F);
	 			m_apStepArray.Add(pStep);
	 		}
			else if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F_v2-1)//SCHEDULE_FILE_VER_v100F_v2) //YULEE 20170522 
			{
				if(fread(&stepData_v100F_v2, sizeof(FILE_STEP_PARAM_V100F_v2), 1, fp) < 1) break;	//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vF2to1011_V1_SCH(&stepData_v100F_v2);
				m_apStepArray.Add(pStep);
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D_LOAD_SCH) //20180627 yulee : 스케쥴 호환
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	
		{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;
		
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v100D_LOAD_SCH,sizeof(FILE_STEP_PARAM_V100D_LOAD_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100D_LOAD_SCH) //ksj 20180528
			{
				if(fread(&stepData_v100D_LOAD_SCH, sizeof(FILE_STEP_PARAM_V100D_LOAD_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vDL_SCHto1011_V1_SCH(&stepData_v100D_LOAD_SCH);
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN) iPatternCnt++; //ksj 20180528		
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F_v2_SCH) //ksj 20180528 : SCH 파일에 패턴 포함 처리. //20180627 yulee : 스케쥴 호환
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER_100F_READ), 1, fp)  < 1)	{ //yulee 20190221 PS_RECORD_FILE_HEADER -> PS_RECORD_FILE_HEADER_100F_READ
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;
		
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v100F_v2_SCH,sizeof(FILE_STEP_PARAM_V100F_v2_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v100F_v2_SCH) //ksj 20180528
			{
				if(fread(&stepData_v100F_v2_SCH, sizeof(FILE_STEP_PARAM_V100F_v2_SCH), 1, fp) < 1) break;	//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_vF_V2_SCHto1013_V1_SCH(&stepData_v100F_v2_SCH);
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN) iPatternCnt++; //ksj 20180528	
			

			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
			
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v1_SCH) //ksj 20180528 : SCH 파일에 패턴 포함 처리. //20180627 yulee : 스케쥴 호환
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;
		
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1011_v1_SCH,sizeof(FILE_STEP_PARAM_V1011_v1_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v1_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1011_v1_SCH, sizeof(FILE_STEP_PARAM_V1011_v1_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_1011_V1_SCHto1013_V1_SCH(&stepData_v1011_v1_SCH);
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN) iPatternCnt++; //ksj 20180528		
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v2_SCH) //yulee 20180828
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER_1011_v2), 1, fp)  < 1)	{ //yulee 20190807 강제병렬 마지막 포인트 순수 채널 -> PS_RECORD_FILE_HEADER_1011_v2
			fclose(fp);
			return FALSE;
		}

		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1011_v2_SCH,sizeof(FILE_STEP_PARAM_V1011_v2_SCH)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1011_v2_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1011_v2_SCH, sizeof(FILE_STEP_PARAM_V1011_v2_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_v1011_v2_SCH(&stepData_v1011_v2_SCH); //yulee 201801002
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v1_SCH) //yulee 201801002
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1013_v1_SCH,sizeof(FILE_STEP_PARAM_V1013_v1_SCH)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v1_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1013_v1_SCH, sizeof(FILE_STEP_PARAM_V1013_v1_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_1013_V1_SCHto1013_V2_SCH(&stepData_v1013_v1_SCH); //yulee 201801002
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1014_v1_SCH) //yulee 201801002
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1014_v1_SCH,sizeof(FILE_STEP_PARAM_V1014_v1_SCH)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1014_v1_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1014_v1_SCH, sizeof(FILE_STEP_PARAM_V1014_v1_SCH), 1, fp) < 1)	break;		//기록 실시
				pStep = new CStep; 
				pStep->SetStepData_v1014_v1_SCH(&stepData_v1014_v1_SCH);
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}
	else if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v2_SCH) //ksj 20180528 : SCH 파일에 패턴 포함 처리. //20180627 yulee : 스케쥴 호환
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		int iCntErr = 0; //yulee 20190318
		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1013_v2_SCH,sizeof(FILE_STEP_PARAM_V1013_v2_SCH)); //ksj 20180528
			
			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v2_SCH) //yulee 20180828
			{
				if(fread(&stepData_v1013_v2_SCH, sizeof(FILE_STEP_PARAM_V1013_v2_SCH), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_v1013_v2_SCH(&stepData_v1013_v2_SCH);
				if(stepData_v1013_v2_SCH.chType == 0)//yulee 20190318
					iCntErr++;
				m_apStepArray.Add(pStep);
			}
			
			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}


		/*
		if(iCntErr == (nSize-1)) //yulee 20190318
		{
			ResetData();

			//File Open
			FILE *fp = fopen(strFileName, "rb");
			if(fp == NULL){
				return FALSE;
			}

			//파일 Header 기록 
			PS_FILE_ID_HEADER	fileHeader;
			if(fread(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)				//기록 실시 
			{
				fclose(fp);
				return FALSE;
			}

			//Header 정보 검사
			if(fileHeader.nFileVersion  > SCHEDULE_FILE_VER_v1014_v1_SCH) //yulee 20180828
			{
				fclose(fp);
				AfxMessageBox("nFileVersion Error");
				return FALSE;
			}

			//모델 정보 기록 
			FILE_TEST_INFORMATION modelData;
			if(fread(&modelData, sizeof(modelData), 1, fp) < 1)			//기록 실시 
			{
				fclose(fp);
				return FALSE;
			}
			m_ModelData = modelData;

			//공정 정보 기록 
			FILE_TEST_INFORMATION testData;
			if(fread(&testData, sizeof(testData), 1, fp)  < 1)			//기록 실시 
			{
				fclose(fp);
				return FALSE;
			}
			m_ProcedureData = testData;

			//지원하지 않는 문자를 변경해준다. 예) \ / : * ? " < > |
			CString strScheduleName;
			strScheduleName.Format("%s", testData.szName);
			strScheduleName.Replace('\\', '_');
			strScheduleName.Replace('/', '_');
			strScheduleName.Replace(':', '_');
			strScheduleName.Replace('*', '_');
			strScheduleName.Replace('?', '_');
			strScheduleName.Replace('"', '_');
			strScheduleName.Replace('<', '_');
			strScheduleName.Replace('>', '_');
			strScheduleName.Replace('|', '_');	
			sprintf(m_ProcedureData.szName, "%s", strScheduleName);

			//cell check 조건 기록 
			FILE_CELL_CHECK_PARAM cellCheck;
			if(fread(&cellCheck, sizeof(cellCheck), 1, fp)  < 1)				//기록 실시 
			{
				fclose(fp);
				return FALSE;
			}
			m_CellCheck = cellCheck;

			if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER_1011_PRE), 1, fp)  < 1)	{
				fclose(fp);
				return FALSE;
			}
			int nSize = stepSize.nColumnCount;
			
			int iCntErr = 0; //yulee 20190318
			for (int i =0; i < nSize ; i++)
			{
				ZeroMemory(&stepData_v1013_v2_SCH,sizeof(FILE_STEP_PARAM_V1013_v2_SCH)); //ksj 20180528
				
				if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1013_v2_SCH) //yulee 20180828
				{
					if(fread(&stepData_v1013_v2_SCH, sizeof(FILE_STEP_PARAM_V1013_v2_SCH), 1, fp) < 1)	break;		//기록 실시 
					pStep = new CStep;
					pStep->SetStepData_v1013_v2_SCH(&stepData_v1013_v2_SCH);
					m_apStepArray.Add(pStep);
				}
				
				if(pStep->m_type == PS_STEP_PATTERN)
				{
					iPatternCnt++; //ksj 20180528	
				}
				//cny--------------------------
				if(pStep->m_type == PS_STEP_REST)   
				{
					iPwrSupplyCnt++;//cny 201809
				}
				if( pStep->m_type != PS_STEP_ADV_CYCLE &&
					pStep->m_type != PS_STEP_LOOP &&
					pStep->m_type != PS_STEP_END )
				{
					iCellBALCnt++;
					iChillerCnt++;
				}
			}

		}
		*/
	}
	else // if(fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1015_v1)
	{
		if(fread(&stepSize, sizeof(PS_RECORD_FILE_HEADER), 1, fp)  < 1)	{
			fclose(fp);
			return FALSE;
		}
		int nSize = stepSize.nColumnCount;

		for (int i =0; i < nSize ; i++)
		{
			ZeroMemory(&stepData_v1015_v1,sizeof(FILE_STEP_PARAM_V1015_v1)); //ksj 20180528

			if (fileHeader.nFileVersion == SCHEDULE_FILE_VER_v1015_v1) //yulee 20180828
			{
				if(fread(&stepData_v1015_v1, sizeof(FILE_STEP_PARAM_V1015_v1), 1, fp) < 1)	break;		//기록 실시 
				pStep = new CStep;
				pStep->SetStepData_v1015_v1(&stepData_v1015_v1);
				m_apStepArray.Add(pStep);
			}

			if(pStep->m_type == PS_STEP_PATTERN)
			{
				iPatternCnt++; //ksj 20180528	
			}
			//cny--------------------------
			if(pStep->m_type == PS_STEP_REST)   
			{
				iPwrSupplyCnt++;//cny 201809
			}
			if( pStep->m_type != PS_STEP_ADV_CYCLE &&
				pStep->m_type != PS_STEP_LOOP &&
				pStep->m_type != PS_STEP_END )
			{
				iCellBALCnt++;
				iChillerCnt++;
			}
		}
	}

	//ksj 20180528 //////////////////////////////////////////////////
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(NULL,szBuff,_MAX_PATH);
	CString strPath = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'))+"\\DataBase";
	CString strSavePatternFileName;
	
	for(int iCnt = 0; iCnt<iPatternCnt;iCnt++)
	{
		FILE_SIMULATION_INFO filePattern;
		ZeroMemory(&filePattern,sizeof(FILE_SIMULATION_INFO));
		if(fread(&filePattern, sizeof(FILE_SIMULATION_INFO), 1, fp) < 1)			//기록 실시 
		{
			fclose(fp);
			//return true;
			return FALSE;
		}
		else
		{
			//ljb 20130514 add 파일 있는지 체크 해서 있으면 삭제 할지 물어 본다.
			strTemp.Format("%s",filePattern.szFileName);	
			int nPos = strTemp.ReverseFind('\\');
			strTemp = strTemp.Right(strTemp.GetLength() - (nPos+1));
			strSavePatternFileName.Format("%s\\%s", strPath, strTemp);
			ZeroMemory(filePattern.szFileName,256);
			sprintf(filePattern.szFileName, "%s",strSavePatternFileName);
			
			CFileFind aFind;
			BOOL bFind = aFind.FindFile(filePattern.szFileName);
			BOOL bDeleteFile(TRUE);
			while(bFind)
			{
				bFind = aFind.FindNextFile();
				//strTemp.Format("Pattern File 동일한 파일이 있습니다. 덮어쓰기하겠습니다.");
				strTemp.Format(Fun_FindMsg("ScheduleData_SetSchedule_msg1","SCHEDULEDATA"));
				
				if(bFind == TRUE)	{
					bDeleteFile = TRUE;
					DeleteFile(aFind.GetFilePath());
				}else	{
					bDeleteFile = FALSE; 
				}
				
			}
			
			if (bDeleteFile)
			{
				//create
				FILE *fp2 = fopen(filePattern.szFileName,"wb");
				
				if(fp2 == NULL) //ksj 20200218
				{
					strTemp.Format("패턴 파일 생성 오류 : %s",filePattern.szFileName);
					AfxMessageBox(strTemp);

					return FALSE;
				}
				else
				{
					BYTE* pBuffer = new BYTE[filePattern.iFileSize];
					fread(pBuffer,filePattern.iFileSize,1,fp);	//sch 에서 읽기
					fwrite(pBuffer,filePattern.iFileSize,1,fp2);	//csv로 쓰기
					fclose(fp2);
					pStep = (CStep *)m_apStepArray.GetAt(filePattern.iStepNo);
					pStep->m_strPatternFileName = filePattern.szFileName;
					delete[] pBuffer;
				}				
			}
			else
			{
				//skip
				BYTE* pBuffer = new BYTE[filePattern.iFileSize];
				fread(pBuffer,filePattern.iFileSize,1,fp);	//sch 에서 읽기
				pStep = (CStep *)m_apStepArray.GetAt(filePattern.iStepNo);
				pStep->m_strPatternFileName = filePattern.szFileName;
				delete[] pBuffer;
			}
		}
	}
	//ksj end ///////////////////////////////////////////////////////


	//cny Cell Balance start///////////////////////////////////////////////////////
	if(iCellBALCnt>0)
	{
		for(int iCnt = 0; iCnt<iCellBALCnt;iCnt++)
		{
			FILE_CELLBAL_INFO fileCellBAL;
			ZeroMemory(&fileCellBAL,sizeof(FILE_CELLBAL_INFO));

			if((fread(&fileCellBAL, sizeof(FILE_CELLBAL_INFO), 1, fp) < 1) && (fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH))
			{
				fclose(fp);
				iCellBALCntNum++;//20190318
				//	return FALSE;
			}
			else
			{
				if (fileCellBAL.nStepIndex != 0)
				{
					pStep = (CStep *)m_apStepArray.GetAt(fileCellBAL.nStepIndex);
					if(!pStep) continue;

					if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v1011_v2_SCH)
					{
						pStep->m_nCellBal_CircuitRes     = 0;
						pStep->m_nCellBal_WorkVoltUpper  = 0;
						pStep->m_nCellBal_WorkVolt       = 0;
						pStep->m_nCellBal_StartVolt      = 0;
						pStep->m_nCellBal_EndVolt        = 0;
						pStep->m_nCellBal_ActiveTime     = 0;
						pStep->m_nCellBal_AutoStopTime   = 0;
						pStep->m_nCellBal_AutoNextStep   = 0;
						//yulee 20190212 CellBalance Info Add
						//Fun_SaveCellBALFileToSch(fp, pStep);
					}
					else if(fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH)
					{
						pStep->m_nCellBal_CircuitRes     = fileCellBAL.nCellBal_CircuitRes;
						pStep->m_nCellBal_WorkVoltUpper  = fileCellBAL.nCellBal_WorkVoltUpper;
						pStep->m_nCellBal_WorkVolt       = fileCellBAL.nCellBal_WorkVolt;
						pStep->m_nCellBal_StartVolt      = fileCellBAL.nCellBal_StartVolt;
						pStep->m_nCellBal_EndVolt        = fileCellBAL.nCellBal_EndVolt;
						pStep->m_nCellBal_ActiveTime     = fileCellBAL.nCellBal_ActiveTime;
						pStep->m_nCellBal_AutoStopTime   = fileCellBAL.nCellBal_AutoStopTime;
						pStep->m_nCellBal_AutoNextStep   = fileCellBAL.nCellBal_AutoNextStep;
					}
				}
			}
		}
	}
	//cny Power Supply start///////////////////////////////////////////////////////
	if(iPwrSupplyCnt>0)
	{
		for(int iCnt = 0; iCnt<iPwrSupplyCnt;iCnt++)
		{
			FILE_PWRSUPPLY_INFO filePwrSupply;
			ZeroMemory(&filePwrSupply,sizeof(FILE_PWRSUPPLY_INFO));
			if((fread(&filePwrSupply, sizeof(FILE_PWRSUPPLY_INFO), 1, fp) < 1) && (fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH))
			{
				fclose(fp);
			iPwrSupplyCntNum++;//20190318
			//	return FALSE;
			}
			else
			{
				if (filePwrSupply.nStepIndex != 0)
				{
					pStep = (CStep *)m_apStepArray.GetAt(filePwrSupply.nStepIndex);
					if(!pStep) continue;

					if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v1011_v2_SCH)
					{
						pStep->m_nPwrSupply_Cmd  =0;
						pStep->m_nPwrSupply_Volt =0;
					}
					else if(fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH)
					{
						pStep->m_nPwrSupply_Cmd  =filePwrSupply.nPwrSupply_Cmd;
						pStep->m_nPwrSupply_Volt =filePwrSupply.nPwrSupply_Volt;
					}	
				}
			}
		}
	}
	//cny Chiller start///////////////////////////////////////////////////////
	if(iChillerCnt>0)
	{
		for(int iCnt = 0; iCnt<iChillerCnt;iCnt++)
		{
			FILE_CHILLER_INFO fileChiller;
			ZeroMemory(&fileChiller,sizeof(FILE_CHILLER_INFO));
			if((fread(&fileChiller, sizeof(FILE_CHILLER_INFO), 1, fp) < 1) && (fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH))
			{
				fclose(fp);
				iChillerCntNum++;
				//return FALSE;
			}
			else
			{
				if (fileChiller.nStepIndex != 0)
				{
					pStep = (CStep *)m_apStepArray.GetAt(fileChiller.nStepIndex);
					if(!pStep) continue;

					if(fileHeader.nFileVersion < SCHEDULE_FILE_VER_v1011_v2_SCH)
					{
						pStep->m_nChiller_Cmd          =0;
						pStep->m_fChiller_RefTemp      =0;
						pStep->m_nChiller_RefCmd       =0;
						pStep->m_nChiller_PumpCmd      =0;
						pStep->m_fChiller_RefPump      =0;
						pStep->m_nChiller_TpData       =0;
						pStep->m_fChiller_ONTemp1      =0;
						pStep->m_nChiller_ONTemp1_Cmd  =0;
						pStep->m_fChiller_ONTemp2      =0;
						pStep->m_nChiller_ONTemp2_Cmd  =0;
						pStep->m_fChiller_OFFTemp1     =0;
						pStep->m_nChiller_OFFTemp1_Cmd =0;
						pStep->m_fChiller_OFFTemp2     =0;
						pStep->m_nChiller_OFFTemp2_Cmd =0; 
						//yulee 20190212 Chiller Info Add
						//Fun_SaveChillerFileToSch(fp, pStep);
					}
					else if(fileHeader.nFileVersion >= SCHEDULE_FILE_VER_v1011_v2_SCH)
					{
						pStep->m_nChiller_Cmd          =fileChiller.nChiller_Cmd;
						pStep->m_fChiller_RefTemp      =fileChiller.fChiller_RefTemp;
						pStep->m_nChiller_RefCmd       =fileChiller.nChiller_RefCmd;
						pStep->m_nChiller_PumpCmd      =fileChiller.nChiller_PumpCmd;
						pStep->m_fChiller_RefPump      =fileChiller.fChiller_RefPump;
						pStep->m_nChiller_TpData       =fileChiller.nChiller_TpData;
						pStep->m_fChiller_ONTemp1      =fileChiller.fChiller_ONTemp1;
						pStep->m_nChiller_ONTemp1_Cmd  =fileChiller.nChiller_ONTemp1_Cmd;
						pStep->m_fChiller_ONTemp2      =fileChiller.fChiller_ONTemp2;
						pStep->m_nChiller_ONTemp2_Cmd  =fileChiller.nChiller_ONTemp2_Cmd;
						pStep->m_fChiller_OFFTemp1     =fileChiller.fChiller_OFFTemp1;
						pStep->m_nChiller_OFFTemp1_Cmd =fileChiller.nChiller_OFFTemp1_Cmd;
						pStep->m_fChiller_OFFTemp2     =fileChiller.fChiller_OFFTemp2;
						pStep->m_nChiller_OFFTemp2_Cmd =fileChiller.nChiller_OFFTemp2_Cmd;    
					}	
				}
			}
		}
	}

	int iSchTotalNum, iCntTotalNum;
	iCntTotalNum = iCellBALCntNum+iChillerCntNum+iPwrSupplyCntNum;
	iSchTotalNum = iCellBALCnt+iChillerCnt+iPwrSupplyCnt;

	if(iCntTotalNum == iSchTotalNum)
	{
		fclose(fp);
		return TRUE;
	}

	fclose(fp);
	return TRUE;
}


// simple공정 조건을 File로 저장한다. -> 그래프 그리기 위해서
// BOOL CScheduleData::SaveToFile(CString strFileName, CScheduleData *pSchedule)
// {
// 	CString strTemp;
// 	if(strFileName.IsEmpty())	return FALSE;
// 	
// 	FILE *fp = fopen(strFileName, "wb");
// 	if(fp == NULL){
// 		return FALSE;
// 	}
// 	
// 	//파일 Header 기록 
// 	PS_FILE_ID_HEADER	FileHeader;
// 	ZeroMemory(&FileHeader, sizeof(PS_FILE_ID_HEADER));
// 	
// 	COleDateTime dateTime = COleDateTime::GetCurrentTime();
// 	sprintf(FileHeader.szCreateDateTime, "%s", dateTime.Format());			//기록 시간
// 	FileHeader.nFileID = SCHEDULE_FILE_ID;									//파일 ID
// 	FileHeader.nFileVersion = SCHEDULE_FILE_VER_v100B;							//파일 Version
// 	sprintf(FileHeader.szDescrition, "PNE power supply schedule file.");	//파일 서명 
// 	fwrite(&FileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);					//기록 실시 
// 	
// 	TRACE("FileHeader Size %d\n", sizeof(FileHeader));
// 	
// 	//모델 정보 기록 
// 	fwrite(&m_ModelData, sizeof(m_ModelData), 1, fp);				//기록 실시 
// 	
// 	TRACE("m_ModelData Size %d\n",  sizeof(FileHeader));
// 	
// 	//공정 정보 기록 
// 	fwrite(&m_ProcedureData, sizeof(m_ProcedureData), 1, fp);		//기록 실시 
// 	
// 	TRACE("m_ProcedureData Size %d\n", sizeof(FileHeader));
// 	
// 	//cell check 조건 기록 
// 	fwrite(&m_CellCheck, sizeof(m_CellCheck), 1, fp);				//기록 실시 
// 	TRACE("m_CellCheck Size %d\n", sizeof(m_CellCheck));
// 	
// 	//step 정보 기록 
// 	FILE_STEP_PARAM_V100B fileStep;
// 	
// 	CStep *pStep;
// 	int nWrite = 0;
// 	for(int i = 0; i< m_apStepArray.GetSize(); i++)
// 	{
// 		pStep = (CStep *)m_apStepArray.GetAt(i);
// 		fileStep = pStep->GetFileStep();
// 		nWrite += fwrite(&fileStep, sizeof(FILE_STEP_PARAM_V100B), 1, fp);					//기록 실시 
// 		// 		TRACE("%d,%d,%d,%d,%d,%d,%f,%f,%f,%f\n", fileStep.bGrade, fileStep.bValueItem, fileStep.bValueStepNo, fileStep.chMode,
// 		// 			fileStep.chStepNo, fileStep.chType, fileStep.fCapaVoltage1, fileStep.fCapaVoltage2, fileStep.fCLimitHigh,
// 		// 			fileStep.fCLimitLow );
// 		
// 	}
// 	
// 	fclose(fp);
// 	return TRUE;
// }



//공정 조건을 File로 저장한다.
BOOL CScheduleData::SaveToFile(CString strFileName, CScheduleData *pSchedule)
{
	CString strTemp;
	if(strFileName.IsEmpty())	return FALSE;

	//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0);
	UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
	if (SchFileVer == SCHEDULE_FILE_VER_v1014_v1_SCH)
	{
		CStep *pStep = new CStep;
		pStep->SaveStep_v1014_v1_SCH(strFileName, (void *)pSchedule, (void *)this);
		delete pStep;
	}
	else if (SchFileVer == SCHEDULE_FILE_VER_v1013_v1_SCH)
	{
		CStep *pStep = new CStep;
		pStep->SaveStep_v1013_v1_SCHto1013_V2_SCH(strFileName, (void *)pSchedule, (void *)this);
		delete pStep;
	}
	else if (SchFileVer == SCHEDULE_FILE_VER_v1011_v2_SCH)
	{
		CStep *pStep = new CStep;
		pStep->SaveStep_v1011_v2_SCH(strFileName, (void *)pSchedule, (void *)this);
		delete pStep;
	}
	else if (SchFileVer == SCHEDULE_FILE_VER_v100F_v2_SCH)
	{
		CStep *pStep = new CStep;
		pStep->SaveStep_vF_V2_SCHto1013_V1_SCH(strFileName, (void *)pSchedule, (void *)this);
		delete pStep;
	}
	else //(SchFileVer == SCHEDULE_FILE_VER_v1015_v1)
	{
		CStep *pStep = new CStep;
		pStep->SaveStep_v1015_v1(strFileName, (void *)pSchedule, (void *)this);
		delete pStep;
	}


	return TRUE;
}

//ksj 20200226 : 공정 조건을 File로 저장한다.
BOOL CScheduleData::SaveToFile_v1015(CString strFileName, CScheduleData *pSchedule)
{
	CString strTemp;
	if(strFileName.IsEmpty())	return FALSE;

	CStep *pStep = new CStep;
	pStep->SaveStep_v1015_v1(strFileName, (void *)pSchedule, (void *)this);
	delete pStep;

	return TRUE;
}

//ksj 20200226 : v1013 공정 조건을 File로 저장한다.
BOOL CScheduleData::SaveToFile_v1013(CString strFileName, CScheduleData *pSchedule)
{
	CString strTemp;
	if(strFileName.IsEmpty())	return FALSE;

	CStep *pStep = new CStep;
	//pStep->SaveStep_v1013_v1_SCHto1013_V2_SCH(strFileName, (void *)pSchedule, (void *)this);
	pStep->SaveStep_v1013(strFileName, (void *)pSchedule, (void *)this); //ksj 20210114
	
	delete pStep;
	return TRUE;
}

#if 0
BOOL CScheduleData::Fun_SaveCellBALFileToSch(FILE *pFile,CStep *pStep)
{
	FILE_CELLBAL_INFO fileCellBAL;
	ZeroMemory(&fileCellBAL,sizeof(FILE_CELLBAL_INFO));
	fileCellBAL.nStepIndex              = pStep->m_StepIndex;
	fileCellBAL.nCellBal_CircuitRes     = pStep->m_nCellBal_CircuitRes;
	fileCellBAL.nCellBal_WorkVoltUpper  = pStep->m_nCellBal_WorkVoltUpper;
	fileCellBAL.nCellBal_WorkVolt       = pStep->m_nCellBal_WorkVolt;
	fileCellBAL.nCellBal_StartVolt      = pStep->m_nCellBal_StartVolt;
	fileCellBAL.nCellBal_EndVolt        = pStep->m_nCellBal_EndVolt;
	fileCellBAL.nCellBal_ActiveTime	    = pStep->m_nCellBal_ActiveTime;
	fileCellBAL.nCellBal_AutoStopTime   = pStep->m_nCellBal_AutoStopTime;
	fileCellBAL.nCellBal_AutoNextStep   = pStep->m_nCellBal_AutoNextStep;
	
	fwrite(&fileCellBAL,sizeof(FILE_CELLBAL_INFO),1,pFile);
	return TRUE;
}

BOOL CScheduleData::Fun_SavePwrSupplyFileToSch(FILE *pFile,CStep *pStep)
{
	FILE_PWRSUPPLY_INFO filePwrSupply;
	ZeroMemory(&filePwrSupply,sizeof(FILE_PWRSUPPLY_INFO));
	filePwrSupply.nStepIndex      = pStep->m_StepIndex;
	filePwrSupply.nPwrSupply_Cmd  = pStep->m_nPwrSupply_Cmd;
	filePwrSupply.nPwrSupply_Volt = pStep->m_nPwrSupply_Volt;
	
	fwrite(&filePwrSupply,sizeof(FILE_PWRSUPPLY_INFO),1,pFile);
	return TRUE;
}

BOOL CScheduleData::Fun_SaveChillerFileToSch(FILE *pFile,CStep *pStep)
{
	FILE_CHILLER_INFO fileChiller;
	ZeroMemory(&fileChiller,sizeof(FILE_CHILLER_INFO));
	fileChiller.nStepIndex           =pStep->m_StepIndex;
	fileChiller.nChiller_Cmd         =pStep->m_nChiller_Cmd;
	fileChiller.fChiller_RefTemp     =pStep->m_fChiller_RefTemp;
	fileChiller.nChiller_RefCmd      =pStep->m_nChiller_RefCmd;
	fileChiller.fChiller_RefPump     =pStep->m_fChiller_RefPump;
	fileChiller.nChiller_PumpCmd     =pStep->m_nChiller_PumpCmd;
	fileChiller.nChiller_TpData      =pStep->m_nChiller_TpData;
	fileChiller.fChiller_ONTemp1     =pStep->m_fChiller_ONTemp1;
	fileChiller.nChiller_ONTemp1_Cmd =pStep->m_nChiller_ONTemp1_Cmd;
	fileChiller.nChiller_ONPump1_Cmd =pStep->m_nChiller_ONPump1_Cmd;
	fileChiller.fChiller_ONTemp2     =pStep->m_fChiller_ONTemp2;
	fileChiller.nChiller_ONTemp2_Cmd =pStep->m_nChiller_ONTemp2_Cmd;
	fileChiller.nChiller_ONPump2_Cmd =pStep->m_nChiller_ONPump2_Cmd;
	fileChiller.fChiller_OFFTemp1    =pStep->m_fChiller_OFFTemp1;
	fileChiller.nChiller_OFFTemp1_Cmd=pStep->m_nChiller_OFFTemp1_Cmd;
	fileChiller.nChiller_OFFPump1_Cmd=pStep->m_nChiller_OFFPump1_Cmd;
	fileChiller.fChiller_OFFTemp2    =pStep->m_fChiller_OFFTemp2;
	fileChiller.nChiller_OFFTemp2_Cmd=pStep->m_nChiller_OFFTemp2_Cmd;
	fileChiller.nChiller_OFFPump2_Cmd=pStep->m_nChiller_OFFPump2_Cmd;
	
	fwrite(&fileChiller,sizeof(FILE_CHILLER_INFO),1,pFile);
	return TRUE;
}

//ksj 20180528 : 스케쥴 파일에 패턴 저장. (울산 SDI 고속형 Cycler 발췌)
BOOL CScheduleData::Fun_SavePatternFileToSch(FILE *pFile,int nStepNo, CString strFileName)
{
	FILE_SIMULATION_INFO filePattern;
	ZeroMemory(&filePattern,sizeof(FILE_SIMULATION_INFO));
	filePattern.iStepNo = nStepNo;
	sprintf(filePattern.szFileName, "%s", strFileName);
	
	//ljb 20130514 메모리 맵을 이용한 파일정보 가져 오기
	HANDLE hFile, hFileMap;
	DWORD dwFileSize;
	//DWORD dwCnt;
	char *szbuff = NULL;
	
	// 읽기 전용으로 lpszPathName인 파일을 읽는다.
	hFile = CreateFile((LPSTR)(LPCSTR)strFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if( hFile == INVALID_HANDLE_VALUE )
	{
		//AfxMessageBox("File Handle invalid value. Failed file open\n", MB_OK | MB_ICONERROR);
		AfxMessageBox(Fun_FindMsg("ScheduleData_Fun_SavePatternFileToSch_msg1","SCHEDULEDATA"), MB_OK | MB_ICONERROR);//&&
		return FALSE;
	}
	
	filePattern.iFileSize = dwFileSize = GetFileSize(hFile, NULL);
	
	// dwFileSize 만큼 메모리에 쓴다.
	hFileMap = CreateFileMapping(hFile, NULL, PAGE_WRITECOPY, 0, dwFileSize, NULL);
	if( hFileMap == NULL )
	{
		//AfxMessageBox("FileMap Handle is NULL. Failed file open\n", MB_OK | MB_ICONERROR );
		AfxMessageBox(Fun_FindMsg("ScheduleData_Fun_SavePatternFileToSch_msg2","SCHEDULEDATA"), MB_OK | MB_ICONERROR );//&&
		CloseHandle(hFile);
		return FALSE;
	}
	
	szbuff = (char *)MapViewOfFile(hFileMap, FILE_MAP_COPY, 0, 0, 0);
	
	fwrite(&filePattern,sizeof(FILE_SIMULATION_INFO),1,pFile); //lmh 201109 패턴 정보를 먼저 저장하고 파일을 세이브한다.
	fwrite(szbuff,filePattern.iFileSize,1,pFile);
	
	CloseHandle(hFileMap);
	CloseHandle(hFile);
	return TRUE;
}
#endif

//////////////////////////////////////////////////////////////////////////
// + BW KIM 2014.02.21
//공정 조건을 File로 저장한다.
BOOL CScheduleData::SaveToFile_ForFTP(CString strFileName, CScheduleData *pSchedule)
{
	CString strTemp;
	if(strFileName.IsEmpty())	return FALSE;
	
	FILE *fp = fopen(strFileName, "wb");
	if(fp == NULL){
		return FALSE;
	}
	
	fclose(fp);
	return TRUE;
}
// - 
//////////////////////////////////////////////////////////////////////////
UINT CScheduleData::GetStepSize()
{
	return m_apStepArray.GetSize();
}

void CScheduleData::ResetData()
{
	memset(&m_ModelData, 0, sizeof(m_ModelData));
	memset(&m_ProcedureData, 0, sizeof(m_ProcedureData));
	memset(&m_CellCheck, 0, sizeof(m_CellCheck));

	CStep *pStep;
	for(int i = m_apStepArray.GetSize()-1; i>=0; i--)
	{
		pStep = (CStep *)m_apStepArray[i];
		delete pStep;
		pStep = NULL;
		m_apStepArray.RemoveAt(i);
	}
	m_apStepArray.RemoveAll();

	m_bContinueCellCode = TRUE;
}

//Load Schedule Data from database
BOOL CScheduleData::SetSchedule(CString strDBName, long lModelPK, long lTestPK)
{
	CDaoDatabase  db;
	if(strDBName.IsEmpty())		return 0;

	try
	{
		db.Open(strDBName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	ResetData();
	
	if(LoadModelInfo(db, lModelPK) == FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadProcedureInfo(db, lModelPK, lTestPK)== FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadCellCheckInfo(db, lTestPK) == FALSE)
	{
		db.Close();
		return FALSE;
	}

	if(LoadStepInfo(db, lTestPK) == FALSE)
	{
		db.Close();
		
		return FALSE;
	}

	db.Close();
	return TRUE;
}

BOOL CScheduleData::LoadModelInfo(CDaoDatabase &db, long lPK)
{
	ASSERT(db.IsOpen());

	CString strSQL;
	strSQL.Format("SELECT No, ModelName, Description, CreatedTime FROM BatteryModel WHERE ModelID = %d ORDER BY No", lPK);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}
	
	ZeroMemory(&m_ModelData, sizeof(m_ModelData));

	//No
	data = rs.GetFieldValue(0);
	m_ModelData.lID = data.lVal;
	//ModelName
	data = rs.GetFieldValue(1);
	sprintf(m_ModelData.szName, "%s", data.pcVal);
	//Description
	data = rs.GetFieldValue(2);
	if(VT_NULL != data.vt)
	{
		sprintf(m_ModelData.szDescription, "%s", data.pcVal);
	}
	//DateTime
	data = rs.GetFieldValue(3);
	COleDateTime  date = data;
	sprintf(m_ModelData.szModifiedTime, "%s", date.Format());

	rs.Close();

	return TRUE;
}

BOOL CScheduleData::LoadProcedureInfo(CDaoDatabase &db, long lModelPK, long lProcPK)
{
	ASSERT(db.IsOpen());

	CString strSQL;
	strSQL.Format("SELECT TestNo, ProcTypeID, TestName, Description, Creator, ModifiedTime FROM TestName WHERE ModelID =  %d AND TestID = %d ORDER BY TestNo", 
					lModelPK, lProcPK);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}
	
	ZeroMemory(&m_ProcedureData, sizeof(m_ProcedureData));
	
	//TestNo
	data = rs.GetFieldValue(0);
	m_ProcedureData.lID = data.lVal;
	//ProcTypeID
	data = rs.GetFieldValue(1);
	m_ProcedureData.lType = data.lVal;
	//TestName
	data = rs.GetFieldValue(2);

	//지원하지 않는 문자를 변경해준다. 예) \ / : * ? " < > |
	CString strScheduleName;
	strScheduleName.Format("%s", data.pcVal);
	strScheduleName.Replace('\\', '_');
	strScheduleName.Replace('/', '_');
	strScheduleName.Replace(':', '_');
	strScheduleName.Replace('*', '_');
	strScheduleName.Replace('?', '_');
	strScheduleName.Replace('"', '_');
	strScheduleName.Replace('<', '_');
	strScheduleName.Replace('>', '_');
	strScheduleName.Replace('|', '_');	
	sprintf(m_ProcedureData.szName, "%s", strScheduleName);

	//Description
	data = rs.GetFieldValue(3);
	if(VT_NULL != data.vt)
	{
		sprintf(m_ProcedureData.szDescription, "%s", data.pcVal);
	}
	//Creator
	data = rs.GetFieldValue(4);
	if(VT_NULL != data.vt)
	{
		sprintf(m_ProcedureData.szCreator, "%s", data.pcVal);		
	}
	//DateTime
	data = rs.GetFieldValue(5);
	COleDateTime  date = data;
	sprintf(m_ProcedureData.szModifiedTime, "%s", date.Format());

	rs.Close();

	return TRUE;
}

BOOL CScheduleData::LoadCellCheckInfo(CDaoDatabase &db, long lProcPK)
{
	ASSERT(db.IsOpen());
	
	CString strQuery;
	
//	CString strFrom;
	char szData[255];

	int count = 0;
	char seps[] = ",";
	char *token;
	char szColl[20];	
	
//	strSQL = "SELECT *";
	//v100A
//	strSQL += " MaxV, MinV, CurrentRange, OCVLimit, TrickleCurrent, TrickleTime, DeltaVoltage, MaxFaultBattery, AutoTime, AutoProYN, PreTestCheck";
	//v100A
	//strSQL += " MaxV, MinV, CurrentRange, OCVLimit, TrickleCurrent, TrickleTime, DeltaVoltage, MaxFaultBattery, AutoTime, AutoProYN, PreTestCheck, CanCategory, CanCompare, CanValue, AuxCategory, AuxCompare, AuxValue";
//	strFrom.Format(" FROM Check WHERE TestID = %d ORDER BY CheckID", lProcPK);
	
//	strQuery = strSQL+strFrom;
	strQuery.Format("SELECT * FROM Check WHERE TestID = %d ORDER BY CheckID", lProcPK);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strQuery, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	if(!rs.IsEOF() && !rs.IsBOF())
	{
		memset(&m_CellCheck, 0, sizeof(m_CellCheck));
		rs.GetFieldValue("SafetyMaxV",data);		//long	MaxV;	
		if(VT_NULL != data.vt) m_CellCheck.fMaxV = data.fltVal;	//ljb 20151012 data.fltVal -> (float)data.lVal
		//else return FALSE;
		rs.GetFieldValue("SafetyMinV",data);		//long	MinV;
		if(VT_NULL != data.vt) m_CellCheck.fMinV = data.fltVal;	//ljb 20151012 data.fltVal -> (float)data.lVal
		//else return FALSE;
		rs.GetFieldValue("SafetyMaxI",data);		//long	MaxI;	
		if(VT_NULL != data.vt) m_CellCheck.fMaxI = data.fltVal;	//ljb 20151012 data.fltVal -> (float)data.lVal
		//else return FALSE;
		rs.GetFieldValue("SafetyMinI",data);		//long	MinI;
		if(VT_NULL != data.vt) m_CellCheck.fCellDeltaV = data.fltVal;	//ljb 20151012 data.fltVal -> (float)data.lVal
		//else return FALSE;
		rs.GetFieldValue("SafetyMaxT",data);		//long	MaxT;	
		if(VT_NULL != data.vt) m_CellCheck.fMaxT = data.fltVal;	//ljb 20151012 data.fltVal -> (float)data.lVal
		//else return FALSE;
		rs.GetFieldValue("SafetyMinT",data);		//long	MinT;
		if(VT_NULL != data.vt) m_CellCheck.fMinT = data.fltVal;	//ljb 20151012 data.fltVal -> (float)data.lVal
		//else return FALSE;
		rs.GetFieldValue("SafetyMaxC",data);		//long	MaxC;	
		if(VT_NULL != data.vt) m_CellCheck.fMaxC = data.fltVal;	//ljb 20151012 data.fltVal -> (float)data.lVal
		//else return FALSE;
		rs.GetFieldValue("SafetyMaxW",data);		//long	MaxW;
		if(VT_NULL != data.vt) m_CellCheck.lMaxW = data.lVal;
		//else return FALSE;
		rs.GetFieldValue("SafetyMaxWh",data);		//long	MaxWh;	
		if(VT_NULL != data.vt) m_CellCheck.lMaxWh = data.lVal;
		//else return FALSE;

// 		rs.GetFieldValue("MaxV",data);		//long	MaxV;	
// 		if(VT_NULL != data.vt) m_CellCheck.fMaxVoltage = data.fltVal;
// 		else return FALSE;
// 		rs.GetFieldValue("MinV",data);		//long	MinV;
// 		if(VT_NULL != data.vt) m_CellCheck.fMinVoltage = data.fltVal;
// 		else return FALSE;
// 		rs.GetFieldValue("CurrentRange",data);		//long	CurrentRange;
// 		if(VT_NULL != data.vt) m_CellCheck.fMaxCurrent = data.fltVal;
// 		else return FALSE;
// 		rs.GetFieldValue("OCVLimit",data);		//long	OCVLimit;
// 		if(VT_NULL != data.vt) m_CellCheck.fVrefVal = data.fltVal;
// 		else return FALSE;
// 		rs.GetFieldValue("TrickleCurrent",data);		//long	TrickleCurrent;
// 		if(VT_NULL != data.vt) m_CellCheck.fIrefVal = data.fltVal;
// 		else return FALSE;
// 		rs.GetFieldValue("TrickleTime",data);		//float	TrickleTime;
// 		if(VT_NULL != data.vt) m_CellCheck.lTrickleTime = data.lVal;
// 		else return FALSE;
// 		rs.GetFieldValue("DeltaVoltage",data);		//float	DeltaVoltage;
// 		if(VT_NULL != data.vt) m_CellCheck.fDeltaVoltage = data.fltVal;
// 		else return FALSE;
// 		rs.GetFieldValue("MaxFaultBattery",data);		//long	MaxFaultBattery;
// 		if(VT_NULL != data.vt) m_CellCheck.nMaxFaultNo = data.lVal;
// 		else return FALSE;
// // 		rs.GetFieldValue("AutoTime",data);		//float	AutoTime;
// // 		if(VT_NULL != data.vt) {}
// // 		else return FALSE;
// // 		rs.GetFieldValue("AutoProYN",data);		//float	AutoProYN;
// // 		if(VT_NULL != data.vt) {}
// // 		else return FALSE;
// 		rs.GetFieldValue("PreTestCheck",data);	//float	PreTestCheck;
// 		//?? DB값이 참이면 255가 들어 있음
// 		//?? 거짓이면 0가 들어 있음
// 		if(VT_NULL != data.vt) m_CellCheck.bPreTest = (BYTE)data.bVal == 0 ? FALSE : TRUE;
// 		else m_CellCheck.bPreTest = FALSE;

		ZeroMemory(szData,255);
		rs.GetFieldValue("CanCategory",data);		// Can Funtion_divition	
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.ican_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}

		ZeroMemory(szData,255);				
		rs.GetFieldValue("CanCompare",data);		// Can Compare Type		
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.cCan_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}

		ZeroMemory(szData,255);				
		rs.GetFieldValue("CanDataType",data);		// Can Compare Type		
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.cCan_data_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}

		ZeroMemory(szData,255);
		rs.GetFieldValue("CanValue",data);			// Can Value
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.fcan_Value[count] = atof(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}

		ZeroMemory(szData,255);
		rs.GetFieldValue("AuxCategory",data);		// Aan Funtion_divition	
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.iaux_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}

		ZeroMemory(szData,255);				
		rs.GetFieldValue("AuxCompare",data);		// Aan Compare Type		
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.cAux_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}
		
		ZeroMemory(szData,255);				
		rs.GetFieldValue("AuxDataType",data);		// Aan Compare Type		
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.cAux_data_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}

		ZeroMemory(szData,255);
		rs.GetFieldValue("AuxValue",data);			// Aan Value
		if(VT_NULL != data.vt){
			strcpy(szData, data.pcVal);
			count = 0;
			token = strtok( szData, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				m_CellCheck.faux_Value[count] = atof(szColl);
				count++;
				token = strtok(NULL, seps );
			}		
		}

		rs.GetFieldValue("STDDeltaVoltageMax",data);		
		if(VT_NULL != data.vt) 
			m_CellCheck.fSTDCellDeltaMaxV = data.dblVal;

		rs.GetFieldValue("STDDeltaVoltageMin",data);		
		if(VT_NULL != data.vt) 
			m_CellCheck.fSTDCellDeltaMinV = data.dblVal;

		rs.GetFieldValue("DeltaVoltageMax",data);		
		if(VT_NULL != data.vt) 
			m_CellCheck.fCellDeltaMaxV = data.dblVal;

		rs.GetFieldValue("DeltaVoltageMin",data);		
		if(VT_NULL != data.vt) 
			m_CellCheck.fCellDeltaMinV = data.dblVal;

		rs.GetFieldValue("DeltaVoltageVent",data);
		if((BYTE)data.bVal == 0)
			m_CellCheck.bfaultcompAuxV_Vent_Flag = FALSE;
		else
			m_CellCheck.bfaultcompAuxV_Vent_Flag = TRUE;
	}
	else
	{
		rs.Close();
		return FALSE;
	}
	rs.Close();
	
	return TRUE;
}

BOOL CScheduleData::LoadStepInfo(CDaoDatabase &db, long lProcPK)
{
	ASSERT(db.IsOpen());
	
	CString strQuery;
	CString strTemp,strWord;
	CString strPatternFileName;
	CString strUserMapFileName;
	int iTempGoto=0;
	//strSQL = "SELECT ";
	//strSQL += "StepID, StepNo, StepProcType, StepType, StepMode, Vref, Iref, EndTime, EndV, EndI, EndCapacity, End_dV, End_dI, CycleCount ";	//14
	//strSQL += ", OverV, LimitV, OverI, LimitI, OverCapacity, LimitCapacity, OverImpedance, LimitImpedance, DeltaTime, DeltaTime1, DeltaV, DeltaI ";	//12
	//strSQL += ", Grade, CompTimeV1, CompTimeV2, CompTimeV3,	CompVLow1,	CompVLow2, CompVLow3, CompVHigh1, CompVHigh2, CompVHigh3 ";	//10
	//strSQL += ", CompTimeI1, CompTimeI2, CompTimeI3, CompILow1, CompILow2, CompILow3, CompIHigh1, CompIHigh2, CompIHigh3, RecordTime, RecordDeltaV, RecordDeltaI ";	//12
	//strSQL += ", CapVLow, CapVHigh ";	//2
	//strSQL += ", Value0, Value1, Value2, Value3, Value4, Value5, Value6, Value7, Value8, Value9 "	//10
	//strSQL += ", CanCategory, CanCompare, CanValue, CanGoto, AuxCategory, AuxCompare, AuxValue, AuxGoto ";	//8

	//strFrom.Format(" FROM Step WHERE TestID = %d ORDER BY StepNo", lProcPK);
	//strQuery = strSQL+strFrom;
	strQuery.Format("SELECT * FROM Step WHERE TestID = %d ORDER BY StepNo", lProcPK);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strQuery, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CStep *pStepData;
	int  iTemp1, iTemp2, iTemp3, iTemp4, iTemp5, iTemp6, iTemp7, iTemp8, iTemp9, iTemp10, iTemp11, iTemp12;
	float fTemp1,fTemp2,fTemp3,fTemp4;
	//flaot fTemp5,fTemp6,fTemp7,fTemp8,fTemp9;
	char szTemp[255];

	int count = 0;
	char seps1[] = " ";
	char seps2[] = ",";
	char *token;
	char szColl[20];	

	while(!rs.IsEOF())
	{
		pStepData = new CStep;
		ASSERT(pStepData);
		rs.GetFieldValue("StepID",data);		//long	m_StepID;	
		pStepData->m_StepID = data.lVal;
		rs.GetFieldValue("StepNo",data);		//long	m_StepNo;
		pStepData->m_StepIndex = BYTE(data.lVal-1);
		rs.GetFieldValue("StepProcType",data);		//long	m_StepProcType;
		pStepData->m_lProcType = data.lVal;
		rs.GetFieldValue("StepType",data);		//long	m_StepType;
		pStepData->m_type = (BYTE)data.lVal;
		rs.GetFieldValue("StepMode",data);		//long	m_StepMode;
		if(data.lVal < 0)	pStepData->m_mode = 0;
		else				pStepData->m_mode = (BYTE)data.lVal;
		rs.GetFieldValue("Vref",data);		//float	m_Vref;
		pStepData->m_fVref_Charge = data.fltVal;		//ljb

		//ljb 2011315 이재복 //////////
		rs.GetFieldValue("Vref_DisCharge",data);		//ljb 20101130 for v100B 방전 전압
		if(VT_NULL != data.vt) pStepData->m_fVref_DisCharge = data.fltVal;
		else pStepData->m_fVref_DisCharge = 0;

		rs.GetFieldValue("Iref",data);		//float	m_Iref;
		pStepData->m_fIref = data.fltVal;

		//ljb 2011315 이재복 //////////
		rs.GetFieldValue("Pref",data);		//ljb 20100820 for v100A
		if(VT_NULL != data.vt) pStepData->m_fPref = data.fltVal;
		else pStepData->m_fPref = 0;
			
		//ljb 2011315 이재복 //////////
		rs.GetFieldValue("Rref",data);		//ljb 20101130 for v100B
		if(VT_NULL != data.vt) pStepData->m_fRref = data.fltVal;
		else pStepData->m_fRref = 0;

		rs.GetFieldValue("EndTime",data);		//long	m_EndTime;
		pStepData->m_fEndTime = PCTIME(data.lVal);
		rs.GetFieldValue("EndV",data);		//float	m_EndV;
		pStepData->m_fEndV_H = data.fltVal;
		rs.GetFieldValue("EndI",data);		//float	m_EndI;
		pStepData->m_fEndI = data.fltVal;
		rs.GetFieldValue("EndCapacity",data);	//float	m_EndCapacity;
		pStepData->m_fEndC = data.fltVal;
		rs.GetFieldValue("End_dV",data);	//float	m_End_dV;
		pStepData->m_fEndDV = data.fltVal;
		rs.GetFieldValue("End_dI",data);	//float	m_End_dI;
		pStepData->m_fEndDI = data.fltVal;
		rs.GetFieldValue("CycleCount",data);	//long	m_CycleCount;
		pStepData->m_nLoopInfoCycle = data.lVal;
		
		rs.GetFieldValue("OverV",data);	//float	m_OverV;
		pStepData->m_fHighLimitV = data.fltVal;
		rs.GetFieldValue("LimitV",data);	//float	m_LimitV;
		pStepData->m_fLowLimitV = data.fltVal;
		rs.GetFieldValue("OverI",data);	//float	m_OverI;
		pStepData->m_fHighLimitI = data.fltVal;
		rs.GetFieldValue("LimitI",data);	//float	m_LimitI;
		pStepData->m_fLowLimitI = data.fltVal;
		rs.GetFieldValue("OverCapacity",data);	//float	m_OverCapacity;
		pStepData->m_fHighLimitC = data.fltVal;
		rs.GetFieldValue("LimitCapacity",data);	//float	m_LimitCapacity;
		pStepData->m_fLowLimitC = data.fltVal;
		rs.GetFieldValue("OverImpedance",data);	//float	m_OverImpedance;
		pStepData->m_fHighLimitImp = data.fltVal;
		rs.GetFieldValue("LimitImpedance",data);	//float	m_LimitImpedance;
		pStepData->m_fLowLimitImp = data.fltVal;

		rs.GetFieldValue("DeltaTime",data);	//long	m_DeltaTime;
		pStepData->m_fDeltaTimeV = PCTIME(data.lVal);
		rs.GetFieldValue("DeltaTime1",data);	//long	m_DeltaTime1;
		pStepData->m_fDeltaTimeI = PCTIME(data.lVal);
		rs.GetFieldValue("DeltaV",data);	//float	m_DeltaV;
		pStepData->m_fDeltaV = data.fltVal;
		rs.GetFieldValue("DeltaI",data);	//float	m_DeltaI;
		pStepData->m_fDeltaI = data.fltVal;
		rs.GetFieldValue("Grade",data);	//BOOL	m_Grade;

		//?? DB값이 참이면 255가 들어 있음
		//?? 거짓이면 0가 들어 있음
		if((BYTE)data.bVal == 0)
			pStepData->m_bGrade = FALSE;
		else
			pStepData->m_bGrade = TRUE;

		rs.GetFieldValue("CompTimeV1",data);	//long	m_CompTimeV1;
		pStepData->m_fCompTimeV[0] = PCTIME(data.lVal);
		rs.GetFieldValue("CompTimeV2",data);	//long	m_CompTimeV2;
		pStepData->m_fCompTimeV[1] = PCTIME(data.lVal);
		rs.GetFieldValue("CompTimeV3",data);	//long	m_CompTimeV3;
		pStepData->m_fCompTimeV[2] = PCTIME(data.lVal);
		rs.GetFieldValue("CompVLow1",data);	//float	m_CompVLow1;
		pStepData->m_fCompLowV[0] = data.fltVal;
		rs.GetFieldValue("CompVLow2",data);	//float	m_CompVLow2;
		pStepData->m_fCompLowV[1] = data.fltVal;
		rs.GetFieldValue("CompVLow3",data);	//float	m_CompVLow3;
		pStepData->m_fCompLowV[2] = data.fltVal;
		rs.GetFieldValue("CompVHigh1",data);	//float	m_CompVHigh1;
		pStepData->m_fCompHighV[0] = data.fltVal;
		rs.GetFieldValue("CompVHigh2",data);	//float	m_CompVHigh2;
		pStepData->m_fCompHighV[1] = data.fltVal;
		rs.GetFieldValue("CompVHigh3",data);	//float	m_CompVHigh3;
		pStepData->m_fCompHighV[2] = data.fltVal;
		
		rs.GetFieldValue("CompTimeI1",data);	//long	m_CompTimeI1;
		pStepData->m_fCompTimeI[0] = PCTIME(data.lVal);
		rs.GetFieldValue("CompTimeI2",data);	//long	m_CompTimeI2;
		pStepData->m_fCompTimeI[1] = PCTIME(data.lVal);
		rs.GetFieldValue("CompTimeI3",data);	//long	m_CompTimeI3;
		pStepData->m_fCompTimeI[2] = PCTIME(data.lVal);
		rs.GetFieldValue("CompILow1",data);	//float	m_CompILow1;
		pStepData->m_fCompLowI[0] = data.fltVal;
		rs.GetFieldValue("CompILow2",data);	//float	m_CompILow2;
		pStepData->m_fCompLowI[1] = data.fltVal;
		rs.GetFieldValue("CompILow3",data);	//float	m_CompILow3;
		pStepData->m_fCompLowI[2] = data.fltVal;
		rs.GetFieldValue("CompIHigh1",data);	//float	m_CompIHigh1;
		pStepData->m_fCompHighI[0] = data.fltVal;
		rs.GetFieldValue("CompIHigh2",data);	//float	m_CompIHigh2;
		pStepData->m_fCompHighI[1] = data.fltVal;
		rs.GetFieldValue("CompIHigh3",data);	//float	m_CompIHigh3;
		pStepData->m_fCompHighI[2] = data.fltVal;

		rs.GetFieldValue("RecordTime",data);	//long	m_RecordTime;
		pStepData->m_fReportTime = PCTIME(data.lVal);
		rs.GetFieldValue("RecordDeltaV",data);	//float	m_RecordDeltaV;
		pStepData->m_fReportV = data.fltVal;
		rs.GetFieldValue("RecordDeltaI",data);	//float	m_RecordDeltaI;
		pStepData->m_fReportI = data.fltVal;

		rs.GetFieldValue("CapVLow",data);	//float	m_CapVLow;
		pStepData->m_fCapaVoltage1 = data.fltVal;
		rs.GetFieldValue("CapVHigh",data);	//float	m_CapVHigh;
		pStepData->m_fCapaVoltage2 = data.fltVal;

		//Goto 정보 -> Value0 
		//(m_nLoopInfoGoto, m_lRange, m_nLoopInfoGotoCnt,m_nAccLoopInfoCycle,m_nAccLoopGroupID, EndV_L(전압), 
		//  m_nGoto_EndV_H_StepID, m_nGoto_EndV_L_, m_nGoto_EndTime_, m_nGoto_EndCVTime_, m_nGoto_EndC_, m_nGoto_EndWh_, m_nGoto_EndSoc) 
		rs.GetFieldValue("Value0",data);	// Value0;
		pStepData->m_nLoopInfoGotoStep = 0;
		pStepData->m_lRange = 0;
		pStepData->m_nMultiLoopGroupID = 0;
		pStepData->m_nMultiLoopInfoCycle = 0;
		pStepData->m_nMultiLoopInfoGotoStep = 0;
		pStepData->m_nAccLoopGroupID = 0;
		pStepData->m_nAccLoopInfoCycle = 0;
		pStepData->m_nAccLoopInfoGotoStep = 0;
		pStepData->m_fEndV_L = 0;
		pStepData->m_nGotoStepEndV_H = 0;
		pStepData->m_nGotoStepEndV_L = 0;
		pStepData->m_nGotoStepEndTime = 0;
		pStepData->m_nGotoStepEndCVTime = 0;
		pStepData->m_nGotoStepEndC = 0;
		pStepData->m_nGotoStepEndWh = 0;
		pStepData->m_nGotoStepEndValue = 0;
		if(VT_NULL != data.vt)
		{
			sprintf(szTemp, "%s", data.pcVal);
			TRACE("%s\n",szTemp);

			count = 0;
			token = strtok( szTemp, seps1 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				TRACE("token %d = %s\n",count+1, szColl);
				//값 입력 
				iTempGoto = atoi(szColl);
				count++;
				if (count == 1)	pStepData->m_nLoopInfoGotoStep = iTempGoto;
				if (count == 2)	pStepData->m_lRange = iTempGoto;
				if (count == 3)	pStepData->m_nMultiLoopGroupID = iTempGoto;
				if (count == 4)	pStepData->m_nMultiLoopInfoCycle = iTempGoto;
				if (count == 5)	pStepData->m_nMultiLoopInfoGotoStep = iTempGoto;
				if (count == 6)	pStepData->m_nAccLoopGroupID = iTempGoto;
				if (count == 7)	pStepData->m_nAccLoopInfoCycle = iTempGoto;
				if (count == 8)	pStepData->m_nAccLoopInfoGotoStep = iTempGoto;
				if (count == 9)	pStepData->m_nGotoStepEndV_H = iTempGoto;
				if (count == 10) pStepData->m_nGotoStepEndV_L = iTempGoto;
				if (count == 11) pStepData->m_nGotoStepEndTime = iTempGoto;
				if (count == 12) pStepData->m_nGotoStepEndCVTime = iTempGoto;
				if (count == 13) pStepData->m_nGotoStepEndC = iTempGoto;
				if (count == 14) pStepData->m_nGotoStepEndWh = iTempGoto;
				if (count == 15) pStepData->m_nGotoStepEndValue = iTempGoto;
				token = strtok(NULL, seps1 );
			}				
			//strTemp = szTemp;	                                                                                                       
			//for (i=1 ; i<20 ; i++)                                                                                                   
			//{                                                                                                                        
			//	iTemp1 = strTemp.GetLength();                                                                                          
			//	iTemp2 = strTemp.Find(" ");                                                                                            
			//	if (iTemp2 < 0) //마지막 테이터                                                                                        
			//	{                                                                                                                      
			//		strWord = strTemp;                                                                                                   
			//		TRACE("%d번째 %s-%d\n",i, strTemp,strTemp.GetLength());                                                              
			//	}                                                                                                                      
			//	else                                                                                                                   
			//	{                                                                                                                      
			//		strWord = strTemp.Left(iTemp2);                                                                                      
			//	}                                                                                                                      
			//	//값 입력                                                                                                              
			//	iTempGoto = atoi(strWord);                                                                                             
			//	if (i == 1)	pStepData->m_nLoopInfoGotoStep = iTempGoto;                                                                
			//	if (i == 2)	pStepData->m_lRange = iTempGoto;                                                                           
			//	if (i == 3)	pStepData->m_nMultiLoopGroupID = iTempGoto;                                                                
			//	if (i == 4)	pStepData->m_nMultiLoopInfoCycle = iTempGoto;                                                              
			//	if (i == 5)	pStepData->m_nMultiLoopInfoGotoStep = iTempGoto;                                                           
			//	if (i == 6)	pStepData->m_nAccLoopGroupID = iTempGoto;                                                                  
			//	if (i == 7)	pStepData->m_nAccLoopInfoCycle = iTempGoto;                                                                
			//	if (i == 8)	pStepData->m_nAccLoopInfoGotoStep = iTempGoto;                                                             
			//	if (i == 9)	pStepData->m_nGotoStepEndV_H = iTempGoto;                                                                  
			//	if (i == 10) pStepData->m_nGotoStepEndV_L = iTempGoto;                                                                 
			//	if (i == 11) pStepData->m_nGotoStepEndTime = iTempGoto;                                                                
			//	if (i == 12) pStepData->m_nGotoStepEndCVTime = iTempGoto;                                                              
			//	if (i == 13) pStepData->m_nGotoStepEndC = iTempGoto;                                                                   
			//	if (i == 14) pStepData->m_nGotoStepEndWh = iTempGoto;                                                                  
			//	if (i == 15) pStepData->m_nGotoStepEndValue = iTempGoto;				TRACE("%d번째 %s-%d\n",i, strWord,strWord.GetLength());
			//	                                                                                                                       
			//	if (iTemp2 < 0) break;	//나가기                                                                                       
			//	if (iTemp1 < (iTemp2+1)) break;                                                                                        
			//	strTemp = strTemp.Right(iTemp1 - (iTemp2+1));                                                                          
 			//}
		}

		//strSQL += ", , Value1, Value2, Value3, Value4, Value5, Value6, Value7, Value8, Value9 "	//10
		//strSQL += ", CanCategory, CanCompare, CanValue, CanGoto, AuxCategory, AuxCompare, AuxValue, AuxGoto ";	//8
		//Report Temp조건 들어옴 
		rs.GetFieldValue("Value1",data);	// Value1;
		if(VT_NULL == data.vt)	pStepData->m_fReportTemp = 0.0f;
		else pStepData->m_fReportTemp = (float)atof(data.pcVal);

		//조건 분기 Condition ( SOC, WattHour )		분기 Low 전압 값을 여기에 쓴다
		rs.GetFieldValue("Value2",data);	// Value 2;
		pStepData->m_fEndV_L = 0;
		pStepData->m_bValueItem = 0;
		pStepData->m_bValueStepNo = 0;
		pStepData->m_fValueRate = 0.0f;
		iTemp1 = iTemp2 =iTemp3 =iTemp4 =iTemp5 =iTemp6 =iTemp7=iTemp8=iTemp9=iTemp10=iTemp11=iTemp12 = 0;
		fTemp1 = fTemp2 = fTemp3 =0.0f;
		if(VT_NULL != data.vt)
		{
			sprintf(szTemp, "%s", data.pcVal);
			//if(sscanf(szTemp, "%f %d %d %f %d %d %d %d %d %f",&fTemp2, &iTemp1, &iTemp2, &fTemp1, &iTemp3, &iTemp4, &iTemp5, &iTemp6, &iTemp7, &fTemp3) > 0)
			if(sscanf(szTemp, "%f %d %d %f %d %d %d %d %d %f %d %d %d %d %d",&fTemp2, &iTemp1, &iTemp2, &fTemp1, &iTemp3, &iTemp4, &iTemp5, &iTemp6, &iTemp7, &fTemp3, &iTemp8, &iTemp9, &iTemp10, &iTemp11, &iTemp12) > 0)
			{
				pStepData->m_fEndV_L = fTemp2;
				pStepData->m_bValueItem = (BYTE)iTemp1;
				pStepData->m_bValueStepNo = (BYTE)iTemp2;
				pStepData->m_fValueRate = fTemp1;
				pStepData->m_bUseCyclePause = iTemp3;	//ljb Cycle 반복 후 Pause 상태 유지 0:사용안함, 1:Pause 사용
				pStepData->m_bUseLinkStep = iTemp4;		//ljb CP-CC 연동 STEP
				pStepData->m_bUseChamberProg = iTemp5;	//ljb 챔버 Prog 모드로 실행 Pattern 99에 설정

				pStepData->nValueLoaderItem = iTemp6;		//ljb 20150512 0: 사용안함 , 1: ON,	 2: OFF
				pStepData->nValueLoaderMode = iTemp7;		//ljb 20150512 0: CP, 1: CC, 2: CV, 3: CR
				pStepData->fValueLoaderSet	= fTemp3;		//ljb 20150512 설정값 W,A,V,Ohm

				pStepData->m_nWaitTimeInit	= iTemp8;		//ljb 20160427
				pStepData->m_nWaitTimeDay	= iTemp9;		//ljb 20160427
				pStepData->m_nWaitTimeHour	= iTemp10;		//ljb 20160427
				pStepData->m_nWaitTimeMin	= iTemp11;		//ljb 20160427
				pStepData->m_nWaitTimeSec	= iTemp12;		//ljb 20160427
			}

			TRACE("STEP %d : Item = %d, CapaStep No = %d, fSocRate = %f\n", pStepData->m_StepIndex+1,pStepData->m_bValueItem ,pStepData->m_bValueStepNo, pStepData->m_fValueRate);
		}
 		//recordSet.m_strSocEnd.Format("%.3f %d %d %.2f %d %d %d %d %d %.2f", pStep->fEndV_L, pStep->nValueRateItem, pStep->nValueStepNo, pStep->fValueRate
 		//	, pStep->bUseCyclePause, pStep->bUseLinkStep, pStep->bUseChamberProg, pStep->nValueLoaderItem, pStep->nValueLoaderMode, pStep->fValueLoaderSet);
 		//TRACE("EndV_L , Save SOC STEP %d: %s\n", recordSet.m_StepNo,recordSet.m_strSocEnd);
		

		//End Watt, Watthour Condition 
		rs.GetFieldValue("Value3",data);	//	Value 3;
		pStepData->m_fEndW = 0.0f;
		pStepData->m_fEndWh = 0.0f;
		fTemp1 = fTemp2 = 0.0f;
		if(VT_NULL != data.vt)
		{
			sprintf(szTemp, "%s", data.pcVal);
			if(sscanf(szTemp, "%f %f", &fTemp1, &fTemp2) > 0)
			{
				pStepData->m_fEndW = fTemp1;
				pStepData->m_fEndWh = fTemp2;
			}
		}
		
		//Safety Temp
		rs.GetFieldValue("Value4",data);		// Value 4
		pStepData->m_fLowLimitTemp = pStepData->m_fHighLimitTemp =0.0f;
		pStepData->m_fEndT = pStepData->m_fStartT =0.0f;
		pStepData->m_bUseCyclePause = pStepData->m_bUseLinkStep = pStepData->m_bUseChamberProg = pStepData->m_bStepChamberStop = FALSE;
		//fTemp1 = fTemp2 = fTemp3 = fTemp4 = 0.0f;
		if(VT_NULL != data.vt)
		{
			//sprintf(szTemp, "%s", data.pcVal);
			//if(sscanf(szTemp, "%f %f %f %f", &fTemp1, &fTemp2, &fTemp3, &fTemp4) > 0)
			//{
			//	pStepData->m_fLowLimitTemp = fTemp1;
			//	pStepData->m_fHighLimitTemp= fTemp2;
			//	pStepData->m_fEndT = fTemp3;
			//	pStepData->m_fStartT= fTemp4;
			//}
			sprintf(szTemp, "%s", data.pcVal);
			TRACE("%s\n",szTemp);
			
			count = 0;
			token = strtok( szTemp, seps1 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				TRACE("token %d = %s\n",count+1, szColl);
				
				count++;
				//값 입력 
				if (count == 1)	pStepData->m_fLowLimitTemp = atof(szColl);
				if (count == 2)	pStepData->m_fHighLimitTemp = atof(szColl);
				if (count == 3)	pStepData->m_fEndT = atof(szColl);
				if (count == 4)	pStepData->m_fStartT = atof(szColl);
				if (count == 5)	pStepData->m_bUseCyclePause = atoi(szColl);
				if (count == 6)	pStepData->m_bUseLinkStep = atoi(szColl);
				if (count == 7)	pStepData->m_bUseChamberProg = atoi(szColl);
				if (count == 8)	pStepData->m_nNoCheckMode = atoi(szColl);	//2014.12.08 데이터 체크 모드
				if (count == 9)	pStepData->m_nCanTxOffMode = atoi(szColl);  //2014.12.08 캔 송시 모드
				if (count == 10)pStepData->m_nCanCheckMode = atoi(szColl);  //20150825 add
				if (count == 11)pStepData->m_bStepChamberStop = atoi(szColl); //yulee 20180829 
				token = strtok(NULL, seps1 );
			}
		}		

		//PatternFile Name
		
		//2014.09.04 패턴 파일과 usermap 파일 이름을 구분하여 저장한다.
		//2014.09.15 mdb Value10으로 만들었지만 다시 Value5를 같이 사용하도록 변경.	
		rs.GetFieldValue("Value5",data);		// Value 5		
		
		if(VT_NULL == data.vt)
		{
			strPatternFileName = "";	//2014.09.15 패턴과 유저맵 경로를 동일하게 사용한다.			
		}
		else{
			strPatternFileName.Format("%s", data.pcVal);			
		}
			
		pStepData->m_strPatternFileName = strPatternFileName;
		
		rs.GetFieldValue("Value6",data);		// Value 6		//float m_ValueLimitLow;
		pStepData->m_lValueLimitLow  = 0;
		pStepData->m_fCVTime = 0.0f;
		fTemp1 = fTemp2 = fTemp3 = 0.0f;
		if(VT_NULL != data.vt)
		{
			long lValue1, lValue2, lValue3;
			lValue1 = lValue2 = lValue3 = 0;
			sprintf(szTemp, "%s", data.pcVal);
			if(sscanf(szTemp, "%ld %f %f %f %ld %ld", &lValue1, &fTemp1, &fTemp2, &fTemp3, &lValue2, &lValue3) > 0)
			{
				pStepData->m_lValueLimitLow  = lValue1;
				pStepData->m_fCVTime = PCTIME(fTemp1);
				pStepData->m_fHighCapacitance = fTemp2;
				pStepData->m_fLowCapacitance = fTemp3;
				pStepData->m_lEndTimeDay = lValue2;		//ljb 20131212 add
				pStepData->m_lCVTimeDay  = lValue3;		//ljb 20131212 add
			}
		}
		
		rs.GetFieldValue("Value7",data);		// Value 7		//float m_ValueLimitHigh;	
		if(VT_NULL == data.vt)
		{
			pStepData->m_lValueLimitHigh  = 0;
		}
		else
		{
			sprintf(szTemp, "%s", data.pcVal);
			CString strTemp;
			strTemp.Format("%s", data.pcVal);
			pStepData->m_lValueLimitHigh = atol(strTemp);
		}

		rs.GetFieldValue("Value8",data);		// Value 8		//float m_ValueOven
 		//pStepData->m_fLowLimitTemp = 0.0f;
 		//pStepData->m_fHighLimitTemp= 0.0f;
		pStepData->m_fEndT = 0.0f;
		pStepData->m_fTref = 0.0f;
		pStepData->m_fHref = 0.0f;
		pStepData->m_fTrate = 0.0f;
		pStepData->m_fHrate = 0.0f;
		pStepData->m_fStartT = 0.0f;
		fTemp1 = fTemp2 = fTemp3 = fTemp4 =0.0f;
		iTemp1 = iTemp2 = iTemp3 = iTemp4 = iTemp5 = 0;
		iTemp6 = iTemp7 = iTemp8 = 0;

		//cny
		pStepData->m_nCellBal_CircuitRes=0;
		pStepData->m_nCellBal_WorkVoltUpper=0;//mV
		pStepData->m_nCellBal_WorkVolt=0;//mV
		pStepData->m_nCellBal_StartVolt=0;//mV
		pStepData->m_nCellBal_EndVolt=0;//mV
		pStepData->m_nCellBal_ActiveTime=0;
		pStepData->m_nCellBal_AutoStopTime=0;
        pStepData->m_nCellBal_AutoNextStep=0;
		pStepData->m_nCellBal_State=0;
		pStepData->m_nCellBal_Log=0;
		pStepData->m_nCellBal_Count=0;

		pStepData->m_nPwrSupply_Cmd=0;//1-ON 0-OFF
		pStepData->m_nPwrSupply_Volt=0;//mV
		pStepData->m_nPwrSupply_State=0;
		pStepData->m_nPwrSupply_Log=0;
		pStepData->m_nPwrSupply_SeqNo=0;

		pStepData->m_nChiller_Cmd=0;
		pStepData->m_fChiller_RefTemp=0;
		pStepData->m_nChiller_RefCmd=0;
		pStepData->m_fChiller_RefPump=0;
		pStepData->m_nChiller_PumpCmd=0;		
		pStepData->m_nChiller_TpData=0;
		pStepData->m_fChiller_ONTemp1=0;
		pStepData->m_nChiller_ONTemp1_Cmd=0;
		pStepData->m_nChiller_ONPump1_Cmd=0;
		pStepData->m_fChiller_ONTemp2=0;
		pStepData->m_nChiller_ONTemp2_Cmd=0;
		pStepData->m_nChiller_ONPump2_Cmd=0;
		pStepData->m_fChiller_OFFTemp1=0;
		pStepData->m_nChiller_OFFTemp1_Cmd=0;
		pStepData->m_nChiller_OFFPump1_Cmd=0;
		pStepData->m_fChiller_OFFTemp2=0;
		pStepData->m_nChiller_OFFTemp2_Cmd=0;
		pStepData->m_nChiller_OFFPump2_Cmd=0;
		pStepData->m_nChiller_State=0;
		pStepData->m_nChiller_Log=0;
		pStepData->m_nChiller_Count=0;
		pStepData->m_nChiller_SeqNo=0;

		if(VT_NULL != data.vt)
		{
			sprintf(szTemp, "%s", data.pcVal);
			if(sscanf(szTemp, "%f %f %f %f %d %d %d %d %d %d %d %d %d %d %d %f %d %f %d %d %f %d %d %f %d %d %f %d %d %f %d %d", 					
					&pStepData->m_fTref,
					&pStepData->m_fHref,
					&pStepData->m_fTrate,
					&pStepData->m_fHrate,					
					&pStepData->m_nCellBal_CircuitRes,//cellbal
					&pStepData->m_nCellBal_WorkVoltUpper,
					&pStepData->m_nCellBal_WorkVolt,
					&pStepData->m_nCellBal_StartVolt,
					&pStepData->m_nCellBal_EndVolt,
					&pStepData->m_nCellBal_ActiveTime,
					&pStepData->m_nCellBal_AutoStopTime,
					&pStepData->m_nCellBal_AutoNextStep,
					&pStepData->m_nPwrSupply_Cmd,//pwrsupply
					&pStepData->m_nPwrSupply_Volt,				
					&pStepData->m_nChiller_Cmd,//chiller
					&pStepData->m_fChiller_RefTemp,
					&pStepData->m_nChiller_RefCmd,
					&pStepData->m_fChiller_RefPump,
					&pStepData->m_nChiller_PumpCmd,
					&pStepData->m_nChiller_TpData,
					&pStepData->m_fChiller_ONTemp1,
					&pStepData->m_nChiller_ONTemp1_Cmd,
					&pStepData->m_nChiller_ONPump1_Cmd,
					&pStepData->m_fChiller_ONTemp2,
					&pStepData->m_nChiller_ONTemp2_Cmd,
					&pStepData->m_nChiller_ONPump2_Cmd,
					&pStepData->m_fChiller_OFFTemp1,
					&pStepData->m_nChiller_OFFTemp1_Cmd,
					&pStepData->m_nChiller_OFFPump1_Cmd,
					&pStepData->m_fChiller_OFFTemp2,
					&pStepData->m_nChiller_OFFTemp2_Cmd,
					&pStepData->m_nChiller_OFFPump2_Cmd
				) > 0)
			{
				//----------------------------------------
			}
		}

		//ljb 2009-03 add Aux,BMS
		rs.GetFieldValue("Value9",data);		// Value 9		//float m_Value Aux BMS
		pStepData->m_fEndAuxVtgHigh = 0.0f;
		pStepData->m_fEndAuxVtgLow = 0.0f;
		pStepData->m_fEndAuxTempHigh = 0.0f;
		pStepData->m_fEndAuxTempLow = 0.0f;
		pStepData->m_fEndBmsVtgHigh = 0.0f;
		pStepData->m_fEndBmsVtgLow = 0.0f;
		pStepData->m_fEndBmsTempHigh = 0.0f;
		pStepData->m_fEndBmsTempLow = 0.0f;
		pStepData->m_fEndBmsFltHigh = 0.0f;
		pStepData->m_fEndBmsSocHigh = 0.0f;
		pStepData->m_fEndBmsSocLow = 0.0f;
		
		pStepData->m_nLoopGotoStepAuxVtgH = 0;
		pStepData->m_nLoopGotoStepAuxVtgL = 0;
		pStepData->m_nLoopGotoStepAuxTempH = 0;
		pStepData->m_nLoopGotoStepAuxTempL = 0;
		pStepData->m_nLoopGotoStepBmsVtgH = 0;
		pStepData->m_nLoopGotoStepBmsVtgL = 0;
		pStepData->m_nLoopGotoStepBmsTempH = 0;
		pStepData->m_nLoopGotoStepBmsTempL = 0;
		pStepData->m_nLoopGotoStepBmsSocH = 0;
		pStepData->m_nLoopGotoStepBmsSocL = 0;
		pStepData->m_nLoopGotoStepBmsFltH = 0;

		if(VT_NULL != data.vt)
		{
			sprintf(szTemp, "%s", data.pcVal);
			TRACE("%s\n",szTemp);
			
			count = 0;
			token = strtok( szTemp, seps1 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				TRACE("token %d = %s\n",count+1, szColl);

				count++;
				//값 입력 
				if (count == 1)	pStepData->m_fEndAuxVtgHigh = atof(strWord);
				if (count == 2)	pStepData->m_fEndAuxVtgLow = atof(strWord);
				if (count == 3)	pStepData->m_fEndAuxTempHigh = atof(strWord);
				if (count == 4)	pStepData->m_fEndAuxTempLow = atof(strWord);
				if (count == 5)	pStepData->m_fEndBmsVtgHigh = atof(strWord);
				if (count == 6)	pStepData->m_fEndBmsVtgLow = atof(strWord);
				if (count == 7)	pStepData->m_fEndBmsTempHigh = atof(strWord);
				if (count == 8)	pStepData->m_fEndBmsTempLow = atof(strWord);
				if (count == 9)	pStepData->m_fEndBmsFltHigh = atof(strWord);
				if (count == 10) pStepData->m_nLoopGotoStepAuxVtgH = atoi(strWord);
				if (count == 11) pStepData->m_nLoopGotoStepAuxVtgL = atoi(strWord);
				if (count == 12) pStepData->m_nLoopGotoStepAuxTempH = atoi(strWord);
				if (count == 13) pStepData->m_nLoopGotoStepAuxTempL = atoi(strWord);
				if (count == 14) pStepData->m_nLoopGotoStepBmsVtgH = atoi(strWord);
				if (count == 15) pStepData->m_nLoopGotoStepBmsVtgL = atoi(strWord);
				if (count == 16) pStepData->m_nLoopGotoStepBmsTempH = atoi(strWord);
				if (count == 17) pStepData->m_nLoopGotoStepBmsTempL = atoi(strWord);
				if (count == 18) pStepData->m_nLoopGotoStepBmsFltH = atoi(strWord);
				if (count == 19) pStepData->m_fEndBmsSocHigh = atof(strWord);
				if (count == 20) pStepData->m_fEndBmsSocLow = atof(strWord);
				if (count == 21) pStepData->m_nLoopGotoStepBmsSocH = atoi(strWord);
				if (count == 22) pStepData->m_nLoopGotoStepBmsSocL = atoi(strWord);
				token = strtok(NULL, seps1 );
			}
		}

		//ljb 20100819 v100A CAN and Aux 조건
		rs.GetFieldValue("CanCategory",data);		// Can Funtion_divition	
		ZeroMemory(pStepData->m_ican_function_division,sizeof(pStepData->m_ican_function_division));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_ican_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps2 );
			}		
		}
			
		rs.GetFieldValue("CanCompare",data);		// Can Compare Type		
		ZeroMemory(pStepData->m_ican_compare_type,sizeof(pStepData->m_ican_compare_type));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_ican_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps2 );
			}		
		}
			
		rs.GetFieldValue("CanDataType",data);		// Can Compare Type		
		ZeroMemory(pStepData->m_ican_data_type,sizeof(pStepData->m_ican_data_type));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_ican_data_type[count] = atoi(szColl);
				if (pStepData->m_ican_function_division[count] != 0) pStepData->m_ican_data_type[count] = 2;	//ljb 20140109 Type를 float로 설정
				count++;
				token = strtok(NULL, seps2 );
			}		
		}

		rs.GetFieldValue("CanValue",data);			// Can Value
		ZeroMemory(pStepData->m_fcan_Value,sizeof(pStepData->m_fcan_Value));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_fcan_Value[count] = atof(szColl);
				count++;
				token = strtok(NULL, seps2 );
			}		
		}
			
		rs.GetFieldValue("CanGoto",data);			// Can branch
		ZeroMemory(pStepData->m_ican_branch,sizeof(pStepData->m_ican_branch));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_ican_branch[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps2 );
			}		
		}

		rs.GetFieldValue("AuxCategory",data);		// Aan Funtion_divition	
		ZeroMemory(pStepData->m_iaux_function_division,sizeof(pStepData->m_iaux_function_division));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_iaux_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps2 );
			}		
		}
			
		rs.GetFieldValue("AuxCompare",data);		// Aan Compare Type		
		ZeroMemory(pStepData->m_iaux_compare_type,sizeof(pStepData->m_iaux_compare_type));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_iaux_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps2 );
			}		
		}
			
		rs.GetFieldValue("AuxDataType",data);		// Aan Compare Type		
		ZeroMemory(pStepData->m_iaux_data_type,sizeof(pStepData->m_iaux_data_type));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_iaux_data_type[count] = atoi(szColl);
				if (pStepData->m_iaux_function_division[count] != 0) pStepData->m_iaux_data_type[count] = 2;	//ljb 20140109 Type를 float로 설정
				count++;
				token = strtok(NULL, seps2 );
			}		
		}

		rs.GetFieldValue("AuxValue",data);			// Aan Value
		ZeroMemory(pStepData->m_faux_Value,sizeof(pStepData->m_faux_Value));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				if (count < 10) 
					pStepData->m_faux_Value[count] = atof(szColl);
				else
					pStepData->m_iaux_conti_time[count-10] = atol(szColl);	//ljb 20170824 add
				count++;
				token = strtok(NULL, seps2 );
			}		
		}

		rs.GetFieldValue("AuxGoto",data);			// Aux Goto
		ZeroMemory(pStepData->m_iaux_branch,sizeof(pStepData->m_iaux_branch));
		if(VT_NULL != data.vt)
		{
			strcpy(szTemp, data.pcVal);
			count = 0;
			token = strtok( szTemp, seps2 );
			while(token != NULL)
			{
				strcpy(szColl, token);		//분리된 값
				pStepData->m_iaux_branch[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps2 );
			}		
		}

		rs.GetFieldValue("StepCellDeltaV",data);		
		pStepData->m_fCellDeltaVStep = data.dblVal;

		rs.GetFieldValue("StepDeltaAuxTemp",data);		
		pStepData->m_fStepDeltaAuxTemp = data.dblVal;

		rs.GetFieldValue("StepDeltaAuxTh",data);		
		pStepData->m_fStepDeltaAuxTH = data.dblVal;

		rs.GetFieldValue("StepDeltaAuxT",data);		
		pStepData->m_fStepDeltaAuxT = data.dblVal;

		rs.GetFieldValue("StepDeltaAuxVTime",data);		
		pStepData->m_fStepDeltaAuxVTime = data.dblVal;

		rs.GetFieldValue("StepAuxV",data);		
		pStepData->m_fStepAuxV = data.dblVal;

		rs.GetFieldValue("StepDeltaVentAuxV",data);	
		if((BYTE)data.bVal == 0)
			pStepData->m_bStepDeltaVentAuxV = FALSE;
		else
			pStepData->m_bStepDeltaVentAuxV = TRUE;

		rs.GetFieldValue("StepDeltaVentAuxTemp",data);
		if((BYTE)data.bVal == 0)
			pStepData->m_bStepDeltaVentAuxTemp = FALSE;
		else
			pStepData->m_bStepDeltaVentAuxTemp = TRUE;

		rs.GetFieldValue("StepDeltaVentAuxTh",data);
		if((BYTE)data.bVal == 0)
			pStepData->m_bStepDeltaVentAuxTh = FALSE;
		else
			pStepData->m_bStepDeltaVentAuxTh = TRUE;

		rs.GetFieldValue("StepDeltaVentAuxT",data);
		if((BYTE)data.bVal == 0)
			pStepData->m_bStepDeltaVentAuxT = FALSE;
		else
			pStepData->m_bStepDeltaVentAuxT = TRUE;

		rs.GetFieldValue("StepVentAuxV",data);
		if((BYTE)data.bVal == 0)
			pStepData->m_bStepVentAuxV = FALSE;
		else
			pStepData->m_bStepVentAuxV = TRUE;

		if(pStepData->m_bGrade == TRUE)
		{
			LoadGradeInfo(db, pStepData);
		}
		rs.MoveNext();

		m_apStepArray.Add(pStepData);
	}

	rs.Close();
	
	return m_apStepArray.GetSize();
}

BOOL CScheduleData::LoadGradeInfo(CDaoDatabase &db, CStep *pStepData)
{
	ASSERT(db.IsOpen());
	ASSERT(pStepData);

	CString strSQL, strQuery;
	CString strFrom;
	
	strSQL.Format("SELECT Value, Value1, GradeItem, GradeCode FROM GRADE WHERE StepID = %d ORDER BY GradeID", pStepData->m_StepID);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	
	float fdata1, fdata2; 
	long lItem;
	CString strcode;
	while(!rs.IsEOF())
	{
		//value0
		data = rs.GetFieldValue(0);	
		fdata1 = data.fltVal;
		//value1
		data = rs.GetFieldValue(1);	
		fdata2 = data.fltVal;
		//Item
		data = rs.GetFieldValue(2);	
		lItem = data.lVal;
		//code
		data = rs.GetFieldValue(3);	
		strcode = data.pcVal;
		pStepData->m_Grading.AddGradeStep(lItem, fdata1, fdata2, strcode);
		rs.MoveNext();
	}

	return pStepData->m_Grading.GetGradeStepSize();
}

CStep* CScheduleData::GetStepData(UINT nStepIndex)
{
	if(GetStepSize() <= nStepIndex)
	{
		return NULL;
	}
	return (CStep *)m_apStepArray[nStepIndex];	
}

BOOL CScheduleData::ExecuteEditor(long /*lModelPK*/, long /*lTestPK*/)
{
/*	CString strTemp, strArgu;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')) + "\\CTSEditorPro.exe";
	strTemp.Format("%s\\CTSEditorPro.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));
	strArgu.Format("%s %s", strModel,  strTest);

	if(ExecuteProgram(strTemp, strArgu, "", "CTSEditorPro", bNewWnd, TRUE)== FALSE)	return FALSE;

	//Argument가 있으면 
	if(!strModel.IsEmpty())
	{
		HWND FirsthWnd;
		FirsthWnd = ::FindWindow(NULL, "CTSEditorPro");
		if (FirsthWnd)
		{
			int nSize = strModel.GetLength()+strTest.GetLength()+2;
			char *pData = new char[nSize];
			sprintf(pData, "%s %s", strModel, strTest);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 3;		//CTS2005 Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(FirsthWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}

	}
*/	return TRUE;
}

void CScheduleData::ShowContent(int nStepIndex)
{
	CTestConReportDlg *pDlg = new CTestConReportDlg(this);
	pDlg->m_nCurStepIndex = nStepIndex;
	pDlg->DoModal();
	delete pDlg;
}

CString CScheduleData::GetTypeString(int nType)
{
	return PSGetTypeMsg(nType);

/*	switch ( nType )
	{
	case PS_STEP_CHARGE		:	return "Charge";
	case PS_STEP_DISCHARGE	:	return "Discharge";
	case PS_STEP_REST		:	return "Rest";
	case PS_STEP_OCV		:	return "OCV";
	case PS_STEP_IMPEDANCE	:	return "Impedance";
	case PS_STEP_END		:	return "종료";
	case PS_STEP_ADV_CYCLE	:	return "Cycle";
	case PS_STEP_LOOP		:	return "Loop";
	default					:	return "None";
	}
*/
}

CString CScheduleData::GetModeString(int nType, int nMode)
{
	CString msg;

	if(nType == PS_STEP_CHARGE || nType == PS_STEP_DISCHARGE)
	{
		switch(nMode)
		{
		case PS_MODE_CCCV:		msg = "CC/CV";		break;	
		case PS_MODE_CC	:		msg = "CC";			break;	
		case PS_MODE_CV	:		msg = "CV";			break;	
		case PS_MODE_CP	:		msg = "CP";			break;	
		case PS_MODE_PUSE:		msg = "Pulse";		break;	
		}
	}
	else if(nType == PS_STEP_IMPEDANCE)
	{
		if(nMode == PS_MODE_DCIMP)
		{
			msg = "DC";
		}
		else if(nMode == PS_MODE_ACIMP)
		{
			msg = "AC";
		}
	}
	return msg;
}

BOOL CScheduleData::SaveToExcelFile(CString strFileName, BOOL bOpenExcel, BOOL bCheckMode)
{
	CString strTemp;
	CString strData;

	FILE *fp = fopen(strFileName, "wt");
	if(fp == NULL)
	{
		return FALSE;
	}

	CTestConReportDlg dlg(this);

	/*
	fprintf(fp, "[모델명]\n");
	fprintf(fp, "%s,%s\n", "모델명", m_ModelData.szName);
	fprintf(fp, "%s,%s\n\n", "설명", m_ModelData.szDescription);

	fprintf(fp, "[스케쥴]\n");
	fprintf(fp, "%s,%s\n", "스케쥴명", m_ProcedureData.szName);
	fprintf(fp, "%s,%s\n", "작성자", m_ProcedureData.szCreator);
	fprintf(fp, "%s,%s\n", "작성시간", m_ProcedureData.szModifiedTime);
	fprintf(fp, "%s,%s\n\n", "설명", m_ProcedureData.szDescription);
	*/
	fprintf(fp, Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg1","SCHEDULEDATA"));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg2","SCHEDULEDATA"), m_ModelData.szName);//&&
	fprintf(fp, "%s,%s\n\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg3","SCHEDULEDATA"), m_ModelData.szDescription);//&&
	
	fprintf(fp, Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg4","SCHEDULEDATA"));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg5","SCHEDULEDATA"), m_ProcedureData.szName);//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg6","SCHEDULEDATA"), m_ProcedureData.szCreator);//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg7","SCHEDULEDATA"), m_ProcedureData.szModifiedTime);//&&
	fprintf(fp, "%s,%s\n\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg8","SCHEDULEDATA"), m_ProcedureData.szDescription);//&&

//	CTimeSpan t(m_CellCheck.lTrickleTime);
// 	if(bCheckMode)
// 	{
// 		fprintf(fp, "[검사 조건]\n");
// 		fprintf(fp, "%s,%s\n", "검사전류", dlg.ValueString(m_CellCheck.fIrefVal, PS_CURRENT, TRUE));
// 		fprintf(fp, "%s,%s\n\n", "검사시간", dlg.ValueString(m_CellCheck.lTrickleTime, PS_STEP_TIME)); 
// 	}
// 	else
// 	{
	/*
		fprintf(fp, "[안전 조건]\n");
		fprintf(fp, "%s,%s\n", "전압상한", dlg.ValueString(m_CellCheck.fMaxV, PS_VOLTAGE, TRUE));
		fprintf(fp, "%s,%s\n", "전압하한", dlg.ValueString(m_CellCheck.fMinV, PS_VOLTAGE, TRUE));
		fprintf(fp, "%s,%s\n", "전류상한", dlg.ValueString(m_CellCheck.fMaxI, PS_CURRENT, TRUE));	//
		fprintf(fp, "%s,%s\n", "전류하한", dlg.ValueString(m_CellCheck.fMaxI, PS_CURRENT, TRUE));
		fprintf(fp, "%s,%s\n", "온도상한", dlg.ValueString(m_CellCheck.fMaxT, PS_CURRENT, TRUE));
		fprintf(fp, "%s,%s\n", "온도하한", dlg.ValueString(m_CellCheck.fMaxT, PS_CURRENT, TRUE));
		fprintf(fp, "%s,%s\n", "용량상한", dlg.ValueString(m_CellCheck.fMaxC, PS_CAPACITY, TRUE));
		fprintf(fp, "%s,%s\n", "전력상한", dlg.ValueString(m_CellCheck.lMaxW, PS_WATT, TRUE));
		fprintf(fp, "%s,%s\n\n", "전력량상한", dlg.ValueString(m_CellCheck.lMaxWh, PS_WATT_HOUR, TRUE));
	*/
	fprintf(fp, Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg9","SCHEDULEDATA"));
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg10","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.fMaxV, PS_VOLTAGE, TRUE));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg11","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.fMinV, PS_VOLTAGE, TRUE));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg12","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.fMaxI, PS_CURRENT, TRUE));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg13","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.fMaxI, PS_CURRENT, TRUE));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg14","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.fMaxT, PS_CURRENT, TRUE));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg15","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.fMaxT, PS_CURRENT, TRUE));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg16","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.fMaxC, PS_CAPACITY, TRUE));//&&
	fprintf(fp, "%s,%s\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg17","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.lMaxW, PS_WATT, TRUE));//&&
	fprintf(fp, "%s,%s\n\n", Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg18","SCHEDULEDATA"), dlg.ValueString(m_CellCheck.lMaxWh, PS_WATT_HOUR, TRUE));//&&
//	}
	

	fprintf(fp, "[Step]\n");
//	fprintf(fp, "StepNo,Type,Mode,VRef,IRef,EndV,EndI,EndTime,EndC,");
	fprintf(fp, "StepNo,Type,Mode,VRef(%s),IRef(%s),End,", dlg.m_UnitTrans.GetUnitString(PS_VOLTAGE), dlg.m_UnitTrans.GetUnitString(PS_CURRENT));
	fprintf(fp, "V Limit,I Limit,C Limit, Imp Limit,");

	//Capacity voltage range
#ifdef _EDLC_TEST_SYSTEM
	fprintf(fp, "Cap Vtg,");
#endif

	for(int a = 0; a<MAX_STEP_COMP_SIZE; a++)
	{
		fprintf(fp, "(V%dlow<V<V%dhigh)/t%d,", a+1, a+1, a+1);
	}
	for(int a = 0; a<MAX_STEP_COMP_SIZE; a++)
	{
		fprintf(fp, "(I%dlow<I<I%dhigh)/t%d,", a+1, a+1, a+1);
	}

	//Save Condition
	

	fprintf(fp, "\n");
	
	int nTotStepNum = GetStepSize();					//Total Step Number
	CStep* pStep = NULL;;
	for(int step = 0; step<nTotStepNum; step++)
	{
		pStep = (CStep *)GetStepData(step);
		if(!pStep)
		{
			fclose(fp);
			return FALSE;
		}
		
		fprintf(fp, "%d,", step+1);			
		fprintf(fp, "%s,", GetTypeString(pStep->m_type));
		fprintf(fp, "%s,", GetModeString(pStep->m_type, pStep->m_mode));
	
		switch(pStep->m_type)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			{
				fprintf(fp, "%s,%s,%s,", dlg.ValueString(pStep->m_fVref_DisCharge, PS_VOLTAGE), dlg.ValueString(pStep->m_fIref, PS_CURRENT), dlg.MakeEndString(pStep));

				if(fabs(pStep->m_fLowLimitV) > 0 && fabs(pStep->m_fHighLimitV) > 0)
				{
					strTemp.Format("%s≤V≤%s", dlg.ValueString(pStep->m_fLowLimitV, PS_VOLTAGE), dlg.ValueString(pStep->m_fHighLimitV, PS_VOLTAGE));
				}
				else if(fabs(pStep->m_fLowLimitV) > 0)
				{
					strTemp.Format("%s≤V", dlg.ValueString(pStep->m_fLowLimitV, PS_VOLTAGE));
				}
				else if(fabs(pStep->m_fHighLimitV) > 0)
				{
					strTemp.Format("V≤%s",  dlg.ValueString(pStep->m_fHighLimitV, PS_VOLTAGE));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);

				if(fabs(pStep->m_fLowLimitI) > 0 && fabs(pStep->m_fHighLimitI) > 0)
				{
					strTemp.Format("%s≤I≤%s", dlg.ValueString(pStep->m_fLowLimitI, PS_CURRENT), dlg.ValueString(pStep->m_fHighLimitI, PS_CURRENT));
				}
				else if(fabs(pStep->m_fLowLimitI) > 0)
				{
					strTemp.Format("%s≤I", dlg.ValueString(pStep->m_fLowLimitI, PS_CURRENT));
				}
				else if(fabs(pStep->m_fHighLimitI) > 0)
				{
					strTemp.Format("I≤%s", dlg.ValueString(pStep->m_fHighLimitI, PS_CURRENT));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);

				if(fabs(pStep->m_fLowLimitC) > 0 && fabs(pStep->m_fHighLimitC) > 0)
				{
					strTemp.Format("%s≤C≤%s", dlg.ValueString(pStep->m_fLowLimitC, PS_CAPACITY), dlg.ValueString(pStep->m_fHighLimitC, PS_CAPACITY));
				}
				else if(fabs(pStep->m_fLowLimitC) > 0)
				{
					strTemp.Format("%s≤C", dlg.ValueString(pStep->m_fLowLimitC, PS_CAPACITY));
				}
				else if(fabs(pStep->m_fHighLimitC) > 0)
				{
					strTemp.Format("C≤%s", dlg.ValueString(pStep->m_fHighLimitC, PS_CAPACITY));
				}
				else 
					strTemp.Empty();

				fprintf(fp, "%s,", strTemp);
				fprintf(fp, ",");				//Impedance

				//Capacity voltage
#ifdef _EDLC_TEST_SYSTEM
				fprintf(fp, "%s~%s,", dlg.ValueString(pStep->m_fCapaVoltage1, PS_VOLTAGE), dlg.ValueString(pStep->m_fCapaVoltage2, PS_VOLTAGE));
#endif

				for(int a = 0; a<MAX_STEP_COMP_SIZE; a++)
				{
					fprintf(fp, "(%s<V<%s)/%s,", dlg.ValueString(pStep->m_fCompLowV[a], PS_VOLTAGE), dlg.ValueString(pStep->m_fCompHighV[a], PS_VOLTAGE), dlg.ValueString(pStep->m_fCompTimeV[a], PS_STEP_TIME)) ;
				}
				for(int a = 0; a<MAX_STEP_COMP_SIZE; a++)
				{
					fprintf(fp, "(%s/I/%s)/%s,", dlg.ValueString(pStep->m_fCompLowI[a], PS_CURRENT), dlg.ValueString(pStep->m_fCompHighI[a], PS_CURRENT), dlg.ValueString(pStep->m_fCompTimeI[a], PS_STEP_TIME)) ;
				}
				fprintf(fp, "\n");
				break;
			}

		case PS_STEP_REST:
			{
				fprintf(fp, ",,%s\n", dlg.MakeEndString(pStep));
				break;
			}
		case PS_STEP_OCV:
			{
				fprintf(fp, ",,,");

				if(fabs(pStep->m_fLowLimitV) > 0 && fabs(pStep->m_fHighLimitV) > 0)
				{
					strTemp.Format("%s≤V≤%s", dlg.ValueString(pStep->m_fLowLimitV, PS_VOLTAGE), dlg.ValueString(pStep->m_fHighLimitV, PS_VOLTAGE));
				}
				else if(fabs(pStep->m_fLowLimitV) > 0)
				{
					strTemp.Format("%s≤V", dlg.ValueString(pStep->m_fLowLimitV, PS_VOLTAGE) );
				}
				else if(fabs(pStep->m_fHighLimitV) > 0)
				{
					strTemp.Format("V≤%s", dlg.ValueString(pStep->m_fHighLimitV, PS_VOLTAGE));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
				
				fprintf(fp, "\n");
				break;
			}
		case PS_STEP_IMPEDANCE:
			{
				if(pStep->m_mode == PS_MODE_DCIMP)
				{
					fprintf(fp, "%s,%s,%s,,,,", dlg.ValueString(pStep->m_fVref_Charge, PS_VOLTAGE), dlg.ValueString(pStep->m_fIref, PS_CURRENT), dlg.MakeEndString(pStep));
				}
				else
				{
					fprintf(fp, ",,,,,,");
				}
				if(fabs(pStep->m_fLowLimitImp) > 0 && fabs(pStep->m_fHighLimitImp) > 0)
				{
					strTemp.Format("%s≤R≤%s", dlg.ValueString(pStep->m_fLowLimitImp, PS_IMPEDANCE), dlg.ValueString(pStep->m_fHighLimitImp, PS_IMPEDANCE));
				}
				else if(fabs(pStep->m_fLowLimitImp) > 0)
				{
					strTemp.Format("%s≤R", dlg.ValueString(pStep->m_fLowLimitImp, PS_IMPEDANCE));
				}
				else if(fabs(pStep->m_fHighLimitImp) > 0)
				{
					strTemp.Format("R≤%s", dlg.ValueString(pStep->m_fHighLimitImp, PS_IMPEDANCE));
				}
				else 
					strTemp.Empty();
				fprintf(fp, "%s,", strTemp);
				fprintf(fp, "\n");
				break;
			}
		case PS_STEP_LOOP:
			{
				fprintf(fp, ",,%s\n", dlg.MakeEndString(pStep));
				break;
			}
		case PS_STEP_END:
		default:
			{
				fprintf(fp, "\n");
				break;
			}
		}
	}

	for(int step = 0; step<nTotStepNum; step++)
	{
		pStep = (CStep *)GetStepData(step);
		if(!pStep)
		{
			fclose(fp);
			return FALSE;
		}
		if(pStep->m_bGrade)
		{
			fprintf(fp, "\n");
			//fprintf(fp, "Step %d 등급조건\n", step+1);
			fprintf(fp, Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg19","SCHEDULEDATA"), step+1);//&&
			//fprintf(fp, "No,하한값,상한값,Code\n");
			fprintf(fp, Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg20","SCHEDULEDATA"));//&&
			
			GRADE_STEP gStep;
			for(int k =0; k < pStep->m_Grading.GetGradeStepSize(); k++)
			{
				gStep = pStep->m_Grading.GetStepData(k);	
				if(/*pStep->m_Grading.m_lGradingItem*/ gStep.lGradeItem == PS_GRADE_CAPACITY)		//EDLC 용량은 F단위로 표시한다.
				{
					fprintf(fp, "%d,%s,%s,%s\n", k+1, dlg.ValueString(gStep.fMin, PS_CAPACITY),  dlg.ValueString(gStep.fMax, PS_CAPACITY), gStep.strCode);
				}
				else if(gStep.lGradeItem == PS_GRADE_IMPEDANCE)
				{
					fprintf(fp, "%d,%s,%s,%s\n", k+1, dlg.ValueString(gStep.fMin, PS_IMPEDANCE),  dlg.ValueString(gStep.fMax, PS_IMPEDANCE), gStep.strCode);
				}
				else
				{
					fprintf(fp, "%d,%s,%s,%s\n", k+1, dlg.ValueString(gStep.fMin, PS_VOLTAGE),  dlg.ValueString(gStep.fMax, PS_VOLTAGE), gStep.strCode);
				}
			}
		}
	}
	
	fclose(fp);	
	
	if(bOpenExcel)
	{
		//Excel로 Open 여부를 묻는다.
		//strTemp.Format("%s를 지금 Open하시겠습니까?", strFileName);
		strTemp.Format(Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg21","SCHEDULEDATA"), strFileName);//&&
		if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) != IDNO)
		{
			if(ExecuteExcel(strFileName) == FALSE)
			{
				//strTemp.Format("%s를 Excel로 열수 없습니다.", strFileName);
				strTemp.Format(Fun_FindMsg("ScheduleData_Fun_SaveToExcelFile_msg22","SCHEDULEDATA"), strFileName);//&&
				AfxMessageBox(strTemp);
			}
		}
	}
	return TRUE;
}

BOOL CScheduleData::ExecuteExcel(CString strFileName)
{
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	strTemp = GetExcelPath();
	if(strTemp.IsEmpty())	return FALSE;

	char buff1[_MAX_PATH];
	char buff2[_MAX_PATH];

	int aa =0;
	do {
		//존재 여부 확인
		aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
		if(aa <= 0)
		{
			//if(AfxMessageBox("Excel 실행파일 경로가 잘못되었습니다. 경로를 설정 하십시요.", MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			if(AfxMessageBox(Fun_FindMsg("ScheduleData_ExecuteExcel_msg1","SCHEDULEDATA"), MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)//&&
			{
				return FALSE;

			}
			strTemp = GetExcelPath();
			if(strTemp.IsEmpty())	return FALSE;
		}
		
	} while( aa <= 0 );	

	GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
	GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, _MAX_PATH);
	
	strTemp.Format("%s %s", buff2, buff1);
	
	BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		//strTemp.Format("%s를 Open할 수 없습니다.", strFileName);
		strTemp.Format(Fun_FindMsg("ScheduleData_ExecuteExcel_msg2","SCHEDULEDATA"), strFileName);//&&
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
	}
	return TRUE;
}

CString CScheduleData::GetExcelPath()
{
	CString strExcelPath = AfxGetApp()->GetProfileString("Config","Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		//pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";
		pDlg.m_ofn.lpstrTitle = Fun_FindMsg("ScheduleData_GetExcelPath_msg1","SCHEDULEDATA");//&&
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		AfxGetApp()->WriteProfileString("Config","Excel Path", strExcelPath);
	}
	return strExcelPath;
}

//Schedule에 Impdedance step이 포함되어 있는지 검사
BOOL CScheduleData::IsThereImpStep()
{
	CStep *pStep;
	for(int step = 0; step<(int)GetStepSize(); step++)
	{
		pStep = GetStepData(step);
		if(pStep)
		{
			if(pStep->m_type == PS_STEP_IMPEDANCE)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

//Schedule에 Grading을 실시하는 Step이 존재하는지 검사 
BOOL CScheduleData::IsThereGradingStep()
{
	CStep *pStep;
	for(int step = 0; step<(int)GetStepSize(); step++)
	{
		pStep = GetStepData(step);
		if(pStep)
		{
			if(pStep->m_Grading.GetGradeStepSize() > 0)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

//현재 Step이 속한 Cycle의 반복 횟수를 구한다.
int CScheduleData::GetCurrentStepCycle(int nStepIndex)
{
	if(nStepIndex < 0)		return 0;

	for(int step = nStepIndex; step < (int)GetStepSize(); step++)
	{
		CStep *pStep = GetStepData(step);
		if(pStep && pStep->m_type == PS_STEP_LOOP)
		{
			return pStep->m_nLoopInfoCycle;
		}
	}
	return 0;
}

BOOL CScheduleData::AddStep_vD(FILE_STEP_PARAM_V100D *pStepData)
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_vD(pStepData);
	m_apStepArray.Add(pStep);

	return TRUE;
}

BOOL CScheduleData::AddStep_vDL(FILE_STEP_PARAM_V100D_LOAD *pStepData)
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_vDL(pStepData);
	m_apStepArray.Add(pStep);

	return TRUE;
}

BOOL CScheduleData::AddStep_vF(FILE_STEP_PARAM_V100F *pStepData)
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_vF(pStepData);
	m_apStepArray.Add(pStep);

	return TRUE;
}

//BOOL CScheduleData::AddStep(FILE_STEP_PARAM_V100D *pStepData)
BOOL CScheduleData::AddStep_vDL_SCH(FILE_STEP_PARAM_V100D_LOAD_SCH *pStepData) //ksj 20180528 //20180627 yulee
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_vDL_SCH(pStepData);
	m_apStepArray.Add(pStep);

	return TRUE;
}

BOOL CScheduleData::AddStep_vF_v2_SCH(FILE_STEP_PARAM_V100F_v2_SCH *pStepData) //ksj 20180528
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_vF2_SCH(pStepData);
	m_apStepArray.Add(pStep);

	return TRUE;
}

BOOL CScheduleData::AddStep_v1011_v1_SCH(FILE_STEP_PARAM_V1011_v1_SCH *pStepData) //yulee 20180828
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_v1011_v1_SCH(pStepData);
	m_apStepArray.Add(pStep);
	
	return TRUE;
}

BOOL CScheduleData::AddStep_v1013_v2_SCH(FILE_STEP_PARAM_V1013_v2_SCH *pStepData) //yulee 20181002
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_v1013_v2_SCH(pStepData);
	m_apStepArray.Add(pStep);
	
	return TRUE;
}

//ksj 20171123
BOOL CScheduleData::AddStep_vFv2(FILE_STEP_PARAM_V100F_v2 *pStepData)
{
	CStep *pStep;
	pStep = new CStep;
	pStep->SetStepData_vF2(pStepData);
	m_apStepArray.Add(pStep);
	
	return TRUE;
}

int CScheduleData::Fun_ISEmptyCommSafety()
{
	//memset(&m_CellCheck, 0, sizeof(m_CellCheck));
	if (m_CellCheck.fMinV == 0 && m_CellCheck.fMaxV == 0 && m_CellCheck.fMaxI == 0 && m_CellCheck.fMaxC == 0 && m_CellCheck.lMaxW == 0 && m_CellCheck.lMaxWh == 0)
	{
		return -1;
	}
	if (m_CellCheck.fMaxV == 0 || m_CellCheck.fMaxI == 0 || m_CellCheck.fCellDeltaV == 0)	//20150811 edit
	{
		return -2;
	}
	// 211015 HKH 절연모드 충전 관련 기능 보완 =============================
	BOOL bUseIsolationSafety = AfxGetApp()->GetProfileInt("Config" , "Use Isolation Safety", 0);
	if(bUseIsolationSafety)
	{
		if (m_CellCheck.fMinV == 0)
		{
			return -3;
		}
	}
	// =====================================================================

	return 1;
}