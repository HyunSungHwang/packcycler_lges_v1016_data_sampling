/*-------------------------------------------------------------------------------------------
| File        : Program.cs
| Project     : Vector FlexRay Fibex to local data converter example
|
| Description : Main program file
|--------------------------------------------------------------------------------------------
| $Author: vismra $    $Date: 2009-12-18 10:49:31 +0100 (Fr, 18 Dez 2009) $   $Revision: 4562 $
| $Id: Program.cs 4562 2009-12-18 09:49:31Z vismra $
|--------------------------------------------------------------------------------------------
| Copyright (c) 2009 by Vector Informatik GmbH.  All rights reserved.
|------------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Fibex2CSharp_Converter
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new Form1());
    }
  }
}