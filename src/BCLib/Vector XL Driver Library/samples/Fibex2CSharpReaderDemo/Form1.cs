/*-------------------------------------------------------------------------------------------
| File        : Form1.cs
| Project     : Vector FlexRay Fibex to local data converter example
|
| Description : Main form of the application
|--------------------------------------------------------------------------------------------
| $Author: vismra $    $Date: 2009-12-18 10:49:31 +0100 (Fr, 18 Dez 2009) $   $Revision: 4562 $
| $Id: Form1.cs 4562 2009-12-18 09:49:31Z vismra $
|--------------------------------------------------------------------------------------------
| Copyright (c) 2009 by Vector Informatik GmbH.  All rights reserved.
|------------------------------------------------------------------------------------------*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Fibex_Parser;

namespace Fibex2CSharp_Converter
{
  public partial class Form1 : Form
  {
    //--------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Constructor.
    /// </summary>
    //--------------------------------------------------------------------------------------------------------
    public Form1()
    {
      InitializeComponent();
    }

    //--------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Called when loading the form.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //--------------------------------------------------------------------------------------------------------
    private void Form1_Load(object sender, EventArgs e)
    {
      dgvOutXlapiClusterParam.AutoGenerateColumns = true;
    }

    //--------------------------------------------------------------------------------------------------------
    /// <summary>
    /// This method is called when the user selected an input file in the file input dialog.
    /// It is used to call the conversion method with the given filename.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //--------------------------------------------------------------------------------------------------------
    private void btnBrowseInputFile_Click(object sender, EventArgs e)
    {
      openFileDialog1.FileName = "";
      openFileDialog1.CheckFileExists = true;
      openFileDialog1.Multiselect = false;
      openFileDialog1.Filter = "Fibex files (*.xml)|*.xml|All files (*.*)|*.*";
      if (openFileDialog1.ShowDialog() == DialogResult.OK)
      {
        if (openFileDialog1.FileName.Length > 0)
        {
          convertFibexToXlapi(openFileDialog1.FileName);
        }
      }
    }

    //--------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Is called when a key is pressed in the XML input field. This method
    /// only recognizes the 'enter' key to start the transformation if pressed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //--------------------------------------------------------------------------------------------------------
    private void tbXmlInputFile_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar.Equals(Keys.Enter))
      {
        if (tbXmlInputFile.Text.Length > 0)
        {
          convertFibexToXlapi(tbXmlInputFile.Text);
        }
      }
    }

    //--------------------------------------------------------------------------------------------------------
    /// <summary>
    /// This method does the conversion from Fibex to XLAPI style
    /// </summary>
    /// <param name="filename">Fibex filename</param>
    //--------------------------------------------------------------------------------------------------------
    private void convertFibexToXlapi(string filename)
    {
      SortedList clusterParams;
      tbConversionState.Text = "Converting...";
      tbXmlInputFile.Text = filename;
      FlexRayConfiguration frConfig = new FlexRayConfiguration();
      FibexConfiguration fibex = new FibexConfiguration();
      string result = fibex.GetConfig(filename, ref frConfig).ToString();
      tbConversionState.Text = result;
      clusterParams = frConfig.GetClusterConfigDescription();
      dataXlapiClusterParam.DataSource = clusterParams;
    }
  }
}