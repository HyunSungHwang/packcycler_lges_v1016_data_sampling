--------------------------------------------------------------------------------

     xlDAIOexample - sample application for the 'XL Family Driver Library'


                      Vector Informatik GmbH, Stuttgart

--------------------------------------------------------------------------------

Vector Informatik GmbH
Ingersheimer Stra�e 24
D-70499 Stuttgart

Tel.:  0711-80670-200
Fax.:  0711-80670-555
EMAIL: support@vector.com
WEB:   www.vector.com

--------------------------------------------------------------------------------

xlDAIOexample is a small test application for the DAIO functionality on 

- CANcardXL (and the IOcab8444opto)
- On-board DAIO of VN16xx devices
- IOpiggy 8642

For further information look into the 'XL Driver Library - Description.pdf' 
document.
