//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by xlMOST150View.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_XLMOST150VIEW_DIALOG        102
#define IDR_HTML_MOSTNODECONFIG         103
#define IDR_MAINFRAME                   128
#define IDD_NODE_CONFIG                 129
#define IDD_GEN_TEST                    131
#define IDD_STREAM_CONFIG               132
#define IDC_LIST1                       1000
#define IDC_ACTIVATE                    1002
#define IDC_DEACTIVATE                  1003
#define IDC_LIGHT                       1004
#define IDC_STARTUP                     1004
#define IDC_DEVICE                      1005
#define IDC_DEACTIVATE2                 1006
#define IDC_STREAMING                   1006
#define IDC_SET_PARAMETER               1009
#define IDC_TIMING_MASTER               1010
#define IDC_DEVICE_MODE_MASTER          1010
#define IDC_TIMING_SLAVE                1011
#define IDC_DEVICE_MODE_SLAVE           1011
#define IDC_NODE_CONFIG_GRP             1012
#define IDC_NODE_ADR                    1015
#define IDC_GROUP_ADR                   1016
#define IDC_FREQUENCY                   1017
#define IDC_TX_CTRL                     1018
#define IDC_BYPASS                      1018
#define IDC_CLEAR                       1019
#define IDC_TEST                        1020
#define IDC_GET_INFO                    1021
#define IDC_BUTTON2                     1022
#define IDC_NODE_CONFIG                 1022
#define IDC_GEN_TEST                    1023
#define IDC_GEN_LIGHT                   1024
#define IDC_GEN_LOCK                    1025
#define IDC_TWINKLE                     1026
#define IDC_ABOUT                       1027
#define IDC_EDIT1                       1028
#define IDC_EDIT2                       1029
#define IDC_BUTTON1                     1031
#define IDC_CTRL                        1031
#define IDC_SEND_ASYNC_TX               1031
#define IDC_ASYNC                       1032
#define IDC_DATETIMEPICKER1             1032
#define IDC_FBLOCK_ID                   1033
#define IDC_ASYNC_TARGET_ADDR           1033
#define IDC_CTRL_TARGET_ADDR            1039
#define IDC_SHUTDOWN                    1040
#define IDC_CL_LIST                     1041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
