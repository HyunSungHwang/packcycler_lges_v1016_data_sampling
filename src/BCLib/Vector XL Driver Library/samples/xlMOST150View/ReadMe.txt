--------------------------------------------------------------------------------

                                xlMOST150View


                      Vector Informatik GmbH, Stuttgart

--------------------------------------------------------------------------------

Vector Informatik GmbH
Ingersheimer Stra�e 24
70499 Stuttgart, Germany

Phone: ++49 - 711 - 80670 - 0
Fax:   ++49 - 711 - 80670 - 111


--------------------------------------------------------------------------------

xlMOST150View is a small test application for the MOST functionality for the
VN2640 MOST150 interface.

For further information look into the 'XL Driver Library - Description.pdf' 
document.


