/*-------------------------------------------------------------------------------------------
| File        : xlGeneral.h
| Project     : Vector MOST150 Example 
|
| Description : 
|--------------------------------------------------------------------------------------------
| $Author: visstz $    $Locker: $   $Revision: 13395 $
| $Header: /VCANDRV/XLAPI/samples/xlMOST150View/xlGeneral.h 2     25.08.05 9:40 J�rg $
|--------------------------------------------------------------------------------------------
| Copyright (c) 2011 by Vector Informatik GmbH.  All rights reserved.
|------------------------------------------------------------------------------------------*/


#pragma once

class CGeneral
{
public:
  CGeneral(void);
  ~CGeneral(void);
   unsigned short StrToShort(char *str); 
};
