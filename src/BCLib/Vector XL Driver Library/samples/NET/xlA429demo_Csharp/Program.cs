﻿/*-------------------------------------------------------------------------------------------
| File        : Program.cs
| Project     : Vector A429 .NET Example
|
| Description : This example demonstrates basic A429 functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/

using System;
using System.Threading;

using vxlapi_NET;

namespace xlA429demo_Csharp
{
  /// <summary>
  /// This program demonstrates how to use the Vector XL for .NET Driver Library on a VN0601 device. 
  /// </summary>
  class Program
  {
    // -----------------------------------------------------------------------------------------------
    // Constants
    // -----------------------------------------------------------------------------------------------
    private const string KAppName = "xlA429demoNET";
    private const string KPressAnyKey = "Press any key to continue...";

    // -----------------------------------------------------------------------------------------------
    // Struct holding channel info
    // -----------------------------------------------------------------------------------------------
    private struct ChannelInfo
    {
      public XLDefine.XL_HardwareType myHwType;
      public uint myHwIndex;
      public uint myHwChannel;
      public int myPortHandle;
      public int myEventHandle;
      public UInt64 myAccessMask;
      public UInt64 myPermissionMask;
      public int myChannelIndex;
      public XLClass.xl_channel_config myChannelConfig;
    }

    // -----------------------------------------------------------------------------------------------
    // Global variables
    // -----------------------------------------------------------------------------------------------
    // Driver access through XLDriver (wrapper):
    private static XLDriver myDriver = new XLDriver();
    
    // Driver configuration:
    private static XLClass.xl_driver_config myConfig = new XLClass.xl_driver_config();

    // Variables required by XLDriver:
    private static ChannelInfo[] myInfo = new ChannelInfo[2]; // Index 0 = Tx, Index 1 = Rx

    // Rx thread:
    private static Thread myRxThread = null;
    private static bool myBlockRxThread = false;

    // Random data generator for Tx messages:
    private static Random myData = new Random();

    /// <summary>
    /// Main routine of the program.
    /// </summary>
    /// <param name="args">Unused.</param>
    static void Main(string[] args)
    {
      Console.WriteLine("-------------------------------------------------------------------");
      Console.WriteLine("                    xlA429demo.NET C# V11.0                        ");
      Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.");
      Console.WriteLine("-------------------------------------------------------------------");
      Console.WriteLine();

      // print .NET wrapper version
      Console.WriteLine("vxlapi_NET        : {0}", typeof(XLDriver).Assembly.GetName().Version);

      // Open XL Driver:
      XLDefine.XL_Status status = myDriver.XL_OpenDriver();
      Console.WriteLine("Open Driver       : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return;
      }

      // Get XL Driver configuration:
      status = myDriver.XL_GetDriverConfig(ref myConfig);
      Console.WriteLine("Get Driver Config : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return;
      }

      // Convert DLL version number into a readable string:
      Console.WriteLine("DLL Version       : {0}", myDriver.VersionToString(myConfig.dllVersion));

      // Display channel count:
      Console.WriteLine("Channels found    : {0}", myConfig.channelCount);

      // Display all channels found:
      for (int counter = 0; counter < myConfig.channelCount; ++counter)
      {
        Console.WriteLine();
        Console.WriteLine("[{0}] {1}", myConfig.channel[counter].name, counter);
        Console.WriteLine("- Channel Mask    : {0}", myConfig.channel[counter].channelMask);
        Console.WriteLine("- Transceiver Name: {0}", myConfig.channel[counter].transceiverName);
        Console.WriteLine("- Serial Number   : {0}", myConfig.channel[counter].serialNumber);
      }

      // If the application name cannot be found in VCANCONF...
      if ((GetApplConfig(0, ref myInfo[0]) != XLDefine.XL_Status.XL_SUCCESS) || (GetApplConfig(1, ref myInfo[1]) != XLDefine.XL_Status.XL_SUCCESS))
      {
        // ...create an item with two ARINC429 channels:
        myDriver.XL_SetApplConfig(KAppName, 0, XLDefine.XL_HardwareType.XL_HWTYPE_VN0601, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_A429);
        myDriver.XL_SetApplConfig(KAppName, 1, XLDefine.XL_HardwareType.XL_HWTYPE_VN0601, 0, 4, XLDefine.XL_BusTypes.XL_BUS_TYPE_A429);
      }

      // ...open Tx and Rx port. Since ARINC 429 is unidirectional, we need two ports:
      // one for transmitting and one for receiving (unlike e.g. CAN where we need one
      // port only for both transmitting and receiving).
      if (!OpenTxPort() || !OpenRxPort()) return;
      try
      {
        // Run Rx thread:
        Console.WriteLine();
        Console.WriteLine("Starting Rx thread...");
        myRxThread = new Thread(new ThreadStart(RXThread));
        myRxThread.Start();
        Console.WriteLine("Done.");
        Console.WriteLine();

        // User information:
        PrintHelp();

        // Start menu loop:
        bool stop = false;
        while (!stop)
        {
          if (myBlockRxThread)
          {
            Console.WriteLine("Rx thread blocked.");
          }

          // Read user input:
          ConsoleKeyInfo info = Console.ReadKey(true);
          Console.WriteLine();
          switch (info.KeyChar)
          {
            case 'b':
              myBlockRxThread = !myBlockRxThread;
              break;
            case 'c':
              TransmitCyclic();
              break;
            case 'h':
            default:
              PrintHelp();
              break;
            case 's':
              StopCyclic();
              break;
            case 't':
              TransmitOnce();
              break;
            case 'x':
              stop = true;
              break;
          }
        }

        // Kill Rx thread:
        myRxThread.Abort();
      }
      catch (System.Exception ex)
      {
        Console.WriteLine("Rx thread not started. Reason: {0}", ex.Message);
        Console.WriteLine(KPressAnyKey);
        Console.ReadKey();
      }

      // Close ports and driver:
      Console.WriteLine("Close Tx Port         : {0}", myDriver.XL_ClosePort(myInfo[0].myPortHandle));
      Console.WriteLine("Close Rx Port         : {0}", myDriver.XL_ClosePort(myInfo[1].myPortHandle));
      Console.WriteLine("Close Driver          : {0}", myDriver.XL_CloseDriver());

      Console.WriteLine("Goodbye.");
    }

    /// <summary>
    /// Demonstrates transmitting a ARINC 429 message only once.
    /// </summary>
    private static void TransmitOnce()
    {
      // Create the message object with some random data:
      var msg = new XLClass.XL_A429_MSG_TX();
      msg.cycleTime = 0;
      msg.data = (uint)myData.Next(524288); // 524288 = 2^19 (max possible value for 19 data bits)
      msg.flags = (uint)XLDefine.XL_A429_TX_MSG_Flags.XL_A429_MSG_FLAG_ON_REQUEST;
      msg.gap = 40000; // Must be given in nanoseconds!
      msg.label = 110;
      msg.parity = (byte)XLDefine.XL_A429_PARAMS_Parity.XL_A429_MSG_PARITY_ODD;
      msg.userHandle = (ushort)myInfo[0].myPortHandle;

      // Send it to the device:
      uint msgCountSent = 0;
      var status = myDriver.XL_A429Transmit(myInfo[0].myPortHandle, myInfo[0].myAccessMask, 1, ref msgCountSent, ref msg);
      Console.WriteLine("Transmitting once     : {0} ({1})", status, msgCountSent);
    }

    /// <summary>
    /// Demonstrates cyclic transmission of a ARINC 429 message.
    /// </summary>
    /// <remarks>
    /// This function needs to be called only once. Repeated transmission is done by the device itself.
    /// </remarks>
    private static void TransmitCyclic()
    {
      // Create the message object with some random data:
      var msg = new XLClass.XL_A429_MSG_TX();
      msg.cycleTime = 50000; // Must be given in nanoseconds!
      msg.data = (uint)myData.Next(524288);
      msg.flags = (uint)XLDefine.XL_A429_TX_MSG_Flags.XL_A429_MSG_FLAG_CYCLIC;
      msg.gap = 40000; // Must be given in nanoseconds!
      msg.label = 110;
      msg.parity = (byte)XLDefine.XL_A429_PARAMS_Parity.XL_A429_MSG_PARITY_ODD;
      msg.userHandle = (ushort)myInfo[0].myPortHandle;

      // Send it to the device:
      uint msgCountSent = 0;
      var status = myDriver.XL_A429Transmit(myInfo[0].myPortHandle, myInfo[0].myAccessMask, 1, ref msgCountSent, ref msg);
      Console.WriteLine("Transmitting cyclic   : {0} ({1})", status, msgCountSent);
    }

    /// <summary>
    /// Demonstrates how to stop a cyclic transmission of a ARINC 429 message.
    /// </summary>
    private static void StopCyclic()
    {
      // Create the message object whose cyclic transmission is to be stopped:
      var msg = new XLClass.XL_A429_MSG_TX();
      msg.cycleTime = 0;
      msg.data = 0;
      msg.flags = (uint)XLDefine.XL_A429_TX_MSG_Flags.XL_A429_MSG_FLAG_DELETE_CYCLIC;
      msg.gap = 0;
      msg.label = 110;
      msg.parity = (byte)XLDefine.XL_A429_PARAMS_Parity.XL_A429_MSG_PARITY_ODD;
      msg.userHandle = (ushort)myInfo[0].myPortHandle;

      // Send it to the device:
      uint msgCountSent = 0;
      var status = myDriver.XL_A429Transmit(myInfo[0].myPortHandle, myInfo[0].myAccessMask, 1, ref msgCountSent, ref msg);
      Console.WriteLine("Stopping cyclic       : {0} ({1})", status, msgCountSent);
    }

    /// <summary>
    /// Write a application channel assignment into a ChannelInfo, including access mask, channelIndex and channelConfig
    /// </summary>
    /// <param name="appChIdx">Application channel index to query.</param>
    /// <param name="chInfo">ChannelInfo that receives the assignment.</param>
    /// <returns>Return value of XL_GetApplConfig.</returns>
    private static XLDefine.XL_Status GetApplConfig(uint appChIdx, ref ChannelInfo chInfo)
    {
      XLDefine.XL_Status status;
      chInfo.myAccessMask = 0;
      chInfo.myChannelIndex = -1;
      chInfo.myChannelConfig = null;
      status = myDriver.XL_GetApplConfig(KAppName, appChIdx, ref chInfo.myHwType, ref chInfo.myHwIndex, ref chInfo.myHwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_A429);
      if (status == XLDefine.XL_Status.XL_SUCCESS)
      {
        chInfo.myChannelIndex = myDriver.XL_GetChannelIndex(chInfo.myHwType, (int)chInfo.myHwIndex, (int)chInfo.myHwChannel);
        if (chInfo.myChannelIndex >= 0 && chInfo.myChannelIndex < 64)
        {
          // VN0601 needs the access mask to be computed from the channel index:
          chInfo.myAccessMask = (ulong)1 << chInfo.myChannelIndex;
        }
        if (chInfo.myChannelIndex >= 0 && chInfo.myChannelIndex < myConfig.channelCount)
        {
          chInfo.myChannelConfig = myConfig.channel[chInfo.myChannelIndex];
        }
      }
      return status;
    }

    /// <summary>
    /// Opens a transmitting port on a VN0601 device.
    /// </summary>
    /// <returns>True if successful.</returns>
    private static bool OpenTxPort()
    {
      // Get application configuration:
      GetApplConfig(0, ref myInfo[0]);

      // Notify user if no channel is assigned to this application:
      if (myInfo[0].myChannelConfig == null || 
        ((myInfo[0].myChannelConfig.channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_A429) == 0))
      {
        PrintAssignError();
        return false;
      }

      myInfo[0].myPermissionMask = myInfo[0].myAccessMask;
      PrintConfig(myInfo[0].myChannelConfig);

      // First step: open a Tx port
      XLDefine.XL_Status status = myDriver.XL_OpenPort(
        ref myInfo[0].myPortHandle, 
        KAppName, 
        myInfo[0].myAccessMask, 
        ref myInfo[0].myPermissionMask, 
        0, // Since this is a Tx port rxQueueSize is not relevant!
        XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION_V4, 
        XLDefine.XL_BusTypes.XL_BUS_TYPE_A429);
      
      Console.WriteLine();
      Console.WriteLine("Open Tx Port          : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return false;
      }

      // Second step: activate the channel
      // This must always be done *before* setting channel parameter!
      status = myDriver.XL_ActivateChannel(
        myInfo[0].myPortHandle,
        myInfo[0].myAccessMask,
        XLDefine.XL_BusTypes.XL_BUS_TYPE_A429,
        XLDefine.XL_AC_Flags.XL_ACTIVATE_NONE);

      Console.WriteLine("Activate Channel      : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return false;
      }

      // Third step: set channel parameter
      var param = new XLClass.XL_A429_PARAMS();
      param.bitrate = 100000;
      param.channelDirection = XLDefine.XL_A429_MSG_CHANNEL_DIR_TX;
      param.minGap = 36; // = 4.5 bit times
      param.parity = (uint)XLDefine.XL_A429_PARAMS_Parity.XL_A429_MSG_PARITY_ODD;
      status = myDriver.XL_A429SetChannelParams(myInfo[0].myPortHandle, myInfo[0].myAccessMask, param);
      
      Console.WriteLine("Set Channel Parameter : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return false;
      }

      return true;
    }

    /// <summary>
    /// Opens a receiving port on a VN0601 device.
    /// </summary>
    /// <returns>True if successful.</returns>
    private static bool OpenRxPort()
    {
      // Get application configuration:
      GetApplConfig(1, ref myInfo[1]);

      // Notify user if no channel is assigned to this application:
      if (myInfo[1].myChannelConfig == null ||
         ((myInfo[1].myChannelConfig.channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_A429) == 0))
      {
        PrintAssignError();
        return false;
      }

      myInfo[1].myPermissionMask = myInfo[1].myAccessMask;
      PrintConfig(myInfo[1].myChannelConfig);

      // First step: open Rx port
      XLDefine.XL_Status status = myDriver.XL_OpenPort(
        ref myInfo[1].myPortHandle,
        KAppName,
        myInfo[1].myAccessMask,
        ref myInfo[1].myPermissionMask,
        XLDefine.XL_A429_RX_FIFO_QUEUE_SIZE_MIN,
        XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION_V4,
        XLDefine.XL_BusTypes.XL_BUS_TYPE_A429);

      Console.WriteLine();
      Console.WriteLine("Open Rx Port          : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return false;
      }

      // Second step: activate the channel
      // This must always be done *before* setting channel parameter!
      status = myDriver.XL_ActivateChannel(
        myInfo[1].myPortHandle,
        myInfo[1].myAccessMask,
        XLDefine.XL_BusTypes.XL_BUS_TYPE_A429,
        XLDefine.XL_AC_Flags.XL_ACTIVATE_NONE);

      Console.WriteLine("Activate Channel      : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return false;
      }

      // Third step: set channel parameter
      // We use default settings here. See class XLDefine for constants.
      var param = new XLClass.XL_A429_PARAMS();
      param.autoBaudrate = (uint)XLDefine.XL_A429_PARAMS_AutoBaudrate.XL_A429_MSG_AUTO_BAUDRATE_ENABLED;
      param.bitrate = 0; // Auto bit rate
      param.channelDirection = XLDefine.XL_A429_MSG_CHANNEL_DIR_RX;
      param.maxBitrate = XLDefine.XL_A429_MSG_BITRATE_FAST_MAX;
      param.minBitrate = XLDefine.XL_A429_MSG_BITRATE_FAST_MIN;
      param.minGap = XLDefine.XL_A429_MSG_GAP_4BIT;
      param.parity = (uint)XLDefine.XL_A429_PARAMS_Parity.XL_A429_MSG_PARITY_ODD;
      status = myDriver.XL_A429SetChannelParams(myInfo[1].myPortHandle, myInfo[1].myAccessMask, param);

      Console.WriteLine("Set Channel Parameter : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return false;
      }

      // Fourth step: get Rx event handle for notification (needed by RXThread)
      status = myDriver.XL_SetNotification(
        myInfo[1].myPortHandle,
        ref myInfo[1].myEventHandle,
        1);

      Console.WriteLine("Set Notification      : {0}", status);
      if (status != XLDefine.XL_Status.XL_SUCCESS)
      {
        PrintFunctionError();
        return false;
      }

      return true;
    }

    /// <summary>
    /// Prints an error message. Call it if channel assignment is not valid.
    /// </summary>
    private static void PrintAssignError()
    {
      Console.WriteLine();
      Console.WriteLine("Please check application settings of \"{0} ARINC429 1/ARINC429 2\", ", KAppName);
      Console.WriteLine("assign it to an available hardware channel and restart this application.");
      Console.WriteLine(KPressAnyKey);
      myDriver.XL_PopupHwConfig();
      Console.ReadKey();
    }

    /// <summary>
    /// Prints an error message. Call it if a functional call does not return XL_SUCCESS.
    /// </summary>
    private static void PrintFunctionError()
    {
      Console.WriteLine("ERROR: Function call failed!");
      Console.WriteLine(KPressAnyKey);
      Console.ReadKey();
    }

    /// <summary>
    /// Prints a short help on the program.
    /// </summary>
    private static void PrintHelp()
    {
      Console.WriteLine("Press one of the following keys to start the demo:");
      Console.WriteLine("  <b> : Block Rx thread for testing a Rx overrun");
      Console.WriteLine("  <c> : Do a cylic transmission of a ARINC message");
      Console.WriteLine("  <h> : Print this help");
      Console.WriteLine("  <s> : Stop cyclic transmission of the ARINC message");
      Console.WriteLine("  <t> : Transmit one single ARINC message");
      Console.WriteLine("  <x> : Exit the demo");
    }

    /// <summary>
    /// Prints the Vector hardware configuration.
    /// </summary>
    private static void PrintConfig(XLClass.xl_channel_config channel)
    {
      Console.WriteLine();
      Console.WriteLine("APPLICATION CONFIGURATION");
      Console.WriteLine("-------------------------------------------------------------------");
      Console.WriteLine("Configured Hardware Channel : {0}", channel.name);
      Console.WriteLine("Hardware Driver Version     : {0}", myDriver.VersionToString(channel.driverVersion));
      Console.WriteLine("Used Transceiver            : {0}", channel.transceiverName);
      Console.WriteLine("-------------------------------------------------------------------");
    }
  
    /// <summary>
    /// Receiving event thread.
    /// </summary>
    /// <remarks>
    /// RX thread waits for Vector interface events and displays received ARINC 429 messages.
    /// </remarks>
    public static void RXThread()
    {
      // Create a new object containing received data:
      var rxEvent = new XLClass.XLa429RxEvent();

      // Note: this thread will be destroyed by function Main
      while (true)
      {
        // Wait for hardware events:
        XLDefine.WaitResults waitResult = myDriver.XL_WaitForSingleObject(myInfo[1].myEventHandle, 1000);

        // If event occurred...
        if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
        {
          // initialize status first...
          var status = XLDefine.XL_Status.XL_SUCCESS;

          // then, while hardware queue is not empty...
          while (status != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
          {
            // block Rx thread to generate RX-Queue overflows...
            while (myBlockRxThread) Thread.Sleep(1000);

            // or receive data from hardware:
            status = myDriver.XL_A429Receive(myInfo[1].myPortHandle, ref rxEvent);

            //  If receiving succeed...
            if (status == XLDefine.XL_Status.XL_SUCCESS)
            {
              // check tag and process further:
              switch (rxEvent.tag)
              {
                case XLDefine.XL_A429_EventTags.XL_A429_EV_TAG_TX_OK:
                  Console.WriteLine("-- XL_A429_EV_TAG_TX_OK --");
                  break;
                case XLDefine.XL_A429_EventTags.XL_A429_EV_TAG_TX_ERR:
                  Console.WriteLine("-- XL_A429_EV_TAG_TX_ERR --");
                  break;
                case XLDefine.XL_A429_EventTags.XL_A429_EV_TAG_RX_OK:
                  Console.WriteLine("-- XL_A429_EV_TAG_RX_OK --");
                  Console.WriteLine("Bitrate: {0}", rxEvent.a429RxOkMsg.bitrate);
                  Console.WriteLine("Label  : {0}", rxEvent.a429RxOkMsg.label);
                  Console.WriteLine("Data   : {0}", rxEvent.a429RxOkMsg.data);
                  break;
                case XLDefine.XL_A429_EventTags.XL_A429_EV_TAG_RX_ERR:
                  Console.WriteLine("-- XL_A429_EV_TAG_RX_ERR --");
                  Console.WriteLine("Error  : {0}", (XLDefine.XL_A429_RX_EVENT_ErrorReason)rxEvent.a429RxErrMsg.errorReason);
                  Console.WriteLine("Label  : {0}", rxEvent.a429RxErrMsg.label);
                  Console.WriteLine("Data   : {0}", rxEvent.a429RxErrMsg.data);
                  break;
                case XLDefine.XL_A429_EventTags.XL_A429_EV_TAG_BUS_STATISTIC:
                  // This event will come periodically...
                  break;
                case XLDefine.XL_A429_EventTags.XL_SYNC_PULSE:
                  // This event will come only if synchronizing with another device.
                  break;
                default:
                  break;
              }
            }
          }
        }
      }
    }
  }
}
