/*-------------------------------------------------------------------------------------------
| File        : Class1.cs
| Project     : Vector DAIO .NET Example
|
| Description : This example demonstrates the basic DAIO functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/

using System;
using System.Runtime.InteropServices;
using System.Threading;

using vxlapi_NET;


namespace xlDAIOexample
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    class Class1
    {
        // -----------------------------------------------------------------------------------------------
        // DLL Import for RX events
        // -----------------------------------------------------------------------------------------------
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int WaitForSingleObject(int handle, int timeOut);
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        // Global variables
        // ----------------------------------------------------------------------------------------------- 
        // Driver access through XLDriver (wrapper)
        private static XLDriver DAIODemo = new XLDriver();
        private static String appName = "xlDAIOexampleNET";

        // Driver configuration
        private static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();

        // Variables required by XLDriver
        private static XLDefine.XL_HardwareType hwType  = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        private static uint hwIndex                     = 0;
        private static uint hwChannel                   = 0;        
        private static int portHandle                   = -1;
        private static int eventHandle                  = -1;
        private static UInt64 accessMask                = 0;
        private static UInt64 permissionMask            = 0;
        private static int channelIndex                 = 0;

        // RX thread
        private static Thread rxThread;

        // IO parameters
        private static uint outputMilliVolt             = 4096;
        private static uint digitalOutputMask           = 0;
        private static uint digitalInputMask            = 0;
        private static uint analogOutputMask            = 0;
        private static uint analogInputMask             = 0;
        private static uint switchMask                  = 0;
        private static uint switchState                 = 0;

        // Analog lines
        private const uint AIO0                         = 0x01;
        private const uint AIO1                         = 0x02;
        private const uint AIO2                         = 0x04;
        private const uint AIO3                         = 0x08;
        private const uint AIO_ALL                      = 0x0F;

        // Digital lines
        private const uint DIO0                         = 0x01;
        private const uint DIO1                         = 0x02;
        private const uint DIO2                         = 0x04;
        private const uint DIO3                         = 0x08;
        private const uint DIO4                         = 0x10;
        private const uint DIO5                         = 0x20;
        private const uint DIO6                         = 0x40;
        private const uint DIO7                         = 0x80;
        private const uint DIO_ALL                      = 0xFF;

        // Combined digital output lines
        private const uint OUTPUT_DIO0_DIO01            = 0x03;
        private const uint OUTPUT_DIO2_DIO03            = 0x0C;
        private const uint OUTPUT_DIO4_DIO05            = 0x30;
        private const uint OUTPUT_DIO6_DIO07            = 0xC0;

        private const uint SWITCH_DIO0_DIO01            = 0x01;
        private const uint SWITCH_DIO2_DIO03            = 0x02;
        private const uint SWITCH_DIO4_DIO05            = 0x04;
        private const uint SWITCH_DIO6_DIO07            = 0x08;
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// MAIN
        /// 
        /// Simple access to one IOcab.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        [STAThread]
        static void Main(string[] args)
        {
            XLDefine.XL_Status status;

            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("                     xlDAIOexample.NET C# V11.0                    ");
            Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.");
            Console.WriteLine("-------------------------------------------------------------------\n");

            // print .NET wrapper version
            Console.WriteLine("vxlapi_NET        : " + typeof(XLDriver).Assembly.GetName().Version);

            // Open XL Driver
            status = DAIODemo.XL_OpenDriver();
            Console.WriteLine("Open Driver       : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Get the complete XL Driver configuration, stored in driverConfig object
            status = DAIODemo.XL_GetDriverConfig(ref driverConfig);
            Console.WriteLine("Get Driver Config : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Convert the dll version into a readable string
            Console.WriteLine("DLL Version       : " + DAIODemo.VersionToString(driverConfig.dllVersion));


            // Display channel count
            Console.WriteLine("Channels found    : " + driverConfig.channelCount);


            // Display all found channels
            for (int i = 0; i < driverConfig.channelCount; i++)
            {
                Console.WriteLine("\n                   [{0}] " + driverConfig.channel[i].name, i);
                Console.WriteLine("                    - Channel Mask    : " + driverConfig.channel[i].channelMask);
                Console.WriteLine("                    - Transceiver Name: " + driverConfig.channel[i].transceiverName);
                Console.WriteLine("                    - Serial Number   : " + driverConfig.channel[i].serialNumber);
            }

            // If the application name cannot be found in VCANCONF....
            if ((DAIODemo.XL_GetApplConfig(appName, 0, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO) != XLDefine.XL_Status.XL_SUCCESS)) 
            {
                //...create the item with one channel
                DAIODemo.XL_SetApplConfig(appName, 0, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO);
                PrintAssignErrorAndPopupHwConf();
            }

            // request the user to assign a usable DAIO channel
            while (!GetAppChannelAndTestIsOk(0, ref accessMask, ref channelIndex))
            {
                PrintAssignErrorAndPopupHwConf();
            }

            PrintConfig();
            permissionMask = accessMask;

            // Open port
            status = DAIODemo.XL_OpenPort(ref portHandle, appName, accessMask, ref permissionMask, 1024, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO);
            Console.WriteLine("\n\nOpen Port             : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Get RX event handle
            status = DAIODemo.XL_SetNotification(portHandle, ref eventHandle, 1);
            Console.WriteLine("Set Notification      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Set measurement frequency (will generate xlEvents)
            status = DAIODemo.XL_DAIOSetMeasurementFrequency(portHandle, accessMask, 1000);
            Console.WriteLine("MeasurementFrequency  : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // DIO0/DIO2 = Output, remaining pins = Input
            switchMask = SWITCH_DIO0_DIO01;
            digitalOutputMask = OUTPUT_DIO0_DIO01;
            digitalInputMask = DIO_ALL & (~digitalOutputMask);

            status = DAIODemo.XL_DAIOSetDigitalParameters(portHandle, accessMask, digitalInputMask, digitalOutputMask);
            Console.WriteLine("DigitalParameters     : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // AIO0 = Output, remaining pins = Input
            analogOutputMask = AIO0;
            analogInputMask = AIO_ALL & (~analogOutputMask);
            status = DAIODemo.XL_DAIOSetAnalogParameters(portHandle, accessMask, analogInputMask, analogOutputMask, 0x03);
            Console.WriteLine("AnalogParameters      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Activate channel
            status = DAIODemo.XL_ActivateChannel(portHandle, accessMask, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO, XLDefine.XL_AC_Flags.XL_ACTIVATE_NONE);
            Console.WriteLine("Activate Channel      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Reset time stamp clock
            status = DAIODemo.XL_ResetClock(portHandle);
            Console.WriteLine("Reset Clock           : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Set analog output (for power supply of DIO0)
            status = DAIODemo.XL_DAIOSetAnalogOutput(portHandle, accessMask, outputMilliVolt, 0, 0, 0);
            Console.WriteLine("Analog Output (" + outputMilliVolt.ToString() + ")  : " + status + "\n\n");
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // INITIALIZATION FINISHED
            Console.WriteLine(" >> Press [ENTER] during measurement to toggle switches.");
            Console.WriteLine(" >> Press [x]+[ENTER] to stop measurement.");
            Console.WriteLine(" >> Press [ENTER] to start...");
            Console.WriteLine("-------------------------------------------------------------------\n");
            Console.Read();

            // Run Rx Thread
            Console.WriteLine("Start Rx Thread...");
            rxThread = new Thread(new ThreadStart(RXThread));
            rxThread.Start();

            // Toggle output
            while (Console.ReadLine() != "x")
            {
                ToggleSwitch();
            }

            // Kill Rx thread
            rxThread.Abort();
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message/exit in case of a functional call does not return XL_SUCCESS
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static int PrintFunctionError()
        {
            Console.WriteLine("\nERROR: Function call failed!\nPress any key to continue...");
            Console.ReadKey();
            return -1;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Toggles output
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void ToggleSwitch()
        {
            switch (switchState)
            {
                case 0x00: switchState = 0x0F; break;
                case 0x0F: switchState = 0x00; break;
            }

            DAIODemo.XL_DAIOSetDigitalOutput(portHandle, accessMask, switchMask, switchState);
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Displays Vector Hardware Configuration.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintConfig()
        {
            Console.WriteLine("\n\nAPPLICATION CONFIGURATION");
            Console.WriteLine("-------------------------------------------------------------------\n");
            Console.WriteLine("Configured Hardware Channel : " + driverConfig.channel[channelIndex].name);
            Console.WriteLine("Hardware Driver Version     : " + DAIODemo.VersionToString(driverConfig.channel[channelIndex].driverVersion));
            Console.WriteLine("Used Transceiver            : " + driverConfig.channel[channelIndex].transceiverName);
            Console.WriteLine("-------------------------------------------------------------------\n");
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message if channel assignment is not valid and popup VHwConfig, so the user can correct the assignment
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintAssignErrorAndPopupHwConf()
        {
            Console.WriteLine("\nPlease check application settings of \"" + appName + " DAIO 1\",\nassign it to an available hardware channel and press enter.");
            DAIODemo.XL_PopupHwConfig();
            Console.ReadKey();
        }
        // -----------------------------------------------------------------------------------------------



        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve the application channel assignment and test if this channel can be opened
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static bool GetAppChannelAndTestIsOk(uint appChIdx, ref UInt64 chMask, ref int chIdx)
        {
            XLDefine.XL_Status status = DAIODemo.XL_GetApplConfig(appName, appChIdx, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                Console.WriteLine("XL_GetApplConfig      : " + status);
                PrintFunctionError();
            }

            chMask = DAIODemo.XL_GetChannelMask(hwType, (int)hwIndex, (int)hwChannel);
            chIdx = DAIODemo.XL_GetChannelIndex(hwType, (int)hwIndex, (int)hwChannel);
            if (chIdx < 0 || chIdx >= driverConfig.channelCount)
            {
                // the (hwType, hwIndex, hwChannel) triplet stored in the application configuration does not refer to any available channel.
                return false;
            }

            // test if DAIO is available on this channel
            return (driverConfig.channel[chIdx].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_DAIO) != 0;
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// EVENT THREAD (RX)
        /// RX thread waits for Vector interface events and displays filtered DAIO messages.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void RXThread()
        {
            // Create new object containing received data 
            XLClass.xl_event receivedEvent = new XLClass.xl_event();  
            
            // Result of XL Driver function calls
            XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_SUCCESS;

            // Result values of WaitForSingleObject 
            XLDefine.WaitResults waitResult = new XLDefine.WaitResults(); 
            
           

            // Note: this thread will be destroyed by MAIN
            while (true)
            {
                // Wait for hardware events
                waitResult = (XLDefine.WaitResults)WaitForSingleObject(eventHandle, 1000);

                // If event occurred...
                if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
                {
                    // ...init xlStatus first
                    xlStatus = XLDefine.XL_Status.XL_SUCCESS;

                    // Afterwards: while hw queue is not empty...
                    while (xlStatus != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
                    {
                        // ...receive data from hardware.
                        xlStatus = DAIODemo.XL_Receive(portHandle, ref receivedEvent);

                        //  If receiving succeed....
                        if (xlStatus == XLDefine.XL_Status.XL_SUCCESS)
                        {
                            // ...and data was Rx msg...
                            if (receivedEvent.tag == XLDefine.XL_EventTags.XL_RECEIVE_DAIO_DATA)
                            {
                                Console.WriteLine("\n - AIO0               : " + receivedEvent.tagData.daioData.value_analog[0]);
                                Console.WriteLine(" - AIO1               : " + receivedEvent.tagData.daioData.value_analog[1]);
                                Console.WriteLine(" - AIO2               : " + receivedEvent.tagData.daioData.value_analog[2]);
                                Console.WriteLine(" - AIO3               : " + receivedEvent.tagData.daioData.value_analog[3]);

                                Console.Write(" - Switch selected    : ");
                                if ((switchMask & 1) == 1) Console.Write("DIO0/DIO1  ");
                                if ((switchMask & 2) == 2) Console.Write("DIO2/DIO3  ");
                                if ((switchMask & 4) == 4) Console.Write("DIO4/DIO5  ");
                                if ((switchMask & 8) == 8) Console.Write("DIO6/DIO7  ");
                                Console.WriteLine("");
                                switch (switchState)
                                {
                                    case 0: Console.WriteLine(" - Switch states      : OPEN"); break;
                                    default: Console.WriteLine(" - Switch states      : CLOSED"); break;
                                }

                                Console.WriteLine(" - Digital Port       : DIO7|DIO6|DIO5|DIO4|DIO3|DI2|DIO1|DIO0|(value)");
                                Console.WriteLine("                           {0:x}|   {1:x}|   {2:x}|   {3:x}|   {4:x}|   {5:x}|   {6:x}|  {7:x}|  ({8:x})  ",
                                    ((receivedEvent.tagData.daioData.value_digital & 128) >> 7),
                                    ((receivedEvent.tagData.daioData.value_digital & 64) >> 6),
                                    ((receivedEvent.tagData.daioData.value_digital & 32) >> 5),
                                    ((receivedEvent.tagData.daioData.value_digital & 16) >> 4),
                                    ((receivedEvent.tagData.daioData.value_digital & 8) >> 3),
                                    ((receivedEvent.tagData.daioData.value_digital & 4) >> 2),
                                    ((receivedEvent.tagData.daioData.value_digital & 2) >> 1),
                                    ((receivedEvent.tagData.daioData.value_digital & 1)),
                                    receivedEvent.tagData.daioData.value_digital);
                            }
                        }
                    }
                }
                // No event occured
            }
        }
        // -----------------------------------------------------------------------------------------------
    }
}
