Option Explicit On

Imports System
Imports System.Runtime.InteropServices
Imports System.Threading
Imports vxlapi_NET


Namespace xlCANdemo

    ' <summary>
    ' Summary description for Class1.
    ' </summary>
    Class Class1

        ' -----------------------------------------------------------------------------------------------
        ' DLL Import for RX events
        ' -----------------------------------------------------------------------------------------------
        <DllImport("kernel32.dll", SetLastError:=True)> _
        Shared Function WaitForSingleObject(ByVal handle As Integer, ByVal timeOut As Integer) As Integer
        End Function
        ' -----------------------------------------------------------------------------------------------



        ' -----------------------------------------------------------------------------------------------
        ' Global variables
        ' -----------------------------------------------------------------------------------------------
        'create port channel object
        Shared appName As String = "xlCANdemoNET"
        Shared CANDemo As XLDriver = New XLDriver
        Shared driverConfig As XLClass.xl_driver_config = New XLClass.xl_driver_config
        Shared rxThread As Thread
        Shared input As String
        Shared hwType As UInt32
        Shared hwIndex As UInt32
        Shared hwChannel As UInt32
        Shared busTypeCAN As XLDefine.XL_BusTypes = XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN
        Shared flags As UInt32
        Shared portHandle As Int32
        Shared eventHandle As Int32
        Shared accessMask As UInt64
        Shared txMask As UInt64
        Shared permissionMask As UInt64
        Shared channelIndex As Int32
        ' -----------------------------------------------------------------------------------------------



        ' -----------------------------------------------------------------------------------------------
        ' <summary>
        ' MAIN
        '
        ' Sends and receives CAN messages using main methods of the "XLDriver" class.
        ' This demo requires two connected CAN channels (Vector network interface).
        ' The configuration is read from Vector Hardware Config (vcanconf.exe).
        ' </summary>
        ' -----------------------------------------------------------------------------------------------
        <STAThread()> _
        Shared Sub Main()

            Dim i As Integer

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine("                    xlCANdemo.NET VBnet V11.0                      ")
            Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.")
            Console.WriteLine("-------------------------------------------------------------------")

            ' Open XL Driver
            Console.WriteLine("Open Driver       : " + CANDemo.XL_OpenDriver().ToString())

            ' Get the complete XL Driver configuration, stored in driverConfig object
            Console.WriteLine("Get Driver Config : " + CANDemo.XL_GetDriverConfig(driverConfig).ToString())

            ' Convert the dll version to a readable string
            Console.WriteLine("DLL Version       : " + CANDemo.VersionToString(driverConfig.dllVersion))

            ' Display channel count
            Console.WriteLine("Channels found    : " + driverConfig.channelCount.ToString())

            ' Display found channels
            For i = 0 To (Convert.ToInt32(driverConfig.channelCount) - Convert.ToInt32(1))
                Console.WriteLine("                   [{0}] " + driverConfig.channel(i).name, i)
                Console.WriteLine("                    - Channel Mask    : " + driverConfig.channel(i).channelMask.ToString())
                Console.WriteLine("                    - Transceiver Name: " + driverConfig.channel(i).transceiverName.ToString())
                Console.WriteLine("                    - Serial Number   : " + driverConfig.channel(i).serialNumber.ToString())
                Console.WriteLine()
            Next

            ' If application not found in VCANCONF....
            If CANDemo.XL_GetApplConfig(appName, Convert.ToUInt32(0), hwType, hwIndex, hwChannel, busTypeCAN) <> XLDefine.XL_Status.XL_SUCCESS Or CANDemo.XL_GetApplConfig(appName, Convert.ToUInt32(1), hwType, hwIndex, hwChannel, busTypeCAN) <> XLDefine.XL_Status.XL_SUCCESS Then

                ' ..create the item with 2 channels
                CANDemo.XL_SetApplConfig(appName, Convert.ToUInt32(0), Convert.ToUInt32(0), Convert.ToUInt32(0), Convert.ToUInt32(0), Convert.ToUInt32(0))
                CANDemo.XL_SetApplConfig(appName, Convert.ToUInt32(1), Convert.ToUInt32(0), Convert.ToUInt32(0), Convert.ToUInt32(0), Convert.ToUInt32(0))
                PrintAssignError()

            Else
                Try

                    ' Read setting of CAN1
                    CANDemo.XL_GetApplConfig(appName, Convert.ToUInt32(0), hwType, hwIndex, hwChannel, busTypeCAN)
                    accessMask = CANDemo.XL_GetChannelMask(Convert.ToInt32(hwType), Convert.ToInt32(hwIndex), Convert.ToInt32(hwChannel))
                    txMask = accessMask
                    PrintConfig()

                    ' Read setting of CAN2
                    CANDemo.XL_GetApplConfig(appName, Convert.ToUInt32(1), hwType, hwIndex, hwChannel, busTypeCAN)
                    accessMask = Convert.ToUInt64(Convert.ToInt32(accessMask) Or Convert.ToInt32(CANDemo.XL_GetChannelMask(Convert.ToInt32(hwType), Convert.ToInt32(hwIndex), Convert.ToInt32(hwChannel))))

                    permissionMask = Convert.ToUInt64(Convert.ToInt32(accessMask) Or Convert.ToInt32(accessMask))
                    PrintConfig()

                    ' Open port
                    Console.WriteLine("Open Port             : " + CANDemo.XL_OpenPort(portHandle, appName, accessMask, permissionMask, Convert.ToUInt32(1024), XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION, busTypeCAN).ToString())

                    ' Check port
                    Console.WriteLine("Can Request Chip State: " + CANDemo.XL_CanRequestChipState(portHandle, accessMask).ToString())

                    ' Activate channel
                    Console.WriteLine("Activate Channel      : " + CANDemo.XL_ActivateChannel(portHandle, accessMask, busTypeCAN, flags).ToString())

                    ' Get RX event handle
                    Console.WriteLine("Set Notification      : " + CANDemo.XL_SetNotification(portHandle, eventHandle, 1).ToString())

                    ' Reset time stamp clock
                    Console.WriteLine("Reset Clock           : " + CANDemo.XL_ResetClock(portHandle).ToString())

                    ' Create and start Rx thread
                    Dim rxThread As New Thread(AddressOf RX_Thread)
                    rxThread.Start()


                    Console.WriteLine("Press <ENTER> to transmit CAN data. Press <x>, <ENTER> to close application.")
                    While Not (Console.ReadLine Like "[xX]")
                        ' Transmit some data				
                        CANTransmitDemo()
                        Console.WriteLine("Transmitted")

                    End While

                    rxThread.Abort()


                Catch
                    ' No channel assigned to hardware
                    PrintAssignError()
                End Try
            End If
        End Sub
        ' -----------------------------------------------------------------------------------------------



        ' -----------------------------------------------------------------------------------------------
        ' <summary>
        ' Displays Vector Hardware Configuration.
        ' </summary>
        ' -----------------------------------------------------------------------------------------------
        Public Shared Sub PrintConfig()
            channelIndex = CANDemo.XL_GetChannelIndex(Convert.ToInt32(hwType), Convert.ToInt32(hwIndex), Convert.ToInt32(hwChannel))
            Console.WriteLine("APPLICATION CONFIGURATION")
            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine("Configured Hardware Channel : " + driverConfig.channel(channelIndex).name)
            Console.WriteLine("Hardware Driver Version     : " + CANDemo.VersionToString(driverConfig.channel(channelIndex).driverVersion))
            Console.WriteLine("Used Transceiver            : " + driverConfig.channel(channelIndex).transceiverName)
            Console.WriteLine("-------------------------------------------------------------------")
        End Sub
        ' -----------------------------------------------------------------------------------------------



        ' -----------------------------------------------------------------------------------------------
        ' <summary>
        ' Error message if channel assignment is not valid.
        ' </summary>
        ' -----------------------------------------------------------------------------------------------
        Public Shared Sub PrintAssignError()
            Console.WriteLine(Chr(10) + "Please check application settings of" + Chr(34) + appName + " CAN1/CAN2" + Chr(34) + Chr(10) + "and assign it to an available hardware channel and restart application.")
            CANDemo.XL_PopupHwConfig()
            Console.ReadLine()
        End Sub
        ' -----------------------------------------------------------------------------------------------



        ' -----------------------------------------------------------------------------------------------
        ' <summary>
        ' Sends some CAN messages.
        ' </summary>
        ' -----------------------------------------------------------------------------------------------
        Public Shared Sub CANTransmitDemo()

            Dim xlEventCollection As XLClass.xl_event_collection = New XLClass.xl_event_collection(Convert.ToUInt32(2))

            xlEventCollection.xlEvent(0).tagData.can_Msg.id = Convert.ToUInt32(&H100)
            xlEventCollection.xlEvent(0).tagData.can_Msg.dlc = Convert.ToUInt16(8)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(0) = Convert.ToByte(1)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(1) = Convert.ToByte(2)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(2) = Convert.ToByte(3)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(3) = Convert.ToByte(4)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(4) = Convert.ToByte(5)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(5) = Convert.ToByte(6)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(6) = Convert.ToByte(7)
            xlEventCollection.xlEvent(0).tagData.can_Msg.data(7) = Convert.ToByte(8)
            xlEventCollection.xlEvent(0).tag = XLDefine.XL_EventTags.XL_TRANSMIT_MSG

            xlEventCollection.xlEvent(1).tagData.can_Msg.id = Convert.ToUInt32(&H200)
            xlEventCollection.xlEvent(1).tagData.can_Msg.dlc = Convert.ToUInt16(8)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(0) = Convert.ToByte(9)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(1) = Convert.ToByte(10)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(2) = Convert.ToByte(11)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(3) = Convert.ToByte(12)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(4) = Convert.ToByte(13)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(5) = Convert.ToByte(14)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(6) = Convert.ToByte(15)
            xlEventCollection.xlEvent(1).tagData.can_Msg.data(7) = Convert.ToByte(16)
            xlEventCollection.xlEvent(1).tag = XLDefine.XL_EventTags.XL_TRANSMIT_MSG

            Console.WriteLine("Transmit Message      : " + CANDemo.XL_CanTransmit(portHandle, txMask, xlEventCollection).ToString())
        End Sub
        ' -----------------------------------------------------------------------------------------------



        ' -----------------------------------------------------------------------------------------------
        ' <summary>
        ' RXThread
        ' RX Thread waits for Vector interface events and displays filtered CAN messages.
        ' </summary>
        ' -----------------------------------------------------------------------------------------------
        Public Shared Sub RX_Thread()

            ' Create new object containing received data 
            Dim receivedEvent As XLClass.xl_event = New XLClass.xl_event

            ' Result of XL Driver function calls
            Dim xlStatus As XLDefine.XL_Status = XLDefine.XL_Status.XL_SUCCESS

            ' Result values of WaitForSingleObject 
            Dim waitResult As XLDefine.WaitResults = New XLDefine.WaitResults


            ' Note: this thread will be destroyed by MAIN
            While (True)

                ' Wait for hardware events
                waitResult = WaitForSingleObject(eventHandle, 1000)

                ' If event occured... 
                If (waitResult <> XLDefine.WaitResults.WAIT_TIMEOUT) Then

                    ' init xlStatus
                    xlStatus = XLDefine.XL_Status.XL_SUCCESS

                    ' afterwards: while hw queue not empty..,
                    While (xlStatus <> XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)

                        ' ...receive data from hardware queue.
                        xlStatus = CANDemo.XL_Receive(portHandle, receivedEvent)

                        '  If receive succeed....
                        If (xlStatus = XLDefine.XL_Status.XL_SUCCESS) Then

                            ' ...and data was Rx msg...
                            If (receivedEvent.tag = XLDefine.XL_EventTags.XL_RECEIVE_MSG) Then

                                ' Check various flags
                                If ((receivedEvent.tagData.can_Msg.flags And XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME) = XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME) Then
                                    Console.WriteLine("ERROR FRAME")

                                ElseIf ((receivedEvent.tagData.can_Msg.flags And XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME) = XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME) Then
                                    Console.WriteLine("REMOTE FRAME")

                                    ' Print only "real" Rx messages
                                ElseIf ((receivedEvent.tagData.can_Msg.flags And XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_TX_COMPLETED) = 0) Then
                                    Console.WriteLine(CANDemo.XL_GetEventString(receivedEvent))

                                End If

                            End If
                        End If
                    End While
                End If
                ' No event occured
            End While
            ' ------------------------------------------------------------------
        End Sub
    End Class
End Namespace
