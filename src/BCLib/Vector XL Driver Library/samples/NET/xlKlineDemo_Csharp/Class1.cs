﻿/*-------------------------------------------------------------------------------------------
| File        : Class1.cs
| Project     : Vector KLine .NET Example
|
| Description : This example demonstrates the basic KLine functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/

using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text;
using vxlapi_NET;

namespace xlKLineDemo
{
  /// <summary>
  /// Summary description for Class1.
  /// </summary>
  class Class1
  {
    // -----------------------------------------------------------------------------------------------
    // DLL Import for RX events
    // -----------------------------------------------------------------------------------------------
    [DllImport("kernel32.dll", SetLastError = true)]
    static extern int WaitForSingleObject(int handle, int timeOut);


    private const int RECEIVE_EVENT_SIZE   =  1;                // DO NOT EDIT! Currently 1 is supported only
    private const int MAX_CH               = 16;                // We have MAX_CH for one port

    private const int FUNC_UART            =  0;                // UART operation mode
    private const int FUNC_TESTER          =  1;                // Tester operation mode
    private const int FUNC_ECUFI           =  2;                // ECU operation mode (fast init)
    private const int FUNC_ECU5BD          =  3;                // ECU opertaion mode (5Bd init)

    private const int ECU_ADDR             = 0x97;              // ECU address during 5Bd init

    private const int XL_INVALID_PORTHANDLE = (-1);

    /* ============================================================================== */
    /*                                                                                */
    /*      GLOBAL VARIABLES                                                          */
    /*                                                                                */
    /* ============================================================================== */

    // Driver access through XLDriver (wrapper)
    private static XLDriver        mDriver                      = new XLDriver();
    // Contains the actual hardware configuration
    private static XLClass.xl_driver_config g_xlDrvConfig = new XLClass.xl_driver_config();

    private static String          g_AppName                    = "xlKlineDemoNET";       //!< Application name which is displayed in VHWconf
    private static int             g_xlPhKline                  = XL_INVALID_PORTHANDLE;  //!< Global porthandle (we use only one!)
    private static UInt64          g_xlCmKline                  = 0;                      //!< Global channelmask (includes all found channels)
    private static UInt64[]        g_xlCmSelKline               = new UInt64[MAX_CH];     //!< Global channelmask array (a single channel per entry)
    private static int             g_KlineChCtr                 = 0;                      //!< Global counter of K-Line channels
    private static uint            g_BaudRate                   = 10400;                  //!< Default KLine baudrate
    private static bool            g_silent                     = true;                   //!< flag to print the message events (on/off)
    private static bool            g_stop                       = false;                  //!< flag to stop the application
    private static uint            g_TimerRate                  = 0;                      //!< Global timerrate (to toggle)
    private static uint            g_TesterResistor             = 0;                      //!< Flag to toggle tester resistor (default off)
    private static bool            g_activated                  = false;                  //!< Global flag to indicate if the K-Line channels are active or not
    private static uint            g_HsMode                     = 0;                      //!< Global flag to toggle high speed mode (on/off)
    private static uint            g_Function                   = FUNC_UART;              //!< Current function: UART, Tester, ECU
    
    // RX thread
    private static Thread g_RXThread;
    private static int g_hKlineMsgEvent = -1;

    /* ============================================================================== */
    /*                                                                                */
    /*      LOCAL FUNCTION DEFINITIONS                                                */
    /*                                                                                */
    /* ============================================================================== */

    //------------------------------------------------------------------------------
    //   Name:           demoHelp                                   
    //   Description:    Shows the program functionality
    //------------------------------------------------------------------------------
    private static void demoHelp() {

      Console.WriteLine();
      Console.WriteLine("----------------------------------------------------------");
      Console.WriteLine("-                  xlKlineDemo - HELP                    -");
      Console.WriteLine("----------------------------------------------------------");
      Console.WriteLine("- Keyboard commands:                                     -");
      Console.WriteLine("- 'v'      Toggle verbose mode                           -");
      Console.WriteLine("- 't'      Transmit a message                            -");
      Console.WriteLine("- 'T'      Transmit a long message                       -");
      Console.WriteLine("- 'i'      Switch tester resistor                        -");
      Console.WriteLine("- 'a'      Toggle timer rate                             -");
      Console.WriteLine("- 'r'      Reset clock                                   -");
      Console.WriteLine("- 'G'      Generate sync pulse - K-Line                  -");
      Console.WriteLine("- 'f'      Exec fastInit (only tester)                   -");
      Console.WriteLine("- '5'      Exec 5BD Init (only tester)                   -");
      Console.WriteLine("- 'H'      Switch highspeed mode                         -");
      Console.WriteLine("- 's'      Activate/deactivate channels                  -");
      Console.WriteLine("- 'p'      Show hardware configuration                   -");
      Console.WriteLine("- 'h'      Help                                          -");
      Console.WriteLine("- 'ESC'    Exit                                          -");
      Console.WriteLine("----------------------------------------------------------");
      Console.WriteLine("- 'PH'->PortHandle; 'CM'->ChannelMask; 'PM'->Permission  -");
      Console.WriteLine("----------------------------------------------------------");
      Console.WriteLine();

    }


    //------------------------------------------------------------------------------
    //   Name:           demoPrintConfig                                   
    //   Description:    Shows the actual hardware configuration
    //------------------------------------------------------------------------------
    private static void demoPrintConfig() {

      uint i;

      Console.WriteLine("----------------------------------------------------------");
      Console.WriteLine("- {0} channels       Hardware Configuration               -", g_xlDrvConfig.channelCount);
      Console.WriteLine("----------------------------------------------------------");

      for (i=0; i < g_xlDrvConfig.channelCount; i++) {

        if (g_xlDrvConfig.channel[i].transceiverType != XLDefine.XL_Transceiver_Types.XL_TRANSCEIVER_TYPE_NONE)
        {
          Console.WriteLine("- Ch:{0,2:D}, CM:0x{1:X3}, {2,-20} {3,-13} -",
          g_xlDrvConfig.channel[i].channelIndex, g_xlDrvConfig.channel[i].channelMask, g_xlDrvConfig.channel[i].name, g_xlDrvConfig.channel[i].transceiverName);
        }
        else {
          Console.WriteLine("- Ch:{0,2:D}, CM:0x{1:X3}, {2,-20} {3,-13} -",
          g_xlDrvConfig.channel[i].channelIndex, g_xlDrvConfig.channel[i].channelMask, g_xlDrvConfig.channel[i].name, "no transceiver");
        }
    
      }

      Console.WriteLine("----------------------------------------------------------");
      Console.WriteLine();
 
    }


    //------------------------------------------------------------------------------
    //   Name:           demoToggleVerbose                                   
    //   Description:    Switch verbose mode on/off
    //------------------------------------------------------------------------------
    private static void demoToggleVerbose() {
      if (g_silent) { 
        g_silent = false;
        Console.WriteLine("demoToggleVerbose, verbose mode ON"); 
      }
      else {
        g_silent = true;
        Console.WriteLine("demoToggleVerbose, verbose mode OFF");
      }
    } // demoToggleVerbose


    //------------------------------------------------------------------------------
    //   Name:           demoToggleTesterResistor                                   
    //   Description:    Switch tester resistor on/off
    //------------------------------------------------------------------------------
    private static void demoToggleTesterResistor(UInt64 xlChanMask) {
      XLDefine.XL_Status xlStatus;
      XLDefine.XL_KLine_TesterResistor resistor;

      if (g_TesterResistor == (uint)XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_ON)
      {
        resistor = XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_OFF;
      }
      else {
        resistor = XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_ON;
      }
      xlStatus = mDriver.XL_KlineSwitchTesterResistor(g_xlPhKline, xlChanMask, resistor);
      Console.WriteLine("demoToggleTesterResistor, XL_KlineSwitchTesterResistor (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS == xlStatus)
      {
        g_TesterResistor = (uint)resistor;
      }
      Console.WriteLine("demoToggleTesterResistor, g_TesterResistor: {0}", ((uint)XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_ON == g_TesterResistor) ? "ON" : "OFF");
    } // demoToggleTesterResistor


    //------------------------------------------------------------------------------
    //   Name:           demoToggleTimerRate                                   
    //   Description:    Toggle the rate of cyclic timer events 1ms/off
    //------------------------------------------------------------------------------
    private static void demoToggleTimerRate() {
      XLDefine.XL_Status xlStatus;
      uint rate;

      if (g_TimerRate != 0) {
        rate = 0;
      }
      else {
        rate = 100;
      }
      xlStatus = mDriver.XL_SetTimerRate(g_xlPhKline, rate);
      Console.WriteLine("demoToggleTimerRate, xlSetTimerRate (timerRate:{0}) result: {1}", rate, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS == xlStatus)
      {
        g_TimerRate = rate;
      }
      Console.WriteLine("demoToggleTimerRate, g_TimerRate: {0}", g_TimerRate);
    } // demoToggleTimerRate


    //------------------------------------------------------------------------------
    //   Name:           demoToggleHighspeedMode                                   
    //   Description:    Toggle high speed mode on/off
    //------------------------------------------------------------------------------
    private static void demoToggleHighspeedMode(UInt64 xlChanMask) {
      XLDefine.XL_Status xlStatus;
      XLDefine.XL_KLine_TrxMode mode;

      if (g_HsMode == (uint)XLDefine.XL_KLine_TrxMode.XL_KLINE_TRXMODE_HIGHSPEED)
      {
        mode = XLDefine.XL_KLine_TrxMode.XL_KLINE_TRXMODE_NORMAL;
      }
      else {
        mode = XLDefine.XL_KLine_TrxMode.XL_KLINE_TRXMODE_HIGHSPEED;
      }
      xlStatus = mDriver.XL_KlineSwitchHighspeedMode(g_xlPhKline, xlChanMask, mode);
      Console.WriteLine("demoToggleHighspeedMode, XL_KlineSwitchHighspeedMode (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS == xlStatus)
      {
        g_HsMode = (uint)mode;
      }
      Console.WriteLine("demoToggleHighspeedMode, g_HsMode: {0}", ((uint)XLDefine.XL_KLine_TrxMode.XL_KLINE_TRXMODE_HIGHSPEED == g_HsMode) ? "HIGHSPEED" : "NORMAL");
    } // demoToggleHighspeedMode


    //------------------------------------------------------------------------------
    //   Name:           demoResetClock                                   
    //   Description:    Reset clock of the K-Line port
    //------------------------------------------------------------------------------
    private static void demoResetClock() {
      XLDefine.XL_Status xlStatus;

      if (XL_INVALID_PORTHANDLE != g_xlPhKline) {
        xlStatus = mDriver.XL_ResetClock(g_xlPhKline);
        Console.WriteLine("demoResetClock, xlResetClock result: {0}", mDriver.XL_GetErrorString(xlStatus));
      }
      else {
        Console.WriteLine("demoResetClock, invalid port handle!");
      }
    } // demoResetClock


    //------------------------------------------------------------------------------
    //   Name:           demoGenerateSyncPulse                                   
    //   Description:    Generate a sync pulse event on the selected channel(s)
    //------------------------------------------------------------------------------
    private static void demoGenerateSyncPulse(UInt64 xlChanMask) {
      XLDefine.XL_Status xlStatus;

      xlStatus = mDriver.XL_GenerateSyncPulse(g_xlPhKline, xlChanMask);
      Console.WriteLine("demoGenerateSyncPulse, xlGenerateSyncPulse (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));
    } // demoGenerateSyncPulse


    //------------------------------------------------------------------------------
    //   Name:           demoTransmit                                   
    //   Description:    Transmit a K-Line message
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status demoTransmit(UInt64 xlChanMask, byte[] buf, uint len) {
      XLDefine.XL_Status xlStatus;
  
      xlStatus = mDriver.XL_KlineTransmit(g_xlPhKline, xlChanMask, len, buf);
      Console.WriteLine("demoTransmit, XL_KlineTransmit (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));

      return xlStatus;
    }


    //------------------------------------------------------------------------------
    //   Name:           demoStartStop                                   
    //   Description:    Toggle channel activation/deactivation
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status demoStartStop() {
      XLDefine.XL_Status xlStatus;

      if (XL_INVALID_PORTHANDLE != g_xlPhKline) {
        if (!g_activated) {
          xlStatus = mDriver.XL_ActivateChannel(g_xlPhKline, g_xlCmKline, XLDefine.XL_BusTypes.XL_BUS_TYPE_KLINE, XLDefine.XL_AC_Flags.XL_ACTIVATE_RESET_CLOCK);
          Console.WriteLine("demoStartStop, xlActivateChannel (PH:{0}, CM:0x{1:X}) result: {2}", g_xlPhKline, g_xlCmKline, mDriver.XL_GetErrorString(xlStatus));
          if (XLDefine.XL_Status.XL_SUCCESS == xlStatus)
          {
            g_activated = true;
          }
        }
        else {
          xlStatus = mDriver.XL_DeactivateChannel(g_xlPhKline, g_xlCmKline);
          Console.WriteLine("demoStartStop, xlDeactivateChannel (PH:{0}, CM:0x{1:X}) result: {2}", g_xlPhKline, g_xlCmKline, mDriver.XL_GetErrorString(xlStatus));
          if (XLDefine.XL_Status.XL_SUCCESS == xlStatus)
          {
            g_activated = false;
          }
        }
      }
      else {
        xlStatus = XLDefine.XL_Status.XL_ERR_INVALID_HANDLE;
        Console.WriteLine("demoStartStop, invalid port handle!");
      }

      return xlStatus;
    }


    //------------------------------------------------------------------------------
    //   Name:           demoCreateRxThread                                   
    //   Description:    Set event notification and create a receive thread
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status demoCreateRxThread() {

      XLDefine.XL_Status        xlStatus = XLDefine.XL_Status.XL_ERROR;
 
      // create the K-Line thread
      if (XL_INVALID_PORTHANDLE != g_xlPhKline) {

        // Send a event for each Msg!!!
        xlStatus = mDriver.XL_SetNotification(g_xlPhKline, ref g_hKlineMsgEvent, 1);
        if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
        {
          Console.WriteLine("demoCreateRxThread, XL_SetNotification result: {0}", mDriver.XL_GetErrorString(xlStatus));
          return xlStatus;
        }

        try
        {
          g_RXThread = new Thread(new ThreadStart(RXThread));
          g_RXThread.Start();
          xlStatus = XLDefine.XL_Status.XL_SUCCESS;
          Console.WriteLine("demoCreateRxThread, RX thread started.");
        }
        catch (System.Exception ex)
        {
          xlStatus = XLDefine.XL_Status.XL_ERROR;
          Console.WriteLine("demoCreateRxThread, Rx thread not started. Reason: {0}", ex.Message);
        }
      }
      else {
        xlStatus = XLDefine.XL_Status.XL_ERR_INVALID_HANDLE;
        Console.WriteLine("demoCreateRxThread, invalid port handle!");
      }

      return xlStatus;
    }


    //------------------------------------------------------------------------------
    //   Name:           exec5BdInitTester                                   
    //   Description:    Execute 5Bd init sequence (tester only)
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status exec5BdInitTester(UInt64 xlChanMaskTester) {
      XLDefine.XL_Status          xlStatus;
      XLClass.xl_kline_init_5BdTester xlKline5BdTester = new XLClass.xl_kline_init_5BdTester();

      xlKline5BdTester.addr    = ECU_ADDR;
      xlKline5BdTester.rate5bd = 50;
      xlKline5BdTester.W1min   = 5000;    //  5ms
      xlKline5BdTester.W1max   = 50000;   // 50ms
      xlKline5BdTester.W2min   = 5000;    //  5ms
      xlKline5BdTester.W2max   = 7000;    //  7ms
      xlKline5BdTester.W3min   = 7000;    //  7ms
      xlKline5BdTester.W3max   = 9000;    //  9ms
      xlKline5BdTester.W4min   = 25000;   // 25ms
      xlKline5BdTester.W4max   = 30000;   // 30ms
      xlKline5BdTester.W4      = 15000;   // 15ms

      xlStatus = mDriver.XL_KlineInit5BdTester(g_xlPhKline, xlChanMaskTester, xlKline5BdTester);
      Console.WriteLine("exec5BdInitTester, XL_KlineInit5BdTester result: {0}", mDriver.XL_GetErrorString(xlStatus));

      return xlStatus;
    }


    //------------------------------------------------------------------------------
    //   Name:           execFastInitTester                                   
    //   Description:    Execute fast init sequence (tester only)
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status execFastInitTester(UInt64 xlChanMaskTester) {
      XLDefine.XL_Status  xlStatus;
      byte[]              data = new byte[] {0x81, 0xE9, 0xF1, 0x81, 0xDC};
      uint                length;
      XLClass.xl_kline_init_tester xlKlineInitTester = new XLClass.xl_kline_init_tester();

      xlKlineInitTester.TiniL = 25000; // 25ms
      xlKlineInitTester.Twup  = 50000; // 50ms
  
      length = (uint)data.Length;

      xlStatus = mDriver.XL_KlineFastInitTester(g_xlPhKline, xlChanMaskTester, length, data, xlKlineInitTester);
      Console.WriteLine("execFastInitTester, XL_KlineFastInitTester result: {0}", mDriver.XL_GetErrorString(xlStatus));

      return xlStatus;
    } // execFastInitTester


    //------------------------------------------------------------------------------
    //   Name:           demoActivateChannel                                   
    //   Description:    Init tester/ECU and activate channel
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status demoActivateChannel(uint func, UInt64 xlChanMask) {

      XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_ERROR;
  
      if (XL_INVALID_PORTHANDLE != g_xlPhKline) {
        if (FUNC_UART == func) {
          xlStatus = prepareUart(xlChanMask);
          Console.WriteLine("demoActivateChannel, prepareUart result: {0}", mDriver.XL_GetErrorString(xlStatus));
          if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
          {
            return xlStatus;
          }
        }

        if (FUNC_TESTER == func) {
          xlStatus = prepareTester(xlChanMask);
          Console.WriteLine("demoActivateChannel, prepareTester result: {0}", mDriver.XL_GetErrorString(xlStatus));
          if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
          {
            return xlStatus;
          }
        }

        if (FUNC_ECUFI == func || FUNC_ECU5BD == func) {
          xlStatus = prepareEcu(xlChanMask, func);
          Console.WriteLine("demoActivateChannel, prepareEcu result: {0}", mDriver.XL_GetErrorString(xlStatus));
          if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
          {
            return xlStatus;
          }
        }

        xlStatus = mDriver.XL_ActivateChannel(g_xlPhKline, xlChanMask, XLDefine.XL_BusTypes.XL_BUS_TYPE_KLINE, XLDefine.XL_AC_Flags.XL_ACTIVATE_RESET_CLOCK);
        Console.WriteLine("demoActivateChannel, xlActivateChannel (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));
        if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
        {
          return xlStatus;
        }
      }
      else {
        xlStatus = XLDefine.XL_Status.XL_ERR_INVALID_HANDLE;
        Console.WriteLine("demoActivateChannel, invalid port handle!");
      }

      return xlStatus;
    }


    //------------------------------------------------------------------------------
    //   Name:           demoInitDriver                                   
    //   Description:    initializes the driver with one port and all founded
    //                   channels which have a connected K-Line cab/piggy.
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status demoInitDriver() {

      XLDefine.XL_Status        xlStatus;
      uint    i;

      // print .NET wrapper version
      Console.WriteLine("demoInitDriver, vxlapi_NET: {0}", typeof(XLDriver).Assembly.GetName().Version);

      // open the driver
      xlStatus = mDriver.XL_OpenDriver();
      Console.WriteLine("demoInitDriver, xlOpenDriver result: {0}", mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }
  
      // get/print the hardware configuration
      xlStatus = mDriver.XL_GetDriverConfig(ref g_xlDrvConfig);
      Console.WriteLine("demoInitDriver, xlGetDriverConfig result: {0}", mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }

      // convert the dll version number into a readable string
      Console.WriteLine("demoInitDriver, DLL Version : " + mDriver.VersionToString(g_xlDrvConfig.dllVersion));

      // select the wanted channels
      g_xlCmKline   = 0;
      g_KlineChCtr = 0;
  
      for (i = 0; i < g_xlDrvConfig.channelCount; i++) {

        // we take all K-Line hardware with K-Line cabs/piggy's
        if ((g_xlDrvConfig.channel[i].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_KLINE) != 0)
        { 
          g_xlCmKline |= g_xlDrvConfig.channel[i].channelMask;
          if (g_KlineChCtr < MAX_CH) {
            g_xlCmSelKline[g_KlineChCtr] = g_xlDrvConfig.channel[i].channelMask;
          }
          g_KlineChCtr++;
        }
      }

      if (g_KlineChCtr > 0) {
        Console.WriteLine("demoInitDriver, {0} K-Line channel(s) found, 0x{1:X}", g_KlineChCtr, g_xlCmKline);
      }
      else {
        xlStatus = XLDefine.XL_Status.XL_ERROR;
        Console.WriteLine("demoInitDriver, No K-Line channels found!");
      }
  
      return xlStatus;
    }


    //------------------------------------------------------------------------------
    //   Name:           demoInitKline                                   
    //   Description:    Open a K-Line port
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status demoInitKline(UInt64 xlChanMask) {

      XLDefine.XL_Status        xlStatus;
      UInt64        xlPermissionMask = 0;
 
      if (g_xlCmKline == 0) {
        Console.WriteLine("demoInitKline, no K-Line channels found!");
        return XLDefine.XL_Status.XL_ERROR;
      }
  
      xlPermissionMask = g_xlCmKline;

      // open ONE port including all channels
      xlStatus = mDriver.XL_OpenPort(ref g_xlPhKline, g_AppName, xlChanMask, ref xlPermissionMask, 256, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION, XLDefine.XL_BusTypes.XL_BUS_TYPE_KLINE);
      Console.WriteLine("demoInitKline, xlOpenPort ({0}, CM:0x{1:X}, PM:0x{2:X}) result: {3}", 
              g_xlPhKline, g_xlCmKline, xlPermissionMask, mDriver.XL_GetErrorString(xlStatus));

      if ((XLDefine.XL_Status.XL_SUCCESS != xlStatus) || (XL_INVALID_PORTHANDLE == g_xlPhKline))
      {
        mDriver.XL_ClosePort(g_xlPhKline);
        g_xlPhKline = XL_INVALID_PORTHANDLE;
        xlStatus = XLDefine.XL_Status.XL_ERROR;
      }
  
      return xlStatus;
  
    } // demoInitKline            


    //------------------------------------------------------------------------------
    //   Name:           prepareUart
    //   Description:    Set UART params, baudrate of K-Line tester
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status prepareUart(UInt64 xlChanMask) {
      XLDefine.XL_Status              xlStatus;
      XLClass.xl_kline_uartPar uartParams = new XLClass.xl_kline_uartPar();

      uartParams.databits = 8;
      uartParams.parity = XLDefine.XL_KLine_UartParity.XL_KLINE_UART_PARITY_NONE;
      uartParams.stopbits = 1;

      if (xlChanMask == 0) {
        Console.WriteLine("prepareUart, invalid channel mask!");
        return XLDefine.XL_Status.XL_ERR_WRONG_PARAMETER;
      }

      xlStatus = mDriver.XL_KlineSetUartParams(g_xlPhKline, xlChanMask, uartParams);
      Console.WriteLine("prepareUart, XL_KlineSetUartParams (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }

      xlStatus = mDriver.XL_KlineSetBaudrate(g_xlPhKline, xlChanMask, g_BaudRate);
      Console.WriteLine("prepareUart, XL_KlineSetBaudrate (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }

      xlStatus = mDriver.XL_KlineSwitchTesterResistor(g_xlPhKline, xlChanMask, XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_ON);
      Console.WriteLine("prepareUart, XL_KlineSwitchTesterResistor (CM:0x{0:X}) result: {1}", xlChanMask, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }
      else
      {
        g_TesterResistor = (uint)XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_ON;
      }

      return xlStatus;

    } // prepareUart


    //------------------------------------------------------------------------------
    //   Name:           prepareTester                                   
    //   Description:    Set baudrate, UART, timing settings,... of K-Line tester
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status prepareTester(UInt64 xlChanMaskTester) {
      XLDefine.XL_Status              xlStatus;
      XLClass.xl_kline_uartPar uartParams = new XLClass.xl_kline_uartPar();
      XLClass.xl_kline_set_communicationTiming_tester comTester = new XLClass.xl_kline_set_communicationTiming_tester();

      uartParams.databits = 8;
      uartParams.parity = XLDefine.XL_KLine_UartParity.XL_KLINE_UART_PARITY_NONE;
      uartParams.stopbits = 1;

      comTester.P1min = 0;
      comTester.P4    = 150;

      if (xlChanMaskTester == 0) {
        Console.WriteLine("prepareTester, invalid channel mask!");
        return XLDefine.XL_Status.XL_ERR_WRONG_PARAMETER;
      }

      xlStatus = mDriver.XL_KlineSetUartParams(g_xlPhKline, xlChanMaskTester, uartParams);
      Console.WriteLine("prepareTester, XL_KlineSetUartParams (CM:0x{0:X}) result: {1}", xlChanMaskTester, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }

      xlStatus = mDriver.XL_KlineSetCommunicationTimingTester(g_xlPhKline, xlChanMaskTester, comTester);
      Console.WriteLine("prepareTester, XL_KlineSetCommunicationTimingTester (CM:0x{0:X}) result: {1}", xlChanMaskTester, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }

      xlStatus = mDriver.XL_KlineSetBaudrate(g_xlPhKline, xlChanMaskTester, g_BaudRate);
      Console.WriteLine("prepareTester, XL_KlineSetBaudrate (CM:0x{0:X}) result: {1}", xlChanMaskTester, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }

      xlStatus = mDriver.XL_KlineSwitchTesterResistor(g_xlPhKline, xlChanMaskTester, XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_ON);
      Console.WriteLine("prepareTester, XL_KlineSwitchTesterResistor (CM:0x{0:X}) result: {1}", xlChanMaskTester, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }
      else
      {
        g_TesterResistor = (uint)XLDefine.XL_KLine_TesterResistor.XL_KLINE_TESTERRESISTOR_ON;
      }

      return xlStatus;

    } // prepareTester


    //------------------------------------------------------------------------------
    //   Name:           prepareEcu                                   
    //   Description:    Set baudrate, UART, timing settings,... of K-Line ECU
    //                   and execute 5Bd init sequence if required
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status prepareEcu(UInt64 xlChanMaskEcu, uint func) {
      XLDefine.XL_Status              xlStatus;
      XLClass.xl_kline_uartPar uartParams = new XLClass.xl_kline_uartPar();
      XLClass.xl_kline_set_communicationTiming_ecu xlKlineSetComEcu = new XLClass.xl_kline_set_communicationTiming_ecu();

      if (xlChanMaskEcu == 0) {
        Console.WriteLine("prepareEcu, invalid channel mask!");
        return XLDefine.XL_Status.XL_ERR_WRONG_PARAMETER;
      }

      switch (func) {
      case FUNC_ECUFI:
      case FUNC_ECU5BD:
        break;
      default:
        Console.WriteLine("prepareEcu, invalid ECU function!");
        return XLDefine.XL_Status.XL_ERR_WRONG_PARAMETER;
      }

      uartParams.databits = 8;
      uartParams.parity = XLDefine.XL_KLine_UartParity.XL_KLINE_UART_PARITY_NONE;
      uartParams.stopbits = 1;

      xlStatus = mDriver.XL_KlineSetUartParams(g_xlPhKline, xlChanMaskEcu, uartParams);
      Console.WriteLine("prepareEcu, XL_KlineSetUartParams (CM:0x{0:X}) result: {1}", xlChanMaskEcu, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }
  
      xlKlineSetComEcu.P1       = 5000;
      xlKlineSetComEcu.P4min    = 100;

      if (FUNC_ECUFI == func) {
        xlKlineSetComEcu.TinilMin = 24000; // 24ms
        xlKlineSetComEcu.TinilMax = 26000; // 26ms
        xlKlineSetComEcu.TwupMin  = 49000; // 49ms
        xlKlineSetComEcu.TwupMax  = 51000; // 51ms
      }
      else {
        xlKlineSetComEcu.TinilMin = 0;
        xlKlineSetComEcu.TinilMax = 0;
        xlKlineSetComEcu.TwupMin  = 0;
        xlKlineSetComEcu.TwupMax  = 0;
      }

      xlStatus = mDriver.XL_KlineSetCommunicationTimingEcu(g_xlPhKline, xlChanMaskEcu, xlKlineSetComEcu);
      Console.WriteLine("prepareEcu, XL_KlineSetCommunicationTimingEcu (CM:0x{0:X}) result: {1}", xlChanMaskEcu, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }

      xlStatus = mDriver.XL_KlineSetBaudrate(g_xlPhKline, xlChanMaskEcu, g_BaudRate);
      Console.WriteLine("prepareEcu, XL_KlineSetBaudrate (CM:0x{0:X}) result: {1}", xlChanMaskEcu, mDriver.XL_GetErrorString(xlStatus));
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        return xlStatus;
      }
  
      if (FUNC_ECU5BD == func) {
        XLClass.xl_kline_init_5BdEcu ecu5BdInitdata = new XLClass.xl_kline_init_5BdEcu();
        ecu5BdInitdata.configure = XLDefine.XL_KLine_Configure5BdEcu.XL_KLINE_CONFIGURE_ECU;
        ecu5BdInitdata.addr         = ECU_ADDR;
        ecu5BdInitdata.rate5bd      = 50;
        ecu5BdInitdata.syncPattern  = 0x55;
        ecu5BdInitdata.W1           = 25000;  // 25ms
        ecu5BdInitdata.W2           = 6000;   //  6ms
        ecu5BdInitdata.W3           = 8000;   //  8ms
        ecu5BdInitdata.W4           = 27000;  // 27ms
        ecu5BdInitdata.W4min        = 12000;  // 12ms
        ecu5BdInitdata.W4max        = 17000;  // 17ms
        ecu5BdInitdata.kb1          = 0xa1;
        ecu5BdInitdata.kb2          = 0xa2;
        ecu5BdInitdata.addrNot      = 0;

        xlStatus = mDriver.XL_KlineInit5BdEcu(g_xlPhKline, xlChanMaskEcu, ecu5BdInitdata);
        Console.WriteLine("prepareEcu, XL_KlineInit5BdEcu (CM:0x{0:X}) result: {1}", xlChanMaskEcu, mDriver.XL_GetErrorString(xlStatus));
        if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
        {
          return xlStatus;
        }
      }

      return xlStatus;
    } // prepareEcu


    //------------------------------------------------------------------------------
    //   Name:           demoCleanUp                                   
    //   Description:    Terminate receive thread, close K-Line port, and close
    //                   the driver
    //------------------------------------------------------------------------------
    private static XLDefine.XL_Status demoCleanUp()
    {
      XLDefine.XL_Status xlStatus;

      // CleanUp K-Line stuff
      g_RXThread.Abort();

      if (XL_INVALID_PORTHANDLE != g_xlPhKline) {
        xlStatus = mDriver.XL_ClosePort(g_xlPhKline);
        Console.WriteLine("demoCleanUp, xlClosePort (PH:{0}) result: {1}", g_xlPhKline, mDriver.XL_GetErrorString(xlStatus));
      }

      g_xlPhKline = XL_INVALID_PORTHANDLE;

      mDriver.XL_CloseDriver();

      return XLDefine.XL_Status.XL_SUCCESS;    // No error handling
    }


    //------------------------------------------------------------------------------
    //   Name:           demoUsage                                   
    //   Description:    Displayed in case of wrong command line arguments
    //------------------------------------------------------------------------------
    static void demoUsage() {
      Console.WriteLine("Usage: xlKlineDemo [<function>] [<baudrate>] [<AppName>]");
      Console.WriteLine("\t<function>:   tester");
      Console.WriteLine("\t<function>:   ecufi");
      Console.WriteLine("\t<function>:   ecu5bd");
    }
    

    // -----------------------------------------------------------------------------------------------
    /// <summary>
    /// MAIN
    /// 
    /// Sends and receives KLine messages. 
    /// This demo requires two connected LIN channels (Vector network interface).
    /// </summary>
    // -----------------------------------------------------------------------------------------------
    [STAThread]
    static void Main(string[] args)
    {
      XLDefine.XL_Status      xlStatus;
      UInt64                  xlChanMaskTester = 0x0001;
      UInt64                  xlChanMaskEcu = 0x0002;
      
      Console.WriteLine("------------------------------------------------------------------------");
      Console.WriteLine("-      xlKlineDemoNET C# - Test Application for K-Line Driver API      -");
      Console.WriteLine("- Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.  -");
      Console.WriteLine("------------------------------------------------------------------------");

      // Parse command line params
      int nbrArgs = args.Length;
      // First argument: function
      if (nbrArgs > 0)
      {
        if (args[0].Equals("-?"))
        {
          demoUsage();
          Environment.Exit(0);
        }
        if (args[0].Equals("tester"))
        {
          g_Function = FUNC_TESTER;
        }
        else if (args[0].Equals("ecufi"))
        {
          g_Function = FUNC_ECUFI;
        }
        else if (args[0].Equals("ecu5bd"))
        {
          g_Function = FUNC_ECU5BD;
        }
        else
        {
          demoUsage();
          Environment.Exit(1);
        }
      }
      // Second argument: baudrate
      if (nbrArgs > 1)
      {
        try
        {
          g_BaudRate = UInt32.Parse(args[1]);
          Console.WriteLine("Baudrate = {0}", g_BaudRate);
        }
        catch (FormatException)
        {
          demoUsage();
          Environment.Exit(1);
        }
        catch (OverflowException)
        {
          demoUsage();
          Environment.Exit(1);
        }
      }
      // Third argument: application name
      if (nbrArgs > 2)
      {
        g_AppName = args[2];
        Console.WriteLine("AppName = {0}", g_AppName);
      }

      // Initialize the driver structures for the application
      xlStatus = demoInitDriver();
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        Environment.Exit((int)xlStatus);
      }

      // Display hardware config
      demoPrintConfig();

      // Select tester channel
      xlChanMaskTester = g_xlCmSelKline[0];
      // Select ECU channel
      if (g_KlineChCtr > 1)
      {
        xlChanMaskEcu = g_xlCmSelKline[1];
      }
      else
      {
        xlChanMaskEcu = g_xlCmSelKline[0];
      }

      // Use only one channel
      g_xlCmKline = xlChanMaskTester;
      if (FUNC_ECU5BD == g_Function || FUNC_ECUFI == g_Function)
      {
        g_xlCmKline = xlChanMaskEcu;
      }
      Console.WriteLine();
      Console.WriteLine("Selected K-Line channel CM:0x{0:X}.", g_xlCmKline);
      Console.WriteLine();

      // Open K-Line port
      xlStatus = demoInitKline(g_xlCmKline);
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        Environment.Exit((int)xlStatus);
      }

      // create the RX thread to read the messages
      xlStatus = demoCreateRxThread();
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        Environment.Exit((int)xlStatus);
      }

      // Go with selected channel on bus
      xlStatus = demoActivateChannel(g_Function, g_xlCmKline);
      if (XLDefine.XL_Status.XL_SUCCESS != xlStatus)
      {
        Environment.Exit((int)xlStatus);
      }
      g_activated = true;

      Console.WriteLine();
      Console.WriteLine(": Press <h> for help");
      
      while (!g_stop)
      {
        // Read user input:
        ConsoleKeyInfo info = Console.ReadKey(true);
        Console.WriteLine();
        switch (info.KeyChar)
        {
          case 'v':
          demoToggleVerbose();
          break;

          case 't': // transmit a message
          {
            byte[] buf = Encoding.ASCII.GetBytes("KLine roxx");
            uint len = (uint)buf.Length;

            demoTransmit(g_xlCmKline, buf, len);
          }
          break;

          case 'T': // transmit a long message
          {
            byte[] buf = new byte[300];
            uint len;

            len = (uint)buf.Length;
            for (uint i = 0; i < len; ++i) {
              buf[i] = (byte)((len - i) % 255);
            }

            demoTransmit(g_xlCmKline, buf, len);
          }
          break;

          case 'i':
            demoToggleTesterResistor(g_xlCmKline);
            break;

          case 'a':
            demoToggleTimerRate();
            break;

          case 'r':
            demoResetClock();
            break;

          case 'G':
            demoGenerateSyncPulse(g_xlCmKline);
            break;

          case 'f':
            execFastInitTester(xlChanMaskTester);
            break;

          case '5':
            exec5BdInitTester(xlChanMaskTester);
            break;

          case 'H':
            demoToggleHighspeedMode(g_xlCmKline);
            break;

          case 's':
            demoStartStop();
            break;

          case 'p':
            demoPrintConfig();
            break;

          case 'h':
            demoHelp();
            break;

          case '\u001B': // ESC: end application
            g_stop = true;
            break;

          default:
            //Console.WriteLine("key: {0:x}", c);
            break;
        }
      }

      // Kill Rx thread:
      g_RXThread.Abort();
        
     
    }


    // -----------------------------------------------------------------------------------------------
    /// <summary>
    /// EVENT THREAD (RX)
    /// 
    /// RX thread waits for Vector interface events and displays filtered KLINE messages.
    /// </summary>
    // ----------------------------------------------------------------------------------------------- 
    public static void RXThread()
    {
      // Create new object containing received data 
      XLClass.xl_event xlEvent = new XLClass.xl_event();

      // Result of XL Driver function calls
      XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_SUCCESS;

      // Result values of WaitForSingleObject 
      XLDefine.WaitResults waitResult = new XLDefine.WaitResults();

      mDriver.XL_FlushReceiveQueue(g_xlPhKline);

      // Note: this thread will be destroyed by MAIN
      while (true)
      {

        // Wait for hardware events
        waitResult = (XLDefine.WaitResults)WaitForSingleObject(g_hKlineMsgEvent, 1000);

        // If event occured...
        if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
        {

          // ...init xlStatus first
          xlStatus = XLDefine.XL_Status.XL_SUCCESS;

          // afterwards: while hardware queue is not empty...
          while (xlStatus != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
          {
            // ...receive data from hardware.
            xlStatus = mDriver.XL_Receive(g_xlPhKline, ref xlEvent);

            if (XLDefine.XL_Status.XL_SUCCESS == xlStatus)
            {

              if (XLDefine.XL_EventTags.XL_SYNC_PULSE == xlEvent.tag)
              {
                Console.WriteLine("{0}", mDriver.XL_GetEventString(xlEvent));
              }
              else if (XLDefine.XL_EventTags.XL_TRANSCEIVER == xlEvent.tag)
              {
                Console.WriteLine("{0}, ch:{1} is_present:{2}", mDriver.XL_GetEventString(xlEvent), xlEvent.chanIndex, xlEvent.tagData.transceiver.is_present);
              }
              else if (XLDefine.XL_EventTags.XL_KLINE_MSG == xlEvent.tag)
              {

                switch (xlEvent.tagData.klineData.klineEvtTag)
                {
                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_RX_DATA:
                    Console.Write("KLINE_RX_DATA, ch:{0} d:0x{1:X} t:{2}", xlEvent.chanIndex, xlEvent.tagData.klineData.data.klineRx.data, xlEvent.timeStamp);
                    if (xlEvent.tagData.klineData.data.klineRx.error != 0)
                    {
                      Console.Write(" err:{0}", xlEvent.tagData.klineData.data.klineRx.error);
                    }
                    Console.WriteLine();
                    break;

                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_TX_DATA:
                    Console.Write("KLINE_TX_DATA, ch:{0} d:0x{1:X} t:{2}", xlEvent.chanIndex, xlEvent.tagData.klineData.data.klineTx.data, xlEvent.timeStamp);
                    if (xlEvent.tagData.klineData.data.klineTx.error != 0)
                    {
                      Console.Write(" err:{0}", xlEvent.tagData.klineData.data.klineTx.error);
                    }
                    Console.WriteLine();
                    break;

                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_TESTER_5BD:
                    Console.Write("KLINE_TESTER_5BD - ");
                    switch (xlEvent.tagData.klineData.data.klineTester5Bd.tag5bd)
                    {
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_ADDR:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_ADDR - 0x{0:X}", xlEvent.tagData.klineData.data.klineTester5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_BAUDRATE:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_BAUDRATE - {0}", xlEvent.tagData.klineData.data.klineTester5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_KB1:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_KB1 0x{0:X}", xlEvent.tagData.klineData.data.klineTester5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_KB2:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_KB2 0x{0:X}", xlEvent.tagData.klineData.data.klineTester5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_KB2NOT:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_KB2NOT 0x{0:X}", xlEvent.tagData.klineData.data.klineTester5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_ADDRNOT:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_ADDRNOT 0x{0:X}", xlEvent.tagData.klineData.data.klineTester5Bd.data);
                        break;
                      default:
                        Console.WriteLine("unknown tag5bd:{0}", xlEvent.tagData.klineData.data.klineTester5Bd.tag5bd);
                        break;
                    }
                    break;

                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_ECU_5BD:
                    Console.Write("KLINE_ECU_5BD - ");
                    switch (xlEvent.tagData.klineData.data.klineEcu5Bd.tag5bd)
                    {
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_ADDR:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_ADDR - 0x{0:X}", xlEvent.tagData.klineData.data.klineEcu5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_BAUDRATE:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_BAUDRATE - {0}", xlEvent.tagData.klineData.data.klineEcu5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_KB1:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_KB1 0x{0:X}", xlEvent.tagData.klineData.data.klineEcu5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_KB2:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_KB2 0x{0:X}", xlEvent.tagData.klineData.data.klineEcu5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_KB2NOT:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_KB2NOT 0x{0:X}", xlEvent.tagData.klineData.data.klineEcu5Bd.data);
                        break;
                      case XLDefine.XL_KLine_Tag5bd.XL_KLINE_EVT_TAG_5BD_ADDRNOT:
                        Console.WriteLine("XL_KLINE_EVT_TAG_5BD_ADDRNOT 0x{0:X}", xlEvent.tagData.klineData.data.klineEcu5Bd.data);
                        break;
                      default:
                        Console.WriteLine("unknown tag5bd:{0}", xlEvent.tagData.klineData.data.klineEcu5Bd.tag5bd);
                        break;
                    }
                    break;

                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_TESTER_FI_WU_PATTERN:
                    Console.WriteLine("KLINE_TESTER_FI_WU_PATTERN");
                    break;

                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_ECU_FI_WU_PATTERN:
                    Console.WriteLine("KLINE_ECU_FI_WU_PATTERN");
                    break;

                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_ERROR:
                    Console.Write("KLINE_ERROR - {0}",
                      XLDefine.XL_KLine_ErrorTag.XL_KLINE_ERROR_TYPE_RXTX_ERROR == xlEvent.tagData.klineData.data.klineError.klineErrorTag ? "RXTX_ERROR" :
                      XLDefine.XL_KLine_ErrorTag.XL_KLINE_ERROR_TYPE_5BD_TESTER == xlEvent.tagData.klineData.data.klineError.klineErrorTag ? "ERROR_5BD_TESTER" :
                      XLDefine.XL_KLine_ErrorTag.XL_KLINE_ERROR_TYPE_5BD_ECU == xlEvent.tagData.klineData.data.klineError.klineErrorTag ? "ERROR_5BD_ECU" :
                      XLDefine.XL_KLine_ErrorTag.XL_KLINE_ERROR_TYPE_IBS == xlEvent.tagData.klineData.data.klineError.klineErrorTag ? "XL_KLINE_IBS" :
                      XLDefine.XL_KLine_ErrorTag.XL_KLINE_ERROR_TYPE_FI == xlEvent.tagData.klineData.data.klineError.klineErrorTag ? "FI_ERROR" : "unknown tag");
                    if (XLDefine.XL_KLine_ErrorTag.XL_KLINE_ERROR_TYPE_IBS == xlEvent.tagData.klineData.data.klineError.klineErrorTag)
                    {
                      Console.Write(" - {0}",
                        XLDefine.XL_KLine_Error_IBS.XL_KLINE_ERR_IBS_P1 == xlEvent.tagData.klineData.data.klineError.data.ibsErr.ibsErr ? "IBS_P1" :
                        XLDefine.XL_KLine_Error_IBS.XL_KLINE_ERR_IBS_P4 == xlEvent.tagData.klineData.data.klineError.data.ibsErr.ibsErr ? "IBS_P4" : "unknown ibs");
                      if ((XLDefine.XL_KLine_Error_RXTX.XL_KLINE_ERR_RXTX_UA & xlEvent.tagData.klineData.data.klineError.data.ibsErr.rxtxErrData) != 0)
                      {
                        Console.Write(" - Unexpected Activity");
                      }
                      if ((XLDefine.XL_KLine_Error_RXTX.XL_KLINE_ERR_RXTX_MA & xlEvent.tagData.klineData.data.klineError.data.ibsErr.rxtxErrData) != 0)
                      {
                        Console.Write(" - Missing Activity");
                      }
                      if ((XLDefine.XL_KLine_Error_RXTX.XL_KLINE_ERR_RXTX_ISB & xlEvent.tagData.klineData.data.klineError.data.ibsErr.rxtxErrData) != 0)
                      {
                        Console.Write(" - Invalid Sync Byte");
                      }
                    }
                    Console.WriteLine();
                    break;

                  case XLDefine.XL_KLine_EventTags.XL_KLINE_EVT_CONFIRMATION:
                    Console.WriteLine("XL_KLINE_EVT_CONFIRMATION ch:{0} tag:{1} result:{2}",
                      xlEvent.tagData.klineData.data.klineConfirmation.channel,
                      xlEvent.tagData.klineData.data.klineConfirmation.confTag,
                      xlEvent.tagData.klineData.data.klineConfirmation.result);
                    break;

                  default:
                    Console.WriteLine("Unknown K-Line event!");
                    break;
                }

              }
              else
              {
                if (!g_silent)
                {
                  Console.WriteLine("ph:{0}, {1}", g_xlPhKline, mDriver.XL_GetEventString(xlEvent));
                }
              }

            }

          }
        }
        // No event occurred
      }
    }
    // -----------------------------------------------------------------------------------------------

  }
}
