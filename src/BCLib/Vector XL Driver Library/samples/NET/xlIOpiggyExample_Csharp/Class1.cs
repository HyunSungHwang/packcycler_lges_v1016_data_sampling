/*-------------------------------------------------------------------------------------------
| File        : Class1.cs
| Project     : Vector IOpiggy .NET Example
|
| Description : This example demonstrates the basic DAIO functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/

using System;
using System.Runtime.InteropServices;
using System.Threading;

using vxlapi_NET;


namespace xlIOpiggyExample
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    class Class1
    {
        // -----------------------------------------------------------------------------------------------
        // DLL Import for RX events
        // -----------------------------------------------------------------------------------------------
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int WaitForSingleObject(int handle, int timeOut);
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        // Global variables
        // ----------------------------------------------------------------------------------------------- 
        // Driver access through XLDriver (wrapper)
        private static XLDriver IOpiggyDemo = new XLDriver();
        private static String appName = "xlIOpiggyNET";

        // Driver configuration
        private static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();

        // Variables required by XLDriver
        private static XLDefine.XL_HardwareType hwType  = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        private static uint hwIndex                     = 0;
        private static uint hwChannel                   = 0;        
        private static int portHandle                   = -1;
        private static int eventHandle                  = -1;
        private static UInt64 accessMask                = 0;
        private static UInt64 permissionMask            = 0;
        private static int channelIndex                 = 0;

        // RX thread
        private static Thread rxThread;

        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// MAIN
        /// 
        /// Simple access to one IOpiggy.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        [STAThread]
        static void Main(string[] args)
        {
            XLDefine.XL_Status status;

            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("                   xlIOpiggyExample.NET C# V11.0                   ");
            Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.");
            Console.WriteLine("-------------------------------------------------------------------\n");

            // print .NET wrapper version
            Console.WriteLine("vxlapi_NET        : " + typeof(XLDriver).Assembly.GetName().Version);

            // Open XL Driver
            status = IOpiggyDemo.XL_OpenDriver();
            Console.WriteLine("Open Driver       : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Get the complete XL Driver configuration, stored in driverConfig object
            status = IOpiggyDemo.XL_GetDriverConfig(ref driverConfig);
            Console.WriteLine("Get Driver Config : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Convert the dll version into a readable string
            Console.WriteLine("DLL Version       : " + IOpiggyDemo.VersionToString(driverConfig.dllVersion));


            // Display channel count
            Console.WriteLine("Channels found    : " + driverConfig.channelCount);


            // Display all found channels
            for (int i = 0; i < driverConfig.channelCount; i++)
            {
                Console.WriteLine("\n                   [{0}] " + driverConfig.channel[i].name, i);
                Console.WriteLine("                    - Channel Mask    : " + driverConfig.channel[i].channelMask);
                Console.WriteLine("                    - Transceiver Name: " + driverConfig.channel[i].transceiverName);
                Console.WriteLine("                    - Serial Number   : " + driverConfig.channel[i].serialNumber);
            }

            // If the application name cannot be found in VCANCONF....
            if ((IOpiggyDemo.XL_GetApplConfig(appName, 0, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO) != XLDefine.XL_Status.XL_SUCCESS)) 
            {
                //...create the item with one channel
                IOpiggyDemo.XL_SetApplConfig(appName, 0, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO);
                PrintAssignErrorAndPopupHwConf();
            }

            // request the user to assign a usable DAIO channel
            while (!GetAppChannelAndTestIsOk(0, ref accessMask, ref channelIndex))
            {
                PrintAssignErrorAndPopupHwConf();
            }

            PrintConfig();
            permissionMask = accessMask;

            // Open port
            status = IOpiggyDemo.XL_OpenPort(ref portHandle, appName, accessMask, ref permissionMask, 1024, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO);
            Console.WriteLine("\n\nOpen Port                   : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Get RX event handle
            status = IOpiggyDemo.XL_SetNotification(portHandle, ref eventHandle, 1);
            Console.WriteLine("Set Notification            : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Config analog ports
            XLClass.xl_daio_set_port setPort = new XLClass.xl_daio_set_port();

            setPort.portType = XLDefine.XL_DAIO_PortTypeMask.XL_DAIO_PORT_TYPE_MASK_ANALOG;
            setPort.portMask = XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D0_ANALOG_A0 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D1_ANALOG_A1 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D2_ANALOG_A2 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D3_ANALOG_A3;

            setPort.portFunction[0] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_ANALOG_OUT;
            setPort.portFunction[1] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_ANALOG_IN;
            setPort.portFunction[2] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_ANALOG_IN;
            setPort.portFunction[3] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_ANALOG_IN;

            status = IOpiggyDemo.XL_IoConfigurePorts(portHandle, accessMask, setPort);
            Console.WriteLine("Config analog ports         : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            setPort.portType = XLDefine.XL_DAIO_PortTypeMask.XL_DAIO_PORT_TYPE_MASK_DIGITAL;
            setPort.portMask = XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D0_ANALOG_A0 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D1_ANALOG_A1 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D2_ANALOG_A2 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D3_ANALOG_A3 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D4 |
                                XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D5;

            setPort.portFunction[0] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_DIGITAL_PUSHPULL;
            setPort.portFunction[1] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_DIGITAL_IN;
            setPort.portFunction[2] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_DIGITAL_IN;
            setPort.portFunction[3] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_DIGITAL_IN;
            setPort.portFunction[4] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_DIGITAL_SWITCH;
            setPort.portFunction[5] = XLDefine.XL_DAIO_PortFunction.XL_DAIO_PORT_DIGITAL_SWITCH;

            status = IOpiggyDemo.XL_IoConfigurePorts(portHandle, accessMask, setPort);
            Console.WriteLine("Config digital ports        : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            status = IOpiggyDemo.XL_IoSetDigOutLevel(portHandle, accessMask, XLDefine.XL_DAIO_OutputLevel.XL_DAIO_DO_LEVEL_5V);
            Console.WriteLine("Config digital out level    : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            status = IOpiggyDemo.XL_IoSetDigInThreshold(portHandle, accessMask, 2500);
            Console.WriteLine("Config digital in threshold : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Set trigger mode
            XLClass.xl_daio_trigger_mode triggerMode = new XLClass.xl_daio_trigger_mode();

            triggerMode.portTypeMask    = XLDefine.XL_DAIO_PortTypeMask.XL_DAIO_PORT_TYPE_MASK_DIGITAL|
                                            XLDefine.XL_DAIO_PortTypeMask.XL_DAIO_PORT_TYPE_MASK_ANALOG;
            triggerMode.triggerType     = XLDefine.XL_DAIO_PortTriggerType.XL_DAIO_TRIGGER_TYPE_CYCLIC;
            triggerMode.cycleTime       = 1000000;
  
            status = IOpiggyDemo.XL_IoSetTriggerMode(portHandle, accessMask, triggerMode);  
            Console.WriteLine("Set Trigger Mode            : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Activate channel
            status = IOpiggyDemo.XL_ActivateChannel(portHandle, accessMask, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO, XLDefine.XL_AC_Flags.XL_ACTIVATE_NONE);
            Console.WriteLine("Activate Channel            : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Start sampling
            status = IOpiggyDemo.XL_IoStartSampling(portHandle, accessMask, XLDefine.XL_DAIO_PortTypeMask.XL_DAIO_PORT_TYPE_MASK_ANALOG);
            Console.WriteLine("Start sampling              : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Run Rx Thread
            Console.WriteLine("Start Rx Thread...");
            rxThread = new Thread(new ThreadStart(RXThread));
            rxThread.Start();

            XLClass.xl_daio_analog_params analogParams = new XLClass.xl_daio_analog_params();
            analogParams.portMask = XLDefine.XL_IO_PortMaskAnalog.XL_DAIO_PORT_MASK_ANALOG_A0;
            analogParams.value[0] = 0;

            XLClass.xl_daio_digital_params digitalParams = new XLClass.xl_daio_digital_params();
            uint switchMask = (uint)(XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D4 |
                                        XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D5);
            // we keep D0 permanently on. On toggle, we also turn on the switch (D4|D5)
            digitalParams.portMask = (uint)XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D0_ANALOG_A0 | switchMask;
            digitalParams.valueMask = (uint)XLDefine.XL_IO_PortMask.XL_DAIO_PORT_MASK_DIGITAL_D0_ANALOG_A0;

            // Toggle output


            while (Console.ReadLine() != "x")
            {
                if (analogParams.value[0] == 0) 
                {
                    analogParams.value[0] = 0x0FFF;
                    digitalParams.valueMask |= switchMask;
                }
                else
                {
                    analogParams.value[0] = 0;
                    digitalParams.valueMask &= ~switchMask;
                }
                status = IOpiggyDemo.XL_IoSetAnalogOutput(portHandle, accessMask, analogParams);
                Console.WriteLine("Set Analog Out: " + status);
                status = IOpiggyDemo.XL_IoSetDigitalOutput(portHandle, accessMask, digitalParams);
                Console.WriteLine("Set Digital Out: " + status);
            }

            // Kill Rx thread
            rxThread.Abort();
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message/exit in case of a functional call does not return XL_SUCCESS
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static int PrintFunctionError()
        {
            Console.WriteLine("\nERROR: Function call failed!\nPress any key to continue...");
            Console.ReadKey();
            return -1;
        }
        // -----------------------------------------------------------------------------------------------






        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Displays Vector Hardware Configuration.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintConfig()
        {
            Console.WriteLine("\n\nAPPLICATION CONFIGURATION");
            Console.WriteLine("-------------------------------------------------------------------\n");
            Console.WriteLine("Configured Hardware Channel : " + driverConfig.channel[channelIndex].name);
            Console.WriteLine("Hardware Driver Version     : " + IOpiggyDemo.VersionToString(driverConfig.channel[channelIndex].driverVersion));
            Console.WriteLine("Used Transceiver            : " + driverConfig.channel[channelIndex].transceiverName);
            Console.WriteLine("-------------------------------------------------------------------\n");
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message if channel assignment is not valid.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintAssignErrorAndPopupHwConf()
        {
            Console.WriteLine("\nPlease check application settings of \"" + appName + " DAIO 1\",\nassign it to an available hardware channel and press enter.");
            IOpiggyDemo.XL_PopupHwConfig();
            Console.ReadKey();
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve the application channel assignment and test if this channel can be opened
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static bool GetAppChannelAndTestIsOk(uint appChIdx, ref UInt64 chMask, ref int chIdx)
        {
            XLDefine.XL_Status status = IOpiggyDemo.XL_GetApplConfig(appName, appChIdx, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_DAIO);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                Console.WriteLine("XL_GetApplConfig      : " + status);
                PrintFunctionError();
            }

            chMask = IOpiggyDemo.XL_GetChannelMask(hwType, (int)hwIndex, (int)hwChannel);
            chIdx = IOpiggyDemo.XL_GetChannelIndex(hwType, (int)hwIndex, (int)hwChannel);
            if (chIdx < 0 || chIdx >= driverConfig.channelCount)
            {
                // the (hwType, hwIndex, hwChannel) triplet stored in the application configuration does not refer to any available channel.
                return false;
            }

            // test if DAIO is available on this channel
            return (driverConfig.channel[chIdx].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_DAIO) != 0;
        }
        // -----------------------------------------------------------------------------------------------



        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// EVENT THREAD (RX)
        /// RX thread waits for Vector interface events and displays filtered DAIO messages.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void RXThread()
        {
            // Create new object containing received data 
            XLClass.xl_event receivedEvent = new XLClass.xl_event();  
            
            // Result of XL Driver function calls
            XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_SUCCESS;

            // Result values of WaitForSingleObject 
            XLDefine.WaitResults waitResult = new XLDefine.WaitResults(); 
            
           

            // Note: this thread will be destroyed by MAIN
            while (true)
            {
                // Wait for hardware events
                waitResult = (XLDefine.WaitResults)WaitForSingleObject(eventHandle, 1000);

                // If event occurred...
                if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
                {
                    // ...init xlStatus first
                    xlStatus = XLDefine.XL_Status.XL_SUCCESS;

                    // Afterwards: while hw queue is not empty...
                    while (xlStatus != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
                    {
                        // ...receive data from hardware.
                        xlStatus = IOpiggyDemo.XL_Receive(portHandle, ref receivedEvent);

                        //  If receiving succeed....
                        if (xlStatus == XLDefine.XL_Status.XL_SUCCESS)
                        {
                            // ...and data was Rx msg...
                            if (receivedEvent.tag == XLDefine.XL_EventTags.XL_RECEIVE_DAIO_PIGGY)
                            {
                                if (receivedEvent.tagData.daioPiggyData.daioEvtTag == XLDefine.XL_DAIO_EventIDs.XL_DAIO_EVT_ID_ANALOG)
                                {
                                    Console.WriteLine("\n - AIO0: {0} mV", receivedEvent.tagData.daioPiggyData.analog.measuredAnalogData0);
                                    Console.WriteLine(" - AIO1: {0} mV", receivedEvent.tagData.daioPiggyData.analog.measuredAnalogData1);
                                    Console.WriteLine(" - AIO2: {0} mV", receivedEvent.tagData.daioPiggyData.analog.measuredAnalogData2);
                                    Console.WriteLine(" - AIO3: {0} mV", receivedEvent.tagData.daioPiggyData.analog.measuredAnalogData3);
                                }
                                if (receivedEvent.tagData.daioPiggyData.daioEvtTag == XLDefine.XL_DAIO_EventIDs.XL_DAIO_EVT_ID_DIGITAL)
                                {
                                    uint di = receivedEvent.tagData.daioPiggyData.digital.digitalInputData;
                                    Console.WriteLine(" - DIO: {0} {1} {2} {3} {4} {5} {6} {7}",
                                        di >> 7, (di >> 6) & 1, (di >> 5) & 1, (di >> 4) & 1, (di >> 3) & 1, (di >> 2) & 1, (di >> 1) & 1, di & 1);
                                }
                            }
                        }
                    }
                }
                // No event occured
            }
        }
        // -----------------------------------------------------------------------------------------------
    }
}
