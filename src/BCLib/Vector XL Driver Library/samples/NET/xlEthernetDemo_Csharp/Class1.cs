/*-------------------------------------------------------------------------------------------
| File        : Class1.cs
| Project     : Vector Ethernet .NET Example
|
| Description : This example demonstrates the basic Ethernet functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/

using System;
using System.Runtime.InteropServices;
using System.Threading;
using vxlapi_NET;


namespace xlEthernetDemo
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    class Class1
    {
        // -----------------------------------------------------------------------------------------------
        // DLL Import for RX events
        // -----------------------------------------------------------------------------------------------
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int WaitForSingleObject(int handle, int timeOut);
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        // Global variables
        // -----------------------------------------------------------------------------------------------
        // Driver access through XLDriver (wrapper)
        private static XLDriver EthernetDemo                = new XLDriver();
        private static String appName                       = "xlEthernetDemoNET";

        // Driver configuration
        private static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();

        // Variables required by XLDriver
        private static XLDefine.XL_HardwareType hwType      = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        private static uint hwIndex                         = 0;
        private static uint hwChannel                       = 0;   
        private static int portHandle                       = -1;
        private static int eventHandle                      = -1;
        private static UInt64 accessMask                    = 0;
        private static UInt64 accessMaskChannel1            = 0;
        private static UInt64 accessMaskChannel2            = 0;

        private static UInt64 permissionMask                = 0;
        private static int channelIndex1                    = 0;
        private static int channelIndex2                    = 0;

        // RX thread
        private static Thread rxThread;
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// MAIN
        /// 
        /// Sends and receives Ethernet messages using main methods of the "XLDriver" class.
        /// This demo requires two connected Ethernet channels (Vector network interface). 
        /// The configuration is read from Vector Hardware Config (vcanconf.exe).
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        [STAThread]
        static int Main(string[] args)
        {
            XLDefine.XL_Status status;

            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("                    xlEthernetDemoNET C# V11.0                     ");
            Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.");
            Console.WriteLine("-------------------------------------------------------------------\n");

            Console.WriteLine("\r\nPress any Key to continue...\r\n");
            Console.ReadKey();

            // Print .NET wrapper version
            Console.WriteLine("vxlapi_NET        : " + typeof(XLDriver).Assembly.GetName().Version);

            // Open XL Driver
            status = EthernetDemo.XL_OpenDriver();
            Console.WriteLine("Open Driver       : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Get XL Driver configuration
            status = EthernetDemo.XL_GetDriverConfig(ref driverConfig);
            Console.WriteLine("Get Driver Config : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Convert the dll version number into a readable string
            Console.WriteLine("DLL Version       : " + EthernetDemo.VersionToString(driverConfig.dllVersion));


            // Display channel count
            Console.WriteLine("Channels found    : " + driverConfig.channelCount);


            // Display all found channels
            for (int i = 0; i < driverConfig.channelCount; i++)
            {
                Console.WriteLine("\n                   [{0}] " + driverConfig.channel[i].name, i);
                Console.WriteLine("                    - Channel Mask    : " + driverConfig.channel[i].channelMask);
                Console.WriteLine("                    - Transceiver Name: " + driverConfig.channel[i].transceiverName);
                Console.WriteLine("                    - Serial Number   : " + driverConfig.channel[i].serialNumber);
            }

            // If the application name cannot be found in VCANCONF...
            if ((EthernetDemo.XL_GetApplConfig(appName, 0, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_ETHERNET) != XLDefine.XL_Status.XL_SUCCESS) ||
                (EthernetDemo.XL_GetApplConfig(appName, 1, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_ETHERNET) != XLDefine.XL_Status.XL_SUCCESS))
            {
                //...create the item with two Ethernet channels
                EthernetDemo.XL_SetApplConfig(appName, 0, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_ETHERNET);
                EthernetDemo.XL_SetApplConfig(appName, 1, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_ETHERNET);
                PrintAssignErrorAndPopupHwConf();
            }

            // Request the user to assign channels until both Ethernet 1 and 2 are assigned to usable channels
            while (!GetAppChannelAndTestIsOk(0, ref accessMaskChannel1, ref channelIndex1) || !GetAppChannelAndTestIsOk(1, ref accessMaskChannel2, ref channelIndex2))
            {
                PrintAssignErrorAndPopupHwConf();
            }

            PrintConfig();
            accessMask = accessMaskChannel1 | accessMaskChannel2;
            permissionMask = accessMask;

            // Open port
            status = EthernetDemo.XL_OpenPort(ref portHandle, appName, accessMask, ref permissionMask, 8 * 1024 * 1024, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION_V4, XLDefine.XL_BusTypes.XL_BUS_TYPE_ETHERNET);
            Console.WriteLine("\n\nOpen Port             : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Just use the default configuration, which can be changed with Vector Hardware Config.
            // If you want to configure the channels from within this application, uncomment the following line
            // and set the values in EthernetExplicitlyConfigureDevice() appropriately.
            //EthernetExplicitlyConfigureDevice();

            // Get Ethernet config channel 1
            XLClass.T_XL_ETH_CONFIG ethernetConfig1 = new XLClass.T_XL_ETH_CONFIG();
            status = EthernetDemo.XL_EthGetConfig(portHandle, accessMaskChannel1, 0, ref ethernetConfig1);
            Console.WriteLine("Get Ethernet conf. CH1: " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();
            PrintEthernetConfig(ethernetConfig1);

            // Get Ethernet config channel 2
            XLClass.T_XL_ETH_CONFIG ethernetConfig2 = new XLClass.T_XL_ETH_CONFIG();
            status = EthernetDemo.XL_EthGetConfig(portHandle, accessMaskChannel2, 0, ref ethernetConfig2);
            Console.WriteLine("Get Ethernet conf. CH2: " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();
            PrintEthernetConfig(ethernetConfig2);
  


            // Activate channel
            status = EthernetDemo.XL_ActivateChannel(portHandle, accessMask, XLDefine.XL_BusTypes.XL_BUS_TYPE_ETHERNET, XLDefine.XL_AC_Flags.XL_ACTIVATE_NONE);
            Console.WriteLine("Activate Channel      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Get RX event handle
            status = EthernetDemo.XL_SetNotification(portHandle, ref eventHandle, 1);
            Console.WriteLine("Set Notification      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Reset time stamp clock
            status = EthernetDemo.XL_ResetClock(portHandle);
            Console.WriteLine("Reset Clock           : " + status + "\n\n");
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Run Rx Thread
            Console.WriteLine("Start Rx thread...");
            rxThread = new Thread(new ThreadStart(RXThread));
            rxThread.Start();

            // User information
            Console.WriteLine("Press <ENTER> to transmit Ethernet packets \n<t>, <ENTER> to twinkle LED Status \n<x>, <ENTER> to exit");

            // Main loop
            while (true)
            {
                // Read user input
                string str = Console.ReadLine();
                    
                if (str == "x") break;
                else if (str == "t")
                {
                    EthernetDemo.XL_EthTwinkleStatusLed(portHandle, accessMask, 0);
                }
                else
                {
                    // Transmit Ethernet data
                    EthernetTransmitDemo();
                }
            }

            // Kill Rx thread
            rxThread.Abort();
            Console.WriteLine("Close Port                     : " + EthernetDemo.XL_ClosePort(portHandle));
            Console.WriteLine("Close Driver                   : " + EthernetDemo.XL_CloseDriver());

            return 0;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message/exit in case of a functional call does not return XL_SUCCESS
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static int PrintFunctionError()
        {
            Console.WriteLine("\nERROR: Function call failed!\nPress any key to continue...");
            Console.ReadKey();
            return -1;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Displays the Vector Hardware Configuration.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintConfig()
        {
            Console.WriteLine("\n\nAPPLICATION CONFIGURATION");

            foreach (int channelIndex in new int[] { channelIndex1, channelIndex2 })
            {
                XLClass.xl_ethernet_bus_params dataEth = driverConfig.channel[channelIndex].busParams.dataEth;
                Console.WriteLine("-------------------------------------------------------------------");
                Console.WriteLine("Configured Hardware Channel : " + driverConfig.channel[channelIndex].name);
                Console.WriteLine("Hardware Driver Version     : " + EthernetDemo.VersionToString(driverConfig.channel[channelIndex].driverVersion));
                Console.WriteLine("Used Transceiver            : " + driverConfig.channel[channelIndex].transceiverName);
                Console.WriteLine("MAC address                 : {0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}",
                      dataEth.macAddr[0], dataEth.macAddr[1], dataEth.macAddr[2], dataEth.macAddr[3], dataEth.macAddr[4], dataEth.macAddr[5]);
                Console.WriteLine("Connector                   : " + dataEth.connector);
                Console.WriteLine("Phy                         : " + dataEth.phy);
                Console.WriteLine("Link                        : " + dataEth.link);
                Console.WriteLine("Speed                       : " + dataEth.speed);
                Console.WriteLine("Clock Mode                  : " + dataEth.clockMode);
                Console.WriteLine("Bypass                      : " + dataEth.bypass);
            }

            Console.WriteLine("-------------------------------------------------------------------\n");
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Displays the Ethernet Configuration.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintEthernetConfig(XLClass.T_XL_ETH_CONFIG ethernetConfig)
        {
            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("    Speed     : " + ethernetConfig.speed);
            Console.WriteLine("    Duplex    : " + ethernetConfig.duplex);
            Console.WriteLine("    Connector : " + ethernetConfig.connector);
            Console.WriteLine("    Phy       : " + ethernetConfig.phy);
            Console.WriteLine("    Clock Mode: " + ethernetConfig.clockMode);
            Console.WriteLine("    MDI Mode  : " + ethernetConfig.mdiMode);
            Console.WriteLine("    BR Pairs  : " + ethernetConfig.brPairs);  
            Console.WriteLine("-------------------------------------------------------------------\n");
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message if channel assignment is not valid.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintAssignErrorAndPopupHwConf()
        {
            Console.WriteLine("\nPlease check application settings of \"" + appName + " Ethernet 1/2\",\nassign it to a available hardware channels and press enter.");
            EthernetDemo.XL_PopupHwConfig();
            Console.ReadKey();
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve the application channel assignment and test if this channel can be opened
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static bool GetAppChannelAndTestIsOk(uint appChIdx, ref UInt64 chMask, ref int chIdx)
        {
            XLDefine.XL_Status status = EthernetDemo.XL_GetApplConfig(appName, appChIdx, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_ETHERNET);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                Console.WriteLine("XL_GetApplConfig      : " + status);
                PrintFunctionError();
            }

            chMask = EthernetDemo.XL_GetChannelMask(hwType, (int)hwIndex, (int)hwChannel);
            chIdx = EthernetDemo.XL_GetChannelIndex(hwType, (int)hwIndex, (int)hwChannel);
            if (chIdx < 0)
            {
                // the (hwType, hwIndex, hwChannel) triplet stored in the application configuration does not refer to any available channel.
                return false;
            }

            // test if Ethernet is available on this channel
            return (driverConfig.channel[chIdx].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_ETHERNET) != 0;
        }
        // -----------------------------------------------------------------------------------------------



        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends a Ethernet message.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void EthernetTransmitDemo()
        {
            XLDefine.XL_Status              txStatus;
            XLClass.T_XL_ETH_DATAFRAME_TX   txData = new XLClass.T_XL_ETH_DATAFRAME_TX();

            
            txData.sourceMAC                     = new byte[] {00, 00, 00, 00, 00, 01};
            txData.destMAC                       = new byte[] {00, 00, 00, 00, 00, 02};
            txData.frameIdentifier               = 0;
            txData.flags                         = XLDefine.XLethernet_TX_Flags.XL_ETH_DATAFRAME_FLAGS_USE_SOURCE_MAC;
            txData.dataLen                       = 1502; //min. 46 bytes + 2 bytes for etherType
            txData.frameData.ethFrame.etherType  = HostToNetworkOrder(0x0ffe);

            for (int i = 0; i < txData.dataLen - 2; i++)
            {
                txData.frameData.ethFrame.payload[i] = (byte)i;
            }

            txStatus = EthernetDemo.XL_EthTransmit(portHandle, accessMaskChannel1, 0xAFFE, txData);
            Console.WriteLine("Transmit Package: " + txStatus);
        }
        // -----------------------------------------------------------------------------------------------



        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the ethernet channels to an application specific configuration.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void EthernetExplicitlyConfigureDevice()
        {
            XLDefine.XL_Status status;

            // Set bypass (not possible in all device modes)
            status = EthernetDemo.XL_EthSetBypass(portHandle, accessMask, 0, XLDefine.XLethernet_BypassMode.XL_ETH_BYPASS_INACTIVE);
            Console.WriteLine("Set bypass mode       : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Prepare Ethernet config
            XLClass.T_XL_ETH_CONFIG ethernetConfig = new XLClass.T_XL_ETH_CONFIG();

            // Set channel 1 to IEEE 100/1000 MBit/s on RJ45 connector
            ethernetConfig.speed = XLDefine.XLethernetSettings_Speed.XL_ETH_MODE_SPEED_AUTO_100_1000;
            ethernetConfig.duplex = XLDefine.XLethernetSettings_Duplex.XL_ETH_MODE_DUPLEX_AUTO;
            ethernetConfig.connector = XLDefine.XLethernetSettings_Connector.XL_ETH_MODE_CONNECTOR_RJ45;
            ethernetConfig.phy = XLDefine.XLethernetSettings_Phy.XL_ETH_MODE_PHY_IEEE_802_3;
            ethernetConfig.clockMode = XLDefine.XLethernetSettings_ClockMode.XL_ETH_MODE_CLOCK_AUTO;
            ethernetConfig.mdiMode = XLDefine.XLethernetSettings_MdiMode.XL_ETH_MODE_MDI_AUTO;
            ethernetConfig.brPairs = XLDefine.XLethernetSettings_BRpairs.XL_ETH_MODE_BR_PAIR_DONT_CARE;

            status = EthernetDemo.XL_EthSetConfig(portHandle, accessMaskChannel1, 0, ethernetConfig);
            Console.WriteLine("Set Ethernet conf. CH1: " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Same configuration for channel 2
            status = EthernetDemo.XL_EthSetConfig(portHandle, accessMaskChannel2, 0, ethernetConfig);
            Console.WriteLine("Set Ethernet conf. CH2: " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// EVENT THREAD (RX)
        /// 
        /// RX thread waits for Vector interface events and displays filtered Ethernet messages.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void RXThread()
        {
            // Create new object containing received data 
            XLClass.T_XL_ETH_EVENT receivedEvent = new XLClass.T_XL_ETH_EVENT();

            // Result of XL Driver function calls
            XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_SUCCESS;

            // Result values of WaitForSingleObject 
            XLDefine.WaitResults waitResult = new XLDefine.WaitResults();



            // Note: this thread will be destroyed by MAIN
            while (true)
            {
                // Wait for hardware events
                waitResult = (XLDefine.WaitResults)WaitForSingleObject(eventHandle, 1000);

                // If event occurred...
                if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
                {
                    // ...init xlStatus first
                    xlStatus = XLDefine.XL_Status.XL_SUCCESS;

                    // afterwards: while hw queue is not empty...
                    while (xlStatus != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
                    {

                        // ...receive data from hardware.
                        xlStatus = EthernetDemo.XL_EthReceive(portHandle, ref receivedEvent);

                        //  If receiving succeed (filter time sync events)...
                        if (xlStatus == XLDefine.XL_Status.XL_SUCCESS && receivedEvent.tag != (XLDefine.XLethernet_EventTags) 8)
                        {
                            Console.WriteLine("RX Ok: " + receivedEvent.tag);

                            string output = "";


                            if (receivedEvent.tag == XLDefine.XLethernet_EventTags.XL_ETH_EVENT_TAG_FRAMERX)
                            {
                                output = "Source MAC: ";                                
                                for (int i = 0; i < receivedEvent.tagData.frameRxOk.sourceMAC.Length; i++)
                                {
                                    output += receivedEvent.tagData.frameRxOk.sourceMAC[i];
                                    if (i < receivedEvent.tagData.frameRxOk.sourceMAC.Length - 1) output += ":";
                                }

                                output += "\nDest MAC: ";
                                for (int i = 0; i < receivedEvent.tagData.frameRxOk.destMAC.Length; i++)
                                {
                                    output += receivedEvent.tagData.frameRxOk.destMAC[i];
                                    if (i < receivedEvent.tagData.frameRxOk.destMAC.Length - 1) output += ":";
                                }

                                ushort etherType = receivedEvent.tagData.frameRxOk.frameData.ethFrame.etherType;
                                output += String.Format("\nEtherType: 0x{0:X4}", NetworkToHostOrder(etherType));

                                output += "\n-----------------------------------------------------------------\n";
                                output += "PAYLOAD\n";
                                
                                for (int i = 0; i < receivedEvent.tagData.frameRxOk.dataLen - 2 &&
                                                i < receivedEvent.tagData.frameRxOk.frameData.ethFrame.payload.Length; i++)
                                {
                                    output += receivedEvent.tagData.frameRxOk.frameData.ethFrame.payload[i] + " ";

                                }

                                Console.WriteLine(output);
                            }
                        }
                    }
                }
                // No event occurred
            }
        }
        // -----------------------------------------------------------------------------------------------

        private static ushort NetworkToHostOrder(ushort val)
        {
            if (BitConverter.IsLittleEndian)
            {
                val = (ushort)((val << 8) | (val >> 8));
            }
            return val;
        }

        private static ushort HostToNetworkOrder(ushort val)
        {
            return NetworkToHostOrder(val);
        }
    }
}
