/*-------------------------------------------------------------------------------------------
| File        : Class1.cs
| Project     : Vector CAN FD .NET Example
|
| Description : This example demonstrates the basic CAN FD functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/

using System;
using System.Runtime.InteropServices;
using System.Threading;
using vxlapi_NET;


namespace xlCANDemo
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    class Class1
    {
        // -----------------------------------------------------------------------------------------------
        // DLL Import for RX events
        // -----------------------------------------------------------------------------------------------
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int WaitForSingleObject(int handle, int timeOut);
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        // Global variables
        // -----------------------------------------------------------------------------------------------
        // Driver access through XLDriver (wrapper)
        private static XLDriver CANDemo                 = new XLDriver();
        private static String appName                   = "xlCANdemoNET";

        // Driver configuration
        private static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();

        // Variables required by XLDriver
        private static XLDefine.XL_HardwareType hwType  = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        private static uint hwIndex                     = 0;
        private static uint hwChannel                   = 0;   
        private static int portHandle                   = -1;
        private static int eventHandle                  = -1;
        private static UInt64 accessMask                = 0;
        private static UInt64 permissionMask            = 0;
        private static UInt64 txMask                    = 0;
        private static UInt64 rxMask                    = 0;
        private static int txCi                         = 0;
        private static int rxCi                         = 0;


        private static uint canFdModeNoIso              = 0;      // Global CAN FD ISO (default) / no ISO mode flag

        // RX thread
        private static Thread rxThread;
        private static bool blockRxThread = false; 
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// MAIN
        /// 
        /// Sends and receives CAN FD messages using main methods of the "XLDriver" class.
        /// This demo requires two connected CAN channels (Vector network interface). 
        /// The configuration is read from Vector Hardware Config (vcanconf.exe).
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        [STAThread]
        static int Main(string[] args)
        {
            XLDefine.XL_Status status;

            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("                    xlCANFDdemo.NET C# V11.0                       ");
            Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.");
            Console.WriteLine("-------------------------------------------------------------------\n");

            // print .NET wrapper version
            Console.WriteLine("vxlapi_NET        : " + typeof(XLDriver).Assembly.GetName().Version);

            // Open XL Driver
            status = CANDemo.XL_OpenDriver();
            Console.WriteLine("Open Driver       : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Get XL Driver configuration
            status = CANDemo.XL_GetDriverConfig(ref driverConfig);
            Console.WriteLine("Get Driver Config : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Convert the dll version number into a readable string
            Console.WriteLine("DLL Version       : " + CANDemo.VersionToString(driverConfig.dllVersion));

            // Display channel count
            Console.WriteLine("Channels found    : " + driverConfig.channelCount);


            // Display all found channels
            for (int i = 0; i < driverConfig.channelCount; i++)
            {
                Console.WriteLine("\n                   [{0}] " + driverConfig.channel[i].name, i);

                if ((driverConfig.channel[i].channelCapabilities & XLDefine.XL_ChannelCapabilities.XL_CHANNEL_FLAG_CANFD_ISO_SUPPORT) == XLDefine.XL_ChannelCapabilities.XL_CHANNEL_FLAG_CANFD_ISO_SUPPORT)
                    Console.WriteLine("                    - CAN FD Support  : yes");
                else Console.WriteLine("                    - CAN FD Support  : no");

                Console.WriteLine("                    - Channel Mask    : " + driverConfig.channel[i].channelMask);
                Console.WriteLine("                    - Transceiver Name: " + driverConfig.channel[i].transceiverName);
            }

            // If the application name cannot be found in VCANCONF...
            if ((CANDemo.XL_GetApplConfig(appName, 0, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN) != XLDefine.XL_Status.XL_SUCCESS) ||
                (CANDemo.XL_GetApplConfig(appName, 1, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN) != XLDefine.XL_Status.XL_SUCCESS))
            {
                //...create the item with two CAN channels
                CANDemo.XL_SetApplConfig(appName, 0, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN);
                CANDemo.XL_SetApplConfig(appName, 1, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN);
                PrintAssignErrorAndPopupHwConf();
            }

            // Request the user to assign channels until both CAN1 (Tx) and CAN2 (Rx) are assigned to usable channels
            while (!GetAppChannelAndTestIsOk(0, ref txMask, ref txCi) || !GetAppChannelAndTestIsOk(1, ref rxMask, ref rxCi))
            {
                PrintAssignErrorAndPopupHwConf();
            }

            PrintConfig();

            accessMask = txMask | rxMask;
            permissionMask = accessMask;

            // --------------------
            // Open port
            // --------------------
            status = CANDemo.XL_OpenPort(ref portHandle, appName, accessMask, ref permissionMask, 16000, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION_V4, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN);
            Console.WriteLine("\n\nOpen Port             : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // --------------------
            // Set CAN FD config
            // --------------------
            XLClass.XLcanFdConf canFdConf   = new XLClass.XLcanFdConf();

            // arbitration bitrate
            canFdConf.arbitrationBitRate    = 1000000;
            canFdConf.tseg1Abr              = 6;
            canFdConf.tseg2Abr              = 3;
            canFdConf.sjwAbr                = 2;

            // data bitrate
            canFdConf.dataBitRate           = canFdConf.arbitrationBitRate * 2;
            canFdConf.tseg1Dbr              = 6;
            canFdConf.tseg2Dbr              = 3;
            canFdConf.sjwDbr                = 2;

            if (canFdModeNoIso > 0)
            {
                canFdConf.options = (byte)XLDefine.XL_CANFD_ConfigOptions.XL_CANFD_CONFOPT_NO_ISO;
            }
            else
            {
                canFdConf.options = 0;
            }

            status = CANDemo.XL_CanFdSetConfiguration(portHandle, accessMask, canFdConf);
            Console.WriteLine("\n\nSet CAN FD Config     : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Get RX event handle
            status = CANDemo.XL_SetNotification(portHandle, ref eventHandle, 1);
            Console.WriteLine("Set Notification      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Activate channel - with reset clock
            status = CANDemo.XL_ActivateChannel(portHandle, accessMask, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN, XLDefine.XL_AC_Flags.XL_ACTIVATE_RESET_CLOCK);
            Console.WriteLine("Activate Channel      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Get XL Driver configuration to get the actual setup parameter
            status = CANDemo.XL_GetDriverConfig(ref driverConfig);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            if (canFdModeNoIso > 0)
            {
                Console.WriteLine("CAN FD mode           : NO ISO");
            }
            else
            {
                Console.WriteLine("CAN FD mode           : ISO");
            }
            Console.WriteLine("TX Arb. BitRate       : " + driverConfig.channel[txCi].busParams.dataCanFd.arbitrationBitRate
                            + "Bd, Data Bitrate: " + driverConfig.channel[txCi].busParams.dataCanFd.dataBitRate + "Bd");

            // Run Rx thread
            Console.WriteLine("Start Rx thread...");
            rxThread = new Thread(new ThreadStart(RXThread));
            rxThread.Start();

            // User information
            Console.WriteLine("Press <ENTER> to transmit CAN messages \n  <b>, <ENTER> to block Rx thread for rx-overrun-test \n  <B>, <ENTER> burst of CAN TX messages \n  <x>, <ENTER> to exit");

            // Transmit CAN data
            while (true)
            {
                if (blockRxThread)Console.WriteLine("Rx thread blocked.");

                    
                // Read user input
                string str = Console.ReadLine();
                if (str == "b")blockRxThread = !blockRxThread;                        
                else if (str == "B")
                {
                    for (int i = 0; i < 1000; i++)
                    {
                        // Burst of CAN frames
                        CANFDTransmitDemo();
                    }
                }
                else if (str == "x") break; 
                else
                {
                    // Send CAN frames
                    CANFDTransmitDemo();
                }
            }

            // Kill Rx thread
            rxThread.Abort();
            Console.WriteLine("Close Port                     : " + CANDemo.XL_ClosePort(portHandle));
            Console.WriteLine("Close Driver                   : " + CANDemo.XL_CloseDriver());


            return 0;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message/exit in case of a functional call does not return XL_SUCCESS
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static int PrintFunctionError()
        {
            Console.WriteLine("\nERROR: Function call failed!\nPress any key to continue...");
            Console.ReadKey();
            return -1;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Displays the Vector Hardware Configuration.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintConfig()
        {
            Console.WriteLine("\n\nAPPLICATION CONFIGURATION");

            foreach (int channelIndex in new int[] { txCi, rxCi })
            {
                Console.WriteLine("-------------------------------------------------------------------");
                Console.WriteLine("Configured Hardware Channel : " + driverConfig.channel[channelIndex].name);
                Console.WriteLine("Hardware Driver Version     : " + CANDemo.VersionToString(driverConfig.channel[channelIndex].driverVersion));
                Console.WriteLine("Used Transceiver            : " + driverConfig.channel[channelIndex].transceiverName);
            }

            Console.WriteLine("-------------------------------------------------------------------\n");
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message if channel assignment is not valid.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintAssignErrorAndPopupHwConf()
        {
            Console.WriteLine("\nPlease check application settings of \"" + appName + " CAN1/CAN2\",\nassign it to available hardware channels and press enter.");
            CANDemo.XL_PopupHwConfig();
            Console.ReadKey();
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve the application channel assignment and test if this channel can be opened
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static bool GetAppChannelAndTestIsOk(uint appChIdx, ref UInt64 chMask, ref int chIdx)
        {
            XLDefine.XL_Status status = CANDemo.XL_GetApplConfig(appName, appChIdx, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                Console.WriteLine("XL_GetApplConfig      : " + status);
                PrintFunctionError();
            }

            chMask = CANDemo.XL_GetChannelMask(hwType, (int)hwIndex, (int)hwChannel);
            chIdx = CANDemo.XL_GetChannelIndex(hwType, (int)hwIndex, (int)hwChannel);
            if (chIdx < 0 || chIdx >= driverConfig.channelCount)
            {
                // the (hwType, hwIndex, hwChannel) triplet stored in the application configuration does not refer to any available channel.
                return false;
            }

            if ((driverConfig.channel[chIdx].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_CAN) == 0)
            {
                // CAN is not available on this channel
                return false;
            }

            if (canFdModeNoIso > 0)
            {
                if ((driverConfig.channel[chIdx].channelCapabilities & XLDefine.XL_ChannelCapabilities.XL_CHANNEL_FLAG_CANFD_BOSCH_SUPPORT) == 0)
                {
                    Console.WriteLine("{0} ({1}) does not support CAN FD NO-ISO", driverConfig.channel[chIdx].name.TrimEnd(' ', '\0'),
                        driverConfig.channel[chIdx].transceiverName.TrimEnd(' ', '\0'));
                    return false;
                }
            }
            else
            {
                if ((driverConfig.channel[chIdx].channelCapabilities & XLDefine.XL_ChannelCapabilities.XL_CHANNEL_FLAG_CANFD_ISO_SUPPORT) == 0)
                {
                    Console.WriteLine("{0} ({1}) does not support CAN FD ISO", driverConfig.channel[chIdx].name.TrimEnd(' ', '\0'),
                        driverConfig.channel[chIdx].transceiverName.TrimEnd(' ', '\0'));
                    return false;
                }
            }

            return true;
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends some CAN messages.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void CANFDTransmitDemo()
        {
            XLDefine.XL_Status txStatus;
            
            // Create an event collection with 2 messages (events)
            XLClass.xl_canfd_event_collection xlEventCollection = new XLClass.xl_canfd_event_collection(2);

            // event 1
            xlEventCollection.xlCANFDEvent[0].tag               = XLDefine.XL_CANFD_TX_EventTags.XL_CAN_EV_TAG_TX_MSG;
            xlEventCollection.xlCANFDEvent[0].tagData.canId     = 0x100;
            xlEventCollection.xlCANFDEvent[0].tagData.dlc       = XLDefine.XL_CANFD_DLC.DLC_CAN_CANFD_8_BYTES;
            xlEventCollection.xlCANFDEvent[0].tagData.msgFlags  = XLDefine.XL_CANFD_TX_MessageFlags.XL_CAN_TXMSG_FLAG_BRS | XLDefine.XL_CANFD_TX_MessageFlags.XL_CAN_TXMSG_FLAG_EDL;
            xlEventCollection.xlCANFDEvent[0].tagData.data[0]   = 1;
            xlEventCollection.xlCANFDEvent[0].tagData.data[1]   = 1;
            xlEventCollection.xlCANFDEvent[0].tagData.data[2]   = 2;
            xlEventCollection.xlCANFDEvent[0].tagData.data[3]   = 2;
            xlEventCollection.xlCANFDEvent[0].tagData.data[4]   = 3;
            xlEventCollection.xlCANFDEvent[0].tagData.data[5]   = 3;
            xlEventCollection.xlCANFDEvent[0].tagData.data[6]   = 4;
            xlEventCollection.xlCANFDEvent[0].tagData.data[7]   = 4;


            // event 2
            xlEventCollection.xlCANFDEvent[1].tag               = XLDefine.XL_CANFD_TX_EventTags.XL_CAN_EV_TAG_TX_MSG;
            xlEventCollection.xlCANFDEvent[1].tagData.canId     = 0x200;
            xlEventCollection.xlCANFDEvent[1].tagData.dlc       = XLDefine.XL_CANFD_DLC.DLC_CANFD_64_BYTES;
            xlEventCollection.xlCANFDEvent[1].tagData.msgFlags  = XLDefine.XL_CANFD_TX_MessageFlags.XL_CAN_TXMSG_FLAG_BRS | XLDefine.XL_CANFD_TX_MessageFlags.XL_CAN_TXMSG_FLAG_EDL;
            xlEventCollection.xlCANFDEvent[1].tagData.data[0]   = 7;
            xlEventCollection.xlCANFDEvent[1].tagData.data[1]   = 7;
            xlEventCollection.xlCANFDEvent[1].tagData.data[2]   = 8;
            xlEventCollection.xlCANFDEvent[1].tagData.data[3]   = 8;
            xlEventCollection.xlCANFDEvent[1].tagData.data[4]   = 9;
            xlEventCollection.xlCANFDEvent[1].tagData.data[5]   = 9;
            xlEventCollection.xlCANFDEvent[1].tagData.data[6]   = 3;
            xlEventCollection.xlCANFDEvent[1].tagData.data[7]   = 2;
            xlEventCollection.xlCANFDEvent[1].tagData.data[8]   = 1;
            xlEventCollection.xlCANFDEvent[1].tagData.data[63]  = 9;


            // Transmit events
            uint messageCounterSent = 0;
            txStatus = CANDemo.XL_CanTransmitEx(portHandle, txMask, ref messageCounterSent, xlEventCollection);
            Console.WriteLine("Transmit Message ({0})     : " + txStatus, messageCounterSent);
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// EVENT THREAD (RX)
        /// 
        /// RX thread waits for Vector interface events and displays filtered CAN messages.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void RXThread()
        {
            // Create new object containing received data 
            XLClass.XLcanRxEvent receivedEvent = new XLClass.XLcanRxEvent();

            // Result of XL Driver function calls
            XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_SUCCESS;

            // Result values of WaitForSingleObject 
            XLDefine.WaitResults waitResult = new XLDefine.WaitResults();


            // Note: this thread will be destroyed by MAIN
            while (true)
            {
                // Wait for hardware events
                waitResult = (XLDefine.WaitResults)WaitForSingleObject(eventHandle, 1000);

                // If event occurred...
                if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
                {
                    // ...init xlStatus first
                    xlStatus = XLDefine.XL_Status.XL_SUCCESS;

                    // afterwards: while hw queue is not empty...
                    while (xlStatus != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
                    {
                        // ...block RX thread to generate RX-Queue overflows
                        while (blockRxThread) Thread.Sleep(1000);

                        // ...receive data from hardware.
                        xlStatus = CANDemo.XL_CanReceive(portHandle, ref receivedEvent);

                        //  If receiving succeed....
                        if (xlStatus == XLDefine.XL_Status.XL_SUCCESS)
                        {
                            Console.WriteLine(CANDemo.XL_CanGetEventString(receivedEvent));
                        }
                    }
                }
                // No event occurred
            }
        }
        // -----------------------------------------------------------------------------------------------
    }
}
