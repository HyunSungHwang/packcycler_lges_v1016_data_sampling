/*-------------------------------------------------------------------------------------------
| File        : Class1.cs
| Project     : Vector LIN .NET Example
|
| Description : This example demonstrates the basic LIN functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/

using System;
using System.Runtime.InteropServices;
using System.Threading;
using vxlapi_NET;

namespace xlLINDemo
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    class Class1
    {
        // -----------------------------------------------------------------------------------------------
        // DLL Import for RX events
        // -----------------------------------------------------------------------------------------------
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int WaitForSingleObject(int handle, int timeOut);
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        // Global variables
        // -----------------------------------------------------------------------------------------------
        // Driver access through XLDriver (wrapper)
        private static XLDriver LINDemo                         = new XLDriver();
        private static String appName                           = "xlLINdemoNET";

        // Driver configuration
        private static XLClass.xl_driver_config driverConfig    = new XLClass.xl_driver_config();

        // Variables required by XLDriver
        private static XLDefine.XL_HardwareType hwType          = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        private static XLClass.xl_linStatPar linStatusParams    = new XLClass.xl_linStatPar();
        private static uint hwIndex                             = 0;
        private static uint hwChannel                           = 0;
        private static int portHandle                           = -1;
        private static int eventHandle                          = -1;

        private static UInt64 accessMaskMaster                  = 0;
        private static UInt64 permissionMask                    = 0;
        private static int channelIndex                         = 0;

        private static Thread rxThread;
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// MAIN
        /// 
        /// Description: Sends and receives LIN messages using main methods of the "XLDriver" class.
        /// This demo requires one LIN channel (Vector network interface). 
        /// The configuration is read from Vector Hardware Config (vcanconf.exe).
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        [STAThread]
        static void Main(string[] args)
        {
            XLDefine.XL_Status status;
            
            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("                       xlLINdemo Single C# V11.0                   ");
            Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.");
            Console.WriteLine("-------------------------------------------------------------------\n");

            // Print .NET wrapper version
            Console.WriteLine("vxlapi_NET        : " + typeof(XLDriver).Assembly.GetName().Version);

            // Open XL Driver
            status = LINDemo.XL_OpenDriver();
            Console.WriteLine("Open Driver       : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Get XL Driver configuration
            status = LINDemo.XL_GetDriverConfig(ref driverConfig);
            Console.WriteLine("Get Driver Config : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Convert the dll version number to readable string
            Console.WriteLine("DLL Version       : " + LINDemo.VersionToString(driverConfig.dllVersion));


            // Display channel count
            Console.WriteLine("Channels found    : " + driverConfig.channelCount);

            // Display found channels
            for (int i = 0; i < driverConfig.channelCount; i++)
            {
                Console.WriteLine("\n                   [{0}] " + driverConfig.channel[i].name, i);
                Console.WriteLine("                    - Channel Mask    : " + driverConfig.channel[i].channelMask);
                Console.WriteLine("                    - Transceiver Name: " + driverConfig.channel[i].transceiverName);
                Console.WriteLine("                    - Serial Number   : " + driverConfig.channel[i].serialNumber);
            }

            // If the application name cannot be found in VCANCONF....
            if ((LINDemo.XL_GetApplConfig(appName, 0, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_LIN) != XLDefine.XL_Status.XL_SUCCESS))
            {
                //...create the item with one LIN channel
                LINDemo.XL_SetApplConfig(appName, 0, 0, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_LIN);
                PrintAssignErrorAndPopupHwConf();
            }

            // Request the user to assign channels until a usable LIN (Master+Slave) channel is assigned
            while (!GetAppChannelAndTestIsOk(0, ref accessMaskMaster, ref channelIndex))
            {
                PrintAssignErrorAndPopupHwConf();
            }

            PrintConfig();
            permissionMask = accessMaskMaster;

            // Open port for both channels
            status = LINDemo.XL_OpenPort(ref portHandle, appName, accessMaskMaster, ref permissionMask, 256, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION_V3, XLDefine.XL_BusTypes.XL_BUS_TYPE_LIN);
            Console.WriteLine("\n\nOpen Port                      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // MASTER/SLAVE
            XLClass.xl_linStatPar linStatPar = new XLClass.xl_linStatPar();
            linStatPar.baudrate = 9600;
            linStatPar.LINMode = XLDefine.XL_LIN_Mode.XL_LIN_MASTER;
            linStatPar.LINVersion = XLDefine.XL_LIN_Version.XL_LIN_VERSION_2_0;

            status = LINDemo.XL_LinSetChannelParams(portHandle, accessMaskMaster, linStatPar);
            Console.WriteLine("\nSet Channel Parameters (MASTER): " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Set DLC for LIN IDs 0...63
            byte[] DLC = new byte[64];
            for (int i = 0; i < 64; i++) DLC[i] = 8;
            status = LINDemo.XL_LinSetDLC(portHandle, accessMaskMaster, DLC);
            Console.WriteLine("Set DLC                        : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Set LIN slave
            byte[] linData = { 1, 2, 3, 4, 5, 6, 7, 8 };
            byte msgDLC = 8;
            byte id = 10;                    
            status = LINDemo.XL_LinSetSlave(portHandle, accessMaskMaster, id, linData, msgDLC, XLDefine.XL_LIN_CalcChecksum.XL_LIN_CALC_CHECKSUM_ENHANCED);
            Console.WriteLine("Set SLAVE                      : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Activate channel
            status = LINDemo.XL_ActivateChannel(portHandle, accessMaskMaster, XLDefine.XL_BusTypes.XL_BUS_TYPE_LIN, XLDefine.XL_AC_Flags.XL_ACTIVATE_NONE);
            Console.WriteLine("Activate Channel               : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Get RX event handle
            status = LINDemo.XL_SetNotification(portHandle, ref eventHandle, 1);
            Console.WriteLine("\nSet Notification               : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Run Rx thread
            Console.WriteLine("Start Rx Thread...");
            rxThread = new Thread(new ThreadStart(RXThread));
            rxThread.Start();


            // MAIN APPLICATION LOOP
            Console.WriteLine("Press <ENTER> to request LIN data. Press <x>, <ENTER> to close application.");
            while (Console.ReadLine() != "x")
            {
                Console.WriteLine("LIN Send Request               : " + LINDemo.XL_LinSendRequest(portHandle, accessMaskMaster, id, 0));
            }

            // Close application
            rxThread.Abort();
            Console.WriteLine("Close Port                     : " + LINDemo.XL_ClosePort(portHandle));
            Console.WriteLine("Close Driver                   : " + LINDemo.XL_CloseDriver());
            Console.ReadLine();
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message/exit in case of a functional call does not return XL_SUCCESS
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static int PrintFunctionError()
        {
            Console.WriteLine("\nERROR: Function call failed!\nPress any key to continue...");
            Console.ReadKey();
            return -1;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Displays Vector Hardware Configuration.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintConfig()
        {
            Console.WriteLine("\n\nAPPLICATION CONFIGURATION");
            Console.WriteLine("-------------------------------------------------------------------\n");
            Console.WriteLine("Configured Hardware Channel : " + driverConfig.channel[channelIndex].name);
            Console.WriteLine("Hardware Driver Version     : " + LINDemo.VersionToString(driverConfig.channel[channelIndex].driverVersion));
            Console.WriteLine("Used Transceiver            : " + driverConfig.channel[channelIndex].transceiverName);
            Console.WriteLine("-------------------------------------------------------------------\n");
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message if channel assignment is not valid and popup VHwConfig, so the user can correct the assignment
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintAssignErrorAndPopupHwConf()
        {
            Console.WriteLine("\nPlease check application settings of \"" + appName + " LIN1\",\nassign it to an available hardware channel and press enter.");
            LINDemo.XL_PopupHwConfig();
            Console.ReadLine();
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve the application channel assignment and test if this channel can be opened
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static bool GetAppChannelAndTestIsOk(uint appChIdx, ref UInt64 chMask, ref int chIdx)
        {
            XLDefine.XL_Status status = LINDemo.XL_GetApplConfig(appName, appChIdx, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_LIN);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                Console.WriteLine("XL_GetApplConfig      : " + status);
                PrintFunctionError();
            }

            chMask = LINDemo.XL_GetChannelMask(hwType, (int)hwIndex, (int)hwChannel);
            chIdx = LINDemo.XL_GetChannelIndex(hwType, (int)hwIndex, (int)hwChannel);
            if (chIdx < 0 || chIdx >= driverConfig.channelCount)
            {
                // the (hwType, hwIndex, hwChannel) triplet stored in the application configuration does not refer to any available channel.
                return false;
            }

            // test if LIN is available on this channel
            return (driverConfig.channel[chIdx].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_LIN) != 0;
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// EVENT THREAD (RX)
        /// RX thread waits for XL interface events and displays LIN messgaes.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public static void RXThread()
        {
            // Create new object containing received data 
            XLClass.xl_event receivedEvent = new XLClass.xl_event();

            // Result of XL Driver function calls
            XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_SUCCESS;

            // Result values of WaitForSingleObject 
            XLDefine.WaitResults waitResult = new XLDefine.WaitResults();  
        

            LINDemo.XL_FlushReceiveQueue(portHandle);

            // Note: this thread will be destroyed by MAIN
            while (true)
            {
                // Wait for hardware events
                waitResult = (XLDefine.WaitResults)WaitForSingleObject(eventHandle, 1000);

                // If event occured...
                if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
                {
                    // ...init xlStatus first
                    xlStatus = XLDefine.XL_Status.XL_SUCCESS;

                    // afterwards: while hardware queue is not empty...
                    while (xlStatus != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
                    {
                        // ...receive data from hardware...
                        xlStatus = LINDemo.XL_Receive(portHandle, ref receivedEvent);

                        switch (receivedEvent.tag)
                        {
                            case XLDefine.XL_EventTags.XL_LIN_MSG:
                                string dir = "RX ";
                                if ((receivedEvent.tagData.linMsgApi.linMsg.flags & XLDefine.XL_MessageFlags.XL_LIN_MSGFLAG_TX)
                                  == XLDefine.XL_MessageFlags.XL_LIN_MSGFLAG_TX)
                                {
                                    dir = "TX ";
                                }

                                Console.WriteLine("XL_LIN_MSG, " + dir + " id:" +
                                      receivedEvent.tagData.linMsgApi.linMsg.id + ", data:" +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[0] + " " +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[1] + " " +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[2] + " " +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[3] + " " +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[4] + " " +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[5] + " " +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[6] + " " +
                                      receivedEvent.tagData.linMsgApi.linMsg.data[7]);
                                break;

                            case XLDefine.XL_EventTags.XL_LIN_ERRMSG:
                                Console.WriteLine("XL_LIN_ERRMSG");
                                break;

                            case XLDefine.XL_EventTags.XL_LIN_SYNCERR:
                                Console.WriteLine("XL_LIN_SYNCERR");
                                break;

                            case XLDefine.XL_EventTags.XL_LIN_NOANS:
                                Console.WriteLine("XL_LIN_NOANS");
                                break;

                            case XLDefine.XL_EventTags.XL_LIN_WAKEUP:
                                Console.WriteLine("XL_LIN_WAKEUP");
                                break;

                            case XLDefine.XL_EventTags.XL_LIN_SLEEP:
                                Console.WriteLine("XL_LIN_SLEEP");
                                break;

                            case XLDefine.XL_EventTags.XL_LIN_CRCINFO:
                                Console.WriteLine("XL_LIN_CRCINFO");
                                break;

                        }
                    }
                }
                // No event occured
            }
        }
        // -----------------------------------------------------------------------------------------------
    }
}
