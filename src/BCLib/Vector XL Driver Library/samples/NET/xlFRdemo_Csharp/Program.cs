/*-------------------------------------------------------------------------------------------
| File        : Programm.cs
| Project     : Vector FlexRay .NET Example
|
| Description : This example demonstrates the basic FlexRay functionality of the XL.NET Driver Library
|--------------------------------------------------------------------------------------------
| Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
|-------------------------------------------------------------------------------------------*/


using System;
using System.Runtime.InteropServices;
using System.Threading;
using vxlapi_NET;


namespace xlFRDemo
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    class Class1
    {
        // -----------------------------------------------------------------------------------------------
        // DLL Import for RX events
        // -----------------------------------------------------------------------------------------------
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int WaitForSingleObject(int handle, int timeOut);
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        // Global variables
        // -----------------------------------------------------------------------------------------------
        // Driver access through XLDriver (wrapper)
        private static XLDriver FRDemo                  = new XLDriver();
        private static String appName                   = "xlFRdemoNET";
        private const uint RX_FIFO_SIZE                 = 32768;
        private const uint PAYLOAD_LENGTH               = 8;

        // Driver configuration
        private static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();
        private static XLClass.xl_fr_cluster_configuration frClusterConfig = new XLClass.xl_fr_cluster_configuration();
        private static XLClass.xl_fr_event eventBuffer  = new XLClass.xl_fr_event();
        private static XLClass.xl_fr_set_modes frMode   = new XLClass.xl_fr_set_modes();

        // Variables required by XLDriver
        private static XLDefine.XL_HardwareType hwType  = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        private static uint hwIndex                     = 0;
        private static uint hwChannel                   = 0;
        private static int portHandle                   = -1;
        private static int eventHandle                  = -1;
        private static UInt64 accessMask_deviceA        = 0;
        private static UInt64 accessMask_deviceB        = 0;
        private static UInt64 permissionMask            = 0;
        private static Thread rxThread;

        private static bool useCC                       = false;
        private static bool useSpyMode                  = false;

        private static uint numberEventsToPrint         = 100;
        private static bool printEvents                 = false;
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// MAIN
        /// 
        /// Configures a single Vector FlexRay interface for sending and receiving FR frames. 
        /// If the COLD CC cannot be set because of the missing Advanced License for the FR API, a second FlexRay
        /// interface is required. In this case use this application a second time with a different E-Ray ID.
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        [STAThread]
        static int Main(string[] args)
        {
            XLDefine.XL_Status status;

            // print .NET wrapper version
            Console.WriteLine("\nvxlapi_NET                 : " + typeof(XLDriver).Assembly.GetName().Version);

            // Open XL Driver
            status = FRDemo.XL_OpenDriver();
            Console.WriteLine("Open Driver                : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("                     xlFRdemo.NET C# V11.0                         ");
            Console.WriteLine("Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.");
            Console.WriteLine("-------------------------------------------------------------------\n");

            Console.WriteLine("\nIf you own the Advanced License of the FR API,\nyou may configure the Coldstart CC.");
            Console.Write("In this case you do not need a second FR interface.\nDo you want to activate the Coldstart CC? (y/n): ");
            ConsoleKeyInfo key = Console.ReadKey();

            if (key.Key == ConsoleKey.Y) {

              // ----------------------
              // Get Keyman licences...
              // ----------------------
              
              uint nbrOfBoxes = 0;
              
              UInt64[] licInfo = new UInt64[4];
              UInt64[] tmpLicInfo = new UInt64[4]; 
              uint boxMask = 0;
              uint boxSerial = 0;
              

              status = FRDemo.XL_GetKeymanBoxes(ref nbrOfBoxes);
              if (status == XLDefine.XL_Status.XL_SUCCESS) {
                Console.WriteLine("\nKeyman License found!\n");
                
                for (uint i = 0; i < nbrOfBoxes; i++)
                {
                  status = FRDemo.XL_GetKeymanInfo(i, ref boxMask, ref boxSerial, ref tmpLicInfo);
                  if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();
                  Console.WriteLine("\nKeyman Dongle " + i + ", Serialnumber: " + boxMask + " " + boxSerial);

                  licInfo[0] |= tmpLicInfo[0];
                  licInfo[1] |= tmpLicInfo[1];
                  licInfo[2] |= tmpLicInfo[2];
                  licInfo[3] |= tmpLicInfo[3];
                  Console.WriteLine("\nlic0: " + licInfo[0] + "\nlic1: " + licInfo[1] + "\nlic2: "+ licInfo[2] + "\nlic3: " + licInfo[3]);
                }
                
              }

              useCC = true; 
            }




            if (useCC) {
                // Spy mode only works if at least two other normal Flexray nodes are present in the cluster.
                Console.Write("\nDo you want to activate the Spy Mode on the second device? (y/n): ");
                key = Console.ReadKey();
                if (key.Key == ConsoleKey.Y) { useSpyMode = true; }
            }

           


            // Get  XL driver configuration
            status = FRDemo.XL_GetDriverConfig(ref driverConfig);
            Console.WriteLine("Get Driver Config          : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Convert DLL version number into readable string
            Console.WriteLine("DLL Version                : " + FRDemo.VersionToString(driverConfig.dllVersion));



            // If application cannot be found in VCANCONF....
            if ((FRDemo.XL_GetApplConfig(appName, 0, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_FLEXRAY) != XLDefine.XL_Status.XL_SUCCESS) ||
                (FRDemo.XL_GetApplConfig(appName, 1, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_FLEXRAY) != XLDefine.XL_Status.XL_SUCCESS))
            {

                //...create item with two FlexRay channel
                FRDemo.XL_SetApplConfig(appName, 0, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_FLEXRAY);
                FRDemo.XL_SetApplConfig(appName, 1, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_FLEXRAY);
                PrintAssignErrorAndPopupHwConf("-> No settings found in VCanConf.");
            }

            if (!useCC || useSpyMode)
            {
                // Request the user to assign channels until both FlexRay channel 1 and 2 are assigned to usable channels
                while (!GetAppChannelAndTestIsOk(0, ref accessMask_deviceA) || !GetAppChannelAndTestIsOk(1, ref accessMask_deviceB))
                {
                    PrintAssignErrorAndPopupHwConf("-> Either Device-A or Device-B not present.");
                }
                permissionMask = accessMask_deviceA | accessMask_deviceB;
            }
            else
            {
                // Request the user to assign FlexRay channel 1 until it is assigned to a usable channel
                while (!GetAppChannelAndTestIsOk(0, ref accessMask_deviceA))
                {
                    PrintAssignErrorAndPopupHwConf("-> Device-A not present.");
                }
                permissionMask = accessMask_deviceA;
            }


            // Open port
            status = FRDemo.XL_OpenPort(ref portHandle, appName, accessMask_deviceA | accessMask_deviceB, ref permissionMask, RX_FIFO_SIZE, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION_V4, XLDefine.XL_BusTypes.XL_BUS_TYPE_FLEXRAY);
            Console.WriteLine("\n\nOpen Port                  : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Set FlexRay configuration
            status = InitFlexRayConfiguration(ref frClusterConfig);
            Console.WriteLine("Set FlexRay Config         : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            // Define startup and sync frame
            Console.WriteLine("Init Startup/sync          : " + InitStartupNormal(1, 2));

            // Get RX event handle
            status = FRDemo.XL_SetNotification(portHandle, ref eventHandle, 1);
            Console.WriteLine("Set Notification           : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();


            Console.Write("\nInitialization finished.");

            // Run Rx thread
            Console.WriteLine("\nStart Rx Thread...");
            rxThread = new Thread(new ThreadStart(RXThread));
            rxThread.Start();

            if (useSpyMode)
            {
                Console.WriteLine("Activate Spy Mode on device B:" + FRDemo.XL_FrActivateSpy(portHandle, accessMask_deviceB, XLDefine.XL_FlexRay_SpyMode.XL_FR_SPY_MODE_ASYNCHRONOUS));
            }


            // Activate channel
            status = FRDemo.XL_ActivateChannel(portHandle, accessMask_deviceA | accessMask_deviceB, XLDefine.XL_BusTypes.XL_BUS_TYPE_FLEXRAY, XLDefine.XL_AC_Flags.XL_ACTIVATE_RESET_CLOCK);
            Console.WriteLine("Activate Channel           : " + status);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            printEvents = true;

            while (true)
            {
                key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.X || key.Key == ConsoleKey.Escape)
                {
                    break;  // exit while-loop
                }
                if (key.Key == ConsoleKey.U)
                {
                    numberEventsToPrint = UInt32.MaxValue;
                }
                printEvents = true;
            }

            // Kill Rx thread
            rxThread.Abort();

            return 0;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message/exit in case of a functional call does not return XL_SUCCESS
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static int PrintFunctionError()
        {
            Console.WriteLine("\nERROR: Function call failed!\nPress any key to continue...");
            Console.ReadKey();
            return -1;
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Error message if channel assignment is not valid and popup VHwConfig, so the user can correct the assignment
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static void PrintAssignErrorAndPopupHwConf(string errStr)
        {
            Console.WriteLine("\n\n-----------------------------------------------------------------------");
            Console.WriteLine("ERROR: Please assign each of the FlexRay channels of application");
            Console.WriteLine(" \"xlFRdemoNET\" to a different FR interface and press Enter.");
            Console.WriteLine(errStr);
            Console.WriteLine("-----------------------------------------------------------------------\n\n");
            FRDemo.XL_PopupHwConfig();
            Console.ReadKey();
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Configures the FlexRay node
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static XLDefine.XL_Status InitFlexRayConfiguration(ref XLClass.xl_fr_cluster_configuration xlConf)
        {
            xlConf.busGuardianEnable = 0;
            xlConf.baudrate = 10000;
            xlConf.busGuardianTick = 0;
            xlConf.externalClockCorrectionMode = 0;
            xlConf.gColdStartAttempts = 0x00000014;
            xlConf.gListenNoise = 11;
            xlConf.gMacroPerCycle = 5000;
            xlConf.gMaxWithoutClockCorrectionFatal = 1;
            xlConf.gMaxWithoutClockCorrectionPassive = 1;
            xlConf.gNetworkManagementVectorLength = 0;
            xlConf.gNumberOfMinislots = 200;
            xlConf.gNumberOfStaticSlots = 60;
            xlConf.gOffsetCorrectionStart = 4501;
            xlConf.gPayloadLengthStatic = PAYLOAD_LENGTH / 2;
            xlConf.gSyncNodeMax = 4;
            xlConf.gdActionPointOffset = 5;
            xlConf.gdDynamicSlotIdlePhase = 1;
            xlConf.gdMacrotick = 0;
            xlConf.gdMinislot = 7;
            xlConf.gdMiniSlotActionPointOffset = 3;
            xlConf.gdNIT = 500;
            xlConf.gdStaticSlot = 40;
            xlConf.gdSymbolWindow = 0;
            xlConf.gdTSSTransmitter = 5;
            xlConf.gdWakeupSymbolRxIdle = 0;
            xlConf.gdWakeupSymbolRxLow = 0;
            xlConf.gdWakeupSymbolRxWindow = 0;
            xlConf.gdWakeupSymbolTxIdle = 0;
            xlConf.gdWakeupSymbolTxLow = 0;
            xlConf.pAllowHaltDueToClock = 0;
            xlConf.pAllowPassiveToActive = 7;
            xlConf.pChannels = 3;
            xlConf.pClusterDriftDamping = 1;
            xlConf.pDecodingCorrection = 0x24;
            xlConf.pDelayCompensationA = 10;
            xlConf.pDelayCompensationB = 10;
            xlConf.pExternOffsetCorrection = 0;
            xlConf.pExternRateCorrection = 0;
            xlConf.pKeySlotUsedForStartup = 1;
            xlConf.pKeySlotUsedForSync = 1;
            xlConf.pLatestTx = 0x000001DF;
            xlConf.pMacroInitialOffsetA = 7;
            xlConf.pMacroInitialOffsetB = 7;
            xlConf.pMaxPayloadLengthDynamic = 2;
            xlConf.pMicroInitialOffsetA = 0x22;
            xlConf.pMicroInitialOffsetB = 0x22;
            xlConf.pMicroPerCycle = 0x00030D40;
            xlConf.pMicroPerMacroNom = 0;
            xlConf.pOffsetCorrectionOut = 0x00000057;
            xlConf.pRateCorrectionOut = 0x00000259;
            xlConf.pSamplesPerMicrotick = 0;
            xlConf.pSingleSlotEnabled = 0;
            xlConf.pWakeupChannel = 0;
            xlConf.pWakeupPattern = 0;
            xlConf.pdAcceptedStartupRange = 0xCD;
            xlConf.pdListenTimeout = 0x61CD8;
            xlConf.pdMaxDrift = 0x0000012C;
            xlConf.pdMicrotick = 0;
            xlConf.gdCASRxLowMax = 7;
            xlConf.gChannels = 0;
            xlConf.vExternOffsetControl = 0;
            xlConf.vExternRateControl = 0;
            xlConf.pChannelsMTS = 0;
            return FRDemo.XL_FrSetConfiguration(portHandle, accessMask_deviceA | accessMask_deviceB, ref frClusterConfig);
        }
        // -----------------------------------------------------------------------------------------------




        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends two Tx frames using E-Ray and COLD (no second FlexRay node required)
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static XLDefine.XL_Status InitStartupNormal(ushort erayId, ushort coldId)
        {

            XLClass.xl_fr_set_modes xlFrMode = new XLClass.xl_fr_set_modes();
            XLClass.xl_fr_event xlFrConfigFrame = new XLClass.xl_fr_event();
            byte[] payload = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88 };
            XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_ERROR;

            // setup the startup and sync frames for the E-Ray
            xlFrConfigFrame.tag = XLDefine.XL_FlexRay_EventTags.XL_FR_TX_FRAME;
            xlFrConfigFrame.flagsChip = XLDefine.XL_FlexRay_FlagsChip.XL_FR_CHANNEL_A;
            xlFrConfigFrame.size = 0;
            xlFrConfigFrame.userHandle = 0;
            xlFrConfigFrame.tagData.frTxFrame.flags = XLDefine.XL_FlexRay_FrameFlags.XL_FR_FRAMEFLAG_REQ_TXACK | XLDefine.XL_FlexRay_FrameFlags.XL_FR_FRAMEFLAG_STARTUP | XLDefine.XL_FlexRay_FrameFlags.XL_FR_FRAMEFLAG_SYNC;
            xlFrConfigFrame.tagData.frTxFrame.offset = 0;
            xlFrConfigFrame.tagData.frTxFrame.repetition = 2;
            xlFrConfigFrame.tagData.frTxFrame.payloadLength = (byte)PAYLOAD_LENGTH / 2;
            xlFrConfigFrame.tagData.frTxFrame.slotID = erayId;
            xlFrConfigFrame.tagData.frTxFrame.txMode = XLDefine.XL_FlexRay_TXMode.XL_FR_TX_MODE_CYCLIC;
            xlFrConfigFrame.tagData.frTxFrame.incrementOffset = 0;
            xlFrConfigFrame.tagData.frTxFrame.incrementSize = 0;

            // copy payload into event buffer to be sent
            for (int i = 0; i < payload.Length; i++) xlFrConfigFrame.tagData.frTxFrame.data[i] = payload[i];

            // initialize startup frame
            xlStatus = FRDemo.XL_FrInitStartupAndSync(portHandle, accessMask_deviceA, xlFrConfigFrame);
            if (xlStatus != XLDefine.XL_Status.XL_SUCCESS) return xlStatus;

            // setup the mode for the E-Ray CC
            xlFrMode.frMode = XLDefine.XL_FlexRay_Mode.XL_FR_MODE_NORMAL;
            xlFrMode.frStartupAttributes = XLDefine.XL_FlexRay_StartupAttributes.XL_FR_MODE_COLDSTART_LEADING;
            xlStatus = FRDemo.XL_FrSetMode(portHandle, accessMask_deviceA, xlFrMode);
            if (xlStatus != XLDefine.XL_Status.XL_SUCCESS) return xlStatus;



            if (!useCC)
            {
                // setup the startup and sync frames for the E-Ray
                xlFrConfigFrame.tag = XLDefine.XL_FlexRay_EventTags.XL_FR_TX_FRAME;
                xlFrConfigFrame.flagsChip = XLDefine.XL_FlexRay_FlagsChip.XL_FR_CHANNEL_A;
                xlFrConfigFrame.size = 0;
                xlFrConfigFrame.userHandle = 0;
                xlFrConfigFrame.tagData.frTxFrame.flags = XLDefine.XL_FlexRay_FrameFlags.XL_FR_FRAMEFLAG_STARTUP | XLDefine.XL_FlexRay_FrameFlags.XL_FR_FRAMEFLAG_SYNC;
                xlFrConfigFrame.tagData.frTxFrame.offset = 0;
                xlFrConfigFrame.tagData.frTxFrame.repetition = 2;
                xlFrConfigFrame.tagData.frTxFrame.payloadLength = (byte)PAYLOAD_LENGTH / 2;
                xlFrConfigFrame.tagData.frTxFrame.slotID = coldId;
                xlFrConfigFrame.tagData.frTxFrame.txMode = XLDefine.XL_FlexRay_TXMode.XL_FR_TX_MODE_CYCLIC;
                xlFrConfigFrame.tagData.frTxFrame.incrementOffset = 0;
                xlFrConfigFrame.tagData.frTxFrame.incrementSize = 0;

                // copy payload into event buffer to be sent
                for (int i = 0; i < payload.Length; i++) xlFrConfigFrame.tagData.frTxFrame.data[i] = payload[i];

                // initialize startup frame
                xlStatus = FRDemo.XL_FrInitStartupAndSync(portHandle, accessMask_deviceB, xlFrConfigFrame);
                if (xlStatus != XLDefine.XL_Status.XL_SUCCESS) return xlStatus;

                // setup the mode for the E-Ray CC
                xlFrMode.frMode = XLDefine.XL_FlexRay_Mode.XL_FR_MODE_NORMAL;
                xlFrMode.frStartupAttributes = XLDefine.XL_FlexRay_StartupAttributes.XL_FR_MODE_COLDSTART_LEADING;
                xlStatus = FRDemo.XL_FrSetMode(portHandle, accessMask_deviceB, xlFrMode);
                if (xlStatus != XLDefine.XL_Status.XL_SUCCESS) return xlStatus;
            }


            else
            {
                // setup the startup and sync frames for the COLD CC
                // NEEDS advanced LICENSE!
                xlFrConfigFrame.tag = XLDefine.XL_FlexRay_EventTags.XL_FR_TX_FRAME;
                xlFrConfigFrame.flagsChip = XLDefine.XL_FlexRay_FlagsChip.XL_FR_CC_COLD_A;
                xlFrConfigFrame.size = 0;
                xlFrConfigFrame.userHandle = 0;
                xlFrConfigFrame.tagData.frTxFrame.flags = XLDefine.XL_FlexRay_FrameFlags.XL_FR_FRAMEFLAG_STARTUP | XLDefine.XL_FlexRay_FrameFlags.XL_FR_FRAMEFLAG_SYNC;
                xlFrConfigFrame.tagData.frTxFrame.offset = 0;
                xlFrConfigFrame.tagData.frTxFrame.repetition = 2;
                xlFrConfigFrame.tagData.frTxFrame.payloadLength = (byte)(payload.Length / 2);
                xlFrConfigFrame.tagData.frTxFrame.slotID = coldId;
                xlFrConfigFrame.tagData.frTxFrame.txMode = XLDefine.XL_FlexRay_TXMode.XL_FR_TX_MODE_CYCLIC;
                xlFrConfigFrame.tagData.frTxFrame.incrementOffset = 0;
                xlFrConfigFrame.tagData.frTxFrame.incrementSize = 0;

                // setup the mode for the COLD CC
                xlStatus = FRDemo.XL_FrInitStartupAndSync(portHandle, accessMask_deviceA, xlFrConfigFrame);

                // check COLD CC license
                if (xlStatus == XLDefine.XL_Status.XL_ERR_NO_LICENSE)
                {
                    Console.WriteLine("\n\n-----------------------------------------------------------------------");
                    Console.WriteLine("WARNING: COLD CC cannot be set. Advanced FlexRay API license required!");
                    Console.WriteLine("         Please use a second FlexRay interface instead and");
                    Console.WriteLine("         change the E-Ray ID and the application name for this application.");
                    Console.WriteLine("         Assign the second hardware to the second application in VHWConf.");
                    Console.WriteLine("         Connect both FlexRay interfaces and restart both applications.");
                    Console.WriteLine("-----------------------------------------------------------------------\n\n");
                    return xlStatus;
                }
                if (xlStatus != XLDefine.XL_Status.XL_SUCCESS) return xlStatus;

                // setup the mode for the COLD CC
                xlFrMode.frMode = XLDefine.XL_FlexRay_Mode.XL_FR_MODE_COLD_NORMAL;
                xlFrMode.frStartupAttributes = XLDefine.XL_FlexRay_StartupAttributes.XL_FR_MODE_COLDSTART_LEADING;
                xlStatus = FRDemo.XL_FrSetMode(portHandle, accessMask_deviceA, xlFrMode);
            }


            if (xlStatus != XLDefine.XL_Status.XL_SUCCESS) return xlStatus;

            return xlStatus;
        }
        // -----------------------------------------------------------------------------------------------

        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve the application channel assignment and test if this channel can be opened
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        private static bool GetAppChannelAndTestIsOk(uint appChIdx, ref UInt64 chMask)
        {
            XLDefine.XL_Status status = FRDemo.XL_GetApplConfig(appName, appChIdx, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_FLEXRAY);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                Console.WriteLine("XL_GetApplConfig      : " + status);
                PrintFunctionError();
            }

            chMask = FRDemo.XL_GetChannelMask(hwType, (int)hwIndex, (int)hwChannel);
            int chIdx = FRDemo.XL_GetChannelIndex(hwType, (int)hwIndex, (int)hwChannel);
            if (chIdx < 0 || chIdx >= driverConfig.channelCount)
            {
                // the (hwType, hwIndex, hwChannel) triplet stored in the application configuration does not refer to any available channel.
                return false;
            }

            // test if FlexRay is available on this channel
            return (driverConfig.channel[chIdx].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_FLEXRAY) != 0;
        }
        // -----------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------
        /// <summary>
        /// EVENT THREAD (RX)
        /// RX thread waits for FlexRay interface events and displays filtered FR messages.
        /// </summary>
        // ----------------------------------------------------------------------------------------------- 
        public static void RXThread()
        {
            // Create new object containing received data  
            XLClass.xl_fr_event receivedEvent = new XLClass.xl_fr_event();

            // Result values of WaitForSingleObject 
            XLDefine.WaitResults waitResult = new XLDefine.WaitResults();     
             
            uint nbrEventsPrinted = 0;

            // Note: this thread will be destroyed by MAIN
            while (true)
            {
                int queuelevel = 0;
                FRDemo.XL_GetReceiveQueueLevel(portHandle, ref queuelevel);
                if (queuelevel == 0)
                {
                    // Wait for hardware events - we only get informed once if the queue level crosses the queue-notify-level!
                    waitResult = (XLDefine.WaitResults)WaitForSingleObject(eventHandle, 1000);
                }
                else
                {
                    // Some elements are already in queue
                    waitResult = XLDefine.WaitResults.WAIT_OBJECT_0;
                }


                // if event occurred...
                if (waitResult != XLDefine.WaitResults.WAIT_TIMEOUT)
                {

                    if (printEvents)
                    {

                        // Afterwards: while HW queue is not empty...
                        while (XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY != FRDemo.XL_FrReceive(portHandle, ref receivedEvent))
                        {
                            // ...receive data from hardware...                                      
                            Console.WriteLine(FRDemo.XL_GetFREventString(receivedEvent));
                            nbrEventsPrinted++;
                            if (nbrEventsPrinted > numberEventsToPrint)
                            {
                                break;
                            }
                        }

                    }
                }

                if (numberEventsToPrint != UInt32.MaxValue)
                {
                    if (printEvents)
                    {
                        if (nbrEventsPrinted > numberEventsToPrint)
                        {
                            Console.WriteLine("++ To reduce CPU load and make events readable only {0} events are printed at once. ++", numberEventsToPrint);
                            Console.WriteLine("++ Press any key for next block of events, 'U' for unlimited events, 'X' to exit ++");
                            printEvents = false;
                            nbrEventsPrinted = 0;
                        }
                    }
                }

            }//while
        }
        // ----------------------------------------------------------------------------------------------- 
    }
}
