//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by xlFlexDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_XLFLEXDEMO_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDD_TXFRAME                     130
#define IDC_DEVICE                      1000
#define IDC_ACTIVATE                    1001
#define IDC_MSGLIST                     1002
#define IDC_CLEAR                       1003
#define IDC_ID                          1004
#define IDC_REPETITION                  1006
#define IDC_OFFSET                      1007
#define IDC_ADDTXFRAME                  1008
#define IDC_SHOW                        1009
#define IDC_REPETITION2                 1009
#define IDC_TXMODE                      1009
#define ID_ABOUT                        1010
#define IDC_CHANNEL                     1011
#define IDC_COLDID                      1011
#define IDC_ERAYID                      1012
#define IDC_ERAYONLY                    1013
#define IDC_COLDCC                      1013
#define IDC_PAYLOADINC                  1014
#define IDC_DATA                        1015
#define IDC_TXAKN                       1016
#define IDC_ERAY                        1017
#define IDC_CHECK1                      1019
#define IDC_SPY                         1019
#define IDC_BTN_ADD_128FRAMES           1020
#define IDC_BTN_COPY                    1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
