//////////////////////////////////////////////////////////////////
// xlDAIOdemo.h : main header for DAIO demo dialog
//

#if !defined(AFX_XLDAIODEMO_H__8D1665F3_2B1E_401E_B6CB_A3603B456DED__INCLUDED_)
#define AFX_XLDAIODEMO_H__8D1665F3_2B1E_401E_B6CB_A3603B456DED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// Hauptsymbole

/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoApp:
// Siehe xlDAIOdemo.cpp f�r die Implementierung dieser Klasse
//

class CXlDAIOdemoApp : public CWinApp {
public:
	CXlDAIOdemoApp();

	// Vom Klassenassistenten generierte �berladungen virtueller Funktionen
	//{{AFX_VIRTUAL(CXlDAIOdemoApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CXlDAIOdemoApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}

#endif // !defined(AFX_XLDAIODEMO_H__8D1665F3_2B1E_401E_B6CB_A3603B456DED__INCLUDED_)
