// xlDAIOdemoDlg.cpp : Implementation
//

#include "stdafx.h"
#include "xlDAIOdemo.h"
#include "xlDAIOdemoDlg.h"
#include "vxlapi.h"

#ifdef _DEBUG
  #define new DEBUG_NEW
  #undef THIS_FILE
  static char THIS_FILE[] = __FILE__;
#endif


// *** GLOBAL definitions ***
#define           MESSAGECNT              100


BOOL              gChannelActivated  = FALSE;   // our config is valid and channel is activated
BOOL              gRXThreadRun      = FALSE;

unsigned int      gLastDigital       =     0;   // last value of digital inputs
unsigned int      gLastAnalog[4];               // last value of analog inputs @@@@
XLaccess          gChannelMask       =     0;   // mask of selected channel
XLportHandle      gPort;                        // global port handles 
HANDLE            gRxHandle=NULL;




/////////////////////////////////////////////////////////////////////////////
// ReceiveThread - background task receiving messages
unsigned long       gThreadID;
HANDLE              gThreadHandle = NULL;

DWORD WINAPI ReceiveThread (void *param) {
  UNREFERENCED_PARAMETER(param);
  unsigned int msgscnt, ret;
  XLevent msgs[MESSAGECNT];

  gRXThreadRun = TRUE;

  while (gRXThreadRun) {

    if (gChannelActivated) {

      if (gRxHandle) WaitForSingleObject(gRxHandle, 1); else Sleep(1);

      msgscnt = MESSAGECNT;
      if (xlReceive(gPort, &msgscnt, msgs) != XL_SUCCESS) continue;

      for (ret=0;ret<msgscnt;ret++){
        if (msgs[ret].tag == XL_RECEIVE_DAIO_DATA) {
          gLastDigital    = msgs[ret].tagData.daioData.value_digital;
          gLastAnalog[2]  = msgs[ret].tagData.daioData.value_analog[2];
          gLastAnalog[3]  = msgs[ret].tagData.daioData.value_analog[3];
        }
      }
    } else {        // suspend thread when measurement is not running
      Sleep(100);
    }
  }
  return 0;
}



/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoDlg DialogBox

CXlDAIOdemoDlg::CXlDAIOdemoDlg(CWnd* pParent)	: CDialog(CXlDAIOdemoDlg::IDD, pParent) {
	//{{AFX_DATA_INIT(CXlDAIOdemoDlg)
	m_Analog1               = 0;
	m_Analog2               = 0;
	m_Analog3               = 0;
	m_Analog4               = 0;
	m_MeasurementFrequency  = 0;
	m_PwmValue              = 0;
	m_PwmFrequency          = 0;
	m_SelectedChannelName   = _T("No channel activated");
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CXlDAIOdemoDlg::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CXlDAIOdemoDlg)
	DDX_Control(pDX, IDC_EDIT4, m_Analog4Ctrl);
	DDX_Control(pDX, IDC_EDIT3, m_Analog3Ctrl);
	DDX_Control(pDX, IDC_EDIT2, m_Analog2Ctrl);
	DDX_Control(pDX, IDC_EDIT1, m_Analog1Ctrl);
	DDX_Control(pDX, IDC_LIST2, m_Screen);
	DDX_Control(pDX, IDC_SLIDER1, m_PwmValueCtrl);
	DDX_Control(pDX, IDC_CHECK4, m_DigitalOut4);
	DDX_Control(pDX, IDC_CHECK3, m_DigitalOut3);
	DDX_Control(pDX, IDC_CHECK2, m_DigitalOut2);
	DDX_Control(pDX, IDC_CHECK1, m_DigitalOut1);
	DDX_Control(pDX, IDC_LIST1, m_DigitalStateList);
	DDX_Control(pDX, IDC_BUTTON2, m_StopCtrl);
	DDX_Control(pDX, IDC_BUTTON1, m_StartCtrl);
	DDX_Text(pDX, IDC_EDIT1, m_Analog1);
	DDX_Text(pDX, IDC_EDIT2, m_Analog2);
	DDX_Text(pDX, IDC_EDIT3, m_Analog3);
	DDX_Text(pDX, IDC_EDIT4, m_Analog4);
	DDX_CBIndex(pDX, IDC_COMBO1, m_MeasurementFrequency);
	DDX_Slider(pDX, IDC_SLIDER1, m_PwmValue);
	DDX_CBIndex(pDX, IDC_COMBO2, m_PwmFrequency);
	DDX_Text(pDX, IDC_SELECTEDCHANNELNAME, m_SelectedChannelName);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CXlDAIOdemoDlg, CDialog)
	//{{AFX_MSG_MAP(CXlDAIOdemoDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnStart)
	ON_BN_CLICKED(IDC_BUTTON2, OnStop)
	ON_BN_CLICKED(IDC_BUTTON3, OnSetAnalog)
	ON_BN_CLICKED(IDC_CHECK1, OnSetDigital1)
	ON_BN_CLICKED(IDC_CHECK2, OnSetDigital2)
	ON_BN_CLICKED(IDC_CHECK3, OnSetDigital3)
	ON_BN_CLICKED(IDC_CHECK4, OnSetDigital4)
	ON_BN_CLICKED(IDC_BUTTON4, OnMeasureNow)
	ON_BN_CLICKED(IDC_BUTTON5, OnSetConfiguration)
	ON_WM_TIMER()
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER1, OnReleasedcaptureSlider1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoDlg Message Handler

BOOL CXlDAIOdemoDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

  // Start background task, and make sure it is running
  gThreadHandle = CreateThread(NULL, 0, ReceiveThread, 0, 0, &gThreadID);
  if (gThreadHandle == NULL) {
    MessageBox("Error: Cannot start receive thread.");
    exit(0);
  }
  // we will set priority to low, to avoid problems with UI when lot of messages are received
  SetThreadPriority(gThreadHandle, THREAD_PRIORITY_BELOW_NORMAL);
	
  // then we have to setup all dialog controls
  m_PwmValueCtrl.SetRange (0, 10000, TRUE); // PWM is in percent value with two decimal places precision
  m_PwmValueCtrl.SetTicFreq (1000);

  SetTimer(NULL, 250, NULL);                // default refresh timer
  m_StopCtrl.EnableWindow(FALSE);           // channel is not activated


	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoDlg OnPaint Function
void CXlDAIOdemoDlg::OnPaint() {
	if (IsIconic())	{
		CPaintDC dc(this); 

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		dc.DrawIcon(x, y, m_hIcon);
	}	else	{
		CDialog::OnPaint();
	}
}

HCURSOR CXlDAIOdemoDlg::OnQueryDragIcon() {
	return (HCURSOR) m_hIcon;
}

/////////////////////////////////////////////////////////////////////////////
// OnCancel - close application correctly
void CXlDAIOdemoDlg::OnCancel() {
  OnStop();
  if (gThreadHandle) {
    if (TerminateProcess (gThreadHandle, 0))
      CloseHandle( gThreadHandle );
  }
	
	CDialog::OnCancel();
}

/////////////////////////////////////////////////////////////////////////////
// AddMessage - adds message to output window and scrolls it
void CXlDAIOdemoDlg::AddMessage(CString msg) {
  m_Screen.InsertString(-1,msg);
  m_Screen.SetTopIndex(m_Screen.GetCount( )-1);
}


////////////// FUNCTIONALITY IMPLEMENTATION //////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// OnTimer - on periodic timer (default 500ms we will update measured values)
void CXlDAIOdemoDlg::OnTimer(UINT nIDEvent) {
  UNREFERENCED_PARAMETER(nIDEvent);
  unsigned int pos;
  CString      txt;
  if (!gChannelActivated) return;

  if (m_DigitalStateList.GetCount() != 8) { // @@@@ // we have wrong content in our output list
    m_DigitalStateList.ResetContent();
    for (pos = 0; pos < 8; pos++) {
      txt.Format ("D %d\tNo data", pos );
      m_DigitalStateList.InsertString(pos, txt);
    }
  }


  // update digital input values
  for (pos = 0; pos < 8; pos++) {
    txt.Format ("D %d\t%s", pos, ((1 << pos) & gLastDigital)? "High":"Low" );
    m_DigitalStateList.DeleteString(pos);
    m_DigitalStateList.InsertString(pos, txt);
  }

  txt.Format("%d", gLastAnalog [2]);
  m_Analog3Ctrl.SetWindowText(txt);
  txt.Format("%d", gLastAnalog [3]);
  m_Analog4Ctrl.SetWindowText(txt);
  
}


/////////////////////////////////////////////////////////////////////////////
// OnFirstStart - function detects if there is valid configuration, when not tries to update it
// returns false when was already defined
BOOL CXlDAIOdemoDlg::OnFirstStart() {
  unsigned int hwType, hwIndex, hwChannel, pos;
  XLdriverConfig cfg;

  // could we found valid configuration?
  if (xlGetApplConfig ("xlDAIOdemo", 0, &hwType, &hwIndex, &hwChannel, XL_BUS_TYPE_DAIO) == XL_SUCCESS) return FALSE;

  hwType    = 0;
  hwIndex   = 0;
  hwChannel = 0;

  // there was not valid configuration, so we will check present hardware
  // and use first hardware channel capable to DAIObus or first hardware channel with IOcab inserted

  if (xlGetDriverConfig (&cfg) != XL_SUCCESS) return FALSE;

  for (pos = 0; pos < cfg.channelCount; pos++) {

    // if we have no hardware yet - get first capable
    if (!hwType && (cfg.channel[pos].channelBusCapabilities & XL_BUS_COMPATIBLE_DAIO)) {
      hwType    = cfg.channel[pos].hwType;
      hwIndex   = cfg.channel[pos].hwIndex;
      hwChannel = cfg.channel[pos].hwChannel;
    }

    // if there is IOcab inserted, than take this one and leave
    if (cfg.channel[pos].channelBusCapabilities & XL_BUS_ACTIVE_CAP_DAIO) {
      hwType    = cfg.channel[pos].hwType;
      hwIndex   = cfg.channel[pos].hwIndex;
      hwChannel = cfg.channel[pos].hwChannel;
      xlSetApplConfig ("xlDAIOdemo", 0, hwType, hwIndex, hwChannel, XL_BUS_TYPE_DAIO);
      return TRUE;
    }
  }

  // if we have some change, update it
  if (hwType) {
    xlSetApplConfig ("xlDAIOdemo", 0, hwType, hwIndex, hwChannel, XL_BUS_TYPE_DAIO);
    return TRUE;
  }
  return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// OnStart - starts the channel with DAIO bus
void CXlDAIOdemoDlg::OnStart() {
  unsigned int hwType, hwIndex, hwChannel, pos;
  XLaccess     initAccess;
  XLdriverConfig cfg;

  if (gChannelActivated) return;

  xlOpenDriver();

  if (OnFirstStart()) {
    xlPopupHwConfig (NULL, 0);
    MessageBox ("Configuration was updated, please check the setting of \"DAIOdemo\" application channel.\n Press OK to proceed."); 
  }

  // now we should get correct application config
  if (xlGetApplConfig ("xlDAIOdemo",          // name of application's channel
                       0,                   // zero based application channel
                       &hwType,             // received type of hardware
                       &hwIndex,            // received hardware index
                       &hwChannel,          // received hardware channel
                       XL_BUS_TYPE_DAIO) 
                       != XL_SUCCESS) {
    AddMessage ("Error: xlGetApplConfig failed.");
    return;
  }

  // retrieve mask for selected channel
  gChannelMask = xlGetChannelMask ((int)hwType, (int)hwIndex, (int)hwChannel);
  if (!gChannelMask){
    AddMessage ("Error: xlGetChannelMask failed.");
    return;
  }

  initAccess = gChannelMask;

  // now we are going to open port for the application
  if (xlOpenPort ( &gPort,                // pointer to port handle
                   "xlDAIOdemo",            // application name
                   gChannelMask,          // selected channel mask
                   &initAccess,           // init access mask request / response
                   1024,                  // receive queue size
                   XL_INTERFACE_VERSION,  // desired xl interface version
                   XL_BUS_TYPE_DAIO)      // we are opening DAIO bus
                   != XL_SUCCESS) {
    AddMessage ("Error: xlOpenPort failed.");
    return;
  }

  // if we have not init access, we should fail here, because than we can only read DAIOdata
  if (gChannelMask != initAccess) {
    AddMessage ("Error: Application cannot get init access.");
    goto lerror;  // from now we should fail safely, because we could have opened port and driver
  }
  
  if (xlSetNotification (gPort, &gRxHandle, 1 ) != XL_SUCCESS) {
    AddMessage ("Error: xlSetNotification failed.");
    goto lerror;
  }

  // we can call configuration here to make sure that after activate channel we will have all settings done
  OnSetConfiguration();

  // following step is going to activate channel and stay prepared for configuration
  if (xlActivateChannel (gPort,                 // application port
                         initAccess,           // desired access mask
                         XL_BUS_TYPE_DAIO,      // we are activating DAIO bus
                         XL_ACTIVATE_RESET_CLOCK)  // timestamp should start from zero
                         != XL_SUCCESS) {
    AddMessage ("Error: xlActivateChannel failed.");
    goto lerror;
  }

  // if we have not init access, we should fail here, because than we can only read DAIOdata
  if (gChannelMask != initAccess) {
    AddMessage ("Error: Application cannot get init access for activation.");
    goto lerror;
  }

  // now we are ready with driver configuration, we have opened channel with DAIO configuration
  // and when bus configuration is updated, we can start measurement

  if (xlGetDriverConfig(&cfg) != XL_SUCCESS) {
    AddMessage ("Error: xlGetDriverConfig failed.");
    goto lerror;
  }

  // now we should display name of the channel and serial number of device we selected
  for (pos = 0; pos < cfg.channelCount; pos++) {
    if (cfg.channel[pos].channelMask == gChannelMask){
      m_SelectedChannelName.Format("%s (s.n. %d)", cfg.channel[pos].name, cfg.channel[pos].serialNumber);
    }
  }
  
  // first reset all values to starting state
  m_DigitalStateList.ResetContent();
  m_DigitalOut1.SetCheck(0);
  m_DigitalOut2.SetCheck(0);
  m_DigitalOut3.SetCheck(0);
  m_DigitalOut4.SetCheck(0);
  m_Analog1 = 0;
  m_Analog2 = 0;
  m_Analog3 = 0;
  m_Analog4 = 0;
  gLastDigital = 0;
  memset(gLastAnalog, 0, sizeof(gLastAnalog));

  m_StartCtrl.EnableWindow (FALSE);
  m_StopCtrl.EnableWindow (TRUE);

  UpdateData(FALSE);
  gChannelActivated = TRUE;
  return;
lerror:
  xlClosePort(gPort);
  xlCloseDriver();

}

/////////////////////////////////////////////////////////////////////////////
// OnStart - stops the channel with DAIO bus
void CXlDAIOdemoDlg::OnStop() {
  if (!gChannelActivated) return;

  gRXThreadRun = FALSE;

  WaitForSingleObject(gThreadHandle, 5000);

  // close driver configuration and free resources to other applications

  xlDeactivateChannel(gPort, gChannelMask);

  xlClosePort(gPort);
  
  xlCloseDriver();    

  gChannelActivated = FALSE;
  m_SelectedChannelName = "No channel activated";
  m_StopCtrl.EnableWindow (FALSE);
  m_StartCtrl.EnableWindow (TRUE);
  UpdateData(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// OnSetConfiguration - function sends current configuration to the device
void CXlDAIOdemoDlg::OnSetConfiguration() {
  unsigned int measure_delay = 0;
  UpdateData(TRUE);

  switch (m_MeasurementFrequency) {
    case 1: measure_delay = 1000;  //    1 Hz
      break;
    case 2: measure_delay =  100;  //   10 Hz
      break;
    case 3: measure_delay =   10;  //  100 Hz
      break;
    case 4: measure_delay =    1;  // 1000 Hz
      break;
  }

  if (xlDAIOSetMeasurementFrequency ( gPort, 
                                      gChannelMask,
                                      measure_delay)
                                      != XL_SUCCESS) {
    AddMessage ("Error: xlDAIOSetMeasurementFrequency failed.");
    return;
  }

  // now we will setup digital channels to output, values we can read anyway
  if (xlDAIOSetDigitalParameters (gPort, gChannelMask, 0x00, 0xff) != XL_SUCCESS) {
    AddMessage ("Error: xlDAIOSetDigitalParameters failed.");
    return;
  }

  // we will use two analog outputs (port 1 and 2) and two analog inputs
  // for all of them input range to 32V
  if (xlDAIOSetAnalogParameters (gPort, gChannelMask, 0x0c, 0x03, 0x0f) != XL_SUCCESS) {
    AddMessage ("Error: xlDAIOSetAnalogParameters failed.");
    return;
  }

  AddMessage ("Configuration was set successfully.");
}


/////////////////////////////////////////////////////////////////////////////
// OnSetAnalog - updates analog outputs
void CXlDAIOdemoDlg::OnSetAnalog() {
  UpdateData(TRUE);

  if (!gChannelActivated) {
    AddMessage ("Warning! OnSetAnalog: channel is not activated.");
    return;
  }

  if (xlDAIOSetAnalogOutput (gPort, gChannelMask, m_Analog1, m_Analog2, m_Analog3, m_Analog4) != XL_SUCCESS) {
    AddMessage ("Error: OnSetAnalog: xlDAIOSetAnalogOutput failed.");
  }

}

/////////////////////////////////////////////////////////////////////////////
// OnSetDigital - updates digital outputs
void CXlDAIOdemoDlg::OnSetDigital1() {
  unsigned int pattern = 0;
  UpdateData(TRUE);

  if (!gChannelActivated) {
    AddMessage ("Warning! OnSetDigital1: channel is not activated.");
    return;
  }

  if (m_DigitalOut1.GetCheck()) pattern = (1 << 0);

  if (xlDAIOSetDigitalOutput (gPort, gChannelMask, 0x1, pattern) != XL_SUCCESS) {
    AddMessage ("Error: OnSetDigital1: xlDAIOSetDigitalOutput failed.");
  }
}

void CXlDAIOdemoDlg::OnSetDigital2() {
  unsigned int pattern = 0;
  UpdateData(TRUE);

  if (!gChannelActivated) {
    AddMessage ("Warning! OnSetDigital2: channel is not activated.");
    return;
  }

  if (m_DigitalOut2.GetCheck()) pattern = (1 << 1);

  if (xlDAIOSetDigitalOutput (gPort, gChannelMask, 0x2, pattern) != XL_SUCCESS) {
    AddMessage ("Error: OnSetDigital2: xlDAIOSetDigitalOutput failed.");
  }
}

void CXlDAIOdemoDlg::OnSetDigital3() {
  unsigned int pattern = 0;
  UpdateData(TRUE);

  if (!gChannelActivated) {
    AddMessage ("Warning! OnSetDigital3: channel is not activated.");
    return;
  }

  if (m_DigitalOut3.GetCheck()) pattern = (1 << 2);

  if (xlDAIOSetDigitalOutput (gPort, gChannelMask, 0x4, pattern) != XL_SUCCESS) {
    AddMessage ("Error: OnSetDigital3: xlDAIOSetDigitalOutput failed.");
  }
}

void CXlDAIOdemoDlg::OnSetDigital4() {
  unsigned int pattern = 0;
  UpdateData(TRUE);

  if (!gChannelActivated) {
    AddMessage ("Warning! OnSetDigital4: channel is not activated.");
    return;
  }

  if (m_DigitalOut4.GetCheck()) pattern = (1 << 3);

  if (xlDAIOSetDigitalOutput (gPort, gChannelMask, 0x8, pattern) != XL_SUCCESS) {
    AddMessage ("Error: OnSetDigital4: xlDAIOSetDigitalOutput failed.");
  }
}

/////////////////////////////////////////////////////////////////////////////
// OnMeasureNow - forces manual measurement 
void CXlDAIOdemoDlg::OnMeasureNow() {
  UpdateData(TRUE);
  if (!gChannelActivated) {
    AddMessage ("Warning! OnMeasureNow: channel is not activated.");
    return;
  }

  if (xlDAIORequestMeasurement (gPort, gChannelMask) != XL_SUCCESS) {
    AddMessage ("Error: OnMeasureNow: xlDAIORequestMeasurement failed.");
  }
}


/////////////////////////////////////////////////////////////////////////////
// OnReleasedcaptureSlider1 - user updated pwm setting
void CXlDAIOdemoDlg::OnReleasedcaptureSlider1(NMHDR* pNMHDR, LRESULT* pResult) {
  UNREFERENCED_PARAMETER(pNMHDR);
  unsigned int frequency = 0;
	*pResult = 0;
  UpdateData(TRUE);
  if (!gChannelActivated) {
    AddMessage ("Warning! OnReleasedcaptureSlider1: channel is not activated.");
    return;
  }

  //if (m_PwmFrequency == 0) return; // pwm is disabled

  if (m_PwmFrequency == 1) frequency = 100;
  if (m_PwmFrequency == 2) frequency = 5000;
  if (m_PwmFrequency == 3) frequency = 20000;
  if (m_PwmFrequency == 4) frequency = 40000;

  if (xlDAIOSetPWMOutput (gPort, gChannelMask, frequency, m_PwmValue) != XL_SUCCESS) {
    AddMessage ("Error: OnMeasureNow: xlDAIORequestMeasurement failed.");
  }
}
