// xlDAIOdemo.cpp : Legt das Klassenverhalten f�r die Anwendung fest.
//

#include "stdafx.h"
#include "xlDAIOdemo.h"
#include "xlDAIOdemoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoApp

BEGIN_MESSAGE_MAP(CXlDAIOdemoApp, CWinApp)
	//{{AFX_MSG_MAP(CXlDAIOdemoApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoApp Konstruktion

CXlDAIOdemoApp::CXlDAIOdemoApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// Das einzige CXlDAIOdemoApp-Objekt

CXlDAIOdemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoApp Initialisierung

BOOL CXlDAIOdemoApp::InitInstance()
{
	// Standardinitialisierung

	CXlDAIOdemoDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Da das Dialogfeld geschlossen wurde, FALSE zur�ckliefern, so dass wir die
	//  Anwendung verlassen, anstatt das Nachrichtensystem der Anwendung zu starten.
	return FALSE;
}
