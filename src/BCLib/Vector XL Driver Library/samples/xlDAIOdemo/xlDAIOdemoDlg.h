// xlDAIOdemoDlg.h : Header-Datei
//

#if !defined(AFX_XLDAIODEMODLG_H__39BA5CE2_86F1_4322_8F90_D2A612F7354B__INCLUDED_)
#define AFX_XLDAIODEMODLG_H__39BA5CE2_86F1_4322_8F90_D2A612F7354B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CXlDAIOdemoDlg Dialogfeld

class CXlDAIOdemoDlg : public CDialog
{
// Konstruktion
public:
	CXlDAIOdemoDlg(CWnd* pParent = NULL);	// Standard-constructor

  BOOL OnFirstStart ();             // when application is started first 
  void AddMessage   (CString msg);   // adds message to output window

// Dialogfelddaten
	//{{AFX_DATA(CXlDAIOdemoDlg)
	enum { IDD = IDD_XLDAIODEMO_DIALOG };
	CEdit	m_Analog4Ctrl;
	CEdit	m_Analog3Ctrl;
	CEdit	m_Analog2Ctrl;
	CEdit	m_Analog1Ctrl;
	CListBox	m_Screen;
	CSliderCtrl	m_PwmValueCtrl;
	CButton	m_DigitalOut4;
	CButton	m_DigitalOut3;
	CButton	m_DigitalOut2;
	CButton	m_DigitalOut1;
	CListBox	m_DigitalStateList;
	CButton	m_StopCtrl;
	CButton	m_StartCtrl;
	UINT	m_Analog1;
	UINT	m_Analog2;
	UINT	m_Analog3;
	UINT	m_Analog4;
	int		m_MeasurementFrequency;
	int		m_PwmValue;
	int		m_PwmFrequency;
	CString	m_SelectedChannelName;
	//}}AFX_DATA

	// Vom Klassenassistenten generierte Überladungen virtueller Funktionen
	//{{AFX_VIRTUAL(CXlDAIOdemoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Message-Map-Funktionen
	//{{AFX_MSG(CXlDAIOdemoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnStart();
	afx_msg void OnStop();
	virtual void OnCancel();
	afx_msg void OnSetAnalog();
	afx_msg void OnSetDigital1();
	afx_msg void OnSetDigital2();
	afx_msg void OnSetDigital3();
	afx_msg void OnSetDigital4();
	afx_msg void OnMeasureNow();
	afx_msg void OnSetConfiguration();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnReleasedcaptureSlider1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // !defined(AFX_XLDAIODEMODLG_H__39BA5CE2_86F1_4322_8F90_D2A612F7354B__INCLUDED_)
