--------------------------------------------------------------------------------

             xlDAIOdemo - Demoprogram for the 'XL Family Driver Library'


                      Vector Informatik GmbH, Stuttgart

--------------------------------------------------------------------------------

Vector Informatik GmbH
Ingersheimer Stra�e 24
D-70499 Stuttgart

Tel.:  0711-80670-200
Fax.:  0711-80670-555
EMAIL: support@vector-informatik.de
WEB:   www.vector-informatik.de

--------------------------------------------------------------------------------

xlDAIOdemo is a small test application for the IOcab functionality on 

- CANcardXL (and the IOcab8444opto)

For further information look into the 'XL Driver Library - Description.pdf' 
document.
