/*-------------------------------------------------------------------------------------------
| File        : FibexReader.h
| Project     : Vector FlexRay Command Line Example
|
| Description : Fibex Converter for Vector XL FlexRay API
|--------------------------------------------------------------------------------------------
| File History:
| Date        Version        Changes
| -----------+--------------+--------------
| 2009-12-17  V1.0.0         Initial version
| 2012-02-23  V1.1.0         Fixed handling of parameters pKeySlotUsedForStartup, pKeySlotUsedForSync, pChannels
|
|--------------------------------------------------------------------------------------------
| $Author: vismra $    $Date: 2012-02-24 06:58:38 +0100 (Fr, 24 Feb 2012) $   $Revision: 15324 $
| $Id: FibexReader.h 15324 2012-02-24 05:58:38Z vismra $
|--------------------------------------------------------------------------------------------
| Copyright (c) 2012 by Vector Informatik GmbH.  All rights reserved.
|------------------------------------------------------------------------------------------*/

#ifndef _FIBEX_READER_H_
#define _FIBEX_READER_H_

// ---------------------------------------------------------------------------------------------------------------
// INCLUDES
// ---------------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#import <msxml4.dll> // Should be installed in Windows\System32 folder. Version 4 should be used.

#include "vxlapi.h" 
// ---------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------
// PROTOTYPES
// ---------------------------------------------------------------------------------------------------------------
XLstatus FbxRd_LoadFibexConfig(const char* xmlFileName, XLfrClusterConfig &clusterConf);
// ---------------------------------------------------------------------------------------------------------------

#endif //_FIBEX_READER_H_
