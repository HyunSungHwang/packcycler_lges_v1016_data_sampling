/*-------------------------------------------------------------------------------------------
| File        : FibexReader.cpp
| Project     : Vector FlexRay Command Line Example
|
| Description : Fibex Converter for Vector XL FlexRay API
|--------------------------------------------------------------------------------------------
| File History:
| Date        Version        Changes
| -----------+--------------+--------------
| 2009-12-17  V1.0.0         Initial version
| 2012-02-23  V1.1.0         Fixed handling of parameters pKeySlotUsedForStartup, pKeySlotUsedForSync, pChannels
|
|--------------------------------------------------------------------------------------------
| $Author: visan $    $Date: 2013-01-04 14:34:05 +0100 (Fr, 04 Jan 2013) $   $Revision: 21108 $
| $Id: FibexReader.cpp 21108 2013-01-04 13:34:05Z visan $
|--------------------------------------------------------------------------------------------
| Copyright (c) 2012 by Vector Informatik GmbH.  All rights reserved.
|------------------------------------------------------------------------------------------*/


// ---------------------------------------------------------------------------------------------------------------
// INCLUDES
// ---------------------------------------------------------------------------------------------------------------
#include "FibexReader.h"
// ---------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------
// Function     : LoadFibexConfig
// Description  : Reads the Fibex configuration and returns a full XLfrClusterConfig
//
//   NOTE: - THIS EXAMPLE SUPPORTS FLEXRAY FIBEX VERSION 2.0.1 FILES.
//           Other Flexray Fibex versions may or may not work.
//           Fibex 3.x.x versions doesn't work!
//         - The used Fibex file must contain the definitions of one node only. If several nodes are present
//           in the file, the result will be undefined - in most cases the values of the last node in the
//           Fibex will be used.
//         - The used Fibex file must contain the definitions of one cluster only.
//
// Parameter fileName    [IN] : Path and name of Fibex file to load
// Parameter clusterConf [OUT]: Reference to XLAPI Flexray cluster configuration structure.
// Return Value : XL_SUCCESS = success, XL_ERROR = unhandled error, XL_ERR_WRONG_PARAMETER = fibex not found
// ---------------------------------------------------------------------------------------------------------------
XLstatus FbxRd_LoadFibexConfig(const char* xmlFileName, XLfrClusterConfig &clusterConf) {
  _bstr_t value = "";
  _bstr_t fileName(xmlFileName);
  CoInitialize(NULL);

  try	{
    // Open XML file
    MSXML2::IXMLDOMDocument2Ptr pXMLDoc;
    HRESULT hr = pXMLDoc.CreateInstance(__uuidof(DOMDocument));			
    hr = pXMLDoc->load(fileName);
    if (hr!=VARIANT_TRUE) {
      return XL_ERR_WRONG_PARAMETER;
    }

    // Get Element list
    MSXML2::IXMLDOMNodeListPtr pNodeList;
    MSXML2::IXMLDOMNodePtr clusterNode;

    // Read cluster section of the config
    pNodeList = pXMLDoc->documentElement->selectNodes("//fx:CLUSTER");
    while((clusterNode = pNodeList->nextNode()) != NULL) {
      value = clusterNode->selectSingleNode("./fx:SPEED/text()")->text;
      clusterConf.baudrate = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:COLD-START-ATTEMPTS/text()")->text;
      clusterConf.gColdStartAttempts = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:LISTEN-NOISE/text()")->text;
      clusterConf.gListenNoise = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MACRO-PER-CYCLE/text()")->text;
      clusterConf.gMacroPerCycle = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MAX-WITHOUT-CLOCK-CORRECTION-FATAL/text()")->text;
      clusterConf.gMaxWithoutClockCorrectionFatal = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MAX-WITHOUT-CLOCK-CORRECTION-PASSIVE/text()")->text;
      clusterConf.gMaxWithoutClockCorrectionPassive = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:NETWORK-MANAGEMENT-VECTOR-LENGTH/text()")->text;
      clusterConf.gNetworkManagementVectorLength = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:NUMBER-OF-MINISLOTS/text()")->text;
      clusterConf.gNumberOfMinislots = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:NUMBER-OF-STATIC-SLOTS/text()")->text;
      clusterConf.gNumberOfStaticSlots = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:OFFSET-CORRECTION-START/text()")->text;
      clusterConf.gOffsetCorrectionStart = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:PAYLOAD-LENGTH-STATIC/text()")->text;
      clusterConf.gPayloadLengthStatic = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:SYNC-NODE-MAX/text()")->text;
      clusterConf.gSyncNodeMax = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:ACTION-POINT-OFFSET/text()")->text;
      clusterConf.gdActionPointOffset = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:DYNAMIC-SLOT-IDLE-PHASE/text()")->text;
      clusterConf.gdDynamicSlotIdlePhase = atoi(value);

      clusterConf.gdMacrotick = 0;            // NOT USED BY XLAPI

      value = clusterNode->selectSingleNode("./flexray:MINISLOT/text()")->text;
      clusterConf.gdMinislot = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MINISLOT-ACTION-POINT-OFFSET/text()")->text;
      clusterConf.gdMiniSlotActionPointOffset = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:N-I-T/text()")->text;
      clusterConf.gdNIT = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:STATIC-SLOT/text()")->text;
      clusterConf.gdStaticSlot= atoi(value);

      value = clusterNode->selectSingleNode("./flexray:SYMBOL-WINDOW/text()")->text;
      clusterConf.gdSymbolWindow = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:T-S-S-TRANSMITTER/text()")->text;
      clusterConf.gdTSSTransmitter = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:WAKE-UP/flexray:WAKE-UP-SYMBOL-RX-IDLE/text()")->text;
      clusterConf.gdWakeupSymbolRxIdle = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:WAKE-UP/flexray:WAKE-UP-SYMBOL-RX-LOW/text()")->text;
      clusterConf.gdWakeupSymbolRxLow = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:WAKE-UP/flexray:WAKE-UP-SYMBOL-RX-WINDOW/text()")->text;
      clusterConf.gdWakeupSymbolRxWindow = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:WAKE-UP/flexray:WAKE-UP-SYMBOL-TX-IDLE/text()")->text;
      clusterConf.gdWakeupSymbolTxIdle = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:WAKE-UP/flexray:WAKE-UP-SYMBOL-TX-LOW/text()")->text;
      clusterConf.gdWakeupSymbolTxLow = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:CAS-RX-LOW-MAX/text()")->text;
      clusterConf.gdCASRxLowMax = atoi(value);
    }

    // Read controller section of the config
    pNodeList = pXMLDoc->documentElement->selectNodes("//fx:CONTROLLER");
    while((clusterNode = pNodeList->nextNode()) != NULL) {
      value = clusterNode->selectSingleNode("./flexray:CLUSTER-DRIFT-DAMPING/text()")->text;
      clusterConf.pClusterDriftDamping = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:DECODING-CORRECTION/text()")->text;
      clusterConf.pDecodingCorrection = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:DELAY-COMPENSATION-A/text()")->text;
      clusterConf.pDelayCompensationA = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:DELAY-COMPENSATION-B/text()")->text;
      clusterConf.pDelayCompensationB = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:EXTERN-OFFSET-CORRECTION/text()")->text;
      clusterConf.pExternOffsetCorrection = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:EXTERN-RATE-CORRECTION/text()")->text;
      clusterConf.pExternRateCorrection = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:LATEST-TX/text()")->text;
      clusterConf.pLatestTx = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MACRO-INITIAL-OFFSET-A/text()")->text;
      clusterConf.pMacroInitialOffsetA = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MACRO-INITIAL-OFFSET-B/text()")->text;
      clusterConf.pMacroInitialOffsetB = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MICRO-INITIAL-OFFSET-A/text()")->text;
      clusterConf.pMicroInitialOffsetA = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MICRO-INITIAL-OFFSET-B/text()")->text;
      clusterConf.pMicroInitialOffsetB = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MICRO-PER-CYCLE/text()")->text;
      clusterConf.pMicroPerCycle = atoi(value);

      clusterConf.pMicroPerMacroNom =  0;     // NOT USED BY XLAPI

      value = clusterNode->selectSingleNode("./flexray:OFFSET-CORRECTION-OUT/text()")->text;
      clusterConf.pOffsetCorrectionOut = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:RATE-CORRECTION-OUT/text()")->text;
      clusterConf.pRateCorrectionOut = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:SAMPLES-PER-MICROTICK/text()")->text;
      clusterConf.pSamplesPerMicrotick = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:SINGLE-SLOT-ENABLED/text()")->text;
      if (strcmp(value, "false") == 0) { clusterConf.pSingleSlotEnabled = 0; }
      else { clusterConf.pSingleSlotEnabled = 1; }

      value = clusterNode->selectSingleNode("./flexray:WAKE-UP-PATTERN/text()")->text;
      clusterConf.pWakeupPattern = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:ALLOW-HALT-DUE-TO-CLOCK/text()")->text;
      if (strcmp(value, "false") == 0) { clusterConf.pAllowHaltDueToClock= 0; }
      else { clusterConf.pAllowHaltDueToClock = 1; }

      value = clusterNode->selectSingleNode("./flexray:ALLOW-PASSIVE-TO-ACTIVE/text()")->text;
      clusterConf.pAllowPassiveToActive = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:ACCEPTED-STARTUP-RANGE/text()")->text;
      clusterConf.pdAcceptedStartupRange = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:LISTEN-TIMEOUT/text()")->text;
      clusterConf.pdListenTimeout = atoi(value);

      value = clusterNode->selectSingleNode("./flexray:MAX-DRIFT/text()")->text;
      clusterConf.pdMaxDrift = atoi(value);

      clusterConf.pdMicrotick = 0;            // NOT USED BY XLAPI
      clusterConf.gChannels = 0;              // NOT USED BY XLAPI
      clusterConf.vExternOffsetControl = 0;   // NOT USED BY XLAPI
      clusterConf.vExternRateControl = 0;     // NOT USED BY XLAPI

      //value = clusterNode->selectSingleNode("./flexray:KEY-SLOT-USAGE/flexray:STARTUP-SYNC/text()")->text;
      //clusterConf.pKeySlotUsedForStartup = atoi(value);
      //clusterConf.pKeySlotUsedForSync = atoi(value);
      clusterConf.pKeySlotUsedForStartup = 0; // NOT USED BY XLAPI
      clusterConf.pKeySlotUsedForSync = 0;    // NOT USED BY XLAPI

      value = clusterNode->selectSingleNode("./flexray:MAX-DYNAMIC-PAYLOAD-LENGTH/text()")->text;
      clusterConf.pMaxPayloadLengthDynamic = atoi(value);

      // Read Controller Config
      clusterConf.pChannels = 0;
      pNodeList = pXMLDoc->documentElement->selectNodes("//fx:CHANNEL");
      while((clusterNode = pNodeList->nextNode()) != NULL)
      {
        value = clusterNode->selectSingleNode("./ho:SHORT-NAME/text()")->text;
        if (strcmp(value, "Channel_A") == 0) { clusterConf.pChannels |= 1; }
        else if (strcmp(value, "Channel_B") == 0) { clusterConf.pChannels |= 2;}
      }
    }
  }
  catch(...) {
    // Unhandled error
    return XL_ERROR;
  }
  CoUninitialize();

  // Success
  return XL_SUCCESS;
} //FbxRd_LoadFibexConfig()
