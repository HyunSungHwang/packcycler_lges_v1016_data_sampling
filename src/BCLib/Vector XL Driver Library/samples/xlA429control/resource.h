//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by xlA429control.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_XLA429CONTROL_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDB_XL                          133
#define IDC_OUTPUT                      1000
#define IDC_SEND                        1001
#define IDC_TX_PARITY                   1002
#define IDC_ONBUS                       1003
#define IDC_OFFBUS                      1004
#define IDC_MSG_FLAGS                   1005
#define IDC_MSG_PARITY                  1006
#define IDC_DATA                        1008
#define IDC_LABEL                       1015
#define IDC_HARDWARE                    1017
#define IDC_TX_MINGAP                   1018
#define IDC_CLEAR                       1019
#define IDC_ABOUT                       1020
#define IDC_TX_BITRATE                  1021
#define IDC_MSG_CYCLE_TIME              1022
#define IDC_MSG_GAP                     1023
#define IDC_LIC_INFO                    1026
#define IDC_RX_PARITY                   1027
#define IDC_RX_MINGAP                   1028
#define IDC_RX_AUTOBAUDRATE             1029
#define IDC_RX_BITRATE                  1030

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
