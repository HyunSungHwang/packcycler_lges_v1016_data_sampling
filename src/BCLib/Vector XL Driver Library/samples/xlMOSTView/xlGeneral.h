/*-------------------------------------------------------------------------------------------
| File        : xlGeneral.h
| Project     : Vector MOST Example 
|
| Description : 
|--------------------------------------------------------------------------------------------
| $Author: vishsh $    $Locker: $   $Revision: 36678 $
|--------------------------------------------------------------------------------------------
| Copyright (c) 2005 by Vector Informatik GmbH.  All rights reserved.
|------------------------------------------------------------------------------------------*/


#pragma once

class CGeneral
{
public:
  CGeneral(void);
  ~CGeneral(void);
   unsigned short StrToShort(char *str); 
};
