// xlMOSTView.h : Hauptheaderdatei f�r die xlMOSTView-Anwendung
//

#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"		// Hauptsymbole


// CxlMOSTViewApp:
// Siehe xlMOSTView.cpp f�r die Implementierung dieser Klasse
//

class CxlMOSTViewApp : public CWinApp
{
public:
	CxlMOSTViewApp();

// �berschreibungen
	public:
	virtual BOOL InitInstance();

// Implementierung

	DECLARE_MESSAGE_MAP()
};

extern CxlMOSTViewApp theApp;
