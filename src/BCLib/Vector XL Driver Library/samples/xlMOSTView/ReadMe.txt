--------------------------------------------------------------------------------

                                xlMOSTView

                      Vector Informatik GmbH, Stuttgart

--------------------------------------------------------------------------------

Vector Informatik GmbH
Ingersheimer Stra�e 24
70499 Stuttgart, Germany

Phone: ++49 - 711 - 80670 - 0
Fax:   ++49 - 711 - 80670 - 111


--------------------------------------------------------------------------------

xlMOSTView is a small test application for the MOST functionality for the
VN26X0 MOST interface.

For further information look into the 'XL Driver Library - Description.pdf' 
document.


