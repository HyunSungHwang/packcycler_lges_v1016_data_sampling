--------------------------------------------------------------------------------

                             xlEthBypassDemo


                      Vector Informatik GmbH, Stuttgart

--------------------------------------------------------------------------------

Vector Informatik GmbH
Ingersheimer Stra�e 24
D-70499 Stuttgart

Tel.:  0711-80670-200
Fax.:  0711-80670-555
EMAIL: support@vector.com
WEB:   www.vector.com

--------------------------------------------------------------------------------

xlEthBypassDemo is a small test application for the bypass functionality. The
application sets bypass mode and prints ethernet frames that pass through the device.

For further information look into the 'XL Driver Library - Description.pdf' 
document.
