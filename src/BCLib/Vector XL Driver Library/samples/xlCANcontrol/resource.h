//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by xlCANcontrol.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_XLCANCONTROL_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDB_XL                          133
#define IDC_OUTPUT                      1000
#define IDC_SEND                        1001
#define IDC_BAUDRATE                    1002
#define IDC_ONBUS                       1003
#define IDC_OFFBUS                      1004
#define IDC_CHANNEL                     1005
#define IDC_D07                         1006
#define IDC_D06                         1007
#define IDC_D00                         1008
#define IDC_D01                         1009
#define IDC_D02                         1010
#define IDC_D03                         1011
#define IDC_D04                         1012
#define IDC_D05                         1013
#define IDC_DLC                         1014
#define IDC_ID                          1015
#define IDC_EXID                        1016
#define IDC_HARDWARE                    1017
#define IDC_CLEAR                       1019
#define IDC_ABOUT                       1020
#define IDC_FILTEREX                    1021
#define IDC_RESET                       1022
#define IDC_FILTERFROM                  1023
#define IDC_FILTERTO                    1024
#define IDC_SETFILTER                   1025
#define IDC_LIC_INFO                    1026

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
