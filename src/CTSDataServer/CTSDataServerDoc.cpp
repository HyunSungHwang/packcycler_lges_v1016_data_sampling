// CTSDataServerDoc.cpp : implementation of the CCTSDataServerDoc class
//

#include "stdafx.h"
#include "CTSDataServer.h"

#include "CTSDataServerDoc.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerDoc

IMPLEMENT_DYNCREATE(CCTSDataServerDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSDataServerDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSDataServerDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerDoc construction/destruction

CCTSDataServerDoc::CCTSDataServerDoc()
{
	// TODO: add one-time construction code here

}

CCTSDataServerDoc::~CCTSDataServerDoc()
{
}

BOOL CCTSDataServerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerDoc serialization

void CCTSDataServerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerDoc diagnostics

#ifdef _DEBUG
void CCTSDataServerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSDataServerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

