// CTSDataServerView.cpp : implementation of the CCTSDataServerView class
//

#include "stdafx.h"
#include "CTSDataServer.h"

#include "CTSDataServerDoc.h"
#include "CTSDataServerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerView

IMPLEMENT_DYNCREATE(CCTSDataServerView, CFormView)

BEGIN_MESSAGE_MAP(CCTSDataServerView, CFormView)
	//{{AFX_MSG_MAP(CCTSDataServerView)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerView construction/destruction

CCTSDataServerView::CCTSDataServerView()
	: CFormView(CCTSDataServerView::IDD)
{
	//{{AFX_DATA_INIT(CCTSDataServerView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here

}

CCTSDataServerView::~CCTSDataServerView()
{
}

void CCTSDataServerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTSDataServerView)
	DDX_Control(pDX, IDC_LIST_LOG, m_LogList);
	//}}AFX_DATA_MAP
}

BOOL CCTSDataServerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCTSDataServerView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

}

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerView printing

BOOL CCTSDataServerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCTSDataServerView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCTSDataServerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CCTSDataServerView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerView diagnostics

#ifdef _DEBUG
void CCTSDataServerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCTSDataServerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSDataServerDoc* CCTSDataServerView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSDataServerDoc)));
	return (CCTSDataServerDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSDataServerView message handlers

void CCTSDataServerView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(::IsWindow(this->GetSafeHwnd()))
	{
		CRect ctrlRect;
		RECT rect;
		::GetClientRect(m_hWnd, &rect);
		if(m_LogList.GetSafeHwnd())
		{
			m_LogList.GetWindowRect(&ctrlRect);
			ScreenToClient(&ctrlRect);
			m_LogList.MoveWindow(ctrlRect.left, ctrlRect.top, rect.right-ctrlRect.left-5, rect.bottom-ctrlRect.top-5, FALSE);

		}
	}	
}

void CCTSDataServerView::WriteLogList(CString strLog)
{
	CTime m_Time = CTime::GetCurrentTime();
	CString strTemp;
	strTemp.Format("%d/%02d/%02d  %02d:%02d:%02d :: %s",m_Time.GetYear(),m_Time.GetMonth(),m_Time.GetDay(),
		              m_Time.GetHour(),m_Time.GetMinute(),m_Time.GetSecond(), strLog);
    
	m_LogList.AddString(strTemp);
	if(m_LogList.GetCount() >= 200) 	
	{
		m_LogList.DeleteString(0);
	}	

	//가장 마지막줄을 선택한 상태였으면 Auto scroll
	if(m_LogList.GetCurSel() == m_LogList.GetCount()-2)
	{
		m_LogList.SetCurSel(m_LogList.GetCount()-1);
	}
}

BOOL CCTSDataServerView::UpdateToDataBase(CString strFolder, int nStepNo)
{
	//20070213
	//cycler 타사 Database에 Update
	CString strSQL, strQuery;

	CString str;

	//결과 파일을 Loading 한다.
	CChData chData(strFolder);
//	WriteLogList(strFolder);
	if(chData.CreateA(FALSE) == FALSE )	return FALSE;

	int nTableSize = chData.GetTableSize();
//	int nStepNo = (int)chData.GetLastDataOfTable(nTableSize-1, RS_COL_STEP_NO);
//	str.Format("%d/%d", nTableSize, nStepNo);
//	WriteLogList(str);
	if(nTableSize < 1|| nStepNo < 1)	return FALSE;

	//조건 파일을 구한다.
	CScheduleData *pSchedule = chData.GetPattern();
	if(pSchedule == NULL)			return FALSE;
	
	if(AfxGetApp()->GetProfileInt("Settings", "ERPServer", FALSE))
	{
		CDatabase db;
		CString strTemp;
		CString strLot(chData.GetTestName());						//시험명
		CString strTray(chData.GetTestSerial());					//Cell 번호(test serial 변수) 
		CString strModelName(pSchedule->GetModelName());			//Model명
		CString strProcName(pSchedule->GetScheduleName());			//Schedule명 
		CString strTypeString("NONE");								//Step type명
		CString strUserID(chData.GetUserID());
		CStep *pStep =  pSchedule->GetStepData(nStepNo-1);
		if(pStep == NULL)	return FALSE;
		
		switch(pStep->m_type)
		{
		case PS_STEP_CHARGE:		strTypeString = "CHAR";	break;
		case PS_STEP_DISCHARGE:		strTypeString = "DISC";	break;
		case PS_STEP_OCV:			strTypeString = "OCV";	break;
		case PS_STEP_IMPEDANCE:	
			if(pStep->m_mode == PS_MODE_DCIMP)	strTypeString = "DCIR";	
			if(pStep->m_mode == PS_MODE_ACIMP)	strTypeString = "ACIR";	
			break;
		case PS_STEP_REST:			strTypeString = "REST";	break;
		case PS_STEP_END:			strTypeString = "END";	break;
		}

		db.Open(_T(ERP_DATABASE_NAME), FALSE, FALSE, _T("ODBC;UID=sa;PWD=ucmon"));
		
		try
		{
			// 1. DataBase Open
			db.BeginTrans();

			strSQL.Format("DELETE FROM AGCond WHERE Lot_No = '%s' AND Tray_No = '%s' AND Proc_Type = '%s' AND Step_No = %d", 
						   strLot.Left(10),  strTray.Left(10), strProcName.Left(12), nStepNo);
			db.ExecuteSQL(strSQL);

			// 2. 조건  Update 조건
			int nGradeSize = pStep->m_Grading.GetGradeStepSize();
			if(nGradeSize > 0)
			{
				CString strGradeItem("NO");
				switch(pStep->m_Grading.GetGradeItem())
				{
				case PS_GRADE_VOLTAGE:			strGradeItem = "VTG";	break;
				case PS_GRADE_AMPEREHOUR:			strGradeItem = "CAP";	break;
				case PS_GRADE_IMPEDANCE:		strGradeItem = "IMP";	break;
				}

				CString aa1, bb1;
				GRADE_STEP grade; 
				for(int g=0; g<nGradeSize; g++)
				{
					grade = pStep->m_Grading.GetStepData(g);
					strTemp.Format(", G%dHigh, G%dLow", g+1, g+1);
					aa1 += strTemp;
					strTemp.Format(",  %f, %f", grade.fMax, grade.fMin);
					bb1 += strTemp;
				}

				strSQL = "INSERT INTO AGCond(Lot_No, Tray_No, Model_Name, Proc_Type, Step_No, Step_Type, V_Ref, I_Ref, End_V, End_Time, End_I, Worker, GradeItem";
				strSQL = strSQL + aa1 + ")";
			
				strQuery.Format(" VALUES('%s', '%s', '%s', '%s', %d, '%s', %.1f, %.1f, %.1f, %d, %.1f, '%s', '%s'", 
								strLot.Left(10),  
								strTray.Left(10),
								strModelName.Left(12),
								strProcName.Left(12),
								nStepNo,
								strTypeString,
								pStep->m_fVref,
								pStep->m_fIref,
								pStep->m_fEndV,
								(int)pStep->m_fEndTime,
								pStep->m_fEndI,
								strUserID.Left(10),
								strGradeItem
							);
				strQuery = strQuery + bb1 + ")";
			}
			else
			{
				strSQL = "INSERT INTO AGCond(Lot_No, Tray_No, Model_Name, Proc_Type, Step_No, Step_Type, V_Ref, I_Ref, End_V, End_Time, End_I, Worker)";			
				strQuery.Format(" VALUES('%s', '%s', '%s', '%s', %d, '%s', %.1f, %.1f, %.1f, %d, %.1f, '%s')", 
								strLot.Left(10),  
								strTray.Left(10),
								strModelName.Left(12),
								strProcName.Left(12),
								nStepNo,
								strTypeString,
								pStep->m_fVref,
								pStep->m_fIref,
								pStep->m_fEndV,
								(int)pStep->m_fEndTime,
								pStep->m_fEndI,
								strUserID.Left(10)
							);

			}
			db.ExecuteSQL(strSQL+strQuery);
			strTemp.Format("ERP Server에 Step %d 공정조건 Update 완료", nStepNo);
			WriteLogList(strTemp);
		
			CString strModuleName;
			strModuleName.Format("CYCLER %d", chData.GetModuleID());
			COleDateTime StartTime;
			StartTime.ParseDateTime(chData.GetStartTime());
			long lTotTime = (long)chData.GetLastDataOfTable(nTableSize-1, RS_COL_TOT_TIME);
			long lStepTime = (long)chData.GetLastDataOfTable(nTableSize-1, RS_COL_TIME);
			if(StartTime.GetStatus() == COleDateTime::valid && lTotTime >= lStepTime)
			{
				COleDateTimeSpan timeESpan(lTotTime/(3600*24));
				COleDateTimeSpan timeSSpan(lStepTime/(3600*24));

				COleDateTime sTime = StartTime + (timeESpan-timeSSpan);
				COleDateTime eTime = StartTime + timeESpan;

				//delete insert
				strSQL.Format("DELETE FROM AGResult WHERE Lot_No = '%s' AND Tray_No = '%d' AND Proc_Type = '%s' AND Step_No = %d AND CELL_NO = '%s'", 
							   strLot.Left(10),  chData.GetChannelNo(), strProcName.Left(12), nStepNo, strTray.Left(10));

				db.ExecuteSQL(strSQL);

				char code = (char)chData.GetLastDataOfTable(nTableSize-1, RS_COL_GRADE);
				// 3. Result Update
				CString strGrade;
				strGrade.Format("%c", code);
				strSQL = "INSERT INTO AGResult(Lot_No, Tray_No, Start_Date, End_Date, Module_ID, Proc_Type, Step_No, Cell_No, End_Code, Step_Time, Tot_Time, Voltage, Curr, Capacity, Impedance, Grade, Worker)";
				strQuery.Format(" VALUES('%s', '%d', '%s', '%s', '%s', '%s', %d, '%s', '%03d', %d, %d, %.1f, %.1f, %.1f, %.3f, '%s', '%s')", 
								strLot.Left(10),						//Lot	//test name
								chData.GetChannelNo(),					//Tray	//Channel No
								sTime.Format("%Y-%m-%d %H:%M:%S"),	//Start Date	//YYYY-MM-DD HH:MM:SS
								eTime.Format("%Y-%m-%d %H:%M:%S"),	//End Date		//YYYY-MM-DD HH:MM:SS
								strModuleName,					//Module ID
								strProcName.Left(12),
								nStepNo,								//Step_No
								strTray.Left(10),						//Cell_No	//Lot No
								(BYTE)chData.GetLastDataOfTable(nTableSize-1, RS_COL_CODE),				//Ch_code
								lStepTime,				//Step_Time
								lTotTime,			//Tot_Time
								chData.GetLastDataOfTable(nTableSize-1, RS_COL_VOLTAGE),					//Voltage
								chData.GetLastDataOfTable(nTableSize-1, RS_COL_CURRENT),					//Current
								chData.GetLastDataOfTable(nTableSize-1, RS_COL_CAPACITY),					//Capacity
								chData.GetLastDataOfTable(nTableSize-1, RS_COL_IR),					//Impedance
								strGrade,								//Grade Code
								strUserID.Left(10)										//Worker
							);
				db.ExecuteSQL(strSQL+strQuery);

/*				FILE *p = fopen("c:\\tt.txt", "wt");
				if(p)
				{
					fprintf(p, "%s%s\n",strSQL, strQuery );
					fclose(p);
				}
*/				//db.CommitTrans();
				strTemp.Format("ERP Server에 Step %d 공정결과 Update 완료", nStepNo);
			}
			else
			{
				strTemp.Format("Step %d timd 정보 오류(%d/%d)", nStepNo, lStepTime, lTotTime);
			}
			db.CommitTrans();
			WriteLogList(strTemp);

		}
		catch (CDBException* e)
		{
/*				FILE *p = fopen("c:\\tt.txt", "wt");
				if(p)
				{
					fprintf(p, "%s%s\n",strSQL, strQuery );
					fclose(p);
				}
*/			WriteLogList(e->m_strError);
			WriteLogList(strSQL);
  			WriteLogList(strQuery);
    		e->Delete();
			db.Rollback();	
		}
		// 3. Close
		db.Close();
	}
	//////////////////////////////////////////////////////////////////////////
	return TRUE;
}
