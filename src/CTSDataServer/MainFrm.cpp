// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CTSDataServer.h"
#include "MainFrm.h"
#include "CTSDataServerDoc.h"
#include "CTSDataServerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_TRAY_MESSAGE			WM_USER + 100

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_SETUP, OnUpdateFilePrintSetup)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	ON_COMMAND(ID_SERVER_OPEN, OnServerOpen)
	ON_COMMAND(ID_SERVER_CLOSE, OnServerClose)
	ON_WM_CLOSE()
	ON_MESSAGE(ID_TRAY_MESSAGE, OnTrayNotifying)
	ON_WM_SYSCOMMAND()
	ON_WM_COPYDATA()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	//	ON_MESSAGE(EPWM_MODULE_CONNECTED, OnConnected)

END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	m_bFlag = FALSE;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

/*	BOOL ret = GetPrivateProfileStruct("CONFIG", "TOPCONFIG", &m_TopConfig, sizeof(STR_TOP_CONFIG), INI_FILE);
	if (ret == FALSE)
	{
		m_TopConfig = GetDefaultTopConfigSet();
		WriteTopConfig(m_TopConfig);
//		WritePrivateProfileStruct("CONFIG", "TOPCONFIG", &m_TopConfig, sizeof(STR_TOP_CONFIG), INI_FILE);
	}
*/

	////////////////////////////////////////////////////////////////////////
	// 트레이 아이콘 삽입하는 부분..
	NOTIFYICONDATA data ;
	HICON hIcon ;

	data.cbSize = sizeof(NOTIFYICONDATA) ;
	data.hWnd = m_hWnd ;
	data.uID = IDI_TRAY_ICON ;
	data.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP ;
	data.uCallbackMessage = ID_TRAY_MESSAGE ;

	hIcon = (HICON) LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_TRAY_ICON)) ;
	data.hIcon = hIcon ;

	strcpy(data.szTip, (LPSTR)(LPCTSTR)"PNE CTS data server") ;

	Shell_NotifyIcon(NIM_ADD, &data) ;
	if ( hIcon )
		DestroyIcon(hIcon) ;
	// 트레이 아이콘 삽입
	////////////////////////////////////////////////////////////////////

	SetTimer(100, 1000, NULL);

	SetWindowText("CTSDataPro Server");
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.style &= ~FWS_ADDTOTITLE;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

/*
STR_TOP_CONFIG GetDefaultTopConfigSet()
{
	STR_TOP_CONFIG strTopConfig;
	memset(&strTopConfig, 0, sizeof(STR_TOP_CONFIG));
	FILE *fp = fopen("SettingFile.dat", "rb");
	if(fp != NULL)
	{
		fread(&strTopConfig, sizeof(STR_TOP_CONFIG), 1, fp);
		fclose(fp);
	}
	return strTopConfig;
}
*/
/*
void CMainFrame::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_FileNew);
}
*/
void CMainFrame::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
}

void CMainFrame::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateFilePrintSetup(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

////////////////////////////////////////////////////////////////////
// 트레이 아이콘 삽입부분..
LONG CMainFrame::OnTrayNotifying(WPARAM wParam, LPARAM lParam)
{
	switch ( lParam )
	{
		case WM_RBUTTONDOWN :
		{
			CMenu menu, *pSubMenu ;
			menu.LoadMenu(IDR_MENU1);
			
			if ( ( pSubMenu = menu.GetSubMenu(0) ) == NULL )  
			{
				return 0L;
			}

			CPoint pt;
			GetCursorPos(&pt);
			SetForegroundWindow();
		
			pSubMenu->TrackPopupMenu(
				TPM_LEFTBUTTON, pt.x, pt.y, this);
			break ;
		}
		case WM_LBUTTONDBLCLK :
		{
			OnServerOpen();
			break;
		}
	}
	return 0;
}

void CMainFrame::OnServerOpen() 
{
	if ( m_bFlag == TRUE ) {
		return ;
	}
	else {
		m_bFlag = TRUE ;
		ShowWindow(SW_SHOWMAXIMIZED) ;		
		SetForegroundWindow();
		//ShowWindow(SW_SHOW) ;		
	}

	m_bFlag = FALSE ;	
}

void CMainFrame::OnServerClose() 
{
	SendMessage(WM_CLOSE) ;	
//	PostMessage(WM_CLOSE);
}

void CMainFrame::OnClose() 
{
	if(m_DataBufferList.GetCount() > 0)
	{
		CString str;
		str.Format("Data base server에 보고 되지 않은 data가 존재 합니다.(%d) 잠시 기다려 주십시요.", m_DataBufferList.GetCount());
		AfxMessageBox(str);
		return;
	}

	KillTimer(100);

	NOTIFYICONDATA data ;

	data.cbSize = sizeof(NOTIFYICONDATA) ;
	data.hWnd = m_hWnd ;
//	data.uID = IDI_TRAY_ICON ;
	data.uID = IDR_MAINFRAME ;
	Shell_NotifyIcon(NIM_DELETE, &data) ;
	
	
	CFrameWnd::OnClose();
}

void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam) 
{
	if ( nID == SC_MINIMIZE )
	{
		ShowWindow(SW_SHOWMINIMIZED) ;
		AfxGetApp()->HideApplication();	
	}
	else if( nID == SC_CLOSE )
	{
		ShowWindow(SW_SHOWMINIMIZED) ;
		AfxGetApp()->HideApplication();	
		return;
	}
	CFrameWnd::OnSysCommand(nID, lParam);
}


BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default
	ASSERT(pCopyDataStruct);

	//CTSMonPro에서 전송되어 온 Message (Cycler)
	
	if(pCopyDataStruct->dwData > 1)
	{
		if(pCopyDataStruct->cbData > 0)
		{
			char szData[MAX_PATH];
			ZeroMemory(szData, MAX_PATH);
			memcpy(szData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);

			CString strData(szData);
			m_cs.Lock();

			m_DataBufferList.AddTail(strData);
			m_awStepNo.Add(WORD(pCopyDataStruct->dwData));
			m_cs.Unlock();
		}
	}			
	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent == 100)
	{
		CString strFolder;	
		int cnt = 0;
		m_cs.Lock();
		POSITION  pos = m_DataBufferList.GetHeadPosition();
		while(pos)
		{
			if(m_awStepNo.GetSize() < 1)
			{
				m_DataBufferList.RemoveAll();
				break;
			}

			strFolder = m_DataBufferList.GetNext(pos);
			int nStepNo = m_awStepNo.GetAt(cnt++);

			TRACE("%s\n", strFolder);
			if(((CCTSDataServerView *)GetActiveView())->UpdateToDataBase(strFolder, nStepNo) == FALSE)
			{
				TRACE("Update error\n");
			}
		}
		m_awStepNo.RemoveAll();
		m_DataBufferList.RemoveAll();
		m_cs.Unlock();
	}
	CFrameWnd::OnTimer(nIDEvent);
}