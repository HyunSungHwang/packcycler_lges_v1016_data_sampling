//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CTSDataServer.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_CTSDATASERVER_FORM          101
#define IDR_MAINFRAME                   128
#define IDR_CTSDATTYPE                  129
#define IDI_TRAY_ICON                   131
#define IDR_MENU1                       132
#define IDD_SETTING_DIALOG              135
#define IDC_LIST_LOG                    1000
#define IDC_DATA_PATH_BUTTON            1002
#define IDC_INTERVAL_EDIT               1009
#define IDC_EDIT1                       1010
#define IDC_MONTH_CHECK                 1011
#define IDC_DAY_CHECK                   1012
#define IDC_DAY_COMBO                   1013
#define IDC_FILE_CHECK                  1018
#define IDC_CHECK_TRAY                  1019
#define IDC_CHECK_MODULE                1020
#define IDC_CHECK_PROFILE_DATA          1021
#define IDC_CHECK_DB_SAVE               1022
#define IDC_CHECK1                      1023
#define ID_SERVER_OPEN                  32773
#define ID_SERVER_CLOSE                 32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
