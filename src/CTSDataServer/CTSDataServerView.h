// CTSDataServerView.h : interface of the CCTSDataServerView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSDATASERVERVIEW_H__0D6A8D0A_3F68_4B71_9FCB_0A60D24BFF0B__INCLUDED_)
#define AFX_CTSDATASERVERVIEW_H__0D6A8D0A_3F68_4B71_9FCB_0A60D24BFF0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCTSDataServerView : public CFormView
{
protected: // create from serialization only
	CCTSDataServerView();
	DECLARE_DYNCREATE(CCTSDataServerView)

public:
	//{{AFX_DATA(CCTSDataServerView)
	enum { IDD = IDD_CTSDATASERVER_FORM };
	CListBox	m_LogList;
	//}}AFX_DATA

// Attributes
public:
	CCTSDataServerDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSDataServerView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL UpdateToDataBase(CString strFolder, int nStepNo);
	void WriteLogList(CString strLog);
	virtual ~CCTSDataServerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCTSDataServerView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CTSDataServerView.cpp
inline CCTSDataServerDoc* CCTSDataServerView::GetDocument()
   { return (CCTSDataServerDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSDATASERVERVIEW_H__0D6A8D0A_3F68_4B71_9FCB_0A60D24BFF0B__INCLUDED_)
