// CTSDataServerDoc.h : interface of the CCTSDataServerDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSDATASERVERDOC_H__0E01CB2E_B79B_418B_AF80_4D8A18DEAAEE__INCLUDED_)
#define AFX_CTSDATASERVERDOC_H__0E01CB2E_B79B_418B_AF80_4D8A18DEAAEE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCTSDataServerDoc : public CDocument
{
protected: // create from serialization only
	CCTSDataServerDoc();
	DECLARE_DYNCREATE(CCTSDataServerDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSDataServerDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCTSDataServerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCTSDataServerDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSDATASERVERDOC_H__0E01CB2E_B79B_418B_AF80_4D8A18DEAAEE__INCLUDED_)
