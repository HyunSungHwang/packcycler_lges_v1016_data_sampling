// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__487ED9AF_5559_41F0_9F8C_A1AEA4F1BBBD__INCLUDED_)
#define AFX_STDAFX_H__487ED9AF_5559_41F0_9F8C_A1AEA4F1BBBD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxdb.h>
#include <afxdao.h>
#include "../BCLib/Include/PSCommonAll.h"

#ifdef _DEBUG
#pragma comment(lib, "../BCLib/Lib/PSCommonD.lib")
#pragma message("Automatically linking with PSCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../BCLib/Lib/PSCommon.lib")
#pragma message("Automatically linking with PSCommon.lib By K.B.H")
#endif	//_DEBUG


#define ERP_DATABASE_NAME	"UCMONDB"


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__487ED9AF_5559_41F0_9F8C_A1AEA4F1BBBD__INCLUDED_)
