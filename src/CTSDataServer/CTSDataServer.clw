; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCTSDataServerView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CTSDataServer.h"
LastPage=0

ClassCount=5
Class1=CCTSDataServerApp
Class2=CCTSDataServerDoc
Class3=CCTSDataServerView
Class4=CMainFrame

ResourceCount=9
Resource1=IDD_ABOUTBOX
Resource2=IDR_MENU1
Resource7=IDR_MAINFRAME
Resource8=IDD_CTSDATASERVER_FORM
Class5=CAboutDlg
Resource9=IDD_SETTING_DIALOG

[CLS:CCTSDataServerApp]
Type=0
HeaderFile=CTSDataServer.h
ImplementationFile=CTSDataServer.cpp
Filter=N
LastObject=CCTSDataServerApp

[CLS:CCTSDataServerDoc]
Type=0
HeaderFile=CTSDataServerDoc.h
ImplementationFile=CTSDataServerDoc.cpp
Filter=N

[CLS:CCTSDataServerView]
Type=0
HeaderFile=CTSDataServerView.h
ImplementationFile=CTSDataServerView.cpp
Filter=D
BaseClass=CFormView
VirtualFilter=VWC
LastObject=CCTSDataServerView


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=CTSDataServer.cpp
ImplementationFile=CTSDataServer.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
Command16=ID_APP_ABOUT
CommandCount=16

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_CTSDATASERVER_FORM]
Type=1
Class=CCTSDataServerView
ControlCount=1
Control1=IDC_LIST_LOG,listbox,1352728835

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_SERVER_OPEN
Command2=ID_SERVER_CLOSE
CommandCount=2

[DLG:IDD_SETTING_DIALOG]
Type=1
Class=?
ControlCount=23
Control1=IDC_FILE_CHECK,button,1342242819
Control2=IDC_STATIC,button,1342177287
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT1,edit,1350631552
Control5=IDC_DATA_PATH_BUTTON,button,1342242816
Control6=IDC_MONTH_CHECK,button,1342242819
Control7=IDC_CHECK_TRAY,button,1342242819
Control8=IDC_CHECK_MODULE,button,1342242819
Control9=IDC_DAY_CHECK,button,1342242819
Control10=IDC_DAY_COMBO,combobox,1344339971
Control11=IDC_STATIC,static,1342308352
Control12=IDC_INTERVAL_EDIT,edit,1350631554
Control13=IDC_STATIC,static,1342308352
Control14=IDOK,button,1342242817
Control15=IDCANCEL,button,1342242816
Control16=IDC_CHECK_PROFILE_DATA,button,1342242819
Control17=IDC_STATIC,button,1342177287
Control18=IDC_CHECK_DB_SAVE,button,1342242819
Control19=IDC_CHECK1,button,1342242819
Control20=IDC_STATIC,static,1342308352
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352
Control23=IDC_STATIC,static,1342308352

