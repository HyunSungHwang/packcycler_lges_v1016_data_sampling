// DCRScopeView.cpp : implementation of the CDCRScopeView class
//

#include "stdafx.h"
#include "DCRScope.h"

#include "DCRScopeDoc.h"
#include "DCRScopeView.h"
#include "TextParsing.h"
#include "MainFrm.h"
#include "DcrSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeView

IMPLEMENT_DYNCREATE(CDCRScopeView, CFormView)

BEGIN_MESSAGE_MAP(CDCRScopeView, CFormView)
	//{{AFX_MSG_MAP(CDCRScopeView)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_CAL, OnButtonCal)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_WM_DESTROY()
	ON_COMMAND(ID_DCR_SETTING, OnDcrSetting)
	ON_COMMAND(ID_AXIS_SET, OnAxisSet)
	ON_UPDATE_COMMAND_UI(ID_AXIS_SET, OnUpdateAxisSet)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_GRAPH_SET, OnGraphSet)
	ON_COMMAND(ID_MAXIMIZE, OnMaximize)
	ON_BN_CLICKED(IDC_BNT_UNZOOM, OnBntUnzoom)
	ON_BN_CLICKED(IDC_BNT_ZOOM, OnBntZoom)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeView construction/destruction

CDCRScopeView::CDCRScopeView()
	: CFormView(CDCRScopeView::IDD)
{
	//{{AFX_DATA_INIT(CDCRScopeView)
	m_fTime1 = 0.0f;
	m_fTime2 = 0.0f;
	m_bUseFitLine = FALSE;
	m_fZoomTime1 = 0.0f;
	m_fZoomTime2 = 0.0f;
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	m_nPrevExcludeCount = AfxGetApp()->GetProfileInt("Settings", "PrevExCount", 0);
	m_nPrevIncludeCount = AfxGetApp()->GetProfileInt("Settings", "PrevInCount", LINEAR_DATA_POINT);
	m_nAfterExcludeCount = AfxGetApp()->GetProfileInt("Settings", "AfterExCount", 0);
	m_nAfterIncludeCount = AfxGetApp()->GetProfileInt("Settings", "AfterInCount", LINEAR_DATA_POINT);
}

CDCRScopeView::~CDCRScopeView()
{
}

void CDCRScopeView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDCRScopeView)
	DDX_Text(pDX, IDC_EDIT1, m_fTime1);
	DDX_Text(pDX, IDC_EDIT2, m_fTime2);
	DDX_Check(pDX, IDC_CHECK, m_bUseFitLine);
	DDX_Text(pDX, IDC_EDIT_T1, m_fZoomTime1);
	DDX_Text(pDX, IDC_EDIT_T2, m_fZoomTime2);
	//}}AFX_DATA_MAP
}

BOOL CDCRScopeView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CDCRScopeView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	m_wndGraph.SubclassDlgItem(IDC_STATIC_GRAPH, this);
//	InitGrid();

	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

//ljb 주석  [10/6/2011 XNOTE]
//	m_fTime1 = (double)atof(AfxGetApp()->GetProfileString("Settings", "Time1", "0.0"));
	m_fTime2 = (double)atof(AfxGetApp()->GetProfileString("Settings", "Time2", "0.0"));

	m_bUseFitLine = AfxGetApp()->GetProfileInt("Settings", "UseFitLine", FALSE);

	m_fZoomTime1 = (double)atof(AfxGetApp()->GetProfileString("Settings", "Zoom Time1", "0.0"));
	m_fZoomTime2 = (double)atof(AfxGetApp()->GetProfileString("Settings", "Zoom Time2", "0.0"));

	COLORREF m_Y1Color, m_Y2Color;
	m_Y1Color = AfxGetApp()->GetProfileInt("Settings", "Y1Color", RGB(230,  50,  0));
	m_Y2Color = AfxGetApp()->GetProfileInt("Settings", "Y2Color", RGB(  35, 50,200));
	m_wndGraph.InitSubset(0, m_Y1Color);
	m_wndGraph.InitSubset(1, m_Y2Color);

	UpdateData(FALSE);
}	


/////////////////////////////////////////////////////////////////////////////
// CDCRScopeView printing

BOOL CDCRScopeView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CDCRScopeView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CDCRScopeView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CDCRScopeView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeView diagnostics

#ifdef _DEBUG
void CDCRScopeView::AssertValid() const
{
	CFormView::AssertValid();
}

void CDCRScopeView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CDCRScopeDoc* CDCRScopeView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDCRScopeDoc)));
	return (CDCRScopeDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeView message handlers
/*
BOOL CDCRScopeView::OnEraseBkgnd(CDC* pDC) 
{
	//return FALSE;
	return TRUE;
}
*/

void CDCRScopeView::OnFileOpen() 
{
	// TODO: Add your command handler code here
	CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "csv file(*.csv)|*.csv|All Files(*.*)|*.*|");
	//pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";
	if(IDOK == pDlg.DoModal())
	{
		LoadData(pDlg.GetPathName());
	}	
}

void CDCRScopeView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	RECT r;
	CRect ctrlRect, ctrlGrid;
	if(::IsWindow(m_hWnd))
	{
		::GetClientRect(m_hWnd, &r);
		
/*		if(m_wndDataGrid.GetSafeHwnd())
		{
			m_wndDataGrid.GetWindowRect(&ctrlGrid);
			ScreenToClient(&ctrlGrid);
			m_wndDataGrid.MoveWindow(r.right-ctrlGrid.Width()-5, ctrlGrid.top, ctrlGrid.Width(), r.bottom-ctrlGrid.top-5, FALSE);
			m_wndDataGrid.Redraw();
			
*/			if(::IsWindow(m_wndGraph.GetSafeHwnd()))
			{
				GetDlgItem(IDC_STATIC_GRAPH)->GetWindowRect(ctrlRect);
				ScreenToClient(&ctrlRect);
				//m_wndGraph.MoveWindow(ctrlRect.left, ctrlRect.top, r.right-ctrlRect.left-5-ctrlGrid.Width()-5, r.bottom-ctrlRect.top-5, TRUE);
				GetDlgItem(IDC_STATIC_GRAPH)->MoveWindow(ctrlRect.left, ctrlRect.top, r.right-ctrlRect.left-5, r.bottom-ctrlRect.top-5, TRUE);
			}
//		}	
	}
}

BOOL CDCRScopeView::DrawGraph()
{
	CDCRScopeDoc *pDoc = (CDCRScopeDoc *)GetDocument();
	CTextParsing *pSheet = pDoc->GetDataSheet();
	
	int n = pSheet->GetRecordCount();	
	CWordArray *pSelYAxis = m_axisDlg.GetSelYAxis();
	
	m_wndGraph.ClearGraph();
	for(int i=0;  i<PE_MAX_SUBSET; i++)
	{
		m_wndGraph.ShowSubset(i, FALSE);
	}

	for(int i = 0; i<pSelYAxis->GetSize() && i< PE_MAX_SUBSET; i++)
	{
		int nColIndex = pSelYAxis->GetAt(i);
		float *fData = pSheet->GetColumnData(nColIndex);
			
		int a = m_axisDlg.GetSelXAxis();
			
		float *fXData = pSheet->GetColumnData(m_axisDlg.GetSelXAxis());
		m_wndGraph.SetDataPtr(i, n, fXData, fData);
		m_wndGraph.SetYAxisLabel(i, pSheet->GetColumnHeader(nColIndex));
		m_wndGraph.SetDataTitle (i, pSheet->GetColumnHeader(nColIndex));
		m_wndGraph.ShowSubset(i, TRUE);
	}

	//Set X Axis Label
	m_wndGraph.SetXAxisLabel(pSheet->GetColumnHeader(m_axisDlg.GetSelXAxis()));

	//Set Title
	CString strTemp;
	CString strFile = pDoc->GetDataFileName();
	strTemp = strFile.Mid(strFile.ReverseFind('\\')+1);
	m_wndGraph.SetMainTitle(strTemp.Left(strTemp.ReverseFind('.')));
	
	return TRUE;
}

void CDCRScopeView::OnButtonCal() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	float fVData1, fIData1;
	float fVData2, fIData2;
	int nDataIndex1, nDataIndex2;

	nDataIndex1 = FindPointData(m_fTime1, fVData1, fIData1, m_bUseFitLine, TRUE);
	if(nDataIndex1 < 0)
	{
		AfxMessageBox("해당 지점1의 Data를 찾을 수 없습니다.");
		return;
	}

	//nDataIndex2 = FindPointData(m_fTime1, fVData2, fIData2, m_bUseFitLine, FALSE);
	nDataIndex2 = FindPointData(m_fTime2, fVData2, fIData2, m_bUseFitLine, FALSE);
	if(nDataIndex2 < 0)
	{
		AfxMessageBox("해당 지점2의 Data를 찾을 수 없습니다.");
		return;
	}

	double fResist = 0.0f;
	double fDeltaI, fDeltaV;
	fDeltaI = fIData1-fIData2;
	fDeltaV = fVData1-fVData2;
	if(fDeltaI != 0.0f)
	{
		fResist = fDeltaV/fDeltaI*1000.0f;
	}

	CString str, str1;
	str.Format("%.3f", fVData1/1000);
	GetDlgItem(IDC_EDIT_V1)->SetWindowText(str);
	str.Format("%.3f", fVData2/1000);
	GetDlgItem(IDC_EDIT_V2)->SetWindowText(str);
	str.Format("%.3f", fIData1/1000);
	GetDlgItem(IDC_EDIT_I1)->SetWindowText(str);
	str.Format("%.3f", fIData2/1000);
	GetDlgItem(IDC_EDIT_I2)->SetWindowText(str);
	str.Format("%.6f", fResist);
	GetDlgItem(IDC_EDIT_R)->SetWindowText(str);

// 	str.Format("V(%.3f, %.3f), I(%.3f, %.3f), R : %f", fVData1, fVData2, fIData1, fIData2, fResist);
// 	m_wndGraph.SetSubTitle(str);

	((CMainFrame *)AfxGetMainWnd())->m_wndGridBar.SelectDataRange(nDataIndex1, nDataIndex1);
	((CMainFrame *)AfxGetMainWnd())->m_wndGridBar.SelectDataRange(nDataIndex2, nDataIndex2, FALSE);

	str.Format("t1:%0.3f", m_fTime1);
	str1.Format("t2:%.3f", m_fTime2);
	m_wndGraph.AddXAnnotation(m_fTime1, m_fTime2, str, str1);
}

//fTime에서의 전압(fData1), 전류(fData2)를 구한다.
int CDCRScopeView::FindPointData(float fTime, float &fData1, float &fData2, BOOL bUseFitLine /*= FALSE*/, BOOL bUsePrevData /*= TRUE*/)
{
	fTime = fTime *1000.0f;	//  [10/6/2011 XNOTE]
	fData1 = 0.0f;
	fData2 = 0.0f;

	CDCRScopeDoc *pDoc = (CDCRScopeDoc *)GetDocument();
	CTextParsing *pSheet = pDoc->GetDataSheet();
	
	int nTot = pSheet->GetRecordCount();

	CString strTemp;

	//Get Time Data
	int nTimeIndex = 0;
	int nVoltageindex = 1;
	int nCurrentIndex = 2;
	float *pfDataT, *pfDataV, *pfDataI;

	pfDataT = pSheet->GetColumnData(nTimeIndex);	
	pfDataV = pSheet->GetColumnData(nVoltageindex);		
	pfDataI = pSheet->GetColumnData(nCurrentIndex);		

	//Find time index
	if(nTot < 1)	return -1;
	if( fTime < pfDataT[0] || pfDataT[nTot-1] < fTime)		return -1;
	int index = 0;
	for(int i=1; i<nTot; i++)
	{
		if(pfDataT[i-1] <= fTime && fTime < pfDataT[i])
		{
			index = i-1;
			break;
		}
	}

	if(bUseFitLine == FALSE)
	{
		//Voltage Data
		fData1 = pfDataV[index];	
		//Current Data
		fData2 = pfDataI[index];

		m_wndGraph.AddLineAnnotation(0, 0, 0, 0, 0, "");
		m_wndGraph.AddLineAnnotation(1, 0, 0, 0, 0, "");
	}
	else
	{
		float a = 0, b=0;
		//최대 근사 1차 방정식을 구한다.(y=aX+b)
		//sigma(y) = n*b+a*sigma(x)
		//sigma(x*y) = b*sigma(x)+a*sigma(x*x)
		// a = (n*sigma(x*y)-sigma(x)*sigma(y))/(n*sigma(x*x)-sigma(x)*sigma(x))
		// b = (sigma(y)-a*sigma(x))/n

		int n = 0;
		int nDataIndex = 0;
		//이전 Data로 현재값 유추 
		if(bUsePrevData)
		{

//			n = index+1-m_nPrevExcludeCount;
// 			if(n < 1)
// 			{
// 				return -1;
// 			}
//			if(n > m_nPrevIncludeCount)	n = m_nPrevIncludeCount;

// 			float fTime2 = 0.0f;
//			float fSigmaXY = 0.0f, fSigmaX = 0.0f, fSigmaY = 0.0f, fSigmaXX = 0.0f;
			//voltage
// 			if(n == 1)
// 			{
// 				a = 0;
// 				b = pfDataV[index-m_nPrevExcludeCount];
// 			}
// 			else
// 			{
// 				for(int i =0; i<n; i++)
// 				{
// 					nDataIndex = index-i-m_nPrevExcludeCount;
// 					//1. sigma(x*y)
// 					fSigmaXY += (pfDataT[nDataIndex]*pfDataV[nDataIndex]);
// 					//2. sigma(x)
// 					fSigmaX += pfDataT[nDataIndex];
// 					//3. sigma(y)
// 					fSigmaY += pfDataV[nDataIndex];
// 					//4. sigma(x*x)
// 					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
// 					fTime2 = pfDataT[nDataIndex];
// 				}
// 				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
// 				b = (fSigmaY-a*fSigmaX)/n;
// 
// 			}
		
//			fData1 = a*fTime+b;
//			strTemp.Format("Y=%.3fX+%.3f", a, b);
			fData1 = pfDataV[index];
			strTemp.Format("V=%.3f", fData1);			
			//m_wndGraph.AddLineAnnotation(0, fTime2, a*fTime2+b, fTime, fData1, strTemp);

			//Current
// 			fSigmaXY = 0.0f; fSigmaX = 0.0f; fSigmaY = 0.0f; fSigmaXX = 0.0f;
// 			if(n == 1)
// 			{
// 				a = 0;
// 				b = pfDataI[index-m_nPrevExcludeCount];
// 			}
// 			else
// 			{
// 				for(int i = 0; i<n; i++)
// 				{
// 					nDataIndex = index-i-m_nPrevExcludeCount;
// 					//1. sigma(x*y)
// 					fSigmaXY += (pfDataT[nDataIndex]*pfDataI[nDataIndex]);
// 					//2. sigma(x)
// 					fSigmaX += pfDataT[nDataIndex];
// 					//3. sigma(y)
// 					fSigmaY += pfDataI[nDataIndex];
// 					//4. sigma(x*x)
// 					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
// 					fTime2 = pfDataT[nDataIndex];
// 				}
// 
// 				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
// 				b = (fSigmaY-a*fSigmaX)/n;
// 			}
// 			fData2 = a*fTime+b;
// 			strTemp.Format("Y=%.3fX+%.3f",a, b);
			fData2 = pfDataI[index];
			strTemp.Format("I=%.3f", fData2);			

			TRACE("Crt1 : %s\n", strTemp);
//			m_wndGraph.AddLineAnnotation(0, fTime2, a*fTime2+b, fTime, fData2, strTemp);
//			m_wndGraph.AddLineAnnotation(fTime, fData2, fTime2, a*fTime2+b, strTemp);
		}
		else	//이후 Data로 현재값 유추
		{
			int startindex = index+m_nAfterExcludeCount;
			n = nTot-startindex;
			if(n < 1)
			{
				return -1;
			}
			if(n > m_nAfterIncludeCount)	n = m_nAfterIncludeCount;
			
			double fTime2 = 0.0f;
			double fSigmaXY = 0.0f, fSigmaX = 0.0f, fSigmaY = 0.0f, fSigmaXX = 0.0f;
			//voltage
			if(n ==1)
			{
				a = 0; 
				b = pfDataV[startindex];
			}
			else
			{
				for(int i =0; i<n; i++)
				{
					nDataIndex = startindex+i;
					//1. sigma(x*y)
					fSigmaXY += (pfDataT[nDataIndex]*pfDataV[nDataIndex]);
					//2. sigma(x)
					fSigmaX += pfDataT[nDataIndex];
					//3. sigma(y)
					fSigmaY += pfDataV[nDataIndex];
					//4. sigma(x*x)
					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
					fTime2 = pfDataT[nDataIndex];
					TRACE("%f\n", pfDataV[nDataIndex]);
				}

				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
				b = (fSigmaY-a*fSigmaX)/n;
			}
			//fData1 = a*fTime+b;
			fData1 = b;
			//strTemp.Format("Y = %.6f X+%.1f", a, b);
			strTemp.Format("v = %.3f", b);


			TRACE("\n %.3f",fTime2/1000);
			TRACE("\n %.3f",a*(fTime2/1000)+b);
			//m_wndGraph.AddLineAnnotation(1, fTime, fData1, fTime2, a*fTime2+b, strTemp);
			m_wndGraph.AddLineAnnotation(1, 0, fData1/1000, fTime2/1000, (a*(fTime2)+b)/1000, strTemp);
			TRACE("\n a=%.6f, b=%.1f",a,b);

			strTemp.Format("Y = %.6f * time + %.3f", a,b);
			m_wndGraph.SetSubTitle(strTemp);

			//Current
			fSigmaXY = 0.0f; fSigmaX = 0.0f; fSigmaY = 0.0f; fSigmaXX = 0.0f;
			if(n ==1)
			{
				a = 0; 
				b = pfDataI[startindex];
			}
			else
			{
				for(int i = 0; i<n; i++)
				{
					nDataIndex = startindex+i;
// 					//1. sigma(x*y)
// 					fSigmaXY += (pfDataT[nDataIndex]*pfDataI[nDataIndex]);
// 					//2. sigma(x)
// 					fSigmaX += pfDataT[nDataIndex];
					//3. sigma(y)
					fSigmaY += pfDataI[nDataIndex];
// 					//4. sigma(x*x)
// 					fSigmaXX += pfDataT[nDataIndex]*pfDataT[nDataIndex];
// 					TRACE("%f %f\n", pfDataT[nDataIndex], pfDataI[nDataIndex]);
					fTime2 = pfDataT[nDataIndex];
				}

// 				a = (n*fSigmaXY-fSigmaX*fSigmaY)/(n*fSigmaXX-fSigmaX*fSigmaX);
// 				b = (fSigmaY-a*fSigmaX)/n;
			}
//			fData2 = a*fTime+b;
			fData2 = fSigmaY / n;
			TRACE("\n Avg Current : %.3f \n", fData2);
//			fData2 = -97000000;	//ljb 임시 

			strTemp.Format("Y=%.3fX+%.3f", a, b);				
//			m_wndGraph.AddLineAnnotation(1, fTime, fData2, fTime2, a*fTime2+b, strTemp);
			TRACE("Crt2 : %s\n", strTemp);

		}
	}
	return index;
}

BOOL CDCRScopeView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		if(GetFocus() == GetDlgItem(IDC_EDIT1) || GetFocus() == GetDlgItem(IDC_EDIT2))
		{
			switch( pMsg->wParam )
			{
			case VK_RETURN:
				OnButtonCal();
				return TRUE;
			}
		}
	}	
	return CFormView::PreTranslateMessage(pMsg);
}

void CDCRScopeView::InitGrid()
{
	m_wndDataGrid.SubclassDlgItem(IDC_CUSTOM_GRID, this);
	m_wndDataGrid.Initialize();
	m_wndDataGrid.GetParam()->EnableUndo(FALSE);
	m_wndDataGrid.SetColCount(5);

//	SetModelGridColumnWidth();

	m_wndDataGrid.SetStyleRange(CGXRange(0, 0),	CGXStyle().SetValue("No"));
	m_wndDataGrid.SetStyleRange(CGXRange(0, 1),	CGXStyle().SetValue("A"));
	m_wndDataGrid.SetStyleRange(CGXRange(0, 2),	CGXStyle().SetValue("B"));

	m_wndDataGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
//	m_wndDataGrid.SetStyleRange(CGXRange().SetCols(COL_MODEL_NAME, COL_MODEL_DESCRIPTION), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));
}

void CDCRScopeView::UpdateGridData()
{
	CDCRScopeDoc *pDoc = (CDCRScopeDoc *)GetDocument();
	CTextParsing *pSheet = pDoc->GetDataSheet();

	if(((CMainFrame *)AfxGetMainWnd())->m_wndGridBar.IsVisible())
	{
		((CMainFrame *)AfxGetMainWnd())->m_wndGridBar.DisplaySheetData(pSheet);
	}
}

void CDCRScopeView::OnFileSave() 
{
	// TODO: Add your command handler code here
	m_wndGraph.SaveGraph();
}

void CDCRScopeView::OnFilePrint() 
{
	// TODO: Add your command handler code here
	m_wndGraph.Print();
}

void CDCRScopeView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	UpdateData();
	CString str;
//ljb 주석  [10/6/2011 XNOTE]
// 	str.Format("%f", m_fTime1);
// 	AfxGetApp()->WriteProfileString("Settings", "Time1", str);
	str.Format("%f", m_fTime2);
	AfxGetApp()->WriteProfileString("Settings", "Time2", str);
	AfxGetApp()->WriteProfileInt("Settings", "UseFitLine", m_bUseFitLine);

	str.Format("%f", m_fZoomTime1);
	AfxGetApp()->WriteProfileString("Settings", "Zoom Time1", str);
	str.Format("%f", m_fZoomTime2);
	AfxGetApp()->WriteProfileString("Settings", "Zoom Time2", str);

}

void CDCRScopeView::OnDcrSetting() 
{
	// TODO: Add your command handler code here
	CDcrSetDlg dlg;
	dlg.m_nPrevExcludeCount = m_nPrevExcludeCount;
	dlg.m_nPrevIncludeCount = m_nPrevIncludeCount;
	dlg.m_nAfterExcludeCount = m_nAfterExcludeCount;
	dlg.m_nAfterIncludeCount = m_nAfterIncludeCount;
	if(dlg.DoModal() ==IDOK)
	{
		m_nPrevExcludeCount =dlg.m_nPrevExcludeCount ;
		m_nPrevIncludeCount =dlg.m_nPrevIncludeCount ;
		m_nAfterExcludeCount=dlg.m_nAfterExcludeCount;
		m_nAfterIncludeCount=dlg.m_nAfterIncludeCount;
		
		AfxGetApp()->WriteProfileInt("Settings", "PrevExCount", m_nPrevExcludeCount);
		AfxGetApp()->WriteProfileInt("Settings", "PrevInCount", m_nPrevIncludeCount);
		AfxGetApp()->WriteProfileInt("Settings", "AfterExCount", m_nAfterExcludeCount);
		AfxGetApp()->WriteProfileInt("Settings", "AfterInCount", m_nAfterIncludeCount);
	}
	
}

BOOL CDCRScopeView::LoadData(CString strFile)
{
	if(GetDocument()->SetDataSheet(strFile))
	{
		DrawGraph();
		UpdateGridData();
		OnButtonCal();
	}
	else
	{
		AfxMessageBox("파일 열기를 실패 하였습니다.");
		return FALSE;
	}

	return TRUE;
}

void CDCRScopeView::OnAxisSet() 
{
	// TODO: Add your command handler code here
	m_axisDlg.SetDataSheet(GetDocument()->GetDataSheet());
	if(m_axisDlg.DoModal())
	{
		DrawGraph();
		UpdateGridData();
		OnButtonCal();

		m_wndGraph.InitSubset(0, m_axisDlg.m_Y1Color);
		m_wndGraph.InitSubset(1, m_axisDlg.m_Y2Color);

	}
	
}

void CDCRScopeView::OnUpdateAxisSet(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!GetDocument()->GetDataFileName().IsEmpty());
}

void CDCRScopeView::OnEditCopy() 
{
	// TODO: Add your command handler code here
//	m_wndGraph.CopyImg();
}

void CDCRScopeView::OnGraphSet() 
{
	// TODO: Add your command handler code here
	m_wndGraph.CustomizeDialog();
}

void CDCRScopeView::OnMaximize() 
{
	// TODO: Add your command handler code here
	m_wndGraph.SetMaximize();
}

void CDCRScopeView::OnBntUnzoom() 
{
	// TODO: Add your control notification handler code here

//	DrawGraph();
//	UpdateGridData();
	m_wndGraph.SetUnZoom();
}

void CDCRScopeView::OnBntZoom() 
{
	// TODO: Add your control notification handler code here
	//m_wndGraph.SetZoom(m_fZoomTime1,m_fZoomTime2);
	m_wndGraph.SetZoom(m_fZoomTime1,m_fZoomTime2);

}
