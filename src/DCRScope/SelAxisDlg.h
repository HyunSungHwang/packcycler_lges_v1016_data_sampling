#if !defined(AFX_SELAXISDLG_H__4443DF9D_8667_4FDC_9F14_DDEF1123294A__INCLUDED_)
#define AFX_SELAXISDLG_H__4443DF9D_8667_4FDC_9F14_DDEF1123294A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelAxisDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelAxisDlg dialog
#include "TextParsing.h"
#include "ColorBox.h"

class CSelAxisDlg : public CDialog
{
// Construction
public:
	int m_nXAxis;
	CWordArray * GetSelYAxis();
	int GetSelXAxis();
	void SetDataSheet(CTextParsing *pDataSheet);
	CSelAxisDlg(CWnd* pParent = NULL);   // standard constructor
	COLORREF m_Y1Color;
	COLORREF m_Y2Color;
// Dialog Data
	//{{AFX_DATA(CSelAxisDlg)
	enum { IDD = IDD_SELECT_ITEM_DLG };
	CColorBox	m_Y2ColorBtn;
	CColorBox	m_Y1ColorBtn;
	CComboBox	m_ctrlY2Axis;
	CComboBox	m_ctrlY1Axis;
	CCheckListBox	m_ctrlYAxis;
	CComboBox	m_ctrlXAxis;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelAxisDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CWordArray m_aSelYAxis;
	CTextParsing	*m_pDataSheet;
	// Generated message map functions
	//{{AFX_MSG(CSelAxisDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeComboX();
	afx_msg void OnButtonY1Color();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELAXISDLG_H__4443DF9D_8667_4FDC_9F14_DDEF1123294A__INCLUDED_)
