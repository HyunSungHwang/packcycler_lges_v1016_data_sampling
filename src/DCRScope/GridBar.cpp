// gridbar.cpp : implementation file
//

#include "stdafx.h"
#include "gridbar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGridBar

CGridBar::CGridBar()
{
}

CGridBar::~CGridBar()
{
}


BEGIN_MESSAGE_MAP(CGridBar, baseCMyBar)
	//{{AFX_MSG_MAP(CGridBar)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CGridBar message handlers

int CGridBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (baseCMyBar::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetSCBStyle(GetSCBStyle() | SCBS_SIZECHILD);

/*	if (!m_wndChild.Create(WS_CHILD|WS_VISIBLE|
			ES_MULTILINE|ES_WANTRETURN|ES_AUTOVSCROLL,
		CRect(0,0,0,0), this, 123))
		return -1;

	m_wndChild.ModifyStyleEx(0, WS_EX_CLIENTEDGE);
*/
	if (!m_GridWnd.Create(WS_CHILD|WS_VISIBLE|ES_AUTOVSCROLL,
		CRect(0,0,0,0), this, 123))
		return -1;

	m_GridWnd.ModifyStyleEx(0, WS_EX_CLIENTEDGE);

	m_GridWnd.Initialize();
//	m_GridWnd.SetRowCount(100);
//	m_GridWnd.SetColCount(10);

	// older versions of Windows* (NT 3.51 for instance)
	// fail with DEFAULT_GUI_FONT
	if (!m_font.CreateStockObject(DEFAULT_GUI_FONT))
		if (!m_font.CreatePointFont(80, "MS Sans Serif"))
			return -1;

	m_GridWnd.SetFont(&m_font);
	m_GridWnd.GetParam()->EnableUndo(FALSE);
	return 0;
}

CWnd * CGridBar::GetClientWnd()
{
	return &m_GridWnd;
}

void CGridBar::SelectDataRange(int nStartIndex, int nEndIndex, BOOL bReset)
{
	int nRowCount = m_GridWnd.GetRowCount();

	if(nStartIndex < 0)			nStartIndex	 = 0;
	if(nEndIndex >= nRowCount)	nEndIndex = nRowCount-1;

	//Reset Selection
	if(bReset)
	{
		m_GridWnd.SelectRange(CGXRange().SetTable(), FALSE);
	}

	//Select Range
	if(nStartIndex <= nEndIndex)
		m_GridWnd.SelectRange(CGXRange().SetCells(nStartIndex+1, 1, nEndIndex+1, m_GridWnd.GetColCount()));
	else 
		return;

	m_GridWnd.ScrollCellInView(nStartIndex+1, 1);
}

void CGridBar::DisplaySheetData(CTextParsing *pSheet)
{
	ASSERT(pSheet);
//  Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
// 	CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
// 	CProgressCtrl ProgressCtrl;
// 	CRect rect;
// 	pStatusBar->GetItemRect(0,rect);
// 	rect.left = rect.Width()/2;
// 	ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
// 	ProgressCtrl.SetRange(0, 100);
// 	int nPos = 0;
// 	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("sheet loading...");

	m_GridWnd.SetColCount(pSheet->GetColumnSize());
	m_GridWnd.SetRowCount(pSheet->GetRecordCount());

	for(int c =0; c<pSheet->GetColumnSize(); c++)
	{
		m_GridWnd.SetValueRange(CGXRange(0, c+1), pSheet->GetColumnHeader(c));
	}

	for(int r=0; r<pSheet->GetRecordCount(); r++)
	{
		for(int c =0; c<pSheet->GetColumnSize(); c++)
		{
			float *fData = pSheet->GetColumnData(c);
			m_GridWnd.SetValueRange(CGXRange(r+1, c+1), fData[r] / 1000.0f);
		}
	}

// 	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");	
}

