// DCRScope.h : main header file for the DCRScope application
//

#if !defined(AFX_DCRScope_H__054C483F_F8CE_4B58_B073_548FC746C9F8__INCLUDED_)
#define AFX_DCRScope_H__054C483F_F8CE_4B58_B073_548FC746C9F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeApp:
// See DCRScope.cpp for the implementation of this class
//

class CDCRScopeApp : public CWinApp
{
public:
	CDCRScopeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCRScopeApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CDCRScopeApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCRScope_H__054C483F_F8CE_4B58_B073_548FC746C9F8__INCLUDED_)
