#if !defined(AFX_DCRSETDLG_H__21E39E75_9EDB_494B_8E3A_A9AE5F25E849__INCLUDED_)
#define AFX_DCRSETDLG_H__21E39E75_9EDB_494B_8E3A_A9AE5F25E849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DcrSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDcrSetDlg dialog

class CDcrSetDlg : public CDialog
{
// Construction
public:
	CDcrSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDcrSetDlg)
	enum { IDD = IDD_DCR_DIALOG };
	int		m_nPrevExcludeCount;
	int		m_nPrevIncludeCount;
	int		m_nAfterExcludeCount;
	int		m_nAfterIncludeCount;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDcrSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDcrSetDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCRSETDLG_H__21E39E75_9EDB_494B_8E3A_A9AE5F25E849__INCLUDED_)
