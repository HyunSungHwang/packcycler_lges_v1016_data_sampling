// DcrSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DCRScope.h"
#include "DcrSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDcrSetDlg dialog


CDcrSetDlg::CDcrSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDcrSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDcrSetDlg)
	m_nPrevExcludeCount = 0;
	m_nPrevIncludeCount = 100;
	m_nAfterExcludeCount = 0;
	m_nAfterIncludeCount = 100;
	//}}AFX_DATA_INIT
}

void CDcrSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDcrSetDlg)
	DDX_Text(pDX, IDC_EDIT3, m_nPrevExcludeCount);
	DDX_Text(pDX, IDC_EDIT1, m_nPrevIncludeCount);
	DDX_Text(pDX, IDC_EDIT4, m_nAfterExcludeCount);
	DDX_Text(pDX, IDC_EDIT2, m_nAfterIncludeCount);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDcrSetDlg, CDialog)
	//{{AFX_MSG_MAP(CDcrSetDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDcrSetDlg message handlers
