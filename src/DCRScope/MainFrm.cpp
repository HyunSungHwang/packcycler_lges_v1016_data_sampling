// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "DCRScope.h"

#include "MainFrm.h"
#include "DCRScopeView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_DROPFILES()
	ON_COMMAND(ID_SHOW_DATA_SHEET, OnShowDataSheet)
	ON_UPDATE_COMMAND_UI(ID_SHOW_DATA_SHEET, OnUpdateShowDataSheet)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	EnableDocking(CBRS_ALIGN_ANY);

	//Bottom Grid Bar
	if (!m_wndGridBar.Create("DataSheet", this, 2002))
	{
		TRACE0("Failed to create dialog bar m_wndGridBar\n");
		return -1;		// fail to create
	}
	m_wndGridBar.SetBarStyle(m_wndGridBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndGridBar.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndGridBar, AFX_IDW_DOCKBAR_BOTTOM);

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	CString sProfile = _T("BarState");
	if (VerifyBarState(sProfile))
	{
		CSizingControlBar::GlobalLoadState(this, sProfile);
		LoadBarState(sProfile);
	}


	DragAcceptFiles();

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.style &= ~FWS_ADDTOTITLE;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


BOOL CMainFrame::OpenCsvFile(CString strFile)
{
	CDCRScopeView *pView;// = (CDCRScopeView *)GetActiveView;
	CFrameWnd *pWnd = GetActiveFrame();
	pView = (CDCRScopeView *)pWnd->GetActiveView();
	return pView->LoadData(strFile);
}

void CMainFrame::OnDropFiles(HDROP hDropInfo) 
{
	// TODO: Add your message handler code here and/or call default
	char str[_MAX_PATH] ={0};
	int cnt = 0xFFFFFFFF;
	//Drop된 파일의 수을 구한다.
	::DragQueryFile(hDropInfo, cnt, (LPTSTR)str, _MAX_PATH);

	if(cnt > 0)
	{
		//제일 첫번째 파일만 사용한다.
		ZeroMemory(str, _MAX_PATH);
		::DragQueryFile(hDropInfo, 0, (LPTSTR)str, _MAX_PATH); 
		CString strTemp, strFileName;
		strFileName = str;

		strTemp = strFileName.Mid(strFileName.ReverseFind('.')+1);
		strTemp.MakeLower();

		if(strTemp == "csv")
		{
			OpenCsvFile(strFileName);
		}
		else
		{
			//AfxMessageBox("지원되지 않는 파일 형식입니다.");
			AfxMessageBox("An unsupported file format.");
			return;
		}
	}
	::DragFinish(hDropInfo);	
//	CFrameWnd::OnDropFiles(hDropInfo);
}

BOOL CMainFrame::VerifyBarState(LPCTSTR lpszProfileName)
{
    CDockState state;
    state.LoadState(lpszProfileName);

    for (int i = 0; i < state.m_arrBarInfo.GetSize(); i++)
    {
        CControlBarInfo* pInfo = (CControlBarInfo*)state.m_arrBarInfo[i];
        ASSERT(pInfo != NULL);
        int nDockedCount = pInfo->m_arrBarID.GetSize();
        if (nDockedCount > 0)
        {
            // dockbar
            for (int j = 0; j < nDockedCount; j++)
            {
                UINT nID = (UINT) pInfo->m_arrBarID[j];
                if (nID == 0) continue; // row separator
                if (nID > 0xFFFF)
                    nID &= 0xFFFF; // placeholder - get the ID
                if (GetControlBar(nID) == NULL)
                    return FALSE;
            }
        }
        
        if (!pInfo->m_bFloating) // floating dockbars can be created later
            if (GetControlBar(pInfo->m_nBarID) == NULL)
                return FALSE; // invalid bar ID
    }

    return TRUE;
}

BOOL CMainFrame::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	CString sProfile = _T("BarState");
	CSizingControlBar::GlobalSaveState(this, sProfile);
	SaveBarState(sProfile);

	return CFrameWnd::DestroyWindow();
}

void CMainFrame::OnShowDataSheet() 
{
	// TODO: Add your command handler code here
	BOOL bShow = m_wndGridBar.IsVisible();
	ShowControlBar(&m_wndGridBar, !bShow, FALSE);
}

void CMainFrame::OnUpdateShowDataSheet(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_wndGridBar.IsVisible());		
}

BOOL CMainFrame::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	return CFrameWnd::OnEraseBkgnd(pDC);
}
