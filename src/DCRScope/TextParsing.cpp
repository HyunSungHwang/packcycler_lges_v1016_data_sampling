// TextParsing.cpp: implementation of the CTextParsing class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DCRScope.h"
#include "TextParsing.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTextParsing::CTextParsing()
{
	m_nRecordCount = 0;
	m_nColumnSize = 0;
	m_ppData = NULL;
}

CTextParsing::~CTextParsing()
{
	if(m_ppData != NULL)
	{
		for(int i=0; i<m_nColumnSize; i++)
		{
			delete[] m_ppData[i];
		}
		delete[]	m_ppData; 
	}
}

BOOL CTextParsing::SetFile(CString strFileName)
{
// 	float	fMaxTime = (float)atof(AfxGetApp()->GetProfileString("Settings", "MaxTime", "0.30"));
	float   fTime= 0;
	
	if(m_ppData)
	{
		for(int i=0; i<m_nColumnSize; i++)
		{
			if(m_ppData[i] != NULL)
			{
				delete[] m_ppData[i];
				m_ppData[i] = NULL;
			}
		}
		delete[]	m_ppData; 
		m_ppData = NULL;
		m_TitleList.RemoveAll();
	}

	CString strTime, strTemp, str;
	CStdioFile aFile;
	CFileException e;

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   return FALSE;
	}

	int iLineNum=0;	//ljb 201167 처음 5줄 버림

	CStringList		strDataList;
	while(aFile.ReadString(strTemp))
	{
		iLineNum++;
		if(!strTemp.IsEmpty())	
		{
			if (iLineNum > 5)
			{
				strTime = strTemp.Left(strTemp.Find(','));
				fTime = atof(strTime);
//				if (fMaxTime < fTime) break;
			}

			strDataList.AddTail(strTemp);
		}
	}
	aFile.Close();
	
	m_nRecordCount = strDataList.GetCount()-1;
	if(m_nRecordCount < 0)		return FALSE;

	int p1=0, s=0;
	POSITION pos = strDataList.GetHeadPosition();

	strTemp = strDataList.GetNext(pos);		//Title
	while(p1!=-1)
	{
		p1 = strTemp.Find(',', s);
		if(p1!=-1)
		{
			str = strTemp.Mid(s, p1-s);
			m_TitleList.AddTail(str);
			s  = p1+1;
		}
	}
	str = strTemp.Mid(s);
	str.TrimLeft(' '); str.TrimRight(' ');
	if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);

	m_nColumnSize = m_TitleList.GetCount();

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}
	
	CStringList strColList;
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				strColList.AddTail(str);
				s  = p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);

		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < m_nColumnSize)
		{
			str = strColList.GetNext(pos1);
			m_ppData[nC][nRecord] = atof(str) * 1000.0f;		//ljb * 1000 추가함  [10/6/2011 XNOTE]
			nC++;
		}
		nRecord++;
	}
	return TRUE;
}

float * CTextParsing::GetColumnData(int nColIndex)
{
	if(nColIndex< 0 || nColIndex >= m_nColumnSize)		return NULL;

	return m_ppData[nColIndex];
}

CString CTextParsing::GetColumnHeader(int nColIndex)
{
	if(nColIndex< 0 || nColIndex >= m_nColumnSize)		return "";

	POSITION pos = m_TitleList.GetHeadPosition();
	CString str;
	int nCnt = 0;
	while(pos)
	{
		str = m_TitleList.GetNext(pos);	
		if(nCnt == nColIndex)
		{
			break;
		}
		nCnt++;
	}
	return str;
}
