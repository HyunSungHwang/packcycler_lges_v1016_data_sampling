#if !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GraphWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeWnd window
#include "../../include/PEGRPAPI.h"
#include "../../include/pemessag.h"

#define PE_MAX_SUBSET	6
#define NULL_DATA		999999999

#define ANNOTATION_COLOR	RGB(0,200,0)

class CDCRScopeWnd : public CStatic
{
// Construction
public:
	CDCRScopeWnd();

// Attributes
public:
	HWND	m_hWndPE;
	int		m_SetsetNum;
	int		m_TotalPoint;
	int		m_ScreenPoint;
	int		m_Delay;
	COLORREF	m_YColor[PE_MAX_SUBSET];
	unsigned long *		m_TimerPtr;
	unsigned long	m_OldTime;

	int			m_TimerID;
	float *		m_pData[PE_MAX_SUBSET];
	int			m_nDataPointCount[PE_MAX_SUBSET];
	BOOL		m_bShowArray[PE_MAX_SUBSET];

	time_t		m_StartTime;
	int			m_Count;

// Operations
public:
	void SetN(UINT type, INT value);
	void SetV(UINT type, VOID FAR * lpData, UINT nItems);
	void SetSZ(UINT type, CHAR FAR *str);
	void SetVCell(UINT type, UINT nCell, VOID FAR * lpData);

	void InitSubset(int subID, COLORREF color, LPCTSTR title = NULL, LPCTSTR yname = NULL);
	void ShowSubset(int id, BOOL bShow);
	void SetSplit(BOOL bSplit);
	void Start();
	void Stop();
	void SetDataPtr(int id, int nCnt, float *fXData, float *fYData);
	void ClearGraph();
	void Print();
	void SetData(int subset, int pos, LPCTSTR xData, double yData);
	void SetMaximize();
	void SetZoom(double dTime1, double dTime2);
	void SetUnZoom();

	virtual BOOL OnCommand( WPARAM wParam, LPARAM lParam);

protected:
//	CString		m_strMainTitle;
//	CString		m_strSubTitle;
//	CString		m_strXName;

	void SetGlobalPEStuff();
	double		m_dMin[PE_MAX_SUBSET], m_dMax[PE_MAX_SUBSET];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCRScopeWnd)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	void CustomizeDialog();
//	void CopyImg();
	void AddLineAnnotation(int nIndex, double fStartX, double fStartY, double  fEndX, double fEndY, CString str);
	void AddXAnnotation(double fXData1, double fXData2, CString str1, CString str2);
	void SaveGraph();
	void SetSubTitle(CString str);
	void SetMainTitle(CString str);
	void SetAnnotation(BOOL bEnable);
	void SetDataTitle(int nSubID, CString str);
	void SetYAxisLabel(int subID, CString str);
	void SetXAxisLabel(CString str);
	virtual ~CDCRScopeWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDCRScopeWnd)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
