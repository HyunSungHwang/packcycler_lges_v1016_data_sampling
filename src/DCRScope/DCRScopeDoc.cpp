// DCRScopeDoc.cpp : implementation of the CDCRScopeDoc class
//

#include "stdafx.h"
#include "DCRScope.h"

#include "DCRScopeDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeDoc

IMPLEMENT_DYNCREATE(CDCRScopeDoc, CDocument)

BEGIN_MESSAGE_MAP(CDCRScopeDoc, CDocument)
	//{{AFX_MSG_MAP(CDCRScopeDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeDoc construction/destruction

CDCRScopeDoc::CDCRScopeDoc()
{
	// TODO: add one-time construction code here

}

CDCRScopeDoc::~CDCRScopeDoc()
{
}

BOOL CDCRScopeDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CDCRScopeDoc serialization

void CDCRScopeDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeDoc diagnostics

#ifdef _DEBUG
void CDCRScopeDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDCRScopeDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeDoc commands

CTextParsing * CDCRScopeDoc::GetDataSheet()
{
	return &m_SheetData;
}

BOOL CDCRScopeDoc::SetDataSheet(CString strFile)
{
	CTextParsing *pSheet = GetDataSheet();
	
	if(pSheet->SetFile(strFile))
	{
		m_strDataFileName = strFile;
		return TRUE;
	}
	return FALSE;
}


CString CDCRScopeDoc::GetDataFileName()
{
	return m_strDataFileName;
}
