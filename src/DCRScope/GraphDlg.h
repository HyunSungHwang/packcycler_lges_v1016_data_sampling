#if !defined(AFX_GRAPHDLG_H__382ED899_973E_4DCB_BAA2_C6857D188866__INCLUDED_)
#define AFX_GRAPHDLG_H__382ED899_973E_4DCB_BAA2_C6857D188866__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GraphDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DCRScopeDlg dialog
#include "GraphWnd1.h"
class DCRScopeDlg : public CDialog
{
// Construction
public:
	DCRScopeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DCRScopeDlg)
	enum { IDD = IDD_DIALOG1 };
	int		m_nPrevExcludeCout;
	int		m_nPrevIncludeCount;
	CString	m_nAfterExcludeCount;
	int		m_nAfterIncludeCount;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DCRScopeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	DCRScopeWnd m_wndGraph;
	// Generated message map functions
	//{{AFX_MSG(DCRScopeDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHDLG_H__382ED899_973E_4DCB_BAA2_C6857D188866__INCLUDED_)
