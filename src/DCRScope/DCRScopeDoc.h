// DCRScopeDoc.h : interface of the CDCRScopeDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DCRScopeDOC_H__D49FC96C_2750_4BB1_90DC_98166C5C3485__INCLUDED_)
#define AFX_DCRScopeDOC_H__D49FC96C_2750_4BB1_90DC_98166C5C3485__INCLUDED_

#include "TextParsing.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CDCRScopeDoc : public CDocument
{
protected: // create from serialization only
	CDCRScopeDoc();
	DECLARE_DYNCREATE(CDCRScopeDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCRScopeDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	CString GetDataFileName();
	CString m_strDataFileName;
	BOOL SetDataSheet(CString strFile);
	CTextParsing * GetDataSheet();
	CTextParsing m_SheetData;
	virtual ~CDCRScopeDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDCRScopeDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCRScopeDOC_H__D49FC96C_2750_4BB1_90DC_98166C5C3485__INCLUDED_)
