; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDcrSetDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DCRScope.h"
LastPage=0

ClassCount=15
Class1=CDCRScopeApp
Class2=CAboutDlg
Class3=CDCRScopeDoc
Class4=CDCRScopeView
Class5=DCRScopeDlg
Class6=DCRScopeWnd
Class7=CGridBar
Class8=CMainFrame
Class9=CMyGridWnd
Class10=CSizingControlBarCF
Class11=CSizingControlBarG
Class12=CSelAxisDlg
Class13=CSizingControlBar
Class14=CSCBMiniDockFrameWnd

ResourceCount=5
Resource1=IDR_MAINFRAME
Resource2=IDD_ABOUTBOX
Resource3=IDD_DCRScope_FORM
Resource4=IDD_SELECT_ITEM_DLG
Class15=CDcrSetDlg
Resource5=IDD_DCR_DIALOG

[CLS:CDCRScopeApp]
Type=0
BaseClass=CWinApp
HeaderFile=DCRScope.h
ImplementationFile=DCRScope.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=DCRScope.cpp
ImplementationFile=DCRScope.cpp
LastObject=CAboutDlg

[CLS:CDCRScopeDoc]
Type=0
BaseClass=CDocument
HeaderFile=DCRScopeDoc.h
ImplementationFile=DCRScopeDoc.cpp

[CLS:CDCRScopeView]
Type=0
BaseClass=CFormView
HeaderFile=DCRScopeView.h
ImplementationFile=DCRScopeView.cpp
Filter=D
VirtualFilter=VWC
LastObject=CDCRScopeView

[CLS:DCRScopeDlg]
Type=0
BaseClass=CDialog
HeaderFile=GraphDlg.h
ImplementationFile=GraphDlg.cpp

[CLS:DCRScopeWnd]
Type=0
BaseClass=CStatic
HeaderFile=GraphWnd.h
ImplementationFile=GraphWnd.cpp

[CLS:CGridBar]
Type=0
BaseClass=baseCMyBar
HeaderFile=GridBar.h
ImplementationFile=GridBar.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
LastObject=CMainFrame
Filter=T
VirtualFilter=fWC

[CLS:CMyGridWnd]
Type=0
BaseClass=CGXGridWnd
HeaderFile=MyGridWnd.h
ImplementationFile=MyGridWnd.cpp

[CLS:CSizingControlBarCF]
Type=0
BaseClass=baseCSizingControlBarCF
HeaderFile=scbarcf.h
ImplementationFile=scbarcf.cpp

[CLS:CSizingControlBarG]
Type=0
BaseClass=baseCSizingControlBarG
HeaderFile=scbarg.h
ImplementationFile=scbarg.cpp

[CLS:CSelAxisDlg]
Type=0
BaseClass=CDialog
HeaderFile=SelAxisDlg.h
ImplementationFile=SelAxisDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_BUTTON_Y1_COLOR

[CLS:CSizingControlBar]
Type=0
BaseClass=baseCSizingControlBar
HeaderFile=sizecbar.h
ImplementationFile=sizecbar.cpp

[CLS:CSCBMiniDockFrameWnd]
Type=0
BaseClass=baseCSCBMiniDockFrameWnd
HeaderFile=sizecbar.h
ImplementationFile=sizecbar.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DCRScope_FORM]
Type=1
Class=CDCRScopeView
ControlCount=31
Control1=IDC_STATIC_GRAPH,static,1342308865
Control2=IDC_STATIC,static,1073872896
Control3=IDC_EDIT1,edit,1082196098
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EDIT2,edit,1350631554
Control6=IDC_BUTTON_CAL,button,1342242816
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308354
Control10=IDC_STATIC,static,1342308354
Control11=IDC_CHECK,button,1342242819
Control12=IDC_EDIT_V1,edit,1350633602
Control13=IDC_EDIT_V2,edit,1350633602
Control14=IDC_EDIT_I1,edit,1350633602
Control15=IDC_EDIT_I2,edit,1350633602
Control16=IDC_STATIC,static,1342308352
Control17=IDC_EDIT_R,edit,1350633602
Control18=IDC_STATIC,button,1342177287
Control19=IDC_STATIC,static,1342177297
Control20=IDC_CUSTOM_GRID,GXWND,1082195968
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352
Control23=IDC_STATIC,static,1342308352
Control24=IDC_STATIC,static,1342308352
Control25=IDC_STATIC,static,1342308352
Control26=IDC_STATIC,static,1073872896
Control27=IDC_EDIT_T1,edit,1082196098
Control28=IDC_STATIC,static,1073872896
Control29=IDC_EDIT_T2,edit,1082196098
Control30=IDC_BNT_ZOOM,button,1342242816
Control31=IDC_BNT_UNZOOM,button,1073807360

[DLG:IDD_DIALOG1]
Type=1
Class=DCRScopeDlg

[DLG:IDD_SELECT_ITEM_DLG]
Type=1
Class=CSelAxisDlg
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308864
Control4=IDC_COMBO_X,combobox,1344339971
Control5=IDC_STATIC,static,1342308864
Control6=IDC_LIST1,listbox,1084309841
Control7=IDC_STATIC,static,1342308864
Control8=IDC_COMBO_Y1,combobox,1344339971
Control9=IDC_COMBO_Y2,combobox,1344339971
Control10=IDC_BUTTON_Y1_COLOR,button,1342242827
Control11=IDC_BUTTON_Y2_COLOR,button,1342242827

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_AXIS_SET
Command4=ID_GRAPH_SET
Command5=ID_MAXIMIZE
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_FILE_PRINT
Command10=ID_APP_ABOUT
CommandCount=10

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_PRINT
Command4=ID_FILE_PRINT_SETUP
Command5=ID_APP_EXIT
Command6=ID_DCR_SETTING
Command7=ID_AXIS_SET
Command8=ID_GRAPH_SET
Command9=ID_SHOW_DATA_SHEET
Command10=ID_MAXIMIZE
Command11=ID_VIEW_TOOLBAR
Command12=ID_VIEW_STATUS_BAR
Command13=ID_APP_ABOUT
CommandCount=13

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_DCR_DIALOG]
Type=1
Class=CDcrSetDlg
ControlCount=13
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1073872896
Control3=IDC_EDIT3,edit,1082196098
Control4=IDC_STATIC,static,1073872896
Control5=IDC_EDIT1,edit,1082196098
Control6=IDC_STATIC,static,1073872896
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT4,edit,1082196098
Control9=IDC_STATIC,static,1073872896
Control10=IDC_EDIT2,edit,1350631554
Control11=IDC_STATIC,static,1342308352
Control12=IDOK,button,1342242817
Control13=IDCANCEL,button,1342242816

[CLS:CDcrSetDlg]
Type=0
HeaderFile=DcrSetDlg.h
ImplementationFile=DcrSetDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CDcrSetDlg
VirtualFilter=dWC

