#if !defined(__GRIDBAR_H__)
#define __GRIDBAR_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// gridbar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGridBar window

#ifndef baseCMyBar
#define baseCMyBar CSizingControlBarG
#endif

#include "MyGridWnd.h"
#include "TextParsing.h"
class CGridBar : public baseCMyBar
{
// Construction
public:
	CGridBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	void DisplaySheetData(CTextParsing *pSheet);
	void SelectDataRange(int nStartIndex, int nEndIndex, BOOL bReset = TRUE);
	CWnd * GetClientWnd();
	virtual ~CGridBar();

protected:
	CMyGridWnd m_GridWnd;
	CEdit	m_wndChild;
	CFont	m_font;

	// Generated message map functions
protected:
	//{{AFX_MSG(CGridBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(__GRIDBAR_H__)
