// GraphDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DCRScope.h"
#include "GraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DCRScopeDlg dialog


DCRScopeDlg::DCRScopeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DCRScopeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DCRScopeDlg)
	m_nPrevExcludeCout = 0;
	m_nPrevIncludeCount = 0;
	m_nAfterExcludeCount = _T("");
	m_nAfterIncludeCount = 0;
	//}}AFX_DATA_INIT
}


void DCRScopeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DCRScopeDlg)
	DDX_Text(pDX, IDC_EDIT3, m_nPrevExcludeCout);
	DDX_Text(pDX, IDC_EDIT1, m_nPrevIncludeCount);
	DDX_Text(pDX, IDC_EDIT4, m_nAfterExcludeCount);
	DDX_Text(pDX, IDC_EDIT2, m_nAfterIncludeCount);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DCRScopeDlg, CDialog)
	//{{AFX_MSG_MAP(DCRScopeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DCRScopeDlg message handlers

BOOL DCRScopeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_wndGraph.SubclassDlgItem(IDC_GP_STATIC, this);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
