//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DCRScope.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_DCRScope_FORM               101
#define IDR_MAINFRAME                   128
#define IDR_DCRScopeTYPE                129
#define IDD_SELECT_ITEM_DLG             131
#define IDI_ICON1                       132
#define IDD_DCR_DIALOG                  133
#define IDC_COMBO_X                     1003
#define IDC_LIST1                       1005
#define IDC_STATIC_GRAPH                1006
#define IDC_EDIT1                       1007
#define IDC_EDIT2                       1008
#define IDC_BUTTON_CAL                  1009
#define IDC_EDIT_T1                     1010
#define IDC_CHECK                       1011
#define IDC_EDIT_V1                     1012
#define IDC_EDIT_V2                     1013
#define IDC_EDIT_I1                     1014
#define IDC_EDIT_I2                     1015
#define IDC_EDIT_R                      1016
#define IDC_CUSTOM_GRID                 1017
#define IDC_COMBO_Y1                    1018
#define IDC_EDIT_T2                     1018
#define IDC_COMBO_Y2                    1019
#define IDC_BUTTON_Y1_COLOR             1021
#define IDC_EDIT3                       1021
#define IDC_BUTTON_Y2_COLOR             1022
#define IDC_EDIT4                       1022
#define IDC_BNT_ZOOM                    1022
#define IDC_BNT_UNZOOM                  1023
#define ID_SHOW_DATA_SHEET              32771
#define ID_DCR_SETTING                  32772
#define ID_AXIS_SET                     32773
#define ID_GRAPH_SET                    32775
#define ID_MAXIMIZE                     32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
