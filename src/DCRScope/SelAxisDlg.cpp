// SelAxisDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DCRScope.h"
#include "SelAxisDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelAxisDlg dialog


CSelAxisDlg::CSelAxisDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelAxisDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelAxisDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDataSheet = NULL;
	m_nXAxis = atol(AfxGetApp()->GetProfileString("Settings", "XAxis", "0"));	
	CString strSelYAxis = AfxGetApp()->GetProfileString("Settings", "YAxis", "1,2,");	//

	int	p1=0, s=0;
	while(p1!=-1)
	{
		p1 = strSelYAxis.Find(',', s);
		if(p1!=-1)
		{
			m_aSelYAxis.Add(atol(strSelYAxis.Mid(s, p1-s)));
			s  = p1+1;
		}
	}

	m_Y1Color = AfxGetApp()->GetProfileInt("Settings", "Y1Color", RGB(230,  50,  0));
	m_Y2Color = AfxGetApp()->GetProfileInt("Settings", "Y2Color", RGB(  35, 50,200));
}


void CSelAxisDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelAxisDlg)
	DDX_Control(pDX, IDC_BUTTON_Y2_COLOR, m_Y2ColorBtn);
	DDX_Control(pDX, IDC_BUTTON_Y1_COLOR, m_Y1ColorBtn);
	DDX_Control(pDX, IDC_COMBO_Y2, m_ctrlY2Axis);
	DDX_Control(pDX, IDC_COMBO_Y1, m_ctrlY1Axis);
	DDX_Control(pDX, IDC_LIST1, m_ctrlYAxis);
	DDX_Control(pDX, IDC_COMBO_X, m_ctrlXAxis);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelAxisDlg, CDialog)
	//{{AFX_MSG_MAP(CSelAxisDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_X, OnSelchangeComboX)
	ON_BN_CLICKED(IDC_BUTTON_Y1_COLOR, OnButtonY1Color)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelAxisDlg message handlers

void CSelAxisDlg::SetDataSheet(CTextParsing *pDataSheet)
{
	m_pDataSheet = pDataSheet;
}

BOOL CSelAxisDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	ASSERT(m_pDataSheet);

	for(int i=0; i<m_pDataSheet->GetColumnSize(); i++)
	{
		m_ctrlXAxis.AddString(m_pDataSheet->GetColumnHeader(i));
	}
	m_ctrlXAxis.SetCurSel(m_nXAxis);
	OnSelchangeComboX();

	m_Y1ColorBtn.SetColor(m_Y1Color);
	m_Y2ColorBtn.SetColor(m_Y2Color);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelAxisDlg::OnOK() 
{
	// TODO: Add extra validation here
	BOOL bCheck = FALSE;
	m_aSelYAxis.RemoveAll();

/*	for(int i=0; i<m_ctrlYAxis.GetCount(); i++)
	{
		if(m_ctrlYAxis.GetCheck(i) == 1)
		{
			m_aSelYAxis.Add(m_ctrlYAxis.GetItemData(i));
		}
	}
*/

	int nSel = m_ctrlY1Axis.GetCurSel();
	if(nSel > 0)
	{
		m_aSelYAxis.Add( m_ctrlY1Axis.GetItemData(nSel));
	}

	nSel = m_ctrlY2Axis.GetCurSel();
	if(nSel > 0)
	{
		m_aSelYAxis.Add( m_ctrlY2Axis.GetItemData(nSel));
	}

	if(m_aSelYAxis.GetSize() < 1)
	{
		MessageBox("Y-축에 표현할 아이템을 선택해 주십시오", "Y-축 아이템 선택", MB_ICONEXCLAMATION);
		return;
	}

	m_nXAxis = m_ctrlXAxis.GetCurSel();

	m_Y1Color = m_Y1ColorBtn.GetColor();
	m_Y2Color = m_Y2ColorBtn.GetColor();
	AfxGetApp()->WriteProfileInt("Settings", "Y1Color", m_Y1Color);
	AfxGetApp()->WriteProfileInt("Settings", "Y2Color", m_Y2Color);

	CString strTemp;
	strTemp.Format("%d", m_nXAxis);
	AfxGetApp()->WriteProfileString("Settings", "XAxis", strTemp);	
	
	CString strY;
	for(int i=0; i<m_aSelYAxis.GetSize(); i++)
	{
		strTemp.Format("%d,", m_aSelYAxis.GetAt(i));
		strY += strTemp;
	}
	AfxGetApp()->WriteProfileString("Settings", "YAxis", strY);	//

	CDialog::OnOK();
}

void CSelAxisDlg::OnSelchangeComboX() 
{
	// TODO: Add your control notification handler code here

	m_ctrlYAxis.ResetContent();
	m_ctrlY1Axis.ResetContent();
	m_ctrlY2Axis.ResetContent();
	m_ctrlY1Axis.AddString("=====");
	m_ctrlY1Axis.SetItemData(0, 0xFFFFFFFF);
	m_ctrlY2Axis.AddString("=====");
	m_ctrlY2Axis.SetItemData(0, 0xFFFFFFFF);

	int nSel = m_ctrlXAxis.GetCurSel();
	if(nSel == CB_ERR)	return;

	int nIndex = 0;
	int nDefaultY1 = 0, nDefaultY2 = 0;
	for(int i=0; i<m_pDataSheet->GetColumnSize(); i++)
	{
		if(nSel != i)
		{
			m_ctrlYAxis.AddString(m_pDataSheet->GetColumnHeader(i));
			m_ctrlYAxis.SetItemData(nIndex, i);
			
			m_ctrlY1Axis.AddString(m_pDataSheet->GetColumnHeader(i));
			m_ctrlY1Axis.SetItemData(nIndex+1, i);
			if(m_aSelYAxis.GetSize() > 0)
			{
				if(m_aSelYAxis.GetAt(0) == i)
				{
					nDefaultY1 = nIndex+1;
				}
			}

			m_ctrlY2Axis.AddString(m_pDataSheet->GetColumnHeader(i));
			m_ctrlY2Axis.SetItemData(nIndex+1, i);
			if(m_aSelYAxis.GetSize() > 1)
			{
				if(m_aSelYAxis.GetAt(1) == i)
				{
					nDefaultY2 = nIndex+1;
				}
			}

			nIndex++;
		}
	}


	m_ctrlY1Axis.SetCurSel(nDefaultY1);
	m_ctrlY2Axis.SetCurSel(nDefaultY2);
}

int CSelAxisDlg::GetSelXAxis()
{
	return m_nXAxis;
}

CWordArray * CSelAxisDlg::GetSelYAxis()
{
	return &m_aSelYAxis;
}

void CSelAxisDlg::OnButtonY1Color() 
{
	// TODO: Add your control notification handler code here
	
}
