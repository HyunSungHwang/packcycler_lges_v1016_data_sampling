// DCRScopeView.h : interface of the CDCRScopeView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DCRScopeVIEW_H__AAD5096D_71C4_4730_8167_6420007DB789__INCLUDED_)
#define AFX_DCRScopeVIEW_H__AAD5096D_71C4_4730_8167_6420007DB789__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphWnd.h"
#include "DCRScopeDoc.h"
#include "MyGridWnd.h"
#include "SelAxisDlg.h"

#define LINEAR_DATA_POINT	100

class CDCRScopeView : public CFormView
{
protected: // create from serialization only
	CDCRScopeView();
	DECLARE_DYNCREATE(CDCRScopeView)

public:
	//{{AFX_DATA(CDCRScopeView)
	enum { IDD = IDD_DCRScope_FORM };
	float	m_fTime1;
	float	m_fTime2;
	float	m_fZoomTime1;
	float	m_fZoomTime2;
	BOOL	m_bUseFitLine;
	//}}AFX_DATA

// Attributes
public:
	CDCRScopeDoc* GetDocument();
	CDCRScopeWnd m_wndGraph;
	CMyGridWnd m_wndDataGrid;
	CSelAxisDlg m_axisDlg;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCRScopeView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL LoadData(CString strFile);
	void UpdateGridData();
	void InitGrid();
	int FindPointData(float fTime, float &fData1, float &fData2, BOOL bUseFitLine = FALSE, BOOL bUsePrevData = TRUE);
	BOOL DrawGraph();
	float m_afData[100];
	virtual ~CDCRScopeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	int m_nPrevExcludeCount;
	int	m_nPrevIncludeCount;
	int m_nAfterExcludeCount;
	int m_nAfterIncludeCount;

// Generated message map functions
protected:
	//{{AFX_MSG(CDCRScopeView)
	afx_msg void OnFileOpen();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonCal();
	afx_msg void OnFileSave();
	afx_msg void OnFilePrint();
	afx_msg void OnDestroy();
	afx_msg void OnDcrSetting();
	afx_msg void OnAxisSet();
	afx_msg void OnUpdateAxisSet(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnGraphSet();
	afx_msg void OnMaximize();
	afx_msg void OnBntUnzoom();
	afx_msg void OnBntZoom();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in DCRScopeView.cpp
inline CDCRScopeDoc* CDCRScopeView::GetDocument()
   { return (CDCRScopeDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCRScopeVIEW_H__AAD5096D_71C4_4730_8167_6420007DB789__INCLUDED_)
