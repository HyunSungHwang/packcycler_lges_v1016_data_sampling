// CTSEditorPro.h : main header file for the CTSEditorPro application
//

#if !defined(AFX_CTSEditorPro_H__B028797E_F79E_413D_8E99_C6CA24E4C7A6__INCLUDED_)
#define AFX_CTSEditorPro_H__B028797E_F79E_413D_8E99_C6CA24E4C7A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProApp:
// See CTSEditorPro.cpp for the implementation of this class
//

class CCTSEditorProApp : public CWinApp
{
public:
	CCTSEditorProApp();

	CString str_BTrim(CString str);
	CString str_GetFile(CString strPath,char ch='\\');
	CString str_GetPath(CString strPath,char ch='\\');
	CString str_GetFileExt(CString strName);
	CString str_IntToStr(int num,int pt=1);
	CString str_GetSplit(CString str,int count,char sp,BOOL bTrim=TRUE);
	int     str_Count(CString tmpstr,CString searchstr);
	
	CString win_GetText(HWND hWnd);
	CString win_GetText(CWnd *wnd);
	CString win_CurrentTime(CString sort);
	int     win_CboGetDataItemFind(CComboBox *cbo,int iVal,BOOL bFocus=FALSE);
	int     win_CboGetFind(CComboBox *cbo,CString str);
	CString win_CboGetString(CComboBox *cbo,CString blank=_T(""));

	BOOL    file_ForceDirectory(LPCTSTR lpDirectory);
	CString file_GetPath(LPCTSTR lpszFilePath);
	int     file_Exists(LPCTSTR lpszName);
	BOOL    file_DirectoryExists(LPCTSTR lpszDir);
	void    file_DeletefileList(CString path);
	
	CString GetValueStrf(float fvalue,int n=-1);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSEditorProApp)
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCTSEditorProApp)
	afx_msg void OnAppAbout();
	afx_msg void OnAppExit();

	afx_msg void OnMenuLangEng();
	afx_msg void OnMenuLangKor();
	afx_msg void OnUpdateLangEng(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLangKor(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CCTSEditorProApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSEditorPro_H__B028797E_F79E_413D_8E99_C6CA24E4C7A6__INCLUDED_)
