#if !defined(AFX_DLG_SOCTABLEEDIT_H__9BBC8826_F48A_40B8_9BB2_4FD134052CB7__INCLUDED_)
#define AFX_DLG_SOCTABLEEDIT_H__9BBC8826_F48A_40B8_9BB2_4FD134052CB7__INCLUDED_
//$1013
#include "MyGridWnd.h"
#include "GridComboBox.h"
#include "RealEditCtrl.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_SocTableEdit.h : header file
//

#define ID_SOCTABLE_COL_COUNT 3
#define ID_SOCTABLE_ROW_COUNT 20

#define ID_SOCTABLE_COL_NO    0
#define ID_SOCTABLE_COL_SOC   1
#define ID_SOCTABLE_COL_VOLT  2
#define ID_SOCTABLE_COL_CURR  3

/////////////////////////////////////////////////////////////////////////////
// Dlg_SocTableEdit dialog

class Dlg_SocTableEdit : public CDialog
{
// Construction
public:

	Dlg_SocTableEdit(CWnd* pParent = NULL);   // standard constructor

	BOOL m_bEditMode;
	CString m_strEditFileName;
	CMyGridWnd m_wndSocTableGrid;
	BOOL m_IsChange;
	
	BOOL InitSocTableGrid();
	void LoadData();
	void IsChangeFlag(BOOL IsFlag);
	void SetRowCount();

// Dialog Data
	//{{AFX_DATA(Dlg_SocTableEdit)
	enum { IDD = IDD_DIALOG_SOCTABLE_EDIT , IDD2 = IDD_DIALOG_SOCTABLE_EDIT_ENG , IDD3 = IDD_DIALOG_SOCTABLE_EDIT_PL  };
	CStatic	m_StaticSocTableCount;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_SocTableEdit)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_SocTableEdit)
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButDel();
	afx_msg void OnButInsert();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LONG OnGridDbClicked(WPARAM wParam, LPARAM lParam);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_SOCTABLEEDIT_H__9BBC8826_F48A_40B8_9BB2_4FD134052CB7__INCLUDED_)
