// ModelSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "ModelSelDlg.h"
#include "CTSEditorProView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModelSelDlg dialog


CModelSelDlg::CModelSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CModelSelDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CModelSelDlg::IDD2):
	(CModelSelDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CModelSelDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pView = pParent;
	ASSERT(m_pView);
}


void CModelSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModelSelDlg)
	DDX_Control(pDX, IDC_SEL_STATIC, m_wndName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModelSelDlg, CDialog)
	//{{AFX_MSG_MAP(CModelSelDlg)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	ON_BN_CLICKED(IDC_BUTTON_COPY, OnButtonCopy)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDbClicked)
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_BN_CLICKED(IDC_BUT_LANGUAGE, &CModelSelDlg::OnBnClickedButLanguage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModelSelDlg message handlers

BOOL CModelSelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_wndName.SetBkColor(RGB(255, 230, 230));

	m_btnModelNew.AttachButton(IDC_BUTTON_NEW, stingray::foundation::SECBitmapButton::Al_Left, IDB_NEW, this);
	m_btnModelDelete.AttachButton(IDC_BUTTON_DELETE, stingray::foundation::SECBitmapButton::Al_Left, IDB_DELETE, this);
	m_btnModelSave.AttachButton(IDC_BUTTON_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);
	m_btnModelCopy.AttachButton(IDC_BUTTON_COPY, stingray::foundation::SECBitmapButton::Al_Left, IDB_COPY2, this);
	m_btnModelEdit.AttachButton(IDC_BUTTON_EDIT, stingray::foundation::SECBitmapButton::Al_Left, IDB_EDIT, this);
	
	((CCTSEditorProView *)m_pView)->InitBatteryModelGrid(IDC_MODEL_GRID_CUSTOM, this, &m_wndBatteryModelGrid);

	// TODO: Add extra initialization here

	UpdateCount();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModelSelDlg::OnButtonNew() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorProView *)m_pView)->NewModel();

	UpdateCount();
}

void CModelSelDlg::OnButtonDelete() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorProView *)m_pView)->ModelDelete();

	UpdateCount();
}

void CModelSelDlg::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorProView *)m_pView)->ModelEdit();
	UpdateCount();
}

void CModelSelDlg::OnButtonCopy() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorProView *)m_pView)->ModelCopy();	
	UpdateCount();
}

void CModelSelDlg::OnButtonSave() 
{
	// TODO: Add your control notification handler code here
	((CCTSEditorProView *)m_pView)->ModelSave();		
	UpdateCount();
}

void CModelSelDlg::OnOK() 
{
	// TODO: Add extra validation here
	SelectOK();

//	CDialog::OnOK();
}

void CModelSelDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	((CCTSEditorProApp *)AfxGetApp())->OnAppExit();

}

LONG CModelSelDlg::OnGridDbClicked(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	if(pGrid->GetCurrentCell(nRow, nCol))
	{
		if(nRow > 0)
		{
			OnOK();
		}
	}
	return 0;
}

LONG CModelSelDlg::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{

	ROWCOL nRow, nCol;			
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	if(nRow < 1)
	{
		m_wndName.SetText("");
	}
	else
	{
		m_wndName.SetText(pGrid->GetValueRowCol(nRow, nCol));
	}
	return 0;
}

void CModelSelDlg::UpdateCount()
{
	CString str;
	str.Format("%d", m_wndBatteryModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(str);
}

BOOL CModelSelDlg::SelectOK()
{
	ROWCOL nRow, nCol;
	if(m_wndBatteryModelGrid.GetCurrentCell(nRow, nCol))
	{
		if(nRow > 0)
		{
			WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
			((CCTSEditorProView *)m_pView)->PostMessage(WM_GRID_MOVECELL, (WPARAM) value, (LPARAM)&m_wndBatteryModelGrid);

			AfxGetMainWnd()->ShowWindow(SW_MAXIMIZE);	//ljb 전체 화면으로 보이기
			ShowWindow(SW_HIDE);
		}
		else
		{
			AfxMessageBox(Fun_FindMsg("SelectOK_msg1","IDD_MODEL_DIALOG"));
			return FALSE;
		}
	}
	else
	{
		AfxMessageBox(Fun_FindMsg("SelectOK_msg1","IDD_MODEL_DIALOG"));
		return FALSE;
	}

	return TRUE;
}

BOOL CModelSelDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		if(GetFocus() == &m_wndBatteryModelGrid)
		{
			switch( pMsg->wParam )
			{
			case VK_INSERT:
				return TRUE;
				
			case VK_DELETE:
				return TRUE;
			}
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CModelSelDlg::OnBnClickedButLanguage()
{
	BOOL bChangeToEng;
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 0);
	if(bSelectedLanguage == 1)
	{
		bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 2);
		if(bChangeToEng == TRUE)
		{
			AfxMessageBox(Fun_FindMsg("IDC_BUTTON_LANGUAGE_CHANGE1","IDD_MODEL_DIALOG"));//&&
			return;
		}
	}
	else
	{
		bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 1);
		if(bChangeToEng == TRUE)
		{
			AfxMessageBox(Fun_FindMsg("IDC_BUTTON_LANGUAGE_CHANGE2","IDD_MODEL_DIALOG"));//&&
			return;
		}
	}
}
