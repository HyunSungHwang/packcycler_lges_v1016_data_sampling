// Dlg_PattSelect.cpp : implementation file
//
//$1013
#include "stdafx.h"
#include "CTSEditorPro.h"
#include "Dlg_PattSelect.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_PattSelect dialog


Dlg_PattSelect::Dlg_PattSelect(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_PattSelect::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_PattSelect::IDD2):
	(Dlg_PattSelect::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_PattSelect)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pDoc=NULL;
	m_pStep=NULL;
	m_pParent=pParent;
}


void Dlg_PattSelect::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_PattSelect)
	DDX_Control(pDX, IDC_STATIC_STEPNO, m_StaticStepNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_PattSelect, CDialog)
	//{{AFX_MSG_MAP(Dlg_PattSelect)
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_PattSelect message handlers

BOOL Dlg_PattSelect::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_StaticStepNo.SetFontAlign(DT_LEFT);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_PattSelect::OnOK() 
{
	//CDialog::OnOK();
}

void Dlg_PattSelect::OnCancel() 
{
	//CDialog::OnCancel();
}

void Dlg_PattSelect::OnClose() 
{	
	//CDialog::OnClose();
}


BOOL Dlg_PattSelect::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam==VK_RETURN ||
			pMsg->wParam==VK_ESCAPE )
		{
			ShowWindow(SW_HIDE);
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_PattSelect::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	
}
