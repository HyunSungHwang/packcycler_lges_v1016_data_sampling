// CTSEditorProDoc.cpp : implementation of the CCTSEditorProDoc class
//

#include "stdafx.h"
#include "CTSEditorPro.h"

#include "CTSEditorProDoc.h"

#include "LoginDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProDoc

IMPLEMENT_DYNCREATE(CCTSEditorProDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSEditorProDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSEditorProDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProDoc construction/destruction

extern CString g_strDataBaseName;
extern CString GetDataBaseName();

CCTSEditorProDoc::CCTSEditorProDoc()
{
	// TODO: add one-time construction code here
	ZeroMemory(&m_sPreTestParam, sizeof(TEST_PARAM));
	ZeroMemory(&m_sInitPreTestParam, sizeof(TEST_PARAM));

	strcpy(m_LoginData.szLoginID, "");
	strcpy(m_LoginData.szPassword, "");
	m_LoginData.nPermission = PS_USER_SUPER;
	strcpy(m_LoginData.szRegistedDate, "");
	strcpy(m_LoginData.szUserName, "");
	strcpy(m_LoginData.szDescription, "");
	m_LoginData.bUseAutoLogOut = FALSE;	
	m_LoginData.lAutoLogOutTime = 0;

	m_nGradingStepLimit = 5;
	m_bEditedFlag = FALSE;

	//m_bEDLCType = FALSE;

	//m_bCycleType = TRUE;

	m_nMinRefI = 25;		//25mA이하 전류 Reference는 입력하지 못하도록 한다. 
	//m_lMaxChargeCurrent = 0;
	m_posUndoList = NULL;

}

CCTSEditorProDoc::~CCTSEditorProDoc()
{
}

BOOL CCTSEditorProDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	init_Language();

	m_bParallelMode = FALSE;	//ljb 201134 이재복 //////////

	m_bUseLogin = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseLogin", FALSE);
	m_lMaxCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	m_lMaxVoltage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);
	m_lMaxCurrent1= AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Aging Current", 500);
	m_nMinRefI = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Min Ref Current", 25);

	m_lUserMaxPower = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UserLimitPower", m_lMaxCurrent * m_lMaxVoltage);	//ljb 20130722 add

//	m_nUnitType = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Unit Type", 1);
//	m_lMaxChargeCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Charge Current", 0);
//	if(m_lMaxChargeCurrent == 0) m_lMaxChargeCurrent = m_lMaxCurrent;
//	m_bEDLCType = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "EDLC", FALSE);

	//전압 전류 단위 Mode
//	m_VoltageUnitMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Vtg Unit Mode", 0);
//	m_CurrentUnitMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Crt Unit Mode", 0);

	m_lVRefLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CCLowRef", 0);
	m_lVRefHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CCHighRef", m_lMaxVoltage);

	CString strTemp;
	char szBuff[64], szUnit[32], szDecimalPoint[32];
//	int nDeicmal;

	//Voltage Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_VtgUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_VtgUnitInfo.szUnit, szUnit) ;
	if(strTemp == "V")
	{
		m_VtgUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mV")	//default V, uV 표시 
	{
		m_VtgUnitInfo.fTransfer = 1.0f ;
	}

	//Current Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_CrtUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_CrtUnitInfo.szUnit, szUnit) ;
	if(strTemp == "A")
	{
		m_CrtUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mA")	//default mA, uA 표시 
	{
		m_CrtUnitInfo.fTransfer = 1.0f ;
	}
	
	//Capacity Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_CapUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_CapUnitInfo.szUnit, szUnit) ;
	if(strTemp == "Ah" || strTemp == "F")
	{
		m_CapUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mA")	//default mAh or uAh 표시 
	{
		m_CapUnitInfo.fTransfer = 1.0f ;
	}

	//Watt Unit
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_WattUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_WattUnitInfo.szUnit, szUnit) ;
	if(strTemp == "KW" || strTemp == "kW")
	{
		m_WattUnitInfo.fTransfer = 1000000.0f ;
	}
	else if(strTemp == "W")
	{
		m_WattUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mW")	//default mW or uW 표시 
	{
		m_WattUnitInfo.fTransfer = 1.0f ;
	}

	//WattHour
	sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	strTemp = szUnit;
	m_WattHourUnitInfo.nFloatPoint = atoi(szDecimalPoint);
	strcpy(m_WattHourUnitInfo.szUnit, szUnit) ;
	if(strTemp == "KWh" || strTemp == "kWh")
	{
		m_WattHourUnitInfo.fTransfer = 1000000.0f ;
	}
	else if(strTemp == "Wh")
	{
		m_WattHourUnitInfo.fTransfer = 1000.0f ;
	}
	else //if(strTemp == "mWh")	//default mWh, uWh 표시 
	{
		m_WattHourUnitInfo.fTransfer = 1.0f ;
	}

	//Capa/Watt/WattHour는 전류 단위를 따라감 
	/*m_CapUnitInfo.nFloatPoint = m_CrtUnitInfo.nFloatPoint;
	m_WattUnitInfo.nFloatPoint = m_CrtUnitInfo.nFloatPoint;
	m_WattHourUnitInfo.nFloatPoint = m_CrtUnitInfo.nFloatPoint;
	if(GetCrtUnit() == "A")
	{
		m_CapUnitInfo.fTransfer = 1000.0f ;
		strcpy(m_CapUnitInfo.szUnit, "Ah") ;

		m_WattUnitInfo.fTransfer = 1000.0f ;
		m_WattHourUnitInfo.fTransfer = 1000.0f;
		strcpy(m_WattUnitInfo.szUnit, "W") ;
		strcpy(m_WattHourUnitInfo.szUnit, "Wh") ;
	}
	else //if(GetCrtUnit() == "mA")
	{
		m_CapUnitInfo.fTransfer = 1.0f ;
		strcpy(m_CapUnitInfo.szUnit, "mAh") ;
		m_WattUnitInfo.fTransfer = 1.0f ;
		m_WattHourUnitInfo.fTransfer = 1.0f;
		strcpy(m_WattUnitInfo.szUnit, "mW") ;
		strcpy(m_WattHourUnitInfo.szUnit, "mWh") ;
	}*/


	/*//
	strTemp = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "C Unit", "mAh");
	if(strTemp == "Ah")
	{
		m_CapUnitInfo.fTransfer = 1000.0f ;
		strcpy(m_CapUnitInfo.szUnit, "Ah") ;
	}
	else //if(strTemp == "mAh")	//default  mAh표시 
	{
		m_CapUnitInfo.fTransfer = 1.0f ;
		strcpy(m_CapUnitInfo.szUnit, "mAh") ;
	}

	strTemp = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "W Unit", "mW");
	if(strTemp == "W")
	{
		m_WattUnitInfo.fTransfer = 1000.0f ;
		m_WattHourUnitInfo.fTransfer = 1000.0f;
		strcpy(m_WattUnitInfo.szUnit, "W") ;
		strcpy(m_WattHourUnitInfo.szUnit, "Wh") ;
	}
	else //if(strTemp == "mW")	//default mW 표시 
	{
		m_WattUnitInfo.fTransfer = 1.0f ;
		m_WattHourUnitInfo.fTransfer = 1.0f;
		strcpy(m_WattUnitInfo.szUnit, "mW") ;
		strcpy(m_WattHourUnitInfo.szUnit, "mWh") ;
	}*/


	//commented by KBH	2006/09/18
	//Unit을 보고 판단하도록 수정
	/*if(m_CurrentUnitMode)
	{
		m_CrtUnitInfo.fTransfer = 1.0f;
		strcpy(m_CrtUnitInfo.szUnit, "uA") ;
	}*/
	
	if(m_bUseLogin)
	{
		CLoginDlg *pDlg;
		pDlg = new CLoginDlg;
		ASSERT(pDlg);

		if(pDlg->DoModal() != IDOK)
		{
			delete pDlg;
			pDlg = NULL;
			return FALSE;
		}
		
		m_LoginData = pDlg->m_LoginInfo; 
		delete pDlg;
		pDlg = NULL;
	
		if(PermissionCheck(PMS_EDITOR_START) == FALSE)
		{
			AfxMessageBox(Fun_FindMsg("OnNewDocument_msg1","EDITOR_DOC"));
			return FALSE;
		}
	}

	if(RequeryProcType() == FALSE)	return FALSE;
	if(RequeryGradeType() == FALSE)	return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProDoc serialization

void CCTSEditorProDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProDoc diagnostics

#ifdef _DEBUG
void CCTSEditorProDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSEditorProDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProDoc commands

int CCTSEditorProDoc::RequeryStepData(LONG lTestID)
{
	if(GetTotalStepNum()>0)
	{
		if(!ReMoveStepArray())	return -1;
	}

	RemoveInitStepArray();

	int nTotStepNum;
	CStepRecordSet	recordSet;

	recordSet.m_strFilter.Format("[TestID] = %ld", lTestID);
	recordSet.m_strSort.Format("[StepNo]");

	try
	{
		recordSet.Open();
		//recordSet.Open(AFX_DAO_USE_DEFAULT_TYPE,"*");
		
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}


	if(recordSet.IsBOF())
	{
		nTotStepNum = 0;
	}
	else
	{
		recordSet.MoveLast();
		nTotStepNum = recordSet.GetRecordCount();
	}

	if(nTotStepNum <= 0 || nTotStepNum > SCH_MAX_STEP)
	{
		recordSet.Close();
		return 0;
	}

	recordSet.MoveFirst(); 
	
	int count = 0;
	char seps[] = ",";
	char *token;
	char szColl[20];
	char szTemp[255];

	STEP *pStep, *pIntStep;

	for(int i= 0;  i< nTotStepNum; i++)
	{
		pStep = new STEP;
		pIntStep = new STEP;

		ZeroMemory(pStep, sizeof(STEP));
		ZeroMemory(pIntStep, sizeof(STEP));

		//Step header
		pStep->chStepNo		= (char)recordSet.m_StepNo;
		pStep->chType		= (char)recordSet.m_StepType;
		pStep->nProcType = recordSet.m_StepProcType;
		pStep->chMode		= (char)recordSet.m_StepMode;
		pStep->fVref_Charge	= recordSet.m_Vref;
		pStep->fVref_DisCharge = recordSet.m_Vref_DisCharge;
		pStep->fIref		= recordSet.m_Iref;
		pStep->fPref		= recordSet.m_Pref;
		pStep->fRref		= recordSet.m_Rref;
		
		//End 조건 
		pStep->ulEndTime	= recordSet.m_EndTime;
		pStep->fEndV_H		= recordSet.m_EndV;
		//pStep->fEndV_L		= recordSet.m_EndV_L;		//v1009 Soc 필드에서 불러옴
		pStep->fEndI		= recordSet.m_EndI;

		//mF 단위 저장을 F단위로 표시 
		pStep->fEndC		= recordSet.m_EndCapacity;
		pStep->fEndDV		= recordSet.m_End_dV;
		pStep->fEndDI		= recordSet.m_End_dI;
		pStep->nLoopInfoCycle = recordSet.m_CycleCount;

		//tep->nLoopInfoGoto = atol(recordSet.m_strGotoValue);

		//20081201 KHS
		if(!recordSet.m_strGotoValue.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_strGotoValue);
			sscanf(szTemp, "%d %d %d %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld"
				, &pStep->nLoopInfoGotoStep, &pStep->lRange
				, &pStep->nMultiLoopGroupID, &pStep->nMultiLoopInfoCycle, &pStep->nMultiLoopInfoGotoStep
				, &pStep->nAccLoopGroupID, &pStep->nAccLoopInfoCycle, &pStep->nAccLoopInfoGotoStep
				, &pStep->nGotoStepEndV_H, &pStep->nGotoStepEndV_L, &pStep->nGotoStepEndTime, &pStep->nGotoStepEndCVTime
				, &pStep->nGotoStepEndC, &pStep->nGotoStepEndWh, &pStep->nGotoStepEndValue);		//20090402 ljb v1009
		}
		
		//안전조건
		pStep->fVLimitHigh	= recordSet.m_OverV;
		pStep->fVLimitLow	= recordSet.m_LimitV;
		pStep->fILimitHigh	= recordSet.m_OverI;
		pStep->fILimitLow	= recordSet.m_LimitI;
		pStep->fCLimitHigh	= recordSet.m_OverCapacity;
		pStep->fCLimitLow	= recordSet.m_LimitCapacity;
		pStep->fImpLimitHigh = recordSet.m_OverImpedance;
		pStep->fImpLimitLow	= recordSet.m_LimitImpedance;
		pStep->fDeltaV		= recordSet.m_DeltaV;	  
		pStep->lDeltaTime = recordSet.m_DeltaTime ;
		pStep->fDeltaI		= recordSet.m_DeltaI;
		pStep->lDeltaTime1  = recordSet.m_DeltaTime1;
		if (pStep->fEndC != 0)
		{
			//20170526 수정 시작 
			//ljb 20180213 edit 종료 용량 값이 0이면 안전 값도 0으로 변경 (최종성 책임 수정요청)
			if (pStep->chType == PS_STEP_PATTERN)   //jb 20170526 Pattern Step 용량 부등호에 따라 안전조건 값 변수 틀림 
			{
				// 				 if (pStep->fCLimitHigh == 0 && recordSet.m_OverCapacity > 0) pStep->fCLimitHigh   = recordSet.m_OverCapacity * 1.1;   //ljb 20170526 용량 110%
				// 				 if (pStep->fCLimitLow == 0 && recordSet.m_OverCapacity < 0) pStep->fCLimitLow   = recordSet.m_OverCapacity * 1.1;   //ljb 20170526 용량 110%
			}
			else
			{
				if (pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
				{
					if (pStep->fCLimitHigh == 0) pStep->fCLimitHigh   = recordSet.m_OverCapacity * 1.1;   //ljb 20150806 용량 110%
				}				 
			}
			//20170526 수정 끝
			
			//20170526 YULEE 수정전 내용 주척처리 시작 
			//if (pStep->fCLimitHigh == 0) pStep->fCLimitHigh	= recordSet.m_OverCapacity * 1.1;	//ljb 20150806 용량 110%
			//20170526 수정전 내용 주척처리 끝 
		}

		//Grading
		pStep->bGrade		= recordSet.m_Grade;

		//전압 상승 비교
		pStep->ulCompTimeV[0] = recordSet.m_CompTimeV1;
		pStep->ulCompTimeV[1] = recordSet.m_CompTimeV2;
		pStep->ulCompTimeV[2] = recordSet.m_CompTimeV3;
		pStep->fCompVHigh[0] = recordSet.m_CompVHigh1;
		pStep->fCompVHigh[1] = recordSet.m_CompVHigh2;
		pStep->fCompVHigh[2] = recordSet.m_CompVHigh3;
		pStep->fCompVLow[0] = recordSet.m_CompVLow1;
		pStep->fCompVLow[1] = recordSet.m_CompVLow2;
		pStep->fCompVLow[2] = recordSet.m_CompVLow3;

		//전류 상승 비교
		pStep->ulCompTimeI[0] = recordSet.m_CompTimeI1;
		pStep->ulCompTimeI[1] = recordSet.m_CompTimeI2;
		pStep->ulCompTimeI[2] = recordSet.m_CompTimeI3;
		pStep->fCompIHigh[0] = recordSet.m_CompIHigh1;
		pStep->fCompIHigh[1] = recordSet.m_CompIHigh2;
		pStep->fCompIHigh[2] = recordSet.m_CompIHigh3;
		pStep->fCompILow[0] = recordSet.m_CompILow1;
		pStep->fCompILow[1] = recordSet.m_CompILow2;
		pStep->fCompILow[2] = recordSet.m_CompILow3;
		
		//EDLC 용량 계산 전압
		pStep->fCapaVoltage1 = recordSet.m_CapVLow;
		pStep->fCapaVoltage2 = recordSet.m_CapVHigh;
		//report조건
		pStep->fReportV		=	recordSet.m_RecordDeltaV;
		pStep->fReportI		=	recordSet.m_RecordDeltaI;
		pStep->ulReportTime =	recordSet.m_RecordTime;
		pStep->fReportTemp = atof(recordSet.m_strReportTemp);
		
		//End 전류 전압 검사
		pStep->fVEndHigh	=	recordSet.m_EndCheckVHigh;
		pStep->fVEndLow		=	recordSet.m_EndCheckVLow;
		pStep->fIEndHigh	=	recordSet.m_EndCheckIHigh;
		pStep->fIEndLow		=	recordSet.m_EndCheckILow;
	
		//EndV_L and SOC 종료 조건
		if(!recordSet.m_strSocEnd.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_strSocEnd);
			 //&pStep->bUseCyclePause(Cycle 완료 후 대기), &pStep->bUseLinkStep(CP모드와 연동), &pStep->bUseChamberProg(챔버 프로그램 모드로 운영);
			//sscanf(szTemp, "%f %d %d %f %d %d %d %d %d %f", &pStep->fEndV_L, &pStep->nValueRateItem, &pStep->nValueStepNo, &pStep->fValueRate, &pStep->bUseCyclePause, &pStep->bUseLinkStep, &pStep->bUseChamberProg, &pStep->nValueLoaderItem, &pStep->nValueLoaderMode, &pStep->fValueLoaderSet);
			sscanf(szTemp, "%f %d %d %f %d %d %d %d %d %f %d %d %d %d %d", 
				&pStep->fEndV_L, &pStep->nValueRateItem, &pStep->nValueStepNo, &pStep->fValueRate, &pStep->bUseCyclePause, 
				&pStep->bUseLinkStep, &pStep->bUseChamberProg, &pStep->nValueLoaderItem, &pStep->nValueLoaderMode, &pStep->fValueLoaderSet,
				&pStep->nWaitTimeInit, &pStep->nWaitTimeDay, &pStep->nWaitTimeHour, &pStep->nWaitTimeMin, &pStep->nWaitTimeSec
				);
		}
		
		//End Watt/WattHour
		if(!recordSet.m_strEndWatt.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_strEndWatt);
			sscanf(szTemp, "%f %f", &pStep->fEndW, &pStep->fEndWh);
		}

		//Temperature Limit		-> Value4
		//2014.12.08 추가. 대기모드 플러그 추가. nNoCheckMode 추가. 2014.12.09 nCanTxOffMode 추가
		if(!recordSet.m_strTempLimit.IsEmpty())
		{
			//ljb 201010  Cycle 완료후 Pause, CP-CC 연동 스텝, 챔버 Prog 모드 사용 추가
			sprintf(szTemp, "%s", recordSet.m_strTempLimit);
			/*sscanf(szTemp, "%f %f %f %f %d %d %d %d %d %d", 
				&pStep->fTempLow, &pStep->fTempHigh, &pStep->fEndTemp, &pStep->fStartTemp, &pStep->bUseCyclePause
				, &pStep->bUseLinkStep, &pStep->bUseChamberProg, &pStep->nNoCheckMode, &pStep->nCanTxOffMode, &pStep->nCanCheckMode );	//20150825 edit
			*/
			sscanf(szTemp, "%f %f %f %f %d %d %d %d %d %d %d %f",  /*yulee 20180829*/
				&pStep->fTempLow, &pStep->fTempHigh, &pStep->fEndTemp, &pStep->fStartTemp, &pStep->bUseCyclePause
				, &pStep->bUseLinkStep, &pStep->bUseChamberProg, &pStep->nNoCheckMode, &pStep->nCanTxOffMode, &pStep->nCanCheckMode
				, &pStep->bUseChamberStop, &pStep->fCellDeltaVStep); //yulee 20190531_6
		  }

		if(pStep->fCellDeltaVStep < -10000) //yulee 20190531_6
			pStep->fCellDeltaVStep = 0.0;

			
		sprintf(pStep->szSimulFile, "%s", recordSet.m_strSimulFile);
				
		//2014.09.02 Usermap 추가.
		sprintf(pStep->szUserMapFile, "%s", recordSet.m_strSimulFile);

		if(!recordSet.m_ValueLimitLow.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_ValueLimitLow);
			//ljb 20131212 edit EndTimeDay, EndCVTimeDay 추가
			sscanf(szTemp, "%ld %ld %f %f %ld %ld", 
				&pStep->lValueLimitLow, &pStep->ulCVTime, &pStep->fCapacitanceHigh, &pStep->fCapacitanceLow,
				&pStep->ulEndTimeDay,&pStep->ulCVTimeDay);
		}
		if(!recordSet.m_ValueLimitHigh.IsEmpty())
		{
			CString strValue;
			strValue.Format("%s", recordSet.m_ValueLimitHigh);
			pStep->lValueLimitHigh = atol(strValue);

		}

		//lyj 20200806 s REST 스탭일 때만 파워서플라이 동작 ======================
		if(pStep->chType != PS_STEP_REST)
		{
			pStep->nPwrSupply_Cmd = 0;
			pStep->nPwrSupply_Volt = 0;
		}
		//lyj 20200806 e  ========================================================

		if(!recordSet.m_ValueOven.IsEmpty())
		{
			sprintf(szTemp, "%s", recordSet.m_ValueOven);
			sscanf(szTemp, "%f %f %f %f %d %d %d %d %d %d %d %d %d %d %d %f %d %f %d %d %f %d %d %f %d %d %f %d %d %f %d %d", 
					&pStep->fTref, 
					&pStep->fHref, 
					&pStep->fTrate, 
					&pStep->fHrate,
					&pStep->nCellBal_CircuitRes,//cellbal
					&pStep->nCellBal_WorkVoltUpper,
					&pStep->nCellBal_WorkVolt,
					&pStep->nCellBal_StartVolt,
					&pStep->nCellBal_EndVolt,
					&pStep->nCellBal_ActiveTime,
					&pStep->nCellBal_AutoStopTime,
					&pStep->nCellBal_AutoNextStep,
					&pStep->nPwrSupply_Cmd,//power
					&pStep->nPwrSupply_Volt,
					&pStep->nChiller_Cmd,//chiller
					&pStep->fChiller_RefTemp,
					&pStep->nChiller_RefCmd,
					&pStep->fChiller_RefPump,
					&pStep->nChiller_PumpCmd,
					&pStep->nChiller_TpData,
					&pStep->fChiller_ONTemp1,
					&pStep->nChiller_ONTemp1_Cmd,
					&pStep->nChiller_ONPump1_Cmd,
					&pStep->fChiller_ONTemp2,
					&pStep->nChiller_ONTemp2_Cmd,
					&pStep->nChiller_ONPump2_Cmd,
					&pStep->fChiller_OFFTemp1,
					&pStep->nChiller_OFFTemp1_Cmd,
					&pStep->nChiller_OFFPump1_Cmd,
					&pStep->fChiller_OFFTemp2,
					&pStep->nChiller_OFFTemp2_Cmd,
					&pStep->nChiller_OFFPump2_Cmd);	
		}
		//ljb 2009-03
		//if(!recordSet.m_ValueBMSandAUX.IsEmpty())
		//{
		//	sprintf(szTemp, "%s", recordSet.m_ValueBMSandAUX);
		//	sscanf(szTemp, "%f %f %f %f %f %f %f %f %f %d %d %d %d %d %d %d %d %d %f %f %d %d"
		//		,&pStep->fEndAuxVtgHigh, &pStep->fEndAuxVtgLow, &pStep->fEndAuxTempHigh, &pStep->fEndAuxTempLow
		//		,&pStep->fEndBmsVtgHigh, &pStep->fEndBmsVtgLow, &pStep->fEndBmsTempHigh, &pStep->fEndBmsTempLow, &pStep->fEndBmsFltHigh
		//		,&pStep->nLoopGotoStepAuxVtgH, &pStep->nLoopGotoStepAuxVtgL, &pStep->nLoopGotoStepAuxTempH, &pStep->nLoopGotoStepAuxTempL
		//		,&pStep->nLoopGotoStepBmsVtgH, &pStep->nLoopGotoStepBmsVtgL, &pStep->nLoopGotoStepBmsTempH, &pStep->nLoopGotoStepBmsTempL
		//		,&pStep->nLoopGotoStepBmsFltH		
		//		,&pStep->fEndBmsSocHigh, &pStep->fEndBmsSocLow, &pStep->nLoopGotoStepBmsSocH, &pStep->nLoopGotoStepBmsSocL);			
		//}

		//ljb 20100819 for v100A  CAN, AUX 조건 설정

		if(!recordSet.m_strCanCategory.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strCanCategory);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->ican_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		//yulee 20181001 reload에서는 필요 없다. 
		/*
		if(!recordSet.m_strCanCodeName.IsEmpty()) //yulee 20180830
		{
			count=0;
			memset(szTemp,0,25);
			sprintf(szTemp, "%s", recordSet.m_strCanCodeName);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				memcpy(pStep->szCanCodeName[count], szColl, sizeof(szColl));
				count++;
				token = strtok(NULL, seps );
			}			
		}
		*/
		if(!recordSet.m_strCanDataType.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strCanDataType);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->cCan_data_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		if(!recordSet.m_strCanCompareType.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strCanCompareType);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->cCan_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		if(!recordSet.m_strCanValue.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strCanValue);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->fcan_Value[count] = atof(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		if(!recordSet.m_strCanGoto.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strCanGoto);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->ican_branch[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		//aux
		if(!recordSet.m_strAuxCategory.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strAuxCategory);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->iaux_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		//yulee 20181001 reload에서는 필요 없다. 
		/*	
		if(!recordSet.m_strAuxCodeName.IsEmpty()) //yulee 20180830
		{
			count=0;
			memset(szTemp,0,25);
			sprintf(szTemp, "%s", recordSet.m_strAuxCodeName);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				memcpy(pStep->szAuxCodeName[count], szColl, sizeof(szColl));
				count++;
				token = strtok(NULL, seps );
			}			
		}
		*/
		if(!recordSet.m_strAuxDataType.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strAuxDataType);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->cAux_data_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		if(!recordSet.m_strAuxCompareType.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strAuxCompareType);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->cAux_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}
		if(!recordSet.m_strAuxValue.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strAuxValue);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				if (count < 10) pStep->faux_Value[count] = atof(szColl);
				else pStep->iaux_conti_time[count-10] = atol(szColl);		//ljb 20170824 add
				count++;
				token = strtok(NULL, seps );
			}			
		}
		if(!recordSet.m_strAuxGoto.IsEmpty())
		{
			count=0;
			memset(szTemp,0,255);
			sprintf(szTemp, "%s", recordSet.m_strAuxGoto);
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				pStep->iaux_branch[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}			
		}

		if(pStep->chType != PS_STEP_END && 
		   pStep->chType != PS_STEP_REST &&
		   pStep->chType != PS_STEP_LOOP )	
		{
			if(RequeryStepGrade(pStep, recordSet.m_StepID)<0)
			{
				TRACE("Grading Load Error\n");
			}
		}

		pStep->fCellDeltaVStep			=	recordSet.m_CellDeltaVStep;//yulee 20190531_5
		pStep->fStepDeltaAuxTemp		=	recordSet.m_StepDeltaAuxTemp;
		pStep->fStepDeltaAuxTh			=	recordSet.m_StepDeltaAuxTh;
		pStep->fStepDeltaAuxT			=	recordSet.m_StepDeltaAuxT;
		pStep->fStepDeltaAuxVTime		=	recordSet.m_StepDeltaAuxVTime;
		pStep->fStepAuxV				=	recordSet.m_StepAuxV;

		pStep->bStepDeltaVentAuxV		=	recordSet.m_StepDeltaVentAuxV;
		pStep->bStepDeltaVentAuxTemp	=	recordSet.m_StepDeltaVentAuxTemp;
		pStep->bStepDeltaVentAuxTh		=	recordSet.m_StepDeltaVentAuxTh;
		pStep->bStepDeltaVentAuxT		=	recordSet.m_StepDeltaVentAuxT;
		pStep->bStepVentAuxV			=	recordSet.m_StepVentAuxV;

		m_apStep.Add((STEP *)pStep);
		
		//초기 복사본을 만든다.
		memcpy(pIntStep, pStep, sizeof(STEP));
		m_apInitStep.Add(pIntStep);

		recordSet.MoveNext();
	}
	recordSet.Close();
	return nTotStepNum;
}

BOOL CCTSEditorProDoc::ReMoveStepArray(int nStepNum)
{
	STEP *pStep;
	int nStepSize = GetTotalStepNum();

	if( nStepNum > 0)
	{
		if(nStepSize < nStepNum)
		{
			return FALSE;
		}
		else
		{
			if( ( pStep = (STEP *)m_apStep.GetAt(nStepNum-1) ) != NULL )
			{
				m_apStep.RemoveAt(nStepNum-1);  
			   delete pStep; // Delete the original element at 0.
			}
			/*pStep = (STEP *)m_apStep[nStepNum];
			delete pStep;
			return FALSE;
			*/
		}
	}
	else
	{
		for (int i = nStepSize-1 ; i>=0 ; i--)
		{
			if( ( pStep = (STEP *)m_apStep.GetAt(i) ) != NULL )
			{
				m_apStep.RemoveAt(i);  
			   delete pStep; // Delete the original element at 0.
			}
//			pStep = (STEP *)m_apStep[i];
//			delete pStep;
		}
		m_apStep.RemoveAll();
	}
	return TRUE;
}

void CCTSEditorProDoc::RemoveInitStepArray()
{
	STEP *pStep;
	int nStepSize = m_apInitStep.GetSize();

	for (int i = nStepSize-1 ; i>=0 ; i--)
	{
		if( ( pStep = (STEP *)m_apInitStep.GetAt(i) ) != NULL )
		{
			delete pStep; // Delete the original element at 0.
			m_apInitStep.RemoveAt(i);  
		}
	}
	m_apInitStep.RemoveAll();
}

BOOL CCTSEditorProDoc::ClearCopyStep()
{
	STEP *pStep;
	int nStepSize = m_apCopyStep.GetSize();

	if( nStepSize > 0)
	{
		for (int i = nStepSize-1 ; i>=0 ; i--)
		{
			if( ( pStep = (STEP *)m_apCopyStep.GetAt(i) ) != NULL )
			{
				m_apCopyStep.RemoveAt(i);  
			   delete pStep; // Delete the original element at 0.
			}
		}
		m_apCopyStep.RemoveAll();
	}
	return TRUE;
}

//설정된 종료 조건을 String으로 만든다.
CString CCTSEditorProDoc::MakeEndString(int nStepNum)
{
	if(GetTotalStepNum()<nStepNum || nStepNum < 0)		return "End Loading Fail";

	int iCanSetCount = 0, iAuxSetCount = 0;
	int nCount;
	int nZeroVoltCheck = 0; //과충방전기 20180709
	float OverChargerMinVolt = 0; //20180810 yulee //yulee 20191017 OverChargeDischarger Mark

	BOOL bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE);


	ULONG lSecond, nHour, lEndTimeDay=0, lCVTimeDay=0;
	CString strTemp, strWaitTime, strFormat,strLoader;
	STEP *pStep = GetStepData(nStepNum);
	if(pStep)
	{
		CString strTemp, strTemp1,strGoto;
			
		for (nCount = 0; nCount < MAX_STEP_CAN_AUX_COMPARE_SIZE; nCount++)
		{
			if (pStep->ican_function_division[nCount] > 0) iCanSetCount++;
			if (pStep->iaux_function_division[nCount] > 0) iAuxSetCount++;
		}
		
		//Wait time 표시 20160422
		strWaitTime.Empty();
		if(pStep->nWaitTimeInit == 1)	
		{
			//strWaitTime = "☞시간 초기화 ";
			strWaitTime = Fun_FindMsg("CTSEditorProDoc_MakeEndString_msg1","EDITOR_DOC");//&&
		}
		if(pStep->nWaitTimeDay > 0 || pStep->nWaitTimeHour > 0 || pStep->nWaitTimeMin > 0 || pStep->nWaitTimeSec > 0)	
		{
			//strWaitTime.Format("☞%dD %02d:%02d:%02d 시간 후 NEXT ",pStep->nWaitTimeDay,pStep->nWaitTimeHour,pStep->nWaitTimeMin,pStep->nWaitTimeSec);
			strWaitTime.Format(Fun_FindMsg("CTSEditorProDoc_MakeEndString_msg2","EDITOR_DOC"),pStep->nWaitTimeDay,pStep->nWaitTimeHour,pStep->nWaitTimeMin,pStep->nWaitTimeSec);//&&
		}
	
		if(pStep->nValueLoaderItem == 1)	
		{
			switch (pStep->nValueLoaderMode)
			{
			case 0:
				strLoader.Format(" ☞LOADER ON CP %.2f", pStep->fValueLoaderSet );
				break;
			case 1:
				strLoader.Format(" ☞LOADER ON CC %.2f", pStep->fValueLoaderSet );
				break;
			case 2:
				strLoader.Format(" ☞LOADER ON CV %.2f", pStep->fValueLoaderSet );
				break;
			case 3:
				strLoader.Format(" ☞LOADER ON CR %.2f", pStep->fValueLoaderSet );
				break;
			}
		}
		else if(pStep->nValueLoaderItem == 2) strLoader.Format(" ☞LOADER OFF");
		else strLoader.Empty();

		switch(pStep->chType)
		{
		case PS_STEP_CHARGE:			
			////Charge	ljb v1009		///////////////////////////////////////
			if(pStep->fEndV_H > 0.0f)	
			{
				strFormat.Format("High V > %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_H, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
				if (pStep->nGotoStepEndV_H == PS_STEP_NEXT || pStep->nGotoStepEndV_H == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_H);
				strTemp = strGoto;
			}
			if(pStep->fEndV_L > 0.0f)	
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("Low V < %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_L, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
				if (pStep->nGotoStepEndV_L == PS_STEP_NEXT || pStep->nGotoStepEndV_L == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_L);
				strTemp += strGoto;
			}
			//////////////////////////////////////////////////////////////////////////
			if(pStep->fEndI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("I < %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndI, FALSE));	//(double)pStep->fEndI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->ulEndTime > 0 || pStep->ulEndTimeDay > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				lEndTimeDay = pStep->ulEndTimeDay;

				if(lEndTimeDay > 0)	//day 표시 
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndTime == PS_STEP_NEXT || pStep->nGotoStepEndTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndTime);
				strTemp += strGoto;
			}
			if(pStep->fEndC > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}

				strFormat.Format("C > %%.%df", m_CapUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, CUnitTrans(pStep->fEndC, FALSE));	//(double)pStep->fEndC/C_UNIT_FACTOR);
				if (pStep->nGotoStepEndC == PS_STEP_NEXT || pStep->nGotoStepEndC == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndC);
				strTemp += strGoto;
			}
			//2005.12 End Vp로 사용 
			if(pStep->fEndDV > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dVp > %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndDV, FALSE));	//(double)pStep->fEndDV/V_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dI > %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndDI, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			//2006.5.1 End Watt로 사용
			if(pStep->fEndW > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("%s > %%.%df", m_WattUnitInfo.szUnit,m_WattUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, WattUnitTrans(pStep->fEndW, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndWh > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("%s > %%.%df", m_WattHourUnitInfo.szUnit, m_WattHourUnitInfo.nFloatPoint);
				strTemp1.Format("%s > %.1f", m_WattHourUnitInfo.szUnit, WattHourUnitTrans(pStep->fEndWh, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				if (pStep->nGotoStepEndWh == PS_STEP_NEXT || pStep->nGotoStepEndWh == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndWh);
				strTemp += strGoto;
			}
			
			if(pStep->nValueStepNo > 0 && pStep->fValueRate > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
// 				if (pStep->nValueRateItem ==1)	//soc
// 					strTemp1.Format("Step %d 의 %.1f%%(SOC)", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
// 				if (pStep->nValueRateItem ==2)	//WattHour
// 					strTemp1.Format("Step %d 의 %.1f%%(WattHour)", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
// 				if (pStep->nGotoStepEndValue == PS_STEP_NEXT || pStep->nGotoStepEndValue == 0)
// 					strGoto.Format("%s ☞ NEXT", strTemp1);
// 				else
// 					strGoto.Format("%s ☞ Step %d", strTemp1, pStep->nGotoStepEndValue);
				if (pStep->nValueRateItem ==1)	//soc
					strTemp1.Format("Step %d Ah %.1f%%", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
				if (pStep->nValueRateItem ==2)	//WattHour
					strTemp1.Format("Step %d WattHour %.1f%%", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
				if (pStep->nGotoStepEndValue == PS_STEP_NEXT || pStep->nGotoStepEndValue == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndValue);
				strTemp += strGoto;
			}

			if(pStep->ulCVTime > 0 || pStep->ulCVTimeDay > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulCVTime / 100;
				long mSec = pStep->ulCVTime % 100;
				lCVTimeDay = pStep->ulCVTimeDay;	//ljb 20131212 add

				//if(lSecond / 86400 > 0)	//day 표시 
				if (lCVTimeDay >0)
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("CV(t) > %dD %d:%02d:%02d.%02d", lCVTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("CV(t) > %dD %d:%02d:%02d", lCVTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp1.Format("CV(t) > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("CV(t) > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndCVTime == PS_STEP_NEXT || pStep->nGotoStepEndCVTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndCVTime);
				strTemp += strGoto;
			}
			
			// 			if(pStep->fEndBmsVtgHigh != 0.0f || pStep->fEndBmsVtgLow != 0.0f || pStep->fEndBmsTempHigh != 0.0f || pStep->fEndBmsTempLow != 0.0f || pStep->fEndBmsFltHigh != 0.0f)
			// 				strTemp += " (Set BMS Goto)" ;
			// 			if(pStep->fEndAuxVtgHigh != 0.0f || pStep->fEndAuxVtgLow != 0.0f || pStep->fEndAuxTempHigh != 0.0f || pStep->fEndAuxTempLow != 0.0f)
			// 				strTemp += " (Set AUX Goto)" ;

// 			for (nCount = 0; nCount < MAX_STEP_CAN_AUX_COMPARE_SIZE; nCount++)
// 			{
// 				if (pStep->ican_function_division[nCount] > 0) iCanSetCount++;
// 				if (pStep->iaux_function_division[nCount] > 0) iAuxSetCount++;
// 			}
			if(iCanSetCount > 0)
			{
				strGoto.Format(" (Set Can(%d))",iCanSetCount);
				strTemp += strGoto;
			}
			if(iAuxSetCount > 0)
			{
				strGoto.Format(" (Set Aux(%d))",iAuxSetCount);
				strTemp += strGoto;
			}
			
			strTemp = strWaitTime + strTemp + strLoader;	//20160422 edit
			return strTemp;
		
		case PS_STEP_DISCHARGE:		//Discharge
			//ljb v1009			////////////////////////////////////////////////
			if(pStep->fEndV_H > 0.0f)	
			{
				strFormat.Format("High V < %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_H, FALSE));//(double)pStep->fEndV/V_UNIT_FACTOR);
				if (pStep->nGotoStepEndV_H == PS_STEP_NEXT || pStep->nGotoStepEndV_H == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_H);
				strTemp = strGoto;

			}
			//20180514 yulee 과충방전기 전압 -값 종료조건 //yulee 20191017 OverChargeDischarger Mark
			nZeroVoltCheck = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "ZeroVoltEndCond", TRUE);
			OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE); //20180810 yulee
			if(pStep->fEndV_L > 0.0f)	
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("Low V < %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_L, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
				if (pStep->nGotoStepEndV_L == PS_STEP_NEXT || pStep->nGotoStepEndV_L == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_L);
				strTemp += strGoto;
			}
			else if(bOverChargerSet)
			{
				if((pStep->fEndV_L == 0.0f) && (nZeroVoltCheck)) //yulee 20190904 
				{
					if(!strTemp.IsEmpty())
					{
						strTemp += " or "; 
					}
					strFormat.Format("Low V < %%.%df", m_VtgUnitInfo.nFloatPoint);
					strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_L, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
					if (pStep->nGotoStepEndV_L == PS_STEP_NEXT || pStep->nGotoStepEndV_L == 0)
						strGoto.Format("%s -> NEXT", strTemp1);
					else
						strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_L);
					strTemp += strGoto;
				}
				else if((pStep->fEndV_L >= -OverChargerMinVolt) && (pStep->fEndV_L < 0)/*&& (nZeroVoltCheck)*/)
				{
					if(!strTemp.IsEmpty())
					{
						strTemp += " or "; 
					}
					strFormat.Format("Low V < %%.%df", m_VtgUnitInfo.nFloatPoint);
					strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_L, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
					if (pStep->nGotoStepEndV_L == PS_STEP_NEXT || pStep->nGotoStepEndV_L == 0)
						strGoto.Format("%s -> NEXT", strTemp1);
					else
						strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_L);
					strTemp += strGoto;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			if(pStep->fEndI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("I < %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndI, FALSE));		//	(double)pStep->fEndI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->ulEndTime > 0 || pStep->ulEndTimeDay > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				lEndTimeDay = pStep->ulEndTimeDay;

				if(lEndTimeDay > 0)	//day 표시 
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec/10);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						//20081229 KHS 시간표시 수정
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
//						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec/10);
					}
					else
					{
						strTemp1.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndTime == PS_STEP_NEXT || pStep->nGotoStepEndTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndTime);
				strTemp += strGoto;
			}
			if(pStep->fEndC > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("C > %%.%df", m_CapUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, CUnitTrans(pStep->fEndC, FALSE));	//(double)pStep->fEndC/C_UNIT_FACTOR);
				if (pStep->nGotoStepEndC == PS_STEP_NEXT || pStep->nGotoStepEndC == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndC);
				strTemp += strGoto;
			}
			//2005.12. End Vp로 사용
			if(pStep->fEndDV < 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dVp > %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndDV, FALSE));//(double)pStep->fEndDV/V_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndDI > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("dI > %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, IUnitTrans(pStep->fEndI, FALSE));	//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			//2006.5.1 End Watt로 사용
			if(pStep->fEndW > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("W > %%.%df", m_WattUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, WattUnitTrans(pStep->fEndW, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndWh > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("Wh > %%.%df", m_WattHourUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, WattHourUnitTrans(pStep->fEndWh, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				if (pStep->nGotoStepEndWh == PS_STEP_NEXT || pStep->nGotoStepEndWh == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndWh);
				strTemp += strGoto;
			}

			if(pStep->nValueStepNo > 0 && pStep->fValueRate > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				if (pStep->nValueRateItem ==1)	//soc
					strTemp1.Format("Step %d Ah %.1f%%", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
				if (pStep->nValueRateItem ==2)	//WattHour
					strTemp1.Format("Step %d WattHour %.1f%%", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
				if (pStep->nGotoStepEndValue == PS_STEP_NEXT || pStep->nGotoStepEndValue == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndValue);
				strTemp += strGoto;
			}

			if(pStep->ulCVTime > 0 || pStep->ulCVTimeDay > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulCVTime / 100;
				long mSec = pStep->ulCVTime % 100;
				lCVTimeDay = pStep->ulCVTimeDay;

				if(lCVTimeDay > 0)	//day 표시 
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("CV(t) > %dD %d:%02d:%02d.%02d", lCVTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("CV(t) > %dD %d:%02d:%02d", lCVTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp1.Format("CV(t) > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("CV(t) > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndCVTime == PS_STEP_NEXT || pStep->nGotoStepEndCVTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndCVTime);
				strTemp += strGoto;
			}
			
			if(iCanSetCount > 0)
			{
				strGoto.Format(" (Set Can (%d))",iCanSetCount);
				strTemp += strGoto;
			}
			if(iAuxSetCount > 0)
			{
				strGoto.Format(" (Set Aux (%d))",iAuxSetCount);
				strTemp += strGoto;
			}
// 			if(pStep->fEndBmsVtgHigh != 0.0f || pStep->fEndBmsVtgLow != 0.0f || pStep->fEndBmsTempHigh != 0.0f || pStep->fEndBmsTempLow != 0.0f || pStep->fEndBmsFltHigh != 0.0f)
// 				strTemp += " (Set BMS Goto)" ;
// 			if(pStep->fEndAuxVtgHigh != 0.0f || pStep->fEndAuxVtgLow != 0.0f || pStep->fEndAuxTempHigh != 0.0f || pStep->fEndAuxTempLow != 0.0f)
// 				strTemp += " (Set AUX Goto)" ;
			strTemp = strWaitTime + strTemp + strLoader;	//20160422 edit
			return strTemp;
		
		case PS_STEP_REST:			//Rest
			//20081229 KHS Impedance Copy
			{		
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				lEndTimeDay = pStep->ulEndTimeDay;

				if(lEndTimeDay > 0)	//day 표시 
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec/10);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						//20081229 KHS 시간표시 수정
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
//						strTemp1.Format("☞ t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec/10);
					}
					else
					{
						strTemp1.Format("-> t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndTime == PS_STEP_NEXT || pStep->nGotoStepEndTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndTime);
				strTemp += strGoto;
				
				if(pStep->fEndV_H > 0.0f)	
				{
					if(!strTemp.IsEmpty())
					{
						strTemp += " or "; 
					}
					strFormat.Format("High V > %%.%df", m_VtgUnitInfo.nFloatPoint);
					strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_H, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
					if (pStep->nGotoStepEndV_H == PS_STEP_NEXT || pStep->nGotoStepEndV_H == 0)
						strGoto.Format("%s -> NEXT", strTemp1);
					else
						strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_H);
					strTemp += strGoto;
				}
				if(pStep->fEndV_L > 0.0f)	
				{
					if(!strTemp.IsEmpty())
					{
						strTemp += " or "; 
					}
					strFormat.Format("Low V < %%.%df", m_VtgUnitInfo.nFloatPoint);
					strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_L, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
					if (pStep->nGotoStepEndV_L == PS_STEP_NEXT || pStep->nGotoStepEndV_L == 0)
						strGoto.Format("%s -> NEXT", strTemp1);
					else
						strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_L);
					strTemp += strGoto;
				}
			}
// 			if(pStep->fEndBmsVtgHigh != 0.0f || pStep->fEndBmsVtgLow != 0.0f || pStep->fEndBmsTempHigh != 0.0f || pStep->fEndBmsTempLow != 0.0f || pStep->fEndBmsFltHigh != 0.0f)
// 				strTemp += " (Set BMS Goto)" ;
// 			if(pStep->fEndAuxVtgHigh != 0.0f || pStep->fEndAuxVtgLow != 0.0f || pStep->fEndAuxTempHigh != 0.0f || pStep->fEndAuxTempLow != 0.0f)
// 				strTemp += " (Set AUX Goto)" ;
			if(iCanSetCount > 0)
			{
				strGoto.Format(" (Set Can (%d))",iCanSetCount);
				strTemp += strGoto;
			}
			if(iAuxSetCount > 0)
			{
				strGoto.Format(" (Set Aux (%d))",iAuxSetCount);
				strTemp += strGoto;
			}
			strTemp = strWaitTime + strTemp + strLoader;	//20160422 edit
			return strTemp;
		case PS_STEP_IMPEDANCE:		//Impedance
			{		
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				lEndTimeDay = pStep->ulEndTimeDay;
				
				if(lEndTimeDay > 0)	//day 표시 
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec/10);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						//20081229 KHS 시간표시 수정
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
//						strTemp.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec/10);
					}
					else
					{
						strTemp1.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndTime == PS_STEP_NEXT || pStep->nGotoStepEndTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndTime);
				strTemp += strGoto;

				
				if(pStep->fEndV_H > 0.0f)	
				{
					if(!strTemp.IsEmpty())
					{
						strTemp += " or "; 
					}
					strFormat.Format("High V > %%.%df", m_VtgUnitInfo.nFloatPoint);
					strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_H, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
					if (pStep->nGotoStepEndV_H == PS_STEP_NEXT || pStep->nGotoStepEndV_H == 0)
						strGoto.Format("%s -> NEXT", strTemp1);
					else
						strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_H);
					strTemp += strGoto;
// 					if(pStep->nLoopInfoGotoStep > 0)
// 					{
// 						strFormat.Format("%s ☞ Step%d", strTemp1, pStep->nLoopInfoGotoStep);
// 						strTemp += strFormat;
// 					}
					

				}
				if(pStep->fEndV_L > 0.0f)
				{
					if(!strTemp.IsEmpty())
					{
						strTemp += " or "; 
					}
					strFormat.Format("Low V < %%.%df",  m_CrtUnitInfo.nFloatPoint);
					strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_L, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
					if (pStep->nGotoStepEndV_L == PS_STEP_NEXT || pStep->nGotoStepEndV_L == 0)
						strGoto.Format("%s ☞ NEXT", strTemp1);
					else
						strGoto.Format("%s ☞ Step %d", strTemp1, pStep->nGotoStepEndV_L);
					strTemp += strGoto;
// 					if(pStep->nLoopInfoCycle > 0)
// 					{
// 						strFormat.Format("%s ☞ Step%d", strTemp1, pStep->nLoopInfoCycle);
// 						strTemp += strFormat;
// 					}
					
				}
			}
			strTemp = strWaitTime + strTemp + strLoader;	//20160422 edit
			return strTemp;


		case PS_STEP_PATTERN:
		case PS_STEP_USER_MAP: //2014.09.02 UserMap 추가.
		case PS_STEP_EXT_CAN:		//ljb 2011318 이재복 //////////
			if(pStep->fEndV_H > 0.0f)	
			{
				strFormat.Format("High V > %%.%df", m_VtgUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_H, FALSE));	//(double)pStep->fEndV/V_UNIT_FACTOR);
				if (pStep->nGotoStepEndV_H == PS_STEP_NEXT || pStep->nGotoStepEndV_H == 0)
					strGoto.Format("%s ☞ NEXT", strTemp1);
				else
					strGoto.Format("%s ☞ Step %d", strTemp1, pStep->nGotoStepEndV_H);
				strTemp += strGoto;
			}
			if(pStep->fEndV_L > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("Low V < %%.%df", m_CrtUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, VUnitTrans(pStep->fEndV_L, FALSE));	//(double)pStep->fEndI/I_UNIT_FACTOR);
				if (pStep->nGotoStepEndV_L == PS_STEP_NEXT || pStep->nGotoStepEndV_L == 0)
					strGoto.Format("%s ☞ NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndV_L);
				strTemp += strGoto;
			}
			if(pStep->ulEndTime > 0 || pStep->ulEndTimeDay > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				lEndTimeDay = pStep->ulEndTimeDay;
				
				if(lEndTimeDay > 0)	//day 표시 
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndTime == PS_STEP_NEXT || pStep->nGotoStepEndTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndTime);
				strTemp += strGoto;
			}
			if(pStep->fEndC != 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}

				if (pStep->fEndC > 0)
					strFormat.Format(Fun_FindMsg("MakeEndString_msg1","EDITOR_DOC"), m_CapUnitInfo.nFloatPoint);
				else
					strFormat.Format(Fun_FindMsg("MakeEndString_msg1","EDITOR_DOC"), m_CapUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, CUnitTrans(pStep->fEndC, FALSE));	//(double)pStep->fEndC/C_UNIT_FACTOR);
				if (pStep->nGotoStepEndC == PS_STEP_NEXT || pStep->nGotoStepEndC == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndC);
				strTemp += strGoto;
			}
			//2006.5.1 End Watt로 사용
			if(pStep->fEndW > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format("W > %%.%df", m_WattUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, WattUnitTrans(pStep->fEndW, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
			}
			if(pStep->fEndWh != 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				strFormat.Format(Fun_FindMsg("MakeEndString_msg2","EDITOR_DOC"), m_WattHourUnitInfo.nFloatPoint);
				if (pStep->fEndWh > 0)
					strTemp1.Format(Fun_FindMsg("MakeEndString_msg3","EDITOR_DOC"), WattHourUnitTrans(pStep->fEndWh, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				else
					strTemp1.Format(Fun_FindMsg("MakeEndString_msg4","EDITOR_DOC"), WattHourUnitTrans(pStep->fEndWh, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				if (pStep->nGotoStepEndWh == PS_STEP_NEXT || pStep->nGotoStepEndWh == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndWh);
				strTemp += strGoto;
			}

			if(pStep->nValueStepNo > 0 && pStep->fValueRate > 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				if (pStep->nValueRateItem ==1)	//soc
					strTemp1.Format("Step %d Ah %.1f%%", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
				if (pStep->nValueRateItem ==2)	//WattHour
					strTemp1.Format("Step %d WattHour %.1f%%", pStep->nValueStepNo, pStep->fValueRate);	//ljb v1009 SOC, WattHour 표시 
				if (pStep->nGotoStepEndValue == PS_STEP_NEXT || pStep->nGotoStepEndValue == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndValue);
				strTemp += strGoto;
			}			
			if(iCanSetCount > 0)
			{
				strGoto.Format(" (Set Can (%d))",iCanSetCount);
				strTemp += strGoto;
			}
			if(iAuxSetCount > 0)
			{
				strGoto.Format(" (Set Aux (%d))",iAuxSetCount);
				strTemp += strGoto;
			}

			strTemp = strWaitTime + strTemp + strLoader;	//20160422 edit
			return strTemp;
		
		case PS_STEP_OCV:				//Ocv
		case PS_STEP_END:				//End
		case PS_STEP_ADV_CYCLE:
			if(pStep->ulEndTime > 0 || pStep->ulEndTimeDay > 0)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				lSecond = pStep->ulEndTime / 100;
				long mSec = pStep->ulEndTime % 100;
				lEndTimeDay = pStep->ulEndTimeDay;
				
				if(lEndTimeDay > 0)	//day 표시 
				{
					nHour = lSecond;// % 86400;
					if(mSec > 0)
					{
						strTemp1.Format("t > %dD %d:%02d:%02d.%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %dD %d:%02d:%02d", lEndTimeDay, nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
					}
				}
				else
				{
					if(mSec > 0)
					{
						strTemp1.Format("t > %d:%02d:%02d.%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60, mSec);
					}
					else
					{
						strTemp1.Format("t > %d:%02d:%02d", lSecond/3600, (lSecond%3600)/60, (lSecond%3600)%60);
					}
				}
				if (pStep->nGotoStepEndTime == PS_STEP_NEXT || pStep->nGotoStepEndTime == 0)
					strGoto.Format("%s -> NEXT", strTemp1);
				else
					strGoto.Format("%s -> Step %d", strTemp1, pStep->nGotoStepEndTime);
				strTemp += strGoto;
			}
			if (pStep->nMultiLoopGroupID > 0)
			{
				strTemp1.Format("(Multi Group %d)", pStep->nMultiLoopGroupID);
				strTemp	= strTemp1;
			}
			
			//20090206 KKY////////////////////////////////////////////////////////////////////////////////////////////
			if(pStep->fEndC != 0.0f)
			{
				strFormat.Format(Fun_FindMsg("MakeEndString_msg1","EDITOR_DOC"), m_CapUnitInfo.nFloatPoint);
				strTemp1.Format(strFormat, CUnitTrans(pStep->fEndC, FALSE));	//(double)pStep->fEndC/C_UNIT_FACTOR);
				strTemp += strTemp1;
				if(pStep->nValueRateItem == 0)
				{
					strTemp1 = Fun_FindMsg("MakeEndString_msg5","EDITOR_DOC");
				}
				if(pStep->nValueRateItem == 1)
				{
					strTemp1 = Fun_FindMsg("MakeEndString_msg6","EDITOR_DOC");
				}
				if(pStep->nValueRateItem == 2)
				{
					strTemp1 = Fun_FindMsg("MakeEndString_msg7","EDITOR_DOC");
				}
				strTemp += strTemp1;
			}
			if(pStep->fEndWh != 0.0f)
			{
				if(!strTemp.IsEmpty())
				{
					strTemp += " or "; 
				}
				//strFormat.Format("합산WattHour %%.%df", m_WattHourUnitInfo.nFloatPoint);
				strFormat.Format(Fun_FindMsg("CTSEditorProDoc_MakeEndString_msg3","EDITOR_DOC"), m_WattHourUnitInfo.nFloatPoint);//&&
				strTemp1.Format(strFormat, WattHourUnitTrans(pStep->fEndWh, FALSE));//(double)pStep->fEndDI/I_UNIT_FACTOR);
				strTemp += strTemp1;
				if(pStep->nValueStepNo == 0)
				{
					strTemp1 = Fun_FindMsg("MakeEndString_msg5","EDITOR_DOC");
				}
				else if(pStep->nValueStepNo == 1)
				{
					strTemp1 = Fun_FindMsg("MakeEndString_msg6","EDITOR_DOC");
				}
				else if(pStep->nValueStepNo == 2)
				{
					strTemp1 = Fun_FindMsg("MakeEndString_msg7","EDITOR_DOC");
				}
				strTemp += strTemp1;
			}
			strTemp = strWaitTime + strTemp + strLoader;	//20160422 edit
			return strTemp;
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
		case -1:					//Not Seleted
			strTemp.Empty();
			return strTemp;
		case PS_STEP_LOOP:
			if(pStep->nLoopInfoGotoStep <= 0 || pStep->nLoopInfoGotoStep == PS_STEP_NEXT)
			{
				strTemp1.Format(Fun_FindMsg("MakeEndString_msg8","EDITOR_DOC"), pStep->nLoopInfoCycle);
			}
			else
			{
				strTemp1.Format(Fun_FindMsg("MakeEndString_msg9","EDITOR_DOC"), pStep->nLoopInfoCycle, pStep->nLoopInfoGotoStep);
			}
			strTemp = strTemp1;
			if (pStep->nAccLoopGroupID > 0)
			{
				strTemp1.Format("(Acc Group %d)", pStep->nAccLoopGroupID);
				strTemp	+= strTemp1;
			}
			if (pStep->nMultiLoopGroupID > 0)
			{
				strTemp1.Format("(Multi Group %d)", pStep->nMultiLoopGroupID);
				strTemp	+= strTemp1;
			}
			strTemp = strWaitTime + strTemp + strLoader;	//20160422 edit		
			return strTemp;
		default:
			strTemp = "Step Type Loading Fail";
			return strTemp;
		}
	}
	else
	{
		return "End Loading Fail";
	}
}


CString CCTSEditorProDoc::MakeCellBalString(int nStepNum,int iType)
{
	if(GetTotalStepNum()<nStepNum || nStepNum < 0)
	{
		return _T("");
	}		
	STEP *pStep = GetStepData(nStepNum);
	if(!pStep) return _T("");
	
	CString strTemp=_T("");
	if(pStep->nCellBal_CircuitRes>=10) 
	{
		if(iType==1)
		{//ToolTip //$1013
			//&& strTemp.Format( _T("회로저항:%d(Ω) 전압상한:%.3f(V)) 전압하한:%.3f(V)) Detection:%d(mV) Release:%d(mV) \
SignalSensTime:%d(sec) AutoStopTime:%d(sec) AutoNextStep:%d"),
			strTemp.Format(Fun_FindMsg("EditorDoc_MakeCellBalString_msg1","EDITOR_DOC"),
			pStep->nCellBal_CircuitRes,
			(float)pStep->nCellBal_WorkVoltUpper/1000.f,
			(float)pStep->nCellBal_WorkVolt/1000.f,
			pStep->nCellBal_StartVolt,
			pStep->nCellBal_EndVolt,
			pStep->nCellBal_ActiveTime,
			pStep->nCellBal_AutoStopTime,
			pStep->nCellBal_AutoNextStep);
		}
		else
		{
			strTemp.Format( _T("%d(Ω)"),pStep->nCellBal_CircuitRes);
		}
	}
	return strTemp;
}

CString CCTSEditorProDoc::MakePwrSupplyString(int nStepNum,int iType)
{
	if(GetTotalStepNum()<nStepNum || nStepNum < 0)
	{
		return _T("");
	}		
	STEP *pStep = GetStepData(nStepNum);
	if(!pStep) return _T("");
	
	CString strTemp=_T("");
	if(pStep->nPwrSupply_Cmd==1) 
	{//ON
		if(iType==1)
		{//ToolTip //$1013
			//&& strTemp.Format( _T("설정전압:%.2f(V)"),
			strTemp.Format(Fun_FindMsg("EditorDoc_MakePwrSupplyString_msg1","EDITOR_DOC"),
		            	(float)pStep->nPwrSupply_Volt/1000.f);
		}
		else
		{
			strTemp.Format( _T("%.2f(V)"),
		            	(float)pStep->nPwrSupply_Volt/1000.f);
		}
		
	} 
	else if(pStep->nPwrSupply_Cmd==2) 
	{//OFF
		strTemp.Format( _T("OFF"));
	}	
	return strTemp;
}

CString CCTSEditorProDoc::MakeChillerString(int nStepNum,int iType)
{
	if(GetTotalStepNum()<nStepNum || nStepNum < 0)
	{
		return _T("");
	}		
	STEP *pStep = GetStepData(nStepNum);
	if(!pStep) return _T("");
	
	CString strTemp=_T("");
	if(pStep->nChiller_Cmd==1) 
	{
		if(iType==1)
		{ //$1013
			//$$ CString strChillerRefCmd=_T("사용안함");
			CString strChillerRefCmd=Fun_FindMsg("EditorDoc_MakeChillerString_msg1","EDITOR_DOC");
			if(pStep->nChiller_RefCmd==1)      strChillerRefCmd=_T("ON");
			else if(pStep->nChiller_RefCmd==2) strChillerRefCmd=_T("OFF");

			//$$ CString strPumpRefCmd=_T("사용안함");
			CString strPumpRefCmd=Fun_FindMsg("EditorDoc_MakeChillerString_msg1","EDITOR_DOC");
			if(pStep->nChiller_PumpCmd==1)      strPumpRefCmd=_T("ON");
			else if(pStep->nChiller_PumpCmd==2) strPumpRefCmd=_T("OFF");

			//$$ strTemp.Format( _T("칠러설정온도:%.3f(℃) 제어:%s Pump설정:%.3f 제어:%s"),
			strTemp.Format(Fun_FindMsg("EditorDoc_MakeChillerString_msg2","EDITOR_DOC"),
				            pStep->fChiller_RefTemp,
							strChillerRefCmd,
							pStep->fChiller_RefPump,
							strPumpRefCmd);
		}
		else
		{
			strTemp.Format( _T("%.3f(℃)"),
				            pStep->fChiller_RefTemp);
		}
	}
	return strTemp;
}

CString CCTSEditorProDoc::MakePattSchedString(int nStepNum)
{
	if(GetTotalStepNum()<nStepNum || nStepNum < 0)
	{
		return _T("");
	}		
	STEP *pStep = GetStepData(nStepNum);
	if(!pStep) return _T("");
	
	CString strTemp=_T("");
	
	return strTemp;
}

CString CCTSEditorProDoc::MakePattSelectString(int nStepNum)
{
	if(GetTotalStepNum()<nStepNum || nStepNum < 0)
	{
		return _T("");
	}		
	STEP *pStep = GetStepData(nStepNum);
	if(!pStep) return _T("");
	
	CString strTemp=_T("");
	
	return strTemp;
}

int CCTSEditorProDoc::GetTotalStepNum()
{
	return m_apStep.GetSize();
}

STEP * CCTSEditorProDoc::GetStepData(int nStepNum /*One Base*/)
{
	STEP *pStep = NULL;
	if(GetTotalStepNum() < nStepNum || nStepNum <1)
		return pStep;

	return (STEP *)m_apStep[nStepNum-1];
}

BOOL CCTSEditorProDoc::RequeryPreTestParam(LONG lTestID)
{
	int count = 0;
	char seps[] = ",";
	char *token;
	char szTemp[255];
	char szColl[20];
	
	CPreTestCheckRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TestID] = %ld", lTestID);

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ZeroMemory(&m_sPreTestParam, sizeof(TEST_PARAM));
	if(recordSet.IsBOF())	// Does not Exist
	{
	}
	else
	{
// 		m_sPreTestParam.fDeltaVoltage = recordSet.m_DeltaVoltage;
// 		m_sPreTestParam.fMaxCurrent = recordSet.m_CurrentRange;
// 		m_sPreTestParam.fMaxVoltage = recordSet.m_MaxV;
// 		m_sPreTestParam.fMinVoltage = recordSet.m_MinV;
// 		m_sPreTestParam.fOCVLimitVal = recordSet.m_OCVLimit;
// 		m_sPreTestParam.fTrickleCurrent = recordSet.m_TrickleCurrent;
// 		m_sPreTestParam.nMaxFaultNo = recordSet.m_MaxFaultBattery ;
// 		m_sPreTestParam.lTrickleTime = recordSet.m_TrickleTime;
// 		m_sPreTestParam.bPreTest = recordSet.m_PreTestCheck;
//		m_sPreTestParam.fMaxV = recordSet.m_DeltaVoltage;
		if (recordSet.m_SafetyMaxV < -1000000 || recordSet.m_SafetyMaxV > 9000000 ) recordSet.m_SafetyMaxV = 0.0f;
		else m_sPreTestParam.fMaxV = recordSet.m_SafetyMaxV;
		
		if (recordSet.m_SafetyMinV < -10000000 || recordSet.m_SafetyMinV > 9000000 ) recordSet.m_SafetyMinV = 0.0f;
		else m_sPreTestParam.fMinV = recordSet.m_SafetyMinV;

		if (recordSet.m_SafetyMaxI < -1000000 || recordSet.m_SafetyMaxI > 9000000 ) recordSet.m_SafetyMaxI = 0.0f;
		else m_sPreTestParam.fMaxI = recordSet.m_SafetyMaxI;
		
		if (recordSet.m_SafetyCellDeltaV < -1000000 || recordSet.m_SafetyCellDeltaV > 9000000 ) recordSet.m_SafetyCellDeltaV = 0.0f;
		else m_sPreTestParam.fCellDataV = recordSet.m_SafetyCellDeltaV;

		/*if (recordSet.m_SafetyCellStdMaxV < -1000000 || recordSet.m_SafetyCellStdMaxV > 9000000 ) recordSet.m_SafetyCellStdMaxV = 0.0f;
		else m_sPreTestParam.fCellDataV = recordSet.m_SafetyCellStdMaxV;

		if (recordSet.m_SafetyCellStdMinV < -1000000 || recordSet.m_SafetyCellStdMinV > 9000000 ) recordSet.m_SafetyCellStdMinV = 0.0f;
		else m_sPreTestParam.fCellDataV = recordSet.m_SafetyCellStdMinV;

		if (recordSet.m_SafetyCellDeltaMaxV < -1000000 || recordSet.m_SafetyCellDeltaMaxV > 9000000 ) recordSet.m_SafetyCellDeltaMaxV = 0.0f;
		else m_sPreTestParam.fCellDataV = recordSet.m_SafetyCellDeltaMaxV;

		if (recordSet.m_SafetyCellDeltaMinV < -1000000 || recordSet.m_SafetyCellDeltaMinV > 9000000 ) recordSet.m_SafetyCellDeltaMinV = 0.0f;
		else m_sPreTestParam.fCellDataV = recordSet.m_SafetyCellDeltaMinV;*/

		if (recordSet.m_SafetyMaxT < -1000000 || recordSet.m_SafetyMaxT > 9000000 ) recordSet.m_SafetyMaxT = 0.0f;
		else m_sPreTestParam.fMaxT = recordSet.m_SafetyMaxT;
		
		if (recordSet.m_SafetyMinT < -1000000 || recordSet.m_SafetyMinT > 9000000 ) recordSet.m_SafetyMinT = 0.0f;
		else m_sPreTestParam.fMinT = recordSet.m_SafetyMinT;

		if (recordSet.m_SafetyMaxC < -1000000 || recordSet.m_SafetyMaxC > 9000000 ) recordSet.m_SafetyMaxC = 0.0f;
		else m_sPreTestParam.fMaxC = recordSet.m_SafetyMaxC;
// 		if (recordSet.m_SafetyMaxW < -1000000 || recordSet.m_SafetyMaxW > 9000000 ) recordSet.m_SafetyMaxW = 0.0f;
// 		else m_sPreTestParam.fMaxW = recordSet.m_SafetyMaxW;
// 		if (recordSet.m_SafetyMaxWh < -1000000 || recordSet.m_SafetyMaxWh > 9000000 ) recordSet.m_SafetyMaxWh = 0.0f;
// 		else m_sPreTestParam.fMaxWh = recordSet.m_SafetyMaxWh;
		if (recordSet.m_SafetyMaxW < -1000 || recordSet.m_SafetyMaxW > 2000000000 ) recordSet.m_SafetyMaxW = 0.0f;
		else m_sPreTestParam.lMaxW = recordSet.m_SafetyMaxW;
		if (recordSet.m_SafetyMaxWh < -1000 || recordSet.m_SafetyMaxWh > 2000000000 ) recordSet.m_SafetyMaxWh = 0.0f;
		else m_sPreTestParam.lMaxWh = recordSet.m_SafetyMaxWh;

		//ljb 20100819 for v100A
		if (recordSet.m_strCanCategory.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strCanCategory);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.ican_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}
		//yulee 20181001 reload에서는 필요 없다. 
		/*
		if (recordSet.m_strCanCodeName.IsEmpty() == FALSE)  //yulee 20180830
		{
			memset(szTemp,0,25);
			sprintf(szTemp,"%s",recordSet.m_strCanCodeName);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				memcpy(m_sPreTestParam.szCanCodeName[count], szColl, sizeof(szColl));
				count++;
				token = strtok(NULL, seps );
			}
		}
		*/
		if (recordSet.m_strCanCompareType.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strCanCompareType);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.cCan_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}

		if (recordSet.m_strCanDataType.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strCanDataType);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.cCan_data_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}

		if (recordSet.m_strCanValue.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strCanValue);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.fcan_Value[count] = atof(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}
		//yulee 20181001 reload에서는 필요 없다. 
		/*
		if (recordSet.m_strAuxCodeName.IsEmpty() == FALSE)
		{
			memset(szTemp,0,25);
			sprintf(szTemp,"%s",recordSet.m_strAuxCodeName);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				memcpy(m_sPreTestParam.szAuxCodeName[count], szColl, sizeof(szColl));
				count++;
				token = strtok(NULL, seps );
			}
		}
		*/
		if (recordSet.m_strAuxCategory.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strAuxCategory);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.iaux_function_division[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}

		if (recordSet.m_strAuxCompareType.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strAuxCompareType);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.cAux_compare_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}

		if (recordSet.m_strAuxDataType.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strAuxDataType);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.cAux_data_type[count] = atoi(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}

		if (recordSet.m_strAuxValue.IsEmpty() == FALSE)
		{
			memset(szTemp,0,255);
			sprintf(szTemp,"%s",recordSet.m_strAuxValue);
			count = 0;
			token = strtok( szTemp, seps );
			while(token != NULL)
			{
				strcpy(szColl, token);
				m_sPreTestParam.faux_Value[count] = atof(szColl);
				count++;
				token = strtok(NULL, seps );
			}
		}
		
		if (recordSet.m_DeltaStdMaxV < -1000 || recordSet.m_DeltaStdMaxV > 2000000000 ) recordSet.m_DeltaStdMaxV = 0.0f;
		else m_sPreTestParam.fStdMaxV = recordSet.m_DeltaStdMaxV;

		if (recordSet.m_DeltaStdMinV < -1000 || recordSet.m_DeltaStdMinV > 2000000000 ) recordSet.m_DeltaStdMinV = 0.0f;
		else m_sPreTestParam.fStdMinV = recordSet.m_DeltaStdMinV;

		if (recordSet.m_DeltaMaxV < -1000 || recordSet.m_DeltaMaxV > 2000000000 ) recordSet.m_DeltaMaxV = 0.0f;
		else m_sPreTestParam.fDataMaxV = recordSet.m_DeltaMaxV;

		if (recordSet.m_DeltaMinV < -1000 || recordSet.m_DeltaMinV > 2000000000 ) recordSet.m_DeltaMinV = 0.0f;
		else m_sPreTestParam.fDataMinV = recordSet.m_DeltaMinV;

		m_sPreTestParam.bDataVent = recordSet.m_DeltaVent;
	} 
	recordSet.Close();

	memcpy(&m_sInitPreTestParam, &m_sPreTestParam, sizeof(TEST_PARAM));

	return TRUE;
}

BOOL CCTSEditorProDoc::AddNewStep(int nStepNum, STEP *pCopyStep)
{
	int nTotStepNum = GetTotalStepNum();
	if(nTotStepNum <0 || nStepNum < 0)	return FALSE;
	
	STEP *pStep;
	pStep = new STEP;
	ASSERT(pStep);
	if(pCopyStep)
	{
		memcpy(pStep, pCopyStep, sizeof(STEP));
	}
	else
	{
		ZeroMemory(pStep, sizeof(STEP));
	}

	pStep->chStepNo = nStepNum;

	if(nStepNum > nTotStepNum)
	{
//			pStep->chType = 0;
//			pStep->chMode =-1;
		try
		{
			m_apStep.Add((STEP *)pStep);
		}
		catch (CMemoryException* e)
		{
			char szError[256];
			AfxMessageBox(e->GetErrorMessage(szError, 255));
			e->Delete();
			delete pStep;
			return FALSE;
		}
	}
	else 
	{
//			pStep->chType =-1;
//			pStep->chMode =-1;
		try
		{
			if((nStepNum - 1 ) < 0)	nStepNum = 1;
			m_apStep.InsertAt(nStepNum-1, pStep);
		}
		catch (CMemoryException* e)
		{
			char szError[256];
			AfxMessageBox(e->GetErrorMessage(szError, 255));
			e->Delete();
			delete pStep;
			return FALSE;
		}
	}
	return TRUE;
}


int CCTSEditorProDoc::SaveStep(LONG lTestID, BOOL bLinkChamber, int nType)
{
	int nTotStepNum = GetTotalStepNum();
	//if(nTotStepNum <= 0)	return TRUE;	//0 Step Size
	
	int rtn = StepValidityCheck();

	if(rtn < 0)		return rtn;
	
	RemoveInitStepArray();

	int i, j;
	CStepRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TestID] = %ld", lTestID);
	recordSet.m_strSort.Format("[StepNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}

	while(!recordSet.IsEOF())	//Clear Step Data
	{
		try						//Delete All Step
		{
			recordSet.Delete();
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			recordSet.Close();
			return -1;
		}
		recordSet.MoveNext();
	}

	CString strTemp, strTemp2;						
	strTemp.Format("[TestID] = %ld", lTestID);
	if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Clear Chcek
	{
		recordSet.Close();
		return -1;
	}	

	STEP* pStep, *pInitStep;	//, *pNextStep;

	for( int i = 0; i< nTotStepNum; i++)		//Save All Step
	{
		pStep = (STEP*)m_apStep[i]; // Get element 0
		ASSERT(pStep);

		//ljb 2010-06-25 CP-CC STEP 연동 Start -------------------
		//pNextStep = NULL;
		//if (pStep->chType != PS_STEP_END)
		//{
		//	pNextStep = (STEP*)m_apStep[i+1]; // Get element 0
		//	if ((pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE))
		//	{
		//		if (pStep->chType == pNextStep->chType && pStep->chMode == PS_MODE_CP && (pNextStep->chMode == PS_MODE_CC || pNextStep->chMode == PS_MODE_CV ||pNextStep->chMode == PS_MODE_CCCV ))
		//		{
		//			if (pNextStep->bUseLinkStep) 
		//				pStep->bUseLinkStep = TRUE;
		//			else 
		//				pStep->bUseLinkStep = FALSE;
		//		}
		//	}
		//}
		//ljb 2010-06-25 CP-CC STEP 연동 End ---------------------
		
		if((int)pStep->chType == 0)
			return -2;
		
		//
		if (pStep->fTref == 0.0f && bLinkChamber)
		{
			if (pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE
				|| pStep->chType == PS_STEP_REST || pStep->chType == PS_STEP_PATTERN 
				|| pStep->chType == PS_STEP_EXT_CAN || pStep->chType == PS_STEP_IMPEDANCE
				|| pStep->chType == PS_STEP_USER_MAP
				) //ljb 2011318 이재복 //////////
			{
				pStep->fTref = 0.1f;
			}
		}
		
		pInitStep = new STEP;
		memcpy(pInitStep, pStep, sizeof(STEP));
		m_apInitStep.Add(pInitStep);

		recordSet.AddNew();
		
		recordSet.m_TestID = lTestID;
		//Step Header
		recordSet.m_StepNo = (long)(i+1);	//pStep->chStepNo;
		recordSet.m_StepType = (long)pStep->chType;
		recordSet.m_StepProcType = pStep->nProcType ;
		recordSet.m_StepMode = (long)pStep->chMode;
		recordSet.m_Vref = pStep->fVref_Charge;
		recordSet.m_Vref_DisCharge = pStep->fVref_DisCharge;
		recordSet.m_Iref = pStep->fIref;
		recordSet.m_Pref = pStep->fPref;
		recordSet.m_Rref = pStep->fRref;

		//End 조건 
		recordSet.m_EndTime = pStep->ulEndTime;
		recordSet.m_EndV = pStep->fEndV_H;
//		recordSet.m_EndV = pStep->fEndV_L;		//Value Item 저장 하는 곳에 저장
		recordSet.m_EndI = pStep->fEndI;
		
		recordSet.m_EndCapacity = pStep->fEndC;

		recordSet.m_End_dV = pStep->fEndDV;
		recordSet.m_End_dI = pStep->fEndDI;
		recordSet.m_CycleCount = pStep->nLoopInfoCycle;
		//ljb 2009-03-24	v1009 Vatual Cycle and ACC Cycle 정보 추가
		//recordSet.m_strGotoValue.Format("%d %d", pStep->nLoopInfoGoto, pStep->lRange);
		//recordSet.m_strGotoValue.Format("%d %d %d", pStep->nLoopInfoGoto, pStep->lRange, pStep->nLoopInfoGotoCnt);		//20081201 KHS
		if (pStep->chType != PS_STEP_ADV_CYCLE)
		{
			pStep->nWaitTimeInit = 0;	//20160422 add
			if (pStep->nValueRateItem == 0)
			{
				pStep->nValueStepNo=0;
				pStep->fValueRate=0.0f;
			}
			if (pStep->nValueLoaderItem == 0)	//  [4/22/2015 이재복]
			{
				pStep->nValueLoaderMode=0;
				pStep->fValueLoaderSet=0.0f;
			}
		}
		else
		{
			pStep->nWaitTimeDay = 0;	//20160422 add
			pStep->nWaitTimeHour = 0;	//20160422 add
			pStep->nWaitTimeMin = 0;	//20160422 add
			pStep->nWaitTimeSec = 0;	//20160422 add
			
		}

		recordSet.m_strGotoValue.Format("%d %d %d %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld"
			, pStep->nLoopInfoGotoStep, pStep->lRange
			, pStep->nMultiLoopGroupID, pStep->nMultiLoopInfoCycle, pStep->nMultiLoopInfoGotoStep
			, pStep->nAccLoopGroupID, pStep->nAccLoopInfoCycle, pStep->nAccLoopInfoGotoStep
			, pStep->nGotoStepEndV_H, pStep->nGotoStepEndV_L, pStep->nGotoStepEndTime, pStep->nGotoStepEndCVTime
			, pStep->nGotoStepEndC, pStep->nGotoStepEndWh, pStep->nGotoStepEndValue);		//20090402 ljb v1009

		//안전 조건
		recordSet.m_OverV = pStep->fVLimitHigh;
		recordSet.m_LimitV = pStep->fVLimitLow;
		recordSet.m_OverI = pStep->fILimitHigh;
		recordSet.m_LimitI = pStep->fILimitLow;
		recordSet.m_OverCapacity = pStep->fCLimitHigh;
		recordSet.m_LimitCapacity = pStep->fCLimitLow;
		recordSet.m_OverImpedance = pStep->fImpLimitHigh;
		recordSet.m_LimitImpedance = pStep->fImpLimitLow;

		recordSet.m_DeltaV = pStep->fDeltaV;
		recordSet.m_DeltaTime = pStep->lDeltaTime;
		recordSet.m_DeltaI =pStep->fDeltaI;
		recordSet.m_DeltaTime1 = pStep->lDeltaTime1; 
		
		//Grading
		recordSet.m_Grade = pStep->bGrade;//36

		//전압상승비교
		recordSet.m_CompTimeV1 = pStep->ulCompTimeV[0];
		recordSet.m_CompTimeV2 = pStep->ulCompTimeV[1]; 
		recordSet.m_CompTimeV3 = pStep->ulCompTimeV[2];
		recordSet.m_CompVLow1 = pStep->fCompVLow[0];
		recordSet.m_CompVLow2 = pStep->fCompVLow[1];
		recordSet.m_CompVLow3 = pStep->fCompVLow[2];
		recordSet.m_CompVHigh1 = pStep->fCompVHigh[0];
		recordSet.m_CompVHigh2 = pStep->fCompVHigh[1];
		recordSet.m_CompVHigh3 = pStep->fCompVHigh[2];

		//전류 상승 비교
		recordSet.m_CompTimeI1 = pStep->ulCompTimeI[0];
		recordSet.m_CompTimeI2 = pStep->ulCompTimeI[1]; 
		recordSet.m_CompTimeI3 = pStep->ulCompTimeI[2];
		recordSet.m_CompILow1 = pStep->fCompILow[0];
		recordSet.m_CompILow2 = pStep->fCompILow[1];
		recordSet.m_CompILow3 = pStep->fCompILow[2];
		recordSet.m_CompIHigh1 = pStep->fCompIHigh[0];
		recordSet.m_CompIHigh2 = pStep->fCompIHigh[1];
		recordSet.m_CompIHigh3 = pStep->fCompIHigh[2];

		//저장 조건
		recordSet.m_RecordDeltaV = pStep->fReportV;
		recordSet.m_RecordDeltaI = pStep->fReportI;
		recordSet.m_RecordTime = pStep->ulReportTime;
		recordSet.m_strReportTemp.Format("%.1f", pStep->fReportTemp);		//Value1에 온도 조건 저장 
	
	
		//EDLC 용량 산출
		recordSet.m_CapVHigh = pStep->fCapaVoltage2;
		recordSet.m_CapVLow = pStep->fCapaVoltage1;
		

		//End 전류 전압 검사
		recordSet.m_EndCheckVHigh = pStep->fVEndHigh;
		recordSet.m_EndCheckVLow  = pStep->fVEndLow	;	
		recordSet.m_EndCheckIHigh = pStep->fIEndHigh;
		recordSet.m_EndCheckILow  = pStep->fIEndLow	;	

		// Soc Save 종료전압 Low 값을 여기에 쓴다
		//recordSet.m_strSocEnd.Format("%.3f %d %d %.2f %d %d %d", pStep->fEndV_L, pStep->nValueRateItem, pStep->nValueStepNo, pStep->fValueRate
 		//	, pStep->bUseCyclePause, pStep->bUseLinkStep, pStep->bUseChamberProg);
		//  [4/22/2015 이재복]
 		//recordSet.m_strSocEnd.Format("%.3f %d %d %.2f %d %d %d %d %d %.2f", pStep->fEndV_L, pStep->nValueRateItem, pStep->nValueStepNo, pStep->fValueRate
 		//	, pStep->bUseCyclePause, pStep->bUseLinkStep, pStep->bUseChamberProg, pStep->nValueLoaderItem, pStep->nValueLoaderMode, pStep->fValueLoaderSet);
		recordSet.m_strSocEnd.Format("%.3f %d %d %.2f %d %d %d %d %d %.2f %d %d %d %d %d", 
			pStep->fEndV_L, pStep->nValueRateItem, pStep->nValueStepNo, pStep->fValueRate, pStep->bUseCyclePause, 
			pStep->bUseLinkStep, pStep->bUseChamberProg, pStep->nValueLoaderItem, pStep->nValueLoaderMode, pStep->fValueLoaderSet,
			pStep->nWaitTimeInit, pStep->nWaitTimeDay, pStep->nWaitTimeHour, pStep->nWaitTimeMin, pStep->nWaitTimeSec
			);
		TRACE("EndV_L , Save SOC STEP %d: %s\n", recordSet.m_StepNo,recordSet.m_strSocEnd);

		//End Watt/WattHour
		recordSet.m_strEndWatt.Format("%.3f %.3f", pStep->fEndW, pStep->fEndWh);

		//Temperature limit		== Value4 에 저장
		//recordSet.m_strTempLimit.Format("%.3f %.3f %.3f", pStep->fTempLow, pStep->fTempHigh, pStep->fEndTemp);
		//recordSet.m_strTempLimit.Format("%.1f %.1f %.1f %.1f", pStep->fTempLow, pStep->fTempHigh, pStep->fEndTemp, pStep->fStartTemp);
		//ljb 201010 Cycler 완료 후 Pause, CP-CC 연동 스텝, 챔버 Prog 모드 추가
		//2014.12.08 Cycler 완료 후 Pause, CP-CC 연동 스텝, 챔버 Prog 모드 추가, 데이터 대기 모드 플러그 추가.
		/*recordSet.m_strTempLimit.Format("%.1f %.1f %.1f %.1f %d %d %d %d %d %d", pStep->fTempLow, pStep->fTempHigh, pStep->fEndTemp, pStep->fStartTemp, pStep->bUseCyclePause, pStep->bUseLinkStep
			, pStep->bUseChamberProg, pStep->nNoCheckMode, pStep->nCanTxOffMode, pStep->nCanCheckMode);		//20150825 edit*/
		//yulee 20180829 -bUseChamberStop 스텝별 챔버 정지 플래그 추가 
		recordSet.m_strTempLimit.Format("%.1f %.1f %.1f %.1f %d %d %d %d %d %d %d", pStep->fTempLow, pStep->fTempHigh, pStep->fEndTemp, pStep->fStartTemp, pStep->bUseCyclePause, pStep->bUseLinkStep
			, pStep->bUseChamberProg, pStep->nNoCheckMode, pStep->nCanTxOffMode, pStep->nCanCheckMode, pStep->bUseChamberStop);

		//2014.09.02 Usermap 추가. m_strSimulFile변수는 동일하게 사용한다.
		if(pStep->chType == PS_STEP_PATTERN){
			recordSet.m_strSimulFile.Format("%s", pStep->szSimulFile);
		}else if(pStep->chType == PS_STEP_USER_MAP){			
			recordSet.m_strSimulFile.Format("%s", pStep->szUserMapFile);
		}
				
 		//ljb v1009 수정
 		//recordSet.m_ValueLimitLow.Format("%ld %ld %.1f %.1f",  
 		//	pStep->lValueLimitLow, pStep->ulCVTime,	pStep->fCapacitanceHigh, pStep->fCapacitanceLow);

		//ljb 20131212 edit v100C 수정	
		//EndTimeDay,CVTimeDay 추가
		recordSet.m_ValueLimitLow.Format("%ld %ld %.1f %.1f %ld %ld",  
			pStep->lValueLimitLow, pStep->ulCVTime,	pStep->fCapacitanceHigh, pStep->fCapacitanceLow, pStep->ulEndTimeDay, pStep->ulCVTimeDay);
				
		recordSet.m_ValueLimitHigh.Format("%ld",  pStep->lValueLimitHigh);

		//lyj 20200806 s REST 스탭일 때만 파워서플라이 동작 ======================
		if(pStep->chType != PS_STEP_REST)
		{
			pStep->nPwrSupply_Cmd = 0;
			pStep->nPwrSupply_Volt = 0;
		}
		//lyj 20200806 e  ========================================================

		recordSet.m_ValueOven.Format("%.1f %.1f %.1f %.1f %d %d %d %d %d %d %d %d %d %d %d %f %d %f %d %d %f %d %d %f %d %d %f %d %d %f %d %d",
			                        pStep->fTref, 
									pStep->fHref, 
									pStep->fTrate, 
									pStep->fHrate,
									pStep->nCellBal_CircuitRes,//cellbal
									pStep->nCellBal_WorkVoltUpper,
									pStep->nCellBal_WorkVolt,
									pStep->nCellBal_StartVolt,
									pStep->nCellBal_EndVolt,
									pStep->nCellBal_ActiveTime,
									pStep->nCellBal_AutoStopTime,
									pStep->nCellBal_AutoNextStep,
									pStep->nPwrSupply_Cmd,//power
									pStep->nPwrSupply_Volt,
									pStep->nChiller_Cmd,//chiller
									pStep->fChiller_RefTemp,
									pStep->nChiller_RefCmd,
									pStep->fChiller_RefPump,
									pStep->nChiller_PumpCmd,
									pStep->nChiller_TpData,
								    pStep->fChiller_ONTemp1,
									pStep->nChiller_ONTemp1_Cmd,
									pStep->nChiller_ONPump1_Cmd,
									pStep->fChiller_ONTemp2,
									pStep->nChiller_ONTemp2_Cmd,
									pStep->nChiller_ONPump2_Cmd,
									pStep->fChiller_OFFTemp1,
									pStep->nChiller_OFFTemp1_Cmd,
									pStep->nChiller_OFFPump1_Cmd,
									pStep->fChiller_OFFTemp2,
									pStep->nChiller_OFFTemp2_Cmd,
									pStep->nChiller_OFFPump2_Cmd);	

		//ljb 2009-03 Aux and BMS
		//recordSet.m_ValueBMSandAUX.Format("%.3f %.3f %.1f %.1f %.3f %.3f %.1f %.1f %.1f %d %d %d %d %d %d %d %d %d %.1f %.1f %d %d"
		//	,pStep->fEndAuxVtgHigh,pStep->fEndAuxVtgLow,pStep->fEndAuxTempHigh,pStep->fEndAuxTempLow
		//	,pStep->fEndBmsVtgHigh,pStep->fEndBmsVtgLow,pStep->fEndBmsTempHigh,pStep->fEndBmsTempLow,pStep->fEndBmsFltHigh
		//	,pStep->nLoopGotoStepAuxVtgH, pStep->nLoopGotoStepAuxVtgL, pStep->nLoopGotoStepAuxTempH, pStep->nLoopGotoStepAuxTempL
		//	,pStep->nLoopGotoStepBmsVtgH, pStep->nLoopGotoStepBmsVtgL, pStep->nLoopGotoStepBmsTempH, pStep->nLoopGotoStepBmsTempL
		//	,pStep->nLoopGotoStepBmsFltH
		//	,pStep->fEndBmsSocHigh,pStep->fEndBmsSocLow, pStep->nLoopGotoStepBmsSocH, pStep->nLoopGotoStepBmsSocL);


		//ljb 20100819 for v100A CAN,AUX 조건 설정 ///////////////////////////
		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->ican_function_division[j]);
			else strTemp2.Format("%d,",pStep->ican_function_division[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strCanCategory = strTemp;

		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->cCan_data_type[j]);
			else strTemp2.Format("%d,",pStep->cCan_data_type[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strCanDataType = strTemp;

		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->cCan_compare_type[j]);
			else strTemp2.Format("%d,",pStep->cCan_compare_type[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strCanCompareType = strTemp;

		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%.3f",pStep->fcan_Value[j]);
			else strTemp2.Format("%.3f,",pStep->fcan_Value[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strCanValue = strTemp;

		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->ican_branch[j]);
			else strTemp2.Format("%d,",pStep->ican_branch[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strCanGoto = strTemp;

		//aux
		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->iaux_function_division[j]);
			else strTemp2.Format("%d,",pStep->iaux_function_division[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strAuxCategory = strTemp;
		
		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->cAux_data_type[j]);
			else strTemp2.Format("%d,",pStep->cAux_data_type[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strAuxDataType = strTemp;
		
		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->cAux_compare_type[j]);
			else strTemp2.Format("%d,",pStep->cAux_compare_type[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strAuxCompareType = strTemp;

		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			//if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%.3f",pStep->faux_Value[j]);
			//else strTemp2.Format("%.3f,",pStep->faux_Value[j]);
			strTemp2.Format("%.3f,",pStep->faux_Value[j]);
			strTemp += strTemp2;
		}
		//ljb 20170824 add aux value + aux conti time 동시에 기록 함.
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->iaux_conti_time[j]);
			else strTemp2.Format("%d,",pStep->iaux_conti_time[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strAuxValue = strTemp;
		
		strTemp.Empty();
		for (j=0; j < MAX_COMPARE_STEP_COUNT; j++)
		{
			if (j+1 == MAX_COMPARE_STEP_COUNT) strTemp2.Format("%d",pStep->iaux_branch[j]);
			else strTemp2.Format("%d,",pStep->iaux_branch[j]);
			strTemp += strTemp2;
		}
		recordSet.m_strAuxGoto = strTemp;
		///////////////////////////////////////////////////////////////

		//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
		UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER);  //ksj 20200825
		if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
		{
			recordSet.m_CellDeltaVStep			=	pStep->fCellDeltaVStep;//yulee 20190531_5
			recordSet.m_StepDeltaAuxTemp		=	pStep->fStepDeltaAuxTemp;
			recordSet.m_StepDeltaAuxTh			=	pStep->fStepDeltaAuxTh;
			recordSet.m_StepDeltaAuxT			=	pStep->fStepDeltaAuxT;
			recordSet.m_StepDeltaAuxVTime		=	pStep->fStepDeltaAuxVTime;
			recordSet.m_StepAuxV				=	pStep->fStepAuxV;

			recordSet.m_StepDeltaVentAuxV		=	pStep->bStepDeltaVentAuxV;
			recordSet.m_StepDeltaVentAuxTemp	=	pStep->bStepDeltaVentAuxTemp;
			recordSet.m_StepDeltaVentAuxTh		=	pStep->bStepDeltaVentAuxTh;
			recordSet.m_StepDeltaVentAuxT		=	pStep->bStepDeltaVentAuxT;
			recordSet.m_StepVentAuxV			=	pStep->bStepVentAuxV;
		}

		recordSet.Update();
	}
	
	recordSet.Requery();		//Step Data Saved

	for(int i = 0; i< nTotStepNum; i++)		//Save All Grade
	{
		pStep = (STEP*)m_apStep[i];		// Get element 0
		ASSERT(pStep);
		if(SaveStepGrade(pStep, recordSet.m_StepID) < 0)	rtn = -3;
		
		recordSet.MoveNext();
	}
	recordSet.Close();
	return rtn;
}

int CCTSEditorProDoc::StepValidityCheck()
{
	int k;
	int nTotStepNum = GetTotalStepNum();
	STEP* pStep, *pTempStep;
	BOOL bUseMaxPower(TRUE);	//20140108 add
	if(nTotStepNum <0 || nTotStepNum >  SCH_MAX_STEP)
	{
		m_strLastErrorString = Fun_FindMsg("StepValidityCheck_v100B_msg1","EDITOR_DOC");
		return -1;
	}

	//ljb 2011321 이재복 ////////// start
	m_lMaxCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	//ljb 20120706
	int nMaxParralCnt;
	nMaxParralCnt = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Parallel Count", 2);
	if (m_bParallelMode)	//ljb 201134 이재복 //////////
	{
		m_lMaxCurrent = m_lMaxCurrent * nMaxParralCnt;
	}

	BOOL bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE);


	//ljb 2011321 이재복 ////////// end

//	m_bCheckParallel = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseParallel", FALSE);			//20120222 KHS
	//m_lUserMaxPower = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UserLimitPower", (m_lMaxCurrent /1000) * (m_lMaxVoltage /1000));	//ljb 20130722 add
	m_lUserMaxPower = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UserLimitPower", 0);	//ljb 20130722 add

	if (m_lUserMaxPower == 0) bUseMaxPower = FALSE;
//	m_lUserMaxPower = (m_lMaxCurrent /1000)* (m_lMaxVoltage /1000);
	if (m_bParallelMode) m_lUserMaxPower = m_lUserMaxPower * 2;	//ljb 20131219 add 병렬 모드 일때


// 	if(m_sPreTestParam.bPreTest)
// 	{
// 		//20070321 전류는 입력하지 않아도 됨
// /*		if(m_sPreTestParam.fTrickleCurrent<m_nMinRefI)
// 		{
// 			m_strLastErrorString.Format("Cell Check Parameter의 전류 설정값이 %dmA 미만으로 설정되었습니다.", m_nMinRefI);
// 			return -5;
// 		}
// */
// 		if(m_sPreTestParam.lTrickleTime < 500 || m_sPreTestParam.lTrickleTime > 6000)
// 		{
// 			m_strLastErrorString = "Cell Check Parameter의 시간 설정값을 5초~60초 범위에서 입력하십시요.";
// 			return -5;
// 		}
// 
// 		//OCV 검사만도 지원함
// /*		if(m_sPreTestParam.fMaxCurrent < 0.0f || m_sPreTestParam.fTrickleCurrent < 0.0f)
// 		{
// 			m_strLastErrorString = "검사 전류값은 0 보다 큰값을 입력하여야 합니다.";
// 			return -5;
// 		}
// */
// //		if(m_sPreTestParam)
// 	}

	if(nTotStepNum < 1)		return 0;

#ifdef _CYCLER_
//////////////////////////////////////////////////////////////////////////
//for Cycle 

	if(m_sPreTestParam.fMaxI < 0.0f )
	{
		m_strLastErrorString = Fun_FindMsg("StepValidityCheck_v100B_msg2","EDITOR_DOC");
		return -5;
	}
	if (m_sPreTestParam.fCellDataV < 0.0f)
	{
		m_strLastErrorString = Fun_FindMsg("StepValidityCheck_v100B_msg3","EDITOR_DOC");
		return -5;
	}

	if(nTotStepNum < 4)
	{
		m_strLastErrorString = Fun_FindMsg("StepValidityCheck_v100B_msg4","EDITOR_DOC");
		return -1;
	}
	
	pStep = (STEP*)m_apStep[0]; // Get element 0 is Cycle
	if(pStep->chType != PS_STEP_ADV_CYCLE)
	{
		m_strLastErrorString = Fun_FindMsg("StepValidityCheck_v100B_msg5","EDITOR_DOC");
		return -1;
	}

	pStep = (STEP*)m_apStep[nTotStepNum-2]; // Get element End-1
	if(pStep->chType != PS_STEP_LOOP)
	{
		m_strLastErrorString = Fun_FindMsg("StepValidityCheck_v100B_msg6","EDITOR_DOC");
		return -1;
	}
//////////////////////////////////////////////////////////////////////////
#endif
	
	pStep = (STEP*)m_apStep[nTotStepNum-1]; // Get element End
	if(pStep->chType != PS_STEP_END)
	{
		m_strLastErrorString = Fun_FindMsg("StepValidityCheck_v100B_msg7","EDITOR_DOC");
		return -1;
	}
	
	int nAdvCycleCount = 0;
	int nNormalStepCount = 0;
	int nCurrentAdvCycleIndex = 0;		//20120222 KHS
	CString strTemp;
	int nLoopIndex = 0;					//20120222 KHS
	int nLoopCnt =0;					//20120222 KHS

	int nPatternStepcount = 0;
	int nUserMapCnt = 0;				//2014.09.02 UserMap 스텝 카운트 추가.
	for(int i = 0; i< nTotStepNum; i++)		//Save All Step
	{
		pStep = (STEP*)m_apStep[i]; // Get element 0
		ASSERT(pStep);

		//20120222 KHS
		//현재 Loop Step을 알기 위해
		for(int j = nLoopIndex+1; j < nTotStepNum; j++)
		{
			pTempStep = (STEP*)m_apStep[j];
			ASSERT(pTempStep);
			
			if(pTempStep->chType == PS_STEP_LOOP && nLoopCnt == 0)
			{
				nLoopIndex = pTempStep->chStepNo;
				if(nLoopIndex < 0) nLoopIndex = -nLoopIndex;
				nLoopCnt++;
			}
		}


		if(pStep->chType < 0 )
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg8","EDITOR_DOC"), i+1);
			return -2;
		}
		
		//종료 시간이 설정되어 있고 Record time이 1Sec이하가 설정되어 있을 경우 
		if(pStep->ulEndTime > 0 && pStep->ulReportTime%100 > 0)
		{
			//경고 MessageBox
			if(pStep->ulEndTime > SCH_MAX_MSEC_DATA_POINT*(pStep->ulReportTime%100))
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg9","EDITOR_DOC"),i+1, (SCH_MAX_MSEC_DATA_POINT*(pStep->ulReportTime%100))/100);
			}
		}

#ifdef _CYCLER_
//////////////////////////////////////////////////////////////////////////
//for cycle

		if(pStep->chType == PS_STEP_ADV_CYCLE)
		{
			nAdvCycleCount++;
			if(nAdvCycleCount > 1)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg10","EDITOR_DOC"), i+1);
				return -2;
			}
			if(nNormalStepCount > 1)	//이전 Loop와 다음 Cycle 사이의 Step이 존재할 경우 
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg11","EDITOR_DOC"), i+1);
				return -2;
			}
			nNormalStepCount = 0;
			if (pStep->nValueRateItem == 1)
			{
				if (pStep->fEndC < 0)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg12","EDITOR_DOC"), i+1);
					return -2;
				}
			}
			if (pStep->nValueRateItem == 2)
			{
				if (pStep->fEndC > 0)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg13","EDITOR_DOC"), i+1);
					return -2;
				}
			}
			if (pStep->nValueStepNo == 1)
			{
				if (pStep->fEndWh < 0)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg14","EDITOR_DOC"), i+1);
					return -2;
				}
			}
			if (pStep->nValueStepNo == 2)
			{
				if (pStep->fEndWh > 0)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg15","EDITOR_DOC"), i+1);
					return -2;
				}
			}
			nCurrentAdvCycleIndex = pStep->chStepNo;		//20120222 KHS
		}
		if(pStep->chType == PS_STEP_LOOP)
		{
			nLoopCnt = 0;		//20120222 KHS
			nAdvCycleCount--;
			if(nAdvCycleCount < 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg16","EDITOR_DOC"), i+1);
				return -2;
			}
			if(nNormalStepCount <= 1)	//[Cycle]을 제외한 Step이 아무것도 없을 경우 
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg17","EDITOR_DOC"), i+1);
				return -2;
			}
			nNormalStepCount = 0;
		}
		nNormalStepCount++;		//Cycle과 Loop 사이 Step의 수를 Count한다.
//////////////////////////////////////////////////////////////////////////

#endif

		//20120222 KHS  이동스텝 Check/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if(pStep->nGotoStepEndV_H > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nGotoStepEndV_H);
			if(pGotoStep)
			{
				int nEndGotoStepIndex = pGotoStep->chStepNo;
				
				if((pGotoStep->chType != PS_STEP_ADV_CYCLE && (nCurrentAdvCycleIndex > nEndGotoStepIndex || nLoopIndex < nEndGotoStepIndex))
					|| (nEndGotoStepIndex == pStep->chStepNo && (nCurrentAdvCycleIndex < nEndGotoStepIndex && nLoopIndex < nEndGotoStepIndex)))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg18","EDITOR_DOC"), i+1);
					return -2;
				}
			}
		}
		if(pStep->nGotoStepEndV_L > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nGotoStepEndV_L);
			if(pGotoStep)
			{
				int nEndGotoStepIndex = pGotoStep->chStepNo;
				
				if((pGotoStep->chType != PS_STEP_ADV_CYCLE && (nCurrentAdvCycleIndex > nEndGotoStepIndex || nLoopIndex < nEndGotoStepIndex))
					|| (nEndGotoStepIndex == pStep->chStepNo && (nCurrentAdvCycleIndex < nEndGotoStepIndex && nLoopIndex < nEndGotoStepIndex)))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg19","EDITOR_DOC"), i+1);
					return -2;
				}
			}
		}
		if(pStep->nGotoStepEndTime > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nGotoStepEndTime);
			if(pGotoStep)
			{
				int nEndGotoStepIndex = pGotoStep->chStepNo;
				
				if((pGotoStep->chType != PS_STEP_ADV_CYCLE && (nCurrentAdvCycleIndex > nEndGotoStepIndex || nLoopIndex < nEndGotoStepIndex))
					|| (nEndGotoStepIndex == pStep->chStepNo && (nCurrentAdvCycleIndex < nEndGotoStepIndex && nLoopIndex < nEndGotoStepIndex)))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg20","EDITOR_DOC"), i+1);
					return -2;
				}
			}
		}
		if(pStep->nGotoStepEndC > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nGotoStepEndC);
			if(pGotoStep)
			{
				int nEndGotoStepIndex = pGotoStep->chStepNo;
				
				if((pGotoStep->chType != PS_STEP_ADV_CYCLE && (nCurrentAdvCycleIndex > nEndGotoStepIndex || nLoopIndex < nEndGotoStepIndex))
					|| (nEndGotoStepIndex == pStep->chStepNo && (nCurrentAdvCycleIndex < nEndGotoStepIndex && nLoopIndex < nEndGotoStepIndex)))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg21","EDITOR_DOC"), i+1);
					return -2;
				}
			}
		}
		if(pStep->nGotoStepEndWh > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nGotoStepEndWh);
			if(pGotoStep)
			{
				int nEndGotoStepIndex = pGotoStep->chStepNo;
				
				if((pGotoStep->chType != PS_STEP_ADV_CYCLE && (nCurrentAdvCycleIndex > nEndGotoStepIndex || nLoopIndex < nEndGotoStepIndex))
					|| (nEndGotoStepIndex == pStep->chStepNo && (nCurrentAdvCycleIndex < nEndGotoStepIndex && nLoopIndex < nEndGotoStepIndex)))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg22","EDITOR_DOC"), i+1);
					return -2;
				}
			}
		}
		if(pStep->nGotoStepEndCVTime > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nGotoStepEndCVTime);
			if(pGotoStep)
			{
				int nEndGotoStepIndex = pGotoStep->chStepNo;
				
				if((pGotoStep->chType != PS_STEP_ADV_CYCLE && (nCurrentAdvCycleIndex > nEndGotoStepIndex || nLoopIndex < nEndGotoStepIndex))
					|| (nEndGotoStepIndex == pStep->chStepNo && (nCurrentAdvCycleIndex < nEndGotoStepIndex && nLoopIndex < nEndGotoStepIndex)))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg23","EDITOR_DOC"), i+1);
					return -2;
				}
			}
		}
		if(pStep->nGotoStepEndValue > 0)
		{
			STEP * pGotoStep = GetStepDataGotoID(pStep->nGotoStepEndValue);
			if(pGotoStep)
			{
				int nEndGotoStepIndex = pGotoStep->chStepNo;
				
				if((pGotoStep->chType != PS_STEP_ADV_CYCLE && (nCurrentAdvCycleIndex > nEndGotoStepIndex || nLoopIndex < nEndGotoStepIndex))
					|| (nEndGotoStepIndex == pStep->chStepNo && (nCurrentAdvCycleIndex < nEndGotoStepIndex && nLoopIndex < nEndGotoStepIndex)))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg24","EDITOR_DOC"), i+1);
					return -2;
				}
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//Grading이 설정되었지만 Step이 없을 경우 
		if(pStep->bGrade && pStep->sGrading_Val.chTotalGrade <= 0)
		{
			pStep->bGrade = 0;
		}

		//DC Impedance 측정일 경우 설정값을 입력 하여야 한다.
		if(pStep->chType == PS_STEP_IMPEDANCE && pStep->chMode == PS_MODE_DCIMP)
		{
			if(pStep->fIref <= m_nMinRefI)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg25","EDITOR_DOC"), i+1);								
				return -1;
			}
			if(pStep->ulEndTime == 0 && pStep->ulEndTimeDay == 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg26","EDITOR_DOC"), i+1);
				return -1;
			}
			
#ifdef _EDLC_CELL_
			//Time is 10usec unit;
			if(pStep->ulEndTime < 200 || pStep->ulEndTime > 1000)
			{//$1013
				//&& m_strLastErrorString.Format("Step %d의 ESR 측정가능 종료 시간은 2~10sec 입니다.", i+1);
				m_strLastErrorString.Format(Fun_FindMsg("EditorDoc_StepValidityCheck_msg1","EDITOR_DOC"), i+1);
				return -1;
			}
#endif
		} //Impedance 
	
		if(pStep->chType == PS_STEP_PATTERN)
		{
			//ljb 201012 전압 High < Low 에러
			if (pStep->fEndV_L > pStep->fEndV_H)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg28","EDITOR_DOC"), i+1);
				return -1;
			}
			////////////////////////////////

			nPatternStepcount++;
			if(nPatternStepcount > PS_MAX_PATTERN_STEP_COUNT)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg29","EDITOR_DOC"),
					PS_MAX_PATTERN_STEP_COUNT);
				return -1;
			}

			if(strlen(pStep->szSimulFile) <= 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg30","EDITOR_DOC"), i+1);
				return -1;
			}					

			//ljb 2011317 이재복 ////////// s
			if (pStep->chMode == PS_MODE_CV)
			{
				//+2015.9.22 USY Del For PatternCV(김재호K)
				//패턴CV에서는 체크하지 말자.
// 				if(pStep->fVref_Charge <= 0 || pStep->fVref_Charge > m_lMaxCurrent)
// 				{
// 					m_strLastErrorString.Format("Step %d의 충전 전류값이 설정 되지 않았거나 범위를 벗어났습니다.(0 ~ %dA)", i+1, m_lMaxCurrent/1000);			
// 					return -1;
// 				}
// 				if(pStep->fVref_DisCharge < 0 || pStep->fVref_DisCharge > m_lMaxCurrent)
// 				{
// 					m_strLastErrorString.Format("Step %d의 방전 전류값이 설정 되지 않았거나 범위를 벗어났습니다.(0 ~ %dA)", i+1, m_lMaxCurrent/1000);			
// 					return -1;
// 				}
				//-
			}
			else
			{
				if(pStep->fVref_Charge <= 0 || pStep->fVref_Charge > m_lMaxVoltage)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg31","EDITOR_DOC"), i+1, m_lMaxVoltage/1000);			
					return -1;
				}
				if(pStep->fVref_DisCharge < 0 || pStep->fVref_DisCharge > m_lMaxVoltage)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg32","EDITOR_DOC"), i+1, m_lMaxVoltage/1000);			
					return -1;
				}
			}
			//ljb 2011317 이재복 ////////// e

			if(pStep->lValueLimitLow < 0)
			{
			//	CString strError;
			//	strError.Format("%d,%d / %d,%d",m_lMaxCurrent, m_nMinRefI,pStep->fValueLimitHigh,pStep->fValueLimitLow);
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg33","EDITOR_DOC"), i+1);								
				return -1;
			}

			if(((float)m_lMaxCurrent/ 1000.0f * m_lMaxVoltage) < pStep->lValueLimitHigh && pStep->chMode == PS_MODE_CP)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg34","EDITOR_DOC"), i+1,  (float)m_lMaxCurrent/1000.0f * m_lMaxVoltage );			
				return -1;
			}

			if (bUseMaxPower)
			{
				if(m_lUserMaxPower < (pStep->lValueLimitHigh / 1000) && pStep->chMode == PS_MODE_CP)	//ljb 20130722 add 
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg35","EDITOR_DOC"), i+1,  m_lUserMaxPower );			
					return -1;
				}
			}
		
			//최소 한가지 이상의 종료조건이 설정 되어야 한다.
			if( pStep->fEndC <= 0 && pStep->fEndDI <= 0 &&
				pStep->fEndDV <=0 && pStep->fEndI <=0 && 
				pStep->fEndV_H <=0 && pStep->fEndV_L <=0 && pStep->nValueStepNo == 0)
			{
				if (pStep->ulEndTime == 0 && pStep->ulEndTimeDay == 0 )
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg36","EDITOR_DOC"), i+1);			
					return -1;
				}
			}

		}
			
		//2014.09.02 PS_STEP_USER_MAP 추가함.
		if(pStep->chType == PS_STEP_USER_MAP)
		{
			//ljb 201012 전압 High < Low 에러
			if (pStep->fEndV_L > pStep->fEndV_H)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg37","EDITOR_DOC"), i+1);
				return -1;
			}
			////////////////////////////////
			
			//2014.09.02 nUserMapCnt 유저 카운터는 패턴 카운터 와 동일하게 설정했음.			
			nUserMapCnt++;
			if(nUserMapCnt > PS_MAX_PATTERN_STEP_COUNT)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg38","EDITOR_DOC"),
					PS_MAX_PATTERN_STEP_COUNT);
				return -1;
			}
			
			if(strlen(pStep->szUserMapFile) <= 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg39","EDITOR_DOC"), i+1);
				return -1;
			}
			//ljb 2011317 이재복 ////////// s
			if(pStep->fVref_Charge <= 0 || pStep->fVref_Charge > m_lMaxVoltage)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg40","EDITOR_DOC"), i+1, m_lMaxVoltage/1000);			
				return -1;
			}
			if(pStep->fVref_DisCharge < 0 || pStep->fVref_DisCharge > m_lMaxVoltage)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg41","EDITOR_DOC"), i+1, m_lMaxVoltage/1000);			
				return -1;
			}
			//ljb 2011317 이재복 ////////// e
			
			if(pStep->lValueLimitLow < 0)
			{
				//	CString strError;
				//	strError.Format("%d,%d / %d,%d",m_lMaxCurrent, m_nMinRefI,pStep->fValueLimitHigh,pStep->fValueLimitLow);
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg42","EDITOR_DOC"), i+1);								
				return -1;
			}
			
			if(((float)m_lMaxCurrent/ 1000.0f * m_lMaxVoltage) < pStep->lValueLimitHigh && pStep->chMode == PS_MODE_CP)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg43","EDITOR_DOC"), i+1,  (float)m_lMaxCurrent/1000.0f * m_lMaxVoltage );			
				return -1;
			}
			
			if (bUseMaxPower)
			{
				if(m_lUserMaxPower < (pStep->lValueLimitHigh / 1000) && pStep->chMode == PS_MODE_CP)	//ljb 20130722 add 
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg44","EDITOR_DOC"), i+1,  m_lUserMaxPower );			
					return -1;
				}
			}
			
			//최소 한가지 이상의 종료조건이 설정 되어야 한다.
			if( pStep->fEndC <= 0 && pStep->fEndDI <= 0 &&
				pStep->fEndDV <=0 && pStep->fEndI <=0 && 
				pStep->fEndV_H <=0 && pStep->fEndV_L <=0 && pStep->nValueStepNo == 0)
			{
				if (pStep->ulEndTime == 0 && pStep->ulEndTimeDay == 0 )
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg45","EDITOR_DOC"), i+1);			
					return -1;
				}
			}
			
		}

		//ljb 2011318 이재복 ////////// s
		if(pStep->chType == PS_STEP_EXT_CAN)
		{
			//ljb 201012 전압 High < Low 에러
			if (pStep->fEndV_L > pStep->fEndV_H)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg46","EDITOR_DOC"), i+1);
				return -1;
			}
			////////////////////////////////
			
			if(pStep->fIref < m_nMinRefI)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg47","EDITOR_DOC"), i+1, m_nMinRefI);			
				return -1;
			}
			//ljb 2011317 이재복 ////////// s
			if(pStep->fVref_Charge <= 0 || pStep->fVref_Charge > m_lMaxVoltage)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg48","EDITOR_DOC"), i+1, m_lMaxVoltage/1000);			
				return -1;
			}
			if(pStep->fVref_DisCharge < 0 || pStep->fVref_DisCharge > m_lMaxVoltage)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg49","EDITOR_DOC"), i+1, m_lMaxVoltage/1000);			
				return -1;
			}
			//ljb 2011317 이재복 ////////// e
			
			//if(pStep->lValueLimitLow < 0 || pStep->fIref <= 0 || pStep->fIref > m_lMaxCurrent )
			if(pStep->fIref <= 0 || pStep->fIref > m_lMaxCurrent )
			{
				//	CString strError;
				//	strError.Format("%d,%d / %d,%d",m_lMaxCurrent, m_nMinRefI,pStep->fValueLimitHigh,pStep->fValueLimitLow);
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg50","EDITOR_DOC"), i+1, m_lMaxCurrent);								
				return -1;
			}
			
			if(((float)m_lMaxCurrent/ 1000.0f * m_lMaxVoltage) < pStep->fPref )
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg51","EDITOR_DOC"), i+1,  ((float)m_lMaxCurrent/1000.0f) * ((float)m_lMaxVoltage/1000.0f));			
				return -1;
			}
			
			
			//최소 한가지 이상의 종료조건이 설정 되어야 한다.
			if( pStep->fEndC <= 0 && pStep->fEndDI <= 0 &&
				pStep->fEndDV <=0 && pStep->fEndI <=0 && 
				pStep->fEndV_H <=0 && pStep->fEndV_L <=0 && pStep->nValueStepNo == 0)
			{
				if (pStep->ulEndTime == 0 && pStep->ulEndTimeDay == 0)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg52","EDITOR_DOC"), i+1);			
					return -1;
				}
			}
			
		}
//ljb 2011318 이재복 ////////// e
		if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
		{
			//formation에서는 CC나 CV에 
#ifdef _FORMATION_
			if(m_lLoadedProcType != PS_PGS_AGING)
			{
				if(pStep->chMode == PS_MODE_CC && pStep->fEndV <= 0.0f)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg53"), i+1);
					return -1;
				}
				if(pStep->chMode == PS_MODE_CV && pStep->fEndI <= 0.0f)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg54"), i+1);
					return -1;
				}
			}
#endif
			//전류 설정값은 반드시 입력해야 한다.
			if(pStep->chMode != PS_MODE_CP && pStep->chMode != PS_MODE_CR)
			{
				if(m_lLoadedProcType == PS_PGS_AGING )
				{
		//			if(pStep->fIref > m_lMaxCurrent1)
		//			{
		//				m_strLastErrorString.Format("Step %d의 설정전류가 최대 전류값을 초과하였습니다.(Aging 공정 최대 전류 %dmA)", i+1, m_lMaxCurrent1);			
		//				return -1;
		//			}
				}
				else
				{
					if(pStep->fIref < m_nMinRefI)
					{
						m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg55","EDITOR_DOC"), i+1, m_nMinRefI);			
						return -1;
					}
				}
				
				
				if(pStep->fIref <= pStep->fEndI || pStep->fIref <= pStep->fILimitLow )
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg56","EDITOR_DOC"), i+1);			
					return -1;
				}
				//ljb 2011322 이재복 ////////// s
				if(PS_MODE_CCCP == pStep->chMode && (pStep->fPref > (float)m_lMaxCurrent/ 1000.0f * m_lMaxVoltage  || pStep->fPref <= 0.0f))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg57","EDITOR_DOC"), i+1,  (float)m_lMaxCurrent/1000.0f * m_lMaxVoltage );			
					return -1;
				}
				//ljb 2011322 이재복 ////////// e

			}
			else	//fPRef에 CP, CR제어값이 들어 있다.
			{
				if(PS_MODE_CP == pStep->chMode && (pStep->fPref > (float)m_lMaxCurrent/ 1000.0f * m_lMaxVoltage  || pStep->fPref <= 0.0f))
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg58","EDITOR_DOC"), i+1,  (float)m_lMaxCurrent/1000.0f * m_lMaxVoltage );			
					return -1;
				}

				if(PS_MODE_CR == pStep->chMode && pStep->fRref <= 0.0f)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg59","EDITOR_DOC"), i+1);			
					return -1;
				}
			}

			float OverChargerMinVolt = 0; //20180810 yulee //yulee 20190904 //yulee 20191017 OverChargeDischarger Mark
			OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);

			if(bOverChargerSet)
			{
				if( pStep->fEndC <= 0 && pStep->fEndDI <= 0 &&
					pStep->fEndDV <=0 && pStep->fEndI <=0 && 
					/*pStep->fEndV_H <=0 && pStep->fEndV_L < 0 && pStep->nValueStepNo == 0)*/
					pStep->fEndV_H <=0 && pStep->fEndV_L <-OverChargerMinVolt && pStep->nValueStepNo == 0) // 20180514 yulee 과충방전기 종료조건 설정
				{
					if (pStep->ulEndTime == 0 && pStep->ulEndTimeDay == 0)
					{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg60","EDITOR_DOC"), i+1);			
						return -1;
					}
				}

				BOOL bDetectNoEndCond; //yulee 20190904 //yulee 20191017 OverChargeDischarger Mark
				bDetectNoEndCond = 0;
				bDetectNoEndCond = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "DetectNoEndCond", 0);  //yulee 20190904
			
				if(bDetectNoEndCond == TRUE)
				{
					m_strLastErrorString.Format("Step %d의 종료 조건이 설정되어 있지 않았습니다.", i+1);			
					return -1;
				}
			}
			else
			{
				//최소 한가지 이상의 종료조건이 설정 되어야 한다.
				if( pStep->fEndC <= 0 && pStep->fEndDI <= 0 &&
					pStep->fEndDV <=0 && pStep->fEndI <=0 && 
					pStep->fEndV_H <=0 && pStep->fEndV_L <=0 && pStep->nValueStepNo == 0)
				{
					if (pStep->ulEndTime == 0 && pStep->ulEndTimeDay == 0)
					{
						m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg60","EDITOR_DOC"), i+1);			
						return -1;
					}
				}
			}

/*			if(pStep->fEndDV > 0)		//2 Step Charge 
			{
				if(pStep->fEndDI <= 0.0f)
				{
					m_strLastErrorString.Format("Step %d의 2단계 전류값이 설정되지 않았습니다.", i+1);			
					return -1;
				}
				//End I, End V, End Time , End C
				if(pStep->fDeltaI <= 0.0f && pStep->fDeltaV <= 0.0f && pStep->lDeltaTime == 0 && pStep->lDeltaTime1 == 0)
				{
					m_strLastErrorString.Format("Step %d의 2단계 종료값이 설정되지 않았습니다.", i+1);			
					return -1;
				}
			}
*/

#ifdef _EDLC_CELL_
			//EDLC시 용량 검사 전압 Check
				if(pStep->fCapaVoltage1 > pStep->fCapaVoltage2)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg61","EDITOR_DOC"), i+1);			
					return -1;
				}

				if(pStep->fCapaVoltage1 > 0.0f && pStep->fCapaVoltage2 <= 0.0f)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg62","EDITOR_DOC"), i+1);			
					return -1;
				}

				if(pStep->fCapaVoltage1 <= 0.0f && pStep->fCapaVoltage2 > 0.0f)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg63","EDITOR_DOC"), i+1);			
					return -1;
				}

				if(m_lLoadedProcType == PS_PGS_FORMATION )
				{
					if(pStep->fCapaVoltage1 == 0.0f && pStep->fCapaVoltage2 == 0.0f)
					{
						m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg64","EDITOR_DOC"), i+1);			
						strTemp.Format(Fun_FindMsg("StepValidityCheck_v100B_msg5","EDITOR_DOC"), i+1);								
						if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDNO)
						{
							return -1;
						}
					}
				}
#endif

			for(k=0; k<SCH_MAX_COMP_POINT; k++)
			{
				//전압 상하한 크기 비교
				if(pStep->fCompVLow[k] > 0.0 && pStep->fCompVHigh[k] > 0.0)
				{
					if(pStep->fCompVLow[k] > pStep->fCompVHigh[k])
					{
						m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg65","EDITOR_DOC"), i+1, k+1 );			
						return -1;
					}
				}

				//전류 상하한 크기 비교
				if( pStep->fCompILow[k] > 0.0 && pStep->fCompIHigh[k] > 0.0)
				{
					if( pStep->fCompILow[k] > pStep->fCompIHigh[k])
					{
						m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg66","EDITOR_DOC"), i+1, k+1 );			
						return -1;
					}
				}
			}
			
			//Soc rate 설정
			if(pStep->nValueRateItem > 0 && pStep->fValueRate>0.0f)
			{
				STEP *pCapStep = GetStepData(pStep->nValueStepNo);
				if(pCapStep == NULL )//|| pCapStep->nValueRateItem < 1)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg67","EDITOR_DOC"), i+1, pStep->nValueStepNo);			
					return -1;
				}
			}
			// DC Loader check [4/22/2015 이재복]
			if(pStep->nValueLoaderItem == 1 && pStep->fValueRate>0.0f)
			{
				if (pStep->nValueLoaderMode == 0)
				{
					if(pStep->fValueLoaderSet > ((m_lMaxCurrent / 1000) * (m_lMaxVoltage / 1000)))
					{
					//m_strLastErrorString.Format("Step %d의 DC Loader Power 값 이상.", i+1);	
						m_strLastErrorString.Format("Step %d DC Loader Power value error.", i+1);	//언어파일작업필요.
						return -1;
					}
				}
			}

		}	//Charge Discharge

		
		if(pStep->chType == PS_STEP_CHARGE)
		{
			//End 전압을 정격전압보다 최소 10mV이하 낮게 설정하여야 한다.
			//End 전압은 CC모드 입력에서만 가능한데 CC동작을 위해서는 CV 전압이 더 높게 설정되어야 하므로 
//			if(pStep->fEndV >= m_lMaxVoltage-10)
//			{
//				m_strLastErrorString.Format("Step %d의 종료 전압이 정격 전압(%dmV)에 비해 높게 설정 되었습니다. 정격전압보다 최소 10mV이하 범위값으로 설정하십시오.", i+1, m_lMaxVoltage);			
//				return -1;				
//			}

			if (bUseMaxPower)
			{
				if (pStep->chMode == PS_MODE_CCCV && m_lUserMaxPower < ((pStep->fIref / 1000) * (pStep->fVref_Charge / 1000)))	//ljb 20130722 add
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg68","EDITOR_DOC"), i+1,(pStep->fIref * pStep->fVref_Charge) / 1000000.0f, m_lUserMaxPower);
					return -1;
				}
				if (pStep->chMode == PS_MODE_CC && m_lUserMaxPower < ((pStep->fIref / 1000) * (pStep->fVref_Charge / 1000)))	//ljb 20130722 add
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg69","EDITOR_DOC"), i+1,(pStep->fIref * pStep->fVref_Charge) / 1000000.0f, m_lUserMaxPower);
					return -1;
				}
				if (pStep->chMode == PS_MODE_CP && m_lUserMaxPower < (pStep->fPref / 1000))	//ljb 20130722 add
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg70","EDITOR_DOC"), i+1, pStep->fPref / 1000, m_lUserMaxPower);
					return -1;
				}

			}
			
			if(pStep->fVref_Charge <= 0.0f)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg71","EDITOR_DOC"), i+1);			
				return -1;
			}
			if(pStep->fVref_Charge <= pStep->fEndDV)	
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg72","EDITOR_DOC"), i+1);			
				return -1;				
			}
			
			if(pStep->fVref_Charge <= pStep->fEndV_H)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg73","EDITOR_DOC"), i+1);		//ljb v1009		
				return -1;
			}
			if(pStep->fVref_Charge <= pStep->fEndV_L)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg74","EDITOR_DOC"), i+1);		//ljb v1009		
				return -1;
			}

			//20180808 yulee step의 refI와 비교하는 값은 reg의 Max Current 이다.(병렬일 때도 동일) 
			long ltmpMaxCurrentComp = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);

			//if(pStep->fIref > m_lMaxChargeCurrent * 2 && pStep->chMode != PS_MODE_CP) //병렬 모드를 진행시 설정 값의 2배로 까지 가능하다.
			if(pStep->fIref > ltmpMaxCurrentComp && pStep->chMode != PS_MODE_CP) 
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg75","EDITOR_DOC"),
					i+1, ltmpMaxCurrentComp ); 
				return -1;
			}

			if(pStep->fVref_Charge > m_lMaxVoltage)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg76","EDITOR_DOC"), i+1,  m_lMaxVoltage/1000 );			
				return -1;
			}

		} //charge
			

		//CC 방전일 경우 종료 전압은 Ref 전압 보다 커야 한다. 
		if(pStep->chType == PS_STEP_DISCHARGE)
		{
			if (bUseMaxPower)
			{
				if (pStep->chMode == PS_MODE_CCCV && m_lUserMaxPower < ((pStep->fIref / 1000) * (pStep->fVref_DisCharge / 1000)))	//ljb 20130722 add
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg77","EDITOR_DOC"), i+1,(pStep->fIref * pStep->fVref_DisCharge) / 1000000.0f, m_lUserMaxPower);
					return -1;
				}
				if (pStep->chMode == PS_MODE_CC && m_lUserMaxPower < ((pStep->fIref / 1000) * (pStep->fVref_DisCharge / 1000)))	//ljb 20130722 add
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg78","EDITOR_DOC"), i+1,(pStep->fIref * pStep->fVref_DisCharge) / 1000000.0f, m_lUserMaxPower);
					return -1;
				}
				if (pStep->chMode == PS_MODE_CP && m_lUserMaxPower < (pStep->fPref / 1000))	//ljb 20130722 add
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg79","EDITOR_DOC"), i+1, pStep->fPref/ 1000, m_lUserMaxPower);
					return -1;
				}
			}

			if(pStep->fVref_DisCharge > 0 && pStep->fEndV_H > 0) //&& pStep->chMode == PS_MODE_CC)		//Vref가 입력된 경우 
			{
				if (pStep->chMode == PS_MODE_CP)
				{

				}
				else
				{
					if(pStep->fVref_DisCharge >= pStep->fEndV_H)
					{
						m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg80","EDITOR_DOC"), i+1);		//ljb v1009
						return -1;
					}
				}
			}
			if(pStep->fVref_DisCharge > 0 && pStep->fEndV_L > 0) //&& pStep->chMode == PS_MODE_CC)		//Vref가 입력된 경우 
			{
				if(pStep->fVref_DisCharge >= pStep->fEndV_L)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg81","EDITOR_DOC"), i+1);		//ljb v1009
					return -1;
				}
			}

			//20180808 yulee step의 refI와 비교하는 값은 reg의 Max Current 이다.(병렬일 때도 동일) 
			long ltmpMaxCurrentComp = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);

			//if(pStep->fIref > m_lMaxCurrent * 2 && pStep->chMode != PS_MODE_CP) //병렬 모드를 진행시 설정 값의 2배로 까지 가능하다.
			if(pStep->fIref > ltmpMaxCurrentComp && pStep->chMode != PS_MODE_CP) //병렬 모드를 진행시 설정 값의 2배로 까지 가능하다.
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg82","EDITOR_DOC"),
					i+1, ltmpMaxCurrentComp ); 
				return -1;
			}

			if (pStep->fEndC < 0 || pStep->fEndWh < 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg83","EDITOR_DOC"),i+1); 
				return -1;
			}

			if(pStep->fVref_DisCharge > m_lMaxVoltage)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg84","EDITOR_DOC"), i+1,  m_lMaxVoltage/1000 );			
				return -1;
			}

		} //Discharge

		//Rest는 반드시 종료 시간이 입력 되어야 한다.
		if(pStep->chType == PS_STEP_REST)
		{
//ljb 2009-07-21 삭제
// 			if(pStep->fEndV_H > 0.0f && pStep->nLoopInfoGotoStep > 0)
// 			{
// 				STEP * pTemp = GetStepData(pStep->nLoopInfoGotoStep);
// 				if(pTemp == NULL || (pTemp->chType != PS_STEP_ADV_CYCLE))
// 				{
// 					m_strLastErrorString.Format("Step %d [분기전압H]의 Goto 정보는 [Cycle] Step을 지정하여야 합니다.", i+1);
// 					return -1;
// 				}
// 			}
// 			if(pStep->fEndV_L > 0.0f && pStep->nLoopInfoCycle > 0)
// 			{
// 				STEP * pTemp = GetStepData(pStep->nLoopInfoCycle);
// 				if(pTemp == NULL || (pTemp->chType != PS_STEP_ADV_CYCLE))
// 				{
// 					m_strLastErrorString.Format("Step %d [분기전압L]의 Goto 정보는 [Cycle] Step을 지정하여야 합니다.", i+1);
// 					return -1;
// 				}
// 			}
			if(pStep->ulEndTime == 0 && pStep->ulEndTimeDay == 0 )
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg85","EDITOR_DOC"), i+1);
				return -1;
			}
			if(pStep->ulEndTimeDay < pStep->nWaitTimeDay )
			{
				//m_strLastErrorString.Format("Step %d의 종료시간은 시작시간 설정 시간 보다 커야 합니다.", i+1);
				m_strLastErrorString.Format("Step %d end time is setup time shall be greater than start time.", i+1); //언어파일작업필요.
				return -1;
			}
		} //rest


#ifdef _CYCLER_
		//Loop는 반드시 Goto와 Cycle 횟수가 입력 되어야 한다.
		if(pStep->chType == PS_STEP_LOOP)
		{

#ifdef _EDLC_CELL_
			//EDLC는 최대 10만회 Cycle 지원 
			if(pStep->nLoopInfoCycle<=0 || pStep->nLoopInfoCycle > 100000)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg86","EDITOR_DOC"), i+1);
				return -1;
			}
#else
			//기타 Cell인 경우는 1만회 지원 
			if(pStep->nLoopInfoCycle<=0 || pStep->nLoopInfoCycle > 10000)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg87","EDITOR_DOC"), i+1);
				return -1;
			}
#endif
			int nStep = 0;
			if(pStep->nLoopInfoGotoStep == 0 || pStep->nLoopInfoGotoStep == PS_STEP_NEXT)
			{
				nStep = i+2;
			}
			else
			{
				nStep = pStep->nLoopInfoGotoStep;
			}

			pTempStep = GetStepData(nStep);
			if(pTempStep == NULL || (pTempStep->chType != PS_STEP_ADV_CYCLE &&  pTempStep->chType != PS_STEP_END))
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg88","EDITOR_DOC"), i+1);
				return -1;
			}
			
		} //loop
#endif
	
		//Oven연동을 위한 동작 시간 검사
		if(pStep->fTref != 0.0f && ((pStep->ulEndTime * (pStep->ulEndTimeDay * 86400)) > 0 && pStep->ulEndTime < 500))	//온도 설정시 최소 5초 이상 step만 설정 가능
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg89","EDITOR_DOC"), i+1);
			AfxMessageBox(m_strLastErrorString);
		}

		//전압 설정값 입력 범위 검사
		if(  pStep->fVref_Charge < 0.0f)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg90","EDITOR_DOC"), i+1);
			return -1;
		}

		float OverChargerMinVolt = 0; //20180810 yulee //yulee 20191017 OverChargeDischarger Mark
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);

		float fMinDischargeValue;
		
		if(bOverChargerSet)
			fMinDischargeValue = -OverChargerMinVolt;
		else
			fMinDischargeValue = 0.0f;
			
		if(  pStep->fVref_DisCharge < fMinDischargeValue)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg91","EDITOR_DOC"), i+1);
			return -1;
		}
		//HIGH 전압 종료값 입력 범위 검사
		if(pStep->fEndV_H < 0.0f)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg92","EDITOR_DOC"), i+1);
			return -7;
		}
		//LOW 전압 종료값 입력 범위 검사

		if(bOverChargerSet)
			fMinDischargeValue = -OverChargerMinVolt;
		else
			fMinDischargeValue = 0.0f;
			
			
		if(pStep->fEndV_L < fMinDischargeValue)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg93","EDITOR_DOC"), i+1);		//ljb v1009
			return -7;
		}
		//전류 종료값 입력 범위 검사
		if(pStep->fEndI < 0.0f)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg94","EDITOR_DOC"), i+1);
			return -8;
		}

		//전압 제한값 입력 범위 검사
		if(pStep->fVLimitLow < 0)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg95","EDITOR_DOC"), i+1);
			return -1;
		}

		//전류 제한값 입력 검사 
		if(pStep->fILimitLow < 0)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg96","EDITOR_DOC"), i+1, 1);
			return -1;
		}
		
		//전압 제한 상한값을 입력하였을 경우 전압 설정값보다 반드시 커야 한다.
		if(pStep->fVLimitHigh > 0)
		{
			if (pStep->chMode == PS_MODE_CP)
			{

			}
			else
			{
				if(pStep->fVLimitHigh <= pStep->fVref_Charge)
				{
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg97","EDITOR_DOC"), i+1);
					return -1;
				}
			}
		}

		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		if(pStep->fILimitHigh > 0 && pStep->chType != PS_STEP_PATTERN)
		{
			if(pStep->fILimitHigh <= pStep->fIref || pStep->fILimitHigh <=pStep->lValueLimitHigh)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg98","EDITOR_DOC"), i+1);
				return -1;
			}
		}

		//전체 안전전류 상한 값이 스텝 설정 전류 값 보다 커야 한다. //yulee 20180729
		if(pStep->fIref > m_lMaxCurrent && pStep->chMode != PS_MODE_CP) //$1013
		{
			//&& m_strLastErrorString.Format("Step %d의 설정 전류가 안전 전류 상한값보다 큽니다.", i+1);
			m_strLastErrorString.Format(Fun_FindMsg("EditorDoc_StepValidityCheck_msg2","EDITOR_DOC"), i+1);
			return -1;
		}

		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		if(pStep->fILimitHigh > 0 && pStep->chType == PS_STEP_PATTERN)
		{
			//ksj 20200825 : 임시 주석처리. 패턴에 있는 출력값이 전류인지 파워인지 구분이 안됨. 아래는 패턴에있는 파워조건도 전류 상한이랑 비교해서 제대로 체크가 안됨.
			//향후 패턴이 파워인지 전류인지 구분해서 안전조건 체크하게 개선해야됨.
			/*if(pStep->fILimitHigh <=pStep->lValueLimitHigh && pStep->chMode == PS_MODE_CC)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg99","EDITOR_DOC"), i+1);
				return -1;
			}*/
		}

		//HIGH 종료 전압값은 전압 제한 하한값보다 커야 한다.
		if(pStep->fVLimitLow > 0 && pStep->fEndV_H > 0)
		{
			if(pStep->fVLimitLow > pStep->fEndV_H)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg100","EDITOR_DOC"), i+1);
				return -1;
			}
		}
		//LOW 종료 전압값은 전압 제한 하한값보다 커야 한다.
		if(pStep->fVLimitLow > 0 && pStep->fEndV_L > 0)
		{
			if(pStep->fVLimitLow > pStep->fEndV_L)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg101","EDITOR_DOC"), i+1);		//ljb v1009
				return -1;
			}
		}

		//용량 상한값이 하한값보다 커야 한다.
		if(pStep->fCLimitHigh > 0.0f && pStep->fCLimitLow> 0.0f && pStep->fCLimitHigh < pStep->fCLimitLow)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg102","EDITOR_DOC"), i+1);
			return -1;
		}

		//용량 상한값이 하한값보다 커야 한다.
		if(pStep->fCapacitanceHigh > 0.0f && pStep->fCapacitanceLow> 0.0f && pStep->fCapacitanceHigh < pStep->fCapacitanceLow)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg103","EDITOR_DOC"), i+1);
			return -1;
		}

		//전압 상한 값이 하한값보다 커야 한다.
		if(pStep->fVLimitHigh > 0.0f && pStep->fVLimitLow > 0.0f && pStep->fVLimitHigh < pStep->fVLimitLow)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg104","EDITOR_DOC"), i+1);
			return -1;
		}

		//전류 상한 값이 하한값보다 커야 한다.
		if(pStep->fILimitHigh > 0 && pStep->fILimitLow > 0 && pStep->fILimitHigh < pStep->fILimitLow)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg105","EDITOR_DOC"), i+1);
			return -1;
		}

		//임피던스 상한 값이 하한값보다 커야 한다.
		if(pStep->fImpLimitHigh > 0 && pStep->fImpLimitLow > 0 && pStep->fImpLimitHigh < pStep->fImpLimitLow)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg106","EDITOR_DOC"), i+1);
			return -1;
		}

		//온도 상한 값이 하한값보다 커야 한다.
		if(pStep->fTempHigh > 0 && pStep->fTempLow > 0 && pStep->fTempHigh < pStep->fTempLow)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg107","EDITOR_DOC"), i+1);
			return -1;
		}
	
		//Cycler만 1초 간격 저장 가능 하다. 
#ifndef _CYCLER_
		if(pStep->ulReportTime != 0 && pStep->ulReportTime < 2)		//1초 설정 
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg108"), i+1);
			return -1;
		}
#endif

#ifdef _CYCLER_
//저장 조건 설정 검사 
		if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE || pStep->chType == PS_STEP_REST)
		{
			if (pStep->fEndC < 0 || pStep->fEndWh < 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg109","EDITOR_DOC"),i+1); 
				return -1;
			}
			if(pStep->fReportI == 0.0f && pStep->fReportV == 0.0f && pStep->fReportTemp == 0.0f && pStep->ulReportTime == 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg110","EDITOR_DOC"), i+1);
				AfxMessageBox(m_strLastErrorString);
			}
		}
#endif
		//ljb 20110103 can, aux 조건 검사
		for( k=0; k<SCH_MAX_GRADE_STEP; k++)
		{
			//CAN 분류 번호 비교
			if(pStep->ican_function_division[k] != 0 && (pStep->ican_function_division[k] < 1 || pStep->ican_function_division[k] > 2001))
			{
				if (pStep->ican_function_division[k] > 9999)	//ljb 20170328 add
				{
					
				}
				else
				{
					//&& m_strLastErrorString.Format("Step %d 의 %d번째 CAN 분류코드 범위가 1000 ~ 1200 이어야 합니다.", i+1, k+1 );			
					m_strLastErrorString.Format(Fun_FindMsg("EditorDoc_StepValidityCheck_msg3","EDITOR_DOC"), i+1, k+1 );
					//$1013 주석해제 ??? m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg111"), i+1, k+1 );			

					return -1;
				}
			}
			//ljb 20140109 주석 처리 , CAN Data Type 비교
// 			if(pStep->ican_function_division[k] != 0 && pStep->cCan_data_type[k] == 0)
// 			{
// 				m_strLastErrorString.Format("Step %d 의 %d번째 CAN Data Type을 설정 하셔야 합니다.", i+1, k+1 );			
// 				return -1;
// 			}
			//CAN Compare 비교
			if(pStep->ican_function_division[k] != 0 && pStep->cCan_compare_type[k] == 0)
			{
				if(pStep->ican_function_division[k] >= 1927 && pStep->ican_function_division[k] <= 1928) //yulee 20181107
				{
					
				}
				//ksj 20210105 : 10010 은 tx인데 compare 안함. 일단 10010 이상은 compare 없어도 저장되게 임시 긴급 조치
				else if(pStep->ican_function_division[k] >= 10010)
				{

				}
				//ksj end
				else
				{
					//&& m_strLastErrorString.Format("Step %d 의 %d번째 CAN 비교 Type을 설정 하셔야 합니다.", i+1, k+1 );
					m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg112","EDITOR_DOC"), i+1, k+1 );
					//$1013 주석해제??? 	m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg112"), i+1, k+1 );			
					return -1;
				}
			}
			
			//AUX 분류 번호 비교
			if(pStep->iaux_function_division[k] != 0 && (pStep->iaux_function_division[k] < 1 || pStep->iaux_function_division[k] > 3001))
			{
				//if (pStep->ican_function_division[k] > 9999)	//ljb 20170328 add
				if (pStep->iaux_function_division[k] > 9999)	//ksj 20171103 : 오타 버그 수정.
				{
					
				}
				else
				{
					//&& m_strLastErrorString.Format("Step %d 의 %d번째 AUX 분류코드 범위가 2000 ~ 2200 이어야 합니다.", i+1, k+1 );			
					m_strLastErrorString.Format(Fun_FindMsg("EditorDoc_StepValidityCheck_msg4","EDITOR_DOC"), i+1, k+1 );			
					//$1013 주석해제?? 				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg113"), i+1, k+1 );			

					return -1;
				}
			}
			//ljb 20140109 주석 처리 , AUX Data Type 비교
// 			if(pStep->iaux_function_division[k] != 0 && pStep->cAux_data_type[k] == 0)
// 			{
// 				m_strLastErrorString.Format("Step %d 의 %d번째 AUX Data Type을 설정 하셔야 합니다.", i+1, k+1 );			
// 				return -1;
// 			}
			//AUX Compare 비교
			if(pStep->iaux_function_division[k] != 0 && pStep->cAux_compare_type[k] == 0)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_v100B_msg114","EDITOR_DOC"), i+1, k+1 );			
				return -1;
			}
		}

	}//for	

	return 0;
}

void CCTSEditorProDoc::OnCloseDocument() 
{
	// TODO: Add your specialized code here and/or call the base class
	if(m_bEditedFlag)
		BackUpDataBase();

	RemoveInitStepArray();
	ReMoveStepArray();
	ClearCopyStep();

	RemoveUndoStep();


	DATA_CODE *pData = NULL;
//	for(int i =0; i<m_apProcData.GetSize(); i++)
//	{
//		pData = (DATA_CODE *)m_apProcData[i];
//		if(pData)
//		{
//			delete pData;
//			pData = NULL;
//		}
//	}
//	m_apProcData.RemoveAll();

	SCH_CODE_MSG *pObject;
	for(int i = 0; i<m_apProcType.GetSize(); i++)
	{
		pObject = (SCH_CODE_MSG *)m_apProcType[i];
		if(pObject)
		{
			delete pObject;
			pObject = NULL;
		}
	}
	m_apProcType.RemoveAll();

	for( int i = 0; i<m_apGradeType.GetSize(); i++)
	{
		pObject = (SCH_CODE_MSG *)m_apGradeType[i];
		if(pObject)
		{
			delete pObject;
			pObject = NULL;
		}
	}
	m_apGradeType.RemoveAll();
	CDocument::OnCloseDocument();
}

int CCTSEditorProDoc::SaveStepGrade(STEP *pStep, LONG lStepID)
{
	if(pStep == NULL)	return -1;
	if(pStep->chType == PS_STEP_END || 
		pStep->chType == PS_STEP_REST ||
		pStep->chType == PS_STEP_LOOP)		
		return 0;;
	
	//	if((int)pStep->sGrading_Val[0].chTotalGrade == 0)	return 0;
	
	CGradeRecordSet	recordSet;
	recordSet.m_strFilter.Format("[StepID] = %ld", lStepID);
	recordSet.m_strSort.Format("[Value]");
	
	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}
	
	while(!recordSet.IsEOF())	
	{
		try						//Delete All Step Grade
		{
			recordSet.Delete();
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			recordSet.Close();
			return -1;
		}
		recordSet.MoveNext();
	}
	
	CString strTemp;
	strTemp.Format("[StepID] = %ld", lStepID);
	
	if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Delete Error
	{
		recordSet.Close();
		return -1;
	}
	
	//	for(int i = 0; i<(int)pStep->sGrading_Val[0].chTotalGrade ; i++)
	for(int i = 0; i<(int)pStep->sGrading_Val.chTotalGrade ; i++)		//Save All Step
	{
		recordSet.AddNew();
		recordSet.m_StepID = lStepID;
		recordSet.m_GradeItem = pStep->sGrading_Val.lGradeItem[i];
		recordSet.m_Value = pStep->sGrading_Val.faValue1[i];
		recordSet.m_Value1 = pStep->sGrading_Val.faValue2[i];
		
		recordSet.m_GradeCode.Format("%c", pStep->sGrading_Val.aszGradeCode[i]);
		recordSet.m_GradeIndex = i+1;
		recordSet.Update();		
	}
		recordSet.Close();
		return 0;

}

int CCTSEditorProDoc::SavePreTestParam(LONG lTestID)
{

	CString strTemp, strSaveData;

	CPreTestCheckRecordSet	recordSet;
	recordSet.m_strFilter.Format("[TestID] = %ld", lTestID);
	recordSet.m_strSort.Format("[CheckID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}

	if(recordSet.IsBOF())	// Add New
	{
		recordSet.AddNew();
		recordSet.m_TestID = lTestID;
		recordSet.m_AutoTime = (DATE)0;
		recordSet.m_AutoProYN = FALSE;
	}	
	else					//Update
	{
		recordSet.Edit();
	}
// 	recordSet.m_DeltaVoltage = m_sPreTestParam.fDeltaVoltage;
// 	recordSet.m_CurrentRange = m_sPreTestParam.fMaxCurrent;
// 	recordSet.m_MaxV = m_sPreTestParam.fMaxVoltage;
// 	recordSet.m_MinV = m_sPreTestParam.fMinVoltage;
// 	recordSet.m_OCVLimit = m_sPreTestParam.fOCVLimitVal;			//v ref
// 	recordSet.m_TrickleCurrent = m_sPreTestParam.fTrickleCurrent;
// 	recordSet.m_MaxFaultBattery	= m_sPreTestParam.nMaxFaultNo;
// 	recordSet.m_TrickleTime	= m_sPreTestParam.lTrickleTime;
// 	recordSet.m_PreTestCheck =	m_sPreTestParam.bPreTest;

//	if (recordSet.m_SafetyMaxV < -1000000 || recordSet.m_SafetyMaxV > 9000000 ) recordSet.m_SafetyMaxV = 0.0f;
	recordSet.m_SafetyMaxV = m_sPreTestParam.fMaxV;
//	if (recordSet.m_SafetyMinV < -1000000 || recordSet.m_SafetyMinV > 9000000 ) recordSet.m_SafetyMinV = 0.0f;
	recordSet.m_SafetyMinV = m_sPreTestParam.fMinV;
//	if (recordSet.m_SafetyMaxI < -1000000 || recordSet.m_SafetyMaxI > 9000000 ) recordSet.m_SafetyMaxI = 0.0f;
	recordSet.m_SafetyMaxI = m_sPreTestParam.fMaxI;
//	if (recordSet.m_SafetyMinI < -1000000 || recordSet.m_SafetyMinI > 9000000 ) recordSet.m_SafetyMinI = 0.0f;
	recordSet.m_SafetyCellDeltaV = m_sPreTestParam.fCellDataV;

	recordSet.m_DeltaStdMaxV	= m_sPreTestParam.fStdMaxV;
	recordSet.m_DeltaStdMinV	= m_sPreTestParam.fStdMinV;
	recordSet.m_DeltaMaxV		= m_sPreTestParam.fDataMaxV;
	recordSet.m_DeltaMinV		= m_sPreTestParam.fDataMinV;

	recordSet.m_DeltaVent		= m_sPreTestParam.bDataVent;

//	if (recordSet.m_SafetyMaxT < -1000000 || recordSet.m_SafetyMaxT > 9000000 ) recordSet.m_SafetyMaxT = 0.0f;
	recordSet.m_SafetyMaxT = m_sPreTestParam.fMaxT;
//	if (recordSet.m_SafetyMinT < -1000000 || recordSet.m_SafetyMinT > 9000000 ) recordSet.m_SafetyMinT = 0.0f;
	recordSet.m_SafetyMinT = m_sPreTestParam.fMinT;
//	if (recordSet.m_SafetyMaxC < -1000000 || recordSet.m_SafetyMaxC > 9000000 ) recordSet.m_SafetyMaxC = 0.0f;
	recordSet.m_SafetyMaxC = m_sPreTestParam.fMaxC;
//	if (recordSet.m_SafetyMaxW < -1000000 || recordSet.m_SafetyMaxW > 9000000 ) recordSet.m_SafetyMaxW = 0.0f;
	recordSet.m_SafetyMaxW = m_sPreTestParam.lMaxW;
//	if (recordSet.m_SafetyMaxWh < -1000000 || recordSet.m_SafetyMaxWh > 9000000 ) recordSet.m_SafetyMaxWh = 0.0f;
	recordSet.m_SafetyMaxWh = m_sPreTestParam.lMaxWh;

	
	//ljb 20100820 for v100A
	int i;
	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%d",m_sPreTestParam.ican_function_division[i]);
		else strTemp.Format("%d,",m_sPreTestParam.ican_function_division[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strCanCategory = strSaveData;
	
	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%d",m_sPreTestParam.cCan_compare_type[i]);
		else strTemp.Format("%d,",m_sPreTestParam.cCan_compare_type[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strCanCompareType = strSaveData;

	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%d",m_sPreTestParam.cCan_data_type[i]);
		else strTemp.Format("%d,",m_sPreTestParam.cCan_data_type[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strCanDataType = strSaveData;

	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%.3f",m_sPreTestParam.fcan_Value[i]);
		else strTemp.Format("%.3f,",m_sPreTestParam.fcan_Value[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strCanValue = strSaveData;

	//aux
	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%d",m_sPreTestParam.iaux_function_division[i]);
		else strTemp.Format("%d,",m_sPreTestParam.iaux_function_division[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strAuxCategory = strSaveData;

	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%d",m_sPreTestParam.cAux_compare_type[i]);
		else strTemp.Format("%d,",m_sPreTestParam.cAux_compare_type[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strAuxCompareType = strSaveData;

	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%d",m_sPreTestParam.cAux_data_type[i]);
		else strTemp.Format("%d,",m_sPreTestParam.cAux_data_type[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strAuxDataType = strSaveData;

	strSaveData.Empty();
	for( int i=0; i < MAX_COMPARE_STEP_COUNT; i++)
	{
		if (i+1 == MAX_COMPARE_STEP_COUNT) strTemp.Format("%.3f",m_sPreTestParam.faux_Value[i]);
		else strTemp.Format("%.3f,",m_sPreTestParam.faux_Value[i]);
		strSaveData += strTemp;
	}
	recordSet.m_strAuxValue = strSaveData;
	//////////////////////////////////////////////////////////////

	recordSet.Update();
	recordSet.Close();

	memcpy(&m_sInitPreTestParam, &m_sPreTestParam, sizeof(TEST_PARAM));

	return 0;
}

int CCTSEditorProDoc::RequeryStepGrade(STEP *pStep, LONG lStepID)
{
	if(pStep == NULL)	return -1;
	
	CGradeRecordSet	recordSet;
	recordSet.m_strFilter.Format("[StepID] = %ld", lStepID);
	recordSet.m_strSort.Format("[GradeID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return -1;
	}

	if(recordSet.IsBOF())
	{
		recordSet.Close();
		return -1;		//Zero Grading Step
	}

	//20081208 KHS Grade 레코드는 등급레코드(count) 다음 조건레코드(count)읽어들이는 코드로 변경
	pStep->sGrading_Val.chTotalGrade = recordSet.GetRecordCount();
	recordSet.MoveFirst();
	pStep->sGrading_Val.iGradeCount=1;
	for(int i = 0; i<recordSet.GetRecordCount() ; i++)		//Save All Step
	{
		pStep->sGrading_Val.lGradeItem[i] = recordSet.m_GradeItem; 
		pStep->sGrading_Val.faValue1[i] = recordSet.m_Value; 
		pStep->sGrading_Val.faValue2[i] = recordSet.m_Value1; 	
		pStep->sGrading_Val.aszGradeCode[i] = recordSet.m_GradeCode.GetAt(0);
		if (recordSet.m_GradeCode == "0" || recordSet.m_GradeCode == "&" || recordSet.m_GradeCode == "+")
			pStep->sGrading_Val.iGradeCount=2;
		
		recordSet.MoveNext();
		continue;
	}
	recordSet.Close();
	return 0;
}

int CCTSEditorProDoc::PermissionCheck(int nAction)
{
	return (nAction & m_LoginData.nPermission);
}

BOOL CCTSEditorProDoc::RequeryProcType()
{
	CProcTypeRecordSet	rs;
	rs.m_strFilter.Format("[ProcType] >= %d", (PS_STEP_CHARGE << 16));
	rs.m_strSort.Format("[ProcType]");

	try
	{
		rs.Open();
	}
	catch (CDaoException *e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF())
	{
		rs.Close();
		return FALSE;
	}

	SCH_CODE_MSG	*pObject;
	pObject = new SCH_CODE_MSG;
	pObject->nCode = 0;
	pObject->nData = 0;
	sprintf(pObject->szMessage, Fun_FindMsg("RequeryProcType_msg1","EDITOR_DOC"));
	m_apProcType.Add(pObject);

	while(!rs.IsEOF())
	{
		pObject = new SCH_CODE_MSG;
		ASSERT(pObject);
		ZeroMemory(pObject, sizeof(SCH_CODE_MSG));
		pObject->nCode = rs.m_ProcType;
		pObject->nData = rs.m_ProcID;
		m_apProcType.Add(pObject);
		sprintf(pObject->szMessage, "%s", rs.m_Description);
		TRACE("Proc Type Id %d : %s\n", rs.m_ProcType, rs.m_Description);
		rs.MoveNext();
	}
	rs.Close();
	return TRUE;
}

int CCTSEditorProDoc::GetProcID(int nIndex)
{
	if(nIndex < 0 || nIndex >=m_apProcType.GetSize())	return 0;

	SCH_CODE_MSG *pData = (SCH_CODE_MSG *)m_apProcType[nIndex];
	return pData->nCode;

}

int CCTSEditorProDoc::GetProcIndex(int nID)
{
	SCH_CODE_MSG *pData;

	for(int i =0; i<m_apProcType.GetSize(); i++)
	{
		pData = (SCH_CODE_MSG *)m_apProcType[i];
		if(pData->nCode == nID)
		{
			return i;
		}
	}

	return 0;
}

BOOL CCTSEditorProDoc::AddCopyStep(int nStepNo)
{
	STEP *pStep, *pSourceStep;
	pStep = new STEP;
	ASSERT(pStep);
	ZeroMemory(pStep, sizeof(STEP));

	pSourceStep = GetStepData(nStepNo);

	if(pSourceStep == NULL)		return FALSE;

	pStep->nEditState = COPY_DATA;
	memcpy(pStep, pSourceStep, sizeof(STEP));
	m_apCopyStep.Add(pStep);
	return TRUE;
}

BOOL CCTSEditorProDoc::PasteStep(int nStepNo)
{
	if(nStepNo < 0)		return FALSE;
	
	int nTotStepCount = GetTotalStepNum();

	STEP *pStep, *pSourceStep;
	
	for(int i = 0; i<m_apCopyStep.GetSize(); i++)
	{
		pSourceStep = (STEP *)m_apCopyStep[i];
		if(nTotStepCount >= nStepNo)	
		{
			pStep = new STEP;
			memcpy(pStep, pSourceStep, sizeof(STEP));
			pStep->chStepNo = nStepNo + i;
			m_apStep.InsertAt(nStepNo-1 + i , pStep);

			pSourceStep->chStepNo = pStep->chStepNo;		//Undo 동작을 위해 
		}
		else
		{
			pStep = new STEP;
			memcpy(pStep, pSourceStep, sizeof(STEP));
			
			pStep->chStepNo = nTotStepCount + i + 1;
			m_apStep.Add(pStep);
			pSourceStep->chStepNo = pStep->chStepNo;		//Undo 동작을 위해
		}
	}

	nTotStepCount = GetTotalStepNum();
	for( int i =0; i<nTotStepCount; i++)
	{
		pStep = (STEP *)m_apStep[i];
		TRACE("Step No %d\n", pStep->chStepNo);
		pStep->chStepNo = i+1;
	}

#if _DEBUG
	for(int i = 0; i<m_apCopyStep.GetSize(); i++)
	{
		pSourceStep = (STEP *)m_apCopyStep[i];
		
		TRACE("Copyed Step %d\n", pSourceStep->chStepNo);
	}
#endif

	return TRUE;
}

BOOL CCTSEditorProDoc::InsertStep(int nStepIndex, STEP *pStep)
{
	if(pStep == NULL)	return FALSE;

	int nTotStepCount = GetTotalStepNum();
	if(nStepIndex < 0 || nStepIndex > nTotStepCount)		return FALSE;
	
	STEP *pInsertStep = NULL;
	pInsertStep = new STEP;
	memcpy(pInsertStep, pStep, sizeof(STEP));
	
	if(nTotStepCount > nStepIndex)	
	{
		m_apStep.InsertAt(nStepIndex , pInsertStep);
	}
	else
	{
		m_apStep.Add(pInsertStep);
	}

	nTotStepCount = GetTotalStepNum();
	for(int  i =0; i<nTotStepCount; i++)
	{
		pInsertStep = (STEP *)m_apStep[i];
		pInsertStep->chStepNo = i+1;
	}

	return TRUE;
}

BOOL CCTSEditorProDoc::BackUpDataBase()
{

//	CString strCurFolder = AfxGetApp()->GetProfileString("Path" ,"CTSEditorPro");	//Get Current Folder(PowerFormation Folde)
//	if(strCurFolder.IsEmpty())
//	{
//		char szBuff[256];
//		if(GetCurrentDirectory(511, szBuff) < 1)	//Get Current Direcotry Name
//		{
//			return FALSE;	
//		}
//		strCurFolder = szBuff;
//	}

	CString strCurFolder;
	CString strDBName = GetDataBaseName();
	if(strDBName.IsEmpty())	return FALSE;
	
	char szFrom[256], szTo[256];
	ZeroMemory(szFrom, 256);
	ZeroMemory(szTo, 256);
	
	strCurFolder = strDBName.Left(strDBName.ReverseFind('\\'));
	if(strCurFolder.IsEmpty())	return FALSE;

	
	sprintf(szTo, "%s\\BackUp", strCurFolder);

	CFileStatus status;
	if( CFile::GetStatus(szTo, status)) 
	{
	} 
	else 
	{
		CreateDirectory(szTo, NULL);
	}

	sprintf(szFrom, "%s", strDBName);
	
//	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	if(strCurFolder.IsEmpty())
	{
// 		switch(nSelLanguage) 20190701
// 		{
// 		case 1: sprintf(szTo, "%s\\BackUp\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME); break;
// 		case 2: sprintf(szTo, "%s\\BackUp\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_EN);break;
// 		case 3: sprintf(szTo, "%s\\BackUp\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_PL); break;
// 		default : sprintf(szTo, "%s\\BackUp\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_EN); break;
// 		}
		sprintf(szTo, "%s\\BackUp\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME);
	}

	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
	ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
    lpFileOp->hwnd = NULL; 
    lpFileOp->wFunc = FO_COPY; 
    lpFileOp->pFrom = szFrom; 
    lpFileOp->pTo = szTo; 
    lpFileOp->fFlags = FOF_NOCONFIRMMKDIR|FOF_FILESONLY|FOF_NOCONFIRMATION ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
    lpFileOp->fAnyOperationsAborted = FALSE; 
    lpFileOp->hNameMappings = NULL; 
    lpFileOp->lpszProgressTitle = NULL; 
	
	SHFileOperation(lpFileOp);

	delete lpFileOp;
	lpFileOp = NULL;
	
	return TRUE;
}

//초기 Load한 것과 비교해서 변경 되었는지 확인 
BOOL CCTSEditorProDoc::IsEdited()
{
	if(m_apStep.GetSize() < 1 && m_apInitStep.GetSize() < 1)	return FALSE;

	if(memcmp(&m_sInitPreTestParam, &m_sPreTestParam, sizeof(TEST_PARAM)) != 0)	return TRUE;

	if(m_apInitStep.GetSize() != m_apStep.GetSize() )	return TRUE;

	STEP *pInitStep, *pStep;
	for(int i=0; i<m_apStep.GetSize(); i++)
	{
		pInitStep = (STEP *)m_apInitStep.GetAt(i);
		pStep = (STEP *)m_apStep.GetAt(i);

		if(memcmp(pInitStep, pStep, sizeof(STEP)) != 0)	
		{
//			memcpy(pInitStep, pStep, sizeof(STEP)); //for debugging
			return TRUE;
		}
		
	}

	return FALSE;
}

//Undo 할수 있도록 현재 step의 Data를 Backup한다.
void CCTSEditorProDoc::MakeUndoStep()
{
/*	RemoveUndoStep();

	//copy current data
	STEP *pStep, *pStep2;
	int nStepSize = m_apStep.GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if( ( pStep = (STEP *)m_apStep.GetAt(i) ) != NULL )
		{
			pStep2 =  new STEP;
			memcpy(pStep2, pStep, sizeof(STEP));
			m_apUpdoStep.Add(pStep2);
		}
	}
	*/

	STEP *pStep, *pStep2;
	CPtrArray *pPtrArray;
	POSITION pos;

	//이전에 Undo를 몇번했으면 이후 목록은 삭제한다.
	if(m_posUndoList != m_aPtrArrayList.GetTailPosition())
	{
		pos = m_posUndoList;
		if(pos)
		{
			m_aPtrArrayList.GetNext(pos);
			while(pos)	
			{
				RemoveUndoList(pos);
				m_aPtrArrayList.RemoveAt(pos);
				pos = m_posUndoList;
				m_aPtrArrayList.GetNext(pos);
				TRACE("Delete undo list %d\n", m_aPtrArrayList.GetCount());
			}
		}
		else
		{
			RemoveUndoStep();	//remove all undo step list
		}
	}

	//최대 개수를 넘으면 가장 앞쪽것을 삭제 한다.
	if(m_aPtrArrayList.GetCount() >= MAX_UNDO_COUNT)
	{
		pos = m_aPtrArrayList.GetHeadPosition();
		RemoveUndoList(pos);
		m_aPtrArrayList.RemoveHead();
		
		TRACE("Delete undo list head\n");
	}
	
	//현재 목록을 제일 뒤에 추가한다.
	pPtrArray = new CPtrArray;
	int nStepSize = m_apStep.GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if( ( pStep = (STEP *)m_apStep.GetAt(i) ) != NULL )
		{
			pStep2 =  new STEP;
			memcpy(pStep2, pStep, sizeof(STEP));
			pPtrArray->Add(pStep2);
		}
	}

	m_aPtrArrayList.AddTail(pPtrArray);
	m_posUndoList = m_aPtrArrayList.GetTailPosition();
	TRACE("Add undo list at tail %d\n", m_aPtrArrayList.GetCount());

	return;
}

BOOL CCTSEditorProDoc::UpdoStepEdit(int &nStartStep, int &nEndStep)
{
	STEP *pStep, *pStep2;
	
/*	ReMoveStepArray();

	//copy current data
	int nStepSize = m_apUpdoStep.GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if(( pStep = (STEP *)m_apUpdoStep.GetAt(i) ) != NULL )
		{
			pStep2 =  new STEP;
			memcpy(pStep2, pStep, sizeof(STEP));
			m_apStep.Add(pStep2);
		}
	}

	//1개의 Undo만 지원하므로 return FALSE;
	return FALSE;
*/
	
	if(m_posUndoList == NULL)	return FALSE;				//Undo 할 이전 내용이 없음 
	
	CPtrArray *pPtrArray = (CPtrArray *)m_aPtrArrayList.GetPrev(m_posUndoList);
	if(pPtrArray)
	{
		ReMoveStepArray();

		//copy current data
		int nStepSize = pPtrArray->GetSize();
		for (int i = 0 ; i<nStepSize ; i++)
		{
			if(( pStep = (STEP *)pPtrArray->GetAt(i) ) != NULL )
			{
				pStep2 =  new STEP;
				memcpy(pStep2, pStep, sizeof(STEP));
				m_apStep.Add(pStep2);
			}
		}
	}
	else
	{
		return FALSE;
	}
	TRACE("Undo list %d\n", m_aPtrArrayList.GetCount());
	return TRUE;
}

BOOL CCTSEditorProDoc::RedoStepEdit()
{
	STEP *pStep, *pStep2;

	if(m_posUndoList == NULL)
	{
		m_posUndoList = m_aPtrArrayList.GetHeadPosition();
		if(m_posUndoList == NULL)	return FALSE;				//Undo 할 이전 내용이 없음 
	}
	else
	{
		if( m_posUndoList == m_aPtrArrayList.GetTailPosition())	
		{
			return FALSE;
		}
		else
		{
			m_aPtrArrayList.GetNext(m_posUndoList);
			if(m_posUndoList == NULL)
			{
				return FALSE;
			}
		}
	}

	CPtrArray *pPtrArray = (CPtrArray *)m_aPtrArrayList.GetNext(m_posUndoList);
	if(pPtrArray)
	{
		ReMoveStepArray();

		//copy current data
		int nStepSize = pPtrArray->GetSize();
		for (int i = 0 ; i<nStepSize ; i++)
		{
			if(( pStep = (STEP *)pPtrArray->GetAt(i) ) != NULL )
			{
				pStep2 =  new STEP;
				memcpy(pStep2, pStep, sizeof(STEP));
				m_apStep.Add(pStep2);
			}
		}
	}
	return TRUE;
}

//Remove All Undo Step
void CCTSEditorProDoc::RemoveUndoStep()
{
	m_posUndoList = m_aPtrArrayList.GetHeadPosition();
	while(m_posUndoList)
	{
		RemoveUndoList(m_posUndoList);
		m_aPtrArrayList.RemoveHead();
		m_posUndoList = m_aPtrArrayList.GetHeadPosition();
	}
}



BOOL CCTSEditorProDoc::RemoveUndoList(POSITION pos)
{
	if(pos == NULL)		return FALSE;

	STEP *pStep;
	CPtrArray *pPtrArray = (CPtrArray *)m_aPtrArrayList.GetAt(pos);
	int nStepSize = pPtrArray->GetSize();
	for (int i = 0 ; i<nStepSize ; i++)
	{
		if( ( pStep = (STEP *)pPtrArray->GetAt(0) ) != NULL )
		{
			pPtrArray->RemoveAt(0);  
		   delete pStep;			// Delete the original element at 0.
		   pStep = NULL;
		}
	}
	pPtrArray->RemoveAll();
	delete pPtrArray;
	pPtrArray = NULL;

	return TRUE;
}

float CCTSEditorProDoc::VUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_VtgUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_VtgUnitInfo.fTransfer;
	}

}

float CCTSEditorProDoc::IUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_CrtUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_CrtUnitInfo.fTransfer;
	}
}
float CCTSEditorProDoc::CUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_CapUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_CapUnitInfo.fTransfer;
	}
}
float CCTSEditorProDoc::WattUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_WattUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_WattUnitInfo.fTransfer;
	}
}

float CCTSEditorProDoc::WattHourUnitTrans(float fData, BOOL bSave)
{
	//Display => Save(m단위)
	if(bSave)
	{
		return fData*m_WattHourUnitInfo.fTransfer;
	}
	else
	{
		return fData/m_WattHourUnitInfo.fTransfer;
	}
}


STEP * CCTSEditorProDoc::GetLastStepData()
{
	int a = m_apStep.GetSize();
	if(a < 1)	return NULL;

	return (STEP *)m_apStep[a-1];
}

//현재 Step 보다 작은  Step에서 Capa ref step을 찾는다.
int CCTSEditorProDoc::GetLastRefCapStepNo(int nCurStepNo)
{
	if(nCurStepNo > GetTotalStepNum() || nCurStepNo < 1)	return 0;

	int nCapRefStep = 0;
	STEP *pStep;
	for(int s=0; s<nCurStepNo; s++)
	{
		pStep = GetStepData(s+1);
		
		if(pStep)		//ljb v1009 SOC , WattHour 스텝 번호 찾기 ????
		{
// 			if(pStep->bUseActualCapa)
// 			{
// 				nCapRefStep = s+1;
// 			}
		}
	}

	return nCapRefStep;
}

CString CCTSEditorProDoc::GetSimulationTitle(CString strFileName)
{
	//파일명을 YYYYMMDDHHmmSS-제목.csv
	CString strFile(strFileName);
	int nStart = strFile.Find('-');
	int nEnd = strFile.ReverseFind('.');

	if(nStart < 0 || nEnd < 0 || nStart >= nEnd)		return strFile;

	return strFile.Mid(nStart+1, nEnd-nStart-1);

}

BOOL CCTSEditorProDoc::RequeryGradeType()
{
	CString strType, strMsg;
	CString strDBName = GetDataBaseName();
	int nCount = 0;
	
	CDaoDatabase  db;
	try
	{
		db.Open(strDBName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
		return FALSE;
	}
	
	COleVariant data;
	CDaoRecordset rs(&db);;	
	
	CString strSQL("SELECT ItemID, ItemName FROM GradeItem ORDER BY ItemID");			
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
		return FALSE;
	}
	
	if(rs.IsBOF())
	{
		rs.Close();
		return FALSE;
	}
	
	SCH_CODE_MSG	*pObject;
	//	pObject = new SCH_CODE_MSG;
	// 	pObject->nCode = 0;
	// 	pObject->nData = 0;
	// 	sprintf(pObject->szMessage, "임의공정");
	// 	m_apProcType.Add(pObject);
	
	while(!rs.IsEOF())
	{
		pObject = new SCH_CODE_MSG;
		ASSERT(pObject);
		ZeroMemory(pObject, sizeof(SCH_CODE_MSG));
		data = rs.GetFieldValue(0);
		pObject->nCode = (int)data.lVal;
		pObject->nData = 0;
		data = rs.GetFieldValue(1);
		strMsg = data.pbVal;
		sprintf(pObject->szMessage, "%s", strMsg);
		m_apGradeType.Add(pObject);
		
		TRACE("Grade Type Id %d : %s\n", pObject->nCode, strMsg);
		rs.MoveNext();
	}
	rs.Close();
	
	return TRUE;
}

BOOL CCTSEditorProDoc::Fun_ReloadDivisionCode()
{
//	CString strTempMDB;
// 	strTempMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
// 	strTempMDB = strTempMDB+ "\\" + "Pack_Schedule_2000.mdb";

	m_uiArryCanDivition.RemoveAll();	//ljb 2011218 이재복 //////////
	m_uiArryAuxDivition.RemoveAll();	//ljb 2011218 이재복 //////////
	m_strArryCanName.RemoveAll();		//ljb 2011218 이재복 //////////
	m_strArryAuxName.RemoveAll();		//ljb 2011218 이재복 //////////

	
	CDaoDatabase  db;
	try
	{
		//db.Open(::GetDataBaseName());
		db.Open(g_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL,strInsertSQL;
	CString strCodeName,strDesc;
	UINT uiCode, uiDivision;
	strSQL.Format("SELECT * FROM DivisionCode");
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	
// 	CRowColArray ra;
// 	int nRow = 1;
	CString strTemp;
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue("Code");
			uiCode = data.lVal;
			//strTemp.Format("%d",iCode);
			
			data = rs.GetFieldValue("Code_Name");
			if(VT_NULL != data.vt) strCodeName.Format("%s", data.pbVal);
			else strCodeName.Empty();
			
			data = rs.GetFieldValue("Division");
			if(VT_NULL != data.vt) uiDivision = data.lVal;
			else uiDivision = 0;
			
			data = rs.GetFieldValue("Description");
			if(VT_NULL != data.vt) strDesc.Format("%s", data.pbVal);
			else strDesc.Empty();
			
			if (uiDivision == 1)	//can
			{
				m_uiArryCanDivition.Add(uiCode);
				m_strArryCanName.Add(strCodeName);
			}
			else if  (uiDivision == 2)	//aux
			{
				m_uiArryAuxDivition.Add(uiCode);
				m_strArryAuxName.Add(strCodeName);

			}
			rs.MoveNext();
		}	
	}
	rs.Close();
	db.Close();
	return TRUE;
}

void CCTSEditorProDoc::Fun_GetArryCanDivision(CUIntArray & arryCanDivision)
{
	arryCanDivision.Copy(m_uiArryCanDivition);
}
void CCTSEditorProDoc::Fun_GetArryAuxDivision(CUIntArray & arryAuxDivision)
{
	arryAuxDivision.Copy(m_uiArryAuxDivition);
}
void CCTSEditorProDoc::Fun_GetArryCanStringName(CStringArray & arryCanStringName)
{
	arryCanStringName.Copy(m_strArryCanName);
}
void CCTSEditorProDoc::Fun_GetArryAuxStringName(CStringArray & arryAuxStringName)
{
	arryAuxStringName.Copy(m_strArryAuxName);
}

//20120222 KHS
STEP * CCTSEditorProDoc::GetStepDataGotoID(int nGotoID)
{
	int nTotalStepNum = GetTotalStepNum();
	
	for(int i = 0; i < nTotalStepNum; i++ )
	{
		STEP * pStep = (STEP *)m_apStep[i];
		if(i+1 == nGotoID)
			return (STEP *)m_apStep[i];
	}
	
	return NULL;
}