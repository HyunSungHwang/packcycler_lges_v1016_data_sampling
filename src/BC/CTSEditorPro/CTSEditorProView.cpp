// CTSEditorProView.cpp : implementation of the CCTSEditorProView class
//

#include "stdafx.h"
#include "CTSEditorPro.h"

#include "CTSEditorProDoc.h"
#include "CTSEditorProView.h"

#include "ProcDataRecordSet.h"
#include "TestTypeRecordset.h"

#include "EditorSetDlg.h"
//#include "ReportDataSelDlg.h"

#include "ModelNameDlg.h"
#include "TestNameDlg.h"

#include "PrntScreen.h"
#include "CommonInputDlg.h"
#include "UserAdminDlg.h"
#include "ChangeUserInfoDlg.h"
#include "ModelInfoDlg.h"
#include "SelectSimulDataDlg.h"

#include "RegCodeDlg.h"
#include "UserMapDlg.h"

#include "LoginDlg.h"
#include "RegeditManage.h"

#include "WorkSTSignEditDlg.h"

#include "Dlg_SocTable.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString g_strDataBaseName;
extern CGlobal g_TabGlobal;

CCTSEditorProView* g_CCTSEditorProViewHandle;

/*

LONG GetStepIndexFromType(BYTE byType)
{
	switch(byType)
	{
	case PS_STEP_LOOP:	return 6;
	default: return (LONG)byType-1;
	}
}

BYTE GetStepTypeFromIndex(LONG nIndex)
{
	switch(nIndex)
	{
	case 6:	return PS_STEP_LOOP;
	default: return (BYTE)nIndex+1;
	}
}
*/
/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProView

IMPLEMENT_DYNCREATE(CCTSEditorProView, CFormView)

extern CString GetDataBaseName();


BEGIN_MESSAGE_MAP(CCTSEditorProView, CFormView)
	//{{AFX_MSG_MAP(CCTSEditorProView)
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LOAD_TEST, OnLoadTest)
	ON_BN_CLICKED(IDC_MODEL_NEW, OnModelNew)
	ON_BN_CLICKED(IDC_TEST_NEW, OnTestNew)
	ON_BN_CLICKED(IDC_MODEL_DELETE, OnModelDelete)
	ON_BN_CLICKED(IDC_TEST_DELETE, OnTestDelete)
	ON_BN_CLICKED(IDC_MODEL_SAVE, OnModelSave)
	ON_BN_CLICKED(IDC_TEST_SAVE, OnTestSave)
	ON_BN_CLICKED(IDC_STEP_SAVE, OnStepSave)
	ON_BN_CLICKED(IDC_STEP_DELETE, OnStepDelete)
	ON_BN_CLICKED(IDC_STEP_INSERT, OnStepInsert)
	ON_BN_CLICKED(IDC_GRADE_CHECK, OnGradeCheck)
	ON_BN_CLICKED(IDC_PRETEST_CHECK, OnPretestCheck)
	ON_EN_CHANGE(IDC_PRETEST_MAXV, OnChangePretestMaxv)
	ON_EN_CHANGE(IDC_PRETEST_DELTA_V, OnChangePretestDeltaV)
	ON_EN_CHANGE(IDC_PRETEST_FAULT_NO, OnChangePretestFaultNo)
	ON_EN_CHANGE(IDC_PRETEST_MAXI, OnChangePretestMaxi)
	ON_EN_CHANGE(IDC_PRETEST_MINV, OnChangePretestMinv)
	ON_EN_CHANGE(IDC_PRETEST_OCV, OnChangePretestOcv)
	ON_EN_CHANGE(IDC_PRETEST_TRCKLE_I, OnChangePretestTrckleI)
	ON_EN_CHANGE(IDC_VTG_HIGH, OnChangeVtgHigh)
	ON_EN_CHANGE(IDC_VTG_LOW, OnChangeVtgLow)
	ON_EN_CHANGE(IDC_CRT_HIGH, OnChangeCrtHigh)
	ON_EN_CHANGE(IDC_CRT_LOW, OnChangeCrtLow)
	ON_EN_CHANGE(IDC_CAP_HIGH, OnChangeCapHigh)
	ON_EN_CHANGE(IDC_CAP_LOW, OnChangeCapLow)
	ON_EN_CHANGE(IDC_IMP_HIGH, OnChangeImpHigh)
	ON_EN_CHANGE(IDC_IMP_LOW, OnChangeImpLow)
	ON_EN_CHANGE(IDC_DELTA_V, OnChangeDeltaV)
	ON_EN_CHANGE(IDC_DELTA_I, OnChangeDeltaI)
	ON_EN_CHANGE(IDC_DELTA_TIME, OnChangeDeltaTime)
	ON_COMMAND(ID_OPTION, OnOption)
	ON_EN_CHANGE(IDC_COMP_V1, OnChangeCompV1)
	ON_EN_CHANGE(IDC_COMP_V2, OnChangeCompV2)
	ON_EN_CHANGE(IDC_COMP_V3, OnChangeCompV3)
	ON_EN_CHANGE(IDC_COMP_TIME1, OnChangeCompTime1)
	ON_EN_CHANGE(IDC_COMP_TIME2, OnChangeCompTime2)
	ON_EN_CHANGE(IDC_COMP_TIME3, OnChangeCompTime3)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_BN_CLICKED(IDC_MODEL_COPY_BUTTON, OnModelCopyButton)
	ON_COMMAND(ID_EXCEL_SAVE, OnExcelSave)
	ON_COMMAND(ID_JASON_FILE_SAVE, OnJasonSchSave) //yulee 20190104 Jason format sch
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_UPDATE_COMMAND_UI(ID_EXCEL_SAVE, OnUpdateExcelSave)
	ON_UPDATE_COMMAND_UI(ID_JASON_FILE_SAVE, OnUpdateJasonSchSave) //yulee 20190104 Jason format sch
	ON_NOTIFY(TCN_SELCHANGE, IDC_PARAM_TAB, OnSelchangeParamTab)
	ON_BN_CLICKED(IDC_EXT_OPTION_CHECK, OnExtOptionCheck)
	ON_BN_CLICKED(IDC_MODEL_EDIT, OnModelEdit)
	ON_BN_CLICKED(IDC_TEST_EDIT, OnTestEdit)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_INSERT_STEP, OnInsertStep)
	ON_COMMAND(ID_DELETE_STEP, OnDeleteStep)
	ON_UPDATE_COMMAND_UI(ID_DELETE_STEP, OnUpdateDeleteStep)
	ON_UPDATE_COMMAND_UI(ID_INSERT_STEP, OnUpdateInsertStep)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_TEST_COPY_BUTTON, OnTestCopyButton)
	ON_COMMAND(ID_EDIT_INSERT, OnEditInsert)
	ON_UPDATE_COMMAND_UI(ID_EDIT_INSERT, OnUpdateEditInsert)
	ON_EN_CHANGE(IDC_REPORT_CURRENT, OnChangeReportCurrent)
	ON_EN_CHANGE(IDC_REPORT_TEMPERATURE, OnChangeReportTemperature)
	ON_EN_CHANGE(IDC_REPORT_VOLTAGE, OnChangeReportVoltage)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_REPORT_TIME, OnDatetimechangeReportTime)
	ON_EN_CHANGE(IDC_CAP_VTG_LOW, OnChangeCapVtgLow)
	ON_EN_CHANGE(IDC_CAP_VTG_HIGH, OnChangeCapVtgHigh)
	ON_EN_CHANGE(IDC_COMP_I_TIME1, OnChangeCompITime1)
	ON_EN_CHANGE(IDC_COMP_I_TIME2, OnChangeCompITime2)
	ON_EN_CHANGE(IDC_COMP_I_TIME3, OnChangeCompITime3)
	ON_EN_CHANGE(IDC_COMP_I1, OnChangeCompI1)
	ON_EN_CHANGE(IDC_COMP_I2, OnChangeCompI2)
	ON_EN_CHANGE(IDC_COMP_I3, OnChangeCompI3)
	ON_EN_CHANGE(IDC_COMP_I4, OnChangeCompI4)
	ON_EN_CHANGE(IDC_COMP_I5, OnChangeCompI5)
	ON_EN_CHANGE(IDC_COMP_I6, OnChangeCompI6)
	ON_EN_CHANGE(IDC_COMP_V4, OnChangeCompV4)
	ON_EN_CHANGE(IDC_COMP_V5, OnChangeCompV5)
	ON_EN_CHANGE(IDC_COMP_V6, OnChangeCompV6)
	ON_NOTIFY(UDN_DELTAPOS, IDC_RPT_MSEC_SPIN, OnDeltaposRptMsecSpin)
	ON_EN_CHANGE(IDC_CELL_CHECK_TIME_EDIT, OnChangeCellCheckTimeEdit)
	ON_BN_CLICKED(IDC_ALL_STEP_BUTTON, OnAllStepButton)
	ON_BN_CLICKED(IDC_SAME_ALL_STEP_BUTTON, OnSameAllStepButton)
	ON_EN_CHANGE(IDC_TEMP_LOW, OnChangeTempLow)
	ON_EN_CHANGE(IDC_TEMP_HIGH, OnChangeTempHigh)
	ON_BN_CLICKED(IDC_BUTTON_MODEL_SEL, OnButtonModelSel)
	ON_COMMAND(ID_ADMINISTRATION, OnAdministration)
	ON_COMMAND(ID_USER_SETTING, OnUserSetting)
	ON_UPDATE_COMMAND_UI(ID_ADMINISTRATION, OnUpdateAdministration)
	ON_UPDATE_COMMAND_UI(ID_USER_SETTING, OnUpdateUserSetting)
	ON_COMMAND(ID_MODEL_REG, OnModelReg)
	ON_UPDATE_COMMAND_UI(ID_MODEL_REG, OnUpdateModelReg)
	ON_EN_CHANGE(IDC_REPORT_TIME_MILI, OnChangeReportTimeMili)
	ON_EN_KILLFOCUS(IDC_REPORT_TIME_MILI, OnKillFocusReportTimeMili)
	ON_EN_CHANGE(IDC_CAPACITANCE_HIGH, OnChangeCapacitanceHigh)
	ON_EN_CHANGE(IDC_CAPACITANCE_LOW1, OnChangeCapacitanceLow)
	ON_EN_CHANGE(IDC_EDIT_ACC_GROUP_COUNT1, OnChangeEditAccGroupCount1)
	ON_EN_CHANGE(IDC_EDIT_ACC_GROUP_COUNT2, OnChangeEditAccGroupCount2)
	ON_EN_CHANGE(IDC_EDIT_ACC_GROUP_COUNT3, OnChangeEditAccGroupCount3)
	ON_EN_CHANGE(IDC_EDIT_ACC_GROUP_COUNT4, OnChangeEditAccGroupCount4)
	ON_EN_CHANGE(IDC_EDIT_ACC_GROUP_COUNT5, OnChangeEditAccGroupCount5)
	ON_CBN_SELCHANGE(IDC_CBO_ACC_GROUP1, OnSelchangeCboAccGroup1)
	ON_CBN_SELCHANGE(IDC_CBO_ACC_GROUP2, OnSelchangeCboAccGroup2)
	ON_CBN_SELCHANGE(IDC_CBO_ACC_GROUP3, OnSelchangeCboAccGroup3)
	ON_CBN_SELCHANGE(IDC_CBO_ACC_GROUP4, OnSelchangeCboAccGroup4)
	ON_CBN_SELCHANGE(IDC_CBO_ACC_GROUP5, OnSelchangeCboAccGroup5)
	ON_EN_CHANGE(IDC_EDIT_MULTI_GROUP_REPEAT1, OnChangeEditMultiGroupRepeat1)
	ON_EN_CHANGE(IDC_EDIT_MULTI_GROUP_REPEAT2, OnChangeEditMultiGroupRepeat2)
	ON_EN_CHANGE(IDC_EDIT_MULTI_GROUP_REPEAT3, OnChangeEditMultiGroupRepeat3)
	ON_EN_CHANGE(IDC_EDIT_MULTI_GROUP_REPEAT4, OnChangeEditMultiGroupRepeat4)
	ON_EN_CHANGE(IDC_EDIT_MULTI_GROUP_REPEAT5, OnChangeEditMultiGroupRepeat5)
	ON_CBN_SELCHANGE(IDC_CBO_MULTI_GOTO1, OnSelchangeCboMultiGoto1)
	ON_CBN_SELCHANGE(IDC_CBO_MULTI_GOTO2, OnSelchangeCboMultiGoto2)
	ON_COMMAND(ID_WORKST_WND, OnWorkSTShow)
	ON_CBN_SELCHANGE(IDC_CBO_MULTI_GOTO3, OnSelchangeCboMultiGoto3)
	ON_CBN_SELCHANGE(IDC_CBO_MULTI_GOTO4, OnSelchangeCboMultiGoto4)
	ON_CBN_SELCHANGE(IDC_CBO_MULTI_GOTO5, OnSelchangeCboMultiGoto5)
	ON_BN_CLICKED(IDC_BUT_SAFETY_SET_CAN_AUX, OnButSafetySetCanAux)
	ON_EN_CHANGE(IDC_SAFETY_MAX_C, OnChangeSafetyMaxC)
	ON_EN_CHANGE(IDC_SAFETY_MAX_I, OnChangeSafetyMaxI)
	ON_EN_CHANGE(IDC_SAFETY_MAX_T, OnChangeSafetyMaxT)
	ON_EN_CHANGE(IDC_SAFETY_MAX_V, OnChangeSafetyMaxV)
	ON_EN_CHANGE(IDC_SAFETY_MAX_W, OnChangeSafetyMaxW)
	ON_EN_CHANGE(IDC_SAFETY_MAX_WH, OnChangeSafetyMaxWh)
	ON_EN_CHANGE(IDC_SAFETY_MIN_I, OnChangeSafetyMinI)
	ON_EN_CHANGE(IDC_SAFETY_MIN_T, OnChangeSafetyMinT)
	ON_EN_CHANGE(IDC_SAFETY_MIN_V, OnChangeSafetyMinV)
	ON_BN_CLICKED(IDC_CHK_CHAMBER, OnChkChamber)
	ON_BN_CLICKED(IDC_BUT_REG_CAN_CODE, OnButRegCanCode)
	ON_BN_CLICKED(IDC_BUT_REG_AUX_CODE, OnButRegAuxCode)
	ON_BN_CLICKED(IDC_CAN_ALL_STEP_BUTTON, OnCanAllStepButton)
	ON_BN_CLICKED(IDC_SAME_CAN_ALL_STEP_BUTTON, OnSameCanAllStepButton)
	ON_BN_CLICKED(IDC_AUX_ALL_STEP_BUTTON, OnAuxAllStepButton)
	ON_BN_CLICKED(IDC_SAME_AUX_ALL_STEP_BUTTON, OnSameAuxAllStepButton)
	ON_BN_CLICKED(IDC_BUT_DB_UP, OnButDbUp)
	ON_BN_CLICKED(IDC_CHK_PARALLEL_MODE, OnChkParallelMode)
	ON_BN_CLICKED(IDC_BUT_DB_CREATE, OnButDbCreate)
	ON_COMMAND(ID_REG_CAN_CODE, OnRegCanCode)
	ON_COMMAND(ID_REG_AUX_CODE, OnRegAuxCode)
	ON_COMMAND(ID_REG_CAN_CODE_MANAGE, OnRegCanCodeManage)
	ON_COMMAND(ID_REG_AUX_CODE_MANAGE, OnRegAuxCodeManage)
	ON_BN_CLICKED(IDC_ALL_STEP_BUTTON2, OnAllStepButton2)
	ON_BN_CLICKED(IDC_SAME_ALL_STEP_BUTTON2, OnSameAllStepButton2)
	ON_COMMAND(ID_WORK_PNE_MANAGE, OnWorkPneManage)
	ON_EN_KILLFOCUS(IDC_SAFETY_MAX_I, OnKillFocusSafety_Max_I) //20180729
	ON_WM_KEYUP()
	ON_WM_COPYDATA()
	ON_WM_CLOSE()	
	ON_EN_CHANGE(IDC_CELL_DELTA_V_STEP, OnChangeCellDeltaVStep)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
	ON_MESSAGE(WM_GRID_CLICK, OnGridClicked)
	ON_MESSAGE(WM_GRID_KILLFOCUS, OnGridKillFocus)
	ON_MESSAGE(WM_GRID_MOVECELL, OnMovedCurrentCell)
	ON_MESSAGE(WM_COMBO_SELECT, OnGridComboSelected)
	ON_MESSAGE(WM_COMBO_DROP_DOWN, OnGridComboDropDown)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)
	ON_MESSAGE(WM_GRID_ROW_DRAG_DROP ,OnSelDragRowsDrop)
	ON_MESSAGE(WM_GRID_MOVEROW, OnLeftCell)
	ON_MESSAGE(WM_END_DLG_CLOSE, OnCloseEndDlg)
	ON_MESSAGE(WM_CELLBAL_DLG_CLOSE, OnCloseDlgCellBal)
	ON_MESSAGE(WM_PWRSUPPLY_DLG_CLOSE, OnCloseDlgPwrSupply)
	ON_MESSAGE(WM_CHILLER_DLG_CLOSE, OnCloseDlgChiller)
	ON_MESSAGE(WM_PATTSCHED_DLG_CLOSE, OnCloseDlgPattSched)
	ON_MESSAGE(WM_PATTSELECT_DLG_CLOSE, OnCloseDlgPattSelect)
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnStartEditing)
	ON_MESSAGE(WM_GRID_ENDEDIT, OnEndEditing)
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDbClicked)
	ON_MESSAGE(WM_GRID_BTN_CLICKED, OnGridBtnClicked)
	ON_MESSAGE(WM_DATETIME_CHANGED, OnDateTimeChanged)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_SAFETY_CELL_DELTA_MIN_V, &CCTSEditorProView::OnEnChangeSafetyCellDeltaMinV)
	ON_EN_CHANGE(IDC_SAFETY_CELL_DELTA_V, &CCTSEditorProView::OnEnChangeSafetyCellDeltaV)
	ON_EN_CHANGE(IDC_SAFETY_CELL_DELTA_MAX_V, &CCTSEditorProView::OnEnChangeSafetyCellDeltaMaxV)
	ON_BN_CLICKED(IDC_CHK_DELTA_CHAMBER, &CCTSEditorProView::OnBnClickedChkDeltaChamber)
	ON_EN_CHANGE(IDC_STEP_AUXT_DELTA, &CCTSEditorProView::OnEnChangeStepAuxtDelta)
	ON_EN_CHANGE(IDC_STEP_AUXTH_DELTA, &CCTSEditorProView::OnEnChangeStepAuxthDelta)
	ON_EN_CHANGE(IDC_STEP_AUXT_AUXTH_DELTA, &CCTSEditorProView::OnEnChangeStepAuxtAuxthDelta)
	ON_EN_CHANGE(IDC_STEP_AUXV_TIME, &CCTSEditorProView::OnEnChangeStepAuxvTime)
	ON_EN_CHANGE(IDC_STEP_AUXV_DELTA, &CCTSEditorProView::OnEnChangeStepAuxvDelta)
	ON_BN_CLICKED(IDC_CHK_STEP_AUX_V, &CCTSEditorProView::OnBnClickedChkStepAuxV)
	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_V, &CCTSEditorProView::OnBnClickedChkStepDeltaAuxV)
	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_TEMP, &CCTSEditorProView::OnBnClickedChkStepDeltaAuxTemp)
	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_TH, &CCTSEditorProView::OnBnClickedChkStepDeltaAuxTh)
	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_T, &CCTSEditorProView::OnBnClickedChkStepDeltaAuxT)
	ON_EN_KILLFOCUS(IDC_SAFETY_CELL_DELTA_STD_MIN_V, &CCTSEditorProView::OnEnKillfocusSafetyCellDeltaStdMinV)
	ON_EN_KILLFOCUS(IDC_SAFETY_CELL_DELTA_STD_MAX_V, &CCTSEditorProView::OnEnKillfocusSafetyCellDeltaStdMaxV)
	END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProView construction/destruction

CCTSEditorProView::CCTSEditorProView()
	: CFormView(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCTSEditorProView::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCTSEditorProView::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CCTSEditorProView::IDD3):
	CCTSEditorProView::IDD)
	, m_bUseVentSafety(FALSE)
{
	//{{AFX_DATA_INIT(CCTSEditorProView)
	m_bExtOption = FALSE;
	m_nFaultNo = 0;
	m_nCheckTime = 0;
	m_ctrlEditAccGroup1 = 0;
	m_ctrlEditAccGroup2 = 0;
	m_ctrlEditAccGroup3 = 0;
	m_ctrlEditAccGroup4 = 0;
	m_ctrlEditAccGroup5 = 0;
	m_ctrlEditMultiGroup1 = 0;
	m_ctrlEditMultiGroup2 = 0;
	m_ctrlEditMultiGroup3 = 0;
	m_ctrlEditMultiGroup4 = 0;
	m_ctrlEditMultiGroup5 = 0;
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	m_pDlg = NULL;
	m_lDisplayModelID = 0;
	m_lDisplayTestID = 0;
	m_nDisplayStep = 0;
	m_bStepDataSaved = TRUE;

	m_pTestTypeCombo = NULL;
	m_pProcTypeCombo = NULL;
	m_pStepTypeCombo = NULL;
	m_pStepModeCombo = NULL;
	m_bCopyed = FALSE;
	m_bPasteUndo = FALSE;

	m_bVHigh = FALSE;
	m_bVLow = FALSE;
	m_bIHigh = FALSE;
	m_bILow = FALSE;
	m_bCHigh = FALSE;
	m_bCLow = FALSE;
	//m_bCellDeltaVstep = FALSE;//yulee 20190531_5
	m_bCapacitanceHigh = FALSE;
	m_bCapacitanceLow = FALSE;
	m_fCapacitanceLow = 0.0f;
	m_fCapacitanceHigh = 0.0f;
	m_fCLowVal = 0.0f;
	m_fCHighVal = 0.0f;
	m_fILowVal = 0.0f;
	m_fIHighVal = 0.0f;
	m_fVLowVal = 0.0f;
	m_fVHighVal = 0.0f;

	m_lLoadedBatteryModelID = 0;
	m_lLoadedTestID = 0;

	m_pTabImageList = NULL;
	
	m_nCurTabIndex = TAB_SAFT_VAL;
	m_nTimer =0;

	m_bShowEndDlgToggle = FALSE;

	m_pModelDlg = NULL;

	m_bUseModelInfo = FALSE;
	m_pWorkSTSignEditDlg=NULL;
	m_bUseRange = FALSE;

	m_bDeltaVVent=FALSE;

	m_bLinkChamber = FALSE;
	m_nCustomMsec = 0; //ksj 20171031;

	m_bFlagConfirmOK = FALSE;

	//cny 201809
	m_bShowCellBalDlgToggle=FALSE;
	m_pDlgCellBal=NULL;
	
	//cny 201809
	m_bShowPwrSupplyDlgToggle=FALSE;
	m_pDlgPwrSupply=NULL;
	
	//cny 201809
	m_bShowChillerDlgToggle=FALSE;
	m_pDlgChiller=NULL;
	
	//cny 201809
	m_bShowPattSchedDlgToggle=FALSE;
	m_pDlgPattSched=NULL;
	
	//cny 201809
	m_bShowPattSelectDlgToggle=FALSE;
	m_pDlgPattSelect=NULL;


	m_fCellDeltaVstep = 0.0f;
	m_fStepDeltaAuxTemp = 0.0f;
	m_fStepDeltaAuxTH = 0.0f;
	m_fStepDeltaAuxT = 0.0f;
	m_fStepDeltaAuxVTime = 0.0f;
	m_fStepAuxV = 0.0f;

	m_bStepDeltaVentAuxTemp=FALSE;		
	m_bStepDeltaVentAuxTh=FALSE;		
	m_bStepDeltaVentAuxT=FALSE;		
	m_bStepDeltaVentAuxV=FALSE;		
	m_bStepVentAuxV=FALSE;		
	m_bUseAuxV=TRUE; //ksj 20210610
}

CCTSEditorProView::~CCTSEditorProView()
{
	if(m_pDlg)
	{
		m_pDlg->DestroyWindow();
		delete m_pDlg;
		m_pDlg = NULL;
	}

	if(m_pTabImageList)
	{
		delete	m_pTabImageList;
	}

	if(m_pModelDlg)
	{
		m_pModelDlg->DestroyWindow();
		delete m_pModelDlg;
	}
	if(m_pWorkSTSignEditDlg)
	{
		m_pWorkSTSignEditDlg->DestroyWindow();
		delete m_pWorkSTSignEditDlg;
		m_pWorkSTSignEditDlg = NULL;
	}
	//cny 201809
	if(m_pDlgCellBal)
	{
		m_pDlgCellBal->DestroyWindow();
		delete m_pDlgCellBal;
		m_pDlgCellBal = NULL;
	}
	//cny 201809
	if(m_pDlgPwrSupply)
	{
		m_pDlgPwrSupply->DestroyWindow();
		delete m_pDlgPwrSupply;
		m_pDlgPwrSupply = NULL;
	}
	//cny 201809
	if(m_pDlgChiller)
	{
		m_pDlgChiller->DestroyWindow();
		delete m_pDlgChiller;
		m_pDlgChiller = NULL;
	}
	//cny 201809
	if(m_pDlgPattSched)
	{
		m_pDlgPattSched->DestroyWindow();
		delete m_pDlgPattSched;
		m_pDlgPattSched = NULL;
	}
	//cny 201809
	if(m_pDlgPattSelect)
	{
		m_pDlgPattSelect->DestroyWindow();
		delete m_pDlgPattSelect;
		m_pDlgPattSelect = NULL;
	}
}

void CCTSEditorProView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTSEditorProView)
	DDX_Control(pDX, IDC_CBO_PARALLEL_NUM, m_CboParallelNum);
	DDX_Control(pDX, IDC_CHK_PARALLEL_MODE, m_bChkParallel);
	DDX_Control(pDX, IDC_CHK_CHAMBER, m_bChkChamber);
	DDX_Control(pDX, IDC_CHK_DELTA_CHAMBER, m_bChkDeltaVVent);
	DDX_Control(pDX, IDC_CBO_MULTI_GOTO5, m_ctrlCboMultiGroup5);
	DDX_Control(pDX, IDC_CBO_MULTI_GOTO4, m_ctrlCboMultiGroup4);
	DDX_Control(pDX, IDC_CBO_MULTI_GOTO3, m_ctrlCboMultiGroup3);
	DDX_Control(pDX, IDC_CBO_MULTI_GOTO2, m_ctrlCboMultiGroup2);
	DDX_Control(pDX, IDC_CBO_MULTI_GOTO1, m_ctrlCboMultiGroup1);
	DDX_Control(pDX, IDC_CBO_ACC_GROUP5, m_ctrlCboAccGroup5);
	DDX_Control(pDX, IDC_CBO_ACC_GROUP4, m_ctrlCboAccGroup4);
	DDX_Control(pDX, IDC_CBO_ACC_GROUP3, m_ctrlCboAccGroup3);
	DDX_Control(pDX, IDC_CBO_ACC_GROUP2, m_ctrlCboAccGroup2);
	DDX_Control(pDX, IDC_CBO_ACC_GROUP1, m_ctrlCboAccGroup1);
	DDX_Control(pDX, IDC_DELTA_TIME, m_DeltaTime);
	DDX_Control(pDX, IDC_REPORT_TIME, m_ReportTime);
	DDX_Control(pDX, IDC_PARAM_TAB, m_ctrlParamTab);
	DDX_Control(pDX, IDC_TOT_STEP_COUNT, m_TotalStepCount);
	DDX_Control(pDX, IDC_LOADED_TEST, m_LoadedTest);
	DDX_Control(pDX, IDC_TEST_LIST_LABEL, m_TestNameLabel);
	DDX_Control(pDX, IDC_TOT_TESTNUM, m_TotTestCount);
	DDX_Control(pDX, IDC_MODEL_TOTNUM, m_TotModelCount);
	DDX_Control(pDX, IDC_PRETEST_CHECK, m_PreTestCheck);
	DDX_Control(pDX, IDC_GRADE_CHECK, m_GradeCheck);
	DDX_Control(pDX, IDC_STEP_NUM, m_strStepNumber);
	DDX_Control(pDX, IDC_TEST_NAME, m_strTestName);
	DDX_Check(pDX, IDC_EXT_OPTION_CHECK, m_bExtOption);
	DDX_Text(pDX, IDC_PRETEST_FAULT_NO, m_nFaultNo);
	DDV_MinMaxInt(pDX, m_nFaultNo, 0, 64);
	DDX_Text(pDX, IDC_CELL_CHECK_TIME_EDIT, m_nCheckTime);
	DDX_Control(pDX, IDC_WARRING_STATIC, m_strWarning);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT1, m_ctrlEditAccGroup1);
	DDV_MinMaxUInt(pDX, m_ctrlEditAccGroup1, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT2, m_ctrlEditAccGroup2);
	DDV_MinMaxUInt(pDX, m_ctrlEditAccGroup2, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT3, m_ctrlEditAccGroup3);
	DDV_MinMaxUInt(pDX, m_ctrlEditAccGroup3, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT4, m_ctrlEditAccGroup4);
	DDV_MinMaxUInt(pDX, m_ctrlEditAccGroup4, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT5, m_ctrlEditAccGroup5);
	DDV_MinMaxUInt(pDX, m_ctrlEditAccGroup5, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_MULTI_GROUP_REPEAT1, m_ctrlEditMultiGroup1);
	DDV_MinMaxUInt(pDX, m_ctrlEditMultiGroup1, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_MULTI_GROUP_REPEAT2, m_ctrlEditMultiGroup2);
	DDV_MinMaxUInt(pDX, m_ctrlEditMultiGroup2, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_MULTI_GROUP_REPEAT3, m_ctrlEditMultiGroup3);
	DDV_MinMaxUInt(pDX, m_ctrlEditMultiGroup3, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_MULTI_GROUP_REPEAT4, m_ctrlEditMultiGroup4);
	DDV_MinMaxUInt(pDX, m_ctrlEditMultiGroup4, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_MULTI_GROUP_REPEAT5, m_ctrlEditMultiGroup5);
	DDV_MinMaxUInt(pDX, m_ctrlEditMultiGroup5, 0, 50000);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_LOW_STATIC, m_StaticIDCLow);
	DDX_Control(pDX, IDC_HIGH_STATIC, m_StaticIDCHigh);
	DDX_Control(pDX, IDC_RPT_TIME_STATIC, m_StaticIDCRptTime);
	DDX_Control(pDX, IDC_V_STATIC, m_StaticIDCV);
	DDX_Control(pDX, IDC_I_STATIC, m_StaticIDCI);
	DDX_Control(pDX, IDC_RPT_V_STATIC, m_StaticIDCRptV);
	DDX_Control(pDX, IDC_CAP_STATIC, m_StaticIDCCap);
	DDX_Control(pDX, IDC_RPT_I_STATIC, m_StaticIDCRptI);
	DDX_Control(pDX, IDC_IMP_STATIC, m_StaticIDCImp);
	DDX_Control(pDX, IDC_RPT_TEMP_STATIC, m_StaticIDCRptTemp);
	DDX_Control(pDX, IDC_TEMP_STATIC, m_StaticIDCTemp);
	DDX_Control(pDX, IDC_CAPACITANCE_STATIC2, m_StaticIDCCapacitance);
	DDX_Control(pDX, IDC_CAP_VTG_STATIC, m_StaticIDCCapVtg);
	DDX_Control(pDX, IDC_RPT_TUNIT_STATIC, m_StaticIDCRptTunit);
	DDX_Control(pDX, IDC_OR_STATIC, m_StaticIDCOr);

	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT1, m_ctrlEditAccGroup1);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT1, m_ctrlEditAccGroup1);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT1, m_ctrlEditAccGroup1);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT1, m_ctrlEditAccGroup1);
	DDX_Text(pDX, IDC_EDIT_ACC_GROUP_COUNT1, m_ctrlEditAccGroup1);

	DDX_Control(pDX, IDC_CHK_STEP_DELTA_AUX_V, m_bChkStepDeltaVentAuxV);
	DDX_Control(pDX, IDC_CHK_STEP_DELTA_AUX_TEMP, m_bChkStepDeltaVentAuxTemp);
	DDX_Control(pDX, IDC_CHK_STEP_DELTA_AUX_TH, m_bChkStepDeltaVentAuxTh);
	DDX_Control(pDX, IDC_CHK_STEP_DELTA_AUX_T, m_bChkStepDeltaVentAuxT);
	DDX_Control(pDX, IDC_CHK_STEP_AUX_V, m_bChkStepVentAuxV);

	DDX_Control(pDX, IDC_SAFETY_CELL_DELTA_STD_MIN_MAX_STATIC, m_StaticCtrlSaftyCommonDelta);
}

BOOL CCTSEditorProView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCTSEditorProView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	
	m_strTestName.SetBkColor(RGB(255, 255, 230));
	m_strTestName.SetTextColor(RGB(255,0,0));

	m_LoadedTest.SetBkColor(RGB(255, 255, 230));
	m_LoadedTest.SetTextColor(RGB(255, 0, 0));
	m_TestNameLabel.SetBkColor(RGB(255, 255, 230));
	m_TestNameLabel.SetTextColor(RGB(255, 0, 0));

	m_strStepNumber.SetTextColor(RGB(255, 0 , 0)).SetFontBold(TRUE);
	m_strStepNumber.SetBkColor(RGB(240, 240, 240));
	
	m_btnModelNew.AttachButton(IDC_MODEL_NEW, stingray::foundation::SECBitmapButton::Al_Left, IDB_NEW, this);
	m_btnModelDelete.AttachButton(IDC_MODEL_DELETE, stingray::foundation::SECBitmapButton::Al_Left, IDB_DELETE, this);
	m_btnModelSave.AttachButton(IDC_MODEL_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);
	m_btnModelCopy.AttachButton(IDC_MODEL_COPY_BUTTON, stingray::foundation::SECBitmapButton::Al_Left, IDB_COPY2, this);
	m_btnModelEdit.AttachButton(IDC_MODEL_EDIT, stingray::foundation::SECBitmapButton::Al_Left, IDB_EDIT, this);

	m_btnProcNew.AttachButton(IDC_TEST_NEW, stingray::foundation::SECBitmapButton::Al_Left, IDB_NEW, this);
	m_btnProcDelete.AttachButton(IDC_TEST_DELETE, stingray::foundation::SECBitmapButton::Al_Left, IDB_DELETE, this);
	m_btnProcSave.AttachButton(IDC_TEST_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);
	m_btnProcEdit.AttachButton(IDC_TEST_EDIT, stingray::foundation::SECBitmapButton::Al_Left, IDB_EDIT, this);
	m_btnProcCopy.AttachButton(IDC_TEST_COPY_BUTTON, stingray::foundation::SECBitmapButton::Al_Left, IDB_COPY2, this);

	m_btnStepNew.AttachButton(IDC_STEP_INSERT, stingray::foundation::SECBitmapButton::Al_Left, IDB_INSERT, this);
	m_btnStepDelete.AttachButton(IDC_STEP_DELETE, stingray::foundation::SECBitmapButton::Al_Left, IDB_REMOVE, this);
	m_btnStepSave.AttachButton(IDC_STEP_SAVE, stingray::foundation::SECBitmapButton::Al_Left, IDB_SAVE, this);

	m_btnLoad.AttachButton(IDC_LOAD_TEST, stingray::foundation::SECBitmapButton::Al_Left, IDB_OPEN, this);
	m_btnModel.AttachButton(IDC_BUTTON_MODEL_SEL, stingray::foundation::SECBitmapButton::Al_Left, IDB_LOAD, this);
	

	m_bVHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_V_High", FALSE);
	m_bVLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_V_Low", FALSE);
	m_bIHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_I_High", FALSE);
	m_bILow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_I_Low", FALSE);
	m_bCHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_C_High", FALSE);
	m_bCLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_C_Low", FALSE);
	//m_bCellDeltaVstep = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_CellDeltaVStep", FALSE);
	m_fCLowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_C_Low_Val", "0"));
	m_fCHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_C_High_Val", "0"));
	m_fILowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_I_Low_Val", "0"));
	m_fIHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_I_High_Val", "0"));
	m_fVLowVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_V_Low_Val", "0"));
	m_fVHighVal = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_V_High_Val", "0"));
	
	m_bUseChamberTemp = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseChamberForTemp", TRUE); //ksj 20160624 기본값 TRUE로 변경
	m_bUseChamberHumi = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseChamberForHumi", TRUE);
	GetDlgItem(IDC_CHK_CHAMBER)->ShowWindow(m_bUseChamberTemp);

	m_bUseParallel = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseParallel", FALSE);			//20120222 KHS
	GetDlgItem(IDC_CHK_PARALLEL_MODE)->ShowWindow(m_bUseParallel);									//20120222 KHS
	//ljb 2011314 이재복 //////////
	m_bUseDatabaseUpdate = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseDatabaseUpdate", FALSE); 
	GetDlgItem(IDC_BUT_DB_CREATE)->ShowWindow(m_bUseDatabaseUpdate);
	//GetDlgItem(IDC_BUT_DB_UP)->ShowWindow(m_bUseDatabaseUpdate);

	//ljb 2011318 이재복 //////////
	m_bUseExternalCAN = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseExternalCAN", FALSE); 
	//2014.09.02 Usermap 사용 유무
	m_bUseUserMap = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION,"UseUserMap",FALSE);
	//ksj 20201117 : 임피던스 스텝 사용 유무
	m_bUseImpedance = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION,"UseImpedance",FALSE);

	m_bCapacitanceHigh = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_Cap_High", FALSE);
	m_bCapacitanceLow = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Auto_Cap_Low", FALSE);
	m_fCapacitanceLow = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_Cap_Low_Val", "0"));
	m_fCapacitanceHigh = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION , "Auto_Cap_High_Val", "0"));

	m_bUseRange = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseRange", FALSE);
	m_nRangeVal = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Range Value", 0);

	//cny
	m_bUseCellBal   = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseCellBal", TRUE);//cny 201809
	m_bUsePwrSupply = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UsePwrSupply", TRUE);//cny 201809
	m_bUseChiller   = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseChiller", TRUE);//cny 201809
	m_bUsePatTable  = FALSE;//AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UsePatTable", TRUE);//cny 201809

	m_bUseAuxV = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseAuxV", TRUE); //ksj 20210610
	
	//ljb resize form view
	CRect rect,rect2;
	::GetClientRect(m_hWnd, &rect);
	
	CString strTemp;
	strTemp.Format("%d,%d",rect.Width(),rect.Height());
	GetDlgItem(IDC_STEP_RANGE)->GetClientRect(rect2);	
	::MoveWindow(m_wndStepGrid,rect2.left+10,rect.top + 300,rect.Width()-rect2.left,rect.Height()-rect.Height()-300, FALSE);
	
	int nIDSel;//&&
	//nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);//&&

	#ifdef _FORMATION_
		//모델 목록이 있는지 검사후 없으며 등록하도록 한다.
		CString strSQL;
		CDaoDatabase  db;
		int nCnt = 0;

		strSQL = "SELECT Name FROM ModelInfo ORDER BY Name";
		try
		{
			db.Open(GetDataBaseName());
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
			

			while(!rs.IsEOF())
			{
				nCnt++;
				rs.MoveNext();
			}
			rs.Close();
			db.Close();

			if(nCnt < 1)
			{
			AfxMessageBox(Fun_FindMsg("OnInitialUpdate_msg1"));
				OnModelReg();
			}
			m_bUseModelInfo = TRUE;
		}
		catch (CDaoException* e)
		{
			//Old Version
			TRACE("Model information tabe not exist\n");
			e->Delete();
		}
	#endif

	#ifdef _DLG_MODEL_
		m_pModelDlg = new CModelSelDlg(this);
		//m_pModelDlg->Create(IDD_MODEL_DIALOG, this);
		m_pModelDlg->Create( //&&
			gi_Language == 1?(IDD_MODEL_DIALOG):
			gi_Language == 2?(IDD_MODEL_DIALOG_ENG):
			gi_Language == 3?(IDD_MODEL_DIALOG_PL):
			IDD_MODEL_DIALOG, this);
		m_pModelDlg->ShowWindow(SW_SHOW);
	#else
		InitBatteryModelGrid(IDC_BATTERY_MODEL, this, &m_wndBatteryModelGrid);
	#endif

	InitTestListGrid();
	InitStepGrid(gi_Language);
	InitGradeGrid();
	InitEditCtrl();
	InitParamTab();
	InitMultiGroup(TRUE);		//ljb v1009
	InitAccGroup(TRUE);		//ljb v1009
	//InitChamberGrid();
	InitParallelCbo();
	
	InitToolTips();
	SetControlCellEdlc();

	m_pDlg = new CEndConditionDlg(this);
	ASSERT(m_pDlg);
	m_pDlg->m_pDoc = (CCTSEditorProDoc *)GetDocument();
	//m_pDlg->Create(IDD_END_COND_DLG, (CMyGridWnd *)&m_wndStepGrid);
	m_pDlg->Create( //&&
		gi_Language == 1?(IDD_END_COND_DLG):
		gi_Language == 2?(IDD_END_COND_DLG_ENG):
		gi_Language == 3?(IDD_END_COND_DLG_PL):
		IDD_END_COND_DLG, (CMyGridWnd *)&m_wndStepGrid);

	m_pDlg->GetClientRect(m_recDlgRect);		//Dialog Client Position


	for(int i =0; i < SCH_MAX_COMP_POINT; i++)
	{
		m_CompTimeV[i].EnableWindow(FALSE);
		m_CompTimeI[i].EnableWindow(FALSE);
	}

	ClearStepState();

	int nCurrentUnitMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Crt Unit Mode", 0);
	
	CString strFormat;
	strFormat.Format(Fun_FindMsg("OnInitialUpdate_msg2","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCrtUnit());
	GetDlgItem(IDC_I_STATIC)->SetWindowText(strFormat);
	strFormat.Format(Fun_FindMsg("OnInitialUpdate_msg3","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCrtUnit());
	GetDlgItem(IDC_RPT_I_STATIC)->SetWindowText(strFormat);
	
	strFormat.Format(Fun_FindMsg("OnInitialUpdate_msg4","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
	GetDlgItem(IDC_V_STATIC)->SetWindowText(strFormat);
	strFormat.Format(Fun_FindMsg("OnInitialUpdate_msg5","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
	GetDlgItem(IDC_RPT_V_STATIC)->SetWindowText(strFormat);
	
	strFormat.Format(Fun_FindMsg("OnInitialUpdate_msg6","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCapUnit());
	GetDlgItem(IDC_CAP_STATIC)->SetWindowText(strFormat);

 	strFormat.Format(Fun_FindMsg("OnInitialUpdate_msg7","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
 	GetDlgItem(IDC_CELL_DELTA_V_STATIC)->SetWindowText(strFormat);//yulee 20190909

	strFormat.Format(Fun_FindMsg("OnInitialUpdate_msg8","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
	GetDlgItem(IDC_STEP_AUXV_DELTA_STATIC)->SetWindowText(strFormat);//yulee 20190909
	

	GetDlgItem(IDC_CHECK_MAXV_UNIT_STATIC)->SetWindowText(GetDocument()->GetVtgUnit());	
	GetDlgItem(IDC_CHECK_MINV_UNIT_STATIC)->SetWindowText(GetDocument()->GetVtgUnit());	

	GetDlgItem(IDC_CHECK_MAX_CRT_UNIT_STATIC)->SetWindowText(GetDocument()->GetCrtUnit());

	GetDlgItem(IDC_CHECK_MAX_C_UNIT_STATIC)->SetWindowText(GetDocument()->GetCapUnit());	
	//GetDlgItem(IDC_CHECK_MIN_C_UNIT_STATIC)->SetWindowText(GetDocument()->GetCapUnit());

	GetDlgItem(IDC_CHECK_MAX_W_UNIT_STATIC)->SetWindowText(GetDocument()->GetWattUnit());	
	GetDlgItem(IDC_CHECK_MAX_WH_UNIT_STATIC)->SetWindowText(GetDocument()->GetWattHourUnit());	
	
	GetDlgItem(IDC_REPORT_TIME_MILI)->SendMessage(EM_LIMITTEXT, 3, 0);

	//2014.09.22 기록조건 10msec 읽기전용 삭제	
	m_bUseMsec = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseMsec", FALSE);
	if(m_bUseMsec){		
		CEdit* theEdit = (CEdit*)GetDlgItem(IDC_REPORT_TIME_MILI);
		theEdit->SetReadOnly(FALSE);	
	}

	//ksj 20171031 : 사용자 정의 msec 스핀 증감 값 추가.
	m_nCustomMsec = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec Interval",0); // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.

	//yulee 20181002 화면에 작업 장소 표시 
	BOOL bUsePlacePlate;
	
	bUsePlacePlate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", FALSE);
	
	if(bUsePlacePlate)
	{
		if(m_pWorkSTSignEditDlg == NULL)
		{
			m_pWorkSTSignEditDlg = new CWorkSTSignEditDlg();
			m_pWorkSTSignEditDlg->Create(IDD_WORKSTSIGNEDIT_DLG, this);
		}
		m_pWorkSTSignEditDlg->ShowWindow(SW_SHOW);
		CRect rectTemp;
		m_pWorkSTSignEditDlg->GetWindowRect(rectTemp);
		::SetWindowPos(m_pWorkSTSignEditDlg->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
		m_pWorkSTSignEditDlg->ShowWindow(SW_SHOWMINIMIZED); //yulee 20181005
	}

	OnWorkSTShow(); //yulee 20181005

	//cny 201809
	ShowCellBalDlg(0);
	ShowPwrSupplyDlg(0);
	ShowChillerDlg(0);
	ShowPattSchedDlg(0);
	ShowPattSelectDlg(0);

	g_CCTSEditorProViewHandle = this;

	m_bUseVentSafety = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Use Vent Safety", 1); //ksj 20200212

	if(m_bUseVentSafety == 0) //ksj 20200212 : vent 사용 버튼 숨김.
	{
		GetDlgItem(IDC_CHK_DELTA_CHAMBER)->ShowWindow(SW_HIDE);
		m_bChkDeltaVVent.SetCheck(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_HIDE);
		m_bChkStepDeltaVentAuxTemp.SetCheck(FALSE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_HIDE);
		m_bChkStepVentAuxV.SetCheck(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_HIDE);
		m_bChkStepDeltaVentAuxTh.SetCheck(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->ShowWindow(SW_HIDE);
		m_bChkStepDeltaVentAuxT.SetCheck(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_HIDE);	
		m_bChkStepDeltaVentAuxV.SetCheck(FALSE);
	}
}

void CCTSEditorProView::InitToolTips()
{
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);

// 	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_CHECK), "전지 접촉 검사");
	
	//전압
//#ifdef _CYCLER_
// 	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_MAXV), "안전 전압 최대값");
// 	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_MINV), "안전 전압 최소값");
// 
// 	//전류
// 	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_MAXI), "안전 전류 최대값(>0)");
// 	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_TRCKLE_I), "안전 전류 최소값(>0)");
// 
// 	//용량 
// 	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_OCV), "안전 용량 최대값");
// 	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_DELTA_V), "안전 용량 최소값");
//#endif 
	//전압
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MAX_V), Fun_FindMsg("InitToolTips_msg1","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MIN_V), Fun_FindMsg("InitToolTips_msg2","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_CELL_DELTA_V), Fun_FindMsg("InitToolTips_msg3","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_CELL_DELTA_MIN_V), Fun_FindMsg("InitToolTips_msg47","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_CELL_DELTA_MAX_V), Fun_FindMsg("InitToolTips_msg48","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V), Fun_FindMsg("InitToolTips_msg45","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V), Fun_FindMsg("InitToolTips_msg46","IDD_CTSEditorPro_CYCL"));
	
	//전류
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MAX_I), Fun_FindMsg("InitToolTips_msg4","IDD_CTSEditorPro_CYCL"));
	
	//온도
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MAX_T), Fun_FindMsg("InitToolTips_msg5","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MIN_T), Fun_FindMsg("InitToolTips_msg6","IDD_CTSEditorPro_CYCL"));

	//기타
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MAX_C), Fun_FindMsg("InitToolTips_msg7","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MAX_W), Fun_FindMsg("InitToolTips_msg8","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_SAFETY_MAX_WH), Fun_FindMsg("InitToolTips_msg9","IDD_CTSEditorPro_CYCL"));
	
	m_tooltip.AddTool(GetDlgItem(IDC_CELL_CHECK_TIME_EDIT), Fun_FindMsg("InitToolTips_msg10","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_PRETEST_FAULT_NO), Fun_FindMsg("InitToolTips_msg11","IDD_CTSEditorPro_CYCL"));

	//&& m_tooltip.AddTool(GetDlgItem(IDC_GRADE_CHECK), "Step에 대한 Grade 여부 선택");
	m_tooltip.AddTool(GetDlgItem(IDC_GRADE_CHECK), Fun_FindMsg("EditorView_InitToolTips_msg1","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_VTG_HIGH), Fun_FindMsg("InitToolTips_msg13","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_VTG_LOW), Fun_FindMsg("InitToolTips_msg14","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CRT_HIGH), Fun_FindMsg("InitToolTips_msg15","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CRT_LOW), Fun_FindMsg("InitToolTips_msg16","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CAP_HIGH), Fun_FindMsg("InitToolTips_msg17","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CAP_LOW), Fun_FindMsg("InitToolTips_msg18","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_IMP_HIGH), Fun_FindMsg("InitToolTips_msg19","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_IMP_LOW), Fun_FindMsg("InitToolTips_msg20","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_TEMP_HIGH), Fun_FindMsg("InitToolTips_msg21","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_TEMP_LOW), Fun_FindMsg("InitToolTips_msg22","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_DELTA_V), Fun_FindMsg("InitToolTips_msg23","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_DELTA_I), Fun_FindMsg("InitToolTips_msg24","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_DELTA_TIME), Fun_FindMsg("InitToolTips_msg25","IDD_CTSEditorPro_CYCL"));

	m_tooltip.AddTool(GetDlgItem(IDC_STEP_AUXT_DELTA), Fun_FindMsg("InitToolTips_msg49","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_STEP_AUXTH_DELTA), Fun_FindMsg("InitToolTips_msg50","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA), Fun_FindMsg("InitToolTips_msg51","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_STEP_AUXV_TIME), Fun_FindMsg("InitToolTips_msg52","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_STEP_AUXV_DELTA), Fun_FindMsg("InitToolTips_msg53","IDD_CTSEditorPro_CYCL"));

	m_tooltip.AddTool(GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V), Fun_FindMsg("InitToolTips_msg54","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP), Fun_FindMsg("InitToolTips_msg55","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH), Fun_FindMsg("InitToolTips_msg56","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T), Fun_FindMsg("InitToolTips_msg57","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_CHK_STEP_AUX_V), Fun_FindMsg("InitToolTips_msg58","IDD_CTSEditorPro_CYCL"));

	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V1), Fun_FindMsg("InitToolTips_msg26","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V4), Fun_FindMsg("InitToolTips_msg27","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_TIME1), Fun_FindMsg("InitToolTips_msg28","IDD_CTSEditorPro_CYCL"));	
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V2), Fun_FindMsg("InitToolTips_msg29","IDD_CTSEditorPro_CYCL"));	
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V5), Fun_FindMsg("InitToolTips_msg30","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V2), Fun_FindMsg("EditorView_InitToolTips_msg1","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V5), Fun_FindMsg("EditorView_InitToolTips_msg2","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_TIME2), Fun_FindMsg("InitToolTips_msg31","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V3), Fun_FindMsg("InitToolTips_msg32","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_V6), Fun_FindMsg("InitToolTips_msg33","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_TIME3), Fun_FindMsg("InitToolTips_msg34","IDD_CTSEditorPro_CYCL"));


	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I1), Fun_FindMsg("InitToolTips_msg35","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I4), Fun_FindMsg("InitToolTips_msg36","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I_TIME1), Fun_FindMsg("InitToolTips_msg37","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I2), Fun_FindMsg("InitToolTips_msg38","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I5), Fun_FindMsg("InitToolTips_msg39","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I_TIME2), Fun_FindMsg("InitToolTips_msg40","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I3), Fun_FindMsg("InitToolTips_msg41","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I6), Fun_FindMsg("InitToolTips_msg42","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_COMP_I_TIME3), Fun_FindMsg("InitToolTips_msg43","IDD_CTSEditorPro_CYCL"));
	m_tooltip.AddTool(GetDlgItem(IDC_BUTTON_MODEL_SEL), Fun_FindMsg("InitToolTips_msg44","IDD_CTSEditorPro_CYCL"));	
	m_tooltip.AddTool(GetDlgItem(IDC_LOGO_STATIC), "http://www.pnesolution.com");	
	

}


/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProView printing

BOOL CCTSEditorProView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCTSEditorProView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCTSEditorProView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CCTSEditorProView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProView diagnostics

#ifdef _DEBUG
void CCTSEditorProView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCTSEditorProView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCTSEditorProDoc* CCTSEditorProView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSEditorProDoc)));
	return (CCTSEditorProDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProView message handlers

LONG CCTSEditorProView::OnGridClicked(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	if(nRow < 1)	return 0L;
	
	CString strTemp;
	if(pGrid != (CMyGridWnd *)&m_wndStepGrid)	return 0L;	//Step List Grid

	CGXStyle style;
	
	pGrid->GetStyleRowCol(nRow, nCol , style, gxCopy, 0);
	
	//Disable 상태이면 Event가 발생하지 않으므로 OnMovedCurrentCell()을 발생 시킴 
	if(style.GetIncludeEnabled())
	{
		if(!style.GetEnabled())
		{
			#ifdef _CYCLER_
				pGrid->SetCurrentCell(nRow, COL_STEP_TYPE);
			#else
				pGrid->SetCurrentCell(nRow, COL_STEP_PROCTYPE);
			#endif
		}	
	}

	//현재 Cell이 End Condition Cell이고 Dlg가 숨겨져 있을 경우 표시한다.
	if(nCol == COL_STEP_END)
	{
		//End Condition Dlg가 없으면 Display하고 있으면 숨긴다.
		m_bShowEndDlgToggle = !m_bShowEndDlgToggle;
		if(m_bShowEndDlgToggle)
		{
			HideEndDlg();
		}
		else 

		{
			ShowEndTypeDlg(nRow);
		}
	}

	//cny 201809
	if(nCol==COL_STEP_CELLBAL)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nRow);
		if(!pStep) return 0;
		
		if( pStep->chType==PS_STEP_REST ||
			pStep->chType==PS_STEP_CHARGE ||
			pStep->chType==PS_STEP_DISCHARGE ||
			pStep->chType==PS_STEP_PATTERN)
		{
			m_bShowCellBalDlgToggle = !m_bShowCellBalDlgToggle;
			
			if(m_bShowCellBalDlgToggle)
			{
				ShowCellBalDlg(1,pStep);
			}
			else
			{
				HideCellBalDlg();
			}
		}
	}
	
	//cny 201809
	if(nCol==COL_STEP_PWRSUPPLY)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nRow);
		if(!pStep) return 0;
		
		if( pStep->chType==PS_STEP_REST) //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
		{
			m_bShowPwrSupplyDlgToggle = !m_bShowPwrSupplyDlgToggle;
			
			if(m_bShowPwrSupplyDlgToggle)
			{
				ShowPwrSupplyDlg(1,pStep);
			}
			else
			{
				HidePwrSupplyDlg();
			}
		}
	}
	//cny 201809
	if(nCol==COL_STEP_CHILLER)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nRow);
		if(!pStep) return 0;
		
		m_bShowChillerDlgToggle = !m_bShowChillerDlgToggle;
		
		if(m_bShowChillerDlgToggle)
		{
			ShowChillerDlg(1,pStep);
		}
		else
		{
			HideChillerDlg();
		}
	}
	//cny 201809
	if(nCol==COL_STEP_PATTABLE)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nRow);
		if(!pStep) return 0;
		
		if(pStep->chType==PS_STEP_ADV_CYCLE)
		{
			m_bShowPattSchedDlgToggle = !m_bShowPattSchedDlgToggle;
			
			if(m_bShowPattSchedDlgToggle)
			{
				ShowPattSchedDlg(1,pStep);
			}
			else
			{
				HidePattSchedDlg();
			}
		}
		else if(pStep->chType!=PS_STEP_OCV &&
			    pStep->chType!=PS_STEP_LOOP &&
			    pStep->chType!=PS_STEP_END)		
		{
			m_bShowPattSelectDlgToggle = !m_bShowPattSelectDlgToggle;
			
			if(m_bShowPattSelectDlgToggle)
			{
				ShowPattSelectDlg(1,pStep);
			}
			else
			{
				HidePattSelectDlg();
			}
		}
	}

	return 0L;
}

LONG CCTSEditorProView::OnGridDbClicked(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	int nCount = m_wndStepGrid.GetRowCount();
	if(nCount > 0 && nRow == 0  )
	{
		if(nCol == COL_STEP_REF_I)
		{
			CCommonInputDlg dlg;
			dlg.m_strTitleString = Fun_FindMsg("OnGridDbClicked_msg1","IDD_CTSEditorPro_CYCL");
			if(dlg.DoModal() == IDOK)
			{
				STEP *pStep;
				for(int step = 0; step < GetDocument()->GetTotalStepNum(); step++)
				{
					pStep = GetDocument()->GetStepData(step+1);
					if(pStep == NULL)	break;
			
					if(dlg.m_nMode == 1)	//All Step
					{
						pStep->fIref = dlg.m_fValue;
						m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
					}
					else if(dlg.m_nMode == 2)	//Charge step
					{
						if(pStep->chType == PS_STEP_CHARGE)
						{
							pStep->fIref = dlg.m_fValue;
							if(pStep->chMode == PS_MODE_CP)		//ljb
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_POWER));
							else
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
						}
					}
					else if(dlg.m_nMode == 3)	//DisCharge step
					{
						if(pStep->chType == PS_STEP_DISCHARGE)
						{
							pStep->fIref = dlg.m_fValue;
							if(pStep->chMode == PS_MODE_CP)		//ljb
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_POWER));
							else
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
//							m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
						}
					}
				}
				//Default 현재 선택된 Step으로 
				DisplayStepGrid();
			}
		}
		else if(nCol == COL_STEP_END)
		{
//			CEndConditionDlg dlg;
//			dlg.DoModal();
		}
		return 0;
	}

	if(nRow < 1)	return 0L;

	if(pGrid == (CMyGridWnd *)&m_wndTestListGrid)			//Test List Grid
	{
		m_bLinkChamber = FALSE;
		m_bDeltaVVent = FALSE;	
		OnLoadTest() ;
//		OnTestEdit() ;
	}
	else if(pGrid == m_pWndCurModelGrid)
	{
//		OnModelEdit() ;
	}

	


	m_wndStepGrid.SetFocus();

	WORD wKeyState = 0;
	wKeyState |= (::GetKeyState(VK_CONTROL) < 0) ? MK_CONTROL:0;
	wKeyState |= (::GetKeyState(VK_SHIFT) < 0) ? MK_SHIFT:0;

	::SendMessage(m_wndStepGrid.m_hWnd, WM_MOUSEWHEEL, MAKEWPARAM(wKeyState, 10000), MAKELPARAM(0, 0));

	return 0;
}

LONG CCTSEditorProView::OnGridKillFocus(WPARAM wParam, LPARAM lParam)
{
/*	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	if(pGrid == &m_wndStepGrid)
		DeleteEndDlg();
*/	return 0L;
}

LRESULT CCTSEditorProView::OnCloseEndDlg(WPARAM wParam, LPARAM lParam)
{
	int nStepNum = int(wParam);
	if(nStepNum >0 && nStepNum <= SCH_MAX_STEP)
	{	
		CString strTemp = GetDocument()->MakeEndString(nStepNum);
		BOOL bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE);
		if(bOverChargerSet)
		{
			if(strTemp.Find(_T("Low V < 0.000"))> -1) //yulee 20190904 //yulee 20191017 OverChargeDischarger Mark
			{
				m_bStepDataSaved = FALSE;
			}
			int nZeroVoltEndCondChkChanged = 0;
			nZeroVoltEndCondChkChanged = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "ZeroVoltEndCondClicked", 0);

			if(nZeroVoltEndCondChkChanged == TRUE)
			{
				m_bStepDataSaved = FALSE;
			}

			if(strTemp.IsEmpty() == TRUE)
			{
				AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "DetectNoEndCond", 1);  //yulee 20190904
			}
			else
			{
				AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "DetectNoEndCond", 0);  //yulee 20190904
			}
		}
		STEP *pStep;
		pStep = (STEP *)GetDocument()->GetStepData(nStepNum);
		if(pStep != NULL)
		{
			//20150806 add 안전값 및 기타 옵션 표시
			DisplayStepOption(pStep);
		}
		m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), _T(strTemp));
		//Update End Type Tools Tip
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_END), CGXStyle()
			         .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
//		m_bStepDataSaved = FALSE;		//Step Data is Edited

		//Formation에서는 CC전압을 자동으로 설정한다.(CC 설정시 반드시 EndV를 입력해야함)
		//EndV 전압의 1%보다 크거나 낮게 자동 설정 
#ifdef _FORMATION_
		pStep = GetDocument()->GetStepData(nStepNum);
		if(pStep)
		{
			if(pStep->chMode == PS_MODE_CC && pStep->fEndV)
			{
				if(pStep->chType == PS_STEP_CHARGE)
				{
					pStep->fVref = pStep->fEndV + GetDocument()->m_lMaxVoltage * 0.01;		//1% Margin
					if(pStep->fVref > GetDocument()->m_lMaxVoltage)	pStep->fVref = GetDocument()->m_lMaxVoltage;
				}
				else if(pStep->chType == PS_STEP_DISCHARGE)
				{
					pStep->fVref = pStep->fEndV - GetDocument()->m_lMaxVoltage * 0.01;
					if(pStep->fVref < 0)	pStep->fVref = 0;
				}
			}
			DisplayStepGrid(nStepNum);
		}
#endif _FORMATION_
	}
	return 1;
}


LRESULT CCTSEditorProView::OnCloseDlgCellBal(WPARAM wParam, LPARAM lParam)
{
	int nStepNum = int(wParam);
	if(nStepNum >0 && nStepNum <= SCH_MAX_STEP)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nStepNum);
		if(pStep == NULL) return 0;
		
		CString strTemp    = GetDocument()->MakeCellBalString(nStepNum);
		CString strToolTip = GetDocument()->MakeCellBalString(nStepNum,1);
		
		m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CELLBAL), strTemp);		
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_CELLBAL), 
			                        CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, strToolTip));
		if(m_pDlgCellBal) m_pDlgCellBal->m_pStep=NULL;
	}
	return 1;
}

LRESULT CCTSEditorProView::OnCloseDlgPwrSupply(WPARAM wParam, LPARAM lParam)
{
	int nStepNum = int(wParam);
	if(nStepNum >0 && nStepNum <= SCH_MAX_STEP)
	{
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nStepNum);
		if(pStep == NULL) return 0;
		
		CString strTemp = GetDocument()->MakePwrSupplyString(nStepNum);
		CString strToolTip = GetDocument()->MakePwrSupplyString(nStepNum,1);
		
		m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY), strTemp);		
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY), 
			                        CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, strToolTip));
		if(m_pDlgPwrSupply) m_pDlgPwrSupply->m_pStep=NULL;
	}
	return 1;
}

LRESULT CCTSEditorProView::OnCloseDlgChiller(WPARAM wParam, LPARAM lParam)
{
	int nStepNum = int(wParam);
	if(nStepNum >0 && nStepNum <= SCH_MAX_STEP)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nStepNum);
		if(pStep == NULL) return 0;
		
		CString strTemp = GetDocument()->MakeChillerString(nStepNum);
		CString strTooltip = GetDocument()->MakeChillerString(nStepNum,1);
		
		m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHILLER), strTemp);
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_CHILLER), 
			                        CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, strTooltip));
		if(m_pDlgChiller) m_pDlgChiller->m_pStep=NULL;
	}
	return 1;
}

LRESULT CCTSEditorProView::OnCloseDlgPattSched(WPARAM wParam, LPARAM lParam)
{
	int nStepNum = int(wParam);
	if(nStepNum >0 && nStepNum <= SCH_MAX_STEP)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nStepNum);
		if(pStep == NULL) return 0;
		
		if(pStep->chType==PS_STEP_ADV_CYCLE)
		{
			CString strTemp = GetDocument()->MakePattSchedString(nStepNum);
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PATTABLE), strTemp);
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_PATTABLE), 
				CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, strTemp));			
			if(m_pDlgPattSched) m_pDlgPattSched->m_pStep=NULL;
		}		
	}
	return 1;
}

LRESULT CCTSEditorProView::OnCloseDlgPattSelect(WPARAM wParam, LPARAM lParam)
{
	int nStepNum = int(wParam);
	if(nStepNum >0 && nStepNum <= SCH_MAX_STEP)
	{			
		STEP *pStep = (STEP *)GetDocument()->GetStepData(nStepNum);
		if(pStep == NULL) return 0;
		
		if( pStep->chType!=PS_STEP_ADV_CYCLE &&
			pStep->chType!=PS_STEP_LOOP &&
			pStep->chType!=PS_STEP_END )
		{
			CString strTemp = GetDocument()->MakePattSelectString(nStepNum);
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PATTABLE), strTemp);
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_PATTABLE), 
				CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, strTemp));			
			if(m_pDlgPattSelect) m_pDlgPattSelect->m_pStep=NULL;
		}		
	}
	return 1;
}

LONG CCTSEditorProView::OnMovedCurrentCell(WPARAM wParam, LPARAM lParam)
{

	CString strTemp;
	ROWCOL nRow, nCol;			
	
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	if(nRow < 1)	return 0L;					//Row or Column Number Error
	//20081208 KHS Grade Grid
	if(pGrid == (CMyGridWnd *)&m_wndGradeGrid)			//Test List Grid
	{
		if(m_nDisplayStep < 0 || m_nDisplayStep >= GetDocument()->GetTotalStepNum())	return 0L;
		STEP *pStep;
		pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
		
		if(!pStep)	return 0L;
		
		switch((int)pStep->chType)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			//			AfxMessageBox("charge step");
			// 			sprintf(str, "%s = 항목 단위", "단위");
			// 			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
			if (nCol == _COL_ITEM1_ || nCol == _COL_GRADE_MIN1_ || nCol == _COL_GRADE_MAX1_)
			{
				switch (atoi(m_wndGradeGrid.GetValueRowCol(nRow, _COL_ITEM1_)))
				{
				case PS_GRADE_VOLTAGE:
 					strTemp.Format(Fun_FindMsg("OnMovedCurrentCell_msg1","IDD_CTSEditorPro_CYCL"),GetDocument()->GetVtgUnit());
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_CAPACITY:
 					strTemp.Format(Fun_FindMsg("OnMovedCurrentCell_msg2","IDD_CTSEditorPro_CYCL"),GetDocument()->GetCapUnit());
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_IMPEDANCE:
 					strTemp = Fun_FindMsg("OnMovedCurrentCell_msg3","IDD_CTSEditorPro_CYCL");
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_CURRENT:
 					strTemp.Format(Fun_FindMsg("OnMovedCurrentCell_msg4","IDD_CTSEditorPro_CYCL"),GetDocument()->GetCrtUnit());
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_TIME:
 					strTemp = Fun_FindMsg("OnMovedCurrentCell_msg5","IDD_CTSEditorPro_CYCL");
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_FARAD:
  					strTemp.Format(Fun_FindMsg("OnMovedCurrentCell_msg6","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCapUnit());
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				default :
 					strTemp = Fun_FindMsg("OnMovedCurrentCell_msg7","IDD_CTSEditorPro_CYCL");
					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
					break;
				}
			}
			else if (nCol == _COL_ITEM2_ || nCol == _COL_GRADE_MIN2_ || nCol == _COL_GRADE_MAX2_)
			{
				switch (atoi(m_wndGradeGrid.GetValueRowCol(nRow, _COL_ITEM2_)))
				{
 				case PS_GRADE_VOLTAGE:
 					strTemp.Format(Fun_FindMsg("OnMovedCurrentCell_msg8","IDD_CTSEditorPro_CYCL"),GetDocument()->GetVtgUnit());
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_CAPACITY:
 					strTemp.Format(Fun_FindMsg("OnMovedCurrentCell_msg9","IDD_CTSEditorPro_CYCL"),GetDocument()->GetCapUnit());
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_IMPEDANCE:
 					strTemp = Fun_FindMsg("OnMovedCurrentCell_msg10","IDD_CTSEditorPro_CYCL");
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_CURRENT:
 					strTemp.Format(Fun_FindMsg("OnMovedCurrentCell_msg11","IDD_CTSEditorPro_CYCL"),GetDocument()->GetCrtUnit());
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_TIME:
 					strTemp = Fun_FindMsg("OnMovedCurrentCell_msg12","IDD_CTSEditorPro_CYCL");
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				case PS_GRADE_FARAD:
 					strTemp = Fun_FindMsg("OnMovedCurrentCell_msg13","IDD_CTSEditorPro_CYCL");
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
 				default :
 					strTemp = Fun_FindMsg("OnMovedCurrentCell_msg14","IDD_CTSEditorPro_CYCL");
 					GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(strTemp);
 					break;
				}
				
			}
			break;
		case PS_STEP_IMPEDANCE:
			if (nCol == _COL_ITEM1_)
			{
				m_wndGradeGrid.SetStyleRange(CGXRange(nRow,_COL_ITEM1_),
					CGXStyle().SetValue((double)PS_GRADE_IMPEDANCE));
				m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(nRow, nCol,1,1),
					CGXStyle().SetEnabled(FALSE));
			}
			break;
		case PS_STEP_OCV:
			if (nCol == _COL_ITEM1_)
			{
				m_wndGradeGrid.SetStyleRange(CGXRange(nRow,_COL_ITEM1_),
					CGXStyle().SetValue((double)PS_GRADE_VOLTAGE));
				m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(nRow, nCol,1,1),
					CGXStyle().SetEnabled(FALSE));
			}
			break;
		}
		return 0L;
	}

	if(pGrid == (CMyGridWnd *)&m_wndTestListGrid)			//Test List Grid
	{
		strTemp = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY);	//Test ID
		if(!strTemp.IsEmpty())
		{
			GetDlgItem(IDC_LOAD_TEST)->EnableWindow(TRUE);
			UpdateDspTest(pGrid->GetValueRowCol(nRow, COL_TEST_NAME), atoi((LPCSTR)(LPCTSTR)strTemp));
		}
		else
		{
			GetDlgItem(IDC_LOAD_TEST)->EnableWindow(FALSE);
		}
	}

	if(pGrid == m_pWndCurModelGrid)		//Battery Model Grid
	{
		strTemp = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_PRIMARY_KEY);	//Battery Model ID
		if(strTemp.IsEmpty())
		{
			int nTotTestNum = m_wndTestListGrid.GetRowCount();
			if(nTotTestNum >0)	m_wndTestListGrid.RemoveRows( 1, nTotTestNum);
			
			GetDlgItem(IDC_TEST_NEW)->EnableWindow(FALSE);
			GetDlgItem(IDC_TEST_DELETE)->EnableWindow(FALSE);
			GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);
			m_TestNameLabel.SetText(Fun_FindMsg("OnMovedCurrentCell_msg15","IDD_CTSEditorPro_CYCL"));
		}
		else
		{
			UpdateDspModel(pGrid->GetValueRowCol(nRow, 1), atoi((LPCSTR)(LPCTSTR)strTemp));
			RequeryTestList(m_lDisplayModelID);

			GetDlgItem(IDC_TEST_NEW)->EnableWindow(TRUE);
			GetDlgItem(IDC_TEST_DELETE)->EnableWindow(TRUE);
			GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);
		
//			strTemp.Format("%s[ID:%ld] 시험 List", m_strDspModelName, m_lDisplayModelID);
//			strTemp.Format("%s", m_strDspModelName);
//			m_TestNameLabel.SetText(strTemp);
		}
	}

//	strTemp.Format("  Model: %s[ID %ld], 공정명: %s [ID %ld]", m_strDspModelName, m_lDisplayModelID, m_strDspTestName, m_lDisplayTestID);
	if(m_strDspTestName.IsEmpty())
	{
		strTemp = Fun_FindMsg("OnMovedCurrentCell_msg16","IDD_CTSEditorPro_CYCL");
//		m_strTestName.SetTextColor(RGB(255, 100, 100));
	}
	else
	{
		strTemp.Format("%s", m_strDspTestName);
//		m_strTestName.SetTextColor(RGB(0, 0, 0));
	}
	m_strTestName.SetText(strTemp);
	
	if(pGrid != (CMyGridWnd *)&m_wndStepGrid)	return 0L;	//Step List Grid

	HideEndDlg();

	int nTotStep = pGrid->GetRowCount();
	if(nRow > nTotStep )			return 0L;				//Step is already Deleted
	
	if(m_nDisplayStep > nTotStep)	m_nDisplayStep = nTotStep;	//S

	if(!UpdateStepGrid(m_nDisplayStep))					//Save Left Step Data
//	if(!UpdateStepGrid(nRow))					//ljb 20110917 
	{
		AfxMessageBox("Update step data error");
	}

		
	if(m_nDisplayStep != nRow)
	{
		m_bShowEndDlgToggle = FALSE;
		if(!DisplayStepGrid(nRow))							//Display Current Step Data
		{
			AfxMessageBox("Step display error");
		}
	}

	if(nCol ==  COL_STEP_END)		//End Type Column
	{
		ShowEndTypeDlg(nRow);							//Show End Type Dlg
	}
	return 0L;
}

void CCTSEditorProView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	//DeleteEndDlg();
	CFormView::OnLButtonDown(nFlags, point);
}

//Resize Window
void CCTSEditorProView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	

	//DeleteEndDlg();
	// 	CRect rect,rect2;
	// 	::GetClientRect(m_hWnd, &rect);
	// 	if(rect.bottom<600 && rect.right<800)	return;
	// 
	// 	CFormView::ShowScrollBar(SB_VERT,FALSE);
	// 	CFormView::ShowScrollBar(SB_HORZ,FALSE);
	// 
	// 	CString strTemp;
	// 	strTemp.Format("%d,%d",rect.Width(),rect.Height());
	// 	
	// if( this->GetSafeHwnd())
	// {
	// 	GetDlgItem(IDC_STEP_RANGE)->GetClientRect(rect2);
	// 
	// 	::MoveWindow(m_wndStepGrid,rect2.left+10,rect.top + 300,rect.Width()-rect2.left,rect.Height()-rect.Height()-300, FALSE);
	// 
	// }
	/*
	if( this->GetSafeHwnd())
	{
		::MoveWindow(m_wndBatteryModelGrid,	10, 55, rect.right*0.42, rect.bottom*0.27, FALSE);
		::MoveWindow(m_wndTestListGrid	  ,	rect.right*0.42+10+20, 55, rect.right*(1-0.42)-(10+20+10), rect.bottom*0.27, FALSE);

		if(GetDlgItem(IDC_STEP_RANGE)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_RANGE)->MoveWindow( 10, rect.bottom*0.27+55+35, rect.right-20, rect.bottom*(1-0.27)-(55+40+10), FALSE);
		::MoveWindow(m_wndStepGrid, 20, rect.bottom*0.27+55+110, rect.right*0.52, rect.bottom*(1-0.27)-(55+110+20), FALSE);
		if(GetDlgItem(IDC_LOADED_TEST)->GetSafeHwnd())
			GetDlgItem(IDC_LOADED_TEST)->SetWindowPos(NULL, 20, rect.bottom*0.27+55+55, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_INSERT)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_INSERT)->SetWindowPos(NULL, 20, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_DELETE)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_DELETE)->SetWindowPos(NULL, 110, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_SAVE)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_SAVE)->SetWindowPos(NULL, 200, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_SELECT_TEST_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_SELECT_TEST_LABEL)->SetWindowPos(NULL, 10, rect.bottom*0.27+55+10, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_NAME)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_NAME)->SetWindowPos(NULL, 120, rect.bottom*0.27+55+10, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_TEST_NEW)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_NEW)->SetWindowPos(NULL, rect.right*0.42+10+20+10, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_DELETE)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_DELETE)->SetWindowPos(NULL, rect.right*0.42+10+20+110, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_SAVE)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_SAVE)->SetWindowPos(NULL, rect.right*0.42+10+20+210, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_LIST_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_LIST_LABEL)->SetWindowPos(NULL, rect.right*0.42+10+20+10, 10, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_MODEL_TOT_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_MODEL_TOT_LABEL)->SetWindowPos(NULL, rect.right*0.42-90, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_MODEL_TOTNUM)->GetSafeHwnd())
			GetDlgItem(IDC_MODEL_TOTNUM)->SetWindowPos(NULL, rect.right*0.42-50, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TEST_TOT_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_TEST_TOT_LABEL)->SetWindowPos(NULL,  rect.right-10-90, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TOT_TESTNUM)->GetSafeHwnd())
			GetDlgItem(IDC_TOT_TESTNUM)->SetWindowPos(NULL,  rect.right-10-50, 30, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		if(GetDlgItem(IDC_LOAD_TEST)->GetSafeHwnd())
			GetDlgItem(IDC_LOAD_TEST)->SetWindowPos(NULL, rect.right*0.52-150, rect.bottom*0.27+55+50, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_STEP_TOT_LABEL)->GetSafeHwnd())
			GetDlgItem(IDC_STEP_TOT_LABEL)->SetWindowPos(NULL, rect.right*0.52-130, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		if(GetDlgItem(IDC_TOT_STEP_COUNT)->GetSafeHwnd())
			GetDlgItem(IDC_TOT_STEP_COUNT)->SetWindowPos(NULL, rect.right*0.52-50, rect.bottom*0.27+55+85, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

		
		//		m_wndGradeGrid.MoveWindow( 5, rect.bottom/100*42, rect.right-10, rect.bottom-rect.bottom/100*42-5, FALSE);
	}
	SetModelGridColumnWidth();
	SetTestGridColumnWidth();*/
}

//Init Model Grid
BOOL CCTSEditorProView::InitBatteryModelGrid(UINT nID, CWnd *pParent, CMyGridWnd *pGrid)
{
	m_pWndCurModelGrid = pGrid;

	m_pWndCurModelGrid->SubclassDlgItem(nID, pParent);
	m_pWndCurModelGrid->Initialize();
	m_pWndCurModelGrid->GetParam()->EnableUndo(FALSE);
	m_pWndCurModelGrid->SetColCount(5);

	SetModelGridColumnWidth();

	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, 0),	CGXStyle().SetValue("No"));
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_NAME),	CGXStyle().SetValue(Fun_FindMsg("InitBatteryModelGrid_msg1","IDD_CTSEditorPro_CYCL")));
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_CREATOR),	CGXStyle().SetValue(Fun_FindMsg("InitBatteryModelGrid_msg2","IDD_CTSEditorPro_CYCL")));

#ifdef _FORMATION_
	//formatio시는 적용 모델명이 기록된다.
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_DESCRIPTION),	CGXStyle().SetValue(Fun_FindMsg("InitBatteryModelGrid_msg3")));
#else
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_DESCRIPTION),	CGXStyle().SetValue(Fun_FindMsg("InitBatteryModelGrid_msg4","IDD_CTSEditorPro_CYCL")));
#endif
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_PRIMARY_KEY),	CGXStyle().SetValue("Primary Key"));
	m_pWndCurModelGrid->SetStyleRange(CGXRange(0, COL_MODEL_EDIT_STATE),	CGXStyle().SetValue("Edit_State"));
	m_pWndCurModelGrid->SetStyleRange(CGXRange().SetCols(COL_MODEL_PRIMARY_KEY, COL_MODEL_EDIT_STATE),	CGXStyle().SetEnabled(FALSE));
	m_pWndCurModelGrid->SetStyleRange(CGXRange().SetCols(COL_MODEL_PRIMARY_KEY, COL_MODEL_EDIT_STATE), CGXStyle().SetControl(GX_IDS_CTRL_STATIC));
	m_pWndCurModelGrid->GetParam()->EnableUndo(TRUE);
	m_pWndCurModelGrid->GetParam()->SetSortRowsOnDblClk(TRUE);
	m_pWndCurModelGrid->SetStyleRange(CGXRange().SetCols(COL_MODEL_NAME, COL_MODEL_DESCRIPTION), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));

	
/*	m_pWndCurModelGrid->SetStyleRange(CGXRange().SetCols(1,2),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
			.SetHorizontalAlignment(DT_LEFT)
	);
*/	
	if(!RequeryBatteryModel())	return FALSE;		//Load Battery Model From Data Base and Display

	return TRUE;
}

//Init Test Grid

BOOL CCTSEditorProView::InitTestListGrid()
{

	m_wndTestListGrid.SubclassDlgItem(IDC_TESTLIST_GRID, this);
	m_wndTestListGrid.Initialize();
	m_wndTestListGrid.GetParam()->EnableUndo(FALSE);
	
	m_wndTestListGrid.SetColCount(8);
	SetTestGridColumnWidth();

	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_NO),	CGXStyle().SetValue("No"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_PROC_TYPE),	CGXStyle().SetValue("Type"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_NAME),	CGXStyle().SetValue(Fun_FindMsg("InitTestListGrid_msg1","IDD_CTSEditorPro_CYCL")));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_CREATOR),	CGXStyle().SetValue(Fun_FindMsg("InitTestListGrid_msg2","IDD_CTSEditorPro_CYCL")));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_DESCRIPTION),	CGXStyle().SetValue(Fun_FindMsg("InitTestListGrid_msg3","IDD_CTSEditorPro_CYCL")));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_EDIT_TIME),	CGXStyle().SetValue(Fun_FindMsg("InitTestListGrid_msg4","IDD_CTSEditorPro_CYCL")));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_RESET_PROC),	CGXStyle().SetValue(Fun_FindMsg("InitTestListGrid_msg5","IDD_CTSEditorPro_CYCL")));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_PRIMARY_KEY),	CGXStyle().SetValue("Primary Key"));
	m_wndTestListGrid.SetStyleRange(CGXRange(0, COL_TEST_EDIT_STATE),	CGXStyle().SetValue("Edit_State"));
	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_PRIMARY_KEY),	CGXStyle().SetEnabled(FALSE).SetControl(GX_IDS_CTRL_STATIC));
	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_EDIT_STATE),	CGXStyle().SetEnabled(FALSE).SetControl(GX_IDS_CTRL_STATIC));

	m_wndTestListGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));

	m_pTestTypeCombo = new CGridComboBox(this, 
								&m_wndTestListGrid,
								GX_IDS_CTRL_ZEROBASED_EX5,
								GX_IDS_CTRL_ZEROBASED_EX5,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE|GXCOMBO_TEXTFIT
								);
	m_wndTestListGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX5, m_pTestTypeCombo);

	CString strType, strMsg;
	CString strDBName = GetDataBaseName();
	int nCount = 0;
	if(!strDBName.IsEmpty())
	{
		CDaoDatabase  db;
		COleVariant data;

		CString strSQL("SELECT TestType, TestTypeName FROM TestType ORDER BY TestType");			
		try
		{
			db.Open(strDBName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				strType.Empty();
				while(!rs.IsEOF())
				{
					data = rs.GetFieldValue(0);
					m_pTestTypeCombo->SetItemData(nCount++, data.lVal);
					
					data = rs.GetFieldValue(1);
					strMsg = data.pbVal;
					strType += strMsg+CString("\n");
					rs.MoveNext();
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			e->Delete();
		}
		
		//2006/9/29 
		//공정에서 실행 가능한 StepList 추가 (CODERIZ)
		//DataBase 호환을 위해 별도로 검색 한다.
		strSQL="SELECT ExeStep FROM TestType ORDER BY TestType";
		try
		{
			db.Open(strDBName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				while(!rs.IsEOF())
				{
					data = rs.GetFieldValue(0);
					m_strTypeMaskArray.Add(CString(data.pcVal));
					rs.MoveNext();
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			TRACE("Type type table ExeStep Field");
			e->Delete();
		}
	}

	//DataBase에서 TestType  정보를 찾을 수 없을 경우는 Defulat 정보 삽입 
	if(nCount == 0)
	{
		strType = " PreCharge\n OCV\n Formation\n OutCharge";
		m_pTestTypeCombo->SetItemData(0, 1);
		m_pTestTypeCombo->SetItemData(1, 2);
		m_pTestTypeCombo->SetItemData(2, 3);
		m_pTestTypeCombo->SetItemData(3, 4);
	}

	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_PROC_TYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX5)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(strType)
	);


	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_EDIT_TIME),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_DATETIMENOCAL )
			.SetUserAttribute(GX_IDS_UA_DATEFORMATTYPE, GX_DTFORMAT_LONGDATE)
			.SetHorizontalAlignment(DT_LEFT)
	);

	m_wndTestListGrid.SetStyleRange(CGXRange().SetCols(COL_TEST_RESET_PROC),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
			.SetHorizontalAlignment(DT_CENTER)
			.SetChoiceList(" N\n Y")
			.SetEnabled(FALSE)
	);


	m_wndTestListGrid.GetParam()->EnableUndo(TRUE);
	return TRUE;

}

//Init Step Grid
BOOL CCTSEditorProView::InitStepGrid(int nSelLanguage)
{
	CString strType;
	CRect rect, cellRect;
	m_wndStepGrid.SubclassDlgItem(IDC_STEP_GRID, this);
	m_wndStepGrid.Initialize();

	// Sample setup for the grid
	m_wndStepGrid.GetParam()->EnableUndo(FALSE);
	m_wndStepGrid.GetParam()->EnableMoveRows(FALSE);		//Drag and Drop 지원

	m_wndStepGrid.SetColCount(COL_STEP_INDEX);		//khs 20081113  //ljb m_wndStepGrid.SetColCount(9);
	m_wndStepGrid.GetClientRect(&rect);
	
	int nColTempSize[6] = {}; //Type,Charge(V),Discharge(V),Current(A),Power(W),Oven Temp, Oven Humi, PowerSupply
	
	switch (nSelLanguage)
	{
	case 1: nColTempSize[0]=100;nColTempSize[1]=70; nColTempSize[2]=70; nColTempSize[3]=60;nColTempSize[4]=70;break;
	case 2: nColTempSize[0]=100;nColTempSize[1]=100;nColTempSize[2]=110;nColTempSize[3]=80;nColTempSize[4]=80;break;
	case 3: nColTempSize[0]=100;nColTempSize[1]=100;nColTempSize[2]=110;nColTempSize[3]=80;nColTempSize[4]=80;break;
	default: nColTempSize[0]=100;nColTempSize[1]=70; nColTempSize[2]=70; nColTempSize[3]=60;nColTempSize[4]=60;break;
	}
	
	//EDLC나 Cycler용이면 Proc Type을 표시하지 않는다.
	#ifdef _CYCLER_
		if(!m_bUseRange)
		{
			m_wndStepGrid.SetColWidth(COL_STEP_CYCLE, COL_STEP_CYCLE, 35);	
			m_wndStepGrid.SetColWidth(COL_STEP_NO, COL_STEP_NO, 35);		
			m_wndStepGrid.SetColWidth(COL_STEP_TYPE, COL_STEP_TYPE, nColTempSize[0]);	
			m_wndStepGrid.SetColWidth(COL_STEP_PROCTYPE, COL_STEP_PROCTYPE, 0);
			m_wndStepGrid.SetColWidth(COL_STEP_MODE, COL_STEP_MODE, 65);	
			m_wndStepGrid.SetColWidth(COL_STEP_REF_CHARGE_V, COL_STEP_REF_CHARGE_V, nColTempSize[1]);	
			m_wndStepGrid.SetColWidth(COL_STEP_REF_DISCHARGE_V, COL_STEP_REF_DISCHARGE_V, nColTempSize[2]);	
			m_wndStepGrid.SetColWidth(COL_STEP_REF_I, COL_STEP_REF_I, nColTempSize[3]);
			m_wndStepGrid.SetColWidth(COL_STEP_RANGE, COL_STEP_RANGE, 0);	//khs RANGE COL 추가
			m_wndStepGrid.SetColWidth(COL_STEP_POWER, COL_STEP_POWER, nColTempSize[4]);	//ljb	POWER COL 추가
			m_wndStepGrid.SetColWidth(COL_STEP_RESISTANCE, COL_STEP_RESISTANCE, 0);	//ljb	20160422 저항 COL 주석처리 (CR모드 없음)
			m_wndStepGrid.SetColWidth(COL_STEP_WAIT_TIME, COL_STEP_WAIT_TIME, 0);	//ljb 20160414 대기시간 추가
			
			
			nColTempSize[5]=((m_bUseChamberTemp)?90:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CHAMBER_TEMP, COL_STEP_CHAMBER_TEMP, nColTempSize[5]);	//ljb	챔버 설정 온도
			
			nColTempSize[5]=((m_bUseChamberHumi)?90:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CHAMBER_HUMI, COL_STEP_CHAMBER_HUMI, nColTempSize[5]);	//ljb	챔버 설정 습도 온도
		
			nColTempSize[5]=((m_bUseCellBal)?80:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CELLBAL, COL_STEP_CELLBAL, nColTempSize[5]);	//lyj 20200901 HLGP

			nColTempSize[5]=((m_bUsePwrSupply)?100:0);
			m_wndStepGrid.SetColWidth(COL_STEP_PWRSUPPLY, COL_STEP_PWRSUPPLY, nColTempSize[5]);//cny 201809
			
			nColTempSize[5]=((m_bUseChiller)?80:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CHILLER, COL_STEP_CHILLER, nColTempSize[5]);//cny 201809
			
			nColTempSize[5]=((m_bUsePatTable)?80:0);
			m_wndStepGrid.SetColWidth(COL_STEP_PATTABLE, COL_STEP_PATTABLE, nColTempSize[5]);//cny 201809
		}
		else
		{
			m_wndStepGrid.SetColWidth(COL_STEP_CYCLE, COL_STEP_CYCLE, 30);	
			m_wndStepGrid.SetColWidth(COL_STEP_NO, COL_STEP_NO, 35);		
			m_wndStepGrid.SetColWidth(COL_STEP_TYPE, COL_STEP_TYPE, nColTempSize[0]);	
			m_wndStepGrid.SetColWidth(COL_STEP_PROCTYPE, COL_STEP_PROCTYPE, 0);
			m_wndStepGrid.SetColWidth(COL_STEP_MODE, COL_STEP_MODE, 65);	
			m_wndStepGrid.SetColWidth(COL_STEP_REF_CHARGE_V, COL_STEP_REF_CHARGE_V, nColTempSize[1]);	
			m_wndStepGrid.SetColWidth(COL_STEP_REF_DISCHARGE_V, COL_STEP_REF_DISCHARGE_V,nColTempSize[2]);	
			m_wndStepGrid.SetColWidth(COL_STEP_REF_I, COL_STEP_REF_I, nColTempSize[3]);
			m_wndStepGrid.SetColWidth(COL_STEP_RANGE, COL_STEP_RANGE, 80);	//khs RANGE COL 추가
			m_wndStepGrid.SetColWidth(COL_STEP_POWER, COL_STEP_POWER, nColTempSize[4]);				//ljb	POWER COL 추가
			m_wndStepGrid.SetColWidth(COL_STEP_RESISTANCE, COL_STEP_RESISTANCE,  0);	//ljb	20160422 저항 COL 주석처리 (CR모드 없음)
			m_wndStepGrid.SetColWidth(COL_STEP_WAIT_TIME, COL_STEP_WAIT_TIME, 0);	//ljb 20160414 대기시간 추가
		
			nColTempSize[5]=((m_bUseChamberTemp)?90:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CHAMBER_TEMP, COL_STEP_CHAMBER_TEMP, nColTempSize[5]);	//ljb	챔버 설정 온도
		
			nColTempSize[5]=((m_bUseChamberHumi)?90:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CHAMBER_HUMI, COL_STEP_CHAMBER_HUMI, nColTempSize[5]);	//ljb	챔버 설정 습도 온도
		
			nColTempSize[5]=((m_bUseCellBal)?80:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CELLBAL, COL_STEP_CELLBAL, nColTempSize[5]);	//cny 201809
			
			nColTempSize[5]=((m_bUsePwrSupply)?100:0);
			m_wndStepGrid.SetColWidth(COL_STEP_PWRSUPPLY, COL_STEP_PWRSUPPLY, nColTempSize[5]);//cny 201809
			
			nColTempSize[5]=((m_bUseChiller)?80:0);
			m_wndStepGrid.SetColWidth(COL_STEP_CHILLER, COL_STEP_CHILLER, nColTempSize[5]);//cny 201809
			
			nColTempSize[5]=((m_bUsePatTable)?80:0);
			m_wndStepGrid.SetColWidth(COL_STEP_PATTABLE, COL_STEP_PATTABLE, nColTempSize[5]);//cny 201809
		}
		cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, 0, COL_STEP_END-1);
		m_wndStepGrid.SetColWidth(COL_STEP_END, COL_STEP_END, rect.Width() - cellRect.Width()-1);
	#else
		// 컴파일 옵션 _CYCLER_ 없으면 아래 실행
		m_wndStepGrid.SetColWidth(COL_STEP_CYCLE, COL_STEP_CYCLE, 0);
		m_wndStepGrid.SetColWidth(COL_STEP_NO, COL_STEP_NO, 35);
		m_wndStepGrid.SetColWidth(COL_STEP_MODE, COL_STEP_MODE, 65);

		m_wndStepGrid.SetColWidth(COL_STEP_TYPE, COL_STEP_TYPE, 85);
		m_wndStepGrid.SetColWidth(COL_STEP_PROCTYPE, COL_STEP_PROCTYPE, 100);
		m_wndStepGrid.SetColWidth(COL_STEP_REF_CHARGE_V, COL_STEP_REF_I, 60);

		cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, 0, COL_STEP_END-1);
		m_wndStepGrid.SetColWidth(COL_STEP_END, COL_STEP_END, rect.Width() - cellRect.Width()-1);
	#endif	// _CYCLER_

	m_wndStepGrid.SetColWidth(COL_STEP_PREIMARY_KEY, COL_STEP_INDEX, 0);

	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_NO),		CGXStyle().SetValue("No"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_CYCLE),	CGXStyle().SetValue("Cyc"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_PROCTYPE),	CGXStyle().SetValue("Step"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_TYPE),		CGXStyle().SetValue("Type"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_MODE),		CGXStyle().SetValue("Mode"));
	
	strType.Format(Fun_FindMsg("InitStepGrid_msg1","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_CHARGE_V),	strType);
	
	strType.Format(Fun_FindMsg("InitStepGrid_msg2","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_DISCHARGE_V),	strType);

	strType.Format(Fun_FindMsg("InitStepGrid_msg3","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCrtUnit());
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I),	strType);

	strType.Format("Range");
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_RANGE),	strType);
	
	strType.Format(Fun_FindMsg("InitStepGrid_msg4","IDD_CTSEditorPro_CYCL"), GetDocument()->GetWattUnit());				//ljb
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_POWER),	strType);

	strType.Format(Fun_FindMsg("InitStepGrid_msg5","IDD_CTSEditorPro_CYCL"));				//ljb
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_RESISTANCE),	strType);

	//strType.Format("지정시간");				//ljb 20160414 add
	strType.Format("WaitTime");				//언어파일작업필요.
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_WAIT_TIME),	strType);
	
	strType.Format(Fun_FindMsg("InitStepGrid_msg6","IDD_CTSEditorPro_CYCL"));				//ljb
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_CHAMBER_TEMP),	strType);

	strType.Format(Fun_FindMsg("InitStepGrid_msg7","IDD_CTSEditorPro_CYCL"));				//ljb
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_CHAMBER_HUMI),	strType);

	strType.Format("Balancing");			//cny 201809
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_CELLBAL),	strType);
	
	strType.Format("PowerSupply");			//cny 201809
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_PWRSUPPLY), strType);
	
	strType.Format("Chiller");			    //cny 201809
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_CHILLER), strType);
	
	strType.Format("Table");			    //cny 201809
	m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_PATTABLE), strType);
	
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_END),		CGXStyle().SetValue(Fun_FindMsg("InitStepGrid_msg8","IDD_CTSEditorPro_CYCL")));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_PREIMARY_KEY),	CGXStyle().SetValue("Primary Key"));
	m_wndStepGrid.SetStyleRange(CGXRange(0, COL_STEP_INDEX),	CGXStyle().SetValue("Index"));
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PREIMARY_KEY, COL_STEP_INDEX), CGXStyle().SetEnabled(FALSE));
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_END, COL_STEP_INDEX), CGXStyle().SetControl(GX_IDS_CTRL_STATIC));

  	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CYCLE),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_CHARGE_V),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
	);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_DISCHARGE_V),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_I),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
	);
//	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(FALSE));
	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_POWER),		//ljb
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_RESISTANCE),		//ljb
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CHAMBER_TEMP),		//ljb
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CHAMBER_HUMI),		//ljb
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	//cny
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CELLBAL),CGXStyle().SetControl(GX_IDS_CTRL_STATIC));//cny 201809		
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PWRSUPPLY),CGXStyle().SetControl(GX_IDS_CTRL_STATIC));//cny 201809	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CHILLER),CGXStyle().SetControl(GX_IDS_CTRL_STATIC));//cny 201809	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PATTABLE),CGXStyle().SetControl(GX_IDS_CTRL_STATIC));//cny 201809	

	m_wndStepGrid.EnableGridToolTips();
    m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, 
         CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_CHARGE_V, COL_STEP_END), 
		                         CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));

	m_pStepTypeCombo = new CGridComboBox(this, 
								&m_wndStepGrid,
								GX_IDS_CTRL_ZEROBASED_EX1,
								GX_IDS_CTRL_ZEROBASED_EX1,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	
	m_wndStepGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX1, m_pStepTypeCombo);

	m_pStepModeCombo = new CGridComboBox(this, 
								&m_wndStepGrid,
								GX_IDS_CTRL_ZEROBASED_EX2,
								GX_IDS_CTRL_ZEROBASED_EX2,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndStepGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX2, m_pStepModeCombo);

	m_pProcTypeCombo = new CGridComboBox(this, 
								&m_wndStepGrid,
								GX_IDS_CTRL_ZEROBASED_EX3,
								GX_IDS_CTRL_ZEROBASED_EX3,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndStepGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX3, m_pProcTypeCombo);

#ifdef _CYCLER_
	#ifdef _EDLC_CELL_
			//strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n ESR\n Pattern\n Loop\n 완료\n";
			if (m_bUseExternalCAN)
				strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n ESR\n Pattern\n External CAN\n Loop\n END\n";	//ljb 2011318 이재복 //////////
			else
				strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n ESR\n Pattern\n Loop\n END\n";
	#else
		#ifdef _ADD_IMPEDANCE_		//ljb 2011525
			//strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Impedance\n Pattern\n Loop\n 완료\n";
			if (m_bUseExternalCAN)
				strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Impedance\n Pattern\n External CAN\n Loop\n END\n";	//ljb 2011318 이재복 //////////
			else
				strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Impedance\n Pattern\n Loop\n END\n";
		#else
			//strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n Loop\n 완료\n";		
			
			//주석처리 by ksj 20201117
			/*
			if (m_bUseExternalCAN)
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n UserMap \n External CAN\n Loop\n END\n";	
				}else{
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n External CAN\n Loop\n END\n";
				}	
				//strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n External CAN\n Loop\n 완료\n";	//ljb 2011318 이재복 //////////
			else{
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n UserMap \n Loop\n END\n";	
				}else{
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n Loop\n END\n";	
				}				
			}	*/

			//ksj 20201117 : 스텝 타입 추가 로직 개선////////////////////////////////////////////////////////////////
			if (m_bUseExternalCAN)
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n UserMap \n External CAN\n";	
				}else{
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n External CAN\n";
				}	
			else{
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n UserMap \n";	
				}else{
					strType = " Cycle\n Charge\n Discharge\n Rest\n Ocv\n Pattern\n";	
				}				
			}	

			if(m_bUseImpedance) //임피던스 스텝 추가.
			{
				strType += " Impedance\n";
			}

			strType += " Loop\n END\n";
			//ksj end////////////////////////////////////////////////////////////////////////////////////////////////
		#endif
	#endif
#else
	#ifdef _EDLC_CELL_
			strType = " Charge\n Discharge\n Rest\n Ocv\n ESR\n END\n";
	#else
			strType = " Charge\n Discharge\n Rest\n Ocv\n Impedance\n END\n";
	#endif
#endif

#ifdef _CYCLER_
	m_pStepTypeCombo->SetItemData(0, PS_STEP_ADV_CYCLE);
	m_pStepTypeCombo->SetItemData(1, PS_STEP_CHARGE);
	m_pStepTypeCombo->SetItemData(2, PS_STEP_DISCHARGE);
	m_pStepTypeCombo->SetItemData(3, PS_STEP_REST);
	m_pStepTypeCombo->SetItemData(4, PS_STEP_OCV);
	#ifdef _EDLC_CELL_
		m_pStepTypeCombo->SetItemData(5, PS_STEP_IMPEDANCE);
		m_pStepTypeCombo->SetItemData(6, PS_STEP_PATTERN);
		if (m_bUseExternalCAN)
		{	
			m_pStepTypeCombo->SetItemData(7, PS_STEP_EXT_CAN);
			m_pStepTypeCombo->SetItemData(8, PS_STEP_LOOP);
			m_pStepTypeCombo->SetItemData(9, PS_STEP_END);
		}
		else
		{
			m_pStepTypeCombo->SetItemData(7, PS_STEP_LOOP);
			m_pStepTypeCombo->SetItemData(8, PS_STEP_END);
		}
	#else
		#ifdef _ADD_IMPEDANCE_		//ljb 2011525 
			m_pStepTypeCombo->SetItemData(5, PS_STEP_IMPEDANCE);
			m_pStepTypeCombo->SetItemData(6, PS_STEP_PATTERN);
			if (m_bUseExternalCAN)
			{
				m_pStepTypeCombo->SetItemData(7, PS_STEP_EXT_CAN);
				m_pStepTypeCombo->SetItemData(8, PS_STEP_LOOP);
				m_pStepTypeCombo->SetItemData(9, PS_STEP_END);
			}
			else
			{
				m_pStepTypeCombo->SetItemData(7, PS_STEP_LOOP);
				m_pStepTypeCombo->SetItemData(8, PS_STEP_END);
			}
		#else
			/*m_pStepTypeCombo->SetItemData(5, PS_STEP_PATTERN);
			if (m_bUseExternalCAN)
			{
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					m_pStepTypeCombo->SetItemData(6, PS_STEP_USER_MAP);
					m_pStepTypeCombo->SetItemData(7, PS_STEP_EXT_CAN);
					m_pStepTypeCombo->SetItemData(8, PS_STEP_LOOP);
					m_pStepTypeCombo->SetItemData(9, PS_STEP_END);
				}else{
					m_pStepTypeCombo->SetItemData(6, PS_STEP_EXT_CAN);
					m_pStepTypeCombo->SetItemData(7, PS_STEP_LOOP);
					m_pStepTypeCombo->SetItemData(8, PS_STEP_END);
				}
			}
			else
			{
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					m_pStepTypeCombo->SetItemData(6, PS_STEP_USER_MAP);
					m_pStepTypeCombo->SetItemData(7, PS_STEP_LOOP);
					m_pStepTypeCombo->SetItemData(8, PS_STEP_END);
				}else{
					m_pStepTypeCombo->SetItemData(6, PS_STEP_LOOP);
					m_pStepTypeCombo->SetItemData(7, PS_STEP_END);
				}				
			}*/

			//ksj 20201117 : 임피던스 스텝 레지스트리 옵션 추가////////////////////////////////////////////////////
			int nItemIdx = 5;
			m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_PATTERN); //5


			if (m_bUseExternalCAN)
			{
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_USER_MAP); //6
					m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_EXT_CAN); //7
					
				}else{
					m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_EXT_CAN); //6
				}
			}
			else
			{
				//2014.09.02 UserMap추가.
				if(m_bUseUserMap){
					m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_USER_MAP); //6
				}				
			}

			if(m_bUseImpedance) //임피던스 스텝 추가.
			{
				m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_IMPEDANCE); //7 or 8
			}

			m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_LOOP); //8 or 9
			m_pStepTypeCombo->SetItemData(nItemIdx++, PS_STEP_END); //9 or 10
			//ksj end/////////////////////////////////////////////////////////////////////////////////////////////


		#endif
	#endif
#else
	m_pStepTypeCombo->SetItemData(0, PS_STEP_CHARGE);
	m_pStepTypeCombo->SetItemData(1, PS_STEP_DISCHARGE);
	m_pStepTypeCombo->SetItemData(2, PS_STEP_REST);
	m_pStepTypeCombo->SetItemData(3, PS_STEP_OCV);
	#ifdef _ADD_IMPEDANCE_		//ljb 2011525 
		m_pStepTypeCombo->SetItemData(4, PS_STEP_IMPEDANCE);
		m_pStepTypeCombo->SetItemData(5, PS_STEP_END);	
	#else
		m_pStepTypeCombo->SetItemData(4, PS_STEP_END);	
#endif
#endif
	



	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_TYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX1)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(strType)
	);

	//khs 20081113 Range Combobox 추가
	//khs 20081118 Spec에 따른 Range값 설정
	CString strRange;
	if(m_nRangeVal == 2)		strRange.Format("Auto\nHigh\nLow");
	else if(m_nRangeVal == 3)	strRange.Format("Auto\nHigh\nMiddle\nLow");
	else if(m_nRangeVal == 4)	strRange.Format("Auto\nHigh\nMiddle\nLow\nMicro");
	else						strRange.Format("Auto");

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_RANGE),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX)
		.SetHorizontalAlignment(DT_LEFT)
		.SetChoiceList(strRange)
	);


	CString strMode(" CC-CV\n CC\n");
	CString strProcType;
	SCH_CODE_MSG *pMsg;

	CCTSEditorProDoc *pDoc = GetDocument();
	for(int i = 0; i < GetDocument()->m_apProcType.GetSize(); i++)
	{
		pMsg = (SCH_CODE_MSG *)pDoc->m_apProcType[i];
		strMode.Format("%s\n", pMsg->szMessage);
		strProcType += strMode;
		m_pProcTypeCombo->SetItemData(i, pMsg->nCode);
	}
	
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PROCTYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX3)
			.SetHorizontalAlignment(DT_LEFT)
			.SetChoiceList(strProcType)
	);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_MODE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX2)
			.SetHorizontalAlignment(DT_LEFT)
	);

	
	m_wndStepGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndStepGrid, IDS_CTRL_REALEDIT));
	m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndStepGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);

	char str[12], str2[7], str3[5];
	//reference current
	sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
	sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
	if(pDoc->GetVtgUnit() == "V")
	{
		sprintf(str2, "%d", 6);		//정수부
	}
	else
	{
		sprintf(str2, "%d", 6);		//정수부
	}

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_CHARGE_V),
		CGXStyle()
			.SetHorizontalAlignment(DT_RIGHT)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_DISCHARGE_V),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);

	sprintf(str3, "%d", GetDocument()->GetCrtUnitFloat());		//소숫점 이하
	sprintf(str, "%%.%dlf", GetDocument()->GetCrtUnitFloat());
	if(pDoc->GetCrtUnit() == "A")
	{
		sprintf(str2, "%d", 6);
	}
	else
	{
		sprintf(str2, "%d", 6);
	}

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_REF_I),
		CGXStyle()
			.SetHorizontalAlignment(DT_RIGHT)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_POWER),		//ljb
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_RESISTANCE),		//ljb
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);


	sprintf(str3, "%d", 1);		//소숫점 이하
	sprintf(str, "%%.%dlf", 1);
	sprintf(str2, "%d", 3);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CHAMBER_TEMP),		//ljb
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_CHAMBER_HUMI),		//ljb
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);

	//현재 선택된 Step을 색상으로 표시한다.
	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(1, m_wndStepGrid.GetColCount()), 
		                        CGXStyle().SetTextColor(RGB(165,165,165)));
	m_wndStepGrid.SetScrollBarMode(SB_BOTH, TRUE, TRUE);// Grid ScrollBar delete
	m_wndStepGrid.GetParam()->EnableUndo(TRUE);

	return TRUE;
}

//Init Grade Grid
BOOL CCTSEditorProView::InitGradeGrid()
{
	CRect rect;
	m_wndGradeGrid.SubclassDlgItem(IDC_GRADE_GRID, this);
	m_wndGradeGrid.Initialize();

	// Sample setup for the grid
	m_wndGradeGrid.GetParam()->EnableUndo(FALSE);
	m_wndGradeGrid.SetRowCount(SCH_MAX_GRADE_STEP);
	m_wndGradeGrid.SetColCount(_COL_GRADE_CODE_);
	m_wndGradeGrid.GetClientRect(&rect);
	m_wndGradeGrid.SetColWidth(0, 0, 30);
	m_wndGradeGrid.SetColWidth(_COL_ITEM1_, _COL_ITEM1_, 65);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MIN1_, _COL_GRADE_MIN1_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MAX1_, _COL_GRADE_MAX1_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_RELATION_, _COL_GRADE_RELATION_, 60);
	m_wndGradeGrid.SetColWidth(_COL_ITEM2_, _COL_ITEM2_, 65);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MIN2_, _COL_GRADE_MIN2_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_MAX2_, _COL_GRADE_MAX2_, 50);
	m_wndGradeGrid.SetColWidth(_COL_GRADE_CODE_, _COL_GRADE_CODE_, 50);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 0, 1, 0);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 1, 0, 2);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 3, 0, 4);
// 	m_wndGradeGrid.SetCoveredCellsRowCol(0, 5, 1, 5);

	m_wndGradeGrid.SetStyleRange(CGXRange(0,0),	CGXStyle().SetValue("No"));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,1),	CGXStyle().SetValue("ITEM1").SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));

//	m_wndGradeGrid.SetStyleRange(CGXRange(0,1),	CGXStyle().SetValue("용량"));
// #ifdef _EDLC_CELL_
// 	m_wndGradeGrid.SetStyleRange(CGXRange(0,3),	CGXStyle().SetValue("ESR"));
// #else
// 	m_wndGradeGrid.SetStyleRange(CGXRange(0,3),	CGXStyle().SetValue("임피던스"));
// #endif
	m_wndGradeGrid.SetStyleRange(CGXRange(0,2),	CGXStyle().SetValue(Fun_FindMsg("InitGradeGrid_msg1","IDD_CTSEditorPro_CYCL")).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,3),	CGXStyle().SetValue(Fun_FindMsg("InitGradeGrid_msg2","IDD_CTSEditorPro_CYCL")).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,4),	CGXStyle().SetValue(Fun_FindMsg("InitGradeGrid_msg3","IDD_CTSEditorPro_CYCL")).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,5),	CGXStyle().SetValue(Fun_FindMsg("InitGradeGrid_msg4","IDD_CTSEditorPro_CYCL")).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,6),	CGXStyle().SetValue(Fun_FindMsg("InitGradeGrid_msg5","IDD_CTSEditorPro_CYCL")).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,7),	CGXStyle().SetValue(Fun_FindMsg("InitGradeGrid_msg6","IDD_CTSEditorPro_CYCL")).SetHorizontalAlignment(DT_CENTER).SetBaseStyle(1));
	m_wndGradeGrid.SetStyleRange(CGXRange(0,8),	CGXStyle().SetValue("Code"));
	m_wndGradeGrid.GetParam()->EnableUndo(TRUE);

//	m_wndGradeGrid.SetFrozenRows(1);

	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_RELATION_),
		CGXStyle().SetHorizontalAlignment(DT_CENTER)
		);
	
	m_pGradeItem1Combo = new CGridComboBox(this, 
								&m_wndGradeGrid,
								GX_IDS_CTRL_ZEROBASED_EX6,
								GX_IDS_CTRL_ZEROBASED_EX6,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndGradeGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX6, m_pGradeItem1Combo);
	
	m_pGradeItem2Combo = new CGridComboBox(this, 
								&m_wndGradeGrid,
								GX_IDS_CTRL_ZEROBASED_EX7,
								GX_IDS_CTRL_ZEROBASED_EX7,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndGradeGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX7, m_pGradeItem2Combo);


	m_pGradeRelationTypeCombo = new CGridComboBox(this, 
								&m_wndGradeGrid,
								GX_IDS_CTRL_ZEROBASED_EX8,
								GX_IDS_CTRL_ZEROBASED_EX8,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndGradeGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX8, m_pGradeRelationTypeCombo);

// 	m_pStepTypeCombo->SetItemData(0, PS_STEP_CHARGE);
// 	m_pStepTypeCombo->SetItemData(1, PS_STEP_DISCHARGE);
// 	m_pStepTypeCombo->SetItemData(0, PS_STEP_CHARGE);
// 	m_pStepTypeCombo->SetItemData(1, PS_STEP_DISCHARGE);
	CString strMode, strGradeType;
	SCH_CODE_MSG *pMsg;
	
	CCTSEditorProDoc *pDoc = GetDocument();
	for(int i = 0; i < GetDocument()->m_apGradeType.GetSize(); i++)
	{
		pMsg = (SCH_CODE_MSG *)pDoc->m_apGradeType[i];
		strMode.Format("%s\n", pMsg->szMessage);
		strGradeType += strMode;
		m_pGradeItem1Combo->SetItemData(i, pMsg->nCode);
		m_pGradeItem2Combo->SetItemData(i, pMsg->nCode);
	}
	
	
//	CString strType = " None\n 전압\n 용량\n 임피던스\n 전류\n 시간\n";
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_ITEM1_),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX6)
		.SetHorizontalAlignment(DT_CENTER)
		.SetChoiceList(strGradeType)
		);
	
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_ITEM2_),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX7)
		.SetHorizontalAlignment(DT_CENTER)
		.SetChoiceList(strGradeType)
		);

	strMode = "None\n AND\n OR\n";
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_RELATION_),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX8)
		.SetHorizontalAlignment(DT_CENTER)
		.SetChoiceList(strMode)
		.SetValue(0L)
		);
	//임시 색상변경
// 	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_RELATION_),
// 		CGXStyle().SetInterior( CGXBrush().SetColor(RGB(255, 153, 0))));			

	m_wndGradeGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndGradeGrid, IDS_CTRL_REALEDIT));
	m_wndGradeGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndGradeGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndGradeGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);

	char str[12], str2[7], str3[5];
	
	//Grade Value 
	sprintf(str, "%%.%dlf", 1);	//소숫점 이하
	sprintf(str2, "%d", 6);		//정수부
	sprintf(str3, "%d", 1);		//소숫점 이하

	m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(1, _COL_GRADE_MIN1_,
		m_wndGradeGrid.GetRowCount(), _COL_GRADE_MAX1_),
		CGXStyle()
			.SetHorizontalAlignment(DT_RIGHT)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
	);
	m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(1, _COL_GRADE_MIN2_,
		m_wndGradeGrid.GetRowCount(), _COL_GRADE_MAX2_),
		CGXStyle()
		.SetHorizontalAlignment(DT_RIGHT)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);

	//Mask control에 한글입력시 오류가 있음
	//2006/4/27
 /*	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(3),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(GX_IDS_CTRL_MASKEDIT)
			.SetUserAttribute(GX_IDS_UA_INPUTMASK, _T("A"))
			.SetUserAttribute(GX_IDS_UA_INPUTPROMPT, _T("_"))
	);
*/

	m_wndGradeGrid.SetStyleRange(CGXRange().SetCols(_COL_GRADE_CODE_),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(GX_IDS_CTRL_EDIT)
			.SetMaxLength(1)
	);
	
	return TRUE;
}

void CCTSEditorProView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	if(m_nTimer != 0)
	{
		KillTimer(m_nTimer);
		m_nTimer =0;
	}
	// TODO: Add your message handler code here
}


LRESULT CCTSEditorProView::OnGridComboDropDown(WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT CCTSEditorProView::OnGridComboSelected(WPARAM wParam, LPARAM lParam)
{
	CGridComboBox *pCombo = (CGridComboBox*) lParam;
	int selID = (int) wParam;
	ROWCOL	nRow, nCol;

	int nTotRowCount;
	DWORD nComboData = 0;
	CCTSEditorProDoc *pDoc = GetDocument();
	CString strTemp;
	
	int nID = -1;
	for(int a=0; a<m_wndTestListGrid.GetRowCount(); a++)
	{
		if(m_lLoadedTestID == atoi(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PRIMARY_KEY)))
		{
			nID = atol(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PROC_TYPE));
			break;
		}
	}
	
	// 20081210 KHS 추가 Step이 Charge or DisCharge 이면 Grade grid에서 Impedance 선택 못함
	if(pCombo == m_pGradeItem1Combo || pCombo == m_pGradeItem2Combo)		
	{
		nTotRowCount = m_wndStepGrid.GetRowCount();
		if(!m_wndStepGrid.GetCurrentCell(&nRow, &nCol))		return 0;
		STEP *pStep = (STEP *)pDoc->GetStepData(nRow);	
		
		switch((int)pStep->chType)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			nComboData = pCombo->GetItemData(selID);
			//AfxMessageBox("ff");
		}
	}

		
	//Step Type or Step Procedure Type Select
	if(pCombo == m_pStepTypeCombo || pCombo == m_pProcTypeCombo)		
	{
		nTotRowCount = m_wndStepGrid.GetRowCount();
		if(!m_wndStepGrid.GetCurrentCell(&nRow, &nCol))		return 0;
		STEP *pStep = (STEP *)pDoc->GetStepData(nRow);	
		
		int nSelType;
		
		//선택한 Step Type을 구함
		nComboData = pCombo->GetItemData(selID);
		if(pCombo == m_pProcTypeCombo)
		{
			 nSelType= nComboData>>16;
			 if(pStep->nProcType == nComboData)		return 0;		//not changed
		}
		else
		{
			nSelType = nComboData;		
			if(pStep->chType == nComboData)			return 0;		//Not changed
		}

		TRACE("Step Type %d Selected\n", nSelType);

		if(nSelType < 0)	return 0;
		
		//선택한 Step Type이 End가 아니면 새로운 Step 자동 생성
		if(nSelType != PS_STEP_END && nTotRowCount == nRow)
		{
			AddStepNew(nRow); 
		}
		//선택한 Step Type이 End이면 밑에 있는 Step을 모두 지움
		if(nSelType == PS_STEP_END && nTotRowCount > nRow)
		{
//			m_wndStepGrid.SelectRange(CGXRange(nRow+1, 0, nTotRowCount, m_wndStepGrid.GetColCount()));
			CDWordArray awStep;
			for(DWORD i = nRow+1; i <= nTotRowCount; i++)
			{
				//m_wndStepGrid.SetCurrentCell(i, COL_STEP_TYPE);	//Move Current select step
				//OnStepDelete();
				awStep.Add(i);
			}
			if(MessageBox(Fun_FindMsg("OnGridComboSelected_msg1","IDD_CTSEditorPro_CYCL"), Fun_FindMsg("OnGridComboSelected_msg2","IDD_CTSEditorPro_CYCL"), MB_ICONQUESTION|MB_YESNO) == IDYES)
			{
				DeleteStep(awStep);
			}
		}

		//Step selection changed to different type
		//Step이 추가 되거나 변경 되었으므로 다시 Step을 구한다.
		pStep = (STEP *)pDoc->GetStepData(nRow);	
		ZeroMemory(pStep, sizeof(STEP));	//Clear Inner Varible Data
	
		pStep->chType = nSelType;		//Update Current Step Type		
		if(pCombo == m_pProcTypeCombo)	//ProcStep이면 Procedure Type을 Update 한다.
		{
			pStep->nProcType =	nComboData;		
		}
		
		int nSelOption = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", 1);	//ljb 20170720 add
		pStep->nCanCheckMode = nSelOption;	//ljb 20170720 통신 모드 default

		if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)			
		{
			//최초 기본 저장 시간을 1분으로 한다.
			if(pStep->fIref == 0.0f)	//최초 
			{
				//pStep->ulReportTime = 60.0f*100.0f;		//60sec
				pStep->ulReportTime = 1.0f*100.0f;		//1sec
			}

			//Aging일 경우 방전 전류 고정
			if(pDoc->m_lLoadedProcType == PS_PGS_AGING)
			{
				pStep->fIref = pDoc->m_lMaxCurrent1;
			}

			if(pStep->chType == PS_STEP_CHARGE) 
			{
#ifdef _NIMH_CELL_
				//NIMH 전지는 CC로만 충전하므로 
				pStep->chMode = PS_MODE_CC;
#else
				pStep->chMode = PS_MODE_CCCV;	// 충전일 경우 기본 CCCV	
#endif
				pStep->fVref_Charge =  (float)(GetDocument()->m_lVRefHigh);			
			}

			if(pStep->chType == PS_STEP_DISCHARGE)
			{
				pStep->chMode = PS_MODE_CC;		//방전일 경우 기본 CC
				pStep->fVref_DisCharge =  (float)(GetDocument()->m_lVRefLow);			
			}

			if(m_bVHigh)	pStep->fVLimitHigh = m_fVHighVal;	//m_fVHighVal*V_UNIT_FACTOR);
			if(m_bVLow)		pStep->fVLimitLow = m_fVLowVal;	//m_fVLowVal*V_UNIT_FACTOR);
			if(m_bIHigh)	pStep->fILimitHigh = m_fIHighVal;	
			if(m_bILow)		pStep->fILimitLow = m_fILowVal;	
			if(m_bCHigh)	pStep->fCLimitHigh = m_fCHighVal;	
			if(m_bCLow)		pStep->fCLimitLow = m_fCLowVal;	
			if(m_bCapacitanceHigh)	pStep->fCapacitanceHigh = m_fCapacitanceHigh;
			if(m_bCapacitanceLow)	pStep->fCapacitanceLow = m_fCapacitanceLow;
			//if(m_bCellDeltaVstep)	pStep->fSafetyCellDeltaVStep = m_bCellDeltaVstep; //yulee 20190531_5
		}	
		else if(pStep->chType == PS_STEP_IMPEDANCE)	// Impedance 일 경우 기본 DC_Imp
		{
			//pStep->ulReportTime = 1.1f*100.0f;		//1.1sec
			pStep->ulReportTime = 1.0f*100.0f;		//1sec

			pStep->chMode = PS_MODE_DCIMP;
			if(m_bVHigh)	pStep->fVLimitHigh = m_fVHighVal;	
			if(m_bVLow)		pStep->fVLimitLow = m_fVLowVal;	//m_fVLowVal*V_UNIT_FACTOR);
			if(m_bIHigh)	pStep->fILimitHigh = m_fIHighVal;	//m_fIHighVal*I_UNIT_FACTOR);
			if(m_bILow)		pStep->fILimitLow = m_fILowVal;	//m_fILowVal*I_UNIT_FACTOR);
			//if(m_bCellDeltaVstep)	pStep->fSafetyCellDeltaVStep = m_bCellDeltaVstep; //yulee 20190531_5
		}
		else if(pStep->chType == PS_STEP_LOOP)
		{
			pStep->nLoopInfoCycle = 1;
			//현재 완료(End) Step이나 다음 cycle의 처음을 지칭한다.
			pStep->nLoopInfoGotoStep  = PS_GOTO_NEXT_CYC;//nRow+1;	// 0 is Next Cycle
		}
		else if(pStep->chType == PS_STEP_REST)
		{
			//pStep->ulReportTime = 300.0f*100.0f;		//5분 
			pStep->ulReportTime = 1.0f*100.0f;		//1sec
			if(m_bVHigh)	pStep->fVLimitHigh = m_fVHighVal;	
			if(m_bVLow)		pStep->fVLimitLow = m_fVLowVal;	//m_fVLowVal*V_UNIT_FACTOR);
		}
		//2014.09.02 UserMap 조건 추가.
		else if(pStep->chType == PS_STEP_PATTERN || pStep->chType == PS_STEP_USER_MAP)		
		{
			pStep->ulReportTime = 1.0f*100.0f;		//1sec
			pStep->lValueLimitHigh = 0;
			pStep->lValueLimitLow = 0;
			//if(m_bCellDeltaVstep)	pStep->fSafetyCellDeltaVStep = m_bCellDeltaVstep; //yulee 20190531_5

// 			strTemp.Format("충전(%s)", GetDocument()->GetVtgUnit());
// 			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_POWER), strTemp);
// 			strTemp.Format("방전(%s)", GetDocument()->GetVtgUnit());
// 			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_RESISTANCE), strTemp);
		}
		//ljb 2011318 이재복 ////////// s
		else if(pStep->chType == PS_STEP_EXT_CAN)
		{
			pStep->ulReportTime = 1.0f*100.0f;		//1sec
			pStep->lValueLimitHigh = 0;
			pStep->lValueLimitLow = 0;
			//if(m_bCellDeltaVstep)	pStep->fSafetyCellDeltaVStep = m_bCellDeltaVstep; //yulee 20190531_5
		}

		//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
		UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
		if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
		{
			if (pStep->chType == PS_STEP_LOOP || pStep->chType == PS_STEP_ADV_CYCLE || pStep->chType == PS_STEP_END)
			{
				m_bChkStepDeltaVentAuxV.SetCheck(FALSE);
				m_bChkStepDeltaVentAuxTemp.SetCheck(FALSE);
				m_bChkStepDeltaVentAuxTh.SetCheck(FALSE);
				m_bChkStepDeltaVentAuxT.SetCheck(FALSE);
				m_bChkStepVentAuxV.SetCheck(FALSE);

				m_bStepDeltaVentAuxTemp = FALSE;		
				m_bStepDeltaVentAuxTh = FALSE;	
				m_bStepDeltaVentAuxT = FALSE;
				m_bStepDeltaVentAuxV = FALSE;
				m_bStepVentAuxV = FALSE;

				pStep->bStepDeltaVentAuxV = m_bStepDeltaVentAuxTemp;
				pStep->bStepDeltaVentAuxTemp = m_bStepDeltaVentAuxTh;
				pStep->bStepDeltaVentAuxTh = m_bStepDeltaVentAuxT;
				pStep->bStepDeltaVentAuxT = m_bStepDeltaVentAuxV;
				pStep->bStepVentAuxV = m_bStepVentAuxV;
			}
			else
			{
				m_fCellDeltaVstep = 300;
				m_fStepDeltaAuxTemp = 5000;
				m_fStepDeltaAuxTH = 10000;
				m_fStepDeltaAuxT = 0;
				m_fStepDeltaAuxVTime = 6000;
				m_fStepAuxV = 100;

				pStep->bStepDeltaVentAuxV = 1;
				pStep->bStepDeltaVentAuxTemp = 1;
				pStep->bStepDeltaVentAuxTh = 1;
				pStep->bStepDeltaVentAuxT = 1;
				pStep->bStepVentAuxV = 1;

				pStep->fCellDeltaVStep = m_fCellDeltaVstep;
				pStep->fStepDeltaAuxTemp = m_fStepDeltaAuxTemp;
				pStep->fStepDeltaAuxTh = m_fStepDeltaAuxTH;
				pStep->fStepDeltaAuxT = m_fStepDeltaAuxT;
				pStep->fStepDeltaAuxVTime = m_fStepDeltaAuxVTime;
				pStep->fStepAuxV = m_fStepAuxV;

				m_bStepDeltaVentAuxTemp = TRUE;		
				m_bStepDeltaVentAuxTh = TRUE;	
				m_bStepDeltaVentAuxT = TRUE;
				m_bStepDeltaVentAuxV = TRUE;
				m_bStepVentAuxV = TRUE;

				pStep->bStepDeltaVentAuxV = m_bStepDeltaVentAuxTemp;
				pStep->bStepDeltaVentAuxTemp = m_bStepDeltaVentAuxTh;
				pStep->bStepDeltaVentAuxTh = m_bStepDeltaVentAuxT;
				pStep->bStepDeltaVentAuxT = m_bStepDeltaVentAuxV;
				pStep->bStepVentAuxV = m_bStepVentAuxV;
			}
		}

//		SettingStepWndType(nRow, pStep->chType);		//Clear Display Value
		DisplayStepGrid	(nRow);
		m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_CHARGE_V);
		
		//2006/9/20
		//적용가능한 Step인지 검사 	
		int nID = -1;
		for(int a=0; a<m_wndTestListGrid.GetRowCount(); a++)
		{
			if(m_lLoadedTestID == atoi(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PRIMARY_KEY)))
			{
				nID = atol(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PROC_TYPE));
				if(IsExecutableStep(nID, pStep->chType, pStep->chMode) == FALSE)
				{
					strTemp.Format(Fun_FindMsg("OnGridComboSelected_msg3","IDD_CTSEditorPro_CYCL"), m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_NAME));
					MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
					return 0;
				}
				break;
			}
		}

		//ljb v1009	ACC GROUP STEP 체크 및 선택 가능 스텝 표시
		Fun_UpdateMultiGroupList();
		Fun_UpdateDisplayMultiGroup();
		Fun_UpdateAccGroupList();
		Fun_UpdateDisplayAccGroup();
		//////////////////////////////////////////////////////////////////////////

	}
	else if(pCombo == m_pStepModeCombo)		//Mode Combo Selected
	{
		nTotRowCount = m_wndStepGrid.GetRowCount();
		if(!m_wndStepGrid.GetCurrentCell(&nRow, &nCol))		return 0;

		STEP *pStep = (STEP *)pDoc->GetStepData(nRow);		//Clear Inner Varible Data
		if(pStep->chMode == m_pStepModeCombo->GetItemData(selID))		return 0;	//not changed
		
		pStep->chMode = m_pStepModeCombo->GetItemData(selID);

		if(pStep->chType == PS_STEP_IMPEDANCE)
		{
			if(pStep->chMode == PS_MODE_ACIMP)			//AC Imp Mode 선택시 관련 변수 초기화
			{
				pStep->ulEndTime = 0;
				pStep->ulEndTimeDay = 0;
				pStep->fVref_Charge = 0.0f;
				pStep->fVref_DisCharge = 0.0f;
				pStep->fIref = 0.0f;
				pStep->fPref = 0.0f;
				pStep->fRref = 0.0f;
			}
		}
		else if(pStep->chType == PS_STEP_DISCHARGE || pStep->chType == PS_STEP_CHARGE)
		{
//			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), "전류("+GetDocument()->GetCrtUnit()+")");
//			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), "전류("+GetDocument()->GetCrtUnit()+")");	//ljb
			//if (pStep->chType == PS_STEP_DISCHARGE) pStep->fVref_DisCharge = m_lVRefLow;
			if(pStep->chMode == PS_MODE_CC)				//CC 모든 선택시 END I 초기화   //ljb cell change 시 초기화
			{
				pStep->fVref_DisCharge = 0.0f;
				pStep->fEndI = 0.0f;
				pStep->ulCVTimeDay = 0;
				pStep->ulCVTime = 0;
				pStep->fPref = 0.0f;
				pStep->fRref = 0.0f;

				if(pStep->chType == PS_STEP_CHARGE)	
				{
					pStep->fVref_Charge = (float)pDoc->m_lMaxVoltage;
				}
				else if(pStep->chType == PS_STEP_DISCHARGE)
				{
					pStep->fVref_DisCharge = (float)pDoc->m_lVRefLow;
				}

				//m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_I);
				if(pStep->chType == PS_STEP_CHARGE)
					m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_CHARGE_V);
				else if(pStep->chType == PS_STEP_DISCHARGE)
					m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_DISCHARGE_V);
			}
			else if(pStep->chMode == PS_MODE_CCCV)		//CC/CV 변경 하면 End V값을 초기화 
			{
				pStep->fEndV_H = 0.0f;
				pStep->fEndV_L = 0.0f;		//ljb v1009
				if(pStep->chType == PS_STEP_CHARGE)
					m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_CHARGE_V);
				else if(pStep->chType == PS_STEP_DISCHARGE)
					m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_DISCHARGE_V);
				//m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_CHARGE_V);
			}
			else if(pStep->chMode == PS_MODE_CP)	//ljb 그리드 추가 할때 수정
			{
				pStep->fVref_Charge = 0.0f;
				pStep->fVref_DisCharge = 0.0f;
				pStep->fIref = 0.0f;
				pStep->fPref = 0.0f;
				pStep->fRref = 0.0f;
				pStep->ulCVTimeDay = 0;
				pStep->ulCVTime = 0;
				if(pStep->chType == PS_STEP_CHARGE)
				{
					pStep->fVref_Charge = (float)pDoc->m_lMaxVoltage;
				}
				else if(pStep->chType == PS_STEP_DISCHARGE)
				{
					//pStep->fVref_DisCharge = (float)pDoc->m_lMaxVoltage;
					pStep->fVref_DisCharge = 0.0f;
				}

				//AfxMessageBox("cp");
//				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_I);
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_POWER);		//ljb
				//전류 Ref 입력 Column에서 Ref Power를 받아 들인다.
//				strTemp.Format("파워(%s)", GetDocument()->GetWattUnit());
//				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
			}
			else if(pStep->chMode == PS_MODE_CCCP)	//ljb 그리드 추가 할때 수정
			{
				pStep->fVref_Charge = 0.0f;
				pStep->fVref_DisCharge = 0.0f;
				pStep->fIref = 0.0f;
				pStep->fPref = 0.0f;
				pStep->fRref = 0.0f;
				pStep->ulCVTimeDay = 0;
				pStep->ulCVTime = 0;
				if(pStep->chType == PS_STEP_CHARGE)
				{
					pStep->fVref_Charge = (float)pDoc->m_lMaxVoltage;
				}
				else if(pStep->chType == PS_STEP_DISCHARGE)
				{
					//pStep->fVref_DisCharge = (float)pDoc->m_lMaxVoltage;
					pStep->fVref_DisCharge = 0.0f;
				}
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_POWER);		//ljb
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				pStep->fVref_Charge = 0.0f;
				pStep->fVref_DisCharge = 0.0f;
				pStep->fIref = 0.0f;
				pStep->fPref = 0.0f;
				pStep->fRref = 0.0f;
				pStep->ulCVTimeDay = 0;
				pStep->ulCVTime = 0;
				if(pStep->chType == PS_STEP_CHARGE)
				{
					pStep->fVref_Charge = (float)pDoc->m_lMaxVoltage;
				}
				else if(pStep->chType == PS_STEP_DISCHARGE)
				{
					pStep->fVref_DisCharge = (float)pDoc->m_lMaxVoltage;
				}
				m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_RESISTANCE);
				//전류 Ref 입력 Column에서 Ref Resistor를  받아 들인다.
				
//				strTemp = "저항(Ω)";
//				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				pStep->fEndV_H = 0.0f;
				pStep->fEndV_L = 0.0f;		//ljb v1009
				pStep->ulCVTimeDay = 0;
				pStep->ulCVTime = 0;
				//pStep->fIref = (float)pDoc->m_lMaxCurrent;
				if(pStep->chType == PS_STEP_CHARGE)
					m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_CHARGE_V);
				else if(pStep->chType == PS_STEP_CHARGE)
					m_wndStepGrid.SetCurrentCell(nRow, COL_STEP_REF_DISCHARGE_V);
			}
		}
		DisplayStepGrid	(nRow);

		//2006/9/20
		//적용가능한 Step인지 검사 	
		int nID = -1;
		for(int a=0; a<m_wndTestListGrid.GetRowCount(); a++)
		{
			if(m_lLoadedTestID == atoi(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PRIMARY_KEY)))
			{
				nID = atol(m_wndTestListGrid.GetValueRowCol(a+1,COL_TEST_PROC_TYPE));
				if(IsExecutableStep(nID, pStep->chType, pStep->chMode) == FALSE)
				{
					strTemp.Format(Fun_FindMsg("OnGridComboSelected_msg4","IDD_CTSEditorPro_CYCL"), m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_NAME));
					MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
					return 0;
				}
				break;
			}
		}

	}
	else if(pCombo == m_pTestTypeCombo)
	{
		nTotRowCount = m_wndTestListGrid.GetRowCount();
		if(!m_wndTestListGrid.GetCurrentCell(&nRow, &nCol))		return 0;
		
		//Test명을 선택된 공정명 Type과 동일 하게 한다.(공정명을 사용자 임의 입력이 아니라 주어진 범위에서 선택하도록)
		CString strName;
		m_pTestTypeCombo->GetWindowText(strName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), strName);
		
		//Step을 검사하여 시행 불가능한 Step이 포함되어 있으면 경고 표시
		if(CheckExecutableStep(selID, atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY)))	 == FALSE)
		{
			strTemp.Format(Fun_FindMsg("OnGridComboSelected_msg5","IDD_CTSEditorPro_CYCL"),
				strName);
			AfxMessageBox(strTemp);
		}
	}
	return 1;
}

//Row Drag
LRESULT CCTSEditorProView::OnSelDragRowsDrop(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	if(pGrid == &m_wndTestListGrid)
	{
		UpdateTestNo();
	}
	else if(pGrid == m_pWndCurModelGrid)
	{
		UpdateModelNo();
	}
	else if(pGrid == &m_wndStepGrid)
	{
		GX_DROP_TARGET *pTarget = (GX_DROP_TARGET *)wParam;


	}
	return TRUE;
}

//Show Context Menu
LRESULT CCTSEditorProView::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = wParam & 0x0000ffff;
	nRow = wParam >> 16;
	
	ASSERT(pGrid);
	if(nCol<0 || nRow < 1 || &m_wndStepGrid != pGrid)	return 0;
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		GetClientRect(rect);
		ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}

/*	if(pGrid == (CMyGridWnd *)&m_wndModuleGrid)
	{
		m_nCmdTarget = BOARD_CMD;
	}
	else if(pGrid == (CMyGridWnd *)&m_wndTopGrid)
	{
		m_nCmdTarget = CHANNEL_CMD;
	}
*/

	//yulee 20190306 //yulee 20190625
	int nSelLanguage, nIDSel;
	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	switch(nSelLanguage)
	{
	case 1:
		{
			nIDSel = IDR_MAINFRAME;
			break;
		}
	case 2:
		{
			nIDSel = IDR_MAINFRAME_ENG;
			break;
		}
	case 3:
		{
			nIDSel = IDR_MAINFRAME_PL;
			break;
		}
	}


	CMenu menu;
	VERIFY(menu.LoadMenu(nIDSel));//yulee 20190625

	CMenu* pPopup = menu.GetSubMenu(2);
	ASSERT(pPopup != NULL);
	CWnd* pWndPopupOwner = this;

	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);

	return TRUE;
}

//Initialize Step Data Edit Ctrl
BOOL CCTSEditorProView::InitEditCtrl()
{
	COleDateTime ole;
	SECCurrencyEdit::Format fmt(FALSE);
	fmt.SetMonetarySymbol(0);
	fmt.SetPositiveFormat(2);
	fmt.SetNegativeFormat(2);
	fmt.EnableLeadingZero(FALSE);
	fmt.SetFractionalDigits(1);
	fmt.SetThousandSeparator(0);
	fmt.SetDecimalDigits(0);
	
	int a = 3, b =3;
	a = GetDocument()->GetVtgUnitFloat();
	if(GetDocument()->GetVtgUnit() == "V")
	{
		b = 4; //2014.09.30 4자리로 변경.
	}
	else
	{
		b = 6;
	}

	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
	//안전조건 전압 상한값 
	VERIFY(m_SafetyMaxV.Initialize(this, IDC_SAFETY_MAX_V));
	m_SafetyMaxV.SetFormat(fmt); m_SafetyMaxV.SetValue(0.0);	
	//안전조건 전압 하한값 
	VERIFY(m_SafetyMinV.Initialize(this, IDC_SAFETY_MIN_V));
	m_SafetyMinV.SetFormat(fmt); m_SafetyMinV.SetValue(0.0);
	
	//end voltage비교
	VERIFY(m_DeltaV.Initialize(this, IDC_DELTA_V));				m_DeltaV.SetFormat(fmt);		m_DeltaV.SetValue(0.0);

	//전압 상승 비교 하한값
	VERIFY(m_CompV[0].Initialize(this, IDC_COMP_V1));			m_CompV[0].SetFormat(fmt);		m_CompV[0].SetValue(0.0);
	VERIFY(m_CompV[1].Initialize(this, IDC_COMP_V2));			m_CompV[1].SetFormat(fmt);		m_CompV[1].SetValue(0.0);
	VERIFY(m_CompV[2].Initialize(this, IDC_COMP_V3));			m_CompV[2].SetFormat(fmt);		m_CompV[2].SetValue(0.0);

	//전압 상승 비교 상한값
	VERIFY(m_CompV1[0].Initialize(this, IDC_COMP_V4));			m_CompV1[0].SetFormat(fmt);		m_CompV1[0].SetValue(0.0);
	VERIFY(m_CompV1[1].Initialize(this, IDC_COMP_V5));			m_CompV1[1].SetFormat(fmt);		m_CompV1[1].SetValue(0.0);
	VERIFY(m_CompV1[2].Initialize(this, IDC_COMP_V6));			m_CompV1[2].SetFormat(fmt);		m_CompV1[2].SetValue(0.0);

	//Report 조건 전압
	VERIFY(m_ReportV.Initialize(this, IDC_REPORT_VOLTAGE));		m_ReportV.SetFormat(fmt);		m_ReportV.SetValue(0.0);

	//EDLC Capa 측정
	VERIFY(m_CapVtgLow.Initialize(this, IDC_CAP_VTG_LOW));		m_CapVtgLow.SetFormat(fmt);		m_CapVtgLow.SetValue(0.0);
	VERIFY(m_CapVtgHigh.Initialize(this, IDC_CAP_VTG_HIGH));	m_CapVtgHigh.SetFormat(fmt);	m_CapVtgHigh.SetValue(0.0);

	VERIFY(m_VtgHigh.Initialize(this, IDC_VTG_HIGH));			m_VtgHigh.SetFormat(fmt);		m_VtgHigh.SetValue(0.0);	
	VERIFY(m_VtgLow.Initialize(this, IDC_VTG_LOW));				m_VtgLow.SetFormat(fmt);		m_VtgLow.SetValue(0.0);
	a = 3, b = 1;
	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);//yulee 20190531_3 //yulee 20190625
	
	a = GetDocument()->GetCrtUnitFloat();
	if(GetDocument()->GetCrtUnit() == "A")
	{
		//b = 3;
		b = 4; //ksj 20171120
	}
	else
	{
		b = 6;
	}

	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
// 	//안전조건 전류 상한값 
// 	VERIFY(m_PreMaxI.Initialize(this, IDC_PRETEST_MAXI));
// 	m_PreMaxI.SetFormat(fmt); m_PreMaxI.SetValue(0.0);
// 	
// 	//안전조건 전류 하한값 
// 	VERIFY(m_TrickleI.Initialize(this, IDC_PRETEST_TRCKLE_I));
// 	m_TrickleI.SetFormat(fmt); m_TrickleI.SetValue(0.0);
// 	
// 	//안전조건 용량 상한값 
// 	VERIFY(m_PreOCV.Initialize(this, IDC_PRETEST_OCV));
// 	m_PreOCV.SetFormat(fmt); m_PreOCV.SetValue(0.0);
// 
// 	//안전조건 Capacitance 상한값 
// 	VERIFY(m_PreOCV2.Initialize(this, IDC_PRETEST_OCV2));
// 	m_PreOCV2.SetFormat(fmt); m_PreOCV2.SetValue(0.0);

	//안전조건 전류 상한값 
	VERIFY(m_SafetyMaxI.Initialize(this, IDC_SAFETY_MAX_I));
	m_SafetyMaxI.SetFormat(fmt); m_SafetyMaxI.SetValue(0.0);
		
	//안전조건 용량 상한값 
	VERIFY(m_SafetyMaxC.Initialize(this, IDC_SAFETY_MAX_C));
	m_SafetyMaxC.SetFormat(fmt); m_SafetyMaxC.SetValue(0.0);
	
	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(6);
	//안전조건 전력 상한값 
	VERIFY(m_SafetyMaxW.Initialize(this, IDC_SAFETY_MAX_W));
	m_SafetyMaxW.SetFormat(fmt); m_SafetyMaxW.SetValue(0.0);

	//안전조건 전력량 상한값 
	VERIFY(m_SafetyMaxWh.Initialize(this, IDC_SAFETY_MAX_WH));
	m_SafetyMaxWh.SetFormat(fmt); m_SafetyMaxWh.SetValue(0.0);
	
/*	VERIFY(m_TrickleTime.AttachDateTimeCtrl(IDC_PRETEST_TRICKLE_TIME, this, 0));
	m_TrickleTime.SetFormat("HH:mm:ss");
	m_TrickleTime.SetTime(ole);
*/
/*	fmt.SetFractionalDigits(0);	fmt.SetDecimalDigits(3);
	VERIFY(m_FaultNo.Initialize(this, IDC_PRETEST_FAULT_NO));
	m_FaultNo.SetFormat(fmt); m_FaultNo.SetValue(0.0);
*/	

	//전류 설정
	VERIFY(m_CrtHigh.Initialize(this, IDC_CRT_HIGH));			m_CrtHigh.SetFormat(fmt);		m_CrtHigh.SetValue(0.0);
	VERIFY(m_CrtLow.Initialize(this, IDC_CRT_LOW));				m_CrtLow.SetFormat(fmt);		m_CrtLow.SetValue(0.0);
	VERIFY(m_DeltaI.Initialize(this, IDC_DELTA_I));				m_DeltaI.SetFormat(fmt);		m_DeltaI.SetValue(0.0);

	//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
	UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
	if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
	{
		//공통 안전조건 

		CString strUnit;
		strUnit.Format(_T("%s"), GetDocument()->GetVtgUnit()); //yulee 20190909
		if(strUnit.CompareNoCase(_T("V"))== 0)
		{
			VERIFY(m_StdDeltaMaxV.Initialize(this, IDC_SAFETY_CELL_DELTA_STD_MAX_V));	m_StdDeltaMaxV.SetFormat(fmt);			m_StdDeltaMaxV.SetValue(4.1);
			VERIFY(m_StdDeltaMinV.Initialize(this, IDC_SAFETY_CELL_DELTA_STD_MIN_V));	m_StdDeltaMinV.SetFormat(fmt);			m_StdDeltaMinV.SetValue(3.4);
			VERIFY(m_SafetyCellDeltaV.Initialize(this, IDC_SAFETY_CELL_DELTA_V));		m_SafetyCellDeltaV.SetFormat(fmt);		m_SafetyCellDeltaV.SetValue(0.05);
			VERIFY(m_DeltaMaxV.Initialize(this, IDC_SAFETY_CELL_DELTA_MAX_V));			m_DeltaMaxV.SetFormat(fmt);				m_DeltaMaxV.SetValue(0.2);
			VERIFY(m_DeltaMinV.Initialize(this, IDC_SAFETY_CELL_DELTA_MIN_V));			m_DeltaMinV.SetFormat(fmt);				m_DeltaMinV.SetValue(0.1);
		}
		else
		{
			SECCurrencyEdit::Format fmtCellVDevSafetyCond(FALSE);
			fmtCellVDevSafetyCond.SetMonetarySymbol(0);
			fmtCellVDevSafetyCond.SetPositiveFormat(2);
			fmtCellVDevSafetyCond.SetNegativeFormat(2);
			fmtCellVDevSafetyCond.EnableLeadingZero(FALSE);
			fmtCellVDevSafetyCond.SetThousandSeparator(0);

			int nDec = 4;
			int nFrac = 1;
			fmtCellVDevSafetyCond.SetFractionalDigits(nFrac);	fmtCellVDevSafetyCond.SetDecimalDigits(nDec);
			VERIFY(m_StdDeltaMaxV.Initialize(this, IDC_SAFETY_CELL_DELTA_STD_MAX_V));	m_StdDeltaMaxV.SetFormat(fmtCellVDevSafetyCond);			m_StdDeltaMaxV.SetValue(4100);
			VERIFY(m_StdDeltaMinV.Initialize(this, IDC_SAFETY_CELL_DELTA_STD_MIN_V));	m_StdDeltaMinV.SetFormat(fmtCellVDevSafetyCond);			m_StdDeltaMinV.SetValue(3400);
			VERIFY(m_SafetyCellDeltaV.Initialize(this, IDC_SAFETY_CELL_DELTA_V));		m_SafetyCellDeltaV.SetFormat(fmtCellVDevSafetyCond);		m_SafetyCellDeltaV.SetValue(50);
			VERIFY(m_DeltaMaxV.Initialize(this, IDC_SAFETY_CELL_DELTA_MAX_V));			m_DeltaMaxV.SetFormat(fmtCellVDevSafetyCond);				m_DeltaMaxV.SetValue(200);
			VERIFY(m_DeltaMinV.Initialize(this, IDC_SAFETY_CELL_DELTA_MIN_V));			m_DeltaMinV.SetFormat(fmtCellVDevSafetyCond);				m_DeltaMinV.SetValue(100);
		}

		m_bChkDeltaVVent.SetCheck(TRUE);

		CString strOut;
		double	dwValue1 =0.0, dwValue2 =0.0;  m_StdDeltaMinV.GetValue(dwValue1); m_StdDeltaMaxV.GetValue(dwValue2);
		if(strUnit.CompareNoCase(_T("V"))== 0)
		{
			strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg1","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
		}
		else
		{
			strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg2","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
		}

		m_StaticCtrlSaftyCommonDelta.SetWindowTextA(strOut);

		SECCurrencyEdit::Format fmt1(FALSE);
		fmt1.SetMonetarySymbol(0);
		fmt1.SetPositiveFormat(2);
		fmt1.SetNegativeFormat(2);
		fmt1.EnableLeadingZero(FALSE);
		fmt1.SetThousandSeparator(0);
		fmt1.SetDecimalDigits(0);
		fmt1.SetFractionalDigits(2);	fmt1.SetDecimalDigits(3);

		//Step 안전조건 AuxT, AuxTH, AuxV 편차 기능
		VERIFY(m_SafetyCellDeltaVStep.Initialize(this, IDC_CELL_DELTA_V_STEP));		m_SafetyCellDeltaVStep.SetFormat(fmt);	m_SafetyCellDeltaVStep.SetValue(0.3);	
		VERIFY(m_StepDeltaAuxTemp.Initialize(this, IDC_STEP_AUXT_DELTA));			m_StepDeltaAuxTemp.SetFormat(fmt);		m_StepDeltaAuxTemp.SetValue(5.0);
		VERIFY(m_StepDeltaAuxTH.Initialize(this, IDC_STEP_AUXTH_DELTA));			m_StepDeltaAuxTH.SetFormat(fmt);		m_StepDeltaAuxTH.SetValue(10.0);
		VERIFY(m_StepDeltaAuxT.Initialize(this, IDC_STEP_AUXT_AUXTH_DELTA));		m_StepDeltaAuxT.SetFormat(fmt);			m_StepDeltaAuxT.SetValue(0.0);
		VERIFY(m_StepDeltaAuxVTime.Initialize(this, IDC_STEP_AUXV_TIME));			m_StepDeltaAuxVTime.SetFormat(fmt1);	m_StepDeltaAuxVTime.SetValue(60.0);
		VERIFY(m_StepDeltaAuxV.Initialize(this, IDC_STEP_AUXV_DELTA));				m_StepDeltaAuxV.SetFormat(fmt);			m_StepDeltaAuxV.SetValue(0.1);
		
		m_StepDeltaAuxV.EnableWindow(FALSE);
		m_bChkStepDeltaVentAuxV.SetCheck(FALSE);
		m_bChkStepDeltaVentAuxTemp.SetCheck(FALSE);
		m_bChkStepDeltaVentAuxTh.SetCheck(FALSE);
		m_bChkStepDeltaVentAuxT.SetCheck(FALSE);
		m_bChkStepVentAuxV.SetCheck(FALSE);

		int iUseVent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "No Use Vent", 0);
		int bUseVentSafety = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Use Vent Safety", 1); //ksj 20200226 : No Use Vent 항목 있는지 모르고 신규 추가했었음. 이쪽에도 조건 추가
		//if (iUseVent)
		if (iUseVent || !bUseVentSafety) //ksj 20200226		
		{
			m_bChkDeltaVVent.ShowWindow(SW_HIDE);
			m_bChkStepDeltaVentAuxV.ShowWindow(SW_HIDE);
			m_bChkStepDeltaVentAuxTemp.ShowWindow(SW_HIDE);
			m_bChkStepDeltaVentAuxTh.ShowWindow(SW_HIDE);
			m_bChkStepDeltaVentAuxT.ShowWindow(SW_HIDE);
			m_bChkStepVentAuxV.ShowWindow(SW_HIDE);
		}

  		CString strFormat;
		if((GetDocument()->GetVtgUnit()).CompareNoCase(_T("mV"))== 0)
		{
			strFormat.Format(Fun_FindMsg("DisplaySaftyCommon_msg4","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
		}
		else
		{
			strFormat.Format(Fun_FindMsg("DisplaySaftyCommon_msg3","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
		}
  		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC3)->SetWindowText(strFormat);
  		
		strFormat.Format(Fun_FindMsg("DisplaySaftyCommon_msg5","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
  		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC2)->SetWindowText(strFormat);

		strFormat.Format(_T("%s"), GetDocument()->GetVtgUnit());
		GetDlgItem(IDC_COMMON_SAFTY_V_STATIC1)->SetWindowText(strFormat);
		GetDlgItem(IDC_COMMON_SAFTY_V_STATIC2)->SetWindowText(strFormat);
		GetDlgItem(IDC_COMMON_SAFTY_V_STATIC3)->SetWindowText(strFormat);		
	}	
	else
	{
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_MAX_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_MIN_MAX_STATIC)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V_YELLOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_MIN_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC3)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_IDC_SAFETY_CELL_DELTA_V_YELLOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_IDC_SAFETY_CELL_DELTA_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_V)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V_YELLOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC2)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_SAFETY_CELL_DELTA_MAX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_CELL_DELTA_V_UNIT_STATIC5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_DELTA_CHAMBER)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_CELL_DELTA_V_STEP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CELL_DELTA_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXT_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_STEP_AUXT_DELTA_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STEP_AUXV_DELTA_STATIC)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_COMMON_SAFTY_V_STATIC1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMMON_SAFTY_V_STATIC2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMMON_SAFTY_V_STATIC3)->ShowWindow(SW_HIDE);
	}

	//ksj 20210610 : AuxV 사용안할시 비활성화
	if(!m_bUseAuxV)
	{
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MIN_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_MIN_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_STD_MAX_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_SAFETY_CELL_DELTA_MAX_V)->EnableWindow(FALSE);
	}

	//전류 상승 비교 하한값
	VERIFY(m_CompI[0].Initialize(this, IDC_COMP_I1));
	m_CompI[0].SetFormat(fmt); m_CompI[0].SetValue(0.0);

	VERIFY(m_CompI[1].Initialize(this, IDC_COMP_I2));
	m_CompI[1].SetFormat(fmt); m_CompI[1].SetValue(0.0);

	VERIFY(m_CompI[2].Initialize(this, IDC_COMP_I3));
	m_CompI[2].SetFormat(fmt); m_CompI[2].SetValue(0.0);

	//전류 상승 비교 상한값
	VERIFY(m_CompI1[0].Initialize(this, IDC_COMP_I4));
	m_CompI1[0].SetFormat(fmt); m_CompI1[0].SetValue(0.0);

	VERIFY(m_CompI1[1].Initialize(this, IDC_COMP_I5));
	m_CompI1[1].SetFormat(fmt); m_CompI1[1].SetValue(0.0);

	VERIFY(m_CompI1[2].Initialize(this, IDC_COMP_I6));
	m_CompI1[2].SetFormat(fmt); m_CompI1[2].SetValue(0.0);

	//Report 조건 전류
	VERIFY(m_ReportI.Initialize(this, IDC_REPORT_CURRENT));
	m_ReportI.SetFormat(fmt); m_ReportI.SetValue(0.0);
	
	a = GetDocument()->GetCapUnitFloat();
	if(GetDocument()->GetCapUnit() == "A" || GetDocument()->GetCapUnit() == "F")
	{
		b = 3;
	}
	else
	{
		b = 6;
	}
	fmt.SetFractionalDigits(a);	fmt.SetDecimalDigits(b);
	VERIFY(m_CapHigh.Initialize(this, IDC_CAP_HIGH));			m_CapHigh.SetFormat(fmt);		m_CapHigh.SetValue(0.0);
	VERIFY(m_CapLow.Initialize(this, IDC_CAP_LOW));				m_CapLow.SetFormat(fmt);		m_CapLow.SetValue(0.0);


	//Report 온도 조건 
	fmt.SetFractionalDigits(1);	fmt.SetDecimalDigits(3);
	VERIFY(m_ReportTemp.Initialize(this, IDC_REPORT_TEMPERATURE));
	m_ReportTemp.SetFormat(fmt); m_ReportTemp.SetValue(0.0);

	//안전조건 온도 상한값 
	VERIFY(m_SafetyMaxT.Initialize(this, IDC_SAFETY_MAX_T));
	m_SafetyMaxT.SetFormat(fmt); m_SafetyMaxT.SetValue(0.0);
	
	//안전조건 온도 하한값 
	VERIFY(m_SafetyMinT.Initialize(this, IDC_SAFETY_MIN_T));
	m_SafetyMinT.SetFormat(fmt); m_SafetyMinT.SetValue(0.0);

// 	VERIFY(m_PreDeltaV.Initialize(this, IDC_PRETEST_DELTA_V));
// 	m_PreDeltaV.SetFormat(fmt); m_PreDeltaV.SetValue(0.0);
	
	//ljb SBC가 챔버와 연동 할때 챔버 온도 상하한값
	VERIFY(m_TempLow.Initialize(this, IDC_TEMP_LOW));
	m_TempLow.SetFormat(fmt); m_TempLow.SetValue(0.0);
	VERIFY(m_TempHigh.Initialize(this, IDC_TEMP_HIGH));
	m_TempHigh.SetFormat(fmt); m_TempHigh.SetValue(0.0);
	
	//Impedance
	VERIFY(m_ImpHigh.Initialize(this, IDC_IMP_HIGH));			m_ImpHigh.SetFormat(fmt);		m_ImpHigh.SetValue(0.0);
	VERIFY(m_ImpLow.Initialize(this, IDC_IMP_LOW));				m_ImpLow.SetFormat(fmt);		m_ImpLow.SetValue(0.0);

	//Capacitance
	VERIFY(m_CapacitanceHigh.Initialize(this, IDC_CAPACITANCE_HIGH));	m_CapacitanceHigh.SetFormat(fmt);	m_CapacitanceHigh.SetValue(0.0);
	VERIFY(m_CapacitanceLow.Initialize(this, IDC_CAPACITANCE_LOW1));	m_CapacitanceLow.SetFormat(fmt);	m_CapacitanceLow.SetValue(0.0);


	//	VERIFY(m_DeltaTime.AttachDateTimeCtrl(IDC_DELTA_TIME, this, 0));
	m_DeltaTime.SetFormat("HH:mm:ss");
	m_DeltaTime.SetTime(DATE(0));

	VERIFY(m_CompTimeV[0].AttachDateTimeCtrl(IDC_COMP_TIME1, this, 0));
	m_CompTimeV[0].SetFormat("HH:mm:ss");
	m_CompTimeV[0].SetTime(ole);

	VERIFY(m_CompTimeV[1].AttachDateTimeCtrl(IDC_COMP_TIME2, this, 0));
	m_CompTimeV[1].SetFormat("HH:mm:ss");
	m_CompTimeV[1].SetTime(ole);

	VERIFY(m_CompTimeV[2].AttachDateTimeCtrl(IDC_COMP_TIME3, this, 0));
	m_CompTimeV[2].SetFormat("HH:mm:ss");
	m_CompTimeV[2].SetTime(ole);

	VERIFY(m_CompTimeI[0].AttachDateTimeCtrl(IDC_COMP_I_TIME1, this, 0));
	m_CompTimeI[0].SetFormat("HH:mm:ss");
	m_CompTimeI[0].SetTime(ole);

	VERIFY(m_CompTimeI[1].AttachDateTimeCtrl(IDC_COMP_I_TIME2, this, 0));
	m_CompTimeI[1].SetFormat("HH:mm:ss");
	m_CompTimeI[1].SetTime(ole);

	VERIFY(m_CompTimeI[2].AttachDateTimeCtrl(IDC_COMP_I_TIME3, this, 0));
	m_CompTimeI[2].SetFormat("HH:mm:ss");
	m_CompTimeI[2].SetTime(ole);

	//Report 시간
//	VERIFY(m_ReportTime.AttachDateTimeCtrl(IDC_REPORT_TIME, this, 0));
	m_ReportTime.SetFormat("HH:mm:ss");
	m_ReportTime.SetTime(DATE(0));


	return TRUE;
}

//ReDraw Step Grid
BOOL CCTSEditorProView::DisplayStepGrid(int nStepNum)
{
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();		//Get Document point
	int nTotStepNum = pDoc->GetTotalStepNum();					//Total Step Number
	if(nTotStepNum < 0)	return FALSE;

	CString strTemp;
	STEP *pStep;
	ROWCOL nRow = m_wndStepGrid.GetRowCount();					//Get Current Displayed Step Num

	if(nStepNum > 0)											//UpDate 1 Step Grid
	{
		if(nStepNum > nTotStepNum || nStepNum > nRow)	return FALSE;

		pStep = (STEP *)pDoc->GetStepData(nStepNum);
		if(!pStep)	return FALSE;
		
		BOOL bLock = m_wndStepGrid.LockUpdate(TRUE);
		
		//Step Grid를 현재 Type에 맞게 Setting 한다.
		//Step Type/Mode 표시 한다.
		SettingStepWndType(nStepNum, (int)pStep->chType);

		//안전값 및 기타 옵션 표시
		DisplayStepOption(pStep);

		//Added by  KBH 2005///////////////////////////////////////////////////////
		//현재 선택된 Step을 색상으로 표시한다.
		//Row Header Style
		for(int k =0; k<nRow; k++)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(k+1, 0), CGXStyle()
													.SetInterior(RGB(255,255,230))
													.SetFont(CGXFont().SetBold(FALSE))
										);
		}
		//Set current row
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, 0), CGXStyle()
										.SetInterior(RGB(255,255,0))
										.SetFont(CGXFont().SetBold(TRUE))
									);
		//////////////////////////////////////////////////////////////////////////

		//all row Grid style
		m_wndStepGrid.SetStyleRange(CGXRange().SetRows(1, nRow), CGXStyle()
										.SetTextColor(RGB(165,165,165))
										.SetFont(CGXFont().SetBold(FALSE))
									);

		//Set current row
		m_wndStepGrid.SetStyleRange(CGXRange().SetRows(nStepNum), CGXStyle()
											.SetTextColor(RGB(0,0,0))
											.SetFont(CGXFont().SetBold(TRUE))
										);
		//////////////////////////////////////////////////////////////////////////
		strTemp.Format(Fun_FindMsg("DisplayStepGrid_charge_msg","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
		m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_CHARGE_V), strTemp);
		strTemp.Format(Fun_FindMsg("DisplayStepGrid_discharge_msg","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
		m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_DISCHARGE_V), strTemp);

		strTemp.Format(Fun_FindMsg("DisplayStepGrid_msg3","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCrtUnit());
		m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I),	strTemp);
		strTemp.Format(Fun_FindMsg("DisplayStepGrid_msg4","IDD_CTSEditorPro_CYCL"), GetDocument()->GetWattUnit());				//ljb
		m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_POWER),	strTemp);
		strTemp.Format(Fun_FindMsg("DisplayStepGrid_msg5","IDD_CTSEditorPro_CYCL"));				//ljb
		m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_RESISTANCE),	strTemp);

		switch(int(pStep->chType))
		{
			//20090206 KKY////////////////////////////////////////////////////////////////////////////////////
		case PS_STEP_ADV_CYCLE:
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			break;
			//////////////////////////////////////////////////////////////////////////////////////////////////
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		//khs 20081113
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_RANGE), pStep->lRange);	//khs 20081113
			
			if(pStep->chMode == PS_MODE_CP)
			{
				m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_POWER), CGXStyle().SetEnabled(TRUE));
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_POWER), pDoc->WattUnitTrans(pStep->fPref, FALSE));//pStep->fIref/I_UNIT_FACTOR);	//ljb
			}
			else if(pStep->chMode == PS_MODE_CCCP)	//ljb 20101214
			{
				m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));		//ljb
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
				m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_POWER), CGXStyle().SetEnabled(TRUE));
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_POWER), pDoc->WattUnitTrans(pStep->fPref, FALSE));//pStep->fIref/I_UNIT_FACTOR);	//ljb
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				//mOhm으로 저장된 data를 Ohm으로 표시 strTemp = "저항(Ω)";
				m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(TRUE));		//ljb
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_RESISTANCE), pStep->fRref);
			}
			else
			{
				m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));		//ljb
				m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			}

			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), GetModeIndex(pStep->chMode));
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_CHARGE_V), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fVref/V_UNIT_FACTOR);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_DISCHARGE_V), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));//pStep->fVref/V_UNIT_FACTOR);

			//챔버온도습도
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_TEMP), pStep->fTref);	//ljb 201010
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_HUMI), pStep->fHref);	//ljb 201010
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CELLBAL)     , pDoc->MakeCellBalString(nStepNum)); //cny 201808			
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY)   , pDoc->MakePwrSupplyString(nStepNum)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHILLER)     , pDoc->MakeChillerString(nStepNum)); //cny 201808
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			break;

		case PS_STEP_REST:
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), (LONG)-1);
			//챔버온도습도
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_TEMP), pStep->fTref);	//ljb 201010
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_HUMI), pStep->fHref);	//ljb 201010

			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CELLBAL)     , pDoc->MakeCellBalString(nStepNum)); //cny 201808			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY)   , pDoc->MakePwrSupplyString(nStepNum)); //cny 201808
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHILLER)     , pDoc->MakeChillerString(nStepNum)); //cny 201808

			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			break;

		case PS_STEP_IMPEDANCE:
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), GetModeIndex(pStep->chMode));
	
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_CHARGE_V), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));	//pStep->fVref/V_UNIT_FACTOR);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(nStepNum)); //cny 201808
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHILLER)   , pDoc->MakeChillerString(nStepNum)); //cny 201808
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			break;

		case PS_STEP_OCV:
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), (LONG)-1);
			break;

		case PS_STEP_LOOP:
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), (LONG)-1);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			break;

		case PS_STEP_PATTERN:
			strTemp =" ---- ";
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_CHARGE_V), strTemp);
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_DISCHARGE_V), strTemp);

			//ljb 201012
			//strTemp.Format("충전(%s)", GetDocument()->GetVtgUnit());
			strTemp.Format(Fun_FindMsg("DisplayStepGrid_charge_msg","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit());
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
			//strTemp.Format("방전(%s)", GetDocument()->GetVtgUnit());
			strTemp.Format(Fun_FindMsg("DisplayStepGrid_discharge_msg","IDD_CTSEditorPro_CYCL"), GetDocument()->GetVtgUnit()); 
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_POWER), strTemp);

			if (pStep->chMode == PS_MODE_CV)
			{
				strTemp.Format(Fun_FindMsg("DisplayStepGrid_charge_msg","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCrtUnit());
				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
				strTemp.Format(Fun_FindMsg("DisplayStepGrid_discharge_msg","IDD_CTSEditorPro_CYCL"), GetDocument()->GetCrtUnit());
				m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_POWER), strTemp);
			}

			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		//khs 20081113
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_RANGE), pStep->lRange);	//khs 20081113
			strTemp = pDoc->GetSimulationTitle(pStep->szSimulFile);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), strTemp);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_MODE), CGXStyle()
				 .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));

			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_POWER), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));//pStep->fIref/I_UNIT_FACTOR);

 			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_POWER), pDoc->VUnitTrans(pStep->fPref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
 			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_RESISTANCE), pDoc->VUnitTrans(pStep->fRref, FALSE));//pStep->fIref/I_UNIT_FACTOR);

			//strTemp.Format("충전(%s)", GetDocument()->GetVtgUnit());
			//m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_POWER), strTemp);
			//strTemp.Format("방전(%s)", GetDocument()->GetVtgUnit());
			//m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_RESISTANCE), strTemp);
			//챔버온도습도
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_TEMP), pStep->fTref);	//ljb 201010
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_HUMI), pStep->fHref);	//ljb 201010
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CELLBAL)   , pDoc->MakeCellBalString(nStepNum)); //cny 201808	
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(nStepNum)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHILLER)   , pDoc->MakeChillerString(nStepNum)); //cny 201808
			
			break;			//ljb 20110917 추가해
		//2014.09.02 USER MAP 추가.
		case PS_STEP_USER_MAP:
			strTemp =" ---- ";
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_CHARGE_V), strTemp);
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_DISCHARGE_V), strTemp);
						
			strTemp.Format("충전(%s)", GetDocument()->GetVtgUnit());
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_REF_I), strTemp);
			strTemp.Format("방전(%s)", GetDocument()->GetVtgUnit());
			m_wndStepGrid.SetValueRange(CGXRange(0, COL_STEP_POWER), strTemp);
			
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_RANGE), pStep->lRange);	
			strTemp = pDoc->GetSimulationTitle(pStep->szUserMapFile);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), strTemp);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_MODE), CGXStyle()
				.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_POWER), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_TEMP), pStep->fTref);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_HUMI), pStep->fHref);

			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(nStepNum)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHILLER)   , pDoc->MakeChillerString(nStepNum)); //cny 201808
			break;			

		case PS_STEP_EXT_CAN:
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(TRUE));		//ljb
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(TRUE));		//ljb
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));		//ljb
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_POWER), CGXStyle().SetEnabled(TRUE));
			
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_CHARGE_V), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_DISCHARGE_V), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_POWER), pDoc->WattUnitTrans(pStep->fPref, FALSE));//pStep->fIref/I_UNIT_FACTOR);	//ljb
			
			m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		//khs 20081113
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_RANGE), pStep->lRange);	//khs 20081113
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_END), pDoc->MakeEndString(nStepNum));
			//m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_MODE), CGXStyle()
			// 				.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
			
			
			//챔버온도습도
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_TEMP), pStep->fTref);	//ljb 201010
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHAMBER_HUMI), pStep->fHref);	//ljb 201010
			
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(nStepNum)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
			m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_CHILLER)   , pDoc->MakeChillerString(nStepNum)); //cny 201808

			//ljb 2011318 이재복 ////////// e
			break;

		case PS_STEP_END:
		default:
			//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_MODE), (LONG)-1);
			break;
		}
		m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_END), CGXStyle()
	         .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->MakeEndString(nStepNum))));
		m_nDisplayStep = nStepNum;

		m_wndStepGrid.LockUpdate(bLock);
		m_wndStepGrid.Redraw();
	}
	else//Update All Step Data
	{
		//ljb Step grid 초기화
		BOOL bLock = m_wndStepGrid.LockUpdate(TRUE);
		
		int nDiffRow = nTotStepNum - nRow;
		if(nDiffRow > 0)
		{	
			m_wndStepGrid.InsertRows(nRow+1, nDiffRow);
		}
		else if(nDiffRow <0)
		{
			m_wndStepGrid.RemoveRows(nTotStepNum+1, nRow);
		}

		if(nTotStepNum == 0)
		{
			m_nDisplayStep = 0;
			DisplayStepOption(NULL);	//Reset Control Data

			for(int i = IDC_VTG_HIGH; i<= IDC_DELTA_TIME; i++)
			{
				GetDlgItem(i)->EnableWindow(FALSE);
			}

			GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(FALSE);//yulee 20190531_3

			GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(FALSE);
			GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(FALSE);
			GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(FALSE);
			GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(FALSE);
			GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(FALSE);

			SetCompCtrlEnable(FALSE);
		}
		else
		{
			if(m_nDisplayStep == 0 && nTotStepNum > 0)
			{
				m_nDisplayStep = 1;
			}

			for(int i = 0; i<nTotStepNum; i++)
			{
				pStep = (STEP *)pDoc->GetStepData(i+1);
				ASSERT(pStep != NULL);
				//////////////////////////////////////////////////////////////////////////
				switch (pStep->nMultiLoopGroupID)
				{
				case 1:
					m_nMultiGroupLoopInfoCycle[0]=pStep->nMultiLoopInfoCycle;
					m_nMultiGroupLoopInfoGoto[0]=pStep->nMultiLoopInfoGotoStep;
					break;
				case 2:
					m_nMultiGroupLoopInfoCycle[1]=pStep->nMultiLoopInfoCycle;
					m_nMultiGroupLoopInfoGoto[1]=pStep->nMultiLoopInfoGotoStep;
					break;
				case 3:
					m_nMultiGroupLoopInfoCycle[2]=pStep->nMultiLoopInfoCycle;
					m_nMultiGroupLoopInfoGoto[2]=pStep->nMultiLoopInfoGotoStep;
					break;
				case 4:
					m_nMultiGroupLoopInfoCycle[3]=pStep->nMultiLoopInfoCycle;
					m_nMultiGroupLoopInfoGoto[3]=pStep->nMultiLoopInfoGotoStep;
					break;
				case 5:
					m_nMultiGroupLoopInfoCycle[4]=pStep->nMultiLoopInfoCycle;
					m_nMultiGroupLoopInfoGoto[4]=pStep->nMultiLoopInfoGotoStep;
					break;
				}
				switch (pStep->nAccLoopGroupID)
				{
				case 1:
					m_nAccGroupLoopInfoCycle[0]=pStep->nAccLoopInfoCycle;
					m_nAccGroupLoopInfoGoto[0]=pStep->nAccLoopInfoGotoStep;
					break;
				case 2:
					m_nAccGroupLoopInfoCycle[1]=pStep->nAccLoopInfoCycle;
					m_nAccGroupLoopInfoGoto[1]=pStep->nAccLoopInfoGotoStep;
					break;
				case 3:
					m_nAccGroupLoopInfoCycle[2]=pStep->nAccLoopInfoCycle;
					m_nAccGroupLoopInfoGoto[2]=pStep->nAccLoopInfoGotoStep;
					break;
				case 4:
					m_nAccGroupLoopInfoCycle[3]=pStep->nAccLoopInfoCycle;
					m_nAccGroupLoopInfoGoto[3]=pStep->nAccLoopInfoGotoStep;
					break;
				case 5:
					m_nAccGroupLoopInfoCycle[4]=pStep->nAccLoopInfoCycle;
					m_nAccGroupLoopInfoGoto[4]=pStep->nAccLoopInfoGotoStep;
					break;
				}
				//////////////////////////////////////////////////////////////////////////
							
				if((i+1) != (int)pStep->chStepNo)
				{
					//AfxMessageBox("Step Number Error");
					//return FALSE;
				}

				SettingStepWndType(i+1, (int)pStep->chType, FALSE);

				//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_TYPE), (LONG)(pStep->chType-1));
				//m_wndStepGrid.SetValueRange(CGXRange(nStepNum, COL_STEP_TYPE), (LONG)GetStepIndexFromType(pStep->chType));		//Step Type

				switch(int(pStep->chType))
				{
					//ljb 20090415 v1009 ////////////////////////////////////////////////////////////////////////
				case PS_STEP_ADV_CYCLE:
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					break;
				case PS_STEP_CHARGE:
				case PS_STEP_DISCHARGE:
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		//khs 20081113
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_RANGE), pStep->lRange);	//khs 20081113

					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_CHARGE_V), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));	//pStep->fVref/V_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_DISCHARGE_V), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));	//pStep->fVref/V_UNIT_FACTOR);
					if(pStep->chMode == PS_MODE_CP)
					{
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_POWER), pDoc->WattUnitTrans(pStep->fPref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					}
					else if(pStep->chMode == PS_MODE_CCCP)
					{
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_POWER), pDoc->WattUnitTrans(pStep->fPref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					}
					else if(pStep->chMode == PS_MODE_CR)
					{
						//mOhm으로 저장된 data를 Ohm으로 표시 
						//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_RESISTANCE), pStep->fPref/1000.0f);
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_RESISTANCE), pStep->fRref);
					}
					else
					{
						m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					}

					//ljb 챔버값 표시
					strTemp.Format("%.1f",pStep->fTref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_TEMP), strTemp);
					strTemp.Format("%.1f",pStep->fHref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_HUMI), strTemp);
					
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CELLBAL)   , pDoc->MakeCellBalString(i+1)); //cny 201808
					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(i+1)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHILLER)   , pDoc->MakeChillerString(i+1)); //cny 201808

					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					break;
				case PS_STEP_REST:
					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_MODE), (LONG)-1);
					//ljb 챔버값 표시
					strTemp.Format("%.1f",pStep->fTref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_TEMP), strTemp);
					strTemp.Format("%.1f",pStep->fHref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_HUMI), strTemp);

					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CELLBAL)   , pDoc->MakeCellBalString(i+1)); //cny 201808
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(i+1)); //cny 201808
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHILLER)   , pDoc->MakeChillerString(i+1)); //cny 201808

					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					break;

				case PS_STEP_IMPEDANCE:
					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_MODE), (LONG)(pStep->chMode-1));
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_CHARGE_V), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fVref/V_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->IUnitTrans(pStep->fIref, FALSE));//pStep->fIref/I_UNIT_FACTOR);

					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(i+1)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHILLER)   , pDoc->MakeChillerString(i+1)); //cny 201808

					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					break;
				
				case PS_STEP_LOOP:
					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_TYPE), (LONG)6);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					break;

				case PS_STEP_PATTERN:
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		//khs 20081113
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_RANGE), pStep->lRange);	//khs 20081113
					strTemp = pDoc->GetSimulationTitle(pStep->szSimulFile);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_MODE), strTemp);
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_MODE), CGXStyle()
						 .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));

 					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_CHARGE_V), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
 					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_DISCHARGE_V), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_POWER), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));//pStep->fIref/I_UNIT_FACTOR);

					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_POWER), pDoc->VUnitTrans(pStep->fPref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
 					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_RESISTANCE), pDoc->VUnitTrans(pStep->fRref, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_POWER, i+1, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(TRUE));		
					//ljb 챔버값 표시
					strTemp.Format("%.1f",pStep->fTref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_TEMP), strTemp);
					strTemp.Format("%.1f",pStep->fHref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_HUMI), strTemp);
					
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CELLBAL)   , pDoc->MakeCellBalString(i+1)); //cny 201808
					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(i+1)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHILLER)   , pDoc->MakeChillerString(i+1)); //cny 201808

					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					break;
				//2014.09.02 PS_STEP_USER_MAP 추가함
				case PS_STEP_USER_MAP:
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		//khs 20081113
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_RANGE), pStep->lRange);	//khs 20081113
					strTemp = pDoc->GetSimulationTitle(pStep->szUserMapFile);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_MODE), strTemp);
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_MODE), CGXStyle()
						.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(strTemp)));
										
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_POWER), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
										
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_POWER, i+1, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(TRUE));		
					//ljb 챔버값 표시
					strTemp.Format("%.1f",pStep->fTref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_TEMP), strTemp);
					strTemp.Format("%.1f",pStep->fHref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_HUMI), strTemp);
					
					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(i+1)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHILLER)   , pDoc->MakeChillerString(i+1)); //cny 201808
					
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					break;

				case PS_STEP_EXT_CAN:
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_CHARGE_V), pDoc->VUnitTrans(pStep->fVref_Charge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_DISCHARGE_V), pDoc->VUnitTrans(pStep->fVref_DisCharge, FALSE));//pStep->fIref/I_UNIT_FACTOR);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_REF_I), pDoc->VUnitTrans(pStep->fIref, FALSE));
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_POWER), pDoc->VUnitTrans(pStep->fPref, FALSE));
					
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_RANGE), CGXStyle().SetEnabled(TRUE));		//khs 20081113
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_RANGE), pStep->lRange);	//khs 20081113
					
					m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_REF_CHARGE_V, i+1, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(TRUE));		
					//ljb 챔버값 표시
					strTemp.Format("%.1f",pStep->fTref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_TEMP), strTemp);
					strTemp.Format("%.1f",pStep->fHref);
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHAMBER_HUMI), strTemp);
					
					//m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_PWRSUPPLY) , pDoc->MakePwrSupplyString(i+1)); //cny 201808 //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_CHILLER)   , pDoc->MakeChillerString(i+1)); //cny 201808
					
					m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
					
				//ljb 2011318 이재복 ////////// e
				case PS_STEP_OCV:
				case PS_STEP_END:
				default:
					break;
				}
				
				//End Type ToolTips UpDate
				m_wndStepGrid.SetStyleRange(CGXRange(i+1, COL_STEP_END), CGXStyle()
					 .SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, _T(pDoc->MakeEndString(i+1))));
				

				if(i+1 == m_nDisplayStep)
				{
					//Option Control들을 Step1내용으로 표시 한다.
					DisplayStepOption(pStep);

					#ifdef _CYCLER_
						m_wndStepGrid.SetCurrentCell(m_nDisplayStep, COL_STEP_TYPE);
					#else
						m_wndStepGrid.SetCurrentCell(m_nDisplayStep, COL_STEP_PROCTYPE);
					#endif		
				}

			}

		}//End for
		
		m_wndStepGrid.LockUpdate(bLock);
		m_wndStepGrid.Redraw();

	}

	strTemp.Format("Step %d", m_nDisplayStep);
	m_strStepNumber.SetText(strTemp);						//Display Step Number

	strTemp.Format("%d", m_wndStepGrid.GetRowCount());
	m_TotalStepCount.SetText(strTemp);
	UpdateData(FALSE);
	return TRUE;
}

//Load Battery Model Information from bata base.
BOOL CCTSEditorProView::RequeryBatteryModel(CString strDefaultName)
{
	int nTotModelNum = 0;
	CBatteryModelRecordSet	recordSet;

	//사용자에 맞는 모델만 Loading
#ifdef _USE_MODEL_USER_
	CString strID(GetDocument()->m_LoginData.szLoginID);
	if(GetDocument()->m_bUseLogin && !strID.IsEmpty())
	{
		recordSet.m_strFilter.Format("[Creator] = '%s'", strID);
	}
#endif

	recordSet.m_strSort.Format("[No]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();

	if(recordSet.IsBOF())
	{
		nTotModelNum = 0;
	}
	else
	{
		recordSet.MoveLast();
		nTotModelNum = recordSet.GetRecordCount();
		recordSet.MoveFirst(); 
	}

	nTotModelNum -= nRow;

	if(nTotModelNum > 0)
	{	
		m_pWndCurModelGrid->InsertRows(nRow+1, nTotModelNum);
	}
	else if(nTotModelNum <0)
	{
		m_pWndCurModelGrid->RemoveRows(nRow+nTotModelNum+1, nRow);
	}

	nRow = 0;
	int nSelRow = 0;
	int nSelID =0;
	CString strSelName;
	while(!recordSet.IsEOF())
	{	
		nRow++;
		//기본으로 표기할 모델 
		if(strDefaultName == recordSet.m_ModelName)
		{
			nSelRow = nRow;
			nSelID =  recordSet.m_ModelID;	
			strSelName = recordSet.m_ModelName;
		}
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), recordSet.m_ModelName);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_CREATOR), recordSet.m_Creator);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), recordSet.m_Description);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_PRIMARY_KEY), recordSet.m_ModelID);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_SAVED);
		recordSet.MoveNext();
	}

	//기본 이름과 맞는게 없을 경우 가장 마자막 표기 
	if(nSelRow == 0)
	{
		if(nRow > 0)
		{
			recordSet.MovePrev();
			strSelName = recordSet.m_ModelName;
			nSelID =  recordSet.m_ModelID;
			nSelRow = nRow;
		}
	}
	//else	//찾았을 경우
		
	UpdateDspModel(strSelName, nSelID);	//Last Battery Model is Current seleted Model
	recordSet.Close();

	//수량 Update
	UpdateBatteryCountState(nRow);

	//기본 모델 선택 
	m_pWndCurModelGrid->SetCurrentCell(nSelRow, 1);
	return TRUE;

}

//Load Test 
BOOL CCTSEditorProView::RequeryTestList(LONG lModelID, CString strDefaultName)
{
	int nTotModelNum = 0;
	CTestListRecordSet	recordSet;

	recordSet.m_strFilter.Format("[ModelID] = %ld", lModelID);
	recordSet.m_strSort.Format("[TestNo]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = m_wndTestListGrid.GetRowCount();

	if(recordSet.IsBOF())
	{
		nTotModelNum = 0;
	}
	else
	{
		recordSet.MoveLast();
		nTotModelNum = recordSet.GetRecordCount();
		recordSet.MoveFirst(); 
	}

	nTotModelNum -= nRow;

	if(nTotModelNum > 0)
	{	
		m_wndTestListGrid.InsertRows(nRow+1, nTotModelNum);
	}
	else if(nTotModelNum <0)
	{
		m_wndTestListGrid.RemoveRows(nRow+nTotModelNum+1, nRow);
	}

	nRow = 0;
	int nSelRow = 0;
	int nSelID  = 0;
	CString strSelName;

	while(!recordSet.IsEOF())
	{	
		nRow++;
		if(strDefaultName == recordSet.m_TestName)
		{
			nSelRow = nRow;
			nSelID  = recordSet.m_TestID;
			strSelName = recordSet.m_TestName;
		}
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(recordSet.m_ProcTypeID));	
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), recordSet.m_TestName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), recordSet.m_Creator);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), recordSet.m_Description);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), recordSet.m_ModifiedTime.Format());
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PRIMARY_KEY), recordSet.m_TestID);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_RESET_PROC), (long)recordSet.m_PreTestCheck);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_SAVED);
		recordSet.MoveNext();
	}
	
	if(nSelRow == 0)
	{
		if(nRow >0)	//가장 마지막 Recoedset을 보여 준다.
		{
			recordSet.MovePrev();
			nSelID = recordSet.m_TestID;
			strSelName = recordSet.m_TestName;
			nSelRow = nRow;
		}
	}
	recordSet.Close();
	
	//default 
	UpdateDspTest(strSelName, nSelID);
	m_wndTestListGrid.SetCurrentCell(nSelRow, 1);
	
	CString strTemp;
	strTemp.Format("%d", nRow);
	m_TotTestCount.SetText(strTemp);
	return TRUE;
}

//Load Step Data
int CCTSEditorProView::RequeryTestStep(LONG lTestID)
{
	int nTotStepNum = 0 ;
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	if(pDoc->RequeryPreTestParam(lTestID)<0)	return -1;
	InitMultiGroup(TRUE);	//ljb v1009
	InitAccGroup(TRUE);	//ljb v1009
	return nTotStepNum = pDoc->RequeryStepData(lTestID);	//Load Step Data to Memory Array
}

void CCTSEditorProView::OnLoadTest() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;

	CCTSEditorProDoc *pDoc = GetDocument();
	//if(pDoc->IsEdited() && pDoc->PermissionCheck(PMS_EDITOR_EDIT) != FALSE)
	if(pDoc->IsEdited() && pDoc->PermissionCheck(PMS_EDITOR_EDIT) != FALSE)
	{
		GetDlgItem(IDC_LOADED_TEST)->GetWindowText(strTemp);
		strTemp += Fun_FindMsg("OnLoadTest_save_msg","IDD_CTSEditorPro_CYCL");
		if(IDYES == MessageBox(strTemp, Fun_FindMsg("OnLoadTest_save","IDD_CTSEditorPro_CYCL"), MB_ICONQUESTION|MB_YESNO))
		{
			if(!StepSave())	return;
		}
	}
	
	ROWCOL nRow, nCol;

	if(!m_wndTestListGrid.GetCurrentCell(nRow, nCol))
	{
		AfxMessageBox(Fun_FindMsg("OnLoadTest_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}
	else
	{
		if(nRow >0)
		{
			m_lDisplayTestID = atoi(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY));

			pDoc->m_lLoadedProcType = PS_PGS_NOWORK;
			if(m_strTypeMaskArray.GetSize() > 0)
			{
				int nID = atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PROC_TYPE));
				if(nID >= 0 && nID < m_strTypeMaskArray.GetSize())
				{
					strTemp = m_strTypeMaskArray.GetAt(nID);

					//Mask가 지정되어 있지 않으면 모두 실행 가능 
					if(!strTemp.IsEmpty())
					{
						strTemp.MakeUpper();
						//공정 Type(AG, PI, PC, FO, IR,  FC, SL, GD, OV)과 
						//시행가능한 Step Type (CODERIZ) C: Charge, D: Discharge, Z: AC Impedance, R: DC Impedance, O: OCV, I:Rest, E:End) NULL이면 모든 Step type 수행가능
						CString strID = strTemp.Left(2);
						if(strID == PS_PGS_AGING_CODE)		pDoc->m_lLoadedProcType =  PS_PGS_AGING;
						else if(strID == PS_PGS_PREIMPEDANCE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_PREIMPEDANCE;
						else if(strID == PS_PGS_PRECHARGE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_PRECHARGE;
						else if(strID == PS_PGS_FORMATION_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_FORMATION;
						else if(strID == PS_PGS_IROCV_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_IROCV;
						else if(strID == PS_PGS_FINALCHARGE_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_FINALCHARGE;
						else if(strID == PS_PGS_SELECTOR_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_SELECTOR;
						else if(strID == PS_PGS_GRADING_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_GRADING;
						else if(strID == PS_PGS_OCV_CODE)	pDoc->m_lLoadedProcType =  PS_PGS_OCV;
					}
				}	
			}
		}	
		else
			return;
	}
	
	m_lLoadedBatteryModelID = m_lDisplayModelID;
	m_lLoadedTestID = m_lDisplayTestID;
	
//	strTemp.Format(" 모델명: %s[ID %ld]  공정명: %s[ID %ld]", m_strDspModelName, m_lLoadedBatteryModelID, m_strDspTestName, m_lLoadedTestID);
	strTemp.Format(" [%s] -> [%s]", m_strDspModelName, m_strDspTestName);
	m_LoadedTest.SetText(strTemp);
	ReLoadStepData(m_lDisplayTestID);
	GetDlgItem(IDC_STEP_INSERT)->EnableWindow(TRUE);
	GetDlgItem(IDC_STEP_DELETE)->EnableWindow(TRUE);
//	GetDlgItem(IDC_STEP_SAVE)->EnableWindow(TRUE);


	//yulee 20180912
	float fMaxCurrentSpec = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current_Spec", 5000);
	UpdateData();
	CString tmpStr;
	GetDlgItem(IDC_SAFETY_MAX_I)->GetWindowText(tmpStr);
	int nParallelNum = 0;
	int MaxISafety = 0;
	int iMaxCurrentSpec = (int)fMaxCurrentSpec/1000;
	nParallelNum = ((atoi(tmpStr))/(iMaxCurrentSpec*1.1))+1;
	if (nParallelNum > 1)
	{
		if((atoi(tmpStr))%((int)(iMaxCurrentSpec*1.1))== 0)
				nParallelNum--;
		int nTmp = nParallelNum-2;
		if(nTmp < 0)
			nTmp = 0;
		m_CboParallelNum.SetCurSel(nTmp);
		//m_bChkParallel.SetCheck(TRUE);
	}
	else
	{
		m_bChkParallel.SetCheck(FALSE);
	}
	if(atoi(tmpStr)>iMaxCurrentSpec*1.1)
		m_bChkParallel.SetCheck(TRUE);
	else
		m_bChkParallel.SetCheck(FALSE);

	UpdateData(FALSE);

 
#ifdef _FORMATION_
	if(pDoc->m_lLoadedProcType == PS_PGS_PRECHARGE || pDoc->m_lLoadedProcType == PS_PGS_FORMATION ||
		pDoc->m_lLoadedProcType == PS_PGS_FINALCHARGE)
	{
		GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(TRUE);		//cycler에서는 Cell 접촉검사 조건을 입력 하지 못하도록 한다.
	}
	else
	{
		GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(FALSE);		//cycler에서는 Cell 접촉검사 조건을 입력 하지 못하도록 한다.
	}
#endif
	
	m_bStepDataSaved = TRUE;
	
	//Undo List 삭제 
	GetDocument()->RemoveUndoStep();

	//ljb v1009 AccGroup
	Fun_UpdateMultiGroupList();
	Fun_UpdateDisplayMultiGroup();
	Fun_UpdateAccGroupList();
	Fun_UpdateDisplayAccGroup();

	//Data Edit를 감시하는 Timer
	if(m_nTimer == 0)
	{
		m_nTimer = SetTimer(1000, 500, NULL);
	}
}

//Model 추가
void CCTSEditorProView::OnModelNew() 
{
	NewModel();
}

//시험 목록 추가
void CCTSEditorProView::OnTestNew() 
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}
	// TODO: Add your control notification handler code here

	COleDateTime curDate = COleDateTime::GetCurrentTime();

	/////////Dialog에서 새로운 이름을 받아 들이도록 수정
	CTestNameDlg	dlg(m_lDisplayModelID);
	dlg.m_strCreator = GetDocument()->m_LoginData.szLoginID;
	dlg.m_createdDate = curDate;
	if(dlg.DoModal() != IDOK)
		return;

	ROWCOL nRow = m_wndTestListGrid.GetRowCount()+1;
	m_wndTestListGrid.InsertRows(nRow, 1);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (long)GetTestIndexFromType(dlg.m_lTestTypeID));	
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), dlg.m_strCreator);	
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), dlg.m_createdDate.Format());
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_NEW);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), dlg.m_strName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), dlg.m_strDecription);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_RESET_PROC), (long)dlg.m_bContinueCellCode);

	m_wndTestListGrid.SetCurrentCell(nRow, COL_TEST_PROC_TYPE);

	OnTestSave();	

	CCTSEditorProDoc *pDoc = GetDocument();
	pDoc->m_sPreTestParam.fStdMaxV = 4100;
	pDoc->m_sPreTestParam.fStdMinV = 3400;
	pDoc->m_sPreTestParam.fCellDataV = 50;
	pDoc->m_sPreTestParam.fDataMaxV = 200;
	pDoc->m_sPreTestParam.fDataMinV = 100;
	pDoc->m_sPreTestParam.bDataVent = 1;

	ROWCOL nCol;
	if(!m_wndTestListGrid.GetCurrentCell(nRow, nCol))
	{
		AfxMessageBox(Fun_FindMsg("OnLoadTest_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}
	else
	{
		if(nRow >0)
		{
			m_lDisplayTestID = atoi(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY));
		}
	}
	pDoc->SavePreTestParam(m_lDisplayTestID);
}

//Step 추가
void CCTSEditorProView::AddStepNew(int nStepNum, BOOL bAuto, STEP *pCopyStep)
{
	// TODO: Add your control notification handler code here
	ROWCOL nRow = m_wndStepGrid.GetRowCount();
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();

	if(nRow >= SCH_MAX_STEP)
	{
		MessageBox(Fun_FindMsg("AddStepNew_msg1","IDD_CTSEditorPro_CYCL"), "Step Error", MB_OK|MB_ICONSTOP);
		return;
	}

	if(nStepNum < 1)	nStepNum = 1;

	if(!pDoc->AddNewStep(nStepNum, pCopyStep))
	{
		MessageBox(Fun_FindMsg("AddStepNew_msg2","IDD_CTSEditorPro_CYCL"), "Step Add Error", MB_OK|MB_ICONSTOP);
		return;
	}

	//	m_wndStepGrid.InsertRows(nStepNum, 1);
	
	if(bAuto)	m_wndStepGrid.InsertRows(nStepNum+1, 1);		//마자막 삽입일 경우는
	else	m_wndStepGrid.InsertRows(nStepNum, 1);

#ifdef _CYCLER_
	m_wndStepGrid.SetCurrentCell(nStepNum, COL_STEP_TYPE);
	m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_TYPE+1, nStepNum, COL_STEP_INDEX), CGXStyle().SetEnabled(FALSE));
#else
	m_wndStepGrid.SetCurrentCell(nStepNum, COL_STEP_PROCTYPE);
	m_wndStepGrid.SetStyleRange(CGXRange(nStepNum, COL_STEP_TYPE, nStepNum, COL_STEP_INDEX), CGXStyle().SetEnabled(FALSE));
#endif

	int jj=0;
	STEP *pStep;
	for(int i=0; i<pDoc->GetTotalStepNum(); i++)
	{
		pStep = pDoc->GetStepData(i+1);
		if(pStep == NULL)	break;
		pStep->chStepNo = i+1;	//ljb 20150601 add
		if(pStep->chType == PS_STEP_LOOP)
		{
			//삭제 step이전이면서 삭제 Step이후를 참조하면 변경 실시(삭제 Step이전을 참조하면 변경하지 않음)
			if(i+1 < nStepNum && pStep->nLoopInfoGotoStep >= nStepNum && pStep->nLoopInfoGotoStep != PS_GOTO_NEXT_CYC)		
			{
				if (pStep->nLoopInfoGotoStep != PS_STEP_PAUSE && pStep->nLoopInfoGotoStep != PS_STEP_NEXT)	pStep->nLoopInfoGotoStep++;
				m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));

			}
			
			//삭제 step이후 이면서 삭제 이후 Step을 참조하면 변경 실시(삭제된 Step이전을 참조하면 변경하지 않음)
			if(i+1 >= nStepNum && pStep->nLoopInfoGotoStep >= nStepNum && pStep->nLoopInfoGotoStep != PS_GOTO_NEXT_CYC)
			{
				if (pStep->nLoopInfoGotoStep != PS_STEP_PAUSE && pStep->nLoopInfoGotoStep != PS_STEP_NEXT)	pStep->nLoopInfoGotoStep++;
				m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
			}
		}
		else
		{
			//ljb 20150326 add 스텝 편집에 의한 Goto 번호 자동 수정 20151217 추가 수정 PS_STEP_PAUSE
			//삭제 step이전이면서 삭제 Step이후를 참조하면 변경 실시(삭제 Step이전을 참조하면 변경하지 않음)
			if(i+1 < nStepNum && pStep->nValueStepNo >= nStepNum && pStep->nValueStepNo != PS_GOTO_NEXT_CYC){
				if (pStep->nValueStepNo != PS_STEP_NEXT && pStep->nValueStepNo != PS_STEP_PAUSE)	pStep->nValueStepNo++;
			}
			if(i+1 < nStepNum && pStep->nMultiLoopInfoGotoStep >= nStepNum && pStep->nMultiLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nMultiLoopInfoGotoStep != PS_STEP_NEXT && pStep->nMultiLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nMultiLoopInfoGotoStep++;
			}
			if(i+1 < nStepNum && pStep->nAccLoopInfoGotoStep >= nStepNum && pStep->nAccLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nAccLoopInfoGotoStep != PS_STEP_NEXT && pStep->nAccLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nAccLoopInfoGotoStep++;
			}
			if(i+1 < nStepNum && pStep->nGotoStepEndV_H >= nStepNum && pStep->nGotoStepEndV_H != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_H != PS_STEP_NEXT && pStep->nGotoStepEndV_H != PS_STEP_PAUSE)	pStep->nGotoStepEndV_H++;
			}
			if(i+1 < nStepNum && pStep->nGotoStepEndV_L >= nStepNum && pStep->nGotoStepEndV_L != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_L != PS_STEP_NEXT && pStep->nGotoStepEndV_L != PS_STEP_PAUSE)	pStep->nGotoStepEndV_L++;
			}
			if(i+1 < nStepNum && pStep->nGotoStepEndTime >= nStepNum && pStep->nGotoStepEndTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndTime != PS_STEP_NEXT && pStep->nGotoStepEndTime != PS_STEP_PAUSE)	pStep->nGotoStepEndTime++;
			}
			if(i+1 < nStepNum && pStep->nGotoStepEndCVTime >= nStepNum && pStep->nGotoStepEndCVTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndCVTime != PS_STEP_NEXT && pStep->nGotoStepEndCVTime != PS_STEP_PAUSE)	pStep->nGotoStepEndCVTime++;
			}
			if(i+1 < nStepNum && pStep->nGotoStepEndC >= nStepNum && pStep->nGotoStepEndC != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndC != PS_STEP_NEXT && pStep->nGotoStepEndC != PS_STEP_PAUSE)	pStep->nGotoStepEndC++;
			}
			if(i+1 < nStepNum && pStep->nGotoStepEndWh >= nStepNum && pStep->nGotoStepEndWh != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndWh != PS_STEP_NEXT && pStep->nGotoStepEndWh != PS_STEP_PAUSE)	pStep->nGotoStepEndWh++;
			}
			if(i+1 < nStepNum && pStep->nGotoStepEndValue >= nStepNum && pStep->nGotoStepEndValue != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndValue != PS_STEP_NEXT && pStep->nGotoStepEndValue != PS_STEP_PAUSE)	pStep->nGotoStepEndValue++;
			}
			if(i+1 < nStepNum && pStep->bUseLinkStep >= nStepNum && pStep->bUseLinkStep != PS_GOTO_NEXT_CYC){
				if (pStep->bUseLinkStep != PS_STEP_NEXT && pStep->bUseLinkStep != PS_STEP_PAUSE)	pStep->bUseLinkStep++;
			}
			for (jj=0; jj<SCH_MAX_END_CAN_AUX_POINT; jj++ )
			{
				if(i+1 < nStepNum && pStep->ican_branch[jj] >= nStepNum && pStep->ican_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->ican_branch[jj] != PS_STEP_NEXT && pStep->ican_branch[jj] != PS_STEP_PAUSE)	pStep->ican_branch[jj]++;
				}
				if(i+1 < nStepNum && pStep->iaux_branch[jj] >= nStepNum && pStep->iaux_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->iaux_branch[jj] != PS_STEP_NEXT && pStep->iaux_branch[jj] != PS_STEP_PAUSE)	pStep->iaux_branch[jj]++;
				}
			}		

			//삭제 step이후 이면서 삭제 이후 Step을 참조하면 변경 실시(삭제된 Step이전을 참조하면 변경하지 않음)
			if(i+1 >= nStepNum && pStep->nValueStepNo >= nStepNum && pStep->nValueStepNo != PS_GOTO_NEXT_CYC){
				if (pStep->nValueStepNo != PS_STEP_NEXT && pStep->nValueStepNo != PS_STEP_PAUSE)	pStep->nValueStepNo++;
			}
			if(i+1 >= nStepNum && pStep->nMultiLoopInfoGotoStep >= nStepNum && pStep->nMultiLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nMultiLoopInfoGotoStep != PS_STEP_NEXT && pStep->nMultiLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nMultiLoopInfoGotoStep++;
			}
			if(i+1 >= nStepNum && pStep->nAccLoopInfoGotoStep >= nStepNum && pStep->nAccLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nAccLoopInfoGotoStep != PS_STEP_NEXT && pStep->nAccLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nAccLoopInfoGotoStep++;
			}
			if(i+1 >= nStepNum && pStep->nGotoStepEndV_H >= nStepNum && pStep->nGotoStepEndV_H != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_H != PS_STEP_NEXT && pStep->nGotoStepEndV_H != PS_STEP_PAUSE)	pStep->nGotoStepEndV_H++;
			}
			if(i+1 >= nStepNum && pStep->nGotoStepEndV_L >= nStepNum && pStep->nGotoStepEndV_L != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_L != PS_STEP_NEXT && pStep->nGotoStepEndV_L != PS_STEP_PAUSE)	pStep->nGotoStepEndV_L++;
			}
			if(i+1 >= nStepNum && pStep->nGotoStepEndTime >= nStepNum && pStep->nGotoStepEndTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndTime != PS_STEP_NEXT && pStep->nGotoStepEndTime != PS_STEP_PAUSE)	pStep->nGotoStepEndTime++;
			}
			if(i+1 >= nStepNum && pStep->nGotoStepEndCVTime >= nStepNum && pStep->nGotoStepEndCVTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndCVTime != PS_STEP_NEXT && pStep->nGotoStepEndCVTime != PS_STEP_PAUSE)	pStep->nGotoStepEndCVTime++;
			}
			if(i+1 >= nStepNum && pStep->nGotoStepEndC >= nStepNum && pStep->nGotoStepEndC != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndC != PS_STEP_NEXT && pStep->nGotoStepEndC != PS_STEP_PAUSE)	pStep->nGotoStepEndC++;
			}
			if(i+1 >= nStepNum && pStep->nGotoStepEndWh >= nStepNum && pStep->nGotoStepEndWh != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndWh != PS_STEP_NEXT && pStep->nGotoStepEndWh != PS_STEP_PAUSE)	pStep->nGotoStepEndWh++;
			}
			if(i+1 >= nStepNum && pStep->nGotoStepEndValue >= nStepNum && pStep->nGotoStepEndValue != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndValue != PS_STEP_NEXT && pStep->nGotoStepEndValue != PS_STEP_PAUSE)	pStep->nGotoStepEndValue++;
			}
			if(i+1 >= nStepNum && pStep->bUseLinkStep >= nStepNum && pStep->bUseLinkStep != PS_GOTO_NEXT_CYC){
				if (pStep->bUseLinkStep != PS_STEP_NEXT && pStep->bUseLinkStep != PS_STEP_PAUSE)	pStep->bUseLinkStep++;
			}
			for (jj=0; jj<SCH_MAX_END_CAN_AUX_POINT; jj++ )
			{
				if(i+1 >= nStepNum && pStep->ican_branch[jj] >= nStepNum && pStep->ican_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->ican_branch[jj] != PS_STEP_NEXT && pStep->ican_branch[jj] != PS_STEP_PAUSE)	pStep->ican_branch[jj]++;
				}
				if(i+1 >= nStepNum && pStep->iaux_branch[jj] >= nStepNum && pStep->iaux_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->iaux_branch[jj] != PS_STEP_NEXT && pStep->iaux_branch[jj] != PS_STEP_PAUSE)	pStep->iaux_branch[jj]++;
				}
			}		

			m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));

		}
	}
	//AccGroup  누적 Goto 설정 값 수정
	for( int i=0; i< 5; i++)
	{
		if( m_nMultiGroupLoopInfoGoto[i] >= nStepNum && m_nMultiGroupLoopInfoGoto[i] != PS_STEP_NEXT) m_nMultiGroupLoopInfoGoto[i]++;
		if( m_nAccGroupLoopInfoGoto[i] >= nStepNum  && m_nAccGroupLoopInfoGoto[i] != PS_STEP_NEXT)	m_nAccGroupLoopInfoGoto[i]++;
	}

}

//Battery Model 삭제
void CCTSEditorProView::OnModelDelete() 
{
	// TODO: Add your control notification handler code here
	ModelDelete();

}

//시험 목록 삭제
void CCTSEditorProView::OnTestDelete() 
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}
	// TODO: Add your control notification handler code here
	CString		strTemp;
	CTestListRecordSet	recordSet;
	ROWCOL nRow = m_wndTestListGrid.GetRowCount();
	if(nRow <1)		return;				//Test Condition is not exist

	CRowColArray	awRows;
	m_wndTestListGrid.GetSelectedRows(awRows);		//Get Selected Test Condition
	if(awRows.GetSize() <=0)	return;
	
	if(awRows.GetSize() == 1)
	{
		strTemp.Format(Fun_FindMsg("OnTestDelete_msg2","IDD_CTSEditorPro_CYCL"), m_wndTestListGrid.GetValueRowCol(awRows[0], COL_TEST_NAME) );
	}
	else
	{
		strTemp = Fun_FindMsg("OnTestDelete_msg3","IDD_CTSEditorPro_CYCL");
	}

	if(IDNO == AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION))	
		return;

	try									//DataBase Open
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	for (int i = awRows.GetSize()-1; i >=0 ; i--)
	{
		strTemp = m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_PRIMARY_KEY);	//Get TestID
		if(!strTemp.IsEmpty())										//Delete Test Condition Form DB	
		{
			strTemp = "TestID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				LONG lDeleteTestID = recordSet.m_TestID;
				DeleteSimulationFileTestData(lDeleteTestID);

				recordSet.Delete();		
				
				if(lDeleteTestID == m_lLoadedTestID)
				{
					ReLoadStepData(lDeleteTestID);
					ClearStepState();
				}
			}
		}
		m_wndTestListGrid.RemoveRows(awRows[i], awRows[i]);			//Delete Test Condition Row
	}

	//순서 저장 2002/10/02
	for( int i = 0; i<m_wndTestListGrid.GetRowCount(); i++)
	{
		strTemp = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY);	//Get TestID
		if(!strTemp.IsEmpty())										//Delete Test Condition Form DB	
		{
			strTemp = "TestID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				recordSet.Edit();
				recordSet.m_TestNo = atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NO));
				recordSet.Update();
			}
		}
	}

	recordSet.Close();
	RequeryTestList(m_lDisplayModelID);			//Re Display Test Condition
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
	
}

//Step Delete
void CCTSEditorProView::OnStepDelete() 
{
	// TODO: Add your control notification handler code here
	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);

	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)	return;

	AddUndoStep();
	DeleteStep(awRows);
	Fun_UpdateMultiGroupList();
	Fun_UpdateAccGroupList();
}

//Battery Model Save
void CCTSEditorProView::OnModelSave() 
{
	// TODO: Add your control notification handler code here
	ModelSave();
}

//시험 목록 저장
void CCTSEditorProView::OnTestSave() 
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}
	// TODO: Add your control notification handler code here
	CString		strTemp, strState;
	CTestListRecordSet	recordSet;
	ROWCOL nCol, nRow = m_wndTestListGrid.GetRowCount();
	
	if(nRow <1)		return;
	
	int nLastEditedModel =0;

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	for(ROWCOL i = 0; i< nRow; i++)
	{
		strState = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_EDIT_STATE);
//		if(strState == CS_SAVED)	continue;					//Saved Test Name		//2002 10/2 순서 저장을 위해 모두 재저장 

		strTemp = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NAME);
		if(strTemp.IsEmpty())	//Test Name is Empty
		{
			strTemp.Format(Fun_FindMsg("OnTestSave_msg2","IDD_CTSEditorPro_CYCL"), i+1);
			MessageBox(strTemp, Fun_FindMsg("OnTestSave_msg3","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
			continue;
		}

/*		strTemp = m_wndTestListGrid.GetValueRowCol(i+1, 5);
		if(strTemp.IsEmpty())
		{
			strTemp = "TestName = '" + strTemp +"'";
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Test Name Duplication
			{
				MessageBox("입력된 시험 조건명이 이미 존재 합니다.", "Test Name Error", MB_OK|MB_ICONSTOP);
				m_wndTestListGrid.SetCurrentCell(i+1, 1);
				recordSet.Close();
				return;
			}
*/		if(strState == CS_NEW)			
		{
			recordSet.AddNew();
		}
		else
		{
			strTemp = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY);
			if(strTemp.IsEmpty())
			{
				recordSet.Close();
				TRACE("Test Primary Key Empty");
				return;
			}
			strTemp.Format("TestID = %ld", atol((LPCSTR)(LPCTSTR)strTemp));
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Find Model Name Data
			{
				recordSet.Edit();
			}
			else
			{
				recordSet.Close();
				TRACE("Test Search Error");
				return;
			}
		}

		recordSet.m_TestNo		= atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NO)); 
		recordSet.m_ModelID		= m_lDisplayModelID;		//////초기화 문제
		recordSet.m_ProcTypeID = m_pTestTypeCombo->GetItemData(atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PROC_TYPE)));
		recordSet.m_TestName	= m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NAME);
		recordSet.m_Creator		= m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_CREATOR);
		recordSet.m_Description = m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_DESCRIPTION);
		recordSet.m_ModifiedTime.ParseDateTime(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_EDIT_TIME));
		recordSet.m_PreTestCheck = atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_RESET_PROC));
		recordSet.Update();
		nLastEditedModel = i+1;
	}
	

	//Update 이후 DB를 재검색하여 List를 Display 한다.

	recordSet.m_strFilter.Format("[ModelID] = %ld", m_lDisplayModelID);
	recordSet.m_strSort.Format("[TestNo]");

	int i =0;
	recordSet.Requery();
	if(!recordSet.IsBOF())
	{
		recordSet.MoveLast();
		i = recordSet.GetRecordCount();
		recordSet.MoveFirst();
	}
	nRow -= i;
	
	if(nRow>0)
	{
		m_wndTestListGrid.RemoveRows(i+1, i+nRow);		//Row가 RecordSet  보다 많으면 잉여 Row를 Delete
	}
	else if(nRow<0)
	{
		m_wndTestListGrid.InsertRows(i+1, -nRow);		//Row가 RescoedSet 보다 작으면 부족 Row Insert
	}
	
	nRow = 0;
	while(!recordSet.IsEOF())
	{	
		nRow++;
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(recordSet.m_ProcTypeID));
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), recordSet.m_TestName);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), recordSet.m_Creator);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), recordSet.m_Description);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), recordSet.m_ModifiedTime.Format());
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PRIMARY_KEY), recordSet.m_TestID);
		m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_SAVED);
	
		if(nRow == nLastEditedModel)				//Last Edited Battery Model is current selected Model
		{
			UpdateDspTest(recordSet.m_TestName,  recordSet.m_TestID);
			m_wndTestListGrid.SetCurrentCell(nRow, COL_TEST_PROC_TYPE);
		}
		recordSet.MoveNext();
	}
	
	recordSet.Close();
	strTemp.Format("%d", nRow);
	m_TotTestCount.SetText(strTemp);
	
	if(m_wndTestListGrid.GetCurrentCell(nRow, nCol))
	{
		if(nRow>0)
			GetDlgItem(IDC_LOAD_TEST)->EnableWindow(TRUE);
	}
	GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);		
	GetDocument()->m_bEditedFlag = TRUE;					//DataBase 변경 Flag (BackUp을 위해)

	UpdateData(FALSE);
}

//Step Save
void CCTSEditorProView::OnStepSave() 
{
	// TODO: Add your control notification handler code here
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}
	m_bFlagConfirmOK = FALSE;

	if (StepSave() == TRUE)
	{
	}

	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)

//	GetDlgItem(IDC_STEP_SAVE)->EnableWindow(FALSE);
}

void CCTSEditorProView::OnStepInsert() 
{
	// TODO: Add your control notification handler code here
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}

	ROWCOL nRow =0, nCol;
	
	if(m_wndStepGrid.GetRowCount() >0)
	{
		if(!m_wndStepGrid.GetCurrentCell(nRow, nCol))
		{
			AfxMessageBox(Fun_FindMsg("OnStepInsert_msg1","IDD_CTSEditorPro_CYCL"));
			return;
		}
	}
	
	AddUndoStep();
	AddStepNew(nRow, FALSE);
	Fun_UpdateMultiGroupList();
	Fun_UpdateAccGroupList();
}

//Step Grid의 입력값을 받아 들인다. 
BOOL CCTSEditorProView::UpdateStepGrid(int nStepNum)
{
//	static int nPrevStepNum = 0;
//	if(nPrevStepNum == nStepNum && nStepNum>0)	return TRUE;		//Update Display Does not Require

	//Display Step Data
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();		//Get Document point
	int nTotStepNum = pDoc->GetTotalStepNum();						//Get Total Step No.
	ROWCOL nRow = m_wndStepGrid.GetRowCount();						//Get Inputed Step No.
	ASSERT(nRow == nTotStepNum);
	ASSERT(nStepNum <= nTotStepNum);
	CString strTemp;
	STEP *pStep;
//	char chTemp;
	//UpdateData(TRUE);

	if(nStepNum > 0)	//UpDate 1 Step Grid
	{
		pStep = (STEP *)pDoc->GetStepData(nStepNum);
		ASSERT(pStep != NULL);
		double	dwValue =0.0;
		pStep->chStepNo = (char)atoi(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_NO));	//Step No.
		
		strTemp = m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_PROCTYPE);						//Step Type	
		if(!strTemp.IsEmpty())
//			pStep->nProcType = pDoc->GetProcID(atoi(strTemp));
			pStep->nProcType = m_pProcTypeCombo->GetItemData(atoi(strTemp));

		strTemp = m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_TYPE);						//Step Type	
		if(!strTemp.IsEmpty())
//			pStep->chType = GetStepTypeFromIndex((char)(atoi(strTemp)));
			pStep->chType = m_pStepTypeCombo->GetItemData(atoi(strTemp));
			
		strTemp = m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_MODE);
		//2014.09.02 PS_STEP_USER_MAP 추가함.
		if(!strTemp.IsEmpty() && pStep->chType != PS_STEP_PATTERN && pStep->chType != PS_STEP_USER_MAP){
			pStep->chMode = m_pStepModeCombo->GetItemData(atoi(strTemp));
		}

		//2014.09.02 PS_STEP_USER_MAP 추가함.
		if(pStep->chType == PS_STEP_PATTERN || pStep->chType == PS_STEP_USER_MAP)
		{
			pStep->fVref_Charge = (float)(pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))));
			pStep->fVref_DisCharge = (float)(pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_POWER))));
//			pStep->fIref = (float)(pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_RESISTANCE))));
		}
		else
		{
			pStep->fVref_Charge = (float)(pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_CHARGE_V))));	//	*V_UNIT_FACTOR);
			pStep->fVref_DisCharge = (float)(pDoc->VUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_DISCHARGE_V))));	//	*V_UNIT_FACTOR);
		
			if(pStep->chMode == PS_MODE_CP)		//ljb		pStep->fIref 저장
			{
				pStep->fPref = (float)(pDoc->WattUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_POWER))));	//	*I_UNIT_FACTOR);	//ljb
			}
			else if(pStep->chMode == PS_MODE_CCCP)		//ljb		pStep->fIref 저장
			{
				pStep->fIref = (float)(pDoc->IUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))));	//	*I_UNIT_FACTOR);
				pStep->fPref = (float)(pDoc->WattUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_POWER))));	//	*I_UNIT_FACTOR);	//ljb
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				//Ohm단위로 입력 받아서 mOhm으로 저장한다.
				//pStep->fRref = (float)(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_RESISTANCE))*1000.0f);	//	*I_UNIT_FACTOR);
				pStep->fRref = (float)(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_RESISTANCE)));	//	*I_UNIT_FACTOR);
			}
			else
			{
				//pStep->fIref = (float)(pDoc->IUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))));	//	*I_UNIT_FACTOR);
				pStep->fIref = (float)(pDoc->IUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_REF_I))));	//	*I_UNIT_FACTOR);
				if (pStep->chType == PS_STEP_EXT_CAN)	//ljb 2011318 이재복 //////////
				{
					pStep->fPref = (float)(pDoc->WattUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_POWER))));	//	*I_UNIT_FACTOR);	//ljb
				}

			}
		}
		//ljb 챔버 
		pStep->fTref = (float)(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_CHAMBER_TEMP)));	//	
		pStep->fHref = (float)(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_CHAMBER_HUMI)));	//	

		//khs 20081113
//		strTemp = atoi(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_RANGE));
//		pStep->lRange = m_pStepRangeCombo->GetItemData(atoi(strTemp));
		pStep->lRange = atoi(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_RANGE));
		
		m_VtgHigh.GetValue(dwValue);	pStep->fVLimitHigh = (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);
		m_VtgLow.GetValue(dwValue); 	pStep->fVLimitLow= (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);
		m_CrtHigh.GetValue(dwValue);	pStep->fILimitHigh = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);
		m_CrtLow.GetValue(dwValue);		pStep->fILimitLow = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);
		m_CapHigh.GetValue(dwValue);	pStep->fCLimitHigh = (float)pDoc->CUnitTrans(dwValue);		//(dwValue*C_UNIT_FACTOR);
		m_CapLow.GetValue(dwValue);		pStep->fCLimitLow = (float)pDoc->CUnitTrans(dwValue);		//(dwValue*C_UNIT_FACTOR);
		m_ImpHigh.GetValue(dwValue);	pStep->fImpLimitHigh = (float)(dwValue*Z_UNIT_FACTOR);
		m_ImpLow.GetValue(dwValue);		pStep->fImpLimitLow = (float)(dwValue*Z_UNIT_FACTOR);
		m_CapacitanceHigh.GetValue(dwValue);	pStep->fCapacitanceHigh = (float)pDoc->CUnitTrans(dwValue);		//(dwValue*C_UNIT_FACTOR);
		m_CapacitanceLow.GetValue(dwValue);		pStep->fCapacitanceLow = (float)pDoc->CUnitTrans(dwValue);		//(dwValue*C_UNIT_FACTOR);
		m_DeltaI.GetValue(dwValue);		pStep->fDeltaI = (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);
		m_DeltaV.GetValue(dwValue);		pStep->fDeltaV = (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);
		m_TempHigh.GetValue(dwValue);	pStep->fTempHigh = (float)(dwValue);			
		m_TempLow.GetValue(dwValue);	pStep->fTempLow = (float)(dwValue);	

		dwValue =0.0;
		m_SafetyCellDeltaVStep.GetValue(dwValue); 	pStep->fCellDeltaVStep = (float)pDoc->VUnitTrans(dwValue); //(dwValue*V_UNIT_FACTOR);//yulee 20190531_3
		m_StepDeltaAuxTemp.GetValue(dwValue);		pStep->fStepDeltaAuxTemp = (float)pDoc->VUnitTrans(dwValue);
		m_StepDeltaAuxTH.GetValue(dwValue);			pStep->fStepDeltaAuxTh = (float)pDoc->VUnitTrans(dwValue);
		m_StepDeltaAuxT.GetValue(dwValue);			pStep->fStepDeltaAuxT = (float)pDoc->VUnitTrans(dwValue);
		m_StepDeltaAuxVTime.GetValue(dwValue);		pStep->fStepDeltaAuxVTime = (float)dwValue*100;
		m_StepDeltaAuxV.GetValue(dwValue);			pStep->fStepAuxV = (float)pDoc->VUnitTrans(dwValue);

		pStep->bStepDeltaVentAuxV = m_bChkStepDeltaVentAuxV.GetCheck();
		pStep->bStepDeltaVentAuxTemp = m_bChkStepDeltaVentAuxTemp.GetCheck();
		pStep->bStepDeltaVentAuxTh = m_bChkStepDeltaVentAuxTh.GetCheck();
		pStep->bStepDeltaVentAuxT = m_bChkStepDeltaVentAuxT.GetCheck();
		pStep->bStepVentAuxV = m_bChkStepVentAuxV.GetCheck();

		//End 전압/전류 비교 
		m_VtgEndHigh.GetValue(dwValue);	pStep->fVEndHigh = (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);
		m_VtgEndLow.GetValue(dwValue); 	pStep->fVEndLow= (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);
		m_CrtEndHigh.GetValue(dwValue);	pStep->fIEndHigh = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);
		m_CrtEndLow.GetValue(dwValue);	pStep->fIEndLow = (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);

		//EDLC 용량 측정 전압 V1, V2
		m_CapVtgLow.GetValue(dwValue);	pStep->fCapaVoltage1 = (float)pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);
		m_CapVtgHigh.GetValue(dwValue);	pStep->fCapaVoltage2 = (float)pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);

		m_ReportV.GetValue(dwValue);	pStep->fReportV = (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);
		m_ReportI.GetValue(dwValue); 	pStep->fReportI= (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);
		m_ReportTemp.GetValue(dwValue); 	pStep->fReportTemp= (float)(dwValue);
		
		COleDateTime time;
	//	time.SetTime(0,0,1);
		m_ReportTime.GetTime(time);
		//m_ReportTime.SetTime(0,0,1);
		pStep->ulReportTime = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;		//저장 간격 설정 시간 
		GetDlgItem(IDC_REPORT_TIME_MILI)->GetWindowText(strTemp);
		pStep->ulReportTime += atol(strTemp)/10;


		m_DeltaTime.GetTime(time);
		pStep->lDeltaTime = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;
		for(int i =0; i<SCH_MAX_COMP_POINT; i++)
		{
			m_CompV[i].GetValue(dwValue);		pStep->fCompVLow[i] = (float)pDoc->VUnitTrans(dwValue);			//(dwValue*V_UNIT_FACTOR);		//전압 하한값 
			m_CompV1[i].GetValue(dwValue);		pStep->fCompVHigh[i] = (float)pDoc->VUnitTrans(dwValue);		//(dwValue*V_UNIT_FACTOR);		//전압 상한값
			m_CompI[i].GetValue(dwValue);		pStep->fCompILow[i] = (float)pDoc->IUnitTrans(dwValue);			//(dwValue*I_UNIT_FACTOR);		//전류 하한값
			m_CompI1[i].GetValue(dwValue);		pStep->fCompIHigh[i] = (float)pDoc->IUnitTrans(dwValue);		//(dwValue*I_UNIT_FACTOR);		//전류 상한값
			
			time = m_CompTimeV[i].GetDateTime();
			pStep->ulCompTimeV[i] = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;	//전압 시간 
			time = m_CompTimeI[i].GetDateTime();
			pStep->ulCompTimeI[i] = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;	//전류 시간 

			//전압 비교 시간이 설정되어 있지 않으며 전압 입력 상하한 값을 무시한다.
			if(pStep->ulCompTimeV[i] == 0)
			{
				pStep->fCompVLow[i] = 0.0f;
				pStep->fCompVHigh[i] = 0.0f;
			}

			//전류 비교 시간이 설정되어 있지 않으며 전류 입력 상하한 값을 무시한다.
			if(pStep->ulCompTimeI[i] == 0)
			{
				pStep->fCompILow[i] = 0.0f;
				pStep->fCompIHigh[i] = 0.0f;
			}
			
			//전압상한 하한이 설정되지 않았을 경우 시간 입력을 무시한다.
			if(pStep->fCompVLow[i] == 0.0f && pStep->fCompVHigh[i] == 0.0f)
			{
				pStep->ulCompTimeV[i] = 0;
			}

			//전류상한 하한이 설정되지 않았을 경우 시간 입력을 무시한다.
			if(pStep->fCompILow[i] == 0.0f && pStep->fCompIHigh[i] == 0.0f)
			{
				pStep->ulCompTimeI[i] = 0;
			}
			//kjh 
		}
		//ljb v1009 MultiGroup and AccGroup ///////////////////////////////////////////////////////////////////
		switch (pStep->nMultiLoopGroupID)
		{
		case 0:
			pStep->nMultiLoopInfoCycle = 0;
			pStep->nMultiLoopInfoGotoStep = 0;
			break;
		case 1:
			pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[0];
			pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[0];
			break;
		case 2:
			pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[1];
			pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[1];
			break;
		case 3:
			pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[2];
			pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[2];
			break;
		case 4:
			pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[3];
			pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[3];
			break;
		case 5:
			pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[4];
			pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[4];
			break;
		}

		switch (pStep->nAccLoopGroupID)
		{
		case 0:
			pStep->nAccLoopInfoCycle = 0;
			pStep->nAccLoopInfoGotoStep = 0;
			break;
		case 1:
			pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[0];
			pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[0];
			break;
		case 2:
			pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[1];
			pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[1];
			break;
		case 3:
			pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[2];
			pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[2];
			break;
		case 4:
			pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[3];
			pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[3];
			break;
		case 5:
			pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[4];
			pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[4];
			break;
		}
		//////////////////////////////////////////////////////////////////////////
		UpdateStepGrade(nStepNum);
	}
	else	//Update All Step Data
	{
		for(int i = 0; i<nTotStepNum; i++)
		{
			pStep = (STEP *)pDoc->GetStepData(i+1);
			if(!pStep)	return FALSE;
			
			pStep->chStepNo = (char)atoi(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_NO));
			pStep->chType = (char)atoi(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_TYPE));
			pStep->chMode = (char)atoi(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_MODE));
			
			//2014.09.02 PS_STEP_USER_MAP 추가함.
			if(pStep->chType == PS_STEP_PATTERN || pStep->chType == PS_STEP_USER_MAP)
			{
				pStep->fVref_Charge = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_CHARGE_V));
				pStep->fVref_DisCharge = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_DISCHARGE_V));
			}
			else
			{
				pStep->fVref_Charge = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_CHARGE_V));
				pStep->fVref_DisCharge = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_DISCHARGE_V));

				if(pStep->chMode == PS_MODE_CP)		//ljb pStep->fIrf 저장
					pStep->fPref = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_POWER));
				else if(pStep->chMode == PS_MODE_CCCP)		//ljb pStep->fIrf 저장
				{
					pStep->fIref = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_I));
					pStep->fPref = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_POWER));
				}
				else if(pStep->chMode == PS_MODE_CR)		//ljb
					pStep->fRref = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_RESISTANCE));
				else
				{
					//pStep->fIref = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_I));
					pStep->fIref = (float)atof(m_wndStepGrid.GetValueRowCol(i+1, COL_STEP_REF_I));
					if (pStep->chType == PS_STEP_EXT_CAN)	//ljb 2011318 이재복 //////////
					{
						pStep->fPref = (float)(pDoc->WattUnitTrans(atof(m_wndStepGrid.GetValueRowCol(nStepNum, COL_STEP_POWER))));	//	*I_UNIT_FACTOR);	//ljb
					}
				}
			}
		//ljb v1009 AccGroup ///////////////////////////////////////////////////////////////////
			if (pStep->chType == PS_STEP_LOOP)
			{
				//ljb v1009 AccGroup ///////////////////////////////////////////////////////////////////
				switch (pStep->nMultiLoopGroupID)
				{
				case 0:
					pStep->nMultiLoopInfoCycle = 0;
					pStep->nMultiLoopInfoGotoStep = 0;
					break;
				case 1:
					pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[0];
					pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[0];
					break;
				case 2:
					pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[1];
					pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[1];
					break;
				case 3:
					pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[2];
					pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[2];
					break;
				case 4:
					pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[3];
					pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[3];
					break;
				case 5:
					pStep->nMultiLoopInfoCycle = m_nMultiGroupLoopInfoCycle[4];
					pStep->nMultiLoopInfoGotoStep = m_nMultiGroupLoopInfoGoto[4];
					break;
				}
				switch (pStep->nAccLoopGroupID)
				{
				case 0:
					pStep->nAccLoopInfoCycle = 0;
					pStep->nAccLoopInfoGotoStep = 0;
					break;
				case 1:
					pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[0];
					pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[0];
					break;
				case 2:
					pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[1];
					pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[1];
					break;
				case 3:
					pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[2];
					pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[2];
					break;
				case 4:
					pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[3];
					pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[3];
					break;
				case 5:
					pStep->nAccLoopInfoCycle = m_nAccGroupLoopInfoCycle[4];
					pStep->nAccLoopInfoGotoStep = m_nAccGroupLoopInfoGoto[4];
					break;
				}
			}
			
		}
		Fun_UpdateMultiGroupList();
		Fun_UpdateDisplayMultiGroup();
		Fun_UpdateAccGroupList();
		Fun_UpdateDisplayAccGroup();
		//////////////////////////////////////////////////////////////////////////
	}
	return TRUE;
}

//Save Left Step Data 
LRESULT CCTSEditorProView::OnLeftCell(WPARAM wParam, LPARAM lParam)
{
/*	CMyGridWnd *pGrid = (CMyGridWnd *)lParam;

	ROWCOL nRow, nCol;			//Check Row, Col
	nCol = wParam & 0x0000ffff;	nRow = wParam >> 16;
	

	if(nRow <= 0 || nCol <=0 )	return FALSE;	//Row, Column Check
	if(pGrid != (CMyGridWnd *)&m_wndStepGrid)	return FALSE;	//Check Step Grid

	if(!UpdateStepGrid(nRow))						//Update Step Data
	{
		AfxMessageBox("Step Data Saving Error");
	}
*/	return TRUE;
}

//Step의 Grade 값을 Grade Grid에 Display 한다.
int CCTSEditorProView::DisplayStepGrade(STEP *pStep)
{
	//reset Grid
	int n;
	for ( n = 1; n <= m_wndGradeGrid.GetRowCount(); n++ )
	{
 		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_ITEM1_), "");
 		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_ITEM2_), "");
 		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_RELATION_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MIN1_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MAX1_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MIN2_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_MAX2_), "");
		m_wndGradeGrid.SetValueRange(CGXRange(n, _COL_GRADE_CODE_), "");
	}

	if(pStep == NULL)	//0으로 표시
	{
		m_GradeCheck.SetCheck(FALSE);
		return 0;
	}

	//Grad에 보여 주는 단위 설정
	char str[32], str2[7], str3[5];

	GRADE sGrade;
	ZeroMemory(&sGrade,sizeof(GRADE));
	memcpy(&sGrade, &pStep->sGrading_Val, sizeof(GRADE));	
//	char szTemp1[36], szTemp2[36];
	float fVal1, fVal2;
	CString		strCode;
	int iGradCnt=0;
	if (sGrade.iGradeCount == 2)
		iGradCnt = (int)sGrade.chTotalGrade / 2;

	for(int i =0; i<(int)sGrade.chTotalGrade; i++)
	{
		strCode.Format("%C", sGrade.aszGradeCode[i]);
		if (i < iGradCnt)
		{
			//항목 표시
			m_wndGradeGrid.SetStyleRange(CGXRange(i+1,_COL_ITEM1_),
				CGXStyle().SetValue(sGrade.lGradeItem[i]));
			m_wndGradeGrid.SetValueRange(CGXRange(i+1, _COL_GRADE_CODE_), strCode);
			//항목 1
			switch((int)pStep->chType)
			{
			case PS_STEP_CHARGE:
			case PS_STEP_DISCHARGE:
				switch (int(sGrade.lGradeItem[i]))
				{
				case PS_GRADE_VOLTAGE:
					fVal1 = GetDocument()->VUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->VUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetVtgUnit() == "V")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CAPACITY:
					fVal1 = GetDocument()->CUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->CUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCapUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCapUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCapUnit() == "C")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CURRENT:
					fVal1 = GetDocument()->IUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->IUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCrtUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCrtUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCrtUnit() == "A")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_TIME:
					fVal1 = sGrade.faValue1[i]/T_UNIT_FACTOR;
					fVal2 = sGrade.faValue2[i]/T_UNIT_FACTOR;
					sprintf(str, "%%.%dlf", 1);	//소숫점 이하
					sprintf(str3, "%d", 1);		//소숫점 이하
					sprintf(str2, "%d", 6);		//정수부
					break;
				default:
					fVal1 = sGrade.faValue1[i];
					fVal2 = sGrade.faValue2[i];
					sprintf(str, "%%.%dlf", 3);	//소숫점 이하
					sprintf(str3, "%d", 3);		//소숫점 이하
					sprintf(str2, "%d", 3);		//정수부
					break;
				}
				break;
			case PS_STEP_IMPEDANCE:
				fVal1 = (double)sGrade.faValue1[i]/Z_UNIT_FACTOR;
				fVal2 = (double)sGrade.faValue2[i]/Z_UNIT_FACTOR;
				sprintf(str, "%%.%dlf", 3);	//소숫점 이하
				sprintf(str3, "%d", 3);		//소숫점 이하
				sprintf(str2, "%d", 3);		//정수부
				break;
			case PS_STEP_OCV:
				fVal1 = GetDocument()->VUnitTrans(sGrade.faValue1[i], FALSE);
				fVal2 = GetDocument()->VUnitTrans(sGrade.faValue2[i], FALSE);
				sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
				sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
				
				if(GetDocument()->GetVtgUnit() == "V")
					sprintf(str2, "%d", 3);		//정수부
				else
					sprintf(str2, "%d", 6);		//정수부
				break;
			case PS_STEP_REST:
			case PS_STEP_END:
			default:
				fVal1 = 0.0f;
				fVal2 = 0.0f;
				sprintf(str, "%%.%dlf", 3);	//소숫점 이하
				sprintf(str3, "%d", 3);		//소숫점 이하
				sprintf(str2, "%d", 3);		//정수부
				break;
			}
			m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(i+1, _COL_GRADE_MIN1_,
				i+1, _COL_GRADE_MAX1_),
				CGXStyle()
				.SetControl(IDS_CTRL_REALEDIT)
				.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
				.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
				.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			);
			m_wndGradeGrid.SetValueRange(CGXRange(i+1, _COL_GRADE_MIN1_), fVal1);
			m_wndGradeGrid.SetValueRange(CGXRange(i+1, _COL_GRADE_MAX1_), fVal2);
		}
		else
		{
			m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_ITEM2_),
				CGXStyle().SetValue(sGrade.lGradeItem[i]));
			if (strCode == '0')
			{
				m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_GRADE_RELATION_),
					CGXStyle().SetValue(0L));
			}
			if (strCode == '&')		// AND
				m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_GRADE_RELATION_),
				CGXStyle().SetValue(1L));
			if (strCode == '+')		// OR
				m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_GRADE_RELATION_),
				CGXStyle().SetValue(2L));
			switch((int)pStep->chType)
			{
			case PS_STEP_CHARGE:
			case PS_STEP_DISCHARGE:
				//항목2
				switch (int(sGrade.lGradeItem[i]))
				{
				case PS_GRADE_VOLTAGE:
					fVal1 = GetDocument()->VUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->VUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetVtgUnit() == "V")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CAPACITY:
					fVal1 = GetDocument()->CUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->CUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCapUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCapUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCapUnit() == "Ah")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_CURRENT:
					fVal1 = GetDocument()->IUnitTrans(sGrade.faValue1[i], FALSE);
					fVal2 = GetDocument()->IUnitTrans(sGrade.faValue2[i], FALSE);
					sprintf(str, "%%.%dlf", GetDocument()->GetCrtUnitFloat());	//소숫점 이하
					sprintf(str3, "%d", GetDocument()->GetCrtUnitFloat());		//소숫점 이하
					
					if(GetDocument()->GetCrtUnit() == "A")
						sprintf(str2, "%d", 3);		//정수부
					else
						sprintf(str2, "%d", 6);		//정수부
					break;
				case PS_GRADE_TIME:
					fVal1 = sGrade.faValue1[i];
					fVal2 = sGrade.faValue2[i];
					sprintf(str, "%%.%dlf", 3);	//소숫점 이하
					sprintf(str3, "%d", 3);		//소숫점 이하
					sprintf(str2, "%d", 3);		//정수부
					break;
				default:
					fVal1 = sGrade.faValue1[i];
					fVal2 = sGrade.faValue2[i];
					sprintf(str, "%%.%dlf", 3);	//소숫점 이하
					sprintf(str3, "%d", 3);		//소숫점 이하
					sprintf(str2, "%d", 3);		//정수부
					break;
				}
				break;
		case PS_STEP_IMPEDANCE:
		case PS_STEP_OCV:
		case PS_STEP_REST:
		case PS_STEP_END:
		default:
			fVal1 = 0.0f;
			fVal2 = 0.0f;
			sprintf(str, "%%.%dlf", 3);	//소숫점 이하
			sprintf(str3, "%d", 3);		//소숫점 이하
			sprintf(str2, "%d", 3);		//정수부
			break;
		}
		m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(i-iGradCnt+1, _COL_GRADE_MIN2_,
			i+1, _COL_GRADE_MAX2_),
			CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			);
		m_wndGradeGrid.SetStyleRange(CGXRange(i-iGradCnt+1,_COL_ITEM2_),
			CGXStyle().SetValue(sGrade.lGradeItem[i]));
		m_wndGradeGrid.SetValueRange(CGXRange(i-iGradCnt+1, _COL_GRADE_MIN2_), fVal1);
		m_wndGradeGrid.SetValueRange(CGXRange(i-iGradCnt+1, _COL_GRADE_MAX2_), fVal2);
		}
	}


	if((int)pStep->chType == PS_STEP_END || 
	   (int)pStep->chType == PS_STEP_REST ||
	   (int)pStep->chType == PS_STEP_LOOP ||
	   (int)pStep->chType == PS_STEP_ADV_CYCLE)
		//|| (int)pStep->chType == PS_STEP_SELECTING || (int)pStep->chType == PS_STEP_GRADING))
	{
//		m_GradeCheck.SetCheck(FALSE);
		m_GradeCheck.EnableWindow(FALSE);
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(FALSE));
	}
	else
	{
//		m_GradeCheck.EnableWindow(TRUE);
		m_GradeCheck.SetCheck(pStep->bGrade);
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
	}

	// 2008-11-20 ljb		Item1, Item2 비활성화
	//GRAY 설정
	if ( (int)pStep->chType == PS_STEP_OCV )
	{
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetValue((double)PS_GRADE_VOLTAGE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetEnabled(FALSE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_GRADE_RELATION_,
			GetDocument()->m_nGradingStepLimit, _COL_GRADE_MAX2_), CGXStyle().SetEnabled(FALSE));
	}
	else if ((int)pStep->chType == PS_STEP_IMPEDANCE)
	{
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetValue((double)PS_GRADE_IMPEDANCE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_ITEM1_,
			GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetEnabled(FALSE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_GRADE_RELATION_,
			GetDocument()->m_nGradingStepLimit, _COL_GRADE_MAX2_), CGXStyle().SetEnabled(FALSE));
	}
	UpdateData(FALSE);	

	return 0;

}

//Grade Grid의 값을 주어진 Grade Data로 Update한다. 
int CCTSEditorProView::UpdateStepGrade(int nStepNum)
{
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();		//Get Document point

	int nTotStepNum = pDoc->GetTotalStepNum();
	if(nTotStepNum <= 0)	return -1;
	if(nStepNum < 1 || nStepNum > nTotStepNum)		return -1;
	
	STEP *pStep;
		pStep = (STEP *)pDoc->GetStepData(nStepNum);
	if(!pStep)	return -1;
		
	CString	strItem1,strItem2;
	CString strTemp, strTemp1, strTemp2, strTemp3, strTemp4, strTemp5,strGrade;
	int iGradeCnt = 0;
	int iItem, nStepType, i;
	pStep->bGrade =  m_GradeCheck.GetCheck();
	if ( pStep->nProcType == 0 )
		nStepType = pStep->chType;
	else
		nStepType = HIWORD(pStep->nProcType);

	TRACE("%d",m_wndGradeGrid.GetRowCount());
	//1번째 항목
	for(int i = 0; i<m_wndGradeGrid.GetRowCount(); i++)
	{
		strTemp1 = strTemp2 = "";
		strTemp1 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MIN1_);		//Min Val
		strTemp2 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MAX1_);		//Max Val
		if (strTemp1.IsEmpty() || strTemp2.IsEmpty())
		{
			iGradeCnt=i;
			break;
		}
		pStep->sGrading_Val.faValue1[i] = (float)atof(strTemp1);
		pStep->sGrading_Val.faValue2[i] = (float)atof(strTemp2);
		strGrade = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_CODE_);		//Grade Code
		if(strGrade.IsEmpty())
		{
			strGrade.Format("%c", 'A'+i);
		}
		pStep->sGrading_Val.aszGradeCode[i] =  strGrade.GetAt(0);

		switch (nStepType)
		{
		case PS_STEP_CHARGE :
		case PS_STEP_DISCHARGE :
			iItem = atoi(m_wndGradeGrid.GetValueRowCol(i+1, _COL_ITEM1_));
			pStep->sGrading_Val.lGradeItem[i] = iItem;
			switch (iItem)
			{
			case PS_GRADE_VOLTAGE:
				pStep->sGrading_Val.faValue1[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CAPACITY:
				pStep->sGrading_Val.faValue1[i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CURRENT:
				pStep->sGrading_Val.faValue1[i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_TIME:
				pStep->sGrading_Val.faValue1[i] *= T_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] *= T_UNIT_FACTOR;
				break;
			default:
				pStep->sGrading_Val.faValue1[i] *= Z_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[i] *= Z_UNIT_FACTOR;
				break;
			}
			break;
		case PS_STEP_IMPEDANCE :
			pStep->sGrading_Val.lGradeItem[i] = PS_GRADE_IMPEDANCE;
			pStep->sGrading_Val.faValue1[i] *= Z_UNIT_FACTOR;
			pStep->sGrading_Val.faValue2[i] *= Z_UNIT_FACTOR;
			break;
		case PS_STEP_OCV :
			pStep->sGrading_Val.lGradeItem[i] = PS_GRADE_VOLTAGE;
			pStep->sGrading_Val.faValue1[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue1[i]);	//*= V_UNIT_FACTOR;
			pStep->sGrading_Val.faValue2[i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue2[i]);	//*= V_UNIT_FACTOR;
			break;
		default :
			pStep->sGrading_Val.faValue1[i]=0;
			pStep->sGrading_Val.faValue2[i]=0;
			pStep->sGrading_Val.lGradeItem[i] = 0;
			break;
		}
	}
	//2번째 항목
	for(int i = 0; i<iGradeCnt; i++)
	{
		strTemp1 = strTemp2 = "";
		strTemp1 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MIN2_);		//Min Val
		strTemp2 = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_MAX2_);		//Max Val
		pStep->sGrading_Val.faValue1[iGradeCnt+i] = (float)atof(strTemp1);
		pStep->sGrading_Val.faValue2[iGradeCnt+i] = (float)atof(strTemp2);

		switch (nStepType)
		{
		case PS_STEP_CHARGE :
		case PS_STEP_DISCHARGE :
			iItem = atoi(m_wndGradeGrid.GetValueRowCol(i+1, _COL_ITEM2_));
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = iItem;
			switch (iItem)
			{
			case PS_GRADE_VOLTAGE:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue1[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] = pDoc->VUnitTrans(pStep->sGrading_Val.faValue2[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CAPACITY:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue1[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] = pDoc->CUnitTrans(pStep->sGrading_Val.faValue2[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_CURRENT:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue1[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] = pDoc->IUnitTrans(pStep->sGrading_Val.faValue2[iGradeCnt+i]);	//*= V_UNIT_FACTOR;
				break;
			case PS_GRADE_TIME:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] *= Z_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] *= Z_UNIT_FACTOR;
				break;
			default:
				pStep->sGrading_Val.faValue1[iGradeCnt+i] *= Z_UNIT_FACTOR;
				pStep->sGrading_Val.faValue2[iGradeCnt+i] *= Z_UNIT_FACTOR;
				break;
			}
			break;
		case PS_STEP_IMPEDANCE :
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = PS_GRADE_IMPEDANCE;
			pStep->sGrading_Val.faValue1[iGradeCnt+i] *= 0;
			pStep->sGrading_Val.faValue2[iGradeCnt+i] *= 0;
			break;
		case PS_STEP_OCV :
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = PS_GRADE_VOLTAGE;
			pStep->sGrading_Val.faValue1[iGradeCnt+i] = 0;
			pStep->sGrading_Val.faValue2[iGradeCnt+i] = 0;
			break;
		default :
			pStep->sGrading_Val.faValue1[iGradeCnt+i]=0;
			pStep->sGrading_Val.faValue2[iGradeCnt+i]=0;
			pStep->sGrading_Val.lGradeItem[iGradeCnt+i] = 0;
			break;
		}
		strGrade="";
		strGrade = m_wndGradeGrid.GetValueRowCol(i+1, _COL_GRADE_RELATION_);		//Grade Code
		if(strGrade.IsEmpty())
		{
			strGrade.Format("%c", '0');
		}
		if(strGrade == "0")
		{
			strGrade.Format("%c", '0');
		}
		if(strGrade == "1")
		{
			strGrade.Format("%c", '&');
		}
		if(strGrade == "2")
		{
			strGrade.Format("%c", '+');
		}		
		pStep->sGrading_Val.aszGradeCode[iGradeCnt+i] =  strGrade.GetAt(0);
		pStep->sGrading_Val.iGradeCount=2;

	}

//		if(strTemp2.GetLength() >SCH_MAX_CODE_SIZE)	//Grade Code 길이 검사 
//		{
//			strTemp1 = strTemp2.Left(SCH_MAX_CODE_SIZE);
//			strTemp2 = strTemp1;
//		}
//		strcpy(pStep->sGrading_Val[0].aszGradeCode[nTotGrade], (LPCSTR)strTemp2);

	//Grading이 설정 되었으나 등급이 입력되어 있지 않은 경우 
	if(iGradeCnt < 1)
	{
		pStep->bGrade =  FALSE;
		m_GradeCheck.SetCheck(pStep->bGrade);
	}
	else
		pStep->sGrading_Val.chTotalGrade = iGradeCnt*2;

	return iGradeCnt*2;
}

//Grade Check 에 따라 Grade Grid의 상태 변경
void CCTSEditorProView::OnGradeCheck() 
{
	if(m_nDisplayStep < 0 || m_nDisplayStep >= GetDocument()->GetTotalStepNum())	return;
	STEP *pStep;
	pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
	
	if(!pStep)	return;
	pStep->bGrade =  m_GradeCheck.GetCheck();
	switch((int)pStep->chType)
	{
	case PS_STEP_CHARGE:
	case PS_STEP_DISCHARGE:
		//		bExGrade = TRUE;
		//20081208 KHS
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
		break;
	case PS_STEP_IMPEDANCE:
	case PS_STEP_OCV:
		m_wndGradeGrid.SetStyleRange(CGXRange(1, 1, GetDocument()->m_nGradingStepLimit, _COL_GRADE_CODE_), CGXStyle().SetEnabled(pStep->bGrade));
		//20081208 KHS		_COL_GRADE_RELATION_ ~ _COL_GRADE_MAX2_ 비활성화
		m_wndGradeGrid.SetStyleRange(CGXRange(1,_COL_ITEM1_, GetDocument()->m_nGradingStepLimit, _COL_ITEM1_), CGXStyle().SetEnabled(FALSE));
		m_wndGradeGrid.SetStyleRange(CGXRange(1, _COL_GRADE_RELATION_, GetDocument()->m_nGradingStepLimit, _COL_GRADE_MAX2_), CGXStyle().SetEnabled(FALSE));
		break;
	case PS_STEP_PATTERN:
	case PS_STEP_USER_MAP:  //2014.09.02 PS_STEP_USER_MAP 추가함
	case PS_STEP_EXT_CAN:	//ljb 2011318 이재복 //////////
	case PS_STEP_LOOP:
	case PS_STEP_ADV_CYCLE:
	case PS_STEP_REST:
	case PS_STEP_END:
	default:
		m_GradeCheck.SetCheck(FALSE);
		return;
	}
	
// 	TEST_PARAM *pParam;
// 	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);
// 	
// 	pParam->bPreTest = m_PreTestCheck.GetCheck();
// 	if(pParam->bPreTest)
// 	{
// 		pParam->fOCVLimitVal = GetDocument()->m_lMaxVoltage;
// 	}
// 	else
// 	{
// 		pParam->fOCVLimitVal = 0;
// 	}
// 	
// 	GetDlgItem(IDC_PRETEST_OCV)->EnableWindow(pParam->bPreTest);
// 	GetDlgItem(IDC_PRETEST_TRCKLE_I)->EnableWindow(pParam->bPreTest);
// 	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->EnableWindow(pParam->bPreTest);
	
	
// 	//CellCheck 방식 변경에 의해 이하 정보는 입력하지 않아도 됨 
// 	GetDlgItem(IDC_PRETEST_MAXV)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_MINV)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_DELTA_V)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_FAULT_NO)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_MAXI)->EnableWindow(FALSE);
	
	m_bStepDataSaved = FALSE;		//Step Data is Edited
	
	DisplayPreTestParam();
}

//PreTest  Check에 따라 입력 Control의 Enable/Disable
void CCTSEditorProView::OnPretestCheck() 
{
	// TODO: Add your control notification handler code here
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);

// 	pParam->bPreTest = m_PreTestCheck.GetCheck();
// 	if(pParam->bPreTest)
// 	{
// 		pParam->fOCVLimitVal = GetDocument()->m_lMaxVoltage;
// 	}
// 	else
// 	{
// 		pParam->fOCVLimitVal = 0;
// 	}

// 	GetDlgItem(IDC_PRETEST_OCV)->EnableWindow(pParam->bPreTest);
// 	GetDlgItem(IDC_PRETEST_TRCKLE_I)->EnableWindow(pParam->bPreTest);
// 	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->EnableWindow(pParam->bPreTest);


// 	//CellCheck 방식 변경에 의해 이하 정보는 입력하지 않아도 됨 
// 	GetDlgItem(IDC_PRETEST_MAXV)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_MINV)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_DELTA_V)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_FAULT_NO)->EnableWindow(FALSE);
// 	GetDlgItem(IDC_PRETEST_MAXI)->EnableWindow(FALSE);

	m_bStepDataSaved = FALSE;		//Step Data is Edited

	DisplayPreTestParam();
}

//PreTest Parameter의 값을 Display한다.
int CCTSEditorProView::DisplayPreTestParam()
{
	TEST_PARAM *pParam;
	CCTSEditorProDoc *pDoc = GetDocument();
	pParam = (TEST_PARAM *)(&pDoc->m_sPreTestParam);

	int nSetCanCount =0, nSetAuxCount =0;
	for (int i=0; i< MAX_COMPARE_STEP_COUNT; i++)
	{
		if (pParam->ican_function_division[i] > 0) nSetCanCount++;
		if (pParam->iaux_function_division[i] > 0) nSetAuxCount++;
	}
	CString strTemp;
	strTemp.Format(Fun_FindMsg("DisplayPreTestParam_msg1","IDD_CTSEditorPro_CYCL"),nSetCanCount,nSetAuxCount);
	strTemp.Replace("\\n","\n"); //ksj 20161210 - line change character fix
	GetDlgItem(IDC_BUT_SAFETY_SET_CAN_AUX)->SetWindowText(strTemp);

	m_SafetyMaxV.SetValue(double(pDoc->VUnitTrans(pParam->fMaxV, FALSE)));
	m_SafetyMinV.SetValue(double(pDoc->VUnitTrans(pParam->fMinV, FALSE)));
	m_SafetyMaxI.SetValue(double(pDoc->IUnitTrans(pParam->fMaxI, FALSE)));
	//yulee 20180731 
	int iTmp;
	iTmp = int(pDoc->IUnitTrans(pParam->fMaxI, FALSE))*1000;
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Max Current", iTmp);

	m_SafetyCellDeltaV.SetValue(double(pDoc->VUnitTrans(pParam->fCellDataV, FALSE)));		//ljb 20160414 edit IUnitTrans -> VUnitTrans
	m_StdDeltaMaxV.SetValue(double(pDoc->VUnitTrans(pParam->fStdMaxV, FALSE)));		//ljb 20160414 edit IUnitTrans -> VUnitTrans
	m_StdDeltaMinV.SetValue(double(pDoc->VUnitTrans(pParam->fStdMinV, FALSE)));		//ljb 20160414 edit IUnitTrans -> VUnitTrans
	m_DeltaMaxV.SetValue(double(pDoc->VUnitTrans(pParam->fDataMaxV, FALSE)));		//ljb 20160414 edit IUnitTrans -> VUnitTrans
	m_DeltaMinV.SetValue(double(pDoc->VUnitTrans(pParam->fDataMinV, FALSE)));		//ljb 20160414 edit IUnitTrans -> VUnitTrans
	m_SafetyMaxT.SetValue(double(pDoc->CUnitTrans(pParam->fMaxT, FALSE)));
	m_SafetyMinT.SetValue(double(pDoc->CUnitTrans(pParam->fMinT, FALSE)));	
	m_SafetyMaxC.SetValue(double(pDoc->CUnitTrans(pParam->fMaxC, FALSE)));		
	m_SafetyMaxW.SetValue(double(pDoc->WattUnitTrans(pParam->lMaxW, FALSE)));
	m_SafetyMaxWh.SetValue(double(pDoc->WattHourUnitTrans(pParam->lMaxWh, FALSE)));

	m_bChkDeltaVVent.SetCheck(pParam->bDataVent);

	int iUseVent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "No Use Vent", 0);
	int bUseVentSafety = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Use Vent Safety", 1); //ksj 20200226 : No Use Vent 항목 있는지 모르고 신규 추가했었음. 이쪽에도 조건 추가
	//if (iUseVent)
	if (iUseVent || !bUseVentSafety) //ksj 20200226		
	{
		m_bChkDeltaVVent.ShowWindow(SW_HIDE);
	}

 	CString strOut;
 	double	dwValue1 =0.0, dwValue2 =0.0;  m_StdDeltaMinV.GetValue(dwValue1); m_StdDeltaMaxV.GetValue(dwValue2);

 	CString strUnit;
 	strUnit.Format(_T("%s"), GetDocument()->GetVtgUnit()); //yulee 20190909
	if(strUnit.CompareNoCase(_T("V"))== 0)
	{
		strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg1","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
	}
	else
	{
		strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg2","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
	}
 	m_StaticCtrlSaftyCommonDelta.SetWindowTextA(strOut);


	/*int iCellDeltaV = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Cell Delta Volt", 500);	//ljb 20150731 add
	if (pParam->fCellDataV == 0)
	{
		m_SafetyCellDeltaV.SetValue(double(pDoc->VUnitTrans((float)iCellDeltaV, FALSE)));
	}*/

	/*m_SafetyCellStdDeltaMaxV.SetValue(double(pDoc->VUnitTrans(0,0f, FALSE)));
	m_SafetyCellStdDeltaMinV.SetValue(double(pDoc->VUnitTrans(0,0f, FALSE)));
	m_SafetyCellDeltaMaxV.SetValue(double(pDoc->VUnitTrans(0,0f, FALSE)));
	m_SafetyCellDeltaMinV.SetValue(double(pDoc->VUnitTrans(0,0f, FALSE)));*/

	return 0;
}

//PreTest Parameter의 값을 받아 들인다.
int CCTSEditorProView::UpdatePreTestParam(int nType)
{
	UpdateData();
	TEST_PARAM *pParam;
	CCTSEditorProDoc *pDoc = GetDocument();
	pParam = (TEST_PARAM*)(&pDoc->m_sPreTestParam);

	double	dwValue =0.0;
	BOOL bValue = FALSE;

	m_SafetyMaxV.GetValue(dwValue);		pParam->fMaxV = (float)pDoc->VUnitTrans(dwValue);
	m_SafetyMinV.GetValue(dwValue);		pParam->fMinV = (float)pDoc->VUnitTrans(dwValue);
	m_SafetyCellDeltaV.GetValue(dwValue);	pParam->fCellDataV = (float)pDoc->VUnitTrans(dwValue);
	m_StdDeltaMaxV.GetValue(dwValue);	pParam->fStdMaxV = (float)pDoc->VUnitTrans(dwValue);
	m_StdDeltaMinV.GetValue(dwValue);	pParam->fStdMinV = (float)pDoc->VUnitTrans(dwValue);
	m_DeltaMaxV.GetValue(dwValue);		pParam->fDataMaxV = (float)pDoc->VUnitTrans(dwValue);
	m_DeltaMinV.GetValue(dwValue);		pParam->fDataMinV = (float)pDoc->VUnitTrans(dwValue);
	bValue = m_bChkDeltaVVent.GetCheck();		pParam->bDataVent = (BOOL)bValue;
	m_SafetyMaxI.GetValue(dwValue);		pParam->fMaxI = (float)pDoc->IUnitTrans(dwValue);
	m_SafetyMaxT.GetValue(dwValue);		pParam->fMaxT = (float)dwValue*1000;
	m_SafetyMinT.GetValue(dwValue);		pParam->fMinT = (float)dwValue*1000;
	m_SafetyMaxC.GetValue(dwValue);		pParam->fMaxC = (float)pDoc->CUnitTrans(dwValue);
	m_SafetyMaxW.GetValue(dwValue);		pParam->lMaxW = (float)pDoc->WattUnitTrans(dwValue);
	m_SafetyMaxWh.GetValue(dwValue);	pParam->lMaxWh = (float)pDoc->WattHourUnitTrans(dwValue);

	CString strTemp, strTemp2;
	double dTemp;
	int lMaxVoltage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);
	int lMaxCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	dTemp = lMaxVoltage * 1.1;
	if (dTemp < pParam->fMaxV)
	{
		
		strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg1","IDD_CTSEditorPro_CYCL"),dTemp / 1000);
		MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		return -1;
	}

	//Max Current_Spec 추가 20180808 yulee
	float fMaxCurrentSpec = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current_Spec", 5000);

	if (m_bChkParallel.GetCheck())
	{
		float fltParallelNum;
		fltParallelNum = (float)m_CboParallelNum.GetCurSel()+2;//yulee 20180719
		dTemp =(int)((fMaxCurrentSpec/1000.0*fltParallelNum) * 1.10);
		int nTmp, nTmp2;
		nTmp = (int)dTemp;
		nTmp2 = (int)(pParam->fMaxI)/1000;
		if (nTmp < nTmp2)
		{
			strTemp2.Format("(Max Current_Spec(Registry)");
			//strTemp.Format("시험 안전조건 Max Current 값(max current_Spec 비교) 이상. (입력가능 max : %.3f A)",dTemp);
			strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg2","IDD_CTSEditorPro_CYCL"),dTemp, strTemp2);
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return -1;
		}
	}
	else
	{
		dTemp =(int)(fMaxCurrentSpec/1000.0 * 1.10);
		int nTmp, nTmp2;
		nTmp = (int)dTemp;
		nTmp2 = (int)(pParam->fMaxI)/1000;
		if (nTmp < nTmp2)
		{
			strTemp2.Format("(Max Current_Spec(Registry)");
			//strTemp.Format("시험 안전조건 Max Current 값(max current_Spec 비교) 이상. (입력가능 max : %.
			//strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg3, (Max Current_Spec(Registry)"),dTemp / 1000);
			strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg3","IDD_CTSEditorPro_CYCL"),dTemp, strTemp2);
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return -1;
		}
	}
	if (pParam->fCellDataV < 0 || pParam->fCellDataV > 10000)  
	{
		strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg4","IDD_CTSEditorPro_CYCL"));
		MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		return -1;
	}

// 	//Check time
// 	CString strTemp;
// 	GetDlgItem(IDC_CELL_CHECK_TIME_EDIT)->GetWindowText(strTemp);
// 	pParam->lTrickleTime = atoi(strTemp)*100;
// 
// 	//not use
// 	GetDlgItem(IDC_PRETEST_FAULT_NO)->GetWindowText(strTemp);
// 	pParam->nMaxFaultNo = atoi(strTemp);
// 	pParam->bPreTest = m_PreTestCheck.GetCheck();


	BOOL bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE); //yulee 20190712 김재호C 요청으로 과충방전만 -값 입력 가능

	if(bOverChargerSet == FALSE)
	{
		if(pParam->fMinV < 0)
		{
			strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg8","IDD_CTSEditorPro_CYCL"));
			AfxMessageBox(strTemp);
			return -1;
		}
	}

	// 211015 HKH 절연모드 충전 관련 기능 보완 ==============================
	BOOL bUseIsolationSafety = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Use Isolation Safety", 0);
	if(bUseIsolationSafety)
	{
		if (pParam->fMinV == 0)
		{
			strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg9","IDD_CTSEditorPro_CYCL"));
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return -1;
		}
	}
	// ======================================================================

	BOOL bUseCommonSafety = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Use Common Safety", 1); //ksj 20170815 : 서훈C 요청으로 안전 조건 옵션 처리
	if(bUseCommonSafety)
	{
		if (pParam->fMaxV == 0 || pParam->fMaxI == 0 )
		{
		strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg5","IDD_CTSEditorPro_CYCL"));
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return -1;
		}
		
		if (pParam->fMaxV == 0 && pParam->fMinV == 0 && pParam->fMaxI == 0 
			&& pParam->fMaxT == 0 && pParam->fMinT == 0 && pParam->fMaxC == 0 && pParam->lMaxW == 0 && pParam->lMaxWh == 0)
		{
		strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg6","IDD_CTSEditorPro_CYCL"));
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return -1;
		}
		else
		{
			if (pParam->fCellDataV == 0)
			{
			strTemp.Format(Fun_FindMsg("UpdatePreTestParam_msg7","IDD_CTSEditorPro_CYCL"));
				MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
				return -1;
			}
			//return 1;
		}		
	}

	BOOL bConfirm = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseConfirmCellDevVolt", FALSE);	//20170807  Cell 전압편차 확인 메시지 띄우기
	if ((bConfirm) && (m_bFlagConfirmOK == FALSE))
	{
		CString strUnit;
		strUnit.Format(_T("%s"), GetDocument()->GetVtgUnit()); //yulee 20190909
		m_bFlagConfirmOK = TRUE;//$1013
		//&& strTemp.Format("시험 안전조건 (최대 셀 전압편차) 값은 %.3f %s 로 설정 되었습니다.",pDoc->VUnitTrans(pParam->fCellDataV, FALSE), pDoc->GetVtgUnit());
		strTemp.Format(Fun_FindMsg("EditorView_UpdatePreTestParam_msg1","IDD_CTSEditorPro_CYCL"),
			pDoc->VUnitTrans(pParam->fStdMinV, FALSE),strUnit, 
			pDoc->VUnitTrans(pParam->fDataMinV, FALSE),strUnit, 
			pDoc->VUnitTrans(pParam->fStdMinV, FALSE),strUnit,
			pDoc->VUnitTrans(pParam->fStdMaxV, FALSE),strUnit,
			pDoc->VUnitTrans(pParam->fCellDataV, FALSE),strUnit,
			pDoc->VUnitTrans(pParam->fStdMaxV, FALSE), strUnit,
			pDoc->VUnitTrans(pParam->fDataMaxV, FALSE),strUnit
			);

		AfxMessageBox(strTemp);
	}

	/*if (pParam->fMaxV == 0 || pParam->fMaxI == 0 )
	{
		strTemp.Format("시험 안전조건이 설정되어 있지 않습니다. \n(최대전압과 최대전류를 입력하세요.)");
		MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		return -1;
	}

	if (pParam->fMaxV == 0 && pParam->fMinV == 0 && pParam->fMaxI == 0 
		&& pParam->fMaxT == 0 && pParam->fMinT == 0 && pParam->fMaxC == 0 && pParam->lMaxW == 0 && pParam->lMaxWh == 0)
	{
		strTemp.Format("시험 안전조건이 설정되어 있지 않습니다. \n(1개 이상 설정 하세요)");
		MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		return -1;
	}
	else
	{
		if (pParam->fCellDataV == 0) 
		{
			strTemp.Format("시험 안전조건 (최대 셀 전압편차) 값이 설정 되어 있지 않습니다.");
			MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
			return -1;
		}
		//return 1;
	}

	BOOL bConfirm = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseConfirmCellDevVolt", FALSE);	//20170807  Cell 전압편차 확인 메시지 띄우기
	if (bConfirm)
	{
		strTemp.Format("시험 안전조건 (최대 셀 전압편차) 값은 %.3f %s 로 설정 되었습니다.",pDoc->VUnitTrans(pParam->fCellDataV, FALSE), pDoc->GetVtgUnit());
		AfxMessageBox(strTemp);
	}*/
	return 1;
}

//Grid 입력 내용의 구별(Primary Key가 존재 하면 Edit, 존재 하지 않으면 New)
LRESULT CCTSEditorProView::OnStartEditing(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = wParam & 0x0000ffff;		nRow = wParam >> 16;	//Get Current Row Col
	if(nRow < 1 || nCol < 1)	return TRUE;

	CString strTemp;

	if(pGrid == (CMyGridWnd *)m_pWndCurModelGrid)
	{
		strTemp  = pGrid->GetValueRowCol(nRow, 3);
		if(!strTemp.IsEmpty())	//if Primary Key is Empty -> New Model
		{						
			m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_EDIT);
		}
		GetDlgItem(IDC_MODEL_SAVE)->EnableWindow(TRUE);
	}
	if(pGrid == (CMyGridWnd *)&m_wndTestListGrid)
	{
		strTemp  = pGrid->GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY);
		if(!strTemp.IsEmpty())	//if Primary Key is Empty -> New Model
		{						
			m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_EDIT);
		}
		GetDlgItem(IDC_TEST_SAVE)->EnableWindow(TRUE);
	}
	if(pGrid == (CMyGridWnd *)&m_wndStepGrid || pGrid == (CMyGridWnd *)&m_wndGradeGrid )
	{
		m_bStepDataSaved = FALSE;	//Step Data is Edited
	}
	
	return TRUE;
}

LRESULT CCTSEditorProView::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
/*	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;		//Get Grid	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);				//Get Current Row Col
	if(pGrid != &m_wndStepGrid)	return 0;
	if(nCol != COL_STEP_REF_V && nCol != COL_STEP_REF_I)	return 0;

	CCTSEditorProDoc *pDoc = GetDocument();
	ASSERT(pDoc);

	STEP *pStep = pDoc->GetStepData(nRow);
	if(pStep == NULL)	return 0;

	if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
	{
		if(m_bVHigh)
		{w
			pStep->fVLimitHigh = pStep->fVref + m_nVHighVal;
//			m_VtgHigh.SetValue((double)(pStep->fVref + m_nVHighVal));
		}
		if(m_bVLow)
		{
			pStep->fVLimitLow = m_nVLowVal;
//			m_VtgLow.SetValue((double)m_nVLowVal);
		}
		if(m_bIHigh)
		{
			m_CrtHigh.SetValue((double)(pStep->fIref + m_nIHighVal));
		}
		if(m_bILow)
		{
			m_CrtLow.SetValue((double)m_nILowVal);
		}
		if(m_bCHigh)
		{
			m_CapHigh.SetValue((double)m_nCHighVal);
		}
		if(m_bCLow)
		{
			m_CapLow.SetValue((double)m_nCLowVal);
		}
	}
	else if(pStep->chType == PS_STEP_IMPEDANCE && pStep->chMode == EP_MODE_DC_IMP)
	{
		if(m_bVHigh)
		{
			m_VtgHigh.SetValue((double)(pStep->fVref + m_nVHighVal));
		}
		if(m_bVLow)
		{
			m_VtgLow.SetValue((double)m_nVLowVal);
		}
		if(m_bIHigh)
		{
			m_CrtHigh.SetValue((double)(pStep->fIref + m_nIHighVal));
		}
		if(m_bILow)
		{
			m_CrtLow.SetValue((double)m_nILowVal);
		}
	}
		 
*/	return 0;
}

//Step Data를 DataBase에서 Load해서 Display 한다.
BOOL CCTSEditorProView::ReLoadStepData(LONG lTestID)
{
	int nStepNum;
	nStepNum = RequeryTestStep(m_lDisplayTestID);

	if(nStepNum < 0)
	{
		AfxMessageBox(Fun_FindMsg("ReLoadStepData_msg1","IDD_CTSEditorPro_CYCL"));
		Fun_UpdateMultiGroupList();
		Fun_UpdateAccGroupList();		//Acc Group 초기화
		return FALSE;
	}
	else
	{

		if(!DisplayStepGrid())
		{
			AfxMessageBox("Step Data Display Error");
		}
		
		//기본적으로 1번 Step Data를 표기 한다.
		if(nStepNum > 0)
			DisplayStepGrid(1);
		
		if(DisplayPreTestParam()<0)
		{
			AfxMessageBox("PreTest Parameter Display Error");
		}
	}
	return TRUE;
}

//Step Type에 따라 Grade Gride의 Real Editor Control을 수정 등록 한다.
void CCTSEditorProView::SettingGradeWndType(int nStepType, int nProcType)
{
	char str[32], str2[7], str3[5];
//	m_wndGradeGrid.SetValueRange(CGXRange(1, 1, MAX_GRADE, 2), "");
	switch(nStepType)
	{
	case PS_STEP_OCV:					//Voltage Grade	format *.***		
		if(nProcType <= 0)
		{
//			sprintf(str, "OCV(%s) Grading", GetDocument()->GetVtgUnit());
			sprintf(str, Fun_FindMsg("SettingGradeWndType_msg1","IDD_CTSEditorPro_CYCL"));
		}
		else
		{
//			sprintf(str, "OCV%d(%s) Grading", nProcType - (PS_STEP_OCV << 16)+1, GetDocument()->GetVtgUnit());
			sprintf(str, Fun_FindMsg("SettingGradeWndType_msg1","IDD_CTSEditorPro_CYCL"));
		}
		GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
		
		sprintf(str, "%%.%dlf", GetDocument()->GetVtgUnitFloat());	//소숫점 이하
		sprintf(str3, "%d", GetDocument()->GetVtgUnitFloat());		//소숫점 이하
		if(GetDocument()->GetVtgUnit() == "V")
		{
			sprintf(str2, "%d", 3);		//정수부

		}
		else
		{
			sprintf(str2, "%d", 6);		//정수부
		}
		m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(_COL_GRADE_MIN1_, _COL_GRADE_MAX1_),
			CGXStyle()
				.SetControl(IDS_CTRL_REALEDIT)
				.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
				.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
				.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);
		m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(_COL_GRADE_MIN2_, _COL_GRADE_MAX2_),
			CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			);

		break;
	case PS_STEP_PATTERN:
	case PS_STEP_USER_MAP:		//2014.09.02 PS_STEP_USER_MAP 추가함
	case PS_STEP_EXT_CAN:		//ljb 2011318 이재복 //////////
	case PS_STEP_CHARGE:		//Capacity or Impedance Grade format ****.*	
	case PS_STEP_DISCHARGE:
	case PS_STEP_IMPEDANCE:
		if(nStepType == PS_STEP_IMPEDANCE)
		{
// #ifdef _EDLC_CELL_
// 				GetDlgItem(IDC_GRADE_CHECK)->SetWindowText("ESR(mΩ) 등급");
// #else
// 				GetDlgItem(IDC_GRADE_CHECK)->SetWindowText("임피던스(mΩ) 등급");
// #endif
//20151110 delete			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(Fun_FindMsg("SettingGradeWndType_msg1"));
				GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(Fun_FindMsg("SettingGradeWndType_msg1","IDD_CTSEditorPro_CYCL"));
		}
		//2014.09.02 PS_STEP_USER_MAP추가함
		else if(nStepType == PS_STEP_PATTERN || nStepType ==  PS_STEP_USER_MAP)
		{
//			sprintf(str, "용량(%s)", GetDocument()->GetCapUnit());
			sprintf(str, Fun_FindMsg("SettingGradeWndType_msg1","IDD_CTSEditorPro_CYCL"));
//20151110 delete		GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
		}
		else
		{
// #ifdef _EDLC_CELL_
// 			sprintf(str, "용량(%s)＆ESR(mΩ)", GetDocument()->GetCapUnit());
// #else
// 			//삼화전기에만 적용
// //			sprintf(str, "용량(%s)＆임피던스(mΩ)", GetDocument()->GetCapUnit());
// 			sprintf(str, "용량(%s)", GetDocument()->GetCapUnit());
// #endif
			sprintf(str, Fun_FindMsg("SettingGradeWndType_msg1","IDD_CTSEditorPro_CYCL"));
//20151110 delete			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
			GetDlgItem(IDC_GRADE_CHECK)->SetWindowText(str);
		}
		sprintf(str, "%%.%dlf", GetDocument()->GetCapUnitFloat());	//소숫점 이하
		sprintf(str3, "%d", GetDocument()->GetCapUnitFloat());		//소숫점 이하
		
		if(GetDocument()->GetCrtUnit() == "A")
		{
			sprintf(str2, "%d", 4);		//정수부

		}
		else
		{
			sprintf(str2, "%d", 6);		//정수부
		}

		m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(_COL_GRADE_MIN1_, _COL_GRADE_MAX1_),
			CGXStyle()
				.SetControl(IDS_CTRL_REALEDIT)
				.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
				.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
				.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		);

		m_wndGradeGrid.SetStyleRange(CGXRange().SetCells(_COL_GRADE_MIN2_, _COL_GRADE_MAX2_),
			CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			);


		break;
	case PS_STEP_REST:
	case PS_STEP_END:
	default:
		m_wndGradeGrid.SetValueRange(CGXRange(2, 1, m_wndGradeGrid.GetRowCount(), _COL_GRADE_CODE_), "");
		break;
	}

}


//Step Type에 따라 Step Grid와 Extened Step Parameter Control 모양을 바꾼다.
//bGradeWnd == TRUE : 옵션 Control 및 Grade control까지 적용 한다.
//bGradeWnd == FALSE : Step gird만 적용 
void CCTSEditorProView::SettingStepWndType(int nStep, int nStepType, BOOL bGradeWnd )
{
	//m_wndStepGrid.LockUpdate();

	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_CYCLE), "");										//Step is Selected Step
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_TYPE), (LONG)GetStepIndexFromType(nStepType));		//Step is Selected Step	
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_MODE), (LONG)-1);									//Mode is Null
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V, nStep, COL_STEP_END), "");			

	SetStepCycleColumn();
	//m_wndChamberGrid.SetStyleRange(CGXRange(1, 1, 2, 2), CGXStyle().SetEnabled(FALSE));

	//Step Grid만 Update하면은 기타 조건들은 손대지 않는다.
	if(bGradeWnd)
	{
		GetDlgItem(IDC_VTG_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_VTG_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CRT_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_CRT_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CAP_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_CAP_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_IMP_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_IMP_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_TEMP_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELTA_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELTA_I)->EnableWindow(FALSE);
		GetDlgItem(IDC_DELTA_TIME)->EnableWindow(FALSE);

		GetDlgItem(IDC_CAPACITANCE_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_CAPACITANCE_LOW1)->EnableWindow(FALSE);


		GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(FALSE);
		GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(FALSE);
		GetDlgItem(IDC_REPORT_TIME)->EnableWindow(FALSE);
		//GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(FALSE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(FALSE);			

		GetDlgItem(IDC_CAP_VTG_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_CAP_VTG_HIGH)->EnableWindow(FALSE);

		GetDlgItem(IDC_EXT_OPTION_CHECK)->EnableWindow(FALSE);
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(FALSE);

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(FALSE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(FALSE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(FALSE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(FALSE);

	}

	STEP *pStep = GetDocument()->GetStepData(nStep);
	m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_PROCTYPE), (LONG)GetDocument()->GetProcIndex(pStep->nProcType));			//Procedure Type is Null
	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_CYCLE), CGXStyle().SetEnabled(TRUE));
	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_WAIT_TIME), CGXStyle().SetEnabled(TRUE));	//20160422 지성시간 white
	CGXRange covered;
	//2014.09.02 PS_STEP_USER_MAP추가함	
	if(nStepType != PS_STEP_PATTERN || nStepType != PS_STEP_USER_MAP)
	{
		//Covered cell을 해제 한다.
		if (m_wndStepGrid.GetCoveredCellsRowCol(nStep, COL_STEP_MODE, covered))
		{
			m_wndStepGrid.SetCoveredCellsRowCol(nStep, COL_STEP_MODE, nStep, COL_STEP_MODE);
		}
		
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),
				CGXStyle()
					.SetControl(GX_IDS_CTRL_ZEROBASED_EX2)
					.SetHorizontalAlignment(DT_LEFT)
			);
	}

	switch(nStepType)
	{
	case PS_STEP_CHARGE:
	case PS_STEP_DISCHARGE:	
		m_pStepModeCombo->ResetItemData();
		m_pStepModeCombo->SetItemData(0, PS_MODE_CCCV);
		m_pStepModeCombo->SetItemData(1, PS_MODE_CC);
		m_pStepModeCombo->SetItemData(2, PS_MODE_CV);

		#ifdef _CYCLER_
			m_pStepModeCombo->SetItemData(3, PS_MODE_CP);
			//m_pStepModeCombo->SetItemData(4, PS_MODE_CR); //ksj 20210610 : 주석처리
			//ljb 삭제 
			//m_pStepModeCombo->SetItemData(4, PS_MODE_CCCP);		//ljb 201012 울산 삼성 QC에서만 사용
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList(PS_CHARGE_MODE_STRING1));
		#else
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList(PS_CHARGE_MODE_STRING2));
		#endif
		
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_END), CGXStyle().SetEnabled(TRUE));
		m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_MODE), (LONG)GetModeIndex(pStep->chMode));
		//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RANGE), CGXStyle().SetChoiceList(PS_RANGE_STRING));

		//Aging 공정일 경우 전압 전류 설정을 하지 못함
		if(GetDocument()->m_lLoadedProcType == PS_PGS_AGING)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE), CGXStyle().SetEnabled(FALSE));
			if(nStepType == PS_STEP_DISCHARGE)
			{
				//방전에선 시간만 설정 가능 
				//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
			}
		}	
	
		
		if(pStep->chMode == PS_MODE_CC)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));		
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));	
		}
		else if(pStep->chMode == PS_MODE_CV)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));		
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
		}
		else if(pStep->chMode == PS_MODE_CP)
		{
 			//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
 			//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
			if (nStepType == PS_STEP_CHARGE) m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
			if (nStepType == PS_STEP_DISCHARGE) m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(TRUE));
		}
		else if(pStep->chMode == PS_MODE_CCCP)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(TRUE));
		}
		else if(pStep->chMode == PS_MODE_CR)
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(TRUE));
			if(nStepType == PS_STEP_CHARGE)
			{
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(TRUE));	//ljb for samsung
			}
			else
			{
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(TRUE));	//ljb for samsung
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
			}
		}
		else	//CC/CV
		{
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
		}

		
		if(bGradeWnd)
		{
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);

			//GetDlgItem(IDC_DELTA_V)->EnableWindow(TRUE);
			//GetDlgItem(IDC_DELTA_I)->EnableWindow(TRUE);
			
			GetDlgItem(IDC_DELTA_TIME)->EnableWindow(TRUE);

			
			GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
			GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseChamberTemp);		

			#ifdef _FORMATION_

				if(	GetDocument()->m_lLoadedProcType == PS_PGS_FORMATION 
					|| GetDocument()->m_lLoadedProcType == PS_PGS_FINALCHARGE 
					|| GetDocument()->m_lLoadedProcType == PS_PGS_PRECHARGE)
				{
					GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
					GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);
					#ifndef _EDLC_TEST_SYSTEM
						GetDlgItem(IDC_CAP_HIGH)->EnableWindow(TRUE);
						GetDlgItem(IDC_CAP_LOW)->EnableWindow(TRUE);
						GetDlgItem(IDC_CAPACITANCE_HIGH)->EnableWindow(TRUE);
						GetDlgItem(IDC_CAPACITANCE_LOW1)->EnableWindow(TRUE);
					#endif
				
					GetDlgItem(IDC_CAP_VTG_LOW)->EnableWindow(TRUE);
					GetDlgItem(IDC_CAP_VTG_HIGH)->EnableWindow(TRUE);

					m_GradeCheck.EnableWindow(TRUE);
					GetDlgItem(IDC_EXT_OPTION_CHECK)->EnableWindow(TRUE);
				}
				else
				{
					m_GradeCheck.EnableWindow(FALSE);
				}
				
				if(GetDocument()->m_lLoadedProcType == PS_PGS_AGING)
				{
					GetDlgItem(IDC_TEMP_LOW)->EnableWindow(TRUE);
					GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(TRUE);
				}
			#else
				GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_CAP_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_CAP_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_CAP_VTG_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_CAP_VTG_HIGH)->EnableWindow(TRUE);

				GetDlgItem(IDC_CAPACITANCE_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_CAPACITANCE_LOW1)->EnableWindow(TRUE);

				m_GradeCheck.EnableWindow(TRUE);
			#endif

			if(pStep->chMode == PS_MODE_CC)			//ljb	setting step wnd type
			{
				SetCompCtrlEnable(TRUE, FALSE);								//전압비교만 Enable 시킴 
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				SetCompCtrlEnable(FALSE, FALSE);							//전류비교만 Enable 시킴 
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I, nStep, COL_STEP_END), CGXStyle().SetEnabled(FALSE));
				SetCompCtrlEnable(TRUE, TRUE);								//전압/전류비교 모두 Enable 시킴 
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
			}
			else if(pStep->chMode == PS_MODE_CCCP)							//ljb 20101214
			{
				SetCompCtrlEnable(TRUE, TRUE);								//전압/전류비교 모두 Enable 시킴 
				//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
			}
			else if(pStep->chMode == PS_MODE_CCCV)
			{
				SetCompCtrlEnable(TRUE, TRUE);								//전압/전류비교 모두 Enable 시킴 
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				SetCompCtrlEnable(TRUE, TRUE);								//전압/전류비교 모두 Enable 시킴 
				//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));
				if(nStepType == PS_STEP_CHARGE)//lmh 20120622 add
				{
					m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
					m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(TRUE));	//ljb for samsung
				}
				else
				{
					m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(TRUE));	//ljb for samsung
					m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));	//ljb for samsung
				}
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER), CGXStyle().SetEnabled(FALSE));
				m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
			}
			else
			{
				SetCompCtrlEnable(FALSE, FALSE);							//모두 Disable 시킴 
			}
		}
		//ljb 20101130
		if (nStepType == PS_STEP_CHARGE) m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));
		if (nStepType == PS_STEP_DISCHARGE) m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(TRUE);//yulee 20190531_3

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(TRUE);
		break;

	case PS_STEP_REST:
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_CHAMBER_TEMP, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(TRUE));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END), CGXStyle().SetEnabled(TRUE));

		//m_wndChamberGrid.SetStyleRange(CGXRange(1, 1, 2, 1), CGXStyle().SetEnabled(TRUE));
	
		if(bGradeWnd)
		{
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);

			#ifdef _FORMATION_
				if(GetDocument()->m_lLoadedProcType == PS_PGS_AGING)
				{
					GetDlgItem(IDC_TEMP_LOW)->EnableWindow(TRUE);
					GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(TRUE);
				}
				else
				{
					GetDlgItem(IDC_TEMP_LOW)->EnableWindow(FALSE);
					GetDlgItem(IDC_TEMP_HIGH)->EnableWindow(FALSE);
				}
			#else

			//rest에서는 1sec이하 저장을 지원하지 않음 특정업체에서 수정 요청 SBC에서 업체별 지원 여부 처리 2009-03-23 ljb
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
			GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);

			#ifdef _PACK_MODEL_TYPE
				GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);
			#endif
			
			#endif

			GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseChamberTemp);		

			SetCompCtrlEnable(FALSE);
			m_GradeCheck.EnableWindow(FALSE);
			
		}
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(TRUE);

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(TRUE);
		break;
		
	case PS_STEP_IMPEDANCE:
		m_pStepModeCombo->ResetItemData();
		m_pStepModeCombo->SetItemData(0, PS_MODE_DCIMP);
		//m_pStepModeCombo->SetItemData(1, PS_MODE_ACIMP);		//ljb 20100928 AC Impedance 주석처리
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList(PS_IMPEDANCE_MODE_STRING));
		m_wndStepGrid.SetValueRange(CGXRange(nStep, COL_STEP_MODE), (LONG)GetModeIndex(pStep->chMode));

		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_CHAMBER_TEMP, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(FALSE));		
		if(pStep->chMode == PS_MODE_ACIMP)
		{
			//AC Imp 측정일 경우 제한치 입력 불가능 하도록 한다.
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V, nStep, COL_STEP_END), CGXStyle().SetEnabled(FALSE));
		}
		else
		{
			//Vref를 사용 못하도록 한다.
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_CHARGE_V, nStep, COL_STEP_REF_CHARGE_V), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_DISCHARGE_V, nStep, COL_STEP_REF_DISCHARGE_V), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_MODE), CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I, nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER, nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END, nStep, COL_STEP_END), CGXStyle().SetEnabled(TRUE));
		}

		if(bGradeWnd)
		{
			if(pStep->chMode == PS_MODE_DCIMP)
			{
				//DC Imp 측정일 경우 제한치 입력 가능 하도록 한다.
				GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);
				GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
				GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);
				
				GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
				GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
				GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
				GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseChamberTemp);		
				GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
				GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);
			}

		
			GetDlgItem(IDC_IMP_HIGH)->EnableWindow(TRUE);	
			GetDlgItem(IDC_IMP_LOW)->EnableWindow(TRUE);
			m_GradeCheck.EnableWindow(TRUE);			
			
			GetDlgItem(IDC_CAPACITANCE_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAPACITANCE_LOW1)->EnableWindow(TRUE);

			#ifdef _EDLC_CELL_
				GetDlgItem(IDC_CAP_VTG_LOW)->EnableWindow(TRUE); //ljb 20180313 김재호 차장 추가 요청
				GetDlgItem(IDC_CAP_VTG_HIGH)->EnableWindow(TRUE);   //ljb 20180313 김재호 차장 추가 요청
			#endif

			SetCompCtrlEnable(FALSE);
			
		}
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(TRUE); //yulee 20190531_3

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(TRUE);
		break;

	case PS_STEP_OCV:
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_END), CGXStyle().SetEnabled(FALSE));
		if(bGradeWnd)
		{
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);		//Use Ocv and Impedance Step
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);
			m_GradeCheck.EnableWindow(TRUE);			//Use Ocv and Impedance Step

			SetCompCtrlEnable(FALSE);
		}
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(TRUE); //yulee 20190531_3

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(TRUE);
		break;

	case PS_STEP_LOOP:
		{
			//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END), CGXStyle().SetEnabled(TRUE));
			
			if(bGradeWnd)
			{
				SetCompCtrlEnable(FALSE);
				m_GradeCheck.EnableWindow(FALSE);
			}
		}
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(FALSE); //yulee 20190531_3
		break;

	case PS_STEP_PATTERN:
		{
			//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE), CGXStyle().SetEnabled(TRUE));
			//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_V, nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(FALSE));
			//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END), CGXStyle().SetEnabled(TRUE));
			//+2015.9.21 USY Mod For PatternCV(김재호K)  일단 수정보류...
			//m_wndStepGrid.SetCoveredCellsRowCol(nStep, COL_STEP_MODE, nStep, COL_STEP_POWER);  //ljb 201012 COL_STEP_REF_I
			m_wndStepGrid.SetCoveredCellsRowCol(nStep, COL_STEP_MODE, nStep, COL_STEP_REF_DISCHARGE_V);  //ljb 201012 COL_STEP_REF_I
			//-
			m_pStepModeCombo->SetItemData(0, 0);
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList("").SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END),	CGXStyle().SetEnabled(TRUE));
 			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I, nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));
 			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(TRUE));		

			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),
				CGXStyle()
					.SetControl(GX_IDS_CTRL_HOTSPOT)
					.SetHorizontalAlignment(DT_LEFT)
					//.SetChoiceList(strMode)
			);
			
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);
			GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAP_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAP_LOW)->EnableWindow(TRUE);

			GetDlgItem(IDC_CAPACITANCE_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAPACITANCE_LOW1)->EnableWindow(TRUE);
			
			GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseChamberTemp);		
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
			GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);

			if(bGradeWnd)
			{
				SetCompCtrlEnable(FALSE);
				m_GradeCheck.EnableWindow(FALSE);
			}
		}
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(TRUE); //yulee 20190531_3

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(TRUE);
		break;
	//2014.09.02 PS_STEP_USER_MAP 추가함
	case PS_STEP_USER_MAP:
		{			
			m_wndStepGrid.SetCoveredCellsRowCol(nStep, COL_STEP_MODE, nStep, COL_STEP_REF_DISCHARGE_V);  //ljb 201012 COL_STEP_REF_I
			m_pStepModeCombo->SetItemData(0, 0);
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList("").SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END),	CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I, nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(TRUE));		
			
			
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),
				CGXStyle()
				.SetControl(GX_IDS_CTRL_HOTSPOT)
				.SetHorizontalAlignment(DT_LEFT)				
				);
			
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);
			GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAP_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAP_LOW)->EnableWindow(TRUE);
			
			GetDlgItem(IDC_CAPACITANCE_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAPACITANCE_LOW1)->EnableWindow(TRUE);
			
			GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseChamberTemp);		
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
			GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);
			
			if(bGradeWnd)
			{
				SetCompCtrlEnable(FALSE);
				m_GradeCheck.EnableWindow(FALSE);
			}
		}
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(TRUE); //yulee 20190531_3

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(TRUE);
		break;
	//ljb 2011318 이재복 ////////// s
	case PS_STEP_EXT_CAN:
		{
			m_pStepModeCombo->SetItemData(0, 0);
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE),	CGXStyle().SetChoiceList("").SetEnabled(FALSE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END),	CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_REF_I, nStep, COL_STEP_REF_I), CGXStyle().SetEnabled(TRUE));
			m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_POWER, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(TRUE));		
			
			GetDlgItem(IDC_VTG_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_VTG_LOW)->EnableWindow(TRUE);
			GetDlgItem(IDC_CRT_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CRT_LOW)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAP_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAP_LOW)->EnableWindow(TRUE);
			
			GetDlgItem(IDC_CAPACITANCE_HIGH)->EnableWindow(TRUE);
			GetDlgItem(IDC_CAPACITANCE_LOW1)->EnableWindow(TRUE);
			
			GetDlgItem(IDC_REPORT_VOLTAGE)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_CURRENT)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TIME)->EnableWindow(TRUE);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->EnableWindow(m_bUseChamberTemp);		
			GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(TRUE);
			GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(TRUE);
			
			if(bGradeWnd)
			{
				SetCompCtrlEnable(FALSE);
				m_GradeCheck.EnableWindow(FALSE);
			}
		}
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(TRUE); //yulee 20190531_3

		GetDlgItem(IDC_STEP_AUXT_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->EnableWindow(TRUE);
		break;
	//ljb 2011318 이재복 ////////// e
	case PS_STEP_END:		
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_END), CGXStyle().SetEnabled(FALSE));
 		break;
	case PS_STEP_ADV_CYCLE:
		//20090206 KKY////////////////////////////////////////////////////////////////////////////////////////////////
		//m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_RESISTANCE), CGXStyle().SetEnabled(FALSE));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_MODE, nStep, COL_STEP_CHAMBER_HUMI), CGXStyle().SetEnabled(FALSE));
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_END), CGXStyle().SetEnabled(TRUE));	
		break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	default:
		if(bGradeWnd)
		{
			SetCompCtrlEnable(FALSE);
			m_GradeCheck.EnableWindow(FALSE);	

		}
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_TYPE, nStep, COL_STEP_END), CGXStyle().SetEnabled(FALSE));
		break;
	}

	BOOL bCellBal=FALSE;
	if(nStepType==PS_STEP_REST) bCellBal=TRUE;
	if(nStepType==PS_STEP_CHARGE) bCellBal=TRUE;
	if(nStepType==PS_STEP_DISCHARGE) bCellBal=TRUE;
	if(nStepType==PS_STEP_PATTERN) bCellBal=TRUE;
	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_CELLBAL), 
	 	                        CGXStyle().SetEnabled(bCellBal));//cny 201809
	
	BOOL bPwrSupply=TRUE;	
	BOOL bChiller=TRUE; //ksj 20200901
	/*if(nStepType==PS_STEP_ADV_CYCLE) bPwrSupply=FALSE;	
	if(nStepType==PS_STEP_OCV)       bPwrSupply=FALSE;
	if(nStepType==PS_STEP_LOOP)      bPwrSupply=FALSE;
	if(nStepType==PS_STEP_END)       bPwrSupply=FALSE;
	if(nStepType==PS_STEP_CHARGE)       bPwrSupply=FALSE; //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
	if(nStepType==PS_STEP_DISCHARGE)       bPwrSupply=FALSE; //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
	if(nStepType==PS_STEP_PATTERN)       bPwrSupply=FALSE; //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
	if(nStepType==PS_STEP_EXT_CAN)       bPwrSupply=FALSE; //lyj 20200806 REST 스탭일 때만 파워서플라이 동작
	if(nStepType==PS_STEP_USER_MAP)       bPwrSupply=FALSE; //lyj 20200806 REST 스탭일 때만 파워서플라이 동작*/

	//ksj 20200901 : 칠러 활성화 여부 분리.
	if(nStepType==PS_STEP_ADV_CYCLE) {bPwrSupply=FALSE; bChiller=FALSE;};	
	if(nStepType==PS_STEP_OCV)       {bPwrSupply=FALSE; bChiller=FALSE;};	
	if(nStepType==PS_STEP_LOOP)      {bPwrSupply=FALSE; bChiller=FALSE;};	
	if(nStepType==PS_STEP_END)      {bPwrSupply=FALSE; bChiller=FALSE;};	
	if(nStepType==PS_STEP_CHARGE)       {bPwrSupply=FALSE; bChiller=TRUE;};	
	if(nStepType==PS_STEP_DISCHARGE)      {bPwrSupply=FALSE; bChiller=TRUE;};	
	if(nStepType==PS_STEP_PATTERN)      {bPwrSupply=FALSE; bChiller=TRUE;};	
	if(nStepType==PS_STEP_EXT_CAN)      {bPwrSupply=FALSE; bChiller=TRUE;};	
	if(nStepType==PS_STEP_USER_MAP)      {bPwrSupply=FALSE; bChiller=TRUE;};	

	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_PWRSUPPLY), 
		CGXStyle().SetEnabled(bPwrSupply));//cny 201809	
	
	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_CHILLER), 
		//CGXStyle().SetEnabled(bPwrSupply));//cny 201809
		CGXStyle().SetEnabled(bChiller));//ksj 20200901
	
	BOOL bPatTable=TRUE;	
	if(nStepType==PS_STEP_OCV)  bPatTable=FALSE;
	if(nStepType==PS_STEP_LOOP) bPatTable=FALSE;
	if(nStepType==PS_STEP_END)  bPatTable=FALSE;
	m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_PATTABLE), 
		CGXStyle().SetEnabled(bPatTable));//cny 201809

	SetApplyAllBtn(nStepType);
	
	//Step 공정 Type을 [관계없음]을 선택 하였을시 임의 Step 선택 가능하도록 한다. 
	if(pStep->nProcType == 0)
	{
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_TYPE), CGXStyle().SetEnabled(TRUE));
	}
	else
	{
		m_wndStepGrid.SetStyleRange(CGXRange(nStep, COL_STEP_TYPE), CGXStyle().SetEnabled(FALSE));
	}
	
	if(bGradeWnd)	SettingGradeWndType(nStepType, pStep->nProcType);		//Grade Wnd Setting

	//m_wndStepGrid.LockUpdate(FALSE);
	//m_wndStepGrid.Redraw();

	//ksj 20210610
	if(!m_bUseAuxV)
	{
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(FALSE);
		GetDlgItem(IDC_STEP_AUXV_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_STEP_AUXV_DELTA)->EnableWindow(FALSE);
	}

}

void CCTSEditorProView::UpdateDspModel(CString strName, LONG lID)
{
	m_lDisplayModelID = lID;
	m_strDspModelName = strName;
	
	//strTemp.Format("%s", m_strDspModelName);
	m_TestNameLabel.SetText(m_strDspModelName);

}

void CCTSEditorProView::UpdateDspTest(CString strName, LONG lID)
{
	m_lDisplayTestID = lID;
	m_strDspTestName = strName;

	if(GetDlgItem(IDC_LOAD_TEST)->m_hWnd && m_tooltip.m_hWnd)
		m_tooltip.AddTool(GetDlgItem(IDC_LOAD_TEST), m_strDspTestName + Fun_FindMsg("UpdateDspTest_msg1","IDD_CTSEditorPro_CYCL"));
}

//Battery Model Count에 따른 변경
void CCTSEditorProView::UpdateBatteryCountState(int nCount)
{
	GetDlgItem(IDC_TEST_NEW)->EnableWindow(nCount);
	GetDlgItem(IDC_TEST_DELETE)->EnableWindow(nCount);
	GetDlgItem(IDC_TEST_SAVE)->EnableWindow(FALSE);
	
	CString strTemp;
	strTemp.Format("%d", nCount);
	m_TotModelCount.SetText(strTemp);

	if(nCount>0)
	{
		strTemp.Format("%s", m_strDspModelName, m_lDisplayModelID);
		m_TestNameLabel.SetText(strTemp);
	}
	else
	{
		m_TestNameLabel.SetText(Fun_FindMsg("UpdateBatteryCountState_msg1","IDD_CTSEditorPro_CYCL"));
	}
}

//remove End Type Dlg
void CCTSEditorProView::HideEndDlg()
{
	if(m_pDlg)			//Delete Dlg 
	{	
		if(m_pDlg->m_hWnd)
		{
			m_pDlg->ShowWindow(SW_HIDE);
		}
	}
}


void CCTSEditorProView::HideCellBalDlg()
{
	if(m_pDlgCellBal)			
	{//Delete Dlg 	
		if(m_pDlgCellBal->m_hWnd)
		{			
			m_pDlgCellBal->ShowWindow(SW_HIDE);
		}
	}
}

void CCTSEditorProView::ShowCellBalDlg(int iType,STEP *pStep)
{
	if(iType==0)
	{
		m_pDlgCellBal = new Dlg_CellBal(this);
		ASSERT(m_pDlgCellBal);
		m_pDlgCellBal->m_pDoc = (CCTSEditorProDoc *)GetDocument();
		//m_pDlgCellBal->Create(IDD_DIALOG_CELLBAL, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgCellBal->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_CELLBAL): 
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_CELLBAL_ENG): 
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DIALOG_CELLBAL_PL): 
			IDD_DIALOG_CELLBAL, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgCellBal->GetClientRect(m_DlgCellBalRect);			
		return;
	}
	else
	{		
		if(!pStep) return;
		if(!m_pDlgCellBal) return;
	
		int nStep=pStep->chStepNo;
		m_pDlgCellBal->m_nStepNo =  nStep;
		m_pDlgCellBal->m_pStep = (STEP *)GetDocument()->GetStepData(nStep);

		CRect dlgRect,gridRect, cellRect,screenRect;
		m_pDlgCellBal->GetWindowRect(dlgRect);
		m_wndStepGrid.GetWindowRect(gridRect);//Get Step Grid Window Position
		//cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_END-3);
		cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_CYCLE);
		
		//////////////////////////////////////////////////////////////////////////
		//ljb 20090324	스텝 End Dialog Size 조절
		screenRect = cellRect;
		ClientToScreen(screenRect);
		int nTopPoint;
		if (screenRect.bottom+dlgRect.Height() > 800)
		{//End Dialog 화면 위로 올림
			nTopPoint = gridRect.top+20+3;
		}
		else
		{//그리드 아래에 End Dialog 표시
			nTopPoint = gridRect.top+cellRect.Height()+3;
		}
 		m_pDlgCellBal->MoveWindow( gridRect.left+cellRect.Width()+3, 
 								   nTopPoint,
 								   m_DlgCellBalRect.Width(), 
 								   m_DlgCellBalRect.Height());
		m_pDlgCellBal->ShowWindow(SW_SHOW);			
	}
}

void CCTSEditorProView::HidePwrSupplyDlg()
{
	if(m_pDlgPwrSupply)			 
	{//Delete Dlg	
		if(m_pDlgPwrSupply->m_hWnd)
		{
			m_pDlgPwrSupply->ShowWindow(SW_HIDE);
		}
	}
}

void CCTSEditorProView::ShowPwrSupplyDlg(int iType,STEP *pStep)
{
	if(iType==0)
	{
		m_pDlgPwrSupply = new Dlg_PwrSupply(this);
		ASSERT(m_pDlgPwrSupply);
		m_pDlgPwrSupply->m_pDoc = (CCTSEditorProDoc *)GetDocument();
		//m_pDlgPwrSupply->Create(IDD_DIALOG_PWRSUPPLY, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgPwrSupply->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_PWRSUPPLY): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_PWRSUPPLY_ENG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DIALOG_PWRSUPPLY_PL): //&&
			IDD_DIALOG_PWRSUPPLY, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgPwrSupply->GetClientRect(m_DlgPwrSupplyRect);			
		return;
	}
	else
	{		
		if(!pStep) return;
		if(!m_pDlgPwrSupply) return;
		
		int nStep=pStep->chStepNo;		
		m_pDlgPwrSupply->m_nStepNo =  nStep;
		m_pDlgPwrSupply->m_pStep = (STEP *)GetDocument()->GetStepData(nStep);				
		
		CRect dlgRect,gridRect, cellRect,screenRect;
		m_pDlgPwrSupply->GetWindowRect(dlgRect);
		m_wndStepGrid.GetWindowRect(gridRect);//Get Step Grid Window Position
		//cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_END-3);
		cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_CYCLE);
		
		//////////////////////////////////////////////////////////////////////////
		//ljb 20090324	스텝 End Dialog Size 조절
		screenRect = cellRect;
		ClientToScreen(screenRect);
		int nTopPoint;
		if (screenRect.bottom+dlgRect.Height() > 800)
		{//End Dialog 화면 위로 올림
			nTopPoint = gridRect.top+20+3;
		}
		else
		{//그리드 아래에 End Dialog 표시
			nTopPoint = gridRect.top+cellRect.Height()+3;
		}		
		m_pDlgPwrSupply->MoveWindow( gridRect.left+cellRect.Width()+3, 
									 nTopPoint,
									 m_DlgPwrSupplyRect.Width(), 
									 m_DlgPwrSupplyRect.Height());
		m_pDlgPwrSupply->ShowWindow(SW_SHOW);
	}
}

void CCTSEditorProView::HideChillerDlg()
{
	if(m_pDlgChiller)			 
	{//Delete Dlg	
		if(m_pDlgChiller->m_hWnd)
		{
			m_pDlgChiller->m_pStep=NULL;
			m_pDlgChiller->ShowWindow(SW_HIDE);
		}
	}
}

void CCTSEditorProView::ShowChillerDlg(int iType,STEP *pStep)
{
	if(iType==0)
	{
		m_pDlgChiller = new Dlg_Chiller(this);
		ASSERT(m_pDlgChiller);
		m_pDlgChiller->m_pDoc = (CCTSEditorProDoc *)GetDocument();
		//m_pDlgChiller->Create(IDD_DIALOG_CHILLER, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgChiller->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_CHILLER): 
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_CHILLER_ENG):
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DIALOG_CHILLER_PL):
			IDD_DIALOG_CHILLER, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgChiller->GetClientRect(m_DlgChillerRect);			
		return;
	}
	else
	{		
		if(!pStep) return;
		if(!m_pDlgChiller) return;
		
		int nStep=pStep->chStepNo;		
		m_pDlgChiller->m_nStepNo =  nStep;
		m_pDlgChiller->m_pStep = (STEP *)GetDocument()->GetStepData(nStep);				
		
		CRect dlgRect,gridRect, cellRect,screenRect;
		m_pDlgChiller->GetWindowRect(dlgRect);
		m_wndStepGrid.GetWindowRect(gridRect);//Get Step Grid Window Position
		//cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_END-3);
		cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_CYCLE);
		
		//////////////////////////////////////////////////////////////////////////
		//ljb 20090324	스텝 End Dialog Size 조절
		screenRect = cellRect;
		ClientToScreen(screenRect);
		int nTopPoint;
		if (screenRect.bottom+dlgRect.Height() > 800)
		{//End Dialog 화면 위로 올림
			nTopPoint = gridRect.top+20+3;
		}
		else
		{//그리드 아래에 End Dialog 표시
			nTopPoint = gridRect.top+cellRect.Height()+3;
		}		
		m_pDlgChiller->MoveWindow( gridRect.left+cellRect.Width()+3, 
								   nTopPoint,
								   m_DlgChillerRect.Width(), 
								   m_DlgChillerRect.Height());
		m_pDlgChiller->ShowWindow(SW_SHOW);			
	}
}

void CCTSEditorProView::HidePattSchedDlg()
{
	if(m_pDlgPattSched)			 
	{//Delete Dlg	
		if(m_pDlgPattSched->m_hWnd)
		{
			m_pDlgPattSched->m_pStep=NULL;
			m_pDlgPattSched->ShowWindow(SW_HIDE);
		}
	}
}

void CCTSEditorProView::ShowPattSchedDlg(int iType,STEP *pStep)
{
	if(iType==0)
	{
		m_pDlgPattSched = new Dlg_PattSched(this);
		ASSERT(m_pDlgPattSched);
		m_pDlgPattSched->m_pDoc = (CCTSEditorProDoc *)GetDocument();
		//m_pDlgPattSched->Create(IDD_DIALOG_PATTSCHED, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgPattSched->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_PATTSCHED): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_PATTSCHED_ENG): //&&
			IDD_DIALOG_PATTSCHED, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgPattSched->GetClientRect(m_DlgPattSchedRect);
		return;
	}
	else
	{		
		if(!pStep) return;
		if(!m_pDlgPattSched) return;
		
		int nStep=pStep->chStepNo;		
		m_pDlgPattSched->m_nStepNo =  nStep;
		m_pDlgPattSched->m_pStep = (STEP *)GetDocument()->GetStepData(nStep);				
		
		CRect dlgRect,gridRect, cellRect,screenRect;
		m_pDlgPattSched->GetWindowRect(dlgRect);
		m_wndStepGrid.GetWindowRect(gridRect);//Get Step Grid Window Position
		//cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_END-3);
		cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_CYCLE);
		
		//////////////////////////////////////////////////////////////////////////
		//ljb 20090324	스텝 End Dialog Size 조절
		screenRect = cellRect;
		ClientToScreen(screenRect);
		int nTopPoint;
		if (screenRect.bottom+dlgRect.Height() > 800)
		{//End Dialog 화면 위로 올림
			nTopPoint = gridRect.top+20+3;
		}
		else
		{//그리드 아래에 End Dialog 표시
			nTopPoint = gridRect.top+cellRect.Height()+3;
		}
		m_pDlgPattSched->MoveWindow( gridRect.left+cellRect.Width()+3, 
								   nTopPoint,
								   m_DlgPattSchedRect.Width(), 
								   m_DlgPattSchedRect.Height());
		m_pDlgPattSched->ShowWindow(SW_SHOW);			
	}
}


void CCTSEditorProView::HidePattSelectDlg()
{
	if(m_pDlgPattSelect)			 
	{//Delete Dlg	
		if(m_pDlgPattSelect->m_hWnd)
		{
			m_pDlgPattSelect->m_pStep=NULL;
			m_pDlgPattSelect->ShowWindow(SW_HIDE);
		}
	}
}

void CCTSEditorProView::ShowPattSelectDlg(int iType,STEP *pStep)
{
	if(iType==0)
	{
		m_pDlgPattSelect = new Dlg_PattSelect(this);
		ASSERT(m_pDlgPattSelect);
		m_pDlgPattSelect->m_pDoc = (CCTSEditorProDoc *)GetDocument();
		//m_pDlgPattSelect->Create(IDD_DIALOG_PATTSELECT, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgPattSelect->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_PATTSELECT): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_PATTSELECT_ENG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DIALOG_PATTSELECT_PL): //&&
			IDD_DIALOG_PATTSELECT, (CMyGridWnd *)&m_wndStepGrid);
		m_pDlgPattSelect->GetClientRect(m_DlgPattSelectRect);
		return;
	}
	else
	{		
		if(!pStep) return;
		if(!m_pDlgPattSelect) return;
		
		int nStep=pStep->chStepNo;		
		m_pDlgPattSelect->m_nStepNo =  nStep;
		m_pDlgPattSelect->m_pStep = (STEP *)GetDocument()->GetStepData(nStep);				
		
		CRect dlgRect,gridRect, cellRect,screenRect;
		m_pDlgPattSelect->GetWindowRect(dlgRect);
		m_wndStepGrid.GetWindowRect(gridRect);//Get Step Grid Window Position
		//cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_END-3);
		cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_CYCLE);
		
		//////////////////////////////////////////////////////////////////////////
		//ljb 20090324	스텝 End Dialog Size 조절
		screenRect = cellRect;
		ClientToScreen(screenRect);
		int nTopPoint;
		if (screenRect.bottom+dlgRect.Height() > 800)
		{//End Dialog 화면 위로 올림
			nTopPoint = gridRect.top+20+3;
		}
		else
		{//그리드 아래에 End Dialog 표시
			nTopPoint = gridRect.top+cellRect.Height()+3;
		}
		m_pDlgPattSelect->MoveWindow( gridRect.left+cellRect.Width()+3, 
								   nTopPoint,
								   m_DlgPattSelectRect.Width(), 
								   m_DlgPattSelectRect.Height());
		m_pDlgPattSelect->ShowWindow(SW_SHOW);			
	}
}

BOOL CCTSEditorProView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		if(GetFocus() == &m_wndTestListGrid)
		{
			switch( pMsg->wParam )
			{
			case VK_INSERT:
				return TRUE;
				
			case VK_DELETE:
				return TRUE;
			}
		}

		if(GetFocus() == &m_wndStepGrid)
		{
			switch( pMsg->wParam )
			{
			case VK_INSERT:
				OnStepInsert();
				return TRUE;

			case VK_DELETE:
				OnStepDelete();
				return TRUE;
			}
		}
	}

	m_tooltip.RelayEvent(pMsg);
	return CFormView::PreTranslateMessage(pMsg);
}


void CCTSEditorProView::ClearStepState()
{
	m_LoadedTest.SetText(Fun_FindMsg("ClearStepState_msg1","IDD_CTSEditorPro_CYCL"));
	m_TotalStepCount.SetText("0");
	GetDlgItem(IDC_STEP_INSERT)->EnableWindow(FALSE);
	GetDlgItem(IDC_STEP_DELETE)->EnableWindow(FALSE);
	GetDlgItem(IDC_STEP_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_LOAD_TEST)->EnableWindow(FALSE);
	GetDlgItem(IDC_PRETEST_CHECK)->EnableWindow(FALSE);
}

void CCTSEditorProView::ShowEndTypeDlg(int nStep)
{
	if(GetDocument()->GetStepData(nStep) == NULL)	return;
	
	m_pDlg->m_nStepNo =  nStep;
	m_pDlg->m_strEndMemo = m_wndStepGrid.GetValueRowCol(nStep, COL_STEP_END);		//COL_STEP_END

/*	m_pDlg->m_EndV.SetValue((double)GetDocument()->VUnitTrans(pStep->fEndV, FALSE));	//pStep->fEndV/V_UNIT_FACTOR);
	m_pDlg->m_EndI.SetValue((double)GetDocument()->IUnitTrans(pStep->fEndI, FALSE));	//pStep->fEndI/I_UNIT_FACTOR);
	m_pDlg->m_EndC.SetValue((double)GetDocument()->CUnitTrans(pStep->fEndC, FALSE));	//pStep->fEndC/C_UNIT_FACTOR);
	m_pDlg->m_EnddV.SetValue((double)GetDocument()->VUnitTrans(pStep->fEndDV, FALSE));	//pStep->fEndDV/V_UNIT_FACTOR);
	m_pDlg->m_EndWatt.SetValue((double)GetDocument()->WattUnitTrans(pStep->fEndW, FALSE));	//pStep->fEndDI/I_UNIT_FACTOR);
	m_pDlg->m_EndWattHour.SetValue((double)GetDocument()->WattHourUnitTrans(pStep->fEndWh, FALSE));	//pStep->fEndDI/I_UNIT_FACTOR);
	COleDateTime endTime;
	ULONG lSecond = pStep->ulEndTime / 100;
	m_pDlg->m_lEndMSec = (pStep->ulEndTime % 100)*10;
	m_pDlg->m_nEndDay = lSecond / 86400;
	int nHour = lSecond % 86400;
	endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
//	m_pDlg->m_EndTime.SetDateTime(endTime);
	m_pDlg->m_ctrlEndTime.SetTime(endTime);

	m_pDlg->m_nLoopInfoCycle = pStep->nLoopInfoCycle;
	m_pDlg->m_nLoopInfoGoto = pStep->nLoopInfoGoto;

	//20051117 SOC 종료 조건 추가 KBH
	m_pDlg->m_bUseActualCapa = pStep->bUseActualCapa;
	m_pDlg->m_fSocRate = pStep->fSocRate;
*/	
	CRect dlgRect,gridRect, cellRect,screenRect;
	m_pDlg->GetWindowRect(dlgRect);
	m_wndStepGrid.GetWindowRect(gridRect);						//Get Step Grid Window Position
//	cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_END-3);
	cellRect = m_wndStepGrid.CalcRectFromRowCol(0, 0, nStep, COL_STEP_CYCLE);
	
	//////////////////////////////////////////////////////////////////////////
	//ljb 20090324	스텝 End Dialog Size 조절
	screenRect = cellRect;
	ClientToScreen(screenRect);
	int nTopPoint;
	if (screenRect.bottom+dlgRect.Height() > 800)
		//End Dialog 화면 위로 올림
		nTopPoint = gridRect.top+20+3;
	else
		//그리드 아래에 End Dialog 표시
		nTopPoint = gridRect.top+cellRect.Height()+3;

	
// 	int nLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
// 	int nWidth = 615;
// 	if(nLanguage == 1) nWidth = 615;
// 	else if(nLanguage ==2) nWidth = 775;
// 	else if(nLanguage ==3) nWidth = 775;
// 	else nWidth = 615;

	/*int nLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	int nWidth = 615;
	if(nLanguage == 1) nWidth = 615;
	else if(nLanguage ==2) nWidth = 550;
	else if(nLanguage ==3) nWidth = 550;
	else nWidth = 615;*/

	m_pDlg->m_bExtViewMode = FALSE;
	m_pDlg->MoveWindow( gridRect.left+cellRect.Width()+3, 
		nTopPoint,
		m_recDlgRect.Width()-545, 
		m_recDlgRect.Height());
	m_pDlg->ShowWindow(SW_SHOW);

	/*if (m_pDlg->m_bExtViewMode)
	{
		//::SetWindowPos( m_pDlg->m_hWnd, HWND_NOTOPMOST, gridRect.left+cellRect.Width()+3, nTopPoint, m_recDlgRect.Width()-545,m_recDlgRect.Height(), SWP_SHOWWINDOW);
		m_pDlg->MoveWindow( gridRect.left+cellRect.Width()+3, 
			nTopPoint,
			m_recDlgRect.Width(), 
			m_recDlgRect.Height());
		m_pDlg->ShowWindow(SW_SHOW);

	}
	else
	{
		
		//::SetWindowPos( m_pDlg->m_hWnd, HWND_NOTOPMOST, gridRect.left+cellRect.Width()+3, nTopPoint, m_recDlgRect.Width()-545,m_recDlgRect.Height(), SWP_SHOWWINDOW);
		//m_pDlg->OnShowWindow(TRUE, 0);
		m_pDlg->MoveWindow( gridRect.left+cellRect.Width()+3, 
			nTopPoint,
			//m_recDlgRect.Width()-405, 
			//m_recDlgRect.Width()-475, 
			m_recDlgRect.Width()-545, 
			m_recDlgRect.Height());
		m_pDlg->ShowWindow(SW_SHOW);
	}*/

// #ifdef _BMS_
// 	m_pDlg->MoveWindow( gridRect.left+cellRect.Width()+3, 
// 		nTopPoint,
// 		m_recDlgRect.Width(), 
// 		m_recDlgRect.Height());
// 	m_pDlg->ShowWindow(SW_SHOW);
// #else
// 	int nTopPoint = gridRect.top+cellRect.Height()+3;
// 	m_pDlg->MoveWindow( gridRect.left+cellRect.Width()+3, 
// 		nTopPoint,
// 		m_recDlgRect.Width()-350, 
// 		m_recDlgRect.Height());
// 	m_pDlg->ShowWindow(SW_SHOW);
// #endif




	STEP *pStep = NULL;
	pStep = GetDocument()->GetStepData(nStep);
	if(pStep == NULL)	return;
	switch(pStep->chType)
	{
	case PS_STEP_ADV_CYCLE:
	case PS_STEP_LOOP:
	case PS_STEP_END:
		m_pDlg->MoveWindow( gridRect.left+cellRect.Width()+3, 
			nTopPoint,
			//m_recDlgRect.Width()-475, 
			m_recDlgRect.Width()-545, //yulee 20180927
			m_recDlgRect.Height());
		m_pDlg->ShowWindow(SW_SHOW);		// Aux BMS 안보이게  하기
		break;
	}
}

BOOL CCTSEditorProView::Fun_CheckGotoStep( STEP *pStep, int nTotStep, CString &strMsg)	//20150206 add
{
	if (pStep->chType == PS_STEP_END) return TRUE;			

	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	STEP *pTempStep=NULL;
	strMsg.Empty();
	int nStepNum = pStep->chStepNo;
	
	if (pStep->nLoopInfoGotoStep != 0 && pStep->nLoopInfoGotoStep != PS_STEP_NEXT && pStep->nLoopInfoGotoStep != PS_STEP_PAUSE && pStep->nLoopInfoGotoStep != nTotStep) 
	{
		if (pStep->nLoopInfoGotoStep > nTotStep) 
		{
			strMsg.Format("STEP %d : LoopGotoStep Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nLoopInfoGotoStep);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : LoopGotoStep Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}
	if (pStep->nMultiLoopInfoGotoStep != 0 && pStep->nMultiLoopInfoGotoStep != PS_STEP_NEXT && pStep->nMultiLoopInfoGotoStep != PS_STEP_PAUSE && pStep->nMultiLoopInfoGotoStep != nTotStep) 
	{
		if (pStep->nMultiLoopInfoGotoStep > nTotStep) 
		{
			strMsg.Format("STEP %d : MultiLoopInfoGotoStep Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nMultiLoopInfoGotoStep);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : MultiLoopInfoGotoStep Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}
	if (pStep->nAccLoopInfoGotoStep != 0 && pStep->nAccLoopInfoGotoStep != PS_STEP_NEXT && pStep->nAccLoopInfoGotoStep != PS_STEP_PAUSE && pStep->nAccLoopInfoGotoStep != nTotStep) 
	{
		if (pStep->nAccLoopInfoGotoStep > nTotStep) 
		{
			strMsg.Format("STEP %d : AccLoopInfoGotoStep Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nAccLoopInfoGotoStep);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : AccLoopInfoGotoStep Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}

	/////////////////////////////////////////
	if (pStep->nGotoStepEndV_H != 0 && pStep->nGotoStepEndV_H != PS_STEP_NEXT && pStep->nGotoStepEndV_H != PS_STEP_PAUSE && pStep->nGotoStepEndV_H != nTotStep) 
	{
		if (pStep->nGotoStepEndV_H > nTotStep) 
		{
			strMsg.Format("STEP %d : GotoStepEndV_H Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nGotoStepEndV_H);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : GotoStepEndV_H Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}	
	if (pStep->nGotoStepEndV_L != 0 && pStep->nGotoStepEndV_L != PS_STEP_NEXT && pStep->nGotoStepEndV_L != PS_STEP_PAUSE && pStep->nGotoStepEndV_L != nTotStep) 
	{
		if (pStep->nGotoStepEndV_L > nTotStep) 
		{
			strMsg.Format("STEP %d : GotoStepEndV_L Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nGotoStepEndV_L);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : GotoStepEndV_L Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}
	if (pStep->nGotoStepEndTime != 0 && pStep->nGotoStepEndTime != PS_STEP_NEXT && pStep->nGotoStepEndTime != PS_STEP_PAUSE && pStep->nGotoStepEndTime != nTotStep) 
	{
		if (pStep->nGotoStepEndTime > nTotStep) 
		{
			strMsg.Format("STEP %d : GotoStepEndTime Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nGotoStepEndTime);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : GotoStepEndTime Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}
	if (pStep->nGotoStepEndCVTime != 0 && pStep->nGotoStepEndCVTime != PS_STEP_NEXT && pStep->nGotoStepEndCVTime != PS_STEP_PAUSE && pStep->nGotoStepEndCVTime != nTotStep) 
	{
		if (pStep->nGotoStepEndCVTime > nTotStep) 
		{
			strMsg.Format("STEP %d : GotoStepEndCVTime Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nGotoStepEndCVTime);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : GotoStepEndCVTime Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}
	if (pStep->nGotoStepEndC != 0 && pStep->nGotoStepEndC != PS_STEP_NEXT && pStep->nGotoStepEndC != PS_STEP_PAUSE && pStep->nGotoStepEndC != nTotStep) 
	{
		if (pStep->nGotoStepEndC > nTotStep) 
		{
			strMsg.Format("STEP %d : GotoStepEndC Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nGotoStepEndC);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : GotoStepEndC Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}
	if (pStep->nGotoStepEndWh != 0 && pStep->nGotoStepEndWh != PS_STEP_NEXT && pStep->nGotoStepEndWh != PS_STEP_PAUSE && pStep->nGotoStepEndWh != nTotStep) 
	{
		if (pStep->nGotoStepEndWh > nTotStep) 
		{
			strMsg.Format("STEP %d : GotoStepEndWh Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nGotoStepEndWh);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : GotoStepEndWh Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}
	if (pStep->nGotoStepEndValue != 0 && pStep->nGotoStepEndValue != PS_STEP_NEXT && pStep->nGotoStepEndValue != PS_STEP_PAUSE && pStep->nGotoStepEndValue != nTotStep) 
	{
		if (pStep->nGotoStepEndValue > nTotStep) 
		{
			strMsg.Format("STEP %d : GotoStepEndValue Error.", nStepNum);
			return FALSE;
		}
		pTempStep = (STEP *)pDoc->GetStepData(pStep->nGotoStepEndValue);
		if (pTempStep->chType != PS_STEP_ADV_CYCLE)
		{
			strMsg.Format("STEP %d : GotoStepEndValue Error.", nStepNum);
			return FALSE;													//Cycle step 이면 리턴
		}
	}

	//CAN
	int a;
	for(a =0; a<SCH_MAX_END_CAN_AUX_POINT; a++)
	{
		if (pStep->ican_branch[a] != 0 && pStep->ican_branch[a] != PS_STEP_NEXT && pStep->ican_branch[a] != PS_STEP_PAUSE && pStep->ican_branch[a] != nTotStep) 
		{
			if (pStep->ican_branch[a] > nTotStep) 
			{
				strMsg.Format("STEP %d : CAN GotoStep Error(%d Num).", nStepNum,a+1);
				return FALSE;
			}
			pTempStep = (STEP *)pDoc->GetStepData(pStep->ican_branch[a]);
			if (pTempStep->chType != PS_STEP_ADV_CYCLE)
			{
				strMsg.Format("STEP %d : CAN GotoStep Error(%d Num).", nStepNum,a+1);
				return FALSE;													//Cycle step 이면 리턴
			}
		}
	}

	//AUX
	for(a =0; a<SCH_MAX_END_CAN_AUX_POINT; a++)
	{
		if (pStep->iaux_branch[a] != 0 && pStep->iaux_branch[a] != PS_STEP_NEXT && pStep->iaux_branch[a] != PS_STEP_PAUSE && pStep->iaux_branch[a] != nTotStep) 
		{
			if (pStep->iaux_branch[a] > nTotStep) 
			{
				strMsg.Format("STEP %d : AUX GotoStep Error(%d Num).", nStepNum,a+1);
				return FALSE;
			}
			pTempStep = (STEP *)pDoc->GetStepData(pStep->iaux_branch[a]);
			if (pTempStep->chType != PS_STEP_ADV_CYCLE)
			{
				strMsg.Format("STEP %d : AUX GotoStep Error(%d Num).", nStepNum,a+1);
				return FALSE;													//Cycle step 이면 리턴
			}
		}
	}
	return TRUE;
}

BOOL CCTSEditorProView::Fun_CheckStepInCycle( STEP *pStep, int nTotStep, CString &strMsg)	//20150206 add
{
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	STEP *pTempStep=NULL;
	int nCycleStart=0, nCycleEnd=0;

	strMsg.Empty();
	int nStepNum = pStep->chStepNo;
	if (pStep->chType == PS_STEP_END) return TRUE;			
	
	//같은 Cycle 안에 있는 스텝인지 검사
	int i=0;
	for (i = nStepNum; i > 0; i--)
	{
		pTempStep = (STEP *)pDoc->GetStepData(i);
		if (pTempStep->chType == PS_STEP_ADV_CYCLE)
		{
			nCycleStart = i;
			break;
		}
		nCycleStart = i;
	}
	for (i = nStepNum; i < nTotStep; i++)
	{
		pTempStep = (STEP *)pDoc->GetStepData(i);
		if (pTempStep->chType == PS_STEP_LOOP || pTempStep->chType == PS_STEP_END)
		{
			nCycleEnd = i;
			break;
		}
		nCycleEnd = i;
	}

	int nLoop;
	int nGotoStep;
	//int nGotoStep = pStep->nLoopInfoGotoStep;
	for (nLoop=0; nLoop < SCH_MAX_END_CAN_AUX_POINT; nLoop++)
	{
		nGotoStep = pStep->ican_branch[nLoop];
		if (nGotoStep == 0 || nGotoStep == PS_STEP_NEXT || nGotoStep == PS_STEP_PAUSE )	continue;
		if (nGotoStep < nCycleStart || nCycleEnd < nGotoStep)	
		{
			//20150901 add nGotoStep이 Cycle STEP 이면 return TRUE
			pTempStep = (STEP *)pDoc->GetStepData(nGotoStep);		
			if (pTempStep == NULL)  //ksj 20210709 : null pointer check 추가
			{				
				strMsg.Format("STEP %d : CAN Goto Error. CAN Div%d goto step %d is not exist.", nStepNum, nLoop+1, nGotoStep);
				return FALSE;
			}
			if (pTempStep->chType == PS_STEP_ADV_CYCLE)	return TRUE;	//20150901 add Cycle step 이면 리턴		
					
			strMsg.Format("STEP %d : CAN Goto Error", nStepNum);
			return FALSE;
		}
	}
	for (nLoop=0; nLoop < SCH_MAX_END_CAN_AUX_POINT; nLoop++)
	{
		nGotoStep = pStep->iaux_branch[nLoop];
		if (nGotoStep == 0 || nGotoStep == PS_STEP_NEXT || nGotoStep == PS_STEP_PAUSE )	continue;
		if (nGotoStep < nCycleStart || nCycleEnd < nGotoStep)	
		{
			//20150901 add nGotoStep이 Cycle STEP 이면 return TRUE
			pTempStep = (STEP *)pDoc->GetStepData(nGotoStep);
			if (pTempStep == NULL)  //ksj 20210709 : null pointer check 추가
			{
				strMsg.Format("STEP %d : AUX Goto Error. AUX Div%d goto step %d is not exist.", nStepNum, nLoop+1, nGotoStep);
				return FALSE;
			}
			if (pTempStep->chType == PS_STEP_ADV_CYCLE)	return TRUE;	//20150901 add Cycle step 이면 리턴
			
			strMsg.Format("STEP %d : AUX Goto Error", nStepNum);
			return FALSE;
		}
	}
	return TRUE;
}
BOOL CCTSEditorProView::StepSave(int nType)
{
	//cny 201809
	HideEndDlg();
	HideCellBalDlg();
	HidePwrSupplyDlg();
	HideChillerDlg();
	HidePattSchedDlg();
	HidePattSelectDlg();

	int nRtn;
	CString strTemp;
	CString strTestName;//yulee 20180927

	if(nType == 1) //yulee 20180912
	{
		//Max Current_Spec 추가 20180808 yulee
		float fMaxCurrentSpec = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current_Spec", 5000);
		CString tmpStr;
		GetDlgItem(IDC_SAFETY_MAX_I)->GetWindowText(tmpStr);
		
		
		//yulee 20180912
		int nParallelNum = 0;
		int iMaxCurrentSpec = (int)fMaxCurrentSpec/1000;
		nParallelNum = ((atoi(tmpStr))/(iMaxCurrentSpec*1.1))+1;
		if (nParallelNum > 1)
		{
			if((atoi(tmpStr))%((int)(iMaxCurrentSpec*1.1))== 0)
					nParallelNum--;
			int nTmp = nParallelNum-2;
			if(nTmp < 0)
			nTmp = 0;
			m_CboParallelNum.SetCurSel(nTmp);
			//m_bChkParallel.SetCheck(TRUE);
		}
		if(atoi(tmpStr)>iMaxCurrentSpec*1.1)
			m_bChkParallel.SetCheck(TRUE);
		else
			m_bChkParallel.SetCheck(FALSE);
	}
	
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();

	if(UpdatePreTestParam(nType) < 1)		//ljb 20150211 add
	{
// 		strTemp.Format("시험 안전조건이 설정되어 있지 않습니다. \n(1개 이상 설정 하세요)");
// 		MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	if(!UpdateStepGrid(m_nDisplayStep))				//Update Current Display Step Data
	{
		AfxMessageBox("Step Data Update Fail...");
		return FALSE;
	}

	int nID = -1;
	CUIntArray	uiarryMultiLoopStart,uiarryMultiLoopEnd,uiarryMultiStack;
	for(int a=0; a<m_wndTestListGrid.GetRowCount(); a++)
	{
		if(m_lLoadedTestID == atoi(m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_PRIMARY_KEY)))
		{
			nID = atol(m_wndTestListGrid.GetValueRowCol(a+1,COL_TEST_PROC_TYPE));
			strTestName = m_wndTestListGrid.GetValueRowCol(a+1,COL_TEST_NAME); //yulee 20180927
			if(CheckExecutableStep(nID, m_lLoadedTestID) == FALSE)
			{
				strTemp.Format(Fun_FindMsg("StepSave_msg1","IDD_CTSEditorPro_CYCL"), m_wndTestListGrid.GetValueRowCol(a+1, COL_TEST_NAME));
				MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
				return FALSE;
			}
			break;
		}
	}
	//ljb STEP 2 는 항상 OCV  이여야 한다. for Samsung QC 
// 	char chType;	
// 	chType = (char)atoi(m_wndStepGrid.GetValueRowCol(2, COL_STEP_TYPE));
// 	if ( chType != PS_STEP_OCV)
// 	{
// 		strTemp = "STEP 2 는 OCV를 설정 하셔야 합니다.";
// 		MessageBox(strTemp, "Error", MB_OK|MB_ICONSTOP);
// 		return FALSE;
// 	}

	CString strMsg;

		//ljb v1009 MultiGroup 동기화 ////////////////////////////////////////////////////////////////////////	
		int nTotalStep = pDoc->GetTotalStepNum();
		uiarryMultiLoopStart.RemoveAll();
		uiarryMultiLoopEnd.RemoveAll();
		uiarryMultiStack.RemoveAll();
		int nStackSize;
		for(int i = 0; i<nTotalStep; i++)
		{
			STEP *pStep = (STEP *)pDoc->GetStepData(i+1);

			//ljb 20150206 add start (goto step 확인)  ///////////////////////////////////////
			if (Fun_CheckGotoStep(pStep,nTotalStep,strMsg) == FALSE)
			{
				if (pStep->chType == PS_STEP_LOOP) { AfxMessageBox(strMsg); return FALSE; }

				if (Fun_CheckStepInCycle(pStep,nTotalStep,strMsg) == FALSE)
				{
					AfxMessageBox(strMsg);
					return FALSE;
				}
			}
			//ljb 20150206 add end   (goto step 확인)  ///////////////////////////////////////
			
			switch (pStep->nMultiLoopGroupID)
			{
			case 0:
				pStep->nMultiLoopInfoCycle=0;
				pStep->nMultiLoopInfoGotoStep=0;
				break;
			case 1:
				pStep->nMultiLoopInfoCycle=m_nMultiGroupLoopInfoCycle[0];
				pStep->nMultiLoopInfoGotoStep=m_nMultiGroupLoopInfoGoto[0];
				if (pStep->chType == PS_STEP_ADV_CYCLE)
				{
					uiarryMultiLoopStart.Add(1);
					uiarryMultiStack.Add(1);
				}
				if (pStep->chType == PS_STEP_LOOP)
				{
					uiarryMultiLoopEnd.Add(1);
					nStackSize = uiarryMultiStack.GetSize();
					if ( nStackSize < 1)
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg2","IDD_CTSEditorPro_CYCL"));
						//AfxMessageBox("Multi Cycle 1 에 다른 Cycle 이 있습니다.");
						return FALSE;
					}
					if (uiarryMultiStack.GetAt(nStackSize-1) == 1)
						uiarryMultiStack.RemoveAt(nStackSize-1);
					else
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg3","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
				}
				break;
			case 2:
				pStep->nMultiLoopInfoCycle=m_nMultiGroupLoopInfoCycle[1];
				pStep->nMultiLoopInfoGotoStep=m_nMultiGroupLoopInfoGoto[1];
				if (pStep->chType == PS_STEP_ADV_CYCLE)
				{
					uiarryMultiLoopStart.Add(2);
					uiarryMultiStack.Add(2);
				}
				if (pStep->chType == PS_STEP_LOOP)
				{
					uiarryMultiLoopEnd.Add(2);
					nStackSize = uiarryMultiStack.GetSize();
					if ( nStackSize < 1)
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg4","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
					if (uiarryMultiStack.GetAt(nStackSize-1) == 2)
						uiarryMultiStack.RemoveAt(nStackSize-1);
					else
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg5","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
				}
				break;
			case 3:
				pStep->nMultiLoopInfoCycle=m_nMultiGroupLoopInfoCycle[2];
				pStep->nMultiLoopInfoGotoStep=m_nMultiGroupLoopInfoGoto[2];
				if (pStep->chType == PS_STEP_ADV_CYCLE)
				{
					uiarryMultiLoopStart.Add(3);
					uiarryMultiStack.Add(3);
				}
				if (pStep->chType == PS_STEP_LOOP)
				{
					uiarryMultiLoopEnd.Add(3);
					nStackSize = uiarryMultiStack.GetSize();
					if ( nStackSize < 1)
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg6","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
					if (uiarryMultiStack.GetAt(nStackSize-1) == 3)
						uiarryMultiStack.RemoveAt(nStackSize-1);
					else
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg7","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
				}
				break;
			case 4:
				pStep->nMultiLoopInfoCycle=m_nMultiGroupLoopInfoCycle[3];
				pStep->nMultiLoopInfoGotoStep=m_nMultiGroupLoopInfoGoto[3];
				if (pStep->chType == PS_STEP_ADV_CYCLE)
				{
					uiarryMultiLoopStart.Add(4);
					uiarryMultiStack.Add(4);
				}
				if (pStep->chType == PS_STEP_LOOP)
				{
					uiarryMultiLoopEnd.Add(4);
					nStackSize = uiarryMultiStack.GetSize();
					if ( nStackSize < 1)
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg8","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
					if (uiarryMultiStack.GetAt(nStackSize-1) == 4)
						uiarryMultiStack.RemoveAt(nStackSize-1);
					else
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg9","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
				}
				break;
			case 5:
				pStep->nMultiLoopInfoCycle=m_nMultiGroupLoopInfoCycle[4];
				pStep->nMultiLoopInfoGotoStep=m_nMultiGroupLoopInfoGoto[4];
				if (pStep->chType == PS_STEP_ADV_CYCLE)
				{
					uiarryMultiLoopStart.Add(5);
					uiarryMultiStack.Add(5);
				}
				if (pStep->chType == PS_STEP_LOOP)
				{
					uiarryMultiLoopEnd.Add(5);
					nStackSize = uiarryMultiStack.GetSize();
					if ( nStackSize < 1)
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg10","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
					if (uiarryMultiStack.GetAt(nStackSize-1) == 5)
						uiarryMultiStack.RemoveAt(nStackSize-1);
					else
					{
						AfxMessageBox(Fun_FindMsg("StepSave_msg11","IDD_CTSEditorPro_CYCL"));
						return FALSE;
					}
				}
				break;
			default:
				pStep->nMultiLoopInfoCycle=0;
				pStep->nMultiLoopInfoGotoStep=0;
				break;
			}

			//ljb v1009 AccGroup 동기화 
			switch (pStep->nAccLoopGroupID)
			{
			case 0:
				pStep->nAccLoopInfoCycle=0;
				pStep->nAccLoopInfoGotoStep=0;
				break;
			case 1:
				pStep->nAccLoopInfoCycle=m_nAccGroupLoopInfoCycle[0];
				pStep->nAccLoopInfoGotoStep=m_nAccGroupLoopInfoGoto[0];
				break;
			case 2:
				pStep->nAccLoopInfoCycle=m_nAccGroupLoopInfoCycle[1];
				pStep->nAccLoopInfoGotoStep=m_nAccGroupLoopInfoGoto[1];
				break;
			case 3:
				pStep->nAccLoopInfoCycle=m_nAccGroupLoopInfoCycle[2];
				pStep->nAccLoopInfoGotoStep=m_nAccGroupLoopInfoGoto[2];
				break;
			case 4:
				pStep->nAccLoopInfoCycle=m_nAccGroupLoopInfoCycle[3];
				pStep->nAccLoopInfoGotoStep=m_nAccGroupLoopInfoGoto[3];
				break;
			case 5:
				pStep->nAccLoopInfoCycle=m_nAccGroupLoopInfoCycle[4];
				pStep->nAccLoopInfoGotoStep=m_nAccGroupLoopInfoGoto[4];
				break;
			default:
				pStep->nAccLoopInfoCycle=0;
				pStep->nAccLoopInfoGotoStep=0;
				break;
			}
		}
		//////////////////////////////////////////////////////////////////////////
		nStackSize = uiarryMultiStack.GetSize();
		if (nStackSize > 0)
		{
			AfxMessageBox(Fun_FindMsg("StepSave_msg12","IDD_CTSEditorPro_CYCL"));
			return FALSE;
		}
		//////////////////////////////////////////////////////////////////////////
	if(Fun_CheckMultiLoop(uiarryMultiLoopStart, uiarryMultiLoopEnd) < 0)
	{
		return FALSE;
	}

	if((nRtn = pDoc->SaveStep(m_lLoadedTestID, m_bLinkChamber, nType))<0)
	{
		strTemp.Format(Fun_FindMsg("StepSave_msg13","IDD_CTSEditorPro_CYCL"), GetDocument()->m_strLastErrorString);
		MessageBox(strTemp, Fun_FindMsg("StepSave_msg14","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
		return FALSE;
	}

	if(pDoc->SavePreTestParam(m_lLoadedTestID)<0)
	{
		strTemp.Format(Fun_FindMsg("StepSave_msg15","IDD_CTSEditorPro_CYCL"), GetDocument()->m_strLastErrorString);
		AfxMessageBox(strTemp);
		return FALSE;
	}

	//yulee 20180906
	if(nType == 1)
	{
		OnStepSave(); //yulee 20181002
		OnLoadTest(); //yulee 20181002
	}

	//Undo List 삭제 
	pDoc->RemoveUndoStep();
	m_bStepDataSaved = TRUE;
	return TRUE;
}

void CCTSEditorProView::SetModelGridColumnWidth()
{
	if( m_pWndCurModelGrid->GetSafeHwnd())
	{
		CRect rect;
		m_pWndCurModelGrid->GetClientRect(&rect);
		m_pWndCurModelGrid->SetColWidth(COL_MODEL_NO, COL_MODEL_NO, rect.Width()*0.1f);
		
		if(GetDocument()->m_bUseLogin == FALSE)
		{
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_NAME, COL_MODEL_NAME, rect.Width()*0.4f);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_CREATOR, COL_MODEL_CREATOR, rect.Width()*0.2f);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_DESCRIPTION, COL_MODEL_DESCRIPTION, rect.Width()*0.3f);
		}
		else
		{
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_NAME, COL_MODEL_NAME, rect.Width()*0.5f);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_CREATOR, COL_MODEL_CREATOR, 0);
			m_pWndCurModelGrid->SetColWidth(COL_MODEL_DESCRIPTION, COL_MODEL_DESCRIPTION, rect.Width()*0.4f);
		}

		m_pWndCurModelGrid->SetColWidth(COL_MODEL_PRIMARY_KEY, COL_MODEL_PRIMARY_KEY, 0); 
		m_pWndCurModelGrid->SetColWidth(COL_MODEL_EDIT_STATE, COL_MODEL_EDIT_STATE, 0); 
	}
}

void CCTSEditorProView::SetTestGridColumnWidth()
{
	if( m_pWndCurModelGrid->GetSafeHwnd())
	{
		CRect rect;
		m_wndTestListGrid.GetClientRect(&rect);
/*		m_wndTestListGrid.SetColWidth(COL_TEST_NO, COL_TEST_NO, rect.Width()*0.05f);
		m_wndTestListGrid.SetColWidth(COL_TEST_PROC_TYPE, COL_TEST_PROC_TYPE, rect.Width()*0.14f);
		m_wndTestListGrid.SetColWidth(COL_TEST_NAME, COL_TEST_NAME, rect.Width()*0.2f);
		m_wndTestListGrid.SetColWidth(COL_TEST_CREATOR, COL_TEST_CREATOR, rect.Width()*0.13f);
		m_wndTestListGrid.SetColWidth(COL_TEST_DESCRIPTION, COL_TEST_DESCRIPTION, rect.Width()*0.20f);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_TIME, COL_TEST_EDIT_TIME, rect.Width()*0.28f);
		m_wndTestListGrid.SetColWidth(COL_TEST_PRIMARY_KEY, COL_TEST_PRIMARY_KEY, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_STATE, COL_TEST_EDIT_STATE, 0);
*/
		m_wndTestListGrid.SetColWidth(COL_TEST_NO, COL_TEST_NO, rect.Width()*0.1f);

		m_wndTestListGrid.SetColWidth(COL_TEST_PROC_TYPE, COL_TEST_PROC_TYPE, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_NAME, COL_TEST_NAME, rect.Width()*0.3f);

#ifdef _CYCLER_
		m_wndTestListGrid.SetColWidth(COL_TEST_RESET_PROC, COL_TEST_RESET_PROC, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_DESCRIPTION, COL_TEST_DESCRIPTION, rect.Width()*0.6f);

#else
//	#ifdef _AUTO_PROCESS_
//			m_wndTestListGrid.SetColWidth(COL_TEST_RESET_PROC, COL_TEST_RESET_PROC, rect.Width()*0.1f);
//			m_wndTestListGrid.SetColWidth(COL_TEST_DESCRIPTION, COL_TEST_DESCRIPTION, rect.Width()*0.5f);
//	#else
			m_wndTestListGrid.SetColWidth(COL_TEST_RESET_PROC, COL_TEST_RESET_PROC, 0);
			m_wndTestListGrid.SetColWidth(COL_TEST_DESCRIPTION, COL_TEST_DESCRIPTION, rect.Width()*0.6f);
//	#endif
#endif

		m_wndTestListGrid.SetColWidth(COL_TEST_CREATOR, COL_TEST_CREATOR, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_TIME, COL_TEST_EDIT_TIME, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_PRIMARY_KEY, COL_TEST_PRIMARY_KEY, 0);
		m_wndTestListGrid.SetColWidth(COL_TEST_EDIT_STATE, COL_TEST_EDIT_STATE, 0);
	}
}

void CCTSEditorProView::OnOption() 
{
	// TODO: Add your command handler code here
	CEditorSetDlg	*pDlg = new CEditorSetDlg(this);
	ASSERT(pDlg);

	if(pDlg->DoModal() == IDOK)
	{
		m_bVHigh  = pDlg->m_bVHigh;
		m_bVLow   = pDlg->m_bVLow ;
		m_bIHigh  = pDlg->m_bIHigh;
		m_bILow   = pDlg->m_bILow;
		m_bCHigh  = pDlg->m_bCHigh;
		m_bCLow   = pDlg->m_bCLow;

		m_fCLowVal = pDlg->m_fCLowVal;
		m_fCHighVal = pDlg->m_fCHighVal ;
		m_fILowVal = pDlg->m_fILowVal ;
		m_fIHighVal = pDlg->m_fIHighVal;
		m_fVLowVal = pDlg->m_fVLowVal;
		m_fVHighVal = pDlg->m_fVHighVal;
		GetDocument()->m_bUseLogin = pDlg->m_bUseLogin;
		GetDocument()->m_lVRefHigh = pDlg->m_lVRefHigh;
		GetDocument()->m_lVRefLow = pDlg->m_lVRefLow;
		char szBuff[64], szUnit[32], szDecimalPoint[32];
		CString strTemp;
		sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "V Unit", "V 3"));
		sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
		strTemp = szUnit;
		GetDocument()->m_VtgUnitInfo.nFloatPoint = atoi(szDecimalPoint);
		strcpy(GetDocument()->m_VtgUnitInfo.szUnit, szUnit) ;
		if(strTemp == "V")
		{
			GetDocument()->m_VtgUnitInfo.fTransfer = 1000.0f ;
		}
		else //if(strTemp == "mV")	//default V, uV 표시 
		{
			GetDocument()->m_VtgUnitInfo.fTransfer = 1.0f ;
		}
		
		//Current Unit
		sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "I Unit", "mA 1"));
		sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
		strTemp = szUnit;
		GetDocument()->m_CrtUnitInfo.nFloatPoint = atoi(szDecimalPoint);
		strcpy(GetDocument()->m_CrtUnitInfo.szUnit, szUnit) ;
		if(strTemp == "A")
		{
			GetDocument()->m_CrtUnitInfo.fTransfer = 1000.0f ;
		}
		else //if(strTemp == "mA")	//default mA, uA 표시 
		{
			GetDocument()->m_CrtUnitInfo.fTransfer = 1.0f ;
		}
		
		//Capacity Unit
		sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "C Unit", "mAh 1"));
		sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
		strTemp = szUnit;
		GetDocument()->m_CapUnitInfo.nFloatPoint = atoi(szDecimalPoint);
		strcpy(GetDocument()->m_CapUnitInfo.szUnit, szUnit) ;
		if(strTemp == "Ah" || strTemp == "F")
		{
			GetDocument()->m_CapUnitInfo.fTransfer = 1000.0f ;
		}
		else //if(strTemp == "mA")	//default mAh or uAh 표시 
		{
			GetDocument()->m_CapUnitInfo.fTransfer = 1.0f ;
		}
		
		//Watt Unit
		sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "W Unit", "mW 1"));
		sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
		strTemp = szUnit;
		GetDocument()->m_WattUnitInfo.nFloatPoint = atoi(szDecimalPoint);
		strcpy(GetDocument()->m_WattUnitInfo.szUnit, szUnit) ;
		if(strTemp == "W")
		{
			GetDocument()->m_WattUnitInfo.fTransfer = 1000.0f ;
		}
		// 1849 : 20200128 KW 조건 추가
		else if(strTemp == "KW")
		{
			GetDocument()->m_WattUnitInfo.fTransfer = 1000000.0f ;
		}
		else //if(strTemp == "mW")	//default mW or uW 표시 
		{
			GetDocument()->m_WattUnitInfo.fTransfer = 1.0f ;
		}
		
		//WattHour
		sprintf(szBuff, AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "Wh Unit", "mWh 1"));
		sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
		strTemp = szUnit;
		GetDocument()->m_WattHourUnitInfo.nFloatPoint = atoi(szDecimalPoint);
		strcpy(GetDocument()->m_WattHourUnitInfo.szUnit, szUnit) ;
		if(strTemp == "Wh")
		{
			GetDocument()->m_WattHourUnitInfo.fTransfer = 1000.0f ;
		}
		// 1849 : 20200128 KWh 조건 추가
		else if(strTemp == "KWh")
		{
			GetDocument()->m_WattHourUnitInfo.fTransfer = 1000000.0f ;
		}
		else //if(strTemp == "mWh")	//default mWh, uWh 표시 
		{
			GetDocument()->m_WattHourUnitInfo.fTransfer = 1.0f ;
		}

		DisplayStepGrid();
	}

	delete pDlg;
	pDlg = NULL;
}


//int CCTSEditorProView::RequeryProcDataTypeID()
//{
//	BOOL	bRtn = TRUE;
//	CCTSEditorProDoc *pDoc = GetDocument();
//	DATA_CODE *pData = NULL;
//	
//	CProcDataRecordSet	rs;
//	rs.m_strSort.Format("[ProgressID]");
//
//	try
//	{
//		if(rs.Open() == FALSE)	return FALSE;
//	}
//	catch (CDBException* e)
//	{
//		e->Delete();
//	}
//
//	if(rs.IsOpen())
//	{
//		while(!rs.IsEOF())
//		{
//			pData = new DATA_CODE;
//			pData->nCode = rs.m_ProgressID;
//			sprintf(pData->szMessage, "%s", rs.m_Name);
//			pData->nDataType = rs.m_DataType;
//			pDoc->m_apProcData.Add(pData);
//			rs.MoveNext();
//		}
//		rs.Close();
//		bRtn = TRUE;
//	}
//	else
//	{
//		pData =  new DATA_CODE;
//		ASSERT(pData);
//		pData->nCode = 0;
//		sprintf(pData->szMessage, "Unknown");
//		pDoc->m_apProcData.Add(pData);
//		bRtn = FALSE;
//	}
//	return bRtn;
//}

BOOL CCTSEditorProView::UpdateTestNo()
{
	CTestListRecordSet	recordSet;
	recordSet.m_strSort.Format("[TestID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	ROWCOL nRow = m_wndTestListGrid.GetRowCount();
	CString key;

	for(int i=0; i<nRow; i++)
	{
		key.Format("TestID = %s", m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY));
		recordSet.Find(AFX_DAO_FIRST, key);
		recordSet.Edit();
		recordSet.m_TestNo = atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_NO));
		recordSet.Update();
	}
	recordSet.Close();
	return TRUE;
}


BOOL CCTSEditorProView::UpdateTypeComboList()
{
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	m_ptProcArray.RemoveAll();
	STEP *pStep;
	SCH_CODE_MSG procType;

	int nTotStep = pDoc->GetTotalStepNum();
	CString strProcType, strTemp;

	for(int j = 0; j < pDoc->m_apProcType.GetSize(); j++)
	{
		memcpy(&procType, pDoc->m_apProcType[j], sizeof(SCH_CODE_MSG));
		int i = 0;
		for(i = 0 ; i<nTotStep; i++)
		{
			pStep = (STEP *)pDoc->GetStepData(i);
			if(pStep->nProcType == procType.nCode)
			{
				break;
			}
		}
		if( i >= nTotStep)
		{
			m_ptProcArray.Add(procType);
			strTemp.Format("%s\n", procType.szMessage);
			strProcType += strTemp;
		}
	}

	m_wndStepGrid.SetStyleRange(CGXRange().SetCols(COL_STEP_PROCTYPE),	CGXStyle().SetChoiceList(strProcType));
	return TRUE;
}

void CCTSEditorProView::UpdateModelNo()
{
	CBatteryModelRecordSet	recordSet;
	recordSet.m_strSort.Format("[ModelID]");

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	CString key;

	for(int i=0; i<nRow; i++)
	{
		key.Format("[ModelID] = %s", m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY));
		recordSet.Find(AFX_DAO_FIRST, key);
		recordSet.Edit();
		recordSet.m_No = atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NO));
		recordSet.Update();
	}
	recordSet.Close();
	return ;
}


//모델 목록 복사 
BOOL CCTSEditorProView::CopyModel(ROWCOL nRow)
{
	if(nRow < 1)	return FALSE;

	CDaoDatabase  db;
	CCTSEditorProDoc *pDoc =  GetDocument();

//	CString strDataBaseName;
//	strDataBaseName = pDoc->m_strDBFolder+ "\\" +FORM_SET_DATABASE_NAME;
	db.Open(g_strDataBaseName);

	int nCount = m_pWndCurModelGrid->GetRowCount() + 1;
	CString name = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_NAME);
	CString descript =  m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_DESCRIPTION);
	long lModelID = atol(m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_PRIMARY_KEY));

	//새로운 명을 Dialog 입력 받도록 수정 /////////////////
	CModelNameDlg	dlg;
	dlg.m_strName = name;
	dlg.m_strDescript = descript;
	dlg.m_strUserID = GetDocument()->m_LoginData.szLoginID;
	if(IDOK != dlg.DoModal())
	{
		return TRUE;
	}
	name = dlg.m_strName;
	descript = dlg.m_strDescript;
	//////////////////////////////////////////


	CString strSQL;

#ifdef _USE_MODEL_USER_
	strSQL.Format("INSERT INTO BatteryModel(No, ModelName, Description) VALUES (%d, '%s', '%s')", nCount, name, descript);
#else
	strSQL.Format("INSERT INTO BatteryModel(No, ModelName, Description, Creator) VALUES (%d, '%s', '%s', '%s')", nCount, name, descript, dlg.m_strUserID);
#endif

	db.Execute(strSQL);

	//Insert 된 Model의 ID를 구한다.
	long newModelID;
	strSQL = "SELECT MAX(ModelID) FROM BatteryModel";
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	if(!rs.IsBOF())
	{
		COleVariant data = rs.GetFieldValue(0);
		newModelID = data.lVal;
	}
	rs.Close();
	db.Close();

	m_pWndCurModelGrid->InsertRows(nCount, 1);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_NAME), name);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_DESCRIPTION), descript);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_PRIMARY_KEY), newModelID);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_CREATOR), dlg.m_strUserID);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nCount, COL_MODEL_EDIT_STATE), CS_SAVED);
	m_pWndCurModelGrid->SetCurrentCell(nCount, 1);

	CopyTestList(lModelID, newModelID );

	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
	
	return TRUE;
}

//TestName 목록 복사 
BOOL CCTSEditorProView::CopyTestList(int lFromModelID, int lToModelID)
{
	CDaoDatabase  db;
	CCTSEditorProDoc *pDoc =  GetDocument();

	db.Open(g_strDataBaseName);

	CString name, descript, creator;
	long check, typeID, testNo, lTestID, lFromTestID;
	COleDateTime modtime;

	CString strSQL;
	strSQL.Format("SELECT TestID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime FROM TestName WHERE ModelID = %d ORDER BY TestNo", lFromModelID);
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);		lFromTestID = data.lVal;
		data = rs.GetFieldValue(1);		name = data.pbVal;
		data = rs.GetFieldValue(2);		descript = data.pbVal;
		data = rs.GetFieldValue(3);		creator = data.pbVal;
		data = rs.GetFieldValue(4);		check = data.lVal;
		data = rs.GetFieldValue(5);		typeID = data.lVal;
		data = rs.GetFieldValue(6);		testNo = data.lVal;
//		data = rs.GetFieldValue(7);		modtime = data;
		modtime = COleDateTime::GetCurrentTime();
		rs.MoveNext();
		strSQL.Format("INSERT INTO TestName(ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime) VALUES (%d, '%s', '%s', '%s', %d, %d, %d, '%s')",
			lToModelID, name, descript, creator, check, typeID, testNo, modtime.Format());

		db.Execute(strSQL);

		//Insert 된 test의 ID를 구한다.
		strSQL = "SELECT MAX(TestID) FROM TestName";
		CDaoRecordset rs1(&db);
		rs1.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs1.IsBOF())
		{
			lTestID = rs1.GetFieldValue(0).lVal;
		}

		CopyCheckParam(lFromTestID, lTestID);
		CopyStepList(lFromTestID, lTestID);
		
		rs1.Close();
	}
	rs.Close();

	db.Close();

	return TRUE;
}

BOOL CCTSEditorProView::CopyStepList(long lFromTestID, long lToTestID)
{
	
	CDaoDatabase  db;
	db.Open(g_strDataBaseName);

	CString strSQL;
	CStepRecordSet rs, rs1;
	rs.m_strFilter.Format("[TestID] = %d", lFromTestID);
	rs.m_strSort = "[StepNo]";

	rs.Open();
	rs1.Open();

	while(!rs.IsEOF())
	{
		rs1.AddNew();					
//		rs1.m_StepID = rs.m_StepID;				//1
		rs1.m_ModelID = rs.m_ModelID;
		rs1.m_TestID = lToTestID;
		rs1.m_StepNo = rs.m_StepNo;
		rs1.m_StepType = rs.m_StepType;
		rs1.m_StepMode = rs.m_StepMode;
		rs1.m_Vref = rs.m_Vref;
		rs1.m_Vref_DisCharge = rs.m_Vref_DisCharge;
		rs1.m_Iref = rs.m_Iref;
		rs1.m_Pref = rs.m_Pref;
		rs1.m_EndTime = rs.m_EndTime;
		rs1.m_EndV = rs.m_EndV;		//10
		rs1.m_EndI = rs.m_EndI;
		rs1.m_CycleCount = rs.m_CycleCount;
		rs1.m_EndCapacity = rs.m_EndCapacity;
		rs1.m_End_dV = rs.m_End_dV;
		rs1.m_End_dI = rs.m_End_dI;
		rs1.m_OverV = rs.m_OverV;
		rs1.m_LimitV = rs.m_LimitV;
		rs1.m_OverI = rs.m_OverI;
		rs1.m_LimitI = rs.m_LimitI;
		rs1.m_OverCapacity = rs.m_OverCapacity;	//20
		rs1.m_LimitCapacity = rs.m_LimitCapacity;
		rs1.m_OverImpedance = rs.m_OverImpedance;
		rs1.m_LimitImpedance = rs.m_LimitImpedance;
		rs1.m_DeltaTime = rs.m_DeltaTime;
		rs1.m_DeltaV = rs.m_DeltaV;
		rs1.m_DeltaTime1 = rs.m_DeltaTime1;
		rs1.m_DeltaI = rs.m_DeltaI;
		rs1.m_Grade = rs.m_Grade;
		rs1.m_CompTimeV1 = rs.m_CompTimeV1;
		rs1.m_CompTimeV2 = rs.m_CompTimeV2;		//30
		rs1.m_CompTimeV3 = rs.m_CompTimeV3;
		rs1.m_CompVLow1 = rs.m_CompVLow1;
		rs1.m_CompVLow2 = rs.m_CompVLow2;
		rs1.m_CompVLow3 = rs.m_CompVLow3;
		rs1.m_CompVHigh1 = rs.m_CompVHigh1;
		rs1.m_CompVHigh2 = rs.m_CompVHigh2;
		rs1.m_CompVHigh3 = rs.m_CompVHigh3;

		rs1.m_CompTimeI1 = rs.m_CompTimeI1;
		rs1.m_CompTimeI2 = rs.m_CompTimeI2;
		rs1.m_CompTimeI3 = rs.m_CompTimeI3;		//40
		rs1.m_CompILow1 = rs.m_CompILow1;
		rs1.m_CompILow2 = rs.m_CompILow2;
		rs1.m_CompILow3 = rs.m_CompILow3;
		rs1.m_CompIHigh1 = rs.m_CompIHigh1;
		rs1.m_CompIHigh2 = rs.m_CompIHigh2;
		rs1.m_CompIHigh3 = rs.m_CompIHigh3;

		rs1.m_CapVHigh		= rs.m_CapVHigh;	
		rs1.m_CapVLow		= rs.m_CapVLow	;
		
		rs1.m_RecordDeltaI	= rs.m_RecordDeltaI;
		rs1.m_RecordDeltaV	= rs.m_RecordDeltaV;		//50
		rs1.m_RecordTime	= rs.m_RecordTime;

		rs1.m_EndCheckVLow  = rs.m_EndCheckVLow ;
		rs1.m_EndCheckVHigh = rs.m_EndCheckVHigh ;
		rs1.m_EndCheckILow  = rs.m_EndCheckILow ;
		rs1.m_EndCheckIHigh	= rs.m_EndCheckIHigh;

		rs1.m_strGotoValue  = rs.m_strGotoValue;
		rs1.m_strReportTemp = rs.m_strReportTemp;
		rs1.m_strSocEnd  = rs.m_strSocEnd;
		rs1.m_strEndWatt  = rs.m_strEndWatt;
		rs1.m_strTempLimit  = rs.m_strTempLimit;		//60	
		
		//2014.09.15 PS_STEP_USER_MAP 추가.
		if(rs.m_StepType == PS_STEP_PATTERN || rs.m_StepType == PS_STEP_USER_MAP){
			rs1.m_strSimulFile  = NewSimulationFile(rs.m_strSimulFile);
		}
		else{
			rs1.m_strSimulFile = rs.m_strSimulFile;
		}		

		rs1.m_ValueLimitLow = rs.m_ValueLimitLow;
		rs1.m_ValueLimitHigh  = rs.m_ValueLimitHigh;
		
		rs1.m_StepProcType = rs.m_StepProcType;		//66

		rs1.m_ValueBMSandAUX  = rs.m_ValueBMSandAUX ;

		rs1.m_strCanCategory  = rs.m_strCanCategory ;
		rs1.m_strCanCompareType  = rs.m_strCanCompareType ;
		rs1.m_strCanValue  = rs.m_strCanValue ;
		rs1.m_strCanGoto  = rs.m_strCanGoto ;
		rs1.m_strAuxCategory  = rs.m_strAuxCategory ;
		rs1.m_strAuxCompareType  = rs.m_strAuxCompareType ;
		rs1.m_strAuxValue  = rs.m_strAuxValue ;
		rs1.m_strAuxGoto  = rs.m_strAuxGoto ;

		rs1.m_ValueOven  = rs.m_ValueOven;			//ljb6 add

		rs1.m_strCanCategory  = rs.m_strCanCategory;			//ljb 20100928 add
		rs1.m_strCanDataType  = rs.m_strCanDataType;			//ljb 20130114 add
		
		rs1.m_strCanCompareType  = rs.m_strCanCompareType;		//ljb 20100928 add
		rs1.m_strCanGoto  = rs.m_strCanGoto;					//ljb 20100928 add
		rs1.m_strCanValue  = rs.m_strCanValue;					//ljb 20100928 add
		rs1.m_strAuxCategory  = rs.m_strAuxCategory;			//ljb 20100928 add
		rs1.m_strAuxCompareType  = rs.m_strAuxCompareType;		//ljb 20100928 add
		rs1.m_strAuxGoto  = rs.m_strAuxGoto;					//ljb 20100928 add
		rs1.m_strAuxValue  = rs.m_strAuxValue;					//ljb 20100928 add
		rs1.m_strAuxDataType = rs.m_strAuxDataType;				//2014.09.23 스케줄 복사 사용시 종료조건 옥스 데이터 미적용 수정.		

		//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
		UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
		if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
		{
			rs1.m_CellDeltaVStep		=	rs.m_CellDeltaVStep;
			rs1.m_StepDeltaAuxTemp		=	rs.m_StepDeltaAuxTemp;
			rs1.m_StepDeltaAuxTh		=	rs.m_StepDeltaAuxTh;
			rs1.m_StepDeltaAuxT			=	rs.m_StepDeltaAuxT;
			rs1.m_StepDeltaAuxVTime		=	rs.m_StepDeltaAuxVTime;
			rs1.m_StepAuxV				=	rs.m_StepAuxV;

			rs1.m_StepDeltaVentAuxV		=	rs.m_StepDeltaVentAuxV;
			rs1.m_StepDeltaVentAuxTemp	=	rs.m_StepDeltaVentAuxTemp;
			rs1.m_StepDeltaVentAuxTh	=	rs.m_StepDeltaVentAuxTh;
			rs1.m_StepDeltaVentAuxT		=	rs.m_StepDeltaVentAuxT;
			rs1.m_StepVentAuxV			=	rs.m_StepVentAuxV;
		}

		rs1.Update();
		
		if(rs.m_Grade)
		{
			strSQL = "SELECT MAX(StepID) FROM Step";
			CDaoRecordset rs2(&db);
			rs2.Open(dbOpenSnapshot, strSQL, dbReadOnly);
			if(!rs2.IsBOF())
			{
				CopyGradeStep(rs.m_StepID, rs2.GetFieldValue(0).lVal);
			}
			rs2.Close();
		}
		rs.MoveNext();
	}
	rs.Close();
	rs1.Close();
	db.Close();

/*	CDaoDatabase  db;
	CCTSEditorProDoc *pDoc =  GetDocument();

	db.Open(g_strDataBaseName);

	CString strSQL;
	strSQL.Format("SELECT TestID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo FROM TestName WHERE ModelID = %d ORDER BY TestNo", lFromTestID);
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);		lFromTestID = data.lVal;
		data = rs.GetFieldValue(1);		name = data.pbVal;
		data = rs.GetFieldValue(2);		descript = data.pbVal;
		data = rs.GetFieldValue(3);		creator = data.pbVal;
		data = rs.GetFieldValue(4);		check = data.lVal;
		data = rs.GetFieldValue(5);		typeID = data.lVal;
		data = rs.GetFieldValue(6);		testNo = data.lVal;
		strSQL.Format("INSERT INTO TestName(ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo) VALUES (%d, '%s', '%s', '%s', %d, %d, %d)",
			lToModelID, name, descript, creator, check, typeID, testNo);

		db.Execute(strSQL);

		//Insert 된 test의 ID를 구한다.
		strSQL = "SELECT MAX(TestID) FROM TestName";
		CDaoRecordset rs1(&db);
		rs1.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		if(!rs1.IsBOF())
		{
			lTestID = rs1.GetFieldValue(0).lVal;
		}
		rs1.Close();
		
		rs.MoveNext();
	}
	rs.Close();

	db.Close();

*/	return TRUE;
}

BOOL CCTSEditorProView::CopyCheckParam(long lFromTestID, long lToTestID)
{
//	CCTSEditorProDoc *pDoc =  GetDocument();

	CPreTestCheckRecordSet rs;
	rs.m_strFilter.Format("[TestID] = %d", lFromTestID); 
	rs.m_strSort = "[CheckID]";

	rs.Open();

	if(!rs.IsEOF())
	{
		float maxV = rs.m_SafetyMaxV;
		float minV = rs.m_SafetyMinV;
		float maxI = rs.m_SafetyMaxI;
		float maxC = rs.m_SafetyMaxC;
		float maxW = rs.m_SafetyMaxW;
		float maxWH = rs.m_SafetyMaxWh;
		float minT = rs.m_SafetyMinT;
		float maxT = rs.m_SafetyMaxT;
		float maxCellDevVolt = rs.m_SafetyCellDeltaV;   //ljb 20170810 add cell delta voltage
		float crtRange = rs.m_CurrentRange;
		float ocvMax = rs.m_OCVLimit;
		float trickle_I = rs.m_TrickleCurrent;
		long trickle_time = rs.m_TrickleTime;
		float deltaV = rs.m_DeltaVoltage;
		int faultNo = rs.m_MaxFaultBattery;
		COleDateTime autoTime = rs.m_AutoTime;
		BOOL autoYN = rs.m_AutoProYN;
		BOOL check = rs.m_PreTestCheck;
		CString strCanCategory = rs.m_strCanCategory;		//ljb 20100928
		CString strCanCompareType = rs.m_strCanCompareType;	//ljb 20100928
		CString strCanDataType = rs.m_strCanDataType;		//ljb 20100928
		CString strCanValue = rs.m_strCanValue;				//ljb 20100928
		
		CString strAuxCategory = rs.m_strAuxCategory;		//ljb 20100928
		CString strAuxCompareType = rs.m_strAuxCompareType;	//ljb 20100928
		CString strAuxDataType = rs.m_strAuxDataType;		//ljb 20100928
		CString strAuxValue = rs.m_strAuxValue;				//ljb 20100928

		float STDdeltaMaxV = rs.m_DeltaStdMaxV;
		float STDdeltaMinV = rs.m_DeltaStdMinV;
		float deltaMaxV = rs.m_DeltaMaxV;
		float deltaMinV = rs.m_DeltaMinV;
		BOOL DeltaVent = rs.m_DeltaVent;

		rs.AddNew();
		rs.m_TestID = lToTestID;
		rs.m_SafetyMaxV = maxV;
		rs.m_SafetyMinV = minV;
		rs.m_SafetyMaxI = maxI;
		rs.m_SafetyMaxC = maxC;
		rs.m_SafetyMaxW = maxW;
		rs.m_SafetyMaxWh = maxWH;
		rs.m_SafetyMinT = minT;
		rs.m_SafetyMaxT = maxT;
		rs.m_SafetyCellDeltaV = maxCellDevVolt;   //ljb 20170810 add cell delta voltage
		rs.m_CurrentRange = crtRange;
		rs.m_OCVLimit = ocvMax;
		rs.m_TrickleCurrent = trickle_I;
		rs.m_TrickleTime = trickle_time;
		rs.m_DeltaVoltage = deltaV;
		rs.m_DeltaStdMaxV = STDdeltaMaxV;
		rs.m_DeltaStdMinV = STDdeltaMinV;
		rs.m_DeltaMaxV = deltaMaxV ;
		rs.m_DeltaMinV = deltaMinV ;
		rs.m_DeltaVent = DeltaVent ;
		rs.m_MaxFaultBattery = faultNo;
		rs.m_AutoTime = autoTime;
		TRACE("%s\n", rs.m_AutoTime.Format());
		rs.m_AutoProYN = autoYN;
		rs.m_PreTestCheck = check;
		rs.m_strCanCategory = strCanCategory;
		rs.m_strCanCompareType = strCanCompareType;
		rs.m_strCanDataType = strCanDataType;
		rs.m_strCanValue = strCanValue;

		rs.m_strAuxCategory = strAuxCategory;
		rs.m_strAuxCompareType = strAuxCompareType;
		rs.m_strAuxDataType = strAuxDataType;
		rs.m_strAuxValue = strAuxValue;		
		rs.Update();
	}
	
	rs.Close();




/*	CDaoDatabase  db;

	db.Open(g_strDataBaseName);

	CString strSQL, strTemp;;
	strSQL.Format("SELECT MaxV, MinV, CurrentRange, OCVLimit, TrickleCurrent, TrickleTime, DeltaVoltage, MaxFaultBattery, AutoTime, AutoProYN, PreTestCheck FROM Check WHERE TestID = %d ORDER BY CheckID", lFromTestID);
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	while(!rs.IsEOF())//TrickleTime, AutoTime
	{
		strSQL = "INSERT INTO Check(TestID, MaxV, MinV, CurrentRange, OCVLimit, TrickleCurrent, DeltaVoltage, MaxFaultBattery, AutoProYN, PreTestCheck )";
		strSQL += " VALUES (";
		
		strTemp.Format("%d", lToTestID);	strSQL += strTemp;	
		data = rs.GetFieldValue(0);		strTemp.Format(", %.3f", data.fltVal);	strSQL += strTemp;	//MAX V	
		data = rs.GetFieldValue(1);		strTemp.Format(", %.3f", data.fltVal);	strSQL += strTemp;	//MIN V
		data = rs.GetFieldValue(2);		strTemp.Format(", %.1f", data.fltVal);	strSQL += strTemp;	//Current Range
		data = rs.GetFieldValue(3);		strTemp.Format(", %.3f", data.fltVal);	strSQL += strTemp;	//OCVLimit
		data = rs.GetFieldValue(4);		strTemp.Format(", %.1f", data.fltVal);	strSQL += strTemp;	//TrickleCurrent
		data = rs.GetFieldValue(5);		strTemp.Format(", '%s'", data.pbVal);		strSQL += strTemp;		//TrickleTime
		data = rs.GetFieldValue(6);		strTemp.Format(", %.3f", data.fltVal);	strSQL += strTemp;		//DeltaVoltage
		data = rs.GetFieldValue(7);		strTemp.Format(", %d", data.lVal);		strSQL += strTemp;		//MaxFaultBattery
		data = rs.GetFieldValue(8);		strTemp.Format(", %d", data.date);		strSQL += strTemp;		//AutoTime
		data = rs.GetFieldValue(9);		strTemp.Format(", %d", data.boolVal);		strSQL += strTemp;		//AutoProYN
		data = rs.GetFieldValue(10);	strTemp.Format(", %d", data.boolVal);		strSQL += strTemp;	//PreTestCheck

		strSQL += ")";
		db.Execute(strSQL);		
		rs.MoveNext();
	}
	rs.Close();
	db.Close();
*/
	return TRUE;
}

BOOL CCTSEditorProView::CopyGradeStep(long lFromStepID, long lToStepID)
{
	CGradeRecordSet rs, rs1;
	rs.m_strFilter.Format("[StepID] = %d", lFromStepID);
	rs.m_strSort = "[GradeID]";

	rs.Open();
	rs1.Open();

	while(!rs.IsEOF())
	{
		rs1.AddNew();					
		rs1.m_StepID = lToStepID;
		rs1.m_GradeItem = rs.m_GradeItem;
		rs1.m_Value = rs.m_Value;
		rs1.m_Value1 = rs.m_Value1;
		rs1.m_GradeCode = rs.m_GradeCode;
		rs1.Update();
		rs.MoveNext();
	}
	rs.Close();
	rs1.Close();
	return TRUE;
}

void CCTSEditorProView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)		return;

	if(BackUpStep(awRows))
	{
		m_bCopyed = TRUE;		//새로운 Step이 Copy 되었을시 
		m_bPasteUndo = FALSE;	//붙여넣기 취소 불가능 
	}
}

void CCTSEditorProView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

void CCTSEditorProView::OnEditPaste() 
{
/*
	CPowerEditorDoc *pDoc = GetDocument();
	if(pDoc->m_apCopyStep.GetSize() < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	if(nRow <= 0)	nRow =1;
	pDoc->PasteStep(nRow);	

	DisplayStepGrid();		//전체 Step ReDraw

	DisplayStepGrid(nRow);	//현재 Display Step을 붙여 넣은 첫번째 Step을 표시 

	m_bPasteUndo = TRUE;	//붙여넣기 Undo 가능 
	m_bCutUndo = FALSE;		//잘라내기 Undo 불가능  
	m_bStepDataSaved = FALSE;
*/
	
	//덮어 쓰도록 수정 
	// TODO: Add your command handler code here
	CCTSEditorProDoc *pDoc = GetDocument();
	int nCopySize = pDoc->m_apCopyStep.GetSize();
	if(nCopySize < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	nRow = awRows[0];

	if(nRow <= 0)	nRow = 1;

	STEP *pStep, *pCopyStep;
	int nStartRow = nRow;
	int nEndRow = nStartRow+nCopySize-1;
	
	int nDestRow = 0;
	int nOrgRowCount = m_wndStepGrid.GetRowCount();

	AddUndoStep();

	for(int i = 0; i<nCopySize; i++)
	{
		nDestRow = nRow+i;
	
		//이미 존재하면 덮어쓴다.
		if(nDestRow <= nOrgRowCount)	
		{
			pStep = (STEP *)pDoc->GetStepData(nDestRow);
			pCopyStep = (STEP *)pDoc->m_apCopyStep[i];
				
			if(pCopyStep->chType == PS_STEP_PATTERN)
			{
				CString temp(pCopyStep->szSimulFile);
				
				sprintf(pCopyStep->szSimulFile, NewSimulationFile(temp));
			}
			//2014.09.02 PS_STEP_USER_MAP 추가함
			else if(pCopyStep->chType == PS_STEP_USER_MAP){
				CString temp(pCopyStep->szUserMapFile);
				
				sprintf(pCopyStep->szUserMapFile, NewSimulationFile(temp));
			}

			if(pStep && pCopyStep)
			{
				memcpy(pStep, pCopyStep,sizeof(STEP));
				DisplayStepGrid(nDestRow);						//변경 결과를 반영하기 위해 반드시 필요함 
			}
		}
		else		//새로운 step을 삽입한다.
		{
			pCopyStep = (STEP *)pDoc->m_apCopyStep[i];
			//2014.09.02 PS_STEP_USER_MAP 추가함
			if(pCopyStep->chType == PS_STEP_PATTERN || pCopyStep->chType == PS_STEP_USER_MAP){
				AfxMessageBox("Pattern2");
			}

			if(pCopyStep)
			{
				AddStepNew(nDestRow, FALSE, pCopyStep);
				DisplayStepGrid(nDestRow);						//변경 결과를 반영하기 위해 반드시 필요함 
			}
		}
	}

	//가장 마지막이 End Step이 아니면 빈 step을 하나 넣는다.
	//2006/4/27
	if((nRow+nCopySize) >= nOrgRowCount)
	{
		pCopyStep = pDoc->GetLastStepData();
		if(pCopyStep)
		{
			if(pCopyStep->chType != PS_STEP_END)
			{
				AddStepNew(m_wndStepGrid.GetRowCount()+1);
			}
		}
	}

	m_wndStepGrid.SelectRange(CGXRange(nStartRow, 0, nEndRow, m_wndStepGrid.GetColCount()));

	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(m_bCopyed );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

void CCTSEditorProView::OnEditInsert() 
{
	// TODO: Add your command handler code here
/*
	CPowerEditorDoc *pDoc = GetDocument();
	if(pDoc->m_apCopyStep.GetSize() < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	if(nRow <= 0)	nRow =1;
	pDoc->PasteStep(nRow);	

	DisplayStepGrid();		//전체 Step ReDraw

	DisplayStepGrid(nRow);	//현재 Display Step을 붙여 넣은 첫번째 Step을 표시 

	m_bPasteUndo = TRUE;	//붙여넣기 Undo 가능 
	m_bCutUndo = FALSE;		//잘라내기 Undo 불가능  
	m_bStepDataSaved = FALSE;
*/
	
	//삽입한다.
	// TODO: Add your command handler code here
	CCTSEditorProDoc *pDoc = GetDocument();
	int nCopySize = pDoc->m_apCopyStep.GetSize();
	if(nCopySize < 1)	return;

	ROWCOL nRow, nCol;
	if(m_wndStepGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;
	if(nRow <= 0)	nRow = 1;

	AddUndoStep();

	STEP *pCopyStep,*pStep;
	int nStartRow = nRow;
	int nEndRow = nStartRow+nCopySize-1;
	int nOrgRowCount = m_wndStepGrid.GetRowCount();
	//Undo 가능하도록 Backup을 해야 한다.
	int nDestRow;
	for(int i = 0; i<nCopySize; i++)
	{
		nDestRow = nStartRow + i;
		pCopyStep = (STEP *)pDoc->m_apCopyStep[i];
		AddStepNew(nDestRow, FALSE, pCopyStep);

		STEP * pDestStep = (STEP *)pDoc->GetStepData(nDestRow);
		
		if(pCopyStep->chType == PS_STEP_PATTERN)
		{
			CString temp(pCopyStep->szSimulFile);
			
			sprintf(pDestStep->szSimulFile, NewSimulationFile(temp));
		}
		//2014.09.02 PS_STEP_USER_MAP 추가함
		else if(pCopyStep->chType == PS_STEP_USER_MAP){
			CString temp(pCopyStep->szUserMapFile);
			
			sprintf(pCopyStep->szUserMapFile, NewSimulationFile(temp));
		}
// 		//AccGroup  누적 Goto 설정 값 수정
// 		for (int j=0; j< 5; j++)
// 		{
// 			if( m_nMultiGroupLoopInfoGoto[j] >= awSteps[i] && m_nMultiGroupLoopInfoGoto[j] != PS_STEP_NEXT) m_nMultiGroupLoopInfoGoto[j]--;
// 			if( m_nAccGroupLoopInfoGoto[j] >= awSteps[i]  && m_nAccGroupLoopInfoGoto[j] != PS_STEP_NEXT)	m_nAccGroupLoopInfoGoto[j]--;
// 		}

// 		if(nDestRow+1 < nStartRow && pStep->nLoopInfoGotoStep > nStartRow && pStep->nLoopInfoGotoStep != PS_GOTO_NEXT_CYC)		
// 		{
// 			if (pStep->nLoopInfoGotoStep != PS_STEP_NEXT)	pStep->nLoopInfoGotoStep += nCopySize;
// 			m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
// 			
// 		}

		TRACE("Insert Step to %d\n", nDestRow);
		DisplayStepGrid(nDestRow);
	}
	for(int i = 0; i<pDoc->GetTotalStepNum(); i++)
	{
		pStep = pDoc->GetStepData(i+1);
		if(pStep == NULL)	break;
		
		if(pStep->chType == PS_STEP_LOOP)
		{
			for (int j=0; j< 5; j++)
			{
				m_nMultiGroupLoopInfoGoto[j] = PS_STEP_NEXT;
				m_nAccGroupLoopInfoGoto[j] = PS_STEP_NEXT;
// 				if( m_nMultiGroupLoopInfoGoto[j] >= awSteps[i] && m_nMultiGroupLoopInfoGoto[j] != PS_STEP_NEXT) m_nMultiGroupLoopInfoGoto[j]--;
// 				if( m_nAccGroupLoopInfoGoto[j] >= awSteps[i]  && m_nAccGroupLoopInfoGoto[j] != PS_STEP_NEXT)	m_nAccGroupLoopInfoGoto[j]--;
			}
		}
	}
	Fun_UpdateMultiGroupList();
	Fun_UpdateAccGroupList();

	m_wndStepGrid.SelectRange(CGXRange(nStartRow, 0, nEndRow, m_wndStepGrid.GetColCount()));
	//DisplayStepGrid(nDestRow);


	//가장 마지막이 End Step이 아니면 빈 step을 하나 넣는다.
	//2006/4/27
	if(nCopySize >= (nOrgRowCount - nStartRow))
	{
		pCopyStep = pDoc->GetLastStepData();
		if(pCopyStep)
		{
			if(pCopyStep->chType != PS_STEP_END)
			{
				AddStepNew(m_wndStepGrid.GetRowCount()+1);
			}
		}
	}

	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnUpdateEditInsert(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(m_bCopyed );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}	
}


void CCTSEditorProView::OnEditCut() 
{
	// TODO: Add your command handler code here
	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)		return;

	if(BackUpStep(awRows, TRUE))
	{
		AddUndoStep();
		DeleteStep(awRows);
	}
}

void CCTSEditorProView::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

void CCTSEditorProView::OnEditUndo() 
{
	// TODO: Add your command handler code here
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	if(m_bPasteUndo)
	{
		int nStart,nEnd;
		m_bPasteUndo = pDoc->UpdoStepEdit(nStart,nEnd);
		DisplayStepGrid();
		//변경된 부분을 선택된 상태로 한다.
	}
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(m_bPasteUndo);
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
}

BOOL CCTSEditorProView::BackUpStep(CRowColArray &awRows, BOOL bEnableUndo)
{
/*	CRowColArray	awRows;
	m_wndStepGrid.GetSelectedRows(awRows);
*/	int nSelNo = awRows.GetSize();

	if(nSelNo < 1)
		return FALSE;

	CCTSEditorProDoc *pDoc = GetDocument();
	pDoc->ClearCopyStep();
	for (int i = 0; i<nSelNo; i++)
	{
		if(awRows[i] > 0)
		{
			pDoc->AddCopyStep(awRows[i]);
		}
	}
	m_bCopyed = bEnableUndo;		//붙여 넣기 가능 
	m_bStepDataSaved = FALSE;
	return TRUE;
}


BOOL CCTSEditorProView::DeleteStep(CDWordArray &awSteps)
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return FALSE;
	}
		
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	ROWCOL nRow = m_wndStepGrid.GetRowCount();
	if(nRow <1)		return FALSE;

//	CRowColArray	awSteps;		//Get Selected Step
//	m_wndStepGrid.GetSelectedRows(awSteps);

	if(awSteps.GetSize() < 1)	return FALSE;

	for (int i = awSteps.GetSize()-1; i >=0 ; i--)
	{
		TRACE("SELECT Row is %d\n", awSteps[i]);
		STEP * pStep;
//		TRACE("STEP num = %d\n",awSteps[i]);
//		pStep = pDoc->GetStepData(awSteps[i]);

		//ljb ------------------------------------
		STEP * pNextStep;	//ljb
		pNextStep = NULL;
		
		pStep = pDoc->GetStepData(awSteps[i]);
		
		if (pStep->chType != PS_STEP_END)
		{
			pNextStep = pDoc->GetStepData(awSteps[i]+1);
		}
		if (pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_CHARGE)
		{
			if (pStep->chMode == PS_MODE_CP && pNextStep->chType == pStep->chType && pNextStep->chMode == PS_MODE_CC)
				pNextStep->bUseLinkStep = FALSE;
		}
		//ljb ------------------------------------

		//Simulation 삭제시 연결된 파일도 삭제
		if(pStep->chType == PS_STEP_PATTERN)
		{
			CString temp(pStep->szSimulFile);
			DeleteSimulationFile(temp);
		}
		//2014.09.02 삭세 UserMap 파일 삭제. 
		if(pStep->chType == PS_STEP_USER_MAP){
			CString temp(pStep->szUserMapFile);
			DeleteSimulationFile(temp);
		}

		if(pStep->chType == PS_STEP_LOOP)
		{
			pStep->nAccLoopInfoGotoStep = PS_STEP_NEXT;
			m_nMultiGroupLoopInfoGoto[pStep->nMultiLoopGroupID] = PS_STEP_NEXT;
			m_nAccGroupLoopInfoGoto[pStep->nAccLoopGroupID] = PS_STEP_NEXT;
		}
		if(!pDoc->ReMoveStepArray(awSteps[i]))
		{
			//MessageBox("Step을 삭제 할수 없습니다.","Delete Error", MB_OK|MB_ICONSTOP);
			MessageBox(Fun_FindMsg("CTSEditorProView_DeleteStep_msg1","IDD_CTSEditorPro_CYCL"),"Delete Error", MB_OK|MB_ICONSTOP);//&&
			return FALSE;
		}

		//AccGroup  누적 Goto 설정 값 수정
		for (int j=0; j< 5; j++)
		{
			if( m_nMultiGroupLoopInfoGoto[j] >= awSteps[i] && m_nMultiGroupLoopInfoGoto[j] != PS_STEP_NEXT) m_nMultiGroupLoopInfoGoto[j]--;
			if( m_nAccGroupLoopInfoGoto[j] >= awSteps[i]  && m_nAccGroupLoopInfoGoto[j] != PS_STEP_NEXT)	m_nAccGroupLoopInfoGoto[j]--;
		}

		m_wndStepGrid.RemoveRows(awSteps[i], awSteps[i]);
	}

	//Added by KBH 2005/12/22 
	//삭제된 이후 Step에 goto 정보가 있을 경우 수정된 Step으로 변경한다.
	int nStartRow = awSteps[0];		// nStep-1+1 가장 처음 Step의 이전 Step보다 1Step 다음 Step
	int nLastRow = awSteps[awSteps.GetSize()-1];
	STEP *pStep;
	int jj=0;
	for(int i = 0; i<pDoc->GetTotalStepNum(); i++)
	{
		pStep = pDoc->GetStepData(i+1);
		if(pStep == NULL)	break;
		pStep->chStepNo = i+1;	//ljb 20150601 add

		if(pStep->chType == PS_STEP_ADV_CYCLE)	continue; //ljb 20150326 add  20151217 추가 수정 PS_STEP_PAUSE
		if(pStep->chType == PS_STEP_LOOP)
		{
			//삭제 step이전이면서 삭제 Step이후를 참조하면 변경 실시(삭제 Step이전을 참조하면 변경하지 않음)
			if(i+1 < nStartRow && pStep->nLoopInfoGotoStep > nStartRow && pStep->nLoopInfoGotoStep != PS_GOTO_NEXT_CYC)		
			{
				//20151217 아래 수정 nValueStepNo -> nLoopInfoGotoStep
				if (pStep->nLoopInfoGotoStep != PS_STEP_NEXT && pStep->nLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nLoopInfoGotoStep -= awSteps.GetSize();
				m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
			}
			
			//삭제 step이후 이면서 삭제 이후 Step을 참조하면 변경 실시(삭제된 Step이전을 참조하면 변경하지 않음)
			if(i+1 >= nStartRow && pStep->nLoopInfoGotoStep >= nStartRow && pStep->nLoopInfoGotoStep != PS_GOTO_NEXT_CYC)
			{
				//20151217 아래 수정 nValueStepNo -> nLoopInfoGotoStep
				if (pStep->nLoopInfoGotoStep != PS_STEP_NEXT && pStep->nLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nLoopInfoGotoStep -= awSteps.GetSize();	
				m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));
			}
		}
		else
		{
			//ljb 20150326 모든스텝 GOTO 분기 번호 조정
			//삭제 step이전이면서 삭제 Step이후를 참조하면 변경 실시(삭제 Step이전을 참조하면 변경하지 않음)
			if(i+1 < nStartRow && pStep->nValueStepNo > nStartRow && pStep->nValueStepNo != PS_GOTO_NEXT_CYC){
				if (pStep->nValueStepNo != PS_STEP_NEXT && pStep->nValueStepNo != PS_STEP_PAUSE)	pStep->nValueStepNo -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nMultiLoopInfoGotoStep > nStartRow && pStep->nMultiLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nMultiLoopInfoGotoStep != PS_STEP_NEXT && pStep->nMultiLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nMultiLoopInfoGotoStep -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nAccLoopInfoGotoStep > nStartRow && pStep->nAccLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nAccLoopInfoGotoStep != PS_STEP_NEXT && pStep->nAccLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nAccLoopInfoGotoStep -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nGotoStepEndV_H > nStartRow && pStep->nGotoStepEndV_H != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_H != PS_STEP_NEXT && pStep->nGotoStepEndV_H != PS_STEP_PAUSE)	pStep->nGotoStepEndV_H -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nGotoStepEndV_L > nStartRow && pStep->nGotoStepEndV_L != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_L != PS_STEP_NEXT && pStep->nGotoStepEndV_L != PS_STEP_PAUSE)	pStep->nGotoStepEndV_L -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nGotoStepEndTime > nStartRow && pStep->nGotoStepEndTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndTime != PS_STEP_NEXT && pStep->nGotoStepEndTime != PS_STEP_PAUSE)	pStep->nGotoStepEndTime -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nGotoStepEndCVTime > nStartRow && pStep->nGotoStepEndCVTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndCVTime != PS_STEP_NEXT && pStep->nGotoStepEndCVTime != PS_STEP_PAUSE)	pStep->nGotoStepEndCVTime -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nGotoStepEndC > nStartRow && pStep->nGotoStepEndC != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndC != PS_STEP_NEXT && pStep->nGotoStepEndC != PS_STEP_PAUSE)	pStep->nGotoStepEndC -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nGotoStepEndWh > nStartRow && pStep->nGotoStepEndWh != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndWh != PS_STEP_NEXT && pStep->nGotoStepEndWh != PS_STEP_PAUSE)	pStep->nGotoStepEndWh -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->nGotoStepEndValue > nStartRow && pStep->nGotoStepEndValue != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndValue != PS_STEP_NEXT && pStep->nGotoStepEndValue != PS_STEP_PAUSE)	pStep->nGotoStepEndValue -= awSteps.GetSize();
			}
			if(i+1 < nStartRow && pStep->bUseLinkStep > nStartRow && pStep->bUseLinkStep != PS_GOTO_NEXT_CYC){
				if (pStep->bUseLinkStep != PS_STEP_NEXT && pStep->bUseLinkStep != PS_STEP_PAUSE)	pStep->bUseLinkStep -= awSteps.GetSize();
			}
			for (jj=0; jj<SCH_MAX_END_CAN_AUX_POINT; jj++ )
			{
				if(i+1 < nStartRow && pStep->ican_branch[jj] > nStartRow && pStep->ican_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->ican_branch[jj] != PS_STEP_NEXT && pStep->ican_branch[jj] != PS_STEP_PAUSE)	pStep->ican_branch[jj] -= awSteps.GetSize();
				}
				if(i+1 < nStartRow && pStep->iaux_branch[jj] > nStartRow && pStep->iaux_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->iaux_branch[jj] != PS_STEP_NEXT && pStep->iaux_branch[jj] != PS_STEP_PAUSE)	pStep->iaux_branch[jj] -= awSteps.GetSize();
				}
			}		
			//삭제 step이후 이면서 삭제 이후 Step을 참조하면 변경 실시(삭제된 Step이전을 참조하면 변경하지 않음)
			if(i+1 >= nStartRow && pStep->nValueStepNo >= nStartRow && pStep->nValueStepNo != PS_GOTO_NEXT_CYC){
				if (pStep->nValueStepNo != PS_STEP_NEXT && pStep->nValueStepNo != PS_STEP_PAUSE)	pStep->nValueStepNo -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nMultiLoopInfoGotoStep >= nStartRow && pStep->nMultiLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nMultiLoopInfoGotoStep != PS_STEP_NEXT && pStep->nMultiLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nMultiLoopInfoGotoStep -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nAccLoopInfoGotoStep >= nStartRow && pStep->nAccLoopInfoGotoStep != PS_GOTO_NEXT_CYC){
				if (pStep->nAccLoopInfoGotoStep != PS_STEP_NEXT && pStep->nAccLoopInfoGotoStep != PS_STEP_PAUSE)	pStep->nAccLoopInfoGotoStep -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nGotoStepEndV_H >= nStartRow && pStep->nGotoStepEndV_H != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_H != PS_STEP_NEXT && pStep->nGotoStepEndV_H != PS_STEP_PAUSE)	pStep->nGotoStepEndV_H -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nGotoStepEndV_L >= nStartRow && pStep->nGotoStepEndV_L != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndV_L != PS_STEP_NEXT && pStep->nGotoStepEndV_L != PS_STEP_PAUSE)	pStep->nGotoStepEndV_L -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nGotoStepEndTime >= nStartRow && pStep->nGotoStepEndTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndTime != PS_STEP_NEXT && pStep->nGotoStepEndTime != PS_STEP_PAUSE)	pStep->nGotoStepEndTime -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nGotoStepEndCVTime >= nStartRow && pStep->nGotoStepEndCVTime != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndCVTime != PS_STEP_NEXT && pStep->nGotoStepEndCVTime != PS_STEP_PAUSE)	pStep->nGotoStepEndCVTime -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nGotoStepEndC >= nStartRow && pStep->nGotoStepEndC != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndC != PS_STEP_NEXT && pStep->nGotoStepEndC != PS_STEP_PAUSE)	pStep->nGotoStepEndC -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nGotoStepEndWh >= nStartRow && pStep->nGotoStepEndWh != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndWh != PS_STEP_NEXT && pStep->nGotoStepEndWh != PS_STEP_PAUSE)	pStep->nGotoStepEndWh -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->nGotoStepEndValue >= nStartRow && pStep->nGotoStepEndValue != PS_GOTO_NEXT_CYC){
				if (pStep->nGotoStepEndValue != PS_STEP_NEXT && pStep->nGotoStepEndValue != PS_STEP_PAUSE)	pStep->nGotoStepEndValue -= awSteps.GetSize();
			}
			if(i+1 >= nStartRow && pStep->bUseLinkStep >= nStartRow && pStep->bUseLinkStep != PS_GOTO_NEXT_CYC){
				if (pStep->bUseLinkStep != PS_STEP_NEXT && pStep->bUseLinkStep != PS_STEP_PAUSE)	pStep->bUseLinkStep -= awSteps.GetSize();
			}
			for (jj=0; jj<SCH_MAX_END_CAN_AUX_POINT; jj++ )
			{
				if(i+1 >= nStartRow && pStep->ican_branch[jj] >= nStartRow && pStep->ican_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->ican_branch[jj] != PS_STEP_NEXT && pStep->ican_branch[jj] != PS_STEP_PAUSE)	pStep->ican_branch[jj] -= awSteps.GetSize();
				}
				if(i+1 >= nStartRow && pStep->iaux_branch[jj] >= nStartRow && pStep->iaux_branch[jj] != PS_GOTO_NEXT_CYC){
					if (pStep->iaux_branch[jj] != PS_STEP_NEXT && pStep->iaux_branch[jj] != PS_STEP_PAUSE)	pStep->iaux_branch[jj] -= awSteps.GetSize();
				}
			}
			m_wndStepGrid.SetValueRange(CGXRange(i+1, COL_STEP_END), pDoc->MakeEndString(i+1));				
		}
	}

	
	nRow = m_wndStepGrid.GetRowCount();

	int nCol;
#ifdef _CYCLER_
	nCol = COL_STEP_TYPE;
#else
	nCol = COL_STEP_PROCTYPE;
#endif

	if(nRow >= nStartRow)		//Last step deleted
	{
		m_wndStepGrid.SetCurrentCell(nStartRow, nCol);
		DisplayStepGrid(nStartRow);						
	}
	else 
	{
		m_wndStepGrid.SetCurrentCell(nStartRow-1, nCol);
		DisplayStepGrid(nStartRow-1);						
	}


	return TRUE;
}

//Step Type의 BomboIndex를 검색 한다.
int CCTSEditorProView::GetStepIndexFromType(int nType)
{
	if(m_pStepTypeCombo == NULL)	return -1;
	
	for(int i=0; i<m_pStepTypeCombo->GetItemDataSize(); i++)
	{
		if(m_pStepTypeCombo->GetItemData(i) == nType)		return i;
	}
	return -1;
}

long CCTSEditorProView::GetModeIndex(int nMode)
{
	if(m_pStepModeCombo == NULL)	return -1;
	
	for(int i=0; i<m_pStepModeCombo->GetItemDataSize(); i++)
	{
		if(m_pStepModeCombo->GetItemData(i) == nMode)		return i;
	}
	return -1;
}



//Test Name의 Combo Index를 검색 한다.
int CCTSEditorProView::GetTestIndexFromType(int nType)
{
	if(m_pTestTypeCombo == NULL)	return -1;
	
	for(int i=0; i<m_pTestTypeCombo->GetItemDataSize(); i++)
	{
		if(m_pTestTypeCombo->GetItemData(i) == nType)		return i;
	}
	return -1;

}

void CCTSEditorProView::OnModelCopyButton() 
{
	// TODO: Add your control notification handler code here
	ModelCopy();
}

void CCTSEditorProView::OnJasonSchSave() //yulee 20190104 Jason format sch
{
#ifdef _DEBUG
	AfxMessageBox("Export Jason Format Sch");
#endif
}

void CCTSEditorProView::OnExcelSave() 
{
	// TODO: Add your command handler code here
	CScheduleData schData;
	if(schData.SetSchedule(GetDataBaseName(), m_lLoadedBatteryModelID, m_lLoadedTestID) == FALSE)
	{
		AfxMessageBox("Step data loading fail!!!");
		return;
	}

	CString strFileName;
	strFileName.Format("%s_%s.csv", schData.GetModelName(), schData.GetScheduleName());

	CFileDialog pDlg(FALSE, "csv", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
	}
#ifdef _CYCLER_
	if(schData.SaveToExcelFile(strFileName, TRUE, FALSE) == FALSE)
	{
		AfxMessageBox(strFileName+ Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL")); 
		return;		
	}
#else
	if(schData.SaveToExcelFile(strFileName, TRUE, TRUE) == FALSE)
	{
		AfxMessageBox(strFileName+ " save fail.");
		return;		
	}
#endif
}

void CCTSEditorProView::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_lLoadedBatteryModelID);
}

void CCTSEditorProView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(FALSE);	
}

BOOL CCTSEditorProView::WriteFile()
{
	CScheduleData schData;
	if(schData.SetSchedule(GetDataBaseName(), m_lLoadedBatteryModelID, m_lLoadedTestID) == FALSE)
	{
		AfxMessageBox("Step data loading fail!!!");
		return FALSE;
	}

	CString FileName;
	FileName.Format("%s_%s.sch", schData.GetModelName(), schData.GetScheduleName());

	//CFileDialog pDlg(FALSE, "csv", FileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "sch file(*.sch)|*.sch|");
	CFileDialog pDlg(FALSE, "csv", FileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "sch file (*.sch)|*.sch|sch file [v1.15.x~v1.16.x](*.sch)|*.sch|sch file [v1013](*.sch)|*.sch|"); //ksj 20210113 : sch 파일 하위 버전 컨버팅 저장 기능 추가.

	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return FALSE;
	}
	
	/*if(schData.SaveToFile(FileName) == FALSE)
	{
		AfxMessageBox(FileName+ Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL")); 
		return FALSE;
	}*/


	//ksj 20210113 : 버전별 sch 파일 선택 저장 기능 추가.
	int nSelFilter = pDlg.m_pOFN->nFilterIndex; //파일 저장 창에서 선택한 type 순서.

	switch(nSelFilter)
	{	
	case 1:
		if(schData.SaveToFile(FileName) == FALSE)
		{
			AfxMessageBox(FileName+ Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL")); 
			return FALSE;
		}
		break;
	case 2: //sch file [v1.15.x~v1.16.x](*.sch) 위에 CFileDialog 초기화시 파일 확장자 필터 순서 바뀌면 여기도 바뀜. 주의!!
		if(schData.SaveToFile_v1015(FileName) == FALSE)
		{
			AfxMessageBox(FileName+ Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL")); 
			return FALSE;
		}
		break;
	case 3: //sch file [v1013](*.sch) 위에 CFileDialog 초기화시 파일 확장자 필터 순서 바뀌면 여기도 바뀜. 주의!!
		if(schData.SaveToFile_v1013(FileName) == FALSE)
		{
			AfxMessageBox(FileName+ Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL")); 
			return FALSE;
		}
		break;
	default:
		{
			CString strTemp;
			strTemp.Format("File ext Error code :%d",nSelFilter);
			AfxMessageBox(strTemp);
			return FALSE;
		}
	}

	return TRUE;
	
	/*CString FileName, strTemp;
	ROWCOL nModelRow = 0, nTestRow = 0;
	//현재 Loading된 모델을 Model Grid에서 찾는다.
	for(int i=0; i<m_pWndCurModelGrid->GetRowCount(); i++)
	{
		if(m_lLoadedBatteryModelID == atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY)))
		{
			nModelRow =  i+1;
			break;
		}
	}
	if(nModelRow == 0)	
	{
		AfxMessageBox("모델 정보를 찾을 수 없습니다.", MB_OK);
		return FALSE;
	}

	//현재 Loading된 공정을 공정 Grid에서 찾는다.
	for(int i = 0; i<m_wndTestListGrid.GetRowCount(); i++)
	{
		if(m_lLoadedTestID == atol(m_wndTestListGrid.GetValueRowCol(i+1, COL_TEST_PRIMARY_KEY)))
		{
			nTestRow =  i+1;
			break;
		}
	}
	if(nTestRow == 0)	
	{
		strTemp.Format("모델 [%s]의 공정 정보를 찾을 수 없습니다.", m_pWndCurModelGrid->GetValueRowCol(nModelRow, 1));
		AfxMessageBox(strTemp, MB_OK);
		return FALSE;
	}


	FILE *fp = fopen(FileName, "wt");
	if(fp == NULL){
		AfxMessageBox(m_strDspModelName+"_"+m_strDspTestName+".sch 파일을 생성할 수 없습니다.", MB_OK);
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	EpFileHeader;

	COleDateTime dateTime = COleDateTime::GetCurrentTime();
	sprintf(EpFileHeader.szCreateDateTime, "%s", dateTime.Format());	//기록 시간
	EpFileHeader.nFileID = SCHEDULE_FILE_ID;				//파일 ID
	EpFileHeader.nFileVersion = SCHEDULE_FILE_VER;			//파일 Version
	sprintf(EpFileHeader.szDescrition, "ADPOWER power supply scheule file.");	//파일 서명 
	fwrite(&EpFileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);				//기록 실시 
	
	//모델 정보 기록 
	SCH_TEST_INFORMATION modelData;
	modelData.lID = atol(m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_PRIMARY_KEY));
	sprintf(modelData.szName, "%s", m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_NAME));
	sprintf(modelData.szDescription, "%s", m_pWndCurModelGrid->GetValueRowCol(nModelRow, COL_MODEL_DESCRIPTION));
	//modelData.szCreator
	//modelData,.szModifiedTime
	fwrite(&modelData, sizeof(SCH_TEST_INFORMATION), 1, fp);				//기록 실시 

	//공정 정보 기록 
	SCH_TEST_INFORMATION testData;
	testData.lID = atol(m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_PROC_TYPE));		//공정 종류 
	sprintf(testData.szName, "%s", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_NAME));
	sprintf(testData.szDescription, "%s", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_DESCRIPTION));
	sprintf(testData.szCreator, "%s",  m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_CREATOR));
	sprintf(testData.szModifiedTime, "%s", m_wndTestListGrid.GetValueRowCol(nTestRow, COL_TEST_EDIT_TIME));
	fwrite(&testData, sizeof(SCH_TEST_INFORMATION), 1, fp);				//기록 실시 

	//cell check 조건 기록 
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);
	FILE_CELL_CHECK_PARAM cellCheck;
	if(ConvertCellCheckFormat(pParam, cellCheck) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}
	fwrite(&cellCheck, sizeof(cellCheck), 1, fp);				//기록 실시 
	
	//step 정보 기록 
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();	//Get Document point
	int nTotStepNum = pDoc->GetTotalStepNum();					//Total Step Number
	STEP *pStep;
	FILE_STEP_PARAM fileStep;
	for(int step = 0; step<nTotStepNum; step++)
	{
		pStep = (STEP *)pDoc->GetStepData(step+1);
		if(!pStep)
		{
			fclose(fp);
			return FALSE;
		}
		ConvertStepFormat(pStep, fileStep);
		fwrite(&fileStep, sizeof(fileStep), 1, fp);				//기록 실시 
	}	
	
	fclose(fp);
	return TRUE;
	*/
}

void CCTSEditorProView::OnFileSave() 
{
	WriteFile();
}

BOOL CCTSEditorProView::ReadFile(int nType)
{
	CString FileName, strTemp;

	if((!m_bStepDataSaved || GetDocument()->IsEdited() )&& GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) != FALSE)
	{
		GetDlgItem(IDC_LOADED_TEST)->GetWindowText(strTemp);
		strTemp += Fun_FindMsg("ReadFile_msg1","IDD_CTSEditorPro_CYCL");
		if(IDYES == MessageBox(strTemp, "SAVE", MB_ICONQUESTION|MB_YESNO))
		{
			if(!StepSave())	return FALSE;
		}
	}
	

	//File Open Dialog
	CFileDialog pDlg(TRUE, "", "", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "sch file(*.sch)|*.sch|All Files (*.*)|*.*|");

	if(IDOK == pDlg.DoModal())
	{
		FileName = pDlg.GetPathName();
	}
	if(FileName.IsEmpty())
	{
		return FALSE;
	}
	
	CScheduleData schData;
	//if(schData.SetSchedule(FileName) == FALSE)
	if(schData.Fun_SetScheduleFromSch(FileName) == FALSE)	//ksj 20180528 : sch 파일에 저장된 패턴파일 복원 (울산 SDI 고속형 Cycler 발췌)
	{
		AfxMessageBox(FileName + Fun_FindMsg("ReadFile_msg2","IDD_CTSEditorPro_CYCL"), MB_OK);
		return FALSE;
	}

	/*
	//File Open
	FILE *fp = fopen(FileName, "rt");
	if(fp == NULL){
		AfxMessageBox(FileName + " 파일을 읽을 수 없습니다.", MB_OK);
		return FALSE;
	}

	//파일 Header 기록 
	PS_FILE_ID_HEADER	EpFileHeader;
	if(fread(&EpFileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)				//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	//Header 정보 검사
	if(EpFileHeader.nFileID != SCHEDULE_FILE_ID)
	{
		AfxMessageBox("시험 조건 파일이 아닙니다.", MB_OK);
		fclose(fp);
		return FALSE;
	}

	//모델 정보 기록 
	SCH_TEST_INFORMATION modelData;
	if(fread(&modelData, sizeof(modelData), 1, fp) < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	*/
	//새로운 모델 이름 지정 
	
	/*	
	CModelNameDlg	dlg(TRUE, this);
	dlg.SetName(schData.GetModelName(), schData.GetModelDescript());
	if(dlg.DoModal() != IDOK)	
	{
		//저장 취소 
		return FALSE;
	}
	*/

	CTestNameDlg	dlg1(m_lDisplayModelID, TRUE, this);
	dlg1.m_strName = schData.GetScheduleName();
	dlg1.m_strDecription = schData.GetScheduleDescript();
	dlg1.m_strCreator = GetDocument()->m_LoginData.szLoginID;//schData.GetScheduleCreator();
	dlg1.m_createdDate = COleDateTime::GetCurrentTime();
	dlg1.m_lTestTypeID = schData.GetScheduleType();
	
	if(dlg1.DoModal() != IDOK)	
	{
		//저장 취소 
		return FALSE;
	}

	
	//이전 모델명에 같은 이름이 있을 경우 이후 공정으로 추가 여부 확인
	//같은 모델명이 없으면 새로운 모델로 추가 한다.
	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	ROWCOL modelRow = 0;
	for(int i =0; i<nRow; i++)
	{
//		if( m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME) == dlg.GetName())			//모델명 비교 
//			&& m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_DESCRIPTION) == dlg.GetDescript())  //설명 비교 
		if( atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY)) == dlg1.m_lModelID)			//모델명 비교 
		{
/*			strTemp.Format("[%s]는 등록 되어 있습니다. [%s]에 추가 공정으로 등록 하시겠습니까?", dlg.GetName(), dlg.GetName());
 			int nRtn = MessageBox(strTemp, "중복 모델", MB_YESNOCANCEL|MB_ICONQUESTION);
			//같은 이름으로 하위 목록으로 등록 
			if(IDYES == nRtn)
			{
				modelRow = i+1;
				break;		
			}
			else if(IDCANCEL == nRtn)
			{
				return FALSE;
			}
*/
			modelRow = i+1;
			break;

		}
	}

/*	if(modelRow == 0)	//같은 모델명 발견되지 않았거나 새로운 모델로 등록하길 원할 경우  
	{
		nRow++;
		//  Model Grid에 새로 추가 하고 ModelSave 실행 
		m_pWndCurModelGrid->InsertRows(nRow, 1);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_NEW);
		m_pWndCurModelGrid->SetCurrentCell(nRow, COL_MODEL_NAME);
		//m_pWndCurModelGrid->GetValueRowCol(nRow, 0));
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), dlg.GetName());
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), dlg.GetDescript());
		
		//DataBase에 모델 추가 
		OnModelSave();
	}
	else	//같은 모델명이 발견되어 추가 공정으로 원할 경우 
	{
		nRow = modelRow;
	}
*/
	nRow = modelRow;

	//추가 모델 정보로 Display 갱신 
	strTemp = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_PRIMARY_KEY);	//Battery Model ID
	UpdateDspModel(m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_NAME), atoi((LPCSTR)(LPCTSTR)strTemp));
	m_pWndCurModelGrid->SetCurrentCell(nRow, COL_MODEL_NAME);
	
	RequeryTestList(m_lDisplayModelID);
		
//	strTemp.Format("%s", m_strDspModelName);
//	m_TestNameLabel.SetText(strTemp);
	
/*
	//공정 정보 기록 
	SCH_TEST_INFORMATION testData;
	if(fread(&testData, sizeof(SCH_TEST_INFORMATION), 1, fp)  < 1)			//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
*/

	//////////////////////////////////////////////////////////////////////////
	//Test 등록 시작 	
	//Grid에 추가 한다.
	nRow = m_wndTestListGrid.GetRowCount()+1;
	m_wndTestListGrid.InsertRows(nRow, 1);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), GetDocument()->m_LoginData.szUserName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_TIME), dlg1.m_createdDate.Format());//yulee 20190708
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_NEW);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (long)GetTestIndexFromType(dlg1.m_lTestTypeID));
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), dlg1.m_strName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), dlg1.m_strDecription);
	
	//DataBase에 기록 =
	OnTestSave();
	
	//Display 갱신 한다.
	strTemp = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY);	//Test ID
	UpdateDspTest(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_NAME), atoi((LPCSTR)(LPCTSTR)strTemp));

	//////////////////////////////////////////////////////////////////////////
	//Step 등록 시작 
	m_lLoadedBatteryModelID = m_lDisplayModelID;
	m_lLoadedTestID = m_lDisplayTestID;
	strTemp.Format(" [%s] -> [%s]", m_strDspModelName, m_strDspTestName);
	m_LoadedTest.SetText(strTemp);

	//cell check 조건 기록 
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&GetDocument()->m_sPreTestParam);
	
/*	FILE_CELL_CHECK_PARAM cellCheck;
	if(fread(&cellCheck, sizeof(cellCheck), 1, fp)  < 1)					//기록 실시 
	{
		fclose(fp);
		return FALSE;
	}
	ConvertCellCheckData(cellCheck, pParam);
*/	
	CELL_CHECK_DATA * pCellCheck = schData.GetCellCheckParam();
// 	pParam->fMaxVoltage = pCellCheck->fMaxVoltage; 
// 	pParam->fMinVoltage = pCellCheck->fMinVoltage; 
// 	pParam->fMaxCurrent = pCellCheck->fMaxCurrent; 
// 	pParam->fOCVLimitVal = pCellCheck->fVrefVal;			//Vref
// 	pParam->fTrickleCurrent = pCellCheck->fIrefVal; 		//Iref
// 	pParam->fDeltaVoltage = pCellCheck->fDeltaVoltage; 
// 	pParam->lTrickleTime = pCellCheck->lTrickleTime;
// 	pParam->nMaxFaultNo = pCellCheck->nMaxFaultNo; 
// 	pParam->bPreTest = pCellCheck->bPreTest;
	pParam->fMaxV = pCellCheck->fMaxV; 
	pParam->fMinV = pCellCheck->fMinV; 
	pParam->fMaxI = pCellCheck->fMaxI; 
	pParam->fCellDataV = pCellCheck->fCellDeltaV; 
	pParam->fMaxT = pCellCheck->fMaxT; 
	pParam->fMinT = pCellCheck->fMinT; 
	pParam->fMaxC = pCellCheck->fMaxC; 
	pParam->lMaxW = pCellCheck->lMaxW; 
	pParam->lMaxWh = pCellCheck->lMaxWh; 
	
	pParam->fStdMaxV = pCellCheck->fSTDCellDeltaMaxV; //lyj 20200408 안전조건 스케쥴불러오기 버그수정
	pParam->fStdMinV = pCellCheck->fSTDCellDeltaMinV; //lyj 20200408 안전조건 스케쥴불러오기 버그수정
	pParam->fDataMaxV = pCellCheck->fCellDeltaMaxV; //lyj 20200408 안전조건 스케쥴불러오기 버그수정
	pParam->fDataMinV = pCellCheck->fCellDeltaMinV; //lyj 20200408 안전조건 스케쥴불러오기 버그수정

	//화면에 표시 
	DisplayPreTestParam();
		
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();	//Get Document point
	pDoc->ReMoveStepArray();

	//step 정보 기록 
	STEP *pStep;
	CStep *pSchStep;

	for(int step =0; step<schData.GetStepSize(); step++)
	{
//		if(fread(&stepData, sizeof(FILE_STEP_PARAM), 1, fp) < 1)
//			break;				//기록 실시 

		pSchStep = schData.GetStepData(step);
		if(pSchStep == NULL)	break;

		pStep = new STEP;
		ASSERT(pStep);
		ZeroMemory(pStep, sizeof(STEP));

		ConvertStepData(pSchStep, pStep);
		pDoc->m_apStep.Add((STEP *)pStep);
	}

	//현재 Step Data를 Grid에 표기
	DisplayStepGrid();


	//현재 Step정보를 DataBase 저장한다.
	//OnStepSave(); //yulee 20180906
	// TODO: Add your control notification handler code here
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return 0;
	}
	
	if (StepSave(nType) == TRUE)
	{
		
	}
	else if(nType)
	{
		GetDlgItem(IDC_STEP_SAVE)->EnableWindow(TRUE);
		m_bStepDataSaved = FALSE;
		return FALSE;
	}
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
	
	//GetDlgItem(IDC_STEP_SAVE)->EnableWindow(FALSE);

	m_bStepDataSaved = TRUE;

	//fclose(fp);
	return TRUE;
}

void CCTSEditorProView::OnFileOpen() 
{
	BOOL bReadFile;
	bReadFile = TRUE;
	//m_bFlagConfirmOK = FALSE;
	ReadFile(bReadFile);
}

BOOL CCTSEditorProView::ConvertStepFormat(STEP *pStep, FILE_STEP_PARAM_V100D_LOAD &newStep)
{
	if(pStep == NULL)	return FALSE;
	int i;

	newStep.chStepNo = pStep->chStepNo;
	newStep.chType = pStep->chType;
	newStep.chMode = pStep->chMode;
	newStep.fVref_Charge = pStep->fVref_Charge;
	newStep.fVref_DisCharge = pStep->fVref_DisCharge;
	newStep.fIref = pStep->fIref;
	newStep.fPref = pStep->fPref;
	newStep.fRref = pStep->fRref;
	newStep.fTref = pStep->fTref;
	newStep.fHref = pStep->fHref;
	newStep.fTrate = pStep->fTrate;
	newStep.fHrate = pStep->fHrate;
	newStep.lEndTimeDay = pStep->ulEndTimeDay;		//ljb 20131212 add
	newStep.fEndTime = (pStep->ulEndTime)/100.0f;
	newStep.fEndV = pStep->fEndV_H;
	newStep.fEndV_L = pStep->fEndV_L	;
	newStep.fEndI = pStep->fEndI;
	newStep.fEndC = pStep->fEndC;
	newStep.fEndDV = pStep->fEndDV;						//Step Charge/Discharge Flag
	newStep.fEndDI = pStep->fEndDI;						//Step 충방전시 2번째 I ref 
	newStep.fVLimitHigh = pStep->fVLimitHigh;
	newStep.fVLimitLow = pStep->fVLimitLow;
	newStep.fILimitHigh = pStep->fILimitHigh;
	newStep.fILimitLow = pStep->fILimitLow;
	newStep.fCLimitHigh = pStep->fCLimitHigh;
	newStep.fCLimitLow = pStep->fCLimitLow;
	newStep.fCapacitanceHigh	= pStep->fCapacitanceHigh;
	newStep.fCapacitanceLow		= pStep->fCapacitanceLow;
	newStep.fImpLimitHigh = pStep->fImpLimitHigh;
	newStep.fImpLimitLow = pStep->fImpLimitLow;
	newStep.fHighLimitTemp = pStep->fTempHigh;
	newStep.fLowLimitTemp = pStep->fTempLow;
	newStep.fDeltaTime = (pStep->lDeltaTime)/100.0f;
	newStep.fDeltaTime1 = (pStep->lDeltaTime1)/100.0f;			//Step 충방전시 2번째 End Cap으로 사용 
	newStep.fDeltaV = pStep->fDeltaV;					//Step 충방전시 2번째 End V
	newStep.fDeltaI = pStep->fDeltaI;					//Step 충방전시 2번째 End I
	newStep.bGrade = pStep->bGrade;

	newStep.lCVTimeDay = pStep->ulCVTimeDay;		//ljb 20131212 add
	newStep.fCVTime = (pStep->ulCVTime)/100.0f;		//ljb 20131212 add

	memcpy(&newStep.sGrading_Val, &pStep->sGrading_Val, sizeof(newStep.sGrading_Val));

	for(int i = 0; i<SCH_MAX_COMP_POINT && i<3; i++)
	{
		newStep.fCompVLow[i] =  pStep->fCompVLow[i];
		newStep.fCompVHigh[i] =  pStep->fCompVHigh[i];
		newStep.fCompTimeV[i] = (pStep->ulCompTimeV[i])/100.0f;
		newStep.fCompILow[i] = pStep->fCompILow[i];
		newStep.fCompIHigh[i] = pStep->fCompIHigh[i];
		newStep.fCompTimeI[i] = (pStep->ulCompTimeI[i])/100.0f;
	}

	newStep.fIEndHigh = pStep->fIEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	newStep.fIEndLow = pStep->fIEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	newStep.fVEndHigh = pStep->fVEndHigh;				//충전 CC/CV에서 종료후 전류 크기 비교
	newStep.fVEndLow = pStep->fVEndLow;				//충전 CC/CV에서 종료후 전류 크기 비교
	newStep.nProcType = pStep->nProcType;

	newStep.fReportV = pStep->fReportV;
	newStep.fReportI  = pStep->fReportI;
	newStep.fReportTime  = (pStep->ulReportTime)/100.0f;
	newStep.fReportTemp = pStep->fReportTemp;
	
	newStep.fCapaVoltage1 = pStep->fCapaVoltage1;		//EDLC 용량 검사시 전압 Point1
	newStep.fCapaVoltage2 = pStep->fCapaVoltage2;		//EDLC 용량 검사시 전압 Point2
	newStep.fDCRStartTime = (pStep->fDCRStartTime)/100.0f;		//EDLC DCR 검사시 Start Time
	newStep.fDCREndTime = (pStep->fDCREndTime)/100.0f;			//EDLC DCR 검사시 End Time
	newStep.fLCStartTime = (pStep->fLCStartTime)/100.0f;			//EDLC LC 측정 시작 시간
	newStep.fLCEndTime = (pStep->fLCEndTime)/100.0f;				//EDLC LC 측정 종료 시간
	newStep.lRange = pStep->lRange;						//Range 선택
	newStep.nLoopInfoGotoStep = pStep->nLoopInfoGotoStep;
	newStep.nLoopInfoCycle = pStep->nLoopInfoCycle;

	//ljb v1009  Loop in Loop//////////////////////////////////////////////////////////////////////////
	newStep.nMultiLoopGroupID = pStep->nMultiLoopGroupID;
	newStep.nMultiLoopInfoCycle = pStep->nMultiLoopInfoCycle;
	newStep.nMultiLoopInfoGotoStep = pStep->nMultiLoopInfoGotoStep;

	newStep.nAccLoopGroupID = pStep->nAccLoopGroupID;
	newStep.nAccLoopInfoCycle = pStep->nAccLoopInfoCycle;
	newStep.nAccLoopInfoGotoStep = pStep->nAccLoopInfoGotoStep;

	//ljb 20101230
	for( int i=0; i < MAX_STEP_CAN_AUX_COMPARE_SIZE; i++)
	{
		newStep.iBranchCanDivision[i] = pStep->ican_function_division[i];
		newStep.fBranchCanValue[i] = pStep->fcan_Value[i];
		newStep.cBranchCanDataType[i] = pStep->cCan_data_type[i];
		newStep.cBranchCanCompareType[i] = pStep->cCan_compare_type[i];
		newStep.wBranchCanStep[i] = pStep->ican_branch[i];
		
		newStep.iBranchAuxDivision[i] = pStep->iaux_function_division[i];
		newStep.fBranchAuxValue[i] = pStep->faux_Value[i];
		newStep.fBranchAuxValue[i] = pStep->iaux_conti_time[i];
		newStep.cBranchAuxDataType[i] = pStep->cAux_data_type[i];
		newStep.cBranchAuxCompareType[i] = pStep->cAux_compare_type[i];
		newStep.wBranchAuxStep[i] = pStep->iaux_branch[i];
	}
	
	newStep.nValueLoaderItem = pStep->nValueLoaderItem;	//0: 사용안함 , 1: ON,	 2: OFF
	newStep.nValueLoaderMode = pStep->nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
	newStep.fValueLoaderSet  = pStep->fValueLoaderSet;		//설정값 W,A,V,Ohm

	//cny CellBal
	newStep.nCellBal_CircuitRes     = pStep->nCellBal_CircuitRes;
	newStep.nCellBal_WorkVoltUpper  = pStep->nCellBal_WorkVoltUpper;
	newStep.nCellBal_WorkVolt       = pStep->nCellBal_WorkVolt;
	newStep.nCellBal_StartVolt      = pStep->nCellBal_StartVolt;
	newStep.nCellBal_EndVolt        = pStep->nCellBal_EndVolt;
	newStep.nCellBal_ActiveTime = pStep->nCellBal_ActiveTime;
	newStep.nCellBal_AutoStopTime   = pStep->nCellBal_AutoStopTime;
	
	//cny PowerSupply
	newStep.nPwrSupply_Cmd  = pStep->nPwrSupply_Cmd;
	newStep.nPwrSupply_Volt = pStep->nPwrSupply_Volt;
	
	//cny Chiller	
	newStep.nChiller_Cmd          = pStep->nChiller_Cmd;
	newStep.fChiller_RefTemp      = pStep->fChiller_RefTemp;
	newStep.nChiller_RefCmd       = pStep->nChiller_RefCmd;
	newStep.fChiller_RefPump      = pStep->fChiller_RefPump;
	newStep.nChiller_PumpCmd      = pStep->nChiller_PumpCmd;	
	newStep.nChiller_TpData       = pStep->nChiller_TpData;
	newStep.fChiller_ONTemp1      = pStep->fChiller_ONTemp1;//-----
	newStep.nChiller_ONTemp1_Cmd  = pStep->nChiller_ONTemp1_Cmd;	
	newStep.nChiller_ONPump1_Cmd  = pStep->nChiller_ONPump1_Cmd;	
	newStep.fChiller_ONTemp2      = pStep->fChiller_ONTemp2;//-----
	newStep.nChiller_ONTemp2_Cmd  = pStep->nChiller_ONTemp2_Cmd;
	newStep.nChiller_ONPump2_Cmd  = pStep->nChiller_ONPump2_Cmd;
	newStep.fChiller_OFFTemp1     = pStep->fChiller_OFFTemp1;//-----
	newStep.nChiller_OFFTemp1_Cmd = pStep->nChiller_OFFTemp1_Cmd;
	newStep.nChiller_OFFPump1_Cmd = pStep->nChiller_OFFPump1_Cmd;
	newStep.fChiller_OFFTemp2     = pStep->fChiller_OFFTemp2;//-----
	newStep.nChiller_OFFTemp2_Cmd = pStep->nChiller_OFFTemp2_Cmd;
	newStep.nChiller_OFFPump2_Cmd = pStep->nChiller_OFFPump2_Cmd;
	
	return TRUE;
}

/*
BOOL CCTSEditorProView::ConvertCellCheckData(FILE_CELL_CHECK_PARAM &paramData, TEST_PARAM *pParam)
{
	if(pParam == NULL)	return FALSE;

	pParam->fMaxVoltage = paramData.fMaxVoltage; 
	pParam->fMinVoltage = paramData.fMinVoltage; 
	pParam->fMaxCurrent = paramData.fMaxCurrent; 
	pParam->fOCVLimitVal = paramData.fVrefVal;			//Vref
	pParam->fTrickleCurrent = paramData.fIrefVal; 		//Iref
	pParam->fDeltaVoltage = paramData.fDeltaVoltage; 
	pParam->OleTrickleTime.SetTime(paramData.lTrickleTime/3600, paramData.lTrickleTime/60, paramData.lTrickleTime%60);
	pParam->nMaxFaultNo = paramData.nMaxFaultNo; 
	pParam->bPreTest = paramData.bPreTest;

	return TRUE;
}
*/

BOOL CCTSEditorProView::ConvertStepData(CStep *pStepData, STEP *pStep)
{
	if(pStep == NULL || pStepData == NULL)	return FALSE;

	//step header
	pStep->chStepNo = pStepData->m_StepIndex+1; 
	pStep->nProcType = pStepData->m_lProcType;
	pStep->chType = pStepData->m_type; 
	pStep->chMode = pStepData->m_mode; 
	pStep->fVref_Charge = pStepData->m_fVref_Charge;
	pStep->fVref_DisCharge = pStepData->m_fVref_DisCharge;
	pStep->fIref = pStepData->m_fIref; 
	pStep->fPref = pStepData->m_fPref; 
	pStep->fRref = pStepData->m_fRref;
	pStep->fTref = pStepData->m_fTref;
	pStep->fHref = pStepData->m_fHref;
	
	//Range
	pStep->lRange = pStepData->m_lRange; 						//Range 선택

	//Cycle Goto 용

	pStep->nGotoStepEndV_H = pStepData->m_nGotoStepEndV_H;
	pStep->nGotoStepEndV_L = pStepData->m_nGotoStepEndV_L;
	pStep->nGotoStepEndTime = pStepData->m_nGotoStepEndTime;
	pStep->nGotoStepEndCVTime = pStepData->m_nGotoStepEndCVTime;
	pStep->nGotoStepEndC = pStepData->m_nGotoStepEndC;
	pStep->nGotoStepEndWh = pStepData->m_nGotoStepEndWh;
	pStep->nGotoStepEndValue = pStepData->m_nGotoStepEndValue;

	//Value Item Soc
	pStep->nValueRateItem = pStepData->m_bValueItem;
	pStep->nValueStepNo = pStepData->m_bValueStepNo;
	pStep->fValueRate = pStepData->m_fValueRate;


	// DC Loader 연계 조건 추가  [4/22/2015 이재복]
	pStep->nValueLoaderItem = pStepData->nValueLoaderItem;
	pStep->nValueLoaderMode = pStepData->nValueLoaderMode;
	pStep->fValueLoaderSet = pStepData->fValueLoaderSet;

	//Loop in Loop
	pStep->nLoopInfoCycle = pStepData->m_nLoopInfoCycle;
	pStep->nLoopInfoGotoStep = pStepData->m_nLoopInfoGotoStep; 

	pStep->nMultiLoopGroupID	= pStepData->m_nMultiLoopInfoCycle;
	pStep->nMultiLoopGroupID	= pStepData->m_nMultiLoopGroupID;
	pStep->nMultiLoopInfoGotoStep	= pStepData->m_nMultiLoopInfoGotoStep;

	pStep->nAccLoopInfoCycle	= pStepData->m_nAccLoopInfoCycle;
	pStep->nAccLoopGroupID		= pStepData->m_nAccLoopGroupID;
	pStep->nAccLoopInfoGotoStep		= pStepData->m_nAccLoopInfoGotoStep;

	//end Goto
	pStep->nGotoStepEndV_H = pStepData->m_nGotoStepEndV_H;
	pStep->nGotoStepEndV_L = pStepData->m_nGotoStepEndV_L;
	pStep->nGotoStepEndTime = pStepData->m_nGotoStepEndTime;
	pStep->nGotoStepEndCVTime = pStepData->m_nGotoStepEndCVTime;
	pStep->nGotoStepEndC = pStepData->m_nGotoStepEndC;
	pStep->nGotoStepEndWh = pStepData->m_nGotoStepEndWh;
	pStep->nGotoStepEndValue = pStepData->m_nGotoStepEndValue;

	//end 조건
															//m_fStartT 시작 온도
	pStep->ulEndTimeDay = pStepData->m_lEndTimeDay;			//ljb 20131212 add
	pStep->ulEndTime = (pStepData->m_fEndTime)*100.0f;		//종료 온도
	pStep->fEndV_H = pStepData->m_fEndV_H;		//ljb v1009
	pStep->fEndV_L = pStepData->m_fEndV_L;		//ljb v1009
	pStep->fEndI = pStepData->m_fEndI; 
	pStep->fEndC = pStepData->m_fEndC; 
	pStep->fEndDV = pStepData->m_fEndDV; 						//Step Charge/Discharge Flag
	pStep->fEndDI = pStepData->m_fEndDI; 						//Step 충방전시 2번째 I ref 
	pStep->bUseCyclePause  = pStepData->m_bUseCyclePause;			//Cycle 반복 후 Pause
	pStep->bUseLinkStep  = pStepData->m_bUseLinkStep;				//연결 STEP
	pStep->bUseChamberProg  = pStepData->m_bUseChamberProg;			//챔버 Prog 모드 실행 Pattern 99로 설정
	pStep->bStepChamberStop	= pStepData->m_bStepChamberStop;			//yulee 20180828 

	pStep->ulCVTimeDay = pStepData->m_lCVTimeDay;			//ljb 20131212 add
	pStep->ulCVTime = (pStepData->m_fCVTime)*100.0f;		//ljb 20131212 add

	pStep->fEndW = pStepData->m_fEndW; //ksj 20200710 : 종료조건 watt 누락 추가
	pStep->fEndWh = pStepData->m_fEndWh; //ksj 20200710 : 종료조건 watthour 누락 추가

	//안전조건
	pStep->fVLimitHigh = pStepData->m_fHighLimitV; 
	pStep->fVLimitLow = pStepData->m_fLowLimitV; 
	pStep->fILimitHigh = pStepData->m_fHighLimitI;
	pStep->fILimitLow = pStepData->m_fLowLimitI; 
	pStep->fCLimitHigh = pStepData->m_fHighLimitC; 
	pStep->fCLimitLow = pStepData->m_fLowLimitC ; 
	pStep->fImpLimitHigh = pStepData->m_fHighLimitImp; 
	pStep->fImpLimitLow = pStepData->m_fLowLimitImp ; 
	pStep->fTempHigh = pStepData->m_fHighLimitTemp; 
	pStep->fTempLow = pStepData->m_fLowLimitTemp ; 
	pStep->fCapacitanceHigh	= pStepData->m_fHighCapacitance;
	pStep->fCapacitanceLow	= pStepData->m_fLowCapacitance;
	
	//Delta
	pStep->lDeltaTime = pStepData->m_fDeltaTimeV*100.0f; 
	pStep->lDeltaTime1 = pStepData->m_fDeltaTimeI*100.0f; 			//Step 충방전시 2번째 End Cap으로 사용 
	pStep->fDeltaV = pStepData->m_fDeltaV; 					//Step 충방전시 2번째 End V
	pStep->fDeltaI = pStepData->m_fDeltaI; 					//Step 충방전시 2번째 End I
	
	//등급조건
	pStep->bGrade = pStepData->m_bGrade; 
	GRADE_STEP gradeStep;
	// 	pStep->sGrading_Val[0].lGradeItem = pStepData->m_Grading.m_lGradingItem;
	// 	pStep->sGrading_Val[0].chTotalGrade = pStepData->m_Grading.GetGradeStepSize();
	pStep->sGrading_Val.chTotalGrade = pStepData->m_Grading.GetGradeStepSize();		//20081208 KHS
	
	int i;
	for(int i = 0; i<pStepData->m_Grading.GetGradeStepSize(); i++)
	{
		gradeStep = pStepData->m_Grading.GetStepData(i);
		pStep->sGrading_Val.lGradeItem[i] = gradeStep.lGradeItem;
		pStep->sGrading_Val.faValue1[i] = gradeStep.fMin;
		pStep->sGrading_Val.faValue2[i] = gradeStep.fMax;
		if(gradeStep.strCode.IsEmpty())
		{
			pStep->sGrading_Val.aszGradeCode[i] = 0;
		}
		else
		{
			pStep->sGrading_Val.aszGradeCode[i] = gradeStep.strCode[0];
		}		
	}

	for(int i = 0; i<SCH_MAX_COMP_POINT && i<MAX_STEP_COMP_SIZE; i++)
	{
		//전압 상승비교

		pStep->fCompVLow[i] = pStepData->m_fCompLowV[i];
		pStep->fCompVHigh[i] =  pStepData->m_fCompHighV[i];
		pStep->ulCompTimeV[i] =  pStepData->m_fCompTimeV[i]*100.0f;

		//전류 상승비교
		pStep->fCompILow[i] =  pStepData->m_fCompLowI[i];
		pStep->fCompIHigh[i] =  pStepData->m_fCompHighI[i];
		pStep->ulCompTimeI[i] =  pStepData->m_fCompTimeI[i]*100.0f;
	}

	//종지값 확인 
	pStep->fIEndHigh = 0.0f;				//충전 CC/CV에서 종료후 전류 크기 비교
	pStep->fIEndLow  = 0.0f;				//충전 CC/CV에서 종료후 전류 크기 비교
	pStep->fVEndHigh = 0.0f;				//방전 CC에서 종료후 전압 크기 비교
	pStep->fVEndLow  = 0.0f;				//방전 CC에서 종료후 전압 크기 비교


	//기록조건 
	pStep->fReportV	 = pStepData->m_fReportV ;
	pStep->fReportI  = pStepData->m_fReportI ;
	pStep->ulReportTime = pStepData->m_fReportTime*100.0f  ;
	pStep->fReportTemp = pStepData->m_fReportTemp;

	//ELDC용
	pStep->fCapaVoltage1 = pStepData->m_fCapaVoltage1; 		//EDLC 용량 검사시 전압 Point1
	pStep->fCapaVoltage2 = pStepData->m_fCapaVoltage2; 		//EDLC 용량 검사시 전압 Point2
	pStep->fDCRStartTime = pStepData->m_fDCRStartTime*100.0f; 		//EDLC DCR 검사시 Start Time
	pStep->fDCREndTime = pStepData->m_fDCREndTime*100.0f; 			//EDLC DCR 검사시 End Time
	pStep->fLCStartTime = pStepData->m_fLCStartTime*100.0f; 			//EDLC LC 측정 시작 시간
	pStep->fLCEndTime = pStepData->m_fLCEndTime*100.0f;				//EDLC LC 측정 종료 시간

	for( int i=0; i < MAX_STEP_CAN_AUX_COMPARE_SIZE; i++)
	{
		pStep->ican_function_division[i] = pStepData->m_ican_function_division[i];
		pStep->fcan_Value[i] = pStepData->m_fcan_Value[i];
		pStep->cCan_data_type[i] = pStepData->m_ican_data_type[i];
		pStep->cCan_compare_type[i] = pStepData->m_ican_compare_type[i];
		pStep->ican_branch[i] = pStepData->m_ican_branch[i];
		
		pStep->iaux_function_division[i] = pStepData->m_iaux_function_division[i];
		pStep->faux_Value[i] = pStepData->m_faux_Value[i];
		pStep->cAux_data_type[i] = pStepData->m_iaux_data_type[i];
		pStep->cAux_compare_type[i] = pStepData->m_iaux_compare_type[i];
		pStep->iaux_branch[i] = pStepData->m_iaux_branch[i];
		pStep->iaux_conti_time[i] = pStepData->m_iaux_conti_time[i];
	}	
	//ljb 20170517 add 종료 옵션
	pStep->lValueLimitHigh	 = pStepData->m_fValueMax ;
	pStep->lValueLimitLow  = pStepData->m_fValueMin ;

	pStep->nNoCheckMode	 = pStepData->m_nNoCheckMode ;
	pStep->nCanTxOffMode  = pStepData->m_nCanTxOffMode ;
	pStep->nCanCheckMode = pStepData->m_nCanCheckMode;
	
	pStep->nWaitTimeInit	 = pStepData->m_nWaitTimeInit ;
	pStep->nWaitTimeDay  = pStepData->m_nWaitTimeDay ;
	pStep->nWaitTimeHour = pStepData->m_nWaitTimeHour;
	pStep->nWaitTimeMin = pStepData->m_nWaitTimeMin;
	pStep->nWaitTimeSec = pStepData->m_nWaitTimeSec;

	strcpy(pStep->szSimulFile, pStepData->m_strPatternFileName.Left(256)); //ksj 20180528

	//cny CellBal
	pStep->nCellBal_CircuitRes     = pStepData->m_nCellBal_CircuitRes;
	pStep->nCellBal_WorkVoltUpper  = pStepData->m_nCellBal_WorkVoltUpper;
	pStep->nCellBal_WorkVolt       = pStepData->m_nCellBal_WorkVolt;
	pStep->nCellBal_StartVolt      = pStepData->m_nCellBal_StartVolt;
	pStep->nCellBal_EndVolt        = pStepData->m_nCellBal_EndVolt;
	pStep->nCellBal_ActiveTime	   = pStepData->m_nCellBal_ActiveTime;
	pStep->nCellBal_AutoStopTime   = pStepData->m_nCellBal_AutoStopTime;
	
	//cny PowerSupply
	pStep->nPwrSupply_Cmd  = pStepData->m_nPwrSupply_Cmd;
	pStep->nPwrSupply_Volt = pStepData->m_nPwrSupply_Volt;
	
	//cny Chiller
	pStep->nChiller_Cmd          = pStepData->m_nChiller_Cmd;
	pStep->fChiller_RefTemp      = pStepData->m_fChiller_RefTemp;
	pStep->nChiller_RefCmd       = pStepData->m_nChiller_RefCmd;
	pStep->fChiller_RefPump      = pStepData->m_fChiller_RefPump;
	pStep->nChiller_PumpCmd      = pStepData->m_nChiller_PumpCmd;	
	pStep->nChiller_TpData       = pStepData->m_nChiller_TpData;
	pStep->fChiller_ONTemp1      = pStepData->m_fChiller_ONTemp1;//----
	pStep->nChiller_ONTemp1_Cmd  = pStepData->m_nChiller_ONTemp1_Cmd;	
	pStep->nChiller_ONPump1_Cmd  = pStepData->m_nChiller_ONPump1_Cmd;	
	pStep->fChiller_ONTemp2      = pStepData->m_fChiller_ONTemp2;//----
	pStep->nChiller_ONTemp2_Cmd  = pStepData->m_nChiller_ONTemp2_Cmd;
	pStep->nChiller_ONPump2_Cmd  = pStepData->m_nChiller_ONPump2_Cmd;
	pStep->fChiller_OFFTemp1     = pStepData->m_fChiller_OFFTemp1;//----
	pStep->nChiller_OFFTemp1_Cmd = pStepData->m_nChiller_OFFTemp1_Cmd;
	pStep->nChiller_OFFPump1_Cmd = pStepData->m_nChiller_OFFPump1_Cmd;
	pStep->fChiller_OFFTemp2     = pStepData->m_fChiller_OFFTemp2;//----
	pStep->nChiller_OFFTemp2_Cmd = pStepData->m_nChiller_OFFTemp2_Cmd;
	pStep->nChiller_OFFPump2_Cmd = pStepData->m_nChiller_OFFPump2_Cmd;

	pStep->fCellDeltaVStep		= pStepData->m_fCellDeltaVStep; //yulee 20190531_6
	pStep->fStepDeltaAuxTemp		= pStepData->m_fStepDeltaAuxTemp;
	pStep->fStepDeltaAuxTh			= pStepData->m_fStepDeltaAuxTH;
	pStep->fStepDeltaAuxT			= pStepData->m_fStepDeltaAuxT;
	pStep->fStepDeltaAuxVTime		= pStepData->m_fStepDeltaAuxVTime;
	pStep->fStepAuxV				= pStepData->m_fStepAuxV;

	pStep->bStepDeltaVentAuxV		= pStepData->m_bStepDeltaVentAuxV;
	pStep->bStepDeltaVentAuxTemp	= pStepData->m_bStepDeltaVentAuxTemp;
	pStep->bStepDeltaVentAuxTh		= pStepData->m_bStepDeltaVentAuxTh;
	pStep->bStepDeltaVentAuxT		= pStepData->m_bStepDeltaVentAuxT;
	pStep->bStepVentAuxV			= pStepData->m_bStepVentAuxV;

	return TRUE;
}

void CCTSEditorProView::OnUpdateExcelSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_lLoadedBatteryModelID);
	
}

void CCTSEditorProView::OnUpdateJasonSchSave(CCmdUI* pCmdUI)  //yulee 20190104 Jason format sch
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_lLoadedBatteryModelID);
	
}

void CCTSEditorProView::InitParamTab()
{
	CString strTemp;
	m_pTabImageList = new CImageList;

	if(!m_pTabImageList->Create(IDB_TAB_BITMAP, 18, 3, RGB(255,255,255)))
	{
		delete m_pTabImageList;
		m_pTabImageList = NULL;
	}

	m_ctrlParamTab.SetImageList(m_pTabImageList);
	m_ctrlParamTab.ModifyStyle(TCS_BOTTOM|TCS_MULTILINE|TCS_VERTICAL|TCS_BUTTONS, TCS_OWNERDRAWFIXED|TCS_FIXEDWIDTH);
	
	TC_ITEM item;
	item.mask = TCIF_TEXT|TCIF_IMAGE;
	item.iImage = 0;
	strTemp = Fun_FindMsg("InitParamTab_msg1","IDD_CTSEditorPro_CYCL");
	item.pszText =  strTemp.GetBuffer(0);	
	m_ctrlParamTab.InsertItem( TAB_SAFT_VAL,&item);

	item.iImage = 2;
	strTemp = Fun_FindMsg("InitParamTab_msg2","IDD_CTSEditorPro_CYCL");
	item.pszText = strTemp.GetBuffer(0);
	strTemp.ReleaseBuffer();

	m_ctrlParamTab.InsertItem( TAB_RECORD_VAL, &item);
	m_ctrlParamTab.SetColor(RGB(0,0,0), RGB(255,255,255), RGB(240,240,240), RGB(240,240,240));	// 글자색, 바깥쪽 라인, 내부 색깔, 이미지쪽 라인
}


void CCTSEditorProView::OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nTabNum = m_ctrlParamTab.GetCurSel();

	TRACE("Tab %d Selected\n", nTabNum);
	GetDlgItem(IDC_HIGH_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_LOW_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_V_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_VTG_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CELL_DELTA_V_STEP)->ShowWindow(SW_HIDE);//yulee 20190531_3
	GetDlgItem(IDC_CELL_DELTA_V_STATIC)->ShowWindow(SW_HIDE);//yulee 20190625
	GetDlgItem(IDC_VTG_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_I_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CRT_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CRT_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_IMP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_LOW)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_TEMP_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DELTA_V)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DELTA_I)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_HIDE);	
	GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BAR_STATIC4)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_CAPACITANCE_HIGH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CAPACITANCE_LOW1)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_GRADE_CHECK_CAPACITANCE)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_STEP_AUXT_DELTA)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXTH_DELTA)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXV_TIME)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXV_DELTA)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_STEP_AUXT_DELTA_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXTH_DELTA_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXV_TIME_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STEP_AUXV_DELTA_STATIC)->ShowWindow(SW_HIDE);

	ShowExtOptionCtrl(SW_HIDE);

	//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
	UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825

	m_nCurTabIndex = nTabNum;
//	m_wndChamberGrid.ShowWindow(SW_HIDE);
	switch(nTabNum)
	{

	case TAB_SAFT_VAL:		//안전 조건 

		
		if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
		{
			GetDlgItem(IDC_STEP_AUXT_DELTA)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STEP_AUXTH_DELTA)->ShowWindow(SW_SHOW);
			//GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STEP_AUXV_TIME)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STEP_AUXV_DELTA)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_SHOW);
			//GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->ShowWindow(SW_SHOW);

			GetDlgItem(IDC_STEP_AUXT_DELTA_STATIC)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STEP_AUXTH_DELTA_STATIC)->ShowWindow(SW_SHOW);
			//GetDlgItem(IDC_STEP_AUXT_AUXTH_DELTA_STATIC)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STEP_AUXV_TIME_STATIC)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STEP_AUXV_DELTA_STATIC)->ShowWindow(SW_SHOW);

			int iUseVent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "No Use Vent", 0);
			int bUseVentSafety = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Use Vent Safety", 1); //ksj 20200226 : No Use Vent 항목 있는지 모르고 신규 추가했었음. 이쪽에도 조건 추가
			//if (iUseVent)
			if (iUseVent || !bUseVentSafety) //ksj 20200226		
			{
				GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_HIDE);
				GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_HIDE);
			}


		}

		//Tab 0
		GetDlgItem(IDC_HIGH_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_LOW_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_V_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_VTG_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CELL_DELTA_V_STEP)->ShowWindow(SW_SHOW);//yulee 20190531_3
		GetDlgItem(IDC_CELL_DELTA_V_STATIC)->ShowWindow(SW_SHOW);//yulee 20190625
		GetDlgItem(IDC_VTG_LOW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_I_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CRT_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CRT_LOW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_LOW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_IMP_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_IMP_HIGH)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_IMP_LOW)->ShowWindow(SW_SHOW);

		


#ifdef _FORMATION_
		GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_SHOW);	
#endif

#ifdef _EDLC_CELL_
//		GetDlgItem(IDC_BAR_STATIC4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BAR_STATIC4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_SHOW);	
		GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_SHOW);
#endif
		if(m_bExtOption)
		{
			ShowExtOptionCtrl(SW_SHOW);
		}

		//Tab 1
		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_HIDE);

		//Tab 2
		GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_HIDE);

		break;
	case TAB_GRADE_VAL:		//Grading조건
		
		//Tab 1
		m_wndGradeGrid.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_SHOW);

//		GetDlgItem(IDC_GRADE_CHECK_CAPACITANCE)->ShowWindow(SW_SHOW);
		
		//Tab 2
		GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_HIDE);
		
		break;
	case TAB_CHAMBER_VAL:		//Chamber 조건
//		m_wndChamberGrid.ShowWindow(SW_SHOW);
		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_HIDE);

		//Tab 2
		GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_HIDE);
		break;
	case TAB_EDLC_CELL_VAL:		//EDLC 조건
		//m_wndChamberGrid.ShowWindow(SW_HIDE);
		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_HIDE);
		
		//Tab 2
		GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_HIDE);
		
		GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_SHOW);	
		GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_SHOW);	
// 		GetDlgItem(IDC_EDLC_DCR_STIME_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_DCR_ETIME_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_LC_STIME_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_LC_ETIME_STATIC)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_DCR_START_TIME)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_DCR_END_TIME)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_LC_START_TIME)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_LC_END_TIME)->ShowWindow(SW_SHOW);
// 		
// 		GetDlgItem(IDC_EDLC_POINT_STATIC_1)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_POINT_STATIC_2)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_POINT_STATIC_3)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_POINT_STATIC_4)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_TIME_MILI_1)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_TIME_MILI_2)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_TIME_MILI_3)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_TIME_MILI_4)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_MSEC_SPIN_1)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_MSEC_SPIN_2)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_MSEC_SPIN_3)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_MSEC_SPIN_4)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_UNIT_TIME_STATIC_1)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_UNIT_TIME_STATIC_2)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_UNIT_TIME_STATIC_3)->ShowWindow(SW_SHOW);
// 		GetDlgItem(IDC_EDLC_UNIT_TIME_STATIC_4)->ShowWindow(SW_SHOW);
		break;
		
	default:	//TAB_RECORD_VAL	//Reporting 조건

		//Tab 1
		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_GRADE_CHECK)->ShowWindow(SW_HIDE);

		//Tab 2
		GetDlgItem(IDC_RPT_TIME_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_SHOW);

#ifndef _FORMATION_		//Formation은 delta time만 적용가능 (Delta V, I등을 적용하기 위해서는 Download program을 수정해야함 )
		if(m_bUseChamberTemp)
		{
			GetDlgItem(IDC_RPT_TEMP_STATIC)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_REPORT_TEMPERATURE)->ShowWindow(SW_SHOW);
		}
		GetDlgItem(IDC_RPT_I_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RPT_V_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_REPORT_VOLTAGE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_REPORT_CURRENT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RPT_TUNIT_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_OR_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_WARRING_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RPT_MSEC_SPIN)->ShowWindow(SW_SHOW);
#endif
	}	

	STEP *pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
	if(pStep)	SetApplyAllBtn(pStep->chType);

	if(!UpdateStepGrid(m_nDisplayStep))				//Update Current Display Step Data
	{
		//AfxMessageBox("Step Data Update Fail...");
	}





/*
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	// Take the default processing unless we set this to something else below.
	*pResult = CDRF_DODEFAULT;
	CNewTabCtrl *bItem = (CNewTabCtrl *) pLVCD->nmcd.lItemlParam ;
	switch (pLVCD->nmcd.dwDrawStage) {
	case CDDS_ITEM:
	case CDDS_POSTPAINT:
	case CDDS_PREERASE:
	case CDDS_POSTERASE:
	case CDDS_ITEMPOSTPAINT :
	case CDDS_ITEMPREERASE :
	case CDDS_ITEMPOSTERASE :
		break ;

	case CDDS_PREPAINT :
		*pResult = CDRF_NEWFONT  ;
		break ;
	case CDDS_ITEMPREPAINT :
		if ( bItem ) {
				//pLVCD->clrText   = bItem->m_color ;

		}
		break ;
	}
	pLVCD->clrTextBk = RGB(0,0,0);
	pLVCD->clrText   = RGB(0,0,0);

	*pResult = 0;*/
}

void CCTSEditorProView::SetCompCtrlEnable(BOOL bEnable, BOOL bAll)
{

	if(bAll)
	{
	GetDlgItem(IDC_COMP_V1)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V3)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V4)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V5)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_V6)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_TIME1)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_TIME2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_TIME3)->EnableWindow(bEnable);

	GetDlgItem(IDC_COMP_I1)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I3)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I4)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I5)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I6)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I_TIME1)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I_TIME2)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMP_I_TIME3)->EnableWindow(bEnable);
	}

	else
	{		
		GetDlgItem(IDC_COMP_V1)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V2)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V3)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V4)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V5)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_V6)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_TIME1)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_TIME2)->EnableWindow(bEnable);
		GetDlgItem(IDC_COMP_TIME3)->EnableWindow(bEnable);

		GetDlgItem(IDC_COMP_I1)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I2)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I3)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I4)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I5)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I6)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I_TIME1)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I_TIME2)->EnableWindow(!bEnable);
		GetDlgItem(IDC_COMP_I_TIME3)->EnableWindow(!bEnable);
	}
}


void CCTSEditorProView::OnExtOptionCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nShow = SW_SHOW;
	if(m_bExtOption)
	{
		nShow = SW_SHOW;
	}
	else
	{
		nShow = SW_HIDE;

		//ext 값을 Reset 시킨다.
		m_VtgEndHigh.SetValue(0.0);
		m_VtgEndLow.SetValue(0.0);
		m_CrtEndHigh.SetValue(0.0);
		m_CrtEndLow.SetValue(0.0);

		//
		for(int count = 0; count < SCH_MAX_COMP_POINT; count++)
		{
			m_CompV[count].SetValue(0.0);			
			m_CompV1[count].SetValue(0.0);			
			m_CompI[count].SetValue(0.0);			
			m_CompI1[count].SetValue(0.0);			
			m_CompTimeV[count].SetDateTime((DATE)0);
			m_CompTimeI[count].SetDateTime((DATE)0);
		}

	}
	ShowExtOptionCtrl(nShow);
}

void CCTSEditorProView::ShowExtOptionCtrl(int nShow)
{
	if(nShow == SW_SHOW)
	{
		GetDlgItem(IDC_EXT_OPTION_CHECK)->SetWindowText(Fun_FindMsg("ShowExtOptionCtrl_msg1","IDD_CTSEditorPro_CYCL"));
	}
	else
	{
		GetDlgItem(IDC_EXT_OPTION_CHECK)->SetWindowText(Fun_FindMsg("ShowExtOptionCtrl_msg2","IDD_CTSEditorPro_CYCL"));	
	}
		
	GetDlgItem(IDC_COMP1_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP2_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP3_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP1_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP2_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP3_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V1)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V3)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V4)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V5)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_V6)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I1)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I3)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I4)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I5)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I6)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_TIME1)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_TIME2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_TIME3)->ShowWindow(nShow);
	GetDlgItem(IDC_BAR_STATIC2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I_TIME1)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I_TIME2)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_I_TIME3)->ShowWindow(nShow);
	
//	GetDlgItem(IDC_BAR_STATIC1)->ShowWindow(nShow);
//	GetDlgItem(IDC_END_VOLTAGE_LOW)->ShowWindow(nShow);
//	GetDlgItem(IDC_END_VOLTAGE_HIGH)->ShowWindow(nShow);
//	GetDlgItem(IDC_END_CURRENT_LOW)->ShowWindow(nShow);
//	GetDlgItem(IDC_END_CURRENT_HIGH)->ShowWindow(nShow);	
//	GetDlgItem(IDC_I_END_STATIC)->ShowWindow(nShow);
//	GetDlgItem(IDC_V_END_STATIC)->ShowWindow(nShow);
//	GetDlgItem(IDC_CV_END_STATIC)->ShowWindow(nShow);
//	GetDlgItem(IDC_CC_END_STATIC)->ShowWindow(nShow);	
//	GetDlgItem(IDC_INTER_STATIC1)->ShowWindow(nShow);	
//	GetDlgItem(IDC_INTER_STATIC2)->ShowWindow(nShow);

	GetDlgItem(IDC_LOW_VAL_STATIC)->ShowWindow(nShow);	
	GetDlgItem(IDC_HIGH_VAL_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_COMP_TIME_STATIC)->ShowWindow(nShow);
	
	
	GetDlgItem(IDC_INTER_STATIC3)->ShowWindow(nShow);	
	GetDlgItem(IDC_INTER_STATIC4)->ShowWindow(nShow);
	GetDlgItem(IDC_INTER_STATIC5)->ShowWindow(nShow);	
	GetDlgItem(IDC_INTER_STATIC6)->ShowWindow(nShow);	
	GetDlgItem(IDC_INTER_STATIC7)->ShowWindow(nShow);
	GetDlgItem(IDC_INTER_STATIC8)->ShowWindow(nShow);	
	GetDlgItem(IDC_BAR_STATIC3)->ShowWindow(nShow);
}

//EDLC나 Cell용에 따라 모양을 변경한다.
void CCTSEditorProView::SetControlCellEdlc()
{
#ifdef _EDLC_CELL_
		//용량 단위를 F를 바꾼다.
		GetDlgItem(IDC_IMP_STATIC)->SetWindowText("ESR(MOhm)");
		GetDlgItem(IDC_IMP_STATIC)->SetWindowText("ESR(MOhm)");
		
#else
		GetDlgItem(IDC_CAP_VTG_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAP_VTG_LOW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAP_VTG_HIGH)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BAR_STATIC4)->ShowWindow(SW_HIDE);
#endif

	//Time Control들은 Resource 속성에서 숨김을 하여도 숨겨지지 않으므로
	//Time Control들을 숨김
	GetDlgItem(IDC_DELTA_TIME)->EnableWindow(FALSE);
	
//	GetDlgItem(IDC_REPORT_TIME)->EnableWindow(FALSE);
//	GetDlgItem(IDC_REPORT_TIME_MILI)->EnableWindow(FALSE);
//	GetDlgItem(IDC_RPT_MSEC_SPIN)->EnableWindow(FALSE);

#ifndef _FORMATION_
	GetDlgItem(IDC_EXT_OPTION_CHECK)->ShowWindow(SW_HIDE);	
#endif

	GetDlgItem(IDC_DELTA_TIME)->ShowWindow(SW_HIDE);
	
	GetDlgItem(IDC_COMP_TIME1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_TIME2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_TIME3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_I_TIME1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_I_TIME2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_COMP_I_TIME3)->ShowWindow(SW_HIDE);	

	if(m_bUseChamberTemp == FALSE)
	{
		GetDlgItem(IDC_TEMP_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TEMP_LOW)->ShowWindow(SW_HIDE);	
		GetDlgItem(IDC_TEMP_HIGH)->ShowWindow(SW_HIDE);	
	}
//	GetDlgItem(IDC_REPORT_TIME)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_REPORT_TIME_MILI)->ShowWindow(SW_HIDE);
}

//안전값을 표시한다.
void CCTSEditorProView::DisplayStepOption( STEP *pStep)
{
	if(pStep == NULL)
	{
		//
		m_VtgHigh.SetValue(0.0);
		m_VtgLow.SetValue(0.0);
		m_CrtHigh.SetValue(0.0);
		m_CrtLow.SetValue(0.0);
		m_CapHigh.SetValue(0.0);
		m_CapLow.SetValue(0.0);
		m_ImpHigh.SetValue(0.0);
		m_ImpLow.SetValue(0.0);
		m_TempHigh.SetValue(0.0);
		m_TempLow.SetValue(0.0);
		m_CapacitanceHigh.SetValue(0.0);
		m_CapacitanceLow.SetValue(0.0);
		//
		m_DeltaV.SetValue(0.0);
		m_DeltaI.SetValue(0.0);
		m_DeltaTime.SetTime((DATE)0);

		//
		m_VtgEndHigh.SetValue(0.0);
		m_VtgEndLow.SetValue(0.0);
		m_CrtEndHigh.SetValue(0.0);
		m_CrtEndLow.SetValue(0.0);

		//
		m_ReportTemp.SetValue(0.0);
		m_ReportV.SetValue(0.0);
		m_ReportI.SetValue(0.0);
		m_ReportTime.SetTime((DATE)0);
		
		//
		m_CapVtgLow.SetValue(0.0);	
		m_CapVtgHigh.SetValue(0.0);	

		//
		for(int count = 0; count < SCH_MAX_COMP_POINT; count++)
		{
			m_CompV[count].SetValue(0.0);			
			m_CompV1[count].SetValue(0.0);			
			m_CompI[count].SetValue(0.0);			
			m_CompI1[count].SetValue(0.0);			
			m_CompTimeV[count].SetDateTime((DATE)0);
			m_CompTimeI[count].SetDateTime((DATE)0);
		}

		m_SafetyCellDeltaVStep.SetValue(0.0);
		m_StepDeltaAuxTemp.SetValue(0);
		m_StepDeltaAuxTH.SetValue(0.0);
		m_StepDeltaAuxT.SetValue(0.0);
		m_StepDeltaAuxVTime.SetValue(0.0);
		m_StepDeltaAuxV.SetValue(0.0);
		m_StepDeltaAuxV.EnableWindow(FALSE);

		m_bChkStepDeltaVentAuxV.SetCheck(FALSE);
		m_bChkStepDeltaVentAuxTemp.SetCheck(FALSE);
		m_bChkStepDeltaVentAuxTh.SetCheck(FALSE);
		m_bChkStepDeltaVentAuxT.SetCheck(FALSE);
		m_bChkStepVentAuxV.SetCheck(FALSE);
	}
	else
	{
		m_bExtOption = FALSE;
		//
		m_VtgHigh.SetValue((double)GetDocument()->VUnitTrans(pStep->fVLimitHigh, FALSE));	//pStep->fVLimitHigh/V_UNIT_FACTOR);
		m_VtgLow.SetValue((double)GetDocument()->VUnitTrans(pStep->fVLimitLow, FALSE));	//pStep->fVLimitLow/V_UNIT_FACTOR);
		m_CrtHigh.SetValue((double)GetDocument()->IUnitTrans(pStep->fILimitHigh, FALSE));	//pStep->fILimitHigh/I_UNIT_FACTOR);
		m_CrtLow.SetValue((double)GetDocument()->IUnitTrans(pStep->fILimitLow, FALSE));	//pStep->fILimitLow/I_UNIT_FACTOR);
		m_CapHigh.SetValue((double)GetDocument()->CUnitTrans(pStep->fCLimitHigh, FALSE));	//pStep->fCLimitHigh/C_UNIT_FACTOR);
		m_CapLow.SetValue((double)GetDocument()->CUnitTrans(pStep->fCLimitLow, FALSE));	//pStep->fCLimitLow/C_UNIT_FACTOR);
		m_TempHigh.SetValue((double)pStep->fTempHigh);	//pStep->fCLimitHigh/C_UNIT_FACTOR);
		m_TempLow.SetValue((double)pStep->fTempLow);	//pStep->fCLimitLow/C_UNIT_FACTOR);
		
		m_ImpHigh.SetValue((double)pStep->fImpLimitHigh/Z_UNIT_FACTOR);
		m_ImpLow.SetValue((double)pStep->fImpLimitLow /Z_UNIT_FACTOR);

		m_CapacitanceHigh.SetValue((double)GetDocument()->CUnitTrans(pStep->fCapacitanceHigh, FALSE));	//pStep->fCLimitHigh/C_UNIT_FACTOR);
		m_CapacitanceLow.SetValue((double)GetDocument()->CUnitTrans(pStep->fCapacitanceLow, FALSE));	//pStep->fCLimitLow/C_UNIT_FACTOR);
		
		//
		m_DeltaI.SetValue((double)GetDocument()->IUnitTrans(pStep->fDeltaI, FALSE));	//pStep->fDeltaI/I_UNIT_FACTOR);
		m_DeltaV.SetValue((double)GetDocument()->VUnitTrans(pStep->fDeltaV, FALSE));	//pStep->fDeltaV/V_UNIT_FACTOR);

		//
		m_ReportTemp.SetValue((double)pStep->fReportTemp);
		m_ReportV.SetValue((double)GetDocument()->VUnitTrans(pStep->fReportV, FALSE));	//pStep->fReportV/V_UNIT_FACTOR);
		m_ReportI.SetValue((double)GetDocument()->IUnitTrans(pStep->fReportI, FALSE));	//pStep->fReportI/I_UNIT_FACTOR);

		//
		m_CapVtgLow.SetValue((double)GetDocument()->VUnitTrans(pStep->fCapaVoltage1, FALSE));		//pStep->fCapaVoltage1/V_UNIT_FACTOR);
		m_CapVtgHigh.SetValue((double)GetDocument()->VUnitTrans(pStep->fCapaVoltage2, FALSE));	//pStep->fCapaVoltage2/V_UNIT_FACTOR);
		
		//Optin 영역
		m_VtgEndHigh.SetValue((double)GetDocument()->VUnitTrans(pStep->fVEndHigh, FALSE));	//pStep->fVEndHigh/V_UNIT_FACTOR);
		m_VtgEndLow.SetValue((double)GetDocument()->VUnitTrans(pStep->fVEndLow, FALSE));		//pStep->fVEndLow/V_UNIT_FACTOR);
		m_CrtEndHigh.SetValue((double)GetDocument()->IUnitTrans(pStep->fIEndHigh, FALSE));	//pStep->fIEndHigh/I_UNIT_FACTOR);
		m_CrtEndLow.SetValue((double)GetDocument()->IUnitTrans(pStep->fIEndLow, FALSE));		//pStep->fIEndLow/I_UNIT_FACTOR);
		if(pStep->fVEndHigh > 0 || pStep->fVEndLow > 0 || pStep->fIEndHigh> 0 || pStep->fIEndLow> 0)
		{
			m_bExtOption = TRUE;
		}
		
		COleDateTime time;
		div_t result;
		int hour;
		ULONG lSecond = pStep->lDeltaTime/100;
		result = div(lSecond, 3600);
		hour = result.quot;
		result = div(result.rem, 60) ;
		time.SetTime(hour, result.quot, result.rem);
		m_DeltaTime.SetTime(time);

		lSecond = pStep->ulReportTime/100;
		long lmiliSec = pStep->ulReportTime%100;
		result = div(lSecond, 3600);
		hour = result.quot;
		result = div(result.rem, 60) ;
		time.SetTime(hour, result.quot, result.rem);
		m_ReportTime.SetTime(time);


		CString strTemp;
		strTemp.Format("%03d", lmiliSec*10);
		GetDlgItem(IDC_REPORT_TIME_MILI)->SetWindowText(strTemp);

		for(int k = 0; k < SCH_MAX_COMP_POINT; k++)
		{
			m_CompV[k].SetValue((double)GetDocument()->VUnitTrans(pStep->fCompVLow[k], FALSE));		//pStep->fCompVLow[k]/V_UNIT_FACTOR);
			m_CompV1[k].SetValue((double)GetDocument()->VUnitTrans(pStep->fCompVHigh[k], FALSE));	//pStep->fCompVHigh[k]/V_UNIT_FACTOR);
			m_CompI[k].SetValue((double)GetDocument()->IUnitTrans(pStep->fCompILow[k], FALSE));		//pStep->fCompILow[k]/I_UNIT_FACTOR);
			m_CompI1[k].SetValue((double)GetDocument()->IUnitTrans(pStep->fCompIHigh[k], FALSE));	//pStep->fCompIHigh[k]/I_UNIT_FACTOR);

			lSecond = pStep->ulCompTimeV[k]/100;
			result = div(lSecond, 3600);
			hour = result.quot;
			result = div(result.rem, 60) ;
			time.SetTime(hour, result.quot, result.rem);
			m_CompTimeV[k].SetDateTime(time);
			
			lSecond = pStep->ulCompTimeI[k]/100;
			result = div(lSecond, 3600);
			hour = result.quot;
			result = div(result.rem, 60) ;
			time.SetTime(hour, result.quot, result.rem);
			m_CompTimeI[k].SetDateTime(time);

			if( pStep->fCompVLow[k] > 0 || pStep->fCompVHigh[k] > 0 || 
				pStep->fCompILow[k] > 0 || pStep->fCompIHigh[k] > 0 ||
				pStep->ulCompTimeV[k] > 0 || pStep->ulCompTimeI[k] > 0
				)
			{
				m_bExtOption = TRUE;
			}
		}

		//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
		UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
		if (SchFileVer >= SCHEDULE_FILE_VER_v1015_v1)
		{
			if (pStep->chType == PS_STEP_LOOP || pStep->chType == PS_STEP_ADV_CYCLE || pStep->chType == PS_STEP_END)
			{
				m_StepDeltaAuxTemp.SetValue(0.0);	
				m_StepDeltaAuxTH.SetValue(0.0);		
				m_StepDeltaAuxT.SetValue(0.0);	
				m_StepDeltaAuxVTime.SetValue(0.0);	
				m_StepDeltaAuxV.SetValue(0.0);	
				m_SafetyCellDeltaVStep.SetValue(0.0);	

				m_bChkStepDeltaVentAuxV.SetCheck(FALSE);
				m_bChkStepDeltaVentAuxTemp.SetCheck(FALSE);
				m_bChkStepDeltaVentAuxTh.SetCheck(FALSE);
				m_bChkStepDeltaVentAuxT.SetCheck(FALSE);
				m_bChkStepVentAuxV.SetCheck(FALSE);
				m_StepDeltaAuxV.EnableWindow(FALSE);
			}
			else
			{
				m_StepDeltaAuxTemp.SetValue((double)GetDocument()->VUnitTrans(pStep->fStepDeltaAuxTemp, FALSE));	
				m_StepDeltaAuxTH.SetValue((double)GetDocument()->VUnitTrans(pStep->fStepDeltaAuxTh, FALSE));	
				m_StepDeltaAuxT.SetValue((double)GetDocument()->VUnitTrans(pStep->fStepDeltaAuxT, FALSE));
				m_StepDeltaAuxVTime.SetValue(pStep->fStepDeltaAuxVTime/100);
				m_StepDeltaAuxV.SetValue((double)GetDocument()->VUnitTrans(pStep->fStepAuxV, FALSE));
				m_SafetyCellDeltaVStep.SetValue((double)GetDocument()->VUnitTrans(pStep->fCellDeltaVStep, FALSE));	

				m_bChkStepDeltaVentAuxV.SetCheck(pStep->bStepDeltaVentAuxV);
				m_bChkStepDeltaVentAuxTemp.SetCheck(pStep->bStepDeltaVentAuxTemp);
				m_bChkStepDeltaVentAuxTh.SetCheck(pStep->bStepDeltaVentAuxTh);
				m_bChkStepDeltaVentAuxT.SetCheck(pStep->bStepDeltaVentAuxT);
				m_bChkStepVentAuxV.SetCheck(pStep->bStepVentAuxV);

				double	dwValue1 =0.0;
				m_StepDeltaAuxVTime.GetValue(dwValue1);
				//if (dwValue1>0)
				if (dwValue1>0 && m_bUseAuxV)
				{
					m_StepDeltaAuxV.EnableWindow(TRUE);
				}
				else
				{
					m_StepDeltaAuxV.EnableWindow(FALSE);
				}
			}
		}				
		
		//Option에 설정된 값이 있으면 자동으로 표시한다.
		if(m_bExtOption && 	m_nCurTabIndex == TAB_SAFT_VAL)
		{
			ShowExtOptionCtrl(SW_SHOW);	
		}
		else
		{
			ShowExtOptionCtrl(SW_HIDE);	
		}
		UpdateData(FALSE);
	}

	DisplayStepGrade(pStep);
//	DisplayStepChamber(pStep);
}

void CCTSEditorProView::SetControlEnable(int nStepType)
{

}

void CCTSEditorProView::OnModelEdit() 
{
	// TODO: Add your control notification handler code here
	ModelEdit();
}

void CCTSEditorProView::OnFilePrint() 
{
	// TODO: Add your command handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;	
}

void CCTSEditorProView::SetDefultTest(CString strModel, CString strTest)
{
	if(!strModel.IsEmpty())
	{
		//현재 이름과 match되는 Model ID를 구한다.
		for(int row =0; row<m_pWndCurModelGrid->GetRowCount(); row++)
		{
			if(m_pWndCurModelGrid->GetValueRowCol(row+1, COL_MODEL_NAME) == strModel)
			{

				UpdateDspModel(strModel, atol(m_pWndCurModelGrid->GetValueRowCol(row+1, COL_MODEL_PRIMARY_KEY)));
				m_pWndCurModelGrid->SetCurrentCell(row+1, COL_MODEL_NAME);
				
				if(!strTest.IsEmpty())
				{
					if(RequeryTestList(m_lDisplayModelID, strTest))
					{
						OnLoadTest();
					}
				}
				break;
			}
		}

		AfxGetMainWnd()->ShowWindow(SW_SHOW);
		m_pModelDlg->ShowWindow(SW_HIDE);

/*		if(RequeryBatteryModel(strModel) && !strTest.IsEmpty())
		{
			if(RequeryTestList(m_lDisplayModelID, strTest))
			{
				OnLoadTest();
			}
		}
*/
	}
}

void CCTSEditorProView::OnInsertStep() 
{
	// TODO: Add your command handler code here
	OnStepInsert();
}

void CCTSEditorProView::OnDeleteStep() 
{
	// TODO: Add your command handler code here
	OnStepDelete();
}

void CCTSEditorProView::OnUpdateInsertStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
	
}

void CCTSEditorProView::OnUpdateDeleteStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndStepGrid == (CMyGridWnd *)GetFocus())
	{
		pCmdUI->Enable(TRUE );
	}
	else
	{
		pCmdUI->Enable(FALSE );
	}
	
}

void CCTSEditorProView::SetStepCycleColumn()
{
	int nTotStepNum = GetDocument()->GetTotalStepNum();
	STEP *pStep;

	int advCycleStep =1;
//	m_wndStepGrid.StoreCoveredCellsRowCol(1, COL_STEP_CYCLE, nTotStepNum,COL_STEP_CYCLE);
//	ROWCOL nRow, nCol;

	CGXRange covered;
	BOOL bLock = m_wndStepGrid.LockUpdate(TRUE);

//	if (m_wndStepGrid.GetCoveredCellsRowCol(1, COL_STEP_CYCLE, covered))
//	{
//	}

	DWORD CycleCount = 0;
	for(int i = 0; i< nTotStepNum; i++)		//
	{
		m_wndStepGrid.SetCoveredCellsRowCol(i+1, COL_STEP_CYCLE, i+1, COL_STEP_CYCLE);
		
		pStep = (STEP *)GetDocument()->GetStepData(i+1);
		ASSERT(pStep);

		if(pStep->chType < 0 )
		{
			break;
		}
		if( pStep->chType == PS_STEP_ADV_CYCLE)
		{
			advCycleStep = i+1;
//			CycleCount++;
		}
		if(pStep->chType == PS_STEP_LOOP)
		{
			CycleCount++;
		}

		if(pStep->chType != PS_STEP_END && pStep->chType != PS_STEP_ADV_CYCLE)
		{
			m_wndStepGrid.SetCoveredCellsRowCol(advCycleStep, COL_STEP_CYCLE, i+1, COL_STEP_CYCLE);
			m_wndStepGrid.SetValueRange(CGXRange(advCycleStep,COL_STEP_CYCLE), CycleCount);
		//	TRACE("MergeCell1 Row(%d, %d)\n", advCycleStep, i+1);
		}

		if(pStep->chType == PS_STEP_LOOP)
		{
			m_wndStepGrid.SetCoveredCellsRowCol(advCycleStep, COL_STEP_CYCLE, i+1, COL_STEP_CYCLE);
		//	TRACE("MergeCell2 Row(%d, %d)\n", advCycleStep, i+1);
			advCycleStep = i+2;
//			CycleCount++;
		}
	}

	m_wndStepGrid.LockUpdate(bLock);
	m_wndStepGrid.Redraw();

}

CString CCTSEditorProView::StepTypeMsg(int type)
{
	CString msg("None");
	switch(type)
	{
	case PS_STEP_NONE		:		msg = "None";		break; 
	case PS_STEP_CHARGE		:		msg = "Charge";		break; 
	case PS_STEP_DISCHARGE	:		msg = "Discharge";	break; 
	case PS_STEP_REST		:		msg = "Rest";		break; 
	case PS_STEP_OCV		:		msg = "OCV";		break; 
	case PS_STEP_IMPEDANCE	:		msg = "Impedance";	break; 
	case PS_STEP_END		:		msg = "END";		break; 
	case PS_STEP_ADV_CYCLE	:		msg = "Cycle";		break; 
	case PS_STEP_LOOP		:		msg = "Loop";		break; 
	case PS_STEP_PATTERN	:		msg = "Simuation";	break;
	case PS_STEP_USER_MAP	:		msg = "UserMap";	break;
	case PS_STEP_EXT_CAN	:	    msg = "Extern CAN";	break;	//ljb 2011318 이재복 //////////
	default					:		msg = "None";		break; 
	}
	return msg;
}

CString CCTSEditorProView::ModeTypeMsg(int type, int mode)
{
	CString msg;

	if(type == PS_STEP_CHARGE || type == PS_STEP_DISCHARGE)
	{
		switch(mode)
		{
		case PS_MODE_CCCV:		msg = "CC/CV";		break;	
		case PS_MODE_CC	:		msg = "CC";			break;	
		case PS_MODE_CV	:		msg = "CV";			break;	
		case PS_MODE_CP	:		msg = "CP";			break;	
		case PS_MODE_PUSE:		msg = "Pulse";		break;
		case PS_MODE_CR:		msg = "CR";			break;
		case PS_MODE_CCCP:		msg = "CC/CP";		break;		//ljb 20101214
		}
	}
	else if(type = PS_STEP_IMPEDANCE)
	{
		if(mode == PS_MODE_DCIMP)
		{
			msg = "DC";
		}
		else if(mode == PS_MODE_ACIMP)
		{
			msg = "AC";
		}
	}
	return msg;
}



void CCTSEditorProView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
//	TRACE("m_bStepDataSaved : %d // Edited : %d\r\n", m_bStepDataSaved, GetDocument()->IsEdited());
	
	if(m_bStepDataSaved == FALSE || GetDocument()->IsEdited())
	{
		GetDlgItem(IDC_STEP_SAVE)->EnableWindow(TRUE);

	}
	else
	{
		GetDlgItem(IDC_STEP_SAVE)->EnableWindow(FALSE);
	}
	
	CFormView::OnTimer(nIDEvent);
}

void CCTSEditorProView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	//cny 201809
	HideEndDlg();
	HideCellBalDlg();
	HidePwrSupplyDlg();
	HideChillerDlg();
	HidePattSchedDlg();
	HidePattSelectDlg();

	CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

void CCTSEditorProView::OnTestCopyButton() 
{
	// TODO: Add your control notification handler code here

	CRowColArray	awRows;
	m_wndTestListGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo <= 0)
	{
		MessageBox(Fun_FindMsg("OnTestCopyButton_msg1","IDD_CTSEditorPro_CYCL"), Fun_FindMsg("OnTestCopyButton_msg2","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
		return;
	}

	CString strMsg;
	CDaoDatabase  db;
	CCTSEditorProDoc *pDoc =  GetDocument();
	db.Open(g_strDataBaseName);
	CString name, descript, creator;
	long check, typeID, testNo, lTestID, lFromTestID;
	COleDateTime modtime;
	COleVariant data;

	CString strSQL;
	CTestNameDlg	dlg(m_lDisplayModelID, TRUE, this);
	
	for (int i = 0; i<nSelNo; i++)
	{
		if(awRows[i] > 0)
		{
			if(m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_EDIT_STATE) !=  CS_NEW)
			{
				lFromTestID = atol(m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_PRIMARY_KEY));
				strSQL.Format("SELECT TestNo, TestName, Description, Creator, PreTestCheck, ProcTypeID, ModifiedTime FROM TestName WHERE ModelID = %d AND TestID =%d", m_lDisplayModelID, lFromTestID);
				CDaoRecordset rs(&db);
				rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				if( rs.IsBOF()|| rs.IsEOF())	
				{
					strMsg.Format(Fun_FindMsg("OnTestCopyButton_msg3","IDD_CTSEditorPro_CYCL"), m_wndTestListGrid.GetValueRowCol(awRows[i], COL_TEST_NAME));
					MessageBox(strMsg, Fun_FindMsg("OnTestCopyButton_msg2","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
					rs.Close();
					continue;
				}
				data = rs.GetFieldValue(0);		testNo = data.lVal;
				data = rs.GetFieldValue(1);		name = data.pbVal;
				data = rs.GetFieldValue(2);		descript = data.pbVal;
				data = rs.GetFieldValue(3);		creator = data.pbVal;
				data = rs.GetFieldValue(4);		check = data.lVal;
				data = rs.GetFieldValue(5);		typeID = data.lVal;
			//	data = rs.GetFieldValue(6);		modtime = data;
				modtime = COleDateTime::GetCurrentTime();
				rs.Close();

				dlg.m_strName = name;
				dlg.m_strDecription = descript;
				dlg.m_strCreator = creator;
				dlg.m_createdDate = modtime;
				dlg.m_lModelID = m_lDisplayModelID;
				dlg.m_lTestTypeID = typeID;
				if(IDOK != dlg.DoModal())
				{
					continue;
				}

				//Type이 바뀌었으면 
				if(typeID != dlg.m_lTestTypeID)
				{
					//Step을 검사하여 시행 불가능한 Step이 포함되어 있으면 경고 표시
					if(CheckExecutableStep( GetTestIndexFromType(dlg.m_lTestTypeID), lFromTestID) == FALSE)
					{
						strMsg.Format(Fun_FindMsg("OnTestCopyButton_msg4","IDD_CTSEditorPro_CYCL"),
							dlg.m_strName);
						if(MessageBox(strMsg, "Error", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
						{
							continue;
						}
					}
				}
				
				int nCount = m_wndTestListGrid.GetRowCount()+1;

				strSQL.Format("INSERT INTO TestName(ModelID, TestName, Description, Creator, PreTestCheck, ProcTypeID, TestNo, ModifiedTime) VALUES (%d, '%s', '%s', '%s', %d, %d, %d, '%s')",
						dlg.m_lModelID, dlg.m_strName, dlg.m_strDecription, dlg.m_strCreator, check, typeID, nCount, modtime.Format());

				db.Execute(strSQL);

					//Insert 된 test의 ID를 구한다.
				strSQL = "SELECT MAX(TestID) FROM TestName";
				CDaoRecordset rs1(&db);
				rs1.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				if(!rs1.IsBOF())
				{
					lTestID = rs1.GetFieldValue(0).lVal;
				}
				rs1.Close();

				if(CopyCheckParam(lFromTestID, lTestID) == FALSE)
				{

				}
				if(CopyStepList(lFromTestID, lTestID))
				{
				}
				
				if(dlg.m_lModelID == m_lDisplayModelID)
				{
					//복사 성공시 추가 
					m_wndTestListGrid.InsertRows(nCount, 1);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_PROC_TYPE), (ROWCOL)GetTestIndexFromType(dlg.m_lTestTypeID));	
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_NAME), dlg.m_strName);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_CREATOR), dlg.m_strCreator);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_DESCRIPTION), dlg.m_strDecription);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_EDIT_TIME), dlg.m_createdDate.Format());
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_PRIMARY_KEY), lTestID);
					m_wndTestListGrid.SetValueRange(CGXRange(nCount, COL_TEST_EDIT_STATE), CS_SAVED);
					m_wndTestListGrid.SetCurrentCell(nCount, 1);
				}
			}
		}
	}
	
	db.Close();
	RequeryTestList(m_lDisplayModelID);			//Re Display Test Condition
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)	
	
	return;	
}

//////////////////////////////////////////////////////////////////////////
//Record 조건 변경 검사 
void CCTSEditorProView::OnChangeReportCurrent() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeReportTemperature() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeReportVoltage() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnDatetimechangeReportTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;

	//lmh start cp_cc모드일경우 cp의 안전조건값 수정시 그다음스텝에도 적용되도록 만듬
	if(m_nDisplayStep > 0)	//UpDate 1 Step Grid
	{
		CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
		STEP *pStep,*pStep2;
		
		pStep = (STEP *)pDoc->GetStepData(m_nDisplayStep);
		if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
		{
			CString strTemp;
			COleDateTime time;
			m_ReportTime.GetTime(time);
			pStep->ulReportTime = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;		//저장 간격 설정 시간 
			GetDlgItem(IDC_REPORT_TIME_MILI)->GetWindowText(strTemp);
			pStep->ulReportTime += atol(strTemp)/10;
			
			if(pStep->bUseLinkStep)
			{
				pStep2 = (STEP *)pDoc->GetStepData(m_nDisplayStep-1);
				pStep2->ulReportTime = pStep->ulReportTime;
			}
			else 
			{
				pStep2 = (STEP *)pDoc->GetStepData(m_nDisplayStep+1);
				if(pStep2->bUseLinkStep)
					pStep2->ulReportTime = pStep->ulReportTime;
			}
		}
	}
	//lmh end ==========================================================
	*pResult = 0;
}
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Cell Check 조건 변경 검사 
void CCTSEditorProView::OnChangePretestMaxv() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangePretestDeltaV() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangePretestFaultNo() 
{
	//Data를 Loading하면서 발생하므로 생략한다.
//	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangePretestMaxi() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangePretestMinv() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangePretestOcv() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangePretestTrckleI() 
{
	m_bStepDataSaved = FALSE;
}

LRESULT CCTSEditorProView::OnDateTimeChanged(WPARAM wParam, LPARAM lParam)
{
	m_bStepDataSaved = FALSE;
	return 1;
}
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//안전 조건 
void CCTSEditorProView::OnChangeVtgHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeVtgLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCrtHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCrtLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCapHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCapLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeImpHigh() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeImpLow() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeDeltaV() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeDeltaI() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeDeltaTime() 
{
	m_bStepDataSaved = FALSE;
}
void CCTSEditorProView::OnChangeCapVtgLow() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeCapVtgHigh() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeCompITime1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompITime2() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompITime3() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompI1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompI2() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompI3() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompI4() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompI5() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompI6() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompV1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompV2() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompV3() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompTime1() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompTime2() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeCompTime3() 
{
	m_bStepDataSaved = FALSE;
}
void CCTSEditorProView::OnChangeCompV4() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompV5() 
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCompV6() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnEnChangeStepAuxtDelta()
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnEnChangeStepAuxthDelta()
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnEnChangeStepAuxtAuxthDelta()
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnEnChangeStepAuxvTime()
{
	double	dwValue1 =0.0;
	m_StepDeltaAuxVTime.GetValue(dwValue1);
	if(dwValue1<0 || dwValue1==0)
	{
		dwValue1 =0.0;
		m_StepDeltaAuxVTime.SetValue(dwValue1);
		m_StepDeltaAuxV.EnableWindow(FALSE);
	}
	else
	{		
		if(m_bUseAuxV)
			m_StepDeltaAuxV.EnableWindow(TRUE);
		else
			m_StepDeltaAuxV.EnableWindow(FALSE);
	}

	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnEnChangeStepAuxvDelta()
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::AddUndoStep()
{
	GetDocument()->MakeUndoStep();
	m_bPasteUndo = TRUE;		//삭제 Undo 가능 하게 
}

/*
---------------------------------------------------------
---------
-- Filename		:	CTSEditorProView.cpp 
-- Description	:
-- Author		:	
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 2014.09.22	이민규		기록조건 시간 설정을 10으로 변경.
----------------------------------------------------------
*/
void CCTSEditorProView::OnDeltaposRptMsecSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CString strData;		
	GetDlgItem(IDC_REPORT_TIME_MILI)->GetWindowText(strData);	
	
	//2014.09.22 기록조건 시간을 100으로 변경.
	int nMinInterValTime = 100;//default 100
	
	int tmpInt;
	tmpInt = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec Interval",0); //yulee 20180829 
	
	if(tmpInt != 0)
		nMinInterValTime = tmpInt;
	
	int mtime = atol(strData);
	if(pNMUpDown->iDelta > 0)
	{	
		//2014.09.22 기록조건 시간을 10으로 변경.
		//10밀리세컨드를 사용할지 구분.
		if(m_bUseMsec){			
			mtime -= nMinInterValTime;
			if(mtime < 0)	mtime = (1000-nMinInterValTime);			
		}else{		
			//mtime -= MIN_TIME_INTERVAL;
			//if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);			

			//ksj 20171031 : 사용자 정의 값 옵션 추가
			if(m_nCustomMsec)
			{
				mtime -= m_nCustomMsec;
				if(mtime < 0)	mtime = (1000-m_nCustomMsec);			
			}
			else
			{
				mtime -= MIN_TIME_INTERVAL;
				if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);			
			}
			//ksj end

		}
		
		//if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);
		//mtime -= MIN_TIME_INTERVAL;
		//mtime -= nMinInterValTime;
		//if(mtime < 0)	mtime = (1000-nMinInterValTime);
	}
	else
	{
		//2014.09.22 기록조건 시간을 10으로 변경.
		if(m_bUseMsec){
			mtime += nMinInterValTime;
		}else{
			//mtime += MIN_TIME_INTERVAL;

			//ksj 20171031 : 사용자 입력 값 추가.
			if(m_nCustomMsec)
				mtime += m_nCustomMsec;
			else
				mtime += MIN_TIME_INTERVAL;
			//ksj end

			
		}

		if(mtime >= 1000)	mtime = 0;
		
		//mtime += MIN_TIME_INTERVAL;
		//mtime += nMinInterValTime;
		//if(mtime >= 1000)	mtime = 0;
	}
	strData.Format("%03d", mtime);
	GetDlgItem(IDC_REPORT_TIME_MILI)->SetWindowText(strData);	
	
	m_bStepDataSaved = FALSE;

	strData.Empty();
	if(mtime > 0)
	{
//ljb 20121226		strData.Format("Step 시작부터 최대 %d초간 %dmsec 간격으로 기록 후 1초간격으로 자동 전환하여 저장", mtime*SCH_MAX_MSEC_DATA_POINT/1000, mtime);
	}
//	GetDlgItem(IDC_WARRING_STATIC)->SetWindowText(strData);
	m_strWarning.SetTextColor(RGB(255, 0 ,0));

	*pResult = 0;
}


void CCTSEditorProView::OnChangeCellCheckTimeEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnAllStepButton() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);

	ApplySelectStep();
	m_bStepDataSaved = FALSE;	

}

void CCTSEditorProView::OnSameAllStepButton() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);

	STEP *pStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pStep)
	{
		ApplySelectStep(pStep->chType);
		m_bStepDataSaved = FALSE;	
	}
}

void CCTSEditorProView::ApplySelectCanAuxStep(int nCopyType, int nStepType)
{
	STEP *pStep = NULL;
	STEP *pCurStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pCurStep == NULL)	return;

	int a;
	for(int step=0; step<GetDocument()->GetTotalStepNum(); step++)
	{
		pStep = GetDocument()->GetStepData(step+1);
		if(pStep == NULL)	break;

		//같은 step만 복사
		if(nStepType > 0 && nStepType != pStep->chType)		continue;

		switch(pStep->chType)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
		case PS_STEP_PATTERN:
		case PS_STEP_USER_MAP:
		case PS_STEP_EXT_CAN:			//ljb 2011318 이재복 //////////
		case PS_STEP_REST:		//Voltage
		case PS_STEP_OCV:
		case PS_STEP_IMPEDANCE:
			for(a =0; a<SCH_MAX_END_CAN_AUX_POINT; a++)
			{
				if (nCopyType == ID_CAN_TYPE)
				{
					pStep->ican_function_division[a] = pCurStep->ican_function_division[a];
					pStep->cCan_compare_type[a] = pCurStep->cCan_compare_type[a];
					pStep->cCan_data_type[a] = pCurStep->cCan_data_type[a];
					pStep->ican_branch[a] = pCurStep->ican_branch[a];
					pStep->fcan_Value[a] = pCurStep->fcan_Value[a];
				}
				else if (nCopyType == ID_AUX_TYPE)
				{
					pStep->iaux_function_division[a] = pCurStep->iaux_function_division[a];
					pStep->cAux_compare_type[a] = pCurStep->cAux_compare_type[a];
					pStep->cAux_data_type[a] = pCurStep->cAux_data_type[a];
					pStep->iaux_branch[a] = pCurStep->iaux_branch[a];
					pStep->faux_Value[a] = pCurStep->faux_Value[a];
					pStep->iaux_conti_time[a] = pCurStep->iaux_conti_time[a];
				}
			}
			break;
		default:
			break;
		}
	}
}

void CCTSEditorProView::ApplySelectStep(int nStepType)
{
	STEP *pStep = NULL;
	STEP *pCurStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pCurStep == NULL)	return;

	int a;
	for(int step=0; step<GetDocument()->GetTotalStepNum(); step++)
	{
		pStep = GetDocument()->GetStepData(step+1);
		if(pStep == NULL)	break;

		//같은 step만 복사
		if(nStepType > 0 && nStepType != pStep->chType)		continue;

		switch(m_nCurTabIndex)
		{
		case TAB_SAFT_VAL	:		//안전 조건 
			{
				switch(pStep->chType)
				{
				case PS_STEP_CHARGE:
				case PS_STEP_DISCHARGE:
				case PS_STEP_PATTERN:
				case PS_STEP_USER_MAP:
				case PS_STEP_EXT_CAN:	//ljb 2011318 이재복 //////////
					pStep->fVLimitHigh = pCurStep->fVLimitHigh;
					pStep->fVLimitLow = pCurStep->fVLimitLow;
					pStep->fILimitHigh = pCurStep->fILimitHigh;
					pStep->fILimitLow = pCurStep->fILimitLow;
					pStep->fCLimitHigh = pCurStep->fCLimitHigh;
					pStep->fCLimitLow = pCurStep->fCLimitLow;
					pStep->fCapacitanceHigh	= pCurStep->fCapacitanceHigh;
					pStep->fCapacitanceLow	= pCurStep->fCapacitanceLow;
					for(a =0; a<SCH_MAX_COMP_POINT; a++)
					{
						pStep->fCompVHigh[a] = pCurStep->fCompVHigh[a];
						pStep->fCompVLow[a] = pCurStep->fCompVLow[a];
						pStep->ulCompTimeV[a] = pCurStep->ulCompTimeV[a];

						pStep->fCompIHigh[a] = pCurStep->fCompIHigh[a];
						pStep->fCompILow[a] = pCurStep->fCompILow[a];
						pStep->ulCompTimeI[a] = pCurStep->ulCompTimeI[a];
					}

					pStep->fCapaVoltage1 = pCurStep->fCapaVoltage1;			//EDLC 용량 검사시 전압 Point1
					pStep->fCapaVoltage2 = pCurStep->fCapaVoltage2;			//EDLC 용량 검사시 전압 Point2
					pStep->fLCStartTime = pCurStep->fLCStartTime;			//EDLC LC 측정 시작 시간
					pStep->fLCEndTime = pCurStep->fLCEndTime;				//EDLC LC 측정 종료 시간
					pStep->fTempHigh = pCurStep->fTempHigh;				
					pStep->fTempLow = pCurStep->fTempLow;
					pStep->fCellDeltaVStep = pCurStep->fCellDeltaVStep;//yulee 20190531_5

					pStep->fStepDeltaAuxTemp = pCurStep->fStepDeltaAuxTemp;
					pStep->fStepDeltaAuxTh = pCurStep->fStepDeltaAuxTh;
					pStep->fStepDeltaAuxT = pCurStep->fStepDeltaAuxT;
					pStep->fStepDeltaAuxVTime = pCurStep->fStepDeltaAuxVTime;
					pStep->fStepAuxV = pCurStep->fStepAuxV;

					pStep->bStepDeltaVentAuxV = pCurStep->bStepDeltaVentAuxV;
					pStep->bStepDeltaVentAuxTemp = pCurStep->bStepDeltaVentAuxTemp;
					pStep->bStepDeltaVentAuxTh = pCurStep->bStepDeltaVentAuxTh;
					pStep->bStepDeltaVentAuxT = pCurStep->bStepDeltaVentAuxT;
					pStep->bStepVentAuxV = pCurStep->bStepVentAuxV;

					break;

				case PS_STEP_REST:		//Voltage
				case PS_STEP_OCV:
					pStep->fVLimitHigh = pCurStep->fVLimitHigh;
					pStep->fVLimitLow = pCurStep->fVLimitLow;

					pStep->fCellDeltaVStep = pCurStep->fCellDeltaVStep;//yulee 20190531_5
					pStep->fStepDeltaAuxTemp = pCurStep->fStepDeltaAuxTemp;
					pStep->fStepDeltaAuxTh = pCurStep->fStepDeltaAuxTh;
					pStep->fStepDeltaAuxT = pCurStep->fStepDeltaAuxT;
					pStep->fStepDeltaAuxVTime = pCurStep->fStepDeltaAuxVTime;
					pStep->fStepAuxV = pCurStep->fStepAuxV;

					pStep->bStepDeltaVentAuxV = pCurStep->bStepDeltaVentAuxV;
					pStep->bStepDeltaVentAuxTemp = pCurStep->bStepDeltaVentAuxTemp;
					pStep->bStepDeltaVentAuxTh = pCurStep->bStepDeltaVentAuxTh;
					pStep->bStepDeltaVentAuxT = pCurStep->bStepDeltaVentAuxT;
					pStep->bStepVentAuxV = pCurStep->bStepVentAuxV;

					break;
					
				case PS_STEP_IMPEDANCE:
					pStep->fVLimitHigh = pCurStep->fVLimitHigh;
					pStep->fVLimitLow = pCurStep->fVLimitLow;
					pStep->fILimitHigh = pCurStep->fILimitHigh;
					pStep->fILimitLow = pCurStep->fILimitLow;
					pStep->fImpLimitHigh = pCurStep->fImpLimitHigh;
					pStep->fImpLimitLow = pCurStep->fImpLimitLow;
					pStep->fDCRStartTime = pCurStep->fDCRStartTime;			//EDLC DCR 검사시 Start Time
					pStep->fDCREndTime = pCurStep->fDCREndTime;			//EDLC DCR 검사시 End Time
					pStep->fCellDeltaVStep = pCurStep->fCellDeltaVStep;//yulee 20190531_5

					pStep->fStepDeltaAuxTemp = pCurStep->fStepDeltaAuxTemp;
					pStep->fStepDeltaAuxTh = pCurStep->fStepDeltaAuxTh;
					pStep->fStepDeltaAuxT = pCurStep->fStepDeltaAuxT;
					pStep->fStepDeltaAuxVTime = pCurStep->fStepDeltaAuxVTime;
					pStep->fStepAuxV = pCurStep->fStepAuxV;

					pStep->bStepDeltaVentAuxV = pCurStep->bStepDeltaVentAuxV;
					pStep->bStepDeltaVentAuxTemp = pCurStep->bStepDeltaVentAuxTemp;
					pStep->bStepDeltaVentAuxTh = pCurStep->bStepDeltaVentAuxTh;
					pStep->bStepDeltaVentAuxT = pCurStep->bStepDeltaVentAuxT;
					pStep->bStepVentAuxV = pCurStep->bStepVentAuxV;
					break;

				case PS_STEP_LOOP:
				case PS_STEP_END:	
				case PS_STEP_ADV_CYCLE:
				default:
					break;
				}
				break;
			}

		case TAB_GRADE_VAL	:		//등급 조건 (같은 종류step만 지원)
			if(pStep->chType == nStepType)
			{
				switch(pStep->chType)
				{
				case PS_STEP_CHARGE:			//용량 Grading
				case PS_STEP_DISCHARGE:
				case PS_STEP_PATTERN:	
				case PS_STEP_USER_MAP:					
				case PS_STEP_EXT_CAN:		//ljb 2011318 이재복 //////////
				case PS_STEP_IMPEDANCE:
				case PS_STEP_OCV:
					//copy grade condition
					pStep->bGrade =  pCurStep->bGrade;
					for(a =0; a<SCH_MAX_GRADE_STEP; a++)
					{
						//20081208 KHS
						pStep->sGrading_Val.faValue1[a] = pCurStep->sGrading_Val.faValue1[a];
						pStep->sGrading_Val.faValue2[a] = pCurStep->sGrading_Val.faValue2[a];
						pStep->sGrading_Val.lGradeItem[a] = pCurStep->sGrading_Val.lGradeItem[a];
						pStep->sGrading_Val.aszGradeCode[a] =  pCurStep->sGrading_Val.aszGradeCode[a];
					}
					pStep->sGrading_Val.chTotalGrade = pCurStep->sGrading_Val.chTotalGrade;
					break;
		
				case PS_STEP_REST:
				case PS_STEP_LOOP:
				case PS_STEP_END:	
				case PS_STEP_ADV_CYCLE:
				default:
					break;
				}
				break;
			}

		case TAB_RECORD_VAL	:		//기록 조건 
			{
				switch(pStep->chType)
				{
				case PS_STEP_CHARGE:
				case PS_STEP_DISCHARGE:
				case PS_STEP_IMPEDANCE:
				case PS_STEP_PATTERN:
				case PS_STEP_USER_MAP:
				case PS_STEP_EXT_CAN:		//ljb 2011318 이재복 //////////
					pStep->fReportV = pCurStep->fReportV;
					pStep->fReportI = pCurStep->fReportI;
					pStep->ulReportTime = pCurStep->ulReportTime;
					pStep->fReportTemp = pCurStep->fReportTemp;
					break;

				case PS_STEP_REST:
					pStep->fReportV = pCurStep->fReportV;
					pStep->fReportI = pCurStep->fReportI;
					pStep->fReportTemp = pCurStep->fReportTemp;					
					pStep->ulReportTime = pCurStep->ulReportTime;
					break;
				case PS_STEP_OCV:
				case PS_STEP_LOOP:
				case PS_STEP_END:	
				case PS_STEP_ADV_CYCLE:
				default:
					break;
				}
			}
			break;
		}
	}
}

void CCTSEditorProView::ApplySelectStepCanMode(int nStepType)
{
	STEP *pStep = NULL;
	STEP *pCurStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pCurStep == NULL)	return;
	
//	int a;
	for(int step=0; step<GetDocument()->GetTotalStepNum(); step++)
	{
		pStep = GetDocument()->GetStepData(step+1);
		if(pStep == NULL)	break;
		
		//같은 step만 복사
		if(nStepType > 0 && nStepType != pStep->chType)		continue;
		pStep->nCanCheckMode = pCurStep->nCanCheckMode;
	}
}

void CCTSEditorProView::SetApplyAllBtn(int nType)
{
	BOOL UseCanCheckMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseCanCheckMode", FALSE);	//20150825 add
	if (UseCanCheckMode)
	{
		GetDlgItem(IDC_ALL_STEP_BUTTON2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_ALL_STEP_BUTTON2)->EnableWindow(TRUE);
		GetDlgItem(IDC_SAME_ALL_STEP_BUTTON2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SAME_ALL_STEP_BUTTON2)->EnableWindow(TRUE);
	}
	
	GetDlgItem(IDC_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	//ljb 2011221 이재복 //////////
	GetDlgItem(IDC_CAN_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAME_CAN_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_AUX_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_SAME_AUX_ALL_STEP_BUTTON)->EnableWindow(TRUE);
	CString strTemp, strMsg, strMsgCan, strMsgAux;
	switch(m_nCurTabIndex)
	{
	case TAB_SAFT_VAL	:		//안전 조건 
		strTemp = Fun_FindMsg("SetApplyAllBtn_msg1","IDD_CTSEditorPro_CYCL");
		GetDlgItem(IDC_ALL_STEP_BUTTON)->SetWindowText(Fun_FindMsg("SetApplyAllBtn_msg2","IDD_CTSEditorPro_CYCL"));
		break;
	case TAB_GRADE_VAL	:		//등급 조건 
		strTemp = Fun_FindMsg("SetApplyAllBtn_msg3","IDD_CTSEditorPro_CYCL");
		GetDlgItem(IDC_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_ALL_STEP_BUTTON)->SetWindowText(Fun_FindMsg("SetApplyAllBtn_msg4","IDD_CTSEditorPro_CYCL"));
		break;
	case TAB_RECORD_VAL	:		//기록 조건 
		strTemp = Fun_FindMsg("SetApplyAllBtn_msg5","IDD_CTSEditorPro_CYCL");
		GetDlgItem(IDC_ALL_STEP_BUTTON)->SetWindowText(Fun_FindMsg("SetApplyAllBtn_msg6","IDD_CTSEditorPro_CYCL"));
		break;
	}

	switch(nType)
	{
	case PS_STEP_CHARGE:
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg7","IDD_CTSEditorPro_CYCL"),strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg8","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg9","IDD_CTSEditorPro_CYCL"));
		break;

	case PS_STEP_DISCHARGE:
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg10","IDD_CTSEditorPro_CYCL"),strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg11","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg12","IDD_CTSEditorPro_CYCL"));
		break;

	case PS_STEP_REST:
		if(m_nCurTabIndex == TAB_GRADE_VAL)
		{
			GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		}
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg13","IDD_CTSEditorPro_CYCL"),strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg14","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg15","IDD_CTSEditorPro_CYCL"));
		break;
		
	case PS_STEP_IMPEDANCE:
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg16","IDD_CTSEditorPro_CYCL"),strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg17","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg18","IDD_CTSEditorPro_CYCL"));
		break;

	case PS_STEP_OCV:
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg19","IDD_CTSEditorPro_CYCL"),strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg20","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg21","IDD_CTSEditorPro_CYCL"));
		break;

	case PS_STEP_PATTERN:
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg22","IDD_CTSEditorPro_CYCL"), strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg23","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg24","IDD_CTSEditorPro_CYCL"));
		break;
	case PS_STEP_USER_MAP:
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg25","IDD_CTSEditorPro_CYCL"), strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg26","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg27","IDD_CTSEditorPro_CYCL"));
		break;
	case PS_STEP_EXT_CAN:
		strMsg.Format(Fun_FindMsg("SetApplyAllBtn_msg28","IDD_CTSEditorPro_CYCL"), strTemp);
		strMsgCan.Format(Fun_FindMsg("SetApplyAllBtn_msg29","IDD_CTSEditorPro_CYCL"));
		strMsgAux.Format(Fun_FindMsg("SetApplyAllBtn_msg30","IDD_CTSEditorPro_CYCL"));
		break;

	case PS_STEP_LOOP:
	case PS_STEP_END:	
	case PS_STEP_ADV_CYCLE:
	default:
		strMsg = "...";
		strMsgCan = "...";
		strMsgAux = "...";

		GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		//ljb 2011221 이재복 //////////
		GetDlgItem(IDC_CAN_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_SAME_CAN_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_AUX_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_SAME_AUX_ALL_STEP_BUTTON)->EnableWindow(FALSE);
		break;
	}
	GetDlgItem(IDC_SAME_ALL_STEP_BUTTON)->SetWindowText(strMsg);


	//ljb 2011221 이재복 //////////
	GetDlgItem(IDC_SAME_CAN_ALL_STEP_BUTTON)->SetWindowText(strMsgCan);
	GetDlgItem(IDC_SAME_AUX_ALL_STEP_BUTTON)->SetWindowText(strMsgAux);

}


void CCTSEditorProView::OnChangeTempLow() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeTempHigh() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bStepDataSaved = FALSE;	
}

//현재 공정에서 주어진 Type의 mode가 진행가능하진 검사 
BOOL CCTSEditorProView::IsExecutableStep(int nTypeComboIndex, WORD type, WORD mode)
{
	//Mask 정보가 없거나 잘못 되었을 경우는 모두 실행 가능하도록 
	if(m_strTypeMaskArray.GetSize() < 0)	return TRUE;
	if(nTypeComboIndex < 0 || nTypeComboIndex >= m_strTypeMaskArray.GetSize())		return TRUE;
	
	CString strMsk;
	strMsk = m_strTypeMaskArray.GetAt(nTypeComboIndex);

	CString strTemp(strMsk);
	strMsk = strTemp.Mid(strTemp.Find('-')+1);

	//Mask가 지정되어 있지 않으면 모두 실행 가능 
	if(strMsk.IsEmpty())		return TRUE;
	
	strMsk.MakeUpper();
	//공정 Type(A, P, F, H, F, S, G)과 
	//시행가능한 Step Type (CODERIZ) C: Charge, D: Discharge, Z: AC Impedance, R: DC Impedance, O: OCV, I:Rest, E:End, S:Simulation ) NULL이면 모든 Step type 수행가능
	switch(type)
	{
	case PS_STEP_CHARGE:
		if(strMsk.Find('C') < 0)		return FALSE;
		break;

	case PS_STEP_DISCHARGE:
		if(strMsk.Find('D') < 0)		return FALSE;
		break;

	case PS_STEP_OCV:
		if(strMsk.Find('O') < 0)		return FALSE;
		break;

	case PS_STEP_IMPEDANCE:
		if(mode == PS_MODE_ACIMP)
		{
			if(strMsk.Find('Z') < 0)	return FALSE;
		}
		else if(mode == PS_MODE_DCIMP)
		{
			if(strMsk.Find('R') < 0)	return FALSE;
		}
		break;

	case PS_STEP_REST:
		if(strMsk.Find('I') < 0)		return FALSE;
		break;

	case PS_STEP_PATTERN:
		if(strMsk.Find('S') < 0)		return FALSE;
		break;
		//ljb 2011318 이재복 ////////// s
	case PS_STEP_EXT_CAN:
		if(strMsk.Find('E') < 0)		return FALSE;
		break;
		//ljb 2011318 이재복 ////////// e	
	//End는 무조건 가능 
	case PS_STEP_END:		
	default:	
		break;
	}

	return TRUE;
}

//주어진 시험에 가능한 Step만 포함되어 있는지 검사 
BOOL CCTSEditorProView::CheckExecutableStep(int nTypeComboIndex, long lTestID)
{
	//현재 Loading되어 있으면 Loading된 Step과 검사하고 
	if(m_lLoadedTestID == lTestID)
	{
		CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
		int nTotStepNum = pDoc->GetTotalStepNum();
		for(int i = 0; i<nTotStepNum; i++)
		{
			STEP *pStep = (STEP *)pDoc->GetStepData(i+1);		
			if(IsExecutableStep(nTypeComboIndex, pStep->chType, pStep->chMode) == FALSE)	
			{
				return FALSE;
			}
		}
	}
	//다를 경우는 DataBase에서 Loading하여 검사한다.
	else
	{
		CDaoDatabase  db;
		db.Open(GetDataBaseName());

		CString strSQL;
		CStepRecordSet rs;
		rs.m_strFilter.Format("[TestID] = %d", lTestID);
		rs.m_strSort = "[StepNo]";
		rs.Open();
		while(!rs.IsEOF())
		{
			if(IsExecutableStep(nTypeComboIndex, rs.m_StepType, rs.m_StepMode) == FALSE)	
			{
				rs.Close();
				return FALSE;
			}
			rs.MoveNext();
		}
		rs.Close();
	}
	return TRUE;
}

void CCTSEditorProView::NewModel()
{
	// TODO: Add your control notification handler code here
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}

	//Dialog 입력 받도록 수정 /////////////////
	CModelNameDlg	dlg;
	dlg.m_strUserID = GetDocument()->m_LoginData.szLoginID;
	if(IDOK != dlg.DoModal())
	{
		return;
	}

	//중복된 이름 사용 못하도록 한다.
	for(int i =0; i<m_pWndCurModelGrid->GetRowCount(); i++)
	{
		if( dlg.GetName() == m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME))
		{
			CString strtemp;
			strtemp.Format(Fun_FindMsg("NewModel_msg1","IDD_CTSEditorPro_CYCL"), dlg.GetName());
			AfxMessageBox(strtemp);
			return;
		}
	}
	//////////////////////////////////////////

	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount()+1;

	m_pWndCurModelGrid->InsertRows(nRow, 1);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_NEW);
	m_pWndCurModelGrid->SetCurrentCell(nRow, 1);

	//dialog 입력 닫아서 자동으로 저장하도록 수정 /////
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), dlg.GetName());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), dlg.GetDescript());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_CREATOR), dlg.m_strUserID);
	//DataBase에 모델 추가 
	OnModelSave();
	////////////////////////////////////////////////////
	
//	GetDlgItem(IDC_MODEL_COPY_BUTTON)->EnableWindow(FALSE);
}

void CCTSEditorProView::ModelDelete()
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}

	CString		strTemp;
	CBatteryModelRecordSet	recordSet;
	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	if(nRow <1)		return;

	CRowColArray	awRows;
	m_pWndCurModelGrid->GetSelectedRows(awRows);
	int nSelSize = awRows.GetSize();
	if( nSelSize <= 0)	return;

	if(nSelSize == 1)
	{
		//strTemp.Format("[%s]을 삭제 하시겠습니까?", m_pWndCurModelGrid->GetValueRowCol(awRows[0], COL_MODEL_NAME));
		strTemp.Format(Fun_FindMsg("CTSEditorProView_ModelDelete_msg1","IDD_CTSEditorPro_CYCL"), m_pWndCurModelGrid->GetValueRowCol(awRows[0], COL_MODEL_NAME));//&&
	}
	else
	{
		//strTemp.Format("선택한 %d개의 공정목록 정보를 삭제 하시겠습니까?", nSelSize);
		strTemp.Format(Fun_FindMsg("CTSEditorProView_ModelDelete_msg2","IDD_CTSEditorPro_CYCL"), nSelSize);//&&
	}

	//if(IDNO == MessageBox(strTemp, "목록 삭제", MB_YESNO|MB_ICONQUESTION))	
	if(IDNO == MessageBox(strTemp, Fun_FindMsg("CTSEditorProView_ModelDelete_msg3","IDD_CTSEditorPro_CYCL"), MB_YESNO|MB_ICONQUESTION))//&&
		return;
	
	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	LONG lDeleteModelID;
	for (int i = nSelSize-1; i >=0 ; i--)
	{
		strTemp = m_pWndCurModelGrid->GetValueRowCol(awRows[i], COL_MODEL_PRIMARY_KEY);
		if(!strTemp.IsEmpty())
		{
			strTemp = "ModelID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				lDeleteModelID = recordSet.m_ModelID;

				// Simulation 파일이 존재하면 삭제한다.///////////////////////////
				DeleteSimulationFile(lDeleteModelID);
				//////////////////////////////////////////////////////////////////////////
				recordSet.Delete();
				
				if(m_lLoadedBatteryModelID == lDeleteModelID)
				{
					RequeryTestList(m_lLoadedBatteryModelID);
					ReLoadStepData(m_lLoadedTestID);
					ClearStepState();
				}
			}
		}
		m_pWndCurModelGrid->RemoveRows(awRows[i], awRows[i]);
	}

	//순서 저장 2002/10/02
	for( int i = 0; i<m_pWndCurModelGrid->GetRowCount(); i++)
	{
		strTemp = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY);		//Get TestID
		if(!strTemp.IsEmpty())										//Delete Test Condition Form DB	
		{
			strTemp = "ModelID = " + strTemp;
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))
			{
				recordSet.Edit();
				recordSet.m_No = atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NO));
				recordSet.Update();
			}
		}
	}

	recordSet.Close();
	RequeryBatteryModel();
	
	GetDlgItem(IDC_MODEL_COPY_BUTTON)->EnableWindow(TRUE);
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
}

void CCTSEditorProView::ModelEdit()
{
	ROWCOL nRow, nCol;

	if(m_pWndCurModelGrid->GetCurrentCell(nRow, nCol) == FALSE)
	{
		return;
	}
	if(nRow < 1)	
	{
		return;
	}

	CModelNameDlg	dlg;
	dlg.m_strName = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_NAME);
	dlg.m_strDescript = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_DESCRIPTION);
	dlg.m_strUserID = m_pWndCurModelGrid->GetValueRowCol(nRow, COL_MODEL_CREATOR);

	if(IDOK != dlg.DoModal())
	{
		return;
	}

	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), dlg.GetName());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), dlg.GetDescript());
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_CREATOR), dlg.m_strUserID);
	m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_EDIT);

	OnModelSave();
}

void CCTSEditorProView::OnTestEdit() 
{
	// TODO: Add your control notification handler code here
	ROWCOL nRow, nCol;

	if(m_wndTestListGrid.GetCurrentCell(nRow, nCol) == FALSE)
	{
		return;
	}
	if(nRow < 1)	
	{
		return;
	}


	CTestNameDlg	dlg(m_lDisplayModelID);
	dlg.m_strName = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_NAME);
	dlg.m_strDecription = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_DESCRIPTION);
	dlg.m_strCreator = m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_CREATOR);
	dlg.m_createdDate.ParseDateTime(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_EDIT_TIME));
	dlg.m_lTestTypeID = m_pTestTypeCombo->GetItemData(atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PROC_TYPE)));
	dlg.m_bContinueCellCode = atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_RESET_PROC));

	long nBefor = dlg.m_lTestTypeID;

	if(IDOK != dlg.DoModal())
	{
		return;
	}

	//Type이 바뀌었으면 
	if(nBefor != dlg.m_lTestTypeID)
	{
		//Step을 검사하여 시행 불가능한 Step이 포함되어 있으면 경고 표시
		if(CheckExecutableStep(GetTestIndexFromType(dlg.m_lTestTypeID), atol(m_wndTestListGrid.GetValueRowCol(nRow, COL_TEST_PRIMARY_KEY)))	 == FALSE)
		{
			CString strTemp;
			//strTemp.Format("수정된 공정[%s]에서 실시할 수 없는 Step을 포함하고 있습니다.\n수정된 공정에서 시행할 수 없는 Step은 삭제되거나 수정되어 합니다.\n\n공정을 수정 하시겠습니까?",
			strTemp.Format(Fun_FindMsg("CTSEditorProView_OnTestEdit_msg1","IDD_CTSEditorPro_CYCL"),//&&
				dlg.m_strName);
			if(MessageBox(strTemp, "Error", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
		}
	}

	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_PROC_TYPE), (long)GetTestIndexFromType(dlg.m_lTestTypeID));
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_NAME), dlg.m_strName);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_DESCRIPTION), dlg.m_strDecription);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_CREATOR), dlg.m_strCreator);
	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_RESET_PROC), (long)dlg.m_bContinueCellCode);

	m_wndTestListGrid.SetValueRange(CGXRange(nRow, COL_TEST_EDIT_STATE), CS_EDIT);

	OnTestSave();	
}

void CCTSEditorProView::ModelCopy()
{
	CRowColArray	awRows;
	m_pWndCurModelGrid->GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();

	if(nSelNo <= 0)
	{
		MessageBox(Fun_FindMsg("ModelCopy_msg1","IDD_CTSEditorPro_CYCL"), Fun_FindMsg("ModelCopy_msg2","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
		return;
	}

	for (int i = 0; i<nSelNo; i++)
	{
		if(awRows[i] > 0)
		{
			if(m_pWndCurModelGrid->GetValueRowCol(awRows[i], COL_MODEL_EDIT_STATE) !=  CS_NEW)
			{
				if(CopyModel(awRows[i]) == FALSE)
				{
					CString strMsg;
					strMsg.Format(Fun_FindMsg("ModelCopy_msg3","IDD_CTSEditorPro_CYCL"), m_pWndCurModelGrid->GetValueRowCol(awRows[i], COL_MODEL_NAME));
					MessageBox(strMsg, Fun_FindMsg("ModelCopy_msg2","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
				}
			}
		}
	}
	
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
}

void CCTSEditorProView::ModelSave()
{
	if(GetDocument()->PermissionCheck(PMS_EDITOR_EDIT) ==  FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnTestNew_msg1","IDD_CTSEditorPro_CYCL"));
		return;
	}

	CString		strTemp, strState;
	CBatteryModelRecordSet	recordSet;
	ROWCOL nRow = m_pWndCurModelGrid->GetRowCount();
	if(nRow <1)		return;
	
	int nLastEditedModel =0;

	try
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}

	for(ROWCOL i = 0; i< nRow; i++)
	{
		strState = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_EDIT_STATE);		//Get Model State

//		if(strState == CS_SAVED)	continue;							//Saved Model Name

		strTemp = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME);
		if(strTemp.IsEmpty())	//Model Name is Empty
		{
			strTemp.Format(Fun_FindMsg("ModelSave_msg1","IDD_CTSEditorPro_CYCL"), i+1);
			MessageBox(strTemp, "Battery Model Name Error", MB_OK|MB_ICONSTOP);
//			m_pWndCurModelGrid->SetCurrentCell(i+1, 1);
//			recordSet.Close();
//			return;
			continue;
		}

/*		strTemp = "ModelName = '" + strTemp + "'";
		if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Model Name Duplication
		{
			strTemp = "모델명 "+m_pWndCurModelGrid->GetValueRowCol(i+1, 1);
			strTemp += "이 이미 존재 합니다.";
			MessageBox(strTemp, "Battery Model Name Error", MB_OK|MB_ICONSTOP);
			m_pWndCurModelGrid->SetCurrentCell(i+1, 1);
			recordSet.Close();
			return;
		}
*/	
		if(strState == CS_NEW)
		{
			recordSet.AddNew();
		}
//		else if(strState == CS_EDIT)
		else
		{
			strTemp = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_PRIMARY_KEY);
			if(strTemp.IsEmpty())
			{
				recordSet.Close();
				TRACE("Model Primary Key Empty");
				return;
			}
			strTemp.Format("ModelID = %ld", atol((LPCSTR)(LPCTSTR)strTemp));
			if(recordSet.Find(AFX_DAO_FIRST, strTemp))	//Find Model Name Data
			{
				recordSet.Edit();
			}
			else
			{
				recordSet.Close();
				TRACE("Battery Model Search Error");
				return;
			}
		}
/*		else
		{
			recordSet.Close();
			TRACE("Cell State Error");
			return;
		}
*/		recordSet.m_No = atol(m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NO));
		recordSet.m_ModelName = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_NAME);
		recordSet.m_Description = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_DESCRIPTION);
		recordSet.m_Creator = m_pWndCurModelGrid->GetValueRowCol(i+1, COL_MODEL_CREATOR);

		recordSet.Update();
		nLastEditedModel = i+1;
	}

	int i=0;

#ifdef _USE_MODEL_USER_
	CString strID(GetDocument()->m_LoginData.szLoginID);
	if(GetDocument()->m_bUseLogin && !strID.IsEmpty())
	{
		recordSet.m_strFilter.Format("[Creator] = '%s'", GetDocument()->m_LoginData.szLoginID);
	}
#endif
	recordSet.m_strSort.Format("[No]");
	recordSet.Requery();

	if(!recordSet.IsBOF())
	{
		recordSet.MoveLast();
		i = recordSet.GetRecordCount();
		recordSet.MoveFirst();
	}
	nRow -= i;
	
	if(nRow>0)
	{
		m_pWndCurModelGrid->RemoveRows(i+1, i+nRow);
	}
	else if(nRow<0)
	{
		m_pWndCurModelGrid->InsertRows(i+1, -nRow);
	}
	
	nRow = 0;
	while(!recordSet.IsEOF())
	{	
		nRow++;
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_NAME), recordSet.m_ModelName);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_CREATOR), recordSet.m_Creator);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_DESCRIPTION), recordSet.m_Description);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_PRIMARY_KEY), recordSet.m_ModelID);
		m_pWndCurModelGrid->SetValueRange(CGXRange(nRow, COL_MODEL_EDIT_STATE), CS_SAVED);
		
		if(nRow == nLastEditedModel)				//Last Edited Battery Model is current selected Model
		{
			UpdateDspModel(recordSet.m_ModelName, recordSet.m_ModelID);
			m_pWndCurModelGrid->SetCurrentCell(nRow, 1);
		}
		recordSet.MoveNext();
	}
	recordSet.Close();
	
	GetDlgItem(IDC_MODEL_SAVE)->EnableWindow(FALSE);
	GetDlgItem(IDC_MODEL_COPY_BUTTON)->EnableWindow(TRUE);

	UpdateBatteryCountState(nRow);
	GetDocument()->m_bEditedFlag = TRUE;		//DataBase 변경 Flag (BackUp을 위해)
}

void CCTSEditorProView::OnWorkSTShow() 
{
	BOOL bUsePlacePlate;
	
	bUsePlacePlate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", FALSE);
	// yulee 20181002
	if(m_pWorkSTSignEditDlg == NULL)
	{
		if(bUsePlacePlate)
		{
		m_pWorkSTSignEditDlg = new CWorkSTSignEditDlg();
		m_pWorkSTSignEditDlg->Create(IDD_WORKSTSIGNEDIT_DLG, this);
		m_pWorkSTSignEditDlg->ShowWindow(SW_SHOW);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", TRUE);
		}
		else
		{
		//AfxMessageBox("관리자에게 문의하세요. Registry 설정 Off");
		}
	}
	else
	{
		m_pWorkSTSignEditDlg->DestroyWindow();
		delete m_pWorkSTSignEditDlg;
		m_pWorkSTSignEditDlg = NULL;

		m_pWorkSTSignEditDlg = new CWorkSTSignEditDlg();
		m_pWorkSTSignEditDlg->Create(IDD_WORKSTSIGNEDIT_DLG, this);
		m_pWorkSTSignEditDlg->ShowWindow(SW_SHOW);
	}
	
}

void CCTSEditorProView::OnButtonModelSel() 
{
	// TODO: Add your control notification handler code here
	if(m_bStepDataSaved == FALSE || GetDocument()->IsEdited() == TRUE)
	{
		CString strTemp;
		strTemp = Fun_FindMsg("OnButtonModelSel_msg1","IDD_CTSEditorPro_CYCL");
		int nRtn = MessageBox(strTemp, "SAVE", MB_ICONQUESTION|MB_YESNOCANCEL);
		if(IDYES == nRtn)
		{
			if(StepSave() == FALSE)
			{
				if(MessageBox(Fun_FindMsg("OnButtonModelSel_msg2","IDD_CTSEditorPro_CYCL"), Fun_FindMsg("OnButtonModelSel_msg3","IDD_CTSEditorPro_CYCL"), MB_ICONQUESTION|MB_YESNO) != IDYES)
				{
					return;
				}
			}
		}
		else if(nRtn == IDCANCEL)
		{
			return;
		}
	}

	m_pModelDlg->ShowWindow(SW_SHOW);
	AfxGetMainWnd()->ShowWindow(SW_HIDE);
}

void CCTSEditorProView::OnAdministration() 
{
	// TODO: Add your command handler code here
	CUserAdminDlg dlg;
	dlg.m_pLoginData = &(GetDocument()->m_LoginData);
	dlg.DoModal();
}

void CCTSEditorProView::OnUserSetting() 
{
	// TODO: Add your command handler code here
	CChangeUserInfoDlg *pDlg;
	pDlg = new CChangeUserInfoDlg(&GetDocument()->m_LoginData, &GetDocument()->m_LoginData, this);
	ASSERT(pDlg);
	pDlg->m_bNewUser = FALSE;
	if(IDOK == pDlg->DoModal())
	{
		memcpy(&GetDocument()->m_LoginData, pDlg->GetUserInfo(), sizeof(STR_LOGIN));
	}
	delete pDlg;
	pDlg = NULL;	
}

void CCTSEditorProView::OnUpdateAdministration(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	if(GetDocument()->m_bUseLogin && (GetDocument()->m_LoginData.nPermission & PMS_USER_SETTING_CHANGE))
	if(GetDocument()->m_bUseLogin && strlen(GetDocument()->m_LoginData.szLoginID) > 0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CCTSEditorProView::OnUpdateUserSetting(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(GetDocument()->m_bUseLogin && strlen(GetDocument()->m_LoginData.szLoginID) > 0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
	
}

void CCTSEditorProView::OnModelReg() 
{
	// TODO: Add your command handler code here
	CModelInfoDlg dlg;
	dlg.m_pDoc = GetDocument();
	dlg.DoModal();
}


void CCTSEditorProView::OnUpdateModelReg(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_bUseModelInfo);
}


LRESULT CCTSEditorProView::OnGridBtnClicked(WPARAM wParam, LPARAM lParam)
{
	
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	if(nRow < 1 || nCol != COL_STEP_MODE)				return 0;

	STEP *pStep = (STEP *)GetDocument()->GetStepData(nRow);	
	if(pStep == NULL || (pStep->chType != PS_STEP_PATTERN && pStep->chType != PS_STEP_USER_MAP))	return 0;

	UpdateStepGrid(nRow);
	
	CSelectSimulDataDlg * pDlg;
	CUserMapDlg* pUserMapDlg;
	pDlg =NULL;
	pUserMapDlg =NULL;
	//2014.09.02 UserMap 추가.
	if(pStep->chType == PS_STEP_PATTERN){

		pDlg = new CSelectSimulDataDlg(this);
		pDlg->m_strSelFile = pStep->szSimulFile;
		pDlg->pStep = pStep;
		
		//+2015.9.22 USY Add For PatternCV
		pDlg->m_bParallelMode = GetDocument()->Fun_GetParallelMode();
		//-

		if(pDlg->DoModal() == IDOK)
		{
			//파일명을 YYYYMMDDHHmmSS-파일명.csv			
			sprintf(pStep->szSimulFile	, "%s", pDlg->m_strSelFile);
			pStep->lValueLimitHigh = pDlg->lLimitHigh;
			pStep->lValueLimitLow = pDlg->lLimitLow;
			pStep->chMode = pDlg->m_chMode;
			DisplayStepGrid(nRow);
		}
	}else if(pStep->chType == PS_STEP_USER_MAP){
	
		pUserMapDlg = new CUserMapDlg(this);
		pUserMapDlg->m_strUserMapFile = pStep->szUserMapFile;
		pUserMapDlg->pStep = pStep;
						
		if(pUserMapDlg->DoModal() == IDOK)
		{
			sprintf(pStep->szUserMapFile, "%s", pUserMapDlg->m_strUserMapFile);
			pStep->chMode = pUserMapDlg->m_chMode;
			DisplayStepGrid(nRow);	
		}

	}
	if(pUserMapDlg != NULL) delete pUserMapDlg;
	if(pDlg != NULL) delete pDlg;
	return 1;
}

//새로운 Simulation 파일을 생성한다.
CString CCTSEditorProView::NewSimulationFile(CString strFile)
{
	CString strSelFile;
	CString strDataTitle;
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));

	CTime ct = CTime::GetCurrentTime();

	int nStart	= strFile.Find('-');
	int nEnd	= strFile.ReverseFind('.');
	
	if(nStart >= 0 && nEnd >= 0 && nStart < nEnd)
	{
		strDataTitle = strFile.Mid(nStart+1, nEnd-nStart-1);
	}	


	strSelFile.Format("%s\\%s-%s.csv", strDir, ct.Format("%Y%m%d%H%M%S"),strDataTitle);	

	//파일이 중복되는 것을 방지하기 위해//////////////////////////////////////////
	int nFile = 2;
	while(CopyFile(strFile, strSelFile, TRUE) == FALSE)
	{
		//AfxMessageBox(strSelFile+" Data를 지정할 수 없습니다.(중복 파일)");
		//return "";
		int nError = GetLastError();
		if(nError == ERROR_FILE_NOT_FOUND)
		{
			AfxMessageBox(strSelFile+Fun_FindMsg("NewSimulationFile_msg1","IDD_CTSEditorPro_CYCL"));
			return strFile;
		}
		CString strFileNum;
		strFileNum.Format("(%d)", nFile++);
		strSelFile.Insert(strSelFile.ReverseFind('.'), strFileNum);
	}
	///////////////////////////////////////////////////////////////////////////////

	return strSelFile;
}

//파일이 존재하면 삭제한다.
void CCTSEditorProView::DeleteSimulationFile(CString strFile)
{
	CFileFind aFind;
	BOOL bFind = aFind.FindFile(strFile);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}
}

//Simulation 파일이 존재하면 해당 경로를 리턴한다.
void CCTSEditorProView::DeleteSimulationFileTestData(LONG lTestID)
{
	CDaoDatabase  db;
	db.Open(GetDataBaseName());
	CString  strReturn;

	CString strSQL;
	CStepRecordSet rs;
	rs.m_strFilter.Format("[TestID] = %d", lTestID);
	rs.m_strSort = "[StepNo]";
	rs.Open();
	while(!rs.IsEOF())
	{		
		if(rs.m_strSimulFile != "")
		{
			strReturn = rs.m_strSimulFile;
			DeleteSimulationFile(strReturn);
		}
		rs.MoveNext();
	}
	rs.Close();
}

//ModelID로 TestID를 얻는다.
void CCTSEditorProView::DeleteSimulationFile(LONG lModelID)
{
	CTestListRecordSet	recordSet;
	CString strTemp;
	
	try									//DataBase Open
	{
		recordSet.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return;
	}

	strTemp.Format("ModelID = %d", lModelID);
	recordSet.Find(AFX_DAO_FIRST, strTemp);
	while(!recordSet.IsEOF())
	{
		DeleteSimulationFileTestData(recordSet.m_TestID);
		recordSet.MoveNext();
	}

	recordSet.Close();
}

void CCTSEditorProView::OnChangeReportTimeMili()
{
}

/*
---------------------------------------------------------
---------
-- Filename		:	CTSEditorProView.cpp 
-- Description	:
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 2014.09.22	이민규		기록조건 msec 반올림 형식으로 변경.
----------------------------------------------------------
*/
void CCTSEditorProView::OnKillFocusReportTimeMili()
{
		
	STEP *pStep;
	pStep = (STEP *)GetDocument()->GetStepData(m_nDisplayStep);
	
	ULONG lReportTime = pStep->ulReportTime * 10;

	CString strValue;
	GetDlgItem(IDC_REPORT_TIME_MILI)->GetWindowText(strValue);
	int nValue = atoi(strValue);
	
	if(lReportTime != nValue){
		m_bStepDataSaved = FALSE;
	}
	
	int nDivision = 5;
	
	int nTemp = nValue % nDivision;

	/*	
	if(nTemp > 5)
		nValue += nDivision - nTemp;		
	else
		nValue -= nTemp;
	*/

	//2014.09.22 10단위 반올림으로 변경.
	nValue = nValue + nDivision;
	nValue = nValue / 10;
	nValue = (nValue * 10);
	
	if(nValue >= 1000)
		nValue = 0;
	if(nValue < 0)
		nValue = 0;


	strValue.Format("%d", nValue);
	GetDlgItem(IDC_REPORT_TIME_MILI)->SetWindowText(strValue);

	nTemp = 0;
}


//DEL void CCTSEditorProView::InitChamberGrid()
//DEL {
//DEL 	CRect rect;
//DEL 	m_wndChamberGrid.SubclassDlgItem(IDC_CHAMBER_GRID, this);
//DEL 	m_wndChamberGrid.Initialize();
//DEL 
//DEL 	// Sample setup for the grid
//DEL 	m_wndChamberGrid.GetParam()->EnableUndo(FALSE);
//DEL 	m_wndChamberGrid.SetRowCount(2);
//DEL 	m_wndChamberGrid.SetColCount(2);
//DEL 	m_wndChamberGrid.GetClientRect(&rect);
//DEL 	m_wndChamberGrid.SetColWidth(0, 0, 60);
//DEL 	m_wndChamberGrid.SetColWidth(1, 1, 65);
//DEL //	m_wndChamberGrid.SetColWidth(2, 2, 0);
//DEL 	m_wndChamberGrid.SetColWidth(2, 2, rect.Width() - 125);
//DEL 	m_wndChamberGrid.SetStyleRange(CGXRange(0,0),	CGXStyle().SetValue("구분"));
//DEL 	m_wndChamberGrid.SetStyleRange(CGXRange(1,0),	CGXStyle().SetValue("온도(℃)"));
//DEL 	m_wndChamberGrid.SetStyleRange(CGXRange(2,0),	CGXStyle().SetValue("습도(％)"));
//DEL 
//DEL 	m_wndChamberGrid.SetStyleRange(CGXRange(0,1),	CGXStyle().SetValue("설정값"));
//DEL 	m_wndChamberGrid.SetStyleRange(CGXRange(0,2),	CGXStyle().SetValue("분당변화율"));
//DEL 
//DEL 	m_wndChamberGrid.GetParam()->EnableUndo(TRUE);
//DEL 
//DEL 	m_wndChamberGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndChamberGrid, IDS_CTRL_REALEDIT));
//DEL 	m_wndChamberGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
//DEL 	m_wndChamberGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
//DEL 	m_wndChamberGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
//DEL 
//DEL 	char str[12], str2[7], str3[5];
//DEL 	
//DEL 	//Grade Value 
//DEL 	sprintf(str, "%%.%dlf", 1);	//소숫점 이하
//DEL 	sprintf(str2, "%d", 3);		//정수부
//DEL 	sprintf(str3, "%d", 1);		//소숫점 이하
//DEL 
//DEL 	m_wndChamberGrid.SetStyleRange(CGXRange().SetCols(1, 2),
//DEL 		CGXStyle()
//DEL 			.SetHorizontalAlignment(DT_RIGHT)
//DEL 			.SetControl(IDS_CTRL_REALEDIT)
//DEL 			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
//DEL 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
//DEL 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
//DEL 	);
//DEL }

//DEL int CCTSEditorProView::UpdateStepChamber(int nStepNum)
//DEL {
//DEL 	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();		//Get Document point
//DEL 
//DEL 	int nTotStepNum = pDoc->GetTotalStepNum();
//DEL 	if(nTotStepNum <= 0)	return -1;
//DEL 	if(nStepNum < 1 || nStepNum > nTotStepNum)		return -1;
//DEL 	
//DEL 	STEP *pStep;
//DEL 	pStep = (STEP *)pDoc->GetStepData(nStepNum);
//DEL 	if(!pStep)	return -1;
//DEL 		
//DEL 	CString strTemp1, strTemp2, strTemp3;
//DEL 	int nTotGrade = 0;
//DEL 	pStep->bGrade =  m_GradeCheck.GetCheck();
//DEL 	for(int i =1; i<=m_wndChamberGrid.GetRowCount(); i++)
//DEL 	{
//DEL 		strTemp1 = m_wndChamberGrid.GetValueRowCol(i, 1);		//설정값
//DEL 		if(strTemp1.IsEmpty())		continue;
//DEL 		strTemp2 = m_wndChamberGrid.GetValueRowCol(i, 2);		//변화율
//DEL 
//DEL 		if(i == 1)
//DEL 		{
//DEL 			pStep->fTref = atof(strTemp1);
//DEL 			pStep->fTrate = atof(strTemp2);
//DEL 		}
//DEL 		else
//DEL 		{
//DEL 			pStep->fHref = atof(strTemp1);
//DEL 			pStep->fHrate = atof(strTemp2);
//DEL 		}
//DEL 	}
//DEL 	return 0;
//DEL }

//DEL int CCTSEditorProView::DisplayStepChamber(STEP *pStep)
//DEL {
//DEL 	if(pStep == NULL)
//DEL 		return -1;
//DEL 	//reset Grid
//DEL 	for ( int n = 1; n <= m_wndChamberGrid.GetRowCount(); n++ )
//DEL 	{
//DEL 		m_wndChamberGrid.SetValueRange(CGXRange(n, 1), "");
//DEL 		m_wndChamberGrid.SetValueRange(CGXRange(n, 2), "");
//DEL 	}
//DEL 
//DEL 	m_wndChamberGrid.SetValueRange(CGXRange(1, 1), pStep->fTref);
//DEL 	m_wndChamberGrid.SetValueRange(CGXRange(1, 2), pStep->fTrate);
//DEL 	m_wndChamberGrid.SetValueRange(CGXRange(2, 1), pStep->fHref);
//DEL 	m_wndChamberGrid.SetValueRange(CGXRange(2, 2), pStep->fHrate);
//DEL 
//DEL 	return 0;
//DEL }

void CCTSEditorProView::OnChangeCapacitanceLow()
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::OnChangeCapacitanceHigh()
{
	m_bStepDataSaved = FALSE;
}

void CCTSEditorProView::InitAccGroup(BOOL bInit)
{
	if (bInit)
	{
		for (int i=0; i<MAX_ACC_GROUP_COUNT ; i++)
		{
			m_nAccGroupLoopInfoCycle[i]=0;
			m_nAccGroupLoopInfoGoto[i]=PS_STEP_NEXT;
		}
		m_ctrlCboAccGroup1.ResetContent();
		m_ctrlCboAccGroup1.AddString("NEXT");
		m_ctrlCboAccGroup1.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboAccGroup2.ResetContent();
		m_ctrlCboAccGroup2.AddString("NEXT");
		m_ctrlCboAccGroup2.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboAccGroup3.ResetContent();
		m_ctrlCboAccGroup3.AddString("NEXT");
		m_ctrlCboAccGroup3.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboAccGroup4.ResetContent();
		m_ctrlCboAccGroup4.AddString("NEXT");
		m_ctrlCboAccGroup4.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboAccGroup5.ResetContent();
		m_ctrlCboAccGroup5.AddString("NEXT");
		m_ctrlCboAccGroup5.SetItemData(0, PS_STEP_NEXT);
	}
	m_ctrlEditAccGroup1 = m_nAccGroupLoopInfoCycle[0];
	m_ctrlEditAccGroup2 = m_nAccGroupLoopInfoCycle[1];
	m_ctrlEditAccGroup3 = m_nAccGroupLoopInfoCycle[2];
	m_ctrlEditAccGroup4 = m_nAccGroupLoopInfoCycle[3];
	m_ctrlEditAccGroup5 = m_nAccGroupLoopInfoCycle[4];

	m_ctrlCboAccGroup1.SetCurSel(0);
	m_ctrlCboAccGroup2.SetCurSel(0);
	m_ctrlCboAccGroup3.SetCurSel(0);
	m_ctrlCboAccGroup4.SetCurSel(0);
	m_ctrlCboAccGroup5.SetCurSel(0);

	m_ctrlCboAccGroup1.EnableWindow(FALSE);
	m_ctrlCboAccGroup2.EnableWindow(FALSE);
	m_ctrlCboAccGroup3.EnableWindow(FALSE);
	m_ctrlCboAccGroup4.EnableWindow(FALSE);
	m_ctrlCboAccGroup5.EnableWindow(FALSE);

}

void CCTSEditorProView::InitParallelCbo()
{
	int nMaxParallelNum;

#ifdef _PARALLEL_FORCE 전처리기 주석처리
	nMaxParallelNum = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Parallel Max Number",2);  // yulee 20180719 add
#else
	nMaxParallelNum = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Parallel Max Number",2);  // yulee 20180719 add
#endif
	m_CboParallelNum.ResetContent();
	for(int i=2; i<=nMaxParallelNum; i++)
	{
		CString strTmp;
		strTmp.Format(_T("%d"), i);
		m_CboParallelNum.AddString(_T(strTmp));
	}
	m_CboParallelNum.SetItemData(0, 0);
	m_CboParallelNum.SetCurSel(0);
}
void CCTSEditorProView::Fun_UpdateAccGroupList()
{
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	CString strTemp;
	int iTemp1=0,iTemp2=0,iTemp3=0,iTemp4=0,iTemp5=0;
	BOOL bNotChange1(TRUE),bNotChange2(TRUE),bNotChange3(TRUE),bNotChange4(TRUE),bNotChange5(TRUE);
	
// 	if(m_ctrlCboAccGroup1.GetCurSel() != LB_ERR) iTemp1 = m_ctrlCboAccGroup1.GetItemData(m_ctrlCboAccGroup1.GetCurSel());
// 	if(m_ctrlCboAccGroup2.GetCurSel() != LB_ERR) iTemp2 = m_ctrlCboAccGroup2.GetItemData(m_ctrlCboAccGroup2.GetCurSel());
// 	if(m_ctrlCboAccGroup3.GetCurSel() != LB_ERR) iTemp3 = m_ctrlCboAccGroup3.GetItemData(m_ctrlCboAccGroup3.GetCurSel());
// 	if(m_ctrlCboAccGroup4.GetCurSel() != LB_ERR) iTemp4 = m_ctrlCboAccGroup4.GetItemData(m_ctrlCboAccGroup4.GetCurSel());
// 	if(m_ctrlCboAccGroup5.GetCurSel() != LB_ERR) iTemp5 = m_ctrlCboAccGroup5.GetItemData(m_ctrlCboAccGroup5.GetCurSel());
	
	
	m_ctrlCboAccGroup1.ResetContent();
	m_ctrlCboAccGroup2.ResetContent();
	m_ctrlCboAccGroup3.ResetContent();
	m_ctrlCboAccGroup4.ResetContent();
	m_ctrlCboAccGroup5.ResetContent();
	
	//선택 가능한 List를 추가 한다.
	m_ctrlCboAccGroup1.AddString("NEXT");
	m_ctrlCboAccGroup2.AddString("NEXT");
	m_ctrlCboAccGroup3.AddString("NEXT");
	m_ctrlCboAccGroup4.AddString("NEXT");
	m_ctrlCboAccGroup5.AddString("NEXT");
	m_ctrlCboAccGroup1.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboAccGroup2.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboAccGroup3.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboAccGroup4.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboAccGroup5.SetItemData(0, PS_STEP_NEXT);
	int nCount=1;
	for(int a=1; a<=m_wndStepGrid.GetRowCount(); a++)
	//for(int a=1; a<= pDoc->GetTotalStepNum(); a++)
	{
		STEP *pStep = (STEP *)pDoc->GetStepData(a);
		switch (pStep->chType)
		{
		case PS_STEP_ADV_CYCLE:
		case PS_STEP_END:
			strTemp.Format("Step %d", a);
			m_ctrlCboAccGroup1.AddString(strTemp);
			m_ctrlCboAccGroup2.AddString(strTemp);
			m_ctrlCboAccGroup3.AddString(strTemp);
			m_ctrlCboAccGroup4.AddString(strTemp);
			m_ctrlCboAccGroup5.AddString(strTemp);
			m_ctrlCboAccGroup1.SetItemData(nCount, a);
			m_ctrlCboAccGroup2.SetItemData(nCount, a);
			m_ctrlCboAccGroup3.SetItemData(nCount, a);
			m_ctrlCboAccGroup4.SetItemData(nCount, a);
			m_ctrlCboAccGroup5.SetItemData(nCount, a);	
// 			if (iTemp1 == a) bNotChange1 = FALSE;
// 			if (iTemp2 == a) bNotChange2 = FALSE;
// 			if (iTemp3 == a) bNotChange3 = FALSE;
// 			if (iTemp4 == a) bNotChange4 = FALSE;
// 			if (iTemp5 == a) bNotChange5 = FALSE;
			nCount++;
			break;
		}
	}
	if (m_nAccGroupLoopInfoGoto[0] == PS_STEP_NEXT)	m_ctrlCboAccGroup1.SetCurSel(0);
	if (m_nAccGroupLoopInfoGoto[1] == PS_STEP_NEXT)	m_ctrlCboAccGroup2.SetCurSel(0);
	if (m_nAccGroupLoopInfoGoto[2] == PS_STEP_NEXT)	m_ctrlCboAccGroup3.SetCurSel(0);
	if (m_nAccGroupLoopInfoGoto[3] == PS_STEP_NEXT)	m_ctrlCboAccGroup4.SetCurSel(0);
	if (m_nAccGroupLoopInfoGoto[4] == PS_STEP_NEXT)	m_ctrlCboAccGroup5.SetCurSel(0);
	//AccGroup  누적 Goto 설정 값으로
	int iCnt = m_ctrlCboAccGroup1.GetCount();
	for (int i=0; i< iCnt ; i++)
	{
		if( m_nAccGroupLoopInfoGoto[0] == m_ctrlCboAccGroup1.GetItemData(i))
		{ 
			m_ctrlCboAccGroup1.SetCurSel(i); 
			if (m_nAccGroupLoopInfoCycle[0] > 0 ) m_ctrlCboAccGroup1.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[1] == m_ctrlCboAccGroup2.GetItemData(i))
		{
			m_ctrlCboAccGroup2.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[1] > 0 ) m_ctrlCboAccGroup2.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[2] == m_ctrlCboAccGroup3.GetItemData(i))
		{
			m_ctrlCboAccGroup3.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[2] > 0 ) m_ctrlCboAccGroup3.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[3] == m_ctrlCboAccGroup4.GetItemData(i))
		{
			m_ctrlCboAccGroup4.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[3] > 0 ) m_ctrlCboAccGroup4.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[4] == m_ctrlCboAccGroup5.GetItemData(i))
		{
			m_ctrlCboAccGroup5.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[4] > 0 ) m_ctrlCboAccGroup5.EnableWindow(TRUE); 
		}
	}
// 	if (bNotChange1) m_ctrlCboAccGroup1.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboAccGroup2.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboAccGroup3.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboAccGroup4.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboAccGroup5.SetCurSel(0);
}

void CCTSEditorProView::Fun_UpdateDisplayAccGroup()
{
	UpdateData();
	int iTemp1=0,iTemp2=0,iTemp3=0,iTemp4=0,iTemp5=0;
	int iCnt=0;
	//AccGroup 누적 횟수 
	m_ctrlEditAccGroup1 = m_nAccGroupLoopInfoCycle[0];
	m_ctrlEditAccGroup2 = m_nAccGroupLoopInfoCycle[1];
	m_ctrlEditAccGroup3 = m_nAccGroupLoopInfoCycle[2];
	m_ctrlEditAccGroup4 = m_nAccGroupLoopInfoCycle[3];
	m_ctrlEditAccGroup5 = m_nAccGroupLoopInfoCycle[4];
	//AccGroup  누적 Goto 초기화
	m_ctrlCboAccGroup1.SetCurSel(0);
	m_ctrlCboAccGroup2.SetCurSel(0);
	m_ctrlCboAccGroup3.SetCurSel(0);
	m_ctrlCboAccGroup4.SetCurSel(0);
	m_ctrlCboAccGroup5.SetCurSel(0);

	//AccGroup  누적 Goto 설정 값으로
	iCnt = m_ctrlCboAccGroup1.GetCount();
	for (int i=0; i< iCnt ; i++)
	{
		if( m_nAccGroupLoopInfoGoto[0] == m_ctrlCboAccGroup1.GetItemData(i))
		{ 
			m_ctrlCboAccGroup1.SetCurSel(i); 
			if (m_nAccGroupLoopInfoCycle[0] > 0 ) m_ctrlCboAccGroup1.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[1] == m_ctrlCboAccGroup2.GetItemData(i))
		{
			m_ctrlCboAccGroup2.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[1] > 0 ) m_ctrlCboAccGroup2.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[2] == m_ctrlCboAccGroup3.GetItemData(i))
		{
			m_ctrlCboAccGroup3.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[2] > 0 ) m_ctrlCboAccGroup3.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[3] == m_ctrlCboAccGroup4.GetItemData(i))
		{
			m_ctrlCboAccGroup4.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[3] > 0 ) m_ctrlCboAccGroup4.EnableWindow(TRUE); 
		}
		if( m_nAccGroupLoopInfoGoto[4] == m_ctrlCboAccGroup5.GetItemData(i))
		{
			m_ctrlCboAccGroup5.SetCurSel(i);
			if (m_nAccGroupLoopInfoCycle[4] > 0 ) m_ctrlCboAccGroup5.EnableWindow(TRUE); 
		}
	}
	UpdateData(FALSE);
}

void CCTSEditorProView::OnChangeEditAccGroupCount1() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboAccGroup1.GetCurSel() != LB_ERR) 
		m_nAccGroupLoopInfoCycle[0] = m_ctrlEditAccGroup1;
	if (m_ctrlEditAccGroup1 == 0) m_ctrlCboAccGroup1.EnableWindow(FALSE);
	else m_ctrlCboAccGroup1.EnableWindow(TRUE);
	UpdateData(FALSE);
}

void CCTSEditorProView::OnChangeEditAccGroupCount2() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboAccGroup2.GetCurSel() != LB_ERR) 
		m_nAccGroupLoopInfoCycle[1] = m_ctrlEditAccGroup2;
	if (m_ctrlEditAccGroup2 == 0) m_ctrlCboAccGroup2.EnableWindow(FALSE);
	else m_ctrlCboAccGroup2.EnableWindow(TRUE);
	UpdateData(FALSE);
}

void CCTSEditorProView::OnChangeEditAccGroupCount3() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboAccGroup3.GetCurSel() != LB_ERR) 
		m_nAccGroupLoopInfoCycle[2] = m_ctrlEditAccGroup3;
	if (m_ctrlEditAccGroup3 == 0) m_ctrlCboAccGroup3.EnableWindow(FALSE);
	else m_ctrlCboAccGroup3.EnableWindow(TRUE);
	UpdateData(FALSE);
}

void CCTSEditorProView::OnChangeEditAccGroupCount4() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboAccGroup4.GetCurSel() != LB_ERR) 
		m_nAccGroupLoopInfoCycle[3] = m_ctrlEditAccGroup4;
	if (m_ctrlEditAccGroup4 == 0) m_ctrlCboAccGroup4.EnableWindow(FALSE);
	else m_ctrlCboAccGroup4.EnableWindow(TRUE);
	UpdateData(FALSE);
}

void CCTSEditorProView::OnChangeEditAccGroupCount5() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboAccGroup5.GetCurSel() != LB_ERR) 
		m_nAccGroupLoopInfoCycle[4] = m_ctrlEditAccGroup5;
	if (m_ctrlEditAccGroup5 == 0) m_ctrlCboAccGroup5.EnableWindow(FALSE);
	else m_ctrlCboAccGroup5.EnableWindow(TRUE);
	UpdateData(FALSE);
}

void CCTSEditorProView::OnSelchangeCboAccGroup1() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nAccGroupLoopInfoGoto[0] = m_ctrlCboAccGroup1.GetItemData(m_ctrlCboAccGroup1.GetCurSel());
	UpdateData(FALSE);
}

void CCTSEditorProView::OnSelchangeCboAccGroup2() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nAccGroupLoopInfoGoto[1]=m_ctrlCboAccGroup2.GetItemData(m_ctrlCboAccGroup2.GetCurSel());
	UpdateData(FALSE);
}

void CCTSEditorProView::OnSelchangeCboAccGroup3() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nAccGroupLoopInfoGoto[2]=m_ctrlCboAccGroup3.GetItemData(m_ctrlCboAccGroup3.GetCurSel());
	UpdateData(FALSE);
}

void CCTSEditorProView::OnSelchangeCboAccGroup4() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nAccGroupLoopInfoGoto[3]=m_ctrlCboAccGroup4.GetItemData(m_ctrlCboAccGroup4.GetCurSel());
	UpdateData(FALSE);
}

void CCTSEditorProView::OnSelchangeCboAccGroup5() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nAccGroupLoopInfoGoto[4]=m_ctrlCboAccGroup5.GetItemData(m_ctrlCboAccGroup5.GetCurSel());
	UpdateData(FALSE);
	
}

void CCTSEditorProView::Fun_UpdateMultiGroupList()
{
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
	CString strTemp;
// 	int iTemp1=0,iTemp2=0,iTemp3=0,iTemp4=0,iTemp5=0;
// 	BOOL bNotChange1(TRUE),bNotChange2(TRUE),bNotChange3(TRUE),bNotChange4(TRUE),bNotChange5(TRUE);
	
// 	if(m_ctrlCboMultiGroup1.GetCurSel() != LB_ERR) iTemp1 = m_ctrlCboMultiGroup1.GetItemData(m_ctrlCboMultiGroup1.GetCurSel());
// 	if(m_ctrlCboMultiGroup2.GetCurSel() != LB_ERR) iTemp2 = m_ctrlCboMultiGroup2.GetItemData(m_ctrlCboMultiGroup2.GetCurSel());
// 	if(m_ctrlCboMultiGroup3.GetCurSel() != LB_ERR) iTemp3 = m_ctrlCboMultiGroup3.GetItemData(m_ctrlCboMultiGroup3.GetCurSel());
// 	if(m_ctrlCboMultiGroup4.GetCurSel() != LB_ERR) iTemp4 = m_ctrlCboMultiGroup4.GetItemData(m_ctrlCboMultiGroup4.GetCurSel());
// 	if(m_ctrlCboMultiGroup5.GetCurSel() != LB_ERR) iTemp5 = m_ctrlCboMultiGroup5.GetItemData(m_ctrlCboMultiGroup5.GetCurSel());
	
	
	m_ctrlCboMultiGroup1.ResetContent();
	m_ctrlCboMultiGroup2.ResetContent();
	m_ctrlCboMultiGroup3.ResetContent();
	m_ctrlCboMultiGroup4.ResetContent();
	m_ctrlCboMultiGroup5.ResetContent();
	
	//선택 가능한 List를 추가 한다.
	m_ctrlCboMultiGroup1.AddString("NEXT");
	m_ctrlCboMultiGroup2.AddString("NEXT");
	m_ctrlCboMultiGroup3.AddString("NEXT");
	m_ctrlCboMultiGroup4.AddString("NEXT");
	m_ctrlCboMultiGroup5.AddString("NEXT");
	m_ctrlCboMultiGroup1.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboMultiGroup2.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboMultiGroup3.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboMultiGroup4.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboMultiGroup5.SetItemData(0, PS_STEP_NEXT);
	int nCount=1;
	for(int a=1; a<=m_wndStepGrid.GetRowCount(); a++)
	{
		STEP *pStep = (STEP *)pDoc->GetStepData(a);
		if(pStep == NULL)
			continue; //ksj 20210709: null pointer check 추가

		switch (pStep->chType)
		{
		case PS_STEP_ADV_CYCLE:
		case PS_STEP_END:
			strTemp.Format("Step %d", a);
			m_ctrlCboMultiGroup1.AddString(strTemp);
			m_ctrlCboMultiGroup2.AddString(strTemp);
			m_ctrlCboMultiGroup3.AddString(strTemp);
			m_ctrlCboMultiGroup4.AddString(strTemp);
			m_ctrlCboMultiGroup5.AddString(strTemp);
			m_ctrlCboMultiGroup1.SetItemData(nCount, a);
			m_ctrlCboMultiGroup2.SetItemData(nCount, a);
			m_ctrlCboMultiGroup3.SetItemData(nCount, a);
			m_ctrlCboMultiGroup4.SetItemData(nCount, a);
			m_ctrlCboMultiGroup5.SetItemData(nCount, a);
// 			if (iTemp1 == a) bNotChange1 = FALSE;
// 			if (iTemp2 == a) bNotChange2 = FALSE;
// 			if (iTemp3 == a) bNotChange3 = FALSE;
// 			if (iTemp4 == a) bNotChange4 = FALSE;
// 			if (iTemp5 == a) bNotChange5 = FALSE;
			nCount++;
			break;
		}
	}
	if (m_nMultiGroupLoopInfoGoto[0] == PS_STEP_NEXT)	m_ctrlCboMultiGroup1.SetCurSel(0);
	if (m_nMultiGroupLoopInfoGoto[1] == PS_STEP_NEXT)	m_ctrlCboMultiGroup2.SetCurSel(0);
	if (m_nMultiGroupLoopInfoGoto[2] == PS_STEP_NEXT)	m_ctrlCboMultiGroup3.SetCurSel(0);
	if (m_nMultiGroupLoopInfoGoto[3] == PS_STEP_NEXT)	m_ctrlCboMultiGroup4.SetCurSel(0);
	if (m_nMultiGroupLoopInfoGoto[4] == PS_STEP_NEXT)	m_ctrlCboMultiGroup5.SetCurSel(0);
	//AccGroup  누적 Goto 설정 값으로
	int iCnt = m_ctrlCboMultiGroup1.GetCount();
	for (int i=0; i< iCnt ; i++)
	{
		if( m_nMultiGroupLoopInfoGoto[0] == m_ctrlCboMultiGroup1.GetItemData(i))
		{ 
			m_ctrlCboMultiGroup1.SetCurSel(i); 
			if (m_nMultiGroupLoopInfoCycle[0] > 0 ) m_ctrlCboMultiGroup1.EnableWindow(TRUE); 
		}
		if( m_nMultiGroupLoopInfoGoto[1] == m_ctrlCboMultiGroup2.GetItemData(i))
		{
			m_ctrlCboMultiGroup2.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[1] > 0 ) m_ctrlCboMultiGroup2.EnableWindow(TRUE); 
		}
		if( m_nMultiGroupLoopInfoGoto[2] == m_ctrlCboMultiGroup3.GetItemData(i))
		{
			m_ctrlCboMultiGroup3.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[2] > 0 ) m_ctrlCboMultiGroup3.EnableWindow(TRUE); 
		}
		if( m_nMultiGroupLoopInfoGoto[3] == m_ctrlCboMultiGroup4.GetItemData(i))
		{
			m_ctrlCboMultiGroup4.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[3] > 0 ) m_ctrlCboMultiGroup4.EnableWindow(TRUE); 
		}
		if( m_nMultiGroupLoopInfoGoto[4] == m_ctrlCboMultiGroup5.GetItemData(i))
		{
			m_ctrlCboMultiGroup5.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[4] > 0 ) m_ctrlCboMultiGroup5.EnableWindow(TRUE); 
		}
	}
// 	if (bNotChange1) m_ctrlCboMultiGroup1.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboMultiGroup2.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboMultiGroup3.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboMultiGroup4.SetCurSel(0);
// 	if (bNotChange1) m_ctrlCboMultiGroup5.SetCurSel(0);
}

void CCTSEditorProView::Fun_UpdateDisplayMultiGroup()
{
	UpdateData();
	int iTemp1=0,iTemp2=0,iTemp3=0,iTemp4=0,iTemp5=0;
	int iCnt=0;
	//MultiGroup 누적 횟수 
	m_ctrlEditMultiGroup1 = m_nMultiGroupLoopInfoCycle[0];
	m_ctrlEditMultiGroup2 = m_nMultiGroupLoopInfoCycle[1];
	m_ctrlEditMultiGroup3 = m_nMultiGroupLoopInfoCycle[2];
	m_ctrlEditMultiGroup4 = m_nMultiGroupLoopInfoCycle[3];
	m_ctrlEditMultiGroup5 = m_nMultiGroupLoopInfoCycle[4];
	//MultiGroup  누적 Goto 초기화
	m_ctrlCboMultiGroup1.SetCurSel(0);
	m_ctrlCboMultiGroup2.SetCurSel(0);
	m_ctrlCboMultiGroup3.SetCurSel(0);
	m_ctrlCboMultiGroup4.SetCurSel(0);
	m_ctrlCboMultiGroup5.SetCurSel(0);
	
	//MultiGroup  누적 Goto 설정 값으로
	iCnt = m_ctrlCboMultiGroup1.GetCount();
	for (int i=0; i< iCnt ; i++)
	{
		if( m_nMultiGroupLoopInfoGoto[0] == m_ctrlCboMultiGroup1.GetItemData(i))
		{
			m_ctrlCboMultiGroup1.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[0] > 0 ) m_ctrlCboMultiGroup1.EnableWindow(TRUE);
		}
		if( m_nMultiGroupLoopInfoGoto[1] == m_ctrlCboMultiGroup2.GetItemData(i))
		{
			m_ctrlCboMultiGroup2.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[1] > 0 ) m_ctrlCboMultiGroup2.EnableWindow(TRUE);
		}
		if( m_nMultiGroupLoopInfoGoto[2] == m_ctrlCboMultiGroup3.GetItemData(i))
		{
			m_ctrlCboMultiGroup3.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[2] > 0 ) m_ctrlCboMultiGroup3.EnableWindow(TRUE);
		}
		if( m_nMultiGroupLoopInfoGoto[3] == m_ctrlCboMultiGroup4.GetItemData(i))
		{
			m_ctrlCboMultiGroup4.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[3] > 0 ) m_ctrlCboMultiGroup4.EnableWindow(TRUE);
		}
		if( m_nMultiGroupLoopInfoGoto[4] == m_ctrlCboMultiGroup5.GetItemData(i))
		{
			m_ctrlCboMultiGroup5.SetCurSel(i);
			if (m_nMultiGroupLoopInfoCycle[4] > 0 ) m_ctrlCboMultiGroup5.EnableWindow(TRUE);
		}
	}
	UpdateData(FALSE);
}

void CCTSEditorProView::InitMultiGroup(BOOL bInit)
{
	if (bInit)
	{
		for (int i=0; i<MAX_ACC_GROUP_COUNT ; i++)
		{
			m_nMultiGroupLoopInfoCycle[i]=0;
			m_nMultiGroupLoopInfoGoto[i]=251;
		}
		m_ctrlCboMultiGroup1.ResetContent();
		m_ctrlCboMultiGroup1.AddString("NEXT");
		m_ctrlCboMultiGroup1.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboMultiGroup2.ResetContent();
		m_ctrlCboMultiGroup2.AddString("NEXT");
		m_ctrlCboMultiGroup2.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboMultiGroup3.ResetContent();
		m_ctrlCboMultiGroup3.AddString("NEXT");
		m_ctrlCboMultiGroup3.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboMultiGroup4.ResetContent();
		m_ctrlCboMultiGroup4.AddString("NEXT");
		m_ctrlCboMultiGroup4.SetItemData(0, PS_STEP_NEXT);
		m_ctrlCboMultiGroup5.ResetContent();
		m_ctrlCboMultiGroup5.AddString("NEXT");
		m_ctrlCboMultiGroup5.SetItemData(0, PS_STEP_NEXT);
	}
	m_ctrlEditMultiGroup1 = m_nMultiGroupLoopInfoCycle[0];
	m_ctrlEditMultiGroup2 = m_nMultiGroupLoopInfoCycle[1];
	m_ctrlEditMultiGroup3 = m_nMultiGroupLoopInfoCycle[2];
	m_ctrlEditMultiGroup4 = m_nMultiGroupLoopInfoCycle[3];
	m_ctrlEditMultiGroup5 = m_nMultiGroupLoopInfoCycle[4];
	
	m_ctrlCboMultiGroup1.SetCurSel(0);
	m_ctrlCboMultiGroup2.SetCurSel(0);
	m_ctrlCboMultiGroup3.SetCurSel(0);
	m_ctrlCboMultiGroup4.SetCurSel(0);
	m_ctrlCboMultiGroup5.SetCurSel(0);

	m_ctrlCboMultiGroup1.EnableWindow(FALSE);
	m_ctrlCboMultiGroup2.EnableWindow(FALSE);
	m_ctrlCboMultiGroup3.EnableWindow(FALSE);
	m_ctrlCboMultiGroup4.EnableWindow(FALSE);
	m_ctrlCboMultiGroup5.EnableWindow(FALSE);
}

void CCTSEditorProView::OnChangeEditMultiGroupRepeat1() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboMultiGroup1.GetCurSel() != LB_ERR) 
		m_nMultiGroupLoopInfoCycle[0] = m_ctrlEditMultiGroup1;
	if (m_ctrlEditMultiGroup1 == 0) m_ctrlCboMultiGroup1.EnableWindow(FALSE);
	else m_ctrlCboMultiGroup1.EnableWindow(TRUE);
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnChangeEditMultiGroupRepeat2() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboMultiGroup2.GetCurSel() != LB_ERR) 
		m_nMultiGroupLoopInfoCycle[1] = m_ctrlEditMultiGroup2;
	if (m_ctrlEditMultiGroup2 == 0) m_ctrlCboMultiGroup2.EnableWindow(FALSE);
	else m_ctrlCboMultiGroup2.EnableWindow(TRUE);
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnChangeEditMultiGroupRepeat3() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboMultiGroup3.GetCurSel() != LB_ERR) 
		m_nMultiGroupLoopInfoCycle[2] = m_ctrlEditMultiGroup3;
	if (m_ctrlEditMultiGroup3 == 0) m_ctrlCboMultiGroup3.EnableWindow(FALSE);
	else m_ctrlCboMultiGroup3.EnableWindow(TRUE);
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnChangeEditMultiGroupRepeat4() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboMultiGroup4.GetCurSel() != LB_ERR) 
		m_nMultiGroupLoopInfoCycle[3] = m_ctrlEditMultiGroup4;
	if (m_ctrlEditMultiGroup4 == 0) m_ctrlCboMultiGroup4.EnableWindow(FALSE);
	else m_ctrlCboMultiGroup4.EnableWindow(TRUE);
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnChangeEditMultiGroupRepeat5() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	if(m_ctrlCboMultiGroup5.GetCurSel() != LB_ERR) 
		m_nMultiGroupLoopInfoCycle[4] = m_ctrlEditMultiGroup5;
	if (m_ctrlEditMultiGroup5 == 0) m_ctrlCboMultiGroup5.EnableWindow(FALSE);
	else m_ctrlCboMultiGroup5.EnableWindow(TRUE);
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnSelchangeCboMultiGoto1() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nMultiGroupLoopInfoGoto[0] = m_ctrlCboMultiGroup1.GetItemData(m_ctrlCboMultiGroup1.GetCurSel());
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnSelchangeCboMultiGoto2() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nMultiGroupLoopInfoGoto[1] = m_ctrlCboMultiGroup2.GetItemData(m_ctrlCboMultiGroup2.GetCurSel());
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnSelchangeCboMultiGoto3() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nMultiGroupLoopInfoGoto[2] = m_ctrlCboMultiGroup3.GetItemData(m_ctrlCboMultiGroup3.GetCurSel());
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnSelchangeCboMultiGoto4() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nMultiGroupLoopInfoGoto[3] = m_ctrlCboMultiGroup4.GetItemData(m_ctrlCboMultiGroup4.GetCurSel());
	UpdateData(FALSE);
	
}

void CCTSEditorProView::OnSelchangeCboMultiGoto5() 
{
	m_bStepDataSaved = FALSE;
	UpdateData();
	m_nMultiGroupLoopInfoGoto[4] = m_ctrlCboMultiGroup5.GetItemData(m_ctrlCboMultiGroup5.GetCurSel());
	UpdateData(FALSE);
	
}

int CCTSEditorProView::Fun_CheckMultiLoop(CUIntArray &uiArryMultiLoopStart,CUIntArray &uiArryMultiLoopEnd)
{
	CString strTemp;
	int iTotCntStart,iTotCntEnd, i;
	iTotCntStart = uiArryMultiLoopStart.GetSize();
	iTotCntEnd = uiArryMultiLoopEnd.GetSize();
	if (iTotCntStart == 0 && iTotCntEnd == 0) return 1;
	if (iTotCntStart == 0 )
	{
		strTemp.Format(Fun_FindMsg("Fun_CheckMultiLoop_msg1","IDD_CTSEditorPro_CYCL"));

		MessageBox(strTemp, Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
		return -1;
	}
	if (iTotCntEnd == 0 )
	{
		strTemp.Format(Fun_FindMsg("Fun_CheckMultiLoop_msg2","IDD_CTSEditorPro_CYCL"));
		MessageBox(strTemp, Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
		return -1;
	}
	if (iTotCntStart != iTotCntEnd )
	{
		strTemp.Format(Fun_FindMsg("Fun_CheckMultiLoop_msg3","IDD_CTSEditorPro_CYCL"));
		MessageBox(strTemp, Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
		return -1;
	}

// 	for (i =0; i < iTotCntStart ; i++)
// 	{
// 		if (uiArryMultiLoopStart.GetAt(i) != uiArryMultiLoopEnd.GetAt((iTotCntStart-1)-i))
// 		{
// 			break;	//쌍이 같은 루프가 아님
// 		}
// 	}
// 	if (i < iTotCntStart)
// 	{
// 		strTemp.Format("저장 실패. 멀티 Cycle %d 이 LOOP를 만들지 않음.", i+1);
// 		MessageBox(strTemp, "저장실패", MB_OK|MB_ICONSTOP);
// 		return -1;
// 	}
	
	BOOL bLoopError(FALSE);
	int iCountNum=0;
	for (i =0; i < iTotCntStart ; i++)
	{
		int iFindNum = uiArryMultiLoopStart.GetAt(i);
		iCountNum=0;
		for (int j=0 ; j< iTotCntStart ; j++)
		{
			if (uiArryMultiLoopStart.GetAt(j) == iFindNum)
			{
				iCountNum++;
			}
		}
		if (iCountNum > 1)
		{
			bLoopError = TRUE;
			break;
		}
		
	}
	if (bLoopError)
	{
		strTemp.Format(Fun_FindMsg("Fun_CheckMultiLoop_msg4","IDD_CTSEditorPro_CYCL"), uiArryMultiLoopStart.GetAt(i));
		MessageBox(strTemp, Fun_FindMsg("SaveToEcellFileFail1","IDD_CTSEditorPro_CYCL"), MB_OK|MB_ICONSTOP);
		return -1;
	}
	return 1;
}


void CCTSEditorProView::OnButSafetySetCanAux() 
{
	// TODO: Add your control notification handler code here
	CCommSafetyCanAux dlg;
	dlg.m_pDoc = GetDocument();
	if (dlg.DoModal() == IDOK)
	{
		//버튼에 이름을 변경 -> 설정 개수 표시
		DisplayPreTestParam();
	}
}

void CCTSEditorProView::OnChangeSafetyMaxC() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMaxI() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMaxT() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMaxV() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMaxW() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMaxWh() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMinI() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMinT() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChangeSafetyMinV() 
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnEnChangeSafetyCellDeltaMinV()
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnEnChangeSafetyCellDeltaV()
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnEnChangeSafetyCellDeltaMaxV()
{
	m_bStepDataSaved = FALSE;	
}

void CCTSEditorProView::OnChkChamber() 
{
	UpdateData();
	m_bLinkChamber = m_bChkChamber.GetCheck();
	UpdateData(FALSE);
}

void CCTSEditorProView::OnBnClickedChkDeltaChamber()
{
	UpdateData();
	m_bStepDataSaved = FALSE;
	m_bDeltaVVent = m_bChkDeltaVVent.GetCheck();
	UpdateData(FALSE);
}

void CCTSEditorProView::OnBnClickedChkStepDeltaAuxV()
{
	UpdateData();
	m_bStepDataSaved = FALSE;
	m_bStepVentAuxV = m_bChkStepVentAuxV.GetCheck();
	UpdateData(FALSE);
}

void CCTSEditorProView::OnBnClickedChkStepDeltaAuxTemp()
{
	UpdateData();
	m_bStepDataSaved = FALSE;
	m_bStepDeltaVentAuxTemp = m_bChkStepDeltaVentAuxV.GetCheck();
	UpdateData(FALSE);
}

void CCTSEditorProView::OnBnClickedChkStepDeltaAuxTh()
{
	UpdateData();
	m_bStepDataSaved = FALSE;
	m_bStepDeltaVentAuxTh = m_bChkStepDeltaVentAuxTemp.GetCheck();
	UpdateData(FALSE);
}

void CCTSEditorProView::OnBnClickedChkStepDeltaAuxT()
{
	UpdateData();
	m_bStepDataSaved = FALSE;
	m_bStepDeltaVentAuxT = m_bChkStepDeltaVentAuxTh.GetCheck();
	UpdateData(FALSE);
}

void CCTSEditorProView::OnBnClickedChkStepAuxV()
{
	UpdateData();
	m_bStepDataSaved = FALSE;
	m_bStepDeltaVentAuxV = m_bChkStepDeltaVentAuxT.GetCheck();
	UpdateData(FALSE);
}

void CCTSEditorProView::OnButRegCanCode() 
{
	// TODO: Add your control notification handler code here
// 	CRegCodeDlg dlg;
// 	dlg.m_nUserType = ID_REG_USER_NOMAL; //사용자
// 	dlg.m_iDivision = ID_CAN_TYPE;	//CAN
// 	dlg.m_strTitle = "CAN CODE 를 등록 합니다.";
// 	dlg.m_strTitle2 = "등록 가능 CODE 범위 (1001 ~ 1200)";
// 	dlg.DoModal();
	
}

void CCTSEditorProView::OnButRegAuxCode() 
{
	// TODO: Add your control notification handler code here
// 	CRegCodeDlg dlg;
// 	dlg.m_nUserType = ID_REG_USER_NOMAL; //사용자
// 	dlg.m_iDivision = ID_AUX_TYPE;	//AUX
// 	dlg.m_strTitle = "외부데이터 CODE 를 등록 합니다.";
// 	dlg.m_strTitle2 = "등록 가능 CODE 범위 (2001 ~ 2200)";
// 	dlg.DoModal();
	
}

void CCTSEditorProView::OnRegCanCodeManage() 
{
	// TODO: Add your command handler code here
	CLoginDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;
	
	CRegCodeDlg dlg;
	dlg.m_nUserType = ID_REG_USER_MANAGE; //관리자
	dlg.m_iDivision = ID_CAN_TYPE;	//CAN
	dlg.m_strTitle = Fun_FindMsg("OnButRegCanCode_msg1","IDD_CTSEditorPro_CYCL");
	//&& dlg.m_strTitle2 = "CODE 범위 (1201 ~ 2000, 10001 ~ 20000)";//$1013
	dlg.m_strTitle2 = Fun_FindMsg("EditorView_OnRegCanCodeManage_msg1","IDD_CTSEditorPro_CYCL"); 
	dlg.DoModal();		
}

void CCTSEditorProView::OnRegAuxCodeManage() 
{
	// TODO: Add your command handler code here
	CLoginDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;
	
	CRegCodeDlg dlg;
	dlg.m_nUserType = ID_REG_USER_MANAGE; //관리자
	dlg.m_iDivision = ID_AUX_TYPE;	//AUX
	dlg.m_strTitle = Fun_FindMsg("OnButRegAuxCode_msg1","IDD_CTSEditorPro_CYCL");
	// dlg.m_strTitle2 = "CODE 범위 (2201 ~ 3000, 20001 ~ 30000)";
	dlg.m_strTitle2 = Fun_FindMsg("EditorView_OnRegAuxCodeManage_msg1","IDD_CTSEditorPro_CYCL"); //&&
	dlg.DoModal();
	
}

void CCTSEditorProView::OnCanAllStepButton() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);
	
	ApplySelectCanAuxStep(ID_CAN_TYPE);
	m_bStepDataSaved = FALSE;		
}

void CCTSEditorProView::OnSameCanAllStepButton() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);
	
	STEP *pStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pStep)
	{
		ApplySelectCanAuxStep(ID_CAN_TYPE, pStep->chType);
		m_bStepDataSaved = FALSE;	
	}	
}

void CCTSEditorProView::OnAuxAllStepButton() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);
	
	ApplySelectCanAuxStep(ID_AUX_TYPE);
	m_bStepDataSaved = FALSE;			
}

void CCTSEditorProView::OnSameAuxAllStepButton() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);
	
	STEP *pStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pStep)
	{
		ApplySelectCanAuxStep(ID_AUX_TYPE,pStep->chType);
		m_bStepDataSaved = FALSE;	
	}	
}

void CCTSEditorProView::OnButDbUp() 
{
	// TODO: Add your control notification handler code here	
	if (Fun_UpdateCheckTable()) 
		AfxMessageBox("PreTest Table Update OK");
	else
		AfxMessageBox("PreTest Table Update FAIL");

	if (Fun_UpdateStepTable()) 
		AfxMessageBox("Step Table Update OK");
	else
		AfxMessageBox("Step Table Update FAIL");
	
}

BOOL CCTSEditorProView::Fun_UpdateCheckTable()
{
	CString strPackMDB,strTempMDB;
//	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));

	strPackMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)

// 	switch(nSelLanguage) 20190701
// 	{
// 		case 1 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000.mdb"; break;
// 		case 2 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 		case 3 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_PL.mdb"; break;
// 		default : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	}
	
	strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000.mdb";

	CDaoDatabase  dbPack;
	try
	{
		//db.Open(::GetDataBaseName());
		dbPack.Open(strPackMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	//temp MDB
	strPackMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	strTempMDB = strPackMDB+ "\\" + "Schedule.mdb";
	
	CDaoDatabase  db;
	try
	{
		//db.Open(::GetDataBaseName());
		db.Open(strTempMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL,strUpdateSQL;
	CString strCodeName;
//	int iCode;
	strSQL = "SELECT CheckID,MaxV,MinV,CurrentRange,OCVLimit,TrickleCurrent,DeltaVoltage FROM Check";

	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	
	long nCheckID;
	long lMaxV,lMinV,lMaxI,lMinI,lMaxC,lMaxT;
	CString strTemp;
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data1,data2,data3,data4,data5,data6;
		while(!rs.IsEOF())
		{
			lMaxV=lMinV=lMaxI=lMinI=lMaxC=lMaxT=0;
			data = rs.GetFieldValue("CheckID");
			nCheckID = data.lVal;

			//변경할 데이터 가져오기
			data1 = rs.GetFieldValue("MaxV");
			lMaxV = (long)data1.fltVal;
			data2 = rs.GetFieldValue("MinV");	
			lMinV = (long)data2.fltVal;
			data3 = rs.GetFieldValue("CurrentRange");	//최대전류
			lMaxI = (long)data3.fltVal;
			data4 = rs.GetFieldValue("OCVLimit");		//최대용량
			lMaxC = (long)data4.fltVal;
			data5 = rs.GetFieldValue("TrickleCurrent");	//최소전류
			lMinI = (long)data5.fltVal;
			data6 = rs.GetFieldValue("DeltaVoltage");	//최대온도
			lMaxT = (long)data6.fltVal;

// 			if(VT_NULL != data.vt) strCodeName.Format("%s", data.pbVal);
// 			else strCodeName.Empty();
			
			//Pack_Schedule_2000.mdb 에 변환된 데이터로 덮어 쓴다.
			strUpdateSQL.Format("UPDATE CHECK SET SafetyMaxV=%ld,SafetyMinV=%ld,SafetyMaxI=%ld,SafetyMinI=%ld,SafetyMaxT=%ld,SafetyMinT=0,SafetyMaxC=%ld,SafetyMaxW=0,SafetyMaxWH=0 WHERE CheckID = %ld"
				,lMaxV,lMinV,lMaxI,lMinI,lMaxT,lMaxC,nCheckID);
			//strUpdateSQL.Format("UPDATE Check SET [SafetyMaxV]=%ld WHERE [CheckID] = %ld"
			//	,fMaxV,nCheckID);
			dbPack.Execute(strUpdateSQL);

			rs.MoveNext();
		}	
	}
	rs.Close();
	db.Close();

	dbPack.CanTransact();
	dbPack.Close();

	return TRUE;
}

BOOL CCTSEditorProView::Fun_UpdateStepTable()
{
	CString strPackMDB,strTempMDB;
	
	strPackMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	
// 	switch(nSelLanguage) 20190701
// 	{
// 	case 1 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000.mdb"; break;
// 	case 2 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	case 3 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_PL.mdb"; break;
// 	default : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	}
	
	strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000.mdb";

	CDaoDatabase  dbPack;
	try
	{
		//db.Open(::GetDataBaseName());
		dbPack.Open(strPackMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	//temp MDB
	strPackMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	strTempMDB = strPackMDB+ "\\" + "Schedule.mdb";
	
	
	CDaoDatabase  db;
	try
	{
		//db.Open(::GetDataBaseName());
		db.Open(strTempMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL,strUpdateSQL;
	CString strCodeName;
	
	strSQL = "SELECT StepID,StepType,StepMode,Vref,Iref,Value2,Value4 FROM Step";
	
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	
	int nStepID;
	long lStepType, lStepMode, lIRef, lVRef_Charge, lVRef_DisCharge, lPref, lRref;
	CString strValue2,strValue4;
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		while(!rs.IsEOF())
		{
			lStepType=lStepMode=lIRef=lVRef_Charge=lVRef_DisCharge=lPref=lRref = 0;

			data = rs.GetFieldValue("StepID");
			nStepID = data.lVal;
			
			//변경할 데이터 가져오기
			data = rs.GetFieldValue("StepType");		//충전:1, 방전:2, 휴지:3, 패턴:4
			lStepType = data.lVal;
			data = rs.GetFieldValue("StepMode");		// CC/CV:1, CC:2, CV:3, DC IMP:4, AC IMP:5, CP:6, PUSE:7, CR:8
			lStepMode = data.lVal;
			data = rs.GetFieldValue("Vref");			//충전 Ref 전압
			lVRef_Charge = (long)data.fltVal;
			lVRef_DisCharge = lVRef_Charge;

			data = rs.GetFieldValue("Iref");			//Ref 전류
			lIRef = (long)data.fltVal;

			
			data = rs.GetFieldValue("Value2");
			if(VT_NULL != data.vt)
			{
				strValue2.Format("%s", data.pbVal);
				strValue2 += " 0 0 0";
			}
			else strValue2.Empty();

			data = rs.GetFieldValue("Value4");
			if(VT_NULL != data.vt)
			{
				strValue4.Format("%s", data.pbVal);
				strValue4 += " 0 0 0";
			}
			else strValue4.Empty();
			

			if (lStepType == 1 || lStepType == 2 )	//충전 또는 방전
			{
				if (lStepMode == 6)	//CP
				{
					lPref = lIRef;
					lIRef = 0;
				}
				else if (lStepMode == 8)	//CR
				{
					lRref = lIRef;
					lIRef = 0;
				}

				if (lStepType == 1) lVRef_DisCharge = 0;	//충전 스텝이면
				if (lStepType == 2) lVRef_Charge = 0;		//방전 스텝이면

			}
			else if (lStepType == 4)	//Pattern
			{
				lVRef_DisCharge = lIRef;
				lIRef = 0;
			}

			//Pack_Schedule_2000.mdb 에 변환된 데이터로 덮어 쓴다.
			strUpdateSQL.Format("UPDATE STEP SET Vref=%ld,Iref=%ld,Pref=%ld,Value2='%s',Value4='%s',Vref_DisCharge=%ld, Rref=%d WHERE StepID = %ld"
				,lVRef_Charge,lIRef,lPref,strValue2,strValue4,lVRef_DisCharge,lRref,nStepID);
			dbPack.Execute(strUpdateSQL);

			rs.MoveNext();
		}	
	}
	rs.Close();
	db.Close();
	
	dbPack.CanTransact();
	dbPack.Close();
	
	return TRUE;
}

void CCTSEditorProView::OnChkParallelMode() 
{
	// TODO: Add your control notification handler code here

	m_bStepDataSaved = FALSE;
	CCTSEditorProDoc *pDoc = GetDocument();

	UpdateData();
	pDoc->Fun_SetParallelMode(m_bChkParallel.GetCheck());
	UpdateData(FALSE);
}
/*
BOOL CCTSEditorProView::Fun_CreateChangeMDB()
{
	CString strPackMDB;
	strPackMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000.mdb";
	
	CDaoDatabase  dbPack;
	try
	{
		dbPack.Open(strPackMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL,strTemp;

	//Pack_Schedule_2000.mdb 에 DivisionCode Table 생성.
	
	strSQL = "CREATE TABLE DivisionCode( ID Counter PRIMARY KEY , Code Long CONSTRAINT MyTableConstraint UNIQUE, Code_Name varchar(255), Division Long, Description varchar(255));";
	if(IDYES ==MessageBox("Create table DivisionCode ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		dbPack.Execute(strSQL);
// 		strSQL = "INSERT INTO DivisionCode(Code,Code_Name,Division) VALUES (1901, 'CELL CV',1)";
// 		dbPack.Execute(strSQL);
// 		strSQL = "INSERT INTO DivisionCode(Code,Code_Name,Division) VALUES (2901, 'CELL CV',2)";
// 		dbPack.Execute(strSQL);
// 		strSQL = "INSERT INTO DivisionCode(Code,Code_Name,Division) VALUES (203, 'THERMISTOR3',2)";
// 		dbPack.Execute(strSQL);
	}
	//yulee 2080830 AuxCodeName varchar(127), CanCodeName varchar(127) 추가 
	strSQL = "ALTER TABLE Check ADD COLUMN CanCategory varchar(255), CanCompare varchar(255), CanDataType varchar(255), CanValue varchar(255)";
	strSQL += ", AuxCategory varchar(255), AuxCompare varchar(255), AuxDataType varchar(255), AuxValue varchar(255)";
	strSQL += ", SafetyMaxV REAL, SafetyMinV REAL, SafetyMaxI REAL, SafetyMinI REAL";
	strSQL += ", SafetyMaxT REAL, SafetyMinT REAL, SafetyMaxC REAL, SafetyMaxW Long, SafetyMaxWH Long";
	if(IDYES ==MessageBox("Alter Table Check ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		dbPack.Execute(strSQL);
	}

	strSQL = "ALTER TABLE Step ADD COLUMN Pref REAL";
	strSQL += ", CanCategory varchar(255), CanCodeName TEXT(127), CanDataType varchar(255), CanCompare varchar(255), CanValue varchar(255), CanGoto varchar(255)";
	strSQL += ", AuxCategory varchar(255), AuxCodeName TEXT(127), AuxDataType varchar(255), AuxCompare varchar(255), AuxValue varchar(255), AuxGoto varchar(255)";
	strSQL += ", Vref_DisCharge REAL, Rref REAL;";
	if(IDYES ==MessageBox("Alter Table Step ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		dbPack.Execute(strSQL);
	}

	dbPack.CanTransact();
	dbPack.Close();
	
	//CDaoTableDefInfo 
	CDaoFieldInfo  fieldinfo;
	return TRUE;
}
*/

BOOL CCTSEditorProView::Fun_CreateChangeMDB()
{
CString strPackMDB;
strPackMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	
// 	switch(nSelLanguage) 20190701
// 	{
// 		case 1 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000.mdb"; break;
// 		case 2 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 		case 3 : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_PL.mdb"; break;
// 		default : strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	}

	strPackMDB = strPackMDB+ "\\" + "Pack_Schedule_2000.mdb"; 

  CDaoDatabase  dbPack;
  try
  {
		dbPack.Open(strPackMDB);
		}
		catch (CDaoException* e)
		{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
		}	
		
		  CString strSQL,strTemp;
		  
			//Pack_Schedule_2000.mdb 에 DivisionCode Table 생성.
			
			  //	strSQL = "CREATE TABLE DivisionCode( ID Counter PRIMARY KEY , Code Long CONSTRAINT MyTableConstraint UNIQUE, Code_Name varchar(255), Division Long, Description varchar(255));";
			  //	if(IDYES ==MessageBox("Create table DivisionCode ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
			  //	{
			  //		dbPack.Execute(strSQL);
			  // 		strSQL = "INSERT INTO DivisionCode(Code,Code_Name,Division) VALUES (1901, 'CELL CV',1)";
			  // 		dbPack.Execute(strSQL);
			  // 		strSQL = "INSERT INTO DivisionCode(Code,Code_Name,Division) VALUES (2901, 'CELL CV',2)";
			  // 		dbPack.Execute(strSQL);
			  // 		strSQL = "INSERT INTO DivisionCode(Code,Code_Name,Division) VALUES (203, 'THERMISTOR3',2)";
			  // 		dbPack.Execute(strSQL);
			  //	}
			  
		    /*
			  //yulee 2080830 AuxCodeName varchar(255), CanCodeName varchar(255) 추가 
			  strSQL = "ALTER TABLE Check ADD COLUMN CanCodeName varchar(255), AuxCodeName varchar(255), AuxContiTime varchar(255)";
			  if(IDYES ==MessageBox("Alter Table Check ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
			  {
			  dbPack.Execute(strSQL);
			  }
			
			  //yulee 2080830 AuxCodeName varchar(255), CanCodeName varchar(255) 추가 
			  strSQL = "ALTER TABLE Step ADD COLUMN CanCodeName varchar(255), AuxCodeName varchar(255), AuxContiTime varchar(255)";
			  if(IDYES ==MessageBox("Alter Table Step ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
			  {
			  dbPack.Execute(strSQL);
			  }
			*/

			  //yulee 2080830 AuxCodeName varchar(255), CanCodeName varchar(255) 추가 
			  strSQL = "ALTER TABLE Check ADD COLUMN StepMemo varchar(255)";
			  //if(IDYES ==MessageBox("Alter Table Check ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
			  if(IDYES ==MessageBox("Alter Table Check ?", Fun_FindMsg("CTSEditorProView_Fun_CreateChangeMDB_msg1","IDD_CTSEditorPro_CYCL"), MB_YESNO|MB_ICONQUESTION)) //&&
			  {
			  dbPack.Execute(strSQL);
			  }
			  //yulee 2080830 AuxCodeName varchar(255), CanCodeName varchar(255) 추가 
			  strSQL = "ALTER TABLE Step ADD COLUMN StepMemo varchar(255)";
			  //if(IDYES ==MessageBox("Alter Table Step ?", "확인", MB_YESNO|MB_ICONQUESTION)) 
			  if(IDYES ==MessageBox("Alter Table Step ?", Fun_FindMsg("CTSEditorProView_Fun_CreateChangeMDB_msg2","IDD_CTSEditorPro_CYCL"), MB_YESNO|MB_ICONQUESTION)) //&&
			  {
				  dbPack.Execute(strSQL);
			  }


			  
				dbPack.CanTransact();
				dbPack.Close();
				
				  //CDaoTableDefInfo 
				  CDaoFieldInfo  fieldinfo;
				  return TRUE;
}


void CCTSEditorProView::OnButDbCreate() 
{
	// TODO: Add your control notification handler code here
	if (Fun_CreateChangeMDB()) 
		AfxMessageBox("Create and Change Table OK");
	else
		AfxMessageBox("Create and Change Table FAIL");
}

void CCTSEditorProView::OnRegCanCode() 
{
	// TODO: Add your command handler code here
	CRegCodeDlg dlg;
	dlg.m_nUserType = ID_REG_USER_NOMAL; //사용자
	dlg.m_iDivision = ID_CAN_TYPE;	//CAN
	dlg.m_strTitle = Fun_FindMsg("OnButRegCanCode_msg1","IDD_CTSEditorPro_CYCL"); //$1013
	//&& dlg.m_strTitle2 = "등록 가능 CODE 범위 (1001 ~ 1200), CAN TX CODE 10010 ~ 10014 는 고정 입니다.";
	dlg.m_strTitle2 = Fun_FindMsg("EditorView_OnRegCanCode_msg1","IDD_CTSEditorPro_CYCL");
	dlg.DoModal();		
}

void CCTSEditorProView::OnRegAuxCode() 
{
	// TODO: Add your command handler code here
	CRegCodeDlg dlg;
	dlg.m_nUserType = ID_REG_USER_NOMAL; //사용자
	dlg.m_iDivision = ID_AUX_TYPE;	//AUX
	dlg.m_strTitle = Fun_FindMsg("OnButRegAuxCode_msg1","IDD_CTSEditorPro_CYCL");//$1013
	//&& dlg.m_strTitle2 = "등록 가능 CODE 범위 (2001 ~ 2200)";
	dlg.m_strTitle2 = Fun_FindMsg("EditorView_OnRegAuxCode_msg1","IDD_CTSEditorPro_CYCL");
	dlg.DoModal();
	
}

void CCTSEditorProView::OnAllStepButton2() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);
	
	ApplySelectStepCanMode();
	m_bStepDataSaved = FALSE;		
}

void CCTSEditorProView::OnSameAllStepButton2() 
{
	//현재 설정된 조건을 모든 step에 적용 한다.
	UpdateStepGrid(m_nDisplayStep);
	
	STEP *pStep = GetDocument()->GetStepData(m_nDisplayStep);
	if(pStep)
	{
		ApplySelectStepCanMode(pStep->chType);
		m_bStepDataSaved = FALSE;	
	}		
}

void CCTSEditorProView::OnWorkPneManage() 
{
	CLoginDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;
	
	CRegeditManage dlgReg;
	dlgReg.DoModal();		
}

void CCTSEditorProView::OnKillFocusSafety_Max_I() //yulee 20180729
{
	int iTmp;
	CString strTmp;
	GetDlgItemText(IDC_SAFETY_MAX_I, strTmp);
	iTmp = (atoi(strTmp))*1000;
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "Max Current", iTmp);
	return;
}

void CCTSEditorProView::onStepSocTable() 
{
	CCTSEditorProDoc *pDoc = (CCTSEditorProDoc *)GetDocument();
    if(!pDoc) return;
	
	Dlg_SocTable dlg;
	dlg.m_sPreTestParam=&pDoc->m_sPreTestParam;
	if(dlg.DoModal()==IDOK)
	{
		GetDocument()->m_bEditedFlag = TRUE;
		GetDlgItem(IDC_STEP_SAVE)->EnableWindow(TRUE);
	}
}

void CCTSEditorProView::OnChangeCellDeltaVStep() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
		m_bStepDataSaved = FALSE;
}

HBRUSH CCTSEditorProView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	int nRet = pWnd->GetDlgCtrlID();		//yulee 20190615_1
	if (  (nRet == IDC_STEP_NUM   			)	
		||(nRet == IDC_LOW_STATIC 			)	
		||(nRet == IDC_RPT_TIME_STATIC 		)
		||(nRet == IDC_V_STATIC  			)
		||(nRet == IDC_I_STATIC				)
		||(nRet == IDC_RPT_V_STATIC			)
		||(nRet == IDC_CAP_STATIC			)
		||(nRet == IDC_RPT_I_STATIC			)
		||(nRet == IDC_IMP_STATIC			)
		||(nRet == IDC_RPT_TEMP_STATIC		)	
		//||(nRet == IDC_TEMP_STATIC			)	
		||(nRet == IDC_CAPACITANCE_STATIC2	)	
		||(nRet == IDC_CAP_VTG_STATIC		)
		||(nRet == IDC_RPT_TUNIT_STATIC		)
 		||(nRet == IDC_CHK_CHAMBER			)
 		||(nRet == IDC_CHK_PARALLEL_MODE	)
 		||(nRet == IDC_WARRING_STATIC		)
		||(nRet == IDC_LOW_STATIC			)
		||(nRet == IDC_HIGH_STATIC 			)
		||(nRet == IDC_OR_STATIC			)
		||(nRet == IDC_STEP_AUXT_DELTA	)
		||(nRet == IDC_STEP_AUXTH_DELTA	)
		||(nRet == IDC_STEP_AUXT_AUXTH_DELTA	)
		||(nRet == IDC_STEP_AUXV_TIME	)
		||(nRet == IDC_STEP_AUXV_DELTA	)
		||(nRet == IDC_STEP_AUXT_DELTA_STATIC	)
		||(nRet == IDC_STEP_AUXTH_DELTA_STATIC	)
		||(nRet == IDC_STEP_AUXT_AUXTH_DELTA_STATIC	)
		||(nRet == IDC_STEP_AUXV_TIME_STATIC	)
		||(nRet == IDC_STEP_AUXV_DELTA_STATIC	)
		||(nRet == IDC_CELL_DELTA_V_STATIC	)
		)
	{
		pDC->SetBkMode(TRANSPARENT);
		hbr = (HBRUSH)GetStockObject(NULL_BRUSH);
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

/*
void CCTSEditorProView::OnMenuLangEng() 
{
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 0);
	if(bSelectedLanguage == 2)
	{
		AfxMessageBox(Fun_FindMsg("OnMenuLangChange_msg2"));//&&	
		return;
	}
	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 2);

	if(bChangeToEng == TRUE)
	{
		AfxMessageBox(Fun_FindMsg("OnMenuLangChange_msg1"));//&&
	}
}

void CCTSEditorProView::OnMenuLangKor() 
{
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 0);
	if(bSelectedLanguage == 1)
	{
		AfxMessageBox(Fun_FindMsg("OnMenuLangChange_msg2"));//&&	
		return;
	}

	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 1);

	if(bChangeToEng == TRUE)
	{
		AfxMessageBox(Fun_FindMsg("OnMenuLangChange_msg1"));//&&	
	}
}
*/



void CCTSEditorProView::OnEnKillfocusSafetyCellDeltaStdMinV()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	double	dwValue1 =0.0, dwValue2 =0.0;
	m_StdDeltaMinV.GetValue(dwValue1);
	m_StdDeltaMaxV.GetValue(dwValue2);
	if(dwValue1<0)
	{
		m_StdDeltaMaxV.SetValue(0.0);
		return;
	}
	else if (dwValue1>dwValue2)
	{
		m_StdDeltaMinV.SetValue(dwValue2);
		return;
	}

	m_bStepDataSaved = FALSE;	

	CString strOut;
	dwValue1 =0.0, dwValue2 =0.0;  m_StdDeltaMinV.GetValue(dwValue1); m_StdDeltaMaxV.GetValue(dwValue2);
	CString strUnit;
	strUnit.Format(_T("%s"), GetDocument()->GetVtgUnit()); //yulee 20190909
	if(strUnit.CompareNoCase(_T("V"))== 0)
	{
		strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg1","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
	}
	else
	{
		strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg2","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
	}
	m_StaticCtrlSaftyCommonDelta.SetWindowTextA(strOut);
}

void CCTSEditorProView::OnEnKillfocusSafetyCellDeltaStdMaxV()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	double	dwValue1 =0.0, dwValue2 =0.0;
	m_StdDeltaMinV.GetValue(dwValue1);
	m_StdDeltaMaxV.GetValue(dwValue2);
	if(dwValue2<0)
	{
		m_StdDeltaMaxV.SetValue(0.0);
	}
	else if (dwValue1>dwValue2)
	{
		m_StdDeltaMaxV.SetValue(dwValue1);
	}

	m_bStepDataSaved = FALSE;	

	CString strOut;
	dwValue1 =0.0, dwValue2 =0.0;  m_StdDeltaMinV.GetValue(dwValue1); m_StdDeltaMaxV.GetValue(dwValue2);
	CString strUnit;
	strUnit.Format(_T("%s"), GetDocument()->GetVtgUnit()); //yulee 20190909
	if(strUnit.CompareNoCase(_T("V"))== 0)
	{
		strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg1","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
	}
	else
	{
		strOut.Format(Fun_FindMsg("DisplaySaftyCommon_msg2","IDD_CTSEditorPro_CYCL"),dwValue1,strUnit, dwValue2, strUnit);
	}
	m_StaticCtrlSaftyCommonDelta.SetWindowTextA(strOut);
}
