// Dlg_SocTable.cpp : implementation file
//
//$1013
#include "stdafx.h"
#include "CTSEditorPro.h"
#include "Dlg_SocTable.h"

#include "Dlg_SocTableEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_SocTable dialog


Dlg_SocTable::Dlg_SocTable(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_SocTable::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_SocTable::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_SocTable::IDD3):
	(Dlg_SocTable::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_SocTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_sPreTestParam=NULL;
	m_IsChange=FALSE;
	
	m_strErrorMsg=_T("");
	m_strSocTableFile=_T("");
	m_strSocTableFile_Edit=_T("");
}


void Dlg_SocTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_SocTable)
	DDX_Control(pDX, IDC_EDIT_FILE_NAME, m_EditFileName);
	DDX_Control(pDX, IDC_EDIT_DATA_TITLE, m_EditDataTitle);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_SocTable, CDialog)
	//{{AFX_MSG_MAP(Dlg_SocTable)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	ON_BN_CLICKED(IDC_BUTTON_OPEN, OnButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_SAVEAS, OnButtonSaveAs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_SocTable message handlers

BOOL Dlg_SocTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if(!m_sPreTestParam)
	{
		OnCancel();
		return TRUE;
	}

	CString strFileName;
	strFileName.Format(_T("%s"),m_sPreTestParam->szSocTableFile);

	m_EditDataTitle.SetWindowText(theApp.str_GetFile(strFileName));	
	m_EditFileName.SetWindowText(strFileName);
		
	m_wndGraph.SubclassDlgItem(IDC_SOCTABLE_GRAPH, this);	
	m_SheetData.SetParent(this);
	
	//파일을 읽어들인다.
	if(LoadData(strFileName) == TRUE)
	{	
		//그래프를 그린다.
		UpdateRawDataInfo(strFileName); 	
	}
	GetDlgItem(IDOK)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_SocTable::OnOK() 
{
	if( m_strSocTableFile_Edit.IsEmpty()==FALSE &&
		m_strSocTableFile_Edit!=_T(""))
	{
		if(theApp.file_Exists(m_strSocTableFile_Edit)==FALSE)
		{
			AfxMessageBox("File Copy Fail");
			return;
		}
		if(CopyFile(m_strSocTableFile_Edit,m_strSocTableFile,FALSE)==FALSE)
		{
			AfxMessageBox("File Copy Fail");
			return;
		}
	}
	if( m_strSocTableFile.IsEmpty()==FALSE &&
		m_strSocTableFile!=_T(""))
	{
		sprintf(m_sPreTestParam->szSocTableFile,"%s",m_strSocTableFile);
	}
	CDialog::OnOK();
}

void Dlg_SocTable::OnClose() 
{
	CDialog::OnClose();
}

void Dlg_SocTable::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL Dlg_SocTable::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam==VK_RETURN ||
			pMsg->wParam==VK_ESCAPE )
		{		
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_SocTable::OnButtonNew() 
{
	m_strSocTableFile=_T("");
	m_strSocTableFile_Edit=_T("");
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName,strFileName,strTitleName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\SocTable", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", "csv", "csv");		//ljb 2009-09-04
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "Save Soc Table file Name";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		strTitleName = pDlg.GetFileName();
	}
	else
	{
		return;
	}
	
	Dlg_SocTableEdit dlg;
	dlg.m_strEditFileName = strFileName;
	dlg.m_bEditMode = FALSE;

	if (dlg.DoModal() == IDOK)
	{		
		//Title를 찾아서 화면에 보여준다.//////
		m_EditDataTitle.SetWindowText(theApp.str_GetFile(strFileName));	
		m_EditFileName.SetWindowText(strFileName);

		//AfxMessageBox("File Save 완료");		
		AfxMessageBox(Fun_FindMsg("Dlg_SocTable_OnButtonNew_msg1","DLG_SOCTABLE"));	//&&
		/////////////////////////////////////////
		
		//파일을 읽어들인다.
		if(LoadData(strFileName) == FALSE) return;

		//그래프를 그린다.
		UpdateRawDataInfo(strFileName);

		IsChangeFlag(TRUE);	
		m_strSocTableFile=strFileName;
	}
}

void Dlg_SocTable::OnButtonEdit() 
{
	m_strSocTableFile=_T("");
	m_strSocTableFile_Edit=_T("");
	CString strFileName=theApp.win_GetText(&m_EditFileName);
    if(strFileName.IsEmpty() || strFileName==_T(""))
	{
		return;
	}
	
	CString strPath=theApp.str_GetPath(strFileName);
	
	CString strTemp;
	strTemp.Format(_T("%s\\temp"),strPath);
	theApp.file_ForceDirectory(strTemp);
	theApp.file_DeletefileList(strTemp+_T("\\*.csv"));

	CString strSrc;
	strSrc.Format(_T("%s\\%s.csv"),strTemp,theApp.win_CurrentTime(_T("ifulllog")));
	if(CopyFile(strFileName,strSrc,FALSE)==FALSE)
	{
		AfxMessageBox("File Copy Fail");
		return;
	}

	Dlg_SocTableEdit dlg;
	dlg.m_strEditFileName = strSrc;
	dlg.m_bEditMode = TRUE;
	
	if (dlg.DoModal() == IDOK)
	{
		//파일을 읽어들인다.
		if(LoadData(strSrc) == FALSE) return;	
			
		//그래프를 그린다.
		UpdateRawDataInfo(strSrc);
		
		IsChangeFlag(TRUE);
		m_strSocTableFile=strFileName;
		m_strSocTableFile_Edit=strSrc;
	}
}

void Dlg_SocTable::OnButtonOpen() 
{	
	m_strSocTableFile=_T("");
	m_strSocTableFile_Edit=_T("");
	CString strFileName=theApp.win_GetText(&m_EditFileName);
	if(theApp.file_Exists(strFileName)==FALSE)
	{
		strFileName=_T("");
	}
	if(SelectRawFile(strFileName)==FALSE) return;
	
	//파일을 읽어들인다.
	if(LoadData(strFileName) == FALSE) return;	
	
	//그래프를 그린다.
	UpdateRawDataInfo(strFileName);
	
	IsChangeFlag(TRUE);	
	m_strSocTableFile=strFileName;
}

void Dlg_SocTable::OnButtonSaveAs() 
{
	//다른 이름으로 저장
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\SocTable", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", "csv", "csv");
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "Save Soc Table file Name";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		CString strTgr = pDlg.GetPathName();
		CString strSrc=theApp.win_GetText(&m_EditFileName);
		
		if (CopyFile(strSrc,strTgr,FALSE) == FALSE)
		{
			AfxMessageBox("File Copy Fail");
		}
	}
}

BOOL Dlg_SocTable::SetDataSheet(CString strFile)
{
	return m_SheetData.SetSocTableFile(strFile, m_strErrorMsg);
}

BOOL Dlg_SocTable::LoadData(CString strFile)
{
	if(strFile.IsEmpty() || strFile==_T(""))
	{
		return FALSE;
	}

	if(SetDataSheet(strFile))
	{
		return TRUE;
	}		
	AfxMessageBox(m_strErrorMsg);
	return FALSE;
}

BOOL Dlg_SocTable::UpdateRawDataInfo(CString strFile)
{	
	if (m_SheetData.GetRecordCount() < 20000)
	{
		DrawGraph();
	}
	else
	{
		//AfxMessageBox("데이터가 많아서 그래프로 표시하지 않습니다.");
		AfxMessageBox(Fun_FindMsg("Dlg_SocTable_UpdateRawDataInfo_msg1","DLG_SOCTABLE"));//&&
	}
	return TRUE;
}

BOOL Dlg_SocTable::DrawGraph()
{
	int i = 0;
	int n = m_SheetData.GetRecordCount();	
	BOOL IsK = FALSE;
	
	m_wndGraph.ClearGraph();
	for(int i = 0;  i<PE_MAX_SUBSET; i++)
	{
		m_wndGraph.ShowSubset(i, FALSE);
	}
	
	float *fXData = m_SheetData.GetColumnData(0); //SOC
	float *fData  = m_SheetData.GetColumnData(1); //Voltage
	float *fData2 = m_SheetData.GetColumnData(2); //Current
	
	m_wndGraph.SetDataPtr(0, n, fXData, fData);
	m_wndGraph.SetDataPtr(1, n, fXData, fData2);
	m_wndGraph.ShowSubset(0, TRUE);
	m_wndGraph.ShowSubset(1, TRUE);
	
	//Set X Axis Label
	m_wndGraph.SetXAxisLabel(m_SheetData.GetColumnHeader(0));

	//Set Y Axis Label
	m_wndGraph.SetYAxisLabel(0, "V");
	m_wndGraph.SetYAxisLabel(1, "I");
	
	m_wndGraph.SetDataTitle(0, "Voltage");
	m_wndGraph.SetDataTitle(1, "Current");

	//Set Title
	CString strTemp;
	CString strFile = theApp.win_GetText(&m_EditFileName);
	strTemp = strFile.Mid(strFile.ReverseFind('-')+1, strFile.ReverseFind('.'));
	m_wndGraph.SetMainTitle(strTemp.Left(strTemp.ReverseFind('.')));
	
	m_SheetData.Clear();	
	return TRUE;
}

BOOL Dlg_SocTable::SelectRawFile(CString &strFile)
{
	//CFileDialog dlg(TRUE, "", strFile, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|");
	CFileDialog dlg(TRUE, "", strFile, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, Fun_FindMsg("Dlg_SocTable_SelectRawFile_msg2","DLG_SOCTABLE"));//&&
	if(dlg.DoModal() == IDOK)
	{
		strFile = dlg.GetPathName();
		if(strFile.GetLength() >= 112)
		{
			//AfxMessageBox("파일명을 영문 112글자(한글56자) 이내로 지정하십시요.");
			AfxMessageBox(Fun_FindMsg("Dlg_SocTable_SelectRawFile_msg1","DLG_SOCTABLE")); //&&
			return FALSE;
		}
		return TRUE;
	}
	return FALSE;	
}

void Dlg_SocTable::IsChangeFlag(BOOL IsFlag)
{
	m_IsChange = IsFlag;
	GetDlgItem(IDOK)->EnableWindow(m_IsChange);
}