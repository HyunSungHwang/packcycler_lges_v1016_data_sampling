// AuxCanCodeSaveInDB.cpp : implementation file
//
//$1013
#include "stdafx.h"
#include "CTSEditorPro.h"
#include "AuxCanCodeSaveInDB.h"
#include "CTSEditorProDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAuxCanCodeSaveInDB dialog


CAuxCanCodeSaveInDB::CAuxCanCodeSaveInDB(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAuxCanCodeSaveInDB::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAuxCanCodeSaveInDB::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CAuxCanCodeSaveInDB::IDD3):
	(CAuxCanCodeSaveInDB::IDD), pParent)
{
	//{{AFX_DATA_INIT(CAuxCanCodeSaveInDB)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT


}


void CAuxCanCodeSaveInDB::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAuxCanCodeSaveInDB)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAuxCanCodeSaveInDB, CDialog)
	//{{AFX_MSG_MAP(CAuxCanCodeSaveInDB)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAuxCanCodeSaveInDB message handlers
BOOL CAuxCanCodeSaveInDB::OnInitDialog() 
{
	CDialog::OnInitDialog();
	

	return 1;
}





