#if !defined(AFX_DATETIMECTRL_H__88389CC2_3E6B_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_DATETIMECTRL_H__88389CC2_3E6B_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DateTimeCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyDateTimeCtrl window
#define WM_DATETIME_CHANGED		WM_USER+3201

class CMyDateTimeCtrl : public SECDateTimeCtrl
{
// Construction
public:
	CMyDateTimeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyDateTimeCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyDateTimeCtrl();
	void OnChanged();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyDateTimeCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATETIMECTRL_H__88389CC2_3E6B_11D2_80A6_0020741517CE__INCLUDED_)
