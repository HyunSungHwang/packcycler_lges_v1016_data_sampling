// SelectSimulDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "SelectSimulDataDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define COLUMN_DATA 1
//#define MAX_PATTERN_ROW_COUNT 200000
#define WM_SET_RANGE		WM_USER + 100
#define WM_STEP_IT			WM_USER + 101
#define WM_PROGRESS_SHOW	WM_USER + 102
/////////////////////////////////////////////////////////////////////////////
// CSelectSimulDataDlg dialog


CSelectSimulDataDlg::CSelectSimulDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSelectSimulDataDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSelectSimulDataDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSelectSimulDataDlg::IDD3):
	(CSelectSimulDataDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSelectSimulDataDlg)	
	m_strDataTitle = _T("");
	m_strEndTimeCycle = "1 Cycle END Time :  0 day 00:00:00.0";
	m_bCheck = TRUE;
	m_CtrlLabFileName = _T("");
	//}}AFX_DATA_INIT
	m_strSelFile = _T("");
	
	isChangeSelFile = FALSE;

	lLimitHigh = 0;
	lLimitLow = 0;
	m_chMode = 0;
	IsChange = FALSE;
	m_strExportFile = "";


	//[20191024]Edited By SKH
	m_CurPage = 1;
	m_strCurPage = "1";
	m_XyChar  = NULL;
	m_pProgress = NULL;

	m_bIsApply = FALSE;
}


CSelectSimulDataDlg::~CSelectSimulDataDlg()
{
	m_SheetData.Clear();
	delete m_ChartViewer.getChart();

	if (m_XyChar != NULL)
	{
		delete m_XyChar;
		m_XyChar = NULL;
	}
}

void CSelectSimulDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectSimulDataDlg)
	DDX_Text(pDX, IDC_EDIT_CUR_PAGE, m_CurPage);
	DDX_Control(pDX, IDC_ChartViewer, m_ChartViewer);
	DDX_Text(pDX, IDC_EDIT_DATA_TITLE, m_strDataTitle);
	DDX_Text(pDX, IDC_STATIC_END_TIME_CYCLE, m_strEndTimeCycle);
	DDX_Check(pDX, IDC_CHECK1, m_bCheck);
	DDX_Text(pDX, IDC_LAB_FILE_NAME, m_CtrlLabFileName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectSimulDataDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectSimulDataDlg)
	ON_BN_CLICKED(IDC_BUTTON_DIR, OnButtonDir)
	ON_EN_CHANGE(IDC_EDIT_DATA_TITLE, OnChangeEditDataTitle)
	ON_BN_CLICKED(IDC_BUTTON_OPEN_EXCEL, OnButtonOpenExcel)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SET_RANGE, OnProgressSetRange)
	ON_MESSAGE(WM_STEP_IT, OnProgressStepIt)
	ON_MESSAGE(WM_PROGRESS_SHOW, OnProgressShow)
	ON_BN_CLICKED(IDC_BUTTON_PRE_PAGE, &CSelectSimulDataDlg::OnBnClickedButtonPrePage)
	ON_BN_CLICKED(IDC_BUTTON_NEXT_PAGE, &CSelectSimulDataDlg::OnBnClickedButtonNextPage)
	ON_CONTROL(CVN_MouseMovePlotArea, IDC_ChartViewer, OnMouseMovePlotArea)
	ON_BN_CLICKED(1283, &CSelectSimulDataDlg::OnBnClicked1283)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectSimulDataDlg message handlers

void CSelectSimulDataDlg::OnButtonDir() 
{
	// TODO: Add your control notification handler code here
	

	
	m_CurPage = 1;

	CString strFile = SelectRawFile(m_strSelFile);
	if (m_strSelFile == strFile) return;

	m_strNewSelFile.Empty();
	if(strFile != "NULL" && !strFile.IsEmpty())
	{
		//데이터를 다시 받아오면 현재페이지 1로한다.
		m_CurPage = 1;
		//상시 화면 전화할떄 그래프를 지워준다.
		delete m_ChartViewer.getChart();

		isChangeSelFile = TRUE;		//적용시에 파일을 변경하기 위해
		CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
		CString strSrc = strDir + "\\" + strFile;
		m_strNewSelFile = strSrc;

		m_strExportFile = strSrc;		





		UpdateRawDataInfo(m_strNewSelFile);//그래프를 그린다.	

		IsChange = TRUE;

		GetDlgItem(IDC_BUTTON_PRE_PAGE)->EnableWindow(false);
		GetDlgItem(IDC_BUTTON_NEXT_PAGE)->EnableWindow(true);
		GetDlgItem(IDOK)->EnableWindow(TRUE);
	}
	else
	{
		IsChange = FALSE;
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	}
//	m_wndGraph.UnZoom();
}

CString CSelectSimulDataDlg::GetPathName()
{
	return m_strSelFile;
}

CString CSelectSimulDataDlg::GetDataTitle()
{
	return m_strDataTitle;
}

CString CSelectSimulDataDlg::SelectRawFile(CString strFile)
{
	CString strOldFile = strFile;
	CString strSelFile;
	CString strNull = "NULL";
	CFileDialog dlg(TRUE, "", strSelFile, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|All Files (*.*)|*.*|");
	if(dlg.DoModal() == IDOK)
	{
		CString strFileName = dlg.GetPathName();
		if(strFileName.GetLength() >= 112)
		{
			AfxMessageBox(Fun_FindMsg("SelectRawFile_msg1","IDD_SIMUL_DATA_DLG"));
		//	AfxMessageBox("파일명을 영문 112글자(한글56자) 이내로 지정하십시요.");
			return strOldFile;
		}

		CString strFileName2 = dlg.GetFileName();
		m_CtrlLabFileName = strFileName;
		m_strDataTitle = strFileName2.Mid(0,strFileName2.ReverseFind('.'));
		UpdateData(FALSE);

		if(LoadData(strFileName) == FALSE)	return strOldFile;	
		
		//1.1 Field 수 및 Record 수
		//1.2 출력값의 MAX값 확인
		//1.3 시간(최소 출력 간격 시간 및 시간 연전 등)
		CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
		
		//2. 기존 Rawdata file 삭제 
		if(strFile.IsEmpty() == FALSE)
		{
			DeleteFile(strDir+"\\"+m_strSelFile);
		}

		//3. copy data file to database folder

		CTime ct = CTime::GetCurrentTime();
		strSelFile.Format("%s.tmp", ct.Format("%Y%m%d%H%M%S"));	

		//if(MakeSimulationFile(strFileName, strDir+"\\"+strSelFile) == FALSE)
		if(CopyFile(strFileName, strDir+"\\"+strSelFile, TRUE) == FALSE)
		{
			AfxMessageBox(strFileName+Fun_FindMsg("SelectRawFile_msg2","IDD_SIMUL_DATA_DLG"));
			return strOldFile;
		}
		return strSelFile;
	}
	else
		return strOldFile;

}

BOOL CSelectSimulDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_BUTTON_PRE_PAGE)->EnableWindow(false);
	m_lMaxRefI = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	m_nMinRefI = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Min Ref Current", 25);

	m_wndGraph.SubclassDlgItem(IDC_SIMUL_GRAPH, this);

	//Progress 생성
	m_pProgress = new CProgressDlg;
	m_pProgress->Create(174);

	m_SheetData.SetParent(this);
	
	// TODO: Add extra initialization here
	if(m_strSelFile.IsEmpty())		//현재 등록된 파일이 없으면
	{
		//1. Select raw data file
		m_strNewSelFile = SelectRawFile(m_strSelFile);
		if(m_strNewSelFile.IsEmpty())	//파일이 등록되지 않았다면.
		{
			OnCancel();				//종료
		}
		else						//파일이 등록되면 그래프를 그린다.
		{
			//m_strDataTitle = m_strNewSelFile.Mid(0,m_strNewSelFile.ReverseFind('.'));
			//m_CtrlLabFileName = m_strNewSelFile;
			isChangeSelFile = TRUE;
			CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
			CString strSrc = strDir + "\\" + m_strNewSelFile;
			m_strExportFile = strSrc;
			m_strNewSelFile = strSrc;			//파일정보로 절대 경로를 갖는다.
			UpdateRawDataInfo(m_strNewSelFile);
		}
		GetDlgItem(IDOK)->EnableWindow(TRUE);
	}
	else							//헌재 등록된 파일이 있다면
	{
		//Title를 찾아서 화면에 보여준다.//////
		m_CtrlLabFileName = m_strSelFile;
		CString strFile(m_strSelFile);
		int nStart=strFile.Find('-');
		int nEnd = strFile.ReverseFind('.');

		if(nStart >= 0 && nEnd >= 0 && nStart < nEnd)
		{
			m_strDataTitle = strFile.Mid(nStart+1, nEnd-nStart-1);
		}
		/////////////////////////////////////////
		m_strExportFile = m_strSelFile;

		if(LoadData(m_strSelFile) == TRUE)	//파일을 읽어들인다.
			UpdateRawDataInfo(m_strSelFile); //그래프를 그린다.				
	}	

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CSelectSimulDataDlg::UpdateRawDataInfo(CString strFile)
{
	
	
	DrawGraph();		//그래프를 그린다.


	return TRUE;
}

void CSelectSimulDataDlg::OnOK() 
{
#if 0
	// TODO: Add extra validation here
	
	UpdateData();
//	AfxMessageBox("현 장비는 Pattern 기능을 제공하지 않습니다. \r\nPattern 기능 사용을 원하시면 (주)피앤이솔루션으로 문의하시기 바랍니다. ");
//	return;

	if(m_strDataTitle.IsEmpty())
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_SIMUL_DATA_DLG"));
		return;
	}
	if (m_strNewSelFile.IsEmpty() && m_strSelFile.IsEmpty()) 
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_SIMUL_DATA_DLG"));
		return;
	}

	//파일명 최종 Update한다.
	CString strSimulationFile;
	CString strSrc;
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));	
	
	if(isChangeSelFile)				//파일을 새로 등록했다면
	{
		int nStart = m_strNewSelFile.ReverseFind('\\')+1;
		int nEnd = m_strNewSelFile.ReverseFind('.') - nStart;
		strSimulationFile.Format("%s-%s.csv",
			m_strNewSelFile.Mid(nStart,nEnd ),m_strDataTitle);

		//이전 파일 삭제
		if(m_strSelFile != "")
		{		
			CFileFind aFind;
			BOOL bFind = aFind.FindFile(m_strSelFile);
			while(bFind)
			{
				bFind = aFind.FindNextFile();
				DeleteFile(aFind.GetFilePath());
			}
		}

		m_strSelFile = m_strNewSelFile;		//다시 등록*/

	}
	else
	{
		CTime ct = CTime::GetCurrentTime();
		strSimulationFile.Format("%s-%s.csv", ct.Format("%Y%m%d%H%M%S"),  m_strDataTitle);
	}

	//file rename 
	strSrc = m_strSelFile;
	m_strSelFile = strDir + "\\" + strSimulationFile;

	int nFile= 2;
	while(rename(strSrc, m_strSelFile) !=0)
	{
		CString strFileNum;
		strFileNum.Format("(%d)", nFile);
		m_strSelFile.Insert(m_strSelFile.ReverseFind('.'), strFileNum);
	}


	//임시 파일 삭제
	DeleteTempFile();
	if(m_pProgress != NULL)
	{
		m_pProgress->DestroyWindow();
		delete m_pProgress;
	}

	if(m_bCheck)
	{
		pStep->ulEndTime =atol(m_strTimeEndCycle);
	}
#endif

	ApplyPattern();

	//임시 파일 삭제
	DeleteTempFile();
	if(m_pProgress != NULL)
	{
		m_pProgress->DestroyWindow();
		delete m_pProgress;
		m_pProgress = NULL;
	}

	CDialog::OnOK();
}

//ksj 20200910 : OnOk 함수에서 분리.
void CSelectSimulDataDlg::ApplyPattern()
{
	// TODO: Add extra validation here

	UpdateData();
	//	AfxMessageBox("현 장비는 Pattern 기능을 제공하지 않습니다. \r\nPattern 기능 사용을 원하시면 (주)피앤이솔루션으로 문의하시기 바랍니다. ");
	//	return;

	if(m_strDataTitle.IsEmpty())
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_SIMUL_DATA_DLG"));
		return;
	}
	if (m_strNewSelFile.IsEmpty() && m_strSelFile.IsEmpty()) 
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_SIMUL_DATA_DLG"));
		return;
	}

	//파일명 최종 Update한다.
	CString strSimulationFile;	
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));	

	if(isChangeSelFile)				//파일을 새로 등록했다면
	{
		int nStart = m_strNewSelFile.ReverseFind('\\')+1;
		int nEnd = m_strNewSelFile.ReverseFind('.') - nStart;
		strSimulationFile.Format("%s-%s.csv",
			m_strNewSelFile.Mid(nStart,nEnd ),m_strDataTitle);

		//이전 파일 삭제
		if(m_strSelFile != "")
		{		
			CFileFind aFind;
			BOOL bFind = aFind.FindFile(m_strSelFile);
			while(bFind)
			{
				bFind = aFind.FindNextFile();
				DeleteFile(aFind.GetFilePath());
			}
		}

		m_strSelFile = m_strNewSelFile;		//다시 등록*/

	}
	else
	{
		CTime ct = CTime::GetCurrentTime();
		strSimulationFile.Format("%s-%s.csv", ct.Format("%Y%m%d%H%M%S"),  m_strDataTitle);
	}

	//file rename 
	CString strSrc;
	strSrc = m_strSelFile;
	m_strSelFile = strDir + "\\" + strSimulationFile;

	int nFile= 2;
	//while(rename(strSrc, m_strSelFile) !=0)
	while(::CopyFile(strSrc, m_strSelFile, TRUE) == 0) //ksj 20200910 : 임시파일 남겨두고 복사하도록 변경. 임시파일은 나중에 지운다. 임시파일 이름을 바꿔버리는 경우, 다시 패턴 수정할려고할때 임시파일이 없어 수정할 수 없는 상황이 생김. //근데 TRUE 했는데도 무조건 덮어씌우는듯??
	{
		CString strFileNum;
		strFileNum.Format("(%d)", nFile);
		m_strSelFile.Insert(m_strSelFile.ReverseFind('.'), strFileNum);
	}


	if(m_bCheck)
	{
		pStep->ulEndTime =atol(m_strTimeEndCycle);
	}

	IsChange = FALSE;
	m_bIsApply = TRUE;
}

void CSelectSimulDataDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	if(IsChange)
	{
		if(AfxMessageBox(Fun_FindMsg("OnCancel_msg1","IDD_SIMUL_DATA_DLG"), MB_YESNO) != IDYES)
			return;
	}
	//임시 파일 삭제
	
	DeleteTempFile();
	if(m_pProgress != NULL)
	{
		m_pProgress->DestroyWindow();
		delete m_pProgress;
		m_pProgress = NULL;
	}
	
	//CDialog::OnCancel();

	if(m_bIsApply) //패턴 에디터에서 수정하여 적용한 경우.
	{
		//ksj 20200910
		//패턴 편집기에서 수정한 패턴이 적용되었습니다.
		//MessageBox("The pattern modified in the pattern editor has been applied.","Info",MB_ICONINFORMATION|MB_OK);
		MessageBox(Fun_FindMsg("OnCancel_msg2","IDD_SIMUL_DATA_DLG"),"Info",MB_ICONINFORMATION|MB_OK);
		CDialog::OnOK();
	}
	else //그냥 캔슬.
	{
		CDialog::OnCancel();
	}
}

BOOL CSelectSimulDataDlg::SetDataSheet(CString strFile)
{
	return m_SheetData.SetSimulationFile(strFile, m_strErrorMsg);
}

BOOL CSelectSimulDataDlg::SetPattrenDataSheet(CString strFile)
{

	if(m_SheetData.SetSimulationHeadFile(strFile,m_strErrorMsg))
	{
		return m_SheetData.SetSimulationRowFile(strFile,m_strErrorMsg);
	}

	return FALSE;
}


BOOL CSelectSimulDataDlg::LoadData(CString strFile)
{
	long low = 0;
	long high = 0;

	if(SetPattrenDataSheet(strFile))
	{

		//1. 파일 읽기 및 파일 유효성 체크
		if(m_SheetData.GetColumnHeader(0) != "T1" && m_SheetData.GetColumnHeader(0) != "t1"
			&&m_SheetData.GetColumnHeader(0) != "T2" && m_SheetData.GetColumnHeader(0) != "t2"
			&&m_SheetData.GetColumnHeader(0) != "TEMP" && m_SheetData.GetColumnHeader(0) != "Temp")
		{
			AfxMessageBox(Fun_FindMsg("LoadData_msg1","IDD_SIMUL_DATA_DLG"));
			return FALSE;
		}
		
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) != "P" && m_SheetData.GetColumnHeader(COLUMN_DATA) != "p" &&
			m_SheetData.GetColumnHeader(COLUMN_DATA) != "I" && m_SheetData.GetColumnHeader(COLUMN_DATA) != "i" &&
			m_SheetData.GetColumnHeader(COLUMN_DATA) != "V" && m_SheetData.GetColumnHeader(COLUMN_DATA) != "v")			//ljb 20150122 add pattern cv	
		{
			AfxMessageBox(Fun_FindMsg("LoadData_msg1","IDD_SIMUL_DATA_DLG"));
			return FALSE;
		}

		CString strType = m_SheetData.GetColumnHeader(COLUMN_DATA); //ksj 20200824 : 출력 타입 파싱
		strType.MakeUpper(); //ksj 20200825 : 대문자로 변경

		TRACE("%d",m_SheetData.GetRecordCount());

		//ksj 20200605
		//if(m_SheetData.GetReadRecordSize() > MAX_PATTERN_ROW_COUNT)
		int nMaxPatternRow = AfxGetApp()->GetProfileInt("Config","MAX_PATTERN_ROW",50000);
		if(m_SheetData.GetReadRecordSize()  > nMaxPatternRow && nMaxPatternRow != 0) //0이면 무시.
		{
			CString strErrorMsg;
			strErrorMsg.Format("The maximum number of rows has been exceeded. (%d, Max: %d)", m_SheetData.GetRecordCount(), nMaxPatternRow);
			AfxMessageBox(strErrorMsg );
			return FALSE;
		}
		//ksj end

		//2. 최소값 최대값 
		float * fValue = m_SheetData.GetColumnData(COLUMN_DATA);
		if(fValue != NULL)
		{
			 
			low = fabs(fValue[0]) * 1000;
			TRACE("Record count: %d",m_SheetData.GetRecordCount());
			for(int i = 0 ; i < m_SheetData.GetRecordCount(); i++)
			{
				if(high < fabs(fValue[i])*1000)
					high = fabs(fValue[i]) * 1000;
				
				if(low > fabs(fValue[i])*1000)
					low = fabs(fValue[i]) * 1000;
			}
		}

		//3. 조건값과 비교
		//3.1 전류 상하한값 비교
		if(low < 0)
		{
			CString m_strError;
			m_strError.Format(Fun_FindMsg("LoadData_msg2","IDD_SIMUL_DATA_DLG"),  0, m_lMaxRefI);
			AfxMessageBox(m_strError);								
			return FALSE;
		}

		//3.2 전류 안전조건 상하한값 비교
		//전류 제한 상한값을 입력하였을 경우 전류 설정값보다 반드시 커야 한다.
		/*if(pStep->fILimitHigh > 0)
		{
			if(high >=pStep->fILimitHigh)
			{
				CString m_strError;
				m_strError.Format(Fun_FindMsg("LoadData_msg3","IDD_SIMUL_DATA_DLG"),  pStep->fILimitHigh, high);
				AfxMessageBox(m_strError);								
				return FALSE;
			}
		}*/
		//ksj 20200825 : 패턴이 전류 형식일때만 전류 상한 조건 체크
		//패턴의 출력이 파워 인경우에는 체크하면 안됨
		if(pStep->fILimitHigh > 0 && strType.Compare("I") == 0)
		{
			if(high >=pStep->fILimitHigh)
			{
				CString m_strError;
				m_strError.Format(Fun_FindMsg("LoadData_msg3","IDD_SIMUL_DATA_DLG"),  pStep->fILimitHigh, high);
				AfxMessageBox(m_strError);								
			}
		}

		//3.3 1Cycle 종료시간 계산
		fValue = m_SheetData.GetColumnData(0);
		if(fValue != NULL)
		{
			if(m_SheetData.GetColumnHeader(0) == "T1" || m_SheetData.GetColumnHeader(0) == "t1" )
			{
				m_cPatternTime = PS_PATTERN_TIME_ACUMULATE;

			}
			else if(m_SheetData.GetColumnHeader(0) == "T2" || m_SheetData.GetColumnHeader(0) == "t2" )
			{
				m_cPatternTime = PS_PATTERN_TIME_OPERATING;
// 				m_strEndTimeCycle.Format("1 Cycle 종료시간 : %d일 %02d:%02d:%02d.%d", oldtinDay, endTime.GetHour(), 
// 					endTime.GetMinute(), endTime.GetSecond(), lmSec);
			}
			else if(m_SheetData.GetColumnHeader(0) == "TEMP" || m_SheetData.GetColumnHeader(0) == "Temp" )
			{
				m_cPatternTime = PS_PATTERN_TIME_TEMPERATURE;
				m_bCheck = FALSE;
			}
			float fTime = 0.0f;
			if(m_SheetData.GetRecordCount() > 0)
			{
				CString strTime;
				TRACE("%.03f", fValue[1]*1000);
				//strTime.Format("%.1f", fValue[m_SheetData.GetRecordCount()-1]*10);
				strTime.Format("%.1f", fValue[m_SheetData.GetRecordCount()-1]*100);
				m_strTimeEndCycle.Format("%.1f", fValue[m_SheetData.GetRecordCount()-1]*100);
				
				ULONG lTime = atol(strTime);
				//ULONG lSecond = (ULONG)fValue[m_SheetData.GetRecordCount()-1];
				ULONG lSecond = (ULONG)fValue[m_SheetData.GetRecordCount()-1];
				ULONG lmSec = (lTime % 100);
				//ULONG lmSec = (lTime % 10);
				COleDateTime endTime;
				
				
				int nHour = lSecond % 86400;
				int nDay = lSecond / 86400;
				endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
				m_strEndTimeCycle.Format(Fun_FindMsg("LoadData_msg4","IDD_SIMUL_DATA_DLG"), nDay, endTime.GetHour(), 
					endTime.GetMinute(), endTime.GetSecond(), lmSec);
			}
			

			UpdateData(FALSE);
		}

		lLimitHigh = high;
		lLimitLow = low;
		
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "I" || m_SheetData.GetColumnHeader(COLUMN_DATA) == "i")		m_chMode = PS_MODE_CC;
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "P" || m_SheetData.GetColumnHeader(COLUMN_DATA) == "p")		m_chMode = PS_MODE_CP;		//ljb 20150122 add
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "V" || m_SheetData.GetColumnHeader(COLUMN_DATA) == "v")		m_chMode = PS_MODE_CV;		//ljb 20150122 add
		
		//+2015.9.23 USY Add For PatternCV
		CheckPatternData(strFile);
		//-

	}
	else
	{
		if(m_strErrorMsg == "")
			AfxMessageBox(Fun_FindMsg("LoadData_msg5","IDD_SIMUL_DATA_DLG"));
		else
			AfxMessageBox(m_strErrorMsg);
		IsChange = FALSE;
		return FALSE;
		
	}
	return TRUE;
}

BOOL CSelectSimulDataDlg::DrawGraph()
{
	int i = 0;
	BOOL IsK = FALSE;

// 	m_wndGraph.ClearGraphData();
// 	m_wndGraph.ShowSubset(0, TRUE);
	
	m_XyChar = new XYChart(1350, 600);

	float *fXData = m_SheetData.GetColumnData(0);		//T
	float *fData = m_SheetData.GetColumnData(COLUMN_DATA);		//P or I
	double* _fXData = new double[m_SheetData.GetRecordCount()];
	double* _fData = new double[m_SheetData.GetRecordCount()];

	int k = m_SheetData.m_ReadRecordCount;
	for(int i = 0 ; i < m_SheetData.GetRecordCount() ; i++)
	{

		_fXData[i] = fXData[i];
		_fData[i]  = fData[i];
	
	}

	for(i = 0 ; i < m_SheetData.GetRecordCount() ; i++)
	{
		if(_fData[i] > 1000)
		{
			IsK = TRUE;
			break;
			
		}
	}
	
	if(IsK == TRUE)
	{
		for(int i = 0 ; i < m_SheetData.GetRecordCount() ; i++)
		{
			_fData[i] = _fData[i] / 1000;
		}
	}
	
	
		// Set the plotarea at (50, 55) with width 100 pixels less than chart width, and height 90 pixels
    // less than chart height. Use a vertical gradient from light blue (f0f6ff) to sky blue (a0c0ff)
    // as background. Set border to transparent and grid lines to white (ffffff).
    m_XyChar->setPlotArea(50, 55, m_XyChar->getWidth() - 100, m_XyChar->getHeight() - 90, m_XyChar->linearGradientColor(0, 55, 0,
        m_XyChar->getHeight() - 35, 0xf0f6ff, 0xa0c0ff), -1, Chart::Transparent, 0xffffff, 0xffffff);

	// Add a legend box at (50, 25) using horizontal layout. Use 10pts Arial Bold as font. Set the
    // background and border color to Transparent.
    m_XyChar->addLegend(50, 25, false, "arialbd.ttf", 10)->setBackground(Chart::Transparent);
	
    // Set axis label style to 8pts Arial Bold
    m_XyChar->xAxis()->setLabelStyle("arialbd.ttf", 8);
    m_XyChar->yAxis()->setLabelStyle("arialbd.ttf", 8);

    // Set the axis stem to transparent
    m_XyChar->xAxis()->setColors(Chart::Transparent);
    m_XyChar->yAxis()->setColors(Chart::Transparent);
 

	//m_pProgress->SetWindowText("그래프를 표현하고 있습니다.");

	//Set Y Axil Label
	if(!IsK)
	{
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "I")
			// Add axis title using 10pts Arial Bold Italic font
			m_XyChar->yAxis()->setTitle("A", "arialbi.ttf", 10);
		else if (m_SheetData.GetColumnHeader(COLUMN_DATA) == "P")
		{
			// Add axis title using 10pts Arial Bold Italic font
			m_XyChar->yAxis()->setTitle("W", "arialbi.ttf", 10);
		}
		else if (m_SheetData.GetColumnHeader(COLUMN_DATA) == "V")
		{
			// Add axis title using 10pts Arial Bold Italic font
			m_XyChar->yAxis()->setTitle("V)", "arialbi.ttf", 10);
		}
	}
	else
	{
		if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "I")
			// Add axis title using 10pts Arial Bold Italic font
			m_XyChar->yAxis()->setTitle("KA", "arialbi.ttf", 10);
		else if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "P")
			// Add axis title using 10pts Arial Bold Italic font
			 m_XyChar->yAxis()->setTitle("KW", "arialbi.ttf", 10);
		else if(m_SheetData.GetColumnHeader(COLUMN_DATA) == "V")
		{
			// Add axis title using 10pts Arial Bold Italic font
			 m_XyChar->yAxis()->setTitle("KV", "arialbi.ttf", 10);
		}
	}
	


    // Add a line layer to the chart using a line width of 2 pixels.
    LineLayer *layer = m_XyChar->addLineLayer();
    layer->setLineWidth(2);
	
	DoubleArray dataY(_fData,m_SheetData.GetRecordCount()); 
    DoubleArray dataX(_fXData,m_SheetData.GetRecordCount());

    // Add 2 data series to the line layer
    layer->setXData(dataX);
    layer->addDataSet(dataY, 0xcc0000, "");

	
	// Assign the chart to the WinChartViewer
    m_ChartViewer.setChart(m_XyChar);


	delete[] _fXData;
	delete[] _fData;
	
	return TRUE;
}

BOOL CSelectSimulDataDlg::DeleteTempFile(CString strFile)
{
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
	CFileFind aFind;
	BOOL bFind;

	if(strFile == "")
		bFind = aFind.FindFile(strDir + "\\" + "??????????????.tmp");
	else
		bFind = aFind.FindFile(strFile);

	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}

	return TRUE;
}

BOOL CSelectSimulDataDlg::MakeSimulationFile(CString strExistingFileName, CString strNexFileName)
{
	CStdioFile aFile;
	CFileException e;

	if( !aFile.Open( strExistingFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   return FALSE;
	}

	CStringList		strDataList;
	CString			strTemp;
	while(aFile.ReadString(strTemp))
	{
		if(strTemp == "STX")			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			while(aFile.ReadString(strTemp))
			{
				if(!strTemp.IsEmpty())	
				{
					strDataList.AddTail(strTemp);
				}
			}

		}	
	}	
	aFile.Close();


	if( !aFile.Open( strNexFileName, CFile::modeWrite|CFile::modeCreate, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be Created " << e.m_cause << "\n";
		#endif
		   return FALSE;
	}

	POSITION pos = strDataList.GetHeadPosition();
	while(pos)
	{
		aFile.WriteString(strDataList.GetAt(pos));
		aFile.WriteString("\r\n");
		strDataList.GetNext(pos);
	}
	
	aFile.Close();



	return TRUE;
}



void CSelectSimulDataDlg::OnChangeEditDataTitle() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow(TRUE);
	
}

LRESULT CSelectSimulDataDlg::OnProgressSetRange(WPARAM wParam, LPARAM lParam)
{
	int nRange = (int)wParam;
	m_pProgress->SetRange(nRange);
	return 1;
}
LRESULT CSelectSimulDataDlg::OnProgressStepIt(WPARAM wParam, LPARAM lParam)
{
	m_pProgress->StepIt();
	return 1;
}
LRESULT CSelectSimulDataDlg::OnProgressShow(WPARAM wParam, LPARAM lParam)
{
	int nShow = (int)wParam;
	m_pProgress->ShowWindow(nShow);
	return 1;
}

void CSelectSimulDataDlg::OnButtonOpenExcel() 
{
	//다른 이름으로 저장
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName,strFileName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\Pattern", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");		//ljb 2009-09-04
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "Save Pattern file Name";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();

		if (CopyFile(m_strExportFile,strFileName,FALSE) == FALSE)
			AfxMessageBox("File Copy Fail");
	}
	else
	{
		return;
	}



	//Excel 로 열기 위해 CScheduleData의 Excel 기능을 사용한다.
// 	CScheduleData scData;
// 	if (m_strExportFile.IsEmpty()) return;
// 	scData.ExecuteExcel(m_strExportFile);
	
}

void CSelectSimulDataDlg::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here
	CString m_strNewSelFile_Old, m_strSelFile_Old;

	CEditSimulDataDlg	dlg;
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
	CString strFile;

	if(!m_strNewSelFile.IsEmpty())		//현재 등록된 파일이 없으면
	{
		//strFile = strDir + "\\" + m_strNewSelFile;
		m_strNewSelFile_Old = m_strNewSelFile;
		strFile = m_strNewSelFile;
	}
	else if(!m_strSelFile.IsEmpty())		//현재 등록된 파일이 없으면
	{
		m_strSelFile_Old = m_strSelFile;
		strFile = m_strSelFile;
	}
	else
		AfxMessageBox(Fun_FindMsg("OnButtonEdit_msg1","IDD_SIMUL_DATA_DLG"));

// 		if (m_strSelFile.Find(".tmp") > 0)
// 		{
// 			strFile = strDir + "\\" + m_strSelFile;
// 		}
// 		else
// 			strFile = m_strSelFile;

	//+2015.9.22 USY Add for PatternCV

	dlg.m_lMaxCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	dlg.m_lMaxVoltage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);

	dlg.m_lMaxCurrent = dlg.m_lMaxCurrent / 1000;
	dlg.m_lMaxVoltage = dlg.m_lMaxVoltage / 1000;

	int nMaxParralCnt;
	nMaxParralCnt = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Parallel Count", 2);
	if (m_bParallelMode)	//ljb 201134 이재복 //////////
	{
		dlg.m_lMaxCurrent = dlg.m_lMaxCurrent * nMaxParralCnt;
	}
	//-
	dlg.m_strEditFileName = strFile;
	dlg.m_cPatternTime = m_cPatternTime;
	dlg.m_chMode = m_chMode;
	dlg.m_bEditMode = TRUE;

	if (dlg.DoModal() == IDOK)
	{
//		strFile = m_strSelFile;
		int nStart=strFile.Find('-');
		int nEnd = strFile.ReverseFind('.');
		
		if(nStart >= 0 && nEnd >= 0 && nStart < nEnd)
		{
			m_strDataTitle = strFile.Mid(nStart+1, nEnd-nStart-1);
		}
		/////////////////////////////////////////
// 		if (m_strSelFile.Find(".tmp") > 0)
// 		{
// 			strFile = strDir + "\\" + m_strSelFile;
// 		}
// 		else
// 			strFile = m_strSelFile;
// 	
// 		if (strFile	== "")
// 		{
// 			if (m_strNewSelFile.Find(".tmp") > 0)
// 			{
// 				strFile = m_strNewSelFile;
// 			}
// 		}
		if(LoadData(strFile) == FALSE)	//파일을 읽어들인다.
			return ;	
		else								//그래프를 그린다.
			UpdateRawDataInfo(strFile);			
		IsChange = TRUE;
		//GetDlgItem(IDOK)->EnableWindow(TRUE); //lyj 20200910 패턴
		GetDlgItem(IDOK)->EnableWindow(FALSE); //lyj 20200910 패턴
		ApplyPattern();

		//수정한 패턴이 적용되었습니다.
		//MessageBox("The modified pattern was applied.","Info",MB_ICONINFORMATION|MB_OK);
		MessageBox(Fun_FindMsg("OnButtonEdit_msg2","IDD_SIMUL_DATA_DLG"),"Info",MB_ICONINFORMATION|MB_OK);
	}
	else
	{
//		AfxMessageBox("cancle");
		m_strNewSelFile = m_strNewSelFile_Old;
		m_strSelFile = m_strSelFile_Old;
	}
	m_strExportFile = strFile;
}

void CSelectSimulDataDlg::OnButtonNew() 
{
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName,strFileName,strTitleName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\Pattern", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");		//ljb 2009-09-04
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "Save Pattern file Name";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
		strTitleName = pDlg.GetFileName();
	}
	else
	{
		return;
	}

	CEditSimulDataDlg	dlg;
	dlg.m_strEditFileName = strFileName;
	dlg.m_cPatternTime = PS_PATTERN_TIME_ACUMULATE;
	dlg.m_chMode = PS_MODE_CC;
	dlg.m_bEditMode = FALSE;

	//+2015.10.02 USY Add for PatternCV
	
	dlg.m_lMaxCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	dlg.m_lMaxVoltage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);

	dlg.m_lMaxCurrent = dlg.m_lMaxCurrent / 1000;
	dlg.m_lMaxVoltage = dlg.m_lMaxVoltage / 1000;

	int nMaxParralCnt;
	nMaxParralCnt = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Parallel Count", 2);
	if (m_bParallelMode)	//ljb 201134 이재복 //////////
	{
		dlg.m_lMaxCurrent = dlg.m_lMaxCurrent * nMaxParralCnt;
	}
	//-
	
	if (dlg.DoModal() == IDOK)
	{
		AfxMessageBox(Fun_FindMsg("OnButtonNew_msg1","IDD_SIMUL_DATA_DLG"));

		//Title를 찾아서 화면에 보여준다.//////
		m_CtrlLabFileName = m_strSelFile;
		CString strFile(strTitleName);
		int nEnd = strFile.ReverseFind('.');
		
		if(nEnd >= 0)
		{
			m_strDataTitle = strFile.Mid(0, nEnd);
		}
		/////////////////////////////////////////
		m_strExportFile = strFileName;
		
		if(LoadData(strFileName) == TRUE)	//파일을 읽어들인다.
			UpdateRawDataInfo(strFileName); //그래프를 그린다.				
	
		UpdateData(FALSE);
		//OnButtonDir();
	}
}

void CSelectSimulDataDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	// 	m_wndGraph.ClearGraphData();
//	m_wndGraph.UnZoom();
	
}


//+2015.9.23 USY Add For PatternCV
//패턴파일의 스텝별 데이터 체크
BOOL CSelectSimulDataDlg::CheckPatternData(CString strFile)
{
	CString	strErrorMsg;
	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;	
	CStringList strColList;
	double lMaxCurrent = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current", 5000);
	double lMaxVoltage = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);
	
	lMaxCurrent = lMaxCurrent / 1000;
	lMaxVoltage = lMaxVoltage / 1000;
	
	int nMaxParralCnt;
	nMaxParralCnt = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Parallel Count", 2);
	if (m_bParallelMode)	//ljb 201134 이재복 //////////
	{
		lMaxCurrent = lMaxCurrent * nMaxParralCnt;
	}

	
	if( !aFile.Open( strFile, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		strErrorMsg.Format(Fun_FindMsg("CheckPatternData_msg1","IDD_SIMUL_DATA_DLG"), strFile);
		AfxMessageBox(strErrorMsg);
		return FALSE;
	}
	
	CStringList		strDataList;
	CString strMode,strValue;
	strMode = "";

	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == "STX")			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			int p1=0, s=0;
			//1. Title//////////////////////////////////////////////////////////////
			if(aFile.ReadString(strTemp))
			{
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
						//m_TitleList.AddTail(str);
						s  = p1+1;
					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				strMode = str;
			}
			else
			{
				aFile.Close(); //lyj 20200518
				strErrorMsg.Format(Fun_FindMsg("CheckPatternData_msg2","IDD_SIMUL_DATA_DLG"), strFile);
				AfxMessageBox(strErrorMsg);
				return FALSE;
			}
			////////////////////////////////////////////////////////////////////////
			double dVoltage;
			double dChgI;
			double dDisI;
			UINT nRow = 1;
			while(aFile.ReadString(strTemp))
			{
				if (strTemp == "") break;

				p1 = s = 0;

				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);			//시간
						s = p1+1;
					}
				}
				if (strMode == "V")
				{//CV에 대해서만 장비설정 최대전류와, 최대전압과, 현재 각 라인별 데이터를 비교하자.

					AfxExtractSubString(strValue,strTemp,1,','); dVoltage = atof(strValue);
					AfxExtractSubString(strValue,strTemp,2,','); dChgI = atof(strValue);
					AfxExtractSubString(strValue,strTemp,3,','); dDisI = atof(strValue);

					if(dVoltage > lMaxVoltage)
					{
						strErrorMsg.Format(Fun_FindMsg("CheckPatternData_msg3","IDD_SIMUL_DATA_DLG"), strFile, nRow, lMaxVoltage);
						aFile.Close();
						AfxMessageBox(strErrorMsg);
						return FALSE;
					}
					if(dChgI > lMaxCurrent)
					{
						strErrorMsg.Format(Fun_FindMsg("CheckPatternData_msg4","IDD_SIMUL_DATA_DLG"), strFile, nRow, lMaxCurrent);
						aFile.Close();
						AfxMessageBox(strErrorMsg);
						return FALSE;
					}
					if(abs(dDisI) > lMaxCurrent)
					{
						strErrorMsg.Format(Fun_FindMsg("CheckPatternData_msg5","IDD_SIMUL_DATA_DLG"), strFile, nRow, lMaxCurrent);
						aFile.Close();
						AfxMessageBox(strErrorMsg);
						return FALSE;
					}
				}
				nRow++;
			}
		}	
	}
	aFile.Close();
	return TRUE;
}

void CSelectSimulDataDlg::OnBnClickedButtonPrePage()
{
	CString strErrorMsg;
	strErrorMsg.Format("%s 파일을 열 수 없습니다.", m_strNewSelFile);
	GetDlgItem(IDC_BUTTON_PRE_PAGE)->EnableWindow(true);
	m_CurPage--;

	if(m_CurPage == 1)
	{	
	
		m_CurPage = 1;
		delete m_ChartViewer.getChart();
		m_SheetData.m_HistoryOffSetPos = 0;
		SetPattrenDataSheet(m_strNewSelFile);
		UpdateRawDataInfo(m_strNewSelFile);
		GetDlgItem(IDC_BUTTON_PRE_PAGE)->EnableWindow(false);

		UpdateData(false);
	

	}else
	{
		
		delete m_ChartViewer.getChart();
		m_SheetData.m_HistoryOffSetPos = m_SheetData.m_HistoryOffSetPos - 2;
		m_SheetData.SetSimulationRowFile(m_strNewSelFile,strErrorMsg);
		UpdateRawDataInfo(m_strNewSelFile);
		UpdateData(false);
	}

		
	

}


void CSelectSimulDataDlg::OnBnClickedButtonNextPage()
{
	CString strErrorMsg;
	strErrorMsg.Format("%s 파일을 열 수 없습니다.", m_strNewSelFile);
	GetDlgItem(IDC_BUTTON_NEXT_PAGE)->EnableWindow(true);

	if(m_SheetData.m_MaxPageFlag == false){
		GetDlgItem(IDC_BUTTON_PRE_PAGE)->EnableWindow(true);
		m_CurPage++;
		delete m_ChartViewer.getChart();
		m_SheetData.SetSimulationRowFile(m_strNewSelFile,strErrorMsg);
		UpdateRawDataInfo(m_strNewSelFile);
		UpdateData(false);

	}else
	{
		GetDlgItem(IDC_BUTTON_NEXT_PAGE)->EnableWindow(false);
		UpdateData(false);

	}
}


void CSelectSimulDataDlg::OnMouseMovePlotArea()
{
    trackLineAxis((XYChart *)m_ChartViewer.getChart(), m_ChartViewer.getPlotAreaMouseX());
    m_ChartViewer.updateDisplay();
	
    // Hide the track cursor when the mouse leaves the plot area
    m_ChartViewer.removeDynamicLayer(CVN_MouseLeavePlotArea);
}


void CSelectSimulDataDlg::trackLineAxis(XYChart *c, int mouseX)
{
    // Clear the current dynamic layer and get the DrawArea object to draw on it.
    DrawArea *d = c->initDynamicLayer();
	
    // The plot area object
    PlotArea *plotArea = c->getPlotArea();
	
    // Get the data x-value that is nearest to the mouse, and find its pixel coordinate.
    double xValue = c->getNearestXValue(mouseX);
    int xCoor = c->getXCoor(xValue);
	
    // The vertical track line is drawn up to the highest data point (the point with smallest
    // y-coordinate). We need to iterate all datasets in all layers to determine where it is.
    int minY = plotArea->getBottomY();
	
    // Iterate through all layers to find the highest data point
    for(int i = 0; i < c->getLayerCount(); ++i) {
        Layer *layer = c->getLayerByZ(i);
		
        // The data array index of the x-value
        int xIndex = layer->getXIndexOf(xValue);
		
        // Iterate through all the data sets in the layer
        for(int j = 0; j < layer->getDataSetCount(); ++j) {
            DataSet *dataSet = layer->getDataSetByZ(j);
			
            double dataPoint = dataSet->getPosition(xIndex);
			if ((dataPoint != Chart::NoValue) && (dataSet->getDataColor() != Chart::Transparent)) {
				minY = min(minY, c->getYCoor(dataPoint, dataSet->getUseYAxis()));
            }
        }
    }
	
    // Draw a vertical track line at the x-position up to the highest data point.
	d->vline(max(minY, plotArea->getTopY()), plotArea->getBottomY() + 6, xCoor, d->dashLineColor(
        0x000000, 0x0101));
	
    // Draw a label on the x-axis to show the track line position
	CString temp;
	temp.Format("<*font,bgColor=000000*>%s<*/font*>", c->xAxis()->getFormattedLabel(xValue, "") );
	TTFText *t = d->text(temp, "arialbd.ttf", 8);
	t->draw(xCoor, plotArea->getBottomY() + 6, 0xffffff, Chart::Top);
	t->destroy();
	
    // Iterate through all layers to build the legend array
    for(int k = 0; k < c->getLayerCount(); ++k) {
        Layer *layer = c->getLayerByZ(k);
		
        // The data array index of the x-value
        int xIndex = layer->getXIndexOf(xValue);
		
        // Iterate through all the data sets in the layer
        for(int j = 0; j < layer->getDataSetCount(); ++j) {
            DataSet *dataSet = layer->getDataSetByZ(j);
			
            // The positional value, axis binding, pixel coordinate and color of the data point.
            double dataPoint = dataSet->getPosition(xIndex);
            Axis *yAxis = dataSet->getUseYAxis();
            int yCoor = c->getYCoor(dataPoint, yAxis);
            int color = dataSet->getDataColor();
			
            // Draw the axis label only for visible data points of named data sets
			if ((dataPoint != Chart::NoValue) && (color != Chart::Transparent) && (yCoor >=
                plotArea->getTopY()) && (yCoor <= plotArea->getBottomY())) {
                // The axis label consists of 3 parts - a track dot for the data point, an axis label,
                // and a line joining the track dot to the axis label.
				
                // Draw the line first. The end point of the line at the axis label side depends on
                // whether the label is at the left or right side of the axis (that is, on whether the
                // axis is on the left or right side of the plot area).
				int xPos = yAxis->getX() + ((yAxis->getAlignment() == Chart::Left) ? -4 : 4);
                d->hline(xCoor, xPos, yCoor, d->dashLineColor(color, 0x0101));
				
                // Draw the track dot
                d->circle(xCoor, yCoor, 4, 4, color, color);
				
                // Draw the axis label. If the axis is on the left side of the plot area, the labels
                // should right aligned to the axis, and vice versa.
				CString temp;
				temp.Format("<*font,bgColor=ff0000*>%s<*/font*>", c->formatValue(dataPoint, "{value|P4}"));
				t = d->text(temp, "arialbd.ttf", 8);
				t->draw(xPos, yCoor, 0xffffff, 
					((yAxis->getAlignment() == Chart::Left) ? Chart::Right : Chart::Left));
				t->destroy();
            }
        }
    }
}


void CSelectSimulDataDlg::OnBnClicked1283()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
