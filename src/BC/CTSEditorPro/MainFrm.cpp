// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CTSEditorPro.h"

#include "MainFrm.h"
#include "CTSEditorProView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

// 	// TODO: Delete these three lines if you don't want the toolbar to
	// be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	
//	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
//		| WS_THICKFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;
	
	cs.style &= ~FWS_ADDTOTITLE;	//ljb

//	cs.cx = 1029;		//ljb size 조절
//	cs.cy = 768;

#ifdef _CYCLER_
	cs.lpszName = PROGRAM_VER;
#else
	cs.lpszName = "CTSEditorPro";
#endif
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
extern CString g_strDataBaseName;

CString GetDataBaseName()
{
/*	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(FORM_PATH_REG_SECTION ,"DataBase");		//Get DataBase Folder
	if(strTemp.IsEmpty())
	{
		return strTemp;
	}
	
	strTemp = strTemp+ "\\" + FORM_SET_DATABASE_NAME;
	return strTemp;
*/
	return g_strDataBaseName;
}

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default

	//CTSMonPro에서 전달된 Message
	if(pCopyDataStruct->dwData == 3)
	{
		if(pCopyDataStruct->cbData > 0)
		{
//			char *pData = new char[pCopyDataStruct->cbData];
//			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
			
			CString strArgu((char *)pCopyDataStruct->lpData);

			CString strModel, strTest;
			strModel = strArgu.Left(strArgu.Find(','));
			strTest = strArgu.Mid(strArgu.Find(',')+1);
			
			//char szBuff1[64], szBuff2[64];
			//sscanf(pData, "%s %s", szBuff1, szBuff2);
//			delete [] pData;
			
			CCTSEditorProView	*pView = (CCTSEditorProView *)GetActiveView();

			if(pView)
				pView->SetDefultTest(strModel, strTest);
		}
	}	
	
	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

void CMainFrame::OnClose() 
{
	CString strTemp;

	// TODO: Add your message handler code here and/or call default
	if(((CCTSEditorProDoc *)GetActiveDocument())->IsEdited() ||
		((CCTSEditorProView *)GetActiveView())->m_bStepDataSaved == FALSE)
	{
		//strTemp = "조건이 변경 되었습니다. 저장후 종료 하시겠습니까?";

		switch(gi_Language)	{
		case 1	:	{
			strTemp = "조건이 변경 되었습니다. 저장후 종료 하시겠습니까?";
					}break;

		case 2	:	{
			strTemp = "save condition change. SAVE & CTSEditorPro Program finish?";
					}break;

		case 3	:	{
			strTemp = "save condition change. SAVE & CTSEditorPro Program finish?";
					}break;
		default:
			strTemp = "조건이 변경 되었습니다. 저장후 종료 하시겠습니까?";
			break;
		}
		//int nRtn = MessageBox(strTemp, "저장", MB_ICONQUESTION|MB_YESNOCANCEL);
		int nRtn = MessageBox(strTemp, "Save", MB_ICONQUESTION|MB_YESNOCANCEL); //&&
		if(IDYES == nRtn)
		{
			if(((CCTSEditorProView *)GetActiveView())->StepSave() == FALSE)
			{
				switch(gi_Language)	
				{
				case 1	:	
					if(MessageBox("저장을 실패 하였습니다. 종료 하시겠습니까?", "종료", MB_ICONQUESTION|MB_YESNO) != IDYES)
						return;
					break;

				case 2	:	

					if(MessageBox("Save fail.Will CTSEditorPro Program finish?", "Exit", MB_ICONQUESTION|MB_YESNO) != IDYES)
						return;
					break;

				case 3	:	

					if(MessageBox("Save fail.Will CTSEditorPro Program finish?", "Exit", MB_ICONQUESTION|MB_YESNO) != IDYES)
						return;
					break;
				default:
					if(MessageBox("저장을 실패 하였습니다. 종료 하시겠습니까?", "종료", MB_ICONQUESTION|MB_YESNO) != IDYES)
						return;
					break;
				}
			}
		}
		else if(nRtn == IDCANCEL)
		{
			return;
		}

	}

	CFrameWnd::OnClose();
}

