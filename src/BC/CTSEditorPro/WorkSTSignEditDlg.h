#if !defined(AFX_WORKSTSIGNEDITDLG_H__64EED44E_3213_461D_9A28_9B210C426C64__INCLUDED_)
#define AFX_WORKSTSIGNEDITDLG_H__64EED44E_3213_461D_9A28_9B210C426C64__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WorkSTSignEditDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWorkSTSignEditDlg dialog
#include "resource.h"
class CWorkSTSignEditDlg : public CDialog
{
// Construction
public:
	CWorkSTSignEditDlg(CWnd* pParent = NULL);   // standard constructor
	CLabel	m_CntrlPlaceSign;
// Dialog Data
	//{{AFX_DATA(CWorkSTSignEditDlg)
	enum { IDD = IDD_WORKSTSIGNEDIT_DLG };

	//}}AFX_DATA

	void SetWinPos (CRect rect);
	void CWorkSTSignEditDlg::InitControlLbl();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkSTSignEditDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
  	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_VIRTUAL
	
// Implementation
protected:
// 	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//  afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	// Generated message map functions
	//{{AFX_MSG(CWorkSTSignEditDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKSTSIGNEDITDLG_H__64EED44E_3213_461D_9A28_9B210C426C64__INCLUDED_)
