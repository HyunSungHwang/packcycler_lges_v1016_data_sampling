#if !defined(AFX_REGCODEDLG_H__54DFCB75_61B5_4121_BE75_79528202CA6A__INCLUDED_)
#define AFX_REGCODEDLG_H__54DFCB75_61B5_4121_BE75_79528202CA6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegCodeDlg.h : header file
//
#include "MyGridWnd.h"	// Added by ClassView

/////////////////////////////////////////////////////////////////////////////
// CRegCodeDlg dialog

class CRegCodeDlg : public CDialog
{
// Construction
public:

	void IsChangeFlag(BOOL IsFlag);
	BOOL Fun_LoadData();
	CMyGridWnd m_CodeListGrid;
	BOOL InitListGrid();
	CRegCodeDlg(CWnd* pParent = NULL);   // standard constructor

public:
	BOOL Fun_SaveData();

	CUIntArray m_arryCode;	//사용자 일때 코드 리스트
	int		m_nUserType;	//ljb 20170831 이재복 작업자 구분 (사용자 1, 관리자 2) //////////
	int		m_iDivision;	//ljb 2011218 이재복 등록하는 항목(CAN = 1, AUX = 2) //////////
	CString m_strTitle;
	CString m_strTitle2;
	BOOL	m_bIsChange;
// Dialog Data
	//{{AFX_DATA(CRegCodeDlg)
	enum { IDD = IDD_REG_CODE_DLG , IDD2 = IDD_REG_CODE_DLG_ENG , IDD3 = IDD_REG_CODE_DLG_PL };
	CString	m_labTitle;
	CString	m_labTitle2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRegCodeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddCode();
	afx_msg void OnDeleteCode();
	afx_msg void OnButSave();
	afx_msg void OnCsvImport();
	afx_msg void OnCsvExport();
	//}}AFX_MSG
	afx_msg LRESULT OnGridBeginEdit(WPARAM wParam, LPARAM lParam);
//	afx_msg void OnGridPaste(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGCODEDLG_H__54DFCB75_61B5_4121_BE75_79528202CA6A__INCLUDED_)
