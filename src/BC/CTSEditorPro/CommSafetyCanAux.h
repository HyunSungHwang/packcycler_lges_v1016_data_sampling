#if !defined(AFX_COMMSAFETYCANAUX_H__925528FC_C96F_4489_99CA_CBFDAD0BDC8D__INCLUDED_)
#define AFX_COMMSAFETYCANAUX_H__925528FC_C96F_4489_99CA_CBFDAD0BDC8D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommSafetyCanAux.h : header file
//
#include "CTSEditorProDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CCommSafetyCanAux dialog

class CCommSafetyCanAux : public CDialog
{
// Construction
public:
	void Fun_FindCanAuxName(UINT uiType, int nPos, int nDivision);
	void InitComboBox();
	void InitControl();
	void Fun_DisplayData();
	CCommSafetyCanAux(CWnd* pParent = NULL);   // standard constructor
	CCTSEditorProDoc *m_pDoc;

// Dialog Data
	//{{AFX_DATA(CCommSafetyCanAux)
	enum { IDD = IDD_DLG_COMM_SAFETY , IDD2 = IDD_DLG_COMM_SAFETY_ENG , IDD3 = IDD_DLG_COMM_SAFETY_PL };
	CComboBox	m_ctrlCboAuxCode_10;
	CComboBox	m_ctrlCboAuxCode_9;
	CComboBox	m_ctrlCboAuxCode_8;
	CComboBox	m_ctrlCboAuxCode_7;
	CComboBox	m_ctrlCboAuxCode_6;
	CComboBox	m_ctrlCboAuxCode_5;
	CComboBox	m_ctrlCboAuxCode_4;
	CComboBox	m_ctrlCboAuxCode_3;
	CComboBox	m_ctrlCboAuxCode_2;
	CComboBox	m_ctrlCboAuxCode_1;
	CComboBox	m_ctrlCboCanCode_10;
	CComboBox	m_ctrlCboCanCode_9;
	CComboBox	m_ctrlCboCanCode_8;
	CComboBox	m_ctrlCboCanCode_7;
	CComboBox	m_ctrlCboCanCode_6;
	CComboBox	m_ctrlCboCanCode_5;
	CComboBox	m_ctrlCboCanCode_4;
	CComboBox	m_ctrlCboCanCode_3;
	CComboBox	m_ctrlCboCanCode_2;
	CComboBox	m_ctrlCboCanCode_1;
	CComboBox	m_ctrlAuxDataType_10;
	CComboBox	m_ctrlAuxDataType_9;
	CComboBox	m_ctrlAuxDataType_8;
	CComboBox	m_ctrlAuxDataType_7;
	CComboBox	m_ctrlAuxDataType_6;
	CComboBox	m_ctrlAuxDataType_5;
	CComboBox	m_ctrlAuxDataType_4;
	CComboBox	m_ctrlAuxDataType_3;
	CComboBox	m_ctrlAuxDataType_2;
	CComboBox	m_ctrlAuxDataType_1;
	CComboBox	m_ctrlCanDataType_10;
	CComboBox	m_ctrlCanDataType_9;
	CComboBox	m_ctrlCanDataType_8;
	CComboBox	m_ctrlCanDataType_7;
	CComboBox	m_ctrlCanDataType_6;
	CComboBox	m_ctrlCanDataType_5;
	CComboBox	m_ctrlCanDataType_4;
	CComboBox	m_ctrlCanDataType_3;
	CComboBox	m_ctrlCanDataType_2;
	CComboBox	m_ctrlCanDataType_1;
	CComboBox	m_ctrlAuxCompare_10;
	CComboBox	m_ctrlAuxCompare_9;
	CComboBox	m_ctrlAuxCompare_8;
	CComboBox	m_ctrlAuxCompare_7;
	CComboBox	m_ctrlAuxCompare_6;
	CComboBox	m_ctrlAuxCompare_5;
	CComboBox	m_ctrlAuxCompare_4;
	CComboBox	m_ctrlAuxCompare_3;
	CComboBox	m_ctrlAuxCompare_2;
	CComboBox	m_ctrlAuxCompare_1;
	CComboBox	m_ctrlCanCompare_10;
	CComboBox	m_ctrlCanCompare_9;
	CComboBox	m_ctrlCanCompare_8;
	CComboBox	m_ctrlCanCompare_7;
	CComboBox	m_ctrlCanCompare_6;
	CComboBox	m_ctrlCanCompare_5;
	CComboBox	m_ctrlCanCompare_4;
	CComboBox	m_ctrlCanCompare_3;
	CComboBox	m_ctrlCanCompare_2;
	CComboBox	m_ctrlCanCompare_1;
	float   m_fCanValue_1;
	float   m_fCanValue_2;
	float   m_fCanValue_3;
	float   m_fCanValue_4;
	float   m_fCanValue_5;
	float   m_fCanValue_6;
	float   m_fCanValue_7;
	float   m_fCanValue_8;
	float   m_fCanValue_9;
	float   m_fCanValue_10;
	float   m_fAuxValue_2;
	float   m_fAuxValue_3;
	float   m_fAuxValue_4;
	float   m_fAuxValue_5;
	float   m_fAuxValue_6;
	float   m_fAuxValue_7;
	float   m_fAuxValue_8;
	float   m_fAuxValue_9;
	float   m_fAuxValue_10;
	float	m_fAuxValue_1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommSafetyCanAux)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CUIntArray uiArryCanCode;
	CUIntArray uiArryAuxCode;
	CStringArray strArryCanName;
	CStringArray strArryAuxName;

	// Generated message map functions
	//{{AFX_MSG(CCommSafetyCanAux)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeCboCanCode2();
	afx_msg void OnSelchangeCboCanCode10();
	afx_msg void OnSelchangeCboCanCode1();
	afx_msg void OnSelchangeCboCanCode3();
	afx_msg void OnSelchangeCboCanCode4();
	afx_msg void OnSelchangeCboCanCode5();
	afx_msg void OnSelchangeCboCanCode6();
	afx_msg void OnSelchangeCboCanCode7();
	afx_msg void OnSelchangeCboCanCode8();
	afx_msg void OnSelchangeCboCanCode9();
	afx_msg void OnSelchangeCboAuxCode1();
	afx_msg void OnSelchangeCboAuxCode2();
	afx_msg void OnSelchangeCboAuxCode3();
	afx_msg void OnSelchangeCboAuxCode4();
	afx_msg void OnSelchangeCboAuxCode5();
	afx_msg void OnSelchangeCboAuxCode6();
	afx_msg void OnSelchangeCboAuxCode7();
	afx_msg void OnSelchangeCboAuxCode8();
	afx_msg void OnSelchangeCboAuxCode9();
	afx_msg void OnSelchangeCboAuxCode10();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMSAFETYCANAUX_H__925528FC_C96F_4489_99CA_CBFDAD0BDC8D__INCLUDED_)
