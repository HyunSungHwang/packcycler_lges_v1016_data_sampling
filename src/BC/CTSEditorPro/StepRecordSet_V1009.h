#if !defined(AFX_STEPRECORDSET_V1009_H__928D9894_0CED_4926_895E_16864946DCE3__INCLUDED_)
#define AFX_STEPRECORDSET_V1009_H__928D9894_0CED_4926_895E_16864946DCE3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StepRecordSet_V1009.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet_V1009 DAO recordset

class CStepRecordSet_V1009 : public CDaoRecordset
{
public:
	CStepRecordSet_V1009(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CStepRecordSet_V1009)

// Field/Param Data
	//{{AFX_FIELD(CStepRecordSet_V1009, CDaoRecordset)
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStepRecordSet_V1009)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STEPRECORDSET_V1009_H__928D9894_0CED_4926_895E_16864946DCE3__INCLUDED_)
