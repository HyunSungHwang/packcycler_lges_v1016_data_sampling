// TestNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "TestNameDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestNameDlg dialog
extern CString g_strDataBaseName;

CTestNameDlg::CTestNameDlg(long lModelID, BOOL bCopyMode/* = FALSE*/, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CTestNameDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CTestNameDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CTestNameDlg::IDD3):
	(CTestNameDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CTestNameDlg)
	m_strName = _T("");
	m_strDecription = _T("");
	m_strCreator = _T("");
	m_bCopyMode = bCopyMode;
	m_bContinueCellCode = FALSE;
	//}}AFX_DATA_INIT
	m_lModelID = lModelID;
	m_lTestTypeID = 0;
}


void CTestNameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestNameDlg)
	DDX_Control(pDX, IDC_PROC_TYPE_COMBO, m_ctrlProcTypeCombo);
	DDX_Control(pDX, IDC_MODEL_SEL_COMBO, m_ctrlModelListCombo);
	DDX_Text(pDX, IDC_TEST_NAME_EDIT, m_strName);
	DDX_Text(pDX, IDC_TEST_DESCRIP_EDIT, m_strDecription);
	DDX_Text(pDX, IDC_TEST_CREATOR_EDIT, m_strCreator);
	DDX_Check(pDX, IDC_RESET_PROC_CHECK, m_bContinueCellCode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestNameDlg, CDialog)
	//{{AFX_MSG_MAP(CTestNameDlg)
	ON_EN_CHANGE(IDC_TEST_NAME_EDIT, OnChangeTestNameEdit)
	ON_EN_CHANGE(IDC_TEST_DESCRIP_EDIT, OnChangeTestDescripEdit)
	ON_EN_CHANGE(IDC_TEST_CREATOR_EDIT, OnChangeTestCreatorEdit)
	ON_CBN_SELCHANGE(IDC_MODEL_SEL_COMBO, OnSelchangeModelSelCombo)
	ON_CBN_SELCHANGE(IDC_PROC_TYPE_COMBO, OnSelchangeProcTypeCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestNameDlg message handlers

void CTestNameDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	int nIndex = m_ctrlModelListCombo.GetCurSel();
	if(nIndex != CB_ERR)
		m_lModelID = m_ctrlModelListCombo.GetItemData(nIndex);
	if(m_strName.IsEmpty())
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_TEST_NAME_DLG"));
		GetDlgItem(IDC_TEST_NAME_EDIT)->SetFocus();
		return;
	}

	if(m_strName.Find('\\') >= 0 || m_strName.Find('/') >= 0 || m_strName.Find(':') >= 0 || m_strName.Find('*') >= 0 ||
		m_strName.Find('?') >= 0|| m_strName.Find('\"') >= 0 || m_strName.Find('<') >= 0|| m_strName.Find('>') >= 0
		||	m_strName.Find('|') >= 0)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_TEST_NAME_DLG"));
		GetDlgItem(IDC_TEST_NAME_EDIT)->SetFocus();
		return;
	}
	
	CDialog::OnOK();
}


BOOL CTestNameDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	if(m_bCopyMode)
	{
		GetDlgItem(IDC_MODEL_SEL_COMBO)->EnableWindow(TRUE);
	}


	//////////////////////////////////////////////////////////////////////////
	//모델 선택 Combo 초기화
	//////////////////////////////////////////////////////////////////////////
	int nCount = 0;
	long lModelPK;
	int nDefaultIndex = 0;
	CString strSQL;
	COleVariant data;
	
	CDaoDatabase  db;
	db.Open(g_strDataBaseName);
	CDaoRecordset rs(&db);

	try
	{
		strSQL = "SELECT No, ModelName, ModelID FROM BatteryModel";
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		while(!rs.IsEOF())
		{
	//		data = rs.GetFieldValue(0);		lNo = data.lVal;
			data = rs.GetFieldValue(2);		lModelPK = data.lVal;
			data = rs.GetFieldValue(1);		
			strSQL.Format("%d : %s", nCount+1, data.pbVal);
			if(m_lModelID == lModelPK) nDefaultIndex = nCount;
			m_ctrlModelListCombo.AddString(strSQL);
			m_ctrlModelListCombo.SetItemData(nCount++, DWORD(lModelPK));
			rs.MoveNext();
		}	
		rs.Close();
	}
	catch (CDaoException *e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);	
		e->Delete();
	}

	m_ctrlModelListCombo.SetCurSel(nDefaultIndex);

	//////////////////////////////////////////////////////////////////////////
	//공정 선택 Combo 초기화
	//////////////////////////////////////////////////////////////////////////
	strSQL = "SELECT TestType, TestTypeName FROM TestType ORDER BY TestType";
	nCount = 0;
	nDefaultIndex = 2;
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue(1);
			m_ctrlProcTypeCombo.AddString((LPCTSTR)data.pbVal);

			data = rs.GetFieldValue(0);
			if(m_lTestTypeID == data.lVal) nDefaultIndex = nCount;

			m_ctrlProcTypeCombo.SetItemData(nCount++, data.lVal);
			rs.MoveNext();
		}
		rs.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();	
	}
	db.Close();

	//Formatin 에선 기본적으로 공정 Type을 1개 선택한다. cf. Cycler에서는 공정 Type이 지정되면 안됨 
#ifdef _CYCLER_
	GetDlgItem(IDC_TEST_NAME_EDIT)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_PROC_TYPE_COMBO)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RESET_PROC_CHECK)->ShowWindow(SW_HIDE);
#else
//////////////////////////////////////////////////////////////////////////
		m_ctrlProcTypeCombo.SetCurSel(nDefaultIndex);
		m_lTestTypeID = m_ctrlProcTypeCombo.GetItemData(nDefaultIndex);
		int nIndex = m_ctrlProcTypeCombo.GetCurSel();
		if(nIndex != LB_ERR)
		{	
			m_ctrlProcTypeCombo.GetLBText(nIndex, m_strName);
		}

	#ifdef _AUTO_PROCESS_
		GetDlgItem(IDC_TEST_NAME_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PROC_TYPE_COMBO)->ShowWindow(SW_SHOW);
	#else
		GetDlgItem(IDC_TEST_NAME_EDIT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_PROC_TYPE_COMBO)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RESET_PROC_CHECK)->ShowWindow(SW_HIDE);
	#endif

#endif
	
	UpdateData(FALSE);
	
	((CEdit *)GetDlgItem(IDC_TEST_NAME_EDIT))->SetSel(0, -1);
	GetDlgItem(IDC_TEST_NAME_EDIT)->SetFocus();
	GetDlgItem(IDC_DATE_TIME_STATIC)->SetWindowText(m_createdDate.Format());
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTestNameDlg::OnChangeTestNameEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow();
}

void CTestNameDlg::OnChangeTestDescripEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow();
	
}

void CTestNameDlg::OnChangeTestCreatorEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow();	
}

void CTestNameDlg::OnSelchangeModelSelCombo() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDOK)->EnableWindow();	
	
}

void CTestNameDlg::OnSelchangeProcTypeCombo() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int nIndex = m_ctrlProcTypeCombo.GetCurSel();
	if(nIndex != LB_ERR)
	{	
		m_lTestTypeID = m_ctrlProcTypeCombo.GetItemData(nIndex);
		m_ctrlProcTypeCombo.GetLBText(nIndex, m_strName);
		UpdateData(FALSE);
	}
}