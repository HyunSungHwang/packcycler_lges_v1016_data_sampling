// Dlg_Chiller.cpp : implementation file
//
//$1013
#include "stdafx.h"
#include "CTSEditorPro.h"
#include "Dlg_Chiller.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chiller dialog


Dlg_Chiller::Dlg_Chiller(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_Chiller::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_Chiller::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_Chiller::IDD3):
	(Dlg_Chiller::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_Chiller)
	//}}AFX_DATA_INIT

	m_pDoc=NULL;
	m_pStep=NULL;
	m_pParent=pParent;
}


void Dlg_Chiller::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Chiller)
	DDX_Control(pDX, IDC_EDIT_CHILLER_REF_PUMP_SET, m_EditChillerRefPumpSet);
	DDX_Control(pDX, IDC_CHK_PUMP_ON_TEMP2, m_ChkPumpOnTemp2);
	DDX_Control(pDX, IDC_CHK_PUMP_ON_TEMP1, m_ChkPumpOnTemp1);
	DDX_Control(pDX, IDC_CHK_PUMP_OFF_TEMP2, m_ChkPumpOffTemp2);
	DDX_Control(pDX, IDC_CHK_PUMP_OFF_TEMP1, m_ChkPumpOffTemp1);
	DDX_Control(pDX, IDC_EDIT_CHILLER_REF_PUMP, m_EditChillerRefPump);
	DDX_Control(pDX, IDC_CHK_CHILLER_ON_TEMP2, m_ChkChillerOnTemp2);
	DDX_Control(pDX, IDC_CHK_CHILLER_ON_TEMP1, m_ChkChillerOnTemp1);
	DDX_Control(pDX, IDC_CHK_CHILLER_OFF_TEMP2, m_ChkChillerOffTemp2);
	DDX_Control(pDX, IDC_CHK_CHILLER_OFF_TEMP1, m_ChkChillerOffTemp1);
	DDX_Control(pDX, IDC_CBO_CHILLER_TPDATA, m_CboChillerTpData);
	DDX_Control(pDX, IDC_CBO_CHILLER_PUMP_CMD, m_CboChillerPumpCmd);
	DDX_Control(pDX, IDC_CBO_CHILLER_REF_CMD, m_CboChillerRefCmd);
	DDX_Control(pDX, IDC_EDIT_CHILLER_REF_TEMP, m_EditChillerRefTemp);
	DDX_Control(pDX, IDC_EDIT_CHILLER_ON_TEMP2, m_EditChillerOnTemp2);
	DDX_Control(pDX, IDC_EDIT_CHILLER_ON_TEMP1, m_EditChillerOnTemp1);
	DDX_Control(pDX, IDC_EDIT_CHILLER_OFF_TEMP2, m_EditChillerOffTemp2);
	DDX_Control(pDX, IDC_EDIT_CHILLER_OFF_TEMP1, m_EditChillerOffTemp1);
	DDX_Control(pDX, IDC_CHK_CHILLER_USE, m_ChkChillerUse);
	DDX_Control(pDX, IDC_STATIC_STEPNO, m_StaticStepNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Chiller, CDialog)
	//{{AFX_MSG_MAP(Dlg_Chiller)
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_CHK_CHILLER_USE, OnChkChillerUse)
	ON_EN_CHANGE(IDC_EDIT_CHILLER_REF_TEMP, OnChangeEditChillerRefTemp)
	ON_EN_CHANGE(IDC_EDIT_CHILLER_ON_TEMP1, OnChangeEditChillerOnTemp1)
	ON_EN_CHANGE(IDC_EDIT_CHILLER_ON_TEMP2, OnChangeEditChillerOnTemp2)
	ON_EN_CHANGE(IDC_EDIT_CHILLER_OFF_TEMP1, OnChangeEditChillerOffTemp1)
	ON_EN_CHANGE(IDC_EDIT_CHILLER_OFF_TEMP2, OnChangeEditChillerOffTemp2)
	ON_CBN_SELCHANGE(IDC_CBO_CHILLER_REF_CMD, OnSelchangeCboChillerRefCmd)
	ON_CBN_SELCHANGE(IDC_CBO_CHILLER_PUMP_CMD, OnSelchangeCboChillerPumpCmd)
	ON_EN_CHANGE(IDC_EDIT_CHILLER_REF_PUMP, OnChangeEditChillerRefPump)
	ON_BN_CLICKED(IDC_CHK_CHILLER_ON_TEMP1, OnChkChillerOnTemp1)
	ON_BN_CLICKED(IDC_CHK_CHILLER_ON_TEMP2, OnChkChillerOnTemp2)
	ON_BN_CLICKED(IDC_CHK_CHILLER_OFF_TEMP1, OnChkChillerOffTemp1)
	ON_BN_CLICKED(IDC_CHK_CHILLER_OFF_TEMP2, OnChkChillerOffTemp2)
	ON_CBN_SELCHANGE(IDC_CBO_CHILLER_TPDATA, OnSelchangeCboChillerTpdata)
	ON_BN_CLICKED(IDC_CHK_PUMP_ON_TEMP1, OnChkPumpOnTemp1)
	ON_BN_CLICKED(IDC_CHK_PUMP_ON_TEMP2, OnChkPumpOnTemp2)
	ON_BN_CLICKED(IDC_CHK_PUMP_OFF_TEMP1, OnChkPumpOffTemp1)
	ON_BN_CLICKED(IDC_CHK_PUMP_OFF_TEMP2, OnChkPumpOffTemp2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chiller message handlers

BOOL Dlg_Chiller::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_StaticStepNo.SetFontAlign(DT_LEFT);
	
	//m_CboChillerRefCmd.AddString(_T("사용안함"));
	m_CboChillerRefCmd.AddString(Fun_FindMsg("Dlg_Chiller_OnInitDialog_msg1","DLG_CHILLER")); //&&
	m_CboChillerRefCmd.AddString(_T("ON"));
	m_CboChillerRefCmd.AddString(_T("OFF"));	
	m_CboChillerRefCmd.SetCurSel(0);

	//m_CboChillerPumpCmd.AddString(_T("사용안함"));
	m_CboChillerPumpCmd.AddString(Fun_FindMsg("Dlg_Chiller_OnInitDialog_msg2","DLG_CHILLER")); //&&
	m_CboChillerPumpCmd.AddString(_T("ON"));
	m_CboChillerPumpCmd.AddString(_T("OFF"));	
	m_CboChillerPumpCmd.SetCurSel(0);
	
	m_CboChillerTpData.AddString(_T("Aux"));
	m_CboChillerTpData.AddString(_T("Can"));	
	m_CboChillerTpData.SetCurSel(0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_Chiller::OnOK() 
{
	//CDialog::OnOK();
}

void Dlg_Chiller::OnCancel() 
{
	//CDialog::OnCancel();
}

void Dlg_Chiller::OnClose() 
{	
	//CDialog::OnClose();
}

BOOL Dlg_Chiller::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam==VK_RETURN ||
			pMsg->wParam==VK_ESCAPE )
		{
			ShowWindow(SW_HIDE);
			return TRUE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_Chiller::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if(!m_pDoc) return;	
	if(m_nStepNo < 1) return;
	
	CString strTemp;
	
	if(bShow==TRUE)
	{
		strTemp.Format(_T("Step : %3d"), m_nStepNo);
		m_StaticStepNo.SetText(strTemp);
		
		onSetObject(0);

		if(m_pStep)
		{
			m_ChkChillerUse.SetCheck(m_pStep->nChiller_Cmd);
			if(m_pStep->nChiller_Cmd==1)
			{
				onSetObject(1);
				
				strTemp.Format(_T("%.3f"),m_pStep->fChiller_RefTemp);
				m_EditChillerRefTemp.SetWindowText(strTemp);

				m_CboChillerRefCmd.SetCurSel(m_pStep->nChiller_RefCmd);

				strTemp.Format(_T("%.3f"),(m_pStep->fChiller_RefPump));
				m_EditChillerRefPump.SetWindowText(strTemp);

				float fltFactorChillPump;
				fltFactorChillPump = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "ChillPumpFactor", "1"));

				FLOAT fltTmp = 0.0; //yulee 20181226
				fltTmp = (atof(strTemp))*fltFactorChillPump; //yulee 20190121
				CString strTmp;
				strTmp.Format("%.3f", fltTmp);
				m_EditChillerRefPumpSet.SetWindowText(strTmp);

				m_CboChillerPumpCmd.SetCurSel(m_pStep->nChiller_PumpCmd);
				
				m_CboChillerTpData.SetCurSel(m_pStep->nChiller_TpData);

				strTemp.Format(_T("%.3f"),m_pStep->fChiller_ONTemp1);
				m_EditChillerOnTemp1.SetWindowText(strTemp);

				m_ChkChillerOnTemp1.SetCheck(m_pStep->nChiller_ONTemp1_Cmd);
				m_ChkPumpOnTemp1.SetCheck(m_pStep->nChiller_ONPump1_Cmd);
				
				
				strTemp.Format(_T("%.3f"),m_pStep->fChiller_ONTemp2);
				m_EditChillerOnTemp2.SetWindowText(strTemp);

				m_ChkChillerOnTemp2.SetCheck(m_pStep->nChiller_ONTemp2_Cmd);
				m_ChkPumpOnTemp2.SetCheck(m_pStep->nChiller_ONPump2_Cmd);

			
				strTemp.Format(_T("%.3f"),m_pStep->fChiller_OFFTemp1);
				m_EditChillerOffTemp1.SetWindowText(strTemp);

				m_ChkChillerOffTemp1.SetCheck(m_pStep->nChiller_OFFTemp1_Cmd);
				m_ChkPumpOffTemp1.SetCheck(m_pStep->nChiller_OFFPump1_Cmd);

			
				strTemp.Format(_T("%.3f"),m_pStep->fChiller_OFFTemp2);
				m_EditChillerOffTemp2.SetWindowText(strTemp);

				m_ChkChillerOffTemp2.SetCheck(m_pStep->nChiller_OFFTemp2_Cmd);
				m_ChkPumpOffTemp2.SetCheck(m_pStep->nChiller_OFFPump2_Cmd);

				onOndoObject(1);
				onOndoObject(2);
				onOndoObject(3);
		        onOndoObject(4);
			}
		}
	}
	else
	{
		if(m_pParent)
		{
			m_pParent->PostMessage(WM_CHILLER_DLG_CLOSE, (WPARAM)m_nStepNo, (LPARAM) this);			
		}
	}
}

void Dlg_Chiller::onSetObject(int iType) 
{
	if(iType==0)
	{
		m_EditChillerRefTemp.EnableWindow(FALSE);		
		m_CboChillerRefCmd.EnableWindow(FALSE);		
		m_CboChillerPumpCmd.EnableWindow(FALSE);		
		m_EditChillerRefPump.EnableWindow(FALSE);
		m_EditChillerRefPumpSet.EnableWindow(FALSE);
		
		m_CboChillerTpData.EnableWindow(FALSE);	
		
		m_EditChillerOnTemp1.EnableWindow(FALSE);
		m_ChkChillerOnTemp1.EnableWindow(FALSE);
		m_ChkPumpOnTemp1.EnableWindow(FALSE);
		m_EditChillerOnTemp2.EnableWindow(FALSE);
		m_ChkChillerOnTemp2.EnableWindow(FALSE);
		m_ChkPumpOnTemp2.EnableWindow(FALSE);
		
		m_EditChillerOffTemp1.EnableWindow(FALSE);
		m_ChkChillerOffTemp1.EnableWindow(FALSE);
		m_ChkPumpOffTemp1.EnableWindow(FALSE);
		m_EditChillerOffTemp2.EnableWindow(FALSE);
		m_ChkChillerOffTemp2.EnableWindow(FALSE);
		m_ChkPumpOffTemp2.EnableWindow(FALSE);
		
		m_EditChillerRefTemp.SetWindowText(_T("0"));
		m_CboChillerRefCmd.SetCurSel(0);
		m_EditChillerRefPump.SetWindowText(_T("0"));
		m_EditChillerRefPumpSet.SetWindowText(_T("0"));
		m_CboChillerTpData.SetCurSel(0);
		m_EditChillerOnTemp1.SetWindowText(_T("0"));
		m_ChkChillerOnTemp1.SetCheck(FALSE);
		m_ChkPumpOnTemp1.SetCheck(FALSE);
		m_EditChillerOnTemp2.SetWindowText(_T("0"));
		m_ChkChillerOnTemp2.SetCheck(FALSE);
		m_ChkPumpOnTemp2.SetCheck(FALSE);
		m_EditChillerOffTemp1.SetWindowText(_T("0"));
		m_ChkChillerOffTemp1.SetCheck(FALSE);
		m_ChkPumpOffTemp1.SetCheck(FALSE);
		m_EditChillerOffTemp2.SetWindowText(_T("0"));
		m_ChkChillerOffTemp2.SetCheck(FALSE);
		m_ChkPumpOffTemp2.SetCheck(FALSE);
	}
	else if(iType==1)
	{
		m_EditChillerRefTemp.EnableWindow(TRUE);		
		m_CboChillerRefCmd.EnableWindow(TRUE);		
		m_CboChillerPumpCmd.EnableWindow(TRUE);		
		m_EditChillerRefPump.EnableWindow(TRUE);
		m_EditChillerRefPumpSet.EnableWindow(TRUE);
		
		m_CboChillerTpData.EnableWindow(TRUE);	
		
		m_EditChillerOnTemp1.EnableWindow(FALSE);
		m_ChkChillerOnTemp1.EnableWindow(TRUE);/////
		m_ChkPumpOnTemp1.EnableWindow(TRUE);///////
		m_EditChillerOnTemp2.EnableWindow(FALSE);
		m_ChkChillerOnTemp2.EnableWindow(TRUE);/////
		m_ChkPumpOnTemp2.EnableWindow(TRUE);/////
		
		m_EditChillerOffTemp1.EnableWindow(FALSE);
		m_ChkChillerOffTemp1.EnableWindow(TRUE);/////
		m_ChkPumpOffTemp1.EnableWindow(TRUE);/////
		m_EditChillerOffTemp2.EnableWindow(FALSE);
		m_ChkChillerOffTemp2.EnableWindow(TRUE);/////
		m_ChkPumpOffTemp2.EnableWindow(TRUE);/////
		
		m_EditChillerRefTemp.SetWindowText(_T("0"));
		m_CboChillerRefCmd.SetCurSel(0);
		m_EditChillerRefPump.SetWindowText(_T("0"));
		m_EditChillerRefPumpSet.SetWindowText(_T("0"));
		m_CboChillerTpData.SetCurSel(0);
		m_EditChillerOnTemp1.SetWindowText(_T("0"));
		m_ChkChillerOnTemp1.SetCheck(FALSE);
		m_ChkPumpOnTemp1.SetCheck(FALSE);
		m_EditChillerOnTemp2.SetWindowText(_T("0"));
		m_ChkChillerOnTemp2.SetCheck(FALSE);
		m_ChkPumpOnTemp2.SetCheck(FALSE);
		m_EditChillerOffTemp1.SetWindowText(_T("0"));
		m_ChkChillerOffTemp1.SetCheck(FALSE);
		m_ChkPumpOffTemp1.SetCheck(FALSE);
		m_EditChillerOffTemp2.SetWindowText(_T("0"));
		m_ChkChillerOffTemp2.SetCheck(FALSE);
		m_ChkPumpOffTemp2.SetCheck(FALSE);
	}
}

void Dlg_Chiller::onOndoObject(int iType) 
{
	if(iType==1)
	{
		if( m_ChkChillerOnTemp1.GetCheck()==TRUE ||
			m_ChkPumpOnTemp1.GetCheck()==TRUE )
		{
			m_EditChillerOnTemp1.EnableWindow(TRUE);
		}
		else
		{
			m_EditChillerOnTemp1.EnableWindow(FALSE);
			m_EditChillerOnTemp1.SetWindowText(_T("0"));
		}
	}
	else if(iType==2)
	{
		if( m_ChkChillerOnTemp2.GetCheck()==TRUE ||
			m_ChkPumpOnTemp2.GetCheck()==TRUE )
		{
			m_EditChillerOnTemp2.EnableWindow(TRUE);
		}
		else
		{
			m_EditChillerOnTemp2.EnableWindow(FALSE);
			m_EditChillerOnTemp2.SetWindowText(_T("0"));
		}
	}
	else if(iType==3)
	{
		if( m_ChkChillerOffTemp1.GetCheck()==TRUE ||
			m_ChkPumpOffTemp1.GetCheck()==TRUE )
		{
			m_EditChillerOffTemp1.EnableWindow(TRUE);
		}
		else
		{
			m_EditChillerOffTemp1.EnableWindow(FALSE);
			m_EditChillerOffTemp1.SetWindowText(_T("0"));
		}
	}
	else if(iType==4)
	{
		if( m_ChkChillerOffTemp2.GetCheck()==TRUE ||
			m_ChkPumpOffTemp2.GetCheck()==TRUE )
		{
			m_EditChillerOffTemp2.EnableWindow(TRUE);
		}
		else
		{
			m_EditChillerOffTemp2.EnableWindow(FALSE);
			m_EditChillerOffTemp2.SetWindowText(_T("0"));
		}
	}
}


void Dlg_Chiller::OnChkChillerUse() 
{
	UpdateData(TRUE);
	
	BOOL bUse=m_ChkChillerUse.GetCheck();
	if(m_pStep)
	{
		m_pStep->nChiller_Cmd=bUse;
	}

	if(bUse==TRUE) onSetObject(1);
	else           onSetObject(0);
	
	UpdateData(FALSE);
}


void Dlg_Chiller::OnSelchangeCboChillerRefCmd() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		int nCurSel=m_CboChillerRefCmd.GetCurSel();
		if(m_pStep)
		{
			m_pStep->nChiller_RefCmd=nCurSel;
		}
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChangeEditChillerRefTemp() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		CString strValue=theApp.win_GetText(&m_EditChillerRefTemp);
		if(strValue.IsEmpty()==FALSE && strValue!=_T(""))
		{
			if(m_pStep)
			{
				m_pStep->fChiller_RefTemp=atof(strValue);
			}
		}
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnSelchangeCboChillerPumpCmd() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		int nCurSel=m_CboChillerPumpCmd.GetCurSel();
		if(m_pStep)
		{
			m_pStep->nChiller_PumpCmd=nCurSel;
		}
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChangeEditChillerRefPump() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		CString strValue=theApp.win_GetText(&m_EditChillerRefPump);
		if(strValue.IsEmpty()==FALSE && strValue!=_T(""))
		{
			if(m_pStep)
			{
				float fltFactorChillPump;
				fltFactorChillPump = atof(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "ChillPumpFactor", "1"));

				m_pStep->fChiller_RefPump=atof(strValue);
				CString StrTmp;
				StrTmp.Format(_T("%.3f"), (atof(strValue))*fltFactorChillPump); //yulee 20190121
				m_EditChillerRefPumpSet.SetWindowText(StrTmp);
			}
		}
	}
	UpdateData(FALSE);
}


void Dlg_Chiller::OnSelchangeCboChillerTpdata() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		int nCurSel=m_CboChillerTpData.GetCurSel();
		if(m_pStep)
		{
			m_pStep->nChiller_TpData=nCurSel;
		}
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChangeEditChillerOnTemp1() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		CString strValue=theApp.win_GetText(&m_EditChillerOnTemp1);
		if(strValue.IsEmpty()==FALSE && strValue!=_T(""))
		{
			if(m_pStep)
			{
				m_pStep->fChiller_ONTemp1=atof(strValue);
			}
		}
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChkChillerOnTemp1() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkChillerOnTemp1.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_ONTemp1_Cmd=bflag;
		}
		onOndoObject(1);
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChangeEditChillerOnTemp2() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		CString strValue=theApp.win_GetText(&m_EditChillerOnTemp2);
		if(strValue.IsEmpty()==FALSE && strValue!=_T(""))
		{
			if(m_pStep)
			{
				m_pStep->fChiller_ONTemp2=atof(strValue);
			}
		}
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChkChillerOnTemp2() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkChillerOnTemp2.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_ONTemp2_Cmd=bflag;
		}
		onOndoObject(2);
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChangeEditChillerOffTemp1() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		CString strValue=theApp.win_GetText(&m_EditChillerOffTemp1);
		if(strValue.IsEmpty()==FALSE && strValue!=_T(""))
		{
			if(m_pStep)
			{
				m_pStep->fChiller_OFFTemp1=atof(strValue);
			}
		}
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChkChillerOffTemp1() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkChillerOffTemp1.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_OFFTemp1_Cmd=bflag;
		}
		onOndoObject(3);
	}
	UpdateData(FALSE);
	
}

void Dlg_Chiller::OnChangeEditChillerOffTemp2() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		CString strValue=theApp.win_GetText(&m_EditChillerOffTemp2);
		if(strValue.IsEmpty()==FALSE && strValue!=_T(""))
		{
			if(m_pStep)
			{
				m_pStep->fChiller_OFFTemp2=atof(strValue);
			}
		}
	}
	UpdateData(FALSE);
}


void Dlg_Chiller::OnChkChillerOffTemp2() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkChillerOffTemp2.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_OFFTemp2_Cmd=bflag;
		}
		onOndoObject(4);
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChkPumpOnTemp1() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkPumpOnTemp1.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_ONPump1_Cmd=bflag;
		}
		onOndoObject(1);
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChkPumpOnTemp2() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkPumpOnTemp2.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_ONPump2_Cmd=bflag;
		}
		onOndoObject(2);
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChkPumpOffTemp1() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkPumpOffTemp1.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_OFFPump1_Cmd=bflag;
		}
		onOndoObject(3);
	}
	UpdateData(FALSE);
}

void Dlg_Chiller::OnChkPumpOffTemp2() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bflag=m_ChkPumpOffTemp2.GetCheck();
		if(m_pStep)
		{
			m_pStep->nChiller_OFFPump2_Cmd=bflag;
		}
		onOndoObject(4);
	}
	UpdateData(FALSE);
}