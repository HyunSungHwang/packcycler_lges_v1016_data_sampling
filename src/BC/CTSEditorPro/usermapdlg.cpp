// CUserMapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "UserMapDlg.h"
#include "TextParsing.h"
#include "RealEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserMapDlg dialog

#define  USER_MAP_SIZE  22			// user map 그리드 컬럼 사이즈
#define  USER_MAP_CONFIG_SIZE 3		//설정값 크기
#define  USER_MAP_CHARGE 0			//충전
#define  USER_MAP_DISCHARGE 1			//방전

// user map 그리드 컬럼 순서.
typedef enum UserMapColumn
{
	SOC0 = 0,SOC1,SOC2,SOC3,SOC4,SOC5,SOC6,
	SOC7,SOC8,SOC9,SOC10,SOC11,SOC12,
	SOC13,SOC14,SOC15,SOC16,SOC17,SOC18,
	SOC19,SOC20,SOC21
};


CUserMapDlg::CUserMapDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CUserMapDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CUserMapDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CUserMapDlg::IDD3):
	(CUserMapDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CUserMapDlg)
	m_CtrlLabFileName = _T("");
	m_ChkDynamicTime = -1;
	m_ChkDynamicMode_P = -1;
	m_ChkCharge = -1;
	//}}AFX_DATA_INIT
}


void CUserMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserMapDlg)
	DDX_Control(pDX, IDC_DATETIMEPICKER_EDNTIME, m_CtrlEndTime);
	DDX_Text(pDX, IDC_STATIC_FILE_NAME, m_CtrlLabFileName);
	DDX_Radio(pDX, IDC_RADIO_DYNAMIC_TIME, m_ChkDynamicTime);
	DDX_Radio(pDX, IDC_RADIO_DYNAMIC_MODE_P, m_ChkDynamicMode_P);
	DDX_Radio(pDX, IDC_RADIO_CHARGE, m_ChkCharge);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserMapDlg, CDialog)
	//{{AFX_MSG_MAP(CUserMapDlg)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_OPEN, OnButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_AS, OnButtonSaveAs)
	ON_BN_CLICKED(IDC_RADIO_DYNAMIC_TIME, OnRadioDynamicTime)
	ON_BN_CLICKED(IDC_RADIO_CUMULATIVE_TIME, OnRadioCumulativeTime)
	ON_BN_CLICKED(IDC_RADIO_DYNAMIC_MODE_P, OnRadioDynamicModeP)
	ON_BN_CLICKED(IDC_RADIO_DYNAMIC_MODE_I, OnRadioDynamicModeI)
	ON_BN_CLICKED(IDC_RADIO_CHARGE, OnRadioCharge)
	ON_BN_CLICKED(IDC_RADIO_DISCHARGE, OnRadioDischarge)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserMapDlg message handlers
/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	클래스 초기화
-- Author		:	이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CtrlEndTime.SetTime(DATE(0));			//2014.09.15
	m_CtrlEndTime.SetFormat("HH:mm:ss");	//2014.09.15

	m_ChkDynamicMode_P	= 0;		//2014.09.15
	m_ChkDynamicTime	= 0;		//2014.09.15
	m_ChkCharge			= 0;
	
	IsChange = FALSE; //유저맵 폼에서 신규열기 확인.

	//그리드를 초기화 합니다
	InitUserMapGrid();

	if(m_strUserMapFile.IsEmpty()){
		m_strNewUserMapFile = SelectUserMapFile(m_strUserMapFile);

		if(m_strNewUserMapFile.IsEmpty())	//파일이 등록되지 않았다면.
		{
			OnCancel();				//종료
		}
		else						//패턴은 그래프 그리기 기능이 있음 그래프 그리기 기능은 빠짐
		{			
			IsChangeSelFile = TRUE;
			CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
			CString strSrc = strDir + "\\" + m_strNewUserMapFile;			
			m_strNewUserMapFile = strSrc;			//파일정보로 절대 경로를 갖는다.	
			
		}
	}else{
		//Title를 찾아서 화면에 보여준다.//////
		IsChangeSelFile = FALSE;

		m_CtrlLabFileName = m_strUserMapFile;
		CString strFile(m_strUserMapFile);
		int nStart = strFile.Find('-');
		int nEnd = strFile.ReverseFind('.');
		
		if(nStart >= 0 && nEnd >= 0 && nStart < nEnd)
		{
			m_strDataTitle = strFile.Mid(nStart+1, nEnd-nStart-1);
		}
			
		UpdateData(FALSE);

		if(LoadData(m_strUserMapFile) == TRUE){
			
		}//파일을 읽어들인다.
	}
	
	
	
	return TRUE; 
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	그리드 초기화.
-- Author		:	이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::InitUserMapGrid()
{	
	CString strColName;	
	CRect rect;
	
	m_wndUserMapGrid.SubclassDlgItem(IDC_USER_MAP_GRID,this);
	m_wndUserMapGrid.Initialize();
	
	m_wndUserMapGrid.GetParam()->EnableUndo(FALSE);
	m_wndUserMapGrid.GetParam()->EnableMoveRows(FALSE);		//Drag and Drop 지원

	m_wndUserMapGrid.SetColCount(USER_MAP_SIZE - 1); //기본 사이즈 01	
	m_wndUserMapGrid.GetClientRect(&rect);
	//m_wndUserMapGrid.SetRowCount(1);

	m_wndUserMapGrid.SetColWidth(SOC0, SOC21, 50);
	
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC0),CGXStyle().SetValue("map"));
	strColName.Format("%d",SOC0 * 5);	
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC1),CGXStyle().SetValue(strColName));	
	strColName.Format("%d",SOC1 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC2),CGXStyle().SetValue(strColName));	
	strColName.Format("%d",SOC2 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC3),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC3 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC4),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC4 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC5),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC5 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC6),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC6 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC7),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC7 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC8),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC8 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC9),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC9 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC10),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC10 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC11),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC11 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC12),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC12 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC13),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC13 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC14),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC14 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC15),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC15 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC16),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC16 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC17),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC17 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC18),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC18 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC19),CGXStyle().SetValue(strColName));
	strColName.Format("%d",SOC19 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC20),CGXStyle().SetValue(strColName));	
	strColName.Format("%d",SOC20 * 5);
	m_wndUserMapGrid.SetStyleRange(CGXRange(0,SOC21),CGXStyle().SetValue(strColName));	
	
	m_wndUserMapGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width	
	
	m_wndUserMapGrid.SetStyleRange(CGXRange().SetCols(SOC0,SOC21),CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetHorizontalAlignment(DT_CENTER));	
	
	m_wndUserMapGrid.SetStyleRange(CGXRange().SetCols(SOC0, SOC21), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));
	
	return TRUE;
}


/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	Usermap 파일을 선택 한다.
-- Author		:	이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
CString CUserMapDlg::SelectUserMapFile(CString strFile)
{

	CString strOldFile = strFile;
	CString strSelFile;
	
	//CFileDialog dlg(TRUE, "", strSelFile, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv 파일(*.csv)|*.csv|All Files (*.*)|*.*|");
	CFileDialog dlg(TRUE, "", strSelFile, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv File(*.csv)|*.csv|All Files (*.*)|*.*|");//&&
	if(dlg.DoModal() == IDOK)
	{
		CString strFilePath = dlg.GetPathName();
		
		CString strFileName = strFilePath.Mid(0,strFilePath.ReverseFind('.'));
				
		m_strDataTitle = GetFileName(strFileName);

		if(strFileName.GetLength() >= 112)
		{
			AfxMessageBox(Fun_FindMsg("SelectUserMapFile_msg1","IDD_USER_MAP_DLG"));//"파일명을 영문 112글자(한글56자) 이내로 지정하십시요."
			return strOldFile;
		}				
		
		m_CtrlLabFileName = strFilePath;
		
		UpdateData(FALSE);  
		
		if(LoadData(strFilePath) == FALSE)	return strOldFile;	
				
		CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));						
		
		CTime ct = CTime::GetCurrentTime();
		strSelFile.Format("%s.tmp", ct.Format("%Y%m%d%H%M%S"));	
				
		if(CopyFile(strFilePath, strDir+"\\"+strSelFile, TRUE) == FALSE)
		{
			AfxMessageBox(strFilePath+Fun_FindMsg("SelectUserMapFile_msg2","IDD_USER_MAP_DLG"));//" Data를 지정할 수 없습니다.(중복 파일)"
			return strOldFile;
		}
			
		return strSelFile;
	}
	else{
		return strOldFile;
	}	
}

BOOL CUserMapDlg::SetDataSheet(CString strFile)
{
	return m_SheetData.SetUserMapFile(strFile, m_strErrorMsg);
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	파일을 읽어 설정 및 유효성 검사.
-- Author		:	이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::LoadData(CString strFile)
{	
	//기존 패턴 데이터는 그래프그리기 기능이 존재함 
	//SetDataSheet 함수를 그대로 구현.
	if(SetDataSheet(strFile))
	{
		//1. 파일 읽기 및 파일 유효성 체크
		if(m_SheetData.GetColumnSize() != USER_MAP_SIZE)
		{
			AfxMessageBox(Fun_FindMsg("LoadData_msg1","IDD_USER_MAP_DLG"));//"파일 형식이 유효하지 않습니다."
			return FALSE;
		}					
		if(!SetGridReadFile(m_SheetData.m_strFileName)){
			AfxMessageBox(Fun_FindMsg("LoadData_msg1","IDD_USER_MAP_DLG"));//"파일 형식이 유효하지 않습니다."
			return FALSE;
		}
	}
	else
	{
		AfxMessageBox(m_strErrorMsg);

		SelectUserMapFile("");		
	}	
	return TRUE;
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	String 구분자로 잘라 리스트로 반환.
-- Author		:	이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
CStringList& CUserMapDlg::StringSplit(CString string,CString separation,CStringList& array)
{	
	array.RemoveAll();
	while (string.GetLength() > 0)
	{
		int Pos = string.Find(separation);
		if (Pos != -1)
		{				
			CString s = string.Left(Pos);
			array.AddTail(s);
			string = string.Mid(Pos + 1);
		}
		else
		{
			array.AddTail(string);
			break;
		}
	}
	return array;
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	파일을 읽어 그리드로 그림.
-- Author		:	이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::SetGridReadFile(CString strFile)
{
	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;
	CStringList strColList;
	CStringList strTitleList;
	BOOL bReadHead = FALSE;
	BOOL bStartRead = FALSE;
	BOOL bTimeHead = FALSE;
	BOOL bConfigHead = FALSE;
				
	if( !aFile.Open( strFile, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		str.Format(Fun_FindMsg("SetGridReadFile_msg1","IDD_USER_MAP_DLG"), strFile);//"%s 파일을 열 수 없습니다."
		AfxMessageBox(str);
		return FALSE;
	}
	
	//그리드 Row 삭제.
	if(m_wndUserMapGrid.GetRowCount() > 0){		
		m_wndUserMapGrid.RemoveRows(1,m_wndUserMapGrid.GetRowCount());
	}
	
	//csv 파일을 읽어 그리드에 출력한다.
	while(aFile.ReadString(strTemp))
	{
		//마지막줄.
		if(strTemp.Left(3) == END_SIMULATION_POINT){
			break;
		}		
		
		//헤더 파일 및 시간 설정을 읽어드린다.
		if(bStartRead && !bReadHead){			
			if(strTemp == ""){
				AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg2","IDD_USER_MAP_DLG"));//"파일 타이틀 데이터가 존재 하지않습니다."
				return -1;
			}
						
			StringSplit(strTemp,",",strTitleList);										
						
			//해더 컬럼을 설정한다.
			POSITION pos = strTitleList.GetHeadPosition();
			int i = 0;
			
			while(pos != NULL){
				CString string = strTitleList.GetNext(pos);		

				if(string.IsEmpty()){
					AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg3","IDD_USER_MAP_DLG"));//"파일 데이터 타이틀이 올바르지 않습니다."
					return -1;
				}

				if(i == 0){
					if(string == "TIME"){ //시간 값라인인지 체크. 그냥 한줄로 하지..
						bTimeHead = TRUE;
					}else if(string == "T1" || string == "T2"){
						bConfigHead = TRUE; //설정값 라인.						
					}
				}
				//시간 설정 라인인경우
				if(bTimeHead){					
					//1데이터는 고정 2번째 시간설정만 셋업한다.							
					if(i == 1){						
						COleDateTime EndTime;
						EndTime.ParseDateTime(string);						
						m_CtrlEndTime.SetTime(EndTime);
						m_CtrlEndTime.SetFormat("HH:mm:ss");	
						bTimeHead = FALSE;
						break;
					}	
					i++;
				}else if(bConfigHead){ //설정값 라인인경우.
					//설정값은 고정으로 사용하구있다. 차후 구현.
					//T1,I 
					if(i == 1){												
						bConfigHead = FALSE;
						break;
					}	
					i++;					
				}else
				{
					if(strTitleList.GetCount() != USER_MAP_SIZE){
						AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg3","IDD_USER_MAP_DLG"));//"파일 타이틀 데이터가 올바르지 않습니다."
						return -1;
					}

					bReadHead = TRUE;
					//그리드 해더구간.
					m_wndUserMapGrid.SetStyleRange(CGXRange(0,i),CGXStyle().SetValue(string));						
					i++;
				}	
				
			}	
		}else if(bStartRead && bReadHead){	
			
			if(strTemp == ""){
				AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg2","IDD_USER_MAP_DLG"));//"파일 데이터가 존재 하지않습니다."
				return -1;
			}

			StringSplit(strTemp,",",strColList);

			if(strColList.GetCount() != USER_MAP_SIZE){
				AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg3","IDD_USER_MAP_DLG"));//"파일 데이터가 올바르지 않습니다."
				return -1;
			}

			int nRow = m_wndUserMapGrid.GetRowCount() + 1;

			POSITION pos = strColList.GetHeadPosition();
			int i = 0;
			m_wndUserMapGrid.InsertRows(nRow, 1); //1행 추가.
			while(pos != NULL){
				CString string = strColList.GetNext(pos);
				if(string.IsEmpty()){
					AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg4","IDD_USER_MAP_DLG"));//"파일 데이터 값이 올바르지 않습니다."
					return -1;
				}
				m_wndUserMapGrid.SetValueRange(CGXRange(nRow, i), string);
				i++;
			}						
		}
		
		if(strTemp.Left(3) == START_SIMULATION_POINT) //STX 후부터 읽어드린다.
		{			
			bStartRead = TRUE;			
		}					

	}		
	
	aFile.Close();

	int nColCnt = m_wndUserMapGrid.GetColCount();

	if(nColCnt > USER_MAP_SIZE){
		AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg5","IDD_USER_MAP_DLG"));//"파일 타이틀 설정이 잘못되었습니다."
		return -1;
	}
	
	int nRow = m_wndUserMapGrid.GetRowCount() + 1;
	if(nRow > USER_MAP_SIZE|| nRow < USER_MAP_SIZE){		
		m_wndUserMapGrid.RemoveRows(1,nRow);		
		AfxMessageBox(Fun_FindMsg("SetGridReadFile_msg6","IDD_USER_MAP_DLG"));//"파일 데이터 Row가 올바르지 않습니다."
		return -1;
	}

	return TRUE;
}

/*
---------------------------------------------------------
---------
-- Filename		: usermapdlg.cpp 
-- Description	: 그리드 내용을 파일로 저장 
				  파일 저장시 설정 정보도 저장한다.
				  STS ->시작점
				  T1 -> 설정정보
				  map -> 맵 정보 
				  순으로 저장한다. csv파일을 참조 한다.
-- Author		: 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::SaveUserMapFile(CString strFilePath)
{	
	if(m_strUserMapFile.IsEmpty() && m_strNewUserMapFile.IsEmpty()){
		return FALSE;
	}
	
	//유효성 검사.
	if(!SaveValidation()){
		return FALSE;
	}


	CString strSaveData;
	CString strTemp,strTemp2;
	int nRowCnt,nColCnt;	

	strSaveData= "";
	nRowCnt = m_wndUserMapGrid.GetRowCount() + 1;
	nColCnt = m_wndUserMapGrid.GetColCount() + 1;

	if(!m_strUserMapFile.IsEmpty()){
		strTemp = m_strUserMapFile;
	}else if(!m_strNewUserMapFile.IsEmpty()){
		strTemp = m_strNewUserMapFile;	
	}

	//다른이름 저장시 사용.
	if(!strFilePath.IsEmpty()){
		strTemp = strFilePath;
	}

	FILE *fp = fopen(strTemp, "wt");

	if(fp != NULL)
	{
		//주석 save
		strSaveData = Fun_FindMsg("SaveUserMapFile_msg1","IDD_USER_MAP_DLG");//"여기는 주석 구문이 위치하는 곳입니다. 어떤 내용을 쓰던지 동작과는 전혀 상관없습니다.\n";
		strSaveData += Fun_FindMsg("SaveUserMapFile_msg2","IDD_USER_MAP_DLG");//"아래 ""STX"" 로 부터는 실제로 Simulation 을 수행하는 데이터입니다.\n";
		strSaveData += Fun_FindMsg("SaveUserMapFile_msg3","IDD_USER_MAP_DLG");//"T1(누적시간),T2(동작시간),I(전류),P(파워)\n";		
		strSaveData += Fun_FindMsg("SaveUserMapFile_msg4","IDD_USER_MAP_DLG");//"수정시에는 포맷에 맞추어 작성하시기 바랍니다.,\n\n";
		strSaveData += "STX\n"; //여기 다음부터 실제 데이터.
					
		fprintf(fp, "%s", strSaveData);	
		strSaveData.Empty();

		UpdateData();

		CString strTime;
		COleDateTime endTime;
		m_CtrlEndTime.GetTime(endTime);
		strTime.Format("%02d:%02d:%02d",endTime.GetHour(),endTime.GetMinute(),endTime.GetSecond());
		
		strSaveData.Format("TIME,%s\n",strTime);
				
		fprintf(fp, "%s", strSaveData);
		strSaveData.Empty();

		strSaveData = "T2,P\n";
		
		fprintf(fp, "%s", strSaveData);
		strSaveData.Empty();
		
		int Val = 0;		
		
		//data save
		for (int i=0; i < nRowCnt ; i++)
		{
			for (int j=0;j < nColCnt; j++)
			{							
				strTemp.Format("%s,",m_wndUserMapGrid.GetValueRowCol(i, j));				
				strSaveData += strTemp;
			}
			strSaveData += "\n";
		}

		fprintf(fp, "%s", strSaveData);	//File Write
		
		fprintf(fp, "%s", "EOF");	//끝을 알림.

		fclose(fp);
		fp = NULL;
	}
	else
	{
		strTemp.Format("File Save error (%s)",m_strUserMapFile);
		AfxMessageBox(strTemp);
		CDialog::OnCancel();
	}
	fp = NULL;	

	return TRUE;
}

/*
---------------------------------------------------------
---------
-- Filename		: usermapdlg.cpp 
-- Description	: 파일 적용.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnButtonSave() 
{		
	if(SaveUserMapFile()){
		if(!CreateUserMapFile()){
			return;
		}

		UpdateData();

		if(m_ChkDynamicMode_P == 0){
			m_chMode = PS_MODE_CP;
		}else if(m_ChkDynamicMode_P == 1) {
			m_chMode = PS_MODE_CC;
		}
		
		CDialog::OnOK();
	}else{
		AfxMessageBox(Fun_FindMsg("OnButtonSave_msg1","IDD_USER_MAP_DLG"));		 //"파일 저장에 실패 하였습니다."
	}
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::CreateUserMapFile()
{
	//파일명 최종 Update한다.
	CString strUsermapFile;
	CString strSrc;
	CString strOldUserMapFile;
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));	
			
	if(IsChangeSelFile)				//파일을 새로 등록했다면
	{
		int nStart = m_strNewUserMapFile.ReverseFind('\\')+1;
		int nEnd = m_strNewUserMapFile.ReverseFind('.') - nStart;
		strUsermapFile.Format("%s-%s.csv",
			m_strNewUserMapFile.Mid(nStart,nEnd),m_strDataTitle);
		
		//이전 파일 삭제
		if(m_strUserMapFile != "")
		{		
			CFileFind aFind;
			BOOL bFind = aFind.FindFile(m_strUserMapFile);
			while(bFind)
			{
				bFind = aFind.FindNextFile();
				DeleteFile(aFind.GetFilePath());
			}
		}
		
		m_strUserMapFile = m_strNewUserMapFile;		//다시 등록*/
		
	}
	else
	{
		strOldUserMapFile = m_strUserMapFile;		
		CTime ct = CTime::GetCurrentTime();
		strUsermapFile.Format("%s-%s.csv", ct.Format("%Y%m%d%H%M%S"),  m_strDataTitle);
	}

	strSrc = m_strUserMapFile;
	m_strUserMapFile = strDir + "\\" + strUsermapFile;

	int nFile= 2;
	while(rename(strSrc, m_strUserMapFile) !=0)
	{
		CString strFileNum;
		strFileNum.Format("(%d)", nFile);
		m_strUserMapFile.Insert(m_strUserMapFile.ReverseFind('.'), strFileNum);
	}
	
	DeleteTempFile();
	
	//m_strUserMapFile = GetFileName(m_strUserMapFile);

	if(!strOldUserMapFile.IsEmpty()){
		//이전 파일 삭제
		CFileFind aFind;
		BOOL bFind = aFind.FindFile(strOldUserMapFile);
		while(bFind)
		{
			bFind = aFind.FindNextFile();
			DeleteFile(aFind.GetFilePath());
		}
	}

	return TRUE;
}

/*
---------------------------------------------------------
---------
-- Filename		:	usermapdlg.cpp 
-- Description	:	임시 파일들 삭제.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::DeleteTempFile(CString strFile)
{
	CString strDir(AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase"));
	CFileFind aFind;
	BOOL bFind;
	
	if(strFile == "")
		bFind = aFind.FindFile(strDir + "\\" + "??????????????.tmp");
	else
		bFind = aFind.FindFile(strFile);
	
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}
	
	return TRUE;
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
CString CUserMapDlg::GetFileName(CString str)
{ 
	str = str.Mid(str.ReverseFind(_T('\\'))+1,str.GetLength() );
	
	return str;
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnButtonOpen() 
{			
	SelectUserMapFile("");	
}


/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	다른이름을 저장
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnButtonSaveAs() 
{
	//다른 이름으로 저장
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName,strFileName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\Pattern", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", "csv", "csv");		
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "Save Pattern file Name";
	pDlg.m_ofn.lpstrInitialDir = defultName;

	UpdateData();

	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();				

		if (SaveUserMapFile(strFileName) == FALSE){
			AfxMessageBox(Fun_FindMsg("OnButtonSave_msg1","IDD_USER_MAP_DLG"));//"파일 저장에 실패하였습니다."
		}			
	}
	else
	{
		return;
	}
	
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnRadioDynamicTime() 
{
	// TODO: Add your control notification handler code here
	
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnRadioCumulativeTime() 
{
	// TODO: Add your control notification handler code here
	
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnRadioDynamicModeP() 
{
	// TODO: Add your control notification handler code here
	m_chMode = PS_MODE_CP;
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnRadioDynamicModeI() 
{
	// TODO: Add your control notification handler code here
	m_chMode = PS_MODE_CC;
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	저장시 유효성 검사.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CUserMapDlg::SaveValidation()
{

	CString strTemp;
	int nVal = 0;
	int nbeforeVal = 0;	
	int nRowCnt = m_wndUserMapGrid.GetRowCount() + 1;
	int nColCnt = m_wndUserMapGrid.GetColCount() + 1;
	
	UpdateData();
	
	// 1. 충전/방전시 데이터 검증.
	for (int i=0; i < nRowCnt ; i++)
	{
		//첫번쨰 열제외.
		if(i == 0){					
			continue;
		}
		for (int j=0;j < nColCnt; j++)
		{			
			strTemp.Format("%s",m_wndUserMapGrid.GetValueRowCol(i, j));
			nVal = atoi(strTemp);						
			
			if(m_ChkCharge == USER_MAP_CHARGE){
				if(j == 0){ //첫컬럼은 제외.
					continue;
				}
				if(nVal < 0){					
					strTemp.Format(Fun_FindMsg("SaveValidation_msg1","IDD_USER_MAP_DLG"),i,j);//"[충전모드] %d열의 %d번째 값이 잘못되었습니다."
					AfxMessageBox(strTemp);
					return FALSE;
				}
			}else if(m_ChkCharge == USER_MAP_DISCHARGE){
				if(j == 0){ //첫컬럼은 제외.
					continue;
				}
				if(nVal >= 0){					
					strTemp.Format(Fun_FindMsg("SaveValidation_msg2","IDD_USER_MAP_DLG"),i,j);//"[방전모드] %d열의 %d번째 값이 잘못되었습니다."
					AfxMessageBox(strTemp);
					return FALSE;
				}	
			}		
		}			
	}

	// 2. 온도/SOC 유효성 검사.
	for (int ii=0; ii < nRowCnt ; ii++)
	{				
		for (int j=0;j < nColCnt; j++)
		{
			strTemp.Format("%s",m_wndUserMapGrid.GetValueRowCol(ii, j));
			nVal = atoi(strTemp);						
		
			// ii = 0 인경우는 SOC검사
			if(ii == 0 && j == 0){
				continue; //map 값일경우 넘김.
			}
			
			if(ii == 0 && j == 1){
				nbeforeVal = nVal;
				continue;;
			}else if(ii == 0){
				if(nVal <= nbeforeVal){
					strTemp.Format(Fun_FindMsg("SaveValidation_msg3","IDD_USER_MAP_DLG"),ii,j);//"[SOC] %d열의 %d번째 값이 잘못되었습니다."
					AfxMessageBox(strTemp);
					return FALSE;
				}
				nbeforeVal = nVal;
				continue;
			}
											
			//온도 유효성 검사.
			if(ii == 1 && j == 0)	{
				nbeforeVal = nVal;
				continue;;
			}
			
			if(ii > 1){
				if(j == 0){
					if(nVal <= nbeforeVal){
						strTemp.Format(Fun_FindMsg("SaveValidation_msg4","IDD_USER_MAP_DLG"),ii,j);//"[온도] %d열의 %d번째 값이 잘못되었습니다."
						AfxMessageBox(strTemp);
						return FALSE;
					}else{
						nbeforeVal = nVal;
						continue;
					}
				}
				continue;
			}

		}			
	}
	
	// 3. 입력값 숫자인지 체크.
	for (int iii=0; iii < nRowCnt ; iii++)
	{	
		//첫번쨰 열제외.
		if(iii == 0){					
			continue;
		}
		
		for (int j=0;j < nColCnt; j++)
		{
			strTemp.Format("%s",m_wndUserMapGrid.GetValueRowCol(iii, j));
			if(!IsNum(strTemp)){
				strTemp.Format(Fun_FindMsg("SaveValidation_msg5","IDD_USER_MAP_DLG"),iii,j);//"%d열의 %d번째 값이 잘못되었습니다. 숫자만 입력해주세요."
				AfxMessageBox(strTemp);
				return FALSE;			
			}			
		}			
	}

	return TRUE;
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	충전 선택시 usermap값을 전부 +시킨다.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
void CUserMapDlg::OnRadioCharge() 
{
	// TODO: Add your control notification handler code here

	UserMapSelectDataChange(1);
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	충전 선택시 usermap값을 전부 -시킨다.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
---
*/
void CUserMapDlg::OnRadioDischarge() 
{
	UserMapSelectDataChange(2);
}

/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	usermap 값을 - + 로 변경처리.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
---
*/
void CUserMapDlg::UserMapSelectDataChange(int nType)
{
	// TODO: Add your control notification handler code here
	int nRowCnt = m_wndUserMapGrid.GetRowCount() + 1;
	int nColCnt = m_wndUserMapGrid.GetColCount() + 1;		
	int nVal = 0;
	CString strTemp;
	
	// 1. 충전/방전시 데이터 검증.
	for (int i=0; i < nRowCnt ; i++)
	{		
		for (int j=0;j < nColCnt; j++)
		{
			strTemp.Format("%s,",m_wndUserMapGrid.GetValueRowCol(i, j));
			nVal = atoi(strTemp);					

			// 모두 음수로 변경.
			if(nVal > 0){				
				nVal = -nVal;								
			}

			switch(nType){
				case 1:
					nVal = -nVal; //모두 양수로 변경.
					break;
				case 2 :
					nVal = nVal; //음수 그대로사용
					break;
				default:
					break;
			}

			strTemp.Format("%d",nVal);
			if(i != 0 && j != 0){
				m_wndUserMapGrid.SetStyleRange(CGXRange(i,j),CGXStyle().SetValue(strTemp));
			}	
		}			
	}
}


/*
---------------------------------------------------------
---------
-- Filename		:	
-- Description	:	문자열 숫자 체크.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
---
*/
BOOL CUserMapDlg::IsNum(CString rotatePer)
{
	int count =rotatePer.GetLength(); 
	int i = 0;
	for(i=0; i < count; i++)
	{ 
		char temp = rotatePer.GetAt(i); 
		
		// 음수 처리. 
		if(i==0 && temp == '-') continue; 
		
		// 입력된 키가 0 ~ 9 사이인가를 체크. 
		if(temp >= '0' && temp <= '9') continue; 
        else break; 
	}
	
	
	if(i == count) return TRUE; 
    else return FALSE; 
}