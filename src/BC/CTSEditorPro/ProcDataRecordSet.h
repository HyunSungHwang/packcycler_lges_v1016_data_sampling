#if !defined(AFX_PROCDATARECORDSET_H__A89B3CA4_840E_445B_9E76_080412F2CF39__INCLUDED_)
#define AFX_PROCDATARECORDSET_H__A89B3CA4_840E_445B_9E76_080412F2CF39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProcDataRecordSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProcDataRecordSet recordset

class CProcDataRecordSet : public CRecordset
{
public:
	CProcDataRecordSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CProcDataRecordSet)

// Field/Param Data
	//{{AFX_FIELD(CProcDataRecordSet, CRecordset)
	int		m_ProgressID;
	CString	m_Name;
	int		m_DataType;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProcDataRecordSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROCDATARECORDSET_H__A89B3CA4_840E_445B_9E76_080412F2CF39__INCLUDED_)
