// MySheet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "MySheet.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMySheet

IMPLEMENT_DYNAMIC(CMySheet, CPropertySheet)

//DEL CMySheet::CMySheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
//DEL 	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
//DEL {
//DEL 
//DEL }
CMySheet::CMySheet(CWnd* pParentWnd /*= NULL*/)
{
// 	AddPage(&m_EndCondPage);
// 	AddPage(&m_EndLoopPage);
// 	AddPage(&m_EndAuxBmsPage);
}

CMySheet::~CMySheet()
{
}


BEGIN_MESSAGE_MAP(CMySheet, CPropertySheet)
	//{{AFX_MSG_MAP(CMySheet)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMySheet message handlers

BOOL CMySheet::OnInitDialog() 
{
//	m_pDoc = (CCTSEditorProDoc *)(CEndConditionTabDlg *)m_pParent->m_hWnd)->m_pDoc;
	BOOL bResult = CPropertySheet::OnInitDialog();
	
	// TODO: Add your specialized code here
	// disable page #2
//	EnablePage (1, FALSE);
	
	return bResult;
}
void CMySheet::EnablePage (int nPage, BOOL bEnable)
{
	// if we want to enable the page
	if (bEnable)
	{
		// remove the index from the map
		m_DisabledPages.RemoveKey (nPage);
		// take out " - Disabled" from tab label
		CTabCtrl* pTab = GetTabControl();
		ASSERT (pTab);
		TC_ITEM ti;
		char szText[100];
		ti.mask = TCIF_TEXT;
		ti.pszText = szText;
		ti.cchTextMax = 100;
		VERIFY (pTab->GetItem (nPage, &ti));
		char * pFound = strstr (szText, " - Disabled");
		if (pFound)
		{
			*pFound = '\0';
			VERIFY (pTab->SetItem (nPage, &ti));
		}
	}
	// if we want to disable it
	else
	{
		// add the index to the map
		m_DisabledPages.SetAt (nPage, nPage);
		// add " - Disabled" to tab label
		CTabCtrl* pTab = GetTabControl();
		ASSERT (pTab);
		TC_ITEM ti;
		char szText[100];
		ti.mask = TCIF_TEXT;
		ti.pszText = szText;
		ti.cchTextMax = 100;
		VERIFY (pTab->GetItem (nPage, &ti));
		strcat (szText, " - Disabled");
		VERIFY (pTab->SetItem (nPage, &ti));
	}
}

BOOL CMySheet::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	NMHDR* pnmh = (NMHDR*)lParam;
	// tab is about to change
	if (TCN_SELCHANGING == pnmh->code)
		// save the current page index
		m_nLastActive = GetActiveIndex ();
	// tab has been changed
	else if (TCN_SELCHANGE == pnmh->code)
	{
		// get the current page index
		int nCurrentPage = GetActiveIndex ();
		SetActivePage(nCurrentPage);
//		Fun_SetActive(nCurrentPage);
		// if current page is in our map of disabled pages
// 		if (m_DisabledPages.Lookup (nCurrentPage, nCurrentPage))
// 			// activate the previous page
// 			PostMessage (PSM_SETCURSEL, m_nLastActive);
	}
	return CPropertySheet::OnNotify(wParam, lParam, pResult);
}

void CMySheet::Fun_SetpDoc()
{
// 	m_EndCondPage.m_pDoc = m_pDoc;
// 	m_EndLoopPage.m_pDoc = m_pDoc;
// 	m_EndAuxBmsPage.m_pDoc = m_pDoc;
}

void CMySheet::Fun_SetActive(UINT iPage)
{
// 	switch (iPage)
// 	{
// 	case 0:
// 		SetActivePage(&m_EndCondPage);
// 		break;
// 	case 1:
// 		SetActivePage(&m_EndLoopPage);
// 		break;
// 	case 2:
// 		SetActivePage(&m_EndAuxBmsPage);
// 		break;
// 
// 	}

}

void CMySheet::UpdateEndCondition()
{
// 	m_EndCondPage.UpdateEndCondition();
// 	m_EndLoopPage.UpdateEndCondition();
// 	m_EndAuxBmsPage.UpdateEndCondition();

}

void CMySheet::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	CPropertySheet::OnClose();
}

void CMySheet::Fun_SetStepNo(int iStepNo)
{
// 	m_EndCondPage.m_nStepNo = iStepNo;
// 	m_EndLoopPage.m_nStepNo = iStepNo;
// 	m_EndAuxBmsPage.m_nStepNo = iStepNo;

// 	if(iStepNo < 1)	return;
// 
// 	STEP *pStep = (STEP*)m_pDoc->m_apStep[iStepNo-1];
// 	ASSERT(pStep);
// 	switch(pStep->chType)
// 	{
// 	case PS_STEP_LOOP:
// 		Fun_SetActive(1);
// 		break;
// 	default :
// 		Fun_SetActive(0);
// 		break;
// 	}
}
