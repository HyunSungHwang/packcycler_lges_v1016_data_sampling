// EditSimulDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "EditSimulDataDlg.h"

#include "RealEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditSimulDataDlg dialog


CEditSimulDataDlg::CEditSimulDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CEditSimulDataDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CEditSimulDataDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CEditSimulDataDlg::IDD3):
	(CEditSimulDataDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CEditSimulDataDlg)
	m_LabFileName = _T("");
	m_LabRowCount = _T("");
	m_lEndMSec = 0;
	IsChange = FALSE;
	m_nChkMode = -1;
	m_nChkTime = -1;
	//}}AFX_DATA_INIT
	//+2015.9.22 USY Add For PatternCV
	m_lMaxCurrent = 0;
	m_lMaxVoltage = 0;
	//-
}


void CEditSimulDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditSimulDataDlg)
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_ctrlEndTime);
	DDX_Text(pDX, IDC_LAB_FILE_NAME, m_LabFileName);
	DDX_Text(pDX, IDC_LAB_COUNT, m_LabRowCount);
	DDX_Text(pDX, IDC_END_MSEC_EDIT, m_lEndMSec);
	DDX_Radio(pDX, IDC_RADIO_MODE1, m_nChkMode);
	DDX_Radio(pDX, IDC_RADIO_TIME1, m_nChkTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditSimulDataDlg, CDialog)
	//{{AFX_MSG_MAP(CEditSimulDataDlg)
	ON_BN_CLICKED(IDC_BUT_INSERT, OnButInsert)
	ON_BN_CLICKED(IDC_BUT_DEL, OnButDel)
	ON_BN_CLICKED(IDC_RADIO3, OnRadioMode1)
	ON_BN_CLICKED(IDC_RADIO4, OnRadioMode2)
	ON_BN_CLICKED(IDC_RADIO5, OnRadioMode3)
	ON_BN_CLICKED(IDC_RADIO_TIME1, OnRadioTime1)
	ON_BN_CLICKED(IDC_RADIO_TIME2, OnRadioTime2)
	ON_BN_CLICKED(IDC_RADIO_TIME3, OnRadioTime3)
	ON_BN_CLICKED(IDC_BUT_TRIANGLE, OnButTriangle)
	ON_BN_CLICKED(IDC_BUT_SQUARE, OnButSquare)
	ON_BN_CLICKED(IDC_BUT_ALL_SELECT, OnButAllSelect)
	ON_NOTIFY(UDN_DELTAPOS, IDC_END_TIME_MSEC_SPIN, OnDeltaposEndTimeMsecSpin)
	ON_BN_CLICKED(IDC_BUT_TIME, OnButTime)
	ON_BN_CLICKED(IDC_RADIO_MODE1, OnRadioMode1)
	ON_BN_CLICKED(IDC_RADIO_MODE2, OnRadioMode2)
	ON_BN_CLICKED(IDC_RADIO_MODE3, OnRadioMode3)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_COMBO_SELECT, OnGridComboSelected)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditSimulDataDlg message handlers

BOOL CEditSimulDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdateData(TRUE);


	m_ctrlEndTime.SetTime(DATE(0));
	m_ctrlEndTime.SetFormat("HH:mm:ss");

	if (m_cPatternTime == PS_PATTERN_TIME_ACUMULATE)	m_nChkTime = 0;
	else if (m_cPatternTime == PS_PATTERN_TIME_OPERATING)	m_nChkTime = 1;
	else if (m_cPatternTime == PS_PATTERN_TIME_TEMPERATURE)	m_nChkTime = 2;
	else 	m_nChkTime = 1;

	if (m_chMode == PS_MODE_CC) m_nChkMode = 0;
	if (m_chMode == PS_MODE_CP) m_nChkMode = 1;		//ljb 20150122 add
	if (m_chMode == PS_MODE_CV) m_nChkMode = 2;		//ljb 20150122 add
	else m_nChkMode = 1;

	m_LabFileName = m_strEditFileName;
	InitPatternGrid();

	if (m_bEditMode) LoadData();


	if (m_cPatternTime == PS_PATTERN_TIME_TEMPERATURE)
	{
		m_ctrlEndTime.SetTime(DATE(0));
		m_ctrlEndTime.SetFormat("HH:mm:ss");
		GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE);
		GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_MS)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_TIME)->EnableWindow(FALSE);

		GetDlgItem(IDC_RADIO_MODE2)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_MODE3)->EnableWindow(FALSE);
	}

	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CEditSimulDataDlg::InitPatternGrid()
{
	CString strType;
	CRect rect, cellRect;
	m_wndPatternGrid.SubclassDlgItem(IDC_PATTERN_GRID, this);
	m_wndPatternGrid.Initialize();

	m_wndPatternGrid.GetParam()->EnableUndo(FALSE);
	m_wndPatternGrid.GetParam()->EnableMoveRows(FALSE);		//Drag and Drop 지원
	
	m_wndPatternGrid.SetRowCount(1);
	m_wndPatternGrid.SetColCount(PATTERN_COL_MODE);		//ljb
	m_wndPatternGrid.GetClientRect(&rect);
	m_wndPatternGrid.SetColWidth(PATTERN_COL_NO			, PATTERN_COL_NO			, 50);
	m_wndPatternGrid.SetColWidth(PATTERN_COL_TIME		, PATTERN_COL_TIME			, 150);
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA1		, PATTERN_COL_DATA1			, 120);		//Start 전류, 파워, 전압
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA2		, PATTERN_COL_DATA2			, 0);		//ljb 삼각파일 경우 END  전류, 파워, 전압 => Hide
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA3		, PATTERN_COL_DATA3			, 0);		//ljb 20150902 add 전압 일 경우 충전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA4		, PATTERN_COL_DATA4			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_MODE		, PATTERN_COL_MODE			, 0);		//ljb 삼각파 Hide (삼각파 , 사각파 선택)
	
//	m_wndPatternGrid.SetRowResize(FALSE);

	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_NO			),	CGXStyle().SetValue("No"));

	if (m_cPatternTime == PS_PATTERN_TIME_ACUMULATE)
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_TIME		),	CGXStyle().SetValue("Time (accumulate)"));
	else if (m_cPatternTime == PS_PATTERN_TIME_OPERATING)
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_TIME		),	CGXStyle().SetValue("Time (operate in row)")); //yulee 20190719 CTSEditorPro (ver. v1013 g.1.3.2.p20190718)
	else if (m_cPatternTime == PS_PATTERN_TIME_TEMPERATURE)
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_TIME		),	CGXStyle().SetValue("Temperature"));

	//+2015.10.02 USY Mod For PatternCV(김재호K)
	if (m_chMode == PS_MODE_CC)
	{
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA1		),	CGXStyle().SetValue("I(A) - S"));
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA2		),	CGXStyle().SetValue("I(A) - E"));
	}
	else if (m_chMode == PS_MODE_CP)
	{
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA1		),	CGXStyle().SetValue("P(W) - S"));
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA2		),	CGXStyle().SetValue("P(W) - E"));
	}
	else if (m_chMode == PS_MODE_CV)
	{
		//+2015.10.02 USY Mod For Pattern CV(Req 김재호K)
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA1		),	CGXStyle().SetValue("V(V) - S"));
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA2		),	CGXStyle().SetValue("V(V) - E"));
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA3		),	CGXStyle().SetValue("Charge Current(A)"));
		m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA4		),	CGXStyle().SetValue("Discharge Current(A)"));
	}

	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_MODE		),	CGXStyle().SetValue("WAVE"));
		
	m_wndPatternGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndPatternGrid.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(0),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	m_wndPatternGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndPatternGrid, IDS_CTRL_REALEDIT));
	m_wndPatternGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndPatternGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndPatternGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	

	char str[12], str2[7], str3[5];
	if (m_chMode == PS_MODE_CC)
	{
		sprintf(str3, "%d", 3);		//소숫점 이하
		sprintf(str, "%%.%dlf", 3);		
		sprintf(str2, "%d", 3);		//정수부
	}
	else if (m_chMode == PS_MODE_CP)
	{
		sprintf(str3, "%d", 3);		//소숫점 이하
		sprintf(str, "%%.%dlf", 3);		
		sprintf(str2, "%d", 6);		//정수부
	}
	else if (m_chMode == PS_MODE_CV)
	{
		sprintf(str3, "%d", 3);		//소숫점 이하
		sprintf(str, "%%.%dlf", 3);		
		sprintf(str2, "%d", 4);		//정수부
	}
	else
	{
		sprintf(str3, "%d", 4);		//소숫점 이하
		sprintf(str, "%%.%dlf", 4);		
		sprintf(str2, "%d", 4);		//정수부
	}
	
	//Edit Ctrl////////////////////////////////////////
	if (m_cPatternTime == PS_PATTERN_TIME_ACUMULATE || m_cPatternTime == PS_PATTERN_TIME_OPERATING)
	{
		m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_TIME),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
			.SetControl(GX_IDS_CTRL_COMBOBOX)
			.SetChoiceList(_T("00:00:00.00\r\n"))
			.SetValue(_T("00:00:00.00"))
			/*
			.SetChoiceList(_T("00:00:00.00\r\n"))
			.SetValue(_T("00:00:00.00"))
			*/
			);
	}
	else
	{
		m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_TIME),
			CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(0L)
	        );
	}

// 	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_MODE),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// 		.SetChoiceList(_T("사각파\r\n삼각파\r\n"))
// 		//.SetValue(_T(""))
// 		.SetValue(0L)
// 	);



	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_DATA1,PATTERN_COL_DATA4),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(0L)
	);
 	m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
	//+2015.9.21 USY Mod For PatternCV(김재호K)
	if(m_chMode == PS_MODE_CV)
	{
		m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(TRUE));
		m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA4), CGXStyle().SetEnabled(TRUE));
		m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA2		, PATTERN_COL_DATA2			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
		m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA3		, PATTERN_COL_DATA3			, 150);		//ljb 20150902 add 전압 일 경우 방전 전류
		m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA4		, PATTERN_COL_DATA4			, 150);		//ljb 20150902 add 전압 일 경우 방전 전류
	}
	else
	{
		m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(FALSE));
		m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA4), CGXStyle().SetEnabled(FALSE));
		m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA2		, PATTERN_COL_DATA2			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
		m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA3		, PATTERN_COL_DATA3			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
		m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA4		, PATTERN_COL_DATA4			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	}
//  	m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(FALSE));
//  	m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA4), CGXStyle().SetEnabled(FALSE));
	//-
	CString strMode = "Triangular\r\nSquare\r\n";
	m_pModeCombo = new CGridComboBox(this, 
								&m_wndPatternGrid,
								GX_IDS_CTRL_ZEROBASED_EX6,
								GX_IDS_CTRL_ZEROBASED_EX6,
								GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
								);
	m_wndPatternGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX6, m_pModeCombo);
	m_pModeCombo->SetItemData(0, 0);
	m_pModeCombo->SetItemData(1, 1);
	
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_MODE),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_ZEROBASED_EX6)
			.SetHorizontalAlignment(DT_CENTER)
			.SetChoiceList(strMode)
			.SetValue(0L)
			);

// 				.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
// 				.SetChoiceList(_T("사각파\r\n삼각파\r\n"))
// 				.SetValue(0L)


// 	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
// 			CGXStyle()
// 				.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
// 				.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
// 				//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\n"))
// 				.SetValue(_T("Unsigned"))
// 		);
	m_wndPatternGrid.GetParam()->EnableUndo(TRUE);

	return TRUE;
}

void CEditSimulDataDlg::LoadData()
{
	CString	strErrorMsg;
	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;
	
	CStringList strColList;
	
//	ZeroMemory(&oldTime, sizeof(PSTimeSet));
	if(m_wndPatternGrid.GetRowCount() > 0)
		m_wndPatternGrid.RemoveRows(1, m_wndPatternGrid.GetRowCount());
	
	if( !aFile.Open( m_strEditFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		//strErrorMsg.Format("%s 파일을 열 수 없습니다.", m_strEditFileName);
		strErrorMsg.Format(Fun_FindMsg("EditSimulDataDlg_LoadData_msg1","IDD_PATTERN_EDITOR_DLG"), m_strEditFileName);//&&
		AfxMessageBox(strErrorMsg);
		return;
	}
	
	CStringList		strDataList;
	int nRow;
	CString strMode,strValue;
	strMode = "";

	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == "STX")			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			int p1=0, s=0;
			//1. Title//////////////////////////////////////////////////////////////
			if(aFile.ReadString(strTemp))
			{
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
						//m_TitleList.AddTail(str);
						s  = p1+1;
					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				strMode = str;
				//if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);
			}
			else
			{
				aFile.Close(); //lyj 20200518
				//AfxMessageBox("파일 데이터가 올바르지 않습니다.");
				AfxMessageBox(Fun_FindMsg("EditSimulDataDlg_LoadData_msg2","IDD_PATTERN_EDITOR_DLG"));//&&
				return ;
			}

			if (strMode == "I")
			{
				m_nChkMode = 0;
			}
			if (strMode == "P")
			{
				m_nChkMode = 1;
			}

			if (strMode == "V")
			{
				m_nChkMode = 2;
				m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA3		, PATTERN_COL_DATA3			, 150);		//ljb 20150902 add 전압 일 경우 충전 전류
				m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA4		, PATTERN_COL_DATA4			, 150);		//ljb 20150902 add 전압 일 경우 방전 전류
//				m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(TRUE));
//				m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA4), CGXStyle().SetEnabled(TRUE));
			}

			//m_nColumnSize = m_TitleList.GetCount();
			
			////////////////////////////////////////////////////////////////////////
			double dTemp;
			while(aFile.ReadString(strTemp))
			{
				if (strTemp == "") break;

				p1 = s = 0;
				int nCol = PATTERN_COL_TIME;

				//Insert Grid
				m_wndPatternGrid.InsertRows(m_wndPatternGrid.GetRowCount()+1, nCol);
				nRow = m_wndPatternGrid.GetRowCount();

				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);			//시간
						s = p1+1;
						m_wndPatternGrid.SetValueRange(CGXRange(nRow, nCol), str);
						nCol++;					
					}
				}
				if (strMode == "V")
				{
					//CV 전류 리미트 설정
//					m_wndPatternGrid.SetStyleRange(
//						CGXRange(nRow, nCol), 
//						CGXStyle().SetInterior( CGXBrush().SetColor(RGB(0,255,0) )));			
					//+2015.9.22 USY Mod For PatternCV
					m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA3), CGXStyle().SetEnabled(TRUE));	//충전 전류 리미트
					m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA4), CGXStyle().SetEnabled(TRUE));	//방전 전류 리미트
//					m_wndPatternGrid.SetStyleRange(CGXRange(nRow, (2)), CGXStyle().SetEnabled(TRUE));	//충전 전류 리미트
//					m_wndPatternGrid.SetStyleRange(CGXRange(nRow, (3)), CGXStyle().SetEnabled(TRUE));	//방전 전류 리미트
					//-

					//m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_MODE), CGXStyle().SetValue(1L));
					AfxExtractSubString(strValue,strTemp,1,','); dTemp = atof(strValue);
					m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA1), CGXStyle().SetValue(dTemp));
					AfxExtractSubString(strValue,strTemp,2,','); dTemp = atof(strValue);
					m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA3), CGXStyle().SetValue(dTemp));
					AfxExtractSubString(strValue,strTemp,3,','); dTemp = atof(strValue);
					m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA4), CGXStyle().SetValue(dTemp));

				}
				else
				{
					str = strTemp.Mid(s);
					str.TrimLeft(' '); str.TrimRight(' ');
					if (nCol == 4)
					{
						if (str.Left(1) == "R")
						{
							//사각파 설정
							m_wndPatternGrid.SetValueRange(CGXRange(nRow, (nCol-1)), "0");
							m_wndPatternGrid.SetStyleRange(CGXRange(nRow, (nCol-1)), CGXStyle().SetEnabled(FALSE));
							m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_MODE), CGXStyle().SetValue(0L));
						}
						else
						{
							//삼각파 설정
							m_wndPatternGrid.SetStyleRange(
								CGXRange(nRow, nCol), 
								CGXStyle().SetInterior( CGXBrush().SetColor(RGB(0,255,0) )));			
							m_wndPatternGrid.SetStyleRange(CGXRange(nRow, (nCol-1)), CGXStyle().SetEnabled(TRUE));
							m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_MODE), CGXStyle().SetValue(1L));
						}
					}
					else
					{
						m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
						m_wndPatternGrid.SetValueRange(CGXRange(nRow, nCol), str);
					}
				}


//				if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
//				m_wndPatternGrid.InsertRows(m_wndPatternGrid.GetRowCount()+1, 1);
			}
			
		}	
	}
	aFile.Close();
	m_LabRowCount.Format("%d",m_wndPatternGrid.GetRowCount());
	m_wndPatternGrid.Redraw();

//	UpdateData()

}

void CEditSimulDataDlg::OnButInsert() 
{
	// TODO: Add your control notification handler code here

	//ksj 20200605
	int nMaxPatternRow = AfxGetApp()->GetProfileInt("Config","MAX_PATTERN_ROW",50000);
	if(m_wndPatternGrid.GetRowCount()  > nMaxPatternRow && nMaxPatternRow != 0) //0이면 무시.
	{
		CString strErrorMsg;
		strErrorMsg.Format("The maximum number of rows has been exceeded. (%d, Max: %d)", m_wndPatternGrid.GetRowCount(), nMaxPatternRow);
		AfxMessageBox(strErrorMsg );
		return;
	}
	//ksj end

	CRowColArray ra;
	m_wndPatternGrid.GetSelectedRows(ra);
	//+2015.9.21 USY Mod for PatternCV(김재호K)
	if(ra.GetSize() > 0)
	{
		m_wndPatternGrid.InsertRows(ra[0] + 1, 1);
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[0] + 1, PATTERN_COL_DATA3), 	CGXStyle().SetValue("0"));
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[0] + 1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(TRUE));
	}
	else
	{
		int nLastRow = m_wndPatternGrid.GetRowCount();
		m_wndPatternGrid.InsertRows(nLastRow+1, 1);
		m_wndPatternGrid.SetStyleRange(CGXRange(nLastRow+1, PATTERN_COL_DATA3), 	CGXStyle().SetValue("0"));
		m_wndPatternGrid.SetStyleRange(CGXRange(nLastRow+1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(TRUE));
	}

	
// 	if(ra.GetSize() > 0)
// 	{
//  		m_wndPatternGrid.InsertRows(ra[0] + 1, 1);
//  		m_wndPatternGrid.SetStyleRange(CGXRange(ra[0] + 1, PATTERN_COL_DATA2), 	CGXStyle().SetValue("0"));
//  		m_wndPatternGrid.SetStyleRange(CGXRange(ra[0] + 1, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
// 	}
// 	else
// 	{
//  		int nLastRow = m_wndPatternGrid.GetRowCount();
//  		m_wndPatternGrid.InsertRows(nLastRow+1, 1);
//  		m_wndPatternGrid.SetStyleRange(CGXRange(nLastRow+1, PATTERN_COL_DATA2), 	CGXStyle().SetValue("0"));
//  		m_wndPatternGrid.SetStyleRange(CGXRange(nLastRow+1, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
// 	}
	//-
	IsChangeFlag(TRUE);	
}

void CEditSimulDataDlg::IsChangeFlag(BOOL IsFlag)
{
	IsChange = IsFlag;
	GetDlgItem(IDOK)->EnableWindow(IsChange);
//	GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
}

BOOL CEditSimulDataDlg::Fun_TimeSaveError(CString strTime)
{
	//uiOldTime = uiSaveTime = 0;
	
	CString strTemp;
	UINT	mHour,mMin,mSec;
	int		nPos;
	float   fMsec;
	fMsec = 0;
	mHour=mMin=mSec=0;
	
	int		nLength;
	
	strTime.TrimLeft();
	strTime.TrimRight();

	nLength = strTime.GetLength();
	nPos = strTime.ReverseFind('.');
	if (nPos > 0)
	{
		strTemp = strTime.Mid(nPos+1,nLength-nPos);
		if (strTemp.GetLength() < 3) strTemp += "000";	//ljb 2011316 이재복 ////////// .3자리가 아니면 0 으로 채워 만든다
		strTemp = strTemp.Left(3);
		fMsec = atof(strTemp);
		if (fMsec != 0) fMsec = fMsec/1000.0f;
		strTime = strTime.Left(nPos);
	}
	if (strTime.ReverseFind(':') < 0)
	{
		mSec = atoi(strTime);
		if (mSec >= 3600)
		{
			mHour = mSec / 3600;
			mSec= mSec - (mHour * 3600); 
		}
		if (mSec >= 360)
		{
			mMin = mSec / 3600;
			mSec= mSec - (mMin * 360); 
		}
//		if (mMSec == 0) fMsec = 0;
//		else fMsec = mMSec / 1000;
		fSaveTime = mHour*3600 + mMin*360 + mSec + fMsec;
	}
	else
	{
		nLength = strTime.GetLength();
		nPos = strTime.ReverseFind(':');
		strTemp = strTime.Mid(nPos+1,nLength-nPos);
		mSec = atoi(strTemp);
		strTime = strTime.Left(nPos);

		nLength = strTime.GetLength();
		nPos = strTime.ReverseFind(':');
		if (nPos > 0)
		{
			strTemp = strTime.Mid(nPos+1,nLength-nPos);
			mMin = atoi(strTemp);
			strTime = strTime.Left(nPos);
		}
		else
		{
			mMin = atoi(strTime);
			strTime.Empty();
		}

		if (strTime.IsEmpty() == FALSE)
		{
			nLength = strTime.GetLength();
			nPos = strTime.ReverseFind(':');
			if (nPos > 0)
			{
				//시간 형식 오류
				return FALSE;
			}
			else
			{
				mHour = atoi(strTime);
				strTime.Empty();
			}

		}
// 		if (mMSec == 0) fMsec = 0;
// 		else fMsec = mMSec / 1000;
		fSaveTime = mHour*3600 + mMin*360 + mSec + fMsec;
	}

//	uiSaveTime = mHour*3600 + mMin*360 + mSec*60 + mMSec;
	if (fOldTime < fSaveTime)
	{
		fOldTime = fSaveTime;
		return TRUE;
	}
	return FALSE;
}
void CEditSimulDataDlg::OnButDel() 
{
	CRowColArray ra;
	m_wndPatternGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndPatternGrid.RemoveRows(ra[i], ra[i]);
	}
	IsChangeFlag(TRUE);		
}

void CEditSimulDataDlg::OnRadioMode1() 
{

	m_chMode = PS_MODE_CC;
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA1		),	CGXStyle().SetValue("I(A) - S"));
//	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA2		),	CGXStyle().SetValue("I(A) - E"));
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA2		, PATTERN_COL_DATA3			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA3		, PATTERN_COL_DATA3			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA4		, PATTERN_COL_DATA4			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	
	
	m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
	m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(FALSE));
	m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA4), CGXStyle().SetEnabled(FALSE));
	
	char str[12], str2[7], str3[5];
	sprintf(str3, "%d", 3);		//소숫점 이하
	sprintf(str, "%%.%dlf", 3);		
	sprintf(str2, "%d", 3);		//정수부
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_DATA1),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);
}

void CEditSimulDataDlg::OnRadioMode2() 
{
	m_chMode = PS_MODE_CP;
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA1		),	CGXStyle().SetValue("P(W) - S"));
//	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA2		),	CGXStyle().SetValue("P(W) - E"));

	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA2		, PATTERN_COL_DATA3			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA3		, PATTERN_COL_DATA3			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA4		, PATTERN_COL_DATA4			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	
	
	//m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
	//m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA3), CGXStyle().SetEnabled(FALSE));
	//m_wndPatternGrid.SetStyleRange(CGXRange(1, PATTERN_COL_DATA4), CGXStyle().SetEnabled(FALSE));
	
	char str[12], str2[7], str3[5];
	sprintf(str3, "%d", 3);		//소숫점 이하
	sprintf(str, "%%.%dlf", 3);		
	sprintf(str2, "%d", 6);		//정수부
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_DATA1),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);
}

void CEditSimulDataDlg::OnRadioMode3() 
{
	int nRowCnt;
	nRowCnt = m_wndPatternGrid.GetRowCount();

	m_chMode = PS_MODE_CV;
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA1		),	CGXStyle().SetValue("V(V) - S"));
	//+2015.9.21 USY Mod For Pattern CV(Req 김재호K)
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA3		),	CGXStyle().SetValue("Charge Current(A)"));
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA4		),	CGXStyle().SetValue("DisCharge Current(A)"));
//	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA3		),	CGXStyle().SetValue("Charge Current Limit"));
//	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA4		),	CGXStyle().SetValue("Discharge Current Limit"));
	//-
//	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_DATA2		),	CGXStyle().SetValue("V(V) - E"));
	
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA2		, PATTERN_COL_DATA3			, 0);		//ljb 20150902 add 전압 일 경우 방전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA3		, PATTERN_COL_DATA3			, 150);		//ljb 20150902 add 전압 일 경우 방전 전류
	m_wndPatternGrid.SetColWidth(PATTERN_COL_DATA4		, PATTERN_COL_DATA4			, 150);		//ljb 20150902 add 전압 일 경우 방전 전류
	
	for (int nRow=1; nRow <= nRowCnt; nRow++)
	{
		m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
		m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA3), CGXStyle().SetEnabled(TRUE));
		m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA4), CGXStyle().SetEnabled(TRUE));
	}
		
	
	char str[12], str2[7], str3[5];
	sprintf(str3, "%d", 3);		//소숫점 이하
	sprintf(str, "%%.%dlf", 3);		
	sprintf(str2, "%d", 4);		//정수부
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_DATA1,PATTERN_COL_DATA2),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);
}

void CEditSimulDataDlg::OnRadioTime1() 
{
	GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE);
	GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE);
	GetDlgItem(IDC_STATIC_MS)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUT_TIME)->EnableWindow(TRUE);

	GetDlgItem(IDC_RADIO_MODE1)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO_MODE2)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO_MODE3)->EnableWindow(TRUE);
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_TIME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetControl(GX_IDS_CTRL_COMBOBOX)
		.SetChoiceList(_T("00:00:00.00\r\n"))
		.SetValue(_T("00:00:00.00"))
		);

	
	m_cPatternTime = PS_PATTERN_TIME_ACUMULATE;		//누적 시간
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_TIME		),	CGXStyle().SetValue("time (accumulate)"));
}

void CEditSimulDataDlg::OnRadioTime2() 
{
	GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE);
	GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE);
	GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE);
	GetDlgItem(IDC_STATIC_MS)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUT_TIME)->EnableWindow(TRUE);

	GetDlgItem(IDC_RADIO_MODE1)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO_MODE2)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO_MODE3)->EnableWindow(TRUE);
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_TIME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		.SetControl(GX_IDS_CTRL_COMBOBOX)
		.SetChoiceList(_T("00:00:00.00\r\n"))
		.SetValue(_T("00:00:00.00"))
		);


	m_cPatternTime = PS_PATTERN_TIME_OPERATING;		//동작 시간
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_TIME		),	CGXStyle().SetValue("Time (operate in row)")); //yulee 20190719 CTSEditorPro (ver. v1013 g.1.3.2.p20190718)
}
void CEditSimulDataDlg::OnRadioTime3() 
{
	m_nChkTime = 2;
	m_nChkMode = 0;
	OnRadioMode1(); 
	GetDlgItem(IDC_RADIO_MODE2)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_MODE3)->EnableWindow(FALSE);

	GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE);
	GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE);
	GetDlgItem(IDC_STATIC_MS)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_TIME)->EnableWindow(FALSE);
		

	char str[12], str2[7], str3[5];
	sprintf(str3, "%d", 1);		//소숫점 이하
	sprintf(str, "%%.%dlf", 1);		
	sprintf(str2, "%d", 4);		//정수부
	
	m_wndPatternGrid.SetStyleRange(CGXRange().SetCols(PATTERN_COL_TIME),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);

	m_cPatternTime = PS_PATTERN_TIME_TEMPERATURE;		//온도
	m_wndPatternGrid.SetStyleRange(CGXRange(0,PATTERN_COL_TIME		),	CGXStyle().SetValue("Temperature"));
	UpdateData(FALSE);
}

LRESULT CEditSimulDataDlg::OnGridComboSelected(WPARAM wParam, LPARAM lParam)
{
	CGridComboBox *pCombo = (CGridComboBox*) lParam;
	int selID = (int) wParam;
	ROWCOL	nRow, nCol;

//	int nTotRowCount;
	DWORD nComboData = 0;
//	CCTSEditorProDoc *pDoc = GetDocument();
	CString strTemp;
	

	//Step Type or Step Procedure Type Select
	if(pCombo == m_pModeCombo )
	{
// 		nTotRowCount = m_wndStepGrid.GetRowCount();
 		if(!m_wndPatternGrid.GetCurrentCell(&nRow, &nCol))		return 0;

		//선택한 Step Type을 구함
 		nComboData = pCombo->GetItemData(selID);
		if(nComboData == 0)
		{
			//사각파
			m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA2), 	CGXStyle().SetValue("0"));
			m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
			m_wndPatternGrid.SetStyleRange(
				CGXRange(nRow, nCol), 
				CGXStyle().SetInterior( CGXBrush().SetColor(RGB(255,255,255) )));			
		}
		if(nComboData == 1)
		{
			//삼각파
			//m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA2), 	CGXStyle().SetValue("0"));
			m_wndPatternGrid.SetStyleRange(CGXRange(nRow, PATTERN_COL_DATA2), CGXStyle().SetEnabled(TRUE));
			m_wndPatternGrid.SetStyleRange(
				CGXRange(nRow, nCol), 
				CGXStyle().SetInterior( CGXBrush().SetColor(RGB(0,255,0) )));			
		}
	}
	return 1;
}

void CEditSimulDataDlg::OnButTriangle() 
{
	CRowColArray ra;
	m_wndPatternGrid.GetSelectedRows(ra);
	CString strPMode,strOldData1,strOldData2;
	int		nRowCnt;
	BOOL bSkipFirst(FALSE);
	nRowCnt = m_wndPatternGrid.GetRowCount();

	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) continue;//Header 삭제불가
		strPMode = m_wndPatternGrid.GetValueRowCol(ra[i], PATTERN_COL_MODE);
		if (strPMode == "1") continue;

		if (ra[i] == 1) 
			strOldData1 = "0";
		else
		{
			strPMode = m_wndPatternGrid.GetValueRowCol(ra[i]-1, PATTERN_COL_MODE);
			if (strPMode == "1")
				strOldData1 = m_wndPatternGrid.GetValueRowCol(ra[i]-1, PATTERN_COL_DATA2); 
			else
				strOldData1 = m_wndPatternGrid.GetValueRowCol(ra[i]-1, PATTERN_COL_DATA1); 

		}

		strOldData2 = m_wndPatternGrid.GetValueRowCol(ra[i], PATTERN_COL_DATA1);
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_DATA1), 	CGXStyle().SetValue(strOldData1));
		
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_DATA2), CGXStyle().SetEnabled(TRUE));
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_DATA2), 	CGXStyle().SetValue(strOldData2));

// 		if (ra[i] == nRowCnt)
// 		{
// //			bSkipFirst = TRUE;
// //			strOldData2 = m_wndPatternGrid.GetValueRowCol(ra[i], PATTERN_COL_DATA1);
// 		}
// 		else
// 		{
// 			strOldData2 = m_wndPatternGrid.GetValueRowCol(ra[i]+1, PATTERN_COL_DATA1);
// 			m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_DATA2), 	CGXStyle().SetValue(strOldData2));
// 
// 		}
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_MODE), CGXStyle().SetValue(1L));
		m_wndPatternGrid.SetStyleRange(
			CGXRange(ra[i], PATTERN_COL_MODE), 
			CGXStyle().SetInterior( CGXBrush().SetColor(RGB(0,255,0) )));

	}
	IsChangeFlag(TRUE);			
}

void CEditSimulDataDlg::OnButSquare() 
{
	CRowColArray ra;
	m_wndPatternGrid.GetSelectedRows(ra);
	CString	strPMode,strOldData1;
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) continue; //Header 삭제불가
			
		strPMode = m_wndPatternGrid.GetValueRowCol(ra[i], PATTERN_COL_MODE);
		if (strPMode == "0") continue;

		strOldData1 = m_wndPatternGrid.GetValueRowCol(ra[i], PATTERN_COL_DATA2); 
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_DATA2), 	CGXStyle().SetValue("0"));
		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));

		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_DATA1), 	CGXStyle().SetValue(strOldData1));

		m_wndPatternGrid.SetStyleRange(CGXRange(ra[i], PATTERN_COL_MODE), CGXStyle().SetValue(0L));
		m_wndPatternGrid.SetStyleRange(
			CGXRange(ra[i], PATTERN_COL_MODE), 
			CGXStyle().SetInterior( CGXBrush().SetColor(RGB(255,255,255) )));			
	}
	IsChangeFlag(TRUE);			
	
}

BOOL CEditSimulDataDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN) {
		if(pMsg->wParam == 13 || pMsg->wParam == 27) return TRUE;
		if(GetAsyncKeyState(VK_CONTROL))
		{
			if(pMsg->wParam == 65 || pMsg->wParam == 97)
			{
				m_wndPatternGrid.SelectRange(CGXRange(1,0, m_wndPatternGrid.GetRowCount(), m_wndPatternGrid.GetColCount()));
				return TRUE;
			}
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CEditSimulDataDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	fOldTime = fSaveTime = 0;

	CString strSaveData;
	CString strTemp,strTemp2;
	int nRowCnt,nColCnt;
	int i,j;

	strSaveData="";
	nRowCnt = m_wndPatternGrid.GetRowCount();
	nColCnt = m_wndPatternGrid.GetColCount();

	//ksj 20200910 : 데이터 없으면 적용 금지
	if(nRowCnt <= 0)
	{
		CString strErrorMsg;
		strErrorMsg.Format("There is no data and cannot be applied. (Data : %d)", nRowCnt);
		AfxMessageBox(strErrorMsg);
		return;
	}
	//ksj end

// 	
// 	strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg1","IDD_PATTERN_EDITOR_DLG"),i+1);
// 	AfxMessageBox(strTemp);


	//ksj 20200605
	int nMaxPatternRow = AfxGetApp()->GetProfileInt("Config","MAX_PATTERN_ROW",50000);
	if(nRowCnt > nMaxPatternRow && nMaxPatternRow != 0) //0이면 무시.
	{

		CString strErrorMsg;
		strErrorMsg.Format("The maximum number of rows has been exceeded. (%d, Max: %d)", nRowCnt, nMaxPatternRow);
		AfxMessageBox(strErrorMsg );
		return;
	}
	//ksj end

	//누적 시간이면 시간 역전 체크 
	if (m_cPatternTime == PS_PATTERN_TIME_ACUMULATE)
	{
		for( int i=0; i < nRowCnt ; i++)
		{
			strTemp.Format("%s",m_wndPatternGrid.GetValueRowCol(i+1, 1));
			if (Fun_TimeSaveError(strTemp) == FALSE)
			{
				//strTemp.Format("Row %d 에서 시간이 맞지 않습니다.",i+1);
				strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg1","IDD_PATTERN_EDITOR_DLG"),i+1);//&&
				AfxMessageBox(strTemp);
				return;
			}
		}
	}

	//온도이면 온도 역전 체크 
	float fTempOld=0.0f;
	float fTemp;
	if (m_cPatternTime == PS_PATTERN_TIME_TEMPERATURE)
	{
		for( int i=0; i < nRowCnt ; i++)
		{
			strTemp.Format("%s",m_wndPatternGrid.GetValueRowCol(i+1, 1));
			fTemp = atof(strTemp);
			if (i==0)
			{
				fTempOld = fTemp;
				continue;
			}
			if (fTempOld > fTemp)
			{
				//strTemp.Format("Row %d 에서 온도 값이 역전 되었습니다.",i+1);
				strTemp.Format("Row %d temperature value reversed",i+1);
				AfxMessageBox(strTemp);
				return;
			}
			fTempOld = fTemp;
		}
	}

	//+2015.9.21 USY Add For PatternCV(김재호K)
	//

	double dTemp = 0;
	//방전전류의 경우, 0보다 큰값이 나오면 아예 저장하기 전에 에러 띄우자. 충전전류는 0보다 작은값이 나오면 에러 띄우자.
	for( int i=0; i < nRowCnt ; i++)
	{
		for (j=1;j < nColCnt;j++)
		{
			if((m_chMode == PS_MODE_CV) && (j == PATTERN_COL_DATA3)) // PatternCV의 충전전류
			{
				dTemp = atof(m_wndPatternGrid.GetValueRowCol(i+1, j));
				if(dTemp < 0)
				{
					//strTemp.Format("Row %d 에서 충전전류값이 0보다 작습니다", i+1);
					strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg2","IDD_PATTERN_EDITOR_DLG"), i+1);//&&
					AfxMessageBox(strTemp);
					return;
				}
				if(dTemp > m_lMaxCurrent)
				{
					//strTemp.Format("Row %d 에서 충전전류값이 장비 설정 최대전류(%d (A))보다 큽니다.", i+1, m_lMaxCurrent);
					strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg3","IDD_PATTERN_EDITOR_DLG"), i+1, m_lMaxCurrent);//&&
					AfxMessageBox(strTemp);
					return;
				}
			}

			if((m_chMode == PS_MODE_CV) && (j == PATTERN_COL_DATA4)) // PatternCV의 방전전류
			{
				dTemp = atof(m_wndPatternGrid.GetValueRowCol(i+1, j));
				if(dTemp > 0)
				{
					//strTemp.Format("Row %d 에서 방전전류값이 0보다 큽니다", i+1);
					strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg4","IDD_PATTERN_EDITOR_DLG"), i+1);//&&
					AfxMessageBox(strTemp);
					return;
				}
				if( abs(dTemp) > m_lMaxCurrent)
				{
					//strTemp.Format("Row %d 에서 방전전류값이 장비 설정 최대전류(%d (A))보다 큽니다.", i+1, m_lMaxCurrent);
					strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg5","IDD_PATTERN_EDITOR_DLG"), i+1, m_lMaxCurrent);//&&
					AfxMessageBox(strTemp);
					return;
				}
			}		
			
			if((m_chMode == PS_MODE_CV) && (j == PATTERN_COL_DATA1)) // PatternCV의 전압
			{
				dTemp = atof(m_wndPatternGrid.GetValueRowCol(i+1, j));
				if( dTemp > m_lMaxVoltage)
				{
					//strTemp.Format("Row %d 에서 전압값이 장비 설정 최대전압(%d (V))보다 큽니다.", i+1, m_lMaxVoltage);
					strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg6","IDD_PATTERN_EDITOR_DLG"), i+1, m_lMaxVoltage);//&&
					AfxMessageBox(strTemp);
					return;
				}
			}	
		}
	}
	//-

	//if(IDNO ==MessageBox("Save Pattern File?", "NO", MB_YESNO|MB_ICONQUESTION)) return;//lyj 20200910 패턴

	//ksj 20200910
	//패턴을 적용하시겠습니까? 적용한 패턴은 스텝에 바로 저장됩니다.
	//Do you want to apply the pattern? The applied pattern is saved directly to the step.
	strTemp.Format(Fun_FindMsg("EditSimulDataDlg_OnOK_msg7","IDD_PATTERN_EDITOR_DLG"));
	if(IDNO ==MessageBox(strTemp, "Apply", MB_YESNO|MB_ICONQUESTION)) return;

	FILE *fp = fopen(m_strEditFileName, "wt"); //lyj 20200518 pattern파일 저장 버그 수정

	/*CString	strErrorMsg;
	CFileException e;
	CStdioFile aFile;

	if( !aFile.Open( m_strEditFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) ) //lyj 20200518 pattern파일 저장 버그 수정
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		//strErrorMsg.Format("%s 파일을 열 수 없습니다.", m_strEditFileName);
		strErrorMsg.Format(Fun_FindMsg("EditSimulDataDlg_LoadData_msg1","IDD_PATTERN_EDITOR_DLG"), m_strEditFileName);//&&
		AfxMessageBox(strErrorMsg);
		strTemp.Format("File Save error (%s)",m_strEditFileName);
		AfxMessageBox(strTemp);
		CDialog::OnCancel();
		return;
	}*/

	if(fp != NULL) //lyj 20200518 pattern파일 저장 버그 수정
	//else //lyj 20200518 pattern파일 저장 버그 수정 //ksj 20200820 : 주석처리
	{
		//주석 save
		//strSaveData = "여기는 주석 구문이 위치하는 곳입니다. 어떤 내용을 쓰던지 동작과는 전혀 상관없습니다.\n";
		//strSaveData += "아래 ""STX"" 로 부터는 실제로 Simulation 을 수행하는 데이터입니다.\n";
		strSaveData = "T1(ADD TIME),T2(WORK TIME),I(current),P(Power)\n\n";
		//strSaveData += ".,\n\n";
/*		
		
		strSaveData = "여기는 주석 구문이 위치하는 곳입니다. 어떤 내용을 쓰던지 동작과는 전혀 상관없습니다.\n";
		strSaveData += "아래 ""STX"" 로 부터는 실제로 Simulation 을 수행하는 데이터입니다.\n";
		strSaveData += "T1(누적시간),T2(동작시간),Temp(온도),I(전류),P(파워)\n";
		//strSaveData += "I-S(시작전류),I-E(종지전류),P-S(시작파워),P-E(종지파워),M(사각파:R  삼각파:T)\n";
		strSaveData += "수정시에는 포맷에 맞추어 작성하시기 바랍니다.,\n\n";
*/
		//stx save
		strSaveData += "STX\n";
		if (m_cPatternTime == PS_PATTERN_TIME_ACUMULATE)	
			strSaveData += "T1,";
		else if (m_cPatternTime == PS_PATTERN_TIME_OPERATING)	
			strSaveData += "T2,";
		else if (m_cPatternTime == PS_PATTERN_TIME_TEMPERATURE)	
			strSaveData += "Temp,";

		if (m_chMode == PS_MODE_CC)			strSaveData += "I\n";
		else if (m_chMode == PS_MODE_CP)	strSaveData += "P\n";	//ljb 20150122 add
		else if (m_chMode == PS_MODE_CV)							//ljb 20150122 add
			strSaveData += "V\n";
		else
			strSaveData += "Error\n";
		
		fprintf(fp, "%s", strSaveData);	//File Write //lyj 20200518 pattern파일 저장 버그 수정 //ksj 20200820 : 주석 해제
		//aFile.WriteString(strSaveData);	//lyj 20200518 pattern파일 저장 버그 수정
		strSaveData.Empty();

		//data save
		if (m_chMode == PS_MODE_CC || m_chMode == PS_MODE_CP) nColCnt = 3;
		for( int i=0; i < nRowCnt ; i++)
		{
			
			for (j=1;j < nColCnt;j++)
			{
//				if (j == PATTERN_COL_DATA2) continue;
//				if (j = PATTERN_COL_MODE) continue;
				//+2015.9.22 USY Add For PatternCV
				if(m_chMode == PS_MODE_CV)
				{
					if(j == PATTERN_COL_DATA2 ) continue;
				}

				strTemp.Format("%s,",m_wndPatternGrid.GetValueRowCol(i+1, j));
// 				if (j == 1 && m_cPatternTime == PS_PATTERN_TIME_ACUMULATE)
// 				{
// 					if (Fun_TimeSaveError(strTemp) == FALSE)
// 					{
// 						strTemp.Format("Row %d 에서 시간이 맞지 않습니다.",nRowCnt);
// 						AfxMessageBox(strTemp);
// 						fclose(fp);
// 						fp = NULL;
// 						DeleteFile(m_strEditFileName);
// 						return;
// 					}
// 				}
				if (strTemp != "" ) strSaveData += strTemp;
			}
// 			strTemp.Format("%s",m_wndPatternGrid.GetValueRowCol(i+1, j));
// 			if (strTemp == "1")
// 				strTemp = "T\n";
// 			else
// 				strTemp = "R\n";
// 			strSaveData += strTemp;
			strSaveData = strSaveData.Left(strSaveData.GetLength()-1);
			strSaveData += "\n";

			if ((i % 1000) == 0)
			{
				fprintf(fp, "%s", strSaveData);	//File Write //lyj 20200518 pattern파일 저장 버그 수정 //ksj 20200820 : 주석 해제
				//aFile.WriteString(strSaveData); //lyj 20200518 pattern파일 저장 버그 수정
				strSaveData.Empty();
			}
		}
		fprintf(fp, "%s", strSaveData);	//File Write  //lyj 20200518 pattern파일 저장 버그 수정 //ksj 20200820 : 주석 해제
		//aFile.WriteString(strSaveData); //lyj 20200518 pattern파일 저장 버그 수정
		fclose(fp); //lyj 20200518 pattern파일 저장 버그 수정
		//aFile.Close(); //lyj 20200518 pattern파일 저장 버그 수정
		fp = NULL; //lyj 20200518 pattern파일 저장 버그 수정 //ksj 20200820 : 주석 해제
	}
// 	else
// 	{
// 		strTemp.Format("File Save error (%s)",m_strEditFileName);
// 		AfxMessageBox(strTemp);
// 		CDialog::OnCancel();
// 	}
//	fp = NULL;
	
	CDialog::OnOK();
}


void CEditSimulDataDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CEditSimulDataDlg::OnButAllSelect() 
{
	m_wndPatternGrid.SelectRange(CGXRange(1,0, m_wndPatternGrid.GetRowCount(), m_wndPatternGrid.GetColCount()));
}

void CEditSimulDataDlg::OnDeltaposEndTimeMsecSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here

	CString strData;
	GetDlgItem(IDC_END_MSEC_EDIT)->GetWindowText(strData);

	int mtime = atol(strData);

/*	
	//end time은 500msec 이하 설정은 하지 못함 
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime == (500-MIN_TIME_INTERVAL))	mtime = 0;
		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if( mtime == MIN_TIME_INTERVAL)	mtime = 500;
		if(mtime >= 1000)	mtime = 0;
	}
*/
	//end time은 0msec 부터 설정 가능
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if(mtime >= 1000)	mtime = 0;
	}

	strData.Format("%03d", mtime);
	GetDlgItem(IDC_END_MSEC_EDIT)->SetWindowText(strData);	
	*pResult = 0;	
}

void CEditSimulDataDlg::OnButTime() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CString strTemp;
	COleDateTime endTime;
	m_ctrlEndTime.GetTime(endTime);
	strTemp.Format("%02d:%02d:%02d.%02d",endTime.GetHour(),endTime.GetMinute(),endTime.GetSecond(),m_lEndMSec);

	CRowColArray ra;
	m_wndPatternGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
	{
		for (int i = ra.GetSize()-1; i >=0 ; i--)
		{
			if(ra[i] == 0) //Header 삭제불가
				continue;
			//m_wndPatternGrid.RemoveRows(ra[i], ra[i]);
			m_wndPatternGrid.SetStyleRange(CGXRange(ra[i] , PATTERN_COL_TIME), 	CGXStyle().SetValue(strTemp));
		}
// 		m_wndPatternGrid.InsertRows(ra[0] + 1, 1);
// 		m_wndPatternGrid.SetStyleRange(CGXRange(ra[0] + 1, PATTERN_COL_DATA2), 	CGXStyle().SetValue("0"));
// 		m_wndPatternGrid.SetStyleRange(CGXRange(ra[0] + 1, PATTERN_COL_DATA2), CGXStyle().SetEnabled(FALSE));
		IsChangeFlag(TRUE);	
	}
	else
	{
		//AfxMessageBox("시간을 입력할 Row를 선택 하세요.");
		AfxMessageBox(Fun_FindMsg("EditSimulDataDlg_OnButTime_msg1","IDD_PATTERN_EDITOR_DLG"));//&&
	}
	
// 	pStep->ulEndTime = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() + m_nEndDay * 86400)*100;		
// 		pStep->ulEndTime += (m_lEndMSec/10);
	UpdateData(TRUE);
}