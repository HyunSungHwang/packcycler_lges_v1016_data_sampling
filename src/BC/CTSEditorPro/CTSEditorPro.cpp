// CTSEditorPro.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CTSEditorPro.h"

#include "MainFrm.h"
#include "CTSEditorProDoc.h"
#include "CTSEditorProView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProApp

BEGIN_MESSAGE_MAP(CCTSEditorProApp, CWinApp)
	//{{AFX_MSG_MAP(CCTSEditorProApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_MENU_LANG_KOR, OnMenuLangKor)
	ON_COMMAND(ID_MENU_LANG_ENG, OnMenuLangEng)
	ON_UPDATE_COMMAND_UI(ID_MENU_LANG_KOR, OnUpdateLangKor)
	ON_UPDATE_COMMAND_UI(ID_MENU_LANG_ENG, OnUpdateLangEng)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProApp construction

CGlobal g_TabGlobal;

CCTSEditorProApp::CCTSEditorProApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCTSEditorProApp object

CCTSEditorProApp theApp;
CString g_strDataBaseName;

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProApp initialization

BOOL CCTSEditorProApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	AfxGetModuleState()->m_dwVersion = 0x0601;
	GXInit();
	
//	AfxGetModuleState()->m_dwVersion = 0x0601;
//	AfxDaoInit();

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.

#ifdef _CYCLER_		//Cycler version
	SetRegistryKey(_T("PNE CTSPack"));
#else
	SetRegistryKey(_T("PNE CTS"));		//formation version
#endif

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	CString strCurFolder = GetProfileString(EDITOR_REG_SECTION ,"Path");	//Get Current Folder(PowerFormation Folde)
	if(strCurFolder.IsEmpty())
	{
		AfxMessageBox("Cant find set Path.");
		char szBuff[_MAX_PATH];
		if(GetCurrentDirectory(_MAX_PATH, szBuff) < 1)	//Get Current Direcotry Name
		{
			sprintf(szBuff, "File Path Name Read Error %d\n", GetLastError());
			AfxMessageBox(szBuff);
			return FALSE;	
		}
		strCurFolder = szBuff;
//		if(WriteProfileString(strSection, "CTSEditorPro", strCurFolder) == 0)	return FALSE;		//Install Program에서 Path를 기록한다.
	}

	//DataBase 이름을 지정한다.
	strCurFolder = GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(PowerFormation Folde)
	
// 	int nLanguage;
// 
// 	nLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	if(strCurFolder.IsEmpty())
	{
// 		switch(nLanguage) 20190701
// 		{
// 		case 1: g_strDataBaseName.Format("%s\\DataBase\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME); break;
// 		case 2: g_strDataBaseName.Format("%s\\DataBase\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_EN);break;
// 		case 3: g_strDataBaseName.Format("%s\\DataBase\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_PL); break;
// 		default : g_strDataBaseName.Format("%s\\DataBase\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_EN); break;
// 		}

		g_strDataBaseName.Format("%s\\DataBase\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME);
	}
	else
	{
// 		switch(nLanguage) 20190701
// 		{
// 		case 1: g_strDataBaseName.Format("%s\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME); break;
// 		case 2: g_strDataBaseName.Format("%s\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_EN);break;
// 		case 3: g_strDataBaseName.Format("%s\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_PL); break;
// 		default : g_strDataBaseName.Format("%s\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME_EN); break;
// 		}

		g_strDataBaseName.Format("%s\\%s", strCurFolder, PS_SCHEDULE_DATABASE_NAME);
	}
	//CString strTemp;

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	//yulee 20190306 //yulee 20190625
	int nSelLanguage, nIDSel;
	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	switch(nSelLanguage)
	{
	case 1:
		{
			nIDSel = IDR_MAINFRAME;
			break;
		}
	case 2:
		{
			nIDSel = IDR_MAINFRAME_ENG;
			break;
		}
	case 3:
		{
			nIDSel = IDR_MAINFRAME_PL;
			break;
		}
	default:
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 1);
		nIDSel = IDR_MAINFRAME;
		break;
	}

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		nIDSel,
		RUNTIME_CLASS(CCTSEditorProDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CCTSEditorProView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	CString strArgu((char *)m_lpCmdLine);
	CString strModel, strTest;
	strModel = strArgu.Left(strArgu.Find(','));
	strTest = strArgu.Mid(strArgu.Find(',')+1);

	if(cmdInfo.m_nShellCommand == CCommandLineInfo::FileOpen && !cmdInfo.m_strFileName.IsEmpty())
	{
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;
		cmdInfo.m_strFileName.Empty();
	}

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	//m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);

#ifdef _DLG_MODEL_
	m_pMainWnd->ShowWindow(SW_HIDE);
#else
	m_pMainWnd->ShowWindow(SW_SHOW);		
#endif

	//Argument로 넘어온 모델의 조건을 기본적으로 Loading 한다.
	if(strModel.GetLength() >0 && strTest.GetLength() > 0)
	{
		POSITION pos = pDocTemplate->GetFirstDocPosition();
		if(pos)
		{
			CCTSEditorProDoc *myDoc =(CCTSEditorProDoc *)(pDocTemplate->GetNextDoc(pos));
			if( myDoc != NULL )
			{
				CCTSEditorProView *pView;
				POSITION pos1 = myDoc->GetFirstViewPosition();
				if(pos1)
				{
					pView = (CCTSEditorProView *)myDoc->GetNextView(pos1);
					if(pView)
					{
						pView->SetDefultTest(strModel, strTest);
					}
				}
			}
		}
	}	
	
	m_pMainWnd->UpdateWindow();

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();


// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX , IDD2 = IDD_ABOUTBOX_ENG };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CStatic info;

	afx_msg void OnMenuLangEng(CCmdUI *pCmdUI);
	afx_msg void OnMenuLangKor(CCmdUI *pCmdUI);
};

//CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
CAboutDlg::CAboutDlg() : CDialog(
								 (AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAboutDlg::IDD):
								 (AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAboutDlg::IDD2):
								CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC_ABOUT_INFO, info);
	
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCTSEditorProApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCTSEditorProApp message handlers


int CCTSEditorProApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	GXTerminate();
	return CWinApp::ExitInstance();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strMsg("CTS Editor");

#ifdef _CYCLER_
	strMsg = "PNE CTSPro Editor";
#else
	strMsg = "PNE CTS Editor";
#endif

#ifdef _EDLC_CELL_
	strMsg += " [Cell type EDLC]";
#else
	strMsg += " [Cell type Li-ion]";
#endif


	GetDlgItem(IDC_TYPE_STATIC)->SetWindowText(strMsg);
	info.SetWindowTextA(PROGRAM_DATE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CCTSEditorProApp::OnAppExit() 
{
	// TODO: Add your command handler code here
	CWinApp::OnAppExit();
}

CString CCTSEditorProApp::str_BTrim(CString str)
{//9 공백 없애기
	str.TrimLeft();
	str.TrimRight();
	
	return str;
}

CString CCTSEditorProApp::str_GetFile(CString strPath,char ch)
{//38 뒷쪽		
	strPath.TrimRight(ch);
	
	int nPos=strPath.ReverseFind(ch);
	if(nPos>-1)
	{
		strPath = strPath.Mid(nPos+1,strPath.GetLength()-(nPos+1));	
	}
	
	return strPath;
}

CString CCTSEditorProApp::str_GetPath(CString strPath,char ch)
{//36 앞쪽		
	strPath.TrimRight(ch);
	
	int nPos=strPath.ReverseFind(ch);
	if(nPos>-1)
	{
		strPath = strPath.Mid(0,nPos+1);	
	}
	
	return strPath;
}

CString CCTSEditorProApp::str_GetFileExt(CString strName)
{//37		
	strName=str_BTrim(strName);
	
	int nPos=strName.ReverseFind('.');
	if(nPos>-1)
	{
		strName = strName.Mid(nPos+1,strName.GetLength()-(nPos+1));	
	}
	
	return strName;
}

CString CCTSEditorProApp::str_IntToStr(int num,int pt)
{//14 자릿수 
	CString str=_T("");
	
	if(pt<2)   str.Format(_T("%d")   ,num);
	if(pt==2)  str.Format(_T("%02d") ,num);
	if(pt==3)  str.Format(_T("%03d") ,num);
	if(pt==4)  str.Format(_T("%04d") ,num);
	if(pt==5)  str.Format(_T("%05d") ,num);
	if(pt==6)  str.Format(_T("%06d") ,num);
	if(pt==7)  str.Format(_T("%07d") ,num);
	if(pt==8)  str.Format(_T("%08d") ,num);
	if(pt==9)  str.Format(_T("%09d") ,num);
	if(pt==10) str.Format(_T("%010d"),num);
	
	return str;
}

int CCTSEditorProApp::str_Count(CString tmpstr,CString searchstr)
{//10 문자열에 특정 문자 갯수	
	int tcount=0;
	int dcount=-1;
	
	for(int i=0; i<tmpstr.GetLength();i++)
	{
		dcount=tmpstr.Find(searchstr,dcount+1);
		if(dcount>-1) ++tcount;
		else if(dcount==-1) break;
	}	
	return tcount;
}

CString CCTSEditorProApp::str_GetSplit(CString str,int count,char sp,BOOL bTrim)
{//11 문자기준으로 문자열 쪽개기
	if(str.IsEmpty()) return _T("");
	int item=str_Count(str,(CString)sp)+1;
	
	if(item<count) return _T("");
	
	CString tmp;
	AfxExtractSubString(tmp,str,count,sp);
	
	if(bTrim==TRUE)	return str_BTrim(tmp);
	
	return tmp;
}

CString CCTSEditorProApp::win_GetText(HWND hWnd)
{//14 윈도우 텍스트 구하기
	if(hWnd==NULL) return _T("");
	
	#ifdef _UNICODE			   
		static wchar_t buf[MAX_PATH];
		::GetWindowText(hWnd,(LPWSTR)&buf, MAX_PATH);
	#else
		TCHAR buf[MAX_PATH];
		::GetWindowText(hWnd,(LPSTR)&buf, MAX_PATH);
	#endif	
	
	return (CString)buf;
}

CString CCTSEditorProApp::win_GetText(CWnd *wnd)
{//15 윈도우 텍스트 구하기
	if(!wnd) return _T("");
	
	CString str=_T("");
	wnd->GetWindowText(str);
	
	return str;
}

CString CCTSEditorProApp::win_CurrentTime(CString sort)
{//76
	SYSTEMTIME now;
	CString str_time=_T("");
	GetLocalTime(&now);
	
	#ifdef _UNICODE			   			
		wchar_t *strDay[]={_T("SUN"),_T("MON"),_T("TUE"),_T("WED"),_T("THU"),_T("FRI"),_T("SAT")};
	#else
		char *strDay[]={_T("SUN"),_T("MON"),_T("TUE"),_T("WED"),_T("THU"),_T("FRI"),_T("SAT")};	
	#endif	
	
	if(sort==_T("ftp"))
	{
		str_time.Format(_T("%2d_%2d_%2d_%2d_%2d"),
			now.wMonth,now.wDay,
			now.wHour,now.wMinute,now.wSecond);
		str_time.Replace(_T(" "),_T("0"));
	} 
	else if(sort==_T("scen"))
	{
		str_time.Format(_T("%02d:%02d"),now.wHour,now.wMinute);
	} 
	else if(sort==_T("week"))
	{		   
		str_time=strDay[now.wDayOfWeek];
		str_time.MakeLower();
	}
	else if(sort==_T("iweek"))
	{
		str_time.Format(_T("%d"),now.wDayOfWeek+1);//1(sunday)부터 시작
	}
	else if(sort==_T("short"))
	{
		str_time.Format(_T("%d_%d_%d_%d"),now.wMonth,now.wDay,now.wHour,now.wMinute);
	}
	else if(sort==_T("full"))
	{
		str_time.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);
	}
	else if(sort==_T("fulllog"))
	{
		str_time.Format(_T("[%04d-%02d-%02d %02d:%02d:%02d]"),now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);
	}
	else if(sort==_T("fulllogm"))
	{
		str_time.Format(_T("[%04d-%02d-%02d %02d:%02d:%02d:%03d]"),now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond,now.wMilliseconds);
	}
	else if(sort==_T("filelog"))
	{
		str_time.Format(_T("%04d-%02d-%02d"),now.wYear,now.wMonth,now.wDay);
	}
	else if(sort==_T("tblog"))
	{
		str_time.Format(_T("%04d%02d"),now.wYear,now.wMonth);
		str_time.Delete(0,2);
	}
	else if(sort==_T("iday"))
	{
		str_time.Format(_T("%02d"),now.wDay);
	} 
	else if(sort==_T("ifulllog"))
	{
		str_time.Format(_T("%04d%02d%02d%02d%02d%02d"),now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond);
	}
	return str_time;
}


int CCTSEditorProApp::win_CboGetDataItemFind(CComboBox *cbo,int iVal,BOOL bFocus)
{//69
	int nCount=cbo->GetCount();
	if(nCount<1) return -1;
	
	for(int i=0;i<nCount;i++)
	{
		int dVal=cbo->GetItemData(i);
		if(dVal==iVal) 
		{
			if(bFocus==TRUE) cbo->SetCurSel(i);
			return i;
		}
	}
	return -1;
}
int CCTSEditorProApp::win_CboGetFind(CComboBox *cbo,CString str)
{//69
	   int focus=cbo->FindString(-1,str);
	   if(focus<0) focus=0;
	   cbo->SetCurSel(focus);
	   
	   return focus;
}
	
 CString CCTSEditorProApp::win_CboGetString(CComboBox *cbo,CString blank)
{//68
	CString value;
	cbo->GetLBText(cbo->GetCurSel(),value);
	
	if(blank!=_T("") && value==blank) value=_T("");
	
	return value;
	}

BOOL CCTSEditorProApp::file_ForceDirectory(LPCTSTR lpDirectory)
{//폴더 생성
	CString szDirectory = lpDirectory;
	szDirectory.TrimRight(_T("\\"));
	
	if ((file_GetPath(szDirectory) == szDirectory) ||
		(file_Exists(szDirectory) == -1))
		return TRUE;
	if (!file_ForceDirectory(file_GetPath(szDirectory)))
		return FALSE;
	if (!CreateDirectory(szDirectory, NULL))
		return FALSE;
	return TRUE;
}

CString CCTSEditorProApp::file_GetPath(LPCTSTR lpszFilePath)
{//전체 파일에서 경로만 구함
	TCHAR szDir[_MAX_DIR];
	TCHAR szDrive[_MAX_DRIVE];
	_tsplitpath(lpszFilePath, szDrive, szDir, NULL, NULL);
	
	return  CString(szDrive) + CString(szDir);
}

int CCTSEditorProApp::file_Exists(LPCTSTR lpszName)
{//파일 & 디렉토리 이 존재 하는지 체크
	CFileFind fileFind;
	if (!fileFind.FindFile(lpszName))
	{
		if (file_DirectoryExists(lpszName))  // if root ex. "C:\"
			return -1;
		return 0;
	}
	fileFind.FindNextFile();
	return fileFind.IsDirectory() ? -1 : 1;
} 

BOOL CCTSEditorProApp::file_DirectoryExists(LPCTSTR lpszDir)
{//디렉토리 존재 여부
	#ifdef _UNICODE			    			
		wchar_t curPath[_MAX_PATH];   // Get the current working directory:
		
		if (!_wgetcwd(curPath, _MAX_PATH))
		{
			return FALSE;
		}
		if (_wchdir(lpszDir))	// retruns 0 if error
		{
			return FALSE;
		}
		_wchdir(curPath);
	#else
		char curPath[_MAX_PATH];   // Get the current working directory:
		
		if (!_getcwd(curPath, _MAX_PATH))
		{
			return FALSE;
		}
		if (_chdir(lpszDir))	// retruns 0 if error
		{
			return FALSE;
		}
		_chdir(curPath); 
	#endif	
	
	return TRUE;
}

void CCTSEditorProApp::file_DeletefileList(CString path)
{//폴더에 있는 파일 지우기	        
	CFileFind finder;
	DWORD fsize=0;
	BOOL bWorking = finder.FindFile(path);
	
	while (bWorking)
	{	  
		bWorking = finder.FindNextFile();	
		
		if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		if(finder.GetFileName()==_T("Thumbs.db")) continue;
		
		if(finder.IsDirectory()) continue;		   
		 
		::DeleteFile(finder.GetFilePath());		
	}
	finder.Close();
}

CString CCTSEditorProApp::GetValueStrf(float fvalue,int n)
{
	float rfvalue=0;
	if(n==1)      rfvalue=(float)(fvalue*10)/10;
	else if(n==2) rfvalue=(float)(fvalue*100)/100;
	else if(n==3) rfvalue=(float)(fvalue*1000)/1000;
	else if(n==4) rfvalue=(float)(fvalue*10000)/10000;
	else if(n==5) rfvalue=(float)(fvalue*100000)/100000;
	else if(n==0) rfvalue=(float)(fvalue*1000)/1000;
	else          rfvalue=fvalue;
	
	if(rfvalue==0) rfvalue=0;
	
	CString str;
	if(n==1)      str.Format(_T("%.1f"),rfvalue);
	else if(n==2) str.Format(_T("%.2f"),rfvalue);
	else if(n==3) str.Format(_T("%.3f"),rfvalue);
	else if(n==4) str.Format(_T("%.4f"),rfvalue);
	else if(n==5) str.Format(_T("%.5f"),rfvalue);
	else if(n==0) str.Format(_T("%.3f"),rfvalue); 
	else          str.Format(_T("%f"),rfvalue); 
	
	return str;
}


void CCTSEditorProApp::OnMenuLangKor() 
{
	CString tmpStr;
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 1);
	if(bSelectedLanguage == 1)
	{
		tmpStr.Format("한국어가 적용되어 있습니다.");
		AfxMessageBox(tmpStr);//&&	
		return;
	}
	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 1);

	if(bChangeToEng == TRUE)
	{
		tmpStr.Format("프로그램 재 시작 시 한국어가 적용됩니다.");
		AfxMessageBox(tmpStr);//&&
	}
}

void CCTSEditorProApp::OnMenuLangEng()//2-English
{
	CString tmpStr;
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 2);
	if(bSelectedLanguage == 2)
	{
		tmpStr.Format("The language is already applied.");
		AfxMessageBox(tmpStr);//&&	
		return;
	}
	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 2);

	if(bChangeToEng == TRUE)
	{
		tmpStr.Format("English will be applied after restart of this program.");
		AfxMessageBox(tmpStr);//&&
	}
}

void CCTSEditorProApp::OnUpdateLangKor(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CCTSEditorProApp::OnUpdateLangEng(CCmdUI* pCmdUI) 
{
	//pCmdUI->Enable(FALSE);
	pCmdUI->Enable(TRUE); //lyj 20200909 english 버튼 활성화
}
