#if !defined(AFX_MYSHEET_H__2AE03E2C_2C4E_4248_9E17_BDFF611FD5DA__INCLUDED_)
#define AFX_MYSHEET_H__2AE03E2C_2C4E_4248_9E17_BDFF611FD5DA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MySheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMySheet
#include <afxtempl.h>

class CMySheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CMySheet)

// Construction
public:
	//CMySheet(CWnd* pParentWnd = NULL);
	CMySheet(CWnd* pParentWnd = NULL);
	virtual ~CMySheet();

// Attributes
public:

// Operations
public:
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMySheet)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
public:
	void Fun_SetStepNo(int iStepNo);
	void UpdateEndCondition();
	void Fun_SetActive(UINT iPage);
	void Fun_SetpDoc();
	void EnablePage (int nPage, BOOL bEnable = TRUE);
	
	CWnd *m_pParent;
// 	CCTSEditorProDoc *m_pDoc;
	// Generated message map functions
protected:
	// we save the current page in TCN_SELCHANGING
	int m_nLastActive;
	// list of indexes of disabled pages
	CMap <int, int&, int, int&> m_DisabledPages;
	//{{AFX_MSG(CMySheet)
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYSHEET_H__2AE03E2C_2C4E_4248_9E17_BDFF611FD5DA__INCLUDED_)
