// CommSafetyCanAux.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "CommSafetyCanAux.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommSafetyCanAux dialog
#define		ID_CAN_NAME		1
#define		ID_AUX_NAME		2

CCommSafetyCanAux::CCommSafetyCanAux(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCommSafetyCanAux::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCommSafetyCanAux::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CCommSafetyCanAux::IDD3):
	(CCommSafetyCanAux::IDD), pParent)
{
	//{{AFX_DATA_INIT(CCommSafetyCanAux)
	m_fCanValue_1 = 0.0f;
	m_fCanValue_2 = 0.0f;
	m_fCanValue_3 = 0.0f;
	m_fCanValue_4 = 0.0f;
	m_fCanValue_5 = 0.0f;
	m_fCanValue_6 = 0.0f;
	m_fCanValue_7 = 0.0f;
	m_fCanValue_8 = 0.0f;
	m_fCanValue_9 = 0.0f;
	m_fCanValue_10 = 0.0f;
	m_fAuxValue_2 = 0.0f;
	m_fAuxValue_3 = 0.0f;
	m_fAuxValue_4 = 0.0f;
	m_fAuxValue_5 = 0.0f;
	m_fAuxValue_6 = 0.0f;
	m_fAuxValue_7 = 0.0f;
	m_fAuxValue_8 = 0.0f;
	m_fAuxValue_9 = 0.0f;
	m_fAuxValue_10 = 0.0f;
	m_fAuxValue_1 = 0.0f;
	//}}AFX_DATA_INIT
}


void CCommSafetyCanAux::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommSafetyCanAux)
	DDX_Control(pDX, IDC_CBO_AUX_CODE_10, m_ctrlCboAuxCode_10);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_9, m_ctrlCboAuxCode_9);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_8, m_ctrlCboAuxCode_8);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_7, m_ctrlCboAuxCode_7);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_6, m_ctrlCboAuxCode_6);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_5, m_ctrlCboAuxCode_5);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_4, m_ctrlCboAuxCode_4);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_3, m_ctrlCboAuxCode_3);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_2, m_ctrlCboAuxCode_2);
	DDX_Control(pDX, IDC_CBO_AUX_CODE_1, m_ctrlCboAuxCode_1);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_10, m_ctrlCboCanCode_10);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_9, m_ctrlCboCanCode_9);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_8, m_ctrlCboCanCode_8);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_7, m_ctrlCboCanCode_7);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_6, m_ctrlCboCanCode_6);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_5, m_ctrlCboCanCode_5);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_4, m_ctrlCboCanCode_4);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_3, m_ctrlCboCanCode_3);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_2, m_ctrlCboCanCode_2);
	DDX_Control(pDX, IDC_CBO_CAN_CODE_1, m_ctrlCboCanCode_1);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_10, m_ctrlAuxDataType_10);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_9, m_ctrlAuxDataType_9);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_8, m_ctrlAuxDataType_8);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_7, m_ctrlAuxDataType_7);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_6, m_ctrlAuxDataType_6);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_5, m_ctrlAuxDataType_5);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_4, m_ctrlAuxDataType_4);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_3, m_ctrlAuxDataType_3);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_2, m_ctrlAuxDataType_2);
	DDX_Control(pDX, IDC_CBO_AUX_VALUE_TYPE_1, m_ctrlAuxDataType_1);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_10, m_ctrlCanDataType_10);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_9, m_ctrlCanDataType_9);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_8, m_ctrlCanDataType_8);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_7, m_ctrlCanDataType_7);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_6, m_ctrlCanDataType_6);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_5, m_ctrlCanDataType_5);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_4, m_ctrlCanDataType_4);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_3, m_ctrlCanDataType_3);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_2, m_ctrlCanDataType_2);
	DDX_Control(pDX, IDC_CBO_CAN_VALUE_TYPE_1, m_ctrlCanDataType_1);
	DDX_Control(pDX, IDC_CBO_AUX_SET10, m_ctrlAuxCompare_10);
	DDX_Control(pDX, IDC_CBO_AUX_SET9, m_ctrlAuxCompare_9);
	DDX_Control(pDX, IDC_CBO_AUX_SET8, m_ctrlAuxCompare_8);
	DDX_Control(pDX, IDC_CBO_AUX_SET7, m_ctrlAuxCompare_7);
	DDX_Control(pDX, IDC_CBO_AUX_SET6, m_ctrlAuxCompare_6);
	DDX_Control(pDX, IDC_CBO_AUX_SET5, m_ctrlAuxCompare_5);
	DDX_Control(pDX, IDC_CBO_AUX_SET4, m_ctrlAuxCompare_4);
	DDX_Control(pDX, IDC_CBO_AUX_SET3, m_ctrlAuxCompare_3);
	DDX_Control(pDX, IDC_CBO_AUX_SET2, m_ctrlAuxCompare_2);
	DDX_Control(pDX, IDC_CBO_AUX_SET1, m_ctrlAuxCompare_1);
	DDX_Control(pDX, IDC_CBO_CAN_SET10, m_ctrlCanCompare_10);
	DDX_Control(pDX, IDC_CBO_CAN_SET9, m_ctrlCanCompare_9);
	DDX_Control(pDX, IDC_CBO_CAN_SET8, m_ctrlCanCompare_8);
	DDX_Control(pDX, IDC_CBO_CAN_SET7, m_ctrlCanCompare_7);
	DDX_Control(pDX, IDC_CBO_CAN_SET6, m_ctrlCanCompare_6);
	DDX_Control(pDX, IDC_CBO_CAN_SET5, m_ctrlCanCompare_5);
	DDX_Control(pDX, IDC_CBO_CAN_SET4, m_ctrlCanCompare_4);
	DDX_Control(pDX, IDC_CBO_CAN_SET3, m_ctrlCanCompare_3);
	DDX_Control(pDX, IDC_CBO_CAN_SET2, m_ctrlCanCompare_2);
	DDX_Control(pDX, IDC_CBO_CAN_SET1, m_ctrlCanCompare_1);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE1, m_fCanValue_1);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE2, m_fCanValue_2);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE3, m_fCanValue_3);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE4, m_fCanValue_4);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE5, m_fCanValue_5);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE6, m_fCanValue_6);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE7, m_fCanValue_7);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE8, m_fCanValue_8);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE9, m_fCanValue_9);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE10, m_fCanValue_10);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE2, m_fAuxValue_2);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE3, m_fAuxValue_3);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE4, m_fAuxValue_4);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE5, m_fAuxValue_5);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE6, m_fAuxValue_6);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE7, m_fAuxValue_7);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE8, m_fAuxValue_8);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE9, m_fAuxValue_9);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE10, m_fAuxValue_10);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE1, m_fAuxValue_1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCommSafetyCanAux, CDialog)
	//{{AFX_MSG_MAP(CCommSafetyCanAux)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_2, OnSelchangeCboCanCode2)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_10, OnSelchangeCboCanCode10)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_1, OnSelchangeCboCanCode1)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_3, OnSelchangeCboCanCode3)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_4, OnSelchangeCboCanCode4)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_5, OnSelchangeCboCanCode5)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_6, OnSelchangeCboCanCode6)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_7, OnSelchangeCboCanCode7)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_8, OnSelchangeCboCanCode8)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CODE_9, OnSelchangeCboCanCode9)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_1, OnSelchangeCboAuxCode1)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_2, OnSelchangeCboAuxCode2)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_3, OnSelchangeCboAuxCode3)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_4, OnSelchangeCboAuxCode4)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_5, OnSelchangeCboAuxCode5)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_6, OnSelchangeCboAuxCode6)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_7, OnSelchangeCboAuxCode7)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_8, OnSelchangeCboAuxCode8)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_9, OnSelchangeCboAuxCode9)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_CODE_10, OnSelchangeCboAuxCode10)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommSafetyCanAux message handlers

void CCommSafetyCanAux::Fun_DisplayData()
{
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&m_pDoc->m_sPreTestParam);
	UpdateData();
	//ljb pParam->ican_function_division[0]; 이 값으로 콤보박스 표시 한다 
	if (pParam->ican_function_division[0] == 0)
	{
		m_ctrlCboCanCode_1.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE1)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_1)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET1)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,1, pParam->ican_function_division[0]);

	if (pParam->ican_function_division[1] == 0)
	{
		m_ctrlCboCanCode_2.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE2)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_2)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET2)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,2, pParam->ican_function_division[1]);

	if (pParam->ican_function_division[2] == 0)
	{
		m_ctrlCboCanCode_3.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE3)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_3)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET3)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,3, pParam->ican_function_division[2]);
	
	if (pParam->ican_function_division[3] == 0)
	{
		m_ctrlCboCanCode_4.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE4)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_4)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET4)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,4, pParam->ican_function_division[3]);
	
	if (pParam->ican_function_division[4] == 0)
	{
		m_ctrlCboCanCode_5.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE5)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_5)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET5)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,5, pParam->ican_function_division[4]);
		
	if (pParam->ican_function_division[5] == 0)
	{
		m_ctrlCboCanCode_6.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE6)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_6)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET6)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,6, pParam->ican_function_division[5]);
	
	if (pParam->ican_function_division[6] == 0)
	{
		m_ctrlCboCanCode_7.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE7)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_7)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET7)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,7, pParam->ican_function_division[6]);
	
	if (pParam->ican_function_division[7] == 0)
	{
		m_ctrlCboCanCode_8.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE8)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_8)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET8)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,8, pParam->ican_function_division[7]);
	
	if (pParam->ican_function_division[8] == 0)
	{
		m_ctrlCboCanCode_9.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE9)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_9)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET9)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,9, pParam->ican_function_division[8]);
	
	if (pParam->ican_function_division[9] == 0)
	{
		m_ctrlCboCanCode_10.SetCurSel(0);
		GetDlgItem(IDC_EDIT_CAN_VALUE10)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_10)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_CAN_SET10)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_CAN_NAME,10, pParam->ican_function_division[9]);
	
	m_fCanValue_1 = pParam->fcan_Value[0];
	m_fCanValue_2 = pParam->fcan_Value[1];
	m_fCanValue_3 = pParam->fcan_Value[2];
	m_fCanValue_4 = pParam->fcan_Value[3];
	m_fCanValue_5 = pParam->fcan_Value[4];
	m_fCanValue_6 = pParam->fcan_Value[5];
	m_fCanValue_7 = pParam->fcan_Value[6];
	m_fCanValue_8 = pParam->fcan_Value[7];
	m_fCanValue_9 = pParam->fcan_Value[8];
	m_fCanValue_10 = pParam->fcan_Value[9];

	//ljb pParam->iaux_function_division[0]; 이 값으로 콤보박스 표시 한다 
	if (pParam->iaux_function_division[0] == 0)
	{
		m_ctrlCboAuxCode_1.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE1)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_1)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET1)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,1, pParam->iaux_function_division[0]);
	
	if (pParam->iaux_function_division[1] == 0)
	{
		m_ctrlCboAuxCode_2.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE2)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_2)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET2)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,2, pParam->iaux_function_division[1]);
	
	if (pParam->iaux_function_division[2] == 0)
	{
		m_ctrlCboAuxCode_3.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE3)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_3)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET3)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,3, pParam->iaux_function_division[2]);
	
	if (pParam->iaux_function_division[3] == 0)
	{
		m_ctrlCboAuxCode_4.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE4)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_4)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET4)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,4, pParam->iaux_function_division[3]);
	
	if (pParam->iaux_function_division[4] == 0)
	{
		m_ctrlCboAuxCode_5.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE5)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_5)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET5)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,5, pParam->iaux_function_division[4]);
	
	if (pParam->iaux_function_division[5] == 0)
	{
		m_ctrlCboAuxCode_6.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE6)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_6)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET6)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,6, pParam->iaux_function_division[5]);
	
	if (pParam->iaux_function_division[6] == 0)
	{
		m_ctrlCboAuxCode_7.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE7)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_7)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET7)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,7, pParam->iaux_function_division[6]);
	
	if (pParam->iaux_function_division[7] == 0)
	{
		m_ctrlCboAuxCode_8.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE8)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_8)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET8)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,8, pParam->iaux_function_division[7]);
	
	if (pParam->iaux_function_division[8] == 0)
	{
		m_ctrlCboAuxCode_9.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE9)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_9)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET9)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,9, pParam->iaux_function_division[8]);
	
	if (pParam->iaux_function_division[9] == 0)
	{
		m_ctrlCboAuxCode_10.SetCurSel(0);
		GetDlgItem(IDC_EDIT_AUX_VALUE10)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_10)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_AUX_SET10)->EnableWindow(FALSE);
	}
	else
		Fun_FindCanAuxName(ID_AUX_NAME,10, pParam->iaux_function_division[9]);

	m_fAuxValue_1 = pParam->faux_Value[0];
	m_fAuxValue_2 = pParam->faux_Value[1];
	m_fAuxValue_3 = pParam->faux_Value[2];
	m_fAuxValue_4 = pParam->faux_Value[3];
	m_fAuxValue_5 = pParam->faux_Value[4];
	m_fAuxValue_6 = pParam->faux_Value[5];
	m_fAuxValue_7 = pParam->faux_Value[6];
	m_fAuxValue_8 = pParam->faux_Value[7];
	m_fAuxValue_9 = pParam->faux_Value[8];
	m_fAuxValue_10 = pParam->faux_Value[9];

	m_ctrlCanDataType_1.SetCurSel(pParam->cCan_data_type[0]);
	m_ctrlCanDataType_2.SetCurSel(pParam->cCan_data_type[1]);
	m_ctrlCanDataType_3.SetCurSel(pParam->cCan_data_type[2]);
	m_ctrlCanDataType_4.SetCurSel(pParam->cCan_data_type[3]);
	m_ctrlCanDataType_5.SetCurSel(pParam->cCan_data_type[4]);
	m_ctrlCanDataType_6.SetCurSel(pParam->cCan_data_type[5]);
	m_ctrlCanDataType_7.SetCurSel(pParam->cCan_data_type[6]);
	m_ctrlCanDataType_8.SetCurSel(pParam->cCan_data_type[7]);
	m_ctrlCanDataType_9.SetCurSel(pParam->cCan_data_type[8]);
	m_ctrlCanDataType_10.SetCurSel(pParam->cCan_data_type[9]);

	m_ctrlAuxDataType_1.SetCurSel(pParam->cAux_data_type[0]);
	m_ctrlAuxDataType_2.SetCurSel(pParam->cAux_data_type[1]);
	m_ctrlAuxDataType_3.SetCurSel(pParam->cAux_data_type[2]);
	m_ctrlAuxDataType_4.SetCurSel(pParam->cAux_data_type[3]);
	m_ctrlAuxDataType_5.SetCurSel(pParam->cAux_data_type[4]);
	m_ctrlAuxDataType_6.SetCurSel(pParam->cAux_data_type[5]);
	m_ctrlAuxDataType_7.SetCurSel(pParam->cAux_data_type[6]);
	m_ctrlAuxDataType_8.SetCurSel(pParam->cAux_data_type[7]);
	m_ctrlAuxDataType_9.SetCurSel(pParam->cAux_data_type[8]);
	m_ctrlAuxDataType_10.SetCurSel(pParam->cAux_data_type[9]);

	m_ctrlCanCompare_1.SetCurSel(pParam->cCan_compare_type[0]);
	m_ctrlCanCompare_2.SetCurSel(pParam->cCan_compare_type[1]);
	m_ctrlCanCompare_3.SetCurSel(pParam->cCan_compare_type[2]);
	m_ctrlCanCompare_4.SetCurSel(pParam->cCan_compare_type[3]);
	m_ctrlCanCompare_5.SetCurSel(pParam->cCan_compare_type[4]);
	m_ctrlCanCompare_6.SetCurSel(pParam->cCan_compare_type[5]);
	m_ctrlCanCompare_7.SetCurSel(pParam->cCan_compare_type[6]);
	m_ctrlCanCompare_8.SetCurSel(pParam->cCan_compare_type[7]);
	m_ctrlCanCompare_9.SetCurSel(pParam->cCan_compare_type[8]);
	m_ctrlCanCompare_10.SetCurSel(pParam->cCan_compare_type[9]);

	m_ctrlAuxCompare_1.SetCurSel(pParam->cAux_compare_type[0]);
	m_ctrlAuxCompare_2.SetCurSel(pParam->cAux_compare_type[1]);
	m_ctrlAuxCompare_3.SetCurSel(pParam->cAux_compare_type[2]);
	m_ctrlAuxCompare_4.SetCurSel(pParam->cAux_compare_type[3]);
	m_ctrlAuxCompare_5.SetCurSel(pParam->cAux_compare_type[4]);
	m_ctrlAuxCompare_6.SetCurSel(pParam->cAux_compare_type[5]);
	m_ctrlAuxCompare_7.SetCurSel(pParam->cAux_compare_type[6]);
	m_ctrlAuxCompare_8.SetCurSel(pParam->cAux_compare_type[7]);
	m_ctrlAuxCompare_9.SetCurSel(pParam->cAux_compare_type[8]);
	m_ctrlAuxCompare_10.SetCurSel(pParam->cAux_compare_type[9]);
	
	UpdateData(FALSE);
}

void CCommSafetyCanAux::InitControl()
{
	int i;
	for( int i=0; i< 7; i++)
	{
		m_ctrlCanDataType_1.SetItemData(i,i);
		m_ctrlCanDataType_2.SetItemData(i,i);
		m_ctrlCanDataType_3.SetItemData(i,i);
		m_ctrlCanDataType_4.SetItemData(i,i);
		m_ctrlCanDataType_5.SetItemData(i,i);
		m_ctrlCanDataType_6.SetItemData(i,i);
		m_ctrlCanDataType_7.SetItemData(i,i);
		m_ctrlCanDataType_8.SetItemData(i,i);
		m_ctrlCanDataType_9.SetItemData(i,i);
		m_ctrlCanDataType_10.SetItemData(i,i);

		m_ctrlAuxDataType_1.SetItemData(i,i);
		m_ctrlAuxDataType_2.SetItemData(i,i);
		m_ctrlAuxDataType_3.SetItemData(i,i);
		m_ctrlAuxDataType_4.SetItemData(i,i);
		m_ctrlAuxDataType_5.SetItemData(i,i);
		m_ctrlAuxDataType_6.SetItemData(i,i);
		m_ctrlAuxDataType_7.SetItemData(i,i);
		m_ctrlAuxDataType_8.SetItemData(i,i);
		m_ctrlAuxDataType_9.SetItemData(i,i);
		m_ctrlAuxDataType_10.SetItemData(i,i);
		
		m_ctrlCanCompare_1.SetItemData(i,i);
		m_ctrlCanCompare_2.SetItemData(i,i);
		m_ctrlCanCompare_3.SetItemData(i,i);
		m_ctrlCanCompare_4.SetItemData(i,i);
		m_ctrlCanCompare_5.SetItemData(i,i);
		m_ctrlCanCompare_6.SetItemData(i,i);
		m_ctrlCanCompare_7.SetItemData(i,i);
		m_ctrlCanCompare_8.SetItemData(i,i);
		m_ctrlCanCompare_9.SetItemData(i,i);
		m_ctrlCanCompare_10.SetItemData(i,i);

		m_ctrlAuxCompare_1.SetItemData(i,i);
		m_ctrlAuxCompare_2.SetItemData(i,i);
		m_ctrlAuxCompare_3.SetItemData(i,i);
		m_ctrlAuxCompare_4.SetItemData(i,i);
		m_ctrlAuxCompare_5.SetItemData(i,i);
		m_ctrlAuxCompare_6.SetItemData(i,i);
		m_ctrlAuxCompare_7.SetItemData(i,i);
		m_ctrlAuxCompare_8.SetItemData(i,i);
		m_ctrlAuxCompare_9.SetItemData(i,i);
		m_ctrlAuxCompare_10.SetItemData(i,i);
	}
}

BOOL CCommSafetyCanAux::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("OnInitDialog_msg1","IDD_DLG_COMM_SAFETY"));

	//if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");

	InitComboBox();		//ljb 2011218 이재복 //////////
	InitControl();
//	SetTimer(1000, 500, NULL);
	Fun_DisplayData();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCommSafetyCanAux::OnOK() 
{
	// TODO: Add extra validation here
	TEST_PARAM *pParam;
	pParam = (TEST_PARAM *)(&m_pDoc->m_sPreTestParam);
	
	UpdateData();
	//ljb 콤보박스에서 데이터를 가져 온다
// 	pParam->ican_function_division[0] = m_iCanCategory_1;
// 	pParam->ican_function_division[1] = m_iCanCategory_2;
// 	pParam->ican_function_division[2] = m_iCanCategory_3;
// 	pParam->ican_function_division[3] = m_iCanCategory_4;
// 	pParam->ican_function_division[4] = m_iCanCategory_5;
// 	pParam->ican_function_division[5] = m_iCanCategory_6;
// 	pParam->ican_function_division[6] = m_iCanCategory_7;
// 	pParam->ican_function_division[7] = m_iCanCategory_8;
// 	pParam->ican_function_division[8] = m_iCanCategory_9;
// 	pParam->ican_function_division[9] = m_iCanCategory_10;
	if(m_ctrlCboCanCode_1.GetCurSel() != LB_ERR)	pParam->ican_function_division[0] = m_ctrlCboCanCode_1.GetItemData(m_ctrlCboCanCode_1.GetCurSel());
	if(m_ctrlCboCanCode_2.GetCurSel() != LB_ERR)	pParam->ican_function_division[1] = m_ctrlCboCanCode_2.GetItemData(m_ctrlCboCanCode_2.GetCurSel());
	if(m_ctrlCboCanCode_3.GetCurSel() != LB_ERR)	pParam->ican_function_division[2] = m_ctrlCboCanCode_3.GetItemData(m_ctrlCboCanCode_3.GetCurSel());
	if(m_ctrlCboCanCode_4.GetCurSel() != LB_ERR)	pParam->ican_function_division[3] = m_ctrlCboCanCode_4.GetItemData(m_ctrlCboCanCode_4.GetCurSel());
	if(m_ctrlCboCanCode_5.GetCurSel() != LB_ERR)	pParam->ican_function_division[4] = m_ctrlCboCanCode_5.GetItemData(m_ctrlCboCanCode_5.GetCurSel());
	if(m_ctrlCboCanCode_6.GetCurSel() != LB_ERR)	pParam->ican_function_division[5] = m_ctrlCboCanCode_6.GetItemData(m_ctrlCboCanCode_6.GetCurSel());
	if(m_ctrlCboCanCode_7.GetCurSel() != LB_ERR)	pParam->ican_function_division[6] = m_ctrlCboCanCode_7.GetItemData(m_ctrlCboCanCode_7.GetCurSel());
	if(m_ctrlCboCanCode_8.GetCurSel() != LB_ERR)	pParam->ican_function_division[7] = m_ctrlCboCanCode_8.GetItemData(m_ctrlCboCanCode_8.GetCurSel());
	if(m_ctrlCboCanCode_9.GetCurSel() != LB_ERR)	pParam->ican_function_division[8] = m_ctrlCboCanCode_9.GetItemData(m_ctrlCboCanCode_9.GetCurSel());
	if(m_ctrlCboCanCode_10.GetCurSel() != LB_ERR)	pParam->ican_function_division[9] = m_ctrlCboCanCode_10.GetItemData(m_ctrlCboCanCode_10.GetCurSel());

	pParam->fcan_Value[0] = m_fCanValue_1;
	pParam->fcan_Value[1] = m_fCanValue_2;
	pParam->fcan_Value[2] = m_fCanValue_3;
	pParam->fcan_Value[3] = m_fCanValue_4;
	pParam->fcan_Value[4] = m_fCanValue_5;
	pParam->fcan_Value[5] = m_fCanValue_6;
	pParam->fcan_Value[6] = m_fCanValue_7;
	pParam->fcan_Value[7] = m_fCanValue_8;
	pParam->fcan_Value[8] = m_fCanValue_9;
	pParam->fcan_Value[9] = m_fCanValue_10;
	
	//ljb 콤보박스에서 데이터를 가져 온다
// 	pParam->iaux_function_division[0] = m_iAuxCategory_1;
// 	pParam->iaux_function_division[1] = m_iAuxCategory_2;
// 	pParam->iaux_function_division[2] = m_iAuxCategory_3;
// 	pParam->iaux_function_division[3] = m_iAuxCategory_4;
// 	pParam->iaux_function_division[4] = m_iAuxCategory_5;
// 	pParam->iaux_function_division[5] = m_iAuxCategory_6;
// 	pParam->iaux_function_division[6] = m_iAuxCategory_7;
// 	pParam->iaux_function_division[7] = m_iAuxCategory_8;
// 	pParam->iaux_function_division[8] = m_iAuxCategory_9;
// 	pParam->iaux_function_division[9] = m_iAuxCategory_10;
	if(m_ctrlCboAuxCode_1.GetCurSel() != LB_ERR)	pParam->iaux_function_division[0] = m_ctrlCboAuxCode_1.GetItemData(m_ctrlCboAuxCode_1.GetCurSel());
	if(m_ctrlCboAuxCode_2.GetCurSel() != LB_ERR)	pParam->iaux_function_division[1] = m_ctrlCboAuxCode_2.GetItemData(m_ctrlCboAuxCode_2.GetCurSel());
	if(m_ctrlCboAuxCode_3.GetCurSel() != LB_ERR)	pParam->iaux_function_division[2] = m_ctrlCboAuxCode_3.GetItemData(m_ctrlCboAuxCode_3.GetCurSel());
	if(m_ctrlCboAuxCode_4.GetCurSel() != LB_ERR)	pParam->iaux_function_division[3] = m_ctrlCboAuxCode_4.GetItemData(m_ctrlCboAuxCode_4.GetCurSel());
	if(m_ctrlCboAuxCode_5.GetCurSel() != LB_ERR)	pParam->iaux_function_division[4] = m_ctrlCboAuxCode_5.GetItemData(m_ctrlCboAuxCode_5.GetCurSel());
	if(m_ctrlCboAuxCode_6.GetCurSel() != LB_ERR)	pParam->iaux_function_division[5] = m_ctrlCboAuxCode_6.GetItemData(m_ctrlCboAuxCode_6.GetCurSel());
	if(m_ctrlCboAuxCode_7.GetCurSel() != LB_ERR)	pParam->iaux_function_division[6] = m_ctrlCboAuxCode_7.GetItemData(m_ctrlCboAuxCode_7.GetCurSel());
	if(m_ctrlCboAuxCode_8.GetCurSel() != LB_ERR)	pParam->iaux_function_division[7] = m_ctrlCboAuxCode_8.GetItemData(m_ctrlCboAuxCode_8.GetCurSel());
	if(m_ctrlCboAuxCode_9.GetCurSel() != LB_ERR)	pParam->iaux_function_division[8] = m_ctrlCboAuxCode_9.GetItemData(m_ctrlCboAuxCode_9.GetCurSel());
	if(m_ctrlCboAuxCode_10.GetCurSel() != LB_ERR)	pParam->iaux_function_division[9] = m_ctrlCboAuxCode_10.GetItemData(m_ctrlCboAuxCode_10.GetCurSel());
	
	pParam->faux_Value[0] = m_fAuxValue_1;
	pParam->faux_Value[1] = m_fAuxValue_2;
	pParam->faux_Value[2] = m_fAuxValue_3;
	pParam->faux_Value[3] = m_fAuxValue_4;
	pParam->faux_Value[4] = m_fAuxValue_5;
	pParam->faux_Value[5] = m_fAuxValue_6;
	pParam->faux_Value[6] = m_fAuxValue_7;
	pParam->faux_Value[7] = m_fAuxValue_8;
	pParam->faux_Value[8] = m_fAuxValue_9;
	pParam->faux_Value[9] = m_fAuxValue_10;

	if(m_ctrlCanDataType_1.GetCurSel() != LB_ERR)	pParam->cCan_data_type[0] = m_ctrlCanDataType_1.GetItemData(m_ctrlCanDataType_1.GetCurSel());
	if(m_ctrlCanDataType_2.GetCurSel() != LB_ERR)	pParam->cCan_data_type[1] = m_ctrlCanDataType_2.GetItemData(m_ctrlCanDataType_2.GetCurSel());
	if(m_ctrlCanDataType_3.GetCurSel() != LB_ERR)	pParam->cCan_data_type[2] = m_ctrlCanDataType_3.GetItemData(m_ctrlCanDataType_3.GetCurSel());
	if(m_ctrlCanDataType_4.GetCurSel() != LB_ERR)	pParam->cCan_data_type[3] = m_ctrlCanDataType_4.GetItemData(m_ctrlCanDataType_4.GetCurSel());
	if(m_ctrlCanDataType_5.GetCurSel() != LB_ERR)	pParam->cCan_data_type[4] = m_ctrlCanDataType_5.GetItemData(m_ctrlCanDataType_5.GetCurSel());
	if(m_ctrlCanDataType_6.GetCurSel() != LB_ERR)	pParam->cCan_data_type[5] = m_ctrlCanDataType_6.GetItemData(m_ctrlCanDataType_6.GetCurSel());
	if(m_ctrlCanDataType_7.GetCurSel() != LB_ERR)	pParam->cCan_data_type[6] = m_ctrlCanDataType_7.GetItemData(m_ctrlCanDataType_7.GetCurSel());
	if(m_ctrlCanDataType_8.GetCurSel() != LB_ERR)	pParam->cCan_data_type[7] = m_ctrlCanDataType_8.GetItemData(m_ctrlCanDataType_8.GetCurSel());
	if(m_ctrlCanDataType_9.GetCurSel() != LB_ERR)	pParam->cCan_data_type[8] = m_ctrlCanDataType_9.GetItemData(m_ctrlCanDataType_9.GetCurSel());
	if(m_ctrlCanDataType_10.GetCurSel() != LB_ERR)	pParam->cCan_data_type[9] = m_ctrlCanDataType_10.GetItemData(m_ctrlCanDataType_10.GetCurSel());
	
	if(m_ctrlAuxDataType_1.GetCurSel() != LB_ERR)	pParam->cAux_data_type[0] = m_ctrlAuxDataType_1.GetItemData(m_ctrlAuxDataType_1.GetCurSel());
	if(m_ctrlAuxDataType_2.GetCurSel() != LB_ERR)	pParam->cAux_data_type[1] = m_ctrlAuxDataType_2.GetItemData(m_ctrlAuxDataType_2.GetCurSel());
	if(m_ctrlAuxDataType_3.GetCurSel() != LB_ERR)	pParam->cAux_data_type[2] = m_ctrlAuxDataType_3.GetItemData(m_ctrlAuxDataType_3.GetCurSel());
	if(m_ctrlAuxDataType_4.GetCurSel() != LB_ERR)	pParam->cAux_data_type[3] = m_ctrlAuxDataType_4.GetItemData(m_ctrlAuxDataType_4.GetCurSel());
	if(m_ctrlAuxDataType_5.GetCurSel() != LB_ERR)	pParam->cAux_data_type[4] = m_ctrlAuxDataType_5.GetItemData(m_ctrlAuxDataType_5.GetCurSel());
	if(m_ctrlAuxDataType_6.GetCurSel() != LB_ERR)	pParam->cAux_data_type[5] = m_ctrlAuxDataType_6.GetItemData(m_ctrlAuxDataType_6.GetCurSel());
	if(m_ctrlAuxDataType_7.GetCurSel() != LB_ERR)	pParam->cAux_data_type[6] = m_ctrlAuxDataType_7.GetItemData(m_ctrlAuxDataType_7.GetCurSel());
	if(m_ctrlAuxDataType_8.GetCurSel() != LB_ERR)	pParam->cAux_data_type[7] = m_ctrlAuxDataType_8.GetItemData(m_ctrlAuxDataType_8.GetCurSel());
	if(m_ctrlAuxDataType_9.GetCurSel() != LB_ERR)	pParam->cAux_data_type[8] = m_ctrlAuxDataType_9.GetItemData(m_ctrlAuxDataType_9.GetCurSel());
	if(m_ctrlAuxDataType_10.GetCurSel() != LB_ERR)	pParam->cAux_data_type[9] = m_ctrlAuxDataType_10.GetItemData(m_ctrlAuxDataType_10.GetCurSel());

	if(m_ctrlCanCompare_1.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[0] = m_ctrlCanCompare_1.GetItemData(m_ctrlCanCompare_1.GetCurSel());
	if(m_ctrlCanCompare_2.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[1] = m_ctrlCanCompare_2.GetItemData(m_ctrlCanCompare_2.GetCurSel());
	if(m_ctrlCanCompare_3.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[2] = m_ctrlCanCompare_3.GetItemData(m_ctrlCanCompare_3.GetCurSel());
	if(m_ctrlCanCompare_4.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[3] = m_ctrlCanCompare_4.GetItemData(m_ctrlCanCompare_4.GetCurSel());
	if(m_ctrlCanCompare_5.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[4] = m_ctrlCanCompare_5.GetItemData(m_ctrlCanCompare_5.GetCurSel());
	if(m_ctrlCanCompare_6.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[5] = m_ctrlCanCompare_6.GetItemData(m_ctrlCanCompare_6.GetCurSel());
	if(m_ctrlCanCompare_7.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[6] = m_ctrlCanCompare_7.GetItemData(m_ctrlCanCompare_7.GetCurSel());
	if(m_ctrlCanCompare_8.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[7] = m_ctrlCanCompare_8.GetItemData(m_ctrlCanCompare_8.GetCurSel());
	if(m_ctrlCanCompare_9.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[8] = m_ctrlCanCompare_9.GetItemData(m_ctrlCanCompare_9.GetCurSel());
	if(m_ctrlCanCompare_10.GetCurSel() != LB_ERR)	pParam->cCan_compare_type[9] = m_ctrlCanCompare_10.GetItemData(m_ctrlCanCompare_10.GetCurSel());
	
	if(m_ctrlAuxCompare_1.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[0] = m_ctrlAuxCompare_1.GetItemData(m_ctrlAuxCompare_1.GetCurSel());
	if(m_ctrlAuxCompare_2.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[1] = m_ctrlAuxCompare_2.GetItemData(m_ctrlAuxCompare_2.GetCurSel());
	if(m_ctrlAuxCompare_3.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[2] = m_ctrlAuxCompare_3.GetItemData(m_ctrlAuxCompare_3.GetCurSel());
	if(m_ctrlAuxCompare_4.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[3] = m_ctrlAuxCompare_4.GetItemData(m_ctrlAuxCompare_4.GetCurSel());
	if(m_ctrlAuxCompare_5.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[4] = m_ctrlAuxCompare_5.GetItemData(m_ctrlAuxCompare_5.GetCurSel());
	if(m_ctrlAuxCompare_6.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[5] = m_ctrlAuxCompare_6.GetItemData(m_ctrlAuxCompare_6.GetCurSel());
	if(m_ctrlAuxCompare_7.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[6] = m_ctrlAuxCompare_7.GetItemData(m_ctrlAuxCompare_7.GetCurSel());
	if(m_ctrlAuxCompare_8.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[7] = m_ctrlAuxCompare_8.GetItemData(m_ctrlAuxCompare_8.GetCurSel());
	if(m_ctrlAuxCompare_9.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[8] = m_ctrlAuxCompare_9.GetItemData(m_ctrlAuxCompare_9.GetCurSel());
	if(m_ctrlAuxCompare_10.GetCurSel() != LB_ERR)	pParam->cAux_compare_type[9] = m_ctrlAuxCompare_10.GetItemData(m_ctrlAuxCompare_10.GetCurSel());

	for (int i=0; i< 10; i++)
	{
		if (pParam->ican_function_division[i] != 0 && (pParam->cCan_data_type[i] == 0 || pParam->cCan_compare_type[i] == 0))
		{
			AfxMessageBox(Fun_FindMsg("CCommSafetyCanAux_msg1","IDD_DLG_COMM_SAFETY"));
			return;
		}
		if (pParam->iaux_function_division[i] != 0 && (pParam->cAux_data_type[i] == 0 || pParam->cAux_compare_type[i] == 0))
		{
			AfxMessageBox(Fun_FindMsg("CCommSafetyCanAux_msg2","IDD_DLG_COMM_SAFETY"));
			return;
		}

	}

	UpdateData(FALSE);	
	CDialog::OnOK();
}

BOOL CCommSafetyCanAux::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN){
		// 13 : Enter Key, 27 : ESC Key
		if(pMsg->wParam == 13 || pMsg->wParam == 27) return TRUE;
        }
	return CDialog::PreTranslateMessage(pMsg);
}

void CCommSafetyCanAux::InitComboBox()
{
	m_pDoc->Fun_GetArryCanDivision(uiArryCanCode);
	m_pDoc->Fun_GetArryAuxDivision(uiArryAuxCode);
	m_pDoc->Fun_GetArryCanStringName(strArryCanName);
	m_pDoc->Fun_GetArryAuxStringName(strArryAuxName);

	int i;
	CString strName;
	//None 등록
	m_ctrlCboCanCode_1.AddString("None");
	m_ctrlCboCanCode_1.SetItemData(0,0);
	m_ctrlCboCanCode_2.AddString("None");
	m_ctrlCboCanCode_2.SetItemData(0,0);
	m_ctrlCboCanCode_3.AddString("None");
	m_ctrlCboCanCode_3.SetItemData(0,0);
	m_ctrlCboCanCode_4.AddString("None");
	m_ctrlCboCanCode_4.SetItemData(0,0);
	m_ctrlCboCanCode_5.AddString("None");
	m_ctrlCboCanCode_5.SetItemData(0,0);
	m_ctrlCboCanCode_6.AddString("None");
	m_ctrlCboCanCode_6.SetItemData(0,0);
	m_ctrlCboCanCode_7.AddString("None");
	m_ctrlCboCanCode_7.SetItemData(0,0);
	m_ctrlCboCanCode_8.AddString("None");
	m_ctrlCboCanCode_8.SetItemData(0,0);
	m_ctrlCboCanCode_9.AddString("None");
	m_ctrlCboCanCode_9.SetItemData(0,0);
	m_ctrlCboCanCode_10.AddString("None");
	m_ctrlCboCanCode_10.SetItemData(0,0);

	m_ctrlCboAuxCode_1.AddString("None");
	m_ctrlCboAuxCode_1.SetItemData(0,0);
	m_ctrlCboAuxCode_2.AddString("None");
	m_ctrlCboAuxCode_2.SetItemData(0,0);
	m_ctrlCboAuxCode_3.AddString("None");
	m_ctrlCboAuxCode_3.SetItemData(0,0);
	m_ctrlCboAuxCode_4.AddString("None");
	m_ctrlCboAuxCode_4.SetItemData(0,0);
	m_ctrlCboAuxCode_5.AddString("None");
	m_ctrlCboAuxCode_5.SetItemData(0,0);
	m_ctrlCboAuxCode_6.AddString("None");
	m_ctrlCboAuxCode_6.SetItemData(0,0);
	m_ctrlCboAuxCode_7.AddString("None");
	m_ctrlCboAuxCode_7.SetItemData(0,0);
	m_ctrlCboAuxCode_8.AddString("None");
	m_ctrlCboAuxCode_8.SetItemData(0,0);
	m_ctrlCboAuxCode_9.AddString("None");
	m_ctrlCboAuxCode_9.SetItemData(0,0);
	m_ctrlCboAuxCode_10.AddString("None");
	m_ctrlCboAuxCode_10.SetItemData(0,0);
	
	//int nCount;
	//nCount = uiArryCanCode.GetSize();
	//Can Code 등록
	for ( i=0; i < uiArryCanCode.GetSize(); i++)
	{
		strName = strArryCanName.GetAt(i);
		m_ctrlCboCanCode_1.AddString(strName);
		m_ctrlCboCanCode_1.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_2.AddString(strName);
		m_ctrlCboCanCode_2.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_3.AddString(strName);
		m_ctrlCboCanCode_3.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_4.AddString(strName);
		m_ctrlCboCanCode_4.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_5.AddString(strName);
		m_ctrlCboCanCode_5.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_6.AddString(strName);
		m_ctrlCboCanCode_6.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_7.AddString(strName);
		m_ctrlCboCanCode_7.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_8.AddString(strName);
		m_ctrlCboCanCode_8.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_9.AddString(strName);
		m_ctrlCboCanCode_9.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCboCanCode_10.AddString(strName);
		m_ctrlCboCanCode_10.SetItemData(i+1,uiArryCanCode.GetAt(i));
	}

	for ( i=0; i < uiArryAuxCode.GetSize(); i++)
	{
		strName = strArryAuxName.GetAt(i);
		m_ctrlCboAuxCode_1.AddString(strName);
		m_ctrlCboAuxCode_1.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_2.AddString(strName);
		m_ctrlCboAuxCode_2.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_3.AddString(strName);
		m_ctrlCboAuxCode_3.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_4.AddString(strName);
		m_ctrlCboAuxCode_4.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_5.AddString(strName);
		m_ctrlCboAuxCode_5.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_6.AddString(strName);
		m_ctrlCboAuxCode_6.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_7.AddString(strName);
		m_ctrlCboAuxCode_7.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_8.AddString(strName);
		m_ctrlCboAuxCode_8.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_9.AddString(strName);
		m_ctrlCboAuxCode_9.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlCboAuxCode_10.AddString(strName);
		m_ctrlCboAuxCode_10.SetItemData(i+1,uiArryAuxCode.GetAt(i));
	}

	m_ctrlCboCanCode_1.SetCurSel(0);
	m_ctrlCboCanCode_2.SetCurSel(0);
	m_ctrlCboCanCode_3.SetCurSel(0);
	m_ctrlCboCanCode_4.SetCurSel(0);
	m_ctrlCboCanCode_5.SetCurSel(0);
	m_ctrlCboCanCode_6.SetCurSel(0);
	m_ctrlCboCanCode_7.SetCurSel(0);
	m_ctrlCboCanCode_8.SetCurSel(0);
	m_ctrlCboCanCode_9.SetCurSel(0);
	m_ctrlCboCanCode_10.SetCurSel(0);

	m_ctrlCboAuxCode_1.SetCurSel(0);
	m_ctrlCboAuxCode_2.SetCurSel(0);
	m_ctrlCboAuxCode_3.SetCurSel(0);
	m_ctrlCboAuxCode_4.SetCurSel(0);
	m_ctrlCboAuxCode_5.SetCurSel(0);
	m_ctrlCboAuxCode_6.SetCurSel(0);
	m_ctrlCboAuxCode_7.SetCurSel(0);
	m_ctrlCboAuxCode_8.SetCurSel(0);
	m_ctrlCboAuxCode_9.SetCurSel(0);
	m_ctrlCboAuxCode_10.SetCurSel(0);
}

void CCommSafetyCanAux::OnSelchangeCboCanCode1() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_1.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_1.GetItemData(m_ctrlCboCanCode_1.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE1)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_1)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET1)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_1.SetFocus();
	UpdateData(FALSE);		
}

void CCommSafetyCanAux::OnSelchangeCboCanCode2() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_2.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_2.GetItemData(m_ctrlCboCanCode_2.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE2)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_2)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET2)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_2.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode3() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_3.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_3.GetItemData(m_ctrlCboCanCode_3.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE3)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_3)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET3)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_3.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode4() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_4.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_4.GetItemData(m_ctrlCboCanCode_4.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE4)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_4)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET4)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_4.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode5() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_5.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_5.GetItemData(m_ctrlCboCanCode_5.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE5)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_5)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET5)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_5.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode6() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_6.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_6.GetItemData(m_ctrlCboCanCode_6.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE6)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_6)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET6)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_6.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode7() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_7.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_7.GetItemData(m_ctrlCboCanCode_7.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE7)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_7)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET7)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_7.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode8() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_8.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_8.GetItemData(m_ctrlCboCanCode_8.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE8)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_8)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET8)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_8.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode9() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_9.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_9.GetItemData(m_ctrlCboCanCode_9.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE9)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_9)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET9)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_9.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboCanCode10() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboCanCode_10.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboCanCode_10.GetItemData(m_ctrlCboCanCode_10.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_CAN_VALUE10)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_VALUE_TYPE_10)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_CAN_SET10)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboCanCode_10.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode1() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_1.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_1.GetItemData(m_ctrlCboAuxCode_1.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE1)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_1)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET1)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_1.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode2() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_2.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_2.GetItemData(m_ctrlCboAuxCode_2.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE2)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_2)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET2)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_2.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode3() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_3.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_3.GetItemData(m_ctrlCboAuxCode_3.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE3)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_3)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET3)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_3.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode4() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_4.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_4.GetItemData(m_ctrlCboAuxCode_4.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE4)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_4)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET4)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_4.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode5() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_5.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_5.GetItemData(m_ctrlCboAuxCode_5.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE5)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_5)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET5)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_5.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode6() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_6.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_6.GetItemData(m_ctrlCboAuxCode_6.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE6)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_6)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET6)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_6.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode7() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_7.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_7.GetItemData(m_ctrlCboAuxCode_7.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE7)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_7)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET7)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_7.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode8() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_8.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_8.GetItemData(m_ctrlCboAuxCode_8.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE8)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_8)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET8)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_8.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode9() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_9.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_9.GetItemData(m_ctrlCboAuxCode_9.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE9)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_9)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET9)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_9.SetFocus();
	UpdateData(FALSE);			
}

void CCommSafetyCanAux::OnSelchangeCboAuxCode10() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCboAuxCode_10.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCboAuxCode_10.GetItemData(m_ctrlCboAuxCode_10.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		GetDlgItem(IDC_EDIT_AUX_VALUE10)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_VALUE_TYPE_10)->EnableWindow(bFlag);
		GetDlgItem(IDC_CBO_AUX_SET10)->EnableWindow(bFlag);
	}
	else
		m_ctrlCboAuxCode_10.SetFocus();
	UpdateData(FALSE);				
}

void CCommSafetyCanAux::Fun_FindCanAuxName(UINT uiType, int nPos, int nDivision)
{
	CString strName="None";
	//int nSelectPos;
	int nFindPos;
	if (uiType == ID_CAN_NAME)
	{
		nFindPos = 1;
		for (int i=0; i < uiArryCanCode.GetSize() ; i++)
		{
			TRACE("nDivision = %d, ArryCanCode = %d \n",nDivision, uiArryCanCode.GetAt(i));
			if (nDivision == uiArryCanCode.GetAt(i))
			{
				strName = strArryCanName.GetAt(i);
				break;
			}
			nFindPos++;
		}
		if (nFindPos > uiArryCanCode.GetSize()) nFindPos = 0;

		if (nPos == 1) m_ctrlCboCanCode_1.SetCurSel(nFindPos);
		if (nPos == 2) m_ctrlCboCanCode_2.SetCurSel(nFindPos);
		if (nPos == 3) m_ctrlCboCanCode_3.SetCurSel(nFindPos);
		if (nPos == 4) m_ctrlCboCanCode_4.SetCurSel(nFindPos);
		if (nPos == 5) m_ctrlCboCanCode_5.SetCurSel(nFindPos);
		if (nPos == 6) m_ctrlCboCanCode_6.SetCurSel(nFindPos);
		if (nPos == 7) m_ctrlCboCanCode_7.SetCurSel(nFindPos);
		if (nPos == 8) m_ctrlCboCanCode_8.SetCurSel(nFindPos);
		if (nPos == 9) m_ctrlCboCanCode_9.SetCurSel(nFindPos);
		if (nPos == 10) m_ctrlCboCanCode_10.SetCurSel(nFindPos);
	}
	else if (uiType == ID_AUX_NAME)
	{
		nFindPos = 1;
		for (int i=0; i < uiArryAuxCode.GetSize() ; i++)
		{
			if (nDivision == uiArryAuxCode.GetAt(i)) break;
			nFindPos++;
		}
		if (nFindPos > uiArryAuxCode.GetSize()) nFindPos = 0;
		
		if (nPos == 1) m_ctrlCboAuxCode_1.SetCurSel(nFindPos);
		if (nPos == 2) m_ctrlCboAuxCode_2.SetCurSel(nFindPos);
		if (nPos == 3) m_ctrlCboAuxCode_3.SetCurSel(nFindPos);
		if (nPos == 4) m_ctrlCboAuxCode_4.SetCurSel(nFindPos);
		if (nPos == 5) m_ctrlCboAuxCode_5.SetCurSel(nFindPos);
		if (nPos == 6) m_ctrlCboAuxCode_6.SetCurSel(nFindPos);
		if (nPos == 7) m_ctrlCboAuxCode_7.SetCurSel(nFindPos);
		if (nPos == 8) m_ctrlCboAuxCode_8.SetCurSel(nFindPos);
		if (nPos == 9) m_ctrlCboAuxCode_9.SetCurSel(nFindPos);
		if (nPos == 10) m_ctrlCboAuxCode_10.SetCurSel(nFindPos);
	}
}

void CCommSafetyCanAux::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	KillTimer(nIDEvent);

// 	Fun_DisplayData();
// 	UpdateData(FALSE);

	CDialog::OnTimer(nIDEvent);
}