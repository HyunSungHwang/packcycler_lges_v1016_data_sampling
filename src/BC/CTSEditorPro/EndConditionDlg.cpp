// EndConditionDlg.cpp : implementation file
//$1013

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "EndConditionDlg.h"
//#include "..\\PowerFormation\\Msgdefine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define		ID_CAN_NAME		1
#define		ID_AUX_NAME		2

/////////////////////////////////////////////////////////////////////////////
// CEndConditionDlg dialog


CEndConditionDlg::CEndConditionDlg(CWnd* pParent /*=NULL*/)
	//: CDialog(CEndConditionDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CEndConditionDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CEndConditionDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CEndConditionDlg::IDD3):
	(CEndConditionDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CEndConditionDlg)
	m_nEndDay = 0;
	m_lEndMSec = 0;
	m_ctrlEditLoopInfoCycle = 0;
	m_ctrlEditfValueRate = 0.0f;
	m_fCanValue_1 = 0.0f;
	m_fCanValue_2 = 0.0f;
	m_fCanValue_3 = 0.0f;
	m_fCanValue_4 = 0.0f;
	m_fCanValue_5 = 0.0f;
	m_fCanValue_7 = 0.0f;
	m_fCanValue_6 = 0.0f;
	m_fCanValue_8 = 0.0f;
	m_fCanValue_9 = 0.0f;
	m_fCanValue_10 = 0.0f;
	m_fAuxValue_1 = 0.0f;
	m_fAuxValue_2 = 0.0f;
	m_fAuxValue_3 = 0.0f;
	m_fAuxValue_4 = 0.0f;
	m_fAuxValue_5 = 0.0f;
	m_fAuxValue_7 = 0.0f;
	m_fAuxValue_6 = 0.0f;
	m_fAuxValue_8 = 0.0f;
	m_fAuxValue_9 = 0.0f;
	m_fAuxValue_10 = 0.0f;
	m_bUseCyclePause = FALSE;
	m_bUseLinkStep = FALSE;
	m_bUseChamberProg = FALSE;
	m_ctrlEditfValueLoader = 0.0f;
	m_nWaitTimeDay = 0;
	m_bUseWaitTimeInit = FALSE;
	m_edit_aux_conti_time_1 = 0;
	m_edit_aux_conti_time_2 = 0;
	m_edit_aux_conti_time_3 = 0;
	m_edit_aux_conti_time_4 = 0;
	m_edit_aux_conti_time_5 = 0;
	m_edit_aux_conti_time_6 = 0;
	m_edit_aux_conti_time_7 = 0;
	m_edit_aux_conti_time_8 = 0;
	m_edit_aux_conti_time_9 = 0;
	m_edit_aux_conti_time_10 = 0;
	m_bChamberStepStop = FALSE;
	//}}AFX_DATA_INIT
//	m_nStepNo =0;
	m_pParent = pParent;
	m_nStepNo = 0;
	m_nMultiLoopGroupID = 0;
	m_nMultiLoopInfoCycle = 0;
	m_nAccLoopGroupID = 0;
	m_nAccLoopInfoCycle = 0;
	m_nCVTimeDay = 0;
	m_lCVTimeMSec = 0;
	m_bExtViewMode = FALSE;
}


void CEndConditionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEndConditionDlg)
	DDX_Control(pDX, IDC_CBO_CAN_CHECK, m_ctrlCboCanCheck);
	DDX_Control(pDX, IDC_WAIT_DATETIMEPICKER, m_ctrlWaitTime);
	DDX_Control(pDX, IDC_CBO_LOADER_MODE_VALUE, m_ctrlCboLoaderMode);
	DDX_Control(pDX, IDC_CBO_LOADER_VALUE_ITEM, m_ctrlCboLoaderValueItem);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_10, m_ctrlAuxDivision_10);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_9, m_ctrlAuxDivision_9);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_8, m_ctrlAuxDivision_8);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_7, m_ctrlAuxDivision_7);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_6, m_ctrlAuxDivision_6);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_5, m_ctrlAuxDivision_5);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_4, m_ctrlAuxDivision_4);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_3, m_ctrlAuxDivision_3);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_2, m_ctrlAuxDivision_2);
	DDX_Control(pDX, IDC_CBO_AUX_DIVISION_1, m_ctrlAuxDivision_1);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_10, m_ctrlCanDivision_10);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_9, m_ctrlCanDivision_9);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_8, m_ctrlCanDivision_8);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_7, m_ctrlCanDivision_7);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_6, m_ctrlCanDivision_6);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_5, m_ctrlCanDivision_5);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_4, m_ctrlCanDivision_4);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_3, m_ctrlCanDivision_3);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_2, m_ctrlCanDivision_2);
	DDX_Control(pDX, IDC_CBO_CAN_DIVISION_1, m_ctrlCanDivision_1);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_10, m_ctrlAuxDataType10);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_9, m_ctrlAuxDataType9);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_8, m_ctrlAuxDataType8);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_7, m_ctrlAuxDataType7);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_6, m_ctrlAuxDataType6);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_5, m_ctrlAuxDataType5);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_4, m_ctrlAuxDataType4);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_3, m_ctrlAuxDataType3);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_2, m_ctrlAuxDataType2);
	DDX_Control(pDX, IDC_CBO_AUX_DATA_TYPE_1, m_ctrlAuxDataType1);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_10, m_ctrlCanDataType10);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_9, m_ctrlCanDataType9);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_8, m_ctrlCanDataType8);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_7, m_ctrlCanDataType7);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_6, m_ctrlCanDataType6);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_4, m_ctrlCanDataType4);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_5, m_ctrlCanDataType5);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_3, m_ctrlCanDataType3);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_2, m_ctrlCanDataType2);
	DDX_Control(pDX, IDC_CBO_CAN_DATA_TYPE_1, m_ctrlCanDataType1);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE10, m_ctrlCanCompare10);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE9, m_ctrlCanCompare9);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE8, m_ctrlCanCompare8);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE7, m_ctrlCanCompare7);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE6, m_ctrlCanCompare6);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE5, m_ctrlCanCompare5);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE4, m_ctrlCanCompare4);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE3, m_ctrlCanCompare3);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE2, m_ctrlCanCompare2);
	DDX_Control(pDX, IDC_CBO_CAN_COMPARE1, m_ctrlCanCompare1);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE10, m_ctrlAuxCompare10);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE9, m_ctrlAuxCompare9);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE8, m_ctrlAuxCompare8);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE7, m_ctrlAuxCompare7);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE6, m_ctrlAuxCompare6);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE5, m_ctrlAuxCompare5);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE4, m_ctrlAuxCompare4);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE3, m_ctrlAuxCompare3);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE2, m_ctrlAuxCompare2);
	DDX_Control(pDX, IDC_CBO_AUX_COMPARE1, m_ctrlAuxCompare1);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO10, m_ctrlAuxGoto_10);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO9, m_ctrlAuxGoto_9);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO8, m_ctrlAuxGoto_8);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO7, m_ctrlAuxGoto_7);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO6, m_ctrlAuxGoto_6);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO5, m_ctrlAuxGoto_5);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO4, m_ctrlAuxGoto_4);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO3, m_ctrlAuxGoto_3);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO2, m_ctrlAuxGoto_2);
	DDX_Control(pDX, IDC_CBO_AUX_GOTO1, m_ctrlAuxGoto_1);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO10, m_ctrlCanGoto_10);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO9, m_ctrlCanGoto_9);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO8, m_ctrlCanGoto_8);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO7, m_ctrlCanGoto_7);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO6, m_ctrlCanGoto_6);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO5, m_ctrlCanGoto_5);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO4, m_ctrlCanGoto_4);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO3, m_ctrlCanGoto_3);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO2, m_ctrlCanGoto_2);
	DDX_Control(pDX, IDC_CBO_CAN_GOTO1, m_ctrlCanGoto_1);
	DDX_Control(pDX, IDC_CBO_ADD_WATTHOUR, m_ctrlCboAddWattHour);
	DDX_Control(pDX, IDC_CBO_ADD_CAP, m_ctrlCboAddCap);
	DDX_Control(pDX, IDC_CBO_VALUE_ITEM, m_ctrlCboValueItem);
	DDX_Control(pDX, IDC_CBO_VALUE_STEP_NO, m_ctrlCboValueStepNo);
	DDX_Control(pDX, IDC_CBO_MULTI_GROUP, m_ctrlCboLoopMultiGroupID);
	DDX_Control(pDX, IDC_CBO_ACC_GROUP, m_ctrlCboLoopAccGroupID);
	DDX_Control(pDX, IDC_CBO_END_WATT_GOTO, m_ctrlCboEndWattHour);
	DDX_Control(pDX, IDC_CBO_END_VTG_L_GOTO, m_ctrlCboEndVtg_L);
	DDX_Control(pDX, IDC_CBO_END_VTG_H_GOTO, m_ctrlCboEndVtg_H);
	DDX_Control(pDX, IDC_CBO_END_VALUE_GOTO, m_ctrlCboEndValue);
	DDX_Control(pDX, IDC_CBO_END_TIME_GOTO, m_ctrlCboEndTime);
	DDX_Control(pDX, IDC_CBO_END_CVTIME_GOTO, m_ctrlCboEndCVTime);
	DDX_Control(pDX, IDC_CBO_END_CAPA_GOTO, m_ctrlCboEndCapa);
	DDX_Control(pDX, IDC_GOTO_COMBO, m_ctrlGoto);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_ctrlEndTime);
	DDX_Control(pDX, IDC_DATETIMEPICKER2, m_ctrlCVTime);	
	DDX_Control(pDX, IDC_STEP_NO, m_strStepNo);
	DDX_Control(pDX, IDC_STEP_END_MEMO, m_strStepEndMemo);
	DDX_Text(pDX, IDC_END_DAY_EDIT, m_nEndDay);
	DDX_Text(pDX, IDC_END_MSEC_EDIT, m_lEndMSec);
	DDX_Text(pDX, IDC_CV_MSEC_EDIT, m_lCVTimeMSec);
	DDX_Text(pDX, IDC_CV_DAY_EDIT, m_nCVTimeDay);
	DDX_Text(pDX, IDC_EDIT_CYCLE, m_ctrlEditLoopInfoCycle);
	DDX_Text(pDX, IDC_EDIT_VALUE_RATE, m_ctrlEditfValueRate);
	DDV_MinMaxFloat(pDX, m_ctrlEditfValueRate, 0.f, 100.f);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET1, m_fCanValue_1);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET2, m_fCanValue_2);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET3, m_fCanValue_3);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET4, m_fCanValue_4);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET5, m_fCanValue_5);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET7, m_fCanValue_7);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET6, m_fCanValue_6);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET8, m_fCanValue_8);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET9, m_fCanValue_9);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_SET10, m_fCanValue_10);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET1, m_fAuxValue_1);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET2, m_fAuxValue_2);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET3, m_fAuxValue_3);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET4, m_fAuxValue_4);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET5, m_fAuxValue_5);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET7, m_fAuxValue_7);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET6, m_fAuxValue_6);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET8, m_fAuxValue_8);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET9, m_fAuxValue_9);
	DDX_Text(pDX, IDC_EDIT_AUX_VALUE_SET10, m_fAuxValue_10);
	DDX_Check(pDX, IDC_CHK_CYCLE_PAUSE, m_bUseCyclePause);
	DDX_Check(pDX, IDC_CHK_LINK_PRE_STEP, m_bUseLinkStep);
	DDX_Check(pDX, IDC_CHK_CHAMBER_PROG_MODE, m_bUseChamberProg);
	DDX_Text(pDX, IDC_EDIT_LOADER_SET_VALUE, m_ctrlEditfValueLoader);
	DDX_Text(pDX, IDC_WAIT_DAY_EDIT, m_nWaitTimeDay);
	DDX_Check(pDX, IDC_CHK_WAIT_TIME_INIT, m_bUseWaitTimeInit);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_1, m_edit_aux_conti_time_1);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_1, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_2, m_edit_aux_conti_time_2);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_2, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_3, m_edit_aux_conti_time_3);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_3, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_4, m_edit_aux_conti_time_4);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_4, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_5, m_edit_aux_conti_time_5);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_5, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_6, m_edit_aux_conti_time_6);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_6, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_7, m_edit_aux_conti_time_7);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_7, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_8, m_edit_aux_conti_time_8);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_8, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_9, m_edit_aux_conti_time_9);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_9, 0, 10);
	DDX_Text(pDX, IDC_EDIT_AUX_CONTI_TIME_10, m_edit_aux_conti_time_10);
	DDV_MinMaxUInt(pDX, m_edit_aux_conti_time_10, 0, 10);
	DDX_Check(pDX, IDC_CHK_CHAMBER_STEP_STOP, m_bChamberStepStop);
	DDX_Control(pDX, IDC_CHECK_ZERO_VOLTAGE, m_bZeroVoltage); //yulee 20191017 OverChargeDischarger Mark

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEndConditionDlg, CDialog)
	//{{AFX_MSG_MAP(CEndConditionDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_END_DAY_EDIT, OnChangeEndDayEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_END_TIME_MSEC_SPIN, OnDeltaposEndTimeMsecSpin)
	ON_EN_KILLFOCUS(IDC_END_MSEC_EDIT, OnKillFocusEndMsec)
	ON_EN_CHANGE(IDC_CV_DAY_EDIT, OnChangeCVTimeDayEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CV_TIME_MSEC_SPIN, OnDeltaposCVTimeMsecSpin)
	ON_CBN_SELCHANGE(IDC_CBO_VALUE_ITEM, OnSelchangeCboValueItem)
	ON_EN_CHANGE(IDC_END_CAP, OnChangeEndCap)
	ON_EN_CHANGE(IDC_END_WATTHOUR, OnChangeEndWatthour)
	ON_EN_CHANGE(IDC_END_VTG_H, OnChangeEndVtgH)
	ON_EN_CHANGE(IDC_END_VTG_L, OnChangeEndVtgL)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER2, OnDatetimechangeDatetimepicker2)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, OnDatetimechangeDatetimepicker1)
	ON_EN_KILLFOCUS(IDC_EDIT_CAN_CATEGORY_SET1, OnKillfocusEditCanCategorySet1)
	ON_BN_CLICKED(IDC_CHK_LINK_PRE_STEP, OnChkLinkPreStep)
	ON_BN_CLICKED(IDC_BUT_EXTEND, OnButExtend)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_1, OnSelchangeCboCanDivision1)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_2, OnSelchangeCboCanDivision2)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_3, OnSelchangeCboCanDivision3)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_4, OnSelchangeCboCanDivision4)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_5, OnSelchangeCboCanDivision5)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_6, OnSelchangeCboCanDivision6)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_7, OnSelchangeCboCanDivision7)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_8, OnSelchangeCboCanDivision8)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_9, OnSelchangeCboCanDivision9)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_DIVISION_10, OnSelchangeCboCanDivision10)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_1, OnSelchangeCboAuxDivision1)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_2, OnSelchangeCboAuxDivision2)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_3, OnSelchangeCboAuxDivision3)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_4, OnSelchangeCboAuxDivision4)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_5, OnSelchangeCboAuxDivision5)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_6, OnSelchangeCboAuxDivision6)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_7, OnSelchangeCboAuxDivision7)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_8, OnSelchangeCboAuxDivision8)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_9, OnSelchangeCboAuxDivision9)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_DIVISION_10, OnSelchangeCboAuxDivision10)
	ON_CBN_SELCHANGE(IDC_CBO_LOADER_VALUE_ITEM, OnSelchangeCboLoaderValueItem)
	ON_CBN_SELCHANGE(IDC_CBO_LOADER_MODE_VALUE, OnSelchangeCboLoaderModeValue)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_WAIT_DATETIMEPICKER, OnDatetimechangeWaitDatetimepicker)
	ON_EN_CHANGE(IDC_WAIT_DAY_EDIT, OnChangeWaitDayEdit)
	ON_CBN_EDITCHANGE(IDC_CBO_CAN_CHECK, OnEditchangeCboCanCheck)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_CHECK, OnSelchangeCboCanCheck)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE1, OnSelchangeCboCanCompare1)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE2, OnSelchangeCboCanCompare2)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE3, OnSelchangeCboCanCompare3)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE4, OnSelchangeCboCanCompare4)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE5, OnSelchangeCboCanCompare5)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE6, OnSelchangeCboCanCompare6)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE7, OnSelchangeCboCanCompare7)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE8, OnSelchangeCboCanCompare8)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE9, OnSelchangeCboCanCompare9)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_COMPARE10, OnSelchangeCboCanCompare10)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE1, OnSelchangeCboAuxCompare1)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE2, OnSelchangeCboAuxCompare2)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE3, OnSelchangeCboAuxCompare3)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE4, OnSelchangeCboAuxCompare4)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE5, OnSelchangeCboAuxCompare5)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE6, OnSelchangeCboAuxCompare6)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE7, OnSelchangeCboAuxCompare7)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE8, OnSelchangeCboAuxCompare8)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE9, OnSelchangeCboAuxCompare9)
	ON_EN_KILLFOCUS(IDC_CV_MSEC_EDIT, OnKillFocusEndMsec)
	ON_CBN_SELCHANGE(IDC_CBO_AUX_COMPARE10, OnSelchangeCboAuxCompare10)
	ON_BN_CLICKED(IDC_CHECK_ZERO_VOLTAGE, OnCheckZeroVoltage) //yulee 20191017 OverChargeDischarger Mark
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEndConditionDlg message handlers

BOOL CEndConditionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_strStepNo.SetTextColor(RGB(255, 0, 0));
	m_strStepNo.SetFontAlign(0);
	m_strStepEndMemo.SetTextColor(RGB(255, 0, 0));
	m_strStepEndMemo.SetFontAlign(0);
	
	// TODO: Add extra initialization here

/*	m_EndTime.AttachDateTimeCtrl(IDC_END_TIME, this, 0);
	m_EndTime.SetFormat("HH:mm:ss");
	m_EndTime.SetTime(ole);
*/
	m_nGotoEndVtg_H = m_nGotoEndVtg_L = m_nGotoEndTime = m_nGotoEndCVTime = 0;
	m_nGotoEndCapa = m_nGotoEndWattHour = m_nGotoEndValue = 0;

	m_bUseChamberTemp = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseChamberForTemp", FALSE);
	m_bUseChamberHumi = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseChamberForHumi", FALSE);
	m_bUseHLGP = AfxGetApp()->GetProfileInt("Config","UseHLGP",0); //lyj 20200730

	InitControl();		//버튼 초기화
	InitComboList();
	InitToolTips();
	
	GetClientRect(rOrgSize);

	//ksj 20180223 : 사용자 정의 msec 스핀 증감 값 추가.
	m_nCustomMsec = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec Interval",0); // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.
	m_nCustomMsecStepEnd = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec IntervalStepEnd",0); // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.
	m_nCustomMsecPattEnd = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec IntervalPattEnd",0); // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.

	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "ZeroVoltEndCondClicked", 0);  //yulee 20190904 //yulee 20191017 OverChargeDischarger Mark
	m_bZeroVoltage.SetCheck(0);//yulee 20190904


	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEndConditionDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	if(m_nStepNo < 1)	return;

//	char szTemp[10];
	CString strTemp;
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	ASSERT(pStep);
	int nTotalStep = m_pDoc->GetTotalStepNum(); //ksj 20200831
	
	//ljb 전 스텝 비교 해서 전 STEP CP 연동 비 활성화
	STEP *pPreStep;
	pPreStep = NULL;
	if (m_nStepNo >= 2)	pPreStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-2];

	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("OnShowWindow_msg2","IDD_END_COND_DLG"));

	//2014.12.08 데이터 무시 모드
	BOOL UseNoCheckMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseNoCheckMode", FALSE);
	if(UseNoCheckMode){
		GetDlgItem(IDC_CHK_NOCHECK_PROG_MODE)->ShowWindow(TRUE);
	}	
	//2014.12.09 캔 송신 플러그 모드
	BOOL UseCanTxOffMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseCanTxOffMode", FALSE);
	if(UseCanTxOffMode){
		GetDlgItem(IDC_CHK_CANTXOFF_PROG_MODE)->ShowWindow(TRUE);
	}	

	//20150825 캔 통신체크 모드
// 	BOOL UseCanCheckMode = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseCanCheckMode", FALSE);
// 	if(UseCanCheckMode){
		//GetDlgItem(IDC_CHK_CAN_MODE)->ShowWindow(TRUE);
		GetDlgItem(IDC_CBO_CAN_CHECK)->ShowWindow(TRUE);
//	}	

	//20151223 Loader연동
	BOOL UseLoader = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseLoader",FALSE);
	BOOL bEnable = FALSE;
	//yulee 20191017 OverChargeDischarger Mark
	BOOL bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE);

	//ksj 20210610 : AuxV 관련 기능. 숨김요청 (위슨테크놀로지)
	BOOL bUseAuxV = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UseAuxV", TRUE);
	if(!bUseAuxV){
 		GetDlgItem(IDC_STATIC_CELL_DELTA_V_STEP)->ShowWindow(FALSE);
 		GetDlgItem(IDC_CELL_DELTA_V_STEP)->ShowWindow(FALSE);
 		GetDlgItem(IDC_STATIC_CELL_DELTA_V_STEP_UNIT)->ShowWindow(FALSE);
// 		GetDlgItem(IDC_STATIC_CELL_DELTA_V_STEP)->EnableWindow(FALSE);
// 		GetDlgItem(IDC_CELL_DELTA_V_STEP)->EnableWindow(FALSE);
// 		GetDlgItem(IDC_STATIC_CELL_DELTA_V_STEP_UNIT)->EnableWindow(FALSE);
	}	

	CString strTemp1,strTemp2;
	if(bShow)
	{
		GetDlgItem(IDC_BUT_EXTEND)->SetWindowText(Fun_FindMsg("OnButExtend_msg2","IDD_END_COND_DLG"));	

		strTemp.Format("Step %3d", m_nStepNo);
		m_strStepNo.SetText(strTemp);

		strTemp.Format(Fun_FindMsg("OnShowWindow_msg2","IDD_END_COND_DLG"), m_strEndMemo);
		m_strStepEndMemo.SetText(strTemp);

		GetDlgItem(IDC_EDIT_CYCLE)->EnableWindow(FALSE);
		GetDlgItem(IDC_GOTO_COMBO)->EnableWindow(FALSE);
		
		GetDlgItem(IDC_STATIC_MULTI_ID)->SetWindowText(Fun_FindMsg("OnShowWindow_msg3","IDD_END_COND_DLG"));				
		GetDlgItem(IDC_CBO_MULTI_GROUP)->EnableWindow(FALSE);			
		GetDlgItem(IDC_CBO_ACC_GROUP)->EnableWindow(FALSE);			


		GetDlgItem(IDC_CV_DAY_EDIT)->EnableWindow(FALSE); 
		GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CV_MSEC_EDIT)->EnableWindow(FALSE); 
		
//ljb 20150805 주석처리 SBC와 챔버통신 안함		GetDlgItem(IDC_EDIT_END_TEMP)->EnableWindow(TRUE);			//ljb SBC에서 챔버 제어 할때 챔버 종료 온도
		GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(FALSE);

		GetDlgItem(IDC_DATETIMEPICKER1)->SetFocus();
		GetDlgItem(IDC_STATIC_ENDC)->SetWindowText(Fun_FindMsg("OnShowWindow_msg4","IDD_END_COND_DLG"));				//20090206 KKY
		GetDlgItem(IDC_STATIC_END_WATTHOUR)->SetWindowText("Wh");		//20090206 KKY
		GetDlgItem(IDC_END_WATT_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattUnit());		//20090206 KKY
		GetDlgItem(IDC_END_WATTHOUR_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattHourUnit());		//20090206 KKY
		
		GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CBO_END_CVTIME_GOTO)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CBO_VALUE_ITEM)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(FALSE); 

		if(UseLoader){
			GetDlgItem(IDC_CBO_LOADER_VALUE_ITEM)->EnableWindow(TRUE); //  [4/22/2015 이재복]
			GetDlgItem(IDC_CBO_LOADER_MODE_VALUE)->EnableWindow(FALSE); //  [4/22/2015 이재복]
			GetDlgItem(IDC_EDIT_LOADER_SET_VALUE)->EnableWindow(FALSE); //  [4/22/2015 이재복]
		}	
		else{
			GetDlgItem(IDC_CBO_LOADER_VALUE_ITEM)->EnableWindow(FALSE); //  [4/22/2015 이재복]
			GetDlgItem(IDC_CBO_LOADER_MODE_VALUE)->EnableWindow(FALSE); //  [4/22/2015 이재복]
			GetDlgItem(IDC_EDIT_LOADER_SET_VALUE)->EnableWindow(FALSE); //  [4/22/2015 이재복]
		}	
		GetDlgItem(IDC_CHK_WAIT_TIME_INIT)->EnableWindow(FALSE);	//20160422 add
		GetDlgItem(IDC_WAIT_DAY_EDIT)->EnableWindow(TRUE);			//20160422 add -> 20170501 edit : FALSE -> TRUE
		GetDlgItem(IDC_WAIT_DATETIMEPICKER)->EnableWindow(TRUE);	//20160422 add

		GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CBO_END_TIME_GOTO)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CBO_END_CAPA_GOTO)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(FALSE); 
		GetDlgItem(IDC_CHK_CYCLE_PAUSE)->EnableWindow(FALSE);		//★★ ljb 201010  ★★★★★★★★
		GetDlgItem(IDC_CHK_LINK_PRE_STEP)->EnableWindow(FALSE);		//ljb 201010  ★★★★★★★★
//		GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->ShowWindow(SW_HIDE);				//ljb 20150805 add SBC와 챔버통신 안함
		GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(FALSE);		//★★ ljb 201010  ★★★★★★★★

		
		//★★ ljb 20091231 S	★★★★★★★★
		GetDlgItem(IDC_LAB_HAND1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_LAB_HAND2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_LAB_HAND3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CBO_END_CAPA_GOTO)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CBO_END_WATT_GOTO)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CBO_END_TIME_GOTO)->ShowWindow(SW_SHOW);

		GetDlgItem(IDC_CBO_ADD_CAP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CBO_ADD_WATTHOUR)->ShowWindow(SW_HIDE);
		m_ctrlCboAddCap.SetCurSel(0);
		m_ctrlCboAddWattHour.SetCurSel(0);


		GetDlgItem(IDC_STATIC_TEMP_TITLE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_END_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_TEMP_UNIT)->ShowWindow(SW_HIDE);
		//★★ ljb 20091231 E ★★★★★★★★

		//		DisplayData();

		if(m_pDoc->m_lLoadedProcType ==  PS_PGS_AGING)
		{	//시간 종료만 가능 
			bEnable = FALSE;
		}
		else 
		{	
			bEnable = TRUE;
		}	

		if(!bOverChargerSet)
		{
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
		}

		switch(pStep->chType)
		{
			//20090206 KKY////////////////////////////////////////////////////////////////////////////////////////////////
		case PS_STEP_ADV_CYCLE:
			GetDlgItem(IDC_CBO_LOADER_VALUE_ITEM)->EnableWindow(FALSE); //  [4/22/2015 이재복]
			//2014.12.08추가
			GetDlgItem(IDC_CHK_NOCHECK_PROG_MODE)->ShowWindow(FALSE);
			GetDlgItem(IDC_CHK_CANTXOFF_PROG_MODE)->ShowWindow(FALSE); //2014.12.09 추가.
			//GetDlgItem(IDC_CHK_CAN_MODE)->ShowWindow(FALSE); //20150825 add
			GetDlgItem(IDC_CBO_CAN_CHECK)->ShowWindow(FALSE);	//20170328 add

			//GetDlgItem(IDC_STATIC_ENDC)->SetWindowText("합산용량");
			GetDlgItem(IDC_STATIC_ENDC)->SetWindowText(Fun_FindMsg("EndConditionDlg_OnShowWindow_msg1","IDD_END_COND_DLG"));//&&
			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_END_WATTHOUR)->SetWindowText(Fun_FindMsg("OnShowWindow_msg6","IDD_END_COND_DLG"));
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(TRUE);
			GetDlgItem(IDC_CBO_MULTI_GROUP)->EnableWindow(TRUE);			
			//★★ ljb 20091231 S	★★★★★★★★
			GetDlgItem(IDC_LAB_HAND1)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_LAB_HAND2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_LAB_HAND3)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CBO_END_CAPA_GOTO)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CBO_END_WATT_GOTO)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CBO_END_TIME_GOTO)->ShowWindow(SW_HIDE);		

			GetDlgItem(IDC_CBO_ADD_WATTHOUR)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CBO_ADD_CAP)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CBO_ADD_WATTHOUR)->ShowWindow(SW_SHOW);
			//★★ ljb 20091231 E ★★★★★★★★

			GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);                     
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);                     
			if(bOverChargerSet)
			{
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE); 
			GetDlgItem(IDC_CHK_WAIT_TIME_INIT)->EnableWindow(TRUE); //20160422 add
			GetDlgItem(IDC_WAIT_DAY_EDIT)->EnableWindow(FALSE);			//20170501 add
			GetDlgItem(IDC_WAIT_DATETIMEPICKER)->EnableWindow(FALSE);	//20170501 add

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(FALSE); //yulee 20180927
			break;
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////

		case PS_STEP_CHARGE:
			if (pPreStep != NULL)
			{
				if (pPreStep->chType == PS_STEP_CHARGE)	GetDlgItem(IDC_CHK_LINK_PRE_STEP)->EnableWindow(TRUE);		//ljb 201010  ★★★★★★★★
			}
//			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(FALSE);		//★★ ljb 201010  ★★★★★★★★
			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(TRUE);		//ljb 20170926  ★★★★★★★★
			if(ListUpValueStepCombo() < 1)
			{
				GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(FALSE);
				GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_CBO_VALUE_ITEM)->EnableWindow(TRUE); 

// 			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(TRUE); 
// 			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(TRUE); 
// 			GetDlgItem(IDC_CBO_END_TIME_GOTO)->EnableWindow(TRUE); 

			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(TRUE); //yulee 20190531_2
			

#ifndef _EDLC_CELL_
			GetDlgItem(IDC_END_CAP)->EnableWindow(bEnable); 
#endif
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(bEnable); 
			GetDlgItem(IDC_END_WATT)->EnableWindow(bEnable); //EndWatt
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(bEnable); 
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);            //ljb 2011   
			if(bOverChargerSet)
			{      
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE);	//ljb 2011

			if(pStep->chMode == PS_MODE_CC)
			{
				GetDlgItem(IDC_END_VTG_H)->SetFocus();
				GetDlgItem(IDC_END_VTG_H)->EnableWindow(bEnable);                     
				GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CCCV)
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);                     
 				GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(FALSE);	
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				GetDlgItem(IDC_CV_DAY_EDIT)->EnableWindow(TRUE); 
				GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(TRUE); 
				GetDlgItem(IDC_CV_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				GetDlgItem(IDC_END_VTG_H)->SetFocus();
				GetDlgItem(IDC_END_VTG_H)->EnableWindow(bEnable);                     
 				GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 
				GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CCCP)
			{
				GetDlgItem(IDC_END_VTG_H)->SetFocus();
				GetDlgItem(IDC_END_VTG_H)->EnableWindow(bEnable);                     
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 
				GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);                     
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);                     
				if(bOverChargerSet)
				{
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
				}
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				//ksj 20160627
				GetDlgItem(IDC_CV_DAY_EDIT)->EnableWindow(TRUE); 
				GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(TRUE); 
				GetDlgItem(IDC_CV_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				GetDlgItem(IDC_END_VTG_H)->EnableWindow(bEnable);                     
				GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else
			{
				GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);                     
				GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(FALSE); 
//				GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);                     
// 				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}	
		
			break;

		case PS_STEP_DISCHARGE:
			if (pPreStep != NULL)
			{
//ljb 20150805 주석처리 SBC와 챔버통신 안함				if (pPreStep->chType == PS_STEP_DISCHARGE)	GetDlgItem(IDC_CHK_LINK_PRE_STEP)->EnableWindow(TRUE);		//ljb 201010  ★★★★★★★★			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(TRUE);		//ljb 201010  ★★★★★★★★
			}
//ljb 20150805 주석처리 SBC와 챔버통신 안함			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(FALSE);		//ljb 201010  ★★★★★★★★
			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(TRUE);		//ljb 20170926  ★★★★★★★★
			if(ListUpValueStepCombo() < 1)
			{
				GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(FALSE);
				GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_CBO_VALUE_ITEM)->EnableWindow(TRUE); 
			GetDlgItem(IDC_CBO_END_TIME_GOTO)->EnableWindow(TRUE); 

#ifndef _EDLC_CELL_
			GetDlgItem(IDC_END_CAP)->EnableWindow(bEnable); 
#endif			
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE);
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(bEnable); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(bEnable); 
			GetDlgItem(IDC_END_WATT)->EnableWindow(bEnable); 
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);            //ljb 2011         
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(FALSE);	//ljb 2011

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(TRUE); //yulee 20190531_2

			if(pStep->chMode == PS_MODE_CC)
			{
				GetDlgItem(IDC_END_VTG_L)->SetFocus();
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(bEnable);
				if(bOverChargerSet)
				{                     
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(bEnable); //yulee 20191017 OverChargeDischarger Mark
				}
				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CCCV)		//ljb 20101214
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);      
			if(bOverChargerSet)
			{               
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
				}
				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				GetDlgItem(IDC_CV_DAY_EDIT)->EnableWindow(TRUE); 
				GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(TRUE); 
				GetDlgItem(IDC_CV_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			else if(pStep->chMode == PS_MODE_CP)
			{
				GetDlgItem(IDC_END_VTG_L)->SetFocus();
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(bEnable);   
			if(bOverChargerSet)
			{                  
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(bEnable); //yulee 20191017 OverChargeDischarger Mark
				}
 				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 				
				GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CCCP)		//ljb 20101214
			{
				GetDlgItem(IDC_END_VTG_L)->SetFocus();
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(bEnable);                     
			if(bOverChargerSet)
			{
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(bEnable); //yulee 20191017 OverChargeDischarger Mark
				}
				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 				
				GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(FALSE); 
			}
			else if(pStep->chMode == PS_MODE_CV)
			{
				GetDlgItem(IDC_END_CRT)->SetFocus();
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);                     
			if(bOverChargerSet)
			{
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
				}
 				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
				//ksj 20160627
				GetDlgItem(IDC_CV_DAY_EDIT)->EnableWindow(TRUE); 
				GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(TRUE); 
				GetDlgItem(IDC_CV_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			else if(pStep->chMode == PS_MODE_CR)
			{
				//ljb GetDlgItem(IDC_END_VTG_H)->EnableWindow(bEnable);                     
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(bEnable);                     
			if(bOverChargerSet)
			{
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(bEnable); //yulee 20191017 OverChargeDischarger Mark
				}
 				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(bEnable); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(bEnable); 
			}
			else
			{
				GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);                     
			if(bOverChargerSet)
			{
				GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
				}
 				GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 
				GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			}				
			break;
		case PS_STEP_REST:
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(TRUE);                     
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(TRUE);
			if(bOverChargerSet)
			{
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(TRUE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(TRUE); 
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(TRUE); 

			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 

			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 

			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(TRUE);		//ljb 20170926  ★★★★★★★★

//			GetDlgItem(IDC_WAIT_DAY_EDIT)->EnableWindow(TRUE);			//20160422 add
//			GetDlgItem(IDC_WAIT_DATETIMEPICKER)->EnableWindow(TRUE);	//20160422 add

			break;

		case PS_STEP_IMPEDANCE:
			if(pStep->chMode == PS_MODE_ACIMP)
			{
				GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			}
			else//pStep->chMode == PS_MODE_DCIMP)
			{
				GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
				GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
				GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			}
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);                     
#ifdef _EDLC_CELL_
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(TRUE);		//ljb 20160314 edit		//v100d		//20170914 YULEE v100f 영문 EDLC ESR 기능 사용
#else
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);											
#endif
			if(bOverChargerSet)
			{								
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(FALSE); 
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 
			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(TRUE); //yulee 20190531_2
// 			GetDlgItem(IDC_CBO_END_TIME_GOTO)->EnableWindow(TRUE); 
// 			GetDlgItem(IDC_CBO_END_TIME_GOTO)->EnableWindow(FALSE); 
// 			GetDlgItem(IDC_CBO_END_CAPA_GOTO)->EnableWindow(FALSE); 
// 			GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(FALSE); 
			break;

		case PS_STEP_LOOP:
			GetDlgItem(IDC_CBO_LOADER_VALUE_ITEM)->EnableWindow(FALSE); //  [4/22/2015 이재복]
			//2014.12.08추가
			GetDlgItem(IDC_CHK_NOCHECK_PROG_MODE)->ShowWindow(FALSE);
			GetDlgItem(IDC_CHK_CANTXOFF_PROG_MODE)->ShowWindow(FALSE); //2014.12.09 추가
			//GetDlgItem(IDC_CHK_CAN_MODE)->ShowWindow(FALSE); //20150825 add
			GetDlgItem(IDC_CBO_CAN_CHECK)->ShowWindow(FALSE);	//20170328 add
			
			GetDlgItem(IDC_STATIC_MULTI_ID)->SetWindowText(Fun_FindMsg("OnShowWindow_msg7","IDD_END_COND_DLG"));				
			GetDlgItem(IDC_CBO_MULTI_GROUP)->EnableWindow(TRUE);			
			GetDlgItem(IDC_EDIT_CYCLE)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_CYCLE)->SendMessage(EM_SETSEL, 0, -1);
			GetDlgItem(IDC_EDIT_CYCLE)->SetFocus();

			GetDlgItem(IDC_GOTO_COMBO)->EnableWindow(TRUE);
			GetDlgItem(IDC_CBO_MULTI_GROUP)->EnableWindow(TRUE);
			GetDlgItem(IDC_CBO_ACC_GROUP)->EnableWindow(TRUE);	
			
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);                     
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);                     
			if(bOverChargerSet)
			{
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(FALSE); 
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 
			//GetDlgItem(IDC_CHK_CYCLE_PAUSE)->EnableWindow(TRUE);		//★★ ljb 201010  ★★★★★★★★ //ksj 20200831 : 주석처리
			GetDlgItem(IDC_WAIT_DAY_EDIT)->EnableWindow(FALSE);			//20170501 add
			GetDlgItem(IDC_WAIT_DATETIMEPICKER)->EnableWindow(FALSE);	//20170501 add

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(FALSE); //yulee 20180927

			//ksj 20200831 : 사이클 종료 후 퍼즈 버튼 활성화 조건 변경
			//마지막 loop에서는 비활성화 처리 한다.			
			//간략하게, 마지막 스텝이면서 loop 이거나, 마지막에서 두번째이면서 loop 이면 마지막 loop로 간이 판단. 
			if(pStep->chStepNo == nTotalStep || pStep->chStepNo == nTotalStep-1) 
			{
				m_bUseCyclePause = FALSE;
				pStep->bUseCyclePause = FALSE;
				UpdateData(FALSE);
				GetDlgItem(IDC_CHK_CYCLE_PAUSE)->EnableWindow(FALSE);
			}
			else
			{
				GetDlgItem(IDC_CHK_CYCLE_PAUSE)->EnableWindow(TRUE);
			}
			
			//ksj end
			break;

		case PS_STEP_PATTERN:
//ljb 20150805 주석처리 SBC와 챔버통신 안함			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(TRUE);		//ljb 201010  ★★★★★★★★
			if(ListUpValueStepCombo() < 1)
			{
				GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(FALSE);
				GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_CBO_VALUE_ITEM)->EnableWindow(TRUE); 

			GetDlgItem(IDC_STATIC_ENDC)->SetWindowText(Fun_FindMsg("OnShowWindow_msg5","IDD_END_COND_DLG"));
			GetDlgItem(IDC_STATIC_END_WATTHOUR)->SetWindowText(Fun_FindMsg("OnShowWindow_msg6","IDD_END_COND_DLG"));
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(TRUE);                     
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(TRUE);                     
			if(bOverChargerSet)
			{
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(TRUE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(TRUE); 
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_CRT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE);		//end power 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(TRUE);

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(TRUE); //yulee 20190531_2
			break;
		//2014.09.02 UserMap 추가 
		case PS_STEP_USER_MAP:
			if(ListUpValueStepCombo() < 1)
			{
				GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(FALSE);
				GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_CBO_VALUE_ITEM)->EnableWindow(TRUE); 
			
			GetDlgItem(IDC_STATIC_ENDC)->SetWindowText(Fun_FindMsg("OnShowWindow_msg5","IDD_END_COND_DLG"));
			GetDlgItem(IDC_STATIC_END_WATTHOUR)->SetWindowText(Fun_FindMsg("OnShowWindow_msg6","IDD_END_COND_DLG"));
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(TRUE);                     
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(TRUE);                     
			if(bOverChargerSet)
			{
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(TRUE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(TRUE); 
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_CRT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_WATT)->EnableWindow(TRUE);	
			
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(TRUE);	

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(TRUE); //yulee 20190531_2
			
			break;
		//ljb 2011318 이재복 ////////// s
		case PS_STEP_EXT_CAN:
//ljb 20150805 주석처리 SBC와 챔버통신 안함			GetDlgItem(IDC_CHK_CHAMBER_PROG_MODE)->EnableWindow(TRUE);		//ljb 201010  ★★★★★★★★
			if(ListUpValueStepCombo() < 1)
			{
				GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(FALSE);
				GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(FALSE); 
			}
			GetDlgItem(IDC_CBO_VALUE_ITEM)->EnableWindow(TRUE); 
			
			GetDlgItem(IDC_STATIC_ENDC)->SetWindowText(Fun_FindMsg("OnShowWindow_msg5","IDD_END_COND_DLG"));
			GetDlgItem(IDC_STATIC_END_WATTHOUR)->SetWindowText(Fun_FindMsg("OnShowWindow_msg6","IDD_END_COND_DLG"));
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(TRUE);                     
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(TRUE);                    
			if(bOverChargerSet)
			{ 
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(TRUE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(TRUE); 
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_CRT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE);
			GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE);		//end power 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(TRUE);

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(TRUE); //yulee 20190531_2
			break;
//ljb 2011318 이재복 ////////// e
		case PS_STEP_OCV:
		case PS_STEP_END:
			//2014.12.08추가
			GetDlgItem(IDC_CHK_NOCHECK_PROG_MODE)->ShowWindow(FALSE);
			GetDlgItem(IDC_CHK_CANTXOFF_PROG_MODE)->ShowWindow(FALSE); //2014.12.09 추가
			//GetDlgItem(IDC_CHK_CAN_MODE)->ShowWindow(FALSE); //20150825 add
			GetDlgItem(IDC_CBO_CAN_CHECK)->ShowWindow(FALSE);	//20170328 add
			GetDlgItem(IDC_WAIT_DAY_EDIT)->EnableWindow(FALSE);			//20170501 add
			GetDlgItem(IDC_WAIT_DATETIMEPICKER)->EnableWindow(FALSE);	//20170501 add

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(FALSE); //yulee 20180927

			break;
		default:
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(FALSE);                     
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(FALSE);                 
			if(bOverChargerSet)
			{    
			GetDlgItem(IDC_CHECK_ZERO_VOLTAGE)->EnableWindow(FALSE); //yulee 20191017 OverChargeDischarger Mark
			}
			GetDlgItem(IDC_END_CRT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_DELTAV)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 
			GetDlgItem(IDC_WAIT_DAY_EDIT)->EnableWindow(FALSE);			//20170501 add
			GetDlgItem(IDC_WAIT_DATETIMEPICKER)->EnableWindow(FALSE);	//20170501 add

			GetDlgItem(IDC_CHK_CHAMBER_STEP_STOP)->ShowWindow(FALSE); //yulee 20190531_2
			break;
		}

		//lyj 20200730 s HLGP LOOP 종료 조건 이름변경 --------------------------------------
		if(m_bUseHLGP && pStep->chType == PS_STEP_LOOP)
		{
			GetDlgItem(IDC_STATIC_ENDV_H)->SetWindowText("Ah_Upper");				
			GetDlgItem(IDC_STATIC_ENDV_L)->SetWindowText("Ah_Lower");				
			GetDlgItem(IDC_END_VTG_H_UNIT_STATIC)->SetWindowText(m_pDoc->GetCapUnit());
			GetDlgItem(IDC_END_VTG_L_UNIT_STATIC)->SetWindowText(m_pDoc->GetCapUnit());	
			GetDlgItem(IDC_END_VTG_H)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_VTG_L)->EnableWindow(TRUE); 			
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(TRUE); 			
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(TRUE);
				
			GetDlgItem(IDC_STATIC_ENDC)->SetWindowText("Wh_Upper");				
			GetDlgItem(IDC_STATIC_END_WATTHOUR)->SetWindowText("Wh_Lower");				
			GetDlgItem(IDC_END_CAPA_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattHourUnit());
			GetDlgItem(IDC_END_WATTHOUR_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattHourUnit());
			GetDlgItem(IDC_END_CAP)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(TRUE); 			
			GetDlgItem(IDC_CBO_END_CAPA_GOTO)->EnableWindow(TRUE); 			
			GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(TRUE); 			
		}
		else
		{
			GetDlgItem(IDC_STATIC_ENDV_H)->SetWindowText(Fun_FindMsg("IDC_STATIC_ENDV_H","IDD_END_COND_DLG"));	//종료전압 HIGH		
			GetDlgItem(IDC_STATIC_ENDV_L)->SetWindowText(Fun_FindMsg("IDC_STATIC_ENDV_L","IDD_END_COND_DLG"));	//종료전압 LOW			
			GetDlgItem(IDC_END_VTG_H_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());
			GetDlgItem(IDC_END_VTG_L_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());
			GetDlgItem(IDC_CBO_END_VTG_H_GOTO)->EnableWindow(FALSE); 			
			GetDlgItem(IDC_CBO_END_VTG_L_GOTO)->EnableWindow(FALSE); 			
			GetDlgItem(IDC_STATIC_ENDC)->SetWindowText(Fun_FindMsg("IDC_STATIC_ENDC","IDD_END_COND_DLG"));		//종료용량		
			GetDlgItem(IDC_STATIC_END_WATTHOUR)->SetWindowText(" WattHour");				
			GetDlgItem(IDC_END_CAPA_UNIT_STATIC)->SetWindowText(m_pDoc->GetCapUnit());
			GetDlgItem(IDC_END_WATTHOUR_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattHourUnit());
//			GetDlgItem(IDC_END_CAP)->EnableWindow(FALSE);  //lyj 20200901 HLGP
// 			if(pStep->chType == PS_STEP_PATTERN) //lyj 20200901 HLGP
// 				GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(TRUE); //lyj 20200901 HLGP
// 			else
// 				GetDlgItem(IDC_END_WATTHOUR)->EnableWindow(FALSE); 	

			GetDlgItem(IDC_CBO_END_CAPA_GOTO)->EnableWindow(FALSE); 			
			GetDlgItem(IDC_CBO_END_WATT_GOTO)->EnableWindow(FALSE); 			

		}
		//lyj 20200730 e HLGP LOOP 종료 조건 이름변경 --------------------------------------


		//  [4/22/2015 이재복]
		if (pStep->nValueLoaderItem == 1)
		{
			if(UseLoader){
				//GetDlgItem(IDC_CBO_LOADER_VALUE_ITEM)->EnableWindow(TRUE); //  [4/22/2015 이재복]
				GetDlgItem(IDC_CBO_LOADER_MODE_VALUE)->EnableWindow(TRUE); //  [4/22/2015 이재복]
				GetDlgItem(IDC_EDIT_LOADER_SET_VALUE)->EnableWindow(TRUE); //  [4/22/2015 이재복]
			}
		}

		//InitCanAuxComboList();		//ljb 2011221 이재복 //////////

		BOOL bUseOnlyMaster = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseCanCheckModeOnlyMaster", FALSE);	//20150825 캔 통신체크 모드 //20180518 yulee master만 사용 시
		int nCount = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Count", 4);			//ljb 20170720 add

		/*
		EndConditionDlg_canComboList_msg1	=	CAN 통신 체크 Master + Slave
		EndConditionDlg_canComboList_msg2	=	CAN 통신 체크 Release
		EndConditionDlg_canComboList_msg3	=	CAN 통신 체크 Master
		EndConditionDlg_canComboList_msg4	=	CAN 통신 체크 Slave
		*/

		if(bUseOnlyMaster == FALSE)
		{
			//ljb 20170328 add
			m_ctrlCboCanCheck.ResetContent();
			m_ctrlCboCanCheck.AddString(Fun_FindMsg("EndConditionDlg_canComboList_msg1","IDD_END_COND_DLG"));	//CAN 통신 체크 Master + Slave
			m_ctrlCboCanCheck.SetItemData(0,0);
			m_ctrlCboCanCheck.AddString(Fun_FindMsg("EndConditionDlg_canComboList_msg2","IDD_END_COND_DLG"));	//CAN 통신 체크 Release
			m_ctrlCboCanCheck.SetItemData(1,1);

			if (nCount == 4)	//ljb 20170720 add
			{
				m_ctrlCboCanCheck.AddString(Fun_FindMsg("EndConditionDlg_canComboList_msg3","IDD_END_COND_DLG"));	//CAN 통신 체크 Master	
				m_ctrlCboCanCheck.SetItemData(2,2);
				m_ctrlCboCanCheck.AddString(Fun_FindMsg("EndConditionDlg_canComboList_msg4","IDD_END_COND_DLG"));	//CAN 통신 체크 Slave
				m_ctrlCboCanCheck.SetItemData(3,3);
			}
		}
		else
		{
			//ljb 20170328 add
			m_ctrlCboCanCheck.ResetContent();
			m_ctrlCboCanCheck.AddString(Fun_FindMsg("EndConditionDlg_canComboList_msg3","IDD_END_COND_DLG"));	//CAN 통신 체크 Master	
			//m_ctrlCboCanCheck.SetItemData(0,0);
			//m_ctrlCboCanCheck.SetItemData(0,2);  //ksj 20201110 : Can 통신체크 Master의 ItemData는 2로 해줘야한다. 기존 0,0은 Master +Slave 로 동작할 듯. 20200511에 아래에다 잘못고쳐서 옮겨 고침. Slave 미사용 설비에서도 0으로 보내도 되면 궂이 이 코드 사용할 필요 없을듯.
			m_ctrlCboCanCheck.SetItemData(0,2);  //ksj 20210615 : Slave까지 체크하는 현상 확인되어, 정확하게 Master 만 체크하도록 다시 변경.

			m_ctrlCboCanCheck.AddString(Fun_FindMsg("EndConditionDlg_canComboList_msg2","IDD_END_COND_DLG"));	//CAN 통신 체크 Release
			//m_ctrlCboCanCheck.SetItemData(1,1);
			//m_ctrlCboCanCheck.SetItemData(1,2); //ksj 2020511 : Can 통신체크 Master의 ItemData는 2로 해줘야한다. 기존 1,1은 Master +Slave 로 동작할 듯.
			m_ctrlCboCanCheck.SetItemData(1,1); //ksj 20201110 : 원복, 위에 문제 있음 마스터로 착각. 릴리즈가 맞음. EndConditionDlg_canComboList_msg2 -> CAN 통신 체크 Release
		}

		ListUpGotoStepCombo();

		UpdateEndGotoList();
		UpdateBMSGotoList();
		DisplayData();
		
		
	}
	else			//Hide Window
	{
		if (UpdateEndCondition() == FALSE)
			return; //yulee 20180905
	}

}		


void CEndConditionDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
}

void CEndConditionDlg::OnOK()
{
	if (UpdateEndCondition())	CDialog::OnOK();
}

BOOL CEndConditionDlg::UpdateEndCondition()
{
	//yulee 20180905 
	int nTmpCntNo = 0;

	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_1) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_2) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_3) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_4) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_5) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_6) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_7) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_8) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_9) >10) nTmpCntNo++;
	if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_10) >10) nTmpCntNo++;
	if(nTmpCntNo > 0)
		//&& AfxMessageBox("지속시간 설정 오류가 있습니다.");  //$1013
		AfxMessageBox(Fun_FindMsg("EndConditionDlg_UpdateEndCondition_msg1","IDD_END_COND_DLG")); 


	UpdateData();

	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	ASSERT(pStep);

	if(nTmpCntNo)
	{
		//&& AfxMessageBox("범위가 넘는 값은 최대값으로 입력됩니다."); //$1013
		AfxMessageBox(Fun_FindMsg("EndConditionDlg_UpdateEndCondition_msg2","IDD_END_COND_DLG")); 
	}

		
	if(m_nStepNo <1 || m_nStepNo >SCH_MAX_STEP)		return FALSE;
	double	dwValue =0.0;
	bool bChkNextStep = false;
	
	if(pStep)
	{
		m_EndV_H.GetValue(dwValue);		pStep->fEndV_H = m_pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);		//V단위 표기 
		m_EndV_L.GetValue(dwValue);		pStep->fEndV_L = m_pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);		//V단위 표기 
		m_EndI.GetValue(dwValue);		pStep->fEndI = m_pDoc->IUnitTrans(dwValue);	//(dwValue*I_UNIT_FACTOR);	
		m_EndC.GetValue(dwValue);		pStep->fEndC = m_pDoc->CUnitTrans(dwValue);	//(dwValue*C_UNIT_FACTOR);											//F단위 표기 
		
		//20170526 수정 시작
		if (pStep->fEndC != 0)
		{
			if (pStep->chType == PS_STEP_PATTERN )
			{
				// 			  if (pStep->fEndC > 0) pStep->fCLimitHigh =    pStep->fEndC * 1.1;   //ljb 20170526 add 스텝안전조건 용량 설정 110%
				// 			  if (pStep->fEndC < 0) pStep->fCLimitLow =    pStep->fEndC * 1.1;   //ljb 20170526 add 스텝안전조건 용량 설정 110%			  
			}
			else if (pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
			{
				pStep->fCLimitLow = 0;
				pStep->fCLimitHigh =    pStep->fEndC * 1.1;   //ljb 20150806 add 스텝안전조건 용량 설정 110%
			}
		}
		else
		{
			pStep->fCLimitLow = 0;		//ljb 20180213 edit 종료 용량 값이 0이면 안전 값도 0으로 변경 (최종성 책임 수정요청)
			pStep->fCLimitHigh = 0;		//ljb 20180213 edit 종료 용량 값이 0이면 안전 값도 0으로 변경 (최종성 책임 수정요청)
		}
		//20170526 수정 끝

		//20170526 YULEE 수정 전 주석처리 시작
		//if (pStep->fEndC != 0) pStep->fCLimitHigh = 	pStep->fEndC * 1.1;	//ljb 20150806 add 스텝안전조건 용량 설정 110%
		//20170526 YULEE 수정 전 주석처리 끝

		m_EnddV.GetValue(dwValue);		pStep->fEndDV = m_pDoc->VUnitTrans(dwValue);	//(dwValue*V_UNIT_FACTOR);
		m_EndWatt.GetValue(dwValue);				pStep->fEndW = m_pDoc->WattUnitTrans(dwValue);	//(dwValue*I_UNIT_FACTOR);
		m_EndWattHour.GetValue(dwValue);		pStep->fEndWh = m_pDoc->WattHourUnitTrans(dwValue);	//(dwValue*I_UNIT_FACTOR);
		m_EndTemp.GetValue(dwValue);		pStep->fEndTemp =  (float)dwValue;	//(dwValue*I_UNIT_FACTOR);
		
		//Goto Value 가져오기
		if(m_ctrlCboEndVtg_H.GetCurSel() != LB_ERR)	 
			pStep->nGotoStepEndV_H		= m_ctrlCboEndVtg_H.GetItemData(m_ctrlCboEndVtg_H.GetCurSel());
		else pStep->nGotoStepEndV_H = PS_STEP_NEXT;
		if(m_ctrlCboEndVtg_L.GetCurSel() != LB_ERR)	 pStep->nGotoStepEndV_L		= m_ctrlCboEndVtg_L.GetItemData(m_ctrlCboEndVtg_L.GetCurSel());
		else pStep->nGotoStepEndV_L = PS_STEP_NEXT;
		if(m_ctrlCboEndTime.GetCurSel() != LB_ERR)	 pStep->nGotoStepEndTime	= m_ctrlCboEndTime.GetItemData(m_ctrlCboEndTime.GetCurSel());
		else pStep->nGotoStepEndTime = PS_STEP_NEXT;
		if(m_ctrlCboEndCVTime.GetCurSel() != LB_ERR) pStep->nGotoStepEndCVTime	= m_ctrlCboEndCVTime.GetItemData(m_ctrlCboEndCVTime.GetCurSel());
		else pStep->nGotoStepEndCVTime = PS_STEP_NEXT;
		if(m_ctrlCboEndCapa.GetCurSel() != LB_ERR)	 pStep->nGotoStepEndC		= m_ctrlCboEndCapa.GetItemData(m_ctrlCboEndCapa.GetCurSel());
		else pStep->nGotoStepEndC = PS_STEP_NEXT;
		if(m_ctrlCboEndWattHour.GetCurSel() != LB_ERR)	pStep->nGotoStepEndWh	= m_ctrlCboEndWattHour.GetItemData(m_ctrlCboEndWattHour.GetCurSel());
		else pStep->nGotoStepEndWh = PS_STEP_NEXT;
		if(m_ctrlCboEndValue.GetCurSel() != LB_ERR)		pStep->nGotoStepEndValue = m_ctrlCboEndValue.GetItemData(m_ctrlCboEndValue.GetCurSel());
		else pStep->nGotoStepEndValue = PS_STEP_NEXT;


		COleDateTime endTime;
		m_ctrlEndTime.GetTime(endTime);
//		endTime = m_EndTime.GetDateTime();
		pStep->ulEndTimeDay = m_nEndDay;	//ljb 20131212 add
		pStep->ulEndTime = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond())*100;		
		//pStep->ulEndTime = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() + m_nEndDay * 86400)*100;		
		pStep->ulEndTime += (m_lEndMSec/10);

		COleDateTime CVTime;
		m_ctrlCVTime.GetTime(CVTime);
		pStep->ulCVTimeDay = m_nCVTimeDay;	//ljb 20131212 add
		//pStep->ulCVTime = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond() + m_nCVTimeDay * 86400)*100;		
		pStep->ulCVTime = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond())*100;		
		pStep->ulCVTime += (m_lCVTimeMSec/10);
		

		//ljb 20160422 add sssssss
		pStep->nWaitTimeInit = m_bUseWaitTimeInit;
		pStep->nWaitTimeDay = m_nWaitTimeDay;
		COleDateTime waitTime;
		m_ctrlWaitTime.GetTime(waitTime);
		pStep->nWaitTimeHour = waitTime.GetHour();
		pStep->nWaitTimeMin = waitTime.GetMinute();
		pStep->nWaitTimeSec = waitTime.GetSecond();
		//ljb 20160422 add eeeeeee

		//ljb 2009-03-24 cycle ////////////////////////////////
		pStep->nLoopInfoCycle			= m_ctrlEditLoopInfoCycle;
		if(m_ctrlGoto.GetCurSel() != LB_ERR)
		{
			pStep->nLoopInfoGotoStep			= m_ctrlGoto.GetItemData(m_ctrlGoto.GetCurSel());
		}
		else	pStep->nLoopInfoGotoStep = PS_STEP_NEXT;
		
		// 		if(m_nLoopInfoEndVGoto > 0)
		// 		{
		// 			STEP *pGotoStep = (STEP*)m_pDoc->m_apStep[m_nLoopInfoEndVGoto-1];
		// 			pStep->nLoopInfoEndVGoto       =  pGotoStep->nGotoStepID;
		// 		}
		// 		else
		// 		{
		// 			pStep->nLoopInfoEndVGoto = 0;
		// 		}
		//////////////////////////////////////////////////////////////////////////
		//pStep->nLoopInfoGoto			= m_nLoopInfoGoto;

		//Multi 그룹 설정		v1009
		if(m_ctrlCboLoopMultiGroupID.GetCurSel() != LB_ERR)
			pStep->nMultiLoopGroupID		= m_ctrlCboLoopMultiGroupID.GetCurSel();
		else pStep->nMultiLoopGroupID		= 0;

		//Acc 그룹 설정		v1009
		if(m_ctrlCboLoopAccGroupID.GetCurSel() != LB_ERR)
			pStep->nAccLoopGroupID		= m_ctrlCboLoopAccGroupID.GetCurSel();
		else pStep->nAccLoopGroupID		= 0;
		
		//ljb 20100820 for v100A /////////////////////////////////////////
//ljb 임시
//		iCode = m_ctrlCanDivision_2.GetItemData(m_ctrlCanDivision_2.GetCurSel());
//		pStep->ican_function_division[0] = m_ctrlCanDivision_1.GetItemData(m_ctrlCanDivision_1.GetCurSel());

// 		pStep->ican_function_division[0] = m_ctrlCanDivision_1.GetItemData(m_ctrlCanDivision_1.GetCurSel());
// 		pStep->ican_function_division[1] = m_ctrlCanDivision_2.GetItemData(m_ctrlCanDivision_2.GetCurSel());
// 		pStep->ican_function_division[2] = m_ctrlCanDivision_3.GetItemData(m_ctrlCanDivision_3.GetCurSel());
// 		pStep->ican_function_division[3] = m_ctrlCanDivision_4.GetItemData(m_ctrlCanDivision_4.GetCurSel());
// 		pStep->ican_function_division[4] = m_ctrlCanDivision_5.GetItemData(m_ctrlCanDivision_5.GetCurSel());
// 		pStep->ican_function_division[5] = m_ctrlCanDivision_6.GetItemData(m_ctrlCanDivision_6.GetCurSel());
// 		pStep->ican_function_division[6] = m_ctrlCanDivision_7.GetItemData(m_ctrlCanDivision_7.GetCurSel());
// 		pStep->ican_function_division[7] = m_ctrlCanDivision_8.GetItemData(m_ctrlCanDivision_8.GetCurSel());
// 		pStep->ican_function_division[8] = m_ctrlCanDivision_9.GetItemData(m_ctrlCanDivision_9.GetCurSel());
// 		pStep->ican_function_division[9] = m_ctrlCanDivision_10.GetItemData(m_ctrlCanDivision_10.GetCurSel());
// 
// 		pStep->iaux_function_division[0] = m_ctrlAuxDivision_1.GetItemData(m_ctrlAuxDivision_1.GetCurSel());
// 		pStep->iaux_function_division[1] = m_ctrlAuxDivision_2.GetItemData(m_ctrlAuxDivision_2.GetCurSel());
// 		pStep->iaux_function_division[2] = m_ctrlAuxDivision_3.GetItemData(m_ctrlAuxDivision_3.GetCurSel());
// 		pStep->iaux_function_division[3] = m_ctrlAuxDivision_4.GetItemData(m_ctrlAuxDivision_4.GetCurSel());
// 		pStep->iaux_function_division[4] = m_ctrlAuxDivision_5.GetItemData(m_ctrlAuxDivision_5.GetCurSel());
// 		pStep->iaux_function_division[5] = m_ctrlAuxDivision_6.GetItemData(m_ctrlAuxDivision_6.GetCurSel());
// 		pStep->iaux_function_division[6] = m_ctrlAuxDivision_7.GetItemData(m_ctrlAuxDivision_7.GetCurSel());
// 		pStep->iaux_function_division[7] = m_ctrlAuxDivision_8.GetItemData(m_ctrlAuxDivision_8.GetCurSel());
// 		pStep->iaux_function_division[8] = m_ctrlAuxDivision_9.GetItemData(m_ctrlAuxDivision_9.GetCurSel());
// 		pStep->iaux_function_division[9] = m_ctrlAuxDivision_10.GetItemData(m_ctrlAuxDivision_10.GetCurSel());

		CString strTemp;
		if (pStep->ican_function_division[0] > 0)
		{
			
			pStep->fcan_Value[0] = m_fCanValue_1;
			if(m_ctrlCanDataType1.GetCurSel() != LB_ERR)	pStep->cCan_data_type[0]	= m_ctrlCanDataType1.GetItemData(m_ctrlCanDataType1.GetCurSel());
			if(m_ctrlCanCompare1.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[0] >= 1961) && (pStep->ican_function_division[0] <= 1968)) ||
					((pStep->ican_function_division[0] >= 1976) && (pStep->ican_function_division[0] <= 1977))||
					((pStep->ican_function_division[0] >= 1984) && (pStep->ican_function_division[0] <= 1985))||
					((pStep->ican_function_division[0] >= 1978) && (pStep->ican_function_division[0] <= 1979))||
					((pStep->ican_function_division[0] >= 1986) && (pStep->ican_function_division[0] <= 1987)))
					//4Ch
				//if((pStep->ican_function_division[0] >= 1961) && (pStep->ican_function_division[0] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[0] = 3;
				}
				else
				{
// 					if((pStep->cCan_compare_type[0] != 0) && (m_ctrlCanCompare1.GetItemData(m_ctrlCanCompare1.GetCurSel()) == 0))
// 					{
// 					}
// 					else
						pStep->cCan_compare_type[0]	= m_ctrlCanCompare1.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_1.GetCurSel() != LB_ERR)	pStep->ican_branch[0]		= m_ctrlCanGoto_1.GetItemData(m_ctrlCanGoto_1.GetCurSel());
		}
		else
		{
			pStep->fcan_Value[0] = 0;
			pStep->cCan_data_type[0]		= 0;
			pStep->cCan_compare_type[0]		= 0;
			pStep->ican_branch[0]		= PS_STEP_NEXT;
		}

		if (pStep->ican_function_division[1] > 0)
		{
// 			m_ctrlCanCompare2.EnableWindow(TRUE);
// 			m_ctrlCanGoto_2.EnableWindow(TRUE);
			pStep->fcan_Value[1] = m_fCanValue_2;
			if(m_ctrlCanDataType2.GetCurSel() != LB_ERR)	pStep->cCan_data_type[1]	= m_ctrlCanDataType2.GetItemData(m_ctrlCanDataType2.GetCurSel());
			if(m_ctrlCanCompare2.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[1] >= 1961) && (pStep->ican_function_division[1] <= 1968)) ||
					((pStep->ican_function_division[1] >= 1976) && (pStep->ican_function_division[1] <= 1977))||
					((pStep->ican_function_division[1] >= 1984) && (pStep->ican_function_division[1] <= 1985))||
					((pStep->ican_function_division[1] >= 1978) && (pStep->ican_function_division[1] <= 1979))||
					((pStep->ican_function_division[1] >= 1986) && (pStep->ican_function_division[1] <= 1987)))
					//if((pStep->ican_function_division[1] >= 1961) && (pStep->ican_function_division[1] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[1] = 3;
				}
				else
				{
// 					if((pStep->cCan_compare_type[1] != 0) && (m_ctrlCanCompare2.GetItemData(m_ctrlCanCompare2.GetCurSel()) == 0))
// 					{
// 
// 					}
// 					else
						pStep->cCan_compare_type[1]	= m_ctrlCanCompare2.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_2.GetCurSel() != LB_ERR)	pStep->ican_branch[1]		= m_ctrlCanGoto_2.GetItemData(m_ctrlCanGoto_2.GetCurSel());
			
		}
		else
		{
			pStep->fcan_Value[1] = 0;
			pStep->cCan_data_type[1]		= 0;
			pStep->cCan_compare_type[1]		= 0;
			pStep->ican_branch[1]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[2] > 0)
		{
			//m_ctrlCanCompare3.EnableWindow(TRUE);
			//m_ctrlCanGoto_3.EnableWindow(TRUE);
			pStep->fcan_Value[2] = m_fCanValue_3;
			if(m_ctrlCanDataType3.GetCurSel() != LB_ERR)	pStep->cCan_data_type[2]	= m_ctrlCanDataType3.GetItemData(m_ctrlCanDataType3.GetCurSel());
			if(m_ctrlCanCompare3.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[2] >= 1961) && (pStep->ican_function_division[2] <= 1968)) ||
					((pStep->ican_function_division[2] >= 1976) && (pStep->ican_function_division[2] <= 1977))||
					((pStep->ican_function_division[2] >= 1984) && (pStep->ican_function_division[2] <= 1985))||
					((pStep->ican_function_division[2] >= 1978) && (pStep->ican_function_division[2] <= 1979))||
					((pStep->ican_function_division[2] >= 1986) && (pStep->ican_function_division[2] <= 1987)))
					//if((pStep->ican_function_division[2] >= 1961) && (pStep->ican_function_division[2] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[2] = 3;
				}
				else
				{
// 					if((pStep->cCan_compare_type[2] != 0) && (m_ctrlCanCompare3.GetItemData(m_ctrlCanCompare3.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cCan_compare_type[2]	= m_ctrlCanCompare3.GetCurSel();
				}
			}if(m_ctrlCanGoto_3.GetCurSel() != LB_ERR)	pStep->ican_branch[2]		= m_ctrlCanGoto_3.GetItemData(m_ctrlCanGoto_3.GetCurSel());
			
		}
		else
		{
			pStep->fcan_Value[2] = 0;
			pStep->cCan_data_type[2]		= 0;
			pStep->cCan_compare_type[2]		= 0;
			pStep->ican_branch[2]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[3] > 0)
		{
			//m_ctrlCanCompare4.EnableWindow(TRUE);
			//m_ctrlCanGoto_4.EnableWindow(TRUE);
			pStep->fcan_Value[3] = m_fCanValue_4;
			if(m_ctrlCanDataType4.GetCurSel() != LB_ERR)	pStep->cCan_data_type[3]	= m_ctrlCanDataType4.GetItemData(m_ctrlCanDataType4.GetCurSel());
			if(m_ctrlCanCompare4.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[3] >= 1961) && (pStep->ican_function_division[3] <= 1968)) ||
					((pStep->ican_function_division[3] >= 1976) && (pStep->ican_function_division[3] <= 1977))||
					((pStep->ican_function_division[3] >= 1984) && (pStep->ican_function_division[3] <= 1985))||
					((pStep->ican_function_division[3] >= 1978) && (pStep->ican_function_division[3] <= 1979))||
					((pStep->ican_function_division[3] >= 1986) && (pStep->ican_function_division[3] <= 1987)))
					//if((pStep->ican_function_division[3] >= 1961) && (pStep->ican_function_division[3] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[3] = 3;
				}
				else
				{
// 					if((pStep->cCan_compare_type[3] != 0) && (m_ctrlCanCompare4.GetItemData(m_ctrlCanCompare4.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cCan_compare_type[3]	= m_ctrlCanCompare4.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_4.GetCurSel() != LB_ERR)	pStep->ican_branch[3]		= m_ctrlCanGoto_4.GetItemData(m_ctrlCanGoto_4.GetCurSel());
		
		}
		else
		{
			pStep->fcan_Value[3] = 0;
			pStep->cCan_data_type[3]		= 0;
			pStep->cCan_compare_type[3]		= 0;
			pStep->ican_branch[3]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[4] > 0)
		{
		//	m_ctrlCanCompare5.EnableWindow(TRUE);
		//	m_ctrlCanGoto_5.EnableWindow(TRUE);
			pStep->fcan_Value[4] = m_fCanValue_5;
			if(m_ctrlCanDataType5.GetCurSel() != LB_ERR)	pStep->cCan_data_type[4]	= m_ctrlCanDataType5.GetItemData(m_ctrlCanDataType5.GetCurSel());
			if(m_ctrlCanCompare5.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[4] >= 1961) && (pStep->ican_function_division[4] <= 1968)) ||
					((pStep->ican_function_division[4] >= 1976) && (pStep->ican_function_division[4] <= 1977))||
					((pStep->ican_function_division[4] >= 1984) && (pStep->ican_function_division[4] <= 1985))||
					((pStep->ican_function_division[4] >= 1978) && (pStep->ican_function_division[4] <= 1979))||
					((pStep->ican_function_division[4] >= 1986) && (pStep->ican_function_division[4] <= 1987)))
					//if((pStep->ican_function_division[4] >= 1961) && (pStep->ican_function_division[4] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[4] = 3;
				}
				else
				{
// 					if((pStep->cCan_compare_type[4] != 0) && (m_ctrlCanCompare5.GetItemData(m_ctrlCanCompare5.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cCan_compare_type[4]	= m_ctrlCanCompare5.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_5.GetCurSel() != LB_ERR)	pStep->ican_branch[4]		= m_ctrlCanGoto_5.GetItemData(m_ctrlCanGoto_5.GetCurSel());
		
		}
		else
		{
			pStep->fcan_Value[4] = 0;
			pStep->cCan_data_type[4]		= 0;
			pStep->cCan_compare_type[4]		= 0;
			pStep->ican_branch[4]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[5] > 0)
		{
			//m_ctrlCanCompare6.EnableWindow(TRUE);
			//m_ctrlCanGoto_6.EnableWindow(TRUE);
			pStep->fcan_Value[5] = m_fCanValue_6;
			if(m_ctrlCanDataType6.GetCurSel() != LB_ERR)	pStep->cCan_data_type[5]	= m_ctrlCanDataType6.GetItemData(m_ctrlCanDataType6.GetCurSel());
			if(m_ctrlCanCompare6.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[5] >= 1961) && (pStep->ican_function_division[5] <= 1968)) ||
					((pStep->ican_function_division[5] >= 1976) && (pStep->ican_function_division[5] <= 1977))||
					((pStep->ican_function_division[5] >= 1984) && (pStep->ican_function_division[5] <= 1985))||
					((pStep->ican_function_division[5] >= 1978) && (pStep->ican_function_division[5] <= 1979))||
					((pStep->ican_function_division[5] >= 1986) && (pStep->ican_function_division[5] <= 1987)))
					//if((pStep->ican_function_division[5] >= 1961) && (pStep->ican_function_division[5] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[5] = 3;
				}
				else
				{
//					if((pStep->cCan_compare_type[5] != 0) && (m_ctrlCanCompare6.GetItemData(m_ctrlCanCompare6.GetCurSel()) == 0))
//					{
//						
//					}
//					else

						pStep->cCan_compare_type[5]	= m_ctrlCanCompare6.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_6.GetCurSel() != LB_ERR)	pStep->ican_branch[5]		= m_ctrlCanGoto_6.GetItemData(m_ctrlCanGoto_6.GetCurSel());
	
		}
		else
		{
			pStep->fcan_Value[5] = 0;
			pStep->cCan_data_type[5]		= 0;
			pStep->cCan_compare_type[5]		= 0;
			pStep->ican_branch[5]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[6] > 0)
		{
			//m_ctrlCanCompare7.EnableWindow(TRUE);
			//m_ctrlCanGoto_7.EnableWindow(TRUE);
			pStep->fcan_Value[6] = m_fCanValue_7;
			if(m_ctrlCanDataType7.GetCurSel() != LB_ERR)	pStep->cCan_data_type[6]	= m_ctrlCanDataType7.GetItemData(m_ctrlCanDataType7.GetCurSel());
			if(m_ctrlCanCompare7.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[6] >= 1961) && (pStep->ican_function_division[6] <= 1968)) ||
					((pStep->ican_function_division[6] >= 1976) && (pStep->ican_function_division[6] <= 1977))||
					((pStep->ican_function_division[6] >= 1984) && (pStep->ican_function_division[6] <= 1985))||
					((pStep->ican_function_division[6] >= 1978) && (pStep->ican_function_division[6] <= 1979))||
					((pStep->ican_function_division[6] >= 1986) && (pStep->ican_function_division[6] <= 1987)))
					//if((pStep->ican_function_division[6] >= 1961) && (pStep->ican_function_division[6] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[6] = 3;
				}
				else
				{

						pStep->cCan_compare_type[6]	= m_ctrlCanCompare7.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_7.GetCurSel() != LB_ERR)	pStep->ican_branch[6]		= m_ctrlCanGoto_7.GetItemData(m_ctrlCanGoto_7.GetCurSel());
	
		}
		else
		{
			pStep->fcan_Value[6] = 0;
			pStep->cCan_data_type[6]		= 0;
			pStep->cCan_compare_type[6]		= 0;
			pStep->ican_branch[6]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[7] > 0)
		{
			//m_ctrlCanCompare8.EnableWindow(TRUE);
			//m_ctrlCanGoto_8.EnableWindow(TRUE);
			pStep->fcan_Value[7] = m_fCanValue_8;
			if(m_ctrlCanDataType8.GetCurSel() != LB_ERR)	pStep->cCan_data_type[7]	= m_ctrlCanDataType8.GetItemData(m_ctrlCanDataType8.GetCurSel());
			if(m_ctrlCanCompare8.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[7] >= 1961) && (pStep->ican_function_division[7] <= 1968)) ||
					((pStep->ican_function_division[7] >= 1976) && (pStep->ican_function_division[7] <= 1977))||
					((pStep->ican_function_division[7] >= 1984) && (pStep->ican_function_division[7] <= 1985))||
					((pStep->ican_function_division[7] >= 1978) && (pStep->ican_function_division[7] <= 1979))||
					((pStep->ican_function_division[7] >= 1986) && (pStep->ican_function_division[7] <= 1987)))
					//if((pStep->ican_function_division[7] >= 1961) && (pStep->ican_function_division[7] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[7] = 3;
				}
				else
				{

						pStep->cCan_compare_type[7]	= m_ctrlCanCompare8.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_8.GetCurSel() != LB_ERR)	pStep->ican_branch[7]		= m_ctrlCanGoto_8.GetItemData(m_ctrlCanGoto_8.GetCurSel());
	
		}
		else
		{
			pStep->fcan_Value[7] = 0;
			pStep->cCan_data_type[7]		= 0;
			pStep->cCan_compare_type[7]		= 0;
			pStep->ican_branch[7]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[8] > 0)
		{
			//m_ctrlCanCompare9.EnableWindow(TRUE);
			//m_ctrlCanGoto_9.EnableWindow(TRUE);
			pStep->fcan_Value[8] = m_fCanValue_9;
			if(m_ctrlCanDataType9.GetCurSel() != LB_ERR)	pStep->cCan_data_type[8]	= m_ctrlCanDataType9.GetItemData(m_ctrlCanDataType9.GetCurSel());
			if(m_ctrlCanCompare9.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[8] >= 1961) && (pStep->ican_function_division[8] <= 1968)) ||
					((pStep->ican_function_division[8] >= 1976) && (pStep->ican_function_division[8] <= 1977))||
					((pStep->ican_function_division[8] >= 1984) && (pStep->ican_function_division[8] <= 1985))||
					((pStep->ican_function_division[8] >= 1978) && (pStep->ican_function_division[8] <= 1979))||
					((pStep->ican_function_division[8] >= 1986) && (pStep->ican_function_division[8] <= 1987)))
					//if((pStep->ican_function_division[8] >= 1961) && (pStep->ican_function_division[8] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[8] = 3;
				}
				else
				{

						pStep->cCan_compare_type[8]	= m_ctrlCanCompare9.GetCurSel();
				}
			}
			if(m_ctrlCanGoto_9.GetCurSel() != LB_ERR)	pStep->ican_branch[8]		= m_ctrlCanGoto_9.GetItemData(m_ctrlCanGoto_9.GetCurSel());
		
		}
		else
		{
			pStep->fcan_Value[8] = 0;
			pStep->cCan_data_type[8]		= 0;
			pStep->cCan_compare_type[8]		= 0;
			pStep->ican_branch[8]		= PS_STEP_NEXT;
		}
		
		if (pStep->ican_function_division[9] > 0)
		{
		//	m_ctrlCanCompare10.EnableWindow(TRUE);
		//	m_ctrlCanGoto_10.EnableWindow(TRUE);
			pStep->fcan_Value[9] = m_fCanValue_10;
			if(m_ctrlCanDataType10.GetCurSel() != LB_ERR)	pStep->cCan_data_type[9]	= m_ctrlCanDataType10.GetItemData(m_ctrlCanDataType10.GetCurSel());
			if(m_ctrlCanCompare10.GetCurSel() != LB_ERR)
			{
				if(((pStep->ican_function_division[9] >= 1961) && (pStep->ican_function_division[9] <= 1968)) ||
					((pStep->ican_function_division[9] >= 1976) && (pStep->ican_function_division[9] <= 1977))||
					((pStep->ican_function_division[9] >= 1984) && (pStep->ican_function_division[9] <= 1985))||
					((pStep->ican_function_division[9] >= 1978) && (pStep->ican_function_division[9] <= 1979))||
					((pStep->ican_function_division[9] >= 1986) && (pStep->ican_function_division[9] <= 1987)))
					//if((pStep->ican_function_division[9] >= 1961) && (pStep->ican_function_division[9] <= 1968))//2Ch
				{
					pStep->cCan_compare_type[9] = 3;
				}
				else
				{

						pStep->cCan_compare_type[9]	= m_ctrlCanCompare10.GetCurSel();
				}
			}if(m_ctrlCanGoto_10.GetCurSel() != LB_ERR)	pStep->ican_branch[9]		= m_ctrlCanGoto_10.GetItemData(m_ctrlCanGoto_10.GetCurSel());
		
		}
		else
		{
			pStep->fcan_Value[9] = 0;
			pStep->cCan_data_type[9]		= 0;
			pStep->cCan_compare_type[9]		= 0;
			pStep->ican_branch[9]		= PS_STEP_NEXT;
		}
		//aux
		if (pStep->iaux_function_division[0] > 0)
		{
			//m_ctrlAuxCompare1.EnableWindow(TRUE);
			//m_ctrlAuxGoto_1.EnableWindow(TRUE);
			pStep->faux_Value[0] = m_fAuxValue_1;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_1) > 10)
				pStep->iaux_conti_time[0] = 10;
			else
				pStep->iaux_conti_time[0] = m_edit_aux_conti_time_1;
			
			if(m_ctrlAuxDataType1.GetCurSel() != LB_ERR)	pStep->cAux_data_type[0]	= m_ctrlAuxDataType1.GetItemData(m_ctrlAuxDataType1.GetCurSel());
			if(m_ctrlAuxCompare1.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[0] >= 2961) && (pStep->iaux_function_division[0] <= 2968)) ||
					((pStep->iaux_function_division[0] >= 2976) && (pStep->iaux_function_division[0] <= 2977))||
					((pStep->iaux_function_division[0] >= 2984) && (pStep->iaux_function_division[0] <= 2985))||
					((pStep->iaux_function_division[0] >= 2978) && (pStep->iaux_function_division[0] <= 2979))||
					((pStep->iaux_function_division[0] >= 2986) && (pStep->iaux_function_division[0] <= 2987)))
					//if((pStep->iaux_function_division[0] >= 2961) && (pStep->iaux_function_division[0] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[0] = 3;
				}
				else
				{
// 					if((pStep->cAux_compare_type[0] != 0) && (m_ctrlAuxCompare1.GetItemData(m_ctrlAuxCompare1.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cAux_compare_type[0]	= m_ctrlAuxCompare1.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_1.GetCurSel() != LB_ERR)	pStep->iaux_branch[0]		= m_ctrlAuxGoto_1.GetItemData(m_ctrlAuxGoto_1.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[0] = 0;
			pStep->iaux_conti_time[0] = 0;
			pStep->cAux_data_type[0]		= 0;
			pStep->cAux_compare_type[0]		= 0;
			pStep->iaux_branch[0]		= PS_STEP_NEXT;
		}

		if (pStep->iaux_function_division[1]> 0)
		{
			//m_ctrlAuxCompare2.EnableWindow(TRUE);
			//m_ctrlAuxGoto_2.EnableWindow(TRUE);
			pStep->faux_Value[1] = m_fAuxValue_2;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_2) > 10)
				pStep->iaux_conti_time[1] = 10;
			else
				pStep->iaux_conti_time[1] = m_edit_aux_conti_time_2;
			if(m_ctrlAuxDataType2.GetCurSel() != LB_ERR)	pStep->cAux_data_type[1]	= m_ctrlAuxDataType2.GetItemData(m_ctrlAuxDataType2.GetCurSel());
			if(m_ctrlAuxCompare2.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[1] >= 2961) && (pStep->iaux_function_division[1] <= 2968)) ||
					((pStep->iaux_function_division[1] >= 2976) && (pStep->iaux_function_division[1] <= 2977))||
					((pStep->iaux_function_division[1] >= 2984) && (pStep->iaux_function_division[1] <= 2985))||
					((pStep->iaux_function_division[1] >= 2978) && (pStep->iaux_function_division[1] <= 2979))||
					((pStep->iaux_function_division[1] >= 2986) && (pStep->iaux_function_division[1] <= 2987)))
					//if((pStep->iaux_function_division[1] >= 2961) && (pStep->iaux_function_division[1] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[1] = 3;
				}
				else
				{

						pStep->cAux_compare_type[1]	= m_ctrlAuxCompare2.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_2.GetCurSel() != LB_ERR)	pStep->iaux_branch[1]		= m_ctrlAuxGoto_2.GetItemData(m_ctrlAuxGoto_2.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[1] = 0;
			pStep->iaux_conti_time[1] = 0;
			pStep->cAux_data_type[1]		= 0;
			pStep->cAux_compare_type[1]		= 0;
			pStep->iaux_branch[1]		= PS_STEP_NEXT;
		}
		
		if (pStep->iaux_function_division[2] > 0)
		{
			//m_ctrlAuxCompare3.EnableWindow(TRUE);
			//m_ctrlAuxGoto_3.EnableWindow(TRUE);
			pStep->faux_Value[2] = m_fAuxValue_3;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_3) > 10)
				pStep->iaux_conti_time[2] = 10;
			else
				pStep->iaux_conti_time[2] = m_edit_aux_conti_time_3;
			if(m_ctrlAuxDataType3.GetCurSel() != LB_ERR)	pStep->cAux_data_type[2]	= m_ctrlAuxDataType3.GetItemData(m_ctrlAuxDataType3.GetCurSel());
			if(m_ctrlAuxCompare3.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[2] >= 2961) && (pStep->iaux_function_division[2] <= 2968)) ||
					((pStep->iaux_function_division[2] >= 2976) && (pStep->iaux_function_division[2] <= 2977))||
					((pStep->iaux_function_division[2] >= 2984) && (pStep->iaux_function_division[2] <= 2985))||
					((pStep->iaux_function_division[2] >= 2978) && (pStep->iaux_function_division[2] <= 2979))||
					((pStep->iaux_function_division[2] >= 2986) && (pStep->iaux_function_division[2] <= 2987)))
					//if((pStep->iaux_function_division[2] >= 2961) && (pStep->iaux_function_division[2] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[2] = 3;
				}
				else
				{

						pStep->cAux_compare_type[2]	= m_ctrlAuxCompare3.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_3.GetCurSel() != LB_ERR)	pStep->iaux_branch[2]		= m_ctrlAuxGoto_3.GetItemData(m_ctrlAuxGoto_3.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[2] = 0;
			pStep->iaux_conti_time[2] = 0;
			pStep->cAux_data_type[2]		= 0;
			pStep->cAux_compare_type[2]		= 0;
			pStep->iaux_branch[2]		= PS_STEP_NEXT;
		}
		
		if (pStep->iaux_function_division[3] > 0)
		{
			//m_ctrlAuxCompare4.EnableWindow(TRUE);
			//m_ctrlAuxGoto_4.EnableWindow(TRUE);
			pStep->faux_Value[3] = m_fAuxValue_4;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_4) > 10)
				pStep->iaux_conti_time[3] = 10;
			else
				pStep->iaux_conti_time[3] = m_edit_aux_conti_time_4;
			if(m_ctrlAuxDataType4.GetCurSel() != LB_ERR)	pStep->cAux_data_type[3]	= m_ctrlAuxDataType4.GetItemData(m_ctrlAuxDataType4.GetCurSel());
			if(m_ctrlAuxCompare4.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[3] >= 2961) && (pStep->iaux_function_division[3] <= 2968)) ||
					((pStep->iaux_function_division[3] >= 2976) && (pStep->iaux_function_division[3] <= 2977))||
					((pStep->iaux_function_division[3] >= 2984) && (pStep->iaux_function_division[3] <= 2985))||
					((pStep->iaux_function_division[3] >= 2978) && (pStep->iaux_function_division[3] <= 2979))||
					((pStep->iaux_function_division[3] >= 2986) && (pStep->iaux_function_division[3] <= 2987)))
					//if((pStep->iaux_function_division[3] >= 2961) && (pStep->iaux_function_division[3] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[3] = 3;
				}
				else
				{

						pStep->cAux_compare_type[3]	= m_ctrlAuxCompare4.GetCurSel();
				}
			}if(m_ctrlAuxGoto_4.GetCurSel() != LB_ERR)	pStep->iaux_branch[3]		= m_ctrlAuxGoto_4.GetItemData(m_ctrlAuxGoto_4.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[3] = 0;
			pStep->iaux_conti_time[3] = 0;
			pStep->cAux_data_type[3]		= 0;
			pStep->cAux_compare_type[3]		= 0;
			pStep->iaux_branch[3]		= PS_STEP_NEXT;
		}
		
		if (pStep->iaux_function_division[4] > 0)
		{
			//m_ctrlAuxCompare5.EnableWindow(TRUE);
			//m_ctrlAuxGoto_5.EnableWindow(TRUE);
			pStep->faux_Value[4] = m_fAuxValue_5;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_5) > 10)
				pStep->iaux_conti_time[4] = 10;
			else
				pStep->iaux_conti_time[4] = m_edit_aux_conti_time_5;
			if(m_ctrlAuxDataType5.GetCurSel() != LB_ERR)	pStep->cAux_data_type[4]	= m_ctrlAuxDataType5.GetItemData(m_ctrlAuxDataType5.GetCurSel());
			if(m_ctrlAuxCompare5.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[4] >= 2961) && (pStep->iaux_function_division[4] <= 2968)) ||
					((pStep->iaux_function_division[4] >= 2976) && (pStep->iaux_function_division[4] <= 2977))||
					((pStep->iaux_function_division[4] >= 2984) && (pStep->iaux_function_division[4] <= 2985))||
					((pStep->iaux_function_division[4] >= 2978) && (pStep->iaux_function_division[4] <= 2979))||
					((pStep->iaux_function_division[4] >= 2986) && (pStep->iaux_function_division[4] <= 2987)))
					//if((pStep->iaux_function_division[4] >= 2961) && (pStep->iaux_function_division[4] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[4] = 3;
				}
				else
				{

						pStep->cAux_compare_type[4]	= m_ctrlAuxCompare5.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_5.GetCurSel() != LB_ERR)	pStep->iaux_branch[4]		= m_ctrlAuxGoto_5.GetItemData(m_ctrlAuxGoto_5.GetCurSel());
	
		}
		else
		{
			pStep->faux_Value[4] = 0;
			pStep->iaux_conti_time[4] = 0;
			pStep->cAux_data_type[4]		= 0;
			pStep->cAux_compare_type[4]		= 0;
			pStep->iaux_branch[4]		= PS_STEP_NEXT;
		}
		
		if (pStep->iaux_function_division[5] > 0)
		{
			//m_ctrlAuxCompare6.EnableWindow(TRUE);
			//m_ctrlAuxGoto_6.EnableWindow(TRUE);
			pStep->faux_Value[5] = m_fAuxValue_6;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_6) > 10)
				pStep->iaux_conti_time[5] = 10;
			else
				pStep->iaux_conti_time[5] = m_edit_aux_conti_time_6;
			if(m_ctrlAuxDataType6.GetCurSel() != LB_ERR)	pStep->cAux_data_type[5]	= m_ctrlAuxDataType6.GetItemData(m_ctrlAuxDataType6.GetCurSel());
			if(m_ctrlAuxCompare6.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[5] >= 2961) && (pStep->iaux_function_division[5] <= 2968)) ||
					((pStep->iaux_function_division[5] >= 2976) && (pStep->iaux_function_division[5] <= 2977))||
					((pStep->iaux_function_division[5] >= 2984) && (pStep->iaux_function_division[5] <= 2985))||
					((pStep->iaux_function_division[5] >= 2978) && (pStep->iaux_function_division[5] <= 2979))||
					((pStep->iaux_function_division[5] >= 2986) && (pStep->iaux_function_division[5] <= 2987)))
					//if((pStep->iaux_function_division[5] >= 2961) && (pStep->iaux_function_division[5] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[5] = 3;
				}
				else
				{
// 					if((pStep->cAux_compare_type[5] != 0) && (m_ctrlAuxCompare6.GetItemData(m_ctrlAuxCompare6.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cAux_compare_type[5]	= m_ctrlAuxCompare6.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_6.GetCurSel() != LB_ERR)	pStep->iaux_branch[5]		= m_ctrlAuxGoto_6.GetItemData(m_ctrlAuxGoto_6.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[5] = 0;
			pStep->iaux_conti_time[5] = 0;
			pStep->cAux_data_type[5]		= 0;
			pStep->cAux_compare_type[5]		= 0;
			pStep->iaux_branch[5]		= PS_STEP_NEXT;
		}
		
		if (pStep->iaux_function_division[6] > 0)
		{
			//m_ctrlAuxCompare7.EnableWindow(TRUE);
			//m_ctrlAuxGoto_7.EnableWindow(TRUE);
			pStep->faux_Value[6] = m_fAuxValue_7;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_7) > 10)
				pStep->iaux_conti_time[6] = 10;
			else
				pStep->iaux_conti_time[6] = m_edit_aux_conti_time_7;
			if(m_ctrlAuxDataType7.GetCurSel() != LB_ERR)	pStep->cAux_data_type[6]	= m_ctrlAuxDataType7.GetItemData(m_ctrlAuxDataType7.GetCurSel());
			if(m_ctrlAuxCompare7.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[6] >= 2961) && (pStep->iaux_function_division[6] <= 2968)) ||
					((pStep->iaux_function_division[6] >= 2976) && (pStep->iaux_function_division[6] <= 2977))||
					((pStep->iaux_function_division[6] >= 2984) && (pStep->iaux_function_division[6] <= 2985))||
					((pStep->iaux_function_division[6] >= 2978) && (pStep->iaux_function_division[6] <= 2979))||
					((pStep->iaux_function_division[6] >= 2986) && (pStep->iaux_function_division[6] <= 2987)))
					//if((pStep->iaux_function_division[6] >= 2961) && (pStep->iaux_function_division[6] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[6] = 3;
				}
				else
				{

						pStep->cAux_compare_type[6]	= m_ctrlAuxCompare7.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_7.GetCurSel() != LB_ERR)	pStep->iaux_branch[6]		= m_ctrlAuxGoto_7.GetItemData(m_ctrlAuxGoto_7.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[6] = 0;
			pStep->iaux_conti_time[6] = 0;
			pStep->cAux_data_type[6]		= 0;
			pStep->cAux_compare_type[6]		= 0;
			pStep->iaux_branch[6]		= PS_STEP_NEXT;
		}
		
		if (pStep->iaux_function_division[7] > 0)
		{
			//m_ctrlAuxCompare8.EnableWindow(TRUE);
			//m_ctrlAuxGoto_8.EnableWindow(TRUE);
			pStep->faux_Value[7] = m_fAuxValue_8;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_8) > 10)
				pStep->iaux_conti_time[7] = 10;
			else
				pStep->iaux_conti_time[7] = m_edit_aux_conti_time_8;
			if(m_ctrlAuxDataType8.GetCurSel() != LB_ERR)	pStep->cAux_data_type[7]	= m_ctrlAuxDataType8.GetItemData(m_ctrlAuxDataType8.GetCurSel());
			if(m_ctrlAuxCompare8.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[7] >= 2961) && (pStep->iaux_function_division[7] <= 2968)) ||
					((pStep->iaux_function_division[7] >= 2976) && (pStep->iaux_function_division[7] <= 2977))||
					((pStep->iaux_function_division[7] >= 2984) && (pStep->iaux_function_division[7] <= 2985))||
					((pStep->iaux_function_division[7] >= 2978) && (pStep->iaux_function_division[7] <= 2979))||
					((pStep->iaux_function_division[7] >= 2986) && (pStep->iaux_function_division[7] <= 2987)))
					//if((pStep->iaux_function_division[7] >= 2961) && (pStep->iaux_function_division[7] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[7] = 3;
				}
				else
				{
// 					if((pStep->cAux_compare_type[7] != 0) && (m_ctrlAuxCompare8.GetItemData(m_ctrlAuxCompare8.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cAux_compare_type[7]	= m_ctrlAuxCompare8.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_8.GetCurSel() != LB_ERR)	pStep->iaux_branch[7]		= m_ctrlAuxGoto_8.GetItemData(m_ctrlAuxGoto_8.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[7] = 0;
			pStep->iaux_conti_time[7] = 0;
			pStep->cAux_data_type[7]		= 0;
			pStep->cAux_compare_type[7]		= 0;
			pStep->iaux_branch[7]		= PS_STEP_NEXT;
		}
		
		if (pStep->iaux_function_division[8] > 0)
		{
			//m_ctrlAuxCompare9.EnableWindow(TRUE);
			//m_ctrlAuxGoto_9.EnableWindow(TRUE);
			pStep->faux_Value[8] = m_fAuxValue_9;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_9) > 10)
				pStep->iaux_conti_time[8] = 10;
			else
				pStep->iaux_conti_time[8] = m_edit_aux_conti_time_9;
			if(m_ctrlAuxDataType9.GetCurSel() != LB_ERR)	pStep->cAux_data_type[8]	= m_ctrlAuxDataType9.GetItemData(m_ctrlAuxDataType9.GetCurSel());
			if(m_ctrlAuxCompare9.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[8] >= 2961) && (pStep->iaux_function_division[8] <= 2968)) ||
					((pStep->iaux_function_division[8] >= 2976) && (pStep->iaux_function_division[8] <= 2977))||
					((pStep->iaux_function_division[8] >= 2984) && (pStep->iaux_function_division[8] <= 2985))||
					((pStep->iaux_function_division[8] >= 2978) && (pStep->iaux_function_division[8] <= 2979))||
					((pStep->iaux_function_division[8] >= 2986) && (pStep->iaux_function_division[8] <= 2987)))
					//if((pStep->iaux_function_division[8] >= 2961) && (pStep->iaux_function_division[8] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[8] = 3;
				}
				else
				{
// 					if((pStep->cAux_compare_type[8] != 0) && (m_ctrlAuxCompare9.GetItemData(m_ctrlAuxCompare9.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cAux_compare_type[8]	= m_ctrlAuxCompare9.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_9.GetCurSel() != LB_ERR)	pStep->iaux_branch[8]		= m_ctrlAuxGoto_9.GetItemData(m_ctrlAuxGoto_9.GetCurSel());
		
		}
		else
		{
			pStep->faux_Value[8] = 0;
			pStep->iaux_conti_time[8] = 0;
			pStep->cAux_data_type[8]		= 0;
			pStep->cAux_compare_type[8]		= 0;
			pStep->iaux_branch[8]		= PS_STEP_NEXT;
		}

		if (pStep->iaux_function_division[9] > 0)
		{
			//m_ctrlAuxCompare10.EnableWindow(TRUE);
			//m_ctrlAuxGoto_10.EnableWindow(TRUE);
			pStep->faux_Value[9] = m_fAuxValue_10;
			if(GetDlgItemInt(IDC_EDIT_AUX_CONTI_TIME_10) > 10)
				pStep->iaux_conti_time[9] = 10;
			else
				pStep->iaux_conti_time[9] = m_edit_aux_conti_time_10;
			if(m_ctrlAuxDataType10.GetCurSel() != LB_ERR)	pStep->cAux_data_type[9]	= m_ctrlAuxDataType10.GetItemData(m_ctrlAuxDataType10.GetCurSel());
			if(m_ctrlAuxCompare10.GetCurSel() != LB_ERR)
			{
				if(((pStep->iaux_function_division[9] >= 2961) && (pStep->iaux_function_division[9] <= 2968)) ||
					((pStep->iaux_function_division[9] >= 2976) && (pStep->iaux_function_division[9] <= 2977))||
					((pStep->iaux_function_division[9] >= 2984) && (pStep->iaux_function_division[9] <= 2985))||
					((pStep->iaux_function_division[9] >= 2978) && (pStep->iaux_function_division[9] <= 2979))||
					((pStep->iaux_function_division[9] >= 2986) && (pStep->iaux_function_division[9] <= 2987)))
					//if((pStep->iaux_function_division[9] >= 2961) && (pStep->iaux_function_division[9] <= 2968))//2Ch
				{
					pStep->cAux_compare_type[9] = 3;
				}
				else
				{
// 					if((pStep->cAux_compare_type[9] != 0) && (m_ctrlAuxCompare10.GetItemData(m_ctrlAuxCompare10.GetCurSel()) == 0))
// 					{
// 						
// 					}
// 					else
						pStep->cAux_compare_type[9]	= m_ctrlAuxCompare10.GetCurSel();
				}
			}
			if(m_ctrlAuxGoto_10.GetCurSel() != LB_ERR)	pStep->iaux_branch[9]		= m_ctrlAuxGoto_10.GetItemData(m_ctrlAuxGoto_10.GetCurSel());
			
		}
		else
		{
			pStep->faux_Value[9] = 0;
			pStep->iaux_conti_time[9] = 0;
			pStep->cAux_data_type[9]		= 0;
			pStep->cAux_compare_type[9]		= 0;
			pStep->iaux_branch[9]		= PS_STEP_NEXT;
		}

// 		int iLoop=0;
// 		for (iLoop=0; iLoop < 10; iLoop++)
// 		{
// 			if (pStep->ican_function_division[iLoop] != 0 && (pStep->cCan_compare_type[iLoop] == 0 || pStep->cCan_data_type[iLoop] == 0 ))
// 			{
// 				AfxMessageBox("CAN 조건 설정 에서 Data Type 또는 비교 Type 을 설정을 하세요.");
// 				return FALSE;
// 			}
// 			if (pStep->iaux_function_division[iLoop] != 0 && (pStep->cAux_compare_type[iLoop] == 0 || pStep->cAux_data_type[iLoop] == 0 ))
// 			{
// 				AfxMessageBox("외부데이터 조건 설정 에서 Data Type 또는 비교 Type 을 설정을 하세요.");
// 				return FALSE;
// 			}
// 		}
// 
// 		for (iLoop=0; iLoop < 10; iLoop++)
// 		{
// 			if (pStep->ican_function_division[iLoop] != 0 && (pStep->ican_function_division[iLoop] < 1001 || pStep->ican_function_division[iLoop] > 2000 ))
// 			{
// 				AfxMessageBox("CAN 조건 설정에서 분류 코드는 1001 ~ 2000 값이 여야 합니다.");
// 				return FALSE;
// 			}
// 			if (pStep->iaux_function_division[iLoop] != 0 && (pStep->iaux_function_division[iLoop] < 2001 || pStep->iaux_function_division[iLoop] > 3000 ))
// 			{
// 				AfxMessageBox("외부데이터 조건에서 분류 코드는 2001 ~ 3000 값이 여야 합니다.");
// 				return FALSE;
// 			}
// 		}

//////////////////////////////////////////////////////////////////////////
		if (pStep->chType != PS_STEP_ADV_CYCLE)
		{
			//Item 선택값 과 Item 기준 스텝,  Value 값 %
			//ljb 2009-03-23 Value Item 설정 pStep->bUseActualCapa 삭제//////////////////////////
			if(m_ctrlCboValueItem.GetCurSel() != LB_ERR)
				pStep->nValueRateItem = m_ctrlCboValueItem.GetItemData(m_ctrlCboValueItem.GetCurSel());
			else 
				pStep->nValueRateItem = 0;
			if(m_ctrlCboValueStepNo.GetCurSel() != LB_ERR)
				pStep->nValueStepNo = m_ctrlCboValueStepNo.GetItemData(m_ctrlCboValueStepNo.GetCurSel());
			else 
				pStep->nValueStepNo = 0;
			pStep->fValueRate = m_ctrlEditfValueRate;

			//  [4/22/2015 이재복]
			if(m_ctrlCboLoaderValueItem.GetCurSel() != LB_ERR)
				pStep->nValueLoaderItem = m_ctrlCboLoaderValueItem.GetItemData(m_ctrlCboLoaderValueItem.GetCurSel());
			else 
				pStep->nValueLoaderItem = 0;
			if(m_ctrlCboLoaderMode.GetCurSel() != LB_ERR)
				pStep->nValueLoaderMode = m_ctrlCboLoaderMode.GetItemData(m_ctrlCboLoaderMode.GetCurSel());
			else 
				pStep->nValueLoaderMode = 0;
			pStep->fValueLoaderSet = m_ctrlEditfValueLoader;

		}
		else
		{
			pStep->nValueLoaderItem = 0;
			pStep->nValueLoaderMode = 0;
			pStep->fValueLoaderSet = 0.0f;

			if(m_ctrlCboAddCap.GetCurSel() != LB_ERR)
				//pStep->nValueRateItem = m_ctrlCboAddCap.GetItemData(m_ctrlCboAddCap.GetCurSel());
				pStep->nValueRateItem = m_ctrlCboAddCap.GetCurSel();
			else 
				pStep->nValueRateItem = 0;
			if(m_ctrlCboAddWattHour.GetCurSel() != LB_ERR)
				pStep->nValueStepNo = m_ctrlCboAddWattHour.GetCurSel();
			else 
				pStep->nValueStepNo = 0;
		}
		if(pStep->chType == PS_STEP_CHARGE || pStep->chType == PS_STEP_DISCHARGE)
		{
			STEP *pStep2 = (STEP*)m_pDoc->m_apStep[m_nStepNo];
			
			//lmh 다음스텝과 CP_CC모드일경우 현재스텝 시간수정시 다음수텝시간도 자동수정
			if(pStep2->bUseLinkStep)
			{
				pStep2->ulEndTimeDay = pStep->ulEndTimeDay;
				pStep2->ulEndTime = pStep->ulEndTime;
				pStep2->ulReportTime = pStep->ulReportTime;
				bChkNextStep = true;
			}
		}
		pStep->bUseCyclePause = m_bUseCyclePause;		//ljb 201010
		pStep->bUseLinkStep = m_bUseLinkStep;			//ljb 201010
		pStep->bUseChamberProg = m_bUseChamberProg;		//ljb 201010
		pStep->bUseChamberStop = m_bChamberStepStop;    //yulee 20180829
		//2014.12.08
		if(IsDlgButtonChecked(IDC_CHK_NOCHECK_PROG_MODE)){
			pStep->nNoCheckMode = 1;
		}else{
			pStep->nNoCheckMode = 0;
		}
		
		if(IsDlgButtonChecked(IDC_CHK_CANTXOFF_PROG_MODE)){
			pStep->nCanTxOffMode = 1;
		}else{
			pStep->nCanTxOffMode = 0;
		}

		//2018.08.28
		if(IsDlgButtonChecked(IDC_CHK_CHAMBER_STEP_STOP)){ //yulee 20180828
			pStep->bUseChamberStop = 1;
		}else{
			pStep->bUseChamberStop = 0;
		}
		

		//20150825 add
// 		if(IsDlgButtonChecked(IDC_CHK_CAN_MODE)){
// 			pStep->nCanCheckMode = 1;
// 		}else{
// 			pStep->nCanCheckMode = 0;
// 		}

		//20170328 add
		if(m_ctrlCboCanCheck.GetCurSel() != LB_ERR)
		{
			pStep->nCanCheckMode = m_ctrlCboCanCheck.GetItemData(m_ctrlCboCanCheck.GetCurSel());
			TRACE("%d \n",pStep->nCanCheckMode);
		}
		
		UpdateData(FALSE);

	}

	if(m_pParent)
		m_pParent->PostMessage(WM_END_DLG_CLOSE, (WPARAM)m_nStepNo, (LPARAM) this);
	if(bChkNextStep)
		m_pParent->PostMessage(WM_END_DLG_CLOSE, (WPARAM)m_nStepNo+1, (LPARAM) this);

	return TRUE;

}

BOOL CEndConditionDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
// 	CWnd * pItem;
// 	pItem = GetDlgItem(IDC_CBO_CAN_DIVISION_1);
// 	if(GetFocus() == pItem->GetFocus())
// 	{
// 		if( pMsg->message == WM_KEYDOWN )
// 		{
// 			if (pMsg->wParam < VK_NUMPAD0 || pMsg->wParam > VK_NUMPAD9) return FALSE;
// 		}
// 
// 	}
	m_tooltip.RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}

void CEndConditionDlg::InitToolTips()
{
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);

	m_tooltip.AddTool(GetDlgItem(IDC_END_VTG_H), Fun_FindMsg("OnShowWindow_msg8","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_END_VTG_L), Fun_FindMsg("OnShowWindow_msg9","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_END_CRT), Fun_FindMsg("OnShowWindow_msg10","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_END_DAY_EDIT), Fun_FindMsg("OnShowWindow_msg11","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_DATETIMEPICKER1), Fun_FindMsg("OnShowWindow_msg12","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_END_CAP), Fun_FindMsg("OnShowWindow_msg13","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_EDIT_CYCLE), Fun_FindMsg("OnShowWindow_msg14","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_CBO_MULTI_GROUP), Fun_FindMsg("OnShowWindow_msg15","IDD_END_COND_DLG"));
//	m_tooltip.AddTool(GetDlgItem(IDC_GOTO_COMBO), Fun_FindMsg("OnShowWindow_msg16"));
//	m_tooltip.AddTool(GetDlgItem(IDC_EDIT_GOTO), "Cycle 반복을 완료후 입력된 Step을 이동(다른 Cycle을 시작이나 완료 Step을 입력)");
	m_tooltip.AddTool(GetDlgItem(IDC_END_DELTAV), Fun_FindMsg("OnShowWindow_msg17","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_END_WATT), Fun_FindMsg("OnShowWindow_msg18","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_CBO_VALUE_STEP_NO), Fun_FindMsg("OnShowWindow_msg19","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_EDIT_VALUE_RATE), Fun_FindMsg("OnShowWindow_msg20","IDD_END_COND_DLG"));
	m_tooltip.AddTool(GetDlgItem(IDC_CHECK_ZERO_VOLTAGE), "Check:0V종료조건 가능//Uncheck:0의 종료조건 무시");  //yulee 20191017 OverChargeDischarger Mark
}

//Value로 사용할 항목과 Step List 업데이트.. Value Goto 활성화
int CEndConditionDlg::ListUpValueStepCombo()
{
	STEP *pStep; 
	CString strTemp;
	int nCount = 0;
	m_ctrlCboValueStepNo.ResetContent();

	//선택 가능한 List를 추가 한다.
	for(int i=0; i<m_pDoc->GetTotalStepNum(); i++)		//현재 step은 제외 하기 위해 -1한다.
	{
		if (i == m_nStepNo-1) continue;
		pStep = (STEP*)m_pDoc->m_apStep[i];
		ASSERT(pStep);
		switch (pStep->chType)
		{
		case PS_STEP_CHARGE:
		case PS_STEP_DISCHARGE:
		case PS_STEP_PATTERN:
		case PS_STEP_EXT_CAN:		//ljb 2011318 이재복 //////////
			strTemp.Format("Step %d", i+1);
			m_ctrlCboValueStepNo.AddString(strTemp);
			m_ctrlCboValueStepNo.SetItemData(nCount++, i+1);
			break;
		}
	}

	//현재 Step의 설정된 값을 표시 한다.
	pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	for(int j=0; j<m_ctrlCboValueStepNo.GetCount(); j++)
	{
		if(m_ctrlCboValueStepNo.GetItemData(j) == pStep->nValueStepNo)
		{
			m_ctrlCboValueStepNo.SetCurSel(j);
			break;
		}
	}
	//Value Item 설정
	if (pStep->nValueRateItem > 0)
	{
		GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(TRUE);
		GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(TRUE); 
	}
	if (nCount == 0 )
	{
		//m_ctrlCboValueStepNo.AddString("사용안함");
		m_ctrlCboValueStepNo.AddString(Fun_FindMsg("EndConditionDlg_ListUpValueStepCombo_msg1","IDD_END_COND_DLG"));//&&
		m_ctrlCboValueStepNo.SetItemData(0, 0);
		m_ctrlCboValueStepNo.SetCurSel(0);
	}

	return nCount;
}

int CEndConditionDlg::ListUpGotoStepCombo()
{
	m_ctrlGoto.ResetContent();
	m_ctrlGoto.AddString("NEXT");
	m_ctrlGoto.SetItemData(0, PS_STEP_NEXT);

	STEP *pCurStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];

	int nDefaultIndex = 0;

	STEP* pStep;
	CString strTemp;
	int nCount = 1;
	BOOL bEndStep = FALSE;

	//선택 가능한 List를 추가 한다.
	for(int i=0; i<m_pDoc->GetTotalStepNum(); i++)		//현재 step은 제외 하기 위해 -1한다.
	{
		pStep = (STEP*)m_pDoc->m_apStep[i];
		ASSERT(pStep);

		if(pStep->chType == PS_STEP_ADV_CYCLE )
		{
			if(pCurStep->nLoopInfoGotoStep == i+1)
			{
				nDefaultIndex = nCount;
			}

			strTemp.Format("Step %2d", i+1);
			m_ctrlGoto.AddString(strTemp);
			m_ctrlGoto.SetItemData(nCount++, i+1);	
			
		}
		else if(pStep->chType == PS_STEP_END)
		{
			if(pCurStep->nLoopInfoGotoStep == i+1)
			{
				nDefaultIndex = nCount;
			}

			strTemp.Format("END");
			m_ctrlGoto.AddString(strTemp);
			m_ctrlGoto.SetItemData(nCount++, i+1);	
		}
	}

	m_ctrlGoto.SetCurSel(nDefaultIndex);
//	OnSelchangeGotoStepCombo();
	return 0;
}

void CEndConditionDlg::OnChangeEndDayEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	UpdateData();

	//long으로 표현 가능한 날짜 (sec×100 된 숫자)
	//21474836.47sec => 248.55 Day 
	//최대 입력범위를 199일 23:59:59 으로 제한 
//	if(m_nEndDay > 199)	m_nEndDay = 199;
	if(m_nEndDay > 1095)	m_nEndDay = 1095;	//ljb 20131212 add 3년으로 제한 365 * 3 = 1095
	
	UpdateData(FALSE);

	COleDateTime endTime;
	ULONG ulTemp=0;
	m_ctrlEndTime.GetTime(endTime);
	//ulTemp = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() + m_nEndDay * 86400)*100;		
	ulTemp = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond())*100;		
	ulTemp += (m_lEndMSec/10);
	if (ulTemp > 0 || m_nEndDay > 0) m_ctrlCboEndTime.EnableWindow(TRUE);
	else m_ctrlCboEndTime.EnableWindow(FALSE);

}

void CEndConditionDlg::OnDeltaposEndTimeMsecSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	UpdateData();
	CString strData;
	GetDlgItem(IDC_END_MSEC_EDIT)->GetWindowText(strData);

	int mtime = atol(strData);

	//ksj 20180203 : 주석 처리
	//end time은 0msec 부터 설정 가능
	/*
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if(mtime >= 1000)	mtime = 0;
	}*/

	//ksj 20180223 : 사용자 종료 조건 ms 단위 옵션 처리
	if(pNMUpDown->iDelta > 0)
	{
// 		mtime -= MIN_TIME_INTERVAL;
// 		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);

		//yulee 20180413 스텝별 종료 조건 설정 (패턴 그리고 나머지)
		STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
			ASSERT(pStep);

		if(pStep->chType == PS_STEP_PATTERN)
		{
			if(m_nCustomMsecPattEnd !=0)
			{
				mtime -= m_nCustomMsecPattEnd;
				if(mtime < 0)	mtime = (1000-m_nCustomMsecPattEnd);	
			}
			else
			{
				mtime -= MIN_TIME_INTERVAL;
				if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);
			}
		}
		else
		{
			if(m_nCustomMsecStepEnd !=0)
			{
				mtime -= m_nCustomMsecStepEnd;
				if(mtime < 0)	mtime = (1000-m_nCustomMsecStepEnd);	
			}
			else
			{
				mtime -= MIN_TIME_INTERVAL;
				if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);
			}
		}

		
	
		/* yulee 20180413 주석처리
		if(m_nCustomMsec)
		{
			mtime -= m_nCustomMsec;
			if(mtime < 0)	mtime = (1000-m_nCustomMsec);			
		}
		else
		{
			mtime -= MIN_TIME_INTERVAL;
			if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);			
		}
		*/
	}
	else
	{
// 		mtime += MIN_TIME_INTERVAL;
// 		if(mtime >= 1000)	mtime = 0;

		
		//yulee 20180413 스텝별 종료 조건 설정 (패턴 그리고 나머지)
		STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
		ASSERT(pStep);
		
		if(pStep->chType == PS_STEP_PATTERN)
		{
			if(m_nCustomMsecPattEnd !=0)
			{
				mtime += m_nCustomMsecPattEnd;	
			}
			else
			{
				mtime += MIN_TIME_INTERVAL;
			}
		}
		else
		{
			if(m_nCustomMsecStepEnd !=0)
			{
				mtime += m_nCustomMsecStepEnd;	
			}
			else
			{
				mtime += MIN_TIME_INTERVAL;
			}
		}

		/* yulee 20180413 주석처리
		//ksj 20171031 : 사용자 입력 값 추가.
		if(m_nCustomMsec)
			mtime += m_nCustomMsec;
		else
			mtime += MIN_TIME_INTERVAL;
		//ksj end
		*/
	
		if(mtime >= 1000)	mtime = 0;
	}



	m_lEndMSec = mtime;	//ljb 20110125

// 	if ( mtime > 0 ) m_ctrlCboEndTime.EnableWindow(TRUE);
// 	else m_ctrlCboEndCVTime.EnableWindow(FALSE);

	strData.Format("%03d", mtime);
	GetDlgItem(IDC_END_MSEC_EDIT)->SetWindowText(strData);	
	
	COleDateTime endTime;
	ULONG ulTemp=0;
	m_ctrlEndTime.GetTime(endTime);
	//ulTemp = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() + m_nEndDay * 86400)*100;		
	ulTemp = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond())*100;		
	ulTemp += (m_lEndMSec/10);
	if (ulTemp > 0 || m_nEndDay > 0) m_ctrlCboEndTime.EnableWindow(TRUE);
	else m_ctrlCboEndTime.EnableWindow(FALSE);
	*pResult = 0;	

	UpdateData(FALSE);
}

void CEndConditionDlg::DisplayData()
{
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	ASSERT(pStep);

	STEP *pPreStep;
	pPreStep = NULL;
	if (m_nStepNo > 2) pPreStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-2];

	m_EndV_H.SetValue((double)m_pDoc->VUnitTrans(pStep->fEndV_H, FALSE));
	m_EndV_L.SetValue((double)m_pDoc->VUnitTrans(pStep->fEndV_L, FALSE));	//v1009 추가
	
	m_bZeroVoltage.SetCheck(0); //yulee 20190904 //yulee 20191017 OverChargeDischarger Mark
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "ZeroVoltEndCond", m_bZeroVoltage.GetCheck()); //yulee 20191017 OverChargeDischarger Mark

	m_EndI.SetValue((double)m_pDoc->IUnitTrans(pStep->fEndI, FALSE));	
	m_EndC.SetValue((double)m_pDoc->CUnitTrans(pStep->fEndC, FALSE));
	m_EnddV.SetValue((double)m_pDoc->VUnitTrans(pStep->fEndDV, FALSE));	
	m_EndWatt.SetValue((double)m_pDoc->WattUnitTrans(pStep->fEndW, FALSE));	
	m_EndWattHour.SetValue((double)m_pDoc->WattHourUnitTrans(pStep->fEndWh, FALSE));
	m_EndTemp.SetValue((double)pStep->fEndTemp);
	
	COleDateTime endTime;
	ULONG lSecond = pStep->ulEndTime / 100;
	m_lEndMSec = (pStep->ulEndTime % 100)*10;
	//m_nEndDay = lSecond / 86400;
	m_nEndDay = pStep->ulEndTimeDay;	//ljb 23131212 add
	int nHour = lSecond;// % 86400;
	endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
	m_ctrlEndTime.SetTime(endTime);

	COleDateTime CVTime;
	lSecond = pStep->ulCVTime / 100;
	m_lCVTimeMSec = (pStep->ulCVTime % 100)*10;
	//m_nCVTimeDay = lSecond / 86400;
	m_nCVTimeDay = pStep->ulCVTimeDay;	//ljb 23131212 add
	nHour = lSecond;// % 86400;
	CVTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
	m_ctrlCVTime.SetTime(CVTime);


 	COleDateTime waitTime;
 	waitTime.SetTime(pStep->nWaitTimeHour, pStep->nWaitTimeMin, pStep->nWaitTimeSec);
	m_bUseWaitTimeInit = pStep->nWaitTimeInit;
	m_nWaitTimeDay = pStep->nWaitTimeDay;
	m_ctrlWaitTime.SetTime(waitTime);

	//콤보 박스 활성화 //////////////////////////////////////////////////////
	if (pStep->fEndV_H > 0) m_ctrlCboEndVtg_H.EnableWindow(TRUE);
	if (pStep->fEndV_L > 0) m_ctrlCboEndVtg_L.EnableWindow(TRUE);
	if (pStep->fEndC != 0) m_ctrlCboEndCapa.EnableWindow(TRUE);
	if (pStep->fEndWh != 0) m_ctrlCboEndWattHour.EnableWindow(TRUE);
	if (pStep->ulEndTime > 0) m_ctrlCboEndTime.EnableWindow(TRUE);
	if (pStep->ulCVTime > 0) m_ctrlCboEndCVTime.EnableWindow(TRUE);
//	if (pStep->nLoopInfoGotoStep > 0) m_ctrlGoto.EnableWindow(TRUE);
	//////////////////////////////////////////////////////////////////////////


	m_ctrlEditLoopInfoCycle = pStep->nLoopInfoCycle;
	m_nLoopInfoGoto = pStep->nLoopInfoGotoStep;
	//Combo 에서 m_LoopInfoGoto 찾아서 표시 한다.		추가해야 함
//..	m_ctrlEditLoopInfoGotoCnt = pStep->nLoopInfoGotoCnt;

	m_nMultiLoopInfoCycle	= pStep->nMultiLoopInfoCycle;
	m_nMultiLoopInfoGoto	= pStep->nMultiLoopInfoGotoStep;
	m_nMultiLoopGroupID = m_ctrlCboLoopMultiGroupID.SetCurSel(pStep->nMultiLoopGroupID);

	m_nAccLoopInfoCycle	= pStep->nAccLoopInfoCycle;
	m_nAccLoopInfoGoto	= pStep->nAccLoopInfoGotoStep;
	m_nAccLoopGroupID = m_ctrlCboLoopAccGroupID.SetCurSel(pStep->nAccLoopGroupID);

//	CString strDivision;
//	m_nCanDivision_1 = pStep->ican_function_division[0];

//ljb 2011221 이재복 //////////
	Fun_FindCanAuxName(ID_CAN_NAME,1, pStep->ican_function_division[0]);
	Fun_FindCanAuxName(ID_CAN_NAME,2, pStep->ican_function_division[1]);
	Fun_FindCanAuxName(ID_CAN_NAME,3, pStep->ican_function_division[2]);
	Fun_FindCanAuxName(ID_CAN_NAME,4, pStep->ican_function_division[3]);
	Fun_FindCanAuxName(ID_CAN_NAME,5, pStep->ican_function_division[4]);
	Fun_FindCanAuxName(ID_CAN_NAME,6, pStep->ican_function_division[5]);
	Fun_FindCanAuxName(ID_CAN_NAME,7, pStep->ican_function_division[6]);
	Fun_FindCanAuxName(ID_CAN_NAME,8, pStep->ican_function_division[7]);
	Fun_FindCanAuxName(ID_CAN_NAME,9, pStep->ican_function_division[8]);
	Fun_FindCanAuxName(ID_CAN_NAME,10, pStep->ican_function_division[9]);

	m_fCanValue_1 = pStep->fcan_Value[0];
	m_fCanValue_2 = pStep->fcan_Value[1];
	m_fCanValue_3 = pStep->fcan_Value[2];
	m_fCanValue_4 = pStep->fcan_Value[3];
	m_fCanValue_5 = pStep->fcan_Value[4];
	m_fCanValue_6 = pStep->fcan_Value[5];
	m_fCanValue_7 = pStep->fcan_Value[6];
	m_fCanValue_8 = pStep->fcan_Value[7];
	m_fCanValue_9 = pStep->fcan_Value[8];
	m_fCanValue_10 = pStep->fcan_Value[9];

	//ljb 임시
	//ljb 2011221 이재복 //////////
	Fun_FindCanAuxName(ID_AUX_NAME,1, pStep->iaux_function_division[0]);
	Fun_FindCanAuxName(ID_AUX_NAME,2, pStep->iaux_function_division[1]);
	Fun_FindCanAuxName(ID_AUX_NAME,3, pStep->iaux_function_division[2]);
	Fun_FindCanAuxName(ID_AUX_NAME,4, pStep->iaux_function_division[3]);
	Fun_FindCanAuxName(ID_AUX_NAME,5, pStep->iaux_function_division[4]);
	Fun_FindCanAuxName(ID_AUX_NAME,6, pStep->iaux_function_division[5]);
	Fun_FindCanAuxName(ID_AUX_NAME,7, pStep->iaux_function_division[6]);
	Fun_FindCanAuxName(ID_AUX_NAME,8, pStep->iaux_function_division[7]);
	Fun_FindCanAuxName(ID_AUX_NAME,9, pStep->iaux_function_division[8]);
	Fun_FindCanAuxName(ID_AUX_NAME,10, pStep->iaux_function_division[9]);

	
	
	m_fAuxValue_1 = pStep->faux_Value[0];
	m_fAuxValue_2 = pStep->faux_Value[1];
	m_fAuxValue_3 = pStep->faux_Value[2];
	m_fAuxValue_4 = pStep->faux_Value[3];
	m_fAuxValue_5 = pStep->faux_Value[4];
	m_fAuxValue_6 = pStep->faux_Value[5];
	m_fAuxValue_7 = pStep->faux_Value[6];
	m_fAuxValue_8 = pStep->faux_Value[7];
	m_fAuxValue_9 = pStep->faux_Value[8];
	m_fAuxValue_10 = pStep->faux_Value[9];

	m_edit_aux_conti_time_1 = pStep->iaux_conti_time[0];
	m_edit_aux_conti_time_2 = pStep->iaux_conti_time[1];
	m_edit_aux_conti_time_3 = pStep->iaux_conti_time[2];
	m_edit_aux_conti_time_4 = pStep->iaux_conti_time[3];
	m_edit_aux_conti_time_5 = pStep->iaux_conti_time[4];
	m_edit_aux_conti_time_6 = pStep->iaux_conti_time[5];
	m_edit_aux_conti_time_7 = pStep->iaux_conti_time[6];
	m_edit_aux_conti_time_8 = pStep->iaux_conti_time[7];
	m_edit_aux_conti_time_9 = pStep->iaux_conti_time[8];
	m_edit_aux_conti_time_10 = pStep->iaux_conti_time[9];

	//ljb 20100820
	m_ctrlCanCompare1.SetCurSel(pStep->cCan_compare_type[0]);
	m_ctrlCanCompare2.SetCurSel(pStep->cCan_compare_type[1]);
	m_ctrlCanCompare3.SetCurSel(pStep->cCan_compare_type[2]);
	m_ctrlCanCompare4.SetCurSel(pStep->cCan_compare_type[3]);
	m_ctrlCanCompare5.SetCurSel(pStep->cCan_compare_type[4]);
	m_ctrlCanCompare6.SetCurSel(pStep->cCan_compare_type[5]);
	m_ctrlCanCompare7.SetCurSel(pStep->cCan_compare_type[6]);
	m_ctrlCanCompare8.SetCurSel(pStep->cCan_compare_type[7]);
	m_ctrlCanCompare9.SetCurSel(pStep->cCan_compare_type[8]);
	m_ctrlCanCompare10.SetCurSel(pStep->cCan_compare_type[9]);

	m_ctrlAuxCompare1.SetCurSel(pStep->cAux_compare_type[0]);
	m_ctrlAuxCompare2.SetCurSel(pStep->cAux_compare_type[1]);
	m_ctrlAuxCompare3.SetCurSel(pStep->cAux_compare_type[2]);
	m_ctrlAuxCompare4.SetCurSel(pStep->cAux_compare_type[3]);
	m_ctrlAuxCompare5.SetCurSel(pStep->cAux_compare_type[4]);
	m_ctrlAuxCompare6.SetCurSel(pStep->cAux_compare_type[5]);
	m_ctrlAuxCompare7.SetCurSel(pStep->cAux_compare_type[6]);
	m_ctrlAuxCompare8.SetCurSel(pStep->cAux_compare_type[7]);
	m_ctrlAuxCompare9.SetCurSel(pStep->cAux_compare_type[8]);
	m_ctrlAuxCompare10.SetCurSel(pStep->cAux_compare_type[9]);

// data type
	m_ctrlCanDataType1.SetCurSel(pStep->cCan_data_type[0]);
	m_ctrlCanDataType2.SetCurSel(pStep->cCan_data_type[1]);
	m_ctrlCanDataType3.SetCurSel(pStep->cCan_data_type[2]);
	m_ctrlCanDataType4.SetCurSel(pStep->cCan_data_type[3]);
	m_ctrlCanDataType5.SetCurSel(pStep->cCan_data_type[4]);
	m_ctrlCanDataType6.SetCurSel(pStep->cCan_data_type[5]);
	m_ctrlCanDataType7.SetCurSel(pStep->cCan_data_type[6]);
	m_ctrlCanDataType8.SetCurSel(pStep->cCan_data_type[7]);
	m_ctrlCanDataType9.SetCurSel(pStep->cCan_data_type[8]);
	m_ctrlCanDataType10.SetCurSel(pStep->cCan_data_type[9]);
	
	m_ctrlAuxDataType1.SetCurSel(pStep->cAux_data_type[0]);
	m_ctrlAuxDataType2.SetCurSel(pStep->cAux_data_type[1]);
	m_ctrlAuxDataType3.SetCurSel(pStep->cAux_data_type[2]);
	m_ctrlAuxDataType4.SetCurSel(pStep->cAux_data_type[3]);
	m_ctrlAuxDataType5.SetCurSel(pStep->cAux_data_type[4]);
	m_ctrlAuxDataType6.SetCurSel(pStep->cAux_data_type[5]);
	m_ctrlAuxDataType7.SetCurSel(pStep->cAux_data_type[6]);
	m_ctrlAuxDataType8.SetCurSel(pStep->cAux_data_type[7]);
	m_ctrlAuxDataType9.SetCurSel(pStep->cAux_data_type[8]);
	m_ctrlAuxDataType10.SetCurSel(pStep->cAux_data_type[9]);

	//////////////////////////////////////////////////////////////////////////
	//ljb 2011210 이재복 S ////////////yulee 20181009-2
	//ljb 임시	
	/*if (pStep->ican_function_division[0] == 0)
		bEnableFlag = FALSE;
	else
		bEnableFlag = TRUE;
	GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bEnableFlag);
	m_ctrlCanDataType1.EnableWindow(bEnableFlag);
	m_ctrlCanCompare1.EnableWindow(bEnableFlag);
	m_ctrlCanGoto_1.EnableWindow(bEnableFlag);*/

				int iCode;
				BOOL bFlag;
				CString strCodeName; //yulee 20180830 
				if(m_ctrlCanDivision_1.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[0];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare1.SetItemData(pStep->cCan_compare_type[0],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
							m_ctrlCanDataType1.EnableWindow(bFlag);
							m_ctrlCanCompare1.EnableWindow(bFlag);
							m_ctrlCanGoto_1.EnableWindow(FALSE);	
							
							m_ctrlCanCompare1.SetItemData(pStep->cCan_data_type[0],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
							m_ctrlCanDataType1.EnableWindow(bFlag);
							m_ctrlCanCompare1.EnableWindow(bFlag);
							m_ctrlCanGoto_1.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
							m_ctrlCanDataType1.EnableWindow(FALSE);
							m_ctrlCanCompare1.EnableWindow(FALSE);
							m_ctrlCanGoto_1.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag); //yulee 20181107
							m_ctrlCanDataType1.EnableWindow(bFlag);
							m_ctrlCanCompare1.EnableWindow(bFlag);
							m_ctrlCanGoto_1.EnableWindow(bFlag);
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(FALSE);
// 							m_ctrlCanDataType1.EnableWindow(FALSE);
// 							m_ctrlCanCompare1.EnableWindow(FALSE);
// 							m_ctrlCanGoto_1.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
						m_ctrlCanDataType1.EnableWindow(bFlag);
						m_ctrlCanCompare1.EnableWindow(bFlag);
						m_ctrlCanGoto_1.EnableWindow(bFlag);
					}
					
			}

// 	if (pStep->ican_function_division[1] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType2.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare2.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_2.EnableWindow(bEnableFlag);
				



				if(m_ctrlCanDivision_2.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[1];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare2.SetItemData(pStep->cCan_compare_type[1],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
							m_ctrlCanDataType2.EnableWindow(bFlag);
							m_ctrlCanCompare2.EnableWindow(bFlag);
							m_ctrlCanGoto_2.EnableWindow(FALSE);	
							
							m_ctrlCanCompare2.SetItemData(pStep->cCan_data_type[1],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
							m_ctrlCanDataType2.EnableWindow(bFlag);
							m_ctrlCanCompare2.EnableWindow(bFlag);
							m_ctrlCanGoto_2.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
							m_ctrlCanDataType2.EnableWindow(FALSE);
							m_ctrlCanCompare2.EnableWindow(FALSE);
							m_ctrlCanGoto_2.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
							m_ctrlCanDataType2.EnableWindow(bFlag);
							m_ctrlCanCompare2.EnableWindow(bFlag);
							m_ctrlCanGoto_2.EnableWindow(bFlag); //yulee 20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(FALSE);
// 							m_ctrlCanDataType2.EnableWindow(FALSE);
// 							m_ctrlCanCompare2.EnableWindow(FALSE);
// 							m_ctrlCanGoto_2.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
						m_ctrlCanDataType2.EnableWindow(bFlag);
						m_ctrlCanCompare2.EnableWindow(bFlag);
						m_ctrlCanGoto_2.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[2] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType3.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare3.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_3.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_3.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[2];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare3.SetItemData(pStep->cCan_compare_type[2],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
							m_ctrlCanDataType3.EnableWindow(bFlag);
							m_ctrlCanCompare3.EnableWindow(bFlag);
							m_ctrlCanGoto_3.EnableWindow(FALSE);	
							
							m_ctrlCanCompare3.SetItemData(pStep->cCan_data_type[2],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
							m_ctrlCanDataType3.EnableWindow(bFlag);
							m_ctrlCanCompare3.EnableWindow(bFlag);
							m_ctrlCanGoto_3.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
							m_ctrlCanDataType3.EnableWindow(FALSE);
							m_ctrlCanCompare3.EnableWindow(FALSE);
							m_ctrlCanGoto_3.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
							m_ctrlCanDataType3.EnableWindow(bFlag);
							m_ctrlCanCompare3.EnableWindow(bFlag);
							m_ctrlCanGoto_3.EnableWindow(bFlag); //20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(FALSE);
// 							m_ctrlCanDataType3.EnableWindow(FALSE);
// 							m_ctrlCanCompare3.EnableWindow(FALSE);
// 							m_ctrlCanGoto_3.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
						m_ctrlCanDataType3.EnableWindow(bFlag);
						m_ctrlCanCompare3.EnableWindow(bFlag);
						m_ctrlCanGoto_3.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[3] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType4.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare4.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_4.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_4.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[3];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare4.SetItemData(pStep->cCan_compare_type[3],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
							m_ctrlCanDataType4.EnableWindow(bFlag);
							m_ctrlCanCompare4.EnableWindow(bFlag);
							m_ctrlCanGoto_4.EnableWindow(FALSE);	
							
							m_ctrlCanCompare4.SetItemData(pStep->cCan_data_type[3],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
							m_ctrlCanDataType4.EnableWindow(bFlag);
							m_ctrlCanCompare4.EnableWindow(bFlag);
							m_ctrlCanGoto_4.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
							m_ctrlCanDataType4.EnableWindow(FALSE);
							m_ctrlCanCompare4.EnableWindow(FALSE);
							m_ctrlCanGoto_4.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
							m_ctrlCanDataType4.EnableWindow(bFlag);
							m_ctrlCanCompare4.EnableWindow(bFlag);
							m_ctrlCanGoto_4.EnableWindow(bFlag); //yulee 20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(FALSE);
// 							m_ctrlCanDataType4.EnableWindow(FALSE);
// 							m_ctrlCanCompare4.EnableWindow(FALSE);
// 							m_ctrlCanGoto_4.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
						m_ctrlCanDataType4.EnableWindow(bFlag);
						m_ctrlCanCompare4.EnableWindow(bFlag);
						m_ctrlCanGoto_4.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[4] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType5.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare5.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_5.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_5.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[4];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare5.SetItemData(pStep->cCan_compare_type[4],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
							m_ctrlCanDataType5.EnableWindow(bFlag);
							m_ctrlCanCompare5.EnableWindow(bFlag);
							m_ctrlCanGoto_5.EnableWindow(FALSE);	
							
							m_ctrlCanCompare5.SetItemData(pStep->cCan_data_type[4],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
							m_ctrlCanDataType5.EnableWindow(bFlag);
							m_ctrlCanCompare5.EnableWindow(bFlag);
							m_ctrlCanGoto_5.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
							m_ctrlCanDataType5.EnableWindow(FALSE);
							m_ctrlCanCompare5.EnableWindow(FALSE);
							m_ctrlCanGoto_5.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
							m_ctrlCanDataType5.EnableWindow(bFlag);
							m_ctrlCanCompare5.EnableWindow(bFlag);
							m_ctrlCanGoto_5.EnableWindow(bFlag);//yulee 20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(FALSE);
// 							m_ctrlCanDataType5.EnableWindow(FALSE);
// 							m_ctrlCanCompare5.EnableWindow(FALSE);
// 							m_ctrlCanGoto_5.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
						m_ctrlCanDataType5.EnableWindow(bFlag);
						m_ctrlCanCompare5.EnableWindow(bFlag);
						m_ctrlCanGoto_5.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[5] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType6.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare6.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_6.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_6.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[5];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare6.SetItemData(pStep->cCan_compare_type[5],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
							m_ctrlCanDataType6.EnableWindow(bFlag);
							m_ctrlCanCompare6.EnableWindow(bFlag);
							m_ctrlCanGoto_6.EnableWindow(FALSE);	
							
							m_ctrlCanCompare6.SetItemData(pStep->cCan_data_type[5],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
							m_ctrlCanDataType6.EnableWindow(bFlag);
							m_ctrlCanCompare6.EnableWindow(bFlag);
							m_ctrlCanGoto_6.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
							m_ctrlCanDataType6.EnableWindow(FALSE);
							m_ctrlCanCompare6.EnableWindow(FALSE);
							m_ctrlCanGoto_6.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
							m_ctrlCanDataType6.EnableWindow(bFlag);
							m_ctrlCanCompare6.EnableWindow(bFlag);
							m_ctrlCanGoto_6.EnableWindow(bFlag);//yulee 20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(FALSE);
// 							m_ctrlCanDataType6.EnableWindow(FALSE);
// 							m_ctrlCanCompare6.EnableWindow(FALSE);
// 							m_ctrlCanGoto_6.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
						m_ctrlCanDataType6.EnableWindow(bFlag);
						m_ctrlCanCompare6.EnableWindow(bFlag);
						m_ctrlCanGoto_6.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[6] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType7.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare7.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_7.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_7.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[6];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare7.SetItemData(pStep->cCan_compare_type[6],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
							m_ctrlCanDataType7.EnableWindow(bFlag);
							m_ctrlCanCompare7.EnableWindow(bFlag);
							m_ctrlCanGoto_7.EnableWindow(FALSE);	
							
							m_ctrlCanCompare7.SetItemData(pStep->cCan_data_type[6],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
							m_ctrlCanDataType7.EnableWindow(bFlag);
							m_ctrlCanCompare7.EnableWindow(bFlag);
							m_ctrlCanGoto_7.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
							m_ctrlCanDataType7.EnableWindow(FALSE);
							m_ctrlCanCompare7.EnableWindow(FALSE);
							m_ctrlCanGoto_7.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
							m_ctrlCanDataType7.EnableWindow(bFlag);
							m_ctrlCanCompare7.EnableWindow(bFlag);
							m_ctrlCanGoto_7.EnableWindow(bFlag);//yulee 20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(FALSE);
// 							m_ctrlCanDataType7.EnableWindow(FALSE);
// 							m_ctrlCanCompare7.EnableWindow(FALSE);
// 							m_ctrlCanGoto_7.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
						m_ctrlCanDataType7.EnableWindow(bFlag);
						m_ctrlCanCompare7.EnableWindow(bFlag);
						m_ctrlCanGoto_7.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[7] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType8.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare8.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_8.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_8.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[7];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare8.SetItemData(pStep->cCan_compare_type[7],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
							m_ctrlCanDataType8.EnableWindow(bFlag);
							m_ctrlCanCompare8.EnableWindow(bFlag);
							m_ctrlCanGoto_8.EnableWindow(FALSE);	
							
							m_ctrlCanCompare8.SetItemData(pStep->cCan_data_type[7],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
							m_ctrlCanDataType8.EnableWindow(bFlag);
							m_ctrlCanCompare8.EnableWindow(bFlag);
							m_ctrlCanGoto_8.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
							m_ctrlCanDataType8.EnableWindow(FALSE);
							m_ctrlCanCompare8.EnableWindow(FALSE);
							m_ctrlCanGoto_8.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
							m_ctrlCanDataType8.EnableWindow(bFlag);
							m_ctrlCanCompare8.EnableWindow(bFlag);
							m_ctrlCanGoto_8.EnableWindow(bFlag);//yulee 20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(FALSE);
// 							m_ctrlCanDataType8.EnableWindow(FALSE);
// 							m_ctrlCanCompare8.EnableWindow(FALSE);
// 							m_ctrlCanGoto_8.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
						m_ctrlCanDataType8.EnableWindow(bFlag);
						m_ctrlCanCompare8.EnableWindow(bFlag);
						m_ctrlCanGoto_8.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[8] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType9.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare9.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_9.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_9.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[8];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare9.SetItemData(pStep->cCan_compare_type[8],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
							m_ctrlCanDataType9.EnableWindow(bFlag);
							m_ctrlCanCompare9.EnableWindow(bFlag);
							m_ctrlCanGoto_9.EnableWindow(FALSE);	
							
							m_ctrlCanCompare9.SetItemData(pStep->cCan_data_type[8],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
							m_ctrlCanDataType9.EnableWindow(bFlag);
							m_ctrlCanCompare9.EnableWindow(bFlag);
							m_ctrlCanGoto_9.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
							m_ctrlCanDataType9.EnableWindow(FALSE);
							m_ctrlCanCompare9.EnableWindow(FALSE);
							m_ctrlCanGoto_9.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
							m_ctrlCanDataType9.EnableWindow(bFlag);
							m_ctrlCanCompare9.EnableWindow(bFlag);
							m_ctrlCanGoto_9.EnableWindow(bFlag);//yulee20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(FALSE);
// 							m_ctrlCanDataType9.EnableWindow(FALSE);
// 							m_ctrlCanCompare9.EnableWindow(FALSE);
// 							m_ctrlCanGoto_9.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
						m_ctrlCanDataType9.EnableWindow(bFlag);
						m_ctrlCanCompare9.EnableWindow(bFlag);
						m_ctrlCanGoto_9.EnableWindow(bFlag);
					}
					
			}
// 	if (pStep->ican_function_division[9] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bEnableFlag);
// 	m_ctrlCanDataType10.EnableWindow(bEnableFlag);
// 	m_ctrlCanCompare10.EnableWindow(bEnableFlag);
// 	m_ctrlCanGoto_10.EnableWindow(bEnableFlag);
				

				if(m_ctrlCanDivision_10.GetCurSel() != LB_ERR)
				{
					iCode = pStep->ican_function_division[9];
					
					if (iCode == 0)
						bFlag = FALSE;
					else
						bFlag = TRUE;
					
					m_ctrlCanCompare10.SetItemData(pStep->cCan_compare_type[9],0);
					
					if(bFlag == TRUE)
					{
						if(iCode == 1901) //yulee 20181009-2
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
							m_ctrlCanDataType10.EnableWindow(bFlag);
							m_ctrlCanCompare10.EnableWindow(bFlag);
							m_ctrlCanGoto_10.EnableWindow(FALSE);	
							
							m_ctrlCanCompare10.SetItemData(pStep->cCan_data_type[9],0);
						}
						else if(((iCode >= 1001) && (iCode <= 1200)) ||
							(iCode == 1924) ||
							(iCode == 1969) ||
							(iCode == 1970))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
							m_ctrlCanDataType10.EnableWindow(bFlag);
							m_ctrlCanCompare10.EnableWindow(bFlag);
							m_ctrlCanGoto_10.EnableWindow(bFlag);
						}
						else if((iCode >= 10010) && (iCode <= 10014))
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
							m_ctrlCanDataType10.EnableWindow(FALSE);
							m_ctrlCanCompare10.EnableWindow(FALSE);
							m_ctrlCanGoto_10.EnableWindow(FALSE);
						}
						else
						{
							GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
							m_ctrlCanDataType10.EnableWindow(bFlag);
							m_ctrlCanCompare10.EnableWindow(bFlag);
							m_ctrlCanGoto_10.EnableWindow(bFlag);//yulee 20181107
// 							GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(FALSE);
// 							m_ctrlCanDataType10.EnableWindow(FALSE);
// 							m_ctrlCanCompare10.EnableWindow(FALSE);
// 							m_ctrlCanGoto_10.EnableWindow(FALSE);
						}
					}
					else
					{
						GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
						m_ctrlCanDataType10.EnableWindow(bFlag);
						m_ctrlCanCompare10.EnableWindow(bFlag);
						m_ctrlCanGoto_10.EnableWindow(bFlag);
					}
					
			}
//Aux
// 	if (pStep->iaux_function_division[0] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType1.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare1.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_1.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_1.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_1.GetItemData(m_ctrlAuxDivision_1.GetCurSel());
		m_ctrlAuxDivision_1.GetLBText(m_ctrlAuxDivision_1.GetCurSel(),strCodeName);//yulee 20180830

		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;

		m_ctrlAuxCompare1.SetItemData(pStep->cAux_compare_type[0],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.EnableWindow(FALSE);
				
				m_ctrlAuxCompare1.SetItemData(pStep->cAux_data_type[0],0);
		
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
			m_ctrlAuxDataType1.EnableWindow(bFlag);
			m_ctrlAuxCompare1.EnableWindow(bFlag);
			m_ctrlAuxGoto_1.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
		}
	}
// 	if (pStep->iaux_function_division[1] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType2.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare2.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_2.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_2.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_2.GetItemData(m_ctrlAuxDivision_2.GetCurSel());
		m_ctrlAuxDivision_2.GetLBText(m_ctrlAuxDivision_2.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare2.SetItemData(pStep->cAux_compare_type[1],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.EnableWindow(FALSE);
				
				m_ctrlAuxCompare2.SetItemData(pStep->cAux_data_type[1],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
			m_ctrlAuxDataType2.EnableWindow(bFlag);
			m_ctrlAuxCompare2.EnableWindow(bFlag);
			m_ctrlAuxGoto_2.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
		}
	}
// 	if (pStep->iaux_function_division[2] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType3.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare3.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_3.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_3.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_3.GetItemData(m_ctrlAuxDivision_3.GetCurSel());
		m_ctrlAuxDivision_3.GetLBText(m_ctrlAuxDivision_3.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare3.SetItemData(pStep->cAux_compare_type[2],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.EnableWindow(FALSE);
				
				m_ctrlAuxCompare3.SetItemData(pStep->cAux_data_type[2],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
			m_ctrlAuxDataType3.EnableWindow(bFlag);
			m_ctrlAuxCompare3.EnableWindow(bFlag);
			m_ctrlAuxGoto_3.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
		}
	}
// 	if (pStep->iaux_function_division[3] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType4.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare4.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_4.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_4.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_4.GetItemData(m_ctrlAuxDivision_4.GetCurSel());
		m_ctrlAuxDivision_4.GetLBText(m_ctrlAuxDivision_4.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare4.SetItemData(pStep->cAux_compare_type[3],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.EnableWindow(FALSE);
				
				m_ctrlAuxCompare4.SetItemData(pStep->cAux_data_type[3],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
			m_ctrlAuxDataType4.EnableWindow(bFlag);
			m_ctrlAuxCompare4.EnableWindow(bFlag);
			m_ctrlAuxGoto_4.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
		}
	}	
// 	if (pStep->iaux_function_division[4] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType5.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare5.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_5.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_5.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_5.GetItemData(m_ctrlAuxDivision_5.GetCurSel());
		m_ctrlAuxDivision_5.GetLBText(m_ctrlAuxDivision_5.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare5.SetItemData(pStep->cAux_compare_type[4],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.EnableWindow(FALSE);
				
				m_ctrlAuxCompare5.SetItemData(pStep->cAux_data_type[4],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
			m_ctrlAuxDataType5.EnableWindow(bFlag);
			m_ctrlAuxCompare5.EnableWindow(bFlag);
			m_ctrlAuxGoto_5.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
		}
	}	
// 	if (pStep->iaux_function_division[5] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType6.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare6.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_6.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_6.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_6.GetItemData(m_ctrlAuxDivision_6.GetCurSel());
		m_ctrlAuxDivision_6.GetLBText(m_ctrlAuxDivision_6.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare6.SetItemData(pStep->cAux_compare_type[5],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.EnableWindow(FALSE);
				
				m_ctrlAuxCompare6.SetItemData(pStep->cAux_data_type[5],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
			m_ctrlAuxDataType6.EnableWindow(bFlag);
			m_ctrlAuxCompare6.EnableWindow(bFlag);
			m_ctrlAuxGoto_6.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
		}
	}	
// 	if (pStep->iaux_function_division[6] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType7.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare7.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_7.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_7.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_7.GetItemData(m_ctrlAuxDivision_7.GetCurSel());
		m_ctrlAuxDivision_7.GetLBText(m_ctrlAuxDivision_7.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare7.SetItemData(pStep->cAux_compare_type[6],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.EnableWindow(FALSE);
				
				m_ctrlAuxCompare7.SetItemData(pStep->cAux_data_type[6],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
			m_ctrlAuxDataType7.EnableWindow(bFlag);
			m_ctrlAuxCompare7.EnableWindow(bFlag);
			m_ctrlAuxGoto_7.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
		}
	}	
// 	if (pStep->iaux_function_division[7] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType8.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare8.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_8.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_8.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_8.GetItemData(m_ctrlAuxDivision_8.GetCurSel());
		m_ctrlAuxDivision_8.GetLBText(m_ctrlAuxDivision_8.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare8.SetItemData(pStep->cAux_compare_type[7],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.EnableWindow(FALSE);
				
				m_ctrlAuxCompare8.SetItemData(pStep->cAux_data_type[7],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
			m_ctrlAuxDataType8.EnableWindow(bFlag);
			m_ctrlAuxCompare8.EnableWindow(bFlag);
			m_ctrlAuxGoto_8.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
		}
	}	
// 	if (pStep->iaux_function_division[8] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType9.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare9.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_9.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_9.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_9.GetItemData(m_ctrlAuxDivision_9.GetCurSel());
		m_ctrlAuxDivision_9.GetLBText(m_ctrlAuxDivision_9.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare9.SetItemData(pStep->cAux_compare_type[8],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.EnableWindow(FALSE);
				
				m_ctrlAuxCompare9.SetItemData(pStep->cAux_data_type[8],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
			m_ctrlAuxDataType9.EnableWindow(bFlag);
			m_ctrlAuxCompare9.EnableWindow(bFlag);
			m_ctrlAuxGoto_9.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
		}
	}	
// 	if (pStep->iaux_function_division[9] == 0)
// 		bEnableFlag = FALSE;
// 	else
// 		bEnableFlag = TRUE;
// 	GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bEnableFlag);
// 	GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bEnableFlag);
// 	m_ctrlAuxDataType10.EnableWindow(bEnableFlag);
// 	m_ctrlAuxCompare10.EnableWindow(bEnableFlag);
// 	m_ctrlAuxGoto_10.EnableWindow(bEnableFlag);

	if(m_ctrlAuxDivision_10.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_10.GetItemData(m_ctrlAuxDivision_10.GetCurSel());
		m_ctrlAuxDivision_10.GetLBText(m_ctrlAuxDivision_10.GetCurSel(),strCodeName);//yulee 20180830
		
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		
		m_ctrlAuxCompare10.SetItemData(pStep->cAux_compare_type[9],0);
		
		
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.EnableWindow(FALSE);
				
				m_ctrlAuxCompare10.SetItemData(pStep->cAux_data_type[9],0);
				
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
			m_ctrlAuxDataType10.EnableWindow(bFlag);
			m_ctrlAuxCompare10.EnableWindow(bFlag);
			m_ctrlAuxGoto_10.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
		}
	}
	
	//2014.12.08 추가.
	CheckDlgButton(IDC_CHK_NOCHECK_PROG_MODE,pStep->nNoCheckMode);
	CheckDlgButton(IDC_CHK_CANTXOFF_PROG_MODE,pStep->nCanTxOffMode); //2014.12.09
//	CheckDlgButton(IDC_CHK_CAN_MODE,pStep->nCanCheckMode);				//20150825 add
	m_ctrlCboCanCheck.SetCurSel(pStep->nCanCheckMode);				//20170328 add

	//2018.08.28 추가. 
	CheckDlgButton(IDC_CHK_CHAMBER_STEP_STOP,pStep->bUseChamberStop); //yulee 20180828 

// 		STEP * pGotoStep = m_pDoc->GetStepDataGotoID(pStep->nLoopInfoEndVGoto);
// 		if(pGotoStep != NULL)
// 			m_nLoopInfoEndVGoto = pGotoStep->chStepNo;
// 		else m_nLoopInfoEndVGoto = 0;
// 		
// 		pGotoStep = m_pDoc->GetStepDataGotoID(pStep->nLoopInfoEndTGoto);
// 		if(pGotoStep != NULL)
// 			m_nLoopInfoEndTGoto = pGotoStep->chStepNo;
// 		else m_nLoopInfoEndTGoto = 0;
// 		
// 		pGotoStep = m_pDoc->GetStepDataGotoID(pStep->nLoopInfoEndCGoto);
// 		if(pGotoStep != NULL)
// 			m_nLoopInfoEndCGoto = pGotoStep->chStepNo;
// 		else m_nLoopInfoEndCGoto = 0;
	

		//20051117 SOC 종료 조건 추가 KBH
	//m_bUseActualCapa = pStep->bUseActualCapa;
	//2009-03-23 ljb 수정
// 	CString strTemp;
// 	strTemp.Format("STEP %d",pStep->nUseDataStepNo);
// 	m_ctrlCapaStepList.SetWindowText(strTemp);
	//m_fSocRate = pStep->m_UseAutucalCapaStepNo;

// 	CRect endRect;
// 	if (pStep->chType == PS_STEP_ADV_CYCLE || pStep->chType == PS_STEP_LOOP || pStep->chType == PS_STEP_END)
// 	{
// 		GetClientRect(endRect);
// 		GetDlgItem(IDC_STEP_END_MEMO)->MoveWindow(endRect.left, endRect.top, endRect.Width()-300,endRect.Height()); 
// 	}

	if (pStep->chType == PS_STEP_ADV_CYCLE)
	{
		m_ctrlCboAddCap.SetCurSel(pStep->nValueRateItem);
		m_ctrlCboAddWattHour.SetCurSel(pStep->nValueStepNo);
	}
	else
	{
		m_ctrlCboValueItem.SetCurSel(pStep->nValueRateItem);
	}

	m_ctrlEditfValueRate = pStep->fValueRate;

	m_ctrlCboLoaderValueItem.SetCurSel(pStep->nValueLoaderItem);	//  [4/22/2015 이재복]
	m_ctrlCboLoaderMode.SetCurSel(pStep->nValueLoaderMode);			//  [4/22/2015 이재복]
	switch (pStep->nValueLoaderMode)
	{
	case 0: 
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("W");
		break;
	case 1:
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("A");
		break;
	case 2: 
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("V");
		break;
	case 3:
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("Ω");
		break;
	}
	m_ctrlEditfValueLoader = pStep->fValueLoaderSet;				//  [4/22/2015 이재복]

	//201010 ljb Cycle 반복 후 Pause 조건 추가
	m_bUseCyclePause = pStep->bUseCyclePause;
	//20100625 ljb CP-CC 연동 STEP
// 	if (pPreStep != NULL)
// 	{
// 		if (pStep->chType == pPreStep->chType && pPreStep->chMode == PS_MODE_CP)
// 		{
// 		}
// 		else
// 		{
// 			pStep->bUseLinkStep = FALSE;
// 		}
// 	}

	m_bUseChamberProg = pStep->bUseChamberProg;  
	m_bChamberStepStop = pStep->bUseChamberStop; //yulee 20180828
	
	m_bUseLinkStep = pStep->bUseLinkStep;
	if(m_bUseLinkStep)
	{
		GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
		GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
		GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
		GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE);
	}	
	
	//ksj 20201014 : 사이클, 루프 스텝에서는 확장조건 사용 불가능하도록 확장 버튼 비활성화 (SBC에서 확장 조건 처리 안함)
	if(pStep->chType == PS_STEP_ADV_CYCLE || pStep->chType == PS_STEP_LOOP)
	{
		GetDlgItem(IDC_BUT_EXTEND)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_BUT_EXTEND)->EnableWindow(TRUE);
	}
	//ksj end

	UpdateData(FALSE);
}

void CEndConditionDlg::OnKillFocusEndMsec()
{
	UpdateData();
	CString strValue;
	GetDlgItem(IDC_END_MSEC_EDIT)->GetWindowText(strValue);
	int nValue = atoi(strValue);

	int nTemp = nValue % 50;
	if(nTemp > 24)
		nValue += 50 - nTemp;
	else
		nValue -= nTemp;

	if(nValue >= 1000)
		nValue = 0;
	if(nValue < 0)
		nValue = 0;
	strValue.Format("%d", nValue);
	GetDlgItem(IDC_END_MSEC_EDIT)->SetWindowText(strValue);
	UpdateData(FALSE);
}

//DEL void CEndConditionDlg::OnSelchangeGotoStepCombo()
//DEL {
//DEL 	UpdateData();
//DEL 	
//DEL 	int nGoto = 0;
//DEL 	if(m_ctrlGoto.GetCurSel() != LB_ERR)	
//DEL 		nGoto = m_ctrlGoto.GetItemData(m_ctrlGoto.GetCurSel());
//DEL // 	else
//DEL // ..		m_ctrlEditLoopInfoGotoCnt = 1;
//DEL 	
//DEL 	
//DEL //	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
//DEL //	ASSERT(pStep);
//DEL 	
//DEL // 	if(nGoto > m_nStepNo || nGoto == 0)
//DEL // 	{
//DEL // 		m_ctrlEditLoopInfoGotoCnt = 1;
//DEL // 		GetDlgItem(IDC_EDIT_GOTOCOUNT)->EnableWindow(FALSE);
//DEL // 	}
//DEL // 	else
//DEL // 	{
//DEL // 		GetDlgItem(IDC_EDIT_GOTOCOUNT)->EnableWindow(TRUE);
//DEL // 	}
//DEL 	UpdateData(FALSE);
//DEL }

void CEndConditionDlg::UpdateBMSGotoList()
{

	m_ctrlCanGoto_1.ResetContent();
	m_ctrlCanGoto_2.ResetContent();
	m_ctrlCanGoto_3.ResetContent();
	m_ctrlCanGoto_4.ResetContent();
	m_ctrlCanGoto_5.ResetContent();
	m_ctrlCanGoto_6.ResetContent();
	m_ctrlCanGoto_7.ResetContent();
	m_ctrlCanGoto_8.ResetContent();
	m_ctrlCanGoto_9.ResetContent();
	m_ctrlCanGoto_10.ResetContent();

	m_ctrlAuxGoto_1.ResetContent();
	m_ctrlAuxGoto_2.ResetContent();
	m_ctrlAuxGoto_3.ResetContent();
	m_ctrlAuxGoto_4.ResetContent();
	m_ctrlAuxGoto_5.ResetContent();
	m_ctrlAuxGoto_6.ResetContent();
	m_ctrlAuxGoto_7.ResetContent();
	m_ctrlAuxGoto_8.ResetContent();
	m_ctrlAuxGoto_9.ResetContent();
	m_ctrlAuxGoto_10.ResetContent();

	STEP *pCurStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];

	int nDefaultIndex1 = 0, nDefaultIndex2 = 0, nDefaultIndex3 = 0, nDefaultIndex4 = 0, nDefaultIndex5 = 0
		, nDefaultIndex6 = 0, nDefaultIndex7 = 0, nDefaultIndex8 = 0, nDefaultIndex9 = 0, nDefaultIndex10 = 0, nDefaultIndex11 = 0;
	
	int nDefaultCanIndex1 = 0, nDefaultCanIndex2 = 0, nDefaultCanIndex3 = 0, nDefaultCanIndex4 = 0, nDefaultCanIndex5 = 0
		, nDefaultCanIndex6 = 0, nDefaultCanIndex7 = 0, nDefaultCanIndex8 = 0, nDefaultCanIndex9 = 0, nDefaultCanIndex10 = 0;
	
	int nDefaultAuxIndex1 = 0, nDefaultAuxIndex2 = 0, nDefaultAuxIndex3 = 0, nDefaultAuxIndex4 = 0, nDefaultAuxIndex5 = 0
		, nDefaultAuxIndex6 = 0, nDefaultAuxIndex7 = 0, nDefaultAuxIndex8 = 0, nDefaultAuxIndex9 = 0, nDefaultAuxIndex10 = 0;

	int iStepPos=0,iStepOldCycle=0,iCnt=0;
	BOOL bIncludeCycle(FALSE);		//자신이 속한 Cycle Step 추가 flag
	STEP* pStep, *pStep2;
	CString strTemp;
	int nCount1 = 0, nCount2 = 0, nCount3 = 0, nCount4 = 0, nCount5 = 0, nCount6 = 0, nCount7 = 0
		, nCount8 = 0, nCount9 = 0, nCount10 = 0, nCount11 = 0;

	int nCanCount1 = 0, nCanCount2 = 0, nCanCount3 = 0, nCanCount4 = 0, nCanCount5 = 0, nCanCount6 = 0, nCanCount7 = 0
		, nCanCount8 = 0, nCanCount9 = 0, nCanCount10 = 0;
	
	int nAuxCount1 = 0, nAuxCount2 = 0, nAuxCount3 = 0, nAuxCount4 = 0, nAuxCount5 = 0, nAuxCount6 = 0, nAuxCount7 = 0
		, nAuxCount8 = 0, nAuxCount9 = 0, nAuxCount10 = 0;


	BOOL bEndStep = FALSE;

	//ljb 20100819 for v100A
	m_ctrlCanGoto_1.AddString("NEXT");
	m_ctrlCanGoto_1.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_2.AddString("NEXT");
	m_ctrlCanGoto_2.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_3.AddString("NEXT");
	m_ctrlCanGoto_3.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_4.AddString("NEXT");
	m_ctrlCanGoto_4.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_5.AddString("NEXT");
	m_ctrlCanGoto_5.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_6.AddString("NEXT");
	m_ctrlCanGoto_6.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_7.AddString("NEXT");
	m_ctrlCanGoto_7.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_8.AddString("NEXT");
	m_ctrlCanGoto_8.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_9.AddString("NEXT");
	m_ctrlCanGoto_9.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCanGoto_10.AddString("NEXT");
	m_ctrlCanGoto_10.SetItemData(0, PS_STEP_NEXT);

	m_ctrlAuxGoto_1.AddString("NEXT");
	m_ctrlAuxGoto_1.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_2.AddString("NEXT");
	m_ctrlAuxGoto_2.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_3.AddString("NEXT");
	m_ctrlAuxGoto_3.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_4.AddString("NEXT");
	m_ctrlAuxGoto_4.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_5.AddString("NEXT");
	m_ctrlAuxGoto_5.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_6.AddString("NEXT");
	m_ctrlAuxGoto_6.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_7.AddString("NEXT");
	m_ctrlAuxGoto_7.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_8.AddString("NEXT");
	m_ctrlAuxGoto_8.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_9.AddString("NEXT");
	m_ctrlAuxGoto_9.SetItemData(0, PS_STEP_NEXT);
	m_ctrlAuxGoto_10.AddString("NEXT");
	m_ctrlAuxGoto_10.SetItemData(0, PS_STEP_NEXT);

	//ljb Pause 추가
	//ljb 20100819 for v100A
	m_ctrlCanGoto_1.AddString("PAUSE");
	m_ctrlCanGoto_1.SetItemData(++nCanCount1, PS_STEP_PAUSE);
	m_ctrlCanGoto_2.AddString("PAUSE");
	m_ctrlCanGoto_2.SetItemData(++nCanCount2, PS_STEP_PAUSE);
	m_ctrlCanGoto_3.AddString("PAUSE");
	m_ctrlCanGoto_3.SetItemData(++nCanCount3, PS_STEP_PAUSE);
	m_ctrlCanGoto_4.AddString("PAUSE");
	m_ctrlCanGoto_4.SetItemData(++nCanCount4, PS_STEP_PAUSE);
	m_ctrlCanGoto_5.AddString("PAUSE");
	m_ctrlCanGoto_5.SetItemData(++nCanCount5, PS_STEP_PAUSE);
	m_ctrlCanGoto_6.AddString("PAUSE");
	m_ctrlCanGoto_6.SetItemData(++nCanCount6, PS_STEP_PAUSE);
	m_ctrlCanGoto_7.AddString("PAUSE");
	m_ctrlCanGoto_7.SetItemData(++nCanCount7, PS_STEP_PAUSE);
	m_ctrlCanGoto_8.AddString("PAUSE");
	m_ctrlCanGoto_8.SetItemData(++nCanCount8, PS_STEP_PAUSE);
	m_ctrlCanGoto_9.AddString("PAUSE");
	m_ctrlCanGoto_9.SetItemData(++nCanCount9, PS_STEP_PAUSE);
	m_ctrlCanGoto_10.AddString("PAUSE");
	m_ctrlCanGoto_10.SetItemData(++nCanCount10, PS_STEP_PAUSE);
	
	m_ctrlAuxGoto_1.AddString("PAUSE");
	m_ctrlAuxGoto_1.SetItemData(++nAuxCount1, PS_STEP_PAUSE);
	m_ctrlAuxGoto_2.AddString("PAUSE");
	m_ctrlAuxGoto_2.SetItemData(++nAuxCount2, PS_STEP_PAUSE);
	m_ctrlAuxGoto_3.AddString("PAUSE");
	m_ctrlAuxGoto_3.SetItemData(++nAuxCount3, PS_STEP_PAUSE);
	m_ctrlAuxGoto_4.AddString("PAUSE");
	m_ctrlAuxGoto_4.SetItemData(++nAuxCount4, PS_STEP_PAUSE);
	m_ctrlAuxGoto_5.AddString("PAUSE");
	m_ctrlAuxGoto_5.SetItemData(++nAuxCount5, PS_STEP_PAUSE);
	m_ctrlAuxGoto_6.AddString("PAUSE");
	m_ctrlAuxGoto_6.SetItemData(++nAuxCount6, PS_STEP_PAUSE);
	m_ctrlAuxGoto_7.AddString("PAUSE");
	m_ctrlAuxGoto_7.SetItemData(++nAuxCount7, PS_STEP_PAUSE);
	m_ctrlAuxGoto_8.AddString("PAUSE");
	m_ctrlAuxGoto_8.SetItemData(++nAuxCount8, PS_STEP_PAUSE);
	m_ctrlAuxGoto_9.AddString("PAUSE");
	m_ctrlAuxGoto_9.SetItemData(++nAuxCount9, PS_STEP_PAUSE);
	m_ctrlAuxGoto_10.AddString("PAUSE");
	m_ctrlAuxGoto_10.SetItemData(++nAuxCount10, PS_STEP_PAUSE);
	//////////////////////////////////////////////////////

	if (pCurStep->ican_branch[0] == PS_STEP_NEXT) nDefaultCanIndex1 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[1] == PS_STEP_NEXT) nDefaultCanIndex2 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[2] == PS_STEP_NEXT) nDefaultCanIndex3 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[3] == PS_STEP_NEXT) nDefaultCanIndex4 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[4] == PS_STEP_NEXT) nDefaultCanIndex5 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[5] == PS_STEP_NEXT) nDefaultCanIndex6 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[6] == PS_STEP_NEXT) nDefaultCanIndex7 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[7] == PS_STEP_NEXT) nDefaultCanIndex8 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[8] == PS_STEP_NEXT) nDefaultCanIndex9 = 0;	//ljb 20150127 add
	if (pCurStep->ican_branch[9] == PS_STEP_NEXT) nDefaultCanIndex10 = 0;	//ljb 20150127 add

	if (pCurStep->ican_branch[0] == PS_STEP_PAUSE) nDefaultCanIndex1 = 1;
	if (pCurStep->ican_branch[1] == PS_STEP_PAUSE) nDefaultCanIndex2 = 1;
	if (pCurStep->ican_branch[2] == PS_STEP_PAUSE) nDefaultCanIndex3 = 1;
	if (pCurStep->ican_branch[3] == PS_STEP_PAUSE) nDefaultCanIndex4 = 1;
	if (pCurStep->ican_branch[4] == PS_STEP_PAUSE) nDefaultCanIndex5 = 1;
	if (pCurStep->ican_branch[5] == PS_STEP_PAUSE) nDefaultCanIndex6 = 1;
	if (pCurStep->ican_branch[6] == PS_STEP_PAUSE) nDefaultCanIndex7 = 1;
	if (pCurStep->ican_branch[7] == PS_STEP_PAUSE) nDefaultCanIndex8 = 1;
	if (pCurStep->ican_branch[8] == PS_STEP_PAUSE) nDefaultCanIndex9 = 1;
	if (pCurStep->ican_branch[9] == PS_STEP_PAUSE) nDefaultCanIndex10 = 1;

	if (pCurStep->iaux_branch[0] == PS_STEP_PAUSE) nDefaultAuxIndex1 = 1;
	if (pCurStep->iaux_branch[1] == PS_STEP_PAUSE) nDefaultAuxIndex2 = 1;
	if (pCurStep->iaux_branch[2] == PS_STEP_PAUSE) nDefaultAuxIndex3 = 1;
	if (pCurStep->iaux_branch[3] == PS_STEP_PAUSE) nDefaultAuxIndex4 = 1;
	if (pCurStep->iaux_branch[4] == PS_STEP_PAUSE) nDefaultAuxIndex5 = 1;
	if (pCurStep->iaux_branch[5] == PS_STEP_PAUSE) nDefaultAuxIndex6 = 1;
	if (pCurStep->iaux_branch[6] == PS_STEP_PAUSE) nDefaultAuxIndex7 = 1;
	if (pCurStep->iaux_branch[7] == PS_STEP_PAUSE) nDefaultAuxIndex8 = 1;
	if (pCurStep->iaux_branch[8] == PS_STEP_PAUSE) nDefaultAuxIndex9 = 1;
	if (pCurStep->iaux_branch[9] == PS_STEP_PAUSE) nDefaultAuxIndex10 = 1;

	//선택 가능한 List를 추가 한다.
	for(int i=0; i<m_pDoc->GetTotalStepNum(); i++)		//현재 step은 제외 하기 위해 -1한다.
	{
		pStep = (STEP*)m_pDoc->m_apStep[i];
		ASSERT(pStep);

		
		if (i > m_nStepNo && bIncludeCycle == FALSE)
		{
			iCnt = iStepOldCycle;
			for(int j=iStepOldCycle ; j < m_pDoc->GetTotalStepNum(); j++)
			{
				pStep2 = (STEP*)m_pDoc->m_apStep[j];
				ASSERT(pStep);
				if(pStep2->chType == PS_STEP_ADV_CYCLE || pStep2->chType == PS_STEP_END)
				{
					bIncludeCycle = TRUE;
					break;
				}
				iCnt++;
				if(m_nStepNo == iCnt )	continue;

				if(pCurStep->ican_branch[0] == iCnt) {	nDefaultCanIndex1 = nCanCount1+1; }
				if(pCurStep->ican_branch[1] == iCnt) {	nDefaultCanIndex2 = nCanCount2+1; }
				if(pCurStep->ican_branch[2] == iCnt) {	nDefaultCanIndex3 = nCanCount3+1; }
				if(pCurStep->ican_branch[3] == iCnt) {	nDefaultCanIndex4 = nCanCount4+1; }
				if(pCurStep->ican_branch[4] == iCnt) {	nDefaultCanIndex5 = nCanCount5+1; }
				if(pCurStep->ican_branch[5] == iCnt) {	nDefaultCanIndex6 = nCanCount6+1; }
				if(pCurStep->ican_branch[6] == iCnt) {	nDefaultCanIndex7 = nCanCount7+1; }
				if(pCurStep->ican_branch[7] == iCnt) {	nDefaultCanIndex8 = nCanCount8+1; }
				if(pCurStep->ican_branch[8] == iCnt) {	nDefaultCanIndex9 = nCanCount9+1; }
				if(pCurStep->ican_branch[9] == iCnt) {	nDefaultCanIndex10 = nCanCount10+1; }

				if(pCurStep->iaux_branch[0] == iCnt) {	nDefaultAuxIndex1 = nAuxCount1+1; }
				if(pCurStep->iaux_branch[1] == iCnt) {	nDefaultAuxIndex2 = nAuxCount2+1; }
				if(pCurStep->iaux_branch[2] == iCnt) {	nDefaultAuxIndex3 = nAuxCount3+1; }
				if(pCurStep->iaux_branch[3] == iCnt) {	nDefaultAuxIndex4 = nAuxCount4+1; }
				if(pCurStep->iaux_branch[4] == iCnt) {	nDefaultAuxIndex5 = nAuxCount5+1; }
				if(pCurStep->iaux_branch[5] == iCnt) {	nDefaultAuxIndex6 = nAuxCount6+1; }
				if(pCurStep->iaux_branch[6] == iCnt) {	nDefaultAuxIndex7 = nAuxCount7+1; }
				if(pCurStep->iaux_branch[7] == iCnt) {	nDefaultAuxIndex8 = nAuxCount8+1; }
				if(pCurStep->iaux_branch[8] == iCnt) {	nDefaultAuxIndex9 = nAuxCount9+1; }
				if(pCurStep->iaux_branch[9] == iCnt) {	nDefaultAuxIndex10 = nAuxCount10+1; }
				
// 				if (pStep2->chType == PS_STEP_END)
// 					strTemp="완료";
// 				else
					strTemp.Format("Step %2d", iCnt);


				//ljb 20100819 for v100A //////////////////////////////////////
				m_ctrlCanGoto_1.AddString(strTemp);
				m_ctrlCanGoto_2.AddString(strTemp);
				m_ctrlCanGoto_3.AddString(strTemp);
				m_ctrlCanGoto_4.AddString(strTemp);
				m_ctrlCanGoto_5.AddString(strTemp);
				m_ctrlCanGoto_6.AddString(strTemp);
				m_ctrlCanGoto_7.AddString(strTemp);
				m_ctrlCanGoto_8.AddString(strTemp);
				m_ctrlCanGoto_9.AddString(strTemp);
				m_ctrlCanGoto_10.AddString(strTemp);

				m_ctrlAuxGoto_1.AddString(strTemp);
				m_ctrlAuxGoto_2.AddString(strTemp);
				m_ctrlAuxGoto_3.AddString(strTemp);
				m_ctrlAuxGoto_4.AddString(strTemp);
				m_ctrlAuxGoto_5.AddString(strTemp);
				m_ctrlAuxGoto_6.AddString(strTemp);
				m_ctrlAuxGoto_7.AddString(strTemp);
				m_ctrlAuxGoto_8.AddString(strTemp);
				m_ctrlAuxGoto_9.AddString(strTemp);
				m_ctrlAuxGoto_10.AddString(strTemp);

				m_ctrlCanGoto_1.SetItemData(++nCanCount1, iCnt);
				m_ctrlCanGoto_2.SetItemData(++nCanCount2, iCnt);
				m_ctrlCanGoto_3.SetItemData(++nCanCount3, iCnt);
				m_ctrlCanGoto_4.SetItemData(++nCanCount4, iCnt);
				m_ctrlCanGoto_5.SetItemData(++nCanCount5, iCnt);
				m_ctrlCanGoto_6.SetItemData(++nCanCount6, iCnt);
				m_ctrlCanGoto_7.SetItemData(++nCanCount7, iCnt);
				m_ctrlCanGoto_8.SetItemData(++nCanCount8, iCnt);
				m_ctrlCanGoto_9.SetItemData(++nCanCount9, iCnt);
				m_ctrlCanGoto_10.SetItemData(++nCanCount10, iCnt);

				m_ctrlAuxGoto_1.SetItemData(++nAuxCount1, iCnt);
				m_ctrlAuxGoto_2.SetItemData(++nAuxCount2, iCnt);
				m_ctrlAuxGoto_3.SetItemData(++nAuxCount3, iCnt);
				m_ctrlAuxGoto_4.SetItemData(++nAuxCount4, iCnt);
				m_ctrlAuxGoto_5.SetItemData(++nAuxCount5, iCnt);
				m_ctrlAuxGoto_6.SetItemData(++nAuxCount6, iCnt);
				m_ctrlAuxGoto_7.SetItemData(++nAuxCount7, iCnt);
				m_ctrlAuxGoto_8.SetItemData(++nAuxCount8, iCnt);
				m_ctrlAuxGoto_9.SetItemData(++nAuxCount9, iCnt);
				m_ctrlAuxGoto_10.SetItemData(++nAuxCount10, iCnt);
				//////////////////////////////////////////////////////////////////////////

			}
			bIncludeCycle = TRUE;
		}

		if(pStep->chType == PS_STEP_ADV_CYCLE || pStep->chType == PS_STEP_END )
		{
			iStepOldCycle = i+1;
			
			if(pCurStep->ican_branch[0] == i+1) {	nDefaultCanIndex1 = nCanCount1+1; }
			if(pCurStep->ican_branch[1] == i+1) {	nDefaultCanIndex2 = nCanCount2+1; }
			if(pCurStep->ican_branch[2] == i+1) {	nDefaultCanIndex3 = nCanCount3+1; }
			if(pCurStep->ican_branch[3] == i+1) {	nDefaultCanIndex4 = nCanCount4+1; }
			if(pCurStep->ican_branch[4] == i+1) {	nDefaultCanIndex5 = nCanCount5+1; }
			if(pCurStep->ican_branch[5] == i+1) {	nDefaultCanIndex6 = nCanCount6+1; }
			if(pCurStep->ican_branch[6] == i+1) {	nDefaultCanIndex7 = nCanCount7+1; }
			if(pCurStep->ican_branch[7] == i+1) {	nDefaultCanIndex8 = nCanCount8+1; }
			if(pCurStep->ican_branch[8] == i+1) {	nDefaultCanIndex9 = nCanCount9+1; }
			if(pCurStep->ican_branch[9] == i+1) {	nDefaultCanIndex10 = nCanCount10+1; }
			
			if(pCurStep->iaux_branch[0] == i+1) {	nDefaultAuxIndex1 = nAuxCount1+1; }
			if(pCurStep->iaux_branch[1] == i+1) {	nDefaultAuxIndex2 = nAuxCount2+1; }
			if(pCurStep->iaux_branch[2] == i+1) {	nDefaultAuxIndex3 = nAuxCount3+1; }
			if(pCurStep->iaux_branch[3] == i+1) {	nDefaultAuxIndex4 = nAuxCount4+1; }
			if(pCurStep->iaux_branch[4] == i+1) {	nDefaultAuxIndex5 = nAuxCount5+1; }
			if(pCurStep->iaux_branch[5] == i+1) {	nDefaultAuxIndex6 = nAuxCount6+1; }
			if(pCurStep->iaux_branch[6] == i+1) {	nDefaultAuxIndex7 = nAuxCount7+1; }
			if(pCurStep->iaux_branch[7] == i+1) {	nDefaultAuxIndex8 = nAuxCount8+1; }
			if(pCurStep->iaux_branch[8] == i+1) {	nDefaultAuxIndex9 = nAuxCount9+1; }
			if(pCurStep->iaux_branch[9] == i+1) {	nDefaultAuxIndex10 = nAuxCount10+1; }
			
			if (pStep->chType == PS_STEP_END)
				strTemp="END";
			else
					strTemp.Format("Step %2d", i+1);
//			strTemp.Format("Step %2d", i+1);

			//ljb 20100819 for v100A //////////////////////////////////////
			m_ctrlCanGoto_1.AddString(strTemp);
			m_ctrlCanGoto_2.AddString(strTemp);
			m_ctrlCanGoto_3.AddString(strTemp);
			m_ctrlCanGoto_4.AddString(strTemp);
			m_ctrlCanGoto_5.AddString(strTemp);
			m_ctrlCanGoto_6.AddString(strTemp);
			m_ctrlCanGoto_7.AddString(strTemp);
			m_ctrlCanGoto_8.AddString(strTemp);
			m_ctrlCanGoto_9.AddString(strTemp);
			m_ctrlCanGoto_10.AddString(strTemp);
			
			m_ctrlAuxGoto_1.AddString(strTemp);
			m_ctrlAuxGoto_2.AddString(strTemp);
			m_ctrlAuxGoto_3.AddString(strTemp);
			m_ctrlAuxGoto_4.AddString(strTemp);
			m_ctrlAuxGoto_5.AddString(strTemp);
			m_ctrlAuxGoto_6.AddString(strTemp);
			m_ctrlAuxGoto_7.AddString(strTemp);
			m_ctrlAuxGoto_8.AddString(strTemp);
			m_ctrlAuxGoto_9.AddString(strTemp);
			m_ctrlAuxGoto_10.AddString(strTemp);
			
			m_ctrlCanGoto_1.SetItemData(++nCanCount1, i+1);
			m_ctrlCanGoto_2.SetItemData(++nCanCount2, i+1);
			m_ctrlCanGoto_3.SetItemData(++nCanCount3, i+1);
			m_ctrlCanGoto_4.SetItemData(++nCanCount4, i+1);
			m_ctrlCanGoto_5.SetItemData(++nCanCount5, i+1);
			m_ctrlCanGoto_6.SetItemData(++nCanCount6, i+1);
			m_ctrlCanGoto_7.SetItemData(++nCanCount7, i+1);
			m_ctrlCanGoto_8.SetItemData(++nCanCount8, i+1);
			m_ctrlCanGoto_9.SetItemData(++nCanCount9, i+1);
			m_ctrlCanGoto_10.SetItemData(++nCanCount10, i+1);
			
			m_ctrlAuxGoto_1.SetItemData(++nAuxCount1, i+1);
			m_ctrlAuxGoto_2.SetItemData(++nAuxCount2, i+1);
			m_ctrlAuxGoto_3.SetItemData(++nAuxCount3, i+1);
			m_ctrlAuxGoto_4.SetItemData(++nAuxCount4, i+1);
			m_ctrlAuxGoto_5.SetItemData(++nAuxCount5, i+1);
			m_ctrlAuxGoto_6.SetItemData(++nAuxCount6, i+1);
			m_ctrlAuxGoto_7.SetItemData(++nAuxCount7, i+1);
			m_ctrlAuxGoto_8.SetItemData(++nAuxCount8, i+1);
			m_ctrlAuxGoto_9.SetItemData(++nAuxCount9, i+1);
			m_ctrlAuxGoto_10.SetItemData(++nAuxCount10, i+1);
			//////////////////////////////////////////////////////////////////////////
		
		}	
	}

	m_ctrlCanGoto_1.SetCurSel(nDefaultCanIndex1);
	m_ctrlCanGoto_2.SetCurSel(nDefaultCanIndex2);
	m_ctrlCanGoto_3.SetCurSel(nDefaultCanIndex3);
	m_ctrlCanGoto_4.SetCurSel(nDefaultCanIndex4);
	m_ctrlCanGoto_5.SetCurSel(nDefaultCanIndex5);
	m_ctrlCanGoto_6.SetCurSel(nDefaultCanIndex6);
	m_ctrlCanGoto_7.SetCurSel(nDefaultCanIndex7);
	m_ctrlCanGoto_8.SetCurSel(nDefaultCanIndex8);
	m_ctrlCanGoto_9.SetCurSel(nDefaultCanIndex9);
	m_ctrlCanGoto_10.SetCurSel(nDefaultCanIndex10);

	m_ctrlAuxGoto_1.SetCurSel(nDefaultAuxIndex1);
	m_ctrlAuxGoto_2.SetCurSel(nDefaultAuxIndex2);
	m_ctrlAuxGoto_3.SetCurSel(nDefaultAuxIndex3);
	m_ctrlAuxGoto_4.SetCurSel(nDefaultAuxIndex4);
	m_ctrlAuxGoto_5.SetCurSel(nDefaultAuxIndex5);
	m_ctrlAuxGoto_6.SetCurSel(nDefaultAuxIndex6);
	m_ctrlAuxGoto_7.SetCurSel(nDefaultAuxIndex7);
	m_ctrlAuxGoto_8.SetCurSel(nDefaultAuxIndex8);
	m_ctrlAuxGoto_9.SetCurSel(nDefaultAuxIndex9);
	m_ctrlAuxGoto_10.SetCurSel(nDefaultAuxIndex10);

}

void CEndConditionDlg::OnChangeCVTimeDayEdit()
{
	// TODO: If this is a RICHEDIT control, the control will not
	UpdateData();
	//long으로 표현 가능한 날짜 (sec×100 된 숫자)
	//21474836.47sec => 248.55 Day 
	//최대 입력범위를 199일 23:59:59 으로 제한 
//	if(m_nCVTimeDay > 199)	m_nCVTimeDay = 199;
	if(m_nCVTimeDay > 1095)	m_nCVTimeDay = 1095;	//ljb 20131212 add 3년으로 제한 365 * 3 = 1095
	UpdateData(FALSE);

	ULONG ulTemp=0;
	COleDateTime CVTime;
	m_ctrlCVTime.GetTime(CVTime);
	//ulTemp = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond() + m_nCVTimeDay * 86400)*100;		
	ulTemp = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond())*100;		
	ulTemp += (m_lCVTimeMSec/10);
	if (ulTemp > 0 || m_nCVTimeDay > 0) m_ctrlCboEndCVTime.EnableWindow(TRUE);
	else m_ctrlCboEndCVTime.EnableWindow(FALSE);
}

void CEndConditionDlg::OnDeltaposCVTimeMsecSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here

	UpdateData();
	CString strData;
	GetDlgItem(IDC_CV_MSEC_EDIT)->GetWindowText(strData);

	int mtime = atol(strData);

	//end time은 0msec 부터 설정 가능
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if(mtime >= 1000)	mtime = 0;
	}
	m_lCVTimeMSec = mtime;	//ljb 20110125
	strData.Format("%03d", mtime);
	GetDlgItem(IDC_CV_MSEC_EDIT)->SetWindowText(strData);	

// 	UpdateData(FALSE);
	ULONG ulTemp=0;
	COleDateTime CVTime;
	m_ctrlCVTime.GetTime(CVTime);
	//ulTemp = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond() + m_nCVTimeDay * 86400)*100;		
	ulTemp = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond())*100;		
	ulTemp += (m_lCVTimeMSec/10);
	if (ulTemp > 0 || m_nCVTimeDay > 0) m_ctrlCboEndCVTime.EnableWindow(TRUE);
	else m_ctrlCboEndCVTime.EnableWindow(FALSE);

	UpdateData(FALSE);
	*pResult = 0;	
}

void CEndConditionDlg::InitControl()
{
	COleDateTime ole;
	SECCurrencyEdit::Format fmt(FALSE);
	fmt.SetMonetarySymbol(0);
	fmt.SetPositiveFormat(2);
	fmt.SetNegativeFormat(2);
	fmt.EnableLeadingZero(FALSE);
	fmt.SetFractionalDigits(1);
	fmt.SetThousandSeparator(0);
	fmt.SetDecimalDigits(0);

	int a = 3, b= 3;
	b = m_pDoc->GetVtgUnitFloat();
	if(m_pDoc->GetVtgUnit() == "V")
	{
		a = 4;  //2014.09.30 전압입력 자리수 4자리로 변경.
	}
	else
	{
		a = 6;
	}
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);

	m_EndV_H.Initialize(this, IDC_END_VTG_H);
	m_EndV_H.SetFormat(fmt); //m_EndV.SetValue(0.0);

	m_EndV_L.Initialize(this, IDC_END_VTG_L);
	m_EndV_L.SetFormat(fmt); //m_EndV.SetValue(0.0);

	m_EnddV.Initialize(this, IDC_END_DELTAV);
	m_EnddV.SetFormat(fmt); //m_EnddV.SetValue(0.0);

	
	b = m_pDoc->GetCrtUnitFloat();
	if(m_pDoc->GetCrtUnit() == "A")
	{
		a = 3;
	}
	else
	{
		a = 6;
	}
	//End Current
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);
	m_EndI.Initialize(this, IDC_END_CRT);
	m_EndI.SetFormat(fmt); //m_EndI.SetValue(0.0);

	//End Capacity
	b = m_pDoc->GetCapUnitFloat();
	fmt.SetFractionalDigits(3);	fmt.SetDecimalDigits(6);
	m_EndC.Initialize(this, IDC_END_CAP);
	m_EndC.SetFormat(fmt); //m_EndC.SetValue(0.0);

	if(m_pDoc->GetWattUnit() == "W" || m_pDoc->GetWattUnit() == "KW") a = 6;
	else a = 3;
	//End Watt
	b = m_pDoc->GetWattUnitFloat();
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);
	m_EndWatt.Initialize(this, IDC_END_WATT);
	m_EndWatt.SetFormat(fmt); //m_EndWatt.SetValue(0.0);

	if(m_pDoc->GetWattHourUnit() == "KWh" || m_pDoc->GetWattHourUnit() == "KW") a = 6;
	//else a = 4;
	else a = 5;
	//End WattHour
	b = m_pDoc->GetWattHourUnitFloat();
	fmt.SetFractionalDigits(b);	fmt.SetDecimalDigits(a);
	m_EndWattHour.Initialize(this, IDC_END_WATTHOUR);
	m_EndWattHour.SetFormat(fmt); //m_EndWatt.SetValue(0.0);
	
	//End Temp
	fmt.SetFractionalDigits(1);	fmt.SetDecimalDigits(3);
	m_EndTemp.Initialize(this, IDC_EDIT_END_TEMP);
	m_EndTemp.SetFormat(fmt); //m_EndWatt.SetValue(0.0);

	//NI-MH Cell인 경우 종료 Delta V에서 Delta Vp를 입력 받는다.
//#ifndef _NIMH_CELL_
//	GetDlgItem(IDC_DELTA_VP_STATIC)->ShowWindow(SW_HIDE);
//	GetDlgItem(IDC_END_DELTAV)->ShowWindow(SW_HIDE);	
//	GetDlgItem(IDC_END_DELTA_V_UNIT_STATIC)->ShowWindow(SW_HIDE);		
//#endif

	GetDlgItem(IDC_END_CRT_UNIT_STATIC)->SetWindowText(m_pDoc->GetCrtUnit());
	GetDlgItem(IDC_END_CAPA_UNIT_STATIC)->SetWindowText(m_pDoc->GetCapUnit());
	GetDlgItem(IDC_END_VTG_H_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());
	GetDlgItem(IDC_END_VTG_L_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());
	GetDlgItem(IDC_END_DELTA_V_UNIT_STATIC)->SetWindowText(m_pDoc->GetVtgUnit());

	//GetDlgItem(IDC_END_WATTHOUR_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattUnit());
	GetDlgItem(IDC_END_WATT_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattUnit()); // 1849 : 20200128 WattHour -> 컨트롤 수정
	GetDlgItem(IDC_END_WATTHOUR_UNIT_STATIC)->SetWindowText(m_pDoc->GetWattHourUnit());
	GetDlgItem(IDC_END_MSEC_EDIT)->SendMessage(EM_LIMITTEXT, 3, 0);


	m_ctrlEndTime.SetTime(DATE(0));
	m_ctrlEndTime.SetFormat("HH:mm:ss");

	m_ctrlCVTime.SetTime(DATE(0));
	m_ctrlCVTime.SetFormat("HH:mm:ss");

	m_ctrlWaitTime.SetTime(DATE(0));
	m_ctrlWaitTime.SetFormat("HH:mm:ss");

}

void CEndConditionDlg::InitCanAuxComboList()
{
	m_pDoc->Fun_GetArryCanDivision(uiArryCanCode);
	m_pDoc->Fun_GetArryAuxDivision(uiArryAuxCode);
	m_pDoc->Fun_GetArryCanStringName(strArryCanName);
	m_pDoc->Fun_GetArryAuxStringName(strArryAuxName);


//	int nSelOption = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", 1);	//ljb 20170720 add
	
//ljb 20110103 v100B
	m_ctrlCanDivision_1.ResetContent();
	m_ctrlCanDivision_2.ResetContent();
	m_ctrlCanDivision_3.ResetContent();
	m_ctrlCanDivision_4.ResetContent();
	m_ctrlCanDivision_5.ResetContent();
	m_ctrlCanDivision_6.ResetContent();
	m_ctrlCanDivision_7.ResetContent();
	m_ctrlCanDivision_8.ResetContent();
	m_ctrlCanDivision_9.ResetContent();
	m_ctrlCanDivision_10.ResetContent();
	m_ctrlAuxDivision_1.ResetContent();
	m_ctrlAuxDivision_2.ResetContent();
	m_ctrlAuxDivision_3.ResetContent();
	m_ctrlAuxDivision_4.ResetContent();
	m_ctrlAuxDivision_5.ResetContent();
	m_ctrlAuxDivision_6.ResetContent();
	m_ctrlAuxDivision_7.ResetContent();
	m_ctrlAuxDivision_8.ResetContent();
	m_ctrlAuxDivision_9.ResetContent();
	m_ctrlAuxDivision_10.ResetContent();

	m_ctrlCanDivision_1.SetItemData(0,0);
	m_ctrlCanDivision_1.AddString("None");
	m_ctrlCanDivision_1.SetCurSel(0);
	m_ctrlCanDivision_2.SetItemData(0,0);
	m_ctrlCanDivision_2.AddString("None");
	m_ctrlCanDivision_2.SetCurSel(0);
	m_ctrlCanDivision_3.SetItemData(0,0);
	m_ctrlCanDivision_3.AddString("None");
	m_ctrlCanDivision_3.SetCurSel(0);
	m_ctrlCanDivision_4.SetItemData(0,0);
	m_ctrlCanDivision_4.AddString("None");
	m_ctrlCanDivision_4.SetCurSel(0);
	m_ctrlCanDivision_5.SetItemData(0,0);
	m_ctrlCanDivision_5.AddString("None");
	m_ctrlCanDivision_5.SetCurSel(0);
	m_ctrlCanDivision_6.SetItemData(0,0);
	m_ctrlCanDivision_6.AddString("None");
	m_ctrlCanDivision_6.SetCurSel(0);
	m_ctrlCanDivision_7.SetItemData(0,0);
	m_ctrlCanDivision_7.AddString("None");
	m_ctrlCanDivision_7.SetCurSel(0);
	m_ctrlCanDivision_8.SetItemData(0,0);
	m_ctrlCanDivision_8.AddString("None");
	m_ctrlCanDivision_8.SetCurSel(0);
	m_ctrlCanDivision_9.SetItemData(0,0);
	m_ctrlCanDivision_9.AddString("None");
	m_ctrlCanDivision_9.SetCurSel(0);
	m_ctrlCanDivision_10.SetItemData(0,0);
	m_ctrlCanDivision_10.AddString("None");
	m_ctrlCanDivision_10.SetCurSel(0);

	m_ctrlAuxDivision_1.SetItemData(0,0);
	m_ctrlAuxDivision_1.AddString("None");
	m_ctrlAuxDivision_1.SetCurSel(0);
	m_ctrlAuxDivision_2.SetItemData(0,0);
	m_ctrlAuxDivision_2.AddString("None");
	m_ctrlAuxDivision_2.SetCurSel(0);
	m_ctrlAuxDivision_3.SetItemData(0,0);
	m_ctrlAuxDivision_3.AddString("None");
	m_ctrlAuxDivision_3.SetCurSel(0);
	m_ctrlAuxDivision_4.SetItemData(0,0);
	m_ctrlAuxDivision_4.AddString("None");
	m_ctrlAuxDivision_4.SetCurSel(0);
	m_ctrlAuxDivision_5.SetItemData(0,0);
	m_ctrlAuxDivision_5.AddString("None");
	m_ctrlAuxDivision_5.SetCurSel(0);
	m_ctrlAuxDivision_6.SetItemData(0,0);
	m_ctrlAuxDivision_6.AddString("None");
	m_ctrlAuxDivision_6.SetCurSel(0);
	m_ctrlAuxDivision_7.SetItemData(0,0);
	m_ctrlAuxDivision_7.AddString("None");
	m_ctrlAuxDivision_7.SetCurSel(0);
	m_ctrlAuxDivision_8.SetItemData(0,0);
	m_ctrlAuxDivision_8.AddString("None");
	m_ctrlAuxDivision_8.SetCurSel(0);
	m_ctrlAuxDivision_9.SetItemData(0,0);
	m_ctrlAuxDivision_9.AddString("None");
	m_ctrlAuxDivision_9.SetCurSel(0);
	m_ctrlAuxDivision_10.SetItemData(0,0);
	m_ctrlAuxDivision_10.AddString("None");
	m_ctrlAuxDivision_10.SetCurSel(0);

	// 	m_pDoc->Fun_GetArryCanDivision(uiArryCanCode);
	// 	m_pDoc->Fun_GetArryAuxDivision(uiArryAuxCode);
	// 	m_pDoc->Fun_GetArryCanStringName(strArryCanName);
	// 	m_pDoc->Fun_GetArryAuxStringName(strArryAuxName);
	CString strTemp; //ksj 20201005

	int a = uiArryCanCode.GetSize();
	a = uiArryAuxCode.GetSize();
	int i;
	for( int i=0; i<uiArryCanCode.GetSize() ; i++)
	{
		/*m_ctrlCanDivision_1.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_2.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_3.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_4.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_5.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_6.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_7.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_8.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_9.AddString(strArryCanName.GetAt(i));
		m_ctrlCanDivision_10.AddString(strArryCanName.GetAt(i));*/

		//ksj 20201005 : real code 표시
		strTemp.Format("%s [%d]",strArryCanName.GetAt(i),uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_1.AddString(strTemp);
		m_ctrlCanDivision_2.AddString(strTemp);
		m_ctrlCanDivision_3.AddString(strTemp);
		m_ctrlCanDivision_4.AddString(strTemp);
		m_ctrlCanDivision_5.AddString(strTemp);
		m_ctrlCanDivision_6.AddString(strTemp);
		m_ctrlCanDivision_7.AddString(strTemp);
		m_ctrlCanDivision_8.AddString(strTemp);
		m_ctrlCanDivision_9.AddString(strTemp);
		m_ctrlCanDivision_10.AddString(strTemp);
		//ksj end
		
		m_ctrlCanDivision_1.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_2.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_3.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_4.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_5.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_6.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_7.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_8.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_9.SetItemData(i+1,uiArryCanCode.GetAt(i));
		m_ctrlCanDivision_10.SetItemData(i+1,uiArryCanCode.GetAt(i));
	}
	for( int i=0; i<uiArryAuxCode.GetSize() ; i++)
	{	
		/*m_ctrlAuxDivision_1.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_2.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_3.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_4.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_5.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_6.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_7.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_8.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_9.AddString(strArryAuxName.GetAt(i));
		m_ctrlAuxDivision_10.AddString(strArryAuxName.GetAt(i));*/

		//ksj 20201005 : real code 표시
		strTemp.Format("%s [%d]",strArryAuxName.GetAt(i),uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_1.AddString(strTemp);
		m_ctrlAuxDivision_2.AddString(strTemp);
		m_ctrlAuxDivision_3.AddString(strTemp);
		m_ctrlAuxDivision_4.AddString(strTemp);
		m_ctrlAuxDivision_5.AddString(strTemp);
		m_ctrlAuxDivision_6.AddString(strTemp);
		m_ctrlAuxDivision_7.AddString(strTemp);
		m_ctrlAuxDivision_8.AddString(strTemp);
		m_ctrlAuxDivision_9.AddString(strTemp);
		m_ctrlAuxDivision_10.AddString(strTemp);
		//ksj end

		m_ctrlAuxDivision_1.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_2.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_3.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_4.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_5.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_6.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_7.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_8.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_9.SetItemData(i+1,uiArryAuxCode.GetAt(i));
		m_ctrlAuxDivision_10.SetItemData(i+1,uiArryAuxCode.GetAt(i));
	}
}
void CEndConditionDlg::InitComboList()
{
	// ljb [4/22/2015 이재복]
	m_ctrlCboLoaderValueItem.ResetContent();
	//m_ctrlCboLoaderValueItem.AddString("사용안함");
	m_ctrlCboLoaderValueItem.AddString(Fun_FindMsg("EndConditionDlg_InitComboList_msg1","IDD_END_COND_DLG"));//&&
	m_ctrlCboLoaderValueItem.SetItemData(0, 0);
	m_ctrlCboLoaderValueItem.AddString("ON");
	m_ctrlCboLoaderValueItem.SetItemData(1, 1);
	m_ctrlCboLoaderValueItem.AddString("OFF");
	m_ctrlCboLoaderValueItem.SetItemData(2, 2);
	m_ctrlCboLoaderValueItem.SetCurSel(0);

	// ljb [4/22/2015 이재복]
	m_ctrlCboLoaderMode.ResetContent();
	m_ctrlCboLoaderMode.AddString("CP");
	m_ctrlCboLoaderMode.SetItemData(0, 0);
	m_ctrlCboLoaderMode.AddString("CC");
	m_ctrlCboLoaderMode.SetItemData(1, 1);
	m_ctrlCboLoaderMode.AddString("CV");
	m_ctrlCboLoaderMode.SetItemData(2, 2);
	m_ctrlCboLoaderMode.AddString("CR");
	m_ctrlCboLoaderMode.SetItemData(3, 3);
	m_ctrlCboLoaderMode.SetCurSel(0);
	
	m_ctrlCboValueItem.ResetContent();
	m_ctrlCboValueItem.AddString(Fun_FindMsg("InitComboList_msg_none","IDD_END_COND_DLG"));
	m_ctrlCboValueItem.SetItemData(0, 0);
	m_ctrlCboValueItem.AddString("Ah");
	m_ctrlCboValueItem.SetItemData(1, 1);
	m_ctrlCboValueItem.AddString("WattHour");
	m_ctrlCboValueItem.SetItemData(2, 2);
	m_ctrlCboValueItem.SetCurSel(0);
	//누적 용량 부호
	m_ctrlCboAddCap.ResetContent();
	m_ctrlCboAddCap.AddString(Fun_FindMsg("InitComboList_msg_all","IDD_END_COND_DLG"));
	m_ctrlCboAddCap.SetItemData(0,0);
	m_ctrlCboAddCap.AddString(Fun_FindMsg("InitComboList_msg_charge","IDD_END_COND_DLG"));
	m_ctrlCboAddCap.SetItemData(1,1);
	m_ctrlCboAddCap.AddString(Fun_FindMsg("InitComboList_msg_discharge","IDD_END_COND_DLG"));
	m_ctrlCboAddCap.SetItemData(2,2);
	m_ctrlCboAddWattHour.ResetContent();
	m_ctrlCboAddWattHour.AddString(Fun_FindMsg("InitComboList_msg_all","IDD_END_COND_DLG"));
	m_ctrlCboAddCap.SetItemData(0,0);
	m_ctrlCboAddWattHour.AddString(Fun_FindMsg("InitComboList_msg_charge","IDD_END_COND_DLG"));
	m_ctrlCboAddCap.SetItemData(1,1);
	m_ctrlCboAddWattHour.AddString(Fun_FindMsg("InitComboList_msg_discharge","IDD_END_COND_DLG"));
	m_ctrlCboAddCap.SetItemData(2,2);

	int i;
	//ljb 20100820 for v100A
	for( int i=0; i<7 ; i++)
	{
		m_ctrlCanCompare1.SetItemData(i,i);
		m_ctrlCanCompare2.SetItemData(i,i);
		m_ctrlCanCompare3.SetItemData(i,i);
		m_ctrlCanCompare4.SetItemData(i,i);
		m_ctrlCanCompare5.SetItemData(i,i);;
		m_ctrlCanCompare6.SetItemData(i,i);
		m_ctrlCanCompare7.SetItemData(i,i);
		m_ctrlCanCompare8.SetItemData(i,i);
		m_ctrlCanCompare9.SetItemData(i,i);
		m_ctrlCanCompare10.SetItemData(i,i);

		m_ctrlAuxCompare1.SetItemData(i,i);
		m_ctrlAuxCompare2.SetItemData(i,i);
		m_ctrlAuxCompare3.SetItemData(i,i);
		m_ctrlAuxCompare4.SetItemData(i,i);
		m_ctrlAuxCompare5.SetItemData(i,i);;
		m_ctrlAuxCompare6.SetItemData(i,i);
		m_ctrlAuxCompare7.SetItemData(i,i);
		m_ctrlAuxCompare8.SetItemData(i,i);
		m_ctrlAuxCompare9.SetItemData(i,i);
		m_ctrlAuxCompare10.SetItemData(i,i);
	}
	for( int i=0; i<3 ; i++)
	{
		m_ctrlCanDataType1.SetItemData(i,i);
		m_ctrlCanDataType2.SetItemData(i,i);
		m_ctrlCanDataType3.SetItemData(i,i);
		m_ctrlCanDataType4.SetItemData(i,i);
		m_ctrlCanDataType5.SetItemData(i,i);
		m_ctrlCanDataType6.SetItemData(i,i);
		m_ctrlCanDataType7.SetItemData(i,i);
		m_ctrlCanDataType8.SetItemData(i,i);
		m_ctrlCanDataType9.SetItemData(i,i);
		m_ctrlCanDataType10.SetItemData(i,i);

		m_ctrlAuxDataType1.SetItemData(i,i);
		m_ctrlAuxDataType2.SetItemData(i,i);
		m_ctrlAuxDataType3.SetItemData(i,i);
		m_ctrlAuxDataType4.SetItemData(i,i);
		m_ctrlAuxDataType5.SetItemData(i,i);
		m_ctrlAuxDataType6.SetItemData(i,i);
		m_ctrlAuxDataType7.SetItemData(i,i);
		m_ctrlAuxDataType8.SetItemData(i,i);
		m_ctrlAuxDataType9.SetItemData(i,i);
		m_ctrlAuxDataType10.SetItemData(i,i);
	}

	//Multi Group and ACC Group 추가
	CString strTemp;
	m_ctrlCboLoopMultiGroupID.ResetContent();
	m_ctrlCboLoopMultiGroupID.AddString(Fun_FindMsg("InitComboList_msg_none","IDD_END_COND_DLG"));
	m_ctrlCboLoopMultiGroupID.SetItemData(0, 0);
	m_ctrlCboLoopAccGroupID.ResetContent();
	m_ctrlCboLoopAccGroupID.AddString(Fun_FindMsg("InitComboList_msg_none","IDD_END_COND_DLG"));
	m_ctrlCboLoopAccGroupID.SetItemData(0, 0);
	for (i=1 ; i<6 ; i++)
	{
		strTemp.Format("Group-%d",i);
		m_ctrlCboLoopAccGroupID.AddString(strTemp);
		m_ctrlCboLoopAccGroupID.SetItemData(i, i);
		strTemp.Format("Multi-%d",i);
		m_ctrlCboLoopMultiGroupID.AddString(strTemp);
		m_ctrlCboLoopMultiGroupID.SetItemData(i, i);
	}
}

void CEndConditionDlg::OnSelchangeCboValueItem() 
{
	// TODO: Add your control notification handler code here
	if(m_ctrlCboValueItem.GetItemData(m_ctrlCboValueItem.GetCurSel()) == 0)
	{
		//m_ctrlCboValueStepNo.SetCurSel(0);
		GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(FALSE);
		m_ctrlCboEndValue.SetCurSel(0);
		m_ctrlEditfValueRate = 0;
	}
	else
	{
		GetDlgItem(IDC_CBO_VALUE_STEP_NO)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_VALUE_RATE)->EnableWindow(TRUE);
		GetDlgItem(IDC_CBO_END_VALUE_GOTO)->EnableWindow(TRUE);
	}	
	m_ctrlCboValueStepNo.SetCurSel(0);
	UpdateData(FALSE);
}

void CEndConditionDlg::UpdateEndGotoList()
{
	m_ctrlCboEndVtg_H.ResetContent();
	m_ctrlCboEndVtg_L.ResetContent();
	m_ctrlCboEndTime.ResetContent();
	m_ctrlCboEndCVTime.ResetContent();
	m_ctrlCboEndCapa.ResetContent();
	m_ctrlCboEndWattHour.ResetContent();
	m_ctrlCboEndValue.ResetContent();
	//m_ctrlGoto.ResetContent();		//ljb 20150206 add

	STEP *pCurStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];

	int nDefaultIndex1 = 0, nDefaultIndex2 = 0, nDefaultIndex3 = 0, nDefaultIndex4 = 0, nDefaultIndex5 = 0
		, nDefaultIndex6 = 0, nDefaultIndex7 = 0, nDefaultIndex8 = 0;
	int iStepPos=0,iStepOldCycle=0,iCnt=0;
	BOOL bIncludeCycle(FALSE);		//자신이 속한 Cycle Step 추가 flag
	STEP* pStep, *pStep2;
	CString strTemp;
	int nCount1 = 1, nCount2 = 1, nCount3 = 1, nCount4 = 1, nCount5 = 1, nCount6 = 1, nCount7 = 1, nCount8 = 1;
	BOOL bEndStep = FALSE;

	m_ctrlCboEndVtg_H.AddString("NEXT");
	m_ctrlCboEndVtg_H.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboEndVtg_L.AddString("NEXT");
	m_ctrlCboEndVtg_L.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboEndTime.AddString("NEXT");
	m_ctrlCboEndTime.SetItemData(0, PS_STEP_NEXT);
	//ksj 20210115 : pause 기능 테스트
	/*m_ctrlCboEndTime.AddString("PAUSE");
	m_ctrlCboEndTime.SetItemData(1, PS_STEP_PAUSE);
	nCount3++;*/
	//ksj end
	m_ctrlCboEndCVTime.AddString("NEXT");
	m_ctrlCboEndCVTime.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboEndCapa.AddString("NEXT");
	m_ctrlCboEndCapa.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboEndWattHour.AddString("NEXT");
	m_ctrlCboEndWattHour.SetItemData(0, PS_STEP_NEXT);
	m_ctrlCboEndValue.AddString("NEXT");
	m_ctrlCboEndValue.SetItemData(0, PS_STEP_NEXT);
	//m_ctrlGoto.AddString("NEXT");				//ljb 20150206 add
	//m_ctrlGoto.SetItemData(0, PS_STEP_NEXT);	//ljb 20150206 add


	//선택 가능한 List를 추가 한다.
	for(int i=0; i<m_pDoc->GetTotalStepNum(); i++)		//현재 step은 제외 하기 위해 -1한다.
	{
		pStep = (STEP*)m_pDoc->m_apStep[i];
		ASSERT(pStep);
		
		if (i > m_nStepNo && bIncludeCycle == FALSE)	//선택된 스텝보다 크고 자신이 속한 Cycle Step 추가 flag
		{
			iCnt = iStepOldCycle;		//자신이 속한 사이클의 ADV_CYCLE 스텝 넘버 (iStepOldCycle)
			for(int j=iStepOldCycle ; j < m_pDoc->GetTotalStepNum(); j++)
			{
				pStep2 = (STEP*)m_pDoc->m_apStep[j];
				ASSERT(pStep);
				if(pStep2->chType == PS_STEP_ADV_CYCLE || pStep2->chType == PS_STEP_END )
				{
					bIncludeCycle = TRUE;
					break;
				}
				iCnt++;
				if(m_nStepNo == iCnt )	continue;
				if(pCurStep->nGotoStepEndV_H == iCnt) { nDefaultIndex1 = nCount1; }
				if(pCurStep->nGotoStepEndV_L == iCnt) {	nDefaultIndex2 = nCount2; }
				if(pCurStep->nGotoStepEndTime == iCnt) { nDefaultIndex3 = nCount3; }
				if(pCurStep->nGotoStepEndCVTime == iCnt) { nDefaultIndex4 = nCount4; }
				if(pCurStep->nGotoStepEndC == iCnt) {	nDefaultIndex5 = nCount5; }
				if(pCurStep->nGotoStepEndWh == iCnt) { nDefaultIndex6 = nCount6; }
				if(pCurStep->nGotoStepEndValue == iCnt) {	nDefaultIndex7 = nCount7; }
				//if(pCurStep->nLoopInfoGotoStep == iCnt) {	nDefaultIndex8 = nCount8; }
				
				strTemp.Format("Step %2d", iCnt);
				m_ctrlCboEndVtg_H.AddString(strTemp);
				m_ctrlCboEndVtg_L.AddString(strTemp);
				m_ctrlCboEndTime.AddString(strTemp);
				m_ctrlCboEndCVTime.AddString(strTemp);
				m_ctrlCboEndCapa.AddString(strTemp);
				m_ctrlCboEndWattHour.AddString(strTemp);
				m_ctrlCboEndValue.AddString(strTemp);
				//m_ctrlGoto.AddString(strTemp);
				
				m_ctrlCboEndVtg_H.SetItemData(nCount1++, iCnt);
				m_ctrlCboEndVtg_L.SetItemData(nCount2++, iCnt);	
				m_ctrlCboEndTime.SetItemData(nCount3++, iCnt);
				m_ctrlCboEndCVTime.SetItemData(nCount4++, iCnt);	
				m_ctrlCboEndCapa.SetItemData(nCount5++, iCnt);
				m_ctrlCboEndWattHour.SetItemData(nCount6++, iCnt);
				m_ctrlCboEndValue.SetItemData(nCount7++, iCnt);	
				//m_ctrlGoto.SetItemData(nCount8++, iCnt);	
			}
			bIncludeCycle = TRUE;
		}

		if(pStep->chType == PS_STEP_ADV_CYCLE || pStep->chType == PS_STEP_END)
		{
			iStepOldCycle = i+1;
			
			if(pCurStep->nGotoStepEndV_H == i+1) { nDefaultIndex1 = nCount1; }
			if(pCurStep->nGotoStepEndV_L == i+1) {	nDefaultIndex2 = nCount2; }
			if(pCurStep->nGotoStepEndTime == i+1) { nDefaultIndex3 = nCount3; }
			if(pCurStep->nGotoStepEndCVTime == i+1) { nDefaultIndex4 = nCount4; }
			if(pCurStep->nGotoStepEndC == i+1) {	nDefaultIndex5 = nCount5; }
			if(pCurStep->nGotoStepEndWh == i+1) { nDefaultIndex6 = nCount6; }
			if(pCurStep->nGotoStepEndValue == i+1) {	nDefaultIndex7 = nCount7; }
			//if(pCurStep->nLoopInfoGotoStep == i+1) {	nDefaultIndex8 = nCount8; }

			if (pStep->chType == PS_STEP_END) 
				strTemp = "END";
			else
				strTemp.Format("Step %2d", i+1);

			m_ctrlCboEndVtg_H.AddString(strTemp);
			m_ctrlCboEndVtg_L.AddString(strTemp);
			m_ctrlCboEndTime.AddString(strTemp);
			m_ctrlCboEndCVTime.AddString(strTemp);
			m_ctrlCboEndCapa.AddString(strTemp);
			m_ctrlCboEndWattHour.AddString(strTemp);
			m_ctrlCboEndValue.AddString(strTemp);
			//m_ctrlGoto.AddString(strTemp);

			m_ctrlCboEndVtg_H.SetItemData(nCount1++, i+1);
			m_ctrlCboEndVtg_L.SetItemData(nCount2++, i+1);	
			m_ctrlCboEndTime.SetItemData(nCount3++, i+1);
			m_ctrlCboEndCVTime.SetItemData(nCount4++, i+1);	
			m_ctrlCboEndCapa.SetItemData(nCount5++, i+1);
			m_ctrlCboEndWattHour.SetItemData(nCount6++, i+1);
			m_ctrlCboEndValue.SetItemData(nCount7++, i+1);	
			//m_ctrlGoto.SetItemData(nCount8++, i+1);	
					
		}
		
	}
	m_ctrlCboEndVtg_H.SetCurSel(nDefaultIndex1);
	m_ctrlCboEndVtg_L.SetCurSel(nDefaultIndex2);
	m_ctrlCboEndTime.SetCurSel(nDefaultIndex3);
	m_ctrlCboEndCVTime.SetCurSel(nDefaultIndex4);
	m_ctrlCboEndCapa.SetCurSel(nDefaultIndex5);
	m_ctrlCboEndWattHour.SetCurSel(nDefaultIndex6);
	m_ctrlCboEndValue.SetCurSel(nDefaultIndex7);
	//m_ctrlGoto.SetCurSel(nDefaultIndex8);
	if (nDefaultIndex1 == 0) m_ctrlCboEndVtg_H.EnableWindow(FALSE);
	if (nDefaultIndex2 == 0) m_ctrlCboEndVtg_L.EnableWindow(FALSE);
	if (nDefaultIndex3 == 0) m_ctrlCboEndCVTime.EnableWindow(FALSE);
	if (nDefaultIndex4 == 0) m_ctrlCboEndTime.EnableWindow(FALSE);
	if (nDefaultIndex5 == 0) m_ctrlCboEndCapa.EnableWindow(FALSE);
	if (nDefaultIndex6 == 0) m_ctrlCboEndWattHour.EnableWindow(FALSE);
	if (nDefaultIndex7 == 0) m_ctrlCboEndValue.EnableWindow(FALSE);
	//if (nDefaultIndex8 == 0) m_ctrlGoto.EnableWindow(FALSE);
}

void CEndConditionDlg::OnChangeEndCap() 
{
	UpdateData();
	double	dwValue =0.0;
	m_EndC.GetValue(dwValue);
	if(dwValue != 0.0)	
		m_ctrlCboEndCapa.EnableWindow(TRUE);
	else 
		m_ctrlCboEndCapa.EnableWindow(FALSE);
	UpdateData(FALSE);
}

void CEndConditionDlg::OnChangeEndWatthour() 
{
	UpdateData();
	double	dwValue =0.0;
	m_EndWattHour.GetValue(dwValue);
	if(dwValue != 0.0)	
		m_ctrlCboEndWattHour.EnableWindow(TRUE);
	else 
		m_ctrlCboEndWattHour.EnableWindow(FALSE);
	UpdateData(FALSE);
}

void CEndConditionDlg::OnChangeEndVtgH() 
{
	UpdateData();
	double	dwValue =0.0;
	m_EndV_H.GetValue(dwValue);
	if(dwValue != 0.0)	
		m_ctrlCboEndVtg_H.EnableWindow(TRUE);
	else 
		m_ctrlCboEndVtg_H.EnableWindow(FALSE);
	UpdateData(FALSE);
}

void CEndConditionDlg::OnChangeEndVtgL() 
{
	UpdateData();
	double	dwValue =0.0;
	m_EndV_L.GetValue(dwValue);
	if(dwValue != 0.0)	
		m_ctrlCboEndVtg_L.EnableWindow(TRUE);
	else 
		m_ctrlCboEndVtg_L.EnableWindow(FALSE);
	UpdateData(FALSE);
}

void CEndConditionDlg::OnDatetimechangeDatetimepicker2(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	UpdateData(FALSE);
	ULONG ulTemp=0;
	COleDateTime CVTime;
	m_ctrlCVTime.GetTime(CVTime);
	//ulTemp = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond() + m_nCVTimeDay * 86400)*100;		
	ulTemp = (CVTime.GetHour()*3600+CVTime.GetMinute()*60+CVTime.GetSecond())*100;		
	ulTemp += (m_lCVTimeMSec/10);
	if (ulTemp > 0 || m_nCVTimeDay > 0) m_ctrlCboEndCVTime.EnableWindow(TRUE);
	else m_ctrlCboEndCVTime.EnableWindow(FALSE);
	
	*pResult = 0;
}

void CEndConditionDlg::OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	UpdateData(FALSE);
	COleDateTime endTime;
	ULONG ulTemp=0;
	m_ctrlEndTime.GetTime(endTime);
	//ulTemp = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond() + m_nEndDay * 86400)*100;		
	ulTemp = (endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond())*100;		
	ulTemp += (m_lEndMSec/10);
	if (ulTemp > 0 || m_nEndDay > 0) m_ctrlCboEndTime.EnableWindow(TRUE);
	else m_ctrlCboEndTime.EnableWindow(FALSE);
	
	*pResult = 0;
}

void CEndConditionDlg::OnKillfocusEditCanCategorySet1() 
{
	// TODO: Add your control notification handler code here
// 	AfxMessageBox("killFocus can categoryset 1");	
}

void CEndConditionDlg::OnChkLinkPreStep() 
{
	if(m_nStepNo <= 2 || m_nStepNo >SCH_MAX_STEP)
	{
		AfxMessageBox(Fun_FindMsg("OnKillfocusEditCanCategorySet1_msg1","IDD_END_COND_DLG"));
		m_bUseLinkStep = FALSE;
		UpdateData(FALSE);
		return;
	}
	
	UpdateData();
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	STEP *pStep2 = (STEP*)m_pDoc->m_apStep[m_nStepNo-2];
	ASSERT(pStep);
	
	if(pStep)
	{
		if(m_bUseLinkStep == TRUE)
		{
			pStep->ulEndTime = pStep2->ulEndTime;
			pStep2->ulReportTime = pStep->ulReportTime;
			pStep2->bUseLinkStep = TRUE;
			COleDateTime endTime;
			ULONG lSecond = pStep->ulEndTime / 100;
			m_lEndMSec = (pStep->ulEndTime % 100)*10;
			//m_nEndDay = lSecond / 86400;
			m_nEndDay = pStep->ulEndTimeDay;	//ljb 20131212 add
			int nHour = lSecond % 86400;
			endTime.SetTime(nHour/3600, (nHour%3600)/60, (nHour%3600)%60);
			m_ctrlEndTime.SetTime(endTime);
			
			UpdateData(FALSE);
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(FALSE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(FALSE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(FALSE);
		}
		else
		{
			pStep2->bUseLinkStep = FALSE;
			GetDlgItem(IDC_END_TIME_MSEC_SPIN)->EnableWindow(TRUE); 	
			GetDlgItem(IDC_END_DAY_EDIT)->EnableWindow(TRUE); 
			GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE); 
			GetDlgItem(IDC_END_MSEC_EDIT)->EnableWindow(TRUE);
		}
	}	
}

void CEndConditionDlg::OnButExtend() 
{
	InitCanAuxComboList();		//ljb 2011221 이재복 //////////
	DisplayData();

//	OnOK();
	m_bExtViewMode = !m_bExtViewMode;
	CRect rRect,rStatic;
	GetClientRect(rRect);
	ClientToScreen(rRect);
	if (m_bExtViewMode)
	{
		GetDlgItem(IDC_BUT_EXTEND)->SetWindowText(Fun_FindMsg("OnButExtend_msg1","IDD_END_COND_DLG"));	
		SetRect(&rRect,rRect.left,rRect.top,rRect.left + rOrgSize.Width(),rRect.top + rOrgSize.Height());
		MoveWindow(rRect,TRUE);
// 		MoveWindow( rRect.left, rRect.top, rRect.left + rOrgSize.Width(), rRect.top + rOrgSize.Height());
// 		ShowWindow(SW_SHOW);

	}
	else
	{
		GetDlgItem(IDC_BUT_EXTEND)->SetWindowText(Fun_FindMsg("OnButExtend_msg2","IDD_END_COND_DLG"));
		GetDlgItem(IDC_STATIC_AUX_END)->GetClientRect(rStatic); //yulee 20180927
		SetRect(&rRect,rRect.left-5,rRect.top-5,rRect.left + rOrgSize.Width()-(rStatic.Width()+12),rRect.top + rOrgSize.Height());
		MoveWindow(rRect,TRUE);
	}

// 	CRect rEnd,rDialog,rStatic;
// 	GetClientRect(rEnd);
// 	GetDlgItem(IDC_STATIC_CAN_END)->GetClientRect(rStatic);
// 	SetRect(&rDialog, rEnd.left,rEnd.top, rEnd.Width() + rStatic.Width()+15,rEnd.Height());
//     MoveWindow(rDialog, TRUE);
	
}

void CEndConditionDlg::Fun_FindCanAuxName(UINT uiType, int nPos, int nDivision)
{
	int nFindPos;
	if (uiType == ID_CAN_NAME)
	{
		nFindPos = 1;
		for (int i=0; i < uiArryCanCode.GetSize() ; i++)
		{
			if (nDivision == uiArryCanCode.GetAt(i)) break;
			nFindPos++;
		}
		if (nFindPos > uiArryCanCode.GetSize()) nFindPos = 0;
		
		if (nPos == 1) m_ctrlCanDivision_1.SetCurSel(nFindPos);
		if (nPos == 2) m_ctrlCanDivision_2.SetCurSel(nFindPos);
		if (nPos == 3) m_ctrlCanDivision_3.SetCurSel(nFindPos);
		if (nPos == 4) m_ctrlCanDivision_4.SetCurSel(nFindPos);
		if (nPos == 5) m_ctrlCanDivision_5.SetCurSel(nFindPos);
		if (nPos == 6) m_ctrlCanDivision_6.SetCurSel(nFindPos);
		if (nPos == 7) m_ctrlCanDivision_7.SetCurSel(nFindPos);
		if (nPos == 8) m_ctrlCanDivision_8.SetCurSel(nFindPos);
		if (nPos == 9) m_ctrlCanDivision_9.SetCurSel(nFindPos);
		if (nPos == 10) m_ctrlCanDivision_10.SetCurSel(nFindPos);
	}
	else if (uiType == ID_AUX_NAME)
	{
		nFindPos = 1;
		for (int i=0; i < uiArryAuxCode.GetSize() ; i++)
		{
			if (nDivision == uiArryAuxCode.GetAt(i)) break;
			nFindPos++;
		}
		if (nFindPos > uiArryAuxCode.GetSize()) nFindPos = 0;
		
		if (nPos == 1) m_ctrlAuxDivision_1.SetCurSel(nFindPos);
		if (nPos == 2) m_ctrlAuxDivision_2.SetCurSel(nFindPos);
		if (nPos == 3) m_ctrlAuxDivision_3.SetCurSel(nFindPos);
		if (nPos == 4) m_ctrlAuxDivision_4.SetCurSel(nFindPos);
		if (nPos == 5) m_ctrlAuxDivision_5.SetCurSel(nFindPos);
		if (nPos == 6) m_ctrlAuxDivision_6.SetCurSel(nFindPos);
		if (nPos == 7) m_ctrlAuxDivision_7.SetCurSel(nFindPos);
		if (nPos == 8) m_ctrlAuxDivision_8.SetCurSel(nFindPos);
		if (nPos == 9) m_ctrlAuxDivision_9.SetCurSel(nFindPos);
		if (nPos == 10) m_ctrlAuxDivision_10.SetCurSel(nFindPos);
	}
}

BOOL CEndConditionDlg::Fun_CheckString(CString strData)
{
	int nLength = strData.GetLength();
	char * pChar;
	CString strTemp;
	for (int i=0; i<nLength; i++)
	{
		strTemp = strData.Mid(i,1);
		pChar = (char *) (LPCTSTR)strTemp;
		if (*pChar < 48 || *pChar > 57)
		{
			return FALSE;
		}
	}
	return TRUE;
}

void CEndConditionDlg::OnSelchangeCboCanDivision1() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_1.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_1.GetItemData(m_ctrlCanDivision_1.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlCanDataType1.EnableWindow(bFlag);
				m_ctrlCanCompare1.EnableWindow(bFlag);
				m_ctrlCanGoto_1.EnableWindow(FALSE);
				m_ctrlCanGoto_1.SetItemData(0,0);
				m_ctrlCanGoto_1.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlCanDataType1.EnableWindow(bFlag);
				m_ctrlCanCompare1.EnableWindow(bFlag);
				m_ctrlCanGoto_1.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlCanDataType1.EnableWindow(FALSE);
				m_ctrlCanCompare1.EnableWindow(FALSE);
				m_ctrlCanCompare1.SetItemData(0,0);
				m_ctrlCanCompare1.SetCurSel(0);
				m_ctrlCanGoto_1.EnableWindow(FALSE);
				m_ctrlCanGoto_1.SetItemData(0,0);
				m_ctrlCanGoto_1.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag); //yulee 20181107
				m_ctrlCanDataType1.EnableWindow(bFlag);
				m_ctrlCanCompare1.EnableWindow(bFlag);
				m_ctrlCanGoto_1.EnableWindow(bFlag);
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(FALSE);
// 				m_fCanValue_1 = 0.0f;
// 				m_ctrlCanDataType1.EnableWindow(FALSE);
// 				m_ctrlCanCompare1.EnableWindow(FALSE);
// 				m_ctrlCanCompare1.SetItemData(0,0);
// 				m_ctrlCanCompare1.SetCurSel(0);
// 				m_ctrlCanGoto_1.EnableWindow(FALSE);
// 				m_ctrlCanGoto_1.SetItemData(0,0);
// 				m_ctrlCanGoto_1.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET1)->EnableWindow(bFlag);
			m_ctrlCanDataType1.EnableWindow(bFlag);
			m_ctrlCanCompare1.EnableWindow(bFlag);
			m_ctrlCanGoto_1.EnableWindow(bFlag);
			m_fCanValue_1 = 0.0f;
			m_ctrlCanCompare1.SetItemData(0,0);
			m_ctrlCanCompare1.SetCurSel(0);
			m_ctrlCanGoto_1.SetItemData(0,0);
			m_ctrlCanGoto_1.SetCurSel(0);
		}
	}
	else
	{
		m_ctrlCanDivision_1.SetCurSel(0);
		m_ctrlCanDivision_1.SetFocus();
	}

	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[0] = m_ctrlCanDivision_1.GetItemData(m_ctrlCanDivision_1.GetCurSel());
	ASSERT(pStep);

	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboCanDivision2() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_2.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_2.GetItemData(m_ctrlCanDivision_2.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlCanDataType2.EnableWindow(bFlag);
				m_ctrlCanCompare2.EnableWindow(bFlag);
				m_ctrlCanGoto_2.EnableWindow(FALSE);
				
				m_ctrlCanCompare2.SetItemData(0,0);
				m_ctrlCanCompare2.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlCanDataType2.EnableWindow(bFlag);
				m_ctrlCanCompare2.EnableWindow(bFlag);
				m_ctrlCanGoto_2.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlCanDataType2.EnableWindow(FALSE);
				m_ctrlCanCompare2.EnableWindow(FALSE);
				m_ctrlCanCompare2.SetItemData(0,0);
				m_ctrlCanCompare2.SetCurSel(0);
				m_ctrlCanGoto_2.EnableWindow(FALSE);
				m_ctrlCanGoto_2.SetItemData(0,0);
				m_ctrlCanGoto_2.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlCanDataType2.EnableWindow(bFlag);
				m_ctrlCanCompare2.EnableWindow(bFlag);
				m_ctrlCanGoto_2.EnableWindow(bFlag); //yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(FALSE);
// 				m_fCanValue_2 = 0.0f;
// 				m_ctrlCanDataType2.EnableWindow(FALSE);
// 				m_ctrlCanCompare2.EnableWindow(FALSE);
// 				m_ctrlCanCompare2.SetItemData(0,0);
// 				m_ctrlCanCompare2.SetCurSel(0);
// 				m_ctrlCanGoto_2.EnableWindow(FALSE);
// 				m_ctrlCanGoto_2.SetItemData(0,0);
// 				m_ctrlCanGoto_2.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET2)->EnableWindow(bFlag);
			m_ctrlCanDataType2.EnableWindow(bFlag);
			m_ctrlCanCompare2.EnableWindow(bFlag);
			m_ctrlCanGoto_2.EnableWindow(bFlag);
			m_fCanValue_2 = 0.0f;
			m_ctrlCanCompare2.SetItemData(0,0);
			m_ctrlCanCompare2.SetCurSel(0);
			m_ctrlCanGoto_2.SetItemData(0,0);
			m_ctrlCanGoto_2.SetCurSel(0);
		}
		
	}
	else
	{
		m_ctrlCanDivision_2.SetCurSel(0);
		m_ctrlCanDivision_2.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[1] = m_ctrlCanDivision_2.GetItemData(m_ctrlCanDivision_2.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboCanDivision3() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_3.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_3.GetItemData(m_ctrlCanDivision_3.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlCanDataType3.EnableWindow(bFlag);
				m_ctrlCanCompare3.EnableWindow(bFlag);
				m_ctrlCanGoto_3.EnableWindow(FALSE);

				m_ctrlCanCompare3.SetItemData(0,0);
				m_ctrlCanCompare3.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlCanDataType3.EnableWindow(bFlag);
				m_ctrlCanCompare3.EnableWindow(bFlag);
				m_ctrlCanGoto_3.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlCanDataType3.EnableWindow(FALSE);
				m_ctrlCanCompare3.EnableWindow(FALSE);
				m_ctrlCanCompare3.SetItemData(0,0);
				m_ctrlCanCompare3.SetCurSel(0);
				m_ctrlCanGoto_3.EnableWindow(FALSE);
				m_ctrlCanGoto_3.SetItemData(0,0);
				m_ctrlCanGoto_3.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlCanDataType3.EnableWindow(bFlag);
				m_ctrlCanCompare3.EnableWindow(bFlag);
				m_ctrlCanGoto_3.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(FALSE);
// 				m_fCanValue_3 = 0.0f;
// 				m_ctrlCanDataType3.EnableWindow(FALSE);
// 				m_ctrlCanCompare3.EnableWindow(FALSE);
// 				m_ctrlCanCompare3.SetItemData(0,0);
// 				m_ctrlCanCompare3.SetCurSel(0);
// 				m_ctrlCanGoto_3.EnableWindow(FALSE);
// 				m_ctrlCanGoto_3.SetItemData(0,0);
// 				m_ctrlCanGoto_3.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET3)->EnableWindow(bFlag);
			m_ctrlCanDataType3.EnableWindow(bFlag);
			m_ctrlCanCompare3.EnableWindow(bFlag);
			m_ctrlCanGoto_3.EnableWindow(bFlag);
			m_fCanValue_3 = 0.0f;
			m_ctrlCanCompare3.SetItemData(0,0);
			m_ctrlCanCompare3.SetCurSel(0);
			m_ctrlCanGoto_3.SetItemData(0,0);
			m_ctrlCanGoto_3.SetCurSel(0);
		}
	}
	else
	{
		m_ctrlCanDivision_3.SetCurSel(0);
		m_ctrlCanDivision_3.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[2] = m_ctrlCanDivision_3.GetItemData(m_ctrlCanDivision_3.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboCanDivision4() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_4.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_4.GetItemData(m_ctrlCanDivision_4.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlCanDataType4.EnableWindow(bFlag);
				m_ctrlCanCompare4.EnableWindow(bFlag);
				m_ctrlCanGoto_4.EnableWindow(FALSE);

				m_ctrlCanCompare4.SetItemData(0,0);
				m_ctrlCanCompare4.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlCanDataType4.EnableWindow(bFlag);
				m_ctrlCanCompare4.EnableWindow(bFlag);
				m_ctrlCanGoto_4.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlCanDataType4.EnableWindow(FALSE);
				m_ctrlCanCompare4.EnableWindow(FALSE);
				m_ctrlCanCompare4.SetItemData(0,0);
				m_ctrlCanCompare4.SetCurSel(0);
				m_ctrlCanGoto_4.EnableWindow(FALSE);
				m_ctrlCanGoto_4.SetItemData(0,0);
				m_ctrlCanGoto_4.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlCanDataType4.EnableWindow(bFlag);
				m_ctrlCanCompare4.EnableWindow(bFlag);
				m_ctrlCanGoto_4.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(FALSE);
// 				m_fCanValue_4 = 0.0f;
// 				m_ctrlCanDataType4.EnableWindow(FALSE);
// 				m_ctrlCanCompare4.EnableWindow(FALSE);
// 				m_ctrlCanCompare4.SetItemData(0,0);
// 				m_ctrlCanCompare4.SetCurSel(0);
// 				m_ctrlCanGoto_4.EnableWindow(FALSE);
// 				m_ctrlCanGoto_4.SetItemData(0,0);
// 				m_ctrlCanGoto_4.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET4)->EnableWindow(bFlag);
			m_ctrlCanDataType4.EnableWindow(bFlag);
			m_ctrlCanCompare4.EnableWindow(bFlag);
			m_ctrlCanGoto_4.EnableWindow(bFlag);
			m_fCanValue_4 = 0.0f;
			m_ctrlCanCompare4.SetItemData(0,0);
			m_ctrlCanCompare4.SetCurSel(0);
			m_ctrlCanGoto_4.SetItemData(0,0);
			m_ctrlCanGoto_4.SetCurSel(0);
		}
		
	}
	else
	{
		m_ctrlCanDivision_4.SetCurSel(0);
		m_ctrlCanDivision_4.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[3] = m_ctrlCanDivision_4.GetItemData(m_ctrlCanDivision_4.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);			
}

void CEndConditionDlg::OnSelchangeCboCanDivision5() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_5.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_5.GetItemData(m_ctrlCanDivision_5.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlCanDataType5.EnableWindow(bFlag);
				m_ctrlCanCompare5.EnableWindow(bFlag);
				m_ctrlCanGoto_5.EnableWindow(FALSE);

				m_ctrlCanCompare5.SetItemData(0,0);
				m_ctrlCanCompare5.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlCanDataType5.EnableWindow(bFlag);
				m_ctrlCanCompare5.EnableWindow(bFlag);
				m_ctrlCanGoto_5.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlCanDataType5.EnableWindow(FALSE);
				m_ctrlCanCompare5.EnableWindow(FALSE);
				m_ctrlCanCompare5.SetItemData(0,0);
				m_ctrlCanCompare5.SetCurSel(0);
				m_ctrlCanGoto_5.EnableWindow(FALSE);
				m_ctrlCanGoto_5.SetItemData(0,0);
				m_ctrlCanGoto_5.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlCanDataType5.EnableWindow(bFlag);
				m_ctrlCanCompare5.EnableWindow(bFlag);
				m_ctrlCanGoto_5.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(FALSE);
// 				m_fCanValue_5 = 0.0f;
// 				m_ctrlCanDataType5.EnableWindow(FALSE);
// 				m_ctrlCanCompare5.EnableWindow(FALSE);
// 				m_ctrlCanCompare5.SetItemData(0,0);
// 				m_ctrlCanCompare5.SetCurSel(0);
// 				m_ctrlCanGoto_5.EnableWindow(FALSE);
// 				m_ctrlCanGoto_5.SetItemData(0,0);
// 				m_ctrlCanGoto_5.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET5)->EnableWindow(bFlag);
			m_ctrlCanDataType5.EnableWindow(bFlag);
			m_ctrlCanCompare5.EnableWindow(bFlag);
			m_ctrlCanGoto_5.EnableWindow(bFlag);
			m_fCanValue_5 = 0.0f;
			m_ctrlCanCompare5.SetItemData(0,0);
			m_ctrlCanCompare5.SetCurSel(0);
			m_ctrlCanGoto_5.SetItemData(0,0);
			m_ctrlCanGoto_5.SetCurSel(0);
		}
	}
	else
	{
		m_ctrlCanDivision_5.SetCurSel(0);
		m_ctrlCanDivision_5.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[4] = m_ctrlCanDivision_5.GetItemData(m_ctrlCanDivision_5.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);			
}

void CEndConditionDlg::OnSelchangeCboCanDivision6() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_6.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_6.GetItemData(m_ctrlCanDivision_6.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlCanDataType6.EnableWindow(bFlag);
				m_ctrlCanCompare6.EnableWindow(bFlag);
				m_ctrlCanGoto_6.EnableWindow(FALSE);

				m_ctrlCanCompare6.SetItemData(0,0);
				m_ctrlCanCompare6.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlCanDataType6.EnableWindow(bFlag);
				m_ctrlCanCompare6.EnableWindow(bFlag);
				m_ctrlCanGoto_6.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlCanDataType6.EnableWindow(FALSE);
				m_ctrlCanCompare6.EnableWindow(FALSE);
				m_ctrlCanCompare6.SetItemData(0,0);
				m_ctrlCanCompare6.SetCurSel(0);
				m_ctrlCanGoto_6.EnableWindow(FALSE);
				m_ctrlCanGoto_6.SetItemData(0,0);
				m_ctrlCanGoto_6.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlCanDataType6.EnableWindow(bFlag);
				m_ctrlCanCompare6.EnableWindow(bFlag);
				m_ctrlCanGoto_6.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(FALSE);
// 				m_fCanValue_6 = 0.0f;
// 				m_ctrlCanDataType6.EnableWindow(FALSE);
// 				m_ctrlCanCompare6.EnableWindow(FALSE);
// 				m_ctrlCanCompare6.SetItemData(0,0);
// 				m_ctrlCanCompare6.SetCurSel(0);
// 				m_ctrlCanGoto_6.EnableWindow(FALSE);
// 				m_ctrlCanGoto_6.SetItemData(0,0);
// 				m_ctrlCanGoto_6.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET6)->EnableWindow(bFlag);
			m_ctrlCanDataType6.EnableWindow(bFlag);
			m_ctrlCanCompare6.EnableWindow(bFlag);
			m_ctrlCanGoto_6.EnableWindow(bFlag);
			m_fCanValue_6 = 0.0f;
			m_ctrlCanCompare6.SetItemData(0,0);
			m_ctrlCanCompare6.SetCurSel(0);
			m_ctrlCanGoto_6.SetItemData(0,0);
			m_ctrlCanGoto_6.SetCurSel(0);
		}
		
	}
	else
	{
		m_ctrlCanDivision_6.SetCurSel(0);
		m_ctrlCanDivision_6.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[5] = m_ctrlCanDivision_6.GetItemData(m_ctrlCanDivision_6.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboCanDivision7() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_7.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_7.GetItemData(m_ctrlCanDivision_7.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlCanDataType7.EnableWindow(bFlag);
				m_ctrlCanCompare7.EnableWindow(bFlag);
				m_ctrlCanGoto_7.EnableWindow(FALSE);

				m_ctrlCanCompare7.SetItemData(0,0);
				m_ctrlCanCompare7.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlCanDataType7.EnableWindow(bFlag);
				m_ctrlCanCompare7.EnableWindow(bFlag);
				m_ctrlCanGoto_7.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlCanDataType7.EnableWindow(FALSE);
				m_ctrlCanCompare7.EnableWindow(FALSE);
				m_ctrlCanCompare7.SetItemData(0,0);
				m_ctrlCanCompare7.SetCurSel(0);
				m_ctrlCanGoto_7.EnableWindow(FALSE);
				m_ctrlCanGoto_7.SetItemData(0,0);
				m_ctrlCanGoto_7.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlCanDataType7.EnableWindow(bFlag);
				m_ctrlCanCompare7.EnableWindow(bFlag);
				m_ctrlCanGoto_7.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(FALSE);
// 				m_fCanValue_7 = 0.0f;
// 				m_ctrlCanDataType7.EnableWindow(FALSE);
// 				m_ctrlCanCompare7.EnableWindow(FALSE);
// 				m_ctrlCanCompare7.SetItemData(0,0);
// 				m_ctrlCanCompare7.SetCurSel(0);
// 				m_ctrlCanGoto_7.EnableWindow(FALSE);
// 				m_ctrlCanGoto_7.SetItemData(0,0);
// 				m_ctrlCanGoto_7.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET7)->EnableWindow(bFlag);
			m_ctrlCanDataType7.EnableWindow(bFlag);
			m_ctrlCanCompare7.EnableWindow(bFlag);
			m_ctrlCanGoto_7.EnableWindow(bFlag);
			m_fCanValue_7 = 0.0f;
			m_ctrlCanCompare7.SetItemData(0,0);
			m_ctrlCanCompare7.SetCurSel(0);
			m_ctrlCanGoto_7.SetItemData(0,0);
			m_ctrlCanGoto_7.SetCurSel(0);
		}
	}
	else
	{
		m_ctrlCanDivision_7.SetCurSel(0);
		m_ctrlCanDivision_7.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[6] = m_ctrlCanDivision_7.GetItemData(m_ctrlCanDivision_7.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboCanDivision8() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_8.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_8.GetItemData(m_ctrlCanDivision_8.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlCanDataType8.EnableWindow(bFlag);
				m_ctrlCanCompare8.EnableWindow(bFlag);
				m_ctrlCanGoto_8.EnableWindow(FALSE);

				m_ctrlCanCompare8.SetItemData(0,0);
				m_ctrlCanCompare8.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlCanDataType8.EnableWindow(bFlag);
				m_ctrlCanCompare8.EnableWindow(bFlag);
				m_ctrlCanGoto_8.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlCanDataType8.EnableWindow(FALSE);
				m_ctrlCanCompare8.EnableWindow(FALSE);
				m_ctrlCanCompare8.SetItemData(0,0);
				m_ctrlCanCompare8.SetCurSel(0);
				m_ctrlCanGoto_8.EnableWindow(FALSE);
				m_ctrlCanGoto_8.SetItemData(0,0);
				m_ctrlCanGoto_8.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlCanDataType8.EnableWindow(bFlag);
				m_ctrlCanCompare8.EnableWindow(bFlag);
				m_ctrlCanGoto_8.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(FALSE);
// 				m_fCanValue_8 = 0.0f;
// 				m_ctrlCanDataType8.EnableWindow(FALSE);
// 				m_ctrlCanCompare8.EnableWindow(FALSE);
// 				m_ctrlCanCompare8.SetItemData(0,0);
// 				m_ctrlCanCompare8.SetCurSel(0);
// 				m_ctrlCanGoto_8.EnableWindow(FALSE);
// 				m_ctrlCanGoto_8.SetItemData(0,0);
// 				m_ctrlCanGoto_8.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET8)->EnableWindow(bFlag);
			m_ctrlCanDataType8.EnableWindow(bFlag);
			m_ctrlCanCompare8.EnableWindow(bFlag);
			m_ctrlCanGoto_8.EnableWindow(bFlag);
			m_fCanValue_8 = 0.0f;
			m_ctrlCanCompare8.SetItemData(0,0);
			m_ctrlCanCompare8.SetCurSel(0);
			m_ctrlCanGoto_8.SetItemData(0,0);
			m_ctrlCanGoto_8.SetCurSel(0);
		}
		
	}
	else
	{
		m_ctrlCanDivision_8.SetCurSel(0);
		m_ctrlCanDivision_8.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[7] = m_ctrlCanDivision_8.GetItemData(m_ctrlCanDivision_8.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboCanDivision9() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_9.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_9.GetItemData(m_ctrlCanDivision_9.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlCanDataType9.EnableWindow(bFlag);
				m_ctrlCanCompare9.EnableWindow(bFlag);
				m_ctrlCanGoto_9.EnableWindow(FALSE);

				m_ctrlCanCompare9.SetItemData(0,0);
				m_ctrlCanCompare9.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlCanDataType9.EnableWindow(bFlag);
				m_ctrlCanCompare9.EnableWindow(bFlag);
				m_ctrlCanGoto_9.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlCanDataType9.EnableWindow(FALSE);
				m_ctrlCanCompare9.EnableWindow(FALSE);
				m_ctrlCanCompare9.SetItemData(0,0);
				m_ctrlCanCompare9.SetCurSel(0);
				m_ctrlCanGoto_9.EnableWindow(FALSE);
				m_ctrlCanGoto_9.SetItemData(0,0);
				m_ctrlCanGoto_9.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlCanDataType9.EnableWindow(bFlag);
				m_ctrlCanCompare9.EnableWindow(bFlag);
				m_ctrlCanGoto_9.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(FALSE);
// 				m_fCanValue_9 = 0.0f;
// 				m_ctrlCanDataType9.EnableWindow(FALSE);
// 				m_ctrlCanCompare9.EnableWindow(FALSE);
// 				m_ctrlCanCompare9.SetItemData(0,0);
// 				m_ctrlCanCompare9.SetCurSel(0);
// 				m_ctrlCanGoto_9.EnableWindow(FALSE);
// 				m_ctrlCanGoto_9.SetItemData(0,0);
// 				m_ctrlCanGoto_9.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET9)->EnableWindow(bFlag);
			m_ctrlCanDataType9.EnableWindow(bFlag);
			m_ctrlCanCompare9.EnableWindow(bFlag);
			m_ctrlCanGoto_9.EnableWindow(bFlag);
			m_fCanValue_9 = 0.0f;
			m_ctrlCanCompare9.SetItemData(0,0);
			m_ctrlCanCompare9.SetCurSel(0);
			m_ctrlCanGoto_9.SetItemData(0,0);
			m_ctrlCanGoto_9.SetCurSel(0);
		}
		
	}
	else
	{
		m_ctrlCanDivision_9.SetCurSel(0);
		m_ctrlCanDivision_9.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[8] = m_ctrlCanDivision_9.GetItemData(m_ctrlCanDivision_9.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboCanDivision10() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlCanDivision_10.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlCanDivision_10.GetItemData(m_ctrlCanDivision_10.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 1901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlCanDataType10.EnableWindow(bFlag);
				m_ctrlCanCompare10.EnableWindow(bFlag);
				m_ctrlCanGoto_10.EnableWindow(FALSE);

				m_ctrlCanCompare10.SetItemData(0,0);
				m_ctrlCanCompare10.SetCurSel(0);
			}
			else if(((iCode >= 1001) && (iCode <= 1200)) ||
				(iCode == 1924) ||
				(iCode == 1969) ||
				(iCode == 1970))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlCanDataType10.EnableWindow(bFlag);
				m_ctrlCanCompare10.EnableWindow(bFlag);
				m_ctrlCanGoto_10.EnableWindow(bFlag);
			}
			else if((iCode >= 10010) && (iCode <= 10014))
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlCanDataType10.EnableWindow(FALSE);
				m_ctrlCanCompare10.EnableWindow(FALSE);
				m_ctrlCanCompare10.SetItemData(0,0);
				m_ctrlCanCompare10.SetCurSel(0);
				m_ctrlCanGoto_10.EnableWindow(FALSE);
				m_ctrlCanGoto_10.SetItemData(0,0);
				m_ctrlCanGoto_10.SetCurSel(0);
			}
			else
			{
				GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlCanDataType10.EnableWindow(bFlag);
				m_ctrlCanCompare10.EnableWindow(bFlag);
				m_ctrlCanGoto_10.EnableWindow(bFlag);//yulee 20181107
// 				GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(FALSE);
// 				m_fCanValue_10 = 0.0f;
// 				m_ctrlCanDataType10.EnableWindow(FALSE);
// 				m_ctrlCanCompare10.EnableWindow(FALSE);
// 				m_ctrlCanCompare10.SetItemData(0,0);
// 				m_ctrlCanCompare10.SetCurSel(0);
// 				m_ctrlCanGoto_10.EnableWindow(FALSE);
// 				m_ctrlCanGoto_10.SetItemData(0,0);
// 				m_ctrlCanGoto_10.SetCurSel(0);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_CAN_VALUE_SET10)->EnableWindow(bFlag);
			m_ctrlCanDataType10.EnableWindow(bFlag);
			m_ctrlCanCompare10.EnableWindow(bFlag);
			m_ctrlCanGoto_10.EnableWindow(bFlag);
			m_fCanValue_10 = 0.0f;
			m_ctrlCanCompare10.SetItemData(0,0);
			m_ctrlCanCompare10.SetCurSel(0);
			m_ctrlCanGoto_10.SetItemData(0,0);
			m_ctrlCanGoto_10.SetCurSel(0);
		}
		
	}
	else
	{
		m_ctrlCanDivision_10.SetCurSel(0);
		m_ctrlCanDivision_10.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->ican_function_division[9] = m_ctrlCanDivision_10.GetItemData(m_ctrlCanDivision_10.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnSelchangeCboAuxDivision1() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_1.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_1.GetItemData(m_ctrlAuxDivision_1.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);

				m_ctrlAuxCompare1.SetItemData(0,0);
				m_ctrlAuxCompare1.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
				m_fAuxValue_1 = 0.0f;
				m_ctrlAuxDataType1.EnableWindow(bFlag);
				m_ctrlAuxCompare1.EnableWindow(bFlag);
				m_ctrlAuxCompare1.SetItemData(0,0);
				m_ctrlAuxCompare1.SetCurSel(0);
				m_ctrlAuxGoto_1.EnableWindow(bFlag);
				m_ctrlAuxGoto_1.SetItemData(0,0);
				m_ctrlAuxGoto_1.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_1,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET1)->EnableWindow(bFlag);
			m_ctrlAuxDataType1.EnableWindow(bFlag);
			m_ctrlAuxCompare1.EnableWindow(bFlag);
			m_ctrlAuxGoto_1.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_1)->EnableWindow(bFlag);
			m_fAuxValue_1 = 0.0f;
			m_ctrlAuxCompare1.SetItemData(0,0);
			m_ctrlAuxCompare1.SetCurSel(0);
			m_ctrlAuxGoto_1.SetItemData(0,0);
			m_ctrlAuxGoto_1.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_1,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_1.SetCurSel(0);
		m_ctrlAuxDivision_1.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[0] = m_ctrlAuxDivision_1.GetItemData(m_ctrlAuxDivision_1.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);			
}

void CEndConditionDlg::OnSelchangeCboAuxDivision2() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_2.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_2.GetItemData(m_ctrlAuxDivision_2.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
				
				m_ctrlAuxCompare2.SetItemData(0,0);
				m_ctrlAuxCompare2.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
				m_fAuxValue_2 = 0.0f;
				m_ctrlAuxDataType2.EnableWindow(bFlag);
				m_ctrlAuxCompare2.EnableWindow(bFlag);
				m_ctrlAuxCompare2.SetItemData(0,0);
				m_ctrlAuxCompare2.SetCurSel(0);
				m_ctrlAuxGoto_2.EnableWindow(bFlag);
				m_ctrlAuxGoto_2.SetItemData(0,0);
				m_ctrlAuxGoto_2.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_2,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET2)->EnableWindow(bFlag);
			m_ctrlAuxDataType2.EnableWindow(bFlag);
			m_ctrlAuxCompare2.EnableWindow(bFlag);
			m_ctrlAuxGoto_2.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_2)->EnableWindow(bFlag);
			m_fAuxValue_2 = 0.0f;
			m_ctrlAuxCompare2.SetItemData(0,0);
			m_ctrlAuxCompare2.SetCurSel(0);
			m_ctrlAuxGoto_2.SetItemData(0,0);
			m_ctrlAuxGoto_2.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_2,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_2.SetCurSel(0);
		m_ctrlAuxDivision_2.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[1] = m_ctrlAuxDivision_2.GetItemData(m_ctrlAuxDivision_2.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboAuxDivision3() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_3.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_3.GetItemData(m_ctrlAuxDivision_3.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
				
				m_ctrlAuxCompare3.SetItemData(0,0);
				m_ctrlAuxCompare3.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
				m_fAuxValue_3 = 0.0f;
				m_ctrlAuxDataType3.EnableWindow(bFlag);
				m_ctrlAuxCompare3.EnableWindow(bFlag);
				m_ctrlAuxCompare3.SetItemData(0,0);
				m_ctrlAuxCompare3.SetCurSel(0);
				m_ctrlAuxGoto_3.EnableWindow(bFlag);
				m_ctrlAuxGoto_3.SetItemData(0,0);
				m_ctrlAuxGoto_3.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_3,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET3)->EnableWindow(bFlag);
			m_ctrlAuxDataType3.EnableWindow(bFlag);
			m_ctrlAuxCompare3.EnableWindow(bFlag);
			m_ctrlAuxGoto_3.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_3)->EnableWindow(bFlag);
			m_fAuxValue_3 = 0.0f;
			m_ctrlAuxCompare3.SetItemData(0,0);
			m_ctrlAuxCompare3.SetCurSel(0);
			m_ctrlAuxGoto_3.SetItemData(0,0);
			m_ctrlAuxGoto_3.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_3,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_3.SetCurSel(0);
		m_ctrlAuxDivision_3.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[2] = m_ctrlAuxDivision_3.GetItemData(m_ctrlAuxDivision_3.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);			
}

void CEndConditionDlg::OnSelchangeCboAuxDivision4() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_4.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_4.GetItemData(m_ctrlAuxDivision_4.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);

				m_ctrlAuxCompare4.SetItemData(0,0);
				m_ctrlAuxCompare4.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
				m_fAuxValue_4 = 0.0f;
				m_ctrlAuxDataType4.EnableWindow(bFlag);
				m_ctrlAuxCompare4.EnableWindow(bFlag);
				m_ctrlAuxCompare4.SetItemData(0,0);
				m_ctrlAuxCompare4.SetCurSel(0);
				m_ctrlAuxGoto_4.EnableWindow(bFlag);
				m_ctrlAuxGoto_4.SetItemData(0,0);
				m_ctrlAuxGoto_4.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_4,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET4)->EnableWindow(bFlag);
			m_ctrlAuxDataType4.EnableWindow(bFlag);
			m_ctrlAuxCompare4.EnableWindow(bFlag);
			m_ctrlAuxGoto_4.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_4)->EnableWindow(bFlag);
			m_fAuxValue_4 = 0.0f;
			m_ctrlAuxCompare4.SetItemData(0,0);
			m_ctrlAuxCompare4.SetCurSel(0);
			m_ctrlAuxGoto_4.SetItemData(0,0);
			m_ctrlAuxGoto_4.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_4,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_4.SetCurSel(0);
		m_ctrlAuxDivision_4.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[3] = m_ctrlAuxDivision_4.GetItemData(m_ctrlAuxDivision_4.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboAuxDivision5() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_5.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_5.GetItemData(m_ctrlAuxDivision_5.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
		
				m_ctrlAuxCompare5.SetItemData(0,0);
				m_ctrlAuxCompare5.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
				m_fAuxValue_5 = 0.0f;
				m_ctrlAuxDataType5.EnableWindow(bFlag);
				m_ctrlAuxCompare5.EnableWindow(bFlag);
				m_ctrlAuxCompare5.SetItemData(0,0);
				m_ctrlAuxCompare5.SetCurSel(0);
				m_ctrlAuxGoto_5.EnableWindow(bFlag);
				m_ctrlAuxGoto_5.SetItemData(0,0);
				m_ctrlAuxGoto_5.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_5,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET5)->EnableWindow(bFlag);
			m_ctrlAuxDataType5.EnableWindow(bFlag);
			m_ctrlAuxCompare5.EnableWindow(bFlag);
			m_ctrlAuxGoto_5.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_5)->EnableWindow(bFlag);
			m_fAuxValue_5 = 0.0f;
			m_ctrlAuxCompare5.SetItemData(0,0);
			m_ctrlAuxCompare5.SetCurSel(0);
			m_ctrlAuxGoto_5.SetItemData(0,0);
			m_ctrlAuxGoto_5.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_5,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_5.SetCurSel(0);
		m_ctrlAuxDivision_5.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[4] = m_ctrlAuxDivision_5.GetItemData(m_ctrlAuxDivision_5.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboAuxDivision6() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_6.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_6.GetItemData(m_ctrlAuxDivision_6.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
				
				m_ctrlAuxCompare6.SetItemData(0,0);
				m_ctrlAuxCompare6.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
				m_fAuxValue_6 = 0.0f;
				m_ctrlAuxDataType6.EnableWindow(bFlag);
				m_ctrlAuxCompare6.EnableWindow(bFlag);
				m_ctrlAuxCompare6.SetItemData(0,0);
				m_ctrlAuxCompare6.SetCurSel(0);
				m_ctrlAuxGoto_6.EnableWindow(bFlag);
				m_ctrlAuxGoto_6.SetItemData(0,0);
				m_ctrlAuxGoto_6.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_6,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET6)->EnableWindow(bFlag);
			m_ctrlAuxDataType6.EnableWindow(bFlag);
			m_ctrlAuxCompare6.EnableWindow(bFlag);
			m_ctrlAuxGoto_6.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_6)->EnableWindow(bFlag);
			m_fAuxValue_6 = 0.0f;
			m_ctrlAuxCompare6.SetItemData(0,0);
			m_ctrlAuxCompare6.SetCurSel(0);
			m_ctrlAuxGoto_6.SetItemData(0,0);
			m_ctrlAuxGoto_6.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_6,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_6.SetCurSel(0);
		m_ctrlAuxDivision_6.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[5] = m_ctrlAuxDivision_6.GetItemData(m_ctrlAuxDivision_6.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboAuxDivision7() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_7.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_7.GetItemData(m_ctrlAuxDivision_7.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);

				m_ctrlAuxCompare7.SetItemData(0,0);
				m_ctrlAuxCompare7.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
				m_fAuxValue_7 = 0.0f;
				m_ctrlAuxDataType7.EnableWindow(bFlag);
				m_ctrlAuxCompare7.EnableWindow(bFlag);
				m_ctrlAuxCompare7.SetItemData(0,0);
				m_ctrlAuxCompare7.SetCurSel(0);
				m_ctrlAuxGoto_7.EnableWindow(bFlag);
				m_ctrlAuxGoto_7.SetItemData(0,0);
				m_ctrlAuxGoto_7.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_7,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET7)->EnableWindow(bFlag);
			m_ctrlAuxDataType7.EnableWindow(bFlag);
			m_ctrlAuxCompare7.EnableWindow(bFlag);
			m_ctrlAuxGoto_7.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_7)->EnableWindow(bFlag);
			m_fAuxValue_7 = 0.0f;
			m_ctrlAuxCompare7.SetItemData(0,0);
			m_ctrlAuxCompare7.SetCurSel(0);
			m_ctrlAuxGoto_7.SetItemData(0,0);
			m_ctrlAuxGoto_7.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_7,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_7.SetCurSel(0);
		m_ctrlAuxDivision_7.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[6] = m_ctrlAuxDivision_7.GetItemData(m_ctrlAuxDivision_7.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboAuxDivision8() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_8.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_8.GetItemData(m_ctrlAuxDivision_8.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);

				m_ctrlAuxCompare8.SetItemData(0,0);
				m_ctrlAuxCompare8.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
				m_fAuxValue_8 = 0.0f;
				m_ctrlAuxDataType8.EnableWindow(bFlag);
				m_ctrlAuxCompare8.EnableWindow(bFlag);
				m_ctrlAuxCompare8.SetItemData(0,0);
				m_ctrlAuxCompare8.SetCurSel(0);
				m_ctrlAuxGoto_8.EnableWindow(bFlag);
				m_ctrlAuxGoto_8.SetItemData(0,0);
				m_ctrlAuxGoto_8.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_8,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET8)->EnableWindow(bFlag);
			m_ctrlAuxDataType8.EnableWindow(bFlag);
			m_ctrlAuxCompare8.EnableWindow(bFlag);
			m_ctrlAuxGoto_8.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_8)->EnableWindow(bFlag);
			m_fAuxValue_8 = 0.0f;
			m_ctrlAuxCompare8.SetItemData(0,0);
			m_ctrlAuxCompare8.SetCurSel(0);
			m_ctrlAuxGoto_8.SetItemData(0,0);
			m_ctrlAuxGoto_8.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_8,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_8.SetCurSel(0);
		m_ctrlAuxDivision_8.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[7] = m_ctrlAuxDivision_8.GetItemData(m_ctrlAuxDivision_8.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboAuxDivision9() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_9.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_9.GetItemData(m_ctrlAuxDivision_9.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
				
				m_ctrlAuxCompare9.SetItemData(0,0);
				m_ctrlAuxCompare9.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
				m_fAuxValue_9 = 0.0f;
				m_ctrlAuxDataType9.EnableWindow(bFlag);
				m_ctrlAuxCompare9.EnableWindow(bFlag);
				m_ctrlAuxCompare9.SetItemData(0,0);
				m_ctrlAuxCompare9.SetCurSel(0);
				m_ctrlAuxGoto_9.EnableWindow(bFlag);
				m_ctrlAuxGoto_9.SetItemData(0,0);
				m_ctrlAuxGoto_9.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_9,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET9)->EnableWindow(bFlag);
			m_ctrlAuxDataType9.EnableWindow(bFlag);
			m_ctrlAuxCompare9.EnableWindow(bFlag);
			m_ctrlAuxGoto_9.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_9)->EnableWindow(bFlag);
			m_fAuxValue_9 = 0.0f;
			m_ctrlAuxCompare9.SetItemData(0,0);
			m_ctrlAuxCompare9.SetCurSel(0);
			m_ctrlAuxGoto_9.SetItemData(0,0);
			m_ctrlAuxGoto_9.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_9,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_9.SetCurSel(0);
		m_ctrlAuxDivision_9.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[8] = m_ctrlAuxDivision_9.GetItemData(m_ctrlAuxDivision_9.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboAuxDivision10() 
{
	UpdateData();
	int iCode;
	BOOL bFlag;
	if(m_ctrlAuxDivision_10.GetCurSel() != LB_ERR)
	{
		iCode = m_ctrlAuxDivision_10.GetItemData(m_ctrlAuxDivision_10.GetCurSel());
		if (iCode == 0)
			bFlag = FALSE;
		else
			bFlag = TRUE;
		if(bFlag == TRUE)
		{
			if(iCode == 2901) //yulee 20181009-2
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
				
				m_ctrlAuxCompare10.SetItemData(0,0);
				m_ctrlAuxCompare10.SetCurSel(0);
			}
			else if((iCode >= 2001) && (iCode <= 2900))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
			}
			else if((iCode >= 20001) && (iCode <= 20006))
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.EnableWindow(bFlag);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
			}
			else
			{
				GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
				m_fAuxValue_10 = 0.0f;
				m_ctrlAuxDataType10.EnableWindow(bFlag);
				m_ctrlAuxCompare10.EnableWindow(bFlag);
				m_ctrlAuxCompare10.SetItemData(0,0);
				m_ctrlAuxCompare10.SetCurSel(0);
				m_ctrlAuxGoto_10.EnableWindow(bFlag);
				m_ctrlAuxGoto_10.SetItemData(0,0);
				m_ctrlAuxGoto_10.SetCurSel(0);
				GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
				CString tmpStr;
				tmpStr.Format("0");
				SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_10,tmpStr);
			}
		}
		else
		{
			GetDlgItem(IDC_EDIT_AUX_VALUE_SET10)->EnableWindow(bFlag);
			m_ctrlAuxDataType10.EnableWindow(bFlag);
			m_ctrlAuxCompare10.EnableWindow(bFlag);
			m_ctrlAuxGoto_10.EnableWindow(bFlag);
			GetDlgItem(IDC_EDIT_AUX_CONTI_TIME_10)->EnableWindow(bFlag);
			m_fAuxValue_10 = 0.0f;
			m_ctrlAuxCompare10.SetItemData(0,0);
			m_ctrlAuxCompare10.SetCurSel(0);
			m_ctrlAuxGoto_10.SetItemData(0,0);
			m_ctrlAuxGoto_10.SetCurSel(0);
			CString tmpStr;
			tmpStr.Format("0");
			SetDlgItemText(IDC_EDIT_AUX_CONTI_TIME_10,tmpStr);
		}
	}
	else
	{
		m_ctrlAuxDivision_10.SetCurSel(0);
		m_ctrlAuxDivision_10.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->iaux_function_division[9] = m_ctrlAuxDivision_10.GetItemData(m_ctrlAuxDivision_10.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);				
}

void CEndConditionDlg::OnSelchangeCboLoaderValueItem() 
{
	UpdateData();
	int nSel = m_ctrlCboLoaderValueItem.GetItemData(m_ctrlCboLoaderValueItem.GetCurSel());
	if( nSel == 0 || nSel == 2)
	{
		m_ctrlEditfValueLoader = 0.0f;

		GetDlgItem(IDC_CBO_LOADER_MODE_VALUE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_LOADER_SET_VALUE)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_CBO_LOADER_MODE_VALUE)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_LOADER_SET_VALUE)->EnableWindow(TRUE);
	}	
	m_ctrlCboLoaderMode.SetCurSel(0);
	UpdateData(FALSE);	
}

void CEndConditionDlg::OnSelchangeCboLoaderModeValue() 
{
	UpdateData();
	int nSel = m_ctrlCboLoaderMode.GetItemData(m_ctrlCboLoaderMode.GetCurSel());
	switch (nSel)
	{
	case 0: 
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("W");
		break;
	case 1:
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("A");
		break;
	case 2: 
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("V");
		break;
	case 3:
		GetDlgItem(IDC_LAB_LOADER_UNIT)->SetWindowText("Ω");
		break;
	}
	UpdateData(FALSE);	
}

void CEndConditionDlg::OnDatetimechangeWaitDatetimepicker(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UpdateData(FALSE);
	COleDateTime waitTime;
	ULONG ulTemp=0;
	m_ctrlWaitTime.GetTime(waitTime);
	ulTemp = (waitTime.GetHour()*3600+waitTime.GetMinute()*60+waitTime.GetSecond())*100;		
// 	ulTemp += (m_lEndMSec/10);
// 	if (ulTemp > 0 || m_nEndDay > 0) m_ctrlCboEndTime.EnableWindow(TRUE);
// 	else m_ctrlCboEndTime.EnableWindow(FALSE);
	
	*pResult = 0;
}

void CEndConditionDlg::OnChangeWaitDayEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	UpdateData();
	
	//long으로 표현 가능한 날짜 (sec×100 된 숫자)
	//21474836.47sec => 248.55 Day 
	//최대 입력범위를 199일 23:59:59 으로 제한 
	if(m_nWaitTimeDay > 1095)	m_nWaitTimeDay = 1095;	//ljb 20131212 add 3년으로 제한 365 * 3 = 1095
	
	UpdateData(FALSE);
	
	
}

void CEndConditionDlg::OnEditchangeCboCanCheck() 
{
	// TODO: Add your control notification handler code here
	
}

void CEndConditionDlg::OnSelchangeCboCanCheck() 
{
	//20170328 add
	UpdateData();
	int iSelect;
	//BOOL bFlag;
	if(m_ctrlCboCanCheck.GetCurSel() != LB_ERR)
	{
		iSelect = m_ctrlCboCanCheck.GetItemData(m_ctrlCboCanCheck.GetCurSel());
	}
	else
	{
		m_ctrlCboCanCheck.SetCurSel(1);
		m_ctrlCboCanCheck.SetFocus();
	}
	
	STEP *pStep = (STEP*)m_pDoc->m_apStep[m_nStepNo-1];
	pStep->nCanCheckMode = m_ctrlCboCanCheck.GetItemData(m_ctrlCboCanCheck.GetCurSel());
	ASSERT(pStep);	
	
	UpdateData(FALSE);		
}

void CEndConditionDlg::OnCheckZeroVoltage()  //yulee 20191017 OverChargeDischarger Mark
{
	UpdateData();
// 	int nPreClickNum = 0, nPresClickNum = 0;
// 	
// 
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "ZeroVoltEndCondClicked", 1);  //yulee 20190904
	
	if(m_bZeroVoltage.GetCheck() == TRUE)
	{
		GetDlgItem(IDC_END_VTG_L)->SetWindowText(0);
		AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "ZeroVoltEndCond", m_bZeroVoltage.GetCheck());
	}
	else if(m_bZeroVoltage.GetCheck() == FALSE)
	{
		GetDlgItem(IDC_END_VTG_L)->SetWindowText(0);
		AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION , "ZeroVoltEndCond", m_bZeroVoltage.GetCheck());
	}
	UpdateData(FALSE);
}


void CEndConditionDlg::OnSelchangeCboCanCompare1() 
{
	UpdateData();
	// TODO: Add your control notification handler code here
	int nCanComp;
	nCanComp = m_ctrlCanCompare1.GetCurSel();

	if(m_ctrlCanDivision_1.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_1.GetItemData(m_ctrlCanDivision_1.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare1_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);

				m_ctrlCanCompare1.SetItemData(0,0);
				m_ctrlCanCompare1.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}		

void CEndConditionDlg::OnSelchangeCboCanCompare2() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nCanComp;
	nCanComp = m_ctrlCanCompare2.GetCurSel();
	
	if(m_ctrlCanDivision_2.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_2.GetItemData(m_ctrlCanDivision_2.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare2_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare2.SetItemData(0,0);
				m_ctrlCanCompare2.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare3() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nCanComp;
	nCanComp = m_ctrlCanCompare3.GetCurSel();
	
	if(m_ctrlCanDivision_3.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_3.GetItemData(m_ctrlCanDivision_3.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare3_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare3.SetItemData(0,0);
				m_ctrlCanCompare3.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare4() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nCanComp;
	nCanComp = m_ctrlCanCompare4.GetCurSel();
	
	if(m_ctrlCanDivision_4.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_4.GetItemData(m_ctrlCanDivision_4.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare4_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare4.SetItemData(0,0);
				m_ctrlCanCompare4.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare5() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nCanComp;
	nCanComp = m_ctrlCanCompare5.GetCurSel();
	
	if(m_ctrlCanDivision_5.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_5.GetItemData(m_ctrlCanDivision_5.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare5_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare5.SetItemData(0,0);
				m_ctrlCanCompare5.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare6() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nCanComp;
	nCanComp = m_ctrlCanCompare6.GetCurSel();
	
	if(m_ctrlCanDivision_6.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_6.GetItemData(m_ctrlCanDivision_6.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare6_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare6.SetItemData(0,0);
				m_ctrlCanCompare6.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare7() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nCanComp;
	nCanComp = m_ctrlCanCompare7.GetCurSel();
	
	if(m_ctrlCanDivision_7.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_7.GetItemData(m_ctrlCanDivision_7.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare7_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare7.SetItemData(0,0);
				m_ctrlCanCompare7.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare8() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlCanCompare8.GetCurSel();
	
	if(m_ctrlCanDivision_8.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_8.GetItemData(m_ctrlCanDivision_8.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare8_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare8.SetItemData(0,0);
				m_ctrlCanCompare8.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare9() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlCanCompare9.GetCurSel();
	
	if(m_ctrlCanDivision_9.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_9.GetItemData(m_ctrlCanDivision_9.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare9_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare9.SetItemData(0,0);
				m_ctrlCanCompare9.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboCanCompare10() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlCanCompare10.GetCurSel();
	
	if(m_ctrlCanDivision_10.GetCurSel() != LB_ERR)
	{
		if(m_ctrlCanDivision_10.GetItemData(m_ctrlCanDivision_10.GetCurSel()) == 1901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboCanCompare3_msg10","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlCanCompare10.SetItemData(0,0);
				m_ctrlCanCompare10.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare1() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare1.GetCurSel();
	
	if(m_ctrlAuxDivision_1.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_1.GetItemData(m_ctrlAuxDivision_1.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare1_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare1.SetItemData(0,0);
				m_ctrlAuxCompare1.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}	
}

void CEndConditionDlg::OnSelchangeCboAuxCompare2() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare2.GetCurSel();
	
	if(m_ctrlAuxDivision_2.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_2.GetItemData(m_ctrlAuxDivision_2.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare2_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare2.SetItemData(0,0);
				m_ctrlAuxCompare2.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare3() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp =m_ctrlAuxCompare3.GetCurSel();
	
	if(m_ctrlAuxDivision_3.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_3.GetItemData(m_ctrlAuxDivision_3.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare3_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare3.SetItemData(0,0);
				m_ctrlAuxCompare3.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare4() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare4.GetCurSel();
	
	if(m_ctrlAuxDivision_4.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_4.GetItemData(m_ctrlAuxDivision_4.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare4_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare4.SetItemData(0,0);
				m_ctrlAuxCompare4.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare5() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare5.GetCurSel();
	
	if(m_ctrlAuxDivision_5.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_5.GetItemData(m_ctrlAuxDivision_5.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare5_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare5.SetItemData(0,0);
				m_ctrlAuxCompare5.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare6() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare6.GetItemData(m_ctrlAuxCompare6.GetCurSel());
	
	if(m_ctrlAuxDivision_6.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_6.GetItemData(m_ctrlAuxDivision_6.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare6_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare6.SetItemData(0,0);
				m_ctrlAuxCompare6.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare7() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare7.GetCurSel();
	
	if(m_ctrlAuxDivision_7.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_7.GetItemData(m_ctrlAuxDivision_7.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare7_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare7.SetItemData(0,0);
				m_ctrlAuxCompare7.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare8() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare8.GetCurSel();
	
	if(m_ctrlAuxDivision_8.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_8.GetItemData(m_ctrlAuxDivision_8.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare8_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare8.SetItemData(0,0);
				m_ctrlAuxCompare8.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare9() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare9.GetCurSel();
	
	if(m_ctrlAuxDivision_9.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_9.GetItemData(m_ctrlAuxDivision_9.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare9_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare9.SetItemData(0,0);
				m_ctrlAuxCompare9.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}

void CEndConditionDlg::OnSelchangeCboAuxCompare10() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	int nCanComp;
	nCanComp = m_ctrlAuxCompare10.GetItemData(m_ctrlAuxCompare10.GetCurSel());
	
	if(m_ctrlAuxDivision_10.GetCurSel() != LB_ERR)
	{
		if(m_ctrlAuxDivision_10.GetItemData(m_ctrlAuxDivision_10.GetCurSel()) == 2901)
		{
			if((nCanComp == 3) || (nCanComp == 6))
			{
				CString tmpStr;
				//tmpStr.Format("Cell_CV에서는 == 또는 != 사용이 불가합니다. 다시 설정해주십시오.");
				tmpStr.Format(Fun_FindMsg("EndConditionDlg_OnSelchangeCboAuxCompare10_msg1","IDD_END_COND_DLG"));//&&
				AfxMessageBox(tmpStr);
				
				m_ctrlAuxCompare10.SetItemData(0,0);
				m_ctrlAuxCompare10.SetCurSel(0);
				UpdateData(FALSE);
			}
		}
	}
}
