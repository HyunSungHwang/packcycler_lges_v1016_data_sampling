// Dlg_CellBal.cpp : implementation file
//
//$1013

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "Dlg_CellBal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBal dialog


Dlg_CellBal::Dlg_CellBal(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_CellBal::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_CellBal::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_CellBal::IDD3):
	(Dlg_CellBal::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_CellBal)
	//}}AFX_DATA_INIT

	m_pDoc=NULL;
	m_pStep=NULL;
	m_pParent=pParent;
}


void Dlg_CellBal::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_CellBal)
	DDX_Control(pDX, IDC_EDIT_CELLBAL_WORKVOLTUPPPER, m_EditCellBALWorkVoltUpper);
	DDX_Control(pDX, IDC_CHK_CELLBAL_ACTIVETIME, m_Chk_CellBALActiveTime);
	DDX_Control(pDX, IDC_EDIT_CELLBAL_ACTIVETIME, m_EditCellBALActiveTime);
	DDX_Control(pDX, IDC_CHK_AUTONEXTSTEP, m_ChkAutoNextStep);
	DDX_Control(pDX, IDC_CBO_CELLBAL_SIGNALSENSTIME, m_CboCellBALSignalSensTime);
	DDX_Control(pDX, IDC_CBO_CELLBAL_AUTOSTOPTIME, m_CboCellBALAutoStopTime);
	DDX_Control(pDX, IDC_CBO_CELLBAL_CIRCUITRES, m_CboCellBALCircuitRes);
	DDX_Control(pDX, IDC_EDIT_CELLBAL_WORKVOLT, m_EditCellBALWorkVolt);
	DDX_Control(pDX, IDC_EDIT_CELLBAL_STARTVOLT, m_EditCellBALStartVolt);
	DDX_Control(pDX, IDC_EDIT_CELLBAL_ENDVOLT, m_EditCellBALEndVolt);
	DDX_Control(pDX, IDC_STATIC_STEPNO, m_StaticStepNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_CellBal, CDialog)
	//{{AFX_MSG_MAP(Dlg_CellBal)
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_EN_CHANGE(IDC_EDIT_CELLBAL_WORKVOLT, OnChangeEditCellbalWorkVolt)
	ON_EN_CHANGE(IDC_EDIT_CELLBAL_STARTVOLT, OnChangeEditCellbalStartVolt)
	ON_EN_CHANGE(IDC_EDIT_CELLBAL_ENDVOLT, OnChangeEditCellbalEndVolt)
	ON_CBN_SELCHANGE(IDC_CBO_CELLBAL_CIRCUITRES, OnSelchangeCboCellbalCircuitres)
	ON_CBN_EDITCHANGE(IDC_CBO_CELLBAL_AUTOSTOPTIME, OnEditchangeCboCellbalAutostoptime)
	ON_CBN_SELCHANGE(IDC_CBO_CELLBAL_AUTOSTOPTIME, OnSelchangeCboCellbalAutostoptime)
	ON_CBN_EDITUPDATE(IDC_CBO_CELLBAL_SIGNALSENSTIME, OnEditupdateCboCellbalSignalsenstime)
	ON_CBN_SELCHANGE(IDC_CBO_CELLBAL_SIGNALSENSTIME, OnSelchangeCboCellbalSignalsenstime)
	ON_BN_CLICKED(IDC_CHK_AUTONEXTSTEP, OnChkAutonextstep)
	ON_EN_UPDATE(IDC_EDIT_CELLBAL_ACTIVETIME, OnUpdateEditCellbalActivetime)
	ON_EN_CHANGE(IDC_EDIT_CELLBAL_ACTIVETIME, OnChangeEditCellbalActivetime)
	ON_EN_KILLFOCUS(IDC_EDIT_CELLBAL_ACTIVETIME, OnKillfocusEditCellbalActivetime)
	ON_CBN_EDITCHANGE(IDC_CBO_CELLBAL_CIRCUITRES, OnEditchangeCboCellbalCircuitres)
	ON_CBN_KILLFOCUS(IDC_CBO_CELLBAL_CIRCUITRES, OnKillfocusCboCellbalCircuitres)
	ON_EN_CHANGE(IDC_EDIT_CELLBAL_WORKVOLTUPPPER, OnChangeEditCellbalWorkvoltuppper)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBal message handlers
BOOL Dlg_CellBal::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_StaticStepNo.SetFontAlign(DT_LEFT);

	//m_CboCellBALCircuitRes.AddString(_T("사용안함"));
	m_CboCellBALCircuitRes.AddString(Fun_FindMsg("Dlg_CellBal_OnInitDialog_msg1","DLG_CELLBAL"));//&&
	m_CboCellBALCircuitRes.AddString(_T("20"));
	m_CboCellBALCircuitRes.AddString(_T("40"));
	m_CboCellBALCircuitRes.AddString(_T("60"));
	m_CboCellBALCircuitRes.SetCurSel(0);

	//m_CboCellBALSignalSensTime.AddString(_T("사용안함"));
	//m_CboCellBALSignalSensTime.AddString(_T("15"));
	//m_CboCellBALSignalSensTime.SetCurSel(0);
	
	//m_CboCellBALAutoStopTime.AddString(_T("바로 멈춤"));
	m_CboCellBALAutoStopTime.AddString(Fun_FindMsg("Dlg_CellBal_OnInitDialog_msg2","DLG_CELLBAL")); //&&
	//m_CboCellBALAutoStopTime.AddString(_T("1분"));
	m_CboCellBALAutoStopTime.AddString(Fun_FindMsg("Dlg_CellBal_OnInitDialog_msg3","DLG_CELLBAL"));//&&
	//m_CboCellBALAutoStopTime.AddString(_T("2분"));
	m_CboCellBALAutoStopTime.AddString(Fun_FindMsg("Dlg_CellBal_OnInitDialog_msg4","DLG_CELLBAL"));//&&
	//m_CboCellBALAutoStopTime.AddString(_T("3분"));
	m_CboCellBALAutoStopTime.AddString(Fun_FindMsg("Dlg_CellBal_OnInitDialog_msg5","DLG_CELLBAL")); //&&
	m_CboCellBALAutoStopTime.SetCurSel(0);

	m_ChkAutoNextStep.SetCheck(FALSE);

	m_EditCellBALActiveTime.SetWindowText("3");
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_CellBal::OnOK() 
{
	//CDialog::OnOK();
}

void Dlg_CellBal::OnCancel() 
{
	//CDialog::OnCancel();
}

void Dlg_CellBal::OnClose() 
{	
	//CDialog::OnClose();
}

BOOL Dlg_CellBal::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam==VK_RETURN ||
			pMsg->wParam==VK_ESCAPE )
		{
			ShowWindow(SW_HIDE);
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_CellBal::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if(!m_pDoc) return;	
	if(m_nStepNo < 1) return;
	
	CString strTemp;

	if(bShow==TRUE)
	{
		strTemp.Format(_T("Step : %3d"), m_nStepNo);
		m_StaticStepNo.SetText(strTemp);

		onSetValue();
	}
	else
	{
		if(m_pParent)
		{
			m_pParent->PostMessage(WM_CELLBAL_DLG_CLOSE, (WPARAM)m_nStepNo, (LPARAM) this);			
		}		
	}	
}

void Dlg_CellBal::onSetValue() 
{
	if(!m_pStep) return;
			
	onSetObject(0,FALSE);

	CString strTemp;
	strTemp.Format(_T("%d"),m_pStep->nCellBal_CircuitRes);
	int nCurSel=theApp.win_CboGetFind(&m_CboCellBALCircuitRes,strTemp);
	//if(nCurSel<1) nCurSel=0; //yulee 20190207
	//m_CboCellBALCircuitRes.SetCurSel(nCurSel);

	strTemp.Format(_T("%d"),m_pStep->nCellBal_CircuitRes); //yulee 20190207
	m_CboCellBALCircuitRes.SetWindowText(strTemp);

	int nValue=atoi(theApp.win_GetText(&m_CboCellBALCircuitRes));

	if((nCurSel>0) || (nValue>0)) //yulee 20190207
	{
		onSetObject(0,TRUE);
	}

	strTemp.Format(_T("%f"),(float)m_pStep->nCellBal_WorkVoltUpper/1000.f);//mV=>V
	m_EditCellBALWorkVoltUpper.SetWindowText(strTemp);

	strTemp.Format(_T("%f"),(float)m_pStep->nCellBal_WorkVolt/1000.f);//mV=>V
	m_EditCellBALWorkVolt.SetWindowText(strTemp);

	strTemp.Format(_T("%d"),m_pStep->nCellBal_StartVolt);
	m_EditCellBALStartVolt.SetWindowText(strTemp);

	strTemp.Format(_T("%d"),m_pStep->nCellBal_EndVolt);
	m_EditCellBALEndVolt.SetWindowText(strTemp);

	
	strTemp.Format(_T("%d"),m_pStep->nCellBal_ActiveTime);
	if(atoi(strTemp) < 3)
	{
		strTemp.Format("3");
	}

	if(atoi(strTemp) >= 10)
	{
		m_Chk_CellBALActiveTime.SetCheck(TRUE);
	}
	else
	{
		m_Chk_CellBALActiveTime.SetCheck(FALSE);
	}
	m_EditCellBALActiveTime.SetWindowText(strTemp);


//	}

	int nMinite=m_pStep->nCellBal_AutoStopTime/60;
	int nSec=m_pStep->nCellBal_AutoStopTime%60;

	if(nSec==0)
	{
		//strTemp.Format(_T("%d분"),nMinite);
		strTemp.Format(Fun_FindMsg("Dlg_CellBal_onSetValue_msg1","DLG_CELLBAL"),nMinite);//&&
		nCurSel=theApp.win_CboGetFind(&m_CboCellBALAutoStopTime,strTemp);
		m_CboCellBALAutoStopTime.SetCurSel(nCurSel);
	}
	else
	{
		strTemp.Format(_T("%d"),m_pStep->nCellBal_AutoStopTime);
		m_CboCellBALAutoStopTime.SetWindowText(strTemp);
	}

	m_ChkAutoNextStep.SetCheck(m_pStep->nCellBal_AutoNextStep);
}

void Dlg_CellBal::onSetObject(int iType,BOOL bflag) 
{
	m_EditCellBALWorkVolt.EnableWindow(bflag);
	m_EditCellBALWorkVoltUpper.EnableWindow(bflag);
	m_EditCellBALStartVolt.EnableWindow(bflag);
	m_EditCellBALEndVolt.EnableWindow(bflag);
	//m_CboCellBALSignalSensTime.EnableWindow(bflag);
	m_EditCellBALActiveTime.EnableWindow(bflag);
	m_CboCellBALAutoStopTime.EnableWindow(bflag);	
	m_ChkAutoNextStep.EnableWindow(bflag);

	if(iType==10)
	{
		m_EditCellBALWorkVolt.SetWindowText(_T("0.00"));
		m_EditCellBALWorkVoltUpper.SetWindowText(_T("0.00"));
		m_EditCellBALStartVolt.SetWindowText(_T("0"));
		m_EditCellBALEndVolt.SetWindowText(_T("0"));
		//m_CboCellBALSignalSensTime.SetCurSel(0);
		m_EditCellBALActiveTime.SetWindowText("3");
		m_Chk_CellBALActiveTime.SetCheck(FALSE);
		m_CboCellBALAutoStopTime.SetCurSel(0);
		m_ChkAutoNextStep.SetCheck(FALSE);

		m_pStep->nCellBal_WorkVoltUpper=0;
		m_pStep->nCellBal_WorkVolt=0;
		m_pStep->nCellBal_StartVolt=0;
		m_pStep->nCellBal_EndVolt=0;
		m_pStep->nCellBal_ActiveTime=0;
		m_pStep->nCellBal_AutoStopTime=0;
		m_pStep->nCellBal_AutoNextStep=FALSE;
	}
}

void Dlg_CellBal::OnSelchangeCboCellbalCircuitres() 
{
	UpdateData(TRUE);
	
	int nValue=0;
	int nCurSel=m_CboCellBALCircuitRes.GetCurSel();
	//nValue=atoi(theApp.win_GetText(&m_CboCellBALCircuitRes));
	CString tmpStr;
	m_CboCellBALCircuitRes.GetLBText(nCurSel,tmpStr);
	nValue = atoi(tmpStr);

	if((nCurSel>0) || ((nValue >= 10)&&(nValue <= 100))) //yulee 20190207
	{
		//nValue=atoi(theApp.win_CboGetString(&m_CboCellBALCircuitRes));
		m_pStep->nCellBal_CircuitRes=nValue;
		onSetObject(0,TRUE);
		onDefault();
	}
	else
	{
		onSetObject(10,FALSE);
	}

	if((m_pStep)&&(nValue >= 10)&&(nValue <= 100))
	{
		m_pStep->nCellBal_CircuitRes=nValue;
	}
	
	UpdateData(FALSE);
	
}

void Dlg_CellBal::OnChangeEditCellbalWorkVolt() 
{
	UpdateData(TRUE);
	
	CString strValue;
	m_EditCellBALWorkVolt.GetWindowText(strValue);
	if(m_pStep)
	{
		m_pStep->nCellBal_WorkVolt=(int)(atof(strValue)*1000);//V=>mV
	}
	
	UpdateData(FALSE);
	
}

void Dlg_CellBal::OnChangeEditCellbalStartVolt() 
{
	UpdateData(TRUE);
	
	CString strValue;
	m_EditCellBALStartVolt.GetWindowText(strValue);
	if(m_pStep)
	{
		m_pStep->nCellBal_StartVolt=atoi(strValue);
	}
	
	UpdateData(FALSE);
}

void Dlg_CellBal::OnChangeEditCellbalEndVolt() 
{
	UpdateData(TRUE);
	
	CString strValue;
	m_EditCellBALEndVolt.GetWindowText(strValue);
	if(m_pStep)
	{
		m_pStep->nCellBal_EndVolt=atoi(strValue);
	}
	
	UpdateData(FALSE);
}

void Dlg_CellBal::OnEditchangeCboCellbalAutostoptime() 
{
	UpdateData(TRUE);
	
	int nValue=atoi(theApp.win_GetText(&m_CboCellBALAutoStopTime));
	if(m_pStep)
	{
		m_pStep->nCellBal_AutoStopTime=nValue;
	}
	
	UpdateData(FALSE);	
}

void Dlg_CellBal::OnSelchangeCboCellbalAutostoptime() 
{
	UpdateData(TRUE);
	
	int nValue=0;
	int nCurSel=m_CboCellBALAutoStopTime.GetCurSel();
	if(nCurSel>0)
	{
		CString strValue=theApp.win_CboGetString(&m_CboCellBALAutoStopTime);
		if(strValue.Find(_T("분"))>-1)
		//if(strValue.Find(Fun_FindMsg("Dlg_CellBal_OnSelchangeCboCellbalAutostoptime_msg1"))>-1) //&&
		{
			strValue.Replace(_T("분"),_T(""));
			//strValue.Replace(Fun_FindMsg("Dlg_CellBal_OnSelchangeCboCellbalAutostoptime_msg1"),_T(""));//&&
			nValue=atoi(strValue)*60;
		}
		else 
		{
			nValue=atoi(strValue);//SEC
		}
	}
	if(m_pStep)
	{
		m_pStep->nCellBal_AutoStopTime=nValue;
	}
	
	UpdateData(FALSE);	
}


void Dlg_CellBal::OnEditupdateCboCellbalSignalsenstime() 
{
	UpdateData(TRUE);
	
	int nValue=atoi(theApp.win_GetText(&m_CboCellBALSignalSensTime));
	if(m_pStep)
	{
		//m_pStep->nCellBal_ActiveTime=nValue;
	}
	
	UpdateData(FALSE);	
}

void Dlg_CellBal::OnSelchangeCboCellbalSignalsenstime() 
{
	int nValue=0;
	int nCurSel=m_CboCellBALSignalSensTime.GetCurSel();
	if(nCurSel>0)
	{
		CString strValue=theApp.win_CboGetString(&m_CboCellBALSignalSensTime);
		nValue=atoi(strValue);//SEC
	}
	if(m_pStep)
	{
		//m_pStep->nCellBal_ActiveTime=nValue;
	}
}

void Dlg_CellBal::OnChkAutonextstep() 
{
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkAutoNextStep.GetCheck();
	if(m_pStep)
	{
		m_pStep->nCellBal_AutoNextStep=bflag;
	}
	
	UpdateData(FALSE);	
}

void Dlg_CellBal::onDefault() 
{
	if(m_pStep)
	{
		//m_pStep->nCellBal_CircuitRes=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_CIRCUITRES", 10);
		m_pStep->nCellBal_WorkVoltUpper=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_WORKVOLTUPPER", 4300);
		m_pStep->nCellBal_WorkVolt=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_WORKVOLT", 3500);
		m_pStep->nCellBal_StartVolt=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_STARTVOLT", 50);
		m_pStep->nCellBal_EndVolt=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_ENDVOLT", 30);
		m_pStep->nCellBal_ActiveTime=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_SIGNALSENSTIME", 3);
		m_pStep->nCellBal_AutoStopTime=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_AUTOSTOPTIME", 60);
		m_pStep->nCellBal_AutoNextStep=AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "CellBal_AUTONEXTSTEP", 1);

		onSetValue();
	}
}

void Dlg_CellBal::OnUpdateEditCellbalActivetime() 
{
	UpdateData(TRUE);
	
	int nValue=atoi(theApp.win_GetText(&m_EditCellBALActiveTime));


	if((nValue < 3) || (nValue > 99))
	{
		if((nValue < 3) && (!m_Chk_CellBALActiveTime.GetCheck()))
		{
			//AfxMessageBox("최소 입력 값은 3입니다.\n3으로 입력 합니다.");
			AfxMessageBox(Fun_FindMsg("Dlg_CellBal_OnUpdateEditCellbalActivetime_msg1","DLG_CELLBAL"));//&&
			nValue = 3;
			m_EditCellBALActiveTime.SetWindowText("3");
		}
		else if(nValue > 99)
		{
			//AfxMessageBox("최대 입력 값은 99입니다.\n99으로 입력 합니다.");
			AfxMessageBox(Fun_FindMsg("Dlg_CellBal_OnUpdateEditCellbalActivetime_msg2","DLG_CELLBAL")); //&&
			nValue = 99;
			m_EditCellBALActiveTime.SetWindowText("99");
		}
	}


	if(m_pStep)
	{
		m_pStep->nCellBal_ActiveTime=nValue;
	}
	
	UpdateData(FALSE);	
}

void Dlg_CellBal::OnChangeEditCellbalActivetime() 
{
	UpdateData(TRUE);

	CString strValue;
	m_EditCellBALActiveTime.GetWindowText(strValue);
	if(m_pStep)
	{
		m_pStep->nCellBal_ActiveTime=(int)(atoi(strValue));//SEC
	}

	UpdateData(FALSE);
}

// void Dlg_CellBal::OnChangeEditCellbalAutostoptime() 
// {
// 	UpdateData(TRUE);
// 	
// 	CString strValue;
// 	m_EditCellBALAutoStopTime.GetWindowText(strValue);
// 
// 	int nValue = 0;
// 	if(m_pStep)
// 	{
// 		if(strValue.Find(_T("분"))>-1)
// 		{
// 			strValue.Replace(_T("분"),_T(""));
// 			nValue=atoi(strValue)*60;
// 		}
// 		else 
// 		{
// 			nValue=atoi(strValue);//SEC
// 		}
// 	}
// 	if(m_pStep)
// 	{
// 		m_pStep->nCellBal_AutoStopTime=nValue;
// 	}
// 	
// 	UpdateData(FALSE);	
// }
// 
// void Dlg_CellBal::OnUpdateEditCellbalAutostoptime() 
// {
// 	UpdateData(TRUE);
// 	
// 	int nValue=atoi(theApp.win_GetText(&m_EditCellBALAutoStopTime));
// 	if(m_pStep)
// 	{
// 		m_pStep->nCellBal_AutoStopTime=nValue;
// 	}
// 	
// 	UpdateData(FALSE);	
// }


void Dlg_CellBal::OnKillfocusEditCellbalActivetime()  //yulee 20190625
{
	UpdateData(TRUE);
	
	int nValue=atoi(theApp.win_GetText(&m_EditCellBALActiveTime));

	if((nValue < 3) && (m_Chk_CellBALActiveTime.GetCheck()))
	{
		if((nValue%3) < 3)
		{
			//AfxMessageBox("Active Time은 최소 입력 값이 3입니다.\n3으로 입력 합니다.");
			AfxMessageBox(Fun_FindMsg("Dlg_CellBal_OnKillfocusEditCellbalActivetime_msg1","DLG_CELLBAL")); //&&
			nValue = 3;
			m_EditCellBALActiveTime.SetWindowText("3");
		}
	}
	UpdateData(FALSE);
}

void Dlg_CellBal::OnEditchangeCboCellbalCircuitres()   //yulee 20190207
{
	UpdateData(TRUE);
	
// 	float fltValue = atof(theApp.win_GetText(&m_CboCellBALCircuitRes));
// 	CString tmpStr;
// 	tmpStr.Format(_T("%f.3"), fltValue);
// 	int nValue = atoi(tmpStr);
	int nValue=atoi(theApp.win_GetText(&m_CboCellBALCircuitRes));
	int nCurSel=m_CboCellBALCircuitRes.GetCurSel();
	
	if((nCurSel > 0)||((nValue >= 10)&&(nValue <= 100)))
	{
		onSetObject(0,TRUE);
	}
	else
	{
		onSetObject(0,FALSE);
	}

	if((m_pStep)&&(nValue >= 10)&&(nValue <= 100))
	{
		m_pStep->nCellBal_CircuitRes=nValue;
	}
	
	UpdateData(FALSE);	
}

void Dlg_CellBal::OnKillfocusCboCellbalCircuitres() //yulee 20190625
{
	UpdateData(TRUE);
	
	int nValue=atoi(theApp.win_GetText(&m_CboCellBALCircuitRes));
	int nCurSel=m_CboCellBALCircuitRes.GetCurSel();
	

	if((m_pStep)&&(nValue >= 10)&&(nValue <= 100))
	{
		//m_pStep->nCellBal_CircuitRes=nValue;
	}
	else
	{
		if(!m_pStep) //yulee 20190625
		{
			UpdateData(FALSE);	
			return;
		}
			m_pStep->nCellBal_CircuitRes=0;
	}
	
	UpdateData(FALSE);		
}

void Dlg_CellBal::OnChangeEditCellbalWorkvoltuppper() 
{
	UpdateData(TRUE);
	
	CString strValue;
	m_EditCellBALWorkVoltUpper.GetWindowText(strValue);
	if(m_pStep)
	{
		m_pStep->nCellBal_WorkVoltUpper=(int)(atof(strValue)*1000);//V=>mV
	}
	
	UpdateData(FALSE);
}