#define PROGRAM_VER             _T("CTSEditorPro (ver. 1.16.1)") 
#define PROGRAM_DATE             _T("CTSEditorPro (2020.10.18)") 




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												History
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/***********************************************************************************************************************
* v1.16.1::Han Kyeong-Hoon(21.10.18)
1. 제주TP 관련 절연모드 충전 관련 기능 보완
검색 : // 211015 HKH 절연모드 충전 관련 기능 보완

/***********************************************************************************************************************
* v1.16.0::Kang se-jun(20.01.20)
   1.  v1016 습도 센서 추가
************************************************************************************************************************/

/***********************************************************************************************************************
* v1.16.0::Kang se-jun(20.01.20)
   1. 오창LGC 안전기능 추가
   v1016 aux Vent 안전기능
************************************************************************************************************************/

/************************************************************************************************************************
* v1.15.3::LYU(19.11.01)
	1. CWA 결과데이터 호환
	2. 패턴 그래프 수정(SKH)
	3. 칠러 2520 추가 중
************************************************************************************************************************/

/************************************************************************************************************************
* v1.15.3::LYU(19.10.22)
	1. Vent Close, Buzzer off, EMG Stop 버튼의 Show가 안됨에 대한 수정
************************************************************************************************************************/

/************************************************************************************************************************
* v1.15.3::LYU(19.09.16)
	1. AuxV 256 대응(주석처리)
	LYU(19.09.17)
	2. 과충방전기 병합 
************************************************************************************************************************/


/***********************************************************************************************************************
* v1.15.2::LYU(19.09.10)
   1. 칠러 연동 수정
   2. 챔버 연동 시험 중 병렬 연결에 관한 오류 수정
   3. 에디터 추가 안전기능에 대한 단위 추가수정
   4. 모니터 Vent close 레지스트리로 예외처리
   5. 2Ch장비의 챔버 연동 수정(Ch2 동작 이상)
   LYU(19.09.11)
   6. 모니터 CAN 설정 실패 시 문구에 프로토콜 버전 포함
   LYU(19.09.18.2)
   7. 모니터 프로그램 외부데이터 설정의 수정 버튼으로 들어갈 시에 콤보박스 분류 1,2,3의 밀림 수정
************************************************************************************************************************/

/***********************************************************************************************************************
* v1.15.0::LJH(19.08.14)
   1. 폴란드향 v2010 V1013으로 vs6.0 V1014 merge 하여 vs2010 V1015 update
      - 하위 버전 호환되도록 파일 분리
	    .- PSCommon::Step.cpp의 SetStep, SaveStep, GetStep
   2. LG 박춘희 책임 요청 사항 추가(V1015 Up)
      - 공통 안전 조건
	    .- 셀 전압(AuxV) 편차 기능 추가(기존에는 편차값만 있었는데 Min, Max 추가)
		.- 셀 전압 편차 시 VENT 사용 체크 박스 추가
	  - 스텝 안전 조건
	    .- 셀 온도(AuxT, AuxTh, AuxT + AuxTh) 편차 기능 추가
		.- 셀 온도 편차 시 VENT 사용 체크 박스 추가(3개 추가:AuxT, AuxTh, AuxT + AuxTh)
		.- 셀 전압 변화량 기능 추가(초, 전압 변화량)
		.- 셀 전압 변화 시 VENT 사용 체크 박스 추가
************************************************************************************************************************/
/***********************************************************************************************************************
   LYU(19.09.18.2)
   8. 모니터 프로그램 외부데이터 설정의 수정 버튼으로 들어갈 시에 콤보박스 분류 1,2,3의 밀림 수정
   ::LYU(2019.10.10)
   9. buzzer, Vent Close 버튼 Hide 레지스트리관리로 체크박스 추가
   10. 칠러 파워서플라이 셀 밸런서 메뉴 Hide 레지스트리관리로 체크박스 추가
************************************************************************************************************************/










//ver. V1014 1.0.0.20190719 (LJH)
//1. LG 오창 9번방 프로그램 업데이트 

//ver. V1014 1.0.0.20190605
//1. 버전 업_스텝별 Cell V Delta 추가에 따르는 구조체 변경

/*
스케쥴 버전
#define SCHEDULE_FILE_VER_v1013_v2_SCH		0x00050002	//1013 VS2010 CWA
*/

//CTSEditorPro (ver. v1013 g.1.3.2.p20190718)
//1. CEditSimulDataDlg 의 PS_PATTERN_TIME_OPERATING 컬럼에 대한 이름 변경 
//2. 장비 번호 표시 CWorkStationSignDlg 표시 위치 왼쪽으로 이동