#if !defined(AFX_DLG_PATTSCHED_H__486DAEE0_CDEF_40F7_B816_4CCDEB27FBEA__INCLUDED_)
#define AFX_DLG_PATTSCHED_H__486DAEE0_CDEF_40F7_B816_4CCDEB27FBEA__INCLUDED_
//$1013
#include "CTSEditorProDoc.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_PattSched.h : header file
//
#define WM_PATTSCHED_DLG_CLOSE		WM_USER+3005
/////////////////////////////////////////////////////////////////////////////
// Dlg_PattSched dialog

class Dlg_PattSched : public CDialog
{
// Construction
public:
	Dlg_PattSched(CWnd* pParent = NULL);   // standard constructor

	CCTSEditorProDoc *m_pDoc;
	CWnd *m_pParent;
	
	STEP    *m_pStep;
	int     m_nStepNo;	

// Dialog Data
	//{{AFX_DATA(Dlg_PattSched)
	enum { IDD = IDD_DIALOG_PATTSCHED , IDD2 = IDD_DIALOG_PATTSCHED_ENG , IDD3 = IDD_DIALOG_PATTSCHED_PL };
	CLabel	m_StaticStepNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_PattSched)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_PattSched)
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_PATTSCHED_H__486DAEE0_CDEF_40F7_B816_4CCDEB27FBEA__INCLUDED_)
