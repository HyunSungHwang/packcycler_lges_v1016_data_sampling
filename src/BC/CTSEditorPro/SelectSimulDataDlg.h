#if !defined(AFX_SELECTSIMULDATADLG_H__53D4C731_5789_44CE_BD38_476A7E11E974__INCLUDED_)
#define AFX_SELECTSIMULDATADLG_H__53D4C731_5789_44CE_BD38_476A7E11E974__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphWnd.h"
#include "TextParsing.h"
#include "ProgressDlg.h"
#include "ChartViewer.h"

#include "EditSimulDataDlg.h"
// SelectSimulDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectSimulDataDlg dialog

class CSelectSimulDataDlg : public CDialog
{
// Construction
public:
	~CSelectSimulDataDlg();	
	BOOL IsChange;
	BOOL m_bIsApply;
	CString m_strErrorMsg;
	BOOL MakeSimulationFile(CString strExistingFileName, CString strNexFileName);
	char m_chMode;						//PS_MODE_CC,PS_MODE_CP,PS_MODE_CV
	char m_cPatternTime;				//★★ ljb 201033  PS_PATTERN_TIME1,PS_PATTERN_TIME_OPERATING
	long lLimitLow;
	long lLimitHigh;
	BOOL DeleteTempFile(CString strFile = "");
	BOOL isChangeSelFile;
	CString m_strNewSelFile;
	BOOL DrawGraph();
	BOOL LoadData(CString strFile);
	BOOL SetDataSheet(CString strFile);
	BOOL UpdateRawDataInfo(CString strFile);
	CString SelectRawFile(CString strFile = "");
	CString GetDataTitle();
	CString GetPathName();
	CSelectSimulDataDlg(CWnd* pParent = NULL);   // standard constructor	

	//+2015.9.23 USY Add for PatternCV
	BOOL CheckPatternData(CString strFile);
	//-

	STEP * pStep;

	LONG m_lMaxRefI;
	int m_nMinRefI;
	CProgressDlg * m_pProgress;

	//+2015.9.22 USY Add For PatternCV
	BOOL m_bParallelMode;		//병렬모드 사용여부
	//-

	
	//[20191024]Edited By SKH
	int m_CurPage;
	CString m_strCurPage;

	CChartViewer m_ChartViewer;
	XYChart *m_XyChar;

	BOOL SetPattrenDataSheet(CString strFile);
	void trackLineAxis(XYChart *c, int mouseX);

	void ApplyPattern();

// Dialog Data
	//{{AFX_DATA(CSelectSimulDataDlg)
	enum { IDD = IDD_SIMUL_DATA_DLG , IDD2 = IDD_SIMUL_DATA_DLG_ENG , IDD3 = IDD_SIMUL_DATA_DLG_PL };
	CString	m_strDataTitle;
	CString m_strEndTimeCycle;
	BOOL	m_bCheck;
	CString	m_CtrlLabFileName;
	//}}AFX_DATA
	CString	m_strSelFile;	
	CDCRScopeWnd m_wndGraph;
	CTextParsing m_SheetData;	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectSimulDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strExportFile;
	CString m_strTimeEndCycle;
	// Generated message map functions
	//{{AFX_MSG(CSelectSimulDataDlg)
	afx_msg void OnButtonDir();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnChangeEditDataTitle();
	afx_msg void OnMouseMovePlotArea();
	afx_msg LRESULT OnProgressSetRange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProgressStepIt(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProgressShow(WPARAM wParam, LPARAM lParam);
	afx_msg void OnButtonOpenExcel();
	afx_msg void OnButtonEdit();
	afx_msg void OnButtonNew();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonPrePage();
	afx_msg void OnBnClickedButtonNextPage();
	afx_msg void OnBnClicked1283();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTSIMULDATADLG_H__53D4C731_5789_44CE_BD38_476A7E11E974__INCLUDED_)
