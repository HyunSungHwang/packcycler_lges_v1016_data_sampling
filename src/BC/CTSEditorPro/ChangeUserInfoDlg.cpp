// ChangeUserInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "ChangeUserInfoDlg.h"

#include "UserAdminDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CChangeUserInfoDlg dialog

CChangeUserInfoDlg::CChangeUserInfoDlg(	STR_LOGIN *pCurLoginInfo, STR_LOGIN *pEnditLoginInfo, CWnd* pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CChangeUserInfoDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CChangeUserInfoDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CChangeUserInfoDlg::IDD3):
	(CChangeUserInfoDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CChangeUserInfoDlg)
	m_ID = _T("");
	m_PWD1 = _T("");
	m_PWD2 = _T("");
	m_strName = _T("");
	m_strDescription = _T("");
	m_bAutoLogOut = FALSE;
	m_nLogOutTime = 0;
	//}}AFX_DATA_INIT
	
	m_pCurLoginInfo = pCurLoginInfo;
	m_pEnditLoginInfo = pEnditLoginInfo;
	if(pEnditLoginInfo == NULL)
	{
		m_pEnditLoginInfo = m_pCurLoginInfo;
	}

	m_bNewUser = TRUE;
	m_lPermission = PMS_SUPERVISOR;
	
	ASSERT(pCurLoginInfo);


}


void CChangeUserInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChangeUserInfoDlg)
	DDX_Control(pDX, IDC_USER_WORKER, m_btnWorker);
	DDX_Control(pDX, IDC_USER_WORK_ADMIN, m_btnMang);
	DDX_Control(pDX, IDC_REMOVE_PERMISSION, m_btnRemovePer);
	DDX_Control(pDX, IDC_ADD_PERMISSION, m_btnAddPer);
	DDX_Control(pDX, IDC_USER_GUEST, m_btnGuest);
	DDX_Control(pDX, IDC_USER_ADMIN, m_btnAdmin);
	DDX_Control(pDX, IDC_SPIN1, m_ctrlLogOutTimeSpin);
	DDX_Control(pDX, IDC_LIST2, m_ctrlPermissionList2);
	DDX_Control(pDX, IDC_LIST1, m_ctrlPermissionList1);
	DDX_Text(pDX, IDC_USER_ID, m_ID);
	DDV_MaxChars(pDX, m_ID, 16);
	DDX_Text(pDX, IDC_USER_PWD, m_PWD1);
	DDV_MaxChars(pDX, m_PWD1, 16);
	DDX_Text(pDX, IDC_USER_PWD_CONFIRM, m_PWD2);
	DDV_MaxChars(pDX, m_PWD2, 16);
	DDX_Text(pDX, IDC_USER_NAME, m_strName);
	DDV_MaxChars(pDX, m_strName, 32);
	DDX_Text(pDX, IDC_USER_DESCRIPTION, m_strDescription);
	DDV_MaxChars(pDX, m_strDescription, 128);
	DDX_Check(pDX, IDC_AUTO_LOGOUT_FLAG, m_bAutoLogOut);
	DDX_Text(pDX, IDC_LOGOUT_TIME, m_nLogOutTime);
	DDV_MinMaxUInt(pDX, m_nLogOutTime, 0, 1000);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChangeUserInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CChangeUserInfoDlg)
	ON_EN_CHANGE(IDC_USER_ID, OnChangeUserId)
	ON_EN_CHANGE(IDC_USER_PWD, OnChangeUserPwd)
	ON_EN_CHANGE(IDC_USER_PWD_CONFIRM, OnChangeUserPwdConfirm)
	ON_EN_CHANGE(IDC_USER_NAME, OnChangeUserName)
	ON_EN_CHANGE(IDC_USER_DESCRIPTION, OnChangeUserDescription)
	ON_BN_CLICKED(IDC_ADD_PERMISSION, OnAddPermission)
	ON_BN_CLICKED(IDC_REMOVE_PERMISSION, OnRemovePermission)
	ON_BN_CLICKED(IDC_AUTO_LOGOUT_FLAG, OnAutoLogoutFlag)
	ON_BN_CLICKED(IDC_USER_GUEST, OnUserGuest)
	ON_BN_CLICKED(IDC_USER_WORKER, OnUserWorker)
	ON_BN_CLICKED(IDC_USER_WORK_ADMIN, OnUserWorkAdmin)
	ON_BN_CLICKED(IDC_USER_ADMIN, OnUserAdmin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChangeUserInfoDlg message handlers

BOOL CChangeUserInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ctrlLogOutTimeSpin.SetRange(0, 1000);

	if(!m_bNewUser)
	{
		m_lPermission = m_pEnditLoginInfo->nPermission;
		m_ID.Format("%s", m_pEnditLoginInfo->szLoginID);
		m_PWD1.Format("%s", m_pEnditLoginInfo->szPassword);
		m_PWD2.Format("%s", m_pEnditLoginInfo->szPassword);
		m_strName.Format("%s",  m_pEnditLoginInfo->szUserName);
		m_strDescription.Format("%s", m_pEnditLoginInfo->szDescription);
		m_bAutoLogOut = m_pEnditLoginInfo->bUseAutoLogOut;
		m_nLogOutTime = m_pEnditLoginInfo->lAutoLogOutTime;
		
		GetDlgItem(IDC_USER_ID)->EnableWindow(FALSE);

		//현재 Login된 사용자의 권한에 따라 처리 
		//수정 권한이 없으면 권한 수정 불가 
		if((m_pCurLoginInfo->nPermission & PMS_USER_SETTING_CHANGE) == FALSE)
		{
			m_ctrlPermissionList2.EnableWindow(FALSE);
			m_ctrlPermissionList1.EnableWindow(FALSE);
			GetDlgItem(IDC_USER_ADMIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_USER_WORK_ADMIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_USER_WORKER)->EnableWindow(FALSE);
			GetDlgItem(IDC_USER_GUEST)->EnableWindow(FALSE);

			//자가 자신이 아니면 아무것도 고칠수가 없다.
			if(strcmp(m_pEnditLoginInfo->szLoginID, m_pCurLoginInfo->szLoginID))
			{
				GetDlgItem(IDC_USER_PWD)->EnableWindow(FALSE);
				GetDlgItem(IDC_USER_PWD_CONFIRM)->EnableWindow(FALSE);
				GetDlgItem(IDC_USER_NAME)->EnableWindow(FALSE);
				GetDlgItem(IDC_USER_DESCRIPTION)->EnableWindow(FALSE);
				GetDlgItem(IDC_AUTO_LOGOUT_FLAG)->EnableWindow(FALSE);
				GetDlgItem(IDC_LOGOUT_TIME)->EnableWindow(FALSE);
				GetDlgItem(IDC_SPIN1)->EnableWindow(FALSE);
			}
		}
	}

	if(!m_bAutoLogOut)
	{
		GetDlgItem(IDC_LOGOUT_TIME)->EnableWindow(FALSE);
		m_ctrlLogOutTimeSpin.EnableWindow(FALSE);
	}

	CRect rect;
	m_ctrlPermissionList1.GetClientRect(&rect);
	int nColInterval = rect.Width();
	m_ctrlPermissionList1.InsertColumn(0, Fun_FindMsg("OnInitDialog_msg1","IDD_USER_SETTING"), LVCFMT_LEFT, nColInterval);//"권한"
	m_ctrlPermissionList1.SetRedraw(FALSE);
	m_ctrlPermissionList1.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlPermissionList1.ModifyStyle(0, LVS_REPORT);
	m_ctrlPermissionList1.SetRedraw(TRUE);
	m_ctrlPermissionList1.DeleteAllItems();

	m_ctrlPermissionList2.GetClientRect(&rect);
	nColInterval = rect.Width();
	m_ctrlPermissionList2.InsertColumn(0, Fun_FindMsg("OnInitDialog_msg1","IDD_USER_SETTING"), LVCFMT_LEFT, nColInterval);//"권한"
	m_ctrlPermissionList2.SetRedraw(FALSE);
	m_ctrlPermissionList2.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,0);
	m_ctrlPermissionList2.ModifyStyle(0, LVS_REPORT);
	m_ctrlPermissionList2.SetRedraw(TRUE);
	m_ctrlPermissionList2.DeleteAllItems();

	DrawList();

	GetDlgItem(IDC_USER_ID)->SetFocus();
	UpdateData(FALSE);
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChangeUserInfoDlg::OnOK() 
{
	if(CheckInfo() == FALSE)
	{
		return;
	}
	CDialog::OnOK();
}

BOOL CChangeUserInfoDlg::CheckInfo()
{
	UpdateData();
	CString strTemp;

	if(m_ID.IsEmpty())	
	{
		AfxMessageBox(Fun_FindMsg("CheckInfo_msg1","IDD_USER_SETTING"));//"ID가 입력되지 않았습니다."
		GetDlgItem(IDC_USER_ID)->SetFocus();
		return FALSE;
	}
	if(m_PWD1 != m_PWD2)
	{
		AfxMessageBox(Fun_FindMsg("CheckInfo_msg2","IDD_USER_SETTING"));//"Password 입력이 일치하지 않습니다."
		GetDlgItem(IDC_USER_PWD_CONFIRM)->SetFocus();
		return FALSE;
	}

	if(m_bNewUser)
	{
		CUserAdminDlg SeekUser;
		if(SeekUser.SearchUser(m_ID, FALSE) == TRUE)
		{
			AfxMessageBox(Fun_FindMsg("CheckInfo_msg3","IDD_USER_SETTING"));//"중복된 ID 입니다."
			GetDlgItem(IDC_USER_ID)->SetFocus();
			return FALSE;
		}
	}

	// TODO: Add extra validation here
	CUserRecordSet rs;		//기존에서 찾는다.
	if(!m_bNewUser)
	{
		if(strlen(m_pEnditLoginInfo->szPassword) < 1)
		{
			rs.m_strFilter.Format("[UserID] = '%s'", m_pEnditLoginInfo->szLoginID);
		}
		else
		{
			rs.m_strFilter.Format("[UserID] = '%s' AND [Password] = '%s'", m_pEnditLoginInfo->szLoginID, m_pEnditLoginInfo->szPassword);
		}
	}

	try
	{
		rs.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;	//DataBase Open Fail
	}

	if(!m_bNewUser)
	{
		if(rs.IsBOF() || rs.IsEOF())		//존재 하지 않는다.
		{
			rs.Close();
			strTemp.Format(Fun_FindMsg("CheckInfo_msg4","IDD_USER_SETTING"), m_pEnditLoginInfo->szLoginID);//"%s는 등록된 사용자가 아닙니다."
			AfxMessageBox(strTemp);
			return FALSE;
		}

		rs.Edit();
	}
	else
	{
		rs.AddNew();

		COleDateTime curTime = COleDateTime::GetCurrentTime();
		sprintf(m_pEnditLoginInfo->szRegistedDate, curTime.Format());
		rs.m_RegistedDate = curTime;

	}

	sprintf(m_pEnditLoginInfo->szLoginID, m_ID.operator const char*());
	m_pEnditLoginInfo->nPermission = m_lPermission;
	sprintf(m_pEnditLoginInfo->szDescription, m_strDescription.operator const char*());
	sprintf(m_pEnditLoginInfo->szUserName, m_strName.operator const char*());
	sprintf(m_pEnditLoginInfo->szPassword, m_PWD1.operator const char*());
	m_pEnditLoginInfo->bUseAutoLogOut = m_bAutoLogOut;
	m_pEnditLoginInfo->lAutoLogOutTime = m_nLogOutTime;

	rs.m_UserID = m_ID;
	rs.m_Authority = m_lPermission;
	rs.m_Description = m_strDescription;
	rs.m_Name = m_strName;
	rs.m_Password = m_PWD1;
	rs.m_AutoLogOut = m_bAutoLogOut;
	rs.m_AutoLogOutTime = m_nLogOutTime;


	//DataBase 정보 Update
	try
	{
		rs.Update();
		rs.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;	//DataBase Open Fail
	}

	if(m_bNewUser == FALSE)
	{
	//	WriteLastLogID(m_LoginInfo);
		UINT nSize = sizeof(STR_LOGIN);
		LPCTSTR lpData = (LPCTSTR)m_pEnditLoginInfo;
		AfxGetApp()->WriteProfileBinary(EDITOR_REG_SECTION, "LastLogin", (BYTE *)lpData, nSize);
	}

	return TRUE;
}

void CChangeUserInfoDlg::OnChangeUserId() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_ID.GetLength() > SCH_MAX_ID_SIZE)	
	{
		m_ID.Delete(SCH_MAX_ID_SIZE);
		UpdateData(FALSE);
	}
	
}

void CChangeUserInfoDlg::OnChangeUserPwd() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_PWD1.GetLength() > SCH_MAX_PWD_SIZE)	
	{
		m_PWD1.Delete(SCH_MAX_PWD_SIZE);
		UpdateData(FALSE);
	}
	
}

void CChangeUserInfoDlg::OnChangeUserPwdConfirm() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_PWD2.GetLength() > SCH_MAX_PWD_SIZE)	
	{
		m_PWD2.Delete(SCH_MAX_PWD_SIZE);
		UpdateData(FALSE);
	}
}

void CChangeUserInfoDlg::OnChangeUserName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_strName.GetLength() > SCH_USER_NAME_SIZE)	
	{
		m_strName.Delete(SCH_USER_NAME_SIZE);
		UpdateData(FALSE);
	}
}

void CChangeUserInfoDlg::OnChangeUserDescription() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_strDescription.GetLength() > SCH_DESCRIPTION_SIZE)	
	{
		m_strDescription.Delete(SCH_DESCRIPTION_SIZE);
		UpdateData(FALSE);
	}
}

void CChangeUserInfoDlg::OnAddPermission() 
{
	// TODO: Add your control notification handler code here
	int nSelected = -1;
//	nSelected = m_ctrlAllChList.GetSelectedCount();
	CString strTemp;
	DWORD data;
	while((nSelected = m_ctrlPermissionList1.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
	{
		data = m_ctrlPermissionList1.GetItemData(nSelected );
		m_lPermission |= data;
	}
	DrawList();
}

void CChangeUserInfoDlg::OnRemovePermission() 
{
	// TODO: Add your control notification handler code here
	int nSelected = -1;
//	nSelected = m_ctrlAllChList.GetSelectedCount();
	CString strTemp;
	DWORD data;
	while((nSelected = m_ctrlPermissionList2.GetNextItem(nSelected, LVNI_SELECTED)) >= 0)
	{
		data = m_ctrlPermissionList2.GetItemData(nSelected );
		m_lPermission = m_lPermission & (~data);
	}
	DrawList();
}


void CChangeUserInfoDlg::DrawList()
{
	LVITEM lvi;
	CString strItem;
	int nItem1= 0, nItem2 = 0;
	m_ctrlPermissionList1.DeleteAllItems();
	m_ctrlPermissionList2.DeleteAllItems();
	
	UpdateData(TRUE);
	lvi.mask =  LVIF_TEXT;
	lvi.iSubItem = 0;
	strItem.Format(Fun_FindMsg("DrawList_msg1","IDD_USER_SETTING"));//_T("장비전원끄기")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	if(m_lPermission & PMS_MODULE_SHUT_DOWN)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_SHUT_DOWN);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_SHUT_DOWN);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg2","IDD_USER_SETTING"));//_T("보정값 수정")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	if(m_lPermission & PMS_MODULE_CAL_UPDATE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_CAL_UPDATE);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_CAL_UPDATE);
		nItem2++;
	}
	
	strItem.Format(Fun_FindMsg("DrawList_msg3","IDD_USER_SETTING"));//_T("작업제어명령")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	if(m_lPermission & PMS_GROUP_CONTROL_CMD)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_GROUP_CONTROL_CMD);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_GROUP_CONTROL_CMD);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg4","IDD_USER_SETTING"));//_T("모니터링 실행")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	if(m_lPermission & PMS_FORM_START)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_FORM_START);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_FORM_START);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg5","IDD_USER_SETTING"));//_T("모니터링 종료")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	if(m_lPermission & PMS_FORM_CLOSE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_FORM_CLOSE);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_FORM_CLOSE);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg6","IDD_USER_SETTING"));//_T("사용자 정보 변경")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_USER_SETTING_CHANGE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_USER_SETTING_CHANGE);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_USER_SETTING_CHANGE);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg7","IDD_USER_SETTING"));//_T("모듈 추가/제거")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_MODULE_ADD_DELETE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_ADD_DELETE);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_ADD_DELETE);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg8","IDD_USER_SETTING"));//_T("공정조건전송")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_TEST_CONDITION_SEND);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_TEST_CONDITION_SEND);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg9","IDD_USER_SETTING"));//_T("공정편집기실행")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_EDITOR_START);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_EDITOR_START);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg10","IDD_USER_SETTING"));//_T("공정편집기종료")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_EDITOR_EDIT);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_EDITOR_EDIT);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg11","IDD_USER_SETTING"));//_T("트레이 등록")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_TRAY_REG);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_TRAY_REG);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg12","IDD_USER_SETTING"));//_T("모드변경")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_MODULE_MODE_CHANGE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_MODE_CHANGE);
		nItem1++;
	}
	else
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_MODE_CHANGE);
		nItem2++;
	}


	UpdateData(FALSE);
}
void CChangeUserInfoDlg::DrawList2()
{
	LVITEM lvi;
	CString strItem;
	int nItem1= 0, nItem2 = 0;
	
	UpdateData(TRUE);
	lvi.mask =  LVIF_TEXT;
	lvi.iSubItem = 0;

	strItem.Format(Fun_FindMsg("DrawList_msg1","IDD_USER_SETTING"));//_T("장비전원끄기")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_MODULE_SHUT_DOWN)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_SHUT_DOWN);
		nItem1++;
	}
       

	strItem.Format(Fun_FindMsg("DrawList_msg2","IDD_USER_SETTING"));//_T("보정값 수정")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_MODULE_CAL_UPDATE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_CAL_UPDATE);
		nItem1++;
	}

	

	strItem.Format(Fun_FindMsg("DrawList_msg3","IDD_USER_SETTING"));//_T("작업제어명령")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_GROUP_CONTROL_CMD)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_GROUP_CONTROL_CMD);
		nItem1++;
	}


	strItem.Format(Fun_FindMsg("DrawList_msg4","IDD_USER_SETTING"));//_T("모니터링 실행")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_FORM_START)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_FORM_START);
		nItem1++;
	}


	strItem.Format(Fun_FindMsg("DrawList_msg5","IDD_USER_SETTING"));//_T("모니터링 종료")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_FORM_CLOSE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_FORM_CLOSE);
		nItem1++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg6","IDD_USER_SETTING"));//_T("사용자 정보 변경")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_USER_SETTING_CHANGE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_USER_SETTING_CHANGE);
		nItem1++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg7","IDD_USER_SETTING"));//_T("모듈 추가/제거")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_MODULE_ADD_DELETE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_ADD_DELETE);
		nItem1++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg8","IDD_USER_SETTING"));//_T("공정조건전송")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_TEST_CONDITION_SEND);
		nItem1++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg9","IDD_USER_SETTING"));//_T("공정편집기실행")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_EDITOR_START);
		nItem1++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg10","IDD_USER_SETTING"));//_T("공정편집기종료")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_EDITOR_EDIT);
		nItem1++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg11","IDD_USER_SETTING"));//_T("트레이 등록")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_TEST_CONDITION_SEND)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_TRAY_REG);
		nItem1++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg12","IDD_USER_SETTING"));//_T("모드변경")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	if(m_lPermission & PMS_MODULE_MODE_CHANGE)
	{
		lvi.iItem = nItem1;
		m_ctrlPermissionList2.InsertItem(&lvi);
		m_ctrlPermissionList2.SetItemData( nItem1, PMS_MODULE_MODE_CHANGE);
		nItem1++;
	}

	UpdateData(FALSE);
}

void CChangeUserInfoDlg::OnAutoLogoutFlag() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(!m_bAutoLogOut)
	{
		GetDlgItem(IDC_LOGOUT_TIME)->EnableWindow(FALSE);
		m_ctrlLogOutTimeSpin.EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_LOGOUT_TIME)->EnableWindow(TRUE);
		m_ctrlLogOutTimeSpin.EnableWindow(TRUE);
	}
}

void CChangeUserInfoDlg::OnUserGuest() 
{
	// TODO: Add your control notification handler code here
	if (Fun_ClearList()) {};
	m_lPermission = PS_USER_GUEST;
	DrawList2();
}

void CChangeUserInfoDlg::OnUserWorker() 
{
	// TODO: Add your control notification handler code here
	if (Fun_ClearList()) {};
	m_lPermission = PS_USER_OPERATOR;
	DrawList2();
}

void CChangeUserInfoDlg::OnUserWorkAdmin() 
{
	// TODO: Add your control notification handler code here
	if (Fun_ClearList()) {};
	m_lPermission = PS_USER_SUPER;
	DrawList2();
}

void CChangeUserInfoDlg::OnUserAdmin() 
{
	// TODO: Add your control notification handler code here
	if (Fun_ClearList()) {};
	m_lPermission = PS_USER_ADMIN;
	DrawList2();
}

STR_LOGIN * CChangeUserInfoDlg::GetUserInfo()
{
	return m_pEnditLoginInfo;
}

BOOL CChangeUserInfoDlg::Fun_ClearList()
{
	LVITEM lvi;
	CString strItem;
	int nItem1= 0, nItem2 = 0;
	m_ctrlPermissionList1.DeleteAllItems();
	m_ctrlPermissionList2.DeleteAllItems();
	
	UpdateData(TRUE);
	lvi.mask =  LVIF_TEXT;
	lvi.iSubItem = 0;
	strItem.Format(Fun_FindMsg("DrawList_msg1","IDD_USER_SETTING"));//_T("장비전원끄기")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_SHUT_DOWN);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg2","IDD_USER_SETTING"));//_T("보정값 수정")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_CAL_UPDATE);
		nItem2++;
	}
	
	strItem.Format(Fun_FindMsg("DrawList_msg3","IDD_USER_SETTING"));//_T("작업제어명령")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_GROUP_CONTROL_CMD);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg4","IDD_USER_SETTING"));//_T("모니터링 실행")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_FORM_START);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg5","IDD_USER_SETTING"));//_T("모니터링 종료")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);

	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_FORM_CLOSE);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg6","IDD_USER_SETTING"));//_T("사용자 정보 변경")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_USER_SETTING_CHANGE);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg7","IDD_USER_SETTING"));//_T("모듈 추가/제거")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_ADD_DELETE);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg8","IDD_USER_SETTING"));//_T("공정조건전송")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_TEST_CONDITION_SEND);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg9","IDD_USER_SETTING"));//_T("공정편집기실행")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_EDITOR_START);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg10","IDD_USER_SETTING"));//_T("공정편집기종료")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_EDITOR_EDIT);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg11","IDD_USER_SETTING"));//_T("트레이 등록")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_TRAY_REG);
		nItem2++;
	}

	strItem.Format(Fun_FindMsg("DrawList_msg12","IDD_USER_SETTING"));//_T("모드변경")
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	{
		lvi.iItem = nItem2;
		m_ctrlPermissionList1.InsertItem(&lvi);
		m_ctrlPermissionList1.SetItemData( nItem2, PMS_MODULE_MODE_CHANGE);
		nItem2++;
	}
	UpdateData(FALSE);
	return TRUE;
}
