// TextParsing.cpp: implementation of the CTextParsing class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "TextParsing.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define WM_SET_RANGE		WM_USER + 100
#define WM_STEP_IT			WM_USER + 101
#define WM_PROGRESS_SHOW	WM_USER + 102
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTextParsing::CTextParsing()
{
	m_nRecordCount = 0;
	m_nColumnSize = 0;
	m_ppData = NULL;
}

CTextParsing::~CTextParsing()
{
	Clear();
}

BOOL CTextParsing::SetFile(CString strFileName)
{
	Clear();

	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   return FALSE;
	}

	CStringList		strDataList;
	while(aFile.ReadString(strTemp))
	{
		if(!strTemp.IsEmpty())	
		{
			strDataList.AddTail(strTemp);
		}
	}
	aFile.Close();
	
	m_nRecordCount = strDataList.GetCount()-1;
	if(m_nRecordCount < 0)		return FALSE;

	int p1=0, s=0;
	POSITION pos = strDataList.GetHeadPosition();

	strTemp = strDataList.GetNext(pos);		//Title
	while(p1!=-1)
	{
		p1 = strTemp.Find(',', s);
		if(p1!=-1)
		{
			str = strTemp.Mid(s, p1-s);
			m_TitleList.AddTail(str);
			s  = p1+1;
		}
	}
	str = strTemp.Mid(s);
	str.TrimLeft(' '); str.TrimRight(' ');
	if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);

	m_nColumnSize = m_TitleList.GetCount();

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}
	
	CStringList strColList;
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				strColList.AddTail(str);
				s  = p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);

		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < m_nColumnSize)
		{
			str = strColList.GetNext(pos1);
			m_ppData[nC][nRecord] = atof(str);
			nC++;
		}
		nRecord++;
	}
	return TRUE;
}

float * CTextParsing::GetColumnData(int nColIndex)
{
	if(nColIndex< 0 || nColIndex >= m_nColumnSize)		return NULL;

	return m_ppData[nColIndex];
}

CString CTextParsing::GetColumnHeader(int nColIndex)
{
	if(nColIndex< 0 || nColIndex >= m_nColumnSize)		return "";

	POSITION pos = m_TitleList.GetHeadPosition();
	CString str;
	int nCnt = 0;
	while(pos)
	{
		str = m_TitleList.GetNext(pos);	
		if(nCnt == nColIndex)
		{
			break;
		}
		nCnt++;
	}
	return str;
}
/*
---------------------------------------------------------
---------
-- Filename		:	TextParsing.cpp 
-- Description	:	CStringList 문자열을 추가한다.
-- Author		:	2014.09.03 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
CStringList& CTextParsing::StringSplit(CString string,CString separation,CStringList& array)
{	
	while (string.GetLength() > 0)
	{
		int Pos = string.Find(separation);
		if (Pos != -1)
		{				
			CString s = string.Left(Pos);
			array.AddTail(s);
			string = string.Mid(Pos + 1);
		}
		else
		{
			array.AddTail(string);
			break;
		}
	}
	return array;
}

/*
---------------------------------------------------------
---------
-- Filename		:	TextParsing.cpp 
-- Description	:	
-- Author		:	2014.09.03 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CTextParsing::SetUserMapFile(CString strFileName, CString &strErrorMsg)
{	
	//Clear();
	delete[]	m_ppData; 
	m_ppData = NULL;
	m_TitleList.RemoveAll();

	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;
	CStringList strColList;
	BOOL bReadHead = FALSE;
	BOOL bStartRead = FALSE;
	BOOL bTimeHead = FALSE;
	BOOL bConfigHead = FALSE;

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		   //strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFileName);
		strErrorMsg.Format(Fun_FindMsg("TextParsing_SetUserMapFile_msg1","TEXTPASING"), strFileName);//&&
		   return FALSE;
	}
	
	
	while(aFile.ReadString(strTemp))
	{
		if(bStartRead && !bReadHead){			

			//STX 다름 1라인은 시간설정값. 
			if(!bTimeHead){
				bTimeHead = TRUE;
				continue;
			}
			//STX 다름 2라인은 정보설정값. 
			if(!bConfigHead){
				bConfigHead = TRUE;
				continue;
			}


			if(strTemp == ""){
				AfxMessageBox("파일 데이터가 올바르지 않습니다.");
				return -1;
			}

			bReadHead = TRUE;		
			StringSplit(strTemp,",",m_TitleList);
			m_nColumnSize = m_TitleList.GetCount();			
		}else if(bStartRead && bReadHead){

			StringSplit(strTemp,",",strColList);
		}

		if(strTemp.Left(3) == START_SIMULATION_POINT)			
		{			
			bStartRead = TRUE;
												
		}else if(strTemp.Left(3) == END_SIMULATION_POINT){
			break;
		}	

		
	}		

	aFile.Close();	

	m_nRecordCount = strColList.GetCount() / 2;
	if(m_nRecordCount < 0)		
	{
		//strErrorMsg.Format("데이터가 올바르지 않습니다.");
		strErrorMsg.Format(Fun_FindMsg("TextParsing_SetUserMapFile_msg2","TEXTPASING"));//&&
		return FALSE;
	}
	
	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}
	
	POSITION pos1 = strColList.GetHeadPosition();
	int nC = 0;
	int nRecord = 0;
	while(pos1 && nC < m_nColumnSize)
	{
		str = strColList.GetNext(pos1);
		if(nC%m_nColumnSize == 0)
		{
			m_ppData[nC][nRecord] = atof(str);
			nC = 1;
		}
		else
		{
			m_ppData[nC][nRecord++] = atof(str);
			nC = 0;
			
		}		
	}

	return TRUE;
}


BOOL CTextParsing::SetSimulationFile(CString strFileName, CString &strErrorMsg)
{
	CTimeSpan CheckTime;
	Clear();

	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;

	CStringList strColList;

	ZeroMemory(&oldTime, sizeof(PSTimeSet));

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   //strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFileName);
		   strErrorMsg.Format(Fun_FindMsg("TextParsing_SetSimulationFile_msg1","TEXTPASING"), strFileName);//&&
		   return FALSE;
	}

	CStringList		strDataList;
	CString			strPtime,strDataS,strDataE,strMode;
	CString			strLastTime,strLastData;
	int nColCnt;
	int nRowCnt=0;

	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == START_SIMULATION_POINT)			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			int p1=0, s=0;
			//1. Title//////////////////////////////////////////////////////////////
			if(aFile.ReadString(strTemp))
			{
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
						m_TitleList.AddTail(str);
						s  = p1+1;
					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);
			}
			else
			{
				//AfxMessageBox("파일 데이터가 올바르지 않습니다.");
				AfxMessageBox(Fun_FindMsg("TextParsing_SetSimulationFile_msg2","TEXTPASING"));//&&
				return -1;
			}
			m_nColumnSize = m_TitleList.GetCount();
			
			////////////////////////////////////////////////////////////////////////
			BOOL	bFirstLineRead(TRUE);
			strLastTime = "";

			while(aFile.ReadString(strTemp))
			{
				nRowCnt++;
				strPtime=strDataS=strDataE=strMode = "";
				nColCnt=0;
				p1 = s = 0;
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
//						CString strMakeTime;
						if (nColCnt == 0)
						{
							if(GetColumnHeader(0) == "T2")		
							{
								//동작시간
								strPtime = GetStringTimePoint(str, TRUE);
							}
							else
							{
								//누적 시간일 경우 동작시간으로 변경한다.
								strPtime = GetStringTimePoint(str);
							}
						}
						if (nColCnt == 1)	strDataS = str;
						if (nColCnt == 2)	strDataE = str;

						//strColList.AddTail(strMakeTime);					
						s = p1+1;
						nColCnt++;

					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				
				if (!strPtime.IsEmpty())
				{
 					//if (strLastTime == "")
// 					if (bFirstLineRead)
// 					{
// 						strLastTime = "0.01";
// 						strColList.AddTail(strLastTime);
// 						strColList.AddTail(strDataS);
// 					}
					if (nColCnt == 1)	strDataS = str;
					if (nColCnt == 3)
					{
						strMode = str;
						strMode.MakeUpper();
						if (strMode	== "T")	
						{
							//삼각파
							if (bFirstLineRead)
							{
								strLastTime = "0.01";
								strColList.AddTail(strLastTime);
								strColList.AddTail(strDataS);
								strColList.AddTail(strPtime);
								strColList.AddTail(strDataE);
							}
							else
							{
								//삼각파
								strColList.AddTail(strLastTime);
								strColList.AddTail(strDataS);
								strColList.AddTail(strPtime);
								strColList.AddTail(strDataE);
							}
						}
						else
						{
							//사각파
							if (bFirstLineRead)
							{
								strLastTime = "0.01";
								strColList.AddTail(strLastTime);
								strColList.AddTail(strDataS);
								strColList.AddTail(strPtime);
								strColList.AddTail(strDataS);
							}
							else
							{
								strColList.AddTail(strLastTime);
								strColList.AddTail(strDataS);
								strColList.AddTail(strPtime);
								strColList.AddTail(strDataS);
							}
						}

					}
					else
					{
						strMode = "R";	//사각파
						if (bFirstLineRead)
						{
							strLastTime = "0.01";
							strColList.AddTail(strLastTime);
							strColList.AddTail(strDataS);
							strColList.AddTail(strPtime);
							strColList.AddTail(strDataS);
						}
						else
						{
							strColList.AddTail(strLastTime);
							strColList.AddTail(strDataS);
							strColList.AddTail(strPtime);
							strColList.AddTail(strDataS);
						}
						// 					strColList.AddTail(strPtime);
						// 					strColList.AddTail(strDataS);
					}
					strLastTime = strPtime;
				}
				//if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
				bFirstLineRead = FALSE;
//				if (nRowCnt > 20000) break;	//  [10/5/2011 XNOTE]
			}
// 			while(aFile.ReadString(strTemp))
// 			{
// 				p1 = s = 0;
// 				while(p1!=-1)
// 				{
// 					p1 = strTemp.Find(',', s);
// 					if(p1!=-1)
// 					{
// 						str = strTemp.Mid(s, p1-s);
// 						CString strMakeTime;
// 						if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
// 							strMakeTime = GetStringTimePoint(str, TRUE);
// 						else
// 							strMakeTime = GetStringTimePoint(str);
// 						
// 						strColList.AddTail(strMakeTime);					
// 						s = p1+1;
// 					}
// 				}
// 				str = strTemp.Mid(s);
// 				str.TrimLeft(' '); str.TrimRight(' ');
// 				if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
// 			}

		}	
	}
	
	
	aFile.Close();

	m_nRecordCount = strColList.GetCount() / 2;
	if(m_nRecordCount < 0)		
	{
		//strErrorMsg.Format("데이터가 올바르지 않습니다.");
		strErrorMsg.Format(Fun_FindMsg("TextParsing_SetSimulationFile_msg3","TEXTPASING"));//&&
		return FALSE;
	}

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}

	POSITION pos1 = strColList.GetHeadPosition();
	int nC = 0;
	int nRecord = 0;
	while(pos1 && nC < m_nColumnSize)
	{
		str = strColList.GetNext(pos1);
		if(nC%m_nColumnSize == 0)
		{
			m_ppData[nC][nRecord] = atof(str);
			nC = 1;
		}
		else
		{
			m_ppData[nC][nRecord++] = atof(str);
			nC = 0;
			
		}		
	}
	

	
/*	
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				//str.TrimLeft(':'); str.TrimRight(':');
				int p2 = 0;
				int s2 = 0;
				nHour = nMin = nSec = nMs = -1;
				
				while(p2!= -1)
				{
					CString strTime;
					p2 = str.Find(':', s2);
					if(p2!= -1)
					{
						strTime = str.Mid(s2, p2-s2);
						s2 = p2 + 1;

						if(nHour < 0)
							nHour = atoi(strTime);
						else if(nMin < 0)
							nMin = atoi(strTime);
					}
					else
					{
						if(s2 < str.GetLength())
						{
							if(nHour < 0)
								nHour = 0;
							if(nMin < 0)
								nMin = 0;
							strTime = str.Mid(s2, str.Find('.'));
							if(strTime == "")
							{
								strTime = str.Mid(str.ReverseFind(':')+1, str.GetLength());								
							}
							nSec = atoi(strTime);
							int nMsFind1, nMsFind2;
							nMsFind1 = str.Find('.');
							nMsFind2 = str.GetLength() - str.Find('.');
							
							if(nMsFind1 < 0)
								strTime = "00";
							else
								strTime = str.Mid(nMsFind1,nMsFind2);
							
							float fTemp = atof(strTime);
							nMs = fTemp * 100;
						}
					}
				
				}
				//str.Remove(':');
				if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
				{
					nHour	+= nTempHour;
					nMin	+= nTempMin;
					nSec	+= nTempSec;
					nMs		+= nTempMs;

					if(nMs > 99)					//990ms 보다 클경우
					{
						nSec += 1;
						nMs -= 100;

					}
					if(nSec > 59)
					{
						nMin += 1;
						nSec -= 60;
					}
					if(nMin > 59)
					{
						nHour += 1;
						nMin -= 60;
					}
				}

				
				CTimeSpan ts(0, nHour, nMin,nSec);
				CTimeSpan ts2(0, nTempHour, nTempMin, nTempSec);
				if(ts.GetTotalSeconds() < ts2.GetTotalSeconds())
				{
					strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
					return FALSE;
				}
				if(ts.GetTotalSeconds() == ts2.GetTotalSeconds())
				{
					if(nTempMs > nMs)
					{
						strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
						return FALSE;
					}
					
				}
				
				str.Format("%d.%02d", ts.GetTotalSeconds(), nMs);
				strColList.AddTail(str);
				
				nTempHour	= nHour;
				nTempMin	= nMin;
				nTempSec	= nSec;
				nTempMs		= nMs;
				CheckTime = ts;
				s			= p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
		pWnd->SendMessage(WM_STEP_IT, m_nRecordCount, 0);

		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < m_nColumnSize)
		{
			str = strColList.GetNext(pos1);
			m_ppData[nC][nRecord] = atof(str);
			nC++;
		}
		nRecord++;
	}
	pWnd->SendMessage(WM_PROGRESS_SHOW, SW_HIDE, 0);*/
	return TRUE;
}
/*
BOOL CTextParsing::SetSimulationFile(CString strFileName, CString &strErrorMsg)
{
	int nTempHour, nTempMin, nTempSec, nTempMs;
	nTempHour = nTempMin = nTempSec = nTempMs = 0;

	int nHour, nMin, nSec, nMs;				
	nHour = nMin = nSec = nMs = -1;

	CTimeSpan CheckTime;
	Clear();

	CString strTemp, str;
	CStdioFile aFile;

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFileName);
		   return FALSE;
	}

	CStringList		strDataList;
	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == START_SIMULATION_POINT)			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			while(aFile.ReadString(strTemp))
			{
				if(!strTemp.IsEmpty())	
				{
					strDataList.AddTail(strTemp);
				}
			}

		}	
	}

	
	aFile.Close();
	
	
	m_nRecordCount = strDataList.GetCount()-1;
	pWnd->SendMessage(WM_SET_RANGE, m_nRecordCount, 0);
	pWnd->SendMessage(WM_PROGRESS_SHOW, SW_SHOW, 0);
	if(m_nRecordCount < 0)		
	{
		strErrorMsg.Format("데이터가 올바르지 않습니다.");
		return FALSE;
	}

	int p1=0, s=0;
	POSITION pos = strDataList.GetHeadPosition();

	strTemp = strDataList.GetNext(pos);		//Title
	while(p1!=-1)
	{
		p1 = strTemp.Find(',', s);
		if(p1!=-1)
		{
			str = strTemp.Mid(s, p1-s);
			m_TitleList.AddTail(str);
			s  = p1+1;
		}
	}
	str = strTemp.Mid(s);
	str.TrimLeft(' '); str.TrimRight(' ');
	if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);

	m_nColumnSize = m_TitleList.GetCount();

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}
	
	CStringList strColList;
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				//str.TrimLeft(':'); str.TrimRight(':');
				int p2 = 0;
				int s2 = 0;
				nHour = nMin = nSec = nMs = -1;
				
				while(p2!= -1)
				{
					CString strTime;
					p2 = str.Find(':', s2);
					if(p2!= -1)
					{
						strTime = str.Mid(s2, p2-s2);
						s2 = p2 + 1;

						if(nHour < 0)
							nHour = atoi(strTime);
						else if(nMin < 0)
							nMin = atoi(strTime);
					}
					else
					{
						if(s2 < str.GetLength())
						{
							if(nHour < 0)
								nHour = 0;
							if(nMin < 0)
								nMin = 0;
							strTime = str.Mid(s2, str.Find('.'));
							if(strTime == "")
							{
								strTime = str.Mid(str.ReverseFind(':')+1, str.GetLength());								
							}
							nSec = atoi(strTime);
							int nMsFind1, nMsFind2;
							nMsFind1 = str.Find('.');
							nMsFind2 = str.GetLength() - str.Find('.');
							
							if(nMsFind1 < 0)
								strTime = "00";
							else
								strTime = str.Mid(nMsFind1,nMsFind2);
							
							float fTemp = atof(strTime);
							nMs = fTemp * 100;

	CFileException e;
						}
					}
				
				}
				//str.Remove(':');
				if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
				{
					nHour	+= nTempHour;
					nMin	+= nTempMin;
					nSec	+= nTempSec;
					nMs		+= nTempMs;

					if(nMs > 99)					//990ms 보다 클경우
					{
						nSec += 1;
						nMs -= 100;

					}
					if(nSec > 59)
					{
						nMin += 1;
						nSec -= 60;
					}
					if(nMin > 59)
					{
						nHour += 1;
						nMin -= 60;
					}
				}

				
				CTimeSpan ts(0, nHour, nMin,nSec);
				CTimeSpan ts2(0, nTempHour, nTempMin, nTempSec);
				if(ts.GetTotalSeconds() < ts2.GetTotalSeconds())
				{
					strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
					return FALSE;
				}
				if(ts.GetTotalSeconds() == ts2.GetTotalSeconds())
				{
					if(nTempMs > nMs)
					{
						strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
						return FALSE;
					}
					
				}
				
				str.Format("%d.%02d", ts.GetTotalSeconds(), nMs);
				strColList.AddTail(str);
				
				nTempHour	= nHour;
				nTempMin	= nMin;
				nTempSec	= nSec;
				nTempMs		= nMs;
				CheckTime = ts;
				s			= p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
		pWnd->SendMessage(WM_STEP_IT, m_nRecordCount, 0);

		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < m_nColumnSize)
		{
			str = strColList.GetNext(pos1);
			m_ppData[nC][nRecord] = atof(str);
			nC++;
		}
		nRecord++;
	}
	pWnd->SendMessage(WM_PROGRESS_SHOW, SW_HIDE, 0);
	return TRUE;
}
*/
void CTextParsing::SetParent(CWnd *pWnd)
{
	this->pWnd = pWnd;
}

void CTextParsing::Clear()
{
	if(m_ppData)
	{
		for(int i=0; i<m_nColumnSize; i++)
		{
			if(m_ppData[i] != NULL)
			{
				delete[] m_ppData[i];
				m_ppData[i] = NULL;
			}
		}
		delete[]	m_ppData; 
		m_ppData = NULL;
		m_TitleList.RemoveAll();
	}
}


BOOL CTextParsing::SetSimulationHeadFile(CString strFileName, CString &strErrorMsg)
{

	Clear();

	
	CString strTemp, str;
	CStringList strColList;

	ZeroMemory(&oldTime, sizeof(PSTimeSet));

	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFileName);
		   return FALSE;
	}


	CString			strLastTime,strLastData;


	m_TitleList.RemoveAll();
	m_ReadOffetHistory.RemoveAll();
	m_AccTimeHistory.RemoveAll();
	m_MaxPageFlag = false;

	bFirstLineRead = true;
	m_HistoryOffSetPos = 0;
	m_ReadRecordSize = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "PatternMaxCount", 200000);

	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == START_SIMULATION_POINT)			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			int p1=0, s=0;
			//1. Title//////////////////////////////////////////////////////////////
			if(aFile.ReadString(strTemp))
			{
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
						m_TitleList.AddTail(str);
						s  = p1+1;
					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				if(str.IsEmpty() == FALSE)	m_TitleList.AddTail(str);
			}
			else
			{
				AfxMessageBox("파일 데이터가 올바르지 않습니다.");
				return -1;
			}
			m_nColumnSize = m_TitleList.GetCount();
			break;


		}
	}
	CString temp;
	temp.Format("%d",aFile.GetPosition());
	m_ReadOffetHistory.AddTail(temp);
	m_AccTimeHistory.AddTail("0");
	aFile.Close();


	return TRUE;
}



BOOL CTextParsing::SetSimulationRowFile(CString strFileName, CString &strErrorMsg)
{
	CTimeSpan CheckTime;

	CString strTemp, str;

	CStringList strColList;

	ZeroMemory(&oldTime, sizeof(PSTimeSet));

	m_strFileName = strFileName;
 	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFileName);
		   return FALSE;
	}

	CStringList		strDataList;
	CString			strPtime,strDataS,strDataE,strMode;
	CString			strLastTime,strLastData;
	int nColCnt;
	int nRowCnt=0;


	int iReadCount = 0;
	int p1,s;
	POSITION pos;


	pos = m_ReadOffetHistory.FindIndex(m_HistoryOffSetPos);	
	aFile.Seek(atol(m_ReadOffetHistory.GetAt(pos)),SEEK_SET);

	pos = m_AccTimeHistory.FindIndex(m_HistoryOffSetPos);
	strLastTime = m_AccTimeHistory.GetAt(pos);


	CString tmpStr;
	tmpStr.Format("1. m_ReadRecordSize : %d, iReadCount : %d", m_ReadRecordSize, iReadCount);
	TRACE(tmpStr);
	while(iReadCount < m_ReadRecordSize)
	{	
		if(aFile.ReadString(strTemp))
		{
			
			iReadCount++;
			nRowCnt++;
			strPtime=strDataS=strDataE=strMode = "";
			nColCnt=0;
			p1 = s = 0;
			while(p1!=-1)
			{
				p1 = strTemp.Find(',', s);
				if(p1!=-1)
				{
					str = strTemp.Mid(s, p1-s);
					//						CString strMakeTime;
					if (nColCnt == 0)
					{
						if(GetColumnHeader(0) == "T2")		
						{
							//동작시간
							strPtime = GetStringTimePoint(str, TRUE);
						}
						else
						{
							//누적 시간일 경우 동작시간으로 변경한다.
							strPtime = GetStringTimePoint(str);
						}
					}
					if (nColCnt == 1)	strDataS = str;
					if (nColCnt == 2)	strDataE = str;
					
					//strColList.AddTail(strMakeTime);					
					s = p1+1;
					nColCnt++;
					
				}
			}
			str = strTemp.Mid(s);
			str.TrimLeft(' '); str.TrimRight(' ');
			
			if (!strPtime.IsEmpty())
			{
				
				if (nColCnt == 1)	strDataS = str;
				if (nColCnt == 3)
				{
					strMode = str;
					strMode.MakeUpper();
					if (strMode	== "T")	
					{
						//삼각파
						if (bFirstLineRead)
						{
							strLastTime = "0.01";
							strColList.AddTail(strLastTime);
							strColList.AddTail(strDataS);
							strColList.AddTail(strPtime);
							strColList.AddTail(strDataE);
						}
						else
						{
							//삼각파
							strColList.AddTail(strLastTime);
							strColList.AddTail(strDataS);
							strColList.AddTail(strPtime);
							strColList.AddTail(strDataE);
						}
					}
					else
					{
						//사각파
						if (bFirstLineRead)
						{
							strLastTime = "0.01";
							strColList.AddTail(strLastTime);
							strColList.AddTail(strDataS);
							strColList.AddTail(strPtime);
							strColList.AddTail(strDataS);
						}
						else
						{
							strColList.AddTail(strLastTime);
							strColList.AddTail(strDataS);
							strColList.AddTail(strPtime);
							strColList.AddTail(strDataS);
						}
					}
					
				}
				else
				{
					strMode = "R";	//사각파
					if (bFirstLineRead)
					{
						strLastTime = "0.01";
						strColList.AddTail(strLastTime);
						strColList.AddTail(strDataS);
						strColList.AddTail(strPtime);
						strColList.AddTail(strDataS);
					}
					else
					{
						strColList.AddTail(strLastTime);
						strColList.AddTail(strDataS);
						strColList.AddTail(strPtime);
						strColList.AddTail(strDataS);
					}
					// 					strColList.AddTail(strPtime);
					// 					strColList.AddTail(strDataS);
				}
				strLastTime = strPtime;
			}
			//if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
			bFirstLineRead = FALSE;		
			
			
	}else
 		break;
	}

	tmpStr.Format("2. m_ReadRecordSize : %d, iReadCount : %d", m_ReadRecordSize, iReadCount);
	TRACE(tmpStr);

	m_ReadRecordCount = iReadCount;
	m_HistoryOffSetPos++;
	if(m_ReadRecordCount < m_ReadRecordSize)
	{
		m_MaxPageFlag = true;
	}else
		m_MaxPageFlag = false;


	if(m_ReadOffetHistory.GetCount() == m_HistoryOffSetPos)
	{
		CString temp;
		temp.Format("%d",aFile.GetPosition());
		m_ReadOffetHistory.AddTail(temp);
		m_AccTimeHistory.AddTail(strLastTime);

	}




	m_nRecordCount = strColList.GetCount() / 2;
	if(m_nRecordCount < 0)		
	{
		strErrorMsg.Format("데이터가 올바르지 않습니다.");
		return FALSE;
	}

	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}

	POSITION pos1 = strColList.GetHeadPosition();
	int nC = 0;
	int nRecord = 0;
	while(pos1 && nC < m_nColumnSize)
	{
		str = strColList.GetNext(pos1);
		if(nC%m_nColumnSize == 0)
		{
			m_ppData[nC][nRecord] = atof(str);
			nC = 1;
		}
		else
		{
			m_ppData[nC][nRecord++] = atof(str);
			nC = 0;
			
		}		
	}
	aFile.Close();

	return TRUE;
}


CString CTextParsing::GetStringTimePoint(CString strTime, BOOL IsRunningTime)
{
	int nTempHour, nTempMin, nTempSec, nTempMs;
	nTempHour = nTempMin = nTempSec = nTempMs = 0;

	int nHour, nMin, nSec, nMs;				
	nHour = nMin = nSec = nMs = -1;
	
	int nIndex, nPos = 0;
	CString strTemp;
	strTime = MakeValidTime(strTime);

	nIndex = strTime.Find(":", nPos);
	if(nIndex != -1)
	{
		while(nIndex!= -1)
		{
			strTemp = strTime.Mid(nPos, nIndex - nPos);
			if(nHour < 0)
				nHour = atoi(strTemp);
			else if(nMin < 0)
				nMin = atoi(strTemp);
			else if(nSec < 0)
				nSec = atoi(strTemp);
			
			nPos = nIndex + 1;
			nIndex = strTime.Find(":", nPos);

		}
	}
	else
	{
		nHour = nMin = 0;
	}
	
	if(nSec < 0)
	{
		strTemp = strTime.Mid(nPos, strTime.GetLength() - nPos);
		nIndex = strTemp.Find(".");
		if(nIndex > 0)
		{
			double fTemp = atof(strTemp.Right(strTemp.GetLength() - nIndex));	//ljb 20150211 edit float -> double
			nMs = fTemp * 100.0f;
			strTemp = strTemp.Left(nIndex);
		}
		nSec = atoi(strTemp);
	}

	if(IsRunningTime == TRUE)	//동작시간일 경우
	{
		nHour	+= oldTime.nHour;
		nMin	+= oldTime.nMin;
		nSec	+= oldTime.nSec;
		nMs		+= oldTime.nMs;

		if(nMs > 99)					//990ms 보다 클경우
		{
			nSec += 1;
			nMs -= 100;

		}
		if(nSec > 59)
		{
			nMin += 1;
			nSec -= 60;
		}
		if(nMin > 59)
		{
			nHour += 1;
			nMin -= 60;
		}

		oldTime.nHour	= nHour;
		oldTime.nMin	= nMin;
		oldTime.nSec	= nSec;
		oldTime.nMs		= nMs;
	}
	
	CTimeSpan ts(0, nHour, nMin,nSec);
	CString strReturn;

	// dhkim 20191218 ksj의 ms표기 관련 수정 사항 적용
	//strReturn.Format("%d.%02d", ts.GetTotalSeconds(), nMs);	
	strReturn.Format("%lld.%02d", ts.GetTotalSeconds(), nMs);	
	
	if((strReturn.IsEmpty() == FALSE)||(strReturn != _T(""))) //yulee 20190610_1
		return strReturn;
	else
		return strReturn; //20190611

	
	
/*	while(p2!= -1)
	{
		p2 = str.Find(':', s2);
		if(p2!= -1)
		{
			strTime = str.Mid(s2, p2-s2);
			s2 = p2 + 1;

			if(nHour < 0)
				nHour = atoi(strTime);
			else if(nMin < 0)
				nMin = atoi(strTime);
		}
		else
		{
			if(s2 < str.GetLength())
			{
				if(nHour < 0)
					nHour = 0;
				if(nMin < 0)
					nMin = 0;
				strTime = str.Mid(s2, str.Find('.'));
				if(strTime == "")
				{
					strTime = str.Mid(str.ReverseFind(':')+1, str.GetLength());								
				}
				nSec = atoi(strTime);
				int nMsFind1, nMsFind2;
				nMsFind1 = str.Find('.');
				nMsFind2 = str.GetLength() - str.Find('.');
				
				if(nMsFind1 < 0)
					strTime = "00";
				else
					strTime = str.Mid(nMsFind1,nMsFind2);
				
				float fTemp = atof(strTime);
				nMs = fTemp * 100;
			}
		}
	
	}
	//str.Remove(':');
	if(GetColumnHeader(0) == "T2")		//동작 시간일 경우 누적시간으로 변경한다.
	{
		nHour	+= nTempHour;
		nMin	+= nTempMin;
		nSec	+= nTempSec;
		nMs		+= nTempMs;

		if(nMs > 99)					//990ms 보다 클경우
		{
			nSec += 1;
			nMs -= 100;

		}
		if(nSec > 59)
		{
			nMin += 1;
			nSec -= 60;
		}
		if(nMin > 59)
		{
			nHour += 1;
			nMin -= 60;
		}
	}

	
	CTimeSpan ts(0, nHour, nMin,nSec);
	CTimeSpan ts2(0, nTempHour, nTempMin, nTempSec);
	if(ts.GetTotalSeconds() < ts2.GetTotalSeconds())
	{
		strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
		return FALSE;
	}
	if(ts.GetTotalSeconds() == ts2.GetTotalSeconds())
	{
		if(nTempMs > nMs)
		{
			strErrorMsg.Format("시간 데이터가 올바르지 않습니다.%d:%d:%d / %d:%d:%d");
			return FALSE;
		}
		
	}
	
	str.Format("%d.%02d", ts.GetTotalSeconds(), nMs);

	return strTime2;*/
}

CString CTextParsing::MakeValidTime(CString strTime)
{
	CString strRTime;
	int nCount = 0;
	int nPos = 0, nIndex = 0;

	nIndex = strTime.Find(':', nPos);
	while(nIndex != -1)
	{
		nCount++;
		nPos = nIndex+1;
		nIndex = strTime.Find(':', nPos);
	}

	if(nCount == 2)		//HH:MM:SS 타입이므로 그대로 리턴
		return strTime;
	else if(nCount == 1)	//MM:SS 타입이므로 HH:MM:SS 타입으로 변환해서 리턴
		strRTime.Format("00:%s", strTime);
	else if(nCount == 0)				//SS 또는 SS.ms 타입이므로 HH:MM:SS 타입으로 변환해서 리턴
		strRTime.Format("00:00:%s", strTime);
	else					//Error
		return "";
	return strRTime;
}


BOOL CTextParsing::SetSocTableFile(CString strFileName, CString &strErrorMsg)
{
	Clear();
	
	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;
	
	CStringList strColList;
	
	m_strFileName = strFileName;
	if( !aFile.Open( strFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{		
		//strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFileName);
		strErrorMsg.Format(Fun_FindMsg("TextParsing_SetSocTableFile_msg1","TEXTPASING"), strFileName);//&&
		return FALSE;
	}
	
	CStringList		strDataList;
	CString			strPtime,strDataS,strDataE,strMode;
	CString			strLastTime,strLastData;
	int nColCnt;
	int nRowCnt=0;
	
	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Find(_T("STX_SOC"))>-1)
		{
			int p1=0, s=0;
			
			m_TitleList.AddTail(_T("Soc"));
			m_TitleList.AddTail(_T("Voltage"));
			m_TitleList.AddTail(_T("Current"));
			m_nColumnSize = m_TitleList.GetCount();
			
			////////////////////////////////////////////////////////////////////////
			BOOL bFirstLineRead(TRUE);
			strLastTime = "";
			
			while(aFile.ReadString(strTemp))
			{
				nRowCnt++;
				strPtime=strDataS=strDataE=strMode = "";
				nColCnt=0;
				p1 = s = 0;
				
				CString str1=theApp.str_GetSplit(strTemp,0,',');
				CString str2=theApp.str_GetSplit(strTemp,1,',');
				CString str3=theApp.str_GetSplit(strTemp,2,',');
				
				strDataS = str1;
				strDataE = str2;
				
				strColList.AddTail(str1);
				strColList.AddTail(str2);
				strColList.AddTail(str3);
			}
		}	
	}
	aFile.Close();
	if(m_nColumnSize<1)
	{
		//strErrorMsg.Format("데이터가 올바르지 않습니다.");
		strErrorMsg.Format(Fun_FindMsg("TextParsing_SetSocTableFile_msg2","TEXTPASING"));//&&
		return FALSE;
	}
	
	m_nRecordCount = strColList.GetCount() / m_nColumnSize;
	if(m_nRecordCount < 0)		
	{
		//strErrorMsg.Format("데이터가 올바르지 않습니다.");
		strErrorMsg.Format(Fun_FindMsg("TextParsing_SetSocTableFile_msg3","TEXTPASING"));//&&
		return FALSE;
	}
	
	m_ppData = new float*[m_nColumnSize];
	for(int i =0; i<m_nColumnSize; i++)
	{
		m_ppData[i] = new float[m_nRecordCount];
	}
	
	POSITION pos1 = strColList.GetHeadPosition();
	int nRecord[3] = {0};
	int nCount=0;
	while(pos1)
	{
		str = strColList.GetNext(pos1);
		
		int nSp=nCount%m_nColumnSize;
		
		m_ppData[nSp][nRecord[nSp]++] = atof(str);
		nCount++;
	}
	return TRUE;
}