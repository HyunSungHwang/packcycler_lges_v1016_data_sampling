#if !defined(AFX_DLG_PWRSUPPLY_H__734712D7_7D1B_42C8_8375_78D3D49EE3AD__INCLUDED_)
#define AFX_DLG_PWRSUPPLY_H__734712D7_7D1B_42C8_8375_78D3D49EE3AD__INCLUDED_
//$1013
#include "CTSEditorProDoc.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_PwrSupply.h : header file
//
#define WM_PWRSUPPLY_DLG_CLOSE		WM_USER+3003
/////////////////////////////////////////////////////////////////////////////
// Dlg_PwrSupply dialog

class Dlg_PwrSupply : public CDialog
{
// Construction
public:
	Dlg_PwrSupply(CWnd* pParent = NULL);   // standard constructor

	CCTSEditorProDoc *m_pDoc;
	CWnd *m_pParent;
	
	STEP    *m_pStep;
	int     m_nStepNo;
	
	void onSetObject(int iType);

// Dialog Data
	//{{AFX_DATA(Dlg_PwrSupply)
	enum { IDD = IDD_DIALOG_PWRSUPPLY , IDD2 = IDD_DIALOG_PWRSUPPLY_ENG, IDD3 = IDD_DIALOG_PWRSUPPLY_PL  };
	CComboBox	m_CboPwrSupplyVolt;
	CLabel	m_StaticStepNo;
	CButton m_RadPwrSupplyON;
	CButton m_RadPwrSupplyOFF;
	CButton m_RadPwrSupplyNONE;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_PwrSupply)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_PwrSupply)
	virtual void OnOK();
	virtual void OnCancel();	
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);	
	afx_msg void OnChkPwrsupplyCmd();
	afx_msg void OnRadPwrsupplyOn();
	afx_msg void OnRadPwrsupplyOff();
	afx_msg void OnRadPwrsupplyNone();
	afx_msg void OnEditchangeCboPwrsupplyVolt();
	afx_msg void OnSelchangeCboPwrsupplyVolt();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_PWRSUPPLY_H__734712D7_7D1B_42C8_8375_78D3D49EE3AD__INCLUDED_)
