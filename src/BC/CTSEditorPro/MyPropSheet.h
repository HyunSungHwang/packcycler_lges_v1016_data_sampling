#if !defined(AFX_MYPROPSHEET_H__F81DD2A0_9146_44CC_AAB0_4E90AB422DED__INCLUDED_)
#define AFX_MYPROPSHEET_H__F81DD2A0_9146_44CC_AAB0_4E90AB422DED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MyPropSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyPropSheet

class CMyPropSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CMyPropSheet)

// Construction
public:
	CMyPropSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CMyPropSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyPropSheet)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyPropSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyPropSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYPROPSHEET_H__F81DD2A0_9146_44CC_AAB0_4E90AB422DED__INCLUDED_)
