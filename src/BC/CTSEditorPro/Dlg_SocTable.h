#if !defined(AFX_DLG_SOCTABLE_H__5EA1BA1C_9F51_4A41_BF8B_2BCD16B38D5B__INCLUDED_)
#define AFX_DLG_SOCTABLE_H__5EA1BA1C_9F51_4A41_BF8B_2BCD16B38D5B__INCLUDED_
//$1013
#include "GraphWnd.h"
#include "TextParsing.h"
#include "ProgressDlg.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_SocTable.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_SocTable dialog

class Dlg_SocTable : public CDialog
{
// Construction
public:

	Dlg_SocTable(CWnd* pParent = NULL);   // standard constructor

	TEST_PARAM   *m_sPreTestParam;
	CDCRScopeWnd m_wndGraph;
	CTextParsing m_SheetData;
	BOOL         m_IsChange;
	CString      m_strErrorMsg;
	CString      m_strSocTableFile;
	CString      m_strSocTableFile_Edit;
	
	BOOL LoadData(CString strFile);
	BOOL SetDataSheet(CString strFile);
	BOOL DrawGraph();
	
	BOOL UpdateRawDataInfo(CString strFile);
	BOOL SelectRawFile(CString &strFile);
	void IsChangeFlag(BOOL IsFlag);

// Dialog Data
	//{{AFX_DATA(Dlg_SocTable)
	enum { IDD = IDD_DIALOG_SOCTABLE , IDD2 = IDD_DIALOG_SOCTABLE_ENG , IDD3 = IDD_DIALOG_SOCTABLE_PL  };
	CStatic	m_EditFileName;
	CEdit	m_EditDataTitle;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_SocTable)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_SocTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButtonNew();
	afx_msg void OnButtonEdit();
	afx_msg void OnButtonOpen();
	afx_msg void OnButtonSaveAs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_SOCTABLE_H__5EA1BA1C_9F51_4A41_BF8B_2BCD16B38D5B__INCLUDED_)
