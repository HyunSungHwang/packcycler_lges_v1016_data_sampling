#if !defined(AFX_DLG_CELLBAL_H__7A3AB969_9E4D_4BD4_9FA0_3C56C041D9A1__INCLUDED_)
#define AFX_DLG_CELLBAL_H__7A3AB969_9E4D_4BD4_9FA0_3C56C041D9A1__INCLUDED_

#include "CTSEditorProDoc.h"
//$1013
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_CellBal.h : header file
//
#define WM_CELLBAL_DLG_CLOSE		WM_USER+3002
/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBal dialog

class Dlg_CellBal : public CDialog
{
// Construction
public:

	Dlg_CellBal(CWnd* pParent = NULL);   // standard constructor
	CCTSEditorProDoc *m_pDoc;
	CWnd *m_pParent;
	
	STEP    *m_pStep;
	int     m_nStepNo;	

	void onSetValue();
	void onSetObject(int iType,BOOL bflag);
	void onDefault();

// Dialog Data
	//{{AFX_DATA(Dlg_CellBal)
	enum { IDD = IDD_DIALOG_CELLBAL , IDD2 = IDD_DIALOG_CELLBAL_ENG, IDD3 = IDD_DIALOG_CELLBAL_PL  };
	CEdit	m_EditCellBALWorkVoltUpper;
	CButton	m_Chk_CellBALActiveTime;
	CEdit	m_EditCellBALAutoStopTime;
	CEdit	m_EditCellBALActiveTime;
	CButton	m_ChkAutoNextStep;
	CComboBox	m_CboCellBALSignalSensTime;
	CComboBox	m_CboCellBALAutoStopTime;
	CComboBox	m_CboCellBALCircuitRes;
	CEdit	m_EditCellBALWorkVolt;
	CEdit	m_EditCellBALStartVolt;
	CEdit	m_EditCellBALEndVolt;
	CLabel	m_StaticStepNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_CellBal)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_CellBal)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnChangeEditCellbalWorkVolt();
	afx_msg void OnChangeEditCellbalStartVolt();
	afx_msg void OnChangeEditCellbalEndVolt();
	afx_msg void OnSelchangeCboCellbalCircuitres();
	afx_msg void OnEditchangeCboCellbalAutostoptime();
	afx_msg void OnSelchangeCboCellbalAutostoptime();	
	afx_msg void OnEditupdateCboCellbalSignalsenstime();
	afx_msg void OnSelchangeCboCellbalSignalsenstime();
	afx_msg void OnChkAutonextstep();
	afx_msg void OnUpdateEditCellbalActivetime();
	afx_msg void OnChangeEditCellbalActivetime();
	afx_msg void OnKillfocusEditCellbalActivetime();
	afx_msg void OnEditchangeCboCellbalCircuitres();
	afx_msg void OnKillfocusCboCellbalCircuitres();
	afx_msg void OnChangeEditCellbalWorkvoltuppper();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CELLBAL_H__7A3AB969_9E4D_4BD4_9FA0_3C56C041D9A1__INCLUDED_)
