// CTSEditorProView.h : interface of the CCTSEditorProView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSEditorProVIEW_H__1B7323B9_7330_4BD6_80E7_6B809D6197A2__INCLUDED_)
#define AFX_CTSEditorProVIEW_H__1B7323B9_7330_4BD6_80E7_6B809D6197A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MyGridWnd.h"
#include "EndConditionDlg.h"
#include "GridComboBox.h"
#include "RealEditCtrl.h"
#include "DateTimeCtrl.h"
#include "ModelSelDlg.h"
#include "CommSafetyCanAux.h"
#include "WorkSTSignEditDlg.h"

#include "Dlg_CellBal.h"
#include "Dlg_PwrSupply.h"
#include "Dlg_Chiller.h"
#include "Dlg_PattSched.h"
#include "Dlg_PattSelect.h"
#include "afxwin.h"

#include "TabCtrlEx.h"

//Grid Column 목록 
#define COL_MODEL_NO			0
#define COL_MODEL_NAME			1
#define COL_MODEL_CREATOR		2
#define COL_MODEL_DESCRIPTION	3
#define COL_MODEL_PRIMARY_KEY	4
#define COL_MODEL_EDIT_STATE	5

#define COL_TEST_NO				0
#define COL_TEST_PROC_TYPE		1
#define COL_TEST_RESET_PROC		2
#define COL_TEST_NAME			3
#define COL_TEST_CREATOR		4
#define COL_TEST_DESCRIPTION	5
#define COL_TEST_EDIT_TIME		6
#define COL_TEST_PRIMARY_KEY	7
#define COL_TEST_EDIT_STATE		8

#define COL_STEP_NO					0
#define COL_STEP_CYCLE				1
#define COL_STEP_PROCTYPE			2
#define COL_STEP_TYPE				3
#define COL_STEP_MODE				4
#define COL_STEP_REF_CHARGE_V		5
#define COL_STEP_REF_DISCHARGE_V	6
#define COL_STEP_REF_I				7
#define COL_STEP_RANGE				8		//khs
#define COL_STEP_POWER				9		//ljb
#define COL_STEP_RESISTANCE			10		//ljb
#define COL_STEP_WAIT_TIME			11		//ljb	20160414 add Cycle Step은 초기화 여부, 일반 Step은 대기시간 설정
#define COL_STEP_CHAMBER_TEMP		12		//ljb	201010
#define COL_STEP_CHAMBER_HUMI		13		//ljb	201010
#define COL_STEP_CELLBAL   		    14		//cny	201809
#define COL_STEP_PWRSUPPLY 		    15		//cny	201809
#define COL_STEP_CHILLER 		    16		//cny	201809
#define COL_STEP_PATTABLE		    17		//cny	201809
#define COL_STEP_END				18
#define COL_STEP_PREIMARY_KEY		19
#define COL_STEP_INDEX				20

#define TAB_SAFT_VAL		0
#define TAB_RECORD_VAL		1
#define TAB_GRADE_VAL		2
#define TAB_EDLC_CELL_VAL	3
#define TAB_CHAMBER_VAL		4

#define MAX_ACC_GROUP_COUNT	5

#define CS_SAVED		"1"
#define CS_NEW			"2"
#define CS_EDIT			"3"

class CCTSEditorProView : public CFormView
{
protected: // create from serialization only
	CCTSEditorProView();
	DECLARE_DYNCREATE(CCTSEditorProView)

public:
	//ljb Main View 변경 하기 v1009
	//(1: 처음에 모델 보이기 IDD = IDD_CTSEditorPro_CYCL1 , 2:모델 같이 보이기 :IDD = IDD_CTSEditorPro_CYCL)
	
	//#ifdef _CYCLER_
	// 	#ifdef _DLG_MODEL_
	//		enum { IDD = IDD_CTSEditorPro_CYCL };
	// 	#else
	// 		enum { IDD = IDD_CTSEditorPro_CYCL };
	// 	#endif
	//#endif

	//{{AFX_DATA(CCTSEditorProView)
	enum { IDD = IDD_CTSEditorPro_CYCL , IDD2 = IDD_CTSEditorPro_CYCL_ENG , IDD3 = IDD_CTSEditorPro_CYCL_PL  };
	CComboBox	m_CboParallelNum;
	CButton	m_bChkParallel;


	CComboBox	m_ctrlCboMultiGroup5;
	CComboBox	m_ctrlCboMultiGroup4;
	CComboBox	m_ctrlCboMultiGroup3;
	CComboBox	m_ctrlCboMultiGroup2;
	CComboBox	m_ctrlCboMultiGroup1;
	CComboBox	m_ctrlCboAccGroup5;
	CComboBox	m_ctrlCboAccGroup4;
	CComboBox	m_ctrlCboAccGroup3;
	CComboBox	m_ctrlCboAccGroup2;
	CComboBox	m_ctrlCboAccGroup1;
	CDateTimeCtrl	m_DeltaTime;
	CDateTimeCtrl	m_ReportTime;
/*	CTabCtrl*/ CTabCtrlEx	m_ctrlParamTab;
	CLabel	m_TotalStepCount;
	CLabel	m_LoadedTest;
	CLabel	m_TestNameLabel;
	CLabel	m_TotTestCount;
	CLabel	m_TotModelCount;
	CButton	m_PreTestCheck;
	CButton	m_GradeCheck;
	CLabel	m_strStepNumber;
	CLabel	m_strTestName;
	BOOL	m_bExtOption;
	int		m_nFaultNo;
	int		m_nCheckTime;
	CLabel	m_strWarning;
	UINT	m_ctrlEditAccGroup1;
	UINT	m_ctrlEditAccGroup2;
	UINT	m_ctrlEditAccGroup3;
	UINT	m_ctrlEditAccGroup4;
	UINT	m_ctrlEditAccGroup5;
	UINT	m_ctrlEditMultiGroup1;
	UINT	m_ctrlEditMultiGroup2;
	UINT	m_ctrlEditMultiGroup3;
	UINT	m_ctrlEditMultiGroup4;
	UINT	m_ctrlEditMultiGroup5;
	//}}AFX_DATA


	CStatic m_StaticCtrlSaftyCommonDelta;
	CButton	m_bChkDeltaVVent;
	CButton	m_bChkChamber;

	CButton	m_bChkStepDeltaVentAuxV;
	CButton	m_bChkStepDeltaVentAuxTemp;
	CButton	m_bChkStepDeltaVentAuxTh;
	CButton	m_bChkStepDeltaVentAuxT;
	CButton	m_bChkStepVentAuxV;

	BOOL m_bStepDeltaVentAuxTemp;		
	BOOL m_bStepDeltaVentAuxTh;	
	BOOL m_bStepDeltaVentAuxT;
	BOOL m_bStepDeltaVentAuxV;
	BOOL m_bStepVentAuxV;




// Attributes
public:

	BOOL Fun_UpdateCheckTable();
	BOOL Fun_UpdateStepTable();

	void onStepSocTable();

private:
	int m_nRangeVal;
	BOOL m_bUseRange;
	enum {_COL_ITEM1_=1, _COL_GRADE_MIN1_ = 2, _COL_GRADE_MAX1_ = 3, _COL_GRADE_RELATION_=4, _COL_ITEM2_=5, _COL_GRADE_MIN2_ = 6, _COL_GRADE_MAX2_ = 7, _COL_GRADE_CODE_ = 8};
	CToolTipCtrl m_tooltip;

	BOOL m_bDeltaVVent;
	BOOL m_bLinkChamber;		//ljb 챔버 연동 스케쥴

	CMyGridWnd m_wndStepGrid;
	CMyGridWnd m_wndTestListGrid;
	CMyGridWnd m_wndBatteryModelGrid;
	CMyGridWnd *m_pWndCurModelGrid;
	CMyGridWnd m_wndGradeGrid;
	//CMyGridWnd m_wndChamberGrid;
	CCTSEditorProDoc* GetDocument();
	CEndConditionDlg *m_pDlg;

	//Grid의 selection Combo
	CGridComboBox *m_pTestTypeCombo;
	CGridComboBox *m_pProcTypeCombo;
	CGridComboBox *m_pStepTypeCombo;
	CGridComboBox *m_pStepModeCombo;
	CGridComboBox *m_pStepRangeCombo;

	//Grade의 selection Combo
	CGridComboBox *m_pGradeItem1Combo;
	CGridComboBox *m_pGradeItem2Combo;
	CGridComboBox *m_pGradeRelationTypeCombo;

	//모델 조작 버튼
	stingray::foundation::SECBitmapButton m_btnModelNew;
	stingray::foundation::SECBitmapButton m_btnModelDelete;
	stingray::foundation::SECBitmapButton m_btnModelSave;
	stingray::foundation::SECBitmapButton m_btnModelCopy;
	stingray::foundation::SECBitmapButton m_btnModelEdit;

	//공정 조작 버튼
	stingray::foundation::SECBitmapButton m_btnProcNew;
	stingray::foundation::SECBitmapButton m_btnProcDelete;
	stingray::foundation::SECBitmapButton m_btnProcSave;
	stingray::foundation::SECBitmapButton m_btnProcEdit;
	stingray::foundation::SECBitmapButton m_btnProcCopy;
	
	//Step 조작 버튼
	stingray::foundation::SECBitmapButton m_btnStepNew;
	stingray::foundation::SECBitmapButton m_btnStepDelete;
	stingray::foundation::SECBitmapButton m_btnStepSave;

	stingray::foundation::SECBitmapButton m_btnLoad;
	stingray::foundation::SECBitmapButton m_btnModel;

	//Cell check 조건
	//SECCurrencyEdit	m_PreMaxV;
	//SECCurrencyEdit	m_PreMinV;
	//SECCurrencyEdit	m_PreMaxI;
	//SECCurrencyEdit	m_PreOCV;
	//SECCurrencyEdit	m_PreOCV2;
	//SECCurrencyEdit	m_TrickleI;
	//SECCurrencyEdit	m_PreDeltaV;

	//CMyDateTimeCtrl	m_TrickleTime;
	//SECCurrencyEdit	m_FaultNo;

	//ljb 20100823 안전 조건 
	SECCurrencyEdit	m_SafetyMaxV;
	SECCurrencyEdit	m_SafetyMinV;
	SECCurrencyEdit	m_SafetyCellDeltaV;		//lj b20150730
	SECCurrencyEdit	m_SafetyMaxI;
	SECCurrencyEdit	m_SafetyMaxT;
	SECCurrencyEdit	m_SafetyMinT;
	SECCurrencyEdit	m_SafetyMaxC;
	SECCurrencyEdit	m_SafetyMaxW;
	SECCurrencyEdit	m_SafetyMaxWh;




	//안전 조건 
	SECCurrencyEdit	m_VtgHigh;
	SECCurrencyEdit	m_VtgLow;
	SECCurrencyEdit	m_CrtHigh;
	SECCurrencyEdit	m_CrtLow;
	SECCurrencyEdit	m_CapHigh;
	SECCurrencyEdit	m_CapLow;
	SECCurrencyEdit	m_ImpHigh;
	SECCurrencyEdit	m_ImpLow;
	SECCurrencyEdit	m_TempHigh;		//ljb SBC가 챔버와 연동할때 챔버 온도 안전조건 HIGH
	SECCurrencyEdit	m_TempLow;		//ljb SBC가 챔버와 연동할때 챔버 온도 안전조건 Low
	SECCurrencyEdit	m_DeltaV;		//lj 
	SECCurrencyEdit	m_StdDeltaMaxV;		//lj b20150730
	SECCurrencyEdit	m_StdDeltaMinV;		//lj b20150730
	SECCurrencyEdit	m_DeltaMaxV;		//lj b20150730
	SECCurrencyEdit	m_DeltaMinV;
	SECCurrencyEdit m_DeltaI;
	SECCurrencyEdit	m_CapacitanceLow;
	SECCurrencyEdit	m_CapacitanceHigh;

	
	//CMyDateTimeCtrl	m_DeltaTime;

	//종지저류 검사
	SECCurrencyEdit	m_VtgEndHigh;
	SECCurrencyEdit	m_VtgEndLow;
	SECCurrencyEdit	m_CrtEndHigh;
	SECCurrencyEdit	m_CrtEndLow;

	//EDLC cap 측정 전압 
	SECCurrencyEdit	m_CapVtgLow;
	SECCurrencyEdit	m_CapVtgHigh;

	//전압 상승 검사
	SECCurrencyEdit	m_CompV[SCH_MAX_COMP_POINT];
	SECCurrencyEdit	m_CompV1[SCH_MAX_COMP_POINT];
	CMyDateTimeCtrl	m_CompTimeV[SCH_MAX_COMP_POINT];

	//전류 상승 검사
	SECCurrencyEdit	m_CompI[SCH_MAX_COMP_POINT];
	SECCurrencyEdit	m_CompI1[SCH_MAX_COMP_POINT];
	CMyDateTimeCtrl	m_CompTimeI[SCH_MAX_COMP_POINT];

	//Report 조건용 control
	SECCurrencyEdit	m_ReportTemp;
	SECCurrencyEdit	m_ReportV;
	SECCurrencyEdit m_ReportI;
	//CMyDateTimeCtrl	m_ReportTime;

	BOOL m_bShowEndDlgToggle;
	int m_nMultiGroupLoopInfoCycle[MAX_ACC_GROUP_COUNT];
	int m_nMultiGroupLoopInfoGoto[MAX_ACC_GROUP_COUNT];

	int m_nAccGroupLoopInfoCycle[MAX_ACC_GROUP_COUNT];
	int m_nAccGroupLoopInfoGoto[MAX_ACC_GROUP_COUNT];

	bool m_bFlagConfirmOK;


	SECCurrencyEdit m_SafetyCellDeltaVStep; //yulee 20190531_3
	SECCurrencyEdit	m_StepDeltaAuxTemp;
	SECCurrencyEdit	m_StepDeltaAuxTH;
	SECCurrencyEdit	m_StepDeltaAuxT;
	SECCurrencyEdit	m_StepDeltaAuxVTime;
	SECCurrencyEdit	m_StepDeltaAuxV;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSEditorProView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

// Implementation
public:
	void InitParamTab();

	BOOL Fun_CheckGotoStep( STEP *pStep, int nTotStep, CString &strMsg);	//20150206 add
	BOOL Fun_CheckStepInCycle( STEP *pStep, int nTotStep, CString &strMsg);	//20150206 add
	BOOL Fun_CreateChangeMDB();
	int Fun_CheckMultiLoop(CUIntArray &uiArryMultiLoopStart,CUIntArray &uiArryMultiLoopEnd);
	void InitMultiGroup(BOOL bInit);
	void Fun_UpdateDisplayMultiGroup();
	void Fun_UpdateMultiGroupList();
	void Fun_UpdateDisplayAccGroup();
	void Fun_UpdateAccGroupList();
	void InitAccGroup(BOOL bInit);
	void InitParallelCbo();

	void DeleteSimulationFile(LONG lModelID);
	void DeleteSimulationFile(CString strFile);
	void DeleteSimulationFileTestData (LONG lTestID);	
	CString NewSimulationFile(CString strFile = "");
	void ModelSave();
	void ModelCopy();
	void ModelEdit();
	void ModelDelete();
	void NewModel();
	BOOL InitBatteryModelGrid(UINT nID, CWnd *pParent, CMyGridWnd *pGrid);
	BOOL CheckExecutableStep(int nTypeComboIndex, long lTestID);
	BOOL IsExecutableStep(int nTypeComboIndex, WORD type, WORD mode);
	void ApplySelectStep(int nStepType = -1);
	void ApplySelectStepCanMode(int nStepType = -1);	//ljb 20150825 add
	void ApplySelectCanAuxStep(int nCopyType = -1, int nStepType = -1);
	
	void SetApplyAllBtn(int nType);
	void AddUndoStep();
	CString ModeTypeMsg(int type, int mode);
	CString StepTypeMsg(int type);
	void SetStepCycleColumn();
	void SetDefultTest(CString strModel, CString strTest);
	void SetControlEnable(int nStepType);
	void DisplayStepOption( STEP *pStep = NULL);
	
	void ShowExtOptionCtrl(int nShow);
	void SetCompCtrlEnable(BOOL bEnable = TRUE, BOOL bAll = TRUE);

	BOOL ReadFile(int nType=0);
	BOOL WriteFile();
	//BOOL LoadTestList(CTestListRecordSet *pRecord);
	int GetTestIndexFromType(int nType);
	int GetStepIndexFromType(int nType);
	long GetModeIndex(int nMode);
	BOOL DeleteStep(CDWordArray &awSteps); 
	BOOL BackUpStep(CRowColArray &awRows, BOOL bEnableUndo = FALSE);
	BOOL m_bPasteUndo;
	BOOL m_bCopyed;
	BOOL CopyGradeStep(long lFromStepID, long lToStepID);
	BOOL CopyCheckParam(long lFromTestID, long lToTestID);
	BOOL CopyStepList(long lFromTestID, long lToTestID);
	BOOL CopyTestList(int lFromModelID, int lToModelID);
	BOOL CopyModel(ROWCOL nRow);
	void UpdateModelNo();
	BOOL UpdateTypeComboList();
	BOOL UpdateTestNo();
	int RequeryProcDataTypeID();
	void SetTestGridColumnWidth();
	void SetModelGridColumnWidth();
	BOOL StepSave(int nType=0);
	BOOL m_bStepDataSaved;
	CRect m_recDlgRect;
	void ShowEndTypeDlg(int nStep);
	void ClearStepState();
	LONG m_lLoadedBatteryModelID;
	LONG m_lLoadedTestID;

	void UpdateDspTest(CString strName, LONG lID);
	void HideEndDlg();
	void UpdateBatteryCountState(int nCount);
	int m_nDisplayStep;
	void UpdateDspModel(CString strName, LONG lID);
	void SettingStepWndType(int nStep, int nStepType, BOOL bGradeWnd = TRUE);
	void SettingGradeWndType(int nStepType, int nProcType = 0);
	BOOL ReLoadStepData(LONG lTestID);
	int UpdatePreTestParam(int nType = 0); //yulee 20181009
	int DisplayPreTestParam();
	int UpdateStepGrade(int nStepNum);
	int DisplayStepGrade(STEP *pStep);
	void AddStepNew(int nStepNum = SCH_STEP_ALL, BOOL bAuto = TRUE, STEP *pCopyStep = NULL);
	BOOL UpdateStepGrid(int nStepNum = SCH_STEP_ALL);
	LONG m_lDisplayModelID;
	LONG m_lDisplayTestID;
	CString m_strDspModelName;
	CString m_strDspTestName;
	int RequeryTestStep(LONG lTestID);
	BOOL RequeryTestList(LONG lModelID, CString strDefaultName = "");
	BOOL RequeryBatteryModel(CString strDefaultName = "");
	BOOL DisplayStepGrid(int nStepNum = SCH_STEP_ALL);

	//cny 201809
	BOOL m_bShowCellBalDlgToggle;
	CRect m_DlgCellBalRect;
	Dlg_CellBal *m_pDlgCellBal;		
	void HideCellBalDlg();
	void ShowCellBalDlg(int iType,STEP *pStep=NULL);
	
	//cny 201809
	BOOL m_bShowPwrSupplyDlgToggle;
	CRect m_DlgPwrSupplyRect;
	Dlg_PwrSupply *m_pDlgPwrSupply;
	void HidePwrSupplyDlg();
	void ShowPwrSupplyDlg(int iType,STEP *pStep=NULL);
	
	//cny 201809
	BOOL m_bShowChillerDlgToggle;
	CRect m_DlgChillerRect;
	Dlg_Chiller *m_pDlgChiller;
	void HideChillerDlg();
	void ShowChillerDlg(int iType,STEP *pStep=NULL);
	
	//cny 201809
	BOOL m_bShowPattSchedDlgToggle;
	CRect m_DlgPattSchedRect;
	Dlg_PattSched *m_pDlgPattSched;
	void HidePattSchedDlg();
	void ShowPattSchedDlg(int iType,STEP *pStep=NULL);
	
	//cny 201809
	BOOL m_bShowPattSelectDlgToggle;
	CRect m_DlgPattSelectRect;
	Dlg_PattSelect *m_pDlgPattSelect;
	void HidePattSelectDlg();
	void ShowPattSelectDlg(int iType,STEP *pStep=NULL);

	CImageList	*m_pTabImageList;

	virtual ~CCTSEditorProView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CWorkSTSignEditDlg * m_pWorkSTSignEditDlg; //yulee 20181002
	CModelSelDlg *m_pModelDlg;
	int m_nTimer;
	BOOL InitEditCtrl();
	BOOL InitGradeGrid();
	BOOL InitStepGrid(int);
	BOOL InitTestListGrid();
	void InitToolTips();
	void SetControlCellEdlc();
	BOOL	m_bVHigh;
	BOOL	m_bVLow;
	BOOL	m_bIHigh;
	BOOL	m_bILow;
	BOOL	m_bCHigh;
	BOOL	m_bCLow;
	BOOL	m_bCapacitanceHigh;
	BOOL	m_bCapacitanceLow;
	float	m_fCapacitanceHigh;
	float	m_fCapacitanceLow;
	float	m_fCLowVal;
	float	m_fCHighVal;
	float	m_fILowVal;
	float	m_fIHighVal;
	float	m_fVLowVal;
	float	m_fVHighVal;

	//BOOL	m_bCellDeltaVstep; //yulee 20190531_5
	float	m_fCellDeltaVstep; //yulee 20190531_5
	float	m_fStepDeltaAuxTemp;
	float	m_fStepDeltaAuxTH;
	float	m_fStepDeltaAuxT;
	float	m_fStepDeltaAuxVTime;
	float	m_fStepAuxV;

	CArray<SCH_CODE_MSG ,SCH_CODE_MSG&> m_ptProcArray;

// Generated message map functions
protected:
	BOOL m_bUseMsec; // 10밀리세턴드를 사용여부 
	int m_nCustomMsec; //ksj 20171031 : spin 컨트롤 증감 값 옵션처리
	BOOL m_bUseModelInfo;
	CStringArray m_strTypeMaskArray;
	BOOL m_bUseParallel;		//20120222 KHS
	BOOL m_bUseChamberTemp;		//ljb 20160414 수정
	BOOL m_bUseChamberHumi;		//ljb 20160414 추가
	BOOL m_bUseDatabaseUpdate;	//ljb 2011314 이재복 //////////
	BOOL m_bUseExternalCAN;		//ljb 2011318 이재복 //////////
	BOOL m_bUseUserMap;			//2014.09.01 Usermap 사용유무(레지스트리설정값)
	BOOL m_bUseCellBal;		    //cny 201809
	BOOL m_bUsePwrSupply;		//cny 201809
	BOOL m_bUseChiller;  		//cny 201809
	BOOL m_bUsePatTable;  		//cny 201809
	BOOL m_bUseImpedance;		//ksj 20201117 : 임피던스 스텝 사용 유무 (레지스트리설정값)
	BOOL m_bUseAuxV;			//ksj 20210610 : AuxV 관련 기능 사용 여부. (위슨테크놀로지 요청. AuxV 사용 안하고 CAN으로 셀 전압 체크하는 설비)

	UINT m_nCurTabIndex;
	BOOL ConvertStepData(CStep *pStepData, STEP *pStep);
	//BOOL ConvertCellCheckData( FILE_CELL_CHECK_PARAM &paramData, TEST_PARAM *pParam);
	BOOL ConvertStepFormat(STEP *pStep, FILE_STEP_PARAM_V100D_LOAD &newStep);
	//BOOL ConvertCellCheckFormat(TEST_PARAM *pParam, FILE_CELL_CHECK_PARAM &newParam);
	//{{AFX_MSG(CCTSEditorProView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnLoadTest();
	afx_msg void OnModelNew();
	afx_msg void OnTestNew();
	afx_msg void OnModelDelete();
	afx_msg void OnTestDelete();
	afx_msg void OnModelSave();
	afx_msg void OnTestSave();
	afx_msg void OnStepSave();
	afx_msg void OnStepDelete();
	afx_msg void OnStepInsert();
	afx_msg void OnGradeCheck();
	afx_msg void OnPretestCheck();
	afx_msg void OnChangePretestMaxv();
	afx_msg void OnChangePretestDeltaV();
	afx_msg void OnChangePretestFaultNo();
	afx_msg void OnChangePretestMaxi();
	afx_msg void OnChangePretestMinv();
	afx_msg void OnChangePretestOcv();
	afx_msg void OnChangePretestTrckleI();
	afx_msg void OnChangeVtgHigh();
	afx_msg void OnChangeVtgLow();
	afx_msg void OnChangeCrtHigh();
	afx_msg void OnChangeCrtLow();
	afx_msg void OnChangeCapHigh();
	afx_msg void OnChangeCapLow();
	afx_msg void OnChangeImpHigh();
	afx_msg void OnChangeImpLow();
	afx_msg void OnChangeDeltaV();
	afx_msg void OnChangeDeltaI();
	afx_msg void OnChangeDeltaTime();
	afx_msg void OnOption();
	afx_msg void OnChangeCompV1();
	afx_msg void OnChangeCompV2();
	afx_msg void OnChangeCompV3();
	afx_msg void OnChangeCompTime1();
	afx_msg void OnChangeCompTime2();
	afx_msg void OnChangeCompTime3();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnModelCopyButton();
	afx_msg void OnExcelSave();
	afx_msg void OnJasonSchSave(); //yulee 20190104 Jason format sch
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnFileSave();
	afx_msg void OnFileOpen();
	afx_msg void OnUpdateExcelSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateJasonSchSave(CCmdUI* pCmdUI); //yulee 20190104 Jason format sch
	afx_msg void OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnExtOptionCheck();
	afx_msg void OnModelEdit();
	afx_msg void OnTestEdit();
	afx_msg void OnFilePrint();
	afx_msg void OnInsertStep();
	afx_msg void OnDeleteStep();
	afx_msg void OnUpdateDeleteStep(CCmdUI* pCmdUI);
	afx_msg void OnUpdateInsertStep(CCmdUI* pCmdUI);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTestCopyButton();
	afx_msg void OnEditInsert();
	afx_msg void OnUpdateEditInsert(CCmdUI* pCmdUI);
	afx_msg void OnChangeReportCurrent();
	afx_msg void OnChangeReportTemperature();
	afx_msg void OnChangeReportVoltage();
	afx_msg void OnDatetimechangeReportTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeCapVtgLow();
	afx_msg void OnChangeCapVtgHigh();
	afx_msg void OnChangeCompITime1();
	afx_msg void OnChangeCompITime2();
	afx_msg void OnChangeCompITime3();
	afx_msg void OnChangeCompI1();
	afx_msg void OnChangeCompI2();
	afx_msg void OnChangeCompI3();
	afx_msg void OnChangeCompI4();
	afx_msg void OnChangeCompI5();
	afx_msg void OnChangeCompI6();
	afx_msg void OnChangeCompV4();
	afx_msg void OnChangeCompV5();
	afx_msg void OnChangeCompV6();
	afx_msg void OnDeltaposRptMsecSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeCellCheckTimeEdit();
	afx_msg void OnAllStepButton();
	afx_msg void OnSameAllStepButton();
	afx_msg void OnChangeTempLow();
	afx_msg void OnChangeTempHigh();
	afx_msg void OnButtonModelSel();
	afx_msg void OnAdministration();
	afx_msg void OnUserSetting();
	afx_msg void OnUpdateAdministration(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUserSetting(CCmdUI* pCmdUI);
	afx_msg void OnModelReg();
	afx_msg void OnUpdateModelReg(CCmdUI* pCmdUI);
	afx_msg void OnChangeReportTimeMili();
	afx_msg void OnKillFocusReportTimeMili();
	afx_msg void OnChangeCapacitanceHigh();
	afx_msg void OnChangeCapacitanceLow();
	afx_msg void OnChangeEditAccGroupCount1();
	afx_msg void OnChangeEditAccGroupCount2();
	afx_msg void OnChangeEditAccGroupCount3();
	afx_msg void OnChangeEditAccGroupCount4();
	afx_msg void OnChangeEditAccGroupCount5();
	afx_msg void OnSelchangeCboAccGroup1();
	afx_msg void OnSelchangeCboAccGroup2();
	afx_msg void OnSelchangeCboAccGroup3();
	afx_msg void OnSelchangeCboAccGroup4();
	afx_msg void OnSelchangeCboAccGroup5();
	afx_msg void OnChangeEditMultiGroupRepeat1();
	afx_msg void OnChangeEditMultiGroupRepeat2();
	afx_msg void OnChangeEditMultiGroupRepeat3();
	afx_msg void OnChangeEditMultiGroupRepeat4();
	afx_msg void OnChangeEditMultiGroupRepeat5();
	afx_msg void OnSelchangeCboMultiGoto1();
	afx_msg void OnSelchangeCboMultiGoto2();
	afx_msg void OnWorkSTShow();
	afx_msg void OnSelchangeCboMultiGoto3();
	afx_msg void OnSelchangeCboMultiGoto4();
	afx_msg void OnSelchangeCboMultiGoto5();
	afx_msg void OnButSafetySetCanAux();
	afx_msg void OnChangeSafetyMaxC();
	afx_msg void OnChangeSafetyMaxI();
	afx_msg void OnChangeSafetyMaxT();
	afx_msg void OnChangeSafetyMaxV();
	afx_msg void OnChangeSafetyMaxW();
	afx_msg void OnChangeSafetyMaxWh();
	afx_msg void OnChangeSafetyMinI();
	afx_msg void OnChangeSafetyMinT();
	afx_msg void OnChangeSafetyMinV();
	afx_msg void OnChkChamber();
	afx_msg void OnButRegCanCode();
	afx_msg void OnButRegAuxCode();
	afx_msg void OnCanAllStepButton();
	afx_msg void OnSameCanAllStepButton();
	afx_msg void OnAuxAllStepButton();
	afx_msg void OnSameAuxAllStepButton();
	afx_msg void OnButDbUp();
	afx_msg void OnChkParallelMode();
	afx_msg void OnButDbCreate();
	afx_msg void OnRegCanCode();
	afx_msg void OnRegAuxCode();
	afx_msg void OnRegCanCodeManage();
	afx_msg void OnRegAuxCodeManage();
	afx_msg void OnAllStepButton2();
	afx_msg void OnSameAllStepButton2();
	afx_msg void OnWorkPneManage();
	afx_msg void OnKillFocusSafety_Max_I();	
	afx_msg void OnChangeCellDeltaVStep();
	//}}AFX_MSG
	afx_msg LONG OnGridDbClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnGridClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnGridKillFocus(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridComboSelected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridComboDropDown(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSelDragRowsDrop(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLeftCell(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnCloseEndDlg(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnCloseDlgCellBal(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnCloseDlgPwrSupply(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnCloseDlgChiller(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnCloseDlgPattSched(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnCloseDlgPattSelect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStartEditing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridBtnClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDateTimeChanged(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_StaticIDCLow;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CStatic m_StaticIDCHigh;
	CStatic m_StaticIDCRptTime;
	CStatic m_StaticIDCV;
	CStatic m_StaticIDCI;
	CStatic m_StaticIDCRptV;
	CStatic m_StaticIDCCap;
	CStatic m_StaticIDCRptI;
	CStatic m_StaticIDCImp;
	CStatic m_StaticIDCRptTemp;
	CStatic m_StaticIDCTemp;
	CStatic m_StaticIDCCapacitance;
	CStatic m_StaticIDCCapVtg;
	CStatic m_StaticIDCRptTunit;
	CStatic m_StaticIDCOr;
	afx_msg void OnEnChangeSafetyCellDeltaMinV();
	afx_msg void OnEnChangeSafetyCellDeltaV();
	afx_msg void OnEnChangeSafetyCellDeltaMaxV();
	afx_msg void OnBnClickedChkDeltaChamber();
	afx_msg void OnEnChangeStepAuxtDelta();
	afx_msg void OnEnChangeStepAuxthDelta();
	afx_msg void OnEnChangeStepAuxtAuxthDelta();
	afx_msg void OnEnChangeStepAuxvTime();
	afx_msg void OnEnChangeStepAuxvDelta();
	//afx_msg void OnBnClickedChkStepAuxtDelta();
	//afx_msg void OnBnClickedChkStepAuxvDelta();
	afx_msg void OnBnClickedChkStepDeltaAuxV();
	afx_msg void OnBnClickedChkStepDeltaAuxTemp();
	afx_msg void OnBnClickedChkStepDeltaAuxTh();
	afx_msg void OnBnClickedChkStepDeltaAuxT();
	afx_msg void OnBnClickedChkStepAuxV();
	afx_msg void OnEnKillfocusSafetyCellDeltaStdMinV();
	afx_msg void OnEnKillfocusSafetyCellDeltaStdMaxV();
	// ksj 20200212 : VENT 사용 여부 show/hide
	BOOL m_bUseVentSafety;
	afx_msg void OnStnClickedCheckCellDeltaVUnitStatic6();
};

#ifndef _DEBUG  // debug version in CTSEditorProView.cpp
inline CCTSEditorProDoc* CCTSEditorProView::GetDocument()
   { return (CCTSEditorProDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSEditorProVIEW_H__1B7323B9_7330_4BD6_80E7_6B809D6197A2__INCLUDED_)
