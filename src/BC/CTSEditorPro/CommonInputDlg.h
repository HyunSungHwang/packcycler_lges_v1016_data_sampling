#if !defined(AFX_COMMONINPUTDLG_H__C9D23E12_C06B_4E03_80BB_46593B957B8F__INCLUDED_)
#define AFX_COMMONINPUTDLG_H__C9D23E12_C06B_4E03_80BB_46593B957B8F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommonInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCommonInputDlg dialog

class CCommonInputDlg : public CDialog
{
// Construction
public:
	CString m_strTitleString;
	int m_nMode;
	CCommonInputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCommonInputDlg)
	enum { IDD = IDD_SAVE_CONFIG_DIALOG , IDD2 = IDD_SAVE_CONFIG_DIALOG_ENG , IDD3 = IDD_SAVE_CONFIG_DIALOG_PL };
	float	m_fValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommonInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:


	// Generated message map functions
	//{{AFX_MSG(CCommonInputDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAllStepButton();
	afx_msg void OnChargeButton();
	afx_msg void OnButton3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMONINPUTDLG_H__C9D23E12_C06B_4E03_80BB_46593B957B8F__INCLUDED_)
