// StepRecordSet_V1009.cpp : implementation file
//

#include "stdafx.h"
#include "ctseditor.h"
#include "StepRecordSet_V1009.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet_V1009

IMPLEMENT_DYNAMIC(CStepRecordSet_V1009, CDaoRecordset)

CStepRecordSet_V1009::CStepRecordSet_V1009(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CStepRecordSet_V1009)
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CStepRecordSet_V1009::GetDefaultDBName()
{
	return _T("");
}

CString CStepRecordSet_V1009::GetDefaultSQL()
{
	return _T("");
}

void CStepRecordSet_V1009::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CStepRecordSet_V1009)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CStepRecordSet_V1009 diagnostics

#ifdef _DEBUG
void CStepRecordSet_V1009::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CStepRecordSet_V1009::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
