#if !defined(AFX_TESTNAMEDLG_H__9402A2ED_9FB1_468C_AEAF_49923E9EEC5F__INCLUDED_)
#define AFX_TESTNAMEDLG_H__9402A2ED_9FB1_468C_AEAF_49923E9EEC5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestNameDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestNameDlg dialog

class CTestNameDlg : public CDialog
{
// Construction
public:
	long m_lTestTypeID;
	long m_lModelID;
	COleDateTime m_createdDate;
	CTestNameDlg(long lModelID, BOOL bCopyMode = FALSE, CWnd* pParent = NULL);   // standard constructor
	
// Dialog Data
	//{{AFX_DATA(CTestNameDlg)
	enum { IDD = IDD_TEST_NAME_DLG , IDD2 = IDD_TEST_NAME_DLG_ENG , IDD3 = IDD_TEST_NAME_DLG_PL };
	CComboBox	m_ctrlProcTypeCombo;
	CComboBox	m_ctrlModelListCombo;
	CString	m_strName;
	CString	m_strDecription;
	CString	m_strCreator;
	BOOL	m_bContinueCellCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestNameDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL m_bCopyMode;

	// Generated message map functions
	//{{AFX_MSG(CTestNameDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeTestNameEdit();
	afx_msg void OnChangeTestDescripEdit();
	afx_msg void OnChangeTestCreatorEdit();
	afx_msg void OnSelchangeModelSelCombo();
	afx_msg void OnSelchangeProcTypeCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTNAMEDLG_H__9402A2ED_9FB1_468C_AEAF_49923E9EEC5F__INCLUDED_)
