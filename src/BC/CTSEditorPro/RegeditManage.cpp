// RegeditManage.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "RegeditManage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegeditManage dialog



CRegeditManage::CRegeditManage(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CRegeditManage::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CRegeditManage::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CRegeditManage::IDD3):
	(CRegeditManage::IDD), pParent)
{
	//{{AFX_DATA_INIT(CRegeditManage) 
	m_check1 = FALSE;
	m_check2 = FALSE;
	m_check3 = FALSE;
	m_check4 = FALSE;
	m_check5 = FALSE;
	m_check6 = FALSE;
	m_check7 = FALSE;
	m_check8 = FALSE;
	m_check9 = FALSE;
	m_check10 = FALSE;
	m_check11 = FALSE;
	m_edit_num1 = 0;
	m_edit_num2 = 0;
	m_edit_num3 = 0;
	m_edit_str1 = _T("");
	m_edit_str2 = _T("");
	m_edit_str3 = _T("");
	m_edit_str4 = _T("");
	m_edit_str5 = _T("");
	m_edit_num4 = 0;
	m_edit_num5 = 0;
	m_check12 = FALSE;
	m_check13 = FALSE;
	m_edit_str6 = _T("");
	m_edit_str7 = _T("");
	m_edit_str8 = _T("");
	m_check14 = FALSE;
	m_edit_str9 = _T("");
	m_edit_str10 = _T("");
	m_chkUseIsolSafety = FALSE;
	//}}AFX_DATA_INIT
}


void CRegeditManage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegeditManage)
	DDX_Control(pDX, IDC_CHECK_CHILLER_USE, m_ChkChillerUse);	
	DDX_Control(pDX, IDC_CHECK_PWRSUPPLY_USE, m_ChkPwrSupplyUse);
	DDX_Control(pDX, IDC_CHECK_CELLBAL_USE, m_ChkCellBalUse);
	DDX_Check(pDX, IDC_CHECK1, m_check1);
	DDX_Check(pDX, IDC_CHECK2, m_check2);
	DDX_Check(pDX, IDC_CHECK3, m_check3);
	DDX_Check(pDX, IDC_CHECK4, m_check4);
	DDX_Check(pDX, IDC_CHECK5, m_check5);
	DDX_Check(pDX, IDC_CHECK6, m_check6);
	DDX_Check(pDX, IDC_CHECK7, m_check7);
	DDX_Check(pDX, IDC_CHECK8, m_check8);
	DDX_Check(pDX, IDC_CHECK9, m_check9);
	DDX_Check(pDX, IDC_CHECK10, m_check10);
	DDX_Check(pDX, IDC_CHECK11, m_check11);
	DDX_Text(pDX, IDC_EDIT1, m_edit_num1);
	DDX_Text(pDX, IDC_EDIT2, m_edit_num2);
	DDX_Text(pDX, IDC_EDIT3, m_edit_num3);
	DDX_Text(pDX, IDC_EDIT4, m_edit_str1);
	DDX_Text(pDX, IDC_EDIT5, m_edit_str2);
	DDX_Text(pDX, IDC_EDIT6, m_edit_str3);
	DDX_Text(pDX, IDC_EDIT7, m_edit_str4);
	DDX_Text(pDX, IDC_EDIT8, m_edit_str5);
	DDX_Text(pDX, IDC_EDIT9, m_edit_num4);
	DDV_MinMaxUInt(pDX, m_edit_num4, 0, 4);
	DDX_Text(pDX, IDC_EDIT10, m_edit_num5);
	DDV_MinMaxUInt(pDX, m_edit_num5, 0, 1);
	DDX_Check(pDX, IDC_CHECK12, m_check12);
	DDX_Check(pDX, IDC_CHECK13, m_check13);
	DDX_Text(pDX, IDC_EDIT11, m_edit_str6);
	DDX_Text(pDX, IDC_EDIT12, m_edit_str7);
	DDX_Text(pDX, IDC_EDIT13, m_edit_str8);
	DDX_Check(pDX, IDC_CHECK14, m_check14);
	DDX_Text(pDX, IDC_EDIT14, m_edit_str9);
	DDX_Text(pDX, IDC_EDIT15, m_edit_str10);
	DDX_Check(pDX, IDC_CHECK_USE_ISOLATION_SAFETY, m_chkUseIsolSafety);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRegeditManage, CDialog)
	//{{AFX_MSG_MAP(CRegeditManage)
	ON_EN_CHANGE(IDC_EDIT14, OnChangeEdit14)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegeditManage message handlers

BOOL CRegeditManage::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_check1 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseLogin", FALSE);
	m_check2 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseLoader",FALSE);			//20151223 Loader연동
	m_check3 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseChamberForTemp", FALSE);	//20160414 챔버 온도 사용
	m_check4 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseChamberForHumi", FALSE);	//20160414 챔버 습도 사용
	
	m_check5 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseParallel", FALSE);		//Parallel 체크 보이기
	
	m_check6 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseExternalCAN", FALSE); 
	m_check7 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseUserMap",FALSE);
	m_check8 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseMsec", FALSE);
	
	m_check9 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseNoCheckMode", FALSE);		//2014.12.08 데이터 무시 모드
	m_check10 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseCanTxOffMode", FALSE);	//2014.12.09 캔 송신 플러그 모드
	m_check11 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseCanCheckMode", FALSE);	//20150825 캔 통신체크 모드
	m_check12 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseConfirmCellDevVolt", FALSE);	//20170807  Cell 전압편차 확인 메시지 띄우기
	m_check13 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Use Common Safety", FALSE);	//20170929 안전조건 저장 확인
	m_check14 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseCanCheckModeOnlyMaster", FALSE);	//20150825 캔 통신체크 모드 //20180518 yulee master만 사용 시

	m_edit_num1 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Current_Spec", 5000); //yulee 20181212 Max Current->Max Current_Spec
	m_edit_num2 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Voltage", 5000);
	m_edit_num3 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Min Ref Current", 25);
	//m_edit_num4 = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "Max Aging Current", 500);

	m_edit_str1 = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "V Unit", "V 3");
	m_edit_str2 = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "I Unit", "mA 1");
	m_edit_str3 = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "C Unit", "mAh 1");
	m_edit_str4 = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "W Unit", "mW 1");
	m_edit_str5 = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "Wh Unit", "mWh 1");
	m_edit_str6.Format(_T("%d"), AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec Interval",0));  // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.
	m_edit_str7.Format(_T("%d"), AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec IntervalStepEnd",0));  // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.
	m_edit_str8.Format(_T("%d"), AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Msec IntervalPattEnd",0));  // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.
	m_edit_str9.Format(_T("%d"), AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Parallel Max Number",4));  // yulee 20180719 add
	m_edit_num4 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Count", 4);   //ljb 20170720 add
	m_edit_num5 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", 1);   //ljb 20170720 add
	m_edit_str10.Format(_T("%s"), AfxGetApp()->GetProfileString(EDITOR_REG_SECTION, "ChillPumpFactor", "1")); //yulee 20190121

	//cny 201809
	m_ChkCellBalUse.SetCheck(AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION   , "UseCellBal"   , TRUE));
	m_ChkPwrSupplyUse.SetCheck(AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION , "UsePwrSupply" , TRUE));
	m_ChkChillerUse.SetCheck(AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION   , "UseChiller"   , TRUE));

	m_chkUseIsolSafety = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Use Isolation Safety", 0);		// 211015 HKH 절연모드 충전 관련 기능 보완

	
	UpdateData(FALSE);	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRegeditManage::OnOK() 
{
	UpdateData(TRUE);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseLogin"			, m_check1);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseLoader"		, m_check2);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseChamberForTemp", m_check3);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseChamberForHumi", m_check4);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseParallel"		, m_check5);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseExternalCAN"	, m_check6);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseUserMap"		, m_check7);	
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseMsec"			, m_check8);	
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseNoCheckMode"	,m_check9);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseCanTxOffMode"	, m_check10);		
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseCanCheckMode"	, m_check11);		
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseConfirmCellDevVolt", m_check12);		
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Use Common Safety", m_check13);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseCanCheckModeOnlyMaster", m_check14);
										  
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Max Current_Spec"		, m_edit_num1);		//yulee 20181212 Max Current->Max Current_Spec
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Max Voltage"		, m_edit_num2);		
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Min Ref Current"	, m_edit_num3);		
	
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION, "V Unit"			, m_edit_str1);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION, "I Unit"			, m_edit_str2);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION, "C Unit"			, m_edit_str3);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION, "W Unit"			, m_edit_str4);
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION, "Wh Unit"			, m_edit_str5);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Msec Interval"		, atoi(m_edit_str6));
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Msec IntervalStepEnd"	, atoi(m_edit_str7));	//yulee 20180413 ???? ?? ?? 
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Msec IntervalPattEnd"	, atoi(m_edit_str8));	//yulee 20180413 ???? ?? ??
	AfxGetApp()->WriteProfileString(EDITOR_REG_SECTION, "ChillPumpFactor"	, m_edit_str10); //yulee 20190121
	UpdateData();
	if(atoi(m_edit_str9)<2) //20180719 yulee add
	{
		//&& AfxMessageBox(_T("병렬 가능숫자 설정에 2이상의 숫자를 입력하세요.")); //$1013
		AfxMessageBox(Fun_FindMsg("RegeditManage_OnChangeEdit14_msg1","IDD_REGEDIT_MANAGE"));
		CString strTmp;
		return;
	}
	UpdateData(FALSE);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Parallel Max Number"	, atoi(m_edit_str9));	//20180719 yulee add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Count", m_edit_num4);	//ljb 20170720 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", m_edit_num5);	//ljb 20170720 add

	//cny 201809
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseCellBal"   , m_ChkCellBalUse.GetCheck());	
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UsePwrSupply" , m_ChkPwrSupplyUse.GetCheck());	
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "UseChiller"   , m_ChkChillerUse.GetCheck());	

	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Use Isolation Safety", m_chkUseIsolSafety);	// 211015 HKH 절연모드 충전 관련 기능 보완

	UpdateData(FALSE);		
	CDialog::OnOK();
}

void CRegeditManage::OnChangeEdit14() 
{	
	UpdateData();
	if(atoi(m_edit_str9)<2) //20180719 yulee add
	{
		//&& AfxMessageBox(_T("2이상의 숫자를 입력하세요."));//$1013
		AfxMessageBox(Fun_FindMsg("RegeditManage_OnChangeEdit14_msg2","IDD_REGEDIT_MANAGE"));
		CString strTmp;
		strTmp.Format("2");
		GetDlgItem(IDC_CBO_PARALLEL_NUM)->SetWindowText(strTmp);
	}
	UpdateData(FALSE);
}