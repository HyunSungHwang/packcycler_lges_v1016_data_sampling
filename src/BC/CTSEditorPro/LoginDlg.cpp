// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "LoginDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CLoginDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CLoginDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CLoginDlg::IDD3):
	(CLoginDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CLoginDlg)
	m_bRemember = FALSE;
	m_strPassword = _T("");
	m_strLoginID = _T("guest");
	//}}AFX_DATA_INIT
	strcpy(m_LoginInfo.szLoginID, "pne");
	//strcpy(m_LoginInfo.szPassword, "pnesolution!");
	strcpy(m_LoginInfo.szPassword, "pnesolution!1"); //ksj 20200203 : 비번 번경
	m_LoginInfo.nPermission = PS_USER_SUPER;
	strcpy(m_LoginInfo.szRegistedDate, "Unknown");
	strcpy(m_LoginInfo.szUserName, "pnesolution");
	strcpy(m_LoginInfo.szDescription, "Supervisor");
	m_LoginInfo.lAutoLogOutTime = 0;
	m_LoginInfo.bUseAutoLogOut = FALSE;
}


void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoginDlg)
	DDX_Check(pDX, IDC_REMEMBER, m_bRemember);
	DDX_Text(pDX, IDC_PASSWORD, m_strPassword);
	DDX_Text(pDX, IDC_LOGINID, m_strLoginID);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	//{{AFX_MSG_MAP(CLoginDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_EN_CHANGE(IDC_PASSWORD, OnChangePassword)
	ON_EN_CHANGE(IDC_LOGINID, OnChangeLoginid)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg message handlers

void CLoginDlg::OnOk() 
{
	// TODO: Add your control notification handler code here
	if(IDPasswordCheck() == FALSE)
	{
		//AfxMessageBox("등록되어 있지 않은 사용자입니다.");
		AfxMessageBox(Fun_FindMsg("LoginDlg_OnOk_msg1","IDD_LOGON_DLG"));//&&
		return;	
	}

	CDialog::OnOK();
}

BOOL CLoginDlg::IDPasswordCheck()
{
	UpdateData(TRUE);
//	m_strLoginID.MakeUpper();
//	m_strPassword.MakeUpper();
	//if(m_strLoginID == "pne" && m_strPassword == "pnesolution!")		//System master ID(Kim Byung Hum)
	if(m_strLoginID == "pne" && m_strPassword == "pnesolution!1")		//ksj 20200203 : 비번 변경
	{
		strcpy(m_LoginInfo.szLoginID, m_strLoginID);
		strcpy(m_LoginInfo.szPassword, m_strPassword);
		m_LoginInfo.nPermission = PS_USER_SUPER;
		strcpy(m_LoginInfo.szRegistedDate, "Unknown");
		strcpy(m_LoginInfo.szUserName, "Kim Byung Hum");
		strcpy(m_LoginInfo.szDescription, "Supervisor");
		m_LoginInfo.lAutoLogOutTime = 0;
		m_LoginInfo.bUseAutoLogOut = FALSE;
		return TRUE;
	}

	if(m_strLoginID == "guest" && m_strPassword == "")				//Default Guest ID
	{
		strcpy(m_LoginInfo.szLoginID, "guest");
		strcpy(m_LoginInfo.szPassword, "");
		m_LoginInfo.nPermission = PS_USER_GUEST;
		strcpy(m_LoginInfo.szRegistedDate, "Unknown");
		strcpy(m_LoginInfo.szUserName, "defalult User");
		strcpy(m_LoginInfo.szDescription, "Guest");
		m_LoginInfo.lAutoLogOutTime = 0;
		m_LoginInfo.bUseAutoLogOut = FALSE;
		return TRUE;			
	}

	SCH_LOGIN_INFO	loginData;
	if(SearchUser(m_strLoginID, TRUE, m_strPassword, &loginData) == FALSE)
	{
		return FALSE;
	}

	m_LoginInfo = loginData;

//	::WriteLastLogID(m_LoginInfo);
	UINT nSize = sizeof(m_LoginInfo);
	LPCTSTR lpData = (LPCTSTR)&m_LoginInfo;
	AfxGetApp()->WriteProfileBinary(EDITOR_REG_SECTION, "LastLogin", (BYTE *)lpData, nSize);
	AfxGetApp()->WriteProfileInt(EDITOR_REG_SECTION, "Logset", m_bRemember);
	return TRUE;
}

BOOL CLoginDlg::SearchUser(CString strUserID, BOOL bPassWordCheck, CString strPassword, SCH_LOGIN_INFO *pUserData)
{
	CUserRecordSet rs;
	
	rs.m_strFilter.Format("[UserID] = '%s'", strUserID);

	try
	{
		rs.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}

	if(bPassWordCheck)
	{
		if(rs.m_Password != strPassword)
		{
			rs.Close();
			return FALSE;
		}
	}

	if(pUserData != NULL)
	{
		CString strTemp;
		pUserData->nPermission = rs.m_Authority;
		sprintf(pUserData->szDescription, rs.m_Description.operator const char*());
		sprintf(pUserData->szUserName, rs.m_Name.operator const char*());
		sprintf(pUserData->szPassword, rs.m_Password.operator const char*());
		strTemp = rs.m_RegistedDate.Format();
		sprintf(pUserData->szRegistedDate, strTemp.operator const char*());
		sprintf(pUserData->szLoginID, rs.m_UserID.operator const char*());
		pUserData->bUseAutoLogOut = rs.m_AutoLogOut;
		pUserData->lAutoLogOutTime = rs.m_AutoLogOutTime;
	}
	rs.Close();
	return TRUE;
}

BOOL CLoginDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	
	UINT nSize;	

	LPVOID* pData;
	if(AfxGetApp()->GetProfileBinary(EDITOR_REG_SECTION, "LastLogin", (LPBYTE *)&pData, &nSize))
	{
		if(nSize == sizeof(m_LoginInfo))	memcpy(&m_LoginInfo, pData, nSize);
		delete [] pData;
	}


	GetDlgItem(IDC_LOGINID)->SetFocus();
	
	m_strPassword.Format("%s", m_LoginInfo.szPassword);
	m_strLoginID.Format("%s", m_LoginInfo.szLoginID);

//	m_bRemember = GetPrivateProfileInt("FORMSET", "Logset", 0, INI_FILE);  // initialization file name
	
	m_bRemember = AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "Logset", 0);

	if(m_bRemember == FALSE)
	{
		m_strPassword.Empty();
	}
		
	UpdateData(FALSE);
	return FALSE;	// return TRUE unless you set the focus to a control
					// EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::OnChangePassword() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_strPassword.GetLength() > SCH_MAX_ID_SIZE)	
	{
		m_strPassword.Delete(SCH_MAX_ID_SIZE);
		UpdateData(FALSE);
	}
}

void CLoginDlg::OnChangeLoginid() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_strLoginID.GetLength() > SCH_MAX_PWD_SIZE)	
	{
		m_strLoginID.Delete(SCH_MAX_PWD_SIZE);
		UpdateData(FALSE);
	}
}