#if !defined(AFX_DLG_CHILLER_H__34CD5856_DEC6_4E41_BD23_AB6274AF18A4__INCLUDED_)
#define AFX_DLG_CHILLER_H__34CD5856_DEC6_4E41_BD23_AB6274AF18A4__INCLUDED_
//$1013
#include "CTSEditorProDoc.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Chiller.h : header file
//
#define WM_CHILLER_DLG_CLOSE		WM_USER+3004
/////////////////////////////////////////////////////////////////////////////
// Dlg_Chiller dialog

class Dlg_Chiller : public CDialog
{
// Construction
public:
	Dlg_Chiller(CWnd* pParent = NULL);   // standard constructor
	
	CCTSEditorProDoc *m_pDoc;
	CWnd *m_pParent;
	
	STEP    *m_pStep;
	int     m_nStepNo;

	void onSetObject(int iType);
	void onOndoObject(int iType);

// Dialog Data
	//{{AFX_DATA(Dlg_Chiller)
	enum { IDD = IDD_DIALOG_CHILLER , IDD2 = IDD_DIALOG_CHILLER_ENG, IDD3 = IDD_DIALOG_CHILLER_PL };
	CEdit	m_EditChillerRefPumpSet;
	CButton	m_ChkPumpOnTemp2;
	CButton	m_ChkPumpOnTemp1;
	CButton	m_ChkPumpOffTemp2;
	CButton	m_ChkPumpOffTemp1;
	CEdit	m_EditChillerRefPump;
	CButton	m_ChkChillerOnTemp2;
	CButton	m_ChkChillerOnTemp1;
	CButton	m_ChkChillerOffTemp2;
	CButton	m_ChkChillerOffTemp1;
	CComboBox	m_CboChillerTpData;
	CComboBox	m_CboChillerPumpCmd;
	CComboBox	m_CboChillerRefCmd;
	CEdit	m_EditChillerRefTemp;
	CEdit	m_EditChillerOnTemp2;
	CEdit	m_EditChillerOnTemp1;
	CEdit	m_EditChillerOffTemp2;
	CEdit	m_EditChillerOffTemp1;
	CButton	m_ChkChillerUse;
	CLabel	m_StaticStepNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Chiller)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Chiller)
	virtual void OnOK();
	virtual void OnCancel();	
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnChkChillerUse();
	afx_msg void OnChangeEditChillerRefTemp();
	afx_msg void OnChangeEditChillerOnTemp1();
	afx_msg void OnChangeEditChillerOnTemp2();
	afx_msg void OnChangeEditChillerOffTemp1();
	afx_msg void OnChangeEditChillerOffTemp2();
	afx_msg void OnSelchangeCboChillerRefCmd();
	afx_msg void OnSelchangeCboChillerPumpCmd();
	afx_msg void OnChangeEditChillerRefPump();
	afx_msg void OnChkChillerOnTemp1();
	afx_msg void OnChkChillerOnTemp2();
	afx_msg void OnChkChillerOffTemp1();
	afx_msg void OnChkChillerOffTemp2();
	afx_msg void OnSelchangeCboChillerTpdata();
	afx_msg void OnChkPumpOnTemp1();
	afx_msg void OnChkPumpOnTemp2();
	afx_msg void OnChkPumpOffTemp1();
	afx_msg void OnChkPumpOffTemp2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CHILLER_H__34CD5856_DEC6_4E41_BD23_AB6274AF18A4__INCLUDED_)
