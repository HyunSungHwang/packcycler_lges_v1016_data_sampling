// MyGridWnd.h: interface for the CMyGridWnd class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYGRIDWND_H__9855466B_D960_4A57_86EE_AA1873FB94EE__INCLUDED_)
#define AFX_MYGRIDWND_H__9855466B_D960_4A57_86EE_AA1873FB94EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "..\\PowerFormation\\Msgdefine.h"
#include "StdAfx.h"

class CMyGridWnd : public CGXGridWnd  
{
public:
	CMyGridWnd();
	virtual ~CMyGridWnd();
	virtual	void OnInitialUpdate();

// Implementation
public:
	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol);
	BOOL OnSelDragRowsMove(ROWCOL nFirstRow, ROWCOL nLastRow, ROWCOL nDestRow);
	BOOL OnSelDragRowsDrop(ROWCOL nFirstRow, ROWCOL nLastRow, ROWCOL nDestRow);
	BOOL OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol);
	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType);
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);

protected:
	//{{AFX_MSG(CMyGridWnd)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_MYGRIDWND_H__9855466B_D960_4A57_86EE_AA1873FB94EE__INCLUDED_)
