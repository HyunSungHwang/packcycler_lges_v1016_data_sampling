#if !defined(AFX_MODELSELDLG_H__63CCF86C_DE4E_4910_BFF5_A371CB5BC6F2__INCLUDED_)
#define AFX_MODELSELDLG_H__63CCF86C_DE4E_4910_BFF5_A371CB5BC6F2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModelSelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CModelSelDlg dialog
#include "MyGridWnd.h"

class CModelSelDlg : public CDialog
{
// Construction
public:
	BOOL SelectOK();
	void UpdateCount();
	CModelSelDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CModelSelDlg)
	enum { IDD = IDD_MODEL_DIALOG , IDD2 = IDD_MODEL_DIALOG_ENG , IDD3 = IDD_MODEL_DIALOG_PL  };
	CLabel	m_wndName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModelSelDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//�� ���� ��ư
	stingray::foundation::SECBitmapButton m_btnModelNew;
	stingray::foundation::SECBitmapButton m_btnModelDelete;
	stingray::foundation::SECBitmapButton m_btnModelSave;
	stingray::foundation::SECBitmapButton m_btnModelCopy;
	stingray::foundation::SECBitmapButton m_btnModelEdit;

	CMyGridWnd m_wndBatteryModelGrid;
	CWnd *m_pView;

	// Generated message map functions
	//{{AFX_MSG(CModelSelDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonNew();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonEdit();
	afx_msg void OnButtonCopy();
	afx_msg void OnButtonSave();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LONG OnGridDbClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnMovedCurrentCell(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedButLanguage();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODELSELDLG_H__63CCF86C_DE4E_4910_BFF5_A371CB5BC6F2__INCLUDED_)
