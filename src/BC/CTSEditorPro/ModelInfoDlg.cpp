// ModelInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "ModelInfoDlg.h"
#include "NewModelInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModelInfoDlg dialog
extern CString GetDataBaseName();

CModelInfoDlg::CModelInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CModelInfoDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CModelInfoDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CModelInfoDlg::IDD3):
	(CModelInfoDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CModelInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CModelInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModelInfoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModelInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CModelInfoDlg)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModelInfoDlg message handlers

void CModelInfoDlg::InitGrid()
{
	m_wndModelGrid.SubclassDlgItem(IDC_MODEL_LIST_GRID, this);
	m_wndModelGrid.Initialize();
	m_wndModelGrid.GetParam()->EnableUndo(FALSE);
	m_wndModelGrid.SetColCount(8);

	CString strTemp;
	CRect rect;
	m_wndModelGrid.GetClientRect(&rect);

#ifdef _AUTO_PROCESS_
	m_wndModelGrid.SetColWidth(0, 0, rect.Width()*0.1f);
	m_wndModelGrid.SetColWidth(1, 1, rect.Width()*0.20f);
	m_wndModelGrid.SetColWidth(2, 2, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(3, 3, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(4, 4, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(5, 5, rect.Width()*0.13f);
	m_wndModelGrid.SetColWidth(6, 6, rect.Width()*0.18f);
	m_wndModelGrid.SetColWidth(7, 7, 0);
	m_wndModelGrid.SetColWidth(8, 8, 0);
#else
	m_wndModelGrid.SetColWidth(0, 0, rect.Width()*0.1f);
	m_wndModelGrid.SetColWidth(1, 1, rect.Width()*0.3f);
	m_wndModelGrid.SetColWidth(2, 2, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(3, 3, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(4, 4, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(5, 5, rect.Width()*0.15f);
	m_wndModelGrid.SetColWidth(6, 6, 0);
	m_wndModelGrid.SetColWidth(7, 7, 0);
	m_wndModelGrid.SetColWidth(8, 8, 0);
#endif

	m_wndModelGrid.SetStyleRange(CGXRange(0, 0),	CGXStyle().SetValue("No"));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 1),	CGXStyle().SetValue(Fun_FindMsg("InitGrid_msg1","IDD_MODEL_INFO_DIALOG")));
	strTemp.Format(Fun_FindMsg("InitGrid_msg2","IDD_MODEL_INFO_DIALOG"), m_pDoc->GetCapUnit());
	m_wndModelGrid.SetStyleRange(CGXRange(0, 2),	CGXStyle().SetValue(strTemp));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 3),	CGXStyle().SetValue(Fun_FindMsg("InitGrid_msg3","IDD_MODEL_INFO_DIALOG")));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 4),	CGXStyle().SetValue(Fun_FindMsg("InitGrid_msg4","IDD_MODEL_INFO_DIALOG")));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 5),	CGXStyle().SetValue(Fun_FindMsg("InitGrid_msg5","IDD_MODEL_INFO_DIALOG")));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 6),	CGXStyle().SetValue(Fun_FindMsg("InitGrid_msg6","IDD_MODEL_INFO_DIALOG")));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 7),	CGXStyle().SetValue(Fun_FindMsg("InitGrid_msg7","IDD_MODEL_INFO_DIALOG")));
	m_wndModelGrid.SetStyleRange(CGXRange(0, 8),	CGXStyle().SetValue(Fun_FindMsg("InitGrid_msg8","IDD_MODEL_INFO_DIALOG")));

	m_wndModelGrid.SetStyleRange(CGXRange().SetCols(2, 8), CGXStyle().SetHorizontalAlignment(DT_CENTER));

	m_wndModelGrid.GetParam()->EnableUndo(TRUE);
	m_wndModelGrid.GetParam()->SetSortRowsOnDblClk(TRUE);
	m_wndModelGrid.SetStyleRange(CGXRange().SetTable(), CGXStyle().SetVertScrollBar(TRUE).SetWrapText(FALSE));
}

BOOL CModelInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(m_pDoc);
	
	// TODO: Add extra initialization here
	InitGrid();

	QueryList();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModelInfoDlg::OnButtonNew() 
{
	// TODO: Add your control notification handler code here
	//새로운 모델 등록
	CString strTemp;
	CNewModelInfoDlg *pDlg = new CNewModelInfoDlg(this);
	
	LPVOID* pData;
	UINT nSize;
	if(AfxGetApp()->GetProfileBinary(EDITOR_REG_SECTION, "LastLogin", (LPBYTE *)&pData, &nSize))
	{
		SCH_LOGIN_INFO loginData;
		if(nSize == sizeof(loginData))
		{
			memcpy(&loginData, pData, nSize);
			pDlg->m_strID = loginData.szLoginID;
		}
		delete [] pData;
	}

	if(pDlg->DoModal() == IDOK)
	{
		//새로운 모델등록 
		CString strSQL;
		CDaoDatabase  db;
		COleVariant data;

				strSQL.Format("SELECT Name FROM ModelInfo WHERE Name = '%s'", pDlg->m_strName);
		try
		{
			db.Open(GetDataBaseName());
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				//존재하는 경우 
				strTemp.Format(Fun_FindMsg("OnButtonNew_msg1","IDD_MODEL_INFO_DIALOG"), pDlg->m_strName);
				AfxMessageBox(strTemp);
			}
			else
			{
				strSQL.Format("INSERT INTO ModelInfo(Name, Width, Height, Depth, Capacity, BCRMask, Description, Creator) VALUES('%s', %f, %f, %f, %f, '%s', '%s', '%s')", 
								pDlg->m_strName, pDlg->m_fWidth, pDlg->m_fHeight, pDlg->m_fDepth, m_pDoc->CUnitTrans(pDlg->m_fCapacity), 
								pDlg->m_strBarCode, pDlg->m_strDescript, pDlg->m_strID);
				db.Execute(strSQL);
			}
			rs.Close();
			db.Close();

			//Grid에 추가 
			int nRow = m_wndModelGrid.GetRowCount()+1;
			m_wndModelGrid.InsertRows(nRow, 1);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 1), pDlg->m_strName);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 2), pDlg->m_fCapacity);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 3), pDlg->m_fWidth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 4), pDlg->m_fHeight);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 5), pDlg->m_fDepth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 6), pDlg->m_strBarCode);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 7), pDlg->m_strID);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 8), pDlg->m_strDescript);
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
		}		
	}
	delete pDlg;

	strTemp.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strTemp);

}

BOOL CModelInfoDlg::QueryList()
{
		//새로운 모델등록 
	CString strSQL;
	CDaoDatabase  db;
	COleVariant data;
	int nRow = m_wndModelGrid.GetRowCount()+1;

	strSQL = "SELECT * FROM ModelInfo ORDER BY Name";
	try
	{
		db.Open(GetDataBaseName());
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	

		while(!rs.IsEOF())
		{
				m_wndModelGrid.InsertRows(nRow, 1);
				
				data = rs.GetFieldValue("Name");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 1), CString(data.pcVal));
				data = rs.GetFieldValue("Capacity");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 2), m_pDoc->CUnitTrans(data.fltVal, FALSE));
				data = rs.GetFieldValue("Width");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 3), data.fltVal);
				data = rs.GetFieldValue("Height");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 4), data.fltVal);
				data = rs.GetFieldValue("Depth");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 5), data.fltVal);
				data = rs.GetFieldValue("BCRMask");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 6), CString(data.pcVal));
				data = rs.GetFieldValue("Creator");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 7), CString(data.pcVal));
				data = rs.GetFieldValue("Description");
				m_wndModelGrid.SetValueRange(CGXRange(nRow, 8), CString(data.pcVal));
				//Grid에 추가 

				rs.MoveNext();
		}
		rs.Close();
		db.Close();
		
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	strSQL.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strSQL);
	
	return TRUE;
}

void CModelInfoDlg::OnButtonDelete() 
{
	// TODO: Add your control notification handler code here
	CString		strTemp;
	ROWCOL nRow = m_wndModelGrid.GetRowCount();
	if(nRow <1)		return;				//Test Condition is not exist

	CRowColArray	awRows;
	m_wndModelGrid.GetSelectedRows(awRows);		//Get Selected Test Condition
	if(awRows.GetSize() <=0)	return;
	
	if(awRows.GetSize() == 1)
	{
		strTemp.Format(Fun_FindMsg("OnButtonDelete_msg1","IDD_MODEL_INFO_DIALOG"), m_wndModelGrid.GetValueRowCol(awRows[0], 1) );
	}
	else
	{
		strTemp = Fun_FindMsg("OnButtonDelete_msg2","IDD_MODEL_INFO_DIALOG");
	}

	if(IDNO == AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION))	
		return;


	CString strSQL;
	CDaoDatabase  db;
	COleVariant data;

	try
	{
		db.Open(GetDataBaseName());
		for(ROWCOL row = awRows.GetSize(); row >0; row--)
		{
			strSQL.Format("DELETE FROM ModelInfo WHERE Name = '%s'", m_wndModelGrid.GetValueRowCol(awRows[row-1], 1));
			db.Execute(strSQL);
			
			m_wndModelGrid.RemoveRows(awRows[row-1], awRows[row-1]);
		}
	
		db.Close();

	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}	
	
	strTemp.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strTemp);

}

void CModelInfoDlg::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here

	ROWCOL nRow,nCol;
	if(m_wndModelGrid.GetCurrentCell(nRow, nCol) == FALSE)
	{
		return;
	}

	if(nRow < 1)
	{
		return;
	}

	CString strTemp;
	CNewModelInfoDlg *pDlg = new CNewModelInfoDlg(this);
	
	LPVOID* pData;
	UINT nSize;
	if(AfxGetApp()->GetProfileBinary(EDITOR_REG_SECTION, "LastLogin", (LPBYTE *)&pData, &nSize))
	{
		SCH_LOGIN_INFO loginData;
		if(nSize == sizeof(loginData))
		{
			memcpy(&loginData, pData, nSize);
			pDlg->m_strID = loginData.szLoginID;
		}
		delete [] pData;
	}
	CString strOrgName = m_wndModelGrid.GetValueRowCol(nRow, 1);
	pDlg->m_strName = strOrgName;
	pDlg->m_fCapacity = atof(m_wndModelGrid.GetValueRowCol(nRow, 2));
	pDlg->m_fWidth	= atof(m_wndModelGrid.GetValueRowCol(nRow, 3));
	pDlg->m_fHeight = atof(m_wndModelGrid.GetValueRowCol(nRow, 4));
	pDlg->m_fDepth	= atof(m_wndModelGrid.GetValueRowCol(nRow, 5));
	pDlg->m_strBarCode = m_wndModelGrid.GetValueRowCol(nRow, 6);
	pDlg->m_strID = m_wndModelGrid.GetValueRowCol(nRow, 7);
	pDlg->m_strDescript = m_wndModelGrid.GetValueRowCol(nRow, 8);
	
	if(pDlg->DoModal() == IDOK)
	{
		//새로운 모델등록 
		CString strSQL;
		CDaoDatabase  db;
		COleVariant data;
		strSQL.Format("SELECT Name FROM ModelInfo WHERE Name = '%s'", pDlg->m_strName);
		try
		{
			db.Open(GetDataBaseName());
			
/*			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF() && !rs.IsEOF())
			{
				//존재하는 경우 
				strTemp.Format("%s 모델은 이미 등록되어 있습니다.", pDlg->m_strName);
				AfxMessageBox(strTemp);
			}
			else
			{
*/				strSQL.Format("UPDATE ModelInfo SET Name = '%s', Width = %f, Height=%f, Depth=%f, Capacity=%f, BCRMask = '%s', Description ='%s', Creator='%s' WHERE Name = '%s'", 
							pDlg->m_strName, pDlg->m_fWidth, pDlg->m_fHeight, pDlg->m_fDepth, m_pDoc->CUnitTrans(pDlg->m_fCapacity), 
							pDlg->m_strBarCode, pDlg->m_strDescript, pDlg->m_strID, strOrgName);
				db.Execute(strSQL);
//			}
//			rs.Close();
			db.Close();

			//Grid에 추가 
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 1), pDlg->m_strName);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 2), pDlg->m_fCapacity);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 3), pDlg->m_fWidth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 4), pDlg->m_fHeight);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 5), pDlg->m_fDepth);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 6), pDlg->m_strBarCode);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 7), pDlg->m_strID);
			m_wndModelGrid.SetValueRange(CGXRange(nRow, 8), pDlg->m_strDescript);
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
		}		
	}
	delete pDlg;
	
	strTemp.Format("%d", m_wndModelGrid.GetRowCount());
	GetDlgItem(IDC_NUM_STATIC)->SetWindowText(strTemp);
}