#if !defined(AFX_DLG_PATTSELECT_H__5E8F6AF3_4459_401A_A77C_9361EDDBAF83__INCLUDED_)
#define AFX_DLG_PATTSELECT_H__5E8F6AF3_4459_401A_A77C_9361EDDBAF83__INCLUDED_
//$1013
#include "CTSEditorProDoc.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_PattSelect.h : header file
//
#define WM_PATTSELECT_DLG_CLOSE		WM_USER+3006
/////////////////////////////////////////////////////////////////////////////
// Dlg_PattSelect dialog

class Dlg_PattSelect : public CDialog
{
// Construction
public:
	Dlg_PattSelect(CWnd* pParent = NULL);   // standard constructor

	CCTSEditorProDoc *m_pDoc;
	CWnd *m_pParent;
	
	STEP    *m_pStep;
	int     m_nStepNo;	

// Dialog Data
	//{{AFX_DATA(Dlg_PattSelect)
	enum { IDD = IDD_DIALOG_PATTSELECT , IDD2 = IDD_DIALOG_PATTSELECT_ENG, IDD3 = IDD_DIALOG_PATTSELECT_PL };
	CLabel	m_StaticStepNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_PattSelect)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_PattSelect)
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_PATTSELECT_H__5E8F6AF3_4459_401A_A77C_9361EDDBAF83__INCLUDED_)
