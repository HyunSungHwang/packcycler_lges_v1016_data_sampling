#if !defined(AFX_ENDCONDITIONDLG_H__C1738E09_89C0_40EE_95E1_D18F031BCBA2__INCLUDED_)
#define AFX_ENDCONDITIONDLG_H__C1738E09_89C0_40EE_95E1_D18F031BCBA2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EndConditionDlg.h : header file
//

// else if (canData.function_division3 == ID_CAN_WARNING)			strTemp = "CAN WARNING";
// else if (canData.function_division3 == ID_CAN_ERROR)			strTemp = "CAN ERROR";
// else if (canData.function_division3 == ID_CAN_REST_OFF)			strTemp = "CAN REST_OFF";
// else if (canData.function_division3 == ID_CAN_PAUSE)			strTemp = "CAN PAUSE";
// else if (canData.function_division3 == ID_CAN_MESSAGE)			strTemp = "CAN MESSAGE";
// else if (canData.function_division3 == ID_CAN_PAUSE_MESSAGE)	strTemp = "CAN PAUSE & MESSAGE";

// CAN WARNING
// CAN ERROR
// CAN REST_OFF
// CAN PAUSE
// CAN MESSAGE
// CAN PAUSE & MESSAGE


/////////////////////////////////////////////////////////////////////////////
// CEndConditionDlg dialog
#include "DateTimeCtrl.h"
#include "CTSEditorProDoc.h"

#define WM_END_DLG_CLOSE		WM_USER+3001
#define GOTO_LOOP_COUNT			40000		//20081202 KHS

#define ID_CAN_PAUSE			1801		//일시정지 사용
#define ID_CAN_MESSAGE			1802		//메시지 표시(SBC -> PC(EMG Message로 받음 ))
#define ID_CAN_PAUSE_MESSAGE	1803		//PAUSE & 메시지 표시
#define ID_CAN_WARNING			1804		//BMS Warning
#define ID_CAN_ERROR			1805		//BMS Error
#define ID_CAN_REST_OFF			1806		//BMS REST_OFF

class CEndConditionDlg : public CDialog
{
// Construction
public:

	int m_nCustomMsec;
	int m_nCustomMsecStepEnd;
	int m_nCustomMsecPattEnd;
	
	void Fun_FindCanAuxName(UINT uiType, int nPos, int nDivision);
	BOOL Fun_CheckString(CString strData);
	void UpdateEndGotoList();
	void InitComboList();
	void InitCanAuxComboList();
	void InitControl();
	void UpdateBMSGotoList();
	void DisplayData();
	int ListUpGotoStepCombo();
	void InitToolTips();
	BOOL UpdateEndCondition();
	int m_nStepNo;
	CEndConditionDlg(CWnd* pParent = NULL);   // standard constructor

	UINT m_nLoopInfoGoto;
	UINT m_nMultiLoopInfoCycle,m_nMultiLoopInfoGoto,m_nMultiLoopGroupID;
	UINT m_nAccLoopInfoCycle,m_nAccLoopInfoGoto,m_nAccLoopGroupID;
	UINT m_nGotoEndVtg_H,m_nGotoEndVtg_L,m_nGotoEndTime,m_nGotoEndCVTime,m_nGotoEndCapa,m_nGotoEndWattHour,m_nGotoEndValue;

	SECCurrencyEdit	m_EndV_H;
	SECCurrencyEdit	m_EndV_L;
	SECCurrencyEdit	m_EndI;
	SECCurrencyEdit	m_EndC;
//	CMyDateTimeCtrl	m_EndTime;
	SECCurrencyEdit	m_EnddV;
	SECCurrencyEdit	m_EndWatt;
	SECCurrencyEdit	m_EndWattHour;
	SECCurrencyEdit	m_EndTemp;

	CString	m_strEndMemo;
	CWnd *m_pParent;
	CToolTipCtrl m_tooltip;
	
	CRect rOrgSize;
	BOOL m_bExtViewMode;		//ljb Can 설정 까지 확장해서 보임

	CCTSEditorProDoc *m_pDoc;
	void OnOK();
//	CCTSEditorProView *m_pView;

// Dialog Data
	//{{AFX_DATA(CEndConditionDlg)
	enum { IDD = IDD_END_COND_DLG , IDD2 = IDD_END_COND_DLG_ENG, IDD3 = IDD_END_COND_DLG_PL };
	CComboBox	m_ctrlCboCanCheck;
	CDateTimeCtrl	m_ctrlWaitTime;
	CComboBox	m_ctrlCboLoaderMode;
	CComboBox	m_ctrlCboLoaderValueItem;
	CComboBox	m_ctrlAuxDivision_10;
	CComboBox	m_ctrlAuxDivision_9;
	CComboBox	m_ctrlAuxDivision_8;
	CComboBox	m_ctrlAuxDivision_7;
	CComboBox	m_ctrlAuxDivision_6;
	CComboBox	m_ctrlAuxDivision_5;
	CComboBox	m_ctrlAuxDivision_4;
	CComboBox	m_ctrlAuxDivision_3;
	CComboBox	m_ctrlAuxDivision_2;
	CComboBox	m_ctrlAuxDivision_1;
	CComboBox	m_ctrlCanDivision_10;
	CComboBox	m_ctrlCanDivision_9;
	CComboBox	m_ctrlCanDivision_8;
	CComboBox	m_ctrlCanDivision_7;
	CComboBox	m_ctrlCanDivision_6;
	CComboBox	m_ctrlCanDivision_5;
	CComboBox	m_ctrlCanDivision_4;
	CComboBox	m_ctrlCanDivision_3;
	CComboBox	m_ctrlCanDivision_2;
	CComboBox	m_ctrlCanDivision_1;
	CComboBox	m_ctrlAuxDataType10;
	CComboBox	m_ctrlAuxDataType9;
	CComboBox	m_ctrlAuxDataType8;
	CComboBox	m_ctrlAuxDataType7;
	CComboBox	m_ctrlAuxDataType6;
	CComboBox	m_ctrlAuxDataType5;
	CComboBox	m_ctrlAuxDataType4;
	CComboBox	m_ctrlAuxDataType3;
	CComboBox	m_ctrlAuxDataType2;
	CComboBox	m_ctrlAuxDataType1;
	CComboBox	m_ctrlCanDataType10;
	CComboBox	m_ctrlCanDataType9;
	CComboBox	m_ctrlCanDataType8;
	CComboBox	m_ctrlCanDataType7;
	CComboBox	m_ctrlCanDataType6;
	CComboBox	m_ctrlCanDataType4;
	CComboBox	m_ctrlCanDataType5;
	CComboBox	m_ctrlCanDataType3;
	CComboBox	m_ctrlCanDataType2;
	CComboBox	m_ctrlCanDataType1;
	CComboBox	m_ctrlCanCompare10;
	CComboBox	m_ctrlCanCompare9;
	CComboBox	m_ctrlCanCompare8;
	CComboBox	m_ctrlCanCompare7;
	CComboBox	m_ctrlCanCompare6;
	CComboBox	m_ctrlCanCompare5;
	CComboBox	m_ctrlCanCompare4;
	CComboBox	m_ctrlCanCompare3;
	CComboBox	m_ctrlCanCompare2;
	CComboBox	m_ctrlCanCompare1;
	CComboBox	m_ctrlAuxCompare10;
	CComboBox	m_ctrlAuxCompare9;
	CComboBox	m_ctrlAuxCompare8;
	CComboBox	m_ctrlAuxCompare7;
	CComboBox	m_ctrlAuxCompare6;
	CComboBox	m_ctrlAuxCompare5;
	CComboBox	m_ctrlAuxCompare4;
	CComboBox	m_ctrlAuxCompare3;
	CComboBox	m_ctrlAuxCompare2;
	CComboBox	m_ctrlAuxCompare1;
	CComboBox	m_ctrlAuxGoto_10;
	CComboBox	m_ctrlAuxGoto_9;
	CComboBox	m_ctrlAuxGoto_8;
	CComboBox	m_ctrlAuxGoto_7;
	CComboBox	m_ctrlAuxGoto_6;
	CComboBox	m_ctrlAuxGoto_5;
	CComboBox	m_ctrlAuxGoto_4;
	CComboBox	m_ctrlAuxGoto_3;
	CComboBox	m_ctrlAuxGoto_2;
	CComboBox	m_ctrlAuxGoto_1;
	CComboBox	m_ctrlCanGoto_10;
	CComboBox	m_ctrlCanGoto_9;
	CComboBox	m_ctrlCanGoto_8;
	CComboBox	m_ctrlCanGoto_7;
	CComboBox	m_ctrlCanGoto_6;
	CComboBox	m_ctrlCanGoto_5;
	CComboBox	m_ctrlCanGoto_4;
	CComboBox	m_ctrlCanGoto_3;
	CComboBox	m_ctrlCanGoto_2;
	CComboBox	m_ctrlCanGoto_1;
	CComboBox	m_ctrlCboAddWattHour;
	CComboBox	m_ctrlCboAddCap;
	CComboBox	m_ctrlCboValueItem;
	CComboBox	m_ctrlCboValueStepNo;
	CComboBox	m_ctrlCboLoopMultiGroupID;
	CComboBox	m_ctrlCboLoopAccGroupID;
	CComboBox	m_ctrlCboEndWattHour;
	CComboBox	m_ctrlCboEndVtg_L;
	CComboBox	m_ctrlCboEndVtg_H;
	CComboBox	m_ctrlCboEndValue;
	CComboBox	m_ctrlCboEndTime;
	CComboBox	m_ctrlCboEndCVTime;
	CComboBox	m_ctrlCboEndCapa;
	CComboBox	m_ctrlGoto;
	CDateTimeCtrl	m_ctrlEndTime;
	CDateTimeCtrl	m_ctrlCVTime;
	CLabel	m_strStepNo;
	CLabel	m_strStepEndMemo;
	UINT	m_nLoopInfoEndVGoto;
	UINT	m_nLoopInfoEndTGoto;
	UINT	m_nLoopInfoEndCGoto;
	BOOL	m_bUseActualCapa;
	UINT	m_nEndDay;
	UINT	m_lEndMSec;
	UINT	m_lCVTimeMSec;
	UINT	m_nCVTimeDay;
	UINT	m_ctrlEditLoopInfoCycle;
	float	m_ctrlEditfValueRate;
	float	m_fCanValue_1;
	float	m_fCanValue_2;
	float	m_fCanValue_3;
	float	m_fCanValue_4;
	float	m_fCanValue_5;
	float	m_fCanValue_7;
	float	m_fCanValue_6;
	float	m_fCanValue_8;
	float	m_fCanValue_9;
	float	m_fCanValue_10;
	float	m_fAuxValue_1;
	float	m_fAuxValue_2;
	float	m_fAuxValue_3;
	float	m_fAuxValue_4;
	float	m_fAuxValue_5;
	float	m_fAuxValue_7;
	float	m_fAuxValue_6;
	float	m_fAuxValue_8;
	float	m_fAuxValue_9;
	float	m_fAuxValue_10;
	BOOL	m_bUseCyclePause;
	BOOL	m_bUseLinkStep;
	BOOL	m_bUseChamberProg;
	float	m_ctrlEditfValueLoader;
	UINT	m_nWaitTimeDay;
	BOOL	m_bUseWaitTimeInit;
	UINT	m_edit_aux_conti_time_1;
	UINT	m_edit_aux_conti_time_2;
	UINT	m_edit_aux_conti_time_3;
	UINT	m_edit_aux_conti_time_4;
	UINT	m_edit_aux_conti_time_5;
	UINT	m_edit_aux_conti_time_6;
	UINT	m_edit_aux_conti_time_7;
	UINT	m_edit_aux_conti_time_8;
	UINT	m_edit_aux_conti_time_9;
	UINT	m_edit_aux_conti_time_10;
	BOOL	m_bChamberStepStop;
	CButton	m_bZeroVoltage; //yulee 20191017 OverChargeDischarger Mark
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEndConditionDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CUIntArray uiArryCanCode;
	CUIntArray uiArryAuxCode;
	CStringArray strArryCanName;
	CStringArray strArryAuxName;

	BOOL m_bUseHLGP; //lyj 20200730
	BOOL m_bUseChamberTemp;
	BOOL m_bUseChamberHumi;
	int ListUpValueStepCombo();

	// Generated message map functions
	//{{AFX_MSG(CEndConditionDlg)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnChangeEndDayEdit();
	afx_msg void OnDeltaposEndTimeMsecSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillFocusEndMsec();
	afx_msg void OnChangeCVTimeDayEdit();
	afx_msg void OnDeltaposCVTimeMsecSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeCboValueItem();
	afx_msg void OnChangeEndCap();
	afx_msg void OnChangeEndWatthour();
	afx_msg void OnChangeEndVtgH();
	afx_msg void OnChangeEndVtgL();
	afx_msg void OnDatetimechangeDatetimepicker2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusEditCanCategorySet1();
	afx_msg void OnChkLinkPreStep();
	afx_msg void OnButExtend();
	afx_msg void OnSelchangeCboCanDivision1();
	afx_msg void OnSelchangeCboCanDivision2();
	afx_msg void OnSelchangeCboCanDivision3();
	afx_msg void OnSelchangeCboCanDivision4();
	afx_msg void OnSelchangeCboCanDivision5();
	afx_msg void OnSelchangeCboCanDivision6();
	afx_msg void OnSelchangeCboCanDivision7();
	afx_msg void OnSelchangeCboCanDivision8();
	afx_msg void OnSelchangeCboCanDivision9();
	afx_msg void OnSelchangeCboCanDivision10();
	afx_msg void OnSelchangeCboAuxDivision1();
	afx_msg void OnSelchangeCboAuxDivision2();
	afx_msg void OnSelchangeCboAuxDivision3();
	afx_msg void OnSelchangeCboAuxDivision4();
	afx_msg void OnSelchangeCboAuxDivision5();
	afx_msg void OnSelchangeCboAuxDivision6();
	afx_msg void OnSelchangeCboAuxDivision7();
	afx_msg void OnSelchangeCboAuxDivision8();
	afx_msg void OnSelchangeCboAuxDivision9();
	afx_msg void OnSelchangeCboAuxDivision10();
	afx_msg void OnSelchangeCboLoaderValueItem();
	afx_msg void OnSelchangeCboLoaderModeValue();
	afx_msg void OnDatetimechangeWaitDatetimepicker(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeWaitDayEdit();
	afx_msg void OnEditchangeCboCanCheck();
	afx_msg void OnSelchangeCboCanCheck();
	afx_msg void OnCheckZeroVoltage(); //yulee 20191017 OverChargeDischarger Mark
	afx_msg void OnSelchangeCboCanCompare1();
	afx_msg void OnSelchangeCboCanCompare2();
	afx_msg void OnSelchangeCboCanCompare3();
	afx_msg void OnSelchangeCboCanCompare4();
	afx_msg void OnSelchangeCboCanCompare5();
	afx_msg void OnSelchangeCboCanCompare6();
	afx_msg void OnSelchangeCboCanCompare7();
	afx_msg void OnSelchangeCboCanCompare8();
	afx_msg void OnSelchangeCboCanCompare9();
	afx_msg void OnSelchangeCboCanCompare10();
	afx_msg void OnSelchangeCboAuxCompare1();
	afx_msg void OnSelchangeCboAuxCompare2();
	afx_msg void OnSelchangeCboAuxCompare3();
	afx_msg void OnSelchangeCboAuxCompare4();
	afx_msg void OnSelchangeCboAuxCompare5();
	afx_msg void OnSelchangeCboAuxCompare6();
	afx_msg void OnSelchangeCboAuxCompare7();
	afx_msg void OnSelchangeCboAuxCompare8();
	afx_msg void OnSelchangeCboAuxCompare9();
	afx_msg void OnSelchangeCboAuxCompare10();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENDCONDITIONDLG_H__C1738E09_89C0_40EE_95E1_D18F031BCBA2__INCLUDED_)
