// RegCodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "RegCodeDlg.h"
#include "RealEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegCodeDlg dialog


CRegCodeDlg::CRegCodeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CRegCodeDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CRegCodeDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CRegCodeDlg::IDD3):
	(CRegCodeDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CRegCodeDlg)
	m_labTitle = _T("");
	m_labTitle2 = _T("");
	//}}AFX_DATA_INIT

}


void CRegCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegCodeDlg)
	DDX_Text(pDX, IDC_LAB_TITLE, m_labTitle);
	DDX_Text(pDX, IDC_LAB_TITLE2, m_labTitle2);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CRegCodeDlg, CDialog)
	//{{AFX_MSG_MAP(CRegCodeDlg)
	ON_BN_CLICKED(IDC_ADD_CODE, OnAddCode)
	ON_BN_CLICKED(IDC_DELETE_CODE, OnDeleteCode)
	ON_BN_CLICKED(IDC_BUT_SAVE, OnButSave)
	ON_BN_CLICKED(IDC_CSV_IMPORT, OnCsvImport)
	ON_BN_CLICKED(IDC_CSV_EXPORT, OnCsvExport)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnGridBeginEdit)
//	ON_MESSAGE(WM_GRID_PASTE, OnGridPaste)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegCodeDlg message handlers

BOOL CRegCodeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdateData();
	m_bIsChange = FALSE;
	m_labTitle = m_strTitle;
	m_labTitle2 = m_strTitle2;
	
	InitListGrid();

	Fun_LoadData();
	
// 	//	m_recordSet.m_strFilter.Format("UserID = '%s' and Password = '%s'", m_strLoginID, m_strPassword);
// 	m_recordSet.m_strSort.Format("[UserID]");
// 	
// 	try
// 	{
// 		m_recordSet.Open();
// 	}
// 	catch (CDaoException* e)
// 	{
// 		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
// 		e->Delete();
// 		return FALSE;
// 	}
// 	
// 	RequeryUserDB();
// 	
// 	m_bUseLoginCheck = 	AfxGetApp()->GetProfileInt(EDITOR_REG_SECTION, "UseLogin", 1);
// 	
// 	if((m_pLoginData->nPermission & PMS_USER_SETTING_CHANGE) == FALSE)
// 	{
// 		GetDlgItem(IDC_ADD_USER)->EnableWindow(FALSE);
// 		GetDlgItem(IDC_DELETE_USER)->EnableWindow(FALSE);
// 	}
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CRegCodeDlg::InitListGrid()
{
	m_CodeListGrid.SubclassDlgItem(IDC_CODE_LIST_GIRD, this);
	m_CodeListGrid.Initialize();
	m_CodeListGrid.SetRowCount(0);
	m_CodeListGrid.SetColCount(3);
	
//	m_CodeListGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetControl(GX_IDS_CTRL_PASSWORD));
	
	//Enable Tooltips
	CRect rect;
	m_CodeListGrid.GetClientRect(&rect);
	//yulee 20180927
	float width = rect.Width()/100.0f;
	m_CodeListGrid.SetColWidth(0, 0, (int)(width*3.0f));
	m_CodeListGrid.SetColWidth(1, 1, (int)(width*7.0f));
	m_CodeListGrid.SetColWidth(2, 2, (int)(width*30.0f));
	m_CodeListGrid.SetColWidth(3, 3, (int)(width*60.0f));
/*
	float width = rect.Width()/100.0f;
	m_CodeListGrid.SetColWidth(0, 0, (int)(width*10.0f));
	m_CodeListGrid.SetColWidth(1, 1, (int)(width*20.0f));
	m_CodeListGrid.SetColWidth(2, 2, (int)(width*30.0f));
	m_CodeListGrid.SetColWidth(3, 3, (int)(width*40.0f));
*/
// 	m_CodeListGrid.SetColWidth(4, 4, (int)(width*10.0f));
// 	m_CodeListGrid.SetColWidth(5, 5, 0);
// 	m_CodeListGrid.SetColWidth(6, 6, 0);
// 	m_CodeListGrid.SetColWidth(7, 7, (int)(width*25.0f));
// 	m_CodeListGrid.SetColWidth(8, 8, (int)(width*30.0f));
	
	//Table setting
	m_CodeListGrid.GetParam()->EnableTrackColWidth(TRUE);	//no Resize Col Width
	//	m_CodeListGrid.SetStyleRange(CGXRange().SetTable(),	CGXStyle().SetFont(CGXFont().SetSize(9).SetFaceName("굴림")));
//	m_CodeListGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetInterior(RGB(192,192,192)));
	
// 	m_CodeListGrid.SetStyleRange(CGXRange().SetCols(5),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 		.SetHorizontalAlignment(DT_CENTER)
// 		);
	
	//Table Name
	m_CodeListGrid.SetStyleRange(CGXRange(0, 0), CGXStyle().SetValue("No"));
	m_CodeListGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("CODE"));
	m_CodeListGrid.SetStyleRange(CGXRange(0, 2), CGXStyle().SetValue("Name"));
	m_CodeListGrid.SetStyleRange(CGXRange(0, 3), CGXStyle().SetValue("Description"));
// 	m_CodeListGrid.SetStyleRange(CGXRange(0, 4), CGXStyle().SetValue("권한"));
// 	m_CodeListGrid.SetStyleRange(CGXRange(0, 5), CGXStyle().SetValue("자동Logout"));
// 	m_CodeListGrid.SetStyleRange(CGXRange(0, 6), CGXStyle().SetValue("자동Logout 대기시간"));
// 	m_CodeListGrid.SetStyleRange(CGXRange(0, 7), CGXStyle().SetValue("등록일"));
// 	m_CodeListGrid.SetStyleRange(CGXRange(0, 8), CGXStyle().SetValue("이름"));

	m_CodeListGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_CodeListGrid, IDS_CTRL_REALEDIT));
	m_CodeListGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_CodeListGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_CodeListGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	char str[12], str2[7];//, str3[5];
	
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 5);		//정수부
	
	m_CodeListGrid.SetStyleRange(CGXRange().SetCols(1),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("1"))
//		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("30000"))
//		.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(1~30000)"))
//		.SetValue(0L)
		
	);
	
	//Edit Ctrl////////////////////////////////////////
	m_CodeListGrid.SetStyleRange(CGXRange().SetCols(2,3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
	);
	
	return TRUE;
}

BOOL CRegCodeDlg::Fun_LoadData()
{
	CString strTempMDB;
	strTempMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	strTempMDB = strTempMDB+ "\\" + "Pack_Schedule_2000.mdb";
	
	CDaoDatabase  db;
	try
	{
		//db.Open(::GetDataBaseName());
		db.Open(strTempMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL,strInsertSQL;
	//strSQL.Format("DELETE FROM ChannelCode1" );
	//strSQL.Format("SELECT * FROM DivisionCode WHERE Division = %d",m_iDivision );
	//db.Execute(strSQL);
	
	// 	CString strSQL;
	// 	//strSQL.Format("SELECT ProcType, ProcID, Description FROM ProcType WHERE ProcType >= %d ORDER BY ProcType", EP_PROC_TYPE_CHARGE_LOW);
	// 	//strSQL.Format("SELECT ProcType, ProcID, Description FROM ProcType WHERE ProcType >= %d ORDER BY ProcType", EP_PROC_TYPE_CHARGE_LOW);
	// 	strSQL.Format("DELETE FROM MesCode WHERE *.* ")
	// 	
	CString strCodeName,strDesc;
	int iCode;
	strSQL.Format("SELECT * FROM DivisionCode WHERE Division = %d order by Code",m_iDivision );
	//strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode1");
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	
	CRowColArray ra;
	int nRow = 1;
	CString strTemp;
	m_arryCode.RemoveAll();

	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue("Code");
			iCode = data.lVal;
			strTemp.Format("%d",iCode);
			data = rs.GetFieldValue("Code_Name");
			if(VT_NULL != data.vt) strCodeName.Format("%s", data.pbVal);
			else strCodeName.Empty();
			data = rs.GetFieldValue("Description");
			if(VT_NULL != data.vt) strDesc.Format("%s", data.pbVal);
			else strDesc.Empty();

			if (m_nUserType == ID_REG_USER_NOMAL)	//사용자
			{
				if (m_iDivision == ID_CAN_TYPE)
				{
					if ((iCode >= 1001 && iCode <= 1200) || (iCode >= 10010 && iCode <= 10014))
					{
						m_CodeListGrid.InsertRows(m_CodeListGrid.GetRowCount()+1, 1);
						m_CodeListGrid.SetValueRange(CGXRange(nRow, 1), strTemp);
						m_CodeListGrid.SetValueRange(CGXRange(nRow, 2), strCodeName);
						m_CodeListGrid.SetValueRange(CGXRange(nRow, 3), strDesc);						
						nRow++;
						m_arryCode.Add(iCode);
					}
					rs.MoveNext();
				}
				else if (m_iDivision == ID_AUX_TYPE)
				{
					if (iCode >= 2001 && iCode <= 2200)
					{
						m_CodeListGrid.InsertRows(m_CodeListGrid.GetRowCount()+1, 1);
						m_CodeListGrid.SetValueRange(CGXRange(nRow, 1), strTemp);
						m_CodeListGrid.SetValueRange(CGXRange(nRow, 2), strCodeName);
						m_CodeListGrid.SetValueRange(CGXRange(nRow, 3), strDesc);
						nRow++;
						m_arryCode.Add(iCode);
					}
					rs.MoveNext();
				}
			}
			else if (m_nUserType == ID_REG_USER_MANAGE)	//관리자
			{
				m_CodeListGrid.InsertRows(m_CodeListGrid.GetRowCount()+1, 1);
				m_CodeListGrid.SetValueRange(CGXRange(nRow, 1), strTemp);
				m_CodeListGrid.SetValueRange(CGXRange(nRow, 2), strCodeName);
				m_CodeListGrid.SetValueRange(CGXRange(nRow, 3), strDesc);
				nRow++;
				rs.MoveNext();
			}
			else
			{
				rs.MoveNext();
			}

		}	
	}
	rs.Close();
	db.Close();
	return TRUE;
}

void CRegCodeDlg::OnAddCode() 
{
	CRowColArray ra;
	m_CodeListGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_CodeListGrid.InsertRows(ra[0] + 1, 1);
	else
		m_CodeListGrid.InsertRows(m_CodeListGrid.GetRowCount()+1, 1);
		
	IsChangeFlag(TRUE);	
}

void CRegCodeDlg::IsChangeFlag(BOOL IsFlag)
{
	m_bIsChange = IsFlag;
	GetDlgItem(IDC_BUT_SAVE)->EnableWindow(IsFlag);
	//GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsFlag);

}

void CRegCodeDlg::OnDeleteCode() 
{
	// TODO: Add your control notification handler code here
	CRowColArray ra;
	m_CodeListGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_CodeListGrid.RemoveRows(ra[i], ra[i]);
	}
	
	IsChangeFlag(TRUE);	
}

void CRegCodeDlg::OnButSave() 
{
	// TODO: Add your control notification handler code here
	if (Fun_SaveData())
	{
		int nTotTestNum = m_CodeListGrid.GetRowCount();
		if(nTotTestNum >0)	m_CodeListGrid.RemoveRows( 1, nTotTestNum);

// 		CRowColArray ra;
// 		m_CodeListGrid.GetSelectedRows(ra);
// 		
// 		for (int i = ra.GetSize()-1; i >=0 ; i--)
// 		{
// 			if(ra[i] == 0) //Header 삭제불가
// 				continue;
// 			m_CodeListGrid.RemoveRows(ra[i], ra[i]);
// 		}
		Fun_LoadData();
		if (Fun_SaveData()) AfxMessageBox("Save END");

	}
	
	
}

BOOL CRegCodeDlg::Fun_SaveData()
{
	CString strFindCode, strFindCode2, strTemp;
	CString strFindName, strFindName2; //ksj 20200728
	int i,j;
	int nCode, nRow;

	//중복 코드가 있는지 CODE 번호가 제대로 입력 됐는지 확인
	for(i=1; i<=m_CodeListGrid.GetRowCount(); i++)
	{
		nRow = i;
		strFindCode = m_CodeListGrid.GetValueRowCol(i, 1);
		strFindName = m_CodeListGrid.GetValueRowCol(i, 2);
		nCode = atoi(strFindCode);
		if (m_iDivision == ID_CAN_TYPE)// && nCode > 2000)
		{
			if (m_nUserType == ID_REG_USER_NOMAL)
			{
				//if  ( (nCode >= 1001 && nCode <= 1200) || (nCode >= 10010 && nCode <= 10014))
				if( (nCode >= 1000 && nCode <= 1200) || (nCode >= 10010 && nCode <= 10014)) //20180712 yulee 2018-07-10 (화) 오후 5:05 메일 상 코드 정리로 변경 
				{

				}
				else
				{
					//&& strTemp.Format("%d 번째 CAN CODE 입력 범위가 맞지 않습니다. 등록 가능 CODE 범위를 확인 하세요. (저장 실패)",nRow);
					strTemp.Format(Fun_FindMsg("RegCodeDlg_Fun_SaveData_msg1","IDD_REG_CODE_DLG"),nRow);
					AfxMessageBox(strTemp);//$1013
					return FALSE;
				}
			}
// 			if (nCode > 9999 && nCode < 20000 )
// 			{
// 
// 			}
// 			else
// 			{
// 			}
		}
		else if (m_iDivision == ID_AUX_TYPE)
		{
			if (m_nUserType == ID_REG_USER_NOMAL)
			{
				//if  ( (nCode >= 2001 && nCode <= 2200))
				if((nCode >= 2000 && nCode <= 2200))//20180712 yulee 2018-07-10 (화) 오후 5:05 메일 상 코드 정리로 변경 
				{
				}
				else
				{
					//&& strTemp.Format("%d 번째 외부데이터 CODE 입력 범위가 맞지 않습니다. 등록 가능 CODE 범위를 확인 하세요. (저장 실패)",nRow);
					strTemp.Format(Fun_FindMsg("RegCodeDlg_Fun_SaveData_msg2","IDD_REG_CODE_DLG"),nRow);
					AfxMessageBox(strTemp);//$1013
					return FALSE;
				}

// 				if (nCode > 19999 && nCode < 30000 )	//ljb 20170828 add Aux 특수코드 범위 등록 가능 20000 ~ 29999 
// 				{
// 					
// 				}
// 				else
// 				{
// 					if  ( nCode < 2001 || nCode > 3000)
// 					{
// 					}
// 				}
			}
		}

		for(j=1; j<=m_CodeListGrid.GetRowCount(); j++)
		{
			if (nRow == j) continue;
			strFindCode2 = m_CodeListGrid.GetValueRowCol(j, 1);
			strFindName2 = m_CodeListGrid.GetValueRowCol(j, 2);
			if (strFindCode == strFindCode2)
			{
				//ksj 20200728 : 중복코드 허용 예외조건 추가. 
				//Name이 Rx_ 또는 Tx_로 시작하는 경우 같은 Rx_ 또는 Tx_ 항목 내에 코드가 중복되는 경우에만 저장 못하도록 한다. (김재호c 요청)

				CString strPrefix1, strPrefix2;

				strPrefix1 = strFindName.Left(3); //앞에 3글자만 자르기
				strPrefix2 = strFindName2.Left(3); //앞에 3글자만 자르기
				strPrefix1.MakeLower(); //소문자로 변환
				strPrefix2.MakeLower(); //소문자로 변환

				//앞 3글자가 tx_또는 rx_ 인 경우에는 두개의 앞 글자가 똑같으면 실패
				if(strPrefix1.Compare("tx_") == 0 || strPrefix1.Compare("rx_") == 0)
				{
					if(strPrefix1.Compare(strPrefix2) == 0)
					{
						//&& strTemp.Format("%d 번째 Code와 %d 번째 Code 번호가 같습니다.(저장 실패)",nRow,j);
						strTemp.Format(Fun_FindMsg("RegCodeDlg_Fun_SaveData_msg3","IDD_REG_CODE_DLG"),nRow,j);
						AfxMessageBox(strTemp);
						return FALSE;
					}
				}
				else
				{
					//&& strTemp.Format("%d 번째 Code와 %d 번째 Code 번호가 같습니다.(저장 실패)",nRow,j);
					strTemp.Format(Fun_FindMsg("RegCodeDlg_Fun_SaveData_msg3","IDD_REG_CODE_DLG"),nRow,j);
					AfxMessageBox(strTemp);
					return FALSE;
				}

				//ksj end

				//&& strTemp.Format("%d 번째 Code와 %d 번째 Code 번호가 같습니다.(저장 실패)",nRow,j);
// 				strTemp.Format(Fun_FindMsg("RegCodeDlg_Fun_SaveData_msg3","IDD_REG_CODE_DLG"),nRow,j);
// 				AfxMessageBox(strTemp);
// 				return FALSE;
			}
		}
	
	}
	//////////////////////////////////////////////////////////////////////////

	CString strMDB;
	strMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	strMDB = strMDB+ "\\" + "Pack_Schedule_2000.mdb";
	
	CDaoDatabase  db;
	try
	{
		//db.Open(::GetDataBaseName());
		db.Open(strMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL,strInsertSQL;
	CString strCode,strName,strDesc;
	UINT	uiCode;

	if (m_nUserType == ID_REG_USER_MANAGE) //전체 삭제 후 다시 insert
	{
		strSQL.Format("DELETE FROM DivisionCode WHERE Division = 0");		//사용하지 않는 Division 코드 임.
		db.Execute(strSQL);
		strSQL.Format("DELETE FROM DivisionCode WHERE Division = %d",m_iDivision );
		db.Execute(strSQL);
//		db.CanTransact();
		Sleep(100);
		
		if (m_CodeListGrid.GetRowCount() < 1) return FALSE;
		
		for(i=1; i<=m_CodeListGrid.GetRowCount(); i++)
		{
			strCode = m_CodeListGrid.GetValueRowCol(i, 1);
			if (strCode.IsEmpty()) continue;
			strName = m_CodeListGrid.GetValueRowCol(i, 2);
			strDesc = m_CodeListGrid.GetValueRowCol(i, 3);
			
			//------------- Insert
			strInsertSQL.Format("INSERT INTO DivisionCode(Code, Code_Name,Division, Description) VALUES ( %d, '%s', %d, '%s')", 
				atoi(strCode),  
				strName,
				m_iDivision,
				strDesc
				);
			db.Execute(strInsertSQL);
			//--------------------------------
		}
		db.Close();
	}
	else
	{
		//사용자 등록 코드 관리
		if (m_arryCode.GetSize() > 0)
		{
			//Load 한 division code 삭제
			for( int i=0; i < m_arryCode.GetSize(); i++)
			{
				uiCode = m_arryCode.GetAt(i);
				if (uiCode >= 10010 && uiCode <= 10014) continue;
				strSQL.Format("DELETE FROM DivisionCode WHERE Code = %d",uiCode );
				db.Execute(strSQL);
				Sleep(10);
			}
			//db.CanTransact();
		}

		for(i=1; i<=m_CodeListGrid.GetRowCount(); i++)
		{
			strCode = m_CodeListGrid.GetValueRowCol(i, 1);
			uiCode = atol(strCode);
			if (strCode.IsEmpty()) continue;
			strName = m_CodeListGrid.GetValueRowCol(i, 2);
			strDesc = m_CodeListGrid.GetValueRowCol(i, 3);
			
			if (uiCode >= 10010 && uiCode <= 10014)
			{
				//------------- update
				strInsertSQL.Format("UPDATE DivisionCode SET Code_Name='%s', Description='%s' WHERE Code = %d", 
				strName, strDesc, uiCode);
				//strInsertSQL.Format("UPDATE DivisionCode SET Code_Name='%s' WHERE Code = %d", 
				//	strName, uiCode);
				db.Execute(strInsertSQL);
				//--------------------------------
			}
			else
			{
				//------------- Insert
				strInsertSQL.Format("INSERT INTO DivisionCode(Code, Code_Name,Division, Description) VALUES ( %d, '%s', %d, '%s')", 
					atoi(strCode),  
					strName,
					m_iDivision,
					strDesc
					);
				db.Execute(strInsertSQL);
				//--------------------------------
			}
		}
		db.Close();
	}


	// 	CString strSQL;
	// 	//strSQL.Format("SELECT ProcType, ProcID, Description FROM ProcType WHERE ProcType >= %d ORDER BY ProcType", EP_PROC_TYPE_CHARGE_LOW);
	// 	//strSQL.Format("SELECT ProcType, ProcID, Description FROM ProcType WHERE ProcType >= %d ORDER BY ProcType", EP_PROC_TYPE_CHARGE_LOW);
	// 	strSQL.Format("DELETE FROM MesCode WHERE *.* ")
	// 	
// 	int iNum;
// 	strSQL.Format("SELECT Code, Messge, Description FROM ChannelCode1");
// 	COleVariant data;
// 	CDaoRecordset rs(&tempdb);
// 	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
// 	
// 	if(!rs.IsBOF() && !rs.IsEOF())
// 	{
// 		COleVariant data;
// 		while(!rs.IsEOF())
// 		{
// 			data = rs.GetFieldValue(0);
// 			iNum = data.lVal;
// 			data = rs.GetFieldValue(1);
// 			if(VT_NULL != data.vt) strMessage.Format("%s", data.pbVal);
// 			data = rs.GetFieldValue(2);
// 			if(VT_NULL != data.vt) strDesc.Format("%s", data.pbVal);
// 			
// 			//------------- Insert MesCode
// 			strInsertSQL.Format("INSERT INTO ChannelCode1(Code, Messge, Description) VALUES ( %d, '%s', '%s')", 
// 				iNum,  
// 				strMessage,
// 				strDesc
// 				);
// 			db.Execute(strInsertSQL);
// 			//--------------------------------
// 			rs.MoveNext();
// 		}	
// 	}
// 	rs.Close();
// 	db.Close();
	return TRUE;
}
LRESULT CRegCodeDlg::OnGridBeginEdit(WPARAM wParam, LPARAM lParam)
{
	IsChangeFlag(TRUE);
	return 1;
}
// void CRegCodeDlg::OnGridPaste(WPARAM wParam, LPARAM lParam)
// {
// 	IsChangeFlag(TRUE);
// }


//ksj 20181021 : CSV파일에서 코드를 불러와 추가한다.
void CRegCodeDlg::OnCsvImport() 
{
	CString strFindCode, strFindCode2, strTemp;
	//int i,j;
	//int nCode;
	int nRow = 1;
	
	CString strFileName;
	CFileDialog dlg(TRUE, "csv", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK == dlg.DoModal())
	{
		strFileName = dlg.GetPathName();
	}
	else
	{
		return;
	}
	
	FILE* fp = fopen(strFileName,"rt");
	
	if(!fp)
	{
		AfxMessageBox("Could not load file.");
		return;
	}

	char szBuffer[2048];
	CString strCode,strName,strDesc;

	fgets(szBuffer,sizeof(szBuffer),fp);
	strTemp = szBuffer;
	if(strTemp.Left(4).Compare("CODE"))
	{
		AfxMessageBox("Could not load file.");
		return;
	}
	
	if(m_CodeListGrid.GetRowCount() > 0)
		m_CodeListGrid.RemoveRows(1,m_CodeListGrid.GetRowCount());

	while(fgets(szBuffer,sizeof(szBuffer),fp) != NULL)
	{
		strTemp = szBuffer;

		AfxExtractSubString(strCode,strTemp,0,',');
		AfxExtractSubString(strName,strTemp,1,',');
		AfxExtractSubString(strDesc,strTemp,2,',');

		m_CodeListGrid.InsertRows(m_CodeListGrid.GetRowCount()+1, 1);
		m_CodeListGrid.SetValueRange(CGXRange(nRow, 1), strCode);
		m_CodeListGrid.SetValueRange(CGXRange(nRow, 2), strName);
		m_CodeListGrid.SetValueRange(CGXRange(nRow, 3), strDesc);

		nRow++;
		
		if(nRow>100) break; //100개 초과시 멈춤.
	}
	
	fclose(fp);

	IsChangeFlag(TRUE);	
}

//ksj 20181021 : CSV파일로 코드를 내보낸다
void CRegCodeDlg::OnCsvExport() 
{

	CString strFindCode, strFindCode2, strTemp;
	int i;
	//int j;
	//int nCode, nRow;
		
	if (m_CodeListGrid.GetRowCount() < 1) return;

	CString strFileName;
	CFileDialog dlg(FALSE, "csv", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
	if(IDOK == dlg.DoModal())
	{
		strFileName = dlg.GetPathName();
	}
	else
	{
		return;
	}

	FILE* fp = fopen(strFileName,"wt");

	if(!fp)
	{
		AfxMessageBox("Could not save file.");
		return;
	}

	fprintf(fp,"CODE,NAME,DESC\n");

	CString strCode,strName,strDesc;
	for(i=1; i<=m_CodeListGrid.GetRowCount(); i++)
	{
		strCode = m_CodeListGrid.GetValueRowCol(i, 1);
		if (strCode.IsEmpty()) continue;
		strName = m_CodeListGrid.GetValueRowCol(i, 2);
		strDesc = m_CodeListGrid.GetValueRowCol(i, 3);

		fprintf(fp,"%s,%s,%s\n",strCode,strName,strDesc);
	}

	fclose(fp);

}
