// Dlg_SocTableEdit.cpp : implementation file
//
//$1013
#include "stdafx.h"
#include "CTSEditorPro.h"
#include "Dlg_SocTableEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_SocTableEdit dialog


Dlg_SocTableEdit::Dlg_SocTableEdit(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_SocTableEdit::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_SocTableEdit::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_SocTableEdit::IDD3):
	(Dlg_SocTableEdit::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_SocTableEdit)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bEditMode=FALSE;
	m_IsChange=FALSE;
}


void Dlg_SocTableEdit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_SocTableEdit)
	DDX_Control(pDX, IDC_SOCTABLE_COUNT, m_StaticSocTableCount);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_SocTableEdit, CDialog)
	//{{AFX_MSG_MAP(Dlg_SocTableEdit)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUT_DEL, OnButDel)
	ON_BN_CLICKED(IDC_BUT_INSERT, OnButInsert)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDbClicked)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_SocTableEdit message handlers

BOOL Dlg_SocTableEdit::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	InitSocTableGrid();

	if(m_bEditMode==TRUE)
	{
		LoadData();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_SocTableEdit::OnClose() 
{
	CDialog::OnClose();
}


void Dlg_SocTableEdit::OnOK() 
{
	UpdateData();

	CString strSaveData=_T("");
	CString strTemp,strTemp2;
	int nRowCnt,nColCnt;
	int i;

	nRowCnt = m_wndSocTableGrid.GetRowCount();
	nColCnt = m_wndSocTableGrid.GetColCount();
	
	FILE *fp = fopen(m_strEditFileName, "wt");

	if(fp != NULL)
	{
		//주석 save
		//strSaveData = "여기는 주석 구문이 위치하는 곳입니다. 어떤 내용을 쓰던지 동작과는 전혀 상관없습니다.\n";
		strSaveData = Fun_FindMsg("Dlg_SocTableEdit_OnOK_msg1","DLG_SOCTABLEEDIT");//&&
		//strSaveData += "아래 ""STX"" 로 부터는 실제로 Soc Table 을 수행하는 데이터입니다.\n";
		strSaveData += Fun_FindMsg("Dlg_SocTableEdit_OnOK_msg2","DLG_SOCTABLEEDIT"); //&&
		//strSaveData += "T1(누적시간),T2(동작시간),Temp(온도),I(전류),P(파워)\n";
		strSaveData += Fun_FindMsg("Dlg_SocTableEdit_OnOK_msg3","DLG_SOCTABLEEDIT");//&&
		//strSaveData += "I-S(시작전류),I-E(종지전류),P-S(시작파워),P-E(종지파워),M(사각파:R  삼각파:T)\n";
		//strSaveData += "수정시에는 포맷에 맞추어 작성하시기 바랍니다.,\n\n";
		strSaveData += Fun_FindMsg("Dlg_SocTableEdit_OnOK_msg4","DLG_SOCTABLEEDIT");//&&

		//STX save
		strSaveData += "STX_SOC\n";
				
		fprintf(fp, "%s", strSaveData);	//File Write
		strSaveData.Empty();

		//data save		
		for( int i=0; i < nRowCnt ; i++)
		{
			strSaveData.Format(_T("%s,%s,%s"),
				                m_wndSocTableGrid.GetValueRowCol(i+1, ID_SOCTABLE_COL_SOC),
								m_wndSocTableGrid.GetValueRowCol(i+1, ID_SOCTABLE_COL_VOLT),
								m_wndSocTableGrid.GetValueRowCol(i+1, ID_SOCTABLE_COL_CURR));
			strSaveData += "\n";

			fprintf(fp, "%s", strSaveData);	//File Write
			strSaveData.Empty();
		}
		fprintf(fp, "%s", strSaveData);	//File Write
		fclose(fp);
		fp = NULL;
	}
	else
	{
		strTemp.Format("File Save error (%s)",m_strEditFileName);
		AfxMessageBox(strTemp);
		return;
	}
	fp = NULL;
	
	CDialog::OnOK();
}

void Dlg_SocTableEdit::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL Dlg_SocTableEdit::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam==VK_RETURN ||
			pMsg->wParam==VK_ESCAPE )
		{		
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL Dlg_SocTableEdit::InitSocTableGrid()
{
	CString strType;
	CRect rect, cellRect;
	m_wndSocTableGrid.SubclassDlgItem(IDC_SOCTABLE_GRID, this);
	m_wndSocTableGrid.Initialize();

	m_wndSocTableGrid.GetParam()->EnableUndo(FALSE);
	m_wndSocTableGrid.GetParam()->EnableMoveRows(FALSE);		//Drag and Drop 지원
	
	m_wndSocTableGrid.SetRowCount(ID_SOCTABLE_ROW_COUNT);
	m_wndSocTableGrid.SetColCount(ID_SOCTABLE_COL_COUNT);		//ljb
	m_wndSocTableGrid.GetClientRect(&rect);
	
	m_wndSocTableGrid.SetColWidth(ID_SOCTABLE_COL_NO		,ID_SOCTABLE_COL_NO			, 50);
	m_wndSocTableGrid.SetColWidth(ID_SOCTABLE_COL_SOC		,ID_SOCTABLE_COL_SOC		, 150);
	m_wndSocTableGrid.SetColWidth(ID_SOCTABLE_COL_VOLT		,ID_SOCTABLE_COL_VOLT		, 150);	
	m_wndSocTableGrid.SetColWidth(ID_SOCTABLE_COL_CURR		,ID_SOCTABLE_COL_CURR		, 150);	
	
	m_wndSocTableGrid.SetStyleRange(CGXRange(0,ID_SOCTABLE_COL_NO			),	CGXStyle().SetValue("No"));
	m_wndSocTableGrid.SetStyleRange(CGXRange(0,ID_SOCTABLE_COL_SOC			),	CGXStyle().SetValue("Soc(%)"));
	//m_wndSocTableGrid.SetStyleRange(CGXRange(0,ID_SOCTABLE_COL_VOLT			),	CGXStyle().SetValue("전압(V)"));
	m_wndSocTableGrid.SetStyleRange(CGXRange(0,ID_SOCTABLE_COL_VOLT			),	CGXStyle().SetValue(Fun_FindMsg("Dlg_SocTableEdit_InitSocTableGrid_msg1","DLG_SOCTABLEEDIT")));//&&
	//m_wndSocTableGrid.SetStyleRange(CGXRange(0,ID_SOCTABLE_COL_CURR			),	CGXStyle().SetValue("전류(I)"));
	m_wndSocTableGrid.SetStyleRange(CGXRange(0,ID_SOCTABLE_COL_CURR			),	CGXStyle().SetValue(Fun_FindMsg("Dlg_SocTableEdit_InitSocTableGrid_msg2","DLG_SOCTABLEEDIT")));//&&
			
	m_wndSocTableGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndSocTableGrid.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndSocTableGrid.SetStyleRange(CGXRange().SetCols(0),
		                            CGXStyle().SetHorizontalAlignment(DT_CENTER) );
	///////////////////////////////////////////////////

	m_wndSocTableGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndSocTableGrid, IDS_CTRL_REALEDIT));
	m_wndSocTableGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndSocTableGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndSocTableGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
		
	char str[12], str2[7], str3[5];
	sprintf(str3, "%d", 3);		//소숫점 이하
	sprintf(str, "%%.%dlf", 3);		
	sprintf(str2, "%d", 4);		//정수부

	m_wndSocTableGrid.SetStyleRange(CGXRange().SetCols(ID_SOCTABLE_COL_SOC),
									CGXStyle().SetControl(IDS_CTRL_REALEDIT)
									          .SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
									          .SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
									          .SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
									          .SetValue(0L) );

	m_wndSocTableGrid.SetStyleRange(CGXRange().SetCols(ID_SOCTABLE_COL_VOLT),
									CGXStyle().SetControl(IDS_CTRL_REALEDIT)
									          .SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
									          .SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
									          .SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
									          .SetValue(0L) );

	m_wndSocTableGrid.SetStyleRange(CGXRange().SetCols(ID_SOCTABLE_COL_CURR),
									CGXStyle().SetControl(IDS_CTRL_REALEDIT)
									          .SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
									          .SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
									          .SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
									          .SetValue(0L) );

	m_wndSocTableGrid.GetParam()->EnableUndo(TRUE);
	return TRUE;
}

void Dlg_SocTableEdit::LoadData()
{
	CString	strErrorMsg;
	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;
	
	CStringList strColList;
	
	if(m_wndSocTableGrid.GetRowCount() > 0)
		m_wndSocTableGrid.RemoveRows(1, m_wndSocTableGrid.GetRowCount());
	
	if( !aFile.Open( m_strEditFileName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		//strErrorMsg.Format("%s 파일을 열 수 없습니다.", m_strEditFileName);
		strErrorMsg.Format(Fun_FindMsg("Dlg_SocTableEdit_LoadData_msg1","DLG_SOCTABLEEDIT"), m_strEditFileName); //&&
		AfxMessageBox(strErrorMsg);
		return;
	}

	int nRow;
	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Find(_T("STX_SOC"))>-1)
		{
			int p1=0, s=0;
			
			////////////////////////////////////////////////////////////////////////
			while(aFile.ReadString(strTemp))
			{
				if (strTemp == "") break;

				p1 = s = 0;
				int nCol = ID_SOCTABLE_COL_SOC;

				//Insert Grid
				m_wndSocTableGrid.InsertRows(m_wndSocTableGrid.GetRowCount()+1, nCol);
				nRow = m_wndSocTableGrid.GetRowCount();
				
				CString str1=theApp.str_GetSplit(strTemp,0,',');
				CString str2=theApp.str_GetSplit(strTemp,1,',');
				CString str3=theApp.str_GetSplit(strTemp,2,',');

				m_wndSocTableGrid.SetValueRange(CGXRange(nRow, nCol++), str1);
				m_wndSocTableGrid.SetValueRange(CGXRange(nRow, nCol++), str2);
				m_wndSocTableGrid.SetValueRange(CGXRange(nRow, nCol++), str3);

				if(nRow>=ID_SOCTABLE_ROW_COUNT) break;
			}			
		}	
	}
	aFile.Close();
	SetRowCount();
	m_wndSocTableGrid.Redraw();
}

LONG Dlg_SocTableEdit::OnGridDbClicked(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col
		
	if(nRow < 1)	return 0L;
	
	if(pGrid == (CMyGridWnd *)&m_wndSocTableGrid)			//Test List Grid
	{
		IsChangeFlag(TRUE);
	}	
	return 0;
}

void Dlg_SocTableEdit::OnButInsert() 
{
	CRowColArray ra;
	m_wndSocTableGrid.GetSelectedRows(ra);
	
	if(ra.GetSize() > 0)
	{
		m_wndSocTableGrid.InsertRows(ra[0] + 1, 1);
	}
	else
	{
		int nLastRow = m_wndSocTableGrid.GetRowCount();
		m_wndSocTableGrid.InsertRows(nLastRow+1, 1);
	}	
	IsChangeFlag(TRUE);	
	SetRowCount();
}

void Dlg_SocTableEdit::OnButDel() 
{
	CRowColArray ra;
	m_wndSocTableGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) continue;//Header

		m_wndSocTableGrid.RemoveRows(ra[i], ra[i]);
	}
	IsChangeFlag(TRUE);	
	SetRowCount();
}

void Dlg_SocTableEdit::IsChangeFlag(BOOL IsFlag)
{
	m_IsChange = IsFlag;
	GetDlgItem(IDOK)->EnableWindow(m_IsChange);
}

void Dlg_SocTableEdit::SetRowCount()
{
	int nCount=m_wndSocTableGrid.GetRowCount();

	m_StaticSocTableCount.SetWindowText(theApp.str_IntToStr(nCount));
}