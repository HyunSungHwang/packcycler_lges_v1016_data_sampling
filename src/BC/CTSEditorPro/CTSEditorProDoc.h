// CTSEditorProDoc.h : interface of the CCTSEditorProDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSEditorProDOC_H__DBF5825C_6297_46F0_8F69_38007690585C__INCLUDED_)
#define AFX_CTSEditorProDOC_H__DBF5825C_6297_46F0_8F69_38007690585C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct tagProcDataTypeIDMessage {
	int nCode;
	char szMessage[SCH_MAX_CODE_SIZE];
	int nDataType;
} DATA_CODE;

#define MAX_UNDO_COUNT	10
#define MAX_COMPARE_STEP_COUNT	10

class CCTSEditorProDoc : public CDocument
{
protected: // create from serialization only
	CCTSEditorProDoc();
	DECLARE_DYNCREATE(CCTSEditorProDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSEditorProDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	
	STEP * GetStepDataGotoID(int nGotoID /*One Base*/);			//20120222 KHS
	void Fun_SetParallelMode(BOOL bFlag) { m_bParallelMode = bFlag; }	//ljb 201134 이재복 //////////
	//+2015.9.22 USY Add For PatternCV
	BOOL Fun_GetParallelMode(){return m_bParallelMode;}
	//-
	void Fun_GetArryCanDivision(CUIntArray & arryCanDivision);
	void Fun_GetArryAuxDivision(CUIntArray & arryCanDivision);
	void Fun_GetArryCanStringName(CStringArray & arryCanStringName);
	void Fun_GetArryAuxStringName(CStringArray & arryAuxStringName);
	BOOL Fun_ReloadDivisionCode();
	BOOL GetCanNAuxCat(LONG lTestID, CStringArray &strArrCanCode, CStringArray &strArrAuxCode, CStringArray &strArrCanName, CStringArray &strArrAuxName); //yulee 20180906
	BOOL GetCanNAuxName(LONG lTestID, CStringArray &strArrCanName, CStringArray &strArrAuxName); //yulee 20180906
	BOOL SearchStr(CStringArray &strArrValue, CString strCode, int nType); //yulee 20180906
	CString GetDivisionNameFromDB(int iSelCode); //yulee 20180906
	CString GetDivisionNameCodeFromDB(int iSelCode);
	int Fun_MDBUpdateDivision(CString strModelName, CString strTestName, int nID, CString strCodeName, int nType); //yulee 20180906

	BOOL RequeryGradeType();
//	long m_lMaxChargeCurrent;
	CString GetSimulationTitle(CString strFileName);
	int GetLastRefCapStepNo(int nCurStepNo);
	long m_lMaxCurrent1;
	long m_lLoadedProcType;
	STEP * GetLastStepData();
	float WattHourUnitTrans(float fData, BOOL bSave = TRUE);
	float WattUnitTrans(float fData, BOOL bSave = TRUE);
	float CUnitTrans(float fData, BOOL bSave = TRUE);
	float IUnitTrans(float fData, BOOL bSave = TRUE);
	float VUnitTrans(float fData, BOOL bSave = TRUE);
	long m_lVRefHigh;
	long m_lVRefLow;
	BOOL RedoStepEdit();
	void RemoveUndoStep();
	BOOL UpdoStepEdit(int &nStartStep, int &nEndStep);
	void MakeUndoStep();
	int		GetVtgUnitFloat()	{	return m_VtgUnitInfo.nFloatPoint;	}
	int		GetCrtUnitFloat()	{	return m_CrtUnitInfo.nFloatPoint;	}
	int		GetCapUnitFloat()	{	return m_CapUnitInfo.nFloatPoint;	}
	int		GetWattUnitFloat()	{	return m_WattUnitInfo.nFloatPoint;	}
	int		GetWattHourUnitFloat()	{	return m_WattHourUnitInfo.nFloatPoint;	}

	CString GetCrtUnit()	{	return m_CrtUnitInfo.szUnit;	};
	CString GetVtgUnit()	{	return m_VtgUnitInfo.szUnit;	};
	CString GetCapUnit()	{	return m_CapUnitInfo.szUnit;	};
	CString GetWattUnit()	{	return m_WattUnitInfo.szUnit;	};
	CString GetWattHourUnit()	{	return m_WattHourUnitInfo.szUnit;	};
	BOOL IsEdited();
	void RemoveInitStepArray();
//	BOOL m_bCycleType;
//	BOOL m_bEDLCType;
	BOOL m_bUseLogin;
	BOOL m_bEditedFlag;
	BOOL BackUpDataBase();
	BOOL InsertStep(int nStepIndex, STEP *pStep);
	BOOL PasteStep(int nStepNo);
	BOOL AddCopyStep(int nStepNo);
	BOOL ClearCopyStep();
	int m_nGradingStepLimit;		//Grading 입력 할 수 있는 최대수 
	CString m_strLastErrorString;
	int GetProcIndex(int nID);
	int GetProcID(int nIndex);
	BOOL RequeryProcType();
	int PermissionCheck(int nAction);
//	CString m_strCurFolder;
//	CString m_strDBFolder;
	SCH_LOGIN_INFO m_LoginData;

	int RequeryStepGrade(STEP *pStep, LONG lStepID);
	int SavePreTestParam(LONG lTestID);
	int SaveStepGrade(STEP *pStep, LONG lStepID);
	int StepValidityCheck();
	int SaveStep(LONG lTestID, BOOL bLinkChamber, int nType = 0);
	BOOL AddNewStep(int nStepNum, STEP *pCopyStep = NULL);
	BOOL RequeryPreTestParam(LONG lTestID);
	STEP * GetStepData(int nStepNum /*One Base*/);
	int GetTotalStepNum();

	CString MakeEndString(int nStepNum);
	CString MakeCellBalString(int nStepNum,int iType=0);//cny
	CString MakePwrSupplyString(int nStepNum,int iType=0);//cny
	CString MakeChillerString(int nStepNum,int iType=0);//cny
	CString MakePattSchedString(int nStepNum);//cny
	CString MakePattSelectString(int nStepNum);//cny

	BOOL ReMoveStepArray(int nStepNum = SCH_STEP_ALL);
	int RequeryStepData(LONG lTestID);
	CPtrArray m_apStep;
	CPtrArray m_apCopyStep;
	CPtrArray m_apUpdoStep;

	CPtrList m_aPtrArrayList;	//List of CPtrArray pointer
	POSITION m_posUndoList;


	CPtrArray m_apProcType;
	CPtrArray m_apGradeType;		//20081208 KHS	Grade Type 추가

	TEST_PARAM m_sPreTestParam;

//	int m_VoltageUnitMode;
//	int m_CurrentUnitMode;

	UNIT_INFO m_CrtUnitInfo;
	UNIT_INFO m_VtgUnitInfo;
	UNIT_INFO m_CapUnitInfo;
	UNIT_INFO m_WattUnitInfo;
	UNIT_INFO m_WattHourUnitInfo;
	
	long	m_nMinRefI;
	
	LONG	m_lUserMaxPower;		//ljb 20130722 add
	LONG	m_lMaxCurrent;
	LONG	m_lMaxVoltage;

	virtual ~CCTSEditorProDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

private:
	BOOL m_bParallelMode;	//ljb 201134 이재복 //////////

	CUIntArray	m_uiArryCanDivition;	//ljb 2011218 이재복 //////////
	CUIntArray	m_uiArryAuxDivition;	//ljb 2011218 이재복 //////////
	CStringArray m_strArryCanName;		//ljb 2011218 이재복 //////////
	CStringArray m_strArryAuxName;		//ljb 2011218 이재복 //////////

protected:

// Generated message map functions
protected:
	BOOL RemoveUndoList(POSITION pos);
	CPtrArray m_apInitStep;	//초기 Loading시 복사본 변경 여부 확인를 위해 
	TEST_PARAM m_sInitPreTestParam;


	//{{AFX_MSG(CCTSEditorProDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSEditorProDOC_H__DBF5825C_6297_46F0_8F69_38007690585C__INCLUDED_)
