// Dlg_PattSched.cpp : implementation file
//
//$1013
#include "stdafx.h"
#include "CTSEditorPro.h"
#include "Dlg_PattSched.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_PattSched dialog


Dlg_PattSched::Dlg_PattSched(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_PattSched::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_PattSched::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_PattSched::IDD3):
	(Dlg_PattSched::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_PattSched)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pDoc=NULL;
	m_pStep=NULL;
	m_pParent=pParent;
}


void Dlg_PattSched::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_PattSched)
	DDX_Control(pDX, IDC_STATIC_STEPNO, m_StaticStepNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_PattSched, CDialog)
	//{{AFX_MSG_MAP(Dlg_PattSched)
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_PattSched message handlers

BOOL Dlg_PattSched::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_StaticStepNo.SetFontAlign(DT_LEFT);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_PattSched::OnOK() 
{
	//CDialog::OnOK();
}

void Dlg_PattSched::OnCancel() 
{
	//CDialog::OnCancel();
}

void Dlg_PattSched::OnClose() 
{	
	//CDialog::OnClose();
}

BOOL Dlg_PattSched::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam==VK_RETURN ||
			pMsg->wParam==VK_ESCAPE )
		{
			ShowWindow(SW_HIDE);
			return TRUE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_PattSched::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if(!m_pDoc) return;	
	if(m_nStepNo < 1) return;
	
	CString strTemp;
	
	if(bShow==TRUE)
	{
		strTemp.Format(_T("Step : %3d"), m_nStepNo);
		m_StaticStepNo.SetText(strTemp);
		
		if(m_pStep)
		{
			
		}
	}
	else
	{
		if(m_pParent)
		{
			m_pParent->PostMessage(WM_PATTSCHED_DLG_CLOSE, (WPARAM)m_nStepNo, (LPARAM) this);			
		}		
	}	
}
