#if !defined(AFX_REGEDITMANAGE_H__2385A532_2233_46E4_90A4_CEFB1C18B2C2__INCLUDED_)
#define AFX_REGEDITMANAGE_H__2385A532_2233_46E4_90A4_CEFB1C18B2C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegeditManage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRegeditManage dialog

class CRegeditManage : public CDialog
{
// Construction
public:

	CRegeditManage(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRegeditManage)
	enum { IDD = IDD_REGEDIT_MANAGE , IDD2 = IDD_REGEDIT_MANAGE_ENG , IDD3 = IDD_REGEDIT_MANAGE_PL };
	CButton	m_ChkChillerUse;		
	CButton	m_ChkPwrSupplyUse;
	CButton	m_ChkCellBalUse;
	BOOL	m_check1;
	BOOL	m_check2;
	BOOL	m_check3;
	BOOL	m_check4;
	BOOL	m_check5;
	BOOL	m_check6;
	BOOL	m_check7;
	BOOL	m_check8;
	BOOL	m_check9;
	BOOL	m_check10;
	BOOL	m_check11;
	UINT	m_edit_num1;
	UINT	m_edit_num2;
	UINT	m_edit_num3;
	CString	m_edit_str1;
	CString	m_edit_str2;
	CString	m_edit_str3;
	CString	m_edit_str4;
	CString	m_edit_str5;
	UINT	m_edit_num4;
	UINT	m_edit_num5;
	BOOL	m_check12;
	BOOL	m_check13;
	CString	m_edit_str6;
	CString	m_edit_str7;
	CString	m_edit_str8;
	BOOL	m_check14;
	CString	m_edit_str9;
	CString	m_edit_str10;
	BOOL	m_chkUseIsolSafety;			// 211015 HKH 절연모드 충전 관련 기능 보완
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegeditManage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRegeditManage)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChangeEdit14();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGEDITMANAGE_H__2385A532_2233_46E4_90A4_CEFB1C18B2C2__INCLUDED_)
