// WorkSTSignEditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "WorkSTSignEditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWorkSTSignEditDlg dialog


CWorkSTSignEditDlg::CWorkSTSignEditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWorkSTSignEditDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWorkSTSignEditDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CWorkSTSignEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWorkSTSignEditDlg)
	DDX_Control(pDX, IDC_STATIC_PLACESIGN, m_CntrlPlaceSign);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWorkSTSignEditDlg, CDialog)
	//{{AFX_MSG_MAP(CWorkSTSignEditDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_LBUTTONDOWN()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUT_CLOSE, OnButClose)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWorkSTSignEditDlg message handlers
BOOL CWorkSTSignEditDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString strPlaceName;
	strPlaceName = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Place Name", "N/A");
	m_CntrlPlaceSign.SetWindowText(strPlaceName);
	InitControlLbl();
	

	SIZE sizeMonitor;

	ZeroMemory(&sizeMonitor, sizeof(SIZE));
	sizeMonitor.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
	sizeMonitor.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);

	CRect dlgRect;
// 	dlgRect.left = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "PlaceSignLeft", 500);
// 	dlgRect.top = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "PlaceSignTop", 500);
 	dlgRect.left = sizeMonitor.cx-550; //yulee 20190719 CTSEditorPro (ver. v1013 g.1.3.2.p20190718)
 	dlgRect.top = 0;

	CRect winRect;
	this->GetClientRect(&winRect);
	
	dlgRect.right = dlgRect.left + winRect.Width();
	dlgRect.bottom = dlgRect.top + winRect.Height();

	SetWinPos(dlgRect);
	return TRUE;
}

void CWorkSTSignEditDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{			
	CDialog::OnShowWindow(bShow, nStatus);	
}

void CWorkSTSignEditDlg::SetWinPos(CRect rect)
{
	::SetWindowPos(m_hWnd, HWND_NOTOPMOST, rect.left, rect.top, rect.Width(), rect.Height(), SWP_SHOWWINDOW);
}

void CWorkSTSignEditDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);
}

void CWorkSTSignEditDlg::InitControlLbl()
{
	m_CntrlPlaceSign.SetFontSize(30)
		.SetTextColor(RGB(0, 0, 0))
		.SetBkColor(RGB(255,255,255))
		.SetFontBold(TRUE)
		.SetFontName("HY������M");
}

BOOL CWorkSTSignEditDlg::PreTranslateMessage(MSG* pMsg) 
{
	int nID = 0;
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam==VK_ESCAPE) return TRUE;
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam==VK_RETURN) return TRUE;
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam==VK_SPACE) return TRUE;
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CWorkSTSignEditDlg::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	CRect rect;
	GetClientRect(rect);
	pDC->FillSolidRect( rect, RGB(0,0,0) );
	
   return TRUE;
}

void CWorkSTSignEditDlg::OnButClose() 
{
	// TODO: Add your control notification handler code here
	CDialog::OnCancel();
}

