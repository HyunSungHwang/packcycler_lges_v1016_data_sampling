#if !defined(AFX_CUserMapDlg_H__465E420A_C7CE_4694_9159_9B924068E5B0__INCLUDED_)
#define AFX_CUserMapDlg_H__465E420A_C7CE_4694_9159_9B924068E5B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserMapDlg.h : header file
//

#include "MyGridWnd.h"
#include "GridComboBox.h"

#include "TextParsing.h"
#include "ProgressDlg.h"



/////////////////////////////////////////////////////////////////////////////
// CUserMapDlg dialog

class CUserMapDlg : public CDialog
{
private:
	BOOL	IsNum(CString rotatePer);
	void	UserMapSelectDataChange(int nType);
	BOOL	SaveValidation();
	BOOL	CreateUserMapFile();
	BOOL	SaveUserMapFile(CString strFilePath = "");
	BOOL	SetGridReadFile(CString strFile);	
	BOOL	LoadData(CString strFile);
	BOOL	InitUserMapGrid();
	CString SelectUserMapFile(CString strFile);
	CString GetFileName(CString str);
	//-------------------	
	CString m_strNewUserMapFile;		
	CString m_strErrorMsg;
	CString m_strDataTitle;
	
	BOOL IsChange;		
	BOOL IsChangeSelFile;		
	
	BOOL DeleteTempFile(CString strFile = "");
	BOOL MakeSimulationFile(CString strExistingFileName, CString strNexFileName);
	BOOL DrawGraph();	
	BOOL SetDataSheet(CString strFile);
	BOOL UpdateRawDataInfo(CString strFile);	
	CString GetDataTitle();
	CString GetPathName();	
	CStringList& StringSplit(CString string,CString separation,CStringList& array);
	
			
	CProgressDlg * m_pProgress;					
	CTextParsing m_SheetData;
	CMyGridWnd m_wndUserMapGrid;
	

// Construction
public:	
	CString m_strUserMapFile;				//Usermap파일 경로.
	char m_chMode;							//설정 모드값 CC,CP
	STEP * pStep;
	CUserMapDlg(CWnd* pParent = NULL);		// standard constructor


// Dialog Data
	//{{AFX_DATA(CUserMapDlg)
	enum { IDD = IDD_USER_MAP_DLG , IDD2 = IDD_USER_MAP_DLG_ENG , IDD3 = IDD_USER_MAP_DLG_PL };
	CDateTimeCtrl	m_CtrlEndTime;
	CString	m_CtrlLabFileName;
	int		m_ChkDynamicTime;
	int		m_ChkDynamicMode_P;
	int		m_ChkCharge;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserMapDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CUserMapDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonOpen();
	afx_msg void OnButtonSaveAs();
	afx_msg void OnRadioDynamicTime();
	afx_msg void OnRadioCumulativeTime();
	afx_msg void OnRadioDynamicModeP();
	afx_msg void OnRadioDynamicModeI();
	afx_msg void OnRadioCharge();
	afx_msg void OnRadioDischarge();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CUserMapDlg_H__465E420A_C7CE_4694_9159_9B924068E5B0__INCLUDED_)
