// CommonInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditorPro.h"
#include "CommonInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommonInputDlg dialog


CCommonInputDlg::CCommonInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCommonInputDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCommonInputDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CCommonInputDlg::IDD3):
	(CCommonInputDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CCommonInputDlg)
	m_fValue = 0.0f;
	//}}AFX_DATA_INIT
	m_nMode = 0;
}


void CCommonInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommonInputDlg)
	DDX_Text(pDX, IDC_VALUE_EDIT, m_fValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCommonInputDlg, CDialog)
	//{{AFX_MSG_MAP(CCommonInputDlg)
	ON_BN_CLICKED(IDC_ALL_STEP_BUTTON, OnAllStepButton)
	ON_BN_CLICKED(IDC_CHARGE_BUTTON, OnChargeButton)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommonInputDlg message handlers

BOOL CCommonInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(!m_strTitleString.IsEmpty())
	{
		GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(m_strTitleString);
	}
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCommonInputDlg::OnAllStepButton() 
{
	// TODO: Add your control notification handler code here
	m_nMode = 1;
	UpdateData();
	CDialog::OnOK();
}

void CCommonInputDlg::OnChargeButton() 
{
	// TODO: Add your control notification handler code here
	m_nMode = 2;
	UpdateData();
	CDialog::OnOK();	
}

void CCommonInputDlg::OnButton3() 
{
	// TODO: Add your control notification handler code here
	m_nMode = 3;
	UpdateData();
	CDialog::OnOK();	
}
