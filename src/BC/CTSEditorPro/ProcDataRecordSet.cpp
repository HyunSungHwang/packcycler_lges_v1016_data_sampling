// ProcDataRecordSet.cpp : implementation file
//

#include "stdafx.h"
#include "CTSEditor.h"
#include "ProcDataRecordSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProcDataRecordSet

IMPLEMENT_DYNAMIC(CProcDataRecordSet, CRecordset)

CProcDataRecordSet::CProcDataRecordSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CProcDataRecordSet)
	m_ProgressID = 0;
	m_Name = _T("");
	m_DataType = 0;
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}

CString CProcDataRecordSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=TestLog");
}

CString CProcDataRecordSet::GetDefaultSQL()
{
	return _T("[ReportDataList]");
}

void CProcDataRecordSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CProcDataRecordSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Int(pFX, _T("[ProgressID]"), m_ProgressID);
	RFX_Text(pFX, _T("[Name]"), m_Name);
	RFX_Int(pFX, _T("[DataType]"), m_DataType);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CProcDataRecordSet diagnostics

#ifdef _DEBUG
void CProcDataRecordSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CProcDataRecordSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
