// stdafx.cpp : source file that includes just the standard includes
//	CTSEditorPro.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

CString g_strpathIni;
int gi_Language;

void init_Language() 
{
	gi_Language=0;
	g_strpathIni = "";
	CString strDefault, getPath;
	strDefault.Format("Don't Read");
	gi_Language = AfxGetApp()->GetProfileInt("Config","Language",1);
	char path[MAX_PATH];
	GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	getPath=(LPSTR)path;
	int i = getPath.ReverseFind('\\');
	getPath = getPath.Left(i);
	getPath.Replace("\\","\\\\");

	switch(gi_Language)	
	{
	case 1	:	{

		g_strpathIni.Format("%s\\Language_PackCycler\\EditorPro\\Korean.ini", getPath);
				}break;

	case 2	:	{
		g_strpathIni.Format("%s\\Language_PackCycler\\EditorPro\\English.ini", getPath);
				}break;

	case 3	:	{
		g_strpathIni.Format("%s\\Language_PackCycler\\EditorPro\\Polish.ini", getPath);
				}break;
	default:
		g_strpathIni.Format("%s\\Language_PackCycler\\EditorPro\\Korean.ini", getPath);
		break;
	}
}

CString Fun_FindMsg(CString strMsg, CString strFindMsg)
{
	CString strDefault;
	CString strReturn;
	strDefault.Format("Don't Read");
	strReturn = GIni::ini_GetFile(g_strpathIni,_T(strFindMsg),_T(strMsg),strDefault);
	if((strReturn.IsEmpty() == FALSE)||(strReturn != _T(""))) //yulee 20190610_1
		return strReturn;
	else
		return strDefault;
}