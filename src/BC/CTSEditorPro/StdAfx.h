// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__9E473411_6574_4AA2_9224_16E9CF02DB45__INCLUDED_)
#define AFX_STDAFX_H__9E473411_6574_4AA2_9224_16E9CF02DB45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#define _AFXDLL //ksj 20190125

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <grid/gxall.h>
#include <toolkit/secall.h>


#ifndef __GDI_PLUS__
#define __GDI_PLUS__
#pragma comment(lib, "gdiplus.lib")
#include <gdiplus.h>
using namespace Gdiplus;
#endif


#include "TabGlobal.h"

#pragma warning(disable:4146)//unary minus operator applied to unsigned type

#include "../../BCLib/Include/DataEditor.h"
#include "../../BCLib/Include/PSCommonAll.h"
//#include <DataEditor.h>			//For Editor data structure defie
#include "../../../DataBase/DataBaseAll.h"
//#include <PSCommonAll.h>		//for PSCommon.dll

#define EDITOR_REG_SECTION			"Config"
#include "./Global/IniParse/IniParser.h"


#ifdef _DEBUG
//#pragma comment(lib, "../../BCLib/Lib/PSCommonD.lib")
#pragma comment(lib, "../../../lib/PSCommonD.lib") //ksj 20200911
#pragma message("Automatically linking with PSCommonD.lib By K.B.H ")
#else
//#pragma comment(lib, "../../BCLib/Lib/PSCommon.lib")
#pragma comment(lib, "../../../lib/PSCommon.lib") //ksj 20200911
#pragma message("Automatically linking with PSCommon.lib By K.B.H")
#endif	//_DEBUG

//Cell ����
//#define _NIMH_CELL_		//Cell Type //	_EDLC_CELL_, _NIMH_CELL_, _LIION_
//#define _EDLC_CELL_		//Cell Type //	_EDLC_CELL_, _NIMH_CELL_, _LIION_
#define _LIION_

//#define _PACK_MODEL_TYPE

#define MIN_TIME_INTERVAL	100
// #ifdef _PACK_MODEL_TYPE
// #define MIN_TIME_INTERVAL	50
// #else
// #define MIN_TIME_INTERVAL	100
// #endif

#ifdef _FORMATION_
#define _AUTO_PROCESS_
#endif

#include "NameVer.h"

extern CString g_strpathIni;
extern int gi_Language;
CString Fun_FindMsg(CString strMsg, CString strFindMsg);
void init_Language();

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__9E473411_6574_4AA2_9224_16E9CF02DB45__INCLUDED_)
