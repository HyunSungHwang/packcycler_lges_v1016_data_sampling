// ConditionFile.h: interface for the CConditionFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONDITIONFILE_H__9D217C97_D1D2_4590_9CED_EECE9C8BFE27__INCLUDED_)
#define AFX_CONDITIONFILE_H__9D217C97_D1D2_4590_9CED_EECE9C8BFE27__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CConditionFile  
{
public:
	BOOL WriteFile();
	CConditionFile();
	virtual ~CConditionFile();

protected:
	CString m_strFileName;
	STR_CONDITION_HEADER	m_modelData;
	STR_CONDITION_HEADER	m_testData;
	EP_PRETEST_PARAM		m_cellCheckParam;
};

#endif // !defined(AFX_CONDITIONFILE_H__9D217C97_D1D2_4590_9CED_EECE9C8BFE27__INCLUDED_)
