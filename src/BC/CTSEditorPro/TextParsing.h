// TextParsing.h: interface for the CTextParsing class.
//
//////////////////////////////////////////////////////////////////////


#if !defined(AFX_TEXTPARSING_H__AEFEAF96_7399_4E67_92EC_8C8278213743__INCLUDED_)
#define AFX_TEXTPARSING_H__AEFEAF96_7399_4E67_92EC_8C8278213743__INCLUDED_

#define START_SIMULATION_POINT "STX"
#define END_SIMULATION_POINT "EOF"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
typedef struct PS_TIMESET
{
	int nHour;
	int nMin;
	int nSec;
	int nMs;
}PSTimeSet;

class CTextParsing  
{
private:
	//2014.09.03 문자열 추가.
	CStringList& StringSplit(CString string,CString separation,CStringList& array);

public:
	CString MakeValidTime(CString strTime);
	CString GetStringTimePoint(CString strTime,  BOOL IsRunningTime = FALSE);
	void Clear();
	void SetParent(CWnd * pWnd);
	BOOL SetSimulationFile(CString strFileName, CString &strErrorMsg);	
	//2014.09.03 Usermap 파일 설정
	BOOL SetUserMapFile(CString strFileName, CString &strErrorMsg);

	CString GetColumnHeader(int nColIndex);
	float * GetColumnData(int nColIndex);
	BOOL SetFile(CString strFileName);
	CTextParsing();
	virtual ~CTextParsing();
	int	GetRecordCount()	{	return m_nRecordCount;	}
	int GetReadRecordSize()	{	return m_ReadRecordCount; } //ksj 20200605 : 추가. GetRecordCount()가 이상해서 별도의 함수 추가.
	int	GetColumnSize()		{	return m_nColumnSize;	}
	CString m_strFileName;

	BOOL SetSocTableFile(CString strFileName, CString &strErrorMsg);	


	//[20191024] Edited By Skh 
	BOOL SetSimulationHeadFile(CString strFileName, CString &strErrorMsg);
	BOOL SetSimulationRowFile(CString strFileName, CString &strErrorMsg);

	CStdioFile aFile;
	CFileException e;

	CStringList m_ReadOffetHistory;
	CStringList m_AccTimeHistory;
	BOOL bFirstLineRead;
	int m_HistoryOffSetPos;
	long m_ReadRecordCount;
	long m_ReadRecordSize;
	int m_MaxPageFlag;


protected:
	float **m_ppData;
	int m_nColumnSize;
	int m_nRecordCount;	
	CStringList m_TitleList;
	PSTimeSet	oldTime;
	
	CWnd * pWnd;
};

#endif // !defined(AFX_TEXTPARSING_H__AEFEAF96_7399_4E67_92EC_8C8278213743__INCLUDED_)
