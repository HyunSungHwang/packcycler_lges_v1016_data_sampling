#if !defined(AFX_EDITSIMULDATADLG_H__42BF9694_B86C_449F_9EFC_8228F9E86423__INCLUDED_)
#define AFX_EDITSIMULDATADLG_H__42BF9694_B86C_449F_9EFC_8228F9E86423__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditSimulDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEditSimulDataDlg dialog
#include "MyGridWnd.h"
#include "GridComboBox.h"

// CSettingPatternDlg dialog
//+2015.9.22 USY Mod For PatternCV
#define PATTERN_COL_NO				0
#define PATTERN_COL_TIME			1
#define PATTERN_COL_DATA1			2
#define PATTERN_COL_DATA2			3
#define PATTERN_COL_DATA3			4	//ljb 20150902 add	charge current limit
#define PATTERN_COL_DATA4			5	//ljb 20150902 add	discharge current limit
#define PATTERN_COL_MODE			6
// #define PATTERN_COL_NO				0
// #define PATTERN_COL_TIME			1
// #define PATTERN_COL_DATA1			2
// #define PATTERN_COL_DATA2			3
// #define PATTERN_COL_DATA3			3	//ljb 20150902 add	charge current limit
// #define PATTERN_COL_DATA4			4	//ljb 20150902 add	discharge current limit
// #define PATTERN_COL_MODE			5
//-

class CEditSimulDataDlg : public CDialog
{
// Construction
public:
	
	BOOL Fun_TimeSaveError(CString strTime);
	void IsChangeFlag(BOOL IsFlag);
	void LoadData();
	CMyGridWnd m_wndPatternGrid;
	CGridComboBox *m_pModeCombo;

	BOOL InitPatternGrid();
	CEditSimulDataDlg(CWnd* pParent = NULL);   // standard constructor

public:
	CString		m_strEditFileName;	//수정 파일
	char		m_cPatternTime;		//동작시간
	char		m_chMode;			//전류,파워,전압
	BOOL		m_bEditMode;
	BOOL		IsChange;

	//+2015.9.22 USY Add for PatternCV
	LONG	m_lMaxCurrent;
	LONG	m_lMaxVoltage;
	//-
private:
	float	fOldTime, fSaveTime;

// Dialog Data
	//{{AFX_DATA(CEditSimulDataDlg)
	enum { IDD = IDD_PATTERN_EDITOR_DLG , IDD2 = IDD_PATTERN_EDITOR_DLG_ENG , IDD3 = IDD_PATTERN_EDITOR_DLG_PL };
	CDateTimeCtrl	m_ctrlEndTime;
	CString	m_LabFileName;
	CString	m_LabRowCount;
	UINT	m_lEndMSec;
	int		m_nChkMode;
	int		m_nChkTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditSimulDataDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CEditSimulDataDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButInsert();
	afx_msg void OnButDel();
	afx_msg void OnRadioMode1();
	afx_msg void OnRadioMode2();
	afx_msg void OnRadioMode3();
	afx_msg void OnRadioTime1();
	afx_msg void OnRadioTime2();
	afx_msg void OnRadioTime3();
	afx_msg void OnButTriangle();
	afx_msg void OnButSquare();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButAllSelect();
	afx_msg void OnDeltaposEndTimeMsecSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButTime();
	//}}AFX_MSG
	afx_msg LRESULT OnGridComboSelected(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITSIMULDATADLG_H__42BF9694_B86C_449F_9EFC_8228F9E86423__INCLUDED_)
