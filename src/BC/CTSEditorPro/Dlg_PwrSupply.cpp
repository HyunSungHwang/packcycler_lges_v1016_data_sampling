// Dlg_PwrSupply.cpp : implementation file
//
//$1013
#include "stdafx.h"
#include "CTSEditorPro.h"
#include "Dlg_PwrSupply.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_PwrSupply dialog


Dlg_PwrSupply::Dlg_PwrSupply(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_PwrSupply::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_PwrSupply::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_PwrSupply::IDD3):
	(Dlg_PwrSupply::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_PwrSupply)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pDoc=NULL;
	m_pStep=NULL;
	m_pParent=pParent;
}


void Dlg_PwrSupply::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_PwrSupply)	
	DDX_Control(pDX, IDC_CBO_PWRSUPPLY_VOLT, m_CboPwrSupplyVolt);
	DDX_Control(pDX, IDC_STATIC_STEPNO, m_StaticStepNo);
	DDX_Control(pDX, IDC_RAD_PWRSUPPLY_ON, m_RadPwrSupplyON);
	DDX_Control(pDX, IDC_RAD_PWRSUPPLY_OFF, m_RadPwrSupplyOFF);
	DDX_Control(pDX, IDC_RAD_PWRSUPPLY_NONE, m_RadPwrSupplyNONE);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_PwrSupply, CDialog)
	//{{AFX_MSG_MAP(Dlg_PwrSupply)
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()	
	ON_BN_CLICKED(IDC_RAD_PWRSUPPLY_ON, OnRadPwrsupplyOn)
	ON_BN_CLICKED(IDC_RAD_PWRSUPPLY_OFF, OnRadPwrsupplyOff)
	ON_BN_CLICKED(IDC_RAD_PWRSUPPLY_NONE, OnRadPwrsupplyNone)
	ON_CBN_EDITCHANGE(IDC_CBO_PWRSUPPLY_VOLT, OnEditchangeCboPwrsupplyVolt)
	ON_CBN_SELCHANGE(IDC_CBO_PWRSUPPLY_VOLT, OnSelchangeCboPwrsupplyVolt)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_PwrSupply message handlers

BOOL Dlg_PwrSupply::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_StaticStepNo.SetFontAlign(DT_LEFT);
		
	m_CboPwrSupplyVolt.AddString(_T("12"));
	m_CboPwrSupplyVolt.AddString(_T("24"));	
	m_CboPwrSupplyVolt.SetWindowText(_T("0"));

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_PwrSupply::OnOK() 
{
	//CDialog::OnOK();
}

void Dlg_PwrSupply::OnCancel() 
{
	//CDialog::OnCancel();
}

void Dlg_PwrSupply::OnClose() 
{	
	//CDialog::OnClose();
}

BOOL Dlg_PwrSupply::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam==VK_RETURN ||
			pMsg->wParam==VK_ESCAPE )
		{
			ShowWindow(SW_HIDE);
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_PwrSupply::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if(!m_pDoc) return;	
	if(m_nStepNo < 1) return;
	
	CString strTemp,strTemp2,strTemp3;
	
	if(bShow==TRUE)
	{
		strTemp.Format(_T("Step : %3d"), m_nStepNo);
		m_StaticStepNo.SetText(strTemp);
		
		onSetObject(0);

		if(m_pStep)
		{
			if(m_pStep->nPwrSupply_Cmd==1)
			{
				m_RadPwrSupplyON.SetCheck(TRUE);
				m_CboPwrSupplyVolt.EnableWindow(TRUE);
			}
			else if(m_pStep->nPwrSupply_Cmd==2)
			{
				m_RadPwrSupplyOFF.SetCheck(TRUE);
				
				m_CboPwrSupplyVolt.EnableWindow(FALSE);
			    m_CboPwrSupplyVolt.SetCurSel(0);//V
				m_pStep->nPwrSupply_Volt=0;
			}
			else
			{
				m_RadPwrSupplyNONE.SetCheck(TRUE);
			}

			if(m_pStep->nPwrSupply_Cmd==1)
			{	
				if(m_pStep->nPwrSupply_Volt==0)
				{
					m_CboPwrSupplyVolt.SetWindowText(_T("0"));
				}
				else if(m_pStep->nPwrSupply_Volt==12000)
				{
					m_CboPwrSupplyVolt.SetCurSel(0);
				}
				else if(m_pStep->nPwrSupply_Volt==24000)
				{
					m_CboPwrSupplyVolt.SetCurSel(1);
				}
				else
				{
					strTemp=theApp.GetValueStrf((float)m_pStep->nPwrSupply_Volt/1000.f,2);//V
					strTemp2=theApp.str_GetSplit(strTemp,0,'.');
					strTemp3=theApp.str_GetSplit(strTemp,1,'.');

					if(atoi(strTemp3)>0)
					{
                        m_CboPwrSupplyVolt.SetWindowText(strTemp);
					}
					else
					{
						m_CboPwrSupplyVolt.SetWindowText(strTemp2);
					}
				}	
			}
		}
	}
	else
	{
		if(m_pParent)
		{
			m_pParent->PostMessage(WM_PWRSUPPLY_DLG_CLOSE, (WPARAM)m_nStepNo, (LPARAM) this);
		}
	}
}

void Dlg_PwrSupply::onSetObject(int iType)
{
	if(iType==0)
	{
		m_RadPwrSupplyON.SetCheck(FALSE);
		m_RadPwrSupplyOFF.SetCheck(FALSE);
		m_RadPwrSupplyNONE.SetCheck(FALSE);	
				
		m_CboPwrSupplyVolt.EnableWindow(FALSE);
		m_CboPwrSupplyVolt.SetWindowText(_T("0"));//0V
	}
}

void Dlg_PwrSupply::OnRadPwrsupplyOn() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bUse=m_RadPwrSupplyON.GetCheck();
		if(m_pStep)
		{
			m_pStep->nPwrSupply_Cmd=1;			
		}

		if(m_pStep->nPwrSupply_Cmd==1)
		{
			m_RadPwrSupplyOFF.SetCheck(FALSE);
			m_RadPwrSupplyNONE.SetCheck(FALSE);

			m_CboPwrSupplyVolt.EnableWindow(TRUE);
			m_CboPwrSupplyVolt.SetCurSel(0);//12V
			m_pStep->nPwrSupply_Volt=12000;
		}			
	}
	
	UpdateData(FALSE);	
}

void Dlg_PwrSupply::OnRadPwrsupplyOff() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bUse=m_RadPwrSupplyOFF.GetCheck();
		if(m_pStep)
		{
			m_pStep->nPwrSupply_Cmd=2;
		}
		if(m_pStep->nPwrSupply_Cmd==2)
		{
			m_RadPwrSupplyON.SetCheck(FALSE);
			m_RadPwrSupplyNONE.SetCheck(FALSE);

			m_CboPwrSupplyVolt.EnableWindow(FALSE);
			m_CboPwrSupplyVolt.SetWindowText(_T("0"));//0V
			m_pStep->nPwrSupply_Volt=0;
		}
	}
	UpdateData(FALSE);	
}

void Dlg_PwrSupply::OnRadPwrsupplyNone() 
{
	UpdateData(TRUE);
	
	if(IsWindowVisible())
	{
		BOOL bUse=m_RadPwrSupplyNONE.GetCheck();
		if(m_pStep)
		{
			m_pStep->nPwrSupply_Cmd=0;
		}
		if(m_pStep->nPwrSupply_Cmd==0)
		{
			m_RadPwrSupplyON.SetCheck(FALSE);
			m_RadPwrSupplyOFF.SetCheck(FALSE);

			m_CboPwrSupplyVolt.EnableWindow(FALSE);
			m_CboPwrSupplyVolt.SetWindowText(_T("0"));//0V
			m_pStep->nPwrSupply_Volt=0;
		}
	}
	UpdateData(FALSE);	
}

void Dlg_PwrSupply::OnEditchangeCboPwrsupplyVolt() 
{
	UpdateData(TRUE);
	
	float fValue=atof(theApp.win_GetText(&m_CboPwrSupplyVolt));
	if(m_pStep)
	{
		m_pStep->nPwrSupply_Volt=(int)(fValue*1000);//mV
	}
	
	UpdateData(FALSE);	
}

void Dlg_PwrSupply::OnSelchangeCboPwrsupplyVolt() 
{
	UpdateData(TRUE);
	
	int nValue=0;
	int nCurSel=m_CboPwrSupplyVolt.GetCurSel();
	if(nCurSel>-1)
	{
		CString strValue=theApp.win_CboGetString(&m_CboPwrSupplyVolt);
		nValue=atoi(strValue)*1000;//mV
	}
	if(m_pStep)
	{
		m_pStep->nPwrSupply_Volt=nValue;
	}
	
	UpdateData(FALSE);	
}
