// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__4EBBAB56_2C27_421C_BD14_D349670197B4__INCLUDED_)
#define AFX_STDAFX_H__4EBBAB56_2C27_421C_BD14_D349670197B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#define REG_REAL_GRAPH	"Real Graph"

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxtempl.h>
#include <grid/gxall.h>
#include <stdlib.h>

#define PE_MAX_SUBSET      6
#define NULL_DATA		   999999999
#define ANNOTATION_COLOR   RGB(0,200,0)

typedef struct Graph_Sub_Info
{
	char szname[1024];
	char cUnit[1024];
	float fMin;
	float fMax;
	COLORREF lineColor;
	CString strAlias;	
	CString sOffset;	
	CString sFactor;	
	BOOL bCheck;
	BOOL bUse;
	
} Graph_Sub_Info;

typedef struct Graph_Global_Info
{
	float graphtime1;
	float graphtime2;
	float graphtime3;
	
	int   graphchxcount;
	int   graphchamxcount;
	int   graphauxxcount;
	int   graphcanxcount;

	int nAuxCount;
	int nCanCount;

	Graph_Sub_Info ChInfo[4];
	Graph_Sub_Info *AuxInfo;
	Graph_Sub_Info *CanInfo;
	Graph_Sub_Info ChamInfo[4];

	int nModuleID;
	int nChIndex;

} Graph_Global_Info;

#define UM_LOGMESSAGE		      WM_USER+101          //LOG Message

#include "./Global/GStr.h"
#include "./Global/GMath.h"
#include "./Global/GWin.h"
#include "./Global/GFile.h"
#include "./Global/GTime.h"
#include "./Global/GLog.h"

//#include "MyDefine.h"
//#pragma comment(lib, "../../../Lib/PEGRAP32.LIB")
#pragma comment(lib, "../../BCLib//PROESS2/PEGRAP32.LIB") //lyj 20200710

#include "../../BCLib/Include/PSCommonAll.h"
#include "../../BCLib/Include/PSServerAll.h"

#ifdef _DEBUG
#pragma comment(lib, "../../../lib/PSCommonD.lib ")
#pragma message("Automatically linking with PSCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../lib/PSCommon.lib")
#pragma message("Automatically linking with PSCommon.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../lib/PSServerD.lib ")
#pragma message("Automatically linking with PSServerD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../lib/PSServer.lib")
#pragma message("Automatically linking with PSServer.lib By K.B.H")
#endif	//_DEBUG

#define FILE_SAVE_ITEM_REG_SET	"FileSave"

extern SFT_SYSTEM_PARAM m_SystemParam;
extern SFT_VARIABLE_CH_DATA *m_pRealChData;
extern SFT_CHAMBER_VALUE *m_pRealchamberData;

extern int m_nRealTimeCounter;
extern int m_nRealTimeCounter2;
extern int m_nRealTimeCounter3;

extern int g_iCnt;

extern COLORREF gc;
extern float g_lchamberdata1, g_lchamberdata2, g_lchamberdata3, g_lchamberdata4;

extern Graph_Global_Info g_GraphInfo;


static void win_SysDelay(DWORD comTime,int iType=0)
{//20 딜레이			
	if(iType==1)
	{
		Sleep(comTime);
		return;
	}
	
	DWORD dwStart = GetTickCount();
	MSG msg;
	while(GetTickCount()-dwStart<comTime){
		if(PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE)){
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			Sleep(1);
		}
	}
	}

static void win_Doevents(HWND hWnd=NULL,DWORD nTimeOut=5000)
{//6 윈도우 메세지 처리
	DWORD nStart=::GetTickCount();
	MSG Message= { 0 };		
	while (PeekMessage(&Message, hWnd, 0, 0, PM_NOREMOVE) == TRUE)
	{ 	   
		if((::GetTickCount()-nStart)>nTimeOut) return;
		
		if(GetMessage(&Message, hWnd, 0, 0) ) 
		{//큐가 비어 있을 경우 무한 대기를 하기 때문에 백그라운드 작업을 할수 없다.
			//메시지 큐에서 메시지를 읽되 무한 대기하며 메시지를 무조건 큐에서 제거한다.
			TranslateMessage(&Message); 
			DispatchMessage(&Message); 	    
		} 
		else
		{
			return;  	   
		}
	}	 
	}


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_STDAFX_H__4EBBAB56_2C27_421C_BD14_D349670197B4__INCLUDED_)
