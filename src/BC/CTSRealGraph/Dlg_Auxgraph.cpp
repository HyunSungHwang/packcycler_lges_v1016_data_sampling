// Dlg_Auxgraph.cpp : implementation file
//

#include "stdafx.h"
#include "ctsrealgraph.h"
#include "Dlg_Auxgraph.h"

#include "CTSRealGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Auxgraph dialog


Dlg_Auxgraph::Dlg_Auxgraph(CWnd* pParent /*=NULL*/)
	: CDialog(Dlg_Auxgraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(Dlg_Auxgraph)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bStartDraw=FALSE;
}


void Dlg_Auxgraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Auxgraph)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Auxgraph, CDialog)
	//{{AFX_MSG_MAP(Dlg_Auxgraph)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Auxgraph message handlers

BOOL Dlg_Auxgraph::OnInitDialog() 
{
	CDialog::OnInitDialog();

	mainTabAux=this;	
	m_wndGraphAux.SubclassDlgItem(IDC_LAB_GRAPH_AUX1, this);
	m_wndGraphAux.m_GraphInfo=&g_GraphInfo;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_Auxgraph::OnOK() 
{
	CDialog::OnOK();
}

void Dlg_Auxgraph::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL Dlg_Auxgraph::onStart()
{	
	if(m_bStartDraw == FALSE)
	{						
		if(m_wndGraphAux.Start(1)==FALSE) return FALSE;
		
		m_bStartDraw = TRUE;	
	}
	return TRUE;
}

void Dlg_Auxgraph::onStop()
{
	m_wndGraphAux.Stop();
	
	m_bStartDraw = FALSE;
}

BOOL Dlg_Auxgraph::Fun_InitGraph()
{
	BOOL bflag=Fun_DrawFrame();
	Fun_HideGraph();
	
	m_wndGraphAux.Fun_SubsetSetting(1);	
	return bflag;
}

BOOL Dlg_Auxgraph::Fun_DrawFrame()
{
	if(!mainDlg) return FALSE;

	int iCnt = 0;
	CString str;
	CString strTemp;

	if(m_wndGraphAux.ClearGraph()==FALSE) return FALSE;
	
	iCnt = mainDlg->Fun_GetCheckAuxinfo();		
	m_wndGraphAux.m_iYCount=iCnt;

	int nItem=0;
	if(g_GraphInfo.nAuxCount>0)
	{
		for(int i=0;i<g_GraphInfo.nAuxCount;i++)
		{// Aux Can Graph Subset Setting
					
			if(g_GraphInfo.AuxInfo[i].bCheck==TRUE) 
			{
				m_wndGraphAux.ShowSubset(nItem, iCnt, TRUE);    
			
				strTemp.Format("%s",g_GraphInfo.AuxInfo[i].strAlias);
				m_wndGraphAux.SetYAxisLabel(nItem, strTemp);
				m_wndGraphAux.SetDataTitle (nItem, strTemp);
				nItem=nItem+1;
			}
		}
	}
	
	//Set X Axis Label	
	m_wndGraphAux.SetXAxisLabel("");
	
	//Set Title
	m_wndGraphAux.SetMainTitle("AUX Graph");
	return TRUE;
}

void Dlg_Auxgraph::Fun_HideGraph()
{
	if(!mainDlg) return;
	
	UpdateData(TRUE);
	
	RECT rt;
	GetClientRect(&rt);
	
	GetDlgItem(IDC_LAB_GRAPH_AUX1)->MoveWindow(rt.left+10,rt.top+10,rt.right-20,(rt.bottom)-20,TRUE);
}


void Dlg_Auxgraph::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	CDialog::OnSize(nType, cx, cy);
	
	RECT r;
	CRect ctrlRect, ctrlGrid;
	if(::IsWindow(m_hWnd))
	{
		::GetClientRect(m_hWnd, &r);
		
		if(::IsWindow(m_wndGraphAux.GetSafeHwnd()))
		{
			Fun_HideGraph();
		}
	}	
		
}

BOOL Dlg_Auxgraph::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_ESCAPE || pMsg->wParam==VK_RETURN)
		{
			return FALSE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_Auxgraph::onGraphMax() 
{
	m_wndGraphAux.SetMaximize();	
}
