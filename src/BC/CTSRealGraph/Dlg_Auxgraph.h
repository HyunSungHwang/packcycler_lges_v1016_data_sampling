#if !defined(AFX_DLG_AUXGRAPH_H__779FFA2A_E39E_4DCB_B1B1_7F247B8315A8__INCLUDED_)
#define AFX_DLG_AUXGRAPH_H__779FFA2A_E39E_4DCB_B1B1_7F247B8315A8__INCLUDED_

#include "GraphWnd.h"
#include "RealGraphWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Auxgraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_Auxgraph dialog

class Dlg_Auxgraph : public CDialog
{
// Construction
public:
	Dlg_Auxgraph(CWnd* pParent = NULL);   // standard constructor

	BOOL m_bStartDraw;
	
	CRealGraphWnd_V2 m_wndGraphAux;

	BOOL Fun_DrawFrame();
	void Fun_HideGraph();
	BOOL Fun_InitGraph();

	void onGraphMax();
	BOOL onStart();
	void onStop();

// Dialog Data
	//{{AFX_DATA(Dlg_Auxgraph)
	enum { IDD = IDD_DIALOG_AUXGRAPH };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Auxgraph)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Auxgraph)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern Dlg_Auxgraph *mainTabAux;
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_AUXGRAPH_H__779FFA2A_E39E_4DCB_B1B1_7F247B8315A8__INCLUDED_)
