// Channel.h: interface for the CChannel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHANNEL_H__723C1F2A_DDB9_467C_AF0C_0BC95D071DF5__INCLUDED_)
#define AFX_CHANNEL_H__723C1F2A_DDB9_467C_AF0C_0BC95D071DF5__INCLUDED_

#include <afxtempl.h>			//for CList by KBH

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_DATA_STACK_SIZE		2000
//#define MAX_RING_BUFFER_SIZE	10
#define MAX_MSEC_DATA_POINT		100

#include <afxmt.h>

//#define DllExport __declsp(dllexport)
//AFX_EXT_CLASS
class CChannel  
{
public:
	WORD GetAccCycleCount1();
	WORD GetAccCycleCount2();
	WORD GetAccCycleCount3();
	WORD GetAccCycleCount4();
	WORD GetAccCycleCount5();
	WORD GetMultiCycleCount1();
	WORD GetMultiCycleCount2();
	WORD GetMultiCycleCount3();
	WORD GetMultiCycleCount4();
	WORD GetMultiCycleCount5();
	LONG GetSyncTime();
	LONG GetSyncDate();
	LONG GetDisChargeWh();
	LONG GetChargeWh();
	LONG GetCapacitance();
	LONG GetDisChargeAh();
	LONG GetChargeAh();
	UINT GetCVTime();
	UINT GetSaveDataStackSize_V1004();
	BOOL PopSaveData(SFT_VARIABLE_CH_DATA_V1004 &varData);
	void ConverChData(SFT_CH_DATA_B &chData);
	void PushSaveData(SFT_VARIABLE_CH_DATA_V1004 varData);
	void SetChannelVarData(SFT_VARIABLE_CH_DATA_V1004 varData);
	int GetProfileIntExt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault);
	UINT GetSaveDataStackSizeA();
	void SetAuxVarData(int nAuxCount, SFT_AUX_DATA * pAuxData);
	void SetAuxVarDataA(SFT_VARIABLE_CH_DATA_A varData);
	void SetCanVarData(int nCanCount , SFT_CAN_DATA *pCanData);
	int GetCanCount();
	SFT_CAN_DATA * GetCanData();
	int m_nCanCount;
	void SetState(WORD state);
	SFT_AUX_DATA * GetAuxData();
	int GetAuxCount();
	
	BOOL SetStepStartData(SFT_MSEC_CH_DATA_INFO *pInfo, LPVOID lpData);
	void ResetData();
	WORD GetStepType();
	WORD GetCurCycleCount();
	WORD GetTotalCycleCount();
	UINT GetSaveDataStackSize();
//	BOOL PopSaveData(SFT_CH_DATA &chData);
//	void PushSaveData(SFT_CH_DATA chData);
	
	BOOL PopSaveData(SFT_VARIABLE_CH_DATA &varData);
	void PushSaveData(SFT_VARIABLE_CH_DATA varData);
	BOOL PopSaveDataA(SFT_VARIABLE_CH_DATA_A &varData);
	void PushSaveDataA(SFT_VARIABLE_CH_DATA_A varData);
	UINT GetTotalTime();
	WORD GetStepNo();
	long GetWattHour();
	long GetWatt();
	BYTE GetGradeCode(int nItem = 0);
	BYTE GetCode();
	UINT GetStepTime();
	WORD GetState();
	long GetImpedance();
	long GetCapacity();
	long GetCurrent();
	long GetVoltage();
	long GetAvgCurrent();
	long GetAvgVoltage();
	long GetCapacitySum();
	long GetTemperature();
	long GetHumidity();
	SFT_CHAMBER_VALUE * GetChamberData();

	BYTE GetReservedCmd()	{	return m_CmdReserved;	}

	void SetChannelData(SFT_CH_DATA chData);
	void SetChamberData(SFT_CHAMBER_VALUE *pChamberData);
	void SetChannelVarData(SFT_VARIABLE_CH_DATA chData);
	void SetChannelVarDataA(SFT_VARIABLE_CH_DATA_A chData);
	void SetChIndex(int index)	{	m_wChIndex = (WORD)index;	}
	operator = (const CChannel& chData);

	CChannel();
	virtual ~CChannel();

//	LPVOID			m_lpStepStartData;
	SFT_MSEC_CH_DATA			m_aStepStartData[MAX_MSEC_DATA_POINT];
	SFT_MSEC_CH_DATA_INFO		m_StepStartDataInfo;
	
private:

    WORD			m_wChIndex;				//Zero Base Channel No
	CCriticalSection	cs;

	WORD			m_state;
    BYTE			m_grade[PS_MAX_STEP_GRADE];
    BYTE			m_cellCode;
	WORD			m_nStepNo;
	unsigned long	m_ulStepTime;
    unsigned long	m_ulTotalTime;
    long			m_lVoltage;
    long			m_lCurrent;
    long			m_lWatt;
    long			m_lWattHour;		//2008/07/24 m_lChargeWh, m_lDisChargeWh 로 변경
	long			m_lChargeWh;
	long			m_lDisChargeWh;
    long			m_lCapacity;			//2008/07/24 m_lChargeAh,m_lDisChargeAh,m_lCapacitance 로 변경
	long			m_lChargeAh;
	long			m_lDisChargeAh;
	long			m_lCapacitance;
    long 			m_lImpedance;
	long			m_lAvgVoltage;
	long			m_lAvgCurrent;
	WORD			m_wStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	long			m_lTemparature;
	long			m_lPressure;
	WORD			m_nCurrentCycleNum;
	WORD			m_nTotalCycleNum;
	WORD			m_nAccCycleGroupNum1;
	WORD			m_nAccCycleGroupNum2;	//ljb v1009
	WORD			m_nAccCycleGroupNum3;	//ljb v1009
	WORD			m_nAccCycleGroupNum4;	//ljb v1009
	WORD			m_nAccCycleGroupNum5;	//ljb v1009
	WORD			m_nMultiCycleGroupNum1;
	WORD			m_nMultiCycleGroupNum2;	//ljb v1009
	WORD			m_nMultiCycleGroupNum3;	//ljb v1009
	WORD			m_nMultiCycleGroupNum4;	//ljb v1009
	WORD			m_nMultiCycleGroupNum5;	//ljb v1009
	long			m_lCapacitySum;

	long			m_lSyncDate;		//날짜
	long			m_lSyncTime;		//시간
	
	unsigned long	m_ulCVTime;
	//2006/06/08
	BYTE			m_CmdReserved;
	BOOL			m_nRunMode;

	//2007/07/31
	int				m_nAuxCount;
	SFT_AUX_DATA	*pAuxData;
	SFT_CAN_DATA	*pCanData;

	//CList<SFT_CH_DATA, const SFT_CH_DATA& > m_aSaveDataList;   // List of Save data
	CList<SFT_VARIABLE_CH_DATA, const SFT_VARIABLE_CH_DATA& > m_aSaveDataList;   // List of Save data
	CList<SFT_VARIABLE_CH_DATA_V1004, const SFT_VARIABLE_CH_DATA_V1004& > m_aSaveDataList_V1004;   // List of Save data
	CList<SFT_VARIABLE_CH_DATA_A, const SFT_VARIABLE_CH_DATA_A& > m_aSaveDataListA;   // List of Save data

	//★★ ljb 20091029  ★★★★★★★★
	SFT_CHAMBER_VALUE *m_pChamberData;
protected:
	long m_lStartCellCheckVoltageLimit;
	long m_lEndCellCheckVoltageLimit;
	void ConverChData(SFT_CH_DATA &chData);
	void ConverChDataA(SFT_CH_DATA_A &chData);
};

#endif // !defined(AFX_CHANNEL_H__723C1F2A_DDB9_467C_AF0C_0BC95D071DF5__INCLUDED_)
