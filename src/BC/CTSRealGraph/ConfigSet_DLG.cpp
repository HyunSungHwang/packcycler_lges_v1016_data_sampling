// ConfigSet_DLG.cpp : implementation file
//

#include "stdafx.h"
#include "ctsrealgraph.h"
#include "ConfigSet_DLG.h"
#include "ColorButton2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigSet_DLG dialog


CConfigSet_DLG::CConfigSet_DLG(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CConfigSet_DLG::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CConfigSet_DLG::IDD2):
	(CConfigSet_DLG::IDD), pParent)
{
	//{{AFX_DATA_INIT(CConfigSet_DLG)
	m_checkAux = FALSE;
	m_bChkVoltage = FALSE;
	m_bChkCurrent = FALSE;
	m_bChkPower = FALSE;
	m_bChkCapacity = FALSE;
	m_bChkCreatTemper = FALSE;
	m_bChkMeasureHumid = FALSE;
	m_bChkCreatHumid = FALSE;
	m_bChkMeasureTemper = FALSE;
	m_fVoltageMin = 0.0f;
	m_fVoltageMax = 0.0f;
	m_fCurrentMin = 0.0f;
	m_fCurrentMax = 0.0f;
	m_fPowerMin = 0.0f;
	m_fPowerMax = 0.0f;
	m_fCapacitorMax = 0.0f;
	m_fCapacitorMin = 0.0f;
	m_fCreatTemperMin = 0.0f;
	m_fCreatTemperMax = 0.0f;
	m_fMeasureTemperMin = 0.0f;
	m_fMeasureTemperMax = 0.0f;
	m_fCreatHumidMin = 0.0f;
	m_fCreatHumidMax = 0.0f;
	m_fMeasureHumidMax = 0.0f;
	m_fMeasureHumidMin = 0.0f;
	m_fChannelGraphTime = 0.0f;
	m_fChamberGraphTime = 0.0f;
	m_fAuxCanGraphTime = 0.0f;
    m_iChxcount=0;
	m_iAuxxcount=0;
	m_iCanxcount=0;
	m_iChamxcount=0;
	//}}AFX_DATA_INIT
}


void CConfigSet_DLG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigSet_DLG)
	DDX_Control(pDX, IDC_CAP_UNIT, m_ComboCapUnit);
	DDX_Control(pDX, IDC_CUR_UNIT, m_ComboCurUnit);
	DDX_Control(pDX, IDC_POW_UNIT, m_ComboPowUnit);
	DDX_Control(pDX, IDC_LIST_CAN, m_ListCtrlCan);
	DDX_Control(pDX, IDC_LIST1, m_ListCtrlAux);
	DDX_Control(pDX, IDC_VOL_UNIT, m_ComboVolUnit);
	DDX_Check(pDX, IDC_CHECK_VOL, m_bChkVoltage);
	DDX_Check(pDX, IDC_CHECK_CURRENT, m_bChkCurrent);
	DDX_Check(pDX, IDC_CHECK_POWER, m_bChkPower);
	DDX_Check(pDX, IDC_CHECK_CAPACITY, m_bChkCapacity);
	DDX_Check(pDX, IDC_CHECK_CTEMP, m_bChkCreatTemper);
	DDX_Check(pDX, IDC_CHECK_MHUMID, m_bChkMeasureHumid);
	DDX_Check(pDX, IDC_CHECK_CHUMID, m_bChkCreatHumid);
	DDX_Check(pDX, IDC_CHECK_MTEMP, m_bChkMeasureTemper);
	DDX_Text(pDX, IDC_VOL_MIN, m_fVoltageMin);
	DDX_Text(pDX, IDC_VOL_MAX, m_fVoltageMax);
	DDX_Text(pDX, IDC_CUR_MIN, m_fCurrentMin);
	DDX_Text(pDX, IDC_CUR_MAX, m_fCurrentMax);
	DDX_Text(pDX, IDC_POW_MIN, m_fPowerMin);
	DDX_Text(pDX, IDC_POW_MAX, m_fPowerMax);
	DDX_Text(pDX, IDC_CAP_MAX, m_fCapacitorMax);
	DDX_Text(pDX, IDC_CAP_MIN, m_fCapacitorMin);
	DDX_Text(pDX, IDC_CT_MIN, m_fCreatTemperMin);
	DDX_Text(pDX, IDC_CT_MAX, m_fCreatTemperMax);
	DDX_Text(pDX, IDC_MT_MIN, m_fMeasureTemperMin);
	DDX_Text(pDX, IDC_MT_MAX, m_fMeasureTemperMax);
	DDX_Text(pDX, IDC_CH_MIN, m_fCreatHumidMin);
	DDX_Text(pDX, IDC_CH_MAX, m_fCreatHumidMax);
	DDX_Text(pDX, IDC_MH_MAX, m_fMeasureHumidMax);
	DDX_Text(pDX, IDC_MH_MIN, m_fMeasureHumidMin);
	DDX_Text(pDX, IDC_CH_TIME, m_fChannelGraphTime);
	DDX_Text(pDX, IDC_CHAMBER_TIME, m_fChamberGraphTime);
	DDX_Text(pDX, IDC_AUXCAN_TIME, m_fAuxCanGraphTime);	
	DDX_Text(pDX, IDC_EDIT_CHXCOUNT, m_iChxcount);
	DDX_Text(pDX, IDC_EDIT_AUXXCOUNT, m_iAuxxcount);
	DDX_Text(pDX, IDC_EDIT_CANXCOUNT, m_iCanxcount);
	DDX_Text(pDX, IDC_EDIT_CHAMXCOUNT, m_iChamxcount);
	//}}AFX_DATA_MAP
	
	DDX_Control(pDX, IDC_VOL_COLOR, m_TWellColor[0]); 
	DDX_Control(pDX, IDC_CUR_COLOR, m_TWellColor[1]);
	DDX_Control(pDX, IDC_POW_COLOR, m_TWellColor[2]);
	DDX_Control(pDX, IDC_CAP_COLOR, m_TWellColor[3]);
	DDX_Control(pDX, IDC_CT_COLOR, m_TWellColor[4]);
	DDX_Control(pDX, IDC_MT_COLOR, m_TWellColor[5]);
	DDX_Control(pDX, IDC_CH_COLOR, m_TWellColor[6]);
	DDX_Control(pDX, IDC_MH_COLOR, m_TWellColor[7]);
}

BEGIN_MESSAGE_MAP(CConfigSet_DLG, CDialog)
	//{{AFX_MSG_MAP(CConfigSet_DLG)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_VOL_COLOR, OnVolColor)
	ON_BN_CLICKED(IDC_CUR_COLOR, OnCurColor)
	ON_BN_CLICKED(IDC_POW_COLOR, OnPowColor)
	ON_BN_CLICKED(IDC_CAP_COLOR, OnCapColor)
	ON_BN_CLICKED(IDC_CT_COLOR, OnCtColor)
	ON_BN_CLICKED(IDC_MT_COLOR, OnMtColor)
	ON_BN_CLICKED(IDC_CH_COLOR, OnChColor)
	ON_BN_CLICKED(IDC_MH_COLOR, OnMhColor)
	ON_WM_DRAWITEM()	
	//}}AFX_MSG_MAP
	//ON_MESSAGE(WM_ON_CHKBOX,FUN_NAME)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigSet_DLG message handlers

BOOL CConfigSet_DLG::OnInitDialog() 
{
	CDialog::OnInitDialog();
		
	DrawColor();

 	Fun_DisplayCondition();
	Fun_DisplayListCtrl();

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigSet_DLG::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
}

BOOL CConfigSet_DLG::file_Finder(CString str_path)
{
	BOOL bWork;
	CFileFind finder;
    bWork=finder.FindFile(str_path);		
	finder.Close();
	
    return bWork;
}

void CConfigSet_DLG::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	int i=0;
	if(g_GraphInfo.nAuxCount>0)
	{		
		for(int i = 0;i<g_GraphInfo.nAuxCount;i++)
		{
			CString strName=m_ListCtrlAux.GetItemText(i,0);;
			strName.Replace("AuxData[","");
			strName.Replace("]","");	
			int nItem=atoi(strName);

			g_GraphInfo.AuxInfo[nItem].lineColor=m_ListCtrlAux.GetItemBkColor(i,1);
		}
	}

	if(g_GraphInfo.nCanCount>0)
	{
		for(int i = 0;i<g_GraphInfo.nCanCount;i++)
		{
			CString strName=m_ListCtrlCan.GetItemText(i,0);;
			strName.Replace("CanData[","");
			strName.Replace("]","");	
			int nItem=atoi(strName);

			g_GraphInfo.CanInfo[nItem].lineColor=m_ListCtrlCan.GetItemBkColor(i,1);
		}
	}

	m_ListCtrlAux.EndEdit(TRUE);
	m_ListCtrlCan.EndEdit(TRUE);

	if(Fun_MakeIniFile() == FALSE)
	{
		return;
	}

	CDialog::OnOK();
}
	
void CConfigSet_DLG::OnVolColor() 
{	
  	g_GraphInfo.ChInfo[0].lineColor = gc;
  	m_TWellColor[0].SetColor(g_GraphInfo.ChInfo[0].lineColor);
}

void CConfigSet_DLG::OnCurColor() 
{
	g_GraphInfo.ChInfo[1].lineColor = gc;
	m_TWellColor[1].SetColor(g_GraphInfo.ChInfo[1].lineColor);
}

void CConfigSet_DLG::OnPowColor() 
{	
	g_GraphInfo.ChInfo[2].lineColor = gc;
	m_TWellColor[2].SetColor(g_GraphInfo.ChInfo[2].lineColor);
}

void CConfigSet_DLG::OnCapColor() 
{
	g_GraphInfo.ChInfo[3].lineColor = gc;
	m_TWellColor[3].SetColor(g_GraphInfo.ChInfo[3].lineColor);
}

void CConfigSet_DLG::OnCtColor() 
{	
	g_GraphInfo.ChamInfo[0].lineColor = gc;
	m_TWellColor[4].SetColor(g_GraphInfo.ChamInfo[0].lineColor);
}

void CConfigSet_DLG::OnMtColor() 
{	
	g_GraphInfo.ChamInfo[1].lineColor = gc;
	m_TWellColor[5].SetColor(g_GraphInfo.ChamInfo[1].lineColor);	
}

void CConfigSet_DLG::OnChColor() 
{	
	g_GraphInfo.ChamInfo[2].lineColor = gc;
	m_TWellColor[6].SetColor(g_GraphInfo.ChamInfo[2].lineColor);
}

void CConfigSet_DLG::OnMhColor() 
{	
	g_GraphInfo.ChamInfo[3].lineColor = gc;
	m_TWellColor[7].SetColor(g_GraphInfo.ChamInfo[3].lineColor);
}



void CConfigSet_DLG::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your message handler code here and/or call default
//	if(nIDCtl==IDC_CUR_COLOR)         // 해당 버튼인지 체크
//  {
// 		CDC dc;
// 		RECT rect;
// 		dc.Attach(lpDrawItemStruct ->hDC);   //  버튼의 DC 구하기
// 		rect = lpDrawItemStruct->rcItem;     // 버튼 영역 구하기    
// 		dc.Draw3dRect(&rect,RGB(255,255,255),RGB(0,0,0)); // 버튼의 외각선 그리기
// 		dc.FillSolidRect(&rect,RGB(0,255,0)); // button 색상을 GREEN 으로
// 
// 		UINT state=lpDrawItemStruct->itemState;  //  버튼 상태 구하기
// 		if((state & ODS_SELECTED))
// 		{
// 			dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT);
// 		}
// 		else
// 		{
// 			dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT);
// 		}
// 		
// 		dc.SetBkColor(RGB(0,255,0)); // TEXT 의 백그라운드 색상
// 		dc.SetTextColor(RGB(255,0,0));  // TEXT 색상
// 		
// 		
// 		TCHAR buffer[MAX_PATH];           // 버튼의 TEXT를 얻기 위한 임시 버퍼
// 		ZeroMemory(buffer,MAX_PATH );     // 버퍼 초기화
// 		::GetWindowText(lpDrawItemStruct->hwndItem,buffer,MAX_PATH); // 버튼의 TEXT 얻기
// 		dc.DrawText(buffer,&rect,DT_CENTER|DT_VCENTER|DT_SINGLELINE);// 버튼에 TEXT 넣기
// 		dc.Detach();  // Button DC 풀어 주기
//    }                

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CConfigSet_DLG::DrawColor()
{		
	m_TWellColor[0].SetColor(g_GraphInfo.ChInfo[0].lineColor);
	m_TWellColor[1].SetColor(g_GraphInfo.ChInfo[1].lineColor);
	m_TWellColor[2].SetColor(g_GraphInfo.ChInfo[2].lineColor);
	m_TWellColor[3].SetColor(g_GraphInfo.ChInfo[3].lineColor);
	m_TWellColor[4].SetColor(g_GraphInfo.ChamInfo[0].lineColor);
	m_TWellColor[5].SetColor(g_GraphInfo.ChamInfo[1].lineColor);
	m_TWellColor[6].SetColor(g_GraphInfo.ChamInfo[2].lineColor);
	m_TWellColor[7].SetColor(g_GraphInfo.ChamInfo[3].lineColor);
}

COleDateTime CConfigSet_DLG::GenRandDate()
{
	COleDateTime now = COleDateTime::GetCurrentTime();
	COleDateTimeSpan span(rand() % 3650, rand() % 24, rand() % 60, rand() % 60);
	now -= span;
	return now;
}

void CConfigSet_DLG::Fun_DisplayCondition()
{
	m_ComboVolUnit.AddString((LPTSTR)(const char*)"V");				// 20100924 SSL -> 채널 그래프 단위 
	m_ComboVolUnit.AddString((LPTSTR)(const char*)"mV");

	if(g_GraphInfo.ChInfo[0].cUnit[0] == 'V') m_ComboVolUnit.SetCurSel(0);
	else m_ComboVolUnit.SetCurSel(1);
	
	m_ComboCurUnit.AddString((LPTSTR)(const char*)"A");
	m_ComboCurUnit.AddString((LPTSTR)(const char*)"mA");

	if(g_GraphInfo.ChInfo[1].cUnit[0] == 'A') m_ComboCurUnit.SetCurSel(0);
	else m_ComboCurUnit.SetCurSel(1);
	
	m_ComboPowUnit.AddString((LPTSTR)(const char*)"Ah");
	m_ComboPowUnit.AddString((LPTSTR)(const char*)"mAh");

	if(g_GraphInfo.ChInfo[2].cUnit[0] == 'A') m_ComboPowUnit.SetCurSel(0);
	else m_ComboPowUnit.SetCurSel(1);

	m_ComboCapUnit.AddString((LPTSTR)(const char*)"Ah");
	m_ComboCapUnit.AddString((LPTSTR)(const char*)"mAh");

	if(g_GraphInfo.ChInfo[3].cUnit[0] == 'A') m_ComboCapUnit.SetCurSel(0);
	else m_ComboCapUnit.SetCurSel(1);

	UpdateData(FALSE);

	m_fChannelGraphTime = g_GraphInfo.graphtime1;				// 20100924 SSL -> Graph Current Condition Display
	m_fAuxCanGraphTime = g_GraphInfo.graphtime2;
	m_fChamberGraphTime = g_GraphInfo.graphtime3;

	m_iChxcount=g_GraphInfo.graphchxcount;
	m_iAuxxcount=g_GraphInfo.graphauxxcount;
	m_iCanxcount=g_GraphInfo.graphcanxcount;
	m_iChamxcount=g_GraphInfo.graphchamxcount;

	m_fVoltageMin = g_GraphInfo.ChInfo[0].fMin;
	m_fVoltageMax = g_GraphInfo.ChInfo[0].fMax;

	m_fCurrentMin = g_GraphInfo.ChInfo[1].fMin;
	m_fCurrentMax = g_GraphInfo.ChInfo[1].fMax;

	m_fPowerMin = g_GraphInfo.ChInfo[2].fMin;
	m_fPowerMax = g_GraphInfo.ChInfo[2].fMax;

	m_fCapacitorMin = g_GraphInfo.ChInfo[3].fMin;
	m_fCapacitorMax = g_GraphInfo.ChInfo[3].fMax;

	m_fCreatTemperMin = g_GraphInfo.ChamInfo[0].fMin;
	m_fCreatTemperMax = g_GraphInfo.ChamInfo[0].fMax;

	m_fMeasureTemperMin = g_GraphInfo.ChamInfo[1].fMin;
	m_fMeasureTemperMax = g_GraphInfo.ChamInfo[1].fMax;

	m_fCreatHumidMin = g_GraphInfo.ChamInfo[2].fMin;
	m_fCreatHumidMax = g_GraphInfo.ChamInfo[2].fMax;

	m_fMeasureHumidMin = g_GraphInfo.ChamInfo[3].fMin;
	m_fMeasureHumidMax = g_GraphInfo.ChamInfo[3].fMax;
	
	if(g_GraphInfo.ChInfo[0].bCheck == 1) m_bChkVoltage = TRUE;
	if(g_GraphInfo.ChInfo[1].bCheck == 1) m_bChkCurrent = TRUE;
	if(g_GraphInfo.ChInfo[2].bCheck == 1) m_bChkPower = TRUE;
	if(g_GraphInfo.ChInfo[3].bCheck == 1) m_bChkCapacity = TRUE;

	if(g_GraphInfo.ChamInfo[0].bCheck == 1) m_bChkCreatTemper = TRUE;
	if(g_GraphInfo.ChamInfo[1].bCheck == 1) m_bChkMeasureTemper = TRUE;
	if(g_GraphInfo.ChamInfo[2].bCheck == 1) m_bChkCreatHumid = TRUE;
	if(g_GraphInfo.ChamInfo[3].bCheck == 1) m_bChkMeasureHumid = TRUE;	
}

CString CConfigSet_DLG::Fun_getAppPath()
{
    HMODULE hModule=::GetModuleHandle(NULL); // handle of current module
    
    CString strExeFileName;
    VERIFY(::GetModuleFileName(hModule, strExeFileName.GetBuffer(_MAX_PATH),_MAX_PATH));
	
    char Drive[_MAX_DRIVE];
    char Path[_MAX_PATH];
	
    _splitpath(strExeFileName, Drive, Path, NULL,NULL);//Filename, Ext);
	
    return CString(Drive)+CString(Path); // has trailing backslash
}

BOOL CConfigSet_DLG::Fun_MakeIniFile()
{
	UpdateData(TRUE);						 // 20100913 SSL -> INI File Make		
	
	CStdioFile file;
	CString strFileName;
	strFileName.Format("%s\\Graph_Config_%02d_%02d.ini", Fun_getAppPath(),g_GraphInfo.nModuleID,g_GraphInfo.nChIndex);
 
	int icnt = 0;	
	if(file_Finder(strFileName)==TRUE) DeleteFile(strFileName);
	
	file.Open(strFileName, CStdioFile::modeCreate | CStdioFile::modeWrite | CStdioFile::typeText);
	
	m_strOrder.Format("[UNIT1]");
	m_strCh_Time.Format("GRAPH1_TIME = %f", m_fChannelGraphTime);
	m_strAux_Time.Format("GRAPH2_TIME = %f", m_fAuxCanGraphTime);
	m_strChamber_Time.Format("GRAPH3_TIME = %f", m_fChamberGraphTime);
		
	file.WriteString(m_strOrder +"\n");
	file.WriteString(m_strCh_Time+"\n");
	file.WriteString(m_strChamber_Time+"\n");
	file.WriteString(m_strAux_Time+"\n\n");


	CString strCh_Xcnt,strAux_Xcnt,strCan_Xcnt,strCham_Xcnt;
	strCh_Xcnt.Format("GRAPHCH_XCNT = %d", m_iChxcount);
	strAux_Xcnt.Format("GRAPHAUX_XCNT = %d", m_iAuxxcount);
	strCan_Xcnt.Format("GRAPHCAN_XCNT = %d", m_iCanxcount);
	strCham_Xcnt.Format("GRAPHCHAM_XCNT = %d", m_iChamxcount);

	file.WriteString(strCh_Xcnt +"\n");
	file.WriteString(strAux_Xcnt+"\n");
	file.WriteString(strCan_Xcnt+"\n");
	file.WriteString(strCham_Xcnt+"\n\n");


	//voltage//////////////////////////////////////////////
	m_strName.Format("DRAWCH1_NAME = VOLTAGE");
	m_strColor.Format("DRAWCH1_COLOR = %d", g_GraphInfo.ChInfo[0].lineColor);
	m_strMinValue.Format("DRAWCH1_MIN = %f", m_fVoltageMin);	
	m_strMaxValue.Format("DRAWCH1_MAX = %f", m_fVoltageMax);
	m_strCheck.Format("DRAWCH1_CHECK = %d", m_bChkVoltage);
		
	if(m_ComboVolUnit.GetCurSel() == 0) m_strUnit.Format("DRAWCH1_UNIT = %s", "V");
	else m_strUnit.Format("DRAWCH1_UNIT = %s", "mV");
									
	m_strAlias.Format("DRAWCH1_ALIAS = ");

	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strUnit+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");	
	
	//current//////////////////////////////////////////////	
	m_strName.Format("DRAWCH2_NAME = CURRENT");
	m_strColor.Format("DRAWCH2_COLOR = %d", g_GraphInfo.ChInfo[1].lineColor);
	m_strMinValue.Format("DRAWCH2_MIN = %f", m_fCurrentMin);	
	m_strMaxValue.Format("DRAWCH2_MAX = %f", m_fCurrentMax);
	m_strCheck.Format("DRAWCH2_CHECK = %d", m_bChkCurrent);

	if(m_ComboCurUnit.GetCurSel() == 0) m_strUnit.Format("DRAWCH2_UNIT = %s", "A");
	else                                m_strUnit.Format("DRAWCH2_UNIT = %s", "mA");
			
	m_strAlias.Format("DRAWCH2_ALIAS = ");

	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strUnit+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");	
	
	//POWER//////////////////////////////////////////////	
	m_strName.Format("DRAWCH3_NAME = POWER");
	m_strColor.Format("DRAWCH3_COLOR = %d", g_GraphInfo.ChInfo[2].lineColor);
	m_strMinValue.Format("DRAWCH3_MIN = %f", m_fPowerMin);	
	m_strMaxValue.Format("DRAWCH3_MAX = %f", m_fPowerMax);
	m_strCheck.Format("DRAWCH3_CHECK = %d", m_bChkPower);

	if(m_ComboPowUnit.GetCurSel() == 0) m_strUnit.Format("DRAWCH3_UNIT = %s", "Ah");
	else                                m_strUnit.Format("DRAWCH3_UNIT = %s", "mAh");
			
	m_strAlias.Format("DRAWCH3_ALIAS = ");

	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strUnit+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");
	
	//CAPACITY//////////////////////////////////////////////	
	m_strName.Format("DRAWCH4_NAME = CAPACITY");
	m_strColor.Format("DRAWCH4_COLOR = %d", g_GraphInfo.ChInfo[3].lineColor);
	m_strMinValue.Format("DRAWCH4_MIN = %f", m_fCapacitorMin);	
	m_strMaxValue.Format("DRAWCH4_MAX = %f", m_fCapacitorMax);
	m_strCheck.Format("DRAWCH4_CHECK = %d", m_bChkCapacity);

	if(m_ComboCapUnit.GetCurSel() == 0) m_strUnit.Format("DRAWCH4_UNIT = %s", "Ah");
	else                                m_strUnit.Format("DRAWCH4_UNIT = %s", "mAh");
			
	m_strAlias.Format("DRAWCH4_ALIAS = ");

	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strUnit+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");
	
	////////////////////////////////////////////////	
	m_strName.Format("DRAWCHAM1_NAME = Creat_Temperature");
	m_strColor.Format("DRAWCHAM1_COLOR = %d", g_GraphInfo.ChamInfo[0].lineColor);
	m_strMinValue.Format("DRAWCHAM1_MIN = %f", m_fCreatTemperMin);	
	m_strMaxValue.Format("DRAWCHAM1_MAX = %f", m_fCreatTemperMax);
	m_strCheck.Format("DRAWCHAM1_CHECK = %d", m_bChkCreatTemper);
	m_strAlias.Format("DRAWCHAM1_ALIAS = ");
		
	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");
	
	m_strName.Format("DRAWCHAM2_NAME = Measure_Temperature");
	m_strColor.Format("DRAWCHAM2_COLOR = %d", g_GraphInfo.ChamInfo[1].lineColor);
	m_strMinValue.Format("DRAWCHAM2_MIN = %f", m_fMeasureTemperMin);	
	m_strMaxValue.Format("DRAWCHAM2_MAX = %f", m_fMeasureTemperMax);
	m_strCheck.Format("DRAWCHAM2_CHECK = %d", m_bChkMeasureTemper);
	m_strAlias.Format("DRAWCHAM2_ALIAS = ");
		
	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");

	m_strName.Format("DRAWCHAM3_NAME = Creat_Humid");
	m_strColor.Format("DRAWCHAM3_COLOR = %d", g_GraphInfo.ChamInfo[2].lineColor);
	m_strMinValue.Format("DRAWCHAM3_MIN = %f", m_fCreatHumidMin);	
	m_strMaxValue.Format("DRAWCHAM3_MAX = %f", m_fCreatHumidMax);
	m_strCheck.Format("DRAWCHAM3_CHECK = %d", m_bChkCreatHumid);
	m_strAlias.Format("DRAWCHAM3_ALIAS = ");
		
	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");
	
	m_strName.Format("DRAWCHAM4_NAME = Measure_Humid");
	m_strColor.Format("DRAWCHAM4_COLOR = %d", g_GraphInfo.ChamInfo[3].lineColor);
	m_strMinValue.Format("DRAWCHAM4_MIN = %f", m_fMeasureHumidMin);	
	m_strMaxValue.Format("DRAWCHAM4_MAX = %f", m_fMeasureHumidMax);
	m_strCheck.Format("DRAWCHAM4_CHECK = %d", m_bChkMeasureHumid);
	m_strAlias.Format("DRAWCHAM4_ALIAS = ");
		
	file.WriteString(m_strName+"\n");
	file.WriteString(m_strColor+"\n");
	file.WriteString(m_strMinValue+"\n");
	file.WriteString(m_strMaxValue+"\n");
	file.WriteString(m_strCheck+"\n");
	file.WriteString(m_strAlias+"\n");
	
	int nCount=0;
	CString strTemp;

	if(g_GraphInfo.nAuxCount>0)
	{
		for(int i=0;i<g_GraphInfo.nAuxCount;i++)
		{			
			if(m_ListCtrlAux.GetCheck(i)==TRUE)
			{
				if(nCount>=PE_MAX_SUBSET)
				{
					AfxMessageBox("6개 이상 선택하실 수 없습니다!!");				
					return FALSE;
				}
				nCount=nCount+1;
			}
			CString strName=m_ListCtrlAux.GetItemText(i,0);;
			strName.Replace("AuxData[","");
			strName.Replace("]","");	
			int nItem=atoi(strName);		
						
			m_strName.Format("DRAWAUX%d_NAME = AUX DATA[%d]",nItem+1,nItem);
			m_strColor.Format("DRAWAUX%d_COLOR = %d",nItem+1,g_GraphInfo.AuxInfo[nItem].lineColor); 

			strTemp = m_ListCtrlAux.GetItemText(i,2);
			m_strMinValue.Format("DRAWAUX%d_MIN = %s",nItem+1, strTemp);

			strTemp = m_ListCtrlAux.GetItemText(i,3);
			m_strMaxValue.Format("DRAWAUX%d_MAX = %s",nItem+1, strTemp);

			strTemp = m_ListCtrlAux.GetItemText(i,4);
			m_strUnit.Format("DRAWAUX%d_UNIT = %s",nItem+1, strTemp);
				
			m_strCheck.Format("DRAWAUX%d_CHECK = %d",nItem+1, m_ListCtrlAux.GetCheck(i));
			
			strTemp = m_ListCtrlAux.GetItemText(i,5);
			m_strAlias.Format("DRAWAUX%d_ALIAS = %s",nItem+1, strTemp);

			file.WriteString(m_strName+"\n");
			file.WriteString(m_strColor+"\n");
			file.WriteString(m_strMinValue+"\n");
			file.WriteString(m_strMaxValue+"\n");
			file.WriteString(m_strUnit+"\n");
			file.WriteString(m_strCheck+"\n");	
			file.WriteString(m_strAlias+"\n");									
		}
	}

	nCount=0;
	if(g_GraphInfo.nCanCount>0)
	{
		for(int i=0;i<g_GraphInfo.nCanCount;i++)
		{	
			if(m_ListCtrlCan.GetCheck(i)==TRUE)
			{
				if(nCount>=PE_MAX_SUBSET)
				{
					AfxMessageBox("6개 이상 선택하실 수 없습니다!!");				
					return FALSE;
				}
				nCount=nCount+1;
			}
			
			CString strName=m_ListCtrlCan.GetItemText(i,0);;
			strName.Replace("CanData[","");
			strName.Replace("]","");	
			int nItem=atoi(strName);
			
			m_strName.Format("DRAWCAN%d_NAME = CAN DATA[%d]",nItem+1,nItem);		
			m_strColor.Format("DRAWCAN%d_COLOR = %d",nItem+1, g_GraphInfo.CanInfo[nItem].lineColor);  
			
			strTemp = m_ListCtrlCan.GetItemText(i,2);
			m_strMinValue.Format("DRAWCAN%d_MIN = %s",nItem+1, strTemp);
			
			strTemp = m_ListCtrlCan.GetItemText(i,3);
			m_strMaxValue.Format("DRAWCAN%d_MAX = %s",nItem+1, strTemp);
			
			strTemp = m_ListCtrlCan.GetItemText(i,4);
			m_strUnit.Format("DRAWCAN%d_UNIT = %s",nItem+1, strTemp);
			
			m_strCheck.Format("DRAWCAN%d_CHECK = %d",nItem+1, m_ListCtrlCan.GetCheck(i));
			
			strTemp = m_ListCtrlCan.GetItemText(i,5);
			m_strOffset.Format("DRAWCAN%d_OFFSET = %s",nItem+1, strTemp);
			
			strTemp = m_ListCtrlCan.GetItemText(i,6);
			m_strFactor.Format("DRAWCAN%d_FACTOR = %s",nItem+1, strTemp);
			
			strTemp = m_ListCtrlCan.GetItemText(i,7);
			m_strAlias.Format("DRAWCAN%d_ALIAS = %s",nItem+1, strTemp);
			
			file.WriteString(m_strName+"\n");
			file.WriteString(m_strColor+"\n");
			file.WriteString(m_strMinValue+"\n");
			file.WriteString(m_strMaxValue+"\n");
			file.WriteString(m_strUnit+"\n");
			file.WriteString(m_strCheck+"\n");		
			file.WriteString(m_strOffset+"\n");
			file.WriteString(m_strFactor+"\n");
			file.WriteString(m_strAlias+"\n");							
		}
	}
	file.Close();
	AfxMessageBox("저장되었습니다");

	
	return TRUE;
}

void CConfigSet_DLG::Fun_DisplayListCtrl()
{	
	/////////////// 20100914 SSL -> 선택된 Aux Data를 보기 위한 ListCtrl ///////////////

	CString tmp;
	CString strTemp;
	int iCnt = 0;
	int iChk = 0;
	int iTemp = 0;
	tmp = _T("");

	CString str;

	m_ListCtrlAux.SetColumnHeader(_T("Name, 120; Color, 80; Min, 80; Max, 80; Unit, 50; Alias, 150"));
	m_ListCtrlAux.SetGridLines(TRUE); // SHow grid lines
	m_ListCtrlAux.SetCheckboxeStyle(RC_CHKBOX_NORMAL); // Enable checkboxes
	m_ListCtrlAux.SetEditable(TRUE); // Allow sub-text edit		

	if(g_GraphInfo.nAuxCount>0)
	{
		for(int i=0;i<g_GraphInfo.nAuxCount;i++) 		
		{
			const int IDX = m_ListCtrlAux.InsertItem(0, _T(""));											
			
			tmp.Format("AuxData[%d]", i);
			m_ListCtrlAux.SetItemText(IDX, 0, tmp);	
			
			m_ListCtrlAux.SetCheck(IDX,g_GraphInfo.AuxInfo[i].bCheck);
			m_ListCtrlAux.SetItemText(IDX, 2, g_GraphInfo.AuxInfo[i].fMin);									
			m_ListCtrlAux.SetItemText(IDX, 3, g_GraphInfo.AuxInfo[i].fMax);
			m_ListCtrlAux.SetItemText(IDX, 4, g_GraphInfo.AuxInfo[i].cUnit);
			m_ListCtrlAux.SetItemText(IDX, 5, g_GraphInfo.AuxInfo[i].strAlias);
			m_ListCtrlAux.SetItemBkColor(IDX, 1, g_GraphInfo.AuxInfo[i].lineColor);
		}	
		m_ListCtrlAux.SortItems(0, TRUE); // sort the 1st column, ascending
	}
	
	////// 20100914 SSL -> 선택된 Can Data를 보기 위한 ListCtrl //////
	m_ListCtrlCan.SetColumnHeader(_T("Name, 120; Color, 80; Min, 80; Max, 80; Unit,0; Offset, 80; Factor, 80; Alias, 150"));
	m_ListCtrlCan.SetGridLines(TRUE); // SHow grid lines
	m_ListCtrlCan.SetCheckboxeStyle(RC_CHKBOX_NORMAL); // Enable checkboxes
	m_ListCtrlCan.SetEditable(TRUE); // Allow sub-text edit	

	if(g_GraphInfo.nCanCount>0)
	{
		for(int j=0;j<g_GraphInfo.nCanCount;j++)
		{
			const int IDX = m_ListCtrlCan.InsertItem(0, _T(""));											
			tmp.Format("CanData[%d]", j);
			m_ListCtrlCan.SetItemText(IDX, 0, tmp);														
			
			m_ListCtrlCan.SetCheck(IDX,g_GraphInfo.CanInfo[j].bCheck);		
			m_ListCtrlCan.SetItemText(IDX, 2, g_GraphInfo.CanInfo[j].fMin);
			m_ListCtrlCan.SetItemText(IDX, 3, g_GraphInfo.CanInfo[j].fMax);
			m_ListCtrlCan.SetItemText(IDX, 4, g_GraphInfo.CanInfo[j].cUnit);
			m_ListCtrlCan.SetItemText(IDX, 5, g_GraphInfo.CanInfo[j].sOffset);
			m_ListCtrlCan.SetItemText(IDX, 6, g_GraphInfo.CanInfo[j].sFactor);
			m_ListCtrlCan.SetItemText(IDX, 7, g_GraphInfo.CanInfo[j].strAlias);
			m_ListCtrlCan.SetItemBkColor(IDX,1,g_GraphInfo.CanInfo[j].lineColor);		
		}	
		m_ListCtrlCan.SortItems(0, TRUE); // sort the 1st column, ascending
	}
	
	UpdateData(FALSE);
}