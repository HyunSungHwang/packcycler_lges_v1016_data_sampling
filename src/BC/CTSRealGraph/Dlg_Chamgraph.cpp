// Dlg_Chamgraph.cpp : implementation file
//

#include "stdafx.h"
#include "ctsrealgraph.h"
#include "Dlg_Chamgraph.h"

#include "CTSRealGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chamgraph dialog


Dlg_Chamgraph::Dlg_Chamgraph(CWnd* pParent /*=NULL*/)
	: CDialog(Dlg_Chamgraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(Dlg_Chamgraph)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bStartDraw=FALSE;
}


void Dlg_Chamgraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Chamgraph)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Chamgraph, CDialog)
	//{{AFX_MSG_MAP(Dlg_Chamgraph)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chamgraph message handlers

BOOL Dlg_Chamgraph::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mainTabCham=this;
		
	m_wndGraphChamber.SubclassDlgItem(IDC_LAB_GRAPH_CHAMBER1, this);
	m_wndGraphChamber.m_GraphInfo=&g_GraphInfo;		

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_Chamgraph::OnOK() 
{
	CDialog::OnOK();
}

void Dlg_Chamgraph::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL Dlg_Chamgraph::onStart()
{	
	if(m_bStartDraw == FALSE)
	{			
		if(m_wndGraphChamber.Start(3)==FALSE) return FALSE;
		
		m_bStartDraw = TRUE;	
	}
	return TRUE;
}

void Dlg_Chamgraph::onStop()
{
	m_wndGraphChamber.Stop();
	
	m_bStartDraw = FALSE;
}


BOOL Dlg_Chamgraph::Fun_InitGraph()
{
	BOOL bflag=Fun_DrawFrame();
	Fun_HideGraph();

	m_wndGraphChamber.Fun_SubsetSetting(3);	
	return bflag;
}


BOOL Dlg_Chamgraph::Fun_DrawFrame()
{
	if(!mainDlg) return FALSE;

	int iCnt = 0;
	CString str;
	CString strTemp;

	if(m_wndGraphChamber.ClearGraph()==FALSE) return FALSE;

	iCnt = mainDlg->Fun_GetCheckChaminfo();
	m_wndGraphChamber.m_iYCount=iCnt;

	for(int i=0;i<4;i++)
	{// Chamber Graph Subset Setting
		if(g_GraphInfo.ChamInfo[i].bCheck==TRUE) 
		{
			m_wndGraphChamber.ShowSubset(i, iCnt, TRUE);
			
			strTemp.Format("%s",g_GraphInfo.ChamInfo[i].szname);
			m_wndGraphChamber.SetYAxisLabel(i, strTemp);
			m_wndGraphChamber.SetDataTitle (i, strTemp);	
		}
	}
	
	//Set X Axis Label
	m_wndGraphChamber.SetXAxisLabel("");
	
	//Set Title
	m_wndGraphChamber.SetMainTitle("Chamber Graph");

	return TRUE;
}

void Dlg_Chamgraph::Fun_HideGraph()
{
	if(!mainDlg) return;
	
	UpdateData(TRUE);
	
	RECT rt;
	GetClientRect(&rt);
			
	GetDlgItem(IDC_LAB_GRAPH_CHAMBER1)->MoveWindow(rt.left+10,rt.top+10,rt.right-20,rt.bottom-20,TRUE);
}

void Dlg_Chamgraph::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	RECT r;
	CRect ctrlRect, ctrlGrid;
	if(::IsWindow(m_hWnd))
	{
		::GetClientRect(m_hWnd, &r);
		
		if(::IsWindow(m_wndGraphChamber.GetSafeHwnd()))
		{
			Fun_HideGraph();
		}
	}		
}

BOOL Dlg_Chamgraph::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_ESCAPE || pMsg->wParam==VK_RETURN)
		{
			return FALSE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_Chamgraph::onGraphMax() 
{
	m_wndGraphChamber.SetMaximize();	
	
}
