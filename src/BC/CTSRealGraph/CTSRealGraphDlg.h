// CTSRealGraphDlg.h : header file
//

#if !defined(AFX_CTSREALGRAPHDLG_H__2D8AF7EA_96AC_4EFD_8B41_4C225365AEC9__INCLUDED_)
#define AFX_CTSREALGRAPHDLG_H__2D8AF7EA_96AC_4EFD_8B41_4C225365AEC9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PNE_define.h"
#include "ConfigSet_DLG.h"
#include "GraphWnd.h"
#include "RealGraphWnd.h"
#include "./Global/MyTabCtrl/MyTabCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CCTSRealGraphDlg dialog
class CCTSRealGraphDlg : public CDialog
{
// Construction
public:

	void Fun_DrawRealData();
	BOOL Fun_GetRealChData();
	void InitControl();
	BOOL Fun_LoadModuleInfo();
	int m_nModuleID;
	int m_nChIndex;
	CCTSRealGraphDlg(CWnd* pParent = NULL);	// standard constructor

	int Fun_GetCheckChinfo();
	int Fun_GetCheckChaminfo();
	int Fun_GetCheckCaninfo();
	int Fun_GetCheckAuxinfo();
	BOOL InitTabGraph() ;

public:
	CString Fun_getAppPath();
	BOOL Fun_LoadConfigFile();
		
	BOOL onStart();
	void Fun_DrawFrame();
	void Fun_HideGraph();
	
	CCyclerModule * m_lpCyclerMD;
	int m_nRealChDataPos;
	BOOL m_bStartDraw;
		
	void TabInit();

// Dialog Data
	//{{AFX_DATA(CCTSRealGraphDlg)
	enum { IDD = IDD_CTSREALGRAPH_DIALOG , IDD2 = IDD_CTSREALGRAPH_DIALOG_ENG };
	CButton	m_ButDrawStop;
	CButton	m_ButDrawStart;
	CButton	m_StaticChinfo;
	CMyTabCtrl	m_TabGraph;
	CString	m_CtrlLabChannelNum;
	CString	m_CtrlLabStepType;
	CString	m_CtrlLabStepNum;
	CString	m_CtrlLabCycleNum;
	CString	m_EditTestName;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSRealGraphDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCTSRealGraphDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButDrawStart();
	afx_msg void OnButDrawStop();
	afx_msg void OnButDrawSet();
	afx_msg void OnButDrawRangeSet();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButDrawMax();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CCTSRealGraphDlg *mainDlg;
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSREALGRAPHDLG_H__2D8AF7EA_96AC_4EFD_8B41_4C225365AEC9__INCLUDED_)
