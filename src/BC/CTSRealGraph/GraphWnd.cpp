// GraphWnd.cpp : implementation file
//

#include "stdafx.h"
#include "GraphWnd.h"
//#include "CTSRealGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeWnd
CDCRScopeWnd::CDCRScopeWnd()
{
	m_hWndPE = NULL;
	m_YColor[0] = RGB(230,  50,  0);
	m_YColor[1] = RGB(  35, 50, 200);
	m_YColor[2] = RGB(  0,255,  0);
	m_YColor[3] = RGB(255,255,  0);
	m_YColor[4] = RGB(  0,255,255);
	m_YColor[5] = RGB(255,192,255);
	m_YColor[6] = RGB(255,255,255);
	m_YColor[7] = RGB(192,192,255);

	m_SetsetNum = PE_MAX_SUBSET;
	m_TotalPoint = 0;
	m_ScreenPoint = 50000;
	m_Delay = 1000;

	m_TimerID = 0;

	for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		m_pData[i] = NULL;
		m_nDataPointCount[i] =0;
		m_bShowArray[i] = FALSE;
		m_dMin[i] = 0.0; 
		m_dMax[i] = 5.0;
	}

	m_bShowArray[0] = TRUE;
	m_TimerPtr = NULL;
	
}

CDCRScopeWnd::~CDCRScopeWnd()
{
	for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		if(m_pData[i])	
		{
			delete[] m_pData[i];
			m_pData[i] = NULL;
		}
	}
}

BEGIN_MESSAGE_MAP(CDCRScopeWnd, CStatic)
	//{{AFX_MSG_MAP(CDCRScopeWnd)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CHAR()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDCRScopeWnd message handlers

void CDCRScopeWnd::PreSubclassWindow() 
{
	CStatic::PreSubclassWindow();

	Fun_LoadConfigFile();
	
	CRect trect, rect;
	
	GetClientRect(trect);
	rect.left = 1;
	rect.right = trect.Width() - 2;
	rect.top = 1;
	rect.bottom = trect.Height() - 2;
    m_hWndPE = PEcreate(PECONTROL_SGRAPH, WS_VISIBLE|WS_BORDER, &rect, m_hWnd, 1000);   
	
	if(!m_hWndPE)
	{
		TRACE("Creation Error");
		return;
	}                     
	
	SetGlobalPEStuff();

	int	nArray[PE_MAX_SUBSET];
	int num=0;
	for(int i=0;i<m_SetsetNum;i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
	
	Fun_SetRealTimeGraph();
}

void CDCRScopeWnd::SetGlobalPEStuff()
{
	SetN(PEP_bPREPAREIMAGES, TRUE);					//mem DC 이용 
	//SetN(PEP_bDATASHADOWS, FALSE);				//Data의 그림자 
	SetN(PEP_nDATAPRECISION, 3);					//소수점 3자리 표기 
	SetN(PEP_bALLOWBESTFITLINE, TRUE);				//근사 곡선 사용 
	SetN(PEP_bALLOWHISTOGRAM, FALSE);				//Histogram 사용 여부 
	SetN(PEP_nGRIDLINECONTROL, PEGLC_BOTH);			//그래프 상에 Grid Line 
	
	SetN(PEP_bMOUSECURSORCONTROL, TRUE);			//마우스로  data를 Click하면 이동함 
	SetN(PEP_nCURSORMODE, PECM_NOCURSOR);			//현재 Data 위치 표시 방법
	SetN(PEP_bCURSORPROMPTTRACKING, TRUE);			//좌측 상단에 Mouse 위치 좌표 표시 
	SetN(PEP_nCURSORPROMPTSTYLE, 3);
	
	SetN(PEP_bALLOWUSERINTERFACE, TRUE);			//Popup 명령 사용			
	//SetN(PEP_bALLOWPOPUP, TRUE);					//Popup 명령 사용				
	SetN(PEP_bALLOWCUSTOMIZATION, TRUE);			//Cutsomize dialog enable
	
	SetN(PEP_bALLOWDATAHOTSPOTS, TRUE);				//마우스로 Click시 Event발생함 
	SetN(PEP_bALLOWSUBSETHOTSPOTS, TRUE);			//
	SetN(PEP_bALLOWPOINTHOTSPOTS, TRUE);			//
	SetN(PEP_bALLOWTABLEHOTSPOTS, TRUE);			//
	SetN(PEP_nFONTSIZE, PEFS_SMALL);				//전체적으로 글자체를 작게 한다.
	
	SetN(PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);		//가로세로 모두 확대 가능 
	
	//real time 
	//SetN(PEP_bAPPENDTOEND, TRUE);					//우에서 좌로 Scroll(실시간 data 그래프시)
	//SetSZ(PEP_szMANUALMAXPOINTLABEL, "##########");
	
	SetSZ(PEP_szMAINTITLEFONT, "Arial");			//
	SetN(PEP_nXAXISSCALECONTROL, PEAC_NORMAL);		//X축 Scale 방식 
	SetN(PEP_nYAXISSCALECONTROL, PEAC_AUTO);		//Y축 Scale 방식 
	
	SetN(PEP_nSUBSETS, m_SetsetNum);				//
	//SetN(PEP_nSUBSETS, 1);						//
	SetN(PEP_nPOINTS, 10000);						//
	//SetN(PEP_nPOINTS, m_TotalPoint);				//
	//SetN(PEP_nPOINTSIZE, PEPS_MICRO);				//
	
	//Graph Object
	SetN(PEP_nGRAPHPLUSTABLE, PEGPT_BOTH);				//Graph + Table 표시
	SetN(PEP_nPOINTSTOGRAPHINIT, PEPTGI_FIRSTPOINTS);	//초기 Point 위치 
	SetN(PEP_nFORCEVERTICALPOINTS, PEFVP_AUTO);			//X축 라벨 가로/세로 표기 방식
	//SetN(PEP_bNORANDOMPOINTSTOGRAPH, TRUE);			//
	//SetN(PEP_bFORCERIGHTYAXIS, TRUE);
	
	//SetN(PEP_nPOINTSTOGRAPH, m_ScreenPoint);			//해당 Point 수로 수평 Scroll 시킴 
	//SetN(PEP_nTARGETPOINTSTOTABLE, 10);	
	//SetN(PEP_nALTFREQTHRESHOLD, 10);
	
	//All graph
	SetVCell(PEP_szaPOINTLABELS, m_TotalPoint-1, " ");
	SetSZ(PEP_szMAINTITLE, "");
	SetSZ(PEP_szSUBTITLE, "");
	//SetSZ(PEP_szXAXISLABEL,(LPSTR)(LPCTSTR) m_strXName);
	SetN(PEP_dwDESKCOLOR, RGB(255,255,255));
	SetN(PEP_dwGRAPHBACKCOLOR, RGB(230,230,220));
	SetN(PEP_dwGRAPHFORECOLOR, RGB(192,192,192));
	//SetV(PEP_dwaSUBSETCOLORS, m_YColor, PE_MAX_SUBSET);
	
	SetAnnotation(TRUE);
	
	//PEnset(m_hWndPE, PEP_bAUTOSCALEDATA, FALSE);					//This property controls whether the ProEssentials will automatically scale data that is very small or very large.
	
	//Null Data setting
	double NullData = NULL_DATA;
	SetV(PEP_fNULLDATAVALUE, &NullData, 0);
	
	int slt[PE_MAX_SUBSET];
	int mas[PE_MAX_SUBSET];
 	for(int i = 0; i<PE_MAX_SUBSET; i++)
 	{
 		slt[i] = 0;
 		mas[i] = 1;
 	}
	SetV(PEP_naSUBSETLINETYPES, slt, PE_MAX_SUBSET);
	SetV(PEP_naMULTIAXESSUBSETS, mas, PE_MAX_SUBSET);
	
 	int oma = PE_MAX_SUBSET;
 	SetV(PEP_naOVERLAPMULTIAXES, &oma, 1);
	
   	SetN(PEP_nWORKINGAXIS, 0); 

}

void CDCRScopeWnd::InitSubset(int subID, int setID, COLORREF color, LPCTSTR title, LPCTSTR yname)
{	
	DOUBLE min, max;
	CString strTemp;

	SetN(PEP_nWORKINGAXIS, subID);
			
	SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);

	strTemp.Format("%s",m_pSubset[setID].cUnit);

	//// 20100924 SSL -> mV, mA, mAh 단위 체크
	if(strTemp.Left(1)=="m")									
	{
		min = m_pSubset[setID].fMin*1000;
		SetV(PEP_fMANUALMINY, &min, 0);
		max = m_pSubset[setID].fMax*1000;
		SetV(PEP_fMANUALMAXY, &max, 0);
	}
	else
	{
		min = m_pSubset[setID].fMin;
		SetV(PEP_fMANUALMINY, &min, 0);
		max = m_pSubset[setID].fMax;
		SetV(PEP_fMANUALMAXY, &max, 0);
	}

	//// 20100924 SSL -> 그래프 축 설정 
	if(setID < 4)
	{
		if(setID == 0)											// 20100924 SSL -> Time display
		{
			SetN(PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
			min = 0.0F;
			SetV(PEP_fMANUALMINX, &min, 0);
			max = m_pSubset[subID].graphtime1;
			SetV(PEP_fMANUALMAXX, &max, 0);
		}

		SetV(PEP_dwaSUBSETCOLORS, m_DrawChannelColor, PE_MAX_SUBSET);
		m_DrawChannelColor[subID] = color;
		SetN(PEP_dwYAXISCOLOR, m_DrawChannelColor[subID]);
	}

	else if(setID >= 4 && setID < 8)
	{
		if(setID == 4)
		{
			SetN(PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
			min = 0.0F;
			SetV(PEP_fMANUALMINX, &min, 0);
			max = m_pSubset[1].graphtime2;
			SetV(PEP_fMANUALMAXX, &max, 0);
		}

		SetV(PEP_dwaSUBSETCOLORS, m_DrawAuxCanColor, PE_MAX_SUBSET); 
		m_DrawAuxCanColor[subID] = color;
		SetN(PEP_dwYAXISCOLOR, m_DrawAuxCanColor[subID]);
	}

	else if(setID >= 8)
	{
		if(setID == 8)
		{
			SetN(PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
			min = 0.0F;
			SetV(PEP_fMANUALMINX, &min, 0);
			max = m_pSubset[2].graphtime3;
			SetV(PEP_fMANUALMAXX, &max, 0);
		}

		SetV(PEP_dwaSUBSETCOLORS, m_DrawChamberColor, PE_MAX_SUBSET);
		m_DrawChamberColor[subID] = color;
		SetN(PEP_dwYAXISCOLOR, m_DrawChamberColor[subID]);
	}

	SetN(PEP_nWORKINGAXIS, setID);

	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::Fun_SubsetSetting(int id)
{
	int iCnt;

	iCnt = 0;

	m_bAuxCanFlag = AfxGetApp()->GetProfileInt(REG_REAL_GRAPH, "AuxCanHide", 0);
 	m_bChamberFlag = AfxGetApp()->GetProfileInt(REG_REAL_GRAPH, "ChamberHide", 0);

	for(int i=id;i<id+4;i++)
	{
		if(m_pSubset[i].fMin != 0 &&  m_pSubset[i].fMax != 0)
		{
			InitSubset(iCnt, i, m_pSubset[i].lineColor);
			iCnt++;
		}
	}
}

void CDCRScopeWnd::SetYAxisLabel(int subID, CString str)
{
	SetN(PEP_nWORKINGAXIS, subID);
	SetSZ(PEP_szYAXISLABEL, (LPSTR)(LPCTSTR) str);
}

void CDCRScopeWnd::SetDataTitle(int nSubID, CString str)
{
	SetVCell(PEP_szaSUBSETLABELS, nSubID, (LPSTR)(LPCTSTR) str);
}

void CDCRScopeWnd::SetXAxisLabel(CString str)
{
	SetSZ(PEP_szXAXISLABEL,(LPSTR)(LPCTSTR) str);
}

void CDCRScopeWnd::ShowSubset(int id, int SetCnt, BOOL bShow)
{
	m_bShowArray[id] = bShow;
	int	nArray[8];
	int num=0;
	for(int i=0;i</*m_SetsetNum*/SetCnt;i++)
	{
		if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);
	PEreinitialize(m_hWndPE);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::SetSplit(BOOL bSplit)
{
	int oma[6];
	float map[6];
	if(bSplit)
	{
		float onesize = 1.0f / m_SetsetNum;
		for(int j=0;j<m_SetsetNum;j++)
		{
			oma[j] = 1;
			map[j] = onesize;
		}
		SetV(PEP_naOVERLAPMULTIAXES, oma, m_SetsetNum);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, m_SetsetNum);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_MEDIUM);
	}
	else
	{
		oma[0] = m_SetsetNum;
		map[0] = 1.0f;
		SetV(PEP_naOVERLAPMULTIAXES, oma, 1);
		SetV(PEP_faMULTIAXESPROPORTIONS, map, 0);
		SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_NONE);
	}
	PEreinitialize(m_hWndPE);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::ClearGraph()
{
	//Max point
	for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		if(m_pData[i] != NULL)
		{
			delete[]	m_pData[i];
			m_pData[i] = NULL;
			m_nDataPointCount[i] = 0;
		}
	}
	m_TotalPoint =0;

	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::Start(int iNo)
{
	SetTimer(iNo, 200, 0);
// 	m_Count = 0;
// 	m_OldTime = 99999999L;
// 
// 	SetN(PEP_nHORZSCROLLPOS, 0);
// 
// 	ClearGraph();
// 
// 	for(int i=0;i<m_SetsetNum;i++)
// 	{
// //		InitSubset(i,  m_dMin[i], m_dMax[i]);
// 	}
// 
// 	time(&m_StartTime);
// 
// 	if(m_TimerID)
// 		KillTimer(m_TimerID);
// 	m_TimerID = SetTimer(11, m_Delay, NULL);
}

void CDCRScopeWnd::Stop()
{
	KillTimer(0);
	KillTimer(1);
	KillTimer(2);
}

void CDCRScopeWnd::SetDataPtr(int id, int nCnt, float *fXData, float *fYData)
{
	//Data Copy
	if(m_pData[id] != NULL)
	{
		delete[]	m_pData[id];
		m_pData[id] = NULL;
	}
	m_pData[id] = new float[nCnt];
	memcpy(m_pData[id], fYData, sizeof(float)*nCnt);

	//Max point
	int nTotalPoint = 0;
	m_nDataPointCount[id] = nCnt;
	for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		if(m_pData[i] != NULL)
		{
			if(nTotalPoint < m_nDataPointCount[i])
				nTotalPoint = m_nDataPointCount[i];
		}
	}
	m_TotalPoint = nTotalPoint;

	SetN(PEP_nPOINTS, m_TotalPoint);	

	//Draw data
	char szName[48];
	float a, b;
	float fMin = 999999999, fMax = -999999999;
	for(int i=0; i<m_TotalPoint; i++)
	{
		a = fXData[i];
		if(i < m_nDataPointCount[id])
		{
			b = m_pData[id][i];
		}
		else
		{
			b = NULL_DATA;
		}

		PEvsetcell(m_hWndPE, PEP_faXDATA, i+id*m_TotalPoint, &a);
		PEvsetcell(m_hWndPE, PEP_faYDATA, i+id*m_TotalPoint, &b);
		
		sprintf(szName, "%f", fXData[i]);
		SetVCell(PEP_szaPOINTLABELS, i, szName);
	}
}

void CDCRScopeWnd::OnTimer(UINT nIDEvent) 
{
	double m;
	int timecount;

	switch(nIDEvent)
	{
		case 0:	
				Fun_SubsetSetting(0);
				
				if (m_nRealTimeCounter >= m_pSubset[0].graphtime1)
				{
					m_nRealTimeCounter = 0;
					m = 0.0F;
					PEvset(m_hWndPE, PEP_fZOOMMINX, &m, 0);
					m = m_pSubset[0].graphtime1;
					PEvset(m_hWndPE, PEP_fZOOMMAXX, &m, 0);
				}

				Fun_DrawChannelGraph();

				break;
			
 		case 1:	
				Fun_SubsetSetting(4);
				
				if (m_nRealTimeCounter2 >= m_pSubset[1].graphtime2)
				{
					m_nRealTimeCounter2 = 0;
					m = 0.0F;
					PEvset(m_hWndPE, PEP_fZOOMMINX, &m, 0);
					m = m_pSubset[1].graphtime2;
					PEvset(m_hWndPE, PEP_fZOOMMAXX, &m, 0);
				}

				if(m_bAuxCanFlag == 0) Fun_DrawAuxCanGraph();

 				break;
				
 	 	case 2 :		
				Fun_SubsetSetting(8);
				
				if (m_nRealTimeCounter3 >= m_pSubset[2].graphtime3)
				{
					m_nRealTimeCounter3 = 0;
					m = 0.0F;
					PEvset(m_hWndPE, PEP_fZOOMMINX, &m, 0);
					m = m_pSubset[2].graphtime3;
					PEvset(m_hWndPE, PEP_fZOOMMAXX, &m, 0);
				}

				if(m_bChamberFlag == 0) Fun_DrawChamberGraph();

				break;
	}

	m_nRealTimeCounter ++;
	m_nRealTimeCounter2 ++;
	m_nRealTimeCounter3 ++;

	PEvget(m_hWndPE, PEP_fZOOMMAXX, &m);

	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);

	CStatic::OnTimer(nIDEvent);
}

void CDCRScopeWnd::OnDestroy() 
{
	CStatic::OnDestroy();
	
	PEdestroy(m_hWndPE);
	if(m_TimerID)
		KillTimer(m_TimerID);
	
}

BOOL CDCRScopeWnd::OnEraseBkgnd(CDC* pDC) 
{
//	BOOL ret =  CStatic::OnEraseBkgnd(pDC);
//	::InvalidateRect(m_hWndPE, NULL, FALSE);
	return FALSE;
}

void CDCRScopeWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

//*********** Functions for Graph ******************************
void CDCRScopeWnd::SetN(UINT type, INT value)
{
	PEnset(m_hWndPE, type, value);
}

void CDCRScopeWnd::SetV(UINT type, VOID FAR * lpData, UINT nItems)
{
	PEvset(m_hWndPE, type, lpData, nItems);
}

void CDCRScopeWnd::SetVCell(UINT type, UINT nCell, VOID FAR * lpData)
{
	PEvsetcell(m_hWndPE, type, nCell, lpData);
}

void CDCRScopeWnd::SetVCellEx(UINT type, int subset, UINT nCell, VOID FAR *lpData)
{
	PEvsetcellEx(m_hWndPE, type, subset, nCell, lpData);
}

void CDCRScopeWnd::SetSZ(UINT type, CHAR FAR *str)
{
	PEszset(m_hWndPE, type, str);
}

void CDCRScopeWnd::Print()
{
	PElaunchprintdialog(m_hWndPE, TRUE, NULL);
}

void CDCRScopeWnd::SetData(int subset, int pos, LPCTSTR xData, double yData)
{
	float data = (float) yData;
	PEvsetcellEx(m_hWndPE, PEP_szaPOINTLABELS, subset, pos, (void FAR*) xData);
	PEvsetcellEx(m_hWndPE, PEP_faYDATA, subset, pos, &data);
}

void CDCRScopeWnd::SetMaximize()
{
	PElaunchmaximize(m_hWndPE);
}

void CDCRScopeWnd::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(nChar == 27 || nChar == VK_RETURN)
	{
		SetN(PEP_bZOOMMODE, FALSE);
		PEresetimage(m_hWndPE, 0, 0);
	}
	CStatic::OnChar(nChar, nRepCnt, nFlags);
}

BOOL CDCRScopeWnd::OnCommand(WPARAM wp, LPARAM lp)
{
    HOTSPOTDATA HSData;

    switch (HIWORD(wp))
    {
        case PEWN_CLICKED:
            PEvget(m_hWndPE, PEP_structHOTSPOTDATA, &HSData);
            if (HSData.nHotSpotType == PEHS_SUBSET)
            {
				/*
                char szTmp[128];
                char szSubset[48];
                szTmp[0] = 0;
                strcat(szTmp, "You clicked subset ");
                sprintf(szSubset, "%i", HSData.w1);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".  Subset label is ");
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".");
                MessageBox(szTmp, "Subset Drill Down", MB_OK | MB_ICONINFORMATION);
				*/
            } 
            else if (HSData.nHotSpotType == PEHS_POINT)
            {
				/*
                if (PECONTROL_PIE != PEnget(m_hWndPE, PEP_nOBJECTTYPE))
                {
                    char szTmp[128];
                    char szSubset[48];
                    szTmp[0] = 0;
                    strcat(szTmp, "You clicked point ");
                    sprintf(szSubset, "%i", HSData.w1);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".  Point label is ");
                    PEvgetcell(m_hWndPE, PEP_szaPOINTLABELS, (UINT) HSData.w1, szSubset);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".");
                    MessageBox(szTmp, "Point Drill Down", MB_OK | MB_ICONINFORMATION);
                }    
                else
                 return CView::OnCommand(wp, lp);
				 */
            }
            else if ((HSData.nHotSpotType == PEHS_TABLE) || (HSData.nHotSpotType == PEHS_DATAPOINT))
            {
				/*
                UINT offset = ((int) HSData.w1 * (int) PEnget(m_hWndPE, PEP_nPOINTS)) + (int) HSData.w2;
                char    szTmp[128];
                char    szSubset[128];
                char    szTmp2[48];
                float   fData;
                szTmp[0] = 0;
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, "항목 : ");
                strcat(szTmp, szSubset);
                strcat(szTmp, "  데이타값 : ");
                PEvgetcell(m_hWndPE, PEP_faYDATA, offset, (LPVOID) &fData);
                sprintf(szTmp2, "%.4f", fData);             
                strcat(szTmp, szTmp2);
                MessageBox(szTmp, "선택된 항목 값", MB_OK | MB_ICONINFORMATION);
				*/
            }
            return TRUE;
    }
    return CStatic::OnCommand(wp, lp);
}



void CDCRScopeWnd::SetAnnotation(BOOL bEnable)
{
    PEnset(m_hWndPE, PEP_bSHOWANNOTATIONS, bEnable);					//Annotation을 사용 한다.
	PEnset(m_hWndPE, PEP_bALLOWANNOTATIONCONTROL, bEnable);			//사용자가 선택 가능 
	PEnset(m_hWndPE, PEP_nLINEANNOTATIONTEXTSIZE, 100);				//Annotation을 보여 준다.
}

void CDCRScopeWnd::OnSize(UINT nType, int cx, int cy) 
{
	CStatic::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	RECT r;
	if(::IsWindow(this->GetSafeHwnd()))
	{
		::GetClientRect(m_hWnd, &r);
		::MoveWindow(m_hWndPE, 0, 0, r.right, r.bottom, TRUE);
	}	
}


void CDCRScopeWnd::SetMainTitle(CString str)
{
	SetSZ(PEP_szMAINTITLE, (LPSTR) (LPCTSTR) str);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::SetSubTitle(CString str)
{
	SetSZ(PEP_szSUBTITLE, (LPSTR) (LPCTSTR)  str);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CDCRScopeWnd::SaveGraph()
{
	PElaunchexport(m_hWndPE);
	//PElaunchtextexport(m_hWndPE, TRUE, "c:\\aa.txt");
}

void CDCRScopeWnd::AddXAnnotation(float fXData1, float fXData2, CString str1, CString str2)
{
	double	m_dVLA[2];							//Point
	int		m_nVLAT[2];						//Line Style
	long	m_lVLAC[2];						//Line Color
	char szBuff[64];

	sprintf(szBuff, "|t%s\t|t%s\t", str1, str2);
	m_dVLA[0] = fXData1;
	m_dVLA[1] = fXData2;
	m_nVLAT[0] = PELT_THINSOLID;
	m_nVLAT[1] = PELT_THINSOLID;
	m_lVLAC[0] = ANNOTATION_COLOR;
	m_lVLAC[1] = ANNOTATION_COLOR;

	//Vertical line annotation
	PEvset(m_hWndPE, PEP_faVERTLINEANNOTATION, m_dVLA, 2);
//	PEvset(m_hWndPE, PEP_szaVERTLINEANNOTATIONTEXT, szBuff, 2);
	PEvset(m_hWndPE, PEP_naVERTLINEANNOTATIONTYPE, m_nVLAT, 2);
	PEvset(m_hWndPE, PEP_dwaVERTLINEANNOTATIONCOLOR, m_lVLAC, 2);


	//X축 Annotation
	sprintf(szBuff, "%s", str1);
	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 0, &m_dVLA[0]);
	PEvsetcell(m_hWndPE, PEP_szaXAXISANNOTATIONTEXT, 0, szBuff);
	PEvsetcell(m_hWndPE, PEP_dwaXAXISANNOTATIONCOLOR, 0, &m_lVLAC[0]);
	sprintf(szBuff, "%s", str2);
	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 1, &m_dVLA[1]);
	PEvsetcell(m_hWndPE, PEP_szaXAXISANNOTATIONTEXT, 1, szBuff);
	PEvsetcell(m_hWndPE, PEP_dwaXAXISANNOTATIONCOLOR, 1, &m_lVLAC[1]);
	PEnset(m_hWndPE, PEP_nAXESANNOTATIONTEXTSIZE, 80);
		

//	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 0, szBuff);
//	PEnset(m_hWndPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);


	


/*  PEnset(m_hWndPE, PEP_bSHOWGRAPHANNOTATIONS, TRUE);					//Annotation을 사용 한다.
	double GAX[15] ={	70.0F, 93.0F, 70.0F, 170.0F, 195.0F, 170.0F,
                        270.0F, 295.0F, 270.0F,  370.0F, 395.0F, 370.0F,    
                        470.0F, 495.0F, 470.0F	};

    double GAY[15] = {	280.0F, 115.0F, 280.0F, 280.0F, 115.0F, 280.0F,
                        280.0F, 115.0F, 280.0F, 280.0F, 115.0F, 280.0F,
                        280.0F, 115.0F, 280.0F	};

    int GAT[15] = {		PEGAT_LINECONTINUE, PEGAT_DOWNTRIANGLESOLID, PEGAT_DOWNTRIANGLESOLID, 
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK};
    long GAC[15] = {	0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0)	};

	double GAX[15]	= {	 70.0F,  93.0F,  70.0F	};
    double GAY[15]	= {	280.0F, 115.0F, 280.0F	};
    int GAT[15]		= {	PEGAT_TOPLEFT, 	PEGAT_BOTTOMRIGHT,	PEGAT_LINECONTINUE};
    long GAC[15]	= {	0L, 0L, RGB(0,255,0)	};
	
	PEvset(m_hWndPE, PEP_faGRAPHANNOTATIONX, GAX, 3);
	PEvset(m_hWndPE, PEP_faGRAPHANNOTATIONY, GAY, 3);
	PEvset(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, GAT,3);
	PEvset(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, GAC, 3);

	int LAT[2] = {PEGAT_MEDIUMSOLIDLINE};
	long LAC[2] = {RGB(0, 0, 255)};
	char LAText[] = "Test Area\t";
	PEvset(m_hWndPE, PEP_naLEGENDANNOTATIONTYPE, LAT, 1);
	PEvset(m_hWndPE, PEP_dwaLEGENDANNOTATIONCOLOR, LAC, 1);
	PEvset(m_hWndPE, PEP_szaLEGENDANNOTATIONTEXT, LAText, 1);
*/            
	::InvalidateRect(m_hWndPE, NULL, FALSE);	
}

void CDCRScopeWnd::AddLineAnnotation(int nIndex, float fStartX, float fStartY, float fEndX, float fEndY, CString str)
{
    int nType = PEGAT_MEDIUMSOLIDLINE;
	DWORD color = ANNOTATION_COLOR;
	double fx, fy;

//	PEnset(m_hWndPE, PEP_bSHOWGRAPHANNOTATIONS, TRUE);					//Annotation을 사용 한다.

	fx = fStartX;
	fy = fStartY;
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONX, nIndex*2, &fx);
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONY, nIndex*2, &fy);
	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, nIndex*2, &color);
	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, nIndex*2, &nType);
	nType = PEGAT_LINECONTINUE;
	fx = fEndX;
	fy = fEndY;
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONX, nIndex*2+1, &fx);
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONY, nIndex*2+1, &fy);
	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, nIndex*2+1, &color);
	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, nIndex*2+1, &nType);

	char szBuff[64];
	ZeroMemory(szBuff, sizeof(szBuff));
	sprintf(szBuff, "%s", str);

	PEvsetcell(m_hWndPE, PEP_szaGRAPHANNOTATIONTEXT, nIndex*2, szBuff);
	PEvsetcell(m_hWndPE, PEP_szaGRAPHANNOTATIONTEXT, nIndex*2+1, "");
	PEnset(m_hWndPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);

//	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, m_nVLAT, 2);
//	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR,nIndex*2, &color);

	::InvalidateRect(m_hWndPE, NULL, FALSE);	
}
/*
void CDCRScopeWnd::CopyImg()
{
	PEcopybitmaptoclipboard(m_hWndPE, NULL);
}
*/

void CDCRScopeWnd::CustomizeDialog()
{
	PElaunchcustomize(m_hWndPE);
}

void CDCRScopeWnd::Fun_SetRealTimeGraph()
{
 	f1 = 0.0F;

 	PEvsetcell(m_hWndPE, PEP_faXDATA, 0, &f1);
 	PEvsetcell(m_hWndPE, PEP_faXDATA, 1, &f1);
 	PEvsetcell(m_hWndPE, PEP_faXDATA, 2, &f1);
	PEvsetcell(m_hWndPE, PEP_faXDATA, 3, &f1);

 	PEvsetcell(m_hWndPE, PEP_faYDATA, 0, &f1);
 	PEvsetcell(m_hWndPE, PEP_faYDATA, 1, &f1);
 	PEvsetcell(m_hWndPE, PEP_faYDATA, 2, &f1);
 	PEvsetcell(m_hWndPE, PEP_faYDATA, 3, &f1);

	PEnset(m_hWndPE, PEP_bALLOWDATAHOTSPOTS, FALSE);
	PEnset(m_hWndPE, PEP_bALLOWSUBSETHOTSPOTS, FALSE);
	PEnset(m_hWndPE, PEP_nALLOWDATALABELS, FALSE);
    PEnset(m_hWndPE, PEP_nALLOWZOOMING, FALSE);
}

BOOL CDCRScopeWnd::Fun_LoadConfigFile()
{
	char szData[30];
	CStdioFile file;
	CString strTemp;
	
	CString strConfigFile;
	strConfigFile.Format("%s\\Graph_Config_%02d_%02d.ini", Fun_getAppPath(),g_GraphInfo.nModuleID,g_GraphInfo.nChIndex);
	
	GetPrivateProfileString("UNIT1", "GRAPH1_TIME", NULL, szData, 512, strConfigFile );
	m_pSubset[0].graphtime1 = atof(szData);
	
	GetPrivateProfileString("UNIT1", "GRAPH2_TIME", NULL, szData, 512, strConfigFile );
	m_pSubset[1].graphtime2 = atof(szData);
	
	GetPrivateProfileString("UNIT1", "GRAPH3_TIME", NULL, szData, 512, strConfigFile );
	m_pSubset[2].graphtime3 = atof(szData);
	
	for(int i=0;i<12;i++)
	{
		strTemp.Format("DRAW%d_NAME",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, m_pSubset[i].szname, 512, strConfigFile); 
		if(i >= 4 && i < 8) m_strName[i-4].Format("%s",m_pSubset[i].szname);
		
		strTemp.Format("DRAW%d_COLOR",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		m_pSubset[i].lineColor = atof(szData); 
		
		strTemp.Format("DRAW%d_MIN",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		m_pSubset[i].fMin = atof(szData);
		
		strTemp.Format("DRAW%d_MAX",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		m_pSubset[i].fMax = atof(szData);
		
		strTemp.Format("DRAW%d_UNIT",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, m_pSubset[i].cUnit, 512, strConfigFile );
		
		strTemp.Format("DRAW%d_CHECK",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
		m_pSubset[i].bCheck = atoi(szData);
	}
	
	return TRUE;
}


CString CDCRScopeWnd::Fun_getAppPath()
{
    HMODULE hModule=::GetModuleHandle(NULL); // handle of current module
    
    CString strExeFileName;
    VERIFY(::GetModuleFileName(hModule, strExeFileName.GetBuffer(_MAX_PATH),_MAX_PATH));
	
    char Drive[_MAX_DRIVE];
    char Path[_MAX_PATH];
	
    _splitpath(strExeFileName, Drive, Path, NULL,NULL);//Filename, Ext);
	
    return CString(Drive)+CString(Path); // has trailing backslash
}

void CDCRScopeWnd::Fun_DrawChannelGraph()
{	
	Fun_LoadConfigFile();

	int icnt = 0;

	for(int i=0;i<4;i++)
	{
		if(m_pSubset[i].bCheck == 1) icnt++;
	}

	if(icnt > 0)
	{
		f1 = (float) m_nRealTimeCounter * 1.0F;
		SetVCellEx(PEP_faXDATA, 0, m_nRealTimeCounter/*0*/, &f1);
		
		double d1 = (double) f1;
		SetVCellEx(PEP_faVERTLINEANNOTATION, 0, 0, &d1);
		
		f1 = m_pRealChData->chData.lVoltage *0.001;      
		//f1=rand() % 100;

		SetVCellEx(PEP_faYDATA, 0, m_nRealTimeCounter/*0*/, &f1);						
	}
	
	if(icnt > 1)
	{		
		f2 = (float) m_nRealTimeCounter * 1.0F;
		SetVCellEx(PEP_faXDATA, 1, m_nRealTimeCounter/*0*/, &f2);
		
		double d2 = (double) f2;
		SetVCellEx(PEP_faVERTLINEANNOTATION, 1, 0, &d2);
		
		f2 = m_pRealChData->chData.lCurrent *0.001;
		SetVCellEx(PEP_faYDATA, 1, m_nRealTimeCounter/*0*/, &f2);
 	}

	if(icnt > 2)
	{
		 f3 = (float) m_nRealTimeCounter * 1.0F;
		 SetVCellEx(PEP_faXDATA, 2, m_nRealTimeCounter/*0*/, &f3);
		 		
		 double d3 = (double) f3;
		 SetVCellEx(PEP_faVERTLINEANNOTATION, 2, 0, &d3);
		 		
		 f3 = m_pRealChData->chData.lWatt *0.001;
		 SetVCellEx(PEP_faYDATA, 2, m_nRealTimeCounter/*0*/, &f3);
	}
		
	if(icnt > 3)
	{
		 f4 = (float) m_nRealTimeCounter * 1.0F;
		 SetVCellEx(PEP_faXDATA, 3, m_nRealTimeCounter/*0*/, &f4);
		 					
		 double d4 = (double) f4;
		 SetVCellEx(PEP_faVERTLINEANNOTATION, 3, 0, &d4);
		 		
		 f4 = m_pRealChData->chData.lCapacitance *0.001;
		 SetVCellEx(PEP_faYDATA, 3, m_nRealTimeCounter/*0*/, &f3);
 	}
}

void  CDCRScopeWnd::Fun_DrawAuxCanGraph()
{
	int iTemp;
	int iCnt = 0;
	CString strTemp;

	Fun_LoadConfigFile();

	if(m_strName[0] != "") 
	{
		strTemp = m_strName[0].Mid(9,1);
		iTemp = atoi(strTemp);
		iCnt++;
	}

 	f5 = (float) m_nRealTimeCounter2 * 1.0F;
	SetVCellEx(PEP_faXDATA, 0, m_nRealTimeCounter2, &f5);
							
	double d5 = (double) f5;
	SetVCellEx(PEP_faVERTLINEANNOTATION, 0, 0, &d5);
	
	if(m_strName[0].Left(3) == "AUX")
	{
		f5 = m_pRealChData->auxData[iTemp].lValue;       
		SetVCellEx(PEP_faYDATA, 0, m_nRealTimeCounter2, &f5);
	}
	
	else if(m_strName[0].Left(3) == "CAN")
	{
		f5 = m_pRealChData->canData->canVal.lVal[iTemp];       
		SetVCellEx(PEP_faYDATA, 0, m_nRealTimeCounter2, &f5);
	}

	if(m_strName[1] != "")
	{
		strTemp = m_strName[1].Mid(9,1);
		iTemp = atoi(strTemp);
		iCnt++;
	}
	
	f6 = (float) m_nRealTimeCounter2 * 1.0F;
	SetVCellEx(PEP_faXDATA, 1, m_nRealTimeCounter2, &f6);
	
	double d6 = (double) f6;
	SetVCellEx(PEP_faVERTLINEANNOTATION, 1, 0, &d6);

	if(m_strName[1].Left(3) == "AUX")
	{
		f6 = m_pRealChData->auxData[iTemp].lValue;       
		SetVCellEx(PEP_faYDATA, 1, m_nRealTimeCounter2, &f6);
	}
	
	else if(m_strName[1].Left(3) == "CAN")
	{
		f6 = m_pRealChData->canData->canVal.lVal[iTemp];       
		SetVCellEx(PEP_faYDATA, 1, m_nRealTimeCounter2, &f6);
	}

	if(m_strName[2] != "")
	{
		strTemp = m_strName[2].Mid(9,1);
		iTemp = atoi(strTemp);
		iCnt++;
	}

	f7 = (float) m_nRealTimeCounter2 * 1.0F;
	SetVCellEx(PEP_faXDATA, 2, m_nRealTimeCounter2, &f7);
	 	
	double d7 = (double) f7;
	SetVCellEx(PEP_faVERTLINEANNOTATION, 2, 0, &d7);
	 	
	if(m_strName[2].Left(3) == "AUX")
	{
		f7 = m_pRealChData->auxData[iTemp].lValue;
 		SetVCellEx(PEP_faYDATA, 2, m_nRealTimeCounter2, &f7);
	}
	else if(m_strName[2].Left(3) == "CAN")
	{
		f7 = m_pRealChData->canData->canVal.lVal[iTemp];       
		SetVCellEx(PEP_faYDATA, 1, m_nRealTimeCounter2, &f7);
	}

	if(m_strName[3] != "")
	{
		strTemp = m_strName[3].Mid(9,1);
		iTemp = atoi(strTemp);
		iCnt++;
	}

	f8 = (float) m_nRealTimeCounter2 * 1.0F;
	SetVCellEx(PEP_faXDATA, 3, m_nRealTimeCounter2, &f8);
		
	double d8 = (double) f8;
	SetVCellEx(PEP_faVERTLINEANNOTATION, 3, 0, &d8);
	 	
	if(m_strName[3].Left(3) == "AUX")
	{
		f8 = m_pRealChData->auxData[iTemp].lValue;
 		SetVCellEx(PEP_faYDATA, 3, m_nRealTimeCounter2, &f8);
	}
	else if(m_strName[3].Left(3) == "CAN")
	{
		f8 = m_pRealChData->canData->canVal.lVal[iTemp];       
		SetVCellEx(PEP_faYDATA, 1, m_nRealTimeCounter2, &f8);
	}

	if(iCnt == 0) AfxGetApp()->WriteProfileInt(REG_REAL_GRAPH ,"AuxCanHide", 1);
}	

void  CDCRScopeWnd::Fun_DrawChamberGraph()
{
	Fun_LoadConfigFile();

	int icnt = 0;
	
	for(int i=8;i<12;i++)
	{
		if(m_pSubset[i].bCheck == 1) icnt++;
	}

	if(icnt == 0) AfxGetApp()->WriteProfileInt(REG_REAL_GRAPH ,"ChamberHide", 1);
	
	if(icnt > 0)
	{
		f9 = (float) m_nRealTimeCounter3 * 1.0F;
		SetVCellEx(PEP_faXDATA, 0, m_nRealTimeCounter3/*0*/, &f9);
		 							
		double d9 = (double) f9;
		SetVCellEx(PEP_faVERTLINEANNOTATION, 0, 0, &d9);
		 
		f9 = g_lchamberdata1;       
		SetVCellEx(PEP_faYDATA, 0, m_nRealTimeCounter3/*0*/, &f9);
	}
			
	if(icnt > 1)
	{
	 	f10 = (float) m_nRealTimeCounter3 * 1.0F;
	 	SetVCellEx(PEP_faXDATA, 1, m_nRealTimeCounter3/*0*/, &f10);
	 			
	 	double d10 = (double) f10;
	 	SetVCellEx(PEP_faVERTLINEANNOTATION, 1, 0, &d10);
	 			
	  	f10 = g_lchamberdata2;
	  	SetVCellEx(PEP_faYDATA, 1, m_nRealTimeCounter3/*0*/, &f10);
	}
	  		
	if(icnt > 2)
	{
	 	f11 = (float) m_nRealTimeCounter3 * 1.0F;
		SetVCellEx(PEP_faXDATA, 2, m_nRealTimeCounter3/*0*/, &f11);
					
		double d11 = (double) f11;
		SetVCellEx(PEP_faVERTLINEANNOTATION, 2, 0, &d11);
		 			
		f11 = g_lchamberdata3;
		SetVCellEx(PEP_faYDATA, 2, m_nRealTimeCounter3/*0*/, &f11);
	}
		 		
	if(icnt > 3)
	{
	 	f12 = (float) m_nRealTimeCounter3 * 1.0F;
	 	SetVCellEx(PEP_faXDATA, 3, m_nRealTimeCounter3/*0*/, &f12);
	 			
		double d12 = (double) f12;
	 	SetVCellEx(PEP_faVERTLINEANNOTATION, 3, 0, &d12);
	 			
	 	f12 = g_lchamberdata4;
	    SetVCellEx(PEP_faYDATA, 3, m_nRealTimeCounter3/*0*/, &f12);
	}

}

