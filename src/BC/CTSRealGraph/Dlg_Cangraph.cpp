// Dlg_Cangraph.cpp : implementation file
//

#include "stdafx.h"
#include "ctsrealgraph.h"
#include "Dlg_Cangraph.h"

#include "CTSRealGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Cangraph dialog


Dlg_Cangraph::Dlg_Cangraph(CWnd* pParent /*=NULL*/)
	: CDialog(Dlg_Cangraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(Dlg_Cangraph)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bStartDraw=FALSE;
}


void Dlg_Cangraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Cangraph)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Cangraph, CDialog)
	//{{AFX_MSG_MAP(Dlg_Cangraph)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Cangraph message handlers

BOOL Dlg_Cangraph::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mainTabCan=this;
	
	m_wndGraphCan.SubclassDlgItem(IDC_LAB_GRAPH_CAN1, this);
	m_wndGraphCan.m_GraphInfo=&g_GraphInfo;	

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void Dlg_Cangraph::OnOK() 
{
	CDialog::OnOK();
}

void Dlg_Cangraph::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL Dlg_Cangraph::onStart()
{	
	if(m_bStartDraw == FALSE)
	{					
		if(m_wndGraphCan.Start(2)==FALSE) return FALSE;
		
		m_bStartDraw = TRUE;	
	}
	return TRUE;
}

void Dlg_Cangraph::onStop()
{
	m_wndGraphCan.Stop();
	
	m_bStartDraw = FALSE;
}


BOOL Dlg_Cangraph::Fun_InitGraph()
{
	BOOL bflag=Fun_DrawFrame();
	Fun_HideGraph();

	m_wndGraphCan.Fun_SubsetSetting(2);	
	return bflag;
}

BOOL Dlg_Cangraph::Fun_DrawFrame()
{
	if(!mainDlg) return FALSE;

	int iCnt = 0;
	CString str;
	CString strTemp;

	if(m_wndGraphCan.ClearGraph()==FALSE) return FALSE;
	
	iCnt = mainDlg->Fun_GetCheckCaninfo();		
	m_wndGraphCan.m_iYCount=iCnt;

	int i=0;
	int nItem=0;
	for(int i = 0;i<g_GraphInfo.nCanCount;i++)
	{// Aux Can Graph Subset Setting
				
		if(g_GraphInfo.CanInfo[i].bCheck==TRUE) 
		{
			m_wndGraphCan.ShowSubset(nItem, iCnt, TRUE);    
		
			strTemp.Format("%s",g_GraphInfo.CanInfo[i].strAlias);
			m_wndGraphCan.SetYAxisLabel(nItem, strTemp);
			m_wndGraphCan.SetDataTitle (nItem, strTemp);
			nItem=nItem+1;
		}
	}
	
	//Set X Axis Label	
	m_wndGraphCan.SetXAxisLabel("");
	
	//Set Title
	m_wndGraphCan.SetMainTitle("CAN Graph");
	return TRUE;
}



void Dlg_Cangraph::Fun_HideGraph()
{
	if(!mainDlg) return;
	
	UpdateData(TRUE);
	
	RECT rt;
	GetClientRect(&rt);
	
	GetDlgItem(IDC_LAB_GRAPH_CAN1)->MoveWindow(rt.left+10,rt.top+10,rt.right-20,(rt.bottom)-20,TRUE);
}


void Dlg_Cangraph::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	RECT r;
	CRect ctrlRect, ctrlGrid;
	if(::IsWindow(m_hWnd))
	{
		::GetClientRect(m_hWnd, &r);
		
		if(::IsWindow(m_wndGraphCan.GetSafeHwnd()))
		{
			Fun_HideGraph();
		}
	}	
		
}

BOOL Dlg_Cangraph::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_ESCAPE || pMsg->wParam==VK_RETURN)
		{
			return FALSE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_Cangraph::onGraphMax() 
{
	m_wndGraphCan.SetMaximize();		
}
