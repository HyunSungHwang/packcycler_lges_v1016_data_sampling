#if !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
#define AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GraphWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDCRScopeWnd window
#include "../../../include/PEGRPAPI.h"
#include "../../../include/pemessag.h"


#define NULL_DATA		999999999
#define MAX_COUNT        1024
#define GRID_INTERVAL    10

#define MAX_SUBSET_INFO 12
#define MAX_REAL_GRAPH 12

#define ANNOTATION_COLOR	RGB(0,200,0)

//class CCTSRealGraphDlg : public CDialog;

class CDCRScopeWnd : public CStatic
{
// Construction
public:
	CDCRScopeWnd();

	typedef struct Graph_Subset_Info							// 20100922 SSL -> Graph Subset을 위한 구조체
	{
		float graphtime1;
		float graphtime2;
		float graphtime3;
		char szname[MAX_SUBSET_INFO];
		char cUnit[MAX_SUBSET_INFO];
		float fMin;
		float fMax;
		COLORREF lineColor;		
		BOOL bCheck;
		CString strAlias;
	} Graph_Subset_Info;
	
	Graph_Subset_Info m_pSubset[MAX_SUBSET_INFO];

	COLORREF m_DrawChannelColor[MAX_REAL_GRAPH];						// 20100923 SSL -> 화면에 그려주는 실시간 Channel 그래프의 색상을 저장하는 배열  
	COLORREF m_DrawAuxCanColor[MAX_REAL_GRAPH];							// 20100923 SSL -> 화면에 그려주는 실시간 Aux Can 그래프의 색상을 저장하는 배열
	COLORREF m_DrawChamberColor[MAX_REAL_GRAPH];						// 20100923 SSL -> 화면에 그려주는 실시간 Chamber 그래프의 색상을 저장하는 배열



// Attributes
public:
	HWND	m_hWndPE;
	int		m_SetsetNum;
	int		m_TotalPoint;
	int		m_ScreenPoint;
	int		m_Delay;
	COLORREF	m_YColor[PE_MAX_SUBSET];
	unsigned long *		m_TimerPtr;
	unsigned long	m_OldTime;

	int			m_TimerID;
	float *		m_pData[PE_MAX_SUBSET];
	int			m_nDataPointCount[PE_MAX_SUBSET];
	BOOL		m_bShowArray[PE_MAX_SUBSET];

	time_t		m_StartTime;

	CString		m_strName[PE_MAX_SUBSET];		
	
	BOOL m_bAuxCanFlag;												// 20100924 SSL -> Aux Can 그래프를 화면에 표시할 지 여부를 결정하는 부울 함수
	BOOL m_bChamberFlag;											// 20100924 SSL -> Chamber 그래프를 화면에 표시할 지 여부를 결정하는 부울 함수

	

// Operations
public:
	void SetN(UINT type, INT value);
	void SetV(UINT type, VOID FAR * lpData, UINT nItems);
	void SetSZ(UINT type, CHAR FAR *str);
	void SetVCell(UINT type, UINT nCell, VOID FAR * lpData);
	void SetVCellEx(UINT type, int subset, UINT nCell, VOID FAR *lpData);

	void InitSubset(int subID, int setID, COLORREF color, LPCTSTR title = NULL, LPCTSTR yname = NULL);

	void ShowSubset(int id, int SetCnt, BOOL bShow);
	void SetSplit(BOOL bSplit);
	void Start(int iNo);
	void Stop();
	void SetDataPtr(int id, int nCnt, float *fXData, float *fYData);
	void ClearGraph();
	void Print();
	void SetData(int subset, int pos, LPCTSTR xData, double yData);
	void SetMaximize();
	CString Fun_getAppPath();

	void Fun_DrawChannelGraph();								// 20100923 SSL -> 실시간 Channel Graph 그리기 
	void Fun_DrawAuxCanGraph();									// 20100923 SSL -> 실시간 Aux Can Graph 그리기
	void Fun_DrawChamberGraph();								// 20100923 SSL -> 실시간 Chamber Graph 그리기

	void Fun_SubsetSetting(int id);								// 20100920 SSL -> Subset값 세팅 
	
	virtual BOOL OnCommand( WPARAM wParam, LPARAM lParam);

//	CCTSRealGraphDlg *pGDlg;

protected:
//	CString		m_strMainTitle;
//	CString		m_strSubTitle;
//	CString		m_strXName;

	void SetGlobalPEStuff();
	double		m_dMin[PE_MAX_SUBSET], m_dMax[PE_MAX_SUBSET];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCRScopeWnd)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	float f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12;
	void Fun_SetRealTimeGraph();
	BOOL Fun_LoadConfigFile();
	void CustomizeDialog();
//	void CopyImg();
	void AddLineAnnotation(int nIndex, float fStartX, float fStartY, float  fEndX, float fEndY, CString str);
	void AddXAnnotation(float fXData1, float fXData2, CString str1, CString str2);
	void SaveGraph();
	void SetSubTitle(CString str);
	void SetMainTitle(CString str);
	void SetAnnotation(BOOL bEnable);
	void SetDataTitle(int nSubID, CString str);
	void SetYAxisLabel(int subID, CString str);
	void SetXAxisLabel(CString str);
	virtual ~CDCRScopeWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDCRScopeWnd)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
