#if !defined(AFX_DLG_CANGRAPH_H__FFE763AE_6625_49FA_8B3C_A83017189179__INCLUDED_)
#define AFX_DLG_CANGRAPH_H__FFE763AE_6625_49FA_8B3C_A83017189179__INCLUDED_

#include "GraphWnd.h"
#include "RealGraphWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Cangraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_Cangraph dialog

class Dlg_Cangraph : public CDialog
{
// Construction
public:
	Dlg_Cangraph(CWnd* pParent = NULL);   // standard constructor

	BOOL m_bStartDraw;
	
	CRealGraphWnd_V2 m_wndGraphCan;
	
	BOOL Fun_DrawFrame();
	void Fun_HideGraph();
	BOOL Fun_InitGraph();

	void onGraphMax();
	BOOL onStart();
	void onStop();

// Dialog Data
	//{{AFX_DATA(Dlg_Cangraph)
	enum { IDD = IDD_DIALOG_CANGRAPH };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Cangraph)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Cangraph)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
extern Dlg_Cangraph *mainTabCan;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CANGRAPH_H__FFE763AE_6625_49FA_8B3C_A83017189179__INCLUDED_)
