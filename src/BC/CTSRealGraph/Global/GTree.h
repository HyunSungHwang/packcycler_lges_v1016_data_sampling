typedef struct tagLVID
{
	LPSHELLFOLDER lpsfParent;
	LPITEMIDLIST  lpi;
	ULONG         ulAttribs;
} LVITEMDATA, *LPLVITEMDATA;

typedef struct tagID
{
	LPSHELLFOLDER lpsfParent;
	LPITEMIDLIST  lpi;
	LPITEMIDLIST  lpifq;
} TVITEMDATA, *LPTVITEMDATA;

class GTree
{
public:   
	GTree();
    ~GTree();
    		

	static void tree_Font(CTreeCtrl *pTree)     
	{//한글 깨질경우 폰트설정함.
		LOGFONT logfont=GGdc::gdc_LogFont(_T("14/0/0/0/400/0/0/0/1/0/0/0/2/Arial"));
		CFont *font=GGdc::gdc_CFont(logfont);
		pTree->SetFont(font);
	}


	static void tree_Renum(CTreeCtrl *pTree,CString stitle=_T(""))
	{
		HTREEITEM tree_root=pTree->GetRootItem();
		if(tree_root==NULL) return;

		CString temp;
		int count=1;
		do 
		{
		  temp.Format(_T("%s%d"),stitle,count);	  
		  pTree->SetItemText(tree_root,temp);
		  tree_root=pTree->GetNextItem(tree_root,TVGN_NEXT);
		  count=count+1;
		} while (tree_root!=NULL);
	}

	static void tree_Drag(HWND hWnd,CTreeCtrl *pTree,HTREEITEM m_dragItem,CPoint point)
	{
		CImageList* pDragImage;
		pDragImage = pTree->CreateDragImage(m_dragItem);
		pTree->SelectItem( m_dragItem );
		pDragImage->BeginDrag( 0, CPoint(0,0) );
		pDragImage->DragEnter( pTree, point);
		::SetCapture(hWnd);
		delete pDragImage;
	}

	static HTREEITEM tree_DragMove(HWND mhWnd,HWND thWnd,int top,CTreeCtrl *pTree,CPoint point)
	{
		CPoint ptTree( point );
		::MapWindowPoints(mhWnd, thWnd, &ptTree, 1 );
		CImageList::DragMove( ptTree );
		UINT uHitTest = TVHT_ONITEM;
		HTREEITEM m_dragTarget = pTree->HitTest( ptTree, &uHitTest);
	    
		CImageList::DragShowNolock(FALSE);
		if( point.y < top + 10 ) ::SendMessage(thWnd,WM_VSCROLL, SB_LINEUP,0);
		else ::SendMessage(thWnd,WM_VSCROLL, SB_LINEDOWN,0);
		pTree->Select(m_dragTarget,TVGN_CARET);		
		CImageList::DragShowNolock(TRUE);
		
		return m_dragTarget;
	}

	static void tree_Drop(CTreeCtrl *pTree)
	{
		CImageList::DragLeave(pTree);
		CImageList::EndDrag();
		ReleaseCapture();
	}

	static int tree_RootCount(CTreeCtrl *pTree)
	{
		int i=-1;
		HTREEITEM tree_root=pTree->GetRootItem();	
	    
		while(tree_root!=NULL)
		{
		  i=i+1;	
		  tree_root=pTree->GetNextItem(tree_root,TVGN_NEXT);	    
		}

		return i;
	}

	static HTREEITEM tree_RootSelect(CTreeCtrl *pTree,int item)
	{
		int i=-1;
		HTREEITEM tree_root=pTree->GetRootItem();	
	    
		while(tree_root!=NULL){
		  i=i+1;
		  if(item==i) return tree_root;

		  tree_root=pTree->GetNextItem(tree_root,TVGN_NEXT);	  
		 	  
		}
		return NULL;
	}

	static int tree_RootStep(CTreeCtrl *pTree,HTREEITEM tree_item)
	{
		int i=-1;
		HTREEITEM tree_root=pTree->GetRootItem();	
	    
		while(tree_root!=NULL){
		  i=i+1;
		  if(tree_root==tree_item) return i;

		  tree_root=pTree->GetNextItem(tree_root,TVGN_NEXT);	  
		}

		return i;
	}

	static int tree_ChildStep(CTreeCtrl *pTree,HTREEITEM tree_root,HTREEITEM tree_item)
	{
		int i=-1;
		HTREEITEM tree_step=pTree->GetChildItem(tree_root);	
	    
		while(tree_step!=NULL)
		{
		  i=i+1;	 
		  if(tree_step==tree_item) return i;

		  tree_step=pTree->GetNextItem(tree_step,TVGN_NEXT);	  		   
		}

		return i;
	}

	static int tree_ChildCount(CTreeCtrl *pTree,HTREEITEM tree_root)
	{
		int i=-1;
		HTREEITEM tree_step=pTree->GetChildItem(tree_root);	
	    
		while(tree_step!=NULL){
		  i=i+1;	 
		  tree_step=pTree->GetNextItem(tree_step,TVGN_NEXT);	  
		}

		return i;
	}

	static HTREEITEM tree_stepSelect(CTreeCtrl *pTree,HTREEITEM tree_root,int nItem)
	{
		int i=-1;
		HTREEITEM tree_step=pTree->GetChildItem(tree_root);	
	    
		while(tree_step!=NULL){
		  i=i+1;	 
		  if(i==nItem) return tree_step;

		  tree_step=pTree->GetNextItem(tree_step,TVGN_NEXT);	  
		  
		}

		return NULL;
	}

	static HTREEITEM tree_SelectFind(CTreeCtrl *pTree,HTREEITEM tree_root,CString szText)
	{
		if(!tree_root) tree_root=pTree->GetSelectedItem();
		if(!tree_root) return NULL;
		
		HTREEITEM tree_step=pTree->GetChildItem(tree_root);	
	    
		while(tree_step!=NULL)
		{
		  CString stext=pTree->GetItemText(tree_step);
		  if(stext==szText) return tree_step;

		  tree_step=pTree->GetNextItem(tree_step,TVGN_NEXT);		  
		}

		return NULL;
	}

	static void tree_SetFocus(CTreeCtrl *pTree,HTREEITEM tree_item,BOOL flag)
	{
		if(!tree_item) return;

		if(flag==TRUE)
		{
			pTree->SetItemState(tree_item, LVIS_SELECTED, TVIS_SELECTED);	
			pTree->SelectItem(tree_item);			
		}
		else 
		{
			pTree->SetItemState(tree_item, ~LVIS_SELECTED, TVIS_SELECTED);	
		}
	}

	static int tree_ImgCount(CTreeCtrl *pTree,HTREEITEM tree_item)
	{	
		int i,j;    
		if(tree_item!=NULL){
		  if(pTree->GetItemImage(tree_item,i,j)==TRUE)	return i;
		}
		return -1;
	}

	static HTREEITEM tree_CurSelRootItem(CTreeCtrl *pTree)
	{
		HTREEITEM tree_curt,tree_paret;

		tree_curt=pTree->GetSelectedItem(); 

		while(tree_curt!=NULL)
		{
		  tree_paret=pTree->GetParentItem(tree_curt);
		  if(tree_paret==NULL) break;
		  else tree_curt=tree_paret;
		}
		return tree_curt;
	}


	static HTREEITEM tree_FindItem(CTreeCtrl *pTree,CString szText, HTREEITEM hItem)
	{//find text		
		HTREEITEM htiSelected = hItem ? hItem : pTree->GetSelectedItem();
		HTREEITEM htiCurrent = tree_GetNextItemEx(pTree,htiSelected);
			
		if(htiCurrent == NULL)
		{
			htiCurrent = pTree->GetRootItem();
		}
		
		while(htiCurrent && htiCurrent != htiSelected)
		{
			CString strItemText = pTree->GetItemText(htiCurrent);
						
			if (strItemText==szText)
			{
				// found string
				return htiCurrent;
			}
		
			// get next
			htiCurrent = tree_GetNextItemEx(pTree,htiCurrent);
			if(htiCurrent == NULL && htiSelected != NULL)
			{
				htiCurrent = pTree->GetRootItem();				
			}
		}
		
		return NULL;
	}

	static HTREEITEM tree_GetNextItemEx(CTreeCtrl *pTree,HTREEITEM hItem)
	{//next item
		HTREEITEM hti;

		if(pTree->ItemHasChildren(hItem))
		{			
			// return first child
			return pTree->GetChildItem(hItem);           
		}
		else
		{
			// return next sibling item
			// Go up the tree to find a parent's sibling if needed.
			while((hti = pTree->GetNextSiblingItem(hItem)) == NULL)
			{
				if((hItem = pTree->GetParentItem(hItem)) == NULL) return NULL;
			}
		}
		return hti;
	}

	static void tree_DeleteText(CTreeCtrl *pTree,CString szText)
	{// delete folder from tree too

		HTREEITEM hCurrentTreeItem = pTree->GetSelectedItem();
		if(!hCurrentTreeItem) return;

		pTree->Expand(hCurrentTreeItem, TVE_EXPAND);

		HTREEITEM hItem = tree_FindItem(pTree,szText, hCurrentTreeItem);
		if(!hItem) return;

		pTree->DeleteItem(hItem);			
	}


	static void tree_SelListDelText(CTreeCtrl *pTree,CPtrArray *SelList)
	{//
		if(!SelList || SelList->GetSize()<1) return;

		for(int i=0;i<SelList->GetSize();i++)
		{
			FILEINFO *FileInfo=(FILEINFO*)SelList->GetAt(i);
			if(!FileInfo) continue;
					
			if (FileInfo->bIsDirectory==TRUE)
			{//direcory
				GTree::tree_DeleteText(pTree,FileInfo->strFileName);			
			}			
		}
	}

	static HTREEITEM tree_SelectText(CTreeCtrl *pTree,CString szText) 
	{
		HTREEITEM hCurrentTreeItem = pTree->GetSelectedItem();
		if(!hCurrentTreeItem) return NULL;
		
		pTree->Expand(hCurrentTreeItem, TVE_EXPAND);
		HTREEITEM hItem = tree_FindItem(pTree,szText, hCurrentTreeItem);
		if(!hItem) return NULL;

		pTree->SelectItem(hItem);	
		return hItem;
	}


	static void tree_SelectDelete(CTreeCtrl *pTree,HTREEITEM hParent) 
	{		
		// remove any children of this directory
		HTREEITEM hItem = pTree->GetChildItem(hParent);
		HTREEITEM hTemp;

		while (hItem != NULL)
		{
			hTemp = pTree->GetNextSiblingItem(hItem);
			pTree->DeleteItem(hItem);
			hItem = hTemp;
		}	
	}

	static HTREEITEM tree_SelectClick(CTreeCtrl *pTree) 
	{
		CPoint point;
		UINT uFlags;		
		GetCursorPos(&point);	
		pTree->ScreenToClient(&point);
		HTREEITEM hItem = pTree->HitTest(point, &uFlags);
		if((hItem != NULL) && (uFlags & TVHT_ONITEM))
		{	
			return hItem;
		}
		return NULL;
	}

	static void tree_TvDeleteItem(LPNMTREEVIEW pNMTreeView) 
	{	
		if(!pNMTreeView) return;

		CComPtr<IMalloc> spMalloc;	
		HRESULT hr=SHGetMalloc(&spMalloc);		
		if (FAILED(hr)) return;
		
		LPTVITEMDATA lptvid	=(LPTVITEMDATA)pNMTreeView->itemOld.lParam;
		if(!lptvid) return;
				
		spMalloc->Free(lptvid->lpi);  
		spMalloc->Free(lptvid->lpifq);  
		spMalloc->Free(lptvid); 	
	}

	static BOOL tree_IsRoot(CTreeCtrl *pTree,HTREEITEM tree_item)
	{
		HTREEITEM tree_root=pTree->GetRootItem();	
	    
		while(tree_root!=NULL)
		{
		  if(tree_root==tree_item) return TRUE;

		  tree_root=pTree->GetNextItem(tree_root,TVGN_NEXT);	  
		}

		return FALSE;
	}

} ;