#define MSGQUEST(hWnd,msg,title) ::MessageBox(hWnd, msg, title, MB_ICONQUESTION|MB_YESNO)
#define MSGQUEOK(hWnd,msg,title) ::MessageBox(hWnd, msg, title, MB_ICONQUESTION|MB_YESNOCANCEL)
#define MSGQUEOC(hWnd,msg,title) ::MessageBox(hWnd, msg, title, MB_ICONQUESTION|MB_OKCANCEL)

#define MSGERROR(hWnd,msg,title) ::MessageBox(hWnd, msg, title, MB_ICONERROR|MB_OK)
#define MSGWARN(hWnd,msg,title)  ::MessageBox(hWnd, msg, title, MB_ICONWARNING|MB_OK)

#define MSGSUCES(hWnd,msg,title) ::MessageBox(hWnd, msg, title, MB_ICONINFORMATION|MB_OK)