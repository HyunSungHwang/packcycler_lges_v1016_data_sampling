// MyTabCtrl.cpp : implementation file
//
/////////////////////////////////////////////////////
// This class is provided as is and Ben Hill takes no
// responsibility for any loss of any kind in connection
// to this code.
/////////////////////////////////////////////////////
// Is is meant purely as a educational tool and may
// contain bugs.
/////////////////////////////////////////////////////
// ben@shido.fsnet.co.uk
// http://www.shido.fsnet.co.uk
/////////////////////////////////////////////////////
// Thanks to a mystery poster in the C++ forum on 
// www.codeguru.com I can't find your name to say thanks
// for your Control drawing code. If you are that person 
// thank you very much. I have been able to use some of 
// you ideas to produce this sample application.
/////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyTabCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/*
m_TabOption.InsertItem(0, _T("NetWork"));
m_TabOption.m_tabPages=new CDialog*[m_TabOption.GetItemCount()];
	
m_TabOption.m_tabPages[0]=new Dlg_Optnet;
m_TabOption.m_tabPages[0]->Create(IDD_DIALOG_NET, &m_TabOption);

m_TabOption.SetRectangle(0);
*/
/////////////////////////////////////////////////////////////////////////////
// CMyTabCtrl

CMyTabCtrl::CMyTabCtrl()
{
	m_tabPages=NULL;

    m_bInit=FALSE;
}

CMyTabCtrl::~CMyTabCtrl()
{
}

BEGIN_MESSAGE_MAP(CMyTabCtrl, CTabCtrl)
	//{{AFX_MSG_MAP(CMyTabCtrl)
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyTabCtrl message handlers

void CMyTabCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	/*CDC* dc = CDC::FromHandle(lpDrawItemStruct->hDC); 

	UINT state = lpDrawItemStruct->itemState; 
    int bgcolor;

	if (state & ODS_SELECTED) bgcolor = RGB(255,0,0);
    else bgcolor = RGB(0,255,0);

	CRect rect;
	GetClientRect(rect);

	DrawFilledRect(dc,rect,bgcolor);*/
}

void CMyTabCtrl::DrawFilledRect(CDC *DC, CRect R, COLORREF color)
{ 
	CBrush B;
	B.CreateSolidBrush(color);
	DC->FillRect(R, &B);
}
 
void CMyTabCtrl::DrawButtonText(CDC *DC, CRect R, CString strText, COLORREF TextColor)
{
    COLORREF prevColor = DC->SetTextColor(TextColor);
    DC->SetBkMode(TRANSPARENT);
	DC->DrawText(strText, strText.GetLength(), R, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	DC->SetTextColor(prevColor);
}

void CMyTabCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CTabCtrl::OnLButtonDown(nFlags, point);

	if(m_tabPages)
	{
		if(m_tabCurrent != GetCurFocus())
		{
			m_tabPages[m_tabCurrent]->ShowWindow(SW_HIDE);
			m_tabCurrent=GetCurFocus();

			m_tabPages[m_tabCurrent]->ShowWindow(SW_SHOW);
			m_tabPages[m_tabCurrent]->SetFocus();
			if(GetParent()) GetParent()->SendMessage(WM_TABFOCUS,m_tabCurrent,0);
		}
	}
}

void CMyTabCtrl::onSelect(int nItem)
{
	if(!m_tabPages) return;

	m_tabCurrent=nItem;

	for(int i=0;i<GetItemCount();i++) m_tabPages[i]->ShowWindow(SW_HIDE);
	
	SetCurSel(nItem); //focus	
	m_tabPages[m_tabCurrent]->ShowWindow(SW_SHOW);

	if(m_tabCurrent>0 &&GetParent()) 
	{
	   GetParent()->SendMessage(WM_TABFOCUS,m_tabCurrent,0);
	}
}

void CMyTabCtrl::SetRectangle(int nItem)
{
	if(!m_tabPages) return;
		
	m_tabCurrent=nItem;
	
	CRect tabRect, itemRect;
	int nX, nY, nXc, nYc;
	
	GetClientRect(&tabRect);
	GetItemRect(0, &itemRect);
	
	nX=itemRect.left;
	nY=itemRect.bottom+1;
	nXc=tabRect.right-itemRect.left-1;//border
	nYc=tabRect.bottom-nY-1;//border
	
	for(int i=0; i < GetItemCount(); i++)
	{
		m_tabPages[i]->MoveWindow(nX, nY, nXc, nYc,TRUE);
		m_tabPages[i]->ShowWindow(SW_HIDE);
		//m_tabPages[i]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);  		
	}
	onSelect(nItem);
}

void CMyTabCtrl::OnSize(UINT nType, int cx, int cy) 
{
	CRect tabRect, itemRect;
	int nX, nY, nXc, nYc;
	
	GetClientRect(&tabRect);
	GetItemRect(0, &itemRect);
	
	nX=itemRect.left;
	nY=itemRect.bottom+1;
	nXc=tabRect.right-itemRect.left-1;//border
	nYc=tabRect.bottom-nY-1;//border
	
	for(int i=0; i < GetItemCount(); i++)
	{	
		m_tabPages[i]->MoveWindow(nX, nY, nXc, nYc,TRUE);

		if(m_tabCurrent==i)
		{
			m_tabPages[i]->ShowWindow(SW_SHOW);
			//m_tabPages[i]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);  
		}
		else
		{
			m_tabPages[i]->ShowWindow(SW_HIDE);
			//m_tabPages[i]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);  
		}
	}	
}