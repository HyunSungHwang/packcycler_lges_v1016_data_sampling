#define round(a) ( int ) ( a + .5 )

class GMath
{
public:   
	GMath();
    ~GMath();
    		

	/*static int math_RoundToInt(double x, int nIndex) 
	{
		double dValue = x;
		int i = 0, j;
		__int64 nGob = 1, nPosValue;
		while( i < nIndex )
		{
			nGob = nGob * 10;
			i++;
		}

		i = 0;
		while( nIndex > 0 ) 
		{
			j = 0;
			int nValue1 = 1;
			while( j < i )
			{
				nValue1 = nValue1 * 10;
				j++;
			}

			nPosValue = (__int64)( dValue * nGob / nValue1 );

			if( nPosValue % 10 >= 5 )
			{
				j = 0;
				double dValue1 = 1;
				while( j < nIndex - 1 )
				{
					dValue1 = dValue1 / 10;
					j++;
				}
				dValue = dValue + dValue1; 
			}
			nIndex--;
			i++;
		}

		return (int)dValue;
	}

	static CRect math_GetResolutionRect(CRect rc, int nResWidth, int nResHeight, int nWidth, int nHeight )
	{
		CRect rc1;
		rc1.left   = math_RoundToInt( rc.left   * ( (double)nResWidth  / (double)nWidth ), 4 );
		rc1.right  = math_RoundToInt( rc.right  * ( (double)nResWidth  / (double)nWidth ), 4 );
		rc1.top    = math_RoundToInt( rc.top    * ( (double)nResHeight / (double)nHeight ), 4 );
		rc1.bottom = math_RoundToInt( rc.bottom * ( (double)nResHeight / (double)nHeight ), 4 );

		//long TwipsPerPixelX  = 1440 / GetDeviceCaps(hdc, LOGPIXELSX);
        //long TwipsPerPixelY   = 1440 / GetDeviceCaps(hdc,LOGPIXELSY);	
				
		return rc1;
	}

	static BOOL math_IsInArea( CRect rc, CPoint point, int nGap=0)
	{
		CRect rect=CRect(rc.left-nGap,rc.top-nGap,rc.right+nGap,rc.bottom+nGap);
		return rect.PtInRect(point);		
	}
	
	static CString math_FormatSize(DWORD dwSizeLow, DWORD dwSizeHigh=0)
	{
		TCHAR szBuff[100];		

		unsigned __int64 nFileSize = ((unsigned __int64)(((DWORD)(dwSizeLow)) | 
									 ((unsigned __int64)((DWORD)(dwSizeHigh))) << 32));
		unsigned __int64 kb = 1;

		if (nFileSize > 1024)
		{
			kb = nFileSize / 1024;
			if (nFileSize % 1024)
				kb++;
		}

		// make it a string
		_ui64tot(kb, szBuff, 10);

		// add thousand seperators
		int nLength = lstrlen(szBuff);
		if (nLength > 3)
		{
			LPCTSTR ptr = szBuff;
			ptr += (nLength-1);

			TCHAR szTemp[100];			
			LPTSTR ptrTemp = szTemp;
			for(int i=0; i<nLength; i++)
			{
				if (i && ((i % 3) == 0)) 
				{
					if (*ptrTemp != ',')
					{
						*ptrTemp = ',';
						ptrTemp = _tcsinc(ptrTemp);
					}
				}
				*ptrTemp = *ptr;
				ptrTemp = _tcsinc(ptrTemp);
				ptr = _tcsdec(szBuff, ptr);
			}
			// terminate string
			*ptrTemp = '\0';
			// reverse string
			_tcsrev(szTemp);
			lstrcpy(szBuff, szTemp);
		}		

		CString str=szBuff;
		return str+_T(" KB");
	}*/

} ;