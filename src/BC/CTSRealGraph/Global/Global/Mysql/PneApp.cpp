#include "StdAfx.h"
#include "PneApp.h"


PneApp::PneApp(void)
{
}

PneApp::~PneApp(void)
{
}


void PneApp::ReConnectTime()
{
	CTime CurTime=GTime::time_GetTime(GWin::win_CurrentTime(_T("full")));
	int nElpSec=GTime::time_GetElapse(m_StartTime,CurTime);

	if(nElpSec>=3600 || 
	   m_MySql.m_bCon==FALSE ||
	   m_MySql.m_iErr==2003 ||
       m_MySql.m_iErr==2013)
	{//1hour(3600)		
		m_StartTime=CurTime;
		m_MySql.ReConn();
	}
}

void PneApp::ReConnectError()
{
	if(mainPneApp.m_MySql.m_iErr>0)
	{								
		if(mainPneApp.m_MySql.m_iErr==2013)
		{//lost connect							
			m_MySql.ReConn();
			GLog::log_Save("mes","db","reconn",NULL,TRUE);
		}
	}
}

BOOL PneApp::CreateTbMonitor()
{//Src Table Create		
	CString table=_T("monitorinfo");//MyISAM
	if(m_MySql.ChkTable(table)==FALSE)
	{	
CString sql;	   
sql.Format(_T("CREATE TABLE `%s` (          \
`NOMONITOR` int(5) NOT NULL AUTO_INCREMENT, \
`NMWORK` varchar(100) DEFAULT NULL,     \
`TESTSERIAL` varchar(20) DEFAULT NULL,  \
`MODULEID` varchar(30) DEFAULT NULL,    \
`NMMODULE` varchar(20) DEFAULT NULL,    \
`DEVICEID` varchar(10) DEFAULT NULL,    \
`BLOCKID` varchar(10) DEFAULT NULL,     \
`NMSCHED` varchar(20) DEFAULT NULL,     \
`CHANNEL` varchar(10) DEFAULT NULL,     \
`CHNO` int(5) DEFAULT NULL,             \
`CHSTEPNO` int(5) DEFAULT NULL,         \
`CHSTATE` int(5) DEFAULT NULL,          \
`EQSTATE` varchar(3) DEFAULT NULL,      \
`EQSTR` varchar(30) DEFAULT NULL,       \
`CHSTEPTYPE` int(5) DEFAULT NULL,       \
`CHDATASELECT` int(5) DEFAULT NULL,     \
`CHCODE` int(5) DEFAULT NULL,           \
`CHGRADECODE` int(5) DEFAULT NULL,      \
`CHCOMMSTATE` int(5) DEFAULT NULL,      \
`CHOUTPUTSTATE` int(5) DEFAULT NULL,    \
`CHINPUTSTATE` int(5) DEFAULT NULL,     \
`CRESERVED1` float DEFAULT NULL,        \
`CRESERVED2` float DEFAULT NULL,        \
`NINDEXFROM` int(11) DEFAULT NULL,      \
`NINDEXTO` int(11) DEFAULT NULL,        \
`NCURRENTCYCLENUM` int(11) DEFAULT NULL,\
`NTOTALCYCLENUM` int(11) DEFAULT NULL,  \
`LSAVESEQUENCE` int(11) DEFAULT NULL,   \
`NACCCYCLEGROUPNUM1` int(11) DEFAULT NULL,\
`NACCCYCLEGROUPNUM2` int(11) DEFAULT NULL,\
`NACCCYCLEGROUPNUM3` int(11) DEFAULT NULL,\
`NACCCYCLEGROUPNUM4` int(11) DEFAULT NULL,\
`NACCCYCLEGROUPNUM5` int(11) DEFAULT NULL,\
`NMULTICYCLEGROUPNUM1` int(11) DEFAULT NULL,\
`NMULTICYCLEGROUPNUM2` int(11) DEFAULT NULL,\
`NMULTICYCLEGROUPNUM3` int(11) DEFAULT NULL,\
`NMULTICYCLEGROUPNUM4` int(11) DEFAULT NULL,\
`NMULTICYCLEGROUPNUM5` int(11) DEFAULT NULL,"),table);

sql+=_T("`FVOLTAGE` VARCHAR(20) DEFAULT NULL,\
`FCURRENT` float DEFAULT NULL,        \
`FCAPACITY` float DEFAULT NULL,       \
`FCAPACITYSUM` float DEFAULT NULL,    \
`FWATT` float DEFAULT NULL,           \
`FWATTHOUR` float DEFAULT NULL,       \
`FSTEPTIME` float DEFAULT NULL,       \
`FTOTALTIME` float DEFAULT NULL,      \
`FIMPEDANCE` float DEFAULT NULL,      \
`FOVENTEMPERATURE` float DEFAULT NULL,\
`FOVENHUMIDITY` float DEFAULT NULL,   \
`FAVGVOLTAGE` float DEFAULT NULL,     \
`FAVGCURRENT` float DEFAULT NULL,     \
`FCHARGEAH` float DEFAULT NULL,       \
`FDISCHARGEAH` float DEFAULT NULL,    \
`FCAPACITANCE` float DEFAULT NULL,    \
`FCHARGEWH` float DEFAULT NULL,       \
`FDISCHARGEWH` float DEFAULT NULL,    \
`FCVTIME` float DEFAULT NULL,         \
`FSYNCDATE` float DEFAULT NULL,       \
`FSYNCTIME` float DEFAULT NULL,       \
`FVOLTAGEINPUT` float DEFAULT NULL,   \
`FVOLTAGEPOWER` float DEFAULT NULL,   \
`FVOLTAGEBUS` float DEFAULT NULL,     \
`FMETER` float DEFAULT NULL,          \
`STARTTIME` varchar(20) DEFAULT NULL, \
`ENDTIME` varchar(20) DEFAULT NULL,   \
`ISPARALLEL` VARCHAR(1) DEFAULT NULL, \
`BMASTER` varchar(1) DEFAULT NULL,    \
`DTREG` datetime DEFAULT NULL,        \
PRIMARY KEY (`NOMONITOR`)             \
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	   return m_MySql.SetQuery(sql);
	}
	return TRUE;
}

BOOL PneApp::SetStateInsert(CTypedPtrList<CPtrList, CHINFO*> &ChList)
{
	int ncount=ChList.GetCount();
	if(ncount<1) return TRUE;

	CHINFO *chinfo=NULL;
	POSITION pos =  ChList.GetHeadPosition();

	for(int i=0;i<ncount;i++)
	{
		chinfo=(CHINFO*)ChList.GetNext(pos);
		if(!chinfo) continue;
		
		if(SetMonitoring(chinfo)==FALSE) return FALSE;
		Sleep(100);
	}

	return TRUE;
}

BOOL PneApp::SetMonitoring(CHINFO *chinfo)
{
	if(!chinfo) return FALSE;

	CString szDeviceid;
	CString szBlockid;
	CString szModuleid;
	CString szChannelIndex;
    
	CString szNmwork;
	CString szNmmodule;
	CString szTestSerial;
	CString szEqstate;
	CString szEqstr;
	CString szNmsched;

	CString szChNo;
	CString szStepNo;
	CString szState;
	CString szStepType;
	CString szChDataSelect;
	CString szChCode;
	CString szChGradeCode;
	CString szChCommState;

	CString szChOutputState;
	CString szChInputState;	
	CString szCReserved1;
	CString szCReserved2;
	
	CString szNIndexFrom;
	CString szNIndexTo;
	CString szNCurrentCycleNum;
	CString szNTotalCycleNum;
	CString szLSaveSequence;			
	CString szNAccCycleGroupNum1;	
	CString szNAccCycleGroupNum2;
	CString szNAccCycleGroupNum3;
	CString szNAccCycleGroupNum4;
	CString szNAccCycleGroupNum5;

	CString szNMultiCycleGroupNum1;
	CString szNMultiCycleGroupNum2;
	CString szNMultiCycleGroupNum3;
	CString szNMultiCycleGroupNum4;
	CString szNMultiCycleGroupNum5;

	CString szFVoltage;		
	CString szFCurrent;
	CString szFCapacity;
	CString szFCapacitySum;
	CString szFWatt;
	CString szFWattHour;
	CString szFStepTime;			
	CString szFTotalTime;			
	CString szFImpedance;			
	CString szFOvenTemperature;		
	CString szFOvenHumidity;			
	CString szFAvgVoltage;
	CString szFAvgCurrent;
	CString szFChargeAh;
	CString szFDisChargeAh;
	CString szFCapacitance;
	CString szFChargeWh;
	CString szFDisChargeWh;
	CString szFCVTime;
	CString szFSyncDate;
	CString szFSyncTime;
	CString szFVoltageInput;			
	CString szFVoltagePower;			
	CString szfVoltageBus;	
	CString szFMeter;
	CString startTime;
	CString endTime;
	CString szIsParallel;
	CString szBMaster;

	szDeviceid.Format(_T("%010s"),g_AppInfo.strDeviceid);
	szBlockid.Format(_T("%010s"),g_AppInfo.strBlockid);

	szNmwork=chinfo->nmwork;
	szModuleid.Format(_T("%d"),chinfo->nModuleID);
	szChannelIndex.Format(_T("%d"),chinfo->nChannelIndex);
	szNmmodule=chinfo->nmmodule;
	szTestSerial=chinfo->testserial;//nmcondition
    szEqstate=chinfo->Eqstate;
	szEqstr=chinfo->Eqstr;
	szNmsched=chinfo->nmsched;

	szChNo.Format(_T("%d"),chinfo->pRecord.chNo);
	szStepNo.Format(_T("%d"),chinfo->pRecord.chStepNo);
	szState.Format(_T("%d"),chinfo->pRecord.chState);
	szStepType.Format(_T("%d"),chinfo->pRecord.chStepType);	
	szChDataSelect.Format(_T("%d"),chinfo->pRecord.chDataSelect);
	szChCode.Format(_T("%d"),chinfo->pRecord.chCode);
    szChGradeCode.Format(_T("%d"),chinfo->pRecord.chGradeCode);
	
	szChCommState.Format(_T("%d"),chinfo->pRecord.chCommState);
	szChOutputState.Format(_T("%d"),chinfo->pRecord.chOutputState);
	szChInputState.Format(_T("%d"),chinfo->pRecord.chInputState);
	szCReserved1.Format(_T("%d"),chinfo->pRecord.cReserved1);
	szCReserved2.Format(_T("%d"),chinfo->pRecord.cReserved2);

	szNIndexFrom.Format(_T("%d"),chinfo->pRecord.nIndexFrom);
	szNIndexTo.Format(_T("%d"),chinfo->pRecord.nIndexTo);
	szNCurrentCycleNum.Format(_T("%d"),chinfo->pRecord.nCurrentCycleNum);
	szNTotalCycleNum.Format(_T("%d"),chinfo->pRecord.nTotalCycleNum);
	szLSaveSequence.Format(_T("%d"),chinfo->pRecord.lSaveSequence);
	szNAccCycleGroupNum1.Format(_T("%d"),chinfo->pRecord.nAccCycleGroupNum1);
	szNAccCycleGroupNum2.Format(_T("%d"),chinfo->pRecord.nAccCycleGroupNum2);
	szNAccCycleGroupNum3.Format(_T("%d"),chinfo->pRecord.nAccCycleGroupNum3);
	szNAccCycleGroupNum4.Format(_T("%d"),chinfo->pRecord.nAccCycleGroupNum4);
	szNAccCycleGroupNum5.Format(_T("%d"),chinfo->pRecord.nAccCycleGroupNum5);
	szNMultiCycleGroupNum1.Format(_T("%d"),chinfo->pRecord.nMultiCycleGroupNum1);
	szNMultiCycleGroupNum2.Format(_T("%d"),chinfo->pRecord.nMultiCycleGroupNum2);
	szNMultiCycleGroupNum3.Format(_T("%d"),chinfo->pRecord.nMultiCycleGroupNum3);
	szNMultiCycleGroupNum4.Format(_T("%d"),chinfo->pRecord.nMultiCycleGroupNum4);
	szNMultiCycleGroupNum5.Format(_T("%d"),chinfo->pRecord.nMultiCycleGroupNum5);

	szFVoltage.Format(_T("%f"),chinfo->pRecord.fVoltage);	
	szFCurrent.Format(_T("%f"),chinfo->pRecord.fCurrent);
	szFCapacity.Format(_T("%f"),chinfo->pRecord.fCapacity);
	szFCapacitySum.Format(_T("%f"),chinfo->fCapacitySum);
	
	szFWatt.Format(_T("%f"),chinfo->pRecord.fWatt);	
	szFWattHour.Format(_T("%f"),chinfo->pRecord.fWattHour);
	szFStepTime.Format(_T("%f"),chinfo->pRecord.fStepTime);
	szFTotalTime.Format(_T("%f"),chinfo->pRecord.fTotalTime);
	szFImpedance.Format(_T("%f"),chinfo->pRecord.fImpedance);

	szFOvenTemperature.Format(_T("%f"),chinfo->pRecord.fOvenTemperature);
	szFOvenHumidity.Format(_T("%f"),chinfo->pRecord.fOvenHumidity);
	szFAvgVoltage.Format(_T("%f"),chinfo->pRecord.fAvgVoltage);
	szFAvgCurrent.Format(_T("%f"),chinfo->pRecord.fAvgCurrent);

	szFChargeAh.Format(_T("%f"),chinfo->pRecord.fChargeAh);
	szFDisChargeAh.Format(_T("%f"),chinfo->pRecord.fDisChargeAh);
	szFCapacitance.Format(_T("%f"),chinfo->pRecord.fCapacitance);
	szFChargeWh.Format(_T("%f"),chinfo->pRecord.fChargeWh);
	szFDisChargeWh.Format(_T("%f"),chinfo->pRecord.fDisChargeWh);
	szFCVTime.Format(_T("%f"),chinfo->pRecord.fCVTime);
	szFSyncDate.Format(_T("%f"),chinfo->pRecord.fSyncDate);
	szFSyncTime.Format(_T("%f"),chinfo->pRecord.fSyncTime);
	szFVoltageInput.Format(_T("%f"),chinfo->pRecord.fVoltageInput);
	szFVoltagePower.Format(_T("%f"),chinfo->pRecord.fVoltagePower);
	szfVoltageBus.Format(_T("%f"),chinfo->pRecord.fVoltageBus);
	szFMeter.Format(_T("%f"),chinfo->fMeter);
	startTime=chinfo->starttime;
	endTime=chinfo->endtime;
	szIsParallel.Format(_T("%d"),chinfo->isParallel);
	szBMaster.Format(_T("%d"),chinfo->bMaster);

	CString nomonitor=chinfo->nomonitor;
	if(nomonitor.IsEmpty() || nomonitor==_T(""))
	{
		nomonitor=m_MySql.GetData4(_T("MONITORINFO"),_T("DEVICEID"),szDeviceid,
													 _T("BLOCKID"),szBlockid,
													 _T("MODULEID"),szModuleid,
													 _T("CHNO"),szChNo,
													 _T("NOMONITOR"));
		chinfo->nomonitor=nomonitor;
	}

    CString sql;
	if(nomonitor.IsEmpty() || nomonitor==_T(""))
	{		
sql.Format(_T("INSERT INTO MONITORINFO\
(NMWORK,DEVICEID,BLOCKID,MODULEID,NMMODULE,CHANNEL,\
TESTSERIAL,EQSTATE,EQSTR,NMSCHED,\
CHNO,CHSTEPNO,CHSTATE,CHSTEPTYPE,CHDATASELECT,CHCODE,CHGRADECODE,\
CHCOMMSTATE,CHOUTPUTSTATE,CHINPUTSTATE,CRESERVED1,CRESERVED2,\
NINDEXFROM,NINDEXTO,NCURRENTCYCLENUM,NTOTALCYCLENUM,LSAVESEQUENCE,\
NACCCYCLEGROUPNUM1,NACCCYCLEGROUPNUM2,NACCCYCLEGROUPNUM3,\
NACCCYCLEGROUPNUM4,NACCCYCLEGROUPNUM5,\
NMULTICYCLEGROUPNUM1,NMULTICYCLEGROUPNUM2,NMULTICYCLEGROUPNUM3,\
NMULTICYCLEGROUPNUM4,NMULTICYCLEGROUPNUM5,\
FVOLTAGE,FCURRENT,FCAPACITY,FCAPACITYSUM,FWATT,FWATTHOUR,FSTEPTIME,FTOTALTIME,\
FIMPEDANCE,FOVENTEMPERATURE,FOVENHUMIDITY,FAVGVOLTAGE,FAVGCURRENT,\
FCHARGEAH,FDISCHARGEAH,FCAPACITANCE,FCHARGEWH,FDISCHARGEWH,\
FCVTIME,FSYNCDATE,FSYNCTIME,FVOLTAGEINPUT,FVOLTAGEPOWER,FVOLTAGEBUS,\
FMETER,STARTTIME,ENDTIME,ISPARALLEL,BMASTER, \
DTREG)                                 \
VALUES ('%s','%s','%s','%s','%s','%s', \
'%s','%s','%s','%s',                   \
'%s','%s','%s','%s','%s','%s','%s',    \
'%s','%s','%s','%s','%s',              \
'%s','%s','%s','%s','%s',              \
'%s','%s','%s',                        \
'%s','%s',                             \
'%s','%s','%s',                        \
'%s','%s',                             \
'%s','%s', '%s','%s','%s','%s','%s','%s',\
'%s','%s','%s','%s','%s',                \
'%s','%s','%s','%s','%s',                \
'%s','%s','%s','%s','%s','%s',           \
'%s','%s','%s','%s','%s',                \
NOW())"),
szNmwork,szDeviceid,szBlockid,szModuleid,szNmmodule,szChannelIndex,
szTestSerial,szEqstate,szEqstr,szNmsched,
szChNo,szStepNo,szState,szStepType,szChDataSelect,szChCode,szChGradeCode,
szChCommState,szChOutputState,szChInputState,szCReserved1,szCReserved2,
szNIndexFrom,szNIndexTo,szNCurrentCycleNum,szNTotalCycleNum,szLSaveSequence,
szNAccCycleGroupNum1,szNAccCycleGroupNum2,szNAccCycleGroupNum3,
szNAccCycleGroupNum4,szNAccCycleGroupNum5,
szNMultiCycleGroupNum1,szNMultiCycleGroupNum2,szNMultiCycleGroupNum3,
szNMultiCycleGroupNum4,szNMultiCycleGroupNum5,
szFVoltage,szFCurrent,szFCapacity,szFCapacitySum,szFWatt,szFWattHour,szFStepTime,szFTotalTime,
szFImpedance,szFOvenTemperature,szFOvenHumidity,szFAvgVoltage,szFAvgCurrent,
szFChargeAh,szFDisChargeAh,szFCapacitance,szFChargeWh,szFDisChargeWh,
szFCVTime,szFSyncDate,szFSyncTime,szFVoltageInput,szFVoltagePower,szfVoltageBus,
szFMeter,startTime,endTime,szIsParallel,szBMaster
);	
	}
	else
	{
sql.Format(_T("UPDATE MONITORINFO SET  \
NMWORK='%s',            \
TESTSERIAL='%s',        \
EQSTATE='%s',           \
EQSTR='%s',             \
NMSCHED='%s',           \
CHANNEL='%s',           \
CHSTEPNO='%s',          \
CHSTATE='%s',           \
CHSTEPTYPE='%s',        \
CHDATASELECT='%s',      \
CHCODE='%s',            \
CHGRADECODE='%s',       \
CHCOMMSTATE='%s',       \
CHOUTPUTSTATE='%s',     \
CHINPUTSTATE='%s',      \
CRESERVED1='%s',        \
CRESERVED2='%s',        \
NINDEXFROM='%s',        \
NINDEXTO='%s',          \
NCURRENTCYCLENUM='%s',  \
NTOTALCYCLENUM='%s',    \
LSAVESEQUENCE='%s',     \
NACCCYCLEGROUPNUM1='%s',   \
NACCCYCLEGROUPNUM2='%s',   \
NACCCYCLEGROUPNUM3='%s',   \
NACCCYCLEGROUPNUM4='%s',   \
NACCCYCLEGROUPNUM5='%s',   \
NMULTICYCLEGROUPNUM1='%s', \
NMULTICYCLEGROUPNUM2='%s', \
NMULTICYCLEGROUPNUM3='%s', \
NMULTICYCLEGROUPNUM4='%s', \
NMULTICYCLEGROUPNUM5='%s', \
FVOLTAGE='%s',          \
FCURRENT='%s',          \
FCAPACITY='%s',         \
FCAPACITYSUM='%s',      \
FWATT='%s',             \
FWATTHOUR='%s',         \
FSTEPTIME='%s',         \
FTOTALTIME='%s',        \
FIMPEDANCE='%s',        \
FOVENTEMPERATURE='%s',  \
FOVENHUMIDITY='%s',     \
FAVGVOLTAGE='%s',       \
FAVGCURRENT='%s',       \
FCHARGEAH='%s',         \
FDISCHARGEAH='%s',      \
FCAPACITANCE='%s',      \
FCHARGEWH='%s',         \
FDISCHARGEWH='%s',      \
FCVTIME='%s',           \
FSYNCDATE='%s',         \
FSYNCTIME='%s',         \
FVOLTAGEINPUT='%s',     \
FVOLTAGEPOWER='%s',     \
FVOLTAGEBUS='%s',       \
FMETER='%s',            \
STARTTIME='%s',         \
ENDTIME='%s',           \
ISPARALLEL='%s',        \
BMASTER='%s',           \
DTREG=NOW()             \
WHERE NOMONITOR='%s' "),
szNmwork,szTestSerial,szEqstate,szEqstr,szNmsched,szChannelIndex,
szStepNo,szState,szStepType,szChDataSelect,szChCode,szChGradeCode,
szChCommState,szChOutputState,szChInputState,szCReserved1,szCReserved2,
szNIndexFrom,szNIndexTo,szNCurrentCycleNum,szNTotalCycleNum,szLSaveSequence,
szNAccCycleGroupNum1,szNAccCycleGroupNum2,szNAccCycleGroupNum3,
szNAccCycleGroupNum4,szNAccCycleGroupNum5,
szNMultiCycleGroupNum1,szNMultiCycleGroupNum2,szNMultiCycleGroupNum3,
szNMultiCycleGroupNum4,szNMultiCycleGroupNum5,
szFVoltage,szFCurrent,szFCapacity,szFCapacitySum,szFWatt,szFWattHour,szFStepTime,szFTotalTime,
szFImpedance,szFOvenTemperature,szFOvenHumidity,szFAvgVoltage,szFAvgCurrent,
szFChargeAh,szFDisChargeAh,szFCapacitance,szFChargeWh,szFDisChargeWh,
szFCVTime,szFSyncDate,szFSyncTime,szFVoltageInput,szFVoltagePower,szfVoltageBus,
szFMeter,startTime,endTime,szIsParallel,szBMaster,
nomonitor);		
	}

	GLog::log_Save("mysql","sql",sql,NULL,TRUE);
		
	BOOL bRet=m_MySql.SetQuery(sql);
	return bRet;

}


