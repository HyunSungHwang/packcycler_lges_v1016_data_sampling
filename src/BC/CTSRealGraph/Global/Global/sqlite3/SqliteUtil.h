#pragma once

#pragma comment(lib, "./Global/sqlite3/sqlite3.lib")
#pragma comment(lib, "./Global/sqlite3/w32SQLite3.lib")

#include "DbSqlite.h"

typedef struct {	
	
	CString chno;
	CString chstepno;
	CString chstate;
	CString chsteptype;
	CString chsteptypestr;
	CString lstep_seq;
	CString chdataselect;
	CString chcode;
	CString chgradecode;
	CString chreserved;

	CString start_time;
	CString end_time;
	
	CString nindexfrom;
	CString nindexto;
	CString ncurrentcyclenum;
	CString ntotalcyclenum;
	CString lsavesequence;
	CString lreserved;
	CString ngotocyclenum;

	CString fvoltage;
	CString fcurrent;
	CString fcharge_amparehour;
	CString fdischarge_amparehour;
	CString fwatt;
	CString fcharge_watthour;
	CString fdischarge_watthour;
	CString fsteptime_day;
	CString fsteptime;

	CString ftotaltime_day;
	CString ftotaltime;
	CString fimpedance;
	CString ftemparature;
	CString ftemparature2;
	CString ftemparature3;
	CString foventemparature;

	CString chusingchamber;
	CString crecordtimeno;
	CString reserved1;
	CString reserved2;

	CString fpressure;
	CString favgvoltage;
	CString favgcurrent;
	CString frealdate;
	CString frealclock;
	CString fdebug1;
	CString fdebug2;

	CString ftemparature_min;
	CString ftemparature_max;
	CString ftemparature_avg;
	CString ftemparature2_min;
	CString ftemparature2_max;
	CString ftemparature2_avg;
    CString ftemparature3_min;
	CString ftemparature3_max;
	CString ftemparature3_avg;

	CString fstep_cc_time_day;
	CString fstep_cc_time;
	CString fstep_cv_time_day;
	CString fstep_cv_time;
	CString fcharge_cc_amparehour;
	CString fcharge_cv_amparehour;
	CString fdischarge_cc_amparehour;
	CString fdischarge_cv_amparehour;
	CString fstartocv;
	
} PS_DATA_END;//config

class SqliteUtil
{
public:
    
	CDbSQLite m_sqlite;	
	CString   m_szDbFile;
	BOOL      m_bOpen;
	BOOL      m_bErrMesg;

	void log_save(CString sType,CString sLog);

	BOOL slite_open(CString AppPath,CString sfile,BOOL bCreate=FALSE);
	void slite_close();
	BOOL slite_tableExist(CString stable);

    BOOL SetExecute(CString sql);
	CSqlStatement* GetExecute(CString sql);
	BOOL SetEditData1(CString table,CString n1,CString v1,CString n2,CString v2);

	CString GetData1(CString table,CString n1,CString v1,CString f);
	CString GetData2(CString table,CString n1,CString v1,CString n2,CString v2,CString f);
	CString GetData3(CString table,CString n1,CString v1,CString n2,CString v2,CString n3,CString v3,CString f);

	BOOL SetDelete(CString table);
	BOOL SetDelete1(CString table,CString n1,CString v1);

	BOOL CreateTbWorkinfo();
	BOOL CreateTbWorkevent();
	BOOL CreateTbWorkstate();
	BOOL CreateTbWorkstep();

	BOOL InsertWorkinfo(CString deviceid,CString blockid,CString lotid,CString cellid,
		  			    CString channel, CString moduleid, CString channelidx,
						CString nmmodule,CString nowdt,CString nmwork,
						CString worker,CString ip,CString schednm,CString schedfile,int stat);
	
	BOOL InsertWorkevent(CString deviceid,CString blockid,CString lotid,CString cellid,
							 CString channel, CString moduleid, CString channelidx,
							 CString sampleno,CString nmwork,CString tpstate,int stat);

	BOOL InsertWorkstate(CString deviceid,CString blockid,CString lotid,CString cellid,
							 CString channel, CString moduleid, CString channelidx,
							 CString tpstate,int stat);

	BOOL InsertWorkStepEnd(CString deviceid,CString blockid,CString lotid,CString cellid,
							 CString channel, CString moduleid, CString channelidx,CString sampleno,
							 CString nmwork,CString cycleno,CString stepno,PS_DATA_END ps_DataEnd,int stat);


public:
	SqliteUtil(void);
	virtual ~SqliteUtil(void);

};
