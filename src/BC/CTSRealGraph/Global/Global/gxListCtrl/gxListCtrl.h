#if !defined(AFX_LISTCTRL_H__2EB671B4_0711_11D3_90AB_00E029355177__INCLUDED_)
#define AFX_LISTCTRL_H__2EB671B4_0711_11D3_90AB_00E029355177__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListCtrl.h : header file
//
#define UM_LISTTEXT		WM_USER+403

typedef struct 
{
	int   nCol;
    BOOL  bSortAscending;

	BOOL  bEditMode;
	int   nMode;//1(CEdit),2(CDateTimeCtrl)
	CString sMode;//"HH:mm","HH:mm:ss";
	DWORD nStyle;

	CListCtrl *listCtrl;

} LISTCOLINFO;

typedef struct 
{
	BOOL		  bDragging;

	int			  nDragIndex;
	CListCtrl*    pDragList;
	CImageList*   pDragImage;	

	CListCtrl*    pDropList;		
	int			  nDropIndex;	
	CWnd*		  pDropWnd;
} DRAGDROPINFO;

/////////////////////////////////////////////////////////////////////////////
// gxListCtrl window

class gxListCtrl : public CListCtrl
{
public:
	gxListCtrl (CString Text = _T("Some Text"));
    virtual ~gxListCtrl();

	LISTCOLINFO  *m_ListColInfo;
	
	CEdit		  *m_pEdit;
	CDateTimeCtrl *m_pPicker;
	CComboBox     *m_pCombo;

	CPoint       m_nPoint,m_vPoint;
    CString      DefaultText;
	BOOL         m_bSortAscending;
	BOOL         m_bAllCheck,m_bListing,m_bFileMode;
	int          m_iLeft;

	COLORREF     m_iOddColor,m_iEvenColor;
	COLORREF     m_iFocusBkColor,m_iFocusText;

	void    SetInit(int iLeft,BOOL bFileMode=FALSE);
	BOOL    ChkEditMode (int Column);
	void    SetEditMode (int nItem, int nMode, CString sMode=_T(""),DWORD nStyle=0);
    BOOL    EditSubItem (int Item, int Column,CWnd *wnd=NULL);
	void    SetEditLabel(CPoint point,BOOL bDbclick=FALSE) ;
	
	void    onCreateEdit(int nItem,int nCol);
	void    onCreatePicker(int nItem,int nCol);
	void    onCreateCombo(int nItem,int nCol);

    int	    HitTestEx (CPoint& Point, int* Column);
    int	    InsertItemEx (int Item);
    void    Resize (int cx, int cy);    
	BOOL    GetCellRect(int iRow, int iCol, int nArea, CRect&rect);
	int     GetColumnCount();
	BOOL    hexNumberToInt (CString HexNumber, int& Number);
	void    SetSelectDelete();

	//FILEINFO
	void       SetFileGetDispInfo(NMHDR *pNMHDR);	
	BOOL       IsDirectory(int nItem);
	CPtrArray* GetSelectFileInfoList();
	FILEINFO*  GetSelectFileInfo();

	void       SetListSort(int iType,int nCol,int iSort=-1);
	void       SetSort();

	//dragbegin
	void onSetDragInit(DRAGDROPINFO *DragdropInfo);
	BOOL onSetDragBegin(DRAGDROPINFO *DragdropInfo,LPNMLISTVIEW pNMLV);
	void onSetDragMouseMove(DRAGDROPINFO *DragdropInfo,CPoint point);
	BOOL onSetDragUp(DRAGDROPINFO *DragdropInfo,CPoint point);
	CImageList* onSetDragCreateImageEx( LPPOINT lpPoint );

	static int CALLBACK SetCompareFunc(LPARAM lParam1, LPARAM lParam2,LPARAM lParamSort);
	static int CALLBACK SetCompareFileInfoFunc(LPARAM lParam1, LPARAM lParam2,LPARAM lParamSort);

    //{{AFX_VIRTUAL(gxListCtrl)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

protected:
    //{{AFX_MSG(gxListCtrl)
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint Point);
	afx_msg void OnCustomdraw( NMHDR* pNMHDR, LRESULT* pResult );
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint Point);	
	afx_msg void OnMouseMove(UINT nFlags, CPoint Point);
	//}}AFX_MSG

    DECLARE_MESSAGE_MAP()
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTCTRL_H__2EB671B4_0711_11D3_90AB_00E029355177__INCLUDED_)
