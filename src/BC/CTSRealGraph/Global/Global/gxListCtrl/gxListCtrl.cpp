// ListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "gxListCtrl.h"

#include "gxEditCell.h"
#include "gxPickerCell.h"
#include "gxComboCell.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IDC_EDITCELL 1001

/////////////////////////////////////////////////////////////////////////////
// gxListCtrl

gxListCtrl::gxListCtrl (CString Text /* = "Some Text" */)
{
    DefaultText = Text;
	m_ListColInfo=NULL;
	m_bSortAscending=TRUE;	
	m_bAllCheck=FALSE;
	m_bListing=FALSE;

    m_pEdit=NULL;
	m_pPicker=NULL;
	m_pCombo=NULL;

	m_bFileMode=FALSE;
	m_iLeft=0;

	m_iOddColor=RGB(23,6,181),//Ȧ��
    m_iEvenColor= RGB(0,0,0);//¦��
    m_iFocusBkColor=RGB(210,245,245);//focus
    m_iFocusText=RGB(255,0,0);//focus	
}

gxListCtrl::~gxListCtrl()
{
}

BEGIN_MESSAGE_MAP(gxListCtrl, CListCtrl)
    //{{AFX_MSG_MAP(gxListCtrl)
    ON_WM_HSCROLL()
    ON_WM_VSCROLL()
    ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnEndLabelEdit)
	ON_NOTIFY_REFLECT ( NM_CUSTOMDRAW, OnCustomdraw )
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// gxListCtrl message handlers

void gxListCtrl::SetInit(int iLeft,BOOL bFileMode)
{
	m_iLeft=iLeft;
	m_bFileMode=bFileMode;
}


void gxListCtrl::Resize (int cx, int cy)
{
    CRect Rect (0, 0, cx, cy);
    MoveWindow (&Rect);
    InvalidateRect (Rect);
    SetColumnWidth (2, LVSCW_AUTOSIZE_USEHEADER); 
}

BOOL gxListCtrl::EditSubItem (int Item, int Column,CWnd *wnd)
{	
	if(!m_ListColInfo)                         return FALSE;
	if(Column>=GetColumnCount())               return FALSE;
	if(m_ListColInfo[Column].bEditMode==FALSE) return FALSE;
    // The returned pointer should not be saved
	
    // Make sure that the item is visible
    if (!EnsureVisible (Item, TRUE)) 
	{
		//InsertItemEx (Item);
		if (!EnsureVisible (Item, TRUE)) return FALSE;
    }

    // Make sure that nCol is valid
    CHeaderCtrl* pHeader = (CHeaderCtrl*) GetDlgItem(0);
    int nColumnCount = pHeader->GetItemCount();
    if (Column >= nColumnCount || GetColumnWidth (Column) < 5) return FALSE;

    // Get the column offset
    int Offset = 0;
    for (int iColumn = 0; iColumn < Column; iColumn++){
		Offset += GetColumnWidth (iColumn);
	}

    CRect Rect;
    GetItemRect (Item, &Rect, LVIR_BOUNDS);

    // Now scroll if we need to expose the column
    CRect ClientRect;
    GetClientRect (&ClientRect);
    if (Offset + Rect.left < 0 || Offset + Rect.left > ClientRect.right)
	{
		CSize Size;
		if (Offset + Rect.left > 0)	Size.cx = -(Offset - Rect.left);
		else  Size.cx = Offset - Rect.left;
		Size.cy = 0;
		Scroll (Size);
		Rect.left -= Size.cx;
    }

    // Get Column alignment
    LV_COLUMN lvCol;
    lvCol.mask = LVCF_FMT;
    GetColumn (Column, &lvCol);
    DWORD dwStyle;
    if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)       dwStyle = ES_LEFT;
    else if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT) dwStyle = ES_RIGHT;
    else dwStyle = ES_CENTER;

    Rect.left += Offset+4;
    Rect.right = Rect.left + GetColumnWidth (Column) - 3;
    if (Rect.right > ClientRect.right)	Rect.right = ClientRect.right;
	
    dwStyle |= WS_BORDER | WS_CHILD | WS_VISIBLE;
	if(m_ListColInfo) dwStyle |= m_ListColInfo[Column].nStyle;//ES_NUMBER
   	
	Rect.left+=m_iLeft;

	if(wnd)
	{				
		wnd->ModifyStyle(0,dwStyle);
		Rect.bottom+=3;
		wnd->MoveWindow(Rect,TRUE);		
	}	
	return TRUE;
}

int gxListCtrl::HitTestEx (CPoint& Point, int* pColumn)
{
    int ColumnNum = 0;
    int Row = HitTest (Point, NULL);
    
    if (pColumn) *pColumn = 0;

    // Make sure that the ListView is in LVS_REPORT
    if ((GetWindowLong (m_hWnd, GWL_STYLE) & LVS_TYPEMASK) != LVS_REPORT){
	  return Row;
	}

    // Get the top and bottom row visible
    Row = GetTopIndex();
    int Bottom = Row + GetCountPerPage();
    if (Bottom > GetItemCount())   Bottom = GetItemCount();
    
    // Get the number of columns
    CHeaderCtrl* pHeader = (CHeaderCtrl*) GetDlgItem(0);
    int nColumnCount = pHeader->GetItemCount();

    // Loop through the visible rows
    for(; Row <= Bottom; Row++){
		// Get bounding rect of item and check whether point falls in it.
		CRect Rect;
		GetItemRect (Row, &Rect, LVIR_BOUNDS);
		if (Rect.PtInRect (Point)){
			// Now find the column
			for (ColumnNum = 0; ColumnNum < nColumnCount; ColumnNum++){
				int ColWidth = GetColumnWidth (ColumnNum);
				if (Point.x >= Rect.left && Point.x <= (Rect.left + ColWidth)){
					if (pColumn) *pColumn = ColumnNum;
					return Row;
				}
				Rect.left += ColWidth;
			}
		}
    }
    return -1;
}

BOOL gxListCtrl::hexNumberToInt (CString HexNumber, int& Number)
{	
    char* pStopString;

	#ifdef _UNICODE			   						
		CStringA strA(HexNumber);    
		Number = strtoul (strA, &pStopString, 16);
	#else		  
		Number = strtoul (HexNumber, &pStopString, 16);
	#endif	

    return Number != ULONG_MAX;

} // hexNumberToInt

BOOL gxListCtrl::ChkEditMode (int Column)
{
	int nCount=GetColumnCount();
	if(nCount<1) return FALSE;
	if(Column>=nCount) return FALSE;
	if(!m_ListColInfo) return FALSE;
	
	return m_ListColInfo[Column].bEditMode;	
}


void gxListCtrl::SetEditMode (int nItem, int nMode,CString sMode,DWORD nStyle)
{//ES_NUMBER, ES_PASSWORD
	int nCount=GetColumnCount();

    if(!m_ListColInfo && nCount>0)
	{
		m_ListColInfo=new LISTCOLINFO[nCount];

		for(int i=0;i<nCount;i++)
		{
			m_ListColInfo[i].bEditMode=FALSE;			
			m_ListColInfo[i].nMode=0;
			m_ListColInfo[i].sMode=_T("");
			m_ListColInfo[i].nStyle=0;
		}
	}

	if(nItem>=nCount)  return;
	if(!m_ListColInfo) return;
	
	m_ListColInfo[nItem].bEditMode=TRUE;	
	m_ListColInfo[nItem].nMode=nMode;	
	m_ListColInfo[nItem].sMode=sMode;;
	m_ListColInfo[nItem].nStyle=nStyle;		
}

int gxListCtrl::InsertItemEx (int Item)
{
	DefaultText=" ";
    int Result = InsertItem (Item + 1, DefaultText);
    CString ItemVal, Temp;

    if (Item == 0)	ItemVal = _T(" ");
    else{
		int HexVal;
		Temp = GetItemText (Item - 1, 1);
		hexNumberToInt (Temp, HexVal);
		ItemVal.Format (_T("%x"), HexVal + 1);
	}

    SetItemText (Item, 1, ItemVal);
    SetColumnWidth (2, LVSCW_AUTOSIZE_USEHEADER);

    return Result;
}

void gxListCtrl::OnHScroll (UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
    if (GetFocus() != this) SetFocus();
    
    CListCtrl::OnHScroll (nSBCode, nPos, pScrollBar);
}

void gxListCtrl::OnVScroll (UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
    if (GetFocus() != this) SetFocus();
    
    CListCtrl::OnVScroll (nSBCode, nPos, pScrollBar);
}

void gxListCtrl::OnEndLabelEdit (NMHDR* pNMHDR, LRESULT* pResult) 
{
    LV_DISPINFO *plvDispInfo = (LV_DISPINFO *)pNMHDR;
    LV_ITEM	*plvItem = &plvDispInfo->item;

    if (plvItem->pszText != NULL)
	{
		CString strText= plvItem->pszText;
		SetItemText (plvItem->iItem, plvItem->iSubItem, strText);		
    }
    *pResult = FALSE;
}

void gxListCtrl::OnLButtonDown (UINT nFlags, CPoint Point) 
{
	m_nPoint=Point;
    CListCtrl::OnLButtonDown (nFlags, Point);   
}

void gxListCtrl::OnMouseMove(UINT nFlags, CPoint Point) 
{
	m_vPoint=Point;
	CListCtrl::OnMouseMove(nFlags, Point);
}

void gxListCtrl::OnLButtonDblClk(UINT nFlags, CPoint Point)
{
	m_nPoint=Point;
	CListCtrl::OnLButtonDblClk(nFlags, Point);	

	if(m_bFileMode==FALSE)	SetEditLabel(Point,FALSE);
}

void gxListCtrl::SetEditLabel(CPoint point,BOOL bDbclick) 
{	
    int ColNum;

	int Index = HitTestEx (point, &ColNum);
	if(Index<0) return;
	
	if(m_bFileMode==TRUE)
	{
		if(bDbclick==TRUE && IsDirectory(Index)==TRUE) return;		
	}
	
	if(m_ListColInfo)
	{
		if (GetWindowLong (m_hWnd, GWL_STYLE) & LVS_EDITLABELS)
		{		
			if(m_ListColInfo[ColNum].nMode==1)
			{//edit box
				onCreateEdit(Index, ColNum);			
			}
			else if(m_ListColInfo[ColNum].nMode==2)
			{//Datatime Picker
				onCreatePicker(Index, ColNum);			
			}
			else if(m_ListColInfo[ColNum].nMode==3)
			{//Combo			
				onCreateCombo(Index, ColNum);
			}		
		}    
	}
	////////////////////////////////////////////////////////////
}

void gxListCtrl::onCreateEdit(int nItem,int nCol)
{	
	m_pEdit= new gxEditCell (this, nItem, nCol, GetItemText (nItem, nCol));

	if(m_pEdit)
	{	
		m_pEdit->Create(WS_CHILD|ES_AUTOHSCROLL,CRect(0,0,0,0),this,9999);	
		EditSubItem(nItem, nCol, m_pEdit);			
	}
}

void gxListCtrl::onCreatePicker(int nItem,int nCol)
{	
	CString svalue=GetItemText (nItem, nCol);
		
	CString sMode=m_ListColInfo[nCol].sMode;	
	m_pPicker = new gxPickerCell (this, nItem, nCol, svalue, sMode);
	
	if(m_pPicker)
	{	
		m_pPicker->Create(WS_CHILD | DTS_RIGHTALIGN | DTS_UPDOWN | WS_TABSTOP,
			              CRect(0,0,0,0),this,9998);		
		EditSubItem(nItem, nCol, m_pPicker); 
		
		m_pPicker->SetFormat(sMode);
		((gxPickerCell*)m_pPicker)->SetTextValue();
	}
}

void gxListCtrl::onCreateCombo(int nItem,int nCol)
{	
	CString svalue=GetItemText (nItem, nCol);

	CString sMode=m_ListColInfo[nCol].sMode;	
	m_pCombo= new gxComboCell (this, nItem, nCol, GetItemText (nItem, nCol));

	if(m_pCombo)
	{			
		m_pCombo->Create(WS_CHILD | CBS_DROPDOWNLIST | WS_VSCROLL | WS_TABSTOP,
			             CRect(0,0,0,50),this,9997);	
		EditSubItem(nItem, nCol, m_pCombo);	

		int nCount=GStr::str_Count(sMode,_T("|"));//����|����|��¥|�ְ�|����|
		if(nCount>0)
		{			
			for(int i=0;i<nCount;i++)
			{
				CString stemp=GStr::str_GetSplit(sMode,i,'|');				
				m_pCombo->AddString(stemp);	

				if(svalue==stemp) m_pCombo->SetCurSel(i);
			}
		}
	}
}

//dongchul
BOOL gxListCtrl::PreTranslateMessage(MSG* pMsg) 
{	
    if (pMsg->message == WM_KEYDOWN)
    {
	    if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_DELETE || 
			pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_TAB || 
			pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN || GetKeyState (VK_CONTROL))
		{	
			::TranslateMessage (pMsg);
			::DispatchMessage (pMsg);
			return FALSE;		    	// DO NOT process further
		}		
    } 
		
	return CListCtrl::PreTranslateMessage(pMsg);
}


//color
void gxListCtrl::OnCustomdraw ( NMHDR* pNMHDR, LRESULT* pResult )
{
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );

	// Take the default processing unless we set this to something else below.
    *pResult = CDRF_DODEFAULT;

    // First thing - check the draw stage. If it's the control's prepaint
    // stage, then tell Windows we want messages for every item.

    if ( CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage )
    {
        *pResult = CDRF_NOTIFYITEMDRAW;
    }
    else if ( CDDS_ITEMPREPAINT == pLVCD->nmcd.dwDrawStage )
    {
        // This is the prepaint stage for an item. Here's where we set the
        // item's text color. Our return value will tell Windows to draw the
        // item itself, but it will use the new color we set here.
       
	   int iCol = pLVCD->iSubItem;
       int iRow = pLVCD->nmcd.dwItemSpec;
	   int State = ListView_GetItemState(m_hWnd, iRow, LVIS_CUT|LVIS_SELECTED|LVIS_FOCUSED);
       CDC *pDC = CDC::FromHandle(pLVCD->nmcd.hdc);

	   CRect rcItem;
       GetCellRect(iRow, iCol, LVIR_BOUNDS, rcItem);
      
	   if((iRow%2)==0)	pLVCD->clrText = m_iEvenColor;//¦��
	   else             pLVCD->clrText = m_iOddColor;//Ȧ��
        
       //if (pLVCD->nmcd.dwItemSpec  == 0 )
	   //{// first line
       //   pLVCD->clrText = RGB(0,0,0);        
	   //}
		
	   if( State & LVIS_SELECTED )
	   {			
	      pLVCD->clrText = m_iFocusText;
		  pLVCD->clrTextBk = m_iFocusBkColor;
	   }		
       // Tell Windows to paint the control itself.
       *pResult = CDRF_DODEFAULT;
	}	
}

int gxListCtrl::GetColumnCount()
{
	int nCount = -1;
	CHeaderCtrl* pHeader = GetHeaderCtrl();

	if (pHeader != NULL) nCount = pHeader->GetItemCount();

	return nCount;
}


BOOL gxListCtrl::GetCellRect(int iRow, int iCol, int nArea, CRect&rect)
{
	if(iCol) return GetSubItemRect(iRow, iCol, nArea, rect);

	if(GetColumnCount()== 1) return GetItemRect(iRow, rect, nArea);

	iCol = 1;
	CRect rCol1;
	if(!GetSubItemRect(iRow, iCol, nArea, rCol1)) return FALSE;

	if(!GetItemRect(iRow, rect, nArea)) return FALSE;

	rect.right = rCol1.left;

	return TRUE;
}

void gxListCtrl::SetSelectDelete()
{//select item	
	// get selected items
	int nIndex = GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
	int nCount = GetSelectedCount();
	if(nIndex==-1 || nCount<1) return;
				
	// download items
	while (nIndex != -1)
	{
		DeleteItem(nIndex);
		nIndex--;

		nIndex = GetNextItem(nIndex, LVNI_ALL | LVNI_SELECTED);	
	}	
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//sorting

void gxListCtrl::SetListSort(int iType,int nCol,int iSort)
{		
	if(nCol<0) return;
	if(iSort!=-1)
	{
		m_bSortAscending=iSort;
		//if(iSort==m_bSortAscending) return;
	}
	
	LISTCOLINFO *ListColInfo=new LISTCOLINFO;
	ListColInfo->nCol=nCol;
	ListColInfo->bSortAscending=m_bSortAscending;	
	ListColInfo->listCtrl=this;
	m_bSortAscending=!m_bSortAscending;
	
	if(iType==1)
	{
		SortItems(SetCompareFileInfoFunc, (LPARAM)ListColInfo);
	}
	else if(iType==2)
	{		
		SetSort();
		SortItems(SetCompareFunc, (LPARAM)ListColInfo);
	}
}

void gxListCtrl::SetSort()
{
	int nCount = GetItemCount() ;
	if ( nCount < 1 )  return;

	for ( int i=0 ; i < nCount ; i++ )
	{
		SetItemData( i, i) ;
	}	
}

int gxListCtrl::SetCompareFileInfoFunc(LPARAM lParam1, LPARAM lParam2,LPARAM lParamSort)
{		
	FILEINFO* pItem1 =(FILEINFO*) lParam1;
	FILEINFO* pItem2 =(FILEINFO*) lParam2;
	LISTCOLINFO* ListColInfo =(LISTCOLINFO*)lParamSort;
	if(!pItem1 || !pItem2 || !ListColInfo)  return 0;

	int nResult;
	BOOL bSortAscending=ListColInfo->bSortAscending;
	
	switch(ListColInfo->nCol) 
	{
		case 0: // File name
		{
			if (pItem1->bIsDirectory && !pItem2->bIsDirectory)
				return (bSortAscending ? -1 : 1);

			if (!pItem1->bIsDirectory && pItem2->bIsDirectory)
				return (bSortAscending ? 1 : -1);

			nResult = pItem1->strFileName.CompareNoCase(pItem2->strFileName);		
			break;
		}
		case 1: // File size
		{
			if (pItem1->bIsDirectory && !pItem2->bIsDirectory)
				return (bSortAscending ? -1 : 1);
		
			if (!pItem1->bIsDirectory && pItem2->bIsDirectory)
				return (bSortAscending ? 1 : -1);

			nResult = (DWORD)(pItem1->nFileSize-pItem2->nFileSize);
			break;
		}
		case 2: // type
		{
			if (pItem1->bIsDirectory && !pItem2->bIsDirectory)
				return -1;

			if (!pItem1->bIsDirectory && pItem2->bIsDirectory)
				return 1;

			nResult = pItem1->strType.CompareNoCase(pItem2->strType);
			break;
		}
		case 3: // Date and time
		{
			nResult = ::CompareFileTime(&pItem1->ftLastWriteTime, &pItem2->ftLastWriteTime);
			break;
		}	
	}

	return (bSortAscending ? nResult : -nResult);
}

int gxListCtrl::SetCompareFunc(LPARAM lParam1, LPARAM lParam2,LPARAM lParamSort)
{		
	LISTCOLINFO* ListColInfo =(LISTCOLINFO*)lParamSort;
	if(!ListColInfo)  return 0;
			
    CListCtrl *pListCtrl = ListColInfo->listCtrl;
	if(!pListCtrl) return 0;

	int nCol = ListColInfo->nCol;
	if(nCol<0) return 0;

	BOOL bSortAscending=ListColInfo->bSortAscending;
    	
    LVFINDINFO info1, info2;
    info1.flags = LVIF_TEXT;
    info1.lParam = lParam1;
    info2.flags = LVIF_TEXT;
    info2.lParam = lParam2;

    int irow1 = pListCtrl->FindItem(&info1,-1);
    int irow2 = pListCtrl->FindItem(&info2,-1);
    
    CString strItem1 = pListCtrl->GetItemText(irow1, nCol);
    CString strItem2 = pListCtrl->GetItemText(irow2, nCol);

	int nResult=0;

	#ifdef _UNICODE	
		CStringA strA(strItem2);
		CStringA strB(strItem1);
		nResult=strcmp(strA, strB);
	#else
		nResult=strcmp(strItem2, strItem1);
	#endif	

	return (bSortAscending ? nResult : -nResult);	
}


void gxListCtrl::SetFileGetDispInfo(NMHDR *pNMHDR)
{	
	CString string=_T("");
	LV_DISPINFO* pDispInfo =(LV_DISPINFO*)pNMHDR;
	if(!pDispInfo) return;
	
    if(pDispInfo->item.mask & LVIF_TEXT) 
	{		
		FILEINFO* pItem =(FILEINFO*) pDispInfo->item.lParam;
		if(!pItem) return;

        switch(pDispInfo->item.iSubItem) 
		{
			case 0: // File name
			{
				::lstrcpy(pDispInfo->item.pszText, pItem->strFileName);
				break;
			}
			case 1: // File size
			{
				if (pItem->bIsDirectory)
				{
					::lstrcpy(pDispInfo->item.pszText, _T("0"));
				}
				else
				{
					::lstrcpy(pDispInfo->item.pszText, pItem->sFileSize);					
				}
				break;
			}
			case 2: // File type
			{
				::lstrcpy(pDispInfo->item.pszText, pItem->strType);
				break;
			}
			case 3: // Date and time
			{							
				CTime time(pItem->ftLastWriteTime);				
				time=time-CTimeSpan(0,9,0,0);//gmt���� 9�ð��� ���� �ѱ��ð�

				string.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
							  time.GetYear(),time.GetMonth(), time.GetDay(),
							  time.GetHour(), time.GetMinute(), time.GetSecond());
				::lstrcpy(pDispInfo->item.pszText, string);
				break;
			}
			case 4:// playtime
		    {			 
				::lstrcpy(pDispInfo->item.pszText, GTime::time_GetTimeStr(pItem->nPlaytime));
				break;
		    } 
        }		
    }
}

BOOL gxListCtrl::IsDirectory(int nItem)
{	
	FILEINFO *FileInfo=(FILEINFO*)GetItemData(nItem);
    if(!FileInfo) return FALSE;

	return FileInfo->bIsDirectory;
}

CPtrArray* gxListCtrl::GetSelectFileInfoList()
{//select item	
	// get selected items
	int nIndex = GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
	int nCount = GetSelectedCount();	
	if(nIndex==-1 || nCount<1) return NULL;
	
	CPtrArray *SelList=new CPtrArray;
	// download items
	while (nIndex != -1)
	{
		FILEINFO *FileInfo=(FILEINFO*)GetItemData(nIndex);
        if(!FileInfo) continue;						
					
		SelList->Add(GFile::file_FileInfoCopy(FileInfo));
		nIndex = GetNextItem(nIndex, LVNI_ALL | LVNI_SELECTED);		
	}
	
	return SelList;
}

FILEINFO* gxListCtrl::GetSelectFileInfo()
{
	int nIndex = GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
	if (nIndex != -1)
	{
		return (FILEINFO*)GetItemData(nIndex);
	}
	return NULL;
}

BOOL gxListCtrl::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	HD_NOTIFY *pHDN = (HD_NOTIFY*)lParam;

	if( (pHDN->hdr.code == HDN_BEGINTRACKW || 
		 pHDN->hdr.code == HDN_BEGINTRACKA)  ) 
	{   
		if(GetColumnWidth(pHDN->iItem) == 0)
		{// when the size of the column is 0, it means we wnat to hide  
		 // this column, we also prevent it from resizing    
			*pResult = TRUE; // disable tracking       
			return TRUE;   // processed message   
		} 
	}
 
	return CListCtrl::OnNotify(wParam, lParam, pResult);
}


//////////////////////////////////////////////////////////////////
void gxListCtrl::onSetDragInit(DRAGDROPINFO *DragdropInfo)
{//drag&drop
	if(!DragdropInfo) return;

	DragdropInfo->pDragList=NULL;
	DragdropInfo->pDropList=NULL;
	DragdropInfo->pDragImage=NULL;
	DragdropInfo->bDragging=FALSE;
	DragdropInfo->nDragIndex=-1;
	DragdropInfo->nDropIndex=-1;
	DragdropInfo->pDropWnd=NULL;
}

BOOL gxListCtrl::onSetDragBegin(DRAGDROPINFO *DragdropInfo,LPNMLISTVIEW pNMLV)
{
	if(!DragdropInfo) return FALSE;

	int nItem=pNMLV->iItem;
	if(nItem>-1)
	{
		DragdropInfo->nDragIndex = nItem;
		int nSelCount=GetSelectedCount();

		POINT pt;
		DragdropInfo->pDragImage = onSetDragCreateImageEx(&pt);//CreateDragImage(nItem, &pt);
		if(DragdropInfo->pDragImage)
		{
			DragdropInfo->pDragImage->SetBkColor(RGB(255, 0, 0));

			DragdropInfo->pDragImage->BeginDrag(0, CPoint(0, 0));
			DragdropInfo->pDragImage->DragEnter(GetDesktopWindow(),pNMLV->ptAction);
			DragdropInfo->pDragImage->DragShowNolock(true);

			DragdropInfo->bDragging = TRUE;
			DragdropInfo->nDropIndex = -1;
			DragdropInfo->pDragList = this;
			DragdropInfo->pDropWnd = this;

			//SetCapture();//main dialog call
			return TRUE;
		}
	}
	return FALSE;
}

CImageList* gxListCtrl::onSetDragCreateImageEx( LPPOINT lpPoint )
{
	CRect	cSingleRect;
	CRect	cCompleteRect( 0,0,0,0 );
	int		nIdx;
	BOOL	bFirst = TRUE;
	
	POSITION pos = GetFirstSelectedItemPosition();
	while (pos)
	{
		nIdx = GetNextSelectedItem( pos );
		GetItemRect( nIdx, cSingleRect, LVIR_BOUNDS );
		if (bFirst)
		{			
			GetItemRect( nIdx, cCompleteRect, LVIR_BOUNDS );
			bFirst = FALSE;
		}
		cCompleteRect.UnionRect( cCompleteRect, cSingleRect );
	}

	CClientDC	cDc(this);	
	CDC 		cMemDC;	
	CBitmap		cBitmap;

	if(!cMemDC.CreateCompatibleDC(&cDc)) return NULL;	
	if(!cBitmap.CreateCompatibleBitmap(&cDc, 
		                              cCompleteRect.Width(), 
									  cCompleteRect.Height()))
	{
		return NULL;
	}
	
	CBitmap* pOldMemDCBitmap = cMemDC.SelectObject( &cBitmap );	
	cMemDC.FillSolidRect(0,0,cCompleteRect.Width(), cCompleteRect.Height(), RGB(0, 255, 0)); 

	CImageList *pSingleImageList;
	CPoint		cPt;

	pos = GetFirstSelectedItemPosition();	
	while (pos)
	{
		nIdx = GetNextSelectedItem( pos );
		GetItemRect( nIdx, cSingleRect, LVIR_BOUNDS );

		pSingleImageList = CreateDragImage( nIdx, &cPt);
		if (pSingleImageList)
		{
			pSingleImageList->DrawIndirect( &cMemDC, 
											0, 
											CPoint( cSingleRect.left-cCompleteRect.left, 
													cSingleRect.top-cCompleteRect.top ),
											cSingleRect.Size(), 
											CPoint(0,0));		
			
			pSingleImageList->DeleteImageList();
			delete pSingleImageList;
			
		}		
	}
	cMemDC.SelectObject( pOldMemDCBitmap );
	CImageList* pCompleteImageList = new CImageList;	
	pCompleteImageList->Create(cCompleteRect.Width(), 
							   cCompleteRect.Height(), 
							   ILC_COLOR | ILC_MASK, 0, 1);	
	pCompleteImageList->Add(&cBitmap, RGB(0, 255, 0)); 
	cBitmap.DeleteObject();
	
	if ( lpPoint )
	{
		CPoint cCursorPos;
		GetCursorPos( &cCursorPos );
		ScreenToClient( &cCursorPos );
		lpPoint->x = cCursorPos.x - cCompleteRect.left;
		lpPoint->y = cCursorPos.y - cCompleteRect.top;
	}

	return( pCompleteImageList );
}

void gxListCtrl::onSetDragMouseMove(DRAGDROPINFO *DragdropInfo,CPoint point)
{
	if(!DragdropInfo) return;

	if (DragdropInfo->bDragging)
	{		
		CPoint pt(point);
		GetParent()->ClientToScreen(&pt);
		DragdropInfo->pDragImage->DragMove(pt);		
		DragdropInfo->pDragImage->DragShowNolock(false);

		CWnd* pDropWnd = WindowFromPoint (pt);
		ASSERT(pDropWnd);
				
		if (pDropWnd != DragdropInfo->pDropWnd)
		{//source & target mismatch
			if (DragdropInfo->nDropIndex != -1)
			{
				CListCtrl* pList = (CListCtrl*)DragdropInfo->pDropWnd;								
				VERIFY (pList->SetItemState (DragdropInfo->nDropIndex, 0, LVIS_DROPHILITED));
				
				VERIFY (pList->RedrawItems (DragdropInfo->nDropIndex, DragdropInfo->nDropIndex));
				pList->UpdateWindow ();
				DragdropInfo->nDropIndex = -1;
			}
			else
			{
				CListCtrl* pList = (CListCtrl*)DragdropInfo->pDropWnd;				
				int i = 0;
				int nCount = pList->GetItemCount();
				for(int i = 0; i < nCount; i++)
				{
					pList->SetItemState(i, 0, LVIS_DROPHILITED);
				}
				pList->RedrawItems(0, nCount);
				pList->UpdateWindow();
			}			
		}
		DragdropInfo->pDropWnd = pDropWnd;
		pDropWnd->ScreenToClient(&pt);
		
		if(pDropWnd->IsKindOf(RUNTIME_CLASS (CListCtrl)))
		{									
			SetCursor(LoadCursor(NULL, IDC_ARROW));
			UINT uFlags;
			CListCtrl* pList = (CListCtrl*)pDropWnd;
			if(pList!=DragdropInfo->pDragList)
			{//source & target mismatch
				GList::list_SetAllUnSelect(pList);//unselect

				pList->RedrawItems (DragdropInfo->nDropIndex, DragdropInfo->nDropIndex);				
				DragdropInfo->nDropIndex = ((CListCtrl*)pDropWnd)->HitTest(pt, &uFlags);
				GList::list_SetFocus(pList,DragdropInfo->nDropIndex);
				
				pList->UpdateWindow();	
			}			
		}
		else
		{
			SetCursor(LoadCursor(NULL, IDC_NO));
		}		
		DragdropInfo->pDragImage->DragShowNolock(true);
	}
}

BOOL gxListCtrl::onSetDragUp(DRAGDROPINFO *DragdropInfo,CPoint point)
{
	if(!DragdropInfo) return FALSE;

	if (DragdropInfo->bDragging)
	{
		//ReleaseCapture ();//main dialog call

		DragdropInfo->bDragging = FALSE;
		
		DragdropInfo->pDragImage->DragLeave (GetDesktopWindow ());
		DragdropInfo->pDragImage->EndDrag ();
		delete DragdropInfo->pDragImage; 

		CPoint pt (point);
		GetParent()->ClientToScreen (&pt);		
		CWnd* pDropWnd = WindowFromPoint (pt);
		ASSERT (pDropWnd); 		
		if (pDropWnd->IsKindOf (RUNTIME_CLASS (CListCtrl)))
		{			
			DragdropInfo->pDropList = (CListCtrl*)pDropWnd; 			
			return TRUE;
		}
	}
	return FALSE;
}