// ModuleIndexTable.cpp: implementation of the CModuleIndexTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CTSRealGraph.h"
#include "ModuleIndexTable.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CModuleIndexTable::CModuleIndexTable()
{

}

CModuleIndexTable::~CModuleIndexTable()
{
	RemoveModuleIndexList();
}

void CModuleIndexTable::RemoveModuleIndexList()
{
	LPCT_MD_INDEX_INFO pObject = NULL;
	int nSize = m_ptrListModuleIndex.GetCount();
	for(int i=0; i<	nSize; i++)
	{
		pObject = (LPCT_MD_INDEX_INFO)m_ptrListModuleIndex.GetHead();
		delete pObject;
		pObject = NULL;
		m_ptrListModuleIndex.RemoveHead();
	}		
	m_ptrListModuleIndex.RemoveAll();
}

BOOL CModuleIndexTable::LoadTable()
{
	RemoveModuleIndexList();
	if(m_strStartFile.IsEmpty() || m_strEndFile.IsEmpty())	return FALSE;

	char szBuff[128];
	int nTemp1, nTemp2, nTemp3, nStartNo, nLastNo, nFileNo;
	FILE *fp = NULL;
	fp = fopen(m_strStartFile, "rt");		//Module에서 Download한 Index 파일에서 Maching table을 만든다.
	LPCT_MD_INDEX_INFO pObject = NULL;
	LPCT_MD_INDEX_INFO pPrevObject = NULL;
	if(fp)
	{
		//저장구조 : fileIndex 1, resultIndex 1, open_year 5, open_month 12, open_day 13
		while( fscanf(fp, "%s %d, %s %d, %s %d, %s %d, %s %d", 
				szBuff, &nFileNo,
				szBuff, &nStartNo,
				szBuff, &nTemp1,
				szBuff, &nTemp2,
				szBuff, &nTemp3 ) > 0)
		{
			pObject = new CT_MD_INDEX_INFO;
			ZeroMemory(pObject, sizeof(CT_MD_INDEX_INFO));
			pObject->lFileSeqNo = nFileNo;
			pObject->lStartNo = nStartNo;
			sprintf(pObject->szStartDate, "%d/%d/%d", 2000+nTemp1, nTemp2, nTemp3);
			
			if(pPrevObject != NULL)
			{
				pPrevObject->lEndNo = nStartNo-1;
			}
			pPrevObject = pObject;
			m_ptrListModuleIndex.AddTail(pObject);
		}
		fclose(fp);
	}
	else
	{
		TRACE("Module index start file %d open fail\n", m_strStartFile);
		return FALSE;
	}

	//PC와 Module의 가장 마지막 Index를 비교해서 다를 경우 마지막쪽 손실이 있음 
	fp = fopen(m_strEndFile, "rt");		//Module에서 Download한 최종 Index파일을 확인한다.
	if(fp)
	{
		//저장 형태 : fileIndex 3, resultIndex 17, open_year 5, open_month 12, open_day 13
		if(fscanf(fp, "%s %d, %s %d, %s %d, %s %d, %s %d", 
				szBuff, &nFileNo,
				szBuff, &nLastNo,
				szBuff, &nTemp1,
				szBuff, &nTemp2,
				szBuff, &nTemp3
				) > 0)
		{
			if(pObject)	pObject->lEndNo = nLastNo;
		}
		else
		{
			//최종 Index를 알수 없을 경우 가장 마지막 삭제 
			if(m_ptrListModuleIndex.GetCount() > 0)
			{
				pObject = (LPCT_MD_INDEX_INFO)m_ptrListModuleIndex.GetTail();
				delete pObject;
				m_ptrListModuleIndex.RemoveTail();
			}
		}
		fclose(fp);
	}
	else
	{	
		//가장마지막 정보를 얻지 못해도 이전 정보만 복구 
		TRACE("Module index end file %d open fail\n", m_strEndFile);
		if(m_ptrListModuleIndex.GetCount() > 0)
		{
			//최종 Index를 알수 없을 경우 가장 마지막 삭제 
			pObject = (LPCT_MD_INDEX_INFO)m_ptrListModuleIndex.GetTail();
			delete pObject;
			m_ptrListModuleIndex.RemoveTail();
		}
	} 
	
#ifdef _DEBUG
	LPCT_MD_INDEX_INFO pObject1 = NULL;
	POSITION  pos = m_ptrListModuleIndex.GetHeadPosition();
	while(pos)
	{
		pObject1 = (LPCT_MD_INDEX_INFO)m_ptrListModuleIndex.GetNext(pos);
		TRACE("FileNo %d: %d~%d(%d), %s\n", pObject1->lFileSeqNo, pObject1->lStartNo, pObject1->lEndNo, pObject1->lEndNo- pObject1->lStartNo+1,  pObject1->szStartDate);
	}		
#endif
	return TRUE;
}

int CModuleIndexTable::GetLastCount()
{
	if(m_ptrListModuleIndex.GetCount() < 1)		return 0;

	LPCT_MD_INDEX_INFO pData = (LPCT_MD_INDEX_INFO)m_ptrListModuleIndex.GetTail();
	if(pData)		return pData->lEndNo;

	return 0;
}

//nStartNo 가 포함된 file의 Index를 찾는다.
long CModuleIndexTable::FindFirstMachFileNo(long nStartNo)
{
	LPCT_MD_INDEX_INFO pObject = NULL;
	POSITION pos = m_ptrListModuleIndex.GetHeadPosition();
	int nCount = 0;
	while(pos)
	{
		nCount ++;
		pObject = (LPCT_MD_INDEX_INFO)m_ptrListModuleIndex.GetNext(pos);
		//SBC에서 한 시험의 처음부터 끝까지 모든 Index파일은 가지고 있음 
		if(pObject->lStartNo <= nStartNo && nStartNo <=pObject->lEndNo)
		{
			break;
		}
	}		
	return nCount;
}

//nStartNo 이후로 주어진 nSequenceNo 파일의 Data크기를 구한다.
long CModuleIndexTable::GetRawDataSize(int nSequenceNo, int nStartNo)
{
	long lSize = 0;
	int nCount = 0;
	
	LPCT_MD_INDEX_INFO pObject = NULL;
	POSITION pos = m_ptrListModuleIndex.GetHeadPosition();
	while(pos)
	{
		nCount ++;
		if(nCount == nSequenceNo)
		{
			pObject = (LPCT_MD_INDEX_INFO)m_ptrListModuleIndex.GetNext(pos);
			if( pObject->lStartNo <= nStartNo && nStartNo <= pObject->lEndNo)
			{
				lSize = pObject->lEndNo-nStartNo+1;
			}
			else	//nStartNo == 0 or 잘못된 Parameter
			{
				lSize = pObject->lEndNo-pObject->lStartNo+1;			
			}
			break;
		}
	}		
	return lSize;
}
