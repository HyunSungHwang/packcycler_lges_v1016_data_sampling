// CTSRealGraphDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSRealGraph.h"
#include "CTSRealGraphDlg.h"

#include "Dlg_Allgraph.h"
#include "Dlg_Chgraph.h"
#include "Dlg_Auxgraph.h"
#include "Dlg_Cangraph.h"
#include "Dlg_Chamgraph.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSRealGraphDlg dialog

CCTSRealGraphDlg::CCTSRealGraphDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCTSRealGraphDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCTSRealGraphDlg::IDD2):
	(CCTSRealGraphDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CCTSRealGraphDlg)
	m_CtrlLabChannelNum = _T("");
	m_CtrlLabStepType = _T("");
	m_CtrlLabStepNum = _T("");
	m_CtrlLabCycleNum = _T("");
	m_EditTestName = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_MAINFRAME2);

	mainDlg=NULL;
	mainTabAll=NULL;
	mainTabCh=NULL;
	mainTabAux=NULL;
	mainTabCan=NULL;	
    mainTabCham=NULL;

	g_GraphInfo.nAuxCount=0;
	g_GraphInfo.nCanCount=0;
	g_GraphInfo.AuxInfo=NULL;
	g_GraphInfo.CanInfo=NULL;	
}

void CCTSRealGraphDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTSRealGraphDlg)
	DDX_Control(pDX, IDC_BUT_DRAW_STOP, m_ButDrawStop);
	DDX_Control(pDX, IDC_BUT_DRAW_START, m_ButDrawStart);
	DDX_Control(pDX, IDC_STATIC_CHINFO, m_StaticChinfo);
	DDX_Control(pDX, IDC_TAB_GRAPH, m_TabGraph);
	DDX_Text(pDX, IDC_LAB_CHANNEL_NUM, m_CtrlLabChannelNum);
	DDX_Text(pDX, IDC_LAB_STEP_TYPE, m_CtrlLabStepType);
	DDX_Text(pDX, IDC_LAB_STEP_NUM, m_CtrlLabStepNum);
	DDX_Text(pDX, IDC_LAB_CYCLE_NUM, m_CtrlLabCycleNum);
	DDX_Text(pDX, IDC_EDIT_TESTNAME, m_EditTestName);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCTSRealGraphDlg, CDialog)
	//{{AFX_MSG_MAP(CCTSRealGraphDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUT_DRAW_START, OnButDrawStart)
	ON_BN_CLICKED(IDC_BUT_DRAW_STOP, OnButDrawStop)
	ON_BN_CLICKED(IDC_BUT_DRAW_SET, OnButDrawSet)
	ON_BN_CLICKED(IDC_BUT_DRAW_RANGE_SET, OnButDrawRangeSet)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUT_DRAW_MAX, OnButDrawMax)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSRealGraphDlg message handlers

BOOL CCTSRealGraphDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	ShowWindow(SW_MAXIMIZE);
	mainDlg=this;
	TabInit();

	g_GraphInfo.nModuleID=m_nModuleID;
	g_GraphInfo.nChIndex=m_nChIndex;

	m_nRealChDataPos=0;
	m_bStartDraw = false;

	m_lpCyclerMD = NULL;
	m_lpCyclerMD = new CCyclerModule;

	ASSERT(m_lpCyclerMD);
	
	if (Fun_LoadModuleInfo()==FALSE)
	{
		AfxMessageBox("실시간 데이터가 없습니다");
		return FALSE;
	}
	InitControl();	        // AUX, CAN 데이터 표시	
	Fun_LoadConfigFile();	// 설정 파일 읽기

	InitTabGraph();
					
	m_ButDrawStart.EnableWindow(TRUE);
	m_ButDrawStop.EnableWindow(FALSE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCTSRealGraphDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCTSRealGraphDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCTSRealGraphDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CCTSRealGraphDlg::OnClose() 
{
	if (m_lpCyclerMD != NULL) delete m_lpCyclerMD;

	CDialog::OnClose();
}

void CCTSRealGraphDlg::TabInit()
{
	m_TabGraph.InsertItem(0, _T("All Graph"));
	m_TabGraph.InsertItem(1, _T("Channel Graph"));
	m_TabGraph.InsertItem(2, _T("AUX Graph"));
	m_TabGraph.InsertItem(3, _T("CAN Graph"));
	m_TabGraph.InsertItem(4, _T("Chamber Graph"));

	m_TabGraph.m_tabPages=new CDialog*[m_TabGraph.GetItemCount()];

	m_TabGraph.m_tabPages[0]=new Dlg_Allgraph;
	m_TabGraph.m_tabPages[0]->Create(IDD_DIALOG_ALLGRAPH, &m_TabGraph);	
	
	m_TabGraph.m_tabPages[1]=new Dlg_Chgraph;
	m_TabGraph.m_tabPages[1]->Create(IDD_DIALOG_CHGRAPH, &m_TabGraph);
	
	m_TabGraph.m_tabPages[2]=new Dlg_Auxgraph;
	m_TabGraph.m_tabPages[2]->Create(IDD_DIALOG_AUXGRAPH, &m_TabGraph);

	m_TabGraph.m_tabPages[3]=new Dlg_Cangraph;
	m_TabGraph.m_tabPages[3]->Create(IDD_DIALOG_CANGRAPH, &m_TabGraph);
	
	m_TabGraph.m_tabPages[4]=new Dlg_Chamgraph;
	m_TabGraph.m_tabPages[4]->Create(IDD_DIALOG_CHAMGRAPH, &m_TabGraph);
	
	m_TabGraph.SetRectangle(0);
}
 
BOOL CCTSRealGraphDlg::Fun_LoadModuleInfo()
{		
	g_GraphInfo.nAuxCount=0;
	g_GraphInfo.nCanCount=0;
	if(g_GraphInfo.AuxInfo)
	{
		g_GraphInfo.AuxInfo=NULL;
	}
	if(g_GraphInfo.CanInfo)
	{
		g_GraphInfo.CanInfo=NULL;
	}

	m_pRealChData = ::SFTGetRealChData(m_nModuleID-1,m_nChIndex);			
	if (m_pRealChData == NULL)	
	{
		return FALSE;
	}

	//m_pRealChData->chData.nAuxCount=10;
	//m_pRealChData->chData.nCanCount=15;

	g_GraphInfo.nAuxCount=m_pRealChData->chData.nAuxCount;
	g_GraphInfo.nCanCount=m_pRealChData->chData.nCanCount;
	
	if(g_GraphInfo.nAuxCount>0)
	{
		g_GraphInfo.AuxInfo=new Graph_Sub_Info[g_GraphInfo.nAuxCount];
		::memset(g_GraphInfo.AuxInfo,0,sizeof(g_GraphInfo.AuxInfo));
	}
	if(g_GraphInfo.nCanCount>0)
	{
		g_GraphInfo.CanInfo=new Graph_Sub_Info[g_GraphInfo.nCanCount];
		::memset(g_GraphInfo.CanInfo,0,sizeof(g_GraphInfo.CanInfo));
	}

	return TRUE;
}

BOOL CCTSRealGraphDlg::Fun_GetRealChData()
{
	m_pRealChData      = ::SFTGetRealChData(m_nModuleID-1,m_nChIndex);
	m_pRealchamberData = ::SFTGetRealChamberData(m_nModuleID-1,m_nChIndex);
	if (m_pRealChData == NULL)	
	{
		return FALSE;
	}

	return TRUE;
}

void CCTSRealGraphDlg::OnTimer(UINT nIDEvent) 
{	
	switch (nIDEvent)
	{
	case ID_TIMER_REAL_CH_DATA:
		if (Fun_GetRealChData())
		{
			m_nRealChDataPos++;
			Fun_DrawRealData();
		}
		else
		{
			KillTimer(nIDEvent);
			AfxMessageBox("Real Data가 수신되지 않습니다.");
		}
		break;
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CCTSRealGraphDlg::InitControl()
{
	UpdateData();

	CString strTemp;
	strTemp.Format("Module[%02d] Ch [%02d]",m_nModuleID,m_nChIndex+1);
	m_CtrlLabChannelNum = strTemp;
		
	UpdateData(FALSE);
}

void CCTSRealGraphDlg::OnButDrawStop() 
{
	if(m_bStartDraw==TRUE)
	{
		if(mainTabAll)    mainTabAll->onStop();
		if(mainTabCh)     mainTabCh->onStop();
		if(mainTabAux)    mainTabAux->onStop();
		if(mainTabCan)    mainTabCan->onStop();
		if(mainTabCham)   mainTabCham->onStop();
		
		m_bStartDraw = FALSE;
		
		m_ButDrawStart.EnableWindow(!m_bStartDraw);
		m_ButDrawStop.EnableWindow(m_bStartDraw);

		KillTimer(ID_TIMER_REAL_CH_DATA);
	}
}

void CCTSRealGraphDlg::OnButDrawStart() 
{
	if(m_pRealChData==NULL)
	{
		AfxMessageBox("실시간 데이터가 없습니다");
		return;
	}

	if(m_bStartDraw == FALSE)
	{		
		if(onStart()==FALSE)
		{
			AfxMessageBox("설정이 잘못되었습니다.다시 저장하여주세요.");			
		}
		
		m_bStartDraw = TRUE;

		m_ButDrawStart.EnableWindow(!m_bStartDraw);
		m_ButDrawStop.EnableWindow(m_bStartDraw);

		KillTimer(ID_TIMER_REAL_CH_DATA);
		SetTimer(ID_TIMER_REAL_CH_DATA,ID_TIMER_REAL_CH_DATA_INTERVAL,NULL);
	}	
}

BOOL CCTSRealGraphDlg::onStart() 
{
	BOOL bflag=TRUE;

	if(mainTabAll) { if(mainTabAll->onStart()==FALSE)  bflag=FALSE; }
	if(mainTabCh)  { if(mainTabCh->onStart()==FALSE)   bflag=FALSE; }
	if(mainTabAux) { if(mainTabAux->onStart()==FALSE)  bflag=FALSE; }
	if(mainTabCan) { if(mainTabCan->onStart()==FALSE)  bflag=FALSE; }
	if(mainTabCham){ if(mainTabCham->onStart()==FALSE) bflag=FALSE; }

	return bflag;
}
		
BOOL CCTSRealGraphDlg::InitTabGraph() 
{
	BOOL bflag=TRUE;

	if(mainTabAll) { if(mainTabAll->Fun_InitGraph()==FALSE)  bflag=FALSE; }
	if(mainTabCh)  { if(mainTabCh->Fun_InitGraph()==FALSE)   bflag=FALSE; }
	if(mainTabAux) { if(mainTabAux->Fun_InitGraph()==FALSE)  bflag=FALSE; }
	if(mainTabCan) { if(mainTabCan->Fun_InitGraph()==FALSE)  bflag=FALSE; }
	if(mainTabCham){ if(mainTabCham->Fun_InitGraph()==FALSE) bflag=FALSE; }

	return bflag;
}

void CCTSRealGraphDlg::OnButDrawSet() 
{
	if( m_bStartDraw == TRUE)
	{
		AfxMessageBox("진행을 멈추고 설정하세요!!");
		return;
	}
	CConfigSet_DLG dlgSet;	
	if (dlgSet.DoModal() == IDOK)
	{
		// 설정 파일 다시 읽기
		Fun_LoadConfigFile();		//설정파일 읽기
		
		InitTabGraph();		
	}	
}

void CCTSRealGraphDlg::Fun_DrawFrame()
{
	if(mainTabAll)    mainTabAll->Fun_DrawFrame();
	if(mainTabCh)     mainTabCh->Fun_DrawFrame();
	if(mainTabAux)    mainTabAux->Fun_DrawFrame();
	if(mainTabCan)    mainTabCan->Fun_DrawFrame();
	if(mainTabCham)   mainTabCham->Fun_DrawFrame();
}

void CCTSRealGraphDlg::Fun_HideGraph()
{
	if(mainTabAll)    mainTabAll->Fun_HideGraph();
	if(mainTabCh)     mainTabCh->Fun_HideGraph();
	if(mainTabAux)    mainTabAux->Fun_HideGraph();
	if(mainTabCan)    mainTabCan->Fun_HideGraph();
	if(mainTabCham)   mainTabCham->Fun_HideGraph();
}

void CCTSRealGraphDlg::OnButDrawRangeSet() 
{	
	if( m_bStartDraw == TRUE)
	{
		AfxMessageBox("진행을 멈추고 설정하세요!!");
		return;
	}

	if (Fun_LoadModuleInfo()) 
	{
		InitControl();	//AUX, CAN 데이터 표시
	}
	else
	{
		AfxMessageBox("정보가 수신되지 않습니다");		
	}	
}

void CCTSRealGraphDlg::Fun_DrawRealData()
{
	DWORD dwtime = GetTickCount();
	CString strTemp;
	
	if(m_pRealchamberData)
	{
		g_lchamberdata1 = m_pRealchamberData->fRefTemper;
		g_lchamberdata2 = m_pRealchamberData->fCurTemper; 
		g_lchamberdata3 = m_pRealchamberData->fRefHumidity;
		g_lchamberdata4 = m_pRealchamberData->fCurHumidity;
	}
	
	if(m_pRealChData)
	{
		m_CtrlLabCycleNum.Format("%d / %d" ,m_pRealChData->chData.nCurrentCycleNum, m_pRealChData->chData.nTotalCycleNum);
		//m_CtrlLabStepNum.Format("%c" ,m_pRealChData->chData.chStepNo);
		m_CtrlLabStepNum.Format("%d" ,m_pRealChData->chData.chStepNo); //ksj 20210117
		
		if( m_pRealChData->chData.chStepType == PS_STEP_CHARGE )          strTemp = "Charge";
		else if (m_pRealChData->chData.chStepType == PS_STEP_DISCHARGE	) strTemp = "DisCharge";
		else if (m_pRealChData->chData.chStepType == PS_STEP_PATTERN	) strTemp = "Pattern";
		else if (m_pRealChData->chData.chStepType == PS_STEP_REST	)     strTemp = "Rest";
		else m_CtrlLabStepType.Format("%d",m_pRealChData->chData.chStepType);
	}

	UpdateData(FALSE); //ksj 20210117
} 

BOOL CCTSRealGraphDlg::Fun_LoadConfigFile()
{
 	char szData[512];
 	CStdioFile file;
 	CString strTemp;

 	CString strConfigFile;
 	strConfigFile.Format("%s\\Graph_Config_%02d_%02d.ini", Fun_getAppPath(),g_GraphInfo.nModuleID,g_GraphInfo.nChIndex);
 
	memset(szData,0,512);
	GetPrivateProfileString("UNIT1", "GRAPH1_TIME", NULL, szData, 512, strConfigFile );
	g_GraphInfo.graphtime1 = (float)atof(szData);
	
	memset(szData,0,512);
	GetPrivateProfileString("UNIT1", "GRAPH2_TIME", NULL, szData, 512, strConfigFile );
	g_GraphInfo.graphtime2 = (float)atof(szData);
	
	memset(szData,0,512);
	GetPrivateProfileString("UNIT1", "GRAPH3_TIME", NULL, szData, 512, strConfigFile );
	g_GraphInfo.graphtime3 = (float)atof(szData);
	
	memset(szData,0,512);
	GetPrivateProfileString("UNIT1", "GRAPHCH_XCNT", NULL, szData, 512, strConfigFile );
	g_GraphInfo.graphchxcount = atoi(szData);
	if(g_GraphInfo.graphchxcount<1) g_GraphInfo.graphchxcount=20;
	
	memset(szData,0,512);
	GetPrivateProfileString("UNIT1", "GRAPHAUX_XCNT", NULL, szData, 512, strConfigFile );
	g_GraphInfo.graphauxxcount = atoi(szData);
	if(g_GraphInfo.graphauxxcount<1) g_GraphInfo.graphauxxcount=20;

	memset(szData,0,512);
	GetPrivateProfileString("UNIT1", "GRAPHCAN_XCNT", NULL, szData, 512, strConfigFile );
	g_GraphInfo.graphcanxcount = atoi(szData);
	if(g_GraphInfo.graphcanxcount<1) g_GraphInfo.graphcanxcount=20;

	memset(szData,0,512);
	GetPrivateProfileString("UNIT1", "GRAPHCHAM_XCNT", NULL, szData, 512, strConfigFile );
	g_GraphInfo.graphchamxcount = atoi(szData);
	if(g_GraphInfo.graphchamxcount<1) g_GraphInfo.graphchamxcount=20;

	int i=0;
	for(int i = 0;i<4;i++)	
	{	
		memset(szData,0,512);
		strTemp.Format("DRAWCH%d_NAME",i+1);		
		GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.ChInfo[i].szname, 512, strConfigFile); 
		
		memset(szData,0,512);
		strTemp.Format("DRAWCH%d_COLOR",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		g_GraphInfo.ChInfo[i].lineColor = (int)atof(szData);
		
		memset(szData,0,512);
		strTemp.Format("DRAWCH%d_MIN",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		g_GraphInfo.ChInfo[i].fMin = (float)atof(szData);
		
		memset(szData,0,512);
		strTemp.Format("DRAWCH%d_MAX",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		g_GraphInfo.ChInfo[i].fMax = (float)atof(szData);
	
		memset(szData,0,512);
		strTemp.Format("DRAWCH%d_UNIT",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.ChInfo[i].cUnit, 512, strConfigFile );

		memset(szData,0,512);
		strTemp.Format("DRAWCH%d_CHECK",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
		g_GraphInfo.ChInfo[i].bCheck = (int)atoi(szData);

		memset(szData,0,512);
		strTemp.Format("DRAWCH%d_ALIAS",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
		g_GraphInfo.ChInfo[i].strAlias = szData;		
	}

	for(int i = 0;i<4;i++)	
	{		
		memset(szData,0,512);
		strTemp.Format("DRAWCHAM%d_NAME",i+1);		
		GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.ChamInfo[i].szname, 512, strConfigFile); 
		
		memset(szData,0,512);
		strTemp.Format("DRAWCHAM%d_COLOR",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);		
		g_GraphInfo.ChamInfo[i].lineColor = (int)atof(szData);
		
		memset(szData,0,512);
		strTemp.Format("DRAWCHAM%d_MIN",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		g_GraphInfo.ChamInfo[i].fMin = (float)atof(szData);
	
		memset(szData,0,512);
		strTemp.Format("DRAWCHAM%d_MAX",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
		g_GraphInfo.ChamInfo[i].fMax = (float)atof(szData);
		
		memset(szData,0,512);
		strTemp.Format("DRAWCHAM%d_UNIT",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.ChamInfo[i].cUnit, 512, strConfigFile );
		
		memset(szData,0,512);
		strTemp.Format("DRAWCHAM%d_CHECK",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );		
		g_GraphInfo.ChamInfo[i].bCheck = (int)atoi(szData);
		
		memset(szData,0,512);
		strTemp.Format("DRAWCHAM%d_ALIAS",i+1);
		GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
		g_GraphInfo.ChamInfo[i].strAlias = szData;		
	}

	if(g_GraphInfo.nAuxCount>0)
	{	
		for(int i = 0;i<g_GraphInfo.nAuxCount;i++)	
		{		
			memset(szData,0,512);
			strTemp.Format("DRAWAUX%d_NAME",i+1);		
			GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.AuxInfo[i].szname, 512, strConfigFile); 
			
			memset(szData,0,512);
			strTemp.Format("DRAWAUX%d_COLOR",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
			g_GraphInfo.AuxInfo[i].lineColor = (int)atof(szData);
		
			memset(szData,0,512);
			strTemp.Format("DRAWAUX%d_MIN",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
			g_GraphInfo.AuxInfo[i].fMin = (float)atof(szData);
			
			memset(szData,0,512);
			strTemp.Format("DRAWAUX%d_MAX",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
			g_GraphInfo.AuxInfo[i].fMax = (float)atof(szData);
			
			memset(szData,0,512);
			strTemp.Format("DRAWAUX%d_UNIT",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.AuxInfo[i].cUnit, 512, strConfigFile );
			
			memset(szData,0,512);
			strTemp.Format("DRAWAUX%d_CHECK",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
			g_GraphInfo.AuxInfo[i].bCheck = (int)atoi(szData);
					
			memset(szData,0,512);
			strTemp.Format("DRAWAUX%d_ALIAS",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
			g_GraphInfo.AuxInfo[i].strAlias = szData;		
		}
	}

	if(g_GraphInfo.nCanCount>0)
	{
		for(int i = 0;i<g_GraphInfo.nCanCount;i++)	
		{		
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_NAME",i+1);		
			GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.CanInfo[i].szname, 512, strConfigFile); 
			
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_COLOR",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
			g_GraphInfo.CanInfo[i].lineColor = (int)atof(szData);
			
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_MIN",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
			g_GraphInfo.CanInfo[i].fMin = (float)atof(szData);
			
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_MAX",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile);
			g_GraphInfo.CanInfo[i].fMax = (float)atof(szData);
			
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_UNIT",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, g_GraphInfo.CanInfo[i].cUnit, 512, strConfigFile );
			
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_CHECK",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );	
			g_GraphInfo.CanInfo[i].bCheck = (int)atoi(szData);
				
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_OFFSET",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
			g_GraphInfo.CanInfo[i].sOffset = szData;	

			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_FACTOR",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
			g_GraphInfo.CanInfo[i].sFactor = szData;	
			
			memset(szData,0,512);
			strTemp.Format("DRAWCAN%d_ALIAS",i+1);
			GetPrivateProfileString("UNIT1", strTemp, NULL, szData, 512, strConfigFile );
			g_GraphInfo.CanInfo[i].strAlias = szData;		
		}
	}
	return TRUE;
}

void CCTSRealGraphDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if(::IsWindow(m_TabGraph.GetSafeHwnd()))
 	{	
		m_TabGraph.MoveWindow(0,100,cx,cy-100);
	}
}

CString CCTSRealGraphDlg::Fun_getAppPath()
{
    HMODULE hModule=::GetModuleHandle(NULL); // handle of current module
    
    CString strExeFileName;
    VERIFY(::GetModuleFileName(hModule, strExeFileName.GetBuffer(_MAX_PATH),_MAX_PATH));
	
    char Drive[_MAX_DRIVE];
    char Path[_MAX_PATH];
	
    _splitpath(strExeFileName, Drive, Path, NULL,NULL);//Filename, Ext);
	
    return CString(Drive)+CString(Path); // has trailing backslash
}

int CCTSRealGraphDlg::Fun_GetCheckChinfo()
{
	int iCnt = 0;
	
	for(int i=0;i<4;i++)
	{
		if(g_GraphInfo.ChInfo[i].bCheck==1) iCnt++; 	
	}
	
	return iCnt;
}

int CCTSRealGraphDlg::Fun_GetCheckChaminfo()
{
	int iCnt = 0;
	
	for(int i=0;i<4;i++)
	{
		if(g_GraphInfo.ChamInfo[i].bCheck==1) iCnt++; 	
	}
	
	return iCnt;
}


int CCTSRealGraphDlg::Fun_GetCheckAuxinfo()
{
	int iCnt = 0;
	
	if(g_GraphInfo.nAuxCount>0)
	{
		for(int i=0;i<g_GraphInfo.nAuxCount;i++)
		{
			if(g_GraphInfo.AuxInfo[i].bCheck==1) iCnt++; 	
		}
	}
	return iCnt;
}

int CCTSRealGraphDlg::Fun_GetCheckCaninfo()
{
	int iCnt = 0;

	if(g_GraphInfo.nCanCount>0)
	{
		for(int i=0;i<g_GraphInfo.nCanCount;i++)
		{
			if(g_GraphInfo.CanInfo[i].bCheck==1) iCnt++; 	
		}
	}
	
	return iCnt;
}


LRESULT CCTSRealGraphDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch(message)
	{
		case WM_TABFOCUS:				
			break;
	}
	return CDialog::WindowProc(message, wParam, lParam);
}

BOOL CCTSRealGraphDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_ESCAPE || pMsg->wParam==VK_RETURN)
		{
			return FALSE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CCTSRealGraphDlg::OnButDrawMax() 
{
	if(m_TabGraph.m_tabCurrent==1 && mainTabCh)        mainTabCh->onGraphMax();
    else if(m_TabGraph.m_tabCurrent==2 && mainTabAux)  mainTabAux->onGraphMax();
	else if(m_TabGraph.m_tabCurrent==3 && mainTabCan)  mainTabCan->onGraphMax();
	else if(m_TabGraph.m_tabCurrent==4 && mainTabCham) mainTabCham->onGraphMax();
}