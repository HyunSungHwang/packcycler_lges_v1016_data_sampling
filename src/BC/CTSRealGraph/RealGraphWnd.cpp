// GraphWnd.cpp : implementation file
//

#include "stdafx.h"
#include "RealGraphWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRealGraphWnd_V2
CRealGraphWnd_V2::CRealGraphWnd_V2()
{
	m_GraphInfo=NULL;
	m_hWndPE = NULL;

	int i=0;
	for(int i = 0; i<	PE_MAX_SUBSET; i++)
	{		
		m_bShowArray[i] = FALSE;
	}
	m_bShowArray[0] = TRUE;

	m_iSelectGraph = 0;
}

CRealGraphWnd_V2::~CRealGraphWnd_V2()
{
}

BEGIN_MESSAGE_MAP(CRealGraphWnd_V2, CStatic)
	//{{AFX_MSG_MAP(CRealGraphWnd_V2)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()	
	ON_WM_SIZE()	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CRealGraphWnd_V2 message handlers

void CRealGraphWnd_V2::PreSubclassWindow() 
{
	CStatic::PreSubclassWindow();
	
	CreateGraph();
}

void CRealGraphWnd_V2::CreateGraph()
{
	CRect trect, rect;	
	GetClientRect(trect);
	rect.left = 1;
	rect.right = trect.Width() - 2;
	rect.top = 1;
	rect.bottom = trect.Height() - 2;
	
    m_hWndPE = PEcreate(PECONTROL_GRAPH, WS_VISIBLE|WS_BORDER, &rect, m_hWnd, 12300);   	
	if(!m_hWndPE)
	{
		TRACE("Creation Error");
		return;
	}                     
	
	SetGlobalPEStuff();	
	Fun_SetRealTimeGraph();
}

void CRealGraphWnd_V2::SetGlobalPEStuff()
{	
	PEnset(m_hWndPE, PEP_bPREPAREIMAGES, TRUE);			
	
	//PEnset(m_hWndPE, PEP_dwDESKCOLOR, RGB(255,255,255));	
	//PEnset(m_hWndPE, PEP_dwGRAPHBACKCOLOR, RGB(230,230,220));
	//PEnset(m_hWndPE, PEP_dwGRAPHFORECOLOR,RGB(192,192,192));

	PEnset(m_hWndPE, PEP_dwDESKCOLOR, 0L);
	PEnset(m_hWndPE, PEP_dwTEXTCOLOR, RGB(255,255,0));
	PEnset(m_hWndPE, PEP_dwGRAPHBACKCOLOR, 0L);
	PEnset(m_hWndPE, PEP_dwGRAPHFORECOLOR, RGB(192,192,192));

	PEnset(m_hWndPE, PEP_nDATAPRECISION, 3);
	PEnset(m_hWndPE, PEP_nALLOWZOOMING, FALSE);//PEAZ_HORZANDVERT
	PEnset(m_hWndPE, PEP_bCURSORPROMPTTRACKING, TRUE);
	PEnset(m_hWndPE, PEP_bFOCALRECT, FALSE);
}


void CRealGraphWnd_V2::Fun_SetRealTimeGraph()
{
	PEnset(m_hWndPE, PEP_nSUBSETS, PE_MAX_SUBSET);	
	PEnset(m_hWndPE, PEP_nPOINTS, 200);
	
	PEszset(m_hWndPE, PEP_szMAINTITLE, "Graph");
	PEszset(m_hWndPE, PEP_szSUBTITLE, "");
	
	int i=0;
	for(int i = 0;i<PE_MAX_SUBSET;i++)
	{
		CString str;
		str.Format("Ch%d",i);
		PEvsetcell(m_hWndPE, PEP_szaSUBSETLABELS, i, (LPSTR)(LPCTSTR)str);		
	}
	
	PEnset(m_hWndPE, PEP_nFONTSIZE, PEFS_SMALL);//전체적으로 글자체를 작게 한다.	
	PEnset(m_hWndPE, PEP_bDATASHADOWS, FALSE);//음영
	
	PEnset(m_hWndPE, PEP_nPOINTSTOGRAPH, 20);//lmh x축 간격  // 0504 SSL
	PEnset(m_hWndPE, PEP_nTARGETPOINTSTOTABLE, 20);

	PEnset(m_hWndPE, PEP_nPOINTSTOGRAPHINIT, PEPTGI_LASTPOINTS);
	PEnset(m_hWndPE, PEP_bNORANDOMPOINTSTOGRAPH, TRUE);
	PEnset(m_hWndPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
	PEnset(m_hWndPE, PEP_bFORCERIGHTYAXIS, FALSE);
	PEnset(m_hWndPE, PEP_nFORCEVERTICALPOINTS, FALSE);
	
	PEvsetcell(m_hWndPE, PEP_szaPOINTLABELS, 199, " ");
	
	//PEnset(m_hWndPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
	//double m = 0.0F;
	//PEvset(m_hWndPE, PEP_fMANUALMINY, &m, 0);
	//m = 100.0F;
	//PEvset(m_hWndPE, PEP_fMANUALMAXY, &m, 0);
	
	//PEnset(m_hWndPE, PEP_nMANUALSCALECONTROLRY, PEMSC_MINMAX);
	//m = 0.0F;
	//PEvset(m_hWndPE, PEP_fMANUALMINRY, &m, 0);
	///m = 100.0F;
	//PEvset(m_hWndPE, PEP_fMANUALMAXRY, &m, 0);
	
	int slt[PE_MAX_SUBSET];
	int mas[PE_MAX_SUBSET];		
	for(int i = 0; i<PE_MAX_SUBSET; i++)
	{		
		slt[i] = PELT_DOT;
		mas[i] = 1;		
	}	
	SetV(PEP_naSUBSETLINETYPES, slt, PE_MAX_SUBSET);
	SetV(PEP_naMULTIAXESSUBSETS, mas, PE_MAX_SUBSET);
	
	int oma = PE_MAX_SUBSET;
	SetV(PEP_naOVERLAPMULTIAXES, &oma, PE_MAX_SUBSET);
		
	////////////////////////////////////////////////////////////		
	PEnset(m_hWndPE, PEP_nPOINTSTOGRAPHINIT, PEPTGI_LASTPOINTS);				
	PEnset(m_hWndPE, PEP_bALLOWDATAHOTSPOTS, FALSE);
	PEnset(m_hWndPE, PEP_bALLOWSUBSETHOTSPOTS, FALSE);
	PEnset(m_hWndPE, PEP_bALLOWPOINTHOTSPOTS, FALSE);
	PEnset(m_hWndPE, PEP_bALLOWTABLEHOTSPOTS, FALSE);	
	PEnset(m_hWndPE, PEP_bALLOWHISTOGRAM, FALSE);
	PEnset(m_hWndPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
}

void CRealGraphWnd_V2::Fun_SubsetSetting(int id)
{	
	if(!m_GraphInfo) return;

	int i=0;
	int nItem=0;
	double min, max;
	CString strTemp;
			
	if(id==0)
	{//channel
		PEnset(m_hWndPE, PEP_nPOINTSTOGRAPH, m_GraphInfo->graphchxcount);;//lmh x축 간격  // 0504 SSL
		PEnset(m_hWndPE, PEP_nTARGETPOINTSTOTABLE, m_GraphInfo->graphchxcount);
		
		for(int i = 0;i<4;i++)
		{
			if(m_GraphInfo->ChInfo[i].bCheck==FALSE) continue;

			PEnset(m_hWndPE, PEP_nWORKINGAXIS, nItem);
			
			strTemp.Format("%s",m_GraphInfo->ChInfo[i].cUnit);
			
			min = m_GraphInfo->ChInfo[i].fMin;
			max = m_GraphInfo->ChInfo[i].fMax;
		
			if((i%2)==1)
			{//right
				SetN(PEP_nMANUALSCALECONTROLRY, PEMSC_MINMAX);
				SetV(PEP_fMANUALMINRY, &min, nItem);
				SetV(PEP_fMANUALMAXRY, &max, nItem);
			}
			else
			{//left
				SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
				SetV(PEP_fMANUALMINY, &min, nItem);
				SetV(PEP_fMANUALMAXY, &max, nItem);
			}			
			
			DWORD dw = m_GraphInfo->ChInfo[i].lineColor;	
			PEvsetcell(m_hWndPE, PEP_dwaSUBSETCOLORS, nItem, &dw);
		
			nItem=nItem+1;
		}	
	}
	else if(id==1 && g_GraphInfo.nAuxCount>0)
	{//aux			
		PEnset(m_hWndPE, PEP_nPOINTSTOGRAPH, m_GraphInfo->graphauxxcount);//lmh x축 간격  // 0504 SSL
		PEnset(m_hWndPE, PEP_nTARGETPOINTSTOTABLE, m_GraphInfo->graphauxxcount);

		for(int i = 0;i<g_GraphInfo.nAuxCount;i++)
		{
			if(m_GraphInfo->AuxInfo[i].bCheck==FALSE) continue;
			
			SetN(PEP_nWORKINGAXIS, nItem);			
			
			strTemp.Format("%s",m_GraphInfo->AuxInfo[i].cUnit);
			
			min = m_GraphInfo->AuxInfo[i].fMin;
			max = m_GraphInfo->AuxInfo[i].fMax;
			
			if((i%2)==1)
			{//right
				SetN(PEP_nMANUALSCALECONTROLRY, PEMSC_MINMAX);
				SetV(PEP_fMANUALMINRY, &min, nItem);
				SetV(PEP_fMANUALMAXRY, &max, nItem);
			}
			else
			{//left
				SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);			
				SetV(PEP_fMANUALMINY, &min, nItem);			
				SetV(PEP_fMANUALMAXY, &max, nItem);			
			}
			
			DWORD dw = m_GraphInfo->AuxInfo[i].lineColor;	
			PEvsetcell(m_hWndPE, PEP_dwaSUBSETCOLORS, nItem, &dw);								
						
			nItem=nItem+1;
		}
	}	
	else if(id==2 && g_GraphInfo.nCanCount>0)
	{//can
		PEnset(m_hWndPE, PEP_nPOINTSTOGRAPH, m_GraphInfo->graphcanxcount);//lmh x축 간격  // 0504 SSL
		PEnset(m_hWndPE, PEP_nTARGETPOINTSTOTABLE, m_GraphInfo->graphcanxcount);

		for(int i = 0;i<g_GraphInfo.nCanCount;i++)
		{
			if(m_GraphInfo->CanInfo[i].bCheck==FALSE) continue;
			
			SetN(PEP_nWORKINGAXIS, nItem);			
		
			strTemp.Format("%s",m_GraphInfo->CanInfo[i].cUnit);
			
			min = m_GraphInfo->CanInfo[i].fMin;
			max = m_GraphInfo->CanInfo[i].fMax;
			
			if((i%2)==1)
			{//right
				SetN(PEP_nMANUALSCALECONTROLRY, PEMSC_MINMAX);
				SetV(PEP_fMANUALMINRY, &min, nItem);
				SetV(PEP_fMANUALMAXRY, &max, nItem);
			}
			else
			{//left
				SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);			
				SetV(PEP_fMANUALMINY, &min, nItem);			
				SetV(PEP_fMANUALMAXY, &max, nItem);			
			}

			DWORD dw = m_GraphInfo->CanInfo[i].lineColor;	
			PEvsetcell(m_hWndPE, PEP_dwaSUBSETCOLORS, nItem, &dw);					
		
			nItem=nItem+1;
		}		
	}
    else if(id==3)
	{//chamber	
		
		PEnset(m_hWndPE, PEP_nPOINTSTOGRAPH, m_GraphInfo->graphchamxcount);//lmh x축 간격  // 0504 SSL
		PEnset(m_hWndPE, PEP_nTARGETPOINTSTOTABLE, m_GraphInfo->graphchamxcount);

		for(int i = 0;i<4;i++)
		{
			if(m_GraphInfo->ChamInfo[i].bCheck==FALSE) continue;
			
			SetN(PEP_nWORKINGAXIS, nItem);			
			
			strTemp.Format("%s",m_GraphInfo->ChamInfo[i].cUnit);
			
			min = m_GraphInfo->ChamInfo[i].fMin;
			max = m_GraphInfo->ChamInfo[i].fMax;
			
			if((i%2)==1)
			{//right
				SetN(PEP_nMANUALSCALECONTROLRY, PEMSC_MINMAX);
				SetV(PEP_fMANUALMINRY, &min, nItem);
				SetV(PEP_fMANUALMAXRY, &max, nItem);
			}
			else
			{//left
				SetN(PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);			
				SetV(PEP_fMANUALMINY, &min, nItem);			
				SetV(PEP_fMANUALMAXY, &max, nItem);			
			}

			DWORD dw = m_GraphInfo->ChamInfo[i].lineColor;	
			PEvsetcell(m_hWndPE, PEP_dwaSUBSETCOLORS, nItem, &dw);									
			
			nItem=nItem+1;			
		}
	}
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CRealGraphWnd_V2::SetYAxisLabel(int subID, CString str)
{	
	SetN(PEP_nWORKINGAXIS, subID);
	SetSZ(PEP_szYAXISLABEL, (LPSTR)(LPCTSTR) str);	
	SetSZ(PEP_szRYAXISLABEL, (LPSTR)(LPCTSTR)str);	

	if((subID%2)==1)
	{//if odd,right label		
		PEnset(m_hWndPE, PEP_nRYAXISCOMPARISONSUBSETS, 1); 
	}
}

void CRealGraphWnd_V2::SetDataTitle(int nSubID, CString str)
{
	SetVCell(PEP_szaSUBSETLABELS, nSubID, (LPSTR)(LPCTSTR) str);
}

void CRealGraphWnd_V2::SetXAxisLabel(CString str)
{
	SetSZ(PEP_szXAXISLABEL,(LPSTR)(LPCTSTR) str);
}

void CRealGraphWnd_V2::ShowSubset(int id, int SetCnt, BOOL bShow)
{
	//int nArray[PE_MAX_SUBSET]={0};
	//nArray[id]=1;
	//PEvset (m_hWndPE, PEP_naRANDOMSUBSETSTOGRAPH, nArray, m_iYCount);

	//nArray[id] = id; // third subset	
	//nArray[1] = 1; // fifth subset	
	//nArray[2] = 2; // fifth subset	
	//nArray[3] = 3; // fifth subset	
	//PEvset (m_hWndPE, PEP_naRANDOMSUBSETSTOGRAPH, nArray, m_iYCount);


	m_bShowArray[id] = bShow;
	int	nArray[PE_MAX_SUBSET];
	int num=0;
	for(int i=0;i<SetCnt;i++)
	{
		//if(m_bShowArray[i])
		{
			nArray[num] = i;
			num++;
		}
	}
		
	SetV(PEP_naRANDOMSUBSETSTOGRAPH, nArray, num);			
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

BOOL CRealGraphWnd_V2::ClearGraph()
{	
	PEreinitialize(m_hWndPE);
	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);

	return TRUE;
}

BOOL CRealGraphWnd_V2::RefreshGraph()
{	
	if(PEresetimage(m_hWndPE, 0, 0)==0)
	{			
		return FALSE;
	}
		
	::InvalidateRect(m_hWndPE, NULL, FALSE);
	return TRUE;
}
	
BOOL CRealGraphWnd_V2::Start(int iNo)
{	
	BOOL bflag=RefreshGraph();

	m_iSelectGraph = iNo;
	
	if(iNo == 0)      SetTimer(iNo,  (int)m_GraphInfo->graphtime1* 1000, 0);//channel
	else if(iNo == 1) SetTimer(iNo,  (int)m_GraphInfo->graphtime2* 1000, 0);//aux
	else if(iNo == 2) SetTimer(iNo,  (int)m_GraphInfo->graphtime2* 1000, 0);//can
	else if(iNo == 3) SetTimer(iNo,  (int)m_GraphInfo->graphtime3* 1000, 0);//chamber

	return bflag;
}

void CRealGraphWnd_V2::Stop()
{
	KillTimer(0);
	KillTimer(1);
	KillTimer(2);
	KillTimer(3);
}

void CRealGraphWnd_V2::OnTimer(UINT nIDEvent) 
{
	RECT rt;
	GetWindowRect(&rt);
	
	switch(nIDEvent)
	{
		case 0:				    
				Fun_DrawChannelGraph();					
				break;			
 		case 1:				
				Fun_DrawAuxGraph();								
 				break;
		case 2 :
			    Fun_DrawCanGraph();				
				break;		
 	 	case 3 :
				Fun_DrawChamberGraph();				
				break;				
	}
	
	CStatic::OnTimer(nIDEvent);
}

void CRealGraphWnd_V2::OnDestroy() 
{
	CStatic::OnDestroy();
	
	PEdestroy(m_hWndPE);
}

BOOL CRealGraphWnd_V2::OnEraseBkgnd(CDC* pDC) 
{
	return FALSE;
}

void CRealGraphWnd_V2::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

//*********** Functions for Graph ******************************
void CRealGraphWnd_V2::SetN(UINT type, INT value)
{
	PEnset(m_hWndPE, type, value);
}

void CRealGraphWnd_V2::SetV(UINT type, VOID FAR * lpData, UINT nItems)
{
	PEvset(m_hWndPE, type, lpData, nItems);
}

void CRealGraphWnd_V2::SetVCell(UINT type, UINT nCell, VOID FAR * lpData)
{
	PEvsetcell(m_hWndPE, type, nCell, lpData);
}

void CRealGraphWnd_V2::SetVCellEx(UINT type, int subset, UINT nCell, VOID FAR *lpData)
{
	PEvsetcellEx(m_hWndPE, type, subset, nCell, lpData);
}

void CRealGraphWnd_V2::SetSZ(UINT type, CHAR FAR *str)
{
	PEszset(m_hWndPE, type, str);
}

void CRealGraphWnd_V2::Print()
{
	PElaunchprintdialog(m_hWndPE, TRUE, NULL);
}

void CRealGraphWnd_V2::SetData(int subset, int pos, LPCTSTR xData, double yData)
{
	float data = (float) yData;
	PEvsetcellEx(m_hWndPE, PEP_faYDATA, subset, pos, &data);
}

void CRealGraphWnd_V2::SetMaximize()
{
	PElaunchmaximize(m_hWndPE);
}

void CRealGraphWnd_V2::OnSize(UINT nType, int cx, int cy) 
{
	CStatic::OnSize(nType, cx, cy);
	
	RECT r;
	if(::IsWindow(this->GetSafeHwnd()))
	{
		::GetClientRect(m_hWnd, &r);
		::MoveWindow(m_hWndPE, 0, 0, r.right, r.bottom, TRUE);
	}	
}

void CRealGraphWnd_V2::SetMainTitle(CString str)
{	
	SetSZ(PEP_szMAINTITLE, (LPSTR) (LPCTSTR) str);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CRealGraphWnd_V2::SetSubTitle(CString str)
{
	SetSZ(PEP_szSUBTITLE, (LPSTR) (LPCTSTR)  str);
	::InvalidateRect(m_hWndPE, NULL, FALSE);
}

void CRealGraphWnd_V2::SaveGraph()
{
	PElaunchexport(m_hWndPE);	
}

void CRealGraphWnd_V2::CopyImg()
{
	PEcopybitmaptoclipboard(m_hWndPE, NULL);
}

void CRealGraphWnd_V2::CustomizeDialog()
{
	PElaunchcustomize(m_hWndPE);
}

BOOL CRealGraphWnd_V2::Fun_DrawChannelGraph()
{	
	if(!m_GraphInfo) return FALSE;
    if(m_iYCount<1) return FALSE;

	CString strTemp;
	float fData[4];
	ZeroMemory(fData,sizeof(fData));
		
	int nItem=0;
	float fvalue=0;

	ConverChData(m_pRealChData->chData);

	for(int i=0;i<4;i++)
	{
		if(m_GraphInfo->ChInfo[i].bCheck == 1)
		{
			if(i==0)      m_fCh[0] = (float)(m_pRealChData->chData.lVoltage *0.001);  
			else if(i==1) m_fCh[1] = (float)(m_pRealChData->chData.lCurrent *0.001);  
			else if(i==2) m_fCh[2] = (float)(m_pRealChData->chData.lWatt    *0.001);  
			else if(i==3) m_fCh[3] = (float)(m_pRealChData->chData.lCapacitance *0.001); 
						
			if(i==0)      fvalue = (float)m_pRealChData->chData.lVoltage;  
			else if(i==1) fvalue = (float)m_pRealChData->chData.lCurrent;  
			else if(i==2) fvalue = (float)m_pRealChData->chData.lWatt;  
			else if(i==3) fvalue = (float)m_pRealChData->chData.lCapacitance;  
		
			//fvalue=(float)(rand() % 100000)*10000;//random

			if(m_GraphInfo->ChInfo[i].cUnit[0] == 'm') fData[nItem] = (float)(fvalue / 1000.0); 
			else                                       fData[nItem] = (float)(fvalue / 1000000.0); 
			
			nItem=nItem+1;
		}
	}
	
	PEvset(m_hWndPE, PEP_faAPPENDYDATA, &fData, 1);
	
 	chtime = COleDateTime::GetCurrentTime();
	strTemp.Format("%s", chtime.Format("%H:%M:%S"));
		
	SetV(PEP_szaAPPENDPOINTLABELDATA, (void FAR*) (const char*) strTemp, 1);
	
	PEresetimage(m_hWndPE, 0, 0);	
	return TRUE;
}

bool  CRealGraphWnd_V2::Fun_DrawAuxGraph()
{
	if(!m_GraphInfo) return false;
	if(m_iYCount<1) return false;

	CString strTemp;
	float fData[PE_MAX_SUBSET];
	ZeroMemory(fData,sizeof(fData));

	int i=0;
	int nItem=0;
	float fvalue=0;

	if(m_GraphInfo->nAuxCount>0)
	{	
		for(int i = 0;i<m_GraphInfo->nAuxCount;i++)
		{	
			if(m_GraphInfo->AuxInfo[i].bCheck == 1)
			{			
				if(nItem==0)      m_fAuxCan[nItem] = (float)(m_pRealChData->auxData[i].lValue*0.001);  
				else if(nItem==1) m_fAuxCan[nItem] = (float)(m_pRealChData->auxData[i].lValue*0.001); 
				else if(nItem==2) m_fAuxCan[nItem] = (float)(m_pRealChData->auxData[i].lValue*0.001);
				else if(nItem==3) m_fAuxCan[nItem] = (float)(m_pRealChData->auxData[i].lValue*0.001);  
				else if(nItem==4) m_fAuxCan[nItem] = (float)(m_pRealChData->auxData[i].lValue*0.001); 
				else if(nItem==5) m_fAuxCan[nItem] = (float)(m_pRealChData->auxData[i].lValue*0.001);
			
				if(nItem==0)      fvalue = (float)m_pRealChData->auxData[i].lValue;  
				else if(nItem==1) fvalue = (float)m_pRealChData->auxData[i].lValue;  
				else if(nItem==2) fvalue = (float)m_pRealChData->auxData[i].lValue;  
				else if(nItem==3) fvalue = (float)m_pRealChData->auxData[i].lValue;  
				else if(nItem==4) fvalue = (float)m_pRealChData->auxData[i].lValue;  
				else if(nItem==5) fvalue = (float)m_pRealChData->auxData[i].lValue;  
			
				//fvalue=(float)(rand() % 10000)*1000000;//random

				/*if(m_GraphInfo->AuxInfo[i].cUnit[0] == 'm') fData[nItem] = (float)(fvalue / 1000.0); 
				else                                        fData[nItem] = (float)(fvalue / 1000000.0); */

				//ksj 20210118
				CString strUnit = m_GraphInfo->AuxInfo[i].cUnit;
				if(m_GraphInfo->AuxInfo[i].cUnit[0] == 'm')
				{
					fData[nItem] = (float)(fvalue / 1000.0); 
				}
				else
				{
					if(strUnit.Compare("AuxT") == 0)
						fData[nItem] = (float)(fvalue / 1000.0); 
					else if(strUnit.Compare("AuxTH") == 0)
						fData[nItem] = (float)(fvalue / 1000000.0);
					else
						fData[nItem] = (float)(fvalue / 1000000.0);
				}
				//ksj end
			
				nItem=nItem+1;
			}
		}	
	}
	PEvset(m_hWndPE, PEP_faAPPENDYDATA, &fData, 1);
	
	auxtime = COleDateTime::GetCurrentTime();
	strTemp.Format("%s", auxtime.Format("%H:%M:%S"));
	
	SetV(PEP_szaAPPENDPOINTLABELDATA, (void FAR*) (const char*) strTemp, 1);
	PEresetimage(m_hWndPE, 0, 0);	
	return true;
}	


bool CRealGraphWnd_V2::Fun_DrawCanGraph()
{
	if(!m_GraphInfo) return false;
	if(m_iYCount<1) return false;

	CString strTemp;
	float fData[PE_MAX_SUBSET];
	ZeroMemory(fData,sizeof(fData));

	int i=0;
	int nItem=0;
	float fvalue=0;
	
	float foffset=0;
	float ffactor=0;

	if(m_GraphInfo->nCanCount>0)
	{
		for(int i = 0;i<m_GraphInfo->nCanCount;i++)
		{
			foffset=0;
			ffactor=0;
			if(m_GraphInfo->CanInfo[i].bCheck == 1)
			{			
				if(PE_MAX_SUBSET<=nItem) continue;

				if(m_GraphInfo->CanInfo[i].sOffset!="") foffset=(float)atof(m_GraphInfo->CanInfo[i].sOffset);
				if(m_GraphInfo->CanInfo[i].sFactor!="") ffactor=(float)atof(m_GraphInfo->CanInfo[i].sFactor);

				if(m_pRealChData->canData->data_type==0) fvalue=m_pRealChData->canData->canVal.fVal[i];
				if(m_pRealChData->canData->data_type==1) fvalue=m_pRealChData->canData->canVal.fVal[i];
				if(m_pRealChData->canData->data_type==2) fvalue=m_pRealChData->canData->canVal.fVal[i];
				if(m_pRealChData->canData->data_type==3) fvalue=m_pRealChData->canData->canVal.fVal[i];
				
				if(nItem==0)      m_fAuxCan[nItem] = fvalue+foffset-ffactor;  
				else if(nItem==1) m_fAuxCan[nItem] = fvalue+foffset-ffactor;  
				else if(nItem==2) m_fAuxCan[nItem] = fvalue+foffset-ffactor;  
				else if(nItem==3) m_fAuxCan[nItem] = fvalue+foffset-ffactor;  
				else if(nItem==4) m_fAuxCan[nItem] = fvalue+foffset-ffactor;  
				else if(nItem==5) m_fAuxCan[nItem] = fvalue+foffset-ffactor;  
				else break;
				
				fData[nItem]=m_fAuxCan[nItem];

				//fData[nItem]=(float)(rand() % 80)+foffset-ffactor;//random
				
				nItem=nItem+1;
			}
		}
	}
	PEvset(m_hWndPE, PEP_faAPPENDYDATA, &fData, 1);
	
	auxtime = COleDateTime::GetCurrentTime();
	strTemp.Format("%s", auxtime.Format("%H:%M:%S"));
	
	SetV(PEP_szaAPPENDPOINTLABELDATA, (void FAR*) (const char*) strTemp, 1);
	PEresetimage(m_hWndPE, 0, 0);	
	return true;
}	

bool  CRealGraphWnd_V2::Fun_DrawChamberGraph()
{
	if(!m_GraphInfo) return false;
    if(m_iYCount<1) return false;

	CString strTemp;
	float fData[4];
	ZeroMemory(fData,sizeof(fData));
	
	int i=0;
	int nItem=0;
	for(int i = 0;i<4;i++)
	{	
		if(m_GraphInfo->ChamInfo[i].bCheck == 1)
		{
			if(nItem==0)      m_fCham[nItem] = (float)g_lchamberdata1;
			else if(nItem==1) m_fCham[nItem] = (float)g_lchamberdata2;
			else if(nItem==2) m_fCham[nItem] = (float)g_lchamberdata3;
			else if(nItem==3) m_fCham[nItem] = (float)g_lchamberdata4;
			
			//m_fCham[nItem]=(float)(rand() % 80);//random

			fData[nItem] = m_fCham[nItem]; 
			
			nItem=nItem+1;
		}
	}
	
	PEvset(m_hWndPE, PEP_faAPPENDYDATA, &fData, 1);
	
	chamtime = COleDateTime::GetCurrentTime();
	strTemp.Format("%s", chamtime.Format("%H:%M:%S"));
	
	SetV(PEP_szaAPPENDPOINTLABELDATA, (void FAR*) (const char*) strTemp, 1);
	PEresetimage(m_hWndPE, 0, 0);	
	return true;
}


//convert data to run mode
void CRealGraphWnd_V2::ConverChData(SFT_CH_DATA &chData)
{
	//if(m_nRunMode == 0 )	//
	{
		if( chData.chStepType == PS_STEP_CHARGE		|| 
			chData.chStepType == PS_STEP_DISCHARGE	|| 
			chData.chStepType == PS_STEP_PATTERN	||
			chData.chStepType == PS_STEP_EXT_CAN	||			//20121119 add		
			(chData.chStepType == PS_STEP_IMPEDANCE && chData.chMode == PS_MODE_DCIMP)
			)
		{
		}
		else
		{
			//			chData.lCapacity = 0;
			chData.lChargeAh	= 0;
			chData.lDisChargeAh	= 0;
			chData.lCapacitance	= 0;
			
			chData.lCurrent = 0;
			chData.lWatt = 0;
			
			//			chData.lWattHour = 0;
			chData.lChargeWh = 0;
			chData.lDisChargeWh = 0;
			
			chData.lAvgVoltage = 0;
			chData.lAvgCurrent = 0;
		}
		
		if(chData.chState == PS_STATE_IDLE || chData.chState == PS_STATE_STANDBY)
		{
			chData.lCurrent = 0;
			chData.lWatt = 0;
		}
		
		if(chData.lImpedance > 1000000000 || chData.lImpedance < 0)	//0 ~ 1KOhm
			chData.lImpedance = 0;
	}
}
