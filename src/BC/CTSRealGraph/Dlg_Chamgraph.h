#if !defined(AFX_DLG_CHAMGRAPH_H__AA7C3CCF_CEC7_4696_9D20_725B1BBE0435__INCLUDED_)
#define AFX_DLG_CHAMGRAPH_H__AA7C3CCF_CEC7_4696_9D20_725B1BBE0435__INCLUDED_

#include "GraphWnd.h"
#include "RealGraphWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Chamgraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chamgraph dialog

class Dlg_Chamgraph : public CDialog
{
// Construction
public:
	Dlg_Chamgraph(CWnd* pParent = NULL);   // standard constructor
	
	BOOL m_bStartDraw;
	
	CRealGraphWnd_V2 m_wndGraphChamber;
	
	BOOL Fun_DrawFrame();
	void Fun_HideGraph();
	BOOL Fun_InitGraph();

	void onGraphMax();
	BOOL onStart();
	void onStop();
	

// Dialog Data
	//{{AFX_DATA(Dlg_Chamgraph)
	enum { IDD = IDD_DIALOG_CHAMGRAPH };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Chamgraph)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Chamgraph)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern Dlg_Chamgraph *mainTabCham;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CHAMGRAPH_H__AA7C3CCF_CEC7_4696_9D20_725B1BBE0435__INCLUDED_)
