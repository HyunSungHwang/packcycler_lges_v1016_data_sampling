/*

BOOL CRealGraphWnd_V2::SetYGraph()
{
/*	int *nArray=new int[m_iYCount]; //[PE_MAX_SUBSET]={0};	
	nArray[0] = 0; // third subset	
	nArray[1] = 1; // fifth subset	
	nArray[2] = 2; // fifth subset	
	nArray[3] = 3; // fifth subset	

	for(int i=0;i<m_iYCount;i++)
	{
	//nArray[i]=i;
	}
	PEvset (m_hWndPE, PEP_naRANDOMSUBSETSTOGRAPH, nArray, m_iYCount);

	//int nItem=0;
	/*for(int i=0;i<4;i++)
	{
		if(m_GraphInfo->ChInfo[i].bCheck == 1)
		{		
			nArray[nItem]=nItem;
			nItem=nItem+1;
		}
	}
	if(PEvset (m_hWndPE, PEP_naRANDOMSUBSETSTOGRAPH, nArray, PE_MAX_SUBSET)==0) AfxMessageBox("e1");

		*/
	/*int *slt=new int[m_iYCount];
	int *mas=new int[m_iYCount];		
	int *oma=new int[m_iYCount];		

	for(int i = 0; i<m_iYCount; i++)
	{		
		slt[i] = PELT_DOT;
		mas[i] = 1;	
		oma[i] = 1;	
	}	
	SetV(PEP_naSUBSETLINETYPES, slt, m_iYCount);
	SetV(PEP_naMULTIAXESSUBSETS, mas, m_iYCount);

	//int oma = m_iYCount;
	//SetV(PEP_naOVERLAPMULTIAXES, &oma, m_iYCount);

	//int oma[3] = {1, 1, 1};
	PEvset(m_hWndPE, PEP_naOVERLAPMULTIAXES, oma, m_iYCount);*/

//	return TRUE;
//}

  void CRealGraphWnd_V2::SetAnnotation(BOOL bEnable)
  {
  PEnset(m_hWndPE, PEP_bSHOWANNOTATIONS, bEnable);				//Annotation을 사용 한다.
  PEnset(m_hWndPE, PEP_bALLOWANNOTATIONCONTROL, bEnable);			//사용자가 선택 가능 
  PEnset(m_hWndPE, PEP_nLINEANNOTATIONTEXTSIZE, 100);				//Annotation을 보여 준다.
  }

CString CRealGraphWnd_V2::Fun_getAppPath()
{
HMODULE hModule=::GetModuleHandle(NULL); // handle of current module

  CString strExeFileName;
  VERIFY(::GetModuleFileName(hModule, strExeFileName.GetBuffer(_MAX_PATH),_MAX_PATH));
  
    char Drive[_MAX_DRIVE];
    char Path[_MAX_PATH];
	
	  _splitpath(strExeFileName, Drive, Path, NULL,NULL);//Filename, Ext);
	  
		return CString(Drive)+CString(Path); // has trailing backslash
		}

void CRealGraphWnd_V2::AddXAnnotation(float fXData1, float fXData2, CString str1, CString str2)
{
	double	m_dVLA[2];							//Point
	int		m_nVLAT[2];						//Line Style
	long	m_lVLAC[2];						//Line Color
	char szBuff[64];

	sprintf(szBuff, "|t%s\t|t%s\t", str1, str2);
	m_dVLA[0] = fXData1;
	m_dVLA[1] = fXData2;
	m_nVLAT[0] = PELT_THINSOLID;
	m_nVLAT[1] = PELT_THINSOLID;
	m_lVLAC[0] = ANNOTATION_COLOR;
	m_lVLAC[1] = ANNOTATION_COLOR;

	//Vertical line annotation
	PEvset(m_hWndPE, PEP_faVERTLINEANNOTATION, m_dVLA, 2);
//	PEvset(m_hWndPE, PEP_szaVERTLINEANNOTATIONTEXT, szBuff, 2);
	PEvset(m_hWndPE, PEP_naVERTLINEANNOTATIONTYPE, m_nVLAT, 2);
	PEvset(m_hWndPE, PEP_dwaVERTLINEANNOTATIONCOLOR, m_lVLAC, 2);


	//X축 Annotation
	sprintf(szBuff, "%s", str1);
	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 0, &m_dVLA[0]);
	PEvsetcell(m_hWndPE, PEP_szaXAXISANNOTATIONTEXT, 0, szBuff);
	PEvsetcell(m_hWndPE, PEP_dwaXAXISANNOTATIONCOLOR, 0, &m_lVLAC[0]);
	sprintf(szBuff, "%s", str2);
	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 1, &m_dVLA[1]);
	PEvsetcell(m_hWndPE, PEP_szaXAXISANNOTATIONTEXT, 1, szBuff);
	PEvsetcell(m_hWndPE, PEP_dwaXAXISANNOTATIONCOLOR, 1, &m_lVLAC[1]);
	PEnset(m_hWndPE, PEP_nAXESANNOTATIONTEXTSIZE, 80);
		

//	PEvsetcell(m_hWndPE, PEP_faXAXISANNOTATION, 0, szBuff);
//	PEnset(m_hWndPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);

 PEnset(m_hWndPE, PEP_bSHOWGRAPHANNOTATIONS, TRUE);					//Annotation을 사용 한다.
	double GAX[15] ={	70.0F, 93.0F, 70.0F, 170.0F, 195.0F, 170.0F,
                        270.0F, 295.0F, 270.0F,  370.0F, 395.0F, 370.0F,    
                        470.0F, 495.0F, 470.0F	};

    double GAY[15] = {	280.0F, 115.0F, 280.0F, 280.0F, 115.0F, 280.0F,
                        280.0F, 115.0F, 280.0F, 280.0F, 115.0F, 280.0F,
                        280.0F, 115.0F, 280.0F	};

    int GAT[15] = {		PEGAT_LINECONTINUE, PEGAT_DOWNTRIANGLESOLID, PEGAT_DOWNTRIANGLESOLID, 
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK,
                        PEGAT_TOPLEFT, PEGAT_BOTTOMRIGHT, PEGAT_ROUNDRECT_THICK};
    long GAC[15] = {	0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0),
                        0L, 0L, RGB(0, 255, 0)	};

	double GAX[15]	= {	 70.0F,  93.0F,  70.0F	};
    double GAY[15]	= {	280.0F, 115.0F, 280.0F	};
    int GAT[15]		= {	PEGAT_TOPLEFT, 	PEGAT_BOTTOMRIGHT,	PEGAT_LINECONTINUE};
    long GAC[15]	= {	0L, 0L, RGB(0,255,0)	};
	
	PEvset(m_hWndPE, PEP_faGRAPHANNOTATIONX, GAX, 3);
	PEvset(m_hWndPE, PEP_faGRAPHANNOTATIONY, GAY, 3);
	PEvset(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, GAT,3);
	PEvset(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, GAC, 3);

	int LAT[2] = {PEGAT_MEDIUMSOLIDLINE};
	long LAC[2] = {RGB(0, 0, 255)};
	char LAText[] = "Test Area\t";
	PEvset(m_hWndPE, PEP_naLEGENDANNOTATIONTYPE, LAT, 1);
	PEvset(m_hWndPE, PEP_dwaLEGENDANNOTATIONCOLOR, LAC, 1);
	PEvset(m_hWndPE, PEP_szaLEGENDANNOTATIONTEXT, LAText, 1);
            
	::InvalidateRect(m_hWndPE, NULL, FALSE);	
}

void CRealGraphWnd_V2::AddLineAnnotation(int nIndex, float fStartX, float fStartY, float fEndX, float fEndY, CString str)
{
    int nType = PEGAT_MEDIUMSOLIDLINE;
	DWORD color = ANNOTATION_COLOR;
	double fx, fy;

	//PEnset(m_hWndPE, PEP_bSHOWGRAPHANNOTATIONS, TRUE);					//Annotation을 사용 한다.

	fx = fStartX;
	fy = fStartY;
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONX, nIndex*2, &fx);
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONY, nIndex*2, &fy);
	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, nIndex*2, &color);
	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, nIndex*2, &nType);
	nType = PEGAT_LINECONTINUE;
	fx = fEndX;
	fy = fEndY;
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONX, nIndex*2+1, &fx);
	PEvsetcell(m_hWndPE, PEP_faGRAPHANNOTATIONY, nIndex*2+1, &fy);
	PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR, nIndex*2+1, &color);
	PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, nIndex*2+1, &nType);

	char szBuff[64];
	ZeroMemory(szBuff, sizeof(szBuff));
	sprintf(szBuff, "%s", str);

	PEvsetcell(m_hWndPE, PEP_szaGRAPHANNOTATIONTEXT, nIndex*2, szBuff);
	PEvsetcell(m_hWndPE, PEP_szaGRAPHANNOTATIONTEXT, nIndex*2+1, "");
	PEnset(m_hWndPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);

	//PEvsetcell(m_hWndPE, PEP_naGRAPHANNOTATIONTYPE, m_nVLAT, 2);
	//PEvsetcell(m_hWndPE, PEP_dwaGRAPHANNOTATIONCOLOR,nIndex*2, &color);

	::InvalidateRect(m_hWndPE, NULL, FALSE);	
}

  void CRealGraphWnd_V2::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
  {
  if(nChar == 27 || nChar == VK_RETURN)
  {
		SetN(PEP_bZOOMMODE, FALSE);
		PEresetimage(m_hWndPE, 0, 0);
		}
		CStatic::OnChar(nChar, nRepCnt, nFlags);
		}

BOOL CRealGraphWnd_V2::OnCommand(WPARAM wp, LPARAM lp)
{
    HOTSPOTDATA HSData;

    switch (HIWORD(wp))
    {
        case PEWN_CLICKED:
            PEvget(m_hWndPE, PEP_structHOTSPOTDATA, &HSData);
            if (HSData.nHotSpotType == PEHS_SUBSET)
            {
				
                char szTmp[128];
                char szSubset[48];
                szTmp[0] = 0;
                strcat(szTmp, "You clicked subset ");
                sprintf(szSubset, "%i", HSData.w1);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".  Subset label is ");
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, szSubset);
                strcat(szTmp, ".");
                MessageBox(szTmp, "Subset Drill Down", MB_OK | MB_ICONINFORMATION);
				
            } 
            else if (HSData.nHotSpotType == PEHS_POINT)
            {
				
                if (PECONTROL_PIE != PEnget(m_hWndPE, PEP_nOBJECTTYPE))
                {
                    char szTmp[128];
                    char szSubset[48];
                    szTmp[0] = 0;
                    strcat(szTmp, "You clicked point ");
                    sprintf(szSubset, "%i", HSData.w1);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".  Point label is ");
                    PEvgetcell(m_hWndPE, PEP_szaPOINTLABELS, (UINT) HSData.w1, szSubset);
                    strcat(szTmp, szSubset);
                    strcat(szTmp, ".");
                    MessageBox(szTmp, "Point Drill Down", MB_OK | MB_ICONINFORMATION);
                }    
                else
                 return CView::OnCommand(wp, lp);
				 
            }
            else if ((HSData.nHotSpotType == PEHS_TABLE) || (HSData.nHotSpotType == PEHS_DATAPOINT))
            {
				/*
                UINT offset = ((int) HSData.w1 * (int) PEnget(m_hWndPE, PEP_nPOINTS)) + (int) HSData.w2;
                char    szTmp[128];
                char    szSubset[128];
                char    szTmp2[48];
                float   fData;
                szTmp[0] = 0;
                PEvgetcell(m_hWndPE, PEP_szaSUBSETLABELS, (UINT) HSData.w1, szSubset);
                strcat(szTmp, "항목 : ");
                strcat(szTmp, szSubset);
                strcat(szTmp, "  데이타값 : ");
                PEvgetcell(m_hWndPE, PEP_faYDATA, offset, (LPVOID) &fData);
                sprintf(szTmp2, "%.4f", fData);             
                strcat(szTmp, szTmp2);
                MessageBox(szTmp, "선택된 항목 값", MB_OK | MB_ICONINFORMATION);
				
            }
            return TRUE;
    }
    return CStatic::OnCommand(wp, lp);
}
*/

//virtual BOOL OnCommand( WPARAM wParam, LPARAM lParam);


	/*for(int i=0; i<	PE_MAX_SUBSET; i++)
	{
		if(m_pData[i] != NULL)
		{
			delete[]	m_pData[i];
			m_pData[i] = NULL;
			m_nDataPointCount[i] = 0;
		}
	}*/
	//m_TotalPoint =0;

	/*if(PEreinitialize(m_hWndPE)==0)
	{
		AfxMessageBox("a");
		return FALSE;
	}	
		
	PEresetimage(m_hWndPE, 0, 0);
	::InvalidateRect(m_hWndPE, NULL, FALSE);*/
//m_bThreadStop = TRUE;	

//if(m_pGraphThread != NULL) m_pGraphThread->SuspendThread();

/*
UINT CRealGraphWnd_V2::DrawGraphThread(LPVOID lParam)
{		
CRealGraphWnd_V2* pMain = (CRealGraphWnd_V2*)lParam;
while (pMain->m_bThreadStop != TRUE)
{
switch(pMain->m_iSelectGraph)
{
case 0:
pMain->Fun_DrawChannelGraph();						
break;
case 1:
pMain->Fun_DrawAuxGraph();										
break;
case 2:
pMain->Fun_DrawCanGraph();										
break;
case 3:
pMain->Fun_DrawChamberGraph();							
break;
}
Sleep(100);
}
return 0;
}
*/

//	static UINT DrawGraphThread(LPVOID lParam);				

//CWinThread*	m_pGraphThread;
//m_pGraphThread = AfxBeginThread(DrawGraphThread, this);
	
//m_DrawChannelColor[nItem]=m_GraphInfo->ChInfo[i].lineColor;			
//SetV(PEP_dwaSUBSETCOLORS, m_DrawChannelColor, PE_MAX_SUBSET);		


//m_fAuxCan[nItem] = 0.0F;
//PEvsetcell(m_hWndPE, PEP_faYDATA, nItem, &m_fAuxCan[nItem]);
//::InvalidateRect(m_hWndPE, NULL, FALSE);

//CString str;
//str.Format("%f %f %f %f",g_lchamberdata1,g_lchamberdata2,g_lchamberdata3,g_lchamberdata4);
//GLog::log_Save("graph","chamber",str,NULL,TRUE);


/*
void CRealGraphWnd_V2::SetSplit(BOOL bSplit)
{
int oma[6];
float map[6];
if(bSplit)
{
float onesize = 1.0f / m_SetsetNum;
for(int j=0;j<m_SetsetNum;j++)
{
oma[j] = 1;
map[j] = onesize;
}
SetV(PEP_naOVERLAPMULTIAXES, oma, m_SetsetNum);
SetV(PEP_faMULTIAXESPROPORTIONS, map, m_SetsetNum);
SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_MEDIUM);
}
else
{
oma[0] = m_SetsetNum;
map[0] = 1.0f;
SetV(PEP_naOVERLAPMULTIAXES, oma, 1);
SetV(PEP_faMULTIAXESPROPORTIONS, map, 0);
SetN(PEP_nMULTIAXESSEPARATORS, PEMAS_NONE);
}
PEreinitialize(m_hWndPE);
::InvalidateRect(m_hWndPE, NULL, FALSE);
}
*/
//PEresetimage(m_hWndPE, 0, 0);
//::InvalidateRect(m_hWndPE, NULL, FALSE);



//PEnset(m_hWndPE, PEP_nTARGETPOINTSTOTABLE, 40);
//fData[nItem] = //rand() % 80;			
//fData[nItem] = m_fAuxCan[nItem]; //rand() % 80;
			
// rand() % 80; //


//m_DrawChannelColor[nItem]=m_GraphInfo->ChInfo[i].lineColor;			
//SetV(PEP_dwaSUBSETCOLORS, m_DrawChannelColor, PE_MAX_SUBSET);
		

//m_DrawAuxColor[nItem]=m_GraphInfo->AuxInfo[i].lineColor;			
//SetV(PEP_dwaSUBSETCOLORS, m_DrawAuxColor, PE_MAX_SUBSET);								
			
//m_DrawCanColor[nItem]=m_GraphInfo->CanInfo[i].lineColor;			
//SetV(PEP_dwaSUBSETCOLORS, m_DrawCanColor, PE_MAX_SUBSET);					

//m_DrawChamberColor[nItem]=m_GraphInfo->ChamInfo[i].lineColor;			
//SetV(PEP_dwaSUBSETCOLORS, m_DrawChamberColor, PE_MAX_SUBSET);					
						