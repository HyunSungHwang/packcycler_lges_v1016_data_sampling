// CyclerChannel.h: interface for the CCyclerChannel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CYCLERCHANNEL_H__F1065011_05CD_44C3_863C_837DF75454A2__INCLUDED_)
#define AFX_CYCLERCHANNEL_H__F1065011_05CD_44C3_863C_837DF75454A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "CalibrationData.h"
#include "ModuleIndexTable.h"
//#include "FtpDownLoad.h"

/*
//ADP KBH 2005/06/08
//Module에서 PC로 전송되는 정보를 파일로 저장하기 위한 구조체
typedef struct s_Ch_File_Save		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	long lVoltage;				// Result Data...
	long lCurrent;
	long lCapacity;
	long lWatt;
	long lWattHour;
	ULONG ulStepTime;			// 이번 Step 진행 시간
	ULONG ulTotalTime;			// 시험 Total 진행 시간
	long lImpedance;			// Impedance (AC or DC)
	long lTemparature;
	long lPressure;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	long	lAvgVoltage;
	long	lAvgCurrent;

	long	nIndexFrom;
	long	nIndexTo;
	
} CT_CH_SAVE_DATA, *LPCT_CH_SAVE_DATA;
*/
/*
typedef struct s_StepEndFileHeader
{
	char szStartTime[64];
	char szEndTime[64];
	char szSerial[64];
	char szUserID[32];
	char szDescript[128];
	char szBuff[128];
} CT_STEP_END_FILE_HEADER;

typedef struct s_Ch_StepEnd_Record		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			// 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fTemparature;
	float fPressure;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	float fAvgVoltage;
	float fAvgCurrent;

	long	nIndexFrom;
	long	nIndexTo;

	float fReserved[16];
	
} CT_STEP_END_RECORD, *LPCT_STEP_END_RECORD;

typedef struct s_RawFileHeader 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_RECORD_FILE_HEADER rsHeader;
} CT_RAW_FILE_HEADER;
*/


typedef struct s_Temp_File
{
	char szFilePath[256];
	char szLot[64];
	char szWorker[64];
	char szDescript[128];
	char szReserved[512];
} CT_TEMP_FILE_DATA;

//#define RAWDATA_FILE_COL_SIZE	6
//#define TABEL_FILE_COLUMN_HEAD 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temperature,Press,VoltageAverage,CurrentAverage,Sequence,"
//20080813 KBH
#define TABEL_FILE_COLUMN_HEAD 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temperature,Press,VoltageAverage,CurrentAverage,Sequence,ChargeAh,DisChargeAh,Capacitance,ChargeWh,DisChargeWh,CVTime,SyncDate,SyncTime,AccCycle"


#define SFTWM_WRITE_LOG	WM_USER+4000

//#define _ADP_RECORD_FILE_ID		5640
#define _ADP_RECORD_FILE_VER	0x1000

//2007/08/09 kjh Aux File Ver
//#define _PNE_AUX_FILE_VER		0x1000
#define _PNE_AUX_FILE_VER_OLD_1	0x1001	//20080104 kjh
#define _PNE_AUX_FILE_VER		0x1002	//20100622 ljb		//Aux Name 추가

//2007/10/18 kjh Can File Ver
#define _PNE_CAN_FILE_VER		0x1000
class CChannelSensor : public CObject
{
private:
	STF_MD_AUX_SET_DATA auxData;
//	CString strAuxName;
	int nMasterChannel;
	BOOL bInstall;

public:
	CChannelSensor()
	{
		ZeroMemory(&auxData, sizeof(STF_MD_AUX_SET_DATA));
		bInstall = FALSE;
		nMasterChannel = 0;
	}
	CChannelSensor(STF_MD_AUX_SET_DATA * aux = NULL, CString strName = "NULL", int nMasterCh = 0)
	{
		memcpy(&auxData, aux, sizeof(STF_MD_AUX_SET_DATA));
		this->nMasterChannel = nMasterCh;
	}
	CChannelSensor(CChannelSensor &Sersor)
	{
		memcpy(&auxData, &Sersor.GetAuxData(), sizeof(STF_MD_AUX_SET_DATA));
		this->nMasterChannel = Sersor.GetMasterChannel();
		this->bInstall = Sersor.GetInstall();
	}

	~CChannelSensor()
	{
	}
	void SetAuxName(CString strName)
	{
		sprintf(auxData.szName, strName);
	}
	CString GetAuxName()
	{
		CString strAuxName;
		strAuxName.Format("%s", this->auxData.szName);
		return strAuxName;
	}
	STF_MD_AUX_SET_DATA GetAuxData()
	{
		return this->auxData;
	}
	int GetAuxChannelNumber()
	{
		return auxData.auxChNo;
	}
	int GetAuxType()
	{
		return auxData.auxType;
	}
	void SetInstall(BOOL bInstall)
	{
		this->bInstall = bInstall;
	}
	int GetInstall()
	{
		return this->bInstall;
	}

	void SetAuxData(STF_MD_AUX_SET_DATA * aux)
	{
		memcpy(&auxData, aux, sizeof(STF_MD_AUX_SET_DATA));
	}

	void SetMasterChannel(int nMasterCh)
	{
		this->nMasterChannel = nMasterCh;
		auxData.chNo = nMasterCh;
	}

	int GetMasterChannel()
	{
		return this->nMasterChannel;
	}

	void RemoveAuxData()
	{
		auxData.lMaxData = 0;
		auxData.lMinData = 0;
		auxData.chNo = 0;
		auxData.lEndMaxData = 0;
		auxData.lEndMinData = 0;
		ZeroMemory(auxData.szName, sizeof(auxData.szName));
		bInstall = TRUE;
		nMasterChannel = 0;
	}
	
};

class CCyclerChannel  
{
public:
	void SetCanTransCount(int nCount);
	void	SetUseOvenMode(BOOL bMode = TRUE)	{	m_bUseOven = bMode;	}
	BOOL	GetUseOvenMode()	{	return m_bUseOven;	}		//Oven 과 연동되어 동작하는지 여부
	int GetChannelIndex()	{	return m_nChannelIndex;}
	int	GetModuleID()	{	return m_nModuleID;	}
	CString GetSyncDateTimeString();
	long GetSyncTime();
	long GetSyncDate();
	long GetAccCycleCount1();
	long GetAccCycleCount2();
	long GetAccCycleCount3();
	long GetAccCycleCount4();
	long GetAccCycleCount5();
	long GetMultiCycleCount1();
	long GetMultiCycleCount2();
	long GetMultiCycleCount3();
	long GetMultiCycleCount4();
	long GetMultiCycleCount5();
	long GetCVTime();
	long GetDisChargeWh();
	long GetChargeWh();
	long GetCapacitance();
	long GetDisChargeAh();
	long GetChargeAh();
	
	BOOL GetSaveDataD(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA & sCanData);
	BOOL RemakeCanPCIndexFile(CString strBackUpRawFile, CString strAuxTStepEndFile, CString strAuxVStepEndFile, CString strNewTableFile);
	BOOL RemakeAuxPCIndexFile(CString strBackUpRawFile, CString strStepEndFile, CString strCanSStepEndFile, CString strNewTableFile);
	int WriteRestoreCanFile (CString strMFileName, CString strSFileName, FILE *fpDestFile, PS_CAN_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo);
	int WriteRestoreAuxFile (CString strTFileName, CString strVFileName, FILE *fpDestFile, PS_AUX_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo);
	int WriteRestoreRawFile(CString strLocalFileName, FILE *fpDestFile, PS_RAW_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo);
//	BOOL GetAuxFileRawData(PS_AUX_FILE_HEADER &FileHeader, FILE * fpDestFile, FILE * fpSourceFile, CString destFileName, CString srcFileName);
//	BOOL GetFileRawData(PS_RAW_FILE_HEADER &fileHeader, FILE * fpDestFile, FILE * fpSourceFile,
//									  CString destFileName, CString srcFileName);
	int GetTotalAuxCount();
	int m_nVoltAuxCount;
	int m_nTempAuxCount;
	void SetCanTitleFile(CString strCanPath, CString strTestName);
	CString GetEndTime();
	BOOL m_IsUserFilterSlave;
	BOOL m_IsUserFilterMaster;
	long WriteResultListFileA(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData);
	long WriteResultListFileB(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData);
	void SetCanCount(int nCount);
	int m_nCanCount;
	int m_nCanTransCount;
	BOOL CreateCanFileA(CString strCanFileName, int nFileIndex, int nCanCount);
	long GetCanItemData(PS_CAN_RAW_DATA canData, WORD wItem, int nCanIndex);
	int GetCanFile(CFile *pFile, PS_CAN_FILE_HEADER * canHeader);
	int CheckAndSaveData();
	int	CheckAndSaveDataA();
	int GetCanCount();
	SFT_CAN_DATA * GetCanData();
	void SetMasterChannelNum(unsigned char uMaster);
	int GetMasterChannelNum();
	BOOL	m_bMaster;
	BOOL IsParallel();
	void SetParallel(BOOL bParallel, BOOL bMaster);
	int GetAuxFile(CFile * pFile, PS_AUX_FILE_HEADER * auxHeader);
	void SetAuxTitleFile(CString strPath,  CString strTestName);
	int GetChFile(CFile & fRS, PS_RAW_FILE_HEADER & rawHeader);
	float GetAuxItemData(PS_AUX_RAW_DATA auxData, WORD wItem, int nAuxIndex);
	BOOL CreateAuxFileA(CString m_strAuxFileName, int nFileIndex, int nAuxCount);
	int m_nAuxCount;
	SFT_AUX_DATA * GetAuxData();
	int	GetAuxCount();
	//CObList	auxList;
	HTREEITEM hCurrentChannelTreeItem;
	CString GetStartTime();
	CScheduleData * GetScheduleData();
	BOOL SaveRunInfoToTempFile();
	void LoadSaveItemSet();
	void SetSaveItemList(CWordArray &awItem);
	BOOL IsStopReserved();	
	BOOL IsPauseReserved();
	BOOL IsDataLoss()		{	return m_bDataLoss;		}		//SearchLostDataRange()를 하여야 실제 정확한 손실 여부를 판단 할 수 있다.

	void WriteLog(CString strLogMsg);
	BOOL	SaveStepMiliSecData();
	BOOL	RemakePCIndexFile(CString strBackUpRawFile, CString strStepEndFile, CString strNewTableFile, CString strOrgTableFile = "");
	CString GetLastErrorString();
	CString GetResultFileName();

	//File Update Lock
	BOOL	LockFileUpdate(BOOL bLock = TRUE);
	BOOL	LoadInfoFromTempFile();	
	void		UpdateStartTime()	{	m_startTime = COleDateTime::GetCurrentTime();	}
	void		SetTestSerial(CString strSerial, CString strUserID = "", CString strDescript = "")	
	{	
		m_strTestSerial = strSerial;	
		m_strDescript = strDescript;
		m_strUserID = strUserID;
	}
	int			CompletedTest();
	CString		GetGradeString();
	CString		GetFilePath();
	void	ResetData();
	
	CString GetTestPath();
	CString GetChFilePath()						{	return m_strFilePath;				}
	CString GetWorkerName()						{	return m_strUserID;				}
	CString GetTestDescript()					{	return m_strDescript;			}
	CString	GetTestSerial()						{	return m_strTestSerial;			}
	CString GetScheduleName()					{	return m_strScheduleName;		}
	CString	GetScheduleFile()					{	return m_strScheduleFileName;	}
	CString	GetLogFileName()					{	return m_strLogFileName;		}
	void	SetScheduleName(CString strName)	{	m_strScheduleName = strName;	}
	
	void SetFilePath(CString strPath, BOOL bReset = TRUE);
	BOOL GetSaveData(PS_STEP_END_RECORD &sChData);
	BOOL GetSaveDataB(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData);
	BOOL GetSaveDataC(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA & sCanData);

	//test name
	void	SetTestName(CString strTestName);
	CString GetTestName();
	
	BYTE		GetGradeCode();
	int	GetCurCycleCount();
	int	GetTotalCycleCount();
	WORD GetCellCode();
	WORD GetState();
	WORD GetStepType();
	WORD GetStepNo();
	ULONG GetStepTime();
	ULONG GetTotTime();
	UINT GetStackedSize();
	long GetImpedance();
	long GetCapacity();
	long GetCurrent();
	long GetVoltage();
	long GetWattHour();
	long GetWatt();
	long GetAvgVoltage();
	long GetAvgCurrent();
	long GetCapacitySum();
	long GetTemperature();
	long GetAuxVoltage();


//	CCalibrationData * GetCaliData();
	CChannel * GetChannel();
	
	CCyclerChannel(UINT nModuleID, UINT nChannelIndex);
	CCyclerChannel();
	virtual ~CCyclerChannel();

	void SetID(UINT nModuleID, UINT nChIndex);

	double GetMeterValue()				{	return	m_fMeterValue;		}
	void	SetMeterValue(double fValue)	{	m_fMeterValue = fValue;		}

	//lost data range
	BOOL	RestoreLostData(CString strIPAddress);
	BOOL	SearchLostDataRange(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd = NULL, BOOL bIncludeInitCh = FALSE);
	int		GetLostDataRangeSize()			{	return m_strDataLossRangeList.GetCount();	}
	CStringList * GetLostDataRangeList()	{	return &m_strDataLossRangeList;				}
	
	void	GetReservedStep(long &lCycleNo, long &lStepNo)	{	lCycleNo = m_lReservedCycleNo; lStepNo = m_lReservedStepNo;	}
	void	SetReservedStep(long lCycleNo, long lStepNo)	{	m_lReservedCycleNo = lCycleNo; m_lReservedStepNo =lStepNo ;	}

protected:
	BOOL	m_bUseOven;
	void UpdateLastData(UINT nStartIndex, UINT nEndIndex, CString strStartT, CString strSerial, CString strUser, CString strDescript, PS_STEP_END_RECORD sChData);
	ULONG GetLastData(CString &strStartT, CString &strSerial, CString &strUser, CString &strDescript, PS_STEP_END_RECORD &sChData);
	float GetItemData(PS_STEP_END_RECORD &chData, WORD wItem);
	int		ParsingChDataString(CString strValue, PS_STEP_END_RECORD &sData);	
	float AddTotalCapacity(WORD nType, float fCapacity);
	long ConvertPCUnitToSbc(float fData, long nItem);
	float ConvertSbcToPCUnit(long lData, long nItem);
	BOOL ReloadReaultData(BOOL bIncludeWorkingStep = TRUE);
	
	void		SetScheduleFile(CString strFileName);
	void		SetLogFile(CString strFileName)		{ m_strLogFileName = strFileName;	};

	CString		m_strLastErrorString;
	CStringList m_strDataLossRangeList;
	CChData		m_chFileDataSet;	
	BOOL		m_bDataLoss;				//통신 Error 및 기타 원인에의해 중간에 Data가 손실되었는지 연부 
											//마지막에 손실된 Data는 FTP 접속 이전에는 확인 할 수 없다. 
	BOOL		m_bLockFileUpdate;			//다른 곳에서 결과 파일 접근시 update locking
	CStringList m_strDataList;				//결과 파일 저장시 임시적은 저장 장소 
	BOOL GetRecordIndex(CString strString, long &lFrom, long &lTo);
//	CCalibrationData	m_CaliData;			//채널의 교정값 

	//test Information
	CString m_strScheduleName;
	CString m_strDescript;
	CString m_strUserID;
	CString m_strTestSerial;			//Test Serial or Lot No
	CString m_strTestName;				//Test 명 
	CString m_strFilePath;				//C:\\Test\M01C01

	//File Name
	CString m_strLogFileName;			//작업 Log 파일명		C:\\Test\M01C01\Test.log
	CString m_strScheduleFileName;		//적용 스케쥴 파일명	C:\\Test\M01C01\Test.sch
	CString m_strResultFileName;		//저장 조건 결과 파일명	C:\\Test\M01C01\Test.cyc
	CString m_strResultStepFileName;	//Step의 결과 파일명	C:\\Test\M01C01\Test.rpt (C:\\Test\M01C01\Test.rp$)

	UINT m_nModuleID;
	UINT m_nChannelIndex;
	float m_fCapacitySum;
	double m_fMeterValue;
	

	COleDateTime	m_startTime;
	CModuleIndexTable	m_RestoreFileIndexTable;
	PS_RECORD_FILE_HEADER	m_FileRecordSetHeader;
	CScheduleData *m_pScheduleData;

	//Text Save
//	BOOL CreateResultFile(CString strFileName);
//	BOOL CreateResultListFile(CString strFileName);
//	BOOL WriteResultListFile(UINT nLineNo, CT_CH_SAVE_DATA &sChData);

	//Binary Save
	BOOL CreateResultFileA(CString strFileName);
//	BOOL WriteResultListFileA(UINT nLineNo, CT_CH_SAVE_DATA &sChData);
//	BOOL WriteResultListFileB(UINT nLineNo, CT_CH_SAVE_DATA &sChData);
//	BOOL WriteResultListFileC(UINT nLineNo, CT_CH_SAVE_DATA &sChData);
//	BOOL WriteResultListFileD(UINT nLineNo, PS_STEP_END_RECORD &sChData);
//	BOOL WriteResultListFileF(UINT nLineNo, PS_STEP_END_RECORD &sChData);

private:
	unsigned char m_uMasterCh;
	long	m_lReservedCycleNo;
	long	m_lReservedStepNo;
	BOOL	m_bParallel;
	

};

#endif // !defined(AFX_CYCLERCHANNEL_H__F1065011_05CD_44C3_863C_837DF75454A2__INCLUDED_)

