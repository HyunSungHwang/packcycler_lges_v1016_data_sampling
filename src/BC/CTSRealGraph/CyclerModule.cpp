// CyclerModule.cpp: implementation of the CCyclerModule class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CTSRealGraph.h"
#include "CyclerModule.h"

//#include <FtpDownLoad.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define MAX_SENSOR_TYPE	3	//0 : 온도센서, 1: 전압센서, 2:CAN 통신
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCyclerModule::CCyclerModule()
{
	m_lpChannelInfo = NULL;
	m_nModuleID = 0;
	m_nTotalChannel = 0;
	m_bCheckUnSafetyShutdown = FALSE;
	pAuxTemp = NULL;
	pAuxVolt = NULL;
//	pAuxCAN = NULL;
	pAuxTemp2 = NULL;
	pAuxVolt2 = NULL;
//	pAuxCAN2 = NULL;
	nParallelCount = 0;
	ZeroMemory(&nInstalledMasterCANCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledSlaveCANCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledMasterCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledSlaveCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&m_canTransData, sizeof(SFT_MD_CAN_TRANS_INFO_DATA));
	ZeroMemory(&m_canData, sizeof(SFT_MD_CAN_INFO_DATA));
}

CCyclerModule::~CCyclerModule()
{
	RemoveChannel();		
	RemoveAllAuxCh();	
}

CCyclerChannel * CCyclerModule::GetChannelInfo(int nChannelIndex)
{
	ASSERT(m_lpChannelInfo != NULL);	//Must call MakeChannel() before use this function
	if(nChannelIndex < m_nTotalChannel)
		return &m_lpChannelInfo[nChannelIndex];

	return NULL;
}

BOOL CCyclerModule::MakeChannel(UINT nTotalCh)
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before
	if(nTotalCh < 1)	return FALSE;
	
	//채널 수가 변경 되었거나 최초일 경우  
	if(nTotalCh != m_nTotalChannel || m_lpChannelInfo == NULL)
	{
		RemoveChannel();
		m_lpChannelInfo = new CCyclerChannel[nTotalCh];
		ASSERT(m_lpChannelInfo);
		for(WORD i=0; i<nTotalCh; i++)
		{
			m_lpChannelInfo[i].SetID(m_nModuleID, i);
		}
		m_nTotalChannel = nTotalCh;
	}
	else
	{
		for(WORD i=0; i<nTotalCh; i++)
		{
			m_lpChannelInfo[i].ResetData();
		}
	}
	return TRUE;
}

void CCyclerModule::RemoveChannel()
{
	if(m_lpChannelInfo != NULL)
	{
		delete [] m_lpChannelInfo;
		m_lpChannelInfo = NULL;
	}
}

WORD CCyclerModule::GetState()
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before
	return ::SFTGetModuleState(m_nModuleID);
}

// apChList: Selected Channel Index Array
int CCyclerModule::SendCommand(UINT nCmd, CWordArray *apChList, LPVOID lpData, UINT nSize)
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before

	SFT_CH_SEL_DATA selBitData;
	ZeroMemory(&selBitData, sizeof(selBitData));		//Channel 선택 Bit 정보

	if(apChList != NULL)
	{
		if(apChList->GetSize() <= 0)	return SFT_NACK;
	
		DWORD dwBitMask = 0x00000001;
		WORD nChIndex;
		for(int i =0; i<apChList->GetSize(); i++)
		{
			nChIndex = apChList->GetAt(i);
			if( nChIndex < sizeof(DWORD)*8 )
			{
				selBitData.nSelBitData[0] |= dwBitMask << nChIndex;
			}
			else
			{
				selBitData.nSelBitData[1] |= dwBitMask << (nChIndex-sizeof(DWORD)*8);
			}
		}
	}
	else	//Send Command to Module
	{

	}
	/*if(nCmd == SFT_CMD_USERPATTERN_DATA)
	{
		SFT_STEP_SIMUL_CONDITION temp;
		memcpy(&temp, lpData, nSize);
		int i = 0;
		i = 1;
	}*/
	return ::SFTSendCommand(m_nModuleID,  nCmd, &selBitData, lpData, nSize);
}

// nChIndex: Selected Channel Index
int CCyclerModule::SendCommand(UINT nCmd, int nChIndex, LPVOID lpData, UINT nSize)
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before

	SFT_CH_SEL_DATA selBitData;
	ZeroMemory(&selBitData, sizeof(selBitData));		//Channel 선택 Bit 정보

	if(nChIndex >= 0)
	{
		DWORD dwBitMask = 0x00000001;
		if( nChIndex < sizeof(DWORD)*8 )
		{
			selBitData.nSelBitData[0] |= dwBitMask << nChIndex;
		}
		else
		{
			selBitData.nSelBitData[1] |= dwBitMask << (nChIndex-sizeof(DWORD)*8);
		}
	}
	return ::SFTSendCommand(m_nModuleID,  nCmd, &selBitData, lpData, nSize);

}

// nChIndex: Selected Channel Index
int CCyclerModule::SendCommand(UINT nCmd, LPVOID lpData, UINT nSize)
{
	return SendCommand(nCmd, NULL, lpData, nSize);
}

CString CCyclerModule::GetCmdErrorMsg(int nCode)
{
	return "UnKnown";
}

void CCyclerModule::SetModuleID(int nModuleIndex, int nModuleID, SFT_SYSTEM_PARAM	*pParam)
{
	ASSERT(nModuleIndex >=0);
	ASSERT(pParam);

	m_nModuleID = nModuleID;
	::SFTSetSysParam(nModuleIndex, nModuleID, pParam);
	
	//Make default Channel
	MakeChannel(::SFTGetModule(nModuleID)->GetChannelCount());




}

float CCyclerModule::GetVoltageSpec(int nRange /*= CAL_RANGE1*/)
{
//	SFTGetSysParam(int nModuleID);
	
		SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
		if(pSys == NULL)						return 0.0;
		if(nRange < 0 || nRange >= 5)			return 0.0;
		float fSpec = (float)pSys->nVoltageSpec[nRange];
/*		if(pSys->byVoltageUnitType == 1)	//nano unit
		{
			fSpec /= 1000.0f; 
		}
*/		return LONG2FLOAT(fSpec);
}

float CCyclerModule::GetCurrentSpec(int nRange /*= CAL_RANGE1*/)
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	if(pSys == NULL)						return 0.0;
	if(nRange < 0 || nRange >= 5)			return 0.0;

	float fSpec = (float)pSys->nCurrentSpec[nRange];
/*	if(pSys->byCurrentUnitType == 1)	//nano unit
	{
		fSpec /= 1000.0f; 
	}
*/	return LONG2FLOAT(fSpec);
}

int	CCyclerModule::GetCurrentRangeCount()
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	if(pSys == NULL)	0;

	if(pSys->wCurrentRange < 1 || pSys->wCurrentRange > 5)	return 0;	//Network변수는 반드시 Check한다.
	return pSys->wCurrentRange;
}

int		CCyclerModule::GetVoltageRangeCount()
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	if(pSys == NULL)	0;
	if(pSys->wVoltageRange < 1 || pSys->wVoltageRange > 5)	return 0;
	return pSys->wVoltageRange;
}

CString CCyclerModule::GetIPAddress()
{
	CString address(::SFTGetIPAddress(m_nModuleID));
	return address;
}


CString CCyclerModule::GetConnectedTime()
{
	SYSTEMTIME *pTime = ::SFTGetConnectedTime(m_nModuleID);	
	if(pTime == FALSE)	return "";

	CTime time(*pTime);
	return time.Format("%Y/%m/%d %H:%M:%S");
}

CString CCyclerModule::GetConnectionElapsedTime()
{
	SYSTEMTIME *pTime = ::SFTGetConnectedTime(m_nModuleID);	
	if(pTime == FALSE)	return "";
	
	SYSTEMTIME  curTime; 
	::GetLocalTime(&curTime);
	
	CTime cononnectTime(*pTime);
	CTime currentTime(curTime);
	CTimeSpan elapsedTime = currentTime - cononnectTime;

	return elapsedTime.Format("%DDay %H:%M:%S");
}

BOOL CCyclerModule::CheckRunningChannel()
{
	CString strMsg;
	CCyclerChannel *pCh;

	for(WORD i=0; i< GetTotalChannel(); i++)
	{
		pCh = GetChannelInfo(i);
		if(pCh)
		{
//			모듈이 접속하면 무조건 최종 시험 정보를 Loading한다.
//			WORD state = pCh->GetState();
//			if(pCh->GetState() == PS_STATE_RUN || pCh->GetState() == PS_STATE_PAUSE)
//			{
				if(pCh->LoadInfoFromTempFile() == FALSE)
				{
//					strMsg.Format("%s Ch %d 이전 시험정보 임시파일 Loading 실패", GetModuleName(nModuleID), i+1);
//					WriteLog(strMsg);
				}
				pCh->WriteLog("통신 연결");
//			}
		}
	}
	m_bCheckUnSafetyShutdown = TRUE;
	return TRUE;
}

//모듈에 있는 Save Last Index 파일을 DownLoad하여 
//PC 저장파일 Index와 비교하여 손실여부를 확인한다.
int CCyclerModule::UpdateDataLossState(CWordArray *pSelCh, BOOL bOnLineMode)
{
#ifdef _DEBUG
		DWORD dwStart = GetTickCount();
#endif		

	return 1; //★★ ljb 20091110  ★★★★★★★★
/*
	if(bOnLineMode)		//모듈이 Online(즉 Network가 확실히 연결된 경우)시만 접속함 => 시간 지연 없에기 위해 
	{
		if( GetState() == PS_STATE_LINE_OFF)
		{
			return 0;
		}
	}

	CString strTemp;
	CWordArray aSelectCh;
	if(pSelCh == NULL)
	{
		for(WORD i=0; i<m_nTotalChannel; i++)
		{
			aSelectCh.Add(i);
		}
	}
	else
	{
		for(WORD k=0; k<pSelCh->GetSize(); k++)
		{
			aSelectCh.Add(pSelCh->GetAt(k));
		}
	}
	
	int nLostRange = 0;
	CString strIP = GetIPAddress();
	if(strIP.IsEmpty())	
	{
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), "Data 손실 검사...", TRUE, TRUE, TRUE);
	progressWnd.SetRange(0, 100);
	//////////////////////////////////////////////////////////////////////////

	strTemp.Format("모듈 %d에 이전 작업 정보 요청 중...", m_nModuleID);
	progressWnd.SetText(strTemp);
	CFtpDownLoad *pDownLoad = new CFtpDownLoad();
	//strIP = "192.168.5.84";
	if(pDownLoad->ConnectFtpSession(strIP)== FALSE)
	{
		delete pDownLoad;
		return 0;
	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strLocalTempFolder(szBuff);
	CString strRemoteFileName, strStartIndexFile, strLastIndexFile;

	progressWnd.SetPos(10);
	int nChannelIndex;
	int nTotSelCount = aSelectCh.GetSize();
	long lDataTemp = 0;
	for(int k=0; k<nTotSelCount; k++)
	{
		nChannelIndex = aSelectCh.GetAt(k);
		strTemp.Format("모듈 %d의 채널 %d 최종 작업 정보 분석 중...", m_nModuleID, nChannelIndex+1);
		progressWnd.SetText(strTemp);
		progressWnd.SetPos(10+int((float)k/(float)nTotSelCount*90.0f));
		
		//Cancled
		if(progressWnd.PeekAndPump() == FALSE)	
			break;

		strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", nChannelIndex+1);
		strStartIndexFile.Format("%s\\savingFileIndex_start.csv", strLocalTempFolder, nChannelIndex);

		if(pDownLoad->DownLoadFile(strStartIndexFile, strRemoteFileName) < 1)
		{
			//초기화 되어 있을 경우 실패함
			//Download  실패 가끔 발생함 => 처리 필요 
// 			pProgressWnd->Hide();
// 			delete pProgressWnd;
// 			delete pDownLoad;
// 			TRACE("%s download fail\n", strRemoteFileName);
// 			return nLostRange;
			
			sprintf(szBuff, "결과 data 시작 정보 Index 요청실패...");
			SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
			continue;
		}
		
		strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_last.csv", nChannelIndex+1);
		strLastIndexFile.Format("%s\\savingFileIndex_last.csv", strLocalTempFolder, nChannelIndex);
		if(pDownLoad->DownLoadFile(strLastIndexFile, strRemoteFileName) < 1)
		{
			//초기화 되어 있을 경우 실패함
			//Download  실패 가끔 발생함 => 처리 필요 
// 			pProgressWnd->Hide();
// 			delete pProgressWnd;
// 			delete pDownLoad;
// 			TRACE("%s download fail\n", strRemoteFileName);
// 			return nLostRange;
			
			sprintf(szBuff, "결과 data 종료 정보 Index 요청실패...");
			SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
			continue;
		}

		//SBC에서 Download한 파일과 PC파일을 비교하여 손실구간이 있는지 확인한다.
		if(m_lpChannelInfo[nChannelIndex].SearchLostDataRange(strStartIndexFile, strLastIndexFile, lDataTemp, &progressWnd) == TRUE)
		{
			nLostRange += lDataTemp;
		}
		else
		{
			sprintf(szBuff, "손실구간 검색 실패.(%s)", m_lpChannelInfo[nChannelIndex].GetLastErrorString());
			SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
//			WriteLog(strTemp);
			TRACE("%s\n", strTemp);
		}
	}

	progressWnd.SetPos(100);
	delete pDownLoad;

#ifdef _DEBUG
		dwStart = GetTickCount() - dwStart;
		TRACE("%%%%%%%%%%%%%%%%%%%%%%%%%% FTP Check %d msec%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n", dwStart);
#endif	

	return nLostRange;
	*/
}

UINT CCyclerModule::GetProtocolVer()
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	return pSys->nProtocolVersion;
}

void CCyclerModule::SetParallelData(SFT_MD_PARALLEL_DATA paralData)
{
	if(paralData.bParallel == 1)
	{
		memcpy(&m_parallelData[paralData.chNoMaster - 1], &paralData, sizeof(SFT_MD_PARALLEL_DATA));
		m_parallelData[paralData.chNoSlave[0] -1].chNoMaster = 0;
		m_parallelData[paralData.chNoSlave[0] -1].bParallel = 0;
	}
	else
	{
		m_parallelData[paralData.chNoMaster - 1].chNoSlave[0] = 0;
		m_parallelData[paralData.chNoMaster - 1].bParallel = 0;
		m_parallelData[paralData.chNoSlave[0] -1].chNoMaster = paralData.chNoSlave[0] -1;
		m_parallelData[paralData.chNoSlave[0] -1].bParallel = 0;
	}

	for(int i = 0 ; i < GetTotalChannel(); i++)
	{
		CCyclerChannel * pCh = GetChannelInfo(i);
		if(i+1 == paralData.chNoMaster)
			pCh->SetParallel(paralData.bParallel, paralData.bParallel);
		if(i+1 == paralData.chNoSlave[0])
		{
			pCh->SetParallel(paralData.bParallel, FALSE);
			pCh->SetMasterChannelNum(paralData.chNoMaster);
		}
		if(i+1 == paralData.chNoSlave[1])
		{
			pCh->SetParallel(paralData.bParallel, FALSE);
			pCh->SetMasterChannelNum(paralData.chNoMaster);
		}
	}
	
	
}

//kjh Temp 
//파일로 부터 Aux 데이터를 불러온다.


int CCyclerModule::GetTotalSensor()
{
	return this->nTotalSensor;
}


int CCyclerModule::GetMaxSensorType()
{
	return MAX_SENSOR_TYPE;
}

void CCyclerModule::RemoveAuxData(STF_MD_AUX_SET_DATA delAux)
{
	switch(delAux.auxType)
	{
	case 0: pAuxTemp2[delAux.auxChNo-1].RemoveAuxData();	break; 
	case 1: pAuxVolt2[delAux.auxChNo-1].RemoveAuxData();	break; 
//	case 2: pAuxCAN2[delAux.auxChNo-1].RemoveAuxData();	break;
	default: break;
	}

}


CChannelSensor * CCyclerModule::GetAuxData(int nSensorIndex, int nSensorType)
{
	switch(nSensorType)
	{
	case 0: 
		if(nSensorIndex < GetMaxTemperature() && nSensorIndex >= 0)
			return &pAuxTemp2[nSensorIndex]; 
		else
		{
			return NULL;
		}
	case 1: 
		if(nSensorIndex < GetMaxVoltage() && nSensorIndex >= 0)
			return &pAuxVolt2[nSensorIndex]; 
		else
			return NULL;
/*	case 2: 
		if(nSensorIndex < GetMaxCAN() && nSensorIndex >= 0)
			return &pAuxCAN2[nSensorIndex]; 
		else
			return NULL;*/
	default : return NULL; 
	}
}

void CCyclerModule::MakeParallel()
{	
	memcpy(m_parallelData, ::SFTGetParallelData(m_nModuleID), sizeof(SFT_MD_PARALLEL_DATA)* _SFT_MAX_PARALLEL_COUNT);

	for(int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
	{
		if(m_parallelData[i].bParallel == TRUE)
		{
			CCyclerChannel *pCh = GetChannelInfo(m_parallelData[i].chNoMaster - 1);
			pCh->SetParallel(m_parallelData[i].bParallel, TRUE);
			pCh = GetChannelInfo(m_parallelData[i].chNoSlave[0] - 1);
			pCh->SetParallel(m_parallelData[i].bParallel, FALSE);
			pCh->SetMasterChannelNum(m_parallelData[i].chNoMaster);

		}
	}
	
}

//병렬 연결 정보를 파일로부터 읽어온다.


int CCyclerModule::GetParallelCount()
{
	return nParallelCount;
}

SFT_MD_PARALLEL_DATA * CCyclerModule::GetParallelData()
{
	return m_parallelData;
}


int CCyclerModule::GetMaxTemperature()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL)						return 0;
	
	return pAuxInfo->wInstalledTemp;
}

int CCyclerModule::GetMaxVoltage()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL)						return 0;
	
	return pAuxInfo->wInstalledVolt;
}
/*
int CCyclerModule::GetMaxCAN()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL)						return 0;
	
	return pAuxInfo->wInstalledCAN;
}*/

void CCyclerModule::MakeAuxData()
{
	int nAuxType = 0;

	if(pAuxTemp != NULL)
	{
		delete [] pAuxTemp;
		delete [] pAuxTemp2;
		pAuxTemp = pAuxTemp2 = NULL;
	}
	if(pAuxVolt != NULL)
	{
		delete [] pAuxVolt;
		delete [] pAuxVolt2;
		pAuxVolt = pAuxVolt2 = NULL;
	}
/*	if(pAuxCAN != NULL)
	{
		delete [] pAuxCAN;
		delete [] pAuxCAN2;
		pAuxCAN = pAuxCAN2 = NULL;

	}*/
	for(nAuxType = 0 ; nAuxType < MAX_SENSOR_TYPE; nAuxType++)
	{
		
		switch(nAuxType)
		{
		case 0:		if(GetMaxTemperature() > 0)
					{
						pAuxTemp = new CChannelSensor[GetMaxTemperature()];
						
						for(int i = 0 ; i < GetMaxTemperature(); i++)
						{
							STF_MD_AUX_SET_DATA *auxData = new STF_MD_AUX_SET_DATA;
							ZeroMemory(auxData, sizeof(STF_MD_AUX_SET_DATA));
							auxData->auxType = nAuxType;
							auxData->auxChNo = i+1;
							pAuxTemp[i].SetAuxData(auxData);
							pAuxTemp[i].SetInstall(TRUE);
							pAuxTemp[i].SetMasterChannel(0);
							delete auxData;
						}
				
						pAuxTemp2 = new CChannelSensor[GetMaxTemperature()];
						memcpy(pAuxTemp2, pAuxTemp, sizeof(CChannelSensor) * GetMaxTemperature());
					}
					break;
		case 1:		if(GetMaxVoltage() > 0)
					{
						pAuxVolt = new CChannelSensor[GetMaxVoltage()];				
						for(int i = 0 ; i < GetMaxVoltage(); i++)
						{
							STF_MD_AUX_SET_DATA *auxData = new STF_MD_AUX_SET_DATA;
							ZeroMemory(auxData, sizeof(STF_MD_AUX_SET_DATA));
							auxData->auxType = nAuxType;
							auxData->auxChNo = i+1;
							pAuxVolt[i].SetAuxData(auxData);
							pAuxVolt[i].SetInstall(TRUE);
							pAuxVolt[i].SetMasterChannel(0);
							delete auxData;
						}

						pAuxVolt2 = new CChannelSensor[GetMaxVoltage()];
						memcpy(pAuxVolt2, pAuxVolt, sizeof(CChannelSensor) * GetMaxVoltage());
					}
					break;
	/*	case 2:		if(GetMaxCAN() > 0)
					{
						pAuxCAN  = new CChannelSensor[GetMaxCAN()];
						for(int i = 0 ; i < GetMaxCAN(); i++)
						{
							STF_MD_AUX_SET_DATA *auxData = new STF_MD_AUX_SET_DATA;
							ZeroMemory(auxData, sizeof(STF_MD_AUX_SET_DATA));
							auxData->auxType = nAuxType;
							auxData->auxChNo = i+1;
							pAuxCAN[i].SetAuxData(auxData);
							pAuxCAN[i].SetInstall(TRUE);
							pAuxCAN[i].SetMasterChannel(0);
							delete auxData;
						}

						pAuxCAN2  = new CChannelSensor[GetMaxCAN()];
						memcpy(pAuxCAN2, pAuxCAN, sizeof(CChannelSensor) * GetMaxCAN());
					}
					break;*/
		default : break; 
		}
	}
	
	SetInstalledAux();
}

void CCyclerModule::SetInstalledAux()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL)						return;

	for(int i = 0 ; i < _SFT_MAX_MAPPING_AUX; i++)
	{
		if(pAuxInfo->auxData[i].chNo > 0)
		{
			CChannelSensor * pSensor = GetAuxData(pAuxInfo->auxData[i].auxChNo-1, pAuxInfo->auxData[i].auxType);
			if(pSensor == NULL)
				continue;

			STF_MD_AUX_SET_DATA auxData;
			memcpy(&auxData, &pAuxInfo->auxData[i], sizeof(STF_MD_AUX_SET_DATA));
			pSensor->SetAuxData(&auxData);
			pSensor->SetMasterChannel(pAuxInfo->auxData[i].chNo);
		}
		
	}
	ApplyAuxData();
}

void CCyclerModule::GetTemperatureAuxList(CChannelSensor *pList)
{
	memcpy(pList, pAuxTemp, sizeof(CChannelSensor)*GetMaxTemperature());
}

void CCyclerModule::GetVoltageAuxList(CChannelSensor *pList)
{
	memcpy(pList, pAuxVolt, sizeof(CChannelSensor)*GetMaxTemperature());
}

/*void CCyclerModule::GetCanAuxList(CChannelSensor *pList)
{
	memcpy(pList, pAuxCAN, sizeof(CChannelSensor)*GetMaxTemperature());
}*/

void CCyclerModule::SetAllParallelReset()
{
	int i = 0;
	for(int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
	{
		m_parallelData[i].bParallel = 0;
		m_parallelData[i].chNoMaster = i+1;
		ZeroMemory(m_parallelData[i].chNoSlave, sizeof(char)*3);
	}

	for(int i = 0 ; i < GetTotalChannel(); i++)
	{
		CCyclerChannel * pCh = GetChannelInfo(i);	
		if(!pCh->m_bMaster)
			pCh->SetMasterChannelNum(0);
		pCh->SetParallel(FALSE, FALSE);
		
	
	}
}

void CCyclerModule::ApplyAuxData()
{
	memcpy(pAuxTemp, pAuxTemp2, sizeof(CChannelSensor) * GetMaxTemperature());
	memcpy(pAuxVolt, pAuxVolt2, sizeof(CChannelSensor) * GetMaxVoltage());

	int i = 0;
	for(int i = 0 ; i < GetTotalChannel(); i++)
	{
		CCyclerChannel * pChannel = GetChannelInfo(i);
		if(pChannel == NULL) return;
		pChannel->m_nTempAuxCount = 0;
		pChannel->m_nVoltAuxCount = 0;
	}
	for(int i = 0 ; i < GetMaxTemperature(); i++)
	{
		if(pAuxTemp[i].GetMasterChannel() > 0)
		{
			CCyclerChannel * pChannel = GetChannelInfo(pAuxTemp[i].GetMasterChannel()-1);
			//20081227 KHS
			if(pChannel == NULL) return;
			pChannel->m_nTempAuxCount++;
		}
	}

	for(int i = 0 ; i < GetMaxVoltage(); i++)
	{
		if(pAuxVolt[i].GetMasterChannel() > 0)
		{
			CCyclerChannel * pChannel = GetChannelInfo(pAuxVolt[i].GetMasterChannel()-1);
			//20081227 KHS
			if(pChannel == NULL) return;
			pChannel->m_nVoltAuxCount++;
		}
	}
	SaveAuxConfig();
}

void CCyclerModule::CencleAuxData()
{
	memcpy(pAuxTemp2, pAuxTemp, sizeof(CChannelSensor) * GetMaxTemperature());
	memcpy(pAuxVolt2, pAuxVolt, sizeof(CChannelSensor) * GetMaxVoltage());
}

void CCyclerModule::RemoveAllAuxCh()
{
	if(pAuxTemp != NULL)
	{
		delete [] pAuxTemp;
		delete [] pAuxTemp2;
		pAuxTemp = pAuxTemp2 = NULL;
	}
	if(pAuxVolt != NULL)
	{
		delete [] pAuxVolt;
		delete [] pAuxVolt2;
		pAuxVolt = pAuxVolt2 = NULL;
	}
/*	if(pAuxCAN != NULL)
	{
		delete [] pAuxCAN;
		delete [] pAuxCAN2;
		pAuxCAN = pAuxCAN2 = NULL;
		
	}*/
}


void CCyclerModule::SaveAuxConfig()
{
	CString strBuff;
	char szBuff[2000];
	CString strFileName;
	CFile file;
	CFileFind aFind;

	CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";

	CString strFileFind = strSaveDir + "\\Config\\*.aux";
	BOOL bFind = aFind.FindFile(strFileFind);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}


	int i = 0;
	for(int nCh = 0 ; nCh < GetTotalChannel(); nCh++)
	{
		if(GetChannelInfo(nCh)->GetTotalAuxCount() <= 0) continue;

		for(int i = 0 ; i < GetMaxTemperature(); i++)
		{
			if(pAuxTemp[i].GetMasterChannel() == nCh+1)
			{
				CString strTemp;
				//ljb 2010-06-22 Aux Name 추가 Save
				strTemp.Format("%d,%d,%s\r\n", pAuxTemp[i].GetAuxChannelNumber(), pAuxTemp[i].GetAuxType(), pAuxTemp[i].GetAuxName());
				strBuff += strTemp;				
			}
		}

		for(int i = 0 ; i < GetMaxVoltage(); i++)
		{
			if(pAuxVolt[i].GetMasterChannel() == nCh+1)
			{
				CString strTemp;
				//ljb 2010-06-22 Aux Name 추가 Save
				strTemp.Format("%d,%d,%s\r\n", pAuxVolt[i].GetAuxChannelNumber(), pAuxVolt[i].GetAuxType(), pAuxVolt[i].GetAuxName());
				strBuff += strTemp;
			}
		}

	/*	for(int i = 0 ; i < GetMaxCAN(); i++)
		{
			if(pAuxCAN[i].GetMasterChannel() == nCh+1)
			{
				CString strTemp;
				//ljb 2010-06-22 Aux Name 추가 Save
				strTemp.Format("%d,%d,%s\r\n", pAuxCAN[i].GetAuxChannelNumber(), pAuxCAN[i].GetAuxType(), pAuxCAN[i].GetAuxName());
				strBuff += strTemp;
			}
		}*/
		strBuff += "\0";

		TRY
		{
			ZeroMemory(&szBuff, 2000);
			
			strFileName.Format("%s\\Config\\M%02dCh%02d.aux", strSaveDir, GetModuleID(),nCh+1);
		

			if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite))
			{
				sprintf(szBuff, strBuff, strBuff.GetLength());
				int nLength = strBuff.GetLength();
				file.Write(szBuff, sizeof(char)*nLength);
				file.Close();
			}


			strBuff = "";
		}CATCH(CFileException, e)
		{
			e->ReportError();
			return;
		}
		END_CATCH

	}
}

SFT_CAN_COMMON_DATA CCyclerModule::GetCANCommonData(int nChIndex, int nType)
{
	return m_canData.canSetAllData.canCommonData[nChIndex][nType-1];
}

SFT_CAN_SET_DATA CCyclerModule::GetCANSetData(int nChIndex, int nType, int nCanIndex)
{
	if(nType == PS_CAN_TYPE_MASTER)
		return m_canData.canSetAllData.canSetData[nChIndex][nCanIndex];
	else if(nType == PS_CAN_TYPE_SLAVE)
		return m_canData.canSetAllData.canSetData[nChIndex][nInstalledMasterCANCount[nChIndex] + nCanIndex];
}

void CCyclerModule::MakeCAN()
{
	ZeroMemory(&m_canData, sizeof(SFT_MD_CAN_INFO_DATA));
	memcpy(&m_canData, ::SFTGetCANInfoData(m_nModuleID), sizeof(SFT_MD_CAN_INFO_DATA));
	ZeroMemory(&nInstalledMasterCANCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledSlaveCANCount, _SFT_MAX_INSTALL_CH_COUNT);

	for(int i = 0 ; i < m_nTotalChannel; i++)
	{
		GetChannelInfo(i)->SetCanCount(m_canData.installedCanCount[i]);
		for(int j = 0 ; j < m_canData.installedCanCount[i]; j++)
		{
			if(m_canData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_MASTER)
				nInstalledMasterCANCount[i]++;
			else if(m_canData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_SLAVE)
				nInstalledSlaveCANCount[i]++;
			else
			{
				TRACE("정의되지 않은 CAN 데이터에 접근합니다.");
				break;
			}
		}

		TRACE("마스터 캔수 :%d /r/n",nInstalledMasterCANCount[i]);
		TRACE("마스터 캔수 :%d /r/n",nInstalledSlaveCANCount[i]);
	}
	SaveCanConfig();
}

int CCyclerModule::GetInstalledCanMaster(int nChIndex)
{
	return nInstalledMasterCANCount[nChIndex];
}

int CCyclerModule::GetInstalledCanSlave(int nChIndex)
{
	return nInstalledSlaveCANCount[nChIndex];
}

void CCyclerModule::SetCANSetData(int nChIndex, SFT_CAN_SET_DATA *pCanSetData, int nCount)
{
	int nMaster, nSlave;
	nMaster = nSlave = 0;
	for(int i = 0; i < nCount; i++)
	{
		if(pCanSetData[i].canType == PS_CAN_TYPE_MASTER)
			nMaster++;
		else if(pCanSetData[i].canType == PS_CAN_TYPE_SLAVE)
			nSlave++;
	}
	nInstalledMasterCANCount[nChIndex] = nMaster;
	nInstalledSlaveCANCount[nChIndex] = nSlave;
	m_canData.installedCanCount[nChIndex] = nCount;
	
	GetChannelInfo(nChIndex)->SetCanCount(m_canData.installedCanCount[nChIndex]);
	
	memcpy(&m_canData.canSetAllData.canSetData[nChIndex], pCanSetData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);
}

void CCyclerModule::SetCANCommonData(int nChIndex, SFT_CAN_COMMON_DATA commonData, int nType)
{
	memcpy(&m_canData.canSetAllData.canCommonData[nChIndex][nType-1], &commonData, sizeof(SFT_CAN_COMMON_DATA));
}

SFT_MD_CAN_INFO_DATA * CCyclerModule::GetCanAllData()
{
	return &m_canData;
}

void CCyclerModule::SaveCanConfig()
{
	CString strBuff;
	char szBuff[5000];
	CString strFileName;
	CFile file;
	CFileFind aFind;

	CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";

	CString strFileFind = strSaveDir + "\\Config\\*.can";
	BOOL bFind = aFind.FindFile(strFileFind);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}


	int i = 0;
	for(int nCh = 0 ; nCh < GetTotalChannel(); nCh++)
	{
		int i = 0;
		for(int i = 0 ; i < nInstalledMasterCANCount[nCh]; i++)
		{
			CString strTemp;
/*			strTemp.Format("%d, %d, %d\r\n",
				i, m_canData.canSetAllData.canSetData[nCh][i].canType,
				m_canData.canSetAllData.canSetData[nCh][i].data_type);*/
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canData.canSetAllData.canSetData[nCh][i].canType,
				m_canData.canSetAllData.canSetData[nCh][i].data_type,
				m_canData.canSetAllData.canSetData[nCh][i].name);
			strBuff+=strTemp;
		}
	
		int nIndex = nInstalledMasterCANCount[nCh];
		for(int i = 0; i < nInstalledSlaveCANCount[nCh]; i++)
		{
			CString strTemp;
//			strTemp.Format("%d, %d, %d\r\n",
//				i, m_canData.canSetAllData.canSetData[nCh][i+nIndex].canType,
//				m_canData.canSetAllData.canSetData[nCh][i+nIndex].data_type);
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canData.canSetAllData.canSetData[nCh][i+nIndex].canType,
				m_canData.canSetAllData.canSetData[nCh][i+nIndex].data_type,
				m_canData.canSetAllData.canSetData[nCh][i+nIndex].name);
			strBuff+=strTemp;
		}
		strBuff += "\0";

		TRY
		{
			ZeroMemory(&szBuff, 5000);
			
			strFileName.Format("%s\\Config\\M%02dCh%02d.can", strSaveDir, GetModuleID(),nCh+1);
		

			if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite))
			{
				sprintf(szBuff, strBuff, strBuff.GetLength());
				int nLength = strBuff.GetLength();
				file.Write(szBuff, sizeof(char)*nLength);
				file.Close();
			}


			strBuff = "";
		}CATCH(CFileException, e)
		{
			e->ReportError();
			return;
		}
		END_CATCH

	}
}

int CCyclerModule::GetInstalledCanTransMaster(int nChIndex)
{
	return nInstalledMasterCANTransCount[nChIndex];
}

int CCyclerModule::GetInstalledCanTransSlave(int nChIndex)
{
	return nInstalledSlaveCANTransCount[nChIndex];
}

void CCyclerModule::MakeCANTrans()
{
	ZeroMemory(&m_canTransData, sizeof(SFT_MD_CAN_TRANS_INFO_DATA));
	memcpy(&m_canTransData, ::SFTGetCANTransInfoData(m_nModuleID), sizeof(SFT_MD_CAN_TRANS_INFO_DATA));
	ZeroMemory(&nInstalledMasterCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledSlaveCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	
	for(int i = 0 ; i < m_nTotalChannel; i++)
	{
		GetChannelInfo(i)->SetCanTransCount(m_canTransData.installedCanCount[i]);
		for(int j = 0 ; j < m_canTransData.installedCanCount[i]; j++)
		{
			TRACE("m_canTransData = %d\r\n",m_canTransData.installedCanCount[i]);
			if(m_canTransData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_MASTER)
				nInstalledMasterCANTransCount[i]++;
			else if(m_canTransData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_SLAVE)
				nInstalledSlaveCANTransCount[i]++;
			else
			{
				TRACE("정의되지 않은 CAN 데이터에 접근합니다.\r\n");
				break;
			}
		}
	}
	SaveCanTransConfig();
}

void CCyclerModule::SetCANSetTransData(int nChIndex, SFT_CAN_SET_TRANS_DATA *pCanSetData, int nCount)
{
	int nMaster, nSlave;
	nMaster = nSlave = 0;
	for(int i = 0; i < nCount; i++)
	{
		if(pCanSetData[i].canType == PS_CAN_TYPE_MASTER)
			nMaster++;
		else if(pCanSetData[i].canType == PS_CAN_TYPE_SLAVE)
			nSlave++;
	}
	nInstalledMasterCANTransCount[nChIndex] = nMaster;
	nInstalledSlaveCANTransCount[nChIndex] = nSlave;
	m_canTransData.installedCanCount[nChIndex] = nCount;
	
	GetChannelInfo(nChIndex)->SetCanTransCount(m_canTransData.installedCanCount[nChIndex]);
	
	memcpy(&m_canTransData.canSetAllData.canSetData[nChIndex], pCanSetData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);
}

void CCyclerModule::SaveCanTransConfig()
{
	CString strBuff;
	char szBuff[5000];
	CString strFileName;
	CFile file;
	CFileFind aFind;

	CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";

	CString strFileFind = strSaveDir + "\\Config\\*.trs";
	BOOL bFind = aFind.FindFile(strFileFind);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}


	int i = 0;
	for(int nCh = 0 ; nCh < GetTotalChannel(); nCh++)
	{
		int i = 0;
		for(int i = 0 ; i < nInstalledMasterCANTransCount[nCh]; i++)
		{
			CString strTemp;
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canTransData.canSetAllData.canSetData[nCh][i].canType,
				m_canTransData.canSetAllData.canSetData[nCh][i].data_type,
				m_canTransData.canSetAllData.canSetData[nCh][i].name);
			strBuff+=strTemp;
		}
	
		int nIndex = nInstalledMasterCANTransCount[nCh];
		for(int i = 0; i < nInstalledSlaveCANTransCount[nCh]; i++)
		{
			CString strTemp;
//			strTemp.Format("%d, %d, %d\r\n",
//				i, m_canData.canSetAllData.canSetData[nCh][i+nIndex].canType,
//				m_canData.canSetAllData.canSetData[nCh][i+nIndex].data_type);
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canTransData.canSetAllData.canSetData[nCh][i+nIndex].canType,
				m_canTransData.canSetAllData.canSetData[nCh][i+nIndex].data_type,
				m_canTransData.canSetAllData.canSetData[nCh][i+nIndex].name);
			strBuff+=strTemp;
		}
		strBuff += "\0";

		TRY
		{
			ZeroMemory(&szBuff, 5000);
			
			strFileName.Format("%s\\Config\\M%02dCh%02d.trs", strSaveDir, GetModuleID(),nCh+1);
		

			if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite))
			{
				sprintf(szBuff, strBuff, strBuff.GetLength());
				int nLength = strBuff.GetLength();
				file.Write(szBuff, sizeof(char)*nLength);
				file.Close();
			}


			strBuff = "";
		}CATCH(CFileException, e)
		{
			e->ReportError();
			return;
		}
		END_CATCH

	}
}

void CCyclerModule::SetCANCommonTransData(int nChIndex, SFT_CAN_COMMON_TRANS_DATA commonData, int nType)
{
	memcpy(&m_canTransData.canSetAllData.canCommonData[nChIndex][nType-1], &commonData, sizeof(SFT_CAN_COMMON_TRANS_DATA));
}

SFT_CAN_SET_TRANS_DATA CCyclerModule::GetCANSetTransData(int nChIndex, int nType, int nCanIndex)
{
	if(nType == PS_CAN_TYPE_MASTER)
		return m_canTransData.canSetAllData.canSetData[nChIndex][nCanIndex];
	else if(nType == PS_CAN_TYPE_SLAVE)
		return m_canTransData.canSetAllData.canSetData[nChIndex][nInstalledMasterCANTransCount[nChIndex] + nCanIndex];
}

SFT_MD_CAN_TRANS_INFO_DATA * CCyclerModule::GetCanTransAllData()
{
	return &m_canTransData;
}

SFT_CAN_COMMON_TRANS_DATA CCyclerModule::GetCANCommonTransData(int nChIndex, int nType)
{
	return m_canTransData.canSetAllData.canCommonData[nChIndex][nType-1];
}
