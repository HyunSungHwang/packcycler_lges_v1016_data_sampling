#if !defined(AFX_DLG_ALLGRAPH_H__1B34CEF5_5DA1_4577_B18D_3AA99D620480__INCLUDED_)
#define AFX_DLG_ALLGRAPH_H__1B34CEF5_5DA1_4577_B18D_3AA99D620480__INCLUDED_

#include "GraphWnd.h"
#include "RealGraphWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Allgraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_Allgraph dialog

class Dlg_Allgraph : public CDialog
{
// Construction
public:
	Dlg_Allgraph(CWnd* pParent = NULL);   // standard constructor

	BOOL m_bStartDraw;

	CRealGraphWnd_V2 m_wndGraphChannel;
	CRealGraphWnd_V2 m_wndGraphAux;
	CRealGraphWnd_V2 m_wndGraphCan;

	BOOL Fun_DrawFrame();
	void Fun_HideGraph();
	BOOL Fun_InitGraph();

	BOOL onStart();
	void onStop();	

// Dialog Data
	//{{AFX_DATA(Dlg_Allgraph)
	enum { IDD = IDD_DIALOG_ALLGRAPH };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Allgraph)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Allgraph)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern Dlg_Allgraph *mainTabAll;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_ALLGRAPH_H__1B34CEF5_5DA1_4577_B18D_3AA99D620480__INCLUDED_)
