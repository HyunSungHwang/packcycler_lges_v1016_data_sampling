// ChCaliData.h: interface for the CChCaliData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_)
#define AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CaliPoint.h"

typedef struct s_Calibraton_Data 
{
	double	dAdData;
	double	dMeterData;
}	CAL_DATA;

class CChCaliData : public CCaliPoint 
{
public:
	BOOL WriteDataToFile(FILE *fp);
	BOOL ReadDataFromFile(FILE *fp);
	//Multi Range FTN
	void GetVCalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetVCheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);
	void GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData);

	//1 Range FTN
	void GetVCalData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetVCheckData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetICalData(WORD wPoint, double &dAdData, double &dMeterData);
	void GetICheckData(WORD wPoint, double &dAdData, double &dMeterData);

	//Multi Range FTN
	void SetVCalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetVCheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);
	void SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData);

	CChCaliData();
	virtual ~CChCaliData();

protected:

	CAL_DATA	m_VCalData[SFT_MAX_VOLTAGE_RANGE][MAX_CALIB_SET_POINT];
	CAL_DATA	m_VCheckData[SFT_MAX_CURRENT_RANGE][MAX_CALIB_CHECK_POINT];
	CAL_DATA	m_ICalData[SFT_MAX_VOLTAGE_RANGE][MAX_CALIB_SET_POINT];
	CAL_DATA	m_ICheckData[SFT_MAX_CURRENT_RANGE][MAX_CALIB_CHECK_POINT];
};

#endif // !defined(AFX_CHCALIDATA_H__9F7DDAEF_1928_40D2_83F2_86298988C4DD__INCLUDED_)
