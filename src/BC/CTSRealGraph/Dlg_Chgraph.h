#if !defined(AFX_DLG_CHGRAPH_H__251BB3C9_10CB_4826_B8CB_1987ED9C6238__INCLUDED_)
#define AFX_DLG_CHGRAPH_H__251BB3C9_10CB_4826_B8CB_1987ED9C6238__INCLUDED_

#include "GraphWnd.h"
#include "RealGraphWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Chgraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chgraph dialog

class Dlg_Chgraph : public CDialog
{
// Construction
public:
	Dlg_Chgraph(CWnd* pParent = NULL);   // standard constructor

	BOOL m_bStartDraw;

	CRealGraphWnd_V2 m_wndGraphChannel;
	
	BOOL Fun_DrawFrame();
	void Fun_HideGraph();
	BOOL Fun_InitGraph();

	void onGraphMax();
	BOOL onStart();
	void onStop();

// Dialog Data
	//{{AFX_DATA(Dlg_Chgraph)
	enum { IDD = IDD_DIALOG_CHGRAPH };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Chgraph)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Chgraph)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern Dlg_Chgraph *mainTabCh;
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CHGRAPH_H__251BB3C9_10CB_4826_B8CB_1987ED9C6238__INCLUDED_)
