// Dlg_Allgraph.cpp : implementation file
//

#include "stdafx.h"
#include "ctsrealgraph.h"
#include "Dlg_Allgraph.h"

#include "CTSRealGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Allgraph dialog


Dlg_Allgraph::Dlg_Allgraph(CWnd* pParent /*=NULL*/)
	: CDialog(Dlg_Allgraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(Dlg_Allgraph)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bStartDraw=FALSE;
}


void Dlg_Allgraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Allgraph)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Allgraph, CDialog)
	//{{AFX_MSG_MAP(Dlg_Allgraph)
	ON_WM_SIZE()	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Allgraph message handlers

BOOL Dlg_Allgraph::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mainTabAll=this;
	
	m_wndGraphChannel.SubclassDlgItem(IDC_LAB_GRAPH_CHANNEL, this);
	m_wndGraphAux.SubclassDlgItem(IDC_LAB_GRAPH_AUX, this);
	m_wndGraphCan.SubclassDlgItem(IDC_LAB_GRAPH_CAN, this);
	
	m_wndGraphChannel.m_GraphInfo=&g_GraphInfo;
	m_wndGraphAux.m_GraphInfo=&g_GraphInfo;
	m_wndGraphCan.m_GraphInfo=&g_GraphInfo;

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_Allgraph::OnOK() 
{
	CDialog::OnOK();
}

void Dlg_Allgraph::OnCancel() 
{	
	CDialog::OnCancel();
}

BOOL Dlg_Allgraph::onStart()
{	
	if(m_bStartDraw == FALSE)
	{			
		if(m_wndGraphChannel.Start(0)==FALSE) return FALSE;
	    if(m_wndGraphAux.Start(1)==FALSE) return FALSE;
		if(m_wndGraphCan.Start(2)==FALSE) return FALSE;
	
		m_bStartDraw = TRUE;	
	}
	return TRUE;
}

void Dlg_Allgraph::onStop()
{
	m_wndGraphChannel.Stop();
	m_wndGraphAux.Stop();
	m_wndGraphCan.Stop();
	
	m_bStartDraw = FALSE;
}


BOOL Dlg_Allgraph::Fun_InitGraph()
{	
	BOOL bflag=Fun_DrawFrame();

	Fun_HideGraph();	
	
	m_wndGraphChannel.Fun_SubsetSetting(0);
	m_wndGraphAux.Fun_SubsetSetting(1);
	m_wndGraphCan.Fun_SubsetSetting(2);	
	return bflag;
}

BOOL Dlg_Allgraph::Fun_DrawFrame()
{
	if(!mainDlg) return FALSE;;

	int iCnt = 0;
	CString str;
	CString strTemp;
	
	if(m_wndGraphChannel.ClearGraph()==FALSE) return FALSE;
	if(m_wndGraphAux.ClearGraph()==FALSE) return FALSE;
	if(m_wndGraphCan.ClearGraph()==FALSE) return FALSE;

	iCnt = mainDlg->Fun_GetCheckChinfo();
    m_wndGraphChannel.m_iYCount=iCnt;

	int i=0;
	int nItem=0;
	
	for(int i = 0;i<4;i++)
	{// Channel Graph Subset Setting
		if(g_GraphInfo.ChInfo[i].bCheck==TRUE) 
		{
			m_wndGraphChannel.ShowSubset(i, iCnt, TRUE);

			if(i==0 && g_GraphInfo.ChInfo[i].bCheck==TRUE) 
			{//votage
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'V') str = "V";
				else str = "mV";			
			}
			else if(i==1 && g_GraphInfo.ChInfo[i].bCheck==TRUE)
			{//current
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'A') str = "A";
				else str = "mA";
			}
			else if(i==2 && g_GraphInfo.ChInfo[i].bCheck==TRUE)
			{//POWER
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'A') str = "Ah";
				else str = "mAh";
			}
			else if(i==3 && g_GraphInfo.ChInfo[i].bCheck==TRUE)
			{//capacity	
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'A')	str = "Ah";
				else str = "mAh";
			}

			strTemp.Format("%s (%s)",g_GraphInfo.ChInfo[i].szname, str);
			m_wndGraphChannel.SetYAxisLabel(i, strTemp);

			strTemp.Format("%s",g_GraphInfo.ChInfo[i].szname);
			m_wndGraphChannel.SetDataTitle (i, strTemp);
		}
	}

	nItem=0;	
	iCnt = mainDlg->Fun_GetCheckAuxinfo();	
	m_wndGraphAux.m_iYCount=iCnt;
	if(g_GraphInfo.nAuxCount>0)
	{
		for(int i = 0;i<g_GraphInfo.nAuxCount;i++)
		{// Aux Can Graph Subset Setting
						
			if(g_GraphInfo.AuxInfo[i].bCheck==TRUE) 
			{
				m_wndGraphAux.ShowSubset(nItem, iCnt, TRUE);    
			
				strTemp.Format("%s",g_GraphInfo.AuxInfo[i].strAlias);
				m_wndGraphAux.SetYAxisLabel(nItem, strTemp);
				m_wndGraphAux.SetDataTitle (nItem, strTemp);
				nItem=nItem+1;
			}
		}
	}

	nItem=0;
	iCnt = mainDlg->Fun_GetCheckCaninfo();		
	m_wndGraphCan.m_iYCount=iCnt;
	if(g_GraphInfo.nCanCount>0)
	{
		for(int i=0;i<g_GraphInfo.nCanCount;i++)
		{// Aux Can Graph Subset Setting		
			
			if(g_GraphInfo.CanInfo[i].bCheck==TRUE) 
			{
				m_wndGraphCan.ShowSubset(nItem, iCnt, TRUE); 
			
				strTemp.Format("%s",g_GraphInfo.CanInfo[i].strAlias);
				m_wndGraphCan.SetYAxisLabel(nItem, strTemp);
				m_wndGraphCan.SetDataTitle (nItem, strTemp);
				nItem=nItem+1;
			}
		}
	}
	
	//Set X Axis Label
	m_wndGraphChannel.SetXAxisLabel("");
	m_wndGraphAux.SetXAxisLabel("");
	m_wndGraphCan.SetXAxisLabel("");
	
	//Set Title
	m_wndGraphChannel.SetMainTitle("Channel Graph");
	m_wndGraphAux.SetMainTitle("AUX Graph");
	m_wndGraphCan.SetMainTitle("CAN Graph");
	return TRUE;
}

void Dlg_Allgraph::Fun_HideGraph()
{
	if(!mainDlg) return;
	
	RECT rt;
	GetClientRect(&rt);
		
	m_wndGraphChannel.MoveWindow(rt.left+10,rt.top+10,rt.right-20,(rt.bottom/3)-20,TRUE);
	
	m_wndGraphAux.MoveWindow(rt.left+10,(rt.bottom/3),rt.right-20,(rt.bottom/3)-20,TRUE);
	
	m_wndGraphCan.MoveWindow(rt.left+10,((rt.bottom/3)*2)-10,rt.right-20,(rt.bottom/3)-20,TRUE);	
}

void Dlg_Allgraph::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	RECT r;
	CRect ctrlRect, ctrlGrid;
	if(::IsWindow(m_hWnd))
	{
		::GetClientRect(m_hWnd, &r);
							
 		if(::IsWindow(m_wndGraphChannel.GetSafeHwnd()))
 		{
			Fun_HideGraph();
 		}
	}		
}

BOOL Dlg_Allgraph::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_ESCAPE || pMsg->wParam==VK_RETURN)
		{
			return FALSE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
