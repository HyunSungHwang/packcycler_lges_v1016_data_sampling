#if !defined(AFX_ADDEDIT_H__51304A2D_E5FC_4593_AA74_6F84A7A2291C__INCLUDED_)
#define AFX_ADDEDIT_H__51304A2D_E5FC_4593_AA74_6F84A7A2291C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAddEdit dialog
#include "Resource.h"
class CAddEdit : public CDialog
{
// Construction
public:
	CAddEdit(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAddEdit)
	enum { IDD = IDD_EDITBOX2 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddEdit)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAddEdit)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDEDIT_H__51304A2D_E5FC_4593_AA74_6F84A7A2291C__INCLUDED_)
