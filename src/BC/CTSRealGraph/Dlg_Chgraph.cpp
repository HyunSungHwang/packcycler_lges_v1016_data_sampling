// Dlg_Chgraph.cpp : implementation file
//

#include "stdafx.h"
#include "ctsrealgraph.h"
#include "Dlg_Chgraph.h"

#include "CTSRealGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chgraph dialog


Dlg_Chgraph::Dlg_Chgraph(CWnd* pParent /*=NULL*/)
	: CDialog(Dlg_Chgraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(Dlg_Chgraph)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bStartDraw=FALSE;
}


void Dlg_Chgraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Chgraph)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Chgraph, CDialog)
	//{{AFX_MSG_MAP(Dlg_Chgraph)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Chgraph message handlers

BOOL Dlg_Chgraph::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mainTabCh=this;
	
	m_wndGraphChannel.SubclassDlgItem(IDC_LAB_GRAPH_CHANNEL1, this);
	m_wndGraphChannel.m_GraphInfo=&g_GraphInfo;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_Chgraph::OnOK() 
{
	CDialog::OnOK();
}

void Dlg_Chgraph::OnCancel() 
{	
	CDialog::OnCancel();
}

BOOL Dlg_Chgraph::onStart()
{
	if(m_bStartDraw == FALSE)
	{		
		if(m_wndGraphChannel.Start(0)==FALSE) return FALSE;
		
		m_bStartDraw = TRUE;	
	}
	return TRUE;
}

void Dlg_Chgraph::onStop()
{
	m_wndGraphChannel.Stop();
	
	m_bStartDraw = FALSE;
	
}

BOOL Dlg_Chgraph::Fun_InitGraph()
{
	BOOL bflag=Fun_DrawFrame();
	Fun_HideGraph();

	m_wndGraphChannel.Fun_SubsetSetting(0);	
	return bflag;
}

BOOL Dlg_Chgraph::Fun_DrawFrame()
{
	if(!mainDlg) return FALSE;

	int iCnt = 0;
	CString str;
	CString strTemp;
	
	if(m_wndGraphChannel.ClearGraph()==FALSE) return FALSE;
		
	iCnt = mainDlg->Fun_GetCheckChinfo();
	m_wndGraphChannel.m_iYCount=iCnt;

	for(int i=0;i<4;i++)
	{// Channel Graph Subset Setting

		if(g_GraphInfo.ChInfo[i].bCheck==TRUE) 
		{
			m_wndGraphChannel.ShowSubset(i, iCnt, TRUE);
			
			if(i==0 && g_GraphInfo.ChInfo[i].bCheck==TRUE) 
			{//votage
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'V') str = "V";
				else str = "mV";			
			}
			else if(i==1 && g_GraphInfo.ChInfo[i].bCheck==TRUE)
			{//current
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'A') str = "A";
				else str = "mA";
			}
			else if(i==2 && g_GraphInfo.ChInfo[i].bCheck==TRUE)
			{//POWER
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'A') str = "Ah";
				else str = "mAh";
			}
			else if(i==3 && g_GraphInfo.ChInfo[i].bCheck==TRUE)
			{//capacity	
				if(g_GraphInfo.ChInfo[i].cUnit[0] == 'A')	str = "Ah";
				else str = "mAh";
			}
			
			strTemp.Format("%s (%s)",g_GraphInfo.ChInfo[i].szname, str);
			m_wndGraphChannel.SetYAxisLabel(i, strTemp);
			m_wndGraphChannel.SetDataTitle (i, g_GraphInfo.ChInfo[i].szname);
		}
	}
	
	//Set X Axis Label
	m_wndGraphChannel.SetXAxisLabel("");
	
	//Set Title
	m_wndGraphChannel.SetMainTitle("Channel Graph");
	return TRUE;
}

void Dlg_Chgraph::Fun_HideGraph()
{
	if(!mainDlg) return;
	
	RECT rt;
	GetClientRect(&rt);
		
	GetDlgItem(IDC_LAB_GRAPH_CHANNEL1)->MoveWindow(rt.left+10,rt.top+10,rt.right-20,(rt.bottom)-20,TRUE);	
}

void Dlg_Chgraph::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	RECT r;
	CRect ctrlRect, ctrlGrid;
	if(::IsWindow(m_hWnd))
	{
		::GetClientRect(m_hWnd, &r);
		
		if(::IsWindow(m_wndGraphChannel.GetSafeHwnd()))
		{
			Fun_HideGraph();
		}
	}	
}

BOOL Dlg_Chgraph::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_ESCAPE || pMsg->wParam==VK_RETURN)
		{
			return FALSE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_Chgraph::onGraphMax() 
{
	m_wndGraphChannel.SetMaximize();		
}
