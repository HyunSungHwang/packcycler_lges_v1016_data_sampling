// CyclerChannel.cpp: implementation of the CCyclerChannel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CTSRealGraph.h"
#include "CyclerChannel.h"

#include <io.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define _MAX_DISK_WIRTE_COUNT	256

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCyclerChannel::CCyclerChannel(UINT nModuleID, UINT nChannelIndex):m_nModuleID(nModuleID), m_nChannelIndex(nChannelIndex)
{
	m_nModuleID = 0;
	m_bDataLoss = FALSE;

	m_fCapacitySum = 0;
	m_fMeterValue = 0.0f;
	
	m_pScheduleData = NULL;
	m_nAuxCount = 0;
	m_nTempAuxCount = 0;
	m_nVoltAuxCount = 0;
	m_bParallel = FALSE;
	m_bMaster = FALSE;
	m_uMasterCh = 0;

/*	ZeroMemory(&m_FileRecordSetHeader, sizeof(PS_RECORD_FILE_HEADER));	
	m_FileRecordSetHeader.nColumnCount = RAWDATA_FILE_COL_SIZE;
	m_FileRecordSetHeader.awColumnItem[0] = PS_DATA_INDEX;					//Data Index Sequence
	m_FileRecordSetHeader.awColumnItem[1] = PS_STEP_TIME;
	m_FileRecordSetHeader.awColumnItem[2] = PS_VOLTAGE;
	m_FileRecordSetHeader.awColumnItem[3] = PS_CURRENT;
	m_FileRecordSetHeader.awColumnItem[4] = PS_CAPACITY;
	m_FileRecordSetHeader.awColumnItem[5] = PS_WATT_HOUR;
*/
	LoadSaveItemSet();
}

CCyclerChannel::CCyclerChannel()
{
	m_nModuleID = 0;
	m_bDataLoss = FALSE;

	m_fCapacitySum = 0;
	m_fMeterValue =0.0f;
	m_pScheduleData = NULL;
	m_nAuxCount = 0;
	m_nTempAuxCount = 0;
	m_nVoltAuxCount = 0;
	m_nCanCount	= 0;
	m_nCanTransCount=0;
	m_bParallel = FALSE;
	m_bMaster = FALSE;
	m_uMasterCh = 0;

/*	ZeroMemory(&m_FileRecordSetHeader, sizeof(PS_RECORD_FILE_HEADER));	
	m_FileRecordSetHeader.nColumnCount = RAWDATA_FILE_COL_SIZE;
	m_FileRecordSetHeader.awColumnItem[0] = PS_DATA_INDEX;					//Data Index Sequence
	m_FileRecordSetHeader.awColumnItem[1] = PS_STEP_TIME;
	m_FileRecordSetHeader.awColumnItem[2] = PS_VOLTAGE;
	m_FileRecordSetHeader.awColumnItem[3] = PS_CURRENT;
	m_FileRecordSetHeader.awColumnItem[4] = PS_CAPACITY;
	m_FileRecordSetHeader.awColumnItem[5] = PS_WATT_HOUR;
*/
	LoadSaveItemSet();
}

CCyclerChannel::~CCyclerChannel()
{
	//아직 파일로 저장되지 않고 버퍼에 남아있는 data를 모두 저장한다.
//	CheckAndSaveDataB();

	if(m_pScheduleData)
	{
		delete m_pScheduleData;
		m_pScheduleData = NULL;
	}

	//외부 데이터 메모리 해제//////////////////////////////////////////////////
/*	if(auxList.GetCount() > 0)
	{
		POSITION pos = auxList.GetHeadPosition();
		while(pos)
		{
			CChannelSensor * pTemp = (CChannelSensor *)auxList.GetAt(pos);
			delete pTemp;
			auxList.GetNext(pos);

		}
	}

	auxList.RemoveAll();*/
	///////////////////////////////////////////////////////////////////////////
}

CChannel * CCyclerChannel::GetChannel()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
}

void CCyclerChannel::SetID(UINT nModuleID, UINT nChIndex)
{
	m_nModuleID = nModuleID;
	m_nChannelIndex = nChIndex;
}

long CCyclerChannel::GetVoltage()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_VOLTAGE);
}

long CCyclerChannel::GetCurrent()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CURRENT);
}

long CCyclerChannel::GetCapacity()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITY);
}
ULONG CCyclerChannel::GetStepTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (ULONG)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_TIME);
}

ULONG CCyclerChannel::GetTotTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (ULONG)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_TOT_TIME);
}

WORD CCyclerChannel::GetState()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STATE);
}

WORD CCyclerChannel::GetStepNo()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_NO);
}

WORD CCyclerChannel::GetStepType()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_TYPE);
}

int CCyclerChannel::GetTotalCycleCount()
{
	return (int)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_TOT_CYCLE);

}

int CCyclerChannel::GetCurCycleCount()
{
	return (int)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CUR_CYCLE);
}

long CCyclerChannel::GetWattHour()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_WATT_HOUR);
}

long CCyclerChannel::GetWatt()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_WATT);
}

long CCyclerChannel::GetAvgVoltage()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AVG_VOLTAGE);
}

long CCyclerChannel::GetAvgCurrent()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AVG_CURRENT);
}

long CCyclerChannel::GetTemperature()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_OVEN_TEMPERATURE);
}

long CCyclerChannel::GetAuxVoltage()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AUX_VOLTAGE);
}

//초기 step에서 현재 step까지의 용량의 총합
long CCyclerChannel::GetCapacitySum()
{
	WORD type = GetStepType();
	if(type == PS_STEP_CHARGE)
	{
		return long(m_fCapacitySum + ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITY));
	}
	else if ( type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE)
	{
		return long(m_fCapacitySum - ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITY));
	}

	return (long)m_fCapacitySum;
}


//return 
CString CCyclerChannel::GetTestName()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return m_strTestName;
}

long CCyclerChannel::GetImpedance()
{
	return (long)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_IMPEDANCE);
}


UINT CCyclerChannel::GetStackedSize()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	
	if(pChannel == NULL)	return 0;

	SFT_MD_SYSTEM_DATA * pSys = ::SFTGetModuleSysData(m_nModuleID);

	//if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION)
	if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : v1015 호환
		return pChannel->GetSaveDataStackSizeA();
	else
		return pChannel->GetSaveDataStackSize();
}

BOOL CCyclerChannel::CreateResultFileA(CString strFileName)
{
	CFile f;
	CFileException e;

	if(strFileName.IsEmpty())	return FALSE;

	if( !f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   return FALSE;
	}

	LPPS_FILE_ID_HEADER pFileHeader = new PS_FILE_ID_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	pFileHeader->nFileID = PS_ADP_RECORD_FILE_ID;
	pFileHeader->nFileVersion = _ADP_RECORD_FILE_VER;
	CTime t = CTime::GetCurrentTime();
	sprintf(pFileHeader->szCreateDateTime, "%s", t.Format("%y/%m/%d %H:%M:%S"));
	sprintf(pFileHeader->szDescrition, "%s", "PNE record data file.");

	f.Write(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	delete pFileHeader;

/*	PS_RECORD_FILE_HEADER *pRSHeader = new PS_RECORD_FILE_HEADER;
	ZeroMemory(pRSHeader, sizeof(PS_RECORD_FILE_HEADER));	
	pRSHeader->nColumnCount = RAWDATA_FILE_COL_SIZE;
	pRSHeader->awColumnItem[0] = PS_DATA_INDEX;					//Data Index Sequence
	pRSHeader->awColumnItem[1] = PS_STEP_TIME;
	pRSHeader->awColumnItem[2] = PS_VOLTAGE;
	pRSHeader->awColumnItem[3] = PS_CURRENT;
	pRSHeader->awColumnItem[4] = PS_CAPACITY;
	pRSHeader->awColumnItem[5] = PS_WATT_HOUR;
*/

	//Data 복구시

	f.Write(&m_FileRecordSetHeader, sizeof(PS_RECORD_FILE_HEADER));

	

//	delete pRSHeader;

	f.Flush();
	f.Close();

	m_strDataList.RemoveAll();
	
	WriteLog(strFileName+" 생성");

	return TRUE;
}


//Aux 추가 2007/08/01
//DEL int CCyclerChannel::CheckAndSaveDataC()
//DEL {	
//DEL 	//파일명 설정 여부 확인 
//DEL 	if(m_strFilePath.IsEmpty())
//DEL 	{
//DEL 		return 0;
//DEL 	}
//DEL 
//DEL 	//외부에서 파일 조작을 할 경우file update를 Lock 시킨 후 update 실시하기 위함 
//DEL 	if(m_bLockFileUpdate == TRUE)
//DEL 	{
//DEL 		TRACE("Ch %d file update is locked\n", m_nChannelIndex+1);
//DEL 		return 0;
//DEL 	}
//DEL 
//DEL 	//저장할 Data가 없으면 return
//DEL 	if(GetStackedSize() < 1)	return 0;
//DEL 
//DEL 	int nRtn = 1;
//DEL 
//DEL 	//Add 2006/8/18
//DEL 	//Load last temp data
//DEL 	//////////////////////////////////////////////////////////////////////////
//DEL 	PS_STEP_END_RECORD prevData;
//DEL 	CString strStartTime, strTestSerial, strUser, strDescript;
//DEL 	long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
//DEL 	TRACE("Last data start index %d\n", lStartIndex);
//DEL 	//////////////////////////////////////////////////////////////////////////
//DEL 
//DEL //	TRACE("Channel %d data save start\n", m_nChannelIndex+1);
//DEL 	
//DEL 	//2007/08/09 Aux Data 저장용//////////////////////////////////
//DEL 	//채널 데이터가 저장하는 방식을 빠른다.
//DEL 
//DEL 	PS_AUX_RAW_DATA sAuxData;
//DEL 	float *pfAuxBuff = NULL;
//DEL 	size_t auxSize;
//DEL 	size_t auxLineSize;
//DEL 	CFile auxFile;
//DEL 	PS_AUX_FILE_HEADER auxHeader;
//DEL 	int nAuxColumnSize;
//DEL 	PS_AUX_RAW_DATA auxData;
//DEL 	
//DEL 
//DEL 	if(nRtn = GetAuxFile(&auxFile, &auxHeader) < 0)	
//DEL 	{
//DEL 		return nRtn;
//DEL 	}
//DEL 	
//DEL 	nAuxColumnSize = auxHeader.auxHeader.nColumnCount;		//save data setting
//DEL 	
//DEL 	if(nAuxColumnSize < 0 || nAuxColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
//DEL 	{
//DEL 		CString strMsg;
//DEL 		strMsg.Format("AuxColumn : %d", nAuxColumnSize);
//DEL 		WriteLog(strMsg);
//DEL 		auxFile.Close();
//DEL 		return -1;
//DEL 	}
//DEL 		
//DEL 	if(nAuxColumnSize > 0)
//DEL 	{
//DEL 		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
//DEL 		auxLineSize =  sizeof(float)*nAuxColumnSize;
//DEL 		pfAuxBuff = new float[nAuxColumnSize*_MAX_DISK_WIRTE_COUNT];
//DEL 		auxSize = auxLineSize*_MAX_DISK_WIRTE_COUNT;
//DEL 		
//DEL 		//Line 수를 Count
//DEL 		int nAuxBodySize = auxFile.GetLength()-sizeof(PS_AUX_FILE_HEADER);
//DEL 		int nAuxRsCount = nAuxBodySize / auxLineSize ;
//DEL 		if(nAuxRsCount < 0 || nAuxBodySize % auxLineSize)
//DEL 		{
//DEL 			//		TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
//DEL 			auxFile.Close();
//DEL 			return -2;
//DEL 		}
//DEL 	}
//DEL 	
//DEL 	//////////////////////////////////////////////////////////////
//DEL 	
//DEL 	//채널 데이터 저장용///////////////////////////////////////////////
//DEL 	PS_RAW_FILE_HEADER rawHeader;
//DEL 	CFile fRS;
//DEL 	PS_STEP_END_RECORD	sChResultData;
//DEL 	float *pfBuff;
//DEL 	size_t rsSize, lineSize;
//DEL 
//DEL 	if(nRtn = GetChFile(fRS,rawHeader) < 0)
//DEL 	{
//DEL 		return nRtn;
//DEL 	}
//DEL 	
//DEL 	int nColumnSize = rawHeader.rsHeader.nColumnCount;		//save data setting
//DEL 
//DEL 	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
//DEL 	{
//DEL 		auxFile.Close();
//DEL 		fRS.Close();
//DEL 		return -3;
//DEL 	}
//DEL 
//DEL 	//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
//DEL 	lineSize = sizeof(float)*nColumnSize;
//DEL 	pfBuff = new float[nColumnSize*_MAX_DISK_WIRTE_COUNT];
//DEL 	rsSize = lineSize*_MAX_DISK_WIRTE_COUNT;
//DEL 
//DEL 	//Line 수를 Count
//DEL 	int nBodySize = fRS.GetLength()-sizeof(PS_RAW_FILE_HEADER);
//DEL 	int nRsCount = nBodySize / lineSize ;
//DEL 	if(nRsCount < 0 || nBodySize % lineSize)
//DEL 	{
//DEL //		TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
//DEL 		auxFile.Close();
//DEL 		fRS.Close();
//DEL 		return -4;
//DEL 	}
//DEL 	
//DEL 	int nStackCount = 0;
//DEL 	
//DEL 
//DEL #ifdef _DEBUG
//DEL 	DWORD dwStart = GetTickCount();
//DEL #endif
//DEL 	
//DEL 	while(GetSaveDataB(sChResultData, sAuxData))	//저장할 Data가 있을 경우 Stack에서 1개 Pop 한다.
//DEL 	{
//DEL 		//sChResultData.lSaveSequence => 1 Base No
//DEL 		if((sChResultData.lSaveSequence-1)  > nRsCount)		//Data loss 발생
//DEL 		{
//DEL 			//중간부 Data 손실 여부만 확인 가능하다. 즉 손실 이후 최소 한번이상의 결과 Data가 갱신되지 않으면 
//DEL 			//자동 검출 불가하다. 
//DEL 			m_bDataLoss = TRUE;					 
//DEL //			TRACE("*****************>>>>>>>>>>>>>> Data loss detected(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
//DEL 			nRtn = 0;
//DEL 		}
//DEL 		else if((sChResultData.lSaveSequence-1)  < nRsCount)	//Index가 역전된 경우 
//DEL 		{
//DEL 			//작업중에 복구 명령을 수행하면 Download와 복구 중에 발생한 Buffering data가 중복되게 된다.
//DEL 			//이럴경우 현재 송신된 data(버퍼에 있던 data)는 이미 모듈에서 download하여 복구한 data이므로 
//DEL 			//저장하지 않고 Skip한다.
//DEL //			nPreviousStepNo = sChResultData.chStepNo;
//DEL //			nPrevTotalCycleNo = sChResultData.nTotalCycleNum;
//DEL //			TRACE("*****************>>>>>>>>>>>>>> Data index ???(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
//DEL 			continue;
//DEL 		}
//DEL 		else												//정상적인 data
//DEL 		{
//DEL //			TRACE("*****************>>>>>>>>>>>>>> Data index %d received\n", sChResultData.lSaveSequence);
//DEL 			m_bDataLoss = FALSE;
//DEL 		}
//DEL 
//DEL 		//Data 저장시 모든 단위를 m단위로 저장한다. (실처리 항목은 최소로 한다.) 파일 Size 문제...
//DEL 		for(int dataIndex = 0; dataIndex <nColumnSize ; dataIndex++)
//DEL 		{
//DEL 			pfBuff[dataIndex + nStackCount * nColumnSize] = GetItemData(sChResultData, rawHeader.rsHeader.awColumnItem[dataIndex]);	//Index 5 is WH	
//DEL 		}		
//DEL 	
//DEL 		for(int nAuxDataIndex = 0; nAuxDataIndex < nAuxColumnSize ; nAuxDataIndex++)
//DEL 		{
//DEL 			pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = sAuxData.auxData[nAuxDataIndex].fValue;
//DEL 		}	
//DEL 			
//DEL 	
//DEL //		TRACE("V:%d/I:%d/C:%d\n", sChResultData.lVoltage,sChResultData.lCurrent,sChResultData.lCapacity );
//DEL 
//DEL 		//////////////////////////////////////////////////////////////////////////
//DEL 		//2006/8/4/
//DEL 		//result file save type mode.
//DEL 
//DEL 		//2. Binary Mode file, TextMode temp. file
//DEL 		lStartIndex = WriteResultListFileF(lStartIndex, nRsCount, sChResultData, sAuxData);
//DEL 		if(lStartIndex < 0)
//DEL 		{
//DEL 			nRtn = -5;
//DEL 		}
//DEL 
//DEL 
//DEL 		//////////////////////////////////////////////////////////////////////////
//DEL 		nRsCount++;
//DEL 		nStackCount++;
//DEL 
//DEL 		//Disk write 성능 향상을 위해 한꺼번에 기록 하도록 수정 
//DEL 		if(nStackCount >= _MAX_DISK_WIRTE_COUNT)
//DEL 		{
//DEL 			fRS.Write(pfBuff, rsSize);
//DEL 
//DEL 			if(nAuxColumnSize > 0)
//DEL 			{
//DEL 				auxFile.Write(pfAuxBuff, auxSize);
//DEL 				CString strMsg;
//DEL 				strMsg.Format("ChNo %d : Stack >= MAX 파일에 기록(Aux 수 : %d)",m_nChannelIndex+1,nAuxColumnSize) ;
//DEL 				WriteLog(strMsg);
//DEL 				//	strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
//DEL 				//		pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
//DEL 				//		pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
//DEL 				
//DEL 			//	TRACE(strMsg);
//DEL 			}
//DEL 			
//DEL 
//DEL 			nStackCount = 0;
//DEL 		}
//DEL 	}
//DEL 
//DEL 	//Disk access 횟수를 줄이기 위해 한꺼번에 저장 
//DEL 	if(nStackCount > 0)
//DEL 	{
//DEL 		fRS.Write(pfBuff, sizeof(float)*nColumnSize*nStackCount);
//DEL 
//DEL 		if(nAuxColumnSize > 0)
//DEL 		{
//DEL 			auxFile.Write(pfAuxBuff, sizeof(float)*nAuxColumnSize*nStackCount);
//DEL 			CString strMsg;
//DEL 			strMsg.Format("ChNo %d : Stack > 0 파일에 기록(Aux 수 : %d)",m_nChannelIndex+1,nAuxColumnSize) ;
//DEL 			WriteLog(strMsg);
//DEL 			TRACE(" Aux Column : %d\r\n", nAuxColumnSize);
//DEL 			//CString strMsg;
//DEL 			//strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
//DEL 			//	pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
//DEL 			//	pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
//DEL 			
//DEL 			//	TRACE(strMsg);
//DEL 		}
//DEL 		
//DEL 	
//DEL 	
//DEL 		//2006/8/18
//DEL 		//Update Last temp file
//DEL 		UpdateLastData(UINT(lStartIndex), nRsCount-1, strStartTime, strTestSerial, strUser, strDescript, sChResultData);
//DEL 	}
//DEL 
//DEL #ifdef _DEBUG
//DEL 	if(m_nChannelIndex == 0)
//DEL 		TRACE("Channel %d data save time : %dms elapsed\n",  m_nChannelIndex+1, GetTickCount()-dwStart);
//DEL #endif
//DEL 
//DEL //	if(sChResultData.chNo == 1)
//DEL //		TRACE("===========================>CH%03d-S02d: [%dmsec]\n", sChResultData.chNo, sChResultData.chStepNo, GetTickCount()-dwTime);
//DEL 
//DEL 	//
//DEL 	if(pfBuff != NULL)
//DEL 	{	
//DEL 		delete[]	pfBuff;		
//DEL 		pfBuff = NULL;
//DEL 	}
//DEL 	if(pfAuxBuff != NULL)
//DEL 	{
//DEL 		delete [] pfAuxBuff;
//DEL 		pfAuxBuff = NULL;
//DEL 	}
//DEL 		
//DEL 	if (fRS.m_hFile != CFile::hFileNull)
//DEL 	{
//DEL 		fRS.Flush();
//DEL 		fRS.Close();
//DEL 		auxFile.Flush();
//DEL 		auxFile.Close();
//DEL 	}
//DEL 	TRACE("Channel %d 결과 Data 저장\n", m_nChannelIndex+1);
//DEL 
//DEL 	return nRtn;
//DEL }

//파일에 저장된 1줄의 String을 Parsing하여 step Data를 추출 한다.
//No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,Time,Code,Grade,Voltage,Current,Capacity,WattHour,DCIR,
// strvalue => data1,data2,data3,data4,data5,data6,data7,
int CCyclerChannel::ParsingChDataString(CString strValue, PS_STEP_END_RECORD &sData)
{
	CString strTemp, strSource;
	strSource = strValue;

//	int nStepNo = 0;
	int index;
	
	//No
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chStepNo = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Index From
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nIndexFrom = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//IndexTo
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nIndexTo = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;
	
	//Current Cycle
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nCurrentCycleNum = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;
	
	//TotalCycle
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nTotalCycleNum = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Type
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chStepType = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Time
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fStepTime = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Code
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chCode = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Grade
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chGradeCode = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Voltage
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fVoltage = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//current
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fCurrent = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//capacity
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fCapacity = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//WattHour
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fWattHour = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//DCIR
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fImpedance = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	return TRUE;
}

void CCyclerChannel::SetTestName(CString strTestName)
{
	m_strTestName = strTestName;
}

void CCyclerChannel::SetFilePath(CString strPath, BOOL bReset)
{
	//c:\\testname\\M01C02
	m_strFilePath = strPath;		//set the file path;

	CString strTemp;
	int index = m_strFilePath.ReverseFind('\\');
	if(index >=0 )
	{
		strTemp = m_strFilePath.Left(index);		//c:\\testname

		index = strTemp.ReverseFind('\\');
		if(index >=0 )
		{
			SetTestName(strTemp.Mid(index+1));							//m_strTestName = testname
			SetScheduleFile(m_strFilePath+"\\"+m_strTestName+".sch");	//m_strScheduleFileName = c:\\testname\\M01C02\\testname.sch
			SetLogFile(m_strFilePath+"\\"+m_strTestName+".log");	//m_strLogFileName =  = c:\\testname\\M01C02\\testname.log
		
			//Set Schedule Name
			CScheduleData *pSchedule = new CScheduleData;
			pSchedule->SetSchedule(m_strScheduleFileName);
			SetScheduleName(pSchedule->GetScheduleName());				//m_strScheduleName
			delete pSchedule;
					
			m_chFileDataSet.SetPath(m_strFilePath);
		}
	}
	
	//Update Test Start Time
	if(bReset)
		UpdateStartTime();
}


BYTE CCyclerChannel::GetGradeCode()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (char)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_GRADE_CODE);
}

WORD CCyclerChannel::GetCellCode()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CODE);
}

// CCalibrationData * CCyclerChannel::GetCaliData()
// {
// 	return &m_CaliData;
// }

CString CCyclerChannel::GetFilePath()
{
	return m_strFilePath;
}

CString CCyclerChannel::GetGradeString()
{
	CString msg;
	msg.Format("%c", GetGradeCode());
	return msg;
}

BOOL CCyclerChannel::GetRecordIndex(CString strString, long &lFrom, long &lTo)
{
	CString strTemp, strSource;
	strSource = strString;

//	int nLineNo = 0;
	int index;
	
	//No
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
//		nLineNo = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else return FALSE;

	//IndexFrom
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		lFrom = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else return FALSE;

	//IndexTo
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		lTo = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else return FALSE;
	
	return TRUE;
}

int CCyclerChannel::CompletedTest()
{
	SFT_MD_SYSTEM_DATA * pSysData = ::SFTGetModuleSysData(m_nModuleID);
	//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION)
	if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : v1015 호환
		return CheckAndSaveData();

	return CheckAndSaveDataA(); 
}

CString CCyclerChannel::GetTestPath()
{
	if(m_strFilePath.IsEmpty())		return "";

	int index = m_strFilePath.ReverseFind('\\');
	if(index < 0)			return	"";

	return m_strFilePath.Left(index);
}

//새로운 시험의 시작
void CCyclerChannel::ResetData()
{
	m_bDataLoss = FALSE;
	m_bLockFileUpdate = FALSE;

	m_strDataLossRangeList.RemoveAll();
	m_strDataList.RemoveAll();

	m_strDescript.Empty();
	m_strUserID.Empty();
	m_strTestSerial.Empty();			//Test Serial or Lot No
	m_strScheduleFileName.Empty();		//C:\\Test\M01C01\Test.sch
	m_strTestName.Empty();				//Test 명 
	m_strFilePath.Empty();				//C:\\Test\M01C01
	m_strScheduleName.Empty();

//	m_chFileDataSet.ResetData();
//	CCalibrationData	m_CaliData;			//채널의 교정값 
//	COleDateTime	m_startTime;
//	CModuleIndexTable	m_RestoreFileIndexTable;

	m_fCapacitySum = 0;
	m_fMeterValue = 0.0f;

	::SFTInitChannel(m_nModuleID, m_nChannelIndex);
}

//.\\Temp\\에서 자기 채널의 임시파일(가장 마지막에 시행한 시험정보가 있음)을 읽어 Setting한다.
BOOL CCyclerChannel::LoadInfoFromTempFile()
{
	CString strTemp, strMsg, strFileName;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));;

	strFileName.Format("%s\\Temp\\M%02dC%02d.tmp", strTemp, m_nModuleID, m_nChannelIndex+1);

	CT_TEMP_FILE_DATA tempData;
	ZeroMemory(&tempData, sizeof(CT_TEMP_FILE_DATA));

	FILE *fp = fopen(strFileName, "rt");

	if(fp == NULL)		return FALSE;
	
	//File Path
	fread(&tempData, sizeof(CT_TEMP_FILE_DATA), 1, fp);

	SetFilePath(tempData.szFilePath);
	SetTestSerial(tempData.szLot, tempData.szWorker, tempData.szDescript);

	//2006/6/12 명령어 예약 정보 기록 
	int lTempData;
	if(fread(&lTempData, sizeof(long), 1, fp) > 0)
	{
		m_lReservedCycleNo = lTempData;
		if(fread(&lTempData, sizeof(long), 1, fp) > 0)
		{
			m_lReservedStepNo = lTempData;
		}
	}

	fclose(fp);

	//Commented by KBH 2006/7/27
	//data가 많은 다수의 채널이 있을 경우 초기 접속시 시간이 지연시킨다.
	//
/*
	//2006/5/15
	//결과 파일을 읽어서 용량 합산을 구한다.
	if(ReloadReaultData(FALSE) == FALSE)		//현재 정상적으로 종료된 최종 Step까지만 Loading 한다.
	{
		m_fCapacitySum = 0;
	}
	else 
	{
		//현재 진행중에 멈춘 step을 제외한 모든 step의 용량 합산을 구한다.
		m_fCapacitySum = (m_chFileDataSet.GetCapacitySum()*1000.0f);	//mAh=>uAh로 변경 or uAh=>nAh로 변경 
	}
	TRACE("Capacity sum => %.3f\n", m_lCapacitySum/1000.0f);
	//
*/
	return TRUE;
}

// 기존 파일을 읽어서 손상된 구간은 Module에서 DownLoad한다.
BOOL CCyclerChannel::RestoreLostData(CString strIPAddress)
{
	//strIPAddress == "192.168.5.84";
	CString srcFileName, destFileName;
	CString strTemp;

	if(strIPAddress.IsEmpty())
	{
		strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	m_nModuleID);
		AfxMessageBox(strTemp);
		return 0;
	}

//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), "복구중...", TRUE, TRUE, TRUE);
	progressWnd.SetRange(0, 100);
//////////////////////////////////////////////////////////////////////////
/*
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	//Get ip address

//////////////////////////////////////////////////////////////////////////
//	strIPAddress = "192.168.9.10";
//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
//////////////////////////////////////////////////////////////////////////
	
	strTemp.Format("Module %d에 접속 시도 중입니다.(IP %s)", m_nModuleID, strIPAddress);
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", m_nModuleID);
		delete pDownLoad;
		return FALSE;
	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;
	CString strAuxTempLocalFileName, strAuxVoltLocalFileName, strCanMLocalFileName, strCanSLocalFileName;
	CString strLocalTempFolder(szBuff);
	CString strStartIndexFile, strEndIndexFile, strStepEndFile;
	CString strAuxTStepEndFile, strAuxVStepEndFile, strCanMStepEndFile, strCanSStepEndFile; 

	//1. 시작 Index파일을 DownLoad한다.
	strTemp.Format("M%02dCH%02d Index 시작 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(10);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

	strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", m_nChannelIndex+1);
	strStartIndexFile = strLocalTempFolder + "\\savingFileIndex_start.csv";
	if(pDownLoad->DownLoadFile(strStartIndexFile, strRemoteFileName) < 1)
	{
		m_strLastErrorString.Format("M%02dCH%02d 시작 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		delete pDownLoad;
		return FALSE;
	}


	//2. 최종 Index 파일을 DownLoad한다.
	strTemp.Format("M%02dCH%02d Index 마지막 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(20);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

	strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_last.csv", m_nChannelIndex+1);
	strEndIndexFile = strLocalTempFolder + "\\savingFileIndex_last.csv";
	if(pDownLoad->DownLoadFile(strEndIndexFile, strRemoteFileName) < 1)
	{
		//가끔 download 실패가 발생한다.
		m_strLastErrorString.Format("M%02dCH%02d 최종 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		delete pDownLoad;
		TRACE("%s download fail\n", strRemoteFileName);
		return FALSE;
	}

	//3. Step End Data 파일을 DownLoad한다.
	strTemp.Format("M%02dCH%02d 각 Step의 완료 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(30);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

	strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveEndData.csv", m_nChannelIndex+1, m_nChannelIndex+1);
	strStepEndFile = strLocalTempFolder + "\\stepEndData.csv";
	if(pDownLoad->DownLoadFile(strStepEndFile, strRemoteFileName) < 1)
	{
		//가끔 download 실패가 발생한다.
		m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		delete pDownLoad;
		TRACE("%s download fail\n", strRemoteFileName);
		return FALSE;
	}

	//Aux용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
	if(m_chFileDataSet.m_nAuxTempCount > 0)
	{
		strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveEndData_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strAuxTStepEndFile = strLocalTempFolder + "\\stepAuxTEndData.csv";
		if(pDownLoad->DownLoadFile(strAuxTStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.
			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}
	}
	
	if(m_chFileDataSet.m_nAuxVoltCount > 0)
	{
		strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveEndData_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strAuxVStepEndFile = strLocalTempFolder + "\\stepAuxVEndData.csv";
		if(pDownLoad->DownLoadFile(strAuxVStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.
			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Can용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
	if(m_chFileDataSet.m_nCanMasterCount > 0)
	{
		strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveEndData_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strCanMStepEndFile = strLocalTempFolder + "\\stepCanMEndData.csv";
		if(pDownLoad->DownLoadFile(strCanMStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.
			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}

	}
	
	if(m_chFileDataSet.m_nCanSlaveCount > 0)
	{
		strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveEndData_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strCanSStepEndFile = strLocalTempFolder + "\\stepCanSEndData.csv";
		if(pDownLoad->DownLoadFile(strCanSStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.
			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////
//	strStartIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_start.csv";
//	strEndIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_last.csv";
//////////////////////////////////////////////////////////////////////////

	//4. Module쪽 Index 파일과 PC쪽 결과 파일을 비교하면서 손실구간을 검색한다.
	//PC쪽에 Data가 하나도 저장안되었을 경우도 복구 가능하도록 
	//현재 작업중인 경우 결과를 파일에 저장하지 않고 Buffer에 쌓고 있도록 한다.
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	strTemp.Format("손실된 구간을 검색중입니다.");
	progressWnd.SetPos(40);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		m_strLastErrorString.Format("M%02dCH%02d 사용자에 의해 취소됨", m_nModuleID, m_nChannelIndex+1);
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE;
	}

	long lLostDataCount =0;
	BOOL nRtn = SearchLostDataRange(strStartIndexFile, strEndIndexFile, lLostDataCount, &progressWnd);
	if(nRtn == FALSE)
	{
		m_strLastErrorString.Format("M%02dCH%02d 손실구간 검색 실패!!!", m_nModuleID, m_nChannelIndex+1);
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}

	if(lLostDataCount <= 0)
	{
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		m_strLastErrorString.Format("M%02dCH%02d 손실된 data 정보가 없습니다.", m_nModuleID, m_nChannelIndex+1);
		return TRUE;	//실제 Data 분석 
	}

	//4. 손실된 구간이 있을 경우 해당 구간의 복구용 파일을 Module에서 Download한다.
//	long nSquenceNo = 0;
	BOOL bDownLoadOK = FALSE;

	srcFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;				//복구하고자 하는 파일
	destFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_RAW_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcAuxFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_AUX_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destAuxFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_AUX_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destAuxFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcCanFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_CAN_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destCanFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_CAN_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destCanFileName);		//Backup할 파일명이 있으면 삭제 

//////////////////////////////////////////////////////////////////////////
//	srcFileName = "C:\\test1\\M01Ch01\\Test1.cyc";
//	destFileName = "C:\\test1\\M01Ch01\\Test1_BackUp.cyc";
//////////////////////////////////////////////////////////////////////////

	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cyc 파일용
	FILE *fpAuxSourceFile = NULL, *fpAuxDestFile = NULL;	//aux 파일용
	FILE *fpCanSourceFile = NULL, *fpCanDestFile = NULL;	//can 파일용

	//cyc용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////

	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE; 
	}

	int nColSize = 0;
	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)
		{
			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			LockFileUpdate(bLockFile);
			fclose(fpDestFile);
			delete pDownLoad;
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		LockFileUpdate(bLockFile);
		fclose(fpDestFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			LockFileUpdate(bLockFile);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			delete pDownLoad;
			return FALSE;	
		}
		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	if(m_chFileDataSet.GetAuxColumnCount() > 0)			
	{
		fpAuxDestFile = fopen(destAuxFileName, "wb");
		if(fpAuxDestFile == NULL)
		{
			m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destAuxFileName);
			return FALSE; 
		}

		fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.GetAuxColumnCount()) == FALSE)
			{
				m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcAuxFileName);
				fclose(fpAuxDestFile);
				return FALSE;	 
			}
			fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		}
		
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcAuxFileName);
			fclose(fpAuxDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxSourceFile) > 0)
		{
			if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxDestFile) < 1)
			{
				m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
				fclose(fpAuxSourceFile);
				fclose(fpAuxDestFile);
				return FALSE;	
			}
		}
		nAuxColSize = auxFileHeader.auxHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//CAN용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	if(m_chFileDataSet.GetCanColumnCount() > 0)			
	{
		fpCanDestFile = fopen(destCanFileName, "wb");
		if(fpCanDestFile == NULL)
		{
			m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destCanFileName);
			return FALSE; 
		}

		fpCanSourceFile = fopen(srcCanFileName, "rb");
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			if(CreateCanFileA(srcCanFileName, 0, m_chFileDataSet.GetCanColumnCount()) == FALSE)
			{
				m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcCanFileName);
				fclose(fpCanDestFile);
				return FALSE;	 
			}
			fpCanSourceFile = fopen(srcCanFileName, "rb");
		}
		
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
			fclose(fpCanDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanDestFile) < 1)
			{
				m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcCanFileName);
				fclose(fpCanSourceFile);
				fclose(fpCanDestFile);
				return FALSE;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	//파일이 잘못되었을 경우 
	if(nColSize < 1 || nColSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", srcFileName, nColSize);
		LockFileUpdate(bLockFile);
		fclose(fpSourceFile);
		fclose(fpDestFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}

	strTemp.Format("M%02dCH%02d 손실된 구간을 복구중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(50);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE;
	}

	UINT nFileSeq = 0;
	ULONG nSequenceNo = 1;
	int nBackupFileNo = 0;
	//File Read Buffer
	float *pfSrcBuff = new float[nColSize];
	float *pfBuff = new float[nColSize];
	float *pfAuxSrcBuff = NULL;
	CAN_VALUE *pCanSrcBuff = NULL;
	if(nAuxColSize > 0)
		pfAuxSrcBuff = new float[nAuxColSize];
	if(nCanColSize > 0)
		pCanSrcBuff = new CAN_VALUE[nCanColSize];
	
	BOOL bCanceled = FALSE;
	BOOL bRun = TRUE;
	int nCount = 0;
	int nPos;
	int nSaveTotSize = (ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 

	int nAuxCount = 0, nCanCount = 0;

	while(bRun)
	{
		//Progress 50~90구간 사용 
		nPos = 50 + int(40.0f*(float)nCount/(float)nSaveTotSize);

//		TRACE("TotLost Count %d, nCount %d, Pos %d\n", nSaveTotSize, nCount, nPos);

		progressWnd.SetPos(nPos);
		
		//Canceled 
		if(progressWnd.PeekAndPump() == FALSE)
		{
			bCanceled = TRUE;
			break;
		}
		nCount++;
		
		//PC쪽 파일은 순서대로 읽는다.
		size_t rdSize = fread(pfSrcBuff, sizeof(float), nColSize, fpSourceFile );

		//Aux,Can 파일도 Raw 파일의 인덱스를 따라간다.
		if(nAuxColSize)
			fread(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxSourceFile);
		if(nCanColSize)
			fread(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanSourceFile);

		if(rdSize <	1)	//PC쪽 파일을 다 읽었을 경우 모듈의 최종 Index와 같은지 확인 한다. 모듈쪽 저장 인텍스가 더크면 뒤쪽 data 최종 복구한다.
		{	
			nFileSeq = nSaveTotSize;//(ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 
			bRun = FALSE;	//PC쪽은 다 읽었음 
		}
		else
		{
			nFileSeq = (ULONG)pfSrcBuff[0];
		}

				
		//Index 검사
		long nStartNo = 0, nEndNo = 0, lLossCount =0;
		
		//PC쪽 검사 결과 data loss range detected	
		if(nFileSeq > nSequenceNo || bRun == FALSE)		
		{
			nStartNo = nSequenceNo;
			while(nFileSeq > nSequenceNo)
			{
				nSequenceNo++;
			}
			nEndNo = nSequenceNo-1;

			int nRestordDataSize = 0;
			lLossCount = nEndNo - nStartNo+1;
			long lTemp = 0;
			while(lLossCount > 0)	//1개의 손실 구간이 모두 복구됨 
			{
				//5. 손실된 부분에 해당하는 복구용 파일을 Index 파일에서 찾아낸다.
				nStartNo += nRestordDataSize;
				lTemp = m_RestoreFileIndexTable.FindFirstMachFileNo(nStartNo);	
				if(lTemp < 1)	
				{
					//현재 손실 구간을 SBC에서 Dowload한 파일목록에서 발견하지 못했을 경우, 
					//기타 오류로 복구 불가시는 현재구간은 복구를 생략한다.
					nRestordDataSize = lLossCount;
					lLossCount = 0;
					break;
				}
*/
/*				
				//이전에 DownLoad한 파일에 손상된 구간이 있을 경우는 Dowload를 생략한다.
				if(nBackupFileNo != lTemp)
				{
					nBackupFileNo = lTemp;
					//6. 복구용 파일을 DownLoad 받는다.
					strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveData%02d.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
					strLocalFileName = strLocalTempFolder + "\\ChRawData.csv";
					bDownLoadOK = TRUE;
					if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) == FALSE)
					{
						//복구용 파일이 존재하지 않을 경우(복구최대시간경과로 삭제된 경우나 기타 오류로 복구 불가)
						//DownLoad 실패시 손실 시작부터 파일에 기록된 가장 마지막 크기 만큼 뛰어 넘고 복구 생략 
						nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);
						bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nAuxTempCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveData%02d_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strAuxTempLocalFileName = strLocalTempFolder + "\\ChAuxTData.csv";
						if(pDownLoad->DownLoadFile(strAuxTempLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nAuxVoltCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveData%02d_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strAuxVoltLocalFileName = strLocalTempFolder + "\\ChAuxVData.csv";
						if(pDownLoad->DownLoadFile(strAuxVoltLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nCanMasterCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveData%02d_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strCanMLocalFileName = strLocalTempFolder + "\\ChCanMData.csv";
						if(pDownLoad->DownLoadFile(strCanMLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nCanSlaveCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveData%02d_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strCanSLocalFileName = strLocalTempFolder + "\\ChCanSData.csv";
						if(pDownLoad->DownLoadFile(strCanSLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;
					}
				}

				//7. DownLoad 파일로 복구 한다.
				// 7.1 data가 부족하면 다음 파일을 DownLoad한다.
				if(bDownLoadOK)
				{					
					
					nRestordDataSize = WriteRestoreRawFile(strLocalFileName, fpDestFile, FileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nAuxColSize)
						WriteRestoreAuxFile(strAuxTempLocalFileName, strAuxVoltLocalFileName, fpAuxDestFile, auxFileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nCanColSize)
						WriteRestoreCanFile(strCanMLocalFileName, strCanSLocalFileName, fpCanDestFile, canFileHeader, nStartNo, nEndNo, nBackupFileNo);
					

				}
				strTemp.Format("Data 복구 실시, 시작Index: %d, 복수 수량:%d개",  nStartNo, nRestordDataSize);
				WriteLog(strTemp);
				lLossCount -= nRestordDataSize;	//if(lLossCount > 0) Download한 파일에 복구 data가 모두 없고 다음파일까지 이어짐 
			}
			
			if(bRun)
			{
				//PC쪽 읽을 data가 아직 남아 있으면 손실 구간 가장 마지막 다음 data까지 읽혔으므로 
				//한줄더 읽어들인 data를 저장한다.
				if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destFileName);
				}
				if(nAuxColSize > 0)
				{
					if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destAuxFileName);
					}
				}
				if(nCanColSize > 0)
				{
					if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destCanFileName);
					}
				}
			}	
			nSequenceNo = nFileSeq;	//손실된 구간 복구 완료 
		}
		else if(nFileSeq < nSequenceNo)	//Module index 오류
		{
			//PC와 모듈의 시험이 불일치 하거나 모듈에 저장된 파일의 index가 잘못 되었을 경우 
			TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);

			m_strLastErrorString.Format("M%02dCH%02d 모듈 정보와 Host 정보가 불일치 합니다.", m_nModuleID, m_nChannelIndex+1);

			if(pfBuff)
			{
				delete [] pfBuff;
				pfBuff = NULL;
			}

			if(pfSrcBuff)
			{
				delete[] pfSrcBuff;
				pfSrcBuff = NULL;
			}
			if(pfAuxSrcBuff)
			{
				delete [] pfAuxSrcBuff;
				pfAuxSrcBuff = NULL;
			}
			if(pCanSrcBuff)
			{
				delete [] pCanSrcBuff;
				pCanSrcBuff = NULL;
			}

			if(fpDestFile)		fflush(fpDestFile);
			if(fpAuxDestFile)	fflush(fpAuxDestFile);
			if(fpCanDestFile)	fflush(fpCanDestFile);

			if(fpSourceFile) fclose(fpSourceFile);
			if(fpDestFile) fclose(fpDestFile);
			if(fpAuxSourceFile) fclose(fpAuxSourceFile);
			if(fpAuxDestFile) fclose(fpAuxDestFile);
			if(fpCanSourceFile) fclose(fpCanSourceFile);
			if(fpCanDestFile) fclose(fpCanDestFile);

			delete pDownLoad;

			_unlink(destFileName);		//PC와 모듈이 맞지 않는 정보일 경우 복구 할 수 없음 
			
			LockFileUpdate(bLockFile);

			return FALSE;
		}
		else
		{
			//PC에서 정상적인 원본에서 임시 복구 파일로 Data를 복사한다.
			if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
			{
				TRACE("Backup File write Error %s\n", destFileName);
			}
			if(nAuxColSize > 0)
			{
				if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destAuxFileName);
				}
			}
			if(nCanColSize > 0)
			{
				if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destCanFileName);
				}
			}

		
		}	//정상
		nSequenceNo++;
	}

	if(pfBuff)
	{
		delete [] pfBuff;
		pfBuff = NULL;
	}

	if(pfSrcBuff)
	{
		delete[] pfSrcBuff;
		pfSrcBuff = NULL;
	}

	if(pfAuxSrcBuff)
	{
		delete [] pfAuxSrcBuff;
		pfAuxSrcBuff = NULL;
	}
	if(pCanSrcBuff)
	{
		delete [] pCanSrcBuff;
		pCanSrcBuff = NULL;
	}

	if(fpDestFile)		fflush(fpDestFile);
	if(fpAuxDestFile)	fflush(fpAuxDestFile);
	if(fpCanDestFile)	fflush(fpCanDestFile);

	if(fpSourceFile) fclose(fpSourceFile);
	if(fpDestFile) fclose(fpDestFile);

	if(fpAuxSourceFile) fclose(fpAuxSourceFile);
	if(fpAuxDestFile)	fclose(fpAuxDestFile);

	if(fpCanSourceFile) fclose(fpCanSourceFile);
	if(fpCanDestFile) fclose(fpCanDestFile);
	delete pDownLoad;

	if(bCanceled)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//복구된 파일을 이용하여 rpt 파일을 생성한다.
	CString strNewTableFile, strOrgTableFile;
	strNewTableFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_RESULT_FILE_NAME_EXT;

	//Aux,Can용 결과 데이터 파일
	CString strNewAuxTabelFile, strOrgAuxTableFile;
	strNewAuxTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_AUX_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgAuxTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_AUX_RESULT_FILE_NAME_EXT;

	CString strNewCanTabelFile, strOrgCanTableFile;
	strNewCanTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_CAN_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgCanTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_CAN_RESULT_FILE_NAME_EXT;

	_unlink(strNewTableFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewAuxTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewCanTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 

//////////////////////////////////////////////////////////////////////////
//	strNewTableFile = "C:\\test1\\M01Ch01\\Test1_BackUp.cts";
//	strOrgTableFile = "C:\\test1\\M01Ch01\\Test1.cts";
//////////////////////////////////////////////////////////////////////////

	strTemp.Format("M%02dCH%02d 손실된 Index 정보를 갱신중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(90);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//Backup이 완료된 Raw파일을 이용하여 rpt 파일을 새로 생성	
	if(RemakePCIndexFile(destFileName, strStepEndFile, strNewTableFile, strOrgTableFile) == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	if(nAuxColSize > 0)
	{
		if(RemakeAuxPCIndexFile(destAuxFileName, strAuxTStepEndFile, strAuxVStepEndFile, strNewAuxTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	if(nCanColSize > 0)
	{
		if(RemakeCanPCIndexFile(destCanFileName, strCanMStepEndFile, strCanSStepEndFile, strNewCanTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	strTemp.Format("M%02dCH%02d 복구용 임시 파일을 삭제중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(95);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
	//원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	

	sprintf(szFrom, "%s", strOrgTableFile);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgAuxTableFile);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgCanTableFile);
	::DeleteFile(szFrom);
	
	ZeroMemory(szFrom, _MAX_PATH);			//Double NULL Terminate
	sprintf(szFrom, "%s", srcFileName);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcAuxFileName);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcCanFileName);
	::DeleteFile(szFrom);


	progressWnd.SetPos(98);
	strTemp.Format("M%02dCH%02d 복구 완료중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetText(strTemp);
	//Backup한 파일이름을 바꾼다.
	if(rename(destFileName, srcFileName) != 0)
	{
		m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	if(nAuxColSize > 0)
	{
		if(rename(destAuxFileName, srcAuxFileName) != 0)
		{
			m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewAuxTabelFile, strOrgAuxTableFile) != 0)
		{
			m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgAuxTableFile);
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}	

	if(nCanColSize > 0)
	{
		if(rename(destCanFileName, srcCanFileName) != 0)
		{
			m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewCanTabelFile, strOrgCanTableFile) != 0)
		{
			m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgCanTableFile);
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}
	

	if(rename(strNewTableFile, strOrgTableFile) != 0)
	{
		m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgTableFile);
		LockFileUpdate(bLockFile);
		return FALSE;
	}


	LockFileUpdate(bLockFile);
	
	strTemp.Format("M%02dCH%02d 복구를 완료 하였습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(100);
	progressWnd.SetText(strTemp);
	progressWnd.PeekAndPump();
	
	Sleep(200);

	m_bDataLoss = FALSE;
	WriteLog("Data 복구 완료");
	*/
	return TRUE;
}
/*////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL CCyclerChannel::RestoreLostData(CString strIPAddress)
{
	CString srcFileName, destFileName;
	CString strTemp;

	if(strIPAddress.IsEmpty())
	{
		strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	m_nModuleID);
		AfxMessageBox(strTemp);
		return 0;
	}

//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), "복구중...", TRUE, TRUE, TRUE);
	progressWnd.SetRange(0, 100);
//////////////////////////////////////////////////////////////////////////

	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	//Get ip address

//////////////////////////////////////////////////////////////////////////
//	strIPAddress = "192.168.9.10";
//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
//////////////////////////////////////////////////////////////////////////
	
	strTemp.Format("Module %d에 접속 시도 중입니다.(IP %s)", m_nModuleID, strIPAddress);
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", m_nModuleID);
		delete pDownLoad;
		return FALSE;
	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;
	CString strLocalTempFolder(szBuff);
	CString strStartIndexFile, strEndIndexFile, strStepEndFile;

	//1. 시작 Index파일을 DownLoad한다.
	strTemp.Format("M%02dCH%02d Index 시작 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(10);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

	strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", m_nChannelIndex+1);
	strStartIndexFile = strLocalTempFolder + "\\savingFileIndex_start.csv";
	if(pDownLoad->DownLoadFile(strStartIndexFile, strRemoteFileName) < 1)
	{
		m_strLastErrorString.Format("M%02dCH%02d 시작 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		delete pDownLoad;
		return FALSE;
	}


	//2. 최종 Index 파일을 DownLoad한다.
	strTemp.Format("M%02dCH%02d Index 마지막 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(20);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

	strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_last.csv", m_nChannelIndex+1);
	strEndIndexFile = strLocalTempFolder + "\\savingFileIndex_last.csv";
	if(pDownLoad->DownLoadFile(strEndIndexFile, strRemoteFileName) < 1)
	{
		//가끔 download 실패가 발생한다.
		m_strLastErrorString.Format("M%02dCH%02d 최종 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		delete pDownLoad;
		TRACE("%s download fail\n", strRemoteFileName);
		return FALSE;
	}

	//3. Step End Data 파일을 DownLoad한다.
	strTemp.Format("M%02dCH%02d 각 Step의 완료 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(30);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

	strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveEndData.csv", m_nChannelIndex+1, m_nChannelIndex+1);
	strStepEndFile = strLocalTempFolder + "\\stepEndData.csv";
	if(pDownLoad->DownLoadFile(strStepEndFile, strRemoteFileName) < 1)
	{
		//가끔 download 실패가 발생한다.
		m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		delete pDownLoad;
		TRACE("%s download fail\n", strRemoteFileName);
		return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//	strStartIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_start.csv";
//	strEndIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_last.csv";
//////////////////////////////////////////////////////////////////////////

	//4. Module쪽 Index 파일과 PC쪽 결과 파일을 비교하면서 손실구간을 검색한다.
	//PC쪽에 Data가 하나도 저장안되었을 경우도 복구 가능하도록 
	//현재 작업중인 경우 결과를 파일에 저장하지 않고 Buffer에 쌓고 있도록 한다.
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	strTemp.Format("손실된 구간을 검색중입니다.");
	progressWnd.SetPos(40);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		m_strLastErrorString.Format("M%02dCH%02d 사용자에 의해 취소됨", m_nModuleID, m_nChannelIndex+1);
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE;
	}

	long lLostDataCount =0;
	BOOL nRtn = SearchLostDataRange(strStartIndexFile, strEndIndexFile, lLostDataCount, &progressWnd);
	if(nRtn == FALSE)
	{
		m_strLastErrorString.Format("M%02dCH%02d 손실구간 검색 실패!!!", m_nModuleID, m_nChannelIndex+1);
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}

	if(lLostDataCount <= 0)
	{
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		m_strLastErrorString.Format("M%02dCH%02d 손실된 data 정보가 없습니다.", m_nModuleID, m_nChannelIndex+1);
		return TRUE;	//실제 Data 분석 
	}

	//4. 손실된 구간이 있을 경우 해당 구간의 복구용 파일을 Module에서 Download한다.
//	long nSquenceNo = 0;
	BOOL bDownLoadOK = FALSE;

	srcFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;				//복구하고자 하는 파일
	destFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_RAW_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 

//////////////////////////////////////////////////////////////////////////
//	srcFileName = "C:\\test1\\M01Ch01\\Test1.cyc";
//	destFileName = "C:\\test1\\M01Ch01\\Test1_BackUp.cyc";
//////////////////////////////////////////////////////////////////////////

	FILE *fpSourceFile, *fpDestFile;
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE; 
	}

	int nColSize = 0;
	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)
		{
			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			LockFileUpdate(bLockFile);
			fclose(fpDestFile);
			delete pDownLoad;
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		LockFileUpdate(bLockFile);
		fclose(fpDestFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			LockFileUpdate(bLockFile);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			delete pDownLoad;
			return FALSE;	
		}
		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	
	//파일이 잘못되었을 경우 
	if(nColSize < 1 || nColSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", srcFileName, nColSize);
		LockFileUpdate(bLockFile);
		fclose(fpSourceFile);
		fclose(fpDestFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}

	strTemp.Format("M%02dCH%02d 손실된 구간을 복구중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(50);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE;
	}

	UINT nFileSeq = 0;
	ULONG nSequenceNo = 1;
	int nBackupFileNo = 0;
	//File Read Buffer
	float *pfSrcBuff = new float[nColSize];
	float *pfBuff = new float[nColSize];

	
	BOOL bCanceled = FALSE;
	BOOL bRun = TRUE;
	int nCount = 0;
	int nPos;
	int nSaveTotSize = (ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 

	int nAuxCount = 0, nCanCount = 0;

	while(bRun)
	{
		//Progress 50~90구간 사용 
		nPos = 50 + int(40.0f*(float)nCount/(float)nSaveTotSize);

//		TRACE("TotLost Count %d, nCount %d, Pos %d\n", nSaveTotSize, nCount, nPos);

		progressWnd.SetPos(nPos);
		
		//Canceled 
		if(progressWnd.PeekAndPump() == FALSE)
		{
			bCanceled = TRUE;
			break;
		}
		nCount++;
		
		//PC쪽 파일은 순서대로 읽는다.
		size_t rdSize = fread(pfSrcBuff, sizeof(float), nColSize, fpSourceFile );
		
		if(rdSize <	1)	//PC쪽 파일을 다 읽었을 경우 모듈의 최종 Index와 같은지 확인 한다. 모듈쪽 저장 인텍스가 더크면 뒤쪽 data 최종 복구한다.
		{	
			nFileSeq = nSaveTotSize;//(ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 
			bRun = FALSE;	//PC쪽은 다 읽었음 
		}
		else
		{
			nFileSeq = (ULONG)pfSrcBuff[0];
		}

				
		//Index 검사
		long nStartNo = 0, nEndNo = 0, lLossCount =0;
		
		//PC쪽 검사 결과 data loss range detected	
		if(nFileSeq > nSequenceNo || bRun == FALSE)		
		{
			nStartNo = nSequenceNo;
			while(nFileSeq > nSequenceNo)
			{
				nSequenceNo++;
			}
			nEndNo = nSequenceNo-1;

			int nRestordDataSize = 0;
			lLossCount = nEndNo - nStartNo+1;
			long lTemp = 0;
			while(lLossCount > 0)	//1개의 손실 구간이 모두 복구됨 
			{
				//5. 손실된 부분에 해당하는 복구용 파일을 Index 파일에서 찾아낸다.
				nStartNo += nRestordDataSize;
				lTemp = m_RestoreFileIndexTable.FindFirstMachFileNo(nStartNo);	
				if(lTemp < 1)	
				{
					//현재 손실 구간을 SBC에서 Dowload한 파일목록에서 발견하지 못했을 경우, 
					//기타 오류로 복구 불가시는 현재구간은 복구를 생략한다.
					nRestordDataSize = lLossCount;
					lLossCount = 0;
					break;
				}
				
				//이전에 DownLoad한 파일에 손상된 구간이 있을 경우는 Dowload를 생략한다.
				if(nBackupFileNo != lTemp)
				{
					nBackupFileNo = lTemp;
					//6. 복구용 파일을 DownLoad 받는다.
					strRemoteFileName.Format("cycler_data//resultData//ch%02d//ch%02d_SaveData%02d.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
					strLocalFileName = strLocalTempFolder + "\\ChRawData.csv";
					if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) == FALSE)
					{
						//복구용 파일이 존재하지 않을 경우(복구최대시간경과로 삭제된 경우나 기타 오류로 복구 불가)
						//DownLoad 실패시 손실 시작부터 파일에 기록된 가장 마지막 크기 만큼 뛰어 넘고 복구 생략 
						nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);
						bDownLoadOK = FALSE;
					}
					else
					{
						bDownLoadOK = TRUE;
					}
				}

				//7. DownLoad 파일로 복구 한다.
				// 7.1 data가 부족하면 다음 파일을 DownLoad한다.
				if(bDownLoadOK)
				{
					nRestordDataSize = 0;
					int lReadData[PS_MAX_ITEM_NUM] = { 0 };

					FILE *fpBackUp = fopen(strLocalFileName,"rt");
					if(fpBackUp)
					{
						int nData1 = 0, nData2 = 0;
						BOOL bFind = FALSE;
						char temp[23][20];
						for(int i = 0 ; i < 23; i++)
						{
							fscanf(fpBackUp, "%s", temp[i]);
						}
						//fscanf(fpBackUp, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
						//				&temp[0],&temp[1],&temp[2],&temp[3],&temp[4],&temp[5],&temp[6],&temp[7],&temp[8],&temp[9],
						//				&temp[10],&temp[11],&temp[12],&temp[13],&temp[14],&temp[15],&temp[16],&temp[17],&temp[18],&temp[19],
						//				&temp[20],&temp[21],&temp[22]);
						
						while(1)
						{

							//모듈에서 가져온 복구파일을 읽어 들인다.
							//////////////////////////////////////////////////////////////////////////
							//resultIndex, state, type, mode, select, 
							//code, grade, stepNo, Vsens, Isens, 
							//capacity, watt, wattHour, runTime,totalRunTime, 
							//z, temp, press, totalCycle, currentCycle, 
							//avgV, avgI
							if(fscanf(fpBackUp, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", 
												&lReadData[PS_DATA_SEQ], &lReadData[PS_TOT_TIME], &lReadData[PS_STEP_TIME],
												&lReadData[PS_STATE], &lReadData[PS_STEP_TYPE], &nData1, &nData2,&lReadData[PS_CODE], 
												&lReadData[PS_GRADE_CODE], &lReadData[PS_STEP_NO], &lReadData[PS_VOLTAGE], 
												&lReadData[PS_CURRENT],	&lReadData[PS_CAPACITY], &lReadData[PS_WATT], &lReadData[PS_WATT_HOUR],
												&lReadData[PS_IMPEDANCE], &lReadData[PS_TEMPERATURE], &nAuxCount, &nCanCount,  &lReadData[PS_TOT_CYCLE],
												&lReadData[PS_CUR_CYCLE], &lReadData[PS_AVG_VOLTAGE], &lReadData[PS_AVG_CURRENT] ) > 0 ) 
							{
								//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
								if(lReadData[PS_DATA_SEQ] >= nStartNo && lReadData[PS_DATA_SEQ] <= nEndNo)
								{
									bFind = TRUE;
									//복구 실시 
									for(int a =0; a<nColSize; a++)
									{
										pfBuff[a] = (float)ConvertSbcToPCUnit(lReadData[FileHeader.rsHeader.awColumnItem[a]], FileHeader.rsHeader.awColumnItem[a]);
									}
									
									if(fwrite(pfBuff, sizeof(float), nColSize, fpDestFile) < 1)
									{
										//TRACE("Backup File write Error %s\n", destFileName);
										//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
										//break;
									}
									nRestordDataSize++;
	//								TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
								}
							}
							else
							{
								//bRead = FALSE;
								break;
							}
														
						}

						//SBC Index 파일에는 있는데 실제 파일에 존재 하지 않으면(SBC 저장 오류) 복구수가 없을 수 있다.
						if(nRestordDataSize < 1)
						{
							nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
						}
						fclose(fpBackUp);
					}
					else 
					{	//SBC에서 download한 backup 파일 open 실패 
						nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
					}
				}
				strTemp.Format("Data 복구 실시, 시작Index: %d, 복수 수량:%d개",  nStartNo, nRestordDataSize);
				WriteLog(strTemp);
				lLossCount -= nRestordDataSize;	//if(lLossCount > 0) Download한 파일에 복구 data가 모두 없고 다음파일까지 이어짐 
			}
			
			if(bRun)
			{
				//PC쪽 읽을 data가 아직 남아 있으면 손실 구간 가장 마지막 다음 data까지 읽혔으므로 
				//한줄더 읽어들인 data를 저장한다.
				if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destFileName);
				}
			}	
			nSequenceNo = nFileSeq;	//손실된 구간 복구 완료 
		}
		else if(nFileSeq < nSequenceNo)	//Module index 오류
		{
			//PC와 모듈의 시험이 불일치 하거나 모듈에 저장된 파일의 index가 잘못 되었을 경우 
			TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);

			m_strLastErrorString.Format("M%02dCH%02d 모듈 정보와 Host 정보가 불일치 합니다.", m_nModuleID, m_nChannelIndex+1);

			if(pfBuff)
			{
				delete [] pfBuff;
				pfBuff = NULL;
			}

			if(pfSrcBuff)
			{
				delete[] pfSrcBuff;
				pfSrcBuff = NULL;
			}

			fflush(fpDestFile);
			if(fpSourceFile) fclose(fpSourceFile);
			if(fpDestFile) fclose(fpDestFile);
			delete pDownLoad;

			_unlink(destFileName);		//PC와 모듈이 맞지 않는 정보일 경우 복구 할 수 없음 
			
			LockFileUpdate(bLockFile);

			return FALSE;
		}
		else
		{
			//PC에서 정상적인 원본에서 임시 복구 파일로 Data를 복사한다.
			if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
			{
				TRACE("Backup File write Error %s\n", destFileName);
			}
		
		}	//정상
		nSequenceNo++;
	}

	if(pfBuff)
	{
		delete [] pfBuff;
		pfBuff = NULL;
	}

	if(pfSrcBuff)
	{
		delete[] pfSrcBuff;
		pfSrcBuff = NULL;
	}

	fflush(fpDestFile);
	if(fpSourceFile) fclose(fpSourceFile);
	if(fpDestFile) fclose(fpDestFile);
	delete pDownLoad;

	if(bCanceled)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//복구된 파일을 이용하여 rpt 파일을 생성한다.
	CString strNewTableFile, strOrgTableFile;
	strNewTableFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_RESULT_FILE_NAME_EXT;
	_unlink(strNewTableFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 

//////////////////////////////////////////////////////////////////////////
//	strNewTableFile = "C:\\test1\\M01Ch01\\Test1_BackUp.cts";
//	strOrgTableFile = "C:\\test1\\M01Ch01\\Test1.cts";
//////////////////////////////////////////////////////////////////////////

	strTemp.Format("M%02dCH%02d 손실된 Index 정보를 갱신중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(90);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//Backup이 완료된 Raw파일을 이용하여 rpt 파일을 새로 생성	
	if(RemakePCIndexFile(destFileName, strStepEndFile, strNewTableFile, strOrgTableFile) == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	strTemp.Format("M%02dCH%02d 복구용 임시 파일을 삭제중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(95);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	if(nAuxCount > 0)
		RestoreLostAuxData(pDownLoad, progressWnd, nFileSeq);
	
	//원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate
	
	//pFrom에 Full Path 제공하고 pTo에 Path명을 제공하지 않고 
	//fFlags Option에 FOF_ALLOWUNDO 부여하고 
	//Delete 동작 하면 휴지통으로 삭제 된다.
//	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
//	ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
//	lpFileOp->hwnd = NULL; 
//	lpFileOp->wFunc = FO_DELETE ; 
//	lpFileOp->pFrom = szFrom; 
//	lpFileOp->pTo = szTo; 
//	lpFileOp->fFlags = FOF_FILESONLY|FOF_NOCONFIRMATION|FOF_ALLOWUNDO|FOF_SILENT ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
//	lpFileOp->fAnyOperationsAborted = FALSE; 
//	lpFileOp->hNameMappings = NULL; 
//	lpFileOp->lpszProgressTitle = NULL; 

	sprintf(szFrom, "%s", strOrgTableFile);


//	if(SHFileOperation(lpFileOp) != 0)		//Move File
//	{
//		//파일이 존재하지 않아도 복구 될 수 있고, 또 backup 파일은 굳이 삭제안되도되므로 에러처리 하지 않음
//		TRACE("%s 삭제 실패\n", strOrgTableFile);
//	}

	//2006/4/11
	//SHFileOperation() 동작이 시간이 오래걸려서 그냥 완전 삭제해 버림

	::DeleteFile(szFrom);
	
	ZeroMemory(szFrom, _MAX_PATH);			//Double NULL Terminate
	sprintf(szFrom, "%s", srcFileName);
	::DeleteFile(szFrom);


	progressWnd.SetPos(98);
	strTemp.Format("M%02dCH%02d 복구 완료중입니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetText(strTemp);
	//Backup한 파일이름을 바꾼다.
	if(rename(destFileName, srcFileName) != 0)
	{
		m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	if(rename(strNewTableFile, strOrgTableFile) != 0)
	{
		m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgTableFile);
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	LockFileUpdate(bLockFile);
	
	strTemp.Format("M%02dCH%02d 복구를 완료 하였습니다.", m_nModuleID, m_nChannelIndex+1);
	progressWnd.SetPos(100);
	progressWnd.SetText(strTemp);
	progressWnd.PeekAndPump();
	
	Sleep(200);

	m_bDataLoss = FALSE;
	WriteLog("Data 복구 완료");
	
	return TRUE;
}
*/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//현재 진행중인 결과 파일을 최종 Data로 갱신한다.
//파일 용량이 많은 경우 시간지연이 발생할 수 있음 
BOOL CCyclerChannel::ReloadReaultData(BOOL bIncludeWorkingStep)
{
	BOOL bLockFile = LockFileUpdate(TRUE);

	BOOL nRtn = m_chFileDataSet.Create(bIncludeWorkingStep);
	
	LockFileUpdate(bLockFile);

	if(nRtn == FALSE)
	{
		m_strLastErrorString = "최종 결과 파일 Loadig 실패";
	}

	return nRtn;
}

//PC에 저장된 결과 파일을 검색하여 Data 손실 구간을 검색한다.
//MessageBox를 띄우면 안된다.
// bIncludeInitCh =>PC쪽의 정보를 찾을 수 없을 경우 복구 여부 
BOOL CCyclerChannel::SearchLostDataRange(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd, BOOL bIncludeInitCh )
{
	CString strTemp;
	lLostDataCount = 0;
	
	m_strDataLossRangeList.RemoveAll();	

#ifdef _DEBUG
	DWORD dwTime = GetTickCount();
#endif

	if(strStartIndexFile.IsEmpty() || strEndIndexFile.IsEmpty())
	{
		m_strLastErrorString = "시작 Index와 종료 Index 파일명 없음";
		return FALSE;
	}

	//PC쪽에 시험 정보가 없는 채널은 최종 시험 정보를 Loading한다.
//	if(bIncludeInitCh)
//	{
		if(m_strFilePath.IsEmpty() || m_strTestName.IsEmpty())
		{
	/*		strTemp.Format("Module %d의 채널 Ch %d에서 시행된 이전 작업 정보가 없습니다. 최종 작업 정보를 Loading 하시겠습니까?", 
				m_nModuleID, m_nChannelIndex+1);
			if(AfxMessageBox(strTemp, MB_YESNO|MB_ICONQUESTION) == IDNO)
			{
				m_bDataLoss = FALSE;
				return 0;
			}
	*/
			//최종 시험 정보를 Loading한다.
			if(LoadInfoFromTempFile()== FALSE)	
			{
				m_strLastErrorString.Format("Host의 최종 시험 정보를 loading할 수 없습니다.");
				m_bDataLoss = FALSE;
				return FALSE;
			}
		}
//	}

#ifdef _DEBUG
	TRACE("===========================>CH%03d 1: [%dmsec]\n", m_nChannelIndex, GetTickCount()-dwTime);
#endif


	//PC쪽 결과 파일을 Loading한다.
	//동일명으로 시험을 하였을시는 이미 파일이 삭제된 상태 
	if(ReloadReaultData() == FALSE)
	{
		//PC쪽에 아무 파일도 생성되어 있지 않으면 복구 할 수 없다.(새로은 시험은 하면서 덮어 쓰기 하였거나 해당 폴더가 삭제된 경우)
		if(bIncludeInitCh == FALSE)	
		{
			m_strLastErrorString.Format("Host의 최종 시험 파일을 찾을 수 없습니다.");
			return FALSE;	
		}
		//else	복구할 위치를 지정하고 PC쪽 Log파일의 저장 파일명을 update한다.  
	}

#ifdef _DEBUG
	TRACE("===========================>CH%03d 2: [%dmsec]\n", m_nChannelIndex, GetTickCount()-dwTime);
#endif

	//모듈에서 downloading한 index파일을 parsing한다.
	m_RestoreFileIndexTable.SetFileName(strStartIndexFile, strEndIndexFile);
	if(m_RestoreFileIndexTable.LoadTable() == FALSE)
	{
		m_strLastErrorString.Format("모듈의 index 정보를 loading할수 없습니다.");	
		return FALSE;
	}
	
	long lDataCount = 0;
	fltPoint *pData = NULL;
	ULONG nSequenceNo = 1;
	long lTotTime = 0, lTimeSecond = 0;
	long lPrevChTime = 0;
//	long saveTime =1;
	long nTotCount = m_chFileDataSet.GetTotalRecordCount();
	long nDataCount = 0;
	
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	if(pProgressWnd)		pProgressWnd->SetPos(0, TRUE);
	//PC Data를 순차적으로 확인하여 빠진 부분 List를 작성한다.
	UINT nFileSeq = 1;
	int nTableSize = m_chFileDataSet.GetTableSize();
	for(int lTableIndex =0; lTableIndex < nTableSize ; lTableIndex++)
	{
		pData =  m_chFileDataSet.GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_SEQ);
		if(pData == NULL)	continue;

		//Index 검사
		int nStartNo = 0, nEndNo = 0;
		for(int i=0; i<lDataCount; i++)
		{
			nFileSeq = (ULONG)pData[i].y;		//sequence no
//			if(i>0)			saveTime = (ULONG)(pData[i].x-pData[i-1].x);

			if(nFileSeq > nSequenceNo)			//data loss range detected
			{
				lTimeSecond = lTotTime + lPrevChTime;	
				nStartNo = nSequenceNo;
				while(nFileSeq > nSequenceNo)
				{
					nSequenceNo++;
				}

				nEndNo = nSequenceNo;
				//구간시작~구간끝, 해당 Step, 해당 시간 
//				strTemp.Format("%d~%d, %d~%d, %d", nStartNo, nEndNo-1, lTimeSecond, lTimeSecond + ((nEndNo-nStartNo)+1)*saveTime ,m_chFileDataSet.GetTableNo(lTableIndex));
				strTemp.Format("%d~%d, %d", nStartNo, nEndNo-1, m_chFileDataSet.GetTableNo(lTableIndex));
				m_strDataLossRangeList.AddTail(strTemp);
				lLostDataCount += (nEndNo-nStartNo)+1;
				
				TRACE("Data loss index %d~%d(%d)\n", nStartNo, nEndNo-1, nEndNo- nStartNo+1);	

			}
			else if(nFileSeq < nSequenceNo)	//Module index 오류
			{
				m_strLastErrorString.Format("Data index 이상 구간이 존재 합니다.(File:%d/Seq. No:%d)", nFileSeq, nSequenceNo);	

				//AfxMessageBox("Data 파일 Index오류가 있습니다.(Module Index 오류)");
				TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);
				//Index가 중복되어 저장되어 있음
			}
			lPrevChTime = (ULONG)pData[i].x;
			//else	{}	//정상
			nSequenceNo++;

			if(pProgressWnd)
			{
				if(nTotCount > 0)
				{
					pProgressWnd->SetPos( (float)nDataCount++/(float)nTotCount*100.0f, TRUE);
				}

				if(pProgressWnd->PeekAndPump(TRUE) == FALSE)
				{
					if(pData)
					{
						delete [] pData;
						pData = NULL;
					}
					LockFileUpdate(bLockFile);
					return FALSE;
				}
			}
		}

		lTotTime += UINT(m_chFileDataSet.GetLastDataOfTable(lTableIndex, "Time"));

		if(pData)
		{
			delete [] pData;
			pData = NULL;
		}
	}
	
	if(pProgressWnd)		pProgressWnd->SetPos(100, TRUE);


#ifdef _DEBUG
	TRACE("===========================>CH%03d 4: [%dmsec]\n", m_nChannelIndex, GetTickCount()-dwTime);
#endif

	nSequenceNo = m_RestoreFileIndexTable.GetLastCount();
	if(nFileSeq < nSequenceNo)		//모듈에 기록된 최종 Index가 PC Index보다 클 경우 마지막 Data가 손실되었다.
	{
		int nStepNo = 2;	//PC쪽에 저장된 data가 없을 경우 Step 번호를 2번으로 한다.(Step 1은 Adevance cycle step)
		nFileSeq++;
		if(nTableSize > 0)	
		{
			nStepNo =  m_chFileDataSet.GetTableNo(nTableSize-1);
		}
		strTemp.Format("%d~%d, %d", nFileSeq, nSequenceNo, nStepNo);
		TRACE("Data loss index %d~%d(%d)\n", nFileSeq, nSequenceNo, nSequenceNo- nFileSeq+1);	
		lLostDataCount += (nSequenceNo- nFileSeq)+1;
		m_strDataLossRangeList.AddTail(strTemp);
	}
	else if(nFileSeq > nSequenceNo)
	{
		TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch하거나 모듈 최종 파일이 없슴 .(%d/%d)\n", nFileSeq, nSequenceNo);
	}
	//else if(nFileSeq == nModuleLastIndex)	//정상
	LockFileUpdate(bLockFile);
	
	if(lLostDataCount > 0)		m_bDataLoss = TRUE;
	else						m_bDataLoss = FALSE;	
				
	return TRUE;//lLostDataCount;
}

CString CCyclerChannel::GetResultFileName()
{
	return m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;
}

//strRawFileName : 복구된 raw data file
//strNewFileName : 새롭게 생성할 Table Index File
//strStepEndFile : 모듈에서 Download한 복구용 Step End file
//strOrgTableFile : 원본 Table Index file
BOOL CCyclerChannel::RemakePCIndexFile(CString strBackUpRawFile, CString strStepEndFile, CString strNewTableFile, CString strOrgTableFile)
{
	//파일명 설정 여부 확인 
	if(strBackUpRawFile.IsEmpty() || strNewTableFile.IsEmpty() || strStepEndFile.IsEmpty())
	{
		return FALSE;
	}

	CString		strTemp;
	COleDateTime t = COleDateTime::GetCurrentTime();

	CStdioFile resultFile;
	CFile		fRS;
	CFileStatus fs;
	CFileException e;

	//////////////////////////////////////////////////////////////////////////
	//복구된 Raw Data와 모듈에서 download한 복구용 Table file을 이용하여 PC용 Table 파일을 생성한다.
	//파일을 Open 한다.
	if( !fRS.Open( strBackUpRawFile, CFile::modeRead, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   m_strLastErrorString.Format("%s 파일을 열수 없습니다.", strBackUpRawFile);
	   return FALSE;
	}

	int nColumnSize = 0;
	PS_RAW_FILE_HEADER *pFileHeader = new PS_RAW_FILE_HEADER;
	if(fRS.Read(pFileHeader, sizeof(PS_RAW_FILE_HEADER)) > 0)
	{
		nColumnSize = pFileHeader->rsHeader.nColumnCount;
	}
	delete pFileHeader;
	
	//파일이 잘못되었을 경우 
	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
	   m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", strBackUpRawFile, nColumnSize);
		fRS.Close();
		return FALSE;	//실제 Data 분석 
	}

	//모듈에서 전송받은 Step End 파일 Open
	FILE *fp = fopen(strStepEndFile, "rt");
	if(fp == NULL)
	{
		m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
		fRS.Close();
		return FALSE;	//실제 Data 분석 
	}

	char temp[23][20];
	for(int i = 0 ; i < 23; i++)
	{
		fscanf(fp, "%s", temp[i]);
	}
	
	float *pfBuff = new float[nColumnSize];
	size_t rsSize = sizeof(float)*nColumnSize;
	long nStartIndex = 0, nEndIndex = 0;
	int nTemp;
	PS_STEP_END_RECORD chTempData, *pChTempData;
	PS_STEP_END_RECORD chLastData;
	ZeroMemory(&chLastData, sizeof(PS_STEP_END_RECORD));

	CPtrArray apTableDataList;
	
	int lReadData[PS_MAX_ITEM_NUM] = { 0 };
	int nSelect;
	int nData1;
	//////////////////////////////////////////////////////////////////////////
	//resultIndex, state, type, mode, select, code, grade, stepNo, Vsens, Isens, 
	//capacity, watt, wattHour, runTime,totalRunTime, z, temp, press, 
	//totalCycle, currentCycle, avgV, avgI
	while(fscanf(fp, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d, %d", 
					&lReadData[PS_DATA_SEQ], &lReadData[PS_TOT_TIME],&lReadData[PS_STEP_TIME],&lReadData[PS_STATE], 
					&lReadData[PS_STEP_TYPE], &nTemp, &nSelect,	&lReadData[PS_CODE], &lReadData[PS_GRADE_CODE], 
					&lReadData[PS_STEP_NO], &lReadData[PS_VOLTAGE], &lReadData[PS_CURRENT],	&lReadData[PS_CAPACITY], 
					&lReadData[PS_WATT], &lReadData[PS_WATT_HOUR], &lReadData[PS_IMPEDANCE], &lReadData[PS_OVEN_TEMPERATURE], 
					&lReadData[PS_OVEN_HUMIDITY], &nData1, &lReadData[PS_TOT_CYCLE], &lReadData[PS_CUR_CYCLE],
					&lReadData[PS_AVG_VOLTAGE], &lReadData[PS_AVG_CURRENT] ) > 0 ) 
	{
		while(fRS.Read(pfBuff, rsSize) > 0)		//data sequence에 해당하는 data가 저장된 순서 index를 구한다.
		{	
			if((ULONG)pfBuff[0] < lReadData[PS_DATA_SEQ])
			{
				nEndIndex++;
			}
			else //if((ULONG)pfBuff[0] >= nSeqNo)
			{	
				if((ULONG)pfBuff[0] > lReadData[PS_DATA_SEQ])	//정확히 일치하는 Data가 없을 경우는 손실된 상태 이전까지가 step data
				{
					nEndIndex--;
				}

				pChTempData = new PS_STEP_END_RECORD;
				ZeroMemory(pChTempData, sizeof(PS_STEP_END_RECORD));
				pChTempData->chNo 			=	m_nChannelIndex+1;				//Channel No
				pChTempData->chStepNo		=	lReadData[PS_STEP_NO];			//StepNo
				pChTempData->chState		=	lReadData[PS_STATE];			//State
				pChTempData->chStepType		=	lReadData[PS_STEP_TYPE];		//Type
				pChTempData->chDataSelect	=	nSelect;
				pChTempData->chCode			= 	lReadData[PS_CODE];				//Code
				pChTempData->chGradeCode	=	lReadData[PS_GRADE_CODE];		//Grade

				pChTempData->nIndexFrom		=	nStartIndex;					//IndexFrom	
				pChTempData->nIndexTo		=	nEndIndex;						//IndexTo
				pChTempData->nCurrentCycleNum = lReadData[PS_CUR_CYCLE];		//CurCycle
				pChTempData->nTotalCycleNum	=	lReadData[PS_TOT_CYCLE];		//TotalCycle
				pChTempData->lSaveSequence	=	lReadData[PS_DATA_SEQ];			//


				pChTempData->fVoltage		= 	ConvertSbcToPCUnit(lReadData[PS_VOLTAGE], PS_VOLTAGE);			//Voltage
				pChTempData->fCurrent		= 	ConvertSbcToPCUnit(lReadData[PS_CURRENT], PS_CURRENT);			//Current
				pChTempData->fCapacity		= 	ConvertSbcToPCUnit(lReadData[PS_CAPACITY], PS_CAPACITY);		//Capacity
				pChTempData->fWatt			=	ConvertSbcToPCUnit(lReadData[PS_WATT], PS_WATT);		
				pChTempData->fWattHour		= 	ConvertSbcToPCUnit(lReadData[PS_WATT_HOUR], PS_WATT_HOUR);		//WattHour
				pChTempData->fStepTime		= 	ConvertSbcToPCUnit(lReadData[PS_STEP_TIME], PS_STEP_TIME);		//Time
				pChTempData->fTotalTime		= 	ConvertSbcToPCUnit(lReadData[PS_TOT_TIME], PS_TOT_TIME);		//TotTime
				pChTempData->fImpedance		=	ConvertSbcToPCUnit(lReadData[PS_IMPEDANCE], PS_IMPEDANCE);		//IR
				pChTempData->fOvenTemperature	= 	ConvertSbcToPCUnit(lReadData[PS_OVEN_TEMPERATURE], PS_OVEN_TEMPERATURE);	//Temp
				pChTempData->fOvenHumidity		= 	ConvertSbcToPCUnit(lReadData[PS_OVEN_HUMIDITY], PS_OVEN_HUMIDITY);		//Press
				pChTempData->fAvgVoltage	= 	ConvertSbcToPCUnit(lReadData[PS_AVG_VOLTAGE], PS_AVG_VOLTAGE);	//AvgVoltage
				pChTempData->fAvgCurrent	= 	ConvertSbcToPCUnit(lReadData[PS_AVG_CURRENT], PS_AVG_CURRENT);	//AvgVoltage

				apTableDataList.Add(pChTempData);

				nEndIndex++;
				nStartIndex = nEndIndex;
				break;
			}
		}
	}
	if (pfBuff != NULL)							delete[]	pfBuff;		
	if (fRS.m_hFile != CFile::hFileNull)		fRS.Close();
	if (fp)										fclose(fp);
	//////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////
	// PC용 Table File을 새로 생성한다.
	if( !fRS.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	{
		//생성 실패
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
			m_strLastErrorString.Format("%s를 생성할 수 없습니다.", strNewTableFile);
		
			//생성 실패시 Delete all step data
			int nSize = apTableDataList.GetSize();
			for(int a=0; a<nSize; a++)
			{
				pChTempData = (PS_STEP_END_RECORD *)apTableDataList.GetAt(0);
				if(pChTempData)
				{
					delete pChTempData;
					pChTempData = NULL;
				}
				apTableDataList.RemoveAt(0);
			}
			return FALSE;
	}

	//Make file header
	//strOrgTableFile 에서 file header를 가져올지 
	//아니면 새롭게 생성할지 
	PS_TEST_RESULT_FILE_HEADER *pHeader = new PS_TEST_RESULT_FILE_HEADER;
	ZeroMemory(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
	pHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
	pHeader->fileHeader.nFileVersion = PS_FILE_VERSION;
	sprintf(pHeader->fileHeader.szCreateDateTime, "%s", t.Format());
	sprintf(pHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
	sprintf(pHeader->testHeader.szStartTime, "%s", m_startTime.Format());
	sprintf(pHeader->testHeader.szEndTime, "%s", t.Format());
	sprintf(pHeader->testHeader.szSerial, "%s", m_strTestSerial);
	sprintf(pHeader->testHeader.szUserID, "%s", m_strUserID);
	sprintf(pHeader->testHeader.szDescript, "%s", m_strDescript);	//기존에 최종 정보가 이미 로딩되어 있다.
	pHeader->testHeader.nRecordSize = 12;
	pHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
	pHeader->testHeader.wRecordItem[1] = PS_CURRENT;
	pHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
	pHeader->testHeader.wRecordItem[3] = PS_WATT;
	pHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
	pHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
	pHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
	pHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
	pHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
	pHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
	pHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
	pHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;

	//Write file header
	fRS.Write(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
	delete pHeader;

	//Save new data
	int nSize = apTableDataList.GetSize();
	for(int a=0; a<nSize; a++)
	{
		pChTempData = (PS_STEP_END_RECORD *)apTableDataList.GetAt(0);
		if(pChTempData)
		{
			fRS.Write(pChTempData, sizeof(PS_STEP_END_RECORD));
			memcpy(&chLastData, pChTempData, sizeof(PS_STEP_END_RECORD));
			delete pChTempData;
			pChTempData = NULL;
		}
		apTableDataList.RemoveAt(0);
	}
	fRS.Flush();
	fRS.Close();	
	//////////////////////////////////////////////////////////////////////////
	
	//rp$ 파일 수정 
	//가장 마지막 step 완료의 EndIndex와 TempData의 Start Index를 비교
	//같은 cycle, 같은 step번호가 아니며(1개 step 전체 가 없을 경우 발생함 ) temp의 start index 수정 
	CString 	strCurTempFile;
	strCurTempFile.Format("%s\\%s.rp$",  m_strFilePath, m_strTestName);
	if(chLastData.chStepType == PS_STEP_END)
	{
		unlink(strCurTempFile);
	}
	else
	{
		if(chLastData.chStepNo > 0 )
		{

			CStringList aTempDataList;	
			if( CFile::GetStatus(strCurTempFile, fs))		 
			{
				if( !resultFile.Open( strCurTempFile, CFile::modeRead|CFile::shareDenyWrite, &e ) )
				{
					#ifdef _DEBUG
					   afxDump << "File could not be opened " << e.m_cause << "\n";
					#endif
					   return FALSE;
				}

				//TRACE("Read all data from %s\n", strFileName);
				while(resultFile.ReadString(strTemp))
				{
					if(!strTemp.IsEmpty())	aTempDataList.AddTail(strTemp);
				}
				resultFile.Close();
			}

			//////////////////////////////////////////////////////////////////////////
		
			//2. 최종 시험 data를 Parsing 한다.
			if(aTempDataList.GetCount() >= 3)	
			{
				ZeroMemory(&chTempData, sizeof(PS_STEP_END_RECORD));
	//			ZeroMemory(&chLastData, sizeof(CT_STEP_END_RECORD));
				//2.1 data parsing
				POSITION pos = aTempDataList.GetTailPosition();
				strTemp = aTempDataList.GetNext(pos);
				
				//CTable dataTable(aTempDataList.GetNext(pos) , strTemp);
				ParsingChDataString(strTemp, chTempData);
	//			ParsingChDataString(strTableDataList.GetTail(), chLastData);

//				if( chLastData.chStepNo != chTempData.chStepNo || chLastData.nTotalCycleNum != chTempData.nTotalCycleNum)
//				{
					if(chLastData.nIndexTo != (chTempData.nIndexFrom+1))
					{
						chTempData.nIndexFrom = chLastData.nIndexTo+1;
						
						strTemp.Format(	"%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,", 
										chTempData.chStepNo,		//StepNo
										chTempData.nIndexFrom,		//IndexFrom	
										chTempData.nIndexTo,		//IndexTo
										chTempData.nCurrentCycleNum,//CurCycle
										chTempData.nTotalCycleNum,	//TotalCycle => Cycle별 파일 생서시는 FileNo로 사용됨
										chTempData.chStepType,		//Type
										chTempData.chState,			//State
										chTempData.fStepTime, 		//Time
										chTempData.fTotalTime, 		//TotTime
										chTempData.chCode,			//Code
										chTempData.chGradeCode,		//Grade
										chTempData.fVoltage, 		//Voltage
										chTempData.fCurrent,		//Current
										chTempData.fCapacity, 		//Capacity
										chTempData.fWattHour, 		//WattHour
										chTempData.fImpedance, 		//IR
										chTempData.fOvenTemperature, 	//Temp
										chTempData.fOvenHumidity, 		//Humidity
										chTempData.fAvgVoltage,		//AvgVoltage
										chTempData.fAvgCurrent		//AvgVoltage						
							);
						FILE *fp = fopen(strCurTempFile, "wt");
						if(fp)
						{	
							pos = aTempDataList.GetHeadPosition();
							CString str1, str2;
							str1 = aTempDataList.GetNext(pos);
							str2 = aTempDataList.GetNext(pos);

							fprintf(fp, "%s\n%s\n%s", str1, str2, strTemp);

							fclose(fp);
						}
					}
//				}
			}	
		}
	}

	return TRUE;
}

BOOL	CCyclerChannel::LockFileUpdate(BOOL bLock)
{
	BOOL bRtn = m_bLockFileUpdate; 
	m_bLockFileUpdate = bLock;	
	return bRtn;
}

//모든 결과는 m단위로 저장한다.
float CCyclerChannel::ConvertSbcToPCUnit(long lData, long nItem)
{
	float fData = 0.0f;
	switch(nItem)
	{
	case		PS_STATE		:	fData = float(lData);			break;
	case		PS_VOLTAGE		:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위	
	case		PS_CURRENT		:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
#ifdef _EDLC_TEST_SYSTEM
	case		PS_CAPACITY		:	fData = float(lData);			break;		//m단위		//u단위(전류종속 단위)
#else
	case		PS_CAPACITY		:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
#endif
	case		PS_IMPEDANCE	:	fData = LONG2FLOAT(lData);		break;		//u단위		
	case		PS_CODE			:	fData = float(lData);			break;
	case		PS_STEP_TIME	:	fData = PCTIME(lData);			break;		//10msec
	case		PS_TOT_TIME		:	fData = PCTIME(lData);			break;		//10msec
	case		PS_GRADE_CODE	:	fData = float(lData);			break;
	case		PS_STEP_NO		:	fData = float(lData);			break;
	case		PS_WATT			:	fData = float(lData);			break;		//mWatt		//u단위(전류종속 단위)
	case		PS_WATT_HOUR	:	fData = float(lData);			break;		//mWh		//u단위(전류종속 단위)
	case		PS_OVEN_TEMPERATURE	:	fData = float(lData);			break;		//m℃
	case		PS_OVEN_HUMIDITY	:	fData = float(lData);			break;		//m℃
	case		PS_AUX_VOLTAGE	:	fData = LONG2FLOAT(lData);		break;		//m~
	case		PS_STEP_TYPE	:	fData = float(lData);			break;
	case		PS_CUR_CYCLE	:	fData = float(lData);			break;
	case		PS_TOT_CYCLE	:	fData = float(lData);			break;
	case		PS_TEST_NAME	:	fData = float(lData);			break;
	case		PS_SCHEDULE_NAME:	fData = float(lData);			break;
	case		PS_CHANNEL_NO	:	fData = float(lData);			break;
	case		PS_MODULE_NO	:	fData = float(lData);			break;
	case		PS_LOT_NO		:	fData = float(lData);			break;
	case		PS_DATA_SEQ		:	fData = float(lData);			break;
	case		PS_AVG_CURRENT	:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
	case		PS_AVG_VOLTAGE	:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
	case		PS_SYNC_DATE	:	fData = float(lData);			break;
	default:	fData = float(lData);								break;
	}
	return fData;
}

//모든 결과는 m단위로 저장한다.
long CCyclerChannel::ConvertPCUnitToSbc(float fData, long nItem)
{
	long lData = 0;
	switch(nItem)
	{
	case		PS_STATE		:	lData = long(fData);			break;
	case		PS_VOLTAGE		:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위	
	case		PS_CURRENT		:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
#ifdef _EDLC_TEST_SYSTEM
	case		PS_CAPACITY		:	lData = float(fData);			break;		//m단위		//u단위(전류종속 단위)
#else
	case		PS_CAPACITY		:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
#endif
	case		PS_IMPEDANCE	:	lData = FLOAT2LONG(fData);		break;		//u단위		
	case		PS_CODE			:	lData = long(fData);			break;
	case		PS_STEP_TIME	:	lData = MDTIME(fData);			break;		//10msec
	case		PS_TOT_TIME		:	lData = MDTIME(fData);			break;		//10msec
	case		PS_GRADE_CODE	:	lData = long(fData);			break;
	case		PS_STEP_NO		:	lData = long(fData);			break;
	case		PS_WATT			:	lData = long(fData);			break;		//mWatt		//u단위(전류종속 단위)
	case		PS_WATT_HOUR	:	lData = long(fData);			break;		//mWh		//u단위(전류종속 단위)
	case		PS_OVEN_TEMPERATURE	:	lData = FLOAT2LONG(fData);		break;		//m℃
	case		PS_OVEN_HUMIDITY	:	lData = FLOAT2LONG(fData);		break;		//m℃
	case		PS_AUX_VOLTAGE	:	lData = FLOAT2LONG(fData);		break;		//m~
	case		PS_STEP_TYPE	:	lData = long(fData);			break;
	case		PS_CUR_CYCLE	:	lData = long(fData);			break;
	case		PS_TOT_CYCLE	:	lData = long(fData);			break;
	case		PS_TEST_NAME	:	lData = long(fData);			break;
	case		PS_SCHEDULE_NAME:	lData = long(fData);			break;
	case		PS_CHANNEL_NO	:	lData = long(fData);			break;
	case		PS_MODULE_NO	:	lData = long(fData);			break;
	case		PS_LOT_NO		:	lData = long(fData);			break;
	case		PS_DATA_SEQ		:	lData = long(fData);			break;
	case		PS_AVG_CURRENT	:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
	case		PS_AVG_VOLTAGE	:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
	default:	lData = long(fData);								break;
	}
	return lData;
}

CString CCyclerChannel::GetLastErrorString()
{
	return	m_strLastErrorString;
}


//SBC에서 전달되어온 mili second data를 저장한다.
BOOL CCyclerChannel::SaveStepMiliSecData()
{
	CChannel *pCh = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pCh == NULL)		return FALSE;

	SFT_MSEC_CH_DATA_INFO *pMiliSecInfo;
	SFT_MSEC_CH_DATA *pMSecData;
	pMSecData = (SFT_MSEC_CH_DATA *)pCh->m_aStepStartData;
	pMiliSecInfo =  &pCh->m_StepStartDataInfo;

	//과부하시(2000개 이상파일 생성시) Event가 지연되어 같은 data에 2번 발생하는 경우 처리 
	//최소한 data 소실은 발생하더라도 중복 data가 저장되는 현상은 방지
	if(pMiliSecInfo->lDataCount < 1 || pMiliSecInfo->lDataCount > MAX_MSEC_DATA_POINT
		|| pMiliSecInfo->lTotCycNo == 0 || pMiliSecInfo->lStepNo == 0 )
	{
		//가끔 부하가 걸리면 같은 data에서 2번의 저장 이벤트가 발생 함
		//따라서 저장후에 lTotCycNo, lStepNo를 0으로 reset 시키고 이들이 0이면 저장하지 않음 
		TRACE("Data write fail!!!!!!!!!!!!!!!!, %d\n", pMiliSecInfo->lDataCount);
		return FALSE;
	}

	TRACE("!!!!!!!!!!!!!!!!!!!!!=================> START\n");

	CString strTemp, strFile;
	strFile.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", m_strFilePath, m_strTestName, pMiliSecInfo->lTotCycNo, pMiliSecInfo->lStepNo);

	BOOL bFind = FALSE;
	
//	CFileFind ff;
//	bFind = ff.FindFile(strTemp);	// 파일이 많으면 시간이 오래 걸림 

	//<io.h>
	if(_access((LPCTSTR)strFile, 0) != -1)	bFind = TRUE;
	
	//중복 파일일 경우 뒷부분에 이어서 저장 아니면 새로 생성
	CString strLine;
	if(bFind == FALSE)
	{
#ifdef _EDLC_TEST_SYSTEM
		strLine.Format("time(sec),S%d_V(mV),S%d_I(mA),\n", pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo);
#else
		strLine.Format("time(sec),S%d_V(mV),S%d_I(mA),S%d_C(mAh),S%d_WH(mWh)\n", pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo);
#endif
		//fprintf(fp, "time,S%d_V,S%d_I\n", pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo);
	}

	for(int i=0; i<pMiliSecInfo->lDataCount; i++)
	{
/*		strTemp.Format("%.1f,%.4f,%.4f\n",	ConvertSbcToPCUnit(pMSecData[i].lSec, PS_STEP_TIME),
											ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
											ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT)
											);
*/
		if(pMSecData[i].lSec < 0)	//음수로 표기되는 시간은 단위가 1msec 이다.
		{
#ifdef _EDLC_TEST_SYSTEM
			strTemp.Format("%.3f,%.4f,%.4f,\n", -(float)pMSecData[i].lSec/1000.0f,
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT)
												//ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
												);
#else
			strTemp.Format("%.3f,%.4f,%.4f,%.4f,%.4f\n", -(float)pMSecData[i].lSec/1000.0f,
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT),
												ConvertSbcToPCUnit(pMSecData[i].lData3, PS_CAPACITY),
												ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
												);
#endif
		}
		else
		{
#ifdef _EDLC_TEST_SYSTEM
			strTemp.Format("%.2f,%.4f,%.4f,\n",	ConvertSbcToPCUnit(pMSecData[i].lSec, PS_STEP_TIME),
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT)
												//ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
											);
#else
			strTemp.Format("%.2f,%.4f,%.4f,%.4f,%.4f\n",	ConvertSbcToPCUnit(pMSecData[i].lSec, PS_STEP_TIME),
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT),
												ConvertSbcToPCUnit(pMSecData[i].lData3, PS_CAPACITY),
												ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
											);
#endif
		}

		strLine += strTemp;

/*		fprintf(fp, "%.1f,%.4f,%.4f\n",	ConvertSbcToPCUnit(pMSecData[i].lSec, PS_STEP_TIME),
										ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
										ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT)
				);	
*/
//		fprintf(fp, "%d,%.1f,%d\n", 	m_nDebugCount++, ConvertSbcToPCUnit(pMSecData[i].lSec, PS_STEP_TIME), pMiliSecInfo->lTotCycNo+i);	
	}
	
	FILE *fp = fopen(strFile, "at+");
	if(fp)
	{
		fprintf(fp, strLine);
		fclose(fp);
		pMiliSecInfo->lDataCount = 0;	//0으로 reset하면 안됨(메모리 할당이 계속 이루어 지므로)
		pMiliSecInfo->lTotCycNo = 0;
		pMiliSecInfo->lStepNo = 0;
#ifdef _DEBUG
// 		if(m_nChannelIndex == 0)
// 		{
// 			static a = 1;
// //			TRACE("3[%03d] ===> Data save end\n", a++);
// //			TRACE("Save time %d msec\n",  GetTickCount()-dTime);
// 		}
#endif
		TRACE("!!!!!!!!!!!!!!!!!!!!!=================> END\n");

		return TRUE;
	}
	return FALSE;
}

//현재까지의 누적 용량을 Update한다.
float CCyclerChannel::AddTotalCapacity(WORD nType, float fCapacity)
{
	if(nType == PS_STEP_CHARGE)
	{
		m_fCapacitySum += fCapacity;
	}
	else if ( nType == PS_STEP_DISCHARGE || nType == PS_STEP_IMPEDANCE)
	{
		m_fCapacitySum -= fCapacity;
	}

	return m_fCapacitySum;
}

//작업에 대한 Log 기록 
void CCyclerChannel::WriteLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);

	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();

	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile )	aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}

BOOL CCyclerChannel::IsStopReserved()
{
	if(SFTGetReservedCmd(m_nModuleID, m_nChannelIndex) == 1)	return TRUE;

	return FALSE;
}

BOOL CCyclerChannel::IsPauseReserved()
{
	if(SFTGetReservedCmd(m_nModuleID, m_nChannelIndex) == 2)	return TRUE;
	
	return FALSE;
}


float CCyclerChannel::GetItemData(PS_STEP_END_RECORD &chData, WORD wItem)
{
	float fData = 0.0f;
	switch(wItem)
	{
	case		PS_STATE		:	fData = float(chData.chState);	break;
	case		PS_VOLTAGE		:	fData = chData.fVoltage;		break;		//u단위		//n단위	
	case		PS_CURRENT		:	fData = chData.fCurrent;		break;		//u단위		//n단위(전류종속 단위)
	case		PS_CAPACITY		:	fData = chData.fCapacity;		break;		//u단위		//n단위(전류종속 단위)
	case		PS_IMPEDANCE	:	fData = chData.fImpedance;		break;		//u단위		
	case		PS_CODE			:	fData = float(chData.chCode);	break;
	case		PS_STEP_TIME	:	fData = chData.fStepTime;			break;		//10msec
	case		PS_TOT_TIME		:	fData = chData.fTotalTime;			break;		//10msec
	case		PS_GRADE_CODE	:	fData = float(chData.chGradeCode);			break;
	case		PS_STEP_NO		:	fData = float(chData.chStepNo);				break;
	case		PS_WATT			:	fData = chData.fWatt;			break;		//mWatt		//u단위(전류종속 단위)
	case		PS_WATT_HOUR	:	fData = chData.fWattHour;		break;		//mWh		//u단위(전류종속 단위)
	//case		PS_TEMPERATURE	:	fData = chData.fTemparature;	break;		//m℃
	case		PS_OVEN_TEMPERATURE	:	fData = chData.fOvenTemperature;	break;		//m℃	//20080829 kjh Oven 온도를 저장
	case		PS_OVEN_HUMIDITY	:	fData = chData.fOvenHumidity;		break;		//m℃	//20080829 kjh Oven 온도를 저장
//	case		PS_AUX_VOLTAGE	:	fData = chData.fAuxVoltage;		break;		//m~
	case		PS_STEP_TYPE	:	fData = float(chData.chStepType);				break;
	case		PS_CUR_CYCLE	:	fData = float(chData.nCurrentCycleNum);			break;
	case		PS_TOT_CYCLE	:	fData = float(chData.nTotalCycleNum);			break;
//	case		PS_TEST_NAME	:	fData = float(lData);			break;
//	case		PS_SCHEDULE_NAME:	fData = float(lData);			break;
	case		PS_CHANNEL_NO	:	fData = float(chData.chNo);			break;
	case		PS_MODULE_NO	:	fData = float(m_nModuleID);			break;
//	case		PS_LOT_NO		:	fData = float(lData);			break;
	case		PS_DATA_SEQ		:	fData = float(chData.lSaveSequence);				break;
	case		PS_AVG_CURRENT	:	fData = chData.fAvgCurrent;		break;		//u단위		//n단위(전류종속 단위)
	case		PS_AVG_VOLTAGE	:	fData = chData.fAvgVoltage;		break;		//u단위		//n단위(전류종속 단위)
	case		PS_METER_DATA	:	fData = (float)m_fMeterValue;
	case		PS_CHARGE_CAP	:	fData = chData.fChargeAh;		break;
	case		PS_DISCHARGE_CAP	:	fData = chData.fDisChargeAh;	break;
	case		PS_CAPACITANCE	:	fData = chData.fCapacitance;	break;
	case		PS_CHARGE_WH	:	fData = chData.fChargeWh;		break;
	case		PS_DISCHARGE_WH	:	fData = chData.fDisChargeWh;	break;
	case		PS_CV_TIME		:	fData = chData.fCVTime;			break;
	case		PS_SYNC_DATE	:	fData = (float)chData.lSyncDate;		break;		//ljb 20140120 edit
	case		PS_SYNC_TIME	:	fData = chData.fSyncTime;		break;
	case		PS_ACC_CYCLE1	:	fData = float(chData.nAccCycleGroupNum1);				break;
	case		PS_ACC_CYCLE2	:	fData = float(chData.nAccCycleGroupNum2);				break;
	case		PS_ACC_CYCLE3	:	fData = float(chData.nAccCycleGroupNum3);				break;
	case		PS_ACC_CYCLE4	:	fData = float(chData.nAccCycleGroupNum4);				break;
	case		PS_ACC_CYCLE5	:	fData = float(chData.nAccCycleGroupNum5);				break;
	case		PS_MULTI_CYCLE1	:	fData = float(chData.nMultiCycleGroupNum1);				break;
	case		PS_MULTI_CYCLE2	:	fData = float(chData.nMultiCycleGroupNum2);				break;
	case		PS_MULTI_CYCLE3	:	fData = float(chData.nMultiCycleGroupNum3);				break;
	case		PS_MULTI_CYCLE4	:	fData = float(chData.nMultiCycleGroupNum4);				break;
	case		PS_MULTI_CYCLE5	:	fData = float(chData.nMultiCycleGroupNum5);				break;
	}
	return fData;
}

void CCyclerChannel::SetSaveItemList(CWordArray &awItem)
{
	if(awItem.GetSize() < 1)	return;
	ZeroMemory(&m_FileRecordSetHeader, sizeof(PS_RECORD_FILE_HEADER));	

	m_FileRecordSetHeader.nColumnCount = awItem.GetSize()+1;
	m_FileRecordSetHeader.awColumnItem[0] = PS_DATA_SEQ;					//Data Index Sequence
	for(int i=0; i<awItem.GetSize(); i++)
	{
		m_FileRecordSetHeader.awColumnItem[1+i] = awItem.GetAt(i);
	}
}

void CCyclerChannel::LoadSaveItemSet()
{
	CWordArray ItemArray;
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Time", TRUE))	ItemArray.Add(PS_STEP_TIME);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage", TRUE))	ItemArray.Add(PS_VOLTAGE);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Current", TRUE))	ItemArray.Add(PS_CURRENT);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Capacity", TRUE))	
	{
		ItemArray.Add(PS_CAPACITY);
		ItemArray.Add(PS_CHARGE_CAP);
		ItemArray.Add(PS_DISCHARGE_CAP);
		ItemArray.Add(PS_CAPACITANCE);
	}
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "WattHour", TRUE))
	{
		ItemArray.Add(PS_WATT_HOUR);
		ItemArray.Add(PS_CHARGE_WH);
		ItemArray.Add(PS_DISCHARGE_WH);
	}
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgCurrent", FALSE))		ItemArray.Add(PS_AVG_CURRENT);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgVoltage", FALSE))		ItemArray.Add(PS_AVG_VOLTAGE);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenTemperature", FALSE))ItemArray.Add(PS_OVEN_TEMPERATURE);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenHumidity", FALSE))	ItemArray.Add(PS_OVEN_HUMIDITY);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Meter", FALSE))			ItemArray.Add(PS_METER_DATA);
				
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "CVTime", FALSE))			ItemArray.Add(PS_CV_TIME);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "SyncTime", FALSE))	
	{
		ItemArray.Add(PS_SYNC_DATE);
		ItemArray.Add(PS_SYNC_TIME);
	}


	SetSaveItemList(ItemArray);
}

BOOL CCyclerChannel::SaveRunInfoToTempFile()
{
	CT_TEMP_FILE_DATA tempData;
	ZeroMemory(&tempData, sizeof(CT_TEMP_FILE_DATA));

	CString strTemp, strMsg;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));;

	FILE *fp;
	strMsg.Format("%s\\Temp\\M%02dC%02d.tmp", strTemp, m_nModuleID, m_nChannelIndex+1);
	fp = fopen(strMsg, "wt");

	if(fp == NULL)	return FALSE;
	
	sprintf(tempData.szFilePath, "%s", GetFilePath());
	sprintf(tempData.szLot, "%s", GetTestSerial());
	sprintf(tempData.szWorker, "%s", GetWorkerName());
	sprintf(tempData.szDescript, "%s", GetTestDescript());
	fwrite(&tempData, sizeof(CT_TEMP_FILE_DATA), 1, fp);

	//2006/6/12 명령어 예약 정보 기록 
	fwrite(&m_lReservedCycleNo, sizeof(long), 1, fp);
	fwrite(&m_lReservedStepNo, sizeof(long), 1, fp);

	fclose(fp);

	return TRUE;
}

//현재 적용된 Schedule 정보
CScheduleData * CCyclerChannel::GetScheduleData()
{
	if(m_strScheduleFileName.IsEmpty())	return NULL;
	if(m_pScheduleData == NULL)
	{
		m_pScheduleData = new CScheduleData;
		m_pScheduleData->SetSchedule(m_strScheduleFileName);
	}
	return m_pScheduleData;
}

void	CCyclerChannel::SetScheduleFile(CString strFileName)
{
	m_strScheduleFileName = strFileName; 
	if(m_pScheduleData != NULL)
	{
		delete m_pScheduleData;
		m_pScheduleData = NULL;
	}
}

CString CCyclerChannel::GetStartTime()
{
	CString strTime = m_startTime.Format();
	return strTime;
}

BOOL CCyclerChannel::GetSaveData(PS_STEP_END_RECORD &sChData)//kjh aux
{
/*	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	SFT_CH_DATA sSaveChData;
	if(pChannel->PopSaveData(sSaveChData)== FALSE)	return FALSE;

	sChData.chNo		= sSaveChData.chNo;					// Channel Number
	sChData.chState		= sSaveChData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType	= sSaveChData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect = sSaveChData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		= sSaveChData.chCode;
	sChData.chStepNo	= sSaveChData.chStepNo;
	sChData.chGradeCode = sSaveChData.chGradeCode;

	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.lCurrent, PS_CURRENT);
	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.lCapacity, PS_CAPACITY);
	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.lWattHour, PS_WATT_HOUR);
	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.ulStepTime, PS_STEP_TIME),			// 이번 Step 진행 시간
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.ulTotalTime, PS_TOT_TIME),			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
	sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.lTemparature, PS_TEMPERATURE);
	sChData.fPressure	= 0.0f;
	sChData.nCurrentCycleNum = sSaveChData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.nTotalCycleNum;
	sChData.lSaveSequence	= sSaveChData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.lAvgCurrent, PS_CURRENT);
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;*/
	return TRUE;
}

BOOL CCyclerChannel::GetSaveDataB(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData)
{
/*	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;
	
	SFT_VARIABLE_CH_DATA_A sSaveChData;
	ZeroMemory(&sSaveChData, sizeof(SFT_VARIABLE_CH_DATA_A));
	if(pChannel->PopSaveDataA(sSaveChData)== FALSE)	return FALSE;
//	CString strLog;
//	strLog.Format("Pop : 채널번호 : %d, Aux Count : %d", sSaveChData.chData.chNo, sSaveChData.chData.nAuxCount);
//	WriteLog(strLog);
	
	sChData.chNo		= sSaveChData.chData.chNo;					// Channel Number
	sChData.chState		= sSaveChData.chData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType	= sSaveChData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect = sSaveChData.chData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		= sSaveChData.chData.chCode;
	sChData.chStepNo	= sSaveChData.chData.chStepNo;
	sChData.chGradeCode = sSaveChData.chData.chGradeCode;
	
	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.chData.lCurrent, PS_CURRENT);
	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.chData.lCapacity, PS_CAPACITY);
	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.chData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.chData.lWattHour, PS_WATT_HOUR);
	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.chData.ulStepTime, PS_STEP_TIME),			// 이번 Step 진행 시간
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.chData.ulTotalTime, PS_TOT_TIME),			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.chData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
//	sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.chData.lTemparature, PS_TEMPERATURE);
	sChData.fPressure	= 0.0f;
	sChData.nCurrentCycleNum = sSaveChData.chData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.chData.nTotalCycleNum;
	sChData.lSaveSequence	= sSaveChData.chData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.chData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.chData.lAvgCurrent, PS_CURRENT);
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;

	
	sAuxData.auxCount = sSaveChData.chData.nAuxCount;
	sAuxData.auxData->chNo = sSaveChData.chData.chNo;					// Channel Number
	for(int i = 0; i < sAuxData.auxCount; i++)
	{
		sAuxData.auxData[i].chNo = sSaveChData.chData.chNo;
		sAuxData.auxData[i].chAuxNo = sSaveChData.auxData[i].chNo;
		sAuxData.auxData[i].chType = sSaveChData.auxData[i].chType;
		sAuxData.auxData[i].fValue = sSaveChData.auxData[i].lValue;
	}
	TRACE("하위버전 Save\r\n");*/
	return TRUE;
}

//return start index
ULONG CCyclerChannel::GetLastData(CString &strStartT, CString &strSerial, CString &strUser, CString &strDescript, PS_STEP_END_RECORD &sChData)
{
	CStdioFile resultFile;
	CFileStatus fs;
	CFileException e;

	CString strCurTempFile, strTemp;
	//////////////////////////////////////////////////////////////////////////
	//1. 임시 파일(.rp$)을 읽어 들인다. (Total 3 Line)
	//Line 1: 시작 시간 종류 시간
	//Line 2: column 목록 
	//Line 2: 최종 data
	CStringList aTempDataList;
	ZeroMemory(&sChData, sizeof(PS_STEP_END_RECORD));
	//최종 data 저장 임시 파일 
	strCurTempFile.Format("%s\\%s.rp$",  m_strFilePath, m_strTestName);						//.rp$
	if(resultFile.Open( strCurTempFile, CFile::modeRead|CFile::shareDenyWrite, &e ))
	{
		while(resultFile.ReadString(strTemp))
		{
			if(!strTemp.IsEmpty())	aTempDataList.AddTail(strTemp);
		}
		resultFile.Close();
		//2. 최종 시험 data를 Parsing 한다.
		if(aTempDataList.GetCount() >= 3)	
		{
			POSITION pos;
			//2.1 data parsing
			pos = aTempDataList.GetTailPosition();
			strTemp = aTempDataList.GetNext(pos);
			
			//CTable dataTable(aTempDataList.GetNext(pos) , strTemp);
			ParsingChDataString(strTemp, sChData);
		
			//2.2 Start Time and End Time Parsing
			int p1, p2, s;
			pos = aTempDataList.GetHeadPosition();
			// 첫줄에서
			// Start Time 정보 추출 
			strTemp   = m_strDataList.GetNext(pos);
			s  = strTemp.Find("StartT");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strStartT = strTemp.Mid(p1+1,p2-p1-1);
			
			// Test Serial 정보 추출 
			s  = strTemp.Find("Serial");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strSerial = strTemp.Mid(p1+1,p2-p1-1);
			
			// UserID 정보 추출 
			s  = strTemp.Find("User");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strUser = strTemp.Mid(p1+1,p2-p1-1);
			
			// Comment 정보 추출 
			s  = strTemp.Find("Descript");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strDescript = strTemp.Mid(p1+1,p2-p1-1);
		}		
	}
	else
	{
		WriteLog("Temporary result file open fail.");

		//load default data
		strStartT = m_startTime.Format();
		strSerial = m_strTestSerial;
		strUser = m_strUserID;
		strDescript = m_strDescript;
	}

	return sChData.nIndexFrom;
}

void CCyclerChannel::UpdateLastData(UINT nStartIndex, UINT nEndIndex, CString strStartT, CString strSerial, CString strUser, CString strDescript, PS_STEP_END_RECORD sChData)
{
	CString strCurTempFile, strDataString;
	strCurTempFile.Format("%s\\%s.rp$",  m_strFilePath, m_strTestName);						//.rp$

	//시험이 완료 되었으면 임시 파일을 삭제한다.
	if(sChData.chStepType == PS_STEP_END)
	{
		unlink(strCurTempFile);
		WriteLog(m_strTestName+" 작업 완료");
	}
	else	//시험이 완료되지 않았으면 다음 step 저장을 위해 Index를 저장한다. Step 번호를 증가 시키고 시작 Index를 저장한다.
	{
		//6. 최종 임시파일은 갱신한다.
		//No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temp,Press,AvgVoltage,AvgCurrent,	

		strDataString.Format(	"%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", 
									sChData.chStepNo,			//StepNo
									nStartIndex,				//IndexFrom	
									nEndIndex,					//IndexTo
									sChData.nCurrentCycleNum,	//CurCycle
									sChData.nTotalCycleNum,		//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
									sChData.chStepType,			//Type
									sChData.chState,			//State
									sChData.fStepTime,			//Time
									sChData.fTotalTime,			//TotTime
									sChData.chCode,				//Code
									sChData.chGradeCode,		//Grade
									sChData.fVoltage, 			//Voltage
									sChData.fCurrent, 			//Current
									sChData.fCapacity, 			//Capacity
									sChData.fWattHour, 			//WattHour
									sChData.fImpedance,			//IR
									sChData.fOvenTemperature, 		//Temp
									sChData.fOvenHumidity, 			//Humidity
									sChData.fAvgVoltage,		//AvgVoltage
									sChData.fAvgCurrent,		//AvgVoltage
									sChData.lSaveSequence,
									sChData.fChargeAh,									
									sChData.fDisChargeAh,
									sChData.fCapacitance,
									sChData.fChargeWh,
									sChData.fDisChargeWh,
									sChData.fCVTime,
									sChData.lSyncDate,
									sChData.fSyncTime,
									sChData.nAccCycleGroupNum1,
									sChData.nAccCycleGroupNum2,
									sChData.nAccCycleGroupNum3,
									sChData.nAccCycleGroupNum4,
									sChData.nAccCycleGroupNum5,
									sChData.nMultiCycleGroupNum1,
									sChData.nMultiCycleGroupNum2,
									sChData.nMultiCycleGroupNum3,
									sChData.nMultiCycleGroupNum4,
									sChData.nMultiCycleGroupNum5
						);
		//현재 step의 진행된 부분까지 tmp파일에 저장한다.
		FILE *fp = fopen(strCurTempFile, "wt");
		if(fp)
		{
			CString strLine1, strLine2;
			COleDateTime t = COleDateTime::GetCurrentTime();
			strLine1.Format("StartT=%s, EndT=%s, Serial=%s, User=%s, Descript=%s", strStartT, t.Format(), strSerial, strUser, strDescript);
			strLine2 = TABEL_FILE_COLUMN_HEAD;
			fprintf(fp, "%s\n%s\n%s", strLine1, strLine2, strDataString);
			fclose(fp);
		}
		//	TRACE("%s 파일 Udpated(%d Step Saved)\n", m_strResultStepFileName, strDataList.GetCount()-1);
	}
	//////////////////////////////////////////////////////////////////////////
}

SFT_AUX_DATA * CCyclerChannel::GetAuxData()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetAuxData();
}
int	CCyclerChannel::GetAuxCount()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetAuxCount();
}


BOOL CCyclerChannel::CreateAuxFileA(CString strFileName, int nFileIndex, int nAuxCount)
{
	CFile f;
	CFileException e;
	
	if(strFileName.IsEmpty())	return FALSE;
	
	if( !f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		return FALSE;
	}
	
	LPPS_FILE_ID_HEADER pFileHeader = new PS_FILE_ID_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	pFileHeader->nFileID = PS_PNE_AUX_FILE_ID;
	pFileHeader->nFileVersion = _PNE_AUX_FILE_VER;
	CTime t = CTime::GetCurrentTime();
	sprintf(pFileHeader->szCreateDateTime, "%s", t.Format("%y/%m/%d %H:%M:%S"));
	sprintf(pFileHeader->szDescrition, "%s", "PNE aux data file.");
	
	f.Write(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	delete pFileHeader;
	
	
	//2. Aux File Header 생성
	PS_AUX_DATA_FILE_HEADER fileAuxSetHeader;
	ZeroMemory(&fileAuxSetHeader, sizeof(PS_AUX_DATA_FILE_HEADER));

	fileAuxSetHeader.nColumnCount = nAuxCount;
	TRACE("Aux ColumnCount : %d\r\n", fileAuxSetHeader.nColumnCount);
	
	//fileAuxSetHeader.awColumnItem[0] = PS_AUX_OWNER_CH_NO;				//chNo
	for(int i = 0; i < nAuxCount; i++)
	{
		fileAuxSetHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
	}
	
	
	f.Write(&fileAuxSetHeader, sizeof(PS_AUX_DATA_FILE_HEADER));
	
	f.Flush();
	f.Close();
	
	m_strDataList.RemoveAll();
	
	WriteLog(strFileName+" 생성");
	
	return TRUE;
}
/*
BOOL CCyclerChannel::CreateAuxFileA(CString strFileName, int nFileIndex)
{
	CFile f;
	CFileException e;
	int nCount = 0;

	if(strFileName.IsEmpty())	return FALSE;
	
	if( !f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		return FALSE;
	}
	
	LPPS_FILE_ID_HEADER pFileHeader = new PS_FILE_ID_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	pFileHeader->nFileID = PS_PNE_AUX_FILE_ID;
	pFileHeader->nFileVersion = _PNE_AUX_FILE_VER;
	CTime t = CTime::GetCurrentTime();
	sprintf(pFileHeader->szCreateDateTime, "%s", t.Format("%y/%m/%d %H:%M:%S"));
	sprintf(pFileHeader->szDescrition, "%s", "PNE aux data file.");
	
	f.Write(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	delete pFileHeader;
	
	
	//2. Aux File Header 생성
	PS_AUX_DATA_FILE_HEADER fileAuxSetHeader;

	if(m_nAuxCount > m_nAuxCount - PS_MAX_AUX_FILE_ITEM * nFileIndex)
		nCount = PS_MAX_AUX_FILE_ITEM;
	else
		nCount = m_nAuxCount % PS_MAX_AUX_FILE_ITEM;


	fileAuxSetHeader.nColumnCount = 1 + nCount* 3;
	fileAuxSetHeader.awColumnItem[0] = PS_AUX_OWNER_CH_NO;				//chNo
	for(int i = 0; i < nCount; i++)
	{
		fileAuxSetHeader.awColumnItem[i*3+1] = PS_AUX_CHANNEL_NO;		//auxChNo
		fileAuxSetHeader.awColumnItem[i*3+2] = PS_AUX_TYPE;				//auxType
		fileAuxSetHeader.awColumnItem[i*3+3] = PS_AUX_VALUE;			//auxValue
		//fileAuxSetHeader.awColumnItem[i*3+5] = 5;				//reserved
	}
	
	
	f.Write(&fileAuxSetHeader, sizeof(PS_AUX_DATA_FILE_HEADER));
	
	f.Flush();
	f.Close();
	
	m_strDataList.RemoveAll();
	
	WriteLog(strFileName+" 생성");
	
	return TRUE;
}
*/

//사용안함 - 20080104 KJH
float CCyclerChannel::GetAuxItemData(PS_AUX_RAW_DATA auxData, WORD wItem, int nAuxIndex)
{

	float fData = 0.0f;
	/*switch(wItem)
	{			
	case PS_AUX_OWNER_CH_NO:	fData = float(auxData.auxData[nAuxIndex].chNo); break;
	case PS_AUX_CHANNEL_NO:	fData = float(auxData.auxData[nAuxIndex].chAuxNo); break;
	case PS_AUX_TYPE:	fData = float(auxData.auxData[nAuxIndex].chType); break;
	case PS_AUX_VALUE:	fData = float(auxData.auxData[nAuxIndex].fValue); break;
	}*/

	return fData;
}

int CCyclerChannel::GetChFile(CFile & fRS, PS_RAW_FILE_HEADER & rawHeader)
{
		
	CFileStatus fs;
	CFileException e;


	
	//파일명 생성 
	//향후 Cycle별로 파일을 1개씩 생성한다
	//	strTemp.Format("%s\\%s_%06d.cyc", m_strFilePath, m_strTestName, 0);
	m_strResultFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_RAW_FILE_NAME_EXT);
	//파일 존재 여부를 검사하여 없으면 생성한다.
	if( CFile::GetStatus(m_strResultFileName, fs) == FALSE) 
	{
		if(CreateResultFileA(m_strResultFileName) == FALSE)
		{
			TRACE("%s를 생성할 수 없습니다.", m_strResultFileName);
			return -21 ;
		}
	}
	
	//파일을 Open 한다.
	if (fRS.m_hFile == CFile::hFileNull)
	{
		if( !fRS.Open( m_strResultFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
		{
#ifdef _DEBUG
			afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
			return -22;
		}
	}
	
	//skip file header
	//read record column set
	if( fRS.Read(&rawHeader, sizeof(PS_RAW_FILE_HEADER)) != sizeof(PS_RAW_FILE_HEADER))
	{
		fRS.Close();
		return -23;
	}

	fRS.SeekToEnd();
	return 0;
}


int CCyclerChannel::GetAuxFile(CFile * pFile, PS_AUX_FILE_HEADER * auxHeader)
{
	CFileStatus	fAuxStatus;		
	CFileException e;

	if(GetTotalAuxCount() < 0 || GetTotalAuxCount() > _SFT_MAX_MAPPING_AUX)
 		return -11;

	CString  m_strAuxFileName;

	m_strAuxFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_AUX_FILE_NAME_EXT);

	if(CFile::GetStatus(m_strAuxFileName, fAuxStatus) == FALSE)
	{
 		if(CreateAuxFileA(m_strAuxFileName, 0, GetAuxCount()) ==FALSE)
 		{
 			TRACE("%s를 생성할 수 없습니다.", m_strAuxFileName);
 			return -12;
 		}
	}

	//파일을 Open 한다.
	if (pFile->m_hFile == CFile::hFileNull)
	{
 		if( !pFile->Open( m_strAuxFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
 		{
 		#ifdef _DEBUG
 			afxDump << "File could not be opened " << e.m_cause << "\n";
 		#endif
 			return -13;
 		}
	}
 	
 	//skip file header
 	
 	//read record column set
 	if( pFile->Read(auxHeader, sizeof(PS_AUX_FILE_HEADER)) != sizeof(PS_AUX_FILE_HEADER))
 	{
 		pFile->Close();
 		return -14;
 	}
 	pFile->SeekToEnd();
 	
 	return 0;
 }
/*
//Aux 최대 갯수는 128개이다.
//Aux 1개당 포함되는 데이터는 4개(AuxChNo, AuxType, Value, Reserved) 이다.
//엑셀에서 사용할 수 있는 최대 Column수 보다 작게 Column 수를 유지하기 위해
//한 파일당 32개 Aux 정보가 저정되도록 한다. 파일 갯수는 4개이다.
//파일 마다 해더 정보(Column 갯수등)를 쓰고 파일에 끝을 찾은 후 전달한다.
int CCyclerChannel::GetAuxFile(CFile * pFile, PS_AUX_FILE_HEADER * auxHeader, int & nFileCount)
{
	CFileStatus	fAuxStatus;		
	CFileException e;

	if(m_nAuxCount < 0 || m_nAuxCount > _SFT_MAX_MAPPING_AUX)
		return -12;

	if(m_nAuxCount > PS_MAX_AUX_FILE_ITEM)  // 파일이 여러개 생성될 경우
	{
		CString	 AuxFileName[PS_MAX_AUX_FILE_COUNT];

		nFileCount = m_nAuxCount % PS_MAX_AUX_FILE_ITEM;	//파일 갯수 저장

		for(int nFileIndex = 0 ; nFileIndex < nFileCount; nFileIndex++)
		{
			//파일 경로
			AuxFileName[nFileIndex].Format("%s\\%s(%d).%s", m_strFilePath, m_strTestName,nFileIndex, PS_AUX_FILE_NAME_EXT);

			//파일이 존재하지 않으면 파일 생성
			if(CFile::GetStatus(AuxFileName[nFileIndex], fAuxStatus) == FALSE)
			{
				//파일을 생성하고 해더 정보를 파일에 쓴다.
				if(CreateAuxFileA(AuxFileName[nFileIndex], nFileIndex) ==FALSE)
				{
					TRACE("%s를 생성할 수 없습니다.", AuxFileName[nFileIndex]);
					return -11;
				}
			}
			
			//파일을 Open 한다.
			if (pFile[nFileIndex].m_hFile == CFile::hFileNull)
			{
				if( !pFile[nFileIndex].Open( AuxFileName[nFileIndex], CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
				{
				#ifdef _DEBUG
					afxDump << "File could not be opened " << e.m_cause << "\n";
				#endif
					return -2;
				}
			}
			
			//skip file header			
			//해더 정보를 읽어온다.
			if( pFile[nFileIndex].Read(&auxHeader[nFileIndex], sizeof(PS_AUX_FILE_HEADER)) != sizeof(PS_AUX_FILE_HEADER))
			{
				pFile[nFileIndex].Close();
				return -9;
			}

			pFile[nFileIndex].SeekToEnd();
		}
	}
	else		//파일이 1개 생성될 경우
	{
		nFileCount = 1;
		CString  m_strAuxFileName;

		m_strAuxFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_AUX_FILE_NAME_EXT);

		if(CFile::GetStatus(m_strAuxFileName, fAuxStatus) == FALSE)
		{
			if(CreateAuxFileA(m_strAuxFileName, 0) ==FALSE)
			{
				TRACE("%s를 생성할 수 없습니다.", m_strAuxFileName);
				return -1;
			}
		}
		
		//파일을 Open 한다.
		if (pFile->m_hFile == CFile::hFileNull)
		{
			if( !pFile->Open( m_strAuxFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
			{
			#ifdef _DEBUG
				afxDump << "File could not be opened " << e.m_cause << "\n";
			#endif
				return -2;
			}
		}
		
		//skip file header
		
		//read record column set
		if( pFile->Read(auxHeader, sizeof(PS_AUX_FILE_HEADER)) != sizeof(PS_AUX_FILE_HEADER))
		{
			pFile->Close();
			return -9;
		}
		pFile->SeekToEnd();
	}
	
	
	
	return 0;
}
*/



//스케줄 시작시 시험 폴더안에 Aux Title 파일을 복사한다.
void CCyclerChannel::SetAuxTitleFile(CString strAuxPath, CString strTestName)
{
// 	char path[MAX_PATH];
//     GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
// 	CString strTemp(path);
	CString strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path"); //ksj 20210507
	CString strPath;
	strTemp += "\\";
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strAuxPath.Mid(strAuxPath.ReverseFind('\\')+1, 7), PS_AUX_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("%sAUX.%s", strTestName, "tlt"); 
	CString strNew = strAuxPath+"\\"+strTemp;
	
		
   	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return;

	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	if(CopyFile(strPath, strNew, TRUE) == FALSE)
	{
		AfxMessageBox(strTemp+" Data 복사에 실패했습니다.");
		return;
	}
	WriteLog("Aux Title 파일 복사 완료");
	
}

void CCyclerChannel::SetParallel(BOOL bParallel, BOOL bMaster)
{
	m_bParallel = bParallel;
	m_bMaster = bMaster;

}

BOOL CCyclerChannel::IsParallel()
{
	return m_bParallel;
}

int CCyclerChannel::GetMasterChannelNum()
{
	return m_uMasterCh;
}

void CCyclerChannel::SetMasterChannelNum(unsigned char uMaster)
{
	m_uMasterCh = uMaster;
}

//1. Aux 추가
//DEL long CCyclerChannel::WriteResultListFileF(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData)
//DEL {
//DEL 	DWORD dwStartTime = GetTickCount();
//DEL 
//DEL 	if(m_strFilePath.IsEmpty())
//DEL 	{
//DEL 		TRACE("File path is empty");
//DEL 		return -1;
//DEL 	}
//DEL 
//DEL 	CFileStatus fs;
//DEL 	CFileException e;
//DEL 	COleDateTime t = COleDateTime::GetCurrentTime();
//DEL 	long nStartIndex=0, nEndIndex=0;
//DEL 	CString strTemp;
//DEL 	CFile rltData;
//DEL 
//DEL 	CString strStartTime, strEndTime, strTestSerial, strUser, strDescript, strLineNo;
//DEL 	strStartTime = m_startTime.Format();
//DEL 	strTestSerial = m_strTestSerial;
//DEL 	strUser = m_strUserID;
//DEL 	strDescript = m_strDescript;
//DEL 
//DEL 	//결과 파일명
//DEL 	m_strResultStepFileName.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_RESULT_FILE_NAME_EXT);		//.
//DEL 	nStartIndex = lStartNo;
//DEL 	//////////////////////////////////////////////////////////////////////////
//DEL 
//DEL 	//3. Step end이면 결과 파일에 저장한다. 
//DEL 	//////////////////////////////////////////////////////////////////////////
//DEL 	nEndIndex = nLineNo;
//DEL 	if(sChData.chDataSelect == SFT_SAVE_STEP_END)
//DEL 	{
//DEL 		//3.1 기존 결과 파일이 존재하지 않으면 생성 
//DEL 		if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		 
//DEL 		{
//DEL 			PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
//DEL 			ZeroMemory(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
//DEL 			pFileHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
//DEL 			pFileHeader->fileHeader.nFileVersion = 0x1000;
//DEL 			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
//DEL 			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
//DEL 			sprintf(pFileHeader->testHeader.szStartTime, "%s", strStartTime);
//DEL 			sprintf(pFileHeader->testHeader.szEndTime, "%s", t.Format());
//DEL 			sprintf(pFileHeader->testHeader.szSerial, "%s", strTestSerial);
//DEL 			sprintf(pFileHeader->testHeader.szUserID, "%s", strUser);
//DEL 			sprintf(pFileHeader->testHeader.szDescript, "%s", strDescript);
//DEL 			pFileHeader->testHeader.nRecordSize = 12;
//DEL 			pFileHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
//DEL 			pFileHeader->testHeader.wRecordItem[1] = PS_CURRENT;
//DEL 			pFileHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
//DEL 			pFileHeader->testHeader.wRecordItem[3] = PS_WATT;
//DEL 			pFileHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
//DEL 			pFileHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
//DEL 			pFileHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
//DEL 			pFileHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
//DEL 			pFileHeader->testHeader.wRecordItem[8] = PS_TEMPERATURE;
//DEL 			pFileHeader->testHeader.wRecordItem[9] = PS_PRESSURE;
//DEL 			pFileHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
//DEL 			pFileHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
//DEL 			
//DEL 			if(rltData.Open(m_strResultStepFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
//DEL 			{
//DEL 				rltData.Write(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));	
//DEL 			}
//DEL 			else
//DEL 			{
//DEL 				delete pFileHeader;
//DEL 				return -1;
//DEL 			}
//DEL 			delete pFileHeader;
//DEL 
//DEL 			nStartIndex = 0; 
//DEL 		}//End if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		
//DEL 		//3.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
//DEL 		else
//DEL 		{
//DEL 			long fsize = 0;
//DEL 			if(!rltData.Open(m_strResultStepFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
//DEL 			{
//DEL 				return -1;
//DEL 			}
//DEL 			fsize = rltData.SeekToEnd();
//DEL 
//DEL 			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
//DEL 			if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
//DEL 			{
//DEL 				//End Time 갱신
//DEL 				PS_TEST_FILE_HEADER *pFileHeader = new PS_TEST_FILE_HEADER;
//DEL 				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
//DEL 				rltData.Read(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
//DEL 				sprintf(pFileHeader->szEndTime, "%s", t.Format());
//DEL 				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
//DEL 				rltData.Write(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
//DEL 				delete pFileHeader;
//DEL 
//DEL 				//가장 마지막 data read
//DEL 				LPPS_STEP_END_RECORD pDataRecord = new PS_STEP_END_RECORD;
//DEL 				rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
//DEL 				rltData.Read(pDataRecord, sizeof(PS_STEP_END_RECORD));
//DEL 
//DEL 				//같은 종류의 step이 이미 저장되어 있을 경우 
//DEL 				//마지막을 현재 data로 하여 갱신한다.
//DEL 				//작업 진행중에 data 복구를 실시할 경우 발생할 수 있다.
//DEL 				if(pDataRecord->chStepNo == sChData.chStepNo && pDataRecord->nTotalCycleNum == sChData.nTotalCycleNum)
//DEL 				{
//DEL 					rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
//DEL 					nStartIndex = pDataRecord->nIndexFrom;
//DEL 				}
//DEL 				else
//DEL 				{
//DEL 					rltData.Seek(0, CFile::end);
//DEL 					//////////////////////////////////////////////////////////////////////////
//DEL 					//debugging check
//DEL 					//손실 data 복구시 모듈에서 아직 전송되지 않은 더 많은 data를 이미 복구해 버리면 발생할 수 있다.
//DEL 					//
//DEL 					if(nStartIndex != pDataRecord->nIndexTo+1)
//DEL 					{
//DEL 						strTemp.Format("Ch %d End file index error Step %d, Total Cycle %d/%d. (Prev range : %d~%d, Next start : %d), Size %d", 
//DEL 										m_nChannelIndex+1, pDataRecord->chStepNo, sChData.nTotalCycleNum, pDataRecord->nTotalCycleNum,
//DEL 										pDataRecord->nIndexFrom, pDataRecord->nIndexTo, nStartIndex, fsize
//DEL 							);
//DEL 						WriteLog(strTemp);
//DEL #ifdef _DEBUG
//DEL 						AfxMessageBox(strTemp);
//DEL #endif
//DEL 					}
//DEL 					//////////////////////////////////////////////////////////////////////////
//DEL 
//DEL 					//시작 Index는 이전 Step완료의 마지막 Index에서 시작 
//DEL 					nStartIndex = pDataRecord->nIndexTo+1;
//DEL 				}
//DEL 				delete pDataRecord;
//DEL 			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
//DEL 			//File Error
//DEL 			else
//DEL 			{
//DEL 				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
//DEL 								sChData.chStepNo, sChData.nTotalCycleNum,
//DEL 								fsize
//DEL 								);
//DEL 				WriteLog(strTemp);
//DEL 				nStartIndex = 0;
//DEL 				if(rltData.m_hFile != NULL)	rltData.Close();
//DEL 				return -1;
//DEL 			}
//DEL 		}	//End else
//DEL 
//DEL 		sChData.nIndexFrom = nStartIndex;
//DEL 		sChData.nIndexTo = nEndIndex;
//DEL 	
//DEL 		//결과 파일을 갱신한다.
//DEL 		if(rltData.m_hFile != NULL)
//DEL 		{
//DEL 			long fsize = 0;
//DEL 
//DEL 			rltData.Write(&sChData, sizeof(PS_STEP_END_RECORD));
//DEL 			rltData.Flush();
//DEL 			fsize = rltData.GetLength();
//DEL 			rltData.Close();
//DEL 
//DEL 			strTemp.Format("Step %d [%s]완료(%d~%d). %dmsec/Size: %d",  
//DEL 				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sChData.nIndexFrom, sChData.nIndexTo, GetTickCount()-dwStartTime, fsize);
//DEL 			WriteLog(strTemp);
//DEL 		}
//DEL 		
//DEL 		//용량 합산 표시를 위한 total용량 갱신 
//DEL 		AddTotalCapacity(sChData.chStepType, (float)ConvertPCUnitToSbc(sChData.fCapacity, PS_CAPACITY));
//DEL 
//DEL 		//다음 Step의 시작 Index 생성 
//DEL 		nStartIndex = nEndIndex+1;
//DEL 
//DEL 		//Send result to database
//DEL 		//20070305 BY KBH
//DEL 		//Send message to database client 
//DEL 		//////////////////////////////////////////////////////////////////////////
//DEL 		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DataBaseUpdate", FALSE))
//DEL 		{
//DEL 			CString strTemp;
//DEL 			TCHAR szCurDir[MAX_PATH];
//DEL 			::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
//DEL 			strTemp.Format("%s\\DataProServer.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));
//DEL 
//DEL 			//Message를 전송한다.
//DEL 			HWND hWnd = ::FindWindow(NULL, "CTSDataPro Server");
//DEL 			if(hWnd)
//DEL 			{
//DEL 				char szData[MAX_PATH];
//DEL 				ZeroMemory(szData, MAX_PATH);
//DEL 				sprintf(szData, "%s", m_strFilePath);
//DEL 				COPYDATASTRUCT CpStructData;
//DEL 				CpStructData.dwData = sChData.chStepNo;		
//DEL 				CpStructData.cbData = m_strFilePath.GetLength();
//DEL 				CpStructData.lpData = szData;
//DEL 				::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
//DEL 				
//DEL //				sprintf(szData, "Send M%d C%d step %d data to data base server", m_nModuleID, m_nChannelIndex+1, sChData.chStepNo);
//DEL //				::SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szData);
//DEL 			}
//DEL 			//////////////////////////////////////////////////////////////////////////
//DEL 		}
//DEL 	} //if(sChData.chDataSelect == SFT_SAVE_STEP_END)
//DEL 	//4 Aux Result File를 생성한다.
//DEL 	if(sAuxData.auxCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
//DEL 	{
//DEL 		CFile auxFile;
//DEL 		CString strResultAuxFile;
//DEL 		strResultAuxFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_AUX_RESULT_FILE_NAME_EXT);
//DEL 		//4.1 기존 결과 파일이 존재하지 않으면 생성 
//DEL 		if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)		 
//DEL 		{
//DEL 			PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
//DEL 			ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
//DEL 			pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
//DEL 			pFileHeader->fileHeader.nFileVersion = 0x1000;
//DEL 			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
//DEL 			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			
//DEL 		
//DEL 			pFileHeader->auxHeader.nColumnCount = sAuxData.auxCount;
//DEL 	
//DEL 			for(int i = 0; i < sAuxData.auxCount; i++)
//DEL 			{
//DEL 				pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
//DEL 			}
//DEL 			
//DEL 			if(auxFile.Open(strResultAuxFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
//DEL 			{
//DEL 				auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
//DEL 			}
//DEL 			else
//DEL 			{
//DEL 				delete pFileHeader;
//DEL 				return -1;
//DEL 			}
//DEL 			delete pFileHeader;
//DEL 			TRACE("Aux Result File 생성\r\n");
//DEL 
//DEL 		}//End if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)
//DEL 
//DEL 		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
//DEL 		else
//DEL 		{
//DEL 			long fsize = 0;
//DEL 			if(!auxFile.Open(strResultAuxFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
//DEL 			{
//DEL 				return -1;
//DEL 			}
//DEL 			fsize = auxFile.SeekToEnd();
//DEL 
//DEL 			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
//DEL 			if(fsize >= sizeof(PS_AUX_FILE_HEADER))
//DEL 			{
//DEL 				//End Time 갱신
//DEL 				PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
//DEL 				auxFile.Seek(sizeof(PS_AUX_FILE_HEADER), CFile::begin);
//DEL 				auxFile.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
//DEL 
//DEL 				delete pFileHeader;
//DEL 				//가장 마지막 data read
//DEL 				float * pfBuff = new float[sAuxData.auxCount];
//DEL 				auxFile.Seek(fsize - sizeof(float)*sAuxData.auxCount, CFile::begin);
//DEL 				auxFile.Read(pfBuff, sizeof(float)*sAuxData.auxCount);
//DEL 				delete [] pfBuff;
//DEL 
//DEL 			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
//DEL 			//File Error
//DEL 			else
//DEL 			{
//DEL 				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
//DEL 								sChData.chStepNo, sChData.nTotalCycleNum,
//DEL 								fsize
//DEL 								);
//DEL 				WriteLog(strTemp);
//DEL 
//DEL 				if(auxFile.m_hFile != NULL)	auxFile.Close();
//DEL 				return -1;
//DEL 			}
//DEL 			TRACE("기존 Aux Result File Open\r\n");
//DEL 		}	//End else
//DEL 
//DEL 		//결과 파일을 갱신한다.
//DEL 		if(auxFile.m_hFile != NULL)
//DEL 		{
//DEL 			long fsize = 0;
//DEL 
//DEL 			float * pfBuff = new float[sAuxData.auxCount];
//DEL 			for(int i = 0 ; i < sAuxData.auxCount; i++)
//DEL 				pfBuff[i] = sAuxData.auxData[i].fValue;
//DEL 			auxFile.Write(pfBuff, sizeof(float)*sAuxData.auxCount);
//DEL 			auxFile.Flush();
//DEL 			auxFile.Close();
//DEL 			
//DEL 
//DEL 			strTemp.Format("Step %d [%s] Aux Count : %d 완료",  
//DEL 				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sAuxData.auxCount);
//DEL 			WriteLog(strTemp);
//DEL 			TRACE(strTemp+"\r\n");
//DEL 			TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
//DEL 								pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);
//DEL 			delete [] pfBuff;
//DEL 		}
//DEL 	
//DEL 	}
//DEL 
//DEL 
//DEL 
//DEL 	//////////////////////////////////////////////////////////////////////////
//DEL 	//else	//Delta t, Delta V, Delta I, Delta T etc... 일 경우는 Start Index는 변화 없습
//DEL 
//DEL 	return nStartIndex;
//DEL }

SFT_CAN_DATA * CCyclerChannel::GetCanData()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetCanData();
}

int CCyclerChannel::GetCanCount()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetCanCount();
}

BOOL CCyclerChannel::GetSaveDataC(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA & sCanData)
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;
	
	SFT_VARIABLE_CH_DATA_V1004 sSaveChData;
	ZeroMemory(&sSaveChData, sizeof(SFT_VARIABLE_CH_DATA_V1004));
	if(pChannel->PopSaveData(sSaveChData)== FALSE)	return FALSE;
//	CString strLog;
//	strLog.Format("Pop : 채널번호 : %d, Aux Count : %d", sSaveChData.chData.chNo, sSaveChData.chData.nAuxCount);
//	WriteLog(strLog);
	
	sChData.chNo		= sSaveChData.chData.chNo;					// Channel Number
	sChData.chState		= sSaveChData.chData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType	= sSaveChData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect = sSaveChData.chData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		= sSaveChData.chData.chCode;
	sChData.chStepNo	= sSaveChData.chData.chStepNo;
	sChData.chGradeCode = sSaveChData.chData.chGradeCode;
	
	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.chData.lCurrent, PS_CURRENT);
	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.chData.lCapacity, PS_CAPACITY);
	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.chData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.chData.lWattHour, PS_WATT_HOUR);
	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.chData.ulStepTime, PS_STEP_TIME),			// 이번 Step 진행 시간
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.chData.ulTotalTime, PS_TOT_TIME),			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.chData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
//	sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.chData.lTemparature, PS_TEMPERATURE);
//	sChData.fOvenHumidity	= 0.0f;
	sChData.nCurrentCycleNum = sSaveChData.chData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.chData.nTotalCycleNum;
	sChData.lSaveSequence	= sSaveChData.chData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.chData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.chData.lAvgCurrent, PS_CURRENT);
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;

//	TRACE("GetSaveDataC -> Capacity : %f, WattHour : %f\n",sChData.fCapacity, sChData.fWattHour);
	
	sAuxData.auxCount = sSaveChData.chData.nAuxCount;
	sAuxData.auxData->chNo = sSaveChData.chData.chNo;					// Channel Number
	for(int i = 0; i < sAuxData.auxCount; i++)
	{
		sAuxData.auxData[i].chNo = sSaveChData.chData.chNo;
		sAuxData.auxData[i].chAuxNo = sSaveChData.auxData[i].auxChNo;
		sAuxData.auxData[i].chType = sSaveChData.auxData[i].auxChType;
		if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TEMPERATURE);
		//else //20180607 yulee 주석처리
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_VOLTAGE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_VOLTAGE);
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE_TH) //20180607 yulee
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TYPE_TEMPERATURE_TH);


	}
	sCanData.canCount = sSaveChData.chData.nCanCount;
//	TRACE("Can Count : %d\r\n", sCanData.canCount);
	for(int i = 0 ; i < sCanData.canCount; i++)
	{
		sCanData.canData[i].chNo = sSaveChData.chData.chNo;
		sCanData.canData[i].canType	= sSaveChData.canData[i].canType;
		memcpy(&sCanData.canData[i].canVal, &sSaveChData.canData[i].canVal, sizeof(CAN_VALUE));
	}
	
	return TRUE;
}

//2007/10/17 
//1. CAN 추가
//2. ITEM 개수 128 -> 온도 : 256, 전압 : 256, Master CAN : 256, Slave CAN : 256 으로 변경

int CCyclerChannel::CheckAndSaveData()
{
	//파일명 설정 여부 확인 
	if(m_strFilePath.IsEmpty())
	{
		return 0;
	}

	//외부에서 파일 조작을 할 경우file update를 Lock 시킨 후 update 실시하기 위함 
	if(m_bLockFileUpdate == TRUE)
	{
		TRACE("Ch %d file update is locked\n", m_nChannelIndex+1);
		return 0;
	}

	//저장할 Data가 없으면 return
	if(GetStackedSize() < 1)	return 0;

	int nRtn = 1;

	//Add 2006/8/18
	//Load last temp data
	//////////////////////////////////////////////////////////////////////////
	PS_STEP_END_RECORD prevData;
	CString strStartTime, strTestSerial, strUser, strDescript;
	long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
	TRACE("Last data start index %d\n", lStartIndex);
	//////////////////////////////////////////////////////////////////////////

//	TRACE("Channel %d data save start\n", m_nChannelIndex+1);
	
	//2007/08/09 Aux Data 저장용//////////////////////////////////
	//채널 데이터가 저장하는 방식을 따른다.

	PS_AUX_RAW_DATA sAuxData;
	float *pfAuxBuff = NULL;
	size_t auxSize;
	size_t auxLineSize;
	CFile auxFile;
	PS_AUX_FILE_HEADER auxHeader;
	int nAuxColumnSize;
//	int  nAuxFileCount;
//	PS_AUX_RAW_DATA auxData;
	

	if(nRtn = GetAuxFile(&auxFile, &auxHeader) < 0)	
	{
		return nRtn;
	}
	
	nAuxColumnSize = auxHeader.auxHeader.nColumnCount;		//save data setting
	
	if(nAuxColumnSize < 0 || nAuxColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
	{
		CString strMsg;
		strMsg.Format("AuxColumn : %d", nAuxColumnSize);
		WriteLog(strMsg);
		auxFile.Close();
		return -1;
	}
		
	if(nAuxColumnSize > 0)
	{
		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
		auxLineSize =  sizeof(float)*nAuxColumnSize;
		pfAuxBuff = new float[nAuxColumnSize*_MAX_DISK_WIRTE_COUNT];
		auxSize = auxLineSize*_MAX_DISK_WIRTE_COUNT;
		
		//Line 수를 Count
		int nAuxBodySize = auxFile.GetLength()-sizeof(PS_AUX_FILE_HEADER);
		int nAuxRsCount = nAuxBodySize / auxLineSize ;
		if(nAuxRsCount < 0 || nAuxBodySize % auxLineSize)
		{
			//		TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
			auxFile.Close();
			return -2;
		}
	}
	
	//////////////////////////////////////////////////////////////
	//2007/10/17 CAN Data 저장용//////////////////////////////////
	//채널 데이터가 저장하는 방식을 따른다.

	PS_CAN_RAW_DATA sCanData;
	SFT_CAN_VALUE *pCanBuff = NULL;
	size_t canSize;
	size_t canLineSize;
	CFile canFile;
	PS_CAN_FILE_HEADER canHeader;
	int nCanColumnSize;
//	int  nCanFileCount;
//	PS_CAN_RAW_DATA canData;
	

	if(nRtn = GetCanFile(&canFile, &canHeader) < 0)	
	{
		return nRtn;
	}
	
	nCanColumnSize = canHeader.canHeader.nColumnCount;		//save data setting
	
	if(nCanColumnSize < 0 || nCanColumnSize > PS_MAX_CAN_SAVE_FILE_COLUMN)
	{
		CString strMsg;
		strMsg.Format("CanColumn : %d", nCanColumnSize);
		WriteLog(strMsg);
		canFile.Close();
		return -1;
	}
		
	if(nCanColumnSize > 0)
	{
		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
		canLineSize =  sizeof(SFT_CAN_VALUE)*nCanColumnSize;
		pCanBuff = new SFT_CAN_VALUE[nCanColumnSize*_MAX_DISK_WIRTE_COUNT];
		canSize = canLineSize*_MAX_DISK_WIRTE_COUNT;
		
		//Line 수를 Count
		int nCanBodySize = canFile.GetLength()-sizeof(PS_CAN_FILE_HEADER);
		int nCanRsCount = nCanBodySize / canLineSize ;
		if(nCanRsCount < 0 || nCanBodySize % canLineSize)
		{
			CString strMsg;
			strMsg.Format("CAN File Size Error (CanColumn : %d)", nCanColumnSize);
			WriteLog(strMsg);
			canFile.Close();
			return -2;
		}
	}
	
	//////////////////////////////////////////////////////////////
	
	//채널 데이터 저장용///////////////////////////////////////////////
	PS_RAW_FILE_HEADER rawHeader;
	CFile fRS;
	PS_STEP_END_RECORD	sChResultData;
	float *pfBuff;
	size_t rsSize, lineSize;

	if(nRtn = GetChFile(fRS,rawHeader) < 0)
	{
		return nRtn;
	}
	
	int nColumnSize = rawHeader.rsHeader.nColumnCount;		//save data setting

	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		auxFile.Close();
		canFile.Close();
		fRS.Close();
		return -3;
	}

	//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
	lineSize = sizeof(float)*nColumnSize;
	pfBuff = new float[nColumnSize*_MAX_DISK_WIRTE_COUNT];
	rsSize = lineSize*_MAX_DISK_WIRTE_COUNT;

	//Line 수를 Count
	int nBodySize = fRS.GetLength()-sizeof(PS_RAW_FILE_HEADER);
	int nRsCount = nBodySize / lineSize ;
	if(nRsCount < 0 || nBodySize % lineSize)
	{
//		TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
		auxFile.Close();
		canFile.Close();
		fRS.Close();
		return -4;
	}
	
	int nStackCount = 0;
	

#ifdef _DEBUG
	DWORD dwStart = GetTickCount();
#endif
	
	while(GetSaveDataC(sChResultData, sAuxData, sCanData))	//저장할 Data가 있을 경우 Stack에서 1개 Pop 한다.
	{
		//sChResultData.lSaveSequence => 1 Base No
		if((sChResultData.lSaveSequence-1)  > nRsCount)		//Data loss 발생
		{
			//중간부 Data 손실 여부만 확인 가능하다. 즉 손실 이후 최소 한번이상의 결과 Data가 갱신되지 않으면 
			//자동 검출 불가하다. 
			m_bDataLoss = TRUE;					 
//			TRACE("*****************>>>>>>>>>>>>>> Data loss detected(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
			nRtn = 0;
		}
		else if((sChResultData.lSaveSequence-1)  < nRsCount)	//Index가 역전된 경우 
		{
			//작업중에 복구 명령을 수행하면 Download와 복구 중에 발생한 Buffering data가 중복되게 된다.
			//이럴경우 현재 송신된 data(버퍼에 있던 data)는 이미 모듈에서 download하여 복구한 data이므로 
			//저장하지 않고 Skip한다.
//			nPreviousStepNo = sChResultData.chStepNo;
//			nPrevTotalCycleNo = sChResultData.nTotalCycleNum;
//			TRACE("*****************>>>>>>>>>>>>>> Data index ???(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
			continue;
		}
		else												//정상적인 data
		{
//			TRACE("*****************>>>>>>>>>>>>>> Data index %d received\n", sChResultData.lSaveSequence);
			m_bDataLoss = FALSE;
		}

		//Data 저장시 모든 단위를 m단위로 저장한다. (실처리 항목은 최소로 한다.) 파일 Size 문제...
		for(int dataIndex = 0; dataIndex <nColumnSize ; dataIndex++)
		{
			pfBuff[dataIndex + nStackCount * nColumnSize] = GetItemData(sChResultData, rawHeader.rsHeader.awColumnItem[dataIndex]);	//Index 5 is WH	
		}		
	
	//	int nAuxIndex = 0;
		for(int nAuxDataIndex = 0; nAuxDataIndex < nAuxColumnSize ; nAuxDataIndex++)
		{
		//	pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = GetAuxItemData(sAuxData, auxHeader.auxHeader.awColumnItem[nAuxDataIndex], nAuxIndex);					
		//	if(auxHeader.auxHeader.awColumnItem[nAuxDataIndex] == PS_AUX_VALUE)
		//		nAuxIndex++;
			pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = sAuxData.auxData[nAuxDataIndex].fValue;
		}	

	//	int nCanIndex = 0;
		for(int nCanDataIndex = 0; nCanDataIndex < nCanColumnSize ; nCanDataIndex++)
		{
			//plCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] = GetCanItemData(sCanData, canHeader.canHeader.awColumnItem[nCanDataIndex], nCanIndex);								
			//if(canHeader.canHeader.awColumnItem[nCanDataIndex] == PS_CAN_VALUE)
			//	nCanIndex++;
			memcpy(&pCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] , &sCanData.canData[nCanDataIndex].canVal, sizeof(SFT_CAN_VALUE));

			
		}	
//		TRACE("StackCount : %d\r\n", nStackCount);
//		TRACE("CAN Data : %f, %f, %f\r\n", pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal);
	
//		TRACE("V:%d/I:%d/C:%d\n", sChResultData.lVoltage,sChResultData.lCurrent,sChResultData.lCapacity );

		//////////////////////////////////////////////////////////////////////////
		//2006/8/4/
		//result file save type mode.

		//2. Binary Mode file, TextMode temp. file
		lStartIndex = WriteResultListFileA(lStartIndex, nRsCount, sChResultData, sAuxData, sCanData);
		if(lStartIndex < 0)
		{
			nRtn = -5;
		}


		//////////////////////////////////////////////////////////////////////////
		nRsCount++;
		nStackCount++;

		//Disk write 성능 향상을 위해 한꺼번에 기록 하도록 수정 
		if(nStackCount >= _MAX_DISK_WIRTE_COUNT)
		{
			fRS.Write(pfBuff, rsSize);

			if(nAuxColumnSize > 0)
			{
				auxFile.Write(pfAuxBuff, auxSize);
				
				//	strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
				//		pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
				//		pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
				
			//	TRACE(strMsg);
			}
			if(nCanColumnSize > 0)
			{
				canFile.Write(pCanBuff, canSize);
				CString strMsg;
				strMsg.Format("ChNo %d : Stack >= MAX 파일에 기록(Aux 수 : %d)(CAN 수 : %d)",
					m_nChannelIndex+1,nAuxColumnSize,nCanColumnSize
					) ;
				WriteLog(strMsg);
			}
			

			nStackCount = 0;
		}
	}

	//Disk access 횟수를 줄이기 위해 한꺼번에 저장 
	if(nStackCount > 0)
	{
		fRS.Write(pfBuff, sizeof(float)*nColumnSize*nStackCount);

		if(nAuxColumnSize > 0)
		{
			auxFile.Write(pfAuxBuff, sizeof(float)*nAuxColumnSize*nStackCount);
			//CString strMsg;
			//strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
			//	pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
			//	pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
			
			//	TRACE(strMsg);
		}
		if(nCanColumnSize > 0)
		{
			canFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*nCanColumnSize*nStackCount);

			CString strMsg;
				strMsg.Format("ChNo %d : Stack >= MAX 파일에 기록(Aux 수 : %d)(CAN 수 : %d)",
					m_nChannelIndex+1,nAuxColumnSize,nCanColumnSize
					) ;
			WriteLog(strMsg);
		}
		
	
	
		//2006/8/18
		//Update Last temp file
		UpdateLastData(UINT(lStartIndex), nRsCount-1, strStartTime, strTestSerial, strUser, strDescript, sChResultData);
	}

#ifdef _DEBUG
	if(m_nChannelIndex == 0)
		TRACE("Channel %d data save time : %dms elapsed\n",  m_nChannelIndex+1, GetTickCount()-dwStart);
#endif

//	if(sChResultData.chNo == 1)
//		TRACE("===========================>CH%03d-S02d: [%dmsec]\n", sChResultData.chNo, sChResultData.chStepNo, GetTickCount()-dwTime);

	//
	if(pfBuff != NULL)
	{	
		delete[]	pfBuff;		
		pfBuff = NULL;
	}
	if(pfAuxBuff != NULL)
	{
		delete [] pfAuxBuff;
		pfAuxBuff = NULL;
	}
	if(pCanBuff != NULL)
	{
		delete [] pCanBuff;
		pCanBuff = NULL;
	}
		
	if (fRS.m_hFile != CFile::hFileNull)
	{
		fRS.Flush();
		fRS.Close();
		auxFile.Flush();
		auxFile.Close();
		canFile.Flush();
		canFile.Close();
	}
	TRACE("Channel %d 결과 Data 저장\n", m_nChannelIndex+1);

	return nRtn;
}

int CCyclerChannel::GetCanFile(CFile *pFile, PS_CAN_FILE_HEADER *canHeader)
{
	CFileStatus	fCanStatus;		
	CFileException e;

	//if(m_nCanCount < 0 || m_nCanCount > _SFT_MAX_MAPPING_CAN)
 	//	return -11;

	CString  m_strCanFileName;

	m_strCanFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_CAN_FILE_NAME_EXT);

	if(CFile::GetStatus(m_strCanFileName, fCanStatus) == FALSE)
	{
 		if(CreateCanFileA(m_strCanFileName, 0, GetCanCount()) ==FALSE)
 		{
 			TRACE("%s를 생성할 수 없습니다.", m_strCanFileName);
 			return -12;
 		}
	}

	//파일을 Open 한다.
	if (pFile->m_hFile == CFile::hFileNull)
	{
 		if( !pFile->Open( m_strCanFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
 		{
 		#ifdef _DEBUG
 			afxDump << "File could not be opened " << e.m_cause << "\n";
 		#endif
 			return -13;
 	}
}
 	
 	//skip file header
 	
 	//read record column set
 	if( pFile->Read(canHeader, sizeof(PS_CAN_FILE_HEADER)) != sizeof(PS_CAN_FILE_HEADER))
 	{
 		pFile->Close();
 		return -14;
 	}
 	pFile->SeekToEnd();
 	
 	return 0;
}

long CCyclerChannel::GetCanItemData(PS_CAN_RAW_DATA canData, WORD wItem, int nCanIndex)
{

	long lData = 0;
	switch(wItem)
	{			
	case PS_CAN_OWNER_CH_NO:	lData = float(canData.canData[nCanIndex].chNo); break;
	case PS_CAN_CHANNEL_NO:	lData = float(nCanIndex); break;
	case PS_CAN_TYPE:	lData = float(canData.canData[nCanIndex].canType); break;
//	case PS_CAN_VALUE:	lData = float(canData.canData[nCanIndex].canVal); break;
	}

	return lData;
}

BOOL CCyclerChannel::CreateCanFileA(CString strFileName, int nFileIndex, int nCanCount)
{
	CFile f;
	CFileException e;
	int nCount = 0;

	if(strFileName.IsEmpty())	return FALSE;
	
	if( !f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		return FALSE;
	}
	
	LPPS_FILE_ID_HEADER pFileHeader = new PS_FILE_ID_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	pFileHeader->nFileID = PS_PNE_CAN_FILE_ID;
	pFileHeader->nFileVersion = _PNE_CAN_FILE_VER;
	CTime t = CTime::GetCurrentTime();
	sprintf(pFileHeader->szCreateDateTime, "%s", t.Format("%y/%m/%d %H:%M:%S"));
	sprintf(pFileHeader->szDescrition, "%s", "PNE aux data file.");
	
	f.Write(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	delete pFileHeader;
	
	
	//2. Can File Header 생성
	PS_CAN_DATA_FILE_HEADER fileCanSetHeader;
	ZeroMemory(&fileCanSetHeader, sizeof(PS_CAN_DATA_FILE_HEADER));

	nCount = nCanCount;


	fileCanSetHeader.nColumnCount = nCount;
	TRACE("Can ColumnCount : %d\r\n", fileCanSetHeader.nColumnCount);
	
	for(int i = 0; i < nCount; i++)
	{
		fileCanSetHeader.awColumnItem[i] = PS_CAN_VALUE;			//CanValue1
	}
	
	
	f.Write(&fileCanSetHeader, sizeof(PS_CAN_DATA_FILE_HEADER));
	
	f.Flush();
	f.Close();
	
	m_strDataList.RemoveAll();
	
	WriteLog(strFileName+" 생성");
	
	return TRUE;
	return TRUE;
}

void CCyclerChannel::SetCanCount(int nCount)
{
	this->m_nCanCount = nCount;
}

long CCyclerChannel::WriteResultListFileA(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData)
{
	DWORD dwStartTime = GetTickCount();

	if(m_strFilePath.IsEmpty())
	{
		TRACE("File path is empty");
		return -1;
	}

	CFileStatus fs;
	CFileException e;
	COleDateTime t = COleDateTime::GetCurrentTime();
	long nStartIndex=0, nEndIndex=0;
	CString strTemp;
	CFile rltData;

	CString strStartTime, strEndTime, strTestSerial, strUser, strDescript, strLineNo;
	strStartTime = m_startTime.Format();
	strTestSerial = m_strTestSerial;
	strUser = m_strUserID;
	strDescript = m_strDescript;

	//결과 파일명
	m_strResultStepFileName.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_RESULT_FILE_NAME_EXT);		//.
	nStartIndex = lStartNo;
	//////////////////////////////////////////////////////////////////////////

	//3. Step end이면 결과 파일에 저장한다. 
	//////////////////////////////////////////////////////////////////////////
	nEndIndex = nLineNo;
	if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		//3.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		 
		{
			PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = PS_FILE_VERSION;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
			sprintf(pFileHeader->testHeader.szStartTime, "%s", strStartTime);
			sprintf(pFileHeader->testHeader.szEndTime, "%s", t.Format());
			sprintf(pFileHeader->testHeader.szSerial, "%s", strTestSerial);
			sprintf(pFileHeader->testHeader.szUserID, "%s", strUser);
			sprintf(pFileHeader->testHeader.szDescript, "%s", strDescript);
			pFileHeader->testHeader.nRecordSize = 12;
			pFileHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[1] = PS_CURRENT;
			pFileHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
			pFileHeader->testHeader.wRecordItem[3] = PS_WATT;
			pFileHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
			pFileHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
			pFileHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
			pFileHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
			pFileHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
			pFileHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
			pFileHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
			
			if(rltData.Open(m_strResultStepFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				rltData.Write(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;

			nStartIndex = 0; 
		}//End if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		
		//3.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!rltData.Open(m_strResultStepFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = rltData.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			{
				//End Time 갱신
				PS_TEST_FILE_HEADER *pFileHeader = new PS_TEST_FILE_HEADER;
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Read(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				sprintf(pFileHeader->szEndTime, "%s", t.Format());
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Write(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				delete pFileHeader;

				//가장 마지막 data read
				LPPS_STEP_END_RECORD pDataRecord = new PS_STEP_END_RECORD;
				rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
				rltData.Read(pDataRecord, sizeof(PS_STEP_END_RECORD));

				//같은 종류의 step이 이미 저장되어 있을 경우 
				//마지막을 현재 data로 하여 갱신한다.
				//작업 진행중에 data 복구를 실시할 경우 발생할 수 있다.
				if(pDataRecord->chStepNo == sChData.chStepNo && pDataRecord->nTotalCycleNum == sChData.nTotalCycleNum)
				{
					rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
					nStartIndex = pDataRecord->nIndexFrom;
				}
				else
				{
					rltData.Seek(0, CFile::end);
					//////////////////////////////////////////////////////////////////////////
					//debugging check
					//손실 data 복구시 모듈에서 아직 전송되지 않은 더 많은 data를 이미 복구해 버리면 발생할 수 있다.
					//
					if(nStartIndex != pDataRecord->nIndexTo+1)
					{
						strTemp.Format("Ch %d End file index error Step %d, Total Cycle %d/%d. (Prev range : %d~%d, Next start : %d), Size %d", 
										m_nChannelIndex+1, pDataRecord->chStepNo, sChData.nTotalCycleNum, pDataRecord->nTotalCycleNum,
										pDataRecord->nIndexFrom, pDataRecord->nIndexTo, nStartIndex, fsize
							);
						WriteLog(strTemp);
#ifdef _DEBUG
						AfxMessageBox(strTemp);
#endif
					}
					//////////////////////////////////////////////////////////////////////////

					//시작 Index는 이전 Step완료의 마지막 Index에서 시작 
					nStartIndex = pDataRecord->nIndexTo+1;
				}
				delete pDataRecord;
			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);
				nStartIndex = 0;
				if(rltData.m_hFile != NULL)	rltData.Close();
				return -1;
			}
		}	//End else

		sChData.nIndexFrom = nStartIndex;
		sChData.nIndexTo = nEndIndex;
	
		//결과 파일을 갱신한다.
		if(rltData.m_hFile != NULL)
		{
			long fsize = 0;

			rltData.Write(&sChData, sizeof(PS_STEP_END_RECORD));
			rltData.Flush();
			fsize = rltData.GetLength();
			rltData.Close();

			strTemp.Format("Step %d [%s]완료(%d~%d). %dmsec/Size: %d",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sChData.nIndexFrom, sChData.nIndexTo, GetTickCount()-dwStartTime, fsize);
			WriteLog(strTemp);
		}
		
		//용량 합산 표시를 위한 total용량 갱신 
		AddTotalCapacity(sChData.chStepType, (float)ConvertPCUnitToSbc(sChData.fCapacity, PS_CAPACITY));

		//다음 Step의 시작 Index 생성 
		nStartIndex = nEndIndex+1;

		//Send result to database
		//20070305 BY KBH
		//Send message to database client 
		//////////////////////////////////////////////////////////////////////////
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DataBaseUpdate", FALSE))
		{
			CString strTemp;
			TCHAR szCurDir[MAX_PATH];
			::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
			strTemp.Format("%s\\DataProServer.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));

			//Message를 전송한다.
			HWND hWnd = ::FindWindow(NULL, "CTSDataPro Server");
			if(hWnd)
			{
				char szData[MAX_PATH];
				ZeroMemory(szData, MAX_PATH);
				sprintf(szData, "%s", m_strFilePath);
				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = sChData.chStepNo;		
				CpStructData.cbData = m_strFilePath.GetLength();
				CpStructData.lpData = szData;
				::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				
//				sprintf(szData, "Send M%d C%d step %d data to data base server", m_nModuleID, m_nChannelIndex+1, sChData.chStepNo);
//				::SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szData);
			}
			//////////////////////////////////////////////////////////////////////////
		}
	} //if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	//4 Aux Result File를 생성한다.
	if(sAuxData.auxCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		CFile auxFile;
		CString strResultAuxFile;
		strResultAuxFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_AUX_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)		 
		{
			PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = _PNE_AUX_FILE_VER;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			
		
			pFileHeader->auxHeader.nColumnCount = sAuxData.auxCount;
	
			for(int i = 0; i < sAuxData.auxCount; i++)
			{
				pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
			}
			
			if(auxFile.Open(strResultAuxFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Aux Result File 생성\r\n");

		}//End if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!auxFile.Open(strResultAuxFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = auxFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_AUX_FILE_HEADER))
			{
				//End Time 갱신
				PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
				auxFile.Seek(sizeof(PS_AUX_FILE_HEADER), CFile::begin);
				auxFile.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				float * pfBuff = new float[sAuxData.auxCount];
				auxFile.Seek(fsize - sizeof(float)*sAuxData.auxCount, CFile::begin);
				auxFile.Read(pfBuff, sizeof(float)*sAuxData.auxCount);
				delete [] pfBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(auxFile.m_hFile != NULL)	auxFile.Close();
				return -1;
			}
			TRACE("기존 Aux Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(auxFile.m_hFile != NULL)
		{
			long fsize = 0;

			float * pfBuff = new float[sAuxData.auxCount];
			for(int i = 0 ; i < sAuxData.auxCount; i++)
				pfBuff[i] = sAuxData.auxData[i].fValue;
			auxFile.Write(pfBuff, sizeof(float)*sAuxData.auxCount);
			auxFile.Flush();
			auxFile.Close();
			

			strTemp.Format("Step %d [%s] Aux Count : %d 완료",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sAuxData.auxCount);
			WriteLog(strTemp);
			TRACE(strTemp+"\r\n");
			TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
								pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);
			delete [] pfBuff;
		}
	
	}
	//5. Can Result File를 생성한다.
	if(sCanData.canCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		CFile CanFile;
		CString strResultCanFile;
		strResultCanFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_CAN_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultCanFile, fs) == FALSE)		 
		{
			PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_CAN_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_CAN_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = 0x1000;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Can end data file.");			
		
			pFileHeader->canHeader.nColumnCount = sCanData.canCount;
	
			for(int i = 0; i < sCanData.canCount; i++)
			{
				pFileHeader->canHeader.awColumnItem[i] = PS_CAN_VALUE;			//CanValue1
			}
			
			if(CanFile.Open(strResultCanFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				CanFile.Write(pFileHeader, sizeof(PS_CAN_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Can Result File 생성\r\n");

		}//End if( CFile::GetStatus(strResultCanFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!CanFile.Open(strResultCanFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = CanFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_CAN_FILE_HEADER))
			{
				//End Time 갱신
				PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
				CanFile.Seek(sizeof(PS_CAN_FILE_HEADER), CFile::begin);
				CanFile.Read(pFileHeader, sizeof(PS_CAN_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				long * plBuff = new long[sCanData.canCount];
				CanFile.Seek(fsize - sizeof(long)*sCanData.canCount, CFile::begin);
				CanFile.Read(plBuff, sizeof(long)*sCanData.canCount);
				delete [] plBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(CanFile.m_hFile != NULL)	CanFile.Close();
				return -1;
			}
			TRACE("기존 Can Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(CanFile.m_hFile != NULL)
		{
			long fsize = 0;

			SFT_CAN_VALUE * pCanBuff = new SFT_CAN_VALUE[sCanData.canCount];
			for(int i = 0 ; i < sCanData.canCount; i++)
				memcpy(&pCanBuff[i], &sCanData.canData[i].canVal, sizeof(SFT_CAN_VALUE));
			CanFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*sCanData.canCount);
			CanFile.Flush();
			CanFile.Close();
			

			strTemp.Format("Step %d [%s] Can Count : %d 완료",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sCanData.canCount);
			WriteLog(strTemp);
			TRACE(strTemp+"\r\n");
			TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
								pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal, pCanBuff[3].fVal, pCanBuff[4].fVal,
								pCanBuff[5].fVal, pCanBuff[6].fVal, pCanBuff[7].fVal);
			delete [] pCanBuff;
		}
	
	}

	//////////////////////////////////////////////////////////////////////////
	//else	//Delta t, Delta V, Delta I, Delta T etc... 일 경우는 Start Index는 변화 없습

	return nStartIndex;
}

long CCyclerChannel::WriteResultListFileB(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData)
{
	DWORD dwStartTime = GetTickCount();

	if(m_strFilePath.IsEmpty())
	{
		TRACE("File path is empty");
		return -1;
	}

	CFileStatus fs;
	CFileException e;
	COleDateTime t = COleDateTime::GetCurrentTime();
	long nStartIndex=0, nEndIndex=0;
	CString strTemp;
	CFile rltData;

	CString strStartTime, strEndTime, strTestSerial, strUser, strDescript, strLineNo;
	strStartTime = m_startTime.Format();
	strTestSerial = m_strTestSerial;
	strUser = m_strUserID;
	strDescript = m_strDescript;

	//결과 파일명
	m_strResultStepFileName.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_RESULT_FILE_NAME_EXT);		//.
	nStartIndex = lStartNo;
	//////////////////////////////////////////////////////////////////////////

	//3. Step end이면 결과 파일에 저장한다. 
	//////////////////////////////////////////////////////////////////////////
	nEndIndex = nLineNo;
	if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		//3.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		 
		{
			PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = PS_FILE_VERSION;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
			sprintf(pFileHeader->testHeader.szStartTime, "%s", strStartTime);
			sprintf(pFileHeader->testHeader.szEndTime, "%s", t.Format());
			sprintf(pFileHeader->testHeader.szSerial, "%s", strTestSerial);
			sprintf(pFileHeader->testHeader.szUserID, "%s", strUser);
			sprintf(pFileHeader->testHeader.szDescript, "%s", strDescript);
			pFileHeader->testHeader.nRecordSize = 21;
			pFileHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[1] = PS_CURRENT;
			pFileHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
			pFileHeader->testHeader.wRecordItem[3] = PS_WATT;
			pFileHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
			pFileHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
			pFileHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
			pFileHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
			pFileHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
			pFileHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
			pFileHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
			pFileHeader->testHeader.wRecordItem[12] = PS_CHARGE_CAP;
			pFileHeader->testHeader.wRecordItem[13] = PS_DISCHARGE_CAP;
			pFileHeader->testHeader.wRecordItem[14] = PS_CAPACITANCE;
			pFileHeader->testHeader.wRecordItem[15] = PS_CHARGE_WH;
			pFileHeader->testHeader.wRecordItem[16] = PS_DISCHARGE_WH;
			pFileHeader->testHeader.wRecordItem[17] = PS_CV_TIME;
			pFileHeader->testHeader.wRecordItem[18] = PS_SYNC_DATE;
			pFileHeader->testHeader.wRecordItem[19] = PS_SYNC_TIME;
			pFileHeader->testHeader.wRecordItem[20] = PS_ACC_CYCLE1;
			pFileHeader->testHeader.wRecordItem[21] = PS_ACC_CYCLE2;
			pFileHeader->testHeader.wRecordItem[22] = PS_ACC_CYCLE3;
			pFileHeader->testHeader.wRecordItem[23] = PS_ACC_CYCLE4;
			pFileHeader->testHeader.wRecordItem[24] = PS_ACC_CYCLE5;
			pFileHeader->testHeader.wRecordItem[25] = PS_MULTI_CYCLE1;
			pFileHeader->testHeader.wRecordItem[26] = PS_MULTI_CYCLE2;
			pFileHeader->testHeader.wRecordItem[27] = PS_MULTI_CYCLE3;
			pFileHeader->testHeader.wRecordItem[28] = PS_MULTI_CYCLE4;
			pFileHeader->testHeader.wRecordItem[29] = PS_MULTI_CYCLE5;
			
			
			if(rltData.Open(m_strResultStepFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				rltData.Write(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;

			nStartIndex = 0; 
		}//End if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		
		//3.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!rltData.Open(m_strResultStepFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = rltData.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			{
				//End Time 갱신
				PS_TEST_FILE_HEADER *pFileHeader = new PS_TEST_FILE_HEADER;
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Read(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				sprintf(pFileHeader->szEndTime, "%s", t.Format());
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Write(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				delete pFileHeader;

				//가장 마지막 data read
				LPPS_STEP_END_RECORD pDataRecord = new PS_STEP_END_RECORD;
				rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
				rltData.Read(pDataRecord, sizeof(PS_STEP_END_RECORD));

				//같은 종류의 step이 이미 저장되어 있을 경우 
				//마지막을 현재 data로 하여 갱신한다.
				//작업 진행중에 data 복구를 실시할 경우 발생할 수 있다.
				if(pDataRecord->chStepNo == sChData.chStepNo && pDataRecord->nTotalCycleNum == sChData.nTotalCycleNum)
				{
					rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
					nStartIndex = pDataRecord->nIndexFrom;
				}
				else
				{
					rltData.Seek(0, CFile::end);
					//////////////////////////////////////////////////////////////////////////
					//debugging check
					//손실 data 복구시 모듈에서 아직 전송되지 않은 더 많은 data를 이미 복구해 버리면 발생할 수 있다.
					//
					if(nStartIndex != pDataRecord->nIndexTo+1)
					{
						strTemp.Format("Ch %d End file index error Step %d, Total Cycle %d/%d. (Prev range : %d~%d, Next start : %d), Size %d", 
										m_nChannelIndex+1, pDataRecord->chStepNo, sChData.nTotalCycleNum, pDataRecord->nTotalCycleNum,
										pDataRecord->nIndexFrom, pDataRecord->nIndexTo, nStartIndex, fsize
							);
						WriteLog(strTemp);
#ifdef _DEBUG
						AfxMessageBox(strTemp);
#endif
					}
					//////////////////////////////////////////////////////////////////////////

					//시작 Index는 이전 Step완료의 마지막 Index에서 시작 
					nStartIndex = pDataRecord->nIndexTo+1;
				}
				delete pDataRecord;
			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);
				nStartIndex = 0;
				if(rltData.m_hFile != NULL)	rltData.Close();
				return -1;
			}
		}	//End else

		sChData.nIndexFrom = nStartIndex;
		sChData.nIndexTo = nEndIndex;
	
		//결과 파일을 갱신한다.
		if(rltData.m_hFile != NULL)
		{
			long fsize = 0;

			rltData.Write(&sChData, sizeof(PS_STEP_END_RECORD));
			rltData.Flush();
			fsize = rltData.GetLength();
			rltData.Close();

			strTemp.Format("Step %d [%s]완료(%d~%d). %dmsec/Size: %d",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sChData.nIndexFrom, sChData.nIndexTo, GetTickCount()-dwStartTime, fsize);
			WriteLog(strTemp);
			//진행 상태 전달		
// 			HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");   //add ljb 2008-10-10 
// 			if(hWnd)
// 			{
// 				char szData[MAX_PATH];
// 				ZeroMemory(szData, MAX_PATH);
// 				sprintf(szData, "%s", strTemp);
// 				COPYDATASTRUCT CpStructData;
// 				CpStructData.dwData = WM_MY_MESSAGE_CTS_STEP_END_DATA;		
// 				CpStructData.cbData = strTemp.GetLength();
// 				CpStructData.lpData = szData;
// 				::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
// 			}
		}
		
		//용량 합산 표시를 위한 total용량 갱신 
		AddTotalCapacity(sChData.chStepType, (float)ConvertPCUnitToSbc(sChData.fCapacity, PS_CAPACITY));

		//다음 Step의 시작 Index 생성 
		nStartIndex = nEndIndex+1;

		//Send result to database
		//20070305 BY KBH
		//Send message to database client 
		//////////////////////////////////////////////////////////////////////////
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DataBaseUpdate", FALSE))
		{
			CString strTemp;
			TCHAR szCurDir[MAX_PATH];
			::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
			strTemp.Format("%s\\DataProServer.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));

			//Message를 전송한다.
			HWND hWnd = ::FindWindow(NULL, "CTSDataPro Server");
			if(hWnd)
			{
				char szData[MAX_PATH];
				ZeroMemory(szData, MAX_PATH);
				sprintf(szData, "%s", m_strFilePath);
				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = sChData.chStepNo;		
				CpStructData.cbData = m_strFilePath.GetLength();
				CpStructData.lpData = szData;
				::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				
//				sprintf(szData, "Send M%d C%d step %d data to data base server", m_nModuleID, m_nChannelIndex+1, sChData.chStepNo);
//				::SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szData);
			}
			//////////////////////////////////////////////////////////////////////////
		}
	} //if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	//4 Aux Result File를 생성한다.
	if(sAuxData.auxCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		CFile auxFile;
		CString strResultAuxFile;
		strResultAuxFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_AUX_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)		 
		{
			PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = _PNE_AUX_FILE_VER;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			
		
			pFileHeader->auxHeader.nColumnCount = sAuxData.auxCount;
	
			for(int i = 0; i < sAuxData.auxCount; i++)
			{
				pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
			}
			
			if(auxFile.Open(strResultAuxFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Aux Result File 생성\r\n");

		}//End if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!auxFile.Open(strResultAuxFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = auxFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_AUX_FILE_HEADER))
			{
				//End Time 갱신
				PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
				auxFile.Seek(sizeof(PS_AUX_FILE_HEADER), CFile::begin);
				auxFile.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				float * pfBuff = new float[sAuxData.auxCount];
				auxFile.Seek(fsize - sizeof(float)*sAuxData.auxCount, CFile::begin);
				auxFile.Read(pfBuff, sizeof(float)*sAuxData.auxCount);
				delete [] pfBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(auxFile.m_hFile != NULL)	auxFile.Close();
				return -1;
			}
			TRACE("기존 Aux Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(auxFile.m_hFile != NULL)
		{
			long fsize = 0;

			float * pfBuff = new float[sAuxData.auxCount];
			for(int i = 0 ; i < sAuxData.auxCount; i++)
				pfBuff[i] = sAuxData.auxData[i].fValue;
			auxFile.Write(pfBuff, sizeof(float)*sAuxData.auxCount);
			auxFile.Flush();
			auxFile.Close();
			

			strTemp.Format("Step %d [%s] Aux Count : %d 완료",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sAuxData.auxCount);
			WriteLog(strTemp);
			TRACE(strTemp+"\r\n");
			TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
								pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);
			delete [] pfBuff;
		}
	
	}
	//5. Can Result File를 생성한다.
	if(sCanData.canCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		CFile CanFile;
		CString strResultCanFile;
		strResultCanFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_CAN_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultCanFile, fs) == FALSE)		 
		{
			PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_CAN_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_CAN_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = 0x1000;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Can end data file.");			
		
			pFileHeader->canHeader.nColumnCount = sCanData.canCount;
	
			for(int i = 0; i < sCanData.canCount; i++)
			{
				pFileHeader->canHeader.awColumnItem[i] = PS_CAN_VALUE;			//CanValue1
			}
			
			if(CanFile.Open(strResultCanFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				CanFile.Write(pFileHeader, sizeof(PS_CAN_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Can Result File 생성\r\n");

		}//End if( CFile::GetStatus(strResultCanFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!CanFile.Open(strResultCanFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = CanFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_CAN_FILE_HEADER))
			{
				//End Time 갱신
				PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
				CanFile.Seek(sizeof(PS_CAN_FILE_HEADER), CFile::begin);
				CanFile.Read(pFileHeader, sizeof(PS_CAN_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				long * plBuff = new long[sCanData.canCount];
				CanFile.Seek(fsize - sizeof(long)*sCanData.canCount, CFile::begin);
				CanFile.Read(plBuff, sizeof(long)*sCanData.canCount);
				delete [] plBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(CanFile.m_hFile != NULL)	CanFile.Close();
				return -1;
			}
			TRACE("기존 Can Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(CanFile.m_hFile != NULL)
		{
			long fsize = 0;

			SFT_CAN_VALUE * pCanBuff = new SFT_CAN_VALUE[sCanData.canCount];
			for(int i = 0 ; i < sCanData.canCount; i++)
				memcpy(&pCanBuff[i], &sCanData.canData[i].canVal, sizeof(SFT_CAN_VALUE));
			CanFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*sCanData.canCount);
			CanFile.Flush();
			CanFile.Close();
			

			strTemp.Format("Step %d [%s] Can Count : %d 완료",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sCanData.canCount);
			WriteLog(strTemp);
			TRACE(strTemp+"\r\n");
			TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
								pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal, pCanBuff[3].fVal, pCanBuff[4].fVal,
								pCanBuff[5].fVal, pCanBuff[6].fVal, pCanBuff[7].fVal);
			delete [] pCanBuff;
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	//else	//Delta t, Delta V, Delta I, Delta T etc... 일 경우는 Start Index는 변화 없습


	return nStartIndex;
}

CString CCyclerChannel::GetEndTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	

	CTimeSpan tempTime((ULONG)PCTIME(::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_TIME)));
	
//	int nDay = m_startTime.GetDay() + tempTime.GetDays();
//	int nHour = m_startTime.GetHour() + tempTime.GetHour();
//	int nMin = m_startTime.GetMinute() + tempTime.GetMinutes();
//	int nSec = m_startTime.GetSecond() + tempTime.GetSeconds();

	COleDateTime sumTime;
	sumTime.SetTime(m_startTime.GetHour() + tempTime.GetHours(), m_startTime.GetMinute() + tempTime.GetMinutes(), 
		m_startTime.GetSecond() + tempTime.GetSeconds());

	CString strTime = sumTime.Format();
	//strTime.Format()
/*	if(timeSpan.GetDays() > 0)
	{
		strMsg =  timeSpan.Format("%Dd %H:%M:%S");
	}
	else
	{
		strMsg = timeSpan.Format("%H:%M:%S");
	}
	}*/
	return strTime;
	

}

void CCyclerChannel::SetCanTitleFile(CString strCanPath, CString strTestName)
{	
// 	char path[MAX_PATH];
//     GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
// 	CString strTemp(path);
	CString strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path"); //ksj 20210507
	CString strPath;
	strTemp += "\\";
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strCanPath.Mid(strCanPath.ReverseFind('\\')+1, 7), PS_CAN_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("%sCAN.%s", strTestName, "tlt"); 
	CString strNew = strCanPath+"\\"+strTemp;
	
		
   	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return;

	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	if(CopyFile(strPath, strNew, TRUE) == FALSE)
	{
		AfxMessageBox(strTemp+" Data 복사에 실패했습니다.");
		return;
	}
	WriteLog("CAN Title 파일 복사 완료");
}

int CCyclerChannel::GetTotalAuxCount()
{
	return m_nTempAuxCount + m_nVoltAuxCount;
}
/*
BOOL CCyclerChannel::GetFileRawData(PS_RAW_FILE_HEADER &FileHeader, FILE * fpDestFile, FILE * fpSourceFile,
									  CString destFileName, CString srcFileName)
{
	BOOL bLockFile = TRUE;
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		return FALSE; 
	}

	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)
		{
			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			fclose(fpDestFile);
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		fclose(fpDestFile);
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			return FALSE;	
		}
	}
	return TRUE;
}

BOOL CCyclerChannel::GetAuxFileRawData(PS_AUX_FILE_HEADER &FileHeader, FILE *fpDestFile, FILE *fpSourceFile,
									   CString destFileName, CString srcFileName)
{
	BOOL bLockFile = TRUE;
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		return FALSE; 
	}

	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateAuxFileA(srcFileName, 0, m_chFileDataSet.GetAuxColumnCount()) == FALSE)
		{
			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			fclose(fpDestFile);
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		fclose(fpDestFile);
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	if(fread(&FileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpDestFile) < 1)
		{
			m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			return FALSE;	
		}
	}
	return TRUE;
}
*/

int CCyclerChannel::WriteRestoreRawFile(CString strLocalFileName, FILE *fpDestFile, PS_RAW_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo)
{
	int nRestordDataSize = 0;
	int lReadData[PS_MAX_ITEM_NUM] = { 0 };
	int nAuxCount = 0, nCanCount = 0;

	float* pfBuff = new float[FileHeader.rsHeader.nColumnCount];

	FILE *fpBackUp = fopen(strLocalFileName,"rt");
	if(fpBackUp)
	{
		int nData1 = 0, nData2 = 0;
		BOOL bFind = FALSE;
		char temp[23][20];
		for(int i = 0 ; i < 23; i++)
		{
			fscanf(fpBackUp, "%s", temp[i]);
		}
		
		while(1)
		{

			//모듈에서 가져온 복구파일을 읽어 들인다.
			//////////////////////////////////////////////////////////////////////////
			//resultIndex, state, type, mode, select, 
			//code, grade, stepNo, Vsens, Isens, 
			//capacity, watt, wattHour, runTime,totalRunTime, 
			//z, temp, press, totalCycle, currentCycle, 
			//avgV, avgI
			if(fscanf(fpBackUp, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", 
								&lReadData[PS_DATA_SEQ], &lReadData[PS_TOT_TIME], &lReadData[PS_STEP_TIME],
								&lReadData[PS_STATE], &lReadData[PS_STEP_TYPE], &nData1, &nData2,&lReadData[PS_CODE], 
								&lReadData[PS_GRADE_CODE], &lReadData[PS_STEP_NO], &lReadData[PS_VOLTAGE], 
								&lReadData[PS_CURRENT],	&lReadData[PS_CAPACITY], &lReadData[PS_WATT], &lReadData[PS_WATT_HOUR],
								&lReadData[PS_IMPEDANCE], &lReadData[PS_OVEN_TEMPERATURE], &nAuxCount, &nCanCount,  &lReadData[PS_TOT_CYCLE],
								&lReadData[PS_CUR_CYCLE], &lReadData[PS_AVG_VOLTAGE], &lReadData[PS_AVG_CURRENT] ) > 0 ) 
			{
				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
				if(lReadData[PS_DATA_SEQ] >= nStartNo && lReadData[PS_DATA_SEQ] <= nEndNo)
				{
					bFind = TRUE;
					//복구 실시 
					for(int a =0; a<FileHeader.rsHeader.nColumnCount; a++)
					{
						pfBuff[a] = (float)ConvertSbcToPCUnit(lReadData[FileHeader.rsHeader.awColumnItem[a]], FileHeader.rsHeader.awColumnItem[a]);
					}
					
					if(fwrite(pfBuff, sizeof(float), FileHeader.rsHeader.nColumnCount, fpDestFile) < 1)
					{
						//TRACE("Backup File write Error %s\n", destFileName);
						//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
						//break;
					}
					nRestordDataSize++;
//								TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
				}
			}
			else
			{
				//bRead = FALSE;
				break;
			}
										
		}	//end While

		//SBC Index 파일에는 있는데 실제 파일에 존재 하지 않으면(SBC 저장 오류) 복구수가 없을 수 있다.
		if(nRestordDataSize < 1)
		{
			nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
		}
		fclose(fpBackUp);
		delete [] pfBuff;
	}
	else 
	{	//SBC에서 download한 backup 파일 open 실패 
		nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
	}
	return nRestordDataSize;
}

int CCyclerChannel::WriteRestoreAuxFile(CString strTFileName, CString strVFileName, FILE *fpDestFile, PS_AUX_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo)
{
	int nRestordDataSize = 0;
	int nAuxCount = 0;
	int nDataIndex = 0;
	FILE *fpTBackUp = NULL;
	FILE *fpVBackUp = NULL;

	long lBuff[PS_MAX_AUX_SAVE_FILE_COLUMN] = {0,};
	float* pfBuff = new float[FileHeader.auxHeader.nColumnCount];
	
	if(m_chFileDataSet.m_nAuxTempCount > 0)
		fpTBackUp = fopen(strTFileName,"rt");
	if(m_chFileDataSet.m_nAuxVoltCount > 0)
		fpVBackUp = fopen(strVFileName,"rt");

	if(fpTBackUp || fpVBackUp)
	{
		int nIndex = 0, nData1 = 0, nData2 = 0;
		BOOL bFind = FALSE;
		char temp[PS_MAX_AUX_SAVE_FILE_COLUMN][20];
		
		if(fpTBackUp)
		{
			for(int i = 0 ; i < m_chFileDataSet.m_nAuxTempCount+3; i++)
				fscanf(fpTBackUp, "%s", temp[i]);
		}
		
		if(fpVBackUp)
		{
			for(int i = 0 ; i < m_chFileDataSet.m_nAuxVoltCount+3; i++)
				fscanf(fpVBackUp, "%s", temp[i]);
		}
		

		while(1)
		{

			//모듈에서 가져온 복구파일을 읽어 들인다.
			//////////////////////////////////////////////////////////////////////////
			//resultIndex, state, type, mode, select, 
			//code, grade, stepNo, Vsens, Isens, 
			//capacity, watt, wattHour, runTime,totalRunTime, 
			//z, temp, press, totalCycle, currentCycle, 
			//avgV, avgI
			if(fpTBackUp)
			{
				fscanf(fpTBackUp, "%d, %d, %d,", &nIndex, &nData1, &nData2);
				for(nDataIndex = 0 ; nDataIndex < m_chFileDataSet.m_nAuxTempCount; nDataIndex++)
				{
					fscanf(fpTBackUp, "%d,", &lBuff[nDataIndex]);
					pfBuff[nDataIndex] = float(lBuff[nDataIndex]);
				}
			}

			if(fpVBackUp)
			{
				fscanf(fpVBackUp, "%d, %d, %d,", &nIndex, &nData1, &nData2);
				for(; nDataIndex < m_chFileDataSet.m_nAuxVoltCount+m_chFileDataSet.m_nAuxTempCount; nDataIndex++)
				{
					fscanf(fpVBackUp, "%d,", &lBuff[nDataIndex]);
					pfBuff[nDataIndex] = float(lBuff[nDataIndex]);
				}
			}		
			if(nIndex == 300)
				nIndex = nIndex+0;
		
				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
			if(nIndex >= nStartNo && nIndex <= nEndNo)
			{
				bFind = TRUE;
				//복구 실시 
									
				if(fwrite(pfBuff, sizeof(float), FileHeader.auxHeader.nColumnCount, fpDestFile) < 1)
				{
					//TRACE("Backup File write Error %s\n", destFileName);
					//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
					//break;
				}
				nRestordDataSize++;
				if(nIndex == 1052)
					TRACE("Aux Data Restore : %d\r\n", nIndex);
//								TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
			}
			if(nIndex >= nEndNo)
				break;
										
		}	//end While
		if(fpTBackUp)	fclose(fpTBackUp);
		if(fpVBackUp)	fclose(fpVBackUp);
		if(pfBuff)	delete [] pfBuff;
	}
	return nRestordDataSize;
}

int CCyclerChannel::WriteRestoreCanFile(CString strMFileName, CString strSFileName, FILE *fpDestFile, PS_CAN_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo)
{
	int nRestordDataSize = 0;
	int nDataIndex = 0;
	long	lHigh =0, lLow =0;
	FILE *fpMBackUp = NULL;
	FILE *fpSBackUp = NULL;

	CAN_VALUE lBuff[PS_MAX_CAN_SAVE_FILE_COLUMN] = {0,};
	//CAN_VALUE* pBuff = new CAN_VALUE[FileHeader.canHeader.nColumnCount];
	
	if(m_chFileDataSet.m_nCanMasterCount > 0)
		fpMBackUp = fopen(strMFileName,"rt");
	if(m_chFileDataSet.m_nCanSlaveCount > 0)
		fpSBackUp = fopen(strSFileName,"rt");

	if(fpMBackUp || fpSBackUp)
	{
		int nIndex = 0, nData1 = 0, nData2 = 0, nData3 = 0, nData4 = 0;
		BOOL bFind = FALSE;
		char temp[PS_MAX_CAN_SAVE_FILE_COLUMN][20];
		
		if(fpMBackUp)
		{
			for(int i = 0 ; i < m_chFileDataSet.m_nCanMasterCount+3; i++)
				fscanf(fpMBackUp, "%s", temp[i]);
		}
		
		if(fpSBackUp)
		{
			for(int i = 0 ; i < m_chFileDataSet.m_nCanSlaveCount+3; i++)
				fscanf(fpSBackUp, "%s", temp[i]);
		}
		

		while(1)
		{

			//모듈에서 가져온 복구파일을 읽어 들인다.
			//////////////////////////////////////////////////////////////////////////
			//resultIndex, state, type, mode, select, 
			//code, grade, stepNo, Vsens, Isens, 
			//capacity, watt, wattHour, runTime,totalRunTime, 
			//z, temp, press, totalCycle, currentCycle, 
			//avgV, avgI
			if(fpMBackUp)
			{
				fscanf(fpMBackUp, "%d, %d, %d, %d, %d,", &nIndex, &nData1, &nData2, &nData3, &nData4);	//ljb 20140115 add -> , &nData3, &nData4
				for(nDataIndex = 0 ; nDataIndex < m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
				{
			//		fscanf(fpMBackUp, "%d, %d", &lHigh, &lLow);
			//		lBuff[nDataIndex].lVal[1] = lHigh;
			//		lBuff[nDataIndex].lVal[0] = lLow;
					fscanf(fpMBackUp, "%f,", &lBuff[nDataIndex].fVal[0]);
					
				}
			}

			if(fpSBackUp)
			{
				fscanf(fpSBackUp, "%d, %d, %d, %d, %d,", &nIndex, &nData1, &nData2, &nData3, &nData4);	//ljb 20140115 add -> , &nData3, &nData4
				for(; nDataIndex < m_chFileDataSet.m_nCanSlaveCount+m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
				{
			//		fscanf(fpSBackUp, "%d, %d", &lHigh, &lLow);
			//		lBuff[nDataIndex].lVal[1] = lHigh;
			//		lBuff[nDataIndex].lVal[0] = lLow;
					fscanf(fpSBackUp, "%f,", &lBuff[nDataIndex].fVal[0]);
				}
			}		
		
				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
			if(nIndex >= nStartNo && nIndex <= nEndNo)
			{
				bFind = TRUE;
				//복구 실시 
									
				if(fwrite(lBuff, sizeof(CAN_VALUE), FileHeader.canHeader.nColumnCount, fpDestFile) < 1)
				{
					//TRACE("Backup File write Error %s\n", destFileName);
					//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
					//break;
				}
				nRestordDataSize++;
			
//								TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
			}
			if(nIndex >= nEndNo)
				break;
										
		}	//end While

		if(fpMBackUp)	fclose(fpMBackUp);
		if(fpSBackUp)	fclose(fpSBackUp);
	}
	return nRestordDataSize;
}

BOOL CCyclerChannel::RemakeAuxPCIndexFile(CString strBackUpRawFile, CString strAuxTStepEndFile, CString strAuxVStepEndFile, CString strNewTableFile)
{
	//파일명 설정 여부 확인 
	if(strBackUpRawFile.IsEmpty() || strNewTableFile.IsEmpty())
	{
		return FALSE;
	}

	COleDateTime t = COleDateTime::GetCurrentTime();
	CFile		fRS;
	CFileException e;

	//////////////////////////////////////////////////////////////////////////
	//복구된 Raw Data와 모듈에서 download한 복구용 Table file을 이용하여 PC용 Table 파일을 생성한다.
	//파일을 Open 한다.
	if( !fRS.Open( strBackUpRawFile, CFile::modeRead, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   m_strLastErrorString.Format("%s 파일을 열수 없습니다.", strBackUpRawFile);
	   return FALSE;
	}

	int nColumnSize = 0;
	PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
	if(fRS.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER)) > 0)
	{
		nColumnSize = pFileHeader->auxHeader.nColumnCount;
	}
	
	delete pFileHeader;
	fRS.Close();
	
	//파일이 잘못되었을 경우 
	if(nColumnSize < 1 || nColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
	{
	   m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", strBackUpRawFile, nColumnSize);
		return FALSE;	//실제 Data 분석 
	}
	

	//모듈에서 전송받은 Step End 파일 Open
	char temp[PS_MAX_AUX_SAVE_FILE_COLUMN][20];
	
	FILE *pAuxTFile = NULL;
	FILE *pAuxVFile = NULL;
	if(strAuxTStepEndFile != "")
	{
		pAuxTFile = fopen(strAuxTStepEndFile, "rt");
		if(pAuxTFile == NULL)
		{
			m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			fclose(pAuxTFile);
			return FALSE;	//실제 Data 분석 
		}

		for(int i = 0 ; i < m_chFileDataSet.m_nAuxTempCount +3 ; i++)
		{
			fscanf(pAuxTFile, "%s,", temp[i]);
		}
	}
	if(strAuxVStepEndFile != "")
	{
		pAuxVFile = fopen(strAuxVStepEndFile, "rt");
		if(pAuxVFile == NULL)
		{
			m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			fclose(pAuxTFile);
			fclose(pAuxVFile);
			return FALSE;	//실제 Data 분석 
		}

		for( int i = 0; i < m_chFileDataSet.m_nAuxVoltCount +3; i++)
		{
			fscanf(pAuxVFile, "%s,", temp[i]);
		}
	}
		
	float *pfBuff = new float[nColumnSize];	
	int lReadData[PS_MAX_AUX_SAVE_FILE_COLUMN] = { 0 };
	int nIndex, nData1, nData2;	
	
	CFile auxFile;
	if( !auxFile.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	{
		//생성 실패
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
			m_strLastErrorString.Format("%s를 생성할 수 없습니다.", strNewTableFile);
		
			return FALSE;
	}

	pFileHeader = new PS_AUX_FILE_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
	pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
	pFileHeader->fileHeader.nFileVersion = 0x1000;
	sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
	sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			

	pFileHeader->auxHeader.nColumnCount = nColumnSize;

	for(int i = 0; i < nColumnSize; i++)
	{
		pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
	}

	auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
	
	int nDataIndex = 0 ;
	while (1)
	{
		if(pAuxTFile)
		{
			if(fscanf(pAuxTFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			{
				for(nDataIndex = 0 ; nDataIndex < m_chFileDataSet.m_nAuxTempCount; nDataIndex++)
				{
					fscanf(pAuxTFile, "%d,", &lReadData[nDataIndex]);
					pfBuff[nDataIndex] = lReadData[nDataIndex];
				}
			}
			else
				break;
		}
		if(pAuxVFile)
		{
			if(fscanf(pAuxVFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			{
				for(nDataIndex ; nDataIndex < m_chFileDataSet.m_nAuxVoltCount + m_chFileDataSet.m_nAuxTempCount; nDataIndex++)
				{
					fscanf(pAuxVFile, "%d,", &lReadData[nDataIndex]);
					pfBuff[nDataIndex] = lReadData[nDataIndex];
				}
			}
			else
				break;
		}
		
		auxFile.Write(pfBuff, sizeof(float) * nColumnSize);
	}
	auxFile.Close();
	if(pAuxTFile)	fclose(pAuxTFile);
	if(pAuxVFile)	fclose(pAuxVFile);
	delete [] pfBuff;
	delete [] pFileHeader;

	return TRUE;
}

BOOL CCyclerChannel::RemakeCanPCIndexFile(CString strBackUpRawFile, CString strCanMStepEndFile, CString strCanSStepEndFile, CString strNewTableFile)
{
	//파일명 설정 여부 확인 
	if(strBackUpRawFile.IsEmpty() || strNewTableFile.IsEmpty())
	{
		return FALSE;
	}

	COleDateTime t = COleDateTime::GetCurrentTime();
	CFile		fRS;
	CFileException e;

	//////////////////////////////////////////////////////////////////////////
	//복구된 Raw Data와 모듈에서 download한 복구용 Table file을 이용하여 PC용 Table 파일을 생성한다.
	//파일을 Open 한다.
	if( !fRS.Open( strBackUpRawFile, CFile::modeRead, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   m_strLastErrorString.Format("%s 파일을 열수 없습니다.", strBackUpRawFile);
	   return FALSE;
	}

	int nColumnSize = 0;
	PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
	if(fRS.Read(pFileHeader, sizeof(PS_CAN_FILE_HEADER)) > 0)
	{
		nColumnSize = pFileHeader->canHeader.nColumnCount;
	}
	
	delete pFileHeader;
	fRS.Close();
	
	//파일이 잘못되었을 경우 
	if(nColumnSize < 1 || nColumnSize > PS_MAX_CAN_SAVE_FILE_COLUMN)
	{
	   m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", strBackUpRawFile, nColumnSize);
		return FALSE;	//실제 Data 분석 
	}
	

	//모듈에서 전송받은 Step End 파일 Open
	char temp[PS_MAX_CAN_SAVE_FILE_COLUMN][20];
	
	FILE *pCanMFile = NULL;
	FILE *pCanSFile = NULL;
	if(strCanMStepEndFile != "")
	{
		pCanMFile = fopen(strCanMStepEndFile, "rt");
		if(pCanMFile == NULL)
		{
			m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			fclose(pCanMFile);
			return FALSE;	//실제 Data 분석 
		}

		for(int i = 0 ; i < m_chFileDataSet.m_nCanMasterCount +3 ; i++)
		{
			fscanf(pCanMFile, "%s,", temp[i]);
		}
	}
	if(strCanSStepEndFile != "")
	{
		pCanSFile = fopen(strCanSStepEndFile, "rt");
		if(pCanSFile == NULL)
		{
			m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			fclose(pCanMFile);
			fclose(pCanSFile);
			return FALSE;	//실제 Data 분석 
		}

		for( int i = 0; i < m_chFileDataSet.m_nCanSlaveCount +3; i++)
		{
			fscanf(pCanSFile, "%s,", temp[i]);
		}
	}
		
	//CAN *pfBuff = new float[nColumnSize];	
	CAN_VALUE lReadData[PS_MAX_CAN_SAVE_FILE_COLUMN] = { 0 };
	int nIndex, nData1, nData2;	
	
	CFile canFile;
	if( !canFile.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	{
		//생성 실패
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
			m_strLastErrorString.Format("%s를 생성할 수 없습니다.", strNewTableFile);
		
			return FALSE;
	}

	pFileHeader = new PS_CAN_FILE_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_CAN_FILE_HEADER));
	pFileHeader->fileHeader.nFileID = PS_PNE_CAN_RESULT_FILE_ID;
	pFileHeader->fileHeader.nFileVersion = 0x1000;
	sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
	sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Can end data file.");			

	pFileHeader->canHeader.nColumnCount = nColumnSize;

	for(int i = 0; i < nColumnSize; i++)
	{
		pFileHeader->canHeader.awColumnItem[i] = PS_CAN_VALUE;			//auxValue
	}

	canFile.Write(pFileHeader, sizeof(PS_CAN_FILE_HEADER));	
	
	int nDataIndex = 0 ;
	while (1)
	{
		if(pCanMFile)
		{
			if(fscanf(pCanMFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			{
				for(nDataIndex = 0 ; nDataIndex < m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
				{
					fscanf(pCanMFile, "%f,", &lReadData[nDataIndex].fVal[0]);
					
				}
			}
			else
				break;
		}
		if(pCanSFile)
		{
			if(fscanf(pCanSFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			{
				for(nDataIndex ; nDataIndex < m_chFileDataSet.m_nCanSlaveCount + m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
				{
					fscanf(pCanSFile, "%d,", &lReadData[nDataIndex].fVal[0]);
				}
			}
			else
				break;
		}
		
		canFile.Write(&lReadData, sizeof(CAN_VALUE) * nColumnSize);
	}
	canFile.Close();
	if(pCanMFile)	fclose(pCanMFile);
	if(pCanSFile)	fclose(pCanSFile);
	//delete [] pfBuff;
	delete [] pFileHeader;

	return TRUE;
}

//2008/07/24  lChargeAh, lDisChargeAh, lCapacitance, lChargeWh, lDisChargeWh, ulCVTime, szSyncTime 추가
BOOL CCyclerChannel::GetSaveDataD(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData)
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;
	
	SFT_VARIABLE_CH_DATA sSaveChData;
	ZeroMemory(&sSaveChData, sizeof(SFT_VARIABLE_CH_DATA));
	if(pChannel->PopSaveData(sSaveChData)== FALSE)	return FALSE;
//	CString strLog;
//	strLog.Format("Pop : 채널번호 : %d, Aux Count : %d", sSaveChData.chData.chNo, sSaveChData.chData.nAuxCount);
//	WriteLog(strLog);
	
	sChData.chNo		= sSaveChData.chData.chNo;					// Channel Number
	sChData.chState		= sSaveChData.chData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType	= sSaveChData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect = sSaveChData.chData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		= sSaveChData.chData.chCode;
	sChData.chStepNo	= sSaveChData.chData.chStepNo;
	sChData.chGradeCode = sSaveChData.chData.chGradeCode;
	
	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.chData.lCurrent, PS_CURRENT);

	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.chData.lChargeAh+sSaveChData.chData.lDisChargeAh, PS_CAPACITY);
	sChData.fChargeAh	= ConvertSbcToPCUnit(sSaveChData.chData.lChargeAh, PS_CAPACITY);
	sChData.fDisChargeAh= ConvertSbcToPCUnit(sSaveChData.chData.lDisChargeAh, PS_CAPACITY);
	sChData.fCapacitance= ConvertSbcToPCUnit(sSaveChData.chData.lCapacitance, PS_CAPACITY);

	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.chData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.chData.lChargeWh+sSaveChData.chData.lDisChargeWh, PS_WATT_HOUR);

	sChData.fChargeWh	= ConvertSbcToPCUnit(sSaveChData.chData.lChargeWh, PS_WATT_HOUR);
	sChData.fDisChargeWh= ConvertSbcToPCUnit(sSaveChData.chData.lDisChargeWh, PS_WATT_HOUR);
	sChData.fCVTime		= ConvertSbcToPCUnit(sSaveChData.chData.ulCVTime, PS_STEP_TIME);
	sChData.lSyncDate	= sSaveChData.chData.lSyncTime[0];		//ljb 20140120 edit
	sChData.fSyncTime	= ConvertSbcToPCUnit(sSaveChData.chData.lSyncTime[1], PS_SYNC_DATE);

	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.chData.ulStepTime, PS_STEP_TIME),			// 이번 Step 진행 시간
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.chData.ulTotalTime, PS_TOT_TIME),			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.chData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
	//sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.chData.lTemparature, PS_TEMPERATURE);
//	sChData.fOvenTemperature = ConvertSbcToPCUnit(sSaveChData.chData.lOvenTemparature, PS_OVEN_TEMPERATURE);		//m℃	//ljb 201008
//	sChData.fOvenHumidity	= ConvertSbcToPCUnit(sSaveChData.chData.lOvenHumidity, PS_OVEN_HUMIDITY);		//m℃	//ljb 201008
	sChData.nCurrentCycleNum = sSaveChData.chData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.chData.nTotalCycleNum;
	sChData.nAccCycleGroupNum1	= sSaveChData.chData.nAccCycleGroupNum1;		//ljb v1009
	sChData.nAccCycleGroupNum2	= sSaveChData.chData.nAccCycleGroupNum2;		
	sChData.nAccCycleGroupNum3	= sSaveChData.chData.nAccCycleGroupNum3;		
	sChData.nAccCycleGroupNum4	= sSaveChData.chData.nAccCycleGroupNum4;		
	sChData.nAccCycleGroupNum5	= sSaveChData.chData.nAccCycleGroupNum5;		
	sChData.nMultiCycleGroupNum1	= sSaveChData.chData.nMultiCycleGroupNum1;		//ljb v1009
	sChData.nMultiCycleGroupNum2	= sSaveChData.chData.nMultiCycleGroupNum2;		
	sChData.nMultiCycleGroupNum3	= sSaveChData.chData.nMultiCycleGroupNum3;		
	sChData.nMultiCycleGroupNum4	= sSaveChData.chData.nMultiCycleGroupNum4;		
	sChData.nMultiCycleGroupNum5	= sSaveChData.chData.nMultiCycleGroupNum5;		
	sChData.lSaveSequence	= sSaveChData.chData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.chData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.chData.lAvgCurrent, PS_CURRENT);
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;

//	TRACE("GetSaveDataC -> Capacity : %f, WattHour : %f\n",sChData.fCapacity, sChData.fWattHour);
	
	sAuxData.auxCount = sSaveChData.chData.nAuxCount;
	sAuxData.auxData->chNo = sSaveChData.chData.chNo;					// Channel Number
	for(int i = 0; i < sAuxData.auxCount; i++)
	{
		sAuxData.auxData[i].chNo = sSaveChData.chData.chNo;
		sAuxData.auxData[i].chAuxNo = sSaveChData.auxData[i].auxChNo;
		sAuxData.auxData[i].chType = sSaveChData.auxData[i].auxChType;
		if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TEMPERATURE);
		//else //20180607 yulee 주석처리
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_VOLTAGE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_VOLTAGE);
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE_TH) //20180607 yulee
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TYPE_TEMPERATURE_TH);


	}
	sCanData.canCount = sSaveChData.chData.nCanCount;
//	TRACE("Can Count : %d\r\n", sCanData.canCount);
	for(int i = 0 ; i < sCanData.canCount; i++)
	{
		sCanData.canData[i].chNo = sSaveChData.chData.chNo;
		sCanData.canData[i].canType	= sSaveChData.canData[i].canType;
		memcpy(&sCanData.canData[i].canVal, &sSaveChData.canData[i].canVal, sizeof(CAN_VALUE));
	}
	
	return TRUE;
}


//2008/7/24 
//1. Cell CV 추가
//2. 동기화 시간 데이터 추가
 int CCyclerChannel::CheckAndSaveDataA()
 {
 //파일명 설정 여부 확인 
 	if(m_strFilePath.IsEmpty())
 	{
 		return 0;
 	}
 
 	//외부에서 파일 조작을 할 경우file update를 Lock 시킨 후 update 실시하기 위함 
 	if(m_bLockFileUpdate == TRUE)
 	{
 		TRACE("Ch %d file update is locked\n", m_nChannelIndex+1);
 		return 0;
 	}
 
 	//저장할 Data가 없으면 return
 	if(GetStackedSize() < 1)	return 0;
 
 	int nRtn = 1;
 
 	//Add 2006/8/18
 	//Load last temp data
 	//////////////////////////////////////////////////////////////////////////
 	PS_STEP_END_RECORD prevData;
 	CString strStartTime, strTestSerial, strUser, strDescript;
 	long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
 	TRACE("Last data start index %d\n", lStartIndex);
 	//////////////////////////////////////////////////////////////////////////
 
 //	TRACE("Channel %d data save start\n", m_nChannelIndex+1);
 	
 	//2007/08/09 Aux Data 저장용//////////////////////////////////
 	//채널 데이터가 저장하는 방식을 따른다.
 
 	PS_AUX_RAW_DATA sAuxData;
 	float *pfAuxBuff = NULL;
 	size_t auxSize;
 	size_t auxLineSize;
 	CFile auxFile;
 	PS_AUX_FILE_HEADER auxHeader;
 	int nAuxColumnSize;
// 	int  nAuxFileCount;
// 	PS_AUX_RAW_DATA auxData;
 	
	int nCheck = 0;
 
 	if(nCheck = GetAuxFile(&auxFile, &auxHeader) < 0)	
 	{
 		return nCheck;
 	}
 	
 	nAuxColumnSize = auxHeader.auxHeader.nColumnCount;		//save data setting
 	
 	if(nAuxColumnSize < 0 || nAuxColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
 	{
 		CString strMsg;
 		strMsg.Format("AuxColumn : %d", nAuxColumnSize);
 		WriteLog(strMsg);
 		auxFile.Close();
 		return -1;
 	}
 		
 	if(nAuxColumnSize > 0)
 	{
 		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
 		auxLineSize =  sizeof(float)*nAuxColumnSize;
 		pfAuxBuff = new float[nAuxColumnSize*_MAX_DISK_WIRTE_COUNT];
 		auxSize = auxLineSize*_MAX_DISK_WIRTE_COUNT;
 		
 		//Line 수를 Count
 		int nAuxBodySize = auxFile.GetLength()-sizeof(PS_AUX_FILE_HEADER);
 		int nAuxRsCount = nAuxBodySize / auxLineSize ;
 		if(nAuxRsCount < 0 || nAuxBodySize % auxLineSize)
 		{
 			//		TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
 			auxFile.Close();
 			return -2;
 		}
 	}
 	
 	//////////////////////////////////////////////////////////////
 	//2007/10/17 CAN Data 저장용//////////////////////////////////
 	//채널 데이터가 저장하는 방식을 따른다.
 
 	PS_CAN_RAW_DATA sCanData;
 	SFT_CAN_VALUE *pCanBuff = NULL;
 	size_t canSize;
 	size_t canLineSize;
 	CFile canFile;
 	PS_CAN_FILE_HEADER canHeader;
 	int nCanColumnSize;
// 	int  nCanFileCount;
// 	PS_CAN_RAW_DATA canData;
 	
 
 	if(nCheck = GetCanFile(&canFile, &canHeader) < 0)	
 	{
 		return nCheck;
 	}
 	
 	nCanColumnSize = canHeader.canHeader.nColumnCount;		//save data setting
 	
 	if(nCanColumnSize < 0 || nCanColumnSize > PS_MAX_CAN_SAVE_FILE_COLUMN)
 	{
 		CString strMsg;
 		strMsg.Format("CanColumn : %d", nCanColumnSize);
 		WriteLog(strMsg);
 		canFile.Close();
 		return -1;
 	}
 		
 	if(nCanColumnSize > 0)
 	{
 		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
 		canLineSize =  sizeof(SFT_CAN_VALUE)*nCanColumnSize;
 		pCanBuff = new SFT_CAN_VALUE[nCanColumnSize*_MAX_DISK_WIRTE_COUNT];
 		canSize = canLineSize*_MAX_DISK_WIRTE_COUNT;
 		
 		//Line 수를 Count
 		int nCanBodySize = canFile.GetLength()-sizeof(PS_CAN_FILE_HEADER);
 		int nCanRsCount = nCanBodySize / canLineSize ;
 		if(nCanRsCount < 0 || nCanBodySize % canLineSize)
 		{
 			CString strMsg;
 			strMsg.Format("CAN File Size Error (CanColumn : %d)", nCanColumnSize);
 			WriteLog(strMsg);
 			canFile.Close();
 			return -2;
 		}
 	}
 	
 	//////////////////////////////////////////////////////////////
 	
 	//채널 데이터 저장용///////////////////////////////////////////////
 	PS_RAW_FILE_HEADER rawHeader;
 	CFile fRS;
 	PS_STEP_END_RECORD	sChResultData;
 	float *pfBuff;
 	size_t rsSize, lineSize;
 
 	if(nCheck = GetChFile(fRS,rawHeader) < 0)
 	{
 		return nCheck;
 	}
 	
 	int nColumnSize = rawHeader.rsHeader.nColumnCount;		//save data setting
 
 	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
 	{
 		auxFile.Close();
 		canFile.Close();
 		fRS.Close();
 		return -3;
 	}
 
 	//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
 	lineSize = sizeof(float)*nColumnSize;
 	pfBuff = new float[nColumnSize*_MAX_DISK_WIRTE_COUNT];
 	rsSize = lineSize*_MAX_DISK_WIRTE_COUNT;
 
 	//Line 수를 Count
 	int nBodySize = fRS.GetLength()-sizeof(PS_RAW_FILE_HEADER);
 	int nRsCount = nBodySize / lineSize ;
 	if(nRsCount < 0 || nBodySize % lineSize)
 	{
 //		TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
 		auxFile.Close();
 		canFile.Close();
 		fRS.Close();
 		return -4;
 	}
 	
 	int nStackCount = 0;
 	
 
 #ifdef _DEBUG
 	DWORD dwStart = GetTickCount();
 #endif
 	
 	while(GetSaveDataD(sChResultData, sAuxData, sCanData))	//저장할 Data가 있을 경우 Stack에서 1개 Pop 한다.
 	{
 		//sChResultData.lSaveSequence => 1 Base No
 		if((sChResultData.lSaveSequence-1)  > nRsCount)		//Data loss 발생
 		{
 			//중간부 Data 손실 여부만 확인 가능하다. 즉 손실 이후 최소 한번이상의 결과 Data가 갱신되지 않으면 
 			//자동 검출 불가하다. 
 			m_bDataLoss = TRUE;					 
 //			TRACE("*****************>>>>>>>>>>>>>> Data loss detected(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
 			nRtn = 0;
 		}
 		else if((sChResultData.lSaveSequence-1)  < nRsCount)	//Index가 역전된 경우 
 		{
 			//작업중에 복구 명령을 수행하면 Download와 복구 중에 발생한 Buffering data가 중복되게 된다.
 			//이럴경우 현재 송신된 data(버퍼에 있던 data)는 이미 모듈에서 download하여 복구한 data이므로 
 			//저장하지 않고 Skip한다.
 //			nPreviousStepNo = sChResultData.chStepNo;
 //			nPrevTotalCycleNo = sChResultData.nTotalCycleNum;
 //			TRACE("*****************>>>>>>>>>>>>>> Data index ???(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
 			continue;
 		}
 		else												//정상적인 data
 		{
 //			TRACE("*****************>>>>>>>>>>>>>> Data index %d received\n", sChResultData.lSaveSequence);
 			m_bDataLoss = FALSE;
 		}
 
 		//Data 저장시 모든 단위를 m단위로 저장한다. (실처리 항목은 최소로 한다.) 파일 Size 문제...
 		for(int dataIndex = 0; dataIndex <nColumnSize ; dataIndex++)
 		{
 			pfBuff[dataIndex + nStackCount * nColumnSize] = GetItemData(sChResultData, rawHeader.rsHeader.awColumnItem[dataIndex]);	//Index 5 is WH	
 		}		
 	
 	//	int nAuxIndex = 0;
 		for(int nAuxDataIndex = 0; nAuxDataIndex < nAuxColumnSize ; nAuxDataIndex++)
 		{
 		//	pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = GetAuxItemData(sAuxData, auxHeader.auxHeader.awColumnItem[nAuxDataIndex], nAuxIndex);					
 		//	if(auxHeader.auxHeader.awColumnItem[nAuxDataIndex] == PS_AUX_VALUE)
 		//		nAuxIndex++;
 			pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = sAuxData.auxData[nAuxDataIndex].fValue;
 		}	
 
 	//	int nCanIndex = 0;
 		for(int nCanDataIndex = 0; nCanDataIndex < nCanColumnSize ; nCanDataIndex++)
 		{
 			//plCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] = GetCanItemData(sCanData, canHeader.canHeader.awColumnItem[nCanDataIndex], nCanIndex);								
 			//if(canHeader.canHeader.awColumnItem[nCanDataIndex] == PS_CAN_VALUE)
 			//	nCanIndex++;
 			memcpy(&pCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] , &sCanData.canData[nCanDataIndex].canVal, sizeof(SFT_CAN_VALUE));
 
 			
 		}	
 //		TRACE("StackCount : %d\r\n", nStackCount);
// 		TRACE("CAN Data : %f, %f, %f\r\n", pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal);
 	
 //		TRACE("V:%d/I:%d/C:%d\n", sChResultData.lVoltage,sChResultData.lCurrent,sChResultData.lCapacity );
 
 		//////////////////////////////////////////////////////////////////////////
 		//2006/8/4/
 		//result file save type mode.
 
 		//2. Binary Mode file, TextMode temp. file
 		//lStartIndex = WriteResultListFileE(lStartIndex, nRsCount, sChResultData);
 		//lStartIndex = WriteResultListFileF(lStartIndex, nRsCount, sChResultData, sAuxData);
 		lStartIndex = WriteResultListFileB(lStartIndex, nRsCount, sChResultData, sAuxData, sCanData);
 		if(lStartIndex < 0)
 		{
 			nRtn = -5;
 		}
 
 
 		//////////////////////////////////////////////////////////////////////////
 		nRsCount++;
 		nStackCount++;
 
 		//Disk write 성능 향상을 위해 한꺼번에 기록 하도록 수정 
 		if(nStackCount >= _MAX_DISK_WIRTE_COUNT)
 		{
 			fRS.Write(pfBuff, rsSize);
 
 			if(nAuxColumnSize > 0)
 			{
 				auxFile.Write(pfAuxBuff, auxSize);
 				
 				//	strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
 				//		pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
 				//		pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
 				
 			//	TRACE(strMsg);
 			}
 			if(nCanColumnSize > 0)
 			{
 				canFile.Write(pCanBuff, canSize);
 				CString strMsg;
 				strMsg.Format("ChNo %d : Stack >= MAX 파일에 기록(Aux 수 : %d)(CAN 수 : %d)",
 					m_nChannelIndex+1,nAuxColumnSize,nCanColumnSize
 					) ;
 				WriteLog(strMsg);
 			}
 			
 
 			nStackCount = 0;
 		}
 	}
 
 	//Disk access 횟수를 줄이기 위해 한꺼번에 저장 
 	if(nStackCount > 0)
 	{
 		fRS.Write(pfBuff, sizeof(float)*nColumnSize*nStackCount);
 
 		if(nAuxColumnSize > 0)
 		{
 			auxFile.Write(pfAuxBuff, sizeof(float)*nAuxColumnSize*nStackCount);
 			//CString strMsg;
 			//strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
 			//	pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
 			//	pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
 			
 			//	TRACE(strMsg);
 		}
 		if(nCanColumnSize > 0)
 		{
 			canFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*nCanColumnSize*nStackCount);
 
 			CString strMsg;
 				strMsg.Format("ChNo %d : Stack >= MAX 파일에 기록(Aux 수 : %d)(CAN 수 : %d)",
 					m_nChannelIndex+1,nAuxColumnSize,nCanColumnSize
 					) ;
 			WriteLog(strMsg);
 		}
 		
 	
 	
 		//2006/8/18
 		//Update Last temp file
 		UpdateLastData(UINT(lStartIndex), nRsCount-1, strStartTime, strTestSerial, strUser, strDescript, sChResultData);
 	}
 
 #ifdef _DEBUG
 	if(m_nChannelIndex == 0)
 		TRACE("Channel %d data save time : %dms elapsed\n",  m_nChannelIndex+1, GetTickCount()-dwStart);
 #endif
 
 //	if(sChResultData.chNo == 1)
 //		TRACE("===========================>CH%03d-S02d: [%dmsec]\n", sChResultData.chNo, sChResultData.chStepNo, GetTickCount()-dwTime);
 
 	//
 	if(pfBuff != NULL)
 	{	
 		delete[]	pfBuff;		
 		pfBuff = NULL;
 	}
 	if(pfAuxBuff != NULL)
 	{
 		delete [] pfAuxBuff;
 		pfAuxBuff = NULL;
 	}
 	if(pCanBuff != NULL)
 	{
 		delete [] pCanBuff;
 		pCanBuff = NULL;
 	}
 		
 	if (fRS.m_hFile != CFile::hFileNull)
 	{
 		fRS.Flush();
 		fRS.Close();
 		auxFile.Flush();
 		auxFile.Close();
 		canFile.Flush();
 		canFile.Close();
 	}
 	TRACE("Channel %d 결과 Data 저장\n", m_nChannelIndex+1);
 
 	return nRtn;
 }



//DEL float CCyclerChannel::GetItemData_V1004(PS_STEP_END_RECORD_V1004 &chData, WORD wItem)
//DEL {
//DEL float fData = 0.0f;
//DEL 	switch(wItem)
//DEL 	{
//DEL 	case		PS_STATE		:	fData = float(chData.chState);	break;
//DEL 	case		PS_VOLTAGE		:	fData = chData.fVoltage;		break;		//u단위		//n단위	
//DEL 	case		PS_CURRENT		:	fData = chData.fCurrent;		break;		//u단위		//n단위(전류종속 단위)
//DEL 	case		PS_CAPACITY		:	fData = chData.fCapacity;		break;		//u단위		//n단위(전류종속 단위)
//DEL 	case		PS_IMPEDANCE	:	fData = chData.fImpedance;		break;		//u단위		
//DEL 	case		PS_CODE			:	fData = float(chData.chCode);	break;
//DEL 	case		PS_STEP_TIME	:	fData = chData.fStepTime;			break;		//10msec
//DEL 	case		PS_TOT_TIME		:	fData = chData.fTotalTime;			break;		//10msec
//DEL 	case		PS_GRADE_CODE	:	fData = float(chData.chGradeCode);			break;
//DEL 	case		PS_STEP_NO		:	fData = float(chData.chStepNo);				break;
//DEL 	case		PS_WATT			:	fData = chData.fWatt;			break;		//mWatt		//u단위(전류종속 단위)
//DEL 	case		PS_WATT_HOUR	:	fData = chData.fWattHour;		break;		//mWh		//u단위(전류종속 단위)
//DEL 	case		PS_TEMPERATURE	:	fData = chData.fTemparature;	break;		//m℃
//DEL 	case		PS_PRESSURE		:	fData = chData.fPressure;		break;		//m~
//DEL 	case		PS_STEP_TYPE	:	fData = float(chData.chStepType);				break;
//DEL 	case		PS_CUR_CYCLE	:	fData = float(chData.nCurrentCycleNum);			break;
//DEL 	case		PS_TOT_CYCLE	:	fData = float(chData.nTotalCycleNum);			break;
//DEL //	case		PS_TEST_NAME	:	fData = float(lData);			break;
//DEL //	case		PS_SCHEDULE_NAME:	fData = float(lData);			break;
//DEL 	case		PS_CHANNEL_NO	:	fData = float(chData.chNo);			break;
//DEL 	case		PS_MODULE_NO	:	fData = float(m_nModuleID);			break;
//DEL //	case		PS_LOT_NO		:	fData = float(lData);			break;
//DEL 	case		PS_DATA_SEQ		:	fData = float(chData.lSaveSequence);				break;
//DEL 	case		PS_AVG_CURRENT	:	fData = chData.fAvgCurrent;		break;		//u단위		//n단위(전류종속 단위)
//DEL 	case		PS_AVG_VOLTAGE	:	fData = chData.fAvgVoltage;		break;		//u단위		//n단위(전류종속 단위)
//DEL 	case		PS_METER_DATA	:	fData = (float)m_fMeterValue;
//DEL 	}
//DEL 	return fData;
//DEL }

//DEL void CCyclerChannel::UpdateLastData_V1004(UINT nStartIndex, UINT nEndIndex, CString strStartT, CString strSerial, CString strUser, CString strDescript, PS_STEP_END_RECORD_V1004 sChData)
//DEL {
//DEL CString strCurTempFile, strDataString;
//DEL 	strCurTempFile.Format("%s\\%s.rp$",  m_strFilePath, m_strTestName);						//.rp$
//DEL 
//DEL 	//시험이 완료 되었으면 임시 파일을 삭제한다.
//DEL 	if(sChData.chStepType == PS_STEP_END)
//DEL 	{
//DEL 		unlink(strCurTempFile);
//DEL 		WriteLog(m_strTestName+" 작업 완료");
//DEL 	}
//DEL 	else	//시험이 완료되지 않았으면 다음 step 저장을 위해 Index를 저장한다. Step 번호를 증가 시키고 시작 Index를 저장한다.
//DEL 	{
//DEL 		//6. 최종 임시파일은 갱신한다.
//DEL 		//No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temp,Press,AvgVoltage,AvgCurrent,
//DEL 		strDataString.Format(	"%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,", 
//DEL 									sChData.chStepNo,			//StepNo
//DEL 									nStartIndex,				//IndexFrom	
//DEL 									nEndIndex,					//IndexTo
//DEL 									sChData.nCurrentCycleNum,	//CurCycle
//DEL 									sChData.nTotalCycleNum,		//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
//DEL 									sChData.chStepType,			//Type
//DEL 									sChData.chState,			//State
//DEL 									sChData.fStepTime,			//Time
//DEL 									sChData.fTotalTime,		//TotTime
//DEL 									sChData.chCode,				//Code
//DEL 									sChData.chGradeCode,		//Grade
//DEL 									sChData.fVoltage, 			//Voltage
//DEL 									sChData.fCurrent, 			//Current
//DEL 									sChData.fCapacity, 			//Capacity
//DEL 									sChData.fWattHour, 			//WattHour
//DEL 									sChData.fImpedance,			//IR
//DEL 									sChData.fTemparature, 		//Temp
//DEL 									sChData.fPressure, 			//Press
//DEL 									sChData.fAvgVoltage,		//AvgVoltage
//DEL 									sChData.fAvgCurrent,		//AvgVoltage
//DEL 									sChData.lSaveSequence
//DEL 						);
//DEL 		//현재 step의 진행된 부분까지 tmp파일에 저장한다.
//DEL 		FILE *fp = fopen(strCurTempFile, "wt");
//DEL 		if(fp)
//DEL 		{
//DEL 			CString strLine1, strLine2;
//DEL 			COleDateTime t = COleDateTime::GetCurrentTime();
//DEL 			strLine1.Format("StartT=%s, EndT=%s, Serial=%s, User=%s, Descript=%s", strStartT, t.Format(), strSerial, strUser, strDescript);
//DEL 			strLine2 = TABEL_FILE_COLUMN_HEAD;
//DEL 			fprintf(fp, "%s\n%s\n%s", strLine1, strLine2, strDataString);
//DEL 			fclose(fp);
//DEL 		}
//DEL 		//	TRACE("%s 파일 Udpated(%d Step Saved)\n", m_strResultStepFileName, strDataList.GetCount()-1);
//DEL 	}
//DEL 	//////////////////////////////////////////////////////////////////////////
//DEL }

long CCyclerChannel::GetChargeAh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CHARGE_CAP);
}

long CCyclerChannel::GetDisChargeAh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_DISCHARGE_CAP);
}

long CCyclerChannel::GetCapacitance()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITANCE);
}

long CCyclerChannel::GetChargeWh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CHARGE_WH);
}

long CCyclerChannel::GetDisChargeWh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_DISCHARGE_WH);
}

long CCyclerChannel::GetCVTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CV_TIME);
}

long CCyclerChannel::GetAccCycleCount1()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE1);
}
long CCyclerChannel::GetAccCycleCount2()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE2);
}
long CCyclerChannel::GetAccCycleCount3()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE3);
}
long CCyclerChannel::GetAccCycleCount4()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE4);
}
long CCyclerChannel::GetAccCycleCount5()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE5);
}
long CCyclerChannel::GetMultiCycleCount1()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE1);
}
long CCyclerChannel::GetMultiCycleCount2()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE2);
}
long CCyclerChannel::GetMultiCycleCount3()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE3);
}
long CCyclerChannel::GetMultiCycleCount4()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE4);
}
long CCyclerChannel::GetMultiCycleCount5()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE5);
}

long CCyclerChannel::GetSyncDate()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_DATE);
}

long CCyclerChannel::GetSyncTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_TIME);
}

CString CCyclerChannel::GetSyncDateTimeString()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	long lDate = ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_DATE);
	long lTime = ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_TIME);
	
	char buffer[20];
	CString strDateTime;
	if(lDate == 0 || lTime == 0)
	{
		SYSTEMTIME st;

		GetLocalTime(&st);  
		sprintf(buffer, "*%d-%02d-%02d %02d:%02d:%02d.%03d" , st.wYear, st.wMonth, st.wDay,
			st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		strDateTime.Format("%s", buffer);
		return strDateTime;
	}
	CString strTemp1, strTemp2;
	ltoa(lDate, buffer, 10);
	strTemp1.Format("%s", buffer);
	strDateTime.Format("%s-%s-%s", strTemp1.Left(4), strTemp1.Mid(4, 2), strTemp1.Right(2));
	strTemp1 = strDateTime;
		//strDateTime.Format("%s-%s-%s %s:%s:%s.%s")

	ltoa(lTime, buffer, 10);
	strTemp2.Format("%09s", buffer);
	strDateTime.Format("%s %s:%s:%s.%s", strTemp1, strTemp2.Left(2), strTemp2.Mid(2,2),
		strTemp2.Mid(4,2), strTemp2.Right(3));
	return strDateTime;

}

void CCyclerChannel::SetCanTransCount(int nCount)
{
	this->m_nCanTransCount = nCount;
}
