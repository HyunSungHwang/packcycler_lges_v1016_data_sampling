#if !defined(AFX_CONFIGSET_DLG_H__9FC37EDA_B1CF_4DF8_A84F_6ED60FB81F09__INCLUDED_)
#define AFX_CONFIGSET_DLG_H__9FC37EDA_B1CF_4DF8_A84F_6ED60FB81F09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConfigSet_DLG.h : header file
//



//#include "PNE_define.h"
//#include "MyGridWnd.h"
#include "ColorButton2.h"
#include "ColorBox.h"
#include "reportctrl.h"
#include "CTSRealGraphDlg.h"
//#include "PNE_define.h"

/////////////////////////////////////////////////////////////////////////////
// CConfigSet_DLG dialog

class CConfigSet_DLG : public CDialog
{
// Construction
public:
	CConfigSet_DLG(CWnd* pParent = NULL);   // standard constructor

	CString m_strSaveFileName;				// 저장되고 있는 파일 이름
	UINT m_nFileNum;						// file number 추가 
	CString m_strSaveFileFullName;
	CBitmap m_bmp1;
	
	CString m_strMaxValue, m_strMinValue;							// 20100913 SSL -> ini 파일에 저장되는 Max Value and Min Value								
	CString	m_strOrder;												// 20100913 SSL -> ini 파일에 저장되는 채널 이름
	CString m_strUnit;												// 20100913 SSL -> ini 파일에 저장되는 단위(V, mV, A, mA, Ah, mAh) 			
	CString m_strAlias;
	CString m_strCh_Time, m_strChamber_Time, m_strAux_Time;			// 20100913 SSL -> ini 파일에 저장되는 각각의 그래프의 시간
	CString m_strName;												// 20100913 SSL -> ini 파일에 저장되는 각각의 개체 이름
	CString m_strColor;												// 20100913 SSL -> ini 파일에 저장되는 색상 
	CString m_strCheck;												// 20100913 SSL -> ini 파일에 저장되는 체크박스 선택 여부
	CString m_strOffset;
	CString m_strFactor;

	CColorBox m_TWellColor[PS_MAX_STATE_COLOR];

	BOOL file_Finder(CString str_path);								// 20100913 SSL -> File 찾기
	BOOL Fun_MakeIniFile();											// 20100913 SSL -> INI 파일 생성
	void Fun_DisplayCondition();									// 20100914 SSL -> INI 파일의 내용을 화면에 표시
	void Fun_DisplayListCtrl();										// 20100927 SSL -> Aux, Can Data를 ListCtrl에 표시
	void DrawColor();												// 20100927 SSL -> 버튼에 색상 표시
	CString Fun_getAppPath();
	static COleDateTime GenRandDate();

	
// Dialog Data
	//{{AFX_DATA(CConfigSet_DLG)
	enum { IDD = IDD_CONFIG_SET_DLG , IDD2 = IDD_CONFIG_SET_DLG_ENG };
	CComboBox	m_ComboCapUnit;
	CComboBox	m_ComboCurUnit;
	CComboBox	m_ComboPowUnit;
	CReportCtrl	m_ListCtrlCan;
	CReportCtrl	m_ListCtrlAux;
	CComboBox	m_ComboVolUnit;
	BOOL	m_checkAux;
	BOOL	m_bChkVoltage;
	BOOL	m_bChkCurrent;
	BOOL	m_bChkPower;
	BOOL	m_bChkCapacity;
	BOOL	m_bChkCreatTemper;
	BOOL	m_bChkMeasureHumid;
	BOOL	m_bChkCreatHumid;
	BOOL	m_bChkMeasureTemper;
	float	m_fVoltageMin;
	float	m_fVoltageMax;
	float	m_fCurrentMin;
	float	m_fCurrentMax;
	float	m_fPowerMin;
	float	m_fPowerMax;
	float	m_fCapacitorMax;
	float	m_fCapacitorMin;
	float	m_fCreatTemperMin;
	float	m_fCreatTemperMax;
	float	m_fMeasureTemperMin;
	float	m_fMeasureTemperMax;
	float	m_fCreatHumidMin;
	float	m_fCreatHumidMax;
	float	m_fMeasureHumidMax;
	float	m_fMeasureHumidMin;
	float	m_fChannelGraphTime;
	float	m_fChamberGraphTime;
	float	m_fAuxCanGraphTime;		
	int  	m_iChxcount;
	int  	m_iAuxxcount;
	int  	m_iCanxcount;
	int	    m_iChamxcount;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigSet_DLG)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CConfigSet_DLG)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnOK();
	afx_msg void OnVolColor();
	afx_msg void OnCurColor();
	afx_msg void OnPowColor();
	afx_msg void OnCapColor();
	afx_msg void OnCtColor();
	afx_msg void OnMtColor();
	afx_msg void OnChColor();
	afx_msg void OnMhColor();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);		
	//}}AFX_MSG
	//afx_msg void Fun_Name();
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGSET_DLG_H__9FC37EDA_B1CF_4DF8_A84F_6ED60FB81F09__INCLUDED_)
