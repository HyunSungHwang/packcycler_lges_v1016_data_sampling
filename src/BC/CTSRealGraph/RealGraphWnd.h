// #if !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
// #define AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GraphWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRealGraphWnd_V2 window
#include "../../../include/PEGRPAPI.h"
#include "../../../include/pemessag.h"


#define NULL_DATA			999999999
#define ANNOTATION_COLOR	RGB(0,200,0)

class CRealGraphWnd_V2 : public CStatic
{
// Construction
public:
	CRealGraphWnd_V2();
	
	Graph_Global_Info *m_GraphInfo;
	
// Attributes
public:
	HWND	m_hWndPE;	
	int     m_iYCount;	
	BOOL	m_bShowArray[PE_MAX_SUBSET];
	
	COleDateTime chtime;		//ljb 2011512 
	COleDateTime auxtime;		//ljb 2011512 
	COleDateTime chamtime;		//ljb 2011512 
	
	void ConverChData(SFT_CH_DATA &chData);
	BOOL RefreshGraph();
	void CreateGraph();
	
// Operations
public:
	void SetN(UINT type, INT value);
	void SetV(UINT type, VOID FAR * lpData, UINT nItems);
	void SetSZ(UINT type, CHAR FAR *str);
	void SetVCell(UINT type, UINT nCell, VOID FAR * lpData);
	void SetVCellEx(UINT type, int subset, UINT nCell, VOID FAR *lpData);
	
	void ShowSubset(int id, int SetCnt, BOOL bShow);	
	BOOL Start(int iNo);
	void Stop();	
	BOOL ClearGraph();
	void Print();
	void SetData(int subset, int pos, LPCTSTR xData, double yData);
	void SetMaximize();
	
	BOOL Fun_DrawChannelGraph();							
	bool Fun_DrawAuxGraph();								
	bool Fun_DrawCanGraph();								
	bool Fun_DrawChamberGraph();							

	void Fun_SubsetSetting(int id);							
	
	
protected:

	void SetGlobalPEStuff();	
	int			m_iSelectGraph;
		

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRealGraphWnd_V2)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	float m_fCh[4];
	float m_fCham[4];
	float m_fAuxCan[PE_MAX_SUBSET];

	void Fun_SetRealTimeGraph();
	void CustomizeDialog();
	void SaveGraph();
	void SetSubTitle(CString str);
	void SetMainTitle(CString str);	
	void SetDataTitle(int nSubID, CString str);
	void SetYAxisLabel(int subID, CString str);
	void SetXAxisLabel(CString str);
	void CopyImg();

	virtual ~CRealGraphWnd_V2();

	// Generated message map functions
protected:
	//{{AFX_MSG(CRealGraphWnd_V2)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();	
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

//#endif // !defined(AFX_GRAPHWND_H__7EA2A462_3364_11D2_80A6_0020741517CE__INCLUDED_)
