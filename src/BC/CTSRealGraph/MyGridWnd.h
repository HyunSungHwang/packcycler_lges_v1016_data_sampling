#if !defined(AFX_MYGRIDWND_H__345D1683_26CE_11D2_975A_444553540000__INCLUDED_)
#define AFX_MYGRIDWND_H__345D1683_26CE_11D2_975A_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MyGridWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyGridWnd window
#include "MsgDefine.h"

struct COLORARRAY
{
	COLORREF BackColor;
	COLORREF TextColor;
};

class CMyGridWnd : public CGXGridWnd
{
// Construction
public:
	CMyGridWnd();

// Attributes
public:
	BOOL		m_bSelectRange;
	CGXRange	m_SelectRange;
	COLORREF	m_BackColor;
	COLORREF	m_SelColor;
	BOOL		m_bSameColSize;
	BOOL		m_bSameRowSize;
	ROWCOL		m_nCol1, m_nCol2;
	COLORREF	m_ColHeaderFgColor[16];
	COLORREF	m_ColHeaderBgColor[16];
	BOOL		m_bCustomColor;
	CGXRange	m_CustomColorRange;
	char *		m_pCustomColorFlag;
	COLORARRAY	m_ColorArray[16];
	BOOL		m_bRowSelection;
	BOOL		m_bCanDelete;
	BOOL		m_bCustomWidth;
	int			m_nWidth[20];
	ROWCOL		m_nOutlineRow;
	ROWCOL		m_nOutlineCol;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyGridWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	virtual ~CMyGridWnd();
	void OnInitialUpdate();
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnStartSelection(ROWCOL nRow, ROWCOL nCol, UINT flags, CPoint point);
	BOOL CanChangeSelection(CGXRange* pRange, BOOL bIsDragging, BOOL bKey);
	int  GetColWidth(ROWCOL nCol);
	int  GetRowHeight(ROWCOL nRow);
	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType);
	BOOL CanSelectCurrentCell(BOOL bSelect, ROWCOL dwSelectRow, ROWCOL dwSelectCol, ROWCOL dwOldRow, ROWCOL dwOldCol);
	void OnChangedSelection(const CGXRange* pRange, BOOL bIsDragging, BOOL bKey);
	void DrawInvertCell(CDC* /*pDC*/, ROWCOL nRow, ROWCOL nCol, CRect rectItem);
	BOOL OnDeleteCell(ROWCOL nRow, ROWCOL nCol);
	BOOL OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol);
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
	void OnCanceledEditing(ROWCOL nRow, ROWCOL nCol);
	void OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol);
	// Generated message map functions
protected:
	//{{AFX_MSG(CMyGridWnd)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYGRIDWND_H__345D1683_26CE_11D2_975A_444553540000__INCLUDED_)
