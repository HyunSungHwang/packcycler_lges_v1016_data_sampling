// MinMaxData.h: interface for the CMinMaxData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MINMAXDATA_H__FA3925C9_7F68_449D_B66E_1DD02BCB5D2A__INCLUDED_)
#define AFX_MINMAXDATA_H__FA3925C9_7F68_449D_B66E_1DD02BCB5D2A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define DATE_TIME_ARRAY_SIZE	32

class CMinMaxData  
{
public:
	void ResetData();
	void AddData(long lData);
	CMinMaxData();
	virtual ~CMinMaxData();

	long m_lValue;
	long m_lMax;
	long m_lMin;
	float m_fAvg;
	double m_dStdD;
	UINT m_nMaxIndex;
	UINT m_nMinIndex;
	char m_szMaxDateTime[DATE_TIME_ARRAY_SIZE];
	char m_szMinDateTime[DATE_TIME_ARRAY_SIZE];

	long	GetCount()	{ return m_nCount;};
	long	GetMinData() {return m_lMin;};
	long	GetMaxData()	{return m_lMax;};
	float	GetAvg()		{return m_fAvg;};
	float	GetStdD()		{return (float)m_dStdD;};
	char *  GetMinTime()	{return m_szMinDateTime;};
	char *	GetMaxTime()	{return m_szMaxDateTime;};
	UINT	GetMinDataIndex()	{return m_nMinIndex;};
	UINT	GetMaxDataIndex() {return m_nMaxIndex;};

protected:
	UINT m_nCount;
	double m_lSum;
	double m_sqareSum;
};

#endif // !defined(AFX_MINMAXDATA_H__FA3925C9_7F68_449D_B66E_1DD02BCB5D2A__INCLUDED_)
