// Dlg_Auxcangraph.cpp : implementation file
//

#include "stdafx.h"
#include "ctsrealgraph.h"
#include "Dlg_Auxcangraph.h"

#include "CTSRealGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Auxcangraph dialog


Dlg_Auxcangraph::Dlg_Auxcangraph(CWnd* pParent /*=NULL*/)
	: CDialog(Dlg_Auxcangraph::IDD, pParent)
{
	//{{AFX_DATA_INIT(Dlg_Auxcangraph)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bStartDraw=FALSE;
}


void Dlg_Auxcangraph::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Auxcangraph)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Auxcangraph, CDialog)
	//{{AFX_MSG_MAP(Dlg_Auxcangraph)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Auxcangraph message handlers

BOOL Dlg_Auxcangraph::OnInitDialog() 
{
	CDialog::OnInitDialog();
			
	m_wndGraphAux.SubclassDlgItem(IDC_LAB_GRAPH_AUX1, this);
	
	m_wndGraphAux.Fun_SubsetSetting(1);
	
	Fun_DrawFrame();
	Fun_HideGraph();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_Auxcangraph::OnOK() 
{
	CDialog::OnOK();
}

void Dlg_Auxcangraph::OnCancel() 
{
	CDialog::OnCancel();
}

void Dlg_Auxcangraph::onStart()
{
	if(!mainDlg) return;
	
	if(m_bStartDraw == FALSE)
	{
		Fun_DrawFrame();
				
		m_wndGraphAux.m_GraphInfo=&g_GraphInfo;		
		m_wndGraphAux.Start(1);
		
		m_bStartDraw = TRUE;	
	}
}

void Dlg_Auxcangraph::onStop()
{
	m_wndGraphAux.Stop();
	
	m_bStartDraw = FALSE;
}

void Dlg_Auxcangraph::Fun_DrawFrame()
{
	if(!mainDlg) return;

	int iCnt = 0;
	CString str;
	CString strTemp;

	m_wndGraphAux.ClearGraph();
	
	iCnt = mainDlg->Fun_GetCheckAuxinfo(); //+mainDlg->Fun_GetCheckCaninfo();		

	int i=0;
	int nItem=0;
	if(g_GraphInfo.nAuxCount>0)
	{
		for(int i = 0;i<g_GraphInfo.nAuxCount;i++)
		{// Aux Can Graph Subset Setting
					
			if(g_GraphInfo.AuxInfo[i].bCheck==TRUE) 
			{
				m_wndGraphAux.ShowSubset(nItem, iCnt, TRUE);    
			
				strTemp.Format("%s",g_GraphInfo.AuxInfo[i].strAlias);
				m_wndGraphAux.SetYAxisLabel(nItem, strTemp);
				m_wndGraphAux.SetDataTitle (nItem, strTemp);
				nItem=nItem+1;
			}
		}
	}

	/*for(int i = 0;i<g_GraphInfo.nCanCount;i++)
	{// Aux Can Graph Subset Setting
				
		if(g_GraphInfo.CanInfo[i].bCheck==TRUE) 
		{
			m_wndGraphAux.ShowSubset(nItem, iCnt, TRUE); 
		
			strTemp.Format("%s",g_GraphInfo.CanInfo[i].strAlias);
			m_wndGraphAux.SetYAxisLabel(nItem, strTemp);
			m_wndGraphAux.SetDataTitle (nItem, strTemp);
			nItem=nItem+1;
		}
	}*/

	
	//Set X Axis Label	
	m_wndGraphAux.SetXAxisLabel("");
	
	//Set Title
	m_wndGraphAux.SetMainTitle("AUX & CAN Graph");	
}


void Dlg_Auxcangraph::Fun_Refresh()
{
	CString ka;
	ka.Format("aux %d",IsWindow(m_wndGraphAux.m_hWndPE));
//	AfxMessageBox(ka);

//	if(IsWindow(m_wndGraphAux.m_hWndPE)) ::ShowWindow(m_wndGraphAux.m_hWndPE,SW_SHOW);	
}


void Dlg_Auxcangraph::Fun_HideGraph()
{
	if(!mainDlg) return;
	
	UpdateData(TRUE);
	
	RECT rt;
	GetClientRect(&rt);
	
	GetDlgItem(IDC_LAB_GRAPH_AUX1)->MoveWindow(rt.left+10,rt.top+10,rt.right-20,(rt.bottom)-20,TRUE);
}


void Dlg_Auxcangraph::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	CDialog::OnSize(nType, cx, cy);
	
	RECT r;
	CRect ctrlRect, ctrlGrid;
	if(::IsWindow(m_hWnd))
	{
		::GetClientRect(m_hWnd, &r);
		
		if(::IsWindow(m_wndGraphAux.GetSafeHwnd()))
		{
			Fun_HideGraph();
		}
	}	
		
}

BOOL Dlg_Auxcangraph::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_ESCAPE || pMsg->wParam==VK_RETURN)
		{
			return FALSE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
