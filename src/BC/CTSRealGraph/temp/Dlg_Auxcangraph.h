#if !defined(AFX_DLG_AUXCANGRAPH_H__0C65B7D0_63AC_49F4_8805_F6BC41791357__INCLUDED_)
#define AFX_DLG_AUXCANGRAPH_H__0C65B7D0_63AC_49F4_8805_F6BC41791357__INCLUDED_

#include "GraphWnd.h"
#include "RealGraphWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Auxcangraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_Auxcangraph dialog

class Dlg_Auxcangraph : public CDialog
{
// Construction
public:
	Dlg_Auxcangraph(CWnd* pParent = NULL);   // standard constructor

	BOOL m_bStartDraw;
	
	CRealGraphWnd_V2 m_wndGraphAux;
	
	void Fun_Refresh();
	void Fun_DrawFrame();
	void Fun_HideGraph();
	void onStart();
	void onStop();
	

// Dialog Data
	//{{AFX_DATA(Dlg_Auxcangraph)
	enum { IDD = IDD_DIALOG_AUXCANGRAPH };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Auxcangraph)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Auxcangraph)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_AUXCANGRAPH_H__0C65B7D0_63AC_49F4_8805_F6BC41791357__INCLUDED_)
