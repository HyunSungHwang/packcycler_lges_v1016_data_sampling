// CTSRealGraph.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CTSRealGraph.h"
#include "CTSRealGraphDlg.h"

#include "Dlg_Allgraph.h"
#include "Dlg_Chgraph.h"
#include "Dlg_Auxgraph.h"
#include "Dlg_Cangraph.h"
#include "Dlg_Chamgraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSRealGraphApp

BEGIN_MESSAGE_MAP(CCTSRealGraphApp, CWinApp)
	//{{AFX_MSG_MAP(CCTSRealGraphApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSRealGraphApp construction

CCTSRealGraphApp::CCTSRealGraphApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCTSRealGraphApp object

CCTSRealGraphApp theApp;
CCTSRealGraphDlg *mainDlg;
Dlg_Allgraph     *mainTabAll;
Dlg_Chgraph      *mainTabCh;
Dlg_Auxgraph     *mainTabAux;
Dlg_Cangraph     *mainTabCan;
Dlg_Chamgraph    *mainTabCham;

/////////////////////////////////////////////////////////////////////////////
// CCTSRealGraphApp initialization

BOOL CCTSRealGraphApp::InitInstance()
{
	AfxEnableControlContainer();

	GXInit();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileNew)
	{
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
	}

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	SetRegistryKey("PNE Real Graph");
		
	srand( (unsigned)time( NULL ) );

	CCTSRealGraphDlg dlg;
	m_pMainWnd = &dlg;
	
	//cmdInfo.m_strFileName="M01CH00";

	if(__argc==3)
	{
		dlg.m_EditTestName=(CString)__argv[2];
	}

	if (cmdInfo.m_strFileName == "")
	{
		dlg.m_nModuleID = 1;
		dlg.m_nChIndex = 0;
	}
	else
	{
		if (cmdInfo.m_strFileName.GetLength() == 7)
		{
			CString smodule=cmdInfo.m_strFileName.Mid(1,2);
			CString schannel=cmdInfo.m_strFileName.Mid(5,2);

			dlg.m_nModuleID=atoi(smodule);
			dlg.m_nChIndex=atoi(schannel);		
		}
	}

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
