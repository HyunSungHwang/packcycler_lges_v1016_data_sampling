// MyGridWnd.cpp : implementation file
//

#include "stdafx.h"
#include "MsgDefine.h"
#include "MyGridWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyGridWnd

CMyGridWnd::CMyGridWnd()
{
	m_bSelectRange = FALSE;
	m_BackColor = RGB(255,255,255);
	m_SelColor = RGB(230,230, 230);
	//	m_SelColor = RGB(0,255,255);
	m_bSameColSize = TRUE;
	m_bSameRowSize = TRUE;
	m_nCol1 = -1;
	m_nCol2 = -1;
	for(int i=0;i<16;i++)
	{
		m_ColHeaderBgColor[i] = RGB(192,192,192);
		m_ColHeaderFgColor[i] = RGB(0,0,0);
	}
	m_ColorArray[ 0].TextColor = RGB(0,0,0);			m_ColorArray[ 0].BackColor = RGB(192,192,192); // ȸ��
	m_ColorArray[ 1].TextColor = RGB(255,255,255);		m_ColorArray[ 1].BackColor = RGB(32,158,29); // �ʷ�
	m_ColorArray[ 2].TextColor = RGB(255,255,255);		m_ColorArray[ 2].BackColor = RGB(230,150,150); // ����
	m_ColorArray[ 3].TextColor = RGB(255,255,255);		m_ColorArray[ 3].BackColor = RGB(20,100,200); // ���Ķ�
	m_ColorArray[ 4].TextColor = RGB(255,255,255);		m_ColorArray[ 4].BackColor = RGB(255,0,128);
	m_ColorArray[ 5].TextColor = RGB(255,255,0);		m_ColorArray[ 5].BackColor = RGB(130,60,200);
	m_ColorArray[ 6].TextColor = RGB(255,0,0);			m_ColorArray[ 6].BackColor = RGB(0,255,0);
	m_ColorArray[ 7].TextColor = RGB(0,0,255);			m_ColorArray[ 7].BackColor = RGB(255,255,0);
	m_ColorArray[ 8].TextColor = RGB(255,0,0);			m_ColorArray[ 8].BackColor = RGB(0,255,255);
	m_ColorArray[ 9].TextColor = RGB(255,0,0);			m_ColorArray[ 9].BackColor = RGB(192,192,192);
	m_ColorArray[10].TextColor = RGB(0,0,0);			m_ColorArray[10].BackColor = RGB(192,192,192);
	m_ColorArray[11].TextColor = RGB(0,0,0);			m_ColorArray[11].BackColor = RGB(192,192,192);
	m_ColorArray[12].TextColor = RGB(0,0,0);			m_ColorArray[12].BackColor = RGB(192,192,192);
	m_ColorArray[13].TextColor = RGB(0,0,0);			m_ColorArray[13].BackColor = RGB(192,192,192);
	m_ColorArray[14].TextColor = RGB(0,0,0);			m_ColorArray[14].BackColor = RGB(255,255,255);
	m_ColorArray[15].TextColor = RGB(0,0,0);			m_ColorArray[15].BackColor = RGB(192,192,192);
	m_bRowSelection = FALSE;
	m_bCanDelete = FALSE;
	m_bCustomWidth = FALSE;
	m_nOutlineRow = 0;
	m_nOutlineCol = 0;
}

CMyGridWnd::~CMyGridWnd()
{
}


BEGIN_MESSAGE_MAP(CMyGridWnd, CGXGridWnd)
	//{{AFX_MSG_MAP(CMyGridWnd)
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CMyGridWnd::OnInitialUpdate()
{
	CGXGridWnd::OnInitialUpdate();

	CGXGridParam *pParam = GetParam();

	BOOL bLock = LockUpdate();
	EnableIntelliMouse();

	pParam->EnableUndo();
	pParam->EnableMoveRows(FALSE);
	pParam->EnableMoveCols(FALSE);
	pParam->SetSyncCurrentCell(TRUE);
	pParam->GetProperties()->SetDisplayHorzLines(FALSE);
	pParam->GetProperties()->SetDisplayVertLines(FALSE);
	pParam->GetProperties()->SetMarkRowHeader(FALSE);
	pParam->GetProperties()->SetMarkColHeader(FALSE);

	ChangeStandardStyle(StandardStyle()
		.SetControl(GX_IDS_CTRL_STATIC)
		.SetVerticalAlignment(DT_VCENTER)
		.SetHorizontalAlignment(DT_CENTER)
		.SetDraw3dFrame(gxFrameRaised)
		.SetFont(
			CGXFont()
				.SetSize(9)
				.SetFaceName("����")
				.SetBold(FALSE)
		)

	);
	ChangeColHeaderStyle(CGXStyle()
		.SetInterior(RGB(0,0,128))
		.SetTextColor(RGB(255,255,255))
		.SetBorders(
			gxBorderAll, 
			CGXPen()
				.SetStyle(PS_NULL)
		)
		.SetFont(
			CGXFont()
				.SetSize(10)
				.SetFaceName("����")
				.SetBold(FALSE)
		)
	);
	EnableHints(FALSE);
//	SetDefaultRowHeight(20);
//	SetDefaultColWidth(80);
	SetColWidth(0, 0, 0);
	
	m_bRefreshOnSetCurrentCell = TRUE;
	if(m_bRowSelection)
	{
		pParam->SetActivateCellFlags(FALSE);
		pParam->EnableSelection(GX_SELFULL & ~GX_SELTABLE & ~GX_SELCOL &  ~GX_SELMULTIPLE);
		pParam->SetSpecialMode(GX_MODELBOX_SS);
		pParam->SetHideCurrentCell(GX_HIDE_ALLWAYS);
	}
	LockUpdate(bLock);
}

BOOL CMyGridWnd::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
	CGXGridWnd::OnLButtonDblClkRowCol(nRow, nCol, nFlags, pt);
	CWnd *pWnd = GetParent();
	if(pWnd)
		pWnd->PostMessage(WM_GRID_DOUBLECLICK, (WPARAM) value, (LPARAM) this);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMyGridWnd message handlers
int CMyGridWnd::GetColWidth(ROWCOL nCol)
{
//	if(!IsPrinting())
	int nWidth;
	int tCol = GetColCount();
	CRect rect = GetGridRect();
	if(nCol>0 && m_bSameColSize && !IsPrinting())
	{
		if(nCol == (ROWCOL)tCol)
		{
			nWidth = rect.Width() - (int (rect.Width() / tCol) * (tCol-1)) - 1;
		}
		else nWidth = rect.Width() / tCol;
	}
	else if(nCol>0 && m_bCustomWidth && !IsPrinting())
	{
		if(nCol == (ROWCOL)tCol)
		{
			int sum = 0;
			for(int i=1;i<tCol;i++)
			{
				sum += m_nWidth[i];
			}
			nWidth = rect.Width() - sum - 1;
		}
		else
			nWidth = m_nWidth[nCol];
	}
	else
	{
		nWidth = CGXGridCore::GetColWidth(nCol);
		if(!IsPrinting() && nCol == GetColCount())
		{
			nCol = GetClientCol(nCol);
			if(nCol>0)
				nWidth = max(nWidth, rect.Width() - CalcSumOfClientColWidths(0, nCol-1));
		}
	}
	return nWidth;
}

int CMyGridWnd::GetRowHeight(ROWCOL nRow)
{
	int nHeight;
	int tRow = GetRowCount()+1;
	CRect rect = GetGridRect();
	if(nRow>=0 && (rect.Height() / tRow) >= 16 && (rect.Height() / tRow) <= 26 && m_bSameRowSize  && !IsPrinting())
	{
		if(nRow == (ROWCOL)tRow-1)
		{
			nHeight = rect.Height() - (int (rect.Height() / tRow) * (tRow-1)) - 1;
		}
		else nHeight = rect.Height() / tRow;
	}
	else if(nRow>=0 && (rect.Height() / tRow) > 26 && m_bSameRowSize  && !IsPrinting())
		nHeight = 26;
	else
		nHeight = CGXGridCore::GetRowHeight(nRow);

	return nHeight;
}

BOOL CMyGridWnd::GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType)
{
	BOOL bRet = CGXGridWnd::GetStyleRowCol(nRow, nCol, style, mt, nType);
	if(IsPrinting())
		return bRet;

	if(style.GetIncludeEnabled())
	{
		if(!style.GetEnabled())
			style.SetInterior(RGB(128,128,128));
//		else
//			style.SetInterior(RGB(192,192,192));
	}

	if (nType == 0 && GetInvertStateRowCol(nRow, nCol, GetParam()->GetRangeList()))
 	{	
 		style.SetInterior(m_SelColor);
		style.SetDraw3dFrame(gxFrameInset);
 		bRet = TRUE;
 	}
	else if(nRow > 0 && nCol > 0 && IsCurrentCell(nRow, nCol))
	{
 		style.SetInterior(m_SelColor);
		style.SetDraw3dFrame(gxFrameInset);
		bRet = TRUE;
	}
	else if(nRow > 0 && nCol > 0)
	{
			if(m_bCustomColor)
			{
				if(m_CustomColorRange.IsCellInRange(nRow, nCol))
				{
					ROWCOL y = nRow - m_CustomColorRange.top;
					ROWCOL x = nCol - m_CustomColorRange.left;
					ROWCOL width = m_CustomColorRange.GetWidth();
					int index = m_pCustomColorFlag[y* width + x];
					if(index<0 || index > 15) index = 15;
					COLORREF bkColor = m_ColorArray[index].BackColor;
					COLORREF fgColor = m_ColorArray[index].TextColor;
					style
						.SetInterior(bkColor)
						.SetTextColor(fgColor);
				}
				else
					style.SetInterior(m_BackColor);
			}
			else
			{
				if(nCol > m_nCol1 && nCol < m_nCol2)
					style
						.SetInterior(m_ColHeaderBgColor[nCol])
						.SetTextColor(m_ColHeaderFgColor[nCol])
						;
				else
				{
					style.SetInterior(m_BackColor);
				}
			}
	}
	return bRet;
}

BOOL CMyGridWnd::OnStartSelection(ROWCOL nRow, ROWCOL nCol, UINT flags, CPoint point)
{
	if(m_bSelectRange)
	{
		if(m_SelectRange.IsCellInRange(nRow, nCol))
			return FALSE;
	}
	return CGXGridWnd::OnStartSelection(nRow, nCol, flags, point);
}

BOOL CMyGridWnd::CanChangeSelection(CGXRange* pRange, BOOL bIsDragging, BOOL bKey)
{
	if(m_bSelectRange && pRange)
	{
		CGXRange range, oldrange;
		oldrange = *pRange;
		range.IntersectRange(oldrange, m_SelectRange);
		if(range.IsValid()==TRUE)
			return FALSE;
	}
	return CGXGridWnd::CanChangeSelection(pRange, bIsDragging, bKey);
}

BOOL CMyGridWnd::CanSelectCurrentCell(BOOL bSelect, ROWCOL dwSelectRow, ROWCOL dwSelectCol, ROWCOL dwOldRow, ROWCOL dwOldCol)
{
	if(m_bSelectRange)
	{
		if(m_SelectRange.IsCellInRange(dwSelectRow, dwSelectCol))
			return FALSE;
	}
	return CGXGridWnd::CanSelectCurrentCell(bSelect, dwSelectRow, dwSelectCol, dwOldRow, dwOldCol);
}

void CMyGridWnd::OnKillFocus(CWnd* pNewWnd) 
{
	CGXGridWnd::OnKillFocus(pNewWnd);
	
	GetParent()->PostMessage(WM_GRID_KILLFOCUS, 0, (LPARAM)this);	
}

void CMyGridWnd::OnSetFocus(CWnd* pOldWnd) 
{
	CGXGridWnd::OnSetFocus(pOldWnd);
	
	GetParent()->PostMessage(WM_GRID_SETFOCUS, 0, (LPARAM)this);	
}

void CMyGridWnd::OnChangedSelection(const CGXRange* pRange, BOOL bIsDragging, BOOL bKey)
{
	CGXGridWnd::OnChangedSelection(pRange, bIsDragging, bKey);
	GetParent()->PostMessage(WM_SEL_CHANGED, 0, (LPARAM) this);
}

void CMyGridWnd::DrawInvertCell(CDC* /*pDC*/, ROWCOL nRow, ROWCOL nCol, CRect rectItem)
{
 	if (m_nNestedDraw == 0)
 	{
 		CGXRange range;
 		if (GetCoveredCellsRowCol(nRow, nCol, range))
 			rectItem = CalcRectFromRowCol(range.top, range.left, range.bottom, range.right);
 
 		InvalidateRect(&rectItem);
 	}
}

BOOL CMyGridWnd::OnDeleteCell(ROWCOL nRow, ROWCOL nCol)
{
	if(m_bCanDelete)
		return CGXGridWnd::OnDeleteCell(nRow, nCol);
	else
		return FALSE;
}

BOOL CMyGridWnd::OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol)
{
	BOOL ret = CGXGridWnd::OnLeftCell(nRow, nCol, nNewRow, nNewCol);
	if(ret)
	{
//		m_nOutlineRow = nRow;
//		if(nRow != nNewRow && nNewRow > 0)
//		GetParent()->PostMessage(WM_GRID_MOVEROW, (WPARAM) nNewRow, (LPARAM) this);
	}
	m_nOutlineRow = nRow;
	m_nOutlineCol = nCol;
	return ret;
}

void CMyGridWnd::OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnMovedCurrentCell(nRow, nCol);
	if(nRow && nCol)
	{
		if((nRow != m_nOutlineRow) || (nCol != m_nOutlineCol) )
		{
			BOOL bLock = LockUpdate();
			if(bLock==FALSE)
			{	
				WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
				GetParent()->PostMessage(WM_GRID_MOVECELL, (WPARAM) value, (LPARAM) this);
			}
			LockUpdate(bLock);
		}
	}
}

BOOL CMyGridWnd::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	BOOL ret = CGXGridWnd::OnStartEditing(nRow, nCol);
	if(ret)
	{
		GetParent()->PostMessage(WM_GRID_BEGINEDIT, (WPARAM) nRow, (LPARAM) this);
	}
	return ret;
}

void CMyGridWnd::OnCanceledEditing(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnCanceledEditing(nRow, nCol);
	GetParent()->PostMessage(WM_GRID_CANCELEDIT, (WPARAM) nRow, (LPARAM) this);
}


void CMyGridWnd::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol)
{
	WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
	CGXGridWnd::OnClickedButtonRowCol(nRow, nCol);
	GetParent()->PostMessage(WM_GRID_BTNCLICK, (WPARAM) value, (LPARAM) this);
}

BOOL CMyGridWnd::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	WPARAM value = ((WPARAM)(nRow) << 16) | ((WPARAM)(nCol));
	CGXGridWnd::OnLButtonClickedRowCol(nRow, nCol, nFlags, pt);
	CWnd *pWnd = GetParent();
	if(pWnd)
		pWnd->PostMessage(WM_GRID_CLICK, (WPARAM) value, (LPARAM) this);
	return TRUE;
}
