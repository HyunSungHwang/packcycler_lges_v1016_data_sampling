#define ID_TIMER_REAL_CH_DATA				100
#define ID_TIMER_REAL_CH_DATA_INTERVAL		500

#define ID_TIMER_REAL_CHAMBER_DATA			100
#define ID_TIMER_REAL_CHAMBER_DRAW_INTERVAL	5000

#define AUX_MAX	5
#define MAX_GCOLOR 12
#define MAX_DRAW_INFO 12


typedef struct Graph_Draw_Info
{
	float graphtime1;
	float graphtime2;
	float graphtime3;


	char szname[MAX_DRAW_INFO];
	char cUnit[MAX_DRAW_INFO];
	int icount;
	int auxcancount[MAX_DRAW_INFO];
	float fMin;
	float fMax;
	COLORREF lineColor;
	CString strAlias;
	BOOL bUse;
	BOOL bCheck;

	CPtrArray DrawInfo;
	CPtrArray AuxcanInfo;
	CPtrArray ChamInfo;

} Graph_Draw_Info;



