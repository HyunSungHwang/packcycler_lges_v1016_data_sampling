# Microsoft Developer Studio Project File - Name="CTSRealGraph" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CTSRealGraph - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CTSRealGraph.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CTSRealGraph.mak" CFG="CTSRealGraph - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CTSRealGraph - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CTSRealGraph - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CTSRealGraph - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/CTSRealGraph_Release/"
# PROP Intermediate_Dir "../../../obj/CTSRealGraph_Release/"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/Release/CTSRealGraph.exe"

!ELSEIF  "$(CFG)" == "CTSRealGraph - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/CTSRealGraph_Debug/"
# PROP Intermediate_Dir "../../../obj/CTSRealGraph_Debug/"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"../../../bin/Debug/CTSRealGraph.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "CTSRealGraph - Win32 Release"
# Name "CTSRealGraph - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AddEdit.cpp
# End Source File
# Begin Source File

SOURCE=..\..\BCLib\SRC\PSServer\Channel.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorBox.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorButton2.cpp
# End Source File
# Begin Source File

SOURCE=.\ConfigSet_DLG.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSRealGraph.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSRealGraph.rc
# End Source File
# Begin Source File

SOURCE=.\CTSRealGraphDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CyclerChannel.cpp
# End Source File
# Begin Source File

SOURCE=.\CyclerModule.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_Allgraph.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_Auxgraph.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_Cangraph.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_Chamgraph.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_Chgraph.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\MinMaxData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\BCLib\SRC\PSServer\Module.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleIndexTable.cpp
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\RealGraphWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AddEdit.h
# End Source File
# Begin Source File

SOURCE=.\ColorBox.h
# End Source File
# Begin Source File

SOURCE=.\ColorButton2.h
# End Source File
# Begin Source File

SOURCE=.\ConfigSet_DLG.h
# End Source File
# Begin Source File

SOURCE=.\CTSRealGraph.h
# End Source File
# Begin Source File

SOURCE=.\CTSRealGraphDlg.h
# End Source File
# Begin Source File

SOURCE=.\CyclerChannel.h
# End Source File
# Begin Source File

SOURCE=.\CyclerModule.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_Allgraph.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_Auxgraph.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_Cangraph.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_Chamgraph.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_Chgraph.h
# End Source File
# Begin Source File

SOURCE=.\GraphWnd.h
# End Source File
# Begin Source File

SOURCE=.\MsgDefine.h
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.h
# End Source File
# Begin Source File

SOURCE=.\PNE_define.h
# End Source File
# Begin Source File

SOURCE=.\RealGraphWnd.h
# End Source File
# Begin Source File

SOURCE=.\ReportCtrl.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\Temp.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\BaTesterDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSRealGraph.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSRealGraph.rc2
# End Source File
# End Group
# Begin Group "MyTabCtrl"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Global\MyTabCtrl\MyTabCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\MyTabCtrl\MyTabCtrl.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
