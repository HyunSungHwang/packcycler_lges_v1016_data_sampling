// AddEdit.cpp : implementation file
//

#include "stdafx.h"
//#include "ctsrealgraph.h"
#include "AddEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddEdit dialog


CAddEdit::CAddEdit(CWnd* pParent /*=NULL*/)
	: CDialog(CAddEdit::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddEdit)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAddEdit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddEdit)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddEdit, CDialog)
	//{{AFX_MSG_MAP(CAddEdit)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddEdit message handlers
