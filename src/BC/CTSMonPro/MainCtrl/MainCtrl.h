// MainCtrl.h: interface for the CMainCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
#define AFX_MAINCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//-----------------------------------------------------
#define ID_CELLBAL_STAT_NONE                  0
#define ID_CELLBAL_STAT_START                 1
#define ID_CELLBAL_STAT_RUN                   2
#define ID_CELLBAL_STAT_STOP                  3
//-----------------------------------------------------
#define ID_PWRSUPY_STAT_NONE                  0
#define ID_PWRSUPY_STAT_ON                    1
#define ID_PWRSUPY_STAT_OFF                   2
#define ID_PWRSUPY_STAT_STOP                  3
//-----------------------------------------------------
#define ID_CHILLER_STAT_NONE                  0
#define ID_CHILLER_STAT_START                 1
#define ID_CHILLER_STAT_ON                    2
#define ID_CHILLER_STAT_OFF                   3
#define ID_CHILLER_STAT_STOP                  4
//-----------------------------------------------------
class CCyclerChannel;

class MainCtrl  
{
public:
	MainCtrl();
	virtual ~MainCtrl();
	
	CCyclerChannel    *m_pChannel;

	SFT_CELLBAL_VALUE m_pCellBALData;

	HANDLE            m_hDealThread_State;
	HANDLE            m_hDealThread_PwrSupply;
	HANDLE            m_hDealThread_CellBAL;
	HANDLE            m_hDealThread_Chiller;
	
	int m_iPwr_StepNo;
	int m_iPwr_CycCurr;

	int m_iChiller_StepNo;
	int m_iChiller_CycCurr;
	bool m_bChiller_DESChiller; //lyj 20200210 DES chiller
	bool m_bChiller_ZeroSet;   //lyj 20201105 Chiller Temp/Pump 0값이면 셋안하도록 레지스트리 처리 default:FALSE

	int m_iCellBAL_StepNo;
	int m_iCellBAL_CycCurr;

	int m_iCellBAL_NextStepCnt;
	
	void onDealThreadStart();
	void onRelease();
	void onSbcRunInit();

	static DWORD WINAPI StateDealThread(LPVOID arg);
	static DWORD WINAPI PwrSupplyDealThread(LPVOID arg);
	static DWORD WINAPI CellBALDealThread(LPVOID arg);
	static DWORD WINAPI ChillerDealThread(LPVOID arg);
	//---------------------------------------------------
	void onPwrSetData(CCyclerChannel *pChannel,int nState);
	void onChillerSetData(CCyclerChannel *pChannel,int nState);
	void onCellBALSetData(CCyclerChannel *pChannel,int nState);	
	//---------------------------------------------------
	void onPwrDealChannel(CCyclerChannel *pChannel);
    void onChillerDealChannel(CCyclerChannel *pChannel);
	void onCellBalChannel(CCyclerChannel *pChannel);
	//---------------------------------------------------
	BOOL onCellBALSendOpModeSet_Sched(CCyclerChannel *pChannel,CtrlSocketClient *ctrlClient,CStep *pStep);
	BOOL onCellBALSendLoadModeSet_Sched(CCyclerChannel *pChannel,CtrlSocketClient *ctrlClient,CStep *pStep);
	BOOL onCellBALSendRun_Sched(CCyclerChannel *pChannel,int module_installed_ch,int nCellBal_Count,CString strChState,CtrlSocketClient *ctrlClient,CStep *pStep);
	BOOL onCellBALSendStop(CCyclerChannel *pChannel,CtrlSocketClient *ctrlClient,int iType=0);
	CString onCellBALGetMachine_ch_data(S_P1_CH_DATA *ch_data,int iType=0);
	CString onCellBALGetMachine_ch_state(char *ch_state,int iType=0);

	BOOL onChillerSbcChPause(CCyclerChannel *pChannel,CStep *pStep,int iType,CString strLogType);
	BOOL onChillerSendRefTemp(CCyclerChannel *pChannel,int nItem,int nOvenID,
		                      float fCurTemp1,float fRefTemp1,float fChillerRefTemp);
	BOOL onChillerSendRefPump(CCyclerChannel *pChannel,int nItem,int nOvenID,
							  int iCurPump1,int iRefPump1,int iPumpRefTemp);
	BOOL onChillerSendRunTemp(CCyclerChannel *pChannel,int nItem,int nOvenID,
		                      int iChillerRefCmd,CString strType);
	BOOL onChillerSendRunPump(CCyclerChannel *pChannel,int nItem,int nOvenID,
		                      int iPumpRefCmd,CString strType);

	BOOL OnChillerCommErrorLog(int nErrNo, int nChNo);

	BOOL onPwrSbcChPause(CCyclerChannel *pChannel,CStep *pStep,int nItem,CString strLogType);


	bool m_bNEXFilter;		//lyj 20200924
	int m_iNEXFilterCount;	//lyj 20200924
	int m_iNEXFilterCount_Loop;
	float m_fltPreCurTemp;	//lyj 20200924
	float m_fltPreRefTemp;	//lyj 20200924
	
protected:

};

#endif // !defined(AFX_MAINCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
