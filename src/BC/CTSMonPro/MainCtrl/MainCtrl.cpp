// MainCtrl.cpp: implementation of the CMainCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../CTSMonPro.h"
#include "MainCtrl.h"

#include "../MainFrm.h"
#include "../CTSMonProDoc.h"
#include "../CTSMonProView.h"
#include "../Global.h"
//#include "../MiniDump.h"//yulee 20190115

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MainCtrl::MainCtrl()
{	
	m_pChannel=NULL;

	m_hDealThread_State=NULL;
	m_hDealThread_PwrSupply=NULL;
	m_hDealThread_CellBAL=NULL;
	m_hDealThread_Chiller=NULL;

	memset(&m_pCellBALData,0,sizeof(SFT_CELLBAL_VALUE));

	m_iPwr_StepNo=0;
	m_iPwr_CycCurr=0;

	m_iChiller_StepNo=0;
	m_iChiller_CycCurr=0;
	m_bChiller_DESChiller = FALSE; //lyj 20200210 Des Chiller
	m_bChiller_ZeroSet = FALSE;

	m_iCellBAL_StepNo=0;
	m_iCellBAL_CycCurr=0;
	m_iCellBAL_NextStepCnt=0;


	m_bNEXFilter = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use NEXFilter", 0);;		//lyj 20200924
	m_iNEXFilterCount = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use NEXFilterCount", 10);	//lyj 20200924 //ksj 20200925 : 기본값 0->10
	m_fltPreCurTemp = 0.0f;	//lyj 20200924
	m_fltPreRefTemp = 0.0f;	//lyj 20200924
	m_iNEXFilterCount_Loop =0;
}

MainCtrl::~MainCtrl()
{
	
}

void MainCtrl::onRelease()
{
	GWin::win_ThreadEnd2(m_hDealThread_State);
	GWin::win_ThreadEnd2(m_hDealThread_PwrSupply);
	GWin::win_ThreadEnd2(m_hDealThread_CellBAL);
	GWin::win_ThreadEnd2(m_hDealThread_Chiller);
}

void MainCtrl::onDealThreadStart()
{
	//CMiniDump::Begin(); //yulee 20190115
	DWORD hThreadId=0;	

	
	if(!m_hDealThread_State)
	{
		hThreadId=0;	
		m_hDealThread_State = CreateThread(NULL, 0, StateDealThread,
		                                  (LPVOID)this, 0, &hThreadId);	   
	}
	if(!m_hDealThread_PwrSupply)
	{
		hThreadId=0;	
		m_hDealThread_PwrSupply = CreateThread(NULL, 0, PwrSupplyDealThread,
		                                  (LPVOID)this, 0, &hThreadId);	   
	}
	if(!m_hDealThread_CellBAL)
	{
		hThreadId=0;	
		m_hDealThread_CellBAL = CreateThread(NULL, 0, CellBALDealThread,
											(LPVOID)this, 0, &hThreadId);	   
	}
	if(!m_hDealThread_Chiller)
	{
		hThreadId=0;	
		m_hDealThread_Chiller = CreateThread(NULL, 0, ChillerDealThread,
			                                 (LPVOID)this, 0, &hThreadId);	   
	}
	//CMiniDump::End(); //yulee 20190115
}

DWORD WINAPI MainCtrl::StateDealThread(LPVOID arg)
{
	MainCtrl *obj=(MainCtrl *)arg;
	if(!obj) return 0;
	
	Sleep(1000);
	
	CCyclerChannel *pChannel=obj->m_pChannel;
	if(!pChannel) return 0;
	
	int nState=0;
	while(1)
	{
		if(g_AppInfo.iEnd==1) break;
		
		nState=pChannel->GetState();
		//-----------------------------------
		obj->onPwrSetData(pChannel,nState);//PowrSupply
        obj->onChillerSetData(pChannel,nState);//Chiller
		obj->onCellBALSetData(pChannel,nState);//CellBAL
		//-----------------------------------
		Sleep(200);
	}
	return 1;
}

DWORD WINAPI MainCtrl::PwrSupplyDealThread(LPVOID arg)
{
	MainCtrl *obj=(MainCtrl *)arg;
	if(!obj) return 0;
	
	Sleep(1000);
	
	CCyclerChannel *pChannel=obj->m_pChannel;
	if(!pChannel) return 0;
	
	int nState=0;
	while(1)
	{		
		if(g_AppInfo.iEnd==1) break;
		
		nState=pChannel->GetState();  
		//---------------------------------------
		if( nState==PS_STATE_RUN )
		{
			obj->onPwrDealChannel(pChannel);//PowerSupply
		}
		Sleep(200);
	}
	return 1;
}

DWORD WINAPI MainCtrl::CellBALDealThread(LPVOID arg)
 {
	MainCtrl *obj=(MainCtrl *)arg;
	if(!obj) return 0;
	
	Sleep(1000);
	
	CCyclerChannel *pChannel=obj->m_pChannel;
	if(!pChannel) return 0;
	
	int nState=0;
	while(1)
	{		
		if(g_AppInfo.iEnd==1) break;
		
		nState=pChannel->GetState();
		//---------------------------------------
		if( nState==PS_STATE_RUN )
		{
			obj->onCellBalChannel(pChannel);//CellBAL
		}
		Sleep(200);
	}
	return 1;
}

DWORD WINAPI MainCtrl::ChillerDealThread(LPVOID arg)
{
	MainCtrl *obj=(MainCtrl *)arg;
	if(!obj) return 0;
	
	Sleep(1000);
	
	CCyclerChannel *pChannel=obj->m_pChannel;
	if(!pChannel) return 0;
	
	int nState=0;
	while(1)
	{		
		if(g_AppInfo.iEnd==1) break;
		
		nState=pChannel->GetState();
		//---------------------------------------
		if( nState==PS_STATE_RUN )
		{
			obj->onChillerDealChannel(pChannel);//Chiller
		}
		Sleep(200);
	}
	return 1;
}

void MainCtrl::onSbcRunInit()
{
	//CMiniDump::Begin(); //yulee 20190115
	m_iPwr_StepNo=0;
	m_iPwr_CycCurr=0;
	
	m_iChiller_StepNo=0;
	m_iChiller_CycCurr=0;
	
	m_iCellBAL_StepNo=0;
	m_iCellBAL_CycCurr=0;
	//CMiniDump::End(); //yulee 20190115
}

void MainCtrl::onPwrDealChannel(CCyclerChannel *pChannel)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return;
	if(!mainFrm)  return;

	int nState=pChannel->GetState();
	if(nState!=PS_STATE_RUN) return;

	CScheduleData *pSchedule=pChannel->GetScheduleData();
	if(!pSchedule) return;
	
	CString strTemp,strTemp2;
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	int nCycCurr=pChannel->GetCurCycleCount();
	//--------------------------------------------
	int   i=0;
	BOOL  bflag=FALSE;
	CStep *pStep=NULL;
	int   nTotal=pSchedule->GetStepSize();
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	for(int i = 0;i<nTotal;i++)
	{
		pStep=pSchedule->GetStepData(i);
        if(!pStep) continue;
		
		strTemp.Format(_T("CHNO:%d_A sch_no:%d pwr_cmd:%d state:%d seqno:%d stepno:%d/%d steptype:%d/%d cyccurr:%d"),
			            nCHNO,i,
						pStep->m_nPwrSupply_Cmd,
						pStep->m_nPwrSupply_State,
						pStep->m_nPwrSupply_SeqNo,						
						(pStep->m_StepIndex+1),nStepNo,
						pStep->m_type,nStepType,nCycCurr);
	
		if( (pStep->m_nPwrSupply_Cmd==1 || pStep->m_nPwrSupply_Cmd==2) &&
			(pStep->m_StepIndex+1)==nStepNo)
		{
			bflag=TRUE;
			break;
		}
	}
	//-------------------------------------
	if(bflag==FALSE) return;
	//-------------------------------------
	int nOutp=0;
	int nItem=mainFrm->onGetSerialPowerNo(nCHNO,nOutp);
	
	CString strLogType;
	strLogType.Format(_T("power_%d_%d"),nItem,nCHNO);

	strTemp2.Format(_T("CHNO:%d_B StepNo:%d StepType:%d Pwr_No:%d Pwr_Cmd:%d Pwr_Volt:%d Outp:%d"),
		            nCHNO,nStepNo,nStepType,nItem,
				    pStep->m_nPwrSupply_Cmd,
				    pStep->m_nPwrSupply_Volt,
				    nOutp);
	if( m_iPwr_StepNo!=nStepNo ||
		m_iPwr_CycCurr!=nCycCurr)
	{
		pStep->m_nPwrSupply_State=0;
		pStep->m_nPwrSupply_Log=0;
		pStep->m_nPwrSupply_SeqNo=0;
		
		log_All(strLogType,_T("start"),_T("-------------------------"));
		log_All(strLogType,_T("state"),strTemp);
		log_All(strLogType,_T("cmd"),strTemp2);
		pStep->m_nPwrSupply_Log=2;
	}
	m_iPwr_StepNo=nStepNo;
	m_iPwr_CycCurr=nCycCurr;	
	//-------------------------------------
	if(nItem<0 || nItem>1) return;
	//-------------------------------------
	float fCur_Volt=0;
	float fSet_Volt=0;
	float fDev=0;
	int   iSeq=0;

	pStep->m_nPwrSupply_SeqNo++;
	if( (pStep->m_nPwrSupply_SeqNo%50)==0)
	{//10sec Interval=>200msec Interval thread
		iSeq=pStep->m_nPwrSupply_SeqNo/50;
		fCur_Volt=mainFrm->m_PwrSupplyCtrl[nItem].m_fState_Volt[nOutp-1];
		fSet_Volt=(float)pStep->m_nPwrSupply_Volt/1000.f;
		
		strTemp2.Format(_T("nLinkPwrCount:%d CellCode:%d ChState:%d StepNo:%d StepType:%d Pwr SetVolt:%f CurVolt:%f"),
						iSeq, 
						pChannel->GetCellCode(),
						pChannel->GetState(), 
						pChannel->GetStepNo(),
						pChannel->GetStepType(),
						fSet_Volt,
						fCur_Volt);
		pChannel->WritePwrSupplyLog(strTemp2);
	}
	//-------------------------------------
	if(pStep->m_nPwrSupply_State==ID_PWRSUPY_STAT_STOP) return;
    //-------------------------------------
	int iError=mainFrm->m_PwrSupplyCtrl[nItem].m_iError;
	if(iError>=5)
	{
		onPwrSbcChPause(pChannel,pStep,nItem,strLogType);
		return;
	}	
	//-------------------------------------
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	if(pStep->m_nPwrSupply_State==ID_PWRSUPY_STAT_ON)
	{//ON
		fCur_Volt=mainFrm->m_PwrSupplyCtrl[nItem].m_fState_Volt[nOutp-1];
		fSet_Volt=(float)pStep->m_nPwrSupply_Volt/1000.f;
	    fDev=fabs(fCur_Volt-fSet_Volt);

		strTemp.Format(_T("CHNO:%d_C StepNo:%d StepType:%d Pwr_No:%d Pwr_Cmd:%d Pwr_CurVolt:%f Pwr_SetVolt:%f Dev:%f"),
					   nCHNO,nStepNo,nStepType,nItem,
					   pStep->m_nPwrSupply_Cmd,
					   fCur_Volt,
					   fSet_Volt,
					   fDev);
		//-----------------------------------------------
		if(pStep->m_nPwrSupply_Log==2)
		{
			log_All(strLogType,_T("cmd"),strTemp);			
		}
		if(fDev<0.01)
		{	
			//pStep->m_nPwrSupply_State=ID_PWRSUPY_STAT_STOP;//Not Use
			pStep->m_nPwrSupply_Log=3;
			return;
		}
		else
		{
			log_All(strLogType,_T("cmd"),strTemp);
			pStep->m_nPwrSupply_Log=2;
		}
	}
	else if(pStep->m_nPwrSupply_State==ID_PWRSUPY_STAT_OFF)
	{//OFF
		fCur_Volt=mainFrm->m_PwrSupplyCtrl[nItem].m_fState_Volt[nOutp-1];
		fSet_Volt=0;

		strTemp.Format(_T("CHNO:%d_D StepNo:%d StepType:%d Pwr_No:%d Pwr_Cmd:%d Pwr_CurVolt:%f"),
						nCHNO,nStepNo,nStepType,nItem,
						pStep->m_nPwrSupply_Cmd,
						fCur_Volt);		
		//-----------------------------------------------
		if(pStep->m_nPwrSupply_Log==2)
		{
			log_All(strLogType,_T("cmd"),strTemp);		
		}
		if(fCur_Volt<0.01f)
		{
			//pStep->m_nPwrSupply_State=ID_PWRSUPY_STAT_STOP;
			pStep->m_nPwrSupply_Log=3;
			return;
		}
		else
		{
			log_All(strLogType,_T("cmd"),strTemp);
			pStep->m_nPwrSupply_Log=2;
		}
	}
	//-------------------------------------
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	int iCmdWaitTime=g_AppInfo.iPwrSupply_CmdWaitTime;
	if(pStep->m_nPwrSupply_Cmd==1)
	{//ON
		fCur_Volt=0;
		fSet_Volt=(float)pStep->m_nPwrSupply_Volt/1000.f;

		int nCmd_Count=1;
		if(mainFrm->m_PwrSupplyCtrl[nItem].m_iState_Outp==0)
		{
			strTemp.Format(_T("CHNO_%d,OUTP ON,OUTP?,"),nCHNO);
			mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(strTemp);
			nCmd_Count+=2;
		}
		if(mainFrm->m_PwrSupplyCtrl[nItem].m_iState_Inst!=nOutp)
		{
			strTemp.Format(_T("CHNO_%d,INST OUTP%d,INST?,"),nCHNO,nOutp);
			mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(strTemp);
			nCmd_Count+=2;
		}

		strTemp.Format(_T("CHNO_%d,VOLT %.2f,VOLT?,"),nCHNO,fSet_Volt);
		mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(strTemp);
		nCmd_Count+=2;

		pStep->m_nPwrSupply_State=ID_PWRSUPY_STAT_ON;//ON
		int nTime=iCmdWaitTime*nCmd_Count;
		Sleep(nTime);
		//------------------------------------------------------------
		fCur_Volt=mainFrm->m_PwrSupplyCtrl[nItem].m_fState_Volt[nOutp-1];		
		strTemp2.Format(_T("PwrCmd ON StepNo:%d StepType:%d SetVolt:%f CurVolt:%f"),
						nStepNo,
						nStepType, 
						fSet_Volt,
						fCur_Volt);		
		pChannel->WriteLog(strTemp2);
	}
	else if(pStep->m_nPwrSupply_Cmd==2)
	{//OFF
		fCur_Volt=0;
		fSet_Volt=0;

		int nCmd_Count=1;
		if(mainFrm->m_PwrSupplyCtrl[nItem].m_iState_Outp==0)
		{
			strTemp.Format(_T("CHNO_%d,OUTP ON,OUTP?,"),nCHNO);
			mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(strTemp);
			nCmd_Count+=2;
		}
		if(mainFrm->m_PwrSupplyCtrl[nItem].m_iState_Inst!=nOutp)
		{
			nCmd_Count++;
			strTemp.Format(_T("CHNO_%d,INST OUTP%d,INST?,"),nCHNO,nOutp);
			mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(strTemp);
			nCmd_Count+=2;
		}
		strTemp.Format(_T("CHNO_%d,VOLT 0.0,VOLT?,"),nCHNO);
		mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(strTemp);
		nCmd_Count+=2;

		pStep->m_nPwrSupply_State=ID_PWRSUPY_STAT_OFF;//OFF
		int nTime=iCmdWaitTime*nCmd_Count;
		Sleep(nTime);
		//------------------------------------------------------------
		fCur_Volt=mainFrm->m_PwrSupplyCtrl[nItem].m_fState_Volt[nOutp-1];		
		strTemp2.Format(_T("PwrCmd OFF StepNo:%d StepType:%d SetVolt:%f CurVolt:%f"),
						nStepNo,
						nStepType, 
						fSet_Volt,
						fCur_Volt);
		pChannel->WriteLog(strTemp2);
	}
	//CMiniDump::End(); //yulee 20190115
}

void MainCtrl::onChillerDealChannel(CCyclerChannel *pChannel)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return;
	if(!mainFrm)  return;
	
	int nState=pChannel->GetState();
	if(nState!=PS_STATE_RUN) return;

	if(!mainView) return;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return;

	int nModuleID=pChannel->GetModuleID();
	int nChannelIndex=pChannel->GetChannelIndex();
	CCyclerModule *lpModule= pDoc->GetModuleInfo(nModuleID); 
	if(!lpModule) return;
	
	CScheduleData *pSchedule=pChannel->GetScheduleData();
	if(!pSchedule) return;
	
	CString strTemp,strTemp2;
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	int nCycCurr=pChannel->GetCurCycleCount();
	int nOvenID=0;

	//--------------------------------------------
	int   i=0;
	BOOL  bflag=FALSE;
	CStep *pStep=NULL;
	int   nTotal=pSchedule->GetStepSize();
	for(int i = 0;i<nTotal;i++)
	{
		pStep=pSchedule->GetStepData(i);
        if(!pStep) continue;

		strTemp.Format(_T("CHNO:%d_A sch_no:%d chiller_cmd:%d state:%d seqno:%d stepno:%d / %d steptype:%d / %d cyccurr:%d"),
						nCHNO,i,
						pStep->m_nChiller_Cmd,
						pStep->m_nChiller_State,
						pStep->m_nChiller_SeqNo,
						(pStep->m_StepIndex+1),nStepNo,
						pStep->m_type,nStepType,nCycCurr);	
		
		if( pStep->m_nChiller_Cmd==1 &&
			(pStep->m_StepIndex+1)==nStepNo)
		{
			bflag=TRUE;
			break;
		}
	}
	if(bflag==FALSE) return;//NOT USE
	//--------------------------------------------
	int nItem=mainFrm->onGetSerialChillerNo(nCHNO);
	
	CString strLogType;
	strLogType.Format(_T("chiller_%d"),nCHNO);                                                        
	
	strTemp2.Format(_T("CHNO:%d_B StepNo:%d StepType:%d \
Chiller_No:%d \
Chiller_Cmd:%d \
RefTemp:%f \
RefCmd:%d \
RefPump:%f \
PumpCmd:%d \
TpData:%d \
ONTemp1:%f \
ONTemp1_Cmd:%d \
ONPump1_Cmd:%d \
ONTemp2:%f \
ONTemp2_Cmd:%d \
ONPump2_Cmd:%d \
OFFTemp1:%f \
OFFTemp1_Cmd:%d \
OFFPump1_Cmd:%d \
OFFTemp2:%f \
OFFTemp2_Cmd:%d \
OFFPump2_Cmd:%d"),
	nCHNO,nStepNo,nStepType,
	nItem,
	pStep->m_nChiller_Cmd,
	pStep->m_fChiller_RefTemp,
	pStep->m_nChiller_RefCmd,//1-Run,2-Stop  
	pStep->m_fChiller_RefPump,
	pStep->m_nChiller_PumpCmd,//1-Run,2-Stop  
	pStep->m_nChiller_TpData,//1-Aux,2-Can      
	pStep->m_fChiller_ONTemp1,
	pStep->m_nChiller_ONTemp1_Cmd,//1-ON 
	pStep->m_nChiller_ONPump1_Cmd,//1-ON 
	pStep->m_fChiller_ONTemp2,
	pStep->m_nChiller_ONTemp2_Cmd,//1-ON   
	pStep->m_nChiller_ONPump2_Cmd,//1-ON 
	pStep->m_fChiller_OFFTemp1,
	pStep->m_nChiller_OFFTemp1_Cmd,//1-ON   
	pStep->m_nChiller_OFFPump1_Cmd,//1-ON 
	pStep->m_fChiller_OFFTemp2,
	pStep->m_nChiller_OFFTemp2_Cmd,//1-ON
	pStep->m_nChiller_OFFPump2_Cmd//1-ON 
	);
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
    //---------------------------------
	if( m_iChiller_StepNo!=nStepNo ||
		m_iChiller_CycCurr!=nCycCurr)
	{
		pStep->m_nChiller_State=0;
		pStep->m_nChiller_Log=0;
		pStep->m_nChiller_Count=0;
		pStep->m_nChiller_SeqNo=0;		
		
		log_All(strLogType,_T("start"),_T("-------------------------"));
		log_All(strLogType,_T("state"),strTemp);
		log_All(strLogType,_T("cmd"),strTemp2);
	    pStep->m_nChiller_Log=2;
	}
	m_iChiller_StepNo=nStepNo;
	m_iChiller_CycCurr=nCycCurr;
	//---------------------------------
	if(nItem<0) return;
	//---------------------------------	
	pStep->m_nChiller_SeqNo++;
	if( (pStep->m_nChiller_SeqNo%50)==0)
	{//10sec Interval=>200msec Interval thread
		int   iSeq=pStep->m_nChiller_SeqNo/50;
		
		float fChillerRefTemp=pStep->m_fChiller_RefTemp;
		float fRefTemp1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRefTemperature(nOvenID);
		float fCurTemp1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetCurTemperature(nOvenID);
 
		strTemp2.Format(_T("nLinkChillerCount:%d CellCode:%d ChState:%d StepNo:%d StepType:%d Chiller CurrRefTemp:%f CurTemp:%f RefTemp:%f"),
						iSeq, 
						pChannel->GetCellCode(),
						pChannel->GetState(), 
						pChannel->GetStepNo(),
						pChannel->GetStepType(),
						fRefTemp1,
						fCurTemp1,
						fChillerRefTemp);
		pChannel->WriteChillerLog(strTemp2);

		int   iPumpRefTemp=pStep->m_fChiller_RefPump;
		int   iRefPump1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nSP;//REF
		int   iCurPump1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPV;//CUR
	
      strTemp2.Format(_T("nLinkPumpCount:%d CellCode:%d ChState:%d StepNo:%d StepType:%d Pump CurrRefTemp:%d CurTemp:%d RefTemp:%d"),
						iSeq, 
						pChannel->GetCellCode(),
						pChannel->GetState(), 
						pChannel->GetStepNo(),
						pChannel->GetStepType(),
						iRefPump1,
						iCurPump1,
						iPumpRefTemp);
		pChannel->WritePumpLog(strTemp2);
	}
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	//---------------------------------
	//strTemp.Format(_T("Chiler State : Chiller_stat_stop"));
	//log_All(strLogType,_T("state"),strTemp);	
	if(pStep->m_nChiller_State==ID_CHILLER_STAT_STOP) return;
	//---------------------------------
	float fChillerRefTemp =pStep->m_fChiller_RefTemp;
	int   iChillerRefCmd  =pStep->m_nChiller_RefCmd;//1-ON,2-OFF

	float fChillerONTemp1 =pStep->m_fChiller_ONTemp1;
	float fChillerONTemp2 =pStep->m_fChiller_ONTemp2;
	float fChillerOFFTemp1=pStep->m_fChiller_OFFTemp1;
	float fChillerOFFTemp2=pStep->m_fChiller_OFFTemp2;

	float fRefTemp1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRefTemperature(nOvenID);
	float fCurTemp1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetCurTemperature(nOvenID);

	int   iPumpRefTemp =pStep->m_fChiller_RefPump;
	int   iPumpRefCmd  =pStep->m_nChiller_PumpCmd;//1-ON,2-OFF

	int   iRefPump1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nSP;//REF
	int   iCurPump1=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPV;//CUR
	
	int   iTpData=pStep->m_nChiller_TpData;

	int   iONTemp1_Cmd =pStep->m_nChiller_ONTemp1_Cmd;//1-ON
	int   iONPump1_Cmd =pStep->m_nChiller_ONPump1_Cmd;//1-ON
	int   iONTemp2_Cmd =pStep->m_nChiller_ONTemp2_Cmd;//1-ON
	int   iONPump2_Cmd =pStep->m_nChiller_ONPump2_Cmd;//1-ON
	int   iOFFTemp1_Cmd=pStep->m_nChiller_OFFTemp1_Cmd;//1-ON
	int   iOFFPump1_Cmd=pStep->m_nChiller_OFFPump1_Cmd;//1-ON
	int   iOFFTemp2_Cmd=pStep->m_nChiller_OFFTemp2_Cmd;//1-ON
	int   iOFFPump2_Cmd=pStep->m_nChiller_OFFPump2_Cmd;//1-ON
	//---------------------------------
	if(pStep->m_nChiller_State==ID_CHILLER_STAT_NONE)
	{//Init ON/OFF Command
		
		//ksj 20200706 : 컨트롤러 모델 체크
		int nModelType = pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetOvenModelType();


        //Chiller Set---------------------------------

		//lyj 20201105 Chiller Temp/Pump 0값이면 셋안하도록 레지스트리 처리 default:TRUE s ========================
		m_bChiller_ZeroSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseChillerZeroSet", FALSE); //lyj 20201105 Chiller Temp/Pump 0값이면 셋안하도록 레지스트리 처리
		if(!m_bChiller_ZeroSet)
		{
			if(fChillerRefTemp == 0.0f)
			{
				//lyj 20201105 Chiller Temp/Pump 0값이면 셋안하도록 레지스트리 처리
			}
			else
			{
				if( fChillerRefTemp>=-30 && //0>=-30 Limit
					onChillerSendRefTemp(pChannel,nItem,nOvenID,fCurTemp1,fRefTemp1,fChillerRefTemp)==FALSE)
				{
					onChillerSbcChPause(pChannel,pStep,1,strLogType);
					return;
				}
			}
		}
		else
		{
			if( fChillerRefTemp>=-30 && //0>=-30 Limit
				onChillerSendRefTemp(pChannel,nItem,nOvenID,fCurTemp1,fRefTemp1,fChillerRefTemp)==FALSE)
			{
				onChillerSbcChPause(pChannel,pStep,1,strLogType);
				return;
			}
		}
		//lyj 20201105 Chiller Temp/Pump 0값이면 셋안하도록 레지스트리 처리 default:TRUE e ========================

// 		if( fChillerRefTemp>=-30 && //0>=-30 Limit
// 			onChillerSendRefTemp(pChannel,nItem,nOvenID,fCurTemp1,fRefTemp1,fChillerRefTemp)==FALSE)
// 		{
// 			onChillerSbcChPause(pChannel,pStep,1,strLogType);
// 			return;
// 		}
		
		if(nModelType == CHILLER_TEMP2520) 
		{
			//ksj 20200706 : TEMP2520 예외처리 추가.
			//현재 칠러 유량 제어시 소수점을 다 버리도록 되어있는데.
			//대전LGC에서 소수점 사용 요청이 있었음. 대전LGC 해당 칠러 TEMP2520 사용.
			//소수점 한자리까지 사용가능하도록 x10해서 처리하도록 한다.
			//전체 로직을 수정하기에는 다양한 칠러에 대해서 모두 검증이 불가능하므로,
			//TEMP2520에 대해서만 예외적으로 처리하도록 함

			//iPumpRefTemp = pStep->m_fChiller_RefPump*10.0f;
			iPumpRefTemp = pStep->m_fChiller_RefPump*10.0f + 0.5f; //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.
			if(iPumpRefTemp == 1) iPumpRefTemp = 0; //유량이 0.1이면 0으로 바꿔줌. 0.1 입력시 유량 0사용 가능하도록함.			

			//iRefPump1=(int)(pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP*10.0f);//REF //TEMP2520일때만 fSP 값 읽힘
			//iCurPump1=(int)(pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV*10.0f);//CUR //TEMP2520일때만 fPV 값 읽힘	
			iRefPump1=(int)(pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP*10.0f + 0.5f);//REF //TEMP2520일때만 fSP 값 읽힘 //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.
			iCurPump1=(int)(pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV*10.0f + 0.5f);//CUR //TEMP2520일때만 fPV 값 읽힘 //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.

			if( pStep->m_fChiller_RefPump > 0 && 
				onChillerSendRefPump(pChannel,nItem,nOvenID,iCurPump1,iRefPump1,iPumpRefTemp)==FALSE)
			{
				onChillerSbcChPause(pChannel,pStep,2,strLogType);
				return;
			}
			//Chiller Run/Stop--------------------------------------------
			if( iChillerRefCmd>0 && 
				onChillerSendRunTemp(pChannel,nItem,nOvenID,iChillerRefCmd,_T("start"))==FALSE)
			{
				onChillerSbcChPause(pChannel,pStep,10+iChillerRefCmd,strLogType);
				return;
			}
		}
		else //기존 처리 코드
		{
			//Pump Set---------------------------------
			//if( iPumpRefTemp>0 && //주석처리
			//ksj 20200326 : 유량 0 입력 가능하도록 조건 개선
			// 0.1또는 0.001 등의 입력으로 유량 0 제어 가능하도록 한다.
			//iPumpRefTemp 값은 소수점이 다 버려져서 0.1 0.001 등은 전부 0이랑 같아지므로 버려지기전의 pStep->m_fChiller_RefPump 조건으로 판단.	

//			m_bChiller_DESChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDESChiller", 0); //lyj 20201102 DES칠러 펌프 소숫점
// 			if(m_bChiller_DESChiller) //lyj 20201102 DES칠러
// 			{
			iPumpRefTemp = pStep->m_fChiller_RefPump*10.0f + 0.5f; //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.
			if(iPumpRefTemp == 1) iPumpRefTemp = 0; //유량이 0.1이면 0으로 바꿔줌. 0.1 입력시 유량 0사용 가능하도록함.	
			iRefPump1=(int)(pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP*10.0f + 0.5f);//REF //TEMP2520일때만 fSP 값 읽힘 //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.
			iCurPump1=(int)(pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV*10.0f + 0.5f);//CUR //TEMP2520일때만 fPV 값 읽힘 //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.			
			
// 			}
			if( pStep->m_fChiller_RefPump > 0 && 
				onChillerSendRefPump(pChannel,nItem,nOvenID,iCurPump1,iRefPump1,iPumpRefTemp)==FALSE)
			{
				onChillerSbcChPause(pChannel,pStep,2,strLogType);
				return;
			}
			//Chiller Run/Stop--------------------------------------------
			if( iChillerRefCmd>0 && 
				onChillerSendRunTemp(pChannel,nItem,nOvenID,iChillerRefCmd,_T("start"))==FALSE)
			{
				onChillerSbcChPause(pChannel,pStep,10+iChillerRefCmd,strLogType);
				return;
			}
		}
	
/*  //yulee 20190129 - pump run/stop동작은 온도 컨트롤러에 의해 칠러에서 자동으로 이루어짐. 
		//Pump Run/Stop--------------------------------------------
		if( iPumpRefCmd>0 &&
			onChillerSendRunPump(pChannel,nItem,nOvenID,iPumpRefCmd,_T("start"))==FALSE)
		{
			onChillerSbcChPause(pChannel,pStep,20+iPumpRefCmd,strLogType);
			return;
		}
*/
		//ksj 20200401 : 다시 펌프제어 추가. ST560은 직접 제어 해줘야함??	
		if(nModelType == CHILLER_ST590)
		{
 			strTemp.Format("iChillerRefCmd : %d step : %d", iChillerRefCmd,pStep->m_StepIndex+1);
 			log_All(strLogType,_T("cmd_pump"),strTemp);	
			//Pump Run/Stop--------------------------------------------

			//ST590 온도on 시 펌프도 on iChillerRefCmd
			if( iChillerRefCmd>0 &&
				onChillerSendRunPump(pChannel,nItem,nOvenID,iChillerRefCmd,_T("start"))==FALSE)
			{
				onChillerSbcChPause(pChannel,pStep,20+iChillerRefCmd,strLogType);
				return;
			}
		}
		//ksj end


		//--------------------------------------------
		pStep->m_nChiller_State=ID_CHILLER_STAT_START;
	}

	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	//---------------------------------
	BOOL    bFirst(TRUE);
	int     nTotMasterCan;
	double	dValue;
	double	dCellMaxValue=0;
	double	dCellMinValue=0;
	double	dCellDevValue=0;
	CString strItem;
    if (pChannel->IsParallel() && pChannel->m_bMaster == FALSE)
	{
		return;	//병렬채널
	}
	//------------------------------------
	int  nChiller_Count=0;
	BOOL bLog=TRUE;//yulee 20190603
	//------------------------------------
	if(iTpData==1)
	{
		//CAN---------------------------------------
		SFT_CAN_SET_DATA canData;
		int nCanCount = pChannel->GetCanCount();
		SFT_CAN_DATA *pCanData = pChannel->GetCanData();
		if(nCanCount>0 && pCanData)
		{
			for( int i = 0; i < lpModule->GetInstalledCanMaster(nChannelIndex); i++)
			{
				strItem=_T("");
				canData = lpModule->GetCANSetData(nChannelIndex, PS_CAN_TYPE_MASTER, i);
				
				if( pCanData[i].data_type == 0 || 
					pCanData[i].data_type == 1 || 
					pCanData[i].data_type == 2)
				{
					strItem.Format("%.6f", pCanData[i].canVal.fVal[0]);
					if (canData.function_division > 100 && canData.function_division < 150)
					{
						strItem.Format(_T("%02x%02x%02x%02x"), 
							(BYTE)pCanData[i].canVal.strVal[0], 
							(BYTE)pCanData[i].canVal.strVal[1], 
							(BYTE)pCanData[i].canVal.strVal[2], 
							(BYTE)pCanData[i].canVal.strVal[3]);
					}
					strItem.TrimRight("0");
					strItem.TrimRight(".");
				}
				else if(pCanData[i].data_type == 3)
				{
					strItem.Format("%s", pCanData[i].canVal.strVal);
				}
				else if (pCanData[i].data_type == 4)
				{
					strItem.Format("%x", (UINT)pCanData[i].canVal.fVal[0]);			
				}
				
				//이부분에 추가
				dValue = atof(strItem);//CAN Value
				if (strItem.IsEmpty()) continue;
				
				if (canData.function_division2 == ID_CHILLER_CAN_DIVISION2)	
				{
					nChiller_Count++;

					if (bFirst)
					{ 
						dCellMaxValue = dCellMinValue = dValue;
						bFirst = FALSE;
					}
					if (dCellMaxValue < dValue) dCellMaxValue = dValue;
					if (dCellMinValue > dValue) dCellMinValue = dValue;
					//------------------------------
					if(bLog==TRUE)
					{
						strTemp.Format(_T("%s(%d):%f min:%f max:%f"),
							canData.name,nChiller_Count,dValue,
							dCellMinValue,dCellMaxValue);
						log_All(strLogType,_T("can_m"),strTemp);
					}					
				}
			}			
			nTotMasterCan = lpModule->GetInstalledCanMaster(nChannelIndex);
			for(int i = 0; i < lpModule->GetInstalledCanSlave(nChannelIndex); i++)
			{
				strItem=_T("");
				canData = lpModule->GetCANSetData(nChannelIndex, PS_CAN_TYPE_SLAVE, i);
				
				if( pCanData[i+nTotMasterCan].data_type == 0 || 
					pCanData[i+nTotMasterCan].data_type == 1 || 
					pCanData[i+nTotMasterCan].data_type == 2 || 
					pCanData[i].data_type == 4)
				{
					strItem.Format(_T("%.6f"), pCanData[i+nTotMasterCan].canVal.fVal[0]);
					strItem.TrimRight("0");	
					strItem.TrimRight(".");
				}
				else if(pCanData[i+nTotMasterCan].data_type == 3)
				{
					strItem.Format(_T("%s"), pCanData[i+nTotMasterCan].canVal.strVal);
				}
				else if (pCanData[i].data_type == 4)
				{
					strItem.Format(_T("%x"), (UINT)pCanData[i].canVal.fVal[0]);
				}
				
				//이부분에 추가
				dValue = atof(strItem);//CAN Value
				if (strItem.IsEmpty()) continue;
				
				if (canData.function_division2 == ID_CHILLER_CAN_DIVISION2)
				{
					nChiller_Count++;

					if (bFirst)
					{ 
						dCellMaxValue = dCellMinValue = dValue;
						bFirst = FALSE;
					}
					if (dCellMaxValue < dValue) dCellMaxValue = dValue;
					if (dCellMinValue > dValue) dCellMinValue = dValue;
					//------------------------------
					if(bLog==TRUE)
					{
						strTemp.Format(_T("%s(%d):%f min:%f max:%f"),
										canData.name,nChiller_Count,dValue,
										dCellMinValue,dCellMaxValue);
						log_All(strLogType,_T("can_s"),strTemp);
					}
				}
			}
		}
	}
	else
	{
		//AUX---------------------------------------
		int nAuxCount = pChannel->GetAuxCount();
		SFT_AUX_DATA * pAuxData = pChannel->GetAuxData();
		if( nAuxCount>0 && pAuxData)
		{
			for(int i=0;i<nAuxCount;i++)
			{
				CChannelSensor * pSensor = lpModule->GetAuxData(pAuxData[i].auxChNo-1, pAuxData[i].auxChType);
				if(pSensor == NULL) continue;
				STF_MD_AUX_SET_DATA AuxSetData = pSensor->GetAuxData();
				
				int   chNo   = pAuxData[i].auxChNo;
				int   chType = pAuxData[i].auxChType;
				long  lValue = pAuxData[i].lValue;
				CString sName=(LPSTR)(LPCTSTR)pSensor->GetAuxName();
				
				switch(pAuxData[i].auxChType)
				{
					case 0: strItem.Format("T%d",pAuxData[i].auxChNo) ;	break;
					case 1: strItem.Format("V%d",pAuxData[i].auxChNo) ;	break;
					//	case 2: strItem.Format("TH%d",pAuxData[i].auxChNo);	break;
					case 2: strItem.Format("TH%d",pAuxData[i].auxChNo - lpModule->GetMaxVoltage());	break;
				}
				dValue = pDoc->UnitTrans( pAuxData[i].lValue, FALSE, pAuxData[i].auxChType);
	     		strItem.Format("%0.3f", dValue);

				if (AuxSetData.funtion_division2 == ID_CHILLER_AUX_DIVISION2)	
				{
					nChiller_Count++;

					if (bFirst)
					{ 
						dCellMaxValue = dCellMinValue = dValue;
						bFirst = FALSE;
					}
					if (dCellMaxValue < dValue) dCellMaxValue = dValue;
					if (dCellMinValue > dValue) dCellMinValue = dValue;
					//------------------------------
					if(bLog==TRUE)
					{
						strTemp.Format(_T("%s(%d):%f min:%f max:%f"),
										pSensor->GetAuxName(),nChiller_Count,dValue,
										dCellMinValue,dCellMinValue);
						log_All(strLogType,_T("aux"),strTemp);
					}
				}
			}
		}
	}
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	pStep->m_nChiller_Count=nChiller_Count;
	if(nChiller_Count<1)
	{
		pStep->m_nChiller_State=ID_CHILLER_STAT_STOP;

		strTemp.Format(_T("CH:%d %s ondo chiller_count(div2:%d):%d"),
		               nCHNO,
					   ((iTpData==1)?_T("can"):_T("aux")),
					   ((iTpData==1)?ID_CHILLER_CAN_DIVISION2:ID_CHILLER_AUX_DIVISION2),
					   nChiller_Count);
		log_All(strLogType,_T("state"),strTemp);
		return;
	}
    //---------------------------------------------
	if (dCellMaxValue == 0 && dCellMinValue == 0) return;
	dCellDevValue = dCellMaxValue - dCellMinValue;
	//---------------------------------------------	
	BOOL bON=FALSE;	
	if(iONTemp1_Cmd==1 || iONTemp2_Cmd==1)
	{
		if(iONTemp1_Cmd==1 && (dCellMaxValue >= fChillerONTemp1)) bON=TRUE;	
		if(iONTemp2_Cmd==1 && (dCellDevValue >= fChillerONTemp2)) bON=TRUE;

		strTemp.Format(_T("CH:%d chiler On:%d(satisfied:1) tempmax(cmd:%d):%.5f/%.5f//tempdev(cmd:%d):%.5f/%.5f"),
			nCHNO,
			bON,
			iONTemp1_Cmd,dCellMaxValue,fChillerONTemp1, iONTemp2_Cmd,dCellDevValue,fChillerONTemp2);

		log_All(strLogType,_T("state"),strTemp);

		if(bON==TRUE)
		{
			if(onChillerSendRunTemp(pChannel,nItem,nOvenID,1,_T("run"))==FALSE)
			{//ON
				onChillerSbcChPause(pChannel,pStep,11,strLogType);
				return;
			}
			pStep->m_nChiller_State=ID_CHILLER_STAT_ON;
		}
	}
// 	if(iONPump1_Cmd==1 || iONPump2_Cmd==1)
// 	{			
// 		if(iONTemp1_Cmd==1 && (dCellMaxValue >= fChillerONTemp1)) bON=TRUE;	
// 		if(iONPump2_Cmd==1 && (dCellDevValue >= fChillerONTemp2)) bON=TRUE;
// 
// 		strTemp.Format(_T("CH:%d pump On:%d tempmax(cmd:%d):%f/%f tempdev(cmd:%d):%f/%f"),
// 		               nCHNO,
// 					   bON,
// 					   iONPump1_Cmd,dCellMaxValue,fChillerONTemp1,
// 					   iONPump2_Cmd,dCellDevValue,fChillerONTemp2);
// 		log_All(strLogType,_T("state"),strTemp);
// 
// 		if(bON==TRUE)
// 		{
// 			if(onChillerSendRunPump(pChannel,nItem,nOvenID,1,_T("run"))==FALSE)
// 			{//ON
// 				onChillerSbcChPause(pChannel,pStep,12,strLogType);
// 				return;
// 			}
// 			pStep->m_nChiller_State=ID_CHILLER_STAT_ON;
// 		}
// 	}

	//ksj 20200406 : 다시 펌프제어 추가. ST560은 직접 제어 해줘야함??
	if(iONPump1_Cmd==1 || iONPump2_Cmd==1)
	{			
		if(iONTemp1_Cmd==1 && (dCellMaxValue >= fChillerONTemp1)) bON=TRUE;	
		if(iONPump2_Cmd==1 && (dCellDevValue >= fChillerONTemp2)) bON=TRUE;
	
		strTemp.Format(_T("CH:%d pump On:%d tempmax(cmd:%d):%f/%f tempdev(cmd:%d):%f/%f"),
		               nCHNO,
					   bON,
					   iONPump1_Cmd,dCellMaxValue,fChillerONTemp1,
				   iONPump2_Cmd,dCellDevValue,fChillerONTemp2);
		 		log_All(strLogType,_T("state"),strTemp);
	 
	 	if(bON==TRUE)
	 	{
	 		/*if(onChillerSendRunPump(pChannel,nItem,nOvenID,1,_T("run"))==FALSE)
	 		{//ON
	 			onChillerSbcChPause(pChannel,pStep,12,strLogType);
	 			return;
	 		}
	 		pStep->m_nChiller_State=ID_CHILLER_STAT_ON;*/

			int nModelTyp = pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetOvenModelType();

			if(nModelTyp == CHILLER_ST590) //온도는590 펌프는 ST560
			{
				if(onChillerSendRunPump(pChannel,nItem,nOvenID,1,_T("run"))==FALSE)
				{//ON
					onChillerSbcChPause(pChannel,pStep,12,strLogType);
					return;
				}
				pStep->m_nChiller_State=ID_CHILLER_STAT_ON;
			}
	 	}
	}
	//ksj end
	//---------------------------------------------
	BOOL bOFF=FALSE;	
	if(iOFFTemp1_Cmd==1 || iOFFTemp2_Cmd==1)
	{
		if(iOFFTemp1_Cmd==1 && (dCellMaxValue < fChillerOFFTemp1)) bOFF=TRUE;//yulee 20190603
		if(iOFFTemp2_Cmd==1 && (dCellDevValue < fChillerOFFTemp2)) bOFF=TRUE;//yulee 20190603
	
		strTemp.Format(_T("CH:%d chiler Off:%d(satisfied:1) tempmax(cmd:%d):%f/%f tempdev(cmd:%d):%f/%f"), //yulee 20190603
		               nCHNO,
					   bOFF,
					   iOFFTemp1_Cmd,dCellMaxValue,fChillerOFFTemp1,
					   iOFFTemp2_Cmd,dCellDevValue,fChillerOFFTemp2);
		log_All(strLogType,_T("state"),strTemp);

		if(bOFF==TRUE)
		{
			if(onChillerSendRunTemp(pChannel,nItem,nOvenID,2,_T("stop"))==FALSE)
			{//OFF
				onChillerSbcChPause(pChannel,pStep,11,strLogType);
				return;
			}
			pStep->m_nChiller_State=ID_CHILLER_STAT_OFF;
		}
	}
// 	if(iOFFPump1_Cmd==1 || iOFFPump2_Cmd==1)
// 	{
// 		if(iOFFPump1_Cmd==1 && (dCellMinValue < fChillerOFFTemp1)) bOFF=TRUE;
// 		if(iOFFPump2_Cmd==1 && (dCellDevValue < fChillerOFFTemp2)) bOFF=TRUE;
// 
// 		strTemp.Format(_T("CH:%d pump Off:%d tempmax(cmd:%d):%f/%f tempdev(cmd:%d):%f/%f"),
// 		               nCHNO,
// 					   bOFF,
// 					   iOFFPump1_Cmd,dCellMaxValue,fChillerOFFTemp1,
// 					   iOFFPump2_Cmd,dCellDevValue,fChillerOFFTemp2);
// 		log_All(strLogType,_T("state"),strTemp);
// 
// 		if(bOFF==TRUE)
// 		{
// 			if(onChillerSendRunPump(pChannel,nItem,nOvenID,2,_T("stop"))==FALSE)
// 			{//OFF
// 				onChillerSbcChPause(pChannel,pStep,12,strLogType);
// 				return;
// 			}
// 			pStep->m_nChiller_State=ID_CHILLER_STAT_OFF;
// 		}
// 	}	


	//ksj 20200406 : 다시 펌프제어 추가. ST560은 직접 제어 해줘야함??
	if(iOFFPump1_Cmd==1 || iOFFPump2_Cmd==1)
	{
		if(iOFFPump1_Cmd==1 && (dCellMinValue < fChillerOFFTemp1)) bOFF=TRUE;
		if(iOFFPump2_Cmd==1 && (dCellDevValue < fChillerOFFTemp2)) bOFF=TRUE;
	 
		strTemp.Format(_T("CH:%d pump Off:%d tempmax(cmd:%d):%f/%f tempdev(cmd:%d):%f/%f"),
		               nCHNO,
					   bOFF,
					   iOFFPump1_Cmd,dCellMaxValue,fChillerOFFTemp1,
					   iOFFPump2_Cmd,dCellDevValue,fChillerOFFTemp2);
		log_All(strLogType,_T("state"),strTemp);
	 
		if(bOFF==TRUE)
		{
			/*if(onChillerSendRunPump(pChannel,nItem,nOvenID,2,_T("stop"))==FALSE)
			{//OFF
				onChillerSbcChPause(pChannel,pStep,12,strLogType);
				return;
			}
			pStep->m_nChiller_State=ID_CHILLER_STAT_OFF;*/

			int nModelTyp = pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetOvenModelType();

			if(nModelTyp == CHILLER_ST590) //온도는590 펌프는 ST560
			{
				if(onChillerSendRunPump(pChannel,nItem,nOvenID,2,_T("stop"))==FALSE)
				{//OFF
					onChillerSbcChPause(pChannel,pStep,12,strLogType);
					return;
				}
				pStep->m_nChiller_State=ID_CHILLER_STAT_OFF;
			}
		}
	}
	//ksj end

	//CMiniDump::End(); //yulee 20190115
}

BOOL MainCtrl::onChillerSbcChPause(CCyclerChannel *pChannel,CStep *pStep,int iType,CString strLogType)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return FALSE;

	if(!mainView) return FALSE;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc) return FALSE;

	int nModuleID=pChannel->GetModuleID();
	int nChannelIndex=pChannel->GetChannelIndex();

	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	int nCHNO=pChannel->m_iCh;

    CString strTemp;
	BOOL bRet=mainView->onSbcPause(nModuleID,nChannelIndex);
	if(bRet==TRUE)
	{
		pStep->m_nChiller_State=ID_CHILLER_STAT_STOP;

		CString strTitle;
		strTitle.Format(_T("CH:%d Warn Happen"),nCHNO);

		CString strMessage;
		if(iType==1)  strMessage=_T("FAILED : Set Temp ref SP(Temp.)");
		if(iType==11) strMessage=_T("FAILED : Chiller RUN");
		if(iType==12) strMessage=_T("FAILED : Chiller STOP");

		if(iType==2)  strMessage=_T("FAILED : Set pump ref SP(LPM)");
		if(iType==21) strMessage=_T("FAILED : Pump RUN");
		if(iType==22) strMessage=_T("FAILED : Pump STOP");

		strTemp.Format(_T("CHNO:%d_C StepNo:%d StepType:%d Error : %s"),
					   nCHNO,nStepNo,nStepType,
					   strMessage);
		log_All(strLogType,_T("error"),strTemp);

		mainView->onShowMessage(1,strTitle,strMessage);
	}
	//CMiniDump::End(); //yulee 20190115
	return bRet;
}

BOOL MainCtrl::onChillerSendRefTemp(CCyclerChannel *pChannel,int nItem,int nOvenID,
									float fCurTemp1,float fRefTemp1,float fChillerRefTemp)
{//Chiller Set	
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return FALSE;

	if(!mainView) return FALSE;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc) return FALSE;

	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();

	CString strLogType;
	strLogType.Format(_T("chiller_%d"),nCHNO);

	BOOL bflag=FALSE;
	BOOL bRet=FALSE;
	int	nRet2=0;
	int nCount=0;
	float fRefTemp2=0;
	float fCurTemp2=0;
	CString strTemp,strTemp2;

	float fltDev = fRefTemp1 - fChillerRefTemp; //fRefTemp1 현재온도 , fChillerRefTemp sch 설정 온도
	if((fabs(fltDev)) < (float)0.1)
	{
		strTemp.Format(_T("CH:%d RefTemp1:%f SetRefTemp:%f"),
						nCHNO,
						fRefTemp1,
						fChillerRefTemp);
		log_All(strLogType,_T("cmd_chill"),strTemp);	
		return TRUE;
	}

	pDoc->m_chillerCtrl[nItem].m_iReqStop=1;	
	int nRetry = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "nChillerRetryNum", 10); //yulee 20190724
	//while(nCount<10)
	while(nCount<nRetry)
	{

		bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.SetTemperature(0,nOvenID,fChillerRefTemp);
		//Sleep(1000);
		Wait(1000); //ksj 20201016
		nRet2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.RequestCurrentValue(0,nOvenID);
		
		fRefTemp2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRefTemperature(nOvenID);
		fCurTemp2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetCurTemperature(nOvenID);
		//--------------------------------------------------------
		strTemp.Format(_T("CH:%d count:%d item:%d chiller Ret:%d Ret2:%d CurTemp1:%f RefTemp1:%f SetRefTemp:%f CurTemp2:%f RefTemp2:%f"),
						nCHNO,nCount+1,nItem,bRet,nRet2,
						fCurTemp1,fRefTemp1,
						fChillerRefTemp,
						fCurTemp2,fRefTemp2);
		log_All(strLogType,_T("cmd_chill"),strTemp);		
		//--------------------------------------------------------
		if(fRefTemp2==fChillerRefTemp )
		{
			bflag=TRUE;
			break;
		}
		//---------------------------------------
		if( pChannel->GetState()!=PS_STATE_RUN )
		{
			break;
		}
		nCount++;

		//Sleep(300); //ksj 20200615 : Delay 추가. 의도적으로 엇박자 발생시킴. (주기적인 리퀘스트와 타이밍 어긋 나도록)
		Wait(300); //ksj 20201016
	}
	pDoc->m_chillerCtrl[nItem].m_iReqStop=0;  
	//--------------------------------------------------------
	strTemp2.Format(_T("Chiller bret:%d StepNo:%d StepType:%d CurrRefTemp:%f CurTemp:%f RefTemp:%f Retry:%d"),
					bflag,
					nStepNo,
					nStepType,
					fRefTemp2,fCurTemp2,
					fChillerRefTemp,
					nCount);
	pChannel->WriteLog(strTemp2);
	//--------------------------------------------------------		
	//CMiniDump::End(); //yulee 20190115
	return bflag;
}

BOOL MainCtrl::onChillerSendRefPump(CCyclerChannel *pChannel,int nItem,int nOvenID,
									int iCurPump1,int iRefPump1,int iPumpRefTemp)
{//Pump Set
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return FALSE;
	
	if(!mainView) return FALSE;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc) return FALSE;

	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	
	CString strLogType;
	strLogType.Format(_T("chiller_%d"),nCHNO);

	BOOL bflag=FALSE;
	BOOL bRet=FALSE;
	BOOL bRet2=FALSE; //ksj 20200616
	int  nCount=0;
	int  iRefPump2=0;
	int  iCurPump2=0;
	CString strTemp,strTemp2;

	float fltDev = iRefPump1-iPumpRefTemp;
	if(fabs(fltDev) < (float)0.1)
	{
		//strTemp.Format(_T("CH:%d RefTemp1:%f SetRefTemp:%f"),
		strTemp.Format(_T("CH:%d RefTemp1:%d SetRefTemp:%d"),
						nCHNO,
						iRefPump1,
						iPumpRefTemp);
		log_All(strLogType,_T("cmd_pump"),strTemp);	
		return TRUE;
	}

	pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_iPumpReqStop=1;
	int nRetry = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "nChillerRetryNum", 10); //yulee 20190724
	//int m_iChiller_DESChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDESChiller", 0); //lyj 20200130 DES chiller

	//lyj 20201102 DES칠러 윗단에서 처리 주석 s ==============
// 	m_iChiller_DESChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDESChiller", 0); //lyj 20200210 DES chiller //ksj20200401 : ST560도 1로 켜줘야됨. 대전LGC
// 
// 	if(m_iChiller_DESChiller) //lyj 20200130 DES chiller
// 	{
// 		iPumpRefTemp = iPumpRefTemp * 10;
// 	}
	//lyj 20201102 DES칠러 윗단에서 처리 주석 e ==============

	//while(nCount<10)
	while(nCount<nRetry)
	{
		/*pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmdAdd(ID_PUMP_CMD_SETSP1,iPumpRefTemp);
		Sleep(1000);


		if(pDoc->m_chillerCtrl->m_ctrlOven.GetOvenModelType() == CHILLER_TEMP2520)
		{
			bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.SetTemperature(0,nOvenID,iPumpRefTemp, 1);
		}
		else
		{
			//pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmd(ID_PUMP_CMD_READSTATE);//GETSTATE
			bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmd(ID_PUMP_CMD_READSTATE);//GETSTATE //ksj 20200615 : 리턴 값 저장하도록 수정
		}*/

		//ksj 20200616 : 로직 변경. 상기 코드는 펌프 값을 못 읽음.
		if(pDoc->m_chillerCtrl->m_ctrlOven.GetOvenModelType() == CHILLER_TEMP2520)
		{			
			//bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.SetTemperature(0,nOvenID,iPumpRefTemp, 1);
			bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.SetTemperature(0,nOvenID,(float)iPumpRefTemp/10.0f, 1); //ksj 20200706
 			//Sleep(1000); //delay 추가
			Wait(1000); //ksj 20201016
 			bRet2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.RequestCurrentValue(0,nOvenID); //칠러 온도처럼, 펌프도 강제 읽기 코드 추가.

// 			iRefPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP*10.0f;//REF //TEMP2520 일때만 fSP에 값 들어옴
// 			iCurPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV*10.0f;//CUR //TEMP2520 일때만 fPV에 값 들어옴

			iRefPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP*10.0f + 0.5f;//REF //TEMP2520 일때만 fSP에 값 들어옴 //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.
			iCurPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV*10.0f + 0.5f;//CUR //TEMP2520 일때만 fPV에 값 들어옴 //ksj 20200723 : float->int 변환시 소수점 아래 값 오차 보정.
		}
		else
		{
			//bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmdAdd(ID_PUMP_CMD_SETSP1,iPumpRefTemp);			 //ksj 20200616 : 주석처리. 문제 생기면 롤백 필요
			//bRet2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmd(ID_PUMP_CMD_READSTATE);//GETSTATE	         //ksj 20200616 : 주석처리. 문제 생기면 롤백 필요

			//ksj 20200616 : 위에 주석처리후 동기처리 코드로 변경. 문제 있으면 위에걸로 주석 해제.
			bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmd(ID_PUMP_CMD_SETSP1,iPumpRefTemp);	
			//Sleep(1000); //delay 추가
			Wait(1000); //ksj 20201016
			bRet2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmd(ID_PUMP_CMD_READSTATE);//GETSTATE		

// 			//lyj 20201102 DES칠러 s ===================
 			m_bChiller_DESChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDESChiller", 0);
 			if(m_bChiller_DESChiller)
			{
				iRefPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP; //lyj 20201207
				iCurPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV; //lyj 20201207
			}
			else //lyj 20201208
			{
				if(iPumpRefTemp != 0) //lyj 20201217
				{
					iPumpRefTemp = iPumpRefTemp/10;
					iPumpRefTemp = iPumpRefTemp*10;
				}
				iRefPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP*10.0f + 0.5f;
				iCurPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV*10.0f + 0.5f;
			}
// 			else{
// 				iRefPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nSP;//REF
// 				iCurPump2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPV;//CUR			
// 			}
			//lyj 20201102 DES칠러 e ===================
		}		
		//ksj end
		
		//--------------------------------------------------------	
		/*strTemp.Format(_T("CH:%d count:%d item:%d pump CurPump1:%d RefPump1:%d SetRefPump:%d CurPump2:%d RefPump2:%d"),
						nCHNO,nCount+1,nItem,
						iCurPump1,iRefPump1,
						iPumpRefTemp,
						iCurPump2,iRefPump2);*/

		//ksj 20200615 : 설정 결과 로그 추가. (bRet)
		strTemp.Format(_T("CH:%d count:%d item:%d bRet:%d bRet2:%d pump CurPump1:%d RefPump1:%d SetRefPump:%d CurPump2:%d RefPump2:%d"),
						nCHNO,nCount+1,nItem, bRet, bRet2,
						iCurPump1,iRefPump1,
						iPumpRefTemp,
						iCurPump2,iRefPump2);
		log_All(strLogType,_T("cmd_pump"),strTemp);
		//--------------------------------------------------------	
		if(iRefPump2==iPumpRefTemp )
		{
			bflag=TRUE;
			break;
		}	
	
		//---------------------------------------
		if( pChannel->GetState()!=PS_STATE_RUN )
		{
			break;
		}
		nCount++;

		//Sleep(300); //ksj 20200615 : Delay 추가. 의도적으로 엇박자 발생시킴. (주기적인 리퀘스트와 타이밍 어긋 나도록)
		Wait(300); //ksj 20201016
	}
	pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_iPumpReqStop=0;
	//--------------------------------------------------------	
	strTemp2.Format(_T("Pump bret:%d StepNo:%d StepType:%d CurrRefTemp:%d CurTemp:%d RefTemp:%d Retry:%d"),
		            bflag,
					nStepNo,
 					nStepType,
					iRefPump2,iCurPump2,
					iPumpRefTemp,
					nCount
					);
	pChannel->WriteLog(strTemp2);
	//--------------------------------------------------------
	//CMiniDump::End(); //yulee 20190115
	return bflag;
}

BOOL MainCtrl::onChillerSendRunTemp(CCyclerChannel *pChannel,int nItem,int nOvenID,
									int iChillerRefCmd,CString strType)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return FALSE;

	if(!mainView) return FALSE;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc) return FALSE;
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	
	CString strLogType;
	strLogType.Format(_T("chiller_%d"),nCHNO);
	
	CString strTemp,strTemp2;
	BOOL bRet=FALSE;
	int  nRet2=0;
	int  nCount=0;
    BOOL bRun=FALSE;
	BOOL bflag=FALSE;
	
	if( iChillerRefCmd==1)
	{//ON
		//-----------------------------------------
		//yulee 20190208

		//Sleep(100);//yulee 20190207
		Wait(100); //ksj 20201016
			//1. Fix Mode 변환
		if(pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRunWorkMode(nOvenID) == FALSE)
		{
			strTemp = "Present : Pattern Mode";
			log_All(strLogType,_T("state"),strTemp);
			if (pDoc->m_chillerCtrl[nItem].m_ctrlOven.SetFixMode(0,nOvenID,TRUE) == FALSE)
			{
				//strTemp = "칠러 FIX 모드로 변환 실패!!!";
				strTemp = "Fail : change to Fix Mode!!!";
				log_All(strLogType,_T("cmd_chil"),strTemp);	
			}
			else
			{
				//strTemp = "칠러 FIX 모드로 변환 성공!!!";
				strTemp = "Success : change to Fix Mode!!!";
				log_All(strLogType,_T("cmd_chil"),strTemp);	
			}
		}
		else
		{
			strTemp = "Present : Fix Mode";
			log_All(strLogType,_T("state"),strTemp);
		}
		//yulee 20190207

		bRun=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRun(nOvenID);
		if(bRun==TRUE)
		{
			strTemp.Format(_T("CH:%d chiller run(Present)"),nCHNO);
		    log_All(strLogType,_T("state"),strTemp);		

			pDoc->m_chillerCtrl[nItem].m_nChillerPlay=1;//ON
			return TRUE;
		}
		//-----------------------------------------
		pDoc->m_chillerCtrl[nItem].m_iReqStop=1;
		int nRetry = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "nChillerRetryNum", 10); //yulee 20190724
		//while(nCount<10)
		while(nCount<nRetry)
		{
			//yulee 20190129 run or stop?
			if (pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRun(nOvenID))
			{
				//if run -> send stop
				if(onChillerSendRunTemp(pChannel,nItem,nOvenID,2,_T("stop"))==FALSE)
				{
					break;
				}
				//Sleep(1500);
				Wait(1500); //ksj 20201016
			}

			bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.SendRunCmd(0, nOvenID);
			//Sleep(1000);
			Wait(1000); //ksj 20201016
			nRet2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.RequestCurrentValue(0,nOvenID);
			//-----------------------------------------
			bRun=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRun(nOvenID);
			if(bRun==TRUE)
			{
				pDoc->m_chillerCtrl[nItem].m_nChillerPlay=1;//ON
			}
			//-----------------------------------------
			strTemp.Format(_T("CH:%d count:%d item:%d chiller_cmd:ON Ret(Cmd):%d Ret2(Stat):%d runstat:%d type:%s"),
				           nCHNO,nCount+1,nItem,
						   bRet,nRet2,
						   bRun,strType);
			log_All(strLogType,_T("cmd_chil"),strTemp);		

			if(nRet2 != 1)
			{
				OnChillerCommErrorLog(nRet2, nCHNO);
			}

			//-----------------------------------------
			if(bRun==TRUE)
			{//ON
				bflag=TRUE;
				break;
			}
			//---------------------------------------
			if( pChannel->GetState()!=PS_STATE_RUN )
			{
				break;
			}
			nCount++;
			//Sleep(300); //lyj 20200625 retry 관련 delay 300 추가
			Wait(300); //ksj 20201016
		}
		pDoc->m_chillerCtrl[nItem].m_iReqStop=0;
		//-----------------------------------------
		strTemp2.Format(_T("Chiller ON bret:%d StepNo:%d StepType:%d retry:%d"),
			                bflag,
							nStepNo,
 							nStepType,
							nCount
							);
		pChannel->WriteLog(strTemp2);
		//-----------------------------------------
		return bflag;
	}
	else if( iChillerRefCmd==2 || //yulee 20190129 && -> || 
		     iChillerRefCmd==12)
	{//OFF
		//-----------------------------------------
		bRun=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRun(nOvenID);
		if(bRun==FALSE)
		{
			strTemp.Format(_T("CH:%d chiller stop"),nCHNO);
		    log_All(strLogType,_T("cmd_chil"),strTemp);	

			pDoc->m_chillerCtrl[nItem].m_nChillerPlay=2;//OFF
			return TRUE;
		}
		//-----------------------------------------
		pDoc->m_chillerCtrl[nItem].m_iReqStop=1;
		int nRetry = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "nChillerRetryNum", 10); //yulee 20190724
		//while(nCount<10)
		while(nCount<nRetry)
		{
			bRet=pDoc->m_chillerCtrl[nItem].m_ctrlOven.SendStopCmd(0, nOvenID);
			//Sleep(1000);
			Wait(1000); //ksj 20201016
			nRet2=pDoc->m_chillerCtrl[nItem].m_ctrlOven.RequestCurrentValue(0,nOvenID);
			//-----------------------------------------
			bRun=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRun(nOvenID);
			if(bRun==FALSE)
			{
				pDoc->m_chillerCtrl[nItem].m_nChillerPlay=2;//OFF
			}
			//-----------------------------------------
			strTemp.Format(_T("CH:%d count:%d item:%d chiller_cmd:OFF Ret(Cmd):%d Ret2(Stat):%d runstat:%d type:%s"),
							nCHNO,nCount+1,nItem,
							bRet,nRet2,
							bRun,strType);
			log_All(strLogType,_T("cmd_chil"),strTemp);

			if(nRet2 != 1)
			{
				OnChillerCommErrorLog(nRet2, nCHNO);
			}

			//-----------------------------------------
			if(bRun==FALSE)
			{//OFF
				bflag=TRUE;
				break;
			}
			//---------------------------------------
			if( pChannel->GetState()!=PS_STATE_RUN )
			{
				break;
			}
			nCount++;
			//OneTime-----------------------------------------
			if(iChillerRefCmd==12) break;
		}
		pDoc->m_chillerCtrl[nItem].m_iReqStop=0;
		//-----------------------------------------			
		strTemp2.Format(_T("Chiller OFF bret:%d StepNo:%d StepType:%d retry:%d"),
			            bflag,
						nStepNo,
						nStepType,
						nCount);
		pChannel->WriteLog(strTemp2);
		//-----------------------------------------
		return bflag;
	}
	//CMiniDump::End(); //yulee 20190115
	return TRUE;
}

BOOL MainCtrl::OnChillerCommErrorLog(int nErrNo, int nChNo)
{
	CString strLogType, strTemp;
	strLogType.Format(_T("chiller_%d"),nChNo);
	switch(nErrNo)
	{
	case 0:{strTemp.Format(_T("RequestCurrentValue:Fail"));
			break;}
	case 3:{strTemp.Format(_T("RequestCurrentValue:Port closed"));
		break;}
	case 4:{strTemp.Format(_T("RequestCurrentValue:Disconnected count >= 5"));
		break;}
	case 5:{strTemp.Format(_T("RequestCurrentValue:WriteToPort:Fail"));
		break;}
	default:{strTemp.Format(_T("N/A"));
			break;
		}
	}
	log_All(strLogType,_T("state"),strTemp);
	return TRUE;
}

BOOL MainCtrl::onChillerSendRunPump(CCyclerChannel *pChannel,int nItem,int nOvenID,
						            int iPumpRefCmd,CString strType)
{

	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return FALSE;

	if(!mainView) return FALSE;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc) return FALSE;
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	
	CString strLogType;
	strLogType.Format(_T("chiller_%d"),nCHNO);       

	CString strTemp,strTemp2;
	int     nCount=0;
    BOOL    bRun=FALSE;
    int     nMode=-1;
	int     nPlay=-1;
	BOOL    bflag=FALSE;

	if( iPumpRefCmd==1)
	{//ON
		//--------------------------------------------------
		nMode=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nMode;//0-Auto,1-Manual
		nPlay=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPlay;//0-Run,1-Stop
		if( nMode==0 && 
			nPlay==0)
		{//ON
			strTemp.Format(_T("CH:%d pump run mode:%d play:%d"),nCHNO,nMode,nPlay);
		    log_All(strLogType,_T("cmd_pump"),strTemp);	
			return TRUE;
		}
		//--------------------------------------------------
		pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_iPumpReqStop=1;
		while(nCount<10)
		{
			pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmdAdd(ID_PUMP_CMD_SETSPNO,1);
			pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmdAdd(ID_PUMP_CMD_AUTO);
			pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmdAdd(ID_PUMP_CMD_RUN);
			//Sleep(1000);
			Wait(1000); //ksj 20201016
			pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmd(ID_PUMP_CMD_READSTATE);//GETSTATE

			nMode=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nMode;//0-Auto,1-Manual
			nPlay=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPlay;//0-Run,1-Stop
            //--------------------------------------------------
			strTemp.Format(_T("CH:%d count:%d item:%d pump_cmd:ON mode:%d play:%d type:%s"),
				           nCHNO,nCount,nItem,
						   nMode,nPlay,strType);
		    log_All(strLogType,_T("cmd_pump"),strTemp);		
			//--------------------------------------------------
			if( nMode==0 && 
				nPlay==0)
			{//ON
				bflag=TRUE;
				break;
			}
			//---------------------------------------
			if( pChannel->GetState()!=PS_STATE_RUN )
			{
				break;
			}
			nCount++;
		}
		pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_iPumpReqStop=0;
		//--------------------------------------------------
		strTemp2.Format(_T("Pump ON StepNo:%d StepType:%d retry:%d"),
						nStepNo,
						nStepType,
						nCount);
		pChannel->WriteLog(strTemp2);
		return bflag;
	}
	else if(iPumpRefCmd==2 ||
		    iPumpRefCmd==12)
	{
		//--------------------------------------------------
		nPlay=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPlay;//0-Run,1-Stop
		if(nPlay==1)
		{//OFF
			strTemp.Format(_T("CH:%d pump stop play:%d"),nCHNO,nPlay);
		    log_All(strLogType,_T("cmd_pump"),strTemp);	
			return TRUE;
		}
		//--------------------------------------------------
		pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_iPumpReqStop=0.1;
		while(nCount<10)
		{
			pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmdAdd(ID_PUMP_CMD_STOP);
			//Sleep(1000);	
			Wait(1000); //ksj 20201016
			pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpCmd(ID_PUMP_CMD_READSTATE);//GETSTATE

			nPlay=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPlay;//0-Run,1-Stop
			//--------------------------------------------------
			strTemp.Format(_T("CH:%d count:%d item:%d pump_cmd:OFF play:%d type:%s"),
							nCHNO,nCount,nItem,
							nPlay,strType);			
			log_All(strLogType,_T("cmd_pump"),strTemp);
			//--------------------------------------------------			
			if(nPlay==1)
			{//OFF
				bflag=TRUE;
				break;
			}
			//---------------------------------------
			if( pChannel->GetState()!=PS_STATE_RUN )
			{
				break;
			}
			nCount++;
			//OneTime-----------------------------------------
			if(iPumpRefCmd==12) break;
		}
		pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_iPumpReqStop=0;
		//-----------------------------------------
		strTemp2.Format(_T("Pump OFF bret:%d StepNo:%d StepType:%d retry:%d"),
			                bflag,
							nStepNo,
 							nStepType,
							nCount);
		pChannel->WriteLog(strTemp2);
		//--------------------------------------------------
		return bflag;
	}
	//CMiniDump::End(); //yulee 20190115
	return TRUE;
}

void MainCtrl::onPwrSetData(CCyclerChannel *pChannel,int nState)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return;
	if(!mainFrm)  return;
	
	if(!mainView) return;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return;
	
	int nModuleID=pChannel->GetModuleID();
	int nChannelIndex=pChannel->GetChannelIndex();
	
	CScheduleData *pSchedule=pChannel->GetScheduleData();
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	int nOvenID=0;
	
	int nOutp=0;
	int nItem=mainFrm->onGetSerialPowerNo(nCHNO,nOutp);
	if(nItem<0 || nItem>1) return;

	float fCur_Volt=mainFrm->m_PwrSupplyCtrl[nItem].m_fState_Volt[nOutp-1];
	float fSet_Volt=0;
	
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	if(nState==PS_STATE_RUN)
	{
		BOOL bflag=FALSE;
		if(pSchedule)
		{
			int nTotal=pSchedule->GetStepSize();
			for(int i=0;i<nTotal;i++)
			{
				CStep *pStep=pSchedule->GetStepData(i);
				if(!pStep) continue;
				
				if( pStep->m_nPwrSupply_State>0 &&
					(pStep->m_StepIndex+1)==nStepNo)
				{
					if(pStep->m_nPwrSupply_Cmd==1)
					{
						fSet_Volt=(float)pStep->m_nPwrSupply_Volt/1000;//V
					}
					bflag=TRUE;
					break;
				}
			}
		}		
		mainFrm->m_PwrSupplyCtrl[nItem].m_pPwrSupplyData[nOutp-1].fSetVoltage=fSet_Volt;
		mainFrm->m_PwrSupplyCtrl[nItem].m_pPwrSupplyData[nOutp-1].fCurVoltage=fCur_Volt;			
	}
	else
	{
		mainFrm->m_PwrSupplyCtrl[nItem].m_pPwrSupplyData[nOutp-1].fSetVoltage=0;
		mainFrm->m_PwrSupplyCtrl[nItem].m_pPwrSupplyData[nOutp-1].fCurVoltage=fCur_Volt;
	}

	//ksj 20201012 : 파워 서플라이가 off 상태이면 전압 로깅하지 않도록 요청 (LGC 이용훈 선임)
	int nPowerOffLogging = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "PowerOffLogging", FALSE); //기본 꺼짐
	if(!nPowerOffLogging) //옵션이 꺼져있으면 전압을 로깅하지 않는다.
	{
		//int iOutp=mainFrm->m_PwrSupplyCtrl[nItem].m_iState_Outp;  //1이 파워 켜짐 상태. //근데 이게 지금 채널 별로 출력 상태를 따로 갖고 있지 않다??? 현장에서 디버깅 필요. 채널별로 출력이 따로 가능한 경우 상태를 따로 저장해야 할 수 있음.
		int iOutp=mainFrm->m_PwrSupplyCtrl[nItem].m_iState_Outp_Ch[nOutp-1];
		if(iOutp != 1)
		{
			//파워 서플라이 켜져있지 않으면..
			mainFrm->m_PwrSupplyCtrl[nItem].m_pPwrSupplyData[nOutp-1].fCurVoltage=0; //현재 전압 0으로 지움.
		}
	}	
	//ksj end

	::SFTSetPwrSupplyData(nModuleID,nChannelIndex, &mainFrm->m_PwrSupplyCtrl[nItem].m_pPwrSupplyData[nOutp-1]);
	//CMiniDump::End(); //yulee 20190115
}

void MainCtrl::onChillerSetData(CCyclerChannel *pChannel,int nState)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return;
	if(!mainFrm)  return;
	
	if(!mainView) return;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return;
	
	int nModuleID=pChannel->GetModuleID();
	int nChannelIndex=pChannel->GetChannelIndex();
	
	CScheduleData *pSchedule=pChannel->GetScheduleData();
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	int nOvenID=0;
	
	int nItem=mainFrm->onGetSerialChillerNo(nCHNO);
    if(nItem<0) return;
	
	//--------------------------------------
	float fRefTemp=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRefTemperature(nOvenID);
	float fCurTemp=pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetCurTemperature(nOvenID);

	/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
	{
		CString strLog;
		strLog.Format("SW Filtering : %d nitem:%d Ref:%f/Cur:%f n1", pChannel->m_iCh,nItem,fRefTemp,fCurTemp);
		GLog::log_All("chiller","debug",strLog);
	}
	/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
	
	m_bNEXFilter = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use NEXFilter", 0);

	m_bChiller_DESChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDESChiller", 0); //lyj 20201207

	//--------------------------------------
 	int iRefPump=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nSP;//REF
 	int iCurPump=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPV;//CUR
	float fRefPump=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP;//REF   //lyj 20201102 DES칠러 펌프 소숫점
	float fCurPump=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV;//CUR   //lyj 20201102 DES칠러 펌프 소숫점
	
	if(m_bChiller_DESChiller) //lyj 20200210 Des Chiller
	{
		fRefPump=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fSP/10.0f;
		fCurPump=pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.fPV/10.0f;
	}

	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	if(nState==PS_STATE_RUN)
	{
		BOOL bflag=FALSE;
		if(pSchedule)
		{
			int nTotal=pSchedule->GetStepSize();
			for(int i=0;i<nTotal;i++)
			{
				CStep *pStep=pSchedule->GetStepData(i);
				if(!pStep) continue;
				
				if( pStep->m_nChiller_Cmd==1 &&
					(pStep->m_StepIndex+1)==nStepNo)
				{
					bflag=TRUE;
					break;
				}
			}
		}

		BOOL bAllwaysChillerLog = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Always ChillerLogging", TRUE); //ksj 20201007 : 칠러 연동 스텝 유무에 관계 없이 칠러 데이터 로깅하도록 기능 추가.

		//if(bflag==TRUE)
		if(bflag==TRUE || bAllwaysChillerLog==TRUE) //ksj 20201007 : 칠러 연동 스텝 유무에 관계 없이 칠러 데이터 로깅하도록 기능 추가.
		{
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//칠러값 노이즈 필터 시작.
			if(m_bNEXFilter)
			{
				m_iNEXFilterCount = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use NEXFilterCount", 0);
				/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
				if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
				{
					CString strLog;
					strLog.Format("SW Filtering : %d nitem:%d Ref:%f/Cur:%f n2", pChannel->m_iCh,nItem,fRefTemp,fCurTemp);
					GLog::log_All("chiller","debug",strLog);
				}
				/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
				if(fCurTemp < 0.1f && fRefTemp < 0.1f )
				{
					if(fCurTemp > -0.1f && fRefTemp > -0.1f )
					{
						/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
						if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
						{
							CString strLog;
							strLog.Format("SW Filtering : %d nitem:%d Ref:%f/Cur:%f start1", pChannel->m_iCh,nItem,fRefTemp,fCurTemp);
							GLog::log_All("chiller","debug",strLog);
						}
						/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
						if(m_iNEXFilterCount_Loop < m_iNEXFilterCount)
						{
							CString strLog;
							strLog.Format("!!SW Filtering : %f/%f -> %f/%f Cnt[%d] start2", fCurTemp,fRefTemp,m_fltPreCurTemp,m_fltPreRefTemp,m_iNEXFilterCount_Loop);
							GLog::log_All("chiller","debug",strLog);
							fCurTemp = m_fltPreCurTemp;
							fRefTemp = m_fltPreRefTemp;
							m_iNEXFilterCount_Loop++;

							//ksj 20201019 : 채널로그에도 필터링한 흔적 남기기
							if(pChannel)
							{
								strLog.Format("ChilT : %f/%f -> %f/%f Cnt[%d]", fCurTemp,fRefTemp,m_fltPreCurTemp,m_fltPreRefTemp,m_iNEXFilterCount_Loop);							
								pChannel->WriteLog(strLog);
							}							
							//ksj end
						}
					}
					else
					{
						m_iNEXFilterCount_Loop = 0;

						if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
						{
							CString strLog;
							strLog.Format("SW Filtering : loop count clear 1");
							GLog::log_All("chiller","debug",strLog);
						}
					}
					
				}
				else
				{
					m_iNEXFilterCount_Loop = 0;

					if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
					{
						CString strLog;
						strLog.Format("SW Filtering : loop count clear 2");
						GLog::log_All("chiller","debug",strLog);
					}
				}
			}
			//칠러값 노이즈 필터 끝.
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			m_fltPreCurTemp = fCurTemp ;
			m_fltPreRefTemp = fRefTemp ;

			//--------------------------------------
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fRefTemper=fRefTemp;
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fCurTemper=fCurTemp;
            //--------------------------------------

// 			pDoc->m_chillerCtrl[nItem].m_pChillerData.iRefPump=iRefPump;
// 			pDoc->m_chillerCtrl[nItem].m_pChillerData.iCurPump=iCurPump;
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fRefPump=fRefPump; //lyj 20201105 NEX1100칠러 소숫점
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fCurPump=fCurPump; //lyj 20201105 NEX1100칠러 소숫점

			/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
			if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
			{
				CString strLog;
				strLog.Format("SW Filtering : %d nitem:%d Ref:%f/Cur:%f PreRef:%f PreCur:%f n3", pChannel->m_iCh,nItem,fRefTemp,fCurTemp,m_fltPreRefTemp,m_fltPreCurTemp);
				GLog::log_All("chiller","debug",strLog);
			}
			/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
			//lyj 20200924 e 중국 빈강 NEX1100 대응 ================

		}
		else
		{
			//--------------------------------------
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fRefTemper=0;
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fCurTemper=0;
            //--------------------------------------
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fRefPump=0;
			pDoc->m_chillerCtrl[nItem].m_pChillerData.fCurPump=0;
			/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
			if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
			{
				CString strLog;
				strLog.Format("SW Filtering : %d nitem:%d Ref:%f/Cur:%f bflag==FALSE", pChannel->m_iCh,nItem,fRefTemp,fCurTemp);
				GLog::log_All("chiller","debug",strLog);
			}
			/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
		}
	}
	else
	{
		//--------------------------------------
		pDoc->m_chillerCtrl[nItem].m_pChillerData.fRefTemper=fRefTemp;
		pDoc->m_chillerCtrl[nItem].m_pChillerData.fCurTemper=fCurTemp;
        //--------------------------------------
		
// 		pDoc->m_chillerCtrl[nItem].m_pChillerData.iRefPump=iRefPump;
// 		pDoc->m_chillerCtrl[nItem].m_pChillerData.iCurPump=iCurPump;	
		pDoc->m_chillerCtrl[nItem].m_pChillerData.fRefPump=fRefPump; //lyj 20201105 NEX1100칠러 소숫점
		pDoc->m_chillerCtrl[nItem].m_pChillerData.fCurPump=fCurPump; //lyj 20201105 NEX1100칠러 소숫점	

		/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
		{
			CString strLog;
			strLog.Format("SW Filtering : %d nitem:%d Ref:%f/Cur:%f nState!=PS_STATE_RUN", pChannel->m_iCh,nItem,fRefTemp,fCurTemp);
			GLog::log_All("chiller","debug",strLog);
		}
		/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
	}
	::SFTSetChillerData(nModuleID,nChannelIndex, &pDoc->m_chillerCtrl[nItem].m_pChillerData); //lyj 20200130
	//CMiniDump::Begin(); //yulee 20190115
}

void MainCtrl::onCellBALSetData(CCyclerChannel *pChannel,int nState)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return;
	if(!mainFrm)  return;
	
	if(!mainView) return;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return;
	
	int nModuleID=pChannel->GetModuleID();
	int nChannelIndex=pChannel->GetChannelIndex();
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	
	CString strLogType;
	strLogType.Format(_T("cellbal_%d"),nCHNO);
	
	CtrlSocketClient *ctrlClient=&pChannel->m_CtrlClient;
	if(!ctrlClient)  return;
   
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	BOOL bData=FALSE;
	if(nState==PS_STATE_RUN)
	{
		if(ctrlClient->m_iState==ID_SOCKET_STAT_CONN)
		{
			bData=TRUE;
			for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
			{
				pChannel->m_MainCtrl.m_pCellBALData.ch_state[i]=pChannel->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].ch_state;
			}		
		}
	}	
	if(bData==FALSE)
	{
		memset(pChannel->m_MainCtrl.m_pCellBALData.ch_state,0,sizeof(pChannel->m_MainCtrl.m_pCellBALData.ch_state));
	}
	::SFTSetCellBALData(nModuleID, nChannelIndex, &pChannel->m_MainCtrl.m_pCellBALData);
	//CMiniDump::End(); //yulee 20190115
}

void MainCtrl::onCellBalChannel(CCyclerChannel *pChannel)
{	
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return;
		
	CtrlSocketClient *ctrlClient=&pChannel->m_CtrlClient;
	if(ctrlClient->m_iState!=ID_SOCKET_STAT_CONN) return;

	int nState=pChannel->GetState();
	if(nState!=PS_STATE_RUN) return;

	int nModuleID=pChannel->GetModuleID();
	int nChannelIndex=pChannel->GetChannelIndex();

	if(!mainView) return;
	CCTSMonProDoc *pDoc=mainView->GetDocument();
	if(!pDoc) return;
	CCyclerModule *lpModule = pDoc->GetModuleInfo(nModuleID);
	if(!lpModule) return;

	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
    int nCycCurr=pChannel->GetCurCycleCount();

	CScheduleData *pSchedule=pChannel->GetScheduleData();
	if(!pSchedule) return;

	//--------------------------------------------
	int     i=0;
	BOOL    bflag=FALSE;
	CStep   *pStep=NULL;
	CString strTemp,strTemp2;

	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	int   nTotal=pSchedule->GetStepSize();
	for(int i = 0;i<nTotal;i++)
	{
		pStep=pSchedule->GetStepData(i);
        if(!pStep) continue;

		strTemp.Format(_T("CH:%d CircuitRes:%d WorkVoltUpper:%d WorkVoltLower:%d StartVolt:%d EndVolt:%d \
SignalSensTime:%d AutoStopTime:%d AutoNextStep:%d \
state:%d stepno:%d / %d steptype:%d / %d cyccurr:%d"),
		nCHNO,
		pStep->m_nCellBal_CircuitRes,
		pStep->m_nCellBal_WorkVoltUpper,
		pStep->m_nCellBal_WorkVolt,
		pStep->m_nCellBal_StartVolt,
		pStep->m_nCellBal_EndVolt,
		pStep->m_nCellBal_ActiveTime,
		pStep->m_nCellBal_AutoStopTime,
		pStep->m_nCellBal_AutoNextStep,
		pStep->m_nCellBal_State,
		(pStep->m_StepIndex+1),nStepNo,
		pStep->m_type,nStepType,
		nCycCurr);
	    
		if( (pStep->m_StepIndex+1)==nStepNo)
		{
			bflag=TRUE;
			break;
		}
	}	
	//--------------------------------------------	
	if(bflag==FALSE) return;
	//--------------------------------------------
	CString strLogType;
	strLogType.Format(_T("cellbal_%d"),nCHNO);

	if( m_iCellBAL_StepNo!=nStepNo ||
		m_iCellBAL_CycCurr!=nCycCurr)
	{
		m_iCellBAL_StepNo=nStepNo;
	    m_iCellBAL_CycCurr=nCycCurr;
		//--------------------------------------------
		onCellBALSendStop(pChannel,ctrlClient,1);
		//--------------------------------------------
		if(pStep->m_nCellBal_CircuitRes<1)
		{//NOT USE
			pStep->m_nCellBal_State=ID_CELLBAL_STAT_STOP;
			return;
		}
		//--------------------------------------------
		pStep->m_nCellBal_State=0;
		pStep->m_nCellBal_Count=0;
		pStep->m_nCellBal_Log=0;
		pStep->m_nCellBal_SeqNo=0;

		log_All(strLogType,_T("start"),_T("-------------------------"));
		log_All(strLogType,_T("state"),strTemp);
		pStep->m_nCellBal_Log=1;
	}
	m_iCellBAL_StepNo=nStepNo;
	m_iCellBAL_CycCurr=nCycCurr;
	//--------------------------------------------
	if(pStep->m_nCellBal_State==ID_CELLBAL_STAT_STOP) return;
	//--------------------------------------------
	if(pStep->m_nCellBal_CircuitRes<1)
	{//NOT USE
		pStep->m_nCellBal_State=ID_CELLBAL_STAT_STOP;
		return;
	}

	pStep->m_nCellBal_SeqNo++;
	if( (pStep->m_nCellBal_SeqNo%5)==0)
	{//10sec Interval=>200msec Interval thread
		int   iSeq=pStep->m_nChiller_SeqNo/5;

		CString str_ch_state=_T("");
		CString str_voltage=_T("");
		CString str_current=_T("");
		strTemp=mainGlobal.onGetCHAR(ctrlClient->m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
		int module_installed_ch=atoi(strTemp);
        if(module_installed_ch>0)
		{	
			for(int i=0;i<module_installed_ch;i++)
			{
				strTemp.Format(_T("%c,"),ctrlClient->m_cmdBodyModuleInfoReply.ch_data[i].ch_state);
				str_ch_state+=strTemp;
				
				strTemp.Format(_T("%s,"),mainGlobal.onGetCHAR(ctrlClient->m_cmdBodyModuleInfoReply.ch_data[i].voltage,10,0,_T("")));//ASC
				str_voltage+=strTemp;

				strTemp.Format(_T("%s,"),mainGlobal.onGetCHAR(ctrlClient->m_cmdBodyModuleInfoReply.ch_data[i].current,10,0,_T("")));//ASC
				str_current+=strTemp;
			}
		}

		if(g_AppInfo.iCEllBalLogRun==1)
		{
			if(ctrlClient->m_cmdBodyModuleInfoReply.op_state!='S')
			{
				strTemp2.Format(_T("nLinkChillerCount:%d CellCode:%d ChState:%d StepNo:%d StepType:%d CellBAL ch_state:%s voltage:%s current:%s"),
					iSeq, 
					pChannel->GetCellCode(),
					pChannel->GetState(), 
					pChannel->GetStepNo(),
					pChannel->GetStepType(),
					str_ch_state,
					str_voltage,
					str_current);
				pChannel->WriteCellBALLog(strTemp2);
			}
		}
		else if(g_AppInfo.iCEllBalLogRun==0)
		{
			strTemp2.Format(_T("nLinkChillerCount:%d CellCode:%d ChState:%d StepNo:%d StepType:%d CellBAL ch_state:%s voltage:%s current:%s"),
				iSeq, 
				pChannel->GetCellCode(),
				pChannel->GetState(), 
				pChannel->GetStepNo(),
				pChannel->GetStepType(),
				str_ch_state,
				str_voltage,
				str_current);
			pChannel->WriteCellBALLog(strTemp2);
		}

	}
	//--------------------------------------------
	if(pStep->m_nCellBal_State>ID_CELLBAL_STAT_NONE)
	{
		if(pStep->m_nCellBal_State==ID_CELLBAL_STAT_RUN)
		{		
			if(ctrlClient->m_cmdBodyModuleInfoReply.op_state=='S')
			{
				BOOL bRet=FALSE;
				if(pStep->m_nCellBal_AutoNextStep==1)
				{
					if((m_iCellBAL_NextStepCnt%3)==0) //yulee 20190208
					{
						bRet=mainView->onSbcNextStep(nModuleID,nChannelIndex);
						if(bRet==TRUE)
						{
							pStep->m_nCellBal_State=ID_CELLBAL_STAT_STOP;
						}
						m_iCellBAL_NextStepCnt = 0;
					}
					m_iCellBAL_NextStepCnt++;
					/*
					bRet=mainView->onSbcNextStep(nModuleID,nChannelIndex);
					if(bRet==TRUE)
					{
						pStep->m_nCellBal_State=ID_CELLBAL_STAT_STOP;
					}
					*/
				}
				else
				{
					pStep->m_nCellBal_State=ID_CELLBAL_STAT_STOP;
				}
				strTemp.Format(_T("CH:%d sbc next ret:%d step ch_state:%s AutoNextStep:%d"),
								nCHNO,bRet,
								ctrlClient->onGetStrMachine_ch_state(),
								pStep->m_nCellBal_AutoNextStep);
				log_All(strLogType,_T("next"),strTemp);
			}
			else
			{
				m_iCellBAL_NextStepCnt = 0;
			}
		}
		return;
	}
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	//-----------------------------------
    int     nRet=0;
	int     nRet2=0;
	float   fVoltMin=0;
	float   fVoltMax=0;	
	int     nCellBal_Count=0;
	int     nCellBal_No=0;
	
	SFT_AUX_DATA *pAuxData = pChannel->GetAuxData();
	if(!pAuxData) return;
	int nAuxCount=pChannel->GetAuxCount();
	if(nAuxCount<1) return;

	CString strCellData=_T("");
	BOOL bCellBAL=FALSE;
	char ch_state[PS_MAX_CELLBAL_COUNT]={0,};
	int nFirstAuxNo = 0, nAuxNoDev = 0; //yulee 20190219
	BOOL bAuxVoltStat = TRUE;
	int nAuxCntBefVolt = 0;
	int nAuxCntVolt = 0;

	for(int i = 0; i < nAuxCount; i++)	//Aux Channel 순번 1~
	{
		CChannelSensor *pSensor=lpModule->GetAuxData(pAuxData[i].auxChNo-1, pAuxData[i].auxChType);
		if(pSensor == NULL) return;
				
		STF_MD_AUX_SET_DATA AuxSetData = pSensor->GetAuxData();
		float fValue = pDoc->UnitTrans( (float)pAuxData[i].lValue, FALSE, pSensor->GetAuxType());
		if(pSensor->GetAuxType()!=PS_AUX_TYPE_VOLTAGE)
		{
			nAuxCntBefVolt++;
			continue; //첫번째 aux type이 Voltage가 아닐 수 있음에 주의 필요
		}
		else
		{
			if(bAuxVoltStat == TRUE)
			{
				nFirstAuxNo = AuxSetData.auxChNo;
				bAuxVoltStat = FALSE;
			}
			nAuxCntVolt ++;
		}



// 		//yulee 20190219
// 		int nOffsetChNo;
// 			nOffsetChNo = 0;
// 
// 		if(AuxSetData.auxChNo-1 != i) //AuxSetData.auxChNo = 26 = 27-1 i = 4	//55,56,57 65,66,67 첫 번째 있는 숫자가 55고 이것이 1번
// 		{
// 			nOffsetChNo = AuxSetData.auxChNo-1-i; //22 = 26-4	nOffsetChNo 이것이 기본 offset 값  //다음으로 넘어오는 값은 이것과의 차이와 Offset 값을 빼서 순서를 찾아야한다. 
// 		}
// 		//55,56,57,65,66,67 첫 번째 있는 숫자가 55고 이것이 1번
// 		//0 , 1, 2, 3, 4, 5

		

		if(((AuxSetData.auxChNo) - (nFirstAuxNo +(nAuxCntVolt-1)) !=  0))
		{
			nAuxNoDev = ((AuxSetData.auxChNo) - nFirstAuxNo) - (i-nAuxCntBefVolt);
		}
		else
		{
			nAuxNoDev = 0;
		}

		bCellBAL=FALSE;
		if(g_AppInfo.iCellBalAuxDiv3>0)   //ksj 20200721 : 코멘트추가. 왜 아무 값이나 들어있으면 체크하는지?? 150001 이어야 체크하는거 아닌지?? 확인 필요
		{
			if( g_AppInfo.iCellBalAuxDiv3==AuxSetData.funtion_division1 ||
				g_AppInfo.iCellBalAuxDiv3==AuxSetData.funtion_division2 ||
				g_AppInfo.iCellBalAuxDiv3==AuxSetData.funtion_division3)
			{
				bCellBAL=TRUE;
			}
		}
		else
		{
			bCellBAL=TRUE;
		}
		if(bCellBAL==TRUE)
		{
// 			int nAddPos = 0;
// 			if(nAuxNoDev > 0)
// 			{
// 				nAddPos = nAuxNoDev - 1;
// 			}
			ch_state[i-nAuxCntBefVolt + nAuxNoDev]=1;//yulee 20190219 8 = (26-22) = 4
			//ch_state[(AuxSetData.auxChNo-1) - nOffsetChNo + nAuxNoDev - nAuxCntBefVolt]=1;//yulee 20190219 8 = (26-22) = 4

			if(nCellBal_Count==0) 
			{
				fVoltMin=fVoltMax=fValue;
			}
			fVoltMin=min(fVoltMin,fValue);
			fVoltMax=max(fVoltMax,fValue);
			nCellBal_Count++;
		}
		nCellBal_No++;
		strTemp.Format(_T("CH:%d No:%d(%s) Count:%d %s(div:%d,%d,%d):%f min:%f max:%f"),
						nCHNO,
						nCellBal_No,
						((bCellBAL==TRUE)?_T("OK"):_T("NO")),
						nCellBal_Count,
						pSensor->GetAuxName(),
						AuxSetData.funtion_division1,
						AuxSetData.funtion_division2,
						AuxSetData.funtion_division3,	
						fValue,fVoltMin,fVoltMax);
		log_All(strLogType,_T("aux"),strTemp);
	}
	pStep->m_nCellBal_Count=nCellBal_Count;
	if(nCellBal_Count<1)
	{
		strTemp.Format(_T("CH:%d aux volt cellbal_count(div3:%d):%d"),
						nCHNO,g_AppInfo.iCellBalAuxDiv3,nCellBal_Count);
		log_All(strLogType,_T("state"),strTemp);
		//-------------------------------------------
		pStep->m_nCellBal_State=ID_CELLBAL_STAT_STOP;
		//-------------------------------------------
		return;
	}
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	float fVoltDev=fabs(fVoltMax-fVoltMin);
	int iVoltMin=fVoltMin*1000;//mV
	int iVoltMax=fVoltMax*1000;//mV
	int iVoltDev=fVoltDev*1000;//mV

	strTemp=mainGlobal.onGetCHAR(ctrlClient->m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
	int  module_installed_ch=atoi(strTemp);
	char op_mode=ctrlClient->m_cmdBodyModuleInfoReply.op_mode;
	CString strChState=mainGlobal.onGetCHAR(ch_state,PS_MAX_CELLBAL_COUNT,0,_T(""),2);//DEC

	strTemp.Format(_T("CH:%d aux volt cellbal_count(div3:%d):%d(%s) min:%f max:%f dev:%f \
CircuitRes:%d WorkVoltUpper:%d WorkVoltLower:%d StartVolt:%d EndVolt:%d SignalSenstime:%d AutoStopTime:%d \
op_mode:%c module_installed_ch:%d voltmin:%d voltmax:%d voltdev:%d "),
	nCHNO,g_AppInfo.iCellBalAuxDiv3,nCellBal_Count,strChState,
	fVoltMin,fVoltMax,fVoltDev,
	pStep->m_nCellBal_CircuitRes,
	pStep->m_nCellBal_WorkVoltUpper,
	pStep->m_nCellBal_WorkVolt,
	pStep->m_nCellBal_StartVolt,
	pStep->m_nCellBal_EndVolt,
	pStep->m_nCellBal_ActiveTime,
	pStep->m_nCellBal_AutoStopTime,
	op_mode,
	module_installed_ch,
	iVoltMin,iVoltMax,iVoltDev);
	log_All(strLogType,_T("state"),strTemp);
	
	/*	
	if( iVoltMin<pStep->m_nCellBal_WorkVolt && 
		iVoltDev<pStep->m_nCellBal_StartVolt) return;
	*/

#ifdef NDEBUG
	if( iVoltMin<pStep->m_nCellBal_WorkVoltUpper && iVoltMin<pStep->m_nCellBal_WorkVolt && 
	  iVoltDev<pStep->m_nCellBal_StartVolt)
	{
	  pStep->m_nCellBal_State=ID_CELLBAL_STAT_RUN;
	  log_All(strLogType,_T("state"),_T("next------------------------"));
	  return;
	}
#endif

	//---------------------------------------------
	pStep->m_nCellBal_State=ID_CELLBAL_STAT_START;
	//--------------------------------------------
	if(onCellBALSendOpModeSet_Sched(pChannel,ctrlClient,pStep)==FALSE) return;
	if(onCellBALSendLoadModeSet_Sched(pChannel,ctrlClient,pStep)==FALSE) return;
	onCellBALSendRun_Sched(pChannel,module_installed_ch,nCellBal_Count,strChState,ctrlClient,pStep);
	//--------------------------------------------
	pStep->m_nCellBal_State=ID_CELLBAL_STAT_RUN;
	log_All(strLogType,_T("state"),_T("run------------------------"));
	//CMiniDump::End(); //yulee 20190115
}

BOOL MainCtrl::onCellBALSendOpModeSet_Sched(CCyclerChannel *pChannel,CtrlSocketClient *ctrlClient,CStep *pStep)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel)   return FALSE;
	if(!ctrlClient) return FALSE;

	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
    int nCycCurr=pChannel->GetCurCycleCount();

	char op_mode=ctrlClient->m_cmdBodyModuleInfoReply.op_mode;
	//--------------------------------------------
	if(op_mode=='A') return TRUE;
	//--------------------------------------------
	
	CString strLogType;
	strLogType.Format(_T("cellbal_%d"),nCHNO);

	CString strTemp,strTemp2;
	int     nCount=0;
	int     nRet=0;
    BOOL    bflag=FALSE;

	ctrlClient->m_iModuleInfoReqStop=1;
	while(nCount<3)
	{
		nRet=ctrlClient->onSetMachine_op_mode_set('A');
        Sleep(500);
		ctrlClient->onSendCommand(P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST);
        Sleep(500);
        //--------------------------------------------
		op_mode=ctrlClient->m_cmdBodyModuleInfoReply.op_mode;
		strTemp.Format(_T("CH:%d step:%d recv op_mode:%c nret_s:%d"),
						nCHNO,nCount+1,
						op_mode,
						nRet);
		log_All(strLogType,_T("state"),strTemp);
		//--------------------------------------------
		if(op_mode=='A')
		{
			bflag=TRUE;
			break;			
		}
		//---------------------------------------
		if( pChannel->GetState()!=PS_STATE_RUN )
		{
			break;
		}
        //--------------------------------------------
		nCount++;
	}
	ctrlClient->m_iModuleInfoReqStop=0;
	//--------------------------------------------------------
	strTemp2.Format(_T("cellbal bret:%d StepNo:%d StepType:%d mode:%c"),
		            bflag,
					nStepNo,
					nStepType,
					op_mode);
	pChannel->WriteLog(strTemp2);
	//--------------------------------------------------------
	//CMiniDump::End(); //yulee 20190115
	return bflag;

}

BOOL MainCtrl::onCellBALSendLoadModeSet_Sched(CCyclerChannel *pChannel,CtrlSocketClient *ctrlClient,CStep *pStep)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel)   return FALSE;
	if(!ctrlClient) return FALSE;
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
    int nCycCurr=pChannel->GetCurCycleCount();

	CString strLogType;
	strLogType.Format(_T("cellbal_%d"),nCHNO);

	CString strTemp,strTemp2;
	CString str_load_mode_value_s;
	CString str_load_mode_set_s;
	CString str_signalsenstime_s;
	CString str_voltUpper_s;
	CString str_endvolt_s;
	CString str_startvolt_s;
	CString str_autostoptime_s;
	CString str_ch_cond_end_time;
	
	S_P1_CMD_BODY_LOAD_MODE_SET cmdBodyLoadModeSet;
	int nSize=sizeof(S_P1_CMD_BODY_LOAD_MODE_SET);
	memset(&cmdBodyLoadModeSet,0,nSize);
	//-----------------------------------------------------
	mainGlobal.onLoadModeCopy(cmdBodyLoadModeSet,
					          ctrlClient->m_cmdBodyModuleInfoReply);
	//-----------------------------------------------------
	str_load_mode_set_s=mainGlobal.onGetCHAR(cmdBodyLoadModeSet.load_mode_set,2,0,_T(""));//ASC

	int iVal=pStep->m_nCellBal_CircuitRes*1000;//mR
	iVal=((iVal>9999999)?9999999:iVal);
	strTemp.Format(_T("-%07dmR"),iVal);//-0010000mR
	str_load_mode_value_s=strTemp;
	mainGlobal.onCopyChar(cmdBodyLoadModeSet.load_mode_value        , strTemp    ,10);
	
	iVal=pStep->m_nCellBal_ActiveTime;//SEC
	iVal=((iVal>99)?99:iVal);
	strTemp=GStr::str_IntToStr(iVal,2);
	str_signalsenstime_s=strTemp;
	mainGlobal.onCopyChar(cmdBodyLoadModeSet.signal_sens_time       , strTemp    ,2);

	iVal=pStep->m_nCellBal_WorkVoltUpper*1000;//uV
	iVal=((iVal>9999999)?9999999:iVal);
	strTemp.Format(_T("%s%07duV"),((iVal>=0)?_T("+"):_T("-")),iVal);//+3500000uV
	str_voltUpper_s=strTemp;
	mainGlobal.onCopyChar(cmdBodyLoadModeSet.availability_voltage_upper    , strTemp    ,10);

	iVal=pStep->m_nCellBal_WorkVolt*1000;//uV
	iVal=((iVal>9999999)?9999999:iVal);
	strTemp.Format(_T("%s%07duV"),((iVal>=0)?_T("+"):_T("-")),iVal);//+3500000uV
	str_endvolt_s=strTemp;
	mainGlobal.onCopyChar(cmdBodyLoadModeSet.availability_voltage_lower    , strTemp    ,10);
		
	iVal=pStep->m_nCellBal_StartVolt*1000;//uV
	iVal=((iVal>9999999)?9999999:iVal);
	strTemp.Format(_T("%s%07duV"),((iVal>=0)?_T("+"):_T("-")),iVal);//+0030000uV
	str_startvolt_s=strTemp;
	mainGlobal.onCopyChar(cmdBodyLoadModeSet.ch_cond_detection      , strTemp    ,10);
	
	iVal=pStep->m_nCellBal_EndVolt*1000;//uV
	iVal=((iVal>9999999)?9999999:iVal);
	strTemp.Format(_T("%s%07duV"),((iVal>=0)?_T("+"):_T("-")),iVal);//+0020000uV
	str_startvolt_s=strTemp;
	mainGlobal.onCopyChar(cmdBodyLoadModeSet.ch_cond_release        , strTemp    ,10);
	
	iVal=pStep->m_nCellBal_AutoStopTime;//SEC
	iVal=((iVal>9999999)?9999999:iVal);
	strTemp.Format(_T("%07dsec"),iVal);//0000000sec
	str_autostoptime_s=strTemp;
	mainGlobal.onCopyChar(cmdBodyLoadModeSet.ch_cond_auto_stop_time , strTemp    ,10);

	iVal=pStep->m_lEndTimeDay*86400;//SEC
	iVal=iVal+(int)pStep->m_fEndTime;//SEC
	iVal=((iVal>9999999)?9999999:iVal);
	strTemp.Format(_T("%07dsec"),iVal);//0000000sec
	str_ch_cond_end_time=strTemp;

	int TmpiVal=0;//Balancing기에 보낼 때는 600s 더해서 보낸다.  //yulee 20190117
	if(iVal > 0)
	{
		TmpiVal = 600 + atoi(strTemp);
		strTemp2.Format(_T("%07dsec"), TmpiVal);
	}

	mainGlobal.onCopyChar(cmdBodyLoadModeSet.ch_cond_end_time       , strTemp2    ,10);

	strTemp.Format(_T("CH:%d send load_mode_value:%s load_mode_set:%s signalsenstime:%s voltupper:%s voltLower:%s startvolt:%s autostoptime:%s ch_cond_end_time:%s(%s) EndTimeDay:%ld EndTime:%f"),
					nCHNO,
					str_load_mode_value_s,
					str_load_mode_set_s,
					str_signalsenstime_s,
					str_voltUpper_s,
					str_endvolt_s,
					str_startvolt_s,
					str_autostoptime_s,
					str_ch_cond_end_time,
					strTemp2,
					pStep->m_lEndTimeDay,
					pStep->m_fEndTime);
	log_All(strLogType,_T("loadset"),strTemp);

	char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
	memset(szData,0,sizeof(szData));
	memcpy(szData,&cmdBodyLoadModeSet,nSize);
	
	CString str_load_mode_set_r;
	CString str_load_mode_value_r;	
	int     nCount=0;
	int     nRet=0;
	BOOL    bflag=FALSE;

	ctrlClient->m_iModuleInfoReqStop=1;
	while(nCount<3)
	{
		nRet=ctrlClient->onSendCommand(P1_CMD_TO_MACHINE_LOAD_MODE_SET,0,nSize,szData);
		Sleep(500);
		ctrlClient->onSendCommand(P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST);
        Sleep(500);
		//------------------------------------------------------------------------------
		str_load_mode_set_r=mainGlobal.onGetCHAR(ctrlClient->m_cmdBodyModuleInfoReply.load_mode_set,2,0,_T(""));//ASC
		str_load_mode_value_r=mainGlobal.onGetCHAR(ctrlClient->m_cmdBodyModuleInfoReply.load_mode_value,10,0,_T(""));//ASC
		strTemp.Format(_T("CH:%d step:%d recv str_load_mode_set:%s(send:%s) load_mode_value:%s(send:%s) nret_s:%d"),
						nCHNO,nCount+1,
						str_load_mode_set_r,
						str_load_mode_set_s,
						str_load_mode_value_r,
						str_load_mode_value_s,
						nRet);
		log_All(strLogType,_T("loadset"),strTemp);
		//--------------------------------------------
		if( str_load_mode_set_s==str_load_mode_set_r &&
			str_load_mode_value_s==str_load_mode_value_r )
		{
			bflag=TRUE;
			break;			
		}
		//---------------------------------------
		if( pChannel->GetState()!=PS_STATE_RUN )
		{
			break;
		}
		//--------------------------------------------
		nCount++;
	}
	ctrlClient->m_iModuleInfoReqStop=0;
	//--------------------------------------------------------
	strTemp2.Format(_T("cellbal bret:%d StepNo:%d StepType:%d load_mode_set:%s load_mode_value:%s"),
					bflag,
					nStepNo,
					nStepType,
					str_load_mode_set_r,
					str_load_mode_value_r);
	pChannel->WriteLog(strTemp2);
	//--------------------------------------------------------
	//CMiniDump::End(); //yulee 20190115
	return bflag;
}


BOOL MainCtrl::onCellBALSendStop(CCyclerChannel *pChannel,CtrlSocketClient *ctrlClient,int iType)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel)   return FALSE;
	if(!ctrlClient) return FALSE;
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
    int nCycCurr=pChannel->GetCurCycleCount();
	
	CString strLogType;
	strLogType.Format(_T("cellbal_%d"),nCHNO);
	
	int nState=ctrlClient->m_pChannel->GetState();
	int module_installed_ch=ctrlClient->onGetMachine_module_installed_ch();

	CString str_ch_state_s;
	CString str_ch_state_r;
	CString strTemp,strTemp2;
	int     nCount=0;
	int     nRet=0;
	BOOL    bflag=FALSE;
	//---------------------------------------------
	if(iType==1)
	{//ALL STOP Check
		str_ch_state_r=onCellBALGetMachine_ch_data(ctrlClient->m_cmdBodyModuleInfoReply.ch_data,1);
		strTemp.Format(_T("CH:%d stop recv ch_state:%s"),
						nCHNO,
						str_ch_state_r);
		log_All(strLogType,_T("stop"),strTemp);

		if(str_ch_state_r.Find(_T("1"))<0) return TRUE;
	}
	//---------------------------------------------
	ctrlClient->m_iModuleInfoReqStop=1;
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	while(nCount<3)
	{
		ctrlClient->onSetMachine_stop_ch(module_installed_ch,str_ch_state_s);
		Sleep(500);
		ctrlClient->onSendCommand(P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST);
		Sleep(500);
		//--------------------------------------------------------------------------
		str_ch_state_s.Replace(_T("1"),_T("0"));//carefully
		str_ch_state_r=onCellBALGetMachine_ch_data(ctrlClient->m_cmdBodyModuleInfoReply.ch_data,1);
		//--------------------------------------------------------------------------
		onCellBALSetData(ctrlClient->m_pChannel,nState);
        //--------------------------------------------------------------------------
		strTemp.Format(_T("CH:%d step:%d stop recv ch_state:%s(send:%s) nret_r:%d"),
						nCHNO,nCount+1,
						str_ch_state_r,
						str_ch_state_s,
		             	nRet);
		log_All(strLogType,_T("stop"),strTemp);
		//--------------------------------------------
		if(str_ch_state_s==str_ch_state_r)
		{
			bflag=TRUE;			
			break;
		}
		//---------------------------------------
		if( pChannel->GetState()!=PS_STATE_RUN )
		{
			break;
		}
		//--------------------------------------------
		nCount++;
	}
	ctrlClient->m_iModuleInfoReqStop=0;
	//--------------------------------------------------------
	strTemp2.Format(_T("cellbal stop bret:%d StepNo:%d StepType:%d ch_state:%s"),
					bflag,
					nStepNo,
					nStepType,
					str_ch_state_r);
	pChannel->WriteLog(strTemp2);
	//--------------------------------------------------------
	//CMiniDump::End(); //yulee 20190115
	return bflag;
}

BOOL MainCtrl::onCellBALSendRun_Sched(CCyclerChannel *pChannel,int module_installed_ch,int nCellBal_Count,CString strChState,
									  CtrlSocketClient *ctrlClient,CStep *pStep)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel)   return FALSE;
	if(!ctrlClient) return FALSE;
	
	int nCHNO=pChannel->m_iCh;
	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
    int nCycCurr=pChannel->GetCurCycleCount();

	CString strLogType;
	strLogType.Format(_T("cellbal_%d"),nCHNO);

	int nState=ctrlClient->m_pChannel->GetState();

	CString str_ch_state_s=strChState;
	CString str_ch_state_r;
	CString strTemp,strTemp2;
	int     nCount=0;
	int     nRet=0;
	BOOL    bflag=FALSE;
	//CMiniDump::End(); //yulee 20190115
	//CMiniDump::Begin(); //yulee 20190115
	ctrlClient->m_iModuleInfoReqStop=1;
	while(nCount<3)
	{
		nRet=ctrlClient->onSetMachine_run(module_installed_ch,str_ch_state_s);
		Sleep(500);
		ctrlClient->onSendCommand(P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST);
		Sleep(500);
		//--------------------------------------------------------------------------
		str_ch_state_r=onCellBALGetMachine_ch_data(ctrlClient->m_cmdBodyModuleInfoReply.ch_data,1);
		//--------------------------------------------------------------------------
		onCellBALSetData(ctrlClient->m_pChannel,nState);
        //--------------------------------------------------------------------------
		strTemp.Format(_T("CH:%d step:%d run recv ch_state:%s(send:%s) nret_r:%d"),
						nCHNO,nCount+1,
						str_ch_state_r,
						str_ch_state_s,
						nRet);
		log_All(strLogType,_T("run"),strTemp);
		//--------------------------------------------
		if(str_ch_state_s==str_ch_state_r) 
		{
			bflag=TRUE;
			break;			
		}
		//---------------------------------------
		if( pChannel->GetState()!=PS_STATE_RUN )
		{
			break;
		}
		//--------------------------------------------
		nCount++;
	}
	ctrlClient->m_iModuleInfoReqStop=0;
	//--------------------------------------------------------
	strTemp2.Format(_T("cellbal run bret:%d StepNo:%d StepType:%d ch_state:%s"),
					bflag,
					nStepNo,
					nStepType,
					str_ch_state_r);
	pChannel->WriteLog(strTemp2);
	//--------------------------------------------------------
	//CMiniDump::End(); //yulee 20190115
	return bflag;
}

CString MainCtrl::onCellBALGetMachine_ch_data(S_P1_CH_DATA *ch_data,int iType)
{
	//CMiniDump::Begin(); //yulee 20190115
	char ch_state[PS_MAX_CELLBAL_COUNT]={0,};

	for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
	{
		ch_state[i]=ch_data[i].ch_state;
	}
	return onCellBALGetMachine_ch_state(ch_state,iType);
	//CMiniDump::End(); //yulee 20190115
}

CString MainCtrl::onCellBALGetMachine_ch_state(char *ch_state,int iType)
{
	//CMiniDump::Begin(); //yulee 20190115
	CString strVal=_T("");
	CString strTemp=_T("");
	for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
	{
		strTemp.Format(_T("%c"),ch_state[i]);
		if(iType==1)
		{
			strTemp=_T("0");
			if( ch_state[i]=='R' ||
				ch_state[i]=='P') 
			{
				strTemp=_T("1");
			}
		}
		strVal+=strTemp;
	}
    return strVal;
	//CMiniDump::End(); //yulee 20190115
}

BOOL MainCtrl::onPwrSbcChPause(CCyclerChannel *pChannel,CStep *pStep,int nItem,CString strLogType)
{
	//CMiniDump::Begin(); //yulee 20190115
	if(!pChannel) return FALSE;
	
	if(!mainView) return FALSE;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc) return FALSE;
	
	int nModuleID=pChannel->GetModuleID();
	int nChannelIndex=pChannel->GetChannelIndex();

	int nStepNo=pChannel->GetStepNo();
	int nStepType=pChannel->GetStepType();
	int nCHNO=pChannel->m_iCh;

	CString strTemp;
	BOOL bRet=mainView->onSbcPause(nModuleID,nChannelIndex);
	if(bRet==TRUE)
	{
		pStep->m_nPwrSupply_State=ID_PWRSUPY_STAT_STOP;
		
		CString strTitle;
		strTitle.Format(_T("CH:%d Warn Happen"),nCHNO);
		
		CString strMessage;
		strMessage.Format(_T("Power Supply(ID:%d) Recv Failed"),nItem+1);
	
		strTemp.Format(_T("CHNO:%d StepNo:%d StepType:%d Pwr_No:%d Recv Error : %s"),
					   nCHNO,nStepNo,nStepType,nItem,
					   strMessage);
		log_All(strLogType,_T("error"),strTemp);
		
		mainView->onShowMessage(1,strTitle,strMessage);
	}
	//CMiniDump::End(); //yulee 20190115
	return bRet;
}
