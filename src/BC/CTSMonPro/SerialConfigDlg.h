#if !defined(AFX_SERIALCONFIGDLG_H__51674942_3258_11D2_975A_444553540000__INCLUDED_)
#define AFX_SERIALCONFIGDLG_H__51674942_3258_11D2_975A_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
//SerialConfigDlg.h : header file
//

#define ID_SERIAL_BCR1     1
#define ID_SERIAL_BCR2     2
#define ID_SERIAL_OVEN1    3
#define ID_SERIAL_OVEN2    4
#define ID_SERIAL_LOAD1    5
#define ID_SERIAL_LOAD2    6
#define ID_SERIAL_POWER1   11
#define ID_SERIAL_POWER2   12
#define ID_SERIAL_CHILLER1 21
#define ID_SERIAL_CHILLER2 22
#define ID_SERIAL_CHILLER3 23
#define ID_SERIAL_CHILLER4 24

/////////////////////////////////////////////////////////////////////////////
// CSerialConfigDlg dialog
#include "SerialPort.h"
#include "afxwin.h"

class CSerialConfigDlg : public CDialog
{
// Construction
public:
	
	CSerialConfigDlg(CWnd* pParent = NULL);   // standard constructor

	CString m_strTitle;
	CString m_ParityStr;

	SERIAL_CONFIG	m_SerialConfig[6];
	
	SERIAL_CONFIG	m_Serial_Power[MAX_POWER_COUNT];
	SERIAL_CONFIG	m_Serial_Chiller[MAX_CHILLER_COUNT];
	SERIAL_CONFIG	m_Serial_Pump[MAX_PUMP_COUNT];
	
	BOOL onLoadConfig(int iType=0);
	void onPowerDeal(int nCmd,int nItem,int nVal=0);
	void onSaveConfig(int iType=0);

// Dialog Data
	//{{AFX_DATA(CSerialConfigDlg)
	enum { IDD = IDD_SERIAL_DLG , IDD2 = IDD_SERIAL_DLG_ENG ,IDD3 = IDD_SERIAL_DLG_PL };
	CComboBox	m_CboChiller4PumpCom;
	CComboBox	m_CboChiller3PumpCom;
	CComboBox	m_CboChiller2PumpCom;
	CComboBox	m_CboChiller1PumpCom;
	CComboBox	m_CboChiller4Chno;
	CComboBox	m_CboChiller3Chno;
	CComboBox	m_CboChiller1Chno;
	CComboBox	m_CboChiller2Chno;
	CComboBox	m_CboChiller4Model;
	CComboBox	m_CboChiller3Model;
	CComboBox	m_CboChiller2Model;
	CComboBox	m_CboChiller1Model;
	CComboBox	m_CboChiller4Com;
	CComboBox	m_CboChiller3Com;
	CComboBox	m_CboChiller2Com;
	CComboBox	m_CboChiller1Com;
	CButton	m_ChkChiller4Use;
	CButton	m_ChkChiller3Use;
	CButton	m_ChkChiller2Use;
	CButton	m_ChkChiller1Use;
	CButton	m_ChkPwr1Use;
	CButton	m_ChkPwr2Use;	
	CComboBox	m_CboPwr1Com;
	CComboBox	m_CboPwr2Com;
	CComboBox	m_CboPwr1AChno;	
	CComboBox	m_CboPwr1BChno;	
	CComboBox	m_CboPwr2AChno;	
	CComboBox	m_CboPwr2BChno;	
	CString		m_ctrlDcLoader_2;
	CString		m_ctrlDcLoader_1;
	CComboBox	m_ctrlOvenMax2;
	CComboBox	m_ctrlOvenMax1;
	CComboBox	m_ctrlOvenModel_2;
	CComboBox	m_ctrlOvenModel_1;
	CString		m_strPortNum;
	CString		m_strBaudRate;
	CString		m_strDataBits;
	int			m_nParity;
	CString		m_strStopBits;
	CString		m_strSendBuffer;
	CString		m_strPortNum1;
	CString		m_strBaudRate1;
	CString		m_strDataBits1;
	int			m_nParity1;
	CString		m_strStopBits1;
	CString		m_strSendBuffer1;
	CString		m_strPortNum2;
	CString		m_strBaudRate2;
	CString		m_strDataBits2;
	int			m_nParity2;
	CString		m_strStopBits2;
	CString		m_strSendBuffer2;
	CString		m_strPortNum3;
	CString		m_strPortNum_Load1;
	CString		m_strPortNum_Load2;
	CString		m_strBaudRate3;
	CString		m_strDataBits3;
	int			m_nParity3;
	CString		m_strStopBits3;
	CString		m_strSendBuffer3;
	BOOL	m_bUsePort1;
	BOOL	m_bUsePort2;
	BOOL	m_bUsePort3;
	BOOL	m_bUsePort4;
	BOOL	m_bUsePort5;
	BOOL	m_bUsePort6;
	float	m_fOndoFactor1;
	float	m_fOndoFactor2;
	float	m_fOndoSPFactor1;
	float	m_fOndoSPFactor2;
	float	m_fChillerPumpFactor;
	//}}AFX_DATA
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSerialConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnOk();
	afx_msg void OnChkPwr1Use();
	afx_msg void OnChkPwr2Use();
	afx_msg void OnButPwr1On();
	afx_msg void OnButPwr1Off();
	afx_msg void OnButPwr2On();
	afx_msg void OnButPwr2Off();	
	afx_msg void OnCheck1();
	afx_msg void OnCheck2();
	afx_msg void OnCheck3();
	afx_msg void OnCheck4();
	afx_msg void OnCheck5();
	afx_msg void OnCheck6();
	afx_msg void OnChkChiller1Use();
	afx_msg void OnChkChiller2Use();
	afx_msg void OnChkChiller3Use();
	afx_msg void OnChkChiller4Use();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CButton m_ChkChiller1AutoStop;
	CButton m_ChkChiller2AutoStop;
	CButton m_ChkChiller3AutoStop;
	CButton m_ChkChiller4AutoStop;
	afx_msg void OnBnClickedChkChiller1Autostop();
	afx_msg void OnBnClickedChkChiller2Autostop();
	afx_msg void OnBnClickedChkChiller3Autostop();
	afx_msg void OnBnClickedChkChiller4Autostop();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALCONFIGDLG_H__51674942_3258_11D2_975A_444553540000__INCLUDED_)
