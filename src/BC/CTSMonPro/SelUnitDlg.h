#if !defined(AFX_SELUNITDLG_H__2AA5AFA1_074A_42F5_82B5_1FE0832F2CC8__INCLUDED_)
#define AFX_SELUNITDLG_H__2AA5AFA1_074A_42F5_82B5_1FE0832F2CC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelUnitDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelUnitDlg dialog

class CSelUnitDlg : public CDialog
{
// Construction
public:	
	int m_nModuleID;
	void SetModuleID(int nModuleID);
	int GetModuleID();
	CSelUnitDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelUnitDlg)
	enum { IDD = IDD_SEL_UNIT_DIALOG , IDD2 = IDD_SEL_UNIT_DIALOG_ENG , IDD3 = IDD_SEL_UNIT_DIALOG_PL};
	CComboBox	m_ctrlUnit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelUnitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelUnitDlg)
	afx_msg void OnSelchangeComboUnit();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELUNITDLG_H__2AA5AFA1_074A_42F5_82B5_1FE0832F2CC8__INCLUDED_)
