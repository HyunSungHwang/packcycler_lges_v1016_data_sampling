#if !defined(AFX_OVENSERILCONFIGDLG_H__6FF82901_7B3F_4DD8_BA12_81495E593268__INCLUDED_)
#define AFX_OVENSERILCONFIGDLG_H__6FF82901_7B3F_4DD8_BA12_81495E593268__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OvenSerilConfigDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COvenSerilConfigDlg dialog


class COvenSerilConfigDlg : public CDialog
{
// Construction
public:
	COvenSerilConfigDlg(CWnd* pParent = NULL);   // standard constructor
	SERIAL_CONFIG m_SerialConfig;

// Dialog Data
	//{{AFX_DATA(COvenSerilConfigDlg)
	enum { IDD = IDD_OVEN_SERIAL_SET_DLG };
	CComboBox	m_ctrlStopBits;
	CComboBox	m_ctrlDataBits;
	CComboBox	m_ctrlBaud;
	CComboBox	m_ctrlPort;
	CComboBox	m_ctrlParity;
	int		m_nPortNum;
	int		m_nBaudRate;
	int		m_nDataBits;
	int		m_nStopBits;
	int		m_nParity;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COvenSerilConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void LoadConfig();
	void SaveConfig();
	CString m_ParityStr;	

	// Generated message map functions
	//{{AFX_MSG(COvenSerilConfigDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
		
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OVENSERILCONFIGDLG_H__6FF82901_7B3F_4DD8_BA12_81495E593268__INCLUDED_)
