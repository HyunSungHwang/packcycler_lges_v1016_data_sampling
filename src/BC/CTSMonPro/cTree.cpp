// cTree.cpp : implementation file
//

#include "stdafx.h" 
 

#include "cTree.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// cTree
IMPLEMENT_DYNAMIC(cTree, CMultiTree)
cTree::cTree()
{
	memDC_bgColor_bitmap = RGB(255,255,255);

	m_pDragImage = NULL;
	m_bLDragging = FALSE; 

	isImageTiled = true ;

	SetDefaultCursor() ;
}

cTree::~cTree()
{
}


BEGIN_MESSAGE_MAP(cTree, CMultiTree)
	//{{AFX_MSG_MAP(cTree)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBeginDrag)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDING, OnItemExpanding)
	ON_WM_ERASEBKGND()
	ON_WM_QUERYNEWPALETTE()
	ON_WM_PALETTECHANGED()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// cTree message handlers

HTREEITEM cTree::AddGroup(const CString & group_name)
{
 	HTREEITEM added_group = 0;
	HTREEITEM groupItem ; 

		if ( (groupItem = GetGroupTreeItem(group_name)) != 0 )  return 0;

	HTREEITEM lastAddedGroup = 0;
		
		//insert the group into the tree
		added_group = InsertItem(group_name, 1,1,0,TVI_FIRST);
	    //Associates 32 bit number with this item
	//	SetItemData(added_group,(DWORD)newGroup);
	
		Expand(added_group,TVE_EXPAND);


	return added_group ;
}

HTREEITEM cTree::GetGroupTreeItem(CString grp_name)
{

	HTREEITEM answer = 0;
	
	HTREEITEM groupItem = GetRootItem()/*(TVI_ROOT, TVGN_NEXT)*/;
	while (groupItem != NULL && !answer)	//while there is still something in the tree
	{
	
		TVITEM item;
		TCHAR szText[1024];
		//CString szText = name;
		item.hItem = groupItem;
		//only get the text and the handle of the item
		item.mask = TVIF_TEXT | TVIF_HANDLE;	
		item.pszText = szText;
		item.cchTextMax = 1024;

		GetItem(&item);

		CString thisGroupName = item.pszText;

		if (thisGroupName == grp_name)
		{
			answer = groupItem;
			break ;
		}
		
		//get the next item for the sake of the while loop ending
		groupItem = GetNextItem(groupItem,TVGN_NEXT);
	}
	return answer;
}

bool cTree::DeleteGroup(CString group_name)
{
	if (group_name.IsEmpty() ) return false ;

	HTREEITEM group_item ;
	if ( (group_item = GetGroupTreeItem(group_name)) == 0 ) return false ;


		//delete every child of this group
		HTREEITEM currentItem = GetNextItem(group_item ,TVGN_CHILD);

		while (currentItem != NULL)
		{
			DeleteItem(currentItem);

			//get the next item for the sake of the while loop ending
			currentItem = GetNextItem(currentItem,TVGN_NEXT);
		}

		//delete the actual group now
		DeleteItem(group_item);

	return true ;
}

HTREEITEM cTree::AddChild(CString buddy_name, CString group_name)
{
 
	HTREEITEM buddy_that_was_added;

	//check if the buddy already exists
	if (GetBuddyTreeItem(buddy_name) != 0) return 0;


		HTREEITEM tree_group = GetGroupTreeItem(group_name);

		//if the group actually exists add the buddy to it
		if (tree_group != 0)
		{
			buddy_that_was_added = InsertItem(buddy_name,2, 2,tree_group,TVI_SORT);

		  //	SetItemData(buddy_that_was_added,(DWORD)newBuddy);
		}
		//create the group then add the buddy
		else	
		{
			tree_group = AddGroup(group_name);
			buddy_that_was_added = InsertItem(buddy_name, 2, 2,tree_group,TVI_SORT);
	
		//	SetItemData(buddyWasAdded,(DWORD)newBuddy);
		}
	

		//this just expands the group the buddy was added to
		HTREEITEM hParent = GetParentItem(buddy_that_was_added);
	
		if (hParent != NULL)	Expand(hParent, TVE_EXPAND);

	
	
	return buddy_that_was_added;
}

HTREEITEM cTree::GetBuddyTreeItem(CString buddy_name)
{
  	HTREEITEM answer = 0;
	//get the root group
	HTREEITEM groupItem = GetRootItem();
	while (groupItem != NULL && !answer)	//while there is still something in the tree
	{
	HTREEITEM currentItem = GetNextItem(groupItem,TVGN_CHILD);
	while (currentItem != NULL && !answer)	//while there is still something in the tree
	{
		TVITEM item;
		TCHAR szText[1024];
		//CString szText = name;
		item.hItem = currentItem;
		//only get the text and the handle of the item
		item.mask = TVIF_TEXT | TVIF_HANDLE;	
		item.pszText = szText;
		item.cchTextMax = 1024;

		/*BOOL answer = */
		GetItem(&item);

		CString thisBuddyName = item.pszText;
		if (thisBuddyName == buddy_name)
			answer = currentItem ;
		else
			answer = 0;

		//get the next item for the sake of the while loop ending
		currentItem = GetNextItem(currentItem,TVGN_NEXT);
	}
		groupItem = GetNextItem(groupItem,TVGN_NEXT);
	}
	return answer;
}

void cTree::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CMultiTree::OnLButtonDblClk(nFlags, point);
	
	HTREEITEM hItem = GetSelectedItem();
	
	// If this is a root node, return
	if (GetParentItem(hItem) == NULL) return ;

}

void cTree::OnRButtonDown(UINT nFlags, CPoint point) 
{

	CMultiTree::OnRButtonDown(nFlags, point);
}

void cTree::OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	*pResult = 0;
	
	// So user cant drag root node
	if (GetParentItem(pNMTreeView->itemNew.hItem) == NULL) return ; 

	// Item user started dragging ...
	if (GetParentItem(pNMTreeView->itemNew.hItem) == NULL) return ; 
	HTREEITEM hLevel1 = GetParentItem(pNMTreeView->itemNew.hItem);
	if(GetParentItem(hLevel1) == NULL) return;
	HTREEITEM hLevel2 = GetParentItem(hLevel1);
	if(GetParentItem(hLevel2) == NULL) return;

	
	
	// Item user started dragging ...
	m_hitemDrag = pNMTreeView->itemNew.hItem;
	m_hitemDrop = NULL;



//	m_pDragImage = CreateDragImage(m_hitemDrag);  // get the image list for dragging
	m_pDragImage = CreateDragImageMulti(&pNMTreeView->ptDrag);  // get the image list for dragging
	

	// CreateDragImage() returns NULL if no image list
	// associated with the tree view control
	if( !m_pDragImage )
		return;

	m_bLDragging = TRUE;
	m_pDragImage->BeginDrag(0, CPoint(-15,-15));
	POINT pt = pNMTreeView->ptDrag;
	ClientToScreen( &pt );
	m_pDragImage->DragEnter(NULL, pt);
	SetCapture();

}

void cTree::OnMouseMove(UINT nFlags, CPoint point) 
{
	HTREEITEM	hitem;
	UINT		flags;
	CRect		rect;
	GetClientRect(&rect);

	if (m_bLDragging)
	{
		POINT pt = point;
		ClientToScreen( &pt );
		CImageList::DragMove(pt);
		if ((hitem = HitTest(point, &flags)) != NULL)
		{
				CImageList::DragShowNolock(FALSE);

				if(point.y  < 3)
				{
					SendMessage(WM_VSCROLL, SB_LINEUP);
				}
				if(point.y < 10)
				{
					SendMessage(WM_VSCROLL, SB_LINEUP);
				}
				if(rect.bottom - point.y < 10)
				{
					SendMessage(WM_VSCROLL, SB_LINEDOWN);
				}


				CString strItemText = GetItemText(hitem);
				//if((strItemText.Left(2) != "Ch" || strItemText.Left(4) != "남은") && (cursor_no != ::GetCursor()))
				if((strItemText.Left(2) != "Ch" || strItemText.Left(4) != Fun_FindMsg("Tree_OnMouseMove_msg1","CTREE")) && (cursor_no != ::GetCursor()))//&&
				{
					::SetCursor(cursor_no);
					// Dont want last selected target highlighted after mouse
					// has moved off of it, do we now ?
					SelectDropTarget(NULL);
				}
				
			// Tests if dragged item is over another child !
		/*	  if ( (GetParentItem(hitem) != NULL) && (cursor_no != ::GetCursor())) 
			  {
				  ::SetCursor(cursor_no);
				   // Dont want last selected target highlighted after mouse
				   // has moved off of it, do we now ?
				   SelectDropTarget(NULL);
			  }*/
			// Is item we're over a root node and not parent root node ?
			//if ( (GetParentItem(hitem) == NULL) && (GetParentItem(m_hitemDrag) != hitem ) ) 
			//if ((strItemText.Left(2) != "Ch" || strItemText.Left(4) != "남은") && (GetParentItem(m_hitemDrag) != hitem ) ) 
				if ((strItemText.Left(2) != "Ch" || strItemText.Left(4) != Fun_FindMsg("Tree_OnMouseMove_msg1","CTREE")) && (GetParentItem(m_hitemDrag) != hitem ) ) //&&
			{
				if (cursor_arr != ::GetCursor()) ::SetCursor(cursor_arr); 
				SelectDropTarget(hitem);
			}

			m_hitemDrop = hitem;
			CImageList::DragShowNolock(TRUE);
		}
	}
	else 
	{
		// Set cursor to arrow if not dragged
		// Otherwise, cursor will stay hand or arrow depen. on prev setting
		::SetCursor(cursor_arr);
	}

	CMultiTree::OnMouseMove(nFlags, point);

}

void cTree::OnLButtonUp(UINT nFlags, CPoint point) 
{

	CMultiTree::OnLButtonUp(nFlags, point);

	if (m_bLDragging)
	{
		m_bLDragging = FALSE;
		CImageList::DragLeave(this);
		CImageList::EndDrag();
		ReleaseCapture();

		if(m_pDragImage != NULL) 
		{ 
		delete m_pDragImage; 
		m_pDragImage = NULL; 
		} 

		// Remove drop target highlighting
		SelectDropTarget(NULL);
		if( m_hitemDrag == m_hitemDrop )
			return;

		HTREEITEM	hitem;
		// Make sure pt is over some item
		if ( ((hitem = HitTest(point, &nFlags)) == NULL)  ) return ;
		// Make sure dropped item isnt a child
//		if (GetParentItem(hitem) != NULL) return ;

		CString strItemText = GetItemText(hitem);
		if(strItemText.Left(2) != "Ch")
		{
			HTREEITEM hParent = GetParentItem(hitem);
			CString strParent = GetItemText(hParent);
			if(strParent.Left(2) == "Ch")
			{
				m_hitemDrop = hParent;
				strItemText = strParent;
			}

		}

		//if(strItemText.Left(2) == "Ch" || strItemText.Left(4) == "남은")
		if(strItemText.Left(2) == "Ch" || strItemText.Left(4) == Fun_FindMsg("Tree_OnMouseMove_msg1","CTREE"))//&&
		{
			// If Drag item is an ancestor of Drop item then return
			HTREEITEM htiParent = m_hitemDrop;
			while( (htiParent = GetParentItem( htiParent )) != NULL )
			{
				if( htiParent == m_hitemDrag ) return;
			}

			Expand( m_hitemDrop, TVE_EXPAND ) ;

			HTREEITEM sendItem = GetFirstSelectedItem();
			CString strMsg;
			HTREEITEM deleteItem[1024];
			int nCnt = 0;
			while(sendItem)
			{
				HTREEITEM htiNew = MoveChildItem( sendItem, m_hitemDrop, TVI_LAST );
				//SelectItem( htiNew );
		
				WPARAM wParam = (WPARAM)m_hitemDrop;
				LPARAM lParam = (LPARAM)htiNew;
				this->GetParent()->SendMessage(WM_TREE_ITEM_DRAG_DROP, wParam, lParam);
				strMsg += GetItemText(htiNew);
				deleteItem[nCnt] = sendItem;
				sendItem = GetNextSelectedItem(sendItem);
				nCnt++;			
			}

			sendItem = GetFirstSelectedItem();
			DeleteItem(sendItem);
			while(nCnt>0)
			{
				DeleteItem(deleteItem[nCnt-1]);
				nCnt--;			
			}
		}

		
	}

}

HTREEITEM cTree::MoveChildItem(HTREEITEM hItem, HTREEITEM htiNewParent, HTREEITEM htiAfter)
{

	TV_INSERTSTRUCT tvstruct;
	HTREEITEM hNewItem;
    CString sText;

    // get information of the source item
    tvstruct.item.hItem = hItem;
    tvstruct.item.mask = TVIF_CHILDREN | TVIF_HANDLE |     TVIF_IMAGE | TVIF_SELECTEDIMAGE;
    GetItem(&tvstruct.item);  
    sText = GetItemText( hItem );
        
    tvstruct.item.cchTextMax = sText.GetLength();
    tvstruct.item.pszText = sText.LockBuffer();

    //insert the item at proper location
    tvstruct.hParent = htiNewParent;
    tvstruct.hInsertAfter = htiAfter;
    tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
    hNewItem = InsertItem(&tvstruct);
    sText.ReleaseBuffer();

    //now copy item data and item state.
    SetItemData(hNewItem,GetItemData(hItem));
    SetItemState(hNewItem,GetItemState(hItem,TVIS_STATEIMAGEMASK),TVIS_STATEIMAGEMASK);

	//now delete the old item
//	DeleteItem(hItem);

    return hNewItem;
}


bool cTree::DeleteChild(CString buddy_name)
{
	if (buddy_name.IsEmpty()) return false;


	HTREEITEM buddyItem = GetBuddyTreeItem(buddy_name);

	if (buddyItem != 0)
	{
		DeleteItem(buddyItem);
		return true ; 
	}

	return false;
}

int cTree::GetChildCountInGroup(CString group_name)
{

	HTREEITEM group_item = GetGroupTreeItem(group_name);	//get the group
	
	if (group_item == 0)					return -1 ;
	if (!ItemHasChildren(group_item))		return  0 ;

		int total_in_group =0 ;
		
		//delete every child of this group
		HTREEITEM currentItem = GetNextItem(group_item ,TVGN_CHILD);

		while (currentItem != NULL)
		{
			++total_in_group ;

			//get the next item for the sake of the while loop ending
			currentItem = GetNextItem(currentItem,TVGN_NEXT);
		}

	return total_in_group ;
}

CString cTree::RemoveBuddyCountFromName(CString name)
{
	if (name.Find("(") > 0)
	{
		name = name.Left(name.Find("(") - 1);
	}
	else return name;

	return name;
}

/*
void cTree::CollapseAll()
{
        HTREEITEM hti = GetRootItem();
        do{
             Expand( hti, TVE_COLLAPSE ); //     CollapseBranch( hti );
        }while( (hti = GetNextSiblingItem( hti )) != NULL );
}

  */

void cTree::OnPaint() 
{

		// Remove comments from next five lines if you don't need any 
	// specialization beyond adding a background image
//	if( m_bitmap.m_hObject == NULL )
//	{
//		CMultiTree::OnPaint();
//		return;
//	}

	CPaintDC dc(this);

	CRect rcClip, rcClient;
	dc.GetClipBox( &rcClip );
	GetClientRect(&rcClient);

	// Create a compatible memory DC 
	CDC memDC;
	memDC.CreateCompatibleDC( &dc );
	
	// Select a compatible bitmap into the memory DC
	CBitmap bitmap, bmpImage;
	bitmap.CreateCompatibleBitmap( &dc, rcClient.Width(), rcClient.Height() );
	memDC.SelectObject( &bitmap );

	
	// First let the control do its default drawing.
	CWnd::DefWindowProc( WM_PAINT, (WPARAM)memDC.m_hDC, 0 );

	// Draw bitmap in the background if one has been set
	if( m_bitmap.m_hObject != NULL )
	{
		// Now create a mask
		CDC maskDC;
		maskDC.CreateCompatibleDC(&dc);
		CBitmap maskBitmap;

		// Create monochrome bitmap for the mask
		maskBitmap.CreateBitmap( rcClient.Width(), rcClient.Height(), 
						1, 1, NULL );
		maskDC.SelectObject( &maskBitmap );
		memDC.SetBkColor(RGB(255,255,255) /*::GetSysColor( COLOR_WINDOW )*/ );

		// Create the mask from the memory DC
		maskDC.BitBlt( 0, 0, rcClient.Width(), rcClient.Height(), &memDC, 
					rcClient.left, rcClient.top, SRCCOPY );

		
		CDC tempDC;
		tempDC.CreateCompatibleDC(&dc);
		tempDC.SelectObject( &m_bitmap );

		CDC imageDC;
		CBitmap bmpImage;
		imageDC.CreateCompatibleDC( &dc );
		bmpImage.CreateCompatibleBitmap( &dc, rcClient.Width(), 
						rcClient.Height() );
		imageDC.SelectObject( &bmpImage );

		if( dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE && m_pal.m_hObject != NULL )
		{
			dc.SelectPalette( &m_pal, FALSE );
			dc.RealizePalette();

			imageDC.SelectPalette( &m_pal, FALSE );
		}

		// Get x and y offset
		CRect rcRoot;
		GetItemRect( GetRootItem(), rcRoot, FALSE );
		rcRoot.left = -GetScrollPos( SB_HORZ );

	
		if (isImageTiled)
		{
			// Draw bitmap in tiled manner to imageDC
			for( int i = rcRoot.left; i < rcClient.right; i += m_cxBitmap )
			for( int j = rcRoot.top; j < rcClient.bottom; j += m_cyBitmap )
				imageDC.BitBlt( i, j, m_cxBitmap, m_cyBitmap, &tempDC, 
							0, 0, SRCCOPY );
		}else
		{
			int x=0,y=0 ;
			(m_cxBitmap > rcClient.right) ? x=0:x=(rcClient.right - m_cxBitmap);
			(m_cyBitmap > rcClient.bottom)? y=0:y=(rcClient.bottom - m_cyBitmap);
			imageDC.BitBlt( x, y, m_cxBitmap, m_cyBitmap, &tempDC, 
							0, 0, SRCCOPY );
		}

		// Set the background in memDC to black. Using SRCPAINT with black and any other
		// color results in the other color, thus making black the transparent color
		memDC.SetBkColor( RGB(0,0,0)/*memDC_bgColor_bitmap*/);        
		memDC.SetTextColor(RGB(255,255,255));
		memDC.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &maskDC, 
				rcClip.left, rcClip.top, SRCAND);

		// Set the foreground to black. See comment above.
		imageDC.SetBkColor(RGB(255,255,255));
		imageDC.SetTextColor(RGB(0,0,0));
		imageDC.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &maskDC, 
				rcClip.left, rcClip.top, SRCAND);

		// Combine the foreground with the background
		imageDC.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), 
					&memDC, rcClip.left, rcClip.top,SRCPAINT);

		//*****************************
			/*	for( int yy = 0; yy < rcClient.Height(); yy++)
				for( int xx = 0; xx < rcClient.Width(); xx++ )
			{
				if (imageDC.GetPixel(CPoint(xx,yy)) == RGB(0,0,0))
					imageDC.SetPixel(CPoint(xx,yy),RGB(255,255,255));
			}
			 Create a compatible memory DC 48068
			CDC whiteDC;
			whiteDC.CreateCompatibleDC( &dc );
	
			// Select a compatible bitmap into the memory DC
				CBitmap cBmp;
			blankBmp.CreateCompatibleBitmap( &dc, rcClient.Width(), rcClient.Height() );
			whiteDC.SelectObject( &blankBmp );*/
		//*****************************

		// Draw the final image to the screen		
		dc.BitBlt( rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), 
					&imageDC, rcClip.left, rcClip.top, SRCCOPY );
	}
	else
	{
		dc.BitBlt( rcClip.left, rcClip.top, rcClip.Width(), 
				rcClip.Height(), &memDC, 
				rcClip.left, rcClip.top, SRCCOPY );
	}

}

void cTree::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if( m_bitmap.m_hObject != NULL ) InvalidateRect(NULL);

	CMultiTree::OnHScroll(nSBCode, nPos, pScrollBar);

}

void cTree::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if( m_bitmap.m_hObject != NULL )	InvalidateRect(NULL);

	CMultiTree::OnVScroll(nSBCode, nPos, pScrollBar);

}

void cTree::OnItemExpanding(NMHDR* pNMHDR, LRESULT* pResult) 
{
	
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	if( m_bitmap.m_hObject != NULL ) InvalidateRect(NULL);
	
	*pResult = 0;

}

BOOL cTree::OnEraseBkgnd(CDC* pDC) 
{
	if( m_bitmap.m_hObject != NULL )	return TRUE;

	return CMultiTree::OnEraseBkgnd(pDC);

}

BOOL cTree::OnQueryNewPalette() 
{

	CClientDC dc(this);
	if( dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE && m_pal.m_hObject != NULL )
	{
		dc.SelectPalette( &m_pal, FALSE );
		BOOL result = dc.RealizePalette();
		if( result )
			Invalidate();
		return result;
	}
	
	return CMultiTree::OnQueryNewPalette();
}

void cTree::OnPaletteChanged(CWnd* pFocusWnd) 
{
	CMultiTree::OnPaletteChanged(pFocusWnd);
	
	if( pFocusWnd == this )
		return;

	OnQueryNewPalette();

}

BOOL cTree::SetBkImage(UINT nIDResource)
{
	return SetBkImage( (LPCTSTR)nIDResource );
}

BOOL cTree::SetBkImage(LPCTSTR lpszResourceName)
{

	// If this is not the first call then Delete GDI objects
	if( m_bitmap.m_hObject != NULL )
		m_bitmap.DeleteObject();
	if( m_pal.m_hObject != NULL )
		m_pal.DeleteObject();
	
	
	HBITMAP hBmp = (HBITMAP)::LoadImage( AfxGetInstanceHandle(), 
			lpszResourceName, IMAGE_BITMAP, 0,0, LR_CREATEDIBSECTION );

	if( hBmp == NULL ) 
		return FALSE;

	m_bitmap.Attach( hBmp );
	BITMAP bm;
	m_bitmap.GetBitmap( &bm );
	m_cxBitmap = bm.bmWidth;
	m_cyBitmap = bm.bmHeight;


	// Create a logical palette for the bitmap
	DIBSECTION ds;
	BITMAPINFOHEADER &bmInfo = ds.dsBmih;
	m_bitmap.GetObject( sizeof(ds), &ds );

	int nColors = bmInfo.biClrUsed ? bmInfo.biClrUsed : 1 << bmInfo.biBitCount;

	// Create a halftone palette if colors > 256. 
	CClientDC dc(NULL);			// Desktop DC
	if( nColors > 256 )
		m_pal.CreateHalftonePalette( &dc );
	else
	{
		// Create the palette

		RGBQUAD *pRGB = new RGBQUAD[nColors];
		CDC memDC;
		memDC.CreateCompatibleDC(&dc);

		memDC.SelectObject( &m_bitmap );
		::GetDIBColorTable( memDC, 0, nColors, pRGB );

		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * nColors);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];

		pLP->palVersion = 0x300;
		pLP->palNumEntries = nColors;

		for( int i=0; i < nColors; i++)
		{
			pLP->palPalEntry[i].peRed = pRGB[i].rgbRed;
			pLP->palPalEntry[i].peGreen = pRGB[i].rgbGreen;
			pLP->palPalEntry[i].peBlue = pRGB[i].rgbBlue;
			pLP->palPalEntry[i].peFlags = 0;
		}

		m_pal.CreatePalette( pLP );

		delete[] pLP;
		delete[] pRGB;
	}
	Invalidate();

	return TRUE;
}



void cTree::SetDefaultCursor()
{
	   // Get the windows directory
        CString strWndDir;
        GetWindowsDirectory(strWndDir.GetBuffer(MAX_PATH), MAX_PATH);
        strWndDir.ReleaseBuffer();

        strWndDir += _T("\\winhlp32.exe");
        // This retrieves cursor #106 from winhlp32.exe, which is a hand pointer
        HMODULE hModule = LoadLibrary(strWndDir);
        if (hModule) {
            HCURSOR hHandCursor = ::LoadCursor(hModule, MAKEINTRESOURCE(106));
            if (hHandCursor)
			{
                cursor_hand = CopyCursor(hHandCursor);
			}
			      
        }
        FreeLibrary(hModule);

		cursor_arr	= ::LoadCursor(NULL, IDC_ARROW);
		cursor_no	= ::LoadCursor(NULL, IDC_NO) ;
}

void cTree::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect		rect;
	GetClientRect(&rect);

	POINT pt = point;
	ClientToScreen( &pt );
		
	//TRACE("X : %d / Y : %d\r\n", point.x, point. y);
	//TRACE("Top : %d / Bottol : %d\r\n", rect.top, rect.bottom);
	if(point.y < 10)
	{
		SendMessage(WM_VSCROLL, SB_LINEUP);
	}
	if(rect.bottom - point.y < 10)
	{
		SendMessage(WM_VSCROLL, SB_LINEDOWN);
	}
	CMultiTree::OnLButtonDown(nFlags, point);
}


CImageList * cTree::CreateDragImageMulti(LPPOINT lpPoint)
{
	CRect	cSingleRect;
	CRect	cCompleteRect( 0,0,0,0 );
	BOOL	bFirst = TRUE;
	COLORREF bkColor(RGB(255,255,255));
	//
	// Determine the size of the drag image
	//
	HTREEITEM hItem = GetFirstSelectedItem();
	while (hItem)
	{
		GetItemRect( hItem, cSingleRect, LVIR_BOUNDS );
		if (bFirst)
		{
			// Initialize the CompleteRect
			GetItemRect( hItem, cCompleteRect, LVIR_BOUNDS );
			bFirst = FALSE;
		}
		cCompleteRect.UnionRect( cCompleteRect, cSingleRect );
		hItem = GetNextSelectedItem( hItem );
	}

	//
	// Create bitmap in memory DC
	//
	CClientDC	cDc(this);
	CDC		cMemDC;
	CBitmap   	cBitmap;

	if(!cMemDC.CreateCompatibleDC(&cDc))
		return NULL;

	if(!cBitmap.CreateCompatibleBitmap(&cDc, cCompleteRect.Width(), cCompleteRect.Height()))
		return NULL;

	CBitmap* pOldMemDCBitmap = cMemDC.SelectObject( &cBitmap );
	// Here green is used as mask color
	cMemDC.FillSolidRect(0,0,cCompleteRect.Width(), cCompleteRect.Height(), RGB(0, 255, 0));

	//
	// Paint each DragImage in the DC
	//
	CImageList *pSingleImageList;

	hItem = GetFirstSelectedItem();
	while (hItem)
	{

		CSize size;
		GetItemRect( hItem, cSingleRect, TRUE );
				
		size = cSingleRect.Size();
		size.cx += 22;	

		pSingleImageList = CreateDragImage( hItem);
		if (pSingleImageList)
		{
			
			pSingleImageList->DrawIndirect( &cMemDC,
							0,
							CPoint( cSingleRect.left-cCompleteRect.left,
							cSingleRect.top-cCompleteRect.top ),
							//cSingleRect.Size(),
							size,
							CPoint(0,0),ILD_NORMAL, 
							SRCCOPY, bkColor,CLR_DEFAULT);
			//pSingleImageList->SetBkColor(bkColor);
			delete pSingleImageList;
			
		}
		hItem = GetNextSelectedItem( hItem );
	}

	cMemDC.SelectObject( pOldMemDCBitmap );
	//
	// Create the imagelist	with the merged drag images
	//
	CImageList* pCompleteImageList = new CImageList;

	pCompleteImageList->Create(	cCompleteRect.Width(),
					cCompleteRect.Height(),
					ILC_COLOR | ILC_MASK, 0, 1);
	// Here green is used as mask color
	pCompleteImageList->Add(&cBitmap, RGB(0, 255, 0));

	cBitmap.DeleteObject();
	//
	// as an optional service:
	// Find the offset of the current mouse cursor to the imagelist
	// this we can use in BeginDrag()
	//
	if ( lpPoint )
	{
		CPoint cCursorPos;
		GetCursorPos( &cCursorPos );
		ScreenToClient( &cCursorPos );
		lpPoint->x = cCursorPos.x - cCompleteRect.left;
		lpPoint->y = cCursorPos.y - cCompleteRect.top;
	}

	return( pCompleteImageList );
}