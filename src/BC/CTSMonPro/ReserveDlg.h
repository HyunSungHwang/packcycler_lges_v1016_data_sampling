#if !defined(AFX_RESERVEDLG_H__40AA2D6E_BDA1_41C7_AD3D_C8A0FA5830CF__INCLUDED_)
#define AFX_RESERVEDLG_H__40AA2D6E_BDA1_41C7_AD3D_C8A0FA5830CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReserveDlg.h : header file
// ksj 20160426 작업 예약 기능 추가
//

#define WORK_START_NORMAL			0		//일반 작업 시작
#define WORK_START_RESERV			1		//작업 예약
#define WORK_START_RESERV_RUN		2		//예약 작업 시작

#define RUN_FAIL					0
#define RUN_OK						1
#define RESERV_OK					2

#define RESERV_ON_BY_CH				1		//yulee 20190503 채널별 ON OFF 
#define RESERV_OFF_BY_CH			0
#define LIST_INDEX_PRIORITY			0
#define LIST_INDEX_STATE			1
#define LIST_INDEX_CHANNEL			2
#define LIST_INDEX_MODEL_NAME		3
#define LIST_INDEX_SCH_NAME			4
#define LIST_INDEX_WORK_NAME		5
#define LIST_INDEX_WORKER_NAME		6
#define LIST_INDEX_RESERVED_TIME	7
#define LIST_INDEX_START_TIME		8
#define LIST_INDEX_LOT				9
#define LIST_INDEX_COMMENT			10
#define LIST_INDEX_CHAMBER			11
#define LIST_INDEX_MUX				12//yulee 20181025

#define RESERV_STATE_WAITING		0
#define RESERV_STATE_WORKING		1
#define RESERV_STATE_FAILED			2
#define RESERV_STATE_STOPPED		3
#define RESERV_STATE_PAUSED  		4
#define RESERV_STATE_DOWNRAWDATA	5//yulee 20190420_1

#define TIMER_CHECK_START			1001  //작업 시작 가능한지 확인
#define TIMER_DELAYED_STARTBEFDOWN  1002  //일정 딜레이 후 작업 시작
#define TIMER_DELAYED_START			1003  //일정 딜레이 후
#define TIMER_CHECK_SBCDATADOWN		1004  //Check Raw Data Down Done yulee 20190420 구현 전 

#define TIMER_CHECK_START_ELAPSE	10000

#define MAX_RESERV_QUEUE			64

#define RESERVATION_REGISTRY_KEY					"Reservation"
#define RESERVATION_REGISTRY_KEY_CONFIG_ENABLE		"Use Run Queue" //작업 예약 활성화 레지스트리 항목
#define RESERVATION_REGISTRY_KEY_CONFIG_ENABLE_VAL	1				//레지스트리에 값이 없을 경우 기본 값. (0: 비활성화 1: 활성화)

//ksj 20160426 작업 예약시 저장할 데이터
typedef struct _S_WORK_RESERVATION_DATA
{
	int nIndex;
	int nPriority;
	int nState;
	//CString strAdwChArray;
	//CString strModelName;
	//CString strSchName;
	//CString strTestName;
	//CString strWorkerName;
	//CString strReservedTime;
	//CString strStartTime;
	//CString strEndTime; //사용안함
	int nScheduleInfoModelPK;
	int nScheduleInfoTestPK;
	int nStartCycle;
	int nStartStep;
	int nStartOptChamber;
	//CString strLot;
	//CString strComment;
	//CString strDataPath;
	//CString strSchPath;
	float	fChamberFixTemp;
	UINT	uiChamberPatternNum;
	float	fChamberDeltaTemp;
	UINT	uiChamberDelayTime;
	float	fChamberDeltaFixTemp;
	BOOL	bEnableStepSkip;
	BOOL	bChamberContinue;
	BOOL	bIsChamberContinue;
	UINT	uiChamberCheckTime;
	UINT	uiChamberCheckFixTime;
	BOOL	bIsCanModeCheck;
	UINT	uiMuxOption; //yulee 20181025

	// 200313 HKH Memory Leak 수정 ==========
	char szAdwChArray[256];
	char szModelName[256];
	char szSchName[256];
	char szTestName[256];
	char szWorkerName[256];
	char szReservedTime[256];
	char szStartTime[256];
	char szEndTime[256]; //사용안함
	char szLot[256];
	char szComment[256];
	char szDataPath[256];
	char szSchPath[256];
	// ======================================
	
}S_WORK_RESERVATION_DATA, *LPS_WORK_RESERVATION_DATA;

class CCTSMonProDoc;
class CCTSMonProView;
class CStartDlgEx;
/////////////////////////////////////////////////////////////////////////////
// CReserveDlg dialog

class CReserveDlg : public CDialog
{
// Construction
public:
	int m_nID;
	int m_nRunFailCnt;
	int m_SearchCh;//yulee 20190420_1
	CPoint m_ptDragFirstPoint;
	int m_nDragItemIndex;
	BOOL m_bDraging;
	void ReserveAdd();
	UINT m_nDelayedStartWaitTime;
	UINT m_nWorkCount;
	CStatusBar m_StatusBar;
	void RunAnalyzer(int nType);
	BOOL SetListItem(int nI, LPS_WORK_RESERVATION_DATA pReservData, BOOL bOpt = TRUE);
	BOOL ExecuteSQL(CString strSQL);
	BOOL ChangePriority(LPS_WORK_RESERVATION_DATA pWork, int nPriority);
	int m_nProgressCnt;
	int RemoveCompleteWork();
	BOOL m_bWorking;
	BOOL GetCntWaitingResv(int &ChCnt1, int &ChCnt2, int &ChCnt3, int &ChCnt4); //yulee 20190420_2
	BOOL IsChWaitingRsvWork(int nSchCh, int &bRsvChExist, int nSearchMax, int Ch1Value, int Ch2Value, int Ch3Value, int Ch4Value);//yulee 20190420_2
	int ChangeState(int nIndex, int nState,CString strCH=_T(""));
	void CheckReservedWork();
	BOOL RestoreReservedWork(LPVOID pWork, CScheduleData* pScheduleInfo, CDWordArray* pAdwChArray, CStartDlgEx *pStartDlg,int *pModelPK, int *pTestPK);
	BOOL CheckCanBeStart(CDWordArray* pAdwChArray, int nStartOptChamber);
	void ResetReservDataArray();
	CString ConvertStateCodeToString(int nState);
	CString ConvertAdwChArrayToString(CDWordArray* pAdwChArray);
	BOOL ConvertAdwChArrayFromString(CString strAdwChArray, CDWordArray* pAdwChArray);
	void InitDatabase();
	CString m_strDatabaseName;
	BOOL AddReservData(CScheduleData* pScheduleInfo, int nModelPK, int nTestPK, CDWordArray* pAdwChArray, CStartDlgEx* pDlg1, CString strSchPath);
	BOOL RequeryReservList();
	void InitList();
	CReserveDlg(CCTSMonProDoc* pDoc, CCTSMonProView* pParent);   // standard constructor
	virtual ~CReserveDlg();	//	200317 HKH Memory Leak 관련 수정
	BOOL DelayedStart(int nType = 0);
	BOOL Fun_DataDownFailProcess();//yulee 20190430
	BOOL CheckSBCDataDown();//yulee 20190420_1

	BOOL CheckResvSbcStop(CCyclerChannel *pChannel);	
	BOOL SetChResvStopBySbcStoporPause(); //yulee 20190503
	LPS_WORK_RESERVATION_DATA m_pTopPriorityWork; //yulee 20190420_2
	CCTSMonProDoc* m_pDoc;
	CCTSMonProView* m_pParent;
	CDWordArray* m_pTargetChArray;
	CPtrArray	m_ptrReservData;
	//LPS_WORK_RESERVATION_DATA m_pTopPriorityWork;
// Dialog Data
	//{{AFX_DATA(CReserveDlg)
	enum { IDD = IDD_RESERVE_DLG , IDD2 = IDD_WORK_RESERVE_DLG_ENG , IDD3 = IDD_WORK_RESERVE_DLG_PL };
	CXPButton	m_btnDown;
	CXPButton	m_btnUp;
	CXPButton	m_btnOk;
	CXPButton	m_btnWorkDel;
	CXPButton	m_btnWorkAdd;
	CXPButton	m_btnWorkStart;
	CProgressCtrl	m_ctrlProgress;
	CListCtrl	m_listReserveQueue;
	CButton m_bRESRV_Ch01_ON_OFF;//yulee 20190503
	CButton m_bRESRV_Ch02_ON_OFF;//yulee 20190503
	CButton m_bRESRV_Ch03_ON_OFF;//yulee 20190503
	CButton m_bRESRV_Ch04_ON_OFF;//yulee 20190503
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReserveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CReserveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonReserveAdd();
	afx_msg void OnButtonTest();
	afx_msg void OnWorkStartStop();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBegindragListRunQueue(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnButtonUp();
	afx_msg void OnButtonDown();
	afx_msg void OnButtonReserveDel();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRclickListRunQueue(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnWorkLogView();
	afx_msg void OnCheckCond();
	afx_msg void OnDataView();
	afx_msg void OnGraphView();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnDestroy();
	afx_msg void OnReservCancel();
	afx_msg void OnClear();
	afx_msg void OnBeginrdragListRunQueue(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//afx_msg void OnWorkStartTest();
	afx_msg void OnButtonReserveRestart();
	afx_msg void OnRadReservCh1();
	afx_msg void OnRadReservCh2();
	afx_msg void OnRadReservCh3();
	afx_msg void OnRadReservCh4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	UINT m_nDelayedStartSec;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESERVEDLG_H__40AA2D6E_BDA1_41C7_AD3D_C8A0FA5830CF__INCLUDED_)
