// ExitProgressWnd.cpp : 구현 파일입니다.
// 2020-10-23, Kang se-jun
// 프로그램 종료시 종료 처리 진행상황을 표시해주기 위한 UI 추가

#include "stdafx.h"
#include "CTSMonPro.h"
#include "ExitProgressWnd.h"


// CExitProgressWnd 대화 상자입니다.

IMPLEMENT_DYNAMIC(CExitProgressWnd, CDialogEx)

CExitProgressWnd::CExitProgressWnd(CWnd* pParent /*=NULL*/)
	: CDialogEx(CExitProgressWnd::IDD, pParent)
{
	Create(IDD_EXIT_PROGRESS_DLG);
}

CExitProgressWnd::~CExitProgressWnd()
{
}

void CExitProgressWnd::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXIT_PROGRESS_BAR, m_ctrlProgressBar);
}


BEGIN_MESSAGE_MAP(CExitProgressWnd, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_FORCE_EXIT, &CExitProgressWnd::OnBnClickedButtonForceExit)
END_MESSAGE_MAP()


// CExitProgressWnd 메시지 처리기입니다.


BOOL CExitProgressWnd::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetBackgroundColor(RGB(255,255,255));
	m_ctrlProgressBar.SetRange32(0,100);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CExitProgressWnd::SetProgress(int nPos)
{
	m_ctrlProgressBar.SetPos(nPos);
}


void CExitProgressWnd::OnBnClickedButtonForceExit()
{

	if(MessageBox("Force exit","exit",MB_YESNO|MB_ICONWARNING) == IDNO) 
		return;

	//프로그램 강제 종료.
	exit(0);
}
