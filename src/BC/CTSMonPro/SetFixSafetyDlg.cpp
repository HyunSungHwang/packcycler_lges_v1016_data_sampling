// SetFixSafetyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMonPro.h"
#include "SetFixSafetyDlg.h"
#include "afxdialogex.h"
#include "LoginManagerDlg.h"
#include "CTSMonProDoc.h"

// CSetFixSafetyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSetFixSafetyDlg, CDialog)

CSetFixSafetyDlg::CSetFixSafetyDlg(CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSetFixSafetyDlg::IDD, pParent)
	, m_strAuxTempLimitH(_T(""))
	, m_strAuxVoltLimitH(_T(""))
	, m_strAuxVoltLimitL(_T(""))
{
	m_pDoc = pDoc;
}

CSetFixSafetyDlg::~CSetFixSafetyDlg()
{
}

void CSetFixSafetyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_AUX_TEMP_LIMIT_H, m_strAuxTempLimitH);
	DDX_Text(pDX, IDC_EDIT_AUX_VOLT_LIMIT_H, m_strAuxVoltLimitH);
	DDX_Text(pDX, IDC_EDIT_AUX_VOLT_LIMIT_L, m_strAuxVoltLimitL);
}


BEGIN_MESSAGE_MAP(CSetFixSafetyDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSetFixSafetyDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSetFixSafetyDlg 메시지 처리기입니다.




BOOL CSetFixSafetyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.	
	int nSafetyFixTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", 80);
	int nSafetyCellVolHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", 4600);
	int nSafetyCellVolLow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltLow", 0);
	

	m_strAuxTempLimitH.Format("%d",nSafetyFixTempHigh);
	m_strAuxVoltLimitH.Format("%d",nSafetyCellVolHigh);
	m_strAuxVoltLimitL.Format("%d",nSafetyCellVolLow);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


//ksj 20200116 : 고정 안전 설정 저장
//저장시 로그인 창을 띄워 한번 더 체크하도록 한다.
//변경한 사용자 ID를 로그에 남긴다.
//변경한 값은 변경 전 후 값을 로그에 남긴다.
//SBC에 AuxSetData 로 설정 값 전송해야함.
void CSetFixSafetyDlg::OnBnClickedOk()
{
	ASSERT(m_pDoc);

	CString strLog;

	//채널들이 사용중인지 확인. 모든 채널이 비가동중이어야 변경가능
	if(!CheckRunState()) return;

	//로그인창 띄워 한번 더 확인
	CLoginManagerDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;	

	//변경전 값 백없.
	CString strAuxTempLimitH_OLD = m_strAuxTempLimitH;
	CString strAuxVoltLimitH_OLD = m_strAuxVoltLimitH;
	CString strAuxVoltLimitL_OLD = m_strAuxVoltLimitL;

	//갱신
	UpdateData();
	
	//저장 시작
	
	strLog.Format("================고정안전기능설정===========================");
	m_pDoc->WriteLog(strLog);
	strLog.Format("ID : %s 사용자가 안전조건을 변경하였습니다.", LoginDlg.m_strUserID);
	m_pDoc->WriteLog(strLog);
	strLog.Format("====================================================");
	m_pDoc->WriteLog(strLog);

	//레지스트리에 기록
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", atoi(m_strAuxTempLimitH));
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", atoi(m_strAuxVoltLimitH));	
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltLow", atoi(m_strAuxVoltLimitL));

	//변경 내역 로깅.
	strLog.Format("외부온도상한 변경 %s'C -> %s'C", strAuxTempLimitH_OLD, m_strAuxTempLimitH);
	m_pDoc->WriteLog(strLog);
	strLog.Format("외부전압상한 변경 %smV -> %smV", strAuxVoltLimitH_OLD, m_strAuxVoltLimitH);
	m_pDoc->WriteLog(strLog);
	strLog.Format("외부온도하한 변경 %smV -> %smV", strAuxVoltLimitL_OLD, m_strAuxVoltLimitL);
	m_pDoc->WriteLog(strLog);

	strLog.Format("====================================================");
	m_pDoc->WriteLog(strLog);

	UpdateAuxData(); //테스트

	MessageBox("설정이 적용되었습니다.","안전설정",MB_ICONINFORMATION);

	CDialog::OnOK();
}


//ksj 20200116 : 가동중인 채널이 있는지 확인
BOOL CSetFixSafetyDlg::CheckRunState(void)
{	
	//채널들이 사용중인지 확인. 모든 채널이 비가동중이어야 변경가능
	CString strLog;
	int nModuleCnt = m_pDoc->GetInstallModuleCount();

	for(int nModuleIdx=0; nModuleIdx < nModuleCnt; nModuleIdx++)
	{
		int nModuleID = m_pDoc->GetModuleID(nModuleIdx);
		CCyclerModule *pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(!pMD) continue;

		int nTotalChCnt = pMD->GetTotalChannel();

		for(int nChIdx=0; nChIdx < nTotalChCnt; nChIdx++)
		{
			CCyclerChannel *pCH = pMD->GetChannelInfo(nChIdx);
			if(!pCH) continue;

			//가동중인 채널 있으면 리턴
			WORD wState = pCH->GetState();
			if(wState == PS_STATE_RUN
				|| wState == PS_STATE_PAUSE
				|| wState == PS_STATE_LINE_OFF //임시 테스트
				)
			{
				strLog.Format("Module %d의 채널 %d가 사용중이므로 설정을 변경할 수 없습니다.",nModuleID, nChIdx+1);
				MessageBox(strLog,"안전설정",MB_ICONERROR);
				return FALSE;
			}
		}

	}

	return TRUE;
}

//ksj 20200116 : 설정 값을 SBC에 전송한다.
//코드 개선 필요.
BOOL CSetFixSafetyDlg::UpdateAuxData(void)
{
	int nModuleID = 1;

	SFT_MD_AUX_INFO_DATA *pAuxInfoData = SFTGetAuxInfoData(nModuleID);

	if(!pAuxInfoData) return FALSE;

	//설정값을 일괄적으로 담아서 업데이트
	int i=0;
	for(i=0;i<_SFT_MAX_MAPPING_AUX;i++)
	{
		pAuxInfoData->auxData[i].vent_lower = 0;
		pAuxInfoData->auxData[i].vent_upper = 0;
	}

	m_pDoc->SendAuxDatatoModule(nModuleID,(STF_MD_AUX_SET_DATA*)pAuxInfoData->auxData);

	return TRUE;
}
