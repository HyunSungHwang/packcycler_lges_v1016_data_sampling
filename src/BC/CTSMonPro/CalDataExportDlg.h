#if !defined(AFX_CALDATAEXPORTDLG_H__FB59F249_B326_424D_8083_9A022300230A__INCLUDED_)
#define AFX_CALDATAEXPORTDLG_H__FB59F249_B326_424D_8083_9A022300230A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalDataExportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCalDataExportDlg dialog

class CCalDataExportDlg : public CDialog
{
// Construction
public:
	CCalDataExportDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCalDataExportDlg)
	enum { IDD = IDD_CAL_REPORT_DIALOG , IDD2 = IDD_CAL_REPORT_DIALOG_ENG , IDD3 = IDD_CAL_REPORT_DIALOG_PL  };
	BOOL	m_bVertial;
	BOOL	m_bHorizental;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalDataExportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCalDataExportDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALDATAEXPORTDLG_H__FB59F249_B326_424D_8083_9A022300230A__INCLUDED_)
