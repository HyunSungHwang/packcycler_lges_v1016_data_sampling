#if !defined(AFX_SETUPOVEN_H__BE33A4FF_D335_4E8D_9189_DFA7241DAE88__INCLUDED_)
#define AFX_SETUPOVEN_H__BE33A4FF_D335_4E8D_9189_DFA7241DAE88__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetupOven.h : header file
//
#include "CTSMonProDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CSetupOven dialog

class CSetupOven : public CDialog
{
// Construction
public:
	
	void SaveOvenData();
	void UpdateRemainChList();
	void UpdateOvenList();
	void UpdateOvenChMapList();
	void LoadOvenData();
	void InitList();
	COvenCtrl	*m_pCtrlOven;
	CSetupOven(CCTSMonProDoc * pDoc, int * nPortLine, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetupOven)
	enum { IDD = IDD_SETUP_OVEN_DLG ,  IDD2 = IDD_SETUP_OVEN_DLG_ENG ,IDD3 = IDD_SETUP_OVEN_DLG_PL };
	CListCtrl	m_ctrlOvenChMapList;
	CListCtrl	m_ctrlChList;
	CComboBox	m_ctrlOvenList;
	BOOL	m_bUseDoorOpenSensor;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupOven)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCTSMonProDoc * m_pDoc;
	int	 m_nPortLine;		//ljb 

	COvenCtrl m_OvenList;
	

	// Generated message map functions
	//{{AFX_MSG(CSetupOven)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBtnPopOven();
	afx_msg void OnBtnPushOven();
	afx_msg void OnSelchangeComboOven();
	afx_msg void OnSetfocusListChannel(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusListOvenchMap(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETUPOVEN_H__BE33A4FF_D335_4E8D_9189_DFA7241DAE88__INCLUDED_)
