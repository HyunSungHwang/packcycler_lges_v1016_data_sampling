#if !defined(AFX_PARALLELCONFIGDLG_H__A8753E81_3763_4FA9_9CFF_8092D0CF5A14__INCLUDED_)
#define AFX_PARALLELCONFIGDLG_H__A8753E81_3763_4FA9_9CFF_8092D0CF5A14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParallelConfigDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParallelConfigDlg dialog
#define  MAX_PARALL_2		2	
#define  MAX_PARALL_4		4

#include "CTSMonProDoc.h"



class CParallelConfigDlg : public CDialog
{
// Construction
public:
	
	int m_nOldItemNum;	//ljb 20130909
	int m_nParallMax;
	BOOL IsChange;
	BOOL UpdateParallelCh(CCyclerModule * pMD, int nCh );
	void InsertSortList(int nIndex, CString strItem);
	int nCurrentModuleID;
	void UpdateParallelList(CString ParItem[], int nCount);
	int GetStartChNo(int nModuleID);
	void UpdateChannelList(int nCurrentModule);
	void UpdateModuleList();
	void OnCombineChannelinFP(); //yulee 20190712
	void OnCombineChannelinNomal(); //yulee 20190712
//	CParallelConfigDlg(CWnd* pParent = NULL);   // standard constructor
	CParallelConfigDlg(CCTSMonProDoc * pDoc, CWnd* pParent = NULL);   // standard constructor
	LRESULT OnChannelAttributeReceived(WPARAM wParam, LPARAM lParam); //ksj 20200122

// Dialog Data
	//{{AFX_DATA(CParallelConfigDlg)
	enum { IDD = IDD_PARALLEL_DLG , IDD2 = IDD_PARALLEL_DLG_ENG ,IDD3 = IDD_PARALLEL_DLG_PL };
	CListCtrl	m_ctrlChList;
	CListCtrl	m_ctrlModuleList;
	CListCtrl	m_ctrlParList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParallelConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CParallelConfigDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBtnCombine();
	afx_msg void OnBtnDivide();
	afx_msg void OnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	CCTSMonProDoc * m_pDoc;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARALLELCONFIGDLG_H__A8753E81_3763_4FA9_9CFF_8092D0CF5A14__INCLUDED_)
