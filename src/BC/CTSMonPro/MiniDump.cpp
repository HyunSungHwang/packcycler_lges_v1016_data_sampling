#include "stdafx.h"
#include "MiniDump.h"
#include <DbgHelp.h>
#include <io.h>
#include <direct.h>

typedef BOOL (WINAPI *MINIDUMPWRITEDUMP)( // Callback 함수의 원형
	HANDLE hProcess, 
	DWORD dwPid, 
	HANDLE hFile, 
	MINIDUMP_TYPE DumpType,
	CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
	CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
	CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam);

LPTOP_LEVEL_EXCEPTION_FILTER PreviousExceptionFilter = NULL;

LONG WINAPI UnHandledExceptionFilter(struct _EXCEPTION_POINTERS *exceptionInfo)
{
	HMODULE	DllHandle		= NULL;

	// Windows 2000 이전에는 따로 DBGHELP를 배포해서 설정해 주어야 한다.
	DllHandle				= LoadLibrary(_T("DBGHELP.DLL"));

	if (DllHandle)
	{
		MINIDUMPWRITEDUMP Dump = (MINIDUMPWRITEDUMP) GetProcAddress(DllHandle, "MiniDumpWriteDump");

		if (Dump)
		{
			TCHAR		DumpPath[MAX_PATH] = {0,};
			SYSTEMTIME	SystemTime;

			GetLocalTime(&SystemTime);

			TCHAR tszBuff[MAX_PATH];
			VERIFY(::GetModuleFileName(AfxGetInstanceHandle(), tszBuff, MAX_PATH));

			CString strTemp, strFilePath;

			strTemp.Format(_T("%s"), tszBuff);
			strTemp = strTemp.Left(strTemp.ReverseFind('\\'));
#ifdef _DEBUG
			strTemp = strTemp.Left(strTemp.ReverseFind('\\'));
#endif
			strTemp = strTemp + _T("\\MemoryDump");

			char MBBuffer[1024];
			memset(MBBuffer,0,sizeof(MBBuffer));

#ifdef _UNICODE
			WideCharToMultiByte(CP_ACP, 0, strTemp.GetBuffer(0), -1, MBBuffer, 1024, 0, 0);
#else
			sprintf(MBBuffer, _T("%s"), strTemp);
#endif

			if (_access(MBBuffer,0)==-1)
				_mkdir(MBBuffer);

			_sntprintf(DumpPath, MAX_PATH, _T("%s\\%04d%02d%02d_%02d%02d%02d.dmp"), strTemp,
				SystemTime.wYear,
				SystemTime.wMonth,
				SystemTime.wDay,
				SystemTime.wHour,
				SystemTime.wMinute,
				SystemTime.wSecond);
			
			HANDLE FileHandle = CreateFile(
				DumpPath, 
				GENERIC_WRITE, 
				FILE_SHARE_WRITE, 
				NULL, CREATE_ALWAYS, 
				FILE_ATTRIBUTE_NORMAL, 
				NULL);

			if (FileHandle != INVALID_HANDLE_VALUE)
			{
				_MINIDUMP_EXCEPTION_INFORMATION MiniDumpExceptionInfo;
				
				MiniDumpExceptionInfo.ThreadId			= GetCurrentThreadId();
				MiniDumpExceptionInfo.ExceptionPointers	= exceptionInfo;
				MiniDumpExceptionInfo.ClientPointers	= NULL;

				BOOL Success = Dump(
					GetCurrentProcess(), 
					GetCurrentProcessId(), 
					FileHandle, 
					//MiniDumpWithFullMemory,
					MiniDumpNormal,
					&MiniDumpExceptionInfo, 
					NULL, 
					NULL);

				if (Success)
				{
					CloseHandle(FileHandle);

					//return EXCEPTION_EXECUTE_HANDLER;
					//return EXCEPTION_CONTINUE_EXECUTION;
					return EXCEPTION_CONTINUE_SEARCH;
				}
			}

			CloseHandle(FileHandle);
		}
	}

	return EXCEPTION_CONTINUE_SEARCH;
}

BOOL CMiniDump::Begin(VOID)
{
	SetErrorMode(0);
	SetErrorMode(SEM_FAILCRITICALERRORS);
	SetErrorMode(SEM_NOALIGNMENTFAULTEXCEPT);
	SetErrorMode(SEM_NOGPFAULTERRORBOX);
	SetErrorMode(SEM_NOOPENFILEERRORBOX);

	PreviousExceptionFilter = SetUnhandledExceptionFilter(UnHandledExceptionFilter);

	return true;
}

BOOL CMiniDump::End(VOID)
{
	SetUnhandledExceptionFilter(PreviousExceptionFilter);

	return true;
}


