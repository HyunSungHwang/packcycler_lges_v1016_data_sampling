// DCLoad.h: interface for the CDCLoad class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DCLOAD_H__02F9DD37_2C8A_4D1D_A07F_C25678F56DF1__INCLUDED_)
#define AFX_DCLOAD_H__02F9DD37_2C8A_4D1D_A07F_C25678F56DF1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "SerialPort.h"

// typedef enum {
// 	/* 0 CC MODE*/ SL_MODE_CC,
// 	/* 1 CV MODE*/ SL_MODE_CV,
// 	/* 2 CR MODE*/ SL_MODE_CR,
// 	/* 3 CP MODE*/ SL_MODE_CP
// 	
// }sl200ModeType;

class CDCLoad  
{
public:

	BOOL bSlMeasCurrFlag;
	BOOL bSlMeasVoltFlag;
	BOOL bSlMeasPowerFlag;

	CString	m_strRecv;
	CString	m_strVer;
	CString m_strMode;		//CC/CV/CR/CP
	CString m_strLoadStat;	//LOAD ON, LOAD OFF
	
	float m_fCurCurr;
	float m_fCurVolt;
	float m_fCurResitance;
	float m_fCurPower;

	CSerialPort *m_pSerial;
	SERIAL_CONFIG m_SerialConfig;

	BOOL InitSerialPortUseOven(int nNum);
	void Fun_ReConnect(int nNum);


	CDCLoad()
	{
		m_pSerial = NULL;
		ZeroMemory(&m_SerialConfig,sizeof(SERIAL_CONFIG));
		ResetData();
	}

	virtual ~CDCLoad()
	{
		if (m_pSerial) delete m_pSerial;
		ResetData();
	}

	BOOL	SetSYSTem();
	void	SetLoadOnOff(BOOL bOnOff);
	void	SetMode(sl200ModeType mode);
	void	SetCurrentValue(float value);
	void	SetVoltageValue(float value);
	void	SetResistanceValue(float value);
	void	SetPowerValue(float value);

	CString	ReqVersion();
	CString	ReqMode();			//CC/CV/CR/CP
	CString	ReqLoadStat();			//LOAD ON/ LOAD OFF
	float	ReqMeasureCurrent();
	float	ReqMeasureVoltage();
	float	ReqMeasureResistance();
	float	ReqMeasurePower();
	BOOL	ReqPortOpen()     { return m_bPortOpen; }
	void onlogsave(CString strtype,CString strlog,int iCHNO); //lyj 20210809

	void ResetData()
	{
		m_strVer = "";
		m_fCurCurr = 0.0f;
		m_fCurVolt = 0.0f;
		m_fCurResitance = 0.0f;
		m_fCurPower = 0.0f;

		m_strLoadStat = "";
		m_strMode = "";
	}

protected:	
	BOOL	m_bPortOpen;
	CString	m_strSendData;




};

#endif // !defined(AFX_DCLOAD_H__02F9DD37_2C8A_4D1D_A07F_C25678F56DF1__INCLUDED_)