// VoltageCheckDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "VoltageCheckDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVoltageCheckDlg dialog


CVoltageCheckDlg::CVoltageCheckDlg(CDWordArray& adwTargetChArray, CCTSMonProDoc* pDoc, CWnd* pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CVoltageCheckDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CVoltageCheckDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CVoltageCheckDlg::IDD3):
	(CVoltageCheckDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CVoltageCheckDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_adwTargetChArray.Copy(adwTargetChArray);
	m_pDoc = pDoc;
	m_bCtrlCreated = FALSE;
}


void CVoltageCheckDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVoltageCheckDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVoltageCheckDlg, CDialog)
	//{{AFX_MSG_MAP(CVoltageCheckDlg)
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVoltageCheckDlg message handlers

BOOL CVoltageCheckDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here


	InitCtrl();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CVoltageCheckDlg::InitCtrl()
{
	int nSelCount = m_adwTargetChArray.GetSize();
	if(nSelCount <= 0)
	{
		EndDialog(0); //선택된 채널이 없으면 대화상자 종료
		return;
	}
	int nModuleID;
	int nChIndex;
	
	int ch;
	CCyclerModule* pMD = NULL;
	CCyclerChannel* pChannel = NULL;
	CCyclerModule* pPrevMD = NULL;
	int nPrevChCnt = 0;

	CRect rect;
	CRect rect2;
	CString strTemp;

	m_pEdit = new CEdit[nSelCount];
	m_pStaticCh = new CStatic[nSelCount];
	m_pStaticV = new CStatic[nSelCount];
	m_bCtrlCreated = TRUE;
	
	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());		
		if(!pPrevMD) pPrevMD = pMD;

		//Create Edit Ctrl
		GetDlgItem(IDC_EDIT_VOLTAGE)->GetWindowRect(&rect);
		rect2.CopyRect(&rect);
		rect2.top = rect.top + ((rect.Height()+5)*ch);
		rect2.bottom = rect2.top + rect.Height();		
		ScreenToClient(&rect2);
		m_pEdit[ch].Create(ES_RIGHT|ES_AUTOVSCROLL|WS_BORDER,rect2,this,IDC_EDIT_VOLTAGE+ch+1);
		m_pEdit[ch].ShowWindow(SW_SHOW);

		//Create Static Ctrl
		GetDlgItem(IDC_STATIC_CH)->GetWindowRect(&rect);
		rect2.CopyRect(&rect);
		rect2.top = rect.top + ((rect.Height()+5)*ch);
		rect2.bottom = rect2.top + rect.Height();		
		ScreenToClient(&rect2);
		
		//strTemp.Format("Ch %d",nChIndex+1);	//만일 모듈이 늘어날경우 네이밍 변경 필요.
		strTemp.Format("Ch %d",nChIndex + 1 + nPrevChCnt); //모듈이 여러개일 경우를 고려.

		m_pStaticCh[ch].Create(strTemp,GetDlgItem(IDC_STATIC_CH)->GetStyle(),rect2,this);
		m_pStaticCh[ch].ShowWindow(SW_SHOW);

		GetDlgItem(IDC_STATIC_V)->GetWindowRect(&rect);
		rect2.CopyRect(&rect);
		rect2.top = rect.top + ((rect.Height()+5)*ch);
		rect2.bottom = rect2.top + rect.Height();		
		
		ScreenToClient(&rect2);
		m_pStaticV[ch].Create("V",GetDlgItem(IDC_STATIC_V)->GetStyle(),rect2,this);
		m_pStaticV[ch].ShowWindow(SW_SHOW);

		m_nLastCtrlRect.CopyRect(rect2);
		
		if(pMD != pPrevMD)
		{
			nPrevChCnt += pMD->GetTotalChannel();
			pPrevMD = pMD;
		}
	}

}

void CVoltageCheckDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
	if(m_bCtrlCreated)
	{
		delete [] m_pEdit;
		delete [] m_pStaticCh;
		delete [] m_pStaticV;
	}	
	
}

void CVoltageCheckDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	//동적 컨트롤 생성에 따라 대화상자의 크기를 변경하고 화면 한가운데로 이동시킨다.
	CRect rect;

	GetWindowRect(&rect);
	
	ClientToScreen(&m_nLastCtrlRect);
	rect.bottom = m_nLastCtrlRect.bottom + 30;

	int nSystemY = GetSystemMetrics(SM_CYSCREEN); // 사용중인 모니터의 해상도 y값을 가져옴
	int nDlgHeight = rect.Height();
	int nDestY = nSystemY/2 - nDlgHeight/2;

	rect.top = nDestY;
	rect.bottom = nDestY + nDlgHeight;

	MoveWindow(&rect);
}

void CVoltageCheckDlg::OnOK() 
{

	int nSelCount = m_adwTargetChArray.GetSize();
	
	int nModuleID;
	int nChIndex;
	
	int ch;
	CCyclerModule* pMD;
	CCyclerChannel* pChannel;

	CString strVoltage;
	double fVoltage;
	double fTolerance;
	double fCurVoltage;
	CString strMessage;

	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
	

		m_pEdit[ch].GetWindowText(strVoltage);
		
		if(strVoltage.IsEmpty())
		{
			//strMessage.Format("Ch %d의 셀 전압을 입력해 주세요.", nChIndex+1);
			strMessage.Format(Fun_FindMsg("VoltageCheckDlg_OnOK_msg1","IDD_VOLTAGE_CHECK_DLG"), nChIndex+1);//&&
			//MessageBox(strMessage,"경고",MB_ICONWARNING|MB_OK);
			MessageBox(strMessage,Fun_FindMsg("VoltageCheckDlg_OnOK_msg2","IDD_VOLTAGE_CHECK_DLG"),MB_ICONWARNING|MB_OK);//&&
			return;
		}

		fVoltage = atof(strVoltage);
		
		
		fCurVoltage = pChannel->GetVoltage()/1000000;
		fTolerance = fabs(fCurVoltage * 0.01); //허용 오차 +- 1%

		CString strTolerance;
		strTolerance.Format("%f",fTolerance);
		strTolerance = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC,"Check Voltage Tolerance",strTolerance);
		
		fTolerance = atof(strTolerance);

		TRACE("fVoltage : %f, fTolerance : %f, fCurVoltage \n",fVoltage,fTolerance,fCurVoltage);

	
		if(fVoltage > fCurVoltage + fTolerance || fVoltage < fCurVoltage - fTolerance)
		{
			//strMessage.Format("Ch %d의 셀 전압을 다시 확인해주세요.", nChIndex+1);
			strMessage.Format(Fun_FindMsg("VoltageCheckDlg_OnOK_msg3","IDD_VOLTAGE_CHECK_DLG"), nChIndex+1);//&&
			//MessageBox(strMessage,"경고",MB_ICONWARNING|MB_OK);
			MessageBox(strMessage,Fun_FindMsg("VoltageCheckDlg_OnOK_msg4","IDD_VOLTAGE_CHECK_DLG"),MB_ICONWARNING|MB_OK);//&&
			return;
		}

	}

	CDialog::OnOK();
}