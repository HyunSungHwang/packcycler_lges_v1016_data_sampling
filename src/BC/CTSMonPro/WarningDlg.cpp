// WarningDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "CTSMonProDoc.h"
// #include "DetailOvenDlg.h"
// #include "MainFrm.h"
#include "WarningDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWarningDlg dialog


CWarningDlg::CWarningDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CWarningDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CWarningDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CWarningDlg::IDD3):
	(CWarningDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CWarningDlg)
	m_strWarringMessage = _T("");
	//}}AFX_DATA_INIT
}


void CWarningDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWarningDlg)
	DDX_Text(pDX, IDC_LAB_MESSAGE, m_strWarringMessage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWarningDlg, CDialog)
	//{{AFX_MSG_MAP(CWarningDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWarningDlg message handlers

INT_PTR CWarningDlg::DoModal() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::DoModal();
}

void CWarningDlg::OnOK() 
{
	// TODO: Add extra validation here

	
	CDialog::OnOK();
}