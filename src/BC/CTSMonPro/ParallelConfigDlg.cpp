// ParallelConfigDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "ParallelConfigDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParallelConfigDlg dialog


CParallelConfigDlg::CParallelConfigDlg(CCTSMonProDoc * pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CParallelConfigDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CParallelConfigDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CParallelConfigDlg::IDD3):
	(CParallelConfigDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CParallelConfigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	IsChange = FALSE;
	nCurrentModuleID = -1; //ksj 20200211 : 초기화 추가.
}





void CParallelConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParallelConfigDlg)
	DDX_Control(pDX, IDC_CHANNEL_LIST, m_ctrlChList);
	DDX_Control(pDX, IDC_LIST1, m_ctrlModuleList);
	DDX_Control(pDX, IDC_PARALLEL_LIST, m_ctrlParList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CParallelConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CParallelConfigDlg)
	ON_BN_CLICKED(IDC_BTN_COMBINE, OnBtnCombine)
	ON_BN_CLICKED(IDC_BTN_DIVIDE, OnBtnDivide)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, OnItemchangedList1)
	ON_MESSAGE(SFTWM_CH_ATTRIBUTE_RECEIVE, OnChannelAttributeReceived)	//ksj 20200122 : 병렬 정보 등 SBC로부터 CH ATTRIBUTE 정보 수신
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParallelConfigDlg message handlers

BOOL CParallelConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect rect;
	m_nParallMax = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Max Parallel Count", 2);
	

	//3개의 리스트 초기화////////////////////////////////////////////////////////////////////////////
	m_ctrlModuleList.GetClientRect(&rect);
	int nColumnWidth = rect.Width() / 3;

	m_ctrlModuleList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES); 

	int n_dummy =  m_ctrlModuleList.InsertColumn(0, "Dummy", LVCFMT_CENTER, 100);	//Dummy Column

	m_ctrlModuleList.InsertColumn(1, _T(Fun_FindMsg("OnInitDialog_msg1","IDD_PARALLEL_DLG")), LVCFMT_CENTER, nColumnWidth);
	//@ m_ctrlModuleList.InsertColumn(1, _T("모듈"), LVCFMT_CENTER, nColumnWidth);
	m_ctrlModuleList.InsertColumn(2, _T(Fun_FindMsg("OnInitDialog_msg2","IDD_PARALLEL_DLG")), LVCFMT_CENTER, nColumnWidth);
	//@ m_ctrlModuleList.InsertColumn(2, _T("연결상태"), LVCFMT_CENTER, nColumnWidth);
	m_ctrlModuleList.InsertColumn(3, _T(Fun_FindMsg("OnInitDialog_msg3","IDD_PARALLEL_DLG")), LVCFMT_CENTER, rect.Width() - (2*nColumnWidth));
	//@ m_ctrlModuleList.InsertColumn(3, _T("채널수"), LVCFMT_CENTER, rect.Width() - (2*nColumnWidth));
	m_ctrlModuleList.DeleteColumn(n_dummy);		//Remove Dummy Column

	m_ctrlChList.GetClientRect(&rect);
	m_ctrlChList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES); 
	n_dummy = m_ctrlChList.InsertColumn(0, "Dummy", LVCFMT_CENTER, rect.Width());	//Dummy Column
	m_ctrlChList.InsertColumn(1, _T(Fun_FindMsg("OnInitDialog_msg4","IDD_PARALLEL_DLG")), LVCFMT_CENTER, rect.Width());
	//@ m_ctrlChList.InsertColumn(1, _T("채널"), LVCFMT_CENTER, rect.Width());
	m_ctrlChList.DeleteColumn(n_dummy);		//Remove Dummy Column

	m_ctrlParList.GetClientRect(&rect);
	m_ctrlParList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES); 
	n_dummy = m_ctrlParList.InsertColumn(0, "Dummy", LVCFMT_CENTER, 100);	//Dummy Column
	
	if (_SFT_MAX_PARALLEL_CHANNEL_2 == m_nParallMax)
	{
		nColumnWidth = rect.Width()/2;
		m_ctrlParList.InsertColumn(1, _T("Master"), LVCFMT_CENTER, nColumnWidth);
		m_ctrlParList.InsertColumn(2, _T("Slave"), LVCFMT_CENTER, rect.Width() - nColumnWidth);
		m_ctrlParList.DeleteColumn(n_dummy);		//Remove Dummy Column

	}
	else if (_SFT_MAX_PARALLEL_CHANNEL_4 == m_nParallMax)
	{
		nColumnWidth = rect.Width() / 4;
		m_ctrlParList.InsertColumn(1, _T("Master"), LVCFMT_CENTER, nColumnWidth);
		m_ctrlParList.InsertColumn(2, _T("Slave"), LVCFMT_CENTER, nColumnWidth);
		m_ctrlParList.InsertColumn(3, _T("Slave"), LVCFMT_CENTER, nColumnWidth);
		m_ctrlParList.InsertColumn(4, _T("Slave"), LVCFMT_CENTER, nColumnWidth);
		m_ctrlParList.DeleteColumn(n_dummy);		//Remove Dummy Column
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////

	UpdateModuleList();		//모듈 리스트 표시

	m_nOldItemNum = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



//모듈 리스트 화면 표시
void CParallelConfigDlg::UpdateModuleList()
{
	CCyclerModule * pMD;
	int nModuleID = 0;
	CString strTmp;
	
	for(int i =0; i < m_pDoc->GetInstallModuleCount(); i++)
	{
		nModuleID = m_pDoc->GetModuleID(i);
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD == NULL)		continue;

		int nIndex = m_ctrlModuleList.GetItemCount();

		strTmp.Format("%s", m_pDoc->GetModuleName(nModuleID));
		m_ctrlModuleList.InsertItem(nIndex, strTmp);

		if( pMD->GetState() != PS_STATE_LINE_OFF )
		{
			//접속 상태 표시 문자
			m_ctrlModuleList.SetItemText(nIndex, 1, _T(Fun_FindMsg("UpdateModuleList_msg1","IDD_PARALLEL_DLG")));
			//@ m_ctrlModuleList.SetItemText(nIndex, 1, _T("연결됨"));
		}
		else
		{			
			m_ctrlModuleList.SetItemText(nIndex, 1, _T(Fun_FindMsg("UpdateModuleList_msg2","IDD_PARALLEL_DLG")));	
			//@ m_ctrlModuleList.SetItemText(nIndex, 1, _T("연결끊김"));	
		}
		
		int nTotlCh = pMD->GetTotalChannel();
		strTmp.Format("%d", nTotlCh);
		m_ctrlModuleList.SetItemText(nIndex, 2, strTmp);		
	}
}

//채널리스트 화면 표시
void CParallelConfigDlg::UpdateChannelList(int nModule)
{
	m_ctrlChList.DeleteAllItems();
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nModule);
	if(pMD == NULL) return;
	int nCount = pMD->GetTotalChannel();


	int nStartCh = GetStartChNo(nModule);
	CString strTemp;

	for(int a = 0; a < nCount; a++ )
	{
		CCyclerChannel * pChInfo = pMD->GetChannelInfo(nStartCh+a -1);
		//20081230 KHS 이전에 묶여있던 병렬 연결도 표시해준다.
//		if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE)
//			continue;
		if(UpdateParallelCh(pMD, nStartCh+a) == FALSE)	//병렬연결되어 있다면 병렬 리스트에 출력
		{
			strTemp.Format("Ch %d",nStartCh+a );		//병렬 연결되어 있지 않다면 채널 리스트에 출력한다.
			m_ctrlChList.InsertItem(a, strTemp);
		}
	}
}

void CParallelConfigDlg::OnCombineChannelinFP() 
{
	CString strTemp;
	int i = 0;
	UINT uState;
	if (_SFT_MAX_PARALLEL_CHANNEL_4 == m_nParallMax)
	{
		CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_4];
		int nIndex = 0;
		int nSelect[_SFT_MAX_PARALLEL_CHANNEL_4];
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);
		if(pMD == NULL)	return;

		if (m_ctrlChList.GetSelectedCount() > _SFT_MAX_PARALLEL_CHANNEL_4)
		{
			CString strErrorMsg;
			strErrorMsg.Format("병렬연결은 최대 %d 채널까지 연결될수 있습니다.", _SFT_MAX_PARALLEL_CHANNEL_4);
			AfxMessageBox(strErrorMsg);
			return;
		}

		if(m_ctrlChList.GetSelectedCount() <= 1)
		{		
			CString strErrorMsg;
			strErrorMsg.Format("병렬연결할 채널을 선택해주세요.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		uState = m_ctrlChList.GetItemState(0, LVIS_SELECTED); 
		//yulee 20181213 병렬 mark
		if (!(uState & LVIS_SELECTED))
		{
			CString strErrorMsg;
			strErrorMsg.Format("병렬연결은 채널1을 선택해야 합니다.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		//첫번째 row 채널 넘버가 1이 아니면 병렬 할 수 없음			//ljb 20110820
		strTemp = m_ctrlChList.GetItemText(0, 0);
		if (strTemp.IsEmpty()) return;
		int chNum = atoi(strTemp.Mid(2));
		if (chNum != 1)
		{
			AfxMessageBox("Channel 1을 포함하여 병렬 모드를 설정 해야 합니다.");
			return;
		}

		for(i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		

			CString strTemp = m_ctrlChList.GetItemText(i, 0);

			chNum = atoi(strTemp.Mid(2));
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
			if(pChInfo == NULL) continue;

			//ljb 20120623 edit start ***********************************************************************
			if(uState & LVIS_SELECTED)
			{
				if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
				{
					AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					return;
				}
			}
			//ljb 20120623 edit end ***********************************************************************

			if(uState & LVIS_SELECTED)
			{

				selectedItem[nIndex] = m_ctrlChList.GetItemText(i,0);
				nSelect[nIndex]= chNum;
				if(nIndex > 0)
				{
					if(nSelect[nIndex-1]+1 !=nSelect[nIndex])
					{
						AfxMessageBox("인접한 채널을 선택해주세요");
						return;
					}
				}
				nIndex++;
			}
		}
		for(i = m_ctrlChList.GetItemCount() -1; i >= 0; i--)
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 	
			if(uState & LVIS_SELECTED)
			{			
				m_ctrlChList.DeleteItem(i);
			}
		}
		UpdateParallelList(selectedItem, m_nParallMax);					//병렬 정보 리스트에 추가
	}
	else if (_SFT_MAX_PARALLEL_CHANNEL_2 == m_nParallMax)
	{
		CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_2];
		int nIndex = 0;
		int nSelect[_SFT_MAX_PARALLEL_CHANNEL_2];
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);
		if(pMD == NULL)	return;

		if (m_ctrlChList.GetSelectedCount() > _SFT_MAX_PARALLEL_CHANNEL_2)
		{
			CString strErrorMsg;
			strErrorMsg.Format("병렬연결은 최대 %d 채널까지 연결될수 있습니다.", _SFT_MAX_PARALLEL_CHANNEL_2);
			AfxMessageBox(strErrorMsg);
			return;
		}
		if(m_ctrlChList.GetSelectedCount() <= 1)
		{		
			CString strErrorMsg;
			strErrorMsg.Format("2개의 채널을 선택해주세요.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		for(i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		

			CString strTemp = m_ctrlChList.GetItemText(i, 0);

			int chNum = atoi(strTemp.Mid(2));
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
			if(pChInfo == NULL) continue;
			if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
			{
				AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
				return;
			}


			if(uState & LVIS_SELECTED)
			{

				selectedItem[nIndex] = m_ctrlChList.GetItemText(i,0);
				nSelect[nIndex]= chNum;
				if(nIndex > 0)
				{
					if(nSelect[nIndex-1]+1 !=nSelect[nIndex])
					{
						AfxMessageBox("인접한 채널을 선택해주세요");
						return;
					}
				}
				nIndex++;
			}
		}
		for(i = m_ctrlChList.GetItemCount() -1; i >= 0; i--)
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 	
			if(uState & LVIS_SELECTED)
			{			
				m_ctrlChList.DeleteItem(i);
			}
		}
		UpdateParallelList(selectedItem, m_nParallMax);					//병렬 정보 리스트에 추가
	}

	IsChange = TRUE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);
}

void CParallelConfigDlg::OnCombineChannelinNomal() 
{
	CString strTemp;
	int i = 0;
	UINT uState;
	if (_SFT_MAX_PARALLEL_CHANNEL_4 == m_nParallMax)
	{
		CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_4];
		int nIndex = 0;
		int nSelect[_SFT_MAX_PARALLEL_CHANNEL_4];
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);
		if(pMD == NULL)	return;

		if (m_ctrlChList.GetSelectedCount() > _SFT_MAX_PARALLEL_CHANNEL_4)
		{
			CString strErrorMsg;
			strErrorMsg.Format("병렬연결은 최대 %d 채널까지 연결될수 있습니다.", _SFT_MAX_PARALLEL_CHANNEL_4);
			AfxMessageBox(strErrorMsg);
			return;
		}

		if(m_ctrlChList.GetSelectedCount() <= 1)
		{		
			CString strErrorMsg;
			strErrorMsg.Format("병렬연결할 채널을 선택해주세요.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		uState = m_ctrlChList.GetItemState(0, LVIS_SELECTED); 
		//ljb 20180226 아래 주석 처리
		//       if (!(uState & LVIS_SELECTED))
		//       {
		//          CString strErrorMsg;
		//          strErrorMsg.Format("병렬연결은 채널1을 선택해야 합니다.");
		//          AfxMessageBox(strErrorMsg);
		//          return;
		//       }

		//첫번째 row 채널 넘버가 1이 아니면 병렬 할 수 없음         //ljb 20110820
		strTemp = m_ctrlChList.GetItemText(0, 0);
		if (strTemp.IsEmpty()) return;
		int chNum = atoi(strTemp.Mid(2));
		//ljb 20180226 아래 주석 처리
		//       if (chNum != 1)
		//       {
		//          AfxMessageBox("Channel 1을 포함하여 병렬 모드를 설정 해야 합니다.");
		//          return;
		//       }

		//yulee 20180227 선택한 채널의 첫번째 채널이 2이면 병렬 기능을 할 수 없도록 변경
		//전체 채널 리스트에서 선택한 채널을 검색하여 선택 되었을 때에 선택 카운트를 올린다.
		//선택 카운트가 1이고 선택된 채널이 Ch 2 이면 알림 메세지를 띄우고 return 한다.
		CString strMasterCh;
		strMasterCh.Format(_T(""));
		int nSelectCnt=0;
		for(i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			if(m_ctrlChList.GetItemState(i, LVIS_SELECTED)&LVIS_SELECTED)
			{
				nSelectCnt++;
				strMasterCh.Format(_T("%s"), m_ctrlChList.GetItemText(i, 0));

				if((strMasterCh.Find(_T("Ch 2")) > -1) && (nSelectCnt == 1))
				{
					AfxMessageBox("Channel 2는 병렬모드 설정 시 Master로 설정이 불가합니다.");
					return;
				}
			}
		}

		for(i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		

			CString strTemp = m_ctrlChList.GetItemText(i, 0);

			chNum = atoi(strTemp.Mid(2));
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
			if(pChInfo == NULL) continue;

			//ljb 20120623 edit start ***********************************************************************
			if(uState & LVIS_SELECTED)
			{
				if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
				{
					AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					return;
				}
			}
			//ljb 20120623 edit end ***********************************************************************

			if(uState & LVIS_SELECTED)
			{

				selectedItem[nIndex] = m_ctrlChList.GetItemText(i,0);
				nSelect[nIndex]= chNum;
				if(nIndex > 0)
				{
					if(nSelect[nIndex-1]+1 !=nSelect[nIndex])
					{
						AfxMessageBox("인접한 채널을 선택해주세요");
						return;
					}
				}
				nIndex++;
			}
		}
		for(i = m_ctrlChList.GetItemCount() -1; i >= 0; i--)
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 	
			if(uState & LVIS_SELECTED)
			{			
				m_ctrlChList.DeleteItem(i);
			}
		}
		UpdateParallelList(selectedItem, m_nParallMax);					//병렬 정보 리스트에 추가
	}
	else if (_SFT_MAX_PARALLEL_CHANNEL_2 == m_nParallMax)
	{
		CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_2];
		int nIndex = 0;
		int nSelect[_SFT_MAX_PARALLEL_CHANNEL_2];
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);
		if(pMD == NULL)	return;

		if (m_ctrlChList.GetSelectedCount() > _SFT_MAX_PARALLEL_CHANNEL_2)
		{
			CString strErrorMsg;
			strErrorMsg.Format("병렬연결은 최대 %d 채널까지 연결될수 있습니다.", _SFT_MAX_PARALLEL_CHANNEL_2);
			AfxMessageBox(strErrorMsg);
			return;
		}
		if(m_ctrlChList.GetSelectedCount() <= 1)
		{		
			CString strErrorMsg;
			strErrorMsg.Format("2개의 채널을 선택해주세요.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		for(i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		

			CString strTemp = m_ctrlChList.GetItemText(i, 0);

			int chNum = atoi(strTemp.Mid(2));
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
			if(pChInfo == NULL) continue;
			if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
			{
				AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
				return;
			}


			if(uState & LVIS_SELECTED)
			{

				selectedItem[nIndex] = m_ctrlChList.GetItemText(i,0);
				nSelect[nIndex]= chNum;
				if(nIndex > 0)
				{
					if(nSelect[nIndex-1]+1 !=nSelect[nIndex])
					{
						AfxMessageBox("인접한 채널을 선택해주세요");
						return;
					}
				}
				nIndex++;
			}
		}
		for(i = m_ctrlChList.GetItemCount() -1; i >= 0; i--)
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 	
			if(uState & LVIS_SELECTED)
			{			
				m_ctrlChList.DeleteItem(i);
			}
		}
		UpdateParallelList(selectedItem, m_nParallMax);					//병렬 정보 리스트에 추가
	}

	IsChange = TRUE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);
}

//병렬연결 설정
void CParallelConfigDlg::OnBtnCombine() 
{
	//yulee 20190705 강제병렬 옵션 처리 Mark
	if(g_AppInfo.iPType==1)
	{
			OnCombineChannelinFP(); 
	}
	else
	{
			OnCombineChannelinNomal();
	}

	/*

	CString strTemp;
	int i = 0;
	UINT uState;
	if (_SFT_MAX_PARALLEL_CHANNEL_4 == m_nParallMax)
	{
		CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_4];
		int nIndex = 0;
		int nSelect[_SFT_MAX_PARALLEL_CHANNEL_4];
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);
		if(pMD == NULL)	return;

		if (m_ctrlChList.GetSelectedCount() > _SFT_MAX_PARALLEL_CHANNEL_4)
		{
			CString strErrorMsg;
			strErrorMsg.Format(Fun_FindMsg("OnBtnCombine_msg1"), _SFT_MAX_PARALLEL_CHANNEL_4);
			//@ strErrorMsg.Format("병렬연결은 최대 %d 채널까지 연결될수 있습니다.", _SFT_MAX_PARALLEL_CHANNEL_4);
			AfxMessageBox(strErrorMsg);
			return;
		}

		if(m_ctrlChList.GetSelectedCount() <= 1)
		{		
			CString strErrorMsg;
			strErrorMsg.Format(Fun_FindMsg("OnBtnCombine_msg2"));
			//@ strErrorMsg.Format("병렬연결할 채널을 선택해주세요.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		uState = m_ctrlChList.GetItemState(0, LVIS_SELECTED); 
			  //ljb 20180226 아래 주석 처리
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
		       if (!(uState & LVIS_SELECTED))
		       {
		          CString strErrorMsg;
		          strErrorMsg.Format("Parallel-mode should include Ch1.");
		          AfxMessageBox(strErrorMsg);
		          return;
		       }		
		}
//#else
else
		{
				
				
		}
//#endif

			   //첫번째 row 채널 넘버가 1이 아니면 병렬 할 수 없음         //ljb 20110820
			  strTemp = m_ctrlChList.GetItemText(0, 0);
			  if (strTemp.IsEmpty()) return;
			   int chNum = atoi(strTemp.Mid(2));
			   
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			  //ljb 20180226 아래 주석 처리
		       if (chNum != 1)
		       {
					AfxMessageBox(Fun_FindMsg("OnBtnCombine_msg4"));
					//@ AfxMessageBox("Channel 1을 포함하여 병렬 모드를 설정 해야 합니다.");
		          return;
		       }				
		}
//#else
else
		{
				//yulee 20180227 선택한 채널의 첫번째 채널이 2이면 병렬 기능을 할 수 없도록 변경
			//전체 채널 리스트에서 선택한 채널을 검색하여 선택 되었을 때에 선택 카운트를 올린다.
			//선택 카운트가 1이고 선택된 채널이 Ch 2 이면 알림 메세지를 띄우고 return 한다.
			CString strMasterCh;
			strMasterCh.Format(_T(""));
			int nSelectCnt=0;
		for(int i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
			{
				if(m_ctrlChList.GetItemState(i, LVIS_SELECTED)&LVIS_SELECTED)
				{
					nSelectCnt++;
					strMasterCh.Format(_T("%s"), m_ctrlChList.GetItemText(i, 0));
				
					if((strMasterCh.Find(_T("Ch 2")) > -1) && (nSelectCnt == 1))
					{
					AfxMessageBox(Fun_FindMsg("OnBtnCombine_msg4"));
					//@ AfxMessageBox("Channel 1을 포함하여 병렬 모드를 설정 해야 합니다.");
						return;
					}
				}
			}			
				
		}
//#endif
		for(int i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		

			CString strTemp = m_ctrlChList.GetItemText(i, 0);

			chNum = atoi(strTemp.Mid(2));
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
			if(pChInfo == NULL) continue;

			//ljb 20120623 edit start ***********************************************************************
			if(uState & LVIS_SELECTED)
			{
				if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
				{
					AfxMessageBox(Fun_FindMsg("OnBtnCombine_msg5"));
					//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					return;
				}
			}
			//ljb 20120623 edit end ***********************************************************************
		
			if(uState & LVIS_SELECTED)
			{
				
			   selectedItem[nIndex] = m_ctrlChList.GetItemText(i,0);
			   nSelect[nIndex]= chNum;
			   if(nIndex > 0)
				{
					if(nSelect[nIndex-1]+1 !=nSelect[nIndex])
					{
						AfxMessageBox(Fun_FindMsg("OnBtnCombine_msg6"));
						//@ AfxMessageBox("인접한 채널을 선택해주세요");
						return;
					}
				}
			   nIndex++;
			}
		}
		for(i = m_ctrlChList.GetItemCount() -1; i >= 0; i--)
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 	
			if(uState & LVIS_SELECTED)
			{			
				m_ctrlChList.DeleteItem(i);
			}
		}
		UpdateParallelList(selectedItem, m_nParallMax);					//병렬 정보 리스트에 추가
	}
	else if (_SFT_MAX_PARALLEL_CHANNEL_2 == m_nParallMax)
	{
		CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_2];
		int nIndex = 0;
		int nSelect[_SFT_MAX_PARALLEL_CHANNEL_2];
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);
		if(pMD == NULL)	return;
		
		if (m_ctrlChList.GetSelectedCount() > _SFT_MAX_PARALLEL_CHANNEL_2)
		{
			CString strErrorMsg;
			strErrorMsg.Format(Fun_FindMsg("OnBtnCombine_msg7"), _SFT_MAX_PARALLEL_CHANNEL_2);
			//@ strErrorMsg.Format("병렬연결은 최대 %d 채널까지 연결될수 있습니다.", _SFT_MAX_PARALLEL_CHANNEL_2);
			AfxMessageBox(strErrorMsg);
			return;
		}
		if(m_ctrlChList.GetSelectedCount() <= 1)
		{		
			CString strErrorMsg;
			strErrorMsg.Format(Fun_FindMsg("OnBtnCombine_msg8"));
			//@ strErrorMsg.Format("2개의 채널을 선택해주세요.");
			AfxMessageBox(strErrorMsg);
			return;
		}
		
		for(int i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		
			
			CString strTemp = m_ctrlChList.GetItemText(i, 0);
			
			int chNum = atoi(strTemp.Mid(2));
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
			if(pChInfo == NULL) continue;
			if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
			{
				AfxMessageBox(Fun_FindMsg("OnBtnCombine_msg9"));
				//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
				return;
			}
			
			
			if(uState & LVIS_SELECTED)
			{
				
				selectedItem[nIndex] = m_ctrlChList.GetItemText(i,0);
				nSelect[nIndex]= chNum;
				if(nIndex > 0)
				{
					if(nSelect[nIndex-1]+1 !=nSelect[nIndex])
					{
						AfxMessageBox(Fun_FindMsg("OnBtnCombine_msg10"));
						//@ AfxMessageBox("인접한 채널을 선택해주세요");
						return;
					}
				}
				nIndex++;
			}
		}
		for(i = m_ctrlChList.GetItemCount() -1; i >= 0; i--)
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 	
			if(uState & LVIS_SELECTED)
			{			
				m_ctrlChList.DeleteItem(i);
			}
		}
		UpdateParallelList(selectedItem, m_nParallMax);					//병렬 정보 리스트에 추가
	}

	IsChange = TRUE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);
	*/
}

//병렬연결 해제
void CParallelConfigDlg::OnBtnDivide() 
{
	int i = 0;
	int chNum = 0;
	SFT_MD_PARALLEL_DATA parallelData;
	CString strTemp;
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);
	if(pMD == NULL) return;

	ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));

	//선택된 List Item 발견시 배열에 입력하고 삭제하기 때문에 List에 Index 위치가 변경된다.
	//그러므로 List에 끝부분부터 작업해야한다.
    for(i = m_ctrlParList.GetItemCount(); i >= 0 ;  i--) 
	{
        UINT uState = m_ctrlParList.GetItemState(i, LVIS_SELECTED); 		
	
        if(uState & LVIS_SELECTED)
		{
			for(int j = 0 ; j < m_nParallMax; j++)
			{
				strTemp = m_ctrlParList.GetItemText(i, j);
				if (strTemp.IsEmpty()) continue;

				chNum = atoi(strTemp.Mid(2));

				CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
				if(pChInfo == NULL) continue;
				if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
				{
					AfxMessageBox(Fun_FindMsg("OnBtnDivide_msg","IDD_PARALLEL_DLG"));
					//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					return;
				}
				
				if(j == 0)
					parallelData.chNoMaster = chNum;			//Master Channel
				else
					parallelData.chNoSlave[j-1] = chNum;		//Slave Channels	
					
				InsertSortList(chNum, strTemp);
			
			}	
			m_ctrlParList.DeleteItem(i);	
			IsChange = TRUE;
			GetDlgItem(IDOK)->EnableWindow(IsChange);
		}
    }	
}

//Channel No의 Start 번호
int CParallelConfigDlg::GetStartChNo(int nModuleID)
{
	CCyclerModule *lpModule;

	int nStartCh = 1;
	//모듈들 채널 번호를 연속하여 표기할 경우 
	if(m_pDoc->m_bChContinueNo)
	{
		int mdID;
		for(int s =0; s<m_pDoc->GetInstallModuleCount(); s++)
		{
			mdID = m_pDoc->GetModuleID(s);
			if(mdID < nModuleID)
			{
				lpModule =  m_pDoc->GetCyclerMD(mdID);
				if(lpModule)
				{
					nStartCh+= lpModule->GetTotalChannel();
				}
			}
		}
	}
	return nStartCh;
}

//모듈 변경시
void CParallelConfigDlg::OnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	int nItem = pNMListView->iItem + 1;
	
	if (m_nOldItemNum != nItem) 
	{
		int nMudlue = m_ctrlModuleList.GetItemData(pNMListView->iItem);
		int nNewModuleID = m_pDoc->GetModuleID(nMudlue);
		
		//Event가 3번 발생하므로 변경시에만 Update함  
		if(nNewModuleID != nCurrentModuleID)
		{
			UpdateChannelList(nNewModuleID);
		}
		//nCurrentModuleID = nNewModuleID;
		nCurrentModuleID =m_nOldItemNum = nItem;
	}
	
	
	*pResult = 0;
}


//병렬 연결된 리스트 표시
void CParallelConfigDlg::UpdateParallelList(CString ParItem [], int nCount)
{
	CString strTemp;
	CString strParallel;

	if(nCount <= 0)
		return;
	int nIndex = m_ctrlParList.GetItemCount();
	m_ctrlParList.InsertItem(nIndex, ParItem[0]);
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
		m_ctrlParList.SetItemText(nIndex , 1, ParItem[1]);			
	}
//#else
else
	{
		m_ctrlParList.SetItemText(nIndex , 1, ParItem[1]);	//yulee 20190712 강제병렬 옵션 처리 Mark(수정)
		if (nCount == _SFT_MAX_PARALLEL_CHANNEL_4)
		{
			m_ctrlParList.SetItemText(nIndex , 2, ParItem[2]);
			m_ctrlParList.SetItemText(nIndex , 3, ParItem[3]);
		}			
				
	}
//#endif
}


//입력될 Item이 현재 nIndex 위치에 있는 ITem보다 작으면
//입력될 ITem보다 작은 값이 나올때까지 검색하여 입력한다.
//ITem의 정렬을 위해....
void CParallelConfigDlg::InsertSortList(int nIndex, CString strItem)
{
	CString strTemp;
	int j = 0;

	if(m_ctrlChList.GetItemCount() == 0)
	{
		m_ctrlChList.InsertItem(0, strItem);
		return;
	}

	if(nIndex < m_ctrlChList.GetItemCount())
	{
		for(j = nIndex  ; j >= 0; j--)
		{
 			strTemp = m_ctrlChList.GetItemText(j, 0);		//입력되어 있는 Item 정보를 얻는다.
			if(atoi(strTemp.Mid(2)) < nIndex)						//크기비교... 입력되어 있는 Item 보다 작으면 아래 수행
			{
				m_ctrlChList.InsertItem(j+1, strItem);				//1. 검색된 위치에 입력
				return;
			}
		}
		m_ctrlChList.InsertItem(0, strItem);						//2. For 문을 다 수행하고도 입력되지 않았으면 가장 처음에 입력
		return;
	}

	
	strTemp = m_ctrlChList.GetItemText(m_ctrlChList.GetItemCount()-1, 0);
	if(atoi(strTemp.Mid(2)) > nIndex)						//크기비교... 입력되어 있는 Item 보다 작으면 아래 수행
	{
		m_ctrlChList.InsertItem(nIndex-1, strItem);			
		return;
	}
	else
	m_ctrlChList.InsertItem(m_ctrlChList.GetItemCount(), strItem);	//3. 위에 작업에서 걸리지 않으면 가장 아래에 입력
	
	
}

//파일에 등록된 병렬 채널을 화면에 표시한다.
BOOL CParallelConfigDlg::UpdateParallelCh(CCyclerModule * pMD, int nCh )
{
	int nParallCount = _SFT_MAX_PARALLEL_COUNT;
	
	SFT_MD_PARALLEL_DATA * pParallel = pMD->GetParallelData();
	for(int i = 0 ; i < nParallCount; i++)
	{
//	#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
			if(nCh == pParallel[i].chNoMaster && pParallel[i].bParallel == TRUE &&  pParallel[i].chNoSlave[2] != 0)	//ljb 20170713 add  && pParallel[i].chNoSlave[2] != 0  
			{
				if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_2)
				{
					CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_2];
					selectedItem[0].Format("Ch %d", pParallel[i].chNoMaster);
					selectedItem[1].Format("Ch %d", pParallel[i].chNoSlave[0]);
					UpdateParallelList(selectedItem, 2);
				}
				else if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_4)
				{
					CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_4];
					selectedItem[0].Format("Ch %d", pParallel[i].chNoMaster);
					selectedItem[1].Format("Ch %d", pParallel[i].chNoSlave[0]);
					if (pParallel[i].chNoSlave[1] == 0) 
						selectedItem[2]="";
					else
						selectedItem[2].Format("Ch %d", pParallel[i].chNoSlave[1]);

					if (pParallel[i].chNoSlave[2] == 0) 
						selectedItem[3]="";
					else
						selectedItem[3].Format("Ch %d", pParallel[i].chNoSlave[2]);
					
					UpdateParallelList(selectedItem, 4);
				}
				return TRUE;	
			}
			else if(nCh == pParallel[i].chNoSlave[0] && pParallel[i].bParallel == TRUE &&  pParallel[i].chNoSlave[2] != 0) //ksj 20171027 : 주석처리
			{
				return TRUE;
			}
			else if(nCh == pParallel[i].chNoSlave[1] && pParallel[i].bParallel == TRUE &&  pParallel[i].chNoSlave[2] != 0) //ksj 20171027 : 주석처리
			{
				return TRUE;
			}
			else if(nCh == pParallel[i].chNoSlave[2] && pParallel[i].bParallel == TRUE)
			{
				return TRUE;
			}
					//ksj 20171027 : 주석처리 되어있던것 해제  -> ljb 20180411 주석 함. (chNoSlave[3] 없음)
		//      else if(nCh == pParallel[i].chNoSlave[3] && pParallel[i].bParallel == TRUE)
		//      {
		//          return TRUE;
		//       }
	}
	//#else
	else
	{
		//if(nCh == pParallel[i].chNoMaster && pParallel[i].bParallel == TRUE &&  pParallel[i].chNoSlave[2] != 0)	//ljb 20170713 add  && pParallel[i].chNoSlave[2] != 0  //ksj 20171027 : 주석처리
		if(nCh == pParallel[i].chNoMaster && pParallel[i].bParallel == TRUE) //ksj 20171027 : 원복
		{
			if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_2)
			{
				CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_2];
				selectedItem[0].Format("Ch %d", pParallel[i].chNoMaster);
				selectedItem[1].Format("Ch %d", pParallel[i].chNoSlave[0]);
				UpdateParallelList(selectedItem, 2);
			}
			else if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_4)
			{
				CString selectedItem[_SFT_MAX_PARALLEL_CHANNEL_4];
				selectedItem[0].Format("Ch %d", pParallel[i].chNoMaster);
				selectedItem[1].Format("Ch %d", pParallel[i].chNoSlave[0]);
				if (pParallel[i].chNoSlave[1] == 0) 
					selectedItem[2]="";
				else
					selectedItem[2].Format("Ch %d", pParallel[i].chNoSlave[1]);

				if (pParallel[i].chNoSlave[2] == 0) 
					selectedItem[3]="";
				else
					selectedItem[3].Format("Ch %d", pParallel[i].chNoSlave[2]);
					
				UpdateParallelList(selectedItem, 4);
			}
			return TRUE;	
		}
//		else if(nCh == pParallel[i].chNoSlave[0] && pParallel[i].bParallel == TRUE &&  pParallel[i].chNoSlave[2] != 0) //ksj 20171027 : 주석처리
		else if(nCh == pParallel[i].chNoSlave[0] && pParallel[i].bParallel == TRUE) //ksj 20171027 : 원복
		{
			return TRUE;
		}
//		else if(nCh == pParallel[i].chNoSlave[1] && pParallel[i].bParallel == TRUE &&  pParallel[i].chNoSlave[2] != 0) //ksj 20171027 : 주석처리
		else if(nCh == pParallel[i].chNoSlave[1] && pParallel[i].bParallel == TRUE) //ksj 20171027 : 원복
		{
			return TRUE;
		}
		else if(nCh == pParallel[i].chNoSlave[2] && pParallel[i].bParallel == TRUE)
		{
			return TRUE;
		}
        //ksj 20171027 : 주석처리 되어있던것 해제  -> ljb 20180411 주석 함. (chNoSlave[3] 없음)
	//      else if(nCh == pParallel[i].chNoSlave[3] && pParallel[i].bParallel == TRUE)
	//      {
	//          return TRUE;
	//      }
	}
//#endif
}
	return FALSE;
}

//다이얼로그 종료
void CParallelConfigDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	if(IsChange == TRUE)
	{
		if(AfxMessageBox(Fun_FindMsg("OnCancel_msg","IDD_PARALLEL_DLG"), MB_YESNO) != IDYES)
			//@ if(AfxMessageBox("병렬 정보가 변경되었습니다.\r\n작업을 취소하시겠습니까?", MB_YESNO) != IDYES)
			return;
	}
	CDialog::OnCancel();
}

//Config 파일에 저장한다.
/*void CParallelConfigDlg::OnOK() 
{
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);

	if(pMD == NULL) return;


	if(pMD->GetState() == PS_STATE_LINE_OFF ) 
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_PARALLEL_DLG"));
		//@ AfxMessageBox("연결되어 있지 않습니다.");
		return;
	}

	//모든 병렬 채널 초기화
	pMD->SetAllParallelReset();	

	SFT_MD_PARALLEL_DATA parallelData;
	for(int i = 0 ; i < m_ctrlParList.GetItemCount(); i++)
	{
		CString strMaster = m_ctrlParList.GetItemText(i, 0);
		CString strSlave1 = m_ctrlParList.GetItemText(i, 1);
		CString strSlave2,strSlave3;
		if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_4)
		{
			strSlave2 = m_ctrlParList.GetItemText(i, 2);
			strSlave3 = m_ctrlParList.GetItemText(i, 3);
		}

		ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
		parallelData.chNoMaster = atoi(strMaster.Mid(2));
		parallelData.chNoSlave[0] = atoi(strSlave1.Mid(2));
		
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
{
		//ksj 20171025 : 주석처리 후 원복
		
		strSlave2 = "ch 3";	//ljb 20170629 강제로 할당
		strSlave3 = "ch 4"; //ljb 20170629 강제로 할당
			if (!strSlave2.IsEmpty()) parallelData.chNoSlave[1] = atoi(strSlave2.Mid(2));
			if (!strSlave3.IsEmpty()) parallelData.chNoSlave[2] = atoi(strSlave3.Mid(2));		
}
//#else
else
{
		//ksj 20171025 : 기존 코드 원복
		if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_4)
		{
			if (!strSlave2.IsEmpty()) parallelData.chNoSlave[1] = atoi(strSlave2.Mid(2));
			if (!strSlave3.IsEmpty()) parallelData.chNoSlave[2] = atoi(strSlave3.Mid(2));
		}

}
//#endif
		parallelData.bParallel = 1;					//병렬 모드로 셋팅

		if(pMD != NULL)
		{		
			pMD->SetParallelData(parallelData);
		}
	}

	if (m_ctrlParList.GetItemCount() == 0)
	{
		log_All(_T("exe"),_T("SetParallelData"),_T("Parallel Release"));
	}

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
{
	//ksj 20171025 : 주석처리
	
	if (m_ctrlParList.GetItemCount() == 0)	//ljb 20170713 ch1,2 , ch3,4 병렬 만들기
	{
		if(pMD != NULL)
		{		
			ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
			parallelData.chNoMaster = 1;
			parallelData.chNoSlave[0] = 2;
			parallelData.bParallel = 1;					//병렬 모드로 셋팅
			pMD->SetParallelData(parallelData);
// 			Sleep(10);
// 			ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
// 			parallelData.chNoMaster = 2;
// 			parallelData.chNoSlave[0] = 2;
// 			parallelData.bParallel = 0;					//병렬 모드로 셋팅
// 			pMD->SetParallelData(parallelData);
 			Sleep(10);
			ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
			parallelData.chNoMaster = 3;
			parallelData.chNoSlave[0] = 4;
			parallelData.bParallel = 1;					//병렬 모드로 셋팅
			pMD->SetParallelData(parallelData);
// 			Sleep(10);
// 			ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
// 			parallelData.chNoMaster = 4;
// 			parallelData.chNoSlave[0] = 4;
// 			parallelData.bParallel = 0;					//병렬 모드로 셋팅
// 			pMD->SetParallelData(parallelData);
		}
	}
	
}
//#else
else
{


}	
//#endif
	m_pDoc->SendParallelDatatoModule(nCurrentModuleID) ;
	IsChange = FALSE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);
	
	//CDialog::OnOK();
}*/


//ksj 20200122 : OnOK 함수 재구현
void CParallelConfigDlg::OnOK() 
{
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);

	if(pMD == NULL) return;


	if(pMD->GetState() == PS_STATE_LINE_OFF ) 
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_PARALLEL_DLG"));
		//@ AfxMessageBox("연결되어 있지 않습니다.");
		return;
	}

	m_pDoc->SendChAttributeRequest(nCurrentModuleID); //SBC에 최신 ChAttribute 값 갱신 요청. //본 명령은 SBC로부터 Ack 가 없다. Ack 대신 Ch_Attribute Reply 수신됨.
	
	//정상적으로 갱신 이벤트 수신되면 OnChannelAttributeReceived 에서 이어서 처리.
	//일정시간 Ch_Attribute reply 없으면 예외처리도 필요함..
	
}

//ksj 20200122 : 채널 Attribute 수신 처리 핸들러
//SBC에서 Ch Attribute 수신 되면 병렬 설정 시작한다.
LRESULT CParallelConfigDlg::OnChannelAttributeReceived(WPARAM wParam , LPARAM lParam)
{

	//채널 어트리뷰트가 갱신되었다는 통지 받았으므로,
	//해당 정보를 가지고 일부만 수정하여 SBC에 재통지.
	m_pDoc->WriteLog("병렬화면 Ch Attribute 수신 확인");
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModuleID);

	if(pMD == NULL) return 0;
	pMD->MakeParallel(); // 20200216 dhkim Parallel 정보를 정확히 못읽어 오는 현상 수정
	SFT_MD_PARALLEL_DATA* pNewestParallelData = pMD->GetParallelData(); //새로 받은 Ch Attribute 데이터.

	//모든 병렬 채널 초기화
	pMD->SetAllParallelReset();	//<-- 이상함. 초기화 시켜야되나??? 우선 남겨둠.


	SFT_MD_PARALLEL_DATA parallelData;
	for(int i = 0 ; i < m_ctrlParList.GetItemCount(); i++)
	{
		CString strMaster = m_ctrlParList.GetItemText(i, 0);
		CString strSlave1 = m_ctrlParList.GetItemText(i, 1);
		CString strSlave2,strSlave3;
		if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_4)
		{
			strSlave2 = m_ctrlParList.GetItemText(i, 2);
			strSlave3 = m_ctrlParList.GetItemText(i, 3);
		}

		//ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
		if(!pNewestParallelData) //ksj 20200122
			ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
		else //ksj 20200122 
			memcpy(&parallelData,pNewestParallelData+i,sizeof(SFT_MD_PARALLEL_DATA));

		parallelData.chNoMaster = atoi(strMaster.Mid(2));
		parallelData.chNoSlave[0] = atoi(strSlave1.Mid(2));

		//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
		if(g_AppInfo.iPType==1)
		{
			//ksj 20171025 : 주석처리 후 원복

			strSlave2 = "ch 3";	//ljb 20170629 강제로 할당
			strSlave3 = "ch 4"; //ljb 20170629 강제로 할당
			if (!strSlave2.IsEmpty()) parallelData.chNoSlave[1] = atoi(strSlave2.Mid(2));
			if (!strSlave3.IsEmpty()) parallelData.chNoSlave[2] = atoi(strSlave3.Mid(2));		
		}
		//#else
		else
		{
			//ksj 20171025 : 기존 코드 원복
			if (m_nParallMax == _SFT_MAX_PARALLEL_CHANNEL_4)
			{
				if (!strSlave2.IsEmpty()) parallelData.chNoSlave[1] = atoi(strSlave2.Mid(2));
				if (!strSlave3.IsEmpty()) parallelData.chNoSlave[2] = atoi(strSlave3.Mid(2));
			}

		}
		//#endif
		parallelData.bParallel = 1;					//병렬 모드로 셋팅

		if(pMD != NULL)
		{		
			pMD->SetParallelData(parallelData);
		}
	}

	if (m_ctrlParList.GetItemCount() == 0)
	{
		log_All(_T("exe"),_T("SetParallelData"),_T("Parallel Release"));
	}

	//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
	if(g_AppInfo.iPType==1)
	{
		//ksj 20171025 : 주석처리

		if (m_ctrlParList.GetItemCount() == 0)	//ljb 20170713 ch1,2 , ch3,4 병렬 만들기
		{
			if(pMD != NULL)
			{		
				//ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
				if(!pNewestParallelData) //ksj 20200122
					ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
				else //ksj 20200122 
					memcpy(&parallelData,pNewestParallelData+1-1,sizeof(SFT_MD_PARALLEL_DATA)); //강제 병렬일때 제대로 동작하는지 확인필요

				parallelData.chNoMaster = 1;
				parallelData.chNoSlave[0] = 2;
				parallelData.bParallel = 1;					//병렬 모드로 셋팅
				pMD->SetParallelData(parallelData);

				Sleep(10);
				//ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
				if(!pNewestParallelData) //ksj 20200122
					ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
				else //ksj 20200122 
					memcpy(&parallelData,pNewestParallelData+3-1,sizeof(SFT_MD_PARALLEL_DATA)); //강제 병렬일때 제대로 동작하는지 확인필요

				parallelData.chNoMaster = 3;
				parallelData.chNoSlave[0] = 4;
				parallelData.bParallel = 1;					//병렬 모드로 셋팅
				pMD->SetParallelData(parallelData);
		
			}
		}

	}
	//#else
	else
	{


	}	
	//#endif
	m_pDoc->SendParallelDatatoModule(nCurrentModuleID) ;
	IsChange = FALSE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);

	//CDialog::OnOK();

	return 1;
}