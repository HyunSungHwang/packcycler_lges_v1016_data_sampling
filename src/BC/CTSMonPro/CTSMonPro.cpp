// CTSMonPro.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h" 
#include "CTSMonPro.h"
#include "whdump.h" //ksj 20210727 : 자체 크래시 덤프 기능 추가.

#include "MainFrm.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"

#include "Global.h"
#include "./Global/Mysql/PneApp.h"
#include "Dlg_CellBAL.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProApp

BEGIN_MESSAGE_MAP(CCTSMonProApp, CWinApp)
	//{{AFX_MSG_MAP(CCTSMonProApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_APP_FOLDER, OnAppFolder) //lyj 20210108
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_MENU_LANG_KOR, OnMenuLangKor)
	ON_COMMAND(ID_MENU_LANG_ENG, OnMenuLangEng)
	ON_UPDATE_COMMAND_UI(ID_MENU_LANG_KOR, OnUpdateLangKor)
	ON_UPDATE_COMMAND_UI(ID_MENU_LANG_ENG, OnUpdateLangEng)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProApp construction

CGlobal g_TabGlobal;

CCTSMonProApp::CCTSMonProApp()
{
	mainView=NULL;
	mainFrm=NULL;
	
	mainDlgCellBal=NULL;
	mainDlgCellBalMain=NULL;

}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCTSMonProApp object

CCTSMonProApp   theApp;
CCTSMonProView  *mainView;
CMainFrame      *mainFrm;
Global          mainGlobal;
PneApp          mainPneApp;			// 20200102 dhkim 현재 사용중이지 않는 것 같음
Dlg_CellBAL     *mainDlgCellBal;
Dlg_CellBALMain *mainDlgCellBalMain;
CString			g_strDataBaseName; //ksj 20200203
/////////////////////////////////////////////////////////////////////////////
// CCTSMonProApp initialization

BOOL CCTSMonProApp::InitInstance()
{
	// CG: The following block was added by the Splash Screen component.
	{

		CCommandLineInfo cmdInfo;

		ParseCommandLine(cmdInfo);



//		CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);

	}
	
	AfxGetModuleState()->m_dwVersion = 0x0601;
	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}
	GXInit();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
	
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	init_Language();
	HANDLE hMutexInstance = ::OpenMutex(MUTEX_ALL_ACCESS|SYNCHRONIZE, FALSE, _T("SFT Cell Test System"));
	if( hMutexInstance )
	{
		AfxMessageBox(Fun_FindMsg("StartProgram1", "IDD_CTSMONPRO"));
		//@ AfxMessageBox("프로그램이 이미 실행중 입니다.");
		return FALSE;
	}
	hMutexInstance = ::CreateMutex(NULL, TRUE, _T("SFT Cell Test System"));
	if( !hMutexInstance )
		return FALSE;


	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey("PNE CTSPack");

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	USES_CONVERSION;//cny
	g_AppPath=GWin::win_GetAppPath();
	g_AppTempPath=g_AppPath+_T("\\temp");
	GFile::file_ForceDirectory(g_AppTempPath);
	
	g_AppIni=g_AppPath+_T("\\Config.ini");
	mainGlobal.app_GetConfig(g_AppPath);//기본 설정
	Fun_SockInit();//cny

	//LoadChannelColTitle();
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

//yulee 20190306

	init_Language();

	int  nIDSel;
	switch(gi_Language)
	{
	case 1:
		  {
			nIDSel = IDR_MAINFRAME;
			break;
		  }
	case 2:
		  {
			nIDSel = IDR_MAINFRAME_ENG;
			break;
		  }
	case 3:
		{
			nIDSel = IDR_MAINFRAME_PL;
			break;
		}
	default:
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 1);
		nIDSel = IDR_MAINFRAME;
		break;
	}

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		nIDSel,
		RUNTIME_CLASS(CCTSMonProDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CCTSMonProView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	//ksj 20210727 : 자체 크래시 덤프 기능 추가.
	MiniDumper mDump( _T("CTSMonitorPro") );
	//ksj end

	//ksj 20200128 : 자동으로 빌드 날짜 붙이기
	CString strVersion;
	COleDateTime oleDateTime;
	oleDateTime.ParseDateTime(CString(__DATE__));
#ifdef _EDLC_TEST_SYSTEM //ksj 20201215 : EDLC 버전 표기 추가.
	strVersion.Format(_T("%s (EDLC) [release: %d-%02d-%02d %s]"), PROGRAM_VER, oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay() ,CString(__TIME__));	
#else
	strVersion.Format(_T("%s [release: %d-%02d-%02d %s]"), PROGRAM_VER, oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay() ,CString(__TIME__));	
#endif
	m_pMainWnd->SetWindowText(strVersion);
	((CMainFrame*)m_pMainWnd)->m_strVersion = strVersion; //ksj 20201013
	//ksj end


	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX , IDD2 = IDD_ABOUTBOX_ENG  , IDD3 = IDD_ABOUTBOX_PL };
	//}}AFX_DATA
public:
	CStatic info;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAboutDlg::IDD):
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAboutDlg::IDD2):
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CAboutDlg::IDD3):
			CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC_ABOUT_INFO, info);

	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// App command to run the dialog
void CCTSMonProApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// App command to run the dialog
void CCTSMonProApp::OnAppFolder() //lyj 20210108 PNECTSPack폴더 열기 메뉴 추가
{
	CString strtemp="";
	strtemp = AfxGetApp()->GetProfileStringA(CT_CONFIG_REG_SEC, "Path" , "");
	if(strtemp != "")
	{
		ShellExecute(NULL, "open", "explorer.exe",  strtemp, NULL, SW_SHOW);
	}
	else
	{
		AfxMessageBox("Don't Open Folder(REG Path)");
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProApp message handlers


int CCTSMonProApp::ExitInstance() 
{
	return CWinApp::ExitInstance();
}

BOOL CCTSMonProApp::PreTranslateMessage(MSG* pMsg)
{
	// CG: The following lines were added by the Splash Screen component.
//	if (CSplashWnd::PreTranslateAppMessage(pMsg))
//		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}

void CCTSMonProApp::LoadChannelColTitle()
{
	m_nMaxChannelColNum = PS_MAX_ITEM_NUM;
	CString strTitle, strData;
	for( int nI = 0; nI < m_nMaxChannelColNum; nI++ )
	{
		strTitle.Format("Item%02d", nI+1);
		strData = GetProfileString("Settings", strTitle);
		if( strData.IsEmpty() )
		{
			switch( nI )	//기본으로 표기되는 Column 명칭 
			{
			case PS_STATE		:	strData = "State";				break;
			case PS_VOLTAGE		:	strData = "Voltage";			break;
			case PS_CURRENT		:	strData = "Current";			break;
			case PS_CAPACITY	:	strData = "Capacity";			break;
			case PS_IMPEDANCE	:	strData = "Impedance";			break;
			case PS_CODE		:	strData = "Code";				break;
			case PS_STEP_TIME	:	strData = "StepTime";			break;
			case PS_TOT_TIME	:	strData = "TotTime";			break;
			case PS_GRADE_CODE	:	strData = "Grade";				break;
			case PS_STEP_NO		:	strData = "StepNo";				break;
			case PS_WATT		:	strData = "Power";				break;
			case PS_WATT_HOUR	:	strData = "wattHour";			break;
			case PS_AUX_TEMPERATURE	:	strData = "AuxTemp";		break;
			case PS_AUX_VOLTAGE	:		strData = "AuxVolt";		break;
			case PS_OVEN_TEMPERATURE:	strData = "OvenTemp";		break;
			case PS_OVEN_HUMIDITY	:	strData = "OvenHumi";		break;
			case PS_STEP_TYPE	:	strData = "Type";				break;
			case PS_CUR_CYCLE	:	strData = "CurCycle";			break;
			case PS_TOT_CYCLE	:	strData = "TotCycle";			break;
			case PS_TEST_NAME	:	strData = "TestName";			break;
			case PS_SCHEDULE_NAME : strData = "Schedule";			break;
			case PS_CHANNEL_NO	:	strData = "Channel";			break;
			case PS_MODULE_NO	:	strData = "Module";				break;
			case PS_LOT_NO		:	strData = "Serial";				break;
			case PS_CHILLER_REF_TEMP:	strData = "ChillerRefTemp";break;
			case PS_CHILLER_CUR_TEMP:	strData = "ChillerCurTemp";break;
			}
			if(!strData.IsEmpty())
				WriteProfileString("Settings", strTitle, strData);
		}
		m_pstrChannelColTitle[nI] = strData;
	}		
}


//표기되는 Column의 명칭을 변경한다.
void CCTSMonProApp::SetColName(int nIndex, CString strData)
{
	if ( nIndex >= m_nMaxChannelColNum || nIndex < 0 )
		return;

	m_pstrChannelColTitle[nIndex] = strData;
	CString strTitle;
	strTitle.Format("Col%02d", nIndex+1);
	WriteProfileString("Settings", strTitle, strData);
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString strMsg = "";

#ifdef __VERSION_LG__
	strMsg += "Defined.. LG Version : Result Data Auto downloading\n";
#endif

#ifdef USE_FILE_BUFFERING
	strMsg += "Defined.. File Buffering : Save buffered data\n";
#endif

#ifdef USE_FILE_IO_STRESS_TEST
	strMsg += "Defined.. File I/O Stress Test : I/O Stress Test\n";
#endif

#ifdef __MULTI_CELL_TYPE__
	strMsg += "Defined.. LG Version : MultiCell Type\n";
#endif

#ifdef USE_NOT_DISPLAY_STOP_STATE
	strMsg += "Defined.. Display State : Do not display the Stop State\n";
#endif


	//info.SetWindowTextA(PROGRAM_DATE);

	//ksj 20200908 : 버전 정보 표기
 	CString strVersion;
 	COleDateTime oleDateTime;
 	oleDateTime.ParseDateTime(CString(__DATE__)); 	
#ifdef _EDLC_TEST_SYSTEM //ksj 20201215 : EDLC 버전 표기 추가.
	strVersion.Format(_T("%s (EDLC) [release: %d-%02d-%02d %s]"), PROGRAM_VER, oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay() ,CString(__TIME__));	
#else
	strVersion.Format(_T("%s [release: %d-%02d-%02d %s]"), PROGRAM_VER, oleDateTime.GetYear(), oleDateTime.GetMonth(), oleDateTime.GetDay() ,CString(__TIME__));	
#endif

	CString strCopyright;
	strCopyright.Format("Copyright (C) %d PNESOLUTION CO., LTD", oleDateTime.GetYear());

	CString strProtocolVer;
	strProtocolVer.Format("Protocol Ver : V%04x", _SFT_PROTOCOL_VERSION);

	info.SetWindowTextA(strVersion);
	this->SetWindowTextA(PROGRAM_VER);
	GetDlgItem(IDC_STATIC_COPYRIGHT)->SetWindowText(strCopyright);	
	GetDlgItem(IDC_LAB_PROTOCOL_VER)->SetWindowText(strProtocolVer);	
	//ksj end

	UpdateData(FALSE);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CCTSMonProApp::Fun_ReceiveStepEnd_Data(CCyclerChannel *pChannel,PS_STEP_END_RECORD *tStepEnd)
{
	if(!tStepEnd) return FALSE;

	CString strTemp;
	strTemp.Format(_T("CHNO:%d steptype:%s(%d)"),
		           tStepEnd->chNo,
				   ::PSGetTypeMsg(tStepEnd->chStepType),
				   tStepEnd->chStepType);
    log_All(_T("sbc"),_T("stepend"),strTemp);

	//-------------------------------------
	if(tStepEnd->chStepType == PS_STEP_END)
	{
		Fun_SbcWorkEnd(pChannel);
	}
	
	return TRUE;
}

void CCTSMonProApp::Fun_CycSendCommand(CCyclerChannel *pChannel,int nCmd)
{
	int nCHNO=pChannel->m_iCh;

	if(!mainView) return;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return;

	CString strTemp;
	strTemp.Format(_T("CHNO:%d cmd:%s(%d)"),
		          nCHNO,
				  pDoc->GetCmdString(nCmd),
				  nCmd);
    log_All(_T("sbc"),_T("cmd"),strTemp);

	if(nCmd==SFT_CMD_STOP)
	{
		Fun_SbcWorkEnd(pChannel);
	}
}

void CCTSMonProApp::Fun_SbcWorkEnd(CCyclerChannel *pChannel)
{
	if(!pChannel) return;

	int nCHNO=pChannel->m_iCh;
	
//yulee 20190702 
//*********************************************************************************************************
	//사용자가 작업 시작 시 또는 파워서플라이 상태창에서 작업 종료 후 P/S 종료에 체크 했다면 작업 종료 후 같이 종료
	// 언체크면 작업 종료 후에도 P/S는 계속 동작 하도록 함.
//*********************************************************************************************************
	BOOL bPSEnd = FALSE;
	switch(nCHNO)
	{
	case 1:
		{
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1, "PwrSplyCh1OffWhenWorkEnd" , FALSE);
			break;
		}
	case 2:
		{
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1, "PwrSplyCh2OffWhenWorkEnd" , FALSE);
			break;
		}
	case 3:
		{
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2, "PwrSplyCh1OffWhenWorkEnd" , FALSE);
			break;
		}
	case 4:
		{
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2, "PwrSplyCh2OffWhenWorkEnd" , FALSE);
			break;
		}
	default:
		bPSEnd = FALSE;
		break;
	}

//*********************************************************************************************************
	if(bPSEnd)
	{
		//PwrSupply--------------------------------
		CString strLog;
		strLog.Format(_T("%d Channel End Detected. Power Stop SEQ Beginning..."), nCHNO); //ksj 20200805: 로그 추가.
		log_All(_T("power"),_T("Stop"),strLog);

		int nOutp=0;
		int nItem_pwr=mainFrm->onGetSerialPowerNo(nCHNO,nOutp);
		if(nItem_pwr>=0 && nItem_pwr<=1)
		{
			CString strTemp;
			strTemp.Format(_T("CHNO_%d,INST OUTP%d,INST?,"),nCHNO,nOutp);
			mainFrm->m_PwrSupplyCtrl[nItem_pwr].onSerialPortWriteBuffer(strTemp);

			strTemp.Format(_T("CHNO_%d,VOLT 0.0,VOLT?,"),nCHNO);
			mainFrm->m_PwrSupplyCtrl[nItem_pwr].onSerialPortWriteBuffer(strTemp);

			strTemp.Format(_T("Power Supply Auto Stopped due to Channel %d End"), nCHNO); //lyj 20210614
			log_All(_T("power"),_T("Stop"),_T(""));
		}
	}

	//CellBAL-------------------------------
	CString strVal;
	int module_installed_ch=pChannel->m_CtrlClient.onGetMachine_module_installed_ch();
	pChannel->m_CtrlClient.onSetMachine_stop_ch(module_installed_ch,strVal);

	//Chiller--------------------------------
	int nItem_chiller=mainFrm->onGetSerialChillerNo(nCHNO);
	if(nItem_chiller>=0)
	{
		// 		int nOvenID=0;
		// 		pChannel->m_MainCtrl.onChillerSendRunTemp(pChannel,nItem_chiller,nOvenID,12,_T("stop"));
		// 		pChannel->m_MainCtrl.onChillerSendRunPump(pChannel,nItem_chiller,nOvenID,12,_T("stop"));

		//ksj 20200807 : 작업 완료시 칠러 자동 중단 기능을 옵션 처리
		CString strTemp, strChiller;
		strTemp.Format("ChillerOffWhenWorkEnd");		
		strChiller.Format("SerialChiller%d",nItem_chiller+1);		
		BOOL bChillerAutoStop = AfxGetApp()->GetProfileInt(strChiller, strTemp , TRUE); //default 는 칠러 자동 중단 활성화

		if(bChillerAutoStop)
		{
			int nOvenID=0;
			pChannel->m_MainCtrl.onChillerSendRunTemp(pChannel,nItem_chiller,nOvenID,12,_T("stop"));
			pChannel->m_MainCtrl.onChillerSendRunPump(pChannel,nItem_chiller,nOvenID,12,_T("stop"));
		}
		//ksj end
	}

	//yulee 20190114
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 0);

	pChannel->m_MainCtrl.onSbcRunInit();
}

//ksj 20200601 : SBC Pause 시 핸들러 추가.
void CCTSMonProApp::Fun_SbcWorkPause(CCyclerChannel *pChannel)
{
	if(!pChannel) return;

	int nCHNO=pChannel->m_iCh;
	CString strTemp;

	//채널 일시정지시 파워 서플라이 정지 기능 on 되어있는지 체크
	BOOL bPSEnd = FALSE;
	switch(nCHNO)
	{
	case 1:
		{
			if(pChannel->GetPauseBeforeStop()) break; //lyj 20210714 완료 전 일시정지의 경우 제외
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1, "PwrSplyCh1OffWhenPause" , FALSE);
			break;
		}
	case 2:
		{
			if(pChannel->GetPauseBeforeStop()) break; //lyj 20210714 완료 전 일시정지의 경우 제외
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1, "PwrSplyCh2OffWhenPause" , FALSE);
			break;
		}
	case 3:
		{
			if(pChannel->GetPauseBeforeStop()) break; //lyj 20210714 완료 전 일시정지의 경우 제외
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2, "PwrSplyCh1OffWhenPause" , FALSE);
			break;
		}
	case 4:
		{
			if(pChannel->GetPauseBeforeStop()) break; //lyj 20210714 완료 전 일시정지의 경우 제외
			bPSEnd = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2, "PwrSplyCh2OffWhenPause" , FALSE);
			break;
		}
	default:
		bPSEnd = FALSE;
		break;
	}

//*********************************************************************************************************
	//if(bPSEnd)
	if(bPSEnd && pChannel->GetCellCode() != 87) //ksj 20200805 : 챔버 연동 대기 일시정지때는 동작하지 않도록 개선.
	{
		//ksj 20200601 : PwrSupply--------------------------------
		strTemp.Format(_T("%d Channel Pause Detected. Power Stop SEQ Beginning..."), nCHNO);
		log_All(_T("power"),_T("Stop"),strTemp);

		int nOutp=0;
		int nItem_pwr=mainFrm->onGetSerialPowerNo(nCHNO,nOutp);
		if(nItem_pwr>=0 && nItem_pwr<=1)
		{
			
			strTemp.Format(_T("CHNO_%d,INST OUTP%d,INST?,"),nCHNO,nOutp);
			mainFrm->m_PwrSupplyCtrl[nItem_pwr].onSerialPortWriteBuffer(strTemp);

			strTemp.Format(_T("CHNO_%d,VOLT 0.0,VOLT?,"),nCHNO);
			mainFrm->m_PwrSupplyCtrl[nItem_pwr].onSerialPortWriteBuffer(strTemp);

			strTemp.Format(_T("Power Supply Auto Stopped due to Channel %d Pause"), nCHNO);
			log_All(_T("power"),_T("Stop"),_T(""));
		}
		//ksj end
	}
	
}


void CCTSMonProApp::OnMenuLangKor() 
{
	CString tmpStr;
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 1);
	if(bSelectedLanguage == 1)
	{
		tmpStr.Format("한국어가 적용되어 있습니다.");
		AfxMessageBox(tmpStr);//&&	
		return;
	}
	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 1);

	if(bChangeToEng == TRUE)
	{
		tmpStr.Format("프로그램 재 시작 시 한국어가 적용됩니다.");
		AfxMessageBox(tmpStr);//&&
	}
}

void CCTSMonProApp::OnMenuLangEng()//2-English
{
	CString tmpStr;
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 2);
	if(bSelectedLanguage == 2)
	{
		tmpStr.Format("The language is already applied.");
		AfxMessageBox(tmpStr);//&&	
		return;
	}
	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 2);

	if(bChangeToEng == TRUE)
	{
		tmpStr.Format("English will be applied after restart of this program.");
		AfxMessageBox(tmpStr);//&&
	}
}

void CCTSMonProApp::OnUpdateLangKor(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CCTSMonProApp::OnUpdateLangEng(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}


///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
void log_All(CString stype,CString stitle,CString slog)
{
	if(slog.IsEmpty() || slog==_T("")) return;
	
	slog.Replace(_T("\r"),_T(""));
	slog.Replace(_T("\n"),_T(""));
	
	GLog::log_All(stype,stitle,slog);	
}

void Fun_Log(CString strlog,CString stype)
{
	if(!mainDlgCellBalMain) return;
	
	mainDlgCellBalMain->onLog(strlog,stype);	
}

void Fun_SockInit()
{
	WSADATA wsa;
	if(WSAStartup(MAKEWORD(2,2), &wsa) != 0)
	{
		log_All(_T("auto"),_T("client"),_T("WS2_32.DLL Init Fail."));
	}
	else
	{
		log_All(_T("auto"),_T("client"),_T("WS2_32.DLL Init Succeed."));
	}
}

//ksj 20201016 : Sleep 함수를 대체하는 Wait 함수 추가.
//쓰레드를 잡고 있지 않는 대기 함수.
//CPU 자원을 사용할 수 있으므로, 쓰레드 공회전시에는 사용하면 안된다.
//쓰레드 공회전용은 Sleep 사용하세요.
void Wait(DWORD dwMillisecond)
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();

	while(GetTickCount() - dwStart < dwMillisecond)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}
