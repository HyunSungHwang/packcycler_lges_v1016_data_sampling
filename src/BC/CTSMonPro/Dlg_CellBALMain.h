#if !defined(AFX_DLG_CELLBALMAIN_H__35BD4FD3_2092_420C_B0A9_8823B0087484__INCLUDED_)
#define AFX_DLG_CELLBALMAIN_H__35BD4FD3_2092_420C_B0A9_8823B0087484__INCLUDED_

#include "./Global/gxListCtrl/gxListCtrl.h"
//--------------------------------------------------
#define ID_LIST_CELLBAL_NO                         0
#define ID_LIST_CELLBAL_MOD                        1
#define ID_LIST_CELLBAL_CH                         2
#define ID_LIST_CELLBAL_IP                         3
#define ID_LIST_CELLBAL_PORT                       4
#define ID_LIST_CELLBAL_USE                        5
#define ID_LIST_CELLBAL_CONNSTATE                  6 
#define ID_LIST_CELLBAL_op_state                   7 
#define ID_LIST_CELLBAL_op_mode                    8 
#define ID_LIST_CELLBAL_module_installed_ch        9 
#define ID_LIST_CELLBAL_module_run_time            10
#define ID_LIST_CELLBAL_internal_temp              11
#define ID_LIST_CELLBAL_load_mode_set              12
#define ID_LIST_CELLBAL_reserved2                  13
#define ID_LIST_CELLBAL_load_mode_value            14
#define ID_LIST_CELLBAL_signal_sens_time           15
#define ID_LIST_CELLBAL_reserved3                  16
#define ID_LIST_CELLBAL_availability_voltage_upper 17
#define ID_LIST_CELLBAL_under_voltage              18
#define ID_LIST_CELLBAL_over_current               19
#define ID_LIST_CELLBAL_over_temp                  20
#define ID_LIST_CELLBAL_availability_voltage_lower 21
#define ID_LIST_CELLBAL_ch_cond_end_current        22
#define ID_LIST_CELLBAL_ch_cond_end_time           23
#define ID_LIST_CELLBAL_ch_cond_detection          24
#define ID_LIST_CELLBAL_ch_cond_release            25
#define ID_LIST_CELLBAL_ch_cond_auto_stop_time     26
#define ID_LIST_CELLBAL_DATE_TIME                  27
#define ID_LIST_CELLBAL_COUNT                      28
//--------------------------------------------------
#define ID_LIST_CELLBALCH_NO           0
#define ID_LIST_CELLBALCH_CH           1
#define ID_LIST_CELLBALCH_ch_state     2
#define ID_LIST_CELLBALCH_reserved1    3
#define ID_LIST_CELLBALCH_voltage      4
#define ID_LIST_CELLBALCH_current      5
#define ID_LIST_CELLBALCH_run_time     6
#define ID_LIST_CELLBALCH_reserved2    7
#define ID_LIST_CELLBALCH_reserved3    8
#define ID_LIST_CELLBALCH_DATE_TIME    9
#define ID_LIST_CELLBALCH_COUNT        10
//--------------------------------------------------
#define ID_CMD_SBC_CH_SETINFO          0
#define ID_CMD_CELLBAL_LIST_INIT       1
#define ID_CMD_CELLBAL_LIST_SETDATA    3
#define ID_CMD_CELLBAL_USE_COUNT       10
#define ID_CMD_CELLBAL_STATE           11
#define ID_CMD_CELLBAL_GETCHANNEL      12
#define ID_CMD_CELLBALCH_LIST_INIT     21
#define ID_CMD_CELLBALCH_STATE         22
#define ID_CMD_CONNECT                 31
#define ID_CMD_DISCONN                 32
#define ID_CMD_CONN_STATE              33
#define ID_CMD_TO_MODULE_INFO_REQUEST  41
#define ID_CMD_TO_RESET                42
#define ID_CMD_TO_OP_MODE_SET          43
#define ID_CMD_TO_LOAD_MODE_SET        44
#define ID_CMD_TO_RUN                  45
#define ID_CMD_TO_STOP                 46
#define ID_CMD_TO_CH_RUN               47
#define ID_CMD_TO_CH_STOP              48
//--------------------------------------------------

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_CellBALMain.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALMain dialog

class Dlg_CellBALMain : public CDialog
{
// Construction
public:
	Dlg_CellBALMain(CWnd* pParent = NULL);   // standard constructor

	int m_iSelectCH;
	CCyclerChannel *m_lpSelectChInfo;

	void onInitObject();
	void onLog(CString strlog,CString stype=_T("test"));
	
	CString onConnState(int iStat);
	int     onSbc_Deal(int iType,int nRow=-1,int nCol=-1,CString strVal=_T(""));
	
	
	HANDLE m_hSbcDealThread;
	static DWORD WINAPI SbcDealThread(LPVOID arg);

// Dialog Data
	//{{AFX_DATA(Dlg_CellBALMain)
	enum { IDD = IDD_DIALOG_CELLBAL_MAIN , IDD2 = IDD_DIALOG_CELLBAL_MAIN_ENG , IDD3 = IDD_DIALOG_CELLBAL_MAIN_PL };
	gxListCtrl	m_ListCellBalCH;
	CListBox	m_ListCellBalLog;
	gxListCtrl	m_ListCellBal;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_CellBALMain)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_CellBALMain)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg LRESULT OnListEditText(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButCellbalConn();
	afx_msg void OnButCellbalDiscon();
	afx_msg void OnDblclkListCellbalLog();
	afx_msg void OnButCellbalTest();
	afx_msg void OnButCellbalStop();
	afx_msg void OnDblclkListCellbal(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButCellbalModinforeq();
	afx_msg void OnButCellbalReset();
	afx_msg void OnButCellbalOpmodeset();
	afx_msg void OnButCellbalLoadmodset();
	afx_msg void OnButCellbalRun();
	afx_msg void OnButCellbalchRun();
	afx_msg void OnButCellbalchStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern Dlg_CellBALMain *mainDlgCellBalMain;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CELLBALMAIN_H__35BD4FD3_2092_420C_B0A9_8823B0087484__INCLUDED_)
