// AlarmReadyDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "AlarmReadyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAlarmReadyDlg dialog
#define TIMER_WAIT_FTP_END		1


CAlarmReadyDlg::CAlarmReadyDlg(int nModuleID, CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CAlarmReadyDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAlarmReadyDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAlarmReadyDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CAlarmReadyDlg::IDD3):
	(CAlarmReadyDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CAlarmReadyDlg)
	m_lab_message = _T("");
	//}}AFX_DATA_INIT

	ASSERT(pDoc);
	m_pDoc = pDoc;
	m_strMessage.Empty();
	m_strText.Empty();
	m_nModuleID = nModuleID;
}


void CAlarmReadyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAlarmReadyDlg)
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
	DDX_Text(pDX, IDC_LAB_MSG, m_lab_message);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAlarmReadyDlg, CDialog)
	//{{AFX_MSG_MAP(CAlarmReadyDlg)
	ON_BN_CLICKED(IDC_BTN_RESEND, OnBtnResend)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAlarmReadyDlg message handlers

void CAlarmReadyDlg::Fun_RunProgress()
{
	UpdateData();
	m_chk_Time++;
	m_iPos++;
	if (m_iPos > 60)
	{
		m_iPos = 0;
		m_nTime++;
		m_strText.Format(Fun_FindMsg("Fun_RunProgress_msg1","IDD_ALARM_MESSAGE_DLG"), m_nTime, m_iPos);
		//@ m_strText.Format("(%02d:%02d 초)", m_nTime, m_iPos);
		m_lab_message =  m_strMessage + m_strText;
		
		if (m_nTime > 10) OnCancel();
	}
	m_strText.Format(Fun_FindMsg("Fun_RunProgress_msg2","IDD_ALARM_MESSAGE_DLG"), m_nTime, m_iPos);
	//@ m_strText.Format("(%02d:%02d 초)", m_nTime, m_iPos);
	m_lab_message =  m_strMessage + m_strText;
	m_progress.SetPos(m_iPos);
	
	if(m_chk_Time == m_pDoc->m_wait_FTP_endTime + m_pDoc->m_wait_FTP_endTime_plusPtrn  )	{
		OnBtnResend();
	}
	UpdateData(FALSE);
}

void CAlarmReadyDlg::Fun_SetTxtMessage(CString strMsg)
{
	UpdateData();
	m_strMessage = strMsg;
	UpdateData(FALSE);
}

BOOL CAlarmReadyDlg::Fun_SetRunInfo(UINT nModuleID, CWordArray *apSelChArray, UINT nCmdID, int nCycleNo, int nStepNo)
{
	if (m_nModuleID != nModuleID) return FALSE;
	SetTimer(TIMER_WAIT_FTP_END, 1000, NULL);	//1초 타이머 
	
	m_nTime = 0;
	m_nCmdID = nCmdID;
	m_nCycleNo= nCycleNo;
	m_nStepNo = nStepNo;
	
	awSelCh.RemoveAll();
	int ch, iTemp;
	for(ch =0; ch < apSelChArray->GetSize(); ch++)
	{
		iTemp = apSelChArray->GetAt(ch);
		awSelCh.Add(iTemp);
	}
	m_iPos = 0;
	m_progress.SetRange(0, 60);
	
	return TRUE;
}

BOOL CAlarmReadyDlg::set_AlarmReadyDlg_SelchArray(CWordArray *apSelChArray)
{
	return TRUE;
}

int CAlarmReadyDlg::Fun_RunTest(int nModuleID)
{
	if (m_nModuleID != nModuleID) return FALSE;
	
	if(m_pDoc->SendCommand(m_nModuleID, &awSelCh, SFT_CMD_RUN, m_nCycleNo, m_nStepNo))
	{
		//작업 시작 정보를 저장한다.(Pause 상태에서 연속작업이 가능하도록...)
		if(m_pDoc->SaveRunInfoToTempFile(m_nModuleID, &awSelCh) == FALSE)
		{
			strTemp.Format(Fun_FindMsg("Fun_RunTest_msg1","IDD_ALARM_MESSAGE_DLG"), m_pDoc->GetModuleName(m_nModuleID));
			//@ strTemp.Format("%s 작업시작 임시파일 정보저장을 실패 하였습니다.", m_pDoc->GetModuleName(m_nModuleID));
			m_pDoc->WriteSysLog(strTemp);
			return -1;
		}
		
		strTemp.Format(Fun_FindMsg("Fun_RunTest_msg2","IDD_ALARM_MESSAGE_DLG"), m_pDoc->GetModuleName(m_nModuleID));
		//@ strTemp.Format("%s 작업시작 성공.", m_pDoc->GetModuleName(m_nModuleID));
		m_pDoc->WriteSysLog(strTemp);

		return 1; //ksj 20200121
	}
	//ksj 20200121 : 작업시작 실패 처리 추가
	else
	{
		strTemp.Format("SendCommand Failure");
		m_pDoc->WriteSysLog(strTemp);
		return -1;
	}
	//ksj end

	//return 1; //ksj 20200121 : 주석처리
}

void CAlarmReadyDlg::Fun_CloseDlg()
{
	OnCancel();
}

void CAlarmReadyDlg::OnBtnResend() 
{
	// TODO: Add your control notification handler code here
	strTemp.Format("=== OnBtnResend === Send_CMD_SCHEDULE_END() Start ");
	m_pDoc->WriteSysLog(strTemp);
	
	//			2013-10-21			bung			:			reSend for MAX_MODUEL_RESEND
	// 	for(int i= 0; i< MAX_MODUEL_RESEND; i++)	{
	// 		m_pDoc->repMD[i]	=	m_pDoc->GetModuleInfo(1);	
	// 		m_pDoc->Send_CMD_SCHEDULE_END(m_pDoc->repMD[i], &m_pDoc->reawSelCh , m_pDoc->resteEnd, m_pDoc->renModuleID);
	// 	}
	// 
	// 	if(m_strMessage.Find('1') != -1)	{
	// 	}
	
	
	if(m_strMessage.Find('1') != -1)	{
		m_pDoc->repMD_1	=	m_pDoc->GetModuleInfo(1);	
		m_pDoc->Send_CMD_SCHEDULE_END(m_pDoc->repMD_1, &m_pDoc->reawSelCh , m_pDoc->resteEnd, m_pDoc->renModuleID);
		
		strTemp.Format("=== OnBtnResend === Send_CMD_SCHEDULE_END() reMouduleID : %d  send end", m_pDoc->renModuleID);
		m_pDoc->WriteSysLog(strTemp);
	}
	
	if(m_strMessage.Find('2') != -1)	{
		m_pDoc->repMD_2	=	m_pDoc->GetModuleInfo(2);	
		m_pDoc->Send_CMD_SCHEDULE_END(m_pDoc->repMD_2, &m_pDoc->reawSelCh , m_pDoc->resteEnd, m_pDoc->renModuleID);
		
		strTemp.Format("=== OnBtnResend === Send_CMD_SCHEDULE_END() reMouduleID : %d  send end", m_pDoc->renModuleID);
		m_pDoc->WriteSysLog(strTemp);
	}
	
	if(m_strMessage.Find('3') != -1)	{
		m_pDoc->repMD_3	=	m_pDoc->GetModuleInfo(3);	
		m_pDoc->Send_CMD_SCHEDULE_END(m_pDoc->repMD_3, &m_pDoc->reawSelCh , m_pDoc->resteEnd, m_pDoc->renModuleID);
		
		strTemp.Format("=== OnBtnResend === Send_CMD_SCHEDULE_END() reMouduleID : %d  send end", m_pDoc->renModuleID);
		m_pDoc->WriteSysLog(strTemp);
	}	
}

void CAlarmReadyDlg::OnCancel() 
{
	strTemp.Format("=== OnCancel === OnCancel start");
	m_pDoc->WriteSysLog(strTemp);
	
	KillTimer(TIMER_WAIT_FTP_END);
	m_pDoc->m_wait_FTP_endTime_plusPtrn =0;
	
	strTemp.Format("=== OnCancel === OnCancel end");
	m_pDoc->WriteSysLog(strTemp);	
	CDialog::OnCancel();
}

void CAlarmReadyDlg::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	switch(nIDEvent)	{
	case TIMER_WAIT_FTP_END	:	{
		Fun_RunProgress();
								}break;
	}
	
	CDialog::OnTimer(nIDEvent);
}