#if !defined(AFX_CALICONFIGDLG_H__4F2433F9_CE16_4658_9EDB_C2DF76BE5E60__INCLUDED_)
#define AFX_CALICONFIGDLG_H__4F2433F9_CE16_4658_9EDB_C2DF76BE5E60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CaliConfigDlg.h : header file
//

#include "ListEditCtrl.h"
/////////////////////////////////////////////////////////////////////////////
// CCaliConfigDlg dialog

class CCaliConfigDlg : public CDialog
{
// Construction
public:
	CCaliConfigDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCaliConfigDlg)
	enum { IDD = IDD_CALI_CONFIG_DLG };
	
	CListEditCtrl	m_wndCurrentLow;
	CListEditCtrl	m_wndCurrentHigh;
	CListEditCtrl	m_wndVoltage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCaliConfigDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCaliConfigDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALICONFIGDLG_H__4F2433F9_CE16_4658_9EDB_C2DF76BE5E60__INCLUDED_)
