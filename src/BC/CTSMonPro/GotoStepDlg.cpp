// GotoStepDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "GotoStepDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGotoStepDlg dialog


CGotoStepDlg::CGotoStepDlg(CCTSMonProDoc *pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CGotoStepDlg::IDD, pParent)z
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CGotoStepDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CGotoStepDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CGotoStepDlg::IDD3):
	(CGotoStepDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CGotoStepDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	m_nNowStepNo = 0;

	ASSERT(pDoc);
}


void CGotoStepDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGotoStepDlg)
	DDX_Control(pDX, IDC_SCHEDULE_LIST, m_ctrlScheduleList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGotoStepDlg, CDialog)
	//{{AFX_MSG_MAP(CGotoStepDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGotoStepDlg message handlers

void CGotoStepDlg::OnOK() 
{
	POSITION pos = m_ctrlScheduleList.GetFirstSelectedItemPosition();
	
	if(pos == NULL)
	{
		//AfxMessageBox("선택된 STEP이 없습니다.");
		AfxMessageBox(Fun_FindMsg("GotoStepDlg_OnOK_msg1","GOTOSTEPDLG"));//&&
		return;
	}
	
	int b = m_ctrlScheduleList.GetNextSelectedItem(pos);
	int m_nSelSepID = m_ctrlScheduleList.GetItemData(b);	// 0 base
	
	CString strMsg;
	if ( Fun_CheckMovableStep(m_nSelSepID) == FALSE)
	{
		//strMsg.Format("STEP(%d)은 이동 할수 없는 스텝 입니다. \n (이동가능한 스텝은 현재 Cycle내 스텝 또는 다른 Cycle STEP만 가능 합니다.",m_nSelSepID+1);
		strMsg.Format(Fun_FindMsg("GotoStepDlg_OnOK_msg2","GOTOSTEPDLG"),m_nSelSepID+1);//&&
		AfxMessageBox(strMsg);
		return;
	}

	//strMsg.Format("STEP %d으로 [%s]을 실행하시겠습니까?", m_nSelSepID+1, m_pDoc->GetCmdString(SFT_CMD_GOTOSTEP));
	strMsg.Format(Fun_FindMsg("GotoStepDlg_OnOK_msg3","GOTOSTEPDLG"), m_nSelSepID+1, m_pDoc->GetCmdString(SFT_CMD_GOTOSTEP));//&&
	//if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	if(IDYES != MessageBox(strMsg, Fun_FindMsg("GotoStepDlg_OnOK_msg4","GOTOSTEPDLG"), MB_YESNO|MB_ICONQUESTION)) //&&
		return;
	
	//이동스텝 명령어 전송
	CCyclerModule * pMD;
	pMD = m_pDoc->GetModuleInfo(m_nModuleID);

	CWordArray SelectCh;
	SelectCh.Add(m_nChannelID);

	SFT_GOTO_STEP_COMM sGotoComm;
	ZeroMemory(&sGotoComm,sizeof(SFT_GOTO_STEP_COMM));
	sGotoComm.user_branch_stepNo = m_nSelSepID+1;

	int nRtn = pMD->SendCommand(SFT_CMD_GOTOSTEP, &SelectCh, &sGotoComm, sizeof(SFT_GOTO_STEP_COMM));
	if (nRtn < 0)
	{
		//AfxMessageBox("명령 전송 실패");
		AfxMessageBox(Fun_FindMsg("GotoStepDlg_OnOK_msg5","GOTOSTEPDLG"));//&&
		return;
	}
	

	CDialog::OnOK();
}

BOOL CGotoStepDlg::Fun_CheckMovableStep(int nStepNo)
{
	CCyclerModule * pMD;
	CCyclerChannel * pChannel;
	pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	pChannel = pMD->GetChannelInfo(m_nChannelID);
	TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
	
	CScheduleData *lpSchedule = pChannel->GetScheduleData();
	CStep *lpStep;

	int nNowStep = pChannel->GetStepNo();
//	nNowStep = 5;
	TRACE("Now STEP = %d \n",nNowStep);
	if (nNowStep == nStepNo) return FALSE;	//동일 스텝이면 이동 못함

	int nCycle =0;
	int nCycleStart, nCycleEnd;
	nCycleStart = nCycleEnd =0;
	
	//다른 Cycle 시작이나 완료 인지 체크 한다.
	for (nCycle=0; nCycle<lpSchedule->GetStepSize(); nCycle++)
	{
		lpStep = lpSchedule->GetStepData(nCycle);		//ljb 20140108 add 스텝 가져오기
		if ((lpStep->m_type == PS_STEP_ADV_CYCLE) && (nStepNo == nCycle)) return TRUE;
		if (lpStep->m_type == PS_STEP_END && (nStepNo == nCycle)) return TRUE;
		//if (lpStep->m_type == PS_STEP_LOOP) return TRUE;
	}

	//해당 Start Cycle Step 찾기
	for (nCycle=nNowStep; 0<nCycle; nCycle--)
	{
		lpStep = lpSchedule->GetStepData(nCycle);		//ljb 20140108 add 스텝 가져오기
		if (lpStep->m_type == PS_STEP_ADV_CYCLE)		//Cycle STEP 찾기
		{
			nCycleStart = nCycle;
			break;
		}
	}
	//해당 END Cycle Step 찾기
	for (nCycle=nNowStep; nCycle<lpSchedule->GetStepSize(); nCycle++)
	{
		lpStep = lpSchedule->GetStepData(nCycle);		//ljb 20140108 add 스텝 가져오기
		if (lpStep->m_type == PS_STEP_LOOP)				//LOOP STEP 찾기
		{
			nCycleEnd = nCycle;
			break;
		}
	}
	if (nCycleStart < nStepNo && nStepNo <= nCycleEnd )
	{
		return TRUE;
	}

		
	return FALSE;
}

BOOL CGotoStepDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	Fun_initList();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGotoStepDlg::Fun_initList()
{
// 	DWORD style = 	m_ctrlList.GetExtendedStyle();
// 	char szBuff[32];
// 	
// 	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
// 	m_ctrlList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);
// 	
// 	m_imgList.Create(IDB_CELL_STATE_ICON_P, 19, 14,RGB(255,255,255));
// 	m_ctrlList.SetImageList(&m_imgList, LVSIL_SMALL);
	
	
	//Style
	//m_ctrlScheduleList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);
	m_ctrlScheduleList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES);//|LVS_EX_TRACKSELECT);
	m_imgList.Create(IDB_CELL_STATE_ICON_P, 19, 14,RGB(255,255,255));
	m_ctrlScheduleList.SetImageList(&m_imgList, LVSIL_SMALL);
	
	// Column 삽입
	m_ctrlScheduleList.InsertColumn(0, "Step No",  LVCFMT_CENTER,  70,  0);
	m_ctrlScheduleList.InsertColumn(1, "Type",  LVCFMT_LEFT,  150,  1);
// 	m_ctrlScheduleList.InsertColumn(2, "설정전압",  LVCFMT_CENTER,  100,  2);
// 	m_ctrlScheduleList.InsertColumn(3, "설정 전류",  LVCFMT_CENTER,  100,  3);
// 	m_ctrlScheduleList.InsertColumn(4, "R Powoer",  LVCFMT_CENTER,  100,  4);
	
	int nCount = m_ctrlScheduleList.GetItemCount();
	
	// Delete all of the items from the list view control.
	for (int i=0;i < nCount;i++)
	{
		m_ctrlScheduleList.DeleteItem(0);
	}

	CCyclerModule * pMD;
	CCyclerChannel * pChannel;
	pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	pChannel = pMD->GetChannelInfo(m_nChannelID);
	TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

	int nStepNo = pChannel->GetStepNo();

	CScheduleData *lpSchedule = pChannel->GetScheduleData();
	CStep *lpStep;

	//	CString strTitle;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	//lvItem.mask = LVIF_TEXT;//|LVIF_IMAGE;
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;

	CString strStep;
	int nI = 0;
	char szBuff[128];
//	int nPK = 0;
	for (nI=0; nI<lpSchedule->GetStepSize(); nI++)
// 	{
// 	}
// 	lpSchedule->GetStepSize();
// 	while(!rs.IsEOF())
	{
// 		data = rs.GetFieldValue(0);		//PK
// 		nPK = data.lVal;
// 		data = rs.GetFieldValue(1);		//No
// 		data = rs.GetFieldValue(2);		//Name

		lpStep = lpSchedule->GetStepData(nI);		//ljb 20140108 add 스텝 가져오기
		if (lpStep == NULL) continue;

		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		sprintf(szBuff,"%d", nI+1);
		lvItem.pszText = szBuff; 
		//20080821 Added by KBH
		//현재 Step 표시
		if(nStepNo-1 == nI)
			lvItem.iImage = 9;
		else
			lvItem.iImage = -1;
		m_ctrlScheduleList.InsertItem(&lvItem);
		m_ctrlScheduleList.SetItemData(lvItem.iItem, nI);
			
		lvItem.iImage = 8;
		switch(lpStep->m_type)
		{
		case PS_STEP_CHARGE:
			lvItem.iImage = 2;
			strStep = "Charge";
			break;
		case PS_STEP_DISCHARGE:
			lvItem.iImage = 1;
			strStep = "DisCharge";
				break;
		case PS_STEP_REST:
			lvItem.iImage = 11;
			strStep = "Rest";
				break;
		case PS_STEP_OCV:
			lvItem.iImage = 7;
			strStep = "OCV";
				break;
		case PS_STEP_IMPEDANCE:
			lvItem.iImage = 10;
			strStep = "Impdance";
				break;
		case PS_STEP_END:
			lvItem.iImage = 4;
			strStep = "END";
				break;
		case PS_STEP_ADV_CYCLE:
			strStep = "CYCLE";
				break;
		case PS_STEP_LOOP:
			strStep = "LOOP";
				break;
		case PS_STEP_PATTERN:
			lvItem.iImage = 12;
			strStep = "Pattern";
				break;
		case PS_STEP_EXT_CAN:
			lvItem.iImage = 12;
			strStep = "Ext CAN";
				break;
		case PS_STEP_USER_MAP:
			lvItem.iImage = 12;
			strStep = "User Map";
				break;
		}
		
// 		lvItem.iImage = -1;
		lvItem.iSubItem = 1;
		//lvItem.pszText = "충전";
		sprintf(lvItem.pszText,"%s",strStep);
//		lvItem.pszText = strStep;
		m_ctrlScheduleList.SetItem(&lvItem);
//		m_ctrlScheduleList.SetItemData(lvItem.iItem, nI);
	}
// 		//data = rs.GetFieldValue(3);		//Description
// 		lvItem.iSubItem = 2;
// 		lvItem.pszText ="전압값"; //data.pcVal; 
// 		m_ctrlScheduleList.SetItem(&lvItem);
// 		
// 		
// 		lvItem.iSubItem = 3;
// 		//sprintf(szBuff, "%s", date.Format());
// 		lvItem.pszText ="전류"; //data.pcVal; 
// 		lvItem.pszText = szBuff; 
// 		m_ctrlScheduleList.SetItem(&lvItem);
// 		
// 		lvItem.iSubItem = 4;
// 		//sprintf(szBuff, "%s", date.Format());
// 		lvItem.pszText ="Power"; //data.pcVal; 
// 			lvItem.pszText = szBuff; 
// 		m_ctrlScheduleList.SetItem(&lvItem);
	
// 
// 	if(lpChannel)
// 	{
// 		m_nNowStepNo = lpChannel->GetStepNo();	//현재 STEP 번호
// 		
// 	
// 		CScheduleData *lpSchedule = lpChannel->GetScheduleData();
// 		//lpSchedule->GetStepSize();
// 		if(lpSchedule)
// 		{							
// 			//CStep *lpStep = lpSchedule->GetStepData(nMinStepNo);
// 			//CStep *lpStep = lpSchedule->GetStepData();		//ljb 20140108 add 현재스텝 가져오기
// 		}

}