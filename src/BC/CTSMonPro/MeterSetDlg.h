#if !defined(AFX_METERSETDLG_H__008DAB5F_AC70_4A70_A329_4614993CE482__INCLUDED_)
#define AFX_METERSETDLG_H__008DAB5F_AC70_4A70_A329_4614993CE482__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MeterSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMeterSetDlg dialog

#include "serialconfigdlg.h"
#include "SerialPort.h"
#include "CTSMonProDoc.h"

class CMeterSetDlg : public CDialog
{
// Construction
public:	
	CString m_strReceiveBuff;
	void SetDocument(CCTSMonProDoc *);
	float ConvertData(float fData);
	BOOL ConnectPort(int nPort);
	CMeterSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMeterSetDlg)
	enum { IDD = IDD_METER_CONTIG_DIALOG , IDD2 = IDD_METER_CONTIG_DIALOG_ENG , IDD3 = IDD_METER_CONTIG_DIALOG_PL };
	CComboBox	m_ctrlChannel;
	CComboBox	m_ctrModuleID;
	int		m_nPort;
	int		m_nMeterType;
	int		m_nOperator;
	float	m_fConvertData;
	int		m_nDataItem;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMeterSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CWordArray m_aInstallCh;
	void InitControl();
	CSerialPort m_SerialPort[2];
	CCTSMonProDoc *m_pDoc;

	CSerialConfigDlg	m_SerialConfig;
	// Generated message map functions
	//{{AFX_MSG(CMeterSetDlg)
	afx_msg void OnComSetButton();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeModuleCombo();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnConnectButton();
	afx_msg void OnDisconnectButton();
	//}}AFX_MSG
	afx_msg LRESULT OnCommunication(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_METERSETDLG_H__008DAB5F_AC70_4A70_A329_4614993CE482__INCLUDED_)
