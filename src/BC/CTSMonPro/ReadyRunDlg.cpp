// ReadyRunDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "ReadyRunDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReadyRunDlg dialog

// CReadyRunDlg::CReadyRunDlg(CWnd* pParent /*=NULL*/)
// 	: CDialog(CReadyRunDlg::IDD, pParent)
CReadyRunDlg::CReadyRunDlg(CCTSMonProDoc *pDoc, CWnd* pParent /*=NULL*/)
: CDialog(
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CReadyRunDlg::IDD):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CReadyRunDlg::IDD2):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CReadyRunDlg::IDD3):
		(CReadyRunDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CReadyRunDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	ASSERT(pDoc);
	m_pDoc = pDoc;

}


void CReadyRunDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReadyRunDlg)
	DDX_Control(pDX, IDC_PROGRESS1, m_CtrlProgress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReadyRunDlg, CDialog)
	//{{AFX_MSG_MAP(CReadyRunDlg)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReadyRunDlg message handlers

BOOL CReadyRunDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdateData();
	CString strTemp;
	strTemp.Format(Fun_FindMsg("OnInitDialog_msg","IDD_WORK_READY_DLG"),m_nReayTime);
	//@ strTemp.Format("대기시간 : %d 초",m_nReayTime);
	GetDlgItem(IDC_LAB_READY_TIME)->SetWindowText(strTemp);

	m_nProgPos=0;

	m_CtrlProgress.SetRange(0,m_nReayTime);
	m_CtrlProgress.SetPos(0);

	SetTimer(1,1000,NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CReadyRunDlg::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	m_nProgPos++;
	m_CtrlProgress.SetPos(m_nProgPos);
	if (m_nProgPos == m_nReayTime) OnOK();

	CDialog::OnTimer(nIDEvent);
}