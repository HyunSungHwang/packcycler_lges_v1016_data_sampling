// stdafx.cpp : source file that includes just the standard includes
//	CTSMonPro.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h" 


CString   g_AppPath;
APPINFO   g_AppInfo;
CString   g_AppIni;
CString   g_AppTempPath;

CAN_RX_SAVE gst_CanRxSave;
CAN_TX_SAVE gst_CanTxSave;

CAN_RX_SAVE gst_CanRxSaveSlave;
CAN_TX_SAVE gst_CanTxSaveSlave;

CString g_strpathIni;
int gi_Language;

void init_Language() 
{
	gi_Language=0;
	g_strpathIni = "";
	CString strDefault, getPath;
	strDefault.Format("Don't Read");
	gi_Language = AfxGetApp()->GetProfileInt("Config","Language",1);
	char path[MAX_PATH];
	GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	getPath=(LPSTR)path;
	int i = getPath.ReverseFind('\\');
	getPath = getPath.Left(i);
	getPath.Replace("\\","\\\\");

	switch(gi_Language)	
	{
	case 1	:	{

		g_strpathIni.Format("%s\\Language_PackCycler\\MonitorPro\\Korean.ini", getPath);
				}break;

	case 2	:	{
		g_strpathIni.Format("%s\\Language_PackCycler\\MonitorPro\\English.ini", getPath);
				}break;

	case 3	:	{
		g_strpathIni.Format("%s\\Language_PackCycler\\MonitorPro\\Polish.ini", getPath);
				}break;
	default:
		g_strpathIni.Format("%s\\Language_PackCycler\\MonitorPro\\Korean.ini", getPath);
		break;
	}
}

CString Fun_FindMsg(CString strMsg, CString strFindMsg)
{
	CString strDefault;
	CString strReturn;
	strDefault.Format("Don't Read");
	strReturn = GIni::ini_GetFile(g_strpathIni,strFindMsg,strMsg,strDefault);
	if((strReturn.IsEmpty() == FALSE)||(strReturn != _T(""))) //yulee 20190610_1
		return strReturn;
	else
		return strDefault;
}