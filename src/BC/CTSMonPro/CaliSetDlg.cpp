// CaliSetDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CaliSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCaliSetDlg dialog


CCaliSetDlg::CCaliSetDlg(CCaliPoint* pCalPoint, 
						 int nMode,
						 CWnd* pParent /*=NULL*/)
//	: CDialog(CCaliSetDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCaliSetDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCaliSetDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CCaliSetDlg::IDD3):
	(CCaliSetDlg::IDD), pParent )
{
	//{{AFX_DATA_INIT(CCaliSetDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nMode = nMode;
	m_pCalPoint = pCalPoint;
	m_nVCalRange = CAL_RANGE1;	
	m_nVCheckRange = CAL_RANGE1;	
	m_nICalRange = CAL_RANGE1;	
	m_nICheckRange = CAL_RANGE1;	

}


void CCaliSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCaliSetDlg)
	DDX_Control(pDX, IDC_I_CHECK_COMBO, m_ctrlICheck);
	DDX_Control(pDX, IDC_V_CHECK_COMBO, m_ctrlVCheck);
	DDX_Control(pDX, IDC_I_SET_COMBO, m_ctrlISet);
	DDX_Control(pDX, IDC_V_SET_COMBO, m_ctrlVSet);
	DDX_Control(pDX, IDC_POINT1, m_wndListPoint1);
	DDX_Control(pDX, IDC_POINT2, m_wndListPoint2);
	DDX_Control(pDX, IDC_POINT3, m_wndListPoint3);
	DDX_Control(pDX, IDC_CHECK1, m_wndListCheck1);
	DDX_Control(pDX, IDC_CHECK2, m_wndListCheck2);
	DDX_Control(pDX, IDC_CHECK3, m_wndListCheck3);
	DDX_Control(pDX, IDC_CALIPOINT, m_ctrlSetCali);
	DDX_Control(pDX, IDC_CHECKPOINT, m_ctrlCheckCali);
	DDX_Check(pDX, IDC_CALIPOINT, m_bCaliPoint);
	DDX_Check(pDX, IDC_CHECKPOINT, m_bCheckPoint);
	DDX_Control(pDX, IDC_MAX_POINT_STATIC, m_cutionLabel);

	
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCaliSetDlg, CDialog)
	//{{AFX_MSG_MAP(CCaliSetDlg)
	ON_CBN_SELCHANGE(IDC_V_SET_COMBO, OnSelchangeVSetCombo)
	ON_CBN_SELCHANGE(IDC_V_CHECK_COMBO, OnSelchangeVCheckCombo)
	ON_CBN_SELCHANGE(IDC_I_SET_COMBO, OnSelchangeISetCombo)
	ON_CBN_SELCHANGE(IDC_I_CHECK_COMBO, OnSelchangeICheckCombo)
	ON_BN_CLICKED(IDC_CALIPOINT, OnCalipoint)
	ON_BN_CLICKED(IDC_CHECKPOINT, OnCheckpoint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCaliSetDlg message handlers

BOOL CCaliSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_cutionLabel.SetTextColor(RGB(255, 0, 0));

	m_bCaliPoint = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Set Cali", 0);
	m_bCheckPoint = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check Cali", 0);

	m_ctrlSetCali.SetCheck(m_bCaliPoint);
	m_ctrlCheckCali.SetCheck(m_bCheckPoint);
	m_wndListPoint1.m_bCheckCali = FALSE;
	m_wndListCheck1.m_bCheckCali = FALSE;

	{ //Set Max Boundary data with Cell Type
		float fMaxBoundaryData;
		fMaxBoundaryData = 10000000.0f;		//Max 10000A;
		m_wndListPoint1.SetBoundaryData(fMaxBoundaryData);
		m_wndListPoint2.SetBoundaryData(fMaxBoundaryData);
		m_wndListPoint3.SetBoundaryData(fMaxBoundaryData);
		m_wndListCheck1.SetBoundaryData(fMaxBoundaryData);
		m_wndListCheck2.SetBoundaryData(fMaxBoundaryData);
		m_wndListCheck3.SetBoundaryData(fMaxBoundaryData);
		
		m_wndListPoint1.SetDigitMode(TRUE);
		m_wndListPoint2.SetDigitMode(TRUE);
		m_wndListPoint3.SetDigitMode(TRUE);
		m_wndListCheck1.SetDigitMode(TRUE);
		m_wndListCheck2.SetDigitMode(TRUE);
		m_wndListCheck3.SetDigitMode(TRUE);

	}

	int nVoltageUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0);
	int nCurrentUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0);

	CString strVItem, strIItem;
	{ //List Property
		int nWidth = 95;
		m_wndListPoint1.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		strVItem = Fun_FindMsg("OnInitDialog_msg1","IDD_CALI_SET_DLG");
		//@ strVItem = "전압값(mV)";
		strIItem = Fun_FindMsg("OnInitDialog_msg2","IDD_CALI_SET_DLG");
		//@ strIItem = "전류값(mA)";
		if(nVoltageUnitMode)	strVItem = Fun_FindMsg("OnInitDialog_msg3","IDD_CALI_SET_DLG");
		//@ if(nVoltageUnitMode)	strVItem = "전압값(uV)";
		if(nCurrentUnitMode)	strIItem = Fun_FindMsg("OnInitDialog_msg4","IDD_CALI_SET_DLG");
		//@ if(nCurrentUnitMode)	strIItem = "전류값(uA)";

		m_wndListPoint1.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);
		DWORD dwExStyle = m_wndListPoint1.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListPoint1.SetExtendedStyle( dwExStyle );

		m_wndListPoint2.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndListPoint2.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListPoint2.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListPoint2.SetExtendedStyle( dwExStyle );

		m_wndListPoint3.InsertColumn(0, "", LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListPoint3.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListPoint3.SetExtendedStyle( dwExStyle );

		m_wndListCheck1.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndListCheck1.InsertColumn(1, strVItem, LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListCheck1.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListCheck1.SetExtendedStyle( dwExStyle );

		m_wndListCheck2.InsertColumn(0, "No", LVCFMT_RIGHT, 45);
		m_wndListCheck2.InsertColumn(1, strIItem, LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListCheck2.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListCheck2.SetExtendedStyle( dwExStyle );

		m_wndListCheck3.InsertColumn(0, "", LVCFMT_RIGHT, nWidth);
		dwExStyle = m_wndListCheck3.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndListCheck3.SetExtendedStyle( dwExStyle );
	}

	CString strMsg;
	for(int i =0; i<MAX_CALIB_SET_POINT; i++)
	{
		strMsg.Format("%d", i+1);
		m_wndListPoint1.SetItemText(i, 0, strMsg);
		m_wndListPoint2.SetItemText(i, 0, strMsg);
	}
	for(int i = 0; i<MAX_CALIB_CHECK_POINT; i++)
	{
		strMsg.Format("%d", i+1);
		m_wndListCheck1.SetItemText(i, 0, strMsg);
		m_wndListCheck2.SetItemText(i, 0, strMsg);
	}

	for(int i = 0; i<SFT_MAX_VOLTAGE_RANGE; i++)
	{
		strMsg.Format("Range #%d", i+1);
		m_ctrlVCheck.AddString(strMsg);
		m_ctrlVSet.AddString(strMsg);;
		m_ctrlVCheck.SetItemData(i, i);
		m_ctrlVSet.SetItemData(i, i);
	}
	for(int i = 0; i<SFT_MAX_CURRENT_RANGE; i++)
	{
		strMsg.Format("Range #%d", i+1);
		m_ctrlICheck.AddString(strMsg);
		m_ctrlISet.AddString(strMsg);
		m_ctrlICheck.SetItemData(i, i);
		m_ctrlISet.SetItemData(i, i);
	}
	m_ctrlVCheck.SetCurSel(0);
	m_ctrlVSet.SetCurSel(0);
	m_ctrlICheck.SetCurSel(0);
	m_ctrlISet.SetCurSel(0);

	LoadCalConfigFile();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CCaliSetDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:
			return FALSE;			
		case VK_ESCAPE:
			return FALSE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CCaliSetDlg::DestroyWindow() 
{
	return CDialog::DestroyWindow();
}

void CCaliSetDlg::OnOK() 
{
	if( SaveAll() == FALSE )
		return;
	
	CDialog::OnOK();
}

BOOL CCaliSetDlg::SaveAll()
{
	UpdatePointData();

	m_pCalPoint->SavePointData();

//	UINT nID = CFile::modeWrite | CFile::shareDenyNone;
//
//	CAL_POINT aCalPoint, aCheckPoint;
//	CString strTmp;
//	CListCtrl* pList = NULL;
//
//	CFile aFile;
//	CFileFind aFinder;
//
//	//////////////////////////////////////////////////////////////////////
//	// 1. Loading Calibration Point File
//	CString strFileName = _T("Cal.dat");
//	if( aFinder.FindFile(strFileName) == FALSE )
//		nID |= CFile::modeCreate;
//	aFinder.Close();
//	
//	TRY
//		aFile.Open(strFileName, nID );
//	CATCH( CFileException, ex )
//	{
//		ex->Delete();
//		return false;
//	}
//	END_CATCH
//
//	for( int nI = 0; nI < MAX_CALIB_RANGE; nI++ )
//	{
//		switch( nI )
//		{
//		case 0://Point1
//			{
//				pList = &m_wndListPoint1;
//				break;
//			}
//		case 1://:Point2
//			{
//				pList = &m_wndListPoint2;
//				break;
//			}
//		case 2://Point3
//			{
//				pList = &m_wndListPoint3;
//				break;
//			}
//		}
//
//		ZeroMemory(&aCalPoint, sizeof(CAL_POINT));
//		aCalPoint.byType = nI;
//		aCalPoint.byValidPointNum = pList->GetItemCount();
//		for( int nX = 0; nX < aCalPoint.byValidPointNum; nX++ )
//		{
//			strTmp = pList->GetItemText(nX, 0);
//			double dblTmp = atof(strTmp) * MULTI_LONG_2_FLOAT;
//			aCalPoint.lPoint[nX] = (long)dblTmp;
//		}
//
//		Sorting(aCalPoint);
//		aFile.Write(&aCalPoint, sizeof(CAL_POINT));
//
////		CopyMemory(&m_pCalPoint[nI], &aCalPoint, sizeof(CAL_POINT));
//	}
//	aFile.Close();
//
//	//////////////////////////////////////////////////////////////////////
//	// 2. Loading Check Point File
//	strFileName = _T("Check.dat");
//	if( aFinder.FindFile(strFileName) == FALSE )
//		nID |= CFile::modeCreate;
//	aFinder.Close();
//
//	TRY
//		aFile.Open(strFileName, nID );
//	CATCH( CFileException, ex )
//	{
//		ex->Delete();
//		return false;
//	}
//	END_CATCH
//
//	for(int nI = 0; nI < MAX_CALIB_RANGE; nI++ )
//	{
//		switch( nI )
//		{
//		case 0://Point1
//			{
//				pList = &m_wndListCheck1;
//				break;
//			}
//		case 1://:Point2
//			{
//				pList = &m_wndListCheck2;
//				break;
//			}
//		case 2://Point3
//			{
//				pList = &m_wndListCheck3;
//				break;
//			}
//		}
//
////		ZeroMemory(&aCheckPoint, sizeof(CAL_POINT));
//		aCheckPoint.byType = nI;
//		aCheckPoint.byValidPointNum = pList->GetItemCount();
//		for( int nX = 0; nX < aCheckPoint.byValidPointNum; nX++ )
//		{
//			strTmp = pList->GetItemText(nX, 0);
//			double dblTmp = atof(strTmp) * MULTI_LONG_2_FLOAT;
//			aCheckPoint.lPoint[nX] = (long)dblTmp;
//		}
//
//		Sorting(aCheckPoint);
//		aFile.Write(&aCheckPoint, sizeof(CAL_POINT));
//
////		CopyMemory(&m_pCheckPoint[nI], &aCheckPoint, sizeof(CAL_POINT));
//	}
//	aFile.Close();
	
	return TRUE;
}

void CCaliSetDlg::LoadCalConfigFile()
{
	CString strTmp;
	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListPoint1.DeleteAllItems();
	m_nVCalRange = m_ctrlVCheck.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetVtgSetPointCount((WORD)m_nVCalRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVSetPoint((WORD)m_nVCalRange, (WORD)i));
		m_wndListPoint1.SetItemText(i, 1, strTmp);	
	}

	m_wndListCheck1.DeleteAllItems();
	m_nVCheckRange = m_ctrlVCheck.GetCurSel();
	for(int i = 0; i<m_pCalPoint->GetVtgCheckPointCount((WORD)m_nVCheckRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVCheckPoint((WORD)m_nVCheckRange, (WORD)i));
		m_wndListCheck1.SetItemText(i, 1, strTmp);	
	}	

	m_wndListPoint2.DeleteAllItems();
	m_nICalRange = m_ctrlISet.GetCurSel();
	for(int i = 0; i<m_pCalPoint->GetCrtSetPointCount((WORD)m_nICalRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetISetPoint((WORD)m_nICalRange, (WORD)i));
		m_wndListPoint2.SetItemText(i, 1, strTmp);	

	}	

	m_wndListCheck2.DeleteAllItems();
	m_nICheckRange = m_ctrlICheck.GetCurSel();
	for(int i = 0; i<m_pCalPoint->GetCrtCheckPointCount((WORD)m_nICheckRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetICheckPoint((WORD)m_nICheckRange, (WORD)i));
		m_wndListCheck2.SetItemText(i, 1, strTmp);	

	}	
}


void CCaliSetDlg::OnSelchangeVSetCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);
	
	CString strTmp;
	UpdatePointData();

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListPoint1.DeleteAllItems();
	m_nVCalRange = m_ctrlVSet.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetVtgSetPointCount((WORD)m_nVCalRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVSetPoint((WORD)m_nVCalRange, (WORD)i));
		m_wndListPoint1.SetItemText(i, 1, strTmp);	
	}

	
}

void CCaliSetDlg::OnSelchangeVCheckCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);
	CString strTmp;

	UpdatePointData();
	
	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListCheck1.DeleteAllItems();
	m_nVCheckRange = m_ctrlVCheck.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetVtgCheckPointCount((WORD)m_nVCheckRange); i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck1.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetVCheckPoint((WORD)m_nVCheckRange, (WORD)i));
		m_wndListCheck1.SetItemText(i, 1, strTmp);	
	}	
}

void CCaliSetDlg::OnSelchangeISetCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);
	CString strTmp;

	UpdatePointData();

	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListPoint2.DeleteAllItems();
	m_nICalRange = m_ctrlISet.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetCrtSetPointCount((WORD)m_nICalRange); (WORD)i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListPoint2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetISetPoint((WORD)m_nICalRange, (WORD)i));
		m_wndListPoint2.SetItemText(i, 1, strTmp);	

	}	
}

void CCaliSetDlg::OnSelchangeICheckCombo() 
{
	// TODO: Add your control notification handler code here

	ASSERT(m_pCalPoint);
	CString strTmp;

	UpdatePointData();
	
	LV_ITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LV_ITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndListCheck2.DeleteAllItems();
	m_nICheckRange = m_ctrlICheck.GetCurSel();
	for(int i=0; i<m_pCalPoint->GetCrtCheckPointCount((WORD)m_nICheckRange); (WORD)i++)
	{
		lvItem.iItem = i;
		strTmp.Format("%d", i+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndListCheck2.InsertItem(&lvItem);
		strTmp.Format("%.3f", m_pCalPoint->GetICheckPoint((WORD)m_nICheckRange, (WORD)i));
		m_wndListCheck2.SetItemText(i, 1, strTmp);	

	}	
}

void CCaliSetDlg::UpdatePointData()
{
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Set Cali", m_bCaliPoint);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Check Cali", m_bCheckPoint);
	//V Calibration Point Update
	CString strTmp;
	double data[MAX_CALIB_SET_POINT];
	m_wndListPoint1.UpdateText();
	int nDatacount = m_wndListPoint1.GetItemCount();
	if(nDatacount > MAX_CALIB_SET_POINT)
	{
		nDatacount = MAX_CALIB_SET_POINT;
	}
	for(int i =0; i<nDatacount; i++)
	{
		strTmp = m_wndListPoint1.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetVSetPointData(nDatacount, data, (WORD)m_nVCalRange);

	m_wndListCheck1.UpdateText();
	nDatacount = m_wndListCheck1.GetItemCount();
	if(nDatacount > MAX_CALIB_CHECK_POINT)
	{
		nDatacount = MAX_CALIB_CHECK_POINT;
	}
	for(int i = 0; i<nDatacount; i++)
	{
		strTmp = m_wndListCheck1.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetVCheckPointData(nDatacount, data, (WORD)m_nVCheckRange);

	m_wndListPoint2.UpdateText();
	nDatacount = m_wndListPoint2.GetItemCount();
	if(nDatacount > MAX_CALIB_SET_POINT)
	{
		nDatacount = MAX_CALIB_SET_POINT;
	}
	for(int i = 0; i<nDatacount; i++)
	{
		strTmp = m_wndListPoint2.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetISetPointData(nDatacount, data, (WORD)m_nICalRange);

	m_wndListCheck2.UpdateText();
	nDatacount = m_wndListCheck2.GetItemCount();
	if(nDatacount > MAX_CALIB_CHECK_POINT)
	{
		nDatacount = MAX_CALIB_CHECK_POINT;
	}
	for(int i = 0; i<nDatacount; i++)
	{
		strTmp = m_wndListCheck2.GetItemText(i, 1);
		data[i] = atof(strTmp);
	}
	m_pCalPoint->SetICheckPointData(nDatacount, data, (WORD)m_nICheckRange);

}

void CCaliSetDlg::OnCalipoint() 
{
	UpdateData();
	m_wndListPoint2.m_bCheckCali = m_bCaliPoint;
}

void CCaliSetDlg::OnCheckpoint() 
{
	UpdateData();
	m_wndListCheck2.m_bCheckCali = m_bCheckPoint;
}