/** 
 *@file		JsonMon.h
 *@brief	JsonMon.h
 *@details	[#JSonMon] 설비 모니터링 정보를 JSON 파일로 내보내기 하는 기능을 구현한 클래스.
 *			가능한 다른 소스에 이식이 쉽도록 클래스로 모듈화 하여 구현.
 *			CTSMonPro 측 추가 필요 포인트는 #JSonMon 으로 검색하면 찾을 수 있도록 주석 태그 달아 놓았음.
 *
 *@author	Kang se-jun
 *@date		2020-06-25
 */

#pragma once

#include "../Include/json.h" //ksj 20200625 : JSON 라이브러리 추가
#include "afxcoll.h"
#ifdef _DEBUG
#pragma comment(lib, "../lib/lib_jsonD.lib") //ksj 20200625 : JSON 라이브러리 추가
#else
#pragma comment(lib, "../lib/lib_json.lib") //ksj 20200625 : JSON 라이브러리 추가
#endif	//_DEBUG


#define JSON_MON_VERSION "1.0.3(20210701)"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define JSON_MON_EXPORT_AUX //정의 되어있는 경우 AUX 관련 상태 JSON 출력. 불필요한 경우 이것만 주석처리
#define JSON_MON_EXPORT_CAN //정의 되어있는 경우 CAN 관련 상태 JSON 출력. 불필요한 경우 이것만 주석처리
#define JSON_MON_EXPORT_CHAMBER //정의 되어있는 경우 챔버 상태 JSON 출력. 불필요한 경우 이것만 주석처리
#define JSON_MON_EXPORT_CHILLER //정의 되어있는 경우 칠러 관련 상태 JSON 출력. 불필요한 경우 이것만 주석처리
#define JSON_MON_EXPORT_POWERSUPPLY //정의 되어있는 경우 파워서플라이 관련 상태 JSON 출력. 불필요한 경우 이것만 주석처리

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define JSON_MON_DEFAULT_EQP_TYPE "PACK" //PACK, CELL
#define JSON_MAX_MODULE_COUNT	4 //최대 처리 가능 충방전기 모듈 개수

////////////////////
#define JSON_DEFAULT_TIME_DAY	"00000000" 
#define JSON_DEFAULT_TIME		"000000" 
////////////////////

class CCTSMonProDoc;
class CCTSMonProView;
class CMainFrame;


//////////////////////////////////////////////////////////////////////////////////////////////////////////
class CJsonMon
{
public:
	CJsonMon(CCTSMonProDoc* pDoc, CCTSMonProView* m_pView, CMainFrame* pMainframe);
	~CJsonMon(void);
	
	void SetJsonPath(CString strJsonPath); //파일 저장 경로 설정
	int RunJsonMon(int nPeriodMilliTimeSec = 10000); //주기적인 모니터링 파일 생성 쓰레드 시작 함수.
	int UpdateJsonMon(int nModuleID); //호출시 쓰레드를 이용하여 모니터링 파일 1회 갱신 후 쓰레드 종료.
	int StopJsonMon(); //쓰레드 중단
	BOOL GetRunState(){ return m_bJsonMonThread; } //모니터링 쓰레드 작동 상태 리턴. stop : 0, run : 1	
	int CreateEmgMonFile(int nModuleID, SFT_EMG_DATA *pEmgData); //모듈 emg 파일 생성 함수	

	CCTSMonProView* GetCCTSMonProView() { return m_pView; } //ksj 20200909 : View 포인터 리턴
	CCTSMonProDoc* GetCCTSMonProDoc() { return m_pDoc; } //ksj 20200909 : Doc 포인터 리턴
	CMainFrame* GetCMainFrame() { return m_pMainframe; } //ksj 20200909 : Mainframe 포인터 리턴

private:

	CCriticalSection m_cs; //크리티컬 섹션 추가.
	CString m_strJsonFilePath;
	CWinThread* m_pJsonMonThread;	
	BOOL m_bJsonMonThread;
	HANDLE m_hWaitEvent;
	HANDLE m_hExitEvent;
	int m_nPeriodMilliTimeSec;

	CString m_strOnTimeDay[JSON_MAX_MODULE_COUNT]; //모듈별 접속 시간(day)
	CString m_strOnTime[JSON_MAX_MODULE_COUNT];	//모듈별 접속 시간
	CString m_strOffTimeDay[JSON_MAX_MODULE_COUNT];//모듈별 접속해제 시간(day)
	CString m_strOffTime[JSON_MAX_MODULE_COUNT];	//모듈별 접속해제 시간
	int m_nPrevModuleOnState[JSON_MAX_MODULE_COUNT]; //최종 모듈 접속 상태 0:오프라인, 1:온라인

	CString GetOnTimeDay(int nModuleID) { return m_strOnTimeDay[nModuleID-1]; }
	CString GetOnTime(int nModuleID) { return m_strOnTime[nModuleID-1]; }
	CString GetOffTimeDay(int nModuleID) { return m_strOffTimeDay[nModuleID-1]; }
	CString GetOffTime(int nModuleID) { return m_strOffTime[nModuleID-1]; }	
	void	SetPrevModuleOnState(int nModuleID, int nState) { m_nPrevModuleOnState[nModuleID-1] = nState; }
	int		GetPrevModuleOnState(int nModuleID) { return m_nPrevModuleOnState[nModuleID-1]; }

	int CreateChannelMonFile(int nModuleID, BOOL bExit = FALSE); //채널 모니터링 파일 생성 함수. 직접 호출 하지말고 RunJsonMon이나 UpdateJsonMon 호출해서 쓰레드를 이용하도록 한다.
	int CreateModuleMonFile(BOOL bExit = FALSE); //모듈 모니터링 파일 생성 함수. 직접 호출 하지말고 RunJsonMon이나 UpdateJsonMon 호출해서 쓰레드를 이용하도록 한다.
	static UINT ThreadAutoUpdateMonFile(LPVOID pParam);
	static UINT ThreadCreateMonFile(LPVOID pParam);


	//상속 서브클래싱 가능하도록 protected 권한의 가상함수로 객체 분할.
	//하기 함수들은 상속받아서 재구현 가능하도록 열어두었음.
	//다른 프로젝트에 이식할때 필요해따라 상속받아서 커스터마이징 할 수 있음.
protected:
	CCTSMonProView* m_pView;
	CCTSMonProDoc* m_pDoc;
	CMainFrame* m_pMainframe;

	//모듈 정보
	virtual int MakeModuleInfo(Json::Value& root, BOOL bExit = FALSE);

	///EMG 정보 생성
	virtual int MakeEmgInfo(int nModuleID, SYSTEMTIME& lt, SFT_EMG_DATA* pEmgData, Json::Value& root);

	//모듈채널정보
	virtual int MakeChannelInfoModule(CCyclerModule* pModule, Json::Value& root, BOOL bExit = FALSE);
	virtual int MakeChannelInfoChannel(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel);
	virtual int MakeChannelInfoAux(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel);
	virtual int MakeChannelInfoCan(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel);
	virtual int MakeChannelInfoChamber(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel);
	virtual int MakeChannelInfoChiller(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel);
	virtual int MakeChannelInfoPowerSupply(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel);
	virtual int MakeChannelInfoTotalChamber(Json::Value& root);
	virtual int MakeChannelInfoTotalChiller(Json::Value& root);		

public:
	void UpdateModuleOffTime(int nModuleID); //특정 모듈의 통신 두절 시간 생성
	void UpdateModuleOnTime(int nModuleID); //특정 모듈의 접속 시간 생성
	BOOL GetConnectedElapsedTime(CCyclerModule* pModule, CString& strConnectedDay, CString& strConnectedTime);
};


/////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct _STR_UPDATE_THREAD_PARAM
{
	CJsonMon* pJsonMon;
	int nModuleID;
}STR_UPDATE_THREAD_PARAM;
