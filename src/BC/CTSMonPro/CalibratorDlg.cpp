// CalibratorDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CTSMonProView.h"
#include "CalibratorDlg.h"
#include "CTSMonProDoc.h"
#include "CaliSetDlg.h"

#include  <math.h>

#include "CalDataExportDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalibratorDlg dialog


CCalibratorDlg::CCalibratorDlg(int nModuleID, CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CCalibratorDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCalibratorDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCalibratorDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CCalibratorDlg::IDD3):
	(CCalibratorDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CCalibratorDlg)
	m_nTotalMode = 0;
	//}}AFX_DATA_INIT

	ASSERT(pDoc);
	m_pDoc = pDoc;

//	m_pChannelData = NULL;
	m_bCalStatus = false;

	m_pCaliResultImage = NULL;
	m_bStopFlag = FALSE;

	m_dwElapsedTime = 60;	
	m_nModuleID = nModuleID;
//	m_dwRemainTime = 0;

	m_nAVGOnePointCalTime = 35;		//35*200msec(CAL_TIMER_INTERVAL) = 7Sec 
	m_nCalibrationItem = CAL_VOLTAGE;

//	m_fVtgDAErrorSet = 2.0f;
//	m_fVtgADErrorSet = 2.0f;
//	m_fCrtDAErrorSet = 2.0f;
//	m_fCrtADErrorSet = 2.0f;

	m_nStateWait = 0;
	m_nChargeDisChargeMode = 0;
	m_nPresNo = -1;

}

CCalibratorDlg::~CCalibratorDlg()
{
//	if( m_pChannelData )
//	{
//		delete [] m_pChannelData;
//	}
//
	m_Rangemenu.DestroyMenu();

	if( m_pCaliResultImage )
	{
		delete m_pCaliResultImage;
	}
}

void CCalibratorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCalibratorDlg)
	DDX_Control(pDX, IDC_ACCURACY_BUTTON, m_btnAccurayBtm);
	DDX_Control(pDX, IDC_BTN_SAVE_FILE, m_btnSaveFile);
	DDX_Control(pDX, IDC_BTN_UPDATE_SEL, m_btnSelect);
	DDX_Control(pDX, IDC_BTN_UPDATE_ALL, m_btnUpdateAll);
	DDX_Control(pDX, IDC_STOP_BUTTON, m_btnStop);
	DDX_Control(pDX, IDC_ALL_ITEM_BUTTON, m_btnAllItem);
	DDX_Control(pDX, IDC_BTN_RANGE1, m_btnRange1);
	DDX_Control(pDX, IDC_BTN_VOLTAGE, m_btnVoltage);
	DDX_Control(pDX, IDC_BTN_CONNECT, m_btnConnect);
	DDX_Control(pDX, IDC_BTN_SET_POINT, m_btnSetPoint);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_COMBO3, m_ctrlSelDataTypeCombo);
	DDX_Control(pDX, IDC_CAL_PROGRESS, m_ctrlProgress);
	DDX_Control(pDX, IDC_CAL_CH_LIST, m_CalChSelList);
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ModuleCombo);
	DDX_Control(pDX, IDC_COMBO_DP_MODE, m_wndTotalPoint);
	DDX_Control(pDX, IDC_LIST_TOTAL, m_wndTotal);
	DDX_Control(pDX, IDC_LIST_CHECK_RESULT, m_wndCheck);
	DDX_Radio(pDX, IDC_RADIO_MODE_CALIB, m_nTotalMode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCalibratorDlg, CDialog)
	//{{AFX_MSG_MAP(CCalibratorDlg)
	ON_WM_SYSCOMMAND()
	ON_BN_CLICKED(IDC_BTN_RANGE1, OnBtnRange1)
	ON_BN_CLICKED(IDC_BTN_SET_POINT, OnBtnSetPoint)
	ON_BN_CLICKED(IDC_BTN_CONNECT, OnBtnConnect)
	ON_BN_CLICKED(IDC_BTN_UPDATE_SEL, OnBtnUpdateSel)
	ON_BN_CLICKED(IDC_BTN_UPDATE_ALL, OnBtnUpdateAll)
	ON_CBN_SELCHANGE(IDC_COMBO_DP_MODE, OnSelchangeComboDpMode)
	ON_WM_TIMER()
	ON_NOTIFY(NM_RCLICK, IDC_LIST_TOTAL, OnRclickListTotal)
	ON_COMMAND(ID_CALI_RANGE1, OnCaliRange1)
	ON_COMMAND(ID_CALI_RANGE2, OnCaliRange2)
	ON_COMMAND(ID_CALI_RANGE3, OnCaliRange3)
	ON_BN_CLICKED(IDC_BTN_VOLTAGE, OnBtnVoltage)
	ON_BN_CLICKED(IDC_BTN_SAVE_FILE, OnBtnSaveFile)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_CAL_CH_LIST, OnItemchangedCalChList)
	ON_BN_CLICKED(IDC_STOP_BUTTON, OnStopButton)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_CAL_CH_LIST, OnGetdispinfoCalChList)
	ON_COMMAND(ID_CALI_RANGE_ALL, OnCaliRangeAll)
	ON_BN_CLICKED(IDC_ALL_ITEM_BUTTON, OnAllItemButton)
	ON_COMMAND(ID_CALI_RANGE4, OnCaliRange4)
	ON_COMMAND(ID_CALI_RANGE5, OnCaliRange5)
	ON_CBN_SELCHANGE(IDC_COMBO3, OnSelchangeCombo3)
	ON_BN_CLICKED(IDC_ACCURACY_BUTTON, OnAccuracyButton)
	ON_COMMAND(ID_CALI_RANGE_CHARGE, OnChargeRange)
	ON_COMMAND(ID_CALI_RANGE_DISCHARGE, OnDisChargeRange)
	ON_COMMAND(ID_CALI_RANGE_CHARGE2, OnChargeRange2)
	ON_COMMAND(ID_CALI_RANGE_DISCHARGE2, OnDisChargeRange2)
	ON_COMMAND(ID_CALI_RANGE_CHARGE3, OnChargeRange3)
	ON_COMMAND(ID_CALI_RANGE_DISCHARGE3, OnDisChargeRange3)
	ON_COMMAND(ID_CALI_RANGE_CHARGE4, OnChargeRange4)
	ON_COMMAND(ID_CALI_RANGE_DISCHARGE4, OnDisChargeRange4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCalibratorDlg message handlers

BOOL CCalibratorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

//	m_fVtgDAErrorSet = (float)atof(AfxGetApp()->GetProfileString(REG_CAL_SECTION, "Volt DA Error", "2.0"));
//	m_fVtgADErrorSet = (float)atof(AfxGetApp()->GetProfileString(REG_CAL_SECTION, "Volt AD Error", "2.0"));
//	m_fCrtDAErrorSet = (float)atof(AfxGetApp()->GetProfileString(REG_CAL_SECTION, "Current DA Error", "2.0"));
//	m_fCrtADErrorSet = (float)atof(AfxGetApp()->GetProfileString(REG_CAL_SECTION, "Current AD Error", "2.0"));
	m_nAVGOnePointCalTime = AfxGetApp()->GetProfileInt(REG_CAL_SECTION, "TimeOneRange", m_nAVGOnePointCalTime);


	CString strMsg;
	//Module Selection List
//	InitTree();
	int nModuleID;
	int curIndex = 0;
	for(WORD j =0; j<m_pDoc->GetInstallModuleCount(); j++)
	{
		nModuleID = m_pDoc->GetModuleID(j);
		strMsg.Format("%s", m_pDoc->GetModuleName(nModuleID));
		m_ModuleCombo.AddString(strMsg);
		m_ModuleCombo.SetItemData(j, (DWORD)nModuleID);			//Set Tree Data to Module ID
		if(nModuleID == m_nModuleID)
		{
			curIndex = j;
		}
	}
	m_ModuleCombo.SetCurSel(curIndex);
	m_nModuleID = m_ModuleCombo.GetItemData(curIndex);

	//상태별 표시할 이미지 로딩
	//m_stateImage.Create(IDB_CELL_STATE_ICON,19,17,RGB(255,255,255));
	m_stateImage.Create(IDB_CELL_STATE_ICON1,26,17,RGB(255,255,255));
	m_CalChSelList.SetImageList(&m_stateImage, LVSIL_SMALL);

	//Channel selection List
	m_CalChSelList.InsertColumn(0, "Channel",  LVCFMT_RIGHT, 70);
	m_CalChSelList.InsertColumn(1, Fun_FindMsg("OnInitDialog_msg1","IDD_CALIBRATION_DLG"),  LVCFMT_RIGHT, 100);
	//@ m_CalChSelList.InsertColumn(1, "상태",  LVCFMT_RIGHT, 100);
	m_CalChSelList.InsertColumn(2, Fun_FindMsg("OnInitDialog_msg2","IDD_CALIBRATION_DLG"),  LVCFMT_RIGHT, 150);
	//@ m_CalChSelList.InsertColumn(2, "최종 교정일",  LVCFMT_RIGHT, 150);
	DWORD dwExStyle = m_CalChSelList.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;// | LVS_EX_SUBITEMIMAGES;
	m_CalChSelList.SetExtendedStyle( dwExStyle );

	//선택 모듈로 다시 그림 
	InitCalSelChList(m_nModuleID);	

	//ksj 20200819 : 자동교정 기능 이식 from V100D
	m_bCaliFirstComm = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCalibrationFirstComm",FALSE);
	if (m_bCaliFirstComm) Fun_CalibrationStart(m_nModuleID);		//20130121
	//ksj end

	//각 채널의 상태를 Update한다.
	UpdateMDStateList();
//	OnBtnConnect();

	{ //Combobox Property
		m_wndTotalPoint.AddString("== All ==");
		m_wndTotalPoint.AddString(Fun_FindMsg("OnInitDialog_msg3","IDD_CALIBRATION_DLG"));
		//@ m_wndTotalPoint.AddString("전압");
		m_wndTotalPoint.AddString(Fun_FindMsg("OnInitDialog_msg4","IDD_CALIBRATION_DLG"));
		//@ m_wndTotalPoint.AddString("전류");
	}
	m_wndTotalPoint.SetCurSel(0);

	{ //Combobox Property
		m_ctrlSelDataTypeCombo.AddString("== All ==");
		m_ctrlSelDataTypeCombo.AddString(Fun_FindMsg("OnInitDialog_msg5","IDD_CALIBRATION_DLG"));
		//@ m_ctrlSelDataTypeCombo.AddString("교정전");
		m_ctrlSelDataTypeCombo.AddString(Fun_FindMsg("OnInitDialog_msg6","IDD_CALIBRATION_DLG"));
		//@ m_ctrlSelDataTypeCombo.AddString("교정후");
	}
	m_ctrlSelDataTypeCombo.SetCurSel(0);
	

	//////////////////////////////////////////////
	// 2. Total Result List
	char szTotalHeader[][20] = 
		{"Channel", "Point", "Module", "instrument"};
	//	{"Channel", "Point", "Module", Fun_FindMsg("OnInitDialog_msg7")};
	//@ {"Channel", "Point", "Module", "계측기"};
	int	cy[] = 
		{ 65, 65, 72, 75};

	
	for(int  nI = 0; nI < 4; nI++ )
	{
		m_wndTotal.InsertColumn( nI, szTotalHeader[nI], LVCFMT_CENTER, cy[nI] );
	}

	CString strChNo;
	LVITEM aItem;	ZeroMemory(&aItem, sizeof(aItem));
	dwExStyle = m_wndTotal.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
	m_wndTotal.SetExtendedStyle( dwExStyle );
	
	//////////////////////////////////////////////
	// 3. Check Result List
	CString strUnit;
	int nVoltageUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0);
	if(nVoltageUnitMode)  strUnit = "(uV/";		else strUnit = "(mV/";
	int nCurrentUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0);
	if(nCurrentUnitMode)  strUnit += "uA)";		else strUnit += "mA)";

	// char szCheckHeader[7][20];  
	char szCheckHeader[7][30];  // 20191219
	strcpy(szCheckHeader[0], "ch");
	strcpy(szCheckHeader[1], "Range");
	sprintf(szCheckHeader[2], Fun_FindMsg("OnInitDialog_msg8","IDD_CALIBRATION_DLG"), strUnit);
	//@ sprintf(szCheckHeader[2], "지령%s", strUnit);
	sprintf(szCheckHeader[3], Fun_FindMsg("OnInitDialog_msg9","IDD_CALIBRATION_DLG"), strUnit);
	//@ sprintf(szCheckHeader[3], "계측기%s", strUnit);
	sprintf(szCheckHeader[4], Fun_FindMsg("OnInitDialog_msg10","IDD_CALIBRATION_DLG"), strUnit);
	//@ sprintf(szCheckHeader[4], "지령오차%s", strUnit);
	sprintf(szCheckHeader[5], Fun_FindMsg("OnInitDialog_msg11","IDD_CALIBRATION_DLG"), strUnit);
	//@ sprintf(szCheckHeader[5], "모듈%s", strUnit);
	sprintf(szCheckHeader[6], Fun_FindMsg("OnInitDialog_msg12","IDD_CALIBRATION_DLG"), strUnit);
	//@ sprintf(szCheckHeader[6], "측정오차%s", strUnit);
		
	int	cz[] = 
		{ 80, 95, 80, 80, 85, 80, 85};

	for(int nI = 0; nI < 7; nI++ )
	{
		m_wndCheck.InsertColumn( nI, szCheckHeader[nI], LVCFMT_RIGHT, cz[nI] );
	}

	dwExStyle = m_wndCheck.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_SUBITEMIMAGES ;
	m_wndCheck.SetExtendedStyle( dwExStyle );

	m_pCaliResultImage = new CImageList;
	m_pCaliResultImage->Create(IDB_CALI_RESULT, 15, 5, RGB(255,255,255));
	m_wndCheck.SetImageList(m_pCaliResultImage,LVSIL_SMALL);

	m_ctrlProgress.SetRange(0, 100);

	SetTimer(2000, 700, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCalibratorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if( nID == 0xF060 )
	{
		return;
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CCalibratorDlg::OnBtnSetPoint() 
{
	ASSERT(m_pDoc);
	CCaliSetDlg *pDlg = new CCaliSetDlg(m_pDoc->GetCalPoint(), 0, this);
	if(pDlg->DoModal() == IDOK)
	{

	}
	
	delete pDlg;
	pDlg = NULL;
}

//void CCalibratorDlg::SetConfigurationData(CAL_POINT* pCalPoint, 
//										  CAL_POINT* pCheckPoint, 
//										  int nModuleIndex, 
//										  BYTE* pPossibleChannelList,
//										  int nPossibleChannelNum)
//{
//	m_pCalPoint = pCalPoint;
//	m_pCheckPoint = pCheckPoint;
//
//	m_nModuleID = SFTGetModuleID(nModuleIndex);
////	m_nModuleID = m_pView->GetDocument()->m_pModuleInfo[nModuleIndex].wdModuleID;
//	m_nSockModuleID = nModuleIndex+1;
//
//	m_nChNoFirst = 0xFFFF;
//	m_nChNoLast = 0x0000;
//	m_nSelectedChNum = nPossibleChannelNum;
//	m_pChannelData = new S_CHANNEL_CAL_DATA[nPossibleChannelNum];
//	ZeroMemory(m_pChannelData, sizeof(S_CHANNEL_CAL_DATA)*m_nSelectedChNum);
//	for( int nI = 0; nI < nPossibleChannelNum; nI++)
//	{
//		m_pChannelData[nI].byChannelID = pPossibleChannelList[nI];
//		if( m_nChNoFirst >= m_pChannelData[nI].byChannelID )
//			m_nChNoFirst = m_pChannelData[nI].byChannelID;
//		if( m_nChNoLast <= m_pChannelData[nI].byChannelID )
//			m_nChNoLast = m_pChannelData[nI].byChannelID;
//
//		for( int nRange = 0; nRange < MAX_CALIB_RANGE; nRange++ )
//		{
//			m_pChannelData[nI].aCalSetData[nRange].byRange = 
//								m_pCalPoint[nRange].byType;
//			m_pChannelData[nI].aCalSetData[nRange].byValidCalPointNum = 
//								m_pCalPoint[nRange].byValidPointNum;
//			CopyMemory(m_pChannelData[nI].aCalSetData[nRange].lCaliPoint, 
//					   m_pCalPoint[nRange].lPoint,
//					   sizeof(m_pChannelData[nI].aCalSetData[nRange].lCaliPoint));
//			
//			m_pChannelData[nI].aCalSetData[nRange].byValidCheckPointNum = 
//								m_pCheckPoint[nRange].byValidPointNum;
//			CopyMemory(m_pChannelData[nI].aCalSetData[nRange].lCheckPoint, 
//					   m_pCheckPoint[nRange].lPoint,
//					   sizeof(m_pChannelData[nI].aCalSetData[nRange].lCheckPoint));
//		}
//	}
//}

void CCalibratorDlg::OnBtnConnect() 
{
	CString strTemp;
	CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	if(pMD == NULL)
	{
		strTemp.Format(Fun_FindMsg("OnBtnConnect_msg1","IDD_CALIBRATION_DLG") , m_pDoc->GetModuleName(m_nModuleID));
		//@ strTemp.Format("%s 정보 요청 실패" , m_pDoc->GetModuleName(m_nModuleID));
		MessageBox(strTemp, Fun_FindMsg("OnBtnConnect_msg2","IDD_CALIBRATION_DLG"));
		//@ MessageBox(strTemp, "실패");
		return;
	}

	if(pMD->GetState() == PS_STATE_LINE_OFF)	return;
	
	BeginWaitCursor();

	SFT_CALI_SET_READY_DATA aType;
	ZeroMemory(&aType, sizeof(SFT_CALI_SET_READY_DATA));

	if(	m_nCalibrationItem == CAL_VOLTAGE)
	{
		aType.nCode = CAL_EXT_SHUNT;	//Read current to ext. shut by voltage type;
	}
	else
	{
		aType.nCode = CAL_METER_SHUNT;	//Read current to ext. shut by voltage type;		
	}
	
	SFT_CALI_READY_RESPONSE *lpData;
	lpData = (SFT_CALI_READY_RESPONSE *)::SFTSendDataCmd(m_nModuleID, SFT_CMD_CALI_READY, NULL, sizeof(SFT_CALI_READY_RESPONSE), &aType, sizeof(SFT_CALI_SET_READY_DATA));

	if(lpData == NULL || lpData->nCode == 0)		//connection fail
	{
		strTemp.Format(Fun_FindMsg("OnBtnConnect_msg3","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_nModuleID));
		//@ strTemp.Format("%s 교정 준비 실패(Meter와 연결 상태 확인)", m_pDoc->GetModuleName(m_nModuleID));
		SetDlgItemText(IDC_COM_STATE_STATIC, strTemp);
		m_pDoc->WriteSysLog(strTemp);
		MessageBox(strTemp, Fun_FindMsg("OnBtnConnect_msg4","IDD_CALIBRATION_DLG"), MB_ICONEXCLAMATION);
		//@ MessageBox(strTemp, "교정 준비 실패", MB_ICONEXCLAMATION);
		
//		GetDlgItem(IDC_BTN_VOLTAGE)->EnableWindow(FALSE);
//		GetDlgItem(IDC_BTN_RANGE1)->EnableWindow(FALSE);	
		GetDlgItem(IDC_BTN_CONNECT)->EnableWindow();
	}
	else
	{
		strTemp.Format(Fun_FindMsg("OnBtnConnect_msg5","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_nModuleID));
		//@ strTemp.Format("%s 교정 준비 성공", m_pDoc->GetModuleName(m_nModuleID));
		SetDlgItemText(IDC_COM_STATE_STATIC, strTemp);
		m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);

//		GetDlgItem(IDC_BTN_VOLTAGE)->EnableWindow();
//		GetDlgItem(IDC_BTN_RANGE1)->EnableWindow();	
		GetDlgItem(IDC_BTN_CONNECT)->EnableWindow(FALSE);	
	}

	EndWaitCursor();
}

void CCalibratorDlg::OnBtnVoltage() 
{
	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
	if(mi == NULL)	return;
	
	m_nCalibrationItem = CAL_VOLTAGE;

	BOOL bUseDischargeCal = AfxGetApp()->GetProfileInt(REG_CAL_SECTION, "UseDischargeCal", 1); //ksj 20170725 : 방전 교정 사용 여부. 기본 값 1 : 사용함.
	
//	OnBtnConnect();

	int nCount = mi->GetVoltageRangeCount();
	if(nCount  > 1)
	{

		m_Rangemenu.DestroyMenu();
		m_SubMenu.DestroyMenu();
		// Create a new menu for the application window.
		VERIFY(m_Rangemenu.CreatePopupMenu());
		m_SubMenu.CreatePopupMenu();
		m_SubMenu.AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg1","IDD_CALIBRATION_DLG"));
		//@ m_SubMenu.AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE, (LPCTSTR)"충전 교정");
		//m_SubMenu.AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg2"));
		if(bUseDischargeCal) m_SubMenu.AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg2","IDD_CALIBRATION_DLG"));
		//@ m_SubMenu.AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE, (LPCTSTR)"방전 교정");
		m_Rangemenu.AppendMenu(MF_POPUP, (UINT)m_SubMenu.GetSafeHmenu(), (LPCTSTR)"Range &1");

		CMenu *pMenu2 = new CMenu();
		pMenu2->CreatePopupMenu();
		pMenu2->AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE2, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg3","IDD_CALIBRATION_DLG"));
		//@ pMenu2->AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE2, (LPCTSTR)"충전 교정");
		//pMenu2->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE2, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg4"));
		if(bUseDischargeCal) pMenu2->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE2, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg4","IDD_CALIBRATION_DLG"));
		//@ pMenu2->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE2, (LPCTSTR)"방전 교정");
		m_Rangemenu.AppendMenu(MF_POPUP, (UINT)pMenu2->GetSafeHmenu(), (LPCTSTR)"Range &2");
		CMenu *pMenu3 = NULL;
		CMenu *pMenu4 = NULL;
		if(nCount > 2)
		{
			pMenu3 = new CMenu();
			pMenu3->CreatePopupMenu();
			pMenu3->AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE3, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg5","IDD_CALIBRATION_DLG"));
			//@ pMenu3->AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE3, (LPCTSTR)"충전 교정");
			//pMenu3->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE3, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg6"));
			if(bUseDischargeCal) pMenu3->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE3, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg6","IDD_CALIBRATION_DLG"));
			//@ pMenu3->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE3, (LPCTSTR)"방전 교정");
			m_Rangemenu.AppendMenu(MF_POPUP, (UINT)pMenu3->GetSafeHmenu(), (LPCTSTR)"Range &3");

		}
		if(nCount > 3)
		{
			pMenu4 = new CMenu();
			pMenu4->CreatePopupMenu();
			pMenu4->AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE4, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg7","IDD_CALIBRATION_DLG"));
			//@ pMenu4->AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE4, (LPCTSTR)"충전 교정");
			//pMenu4->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE4, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg8"));
			if(bUseDischargeCal) pMenu4->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE4, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg8","IDD_CALIBRATION_DLG"));
			//@ pMenu4->AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE4, (LPCTSTR)"방전 교정");
			m_Rangemenu.AppendMenu(MF_POPUP, (UINT)pMenu4->GetSafeHmenu(), (LPCTSTR)"Range &4");
		}
		if(nCount > 4)
			m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE5, (LPCTSTR)"Range &5");

		//함수 구현이 아직 안되어 있음
		m_Rangemenu.AppendMenu(MF_SEPARATOR);

		CPoint ptMouse; 
		GetCursorPos(&ptMouse); 
		m_nSelectedRange = m_Rangemenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y,  this); 

		if(pMenu2 != NULL)
		{
			delete pMenu2;
		}
		if(pMenu3 != NULL)
			delete pMenu3;
		if(pMenu4 != NULL)
			delete pMenu4;
	}
	else
	{
		//OnCaliRange1();
		m_Rangemenu.DestroyMenu();
		// Create a new menu for the application window.
		//		VERIFY(m_Rangemenu.CreatePopupMenu());
		VERIFY(m_Rangemenu.CreatePopupMenu());
		m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg9","IDD_CALIBRATION_DLG"));
		//@ m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE_CHARGE, (LPCTSTR)"충전 교정");
		//m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg10"));
		if(bUseDischargeCal) m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE, (LPCTSTR)Fun_FindMsg("OnBtnVoltage_msg10","IDD_CALIBRATION_DLG"));
		//@ m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE_DISCHARGE, (LPCTSTR)"방전 교정");
		CPoint ptMouse; 
		GetCursorPos(&ptMouse); 
		m_nSelectedRange = m_Rangemenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y,  this); 
	}


}

//전류 교정 명령
void CCalibratorDlg::OnBtnRange1()
{
	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
	if(mi == NULL)	return;

	m_nCalibrationItem =  CAL_CURRENT;

//	OnBtnConnect();

	int nCount = mi->GetCurrentRangeCount() ;
	if( nCount > 1)
	{
		m_Rangemenu.DestroyMenu();
		// Create a new menu for the application window.
		VERIFY(m_Rangemenu.CreatePopupMenu());
		m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE1, (LPCTSTR)"Range &1");
		m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE2, (LPCTSTR)"Range &2");
		
		if(nCount > 2)
			m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE3, (LPCTSTR)"Range &3");
		if(nCount > 3)
			m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE4, (LPCTSTR)"Range &4");
		if(nCount > 4)
			m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE5, (LPCTSTR)"Range &5");

		//함수 구현이 아직 안되어 있음
		m_Rangemenu.AppendMenu(MF_SEPARATOR);
		m_Rangemenu.AppendMenu(MF_STRING, ID_CALI_RANGE_ALL, (LPCTSTR)"Range &All");
		m_Rangemenu.EnableMenuItem(ID_CALI_RANGE_ALL, MF_BYCOMMAND);

		CPoint ptMouse; 
		GetCursorPos(&ptMouse); 
		m_Rangemenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y,  this); 
	}
	else
	{
		OnCaliRange1();
	}
}

//선택한 채널 교정 Data Update 실시
void CCalibratorDlg::OnBtnUpdateSel() 
{
	CCyclerModule *pMD = m_pDoc->GetCyclerMD(m_nModuleID);
	if(pMD == NULL)		return;
	CCyclerChannel *pCH;
	CCalibrationData *pChCalData;
	int nChIndex;
	CWordArray aSelChArray;

	POSITION pos = m_CalChSelList.GetFirstSelectedItemPosition();
	while(pos)
	{
		nChIndex = m_CalChSelList.GetItemData(m_CalChSelList.GetNextSelectedItem(pos));
		pCH = pMD->GetChannelInfo(nChIndex);
		if(pCH == NULL)		continue;
		pChCalData = pCH->GetCaliData();
		if(pChCalData->GetState() == CAL_DATA_MODIFY)
		{
			aSelChArray.Add((WORD)nChIndex);
		}
	}
	if(aSelChArray.GetSize() > 0)
	{
		SendUpdateCmd(m_nModuleID, &aSelChArray);
	}
	else
	{
		MessageBox(Fun_FindMsg("OnBtnUpdateSel_msg1","IDD_CALIBRATION_DLG"), Fun_FindMsg("OnBtnUpdateSel_msg2","IDD_CALIBRATION_DLG"), MB_ICONSTOP|MB_OK);  //$
		//@ MessageBox("Update 할 채널이 없습니다.", "실패", MB_ICONSTOP|MB_OK);
	}
}
//모든 채널 교정 data Update 실시
void CCalibratorDlg::OnBtnUpdateAll() 
{
	CCyclerModule *pMD = m_pDoc->GetCyclerMD(m_nModuleID);
	if(pMD == NULL)		return;
	CCyclerChannel *pCH;
	CCalibrationData *pChCalData;
	int nChIndex;
	CWordArray aSelChArray;

	int nItemCount = m_CalChSelList.GetItemCount();
	for(int nItem =0; nItem<nItemCount; nItem++)
	{
		nChIndex = m_CalChSelList.GetItemData(nItem);
		pCH = pMD->GetChannelInfo(nChIndex);
		if(pCH == NULL)		continue;
		pChCalData = pCH->GetCaliData();
		if(pChCalData->GetState() == CAL_DATA_MODIFY)
		{
			aSelChArray.Add((WORD)nChIndex);
		}
	}
	
	if(aSelChArray.GetSize() > 0)
	{
		SendUpdateCmd(m_nModuleID, &aSelChArray);
	}
	else
	{
		MessageBox(Fun_FindMsg("OnBtnUpdateAll_msg1","IDD_CALIBRATION_DLG"), Fun_FindMsg("OnBtnUpdateAll_msg2","IDD_CALIBRATION_DLG"), MB_ICONSTOP|MB_OK);
		//@ MessageBox("Update 할 채널이 없습니다.", "실패", MB_ICONSTOP|MB_OK);
	}
}

//모듈에 교정한 정보로 수정 할 것을 전송한다.
BOOL CCalibratorDlg::SendUpdateCmd(int nModuleID, CWordArray *pSelChArray) 
{
	if(pSelChArray == FALSE)	return FALSE;
	
	CWaitCursor cursor;	

	CString strMsg;
	CCyclerModule *pMD;
	CCyclerChannel *pCH;
	CCalibrationData *pChCalData;

	pMD = m_pDoc->GetCyclerMD(nModuleID);
	if(pMD == NULL)		return	FALSE;
	int nRtn;
	//현재 선택된 채널이 새롭게 교정되었을 경우
	if((nRtn = pMD->SendCommand(SFT_CMD_CALI_UPDATE, pSelChArray)) != SFT_ACK)
	//if((nRtn = pMD->SendCommand(SFT_CMD_CALI_UPDATE, 1,)) != SFT_ACK)
	{
		strMsg.Format(Fun_FindMsg("SendUpdateCmd_msg1","IDD_CALIBRATION_DLG"),
			//@ strMsg.Format("%s 교정 Data Update 지령 전송에 실패... (%s)",
			m_pDoc->GetModuleName(m_nModuleID), pMD->GetCmdErrorMsg(nRtn));
		m_pDoc->WriteSysLog(strMsg);
	}
	else
	{
		int nChIndex;
		for(int a =0; a<pSelChArray->GetSize(); a++)
		{
			nChIndex =  pSelChArray->GetAt(a);
			strMsg.Format(Fun_FindMsg("SendUpdateCmd_msg2","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1);
			//@ strMsg.Format("%s Ch %d 교정 Data Update 지령 전송 성공", m_pDoc->GetModuleName(nModuleID), nChIndex+1);
			m_pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);	
			
			TCHAR szBuff[_MAX_PATH];
			::GetModuleFileName(AfxGetApp()->m_hInstance, szBuff, _MAX_PATH);
			CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));
			CString strFieName;
			strFieName.Format("%s\\Calibration\\M%02dC%02d.cal", path, nModuleID, nChIndex+1);
			//Host의 교정 Data 파일도 Update 시킴 
			pCH = pMD->GetChannelInfo(nChIndex);
			if(pCH == NULL)		continue;
			pChCalData = pCH->GetCaliData();
			pChCalData->SaveDataToFile(strFieName);
			
			UpdateMDStateList(nChIndex);

		}
		return TRUE;		
	}
	return FALSE;
}

void CCalibratorDlg::OnCommConnected()
{
	GetDlgItem(IDC_BTN_VOLTAGE)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_RANGE1)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_RANGE2)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_RANGE3)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_UPDATE_SEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE_ALL)->EnableWindow(FALSE);
	GetDlgItem(IDC_ALL_ITEM_BUTTON)->EnableWindow(TRUE);

	

	m_bCalStatus = true;

	CString strMsg;
	strMsg.Format("Module %02d :: HP34401 Connected", m_nModuleID);
//	m_pView->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
}

void CCalibratorDlg::OnCommDisconnected()
{
	GetDlgItem(IDC_BTN_VOLTAGE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_RANGE1)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_RANGE2)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_RANGE3)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE_SEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE_ALL)->EnableWindow(FALSE);
	GetDlgItem(IDC_ALL_ITEM_BUTTON)->EnableWindow(FALSE);

	m_bCalStatus = false;
}

//수정된 Data가 있어 Update가능함 
void CCalibratorDlg::OnCalDataModified()
{
	GetDlgItem(IDC_BTN_UPDATE_SEL)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_UPDATE_ALL)->EnableWindow(TRUE);
}

//교정 확인 data를 표시한다
//단순 확인 명령에 대한 결과 수신 
void CCalibratorDlg::SetReceivedCheckData(int nModuleID, int nChIndex)
{
	CListCtrl* pWndResult = &m_wndCheck;
	CCaliPoint *pCalPoint = m_pDoc->GetCalPoint();


	CString strTemp;
	LV_ITEM aItem;
	aItem.mask = LVIF_IMAGE|LVIF_TEXT;
	int nItem = 0;
	
	CCyclerModule *pMD;
	CCyclerChannel *pCH;
	CCalibrationData *pChCalData;
	WORD nI;
	double dADData, dMeterData;

	pMD = m_pDoc->GetCyclerMD(nModuleID);
	if(pMD == NULL)		return;
	pCH = pMD->GetChannelInfo(nChIndex);
	if(pCH == NULL)		return;
	pChCalData = pCH->GetCaliData();

	pWndResult->DeleteAllItems();

	strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg1","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1);
	//@ strTemp.Format("%s Ch%d 교정 Data", m_pDoc->GetModuleName(nModuleID), nChIndex+1);
	SetDlgItemText(IDC_CH_CAL_DATA_STATIC, strTemp);

	int nSelDataType = m_ctrlSelDataTypeCombo.GetCurSel();
	int nSelItemType = 	m_wndTotalPoint.GetCurSel();
	BOOL bUseDischargeCal = AfxGetApp()->GetProfileInt(REG_CAL_SECTION, "UseDischargeCal", 1); //ksj 20170725 : 방전 교정 사용 여부

	if(nSelItemType != 2)	//전압만 표기
	{
		//전압 교정 확인 Point 표시 
		for(int nI = 0; nI<SFT_MAX_VOLTAGE_RANGE; nI++)
		{	
			if(nSelDataType != 2)	//교정전 data만 
			{
				for(WORD wSel = 0 ; wSel < 2; wSel++)
				{
					if(wSel == 1 && bUseDischargeCal == 0) continue; //ksj 20170725 : 방전 교정은 방전 교정 사용 옵션이 켜져 있어야 출력한다.

					for(WORD pos = 0; pos < pChCalData->m_PointData.GetVtgSetPointCount(nI) && pos < MAX_CALIB_SET_POINT; pos++)
					{
						strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg2","IDD_CALIBRATION_DLG"), nChIndex+1);
						//@ strTemp.Format("CH%02d 전압", nChIndex+1);
						aItem.iSubItem = 0;
						aItem.iImage = 0;
						aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
						aItem.iItem = nItem++;
						pWndResult->InsertItem(&aItem);
						
						if(wSel == 0) strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg3","IDD_CALIBRATION_DLG"), nI+1);
						//@ if(wSel == 0) strTemp.Format("Range-%d 충전교정", nI+1);
						else strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg4","IDD_CALIBRATION_DLG"), nI+1);
						//@ else strTemp.Format("Range-%d 방전교정", nI+1);
						pWndResult->SetItemText(aItem.iItem, 1, strTemp.Right(16));
						
						strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetVSetPoint(nI, pos));
						pWndResult->SetItemText(aItem.iItem, 2, strTemp.Right(16));
						
						pChCalData->m_ChCaliData.GetVCalData(nI, wSel, pos, dADData, dMeterData);
						strTemp.Format("%.3f", (float)dMeterData);
						pWndResult->SetItemText(aItem.iItem, 3, strTemp.Right(16));
						
						strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetVSetPoint(nI, pos)-dMeterData);
						pWndResult->SetItemText(aItem.iItem, 4, strTemp.Right(16));
						
						strTemp.Format("%.3f", (float)dADData);
						pWndResult->SetItemText(aItem.iItem, 5, strTemp.Right(16));
						
						strTemp.Format("%.3f", (float)(dMeterData-dADData));
						pWndResult->SetItemText(aItem.iItem, 6, strTemp.Right(16));
					}
				}
			}
			
			if(nSelDataType != 1)	//교정후 data만 
			{
				for(WORD wSEL = 0; wSEL < 2; wSEL++)
				{
					if(wSEL == 1 && bUseDischargeCal == 0) continue; //ksj 20170725 : 방전 교정은 방전 교정 사용 옵션이 켜져 있어야 출력한다.

					for(WORD pos = 0; pos < pChCalData->m_PointData.GetVtgCheckPointCount(nI) && pos < MAX_CALIB_CHECK_POINT; pos++)
					{
						strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg5","IDD_CALIBRATION_DLG"), nChIndex+1);
						//@ strTemp.Format("CH%02d 전압", nChIndex+1);
						aItem.iSubItem = 0;
						aItem.iImage = 2;
						aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
						aItem.iItem = nItem++;
						pWndResult->InsertItem(&aItem);
						
						if(wSEL == 0 ) strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg6","IDD_CALIBRATION_DLG"), nI+1);
						//@ if(wSEL == 0 ) strTemp.Format("Range-%d 충전검사", nI+1);
						else strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg7","IDD_CALIBRATION_DLG"), nI+1);
						//@ else strTemp.Format("Range-%d 방전검사", nI+1);
						
						pWndResult->SetItemText(aItem.iItem, 1, strTemp.Right(16));
						
						strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetVCheckPoint(nI, pos));
						pWndResult->SetItemText(aItem.iItem, 2, strTemp.Right(16));
						
						pChCalData->m_ChCaliData.GetVCheckData(nI, wSEL, pos, dADData, dMeterData);
						strTemp.Format("%.3f", (float)dMeterData);
						pWndResult->SetItemText(aItem.iItem, 3, strTemp.Right(16));
						
						strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetVCheckPoint(nI, pos)-dMeterData);
						pWndResult->SetItemText(aItem.iItem, 4, strTemp.Right(16));
						//					if( fabs(pChCalData->m_PointData.GetVCheckPoint(nI, pos)-dMeterData) >  m_fVtgDAErrorSet)
						if( fabs(pChCalData->m_PointData.GetVCheckPoint(nI, pos)-dMeterData) > pCalPoint->m_dVDAAccuracy[nI])					
						{
							aItem.iSubItem = 4;
							aItem.iImage = 4;
							pWndResult->SetItem(&aItem);
						}
						
						strTemp.Format("%.3f", float(dADData));
						pWndResult->SetItemText(aItem.iItem, 5, strTemp.Right(16));
						
						strTemp.Format("%.3f", float(dMeterData-dADData));
						pWndResult->SetItemText(aItem.iItem, 6, strTemp.Right(16));
						
						//					if(fabs(dMeterData-dADData) > m_fVtgADErrorSet)
						if(fabs(dMeterData-dADData) > pCalPoint->m_dVADAccuracy[nI])
						{
							aItem.iSubItem = 6;
							aItem.iImage = 4;
							pWndResult->SetItem(&aItem);
						}
					}

				}
			}
		}
	}

	if(nSelItemType != 1)	//전류만 표기
	{
		//전류 교정 확인 Point 표시 
		for(int nI = 0; nI<SFT_MAX_CURRENT_RANGE; nI++)
		{
			if(nSelDataType != 2)	//교정전 data만 
			{
				for(WORD pos = 0; pos < pChCalData->m_PointData.GetCrtSetPointCount(nI) && pos <MAX_CALIB_SET_POINT; pos++)
				{
					strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg8","IDD_CALIBRATION_DLG"), nChIndex+1);
					//@ strTemp.Format("CH%02d 전류", nChIndex+1);
					aItem.iSubItem = 0;
					aItem.iImage = 1;
					aItem.iItem = nItem++;
					aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
					pWndResult->InsertItem(&aItem);

					strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg9","IDD_CALIBRATION_DLG"), nI+1);
					//@ strTemp.Format("Range-%d 교정", nI+1);
					pWndResult->SetItemText(aItem.iItem, 1, strTemp.Right(16));

					strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetISetPoint(nI, pos));
					pWndResult->SetItemText(aItem.iItem, 2, strTemp.Right(16));
					
					pChCalData->m_ChCaliData.GetICalData(nI, pos, dADData, dMeterData);
					
					strTemp.Format("%.3f", (float)dMeterData);
					pWndResult->SetItemText(aItem.iItem, 3, strTemp.Right(16));

					strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetISetPoint(nI, pos) - dMeterData);
					pWndResult->SetItemText(aItem.iItem, 4, strTemp.Right(16));
					
					
					strTemp.Format("%.3f", (float)dADData);
					pWndResult->SetItemText(aItem.iItem, 5, strTemp.Right(16));
					
					strTemp.Format("%.3f", (float)(dMeterData - dADData));
					pWndResult->SetItemText(aItem.iItem, 6, strTemp.Right(16));
				}
			}

			if(nSelDataType != 1)	//교정후 data만 
			{
				for(WORD pos = 0; pos < pChCalData->m_PointData.GetCrtCheckPointCount(nI) && pos < MAX_CALIB_CHECK_POINT; pos++)
				{
					strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg10","IDD_CALIBRATION_DLG"), nChIndex+1);
					//@ strTemp.Format("CH%02d 전류", nChIndex+1);
					aItem.iSubItem = 0;
					aItem.iImage = 3;
					aItem.iItem = nItem++;
					aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
					pWndResult->InsertItem(&aItem);

					strTemp.Format(Fun_FindMsg("SetReceivedCheckData_msg11","IDD_CALIBRATION_DLG"), nI+1);
					//@ strTemp.Format("Range-%d 검사", nI+1);
					pWndResult->SetItemText(aItem.iItem, 1, strTemp.Right(16));

					strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetICheckPoint(nI, pos));
					pWndResult->SetItemText(aItem.iItem, 2, strTemp.Right(16));
					
					pChCalData->m_ChCaliData.GetICheckData(nI, pos, dADData, dMeterData);
					strTemp.Format("%.3f", (float)dMeterData);
					pWndResult->SetItemText(aItem.iItem, 3, strTemp.Right(16));
					
					strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetICheckPoint(nI, pos) - dMeterData);
					pWndResult->SetItemText(aItem.iItem, 4, strTemp.Right(16));
//					if( fabs(pChCalData->m_PointData.GetICheckPoint(nI, pos)-dMeterData) > m_fCrtDAErrorSet)
					if( fabs(pChCalData->m_PointData.GetICheckPoint(nI, pos)-dMeterData) > pCalPoint->m_dIDAAccuracy[nI])					
					{
						aItem.iSubItem = 4;
						aItem.iImage = 4;
						pWndResult->SetItem(&aItem);
					}
					
					strTemp.Format("%.3f", float(dADData));
					pWndResult->SetItemText(aItem.iItem, 5, strTemp.Right(16));

					strTemp.Format("%.3f", float(dMeterData - dADData));
					pWndResult->SetItemText(aItem.iItem, 6, strTemp);
					
//					if(fabs(dMeterData-dADData) > m_fCrtADErrorSet)
					if(fabs(dMeterData-dADData) > pCalPoint->m_dIADAccuracy[nI])
					{
						aItem.iSubItem = 6;
						aItem.iImage = 4;
						pWndResult->SetItem(&aItem);
					}
				}
			}
		}
	}
}

//모듈에서 교정 완료 Message가 수신 되었을 때...
//이 Message가 수신되지 않을 경우 예외 처리가 필요함
void CCalibratorDlg::SetReceiveResultData(LPSFT_CALI_END_DATA pReceivedCalData)
{
	ASSERT(pReceivedCalData);

	CString strMsg;

	//1 Point Calibration average time 
	if(m_aBatchCaliData.byTotalPoint)
	{
		m_nAVGOnePointCalTime = m_nTimeOutCount/m_aBatchCaliData.byTotalPoint;		
	}
	
	//Data 확인 
//	CCaliPoint *pCalPoint = m_pDoc->GetCalPoint();
//	BYTE byRange = pReceivedCalData->byRange;

	BOOL	bValiDate = TRUE;
	strMsg.Format(Fun_FindMsg("SetReceivedResultData_msg1","IDD_CALIBRATION_DLG"), 
		//@ strMsg.Format("%s 교정 완료(Ch %d/Type %d/Range %d/%d/%d)", 
					m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID),
					pReceivedCalData->byChannelID,			//One Base
					pReceivedCalData->nType,
					pReceivedCalData->byRange,
					pReceivedCalData->byValidCalPointNum,
					pReceivedCalData->byValidCheckPointNum
				);
	m_pDoc->WriteSysLog(strMsg);

	if(pReceivedCalData->byChannelID != m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)+1)
	{
		bValiDate = FALSE;
	}

	//Data 수량 확인
	//Range 확인 
	if( pReceivedCalData->byRange != m_aBatchCaliData.aCaliSet.nRange )		bValiDate = FALSE;
		//교정 포인터 수 확인
	if( pReceivedCalData->byValidCalPointNum  != m_aBatchCaliData.aCaliSet.pointData.byValidPointNum)	bValiDate = FALSE;
		//확인 포인터 수 확인 
	if( pReceivedCalData->byValidCheckPointNum != m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum)	bValiDate = FALSE;
	//////////////////////////////////////////////////////////////////////////

	if(pReceivedCalData->nSelType != m_nChargeDisChargeMode) bValiDate = FALSE;

	if(bValiDate == FALSE)
	{
		strMsg.Format(Fun_FindMsg("SetReceivedResultData_msg2","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), pReceivedCalData->byChannelID);
		//@ strMsg.Format("%s Ch %d 교정 완료 Data Error.", m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), pReceivedCalData->byChannelID);
		MessageBox(strMsg,  Fun_FindMsg("SetReceivedResultData_msg3","IDD_CALIBRATION_DLG"), MB_ICONSTOP|MB_OK);
		//@ MessageBox(strMsg,  "실패", MB_ICONSTOP|MB_OK);
	}
	else
	{
		CString strItem;
		if(pReceivedCalData->nType == CAL_VOLTAGE)
		{
			strItem = Fun_FindMsg("SetReceivedResultData_msg4","IDD_CALIBRATION_DLG");
			//@ strItem = "전압";
		}
		else if(pReceivedCalData->nType == CAL_CURRENT)
		{
			strItem = Fun_FindMsg("SetReceivedResultData_msg5","IDD_CALIBRATION_DLG");
			//@ strItem = "전류";
		}

		//Update Channel member variable
		UpdateChCaliData(pReceivedCalData);

		//채널 상태를 Update한다.
		UpdateMDStateList(pReceivedCalData->byChannelID-1);
		
		//결과 리스트에 방금 완료된 채널을 표시 한다.
		SetReceivedCheckData(m_aBatchCaliData.nModuleID, pReceivedCalData->byChannelID-1);		//byChannelID One Base
		strMsg.Format(	Fun_FindMsg("SetReceivedResultData_msg6","IDD_CALIBRATION_DLG"), 
			//@ strMsg.Format(	"%s Ch %d %s Range%d 교정 완료.(%dsec)", 
						m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID),
						pReceivedCalData->byChannelID, 
						strItem,
						pReceivedCalData->byRange+1,
						int(m_nTimeOutCount*0.2f)
					);
	}	
	SetDlgItemText(IDC_EDIT_STATUS, strMsg);
	
	EndWaitState();

	//Stop Button이 눌린 상태이면 
	if(m_bStopFlag != TRUE)
	{
		//다음 전송할 채널이나 Range가 있으면 전송한다.
		//바로 다음채널 시작 명령을 보낼시 모듈이 완전하게 상태전이가 안된 경우가 있다.(교정중->Idle or standby)
		//다음 명령 전송에 대한 시간 지연이 필요하여 Timer를 발생시키고 일정시간 지연후에 보냄 
		SetTimer(2001, 1500, NULL);
//		SendNextCalInfo();
	}


	//다음 채널 교정 명령을 전송함 
	//현재 채널이 교정 가능한 상태가 아니면 다음 채널에 전송한다.

/*	while (m_aBatchCaliData.byChSelIndex+1 < m_aCaliSelChArray.GetSize())
	{
		m_aBatchCaliData.byChSelIndex++;		//다음 선택 채널로 이동 
		int nChIndex = m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex);		//다음 채널의 Index를 구함 
		if(SendCalStart(nChIndex))			//다음 채널에 시작 명령을 전송 
		{
			break;							//전송 명령에 성공하였으므로 다음채널에는 보내지 않음
		}
		else
		{

		}
		//다음 채널에 전송 실패 할 경우는 그 다음 채널에 전송 
	}
*/
}

void CCalibratorDlg::BeginWaitState()
{
	GetDlgItem(IDC_BTN_VOLTAGE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_RANGE1)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE_SEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE_ALL)->EnableWindow(FALSE);
	GetDlgItem(IDC_MODULE_SEL_COMBO)->EnableWindow(FALSE);
	GetDlgItem(IDC_ALL_ITEM_BUTTON)->EnableWindow(FALSE);
	
	GetDlgItem(IDC_STOP_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDOK)->EnableWindow(FALSE);

//	BeginWaitCursor();

//	m_nTimerArrow = 0;
//	m_bDirection = true;
	m_ctrlProgress.SetPos(0);
	m_nTimeOutCount = 0;
	m_dwElapsedTime = m_aBatchCaliData.byTotalPoint*m_nAVGOnePointCalTime;	//전체 예상되는 시간(각 Point 수 * 예상 시간) 
	
	SetTimer(1000, CAL_TIMER_INTERVAL, NULL);
}

void CCalibratorDlg::EndWaitState()
{
	KillTimer(1000);
	
	GetDlgItem(IDC_BTN_VOLTAGE)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_RANGE1)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_UPDATE_SEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE_ALL)->EnableWindow(FALSE);
	GetDlgItem(IDC_MODULE_SEL_COMBO)->EnableWindow(TRUE);
	GetDlgItem(IDC_ALL_ITEM_BUTTON)->EnableWindow(TRUE);

	GetDlgItem(IDC_STOP_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDOK)->EnableWindow(TRUE);

//	m_bCalStatus = true;

	//수정된 Data가 있어 Update가능 
	OnCalDataModified();

//	EndWaitCursor();
//	CString strTime;

//	DWORD dwTempTime = timeGetTime();
//	TRACE("ElapsedTime : %d\n", (dwTempTime - m_dwElapsedTime));
	
//	int nSec = (dwTempTime - m_dwElapsedTime)/1000;
//	int nMin = nSec / 60;
//	nSec = nSec - (nMin*60);
//	strTime.Format("Elapsed Time ▷ %02d:%02d", nMin, nSec);

//	SetDlgItemText(IDC_EDIT_STATUS, strTime);
	m_ctrlProgress.SetPos(100);
}


//모든 채널에 대해서 교정값을 표시한다.
void CCalibratorDlg::OnSelchangeComboDpMode() 
{
	POSITION pos = m_CalChSelList.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_CalChSelList.GetNextSelectedItem(pos);
		int nChIndex = m_CalChSelList.GetItemData(nItem);
		SetReceivedCheckData(m_nModuleID, nChIndex);	
		
		CString strTemp;
		strTemp.Format(Fun_FindMsg("OnSelchangeComboDpMode_msg","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_nModuleID), nChIndex+1);
		//@ strTemp.Format("%s Ch%d 교정 선택", m_pDoc->GetModuleName(m_nModuleID), nChIndex+1);
		SetDlgItemText(IDC_CAL_SEL_CH_STATIC, strTemp);
	}
}
/*
void CCalibratorDlg::OnRadioModeCalib() 
{
	UpdateData();
//	OnSelchangeComboDpMode();
}

void CCalibratorDlg::OnRadioModeCheck() 
{
	UpdateData();
//	OnSelchangeComboDpMode();
}
*/
void CCalibratorDlg::OnTimer(UINT_PTR  nIDEvent) 
{
	if(nIDEvent == 1000)
	{
		//Time Out이 발생함 
		if(m_nTimeOutCount > m_dwElapsedTime+ CAL_TIME_OUT_COUNT)		//예상 시각 보다 30초 경과하면 모듈쪽에서 이상이 있어 응답이 없다고 판다.
		{
			EndWaitState();
			
			CString strMsg;
			strMsg.Format(Fun_FindMsg("OnTimer_msg1","IDD_CALIBRATION_DLG"), 
				//@ strMsg.Format("%s Ch %d 교정 완료 응답 없음", 
				m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID),
				m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)+1
			);

			SetDlgItemText(IDC_EDIT_STATUS, strMsg);
			MessageBox(strMsg,  Fun_FindMsg("OnTimer_msg2","IDD_CALIBRATION_DLG"), MB_ICONSTOP|MB_OK);
			//@ MessageBox(strMsg,  "실패", MB_ICONSTOP|MB_OK);
		}

		int pos = int((float)m_nTimeOutCount++/(float)m_dwElapsedTime * 100.0f);
		m_ctrlProgress.SetPos(pos);
	}
	else if(nIDEvent == 2000)
	{
		//ㅌㄴㅌㅌㅌㅌㅌㄴㅌㄴㅇㅌㄴㅌㄴㅌㅌ깜빡거림 제거를 위해 상태가 변했을시만 Redraw();
		CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_nModuleID);
		if(pMD)
		{
			LVITEM lvItem;
			ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.mask = LVIF_IMAGE;
			CCyclerChannel *pCh ;
			for(int i= 0; i< pMD->GetTotalChannel(); i++)
			{
				pCh = m_pDoc->GetChannelInfo(m_nModuleID, i);
				if(pCh)
				{
					lvItem.iItem = i;
					lvItem.iImage = m_pDoc->GetStateImageIndex(pCh->GetState(), pCh->GetStepType());
					m_CalChSelList.SetItem(&lvItem);
				}
			}
		}		
	}
	else if(nIDEvent == 2001)	//다음 교정 조건 전송 Envent
	{
		//다음 채널 전송 성공 
		KillTimer(2001);
		SendNextCalInfo();
	}

//	TRACE("Out Count %d/Total %d/ POS %d\n", m_nTimeOutCount, m_dwElapsedTime, pos);


	CDialog::OnTimer(nIDEvent);
}

void CCalibratorDlg::OnRclickListTotal(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int nSelLanguage, nIDSel;//&&
	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	
	switch(nSelLanguage)
	{
	case 1:
		{
			nIDSel = IDR_POP_MENU;
			break;
		}
	case 2:
		{
			nIDSel = IDR_POP_MENU_ENG;
			break;
		}
	}

	CMenu menu; 
	//menu.LoadMenu(IDR_POP_MENU); 
	menu.LoadMenu(nIDSel);
	CMenu* popmenu = menu.GetSubMenu(1); 
	CPoint ptMouse; 
	GetCursorPos(&ptMouse); 
	popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
		ptMouse.x, ptMouse.y, this); 
	*pResult = 0;
}

/*
void CCalibratorDlg::OnUpdateCaliRangeAll(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(m_bCalStatus == FALSE)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}

	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
	if(mi == NULL)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}

	if(m_nCalibrationItem ==  CAL_CURRENT)
	{
		pCmdUI->Enable(mi->GetCurrentRangeCount() > CAL_RANGE1);
	}
	else if(m_nCalibrationItem ==  CAL_VOLTAGE)
	{
		pCmdUI->Enable(mi->GetVoltageRangeCount() > CAL_RANGE1);
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}
}

void CCalibratorDlg::OnUpdateCaliRange1(CCmdUI* pCmdUI) 
{
	if(m_bCalStatus == FALSE)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}

	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
	if(mi == NULL)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}


	if(m_nCalibrationItem ==  CAL_CURRENT)
	{
		pCmdUI->Enable(mi->GetCurrentRangeCount() > CAL_RANGE1);
	}
	else if(m_nCalibrationItem ==  CAL_VOLTAGE)
	{
		pCmdUI->Enable(mi->GetVoltageRangeCount() > CAL_RANGE1);
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}
}

void CCalibratorDlg::OnUpdateCaliRange2(CCmdUI* pCmdUI) 
{

	if(m_bCalStatus == FALSE)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}

	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
	if(mi == NULL)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}

	if(m_nCalibrationItem ==  CAL_CURRENT)
	{
		pCmdUI->Enable(mi->GetCurrentRangeCount() > CAL_RANGE2);
	}
	else if(m_nCalibrationItem ==  CAL_VOLTAGE)
	{
		pCmdUI->Enable(mi->GetVoltageRangeCount() > CAL_RANGE2);
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}
}

void CCalibratorDlg::OnUpdateCaliRange3(CCmdUI* pCmdUI) 
{
	if(m_bCalStatus == FALSE)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}
	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
	if(mi == NULL)
	{
		pCmdUI->Enable(FALSE);	
		return;
	}

	if(m_nCalibrationItem ==  CAL_CURRENT)
	{
		pCmdUI->Enable(mi->GetCurrentRangeCount() > CAL_RANGE3);
	}
	else if(m_nCalibrationItem ==  CAL_VOLTAGE)
	{
		pCmdUI->Enable(mi->GetVoltageRangeCount() > CAL_RANGE3);
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}
}
*/
// void CCalibratorDlg::ManualCalibration(BYTE /*byRange*/, int /*nChannelIndex*/)
// {
// 	DWORD dwChannel[2];	
// 	ZeroMemory(dwChannel, sizeof(dwChannel));
// }

void CCalibratorDlg::OnCaliRange1() 
{
	m_nCalMode = CAL_SINGLE_RANGE;
	WORD nRange = CAL_RANGE1;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange1_msg1","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		if(m_nChargeDisChargeMode == 0) strMsg.Format(Fun_FindMsg("OnCaliRange1_msg2","IDD_CALIBRATION_DLG"), nRange+1);
		//@ if(m_nChargeDisChargeMode == 0) strMsg.Format("선택된 채널에 대해 전압 Range %d 충전 교정을 실시하시겠습니까?", nRange+1);
		else strMsg.Format(Fun_FindMsg("OnCaliRange1_msg3","IDD_CALIBRATION_DLG"), nRange+1);
		//@ else strMsg.Format("선택된 채널에 대해 전압 Range %d 방전 교정을 실시하시겠습니까?", nRange+1);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange1_msg3","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}

	if(MessageBox(strMsg, Fun_FindMsg("OnCaliRange1_msg4","IDD_CALIBRATION_DLG"), MB_YESNO|MB_ICONQUESTION) != IDYES)
		//@ if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;


	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nModuleID, m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox(Fun_FindMsg("OnCaliRange1_msg5","IDD_CALIBRATION_DLG"), "Error", MB_ICONSTOP|MB_OK);
		//@ MessageBox("교정 정보가 정확하지 않아 시작할 수 없습니다.(교정 가능한 채널을 선택하고 교정 포인트 설정를 확인 하십시요)", "Error", MB_ICONSTOP|MB_OK);
	}

/*
	//다채널 선택하여 진행하고 Stop 명령을 지원하기 위해 Timer를 이용하여 한채널씩 전송한다.
	if(UpdateBatchData(m_nCalibrationItem, CAL_RANGE1))
	{
		//가장 처음 선택된 채널에 시작 명령을 전송하고
		//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
		if(SendCalStart(m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)))
		{
			//첫번째 채널이 전송 가능하지 않을 경우			
		}
	}
	else
	{
		MessageBox("교정 채널이 선택되지 않았습니다.", "채널선택");
	}
*/
}

void CCalibratorDlg::OnCaliRange2() 
{
	m_nCalMode = CAL_SINGLE_RANGE;
	WORD nRange = CAL_RANGE2;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange2_msg1","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		if(m_nChargeDisChargeMode == 0) strMsg.Format(Fun_FindMsg("OnCaliRange2_msg2","IDD_CALIBRATION_DLG"), nRange+1);
		//@ if(m_nChargeDisChargeMode == 0) strMsg.Format("선택된 채널에 대해 전압 Range %d 충전 교정을 실시하시겠습니까?", nRange+1);
		else strMsg.Format(Fun_FindMsg("OnCaliRange1_msg3","IDD_CALIBRATION_DLG"), nRange+1);
		//@ else strMsg.Format("선택된 채널에 대해 전압 Range %d 방전 교정을 실시하시겠습니까?", nRange+1);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange2_msg4","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}

	if(MessageBox(strMsg, Fun_FindMsg("OnCaliRange2_msg5","IDD_CALIBRATION_DLG"), MB_YESNO|MB_ICONQUESTION) != IDYES)
		//@ if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;


	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nModuleID, m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox(Fun_FindMsg("OnCaliRange2_msg6","IDD_CALIBRATION_DLG"), "Error", MB_ICONSTOP|MB_OK);
		//@ MessageBox("교정 정보가 정확하지 않아 시작할 수 없습니다.", "Error", MB_ICONSTOP|MB_OK);
	}

/*	m_nCalMode = CAL_SINGLE_RANGE;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", CAL_RANGE2+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		strMsg.Format("선택된 채널에 대해 전압 Range %d 교정을 실시하시겠습니까?", CAL_RANGE2+1);
	}
	else
	{
		strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", CAL_RANGE2+1);
	}

	if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;

	//다채널 선택하여 진행하고 Stop 명령을 지원하기 위해 Timer를 이용하여 한채널씩 전송한다.
	if(UpdateBatchData(m_nCalibrationItem, CAL_RANGE2))
	{
		//가장 처음 선택된 채널에 시작 명령을 전송하고
		//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
		if(SendCalStart(m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)))
		{
			//첫번째 채널이 전송 가능하지 않을 경우			
		}
	}
	else
	{
		MessageBox("교정 채널이 선택되지 않았습니다.", "채널선택");
	}
*/
}

void CCalibratorDlg::OnCaliRange3() 
{

	m_nCalMode = CAL_SINGLE_RANGE;
	WORD nRange = CAL_RANGE3;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange3_msg1","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		if(m_nChargeDisChargeMode == 0) strMsg.Format(Fun_FindMsg("OnCaliRange3_msg2","IDD_CALIBRATION_DLG"), nRange+1);
		//@ if(m_nChargeDisChargeMode == 0) strMsg.Format("선택된 채널에 대해 전압 Range %d 충전 교정을 실시하시겠습니까?", nRange+1);
		else strMsg.Format(Fun_FindMsg("OnCaliRange3_msg3","IDD_CALIBRATION_DLG"), nRange+1);
		//@ else strMsg.Format("선택된 채널에 대해 전압 Range %d 방전 교정을 실시하시겠습니까?", nRange+1);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange3_msg4","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}

	if(MessageBox(strMsg, Fun_FindMsg("OnCaliRange3_msg5","IDD_CALIBRATION_DLG"), MB_YESNO|MB_ICONQUESTION) != IDYES)
		//@ if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;


	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nModuleID, m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox(Fun_FindMsg("OnCaliRange3_msg6","IDD_CALIBRATION_DLG"), "Error", MB_ICONSTOP|MB_OK);
		//@ MessageBox("교정 정보가 정확하지 않아 시작할 수 없습니다.", "Error", MB_ICONSTOP|MB_OK);
	}

/*	m_nCalMode = CAL_SINGLE_RANGE;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", CAL_RANGE3+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		strMsg.Format("선택된 채널에 대해 전압 Range %d 교정을 실시하시겠습니까?", CAL_RANGE3+1);
	}
	else
	{
		strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", CAL_RANGE3+1);
	}

	if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;
	
	//다채널 선택하여 진행하고 Stop 명령을 지원하기 위해 Timer를 이용하여 한채널씩 전송한다.
	if(UpdateBatchData(m_nCalibrationItem, CAL_RANGE3))
	{
		//가장 처음 선택된 채널에 시작 명령을 전송하고
		//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
		if(SendCalStart(m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)))
		{
			//첫번째 채널이 전송 가능하지 않을 경우			
		}
	}
	else
	{
		MessageBox("교정 채널이 선택되지 않았습니다.", "채널선택");
	}
	*/

}

void CCalibratorDlg::OnCaliRange4() 
{
	// TODO: Add your command handler code here
/*	m_nCalMode = CAL_SINGLE_RANGE;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", CAL_RANGE4+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		strMsg.Format("선택된 채널에 대해 전압 Range %d 교정을 실시하시겠습니까?", CAL_RANGE4+1);
	}
	else
	{
		strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", CAL_RANGE4+1);
	}

	if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;
	
	//다채널 선택하여 진행하고 Stop 명령을 지원하기 위해 Timer를 이용하여 한채널씩 전송한다.
	if(UpdateBatchData(m_nCalibrationItem, CAL_RANGE4))
	{
		//가장 처음 선택된 채널에 시작 명령을 전송하고
		//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
		if(SendCalStart(m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)))
		{
			//첫번째 채널이 전송 가능하지 않을 경우			
		}
	}
	else
	{
		MessageBox("교정 채널이 선택되지 않았습니다.", "채널선택");
	}*/
	m_nCalMode = CAL_SINGLE_RANGE;
	WORD nRange = CAL_RANGE4;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange4_msg1","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		if(m_nChargeDisChargeMode == 0) strMsg.Format(Fun_FindMsg("OnCaliRange4_msg2","IDD_CALIBRATION_DLG"), nRange+1);
		//@ if(m_nChargeDisChargeMode == 0) strMsg.Format("선택된 채널에 대해 전압 Range %d 충전 교정을 실시하시겠습니까?", nRange+1);
		else strMsg.Format(Fun_FindMsg("OnCaliRange4_msg3","IDD_CALIBRATION_DLG"), nRange+1);
		//@ else strMsg.Format("선택된 채널에 대해 전압 Range %d 방전 교정을 실시하시겠습니까?", nRange+1);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange4_msg4","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}

	if(MessageBox(strMsg, Fun_FindMsg("OnCaliRange4_msg5","IDD_CALIBRATION_DLG"), MB_YESNO|MB_ICONQUESTION) != IDYES)
		//@ if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;


	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nModuleID, m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox(Fun_FindMsg("OnCaliRange4_msg6","IDD_CALIBRATION_DLG"), "Error", MB_ICONSTOP|MB_OK);
		//@ MessageBox("교정 정보가 정확하지 않아 시작할 수 없습니다.", "Error", MB_ICONSTOP|MB_OK);
	}	
}

void CCalibratorDlg::OnCaliRange5() 
{
	// TODO: Add your command handler code here
/*	m_nCalMode = CAL_SINGLE_RANGE;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", CAL_RANGE5+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		strMsg.Format("선택된 채널에 대해 전압 Range %d 교정을 실시하시겠습니까?", CAL_RANGE5+1);
	}
	else
	{
		strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", CAL_RANGE5+1);
	}

	if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;
	
	//다채널 선택하여 진행하고 Stop 명령을 지원하기 위해 Timer를 이용하여 한채널씩 전송한다.
	if(UpdateBatchData(m_nCalibrationItem, CAL_RANGE5))
	{
		//가장 처음 선택된 채널에 시작 명령을 전송하고
		//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
		if(SendCalStart(m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)))
		{
			//첫번째 채널이 전송 가능하지 않을 경우			
		}
	}
	else
	{
		MessageBox("교정 채널이 선택되지 않았습니다.", "채널선택");
	}
*/
	m_nCalMode = CAL_SINGLE_RANGE;
	WORD nRange = CAL_RANGE5;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange5_msg1","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange5_msg2","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전압 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnCaliRange5_msg3","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}

	if(MessageBox(strMsg, Fun_FindMsg("OnCaliRange5_msg4","IDD_CALIBRATION_DLG"), MB_YESNO|MB_ICONQUESTION) != IDYES)
		//@ if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;


	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nModuleID, m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox(Fun_FindMsg("OnCaliRange5_msg5","IDD_CALIBRATION_DLG"), "Error", MB_ICONSTOP|MB_OK);
		//@ MessageBox("교정 정보가 정확하지 않아 시작할 수 없습니다.", "Error", MB_ICONSTOP|MB_OK);
	}	
}

void CCalibratorDlg::OnCaliRangeAll() 
{
	m_nCalMode = CAL_ALL_RANGE;
	WORD nRange = CAL_RANGE1;

	CString strMsg;
	if(m_nCalibrationItem == CAL_CURRENT)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRangeAll_msg1","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전류 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else if(m_nCalibrationItem == CAL_VOLTAGE)
	{
		strMsg.Format(Fun_FindMsg("OnCaliRangeAll_msg2","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 전압 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnCaliRangeAll_msg3","IDD_CALIBRATION_DLG"), nRange+1);
		//@ strMsg.Format("선택된 채널에 대해 Range %d 교정을 실시하시겠습니까?", nRange+1);
	}

	if(MessageBox(strMsg, Fun_FindMsg("OnCaliRangeAll_msg4","IDD_CALIBRATION_DLG"), MB_YESNO|MB_ICONQUESTION) != IDYES)
		//@ if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;


	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nModuleID, m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox(Fun_FindMsg("OnCaliRangeAll_msg5","IDD_CALIBRATION_DLG"), "Error", MB_ICONSTOP|MB_OK);
		//@ MessageBox("교정 정보가 정확하지 않아 시작할 수 없습니다.", "Error", MB_ICONSTOP|MB_OK);
	}
	
/*	CString strMsg;
	if(	m_nCalibrationItem == CAL_VOLTAGE)
	{
		strMsg.Format("선택된 채널에 대해 전압 전구간을 교정 하시겠습니까?");
	}
	else
	{
		strMsg.Format("선택된 채널에 대해 전류 전구간을 교정 하시겠습니까?");
	}
	if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;

	//Start 를 전송 
	m_nCalMode = CAL_ALL_ITEM_RANGE;
	int nRange = CAL_RANGE1;

	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox("교정을 시작할 수 없습니다.", "Error", MB_ICONSTOP|MB_OK);
	}
	*/
}

void CCalibratorDlg::OnOK() 
{
	//현재 모듈이 수정된 내용이 있는지 확인 하여 저장한다.
	CString strMsg;
	if(IsEditedCalData(m_nModuleID))
	{
		strMsg.Format(Fun_FindMsg("OnOK_msg","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_nModuleID));
		//@ strMsg.Format("%s 교정 Data가 수정 되었습니다. 지금 Update 하시겠습니까?", m_pDoc->GetModuleName(m_nModuleID));
		//수정 data update
		int nRtn;
		if((nRtn = MessageBox(strMsg, "Data Update", MB_YESNOCANCEL|MB_ICONQUESTION)) == IDYES)
		{
			OnBtnUpdateAll(); 
		}
		else if(nRtn == IDCANCEL)		//return;
		{
			return;
		}
		else
		{
			//수정된 내용은 모두 무시된다.
		}
	}

	//현재 교정 진행 중인지 검사
//	if()
//	{
//		if(IDYES != MessageBox("현재 교정 진행중입니다. 무시하고 종료 하시겠습니까?", "종료", MB_YESNO|MB_ICONQUESTION))
//			return;
//	}

	AfxGetApp()->WriteProfileInt(REG_CAL_SECTION, "TimeOneRange", m_nAVGOnePointCalTime);
	
	KillTimer(2000);

	if (m_bCaliFirstComm) Fun_CalibrationEnd(m_nModuleID);		//20130121 //ksj 20200819 : 자동교정 기능 이식
	
	CDialog::OnOK();
}

BOOL CCalibratorDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_ESCAPE :
			{
//				OnOK();
				return FALSE;
			}
		case VK_RETURN:
			{
				return FALSE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

//Report파일을 생성한다.
void CCalibratorDlg::OnBtnSaveFile() 
{
	CString strCSVFile1;
	CString strCSVFile2;
	
	CCalDataExportDlg *pDlg = new CCalDataExportDlg(this);

	if(pDlg->DoModal() == IDOK)
	{
		if(pDlg->m_bVertial)
		{
			strCSVFile2 = "CalReport2.csv";	
			CFileDialog dlg(FALSE, "csv", strCSVFile2, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
			dlg.m_ofn.lpstrTitle = Fun_FindMsg("OnBtnSaveFile_msg1","IDD_CALIBRATION_DLG");
			//@ dlg.m_ofn.lpstrTitle = "수직형태 저장파일명";
			if(IDOK == dlg.DoModal())
			{
				strCSVFile2 = dlg.GetPathName();
				if(ExportVerticalReport(strCSVFile2) == FALSE)
				{
					strCSVFile2.Empty();
					AfxMessageBox(strCSVFile2+ Fun_FindMsg("OnBtnSaveFile_msg2","IDD_CALIBRATION_DLG"));
					//@ AfxMessageBox(strCSVFile2+ "저장 실패!!!");
				}
			}
			else 
			{
				delete pDlg;
				pDlg = NULL;
				return;
			}
		}
		if(pDlg->m_bHorizental)
		{
			strCSVFile1 = "CalReport1.csv";	
			CFileDialog dlg(FALSE, "csv", strCSVFile1, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "csv file(*.csv)|*.csv|");
			dlg.m_ofn.lpstrTitle = Fun_FindMsg("OnBtnSaveFile_msg3","IDD_CALIBRATION_DLG");
			//@ dlg.m_ofn.lpstrTitle = "수평형태 저장파일명";
			if(IDOK == dlg.DoModal())
			{
				strCSVFile1 = dlg.GetPathName();
				if(ExportHorizentalReport(strCSVFile1)==FALSE)
				{
					strCSVFile2.Empty();
					AfxMessageBox(strCSVFile1+ Fun_FindMsg("OnBtnSaveFile_msg4","IDD_CALIBRATION_DLG"));
					//@ AfxMessageBox(strCSVFile1+ "저장 실패!!!");
				}
			}
			else 
			{
				delete pDlg;
				pDlg = NULL;
				return;
			}
		}

		CString strTemp1;
		if(!strCSVFile1.IsEmpty())
		{
			strTemp1.Format(Fun_FindMsg("OnBtnSaveFile_msg5","IDD_CALIBRATION_DLG"), strCSVFile1);
			//@ strTemp1.Format("%s를 지금 Open하시겠습니까?", strCSVFile1);

			if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
			{
				m_pDoc->ExecuteExcel(strCSVFile1);
			}
		}
		if(!strCSVFile2.IsEmpty())
		{
			strTemp1.Format(Fun_FindMsg("OnBtnSaveFile_msg6","IDD_CALIBRATION_DLG"), strCSVFile2);
			//@ strTemp1.Format("%s를 지금 Open하시겠습니까?", strCSVFile2);

			if(MessageBox(strTemp1, "File Open", MB_ICONQUESTION|MB_YESNO) != IDNO)
			{
				m_pDoc->ExecuteExcel(strCSVFile2);
			}
		}

	}
	
	delete pDlg;
	pDlg = NULL;

	return;
}


void CCalibratorDlg::UpdateCalDataList()
{
	//GetModlueID
	int nSel = m_ModuleCombo.GetCurSel();
	if(nSel < 0)	return;
	m_nModuleID = m_ModuleCombo.GetItemData(nSel);
	CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	if(pMD == NULL)		return;

	m_wndTotal.DeleteAllItems();

	m_wndTotal.InsertColumn(0, "Channel",  LVCFMT_RIGHT, 100);
	m_wndTotal.InsertColumn(1, "AD",  LVCFMT_RIGHT, 100);
	m_wndTotal.InsertColumn(2, "Meter",  LVCFMT_RIGHT, 100);

	CString strTitle;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	for(int nI = 0; nI < pMD->GetTotalChannel(); nI++ )
	{
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.iImage = 0;
		strTitle.Format("%d", nI+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
		m_wndTotal.InsertItem(&lvItem);
	}
}

void CCalibratorDlg::InitTree()
{
//	TV_INSERTSTRUCT		tvstruct;
//	ZeroMemory(&tvstruct, sizeof(TV_INSERTSTRUCT));
//	HTREEITEM	hItem1, hItem2;
//	char szName[64];
//
//	for(int j =0; j<m_pDoc->GetInstallModuleCount(); j++)
//	{
//		tvstruct.hParent = NULL;
//		tvstruct.hInsertAfter = TVI_LAST;
//		tvstruct.item.iImage = 0;
//		tvstruct.item.iSelectedImage = 1;
//		sprintf(szName, "%s", m_pDoc->GetModuleName(m_pDoc->GetModuleID(j)));
//		tvstruct.item.pszText = szName;		//Display Module ID String
//		tvstruct.item.cchTextMax = 64;
//		tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
//		hItem1 = m_ModuleTree.InsertItem(&tvstruct);
//		m_ModuleTree.SetItemData(hItem1, (DWORD)m_pDoc->GetModuleID(j));			//Set Tree Data to Module ID
//
//		CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_pDoc->GetModuleID(j));
//		if(pMD == NULL)		continue;
//
//		for(int nI = 0; nI < pMD->GetTotalChannel(); nI++ )
//		{
//			sprintf(szName, "Channel %d", nI+1);
//			tvstruct.hParent = hItem1;
//			tvstruct.hInsertAfter = TVI_LAST;
//			tvstruct.item.pszText = szName;
//			tvstruct.item.cchTextMax = 64;
//			tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
//			hItem2 = m_ModuleTree.InsertItem(&tvstruct);
//			m_ModuleTree.SetItemData(hItem2, (DWORD)nI+1);							//Set Tree Data to Channel ID
//		}
//	}
}

void CCalibratorDlg::InitCalSelChList(int nNewModuleID)
{
	m_nModuleID = nNewModuleID;
	CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	if(pMD == NULL)		return;

	CCyclerChannel *pCH;
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szBuff, _MAX_PATH);
	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));
	CString strFieName;

	m_CalChSelList.DeleteAllItems();
//
	CString strTitle;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	for(int nI = 0; nI < pMD->GetTotalChannel(); nI++ )
	{
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.iImage = I_IMAGECALLBACK;
		strTitle.Format("Ch %d", nI+1);
		lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
		m_CalChSelList.InsertItem(&lvItem);
		m_CalChSelList.SetItemData(lvItem.iItem, nI);

		//모듈이 새로 선택되면 현재 수정된 data를 모두 무시 하고 원본 Data를 Loading한다.
		pCH = pMD->GetChannelInfo(nI);
		if(pCH == NULL)		continue;
		strFieName.Format("%s\\Calibration\\M%02dC%02d.cal", path, m_nModuleID, nI+1);
		//Host의 교정 Data를 Load함 
		pCH->GetCaliData()->LoadDataFromFile(strFieName);
	}
}


BOOL CCalibratorDlg::SendCalStart(UINT nChIndex)
{
	CString strMsg;
	CCyclerModule *pMD = m_pDoc->GetCyclerMD(m_aBatchCaliData.nModuleID);
	if(pMD == NULL)
		return FALSE;

	//전송 가능여부 Check
	CCyclerChannel *pCH = pMD->GetChannelInfo(nChIndex);
	if(pCH)
	{
//		WORD state = pCH->GetState();
		if(	pCH->GetState() != PS_STATE_IDLE && 
			pCH->GetState() != PS_STATE_STANDBY && 
			pCH->GetState() != PS_STATE_END &&
			pCH->GetState() != PS_STATE_READY
		)
		{
			strMsg.Format(Fun_FindMsg("SendCalStart_msg1","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1);
			//@ strMsg.Format("%s Channel %d 교정가능한 상태가 아닙니다.", m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1);
			m_pDoc->WriteSysLog(strMsg);
#ifndef _DEBUG
			return FALSE;
#endif			
		}
	}

	int nRtn;
	m_aBatchCaliData.aCaliSet.nSel = m_nChargeDisChargeMode;//lmh 20120504 add
	//Send Calibration Start Command
	if((nRtn = pMD->SendCommand(SFT_CMD_CALI_START, nChIndex, &m_aBatchCaliData.aCaliSet, sizeof(SFT_CALI_START_INFO))) == SFT_ACK)
	{
		strMsg.Format(Fun_FindMsg("SendCalStart_msg2","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1);
		//@ strMsg.Format("%s Channel %d 교정 명령전송 성공", m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1);
		m_pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("SendCalStart_msg3","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1, nRtn);
		//@ strMsg.Format("%s Channel %d 교정 명령전송 실패(%d)", m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1, nRtn);
//		MessageBox(strMsg + "[교정 준비가 되어 있지 않음 ]", "Error", MB_OK|MB_ICONSTOP);
		m_pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
		return FALSE;
	}

	CString strItem;
	if(m_aBatchCaliData.aCaliSet.nType == CAL_VOLTAGE)
	{
		strItem = Fun_FindMsg("SendCalStart_msg4","IDD_CALIBRATION_DLG");
		//@ strItem = "전압";
	}
	else if(m_aBatchCaliData.aCaliSet.nType == CAL_CURRENT)
	{
		strItem = Fun_FindMsg("SendCalStart_msg5","IDD_CALIBRATION_DLG");
		//@ strItem = "전류";
	}
	
	strMsg.Format(Fun_FindMsg("SendCalStart_msg6","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1, strItem, m_aBatchCaliData.aCaliSet.nRange+1);
	//@ strMsg.Format("%s Ch %d %s Range%d 교정중...", m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID), nChIndex+1, strItem, m_aBatchCaliData.aCaliSet.nRange+1);
	SetDlgItemText(IDC_EDIT_STATUS, strMsg);

	m_bStopFlag = FALSE;
	
	BeginWaitState();
	
	return TRUE;
}
/*
//교정 조건에 대한 Data를 Update한다.
BOOL CCalibratorDlg::UpdateBatchData(int nType, WORD wRange, int nMode)
{
	//Module ID Update
	int nID = m_ModuleCombo.GetCurSel();
	if(nID >= 0)
	{
		m_aBatchCaliData.nModuleID = m_ModuleCombo.GetItemData(nID);
	}
	else return	FALSE;


	//다채널 선택하여 진행하고 Stop 명령을 지원하기 위해 Timer를 이용하여 한채널씩 전송한다.
	int nItem;
	WORD nChIndex;
	POSITION pos = m_CalChSelList.GetFirstSelectedItemPosition();
	m_aCaliSelChArray.RemoveAll();
	while(pos)
	{
		nItem = m_CalChSelList.GetNextSelectedItem(pos);
		nChIndex = (WORD)m_CalChSelList.GetItemData(nItem);
		m_aCaliSelChArray.Add(nChIndex);
	}
	if(m_aCaliSelChArray.GetSize() < 1)	
	{
		return		FALSE;
	}

	return UpdateBatchCalPointData(nType, wRange);

	/*
	CCaliPoint *pCalPoint = m_pDoc->GetCalPoint();

	//Set Command Body
	ZeroMemory(&m_aBatchCaliData.aCaliSet, sizeof(SFT_CALI_START_INFO));
	m_aBatchCaliData.byTotalPoint = 0;
	m_aBatchCaliData.aCaliSet.nType =	nType;
	m_aBatchCaliData.aCaliSet.nRange =	wRange;
	m_aBatchCaliData.aCaliSet.nMode = CAL_MODE_CALIBRATION;
	if(nType == CAL_VOLTAGE)
	{
		if(m_aBatchCaliData.aCaliSet.nMode == CAL_MODE_CALIBRATION)
		{
			m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetVtgSetPointCount(wRange);
		}
		m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetVtgCheckPointCount(wRange);

		m_aBatchCaliData.aCaliSet.pointData.byValidPointNum = pCalPoint->GetVtgSetPointCount(wRange);
		for(int i=0; i<m_aBatchCaliData.aCaliSet.pointData.byValidPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCaliPoint[i] = FLOAT2LONG(pCalPoint->GetVSetPoint(wRange, i));
		}
			
		m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum = pCalPoint->GetVtgCheckPointCount(wRange);
		for(int i = 0; i<m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCheckPoint[i] =FLOAT2LONG(pCalPoint->GetVCheckPoint(wRange, i));
		}
	}
	else if(nType == CAL_CURRENT)	//PS_CURRENT
	{
		if(m_aBatchCaliData.aCaliSet.nMode == CAL_MODE_CALIBRATION)
		{
			m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetCrtSetPointCount(wRange);
		}
		m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetCrtCheckPointCount(wRange);
		
		m_aBatchCaliData.aCaliSet.pointData.byValidPointNum = pCalPoint->GetCrtSetPointCount(wRange);
		for(int i=0; i<m_aBatchCaliData.aCaliSet.pointData.byValidPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCaliPoint[i] = FLOAT2LONG(pCalPoint->GetISetPoint(wRange, i));
		}
			
		m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum = pCalPoint->GetCrtCheckPointCount(wRange);
		for(int i = 0; i<m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCheckPoint[i] =FLOAT2LONG(pCalPoint->GetICheckPoint(wRange, i));
		}
	}
	else return FALSE;

	//가장 처음 선택된 채널에 시작 명령을 전송하고
	//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
	m_aBatchCaliData.byChSelIndex = 0;

	return TRUE;
	*/
//}

void CCalibratorDlg::UpdateMDStateList(int nChIndex)
{
	//GetModlueID
	CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	if(pMD == NULL)		return;

//	LV_ITEM lvItem;
//	ZeroMemory(&lvItem, sizeof(LV_ITEM));
//	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;

	CCyclerChannel *pCH;
	CString strTitle;
	if(nChIndex < 0)
	{
		for(int nI = 0; nI < pMD->GetTotalChannel(); nI++ )
		{
			pCH = pMD->GetChannelInfo(nI);
			if(pCH)
			{
//				lvItem.iItem = nI;
//				lvItem.iSubItem = 1;
				int nDataState = pCH->GetCaliData()->GetState();
				
				if(CAL_DATA_SAVED == nDataState)
				{
					strTitle = Fun_FindMsg("UpdateMDStateList_msg1","IDD_CALIBRATION_DLG");
					//@ strTitle = "완료";
//					lvItem.pszText =  "완료";
				}
				else if(CAL_DATA_MODIFY == nDataState)
				{
					strTitle = Fun_FindMsg("UpdateMDStateList_msg2","IDD_CALIBRATION_DLG");
					//@ strTitle = "수정됨";
//					lvItem.pszText =  "수정됨";
				}
				else if(CAL_DATA_ERROR == nDataState)
				{
					strTitle = Fun_FindMsg("UpdateMDStateList_msg3","IDD_CALIBRATION_DLG");
					//@ strTitle = "오차범위초과";
//					lvItem.pszText =  "오차범위초과";
				}
				else //if(CAL_DATA_EMPTY == nDataState)
				{
					strTitle = "UnLoad";
				}
//				m_CalChSelList.SetItem(&lvItem);

				m_CalChSelList.SetItemText(nI, 1, strTitle);				
				m_CalChSelList.SetItemText(nI, 2,pCH->GetCaliData()->GetUpdateTime());
			}
		}	
	}
	else
	{
		pCH = pMD->GetChannelInfo(nChIndex);
		if(pCH)
		{
			int nDataState = pCH->GetCaliData()->GetState();
			if(CAL_DATA_SAVED == nDataState)
			{
				strTitle = Fun_FindMsg("UpdateMDStateList_msg4","IDD_CALIBRATION_DLG");
				//@ strTitle = "완료";
			}
			else if(CAL_DATA_MODIFY == nDataState)
			{
				strTitle = Fun_FindMsg("UpdateMDStateList_msg5","IDD_CALIBRATION_DLG");
				//@ strTitle = "수정됨";
			}
			else if(CAL_DATA_ERROR == nDataState)
			{
				strTitle = Fun_FindMsg("UpdateMDStateList_msg6","IDD_CALIBRATION_DLG");
				//@ strTitle = "오차범위초과";
			}
			else //if(CAL_DATA_EMPTY == nDataState)
			{
				strTitle = "UnLoad";
			}

			m_CalChSelList.SetItemText(nChIndex, 1, strTitle);
			m_CalChSelList.SetItemText(nChIndex, 2, pCH->GetCaliData()->GetUpdateTime());
		}
	}
}

void CCalibratorDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here
	//GetModlueID
	//채널 리스트를 다시 그린다.
	int nSel = m_ModuleCombo.GetCurSel();
	if(nSel < 0)	return;
	int nModuleID = m_ModuleCombo.GetItemData(nSel);
	
	if(m_nModuleID == nModuleID)	return;

	CString strMsg;
	//이전 채널이 변경된 내용이 있으면 Update여부 확인한다.
	if(IsEditedCalData(m_nModuleID))
	{
		strMsg.Format(Fun_FindMsg("OnSelchangeModuleSelCombo_msg","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_nModuleID));
		//@ strMsg.Format("%s 교정 Data가 수정 되었습니다. 지금 Update 하시겠습니까?", m_pDoc->GetModuleName(m_nModuleID));
		//수정 data update
		int nRtn;
		if((nRtn = MessageBox(strMsg, "Data Update", MB_YESNOCANCEL|MB_ICONQUESTION)) == IDYES)
		{
			OnBtnUpdateAll(); 
		}
		else if(nRtn == IDCANCEL)		//return;
		{
			//Combo를 원래 상태로 만들고 Return 한다.
			for(int a =0; a<m_ModuleCombo.GetCount(); a++)
			{
				if(m_ModuleCombo.GetItemData(a) == (DWORD)m_nModuleID)
				{
					m_ModuleCombo.SetCurSel(a);
					return;
				}
			}
		}
		else
		{
			//수정된 내용은 모두 무시된다.
		}
	}

	//선택 모듈로 다시 그림 
	InitCalSelChList(nModuleID);	

	//각 채널의 상태를 Update한다.
	UpdateMDStateList();

//	OnBtnConnect();
}

//채널 변수의 교정값 변수를 Updat 시킨다.
BOOL CCalibratorDlg::UpdateChCaliData(LPSFT_CALI_END_DATA pReceivedCalData)
{
	//교정 명령을 보낸것에 대한 응답이면 
	CCyclerModule *pMD;
	CCyclerChannel *pCH;
	CCalibrationData *pChCalData;
	WORD nI;
	double dPointData[MAX_CALIB_CHECK_POINT];
	CCaliPoint *pCalPoint = m_pDoc->GetCalPoint();
	BOOL bSEl = 0 ;
	if(pReceivedCalData->nSelType == 4) bSEl = 1;

	int nChIndex = m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex);
	if( nChIndex == pReceivedCalData->byChannelID-1 )		//byChannelID => One Base
	{
		pMD = m_pDoc->GetCyclerMD(m_aBatchCaliData.nModuleID);
		if(pMD == NULL)		return	FALSE;
		pCH = pMD->GetChannelInfo(nChIndex);
		
		if(pCH == NULL)		return	FALSE;
		pChCalData = pCH->GetCaliData();

		pChCalData->SetEdited();			//Set modified flag to CAL_DATA_MODIFY
	
		if(pReceivedCalData->nType == CAL_VOLTAGE)
		{
			//lmh 20120504 add
//			pChCalData->m_PointData.m_nChargeDisChargeMode[pReceivedCalData->byRange] = pReceivedCalData->nSelType;//lmh 20120504 cal add 수정
			//교정 Point 정보 Update
			ZeroMemory(dPointData, sizeof(dPointData));
			for(nI = 0; nI<pReceivedCalData->byValidCalPointNum && nI < MAX_CALIB_SET_POINT ; nI++)
			{
//				dPointData[nI] = m_aBatchCaliData.aCaliSet.lCaliPoint[nI]/100000.0f;
				dPointData[nI] = pCalPoint->GetVSetPoint(pReceivedCalData->byRange, nI);
			}
			pChCalData->m_PointData.SetVSetPointData(	pReceivedCalData->byValidCalPointNum, 
														dPointData,
														pReceivedCalData->byRange);
			//교정 검사 Point 정보 Update
			ZeroMemory(dPointData, sizeof(dPointData));
			for(nI = 0; nI<pReceivedCalData->byValidCheckPointNum && nI < MAX_CALIB_SET_POINT; nI++)
			{
//				dPointData[nI] = m_aBatchCaliData.aCaliSet.lCheckPoint[nI]/100000.0f;
				dPointData[nI] = pCalPoint->GetVCheckPoint( pReceivedCalData->byRange, nI);
			}
			pChCalData->m_PointData.SetVCheckPointData(	pReceivedCalData->byValidCheckPointNum, 
														dPointData,
														pReceivedCalData->byRange);

			//교정 결과값 Update
			for(nI = 0; nI < pReceivedCalData->byValidCalPointNum && nI<MAX_CALIB_CHECK_POINT; nI++)
			{
				pChCalData->m_ChCaliData.SetVCalData(	pReceivedCalData->byRange,
														(UINT)bSEl,
														nI, 
														LONG2FLOAT(pReceivedCalData->lCaliAD[nI]), 
														LONG2FLOAT(pReceivedCalData->lCaliDVM[nI]));
			}

			//교정 결과 확인값 Udpate
			for(nI = 0; nI < pReceivedCalData->byValidCheckPointNum && nI<MAX_CALIB_CHECK_POINT; nI++)
			{
				pChCalData->m_ChCaliData.SetVCheckData(	pReceivedCalData->byRange, 
														(UINT)bSEl,
														nI, 
														LONG2FLOAT(pReceivedCalData->lCheckAD[nI]), 
														LONG2FLOAT(pReceivedCalData->lCheckDVM[nI]));
				
				//교정 확인값이 설정범위 이상 벗어났으면 오차발생 표시
//				if( fabs(pChCalData->m_PointData.GetVCheckPoint(pReceivedCalData->byRange, nI)-LONG2FLOAT(pReceivedCalData->lCheckDVM[nI])) > m_fVtgDAErrorSet ||
//					fabs(LONG2FLOAT(pReceivedCalData->lCheckDVM[nI]) - LONG2FLOAT(pReceivedCalData->lCheckAD[nI])) > m_fVtgADErrorSet)
				if( fabs(pChCalData->m_PointData.GetVCheckPoint(pReceivedCalData->byRange, nI)-LONG2FLOAT(pReceivedCalData->lCheckDVM[nI])) > pCalPoint->m_dVDAAccuracy[pReceivedCalData->byRange] ||
					fabs(LONG2FLOAT(pReceivedCalData->lCheckDVM[nI]) - LONG2FLOAT(pReceivedCalData->lCheckAD[nI])) > pCalPoint->m_dVADAccuracy[pReceivedCalData->byRange])
				{
					pChCalData->SetState(CAL_DATA_ERROR);
				}
			}
		}
		else	//PS_CURRENT
		{
			//교정 Point 정보 Update
			ZeroMemory(dPointData, sizeof(dPointData));
			for(int nI = 0; nI<pReceivedCalData->byValidCalPointNum && nI<MAX_CALIB_SET_POINT; nI++)
			{
//				dPointData[nI] = m_aBatchCaliData.aCaliSet.lCaliPoint[nI]/100.0f;
				dPointData[nI] = pCalPoint->GetISetPoint(pReceivedCalData->byRange, nI);
			}
			pChCalData->m_PointData.SetISetPointData(	pReceivedCalData->byValidCalPointNum, 
														dPointData,
														pReceivedCalData->byRange);
			
			//교정 검사 Point 정보 Update
			ZeroMemory(dPointData, sizeof(dPointData));
			for(nI = 0; nI<pReceivedCalData->byValidCheckPointNum && nI<MAX_CALIB_SET_POINT; nI++)
			{
//				dPointData[nI] = m_aBatchCaliData.aCaliSet.lCheckPoint[nI]/100.0f;
				dPointData[nI] = pCalPoint->GetICheckPoint(pReceivedCalData->byRange, nI);
			}
			pChCalData->m_PointData.SetICheckPointData(	pReceivedCalData->byValidCheckPointNum, 
														dPointData,
														pReceivedCalData->byRange);
			//교정 결과값 Update
			for(nI = 0; nI < pReceivedCalData->byValidCalPointNum && nI<MAX_CALIB_CHECK_POINT; nI++)
			{
				pChCalData->m_ChCaliData.SetICalData(	pReceivedCalData->byRange, 
														nI, 
														LONG2FLOAT(pReceivedCalData->lCaliAD[nI]), 
														LONG2FLOAT(pReceivedCalData->lCaliDVM[nI]));
			}

			//교정 결과 확인값 Udpate
			for(nI = 0; nI < pReceivedCalData->byValidCheckPointNum && nI < MAX_CALIB_CHECK_POINT; nI++)
			{
				pChCalData->m_ChCaliData.SetICheckData(	pReceivedCalData->byRange, 
														nI, 
														LONG2FLOAT(pReceivedCalData->lCheckAD[nI]), 
														LONG2FLOAT(pReceivedCalData->lCheckDVM[nI]));
				//교정 확인값이 설정범위 이상 벗어났으면 오차발생 표시
//				if( fabs(pChCalData->m_PointData.GetICheckPoint(pReceivedCalData->byRange, nI)-LONG2FLOAT(pReceivedCalData->lCheckDVM[nI])) > m_fCrtDAErrorSet ||
//					fabs(LONG2FLOAT(pReceivedCalData->lCheckDVM[nI]) - LONG2FLOAT(pReceivedCalData->lCheckAD[nI])) > m_fCrtADErrorSet)
				if( fabs(pChCalData->m_PointData.GetICheckPoint(pReceivedCalData->byRange, nI)-LONG2FLOAT(pReceivedCalData->lCheckDVM[nI])) > pCalPoint->m_dIDAAccuracy[pReceivedCalData->byRange] ||
					fabs(LONG2FLOAT(pReceivedCalData->lCheckDVM[nI]) - LONG2FLOAT(pReceivedCalData->lCheckAD[nI])) > pCalPoint->m_dIADAccuracy[pReceivedCalData->byRange])
				{

					pChCalData->SetState(CAL_DATA_ERROR);
				}
			}
		}
	}
	return TRUE;
}

//채널 리스트를 변경하면 data를 표기 한다.
void CCalibratorDlg::OnItemchangedCalChList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW *)pNMHDR;
	// TODO: Add your control notification handler code here
	POSITION pos = m_CalChSelList.GetFirstSelectedItemPosition();
	//20180619 yulee add _ 교정 결과 데이터 (채널 업데이트 시에는) 업데이트 스킵을 위함 
	int nPrevNo;
	
	for(int i=0; i<m_CalChSelList.GetItemCount();i++)
	{
		if(m_CalChSelList.GetItemState(i, LVIS_SELECTED)&LVIS_SELECTED)
			m_nPresNo=i;
	}
	
	nPrevNo = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CalibChSave", 0);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CalibChSave", m_nPresNo);

	
if(m_nPresNo != nPrevNo)
{
	if(pos != NULL)
	{
		int nItem = m_CalChSelList.GetNextSelectedItem(pos);
		int nChIndex = m_CalChSelList.GetItemData(nItem);
		SetReceivedCheckData(m_nModuleID, nChIndex);	
		
		CString strTemp;
		strTemp.Format(Fun_FindMsg("OnItemchangedCalChList_msg","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_nModuleID), nChIndex+1);
		//@ strTemp.Format("%s Ch%d 교정 선택", m_pDoc->GetModuleName(m_nModuleID), nChIndex+1);
		SetDlgItemText(IDC_CAL_SEL_CH_STATIC, strTemp);
	}
}
	
	*pResult = 0;
}

//진행 중이 교정 중단.
void CCalibratorDlg::OnStopButton() 
{
	// TODO: Add your control notification handler code here
	//현재 진행중인 채널을 완료하고 중단한다.
	MessageBox(Fun_FindMsg("OnStopButton_msg1","IDD_CALIBRATION_DLG"),  Fun_FindMsg("OnStopButton_msg2","IDD_CALIBRATION_DLG"), MB_ICONINFORMATION|MB_OK);
	//@ MessageBox("현재 진행중인 채널의 교정을 완료후 종료 합니다.",  "알림", MB_ICONINFORMATION|MB_OK);
	m_bStopFlag = TRUE;
}

//교정 Data가 수정 되었는지 확인 한다.
BOOL CCalibratorDlg::IsEditedCalData(int nModuleID)
{
	CCyclerModule *pMD;
	CCyclerChannel *pCH;
	CCalibrationData *pChCalData;

	pMD = m_pDoc->GetCyclerMD(nModuleID);
	if(pMD == NULL)		return	FALSE;
	for(int i =0; i<pMD->GetTotalChannel(); i++)
	{
		pCH = pMD->GetChannelInfo(i);
		if(pCH == NULL)		continue;
		pChCalData = pCH->GetCaliData();
		if(pChCalData->GetState() == CAL_DATA_MODIFY)		return TRUE;
	}
	return FALSE;
}


void CCalibratorDlg::OnGetdispinfoCalChList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	 LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;

//    long index = pDispInfo->item.iItem;
//    long subItem = pDispInfo->item.iSubItem;
//    long objCode = pDispInfo->item.lParam;

	UINT nChannelIndex;
	DWORD dwImgIndex = 0;
	if(pDispInfo->item.mask & LVIF_IMAGE)
    {
		nChannelIndex = m_CalChSelList.GetItemData(pDispInfo->item.iItem);
		CCyclerChannel *pCh = m_pDoc->GetChannelInfo(m_nModuleID, nChannelIndex);
		if(pCh)
		{
			dwImgIndex = m_pDoc->GetStateImageIndex(pCh->GetState(), pCh->GetStepType());
		}

//		if(pDispInfo->item.state & LVIS_SELECTED)
//		{
//			dwImgIndex += 2;
//		}
		pDispInfo->item.iImage = dwImgIndex;
 
		*pResult = 1; // 프로그램상으로 처리했다는 뜻으로 *pResult = 1 로 합니다.
		return;
    }
	*pResult = 0;
}

void CCalibratorDlg::OnAllItemButton() 
{
	// TODO: Add your control notification handler code here

	CString strMsg;
	strMsg.Format(Fun_FindMsg("OnAllItemButton_msg1","IDD_CALIBRATION_DLG"));
	//@ strMsg.Format("선택된 채널에 대해 전압/전류 모든 구간을 교정을 실시하시겠습니까?");
	if(MessageBox(strMsg, Fun_FindMsg("OnAllItemButton_msg2","IDD_CALIBRATION_DLG"), MB_YESNO|MB_ICONQUESTION) != IDYES)
		//@ if(MessageBox(strMsg, "교정 확인", MB_YESNO|MB_ICONQUESTION) != IDYES)
		return;

	//Start 를 전송 
	m_nCalibrationItem = CAL_VOLTAGE;
	m_nCalMode = CAL_ALL_ITEM_RANGE;
	WORD nRange = CAL_RANGE1;

	//전압 Range 1부터 시작한다.
	if(StartCalibration(m_nModuleID, m_nCalibrationItem, nRange, m_nCalMode) == FALSE)
	{
		MessageBox(Fun_FindMsg("OnAllItemButton_msg3","IDD_CALIBRATION_DLG"), "Error", MB_ICONSTOP|MB_OK);
		//@ MessageBox("교정 정보가 정확하지 않아 시작할 수 없습니다.", "Error", MB_ICONSTOP|MB_OK);
	}
}

BOOL CCalibratorDlg::ExportHorizentalReport(CString strFileName)
{
	if(strFileName.IsEmpty())		return FALSE;

	CString strCrtUnit = "mA";
	CString strVtgUnit("mV");
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0))  strCrtUnit = "uA";
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0))  strVtgUnit = "uV";

	CString strTemp;
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szBuff, _MAX_PATH);
	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	FILE *fp = NULL;
	fp = fopen(strFileName, "wt");
	if(fp == NULL)
	{
		return FALSE;
	}

	double dADData, dMeterData;
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	CCalibrationData *pChCalData;
	int nModuleID;

	BOOL bFirst = TRUE;
	for(WORD nVRnage =0; nVRnage < SFT_MAX_VOLTAGE_RANGE; nVRnage++)
	{

		for(int md =0; md < m_pDoc->GetInstallModuleCount(); md++)
		{
			nModuleID = m_pDoc->GetModuleID(md);
			pMD = m_pDoc->GetModuleInfo(nModuleID);
			if(pMD == NULL)		continue;
			
			for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
			{
				pCh = pMD->GetChannelInfo(ch);
				if(pCh == NULL)		continue;

				pChCalData = pCh->GetCaliData();
				
				//Host의 교정 Data를 Load함 
				strTemp.Format("%s\\Calibration\\M%02dC%02d.cal", path, nModuleID, ch+1);
				if(pChCalData->IsLoadedData() == FALSE)
				{
					if(pChCalData->LoadDataFromFile(strTemp) == FALSE)
					{
						continue;
					}
				}
				
				//Column Header 삽입 
				if(bFirst)
				{
					bFirst = FALSE;
					//전압 교정 확인 Point 표시 
					fprintf(fp, "Module,CH,Range,");
					for(WORD pos = 0; pos < pChCalData->m_PointData.GetVtgCheckPointCount(nVRnage) && pos < MAX_CALIB_CHECK_POINT; pos++)
					{
						fprintf(fp, Fun_FindMsg("ExportHorizentalReport_msg1","IDD_CALIBRATION_DLG"), 
							//@ fprintf(fp, "지령%d(%s),계측기%d(%s),지령오차%d(%s),측정%d(%s),측정오차%d(%s),", 
									pos+1, strVtgUnit, pos+1, strVtgUnit,pos+1, strVtgUnit,pos+1, strVtgUnit,pos+1,strVtgUnit);
					}
					fprintf(fp, "\n");
				}
				
				if(pChCalData->m_PointData.GetVtgCheckPointCount(nVRnage) <=0 )		continue;
				
				fprintf(fp, "%d,", nModuleID);		//Module
				fprintf(fp, "%d,", ch+1);			//Channel
				fprintf(fp, "%d,", nVRnage+1);		//Range


				//전압 교정 확인 Point 표시 
				for(WORD wSel = 0; wSel < 2; wSel++)
				{
					for(WORD pos = 0; pos < pChCalData->m_PointData.GetVtgCheckPointCount(nVRnage) && pos < MAX_CALIB_CHECK_POINT; pos++)
					{
						fprintf(fp, "%.3f,", pChCalData->m_PointData.GetVCheckPoint(nVRnage, pos));	//지령
						pChCalData->m_ChCaliData.GetVCheckData(nVRnage, wSel,pos, dADData, dMeterData);
						fprintf(fp, "%.3f,", (float)dMeterData);									//Meter
						fprintf(fp, "%.3f,", (float)pChCalData->m_PointData.GetVCheckPoint(nVRnage, pos)-dMeterData);	//오차 
						fprintf(fp, "%.3f,", float(dADData));										//측정
						fprintf(fp, "%.3f,", float(dMeterData-dADData));							//오차
					}
					fprintf(fp, "\n");
				}
				
			}
		}
	}
	
	fprintf(fp, "\n");
				
	bFirst = TRUE;
	for(WORD nIRange =0; nIRange < SFT_MAX_CURRENT_RANGE; nIRange++)
	{
		for(int md =0; md < m_pDoc->GetInstallModuleCount(); md++)
		{
			nModuleID = m_pDoc->GetModuleID(md);
			pMD = m_pDoc->GetModuleInfo(nModuleID);
			if(pMD == NULL)		continue;
			
			for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
			{
				//Column Header 삽입 
				pCh = pMD->GetChannelInfo(ch);
				if(pCh == NULL)		continue;

				pChCalData = pCh->GetCaliData();
				
				//Host의 교정 Data를 Load함 
				strTemp.Format("%s\\Calibration\\M%02dC%02d.cal", path, nModuleID, ch+1);
				if(pChCalData->IsLoadedData() == FALSE)
				{
					if(pChCalData->LoadDataFromFile(strTemp) == FALSE)
					{
						continue;
					}
				}
				//Column Header 삽입 
				if(bFirst)
				{
					bFirst = FALSE;
					//전압 교정 확인 Point 표시 
					fprintf(fp, "Module,CH,Range,");
					for(WORD pos = 0; pos < pChCalData->m_PointData.GetCrtCheckPointCount(nIRange) && pos < MAX_CALIB_CHECK_POINT; pos++)
					{
						fprintf(fp, Fun_FindMsg("ExportHorizentalReport_msg2","IDD_CALIBRATION_DLG"), 
							//@ fprintf(fp, "지령 %d(%s),계측기%d(%s),지령오차%d(%s),측정%d(%s),측정오차%d(%s),", 
							pos+1, strCrtUnit, pos+1, strCrtUnit,pos+1, strCrtUnit,pos+1, strCrtUnit,pos+1,strCrtUnit);
					}
					fprintf(fp, "\n");
				}
				
				if(pChCalData->m_PointData.GetCrtCheckPointCount(nIRange) <= 0)		continue;

				fprintf(fp, "%d,", nModuleID);
				fprintf(fp, "%d,", ch+1);
				fprintf(fp, "%d,", nIRange+1);
				
				for(WORD pos = 0; pos < pChCalData->m_PointData.GetCrtCheckPointCount(nIRange) && pos < MAX_CALIB_CHECK_POINT; pos++)
				{
					fprintf(fp, "%.3f,", (float)pChCalData->m_PointData.GetICheckPoint(nIRange, pos));
							
					pChCalData->m_ChCaliData.GetICheckData(nIRange, pos, dADData, dMeterData);
					fprintf(fp, "%.3f,", (float)dMeterData);
					fprintf(fp, "%.3f,", (float)pChCalData->m_PointData.GetICheckPoint(nIRange, pos) - dMeterData);
					fprintf(fp, "%.3f,", float(dADData));
					fprintf(fp, "%.3f,", float(dMeterData - dADData));	
				}
				fprintf(fp, "\n");
			}
		}
	}
	fclose(fp);
	return TRUE;
}

BOOL CCalibratorDlg::ExportVerticalReport(CString strFileName)
{
	CString strCrtUnit = "mA";
	CString strVtgUnit("mV");
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0))  strCrtUnit = "uA";
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0))  strVtgUnit = "uV";

	CString strTemp;
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szBuff, _MAX_PATH);
	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	FILE *fp = NULL;
	fp = fopen(strFileName, "wt");
	if(fp == NULL)
	{
		return FALSE;
	}

	double dADData, dMeterData;
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	CCalibrationData *pChCalData;
	int nModuleID;
	fprintf(fp, Fun_FindMsg("ExportVerticalReport_msg1","IDD_CALIBRATION_DLG"));
	//@ fprintf(fp, "Module,CH,Range,전압/전류,지령,계측기,계측오차,측정,측정오차\n");
	
	for(int md =0; md < m_pDoc->GetInstallModuleCount(); md++)
	{
//		fprintf(fp, "Module,%s\n", m_pDoc->GetModuleName(nModuleID));
		nModuleID = m_pDoc->GetModuleID(md);
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD == NULL)		continue;
		
		for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
		{
			//Column Header 삽입 
			
			pCh = pMD->GetChannelInfo(ch);
			if(pCh == NULL)		continue;

			pChCalData = pCh->GetCaliData();
			
			//Host의 교정 Data를 Load함 
			strTemp.Format("%s\\Calibration\\M%02dC%02d.cal", path, nModuleID, ch+1);
			//if(pCh->GetCaliData()->)
			if(pChCalData->LoadDataFromFile(strTemp) == FALSE)
			{
				continue;
			}

			//전압 교정 확인 Point 표시 
			for(WORD nI =0; nI<SFT_MAX_VOLTAGE_RANGE; nI++)
			{
/*				for(int pos = 0; pos < pChCalData->m_PointData.GetVtgSetPointCount(nI) && pos < MAX_CALIB_SET_POINT; pos++)
				{
					strTemp.Format("CH%02d 전압", nChIndex+1);
			
					strTemp.Format("Range-%d 교정", nI+1);
					pWndResult->SetItemText(aItem.iItem, 1, strTemp.Right(16));

					strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetVSetPoint(nI, pos));
					pWndResult->SetItemText(aItem.iItem, 2, strTemp.Right(16));
							
					pChCalData->m_ChCaliData.GetVCalData(nI, pos, dADData, dMeterData);
					strTemp.Format("%.3f", (float)dMeterData);
					pWndResult->SetItemText(aItem.iItem, 3, strTemp.Right(16));
							
					strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetVSetPoint(nI, pos)-dMeterData);
					pWndResult->SetItemText(aItem.iItem, 4, strTemp.Right(16));
						
					strTemp.Format("%.3f", (float)dADData);
					pWndResult->SetItemText(aItem.iItem, 5, strTemp.Right(16));
							
					strTemp.Format("%.3f", (float)(dMeterData-dADData));
					pWndResult->SetItemText(aItem.iItem, 6, strTemp.Right(16));
				}
*/						
				for(WORD wSel = 0; wSel < 2; wSel++)
				{
					for(WORD pos = 0; pos < pChCalData->m_PointData.GetVtgCheckPointCount(nI) && pos < MAX_CALIB_CHECK_POINT; pos++)
					{
						//fprintf(fp, "CH,Range,전압/전류,지령,계측기,계측오차,측정,측정오차\n");
						fprintf(fp, "%d,", nModuleID);
						fprintf(fp, "%d,", ch+1);
						fprintf(fp, "%d,", nI+1);
						fprintf(fp, Fun_FindMsg("ExportVerticalReport_msg2","IDD_CALIBRATION_DLG"),strVtgUnit);
						//@ fprintf(fp, "전압(%s),",strVtgUnit);
						fprintf(fp, "%.3f,", pChCalData->m_PointData.GetVCheckPoint(nI, pos));
						
						pChCalData->m_ChCaliData.GetVCheckData(nI, wSel, pos, dADData, dMeterData);
						fprintf(fp, "%.3f,", (float)dMeterData);
						fprintf(fp, "%.3f,", (float)pChCalData->m_PointData.GetVCheckPoint(nI, pos)-dMeterData);
						fprintf(fp, "%.3f,", float(dADData));
						fprintf(fp, "%.3f\n", float(dMeterData-dADData));
					}
				}
				
			}
				
				//전류 교정 확인 Point 표시 
			for(int nI = 0; nI<SFT_MAX_CURRENT_RANGE; nI++)
			{
/*					for(int pos = 0; pos < pChCalData->m_PointData.GetCrtSetPointCount(nI) && pos <MAX_CALIB_SET_POINT; pos++)
					{
						strTemp.Format("CH%02d 전류", nChIndex+1);
						pWndResult->InsertItem(&aItem);

						strTemp.Format("Range-%d 교정", nI+1);
						pWndResult->SetItemText(aItem.iItem, 1, strTemp.Right(16));

						strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetISetPoint(nI, pos));
						pWndResult->SetItemText(aItem.iItem, 2, strTemp.Right(16));
							
						pChCalData->m_ChCaliData.GetICalData(nI, pos, dADData, dMeterData);
							
						strTemp.Format("%.3f", (float)dMeterData);
						pWndResult->SetItemText(aItem.iItem, 3, strTemp.Right(16));

						strTemp.Format("%.3f", (float)pChCalData->m_PointData.GetISetPoint(nI, pos) - dMeterData);
						pWndResult->SetItemText(aItem.iItem, 4, strTemp.Right(16));
							
							
						strTemp.Format("%.3f", (float)dADData);
						pWndResult->SetItemText(aItem.iItem, 5, strTemp.Right(16));
							
						strTemp.Format("%.3f", (float)(dMeterData - dADData));
						pWndResult->SetItemText(aItem.iItem, 6, strTemp.Right(16));
					}
*/
				for(WORD pos = 0; pos < pChCalData->m_PointData.GetCrtCheckPointCount(nI) && pos < MAX_CALIB_CHECK_POINT; pos++)
				{
					fprintf(fp, "%d,", nModuleID);
					fprintf(fp, "%d,", ch+1);
					fprintf(fp, "%d,", nI+1);
					fprintf(fp, Fun_FindMsg("ExportVerticalReport_msg3","IDD_CALIBRATION_DLG"), strCrtUnit);
					//@ fprintf(fp, "전류(%s),", strCrtUnit);
					fprintf(fp, "%.3f,", (float)pChCalData->m_PointData.GetICheckPoint(nI, pos));
						
					pChCalData->m_ChCaliData.GetICheckData(nI, pos, dADData, dMeterData);
					fprintf(fp, "%.3f,", (float)dMeterData);
					fprintf(fp, "%.3f,", (float)pChCalData->m_PointData.GetICheckPoint(nI, pos) - dMeterData);
					fprintf(fp, "%.3f,", float(dADData));
					fprintf(fp, "%.3f\n", float(dMeterData - dADData));	
				}
			}
		}
	}

	fclose(fp);
	return TRUE;
}

BOOL CCalibratorDlg::UpdateBatchCalPointData(int nType, WORD wRange)
{
	CCaliPoint *pCalPoint = m_pDoc->GetCalPoint();

	//Set Command Body
	ZeroMemory(&m_aBatchCaliData.aCaliSet, sizeof(SFT_CALI_START_INFO));
	m_aBatchCaliData.byTotalPoint = 0;
	m_aBatchCaliData.aCaliSet.nType =	nType;
	m_aBatchCaliData.aCaliSet.nRange =	wRange;
	m_aBatchCaliData.aCaliSet.nMode = CAL_MODE_CALIBRATION;
	if(nType == CAL_VOLTAGE)
	{
		if(m_aBatchCaliData.aCaliSet.nMode == CAL_MODE_CALIBRATION)
		{
			m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetVtgSetPointCount(wRange);
		}
		m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetVtgCheckPointCount(wRange);

		m_aBatchCaliData.aCaliSet.pointData.byValidPointNum = (BYTE)pCalPoint->GetVtgSetPointCount(wRange);
		for(int i=0; i<m_aBatchCaliData.aCaliSet.pointData.byValidPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCaliPoint[i] = FLOAT2LONG(pCalPoint->GetVSetPoint(wRange, (WORD)i));
		}
			
		m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum = (BYTE)pCalPoint->GetVtgCheckPointCount(wRange);
		for(int i = 0; i<m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCheckPoint[i] =FLOAT2LONG(pCalPoint->GetVCheckPoint(wRange, (WORD)i));
		}
	}
	else if(nType == CAL_CURRENT)	//PS_CURRENT
	{
		if(m_aBatchCaliData.aCaliSet.nMode == CAL_MODE_CALIBRATION)
		{
			m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetCrtSetPointCount(wRange);
		}
		m_aBatchCaliData.byTotalPoint +=(BYTE)pCalPoint->GetCrtCheckPointCount(wRange);
		
		m_aBatchCaliData.aCaliSet.pointData.byValidPointNum = (BYTE)pCalPoint->GetCrtSetPointCount(wRange);
		for(int i=0; i<m_aBatchCaliData.aCaliSet.pointData.byValidPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCaliPoint[i] = FLOAT2LONG(pCalPoint->GetISetPoint(wRange, (WORD)i));
		}
			
		m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum = (BYTE)pCalPoint->GetCrtCheckPointCount(wRange);
		for(int i = 0; i<m_aBatchCaliData.aCaliSet.pointData.byCheckPointNum; i++)
		{
			m_aBatchCaliData.aCaliSet.pointData.lCheckPoint[i] =FLOAT2LONG(pCalPoint->GetICheckPoint(wRange, (WORD)i));
		}
		m_nChargeDisChargeMode = 0;
	}
	else 
	{
		return FALSE;
	}

	return TRUE;
}

//한개의 Range에 대해서 교정이 완료되면 다음에 교정할 정보를 찾아 전송한다.
BOOL CCalibratorDlg::SendNextCalInfo()
{
	int nNextType = 0;
	WORD wNextRange = 0;

	if(FindNextRangeInfo(m_aBatchCaliData.aCaliSet.nType, (WORD)m_aBatchCaliData.aCaliSet.nRange, nNextType, wNextRange) == FALSE)
	{
		//현재 채널에서 더이상 진행할 채널이 없음 
		//다음 채널 교정 명령을 전송함, 현재 채널이 교정 가능한 상태가 아니면 다음 채널에 전송한다.
		m_aBatchCaliData.byChSelIndex++;

		//선택한 모든 채널에 대해서 교정 완료 
		if(m_aBatchCaliData.byChSelIndex >= m_aCaliSelChArray.GetSize())
		{
			return FALSE;
		}

		if(m_nCalMode == CAL_ALL_ITEM_RANGE)
		{
			m_aBatchCaliData.aCaliSet.nType =	CAL_VOLTAGE;
			m_aBatchCaliData.aCaliSet.nRange = CAL_RANGE1;
		}
		else if(m_nCalMode == CAL_ALL_RANGE)
		{
			m_aBatchCaliData.aCaliSet.nRange = CAL_RANGE1;
		}
		else if(m_nCalMode == CAL_SINGLE_RANGE)
		{
			//변화 없음 
		}
	}
	else	//현재 채널에서 다음 교정 구간이 발견되면...
	{
		m_aBatchCaliData.aCaliSet.nType =	nNextType;
		m_aBatchCaliData.aCaliSet.nRange =	wNextRange;
	}
	
	if(UpdateBatchCalPointData(m_aBatchCaliData.aCaliSet.nType, (WORD)m_aBatchCaliData.aCaliSet.nRange))
	{
		//가장 처음 선택된 채널에 시작 명령을 전송하고
		//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
		BOOL	bSended = FALSE;
		while (m_aBatchCaliData.byChSelIndex < m_aCaliSelChArray.GetSize())
		{
			if(SendCalStart(m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)))
			{
				TRACE(Fun_FindMsg("SendNextCalInfo_msg","IDD_CALIBRATION_DLG"), 
					//@ TRACE("M%02dCh%02d 교정 명령 전송 TYPE: %d, RANGE: %d\n", 
						m_aBatchCaliData.nModuleID, 
						m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex),
						m_aBatchCaliData.aCaliSet.nType,
						m_aBatchCaliData.aCaliSet.nRange
					);
				bSended = TRUE;
				break;		//현재 채널에 시작 명령을 정상적으로 보냈을 경우 
			}
			else
			{
				//전송 실패시 다음 채널 선택한다.(교정 가능한 상태가 아니면 Skip)
				m_aBatchCaliData.byChSelIndex++;		//다음 선택 채널로 이동 
				//if(Mode All or Voltage All)
				if(m_nCalMode == CAL_ALL_ITEM_RANGE)
				{
					m_aBatchCaliData.aCaliSet.nType =	CAL_VOLTAGE;
					m_aBatchCaliData.aCaliSet.nRange = CAL_RANGE1;
				}
				else if(m_nCalMode == CAL_ALL_RANGE)
				{
					m_aBatchCaliData.aCaliSet.nRange = CAL_RANGE1;
				}
				else if(m_nCalMode == CAL_SINGLE_RANGE)
				{
					//변화 없음 
				}
			}
		}
		if(bSended == FALSE)	//이후 채널에 시작할 수 있는 채널이 하나도 없을 경우 
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}

//현제 설정된 교정 옵션을 검색하여 다음 교정할 type과 Range를 찾는다.
BOOL CCalibratorDlg::FindNextRangeInfo(int nCurType, WORD wCurRnage, int &nNextType, WORD &wNextRange)
{
	if(m_nCalMode == CAL_SINGLE_RANGE)	return FALSE;	//1구간만 교정 이면 무조건 다음 교정할 구간은 없음 

	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
	if(mi == NULL)	return FALSE;

	int nCount = 0;
	if(nCurType == CAL_VOLTAGE)
	{
		nCount = mi->GetVoltageRangeCount();

#ifdef _DEBUG
		nCount = 4;
#endif // _DEBUG

		if( wCurRnage+1 >= nCount)		//전압의 전 Range의 교정이 끝났으면 
		{
			if(m_nCalMode == CAL_ALL_ITEM_RANGE)	//모든 Item에 대해 교정이면 전류 Range1으로 이동하고 
			{
				//All Range Mode
				nNextType = CAL_CURRENT;
				wNextRange = CAL_RANGE1;
			}
			else //if(m_nCalMode == CAL_ALL_RANGE)	//전압 전 Range에 대한 교정이었으면 완료한다.
			{
				//All Voltage Mode
				return FALSE;
			}
		}
		else
		{
			nNextType = CAL_VOLTAGE;				//전압 다음 Range로 이동 
			wNextRange = WORD(wCurRnage+1);
		}
	}
	else
	{
		nCount = mi->GetCurrentRangeCount();

		if( wCurRnage+1 >= nCount)		//전구간 교정 완료 
		{
			return FALSE;
		}
		else							//전류 다음 Range로 이동 
		{
			nNextType = CAL_CURRENT;
			wNextRange = WORD(wCurRnage+1);
		}
	}

	return TRUE;
}

BOOL CCalibratorDlg::StartCalibration(int nModuleID, int nType, WORD wRange, int /*nMode*/)
{
//	CCyclerModule *mi = m_pDoc->GetModuleInfo(m_nModuleID);
//	if(mi == NULL)	return FALSE;

	//Module ID Update
	m_aBatchCaliData.nModuleID = nModuleID;
	
	//가장 처음 선택된 채널에 시작 명령을 전송하고
	m_aBatchCaliData.byChSelIndex = 0;

	//다채널 선택하여 진행하고 Stop 명령을 지원하기 위해 Timer를 이용하여 한채널씩 전송한다.
	int nItem;
	WORD nChIndex;
	POSITION pos = m_CalChSelList.GetFirstSelectedItemPosition();
	m_aCaliSelChArray.RemoveAll();
	while(pos)
	{
		nItem = m_CalChSelList.GetNextSelectedItem(pos);
		nChIndex = (WORD)m_CalChSelList.GetItemData(nItem);
		m_aCaliSelChArray.Add(nChIndex);
	}
	
	if(m_aCaliSelChArray.GetSize() < 1)	
	{
		return FALSE;
	}
	
	//전압 Range 1부터 시작한다.
	if(UpdateBatchCalPointData(nType, wRange))
	{
		TRACE(Fun_FindMsg("StartCalibration_msg","IDD_CALIBRATION_DLG"), 
			//@ TRACE("M%02dCh%02d 교정 명령 전송 TYPE: %d, RANGE: %d\n", 
				m_aBatchCaliData.nModuleID, 
				m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex),
				nType,
				wRange
			);

		//가장 처음 선택된 채널에 시작 명령을 전송하고
		//완료 명령이 들어 오면 다음 채널 시작 명령을 전송한다.	//SetReceiveResultData()
		if(SendCalStart(m_aCaliSelChArray.GetAt(m_aBatchCaliData.byChSelIndex)) == FALSE)
		{
			return FALSE;
			//첫번째 채널이 전송 가능하지 않을 경우			
		}
	}
	else
	{	
		return FALSE;
	}

	return TRUE;
}

void CCalibratorDlg::OnSelchangeCombo3() 
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_CalChSelList.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_CalChSelList.GetNextSelectedItem(pos);
		int nChIndex = m_CalChSelList.GetItemData(nItem);
		SetReceivedCheckData(m_nModuleID, nChIndex);	
		
		CString strTemp;
		strTemp.Format(Fun_FindMsg("OnSelchangeCombo3_msg","IDD_CALIBRATION_DLG"), m_pDoc->GetModuleName(m_nModuleID), nChIndex+1);
		//@ strTemp.Format("%s Ch%d 교정 선택", m_pDoc->GetModuleName(m_nModuleID), nChIndex+1);
		SetDlgItemText(IDC_CAL_SEL_CH_STATIC, strTemp);
	}	
}

void CCalibratorDlg::OnAccuracyButton() 
{
	// TODO: Add your control notification handler code here
	ASSERT(m_pDoc);
	
	CAccuracySetDlg *pDlg = new CAccuracySetDlg(m_pDoc->GetCalPoint(), this);
	pDlg->DoModal();

	delete pDlg;
	pDlg = NULL;
}

/*afx_msg*/ void CCalibratorDlg::OnChargeRange()
{
	m_nChargeDisChargeMode = 0;
	OnCaliRange1();
}

/*afx_msg*/ void CCalibratorDlg::OnDisChargeRange()
{
	// 
	m_nChargeDisChargeMode = 4;
	OnCaliRange1();
}

/*afx_msg*/ void CCalibratorDlg::OnChargeRange2()
{
	m_nChargeDisChargeMode = 0;
	OnCaliRange2();
}

/*afx_msg*/ void CCalibratorDlg::OnDisChargeRange2()
{
	// 
	m_nChargeDisChargeMode = 4;
	OnCaliRange2();
}

/*afx_msg*/ void CCalibratorDlg::OnChargeRange3()
{
	m_nChargeDisChargeMode = 0;	
	OnCaliRange3();
}

/*afx_msg*/ void CCalibratorDlg::OnDisChargeRange3()
{
	// 
	m_nChargeDisChargeMode = 4;
	OnCaliRange3();
}

/*afx_msg*/ void CCalibratorDlg::OnChargeRange4()
{
	m_nChargeDisChargeMode = 0;
	OnCaliRange4();
}

/*afx_msg*/ void CCalibratorDlg::OnDisChargeRange4()
{
	// 
	m_nChargeDisChargeMode = 4;
	OnCaliRange4();
}

//ksj 20200819: 자동 교정 기능 이식 from V100D
BOOL CCalibratorDlg::Fun_CalibrationStart(int nNewModuleID)
{
	// 	m_nModuleID = nNewModuleID;
	// 	CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	// 	if(pMD == NULL)		return;

	SFT_DAQ_ISOLATION sDaqIsolation;
	ZeroMemory(&sDaqIsolation,sizeof(SFT_DAQ_ISOLATION));
	sDaqIsolation.bISO = 1;

	int nModuleID = nNewModuleID;
	BOOL bFlag = m_pDoc->SetCalibrationStartEnd(nModuleID,&sDaqIsolation);

	if (bFlag == FALSE)
	{
		CString strMsg;
		strMsg.Format("Calibration START command send Fail... (%s)", m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID));
		m_pDoc->WriteSysLog(strMsg);
	}
	return bFlag;
}

//ksj 20200819: 자동 교정 기능 이식 from V100D
BOOL CCalibratorDlg::Fun_CalibrationEnd(int nNewModuleID)
{
	// 	m_nModuleID = nNewModuleID;
	// 	CCyclerModule *pMD = m_pDoc->GetModuleInfo(m_nModuleID);
	// 	if(pMD == NULL)		return;

	SFT_DAQ_ISOLATION sDaqIsolation;
	ZeroMemory(&sDaqIsolation,sizeof(SFT_DAQ_ISOLATION));
	sDaqIsolation.bISO = 0;

	int nModuleID = nNewModuleID;
	BOOL bFlag = m_pDoc->SetCalibrationStartEnd(nModuleID,&sDaqIsolation);

	if (bFlag == FALSE)
	{
		CString strMsg;
		strMsg.Format("Calibration END command send fail... (%s)", m_pDoc->GetModuleName(m_aBatchCaliData.nModuleID));
		m_pDoc->WriteSysLog(strMsg);
	}
	return bFlag;
}