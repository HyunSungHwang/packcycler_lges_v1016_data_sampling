// UnitComLogDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "UnitComLogDlg.h"
#include "FolderDialog.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnitComLogDlg dialog


CUnitComLogDlg::CUnitComLogDlg(CString strIpAddress, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CUnitComLogDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CUnitComLogDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CUnitComLogDlg::IDD3):
	(CUnitComLogDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CUnitComLogDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_strIpAddress = strIpAddress;

}


void CUnitComLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUnitComLogDlg)
	DDX_Control(pDX, IDC_SEL_LIST, m_wndSelList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUnitComLogDlg, CDialog)
	//{{AFX_MSG_MAP(CUnitComLogDlg)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	ON_NOTIFY(NM_DBLCLK, IDC_SEL_LIST, OnDblclkSelList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUnitComLogDlg message handlers

BOOL CUnitComLogDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	DWORD style = 	m_wndSelList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_wndSelList.SetExtendedStyle(style );
	
	m_wndSelList.InsertColumn(0, "No", LVCFMT_LEFT, 50);
	m_wndSelList.InsertColumn(1, "File name", LVCFMT_LEFT, 350);
	m_wndSelList.InsertColumn(2, "Size", LVCFMT_LEFT, 100);
	
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	char szName[32];
	
	//원격 접속한다.
	
	CString strLoginID = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Login ID", "root");
	CString strPassword = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Login Password", "dusrnth");
	CString strLocation = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Log Location", "cycler_data/log");
	
	CWaitCursor wait;
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;
	
	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(m_strIpAddress, strLoginID, strPassword);
		if(!pFtpConnection->SetCurrentDirectory(strLocation))
		{
			AfxMessageBox(Fun_FindMsg("OnInitDialog_msg","IDD_SEL_UNIT_LOG_DIALOG"));
			//@ AfxMessageBox("지정 경로를 찾을 수 없습니다.");
		}
		else
		{
			int nI = 0;
			int nSize = 0;
			CString strFile;
			CFtpFileFind pFileFind(pFtpConnection);
			BOOL bWorking = pFileFind.FindFile(_T("*"));
			while (bWorking)
			{
				bWorking = pFileFind.FindNextFile();
				strFile = pFileFind.GetFileName();
				nSize	= pFileFind.GetLength();
				TRACE("%s\n", strFile);
				
				sprintf(szName, "%d", nI+1);
				lvItem.iItem = nI;
				lvItem.iSubItem = 0;
				lvItem.pszText = szName;
				m_wndSelList.InsertItem(&lvItem);
				m_wndSelList.SetItemData(lvItem.iItem, nI);		//==>LVN_ITEMCHANGED 를 발생 기킴 
				m_wndSelList.SetItemText(nI, 1, strFile);
				nSize = nSize/1000.0f;
				if(nSize < 1)	nSize = 1;
				strFile.Format("%dKB", nSize);
				m_wndSelList.SetItemText(nI, 2, strFile);
				nI++;
			}
			pFileFind.Close();
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
	}
	if(pFtpConnection)	pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUnitComLogDlg::OnButtonSave() 
{
	CFolderDialog dlg;
	CString strDir;
	if( dlg.DoModal() == IDOK)
	{
		strDir = dlg.GetPathName();
	}
	else 
	{
		return;
	}
	
	//Download file to temp. folder
	CString strLoginID = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Login ID", "root");
	CString strPassword = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Login Password", "dusrnth");
	CString strLocation = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Log Location", "cycler_data/log");
	
	int nCnt = 0;
	CWaitCursor wait;
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;
	
	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(m_strIpAddress, strLoginID, strPassword);
		if(!pFtpConnection->SetCurrentDirectory(strLocation))
		{
			AfxMessageBox(Fun_FindMsg("OnButtonSave_msg","IDD_SEL_UNIT_LOG_DIALOG"));
			//@ AfxMessageBox("지정 경로를 찾을 수 없습니다.");
		}
		else
		{
			POSITION pos = m_wndSelList.GetFirstSelectedItemPosition();
			while(pos)
			{
				int nItem = m_wndSelList.GetNextSelectedItem(pos);
				CString strSelFileName = m_wndSelList.GetItemText(nItem, 1);
				CString toFileName;
				toFileName.Format("%s\\%s", strDir, strSelFileName);
				if(pFtpConnection->GetFile(strSelFileName, toFileName, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_ASCII) == FALSE)
				{
					MessageBox(strSelFileName +" download fail!!!", "Error", MB_OK|MB_ICONSTOP);
				}
				nCnt++;
			}
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
	}
	if(pFtpConnection) pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;
	
	if(nCnt > 0)
	{
		CString strTemp;
		strTemp.Format("Total %d file download complite.", nCnt);
		AfxMessageBox(strTemp);
	}	
}

void CUnitComLogDlg::OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_wndSelList.GetFirstSelectedItemPosition();
	if(pos == NULL)	return;
	int nItem = m_wndSelList.GetNextSelectedItem(pos);
	CString strSelFileName = m_wndSelList.GetItemText(nItem, 1);
	
	//Download file to temp. folder
	CString strLoginID = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Login ID", "root");
	CString strPassword = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Login Password", "dusrnth");
	CString strLocation = AfxGetApp()->GetProfileString(CT_CONFIG_REG_FTP_SEC, "Log Location", "cycler_data/log");
	
	CWaitCursor wait;
	CInternetSession ftpSession;
	CFtpConnection *pFtpConnection = NULL;
	
	try
	{
		pFtpConnection = ftpSession.GetFtpConnection(m_strIpAddress, strLoginID, strPassword);
		if(!pFtpConnection->SetCurrentDirectory(strLocation))
		{
			AfxMessageBox(Fun_FindMsg("OnDblclkSelList_msg1","IDD_SEL_UNIT_LOG_DIALOG"));
			//@ AfxMessageBox("지정 경로를 찾을 수 없습니다.");
		}
		else
		{
			CString toFileName;
			toFileName.Format("%s\\Temp\\Unitlog.txt", AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path"));
			if(pFtpConnection->GetFile(strSelFileName, toFileName, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_ASCII) == FALSE)
			{
				MessageBox(strSelFileName +" download fail!!!", "Error", MB_OK|MB_ICONSTOP);
			}
			else
			{
				//Execute notepad
				STARTUPINFO	stStartUpInfo;
				PROCESS_INFORMATION	ProcessInfo;
				ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
				ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
				
				stStartUpInfo.cb = sizeof(STARTUPINFO);
				stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
				stStartUpInfo.wShowWindow = SW_SHOWNORMAL;
				
				char szWinDir[256];
				GetWindowsDirectory(szWinDir, 255);
				CString strTemp;
				strTemp.Format("%s\\Notepad.exe %s", szWinDir, toFileName);
				
				BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
				if(bFlag == FALSE)
				{
					strTemp.Format(Fun_FindMsg("OnDblclkSelList_msg2","IDD_SEL_UNIT_LOG_DIALOG"), toFileName);
					//@ strTemp.Format("%s 를 찾을 수 없습니다.", toFileName);
					MessageBox(strTemp, "File not found", MB_OK|MB_ICONSTOP);
				}	
			}
		}
	}
	catch(CInternetException *e)
	{
		TCHAR sz[1024];
		e->GetErrorMessage(sz, 1024);
		AfxMessageBox(sz);
		e->Delete();
	}
	if(pFtpConnection) pFtpConnection->Close();
	ftpSession.Close();
	delete pFtpConnection;
		
	*pResult = 0;
}