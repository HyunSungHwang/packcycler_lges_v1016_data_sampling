#if !defined(AFX_DISPLAYITEMEDITDLG_H__320688AB_1B48_403A_BFA4_11F1DA2AC558__INCLUDED_)
#define AFX_DISPLAYITEMEDITDLG_H__320688AB_1B48_403A_BFA4_11F1DA2AC558__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DisplayItemEditDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDisplayItemEditDlg dialog

class CDisplayItemEditDlg : public CDialog
{
// Construction
public:
	BOOL m_nEditMode;
	CString m_strRegistedItem;
	int m_nItemNo;
	CDisplayItemEditDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDisplayItemEditDlg)
	enum { IDD = IDD_DSP_ITEM_EDIT_DLG , IDD2 = IDD_DSP_ITEM_EDIT_DLG_ENG , IDD3 = IDD_DSP_ITEM_EDIT_DLG_PL };
	CComboBox	m_ctrlItemCombo;
	CString	m_strTitle;
	UINT	m_nSortNo;
	UINT	m_nWidth;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDisplayItemEditDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDisplayItemEditDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeDataItemCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DISPLAYITEMEDITDLG_H__320688AB_1B48_403A_BFA4_11F1DA2AC558__INCLUDED_)
