#if !defined(AFX_SHOWEMGDLG_H__CC746CF1_BA76_4F48_A0D1_12386E72BB7F__INCLUDED_)
#define AFX_SHOWEMGDLG_H__CC746CF1_BA76_4F48_A0D1_12386E72BB7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShowEmgDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CShowEmgDlg dialog

#include "CTSMonProView.h"
class CCTSMonProView;

class CShowEmgDlg : public CDialog
{
// Construction
public:
	
	CCTSMonProView* m_pView;
	CString m_strEmgMsg;
	CString strEmgValue;
	CString strMsg;
	void SetEmgData(CString strModuleName, CString strCodeMsg, long lValue, CString strCanName);	
	void SetWarnData(CString strTitle,CString strMessage);

	CShowEmgDlg(CCTSMonProView* pView = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CShowEmgDlg)
	enum { IDD = IDD_SHOW_EMG_DLG , IDD2 = IDD_SHOW_EMG_DLG_ENG , IDD3 = IDD_SHOW_EMG_DLG_PL };
	CLabel	m_ctrlEmgValue;
	CLabel	m_ctrlMsgLabel;
	CLabel	m_ctrlEmgLabel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShowEmgDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CShowEmgDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHOWEMGDLG_H__CC746CF1_BA76_4F48_A0D1_12386E72BB7F__INCLUDED_)
