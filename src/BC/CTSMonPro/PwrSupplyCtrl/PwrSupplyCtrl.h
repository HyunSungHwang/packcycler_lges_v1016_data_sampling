// PwrSupplyCtrl.h: interface for the CPwrSupplyCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PWRSUPPLYCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
#define AFX_PWRSUPPLYCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_

#include "../serialconfigdlg.h"
#include "../SerialPort.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPwrSupplyCtrl  
{
public:
	CPwrSupplyCtrl();
	virtual ~CPwrSupplyCtrl();
	
	SFT_PWRSUPPLY_VALUE m_pPwrSupplyData[MAX_POWER_CH_COUNT];

	CSerialPort	m_SerialPort;

	int    m_nItem;
	int    m_iError;
	int    m_iState_Inst;
	int    m_iState_Outp;
	int    m_iState_Outp_Ch[MAX_POWER_CH_COUNT]; //ksj 20201012 : 채널별 상태 따로 저장하도록 추가.
	float  m_fState_Volt[MAX_POWER_CH_COUNT];
	
	CWnd   *m_pWnd;
	HANDLE m_hThread;
	
	CStringArray m_PwrCmdList;

	long   m_iEmptyCount;
	int    m_iTpGetState;

	void onlogsave(CString strtype,CString strlog,int iCHNO);
	void onResetData();
	void onSetInit(int nItem,CWnd *pWnd);
	BOOL onSetPortSerialPort();
	BOOL onInitSerialPort(SERIAL_CONFIG &config);

	BOOL onSerialPortWrite(CString strData,BOOL bMesg=TRUE);
	BOOL onSerialPortWriteBuffer(CString strData);

	int  onPwrDeal(int iType,int nItem);
	int  onPwrDealSub(int nItem,CString strData,BOOL bLog=TRUE);
	void onPwrCmdRemove();

	void    onPwrGetState();
	CString onPwrRecvWait(int nTime=0);
	int     onPwrWaitTime();

	static DWORD WINAPI PwrDealThread(LPVOID arg);

protected:

};

#endif // !defined(AFX_PWRSUPPLYCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
