// PwrSupplyCtrl.cpp: implementation of the CPwrSupplyCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "../CTSMonPro.h"
#include "PwrSupplyCtrl.h"

#include "../MainFrm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPwrSupplyCtrl::CPwrSupplyCtrl()
{	
	m_hThread=NULL;
	m_pWnd=NULL;

	m_nItem=-1;
	m_iError=0;

	m_iEmptyCount=0;
	m_iTpGetState=0;

	m_iState_Inst=0;
	m_iState_Outp=-1;
	memset(m_fState_Volt,0,sizeof(m_fState_Volt));
	memset(m_pPwrSupplyData,0,sizeof(m_pPwrSupplyData));
}

CPwrSupplyCtrl::~CPwrSupplyCtrl()
{
	GWin::win_ThreadEnd2(m_hThread);
}

void CPwrSupplyCtrl::onResetData()
{	
	m_nItem=-1;
	m_iError=0;	
	
	m_iState_Inst=0;
	m_iState_Outp=-1;
	memset(m_fState_Volt,0,sizeof(m_fState_Volt));
	memset(m_iState_Outp_Ch,-1,sizeof(m_iState_Outp_Ch));//ksj 20201012
}

void CPwrSupplyCtrl::onSetInit(int nItem,CWnd *pWnd)
{	
	m_nItem=nItem;//0 start
	m_pWnd=pWnd;
	m_iError=0;

	if(!m_hThread)
	{
		DWORD hThreadId=0;
		m_hThread = CreateThread(NULL, 0, PwrDealThread,
			                     (LPVOID)this, 0, &hThreadId);	
	}
}

BOOL CPwrSupplyCtrl::onSetPortSerialPort()
{
	if(!mainFrm) return FALSE;
	
	if(m_SerialPort.IsPortOpen() == TRUE)
	{
		m_SerialPort.DisConnect();
	}
	if(m_SerialPort.IsPortOpen() == FALSE)
	{
		CString strKey;
		strKey.Format(_T("SerialPower%d"),m_nItem+1);
		
		if(AfxGetApp()->GetProfileInt(strKey, "Use", 0)==1)
		{
			CSerialConfigDlg dlg;
			dlg.onLoadConfig();
			onInitSerialPort(dlg.m_Serial_Power[m_nItem]);
		}
	}
	return m_SerialPort.IsPortOpen();
}

BOOL CPwrSupplyCtrl::onInitSerialPort(SERIAL_CONFIG &config)
{
	CString strTemp;

	if(m_SerialPort.IsPortOpen())
	{
		//strTemp.Format("COM %d 포트는 이미 연결 되어 있습니다.", config.nPortNum);
		strTemp.Format(Fun_FindMsg("PwrSupplyCtrl_onInitSerialPort_msg1","PWRSUPPLYCTRL"), config.nPortNum);//&&
		//AfxMessageBox(strTemp);
		onlogsave(_T("state"),strTemp,config.nPortNum); //lyj 20210809
		return FALSE;
	}
	BOOL flag = m_SerialPort.InitPort( m_pWnd->m_hWnd,
										config.nPortNum, 
										config.nPortBaudRate,
										config.portParity,
										config.nPortDataBits,
										config.nPortStopBits,
										config.nPortEvents,
										config.nPortBuffer );
	if( flag == FALSE )
	{
		//strTemp.Format("COM %d 포트 초기화를 실패하였습니다!", config.nPortNum);
		strTemp.Format(Fun_FindMsg("PwrSupplyCtrl_onInitSerialPort_msg2","PWRSUPPLYCTRL"), config.nPortNum);//&&
		//AfxMessageBox(strTemp); 
		onlogsave(_T("state"),strTemp,config.nPortNum); //lyj 20210809
		return FALSE;
	}
	m_SerialPort.StartMonitoring();
	m_SerialPort.m_bRecv=FALSE;

	m_PwrCmdList.Add(_T("OUTP?,"));
	m_PwrCmdList.Add(_T("INST?,"));
	m_PwrCmdList.Add(_T("VOLT?,"));
	return flag;
}

BOOL CPwrSupplyCtrl::onSerialPortWrite(CString strData,BOOL bMesg)
{//cny 201809
	//-----------------------------------------
	//int nItem=m_nItem;
	//-----------------------------------------

	CString strTemp;
	if(m_SerialPort.IsPortOpen() == FALSE)
	{			
		strTemp.Format(_T("Port Disconnected"));	
		if(bMesg==TRUE)
		{
			AfxMessageBox(strTemp);
		}
		return FALSE;
	}

	int nCount=strData.GetLength()+5;
	char *cSendPcbComm=new char[nCount];
	memset(cSendPcbComm,0,nCount);
		
	int nCol=0;
	cSendPcbComm[nCol++]='O';
	cSendPcbComm[nCol++]='D';
	cSendPcbComm[nCol++]='A';
	cSendPcbComm[nCol++]=0x01;

	int i=0;
	for(int i = 0;i<strData.GetLength();i++)
	{
		cSendPcbComm[nCol++] = (BYTE)strData[i];
	}
	cSendPcbComm[nCol++] = 0x0A;

	m_SerialPort.WriteToPort(cSendPcbComm,nCol);
	delete cSendPcbComm;
	return TRUE;
}

void CPwrSupplyCtrl::onlogsave(CString strtype,CString strlog,int iCHNO)
{
	CString stype;
	stype.Format(_T("power_%d"),m_nItem);
	if(iCHNO>0)
	{
		stype.Format(_T("power_%d_%d"),m_nItem,iCHNO);
	}	
	log_All(stype,strtype,strlog);
}

BOOL CPwrSupplyCtrl::onSerialPortWriteBuffer(CString strData)
{//cny 201809
	//-----------------------------------------
	int nItem=m_nItem;
	//-----------------------------------------

	CString strTemp;
	if(m_SerialPort.IsPortOpen() == FALSE)
	{
		return FALSE;
	}
	CString str1=GStr::str_GetSplit(strData,0,',');

	int nCHNO=0;
	if(str1.Find(_T("CHNO_"))>-1)
	{
		nCHNO=_ttoi(GStr::str_GetSplit(str1,1,'_'));
	}
	
	strTemp.Format(_T("CHNO:%d power:%d cmd:%s bufcount:%d"),
		            nCHNO,nItem,strData,m_PwrCmdList.GetSize());
	onlogsave(_T("add"),strTemp,nCHNO);

	m_PwrCmdList.Add(strData);
	return TRUE;
}

int CPwrSupplyCtrl::onPwrWaitTime()
{
	int nTime=g_AppInfo.iPwrSupply_CmdWaitTime;//200msec
	if(nTime<100)
	{
		nTime=200;
	}
	return nTime;
}

DWORD WINAPI CPwrSupplyCtrl::PwrDealThread(LPVOID arg)
{//cny 201809
	CPwrSupplyCtrl *obj=(CPwrSupplyCtrl *)arg;
	if(!obj) return 0;
	if(!mainFrm) return 0;
	
	Sleep(1000);
	
	CString strTemp;
	int  nTime=obj->onPwrWaitTime();
	int  nRet=0;
    int  nItem=obj->m_nItem;
	
	strTemp.Format(_T("Power%d Thread Start"),nItem);
	obj->onlogsave(_T("thread"),strTemp,0);

	while(1)
	{
		if(g_AppInfo.iEnd==1) break;
		nTime=obj->onPwrWaitTime();
		//---------------------------------
		if(obj->m_SerialPort.IsPortOpen())
		{
			nItem=obj->m_nItem;
			nRet=obj->onPwrDeal(1,nItem);//CMD Deal
		}
		//---------------------------------
		Sleep(nTime);
	}
	
	strTemp.Format(_T("Power%d Thread End"),nItem);
	obj->onlogsave(_T("thread"),strTemp,0);
	return 1;
}

int CPwrSupplyCtrl::onPwrDeal(int iType,int nItem)
{//cny 201809
	if(iType==0)
	{
		onSerialPortWriteBuffer(_T("OUTP OFF,OUTP?,"));
	}
	else if(iType==1)
	{
		if(nItem<0)  return -1;
		if(!mainFrm) return -1;
		if(nItem>=MAX_POWER_COUNT) return -1;
		
		int nCount=m_PwrCmdList.GetSize();
		if(nCount<1)
		{
			onPwrGetState();
			return -1;
		}
        //-------------------------------
		m_iEmptyCount=0;
		//-------------------------------
		CString strData=m_PwrCmdList.GetAt(0);
		if(strData.IsEmpty() || strData==_T(""))
		{
			onPwrCmdRemove();
			return 1;
		}
		if(onPwrDealSub(nItem,strData)==1)
		{
			onPwrCmdRemove();
			return 1;
		}
	}
	return 1;
}

void CPwrSupplyCtrl::onPwrCmdRemove()
{
	if(m_PwrCmdList.GetSize()<1) return;
	
    m_PwrCmdList.RemoveAt(0);
}

int CPwrSupplyCtrl::onPwrDealSub(int nItem,CString strData,BOOL bLog)
{
	float fVolt=0;
	int nSplitCount=GStr::str_Count(strData,_T(","))+1;
	CString strTemp;

	if(nSplitCount<1) return 1;

	int nDelay=0;
	int nCHNO=0;
	for(int i=0;i<nSplitCount;i++)
	{
		nDelay=0;
		CString strSend=GStr::str_GetSplit(strData,i,',');
		if(strSend.Find(_T("CHNO_"))>-1)
		{
			nCHNO=_ttoi(GStr::str_GetSplit(strSend,1,'_'));
			continue;
		}
		if(strSend.IsEmpty() || strSend==_T("")) continue;
		if(strSend==_T("OUTP ON")  && m_iState_Outp==1)
		{
			strTemp.Format(_T("CHNO:%d power:%d send:%s error:%d OUTP:%d"),
							nCHNO,
							nItem,
							strSend,
							m_iError,
							m_iState_Outp);
			if(bLog==TRUE)
			{
				onlogsave(_T("state"),strTemp,nCHNO);
			}
			continue;
		}
		if(strSend==_T("OUTP OFF") && m_iState_Outp==0)
		{			
			strTemp.Format(_T("CHNO:%d power:%d send:%s error:%d OUTP:%d"),
							nCHNO,
							nItem,
							strSend,
							m_iError,
							m_iState_Outp);
			if(bLog==TRUE)
			{
				onlogsave(_T("state"),strTemp,nCHNO);
			}
			continue;
		}
		if(strSend.Find(_T("INST OUTP"))>-1)
		{			
			strTemp=strSend;
			strTemp.Replace(_T("INST OUTP"),_T(""));
			if(m_iState_Inst==atoi(strTemp))
			{
				strTemp.Format(_T("CHNO:%d power:%d send:%s error:%d INST:%d"),
								nCHNO,
								nItem,
								strSend,
								m_iError,
								m_iState_Inst);
				if(bLog==TRUE)
				{
					onlogsave(_T("state"),strTemp,nCHNO);
				}
				continue;
			}
		}
		if(onSerialPortWrite(strSend,FALSE)==FALSE)
		{
			strTemp.Format(_T("CHNO:%d power:%d send:%s error:%d connect false"),
					       nCHNO,
						   nItem,
						   strSend,
						   m_iError);
			if(bLog==TRUE)
			{
				onlogsave(_T("state"),strTemp,nCHNO);
			}
			break;
		}
		//---------------------------
		int nQindex=strSend.Find(_T("?"));
		if(nQindex<0)
		{
			nDelay=onPwrWaitTime();
		}
		//---------------------------
		CString strRecv=onPwrRecvWait(nDelay);
		//---------------------------
		if(nQindex>-1)
		{
			if(strRecv.IsEmpty() || strRecv==_T(""))
			{
				m_iError++;

				strTemp.Format(_T("CHNO:%d power:%d send:%s recv:%s Qindex:%d error:%d"),
								nCHNO,
								nItem,
								strSend,
								strRecv,
								nQindex,
								m_iError);
				if(bLog==TRUE)
				{
					onlogsave(_T("recv"),strTemp,nCHNO);
				}
			}
			else
			{
				m_iError=0;

				if(strSend.Find(_T("INST"))>-1)
				{
					strTemp=strRecv;
					strTemp.Replace(_T("OUTP"),_T(""));
					m_iState_Inst=atoi(strTemp);	
				}
				if(strSend.Find(_T("OUTP"))>-1)
				{
					m_iState_Outp=atoi(strRecv);

					//ksj 20201012 : 파워 채널별로 상태 따로 갖도록 추가
					if(m_iState_Inst>=1 && m_iState_Inst<=MAX_POWER_CH_COUNT)
					{
						m_iState_Outp_Ch[m_iState_Inst-1]=m_iState_Outp;
					}
					//ksj end					
				}
				if(strSend.Find(_T("VOLT"))>-1)
				{
					fVolt=atof(strRecv);
					if(m_iState_Inst>=1 && m_iState_Inst<=MAX_POWER_CH_COUNT)
					{
						m_fState_Volt[m_iState_Inst-1]=fVolt;
					}
				}
			}
		}
		strTemp.Format(_T("CHNO:%d power:%d send:%s recv:%s Qindex:%d error:%d INST:%d OUTP:%d Volt:%f"),
					    nCHNO,
						nItem,
					    strSend,
					    strRecv,
						nQindex,
						m_iError,
						m_iState_Inst,
						m_iState_Outp,
						fVolt);
		if(bLog==TRUE)
		{
			onlogsave(_T("recv"),strTemp,nCHNO);
		}		
	}
	return 1;
}

CString CPwrSupplyCtrl::onPwrRecvWait(int nTime)
{
	if(nTime>0)
	{
		WaitForSingleObject(m_hThread,nTime);
		return _T("");
	}	
	//1000msec WaitTime
	int nCount=0;
	CString strValue;
	while(nCount<=10)
	{
		if(m_SerialPort.GetRxStr(strValue,0x0A)==TRUE)
		{
			return strValue;
		}
		nCount++;
		WaitForSingleObject(m_hThread,100);
	}
	return _T("");
}

void CPwrSupplyCtrl::onPwrGetState()
{	
	m_iEmptyCount++;
	if(m_iEmptyCount<10) return;
    m_iEmptyCount=0;
    m_iTpGetState++;
	
	int  nItem=m_nItem;
	
	CString strData=_T("");
	if(m_iTpGetState==1)
	{
		int iInst=((m_iState_Inst==1)?2:1);	
        strData.Format(_T("INST OUTP%d,INST?,"),iInst);
	}
	else if(m_iTpGetState==2)
	{
		strData=_T("OUTP?,");
	}
	else if(m_iTpGetState>=3)
	{
		strData=_T("VOLT?,");
		m_iTpGetState=0;
	}

	onPwrDealSub(nItem,strData,FALSE);	
}
