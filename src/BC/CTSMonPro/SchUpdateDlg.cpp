// SchUpdateDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "CTSMonProView.h"
#include "SchUpdateDlg.h"
#include "CTSMonProDoc.h"
#include "WorkWarnin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSchUpdateDlg dialog

class CCTSMonProDoc;

CSchUpdateDlg::CSchUpdateDlg(int nModuleID, int nChIndex,CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSchUpdateDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSchUpdateDlg::IDD2):	
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSchUpdateDlg::IDD3):	
	(CSchUpdateDlg::IDD), pParent)
// 	, m_chk_step_delta_aux_v(false)
// 	, m_chk_step_delta_aux_temp(false)
// 	, m_chk_step_delta_aux_th(false)
// 	, m_chk_step_delta_aux_t(false)
// 	, m_chk_step_aux_v(false)
, m_bChk_step_delta_aux_v(FALSE)
, m_bChk_step_delta_aux_temp(FALSE)
, m_bChk_step_delta_aux_th(FALSE)
, m_bChk_step_delta_aux_t(FALSE)
, m_bChk_step_aux_v(FALSE)
{
	//{{AFX_DATA_INIT(CSchUpdateDlg)
	m_edit_step_no = 0;
	m_edit_step_mode = 0;
	m_edit_step_ref_I = _T("");
	m_edit_step_P = _T("");
	m_edit_step_type = 0;
	m_edit_step_end_wh = _T("");
	m_edit_step_end_volt_min = _T("");
	m_edit_step_end_volt_max = _T("");
	m_edit_step_end_time = 0;
	m_edit_step_end_power = _T("");
	m_edit_step_end_Ah = _T("");
	m_edit_step_ref_d_volt = _T("");
	m_edit_step_ref_c_volt = _T("");
	m_edit_step_end_I = _T("");
	m_edit_step_end_time_day = 0;
	m_edit_safety_c_high = _T("");
	m_edit_safety_c_low = _T("");
	m_edit_safety_i_high = _T("");
	m_edit_safety_i_low = _T("");
	m_edit_safety_z_high = _T("");
	m_edit_safety_z_low = _T("");
	m_edit_safety_v_high = _T("");
	m_edit_safety_v_low = _T("");
	m_edit_record_delta_i = _T("");
	m_edit_record_delta_v = _T("");
	m_edit_record_time_ms = 0;
	m_edit_step_end_time_ms = 0;
//	m_edit_safety_delta_v_step = _T("");
	m_edit_cell_delta_v_step=_T("");	//lyj 20200214
	m_edit_step_auxt_delta=_T("");	//lyj 20200214
	m_edit_step_auxth_delta=_T("");	//lyj 20200214
	m_edit_step_auxt_auxth_delta=_T("");	//lyj 20200214
	m_edit_step_auxv_time=_T("");	//lyj 20200214
	m_edit_step_auxv_delta=_T("");	//lyj 20200214
	//}}AFX_DATA_INIT

	ASSERT(pDoc);
	m_pDoc = pDoc;
	m_nModuleID = nModuleID;
	m_nChIndex = nChIndex;

	
	m_pCH = pDoc->GetChannelInfo(nModuleID, nChIndex);
}


void CSchUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSchUpdateDlg)
	DDX_Control(pDX, IDC_UPDATE_STEP_LIST, m_ctrlList);
	DDX_Control(pDX, IDC_DATETIMEPICKER_RECORD_TIME, m_ctrlRecordTime);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_ctrlEndTime);
	DDX_Control(pDX, IDC_COMBO_STEP_MODE, m_combo_step_mode);
	DDX_Control(pDX, IDC_COMBO_STEP_TYPE, m_combo_step_type);
	DDX_Text(pDX, IDC_EDIT_STEP_NO, m_edit_step_no);
	DDX_Text(pDX, IDC_EDIT_STEP_MODE, m_edit_step_mode);
	DDX_Text(pDX, IDC_EDIT_STEP_REF_I, m_edit_step_ref_I);
	DDX_Text(pDX, IDC_EDIT_STEP_REF_P, m_edit_step_P);
	DDX_Text(pDX, IDC_EDIT_STEP_TYPE, m_edit_step_type);
	DDX_Text(pDX, IDC_EDIT_STEP_END_WH, m_edit_step_end_wh);
	DDX_Text(pDX, IDC_EDIT_STEP_END_VOLT_L, m_edit_step_end_volt_min);
	DDX_Text(pDX, IDC_EDIT_STEP_END_VOLT_H, m_edit_step_end_volt_max);
	DDX_Text(pDX, IDC_EDIT_STEP_END_TIME, m_edit_step_end_time);
	DDX_Text(pDX, IDC_EDIT_STEP_END_P, m_edit_step_end_power);
	DDX_Text(pDX, IDC_EDIT_STEP_END_AH, m_edit_step_end_Ah);
	DDX_Text(pDX, IDC_EDIT_STEP_D_VOLT, m_edit_step_ref_d_volt);
	DDX_Text(pDX, IDC_EDIT_STEP_C_VOLT, m_edit_step_ref_c_volt);
	DDX_Text(pDX, IDC_EDIT_STEP_END_I, m_edit_step_end_I);
	DDX_Text(pDX, IDC_EDIT_STEP_END_TIME_DAY, m_edit_step_end_time_day);
	DDX_Text(pDX, IDC_EDIT_SAFETY_C_HIGH, m_edit_safety_c_high);
	DDX_Text(pDX, IDC_EDIT_SAFETY_C_LOW, m_edit_safety_c_low);
	DDX_Text(pDX, IDC_EDIT_SAFETY_I_HIGH, m_edit_safety_i_high);
	DDX_Text(pDX, IDC_EDIT_SAFETY_I_LOW, m_edit_safety_i_low);
	DDX_Text(pDX, IDC_EDIT_SAFETY_IMP_HIGH, m_edit_safety_z_high);
	DDX_Text(pDX, IDC_EDIT_SAFETY_IMP_LOW, m_edit_safety_z_low);
	DDX_Text(pDX, IDC_EDIT_SAFETY_V_HIGH, m_edit_safety_v_high);
	DDX_Text(pDX, IDC_EDIT_SAFETY_V_LOW, m_edit_safety_v_low);
	DDX_Text(pDX, IDC_EDIT_RECORD_DELTA_A, m_edit_record_delta_i);
	DDX_Text(pDX, IDC_EDIT_RECORD_DELTA_V, m_edit_record_delta_v);
	DDX_Text(pDX, IDC_EDIT_RECORD_TIME_MS, m_edit_record_time_ms);
	DDX_Text(pDX, IDC_EDIT_CELL_DELTA_V_STEP, m_edit_cell_delta_v_step); //lyj 20200214 
	DDX_Text(pDX, IDC_EDIT_STEP_AUXT_DELTA, m_edit_step_auxt_delta);	 //lyj 20200214 
	DDX_Text(pDX, IDC_EDIT_STEP_AUXTH_DELTA, m_edit_step_auxth_delta);	 //lyj 20200214 
	DDX_Text(pDX, IDC_EDIT_STEP_AUXT_AUXTH_DELTA, m_edit_step_auxt_auxth_delta);	 //lyj 20200214 
	DDX_Text(pDX, IDC_EDIT_STEP_AUXV_TIME, m_edit_step_auxv_time);	 //lyj 20200214 
	DDX_Text(pDX, IDC_EDIT_STEP_AUXV_DELTA, m_edit_step_auxv_delta);	 //lyj 20200214 
	// 	DDX_Control(pDX ,IDC_CHK_STEP_DELTA_AUX_V, m_chk_step_delta_aux_v);
	// 	DDX_Control(pDX ,IDC_CHK_STEP_DELTA_AUX_TEMP, m_chk_step_delta_aux_temp);
	// 	DDX_Control(pDX ,IDC_CHK_STEP_DELTA_AUX_TH, m_chk_step_delta_aux_th);
	// 	DDX_Control(pDX ,IDC_CHK_STEP_DELTA_AUX_T, m_chk_step_delta_aux_t);
	// 	DDX_Control(pDX ,IDC_CHK_STEP_AUX_V, m_chk_step_aux_v);


	DDV_MinMaxInt(pDX, m_edit_record_time_ms, 0, 999);
	DDX_Text(pDX, IDC_EDIT_MS, m_edit_step_end_time_ms);
	DDV_MinMaxUInt(pDX, m_edit_step_end_time_ms, 0, 999);
	//	DDX_Text(pDX, IDC_EDIT_SAFETY_CELLDETA_V_STEP, m_edit_safety_delta_v_step);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHK_STEP_DELTA_AUX_V, m_bChk_step_delta_aux_v);
	DDX_Check(pDX, IDC_CHK_STEP_DELTA_AUX_TEMP, m_bChk_step_delta_aux_temp);
	DDX_Check(pDX, IDC_CHK_STEP_DELTA_AUX_TH, m_bChk_step_delta_aux_th);
	DDX_Check(pDX, IDC_CHK_STEP_DELTA_AUX_T, m_bChk_step_delta_aux_t);
	DDX_Check(pDX, IDC_CHK_STEP_AUX_V, m_bChk_step_aux_v);
}


BEGIN_MESSAGE_MAP(CSchUpdateDlg, CDialog)
	//{{AFX_MSG_MAP(CSchUpdateDlg)
	ON_BN_CLICKED(IDC_BUT_STEP_SCH_REQUEST, OnButStepSchRequest)
	ON_CBN_SELCHANGE(IDC_COMBO_STEP_TYPE, OnSelchangeComboStepType)
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_END_TIME, OnDeltaposSpinEndTime)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RECORD_TIME, OnDeltaposSpinRecordTime)
	ON_CBN_SELCHANGE(IDC_COMBO_STEP_MODE, OnSelchangeComboStepMode)
	ON_EN_CHANGE(IDC_EDIT_STEP_NO, OnChangeEditStepNo)
	ON_CBN_EDITCHANGE(IDC_COMBO_STEP_TYPE, OnEditchangeComboStepType)
	ON_CBN_EDITCHANGE(IDC_COMBO_STEP_MODE, OnEditchangeComboStepMode)
	ON_EN_CHANGE(IDC_EDIT_STEP_C_VOLT, OnChangeEditStepCVolt)
	ON_EN_CHANGE(IDC_EDIT_STEP_D_VOLT, OnChangeEditStepDVolt)
	ON_EN_CHANGE(IDC_EDIT_STEP_REF_I, OnChangeEditStepRefI)
	ON_EN_CHANGE(IDC_EDIT_STEP_REF_P, OnChangeEditStepRefP)
	ON_EN_CHANGE(IDC_EDIT_STEP_END_VOLT_H, OnChangeEditStepEndVoltH)
	ON_EN_CHANGE(IDC_EDIT_STEP_END_VOLT_L, OnChangeEditStepEndVoltL)
	ON_EN_CHANGE(IDC_EDIT_STEP_END_I, OnChangeEditStepEndI)
	ON_EN_CHANGE(IDC_EDIT_STEP_END_TIME_DAY, OnChangeEditStepEndTimeDay)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, OnDatetimechangeDatetimepicker1)
	ON_EN_CHANGE(IDC_EDIT_MS, OnChangeEditMs)
	ON_EN_CHANGE(IDC_EDIT_STEP_END_AH, OnChangeEditStepEndAh)
	ON_EN_CHANGE(IDC_EDIT_STEP_END_WH, OnChangeEditStepEndWh)
	ON_EN_CHANGE(IDC_EDIT_STEP_END_P, OnChangeEditStepEndP)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_V_LOW, OnChangeEditSafetyVLow)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_V_HIGH, OnChangeEditSafetyVHigh)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_I_LOW, OnChangeEditSafetyILow)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_I_HIGH, OnChangeEditSafetyIHigh)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_C_LOW, OnChangeEditSafetyCLow)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_C_HIGH, OnChangeEditSafetyCHigh)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_IMP_LOW, OnChangeEditSafetyImpLow)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_IMP_HIGH, OnChangeEditSafetyImpHigh)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER_RECORD_TIME, OnDatetimechangeDatetimepickerRecordTime)
	ON_EN_CHANGE(IDC_EDIT_RECORD_TIME_MS, OnChangeEditRecordTimeMs)
	ON_EN_CHANGE(IDC_EDIT_RECORD_DELTA_V, OnChangeEditRecordDeltaV)
	ON_EN_CHANGE(IDC_EDIT_RECORD_DELTA_A, OnChangeEditRecordDeltaA)
	ON_NOTIFY(NM_CLICK, IDC_UPDATE_STEP_LIST, OnClickUpdateStepList)
	ON_EN_CHANGE(IDC_EDIT_CELL_DELTA_V_STEP, &CSchUpdateDlg::OnEnChangeEditCellDeltaVStep) //lyj 20200214
	ON_EN_CHANGE(IDC_EDIT_STEP_AUXT_DELTA, &CSchUpdateDlg::OnEnChangeEditStepAuxtDelta)	//lyj 20200214
	ON_EN_CHANGE(IDC_EDIT_STEP_AUXTH_DELTA, &CSchUpdateDlg::OnEnChangeEditStepAuxthDelta)	//lyj 20200214
	ON_EN_CHANGE(IDC_EDIT_STEP_AUXT_AUXTH_DELTA, &CSchUpdateDlg::OnEnChangeEditStepAuxtAuxthDelta)	//lyj 20200214
	ON_EN_CHANGE(IDC_EDIT_STEP_AUXV_TIME, &CSchUpdateDlg::OnEnChangeEditStepAuxvTime)	//lyj 20200214
	ON_EN_CHANGE(IDC_EDIT_STEP_AUXV_DELTA, &CSchUpdateDlg::OnEnChangeEditStepAuxvDelta)	//lyj 20200214
	//}}AFX_MSG_MAP

	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_V, &CSchUpdateDlg::OnBnClickedChkStepDeltaAuxV)
	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_TEMP, &CSchUpdateDlg::OnBnClickedChkStepDeltaAuxTemp)
	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_TH, &CSchUpdateDlg::OnBnClickedChkStepDeltaAuxTh)
	ON_BN_CLICKED(IDC_CHK_STEP_DELTA_AUX_T, &CSchUpdateDlg::OnBnClickedChkStepDeltaAuxT)
	ON_BN_CLICKED(IDC_CHK_STEP_AUX_V, &CSchUpdateDlg::OnBnClickedChkStepAuxV)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSchUpdateDlg message handlers

BOOL CSchUpdateDlg::OnInitDialog() 
{	
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	ZeroMemory(&sStepRequest,sizeof(SFT_STEP_INFO_REQUEST));
	ZeroMemory(&m_sStepInfoData,sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상

	InitDataUnit();
	InitList();

	//ksj 20170807 : combo box 초기화
	CString strMsg;
	int nIndex = 0;
	m_combo_step_type.AddString("= Type =");	
	m_combo_step_type.SetItemData(nIndex++, 0);

	for(int nType = PS_STEP_CHARGE; nType <= PS_STEP_PAUSE; nType++)
	{
		strMsg = PSGetTypeMsg(nType);
		
		if(strMsg.IsEmpty() ||
			//strMsg == "작업중")	continue;
			strMsg == Fun_FindMsg("SchUpdateDlg_OnInitDialog_msg1","IDD_SCH_UPDATE_DLG"))	continue;//&&

		//ksj 20170809 : 충전, 방전, 휴지, 패턴만 지정가능
		if(nType != PS_STEP_CHARGE &&
		   nType != PS_STEP_DISCHARGE &&
		   nType != PS_STEP_REST &&
		   nType != PS_STEP_PATTERN)
		{
			continue;
		}
		//ksj end
		
		m_combo_step_type.AddString(strMsg);
		m_combo_step_type.SetItemData(nIndex++, nType);
	}
	m_combo_step_type.SetCurSel(0);
	OnSelchangeComboStepType();
	OnSelchangeComboStepMode();

	nIndex = 0;
	m_combo_step_mode.AddString("= Mode =");	
	m_combo_step_mode.SetItemData(nIndex++, 0);
	m_combo_step_mode.AddString("CC/CV");	
	m_combo_step_mode.SetItemData(nIndex++, PS_MODE_CCCV);
	m_combo_step_mode.AddString("CC");	
	m_combo_step_mode.SetItemData(nIndex++, PS_MODE_CC);
	m_combo_step_mode.AddString("CV");	
	m_combo_step_mode.SetItemData(nIndex++, PS_MODE_CV);
	m_combo_step_mode.AddString("CP");	
	m_combo_step_mode.SetItemData(nIndex++, PS_MODE_CP);
	m_combo_step_mode.AddString("CR");	
	m_combo_step_mode.SetItemData(nIndex++, PS_MODE_CR);
	
	m_combo_step_mode.SetCurSel(0);

	//ksj 20170808 : 데이터 단위, 라벨 초기화
	
	CString strUnit;
	char szBuff[64];
	char szDecimalPoint[32], szUnit[32];
	CString strTemp1, strTemp2;
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_VUNIT_HIGH)->SetWindowText(szUnit);
	GetDlgItem(IDC_STATIC_VUNIT_LOW)->SetWindowText(szUnit);
	m_nVDec = atoi(szDecimalPoint);

// 	GetDlgItem(IDC_STATIC_CellDeltaVStep)->SetWindowText(strTemp1);//yulee 20190531_3
// 	strTemp2.Format(_T("%s(%s)"), strTemp1, szUnit);
// 	GetDlgItem(IDC_STATIC_CellDeltaVStep)->SetWindowText(strTemp2);
	
	GetDlgItem(IDC_STATIC_V1)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V1)->SetWindowText(strTemp2);
	
	GetDlgItem(IDC_STATIC_V2)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V2)->SetWindowText(strTemp2);
	
	GetDlgItem(IDC_STATIC_V3)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V3)->SetWindowText(strTemp2);
	
	GetDlgItem(IDC_STATIC_V4)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V4)->SetWindowText(strTemp2);
	
	GetDlgItem(IDC_STATIC_V11)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V6)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V7)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V8)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V9)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_V10)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);

	
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "A 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_IUNIT)->SetWindowText(szUnit);
	m_nIDec = atoi(szDecimalPoint);
	
	GetDlgItem(IDC_STATIC_I1)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_I1)->SetWindowText(strTemp2);
	
	GetDlgItem(IDC_STATIC_I2)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_I2)->SetWindowText(strTemp2);
	
	GetDlgItem(IDC_STATIC_I3)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_I3)->SetWindowText(strTemp2);
	
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "C Unit", "Ah 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_CUNIT)->SetWindowText(szUnit);
	m_nCDec = atoi(szDecimalPoint);
	
	GetDlgItem(IDC_STATIC_C1)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_C1)->SetWindowText(strTemp2);
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "W Unit", "W 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
//	GetDlgItem(IDC_STATIC_WUNIT)->SetWindowText(szUnit);
	m_nWDec = atoi(szDecimalPoint);
	
	GetDlgItem(IDC_STATIC_P1)->GetWindowText(strTemp1);
	strTemp2.Format(_T("%s(%s)"),strTemp1, szUnit);
	GetDlgItem(IDC_STATIC_P1)->SetWindowText(strTemp2);
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Wh Unit", "Wh 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_WHUNIT)->SetWindowText(szUnit);
	m_nWhDec = atoi(szDecimalPoint);
	
	////////////////////////////////////////////////////////////////////////


	//ksj 20170807 : 시간 컨트롤 초기화
	m_ctrlEndTime.SetTime(DATE(0));
	m_ctrlEndTime.SetFormat("HH:mm:ss");
	GetDlgItem(IDC_EDIT_MS)->SetWindowText("000");

	m_ctrlRecordTime.SetTime(DATE(0));
	m_ctrlRecordTime.SetFormat("HH:mm:ss");
	GetDlgItem(IDC_EDIT_RECORD_TIME_MS)->SetWindowText("000");

	//ksj 20171031 : 사용자 정의 msec 스핀 증감 값 추가.
	m_nCustomMsec = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Msec Interval",0); // 0인 경우 기본값 MIN_TIME_INTERVAL 사용하도록 되어있음.
	
	int iUseVent = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC , "Use Vent Safety", 0); //lyj 20200217 no use vent
	if (!iUseVent)
	{
		GetDlgItem(IDC_STATIC_VENT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_V)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_TH)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_DELTA_AUX_T)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHK_STEP_AUX_V)->ShowWindow(SW_HIDE);
	}


	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSchUpdateDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	//m_sStepInfoData.stepHeader.nStepTypeID = m_edit_step_type;
	//m_sStepInfoData.stepHeader.mode = m_edit_step_mode;

	CString strMsg;
	CWorkWarnin WorkWarningDlg;
	//strMsg = "STEP 조건을 변경 실행 하시겠습니까?";
	strMsg = Fun_FindMsg("SchUpdateDlg_OnOK_msg1","IDD_SCH_UPDATE_DLG");//&&
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL)
	{
		ShowWindow(SW_HIDE); //창이 뒤에 숨어 있으므로 아예 숨겼다가 다시 보여주기.
		ShowWindow(SW_SHOW);
		return;
	}
	
	
	SFT_STEP_CONDITION_V3 sOrgStepInfoData; //ksj 20170810 : Log용으로 원본 값 임시 저장 //lyj 20200214 LG v1015 이상
	memcpy(&sOrgStepInfoData, &m_sStepInfoData,sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상

	COleDateTime time;
	CString strTemp;

	m_sStepInfoData.stepHeader.stepNo = m_edit_step_no; //ksj 20170809
	//ksj 20170807 : 콤보 박스에서 값 가져옴
	m_sStepInfoData.stepHeader.nStepTypeID = m_combo_step_type.GetItemData(m_combo_step_type.GetCurSel());
	m_sStepInfoData.stepHeader.mode = m_combo_step_mode.GetItemData(m_combo_step_mode.GetCurSel());

	//ksj 20170807 : 단위 환산하여 값 적용
	m_sStepInfoData.stepReferance->lVref_DisCharge = atof(m_edit_step_ref_d_volt) / m_fVUnit;
	m_sStepInfoData.stepReferance->lVref_Charge= atof(m_edit_step_ref_c_volt) / m_fVUnit;
	m_sStepInfoData.stepReferance->lIref = atof(m_edit_step_ref_I) / m_fIUnit;
	m_sStepInfoData.stepReferance->lPref = atof(m_edit_step_P) / m_fWUnit;
	//m_sStepInfoData.stepReferance->lIref = m_edit_step_end_wh;
	
	m_sStepInfoData.stepReferance->ulEndTimeDay = m_edit_step_end_time_day;
	//m_sStepInfoData.stepReferance->ulEndTime = m_edit_step_end_time;

	m_ctrlEndTime.GetTime(time);
	m_sStepInfoData.stepReferance->ulEndTime = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;		//저장 간격 설정 시간 
	m_sStepInfoData.stepReferance->ulEndTime += m_edit_step_end_time_ms/10;

	m_sStepInfoData.stepReferance->lEndI= atof(m_edit_step_end_I) / m_fIUnit;
	m_sStepInfoData.stepReferance->lEndC= atof(m_edit_step_end_Ah)/ m_fCUnit;
	m_sStepInfoData.stepReferance->lEndWatt = atof(m_edit_step_end_power) / m_fWUnit;
	m_sStepInfoData.stepReferance->lEndV_L = atof(m_edit_step_end_volt_min) / m_fVUnit;
	m_sStepInfoData.stepReferance->lEndV_H = atof(m_edit_step_end_volt_max) / m_fVUnit;

	//ksj 20170808 : 안전조건 및 기록 조건 추가
	//안전조건

	m_sStepInfoData.lLowV = atof(m_edit_safety_v_low) / m_fVUnit;	
	m_sStepInfoData.lHighV = atof(m_edit_safety_v_high) / m_fVUnit;	
	m_sStepInfoData.lLowI = atof(m_edit_safety_i_low) / m_fIUnit;
	m_sStepInfoData.lHighI = atof(m_edit_safety_i_high) / m_fIUnit;
	m_sStepInfoData.lLowC = atof(m_edit_safety_c_low) / m_fCUnit;
	m_sStepInfoData.lHighC = atof(m_edit_safety_c_high) / m_fCUnit;
	m_sStepInfoData.lLowZ = atof(m_edit_safety_z_low) * 1000000.0f;
	m_sStepInfoData.lHighZ = atof(m_edit_safety_z_high) * 1000000.0f;
//	m_sStepInfoData.lCellDeltaVStep = atof(m_edit_safety_delta_v_step) / m_fVUnit;//yulee 20190531_3


	m_sStepInfoData.lCellDeltaVStep = atof(m_edit_cell_delta_v_step)  / m_fVUnit;	//lyj 20200214
	m_sStepInfoData.faultcompAuxTemp = atof(m_edit_step_auxt_delta) * 1000.0f;	//lyj 20200214
	m_sStepInfoData.faultcompAuxTh = atof(m_edit_step_auxth_delta) * 1000000.0f;	//lyj 20200214
	m_sStepInfoData.faultcompAuxT = atof(m_edit_step_auxt_auxth_delta) * 1000.0f;	//lyj 20200214
	m_sStepInfoData.faultcompAuxV_Time = atof(m_edit_step_auxv_time) * 100.0f;	//lyj 20200214
	m_sStepInfoData.faultcompAuxV = atof(m_edit_step_auxv_delta) / m_fVUnit;	//lyj 20200214
	m_sStepInfoData.faultCompAuxV_Vent_flag = m_bChk_step_delta_aux_v ;
	m_sStepInfoData.faultCompAuxTemp_Vent_flag = m_bChk_step_delta_aux_temp ;
	m_sStepInfoData.faultCompAuxTh_Vent_flag = m_bChk_step_delta_aux_th;
	m_sStepInfoData.faultCompAuxT_vent_flag = m_bChk_step_delta_aux_t;
	m_sStepInfoData.faultDelta_AuxV_Vent_flag = m_bChk_step_aux_v;

	//기록조건	
	m_sStepInfoData.conREC.lDeltaV = atof(m_edit_record_delta_v) / m_fVUnit;
	m_sStepInfoData.conREC.lDeltaI = atof(m_edit_record_delta_i) / m_fIUnit;
	
	m_ctrlRecordTime.GetTime(time);
	m_sStepInfoData.conREC.lTime = (time.GetHour()*3600+time.GetMinute()*60+time.GetSecond())*100;		//저장 간격 설정 시간 
	m_sStepInfoData.conREC.lTime += m_edit_record_time_ms/10;

	//Fun_SendSchUpdaeCmd()
	//CDialog::OnOK();

	
	//ksj 20170810 : 전송 성공할때만 창닫기.
	if(!Fun_SendSchUpdaeCmd())	 
	{
		ShowWindow(SW_HIDE); //창이 뒤에 숨어 있으므로 아예 숨겼다가 다시 보여주기.
		ShowWindow(SW_SHOW);

		//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("Step Update 실패.")); //ksj 20170823
		GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SchUpdateDlg_OnOK_msg2","IDD_SCH_UPDATE_DLG")); //ksj 20170823//&&
		return;
	}
	

	UpdateSchStep(); //ksj 20170811 : SCH 파일 업데이트	
	WriteSchUpdateLog(&sOrgStepInfoData, &m_sStepInfoData); //스케쥴 업데이트 로그 기록.

	DisableUpdate();
	//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("Step Update 완료.")); //ksj 20170823
	GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SchUpdateDlg_OnOK_msg3","IDD_SCH_UPDATE_DLG")); //ksj 20170823//&&
	UpdateList(); //ksj 20170823 : 스텝 리스트 갱신
	
	//CDialog::OnOK(); //ksj 20170823 : 주석처리. 창닫지 않음.
}

void CSchUpdateDlg::OnButStepSchRequest() 
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	if (m_edit_step_no == 0)
	{
		//AfxMessageBox("STEP 번호를 입력 하세요. (0이상)");
		//AfxMessageBox("STEP 을 선택해주세요.");
		AfxMessageBox(Fun_FindMsg("SchUpdateDlg_OnButStepSchRequest_msg1","IDD_SCH_UPDATE_DLG"));//&&
		return;
	}
	
	sStepRequest.lStpepNo = m_edit_step_no -1 ; //0 base  1번째 스텝

	CWaitCursor cursor;	
	
	CString strMsg;
	CCyclerModule *pMD;
	//CCyclerChannel *pCH;
	
	pMD = m_pDoc->GetCyclerMD(m_nModuleID);
	if(pMD == NULL)		return;

	//int nRtn = 0;
	/*CWordArray SelectCh;
	SelectCh.Add(m_nChIndex);*/
	
	//	CCyclerModule *pMD = GetModuleInfo(nModuleID);
// 	if((nRtn = pMD->SendCommand(SFT_CMD_STEP_DATA_REQUEST, m_nChIndex, &sStepRequest, sizeof(SFT_STEP_INFO_REQUEST))) != SFT_ACK)
// 	{
// 		CString strMsg;
// 		strMsg.Format("Step Info Request 명령 전달 실패! Return Value : %x", nRtn);
// 		AfxMessageBox(strMsg);
// 		return;
// 	}
	
	
	//pMD->SendCommand(SFT_CMD_STEP_DATA_REQUEST, m_nChIndex, &sStepRequest, sizeof(SFT_STEP_INFO_REQUEST));

	//ksj 20170808 : SendCommand를 사용할 경우 ACK 수신 대기를 10초간 하게 되므로 블록킹 상태에 걸리게 된다.
	//쓰레드로 명령을 전송하여 블록킹을 회피한다.
	ST_SEND_COMMAND_THREAD_PARAM* pParam = new ST_SEND_COMMAND_THREAD_PARAM;

	pParam->pMD = pMD;
	pParam->nCommand = SFT_CMD_STEP_DATA_REQUEST;
	//pParam->nChIndex = m_nChIndex;
	pParam->SelectCh.Add(m_nChIndex);
	pParam->pStepRequest = &sStepRequest;
	pParam->nSize = sizeof(SFT_STEP_INFO_REQUEST);

	AfxBeginThread(SendCommandThread,pParam);	
	//ksj end

	if(m_pCH) m_pCH->WriteSchUpdateLog("STEP Schedule Request"); //ksj 201708010 : 로그 기록

	//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("Step 정보 요청....")); //ksj 20170823
	GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SchUpdateDlg_OnButStepSchRequest_msg2","IDD_SCH_UPDATE_DLG")); //ksj 20170823//&&
}


//ksj 20170808 : SendCommand를 사용할 경우 ACK 수신 대기를 10초간 하게 되므로 블록킹 상태에 걸리게 된다.
//쓰레드로 명령을 전송하여 블록킹을 회피한다.
UINT CSchUpdateDlg::SendCommandThread(LPVOID lpParam)
{
	ST_SEND_COMMAND_THREAD_PARAM *pSendCommandParam = (ST_SEND_COMMAND_THREAD_PARAM *) lpParam;


	if(pSendCommandParam == NULL)
		return 0;

	if(pSendCommandParam->pMD == NULL)
		return 0;


	pSendCommandParam->pMD->SendCommand(pSendCommandParam->nCommand,
										//pSendCommandParam->nChIndex,
										&pSendCommandParam->SelectCh,
										pSendCommandParam->pStepRequest,
										pSendCommandParam->nSize);

	delete pSendCommandParam;

	return 1;
}

void CSchUpdateDlg::SetReceiveStepInfoData(SFT_STEP_CONDITION_V3 *stepInfoData) //lyj 20200214 LG v1015 이상
{
	UpdateData(TRUE);

	InitDataUnit();

	memcpy(&m_sStepInfoData, stepInfoData, sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상
	
	m_edit_step_type = m_sStepInfoData.stepHeader.nStepTypeID;
	m_edit_step_mode = m_sStepInfoData.stepHeader.mode;

	//ksj 20170809 : 스텝 타입이 충,방전,휴지,패턴이 아닌 경우 수정 불가	
	CString strTemp;
	if(m_edit_step_type != PS_STEP_CHARGE &&
		m_edit_step_type != PS_STEP_DISCHARGE &&
		m_edit_step_type != PS_STEP_REST &&
		m_edit_step_type != PS_STEP_PATTERN)
	{
		GetDlgItem(IDOK)->EnableWindow(FALSE);
		//strTemp.Format("Step:%d \"%s\" 스텝은 변경할 수 없습니다.",m_edit_step_no, PSGetTypeMsg(m_edit_step_type));
		strTemp.Format(Fun_FindMsg("SchUpdateDlg_SetReceiveStepInfoData_msg1","IDD_SCH_UPDATE_DLG"),m_edit_step_no, PSGetTypeMsg(m_edit_step_type));//&&
		//MessageBox(strTemp,"경고",MB_OK|MB_ICONSTOP);
		MessageBox(strTemp,Fun_FindMsg("SchUpdateDlg_SetReceiveStepInfoData_msg2","IDD_SCH_UPDATE_DLG"),MB_OK|MB_ICONSTOP); //&&
		m_edit_step_no = 0;
		UpdateData(FALSE);
		return;
	}
	
	//ksj 20170807 : 콤보 박스에 값 표시
	int nIndex;
	int i;
	for(int i = 0 ; i < m_combo_step_type.GetCount(); i++)
	{
		nIndex = m_combo_step_type.GetItemData(i);

		if(nIndex == m_sStepInfoData.stepHeader.nStepTypeID)
		{
			m_combo_step_type.SetCurSel(i);
			break;
		}
	}
	OnSelchangeComboStepType();

	for(int i = 0 ; i < m_combo_step_mode.GetCount(); i++)
	{
		nIndex = m_combo_step_mode.GetItemData(i);
		
		if(nIndex == m_sStepInfoData.stepHeader.mode)
		{
			m_combo_step_mode.SetCurSel(i);
			break;
		}
	}	
	OnSelchangeComboStepMode();

	m_edit_step_no = m_sStepInfoData.stepHeader.stepNo; //ksj 20170809 : 스텝 번호 갱신 추가.

	//ksj 20170807 : 단위 변환
	strTemp.Format(_T("%%.%df"),m_nVDec);
	m_edit_step_ref_d_volt.Format(strTemp, m_sStepInfoData.stepReferance->lVref_DisCharge * m_fVUnit);
	m_edit_step_ref_c_volt.Format(strTemp,m_sStepInfoData.stepReferance->lVref_Charge * m_fVUnit);

	strTemp.Format(_T("%%.%df"),m_nIDec);
	m_edit_step_ref_I.Format(strTemp,m_sStepInfoData.stepReferance->lIref * m_fIUnit);

	strTemp.Format(_T("%%.%df"),m_nWDec);
	m_edit_step_P.Format(strTemp,m_sStepInfoData.stepReferance->lPref * m_fWUnit);
	
	m_edit_step_end_time_day = m_sStepInfoData.stepReferance->ulEndTimeDay;
	m_edit_step_end_time = m_sStepInfoData.stepReferance->ulEndTime;

	COleDateTime time;
	div_t result;
	int hour;
	ULONG lSecond = m_sStepInfoData.stepReferance->ulEndTime/100;	
	//long lmiliSec =m_sStepInfoData.stepReferance->ulEndTime%10;
	long lmiliSec =m_sStepInfoData.stepReferance->ulEndTime%100; //ksj 20171103
	result = div(lSecond, 3600);
	hour = result.quot;
	result = div(result.rem, 60) ;
	time.SetTime(hour, result.quot, result.rem);
	m_ctrlEndTime.SetTime(time);
	//m_edit_step_end_time_ms = lmiliSec;
	m_edit_step_end_time_ms = lmiliSec*10; //ksj 20171103


	strTemp.Format(_T("%%.%df"),m_nIDec);
	m_edit_step_end_I.Format(strTemp,m_sStepInfoData.stepReferance->lEndI * m_fIUnit);
	strTemp.Format(_T("%%.%df"),m_nCDec);
	m_edit_step_end_Ah.Format(strTemp,m_sStepInfoData.stepReferance->lEndC * m_fCUnit);
	strTemp.Format(_T("%%.%df"),m_nWhDec);
	m_edit_step_end_wh.Format(strTemp,m_sStepInfoData.stepReferance->lEndWattHour * m_fWhUnit);
	strTemp.Format(_T("%%.%df"),m_nWDec);
	m_edit_step_end_power.Format(strTemp,m_sStepInfoData.stepReferance->lEndWatt * m_fWUnit);

	strTemp.Format(_T("%%.%df"),m_nVDec);
	m_edit_step_end_volt_min.Format(strTemp,m_sStepInfoData.stepReferance->lEndV_L * m_fVUnit);
	m_edit_step_end_volt_max.Format(strTemp,m_sStepInfoData.stepReferance->lEndV_H * m_fVUnit);

	//ksj 20170808 : 안전조건 및 기록 조건 추가
	//안전조건
	strTemp.Format(_T("%%.%df"),m_nVDec);
	m_edit_safety_v_low.Format(strTemp,m_sStepInfoData.lLowV * m_fVUnit);	
	m_edit_safety_v_high.Format(strTemp,m_sStepInfoData.lHighV * m_fVUnit);
	strTemp.Format(_T("%%.%df"),m_nIDec);
	m_edit_safety_i_low.Format(strTemp,m_sStepInfoData.lLowI * m_fIUnit);
	m_edit_safety_i_high.Format(strTemp,m_sStepInfoData.lHighI * m_fIUnit);
	strTemp.Format(_T("%%.%df"),m_nCDec);
	m_edit_safety_c_low.Format(strTemp,m_sStepInfoData.lLowC * m_fCUnit);
	m_edit_safety_c_high.Format(strTemp,m_sStepInfoData.lHighC * m_fCUnit);
	strTemp.Format(_T("%%.%df"),3); //임피던스는 임시로
	m_edit_safety_z_low.Format(strTemp,m_sStepInfoData.lLowZ / 1000000.0f);
	m_edit_safety_z_high.Format(strTemp,m_sStepInfoData.lHighZ / 1000000.0f);
//	strTemp.Format(_T("%%.%df"),m_nVCellDeltaDEC);
//	m_edit_safety_delta_v_step.Format(strTemp,m_sStepInfoData.lCellDeltaVStep * m_fVUnit);//yulee 20190531_3_1
	
	strTemp.Format(_T("%%.%df"),m_nVDec);
	m_edit_cell_delta_v_step.Format(strTemp,m_sStepInfoData.lCellDeltaVStep * m_fVUnit);		//lyj 20200214
	m_edit_step_auxt_delta.Format(strTemp,m_sStepInfoData.faultcompAuxTemp * 0.001f);	//lyj 20200214
	m_edit_step_auxth_delta.Format(strTemp,m_sStepInfoData.faultcompAuxTh * 0.000001f);		//lyj 20200214
	m_edit_step_auxt_auxth_delta.Format(strTemp,m_sStepInfoData.faultcompAuxT * 0.001f);		//lyj 20200214
	m_edit_step_auxv_time.Format(strTemp,m_sStepInfoData.faultcompAuxV_Time * 0.01f);		//lyj 20200214
	m_edit_step_auxv_delta.Format(strTemp,m_sStepInfoData.faultcompAuxV * m_fVUnit);	//lyj 20200214
	m_bChk_step_delta_aux_v = m_sStepInfoData.faultCompAuxV_Vent_flag;
	m_bChk_step_delta_aux_temp = m_sStepInfoData.faultCompAuxTemp_Vent_flag;
	m_bChk_step_delta_aux_th = m_sStepInfoData.faultCompAuxTh_Vent_flag;
	m_bChk_step_delta_aux_t = m_sStepInfoData.faultCompAuxT_vent_flag;
	m_bChk_step_aux_v = m_sStepInfoData.faultDelta_AuxV_Vent_flag;


	//기록조건
	strTemp.Format(_T("%%.%df"),m_nVDec);
	m_edit_record_delta_v.Format(strTemp,m_sStepInfoData.conREC.lDeltaV * m_fVUnit);
	strTemp.Format(_T("%%.%df"),m_nIDec);
	m_edit_record_delta_i.Format(strTemp,m_sStepInfoData.conREC.lDeltaI * m_fIUnit);


	lSecond = m_sStepInfoData.conREC.lTime/100;	
	//lmiliSec = m_sStepInfoData.conREC.lTime%10;
	lmiliSec = m_sStepInfoData.conREC.lTime%100; //ksj 20171103
	result = div(lSecond, 3600);
	hour = result.quot;
	result = div(result.rem, 60) ;
	time.SetTime(hour, result.quot, result.rem);
	m_ctrlRecordTime.SetTime(time);
	//m_edit_record_time_ms = lmiliSec;
	m_edit_record_time_ms = lmiliSec*10; //ksj 20171103

	UpdateData(FALSE);
	
//	AfxMessageBox("Step Info recv");

	if(m_pCH) m_pCH->WriteSchUpdateLog("Step Info recv"); //ksj 201708010 :로그 기록

	DisableUpdate();

	//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("Step 정보 수신 완료.")); //ksj 20170823
	GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SchUpdateDlg_SetReceiveStepInfoData_msg3","IDD_SCH_UPDATE_DLG")); //ksj 20170823//&&
	//UpdateList();
}

//void CSchUpdateDlg::Fun_SendSchUpdaeCmd()
BOOL CSchUpdateDlg::Fun_SendSchUpdaeCmd() //ksj 20170810 : 리턴 값 추가.
{
	UpdateData(TRUE);
	if (m_edit_step_no == 0)
	{
		//AfxMessageBox("Update할 Step을 선택해주세요.");
		AfxMessageBox(Fun_FindMsg("SchUpdateDlg_Fun_SendSchUpdaeCmd_msg1","IDD_SCH_UPDATE_DLG"));//&&
		return FALSE;
	}
	
	//ksj 20170809
	int nType = m_combo_step_type.GetItemData(m_combo_step_type.GetCurSel());
	if(nType == 0)
	{
		//AfxMessageBox("STEP Type을 선택하세요.");
		AfxMessageBox(Fun_FindMsg("SchUpdateDlg_Fun_SendSchUpdaeCmd_msg2","IDD_SCH_UPDATE_DLG"));//&&
		return FALSE;
	}

	if((nType == PS_STEP_CHARGE || nType == PS_STEP_DISCHARGE)
		&& m_combo_step_mode.GetItemData(m_combo_step_mode.GetCurSel()) == 0)
	{
		//AfxMessageBox("STEP Mode을 선택하세요.");
		AfxMessageBox(Fun_FindMsg("SchUpdateDlg_Fun_SendSchUpdaeCmd_msg3","IDD_SCH_UPDATE_DLG"));//&&
		return FALSE;
	}
	//ksj end

	SFT_CMD_SCHEDULE_UPDATE_BODY sScheduleUpdateBody;
	ZeroMemory(&sScheduleUpdateBody,sizeof(SFT_CMD_SCHEDULE_UPDATE_BODY));
	memcpy(&sScheduleUpdateBody.sStepInfo, &sStepRequest,sizeof(SFT_STEP_INFO_REQUEST)); 
	sScheduleUpdateBody.sStepInfo.lStpepNo = m_edit_step_no - 1;	//ljb 20170808 add
	memcpy(&sScheduleUpdateBody.sTestSchedule, &m_sStepInfoData, sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상

	CWaitCursor cursor;	
	
	CString strMsg;
	CCyclerModule *pMD;
	//CCyclerChannel *pCH;
	//CCalibrationData *pChCalData;
	
	pMD = m_pDoc->GetCyclerMD(m_nModuleID);
	if(pMD == NULL)		return FALSE;
	
	int nRtn;
	if((nRtn = pMD->SendCommand(SFT_CMD_STEP_DATA_UPDATE, m_nChIndex, &sScheduleUpdateBody, sizeof(SFT_CMD_SCHEDULE_UPDATE_BODY))) != SFT_ACK)	//Body 있는 Command
	{
		//AfxMessageBox("command 전송 실패");
		AfxMessageBox(Fun_FindMsg("SchUpdateDlg_Fun_SendSchUpdaeCmd_msg4","IDD_SCH_UPDATE_DLG"));//&&
		return FALSE;
	}

	return TRUE;
}

BOOL CSchUpdateDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_ESCAPE :
			{
				//				OnOK();
				return FALSE;
			}
		case VK_RETURN:
			{
				return FALSE;
			}
		}
	}	
	return CDialog::PreTranslateMessage(pMsg);
}

void CSchUpdateDlg::OnSelchangeComboStepType() 
{	
	
	//ksj 20170807 : 충, 방전일때만 모드 선택 가능하도록 활성/비활성화
	BOOL bDisable = FALSE;
	CString strTemp;
	int nType = m_combo_step_type.GetItemData(m_combo_step_type.GetCurSel());
	if(nType == PS_STEP_CHARGE || nType == PS_STEP_DISCHARGE || nType == PS_STEP_EXT_CAN || nType == PS_STEP_PATTERN)
	{
	
		if(nType != PS_STEP_PATTERN)
		{
			m_combo_step_mode.EnableWindow(TRUE);
		}		
		GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(TRUE);

		if(nType == PS_STEP_CHARGE || nType == PS_STEP_PATTERN || nType == PS_STEP_EXT_CAN)
		{			
			GetDlgItem(IDC_EDIT_STEP_C_VOLT)->EnableWindow(TRUE); //충전 전압 활성화
		}
		else
		{
			GetDlgItem(IDC_EDIT_STEP_C_VOLT)->EnableWindow(FALSE); //충전 전압 비활성화
			GetDlgItem(IDC_EDIT_STEP_C_VOLT)->SetWindowText("0");
		}

		if(nType == PS_STEP_DISCHARGE || nType == PS_STEP_PATTERN || nType == PS_STEP_EXT_CAN)
		{
			GetDlgItem(IDC_EDIT_STEP_D_VOLT)->EnableWindow(TRUE); //방전 전압 활성화
		}
		else
		{
			GetDlgItem(IDC_EDIT_STEP_D_VOLT)->EnableWindow(FALSE); //방전 전압 비활성화
			GetDlgItem(IDC_EDIT_STEP_D_VOLT)->SetWindowText("0");
		}

		if(nType == PS_STEP_EXT_CAN)
		{
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText("0");
		}

		if(nType == PS_STEP_PATTERN)
		{
			if(m_edit_step_type != PS_STEP_PATTERN) //다른 타입에서 패턴으로 전환 불가.
			{
				bDisable = TRUE;
				m_combo_step_type.SetCurSel(0);
				for(int i=0; i< m_combo_step_type.GetCount();i++)
				{
					if(m_combo_step_type.GetItemData(i) == m_edit_step_type)
					{
						bDisable = FALSE;
						m_combo_step_type.SetCurSel(i);
						break;
					}
				}				

				//strTemp.Format("%s Step은 Pattern으로 변경이 불가능합니다.",PSGetTypeMsg(m_edit_step_type));
				strTemp.Format(Fun_FindMsg("SchUpdateDlg_OnSelchangeComboStepType_msg3","IDD_SCH_UPDATE_DLG"),PSGetTypeMsg(m_edit_step_type));//&&
				//MessageBox(strTemp,"경고",MB_OK|MB_ICONSTOP);				
				MessageBox(strTemp,Fun_FindMsg("SchUpdateDlg_OnSelchangeComboStepType_msg4","IDD_SCH_UPDATE_DLG"),MB_OK|MB_ICONSTOP);				//&&
				OnSelchangeComboStepType(); //다시 호출. (무한루프 주의)	
				OnSelchangeComboStepMode();
				return;
			}
			else
			{   //충방 활성화, 전류 비활성
				GetDlgItem(IDC_EDIT_STEP_C_VOLT)->EnableWindow(TRUE);			
				GetDlgItem(IDC_EDIT_STEP_D_VOLT)->EnableWindow(TRUE);			
				GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_STEP_REF_I)->SetWindowText(_T("0"));
				GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));
		
				//종료조건 파워 비활성
				GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_STEP_END_P)->SetWindowText(_T("0"));
				GetDlgItem(IDC_EDIT_STEP_END_AH)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_STEP_END_WH)->EnableWindow(TRUE);
				
				//GetDlgItem(IDC_STATIC_END_AH)->SetWindowText(_T("합산 용량"));
				GetDlgItem(IDC_STATIC_END_AH)->SetWindowText(Fun_FindMsg("SchUpdateDlg_OnSelchangeComboStepType_msg1","IDD_SCH_UPDATE_DLG"));//&&
				//GetDlgItem(IDC_STATIC_END_WH)->SetWindowText(_T("합산 Wh"));
				GetDlgItem(IDC_STATIC_END_WH)->SetWindowText(Fun_FindMsg("SchUpdateDlg_OnSelchangeComboStepType_msg2","IDD_SCH_UPDATE_DLG"));//&&
			}
		}

		//안전조건 전류, 용량 활성화
		GetDlgItem(IDC_EDIT_SAFETY_I_LOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SAFETY_I_HIGH)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SAFETY_C_LOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SAFETY_C_HIGH)->EnableWindow(TRUE);

	}
	else if(nType == PS_STEP_REST) //레스트
	{
		//종료조건 파워 비활성
		GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_END_I)->SetWindowText(_T("0"));
		GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_END_P)->SetWindowText(_T("0"));
		GetDlgItem(IDC_EDIT_STEP_END_AH)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_END_AH)->SetWindowText(_T("0"));
		GetDlgItem(IDC_EDIT_STEP_END_WH)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_END_WH)->SetWindowText(_T("0"));

		//안전조건 전류, 용량 비활성화
		GetDlgItem(IDC_EDIT_SAFETY_I_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_SAFETY_I_LOW)->SetWindowText(_T("0"));
		GetDlgItem(IDC_EDIT_SAFETY_I_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_SAFETY_I_HIGH)->SetWindowText(_T("0"));
		GetDlgItem(IDC_EDIT_SAFETY_C_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_SAFETY_C_LOW)->SetWindowText(_T("0"));
		GetDlgItem(IDC_EDIT_SAFETY_C_HIGH)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_SAFETY_C_HIGH)->SetWindowText(_T("0"));

		bDisable = TRUE;
	}
	else
	{
		bDisable = TRUE;
	}


	if(bDisable)
	{
		
		m_combo_step_mode.EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_C_VOLT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_D_VOLT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);

		GetDlgItem(IDC_EDIT_STEP_C_VOLT)->SetWindowText("0");
		GetDlgItem(IDC_EDIT_STEP_D_VOLT)->SetWindowText("0");
		GetDlgItem(IDC_EDIT_STEP_REF_I)->SetWindowText("0");
		GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText("0");

		
	}
	
	if(nType == PS_STEP_CHARGE || nType == PS_STEP_DISCHARGE)
	{
		m_combo_step_mode.SetCurSel(1); //충방전 타입 선택시 첫번째 모드(CCCV)를 기본 값으로 선택
		OnSelchangeComboStepMode();
	}
	else
	{
		m_combo_step_mode.SetCurSel(0);
		OnSelchangeComboStepMode();
	}
	
}

//ksj 20170807 : 값 단위 초기화
void CSchUpdateDlg::InitDataUnit()
{
	CString strUnit;

	m_fVUnit = 1.0f;
	m_fIUnit = 1.0f;
	m_fCUnit = 1.0f;
	m_fWUnit = 1.0f;
	m_fWhUnit = 1.0f;

	GetDlgItem(IDC_STATIC_VUNIT_HIGH)->GetWindowText(strUnit);

	if(strUnit == "V")
	{		
		m_fVUnit = 0.000001f;
	}
	if(strUnit == "mV")
	{		
		m_fVUnit = 0.001f;
	}

	GetDlgItem(IDC_STATIC_IUNIT)->GetWindowText(strUnit);

	if(strUnit == "A")
	{
		m_fIUnit = 0.000001f;
	}
	if(strUnit == "mA")
	{
		m_fIUnit = 0.001f;
	}
	else if(strUnit == "nA")
	{
		m_fIUnit = 1000.0f;
	}

	GetDlgItem(IDC_STATIC_CUNIT)->GetWindowText(strUnit);
	
	if(strUnit == "Ah")
	{
		m_fCUnit = 0.000001f;
	}
	else if(strUnit == "mAh")
	{
		m_fCUnit = 0.001f;
	}
	else if(strUnit == "nAh")
	{
		m_fCUnit = 1000.0f;
	}

//	GetDlgItem(IDC_STATIC_WUNIT)->GetWindowText(strUnit);
	
	if(strUnit == "KW" || strUnit == "kW")
	{
		m_fWUnit = 0.000001f ;		
	}
	else if(strUnit == "W")
	{
		m_fWUnit = 0.001 ;
	}
	else if(strUnit == "uW")
	{
		m_fWUnit = 1000.0f ;
	}

	GetDlgItem(IDC_STATIC_WHUNIT)->GetWindowText(strUnit);
	
	if(strUnit == "KWh" || strUnit == "kWh")
	{
		m_fWhUnit = 0.000001f ;	
	}
	else if(strUnit == "Wh")
	{
		m_fWhUnit = 0.001;
	}
	else if(strUnit == "uWh")
	{
		m_fWhUnit = 1000.0f;
	}
}

void CSchUpdateDlg::SetDefaultStepNo(UINT nStepNo)
{
	m_nDefaultStepNo = nStepNo;
}

void CSchUpdateDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	//ksj 20170809 : 기본 스텝 번호가 지정된 경우 자동으로 해당 스텝 요청하여 표시한다.
	if(bShow == TRUE)
	{
		if(m_nDefaultStepNo > 0)
		{
			m_edit_step_no = m_nDefaultStepNo;
			UpdateData(FALSE);
			OnButStepSchRequest();
		}
	}
}


void CSchUpdateDlg::OnDeltaposSpinEndTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CString strData;		
	GetDlgItem(IDC_EDIT_MS)->GetWindowText(strData);	

	//int nMinInterValTime = 10;
	
	//ksj 20180223 : 주석처리
	/*
	int mtime = atol(strData);
	if(pNMUpDown->iDelta > 0)
	{		
 		mtime -= MIN_TIME_INTERVAL;
 		if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);	
	}
	else
	{
 		mtime += MIN_TIME_INTERVAL;		
 		if(mtime >= 1000)	mtime = 0;		

	}*/

	//ksj 20180223 : ms 단위 변경
	int mtime = atol(strData);
	if(pNMUpDown->iDelta > 0)
	{		
		if(m_nCustomMsec)
		{
			mtime -= m_nCustomMsec;
			if(mtime < 0)	mtime = (1000-m_nCustomMsec);	
		}
		else
		{
			mtime -= MIN_TIME_INTERVAL;
			if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);	
		}	
	}
	else
	{
		if(m_nCustomMsec)
		{
			mtime += m_nCustomMsec;						
		}
		else
		{
			mtime += MIN_TIME_INTERVAL;				
		}
		if(mtime >= 1000)	mtime = 0;				
		
	}
	strData.Format("%03d", mtime);
	GetDlgItem(IDC_EDIT_MS)->SetWindowText(strData);	
		
	
	strData.Empty();
	
	*pResult = 0;
}

void CSchUpdateDlg::OnDeltaposSpinRecordTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CString strData;		
	GetDlgItem(IDC_EDIT_RECORD_TIME_MS)->GetWindowText(strData);	
	
	//int nMinInterValTime = 10;
	
	int mtime = atol(strData);
	if(pNMUpDown->iDelta > 0)
	{	
// 		mtime -= MIN_TIME_INTERVAL;
// 		if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);	

		//ksj 20171103
		if(m_nCustomMsec)
		{
			mtime -= m_nCustomMsec;
			if(mtime < 0)	mtime = (1000-m_nCustomMsec);	
		}
		else
		{
			mtime -= MIN_TIME_INTERVAL;
			if(mtime < 0)	mtime = (1000-MIN_TIME_INTERVAL);	
		}
		
	}
	else
	{
// 		mtime += MIN_TIME_INTERVAL;		
// 		if(mtime >= 1000)	mtime = 0;		

		//ksj 20171103
		if(m_nCustomMsec)
		{
			mtime += m_nCustomMsec;						
		}
		else
		{
			mtime += MIN_TIME_INTERVAL;				
		}
		if(mtime >= 1000)	mtime = 0;		
	}
	strData.Format("%03d", mtime);
	GetDlgItem(IDC_EDIT_RECORD_TIME_MS)->SetWindowText(strData);	
	
	
	strData.Empty();
}

void CSchUpdateDlg::OnSelchangeComboStepMode() 
{
	//종료조건 edit 비활성화
	//GetDlgItem(IDC_STATIC_END_AH)->SetWindowText(_T("종료 용량"));
	GetDlgItem(IDC_STATIC_END_AH)->SetWindowText(Fun_FindMsg("SchUpdateDlg_OnSelchangeComboStepMode_msg1","IDD_SCH_UPDATE_DLG"));//&&
	//GetDlgItem(IDC_STATIC_END_WH)->SetWindowText(_T("종료 Wh"));
	GetDlgItem(IDC_STATIC_END_WH)->SetWindowText(Fun_FindMsg("SchUpdateDlg_OnSelchangeComboStepMode_msg2","IDD_SCH_UPDATE_DLG"));//&&

	int nType = m_combo_step_type.GetItemData(m_combo_step_type.GetCurSel());
	int nMode = m_combo_step_mode.GetItemData(m_combo_step_mode.GetCurSel());
	
	if(nType == PS_STEP_CHARGE)
	{
		if(nMode == PS_MODE_CCCV)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(TRUE);	
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(TRUE);		
		}
		else if(nMode == PS_MODE_CC)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(TRUE);	
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(FALSE);			
			GetDlgItem(IDC_EDIT_STEP_END_I)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(TRUE);
		}
		else if(nMode == PS_MODE_CV)
		{			
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(TRUE);	
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(TRUE);			
		}		
		else if(nMode == PS_MODE_CP)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(TRUE);			
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);						
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(TRUE);			
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_I)->SetWindowText(_T("0"));
		}
		else if(nMode == PS_MODE_CR)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(TRUE);			
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);						
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);			
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_I)->SetWindowText(_T("0"));
		}

		GetDlgItem(IDC_EDIT_STEP_END_AH)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_STEP_END_WH)->EnableWindow(TRUE);
	}
	else if(nType == PS_STEP_DISCHARGE)
	{
		if(nMode == PS_MODE_CCCV)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(TRUE);
		}
		else if(nMode == PS_MODE_CC)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(FALSE);			
			GetDlgItem(IDC_EDIT_STEP_END_I)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(TRUE);
		}
		else if(nMode == PS_MODE_CV)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(TRUE);	
		}
		else if(nMode == PS_MODE_CP)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(TRUE);			
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_P)->SetWindowText(_T("0"));
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);						
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(TRUE);			
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_I)->SetWindowText(_T("0"));
		}
		else if(nMode == PS_MODE_CR)
		{
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_L)->EnableWindow(TRUE);			
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_END_VOLT_H)->SetWindowText(_T("0"));			
			GetDlgItem(IDC_EDIT_STEP_END_P)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_STEP_END_I)->EnableWindow(TRUE);					
			GetDlgItem(IDC_EDIT_STEP_REF_P)->EnableWindow(FALSE);			
			GetDlgItem(IDC_EDIT_STEP_REF_P)->SetWindowText(_T("0"));				
			GetDlgItem(IDC_EDIT_STEP_REF_I)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_STEP_REF_I)->SetWindowText(_T("0"));
		}

		GetDlgItem(IDC_EDIT_STEP_END_AH)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_STEP_END_WH)->EnableWindow(TRUE);
	}
}

//ksj 20170810 : Update 시 이전 값과 현재 값 변경 로그 기록.
void CSchUpdateDlg::WriteSchUpdateLog(SFT_STEP_CONDITION_V3* pSrc, SFT_STEP_CONDITION_V3* pDst) //lyj 20200214 LG v1015 이상
{
	//CCyclerModule *pMD;
	//CCyclerChannel *pCH = m_pDoc->GetChannelInfo(m_nModuleID, m_nChIndex);
	
	if(m_pCH == NULL)		return;

	CString strLog;

	strLog.Format(_T("-------------------- Step Update Log ------------------\n\
				Step No : %d -> %d\n\
				Step Type : %d -> %d\n\
				Step Mode : %d -> %d\n\n\
				Discharge Vref : %d -> %d\n\
				Charge Vref : %d -> %d\n\
				Iref : %d -> %d\n\
				Pref : %d -> %d\n\n\
				EndTimeDay : %d -> %d\n\
				EndTime : %d -> %d\n\
				EndI : %d -> %d\n\
				EndC : %d -> %d\n\
				EndWatt : %d -> %d\n\
				EndV_L : %d -> %d\n\
				EndV_H : %d -> %d\n\n\
				LowV : %d -> %d\n\
				HighV : %d -> %d\n\
				LowI : %d -> %d\n\
				HighI : %d -> %d\n\
				LowC : %d -> %d\n\
				HighC : %d -> %d\n\
				LowZ : %d -> %d\n\
				HighZ : %d -> %d\n\n\
//				CellDeltaVStep : %d -> %d\n\
				Record_DeltaV : %d -> %d\n\
				Record_DeltaI : %d -> %d\n\
				Record_Time : %d -> %d\n\
		"),
		pSrc->stepHeader.stepNo, pDst->stepHeader.stepNo,
		pSrc->stepHeader.nStepTypeID, pDst->stepHeader.nStepTypeID,
		pSrc->stepHeader.mode, pDst->stepHeader.mode,
		pSrc->stepReferance->lVref_DisCharge, pDst->stepReferance->lVref_DisCharge,
		pSrc->stepReferance->lVref_Charge, pDst->stepReferance->lVref_Charge,
		pSrc->stepReferance->lIref, pDst->stepReferance->lIref,
		pSrc->stepReferance->lPref, pDst->stepReferance->lPref,
		pSrc->stepReferance->ulEndTimeDay, pDst->stepReferance->ulEndTimeDay,
		pSrc->stepReferance->ulEndTime, pDst->stepReferance->ulEndTime,
		pSrc->stepReferance->lEndI, pDst->stepReferance->lEndI,
		pSrc->stepReferance->lEndC, pDst->stepReferance->lEndC,
		pSrc->stepReferance->lEndWatt, pDst->stepReferance->lEndWatt,
		pSrc->stepReferance->lEndV_L, pDst->stepReferance->lEndV_L,
		pSrc->stepReferance->lEndV_H, pDst->stepReferance->lEndV_H,
		pSrc->lLowV, pDst->lLowV,
		pSrc->lHighV, pDst->lHighV,
		pSrc->lLowI, pDst->lLowI,
		pSrc->lHighI, pDst->lHighI,
		pSrc->lLowC, pDst->lLowC,
		pSrc->lHighC, pDst->lHighC,
		pSrc->lLowZ, pDst->lLowZ,
		pSrc->lHighZ, pDst->lHighZ,
//		pSrc->lCellDeltaVStep, pDst->lCellDeltaVStep,
		pSrc->conREC.lDeltaV ,pDst->conREC.lDeltaV,
		pSrc->conREC.lDeltaI ,pDst->conREC.lDeltaI,
		pSrc->conREC.lTime ,pDst->conREC.lTime
		);


	m_pCH->WriteSchUpdateLog(strLog);


}

//ksj 20170811 : 기존 SCH 파일을 읽어 특정 스텝만 변경하여 업데이트한다.
BOOL CSchUpdateDlg::UpdateSchStep()
{
	CString strTemp;
	CString strLogMsg;
	CString strContent = m_pCH->GetDataPath();
	CTime tTime = CTime::GetCurrentTime();
	CScheduleData stScheduleData;
	CStep* pStep = NULL;
	CString strLatestSchFileName = m_pCH->GetDataPath() + "\\" + m_pCH->GetTestName() + ".sch";
	CString strUpdateFileName;
	CString strTime;
	strTime.Format(_T("%d%02d%02d_%02d%02d%02d"),tTime.GetYear(),tTime.GetMonth(),tTime.GetDay(),tTime.GetHour(),tTime.GetMinute(),tTime.GetSecond());	
	
	strLogMsg.Format("%s\\Update", strContent);	
	::CreateDirectory(strLogMsg, NULL);	
	strLogMsg.Format("%s\\Update\\Update_%s", strContent, strTime);
	strContent	= strLogMsg;
	
	if(stScheduleData.SetSchedule(strLatestSchFileName)) //마지막 업데이트된 sch 파일(대표 sch)을 open 한다.
	{
		pStep = stScheduleData.GetStepData(m_sStepInfoData.stepHeader.stepNo-1); //변경할 스텝 정보 로드
		if(pStep == NULL) return FALSE;

		COleDateTime  date = COleDateTime::GetCurrentTime();
		sprintf((char*)stScheduleData.GetScheduleEditTime(), "%s", date.Format()); //ksj 20170811 : 스케쥴 변경 시간도 현재 시간으로 바꾼다.
		//스텝 정보 변조.
		pStep->m_type = m_sStepInfoData.stepHeader.nStepTypeID;		
		pStep->m_mode = m_sStepInfoData.stepHeader.mode;
		pStep->m_fVref_DisCharge = m_sStepInfoData.stepReferance->lVref_DisCharge / 1000.0f;
		pStep->m_fVref_Charge = m_sStepInfoData.stepReferance->lVref_Charge / 1000.0f;
		pStep->m_fIref = m_sStepInfoData.stepReferance->lIref / 1000.0f;
		pStep->m_fPref = m_sStepInfoData.stepReferance->lPref / 1000.0f;
		pStep->m_lEndTimeDay = m_sStepInfoData.stepReferance->ulEndTimeDay;
		pStep->m_fEndTime = m_sStepInfoData.stepReferance->ulEndTime / 100.0f;
		pStep->m_fEndI = m_sStepInfoData.stepReferance->lEndI / 1000.0f;
		pStep->m_fEndC = m_sStepInfoData.stepReferance->lEndC;
		pStep->m_fEndW = m_sStepInfoData.stepReferance->lEndWatt / 1000.0f;
		pStep->m_fEndWh = m_sStepInfoData.stepReferance->lEndWattHour / 1000.0f;
		pStep->m_fEndV_L = m_sStepInfoData.stepReferance->lEndV_L / 1000.0f;
		pStep->m_fEndV_H = m_sStepInfoData.stepReferance->lEndV_H / 1000.0f;
		pStep->m_fLowLimitV = m_sStepInfoData.lLowV / 1000.0f;
		pStep->m_fHighLimitV = m_sStepInfoData.lHighV / 1000.0f;
		pStep->m_fLowLimitI = m_sStepInfoData.lLowI / 1000.0f;
		pStep->m_fHighLimitI = m_sStepInfoData.lHighI / 1000.0f;
		pStep->m_fLowLimitC = m_sStepInfoData.lLowC / 1000.0f;;
		pStep->m_fHighLimitC = m_sStepInfoData.lHighC / 1000.0f;;
		pStep->m_fLowLimitImp = m_sStepInfoData.lLowZ / 1000.0f;
		pStep->m_fHighLimitImp = m_sStepInfoData.lHighZ / 1000.0f;
		pStep->m_fDeltaV = m_sStepInfoData.conREC.lDeltaV / 1000.0f;
		pStep->m_fDeltaI = m_sStepInfoData.conREC.lDeltaI / 1000.0f;
		pStep->m_fReportTime = m_sStepInfoData.conREC.lTime / 100.0f;		

		/////////////////////////////
		
		pStep->m_fCellDeltaVStep = m_sStepInfoData.lCellDeltaVStep / 1000.0f; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_fStepDeltaAuxTemp = m_sStepInfoData.faultcompAuxTemp ; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_fStepDeltaAuxTH = m_sStepInfoData.faultcompAuxTh / 1000.0f; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_fStepDeltaAuxT = m_sStepInfoData.faultcompAuxT; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_fStepDeltaAuxVTime = m_sStepInfoData.faultcompAuxV_Time; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_fStepAuxV = m_sStepInfoData.faultcompAuxV / 1000.0f; //ksj 20200216 : V1015 누락 사항 추가

		pStep->m_bStepDeltaVentAuxV = m_sStepInfoData.faultCompAuxV_Vent_flag; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_bStepDeltaVentAuxTemp = m_bChk_step_delta_aux_temp; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_bStepDeltaVentAuxTh = m_bChk_step_delta_aux_th; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_bStepDeltaVentAuxT = m_bChk_step_delta_aux_t; //ksj 20200216 : V1015 누락 사항 추가
		pStep->m_bStepVentAuxV = m_bChk_step_aux_v; //ksj 20200216 : V1015 누락 사항 추가
		//스텝 정보 변조 끝... SBC와 싱크가 안맞을 수도 있다.
		
		if( ::CreateDirectory(strContent, NULL) == FALSE )
		{
			//strTemp.Format("Update 저장용 폴더 생성 실패(%s)", strLogMsg);
			strTemp.Format(Fun_FindMsg("SchUpdateDlg_UpdateSchStep_msg1","IDD_SCH_UPDATE_DLG"), strLogMsg);//&&
			m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		else //폴더 생성 성공.
		{
			strUpdateFileName = strContent + "\\" + m_pCH->GetTestName() + ".sch";

			UINT ProtocolVersion = (UINT)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Protocol Version",0); //yulee 20180829 
			if(FALSE == stScheduleData.SaveToFile(strUpdateFileName)) //변조한 파일을 Update 폴더아래
			{
				//strLogMsg.Format("%s Schedule 파일 생성 실패", strUpdateFileName);
				strLogMsg.Format(Fun_FindMsg("SchUpdateDlg_UpdateSchStep_msg2","IDD_SCH_UPDATE_DLG"), strUpdateFileName);//&&
				m_pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			else //파일 저장 성공
			{
				DeleteFile(strLatestSchFileName); //상위 폴더의 대표 sch 제거
				CopyFile(strUpdateFileName,strLatestSchFileName,FALSE); //Update 된 sch 파일로 대체
			}		
		}		
	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}

void CSchUpdateDlg::DisableUpdate()
{
	m_bUpdate = FALSE;
	GetDlgItem(IDOK)->EnableWindow(m_bUpdate);
}

void CSchUpdateDlg::EnableUpdate()
{
	m_bUpdate = TRUE;
	GetDlgItem(IDOK)->EnableWindow(m_bUpdate);
}

void CSchUpdateDlg::OnChangeEditStepNo() 
{
	EnableUpdate();
		
}

void CSchUpdateDlg::OnEditchangeComboStepType() 
{
	EnableUpdate();	
}

void CSchUpdateDlg::OnEditchangeComboStepMode() 
{
	EnableUpdate();	
}

void CSchUpdateDlg::OnChangeEditStepCVolt() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditStepDVolt() 
{
	EnableUpdate();	
}

void CSchUpdateDlg::OnChangeEditStepRefI() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditStepRefP() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditStepEndVoltH() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditStepEndVoltL() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditStepEndI() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditStepEndTimeDay() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	EnableUpdate();
	
	*pResult = 0;
}

void CSchUpdateDlg::OnChangeEditMs() 
{
	EnableUpdate();	
}

void CSchUpdateDlg::OnChangeEditStepEndAh() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditStepEndWh() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditStepEndP() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditSafetyVLow() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditSafetyVHigh() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditSafetyILow() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditSafetyIHigh() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditSafetyCLow() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditSafetyCHigh() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditSafetyImpLow() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditSafetyImpHigh() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnDatetimechangeDatetimepickerRecordTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	EnableUpdate();
	
	*pResult = 0;
}

void CSchUpdateDlg::OnChangeEditRecordTimeMs() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnChangeEditRecordDeltaV() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::OnChangeEditRecordDeltaA() 
{
	EnableUpdate();
	
}

void CSchUpdateDlg::InitList()
{

	if(!m_pCH) return;

	CString strTemp;
	CString strSchFileName;	
	CScheduleData schData;
	strSchFileName = m_pCH->GetScheduleFile();
	if(schData.SetSchedule(strSchFileName)== FALSE)
	{
		//AfxMessageBox("스케줄 정보를 읽을 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SchUpdateDlg_InitList_msg1","IDD_SCH_UPDATE_DLG"));//&&
		return;
	}

	DWORD style = 	m_ctrlList.GetExtendedStyle();
	//char szBuff[32];
	
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES;
	m_ctrlList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

	m_imgList.Create(IDB_CELL_STATE_ICON_P, 19, 14,RGB(255,255,255));
	m_ctrlList.SetImageList(&m_imgList, LVSIL_SMALL);

	// Column 삽입
	m_ctrlList.InsertColumn(0, "No",  LVCFMT_RIGHT,  40,  0);
	m_ctrlList.InsertColumn(1, "Type",  LVCFMT_CENTER,  90,  1);

	UpdateList();
}


void CSchUpdateDlg::OnClickUpdateStepList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_ctrlList.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_ctrlList.GetNextSelectedItem(pos);
		int nStepIndex = m_ctrlList.GetItemData(nItem);
				
		m_edit_step_no = nStepIndex+1;
		
		UpdateData(FALSE);
		OnButStepSchRequest();
	}
	*pResult = 0;
}

void CSchUpdateDlg::UpdateList()
{
	if(!m_pCH) return;

	m_nCurStepIndex = m_pCH->GetStepNo()-1; //현재 작업중인 스텝 정보
	
	CString strTemp;
	CString strSchFileName;	
	CScheduleData schData;
	strSchFileName = m_pCH->GetScheduleFile();
	if(schData.SetSchedule(strSchFileName)== FALSE)
	{
		//AfxMessageBox("스케줄 정보를 읽을 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SchUpdateDlg_UpdateList_msg1","IDD_SCH_UPDATE_DLG"));//&&
		return;
	}
	
	m_ctrlList.DeleteAllItems();

	char szBuff[32];

	CString strMode;
	CStep *pStep;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	for(int step =0; step < schData.GetStepSize(); step++)
	{
		lvItem.iItem = step;
		lvItem.iSubItem = 0;
		lvItem.iImage = -1;
		sprintf(szBuff,"%d", step+1);
		lvItem.pszText = szBuff; 
		
		if(m_nCurStepIndex == step)
		{
			lvItem.iImage = 9;
		}
		m_ctrlList.InsertItem(&lvItem);
		m_ctrlList.SetItemData(lvItem.iItem, step);
		
		pStep = schData.GetStepData(step);
		ASSERT(pStep);
		
		lvItem.iSubItem = 1;
		if(pStep->m_type == PS_STEP_CHARGE)				lvItem.iImage = 2;
		else if(pStep->m_type == PS_STEP_DISCHARGE)		lvItem.iImage = 1;
		else if(pStep->m_type == PS_STEP_REST)			lvItem.iImage = 11;
		else if(pStep->m_type == PS_STEP_OCV)			lvItem.iImage = 7;
		else if(pStep->m_type == PS_STEP_IMPEDANCE)		lvItem.iImage = 10;
		else if(pStep->m_type == PS_STEP_END)			lvItem.iImage = 4;
		else if(pStep->m_type == PS_STEP_PATTERN)		lvItem.iImage = 12;
		else if(pStep->m_type == PS_STEP_USER_MAP)		lvItem.iImage = 12;
		else lvItem.iImage = 8;
		
		sprintf(szBuff,"%s", 	schData.GetTypeString(pStep->m_type));
		lvItem.pszText = szBuff; 
		m_ctrlList.SetItem(&lvItem);
	}

}

void CSchUpdateDlg::OnChangeEditStepDeltaVolt() 
{
	EnableUpdate();
}

void CSchUpdateDlg::OnEnChangeEditCellDeltaVStep()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnEnChangeEditStepAuxtDelta()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnEnChangeEditStepAuxthDelta()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnEnChangeEditStepAuxtAuxthDelta()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnEnChangeEditStepAuxvTime()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnEnChangeEditStepAuxvDelta()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnBnClickedChkStepDeltaAuxV()
{
	EnableUpdate();

}


void CSchUpdateDlg::OnBnClickedChkStepDeltaAuxTemp()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnBnClickedChkStepDeltaAuxTh()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnBnClickedChkStepDeltaAuxT()
{
	EnableUpdate();
}


void CSchUpdateDlg::OnBnClickedChkStepAuxV()
{
	EnableUpdate();
}
