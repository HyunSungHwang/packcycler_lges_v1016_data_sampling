#if !defined(AFX_DLG_PWRSUPPLYSTAT_H__A908D77B_908E_4DEC_9F18_6610F3DFD674__INCLUDED_)
#define AFX_DLG_PWRSUPPLYSTAT_H__A908D77B_908E_4DEC_9F18_6610F3DFD674__INCLUDED_

#include "CTSMonProDoc.h"
#include "afxwin.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_PwrSupplyStat.h : header file
//

#define ID_PWRSUPPLY_TIMER_SIZE  3000

/////////////////////////////////////////////////////////////////////////////
// Dlg_PwrSupplyStat dialog

class Dlg_PwrSupplyStat : public CDialog
{
// Construction
public:
	Dlg_PwrSupplyStat(CCTSMonProDoc * pDoc,CWnd* pParent = NULL);   // standard constructor

	CCTSMonProDoc *m_pDoc;
	
	int m_nItem;
	
	void InitControl();
	void onSetState();
	void onSizeSave(int iType);

	BOOL onChkSbcRunState();
	BOOL onChkSerialIsPort();

// Dialog Data
	//{{AFX_DATA(Dlg_PwrSupplyStat)
	enum { IDD = IDD_DIALOG_PWRSUPPLY_STAT , IDD2 = IDD_DIALOG_PWRSUPPLY_STAT_ENG , IDD3 = IDD_DIALOG_PWRSUPPLY_STAT_PL };
	CLabel	m_StaticPwrSupplyVOLT2;
	CLabel	m_StaticPwrSupplyVOLT1;
	CEdit	m_EditPwrSupplyVOLT2;
	CEdit	m_EditPwrSupplyVOLT1;
	CLabel	m_StaticPwrSupplyComm;
	CLabel	m_StaticPwrSupplyOUTP;
	CLabel	m_StaticPwrSupplyINST;
	CLabel	m_StaticPwrSupplyID;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_PwrSupplyStat)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_PwrSupplyStat)
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnButPwrsupplyOutpOn();
	afx_msg void OnButPwrsupplyOutpOff();
	afx_msg void OnButPwrsupplyVolt1();
	afx_msg void OnButPwrsupplyVolt2();
	afx_msg void OnButPwrsupplyInst1();
	afx_msg void OnButPwrsupplyInst2();	
	afx_msg void OnWindowPosChanged(WINDOWPOS FAR* lpwndpos);	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	// yulee 20190702 Add
	CLabel m_StaticPwrAutoStopSetCh1;
	// yulee 20190702 Add
	CLabel m_StaticPwrAutoStopSetCh2;
	afx_msg void OnBnClickedButAutostop1();
	afx_msg void OnBnClickedButAutostop2();
	afx_msg void OnBnClickedButAutostopPause1();
	afx_msg void OnBnClickedButAutostopPause2();
	CLabel m_StaticPwrAutoStopAtPauseSetCh1;
	CLabel m_StaticPwrAutoStopAtPauseSetCh2;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_PWRSUPPLYSTAT_H__A908D77B_908E_4DEC_9F18_6610F3DFD674__INCLUDED_)
