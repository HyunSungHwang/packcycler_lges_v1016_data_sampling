#pragma once


// CSetFixSafetyDlg 대화 상자입니다.
class CCTSMonProDoc;
class CSetFixSafetyDlg : public CDialog
{
	DECLARE_DYNAMIC(CSetFixSafetyDlg)

public:
	CSetFixSafetyDlg(CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSetFixSafetyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SET_FIX_SAFETY_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CString m_strAuxTempLimitH;	
	CString m_strAuxVoltLimitH;
	CString m_strAuxVoltLimitL;	
	CCTSMonProDoc *m_pDoc;
	BOOL CheckRunState(void);
	BOOL UpdateAuxData(void);
};
