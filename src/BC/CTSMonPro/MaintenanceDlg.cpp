// MaintenanceDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "TextInputDlg.h"
#include "MaintenanceDlg.h"

#include "ModuleAddDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMaintenanceDlg dialog

CMaintenanceDlg::CMaintenanceDlg(CCTSMonProDoc *pDoc, bool bMaintenanceMode, CWnd* pParent /*=NULL*/)
//	: CDialog(CMaintenanceDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CMaintenanceDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CMaintenanceDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CMaintenanceDlg::IDD3):
	(CMaintenanceDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CMaintenanceDlg)
	m_nRackID = 1;
	m_bUseAutoRestarting = FALSE;
	m_nWaitTimeForAutoRestart = 0;
	m_nInstalledModuleNum = 0;
	//}}AFX_DATA_INIT

	m_nCurSelIndex = 0;
	m_pSelectedItem = new BYTE[((CCTSMonProApp*)AfxGetApp())->GetColNum()];
	ZeroMemory(m_pSelectedItem, ((CCTSMonProApp*)AfxGetApp())->GetColNum());

	m_bMaintenanceMode = bMaintenanceMode;

	ASSERT(pDoc);
	m_pDoc = pDoc;
	m_bChanged = FALSE;
	m_pChStsSmallImage= NULL;
}

CMaintenanceDlg::~CMaintenanceDlg()
{
	if( m_pSelectedItem )
		delete [] m_pSelectedItem;

	if( m_pChStsSmallImage )
		delete m_pChStsSmallImage;
}

void CMaintenanceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMaintenanceDlg)
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_BTN_DELETE, m_btnDelete);
	DDX_Control(pDX, IDC_BTN_ADD, m_btnAdd);
	DDX_Control(pDX, IDC_LIST_CHANNEL_DP_ITEM, m_wndChannelList);
	DDX_Control(pDX, IDC_LIST_MODULE_INFO, m_wndModuleList);
	DDX_Control(pDX, IDC_IPADDRESS1, m_ctrlServerAddress);
	DDX_Text(pDX, IDC_EDIT_UNIT_ID, m_nRackID);
	DDX_Check(pDX, IDC_AUTORESTART, m_bUseAutoRestarting);
	DDX_Text(pDX, IDC_EDIT_WAITTIME_AUTORESTART, m_nWaitTimeForAutoRestart);
	DDX_Text(pDX, IDC_TOTAL_MD_EDIT, m_nInstalledModuleNum);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMaintenanceDlg, CDialog)
	//{{AFX_MSG_MAP(CMaintenanceDlg)
	ON_BN_CLICKED(IDC_BTN_ADD, OnBtnAdd)
	ON_BN_CLICKED(IDC_BTN_DELETE, OnBtnDelete)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CHANNEL_DP_ITEM, OnRclickListChannelDpItem)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CHANNEL_DP_ITEM, OnDblclkListChannelDpItem)
	ON_BN_CLICKED(IDC_AUTORESTART, OnAutorestart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaintenanceDlg message handlers

BOOL CMaintenanceDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_nSetDPItemNum = 0;
	
	int nItemNum;
	CCTSMonProApp* pApp = (CCTSMonProApp *)AfxGetApp();
	CString strTmp;
	LVITEM lvItem;	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;
	
	m_nInstalledModuleNum = m_pDoc->GetInstallModuleCount();

	m_nRackID = pApp->GetProfileInt(CT_CONFIG_REG_SEC, "Rack ID", 1);

//	{// 1. General Information
//		CString	strIP			= pApp->GetProfileString(CT_CONFIG_REG_SEC, "Control Address");
//
//		BYTE byAddress[4];
//		DWORD dwIP = inet_addr(strIP);
//		byAddress[0] = (BYTE) (dwIP >> 24) ;
//		byAddress[1] = (BYTE) (dwIP >> 16);
//		byAddress[2] = (BYTE) (dwIP >> 8);
//		byAddress[3] = (BYTE) (dwIP >> 0);
//		m_ctrlServerAddress.SetAddress(byAddress[3], byAddress[2], byAddress[1], byAddress[0]);
//	}	

	{// 2. Module Information
		DWORD dwExStyle = m_wndModuleList.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		m_wndModuleList.SetExtendedStyle(dwExStyle);
		m_wndModuleList.InsertColumn(0, "ModuleID", LVCFMT_RIGHT, 80);
		m_wndModuleList.InsertColumn(1, Fun_FindMsg("OnInitDialog_msg1","IDD_MAINTENANCE_DLG"), LVCFMT_RIGHT, 60);
		//@ m_wndModuleList.InsertColumn(1, "채널수", LVCFMT_RIGHT, 60);

		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0))
		{
			m_wndModuleList.InsertColumn(2, "V Spec(uV)", LVCFMT_RIGHT, 60);
		}
		else
		{
			m_wndModuleList.InsertColumn(2, "V Spec(mV)", LVCFMT_RIGHT, 60);
		}
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0))
		{
			m_wndModuleList.InsertColumn(3, "I Spec(uA)", LVCFMT_RIGHT, 60);
		}
		else
		{
			m_wndModuleList.InsertColumn(3, "I Spec(mA)", LVCFMT_RIGHT, 60);
		}

		m_wndModuleList.InsertColumn(4, Fun_FindMsg("OnInitDialog_msg2","IDD_MAINTENANCE_DLG"), LVCFMT_RIGHT, 50);
		//@ m_wndModuleList.InsertColumn(4, "전압Range수", LVCFMT_RIGHT, 50);
		m_wndModuleList.InsertColumn(5, Fun_FindMsg("OnInitDialog_msg3","IDD_MAINTENANCE_DLG"), LVCFMT_RIGHT, 50);
		//@ m_wndModuleList.InsertColumn(5, "전류Range수", LVCFMT_RIGHT, 50);
		m_wndModuleList.InsertColumn(6, Fun_FindMsg("OnInitDialog_msg4","IDD_MAINTENANCE_DLG"), LVCFMT_RIGHT, 100);
		//@ m_wndModuleList.InsertColumn(6, "IP 주소", LVCFMT_RIGHT, 100);
		m_wndModuleList.InsertColumn(7, Fun_FindMsg("OnInitDialog_msg5","IDD_MAINTENANCE_DLG"), LVCFMT_RIGHT, 70);
		//@ m_wndModuleList.InsertColumn(7, "제어기 Type", LVCFMT_RIGHT, 70);
		
		UpdateModuleList();
	}

	{// 3. Channel Information

//		m_pChStsSmallImage = new CImageList;
//		m_pChStsSmallImage->Create(IDB_CHECK,16,5,RGB(255,255,255));
//		m_wndChannelList.SetImageList(m_pChStsSmallImage,LVSIL_SMALL);

		DWORD dwExStyle = m_wndChannelList.GetExtendedStyle();
		dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES ;
		m_wndChannelList.SetExtendedStyle(dwExStyle);
		m_wndChannelList.InsertColumn(0, "Column Name", LVCFMT_CENTER, 120);
		m_wndChannelList.InsertColumn(1, "Width", LVCFMT_CENTER, 50);
		m_nChannelDPItemNum = pApp->GetProfileInt("DisplayItem", "ItemNum", 4);

		CString strTitle;
		for( int nX = 0; nX < pApp->GetColNum(); nX++ )
		{
			nItemNum = 0;
/*			for( int nI = 0; nI < m_nChannelDPItemNum; nI++ )
			{
				strTmp.Format("Col%02d", nI+1);
				strTitle = pApp->GetProfileString("DisplayItem", strTmp);
				
				if( pApp->GetColName(nX).Compare(strTitle) == FALSE )
				{
					strTmp.Format("Width%02d", nI+1);
					nItemNum = pApp->GetProfileInt("DisplayItem", strTmp, 50);
					m_pSelectedItem[nX] = TRUE;
					break;
				}
			}

			if( m_pSelectedItem[nX] )
			{
				lvItem.iImage = 1;
				m_nSetDPItemNum++;
			}
			else
			{
				lvItem.iImage = 0;
			}
*/			lvItem.iItem = nX;
			lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
			lvItem.iSubItem = 0;
			lvItem.pszText = (LPSTR)(LPCTSTR)pApp->GetColName(nX);
			m_wndChannelList.InsertItem(&lvItem);

			
			strTmp.Format("%d", nItemNum);
			m_wndChannelList.SetItemText(lvItem.iItem, 1, strTmp);
		}
	}

	{//4. Operating Configuration
		m_bUseAutoRestarting = pApp->GetProfileInt(CT_CONFIG_REG_SEC, "AutoRestart", 1);
		m_nWaitTimeForAutoRestart = pApp->GetProfileInt(CT_CONFIG_REG_SEC, "AutoRestartRemainTime", 30);
		GetDlgItem(IDC_EDIT_WAITTIME_AUTORESTART)->EnableWindow(m_bUseAutoRestarting);

		pApp->WriteProfileInt(CT_CONFIG_REG_SEC, "AutoRestart", m_bUseAutoRestarting);
	}
	UpdateData(FALSE);

	if( m_bMaintenanceMode == false )
	{
		GetDlgItem(IDC_EDIT_UNIT_ID)->EnableWindow(FALSE);
		GetDlgItem(IDC_IPADDRESS1)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_ADD)->EnableWindow(FALSE);
		GetDlgItem(IDC_BTN_DELETE)->EnableWindow(FALSE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMaintenanceDlg::OnDblclkListChannelDpItem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	POSITION pos = m_wndChannelList.GetFirstSelectedItemPosition();
	int nIndex = m_wndChannelList.GetNextSelectedItem(pos);
	m_pSelectedItem[nIndex] = BYTE(m_pSelectedItem[nIndex]==TRUE? FALSE : TRUE);

	LVITEM lvItem;	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.iItem = nIndex;
	lvItem.iSubItem = 0;
	lvItem.mask = LVIF_TEXT | LVIF_IMAGE;
	if( m_pSelectedItem[nIndex] == TRUE )
	{
		lvItem.iImage = 1;
		m_nSetDPItemNum++;
	}
	else
	{
		lvItem.iImage = 0;
		m_nSetDPItemNum--;
	}

	lvItem.pszText = (LPSTR)(LPCTSTR)((CCTSMonProApp *)AfxGetApp())->GetColName(nIndex);
	m_wndChannelList.SetItem(&lvItem);
	
	*pResult = 0;
}

void CMaintenanceDlg::OnBtnAdd() 
{
	UpdateData();

	CModuleAddDlg *pDlg;
	pDlg = new CModuleAddDlg;
	if(pDlg == NULL)	return;

	static int nNewModuleID = 0;
	if(nNewModuleID == 0)
	{
		//가장 마지막 모듈 번호를 기본으로 표시
		pDlg->m_nModuleID = m_pDoc->GetModuleID(m_pDoc->GetInstallModuleCount()-1)+1;
	}
	else
	{
		pDlg->m_nModuleID = nNewModuleID+1;
	}

	if(IDCANCEL == pDlg->DoModal())	{
		delete pDlg;
		pDlg = NULL;
		return;
	}

	nNewModuleID = pDlg->m_nModuleID;
	
	delete pDlg;
	pDlg = NULL;

	CString strText;
	if(AddModule(nNewModuleID))
	{
		LVITEM lvItem;	ZeroMemory(&lvItem, sizeof(LVITEM));
		lvItem.mask = LVIF_TEXT;
		lvItem.iItem = m_wndModuleList.GetItemCount();

		lvItem.iSubItem = 0;
		strText.Format("%s", m_pDoc->GetModuleName(nNewModuleID));
		lvItem.pszText = (LPSTR)(LPCTSTR)strText;
		m_wndModuleList.InsertItem(&lvItem);
		m_wndModuleList.SetItemData(lvItem.iItem, nNewModuleID);

		m_nInstalledModuleNum++;
		m_bChanged = TRUE;
		UpdateData(FALSE);
	}
}

void CMaintenanceDlg::OnBtnDelete() 
{
	int nSelCount = m_wndModuleList.GetSelectedCount();
	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
	int nSelectIndex; 
	int nModuleID;

	CString strMsg;
	if(nSelCount >= 0)		//여러개의 모듈 선택시
	{
		strMsg.Format(Fun_FindMsg("OnBtnDelete_msg1","IDD_MAINTENANCE_DLG"), nSelCount);
		//@ strMsg.Format("선택한 %d개의 모듈을 삭제 하시겠습니까?", nSelCount);
	}
	else if(nSelCount == 1)	//1개의 모듈 선택시
	{
		if(pos)
		{
			nSelectIndex = m_wndModuleList.GetNextSelectedItem(pos);
			nModuleID = m_wndModuleList.GetItemData(nSelectIndex);
			strMsg.Format(Fun_FindMsg("OnBtnDelete_msg2","IDD_MAINTENANCE_DLG"), m_pDoc->GetModuleName(nModuleID));
			//@ strMsg.Format("%s를 삭제 하시겠습니까?", m_pDoc->GetModuleName(nModuleID));
		}
		else
		{
			strMsg = Fun_FindMsg("OnBtnDelete_msg3","IDD_MAINTENANCE_DLG");
			//@ strMsg = "선택한 모듈이 없습니다.";
			AfxMessageBox(strMsg);
			return;
		}
	}
	else					//선택된 모듈이 없을 시
	{
		strMsg = Fun_FindMsg("OnBtnDelete_msg","IDD_MAINTENANCE_DLG");
		AfxMessageBox(strMsg);
		return;
	}

	
	if(IDYES == AfxMessageBox(strMsg, MB_YESNO|MB_ICONQUESTION))
	{
		CDWordArray selArray;
		pos = m_wndModuleList.GetFirstSelectedItemPosition();
		while(pos)
		{
			selArray.Add((DWORD)m_wndModuleList.GetNextSelectedItem(pos));
		}

		for(int i = selArray.GetSize(); i>0; i--)
		{
			nModuleID = m_wndModuleList.GetItemData(selArray[i-1]);
			//delete from database
			if(DeleteModule(nModuleID))
			{
				m_wndModuleList.DeleteItem(selArray[i-1]);
				m_nInstalledModuleNum--;
			}
		}

		m_bChanged = TRUE;
		UpdateData(FALSE);
	}
}

void CMaintenanceDlg::OnOK() 
{
	if(m_bChanged)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_MAINTENANCE_DLG"));
		//@ AfxMessageBox("프로그램을 종료후 재실행하여야 변경내용이 적용됩니다.\n");
	}

	UpdateData();
	
/*	CCTSMonProApp* pApp = (CCTSMonProApp *)AfxGetApp();
		
	{// 1. General Information
		pApp->WriteProfileInt(CT_CONFIG_REG_SEC, "Rack ID", m_nRackID);
		
		CString strIP;
		BYTE byAddress[4];
		m_ctrlServerAddress.GetAddress(byAddress[0], byAddress[1], byAddress[2], byAddress[3]);
		strIP.Format("%d.%d.%d.%d", byAddress[0], byAddress[1], byAddress[2], byAddress[3]);
		pApp->WriteProfileString(CT_CONFIG_REG_SEC, "Control Address", strIP);
	}	

	{// 3. Channel Information
		int nInstallIndex = 1;
		CString strTitle;
		for( int nX = 0; nX < pApp->GetColNum(); nX++ )
		{
			if( m_pSelectedItem[nX] == TRUE )
			{
				strTitle.Format("Title%02d", nInstallIndex);
				pApp->WriteProfileString("DisplayItem", strTitle, pApp->GetColName(nX));
				strTitle.Format("Width%02d", nInstallIndex++);
				pApp->WriteProfileInt("DisplayItem", strTitle, 50);
			}
		}

		if( nInstallIndex < pApp->GetColNum() )
		{
			for( int nI = nInstallIndex; nI < pApp->GetColNum(); nI++ )
			{
				strTitle.Format("Title%02d", nI+1);
				pApp->WriteProfileString("DisplayItem", strTitle, "");
				strTitle.Format("Width%02d", nI+1);
				pApp->WriteProfileInt("DisplayItem", strTitle, 0);
			}
		}

		pApp->WriteProfileInt("DisplayItem", "ItemNum", m_nSetDPItemNum);
	}

	{//4. Operating Configuration
		pApp->WriteProfileInt(CT_CONFIG_REG_SEC, "AutoRestart", m_bUseAutoRestarting);

		if( m_bUseAutoRestarting )
		{
			if( m_nWaitTimeForAutoRestart <= 0 )
			{
				MessageBox("AutoRestarting 기능을 사용하기 위해선 Wait Time이 필요합니다.");
				return;
			}
			pApp->WriteProfileInt(CT_CONFIG_REG_SEC, "AutoRestartRemainTime", m_nWaitTimeForAutoRestart);
		}		
	}
*/	CDialog::OnOK();
}

void CMaintenanceDlg::OnRclickListChannelDpItem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	POSITION pos = m_wndChannelList.GetFirstSelectedItemPosition();
	if( pos == NULL )
		return;

	int nCurSelIndex = m_wndChannelList.GetNextSelectedItem(pos);
	CString strPrevData = m_wndChannelList.GetItemText(nCurSelIndex, 0);

	CRect rect;
	POINT point;
	WINDOWPLACEMENT wndPl;
	GetDlgItem(IDC_LIST_CHANNEL_DP_ITEM)->GetClientRect(rect);
	GetDlgItem(IDC_LIST_CHANNEL_DP_ITEM)->GetWindowPlacement(&wndPl);

	m_wndChannelList.GetItemPosition(nCurSelIndex, &point);
	rect.left	+= wndPl.rcNormalPosition.left;
	rect.top	+= wndPl.rcNormalPosition.top+point.y;

	GetWindowPlacement(&wndPl);
	rect.left	+= wndPl.rcNormalPosition.left+5;
	rect.top	+= wndPl.rcNormalPosition.top+23;
	rect.bottom	=  rect.top+18;
	rect.right	=  rect.left+rect.right;

	m_nCurSelIndex = nCurSelIndex;
	CTextInputDlg dlg(strPrevData, 0x00, &rect);
	if( dlg.DoModal() == IDOK )
		OnUpdateChannelColString(TRUE, dlg.m_strData);
	else
		OnUpdateChannelColString(FALSE);

	*pResult = 0;
}

void CMaintenanceDlg::OnUpdateChannelColString(BOOL bUpdate, CString strData)
{
	if( bUpdate )
	{
		CCTSMonProApp* pApp = (CCTSMonProApp *)AfxGetApp();
		pApp->SetColName(m_nCurSelIndex, strData);
		
		m_wndChannelList.SetItemText(m_nCurSelIndex, 0, strData);
	}
}

BOOL CMaintenanceDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_RETURN:	
			return FALSE;
		case VK_ESCAPE:
			return FALSE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CMaintenanceDlg::OnAutorestart() 
{
	UpdateData();
	GetDlgItem(IDC_EDIT_WAITTIME_AUTORESTART)->EnableWindow(m_bUseAutoRestarting);
}

void CMaintenanceDlg::UpdateModuleList()
{
	CString strTmp;
	LVITEM lvItem;	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;
	int nModuleID;
	CString strTemp, strTemp2;
//	double dVSpec = 0, dISpec =0;
	int nTemp1, nTemp2;
	int a;
	for( int nI = 0; nI < m_nInstalledModuleNum; nI++ )
	{
		//Module ID
		nModuleID = m_pDoc->GetModuleID(nI);
		CCyclerModule *mi = m_pDoc->GetModuleInfo(nModuleID);
		strTmp.Format("%s", m_pDoc->GetModuleName(nModuleID));
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = (LPSTR)(LPCTSTR)strTmp;
		m_wndModuleList.InsertItem(&lvItem);
		m_wndModuleList.SetItemData(lvItem.iItem, nModuleID);
		
		if(mi == NULL)	continue;

		strTemp.Format("%d", mi->GetTotalChannel());
		m_wndModuleList.SetItemText(lvItem.iItem, 1, strTemp);			//"채널수"		

		nTemp1 = mi->GetVoltageRangeCount(); 
		nTemp2 = mi->GetCurrentRangeCount();
		
		strTemp.Empty();
		for(a =0; a<nTemp1; a++)
		{
			strTemp2.Format("%d", (long)mi->GetVoltageSpec(a));
			strTemp += strTemp2;
			if( a < nTemp1-1)	strTemp += "/";	
		}
		m_wndModuleList.SetItemText(lvItem.iItem, 2, strTemp );			//"V Spec"

		strTemp.Empty();
		for(a =0; a<nTemp1; a++)
		{
			strTemp2.Format("%d", (long)mi->GetCurrentSpec(a));
			strTemp += strTemp2;
			if( a < nTemp1-1)	strTemp += "/";	
		}
		m_wndModuleList.SetItemText(lvItem.iItem, 3, strTemp );			//"I Spec"
		
		strTemp.Format("%d",  nTemp1);
		m_wndModuleList.SetItemText(lvItem.iItem, 4, strTemp	);		//"전압Range"	

		strTemp.Format("%d", nTemp2 );
		m_wndModuleList.SetItemText(lvItem.iItem, 5, strTemp	);		//"전류Range"	
		
		m_wndModuleList.SetItemText(lvItem.iItem, 6, m_pDoc->GetModuleIPAddress(nModuleID, FALSE)	);	//"IP"			
		
		m_wndModuleList.SetItemText(lvItem.iItem, 7, ""	);	//"제어기 Type"	
		
	}
}

BOOL CMaintenanceDlg::DeleteModule(int nModuleID)
{
	CDaoDatabase  db;

	CString strSQL;
	strSQL.Format("DELETE FROM SystemConfig WHERE ModuleID = %d", nModuleID);
	try
	{
		db.Open(m_pDoc->GetDataBaseName());
		db.Execute(strSQL);
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	strSQL.Format(Fun_FindMsg("DleteModule_msg","IDD_MAINTENANCE_DLG"), m_pDoc->GetModuleName(nModuleID));
	//@ strSQL.Format("%s가 삭제 되었습니다.", m_pDoc->GetModuleName(nModuleID));
	m_pDoc->WriteSysLog(strSQL);
	
	return TRUE;
}

BOOL CMaintenanceDlg::AddModule(int nModuleID)
{
	CDaoDatabase  db;
	CString strSQL;
	
	//DB에 존제 여부 검사 
	strSQL.Format("SELECT ModuleID FROM SystemConfig WHERE ModuleID = %d", nModuleID);
	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	//이미 존재한다.
	if(rs.IsBOF())
	{
		TRACE("Begin of recordset\n");
	}
	if(rs.IsEOF())
	{
		TRACE("End of recordset\n");
	}
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		rs.Close();
		db.Close();
		strSQL.Format(Fun_FindMsg("AddModule_msg","IDD_MAINTENANCE_DLG"), m_pDoc->GetModuleName(nModuleID));
		//@ strSQL.Format("%s (은)는 이미 등록되어 있습니다.", m_pDoc->GetModuleName(nModuleID));
		AfxMessageBox(strSQL);
		return FALSE;
	}
	rs.Close();

	strSQL.Format("INSERT INTO SystemConfig(ModuleID) VALUES (%d)", nModuleID);
	try
	{
		db.Execute(strSQL);
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	return TRUE;
}