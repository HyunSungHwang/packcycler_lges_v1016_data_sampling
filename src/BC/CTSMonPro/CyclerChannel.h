// CyclerChannel.h: interface for the CCyclerChannel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CYCLERCHANNEL_H__F1065011_05CD_44C3_863C_837DF75454A2__INCLUDED_)
#define AFX_CYCLERCHANNEL_H__F1065011_05CD_44C3_863C_837DF75454A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CalibrationData.h"
#include "ModuleIndexTable.h"
#include "FtpDownLoad.h"
#include "CellBALCtrl/CtrlSocketHeader.h"
#include "CellBALCtrl/CtrlSocketClient.h"
#include "MainCtrl/MainCtrl.h"

/*
//ADP KBH 2005/06/08
//Module에서 PC로 전송되는 정보를 파일로 저장하기 위한 구조체
typedef struct s_Ch_File_Save		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	long lVoltage;				// Result Data...
	long lCurrent;
	long lCapacity;
	long lWatt;
	long lWattHour;
	ULONG ulStepTime;			// 이번 Step 진행 시간
	ULONG ulTotalTime;			// 시험 Total 진행 시간
	long lImpedance;			// Impedance (AC or DC)
	long lTemparature;
	long lPressure;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	long	lAvgVoltage;
	long	lAvgCurrent;

	long	nIndexFrom;
	long	nIndexTo;
	
} CT_CH_SAVE_DATA, *LPCT_CH_SAVE_DATA;
*/
/*
typedef struct s_StepEndFileHeader
{
	char szStartTime[64];
	char szEndTime[64];
	char szSerial[64];
	char szUserID[32];
	char szDescript[128];
	char szBuff[128];
} CT_STEP_END_FILE_HEADER;

typedef struct s_Ch_StepEnd_Record		
{
	BYTE chNo;					// Channel Number
	BYTE chStepNo;
	BYTE chState;				// Run, Stop(Manual, Error), End
	BYTE chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	BYTE chDataSelect;			// For Display Data, For Saving Data
	BYTE chCode;
	BYTE chGradeCode;
	BYTE chReserved;

	float fVoltage;				// Result Data...
	float fCurrent;
	float fCapacity;
	float fWatt;
	float fWattHour;
	float fStepTime;			// 이번 Step 진행 시간
	float fTotalTime;			// 시험 Total 진행 시간
	float fImpedance;			// Impedance (AC or DC)
	float fTemparature;
	float fPressure;
	ULONG nCurrentCycleNum;
	ULONG nTotalCycleNum;
	ULONG lSaveSequence;			// Expanded Field
	float fAvgVoltage;
	float fAvgCurrent;

	long	nIndexFrom;
	long	nIndexTo;

	float fReserved[16];
	
} CT_STEP_END_RECORD, *LPCT_STEP_END_RECORD;

typedef struct s_RawFileHeader 
{
	PS_FILE_ID_HEADER	fileHeader;
	PS_RECORD_FILE_HEADER rsHeader;
} CT_RAW_FILE_HEADER;
*/


typedef struct s_Temp_File
{
	char szFilePath[256];
	char szLot[64];
	char szWorker[64];
	char szDescript[128];
	char szCellDeltaVolt[10];	//ljb 20150806 add
	char szReserved[502];
} CT_TEMP_FILE_DATA;

//#define RAWDATA_FILE_COL_SIZE	6
//#define TABEL_FILE_COLUMN_HEAD 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temperature,Press,VoltageAverage,CurrentAverage,Sequence,"
//20080813 KBH
#define TABEL_FILE_COLUMN_HEAD_v1009 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temperature,Press,VoltageAverage,CurrentAverage,Sequence,ChargeAh,DisChargeAh,Capacitance,ChargeWh,DisChargeWh,CVTime,SyncDate,SyncTime,AccCycle"
//ljb 20101227 
//#define TABEL_FILE_COLUMN_HEAD 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Voltage,Current,Capacity,Power,WattHour,IR,OvenT,OvenH,VoltageAverage,CurrentAverage,Sequence,ChargeAh,DisChargeAh,Capacitance,ChargeWh,DisChargeWh,CVTime,SyncDate,SyncTime,AccCycle1,AccCycle2,AccCycle3,AccCycle4,AccCycle5,MultiCycle1,MultiCycle2,MultiCycle3,MultiCycle4,MultiCycle5,CommState,Voltage Input,Voltage Power,Voltage Bus,OutputState,InputState,Code,Grade"
#define TABEL_FILE_COLUMN_HEAD 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Voltage,Current,Capacity,WattHour,IR,OvenT,OvenH,VoltageAverage,CurrentAverage,Sequence,ChargeAh,DisChargeAh,Capacitance,ChargeWh,DisChargeWh,CVTime,SyncDate,SyncTime,AccCycle1,AccCycle2,AccCycle3,AccCycle4,AccCycle5,MultiCycle1,MultiCycle2,MultiCycle3,MultiCycle4,MultiCycle5,CommState,Voltage Input,Voltage Power,Voltage Bus,OutputState,InputState,Code,Grade"
//20110319 KHS  Temp 포함
#define TABLE_FILE_COLUMN_HEAD2 "Module,Channel,StartTime,EndTime,StepNo,StepType,StateCode,StepTime,TotalTime,Voltage,Current,Capacity,Power,WattHour,Impedance,Temp,CurCycle,TotalCycle,AvgVoltage,AvgCurrent,ChargeCapacity,DischargeCapacity"
//20110319 KHS  Temp 미포함
//#define TABLE_FILE_COLUMN_HEAD2 "Module,Channel,StartTime,EndTime,StepNo,StepType,StateCode,StepTime,TotalTime,Voltage,Current,Capacity,Power,WattHour,Impedance,CurCycle,TotalCycle,AvgVoltage,AvgCurrent,ChargeCapacity,DischargeCapacity"
#define TABLE_FILE_COLUMN_HEAD3 "Module,Channel,StartTime,EndTime,StepNo,StepType,StateCode,StepTime,TotalTime,Voltage,Current,Capacity,Power,WattHour,Impedance,Temp,CurCycle,TotalCycle,AvgVoltage,AvgCurrent,ChargeCapacity,DischargeCapacity,LossTime,DataLoss"		//20120210 KHS
#define TABLE_FILE_COLUMN_HEAD4	"Module,Channel,Voltage,Current";
#define TABLE_FILE_COLUMN_HEAD5 "Module,Channel,StartTime,EndTime,StepNo,StepType,StateCode,StepTime,TotalTime,Voltage,Current" //yulee 20181124
//#define TABLE_FILE_COLUMN_HEAD6 "CycleNo,State,EndTime,Voltage,Current,Capacity,WattHour" //lyj 20200521
#define TABLE_FILE_COLUMN_HEAD6 "채널,작업명,스케쥴명,Step,Type,코드,시작시간,종료시간,총시간,스텝시간,총 Cycle,현재 Cycle,전압,전류,용량,충전용량,방전용량,Power,WattHour,충전WattHour,방전WattHour,평균전압,평균전류,IR"

#define SFTWM_WRITE_LOG	WM_USER+4000

//#define _ADP_RECORD_FILE_ID		5640
#define _ADP_RECORD_FILE_VER_OLD1	0x1000
#define _ADP_RECORD_FILE_VER		0x1001

//2007/08/09 kjh Aux File Ver
//#define _PNE_AUX_FILE_VER		0x1000
#define _PNE_AUX_FILE_VER_OLD_1	0x1001	//20080104 kjh
#define _PNE_AUX_FILE_VER		0x1002	//20100622 ljb		//Aux Name 추가

//2007/10/18 kjh Can File Ver
#define _PNE_CAN_FILE_VER_OLD	0x1000
#define _PNE_CAN_FILE_VER		0x1002	//ljb 20100716
class CChannelSensor : public CObject
{
private:
	STF_MD_AUX_SET_DATA auxData;
	int nMasterChannel;
	BOOL bInstall;

public:
	CChannelSensor()
	{
		ZeroMemory(&auxData, sizeof(STF_MD_AUX_SET_DATA));
		bInstall = FALSE;
		nMasterChannel = 0;
	}
	CChannelSensor(STF_MD_AUX_SET_DATA * aux = NULL, CString strName = "NULL", int nMasterCh = 0)
	{
		memcpy(&auxData, aux, sizeof(STF_MD_AUX_SET_DATA));
		this->nMasterChannel = nMasterCh;
	}
	CChannelSensor(CChannelSensor &Sersor)
	{
		memcpy(&auxData, &Sersor.GetAuxData(), sizeof(STF_MD_AUX_SET_DATA));
		this->nMasterChannel = Sersor.GetMasterChannel();
		this->bInstall = Sersor.GetInstall();
	}

	~CChannelSensor()
	{
	}
	void SetAuxName(CString strName)
	{
		sprintf(auxData.szName, strName);
	}
	void SetAuxDivision3()
	{
		auxData.funtion_division3 = 20007;
	}
	CString GetAuxName()
	{
		if(this->auxData.szName==NULL)
			return _T("");

		CString strAuxName;
		strAuxName.Format("%s", this->auxData.szName);
		return strAuxName;
	}
	STF_MD_AUX_SET_DATA GetAuxData()
	{
		return this->auxData;
	}
	int GetAuxChannelNumber()
	{
		return auxData.auxChNo;
	}
	int GetAuxType()
	{
		return auxData.auxType;
	}
	void SetInstall(BOOL bInstall)
	{
		this->bInstall = bInstall;
	}
	int GetInstall()
	{
		return this->bInstall;
	}

	void SetAuxData(STF_MD_AUX_SET_DATA * aux)
	{
		memcpy(&auxData, aux, sizeof(STF_MD_AUX_SET_DATA));
	}

	void SetMasterChannel(int nMasterCh)
	{
		this->nMasterChannel = nMasterCh;
		auxData.chNo = nMasterCh;
	}

	int GetMasterChannel()
	{
		return this->nMasterChannel;
	}

	void RemoveAuxData()
	{
		auxData.lMaxData = 0;
		auxData.lMinData = 0;
		auxData.chNo = 0;
		auxData.lEndMaxData = 0;
		auxData.lEndMinData = 0;
		ZeroMemory(auxData.szName, sizeof(auxData.szName));
		bInstall = TRUE;
		nMasterChannel = 0;

		//ksj 20200121
		auxData.vent_use_flag = 0;
		auxData.vent_upper = 0;
		auxData.vent_lower = 0;
	}

	
};

class CCyclerChannel  
{
public:
	int     m_iChannelNo;
	//cny-------------------------
	CtrlSocketClient  m_CtrlClient;	
	MainCtrl          m_MainCtrl;
	void CycSendCommand(int nCmd);		

	//2014.10.15 전체 복구 
	BOOL RestoreLostAll(CString strSBCFolder, CString strFileFolder, int nModuleID, int nChannelID);
	
	int     m_iCh;
	CString m_strNmmodule;	

	CString m_strCellDeltaVolt;	//ljb 20150806 add 셀전압편차 값

	BOOL	GetUserComplete()						{	return m_bUserComplete;				} 
	void		SetUserComplete(BOOL bUserComplete)		{	m_bUserComplete = bUserComplete;	} //lyj 20201028
	BOOL	GetPauseBeforeStop()						{	return m_bPauseBeforeStop;				} //lyj 20210714
	void	SetPauseBeforeStop(BOOL bPauseBeforeStop)	{	m_bPauseBeforeStop = bPauseBeforeStop;	} //lyj 20210714
	//lmh 20111213 챔버 연동 관련 필요한 것들////////////////////////
	bool m_bOvenLinkChargeDisCharge;//온도대기 
	int m_nStartOptChamber;//챔버 시작옵션
	float m_fchamberFixTemp;//챔버 시작 옵션에 따른 fix온도 
	float m_fChamberDeltaFixTemp;//챔버 시작 옵션에 따른 Delta fix온도 
	float m_fchamberDeltaTemp;//챔버 시작 옵션에 따른 Delta온도 
	UINT m_uichamberPatternNum;//챔버 패턴 번호
	UINT m_uiChamberDelayTime;//챔버 연동 전 대기 시간
	UINT m_uiChamberID;//해당 오븐 아이디
	UINT m_uiChamberCheckFixTime;//챔버 온도 비교 시작 시간 fix모드
	UINT m_uiChamberCheckTime;//챔버 온도 비교 시작 시간
	void ChamberValueInit();
	/////////////////////////////////////////////////////////////////
	//20110319 KHS Monitoring Data Text Save/////////////////////////
	CString m_strLossTime;						//20120210 KHS
	void	UpdateModuleSpec();
	void	UpdateChannelState();
	void	UpdateChannelEndState(CString strStartTime, PS_STEP_END_RECORD channelData);		//20120210 KHS
	void	UpdateChannelEndLossState(CString strStartTime, PS_STEP_END_RECORD channelData);	//20120210 KHS
	void	UpdateChannelLossState();			//20120210 KHS
	CString TimeValueString(long lData);		//20120210 KHS
	CString ValueString(double dData, int item, BOOL bUnit = FALSE);
	/////////////////////////////////////////////////////////////////

	BOOL Fun_GetFirstNgState();
	void Fun_SetFirstNgState(BOOL bSetFlag);

	float Fun_ConvertToFloat(WORD wType, long ulData);
	BOOL Fun_ConverToString(CString & strData, int * lReadData, int & nSelect);
	BOOL Fun_ConverToStringAux(CString & strData, long * pReadData, int nStartPos, int nAuxCount,ULONG & ulIndex);
	BOOL Fun_ConverToStringCan(CString & strData, CAN_VALUE * pReadData, int nStartPos, int nCanCount,ULONG & ulIndex);
	void SetCanTransCount(int nCount);
	float m_fOvenTemp;		//Chamber Temp(OVEN)
	float m_fOvenHumidity;  //ljb 20100726 Chamber Humidity(OVEN)
	float m_fChillerRefTemp;//ljb 20170912 Chiller Temp(CHILLER)
	float m_fChillerCurTemp;//ljb 20170912 Chiller Temp(CHILLER)
	void	SetUseOvenMode(BOOL bMode = TRUE)	{	m_bUseOven = bMode;	}
	BOOL	GetUseOvenMode()	{	return m_bUseOven;	}		//Oven 과 연동되어 동작하는지 여부
	int GetChannelIndex()	{	return m_nChannelIndex;}
	int	GetModuleID()	{	return m_nModuleID;	}
	CString GetSyncDateTimeString();
	long GetSyncTime();
	long GetSyncDate();
	long GetAccCycleCount1();
	long GetAccCycleCount2();
	long GetAccCycleCount3();
	long GetAccCycleCount4();
	long GetAccCycleCount5();
	long GetMultiCycleCount1();
	long GetMultiCycleCount2();
	long GetMultiCycleCount3();
	long GetMultiCycleCount4();
	long GetMultiCycleCount5();
	long GetMuxUseState();			//ljb 20151111 add
	long GetMuxBackupInfo();		//ljb 20151111 add
	long GetCVTimeDay();			//ljb 20131125 add
	long GetCVTime();
	long GetDisChargeWh();
	long GetChargeWh();
	long GetCapacitance();
	long GetDisChargeAh();
	long GetChargeAh();
	
	//ljb GetSaveDataD = ver. 100A에서 사용 
	BOOL GetSaveDataD(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA & sCanData, 
		             PS_OVEN_RAW_DATA & sOvenData, PS_LOAD_RAW_DATA & sLoaderData,PS_OVEN_RAW_DATA & sChillerData,
					 PS_CELLBAL_RAW_DATA &sCellBALData,PS_PWRSUPPLY_RAW_DATA &sPwrSupplyData/*, //yulee 20190514 cny work trasfering
					 PS_STEP_END_RECORD_EXTRA sChDataExtra //ksj 20201013 : 추가*/
					 );
	/*BOOL GetSaveDataD(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA & sCanData, 
		             PS_OVEN_RAW_DATA & sOvenData, PS_LOAD_RAW_DATA & sLoaderData,PS_OVEN_RAW_DATA & sChillerData,
					 PS_CELLBAL_RAW_DATA &sCellBALData);*/
	BOOL RemakeCanPCIndexFile(CString strBackUpRawFile, CString strAuxTStepEndFile, CString strAuxVStepEndFile, CString strNewTableFile);
	BOOL RemakeAuxPCIndexFile(CString strBackUpRawFile, CString strStepEndFile, CString strCanSStepEndFile, CString strNewTableFile);
	BOOL RemakeAuxPCIndexFile2(CString strBackUpRawFile, CString strStepEndFile, CString strCanSStepEndFile, CString strNewTableFile);
	int WriteRestoreCanFile (CString strMFileName, CString strSFileName, FILE *fpDestFile, PS_CAN_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo);
	int WriteRestoreAuxFile (CString strTFileName, CString strVFileName, FILE *fpDestFile, PS_AUX_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo);
	int WriteRestoreRawFile(CString strLocalFileName, FILE *fpDestFile, PS_RAW_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo);
	//BOOL GetAuxFileRawData(PS_AUX_FILE_HEADER &FileHeader, FILE * fpDestFile, FILE * fpSourceFile, CString destFileName, CString srcFileName);
	//BOOL GetFileRawData(PS_RAW_FILE_HEADER &fileHeader, FILE * fpDestFile, FILE * fpSourceFile,
	//								  CString destFileName, CString srcFileName);
	int GetTotalAuxCount();
	int GetAuxVoltCount();
	int GetAuxTempCount();
	int GetAuxTempThCount();
	int GetAuxHumiThCount(); //ksj 20200116

	int m_nVoltAuxCount;
	int m_nTempAuxCount;
	int m_nTempThAuxCount;
	int m_nHumiAuxCount; //ksj 20200116 : v1016 습도 추가

	void SetCanTitleFile(CString strCanPath, CString strTestName);
	CString GetEndTime();
	BOOL m_IsUserFilterSlave;
	BOOL m_IsUserFilterMaster;
	long WriteCtsFile();
	long WriteResultListFileA(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData);
	long WriteResultListFile_V100B(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData);
	//long WriteResultListFile_V100B(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData, PS_STEP_END_RECORD_EXTRA &sChDataExtra); //ksj 20201013 : 스텝엔드 추가 정보 구조체 추가
	void SetCanCount(int nCount);
	int m_nCanCount;
	int m_nCanTransCount;
	BOOL CreateCanFileA(CString strCanFileName, int nFileIndex, int nCanCount);
	
	long GetCanItemData(PS_CAN_RAW_DATA canData, WORD wItem, int nCanIndex);
	int GetCanFile(CFile *pFile, PS_CAN_FILE_HEADER * canHeader);
	int CheckAndSaveData();			//ljb ver.100A보다 작을 경우
	int	CheckAndSaveData_v100D();		//ljb 20101231 
	int GetCanCount();
	SFT_CAN_DATA * GetCanData();
	void SetMasterChannelNum(unsigned char uMaster);
	int GetMasterChannelNum();
	BOOL	m_bMaster;
	BOOL IsParallel();
	void SetParallel(BOOL bParallel, BOOL bMaster);
	int GetAuxFile(CFile * pFile, PS_AUX_FILE_HEADER * auxHeader);
	void SetAuxTitleFile(CString strPath,  CString strTestName);
	int GetChFile(CFile & fRS, PS_RAW_FILE_HEADER & rawHeader);
	float GetAuxItemData(PS_AUX_RAW_DATA auxData, WORD wItem, int nAuxIndex);
	BOOL CreateAuxFileA(CString m_strAuxFileName, int nFileIndex, int nAuxVoltCount, int nAuxTempCount);
	int m_nAuxCount;
	SFT_AUX_DATA * GetAuxData();
	int	GetAuxCount();
	//CObList	auxList;
	HTREEITEM hCurrentChannelTreeItem;
	CString GetStartTime();
	CScheduleData * GetScheduleData();
	BOOL SaveRunInfoToTempFile();
	void LoadSaveItemSet();
	void SetSaveItemList(CWordArray &awItem);
	BOOL IsStopReserved();	
	BOOL IsPauseReserved();
	BOOL IsDataLoss()		{	return m_bDataLoss;		}		//SearchLostDataRange()를 하여야 실제 정확한 손실 여부를 판단 할 수 있다.

	void SetFtpAllDown(BOOL bDownOK)	{	 m_bFtpDataAllDownOK = bDownOK;	}//ljb 20150522 손실 없이 저장 완료 저장
	BOOL IsFtpDataAllDownOK()			{	return m_bFtpDataAllDownOK;		}//ljb 20150522 손실 없이 저장 완료 되었는지 체크

	void WriteLog(CString strLogMsg);
	void WriteChamberLog(CString strLogMsg);
	void WritePwrSupplyLog(CString strLogMsg);
	void WriteChillerLog(CString strLogMsg);
	void WritePumpLog(CString strLogMsg);
	void WriteCellBALLog(CString strLogMsg);

	void DeleteBuffer(float *pfAuxBuff , SFT_CAN_VALUE *pCanBuff , float *pfBuff); //lyj 20210910 메모리누수 조치
	void WriteSchUpdateLog(CString strLogMsg); //ksj 20170810
	BOOL	SaveStepMiliSecData();
	BOOL	RemakePCIndexFile(CString strBackUpRawFile, CString strStepEndFile, CString strNewTableFile, CString strOrgTableFile = "");
	CString GetLastErrorString();
	CString GetResultFileName();

	//File Update Lock
	BOOL	LockFileUpdate(BOOL bLock = TRUE);
	BOOL	LoadInfoFromTempFile();	
	void		UpdateStartTime()	{	m_startTime = COleDateTime::GetCurrentTime();	}
	void		SetTestSerial(CString strSerial, CString strUserID = "", CString strDescript = "", CString strCellDeltaVolt = "")	
	{	
		m_strTestSerial = strSerial;	
		m_strDescript = strDescript;
		m_strUserID = strUserID;
		m_strCellDeltaVolt = strCellDeltaVolt;
	}
	void		SetTestSerial2(CString strTray = "", CString strBcr = "")
	{
		m_strTrayNo = strTray;	
		m_strBcrNo = strBcr;
	}

	int			CompletedTest();
	CString		GetGradeString();
	CString		GetFilePath();
	void	ResetData();
	
	CString GetTestPath();
	CString GetChFilePath()						{   return m_strFilePath;			}
	CString GetWorkerName()						{	return m_strUserID;				}
	CString GetTestDescript()					{	return m_strDescript;			}
	CString	GetTestSerial()						{	return m_strTestSerial;			}
	CString	GetTestTrayNo()						{	return m_strTrayNo;			}
	CString	GetTestBcrNo()						{	return m_strBcrNo;			}
	CString GetScheduleName()					{	return m_strScheduleName;		}
	CString	GetScheduleFile()					{	return m_strScheduleFileName;	}
	CString GetScheduleFileForSbc()				{	return m_strScheduleFileNameForSbc;		}	//ljb 20130404
	CString GetScheduleFileForSbc2()			{	return m_strScheduleFileNameForSbc2;	}	//ljb 20160427 add

	CString	GetLogFileName()					{	return m_strLogFileName;		}
	void	SetCompleteWork(BOOL bComplete)		{   m_bCompleteWork = bComplete;	}	//ljb
	BOOL	GetCompleteWork()					{   return m_bCompleteWork;			}	//ljb
	void	SetScheduleName(CString strName)	{	m_strScheduleName = strName;	}
	
	void SetFilePath(CString strPath, BOOL bReset = TRUE);
	BOOL GetSaveData(PS_STEP_END_RECORD &sChData);
	//BOOL GetSaveDataB(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData);
	BOOL GetSaveDataC(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA & sCanData);

	//test name
	void	SetTestName(CString strTestName);
	CString GetTestName();
	
	BYTE		GetGradeCode();
	int	GetCurCycleCount();
	int	GetTotalCycleCount();
	WORD GetCellCode();
	WORD GetState();
	WORD GetStepType();
	WORD GetStepNo();
	WORD GetStepMode(); //ksj 20200625
	ULONG GetStepTimeDay();		//ljb 20131125 add
	ULONG GetStepTime();
	ULONG GetTotTimeDay();		//ljb 20131125 add
	ULONG GetTotTime();
	UINT GetStackedSize();
	long GetImpedance();
	long GetCapacity();
	long GetCurrent();
	long GetVoltage();
	long GetVoltage_Input();
	long GetVoltage_Power();
	long GetVoltage_Bus();
	long GetWattHour();
	long GetWatt();
	long GetAvgVoltage();
	long GetAvgCurrent();
	long GetCapacitySum();
	long GetTemperature();
	long GetTemperature_TH(); //20180612 yulee
	long GetAuxVoltage();
	long GetCommState();		//ljb v100B
	long GetChamberUsing();		//ljb v100B
	long GetChOutputState();		//ljb 201012 v100B
	long GetChInputState();			//ljb 201012 v100B

	BYTE GetCbank();

	CCalibrationData * GetCaliData();
	CChannel * GetChannel();
	
	CCyclerChannel(UINT nModuleID, UINT nChannelIndex);
	CCyclerChannel();
	virtual ~CCyclerChannel();

	void SetID(UINT nModuleID, UINT nChIndex);

	double GetMeterValue()				{	return	m_fMeterValue;		}
	void	SetMeterValue(double fValue)	{	m_fMeterValue = fValue;		}

	//lost data range
	BOOL	RestoreLostData(CString strIPAddress);
	BOOL	RestoreLostData_lmh_localTest(CString strIPAddress);
	BOOL	Fun_DownloadData(CString strIPAddress, CString strLocalFolder,UINT nModuleID, UINT nChIndex , BOOL bAlarmFlag = TRUE);
	BOOL	RestoreLostDataFromFolder(CString strSBCFolder,CString strFolder, int nModuleID, int nChannelID);	//ljb 2011329 이재복 //////////
	BOOL	RestoreLostDataFromFolder2(CString strSBCFolder,CString strFolder, int nModuleID, int nChannelID);	//ljb 2011922 이재복 //////////
	BOOL	SearchLostDataRange(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd = NULL, BOOL bIncludeInitCh = FALSE);
	BOOL	SearchLostDataRange2(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd = NULL, BOOL bIncludeInitCh = FALSE);
	BOOL	SearchLostDataRange3(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd = NULL, BOOL bIncludeInitCh = FALSE);
	int		GetLostDataRangeSize()			{	return m_strDataLossRangeList.GetCount();	}
	CStringList * GetLostDataRangeList()	{	return &m_strDataLossRangeList;				}
	
	void	GetReservedStep(long &lCycleNo, long &lStepNo)	{	lCycleNo = m_lReservedCycleNo; lStepNo = m_lReservedStepNo;	}
	void	SetReservedStep(long lCycleNo, long lStepNo)	{	m_lReservedCycleNo = lCycleNo; m_lReservedStepNo =lStepNo ;	}

	BOOL	GetFileLocking()			{	return m_bFileSaving; }			//ljb 201101
	void	SetFileLocking(BOOL bFlag)	{	m_bFileSaving = bFlag;	}		//ljb 201101

	//ljb 20110920
	BOOL CreateResultDataFile(CString strFileName);

	BOOL	m_bDataLoss;			//20120210 KHS  public으로 변경

	CRITICAL_SECTION	m_csCommunicationSync;



protected:
	CUnitTrans m_UnitTrans;			//20110319 KHS
	void SaveStepEndData(CString strStartT, PS_STEP_END_RECORD sChData);			//20110319 KHS
	void SaveStepEndAuxData(CString strStartT, PS_STEP_END_RECORD sChData, PS_AUX_RAW_DATA sAuxData);//yulee 20181124
	void SaveStepEndCANData(CString strStartT, PS_STEP_END_RECORD sChData, PS_CAN_RAW_DATA sCANData);//yulee 20181124
	void SaveStepEndDataMerge(CString strStartT, PS_STEP_END_RECORD sChData, PS_AUX_RAW_DATA sAuxData, PS_CAN_RAW_DATA sCANData);	//lyj 20200521 
	//void SaveStepEndDataMerge(CString strStartT, PS_STEP_END_RECORD_EXTRA sChDataExtra, PS_AUX_RAW_DATA sAuxData, PS_CAN_RAW_DATA sCANData);	//ksj 20201013 : 스텝엔드 구조체 변경
	int GetFindCharIndexCount(CString parm_string, char parm_find_char ,int parm_find_index); //lyj 20200521
	CString getAuxCANTitleFromTLT(CString strTestFilePath, CString strTestName, int ModuleID, int iCANLineNo, int nTypeOper);//yulee 20181124
	BOOL m_bFileSaving;		//ljb 201101
	CWordArray m_SaveItemArray;	//ljb 20101231
	BOOL	m_bSendNgState;		//NG State 보고 했는지 안 했는지
	BOOL	m_bUseOven;
	BOOL	m_bCompleteWork;		//ljb
	void UpdateLastData(UINT nStartIndex, UINT nEndIndex, CString strStartT, CString strSerial, CString strUser, CString strDescript, PS_STEP_END_RECORD sChData);
	ULONG GetLastData(CString &strStartT, CString &strSerial, CString &strUser, CString &strDescript, PS_STEP_END_RECORD &sChData);

	BOOL	m_bUseGetLastDataCache; //ksj 20210604 : rp$ 메모리 캐시 기능 사용할지 여부
	int		ResetGetLastDataCache(); //ksj 20210604 : rp$ 메모리에 저장되어있는 캐시 초기화
	int		SetGetLastDataCache(CString &strStartT, CString &strSerial, CString &strUser, CString &strDescript, PS_STEP_END_RECORD &sChData);
	BOOL	m_bIsGetLastDataCached; //ksj 20210604 : 최종 rp$ 값이 메모리에 저장되어있는지 여부
	PS_STEP_END_RECORD m_stLastDataCache; //ksj 20210604 : 최종 rp$ 값 캐시
	CString m_strCacheStartT; //ksj 20210604 : 최종 rp$ 값 캐시
	CString m_strCacheSerial; //ksj 20210604 : 최종 rp$ 값 캐시
	CString m_strCacheUser; //ksj 20210604 : 최종 rp$ 값 캐시
	CString m_strCacheDescript; //ksj 20210604 : 최종 rp$ 값 캐시

	float GetItemData(PS_STEP_END_RECORD &chData, WORD wItem);
	int		ParsingChDataString(CString strValue, PS_STEP_END_RECORD &sData);	
	float AddTotalCapacity(WORD nType, float fCapacity);
	long ConvertPCUnitToSbc(float fData, long nItem);
	float ConvertSbcToPCUnit(long lData, long nItem);
	BOOL ReloadReaultData(BOOL bIncludeWorkingStep = TRUE);
	
	void		SetScheduleFile(CString strFileName);
	void		SetLogFile(CString strFileName)		        { m_strLogFileName = strFileName;	          };
	void		SetChamberLogFile(CString strFileName)		{ m_strLogChamberFileName = strFileName;	  };
	
	void		SetSchUpdateLogFile(CString strFileName)	{ m_strSchUpdateLogFileName = strFileName;	  };//ksj 20170810
	void		SetScheduleFileForSbc(CString strFileName)	{ m_strScheduleFileNameForSbc = strFileName;  };//20130404
	void		SetScheduleFileForSbc2(CString strFileName)	{ m_strScheduleFileNameForSbc2 = strFileName; };//20160427 add

	void		SetPwrSupplyLogFile(CString strFileName)	{ m_strLogPwrSupplyFileName = strFileName;	  };
	void		SetChillerLogFile(CString strFileName)	    { m_strLogChillerFileName = strFileName;	  };
	void		SetPumpLogFile(CString strFileName)	        { m_strLogPumpFileName = strFileName;	  };
	void		SetCellBALLogFile(CString strFileName)	    { m_strLogCellBALFileName = strFileName;	  };

	CString		m_strLastErrorString;
	CStringList m_strDataLossRangeList;
	CChData		m_chFileDataSet;
	BOOL		m_bDataLossLogCnt;			//20120210 KHS
	//BOOL		m_bDataLoss;				//통신 Error 및 기타 원인에의해 중간에 Data가 손실되었는지 연부 
											//마지막에 손실된 Data는 FTP 접속 이전에는 확인 할 수 없다. 
	
	BOOL		m_bFtpDataAllDownOK;		//ljb 20150522 모두데이터 download 완료

	BOOL		m_bLockFileUpdate;			//다른 곳에서 결과 파일 접근시 update locking
	CStringList m_strDataList;				//결과 파일 저장시 임시적은 저장 장소 
	BOOL GetRecordIndex(CString strString, long &lFrom, long &lTo);
	CCalibrationData	m_CaliData;			//채널의 교정값 

	//test Information
	CString m_strScheduleName;
	CString m_strDescript;
	CString m_strUserID;
	CString m_strTestSerial;			//Test Serial or Lot No
	CString m_strTestName;				//Test 명 
	CString m_strFilePath;				//C:\\Test\M01C01
	CString m_strSaveTestName;			//ljb 20131205 파일로 저장하는 파일명

	CString m_strTrayNo;				//20160314 add Test Tray
	CString m_strBcrNo;					//20160314 add Test Bcr

	//File Name
	CString m_strLogFileName;			//작업 Log 파일명		C:\\Test\M01C01\Test.log
	CString m_strLogChamberFileName;	//작업 Log 파일명		C:\\Test\M01C01\Test.log	
	CString m_strScheduleFileName;		//적용 스케쥴 파일명	C:\\Test\M01C01\Test.sch
	CString m_strScheduleFileNameForSbc;		
	CString m_strScheduleFileNameForSbc2;		
	CString m_strResultFileName;		//저장 조건 결과 파일명	C:\\Test\M01C01\Test.cyc
	CString m_strResultStepFileName;	//Step의 결과 파일명	C:\\Test\M01C01\Test.rpt (C:\\Test\M01C01\Test.rp$)
	CString m_strSchUpdateLogFileName;  //ksj 20170810 : 스케쥴 Update Log 파일명
	CString m_strLogPwrSupplyFileName;
	CString m_strLogChillerFileName;
	CString m_strLogPumpFileName;
	CString m_strLogCellBALFileName;

	UINT m_nModuleID;
	UINT m_nChannelIndex;
	float m_fCapacitySum;
	double m_fMeterValue;
	

	COleDateTime	m_startTime;
	CModuleIndexTable	m_RestoreFileIndexTable;
	PS_RECORD_FILE_HEADER	m_FileRecordSetHeader;
	BOOL m_bScheduleData;
	CScheduleData *m_pScheduleData;

	//Text Save
	//BOOL CreateResultFile(CString strFileName);
	//BOOL CreateResultListFile(CString strFileName);
	//BOOL WriteResultListFile(UINT nLineNo, CT_CH_SAVE_DATA &sChData);

	//Binary Save
	BOOL CreateResultFileA(CString strFileName);
	BOOL Fun_CheckLossData(CString strFileName, long nLastIndex);		//ljb 20180223 add cyc file size와 LastIndex 비교 해서 손실 체크
	
	//BOOL WriteResultListFileA(UINT nLineNo, CT_CH_SAVE_DATA &sChData);
	//BOOL WriteResultListFile_V100B(UINT nLineNo, CT_CH_SAVE_DATA &sChData);

	BOOL	m_bUserComplete; //lyj 20201028
	//BOOL	m_bChiller_DESChiller; //lyj 20201102 DES칠러

private:
	unsigned char m_uMasterCh;
	long	m_lReservedCycleNo;
	long	m_lReservedStepNo;
	BOOL	m_bParallel;
	BOOL	m_bPauseBeforeStop; //lyj 20210714 완료 전 일시정지 플래그
	
public:
	CString GetDataPath();
	// Added by 224 (2014/10/20) : 공정 끝나는 시점에 LIN 모듈 종료 위해
	WORD m_prev_state ;		// PSServer-> CChannel::GetState() 의 값을 보관 후에 이전 값과 다른 경우 
	// 저장한다.
	// 이전값과 다르고, 현재값이 "완료" 인 경우 LIN request 를 종료한다.
	WORD m_prev_type ;
	void WriteAuxStateLog(void);
	void WriteCanStateLog(void);
	void WriteParallelStateLog(SFT_MD_PARALLEL_DATA* pParallelData);
	BOOL UpdateStartTimeFromCts(CString strCtsFilePath);
	int m_nNetworkErrorCnt;	
	int m_nNetworkErrorTimer;
	// ksj 20200702 : 네트워크 에러 카운터 및 타이머 초기화
	void ResetNetworkErrorCount(void);
};

#endif // !defined(AFX_CYCLERCHANNEL_H__F1065011_05CD_44C3_863C_837DF75454A2__INCLUDED_)

