#include "afxwin.h"
#if !defined(AFX_WORKWARNIN_H__191CC1AF_0299_4372_8D63_150BE217A3FB__INCLUDED_)
#define AFX_WORKWARNIN_H__191CC1AF_0299_4372_8D63_150BE217A3FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WorkWarnin.h : header file
//
const COLORREF CLOUDBLUE = RGB(128, 184, 223);
const COLORREF WHITE = RGB(255, 255, 255);
const COLORREF BLACK = RGB(1, 1, 1);
const COLORREF DKGRAY = RGB(128, 128, 128);
const COLORREF LLTGRAY = RGB(220, 220, 220);
const COLORREF LTGRAY = RGB(192, 192, 192);
const COLORREF YELLOW = RGB(255, 255, 0);
const COLORREF DKYELLOW = RGB(128, 128, 0);
const COLORREF RED = RGB(255, 0, 0);
const COLORREF DKRED = RGB(128, 0, 0);
const COLORREF BLUE = RGB(0, 0, 255);
const COLORREF LBLUE = RGB(192, 192, 255);
const COLORREF DKBLUE = RGB(0, 0, 128);
const COLORREF CYAN = RGB(0, 255, 255);
const COLORREF DKCYAN = RGB(0, 128, 128);
const COLORREF GREEN = RGB(0, 255, 0);
const COLORREF DKGREEN = RGB(0, 128, 0);
const COLORREF MAGENTA = RGB(255, 0, 255);
const COLORREF DKMAGENTA = RGB(128, 0, 128);
const COLORREF ORANGE = RGB(255, 153, 0);
/////////////////////////////////////////////////////////////////////////////
// CWorkWarnin dialog

class CWorkWarnin : public CDialog
{
// Construction
public:	
	CString	m_strMessage;
	void DrawText();
	void Fun_SetMessage(CString strMsg);
	void InitControl();
	void Fun_SetButton_OK(CString strMsg);	//lyj 20210713
	void Fun_SetButton_Cancle(CString strMsg);//lyj 20210713
	CString m_strBtn_OK;
	CString m_strBtn_Cancle;

	CWorkWarnin(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CWorkWarnin)
	enum { IDD = IDD_WORK_WARNING , IDD2 = IDD_WORK_WARNING_ENG ,IDD3 = IDD_WORK_WARNING_PL };
	CLabel	m_ctrlLabMessage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkWarnin)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWorkWarnin)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKWARNIN_H__191CC1AF_0299_4372_8D63_150BE217A3FB__INCLUDED_)
