#if !defined(AFX_ALARMREADYDLG_H__CF516CC9_5142_4F1E_B0AE_2A2DC8E603CE__INCLUDED_)
#define AFX_ALARMREADYDLG_H__CF516CC9_5142_4F1E_B0AE_2A2DC8E603CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AlarmReadyDlg.h : header file
//
#include "CTSMonProView.h"

/////////////////////////////////////////////////////////////////////////////
// CAlarmReadyDlg dialog
class CCTSMonProDoc;

class CAlarmReadyDlg : public CDialog
{
// Construction
public:
	void Fun_CloseDlg();
	BOOL Fun_SetRunInfo(UINT nModuleID, CWordArray *apSelChArray, UINT nCmdID, int nCycleNo, int nStepNo);
	//	void Fun_SetDoc(CCTSMonProDoc *pDoc);
	
	void Fun_SetTxtMessage(CString strMsg);
	void Fun_RunProgress();
	int	 Fun_RunTest(int nModuleID);
	CAlarmReadyDlg(int nModuleID, CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor
	
	CCTSMonProDoc * m_pDoc;
	
	CWordArray awSelCh;
	CString m_strMessage;
	CString m_strText;
	
	
	CCyclerModule *pMD;
	CCyclerChannel *pChInfo;
	
	BOOL set_AlarmReadyDlg_SelchArray(CWordArray *apSelChArray);

// Dialog Data
	//{{AFX_DATA(CAlarmReadyDlg)
	enum { IDD = IDD_ALARM_MESSAGE_DLG , IDD2 = IDD_ALARM_MESSAGE_DLG_ENG , IDD3 = IDD_ALARM_MESSAGE_DLG_PL};
	CProgressCtrl	m_progress;
	CString	m_lab_message;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAlarmReadyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	int m_iPos;
	UINT m_nTime;	//분단위로 표시
	
	UINT m_nModuleID;
	CWordArray *m_apSelChArray;
	UINT m_nCmdID;
	UINT m_nCycleNo;
	UINT m_nStepNo;
	
	//bung value
	CString strTemp;
	
	int m_chk_Time;

	// Generated message map functions
	//{{AFX_MSG(CAlarmReadyDlg)
	afx_msg void OnBtnResend();
	virtual void OnCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALARMREADYDLG_H__CF516CC9_5142_4F1E_B0AE_2A2DC8E603CE__INCLUDED_)
