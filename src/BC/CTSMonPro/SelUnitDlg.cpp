// SelUnitDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "SelUnitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelUnitDlg dialog


CSelUnitDlg::CSelUnitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSelUnitDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSelUnitDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSelUnitDlg::IDD3):
	(CSelUnitDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSelUnitDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSelUnitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelUnitDlg)
	DDX_Control(pDX, IDC_COMBO_UNIT, m_ctrlUnit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelUnitDlg, CDialog)
	//{{AFX_MSG_MAP(CSelUnitDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_UNIT, OnSelchangeComboUnit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelUnitDlg message handlers

void CSelUnitDlg::OnSelchangeComboUnit() 
{
	// TODO: Add your control notification handler code here
	int index = m_ctrlUnit.GetCurSel();
	if(index >= 0 )
	{
		m_nModuleID = m_ctrlUnit.GetItemData(index);
	}	
}

BOOL CSelUnitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	int nDefaultIndex = -1;
	
	long rtn;
	HKEY hKey = 0;
	BYTE buf[512];
	DWORD size = 511;
	DWORD type;
	CString strTemp, strDest, strDBPath;
	rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PNE CTSPack\\CTSMonPro\\Config", 0, KEY_READ, &hKey);
	//Note Found 
	if(ERROR_SUCCESS == rtn)
	{
		//Load PowerFormation DataBase  Path
		size = 511;
		rtn = ::RegQueryValueEx(hKey, "Path", NULL, &type, buf, &size);
		::RegCloseKey(hKey);
		
		//
		if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
		{
//			int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

// 			switch(nSelLanguage) 20190701
// 			{
// 			case 1: strDBPath.Format("%s\\DataBase\\%s", buf, PS_SCHEDULE_DATABASE_NAME); break;
// 			case 2: strDBPath.Format("%s\\DataBase\\%s", buf, PS_SCHEDULE_DATABASE_NAME_EN); break;
// 			case 3: strDBPath.Format("%s\\DataBase\\%s", buf, PS_SCHEDULE_DATABASE_NAME_PL); break;
// 			default : strDBPath.Format("%s\\DataBase\\%s", buf, PS_SCHEDULE_DATABASE_NAME_EN); break;
// 			}

			strDBPath.Format("%s\\DataBase\\%s", buf, PS_SCHEDULE_DATABASE_NAME);
			
			CString address;
			CDaoDatabase  db;
			db.Open(strDBPath);
			
			CString strSQL;
			strSQL = "SELECT data3, ModuleID FROM SystemConfig ORDER BY ModuleID";
			try
			{
				CDaoRecordset rs(&db);
				
				rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				int a = 0;
				while(!rs.IsEOF())
				{
					COleVariant data = rs.GetFieldValue(0);
					address = data.pbVal;
					data = rs.GetFieldValue(1);
					m_ctrlUnit.AddString(address);
					
					if(m_nModuleID == data.lVal)
					{
						nDefaultIndex =  a;
					}
					m_ctrlUnit.SetItemData( a++, data.lVal);
					rs.MoveNext();
				}
				rs.Close();
				db.Close();
			} 
			catch (CDaoException *e)
			{
				e->Delete();
			}	
		}
		else
		{
			AfxMessageBox(Fun_FindMsg("OnInitDialog_msg1","IDD_SEL_UNIT_DIALOG"));
			//@ AfxMessageBox("CTS 설치 정보를 찾을 수 없습니다.");
			return FALSE;
		}
	}
	else
	{
		AfxMessageBox(Fun_FindMsg("OnInitDialog_msg2","IDD_SEL_UNIT_DIALOG"));
		//@ AfxMessageBox("CTS 설치 정보를 찾을 수 없습니다.");
		return FALSE;
	}
	
	m_ctrlUnit.SetCurSel(nDefaultIndex);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CSelUnitDlg::GetModuleID()
{
	return m_nModuleID;
}

void CSelUnitDlg::SetModuleID(int nModuleID)
{
	m_nModuleID = nModuleID;
}

void CSelUnitDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}