#if !defined(AFX_DLG_CELLBALOPT_H__9DDC7381_DD55_45CC_A184_01DA5A3011B2__INCLUDED_)
#define AFX_DLG_CELLBALOPT_H__9DDC7381_DD55_45CC_A184_01DA5A3011B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_CellBALOpt.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALOpt dialog

class Dlg_CellBALOpt : public CDialog
{
// Construction
public:
	Dlg_CellBALOpt(CWnd* pParent = NULL);   // standard constructor

	void InitObject();
	void SetValue() ;

// Dialog Data
	//{{AFX_DATA(Dlg_CellBALOpt)
	enum { IDD = IDD_DIALOG_CELLBAL_OPT , IDD2 = IDD_DIALOG_CELLBAL_OPT_ENG , IDD3 = IDD_DIALOG_CELLBAL_OPT_PL };
	//CButton	m_ChkCellBalLogRunInResult;
	CEdit	m_EditCellBalAuxDiv3;
	CButton	m_ChkCellBalLog;
	CButton	m_ChkCellBalShow;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_CellBALOpt)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_CellBALOpt)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnChkCellbalshow();
	afx_msg void OnChkCellballog();
	afx_msg void OnChangeEditCellbalAuxdiv3();
	afx_msg void OnChkCellballogResult();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CELLBALOPT_H__9DDC7381_DD55_45CC_A184_01DA5A3011B2__INCLUDED_)
