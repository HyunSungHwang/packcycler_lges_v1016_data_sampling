// ColorListCtrl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMonPro.h"
#include "ColorListCtrl.h"


// CColorListCtrl

IMPLEMENT_DYNAMIC(CColorListCtrl, CListCtrl)

CColorListCtrl::CColorListCtrl()
{

}

CColorListCtrl::~CColorListCtrl()
{
}


BEGIN_MESSAGE_MAP(CColorListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CColorListCtrl::OnNMCustomdraw)
END_MESSAGE_MAP()



// CColorListCtrl 메시지 처리기입니다.



//ksj 20200120 : 특정 셀 색상 변경하도록 처리
//임시로 하드코딩했으나
//클래스 재사용 가능하도록 프로그램 정리 필요.
void CColorListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(pNMCD->dwDrawStage == CDDS_PREPAINT){
		*pResult = (LRESULT)CDRF_NOTIFYITEMDRAW; 
		return; // 여기서 함수를 빠져 나가야 *pResult 값이 유지된다.
	}
 
	if(pNMCD->dwDrawStage == CDDS_ITEMPREPAINT){ 

        // 한 줄 (row) 가 그려질 때. 여기서만 설정하면 한 줄이 모두 동일하게 설정이 된다.
//		if(_lstDir.GetItemData(pNMCD->dwItemSpec) == 0){//dwItemSpec 이 현재 그려지는 row index
		if(GetItemData(pNMCD->dwItemSpec) == 0){//dwItemSpec 이 현재 그려지는 row index
			NMLVCUSTOMDRAW *pDraw = (NMLVCUSTOMDRAW*)(pNMHDR); 
 			pDraw->clrText = 0xffffff; 
			pDraw->clrTextBk = 0x0; 
			//*pResult = (LRESULT)CDRF_NEWFONT;//여기서 나가면 sub-item 이 변경되지 않는다.
			*pResult = (LRESULT)CDRF_NOTIFYSUBITEMDRAW;//sub-item 을 변경하기 위해서. 
			return;//여기서 중단해야 *pResult 값이 유지된다.
		}
		else{ // When all matrices are already made. 
			NMLVCUSTOMDRAW *pDraw = (NMLVCUSTOMDRAW*)(pNMHDR); 
 			pDraw->clrText = 0x0; 
			pDraw->clrTextBk = RGB(255,255,196);  
			*pResult = (LRESULT)CDRF_NEWFONT; 
			return; 
		}
	}
	else if(pNMCD->dwDrawStage == (CDDS_SUBITEM | CDDS_ITEMPREPAINT)){	
                // sub-item 이 그려지는 순간. 위에서 *pResult 에 CDRF_NOTIFYSUBITEMDRAW 를 해서 여기로

                // 올 수 있었던 것이다.

		NMLVCUSTOMDRAW *pDraw = (NMLVCUSTOMDRAW*)(pNMHDR); 
		//CString text= _lstDir.GetItemText(pNMCD->dwItemSpec, pDraw->iSubItem); 
		
		int nSubItem = pDraw->iSubItem;
		switch(nSubItem)
		{
		case 7: //vent 사용 여부 색처리
			{
				CString text= GetItemText(pNMCD->dwItemSpec, pDraw->iSubItem); 
				if(text == _T("O")){
					pDraw->clrText = RGB(37,194,84); 
					pDraw->clrTextBk = RGB(255,221,211);
				}
				else if(text == _T("X")){
					pDraw->clrText = RGB(255,0,0); 
					pDraw->clrTextBk = 0xffffff; 			
				}
				else{
					pDraw->clrText = 0x0; 
					pDraw->clrTextBk = 0xffffff; 
				}
			}
			break;
		case 5: //안전 상한 하한
		case 6:
			{
				CString text= GetItemText(pNMCD->dwItemSpec, 7); 
				if(text == _T("O")){
					pDraw->clrText = 0x0; 
					pDraw->clrTextBk = RGB(255,221,211);
				}
				else if(text == _T("X")){
					pDraw->clrText = 0x0; 
					pDraw->clrTextBk = 0xffffff; 			
				}
				else{
					pDraw->clrText = 0x0; 
					pDraw->clrTextBk = 0xffffff; 
				}
			}
			break;
		default:
			{
				pDraw->clrText = 0x0; 
				pDraw->clrTextBk = 0xffffff; 
			}			
			break;
		}
		
		
		*pResult = (LRESULT)CDRF_NEWFONT; // 이렇게 해야 설정한 대로 그려진다.
		return;
	}
	////
	*pResult = 0;
}
