#ifndef __SendToFTP_v1014_v1_SCH__
#define __SendToFTP_v1014_v1_SCH__

#pragma once


#include "CTSMonProDoc.h"

class CSendToFTP_v1014_v1_SCH
{
public:
	CSendToFTP_v1014_v1_SCH(void);
	~CSendToFTP_v1014_v1_SCH(void);

	
	BOOL SendSchFTP_v1014_v1_SCH(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, int nOption,int ReserveID, CCTSMonProDoc* pDoc);
	BOOL SendSchFTP2_v1014_v1_SCH(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, CCTSMonProDoc* pDoc);
};

#endif