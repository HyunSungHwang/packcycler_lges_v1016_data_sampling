#if !defined(AFX_WARNINGDLG_H__8A87E1E8_9962_4810_B377_8A3238D57FAD__INCLUDED_)
#define AFX_WARNINGDLG_H__8A87E1E8_9962_4810_B377_8A3238D57FAD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WarningDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWarningDlg dialog

class CWarningDlg : public CDialog
{
// Construction
public:
	
	CWarningDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CWarningDlg)
	enum { IDD = IDD_WARNING , IDD2 = IDD_WARNING_ENG , IDD3 = IDD_WARNING_PL };
	CString	m_strWarringMessage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWarningDlg)
	public:
	virtual INT_PTR  DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWarningDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WARNINGDLG_H__8A87E1E8_9962_4810_B377_8A3238D57FAD__INCLUDED_)
