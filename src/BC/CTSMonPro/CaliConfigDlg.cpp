// CaliConfigDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CaliConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCaliConfigDlg dialog


CCaliConfigDlg::CCaliConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCaliConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCaliConfigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCaliConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCaliConfigDlg)
	DDX_Control(pDX, IDC_LIST_CURRENT_L, m_wndCurrentLow);
	DDX_Control(pDX, IDC_LIST_CURRENT_H, m_wndCurrentHigh);
	DDX_Control(pDX, IDC_LIST_VOLTAGE, m_wndVoltage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCaliConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CCaliConfigDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCaliConfigDlg message handlers

BOOL CCaliConfigDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
		case VK_ESCAPE:
			return m_wndVoltage.PreTranslateMessage(pMsg);
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
