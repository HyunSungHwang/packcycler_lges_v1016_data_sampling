#if !defined(AFX_DLG_CELLBALLOADMODESET_H__BC7FDC64_B326_483E_B4DF_0B3B47BEA7BD__INCLUDED_)
#define AFX_DLG_CELLBALLOADMODESET_H__BC7FDC64_B326_483E_B4DF_0B3B47BEA7BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_CellBALLoadModeSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALLoadModeSet dialog

class Dlg_CellBALLoadModeSet : public CDialog
{
	// Construction
public:
	Dlg_CellBALLoadModeSet(CWnd* pParent = NULL);   // standard constructor
	
	int m_iState_Conn;
	S_P1_CMD_BODY_LOAD_MODE_SET *m_cmdBodyLoadModeSet;
	void onCellBALMakeLoadModeSet_User();
	
	// Dialog Data
	//{{AFX_DATA(Dlg_CellBALLoadModeSet)
	enum { IDD = IDD_DIALOG_CELLBAL_LOADMODESET , IDD2 = IDD_DIALOG_CELLBAL_LOADMODESET_ENG , IDD3 = IDD_DIALOG_CELLBAL_LOADMODESET_PL };
	CEdit	m_EditSignalSensTime;
	CEdit	m_EditChCondEndVoltage;
	CEdit	m_EditChCondEndTime;
	CEdit	m_EditChCondEndCurrent;
	CEdit	m_EditChCondDetection;
	CEdit	m_EditChCondAutoStopTime;
	CEdit	m_EditUnderVoltage;
	CEdit	m_EditReserved2;
	CEdit	m_EditReserved1;
	CEdit	m_EditOverVoltage;
	CEdit	m_EditOverTemp;
	CEdit	m_EditChCondRelease;
	CEdit	m_EditOverCurrent;
	CEdit	m_EditLoadModeValue;
	CEdit	m_EditLoadModeSet;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_CellBALLoadModeSet)
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(Dlg_CellBALLoadModeSet)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnClose();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CELLBALLOADMODESET_H__BC7FDC64_B326_483E_B4DF_0B3B47BEA7BD__INCLUDED_)
