// LogDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "LogDlg.h"

#include "LogSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogDlg dialog


CLogDlg::CLogDlg(CWnd* pParent /*=NULL*/)
//	: CDialog(CLogDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CLogDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CLogDlg::IDD2):
	(CLogDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CLogDlg)
	m_bTopMostFlag = TRUE;
	//}}AFX_DATA_INIT
	m_nHideDelay = 5;
	m_pSetLayeredWindowAttributes = NULL;
	m_bUseAutoShow = TRUE;
}


void CLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogDlg)
	DDX_Control(pDX, IDC_LOG_LIST, m_ctrlLogList);
	DDX_Check(pDX, IDC_MOST_TOP_CHECK, m_bTopMostFlag);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogDlg, CDialog)
	//{{AFX_MSG_MAP(CLogDlg)
	ON_BN_CLICKED(IDC_MOST_TOP_CHECK, OnMostTopCheck)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_SETCURSOR()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_LOG_CLEAR, OnLogClear)
	ON_COMMAND(ID_LOG_CONFIG, OnLogConfig)
	ON_BN_CLICKED(IDC_FILE_VIEW_BUTTON, OnFileViewButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogDlg message handlers

void CLogDlg::OnOK() 
{
	// TODO: Add extra validation here
	ShowWindow(SW_HIDE);
	
//	CDialog::OnOK();
}

BOOL CLogDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CRect logRect;
	logRect.left = AfxGetApp()->GetProfileInt("LogSetting", "left", 0);
	logRect.top = AfxGetApp()->GetProfileInt("LogSetting", "top", 0);
	logRect.right = AfxGetApp()->GetProfileInt("LogSetting", "right", 0);
	logRect.bottom = AfxGetApp()->GetProfileInt("LogSetting", "bottom", 0);

	m_bTopMostFlag = AfxGetApp()->GetProfileInt("LogSetting", "TopMost", TRUE);
	m_bUseTranparent = AfxGetApp()->GetProfileInt("LogSetting", "Transparent", TRUE);
	m_nLogLevel = AfxGetApp()->GetProfileInt("LogSetting", "Log Level", CT_LOG_LEVEL_NORMAL);
	m_nHideDelay = AfxGetApp()->GetProfileInt("LogSetting", "Hide Delay", 5);		//자동 숨김 Delay 초
	m_bUseAutoShow = AfxGetApp()->GetProfileInt("LogSetting", "Auto Show", TRUE);		//자동 숨김 Delay 초


	m_strInstallPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path");

	if(logRect.Width() == 0 || logRect.Height() == 0)
	{
		OnMostTopCheck();
	}
	else
	{
		SetWinPos(logRect, m_bTopMostFlag);
		
	}
	UpdateData(FALSE);

	// Here we import the function from USER32.DLL
	HMODULE hUser32 = GetModuleHandle(_T("USER32.DLL"));
	m_pSetLayeredWindowAttributes = 
						   (lpfnSetLayeredWindowAttributes)GetProcAddress(hUser32, 
						   "SetLayeredWindowAttributes");


	// If the import did not succeed, make sure your app can handle it!
	if (NULL == m_pSetLayeredWindowAttributes)
		return FALSE; //Bail out!!!

	//If the function was imported correctly we must set the dialog we want to make transparent into "transparent-mode". 
	//E.G. Set the style for the dialog so that it can be transparent, and that is done with the flag WS_EX_LAYERED defined earlier.
	// Check the current state of the dialog, and then add the 
	// WS_EX_LAYERED attribute
	SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd, GWL_EXSTYLE)|WS_EX_LAYERED);

	SetLogEvent();

	//1 day event
	SetTimer(200, 24*60*60*1000, NULL);

	// TODO: Add extra initialization here
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLogDlg::OnMostTopCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CRect winRect;
	GetWindowRect(&winRect);

	if(m_bTopMostFlag)
	{
		ResetTransparent();
	}
	else
	{
		SetLogEvent();
	}
	SetWinPos(winRect, m_bTopMostFlag);
}

void CLogDlg::SetWinPos(CRect rect, BOOL bTopMost)
{
	if(bTopMost)	
		::SetWindowPos(m_hWnd, HWND_TOPMOST, rect.left, rect.top, rect.Width(), rect.Height(), SWP_SHOWWINDOW);
	else
		::SetWindowPos(m_hWnd, HWND_NOTOPMOST, rect.left, rect.top, rect.Width(), rect.Height(), SWP_SHOWWINDOW);
}

BOOL CLogDlg::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	KillTimer(200);
	
	CRect logRect;
	GetWindowRect(&logRect);

	AfxGetApp()->WriteProfileInt("LogSetting", "left", logRect.left);
	AfxGetApp()->WriteProfileInt("LogSetting", "top", logRect.top);
	AfxGetApp()->WriteProfileInt("LogSetting", "right", logRect.right);
	AfxGetApp()->WriteProfileInt("LogSetting", "bottom",logRect.bottom );

	AfxGetApp()->WriteProfileInt("LogSetting", "TopMost", m_bTopMostFlag);	

	AfxGetApp()->WriteProfileInt("LogSetting", "TopMost", m_bTopMostFlag);
	AfxGetApp()->WriteProfileInt("LogSetting", "Transparent", m_bUseTranparent);
	AfxGetApp()->WriteProfileInt("LogSetting", "Log Level", m_nLogLevel);
	AfxGetApp()->WriteProfileInt("LogSetting", "Hide Delay", m_nHideDelay);		//자동 숨김 Delay 초
	AfxGetApp()->WriteProfileInt("LogSetting", "Auto Show", m_bUseAutoShow);	
	
	return CDialog::DestroyWindow();
}

void CLogDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect rect;
	GetClientRect(rect);
	
	CWnd *pWnd = GetDlgItem(IDC_LOG_LIST);
	if( pWnd->GetSafeHwnd() )
	{
		CRect clientRect;
		pWnd->GetWindowRect(clientRect);
		ScreenToClient(clientRect);

		pWnd->MoveWindow(clientRect.left, clientRect.top, rect.Width()-clientRect.left*2, rect.Height()-clientRect.top-10);
	}
}

//1. List 에 Update
//2. Log 파일에 저장
//3. Log Dialog를 최상위로 한다음 Delay이후 서서히 사라지게 함 
void CLogDlg::WriteLog(CString strLogString, INT nLevel)
{

	//log level이 현재 설정된 level 보다 높으면 기록하지 않는다.
	//log level이 낮을 수록 사용자 Message에 가깝고, 높을 수록 시스템 Message에 가깝다.
	if(m_nLogLevel < nLevel)	return;
		
	//현재 시간 구함 
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogString);

//	COLORREF colLine;
//	switch( nLevel )
//	{
//	case EP_LIST_COLOR_BLACK: colLine = RGB(0,0,0);break;
//	case EP_LIST_COLOR_NONE: colLine = RGB(0,0,0);break;
//	case EP_LIST_COLOR_RED: colLine = RGB(255,0,0);break;
//	case EP_LIST_COLOR_BLUE: colLine = RGB(0,0,255);break;
//	case EP_LIST_COLOR_GREEN: colLine = RGB(0,255,0);break;
//	case EP_LIST_COLOR_ORANGE: colLine = RGB(255,125,0);break;
//	case EP_LIST_COLOR_JEAN: colLine = RGB(0,125,255);break;
//	case EP_LIST_COLOR_YELLOW: colLine = RGB(255,255,0);break;
//	case EP_LIST_COLOR_VIOLET: colLine = RGB(255,0,255);break;
//	}

	//List에 표기 
	if ( m_ctrlLogList.GetCount() > 200 )
	{
		m_ctrlLogList.DeleteString(0);
	}
	m_ctrlLogList.AddString(strMsg);
	m_ctrlLogList.SetCurSel( m_ctrlLogList.GetCount()-1);
	
	m_strLogFileName.Format("%s\\Log\\%s.log", m_strInstallPath, tm.Format("%Y%m%d"));


	//파일 유뮤 검색
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();

	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile )	
			aFile.SeekToEnd();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
	aFile.WriteString(strMsg + "\n");
	aFile.Close();
	
	//Log Event 발생 
//	SetLogEvent();		//ljb 20101004 주석 (권준구 요청)
}

void CLogDlg::WriteEventLog(CString strLogString, INT nLevel)
{
	
	//log level이 현재 설정된 level 보다 높으면 기록하지 않는다.
	//log level이 낮을 수록 사용자 Message에 가깝고, 높을 수록 시스템 Message에 가깝다.
	if(m_nLogLevel < nLevel)	return;
	
	//현재 시간 구함 
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogString);
		
	//List에 표기 
	if ( m_ctrlLogList.GetCount() > 200 )
	{
		m_ctrlLogList.DeleteString(0);
	}
	m_ctrlLogList.AddString(strMsg);
	m_ctrlLogList.SetCurSel( m_ctrlLogList.GetCount()-1);
	
	m_strLogFileName.Format("%s\\Event_Log\\%s.log", m_strInstallPath, tm.Format("%Y%m"));
	
	
	//파일 유뮤 검색
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogFileName) == FALSE )
	{

		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile )	
			aFile.SeekToEnd();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
	aFile.WriteString(strMsg + "\n");
	aFile.Close();
}

void CLogDlg::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	// Sets the window to 70% visibility.
	CDialog::OnTimer(nIDEvent);

	//과거 Log 삭제 Event
	//최근 1년간 log만을 유지한다.
	if(nIDEvent == 200)
	{
		long nFileTime, nExpireTime;
		CString strFileName, strTemp;
		
		CTime tm = CTime::GetCurrentTime() - CTimeSpan(365, 0, 0, 0);
		nExpireTime = tm.GetYear()*10000+tm.GetMonth()*100+tm.GetDay();
		
		CFileFind finder;
		strFileName.Format("%s\\Log\\*.log", m_strInstallPath);
		BOOL bFind = finder.FindFile(strFileName);
		
		while(bFind)
		{
			bFind = finder.FindNextFile();
			strFileName = finder.GetFilePath();
			strTemp = strFileName.Mid(strFileName.ReverseFind('\\')+1);
			nFileTime = atoi(strTemp.Left(strTemp.ReverseFind('.')));

			if(nFileTime < nExpireTime)
			{
				_unlink(strFileName);
			}
		}
		return;
	}

	ASSERT(m_pSetLayeredWindowAttributes);

	//초기 5초 가량은 투명해지지 않고 기다린다.
	//Timer 200ms*25 = 5000ms
	if(m_nCount  > 100)
	{
		m_nCount -=1;	
		
		return;
	}

	//투명도를 5%단위로 계속 늘린다.
	m_pSetLayeredWindowAttributes(m_hWnd, 0, 255*m_nCount/100, LWA_ALPHA);
	if(m_nCount <= 0)
	{
		KillTimer(100);
		
	}
	m_nCount -=1;	

	//최상위 Window이면 투명해지는 상태를 설정하지 않는다.
	if(m_bTopMostFlag)
	{
		ResetTransparent();
	}
}

//log Dialog에 Event가 발생시
void CLogDlg::SetLogEvent()
{

	if (NULL == m_pSetLayeredWindowAttributes)
		return; //Bail out!!!

	//1. 투명도를 0%로 한다.
	ResetTransparent();

	//2 투명도 사용여부 Check
	if(m_bUseTranparent == FALSE)	return;

	//3. 최상위 Window이면 투명해지는 상태를 설정하지 않는다.
	if(m_bTopMostFlag)	return;

	//4. 현재 숨김 속성이 아니면 최상위 Window로 가져 온다.
	if(m_bUseAutoShow && IsWindowVisible())
		SetForegroundWindow();

	m_nCount = 	100 + int((float)m_nHideDelay/0.2f);	//100%에 0.2초 Timer로 몇번인지 count하여 더한다. 
	SetTimer(100, 200, NULL);
}

//가장 적정한 것은 SetFocus
BOOL CLogDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// TODO: Add your message handler code here and/or call default
	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}

BOOL CLogDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN || pMsg->message == WM_LBUTTONDOWN
		|| pMsg->message == WM_RBUTTONDOWN)
	{
		SetLogEvent();	
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CLogDlg::ResetTransparent()
{
	//Timer를 죽이고 
	if(m_nCount < (100 + int((float)m_nHideDelay/0.2f)))
	{
		KillTimer(100);
	}
	//투명도를 없앤다.
	if(m_pSetLayeredWindowAttributes)
		m_pSetLayeredWindowAttributes(m_hWnd, 0, 255, LWA_ALPHA);
}

void CLogDlg::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: Add your message handler code here
	int nSelLanguage, nIDSel; //&&
	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	
	switch(nSelLanguage)
	{
	case 1:
		{
			nIDSel = IDR_POP_MENU;
			break;
		}
	case 2:
		{
			nIDSel = IDR_POP_MENU_ENG;
			break;
		}
	}

	CMenu menu; 
	//menu.LoadMenu(IDR_POP_MENU);
	menu.LoadMenu(nIDSel);
	CMenu* popmenu = menu.GetSubMenu(2); 
	CPoint ptMouse; 
	GetCursorPos(&ptMouse); 
	popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, this); 
	
}

void CLogDlg::OnLogClear() 
{
	// TODO: Add your command handler code here
	m_ctrlLogList.ResetContent();
}

void CLogDlg::OnLogConfig() 
{
	// TODO: Add your command handler code here

	CLogSetDlg	dlg;
	
	dlg.m_bUseAutoHide = m_bUseTranparent;
	dlg.m_nLogLevel = m_nLogLevel;
	dlg.m_nHideDelay = m_nHideDelay;
	dlg.m_bAutoShow = m_bUseAutoShow;
	if(IDOK == dlg.DoModal())
	{
		m_bUseTranparent = dlg.m_bUseAutoHide;
		m_nLogLevel = dlg.m_nLogLevel;
		m_nHideDelay = dlg.m_nHideDelay;
		m_bUseAutoShow = dlg.m_bAutoShow;
	}

	SetLogEvent();
}

void CLogDlg::OnFileViewButton() 
{
	// TODO: Add your control notification handler code here

	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	
	CString strTemp;
	strTemp.Format("Notepad.exe %s", m_strLogFileName);
	
	BOOL bFlag = CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
	if(bFlag == FALSE)
	{
		MessageBox(Fun_FindMsg("OnFileViewButton_msg","IDD_LOG_DLG"), "File not found", MB_OK|MB_ICONSTOP);
		//@ MessageBox("파일을 찾을 수 없습니다.", "File not found", MB_OK|MB_ICONSTOP);
	}
	CloseHandle( ProcessInfo.hProcess );
	CloseHandle( ProcessInfo.hThread );
}

void CLogDlg::Fun_SetTitle(CString strTitle)
{
	SetWindowText(strTitle);
}