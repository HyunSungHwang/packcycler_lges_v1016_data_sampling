// WorkStationSignDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "WorkStationSignDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWorkStationSignDlg dialog


CWorkStationSignDlg::CWorkStationSignDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWorkStationSignDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWorkStationSignDlg)
//	m_PlaceSign = _T("");
	//}}AFX_DATA_INIT
}


void CWorkStationSignDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWorkStationSignDlg)
	DDX_Control(pDX, IDC_STATIC_PLACESIGN, m_CntrlPlaceSign);
//	DDX_Text(pDX, IDC_STATIC_PLACESIGN, m_PlaceSign);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWorkStationSignDlg, CDialog)
	//{{AFX_MSG_MAP(CWorkStationSignDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_LBUTTONDOWN()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUT_CLOSE, OnButClose)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWorkStationSignDlg message handlers
BOOL CWorkStationSignDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString strPlaceName;
	strPlaceName = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Place Name", "N/A");
	m_CntrlPlaceSign.SetWindowText(strPlaceName);
	InitControlLbl();
	

	SIZE sizeMonitor;

	ZeroMemory(&sizeMonitor, sizeof(SIZE));
	sizeMonitor.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
	sizeMonitor.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);

	CRect dlgRect;
// 	dlgRect.left = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "PlaceSignLeft", 500);
// 	dlgRect.top = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "PlaceSignTop", 500);
 	dlgRect.left = sizeMonitor.cx-750; //yulee 20190719 CTSMonitorPro (ver. V1013 g.1.3.2.p20190718)
 	dlgRect.top = 0;

	CRect winRect;
	this->GetClientRect(&winRect);
	
	dlgRect.right = dlgRect.left + winRect.Width();
	dlgRect.bottom = dlgRect.top + winRect.Height();

	SetWinPos(dlgRect);
	return TRUE;
}

void CWorkStationSignDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{			
	CDialog::OnShowWindow(bShow, nStatus);	
}

void CWorkStationSignDlg::SetWinPos(CRect rect)
{
	::SetWindowPos(m_hWnd, HWND_NOTOPMOST, rect.left, rect.top, rect.Width(), rect.Height(), SWP_SHOWWINDOW);
}

void CWorkStationSignDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);
}

void CWorkStationSignDlg::InitControlLbl()
{
	m_CntrlPlaceSign.SetFontSize(30)
		.SetTextColor(RGB(0, 0, 0))
		.SetBkColor(RGB(255,255,255))
		.SetFontBold(TRUE)
		.SetFontName("HYheadlineM");
		//.SetFontName("HY 헤드라인M");//yulee 20190405 check
}

BOOL CWorkStationSignDlg::PreTranslateMessage(MSG* pMsg) 
{
	int nID = 0;
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam==VK_ESCAPE) return TRUE;
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam==VK_RETURN) return TRUE;
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam==VK_SPACE) return TRUE;
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CWorkStationSignDlg::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	CRect rect;
	GetClientRect(rect);
	pDC->FillSolidRect( rect, RGB(0,0,0) );
	
   return TRUE;
}

void CWorkStationSignDlg::OnButClose() 
{
	// TODO: Add your control notification handler code here
	CDialog::OnCancel();
}

