// CTSMonProDoc.h : interface of the CCTSMonProDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSMonProDOC_H__89A16358_4D1C_41DE_8139_700663157CB7__INCLUDED_)
#define AFX_CTSMonProDOC_H__89A16358_4D1C_41DE_8139_700663157CB7__INCLUDED_

#include "LogDlg.h"		// Added by ClassView
#include "OvenCtrl.h"
#include "DCLoad.h"		//  [4/24/2015 이재복]
#include <direct.h>		//ljb ForceDirectory용

// Added by 224 (2015/03/31) : Uart 지원
// #include "UartCanConvBroker.h"
// #include "UartConvDlg.h"

#include "modbus.h"
#include "LinCanConvBroker.h"
#include "UartCanConvBroker.h"

#include "xlLINFunctions.h"
#include "xlCANFunctions.h"
#include "ChillerCtrl/ChillerCtrl.h"
#include "ChamberCtrl/ChamberCtrl.h"
#include "ModbusConvDlg.h"
#include "ModbusCanConvBroker.h"

#include "ExitProgressWnd.h" //ksj 20201023 : 프로그램 종료 진행 상황 안내 창 추가

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define		MAX_MODUEL_RESEND	(3)

class CAlarmReadyDlg;		

class CCalibratorDlg;
class CSafetyUpdateDlg;
class CSchUpdateDlg;

typedef struct Code_Information {
	BYTE code;
	char szMsg[32];
	char szDescript[256];
} CODE_INFO;

typedef struct Flicker_Information{
	int nCode;
	COLORREF dwColor;
	int nPriority;
	int nShowColorTable;
	int nAutoBuzzerStop; //ksj 20201013 : 부저 자동 중단 기능 추가.
} FLICKER_INFO;

class CUartConvDlg ;
class CCANStandAloneFunc ;
class CLinConvDlg ;
class CCTSMonProDoc : public CDocument
{
protected: // create from serialization only
	CCTSMonProDoc();
	DECLARE_DYNCREATE(CCTSMonProDoc)

// Attributes
public:
	int     CopyDirectory(CString szFrom, CString szTo);   //ksj 20210727 : 폴더 복사 함수 추가
	BOOL	ForceDirectory(LPCTSTR lpDirectory);
	int		FileExists(LPCTSTR lpszName);
	CString GetFilePath(LPCTSTR lpszFilePath);
	BOOL	DirectoryExists(LPCTSTR lpszDir);
	BOOL	file_Finder(CString str_path);		
	void	Log_write(CString strLogString, int option);	
	void	Fun_GetArryCanDivision(CUIntArray & arryCanDivision);
	void	Fun_GetArryCanStringName(CStringArray & arryCanStringName);
	void	Fun_GetArryAuxDivision(CUIntArray & arryAuxDivision);
	void	Fun_GetArryAuxStringName(CStringArray & arryAuxStringName);
	void	Fun_GetArryAuxThrDivision(CUIntArray & arryAuxThrDivision);
	void	Fun_GetArryAuxThrStringName(CStringArray & arryAuxThrStringName);
	void	Fun_GetArryAuxHumiDivision(CUIntArray & arryAuxThrDivision);
	void	Fun_GetArryAuxHumiStringName(CStringArray & arryAuxThrStringName);
	BOOL	Fun_ReloadDivisionCode();
	int		GetPattPathFromDB(int ReserveID, int nStepNo, CString &strPattPath);
	CString Fun_GetFunctionString(UINT nType, int nDivision); //ksj 20201005
	
	CAlarmReadyDlg		* m_pAlarmReadyDlg[MAX_MODUEL_RESEND];
	CAlarmReadyDlg		* m_pAlarmReadyDlg1;
	CAlarmReadyDlg		* m_pAlarmReadyDlg2;
	CAlarmReadyDlg		* m_pAlarmReadyDlg3;
	UINT				selectModule;
	CWordArray			reawSelCh;
	CCyclerModule		*repMD[MAX_MODUEL_RESEND];		
	CCyclerModule		*repMD_1;
	CCyclerModule		*repMD_2;
	CCyclerModule		*repMD_3;
	SFT_STEP_END_BODY resteEnd;
	UINT				renModuleID; 	
	int					m_wait_FTP_endTime_plusPtrn;
	int					m_wait_FTP_endTime;		
	SFT_CHAMBER_VALUE	m_sChamData_1;				//PSERVER.DLL에 챔버 데이터 전달
	SFT_CHAMBER_VALUE	m_sChamData_2;				//PSERVER.DLL에 챔버 데이터 전달
	SFT_LOADER_VALUE	m_sLoaderData1;				//PSERVER.DLL에 Loader 데이터 전달	20150427 add
	SFT_LOADER_VALUE	m_sLoaderData2;				//PSERVER.DLL에 Loader 데이터 전달	20150427 add
	int 				m_nSendingModeComm_ch1;				//0:skip, 1:CP, 2:CV, 3:CC, 4: CR 명령 전송 	20150427 add
	int					m_nSendingModeComm_ch2;				//0:skip, 1:CP, 2:CV, 3:CC, 4: CR 명령 전송 	20150427 add
	int					m_nRunCmdDelayTime;
	CString				m_strModuleName;
	CPtrArray			m_aTestCond;
	int					m_nTestCondStepNo;
	int					m_nAutoRestartRemainTime;
	CString				m_strPassword;	
	CCalibratorDlg*		m_pCalibratorDlg;
	CSafetyUpdateDlg*	m_pSafetyUpdateDlg;
	CSchUpdateDlg*		m_pSchUpdateDlg;
	BOOL				m_bAutoRestartFromPrevUnsafeShutdown;	
	COvenCtrl			m_ctrlOven1;
	COvenCtrl			m_ctrlOven2;	

	CDCLoad				m_ctrlLoader1;				//  [4/24/2015 이재복]
	CDCLoad				m_ctrlLoader2;				//  [4/24/2015 이재복]

	int					m_nOvenPortLine;			//챔버 라인	

	int					m_iOvenStartOption_1;		//챔버 Start Option
	float				m_fOvenStartFixTemp_1;		//챔버 Start Temp
	UINT				m_uiOvenCheckFixTime_1;		//Fix Mode 온도 비교 시간 1초 = 100
	float				m_fOvenDeltaFixTemp_1;		//Fix Mode 허용 온도 비교차
	UINT				m_uiOvenPatternNum_1;		//챔버 Start Pattern Number
	UINT				m_uiOvenCheckTime_1;		//온도 비교 시간 1초 = 100
	float				m_fOvenDeltaTemp_1;			//허용 온도 비교차
	int					m_iOvenStartOption_2;		//챔버 Start Option
	float				m_fOvenStartFixTemp_2;		//챔버 Start Temp
	UINT				m_uiOvenCheckFixTime_2;		//Fix Mode 온도 비교 시간 1초 = 100
	float				m_fOvenDeltaFixTemp_2;		//Fix Mode 허용 온도 비교차
	UINT				m_uiOvenPatternNum_2;		//챔버 Start Pattern Number
	UINT				m_uiOvenCheckTime_2;		//온도 비교 시간 1초 = 100
	float				m_fOvenDeltaTemp_2;			//허용 온도 비교차	

	int					m_nChamberOperationType;	//ljb 20170313 chamber operation type
													// 0 : 삼성, 1 : SK 

	int					m_nChamberWriteCount1;		//ljb 20170720 chamber log 남기기 count
	int					m_nChamberWriteCount2;		//ljb 20170720 chamber log 남기기 count

	int					m_nChamberStopRetry1;  //yulee 20180910
	int					m_nChamberStopRetry2;  //yulee 20180910
	int					m_nIsChamberStop1;		//yulee 20180910
	int					m_nIsChamberStop2;		//yulee 20180910


	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSMonProDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	//int SendAuxDataRequest(UINT nModuleID); //ksj 20160624
	BOOL Fun_FtpDownSelStepEndData(int nModuleID, int nChannelIndex, int nOption = 0);
	int ParsingStartIndexFile(CString strStartFileName, int &nSBCFileNo);
	BOOL SetCalibrationStartEnd(int nModuleID,  SFT_DAQ_ISOLATION * DAQCommandData);	//201301 //ksj 20200819 : 자동 교정 기능 이식
	BOOL Fun_FtpDownSelChannelData(int nModuleID, CWordArray *pSelChArray);
	BOOL Fun_FtpDownSelChannelAllSBCData(int nModuleID, CWordArray *pSelChArray, int &nChkNum, int &nTotalSBCFileNum, int &nDownNum);//ylee 20190420
	BOOL Fun_FtpDownAllChannelData(int nModuleID);
	static UINT ThreadFtpDownload(LPVOID pParam);
	BOOL Fun_ChkCompleteDownAllSBCFile(int nModuleID, CWordArray *pSelChArray); //yulee 20190424
	BOOL Fun_MakeOrEditFTPDownResultFile(int nModuleID, int ChannelIndex, BOOL bRet, int SBCTotalFileNum, int CheckCntNum, int DownNum);//yulee 20190424
	BOOL Fun_GetTokenSBCAllDown(CString strLine, CStringArray &ArrStr); //yulee 20190424
	int		m_nFtpResponseCode;

	BOOL	m_bDayChange;			//ljb 20150522 날자 변경 되면 모든 채널 데이터를 FTP down 한다.
	BOOL	m_bRunFtpDown;			//ljb 20150522 TIMER_LIVE_CHECK 타이머로 작업완료를 검사 한다. 
	//  (채널에 IsFtpDataAllDownOK 함수가 OK이면 모든 데이터 다운 완료)	
	BOOL	m_bWorkingFtpDown;		//ljb 20150522 FTP download 동작 중 이면 True
	CDWordArray m_awAutoRestoreChArray;		//ljb 20150522 자동 복구 위해 모듈정보 채널 정보 입력

	BOOL	m_bRunFtpDownStepEnd;			//ksj 20160801 작업종료시 StepEnd Raw Data 다운로드.	
	int		m_nFtpDownStepEndModuleID;		//ksj 20160801 다운로드할 모듈 ID;
	int		m_nFtpDownStepEndChannelIndex;	//ksj 20160801 다운로드할 채널 Index;
	unsigned char	m_ucFtpDownIndex;	//lyj 20200526 다운로드 개선
	int		m_FTPFileDownRetryCnt;//yulee 20190424
	BOOL	m_bSucessFtpDownAllSBCData;//yulee 20190424
	BOOL	m_bRunFtpDownAllSBCData;		//yulee 20190420 작업 시작 시 모든 Raw Data 다운로드.
	//--------------------------------------------------------------------
	// modbus - can converting 모듈. Added by 224 (2014/04/23)
	//--------------------------------------------------------------------
	
	UINT	m_nSlaveID;
	modbus_t* ctx;
    UINT m_wTimerRes;
	//	DWORD m_timerID ;
	//	CCANFunctions* m_pCAN;
	CCANStandAloneFunc* m_pCAN;
	CModbusConvDlg* m_pModbusDlg ;
	CLinConvDlg* m_pLinDlg ;
	CUartConvDlg* m_pUartDlg ;
	BOOL	m_bUseHLGP;

	static BOOL m_bSendCANData ;
	//	static MCC_MODE m_nModbusMode ;
	//	static CString m_strModbusSettingName ;
	//	static void CALLBACK TimeProc(UINT _timerId, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2) ;
	void StopMCCThread(INT nDevID);
	void PauseMCCThread(INT nDevID);
	BOOL BeginMCCThread(int nSelCh, int nSelModule, CString strDBName);
	//	void SendCANData(XLevent& xlEvent, CWnd* pWnd, BOOL bShow = TRUE);
	
	INT m_nModbusAddr ;
	INT GetModbusAddr() { ASSERT(this) ; return m_nModbusAddr ; }
	
	INT m_nModbusLen ;
	INT GetModbusLen() { ASSERT(this) ; return m_nModbusLen ; }
	
	CString m_strModbusCommunication ;
	INT m_nModbusDatabit  ;
	INT m_nModbusStopbit  ;
	CHAR m_cModbusParity  ;
	LONG m_lModbusBitrate ;
	CString m_strModbusIPAddr ;
	INT m_nModbusPort ;
	
	BOOL m_bSaveRxFlag ;
	void SetSaveRxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveRxFlag = b ; }
	
	BOOL m_bSaveTxFlag ;
	void SetSaveTxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveTxFlag = b ; }
	
	//	INT m_nChannel ;
	INT GetLinCanChannel(INT nDevID)    { ASSERT(this) ; return m_LinCanConv[nDevID].m_pCAN->GetChannelNo() ; }
	INT GetModbusCanChannel(INT nDevID) { ASSERT(this) ; return m_ModbusCanConv[nDevID].m_pCAN->GetChannelNo() ; }
	INT GetUartCanChannel(INT nDevID) { ASSERT(this) ; return m_UartCanConv[nDevID].m_pCAN->GetChannelNo() ; }
	
#define MAX_MODBUS_SET		8	// 최대 LIN-CAN 장비 수. 하나의 쌍으로 작동한다. 최대 PCI 슬롯 4개
	
	CModbusCanConvBroker m_ModbusCanConv[MAX_MODBUS_SET] ;
	MCC_MODE GetModbusMode(INT nDevID) { ASSERT(this) ; return m_ModbusCanConv[nDevID].m_nModbusMode ; }
	
	void ClearModbusAllShowMessageFlag() ;
	void SetModbusShowMessageFlag(INT nDevID, CListBox* pRx, CListBox* pTx) ;
	
	//--------------------------------------------------------------------
	// LIN - can converting 모듈. Added by 224 (2014/10/09)
	//--------------------------------------------------------------------
#define MAX_LIN_SET		4	// 최대 LIN-CAN 장비 수. 하나의 쌍으로 작동한다. 최대 PCI 슬롯 4개
	CRITICAL_SECTION m_csShowMessage ;
	
	//	CLINFunctions* m_pLIN ;	
	//	CLINFunctions m_LIN ;	
	// 	BOOL m_bLinOnBus ;			// 스케쥴러 쓰레드에서의 진행여부 플래그
	// 	UINT m_nLinID ;
	void StopLinCanConvThread(INT nDevID);
	void PauseLinCanConvThread(INT nDevID);
	BOOL BeginLinCanConvThread(int nSelCh, int nSelModule, CString strDBName);
	
	// LIN-CAN 컨버팅 모듈 - 멀티 채널 지원을 위해 Broker 사용
	CLinCanConvBroker m_LinCanConv[MAX_LIN_SET] ;
	LCC_MODE GetLinMode(INT nDevID) { ASSERT(this) ; return m_LinCanConv[nDevID].m_nLinMode ; }
	
	void ClearLinAllShowMessageFlag() ;
	void SetLinShowMessageFlag(INT nDevID, CListBox* pRx, CListBox* pTx) ;
	
	//--------------------------------------------------------------------
	
	//--------------------------------------------------------------------
	// UART - can converting 모듈. Added by 224 (2015/03/25)
	//--------------------------------------------------------------------
	
#define MAX_UART_SET		8	// 최대 UART-CAN 장비 수. 하나의 쌍으로 작동한다. 최대 PCI 슬롯 4개
	
	void StopUartCanConvThread(INT nDevID);
	void PauseUartCanConvThread(INT nDevID);
	BOOL BeginUartCanConvThread(int nSelCh, int nSelModule, CString strDBName);	
	
	// UART-CAN 컨버팅 모듈 - 멀티 채널 지원을 위해 Broker 사용
	CUartCanConvBroker m_UartCanConv[MAX_UART_SET] ;
	UCC_MODE GetUartMode(INT nDevID) { ASSERT(this) ; return m_UartCanConv[nDevID].m_nUartMode ; }
	
	void ClearUartAllShowMessageFlag() ;
	void SetUartShowMessageFlag(INT nDevID, CListBox* pRx, CListBox* pTx) ;
	
	//--------------------------------------------------------------------
		
	BOOL Send_CMD_SCHEDULE_END(CCyclerModule *pMD, CWordArray *pSelChArray, SFT_STEP_END_BODY steEnd, unsigned int nModuleID, unsigned int nWaitTime = 0 );	
	BOOL Send_CMD_SCHEDULE_END2(CCyclerModule *pMD, CWordArray *pSelChArray, SFT_STEP_END_BODY steEnd, unsigned int nModuleID );	
	BOOL Fun_CheckReadyDlg(int nModuleID);
	void Fun_ReadyFromSbc(int nModuleID,int nResponse);
	void Fun_SaveFileSBCbinary(CString strFile);
	BOOL Fun_FtpConnect(CString strIP);
	void Fun_FtpDisConnect();
	BOOL Fun_FtpChangeDir(CString strPath);		//FTP /root 에서 전달받은 인수 폴더로 이동(없으면 생성) ex. /schedule/ch001
	BOOL Fun_FtpFileFind(CString strFindFile);
	BOOL Fun_FtpFileDeleteAll(CString strPath);
	BOOL Fun_FtpPutFile(CString strFullPath);
	BOOL Fun_FtpGetFile(CString strFullPath, CString strFtpFileName); //ksj 20160728 (SBC Raw Data 다운로드를 위해 추가.)
	BOOL Fun_FtpFileList();	
	BOOL SendScheduleToModuleTest();
	BOOL Fun_CheckPatternFile(CString strFile);
	void UpdateTextMonitoringFile();		//20110319 KHS
	void Fun_AddLossModuleData(UINT nModule, UINT nChannel);
	BOOL Fun_AllPause();
// 	BOOL ForceDirectory(LPCTSTR lpDirectory);
// 	int FileExists(LPCTSTR lpszName);
// 	CString GetFilePath(LPCTSTR lpszFilePath);
// 	BOOL DirectoryExists(LPCTSTR lpszDir);
// 	BOOL file_Finder(CString str_path);

	int SetDAQIsolationDataToModule(UINT nModuleID, SFT_DAQ_ISOLATION * DAQCommandData);		//20100116 ljb
	int SetDAQIsolationDataToChannel(UINT nModuleID, SFT_DAQ_ISOLATION * DAQCommandData, int nOption = 0); 		//ksj 20210104 : 채널단위 전송 기능 및 절연 커맨드 종류 옵션 추가

	int SetMuxCommandDataToModule(UINT nModuleID, CWordArray *pSelCh,  SFT_MUX_SELECT * MuxCommandData);		//20100116 ljb
//	int SetLINCANSelectCommandDataToModule(UINT nModuleID, int nSelCh, SFT_LINCAN_SELECT * LINCANSelect); //yulee 20181114
	
	//int SetRTTableSendEndSignalToModule(UINT nModuleID, SFT_RT_TABLE_SEND_END *RT_Table_End_Flag);
	int SetRTTableSendEndSignalToModule(UINT nModuleID, void *RT_Table_End_Flag, int nRtType); //ksj 20200207
	int SetCANTransDataToModule(UINT nModuleID, SFT_TRANS_CMD_CHANNEL_CAN_SET * pCanData, int nChIndex);
	void CheckUsePort();

	void LoadOvenData(int nItem);
	BOOL CheckOven_Line1(int nOvenID);
	BOOL CheckOven_Line2(int nOvenID);
	void CheckOvenStopStateAndSendStopCmd(CDWordArray &adwCheckChArray, int nChNo = 1);
//	void ChkPauseChannelStart(int iNo,int nOvenID);//lmh 20111125 챔버 연동 & 충방전 시 온도 대기했다가 작업 계속 전송
	void OvenStartStateAndSendStartCmd(int nOvenPort, int nStartOption, UINT nPatternNum, float fTemp,int nOvenId = 0);
	void OvenCheckOption(int nOvenPort, UINT nCheckTime,float fDeltaTemp);
	void OvenCheckFixOption(int nOvenPort, UINT nCheckTime,float fDeltaTemp);
	void OvenStopStateAndSendStopCmd(int nOvenPort);
	CString GetLastErrorString();
	
	int StepValidityCheck(CStep *pStep, long lMaxVoltage, long lMaxCurrent);
	int ScheduleValidityCheck(CScheduleData *pSchedule, CDWordArray * pdwTargetCh);
	int SendAuxDatatoModuleA(UINT nModuleID, STF_MD_AUX_SET_DATA_A * auxData);
	int SetCANDataToModule(UINT nModuleID, SFT_RCV_CMD_CHANNEL_CAN_SET * pCanData, int nChIndex);
	int SetCANChangeDataToModule(UINT nModuleID, SFT_SEND_CMD_CAN_CHANGE_SET * pCanChangeData, int nChIndex);
	
	SFT_STEP_SIMUL_CONDITION GetNetworkFormatSimulStep(CStep *pStep);
	UINT GetStateImageIndexA(WORD state, WORD nType,  WORD nParallelType);
	float UnitTrans(float fData, BOOL bSave, int nAuxType);
	float VUnitTrans(float fData, BOOL bSave);
	float TUnitTrans(float fData, BOOL bSave);
//	SFT_STEP_SIMUL_CONDITION GetNetworkFormatStepSimul(CStep * pStep);
	BOOL LoadPatternDataFromFile(CString strFileName, STF_MD_USERPATTERN_DATA &);
	BOOL LoadPatternDataFromFileForFtp(int nModuleID, CString strFileName, long &lTimeType, long &lModeType, long &lPlusMode);	//ljb 20130404 add
	BOOL LoadPatternDataFromFileForCheckSum(CString strFileName, long &lFileSize, long &lCheckSum);
	BOOL Fun_MemoryCopyFileAndCheckSum(CString strScrFileName, CString strDscFileName, ULONG &lFileSize, ULONG &lCheckSum);
//	BOOL LoadSbcSchedule(CString strFileName);

	int SendCanDatatoModule(UINT nModuleID, STF_MD_CAN_SET_DATA * canData);		//20090202 ljb
	int SendAuxDatatoModule(UINT nModuleID, STF_MD_AUX_SET_DATA * auxData);
	int SendAuxDatatoModule1015(UINT nModuleID, STF_MD_AUX_SET_DATA_V1015 * auxData); //ksj 20201013 : V1015 호환 추가
	int SendParallelDatatoModule(UINT nModuleID);
	CString GetDataUnit(int nItem);
	void Wait_Milli(DWORD dwMillisecond);//yulee 20190314
	BOOL m_bChContinueNo;
	INT m_VoltageUnitMode;
	INT	m_CurrentUnitMode;
	int m_nTimeUnit;
	CString m_strVUnit;
	int m_nVDecimal ;
	CString m_strIUnit;
	int m_nIDecimal;
	CString m_strCUnit;
	int m_nCDecimal;
	CString m_strWUnit;
	int m_nWDecimal;
	CString m_strWhUnit;
	int m_nWhDecimal;
	UINT	m_nInstalledModuleNo;					//20090105 KHS 설치된 모듈의 수

	CWinThread*			m_FileSaveThread;			//파일 저장 쓰레드
	CWinThread*			m_LoaderThread1;
	CWinThread*			m_LoaderThread2;
	CWinThread*			m_SBCFileDownThread;	//SBC 파일 다운 Thread
	CWinThread*			m_OvenThread1;		//ljb 20180219 add 챔버 체크 쓰레드
	CWinThread*			m_OvenThread2;		//ljb 20180219 add 챔버 체크 쓰레드
	
	void UpdateUnitSetting();
//	void CheckRunningChannel(int nModuleID);
	BOOL ExecuteExcel(CString strFileName);
	BOOL SendContinueCmd(int nModule, CWordArray *pSelChArray);
	BOOL SaveRunInfoToTempFile(int nModuleID, CWordArray *pSelChArray);
	CString GetEmgMsg(long lCode);
	CString GetCmdString(int nCmd);
	BOOL SetColorConfig();
	UINT GetInstallModuleCount()		{		return m_nInstalledModuleNo;	}


	CString	ValueString(long lData, int item);
	BOOL ValueString_CellCode(long lData, int item, CString *srtName, CString *strDesc);
	CString GetModuleIPAddress(int nModuleID, BOOL bUserDlg = TRUE);
	BOOL WriteModuleInfoToDataBase(int nModuleID);
	BOOL ExecuteEditor(CString strModel = "", CString strTest = "", BOOL bNewWnd = FALSE);
	BOOL ExecuteProgram (	CString strProgram, 
							CString strArgument = "", 
							CString strClassName = "", 
							CString strWindowName = "", 
							BOOL bNewExe = FALSE,
							BOOL bWait = FALSE
						);
	void ShowLogDlg();
	void ShowEventLogDlg();	//ljb 201012
	void CalibrationExe(int nModuleID, CWnd *pParent = NULL);
	void Fun_SafetyUpdateExe(CDWordArray &adwTargetCh, CWnd *pParent = NULL);
	void Fun_SchUpdateExe(CDWordArray &adwTargetCh, CWnd *pParent = NULL);
	BOOL CompletedTest(UINT nModuleID, UINT nChIndex);
	SFT_STEP_CONDITION_V2 GetNetworkFormatStep(CStep *pStep); //lyj 20200214 LG v1015 미만
	SFT_STEP_CONDITION_V3 GetNetworkFormatStep_V1015(CStep *pStep); //lyj 20200214 LG v1015 이상
//	BOOL SendStartCmd(UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, int nStartCycleNo =0, int nStartStepNo = 0, int nStartOptChamber = 0);
	
	BOOL SendStartCmd(CProgressWnd * pProgWnd, CDWordArray *pSelChArray, CScheduleData *pSchData, int nStartCycleNo =0, int nStartStepNo = 0, int nStartOptChamber = 0, int nOption = 0, int ReserveID = 0);
	BOOL SendSchUpdateCmd(CProgressWnd * pProgWnd, CDWordArray *pSelChArray, CScheduleData *pSchData);
	BOOL SendScheduleToModule(UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd * pProgWnd);
	BOOL SendScheduleToModuleFTP(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, int nOption = 0, int ReserveID = 0);
	BOOL SendScheduleToModuleFTP2(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd);
	BOOL InitChScheduleToModuleFTP(UINT nModuleID, CWordArray *pSelChArray);	//ljb 20151210 add
	CCaliPoint * GetCalPoint();
	BOOL CheckCmdState(UINT nModuleID, UINT nChannelIndex, UINT nCmdID, BOOL bModule = FALSE);
	BOOL CheckCmdUIState(UINT nModuleID, UINT nChannelIndex, UINT nCmdID, BOOL bModule = FALSE);
	
	CString GetDataBaseName()	{	
		return m_strDataBaseName;	
	}
	CString GetWorkInfoDataBaseName()	{	return m_strWorkInfoDataBaseName;	}	//ljb 20160314 add

	CString GetCodeDataBaseName()	{	return m_strCodeDataBaseName; } //ksj 20200804

	CCyclerModule * GetCyclerMD(UINT nModuleID);
	INT GetModuleIndex(int nModuleID);
	UINT	GetModuleID(UINT nIndex);
	CCyclerModule * m_lpCyclerMD;
	CCyclerModule * GetModuleInfo(UINT nModuleID);
	CCyclerChannel * GetChannelInfo(int nModuleID/* One Base*/, int nChIndex /*Zero Base*/);
	CString m_strCurPath;
	CString m_strDataBaseName;
	CString m_strCodeDataBaseName; //ksj 20200804
	CString m_strWorkInfoDataBaseName;
	UINT GetStateImageIndex(WORD state, WORD nType = 0);
	CString GetModuleName(int nModuleID);

	void	WriteSysLog(CString strMsg, INT nLevel = CT_LOG_LEVEL_NORMAL);
	void	WriteSysEventLog(CString strMsg, INT nLevel = CT_LOG_LEVEL_NORMAL);		//ljb 201012
	void	WriteWorkLog(int nModuleID, int nChannelIndex, CString strMsg, INT nLevel = CT_LOG_LEVEL_NORMAL);

	BOOL	SendCommand(UINT nModuleID, CWordArray *apSelChArray, UINT nCmdID, int nCycleNo =0, int nStepNo = 0);
//	BOOL	SendCommand(CDWordArray *adwTargetCh, UINT nCmdID, int nCycleNo =0, int nStepNo = 0);
	bool	PollingControlServer();
	void	SetChannelData(int nModuleID, char* pBuf, int nSize);
	static UINT	SetChannelDataThread(LPVOID pParam);				//201101 파일저장 스레드
	static UINT ChkLoaderState_1(LPVOID pParam);
	static UINT ChkLoaderState_2(LPVOID pParam);

	static UINT ChkOvenState_1(LPVOID pParam);
	static UINT ChkOvenState_2(LPVOID pParam);
	
	void 	SetCalResultData(int nModuleID, char* pBuf);
	void 	SetStepInfoResultData(int nModuleID, int nChannelIndex, char* pBuf);
	void 	SetSafetyInfoResultData(int nModuleID, char* pBuf);
	
	void	SetCalCommConnected();
	BOOL	InitNetworkConfig();
	//Module 접속 완료시 
	void	SetModuleLineOn(UINT nModuleNo, LPSFT_MD_SYSTEM_DATA lpSysParam);
	void	ResetModuleLineState(UINT nModuleNo);

	//+2015.9.23 USY Add For PatternCV
	BOOL	CheckPatternData(CString strFile, float fMaxVoltage, float fMaxCurrent, UINT nParallelCnt);
	//-
	
	//2014.12.10 간이 충방전 사용유무
	BOOL m_bSimpleTest;	
	BOOL m_bChamberStand; //2014.12.23 챔버 대기 모드 1: 사용 0 : 미사용

	CUIntArray	m_uiArryCanDivition;	//ljb 2011218 이재복 //////////
	CStringArray m_strArryCanName;		//ljb 2011218 이재복 //////////
	CUIntArray	m_uiArryAuxDivition;	//ljb 2011218 이재복 //////////
	CStringArray m_strArryAuxName;		//ljb 2011218 이재복 //////////
	CUIntArray	m_uiArryAuxThrDivition;
	CStringArray m_strArryAuxThrName;
	CUIntArray	m_uiArryAuxHumiDivition; //ksj 20200207 : 습도 센서 추가
	CStringArray m_strArryAuxHumiName; //ksj 20200207 : 습도 센서 추가
	BOOL m_bOverChargerSet; //yulee 20191017 OverChargeDischarger Mark
	int m_bChkVolt;

	CString m_strModuleInfo;	//ljb 20150731 add
	CChillerCtrl m_chillerCtrl[MAX_CHILLER_COUNT];
	CChamberCtrl m_ChamberCtrl; //yulee 20190608
    void LoadChillerData(int nItem);
	void LoadChamberData(int nItem);//yulee 20190608

	virtual ~CCTSMonProDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void	WriteLog(CString strMsg, INT nLevel = CT_LOG_LEVEL_NORMAL); //yulee 20181008 

	CString m_strLastErrorString;					//최후 Error Message
protected:
	//void	WriteLog(CString strMsg, INT nLevel = CT_LOG_LEVEL_NORMAL);
	void	WriteEventLog(CString strMsg, INT nLevel = CT_LOG_LEVEL_NORMAL);	//ljb 201012
	CString GetExcelPath();
//	BOOL LoadCodeTable();
//	CList < CODE_INFO, CODE_INFO&>	m_codeInfoList;
	BOOL SetModuleConfig();
//	PS_COLOR_CONFIG *m_pStateColor;					//상태별 표기할 색상 및 문자 설정값
	BOOL	AssignModuleMemory(UINT nTotalModule);	//모듈관리 위한 메모리 할당.
	void	RemoveModule();							//모듈 메로리 제거
//	UINT	m_nInstalledModuleNo;					//설치된 모듈의 수 
	BOOL	ReadRegistryInfo();						//Registry에 설정된 설정변수 Loading
	CCaliPoint	m_CalPointData;						//현재 설정된 Calibration 조건 
	CLogDlg *	m_pLogDlg;							//Action Log 화면 
	CLogDlg *	m_pEventLogDlg;						//Event Log (이상 로그)
	UINT	m_nSaveStackSize;						//저장 Data를 일정 수량 만큼 쌓은 다음에 한꺼번에 파일에 기록하기 위한 변수
	
	UINT	SearchInstalledModuleCount();			//현재 DataBase에 설정되 모듈의 총수 검색

	// 200313 HKH Memory Leak 수정 ===============
	bool FinishDocThread();

	bool m_bFinishFileSave;
	bool m_bFinishFileSaveAck;
	bool m_bFinishSBCFileDown;
	bool m_bFinishSBCFileDownAck;
	bool m_bFinishLoader1;
	bool m_bFinishLoader1Ack;
	bool m_bFinishLoader2;
	bool m_bFinishLoader2Ack;
	// =============================================

	// Generated message map functions
protected:
	ftp *theFtpConnection;

	//{{AFX_MSG(CCTSMonProDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


public:
#if 0 //MDB 자동 업데이트 관련하여, CUpdateDatabase 클래스로 코드 이동 및 정리.	
	int ExecuteQueryDB(CStringArray& saSQL, CString strDatabaseName, BOOL bFailContinue = TRUE);
	int UpdateChannelcode_v1016();
	int UpdateDefaultUser();
	int GetMDBChannelCodeCount(CDaoDatabase& db, CString strTable, int nChannelCode);
	int GetMDBUserIDCount(CDaoDatabase& db, CString strUserID);	
	int UpdateMDBAuxH(void);//ksj 20200211 : MDB에 자동으로 습도센서 AuxH를 추가한다
	int UpdateMDBSequence(void);
	int UpdateMDBFlicker(void); //lyj 20201005	
#endif

	int UpdateDatabase(void);
	int LoadFlickerTable(void); //lyj 20201005
	int SendChAttributeRequest(int nModuleID);	
	int GetDirFilesNum(CString dirName);
	CString GetLocalIPAddress(void);
	BOOL SetFTPCache(void);
	BOOL SetWindowsMinidump(void);

	CList < FLICKER_INFO, FLICKER_INFO&> m_FlickerInfoList; //lyj 20201005
	CCriticalSection m_csFlickerInfoList;

	CString GetChannelCodeTableLangName(void);
	int InitDatabasePath(void);
	void SetAppExitProgress(int nPos);
	double CompareSystemTime( PSYSTEMTIME pTargetTime, PSYSTEMTIME pCompareTime ); //ksj 20201115
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSMonProDOC_H__89A16358_4D1C_41DE_8139_700663157CB7__INCLUDED_)

