// CTSMonProView.cpp : implementation of the CCTSMonProView class
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"
#include "CaliSetDlg.h"
#include "MaintenanceDlg.h"
#include "SelectScheduleDlg.h"
#include "StartDlg.h"
#include "StartDlgEx.h" //ksj 20160614
#include "LoginManagerDlg.h"
#include "RegeditManage.h"

#include "UserConfigDlg.h"
#include "RestoreDlg.h"
#include "NoInputDlg.h"
#include "SimpleTestDlg.h"
#include "ParallelConfigDlg.h"
#include "AuxDataDlg.h"
#include "SettingCanDlg.h"
#include "SettingCanDlg_Transmit.h"
#include "SettingCanFdDlg.h"
#include "SettingCanFdDlg_Transmit.h"
#include "SettingCanChangeDlg.h"
#include "WorkWarnin.h"
#include "ReadyRunDlg.h"

#include "ReserveDlg.h"			//ksj 20160426
#include "GotoStepDlg.h"
#include "VoltageCheckDlg.h" //ksj 20160726

#include "RTTableSet.h" //yulee 20181118

//#include "MiniDump.h"//yulee 20190111
#include "AuxVentSafetyDlg.h" //ksj 20200116
#include "FlickerConfigDlg.h" //lyj 20200824

#include "JsonMon.h" //ksj 20200902 : [#JSonMon] JsonMon 클래스 헤더 추가.

#include "MainFrm.h"
#include "ShowEmgDlg.h"
#include "Global.h"
#include "./Global/Mysql/PneApp.h"

#include <math.h>
#include <process.h>

#include "ModbusCanConvBroker.h"
#include "ModbusConvDlg.h"
#include "LinConvDlg.h"
#include "UartConvDlg.h"

#pragma comment(lib, "winmm")
#include <mmsystem.h>
// 210603 HKH 자동 업데이트 예약기능 //lyj 20210721 LGES 이식 s ============
#include "DlgUpdateReserve.h"
#include <TlHelp32.h>
// 210603 HKH 자동 업데이트 예약기능 //lyj 20210721 LGES 이식 e ============

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CFont g_Font;

#define ID_AUX_HIGH_LOW			1
#define ID_AUX_HIGH				2
#define ID_AUX_LOW				3

// #define ID_CAN_CELL_VTG_ALL		151
// #define ID_CAN_CELL_VTG_HIGH	152
// #define ID_CAN_CELL_VTG_LOW		153
// #define ID_CAN_TEMPE_ALL		154
// #define ID_CAN_TEMPE_HIGH		155
// #define ID_CAN_TEMPE_LOW		156
// #define ID_CAN_BMS_FLT			157
// #define ID_CAN_SOC_ALL			158
// #define ID_CAN_SOC_HIGH			159
// #define ID_CAN_SOC_LOW			160

// #define ID_CAN_USER_RUN			301
// #define ID_CAN_USER_STOP		302
// #define ID_CAN_USER_PAUSE		303
// #define ID_CAN_USER_CONTINUE	304
// #define ID_CAN_USER_NEXT		305
// #define ID_CAN_USER_GOTO		306
// #define ID_CAN_USER_STATE		307
/////////////////////////////////////////////////////////////////////////////
// CCTSMonProView

IMPLEMENT_DYNCREATE(CCTSMonProView, CFormView)

BEGIN_MESSAGE_MAP(CCTSMonProView, CFormView)
	//{{AFX_MSG_MAP(CCTSMonProView)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_TESTVIEW, OnBtnTestview)
	ON_COMMAND(ID_CHECK_COND, OnCheckCond)
	ON_COMMAND(ID_CONFIG, OnConfig)
	ON_COMMAND(ID_CALI_SETTING, OnCaliSetting)
	ON_UPDATE_COMMAND_UI(ID_CHECK_COND, OnUpdateCheckCond)
	ON_UPDATE_COMMAND_UI(ID_CALI_SETTING, OnUpdateCaliSetting)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CHANNEL, OnRclickListChannel)
	ON_WM_SIZE()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_MODULE, OnDblclkListModule)
	ON_COMMAND(ID_CLEAR_STATE, OnClearState)
	ON_UPDATE_COMMAND_UI(ID_CLEAR_STATE, OnUpdateClearState)
	ON_UPDATE_COMMAND_UI(ID_CMD_RUN, OnUpdateCmdRun)
	ON_UPDATE_COMMAND_UI(ID_CMD_PAUSE, OnUpdateCmdPause)
	ON_UPDATE_COMMAND_UI(ID_CMD_CONTINUE, OnUpdateCmdContinue)
	ON_UPDATE_COMMAND_UI(ID_CMD_STOP, OnUpdateCmdStop)
	ON_COMMAND(ID_CMD_RUN, OnCmdRun)
	ON_COMMAND(ID_CMD_PAUSE, OnCmdPause)
	ON_COMMAND(ID_CMD_CONTINUE, OnCmdContinue)
	ON_COMMAND(ID_CMD_STOP, OnCmdStop)
	ON_COMMAND(ID_MANAGE_PATTERN, OnManagePattern)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_LIST_CHANNEL, OnGetdispinfoListChannel)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_LIST_MODULE, OnGetdispinfoListModule)
	ON_NOTIFY(NM_RCLICK, IDC_GRID_CUSTOM, OnGridRButtonUp)
	ON_NOTIFY(NM_CLICK, IDC_GRID_CUSTOM, OnGridLButtonUp)
	ON_COMMAND(ID_GRID_CONFIG, OnGridConfig)
	ON_COMMAND(ID_CALIBRATION, OnCalibration)
	ON_COMMAND(ID_LOG_WND, OnLogWnd)
	ON_UPDATE_COMMAND_UI(ID_GRID_CONFIG, OnUpdateGridConfig)
	ON_COMMAND(ID_DATA_VIEW, OnDataView)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_MODULE, OnItemchangedListModule)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_SELECT_ALL, OnSelectAll)
	ON_UPDATE_COMMAND_UI(ID_SELECT_ALL, OnUpdateSelectAll)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_CHANNEL, OnItemchangedListChannel)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_MODULE, OnRclickListModule)
	ON_UPDATE_COMMAND_UI(ID_NEXT_STEP, OnUpdateNextStep)
	ON_COMMAND(ID_NEXT_STEP, OnCmdNextStep)
	ON_COMMAND(ID_USER_CONFIG, OnUserConfig)
	ON_COMMAND(ID_DATA_RESTORE, OnDataRestore)
	ON_UPDATE_COMMAND_UI(ID_DATA_RESTORE, OnUpdateDataRestore)
	ON_COMMAND(ID_GRAPH_VIEW, OnGraphView)
	ON_COMMAND(ID_EXE_DATA_VIEW, OnExeDataView)
	ON_COMMAND(ID_WORK_LOG_VIEW, OnWorkLogView)
	ON_COMMAND(ID_WORK_FOLDER_VIEW, OnWorkFolderView) //lyj 20210108
	ON_COMMAND(ID_CMD_STOP_CANCEL, OnCmdStopCancel)
	ON_UPDATE_COMMAND_UI(ID_CMD_STOP_CANCEL, OnUpdateCmdStopCancel)
	ON_COMMAND(ID_CMD_STOP_STEP, OnCmdStopStep)
	ON_COMMAND(ID_CMD_STOP_CYCLE, OnCmdStopCycle)
	ON_COMMAND(ID_CMD_STOP_ETC, OnCmdStopEtc)
	ON_COMMAND(ID_CMD_PAUSE_STEP, OnCmdPauseStep)
	ON_COMMAND(ID_CMD_PAUSE_CYCLE, OnCmdPauseCycle)
	ON_COMMAND(ID_CMD_PAUSE_ETC, OnCmdPauseEtc)
	ON_COMMAND(ID_CMD_PAUSE_CANCEL, OnCmdPauseCancel)
	ON_UPDATE_COMMAND_UI(ID_CMD_STOP_STEP, OnUpdateCmdStopStep)
	ON_UPDATE_COMMAND_UI(ID_CMD_STOP_CYCLE, OnUpdateCmdStopCycle)
	ON_UPDATE_COMMAND_UI(ID_CMD_STOP_ETC, OnUpdateCmdStopEtc)
	ON_UPDATE_COMMAND_UI(ID_CMD_PAUSE_STEP, OnUpdateCmdPauseStep)
	ON_UPDATE_COMMAND_UI(ID_CMD_PAUSE_CYCLE, OnUpdateCmdPauseCycle)
	ON_UPDATE_COMMAND_UI(ID_CMD_PAUSE_ETC, OnUpdateCmdPauseEtc)
	ON_UPDATE_COMMAND_UI(ID_CMD_PAUSE_CANCEL, OnUpdateCmdPauseCancel)
	ON_COMMAND(ID_SIMPLE_TEST, OnSimpleTest)
	ON_UPDATE_COMMAND_UI(ID_SIMPLE_TEST, OnUpdateSimpleTest)
	ON_UPDATE_COMMAND_UI(ID_PARALLEL_CONFIG, OnUpdateParallelConfig)
	ON_COMMAND(ID_PARALLEL_CONFIG, OnParallelConfig)
	ON_COMMAND(ID_AUX_DATA_CONFIG, OnAuxDataConfig)
	ON_UPDATE_COMMAND_UI(ID_AUX_DATA_CONFIG, OnUpdateAuxDataConfig)
	ON_BN_CLICKED(IDC_BTN_START, OnBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, OnBtnStop)
	ON_BN_CLICKED(IDC_BTN_NEXT_STEP, OnBtnNextStep)
	ON_BN_CLICKED(IDC_BTN_DATA_VIEW, OnBtnDataView)
	ON_BN_CLICKED(IDC_BTN_GRAPH_VIEW, OnBtnGraphView)
	ON_BN_CLICKED(IDC_BTN_CHECK_COND, OnBtnCheckCond)
	ON_COMMAND(ID_CAN_CONFIG, OnCanConfig)
	ON_COMMAND(ID_CAN_CONFIG_TRANSMIT, OnCanConfigTransmit)
	ON_COMMAND(ID_CAN_LINTOCAN_RX, OnCanConfigLintoCANRX)
	ON_COMMAND(ID_CAN_LINTOCAN_TX, OnCanConfigLintoCANTX)
	ON_UPDATE_COMMAND_UI(ID_CAN_CONFIG, OnUpdateCanConfig)
	ON_UPDATE_COMMAND_UI(ID_BCR_SET, OnUpdateBcrSet)
	ON_COMMAND(ID_OVEN_WND, OnOvenWnd)
	ON_UPDATE_COMMAND_UI(ID_OVEN_WND, OnUpdateOvenWnd)
	ON_UPDATE_COMMAND_UI(ID_CAN_CONFIG_TRANSMIT, OnUpdateCanConfigTransmit)
	ON_COMMAND(ID_TELNET_MODULE, OnTelnetModule)
	ON_COMMAND(ID_PING_MODULE, OnPingModule)
	ON_COMMAND(ID_CMD_RUN_UPDATE, OnCmdRunUpdate)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, OnSelchangeTab1)
	ON_BN_CLICKED(IDC_BUT_ANAL, OnButAnal)
	ON_BN_CLICKED(IDC_BUT_EDITOR, OnButEditor)
	ON_BN_CLICKED(IDC_BTN_VIEW_LOG, OnBtnViewLog)
	ON_BN_CLICKED(IDC_BUT_ALL_VIEW, OnButAllView)
	ON_BN_CLICKED(IDC_BTN_PAUSE, OnBtnPause)
	ON_COMMAND(ID_CMD_SET_CHAMBER_USING, OnCmdSetChamberUsing)
	ON_COMMAND(ID_CMD_RELEASE_CHAMBER_USING, OnCmdReleaseChamberUsing)
	ON_COMMAND(ID_CMD_CHAMBER_CONTINUE, OnCmdChamberContinue)
	ON_COMMAND(ID_CMD_CYCLE_CONTINUE, OnCmdCycleContinue)
	ON_BN_CLICKED(IDC_BUT_STOP_BUZZER, OnButStopBuzzer)
	ON_BN_CLICKED(IDC_BUT_RESET_ALARM, OnButResetAlarm)
	ON_BN_CLICKED(IDC_BUT_SET_COMM, OnButSetComm)
	ON_BN_CLICKED(IDC_BUT_JIG_TAG, OnButJigTag)
	ON_UPDATE_COMMAND_UI(ID_OVEN_WND_2, OnUpdateOvenWnd2)
	ON_COMMAND(ID_OVEN_WND_2, OnOvenWnd2)
	ON_COMMAND(ID_WORKST_WND, OnWorkSTShow)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_START, OnBtnDaqIsolationStart)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_END, OnBtnDaqIsolationEnd)
	ON_COMMAND(ID_PARALLEL_CON, OnParallelCon)
	ON_UPDATE_COMMAND_UI(ID_PARALLEL_CON, OnUpdateParallelCon)
	ON_COMMAND(ID_PARALLEL_DISCON, OnParallelDiscon)
	ON_UPDATE_COMMAND_UI(ID_PARALLEL_DISCON, OnUpdateParallelDiscon)
	ON_COMMAND(ID_CMD_ISOLATION_END, OnCmdIsolationEnd)
	ON_BN_CLICKED(IDC_BTN_VIEW_REALGRAPH, OnBtnViewRealgraph)
	ON_COMMAND(ID_WORK_PNE_MANAGE, OnWorkPneManage)
	ON_COMMAND(ID_UART_CONFIG, OnUartConfig)
	ON_COMMAND(ID_LIN_CONFIG, OnLinConfig)
	ON_COMMAND(ID_MODBUS_CONFIG, OnModbusConfig)
	ON_BN_CLICKED(IDC_BUT_RECONNECT_LOAD_1, OnButReconnectLoad1)
	ON_BN_CLICKED(IDC_BUT_RECONNECT_LOAD_2, OnButReconnectLoad2)
	ON_COMMAND(ID_CMD_CHAM1_SET, OnCmdCham1Set)
	ON_COMMAND(ID_CMD_CHAM2_SET, OnCmdCham2Set)
	ON_BN_CLICKED(IDC_BUT_MUX_CHOICE, OnButMuxChoice)
	ON_COMMAND(ID_CMD_CH_INIT, OnCmdChInit)
	ON_UPDATE_COMMAND_UI(ID_CMD_CH_INIT, OnUpdateCmdChInit)
	ON_COMMAND(ID_GOTO_STEP, OnGotoStep)
	ON_UPDATE_COMMAND_UI(ID_GOTO_STEP, OnUpdateGotoStep)
	ON_BN_CLICKED(IDC_BUT_LOAD_CHOICE, OnButLoadChoice)
	ON_COMMAND(ID_CMD_RUN_QUEUE, OnCmdRunQueue)
	ON_COMMAND(ID_RUN_QUEUE_VIEW, OnRunQueueView)
	ON_BN_CLICKED(IDC_BTN_RESERV, OnBtnReserv)
	ON_COMMAND(ID_SAFETY_COND_UPDATE, OnCmdSafetyCondUpade)
	ON_COMMAND(ID_SCH_STEP_UPDATE, OnCmdSchStepUpdate)
	ON_COMMAND(ID_SCH_ALL_STEP_UPDATE, OnCmdSchAllStepUpdate)
	ON_WM_COPYDATA()
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_END_A2, OnBtnDaqIsolationEndA2)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_END_B2, OnBtnDaqIsolationEndB2)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_START_AB2, OnBtnDaqIsolationStartAb2)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_END_A1, OnBtnDaqIsolationEndA1)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_END_B1, OnBtnDaqIsolationEndB1)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_START_AB1, OnBtnDaqIsolationStartAb1)
	ON_COMMAND(ID_CELLBAL_SET, OnCellbalSet)
	ON_COMMAND(ID_MENU_WND_PWR, OnMenuWndPwr)
	ON_UPDATE_COMMAND_UI(ID_MENU_WND_PWR, OnUpdateMenuWndPwr)
	ON_COMMAND(ID_MENU_WND_PWR2, OnMenuWndPwr2)
	ON_UPDATE_COMMAND_UI(ID_MENU_WND_PWR2, OnUpdateMenuWndPwr2)
	ON_COMMAND(ID_MENU_WND_CHILLER1, OnMenuWndChiller1)
	ON_UPDATE_COMMAND_UI(ID_MENU_WND_CHILLER1, OnUpdateMenuWndChiller1)
	ON_COMMAND(ID_MENU_WND_CHILLER2, OnMenuWndChiller2)
	ON_UPDATE_COMMAND_UI(ID_MENU_WND_CHILLER2, OnUpdateMenuWndChiller2)
	ON_COMMAND(ID_MENU_WND_CHILLER3, OnMenuWndChiller3)
	ON_UPDATE_COMMAND_UI(ID_MENU_WND_CHILLER3, OnUpdateMenuWndChiller3)
	ON_COMMAND(ID_MENU_WND_CHILLER4, OnMenuWndChiller4)
	ON_UPDATE_COMMAND_UI(ID_MENU_WND_CHILLER4, OnUpdateMenuWndChiller4)
	ON_COMMAND(ID_MENUBAR_THTABLE_SET, OnMenubarThtableSet)
	ON_BN_CLICKED(IDC_BTN_EMG, OnBtnEmg)
	ON_UPDATE_COMMAND_UI(ID_CAN_LINTOCAN_RX, OnUpdateCanLintocanRx)
	ON_UPDATE_COMMAND_UI(ID_CAN_LINTOCAN_TX, OnUpdateCanLintocanTx)
	ON_BN_CLICKED(IDC_BTN_BuzzerStop, OnBTNBuzzerStop)

	ON_COMMAND(ID_CAN_RX485_RX, OnCanConfigRS485RX)
	ON_COMMAND(ID_CAN_RX485_TX, OnCanConfigRS485TX)
	ON_UPDATE_COMMAND_UI(ID_CAN_RX485_RX, OnUpdateRS485Rx)
	ON_UPDATE_COMMAND_UI(ID_CAN_RX485_TX, OnUpdateRS485Tx)

	ON_COMMAND(ID_ISOLATION_REQUSET_ENABLE, OnCmdIsolationEnable)
	ON_UPDATE_COMMAND_UI(ID_ISOLATION_REQUSET_ENABLE, OnUpdateIsolationEnable)
	ON_COMMAND(ID_ISOLATION_REQUSET_DISABLE, OnCmdIsolationDisable)
	ON_UPDATE_COMMAND_UI(ID_ISOLATION_REQUSET_DISABLE, OnUpdateIsolationDisable)


	ON_COMMAND(ID_CAN_SMBUS_RX, OnCanConfigSMBusRX)
	ON_COMMAND(ID_CAN_SMBUS_TX, OnCanConfigSMBusTX)
	ON_UPDATE_COMMAND_UI(ID_CAN_SMBUS_RX, OnUpdateSMBusRx)
	ON_UPDATE_COMMAND_UI(ID_CAN_SMBUS_TX, OnUpdateSMBusTx)

	ON_WM_CTLCOLOR()
	ON_WM_CLOSE()
	ON_WM_SYSCOMMAND()
	//}}AFX_MSG_MAP
	// Standard printing commands
	//ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	//ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	//ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
	ON_MESSAGE(SFTWM_TEST_RESULT, OnChannelDataReceived)
	ON_MESSAGE(SFTWM_MODULE_CONNECTED, OnModuleConnected)
	ON_MESSAGE(SFTWM_MODULE_CLOSED, OnModuleDisconnected)
	ON_MESSAGE(SFTWM_MODULE_CONNECTING, OnModuleConnecting) //ksj 20201209 : 모듈 접속 진행 상태 핸들러 추가.
	ON_MESSAGE(SFTWM_CALI_RESULT_DATA, OnSetCalResultData)
	ON_MESSAGE(SFTWM_MODULE_EMG, OnModuleEmergency)
	ON_MESSAGE(SFTWM_MSEC_CH_DATA, OnMSecChannelDataReceived)
	ON_MESSAGE(SFTWM_BMS_COMMAND, OnModuleBmsCommandReceived)
	ON_MESSAGE(SFTWM_BMS_RESULT_COMMAND, OnModuleBmsResultDataReceived)
	ON_MESSAGE(SFTWM_MUX_CHANGE_RESULT, OnModuleMuxCommandReceived)
	ON_MESSAGE(SFTWM_RT_TABLE_REC, OnRTTableCommandReceived)
	ON_MESSAGE(SFTWM_RT_HUMI_TABLE_REC, OnRTHumiTableCommandReceived) //ksj 20200207
	ON_MESSAGE(SFT_CMD_FTP_END_OK_RESPONSE, OnCmdReadyRun)		//ljb 20130514
	ON_MESSAGE(SFT_CMD_FTP_END_NG_RESPONSE, OnCmdReadyNgRun)		//ljb 20130621
	ON_MESSAGE(SFTWM_TEST_COMPLETE, OnChannelTestComplete)	//ksj 20160728
	ON_MESSAGE(SFTWM_TEST_PAUSE, OnChannelTestPause)	//ksj 20200601
	ON_MESSAGE(SFTWM_CH_ATTRIBUTE_RECEIVE, OnChannelAttributeReceived)	//ksj 20200122 : 병렬 정보 등 SBC로부터 CH ATTRIBUTE 정보 수신
	ON_MESSAGE(SFTWM_STEP_INFO_DATA, OnSetStepInfoData)
	ON_MESSAGE(SFTWM_SAFETY_INFO_DATA, OnSetSafetyInfoData)
	ON_NOTIFY(GVN_SELCHANGED, IDC_GRID_CUSTOM,OnGridSelectChanged)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CHANNEL, OnNMDblclkListChannel)
	ON_BN_CLICKED(IDC_BUT_CBANK_ON, &CCTSMonProView::OnBnClickedButCbankOn)
	ON_BN_CLICKED(IDC_BUT_CBANK_OFF, &CCTSMonProView::OnBnClickedButCbankOff)
	ON_COMMAND(ID_CMD_CHILL1_TEST_SET, &CCTSMonProView::OnCmdChill1TestSet)
	ON_COMMAND(ID_CMD_CHILL2_TEST_SET, &CCTSMonProView::OnCmdChill2TestSet)
	ON_COMMAND(ID_CMD_PUMP1_TEST_SET, &CCTSMonProView::OnCmdPump1TestSet)
	ON_COMMAND(ID_CMD_PUMP2_TEST_SET, &CCTSMonProView::OnCmdPump2TestSet)
	ON_COMMAND(ID_AUX_VENT_SAFETY_CONFIG, &CCTSMonProView::OnAuxVentSafetyConfig)
	ON_COMMAND(ID_FLICKER_CONFIG, &CCTSMonProView::OnFlickerConfig) //lyj 20200824
	ON_BN_CLICKED(IDC_BTN_VENT_CLEAR, &CCTSMonProView::OnBnClickedBtnVentClear)
	ON_BN_CLICKED(IDC_BTN_VENT_OPEN, &CCTSMonProView::OnBnClickedBtnVentOpen)
	ON_BN_CLICKED(IDC_BUTTON_TESTBTN, &CCTSMonProView::OnBnClickedButtonTestbtn)
	ON_UPDATE_COMMAND_UI(ID_AUX_VENT_SAFETY_CONFIG, &CCTSMonProView::OnUpdateAuxVentSafetyConfig)
	ON_UPDATE_COMMAND_UI(ID_LIN_CONFIG, &CCTSMonProView::OnUpdateLinConfig)
	ON_UPDATE_COMMAND_UI(ID_MODBUS_CONFIG, &CCTSMonProView::OnUpdateModbusConfig)
	ON_UPDATE_COMMAND_UI(ID_UART_CONFIG, &CCTSMonProView::OnUpdateUartConfig)
	ON_UPDATE_COMMAND_UI(ID_CMD_CHAM1_SET, &CCTSMonProView::OnUpdateCmdCham1Set)
	ON_UPDATE_COMMAND_UI(ID_CMD_CHAM2_SET, &CCTSMonProView::OnUpdateCmdCham2Set)
	ON_UPDATE_COMMAND_UI(ID_CMD_CHILL1_TEST_SET, &CCTSMonProView::OnUpdateCmdChill1TestSet)
	ON_UPDATE_COMMAND_UI(ID_CMD_CHILL2_TEST_SET, &CCTSMonProView::OnUpdateCmdChill2TestSet)
	ON_UPDATE_COMMAND_UI(ID_CMD_PUMP1_TEST_SET, &CCTSMonProView::OnUpdateCmdPump1TestSet)
	ON_UPDATE_COMMAND_UI(ID_CMD_PUMP2_TEST_SET, &CCTSMonProView::OnUpdateCmdPump2TestSet)
	ON_UPDATE_COMMAND_UI(ID_CELLBAL_SET, &CCTSMonProView::OnUpdateCellbalSet)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_START2, &CCTSMonProView::OnBnClickedBtnDaqIsolationStart2)
	ON_BN_CLICKED(IDC_BTN_DAQ_ISOLATION_END2, &CCTSMonProView::OnBnClickedBtnDaqIsolationEnd2)
	ON_COMMAND(ID_CONN_SEQ_LOG_VIEW, &CCTSMonProView::OnConnSeqLogView)
	ON_UPDATE_COMMAND_UI(ID_CONN_SEQ_LOG_VIEW, &CCTSMonProView::OnUpdateConnSeqLogView)
	// 210603 HKH 자동 업데이트 예약기능 //lyj 20210721 LGES 이식 s =========== //lyj 202210809타이머 사용으로 ㅈ 변경
	//ON_COMMAND(ID_AUTO_UPDATE_RESERVE, OnAutoUpdaetReserve) 
	//ON_UPDATE_COMMAND_UI(ID_AUTO_UPDATE_RESERVE, OnUpdateAutoUpdaetReserve)
	// 210603 HKH 자동 업데이트 예약기능 //lyj 20210721 LGES 이식 e ===========
	ON_BN_CLICKED(IDC_BUT_ISOL_CHOICE, &CCTSMonProView::OnButIsolChoice)
	END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProView construction/destruction

// CCTSMonProView::CCTSMonProView()
// 	: CFormView(CCTSMonProView::IDD)
// {
	CCTSMonProView::CCTSMonProView()
	:CFormView(	
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCTSMonProView::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCTSMonProView::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CCTSMonProView::IDD3):
	(CCTSMonProView::IDD)
	)
	, m_pParallelConfigDlg(NULL)
	, m_LabAuxTherHumiMax(_T(""))
	, m_LabAuxTherHumiMin(_T(""))
	, m_bUseVentSafety(FALSE)
	, m_bUseAuxH(FALSE)	
	, m_bThreadTest(FALSE)
	, m_nMaxAuxFloatingPoint(0)
	, m_nMaxCanFloatingPoint(0)
	{	//{{AFX_DATA_INIT(CCTSMonProView)
	m_ctrlLoaderState_1 = _T("");
	m_ctrlLoaderState_2 = _T("");
	m_ctrlLal_CellDeltaVolt = _T("");
	m_LabAuxTempMax = _T("");
	m_LabAuxTempMin = _T("");
	m_LabAuxVoltMax = _T("");
	m_LabAuxVoltMin = _T("");
	m_LabAuxTherMax = _T("");
	m_LabAuxTherMin = _T("");
	//}}AFX_DATA_INIT
	// TODO: add construction code here

	bShowEmgDlg = FALSE; //2014.09.26 EMG알람창 확인용.


	m_nCurrentModuleID = 1;			// FIX
	m_pItem = NULL;
	m_pChLineSts = NULL;			//Module state Image List
	m_pChStsSmallImage = NULL;		//Channel state Image List

	m_nCurTabIndex = 1;
	m_nDisplayCol = 8;
	m_bUpdateLock = FALSE;
	m_nChannelUpdateInterval = 2000;
	m_nModuleUpdateInterval = 2000;

	m_lChMuxState = 0;
	m_lChMuxBackupInfo = 0;
	
	m_bRestoring = FALSE;

	m_PauseSubMenu.CreatePopupMenu();
	m_PauseSubMenu.SetMenuHeight(20);
	m_PauseSubMenu.SetMenuWidth(170);
	m_PauseSubMenu.SetMenuType(MIT_XP);
	m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE, Fun_FindMsg("CTSMonProView_msg1","IDD_CTSMonPro_FORM"), IDB_PAUSE);
	//@ m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE, "즉시 멈춤 시행(&I)", IDB_PAUSE);
	m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_STEP, Fun_FindMsg("CTSMonProView_msg2","IDD_CTSMonPro_FORM"));
	//@ m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_STEP,"현재 Step 완료후(&E)");
	m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_CYCLE, Fun_FindMsg("CTSMonProView_msg3","IDD_CTSMonPro_FORM"));
	//@ m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_CYCLE,"현재 Cycle 반복 완료후(&Y)");
	m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_ETC, Fun_FindMsg("CTSMonProView_msg4","IDD_CTSMonPro_FORM"));
	//@ m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_ETC,"기타...(&E)");
	m_PauseSubMenu.AppendMenu(MF_SEPARATOR,0,"");
	m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_CANCEL, Fun_FindMsg("CTSMonProView_msg5","IDD_CTSMonPro_FORM"), IDB_UNDO);
	//@ m_PauseSubMenu.AppendMenu(MF_ENABLED,ID_CMD_PAUSE_CANCEL,"취소(&C)", IDB_UNDO);

	m_StopSubMenu.CreatePopupMenu();
	m_StopSubMenu.SetMenuHeight(20);
	m_StopSubMenu.SetMenuWidth(170);
	m_StopSubMenu.SetMenuType(MIT_XP);
	m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP, Fun_FindMsg("CTSMonProView_msg6","IDD_CTSMonPro_FORM"),IDB_STOP);
	//@ m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP,"즉시 완료 시행(&I)",IDB_STOP);
	m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_STEP, Fun_FindMsg("CTSMonProView_msg7","IDD_CTSMonPro_FORM"));
	//@ m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_STEP,"현재 Step 완료후(&E)");
	m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_CYCLE, Fun_FindMsg("CTSMonProView_msg8","IDD_CTSMonPro_FORM"));
	//@ m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_CYCLE,"현재 Cycle 반복 완료후(&Y)");
	m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_ETC, Fun_FindMsg("CTSMonProView_msg9","IDD_CTSMonPro_FORM"));
	//@ m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_ETC,"기타...(&E)");
	m_StopSubMenu.AppendMenu(MF_SEPARATOR,0,"");
	m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_CANCEL, Fun_FindMsg("CTSMonProView_msg10","IDD_CTSMonPro_FORM"), IDB_UNDO);
	//@ m_StopSubMenu.AppendMenu(MF_ENABLED,ID_CMD_STOP_CANCEL,"취소(&C)", IDB_UNDO);


	m_ControlMenu.CreatePopupMenu();
	m_ControlMenu.SetMenuHeight(20);
	//m_ControlMenu.SetMenuWidth(170);
	m_ControlMenu.SetMenuWidth(230); //20190630
	m_ControlMenu.SetMenuType(MIT_XP);
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_RUN,Fun_FindMsg("CTSMonProView_msg11","IDD_CTSMonPro_FORM"), IDB_START);
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_RUN,"작업시작(&R)", IDB_START);
	m_ControlMenu.AppendMenu(MF_POPUP, (UINT)m_PauseSubMenu.m_hMenu, Fun_FindMsg("CTSMonProView_msg12","IDD_CTSMonPro_FORM"), IDB_PAUSE);

	m_bUseWorkReserv = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE_VAL);
	if(m_bUseWorkReserv) //ksj 20160613 작업 예약 기능 레지스트리 옵션 처리.
	{
		//m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_RUN_QUEUE,"작업예약(&E)"); //ksj 20160426
	//	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_RUN_QUEUE,"Operation Scheduling(&E)"); //ksj 20161210
		m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_RUN_QUEUE,Fun_FindMsg("CTSMonProView_msg29","IDD_CTSMonPro_FORM")); //ksj 20161210 //언어파일 작업필요
	}
	
	//@ m_ControlMenu.AppendMenu(MF_POPUP, (UINT)m_PauseSubMenu.m_hMenu, "작업멈춤(&P)", IDB_PAUSE);
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_CONTINUE, Fun_FindMsg("CTSMonProView_msg13","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_CONTINUE,"작업계속(&C)");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_NEXT_STEP, Fun_FindMsg("CTSMonProView_msg14","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_NEXT_STEP,"다음 Step(&N)");

	m_ControlMenu.AppendMenu(MF_ENABLED,ID_GOTO_STEP, Fun_FindMsg("CTSMonProView_msg33","IDD_CTSMonPro_FORM"));
	//@m_ControlMenu.AppendMenu(MF_ENABLED,ID_GOTO_STEP,"이동 Step(&G)");		//ljb 20151212 add

	m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");
	m_ControlMenu.AppendMenu(MF_POPUP, (UINT)m_StopSubMenu.m_hMenu, Fun_FindMsg("CTSMonProView_msg15","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_POPUP, (UINT)m_StopSubMenu.m_hMenu, "작업 완료(&T)");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_SIMPLE_TEST,Fun_FindMsg("CTSMonProView_msg16","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_SIMPLE_TEST,"간이 충방전...(&M)");
	m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_DATA_VIEW, Fun_FindMsg("CTSMonProView_msg17","IDD_CTSMonPro_FORM"), IDB_TABLE_DATA);
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_DATA_VIEW,"결과 Data 확인...(&V)", IDB_TABLE_DATA);
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_GRAPH_VIEW, Fun_FindMsg("CTSMonProView_msg18","IDD_CTSMonPro_FORM"), IDB_GRAPH);
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_GRAPH_VIEW,"그래프 그리기...(&G)", IDB_GRAPH);
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CHECK_COND, Fun_FindMsg("CTSMonProView_msg19","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_CHECK_COND,"진행 스케쥴 확인...(&S)");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_WORK_LOG_VIEW, Fun_FindMsg("CTSMonProView_msg20","IDD_CTSMonPro_FORM"), IDB_LOG);
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_WORK_LOG_VIEW,"작업 로그보기(&L)", IDB_LOG);
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_WORK_FOLDER_VIEW, Fun_FindMsg("CTSMonProView_msg48","IDD_CTSMonPro_FORM"), IDB_LOG); //lyj 20210108
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_WORK_FOLDER_VIEW,"작업 폴더 바로가기(&F)", IDB_LOG);
	m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_EDIT_COPY, Fun_FindMsg("CTSMonProView_msg21","IDD_CTSMonPro_FORM"), IDB_COPY);
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_EDIT_COPY,"복사(&C)\tCtrl+C", IDB_COPY);
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_SELECT_ALL, Fun_FindMsg("CTSMonProView_msg22","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_SELECT_ALL,"전체 선택(&A)\tCtrl+A");
	//★★ ljb 201012 S	★★★★★★★★
	m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_SET_CHAMBER_USING, Fun_FindMsg("CTSMonProView_msg23","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_SET_CHAMBER_USING,"챔버연동 설정(&T)");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_RELEASE_CHAMBER_USING, Fun_FindMsg("CTSMonProView_msg24","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_RELEASE_CHAMBER_USING,"챔버연동 해제(&C)");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_CHAMBER_CONTINUE, Fun_FindMsg("CTSMonProView_msg25","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_CHAMBER_CONTINUE,"챔버연동 다음 Step(&Y)");
	//★★ ljb 201012 S	★★★★★★★★
	m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_CYCLE_CONTINUE, Fun_FindMsg("CTSMonProView_msg26","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_CMD_CYCLE_CONTINUE,"Cycle 완료 후 Continue");
	//★★ ljb 201105 S	★★★★★★★★
	//m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");
 	m_ControlMenu.AppendMenu(MF_ENABLED,ID_REAL_TIME_GRAPH_VIEW, Fun_FindMsg("CTSMonProView_msg27","IDD_CTSMonPro_FORM"));
	//@ m_ControlMenu.AppendMenu(MF_ENABLED,ID_REAL_TIME_GRAPH_VIEW,"실시간 그래프 그리기");
	//lmh 20120113
	//m_ControlMenu.AppendMenu(MF_SEPARATOR,0,""); //ksj 20200214
	//m_ControlMenu.AppendMenu(MF_ENABLED,ID_PARALLEL_CON, Fun_FindMsg("CTSMonProView_msg28","IDD_CTSMonPro_FORM")); //ksj 20200214 : 주석처리. 기능 고장나있음. 메뉴의 병렬 설정 기능과 기능이 이원화되어있어 관리 어려움. 제거함
	//m_ControlMenu.AppendMenu(MF_ENABLED,ID_PARALLEL_DISCON, Fun_FindMsg("CTSMonProView_msg34","IDD_CTSMonPro_FORM")); //ksj 20200214 : 주석처리. 기능 고장나있음. 메뉴의 병렬 설정 기능과 기능이 이원화되어있어 관리 어려움. 제거함

	m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");//$1013

	BOOL bUseSafetyConditionUpdate = FALSE;	//lyj 20200217
	bUseSafetyConditionUpdate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSafetyConditionUpdate", 0);
	if(bUseSafetyConditionUpdate== TRUE)
	{
		//m_ControlMenu.AppendMenu(MF_ENABLED,ID_SAFETY_COND_UPDATE,"시험 안전조건 변경");	//ljb 20170728 add
		m_ControlMenu.AppendMenu(MF_ENABLED,ID_SAFETY_COND_UPDATE,Fun_FindMsg("CTSMonProView_msg30","IDD_CTSMonPro_FORM"));	//ljb 20170728 add //&&
	}
	
	BOOL bUseSchStepUpdate = FALSE;	//lyj 20200217
	bUseSchStepUpdate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSchStepUpdate", 0);
	if(bUseSchStepUpdate == TRUE)
	{
		//m_ControlMenu.AppendMenu(MF_ENABLED,ID_SCH_STEP_UPDATE,"스케쥴 STEP 변경");			//ljb 20170728 add
		m_ControlMenu.AppendMenu(MF_ENABLED,ID_SCH_STEP_UPDATE,Fun_FindMsg("CTSMonProView_msg31","IDD_CTSMonPro_FORM"));			//ljb 20170728 add //&&
	}

	m_ControlMenu.AppendMenu(MF_SEPARATOR,0,"");//$1013
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_ISOLATION_REQUSET_ENABLE,Fun_FindMsg("CTSMonProView_msg35","IDD_CTSMonPro_FORM"));			//ljb 20170728 add //&&
	m_ControlMenu.AppendMenu(MF_ENABLED,ID_ISOLATION_REQUSET_DISABLE,Fun_FindMsg("CTSMonProView_msg36","IDD_CTSMonPro_FORM"));			//ljb 20170728 add //&&

	BOOL bUseSchAllStepUpdate = FALSE;	//yulee 20181216
	bUseSchAllStepUpdate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSchAllStepUpdate", 0);
	if(bUseSchAllStepUpdate == TRUE)
		//m_ControlMenu.AppendMenu(MF_ENABLED,ID_SCH_ALL_STEP_UPDATE,"스케쥴 전체 변경");		//ljb 20170728 add
		m_ControlMenu.AppendMenu(MF_ENABLED,ID_SCH_ALL_STEP_UPDATE, Fun_FindMsg("CTSMonProView_msg32","IDD_CTSMonPro_FORM"));		//&&

	m_wndMeterDlg = NULL;
	pCurrentChannel = NULL;
	m_nCurrentChannelNo = -1;
	m_nPreChannelNo = -2; //yulee 20181009
	m_pRestoreDlg = NULL;

	m_pDetailOvenDlg1=NULL;
	m_pDetailOvenDlg2=NULL;

	m_pWorkStationSignDlg=NULL; //yulee 20181009

	ZeroMemory(&arrayChInfo, PS_MAX_ITEM_NUM);	// int[32] , 32

	//m_pBms_Result_Cpu_Temp = new SFT_BMS_CPU_ID_TEMP;
 	//ZeroMemory(m_pBms_Result_Cpu_Temp,sizeof(SFT_BMS_CPU_ID_TEMP));

	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	//ljb
	m_PlayState=PS1_NONE;

	m_onPauseLog1 = TRUE; //yulee 20181008-1 로그 보강
	m_onPauseLog2 = TRUE; //yulee 20181008-1 로그 보강
	m_onPauseLog3 = TRUE; //yulee 20181008-1 로그 보강
	m_onPauseLog4 = TRUE; //yulee 20181008-1 로그 보강

	m_AlramSoundEnable = FALSE;

	//cny 201809
	m_pDlgCellBal=NULL;
	//yulee 20181114
	m_pRTtableSet=NULL;
	
	m_pPwrSupplyStat1=NULL;
	m_pPwrSupplyStat2=NULL;
	
	m_pDetailChillerDlg1=NULL;
	m_pDetailChillerDlg2=NULL;
	m_pDetailChillerDlg3=NULL;
	m_pDetailChillerDlg4=NULL;

	m_FlagBackgroundColor = 1; //yulee 20190604
	m_backBrush.CreateSolidBrush(ID_COLOR_RED); //yulee 20190604
// 	m_FPreFickerInfo.nCode = 0; //lyj 20201007 Flicker 초기화
// 	m_FPreFickerInfo.nPriority = 3; //lyj 20201007 Flicker 초기화
// 	m_FPreFickerInfo.nAutoBuzzerStop = 0; //ksj 20201013 : 초기화 추가
	InitPreFlickerInfo(); //ksj 20201013 : 재호출 가능한 함수로 분리

	m_bUseAuxH = TRUE; //ksj 20200215 : Aux Humidity 사용 여부
	m_bUseCanSettingDlg = TRUE; //ksj 20200215 : Can 설정 노출 여부
	m_bUseLinCanSettingDlg = TRUE; //ksj 20200215 : LintoCan 설정 노출 여부
	m_bUseRS485SettingDlg = TRUE; //ksj 20200215 : RS485 설정 노출 여부
	m_bUseSMBusSettingDlg = TRUE; //ksj 20200215 : SMBus 설정 노출 여부
	m_bUseLinSettingDlg = TRUE; //ksj 20200215 : Lin 설정 노출 여부
	m_bUseModbusSettingDlg = TRUE; //ksj 20200215 : Modbus 설정 노출 여부
	m_bUseUartSettingDlg = TRUE; //ksj 20200215 : UART 설정 노출 여부	
//	m_bUseCanMux = FALSE; //ksj 20201208  //ksj 20201223 : 기능 취소됨

	m_pJsonMon = NULL; //ksj 20200902 : [#JSonMon] JSON 모니터링 파일 생성 클래스 포인터 초기화.

	m_bThreadSetFlickerColor = FALSE; //ksj 20201013
	m_pThreadSetFlickerColor = NULL; //ksj 20201013

	m_bUpdateRunning = FALSE;
	ZeroMemory(m_pDlgConnSeq,sizeof(m_pDlgConnSeq)); //ksj 20201209
}

CCTSMonProView::~CCTSMonProView()
{
	m_PlayState=PS1_END;

	if( m_pItem )
	{	
		CItemOnDisplay::DeleteAllItemOnDisplay(m_pItem);
	}

	if( m_pChStsSmallImage )
		delete m_pChStsSmallImage;

	if( m_pChLineSts )
		delete m_pChLineSts;

	if(m_wndMeterDlg)
	{
		m_wndMeterDlg->DestroyWindow();
		delete m_wndMeterDlg;
		m_wndMeterDlg = NULL;
	}

	if(m_pDetailOvenDlg1)
	{
		m_pDetailOvenDlg1->DestroyWindow();
		delete m_pDetailOvenDlg1;
		m_pDetailOvenDlg1 = NULL;
	}
	if(m_pDetailOvenDlg2)
	{
		m_pDetailOvenDlg2->DestroyWindow();
		delete m_pDetailOvenDlg2;
		m_pDetailOvenDlg2 = NULL;
	}
	if(m_pWorkStationSignDlg)
	{
		m_pWorkStationSignDlg->DestroyWindow();
		delete m_pWorkStationSignDlg;
		m_pWorkStationSignDlg = NULL;
	}

	//cny 201809
	if(m_pPwrSupplyStat1)
	{
		m_pPwrSupplyStat1->DestroyWindow();
		delete m_pPwrSupplyStat1;
		m_pPwrSupplyStat1 = NULL;
	}
	if(m_pPwrSupplyStat2)
	{
		m_pPwrSupplyStat2->DestroyWindow();
		delete m_pPwrSupplyStat2;
		m_pPwrSupplyStat2 = NULL;
	}
	
	if(m_pDetailChillerDlg1)
	{
		m_pDetailChillerDlg1->DestroyWindow();
		delete m_pDetailChillerDlg1;
		m_pDetailChillerDlg1 = NULL;
	}
	if(m_pDetailChillerDlg2)
	{
		m_pDetailChillerDlg2->DestroyWindow();
		delete m_pDetailChillerDlg2;
		m_pDetailChillerDlg2 = NULL;
	}
	if(m_pDetailChillerDlg3)
	{
		m_pDetailChillerDlg3->DestroyWindow();
		delete m_pDetailChillerDlg3;
		m_pDetailChillerDlg3 = NULL;
	}
	if(m_pDetailChillerDlg4)
	{
		m_pDetailChillerDlg4->DestroyWindow();
		delete m_pDetailChillerDlg4;
		m_pDetailChillerDlg4 = NULL;
	}

	//ksj 20200122
	if(m_pParallelConfigDlg)
	{
		delete m_pParallelConfigDlg;
		m_pParallelConfigDlg = NULL;
	}

	// 200313 HKH Memory Leak 수정
	if (m_pDlgCellBal != NULL)
	{
		m_pDlgCellBal->DestroyWindow();
		delete m_pDlgCellBal;
		m_pDlgCellBal = NULL;
	}

	if (m_pReserveDlg != NULL)
	{
		m_pReserveDlg->DestroyWindow();
		delete m_pReserveDlg;
		m_pReserveDlg = NULL;
	}

// 	ksj 20200902 : 모니터링 JSON 파일 생성 클래스 종료 -> //ksj 20201023 : 주석처리. CTSMonProDoc에서 서버객체를 먼저 종료시켜버리므로, 크래시 날 수 있다. 서버 Close 전에 먼저 Json을 제거해야되므로, Doc에서 제거 하도록 변경
// 		if(m_pJsonMon != NULL)
// 		{
// 			delete m_pJsonMon;
// 			m_pJsonMon = NULL;
// 		}

	//ksj 20201013 : 깜빡임 색상 선택 쓰레드 종료
	m_bThreadSetFlickerColor = FALSE; 
	if(m_pThreadSetFlickerColor != NULL)
	{
		DWORD retval = WaitForSingleObject(m_pThreadSetFlickerColor->m_hThread, 2500); //종료 대기, 쓰레드 내부 루프에 sleep 2초가 있으므로 최악의 경우 2초대기
		m_pThreadSetFlickerColor = NULL;
		switch (retval)
		{
		case WAIT_OBJECT_0:
			TRACE("m_pThreadSetFlickerColor has closed");
			break;
		case WAIT_TIMEOUT:
			TRACE("m_pThreadSetFlickerColor has Timeout");
			break;
		case WAIT_FAILED:    // 오류 발생
			TRACE("m_pThreadSetFlickerColor has Error");
			break;
		}
	}

	//ksj 20201211 : 접속 상태표시창 해제
	for(int nI=0;nI<SFT_MAX_MODULE_NUM;nI++)
	{
		if(m_pDlgConnSeq[nI])
		{
			delete m_pDlgConnSeq[nI];
			m_pDlgConnSeq[nI] = NULL;
		}
	}
}

void CCTSMonProView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCTSMonProView)
	DDX_Control(pDX, IDC_BTN_VENT_CLEAR, m_ctrlVentClear);
	DDX_Control(pDX, IDC_BTN_BuzzerStop, m_ctrlButBuzzerStop);
	DDX_Control(pDX, IDC_BTN_EMG, m_ctrlEMG);
	DDX_Control(pDX, IDC_BTN_RESERV, m_ctrlReserv);
	DDX_Control(pDX, IDC_BTN_VIEW_REALGRAPH, m_ctrlViewRealGraph);
	DDX_Control(pDX, IDC_LABEL_BMS_CHINFO, m_ctrlLabelBmsChInfo);
	DDX_Control(pDX, IDC_BUT_ALL_VIEW, m_ctrlAllView);
	DDX_Control(pDX, IDC_BTN_VIEW_LOG, m_ctrlViewLog);
	DDX_Control(pDX, IDC_BUT_EDITOR, m_ctrlEditor);
	DDX_Control(pDX, IDC_BUT_ANAL, m_ctrlAnal);
	DDX_Control(pDX, IDC_TAB1, m_tabCtrl);
	DDX_Control(pDX, IDC_LABEL_WH_UNIT1, m_ctrlLabelWhUnit2);
	DDX_Control(pDX, IDC_LABEL_WH_UNIT, m_ctrlLabelWhUnit);
	DDX_Control(pDX, IDC_LABEL_W_UNIT1, m_ctrlLabelWUnit2);
	DDX_Control(pDX, IDC_LABEL_W_UNIT, m_ctrlLabelWUnit);
	DDX_Control(pDX, IDC_STATIC_WATTHOUR2, m_ctrlStaticWattHour2);
	DDX_Control(pDX, IDC_STATIC_WATTHOUR, m_ctrlStaticWattHour);
	DDX_Control(pDX, IDC_STATIC_WATT2, m_ctrlStaticWatt2);
	DDX_Control(pDX, IDC_STATIC_WATT, m_ctrlStaticWatt);
	DDX_Control(pDX, IDC_LABEL_WATTHOUR2, m_ctrlLabelWattHour2);
	DDX_Control(pDX, IDC_LABEL_WATTHOUR, m_ctrlLabelWattHour);
	DDX_Control(pDX, IDC_LABEL_WATT2, m_ctrlLabelWatt2);
	DDX_Control(pDX, IDC_LABEL_WATT, m_ctrlLabelWatt);
	DDX_Control(pDX, IDC_LIST_CAN_INFO, m_wndCanList);
	DDX_Control(pDX, IDC_BTN_DATA_VIEW, m_ctrlBtnDataView);
	DDX_Control(pDX, IDC_BTN_CHECK_COND, m_ctrlBtnCheckCond);
	DDX_Control(pDX, IDC_BTN_GRAPH_VIEW, m_ctrlGraphView);
	DDX_Control(pDX, IDC_LABEL_CH_STATE, m_ctrlLabelChState);
	DDX_Control(pDX, IDC_LABEL_CHINFO, m_ctrlLabelChInfo);
	DDX_Control(pDX, IDC_LABEL_TOTALTIME, m_ctrlLabelTotalTime);
	DDX_Control(pDX, IDC_LABEL_STEPTIME, m_ctrlLabelStepTime);
	DDX_Control(pDX, IDC_STATIC_TOTALTIME, m_ctrlStaticTotalTime);
	DDX_Control(pDX, IDC_STATIC_STEPTIME, m_ctrlStaticStepTime);
	DDX_Control(pDX, IDC_BTN_PAUSE, m_ctrlPause);
	DDX_Control(pDX, IDC_BTN_STOP, m_ctrlStop);
	DDX_Control(pDX, IDC_BTN_START, m_ctrlStart);
	DDX_Control(pDX, IDC_BTN_NEXT_STEP, m_ctrlNextStep);
	DDX_Control(pDX, IDC_LABEL_CHANNEL, m_ctrlLabelCh);
	DDX_Control(pDX, IDC_LABEL_IMPEDANCE, m_ctrlLabelImpedance);
	DDX_Control(pDX, IDC_LABEL_VOLTAGE, m_ctrlLabelVoltage);
	DDX_Control(pDX, IDC_LABEL_CURRENT, m_ctrlLabelCurrent);
	DDX_Control(pDX, IDC_LABEL_CAPASITOR, m_ctrlLabelCapasity);
	DDX_Control(pDX, IDC_STATIC_VOLTAGE, m_ctrlStaticVoltage);
	DDX_Control(pDX, IDC_STATIC_CURRENT, m_ctrlStaticCurrent);
	DDX_Control(pDX, IDC_STATIC_CAPASITOR, m_ctrlStaticCapasity);
	DDX_Control(pDX, IDC_LABEL_V_UNIT, m_ctrlLabelVUnit);
	DDX_Control(pDX, IDC_LABEL_A_UNIT, m_ctrlLabelAUnit);
	DDX_Control(pDX, IDC_LABEL_C_UNIT, m_ctrlLabelCUnit);
	DDX_Control(pDX, IDC_LABEL_VOLTAGE2, m_ctrlLabelVoltage2);
	DDX_Control(pDX, IDC_LABEL_CURRENT2, m_ctrlLabelCurrent2);
	DDX_Control(pDX, IDC_LABEL_CAPASITOR2, m_ctrlLabelCapasity2);
	DDX_Control(pDX, IDC_STATIC_VOLTAGE2, m_ctrlStaticVoltage2);
	DDX_Control(pDX, IDC_STATIC_CURRENT2, m_ctrlStaticCurrent2);
	DDX_Control(pDX, IDC_STATIC_CAPASITOR2, m_ctrlStaticCapasity2);
	DDX_Control(pDX, IDC_LABEL_V_UNIT1, m_ctrlLabelVUnit2);
	DDX_Control(pDX, IDC_LABEL_A_UNIT1, m_ctrlLabelAUnit2);
	DDX_Control(pDX, IDC_LABEL_C_UNIT1, m_ctrlLabelCUnit2);
	DDX_Control(pDX, IDC_LABEL_VOLTAGE3, m_ctrlLabelVoltage3);
	DDX_Control(pDX, IDC_LABEL_CURRENT3, m_ctrlLabelCurrent3);
	DDX_Control(pDX, IDC_LABEL_CAPASITOR3, m_ctrlLabelCapasity3);
	DDX_Control(pDX, IDC_STATIC_VOLTAGE3, m_ctrlStaticVoltage3);
	DDX_Control(pDX, IDC_STATIC_CURRENT3, m_ctrlStaticCurrent3);
	DDX_Control(pDX, IDC_STATIC_CAPASITOR3, m_ctrlStaticCapasity3);
	DDX_Control(pDX, IDC_LABEL_V_UNIT2, m_ctrlLabelVUnit3);
	DDX_Control(pDX, IDC_LABEL_A_UNIT2, m_ctrlLabelAUnit3);
	DDX_Control(pDX, IDC_LABEL_C_UNIT2, m_ctrlLabelCUnit3);
	DDX_Control(pDX, IDC_LABEL_VOLTAGE4, m_ctrlLabelVoltage4);
	DDX_Control(pDX, IDC_LABEL_CURRENT4, m_ctrlLabelCurrent4);
	DDX_Control(pDX, IDC_LABEL_CAPASITOR4, m_ctrlLabelCapasity4);
	DDX_Control(pDX, IDC_STATIC_VOLTAGE4, m_ctrlStaticVoltage4);
	DDX_Control(pDX, IDC_STATIC_CURRENT4, m_ctrlStaticCurrent4);
	DDX_Control(pDX, IDC_STATIC_CAPASITOR4, m_ctrlStaticCapasity4);
	DDX_Control(pDX, IDC_LABEL_V_UNIT3, m_ctrlLabelVUnit4);
	DDX_Control(pDX, IDC_LABEL_A_UNIT3, m_ctrlLabelAUnit4);
	DDX_Control(pDX, IDC_LABEL_C_UNIT3, m_ctrlLabelCUnit4);
	DDX_Control(pDX, IDC_STATIC_IMPEDANCE, m_ctrlStaticImpedance);
	DDX_Control(pDX, IDC_STATIC_CHANNEL, m_ctrlStaticCh);
	DDX_Control(pDX, IDC_LIST_CH_INFO, m_wndChInfoList);
	DDX_Control(pDX, IDC_LIST_AUX, m_wndAuxList);
	DDX_Control(pDX, IDC_RESERVED_CMD_STATIC, m_ReservedCmdStatic);
	DDX_Control(pDX, IDC_LIST_LOG, m_ctrlLogList);
	DDX_Text(pDX, IDC_LABEL_LOAD_STATE_1, m_ctrlLoaderState_1);
	DDX_Text(pDX, IDC_LABEL_LOAD_STATE_2, m_ctrlLoaderState_2);
	DDX_Text(pDX, IDC_LAB_CELL_SAFETY_MAX_VALUE, m_ctrlLal_CellDeltaVolt);
	DDX_Text(pDX, IDC_LAB_AUX_TEMP_MAX, m_LabAuxTempMax);
	DDX_Text(pDX, IDC_LAB_AUX_TEMP_MIN, m_LabAuxTempMin);
	DDX_Text(pDX, IDC_LAB_AUX_VOLT_MAX, m_LabAuxVoltMax);
	DDX_Text(pDX, IDC_LAB_AUX_VOLT_MIN, m_LabAuxVoltMin);
	DDX_Text(pDX, IDC_LAB_AUX_THER_MAX, m_LabAuxTherMax);
	DDX_Text(pDX, IDC_LAB_AUX_THER_MIN, m_LabAuxTherMin);
	DDX_Control(pDX, IDC_LIST_CHANNEL, m_wndChannelList);
	DDX_Control(pDX, IDC_LIST_MODULE, m_wndModuleList);
	DDX_Control(pDX, IDC_RADIO1, m_wndLedButton);
	DDX_Control(pDX, IDC_GRID_CUSTOM, m_Grid);             // associate the grid window with a C++ object
	DDX_Control(pDX, IDC_BUT_CBANK_ON, m_ctrlCbankOn);
	DDX_Control(pDX, IDC_BUT_CBANK_OFF, m_ctrlCbankOff);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_BTN_VENT_OPEN, m_ctrlVentOpen);
	DDX_Text(pDX, IDC_LAB_AUX_THER_HUMI_MAX, m_LabAuxTherHumiMax);
	DDX_Text(pDX, IDC_LAB_AUX_THER_HUMI_MIN, m_LabAuxTherHumiMin);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_END_A1, m_btnIsoEndA1);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_END_B1, m_btnIsoEndB1);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_START_AB1, m_btnIsoStartAB1);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_END_A2, m_btnIsoEndA2);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_END_B2, m_btnIsoEndB2);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_START_AB2, m_btnIsoStartAB2);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_START, m_btnIsoCh1);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_START2, m_btnIsoCh2);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_END, m_btnNIsoCh1);
	DDX_Control(pDX, IDC_BTN_DAQ_ISOLATION_END2, m_btnNIsoCh2);
	DDX_Control(pDX, IDC_CBO_ISOL, m_ctrlCboISOL);
}

BOOL CCTSMonProView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	//cs.style |= WS_CLIPCHILDREN   ;


	return CFormView::PreCreateWindow(cs);
}

void CCTSMonProView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
		
	EnableFunctions(); //ksj 20200214 : 레지스트리 옵션에 따라 기능 노출

	mainView=this;
	GetFont()->GetLogFont(&m_lfGridFont);
	//Font를 Registry에서 Load
	UINT nSize;
	LPVOID* pData;
	AfxGetApp()->GetProfileBinary(CT_CONFIG_REG_SEC, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nSize > 0)	memcpy(&m_lfGridFont, pData, nSize);
	 delete [] pData;

	 LOGFONT logFont;
	 GetFont()->GetLogFont(&logFont);
	 logFont.lfHeight = 14;
	 logFont.lfWidth = 6;

	 sprintf(logFont.lfFaceName, "%s", "굴림"); // Need a face name "Arial".
	 m_lfChListFont.CreateFontIndirect(&logFont);


/* 메모리 덤프 예제 
// 실행 파일 있는 폴더에 MemoryDump 생성 됨
//C:\Working\CTSPack\v1013\CTSPack_V1013_real_ch_LGC_20181019_CellBalance_CANFD_OX20190115\bin\Release\MemoryDump
//20190115_214733.dmp 이와 같이 생성됨 
//위의 파일을 vc60.pdb 이 있는 폴더 
//C:\Working\CTSPack\v1013\CTSPack_V1013_real_ch_LGC_20181019_CellBalance_CANFD_OX20190115\obj\CTSMonPro_Release 로 가져와 복사
//20190115_214733.dmp클릭하여 실행
//네이티브로 디버깅하면 중단점 찾을 수 있음 

	CMiniDump::Begin();
	
	MyObject* Test = new MyObject;
	Test = NULL;
	Test->mIsOpened = TRUE;     
	
    CMiniDump::End();
*/

	m_nDisplayCol =  AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "GridColumnSize", 8);

	char szCurrentDir[128];
	GetCurrentDirectory(128, szCurrentDir);



	m_bUseAux = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseAUX", FALSE);	//ljb 2009-01 추가
	m_bUseCan = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCAN", FALSE);

	m_bUseIsolation = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolation", FALSE);
	m_bUseLoader = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLoader", FALSE);		//ljb 20151218 add
	m_bUseMux = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseMuxLink", FALSE);			//ljb 20151218 add
	m_bUseLoadType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLoad Type", FALSE);		//ljb 20160122 add
	m_bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE);		//yulee 20180227 OverCharger
	float OverChargerMinVolt = 0; //yulee 20191017 OverChargeDischarger Mark
	if(m_bOverChargerSet) //yulee 20180810
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);

	m_bUseCbank = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCbank", FALSE);
	m_bAccCycleOffSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AccCycleOffSet", FALSE);


	m_wndLedButton.SetImage(IDB_GREEN_BUTTON, 15);
	m_wndLedButton.Depress(FALSE);

	InitGrid();
	InitTabCtrl();
	//InitLabel(18);
	//InitLabel(14); //yulee 20180905
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Module List ctrl 초기화 
	InitModuleList();

	//Channel List ctrl 초기화 
	InitChannelList();

//yulee 20181019
	int chCnt = m_wndChannelList.GetItemCount();

	if(chCnt == 1)
	{
		InitLabel(18);
	}
	else if(chCnt == 2)
	{
	
		InitLabel(18);
	}
	else if(chCnt == 4)
	{
		InitLabel(18); //yulee 20180905
	}

	//Channel Info List Ctrl 초기화
	InitChInfoList();
	InitStaticCounter();

	//Can List Ctrl 초기화
 	InitCanList();

	//Aux List ctrl 초기화
 	InitAuxList();

	if(m_bUseAux)
	{
		m_wndAuxList.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_AUX)->ShowWindow(SW_SHOW);
	}
	else
	{
		m_wndAuxList.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_AUX)->ShowWindow(SW_HIDE);
	}
	if(m_bUseCan == FALSE)
	{
		m_wndCanList.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_CAN)->ShowWindow(SW_HIDE);
	}
	else
	{
		m_wndCanList.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_CAN)->ShowWindow(SW_SHOW);
	}

	if(m_bUseIsolation){					
		//GetDlgItem(IDC_BTN_DAQ_ISOLATION_START)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_BTN_DAQ_ISOLATION_END)->ShowWindow(SW_SHOW);

		/*
		GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_A1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_A2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_B1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_B2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DAQ_ISOLATION_START_AB1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DAQ_ISOLATION_START_AB2)->ShowWindow(SW_SHOW);
		*/

		//lyj 20210913 절연/비절연 V101B와 동일하게 정리 s ==========
		bool bUseIsolationBranch = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationBranch", 0);
		int  nBranchNum = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationBranchNum", 2);
		bool bUseIsolationCMD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCMD", 1); //lyj 20210913 0: SFT_CMD_DAQ_ISOLATION_COMM_REQUEST  1: SFT_CMD_OUTPUT_CH_CHANGE_REQUEST 

		GetDlgItem(IDC_CBO_ISOL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUT_ISOL_CHOICE)->ShowWindow(SW_SHOW);

		if(bUseIsolationBranch && bUseIsolationCMD) 
		{
			m_ctrlCboISOL.DeleteString(1); //lyj 20210913 분기 사용이면 혼선방지를 위해 Non Isolation 삭제
			m_ctrlCboISOL.AddString("Branch A");
			m_ctrlCboISOL.AddString("Branch B");
			if(nBranchNum == 3) m_ctrlCboISOL.AddString("Branch C"); //lyj 20210913 
			if(nBranchNum == 4) m_ctrlCboISOL.AddString("Branch D"); //lyj 20210913 
		}
		//lyj 20210913 절연/비절연 V101B와 동일하게 정리 e ==========

		/* lyj 20210913 절연/비절연 V101B와 동일하게 정리
		//ksj 20201124 : 절연 비절연 활성화 세부 옵션
		BOOL bUseIsolationType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationType", 0);
		BOOL bUseIsolationCh1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCh1", FALSE);
		BOOL bUseIsolationCh2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCh2", FALSE);

		if(bUseIsolationCh1)
		{
			if(bUseIsolationType) //a비절연, b비절연 방식
			{
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_A1)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_B1)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_START_AB1)->ShowWindow(SW_SHOW);
			}
			else //그냥 절연/비절연
			{
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_START)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_END)->ShowWindow(SW_SHOW);
			}
			
		}
		if(bUseIsolationCh2)
		{
			if(bUseIsolationType) //a비절연, b비절연 방식
			{
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_A2)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_END_B2)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_START_AB2)->ShowWindow(SW_SHOW);
			}
			else //그냥 절연/비절연
			{
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_START2)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_BTN_DAQ_ISOLATION_END2)->ShowWindow(SW_SHOW);
			}			
		}
		//ksj end
		*/
		
		// 211015 HKH 절연모드 충전 관련 기능 보완
		int nCurSel = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Select IsolChoice", 0);
		m_ctrlCboISOL.SetCurSel(nCurSel);
		
	}


	if(m_bUseCbank)
	{				
		GetDlgItem(IDC_BUT_CBANK_ON)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUT_CBANK_OFF)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_BUT_CBANK_ON)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BUT_CBANK_OFF)->ShowWindow(SW_HIDE);
	}

	//ljb 20151218 add
	if(m_bUseLoader){					
		GetDlgItem(IDC_LABEL_LOAD_STATE_1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_LABEL_LOAD_STATE_2)->ShowWindow(SW_SHOW);
	}
	//ljb 20151218 add
	if(m_bUseMux){					
		GetDlgItem(IDC_CBO_MUX)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUT_MUX_CHOICE)->ShowWindow(SW_SHOW);
	}

	//ljb 20160122 LOAD Type 0:EDLC, 1: Cell
	if(m_bUseLoadType){					
		GetDlgItem(IDC_CBO_LOAD)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUT_LOAD_CHOICE)->ShowWindow(SW_SHOW);
	}

	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryAuxDivision(m_uiArryAuxDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryAuxStringName(m_strArryAuxName);	//ljb 2011222 이재복 //////////

	GetDocument()->Fun_GetArryAuxThrDivision(m_uiArryAuxThrDivition);
	GetDocument()->Fun_GetArryAuxThrStringName(m_strArryAuxThrName);

	GetDocument()->Fun_GetArryAuxHumiDivision(m_uiArryAuxHumiDivition); //ksj 20200207
	GetDocument()->Fun_GetArryAuxHumiStringName(m_strArryAuxHumiName); //ksj 20200207

	// Channel 정보 Display를 위한 타이머
	m_nChannelUpdateInterval = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Channel Timer", 2);
	SetTimer(TIMER_DISPLAY_CHANNEL, m_nChannelUpdateInterval*1000, NULL);
	// Module 정보 Display를 위한 타이머
	m_nModuleUpdateInterval = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Module Timer", 2);
	SetTimer(TIMER_DISPLAY_MODULE, m_nModuleUpdateInterval*1000, NULL);	
	// Monitoring Data 저장을 위한 타이머
	m_nMonitoringInterval = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Monitoring Timer", 1);
//  #ifdef _DEBUG //ksj 20201221
//  	SetTimer(TIMER_MONITORING, 1000, NULL); //test
//  #else
	SetTimer(TIMER_MONITORING, m_nMonitoringInterval*1000*60, NULL);
//#endif

	//ksj 20160801 SBC Raw Data FTP 백업 기능. 일반 Cycler에서 가져와 적용. 날짜 변경을 체크한다.
	m_tmpDay = GWin::win_CurrentTime("filelog");
	SetTimer(TIMER_DAY_CHANGE_CHECK, 60*1000, NULL);		//날짜 변경 체크.
	
	SetTimer(TIMER_AUTO_UPDATE, m_nMonitoringInterval*1000*30, NULL); //lyj 20210809 자동 업데이트 타이머

	onUsePlacePlate();

	g_Font.CreateFont(20, 8, 0, 0, FW_BOLD ,
		0, 0, 0, DEFAULT_CHARSET, 0,
		CLIP_CHARACTER_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH |
		FF_DONTCARE, "Arial");

	m_ctrlButBuzzerStop.SetFont(&g_Font);
	m_ctrlButBuzzerStop.SetTextAlign(CButtonSc::BTNST_ALIGN_TEXT_LEFT);
	m_ctrlButBuzzerStop.SetCheck(BST_UNCHECKED);
	//m_ctrlButBuzzerStop.SetWindowTextA(" BUZZER STOP");
	m_ctrlButBuzzerStop.SetWindowTextA(" BUZZER\n STOP"); //ksj 20200122
	m_ctrlButBuzzerStop.SetIcon(IDI_MUTE_ICON, CSize(64, 64), IDI_MUTE_ICON, CSize(64, 64), IDI_MUTE_ICON, CSize(64, 64));

	m_ctrlEMG.SetFont(&g_Font);
	m_ctrlEMG.SetTextAlign(CButtonSc::BTNST_ALIGN_TEXT_LEFT);
	m_ctrlEMG.SetCheck(BST_UNCHECKED);
	//m_ctrlEMG.SetWindowTextA(" EMG STOP");
	m_ctrlEMG.SetWindowTextA(" EMG\nSTOP");//ksj 20200122
	m_ctrlEMG.SetIcon(IDI_EMG_STOP_ICON, CSize(64, 64), IDI_EMG_STOP_ICON, CSize(64, 64), IDI_EMG_STOP_ICON, CSize(64, 64));


	m_ctrlVentClear.SetFont(&g_Font);
	m_ctrlVentClear.SetTextAlign(CButtonSc::BTNST_ALIGN_TEXT_LEFT);
	m_ctrlVentClear.SetCheck(BST_UNCHECKED);
	//m_ctrlVentClear.SetWindowTextA(" VENT CLEAR");
	m_ctrlVentClear.SetWindowTextA(" VENT\n CLOSE"); //ksj 20200120
	m_ctrlVentClear.SetIcon(IDI_VENT_CLEAR, CSize(64, 64), IDI_VENT_CLEAR, CSize(64, 64), IDI_VENT_CLEAR, CSize(64, 64));

	//ksj 20200122 : v1016, Vent Open 기능 추가
	m_ctrlVentOpen.SetFont(&g_Font);
	m_ctrlVentOpen.SetTextAlign(CButtonSc::BTNST_ALIGN_TEXT_LEFT);
	m_ctrlVentOpen.SetCheck(BST_UNCHECKED);
	m_ctrlVentOpen.SetWindowTextA(" VENT\n OPEN"); //ksj 20200120
	m_ctrlVentOpen.SetIcon(IDI_VENT_OPEN, CSize(64, 64), IDI_VENT_OPEN, CSize(64, 64), IDI_VENT_OPEN, CSize(64, 64));
	//ksj end
	
	HideOrShowButton();
	//HideVentClose();
	
	//CMiniDump::Begin(); //yulee 20190121
	GetDocument()->WriteSysLog("Program Started");
 	GetDocument()->WriteSysEventLog("Start Event log write");	//ljb 201012

	//Serial Port 사용을 체크하고 포트를 연다.	
	GetDocument()->CheckUsePort();

	onInit();

	//ksj 20160426 작업 예약 윈도우 생성
	m_pReserveDlg = new CReserveDlg(GetDocument(),this);
	//m_pReserveDlg->Create(IDD_RESERVE_DLG);	
	m_pReserveDlg->Create(
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_RESERVE_DLG):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_WORK_RESERVE_DLG_ENG):
		IDD_RESERVE_DLG);

	//m_pReserveDlg->ModifyStyleEx(0, WS_EX_APPWINDOW, SWP_FRAMECHANGED);

	//ksj 20160613 작업 예약 기능 레지스트리 옵션 처리.
	//if(!AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE_VAL)) 
	m_bUseWorkReserv = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE_VAL);
	if(!m_bUseWorkReserv) 
	{
		GetDlgItem(IDC_BTN_RESERV)->ShowWindow(SW_HIDE);
	}

	//ksj 20201209 : 초기 접속시 SBC와 ConnectionSequence 상태 실시간 표시 기능 추가
	//모듈 개수만큼 창 생성.
	int nModuleCount = GetDocument()->GetInstallModuleCount();
	ZeroMemory(m_pDlgConnSeq,sizeof(m_pDlgConnSeq));
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Display ConnSeq", TRUE))  //레지스트리 옵션 처리.
	{
		for(int nI=0;nI<nModuleCount;nI++)
		{
			m_pDlgConnSeq[nI] = new CDlgConnectionSequence(this);
			m_pDlgConnSeq[nI]->Create(
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_CONNECTION_SEQUENCE):
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_CONNECTION_SEQUENCE):
				IDD_DIALOG_CONNECTION_SEQUENCE);
		}
	}
	//ksj end 

	//yulee 20190114
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 0);

	OnWorkSTShow(); //yulee 20181005

	//------------------------------
	onPwrSupplyUse(1);//cny 201809
	onPwrSupplyUse(2);//cny 201809
	//------------------------------
	onCellbalShow(1);//cny 201809
	onCellbalShow(2);//cny 201809
	//------------------------------
	onChillerUse(1);//cny 201809
	onChillerUse(2);//cny 201809
	onChillerUse(3);//cny 201809
	onChillerUse(4);//cny 201809	
	
	//ksj 20200202 : 처음 1회 호출.
	PSGetColorCfgData(FALSE);


	//ksj 20201013 : 깜빡임 색상 검출 쓰레드 생성
	if(!AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Flicker Color Table", TRUE));
	{
		m_bThreadSetFlickerColor = TRUE;
		m_pThreadSetFlickerColor = AfxBeginThread(ThreadSetFlickerColor, this);
	}	

	//ksj 20200903 : JSON 모니터링 초기화
	InitJsonMon();
	 
	UpdateUserConfig(); //ksj 20201112
		
	//ksj 20200911 : 테스트용 버튼 활성화
	int nTestButton = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Enable TEST BUTTON", FALSE);
#ifdef _DEBUG
	nTestButton = TRUE;
#endif
	if(nTestButton)
	{
		GetDlgItem(IDC_BUTTON_TESTBTN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUTTON_TESTBTN)->EnableWindow(TRUE);
	}	
	//ksj end
}

void CCTSMonProView::ResetChinfoListHeightInCh4(unsigned CtrlID, int Gap) //yulee 20181022
{
	CWnd* pWnd = NULL;

	CRect rectCtrl;
	pWnd = GetDlgItem(CtrlID);

	if(pWnd->GetSafeHwnd())
	{
		pWnd->GetWindowRect(rectCtrl);
		ScreenToClient(&rectCtrl);
		
		pWnd->MoveWindow(rectCtrl.left, rectCtrl.top, rectCtrl.Width(), rectCtrl.Height()-Gap);
	}
}

void CCTSMonProView::ResetFuncBut(unsigned CtrlID, int nRow, int nColumn, int nChNum, int nSizeType)
{
	CWnd* pWnd = NULL;
	CWnd* pWnd1 = NULL;
	CWnd* pWnd2 = NULL;

	int nSelLanguage;
	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	
	CRect rectCtrl, rectPt, rectTopPt;
	if(nChNum == 2)
	{
		pWnd = GetDlgItem(CtrlID);
		pWnd1 = GetDlgItem(IDC_STA_CH2INFOBOXINCH2);
		//pWnd2 = GetDlgItem(IDC_STA_CH4INFOBOX);
		
		if(pWnd->GetSafeHwnd())
		{
			pWnd->GetWindowRect(rectCtrl);
			ScreenToClient(&rectCtrl);
			pWnd1->GetWindowRect(rectPt);
			ScreenToClient(&rectPt);
			//pWnd2->GetWindowRect(rectTopPt);
			//ScreenToClient(&rectTopPt);
			
			if(nSelLanguage == 1)
			{
				//pWnd->MoveWindow(rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5), rectCtrl.Width(), rectCtrl.Height());
				::SetWindowPos(pWnd->m_hWnd,NULL,rectPt.left+(81*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5),0,0, SWP_NOSIZE|SWP_SHOWWINDOW);

			}
			else if(nSelLanguage == 2)
			{
				//pWnd->MoveWindow(rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5), rectCtrl.Width(), rectCtrl.Height());
				::SetWindowPos(pWnd->m_hWnd,NULL,rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5),0,0,SWP_NOSIZE|SWP_SHOWWINDOW);

			}
			else if(nSelLanguage == 3)
			{
				//pWnd->MoveWindow(rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5), rectCtrl.Width(), rectCtrl.Height());
				::SetWindowPos(pWnd->m_hWnd,NULL,rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5),0,0,SWP_NOSIZE|SWP_SHOWWINDOW);

			}
		}
	}
	else if(nChNum == 4) //yulee 20181024
	{
		pWnd = GetDlgItem(CtrlID);
		pWnd1 = GetDlgItem(IDC_STATIC_CHINFO);
		
		if(pWnd->GetSafeHwnd())
		{
			pWnd->GetWindowRect(rectCtrl);
			ScreenToClient(&rectCtrl);
			pWnd1->GetWindowRect(rectPt);
			ScreenToClient(&rectPt);
		

			if(nSelLanguage == 1)
			{
				if(nSizeType == 0)
				{
					//pWnd->MoveWindow(rectPt.left+(88*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5), rectCtrl.Width()-1.5, rectCtrl.Height());
					
					::SetWindowPos(pWnd->m_hWnd,NULL,rectPt.left+(85*(nRow-1)), rectPt.bottom+(28*(nColumn-1)+5),0,0,SWP_NOSIZE|SWP_SHOWWINDOW);
				}
				else
				{
					//pWnd->MoveWindow(rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5), rectCtrl.Width(), rectCtrl.Height());
					
					::SetWindowPos(pWnd->m_hWnd,NULL,rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5),0,0,SWP_NOSIZE|SWP_SHOWWINDOW);
				}
			}
			else if(nSelLanguage == 2 || nSelLanguage == 3)
			{
				if(nSizeType == 0)
				{
					//pWnd->MoveWindow(rectPt.left+(88*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5), rectCtrl.Width()-1.5, rectCtrl.Height());
					
					::SetWindowPos(pWnd->m_hWnd,NULL,rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5),rectCtrl.Width(),rectCtrl.Height(),SWP_SHOWWINDOW);
				}
				else
				{
					//pWnd->MoveWindow(rectPt.left+(101*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5), rectCtrl.Width(), rectCtrl.Height());
					
					::SetWindowPos(pWnd->m_hWnd,NULL,rectPt.left+(120*(nRow-1)), rectPt.bottom+(25*(nColumn-1)+5),rectCtrl.Width(),rectCtrl.Height(),SWP_SHOWWINDOW);
				}
			}

			//pWnd->GetWindowRect(rectCtrl);
		}
	}

}

void CCTSMonProView::onInit()
{	
	mainPneApp.m_StartTime=GTime::time_GetTime(GWin::win_CurrentTime(_T("full")));
	//2014.08.14 
	//m_hState=(HANDLE)_beginthreadex(NULL,0,StateThread,(LPVOID)this,0,NULL);		
}

UINT WINAPI CCTSMonProView::StateThread( LPVOID wnd )
{	
	CCTSMonProView *dlg=(CCTSMonProView*)wnd;
	if(!dlg) return 0;
	
	dlg->onDbInit();
	
	int iPooltime=3000;
	Sleep(iPooltime);
		
	while(TRUE)
	{	
		if(g_AppInfo.iState==0) break;
						
		mainPneApp.ReConnectTime();//regular time, reconnect
						
		dlg->onMonitoringDb();	
		
		mainPneApp.ReConnectError();//if error, reconnect
		
		Sleep(iPooltime);
	}
	
	return 0;
}

void CCTSMonProView::onDbInit()
{
	if(g_AppInfo.strDbauto==_T("1"))
	{		
		mainPneApp.m_MySql.DBConn(g_AppInfo.strDbip,g_AppInfo.strDbid,g_AppInfo.strDbpwd,
			g_AppInfo.strDbname,_ttoi(g_AppInfo.strDbport),FALSE,TRUE);

		mainPneApp.CreateTbMonitor();
	}
}

void CCTSMonProView::onMonitoringDeal()
{	
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pModule = NULL;
	CCyclerChannel *lpChannelInfo=NULL;
	
	UINT nModuleID, nChannelIndex,nStartCh;
	char szBuff[128];	

	//PS_COLOR_CONFIG* pColorData=::PSGetColorCfgData(FALSE);
	PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData2(); //ksj 20200202 : 원래 함수(PSGetColorCfgData)는 호출시마다 파일에서 다시 불러오므로 해당 부분 생략.

	//float fData;
	CStep *pStep;
	CScheduleData schData;
	
	for(UINT nI = 0; nI < pDoc->GetInstallModuleCount(); nI++ )
	{
		pModule = pDoc->GetModuleInfo(pDoc->GetModuleID(nI));
		if(!pModule) continue;

		nModuleID=pModule->GetModuleID();
		nStartCh = GetStartChNo(nModuleID);
			
		for(UINT i=0; i<pModule->GetTotalChannel(); i++)
		{
			nChannelIndex=i;

			CCyclerChannel *lpChannelInfo = pModule->GetChannelInfo(i);
			if(!lpChannelInfo) continue;

			lpChannelInfo->m_iCh=nStartCh+nChannelIndex;
			lpChannelInfo->m_strNmmodule=pDoc->GetModuleName(nModuleID);
			CHINFO *chinfo=mainGlobal.ch_GetChList(nModuleID,nChannelIndex);
			if(chinfo)
			{
				m_PlayState=PS1_PAUSE;
				chinfo->nmwork=lpChannelInfo->GetTestName();
				chinfo->nModuleID=nModuleID;//
				
				chinfo->nChannelIndex=nStartCh+nChannelIndex;//
				chinfo->nmmodule=pDoc->GetModuleName(nModuleID);
				chinfo->testserial=lpChannelInfo->GetTestSerial();//nmcondition
				chinfo->nmsched=lpChannelInfo->GetScheduleName();
				
				chinfo->pRecord.chNo=(unsigned char)nChannelIndex;
				chinfo->pRecord.chStepNo=lpChannelInfo->GetStepNo();
				chinfo->pRecord.chState                 =(unsigned char)lpChannelInfo->GetState();
				chinfo->pRecord.chStepType				=(unsigned char)lpChannelInfo->GetStepType();
				
				chinfo->pRecord.chDataSelect            =0;
				
				chinfo->pRecord.chCode					=(unsigned char)lpChannelInfo->GetCellCode();
				chinfo->pRecord.chGradeCode				=lpChannelInfo->GetGradeCode();
				chinfo->pRecord.chCommState				=lpChannelInfo->GetCommState();
				chinfo->pRecord.chOutputState			=lpChannelInfo->GetChOutputState();
				chinfo->pRecord.chInputState			=lpChannelInfo->GetChInputState();
				chinfo->pRecord.cReserved1              =0;
				chinfo->pRecord.cReserved2              =0;
				
				chinfo->pRecord.nIndexFrom              =0;
				chinfo->pRecord.nIndexTo                =0;			
				
				chinfo->pRecord.nCurrentCycleNum		=lpChannelInfo->GetCurCycleCount();
				chinfo->pRecord.nTotalCycleNum			=lpChannelInfo->GetTotalCycleCount();
				chinfo->pRecord.lSaveSequence           =0;
				
				chinfo->pRecord.nAccCycleGroupNum1      =lpChannelInfo->GetAccCycleCount1();
				chinfo->pRecord.nAccCycleGroupNum2      =lpChannelInfo->GetAccCycleCount2();
				chinfo->pRecord.nAccCycleGroupNum3      =lpChannelInfo->GetAccCycleCount3();
				chinfo->pRecord.nAccCycleGroupNum4      =lpChannelInfo->GetAccCycleCount4();
				chinfo->pRecord.nAccCycleGroupNum5      =lpChannelInfo->GetAccCycleCount5();
				
				chinfo->pRecord.nMultiCycleGroupNum1    =lpChannelInfo->GetMultiCycleCount1();
				chinfo->pRecord.nMultiCycleGroupNum2    =lpChannelInfo->GetMultiCycleCount2();
				chinfo->pRecord.nMultiCycleGroupNum3    =lpChannelInfo->GetMultiCycleCount3();
				chinfo->pRecord.nMultiCycleGroupNum4    =lpChannelInfo->GetMultiCycleCount4();
				chinfo->pRecord.nMultiCycleGroupNum5    =lpChannelInfo->GetMultiCycleCount5();
				
				chinfo->pRecord.fVoltage				=(float)lpChannelInfo->GetVoltage();
				chinfo->pRecord.fCurrent				=(float)lpChannelInfo->GetCurrent();
				chinfo->pRecord.fCapacity				=(float)lpChannelInfo->GetCapacity();
				chinfo->fCapacitySum			    	=(float)lpChannelInfo->GetCapacitySum();
				chinfo->pRecord.fWatt					=(float)lpChannelInfo->GetWatt();
				chinfo->pRecord.fWattHour			    =(float)lpChannelInfo->GetWattHour();
//				chinfo->pRecord.fStepTimeday			=(float)lpChannelInfo->GetStepTimeDay();
				chinfo->pRecord.fStepTime				=(float)lpChannelInfo->GetStepTime();
//				chinfo->pRecord.fTotalTimeDay			=(float)lpChannelInfo->GetTotTimeDay();
				chinfo->pRecord.fTotalTime				=(float)lpChannelInfo->GetTotTime();
				chinfo->pRecord.fImpedance				=(float)lpChannelInfo->GetImpedance();
				
				chinfo->pRecord.fAvgVoltage	            =(float)lpChannelInfo->GetAvgVoltage();
				chinfo->pRecord.fAvgCurrent             =(float)lpChannelInfo->GetAvgCurrent();
				chinfo->pRecord.fChargeAh               =(float)lpChannelInfo->GetChargeAh();
				chinfo->pRecord.fDisChargeAh            =(float)lpChannelInfo->GetDisChargeAh();
				chinfo->pRecord.fCapacitance            =(float)lpChannelInfo->GetCapacitance();
				chinfo->pRecord.fChargeWh               =(float)lpChannelInfo->GetChargeWh();
				chinfo->pRecord.fDisChargeWh            =(float)lpChannelInfo->GetDisChargeWh();
				chinfo->pRecord.fCVTime                 =(float)lpChannelInfo->GetCVTime();
				chinfo->pRecord.lSyncDate               =lpChannelInfo->GetSyncDate();
				chinfo->pRecord.fSyncTime               =(float)lpChannelInfo->GetSyncTime();
				chinfo->pRecord.fVoltageInput           =(float)lpChannelInfo->GetVoltage_Input();
				chinfo->pRecord.fVoltagePower           =(float)lpChannelInfo->GetVoltage_Power();
				chinfo->pRecord.fVoltageBus             =(float)lpChannelInfo->GetVoltage_Bus();			
				
				chinfo->fMeter			    	        =(float)lpChannelInfo->GetMeterValue();
				chinfo->starttime                       =lpChannelInfo->GetStartTime();
				chinfo->endtime                         =lpChannelInfo->GetEndTime();
				chinfo->isParallel                      =lpChannelInfo->IsParallel();
				chinfo->bMaster                         =lpChannelInfo->m_bMaster;
				
				//ljb 상태 표시 ////////////////////////////////////////////////////////////////////////
				//Current Step Type (Charge, Discharge, OCV, Rest)
				CString strDisplay,strTemp;
				int iImage=-1;
				//nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STATE);
				//if( nColIndex >= 0 )
				//{		
					// 여결정보 끊김 별도로 뺌		
				//}
				// # 연결정보 끊김 표시				
				if( lpChannelInfo->GetState() == PS_STATE_LINE_OFF )
				{				
					sprintf(szBuff, "%s", Fun_FindMsg("onMonitoringDeal_msg1","IDD_CTSMonPro_FORM"));
					//@ sprintf(szBuff, "%s", "연결 끊김");
				}
				else
				{
					//TRACE("%d : %d\n",lpChannelInfo->GetState(), lpChannelInfo->GetStepType());					
					if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
					{
						////////// 병렬 연결시 표시
						strDisplay = Fun_FindMsg("onMonitoringDeal_msg2","IDD_CTSMonPro_FORM");
						//@ strDisplay = "병렬연결";
					}
					else
					{
						WORD state = lpChannelInfo->GetState() ;
						WORD type  = lpChannelInfo->GetStepType() ;
						
						strDisplay.Format("%s",::PSGetStateMsg(state, type));
						//							strDisplay.Format("%s",::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
						//sprintf(szBuff, "%s", ::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
						
						// Added by 224 (2014/10/20) :
						// 작업공정 종료시 LIN 작업도 종료한다.
						if (state != lpChannelInfo->m_prev_state || type != lpChannelInfo->m_prev_type)
						{
							lpChannelInfo->m_prev_state = state ;
							lpChannelInfo->m_prev_type  = type ;
							TRACE("ModuleID[%d]. channel[%d]. state = %02X, %s\n", nModuleID, nChannelIndex, state, strDisplay) ;
							
							//if (state == PS_STATE_END || strDisplay == "완료")
							if (state == PS_STATE_END || strDisplay == Fun_FindMsg("CTSMonProView_onMonitoringDeal_msg1","IDD_CTSMonPro_FORM"))//&&
							{
								// LIN 공정이 동작하고 있으면 정지시킨다.
								INT nDevID = (nModuleID-1) * 2 + nChannelIndex ;
								pDoc->StopLinCanConvThread(nDevID) ;
								
								// Lin 설정 dlalog 가 띄어져 있으면 닫는다.
								if (m_pLinConvDlg)
								{
									m_pLinConvDlg->PostMessage(WM_COMMAND, MAKELONG(IDC_CLOSE, BN_CLICKED), NULL) ;
								}
								
							}
						}
					}
					
					if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
					{
						pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
						if(pStep)
						{
							if(pStep->m_bUseCyclePause == TRUE)
							{
								strDisplay += " (Cycle Pause) ";
							}
						}
					}
					
					if (lpChannelInfo->GetChamberUsing())
					{
						if(lpChannelInfo->m_bOvenLinkChargeDisCharge &&( lpChannelInfo->GetStepType() == PS_STEP_CHARGE || lpChannelInfo->GetStepType() == PS_STEP_DISCHARGE) )//201111 챔버 연동 온도 대기시 멘트 수정
						{
							strDisplay += Fun_FindMsg("onMonitoringDeal_msg3","IDD_CTSMonPro_FORM"); 
							//@ strDisplay += "(챔버연동온도대기멈춤)"; 
						}
						else strDisplay += pDoc->ValueString(lpChannelInfo->GetChamberUsing(), PS_CHAMBER_USING);
					}
					sprintf(szBuff, "%s", strDisplay);			
				}
				
				//1. 명령 예약 상태 표시 
				if(lpChannelInfo->IsStopReserved())
				{
					iImage = 12;
				}
				else if(lpChannelInfo->IsPauseReserved())
				{
					iImage = 13;
				}
				else
				{
					//자동복구 주석
					//2. Data Loss 상태 표시
					if(lpChannelInfo->IsDataLoss())
					{
						//TRACE("%s \n",::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
						iImage = pDoc->GetStateImageIndex(PS_STATE_FAIL);
						
						//자동복구 리스트에 추가
						//if ((lpChannelInfo->GetState() == PS_STATE_END || lpChannelInfo->GetState() == PS_STATE_IDLE || lpChannelInfo->GetState() == PS_STATE_STANDBY) && lpChannelInfo->GetCompleteWork() == FALSE)
						if ((lpChannelInfo->GetState() == PS_STATE_END) && lpChannelInfo->GetCompleteWork() == FALSE)
						{								
						}
					}
					else
					{
						iImage = -1;
					}
				}
				
				chinfo->Eqstate=GStr::str_IntToStr(iImage);
				chinfo->Eqstr=szBuff;
					m_PlayState=PS1_PLAY;		
			}			
		}
	}
}

void CCTSMonProView::onMonitoringDb()
{			
	if(m_PlayState==PS1_PLAY)
	{//pooling
		BOOL bRet=mainPneApp.SetStateInsert(g_AppInfo.ChList);
		
		if(bRet==FALSE && mainPneApp.m_MySql.m_iErr==145)
		{//table repair
			mainPneApp.m_MySql.SetQuery(_T("REPAIR TABLE MONITORINFO"));
			GLog::log_Save("monitor","db","repair",NULL,TRUE);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CCTSMonProView printing

BOOL CCTSMonProView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CCTSMonProView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CCTSMonProView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CCTSMonProView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProView diagnostics

#ifdef _DEBUG
void CCTSMonProView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCTSMonProView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
CCTSMonProDoc* CCTSMonProView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCTSMonProDoc)));
	return (CCTSMonProDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProView message handlers

//모듈 상태 표시용 리스트 컨트롤 초기화 
void CCTSMonProView::InitModuleList()
{
	CCTSMonProDoc *pDoc = GetDocument();

//	GetDlgItem(IDC_LIST_MODULE)->ShowWindow(SW_SHOW);

	//상태 표시용 아이콘 생성 
	m_pChLineSts = new CImageList;
//	m_pChLineSts->Create(IDB_LINE_STATE, 20, 4,RGB(255,255,255));
	m_pChLineSts->Create(IDB_MD_STATE, 19, 2,RGB(255,255,255));


	m_wndModuleList.SetImageList(m_pChLineSts, LVSIL_SMALL);
	m_wndModuleList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_wndModuleList.SetFont(&m_lfChListFont);

	//Column 3개 생성 
// 	char szTitle[][30] = 
// 	{
// 		Fun_FindMsg("onMonitoringDeal_msg4"), Fun_FindMsg("onMonitoringDeal_msg5"), Fun_FindMsg("onMonitoringDeal_msg6"),
// 		Fun_FindMsg("onMonitoringDeal_msg7"), Fun_FindMsg("onMonitoringDeal_msg8"), Fun_FindMsg("onMonitoringDeal_msg9"),
// 		Fun_FindMsg("onMonitoringDeal_msg10"), Fun_FindMsg("onMonitoringDeal_msg11"), Fun_FindMsg("onMonitoringDeal_msg12"), Fun_FindMsg("onMonitoringDeal_msg13")
// 
// 			//@ "모듈", "연결상태", "연결 시간", "전체채널수", "대기채널수", "작업채널수", "접속횟수", "IP Address", "Spec.", "SBC Info"
// 	};
	char szTitle[10][30];
	ZeroMemory(szTitle,300);
	sprintf(szTitle[1],"%s",Fun_FindMsg("onMonitoringDeal_msg5","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[2],"%s",Fun_FindMsg("onMonitoringDeal_msg6","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[3],"%s",Fun_FindMsg("onMonitoringDeal_msg7","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[4],"%s",Fun_FindMsg("onMonitoringDeal_msg8","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[5],"%s",Fun_FindMsg("onMonitoringDeal_msg9","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[6],"%s",Fun_FindMsg("onMonitoringDeal_msg10","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[7],"%s",Fun_FindMsg("onMonitoringDeal_msg11","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[8],"%s",Fun_FindMsg("onMonitoringDeal_msg12","IDD_CTSMonPro_FORM"));
	sprintf(szTitle[9],"%s",Fun_FindMsg("onMonitoringDeal_msg13","IDD_CTSMonPro_FORM"));


	//각 column 폭 지정 
	int nWidth[10] = 
	{
		90, 100, 180, 100, 100, 100, 80, 100, 150, 150  
	};
	for(int i = 0; i < 10; i++)
	{
		m_wndModuleList.InsertColumn( i+1,
									  szTitle[i], 
									  LVCFMT_CENTER, 
									  nWidth[i], 
									  i);
	}

//	CString strTitle;
	char szName[128];

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
	CCyclerModule *lpModule;
	for(UINT nI = 0; nI < pDoc->GetInstallModuleCount(); nI++ )
	{
		sprintf(szName, "%s", pDoc->GetModuleName(pDoc->GetModuleID(nI)));
		lvItem.iItem = nI;
		lvItem.iSubItem = 0;
		lvItem.pszText = szName;
//		lvItem.lParam = pDoc->GetModuleID(nI);
		lvItem.iImage = I_IMAGECALLBACK;
		m_wndModuleList.InsertItem(&lvItem);
		m_wndModuleList.SetItemData(lvItem.iItem, pDoc->GetModuleID(nI));		//==>LVN_ITEMCHANGED 를 발생 기킴 
		
		sprintf(szName, "0");
		m_wndModuleList.SetItemText(nI, 6, szName);
		lpModule = pDoc->GetModuleInfo(pDoc->GetModuleID(nI));
		m_wndModuleList.SetItemText(nI, 7, lpModule->GetIPAddress());
		//Default Selected Module ID

		//Range 모두 표시 20081222 KHS
		CString rng, str;
		str.Format("%.1fV/", lpModule->GetVoltageSpec()/1000.0f);
		for(int r=0; r<lpModule->GetCurrentRangeCount(); r++)
		{
			if(r>0)
			{
				str+=",";
			}
			rng.Format("%.1f", lpModule->GetCurrentSpec(r)/1000.0f);
			str += rng;
		}
		rng.Format("A(%dR)", lpModule->GetCurrentRangeCount());
		str += rng;
		//sprintf(szName, "%.1fV/%.1fA(%dR)", lpModule->GetVoltageSpec()/1000.0f, lpModule->GetCurrentSpec()/1000.0f, lpModule->GetCurrentRangeCount());
		m_wndModuleList.SetItemText(nI, 8, str);
	}

	//가장 첫번째를 기본으로 선택함 
	SetModuleSelChanged(pDoc->GetModuleID(0));
	m_wndModuleList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
	
	//content update
	OnUpdateModuleList();
}

void CCTSMonProView::InitChannelList()
{
	m_pChStsSmallImage = new CImageList;

	//상태별 표시할 이미지 로딩
	//m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON,19,17,RGB(255,255,255));
	m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON1,26,17,RGB(255,255,255));
	m_wndChannelList.SetImageList(m_pChStsSmallImage, LVSIL_SMALL);
	m_wndChannelList.SetFont(&m_lfChListFont);

	//
	DWORD style = 	m_wndChannelList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;
	m_wndChannelList.SetExtendedStyle(style );//|LVS_EX_TRACKSELECT);

//	LVCOLUMN col;
//	ZeroMemory(&col, sizeof(col));
//	col.mask = LVIF_TEXT|LVCF_WIDTH|LVCF_SUBITEM;
//	char szBuff[128];
	// Column 삽입
	m_pItem = CItemOnDisplay::CreateItemsOnDisplay();		//Registry의 [DisplayItem]에서 표기할 Column을 읽는다.
/*	for(int i = 0; i < CItemOnDisplay::GetItemNum(); i++)
	{
		m_wndChannelList.InsertColumn(i,
									  m_pItem[i].GetTitle(), 
									  LVCFMT_CENTER, 
									  m_pItem[i].GetWidth(), 
									  i);
//		col.cx =m_pItem[i].GetWidth();
//		col.iSubItem = i;
//		sprintf(szBuff, "%s", m_pItem[i].GetTitle());
//		col.pszText = szBuff;
//		col.cchTextMax = 128;
//		m_wndChannelList.InsertColumn(i, &col);
	}
*/


	RemakeChannelList();
	ReDrawChListCtrl(m_nCurrentModuleID);
	UpdateChListData();

	m_wndChannelList.SetHeadBitmap(IDB_LIST_HEAD);
	m_wndChannelList.SetBodyBitmap(IDB_LIST_BODY);
	m_wndChannelList.SetTailBitmap(IDB_LIST_TAIL);

	//channel 첫번째 선택		ljb add 2008-10-16
	m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);

}

// Added by 224 (2014/12/05) :
// 비정상 종료 후 재시작시 최종공정 자동 실행하게 구성
// 최초 시작 시 에만 한번 실행한다.
void CCTSMonProView::CheckPrevProcessStatusAndReprocess()
{
	static BOOL bFirstTime = TRUE ;
	if (!bFirstTime)
		return ;

	bFirstTime = FALSE ;

	CCTSMonProDoc *pDoc = GetDocument();

	CString strSection ;
	CString strDBName ;
	BOOL bIsModbusProcessing = FALSE ;
	INT nDevID = -1 ;
	
	// MOD0CH1
	strSection  = "MOD0CH1" ;
	bIsModbusProcessing = (0 == AfxGetApp()->GetProfileInt(strSection, "EndFlag", 1)) && (1 == AfxGetApp()->GetProfileInt(strSection, "Type", 1)) ;
	strDBName = AfxGetApp()->GetProfileString(strSection, "Name", "") ;
	nDevID = AfxGetApp()->GetProfileInt(strSection, "DEVID", -1) ;
	if (bIsModbusProcessing && !strDBName.IsEmpty() && nDevID != -1)
	{
		MODBUS_INFO info ;
		ZeroMemory(&info, sizeof(MODBUS_INFO)) ;
		
		CModbusConvDlg::GetModbusCommInfo(strDBName, &info) ;
		
		CArray<PMCC_INFO, PMCC_INFO> mcc_array ;
		CModbusConvDlg::GetModbusConvInfo(strDBName, mcc_array) ;
		
		BOOL bReturn = 
			pDoc->m_ModbusCanConv[nDevID].BeginModbusCanConvThread(nDevID
																 , &info
																 , strDBName
																 , mcc_array) ;
		
		// 연결 실패한 경우는 메모리를 삭제한다.
		if (!bReturn)
		{
			for (INT nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
			{
				MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
				delete ptr ;
			}
		}
		
		// 배열 클리어.
		mcc_array.RemoveAll() ;
	}
	
	// MOD0CH2
	strSection  = "MOD0CH2" ;
	bIsModbusProcessing = (0 == AfxGetApp()->GetProfileInt(strSection, "EndFlag", 1)) && (1 == AfxGetApp()->GetProfileInt(strSection, "Type", 1)) ;
	strDBName = AfxGetApp()->GetProfileString(strSection, "Name", "") ;
	nDevID = AfxGetApp()->GetProfileInt(strSection, "DEVID", -1) ;
	if (bIsModbusProcessing && !strDBName.IsEmpty() && nDevID != -1)
	{
		MODBUS_INFO info ;
		ZeroMemory(&info, sizeof(MODBUS_INFO)) ;
		
		CModbusConvDlg::GetModbusCommInfo(strDBName, &info) ;
		
		CArray<PMCC_INFO, PMCC_INFO> mcc_array ;
		CModbusConvDlg::GetModbusConvInfo(strDBName, mcc_array) ;
		
		BOOL bReturn = 
			pDoc->m_ModbusCanConv[nDevID].BeginModbusCanConvThread(nDevID
			, &info
			, strDBName
			, mcc_array) ;
		
		// 연결 실패한 경우는 메모리를 삭제한다.
		if (!bReturn)
		{
			for (INT nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
			{
				MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
				delete ptr ;
			}
		}
		
		// 배열 클리어.
		mcc_array.RemoveAll() ;
	}
	
	// MOD1CH1
	strSection  = "MOD1CH1" ;
	bIsModbusProcessing = (0 == AfxGetApp()->GetProfileInt(strSection, "EndFlag", 1)) && (1 == AfxGetApp()->GetProfileInt(strSection, "Type", 1)) ;
	strDBName = AfxGetApp()->GetProfileString(strSection, "Name", "") ;
	nDevID = AfxGetApp()->GetProfileInt(strSection, "DEVID", -1) ;
	if (bIsModbusProcessing && !strDBName.IsEmpty() && nDevID != -1)
	{
		MODBUS_INFO info ;
		ZeroMemory(&info, sizeof(MODBUS_INFO)) ;
		
		CModbusConvDlg::GetModbusCommInfo(strDBName, &info) ;
		
		CArray<PMCC_INFO, PMCC_INFO> mcc_array ;
		CModbusConvDlg::GetModbusConvInfo(strDBName, mcc_array) ;
		
		BOOL bReturn = 
			pDoc->m_ModbusCanConv[nDevID].BeginModbusCanConvThread(nDevID
			, &info
			, strDBName
			, mcc_array) ;
		
		// 연결 실패한 경우는 메모리를 삭제한다.
		if (!bReturn)
		{
			for (INT nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
			{
				MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
				delete ptr ;
			}
		}
		
		// 배열 클리어.
		mcc_array.RemoveAll() ;
	}
	
	// MOD1CH2
	strSection  = "MOD1CH2" ;
	bIsModbusProcessing = (0 == AfxGetApp()->GetProfileInt(strSection, "EndFlag", 1)) && (1 == AfxGetApp()->GetProfileInt(strSection, "Type", 1)) ;
	strDBName = AfxGetApp()->GetProfileString(strSection, "Name", "") ;
	nDevID = AfxGetApp()->GetProfileInt(strSection, "DEVID", -1) ;
	if (bIsModbusProcessing && !strDBName.IsEmpty() && nDevID != -1)
	{
		MODBUS_INFO info ;
		ZeroMemory(&info, sizeof(MODBUS_INFO)) ;
		
		CModbusConvDlg::GetModbusCommInfo(strDBName, &info) ;
		
		CArray<PMCC_INFO, PMCC_INFO> mcc_array ;
		CModbusConvDlg::GetModbusConvInfo(strDBName, mcc_array) ;
		
		BOOL bReturn = 
			pDoc->m_ModbusCanConv[nDevID].BeginModbusCanConvThread(nDevID
			, &info
			, strDBName
			, mcc_array) ;
		
		// 연결 실패한 경우는 메모리를 삭제한다.
		if (!bReturn)
		{
			for (INT nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
			{
				MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
				delete ptr ;
			}
		}
		
		// 배열 클리어.
		mcc_array.RemoveAll() ;
	}

}


//Timer에 의해 주기적으로 Module List를 Update 한다.
void CCTSMonProView::OnUpdateModuleList()
{
 	//m_ctrlStop.EnableWindow(UpdateSelectedChList(SFT_CMD_PAUSE, FALSE));
 	//m_ctrlNextStep.EnableWindow(UpdateSelectedChList(SFT_CMD_PAUSE, FALSE));

	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	CString strModuleID, strLineState, strTime,strIp;
	int nTotlCh, nRunCh;

	//m_wndModuleList.SetRedraw(FALSE);

	BOOL bStateChanged = FALSE;
	CCyclerModule *pMD;
	int nModuleID,nStartCh;

	for ( UINT nI = 0; nI < pDoc->GetInstallModuleCount(); nI++ )
	{
		nModuleID=pDoc->GetModuleID(nI);
		pMD = pDoc->GetModuleInfo(nModuleID);
		if(pMD == NULL)	break;
		
		nStartCh = GetStartChNo(nModuleID);	
		if( pMD->GetState() != PS_STATE_LINE_OFF )
		{
			//접속 상태 표시 문자
			strLineState = _T(Fun_FindMsg("OnUpdateModuleList_msg1","IDD_CTSMonPro_FORM"));
			//@ strLineState = _T("연결됨");
			//동작 시간표시 
			//strTime = pMD->GetConnectedTime();
			strTime = pMD->GetConnectionElapsedTime();
			strIp =  pMD->GetIPAddress();
			m_wndModuleList.SetItemText(nI, 7, strIp);		//ljb 201012

			// Added by 224 (2014/12/05) :
			// 비정상 종료 후 재시작시 최종공정 자동 실행하게 구성
			// 최초 시작 시 에만 한번 실행한다.
			
			static BOOL bIsFirstTime = TRUE ;
			
			if (bIsFirstTime)
			{
				CheckPrevProcessStatusAndReprocess() ;
				
				bIsFirstTime = FALSE ;
			}

		}
		else
		{			
			strLineState = _T(Fun_FindMsg("OnUpdateModuleList_msg2","IDD_CTSMonPro_FORM"));	
			//@ strLineState = _T("연결 끊김");	
			strTime = "0Day 00:00:00";

		}
		//깜밖임 제거를 위해 변했을 때만 Update 함 
		if(m_wndModuleList.GetItemText(nI, 1) != strLineState)
		{
			bStateChanged = TRUE;
		}
				
		m_wndModuleList.SetItemText(nI, 1, strLineState);
		m_wndModuleList.SetItemText(nI, 2, strTime);

		GetDlgItem(IDC_STATIC_INFO_CONN)->SetWindowText(strLineState);
					
		nTotlCh = pMD->GetTotalChannel();
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
		{
			if (nTotlCh > 2) nTotlCh = 2;	//ljb 20170710 add 표시 채널수 변경 //ksj 20180212 : 주석처리.
		}
//#endif
		nRunCh = 0;
		for(int ch =0; ch<nTotlCh; ch++)
		{
			CCyclerChannel *lpChannelInfo = pMD->GetChannelInfo(ch);
			if(lpChannelInfo == NULL)	break;

			lpChannelInfo->m_iCh=nStartCh+ch;
			WORD state = lpChannelInfo->GetState();
			
			// PS_STATE_IDLE / PS_STATE_STANDBY	
			if(  state == PS_STATE_RUN || state == PS_STATE_PAUSE || state == PS_STATE_MAINTENANCE)
			{
				nRunCh++;
			}
		}
		strTime.Format("%d", nTotlCh);
		m_wndModuleList.SetItemText(nI, 3, strTime);
		strTime.Format("%d", nTotlCh-nRunCh);
		m_wndModuleList.SetItemText(nI, 4, strTime);
		strTime.Format("%d", nRunCh);
		m_wndModuleList.SetItemText(nI, 5, strTime);
	}
	//m_wndModuleList.SetRedraw(TRUE);
	if(bStateChanged)
	{
		m_wndModuleList.Invalidate();
		m_ctrlLabelChState.SetText(strLineState);
	}
}

void CCTSMonProView::RemakeChannelList()
{
	LockUpdate();

	ASSERT(m_pItem);

	CString strTitle;
	
	int nColumnCount = m_wndChannelList.GetHeaderCtrl()->GetItemCount();
	// Delete all of the columns.
	for (int i=0;i < nColumnCount;i++)
	{
	   m_wndChannelList.DeleteColumn(0);
	}

	CItemOnDisplay::DeleteAllItemOnDisplay(m_pItem);
	
	m_pItem = CItemOnDisplay::CreateItemsOnDisplay();
	for(int  nI = 0; nI < CItemOnDisplay::GetItemNum(); nI++)
	{
		strTitle = GetDocument()->GetDataUnit(m_pItem[nI].GetItemNo());
		if(strTitle.IsEmpty())	
		{
			strTitle = m_pItem[nI].GetTitle();
		}
		else
		{
			strTitle.Format("%s(%s)", m_pItem[nI].GetTitle(), GetDocument()->GetDataUnit(m_pItem[nI].GetItemNo()));
		}
		
		m_wndChannelList.InsertColumn(nI+1,
									  strTitle, 
									  LVCFMT_CENTER, 
									  m_pItem[nI].GetWidth(), 
									  nI);
	}

	//ReDraw Channel List
//	UpdateChListData();

	UnLockUpdate();
}

void CCTSMonProView::OnDblclkListModule(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
//	int nItem = pDispInfo->item.iItem;
//    long index = pDispInfo->item.iItem;
//    long subItem = pDispInfo->item.iSubItem;
//    long objCode = pDispInfo->item.lParam;


	//선택 list의 모듈 ID를 구함 
//	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
//	if( pos == NULL ) 		return;
//	int nNewModuleID = m_wndModuleList.GetItemData(m_wndModuleList.GetNextSelectedItem(pos));
//
//	//선택에 변화 없음
//	if( nNewModuleID != m_nCurrentModuleID )
//	{
//		SetModuleSelChanged(nNewModuleID);
//	}
	*pResult = 0;
}

void CCTSMonProView::OnUpdateChannelList()
{	
	UpdateChListData();
}

//교정 완료 명령 수신시 
LRESULT CCTSMonProView::OnSetCalResultData(WPARAM wParam, LPARAM lParam)
{
	TRACE("Module %d Clibration result data received\n", (int)wParam);
	GetDocument()->SetCalResultData((int)wParam, (char *)lParam);
	return 1;
}

//ljb 20170731 Step Info reply 수신시 
LRESULT CCTSMonProView::OnSetStepInfoData(WPARAM wParam, LPARAM lParam)
{
	//TRACE("Module %d Clibration result data received\n", (int)wParam);
	//GetDocument()->SetStepInfoResultData((int)wParam, (char *)lParam);

	//ksj 20170809 : 채널 정보 추가.
	GetDocument()->SetStepInfoResultData(LOWORD(wParam),HIWORD(wParam),(char *)lParam);
	return 1;
}
//ljb 20170731 Safety Info reply 수신시 
LRESULT CCTSMonProView::OnSetSafetyInfoData(WPARAM wParam, LPARAM lParam)
{
	//TRACE("Module %d Clibration result data received\n", (int)wParam);
	SFT_CMD_SAFETY_UPDATE_REPLY safetyInfoData;
	CopyMemory(&safetyInfoData, (char *)lParam, sizeof(SFT_CMD_SAFETY_UPDATE_REPLY));
	GetDocument()->SetSafetyInfoResultData((int)wParam, (char *)lParam);
	return 1;
}

//비상 정지 명령 수신 
LRESULT CCTSMonProView::OnModuleEmergency(WPARAM wParam, LPARAM lParam)
{
	TRACE("Module %d emergency data received\n", (int)wParam);
	SFT_EMG_DATA *pEmgData = (SFT_EMG_DATA *)lParam;
	if(pEmgData == NULL)	return 0;

	CString strTemp,strCanName;
	strCanName.Empty();
	CCTSMonProDoc *pDoc = GetDocument();
	if (pEmgData->lCode == 27) //CAN Warning(27)
	{
		strCanName.Format("%s", pEmgData->szName);
		strTemp.Format(Fun_FindMsg("OnModuleEmergency_msg1","IDD_CTSMonPro_FORM"), 
			//@ strTemp.Format("%s에서 채널(%d)에 %s : %s  발생하였습니다.", 
			pDoc->GetModuleName((int)wParam), pEmgData->lValue,pDoc->GetEmgMsg(pEmgData->lCode), strCanName );
	}
	else if (pEmgData->lCode == 28)	//CAN Warning(27)
	{
		strCanName.Format("%s", pEmgData->szName);
		strTemp.Format(Fun_FindMsg("OnModuleEmergency_msg2","IDD_CTSMonPro_FORM"), 
			//@ strTemp.Format("%s에서 채널(%d)에 %s : %s  발생하였습니다.", 
			pDoc->GetModuleName((int)wParam), pEmgData->lValue, pDoc->GetEmgMsg(pEmgData->lCode), strCanName );
	}
	else if(pEmgData->lCode == 25 || pEmgData->lCode == 26)//lmh 20111111 경알람 add
	{
		strTemp.Format(Fun_FindMsg("OnModuleEmergency_msg3","IDD_CTSMonPro_FORM"), 
			//@ strTemp.Format("%s에서 경 알람이 발생하였습니다. [%s(Value : %d)]", 
			pDoc->GetModuleName((int)wParam), pDoc->GetEmgMsg(pEmgData->lCode), pEmgData->lValue);
	}	
	else
	{
		strTemp.Format(Fun_FindMsg("OnModuleEmergency_msg4","IDD_CTSMonPro_FORM"), 
			//@ strTemp.Format("%s에서 비상 상태가 발생하였습니다. [%s(Value : %d)]", 
			pDoc->GetModuleName((int)wParam), pDoc->GetEmgMsg(pEmgData->lCode), pEmgData->lValue);
	}
	pDoc->WriteSysEventLog(strTemp);	//ljb 201012
	pDoc->WriteSysLog(strTemp);

	//작업중인 채널의 작업로그에 기록 
	CCyclerModule *pModule = pDoc->GetModuleInfo((int)wParam);
	if(pModule)
	{
		for(UINT i=0; i<pModule->GetTotalChannel(); i++)
		{
			CCyclerChannel *pChInfo = pModule->GetChannelInfo(i);
			if(	pChInfo->GetState() != PS_STATE_IDLE && 
				pChInfo->GetState() != PS_STATE_STANDBY &&
				pChInfo->GetState() != PS_STATE_READY &&
				pChInfo->GetState() != PS_STATE_END
			)
			{
				pChInfo->WriteLog(strTemp);
			}
		}
	}


	//ksj 20200907 : [#JSonMon] EMG 상태 JSON 파일 생성
	//EMG 창 띄우기전에 먼저 생성 시킨다.
	if(m_pJsonMon)
	{
		int nModuleID = (int)wParam;
		m_pJsonMon->CreateEmgMonFile(nModuleID, pEmgData);
	}
	//ksj end

	//2014.09.26 dlg(this)수정
	//CShowEmgDlg dlg;
	CShowEmgDlg dlg(this);		
	
	CString strErrorMsg;
	if(pEmgData->lCode == 1001){ //모니터에서 개인적인 알람코드로 사용. mdb 추가없이 추가.
		strErrorMsg = Fun_FindMsg("OnModuleEmergency_msg5","IDD_CTSMonPro_FORM");
		//@ strErrorMsg = "보조전압 이상";
	}else{
		strErrorMsg = pDoc->GetEmgMsg(pEmgData->lCode);
	}

	dlg.SetEmgData(pDoc->GetModuleName((int)wParam), strErrorMsg, pEmgData->lValue, strCanName);

	dlg.DoModal();
	return 1;
	//MessageBox(strTemp, "비상상태", MB_ICONSTOP|MB_OK);
}

//채널의 결과 Data가 전송되었다는 Message
//wParam : Modue No
//lParam : char[SFT_MAX_CH_PER_MD] => 각 채널의 data 저장 근거(시간, 전압, 온도 StepEnd등...)
LRESULT CCTSMonProView::OnChannelDataReceived(WPARAM wParam, LPARAM lParam)
{
	//	TRACE("Channel result data received\n");
	int nModuleID = (int)wParam;
	
	GetDocument()->SetChannelData(nModuleID, (char *)lParam, 0);
	return 1;
}

//Channel data를 msec로 저장할 경우
//lParam 구조
// { SFT_MSEC_CH_DATA_INFO + SFT_MSEC_CH_DATA_INFO.lDataCount * SFT_MSEC_CH_DATA }
LRESULT CCTSMonProView::OnMSecChannelDataReceived(WPARAM wParam, LPARAM lParam)
{
	int nModuleID = LOWORD(wParam);
	int nChannNo = HIWORD(wParam);

	//data 부분 저장 
	CCyclerModule *pMD =GetDocument()->GetModuleInfo(nModuleID);
	if(pMD)
	{
		//step의 msec data 저장
		CCyclerChannel *pCh = pMD->GetChannelInfo(nChannNo-1);
		if(pCh)
		{
			CChannel *pNetCh = pCh->GetChannel();
			if(pNetCh != NULL)
			{
				SFT_MSEC_CH_DATA_INFO *pMiliSecInfo;
				pMiliSecInfo =  &pNetCh->m_StepStartDataInfo;

				//data header 분리 
				CString strTemp;
				strTemp.Format("%s Ch%d step %d received MSEC data. (Cyc %d, Data point %d)", GetDocument()->GetModuleName(nModuleID), 
					nChannNo, pMiliSecInfo->lStepNo ,pMiliSecInfo->lTotCycNo, pMiliSecInfo->lDataCount);
				pCh->WriteLog(strTemp);
				GetDocument()->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);				
			}

//#ifdef _DEBUG			
//			if(nChannNo == 1)
//			{
//				static int a = 1;
//				TRACE("2[%03d] ===> Data save start\n", a++);
//			}
//#endif
			
			if(pCh->SaveStepMiliSecData() == FALSE)
			{
				CString strTemp;
				strTemp.Format("%s Ch%d step MSEC data save fail.", GetDocument()->GetModuleName(nModuleID), nChannNo);
				pCh->WriteLog(strTemp);
				GetDocument()->WriteSysLog(strTemp);
			}
		}
	}
	return 1;
}

//20130514 add Module에서 SBC Ready 메시지
LRESULT CCTSMonProView::OnCmdReadyRun(WPARAM wParam, LPARAM lParam)
{
	int nModuleID = (UINT)wParam;
	GetDocument()->Fun_ReadyFromSbc(nModuleID,1);// 작업 시작 OK
	return 1;
}

//20130514 add Module에서 SBC Ready 메시지
LRESULT CCTSMonProView::OnCmdReadyNgRun(WPARAM wParam, LPARAM lParam)
{
	int nModuleID = (UINT)wParam;
	GetDocument()->Fun_ReadyFromSbc(nModuleID,2);//  작업 시작 NG
	return 1;
}

BOOL FindReg(CString strFindKey)
{
	HKEY hKey = NULL;
	DWORD dwindex = 0;
	DWORD dwSize = 0;
	DWORD cbName = MAX_PATH;
	long lStatus;
	TCHAR szKeyName[MAX_PATH];
	TCHAR szKeyInfo[MAX_PATH];
	BOOL bFindReg = FALSE;

	CString strSubKey = "Software\\PNE CTSPack\\CTSMonPro\\Config";
	CString ErrorMsg;

	//HKEY_CURRENT_USER\Software\PNE CTSPack\CTSMonPro\Config
	if (RegOpenKeyEx(HKEY_CURRENT_USER, strSubKey, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS)
	{
	}
	else
	{
		while(lStatus = RegEnumKeyEx(hKey,dwindex,szKeyName,&cbName,NULL,NULL,NULL,NULL) != ERROR_NO_MORE_ITEMS)
		{
			if (lStatus == ERROR_SUCCESS)
			{
				HKEY hItem = NULL;
				if(RegOpenKeyEx(hKey, szKeyName,0,KEY_READ,&hItem) != ERROR_SUCCESS)
				{

				}
				else
				{
					ZeroMemory(szKeyInfo, MAX_PATH);
					dwSize = sizeof(szKeyInfo);
					lStatus = RegQueryValueEx(hItem,NULL,NULL,NULL,(LPBYTE)szKeyInfo,&dwSize);
					if(lStatus == ERROR_SUCCESS)
					{
						if(strFindKey == szKeyInfo)
						{
							bFindReg = TRUE;
						}
					}
				}
				RegCloseKey(hItem);
			}
		}
		dwindex++;
		cbName = MAX_PATH;
	}
	RegCloseKey(hKey);

	return bFindReg;
}

//Module이 접속 되었다는 Message
LRESULT CCTSMonProView::OnModuleConnected(WPARAM wParam, LPARAM lParam)
{
	//접속된 Module의 config 정보
	LPSFT_MD_SYSTEM_DATA lpModuleConfig = (LPSFT_MD_SYSTEM_DATA )lParam;
	int nModuleID = (UINT)wParam;
	GetDocument()->SetModuleLineOn(nModuleID, lpModuleConfig);
	
	if(nModuleID == m_nCurrentModuleID)
	{
		ReDrawGrid();
		ReDrawChListCtrl(nModuleID);
	}
	OnUpdateModuleList();

	//ksj 20200907 : [#JSonMon] SBC 접속시 JSON에 상태 업데이트
	if(m_pJsonMon)	m_pJsonMon->UpdateJsonMon(nModuleID);
	//ksj end

	//ksj 20201013 : UI 프로토콜 호환모드 표시
	CString strTemp;
	CString strVersion;
	CMainFrame* pMainFrm = (CMainFrame*)AfxGetMainWnd();

	if(lpModuleConfig->nProtocolVersion != _SFT_PROTOCOL_VERSION)
	{
		if(lpModuleConfig->nProtocolVersion == _SFT_PROTOCOL_VERSION_1015)
		{
			//V1015 호환성 모드 동작 가능.
			
			if(pMainFrm)
			{				
				strTemp.Format("%s [compatibility mode : 0x%04x]", pMainFrm->m_strVersion, lpModuleConfig->nProtocolVersion);
				pMainFrm->SetWindowText(strTemp);
			}			 
		}
		else
		{
			 //버전 불일치 호환 불가 오작동 가능
			strTemp.Format("Protocol Version does not match S/W:0x%04x F/W:0x%04x", _SFT_PROTOCOL_VERSION, lpModuleConfig->nProtocolVersion);
			MessageBox(strTemp,"Protocol Error",MB_OK|MB_ICONERROR);
		}
		
	}	
	else //기본 버전정보로 갱신
	{
		if(pMainFrm)
		{		
			pMainFrm->SetWindowText(pMainFrm->m_strVersion);
		}			 
	}
	//ksj end

	//채널이 Run인 상태가 있으면 이전 최종 시험정보를 Loading한다.
	//접속과 동시에 하면 안되고 채널 상태가 최소 한번 보고 된후 해야 한다.
	//GetDocument()->CheckRunningChannel(nModuleID);

	//접속 횟수 Update
	for(int nI = 0; nI < m_wndModuleList.GetItemCount(); nI++ )
	{
		int nMD = m_wndModuleList.GetItemData(nI);
		if(nMD == nModuleID)
		{
			long lCnt = atol(m_wndModuleList.GetItemText(nI, 6))+1;
			CString str;
			str.Format("%d", lCnt);
			m_wndModuleList.SetItemText(nI, 6, str);

			str.Format("SystemType: %d, ProtocolVer: %x, OSver: %d, ModelName: %s"
				,lpModuleConfig->nSystemType,lpModuleConfig->nProtocolVersion,lpModuleConfig->nOSVersion,lpModuleConfig->szModelName);
			m_wndModuleList.SetItemText(nI, 9, str);

			CCyclerModule * pMD = GetDocument()->GetModuleInfo(nModuleID);
			int nChCount = pMD->GetTotalChannel();
			for (int i = 0; i < nChCount; i++)
			{
				CString strFindReg;
				strFindReg.Format("CH%d_Isolation_use", i+1);
				if(FindReg(strFindReg) != TRUE)
				{
					AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, strFindReg, 0);
				}	
				}

			//ksj 20200703 : 모듈 접속 시간 저장.		
			CCTSMonProDoc* pDoc = (CCTSMonProDoc*) GetDocument();
			if(pMD)
			{
				pMD->m_nConnectCount++; //접속 횟수 카운트 추가
				pMD->m_timeConnected = COleDateTime::GetCurrentTime();
				CString strTemp;
				strTemp.Format("MD%d ConnectedTime : %s", nModuleID, pMD->m_timeConnected.Format("%Y-%m-%d %H:%M:%S"));
				pDoc->WriteLog(strTemp);

				for (int i = 0; i < pMD->GetTotalChannel(); i++)
				{
					//ksj 20200703 : 채널로그에 접속시간 저장.
					CCyclerChannel* pCH = pMD->GetChannelInfo(i);
					if(pCH)	pCH->WriteLog(strTemp);		
			}
			}
			//ksj end

			break;
		}
	}

	return 1;
}

//Module이 접속이 종료 되었다는 Message 
LRESULT CCTSMonProView::OnModuleDisconnected(WPARAM wParam, LPARAM lParam)
{
	GetDocument()->ResetModuleLineState((UINT)wParam);
	OnUpdateModuleList();


	//ksj 20200703 : 모듈 통신 끊어지는 경우 해당 시간 저장.
	int nModuleID = (UINT)wParam;
	CCTSMonProDoc* pDoc = (CCTSMonProDoc*) GetDocument();
	CCyclerModule *pModule = pDoc->GetModuleInfo(nModuleID);
	if(pModule)
	{
		pModule->m_timeDisconnected = COleDateTime::GetCurrentTime();

		CString strTemp, strTemp2;
		CString strRecentCommLog; //ksj 20201115
		strTemp.Format("MD%d DisconnectedTime : %s", nModuleID, pModule->m_timeDisconnected.Format("%Y-%m-%d %H:%M:%S"));
		pDoc->WriteLog(strTemp);

		int nUseRecentCommLog = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseRecentCommLog", 1); //ksj 20201115 : 통신 두절 시점에 가장 마지막으로 송/수신 했던 커맨드 시간과 통신 두절 시간차이 계산하여 로그 남기도록 기능 추가
		if(nUseRecentCommLog)
		{
			//ksj 20201115 : 로그 추가.////////////
			//통신 두절 직전까지 최종 주고 받은 명령이 어떤것이었고, 이후 통신 두절까지 몇초 걸렸는지 로그 기록 추가.
			//SBC가 Timeout으로 통신 두절시키는 현상 체크용.		
			CModule* pLowModule = ::SFTGetModule(nModuleID);
			if(pLowModule)
			{
				SYSTEMTIME tm, tm2;
				int nRecentCmdId;
				tm2 = pLowModule->GetStdDisconnectTime();
				double dDiff = 0.0f;

				//통신두절 계산 기준 시간. 현재
				strTemp2.Format("disconnect calc std time ::::: %d-%02d-%02d %02d:%02d:%02d.%03d\n",
					tm2.wYear, tm2.wMonth, tm2.wDay,
					tm2.wHour, tm2.wMinute, tm2.wSecond, tm2.wMilliseconds);				
				strRecentCommLog += strTemp2;

				//가장 최근 port1에서 sendcommand한 시간 (send command를 거치지 않는 전송은 기록되지 않는다)
				nRecentCmdId = pLowModule->GetRecentSendCmdID();
				tm = pLowModule->GetRecentSendCmdTime();			
				dDiff = pDoc->CompareSystemTime(&tm2,&tm);
				if(nRecentCmdId != 0)
				{
					strTemp2.Format("Recent SendCommand time(PC->SBC) ::::: cmdid:0x%04x, time:%d-%02d-%02d %02d:%02d:%02d.%03d, diff:%.0fs\n",
						nRecentCmdId,
						tm.wYear, tm.wMonth, tm.wDay,
						tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds,
						dDiff);
					strRecentCommLog += strTemp2;		
				}


				//가장 최근 port1에서 ack 수신한 시간
				nRecentCmdId = pLowModule->GetRecentRecvAckCmdID1();
				tm = pLowModule->GetRecentRecvAckTime1();		
				dDiff = pDoc->CompareSystemTime(&tm2,&tm);

				if(nRecentCmdId != 0)
				{
					strTemp2.Format("Recent port1 recv Response time(SBC->PC) ::::: cmdid:0x%04x, time:%d-%02d-%02d %02d:%02d:%02d.%03d, diff:%.0fs\n",
						nRecentCmdId,
						tm.wYear, tm.wMonth, tm.wDay,
						tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds,
						dDiff);
					strRecentCommLog += strTemp2;	
				}

				//가장 최근 port2에서 ack 수신한 시간
				nRecentCmdId = pLowModule->GetRecentRecvAckCmdID2();
				tm = pLowModule->GetRecentRecvAckTime2();	
				dDiff = pDoc->CompareSystemTime(&tm2,&tm);

				if(nRecentCmdId != 0)
				{
					strTemp2.Format("Recent port2 recv Response time(SBC->PC) ::::: cmdid:0x%04x, time:%d-%02d-%02d %02d:%02d:%02d.%03d, diff:%.0fs\n",
						nRecentCmdId,
						tm.wYear, tm.wMonth, tm.wDay,
						tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds,
						dDiff);
					strRecentCommLog += strTemp2;		
				}

				//가장 최근 port1에서 ack 송신한 시간
				nRecentCmdId = pLowModule->GetRecentSendAckCmdID1();
				tm = pLowModule->GetRecentSendAckTime1();	
				dDiff = pDoc->CompareSystemTime(&tm2,&tm);

				if(nRecentCmdId != 0)
				{
					strTemp2.Format("Recent port1 Send Response time(PC->SBC) ::::: cmdid:0x%04x, time:%d-%02d-%02d %02d:%02d:%02d.%03d, diff:%.0fs\n",
						nRecentCmdId,
						tm.wYear, tm.wMonth, tm.wDay,
						tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds,
						dDiff);
					strRecentCommLog += strTemp2;
				}

				//가장 최근 port2에서 ack 송신한 시간
				nRecentCmdId = pLowModule->GetRecentSendAckCmdID2();
				tm = pLowModule->GetRecentSendAckTime2();		
				dDiff = pDoc->CompareSystemTime(&tm2,&tm);

				if(nRecentCmdId != 0)
				{
					strTemp2.Format("Recent port2 Send Response time(PC->SBC) ::::: cmdid:0x%04x, time:%d-%02d-%02d %02d:%02d:%02d.%03d, diff:%.0fs\n",
						nRecentCmdId,
						tm.wYear, tm.wMonth, tm.wDay,
						tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds,
						dDiff);
					strRecentCommLog += strTemp2;
				}

				//가장 최근 port1에서 Heartbit 수신한 시간
				nRecentCmdId = pLowModule->GetRecentSendHeartBeatID();
				tm = pLowModule->GetRecentSendHeartBeatTime();		
				dDiff = pDoc->CompareSystemTime(&tm2,&tm);

				if(nRecentCmdId != 0)
				{
					strTemp2.Format("Recent Heartbeat Recv time(SBC->PC) ::::: cmdid:0x%04x, time:%d-%02d-%02d %02d:%02d:%02d.%03d, diff:%.0fs\n",
						nRecentCmdId,
						tm.wYear, tm.wMonth, tm.wDay,
						tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds,
						dDiff);
					strRecentCommLog += strTemp2;
				}

				//가장 최근 port1에서 Heartbit 응답 송신한 시간
				nRecentCmdId = pLowModule->GetRecentSendHeartBeatID();
				tm = pLowModule->GetRecentSendHeartBeatTime();		
				dDiff = pDoc->CompareSystemTime(&tm2,&tm);

				if(nRecentCmdId != 0)
				{
					strTemp2.Format("Recent Heartbeat Send response time(PC->SBC) ::::: cmdid:0x%04x, time:%d-%02d-%02d %02d:%02d:%02d.%03d, diff:%.0fs\n",
						nRecentCmdId,
						tm.wYear, tm.wMonth, tm.wDay,
						tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds,
						dDiff);
					strRecentCommLog += strTemp2;
				}		

				pDoc->WriteLog(strRecentCommLog);
			}

			//ksj end//////////////////////////////
		}

		for (int i = 0; i < pModule->GetTotalChannel(); i++)
		{
			//ksj 20200703 : 채널로그에 접속시간 저장.
			CCyclerChannel* pCH = pModule->GetChannelInfo(i);
			if(!pCH) continue;
			
			pCH->WriteLog(strTemp);		
			pCH->WriteLog(strRecentCommLog);
		}
			
	}
	//ksj end

// #ifdef _DEBUG
// 	CString str;
// 	str.Format(Fun_FindMsg("OnModuleDisconnected_msg1","IDD_CTSMonPro_FORM"), (UINT)wParam);
// 	//@ str.Format("Module %d 통신 종료", (UINT)wParam);
// //	AfxMessageBox(str);
// #endif

	//ksj 20200907 : [#JSonMon] SBC 접속해제시 JSON 파일에 상태 업데이트
	//int nModuleID = (int)wParam;
	if(m_pJsonMon)	m_pJsonMon->UpdateJsonMon(nModuleID);
	//ksj end

	return 1;
}
//ksj 20201209 : Module이 접속 되고 있다는 Message
//향후 시각적으로 진행상태 표시할 수 있도록 개발 필요
//우선 팝업되는 시스템 로그 창에서 진행 상태 확인 할 수 있도록 1차 개발 진행.
LRESULT CCTSMonProView::OnModuleConnecting(WPARAM wParam, LPARAM lParam)
{
	TRACE("########## OnModuleConnecting %d, %d\n",wParam,lParam);

	CCTSMonProDoc* pDoc= (CCTSMonProDoc*) GetDocument();
	CString strTemp;
	int nModuleID = wParam;
	int nProgress = lParam;

	if(nModuleID > SFT_MAX_MODULE_NUM || nModuleID < 1)
		return 0;

	int nModuleIndex = pDoc->GetModuleIndex(nModuleID);

	if(pDoc)
	{
		if(nProgress >= 0) //정상 진행중
		{
			strTemp.Format("[Unit:%d] Connection Seq Lev %d", nModuleID, nProgress);
			pDoc->WriteSysLog(strTemp);

			switch(nProgress)
			{
			case 1: //커넥션 시퀀스 시작
				{
					strTemp.Format("[Unit:%d] Start Connection Seq...", nModuleID);
					pDoc->WriteSysLog(strTemp);

					strTemp.Format("SystemInfo request seq");
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						//strTemp.Format("충방전기(Unit%02d)에서 PC 접속시도 중...",nModuleID);
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg37","IDD_CTSMonPro_FORM"),nModuleID);
						m_pDlgConnSeq[nModuleIndex]->SetText(strTemp);
						//strTemp.Format("시스템(SysInfo) 정보 요청 단계");
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg38","IDD_CTSMonPro_FORM"));
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetPos(0);
						//m_pDlgConnSeq[nModuleIndex]->SetFlashText(TRUE);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}					
				}			
				break;
			case 2: //모듈 셋 데이터
				{
					strTemp.Format("[Unit:%d] ModuleSet Info request Seq", nModuleID);
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						//strTemp.Format("충방전기(Unit%02d)에서 PC 접속중...",nModuleID);
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg39","IDD_CTSMonPro_FORM"),nModuleID);
						m_pDlgConnSeq[nModuleIndex]->SetText(strTemp);
						//strTemp.Format("시스템(SysInfo) 정보 요청 단계");
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg40","IDD_CTSMonPro_FORM"));
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetPos(10);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}	
				}			
				break;
			case 3: //채널 어트리튜브 (병렬 정보 등)
				{
					strTemp.Format("[Unit:%d] ChAttribute Info Request Seq", nModuleID);
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						//strTemp.Format("채널설정(ChAttribute) 정보 요청 단계");
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg41","IDD_CTSMonPro_FORM"));
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetPos(30);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}
				}			
				break;
			case 4: //외부 데이터
				{
					strTemp.Format("[Unit:%d] Aux Info Request Seq", nModuleID);
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						//strTemp.Format("외부데이터 설정(AUX) 정보 요청 단계");
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg42","IDD_CTSMonPro_FORM"));
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetPos(50);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}
				}			
				break;
			case 5: //캔 리시브
				{
					strTemp.Format("[Unit:%d] Can Receive Info request Seq", nModuleID);
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						//strTemp.Format("CAN 설정 정보 요청 단계");
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg43","IDD_CTSMonPro_FORM"));
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetPos(60);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}
				}			
				break;
			case 6: //캔 트랜스밋
				{
					//strTemp.Format("[Unit:%d] 충방전기 CAN Transmit 설정 정보 요청 단계", nModuleID);
					strTemp.Format("[Unit:%d] CAN Transmit Info request Seq", nModuleID);
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						//strTemp.Format("CAN Transmit 설정 정보 요청 단계");
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg44","IDD_CTSMonPro_FORM"));
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetPos(70);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}
				}			
				break;
			case 0: //커넥션 시퀀스 성공.
				{
					//strTemp.Format("[Unit:%d] 충방전기 초기 접속 단계 성공.", nModuleID);
					strTemp.Format("[Unit:%d] Connection Sequence Success.", nModuleID);
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						//strTemp.Format("충방전기(Unit%02d)와 PC 접속이 완료되었습니다.", nModuleID);
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg45","IDD_CTSMonPro_FORM"),nModuleID);
						m_pDlgConnSeq[nModuleIndex]->SetText(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetFlashText(FALSE);
						//strTemp.Format("초기 접속 단계 성공.");
						strTemp.Format(Fun_FindMsg("CTSMonProView_msg46","IDD_CTSMonPro_FORM"));
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetPos(100);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}
				}
				break;
			default: //정의 되지 않은 상태.
				{
					strTemp.Format("[Unit:%d] Error. Undefined Connection Seq received", nModuleID);
					pDoc->WriteSysLog(strTemp);

					if(m_pDlgConnSeq[nModuleIndex])
					{
						m_pDlgConnSeq[nModuleIndex]->SetText(strTemp);
						m_pDlgConnSeq[nModuleIndex]->AddLog(strTemp);
						m_pDlgConnSeq[nModuleIndex]->SetFlashText(FALSE);
						m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
					}
				}
				break;
			}
		}
		else //접속중 실패
		{
			strTemp.Format("[Unit:%d] Connection Failure. (Code :%d)", nModuleID, nProgress);
			pDoc->WriteSysLog(strTemp);

			if(m_pDlgConnSeq[nModuleIndex])
			{
				//strTemp.Format("충방전기(Unit %d)와 PC 접속이 실패하였습니다.. (Code :%d)", nModuleID, nProgress);
				strTemp.Format(Fun_FindMsg("CTSMonProView_msg47","IDD_CTSMonPro_FORM"), nModuleID, nProgress);
				m_pDlgConnSeq[nModuleIndex]->SetFlashText(FALSE);
				m_pDlgConnSeq[nModuleIndex]->SetText(strTemp);	
				m_pDlgConnSeq[nModuleIndex]->ShowWindow(SW_SHOW);
			}
		}		
		
	}

	return 1;
}


void CCTSMonProView::OnTimer(UINT_PTR  nIDEvent) 
{
	CRect rect;
	switch (nIDEvent)
	{
	case TIMER_DISPLAY_CHANNEL:
		if(m_bUpdateLock == FALSE)
		{
			UpdateChListData();			//wndChannelList 에 정보 표시
			UpdateAuxListData();
			UpdateChInfoList();			//Static 정보표시 및 세로 채널 정보 표시
			UpdateGridData();			//사용하지 않고 있음 ( 채널 정보표시 표 형식) -> m_Grid
			UpdateCANListData();
			SetSelectedChNo(m_nCurrentModuleID);


			//UpdateAuxSafetyCond(); //ksj 20200116 : 화면에 aux fix 안전 조건 표시한다. 타이머에서 상시 갱신 할지 여부는 나중에 최종 결정.
			ChannelCodeCheck(); //ksj 20200702 : 체널코드 체크 로직 추가.
		}
		break;
	case TIMER_DISPLAY_MODULE:
		OnUpdateModuleList();
		break;
	case TIMER_DELAY_TIMER:
//		KillTimer(TIMER_DELAY_TIMER);
//		GetDocument()->m_bDelayTime = FALSE
		break;
		//20110319 KHS
	case TIMER_MONITORING:
		GetDocument()->UpdateTextMonitoringFile();
		break;
	case TIMER_DAY_CHANGE_CHECK	:	{ //ksj 20160801 SBC Raw Data 백업 기능 날짜 변경 체크.
		Fun_DayChangeCheck();		//ljb 20150522 add 날자 변경 체크
									}
		break;
	case TIMER_SBCDATADOWN_DONE_CHECK:{
		Fun_SBCDataDownDoneCheck();
									  }
		break;
	case TIMER_WINDOW_FLICKERING:{//yulee 20190531
		Fun_WindowFlickering();
		}break;
	case TIMER_ALARM_ENABLE:{
		m_AlramSoundEnable = FALSE;
		sndPlaySound(NULL, SND_ASYNC);
		KillTimer(TIMER_ALARM_ENABLE);
		}break;
	case TIMER_AUTO_UPDATE:{
		if((AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Auto Update", 1))) Fun_AutoUpdate(); //lyj 20210810 옵션 처리
		}break;

	default :
		break;
	}
}

void CCTSMonProView::OnBtnTestview() 

 {
	//선택 채널 리스트를 Update 한다.
	if(UpdateSelectedChList()  == FALSE )
	{
		AfxMessageBox(Fun_FindMsg("OnBtnTestview_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("선택된 채널이 없습니다.");
		//CTSAnal만 실행 
		return;
	}
	
	//선택채널의 결과 Data 폴더명을 모은다.
	//폴더명1\n폴더명2\n....으로 전송('\n'으로 폴더들을 구별하여 보낸다.)
 	//CString strData;
 	//CString strTemp;
	CString strSchFileName;
	CCyclerChannel *pChannel;
	//int nModuleID;
	//int nChIndex;
	for(int i =0; i<m_adwTargetChArray.GetSize(); i++)
	{
		//		pChannel = GetDocument()->GetChannelInfo(m_nCurrentModuleID, m_adwTargetChArray.GetAt(i));
		DWORD dwData = m_adwTargetChArray.GetAt(i);
		TRACE("M:%d,C:%d \n",HIWORD(dwData), LOWORD(dwData));
		pChannel = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
		if(pChannel == NULL)		return;
		strSchFileName = pChannel->GetScheduleFile();
		if(strSchFileName.IsEmpty())	return;
		CScheduleData schData;
		if(schData.SetSchedule(strSchFileName)== FALSE)
		{
			AfxMessageBox(Fun_FindMsg("OnBtnTestview_msg2","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox("스케줄 정보를 읽을 수 없습니다.");
			return;
		}
		schData.ShowContent(pChannel->GetStepNo()-1);
		break;
	}


// 	if(GetSelModuleChannel(nModuleID, nChIndex))
// 	{
// 		CCyclerChannel *pCh = GetDocument()->GetChannelInfo(nModuleID, nChIndex);
// 		if(pCh == NULL)		return;
// 		CString strSchFileName = pCh->GetScheduleFile();
// 		if(strSchFileName.IsEmpty())	return;
// 		
// 		CScheduleData schData;
// 		if(schData.SetSchedule(strSchFileName)== FALSE)
// 		{
// 			AfxMessageBox("스케줄 정보를 읽을 수 없습니다.");
// 			return;
// 		}
// 		schData.ShowContent(pCh->GetStepNo()-1);
// 	}
// 	else
// 	{
// 		AfxMessageBox("확인할 수 있는 Data가 없습니다.");
// 	}

// 	WORD wSelCh = m_awTargetChArray.GetAt(0);
// 	CCyclerChannel *pCh = GetDocument()->GetChannelInfo(m_nCurrentModuleID, wSelCh);
// 	if(pCh == NULL)		return;
// 	CString strSchFileName = pCh->GetScheduleFile();
// 	if(strSchFileName.IsEmpty())	return;
// 
// 	CScheduleData schData;
// 	if(schData.SetSchedule(strSchFileName)== FALSE)
// 	{
// 		AfxMessageBox("스케줄 정보를 읽을 수 없습니다.");
// 		return;
// 	}
// 	schData.ShowContent(pCh->GetStepNo()-1);
}

void CCTSMonProView::OnRclickListChannel(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CPoint ptMouse; 
	GetCursorPos(&ptMouse); 
	CCyclerModule *pMD;
	CCyclerChannel *pChInfo;

	
//	CMenu menu; 
//	menu.LoadMenu(IDR_POP_MENU); 
//	CMenu* popmenu = menu.GetSubMenu(0); 
//	popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
//		ptMouse.x, ptMouse.y, AfxGetMainWnd()); 

	CCTSMonProDoc *pDoc = GetDocument();
	UINT nModuleID=0,nChannelID=0;
	
	UINT mdID;
	if (m_nCurrentModuleID == 0)
	{
		nChannelID = m_nCurrentChannelNo;
		int nTotMDCnt = pDoc->GetInstallModuleCount();
		for(int s =0; s<nTotMDCnt; s++)
		{
			nModuleID = s+1;
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				if (pMD->GetTotalChannel() < nChannelID + 1)
				{
					nChannelID = nChannelID - pMD->GetTotalChannel();
				}
				else
				{
					break;
				}
			}
		}
	}
	else
	{
		nModuleID = m_nCurrentModuleID;
		nChannelID = m_nCurrentChannelNo;
	}

	pMD = GetDocument()->GetModuleInfo(nModuleID);
	if(pMD == NULL) return;
	pChInfo = pMD->GetChannelInfo(nChannelID);
	if(pChInfo == NULL) return;

// 	CCyclerModule * pMD = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 	if(pMD == NULL) return;
// 	CCyclerChannel * pChInfo = pMD->GetChannelInfo(m_nCurrentChannelNo);
// 	if(pChInfo == NULL) return;

	if(pChInfo->IsParallel() && pChInfo->m_bMaster == FALSE)
	{		
		CString strMsg;
		strMsg.Format(Fun_FindMsg("OnRclickListChannel_msg1","IDD_CTSMonPro_FORM"),
			//@ strMsg.Format("병렬모드로 설정되어 있습니다. \n모든 작업은 Master : [Ch%d] 에서 실행하세요",
			pChInfo->GetMasterChannelNum());
		AfxMessageBox(strMsg);
		return;
		
	}

	m_ControlMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
		ptMouse.x, ptMouse.y, AfxGetMainWnd()); 

	*pResult = 0;

}

void CCTSMonProView::OnNMDblclkListChannel(NMHDR *pNMHDR, LRESULT *pResult)  //yulee 20190814
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	CPoint ptMouse; 
	GetCursorPos(&ptMouse); 
	CCyclerModule *pMD;
	CCyclerChannel *pChInfo;
	int nCode=0;
	BOOL bFindDesc = FALSE;
	CString strCodeName, strCodeDesc, strMsg;


	//	CMenu menu; 
	//	menu.LoadMenu(IDR_POP_MENU); 
	//	CMenu* popmenu = menu.GetSubMenu(0); 
	//	popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
	//		ptMouse.x, ptMouse.y, AfxGetMainWnd()); 

	CCTSMonProDoc *pDoc = GetDocument();
	UINT nModuleID=0,nChannelID=0;

	UINT mdID;
	if (m_nCurrentModuleID == 0)
	{
		nChannelID = m_nCurrentChannelNo;
		int nTotMDCnt = pDoc->GetInstallModuleCount();
		for(int s =0; s<nTotMDCnt; s++)
		{
			nModuleID = s+1;
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				if (pMD->GetTotalChannel() < nChannelID + 1)
				{
					nChannelID = nChannelID - pMD->GetTotalChannel();
				}
				else
				{
					break;
				}
			}
		}
	}
	else
	{
		nModuleID = m_nCurrentModuleID;
		nChannelID = m_nCurrentChannelNo;
	}

	pMD = GetDocument()->GetModuleInfo(nModuleID);
	if(pMD == NULL) return;
	pChInfo = pMD->GetChannelInfo(nChannelID);
	if(pChInfo == NULL) return;

	// 	CCyclerModule * pMD = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
	// 	if(pMD == NULL) return;
	// 	CCyclerChannel * pChInfo = pMD->GetChannelInfo(m_nCurrentChannelNo);
	// 	if(pChInfo == NULL) return;

	if(pChInfo->IsParallel() && pChInfo->m_bMaster == FALSE)
	{		
		CString strMsg;
		strMsg.Format(Fun_FindMsg("OnRclickListChannel_msg1","IDD_CTSMonPro_FORM"),
			//@ strMsg.Format("병렬모드로 설정되어 있습니다. \n모든 작업은 Master : [Ch%d] 에서 실행하세요",
			pChInfo->GetMasterChannelNum());
		AfxMessageBox(strMsg);
		return;

	}

	if(pNMItemActivate->iSubItem == 4)
	{
		nCode = pChInfo->GetCellCode();
		if (nCode != 0)
		{
			PSCellCodeMsg(nCode, strCodeName, strCodeDesc);
			strMsg.Format("Code : %d\nMessage : %s\nDescription : %s", nCode, strCodeName, strCodeDesc);
			AfxMessageBox(strMsg);
		}
	}

	/*
	CString strMasterCh;
	CCyclerModule *lpModule = NULL;
	CCyclerChannel *lpChannelInfo;
	CString strCodeName, strCodeDesc, strMsg;
	int nCode;
	BOOL bFindDesc = FALSE;
	int nColIndex=0;
	lpModule = pDoc->GetModuleInfo(nModuleID);
	if(lpModule == NULL)	return;

	for(int i = 0 ; i < m_wndChannelList.GetItemCount() ;  i++) 
	{
		if(m_wndChannelList.GetItemState(i, LVIS_SELECTED)&LVIS_SELECTED)
		{
			if(pNMItemActivate->iItem == i)
			{
				lpChannelInfo = lpModule->GetChannelInfo(i+1);
				if(lpChannelInfo == NULL )	continue;

				nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CODE);
				if( nColIndex >= 0 )
				{
					nCode = lpChannelInfo->GetCellCode();
					if(pDoc->ValueString_CellCode(nCode,PS_CODE, &strCodeName, &strCodeDesc) == TRUE)
					{
						bFindDesc == TRUE;
					}
					if(bFindDesc == TRUE)
						strMsg.Format("Code: %d, Message:%s, Description: %s", nCode, strCodeName, strCodeDesc);

					if (strMsg != "")
						AfxMessageBox(strMsg);
				}
			}
		}
	}*/
}

void CCTSMonProView::OnCheckCond() 
{
	OnBtnTestview();
}

BOOL CCTSMonProView::DestroyWindow() 
{
//	WriteLog("Program Shutdown");


	g_Font.DeleteObject();

	return CFormView::DestroyWindow();
}

//Control server와 접속이 안되는 경우 재접속 시도 한다.
void CCTSMonProView::CloseThis(BYTE byUnitNo)
{
//	CCTSMonProDoc* pDoc = (CCTSMonProDoc*)GetDocument();

//	if( pDoc->m_pControlSystemSock )
//	{
//		delete pDoc->m_pControlSystemSock;
//		pDoc->m_pControlSystemSock = NULL;
//	}

	SetConnectionStatus(false);
}

void CCTSMonProView::SetConnectionStatus(bool bConnect)
{
	//접속 상태 LED Update
	GetDlgItem(IDC_RADIO1)->SetWindowText(bConnect ? _T("On Line") : _T("Off Line"));
	m_wndLedButton.Depress(bConnect);

	int nUsePolling, nPollingInterval;
	{// Get Registry Data
		nUsePolling = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Polling", -1);
		if( nUsePolling < 0 )
		{
			nUsePolling = 1;
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Polling", nUsePolling);
		}
		nPollingInterval = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Polling Interval", -1);
		if( nPollingInterval < 0 )
		{
			nPollingInterval = 5000;
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Polling Interval", nPollingInterval);
		}
	}

}

void CCTSMonProView::OnConfig() 
{
	CLoginManagerDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;

	CMaintenanceDlg dlg(GetDocument());
	if( dlg.DoModal() == IDOK )
	{
	}
}

void CCTSMonProView::OnCaliSetting() 
{
	CLoginManagerDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;
	
	CCTSMonProDoc* pDoc = (CCTSMonProDoc*)GetDocument();
	CCaliSetDlg dlg (pDoc->GetCalPoint());
	if( dlg.DoModal() == IDOK )
	{

	}
}

void CCTSMonProView::OnUpdateCheckCond(CCmdUI* pCmdUI) 
{

}

void CCTSMonProView::OnUpdateCaliSetting(CCmdUI* pCmdUI) 
{

}

void CCTSMonProView::OnClearState() 
{
	//1. 선택된 채널 중 Clear State 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_CLEAR)  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnClearState_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}
	
	//2. Check to user
	if(IDYES!=MessageBox(Fun_FindMsg("OnClearState_msg2","IDD_CTSMonPro_FORM"), 
		//@ if(IDYES!=MessageBox("초기화를 실행하시겠습니까?", 
						 Fun_FindMsg("OnClearState_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
						 //@ "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
		return;
	
	//3. Send Command
	SendCmdToModule(m_adwTargetChArray, SFT_CMD_CLEAR);
//	GetDocument()->SendCommand(m_nCurrentModuleID, &m_awTargetChArray, SFT_CMD_CLEAR);
}

void CCTSMonProView::OnSize(UINT nType, int cx, int cy) 
{
	//int nUseAllBtn = AfxGetApp()->GetProfileInt("Config","USE ALL BUTTON",7);//yulee 20190909 Vent 없는 장비 처리
	int nUseAllBtn = AfxGetApp()->GetProfileInt("Config","USE ALL BUTTON",15);//ksj 20200122 : vent open 추가

	BOOL bUseButton;
	int nUseBtnNum = 0;
	//int nWidthBnt = 200;
	int nWidthBnt = 180; //ksj 20200122 : 버튼 사이즈 축소

//#define nTOT_BTN_NUM 3 //ksj 20200122 : 주석처리 후 위치 이동
	BOOL bShowVentClear = FALSE;
	BOOL bShowBuzzerOff = FALSE;
	BOOL bShowEmgStop	= FALSE;
	BOOL bShowVentOpen	= FALSE; //ksj 20200122


	if(nUseAllBtn>0)
		bUseButton = TRUE;
	else if(nUseAllBtn == 0)
		bUseButton = FALSE;

	/*
	  1st Bit       2nd Bit       3rd Bit     4th bit
	Vent Close // Buzzer Stop // Emg Stop // Vent Open

	2진수 3자리를 십진수로 바꿔서 
	모두 Show : 111 : 7
	Vent close와 Buzzer stop만 show : 110 : 3
	Vent close만 Show : 100 : 1

	*/

	int binaryBtnSw[nTOT_BTN_NUM] = {0,};
	if(bUseButton == true)
	{
		/*for(int i=0; i<nTOT_BTN_NUM; i++)
		{
			binaryBtnSw[i] = nUseAllBtn%2;
			nUseAllBtn = nUseAllBtn/2;
			if(binaryBtnSw[i] == 1)
			{
				nUseBtnNum++;
				if(i == 0)
				{
					bShowVentClear = TRUE;
				}
				else if(i == 1)
				{
					bShowBuzzerOff = TRUE;
				}
				else if(i == 2)
				{
					bShowEmgStop = TRUE;
				}
				else if(i == 3) //ksj 20200122
				{
					bShowVentOpen = TRUE;
			}
		}
		}*/
		//ksj 20200122 : 상기 코드 주석처리 후 새로 구현
		//버튼 순서랑 bit flag 순서랑 다름
		bShowVentOpen	 = (nUseAllBtn & 0x08)?TRUE:FALSE;	 //첫번째 버튼이지만 bitflag는 4번째bit 참조.
		binaryBtnSw[0]  = bShowVentOpen;
		if(bShowVentOpen) nUseBtnNum++;

		bShowVentClear	= (nUseAllBtn & 0x01)?TRUE:FALSE;
		binaryBtnSw[1]  = bShowVentClear;
		if(bShowVentClear) nUseBtnNum++;

		bShowBuzzerOff	 = (nUseAllBtn & 0x02)?TRUE:FALSE;	
		binaryBtnSw[2]  = bShowBuzzerOff;
		if(bShowBuzzerOff) nUseBtnNum++;

		bShowEmgStop	 = (nUseAllBtn & 0x04)?TRUE:FALSE;
		binaryBtnSw[3]  = bShowEmgStop;
		if(bShowEmgStop) nUseBtnNum++;
		//ksj end
			
	}

	BOOL bUseVentBtn;
	if(nUseBtnNum > 0)
		bUseVentBtn = TRUE;

	CFormView::OnSize(nType, cx, cy);
	CRect rect,rectChinfo,rectAux,rectCan;

	CRect winRect;
	GetClientRect(winRect);
	
	CWnd* pWnd = NULL;
	CWnd* pWnd1 = NULL;
	CWnd* pWnd2 = NULL;

	pWnd = GetDlgItem(IDC_LABEL_CONNECT);
	if( pWnd->GetSafeHwnd() )
	{
		pWnd->ShowWindow(SW_HIDE);
	}

	pWnd = GetDlgItem(IDC_RADIO1);
	if( pWnd->GetSafeHwnd() )
	{		
		pWnd->ShowWindow(SW_HIDE);
	}

	pWnd = GetDlgItem(IDC_LABEL_INFO);
	if( pWnd->GetSafeHwnd() )
	{
		pWnd->ShowWindow(SW_HIDE);
	}

	int nSelLanguage;
	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	//Module Box
	if(nSelLanguage == 1)
	{
		pWnd = GetDlgItem(IDC_MODULE_BOX_STATIC);
		if(pWnd->GetSafeHwnd())
		{
			pWnd->GetWindowRect(rect);
			ScreenToClient(&rect);
			/*pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-620, rect.Height());*/
			//pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-(206*nUseBtnNum), rect.Height());
			pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-((nWidthBnt+6)*nUseBtnNum), rect.Height()); //ksj 20200122			
		}
	}
	else if(nSelLanguage == 2)
	{
		//yulee 20190306 //English
		pWnd = GetDlgItem(IDC_MODULE_BOX_STATIC);
		if(pWnd->GetSafeHwnd())
		{

			pWnd->GetWindowRect(rect);
			ScreenToClient(&rect);
			//pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-(206*nUseBtnNum), rect.Height());
			pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-((nWidthBnt+6)*nUseBtnNum), rect.Height()); //ksj 20200122

			//pWnd->MoveWindow(winRect.Width()*5/1000, winRect.Height()*5/1000, winRect.Width()-rect.left*2-100, rect.Height());
			//pWnd->MoveWindow(winRect.Width()*5/1000, winRect.Height()*5/1000, winRect.Width()-rect.left*2-100, winRect.Height()*120/1000);
		}
	}	//yulee 20190306 //Korean
	else if(nSelLanguage == 3)
	{
		//yulee 20190306 //English
		pWnd = GetDlgItem(IDC_MODULE_BOX_STATIC);
		if(pWnd->GetSafeHwnd())
		{
			pWnd->GetWindowRect(rect);
			ScreenToClient(&rect);
			//pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-(206*nUseBtnNum), rect.Height());
			pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-((nWidthBnt+6)*nUseBtnNum), rect.Height()); //ksj 20200122


			//pWnd->MoveWindow(winRect.Width()*5/1000, winRect.Height()*5/1000, winRect.Width()-rect.left*2-100, rect.Height());
			//pWnd->MoveWindow(winRect.Width()*5/1000, winRect.Height()*5/1000, winRect.Width()-rect.left*2-100, winRect.Height()*120/1000);
		}
	}	//yulee 20190306 //Korean

	// 	pWnd = GetDlgItem(IDC_MODULE_BOX_STATIC);
	// 	if(pWnd->GetSafeHwnd())
	// 	{
	// 		pWnd->GetWindowRect(rect);
	// 		ScreenToClient(&rect);
	// 		pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-100, rect.Height());
	// 	}

	//Module List Ctrl
	pWnd = GetDlgItem(IDC_LIST_MODULE);
	pWnd1 = GetDlgItem(IDC_MODULE_BOX_STATIC);
	pWnd2 = GetDlgItem(IDC_STATIC1);

	if(nSelLanguage == 1)
	{
		if(pWnd->GetSafeHwnd())
		{
			pWnd1->GetWindowRect(rect);
			ScreenToClient(&rect); 
			//pWnd->MoveWindow(rect.left+8, rect.top+13,winRect.Width()-rect.left*2 - 15-(206*nUseBtnNum),rect.Height()-18);
			pWnd->MoveWindow(rect.left+8, rect.top+13,winRect.Width()-rect.left*2 - 15-((nWidthBnt+6)*nUseBtnNum),rect.Height()-18); //ksj 20200122
		}
	}
	else if(nSelLanguage == 2)
	{
		if(pWnd->GetSafeHwnd())
		{
			pWnd1->GetWindowRect(rect);
			ScreenToClient(&rect); 
			//pWnd->MoveWindow(rect.left+8, rect.top+13,winRect.Width()-rect.left*2 - 15-(206*nUseBtnNum),rect.Height()-18);
			pWnd->MoveWindow(rect.left+8, rect.top+13,winRect.Width()-rect.left*2 - 15-((nWidthBnt+6)*nUseBtnNum),rect.Height()-18); //ksj 20200122
		}

		if(pWnd2->GetSafeHwnd())
		{
			CRect rectTmp;
			pWnd2->GetWindowRect(rectTmp);
			ScreenToClient(&rectTmp);
			pWnd2->MoveWindow(winRect.Width()*5/1000+15,  rect.top, rectTmp.Width(), rectTmp.Height());
		}
	}

	else if(nSelLanguage == 3)
	{
		if(pWnd->GetSafeHwnd())
		{
			pWnd1->GetWindowRect(rect);
			ScreenToClient(&rect); 
			//pWnd->MoveWindow(rect.left+8, rect.top+13,winRect.Width()-rect.left*2 - 15-(206*nUseBtnNum),rect.Height()-18);
			pWnd->MoveWindow(rect.left+8, rect.top+13,winRect.Width()-rect.left*2 - 15-((nWidthBnt+6)*nUseBtnNum),rect.Height()-18); //ksj 20200122
		}

		if(pWnd2->GetSafeHwnd())
		{
			CRect rectTmp;
			pWnd2->GetWindowRect(rectTmp);
			ScreenToClient(&rectTmp);
			pWnd2->MoveWindow(winRect.Width()*5/1000+15,  rect.top, rectTmp.Width(), rectTmp.Height()); //yulee 20190306
		}
	}


	//BOOL bUseVentBtn = AfxGetApp()->GetProfileInt("Config","USE VENT BUTTON",1);//yulee 20190909 Vent 없는 장비 처리

	if(bUseVentBtn == TRUE)
	{
		BOOL bUseAllBtn = AfxGetApp()->GetProfileInt("Config","USE ALL BUTTON",1);//yulee 20190909 Vent 없는 장비 처리 //ksj 20200122 : 주석추가. 코드 이상함 검토 필요.
		if(bUseAllBtn == FALSE)
		{
			pWnd = GetDlgItem(IDC_MODULE_BOX_STATIC);
			if(pWnd->GetSafeHwnd())
			{
				pWnd->GetWindowRect(rect);
				ScreenToClient(&rect);
				/*pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2-620, rect.Height());*/
				pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2, rect.Height());
			}

			pWnd = GetDlgItem(IDC_LIST_MODULE);
			pWnd1 = GetDlgItem(IDC_MODULE_BOX_STATIC);
			if(pWnd->GetSafeHwnd())
			{
				pWnd1->GetWindowRect(rect);
				ScreenToClient(&rect); 
				pWnd->MoveWindow(rect.left+8, rect.top+13,winRect.Width()-rect.left*2 - 20,rect.Height()-18);
			}
		}
		else
		{
			int nIDCBTNArr[nTOT_BTN_NUM] = {0,};
			int nBntCnt;
			nBntCnt = 0;


			if(nUseBtnNum > 0)
			{
				for(int i=0;i<nTOT_BTN_NUM; i++)
				{
					if(binaryBtnSw[i] == TRUE)
					{
// 						if(i == 0)
// 						{
// 							nIDCBTNArr[nBntCnt] = IDC_BTN_VENT_CLEAR;
// 							nBntCnt++;
// 						}
// 						else if(i == 1)
// 						{
// 							nIDCBTNArr[nBntCnt] = IDC_BTN_BuzzerStop;
// 							nBntCnt++;
// 						}
// 						else if(i == 2)
// 						{
// 							nIDCBTNArr[nBntCnt] = IDC_BTN_EMG;
// 							nBntCnt++;
// 						}
// 						else if(i == 3)
// 						{
// 							nIDCBTNArr[nBntCnt] = IDC_BTN_VENT_OPEN;
// 							nBntCnt++;
// 						}

						//ksj 20200122 : 버튼 순서 변경
						if(i == 0)
						{
							nIDCBTNArr[nBntCnt] = IDC_BTN_VENT_OPEN;
							nBntCnt++;
						}
						else if(i == 1)
						{
							nIDCBTNArr[nBntCnt] = IDC_BTN_VENT_CLEAR;
							nBntCnt++;
						}
						else if(i == 2)
						{
							nIDCBTNArr[nBntCnt] = IDC_BTN_BuzzerStop;
							nBntCnt++;
						}
						else if(i == 3)
						{
							nIDCBTNArr[nBntCnt] = IDC_BTN_EMG;
							nBntCnt++;
						}
						
					}
				}
			}

			CRect	rectCtrl, rectPt, rectPt2;

			if(nIDCBTNArr[0] != 0)
			{
				pWnd = GetDlgItem(nIDCBTNArr[0]);
				pWnd1 = GetDlgItem(IDC_MODULE_BOX_STATIC);

				if(pWnd->GetSafeHwnd())
				{
					pWnd->GetWindowRect(rectCtrl);
					ScreenToClient(&rectCtrl);
					pWnd1->GetWindowRect(rectPt);
					ScreenToClient(&rectPt);

					pWnd->MoveWindow(rectPt.right+5, rectPt.top+10, nWidthBnt,rectPt.bottom-20);
					//rectPt.left*2 - 15-(206*nUseBtnNum)
					//pWnd->MoveWindow(rectPt.right+5, rectPt.top+10, winRect.Width()-rect.right-433,rectPt.bottom-20);
					pWnd->GetWindowRect(rectCtrl);
				}
			}
			
			if(nIDCBTNArr[1] != 0)
			{
				pWnd = GetDlgItem(nIDCBTNArr[1]);
				pWnd1 = GetDlgItem(nIDCBTNArr[0]);
				pWnd2 = GetDlgItem(IDC_MODULE_BOX_STATIC);

				if(pWnd->GetSafeHwnd())
				{
					pWnd->GetWindowRect(rectCtrl);
					ScreenToClient(&rectCtrl);
					pWnd1->GetWindowRect(rectPt);
					ScreenToClient(&rectPt);
					pWnd2->GetWindowRect(rectPt2);
					ScreenToClient(&rectPt2);


					pWnd->MoveWindow(rectPt.right+5, rectPt2.top+10, nWidthBnt,rectPt2.bottom-20);
					pWnd->GetWindowRect(rectCtrl);
				}
			}

			if(nIDCBTNArr[2] != 0)
			{
				//IDC_BTN_BuzzerStop
				pWnd = GetDlgItem(nIDCBTNArr[2]);
				pWnd1 = GetDlgItem(nIDCBTNArr[1]);
				pWnd2 = GetDlgItem(IDC_MODULE_BOX_STATIC);

			/*CRect	rectCtrl, rectPt;*/

			if(pWnd->GetSafeHwnd())
			{
				pWnd->GetWindowRect(rectCtrl);
				ScreenToClient(&rectCtrl);
				pWnd1->GetWindowRect(rectPt);
				ScreenToClient(&rectPt);
				pWnd2->GetWindowRect(rectPt2);
				ScreenToClient(&rectPt2);


					pWnd->MoveWindow(rectPt.right+5, rectPt2.top+10, nWidthBnt,rectPt2.bottom-20);
					pWnd->GetWindowRect(rectCtrl);
				}
			}

			//ksj 20200122 : 4번째 버튼 추가
			if(nIDCBTNArr[3] != 0)
			{
				//IDC_BTN_BuzzerStop
				pWnd = GetDlgItem(nIDCBTNArr[3]);
				pWnd1 = GetDlgItem(nIDCBTNArr[2]);
				pWnd2 = GetDlgItem(IDC_MODULE_BOX_STATIC);

				if(pWnd->GetSafeHwnd())
				{
					pWnd->GetWindowRect(rectCtrl);
					ScreenToClient(&rectCtrl);
					pWnd1->GetWindowRect(rectPt);
					ScreenToClient(&rectPt);
					pWnd2->GetWindowRect(rectPt2);
					ScreenToClient(&rectPt2);


					pWnd->MoveWindow(rectPt.right+5, rectPt2.top+10, nWidthBnt,rectPt2.bottom-20);
					pWnd->GetWindowRect(rectCtrl);
		}
	}
			//ksj end
		}
	}

	pWnd = GetDlgItem(IDC_CHANNEL_BOX_STATIC);
	//channel Box
	if(nSelLanguage == 1)
	{
		if( pWnd->GetSafeHwnd() )
		{
			pWnd->GetWindowRect(rect);
			ScreenToClient(&rect);
			pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2, rect.Height());
			
			// 		if(winRect.Width() > 500)
			// 			pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2, winRect.Height()-rect.top-5);
		}
	}
	else if(nSelLanguage == 2)
	{
		if( pWnd->GetSafeHwnd() )
		{
			pWnd->GetWindowRect(rect);
			ScreenToClient(&rect);//(rect.left, rect.top, winRect.Width()-rect.left*2, rect.Height());
			pWnd->MoveWindow(winRect.Width()*5/1000, rect.top, winRect.Width()-rect.left*2, rect.Height());
		}
	}

	else if(nSelLanguage == 3)
	{
		if( pWnd->GetSafeHwnd() )
		{
			pWnd->GetWindowRect(rect);
			ScreenToClient(&rect);//(rect.left, rect.top, winRect.Width()-rect.left*2, rect.Height());
			pWnd->MoveWindow(winRect.Width()*5/1000, rect.top, winRect.Width()-rect.left*2, rect.Height());
		}
	}

	//Channel List Ctrl
	pWnd = GetDlgItem(IDC_LIST_CHANNEL);
	pWnd1 = GetDlgItem(IDC_CHANNEL_BOX_STATIC);
	pWnd2 = GetDlgItem(IDC_STATIC2);
	

	if(nSelLanguage == 1)
	{
		if(pWnd->GetSafeHwnd())
		{
			pWnd1->GetWindowRect(rect);
			ScreenToClient(&rect);
			pWnd->MoveWindow(rect.left+8, rect.top+20,winRect.Width()-rect.left*2 - 20,rect.Height()-26);
		}
		
		pWnd = GetDlgItem(IDC_LIST_LOG);
		if( pWnd->GetSafeHwnd() )
		{
			pWnd->ShowWindow(SW_HIDE);
	/*		pWnd->GetWindowRect(rect);
			ScreenToClient(&rect);

			pWnd->MoveWindow(rect.left, winRect.bottom-rect.Height()-10, winRect.Width()-rect.left-10, rect.Height());
			nHeightOffset = rect.Height()+10+10;
	*/	}
	}
	else if(nSelLanguage == 2)
	{
		if(pWnd->GetSafeHwnd())
		{
			pWnd1->GetWindowRect(rect);
			ScreenToClient(&rect);
			pWnd->MoveWindow(rect.left+8, rect.top+18,winRect.Width()-rect.left*2 - 20, rect.Height()-25);
		}
		
		pWnd = GetDlgItem(IDC_LIST_LOG);
		if( pWnd->GetSafeHwnd() )
		{
			pWnd->ShowWindow(SW_HIDE);
		}

		if(pWnd2->GetSafeHwnd())
		{
			CRect rectTmp;
			pWnd2->GetWindowRect(rectTmp);
			ScreenToClient(&rectTmp);
			pWnd2->MoveWindow(winRect.Width()*5/1000+15,  rect.top, rectTmp.Width(), rectTmp.Height()); //yulee 20190306
		}
	}

	else if(nSelLanguage == 3)
	{
		if(pWnd->GetSafeHwnd())
		{
			pWnd1->GetWindowRect(rect);
			ScreenToClient(&rect);
			pWnd->MoveWindow(rect.left+8, rect.top+18,winRect.Width()-rect.left*2 - 20, rect.Height()-25);
		}

		pWnd = GetDlgItem(IDC_LIST_LOG);
		if( pWnd->GetSafeHwnd() )
		{
			pWnd->ShowWindow(SW_HIDE);
		}

		if(pWnd2->GetSafeHwnd())
		{
			CRect rectTmp;
			pWnd2->GetWindowRect(rectTmp);
			ScreenToClient(&rectTmp);
			pWnd2->MoveWindow(winRect.Width()*5/1000+15,  rect.top, rectTmp.Width(), rectTmp.Height()); //yulee 20190306
		}
	}

	pWnd = GetDlgItem(IDC_BTN_RESET_GEN_LOG);
	if( pWnd->GetSafeHwnd() )
	{
		pWnd->ShowWindow(SW_HIDE);
/*		pWnd->GetClientRect(rect);

		pWnd->MoveWindow(winRect.Width()-rect.Width()-10, winRect.Height()-rect.top-nHeightOffset, rect.Width(), rect.Height());
*/	}

	//Channel List Ctrl
	if( m_Grid.GetSafeHwnd() )
	{
		m_Grid.GetWindowRect(rect);
		ScreenToClient(&rect);
//기존		m_Grid.MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2, winRect.Height()-rect.top-15);
		m_Grid.MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2, winRect.Height()-rect.top-15);
		//m_Grid.MoveWindow(rect.left, rect.top, winRect.Width() - rect.left - (winRect.Width() * 0.03), winRect.Height()-rect.top-15);
//		m_Grid.ExpandColumnsToFit();
//		m_Grid.ExpandRowsToFit();
		m_Grid.ExpandToFit();
	}
	
// 	//Channel Info Static Ctrl
// 	pWnd = GetDlgItem(IDC_STATIC_CHINFO);
// 	if(pWnd->GetSafeHwnd())
// 	{
// 		pWnd->GetWindowRect(rect);
// 		ScreenToClient(&rect);
// 		pWnd->MoveWindow(rect.left, rect.top,rect.Width(),winRect.Height() - rect.top-20);
// 	}
// 	
// 	//Channel Info List Ctrl
// 	pWnd = GetDlgItem(IDC_LIST_CH_INFO);
// 	if(pWnd->GetSafeHwnd())
// 	{
// 		pWnd->GetWindowRect(rect);
// 		ScreenToClient(&rect);
// 		pWnd->MoveWindow(rect.left, rect.top,rect.Width(),winRect.Height() - rect.top-30);
// 	}
	
// 	pWnd = GetDlgItem(IDC_LIST_CHANNEL);
// 	if( pWnd->GetSafeHwnd() )
// 	{
// 		pWnd->GetWindowRect(rect);
// 		ScreenToClient(&rect);
// 		pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2, rect.Height());
// 	}

//////////////////////////////////////////////////////////////////////////

	//ksj 20201124
	BOOL bUseIsolation = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolation", TRUE);
	BOOL bUseIsolationType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationType", 0);
	//ksj end

	//Channel detail Box
	pWnd1 = GetDlgItem(IDC_STATIC_CHINFO);
	if(pWnd1->GetSafeHwnd())
	{
		pWnd1->GetWindowRect(rectChinfo);
		ScreenToClient(&rectChinfo);
		//pWnd1->MoveWindow(rectChinfo.left, rectChinfo.top, rectChinfo.Width(), winRect.Height() - 110 - rectChinfo.top-10); 
				
		int nRectHeight = winRect.Height();
		if(bUseIsolation) //절연 기능 켜져있으면 채널상세정보창 더 줄인다.
		{
			nRectHeight-=180;
		}
		else
		{
			nRectHeight-=110;
		}
		pWnd1->MoveWindow(rectChinfo.left, rectChinfo.top, rectChinfo.Width(), nRectHeight - rectChinfo.top-10); //ksj 20201124
		//ksj end

		pWnd1->GetWindowRect(rectChinfo);
		ScreenToClient(&rectChinfo);
	}
	//Channel Info List Ctrl
	pWnd = GetDlgItem(IDC_LIST_CH_INFO);
	if(pWnd->GetSafeHwnd())
	{
		pWnd->MoveWindow(rectChinfo.left+5, rectChinfo.top+20, rectChinfo.Width()-10, rectChinfo.Height()-25);
	}

	if(m_bUseCbank)
	{
		pWnd1 = GetDlgItem(IDC_STATIC_CHINFO);
		pWnd = GetDlgItem(IDC_LIST_CH_INFO);
		if(pWnd->GetSafeHwnd())
		{
			pWnd1->MoveWindow(rectChinfo.left, rectChinfo.top, rectChinfo.Width(), winRect.Height() - 130 - rectChinfo.top-10); 
			pWnd->MoveWindow(rectChinfo.left+5, rectChinfo.top+20, rectChinfo.Width()-10, rectChinfo.Height()-55);
		}
	}

	
	//yulee 20181024 추가 
	CString tmpStr;
	//tmpStr.Format("결과데이터");
	tmpStr.Format(Fun_FindMsg("OnSize_msg1","IDD_CTSMonPro_FORM")); //&&
	SetDlgItemText(IDC_BTN_DATA_VIEW, tmpStr);
	//tmpStr.Format("그래프 확인");	
	tmpStr.Format(Fun_FindMsg("OnSize_msg2","IDD_CTSMonPro_FORM")); //&&
	SetDlgItemText(IDC_BTN_GRAPH_VIEW, tmpStr);	
	//tmpStr.Format("진행스케쥴");	
	tmpStr.Format(Fun_FindMsg("OnSize_msg3","IDD_CTSMonPro_FORM")); //&&
	SetDlgItemText(IDC_BTN_CHECK_COND, tmpStr);			
	//tmpStr.Format("실시간그래프");
	tmpStr.Format(Fun_FindMsg("OnSize_msg4","IDD_CTSMonPro_FORM")); //&&
	SetDlgItemText(IDC_BTN_VIEW_REALGRAPH, tmpStr);


	//ksj 20201124
	int nRectHeight = winRect.Height();
	if(bUseIsolation && bUseIsolationType) //절연 기능 켜져있으면 aux상세정보창 더 줄인다.
	{
		nRectHeight-=70;
	}
	//ksj end

//	int nAuxLeft = 0;
	pWnd = GetDlgItem(IDC_STATIC_AUX);
	
	if(pWnd->GetSafeHwnd())
	{
		pWnd->GetWindowRect(rectAux);
		ScreenToClient(&rectAux);

		if(m_bUseCan)
			//pWnd->MoveWindow(rectAux.left, rectAux.top, ((winRect.Width() - (rectChinfo.left + rectChinfo.Width()) -10) / 2) -90, winRect.Height() - rectAux.top-10);
			//pWnd->MoveWindow(rectAux.left, rectAux.top, ((winRect.Width() - (rectChinfo.left + rectChinfo.Width()) -10) / 2) -50, winRect.Height() - rectAux.top-10); //ksj 2020017
			pWnd->MoveWindow(rectAux.left, rectAux.top, ((winRect.Width() - (rectChinfo.left + rectChinfo.Width()) -10) / 2) -50, nRectHeight - rectAux.top-10); //ksj 20201124
		else
			//pWnd->MoveWindow(rectAux.left, rectAux.top, winRect.Width() - (rectChinfo.left + rectChinfo.Width()) -15, winRect.Height() - rectAux.top-10);
			pWnd->MoveWindow(rectAux.left, rectAux.top, winRect.Width() - (rectChinfo.left + rectChinfo.Width()) -15, nRectHeight - rectAux.top-10); //ksj 20201124
		
		pWnd->GetWindowRect(rectAux);
		ScreenToClient(&rectAux);
	}
	//Aux Info List Ctrl
	pWnd = GetDlgItem(IDC_LIST_AUX);
	if(pWnd->GetSafeHwnd())
	{
		CWnd* pWnd2 = NULL;
		pWnd2 = GetDlgItem(IDC_LAB_AUX_TEMP_MIN);
		CRect rectTempEdit;
		pWnd2->GetWindowRect(rectTempEdit);
		ScreenToClient(&rectTempEdit);		

		//pWnd->MoveWindow(rectAux.left+5, rectTempEdit.bottom+10, rectAux.Width()-15, winRect.Height() - rectAux.top-10 - (rectTempEdit.bottom - rectAux.top +10)); //yulee 20190306
		pWnd->MoveWindow(rectAux.left+5, rectTempEdit.bottom+10, rectAux.Width()-15, nRectHeight - rectAux.top-10 - (rectTempEdit.bottom - rectAux.top +10)); //ksj 20201124

		//pWnd->MoveWindow(rectAux.left+5, rectAux.top+110, rectAux.Width()-15, rectAux.Height()-115);
		//pWnd->MoveWindow(rectAux.left+5, rectAux.top+40, rectAux.Width()-15, rectAux.Height()-45); //dev only
		//pWnd->MoveWindow(rectAux.left+5, rectAux.top+90, rectAux.Width()-15, rectAux.Height()-95);
		
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_MIN);
// 		pWnd1->GetWindowRect(rectLabel);
// 		pWnd2 = GetDlgItem(IDC_LAB_CELL_MIN_VALUE);
// 		pWnd2->GetWindowRect(rectLabel2);
// 		pWnd1->MoveWindow(rectAux.left+15, rectAux.top+20, rectLabel.Width(), rectLabel.Height());
// 		pWnd2->MoveWindow(rectAux.left+rectLabel.Width() +20, rectAux.top+20, rectLabel2.Width(), rectLabel2.Height());
// 
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_MAX);
// 		pWnd1->MoveWindow(rectAux.left+200, rectAux.top+20, rectLabel.Width(), rectLabel.Height());
// 		pWnd2 = GetDlgItem(IDC_LAB_CELL_MIN_VALUE);
// 		pWnd2->MoveWindow(rectAux.left+rectLabel.Width() +200, rectAux.top+20, rectLabel2.Width(), rectLabel2.Height());
// 
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_DELTA);
// 		pWnd1->MoveWindow(rectAux.left+15, rectAux.top+40, rectLabel.Width(), rectLabel.Height());
// 		pWnd2 = GetDlgItem(IDC_LAB_CELL_DELTA_VALUE);
// 		pWnd2->MoveWindow(rectAux.left+rectLabel.Width() +20, rectAux.top+40, rectLabel2.Width(), rectLabel2.Height());
// 
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_SAFETY_MAX);
// 		pWnd1->GetWindowRect(rectLabel);
// 		pWnd2 = GetDlgItem(IDC_LAB_CELL_SAFETY_MAX_VALUE);
// 		pWnd2->GetWindowRect(rectLabel2);
// 		pWnd1->MoveWindow(rectAux.left+15, rectAux.top+70, rectLabel.Width(), rectLabel.Height());
// 		pWnd2->MoveWindow(rectAux.left+rectLabel.Width() +20, rectAux.top+70, rectLabel2.Width(), rectLabel2.Height());

	
// 		pWnd->GetWindowRect(rectLabel);
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_MIN);
// 		pWnd1->MoveWindow(rectAux.left+15, rectAux.top+20, rectLabel.Width(), rectLabel.Height());
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_MAX);
// 		pWnd1->MoveWindow(rectAux.left+200, rectAux.top+20, rectLabel.Width(), rectLabel.Height());
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_DELTA);
// 		pWnd1->MoveWindow(rectAux.left+15, rectAux.top+40, rectLabel.Width(), rectLabel.Height());
// 		pWnd1 = GetDlgItem(IDC_LAB_CELL_SAFETY_MAX);
// 		pWnd1->MoveWindow(rectAux.left+15, rectAux.top+70, rectLabel.Width(), rectLabel.Height());
}
	
	//Can Static Ctrl
	pWnd = GetDlgItem(IDC_STATIC_CAN);
	if(pWnd->GetSafeHwnd())
	{
		pWnd->GetWindowRect(rectCan);
		ScreenToClient(&rectCan);
		if(m_bUseAux)
		{
			//pWnd->MoveWindow(rectAux.left + rectAux.Width()+ 10, rectAux.top,winRect.Width() - rectAux.left- rectAux.Width() - 20 , winRect.Height() - rectAux.top-10);
			pWnd->MoveWindow(rectAux.left + rectAux.Width()+ 10, rectAux.top,winRect.Width() - rectAux.left- rectAux.Width() - 20 , nRectHeight - rectAux.top-10); //ksj 20201124
			GetDlgItem(IDC_LAB_CELL_SAFETY_MAX)->ShowWindow(SW_SHOW);		//20150807 add
			GetDlgItem(IDC_LAB_CELL_SAFETY_MAX_VALUE)->ShowWindow(SW_SHOW);	//20150807 add
			GetDlgItem(IDC_LAB_CELL_SAFETY_MAX_UNIT)->ShowWindow(SW_SHOW);	//20150807 add

			//ksj 20210115 
			GetDlgItem(IDC_LAB_AUX_TEMP_MAX)->ShowWindow(SW_SHOW);		
			GetDlgItem(IDC_LAB_AUX_VOLT_MAX)->ShowWindow(SW_SHOW);	
			GetDlgItem(IDC_LAB_AUX_THER_MAX)->ShowWindow(SW_SHOW);	
			GetDlgItem(IDC_LAB_AUX_THER_HUMI_MAX)->ShowWindow(SW_SHOW);	
			GetDlgItem(IDC_LAB_AUX_TEMP_MIN)->ShowWindow(SW_SHOW);		
			GetDlgItem(IDC_LAB_AUX_VOLT_MIN)->ShowWindow(SW_SHOW);	
			GetDlgItem(IDC_LAB_AUX_THER_MIN)->ShowWindow(SW_SHOW);	
			GetDlgItem(IDC_LAB_AUX_THER_HUMI_MIN)->ShowWindow(SW_SHOW);	
			//ksj end
		}
		else
		{
			//pWnd->MoveWindow(rectAux.left, rectAux.top, winRect.Width() - rectAux.left - 10, winRect.Height() - rectAux.top-10);		
			pWnd->MoveWindow(rectAux.left, rectAux.top, winRect.Width() - rectAux.left - 10, nRectHeight - rectAux.top-10);		//ksj 20201124
			GetDlgItem(IDC_LAB_CELL_SAFETY_MAX)->ShowWindow(SW_HIDE);		//20150807 add
			GetDlgItem(IDC_LAB_CELL_SAFETY_MAX_VALUE)->ShowWindow(SW_HIDE);	//20150807 add
			GetDlgItem(IDC_LAB_CELL_SAFETY_MAX_UNIT)->ShowWindow(SW_HIDE);	//20150807 add

			//ksj 20210115 
			GetDlgItem(IDC_LAB_AUX_TEMP_MAX)->ShowWindow(SW_HIDE);		
			GetDlgItem(IDC_LAB_AUX_VOLT_MAX)->ShowWindow(SW_HIDE);	
			GetDlgItem(IDC_LAB_AUX_THER_MAX)->ShowWindow(SW_HIDE);	
			GetDlgItem(IDC_LAB_AUX_THER_HUMI_MAX)->ShowWindow(SW_HIDE);	
			GetDlgItem(IDC_LAB_AUX_TEMP_MIN)->ShowWindow(SW_HIDE);		
			GetDlgItem(IDC_LAB_AUX_VOLT_MIN)->ShowWindow(SW_HIDE);	
			GetDlgItem(IDC_LAB_AUX_THER_MIN)->ShowWindow(SW_HIDE);	
			GetDlgItem(IDC_LAB_AUX_THER_HUMI_MIN)->ShowWindow(SW_HIDE);	
			//ksj end
		}
		pWnd->GetWindowRect(rectCan);
		ScreenToClient(&rectCan);
	}
	//Can List Ctrl
	pWnd = GetDlgItem(IDC_LIST_CAN_INFO);
	if(pWnd->GetSafeHwnd())
	{
		pWnd->MoveWindow(rectCan.left+5, rectCan.top+20, rectCan.Width()-10, rectCan.Height()-25);		
	}

//////////////////////////////////////////////////////////////////////////

	//Tab Control
// 	pWnd = GetDlgItem(IDC_TAB1);
// 	if( pWnd->GetSafeHwnd() )
// 	{
// 		pWnd->GetWindowRect(rect);
// 		ScreenToClient(&rect);
// 		if(winRect.Width() > 500)
// 		{
// 			int nHeight = 650;
// 			if(winRect.Height()-rect.top-10 > nHeight)	nHeight = winRect.Height()-rect.top-10;
// 			
// 			pWnd->MoveWindow(rect.left, rect.top, winRect.Width()-rect.left*2, nHeight);
// 		}
// 	}
	

	ResetFuncAllBut();
}

void CCTSMonProView::ResetFuncAllBut()
{
	/*ResetFuncBut(IDC_BTN_DATA_VIEW		, 1, 1, 4, 0);
	ResetFuncBut(IDC_BTN_GRAPH_VIEW		, 2, 1, 4, 0);
	ResetFuncBut(IDC_BTN_CHECK_COND		, 3, 1, 4, 0);
	ResetFuncBut(IDC_BTN_START			, 1, 2, 4, 0);
	ResetFuncBut(IDC_BTN_PAUSE			, 2, 2, 4, 0);
	ResetFuncBut(IDC_BTN_NEXT_STEP		, 3, 2, 4, 0);
	ResetFuncBut(IDC_BTN_STOP			, 1, 3, 4, 0);
	if(m_bUseWorkReserv) 
		ResetFuncBut(IDC_BTN_RESERV			, 2, 3, 4, 0);
	ResetFuncBut(IDC_BTN_VIEW_REALGRAPH	, 3, 3, 4, 0);
	ResetFuncBut(IDC_BUT_EDITOR			, 1, 4, 4, 0);
	ResetFuncBut(IDC_BUT_ANAL			, 2, 4, 4, 0);
	if(m_bUseCbank)
	{
		ResetFuncBut(IDC_BUT_CBANK_ON			, 1, 5, 4, 0);
		ResetFuncBut(IDC_BUT_CBANK_OFF			, 2, 5, 4, 0);
	}*/
	int nCol = 1;
	ResetFuncBut(IDC_BTN_DATA_VIEW		, nCol++, 1, 4, 0);
	ResetFuncBut(IDC_BTN_GRAPH_VIEW		, nCol++, 1, 4, 0);
	ResetFuncBut(IDC_BTN_CHECK_COND		, nCol++, 1, 4, 0);
	nCol = 1;
	ResetFuncBut(IDC_BTN_START			, nCol++, 2, 4, 0);
	ResetFuncBut(IDC_BTN_PAUSE			, nCol++, 2, 4, 0);
	ResetFuncBut(IDC_BTN_NEXT_STEP		, nCol++, 2, 4, 0);
	nCol = 1;
	ResetFuncBut(IDC_BTN_STOP			, nCol++, 3, 4, 0);
	if(m_bUseWorkReserv) 
		ResetFuncBut(IDC_BTN_RESERV			, nCol++, 3, 4, 0);
	ResetFuncBut(IDC_BTN_VIEW_REALGRAPH	, nCol++, 3, 4, 0);
	nCol = 1;
	ResetFuncBut(IDC_BUT_EDITOR			, nCol++, 4, 4, 0);
	ResetFuncBut(IDC_BUT_ANAL			, nCol++, 4, 4, 0);
	if(m_bUseCbank)
	{
		nCol = 1;
		ResetFuncBut(IDC_BUT_CBANK_ON			, nCol++, 5, 4, 0);
		ResetFuncBut(IDC_BUT_CBANK_OFF			, nCol++, 5, 4, 0);
	}
	
	//ksj 20201124 ; 절연/비절연 기능 활성화 /////////////////////////////////////////////////////////////////////////
	BOOL bUseIsolationType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationType", 0);
// 	BOOL bUseIsolationCh1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCh1", TRUE);
// 	BOOL bUseIsolationCh2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCh2", TRUE);
	BOOL bUseIsolationCh1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCh1", FALSE); //lyj 20210714 기본값 TRUE->FALSE
	BOOL bUseIsolationCh2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCh2", FALSE); //lyj 20210714 기본값 TRUE->FALSE
	if(bUseIsolationCh1)
	{
		/*lyj 20210913 절연비절연 콤보박스로 변경하여 주석처리
		if(bUseIsolationType)
		{
			//a절연, b절연 방식
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_END_A1	, 1, 6, 4, 1);
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_END_B1	, 2, 6, 4, 1);
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_START_AB1, 3, 6, 4, 1);
		}
		else*/
		{
			//그냥 절연/비절연
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_START, 1, 6, 4, 1);
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_END	, 2, 6, 4, 1);
		}
		
	}
	
	if(bUseIsolationCh2)
	{
		/*lyj 20210913 절연비절연 콤보박스로 변경하여 주석처리
		if(bUseIsolationType)
		{
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_END_A2	, 1, 7, 4, 1);
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_END_B2	, 2, 7, 4, 1);
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_START_AB2, 3, 7, 4, 1);
		}
		else*/
		{
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_START2	, 1, 7, 4, 1);
			ResetFuncBut(IDC_BTN_DAQ_ISOLATION_END2	, 2, 7, 4, 1);
		}		
	}	
	//ksj end ///////////////////////////////////////////////////////////////////////

	// 211015 HKH 절연모드 충전 관련 기능 보완
	BOOL bUseIsolation = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolation",FALSE);
	if (bUseIsolation)
	{
		ResetFuncBut(IDC_CBO_ISOL			, 1, 6, 4, 0);
		ResetFuncBut(IDC_BUT_ISOL_CHOICE	, 2, 6, 4, 0);
	}

}

//Control server에 재접속 하려는 시도 
void CCTSMonProView::OnTimerFuncPollingControlServer()
{
	if( GetDocument()->PollingControlServer() == false)
	{
		CloseThis(1);
	}
}

void CCTSMonProView::OnUpdateCmdRun(CCmdUI* pCmdUI) 
{
	//RUN 명령은 여러 모듈에 한꺼번에 전송할 수 없다. 
	//Run명령에 의해 전송된 Channel들은 모두 같은 조건을 가지고 시행 하여야 한다.
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
// 		CCyclerModule *pMD;
// 		int nModuleID;
// 		BOOL bAble = FALSE;
// 		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 		while(pos)
// 		{
// 			int item = m_wndModuleList.GetNextSelectedItem(pos);
// 			nModuleID  = m_wndModuleList.GetItemData(item);		
// 			pMD = GetDocument()->GetModuleInfo(nModuleID);
// 			if(pMD)
// 			{
// 				if(pMD->GetState() == PS_STATE_LINE_OFF)
// 				{
// 					bAble = FALSE;
// 					break;
// 				}
// 				else
// 				{
// 					bAble = TRUE;
// 				}
// 
// 			}
// 			else 
// 			{
// 				bAble = FALSE;
// 				break;
// 			}
// 		}
		pCmdUI->Enable(TRUE);	
		m_ctrlStart.EnableWindow(TRUE);
		
	}
	else
	{
#ifndef _DEBUG
		pCmdUI->Enable(UpdateUIChList(SFT_CMD_RUN, FALSE));	
		m_ctrlStart.EnableWindow(UpdateUIChList(SFT_CMD_RUN, FALSE));
#else
		pCmdUI->Enable(TRUE);
#endif

	}

/*	BOOL bAble = FALSE;

	CCyclerModule * pMD = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
	if(pMD == NULL) return;
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(m_nCurrentChannelNo);
	if(pChInfo == NULL) return;

	if(pCurrentChannel != NULL)
	{
		if(pCurrentChannel->GetState() != PS_STATE_LINE_OFF)
		{
			if(pChInfo->IsParallel() && pChInfo->m_bMaster == FALSE)
			{
				bAble = FALSE;
			}
			else
				bAble = TRUE;
		
		}


	}
	pCmdUI->Enable(bAble);	
	m_ctrlStart.EnableWindow(bAble);*/

}

void CCTSMonProView::OnUpdateNextStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		CCyclerModule *pMD;
		int nModuleID;
		BOOL bAble = FALSE;
		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
		while(pos)
		{
			int item = m_wndModuleList.GetNextSelectedItem(pos);
			nModuleID  = m_wndModuleList.GetItemData(item);
			pMD = GetDocument()->GetModuleInfo(nModuleID);
			if(pMD)
			{
				if(pMD->GetState() == PS_STATE_LINE_OFF)
				{
					bAble = FALSE;
					break;
				}
				else
				{
					bAble = TRUE;
				}
			}
			else 
			{
				bAble = FALSE;
				break;
			}
		}
		//m_ctrlNextStep.EnableWindow(bAble);
		pCmdUI->Enable(bAble);		
		
	}
	else	
	{
		//m_ctrlNextStep.EnableWindow(UpdateUIChList(SFT_CMD_NEXTSTEP, FALSE));
		pCmdUI->Enable(UpdateUIChList(SFT_CMD_NEXTSTEP, FALSE));		
	}

/*	BOOL bAble = FALSE;
	CCyclerModule * pMD = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
	if(pMD == NULL) return;
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(m_nCurrentChannelNo);
	if(pChInfo == NULL) return;

	if(pCurrentChannel != NULL)
	{
		if(pCurrentChannel->GetState() != PS_STATE_LINE_OFF)
		{
			if(pChInfo->IsParallel() && pChInfo->m_bMaster == FALSE)
			{
				bAble = FALSE;
			}
			else
				bAble = TRUE;
		}


	}
	pCmdUI->Enable(bAble);	
	m_ctrlNextStep.EnableWindow(bAble);*/
}


void CCTSMonProView::OnUpdateCmdPause(CCmdUI* pCmdUI) 
{
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
// 		CCyclerModule *pMD;
// 		int nModuleID;
// 		BOOL bAble = FALSE;
// 		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 		while(pos)
// 		{
// 			int item = m_wndModuleList.GetNextSelectedItem(pos);
// 			nModuleID  = m_wndModuleList.GetItemData(item);
// 			pMD = GetDocument()->GetModuleInfo(nModuleID);
// 			if(pMD)
// 			{
// 				if(pMD->GetState() == PS_STATE_LINE_OFF)
// 				{
// 					bAble = FALSE;
// 					break;
// 				}
// 				else
// 				{
// 					bAble = TRUE;
// 				}
// 			}
// 			else 
// 			{
// 				bAble = FALSE;
// 				break;
// 			}
// 		}
		pCmdUI->Enable(TRUE);	
	}
	else
	{
		pCmdUI->Enable(UpdateUIChList(SFT_CMD_PAUSE, FALSE));	
	}

	/*BOOL bAble = FALSE;
	CCyclerModule * pMD = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
	if(pMD == NULL) return;
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(m_nCurrentChannelNo);
	if(pChInfo == NULL) return;

	if(pCurrentChannel != NULL)
	{
		if(pCurrentChannel->GetState() != PS_STATE_LINE_OFF)
		{
			if(pChInfo->IsParallel() && pChInfo->m_bMaster == FALSE)
			{
				bAble = FALSE;
			}
			else
				bAble = TRUE;
		}


	}
	pCmdUI->Enable(bAble);	
	m_ctrlStop.EnableWindow(bAble);*/
}

void CCTSMonProView::OnUpdateCmdContinue(CCmdUI* pCmdUI) 
{
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
// 		CCyclerModule *pMD;
// 		int nModuleID;
// 		BOOL bAble = FALSE;
// 		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 		while(pos)
// 		{
// 			int item = m_wndModuleList.GetNextSelectedItem(pos);
// 			nModuleID  = m_wndModuleList.GetItemData(item);
// 			pMD = GetDocument()->GetModuleInfo(nModuleID);
// 			if(pMD)
// 			{
// 				if(pMD->GetState() == PS_STATE_LINE_OFF)
// 				{
// 					bAble = FALSE;
// 					break;
// 				}
// 				else
// 				{
// 					bAble = TRUE;
// 				}
// 
// 			}
// 			else 
// 			{
// 				bAble = FALSE;
// 				break;
// 			}
// 		}
	}
	else	
	{
		pCmdUI->Enable(UpdateUIChList(SFT_CMD_CONTINUE, FALSE));	
	}
}

void CCTSMonProView::OnUpdateCmdStop(CCmdUI* pCmdUI) 
{
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
// 		CCyclerModule *pMD;
// 		int nModuleID;
// 		BOOL bAble = FALSE;
// 		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 		while(pos)
// 		{
// 			int item = m_wndModuleList.GetNextSelectedItem(pos);
// 			nModuleID  = m_wndModuleList.GetItemData(item);
// 			pMD = GetDocument()->GetModuleInfo(nModuleID);
// 			if(pMD)
// 			{
// 				if(pMD->GetState() == PS_STATE_LINE_OFF)
// 				{
// 					bAble = FALSE;
// 					break;
// 				}
// 				else
// 				{
// 					bAble = TRUE;
// 				}
// 			}
// 			else 
// 			{
// 				bAble = FALSE;
// 				break;
// 			}
// 		}
//		m_ctrlStop.EnableWindow(TRUE);
		pCmdUI->Enable(TRUE);
	}
	else	
	{
		//m_ctrlStop.EnableWindow(UpdateUIChList(SFT_CMD_PAUSE, FALSE));
		 pCmdUI->Enable(UpdateUIChList(SFT_CMD_STOP, FALSE));	
	}
}

void CCTSMonProView::OnUpdateClearState(CCmdUI* pCmdUI) 
{
//	pCmdUI->Enable(UpdateUIChList(0, FALSE));
	pCmdUI->Enable(FALSE);

}


//Standby에 있는(다른 조건이 할당된 채널들)을 동시에 시작할 수 있게 할 것인가? 
// 주 용도가 시험용일 경우는 시작시 마다 시험조건을 다시 보낸다.
// 주 용도가 양산일 경우는 시작명령만 보낸다. 

void CCTSMonProView::Fun_SchAllUpdate(CString strTestPath, CString strContent)
{						   
	//1. 선택된 채널 중 Run 명령이 전송가능한 채널을 Update 한다.
	// 확인 Message 창이나 공정 선택 창이 떠있는 동안 모듈의 상태가 변하여서 전송가능 못하 수도 있으므로 Check

	//다른 입력창들이 많으므로 확이 하지 않는다.
	//2. Check to user

	//3. 스케쥴 목록에서 스케쥴 선택
	//   1) 한개의 시험은 모두 같은 조건이 할당된 채널들이어야 한다.
	//   2) 다른 조건이 할당된 채널이 같은 시험이 되지 않도록 시험 시작시에 선택된 채널에 언제나 시험 조건을 새로 전송한다.

	BOOL bLock = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLockForRun",FALSE);	//ljb 20150316
	if (bLock)
	{
		CLoginManagerDlg LoginDlg;
		if(LoginDlg.DoModal() != IDOK) return;
	}
	
	CCTSMonProDoc *pDoc = GetDocument();
	CString strLogMsg, strTestName, strScheduleFileName;


	CDWordArray adwPauseCh;
	CCyclerModule	*pMD;
	CCyclerChannel	*pChannel;

	UINT nModuleID = 0;
	UINT nChIndex = 0;

	//////////////////////////////////////////////////////////////////////////
	//m_awTargetChArray을 구함
	//if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	//if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_SAFETY_DATA_UPDATE)  == FALSE) //ksj 20200218 : Stop 명령 조건이 변경되어, 별도로 DATA_UPDATE 조건 추가.	
	{
		#ifndef _DEBUG
		AfxMessageBox(Fun_FindMsg("OnCmdRun_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
			return;
		#endif
	}
 	//////////////////////////////////////////////////////////////////////////

	int nSelCount = m_adwTargetChArray.GetSize();

	CPtrArray	aPtrSelCh;
	CString strLotNo;
	CString strTemp;
	
	int  nMinVoltage;
	if(m_bOverChargerSet) //yulee 20180227 OverCharger //yulee 20191017 OverChargeDischarger Mark
	{
		float OverChargerMinVolt = 0;
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);
		nMinVoltage = -OverChargerMinVolt;//yulee 20180810 //atoi(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Min Voltage", "-30000")); //mV 단위
	}
	else
		nMinVoltage = atoi(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Min Voltage", "-500")); //mV 단위
	//nMinVoltage = atoi(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Min Voltage", "-500")); //mV 단위

	float fMinVoltage = nMinVoltage * 1000;

	BOOL bStartStepOption = TRUE;
	///선택한 채널들이 이전에 정상적으로 끝났는지 확인한다.
	///시작 명령을 취소하면 나중에 Run명령을 전송하면 안된다.


	//ksj 20160726 작업 시작전 전압 확인 팝업 추가.
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check Voltage", 0)) //레지스트리 옵션 처리
	{
		CVoltageCheckDlg voltCheckDlg(m_adwTargetChArray, GetDocument());
		
		if(voltCheckDlg.DoModal() == IDCANCEL)	return;
		
	}

	//ksj 20160726 작업 시작전 외부센서 안전조건 확인
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check AuxSafeCond", 0)) //레지스트리 옵션 처리
	{
		if(!CheckAuxSafetyCond()) return;
	}


	//ljb 20130514 add 모듈별 작업 준비 중인지 체크
	int ch;
	int nCheckModuleID = 0;
	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		if(ch == 0)
		{
			nCheckModuleID = nModuleID;
			if (pDoc->Fun_CheckReadyDlg(nCheckModuleID) == TRUE)
			{
				strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg2","IDD_CTSMonPro_FORM"),nCheckModuleID);
				//@ strLogMsg.Format("ReadyDlg 이상 입니다. (module ID : %d)",nCheckModuleID);
				AfxMessageBox(strLogMsg);
				return;
			}
		}
		if (nCheckModuleID != nModuleID)
		{
			nCheckModuleID = nModuleID;
			if (pDoc->Fun_CheckReadyDlg(nCheckModuleID) == TRUE)
			{
				strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg3","IDD_CTSMonPro_FORM"),nCheckModuleID);
				//@ strLogMsg.Format("ReadyDlg 이상 입니다. (module ID : %d)",nCheckModuleID);
				AfxMessageBox(strLogMsg);
				return;
			}
		}
	}

	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

// 		if (pChannel->GetState() == PS_STATE_RUN)
// 		{
// 			strTemp.Format("Module : %d, Ch %d 채널을 시작 할 수 없습니다. 채널을 다시 선택 하세요.",nModuleID,nChIndex+1);
// 			AfxMessageBox(strTemp);
// 			return;
// 		}
// 		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		
		//Step Skip 기능 가능 여부 검사 
		if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSIONV3)		bStartStepOption = FALSE;
		
	
		//잠시 멈춤 Channel
		if(pChannel->GetState() == PS_STATE_PAUSE)
		{
			adwPauseCh.Add(MAKELONG(nChIndex, nModuleID));
			TRACE("Pause Module %d ch %d\n", nModuleID, nChIndex);
		}
		
		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);
		strLotNo = pChannel->GetTestSerial();
		
		//ksj 20160728 이전 작업의 SBC StepEndData를 다운로드 한다.
		pDoc->m_nFtpDownStepEndModuleID = nModuleID;
		pDoc->m_nFtpDownStepEndChannelIndex = nChIndex;
		//yulee 20190420 이전 작업의 SBC Raw Data를 다운로드 한다.
		//pDoc->m_bRunFtpDownStepEnd = TRUE;
		pDoc->m_bRunFtpDownAllSBCData = TRUE;
		pDoc->m_FTPFileDownRetryCnt = 0;
		pDoc->m_bSucessFtpDownAllSBCData = FALSE;
	}
	
	//만약 Pause인 채널이 있으면 Stop명령을 보낸다.
// 	if(adwPauseCh.GetSize() > 0)
// 	{
// 		if(AfxMessageBox("선택한 채널의 이전 작업을 초기화 하고 새로운 작업을 설정 하시겠습니까?", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
// 		{
// 			return;
// 		}
// 		if(SendCmdToModule(adwPauseCh, SFT_CMD_STOP) == FALSE)	
// 		{
// 			AfxMessageBox("작업 완료 처리 실패");
// 			return;
// 		}
//	}
	//이전시험 정상 종료 여부 확인 끝
	//////////////////////////////////////////////////////////////////////////

	((CMainFrame *)AfxGetMainWnd())->m_bBlockFlag = TRUE;

	//새롭게 시행할 공정을 선택한다.
	//////////////////////////////////////////////////////////////////////////
	CSelectScheduleDlg	dlg(GetDocument());
//	CStartDlg	startDlg(GetDocument(), this);
// 	startDlg.m_bEnableStepSkip = bStartStepOption;
// 	startDlg.m_strLot = strLotNo;
	CScheduleData	scheduleInfo;
	CScheduleData	*scheduleInfo_cur; //lyj 20210817

	Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

	int nRtn = 0;
	int nModelPK = 0;
	int nTestPK = 0;

	CString strMsg;
	CWorkWarnin WorkWarningDlg;
	while(1)
	{
		dlg.m_nSelModelID = nModelPK;
		dlg.m_nSelScheduleID = nTestPK;
		
		if(IDOK != dlg.DoModal())
		{
			nRtn = 0;
			break;
		}
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

		nModelPK = dlg.m_nSelModelID;		//선택 모델 PK
		nTestPK = dlg.m_nSelScheduleID;		//선택 스케쥴 PK

		//4. 선택한 공정의 조건을 DataBase에서 Load한다.
		if(scheduleInfo.SetSchedule(pDoc->GetDataBaseName(), nModelPK, nTestPK) == FALSE)
		{
			//strLogMsg.Format("공정조건 불러오기 실패!!!");
			strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg22","IDD_CTSMonPro_FORM"));//&&
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			nRtn = 0;
			break;
		}
		else
		{
			TRACE("%f\n",scheduleInfo.m_CellCheck.fMaxV);
			TRACE("%f\n",scheduleInfo.m_CellCheck.fMinV);
			TRACE("%f\n",scheduleInfo.m_CellCheck.fCellDeltaV);
			TRACE("%f\n",scheduleInfo.m_CellCheck.fMaxI);
			if (scheduleInfo.Fun_ISEmptyCommSafety() == -1)	
			{
				//strLogMsg.Format("시험 안전조건이 설정 되어 있지 않습니다. !!!");
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg1","IDD_CTSMonPro_FORM"));//&&
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				AfxMessageBox(strLogMsg);
				nRtn = 0;
				break;
			}
			if (scheduleInfo.Fun_ISEmptyCommSafety() == -2)	//20150811 edit
			{
				//strLogMsg.Format("시험 안전조건 값이 설정 되어 있지 않습니다. (최대전압, 최대전류, 최대 셀전압편차) !!!");
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg2","IDD_CTSMonPro_FORM"));//&&
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				AfxMessageBox(strLogMsg);
				nRtn = 0;
				break;
			}
			if (scheduleInfo.Fun_ISEmptyCommSafety() == -3)	// 211015 HKH 절연모드 충전 관련 기능 보완
			{
				//strLogMsg.Format("시험 안전조건 값이 설정 되어 있지 않습니다. (최소전압) !!!");
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg24","IDD_CTSMonPro_FORM"));//&&
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				AfxMessageBox(strLogMsg);
				nRtn = 0;
				break;
			}
			nRtn = 1;	//스케쥴 정상 선택 완료

			//ksj 20170823 : 현재 스케쥴의 현재 스텝과 바뀔 스케쥴의 스텝을 비교하여, 
			//ljb 520170922	 현재 스케쥴 스텝번호에 해당되는 스텝이 cycle, loop, end, pattern 인경우 바꾸지 않는다
			//ljb 520170922	 현재 스케쥴 스텝번호와 변경 스케쥴 총 스텝수와 비교 하여 총스텝수가 작으면 바꾸지 않는다
 			int nCurrentStep = 0;
			int nChangeSchStepSize = 0;
			int nChangeStepType = 0;
			CStep *pChangeStep = NULL;
			for(int tCh = 0; tCh <nSelCount ; tCh++)
			{
				pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

				nCurrentStep = pChannel->GetStepNo(); //현재 Step 번호
				nChangeSchStepSize = scheduleInfo.GetStepSize(); //변경할 스케쥴의 스텝 사이즈				

				if(nCurrentStep > nChangeSchStepSize) //현재 진행중인 스텝이 변경할 스텝의 전체 사이즈보다 크면 변경하지 않는다.
				{
					//strLogMsg.Format("Step Size 오류 (현재 스텝 :%d, 변경 스케쥴 크기 :%d)",nCurrentStep, nChangeSchStepSize);
					strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg3","IDD_CTSMonPro_FORM"),nCurrentStep, nChangeSchStepSize);//&&
					pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strLogMsg);
					nRtn = 0;
					break;
				}

				nChangeStepType = scheduleInfo.GetStepData(nCurrentStep-1)->m_type; //스케쥴을 변경할 경우 현재 스텝에 해당하는 변경 스텝 타입.

				if(nChangeStepType == PS_STEP_LOOP ||   //현재 진행중인 스텝이 loop, cycle, end, patten 으로 변경되는 것을 막는다.
					nChangeStepType == PS_STEP_ADV_CYCLE ||
					nChangeStepType == PS_STEP_PATTERN ||
					nChangeStepType == PS_STEP_END
					)
				{
					//strLogMsg.Format("현재 스텝 %d는 %s 스텝으로 변경할 수 없습니다.",nCurrentStep, PSGetTypeMsg(nChangeStepType));
					strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg4","IDD_CTSMonPro_FORM"),nCurrentStep, PSGetTypeMsg(nChangeStepType));//&&
					pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strLogMsg);
					nRtn = 0;
					break;
				}

				//lyj 20210817 전체스케쥴 변경 시 최대전압 변경되는지 확인 s =============
				scheduleInfo_cur = pChannel->GetScheduleData();
				if(scheduleInfo_cur->m_CellCheck.fMaxV != scheduleInfo.m_CellCheck.fMaxV)
				{
					//strLogMsg.Format("기존스케쥴 전압최대: %.1f \n변경스케쥴 전압최대 : %.1f 이 다릅니다. \n 전압최대 값 확인하여주십시오",scheduleInfo_cur->m_CellCheck.fMaxV, scheduleInfo.m_CellCheck.fMaxV);
					strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg23","IDD_CTSMonPro_FORM"),scheduleInfo_cur->m_CellCheck.fMaxV, scheduleInfo.m_CellCheck.fMaxV);
					pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);					
					AfxMessageBox(strLogMsg);
					nRtn = 0;
					break;
				}
				//lyj 20210817 e =============		
			}

// 			if(nRtn == 0) 
// 			{
// 				break;
// 			}
			//ksj end
		}
//		nRtn = 1;	//스케쥴 정상 선택 완료
		break;
	}// while(1) end

	if(nRtn == 0)
	{
		//AfxMessageBox("변경할 수 없는 스케쥴 입니다.");
		AfxMessageBox(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg5","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	//////////////////////////////////////////////////////////////////////////

// 	CString strMsg;
// 	CWorkWarnin WorkWarningDlg;
	//strMsg = "전체 스케쥴 변경을 진행 하시겠습니까?";
	strMsg = Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg6","IDD_CTSMonPro_FORM");//&&
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;

	
	//20070831 KJH 스케줄의 Step 값을 확인한다.
	if(GetDocument()->ScheduleValidityCheck(&scheduleInfo, &m_adwTargetChArray) < 0)
	{
		AfxMessageBox(GetDocument()->GetLastErrorString());
		return;
	}

	//5. 입력한 작업명과 저장 위치 확인 
	// c:\\Test_Name 형태 
	//strTestPath = startDlg.GetTestPath();	
	ASSERT(!strTestPath.IsEmpty());
	strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);


	//ksj 20170810 : Update 폴더 명에 시간 구분 추가.
	CString strOriginalContent;
	CTime time = CTime::GetCurrentTime();
	CString strTime;
	strTime.Format(_T("%d%02d%02d_%02d%02d%02d"),time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
	strOriginalContent = strContent;
	
	//시험에 적용된 UPDATE 폴더 생성 ljb 20170801 add
	strLogMsg.Format("%s\\Update", strContent);

	if( ::CreateDirectory(strLogMsg, NULL) == FALSE ) //ksj 20170810 : Update 상위 폴더 생성.
	{
		//strTemp.Format("Update 저장용 폴더 생성 실패(%s)", strLogMsg);
		strTemp.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg7","IDD_CTSMonPro_FORM"), strLogMsg);//&&
		pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
	}
	
	strLogMsg.Format("%s\\Update\\Update_%s", strContent, strTime); //ksj 20170810 : Update 하위 폴더 경로 지정.
	strContent	= strLogMsg;
	
	//5.1 Pattern File 폴더를 생성시킨다.
	if( ::CreateDirectory(strLogMsg, NULL) == FALSE )
	{
		//strTemp.Format("Update 저장용 폴더 생성 실패(%s)", strLogMsg);
		strTemp.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg8","IDD_CTSMonPro_FORM"), strLogMsg);//&&
		pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
	}
	//strLogMsg.Format("Update 저장 폴더 생성 성공(%s)", strContent);
	strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg9","IDD_CTSMonPro_FORM"), strContent);//&&
	pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_SYSTEM);

	//시험에 적용된 Pattern File 저장 폴더 생성 ljb 20130502 add
	strLogMsg.Format("%s\\Pattern", strContent);

	//5.1 Pattern File 폴더를 생성시킨다.
	if( ::CreateDirectory(strLogMsg, NULL) == FALSE )
	{
		//strTemp.Format("Pattern 저장용 폴더 생성 실패(%s)", strLogMsg);
		strTemp.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg10","IDD_CTSMonPro_FORM"), strLogMsg);//&&
		pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
	}
	//strLogMsg.Format("Data 저장 폴더 생성 성공(%s)", strContent);
	strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg11","IDD_CTSMonPro_FORM"), strContent);//&&
	pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_SYSTEM);

	//////////////////////////////////////////////////////////////////////////
	//CProgressWnd progressWnd(AfxGetMainWnd(), "전송중...", TRUE);
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg12","IDD_CTSMonPro_FORM"), TRUE);//&&
    progressWnd.GoModal();
	progressWnd.SetRange(0, 100);
	//////////////////////////////////////////////////////////////////////////

	CDWordArray adwChArray;
	nSelCount = aPtrSelCh.GetSize();
	int tCh;

	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);
		
		if(pChannel == NULL)
		{
			//TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
			TRACE(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg13","IDD_CTSMonPro_FORM"));//&&
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);
		
		adwChArray.Add(MAKELONG(nChIndex, nModuleID));
		
		//6. 선택된 Channel을 시험준비 한다.
		progressWnd.SetPos(0);
		//strLogMsg.Format("%s 의 채널 %d 결과 폴더 생성...", pDoc->GetModuleName(nModuleID), nChIndex+1);
		strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg14","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nModuleID), nChIndex+1);//&&
		progressWnd.SetText(strLogMsg);
		progressWnd.SetPos(int((float)tCh/(float)nSelCount*100.0f));
		
		//Cancel 처리
		if(progressWnd.PeekAndPump() == FALSE)				break;
		
// 		int nStartChNo = GetStartChNo(nModuleID);
// 		strContent.Format("%s\\M%02dCh%02d[%03d]", strTestPath, nModuleID, nChIndex+1, nStartChNo+nChIndex);
// 		
// 		//6.1 Channel 폴더를 생성시킨다.
// 		if (pDoc->ForceDirectory(strContent) == FALSE)
// 		{
// 			strLogMsg.Format("Data 저장 폴더 생성 실패(%s)", strContent);
// 			AfxMessageBox(strLogMsg);
// 			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
// 			return;
// 		}
// 		else
// 		{
// 			strLogMsg.Format("%s\\StepStart", strContent);
// 			pDoc->ForceDirectory(strLogMsg);
// 		}
		Sleep(100);
		
		//6.2 각 폴더에 스케줄 파일을 생성한다.
		// c:\\Test_Name\M01C01\Test_Name.sch
		strScheduleFileName = strContent + "\\" + strTestName+ ".sch";	//20170729 파일 있는지 체크하는 부분 추가 해야 함.
		::DeleteFile(strScheduleFileName);	//ljb 20170801 임시 삭제
		if(FALSE == scheduleInfo.SaveToFile(strScheduleFileName))
		{
			//strLogMsg.Format("%s Schedule 파일 생성 실패", strTestName);
			strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg15","IDD_CTSMonPro_FORM"), strTestName);//&&
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			continue;
		}

		//ksj 20170810 : 상위 폴더에 스케쥴 파일을 덮어 씌운다.
		CString strMainSchFileName = strOriginalContent + "\\" + strTestName+ ".sch";
		DeleteFile(strMainSchFileName); //상위 폴더 sch 파일 제거
		CopyFile(strScheduleFileName,strMainSchFileName,FALSE); //Update 된 sch 파일로 상위 폴더 sch 대체
		
		
// 		strChTrayno = pChannel->GetTestTrayNo();
// 		strChBcrNo = pChannel->GetTestBcrNo();
// 		pChannel->ResetData();
// 		//6.3 시험명 설정
//  		pChannel->SetFilePath(strContent);	//ljb 20130626 수정 (함수내부)		
// 
//  		//Set Test Lot No
// 		pChannel->SetTestSerial(startDlg.m_strLot, startDlg.m_strWorker, startDlg.m_strComment,pChannel->m_strCellDeltaVolt);
// 		pChannel->SetTestSerial2(strChTrayno, strChBcrNo);
// 		pChannel->LoadSaveItemSet();
// 		
// 		//6.4 Aux Title 파일 생성
// 		pChannel->SetAuxTitleFile(strContent, strTestName);
// 		//6.5 Can Title 파일 생성
// 		pChannel->SetCanTitleFile(strContent, strTestName);
// 		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
// 
// 		//ljb 20130404 CTS File 생성
// 		pChannel->WriteCtsFile();

	
		//ljb 20160314 BCR Data write
// 		CFileFind dbFinder;
// 		if(dbFinder.FindFile(GetDocument()->GetWorkInfoDataBaseName()))	//20160314   Work_Info_2000.mdb
// 		{
// 			BOOL bInsertWorkInfo = FALSE;
// 			
// 			bInsertWorkInfo = InsertWorkInfo(pChannel->GetModuleID(), pChannel->GetChannelIndex()+1, pChannel->GetScheduleName(), pChannel->GetTestName(), pChannel->GetTestPath(), strChTrayno, strChBcrNo, pChannel->GetTestSerial());
// 			if(bInsertWorkInfo == FALSE)
// 			{
// 				//pDoc->WriteSysLog("CH%02d bar-code Information save fail", pChannel->GetChannelIndex()+1);
// 				strLogMsg.Format("CH%02d bar-code Information save fail", pChannel->GetChannelIndex()+1);
// 				AfxMessageBox(strLogMsg);
// 				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
// 			}
// 		}

	}
	Sleep(200);	//모듈 상태 전이를 위한 시간 지연 

	//strLogMsg.Format("Model Name : %s, Schedule Name : %s -> SBC로 전송 시작", scheduleInfo.GetModelName(), scheduleInfo.GetScheduleName());
	strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg16","IDD_CTSMonPro_FORM"), scheduleInfo.GetModelName(), scheduleInfo.GetScheduleName());//&&
	pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);

	CCyclerChannel *lpChannelInfo = pMD->GetChannelInfo(ch);
	if(lpChannelInfo) lpChannelInfo->Fun_SetFirstNgState(FALSE);
	//충방전 Start	
	if(pDoc->SendSchUpdateCmd(&progressWnd, &adwChArray, &scheduleInfo) == FALSE)
	{
		//AfxMessageBox(strTestName + " 스케쥴 업데이트를 실패 하였습니다.");
		AfxMessageBox(strTestName + Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg17","IDD_CTSMonPro_FORM"));//&&
		//ksj 20170810 : Update 로그 기록
		for(tCh = 0; tCh <nSelCount ; tCh++)
		{
			pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);
			
			if(pChannel == NULL)
			{
				//TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
				TRACE(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg18","IDD_CTSMonPro_FORM"));//&&
				continue;
			}		
			strLogMsg.Format(_T("-------------------- Full Schedule Update Log ------------------\n\
				Full Schedule Update failure"));
			pChannel->WriteSchUpdateLog(strLogMsg);
		}
		//ksj end
	}
	else
	{
		//strTemp.Format("스케쥴 업데이트 전송 완료");
		strTemp.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg19","IDD_CTSMonPro_FORM"));//&&
		progressWnd.SetText(strTemp);
		progressWnd.SetPos(100);

		//strLogMsg.Format("Model Name : %s, Schedule Name : %s -> SBC로 전송 성공", scheduleInfo.GetModelName(), scheduleInfo.GetScheduleName());
		strLogMsg.Format(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg20","IDD_CTSMonPro_FORM"), scheduleInfo.GetModelName(), scheduleInfo.GetScheduleName());//&&
		pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);

		//ksj 20170810 : Update 로그 기록
		for(tCh = 0; tCh <nSelCount ; tCh++)
		{
			pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);
			
			if(pChannel == NULL)
			{
				//TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
				TRACE(Fun_FindMsg("CTSMonProView_Fun_SchAllUpdate_msg21","IDD_CTSMonPro_FORM"));//&&
				continue;
			}		
			strLogMsg.Format(_T("-------------------- Full Schedule Update Log ------------------\n\
				Model Name : %s, Shcedule Name : %s, sch edited time: %s"), scheduleInfo.GetModelName(), scheduleInfo.GetScheduleName(), scheduleInfo.GetScheduleEditTime());
			pChannel->WriteSchUpdateLog(strLogMsg);
			pChannel->SetScheduleName(scheduleInfo.GetScheduleName()); //lyj 20210902 표시되는 스케쥴명도 변경
		}
		//ksj end
	}

	Sleep(200);

	((CMainFrame *)AfxGetMainWnd())->m_bBlockFlag = FALSE;
}

BOOL CCTSMonProView::CheckAuxListSet(BOOL &bTemp, BOOL &bTher) //20181114 yulee
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pMD;
	CCyclerChannel *lpChannelInfo;
	
	int nCntTemp, nCntTher;
	nCntTemp = 0;
	nCntTher = 0;

	UINT nModuleID=0, nChannelID=0;
	
	UINT mdID;
	if (m_nCurrentModuleID == 0)
	{
		nChannelID = m_nCurrentChannelNo;
		int nTotMDCnt = pDoc->GetInstallModuleCount();
		for(int s =0; s<nTotMDCnt; s++)
		{
			nModuleID = s+1;
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				if (pMD->GetTotalChannel() < nChannelID + 1)
				{
					nChannelID = nChannelID - pMD->GetTotalChannel();
				}
				else
				{
					break;
				}
			}
		}
	}
	else
	{
		nModuleID = m_nCurrentModuleID;
		nChannelID = m_nCurrentChannelNo;
	}

	CString strItem,strTemp;
	CCyclerModule *lpModule = GetDocument()->GetModuleInfo(nModuleID);
	if(lpModule == NULL)	return FALSE;

	//현재 채널 정보
	lpChannelInfo =  lpModule->GetChannelInfo(nChannelID);
	if(lpChannelInfo == NULL)	return FALSE;

	m_ctrlLal_CellDeltaVolt = lpChannelInfo->m_strCellDeltaVolt;	//ljb 20150806
	UpdateData(FALSE);

	//ljb 201137 이재복 //////////
	if (lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE) return TRUE;
	int nAuxListCount = m_wndAuxList.GetItemCount();
	int nAuxPos=0;

	SFT_AUX_DATA * pAuxData = lpChannelInfo->GetAuxData();

	//BOOL bAuxFirstTemp(TRUE), bAuxFirstVolt(TRUE), bAuxFirstTher(TRUE); //ksj 20200116 : 주석처리. 사용하지 않는 변수인 것 같아 주석처리.
	for(int i = 0; i < lpChannelInfo->GetAuxCount(); i++)
	{
		CChannelSensor * pSensor;
		pSensor = lpModule->GetAuxData(pAuxData[i].auxChNo-1, pAuxData[i].auxChType);

		if(pSensor == NULL)
		{
			CString strMsg;
			//strMsg.Format("Error : 설정되지 않은 외부데이터에 접근(ChNo : %d, Aux Ch : %d, Aux Type : %d, Aux Count : %d)",
			strMsg.Format(Fun_FindMsg("CTSMonProView_CheckAuxListSet_msg1","IDD_CTSMonPro_FORM"),//&&
				m_nCurrentChannelNo+1, pAuxData[i].auxChNo, pAuxData[i].auxChType, lpChannelInfo->GetAuxCount());
			lpChannelInfo->WriteLog(strMsg);
			return FALSE;
		}

		if(pAuxData[i].auxChType == PS_AUX_TYPE_VOLTAGE)	//ljb 20170829 add
		{

		}
		else if(pAuxData[i].auxChType == PS_AUX_TYPE_TEMPERATURE)
		{
			nCntTemp++;

		}
		else if(pAuxData[i].auxChType == PS_AUX_TYPE_TEMPERATURE_TH)
		{
			nCntTher++;

		}
	}
	if(nCntTemp > 0)
	{
		bTemp = TRUE;
		return TRUE;
	}
	if(nCntTher > 0)
	{
		bTher = TRUE;
		return TRUE;
	}

	return TRUE;

}

BOOL CCTSMonProView::CheckAuxRTtableSet(int *ArrChkRet1, int *ArrChkRet2, int *ArrChkRet3, int *ArrChkRet4) //20181118 yulee
{
	CCyclerModule *pMD;
	int nModuleID;
	nModuleID = 1;
	CCTSMonProDoc *pDoc = GetDocument();
	pMD = pDoc->GetModuleInfo(nModuleID);
	int nTotalCh = pMD->GetTotalChannel();
	for(int ch =0; ch < nTotalCh; ch++)
	{

		m_wndChannelList.SetItemState(-1,0, LVIS_FOCUSED | LVIS_SELECTED);
		Wait_Milli(300);
		m_wndChannelList.SetItemState(ch, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED |LVIS_SELECTED);
		Wait_Milli(300);
		UpdateAuxListData();
		Wait_Milli(300);

		CString strItem,strTemp;
		CCyclerModule *lpModule = GetDocument()->GetModuleInfo(nModuleID);
		if(lpModule == NULL)	return FALSE;
		CCyclerChannel *lpChannelInfo = pMD->GetChannelInfo(ch);
		if(lpChannelInfo == NULL)	return FALSE;
		//ljb 201137 이재복 //////////
		if (lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE) return TRUE;

		SFT_AUX_DATA * pAuxData = lpChannelInfo->GetAuxData();
		BOOL bAuxFirstTemp(TRUE), bAuxFirstVolt(TRUE), bAuxFirstTher(TRUE);

		if(	lpChannelInfo->GetState() != PS_STATE_IDLE && 
			lpChannelInfo->GetState() != PS_STATE_STANDBY &&
			lpChannelInfo->GetState() != PS_STATE_READY &&
			lpChannelInfo->GetState() != PS_STATE_END
			)
		//if((lpChannelInfo->GetState() == PS_STATE_RUN))//yulee 20181118
			/*||(lpChannelInfo->GetState() == PS_STATE_PAUSE))*/
		{
			for(int i = 0; i < m_wndAuxList.GetItemCount(); i++)
			{
				CString strItem1, strItem2;
				int nNum = -1;
				strItem1 = m_wndAuxList.GetItemText(i, 1);
				strItem2 = 	m_wndAuxList.GetItemText(i, 10);
				nNum = atoi(strItem2.Mid(strItem2.Find('h')+1, strItem2.GetLength()));
				
				//if(strItem1.Find("써미스터")> -1) //yulee 20190405 check
				if(strItem1.Find(Fun_FindMsg("CTSMonProView_CheckAuxRTtableSet_msg1","IDD_CTSMonPro_FORM"))> -1) //&&
				{
					if((--nNum) > -1)
					{
						if(ch == 0)
						{
							ArrChkRet1[nNum] = TRUE;
						}
						else if(ch == 1)
						{
							ArrChkRet2[nNum] = TRUE;
						}
						else if(ch == 2)
						{
							ArrChkRet3[nNum] = TRUE;
						}
						else if(ch == 3)
						{
							ArrChkRet4[nNum] = TRUE;
						}
					}
				}					
			}
		}
	}

	return TRUE;//cny
}


BOOL CCTSMonProView::CheckCANListSet(BOOL &bCANTemp) //20181114 yulee
{
	CCyclerChannel *lpChannelInfo;
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pMD;
	
	UINT nModuleID=0,nChannelID=0;
	
	UINT mdID;
	if (m_nCurrentModuleID == 0)
	{
		nChannelID = m_nCurrentChannelNo;
		int nTotMDCnt = pDoc->GetInstallModuleCount();
		for(int s =0; s<nTotMDCnt; s++)
		{
			nModuleID = s+1;
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				if (pMD->GetTotalChannel() < nChannelID + 1)
				{
					nChannelID = nChannelID - pMD->GetTotalChannel();
				}
				else
				{
					break;
				}
			}
		}
	}
	else
	{
		nModuleID = m_nCurrentModuleID;
		nChannelID = m_nCurrentChannelNo;
	}

	CString strItem,strBmsRdy;
	CCyclerModule *lpModule = GetDocument()->GetModuleInfo(nModuleID);
	if(lpModule == NULL)	return FALSE;

	//현재 채널 정보
	lpChannelInfo =  lpModule->GetChannelInfo(nChannelID);
	if(lpChannelInfo == NULL)	return FALSE;

	//무슨의미인지 모르겠음..
	if (lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE) return FALSE;

	//m_wndCanList.DeleteAllItems();
	int nCanListCount = m_wndCanList.GetItemCount();	//리스트가 비어있는지 확인하는 변수

	SFT_CAN_DATA * pCanData = lpChannelInfo->GetCanData();	
	if(pCanData == NULL) return FALSE;

	for(int i = 0; i < lpModule->GetInstalledCanMaster(nChannelID); i++)
	{
		SFT_CAN_SET_DATA canData = lpModule->GetCANSetData(nChannelID, PS_CAN_TYPE_MASTER, i);
		
		if(pCanData[i].data_type == 0 || pCanData[i].data_type == 1 || pCanData[i].data_type == 2)
		{
			strItem.Format("%.6f", pCanData[i].canVal.fVal[0]);
			if (canData.function_division > 100 && canData.function_division < 150)
				strItem.Format("%02x%02x%02x%02x", (BYTE)pCanData[i].canVal.strVal[0], (BYTE)pCanData[i].canVal.strVal[1], (BYTE)pCanData[i].canVal.strVal[2], (BYTE)pCanData[i].canVal.strVal[3]);
			
			strItem.TrimRight("0");
			strItem.TrimRight(".");
		}
		else if(pCanData[i].data_type == 3){
			strItem.Format("%s", pCanData[i].canVal.strVal);
		}
		else if (pCanData[i].data_type == 4){
			strItem.Format("%x", (UINT)pCanData[i].canVal.fVal[0]);			
		}

		//이부분에서 Division Code확인
		if (canData.function_division != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
			if((strItem.Mid(strItem.Find("["), strItem.Find("]"))).CompareNoCase("10015") == TRUE)
			{
				bCANTemp = TRUE;
				return TRUE;
			}

		}
		if (canData.function_division2 != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
			if((strItem.Mid(strItem.Find("["), strItem.Find("]"))).CompareNoCase("10015") == TRUE)
			{
				bCANTemp = TRUE;
				return TRUE;
			}
		}
		if (canData.function_division3 != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
			if((strItem.Mid(strItem.Find("["), strItem.Find("]"))).CompareNoCase("10015") == TRUE)
			{
				bCANTemp = TRUE;
				return TRUE;
			}
		}
	}
	
	
	for(int i = 0; i < lpModule->GetInstalledCanSlave(nChannelID); i++)
	{
		SFT_CAN_SET_DATA canData = lpModule->GetCANSetData(nChannelID, PS_CAN_TYPE_SLAVE, i);
		
		if (canData.function_division != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
			if((strItem.Mid(strItem.Find("["), strItem.Find("]"))).CompareNoCase("10015") == TRUE)
			{
				bCANTemp = TRUE;
				return TRUE;
			}
		}
		
		if (canData.function_division2 != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
			if((strItem.Mid(strItem.Find("["), strItem.Find("]"))).CompareNoCase("10015") == TRUE)
			{
				bCANTemp = TRUE;
				return TRUE;
			}
		}
		
		if (canData.function_division3 != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
			if((strItem.Mid(strItem.Find("["), strItem.Find("]"))).CompareNoCase("10015") == TRUE)
			{
				bCANTemp = TRUE;
				return TRUE;
			}
		}
	}
	bCANTemp = FALSE;
	return TRUE;
}

BOOL CCTSMonProView::CheckTempTherCANTemp(BOOL &bTempChk, BOOL &bTherChk, BOOL &bCANTempChk) //yulee 20181114
{
	CheckAuxListSet(bTempChk, bTherChk);
	CheckCANListSet(bCANTempChk);
	if((bTempChk == TRUE)||(bTherChk == TRUE)||(bCANTempChk == TRUE))
		return TRUE;
	else if((bTempChk == FALSE)&&(bTherChk == FALSE)&&(bCANTempChk == FALSE)) //설정된 설정 없음
		return FALSE;

	return FALSE;//cny
}

void CCTSMonProView::OnCmdRun() 
{
	//1. 선택된 채널 중 Run 명령이 전송가능한 채널을 Update 한다.
	// 확인 Message 창이나 공정 선택 창이 떠있는 동안 모듈의 상태가 변하여서 전송가능 못하 수도 있으므로 Check

	//다른 입력창들이 많으므로 확이 하지 않는다.
	//2. Check to user

	//3. 스케쥴 목록에서 스케쥴 선택
	//   1) 한개의 시험은 모두 같은 조건이 할당된 채널들이어야 한다.
	//   2) 다른 조건이 할당된 채널이 같은 시험이 되지 않도록 시험 시작시에 선택된 채널에 언제나 시험 조건을 새로 전송한다.

	BOOL bLock = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLockForRun",FALSE);	//ljb 20150316
	if (bLock)
	{
		CLoginManagerDlg LoginDlg;
		if(LoginDlg.DoModal() != IDOK) return;
	}
	
	CCTSMonProDoc *pDoc = GetDocument();
	CString strTestPath, strContent, strLogMsg, strTestName, strScheduleFileName;


	CDWordArray adwPauseCh;
	CCyclerModule	*pMD = NULL;
	CCyclerChannel	*pChannel = NULL;

	BOOL	bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);
    BOOL	bChamRun = FALSE;	
	BOOL	bDownAllRawDataBefRun = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DownAllRawDataBefRun", TRUE); //yulee 20190420



	UINT nModuleID = 0;
	UINT nChIndex = 0;

	//////////////////////////////////////////////////////////////////////////
	//m_awTargetChArray을 구함
	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		#ifndef _DEBUG
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("CTSMonProView_OnCmdRun_msg1","IDD_CTSMonPro_FORM"));//&&
			return;
		#endif
	}

	BOOL bTempChk=FALSE, bTherChk=FALSE, bCANTempChk=FALSE;

	BOOL m_bTempSensorCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "TempSensorCheck", TRUE);		//yulee 20180227 OverCharger

	if(m_bTempSensorCheck == TRUE) //yulee 20190218 온도 측정 옵션처리
	{
 		if(CheckTempTherCANTemp(bTempChk, bTherChk, bCANTempChk) == FALSE)
 		{
 			//AfxMessageBox("온도 측정이 없습니다. 작업을 시작 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("CTSMonProView_OnCmdRun_msg2","IDD_CTSMonPro_FORM"));//&&
 			return;
 		} 
	}
 	//////////////////////////////////////////////////////////////////////////

	int nSelCount = m_adwTargetChArray.GetSize();

	CPtrArray	aPtrSelCh;
	CString strLotNo;
	CString strTemp;

	int  nMinVoltage;

	if(m_bOverChargerSet) //yulee 20180227 OverCharger //yulee 20191017 OverChargeDischarger Mark
	{
		float OverChargerMinVolt = 0;
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);
		nMinVoltage = -OverChargerMinVolt;//yulee 20180810 //atoi(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Min Voltage", "-30000")); //mV 단위
	}
	else
		nMinVoltage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Min Voltage", -500); //mV 단위
	//nMinVoltage = atoi(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Min Voltage", "-500")); //mV 단위

	float fMinVoltage = nMinVoltage * 1000;

	BOOL bStartStepOption = TRUE;
	///선택한 채널들이 이전에 정상적으로 끝났는지 확인한다.
	///시작 명령을 취소하면 나중에 Run명령을 전송하면 안된다.

	//ksj 20160726 작업 시작전 전압 확인 팝업 추가.
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check Voltage", 0)) //레지스트리 옵션 처리
	{
		CVoltageCheckDlg voltCheckDlg(m_adwTargetChArray, GetDocument());
		
		if(voltCheckDlg.DoModal() == IDCANCEL)	return;
		
	}

	//ksj 20160726 작업 시작전 외부센서 안전조건 확인
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check AuxSafeCond", 0)) //레지스트리 옵션 처리
	{
		if(!CheckAuxSafetyCond()) return;
	}


	//ljb 20130514 add 모듈별 작업 준비 중인지 체크
	int ch;
	int nCheckModuleID = 0;
	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		if(ch == 0)
		{
			nCheckModuleID = nModuleID;
			if (pDoc->Fun_CheckReadyDlg(nCheckModuleID) == TRUE)
			{
				//strLogMsg.Format("ReadyDlg 이상 입니다. (module ID : %d)",nCheckModuleID);
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_OnCmdRun_msg3","IDD_CTSMonPro_FORM"),nCheckModuleID);//&&
				AfxMessageBox(strLogMsg);
				return;
			}
		}
		if (nCheckModuleID != nModuleID)
		{
			nCheckModuleID = nModuleID;
			if (pDoc->Fun_CheckReadyDlg(nCheckModuleID) == TRUE)
			{
				//strLogMsg.Format("ReadyDlg 이상 입니다. (module ID : %d)",nCheckModuleID);
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_OnCmdRun_msg4","IDD_CTSMonPro_FORM"),nCheckModuleID);//&&
				AfxMessageBox(strLogMsg);
				return;
			}
		}
	}

	int nCanSetting=0;
	int nCanMasterCnt=0;
	int nCanSlaveCnt=0;
	SFT_CAN_DATA * pCanData;
	CWordArray aSelChArray;
	//SFT_CAN_SET_DATA canData;
	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

		if (pChannel->GetState() == PS_STATE_RUN)
		{
			strTemp.Format(Fun_FindMsg("OnCmdRun_msg4","IDD_CTSMonPro_FORM"), nModuleID,nChIndex+1);
			//@ strTemp.Format("Module : %d, Ch %d 채널을 시작 할 수 없습니다. 채널을 다시 선택 하세요.",nModuleID,nChIndex+1);
			AfxMessageBox(strTemp);
			return;
		}
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());


		// 211015 HKH 절연모드 충전 관련 기능 보완 =============================
		BOOL bUseIsolationSafety = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC , "Use Isolation Safety", 0);
		if(bUseIsolationSafety)
		{
			long lSafetyStartV = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC , "Safety Start Voltage", 5);	// V 단위로 사용
			long lV = pChannel->GetVoltage();
			if (pChannel->GetVoltage() < (lSafetyStartV * 1000000))	// GetVoltage()는 uV 단위이므로 설정치 x1000000 해서 사용
			{
				strTemp.Format(Fun_FindMsg("OnCmdRun_msg29","IDD_CTSMonPro_FORM"), nModuleID, nChIndex+1, (float)(lSafetyStartV));
				//@ strTemp.Format("Module : %d, Ch %d 현재 전압값이 기준치보다 낮습니다. 케이블을 확인해주세요.(설정 기준치 : %.3d V)",nModuleID, nChIndex+1, (float)(nSafetyStartV / 1000.0));
				AfxMessageBox(strTemp);
				return;
			}
		}
		// =====================================================================

		
		//if(pChannel->GetVoltage() < fMinVoltage)
		//{
		//	//역전압이나 최저 설정 전압보다 작을 경우 리턴시킴 -> 향후 YesNo 로 선택할 수 있도록 해야하지 않을까? kjh
		//	CString strErrorMsg;
		//	strErrorMsg.Format("채널 %d에 최저 설정 전압보다 작습니다. \nCell의 방향 및 연결 상태를 확인하세요",
		//		nChIndex+1);
		//	AfxMessageBox(strErrorMsg);
		// #ifndef _DEBUG
		// 			return;
		// #endif
 		//}
		
		//Step Skip 기능 가능 여부 검사 
		if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSIONV3)		bStartStepOption = FALSE;
		if (ch == 0)	//ljb 20170922 add CAN 설정을 확인 한다. 0 : 설정 없음, 1 : 마스터만 설정, 2: 슬레이브만 설정, 3: 마스터 + 슬레이브 설정
		{
			pCanData = pChannel->GetCanData();	
			if(pCanData == NULL) 
			{
				nCanSetting=0;
			}
			else
			{
				nCanMasterCnt = pMD->GetInstalledCanMaster(nChIndex);
				nCanSlaveCnt = pMD->GetInstalledCanSlave(nChIndex);
				if (nCanMasterCnt > 0 && nCanSlaveCnt > 0) nCanSetting = 1;
				else if (nCanMasterCnt > 0 && nCanSlaveCnt == 0 ) nCanSetting = 2;
				else if (nCanMasterCnt == 0 && nCanSlaveCnt > 0) nCanSetting = 3;
				else nCanSetting=0;
			}
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", nCanSetting);	//ljb 20170922 add
		}
		
		if(bCheckPrevData)
		{
			CWordArray wOneCh;
			wOneCh.Add((WORD)nChIndex);
			
			//작업 정보 입력전에 검사 하여야 한다. (중복되 폴더가 있으면 삭제 되므로 )
			//이전 시험이 data 손실 구간 정보를 Update한다.
			//CCyclerChannel::IsDataLoss() 호출 이전에 최종 정보로 Update 하기 위해 
			pMD->UpdateDataLossState(&wOneCh);	//data량이 많을시 Loading 시간 측정 필요
			//pMD->UpdateDataSimpleLossState(&wOneCh);	//20080318 Data가 많을 경우 많은 시간이 소요되므로 간단하게 변경
			
			if(pChannel->IsDataLoss() == TRUE)
			{
				strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg5","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nModuleID), nChIndex+1, pChannel->GetTestName()); 
				//@ strLogMsg.Format("%s의 Channel %d 에서 시행한 이전 시험 [%s]에 data 손실 구간이 존재합니다.", pDoc->GetModuleName(nModuleID), nChIndex+1, pChannel->GetTestName()); 
				strLogMsg += Fun_FindMsg("OnCmdRun_msg6","IDD_CTSMonPro_FORM");
				//@ strLogMsg += "Data를 복구하기 위해서는 [도구]->[Data 복구...]를 시행하기기 바랍니다.";
				strLogMsg += Fun_FindMsg("OnCmdRun_msg7","IDD_CTSMonPro_FORM");
				//@ strLogMsg += "복구하지 않고 새로운 시험을 시작하게 되면 이전 손실된 data는 복구할 수 없게됩니다.\n";
				strLogMsg += Fun_FindMsg("OnCmdRun_msg8","IDD_CTSMonPro_FORM");
				//@ strLogMsg += "새로운 작업을 시작 하시겠습니까?";
				int nUserSel = MessageBox(strLogMsg, Fun_FindMsg("OnCmdRun_msg9","IDD_CTSMonPro_FORM"), MB_ICONQUESTION|MB_YESNOCANCEL);
				//@ int nUserSel = MessageBox(strLogMsg, "Data 손실", MB_ICONQUESTION|MB_YESNOCANCEL);
				if(nUserSel == IDCANCEL)	
				{
					return;		//Run 명령 취소 
				}
				else if(nUserSel == IDNO)
				{
					//시작 명령을 보내기 위한 선택 목록에서 제외 하여야 한다.
					continue;	//Data를 복구함 
				}
			}
		}
		
		//잠시 멈춤 Channel
		if(pChannel->GetState() == PS_STATE_PAUSE)
		{
			adwPauseCh.Add(MAKELONG(nChIndex, nModuleID));
			TRACE("Pause Module %d ch %d\n", nModuleID, nChIndex);
		}
		
		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);
		strLotNo = pChannel->GetTestSerial();
		
		if(bDownAllRawDataBefRun)//yulee 20190420
		{
			//ksj 20160728 이전 작업의 SBC StepEndData를 다운로드 한다.
			pDoc->m_nFtpDownStepEndModuleID = nModuleID;
			pDoc->m_nFtpDownStepEndChannelIndex = nChIndex;
			//yulee 20190420 이전 작업의 SBC Data를 다운로드 한다.
			//pDoc->m_bRunFtpDownStepEnd = TRUE;
			pDoc->m_bRunFtpDownAllSBCData = TRUE;
			pDoc->m_FTPFileDownRetryCnt = 0;
			pDoc->m_bSucessFtpDownAllSBCData = FALSE;
			pDoc->m_bRunFtpDownStepEnd = TRUE;
			pDoc->m_ucFtpDownIndex = pDoc->m_ucFtpDownIndex | (1 << nChIndex) ; //lyj 20200527 다운로드 개선
		}

		aSelChArray.Add((WORD)nChIndex);
	}

	//lyj 20210720 s =====================
	BOOL bWarningBeforeRun = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "WarningBeforeRun", TRUE);
	if(bWarningBeforeRun)
	{
		CString strMessage;
		strMessage.Format("%s","이전 시험 데이터의 백업이 완전하지 않습니다!");
		pDoc->Fun_ChkCompleteDownAllSBCFile(nModuleID, &aSelChArray);

		if(aSelChArray.GetSize()>0)
		{
			CWorkWarnin WorkWarningDlg;
			WorkWarningDlg.Fun_SetMessage(strMessage);
			WorkWarningDlg.Fun_SetButton_OK("무시하고 계속");
			WorkWarningDlg.Fun_SetButton_Cancle("작업 취소");

			if (WorkWarningDlg.DoModal() == IDOK) 
			{
				if(MessageBox("이전 시험 데이터의 백업이 완료되지 않았습니다.\n새로운 작업 시작시 이전 데이터에 손실이 있는 경우 이전 데이터의 손실 복구가 불가능할 수 있습니다.\n잠시후에 다시 시도해주세요.\n무시하고 새 작업을 실행 하시겠습니까?","Information",MB_YESNO|MB_ICONWARNING) == IDNO)
				{
					return;
				}
			}
			else
			{
				return;
			}
		}
	}
	//lyj 20210720 e =====================
	//만약 Pause인 채널이 있으면 Stop명령을 보낸다.
	if(adwPauseCh.GetSize() > 0)
	{
		if(AfxMessageBox(Fun_FindMsg("OnCmdRun_msg10","IDD_CTSMonPro_FORM"), MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
			//@ if(AfxMessageBox("선택한 채널의 이전 작업을 초기화 하고 새로운 작업을 설정 하시겠습니까?", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
		{
			return;
		}
		if(SendCmdToModule(adwPauseCh, SFT_CMD_STOP) == FALSE)	
		{
			AfxMessageBox(Fun_FindMsg("OnCmdRun_msg11","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox("작업 완료 처리 실패");
			return;
		}
	}
	//이전시험 정상 종료 여부 확인 끝
	//////////////////////////////////////////////////////////////////////////

	SetTimer(TIMER_ALARM_ENABLE, 1000, NULL);

	((CMainFrame *)AfxGetMainWnd())->m_bBlockFlag = TRUE;


	//새롭게 시행할 공정을 선택한다.
	//////////////////////////////////////////////////////////////////////////

	CSelectScheduleDlg	dlg(GetDocument());
	CStartDlg	startDlg(GetDocument(), this);
	startDlg.m_bEnableStepSkip = bStartStepOption;
	startDlg.m_strLot = strLotNo;
	CScheduleData	scheduleInfo;

	Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

	int nRtn = 0;
	long nModelPK = 0;
	long nTestPK = 0;
	while(1)
	{
		dlg.m_nSelModelID = nModelPK; //20190625
		dlg.m_nSelScheduleID = nTestPK; //20190625
		
		if(IDOK != dlg.DoModal())
		{
			break;
		}
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

		nModelPK = dlg.m_nSelModelID;		//선택 모델 PK 
		nTestPK = dlg.m_nSelScheduleID;		//선택 스케쥴 PK 

		//4. 선택한 공정의 조건을 DataBase에서 Load한다.
		//if(scheduleInfo.SetSchedule(pDoc->GetDataBaseName(), nModelPK, nTestPK) == FALSE)
		if(scheduleInfo.SetSchedule(pDoc->GetDataBaseName(), dlg.m_nSelModelID, dlg.m_nSelScheduleID) == FALSE) //20190625
		{
			strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg12","IDD_CTSMonPro_FORM"));
			//@ strLogMsg.Format("공정조건 불러오기 실패!!!");
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			break;
		}
		else
		{
			TRACE("%f\n",scheduleInfo.m_CellCheck.fMaxV);
			TRACE("%f\n",scheduleInfo.m_CellCheck.fMinV);
			TRACE("%f\n",scheduleInfo.m_CellCheck.fCellDeltaV);
			TRACE("%f\n",scheduleInfo.m_CellCheck.fMaxI);
			//if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckCommSafety", FALSE) ==  TRUE)
			//{
				if (scheduleInfo.Fun_ISEmptyCommSafety() == -1)	
				{
					strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg13","IDD_CTSMonPro_FORM"));
					//@ strLogMsg.Format("시험 안전조건이 설정 되어 있지 않습니다. !!!");
					pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strLogMsg);
					break;
				}
				if (scheduleInfo.Fun_ISEmptyCommSafety() == -2)	//20150811 edit
				{
					strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg14","IDD_CTSMonPro_FORM"));
					//@ strLogMsg.Format("시험 안전조건 값이 설정 되어 있지 않습니다. (최대전압, 최대전류, 최대 셀전압편차) !!!");
					pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strLogMsg);
					break;
				}
				if (scheduleInfo.Fun_ISEmptyCommSafety() == -3)	// 211015 HKH 절연모드 충전 관련 기능 보완
				{
					//strLogMsg.Format("시험 안전조건 값이 설정 되어 있지 않습니다. (최소전압) !!!");
					strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg28","IDD_CTSMonPro_FORM"));//&&
					pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strLogMsg);
					break;
				}
			//}
		}
		
		//5. 시험명 및 기타 정보 입력 받아 시험명으로 폴더를 생성한다. 
		startDlg.SetInfoData(&scheduleInfo , &aPtrSelCh);

		startDlg.DoModal();

		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

		if(startDlg.m_nReturn == IDCANCEL)
		{
			break;
		}
		else if(startDlg.m_nReturn == IDOK)
		{
			nRtn = 1;
			break;
		}
		//else if(dlg1.m_nReturn == IDRETRY)
	}

	if(nRtn == 0)
	{
		return;
	}
	//////////////////////////////////////////////////////////////////////////

	//20070831 KJH 스케줄의 Step 값을 확인한다.
	if(GetDocument()->ScheduleValidityCheck(&scheduleInfo, &m_adwTargetChArray) < 0)
	{
		AfxMessageBox(GetDocument()->GetLastErrorString());
		return;
	}

	//5. 입력한 작업명과 저장 위치 확인 
	// c:\\Test_Name 형태 
	strTestPath = startDlg.GetTestPath();
	ASSERT(!strTestPath.IsEmpty());
	strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

	//시험에 적용된 Pattern File 저장 폴더 생성 ljb 20130502 add
	strLogMsg.Format("%s\\Pattern", strTestPath);

	//5.1 Pattern File 폴더를 생성시킨다.
	if( ::CreateDirectory(strLogMsg, NULL) == FALSE )
	{
		strContent.Format(Fun_FindMsg("OnCmdRun_msg15","IDD_CTSMonPro_FORM"), strLogMsg);
		//@ strContent.Format("Pattern 저장용 폴더 생성 실패(%s)", strLogMsg);
		pDoc->WriteSysLog(strContent, CT_LOG_LEVEL_NORMAL);
	}
	strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg16","IDD_CTSMonPro_FORM"), strContent);
	//@ strLogMsg.Format("Data 저장 폴더 생성 성공(%s)", strContent);
	pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_SYSTEM);

	//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("OnCmdRun_msg17","IDD_CTSMonPro_FORM"), TRUE);
	//@ CProgressWnd progressWnd(AfxGetMainWnd(), "전송중...", TRUE);
    progressWnd.GoModal();
	progressWnd.SetRange(0, 100);
	//////////////////////////////////////////////////////////////////////////

	COvenCtrl	*pCtrlOven;				//ljb add
	int			nOvenPortLine;			//ljb add
	CDWordArray adwChArray;
	nSelCount = aPtrSelCh.GetSize();
	int tCh;
	CString strChTrayno;
	CString strChBcrNo;

	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);
		
		if(pChannel == NULL)
		{
			TRACE(Fun_FindMsg("OnCmdRun_msg18","IDD_CTSMonPro_FORM"));
			//@ TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);
		
		//Sleep(200);
		//ljb 20110124 최종상태 체크 주석		//최종 상태 다시 Check
		//if(pDoc->CheckCmdState(nModuleID, nChIndex, SFT_CMD_RUN) == FALSE)			
		//{
		//	TRACE("M%dCH%d는 작업을 시작할 수 없는 상태 입니다.\n", nModuleID, nChIndex+1);
		//	strTemp.Format("시작명령 전송전 최종상태 Check : M%dCH%d는 작업을 시작할 수 없는 상태 입니다.\n", nModuleID, nChIndex+1);
		//	pDoc->WriteSysLog(strTemp);
		//	continue;
		//}
		
		adwChArray.Add(MAKELONG(nChIndex, nModuleID));
		
		//6. 선택된 Channel을 시험준비 한다.
		progressWnd.SetPos(0);
		strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg19","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nModuleID), nChIndex+1);
		//@ strLogMsg.Format("%s 의 채널 %d 결과 폴더 생성...", pDoc->GetModuleName(nModuleID), nChIndex+1);
		progressWnd.SetText(strLogMsg);
		progressWnd.SetPos(int((float)tCh/(float)nSelCount*100.0f));
		
		
		//Cancel 처리
		if(progressWnd.PeekAndPump() == FALSE)				break;

		
		//CStartDlg::OnOk()에서 test_name folder는 생성한다.
		//c:\\Test_Name\\M01C01 형태
		int nStartChNo = GetStartChNo(nModuleID);
		strContent.Format("%s\\M%02dCh%02d[%03d]", strTestPath, nModuleID, nChIndex+1, nStartChNo+nChIndex);
		
		//6.1 Channel 폴더를 생성시킨다.
		if (pDoc->ForceDirectory(strContent) == FALSE)
		{
			strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg20","IDD_CTSMonPro_FORM"), strContent);
			//@ strLogMsg.Format("Data 저장 폴더 생성 실패(%s)", strContent);
			AfxMessageBox(strLogMsg);
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			return;
		}
		else
		{
			strLogMsg.Format("%s\\StepStart", strContent);
			pDoc->ForceDirectory(strLogMsg);

			//ksj 20170810 : Update 폴더 생성 추가
			strLogMsg.Format("%s\\Update", strContent);
			if(pDoc->ForceDirectory(strLogMsg))
			{
				strLogMsg.Format("%s\\Update\\original", strContent);
				pDoc->ForceDirectory(strLogMsg);
			}
		}
		Sleep(100);
		
		//6.2 각 폴더에 스케줄 파일을 생성한다.
		// c:\\Test_Name\M01C01\Test_Name.sch
		strScheduleFileName = strContent + "\\" + strTestName+ ".sch";
		log_All(_T("sbc"),_T("sched"),strScheduleFileName);
		//cny---------------------------------------------------

		if(FALSE == scheduleInfo.SaveToFile(strScheduleFileName))
		{
			strLogMsg.Format(Fun_FindMsg("OnCmdRun_msg21","IDD_CTSMonPro_FORM"), strTestName);
			//@ strLogMsg.Format("%s Schedule 파일 생성 실패", strTestName);
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			continue;
		}

		//ksj 20170810 : Update 폴더에 원본 sch 파일을 추가로 저장.
		CString strCopyFile = strContent + "\\Update\\original\\" + strTestName+ ".sch";
		CopyFile(strScheduleFileName, strCopyFile, FALSE);
		
		strChTrayno = pChannel->GetTestTrayNo();
		strChBcrNo = pChannel->GetTestBcrNo();
		pChannel->ResetData();
		//6.3 시험명 설정
 		pChannel->SetFilePath(strContent);	//ljb 20130626 수정 (함수내부)		

 		//Set Test Lot No
		pChannel->SetTestSerial(startDlg.m_strLot, startDlg.m_strWorker, startDlg.m_strComment,pChannel->m_strCellDeltaVolt);
		pChannel->SetTestSerial2(strChTrayno, strChBcrNo);
		pChannel->LoadSaveItemSet();
		
		//6.4 Aux Title 파일 생성
		pChannel->SetAuxTitleFile(strContent, strTestName);
		//6.5 Can Title 파일 생성
		pChannel->SetCanTitleFile(strContent, strTestName);
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

		//ljb 20130404 CTS File 생성
		pChannel->WriteCtsFile();

		CString strProtocolVer;
		CString strSBCVer;
		

		//strProtocolVer.Format("Protocol ver %x", pMD->GetProtocolVer());
		strProtocolVer.Format("Protocol ver: %x, S/W ver: %s", pMD->GetProtocolVer(), PROGRAM_VER); //ksj 20201112 : 프로그램 버전 정보도 채널 로그에 남기기
		pChannel->WriteLog(strProtocolVer);

		SFT_MD_SYSTEM_DATA* pSysData = ::SFTGetModuleSysData(nModuleID); //ksj 20201112 : SBC 버전 정보 추가
		if(pSysData)  //ksj 20201112 : SBC 버전 정보 추가
		{
			//sbc 정보 출력
			strSBCVer.Format("Sbc_Ip: %s, SystemType: %d, ProtocolVer: %x, OSver: %d, Can_Type:%d, Total_Board_Count:%d, Channel_Per_Board:%d, Install_Channel_Count:%d, ModelName: %s"
				,pMD->GetIPAddress(),pSysData->nSystemType,pSysData->nProtocolVersion,pSysData->nOSVersion,pSysData->byCanCommType,pSysData->wInstalledBoard,pSysData->wChannelPerBoard,pSysData->nInstalledChCount,pSysData->szModelName);

			pChannel->WriteLog(strSBCVer);
			
			//spec 출력
			strSBCVer.Format("Spec Range: ");			
			for(int nVSpecRangeIdx=0; nVSpecRangeIdx<pSysData->wVoltageRange && nVSpecRangeIdx<5;nVSpecRangeIdx++)
			{
				strTemp.Format("V%d: %dV, ",nVSpecRangeIdx+1, (int)(pSysData->nVoltageSpec[nVSpecRangeIdx]/1000000.0f));
				strSBCVer += strTemp;
			}
			for(int nISpecRangeIdx=0; nISpecRangeIdx<pSysData->wCurrentRange && nISpecRangeIdx<5;nISpecRangeIdx++)
			{
				strTemp.Format("I%d: %dA, ",nISpecRangeIdx+1, (int)(pSysData->nCurrentSpec[nISpecRangeIdx]/1000000.0f));
				strSBCVer += strTemp;
			}

			pChannel->WriteLog(strSBCVer);
		}

		//ksj 20201112 : 강제 병렬 여부 로깅
		strTemp.Format("Force Parallel : %s", (g_AppInfo.iPType)?"YES":"NO");
		pChannel->WriteLog(strTemp);

		//ljb 20170403 작업시작 정보 write
		//pChannel->WriteLog();
		
		//ljb 20160314 BCR Data write
		CFileFind dbFinder;
		if(dbFinder.FindFile(GetDocument()->GetWorkInfoDataBaseName()))	//20160314   Work_Info_2000.mdb
		{
			BOOL bInsertWorkInfo = FALSE;
			
			bInsertWorkInfo = InsertWorkInfo(pChannel->GetModuleID(), pChannel->GetChannelIndex()+1, pChannel->GetScheduleName(), pChannel->GetTestName(), pChannel->GetTestPath(), strChTrayno, strChBcrNo, pChannel->GetTestSerial());
			if(bInsertWorkInfo == FALSE)
			{
				//pDoc->WriteSysLog("CH%02d bar-code Information save fail", pChannel->GetChannelIndex()+1);
				strLogMsg.Format("CH%02d bar-code Information save fail", pChannel->GetChannelIndex()+1);
				AfxMessageBox(strLogMsg);
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			}
		}

		//ljb 20131030 add //////////////////////////////////////////
		UpdateData();
		CDWordArray awOvenArray;
		for(int ch = 0; ch<nSelCount; ch++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(ch);
			nModuleID = HIWORD(dwData);

			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.14
			int nChamberOperationType=0; // 0 : 삼성, 1 : SK 
			// 기본적으로 0을 사용한다. 1채널이든 2채널 장비든 1개의 챔버를 사용한다. 
			//            1은 KTL 과 SK 에서 2ch 장비에서 채널별로 챔버를 연동시키고자 할 때 사용한다.
			nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 0);

			if(nChamberOperationType == 0)
			{
				if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == TRUE)
				{
					pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven1;
					pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
					nOvenPortLine = 1;
				}
				else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == TRUE)
				{
					pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
					pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
					nOvenPortLine = 2;				
				}
			}
			else
			{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
				{
					if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == true && nChIndex == 0) //yulee 20181213 병렬 mark
					{
						pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven1;
						pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
						nOvenPortLine = 1;
					}
					else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == true && nChIndex == 1) //yulee 20181213 병렬 mark
					{
						pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
						pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
						nOvenPortLine = 2;				
					}
				
				}
//#else
			else
				{
				//대전 SK용 20140122 //20180510 yulee 채널 개수와 챔버 컨트롤러의 개수에 따른 동작 
				//CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
				CCyclerModule * pMD = GetDocument()->GetModuleInfo(nModuleID); //yulee 20180729
				int nTotalCount = pMD->GetTotalChannel();
					if(nTotalCount == 2)//충방전기2채널 - 오븐 콘트롤러 2채널 사용 시 각각 제어 할 수 있도록  
					{
						if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == TRUE && nChIndex == 0)
						{
							pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven1;
							pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
							nOvenPortLine = 1;
						}
						else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == TRUE && nChIndex == 1)
						{
							pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
							pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
							nOvenPortLine = 2;				
						}
					}
					else if(nTotalCount == 4) //충방전기4채널 - 오븐 콘트롤러 2채널 사용 시 각각 제어 할 수 있도록 1,2채널은 1컨트롤러 3,4채널은 2 컨트롤러  
					{
						if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == TRUE && (nChIndex == 0 || nChIndex == 1))
						{
							pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven1;
							pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
							nOvenPortLine = 1;
						}
						else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == TRUE && (nChIndex == 2 || nChIndex == 3))
						{
							pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
							pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
							nOvenPortLine = 2;				
						}
					}
				//if (pDoc->m_ctrlOven1.m_bUseOvenCtrl == true && nChIndex == 0)
				//{
				//	pCtrlOven = &pDoc->m_ctrlOven1;
				//	pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
				//	nOvenPortLine = 1;
				//}
				//else if (pDoc->m_ctrlOven2.m_bUseOvenCtrl == true && nChIndex == 1)
				//{
				//	pCtrlOven = &pDoc->m_ctrlOven2;
				//	pCtrlOven->GetOvenIndexList(adwChArray,awOvenArray);
				//	nOvenPortLine = 2;				
				//}
				}
//#endif
			}
			//////////////////////////////////////////////////////////////////////////
		}
		//ljb 20111118 챔버 연동을 위한 챔버 구동 ----------------------------------------------------------------------------
		int iSize = adwChArray.GetSize();
		//Send Oven Start Command
		DWORD dwData;
		pChannel->ChamberValueInit();

		for(int iOvenCnt = 0; iOvenCnt < awOvenArray.GetSize(); iOvenCnt++)
		{
			dwData = 0;
			dwData = awOvenArray.GetAt(iOvenCnt);
			if(pCtrlOven->m_bUseOvenCtrl)
			{
				pChannel->m_nStartOptChamber = startDlg.m_nStartOptChamber;
				if (startDlg.m_nStartOptChamber == 1)	//ljb 챔버 Fix Mode
				{
					pDoc->OvenCheckFixOption(nOvenPortLine, startDlg.m_uiChamberCheckFixTime,startDlg.m_fChamberDeltaFixTemp);
					pDoc->OvenStartStateAndSendStartCmd(nOvenPortLine, 1,0,startDlg.m_fChamberFixTemp,dwData);
					pChannel->m_uiChamberCheckFixTime = startDlg.m_uiChamberCheckFixTime;
					pChannel->m_fChamberDeltaFixTemp = startDlg.m_fChamberDeltaFixTemp;
					pChannel->m_fchamberFixTemp;
				}
				else if (startDlg.m_nStartOptChamber == 2)	//ljb 챔버 Prog Mode
				{
					pDoc->OvenStartStateAndSendStartCmd(nOvenPortLine, 2,startDlg.m_uiChamberPatternNum,0,dwData);
					pChannel->m_uichamberPatternNum = startDlg.m_uiChamberPatternNum;
				}
				else if (startDlg.m_nStartOptChamber == 3)	//ljb 챔버 스케쥴 연동 Mode
				{
					startDlg.m_uiChamberCheckTime;	//챔버 온도 비교 시작 시간 1초는 == 100
					startDlg.m_fChamberDeltaTemp;	//챔버 비교 온도
					
					pDoc->OvenCheckOption(nOvenPortLine, startDlg.m_uiChamberCheckTime,startDlg.m_fChamberDeltaTemp);
					pDoc->OvenStartStateAndSendStartCmd(nOvenPortLine, 3,99,0,dwData);

					pChannel->m_uiChamberCheckTime = startDlg.m_uiChamberCheckTime;
					pChannel->m_fchamberDeltaTemp = startDlg.m_fChamberDeltaTemp;					
				}
				else if(startDlg.m_nStartOptChamber == 5)	//yulee 20180820 //챔버 사용 안함(챔버 중지)
				{
					//1. 채널이 할당된 챔버의 번호를 가져온다. 20180515 yulee 
					//2. 그 챔버가 동작 중인지를 확인한다. 
					//3. 동작 중이면 챔버 관련 컨트롤러를 비활성화 시킨다. 
					
					int nModuleID, nChIndex, nOvenID = -1, i;
					//int nOvenIndex;
					for(int i = 0; i< aPtrSelCh.GetSize(); i++)
					{
						CCyclerChannel *pCh = (CCyclerChannel *)aPtrSelCh.GetAt(i);
						nModuleID = pCh->GetModuleID();
						nChIndex = pCh->GetChannelIndex();
					}
					
					CDWordArray adwSelChList, adwOvenList;
					pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndexList(adwSelChList , adwOvenList);
					
					COvenCtrl *pCtrlOven;
					nOvenID = pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndex(nModuleID, nChIndex);
					if(nOvenID != -1)
					{
						nOvenID = pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndex(nModuleID, nChIndex);
						pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven1;
					}
					else
					{
						nOvenID = pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndex(nModuleID, nChIndex);
						if(nOvenID != -1)
						{
							nOvenID =pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndex(nModuleID, nChIndex);
							pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
						}
					}
					
					
					BOOL bOvenRun = FALSE;
					BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven", FALSE);
					
					if(bUseOven != FALSE)
					{
						if(pCtrlOven->GetRun(nOvenID))	//챔버 동작 중
						{
							if(pCtrlOven->SendStopCmd(0, nOvenID) == FALSE)
							{
								//strTemp = "챔버 STOP 명령 실패!!!";
								strTemp = Fun_FindMsg("CTSMonProView_OnCmdRun_msg5","IDD_CTSMonPro_FORM");//&&
								pDoc->WriteSysLog(strTemp);
							}
						}
					}
				}	
			}
			
			//if (startDlg.m_nStartOptChamber != 4 && startDlg.m_uiChamberDelayTime > 0)
			if (((startDlg.m_nStartOptChamber != 4) && (startDlg.m_nStartOptChamber != 5)) && startDlg.m_uiChamberDelayTime > 0) //yulee 20180820
			{
				strTemp.Format(Fun_FindMsg("OnCmdRun_msg22","IDD_CTSMonPro_FORM"));
				//@ strTemp.Format("챔버 Run 후 대기 중...");
				progressWnd.SetText(strTemp);
				
				nRtn = 0;
				CReadyRunDlg Readydlg(GetDocument());
				while(1)
				{
					Readydlg.m_nReayTime = startDlg.m_uiChamberDelayTime;
					
					if(IDOK == Readydlg.DoModal())
					{
						nRtn = 1;
						break;
					}
					break;
				}
				if(nRtn == 0)
				{
					return;
				}
			}
			
			//챔버 동작 확인
			//if (startDlg.m_nStartOptChamber != 4)
			if((startDlg.m_nStartOptChamber != 4) && (startDlg.m_nStartOptChamber != 5))//yulee 20180820
			{
				if(pCtrlOven->GetRun(dwData) == FALSE)	//챔버 동작 중
				{
					strTemp = Fun_FindMsg("OnCmdRun_msg23","IDD_CTSMonPro_FORM");
					//@ strTemp = "챔버 동작을 확인 하세요. 충방전을 진행 하시겠습니까 ?";
					if(IDNO == MessageBox(strTemp, Fun_FindMsg("OnCmdRun_msg24","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
						//@ if(IDNO == MessageBox(strTemp, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
					{
						return;
					}
					else
					{
						Sleep(5000);
						
						//챔버 동작 확인 2
						//if (startDlg.m_nStartOptChamber != 4)
						if((startDlg.m_nStartOptChamber != 4) && (startDlg.m_nStartOptChamber != 5))//yulee 20180820
						{
							if(pCtrlOven->GetRun(dwData) == FALSE)	//챔버 동작 중
							{
								//strTemp = "챔버 동작을 확인 하세요.";
								strTemp = Fun_FindMsg("CTSMonProView_OnCmdRun_msg6","IDD_CTSMonPro_FORM");//&&
								AfxMessageBox(strTemp);
								//strTemp = "챔버 동작을 확인 하세요. 챔버 이상동작 감지. 챔버연동이나 챔버 시작 안함.";
								strTemp = Fun_FindMsg("CTSMonProView_OnCmdRun_msg7","IDD_CTSMonPro_FORM");//&&
								pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
								//AfxMessageBox(strTestName + " 작업 시작을 실패 하였습니다.(챔버연동 이상)");
								AfxMessageBox(strTestName + Fun_FindMsg("CTSMonProView_OnCmdRun_msg8","IDD_CTSMonPro_FORM"));//&&
#ifdef _DEBUG
#else
								return;
#endif
							}
						}
					}
				}
			}
		}

		//챔버연동 설정으로 작업이 시작 될 시 m_parallelData 의 reserved[0]의 값을 업데이트 한다. 
		// 		for(int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
		// 		{
		SFT_MD_PARALLEL_DATA * pParaData; //yulee 20190322
		int ChIndex;
		pParaData = pMD->GetParallelData();
		pChannel->WriteParallelStateLog(pParaData); //ksj 20200203 : 채널로그에 병렬 상태 남긴다.

		ChIndex = pChannel->GetChannelIndex();
		if( ChIndex < pMD->GetTotalChannel())
		{
			if((startDlg.m_nStartOptChamber != 4) && (startDlg.m_nStartOptChamber != 5))
			{
				pParaData[ChIndex].reserved[0] = TRUE;	
				CString tmpStr;
				tmpStr.Format(_T("Ch %d는 Chamber 연동 동작, chamber_control flag ON : %d"), pChannel->GetChannelIndex()+1, pParaData[ChIndex].reserved[0]); 
				pDoc->WriteSysEventLog(tmpStr);	//ljb 201012
				pDoc->WriteSysLog(tmpStr);
				pDoc->WriteLog(tmpStr);
			}
			else
			{
				pParaData[ChIndex].reserved[0] = FALSE;	
				CString tmpStr;
				tmpStr.Format(_T("Ch %d는 Chamber 연동 동작 아님, chamber_control flag OFF : %d"), pChannel->GetChannelIndex()+1, pParaData[ChIndex].reserved[0]); 
				pDoc->WriteSysEventLog(tmpStr);	//ljb 201012
				pDoc->WriteSysLog(tmpStr);
				pDoc->WriteLog(tmpStr);
			}
		}
		/*}*/
		//--------------------------------------------------------------------------------------------------------------------
	}
	Sleep(200);	//모듈 상태 전이를 위한 시간 지연 
	if(pChannel)
	pChannel->Fun_SetFirstNgState(FALSE);//yulee 20190312

	//충방전 Start	
	if(pDoc->SendStartCmd(&progressWnd, &adwChArray, &scheduleInfo, startDlg.m_nStartCycle, startDlg.m_nStartStep,startDlg.m_nStartOptChamber) == FALSE)
	{

		//bRunOK = FALSE;
		strTemp.Format("%s %s",strTestName,  Fun_FindMsg("OnCmdRun_msg25","IDD_CTSMonPro_FORM"));
		AfxMessageBox(strTemp);
		//@ AfxMessageBox(strTestName + " 작업 시작을 실패 하였습니다.");
	}
	else
	{
		//////////////////////////////////////////////////yulee 20181202
		
		for(int i=0; i< aPtrSelCh.GetSize(); i++)
		{
			CCyclerChannel *pCh = (CCyclerChannel *)aPtrSelCh.GetAt(i);
			
			CString RegName;
			RegName.Format("OnProgressSimpleTest_%d", pCh->m_iCh);

			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, RegName, 0);
		}
		//////////////////////////////////////////////////
		//AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "OnProgressSimpleTest", 0);
		strTemp.Format(Fun_FindMsg("OnCmdRun_msg26","IDD_CTSMonPro_FORM"));
		//@ strTemp.Format("스케쥴 전송 완료");
		progressWnd.SetText(strTemp);
		progressWnd.SetPos(100);

		int nModuleID, nChIndex;  //yulee 20181008-1 로그 보강
		for(int i=0; i< aPtrSelCh.GetSize(); i++) 
		{
			CCyclerChannel *pCh = (CCyclerChannel *)aPtrSelCh.GetAt(i);
			nModuleID = pCh->GetModuleID();
			nChIndex = pCh->GetChannelIndex();
			switch(nChIndex)
			{
				case 0:
					{
						m_onPauseLog1 = TRUE;
						break;
					}
				case 1:
					{
						m_onPauseLog2 = TRUE;
						break;
					}
				case 2:
					{
						m_onPauseLog3 = TRUE;
						break;
					}
				case 3:
					{
						m_onPauseLog4 = TRUE;
						break;
					}
			}
		}
	}

	Sleep(200);

	((CMainFrame *)AfxGetMainWnd())->m_bBlockFlag = FALSE;
}

void CCTSMonProView::OnCmdPause() 
{
	CString strMsg;
	
	//Warning message for Samsung QC Cycler
	CWorkWarnin WorkWarningDlg;
	strMsg = Fun_FindMsg("OnCmdPause_msg1","IDD_CTSMonPro_FORM");
	//@ strMsg = "작업을 잠시멈춤 하시겠습니까?";
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;

// 	if(&m_wndModuleList == (CListCtrl *)GetFocus())
// 	{
// 		strMsg.Format("선택한 모듈의 모든 채널에 [%s]명령을 전송하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
// 		if(MessageBox(strMsg, "모듈명령", MB_ICONQUESTION|MB_YESNO) == IDYES)
// 		{
// 			SendCmdToModule(SFT_CMD_PAUSE);
// 		}
// 		return;
// 	}

	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdPause_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}

	strMsg.Format(Fun_FindMsg("OnCmdPause_msg3","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	//@ strMsg.Format("[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, Fun_FindMsg("OnCmdPause_msg4","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
		return;

	//3. Send Command
	if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE)) 
		AfxMessageBox(Fun_FindMsg("OnCmdPause_msg5","IDD_CTSMonPro_FORM"));
	//@ if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE)) AfxMessageBox("전송 실패");
}

void CCTSMonProView::OnCmdContinue() 
{
	CString strMsg;
// 	if(&m_wndModuleList == (CListCtrl *)GetFocus())
// 	{
// 		strMsg.Format("선택한 모듈의 모든 채널에 [%s]명령을 전송하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_CONTINUE));
// 		if(MessageBox(strMsg, "모듈명령", MB_ICONQUESTION|MB_YESNO) == IDYES)
// 		{
// 			SendCmdToModule(SFT_CMD_CONTINUE);
// 		}
// 		return;
// 	}

	//1. 선택된 채널 중 Continue 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_CONTINUE)  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdContinue_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}

	//2. Check to user
	strMsg.Format(Fun_FindMsg("OnCmdContinue_msg2","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_CONTINUE));
	//@ strMsg.Format("[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_CONTINUE));
	if(IDYES!=MessageBox(strMsg, Fun_FindMsg("OnCmdContinue_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES!=MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
		return;
	
	//3. Send Command
	if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_CONTINUE)) AfxMessageBox(Fun_FindMsg("OnCmdContinue_msg4","IDD_CTSMonPro_FORM"));
	//@ if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_CONTINUE)) AfxMessageBox("전송 실패");
	
// 	m_FPreFickerInfo.nPriority = 3; //lyj 20201007 Flicer 초기화
// 	m_FPreFickerInfo.nCode = 0; //lyj 20201007 Flicker 초기화
// 	m_FPreFickerInfo.nAutoBuzzerStop = 0; //ksj 20201013 : 초기화 추가
	InitPreFlickerInfo(); //ksj 20201013

	SetTimer(TIMER_ALARM_ENABLE, 1000, NULL);
}

void CCTSMonProView::OnCmdStop() 
{
	//yulee 20181008-1 로그 보강
	CCyclerModule *lpModule = NULL;
	CCyclerChannel *lpChannelInfo;
	CCTSMonProDoc *pDoc = GetDocument();
	UINT nModuleID, nChannelIndex,nStartCh;
	for( int nI = 0; nI < m_wndChannelList.GetItemCount() ; nI++ )	//channel index
	{
		DWORD dwID = m_wndChannelList.GetItemData(nI);
		nModuleID = HIWORD(dwID);
		nChannelIndex = LOWORD(dwID);
		nStartCh = GetStartChNo(nModuleID);
		
		lpModule = pDoc->GetModuleInfo(nModuleID);
		if(lpModule == NULL)	continue;
		lpChannelInfo = lpModule->GetChannelInfo(nChannelIndex);
		if(lpChannelInfo == NULL )	continue;
		
		lpChannelInfo->Fun_SetFirstNgState(FALSE);
		switch(nChannelIndex)
		{
		case 0:
			{
				m_onPauseLog1 = FALSE;
				break;
			}
		case 1:
			{
				m_onPauseLog2 = FALSE;
				break;
			}
		case 2:
			{
				m_onPauseLog3 = FALSE;
				break;
			}
		case 3:
			{
				m_onPauseLog4 = FALSE;
				break;
			}
		}
	}

	CString strMsg;

	//Warning message for Samsung QC Cycler
	CWorkWarnin WorkWarningDlg;
	strMsg = Fun_FindMsg("OnCmdStop_msg1","IDD_CTSMonPro_FORM");
	//@ strMsg = "작업을 완료하면 이후 새로운 작업을 해야 합니다.";
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;

	//Stop시 먼저 Pause로 보내고 완전종료 할 것인지 확인하는 방식으로 변경
	//1. 선택된 채널 중 Pause 명령이 전송가능한 채널을 Update 한다.
//	CDWordArray arTargetCh;
	if(UpdateSelectedChList(SFT_CMD_PAUSE))
	{
		strMsg.Format(Fun_FindMsg("OnCmdStop_msg2","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_PAUSE));
		//@ strMsg.Format("진행중인 시험을 중지 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
		if(IDYES!=MessageBox(strMsg, Fun_FindMsg("OnCmdStop_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
			//@ if(IDYES!=MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
			return;
		//lyj 20210714 s 완료 전 일시정지 시 플래그 ON ===============
		for(int i = 0; i < m_adwTargetChArray.GetSize(); i++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(i);
			INT nModuleID = HIWORD(dwData);
			INT nChannelIndex = LOWORD(dwData);

			lpModule = pDoc->GetModuleInfo(nModuleID);
			if(lpModule == NULL)	continue;
			lpChannelInfo = lpModule->GetChannelInfo(nChannelIndex);
			if(lpChannelInfo == NULL )	continue;
			lpChannelInfo->SetPauseBeforeStop(TRUE);
		}
		//lyj 20210714 e 완료 전 일시정지 시 플래그 ON ===============
		//2. Send Pause Command
		SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE);
		
		
		//상태가 전이 되기를 기다린다.
		Sleep(2000);
		strMsg.Format(Fun_FindMsg("OnCmdStop_msg4","IDD_CTSMonPro_FORM"), 
			//@ strMsg.Format("선택된 채널이 모두 [%s]상태로 변경되었습니다. 진행중인 시험을 [%s] 시킵니다.\n[%s]을 실행하시겠습니까?", 
			GetDocument()->GetCmdString(SFT_CMD_PAUSE), 
			GetDocument()->GetCmdString(SFT_CMD_STOP), 
			GetDocument()->GetCmdString(SFT_CMD_STOP) 
			);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnCmdStop_msg5","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_STOP), GetDocument()->GetCmdString(SFT_CMD_STOP));
		//@ strMsg.Format("진행중인 시험을 [%s] 시킵니다.\n[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_STOP), GetDocument()->GetCmdString(SFT_CMD_STOP));
	}
		//Sleep(200);
	
	if(IDYES!=MessageBox(strMsg, Fun_FindMsg("OnCmdStop_msg7","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)) 
		//@ if(IDYES!=MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)) 
		return;

	//3. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdStop_msg6","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}
	
	//4. Send Stop Command
	//상태가 전이 되기를 기다린다.
//	Sleep(200);
	if(FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_STOP)) AfxMessageBox(Fun_FindMsg("OnCmdStop_msg8","IDD_CTSMonPro_FORM"));
	//@ if(FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_STOP)) AfxMessageBox("전송 실패");

	if(GetDocument()->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl)
	{
		GetDocument()->CheckOvenStopStateAndSendStopCmd(m_adwTargetChArray, 1);	//모든 연동 Channel이 Stop 상태이고 Oven이 동작중인 경우 
	}

	if(GetDocument()->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl) //20180515 yulee
	{
		GetDocument()->CheckOvenStopStateAndSendStopCmd(m_adwTargetChArray, 2);	//모든 연동 Channel이 Stop 상태이고 Oven이 동작중인 경우 
	}

	// Added by 224 (2014/10/20) : LIN 작업 종료
	for(int i = 0; i < m_adwTargetChArray.GetSize(); i++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(i);
		INT nModuleID = HIWORD(dwData);
		INT nChannelIndex = LOWORD(dwData);
		
		INT nDevID = (nModuleID-1) * 2 + nChannelIndex ;
		GetDocument()->StopLinCanConvThread(nDevID) ;
		lpModule = pDoc->GetModuleInfo(nModuleID);
		lpChannelInfo = lpModule->GetChannelInfo(nChannelIndex);
		lpChannelInfo->SetPauseBeforeStop(FALSE); //lyj 20210714 완료 전 일시정지 시 플래그 OFF
	}

// 	m_FPreFickerInfo.nCode = 0; //lyj 20201007 Flicker 초기화
// 	m_FPreFickerInfo.nPriority = 3; //lyj 20201007 Flicker 초기화
// 	m_FPreFickerInfo.nAutoBuzzerStop = 0; //ksj 20201013 : 초기화 추가
	InitPreFlickerInfo(); //ksj 20201013

	SetTimer(TIMER_ALARM_ENABLE, 1000, NULL);
}
void CCTSMonProView::OnCmdNextStep() 
{
	// TODO: Add your command handler code here
	CString strMsg;
// 	if(&m_wndModuleList == (CListCtrl *)GetFocus())
// 	{
// 		strMsg.Format("선택한 모듈의 모든 채널에 [%s]명령을 전송하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_NEXTSTEP));
// 		if(MessageBox(strMsg, "모듈명령", MB_ICONQUESTION|MB_YESNO) == IDYES)
// 		{
// 			SendCmdToModule(SFT_CMD_NEXTSTEP);
// 		}
// 		return;
// 	}

	//1. 선택된 채널 중 NEXT 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_NEXTSTEP)  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdNextStep_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}
	
	//2. Check to user
	strMsg.Format(Fun_FindMsg("OnCmdNextStep_msg2","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_NEXTSTEP));
	//@ strMsg.Format("[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_NEXTSTEP));
	if(IDYES != MessageBox(strMsg, Fun_FindMsg("OnCmdNextStep_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
		return;

	DWORD dwData;
	CWaitCursor wait;
	int nModuleID, nChannelIndex;
	for(int s=0; s<m_adwTargetChArray.GetSize(); s++)
	{
		dwData = m_adwTargetChArray.GetAt(s);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);

		//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 0
		CString RegName, strTemp;
		RegName.Format("OvenContinueSend_ch%d",nChannelIndex + 1);
		BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 0);
		strTemp.Format(" %s registry %s Write 0", OVEN_SETTING, RegName);
		log_All(_T("sbc"),_T("next"),strTemp);
	}
	
	//3. Send Command
	if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_NEXTSTEP)) AfxMessageBox(Fun_FindMsg("OnCmdNextStep_msg4","IDD_CTSMonPro_FORM"));
	//@ if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_NEXTSTEP)) AfxMessageBox("전송 실패");

	SetTimer(TIMER_ALARM_ENABLE, 1000, NULL);
}

BOOL CCTSMonProView::onSbcNextStep(int nModuleID,int nChannelIndex) 
{	
	m_adwTargetChArray.RemoveAll();
	//m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
	
	if(GetDocument()->CheckCmdState(nModuleID, nChannelIndex, SFT_CMD_NEXTSTEP))
	{
		m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
	}
	
	BOOL bRet=SendCmdToModule(m_adwTargetChArray, SFT_CMD_NEXTSTEP);
	
	CString strTemp;
	strTemp.Format(_T("fore next step ret:%d mod:%d ch:%d"),bRet,nModuleID,nChannelIndex);
	log_All(_T("sbc"),_T("next"),strTemp);

	return bRet;
}

BOOL CCTSMonProView::onSbcPause(int nModuleID,int nChannelIndex) 
{	
	m_adwTargetChArray.RemoveAll();
	//m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
	
	if(GetDocument()->CheckCmdState(nModuleID, nChannelIndex, SFT_CMD_PAUSE))
	{
		m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
	}
	
	BOOL bRet=SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE);
	
	CString strTemp;
	strTemp.Format(_T("fore pause step ret:%d mod:%d ch:%d"),bRet,nModuleID,nChannelIndex);
	log_All(_T("sbc"),_T("next"),strTemp);
	return bRet;
}

void CCTSMonProView::OnManagePattern() 
{
	// Get Current Directory Path
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);

	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	CString strPatternPath, strDataPath;
	strPatternPath.Format("%s\\CTSEditorPro.exe", path);
	GetDocument()->ExecuteProgram(strPatternPath, "", "", "CTSEditorPro");
}




void CCTSMonProView::OnGetdispinfoListChannel(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
//	 LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;

//    long index = pDispInfo->item.iItem;
//    long subItem = pDispInfo->item.iSubItem;
//    long objCode = pDispInfo->item.lParam;

	//깜박거림이 심하여 UpdateChListData() 에서 변경하도록 수정 
	//2006/04/11 KBH

/*	UINT nChannelIndex;
	DWORD dwImgIndex;
	if(pDispInfo->item.mask & LVIF_IMAGE)
    {
		nChannelIndex = m_wndChannelList.GetItemData(pDispInfo->item.iItem);
		CCyclerChannel *pCh = GetDocument()->GetChannelInfo(m_nCurrentModuleID, nChannelIndex);
		if(pCh)
		{
			if(pDispInfo->item.iSubItem == CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CHANNEL_NO))
			{
				dwImgIndex = GetDocument()->GetStateImageIndex(pCh->GetState(), pCh->GetStepType());
				pDispInfo->item.iImage = dwImgIndex;
			}
			//자동 detect 된 경우만 표시됨 
			if(pCh->IsDataLoss() && pDispInfo->item.iSubItem == CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STATE))
			{
				pDispInfo->item.iImage = GetDocument()->GetStateImageIndex(PS_STATE_FAIL);
			}

			//////////////////////////////////////////////////////////////////////////
			//Reference 설정에서 범위 벗어난 채널 색상 표시 위해 
			PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData(FALSE);
			if(pColorData)
			{
				if(pColorData->bShowOver)
				{
					float fVoltage, fCurrent;
					CScheduleData schData;
					if(schData.SetSchedule(pCh->GetScheduleFile()))
					{
						CStep *pStep = schData.GetStepData(pCh->GetStepNo()-1);
						if(pStep)
						{
							//충전중 전압이 설정범위를 벗어남
							if( fabs(fVoltage - pStep->m_fVref) > pColorData->VOverConfig.fValue 
								&& pStep->m_type == PS_STEP_CHARGE 
								&& pColorData->VOverConfig.fValue > 0.0f
								&& pDispInfo->item.iSubItem == CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_VOLTAGE)
								)
							{
								pDispInfo->item.iImage = GetDocument()->GetStateImageIndex(PS_STATE_FAIL);
							}
							
							//충방전 중 전류가 설정범위를 벗어남
							if( fabs(fCurrent - pStep->m_fIref) > pColorData->IOverConfig.fValue 
								&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE) 
								&& pColorData->IOverConfig.fValue > 0.0f
								&& pDispInfo->item.iSubItem == CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CURRENT))
							{
								pDispInfo->item.iImage = GetDocument()->GetStateImageIndex(PS_STATE_FAIL);
							}
						}
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////
		}

//		if(pDispInfo->item.state & LVIS_SELECTED)
//		{
//			dwImgIndex += 2;
//		}
 
		*pResult = 1; // 프로그램상으로 처리했다는 뜻으로 *pResult = 1 로 합니다.
		return;
    }
*/	*pResult = 0;

}


/*void CMyDlg::GetTextCallback(int iIndex, int iSubItem, long lCode, CString& cs)
{
    int lastId = m_AreaList.Count()-1;
    tAREA *pArea = m_AreaList.Goto(lCode);
    if (pArea == NULL) return;

    CString fm;
    double area;
    cs = "";
    int columnId = m_ListCtrl.GetColumnIndex(iSubItem);

    switch (columnId) 
    {
    case 1:
        cs = pArea->name;
        break;
    case 2:
        if (pArea->no == 0) cs = "소계";
        else if (pArea->no == -1) cs = "합계";
        else cs.Format("%d", pArea->no);
        break;
    case 3:
        cs.Format("%s", pArea->string);
        break;
    case 4:
        if (pArea->no < 1) {
            //소계면적
            fm.Format("%%.%df", gConfig.m_iDigitSumArea);
            area = pArea->area;
            cs.Format(fm, area); 
        }
        else {
            //Line면적
            fm.Format("%%.%df", gConfig.m_iDigitArea);
            area = pArea->area;
            cs.Format(fm, area); 
        }
        break;
    case 5:  
        if (pArea->no < 1) {
            fm.Format("%%.%df", gConfig.m_iDigitSumArea);
            area = pArea->area * 0.3025;
            if (gConfig.m_bRoundUpSumArea) 
                 area = RoundUp(area, gConfig.m_iDigitSumArea);
            else area = RoundDn(area, gConfig.m_iDigitSumArea);
            cs.Format(fm, area); 
        }
        break;
    }

}
*/

void CCTSMonProView::OnGetdispinfoListModule(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	
//    long index = pDispInfo->item.iItem;
//    long subItem = pDispInfo->item.iSubItem;
//    long objCode = pDispInfo->item.lParam;

	UINT nModuleID;
	DWORD dwImgIndex;
    if(pDispInfo->item.mask & LVIF_IMAGE)
    {
		nModuleID = m_wndModuleList.GetItemData(pDispInfo->item.iItem);
		if(::SFTGetModuleState(nModuleID) == PS_STATE_LINE_OFF)
		{
			dwImgIndex = 1;
		}
		else
		{
			dwImgIndex = 0;
		}
		
		if(pDispInfo->item.state & LVIS_SELECTED)
		{
//			dwImgIndex += 2;
		}
		pDispInfo->item.iImage = dwImgIndex;	

//      CString cs;
//		GetTextCallback(index, subItem, objCode, cs);
//		lstrcpyn(pDispInfo->item.pszText, cs, pDispInfo->item.cchTextMax);
  
//		*pResult = 1; // 프로그램상으로 처리했다는 뜻으로 *pResult = 1 로 합니다.
//		return;
    }

	*pResult = 0;
}

void CCTSMonProView::OnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
//	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	
	SetCurTab(m_tabCtrl.GetCurSel());
	
	*pResult = 0;
}


BOOL CCTSMonProView::InitGrid()
{
// 	m_ImageList.Create(MAKEINTRESOURCE(IDB_IMAGES), 16, 1, RGB(255,255,255));
//	m_Grid.SetImageList(&m_ImageList);

	m_Grid.EnableDragAndDrop(FALSE);
 	m_Grid.SetEditable(FALSE);
	m_Grid.SetVirtualMode(FALSE);
	
	//Cell의 색상 
	m_Grid.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
   
	//여러 Row와 Column을 동시에 선택 가능하도록 한다.
	m_Grid.SetSingleRowSelection(FALSE);
	m_Grid.SetSingleColSelection(FALSE);
	m_Grid.SetHeaderSort(FALSE);	//Sort기능 사용안함 
	
	//크기 조정을 사용자가 못하도록 한다.
	m_Grid.SetColumnResize(FALSE);
	m_Grid.SetRowResize(FALSE);
	
	//Header 부분을 각각 1칸씩 사용한다.
	m_Grid.SetFixedRowCount(1); 
    m_Grid.SetFixedColumnCount(1); 

	ReDrawGrid();

	return TRUE;

}

BOOL CCTSMonProView::InitTabCtrl()
{	
	m_TreeImageList.Create(IDB_TREE_BITMAP, 19, 4, RGB(255,255,255));
	m_tabCtrl.SetImageList(&m_TreeImageList);

	TC_ITEM item;
	item.mask = TCIF_TEXT|TCIF_IMAGE;

/*	item.pszText = "상태";
	item.iImage = 5;
	m_tabCtrl.InsertItem(TAB_STATE, &item);

	item.iImage = 0;
	item.pszText = _T("전압");
	m_tabCtrl.InsertItem(TAB_VOLTAGE, &item);

	item.pszText = "전류";
	item.iImage = 1;
	m_tabCtrl.InsertItem(TAB_CURRENT, &item);

	item.pszText = "용량";
	item.iImage = 2;
	m_tabCtrl.InsertItem(TAB_CAPACITY, &item);

	item.pszText = "등급";
	item.iImage = 3;
	m_tabCtrl.InsertItem(TAB_GRADE, &item);

	item.pszText = "기본보기";
	item.iImage = 5;
	m_tabCtrl.InsertItem(TAB_DEFAULT, &item);*/

	item.pszText = "채널정보";
	item.iImage = 4;
	m_tabCtrl.InsertItem(TAB_CH_LIST, &item);



/*	item.pszText = "시험명";
	item.iImage = 6;
	m_tabCtrl.InsertItem(TAB_TEST_NAME, &item);*/

//	m_tabCtrl.SetCurSel(TAB_CH_LIST); 
	return TRUE;
}

void CCTSMonProView::OnGridRButtonUp(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
    *pResult = 0;
    int nRow = pItem->iRow;
    int nCol = pItem->iColumn;
	
	if(pItem->hdr.hwndFrom == m_Grid.m_hWnd)
	{
//		TRACE("Cell (%d, %d) right button clicked\n", nRow, nCol);
		if(nRow > 0 && nCol >0)
		{
			CPoint ptMouse; 
			GetCursorPos(&ptMouse); 

//			CMenu menu; 
//			menu.LoadMenu(IDR_POP_MENU); 
//			CMenu* popmenu = menu.GetSubMenu(0); 
//			popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, GetParent()); 				
			
			m_ControlMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
				ptMouse.x, ptMouse.y, AfxGetMainWnd()); 				
		}
	}
}

void CCTSMonProView::OnGridLButtonUp(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
    *pResult = 0;
    int nRow = pItem->iRow;
    int nCol = pItem->iColumn;
	
	if(pItem->hdr.hwndFrom == m_Grid.m_hWnd)
	{
//		TRACE("Cell (%d, %d) left button clicked\n", nRow, nCol);
		if(nRow > 0 && nCol >0)
		{
		}
	}
}

//선택된 Channel 중 주어진 명령을 전송할 수 있는 리스트를 Update 한다.
//nCmdID = SFT_CMD_NONE 이면 상태 구별 하지 않고 List에 추가
//bUpdate = TRUE	List Update 여부 결정 
BOOL CCTSMonProView::UpdateSelectedChList(UINT nCmdID/*= SFT_CMD_NONE*/, BOOL bUpdate /*= TRUE*/)
{
	POSITION pos;
	UINT nModuleID, nChannelIndex;
	
	m_adwTargetChArray.RemoveAll();	//ljb
// 	if (m_nCurrentModuleID == 0)
// 	{
// 		//전체 보기 선택
// 	}
// 	else
// 	{
// 		//모듈별 채널
// 
// 	}
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
		while(pos)
		{
			nModuleID = m_wndModuleList.GetItemData(m_wndModuleList.GetNextSelectedItem(pos));
			CCyclerModule *pMD = GetDocument()->GetModuleInfo(nModuleID);
			if(pMD == NULL) return FALSE; //ksj 20160628
			for(nChannelIndex = 0; nChannelIndex<pMD->GetTotalChannel(); nChannelIndex++)
			{
				//상태를 확인 한다.
				if(nCmdID == SFT_CMD_NONE && bUpdate == TRUE)
				{
					//상태 구별없이 무조건 Update한다.
					m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
				}
				else
				{
					//상태 구별하여 Update 시킨다.
					if(GetDocument()->CheckCmdState(nModuleID, nChannelIndex, nCmdID))
					{
						//단지 검사만 할 경우는 Update 시키지 않는다.
						if(bUpdate == TRUE)
						{
							m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
						}
					}
					else
					{
						//Module 명령은 상태가 안맞아도 된다. (보내는 부분에서 검사함)
						//return 0;
					}
				}
			}
		}
	}
	else
	{
		CCellID cellID;
		CCellList *cellList = m_Grid.GetSelectedCellList();
		
		if(m_nCurTabIndex == TAB_CH_LIST)	//Channel List Tab
		{
			pos = m_wndChannelList.GetFirstSelectedItemPosition();
		}
		else
		{
			pos = cellList->GetHeadPosition();
		}

		if(pos == NULL)		return FALSE;
		
		while( pos )
		{
			//Channel detail view state
			if(m_nCurTabIndex == TAB_CH_LIST)
			{
				//선택 Cell의 ModuleID와 Channel Index를 구한다.
				DWORD dwData = m_wndChannelList.GetItemData(m_wndChannelList.GetNextSelectedItem(pos));
				nChannelIndex = LOWORD(dwData);
				nModuleID = HIWORD(dwData);
// 				nChannelIndex = m_wndChannelList.GetItemData(m_wndChannelList.GetNextSelectedItem(pos));			
// 				nChannelIndex = LOWORD(nChannelIndex);	//ljb 201012 add
			}
			else
			{
				cellID = cellList->GetNext(pos);
				//선택 Cell의 ModuleID와 Channel Index를 구한다.
				//nChannelIndex = m_Grid.GetItemData(cellID.row, cellID.col);
				GetGridModuleChannel(cellID.row, cellID.col, nModuleID, nChannelIndex);	
			}

			//상태를 확인 한다.
			if(nCmdID == SFT_CMD_NONE && bUpdate == TRUE)
			{
				//상태 구별없이 무조건 Update한다.
				//m_aSelChArray.Add(MAKELONG(nChannelIndex, nModuleID));
				m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
			}
			else
			{
				//상태 구별하여 Update 시킨다.
				if(GetDocument()->CheckCmdState(nModuleID, nChannelIndex, nCmdID))
				{
					//단지 검사만 할 경우는 Update 시키지 않는다.
					if(bUpdate == TRUE)
					{
						//m_aSelChArray.Add(MAKELONG(nChannelIndex, nModuleID));
						m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
					}
					
				}
				else
				{
					//상태에 맞지 않는 채널이 선택되었을 경우 return FALSE
					return 0;

					/*//ksj 20200227 : 공유 받아 소스 적용. 검증 후 적용 필요.
					// 20200226 dhkim Puase시 Return 0 을 시켜 버리면 Run이 아닌 채널이 있는 경우 Run채널도 Pause를 건너띄
					// SFT_CMD_PAUSE를 사용하는 UpdateSelectedChList는 현재까진 영향을 받지 않으므로 아래와 같이 수정함.
					if(nCmdID== SFT_CMD_PAUSE)
						;
					else
						return FALSE;*/
				}
			}
		}
	}

	//상태에 안맞는게 있었으면 이전에 이미 return되었음
	if(bUpdate == FALSE)
	{
		return TRUE;
	}
//	return  m_aSelChArray.GetSize();
	if(m_adwTargetChArray.GetSize() > 0)
		return TRUE;
	
	return 	FALSE;
}
/*
					return 0;

	
				}
			}
			m_wndChannelList.GetNextSelectedItem(pos);
		}
	}

	//상태에 안맞는게 있었으면 이전에 이미 return되었음
	if(bUpdate == FALSE)
	{
		return TRUE;
	}
//	return  m_aSelChArray.GetSize();
	if(m_adwTargetChArray.GetSize() > 0)
		return TRUE;
	
	return 	FALSE;
}
*/
//선택된 Channel 중 주어진 명령을 전송할 수 있는 리스트를 Update 한다.
//nCmdID = SFT_CMD_NONE 이면 상태 구별 하지 않고 List에 추가
//bUpdate = TRUE	List Update 여부 결정 
BOOL CCTSMonProView::UpdateSelectedChListEMG(UINT nCmdID/*= SFT_CMD_NONE*/, BOOL bUpdate /*= TRUE*/)
{
	POSITION pos;
	UINT nModuleID, nChannelIndex;
	
	m_adwTargetChArray.RemoveAll();	//ljb


 	m_wndChannelList.SetItemState(-1, LVIS_SELECTED, LVIS_SELECTED);


	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
		while(pos)
		{
			nModuleID = m_wndModuleList.GetItemData(m_wndModuleList.GetNextSelectedItem(pos));
			CCyclerModule *pMD = GetDocument()->GetModuleInfo(nModuleID);
			if(pMD == NULL) return FALSE; //ksj 20160628
			for(nChannelIndex = 0; nChannelIndex<pMD->GetTotalChannel(); nChannelIndex++)
			{
				//상태를 확인 한다.
				if(nCmdID == SFT_CMD_NONE && bUpdate == TRUE)
				{
					//상태 구별없이 무조건 Update한다.
					m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
				}
				else
				{
					//상태 구별하여 Update 시킨다.
					if(GetDocument()->CheckCmdState(nModuleID, nChannelIndex, nCmdID))
					{
						//단지 검사만 할 경우는 Update 시키지 않는다.
						if(bUpdate == TRUE)
						{
							m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
						}
					}
					else
					{
						//Module 명령은 상태가 안맞아도 된다. (보내는 부분에서 검사함)
						//return 0;
					}
				}
			}
		}
	}
	else
	{
		CCellID cellID;
		CCellList *cellList = m_Grid.GetSelectedCellList();
		
		if(m_nCurTabIndex == TAB_CH_LIST)	//Channel List Tab
		{
			pos = m_wndChannelList.GetFirstSelectedItemPosition();
		}
		else
		{
			pos = cellList->GetHeadPosition();
		}

		if(pos == NULL)		return FALSE;
		
		while( pos )
		{
			//Channel detail view state
			if(m_nCurTabIndex == TAB_CH_LIST)
			{
				//선택 Cell의 ModuleID와 Channel Index를 구한다.
				DWORD dwData = m_wndChannelList.GetItemData(m_wndChannelList.GetNextSelectedItem(pos));
				nChannelIndex = LOWORD(dwData);
				nModuleID = HIWORD(dwData);
// 				nChannelIndex = m_wndChannelList.GetItemData(m_wndChannelList.GetNextSelectedItem(pos));			
// 				nChannelIndex = LOWORD(nChannelIndex);	//ljb 201012 add
			}
			else
			{
				cellID = cellList->GetNext(pos);
				//선택 Cell의 ModuleID와 Channel Index를 구한다.
				//nChannelIndex = m_Grid.GetItemData(cellID.row, cellID.col);
				GetGridModuleChannel(cellID.row, cellID.col, nModuleID, nChannelIndex);	
			}

			//상태를 확인 한다.
			if(nCmdID == SFT_CMD_EMERGENCY && bUpdate == TRUE)
			{
				//상태 구별없이 무조건 Update한다.
				//m_aSelChArray.Add(MAKELONG(nChannelIndex, nModuleID));
				m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
			}
			else
			{
				//상태 구별하여 Update 시킨다.
				if(GetDocument()->CheckCmdState(nModuleID, nChannelIndex, nCmdID))
				{
					//단지 검사만 할 경우는 Update 시키지 않는다.
					if(bUpdate == TRUE)
					{
						//m_aSelChArray.Add(MAKELONG(nChannelIndex, nModuleID));
						m_adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
					}
					
				}
				else
				{
					//상태에 맞지 않는 채널이 선택되었을 경우 return FALSE
					//return 0;
				}
			}
		}
	}

	//상태에 안맞는게 있었으면 이전에 이미 return되었음
	if(bUpdate == FALSE)
	{
		return TRUE;
	}
//	return  m_aSelChArray.GetSize();
	if(m_adwTargetChArray.GetSize() > 0)
		return TRUE;
	
	return 	FALSE;
}

//ljb pCmdUI용 사용함
BOOL CCTSMonProView::UpdateUIChList(UINT nCmdID/*= SFT_CMD_NONE*/, BOOL bUpdate /*= TRUE*/)
{
	POSITION pos;
	UINT nModuleID, nChannelIndex;
	CDWordArray adwTargetChArray;
	
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
		while(pos)
		{
			nModuleID = m_wndModuleList.GetItemData(m_wndModuleList.GetNextSelectedItem(pos));
			CCyclerModule *pMD = GetDocument()->GetModuleInfo(nModuleID);
			for(nChannelIndex = 0; nChannelIndex<pMD->GetTotalChannel(); nChannelIndex++)
			{
				//상태를 확인 한다.
				if(nCmdID == SFT_CMD_NONE && bUpdate == TRUE)
				{
					//상태 구별없이 무조건 Update한다.
					adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
				}
				else
				{
					//상태 구별하여 Update 시킨다.
					if(GetDocument()->CheckCmdUIState(nModuleID, nChannelIndex, nCmdID))
					{
						//단지 검사만 할 경우는 Update 시키지 않는다.
						if(bUpdate == TRUE)
						{
							adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
						}
					}
					else
					{
						//Module 명령은 상태가 안맞아도 된다. (보내는 부분에서 검사함)
						//return 0;
					}
				}
			}
		}
	}
	else
	{
		CCellID cellID;
		CCellList *cellList = m_Grid.GetSelectedCellList();
		
		if(m_nCurTabIndex == TAB_CH_LIST)	//Channel List Tab
		{
			pos = m_wndChannelList.GetFirstSelectedItemPosition();
		}
		else
		{
			pos = cellList->GetHeadPosition();
		}

		if(pos == NULL)		return FALSE;
		
		while( pos )
		{
			//Channel detail view state
			if(m_nCurTabIndex == TAB_CH_LIST)
			{
				//선택 Cell의 ModuleID와 Channel Index를 구한다.
				DWORD dwData = m_wndChannelList.GetItemData(m_wndChannelList.GetNextSelectedItem(pos));
				nChannelIndex = LOWORD(dwData);
				nModuleID = HIWORD(dwData);
			}
			else
			{
				cellID = cellList->GetNext(pos);
				//선택 Cell의 ModuleID와 Channel Index를 구한다.
				GetGridModuleChannel(cellID.row, cellID.col, nModuleID, nChannelIndex);	
			}

			//상태를 확인 한다.
			if(nCmdID == SFT_CMD_NONE && bUpdate == TRUE)
			{
				//상태 구별없이 무조건 Update한다.
				adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
			}
			else
			{
				//상태 구별하여 Update 시킨다.
				if(GetDocument()->CheckCmdUIState(nModuleID, nChannelIndex, nCmdID))
				{
					//단지 검사만 할 경우는 Update 시키지 않는다.
					if(bUpdate == TRUE)
					{
						adwTargetChArray.Add(MAKELONG(nChannelIndex, nModuleID));
					}
					
				}
				else
				{
					//상태에 맞지 않는 채널이 선택되었을 경우 return FALSE
					return 0;
				}
			}
		}
	}

	//상태에 안맞는게 있었으면 이전에 이미 return되었음
	if(bUpdate == FALSE)
	{
		return TRUE;
	}

	if(adwTargetChArray.GetSize() > 0)
		return TRUE;
	
	return 	FALSE;
}

//
BOOL CCTSMonProView::GetGridModuleChannel(const int nRow, const int nCol, UINT &nModuleID, UINT &nChIndex)
{
	int nGridColCount = m_Grid.GetColumnCount();
	int nGridRowCount = m_Grid.GetRowCount();

	if(nRow > nGridRowCount || nRow < 1)	return FALSE;
	if(nCol > nGridColCount || nCol < 1)	return FALSE;
	
// 	nModuleID = m_nCurrentModuleID;
// 	nChIndex = (nGridColCount-1)*(nRow-1) + nCol-1;
	DWORD dwData = m_Grid.GetItemData(nRow, nCol);
	nChIndex = LOWORD(dwData);
	nModuleID = HIWORD(dwData);
	return TRUE;
}

void CCTSMonProView::UpdateGridData()
{
// 	CCTSMonProDoc *pDoc = GetDocument();
// 	CString strBuff;
// 	COLORREF textColor;
// 	COLORREF bkColor;
// 	CString strVtg, strCrt, strCap;
// 	CCyclerModule *lpModule;
// 
// 	if(m_nCurTabIndex == TAB_CH_LIST)		return;
// 
// 	int nStartCh = GetStartChNo(m_nCurrentModuleID);
// 
// 	lpModule = pDoc->GetModuleInfo(m_nCurrentModuleID);
// 	if(lpModule == NULL)	return;
// 	
// 	int nColCount = m_Grid.GetColumnCount();
// 	int nRowCount = m_Grid.GetRowCount();
// 
// 
// 
// 	int nChCount = 0;
// 	CCyclerChannel *lpChannelInfo;
// 	WORD wState;
// 	CString strTipVal, strTemp;
// 	
// 	//범위 색상 표시 위해 
// 	float fVoltage, fCurrent;
// 	PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData(FALSE);
// 	
// 	for( int row = 0; row < nRowCount-1; row++ )	
// 	{
// 		for( int col = 0; col < nColCount-1; col++ )	
// 		{
// 			lpChannelInfo = lpModule->GetChannelInfo(nChCount++);
// 			if(lpChannelInfo == NULL)	continue;
// 			
// 			wState= lpChannelInfo->GetState();
// 			strVtg = pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE);
// 			strCrt = pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT);
// 			strCap = pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY);
// 
// 			fVoltage = (float)lpChannelInfo->GetVoltage()/1000.0f;
// 			fCurrent = (float)lpChannelInfo->GetCurrent()/1000.0f;
// 			
// 			switch(m_nCurTabIndex)
// 			{
// 			case TAB_VOLTAGE:			
// 				strBuff.Format("%s", strVtg);
// 				break;
// 
// 			case TAB_CURRENT:			
// 				strBuff.Format("%s", strCrt);
// 				break;
// 
// 			case TAB_CAPACITY:			
// 				strBuff.Format("%s", strCap);	
// 				break;
// 
// 			case TAB_GRADE:			
// 				strBuff.Format("%s", lpChannelInfo->GetGradeString());	
// 				break;
// 
// 			case TAB_STATE:
// 				strBuff.Format("%s", ::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));		//pscommon.dll
// 				break;
// 			
// 			case TAB_TEST_NAME:
// 				strBuff.Format("조건:%s\n이름:%s\nLOT :%s", lpChannelInfo->GetScheduleName(), lpChannelInfo->GetTestName(), lpChannelInfo->GetTestSerial());	
// 				break;
// 
// 			default:
// 				strBuff.Empty();
// 			}
// 			m_Grid.SetItemText(row+1, col+1, strBuff);
// 			strTipVal.Format(" [ Ch %d ] \n V : %s \n  I : %s \n C : %s ", nStartCh + nChCount -1, strVtg, strCrt,  strCap);
// 			m_Grid.SetToopTipText(row+1, col+1, strTipVal);
// 			
// 			::PSGetStateColor(wState, lpChannelInfo->GetStepType(), textColor, bkColor);		//pscommon.dll
// 
// 			//////////////////////////////////////////////////////////////////////////
// 			//Reference 설정에서 범위 벗어난 채널 색상 표시 위해 
// 			if(pColorData)
// 			{
// 				CScheduleData schData;
// 				if(pColorData->bShowOver)
// 				{
// 					if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
// 					{
// 						CStep *pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
// 						if(pStep)
// 						{
// 							//충전중 전압이 설정범위를 벗어남
// 							if( fabs(fVoltage - pStep->m_fVref_Charge) > pColorData->VOverConfig.fValue 
// //								&& fabs(fVoltage - pStep->m_fVref_DisCharge) > pColorData->VOverConfig.fValue 
// 								&& pStep->m_type == PS_STEP_CHARGE 
// 								&& m_nCurTabIndex == TAB_VOLTAGE
// 								&& pColorData->VOverConfig.fValue > 0.0f)
// 							{
// 								textColor = pColorData->VOverConfig.TOverColor;
// 								bkColor = pColorData->VOverConfig.BOverColor;
// 							}
// 							
// 							//충방전 중 전류가 설정범위를 벗어남
// 							if( fabs(fCurrent - pStep->m_fIref) > pColorData->IOverConfig.fValue 
// 								&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE) 
// 								&& m_nCurTabIndex == TAB_CURRENT
// 								&& pColorData->IOverConfig.fValue > 0.0f)
// 							{
// 								textColor = pColorData->IOverConfig.TOverColor;
// 								bkColor = pColorData->IOverConfig.BOverColor;
// 							}
// 						}
// 					}
// 				}
// 			}
// 			//////////////////////////////////////////////////////////////////////////
// 
// 			m_Grid.SetItemBkColour(row+1, col+1, bkColor);
// 			m_Grid.SetItemFgColour(row+1, col+1, textColor);
// 		}
// 	}
// 
// 	m_Grid.Refresh();
	CCTSMonProDoc *pDoc = GetDocument();
	CString strBuff;
	COLORREF textColor;
	COLORREF bkColor;
	CString strVtg, strCrt, strCap;
	CCyclerModule *lpModule;
	CCyclerChannel *lpChannelInfo;

	if(m_nCurTabIndex == TAB_CH_LIST)		return;

	int nColCount = m_Grid.GetColumnCount();
	int nRowCount = m_Grid.GetRowCount();
	int nStartCh = 0;
	WORD wState;
	CString strTipVal, strTemp;
	
	//범위 색상 표시 위해 
	float fVoltage, fCurrent;
	int nModuleID, nChannel;

	//PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData(FALSE);
	PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData2(); //ksj 20200202 : 원래 함수(PSGetColorCfgData)는 호출시마다 파일에서 다시 불러오므로 해당 부분 생략.
	
	for( int row = 0; row < nRowCount-1; row++ )	
	{
		for( int col = 0; col < nColCount-1; col++ )	
		{
			DWORD dwData = m_Grid.GetItemData(row+1, col+1);
			nModuleID =  HIWORD(dwData);
			nChannel = LOWORD(dwData);
			lpModule = pDoc->GetModuleInfo(nModuleID);
			if(lpModule == NULL)	continue;

			lpChannelInfo = lpModule->GetChannelInfo(nChannel);
			if(lpChannelInfo == NULL)	continue;
			
			nStartCh = GetStartChNo(nModuleID);	
			wState= lpChannelInfo->GetState();
			strVtg = pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE);
			strCrt = pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT);
			strCap = pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY);

			fVoltage = (float)lpChannelInfo->GetVoltage()/1000.0f;
			fCurrent = (float)lpChannelInfo->GetCurrent()/1000.0f;
			
			switch(m_nCurTabIndex)
			{
			case TAB_VOLTAGE:			
				strBuff.Format("%s", strVtg);
				break;

			case TAB_CURRENT:			
				strBuff.Format("%s", strCrt);
				break;

			case TAB_CAPACITY:			
				strBuff.Format("%s", strCap);	
				break;

			case TAB_GRADE:			
				strBuff.Format("%s", lpChannelInfo->GetGradeString());	
				break;

			case TAB_STATE:
				strBuff.Format("%s", ::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));		//pscommon.dll
				break;
			
			case TAB_TEST_NAME:
				strBuff.Format(Fun_FindMsg("UpdateGridData_msg","IDD_CTSMonPro_FORM"), lpChannelInfo->GetScheduleName(), lpChannelInfo->GetTestName(), lpChannelInfo->GetTestSerial());	
				//@ strBuff.Format("조건:%s\n이름:%s\nLOT :%s", lpChannelInfo->GetScheduleName(), lpChannelInfo->GetTestName(), lpChannelInfo->GetTestSerial());	
				break;

			default:
				strBuff.Empty();
			}
			m_Grid.SetItemText(row+1, col+1, strBuff);
			strTipVal.Format(" [ Ch %d ] \n V : %s \n  I : %s \n C : %s ", nStartCh + nChannel, strVtg, strCrt,  strCap);
			m_Grid.SetToopTipText(row+1, col+1, strTipVal);
			
			::PSGetStateColor(wState, lpChannelInfo->GetStepType(), textColor, bkColor);		//pscommon.dll

			//////////////////////////////////////////////////////////////////////////
			//Reference 설정에서 범위 벗어난 채널 색상 표시 위해 
			if(pColorData)
			{
				CScheduleData schData;
				if(pColorData->bShowOver)
				{
					if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
					{
						CStep *pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
						if(pStep)
						{
							//충전중 전압이 설정범위를 벗어남
							if( fabs(fVoltage - pStep->m_fVref_Charge) > pColorData->VOverConfig.fValue 
								&& pStep->m_type == PS_STEP_CHARGE 
								&& m_nCurTabIndex == TAB_VOLTAGE
								&& pColorData->VOverConfig.fValue > 0.0f)
							{
								textColor = pColorData->VOverConfig.TOverColor;
								bkColor = pColorData->VOverConfig.BOverColor;
							}
							
							//충방전 중 전류가 설정범위를 벗어남
							if( fabs(fCurrent - pStep->m_fIref) > pColorData->IOverConfig.fValue 
								&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE || pStep->m_type == PS_STEP_EXT_CAN)	//ljb 2011322 이재복 //////////
								&& m_nCurTabIndex == TAB_CURRENT
								&& pColorData->IOverConfig.fValue > 0.0f)
							{
								textColor = pColorData->IOverConfig.TOverColor;
								bkColor = pColorData->IOverConfig.BOverColor;
							}
						}
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////

			m_Grid.SetItemBkColour(row+1, col+1, bkColor);
			m_Grid.SetItemFgColour(row+1, col+1, textColor);
		}
	}

	m_Grid.Refresh();
}

void CCTSMonProView::UpdateChListData()
{	
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *lpModule = NULL;

	//system load 감소 
	if(m_nCurTabIndex != TAB_CH_LIST)		return;
	char szBuff[128];
	int nColIndex;

//	int nRow = lpModule->GetTotalChannel();
//	ASSERT(m_wndChannelList.GetItemCount() == nRow);

	CCyclerChannel *lpChannelInfo;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));

//	m_wndChannelList.SetRedraw(FALSE);
//	WORD bStateChanged = FALSE;
//	m_wndChannelList.LockWindowUpdate();

	//PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData(FALSE);
	PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData2(); //ksj 20200202 : 원래 함수(PSGetColorCfgData)는 호출시마다 파일에서 다시 불러오므로 해당 부분 생략.
	float fData;
	CStep *pStep;
	CScheduleData schData;

	UINT nModuleID, nChannelIndex,nStartCh;

	int chCnt = m_wndChannelList.GetItemCount();

	//TRACE("채널 수 = %d\n",chCnt);
	int nNonePauseCnt, nI;
	nNonePauseCnt = m_wndChannelList.GetItemCount();
	int nPauseCnt = 0; //lyj 202001007 Flicker
	
	for( nI = 0; nI < m_wndChannelList.GetItemCount() ; nI++ )	//channel index
	{
		DWORD dwID = m_wndChannelList.GetItemData(nI);
		nModuleID = HIWORD(dwID);
		nChannelIndex = LOWORD(dwID);
		nStartCh = GetStartChNo(nModuleID);

		lpModule = pDoc->GetModuleInfo(nModuleID);
		if(lpModule == NULL)	continue;
		lpChannelInfo = lpModule->GetChannelInfo(nChannelIndex);
		if(lpChannelInfo == NULL )	continue;
		//if (lpChannelInfo->GetState() == PS_STATE_LINE_OFF) continue;
		
			
   	    lvItem.mask =LVIF_IMAGE;
		
		//ljb 채널 표시 ////////////////////////////////////////////////////////////////////////
		//Current Step Type (Charge, Discharge, OCV, Rest)
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CHANNEL_NO);



//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
		if(g_AppInfo.iPType==1)
		{						//yulee 20181213 병렬 mark
			if (nI > 1) //ljb 20170710 add   ch3, ch4 채널 HIDE
			{        
				if( nColIndex >= 0 )
				{		
	 				lvItem.iItem = nI;
	 				lvItem.iSubItem = nColIndex;
	 				lvItem.iImage = 0;//pDoc->GetStateImageIndex(lpChannelInfo->GetState(), lpChannelInfo->GetStepType());
				
					lvItem.pszText = "";
					m_wndChannelList.SetItem(&lvItem, 1);
				}
				continue;
			}
			else
			{
				if( nColIndex >= 0 )
				{
				
					lvItem.iItem = nI;
					lvItem.iSubItem = nColIndex;
					lvItem.iImage = pDoc->GetStateImageIndex(lpChannelInfo->GetState(), lpChannelInfo->GetStepType());
				
					if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster)
					{
						sprintf(szBuff, "Ch %d[M]", nI+1);
						lvItem.pszText = szBuff;
						m_wndChannelList.SetItem(&lvItem, 1);
					}
					else if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
					{
						sprintf(szBuff, "Ch %d[S]", nI+1);
						lvItem.pszText = szBuff;
						m_wndChannelList.SetItem(&lvItem, 3);
					}
					else
					{
						sprintf(szBuff, "Ch %d", nI+1);
						lvItem.pszText = szBuff;
						m_wndChannelList.SetItem(&lvItem, 0);
					}			
				}		
			}
		}
		else
		{
			if( nColIndex >= 0 )
			{				
				lvItem.iItem = nI;
				lvItem.iSubItem = nColIndex;
				lvItem.iImage = pDoc->GetStateImageIndex(lpChannelInfo->GetState(), lpChannelInfo->GetStepType());

				if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster)
				{
					sprintf(szBuff, "Ch %d[M]", nI+1);
					lvItem.pszText = szBuff;
					m_wndChannelList.SetItem(&lvItem, 1);
				}
				else if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				{
					sprintf(szBuff, "Ch %d[S]", nI+1);
					lvItem.pszText = szBuff;
					m_wndChannelList.SetItem(&lvItem, 3);
				}
				else
				{
					sprintf(szBuff, "Ch %d", nI+1);					
					lvItem.pszText = szBuff;
					m_wndChannelList.SetItem(&lvItem, 0);
				}			
			}			
		}
//#endif
		lvItem.mask =LVIF_IMAGE|LVIF_TEXT;


		//ljb 상태 표시 ////////////////////////////////////////////////////////////////////////
		//Current Step Type (Charge, Discharge, OCV, Rest)
		CString strDisplay,strTemp;
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STATE);
		if( nColIndex >= 0 )
		{		
			//ljb 연결끊김 표시
			if( lpChannelInfo->GetState() == PS_STATE_LINE_OFF )
			{					
 				sprintf(szBuff, "%s(Code:%d)", Fun_FindMsg("UpdateChListData_msg1","IDD_CTSMonPro_FORM"),lpChannelInfo->GetState());
				//@ sprintf(szBuff, "%s(Code:%d)", "연결 끊김",lpChannelInfo->GetState());
			}
			else
			{
				//TRACE("%d : %d\n",lpChannelInfo->GetState(), lpChannelInfo->GetStepType());

				if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				{
					//ljb 201137 이재복 ////////// 병렬 연결시 표시					
					strDisplay = Fun_FindMsg("UpdateChListData_msg2","IDD_CTSMonPro_FORM");
					//@ strDisplay = "병렬연결";
				}
				else
				{					
					//strDisplay.Format("%s",::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
					strDisplay.Format("%s",::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
//					sprintf(szBuff, "%s", ::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
				}

				if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
				{
					pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
					if(pStep)
					{
						if(pStep->m_bUseCyclePause == TRUE)
						{
							strDisplay += " (Cycle Pause) ";
						}
					}
				}

				if (lpChannelInfo->GetChamberUsing())
				{
					if(lpChannelInfo->m_bOvenLinkChargeDisCharge &&( lpChannelInfo->GetStepType() == PS_STEP_CHARGE || lpChannelInfo->GetStepType() == PS_STEP_DISCHARGE) )//201111 챔버 연동 온도 대기시 멘트 수정
					{
						strDisplay += Fun_FindMsg("UpdateChListData_msg3","IDD_CTSMonPro_FORM"); 
						//@ strDisplay += "(챔버연동온도대기멈춤)"; 
					}
					else strDisplay += pDoc->ValueString(lpChannelInfo->GetChamberUsing(), PS_CHAMBER_USING);
				}
				sprintf(szBuff, "%s", strDisplay);			
			}
		
			lvItem.iItem = nI;
			lvItem.iSubItem = nColIndex;
			lvItem.pszText = szBuff;
			
			//1. 명령 예약 상태 표시 
			if(lpChannelInfo->IsStopReserved())
			{
				lvItem.iImage = 12;
			}
			else if(lpChannelInfo->IsPauseReserved())
			{
				lvItem.iImage = 13;
			}
			else
			{
				//자동복구 주석
				//2. Data Loss 상태 표시
				if(lpChannelInfo->IsDataLoss())
				{
					//TRACE("%s \n",::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
 					lvItem.iImage = pDoc->GetStateImageIndex(PS_STATE_FAIL);
					
					//자동복구 리스트에 추가
					//if ((lpChannelInfo->GetState() == PS_STATE_END || lpChannelInfo->GetState() == PS_STATE_IDLE || lpChannelInfo->GetState() == PS_STATE_STANDBY) && lpChannelInfo->GetCompleteWork() == FALSE)
					if ((lpChannelInfo->GetState() == PS_STATE_END) && lpChannelInfo->GetCompleteWork() == FALSE)
					{
						if (m_bRestoring == FALSE)
						{
							m_bRestoring = TRUE;

						}
					}
				}
				else
				{
					lvItem.iImage = -1;
				}
			}

			m_wndChannelList.SetItem(&lvItem);	
		}

		//Voltage		
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_VOLTAGE);
		if( nColIndex >= 0 )
		{
			//m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "%s", "---");
			else
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
			
			lvItem.iItem = nI;
			lvItem.iSubItem = nColIndex;
			lvItem.pszText = szBuff;
			lvItem.iImage = -1;
			
			//////////////////////////////////////////////////////////////////////////
			//Reference 설정에서 범위 벗어난 채널 색상 표시 위해 
			if(pColorData)
			{
				if(pColorData->bShowOver)
				{
					if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
					{
						pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
						if(pStep)
						{
							//uV->mV
							fData = (float)lpChannelInfo->GetVoltage()/1000.0f;
							//충전중 전압이 설정범위를 벗어남
							if( fabs(fData - pStep->m_fVref_Charge) > pColorData->VOverConfig.fValue 
								//&& fabs(fData - pStep->m_fVref_DisCharge) > pColorData->VOverConfig.fValue 
								&& pStep->m_type == PS_STEP_CHARGE 
								&& pColorData->VOverConfig.fValue > 0.0f
								)
							{
								lvItem.iImage = pDoc->GetStateImageIndex(PS_STATE_FAIL);
							}
						}
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////
			
			m_wndChannelList.SetItem(&lvItem);
		}

		//Current
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CURRENT);
		if( nColIndex >= 0 )
		{
			//m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "%s", "---");
			else
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));

			lvItem.iItem = nI;
			lvItem.iSubItem = nColIndex;
			lvItem.pszText = szBuff;
			lvItem.iImage = -1;

			//////////////////////////////////////////////////////////////////////////
			//Reference 설정에서 범위 벗어난 채널 색상 표시 위해 
			if(pColorData)
			{
				if(pColorData->bShowOver)
				{
					if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
					{
						pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
						if(pStep)
						{				
							//uA => mA or nA=>uA
							fData = (float)lpChannelInfo->GetCurrent()/1000.0f;
							//충방전 중 전류가 설정범위를 벗어남
							if( fabs(fData - pStep->m_fIref) > pColorData->IOverConfig.fValue 
								&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE || pStep->m_type == PS_STEP_EXT_CAN)		//ljb 2011322 이재복 //////////
								&& pColorData->IOverConfig.fValue > 0.0f
								)
							{
								lvItem.iImage = pDoc->GetStateImageIndex(PS_STATE_FAIL);
							}
						}
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////
			m_wndChannelList.SetItem(&lvItem);
		}

		//Capacity
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CAPACITY);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"");
			else
				m_wndChannelList.SetItemText(nI, nColIndex,pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));			
		}

		//Test Condition Name (Pattern)
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_LOT_NO);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetTestSerial());
			
		}

		//Total Time
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_TOT_TIME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"");
			else
			{
				//ljb 20131125 edit
				//m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetTotTime(), PS_TOT_TIME));
				if (lpChannelInfo->GetTotTimeDay() > 0) 
					strTemp.Format("D%d %s",lpChannelInfo->GetTotTimeDay(), pDoc->ValueString(lpChannelInfo->GetTotTime(), PS_TOT_TIME));
				else
					strTemp = pDoc->ValueString(lpChannelInfo->GetTotTime(), PS_TOT_TIME);

				m_wndChannelList.SetItemText(nI, nColIndex, strTemp);
			}
			
		}

		//Current Step Cycle
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem,  PS_CUR_CYCLE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "---");
			else
				sprintf(szBuff, "%d", lpChannelInfo->GetCurCycleCount());
			m_wndChannelList.SetItemText(nI, nColIndex, szBuff);
		}

		//Total Cycle
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_TOT_CYCLE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "");
			else
				sprintf(szBuff, "%d", lpChannelInfo->GetTotalCycleCount());
			
			m_wndChannelList.SetItemText(nI, nColIndex, szBuff);
		}
		
		//Test Name
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_TEST_NAME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetTestName());
			
		}
		//yulee 20190531 채널 상태가 PS_STATE_PAUSE인 경우 화면 Flickering timer를 시작한다. 
		
		//if(nTmpFlag == TRUE)//yulee 20190604 Test
		if (lpChannelInfo->GetState() == PS_STATE_PAUSE)
		{

			/*
		//lyj 20201007 Flicker s ================
			int nChannelCode = lpChannelInfo->GetCellCode();

			POSITION pos = pDoc->m_FlickerInfoList.GetHeadPosition();
			FLICKER_INFO FlickerInfo;

			if(m_FPreFickerInfo.nCode != nChannelCode)
			{
				while(pos)
				{
					FlickerInfo = (FLICKER_INFO)pDoc->m_FlickerInfoList.GetNext(pos);

					if(nChannelCode == FlickerInfo.nCode)
					{
						//if(m_FPreFickerInfo.nPriority >= FlickerInfo.nPriority) // 우선순위 검사
						if(m_FPreFickerInfo.nPriority > FlickerInfo.nPriority) // 우선순위 검사
						{
// 							m_FPreFickerInfo.nPriority = FlickerInfo.nPriority; //이전 우선순위 코드 저장
// 							m_FPreFickerInfo.nCode = nChannelCode; //이전 채널 코드 저장
							m_FPreFickerInfo = FlickerInfo; //ksj 20201013 : 구조체 복사. 최우선 순위 정보 저장
							
							m_backBrush.DeleteObject();
							m_backBrush.CreateSolidBrush(FlickerInfo.Color);

						}
						else
						{
						}
						
						break;			

					}
				}
			}
			else
			{
				
			}		
			//lyj 20201007 Flicker e ================*/

			KillTimer(TIMER_WINDOW_FLICKERING);
			SetTimer(TIMER_WINDOW_FLICKERING, 500, NULL);
			if (m_AlramSoundEnable == FALSE)
			{
				m_AlramSoundEnable = TRUE;
				CString szSoundPath = "alarm.wav";
				PlaySound(szSoundPath, AfxGetInstanceHandle(),  SND_ASYNC | SND_LOOP); 
			}
			//nTmpFlag = FALSE;
			if(nNonePauseCnt > 0)
				nNonePauseCnt--;
		}
		else
		{
		
// 			if(nNonePauseCnt < m_wndChannelList.GetItemCount())
// 			{
// 				nNonePauseCnt++;
// 			}
		}

		if (lpChannelInfo->GetCellCode() != 0)
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
			{
				//sprintf(szBuff, "---",);
			}
			else
			{
				if (lpChannelInfo->GetCellCode() < 64 || lpChannelInfo->GetCellCode() > 95)
				{
					BOOL bPauseLog;
					switch(nChannelIndex)
					{
					case 0:
						{
							bPauseLog = m_onPauseLog1;
							break;
						}
					case 1:
						{
							bPauseLog = m_onPauseLog2;
							break;
						}
					case 2:
						{
							bPauseLog = m_onPauseLog3;
							break;
						}
					case 3:
						{
							bPauseLog = m_onPauseLog4;
							break;
						}
					}


					if ((lpChannelInfo->Fun_GetFirstNgState() != TRUE) && (bPauseLog == TRUE)) //yulee 20181008-1 로그 보강
					{
						lpChannelInfo->Fun_SetFirstNgState(TRUE);
						// 1. Log에 Write 
						CString strMsg;
						strMsg.Format("Step %d [Pause] %s",lpChannelInfo->GetStepNo(),pDoc->ValueString(lpChannelInfo->GetCellCode(),PS_CODE));
						lpChannelInfo->WriteLog(strMsg);
						strMsg.Format("%s CH %d Step %d: Pause -> %s", pDoc->GetModuleName(nModuleID)
							, nChannelIndex+1,lpChannelInfo->GetStepNo(),pDoc->ValueString(lpChannelInfo->GetCellCode(),PS_CODE));
						pDoc->WriteSysEventLog(strMsg, CT_LOG_LEVEL_NORMAL); //Event_Log
						pDoc->WriteSysLog(strMsg,CT_LOG_LEVEL_NORMAL); //시스템 로그 //yulee 20190228 sys로그 내용 변경
					}
				}
			}

		}

		//CellCode
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CODE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCellCode(),PS_CODE));
			
		}

		//Current Step Time
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STEP_TIME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
			{
				//m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetStepTime(), PS_STEP_TIME));
				if (lpChannelInfo->GetStepTimeDay() > 0) 
					strTemp.Format("D%d %s",lpChannelInfo->GetStepTimeDay(), pDoc->ValueString(lpChannelInfo->GetStepTime(), PS_STEP_TIME));
				else
					strTemp = pDoc->ValueString(lpChannelInfo->GetStepTime(), PS_STEP_TIME);
				
				m_wndChannelList.SetItemText(nI, nColIndex, strTemp);
			}


		}

		//Current Step No
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STEP_NO);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
			{
				sprintf(szBuff, "%d", lpChannelInfo->GetStepNo());
				m_wndChannelList.SetItemText(nI, nColIndex, szBuff);
			}
		}

		//Grade Code
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_GRADE_CODE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetGradeCode(), PS_GRADE_CODE));
		}

		//Impedance
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_IMPEDANCE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetImpedance(), PS_IMPEDANCE));
		}
		
		//Step Type
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STEP_TYPE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetStepType(), PS_STEP_TYPE));
		}
		//Schedule Name
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_SCHEDULE_NAME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetScheduleName());
		}

		//Watt Hour Name
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_WATT_HOUR);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetWattHour(), PS_WATT_HOUR));
		}
		//Watt
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_WATT);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetWatt(), PS_WATT));
		}

		//2006.5.1 추가 
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_AVG_VOLTAGE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAvgVoltage(), PS_VOLTAGE));
		}
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_AVG_CURRENT);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAvgCurrent(), PS_CURRENT));
		}
		//충전용량+방전용량으로 표시 
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CAPACITY_SUM);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCapacitySum(), PS_CAPACITY));
		}

		// 2009_02_17 kky
// 		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_WATTHOUR_SUM);
// 		if( nColIndex >= 0 )
// 		{
// 			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetWattHourSum(), PS_WATT_HOUR));
// 		}

		//////////////////////////////////////////////////////////////////////////

		//용량을 충전 용량 방전 용량으로 구별하여 표시 
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CHARGE_CAP);
		if( nColIndex >= 0)
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
			{
				if(lpChannelInfo->GetStepType() == PS_STEP_CHARGE)
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
				}
				else
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(0.0, PS_CAPACITY));
				}
			}
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_DISCHARGE_CAP);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
			{
				if(lpChannelInfo->GetStepType() == PS_STEP_DISCHARGE)
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
				}
				else
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(0.0, PS_CAPACITY));
				}
			}

		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_METER_DATA);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetMeterValue(), PS_METER_DATA));
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_START_TIME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex, "");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetStartTime());
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_END_TIME);
		if( nColIndex >= 0 )
		{
			//m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetStartTime());
		}

		if (m_bUseCbank)
		{
			nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CBANK);
			if( nColIndex >= 0 )
			{
				if( lpChannelInfo->GetState() == PS_STATE_LINE_OFF )
					m_wndChannelList.SetItemText(nI, nColIndex, "");
				else
				{
					if (lpChannelInfo->GetCbank())
					{
						m_wndChannelList.SetItemText(nI, nColIndex, "ON");
					}
					else
					{
						m_wndChannelList.SetItemText(nI, nColIndex, "OFF");
					}
				}
			}
		}
		
		//lyj 20200228 세인ENG, [20191209]Edited By SKH Acc Cycle 수정
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_ACC_CYCLE1);
		if( nColIndex >= 0 )
		{
			if(m_bAccCycleOffSet) //lyj 20201013 세인 요청 누적사이클 BASE 0 -> 1
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount1()+1, PS_ACC_CYCLE1));
			}
			else
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount1(), PS_ACC_CYCLE1));
			}
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_ACC_CYCLE2);
		if( nColIndex >= 0 )
		{
			if(m_bAccCycleOffSet) //lyj 20201013 세인 요청 누적사이클 BASE 0 -> 1
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount2()+1, PS_ACC_CYCLE2));	
			}
			else
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount2(), PS_ACC_CYCLE2));	
			}
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_ACC_CYCLE3);
		if( nColIndex >= 0 )
		{
			if(m_bAccCycleOffSet) //lyj 20201013 세인 요청 누적사이클 BASE 0 -> 1
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount3()+1, PS_ACC_CYCLE3));
			}
			else
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount3(), PS_ACC_CYCLE3));
			}
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_ACC_CYCLE4);
		if( nColIndex >= 0 )
		{
			if(m_bAccCycleOffSet) //lyj 20201013 세인 요청 누적사이클 BASE 0 -> 1
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount4()+1, PS_ACC_CYCLE4));
			}
			else
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount4(), PS_ACC_CYCLE4));
			}
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_ACC_CYCLE5);
		if( nColIndex >= 0 )
		{
			if(m_bAccCycleOffSet) //lyj 20201013 세인 요청 누적사이클 BASE 0 -> 1
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount5()+1, PS_ACC_CYCLE5));
			}
			else
			{
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAccCycleCount5(), PS_ACC_CYCLE5));
			}
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_MULTI_CYCLE1);
		if( nColIndex >= 0 )
		{
			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetMultiCycleCount1(), PS_MULTI_CYCLE1));
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_MULTI_CYCLE2);
		if( nColIndex >= 0 )
		{
			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetMultiCycleCount2(), PS_MULTI_CYCLE2));
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_MULTI_CYCLE3);
		if( nColIndex >= 0 )
		{
			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetMultiCycleCount3(), PS_MULTI_CYCLE3));
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_MULTI_CYCLE4);
		if( nColIndex >= 0 )
		{
			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetMultiCycleCount4(), PS_MULTI_CYCLE4));
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_MULTI_CYCLE5);
		if( nColIndex >= 0 )
		{
			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetMultiCycleCount5(), PS_MULTI_CYCLE5));
		}

	}

	if(nI == m_wndChannelList.GetItemCount())
	{
		if(m_wndChannelList.GetItemCount() == nNonePauseCnt)
		{
			KillTimer(TIMER_WINDOW_FLICKERING);
			KillTimer(TIMER_ALARM_ENABLE);
// 			m_FPreFickerInfo.nCode = 0;
// 			m_FPreFickerInfo.nPriority = 3; //lyj 20201007 Flicker 초기화
// 			m_FPreFickerInfo.nAutoBuzzerStop = 0; //ksj 20201013 : 초기화 추가
			InitPreFlickerInfo(); //ksj 20201013

			if(m_FlagBackgroundColor != 1)
			{
				m_FlagBackgroundColor = 1;
				CFormView::OnInitialUpdate();
			}
		}
	}

//	m_wndChannelList.UnlockWindowUpdate();
//	m_wndChannelList.SetRedraw(TRUE);
	m_wndChannelList.Invalidate();

//ljb 20150611 주석 처리	onMonitoringDeal();//monitoring

	CString strTemp;
	strTemp.Format("1 LODE %s, %s, volt = %.3f, cur = %.3f", pDoc->m_ctrlLoader1.m_strLoadStat,pDoc->m_ctrlLoader1.m_strMode, pDoc->m_ctrlLoader1.m_fCurVolt,pDoc->m_ctrlLoader1.m_fCurCurr);
	m_ctrlLoaderState_1 = strTemp;
	strTemp.Format("2 LODE %s, %s, volt = %.3f, cur = %.3f", pDoc->m_ctrlLoader2.m_strLoadStat,pDoc->m_ctrlLoader2.m_strMode, pDoc->m_ctrlLoader2.m_fCurVolt,pDoc->m_ctrlLoader2.m_fCurCurr);
	m_ctrlLoaderState_2 = strTemp;
	UpdateData(FALSE);

}

//아래는 기존 UpdateChListData() 코드
/*
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *lpModule = pDoc->GetModuleInfo(m_nCurrentModuleID);
	if(lpModule == NULL)	return;

	char szBuff[128];//, szTestName[128];
	int nColIndex;
	int nRow = lpModule->GetTotalChannel();

	ASSERT(m_wndChannelList.GetItemCount() == nRow);
	CCyclerChannel *lpChannelInfo;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));

	PS_COLOR_CONFIG* pColorData =  ::PSGetColorCfgData(FALSE);
	float fData;
	CStep *pStep;
	CScheduleData schData;

	for( int nI = 0; nI < nRow ; nI++ )	//channel index
	{
		lpChannelInfo = lpModule->GetChannelInfo(nI);
		if(lpChannelInfo == NULL)	continue;

		if (nI ==0)
		{
			//BMA에 상태 정보 전송
			//STF_MD_CAN_SET_DATA *pBmsData = (STF_MD_CAN_SET_DATA *)lParam;
			//if(pBmsData == NULL)	return;
			HANDLE	handle = AfxGetInstanceHandle();		//자신의 핸들 얻기
			HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");
		
			if(hWnd)
			{
				m_Bma_Channel_Info.bChannel = nI;
				m_Bma_Channel_Info.wState = lpChannelInfo->GetState();
				m_Bma_Channel_Info.wStepType = lpChannelInfo->GetStepType();
				m_Bma_Channel_Info.lVoltage = lpChannelInfo->GetVoltage();
				m_Bma_Channel_Info.lCurrent = lpChannelInfo->GetCurrent();
				m_Bma_Channel_Info.ulTotalTime = lpChannelInfo->GetTotTime();
				m_Bma_Channel_Info.ulStepTime = lpChannelInfo->GetStepTime();
				m_Bma_Channel_Info.nTotalCycle = lpChannelInfo->GetTotalCycleCount();
				m_Bma_Channel_Info.nCurCycle = lpChannelInfo->GetCurCycleCount();

				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = WM_MY_MESSAGE_CTS_BMA_CH_INFO;		
				CpStructData.cbData = sizeof(SFT_BMA_CHANNEL_INFO);
				CpStructData.lpData = (PVOID)&m_Bma_Channel_Info;
				::SendMessageA(hWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&CpStructData);
				TRACE("BMA로 channel Info 전송완료(%d,%d,%d,%d) \n"
					,m_Bma_Channel_Info.wState,m_Bma_Channel_Info.wStepType,m_Bma_Channel_Info.lVoltage,m_Bma_Channel_Info.lCurrent);
			}

		}

		lvItem.mask =LVIF_IMAGE;

		//state image
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CHANNEL_NO);
		if( nColIndex >= 0 )
		{
			lvItem.iItem = nI;
			lvItem.iSubItem = nColIndex;
			//lvItem.iImage = pDoc->GetStateImageIndex(lpChannelInfo->GetState(), lpChannelInfo->GetStepType());
		
			lvItem.iImage = pDoc->GetStateImageIndex(lpChannelInfo->GetState(), lpChannelInfo->GetStepType());
			//m_wndChannelList.SetItem(&lvItem);
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster)
			{
				sprintf(szBuff, "Ch %d[M]", nI+1);
				lvItem.pszText = szBuff;
				m_wndChannelList.SetItem(&lvItem, 1);
			}
			else if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
			{
				sprintf(szBuff, "Ch %d[S]", nI+1);
				lvItem.pszText = szBuff;
				m_wndChannelList.SetItem(&lvItem, 3);
			}
			else
			{
				sprintf(szBuff, "Ch %d", nI+1);
				lvItem.pszText = szBuff;
				m_wndChannelList.SetItem(&lvItem, 0);
			}

		}

		lvItem.mask =LVIF_IMAGE|LVIF_TEXT;

		//Voltage		
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_VOLTAGE);
		if( nColIndex >= 0 )
		{
			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
			
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "-");
			else
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
			lvItem.iItem = nI;
			lvItem.iSubItem = nColIndex;
			lvItem.pszText = szBuff;
			lvItem.iImage = -1;
			
			//////////////////////////////////////////////////////////////////////////
			//Reference 설정에서 범위 벗어난 채널 색상 표시 위해 
			if(pColorData)
			{
				if(pColorData->bShowOver)
				{
					if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
					{
						pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
						if(pStep)
						{
							//uV->mV
							fData = (float)lpChannelInfo->GetVoltage()/1000.0f;
							//충전중 전압이 설정범위를 벗어남
							if( fabs(fData - pStep->m_fVref) > pColorData->VOverConfig.fValue 
								&& pStep->m_type == PS_STEP_CHARGE 
								&& pColorData->VOverConfig.fValue > 0.0f
								)
							{
								lvItem.iImage = pDoc->GetStateImageIndex(PS_STATE_FAIL);
							}
						}
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////
			
			m_wndChannelList.SetItem(&lvItem);
		}

		//Current
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CURRENT);
		if( nColIndex >= 0 )
		{
//			m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));

			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "-");
			else
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));

			lvItem.iItem = nI;
			lvItem.iSubItem = nColIndex;
			lvItem.pszText = szBuff;
			lvItem.iImage = -1;

			//////////////////////////////////////////////////////////////////////////
			//Reference 설정에서 범위 벗어난 채널 색상 표시 위해 
			if(pColorData)
			{
				if(pColorData->bShowOver)
				{
					if(schData.SetSchedule(lpChannelInfo->GetScheduleFile()))
					{
						pStep = schData.GetStepData(lpChannelInfo->GetStepNo()-1);
						if(pStep)
						{				
							//uA => mA or nA=>uA
							fData = (float)lpChannelInfo->GetCurrent()/1000.0f;
							//충방전 중 전류가 설정범위를 벗어남
							if( fabs(fData - pStep->m_fIref) > pColorData->IOverConfig.fValue 
								&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE) 
								&& pColorData->IOverConfig.fValue > 0.0f
								)
							{
								lvItem.iImage = pDoc->GetStateImageIndex(PS_STATE_FAIL);
							}
						}
					}
				}
			}
			//////////////////////////////////////////////////////////////////////////
			m_wndChannelList.SetItem(&lvItem);
		}

		//Capacity
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CAPACITY);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else				
				m_wndChannelList.SetItemText(nI, nColIndex,pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
		}

		//Test Condition Name (Pattern)
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_LOT_NO);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else				
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetTestSerial());
		}

		//Total Time
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_TOT_TIME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else				
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetTotTime(), PS_TOT_TIME));
		}

		//Current Step Cycle
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem,  PS_CUR_CYCLE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "-");
			else
				sprintf(szBuff, "%d", lpChannelInfo->GetCurCycleCount());
			m_wndChannelList.SetItemText(nI, nColIndex, szBuff);
		}

		//Total Cycle
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_TOT_CYCLE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "-");
			else
				sprintf(szBuff, "%d", lpChannelInfo->GetTotalCycleCount());
			m_wndChannelList.SetItemText(nI, nColIndex, szBuff);
		}
		
		//Test Name
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_TEST_NAME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetTestName());
		}

		//Current Error Code
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CODE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCellCode(),PS_CODE));
		}

		//Current Step Type (Charge, Discharge, OCV, Rest)
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STATE);
		if( nColIndex >= 0 )
		{		
//			m_wndChannelList.SetItemText(nI, nColIndex, ::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));

			if( lpChannelInfo->GetState() == PS_STATE_LINE_OFF )
			{				
				sprintf(szBuff, "%s", "연결 끊김");
			}
			else
			{						
				if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
					sprintf(szBuff, "병렬연결");
				else
					sprintf(szBuff, "%s", ::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType()));
			}

			if(nI == m_nCurrentChannelNo)
			{				
				COLORREF textColor, bkColor;
				PSGetTypeColor(lpChannelInfo->GetStepType(), textColor, bkColor);

				m_ctrlLabelChState.SetTextColor(textColor);
				m_ctrlLabelChState.SetBkColor(bkColor);
				m_ctrlLabelChState.SetText(szBuff);
			}
			

			lvItem.iItem = nI;
			lvItem.iSubItem = nColIndex;
			lvItem.pszText = szBuff;

			//1. 명령 예약 상태 표시 
			if(lpChannelInfo->IsStopReserved())
			{
				lvItem.iImage = 12;
			}
			else if(lpChannelInfo->IsPauseReserved())
			{
				lvItem.iImage = 13;
			}
			else
			{
				//2. Data Loss 상태 표시
				if(lpChannelInfo->IsDataLoss())
				{
					lvItem.iImage = pDoc->GetStateImageIndex(PS_STATE_FAIL);
				}
				else
				{
					lvItem.iImage = -1;
				}
			}
			
			m_wndChannelList.SetItem(&lvItem);
	//		TRACE("CH State :%s\r\n", szBuff);

		}

		//Current Step Time
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STEP_TIME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetStepTime(), PS_STEP_TIME));
		}

		//Current Step No
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STEP_NO);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				sprintf(szBuff, "-");
			else
				sprintf(szBuff, "%d", lpChannelInfo->GetStepNo());
			m_wndChannelList.SetItemText(nI, nColIndex, szBuff);
		}

		//Grade Code
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_GRADE_CODE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetGradeCode(), PS_GRADE_CODE));
		}

		//Impedance
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_IMPEDANCE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetImpedance(), PS_IMPEDANCE));
		}
		
		//Step Type
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STEP_TYPE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetStepType(), PS_STEP_TYPE));
		}
		//Schedule Name
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_SCHEDULE_NAME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetScheduleName());
		}

		//Watt Hour Name
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_WATT_HOUR);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetWattHour(), PS_WATT_HOUR));
		}
		//Watt
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_WATT);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetWatt(), PS_WATT));
		}

		//2006.5.1 추가 
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_AVG_VOLTAGE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAvgVoltage(), PS_VOLTAGE));
		}
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_AVG_CURRENT);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAvgCurrent(), PS_CURRENT));
		}
		//충전용량+방전용량으로 표시 
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CAPACITY_SUM);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCapacitySum(), PS_CAPACITY));
		}

		//용량을 충전 용량 방전 용량으로 구별하여 표시 
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CHARGE_CAP);
		if( nColIndex >= 0)
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
			{
				if(lpChannelInfo->GetStepType() == PS_STEP_CHARGE)
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
				}
				else
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(0.0, PS_CAPACITY));
				}
			}
			
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_DISCHARGE_CAP);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
			{
				if(lpChannelInfo->GetStepType() == PS_STEP_DISCHARGE)
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
				}
				else
				{
					m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(0.0, PS_CAPACITY));
				}
			}
		}

		//nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_TEMPERATURE);
		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_AUX_TEMPERATURE);
		
		CString strTemp;
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				//strTemp = Fun_GetAuxTemperature(lpModule,lpChannelInfo, nI);
				m_wndChannelList.SetItemText(nI, nColIndex,Fun_GetAuxTemperature(lpModule, lpChannelInfo, nI));
				//m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetTemperature(), PS_TEMPERATURE));
		}
		//m_wndChannelList.SetItemText(nI, nColIndex,Fun_GetAuxTemperature(lpChannelInfo, nI));

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_AUX_VOLTAGE);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetAuxVoltage(), PS_AUX_VOLTAGE));
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_METER_DATA);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, pDoc->ValueString(lpChannelInfo->GetMeterValue(), PS_METER_DATA));
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_START_TIME);
		if( nColIndex >= 0 )
		{
			if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				m_wndChannelList.SetItemText(nI, nColIndex,"-");
			else
				m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetStartTime());
		}

		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_END_TIME);
		if( nColIndex >= 0 )
		{
			//m_wndChannelList.SetItemText(nI, nColIndex, lpChannelInfo->GetStartTime());
		}
		//////////////////////////////////////////////////////////////////////////
	}
	m_wndChannelList.Invalidate();
}
*/

//void CCTSMonProView::GetStateColor(WORD state, WORD type, COLORREF &textColor, COLORREF &bkColor)
//{
//	BYTE colorIndex = GetDocument()->GetStateColorIndex(state, type);
//	PS_COLOR_CONFIG *pColor = GetDocument()->GetStateCfg();
//	textColor = pColor->stateConfig[colorIndex].TStateColor;
//	bkColor = pColor->stateConfig[colorIndex].BStateColor;
//}

void CCTSMonProView::OnGridConfig() 
{
	// TODO: Add your command handler code here
	if(GetDocument()->SetColorConfig() == TRUE)
	{
		//ReDraw();
	}
}

void CCTSMonProView::OnCalibration() 
{
	// TODO: Add your command handler code here
	CLoginManagerDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;

	GetDocument()->CalibrationExe(m_nCurrentModuleID, GetDesktopWindow());
}

void CCTSMonProView::OnLogWnd() 
{
	// TODO: Add your command handler code here
	GetDocument()->ShowLogDlg();
}

void CCTSMonProView::SetModuleSelChanged(int nModuleID)
{
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
// 	CCyclerModule *pMD = pDoc->GetModuleInfo(nModuleID);
// 	if(pMD == NULL)		return;

	CWaitCursor cursor;	

	m_nCurrentModuleID = nModuleID;
//	GetDlgItem(IDC_SEL_MD_STATIC)->SetWindowText(GetDocument()->GetModuleName(m_nCurrentModuleID));


	//Grid content update
	ReDrawGrid();
	UpdateGridData();

	//Channel List Update
	ReDrawChListCtrl(nModuleID);
	UpdateChListData();

}

void CCTSMonProView::OnUpdateGridConfig(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}

void CCTSMonProView::OnDataView() 
{
	// TODO: Add your command handler code here
	ExeCTSAnal(3);
}

//Grid를 새로운 Row/Col로 다시 그린다.
BOOL CCTSMonProView::ReDrawGrid()
{
	//5*9 Grid 생성
	CCyclerModule *pMD = GetDocument()->GetCyclerMD(m_nCurrentModuleID);
	if(pMD == NULL)		return	FALSE;

	LockUpdate();
	
	CFont aFont;
	aFont.CreateFontIndirect(&m_lfGridFont);
	m_Grid.SetFont(&aFont);

	int nTotalCh = pMD->GetTotalChannel();
	int nRow = nTotalCh/m_nDisplayCol+1;		//Row Header 추가 
	if(nTotalCh%m_nDisplayCol)	nRow++;
	int nCol = m_nDisplayCol+1;					//Column Header 추가 

	//Reset Cell property by remove row,col
//	m_Grid.SetRowCount(1); 
//  m_Grid.SetColumnCount(1); 

//	if(nRow == m_Grid.GetRowCount() && nCol == m_Grid.GetColumnCount() )	return TRUE;

	//make new cell
	m_Grid.SetRowCount(nRow); 
    m_Grid.SetColumnCount(nCol); 

	int nStartCh = GetStartChNo(m_nCurrentModuleID);

	CString str; 
	int nCount = 0;
	for (int row = 0; row < nRow; row++)
    {
		for (int col = 0; col < nCol; col++)
		{
			if( row == 0 && col > 0)
			{
				str.Format(_T("%c"), 'A'+ col-1);			//Column Header를 넣는다.
				m_Grid.SetItemText(row, col, str);
			}
			if( col == 0 && row > 0)
			{
				str.Format(_T("%d"), row);
				m_Grid.SetItemText(row, col, str);			//Row Header를 넣는다.
			}

//			m_Grid.SetTextSize(row, col, m_nGridFontSize);
			//Text 정렬 형태 
			m_Grid.SetItemFormat(row, col, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
			
			//Set Data to Channel No
			if(row > 0 && col > 0)
			{

				m_Grid.SetItemData(row, col, nCount++);
				str.Format("%d", nStartCh+nCount-1);
				m_Grid.SetLabelText(row, col, str);
			}
		}
    }

	if(m_nCurTabIndex == TAB_TEST_NAME)		GridTextMode(TRUE);

	UpdateGridRowTopCell();

	m_Grid.ExpandToFit();
	
//	m_Grid.ExpandColumnsToFit();	//폭을 균일 분배로 맞춤
//	m_Grid.ExpandRowsToFit();		//높이를 균일 분배로 맞춤
//	m_Grid.Refresh();

	UnLockUpdate();

	return TRUE;
}

void CCTSMonProView::ReDrawChListCtrl(int nModuleID)
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pMD = NULL;
	int mdID;

	//previous state
	POSITION pos = m_wndChannelList.GetFirstSelectedItemPosition();
    int nPreSel = m_wndChannelList.GetNextSelectedItem(pos);
	int nPrevCount = m_wndChannelList.GetItemCount();
	int nCount = 0;
	int nTotMDCnt = pDoc->GetInstallModuleCount();

	//전체 보기 Mode
	if(nModuleID < 1)
	{
		nCount = 0;
		for(int s =0; s<nTotMDCnt; s++)
		{
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				nCount+= pMD->GetTotalChannel();
			}
		}
	}
	else	//1 Module Mode
	{
		pMD =  pDoc->GetCyclerMD(nModuleID);
		if(pMD)
		{
			nCount = pMD->GetTotalChannel();
		}
		nTotMDCnt = 1;
	}

	//Redraw list
	//////////////////////////////////////////////////////////////////////////
	LockUpdate();

	CString strTitle;
	DWORD dwData = 0;
	char szBuff[64];
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
	if(nPrevCount != nCount)
	{
		m_wndChannelList.DeleteAllItems();
		
		for(int nI = 0; nI < nCount; nI++ )
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
			{
				if (nI > 1) continue; //[20201123]Edited By Skh 주석
			}
//#endif
			lvItem.iItem = nI;
			lvItem.iSubItem = 0;
			lvItem.iImage = I_IMAGECALLBACK;
			m_wndChannelList.InsertItem(&lvItem);
		}	
	}

	//전체 모듈을  
	int nMDColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_MODULE_NO);	//List 상에서 현재 표기된 Column
	int nChColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CHANNEL_NO);

	int nListIndex = 0;
	for(int s=0; s<nTotMDCnt; s++)
	{
		if(nModuleID < 1)	
		{
			//여러 모듈 보기 
			mdID = pDoc->GetModuleID(s);
		}
		else	
		{
			//단독 모듈 보기 
			mdID = nModuleID;
		}

		pMD = pDoc->GetCyclerMD(mdID);
		if(pMD == NULL)		continue;

		//ksj 20201116 : 모니터링 화면에서 채널 숨기는 기능.		
		CString strEntry;
		strEntry.Format("MD%d HIDE CHANNEL FLAG",mdID);
		int nHideChFlag = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, strEntry, 0x00); //32bit 모든 채널 출력. 최대 32채널 까지 숨김 가능. 해당 bit 켜져있으면 숨긴다.
		//ksj end

		int nStartCh = GetStartChNo(mdID);
		int nChCount = pMD->GetTotalChannel();

		for(int a = 0; a < nChCount; a++ )
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리 //yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
			{
				if (a > 1) continue; //ljb 20170710 add //ksj 20180212 : 주석처리
			}
//#endif
			dwData= MAKELONG(a, mdID);

			//ksj 20201116 : 모니터링 화면에서 채널 숨기는 기능.		
			int nHideChCheck = (pMD->GetTotalChannel() <= 32)?0x01<<a:0x00;			
			int nIsHide = nHideChFlag & nHideChCheck;

			if(nIsHide) //숨김 플래그가 켜져있는지 채널별 플래그랑 비교.
			{
				continue; //채널 그리지 않고 넘어감 (채널 숨김)	
			}
			//ksj end

			m_wndChannelList.SetItemData(nListIndex, dwData);
			///////////////////////////////////////////////////////////////////////////
			// 1. Module Number
			if( nMDColIndex >= 0 )	//Module 번호 Column을 표기 하도록 설정되었으면 
			{
				m_wndChannelList.SetItemText(nListIndex, nMDColIndex, GetDocument()->GetModuleName(mdID));
			}

			///////////////////////////////////////////////////////////////////////////
			// 2. Channel Status
			if( nChColIndex >= 0 )
			{
				sprintf(szBuff, "Ch %d", nStartCh+a); 
				m_wndChannelList.SetItemText(nListIndex, nChColIndex, szBuff);
			}

			nListIndex++;
		}
	}

	UnLockUpdate();
	//////////////////////////////////////////////////////////////////////////

	//Select previous selected item
	int nSel = 0;
	if(nPreSel < m_wndChannelList.GetItemCount() && nPreSel >= 0)
	{
		nSel = nPreSel;
	}
	dwData = m_wndChannelList.GetItemData(nSel);
	m_wndChannelList.SetItemState(nSel, LVIS_SELECTED, LVIS_SELECTED);	//==>LVN_ITEMCHANGED 를 발생 기킴 =>OnItemchangedListChannel(NMHDR* pNMHDR, LRESULT* pResult) 호출 
	
// 	CCTSMonProDoc *pDoc = GetDocument();
// 
// 	CCyclerModule *pMD = pDoc->GetCyclerMD(m_nCurrentModuleID);
// 	if(pMD == NULL)		return;
// 
// 	LockUpdate();
// 	
// 	CString strTitle;
// 	int nColIndex;
// 	char szBuff[64];
// 	LVITEM lvItem;
// 	ZeroMemory(&lvItem, sizeof(LVITEM));
// 	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
// 
// 	int nPrevCount = m_wndChannelList.GetItemCount();
// 	int nCount = pMD->GetTotalChannel();
// 	
// 	if(nPrevCount != nCount)
// 	{
// 		m_wndChannelList.DeleteAllItems();
// 		
// 		for(int nI = 0; nI < nCount; nI++ )
// 		{
// 			lvItem.iItem = nI;
// 			lvItem.iSubItem = 0;
// 			lvItem.iImage = I_IMAGECALLBACK;
// 			strTitle.Format("");
// 			lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
// 			m_wndChannelList.InsertItem(&lvItem);
// 			m_wndChannelList.SetItemData(lvItem.iItem, nI);
// 
// 		}	
// 	}
// 
// 
// 	int nStartCh = GetStartChNo(m_nCurrentModuleID);
// 
// 	for(int a = 0; a < nCount; a++ )
// 	{
// 		///////////////////////////////////////////////////////////////////////////
// 		// 1. Module Number
// 		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_MODULE_NO);	//List 상에서 현재 표기된 Column
// 		if( nColIndex >= 0 )	//Module 번호 Column을 표기 하도록 설정되었으면 
// 		{
// 			m_wndChannelList.SetItemText(a, nColIndex, GetDocument()->GetModuleName(m_nCurrentModuleID));
// 		}
// 
// 		///////////////////////////////////////////////////////////////////////////
// 		// 2. Channel Status
// 		nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_CHANNEL_NO);
// 		if( nColIndex >= 0 )
// 		{
// 			sprintf(szBuff, "Ch %d", nStartCh+a); 
// 			m_wndChannelList.SetItemText(a, nColIndex, szBuff);
// 		}
// 	}
// 
// 	UnLockUpdate();

}

//현재 선택된 Module의 상태를 파일로 저장한다.
void CCTSMonProView::OnFileSave() 
{
	// TODO: Add your command handler code here
	CCTSMonProDoc *pDoc = GetDocument();

	if (m_nCurrentModuleID < 1)
	{
		AfxMessageBox(Fun_FindMsg("OnFileSave_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업할 모듈을 선택 하세요.");
		return;
	}

	CString strFileName;
	strFileName = pDoc->GetModuleName(m_nCurrentModuleID);
	
	CFileDialog pDlg(FALSE, "", strFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "csv File(*.csv)|*.csv|");
	if(IDOK == pDlg.DoModal())
	{
		strFileName = pDlg.GetPathName();
	}
	if(strFileName.IsEmpty())
	{
		return;
	}

	FILE *fp = fopen(strFileName, "wt");
	if(fp == NULL)	return;

	CString strData;
	strData.Format(Fun_FindMsg("OnFileSave_msg2","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(m_nCurrentModuleID)); 	
	//@ strData.Format("모듈번호, %s\n", pDoc->GetModuleName(m_nCurrentModuleID)); 	
	fprintf(fp, "%s", strData);
	
	CCyclerModule *lpModule = pDoc->GetModuleInfo(m_nCurrentModuleID);
	if(lpModule == NULL)
	{
		fclose(fp);
		return;
	}
	
	int nColCount = m_Grid.GetColumnCount();
	int nRowCount = m_Grid.GetRowCount();

	CCyclerChannel *lpChannelInfo= NULL;

	int row, col;

	//상태 
	int nChCount = 0;
	for(row = 0; row < nRowCount-1; row++ )	
	{
		for(col = 0; col < nColCount-1; col++ )	
		{
			lpChannelInfo = lpModule->GetChannelInfo(nChCount++);
			if(lpChannelInfo == NULL)	break;
			
			fprintf(fp, "%s", pDoc->ValueString(lpChannelInfo->GetState(), PS_STATE));
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

	//시험명 
	nChCount = 0;
	for(row = 0; row < nRowCount-1; row++ )	
	{
		for(col = 0; col < nColCount-1; col++ )	
		{
			lpChannelInfo = lpModule->GetChannelInfo(nChCount++);
			if(lpChannelInfo == NULL)	break;
			
			fprintf(fp, "%s", lpChannelInfo->GetTestName());
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

	//전압 
	nChCount = 0;
	for(row = 0; row < nRowCount-1; row++ )	
	{
		for(col = 0; col < nColCount-1; col++ )	
		{
			lpChannelInfo = lpModule->GetChannelInfo(nChCount++);
			if(lpChannelInfo == NULL)	break;
			
			fprintf(fp, "%s",  pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

	//전류
	nChCount = 0;
	for(row = 0; row < nRowCount-1; row++ )	
	{
		for(col = 0; col < nColCount-1; col++ )	
		{
			lpChannelInfo = lpModule->GetChannelInfo(nChCount++);
			if(lpChannelInfo == NULL)	break;
			
			fprintf(fp, "%s", pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

	//용량 
	nChCount = 0;
	for(row = 0; row < nRowCount-1; row++ )	
	{
		for(col = 0; col < nColCount-1; col++ )	
		{
			lpChannelInfo = lpModule->GetChannelInfo(nChCount++);
			if(lpChannelInfo == NULL)	break;
			
			fprintf(fp, "%s", pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

	//Grading Code 
	nChCount = 0;
	for(row = 0; row < nRowCount-1; row++ )	
	{
		for(col = 0; col < nColCount-1; col++ )	
		{
			lpChannelInfo = lpModule->GetChannelInfo(nChCount++);
			if(lpChannelInfo == NULL)	break;
			
			fprintf(fp, "%s", lpChannelInfo->GetGradeString());
		}
		fprintf(fp, "\n");
	}

	fclose(fp);	
}

void CCTSMonProView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	// The pointer to my header control.

/*	if(m_wndChannelList.GetSafeHwnd())
	{
		CHeaderCtrl* pmyHeaderCtrl = m_wndChannelList.GetHeaderCtrl();

		// Find the item whose text matches lpszmyString, and 
		// replace it with lpszmyString2.
		int    i, nCount = pmyHeaderCtrl->GetItemCount();
		HDITEM hdi;
		TCHAR  lpBuffer[128];

		LVCOLUMN col;
		col.mask = LVCF_TEXT|LVCF_SUBITEM;
		col.cchTextMax = 128;
		col.pszText = lpBuffer;

		hdi.mask = HDI_TEXT;
		hdi.pszText = lpBuffer;
		hdi.cchTextMax = 128;

		for( int i=0; i < nCount; i++)
		{
			pmyHeaderCtrl->GetItem(i, &hdi);
			m_wndChannelList.GetColumn(i, &col);
 
	//		TRACE("%s/%d\n", hdi.pszText, hdi.lParam);
			TRACE("%s/%d\n", col.pszText, col.iSubItem);
		}
	}
*/
	KillTimer(TIMER_DISPLAY_CHANNEL);
	KillTimer(TIMER_DISPLAY_MODULE);
	KillTimer(TIMER_WINDOW_FLICKERING);
	KillTimer(TIMER_AUTO_UPDATE); //lyj 20210809
/*	if(m_pItem)
	{
		int nWidth;
		int nItemNum = CItemOnDisplay::GetItemNum();
		for( int nI = 0; nI < nItemNum; nI++ )
		{
			nWidth = m_wndChannelList.GetColumnWidth(nI);
			
			if( nWidth > 1000 || nWidth <= 0 )
			{
				nWidth = 60;
			}
			m_pItem[nI].SetWidth(nWidth);
		}
	}	
*/
	int nItemNum = m_pItem->GetItemNum();
	CString strItemNo;
	CCTSMonProApp* pApp = (CCTSMonProApp *)AfxGetApp();
	for( int nI = 0; nI < nItemNum; nI++ )
	{
		strItemNo.Format("Width%02d", nI+1);
		pApp->WriteProfileInt("DisplayItem", strItemNo, m_pItem[nI].GetWidth());
	}

	m_lfChListFont.DeleteObject();
}

void CCTSMonProView::OnItemchangedListModule(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	int nNewModuleID = m_wndModuleList.GetItemData(pNMListView->iItem);

	//Event가 3번 발생하므로 변경시에만 Update함  
	if(nNewModuleID != m_nCurrentModuleID)
	{
		SetModuleSelChanged(nNewModuleID);
	}

	*pResult = 0;
}

void CCTSMonProView::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();

	if(&m_wndChannelList== pWnd)
	{
		CopyDataToClipBoard(&m_wndChannelList);
	}
	if(&m_wndModuleList == pWnd)
	{
		CopyDataToClipBoard(&m_wndModuleList);
	}

	if(&m_Grid == pWnd)
	{
		m_Grid.OnEditCopy();	
	}
}

void CCTSMonProView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_Grid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(m_Grid.GetSelectedCount() > 0);	
	}
	else if(&m_wndModuleList == (CListCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndModuleList.GetSelectedCount() > 0);
	}
	else if(&m_wndChannelList == (CListCtrl *)pWnd)
	{
		pCmdUI->Enable(m_wndChannelList.GetSelectedCount()>0);
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}	
}

#include <afxadv.h>

//선택한 리스트 Control의 내용을 ClipBoard로 복사한다.
void CCTSMonProView::CopyDataToClipBoard(CListCtrl *pListCtrl)
{
	int nItem;
	CString str;
	char szBuff[128];
	
	if(pListCtrl == NULL)	return;

	int  nColumnCount =0 ;
	CHeaderCtrl* pHeaderCtrl = pListCtrl->GetHeaderCtrl();
	
	if(pHeaderCtrl)
		nColumnCount = pHeaderCtrl->GetItemCount();

	if(nColumnCount <1)		return;

	//CString 변수는 64K 길이 제한이 있으므로 메모리를 이용하여 복사한다.(OLE)
	CSharedFile sf(GMEM_MOVEABLE|GMEM_DDESHARE|GMEM_ZEROINIT);

	POSITION pos = pListCtrl->GetFirstSelectedItemPosition();
	while(pos)
	{
		nItem = pListCtrl->GetNextSelectedItem(pos);
		
		for(int j=0; j<nColumnCount; j++)
		{
			pListCtrl->GetItemText(nItem, j, szBuff, 128);	
			str += szBuff;
			if(j != nColumnCount-1)
				str += "\t";
		}
		str += "\n";
		sf.Write(str.GetBuffer(1), str.GetLength());
		str.ReleaseBuffer();
		str.Empty();
	}


	// 제일 마지막에 NULL문자를 기록하고...
	char c = '\0';
	sf.Write(&c, 1);

	// SharedFile의 메모리 핸들을 구합니다...
	DWORD dwLen = sf.GetLength();
	HGLOBAL hMem = sf.Detach();
	if (!hMem) return;

	hMem = ::GlobalReAlloc(hMem, dwLen, GMEM_MOVEABLE|GMEM_DDESHARE|GMEM_ZEROINIT);
	if (!hMem) return;


	// OleDataSource를 만들고, 그 곳에 메모리 핸들의 Data를 잡습니다... 
	COleDataSource* pSource = new COleDataSource();
	pSource->CacheGlobalData(CF_TEXT, hMem);
	if (!pSource) return;

	// 그리고, 그 OleDataSource를 Clipboard에 붙여 넣습니다.....
	pSource->SetClipboard();    
}

void CCTSMonProView::OnSelectAll() 
{
	// TODO: Add your command handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndChannelList == (CListCtrl *)pWnd)
	{
		for(int i =0; i< m_wndChannelList.GetItemCount(); i++)
		{
			m_wndChannelList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
		}
	}
	if(&m_Grid == (CGridCtrl *)pWnd)
	{
		m_Grid.OnEditSelectAll();
	}		
}

void CCTSMonProView::OnUpdateSelectAll(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	CWnd *pWnd= GetFocus();
	if(&m_wndChannelList == (CListCtrl *)pWnd)
	{
		pCmdUI->Enable(TRUE);	
	}
	else if(&m_Grid == (CGridCtrl *)pWnd)
	{
		pCmdUI->Enable(TRUE);	
	}
	else
	{
		pCmdUI->Enable(FALSE);	
	}		
}

void CCTSMonProView::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

BOOL CCTSMonProView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class
	HD_NOTIFY * phdn = (HD_NOTIFY *) lParam;

	if(phdn->hdr.code == HDN_ENDTRACK)
	{
		if(m_pItem)
		{
			int nWidth;
			int nItemNum = CItemOnDisplay::GetItemNum();
			for( int nI = 0; nI < nItemNum; nI++ )
			{
				nWidth = m_wndChannelList.GetColumnWidth(nI);
				
				if( nWidth > 1000 || nWidth <= 0 )
				{
					nWidth = 60;
				}
				m_pItem[nI].SetWidth(nWidth);
			}
		}	


//		if(phdn->hdr.hwndFrom == m_wndChannelList.m_hWnd)
		{
		//	phdn->iButton == 0		//left button
			TRACE("End Drag %d, Item %d, %d\n", wParam, phdn->iItem, phdn->hdr.idFrom);

/*			CHeaderCtrl* pHeaderCtrl = m_wndChannelList.GetHeaderCtrl();
			
			int nCount =0;
			if(pHeaderCtrl)
				nCount = pHeaderCtrl->GetItemCount();

			if(nCount < 1)	return 0;

			LPINT pnOrder = (LPINT) malloc(nCount*sizeof(int));
			ASSERT(pnOrder != NULL);

			pHeaderCtrl->GetOrderArray(pnOrder, nCount);
	
			TCHAR  lpBuffer[128];
			int i;
			LVCOLUMN col;
			col.mask = LVCF_TEXT|LVCF_SUBITEM;
			col.cchTextMax = 128;
			col.pszText = lpBuffer;
			
			for( int i=0; i < nCount; i++)
			{
			   TRACE("%d /", pnOrder[i]);
				m_wndChannelList.GetColumn(i, &col);
 
		//		TRACE("%s/%d\n", hdi.pszText, hdi.lParam);
		//		TRACE("%s/%d\n", col.pszText, col.iSubItem);
			}
			TRACE("%\n");

//			pHeaderCtrl->SetOrderArray(nCount, pnOrder);
			free(pnOrder);	
*/		}
	}
	return CFormView::OnNotify(wParam, lParam, pResult);
}

//Grid에서 채널 선택이 바뀌면 
//채널 라벨의 현재 채널명을 수정한다.
void CCTSMonProView::OnGridSelectChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_GRIDVIEW *pItem = (NM_GRIDVIEW *)pNMHDR;
    *pResult = 0;
    int nRow = pItem->iRow;
    int nCol = pItem->iColumn;

	int nChIndex = m_Grid.GetItemData(nRow, nCol);
	CString strMsg;
	if(nRow > 0 && nCol > 0)
	{
		SetSelectedChNo(m_nCurrentModuleID, nChIndex);
	}
}

//상세 보기에서 채널 선택이 바뀌면  
//채널 라벨의 현재 채널명을 수정한다.
void CCTSMonProView::OnItemchangedListChannel(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CString strMsg;
	POSITION pos = m_wndChannelList.GetFirstSelectedItemPosition();
    int nSel = m_wndChannelList.GetNextSelectedItem(pos);

	if(nSel >=  0)
	{
		SetSelectedChNo(m_nCurrentModuleID, nSel);
		if(m_nCurrentChannelNo != nSel)	//채널 인덱스가 변경되었으므로 화면의 표시도 바뀌어야 한다.
		{
			m_wndAuxList.DeleteAllItems();		//Aux List를 지우고 새로 그리도록
			m_wndCanList.DeleteAllItems();		//Can List를 지우고 새로 그리도록
		}
		//m_nCurrentChannelNo = nSel; //ksj 20201116 :주석처리

		//ksj 20201116 : 선택 채널 번호 가져오는 방식 변경 (검증 필요)
		DWORD dwID = m_wndChannelList.GetItemData(nSel);
		int nModuleID = HIWORD(dwID);
		int nChannelIndex = LOWORD(dwID);
		m_nCurrentChannelNo = nChannelIndex;
		//ksj end		
		
	}

	//InitStaticCounter(); //yulee 20181022
	//DisplayLblChange(); //yulee 20181019
	
	

	*pResult = 0;
}

void CCTSMonProView::DisplayLblChange()
{
	UpdateData();

	SetLedUnitAll();
	//yulee 20181019
	int chCnt = m_wndChannelList.GetItemCount();

	int nFontSize = 18;

	if(chCnt == 1) //yulee 20181029
	{
		m_ctrlLabelVoltage.SetFontSize(nFontSize);
		m_ctrlLabelImpedance.SetFontSize(nFontSize);
		m_ctrlLabelCurrent.SetFontSize(nFontSize);
		m_ctrlLabelCh.SetFontSize(nFontSize);
		m_ctrlLabelCapasity.SetFontSize(nFontSize);
		m_ctrlLabelStepTime.SetFontSize(nFontSize);
		m_ctrlLabelTotalTime.SetFontSize(nFontSize);
		m_ctrlLabelWatt.SetFontSize(nFontSize);
		m_ctrlLabelWattHour.SetFontSize(nFontSize);
		m_ctrlLabelVUnit.SetFontSize(nFontSize);
		m_ctrlLabelAUnit.SetFontSize(nFontSize);
		m_ctrlLabelCUnit.SetFontSize(nFontSize);
		m_ctrlLabelWUnit.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit.SetFontSize(nFontSize);

		m_ctrlLabelChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);

		m_ctrlLabelBmsChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);

		m_ctrlLabelChState.SetFontSize(15)
			//.SetTextColor(RGB(200,50,50))
			//.SetBkColor(RGB(255,0,0))
			//.SetGradientColor(RGB(255,255,255))
			//.SetFontBold(TRUE)
			//.SetGradient(TRUE)
			.SetSunken(TRUE)
			.SetFontName("HY헤드라인M");

		GetDlgItem(IDC_STA_CH1INFOBOXINCH2)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE      )->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE     )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT       )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT      )->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT     )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT       )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR    )->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR   )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT       )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATT         )->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATT        )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_W_UNIT       )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATTHOUR     )->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATTHOUR    )->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WH_UNIT      )->ShowWindow(TRUE);


		GetDlgItem(IDC_STA_CH1INFOBOXINCH4)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH4)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH2)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_VOLTAGE2     )->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_VOLTAGE2    )->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_V_UNIT1      )->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CURRENT2     )->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CURRENT2    )->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_A_UNIT1      )->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CAPASITOR2   )->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CAPASITOR2  )->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_C_UNIT1      )->ShowWindow(FALSE);

		GetDlgItem(IDC_STA_CH3INFOBOX		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_VOLTAGE3		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_STATIC_VOLTAGE3		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_V_UNIT2		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_CURRENT3		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_STATIC_CURRENT3		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_A_UNIT2		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_CAPASITOR3		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CAPASITOR3	)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_C_UNIT2		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_STA_CH4INFOBOX		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_VOLTAGE4		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_STATIC_VOLTAGE4		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_V_UNIT3		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_CURRENT4		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_STATIC_CURRENT4		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_A_UNIT3		)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_CAPASITOR4		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CAPASITOR4	)->ShowWindow(FALSE);	
		GetDlgItem(IDC_LABEL_C_UNIT3		)->ShowWindow(FALSE);

		HideOrShowButton();
		//HideVentClose();
	}
	else if(chCnt == 2)
	{
		nFontSize = 15;
		m_ctrlLabelVoltage.SetFontSize(nFontSize);
		m_ctrlLabelImpedance.SetFontSize(nFontSize);
		m_ctrlLabelCurrent.SetFontSize(nFontSize);

		m_ctrlLabelCh.SetFontSize(nFontSize);
		m_ctrlLabelCapasity.SetFontSize(nFontSize);
		m_ctrlLabelStepTime.SetFontSize(nFontSize);
		m_ctrlLabelTotalTime.SetFontSize(nFontSize);

		m_ctrlLabelCurrent2.SetFontSize(nFontSize);
		m_ctrlLabelVoltage2.SetFontSize(nFontSize);
		m_ctrlLabelCapasity2.SetFontSize(nFontSize);

		m_ctrlLabelWatt.SetFontSize(nFontSize);
		m_ctrlLabelWatt2.SetFontSize(nFontSize);
		m_ctrlLabelWattHour.SetFontSize(nFontSize);
		m_ctrlLabelWattHour2.SetFontSize(nFontSize);

		m_ctrlLabelVUnit.SetFontSize(nFontSize);
		m_ctrlLabelVUnit2.SetFontSize(nFontSize);
		m_ctrlLabelAUnit.SetFontSize(nFontSize);
		m_ctrlLabelAUnit2.SetFontSize(nFontSize);
		m_ctrlLabelCUnit.SetFontSize(nFontSize);
		m_ctrlLabelCUnit2.SetFontSize(nFontSize);
		m_ctrlLabelWUnit.SetFontSize(nFontSize);
		m_ctrlLabelWUnit2.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit2.SetFontSize(nFontSize);

		m_ctrlLabelChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName(Fun_FindMsg("InitLabel_msg1","IDD_CTSMonPro_FORM"))
			//@ .SetFontName("HY헤드라인M")
			.SetSunken(TRUE);

		m_ctrlLabelBmsChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName(Fun_FindMsg("InitLabel_msg2","IDD_CTSMonPro_FORM"))
			//@ .SetFontName("HY헤드라인M")
			.SetSunken(TRUE);

		m_ctrlLabelChState.SetFontSize(15)
			//.SetTextColor(RGB(200,50,50))
			//.SetBkColor(RGB(255,0,0))
			//.SetGradientColor(RGB(255,255,255))
			//.SetFontBold(TRUE)
			//.SetGradient(TRUE)
			.SetSunken(TRUE)
			.SetFontName("HY헤드라인M");

		GetDlgItem(IDC_STA_CH1INFOBOXINCH4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH3INFOBOX	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_VOLTAGE3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_VOLTAGE3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_V_UNIT2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CURRENT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CURRENT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_A_UNIT2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CAPASITOR3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CAPASITOR3)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_C_UNIT2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH4INFOBOX	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_VOLTAGE4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_VOLTAGE4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_V_UNIT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CURRENT4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CURRENT4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_A_UNIT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CAPASITOR4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CAPASITOR4)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_C_UNIT3	)->ShowWindow(FALSE);

		GetDlgItem(IDC_STA_CH1INFOBOXINCH2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR2)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT1	)->ShowWindow(TRUE);

		GetDlgItem(IDC_LABEL_WATT			)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATT			)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_W_UNIT			)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATTHOUR		)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATTHOUR		)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WH_UNIT		)->ShowWindow(TRUE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATT2			)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATT2			)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_W_UNIT1		)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATTHOUR2		)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATTHOUR2		)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WH_UNIT1		)->ShowWindow(TRUE);

		GetDlgItem(IDC_BTN_DATA_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_GRAPH_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_CHECK_COND		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_START			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_PAUSE			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_NEXT_STEP		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_STOP			    )->ShowWindow(TRUE);
		if(m_bUseWorkReserv) GetDlgItem(IDC_BTN_RESERV			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_VIEW_REALGRAPH	)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_EDITOR			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_ANAL				)->ShowWindow(TRUE);

		HideOrShowButton();
		//HideVentClose();	

		int nGap = 0;
		int nGapTimes = 0;
		int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);//yulee 20190706 2Ch 표시
		if(nSelLanguage == 1)//Ch2
		{
			nGap = 260;
		}
		else if(nSelLanguage == 2)
		{
			nGap = 260;
		}
		else if(nSelLanguage == 3)
		{
			nGap = 260;
		}

		nGapTimes = nGap *1;
		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH2, IDC_STA_CH2INFOBOXINCH2, nGapTimes);//yulee 20181019

		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_WATT, IDC_LABEL_WATT2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_WATT, IDC_STATIC_WATT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_W_UNIT, IDC_LABEL_W_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_WATTHOUR, IDC_LABEL_WATTHOUR2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_WATTHOUR, IDC_STATIC_WATTHOUR2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_WH_UNIT, IDC_LABEL_WH_UNIT1, nGapTimes);

	}
	else if(chCnt == 4)
	{
		m_ctrlLabelImpedance.SetFontSize(nFontSize);
		m_ctrlLabelCh.SetFontSize(nFontSize);

		m_ctrlLabelVoltage.SetFontSize(nFontSize);
		m_ctrlLabelCurrent.SetFontSize(nFontSize);
		m_ctrlLabelCapasity.SetFontSize(nFontSize);

		m_ctrlLabelCurrent2.SetFontSize(nFontSize);
		m_ctrlLabelVoltage2.SetFontSize(nFontSize);
		m_ctrlLabelCapasity2.SetFontSize(nFontSize);

		m_ctrlLabelVoltage3.SetFontSize(nFontSize); //yulee 20180905
		m_ctrlLabelCurrent3.SetFontSize(nFontSize);
		m_ctrlLabelCapasity3.SetFontSize(nFontSize);

		m_ctrlLabelCurrent4.SetFontSize(nFontSize);
		m_ctrlLabelVoltage4.SetFontSize(nFontSize);
		m_ctrlLabelCapasity4.SetFontSize(nFontSize);


		m_ctrlLabelStepTime.SetFontSize(nFontSize);
		m_ctrlLabelTotalTime.SetFontSize(nFontSize);

		m_ctrlLabelWatt.SetFontSize(nFontSize);
		m_ctrlLabelWatt2.SetFontSize(nFontSize);
		m_ctrlLabelWattHour.SetFontSize(nFontSize);
		m_ctrlLabelWattHour2.SetFontSize(nFontSize);

		m_ctrlLabelVUnit.SetFontSize(nFontSize);
		m_ctrlLabelVUnit2.SetFontSize(nFontSize);
		m_ctrlLabelAUnit.SetFontSize(nFontSize);
		m_ctrlLabelAUnit2.SetFontSize(nFontSize);
		m_ctrlLabelCUnit.SetFontSize(nFontSize);
		m_ctrlLabelCUnit2.SetFontSize(nFontSize);

		m_ctrlLabelVUnit3.SetFontSize(nFontSize); //yulee 20180905
		m_ctrlLabelVUnit4.SetFontSize(nFontSize);
		m_ctrlLabelAUnit3.SetFontSize(nFontSize);
		m_ctrlLabelAUnit4.SetFontSize(nFontSize);
		m_ctrlLabelCUnit3.SetFontSize(nFontSize);
		m_ctrlLabelCUnit4.SetFontSize(nFontSize);


		m_ctrlLabelWUnit.SetFontSize(nFontSize);
		m_ctrlLabelWUnit2.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit2.SetFontSize(nFontSize);

		m_ctrlLabelChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);

		m_ctrlLabelBmsChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);

		m_ctrlLabelChState.SetFontSize(15)
			//.SetTextColor(RGB(200,50,50))
			//.SetBkColor(RGB(255,0,0))
			//.SetGradientColor(RGB(255,255,255))
			//.SetFontBold(TRUE)
			//.SetGradient(TRUE)
			.SetSunken(TRUE)
			.SetFontName(Fun_FindMsg("InitLabel_msg3","IDD_CTSMonPro_FORM"));
		//@ 					  .SetFontName("HY헤드라인M");


		GetDlgItem(IDC_STA_CH1INFOBOXINCH4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH4	)->ShowWindow(TRUE);

		GetDlgItem(IDC_STA_CH3INFOBOX	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR3)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STA_CH4INFOBOX	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR4)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT3	)->ShowWindow(TRUE);

		GetDlgItem(IDC_STA_CH1INFOBOXINCH2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR2)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT1	)->ShowWindow(TRUE);

		GetDlgItem(IDC_STA_CH1INFOBOXINCH2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATT			)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATT			)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_W_UNIT			)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATTHOUR		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATTHOUR		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WH_UNIT		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATT2			)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATT2			)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_W_UNIT1		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATTHOUR2		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATTHOUR2		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WH_UNIT1		)->ShowWindow(FALSE);

		GetDlgItem(IDC_BTN_DATA_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_GRAPH_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_CHECK_COND		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_START			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_PAUSE			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_NEXT_STEP		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_STOP			    )->ShowWindow(TRUE);
		if(m_bUseWorkReserv) GetDlgItem(IDC_BTN_RESERV			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_VIEW_REALGRAPH	)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_EDITOR			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_ANAL				)->ShowWindow(TRUE);

		HideOrShowButton();
		//HideVentClose();	

		int nGap = 0;
		int nGapTimes = 0;
		int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);//yulee 20190706 4Ch 표시
		if(nSelLanguage == 1)//4CH
		{
			nGap = 150;
		}
		else if(nSelLanguage == 2)
		{
			nGap = 150;
		}
		else if(nSelLanguage == 3)
		{
			nGap = 150;
		}

		nGapTimes = nGap *1;
		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH4, IDC_STA_CH2INFOBOXINCH4, nGapTimes);//yulee 20181019
		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT1, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT1, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT1, nGapTimes);

		nGapTimes = nGap *2;
		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH4, IDC_STA_CH3INFOBOX, nGapTimes);//yulee 20181019
		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE3, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT3, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR3, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT2, nGapTimes);

		nGapTimes = nGap *3;
		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH4, IDC_STA_CH4INFOBOX, nGapTimes);//yulee 20181022
		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE4, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE4, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT4, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT4, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR4, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR4, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT3, nGapTimes);
	}

	UpdateData(FALSE);
}

//선택 모듈의 모든 채널에 명령 전송할 수 있도록 
//명령 Context Menu 표시 
void CCTSMonProView::OnRclickListModule(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	CMenu menu; 
	switch(gi_Language) //20190704
	{
	case 1 : menu.LoadMenu(IDR_MAINFRAME); break;
	case 2 : menu.LoadMenu(IDR_MAINFRAME_ENG); break;
	case 3 : menu.LoadMenu(IDR_MAINFRAME_PL); break;
	default : menu.LoadMenu(IDR_MAINFRAME_ENG); break;
	}
	//menu.LoadMenu(IDR_MAINFRAME);
	CMenu* popmenu = menu.GetSubMenu(2); 
	CPoint ptMouse; 
	GetCursorPos(&ptMouse); 

	m_bUseWorkReserv = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE_VAL);
	if(!m_bUseWorkReserv) //ksj 20160613 작업 예약 기능 레지스트리 옵션 처리.
	{
		popmenu->RemoveMenu(ID_CMD_RUN_QUEUE,MF_BYCOMMAND);
	}

	popmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
		ptMouse.x, ptMouse.y, AfxGetMainWnd()); 



	*pResult = 0;	
}

// 선택된 모듈에서 해당 명령이 전송가능한 채널에 명령을 전송한다.
// Continue, Run 명령은 이 함수로 보내면 안됨
// Continue => CCTSMonProDoc::SendContinueCmd()
// Run => OnCmdRun() 을 이용해야함 
// void CCTSMonProView::SendCmdToModule(int nCmd)
// {
// 
// 	CCyclerModule *pMD;
// 	CCTSMonProDoc *pDoc = GetDocument();
// 	CWordArray chArray;
// 	CString strMsg;
// 
// 	CWaitCursor wait;
// 	CDWordArray arTargetCh;
// 
// 	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 	while(pos)
// 	{
// 		int nItem = m_wndModuleList.GetNextSelectedItem(pos);
// 		int nModuleID = m_wndModuleList.GetItemData(nItem);
// 		pMD = pDoc->GetModuleInfo(nModuleID);
// 		if(pMD)
// 		{
// 			if(pMD->GetState() != PS_STATE_LINE_OFF)
// 			{
// 				chArray.RemoveAll();
// 
// 				//전송 가능한 채널 List를 구한다.
// 				for(WORD ch =0; ch<pMD->GetTotalChannel(); ch++)
// 				{
// 					if(pDoc->CheckCmdState(nModuleID, (UINT)ch, nCmd))
// 					{
// 						chArray.Add(ch);
// 						if(nCmd == SFT_CMD_STOP)
// 						{
// 							
// 							arTargetCh.Add(MAKELONG(ch, nModuleID));
// 						}
// 					}
// 				}
// 				//전송 가능한 채널이 있으면 전송 
// 				if(chArray.GetSize() > 0)
// 				{
// 					//Continue 명령은 이전 시험 검사 후 보낸다.
// 					if(nCmd == SFT_CMD_CONTINUE)
// 					{
// 						pDoc->SendContinueCmd(nModuleID, &chArray);
// 					}
// 					
// 					else//기타 명령은 단순히 명령 전송하면 된다. (cf. Run명령은 전송하면 안됨=>시험 조건 보내고 사용자 입력을 받아야함)
// 					{
// 						pDoc->SendCommand(nModuleID, &chArray, nCmd);
// 					}
// 
// 				}
// 				else
// 				{
// 					strMsg.Format("%s는 [%s]명령 전송가능한 채널이 없습니다.", pDoc->GetModuleName(nModuleID), pDoc->GetCmdString(nCmd));
// 					pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
// 				}
// 			}
// 		}
// 	}
// 
// 	//Send Oven Stop Command
// 	if(nCmd == SFT_CMD_STOP && 	pDoc->m_ctrlOven.m_bUseOvenCtrl)
// 	{
// 		//20071114 Oven Control
// 		pDoc->CheckOvenStopStateAndSendStopCmd(arTargetCh);	//모든 연동 Channel이 Stop 상태이고 Oven이 동작중인 경우 
// 	}

	//Send Oven Continue Command
//}

//선택된 모듈에서 해당 명령이 전송가능한 채널에 명령을 전송한다.
//Continue, Run 명령은 이 함수로 보내면 안됨
//Continue => CCTSMonProDoc::SendContinueCmd()
//Run => OnCmdRun() 을 이용해야함 
BOOL CCTSMonProView::SendCmdToModule(CDWordArray &adwTargetCh, int nCmd, int nCycleNo, int nStepNo)
{
	CCTSMonProDoc *pDoc = GetDocument();
	CString strMsg;

	CWordArray chArray;
	DWORD dwData;
	CWaitCursor wait;
	int nPrevModule = 0, nModuleID, nChannelIndex;
	int nCurModuleID = 0;

	for(int s=0; s<adwTargetCh.GetSize(); s++)
	{
		dwData = adwTargetCh.GetAt(s);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);

		if(s == 0)	nPrevModule = nModuleID;		

		//모듈 별로 구별하여 전송한다.
		//if(nPrevModule != nModuleID || s == arTargetCh.GetSize()-1)
		if(nPrevModule != nModuleID)
		{
			nCurModuleID = nPrevModule;
			
			//전송 가능한 채널이 있으면 전송 
			if(chArray.GetSize() > 0)
			{
				//Continue 명령은 이전 시험 검사 후 보낸다.
				if(nCmd == SFT_CMD_CONTINUE)
				{
					pDoc->SendContinueCmd(nCurModuleID, &chArray);
				}
				else if(nCmd == SFT_CMD_RUN)	//(cf. Run명령은 전송하면 안됨=>시험 조건 보내고 사용자 입력을 받아야함)
				{
					return FALSE;
				}
				else//기타 명령은 단순히 명령 전송하면 된다. 
				{
					pDoc->SendCommand(nCurModuleID, &chArray, nCmd, nCycleNo, nStepNo);
				}
				
			}
			else
			{
				strMsg.Format(Fun_FindMsg("SendCmdToModule_msg1","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				//@ strMsg.Format("%s는 [%s]명령 전송가능한 채널이 없습니다.", pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
			}
			chArray.RemoveAll();
		}
		chArray.Add(nChannelIndex);
		nPrevModule = nModuleID;
		
		if(s == adwTargetCh.GetSize()-1)
		{
			nCurModuleID = 	nModuleID;

			//전송 가능한 채널이 있으면 전송 
			if(chArray.GetSize() > 0)
			{
				//Continue 명령은 이전 시험 검사 후 보낸다.
				if(nCmd == SFT_CMD_CONTINUE)
				{
					pDoc->SendContinueCmd(nCurModuleID, &chArray);
				}
				else if(nCmd == SFT_CMD_RUN)	//(cf. Run명령은 전송하면 안됨=>시험 조건 보내고 사용자 입력을 받아야함)
				{
					return FALSE;
				}
				else//기타 명령은 단순히 명령 전송하면 된다. 
				{
					if (pDoc->SendCommand(nCurModuleID, &chArray, nCmd, nCycleNo, nStepNo))
					{
					}
					else
					{
						//AfxMessageBox(Fun_FindMsg("SendCmdToModule_msg2","IDD_CTSMonPro_FORM"));
						//@ strMsg.Format("pDoc->SendCommand() 전송 실패1");
						pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
						
					}
				}
			}
			else
			{
				strMsg.Format(Fun_FindMsg("SendCmdToModule_msg3","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				//@ strMsg.Format("%s는 [%s]명령 전송가능한 채널이 없습니다.", pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
			}

		}
		//Send MES to NG SendNgFirst Flag Fals로 전환
		//if (nCmd == SFT_CMD_CONTINUE)
		//{
		//	CCyclerChannelMESProc *pChannelMes;
		//	pChannelMes = pDoc->GetChannelMESInfo(nModuleID,nChannelIndex);
		//	if (pChannelMes->Fun_GetEQState() == EQ_STATE_AUTO)
		//	{
		//		pChannelMes->Fun_SetFirstNgState(FALSE);	//NG 보고 안한 상태로 전환
		//	}
		//}		
	}
	
	//Send Oven Stop Command
	//if(nCmd == SFT_CMD_STOP && AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE))
	//{
	//	//20071114 Oven Control	
	//	//pDoc->CheckOvenStopStateAndSendStopCmd(arTargetCh);	//모든 연동 Channel이 Stop 상태이고 Oven이 동작중인 경우 
	//	pDoc->OvenStopStateAndSendStopCmd(1);
	//}

	InitPreFlickerInfo(); //ksj 20201015

	return TRUE;
}

void CCTSMonProView::OnUserConfig() 
{
	// TODO: Add your command handler code here
	CUserConfigDlg *pDlg;
	pDlg = new CUserConfigDlg(this);

	CFont *pFont = m_Grid.GetFont();
	if (pFont)
	{
	   LOGFONT lf;
	   pFont->GetLogFont(&lf);
	   TRACE("Typeface name of font = %s\n", lf.lfFaceName);
	   pDlg->m_afont = lf;
	}

	if(pDlg->DoModal() == IDOK)
	{
		
		CFont aFont;
		aFont.CreateFontIndirect(&pDlg->m_afont);
		m_Grid.SetFont(&aFont);

		//Unit 정보를 Update 시킴 
		GetDocument()->UpdateUnitSetting();
		
		if(m_nDisplayCol != pDlg->m_nGirdColSize)
		{
			m_nDisplayCol = pDlg->m_nGirdColSize;
			ReDrawGrid();
			UpdateGridData();

			RemakeChannelList(); //ksj 20201223 : 아래에서 여기로 위치 이동. remake하면 리스트가 쓰레기 값으로 깨짐. 디버깅 필요
			UpdateChListData(); //ksj 20201223 : 아래에서 여기로 위치 이동.
		}
		
		if(m_nChannelUpdateInterval != pDlg->m_nChTimer)
		{
			m_nChannelUpdateInterval = pDlg->m_nChTimer;
			KillTimer(TIMER_DISPLAY_CHANNEL);
			SetTimer(TIMER_DISPLAY_CHANNEL, m_nChannelUpdateInterval*1000, NULL);
		}
		
		if(m_nModuleUpdateInterval  != pDlg->m_nMdTimer)
		{
			m_nModuleUpdateInterval = pDlg->m_nMdTimer;
			KillTimer(TIMER_DISPLAY_MODULE);
			SetTimer(TIMER_DISPLAY_MODULE, m_nModuleUpdateInterval*1000, NULL);
		}

		if(m_bUseCan != pDlg->m_bUseCan)
		{
			m_bUseCan = pDlg->m_bUseCan;
			if(m_bUseCan == FALSE)
			{
				m_wndCanList.ShowWindow(SW_HIDE);
				GetDlgItem(IDC_STATIC_CAN)->ShowWindow(SW_HIDE);
				m_wndAuxList.SetColumnWidth(0, 50);
				m_wndAuxList.SetColumnWidth(1, 200);
				m_wndAuxList.SetColumnWidth(2, 120);
				m_wndAuxList.SetColumnWidth(3, 90);
				m_wndAuxList.SetColumnWidth(4, 90);
				m_wndAuxList.SetColumnWidth(5, 90);
				m_wndAuxList.SetColumnWidth(6, 90);

			}
			else
			{
				m_wndCanList.ShowWindow(SW_SHOW);
				GetDlgItem(IDC_STATIC_CAN)->ShowWindow(SW_SHOW);
				m_wndAuxList.SetColumnWidth(0, 50);
				m_wndAuxList.SetColumnWidth(1, 108);
				m_wndAuxList.SetColumnWidth(2, 80);
				m_wndAuxList.SetColumnWidth(3, 60);
				m_wndAuxList.SetColumnWidth(4, 60);
				m_wndAuxList.SetColumnWidth(5, 60);
				m_wndAuxList.SetColumnWidth(6, 60);

			}
		}

		//리스트 control에 Column 정보를 반영함 
		// 		RemakeChannelList();  //주석처리 by ksj 2020123 : 재초기화시 깨지는 것 같음.
		// 		UpdateChListData(); //주석처리 by ksj 20201223 : 재초기화시 깨지는 것 같음.
		
		//		SetLedUnitAll();
		InitLabel(18);

		UpdateUserConfig(); //ksj 20201112
	}

	delete pDlg;
	pDlg = NULL;

	m_bUseAux = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseAUX", FALSE);	//ljb 2009-01 추가
	m_bUseCan = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCAN", FALSE);	//ljb 2009-01 추가
	if(m_bUseAux)
	{
		m_wndAuxList.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_AUX)->ShowWindow(SW_SHOW);
	}
	else
	{
		m_wndAuxList.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_AUX)->ShowWindow(SW_HIDE);
	}
	if(m_bUseCan)
	{
		m_wndCanList.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_CAN)->ShowWindow(SW_SHOW);
	}
	else
	{
		m_wndCanList.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_CAN)->ShowWindow(SW_HIDE);
	}
	SendMessage(WM_SIZE,0,0);

}


void CCTSMonProView::UpdateGridRowTopCell()
{
		//Unit을 다시 표시 
		CString str;
		CString strMsg;
		switch(m_nCurTabIndex)
		{
		case TAB_CH_LIST:
			str = "";
			break;
		case TAB_VOLTAGE:
			str = GetDocument()->m_strVUnit;		break;
		case TAB_CURRENT: 
			str = GetDocument()->m_strIUnit;		break;
		case TAB_CAPACITY:
			str = GetDocument()->m_strCUnit;
			break;
		case TAB_GRADE:
			str = Fun_FindMsg("UpdateGridRowTopCell_msg1","IDD_CTSMonPro_FORM");
			//@ str = "등급";
			break;
		case TAB_STATE :
			str = Fun_FindMsg("UpdateGridRowTopCell_msg2","IDD_CTSMonPro_FORM");
			//@ str = "상태";
			break;
		case TAB_TEST_NAME:	
			str = Fun_FindMsg("UpdateGridRowTopCell_msg3","IDD_CTSMonPro_FORM");
			//@ str = "작업명";
			break;
		default:
			str = "";
			break;
		}	
		
		m_Grid.SetItemText(0, 0, str);			//Row Header를 넣는다.
//		m_Grid.SetTextSize(0, 0, m_nGridFontSize);
		m_Grid.SetItemFormat(0, 0, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
}

void CCTSMonProView::OnDataRestore() 
{
	// TODO: Add your command handler code here
	CRestoreDlg dlg(GetDocument(), this);
	dlg.DoModal();
}

void CCTSMonProView::OnUpdateDataRestore(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
	//pCmdUI->Enable(FALSE);
	
}

BOOL CCTSMonProView::GridTextMode(BOOL bText)
{
	int totRow, totCol;
	totRow = m_Grid.GetRowCount();
	totCol = m_Grid.GetColumnCount();
	UINT fmt; 

	for(int row = 1; row < totRow; row++)
	{
		for(int col = 1; col < totCol; col++)
		{
			fmt = m_Grid.GetItemFormat(row, col);
			if(bText)
			{
				fmt &= ~DT_SINGLELINE;
				fmt &= ~DT_CENTER;
				fmt |= DT_LEFT;
				m_Grid.SetItemFormat(row, col, fmt);
				m_Grid.SetTextSize(row, col, 15);
			}
			else
			{
				CFont *pFont = m_Grid.GetFont();
				if (pFont)
				{
					LOGFONT lf;
					pFont->GetLogFont(&lf);
					TRACE("Typeface name of font = %s\n", lf.lfFaceName);
					m_Grid.SetTextSize(row, col, lf.lfHeight);
				}
				fmt |= DT_SINGLELINE;
				fmt &= ~DT_LEFT;
				fmt |= DT_CENTER;
				m_Grid.SetItemFormat(row, col, fmt);
			}
		}
	}
	m_Grid.Refresh();
	return TRUE;
}

//연속 표기시 각 모듈의 시작 채널 번호 
int CCTSMonProView::GetStartChNo(int nModuleID)
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *lpModule;

	int nStartCh = 1;
	//모듈들 채널 번호를 연속하여 표기할 경우 
	if(pDoc->m_bChContinueNo)
	{
		int mdID;
		for(int s =0; s<pDoc->GetInstallModuleCount(); s++)
		{
			mdID = pDoc->GetModuleID(s);
			if(mdID < nModuleID)
			{
				lpModule =  pDoc->GetCyclerMD(mdID);
				if(lpModule)
				{
					nStartCh += lpModule->GetTotalChannel();
				}
			}
		}
	}
	return nStartCh;
}

void CCTSMonProView::OnGraphView() 
{
	ExeCTSAnal(4);
}

void CCTSMonProView::ExeCTSAnal(int nType)
{
	// TODO: Add your command handler code here
	//선택 채널 리스트를 Update 한다.
	if(UpdateSelectedChList()  == FALSE && nType != 0)
	{
		AfxMessageBox(Fun_FindMsg("ExeCTSAnal_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("선택된 채널이 없습니다.");
		//CTSAnal만 실행 
		return;
	}
	
	//선택채널의 결과 Data 폴더명을 모은다.
	//폴더명1\n폴더명2\n....으로 전송('\n'으로 폴더들을 구별하여 보낸다.)
	CString strData;
	CString strTemp;
	CCyclerChannel *pChannel;
	for(int i =0; i<m_adwTargetChArray.GetSize(); i++)
	{
		//		pChannel = GetDocument()->GetChannelInfo(m_nCurrentModuleID, m_adwTargetChArray.GetAt(i));
		DWORD dwData = m_adwTargetChArray.GetAt(i);
		TRACE("M:%d,C:%d \n",HIWORD(dwData), LOWORD(dwData));
		pChannel = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
		if(pChannel)
		{
			strTemp = pChannel->GetFilePath() ;
			if(!strTemp.IsEmpty())
			{
				strData = strData + strTemp +"\n";
			}
		}
	}
	
	if(strData.IsEmpty() && nType != 0)
	{
		AfxMessageBox(Fun_FindMsg("ExeCTSAnal_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("확인할 수 있는 Data가 없습니다.");
		return;
	}
	
	
	CString strProgramName;
	//strProgramName.Format("%s\\CTSAnalyzerPro.exe", GetDocument()->m_strCurPath); //lyj 20210111 
	strProgramName.Format("%s\\CTSAnalyzerPro.exe", AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC,"Path_Analyzer",GetDocument()->m_strCurPath)); //lyj 20210111 Analyzer경로 유지보수강화
	if(GetDocument()->ExecuteProgram(strProgramName, "", PS_DATA_ANAL_PRO_CLASS_NAME, "", FALSE, TRUE))
	{
		if(nType != 0)
		{
			//Message를 전송한다.
			CWnd *pWnd = FindWindow(PS_DATA_ANAL_PRO_CLASS_NAME, NULL);
			if(pWnd)
			{
				int nSize = strData.GetLength()+1;
				char *pData = new char[nSize];
				sprintf(pData, "%s", strData);
				pData[nSize-1] = '\0';
				
				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = nType;		//CTSMonPro Program 구별 Index
				CpStructData.cbData = nSize;
				CpStructData.lpData = pData;
				
				TRACE("Send File %s\n", pData);
				
				::SendMessage(pWnd->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				
				delete [] pData;
			}
		}
	}	
}

void CCTSMonProView::ExeCTSRealGraphic()
{
	// TODO: Add your command handler code here
	//선택 채널 리스트를 Update 한다.
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("ExeCTSRealGraphic_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("선택된 채널이 없습니다.");
		return;
	}
	if (1 < m_adwTargetChArray.GetSize())
	{
		AfxMessageBox(Fun_FindMsg("ExeCTSRealGraphic_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("1채널만 선택 하세요.");
		return;
	}
	
	//선택채널의 결과 Data 폴더명을 모은다.
	//폴더명1\n폴더명2\n....으로 전송('\n'으로 폴더들을 구별하여 보낸다.)
	CString strRunCh;

	for(int i =0; i<m_adwTargetChArray.GetSize(); i++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(i);
		TRACE("M:%d,C:%d \n",HIWORD(dwData), LOWORD(dwData));
		strRunCh.Format("M%02dCH%02d",HIWORD(dwData), LOWORD(dwData));
 		/*pChannel = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
 		if(pChannel)
 		{
 			strTemp = pChannel->GetFilePath() ;
 			if(!strTemp.IsEmpty())
 			{
 				strData = strData + strTemp +"\n";
 			}
 		}*/
		break;
	}
	
	CString strProgramName;
	strProgramName.Format("%s\\CTSRealGraph.exe", g_AppPath);	
	if(GetDocument()->ExecuteProgram(strProgramName, strRunCh, PS_DATA_REAL_GRAPH_CLASS_NAME, "", FALSE, TRUE))
	{
 		/*if(nType != 0)
 		{
 			//Message를 전송한다.
 			CWnd *pWnd = FindWindow(PS_DATA_ANAL_PRO_CLASS_NAME, NULL);
 			if(pWnd)
 			{
 				int nSize = strData.GetLength()+1;
 				char *pData = new char[nSize];
 				sprintf(pData, "%s", strData);
 				pData[nSize-1] = '\0';
 
 				COPYDATASTRUCT CpStructData;
 				CpStructData.dwData = nType;		//CTSMonPro Program 구별 Index
 				CpStructData.cbData = nSize;
 				CpStructData.lpData = pData;
 
 				TRACE("Send File %s\n", pData);
 				
 				::SendMessage(pWnd->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
 				
 				delete [] pData;
 			}
 		}*/
	}	
}

void CCTSMonProView::SetCurTab(int nTab)
{
//	GetDlgItem(IDC_GRID_SEL_CH_STATIC)->ShowWindow(SW_SHOW);
//	GetDlgItem(IDC_SEL_CH_STATIC)->ShowWindow(SW_HIDE);

	m_tabCtrl.SetCurSel(nTab);
	m_nCurTabIndex = nTab;
	TRACE("Tab %d Selected\n", nTab);
	CString strMsg;
	/*switch(nTab)
	{

	case TAB_CH_LIST:
		m_wndChannelList.ShowWindow(SW_SHOW);
		m_Grid.ShowWindow(SW_HIDE);
		//m_wndAuxList.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_GRID_SEL_CH_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SEL_CH_STATIC)->ShowWindow(SW_SHOW);
		break;
	case TAB_VOLTAGE:
		m_wndChannelList.ShowWindow(SW_HIDE);
		m_Grid.ShowWindow(SW_SHOW);
		GridTextMode(FALSE);
		break;
	case TAB_CURRENT: 
		m_wndChannelList.ShowWindow(SW_HIDE);
		m_Grid.ShowWindow(SW_SHOW);
		GridTextMode(FALSE);
		break;
	case TAB_CAPACITY:
		m_wndChannelList.ShowWindow(SW_HIDE);
		m_Grid.ShowWindow(SW_SHOW);
		GridTextMode(FALSE);
		break;
	case TAB_TEST_NAME:	
		m_wndChannelList.ShowWindow(SW_HIDE);
		m_Grid.ShowWindow(SW_SHOW);
		GridTextMode(TRUE);
		break;
	case TAB_STATE:
	case TAB_GRADE:
	default:
		GridTextMode(FALSE);
		m_wndChannelList.ShowWindow(SW_HIDE);
		m_Grid.ShowWindow(SW_SHOW);
		//m_wndAuxList.ShowWindow(SW_SHOW);
		break;
	}
	*/
	UpdateGridRowTopCell();
	UpdateGridData();
	
}

void CCTSMonProView::OnExeDataView() 
{
	// TODO: Add your command handler code here
	ExeCTSAnal();
}

void CCTSMonProView::OnWorkLogView() 
{
	// TODO: Add your command handler code here
	CCyclerChannel *pChInfo;
	int nModuleID, nChannel;
	if(GetSelModuleChannel(nModuleID, nChannel))
	{
		pChInfo = GetDocument()->GetChannelInfo(nModuleID, nChannel);
		if(pChInfo)
		{
			if(GetDocument()->ExecuteProgram("notepad.exe", pChInfo->GetLogFileName(), "", "", TRUE, FALSE) == FALSE)
			{
				AfxMessageBox(pChInfo->GetLogFileName() +Fun_FindMsg("OnWorkLogView_msg1","IDD_CTSMonPro_FORM"));
				//@ AfxMessageBox(pChInfo->GetLogFileName() +"를 찾을 수 없습니다.");
			}
		}
	}
	else
	{
		AfxMessageBox(Fun_FindMsg("OnWorkLogView_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("선택된 채널이 없습니다.");
	}

// 	CCyclerChannel *pChInfo;	
// 	if(UpdateSelectedChList() == FALSE)
// 	{
// 		AfxMessageBox("선택된 채널이 없습니다.");
// 		return;
// 	}
// 
// 	pChInfo = GetDocument()->GetChannelInfo(m_nCurrentModuleID, m_aTargetChArray.GetAt(0));
// 	if(pChInfo)
// 	{
// 		if(GetDocument()->ExecuteProgram("notepad.exe", pChInfo->GetLogFileName(), "", "", TRUE, FALSE) == FALSE)
// 		{
// 			AfxMessageBox(pChInfo->GetLogFileName() +"를 찾을 수 없습니다.");
// 		}
// 	}
}

void CCTSMonProView::OnWorkFolderView() //lyj 20210108 작업 폴더 보기 메뉴 추가
{
	// TODO: Add your command handler code here
	CCyclerChannel *pChInfo;
	int nModuleID, nChannel;
	if(GetSelModuleChannel(nModuleID, nChannel))
	{
		pChInfo = GetDocument()->GetChannelInfo(nModuleID, nChannel);
		if(pChInfo)
		{
			if(ShellExecute(NULL, "open", "explorer.exe",  pChInfo->GetChFilePath(), NULL, SW_SHOW) == FALSE)
			{
				AfxMessageBox(pChInfo->GetChFilePath() +Fun_FindMsg("OnWorkLogView_msg1","IDD_CTSMonPro_FORM"));
			}
		}
	}
	else
	{
		AfxMessageBox(Fun_FindMsg("OnWorkLogView_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("선택된 채널이 없습니다.");
	}
}


//현재 Step을 완료한 후 멈추도록 한다.
void CCTSMonProView::OnCmdStopStep() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 
		return;
	}
	
	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	//ifUpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE) //lyj 20201105 세인ENG /
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_STOP)+Fun_FindMsg("OnCmdStopStep_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_STOP)+"을 전송 가능한 채널이 없습니다.");
		return;
	}
	
	CString strMsg;
	//Warning message for Samsung QC Cycler
	CWorkWarnin WorkWarningDlg;
	strMsg = Fun_FindMsg("OnCmdStopStep_msg2","IDD_CTSMonPro_FORM");
	//@ strMsg = "작업을 완료하면 이 후 새로운 작업을 해야 합니다.";
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;
	
	strMsg.Format(Fun_FindMsg("OnCmdStopStep_msg3","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_STOP));
	//@ strMsg.Format("선택된 채널의 작업중인 현재 Step을 완료 후 중지하게 됩니다.\n\n[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_STOP));
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdStopStep_msg4","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		//여러 채널을 동시에 보내지 않고 1채널씩 전송한다.
		CCyclerChannel *pChInfo;
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(index);
			CDWordArray OneChArray;
			OneChArray.Add(dwData);
//			m_nCurrentModuleID = HIWORD(dwData);		//20090601 KHS
			pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
			if(pChInfo)
			{
//				GetDocument()->SendCommand(m_nCurrentModuleID, &OneChArray, SFT_CMD_STOP, pChInfo->GetCurCycleCount(), pChInfo->GetStepNo());	
				SendCmdToModule(OneChArray, SFT_CMD_STOP, pChInfo->GetCurCycleCount(), pChInfo->GetStepNo());

			}
		}
	}	
}

void CCTSMonProView::OnUpdateCmdStopStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
			pCmdUI->Enable(UpdateUIChList(SFT_CMD_STOP, FALSE));		
// 		}
// 		else
// 		{
// 			pCmdUI->Enable(FALSE);
// 		}
	}
}

//현재 Cycle을 완료한 후 멈추도록 한다.
void CCTSMonProView::OnCmdStopCycle() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 	
		return;
	}
	
	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	//if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE) //lyj 20201105 세인ENG
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_STOP)+Fun_FindMsg("OnCmdStopCycle_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_STOP)+"을 전송 가능한 채널이 없습니다.");
		return;
	}
	
	CString strMsg;
	//Warning message for Samsung QC Cycler
	CWorkWarnin WorkWarningDlg;
	strMsg = Fun_FindMsg("OnCmdStopCycle_msg2","IDD_CTSMonPro_FORM");
	//@ strMsg = "작업을 완료하면 이 후 새로운 작업을 해야 합니다.";
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;

	strMsg.Format(Fun_FindMsg("OnCmdStopCycle_msg3","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_STOP));
	//@ strMsg.Format("선택된 채널의 작업중인 현재 Cycle 반복을 완료 후 중지하게 됩니다.\n\n[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_STOP));
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdStopCycle_msg4","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		//여러 채널을 동시에 보내지 않고 1채널씩 전송한다.
		CCyclerChannel *pChInfo;
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(index);
			CDWordArray OneChArray;
			OneChArray.Add(dwData);
			pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
			if(pChInfo)
			{
				//현재 Cycle의 최종 Step번호를 찾는다.
				CScheduleData schData(pChInfo->GetScheduleFile());
				int nStepNo = 0;
				for(int step = pChInfo->GetStepNo(); step<=schData.GetStepSize(); step++)
				{
					CStep *pStep = schData.GetStepData(step-1);
					if(pStep && pStep->m_type == PS_STEP_LOOP)
					{
						nStepNo = step;
						break;
					}
				}
				if(nStepNo)	
				{
					//GetDocument()->SendCommand(m_nCurrentModuleID, &OneChArray, SFT_CMD_STOP, pChInfo->GetCurCycleCount(), nStepNo);	
					SendCmdToModule(OneChArray, SFT_CMD_STOP, pChInfo->GetCurCycleCount(), nStepNo);
				}
			}
		}
	}		
}

void CCTSMonProView::OnUpdateCmdStopCycle(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
			pCmdUI->Enable(UpdateUIChList(SFT_CMD_STOP, FALSE));		
// 		}
// 		else
// 		{
// 			pCmdUI->Enable(FALSE);
// 		}
	}
}


//작업자가 입력한 Cycle고 Step을 완료한 후 멈추도록 한다.
void CCTSMonProView::OnCmdStopEtc() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 	
		return;
	}

	int nCycleNo = 0;
	int nStepNo = 0;
	CNoInputDlg dlg;
	if(IDOK != dlg.DoModal())
	{
		return;
	}
	nCycleNo = dlg.m_nCycleNo;
	nStepNo = dlg.m_nStepNo;

	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	//if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE) //lyj 20201105 세인ENG
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_STOP)+Fun_FindMsg("OnCmdStopEtc_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_STOP)+"을 전송 가능한 채널이 없습니다.");
		return;
	}
	
	//입력된 CycleNo, StepNo 유효성 검사
	CString strMsg;

	//Warning message for Samsung QC Cycler
	CWorkWarnin WorkWarningDlg;
	strMsg = Fun_FindMsg("OnCmdStopEtc_msg2","IDD_CTSMonPro_FORM");
	//@ strMsg = "작업을 완료하면 이 후 새로운 작업을 해야 합니다.";
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;

	CCyclerChannel *pChInfo;
	CDWordArray adwFailCh;
	DWORD dwData;

	for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
	{
		dwData = m_adwTargetChArray.GetAt(index);
//		m_nCurrentModuleID = HIWORD(dwData);		//20090601 KHS
		pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
		if(nCycleNo < pChInfo->GetCurCycleCount())	//이미 완료한 cycle
		{
			adwFailCh.Add(m_adwTargetChArray.GetAt(index));
		}
		else if(nCycleNo == pChInfo->GetCurCycleCount())
		{
			if( nStepNo < pChInfo->GetStepNo())		//이미 완료한 Step
			{
				adwFailCh.Add(m_adwTargetChArray.GetAt(index));
			}
		}

		//현재 cycle수 보다 큰값이 입력되었는지 확인 
		CScheduleData schData(pChInfo->GetScheduleFile());
		//1. StepNo검사 
		if(nStepNo >= schData.GetStepSize())	//End Step으로는 갈수 없다.
		{
			strMsg.Format(	Fun_FindMsg("OnCmdStopEtc_msg3","IDD_CTSMonPro_FORM"), 
				//@ strMsg.Format(	"CH %d는 적용된 Step은 %d Step입니다. %d 미만값을 입력하십시요.", 
							m_adwTargetChArray.GetAt(index)+1,
							schData.GetStepSize(),
							schData.GetStepSize()
							);

		}
		//2. Cycle Check
		if(nCycleNo >schData.GetCurrentStepCycle(nStepNo-1))
		{
			strMsg.Format(	Fun_FindMsg("OnCmdStopEtc_msg4","IDD_CTSMonPro_FORM"), 
				//@ strMsg.Format(	"CH %d는 Step %d를 %d회 시행 하도록 설정되어 있습니다. %d 이하값을 입력하십시요.", 
							m_adwTargetChArray.GetAt(index)+1, 
							nStepNo, 
							schData.GetCurrentStepCycle(nStepNo-1), 
							schData.GetCurrentStepCycle(nStepNo-1)
						 );
			AfxMessageBox(strMsg);
			return;
		}
	}


	if(adwFailCh.GetSize() > 0)
	{
		CString strTemp;
		strMsg.Empty();
		for(int a = 0; a<adwFailCh.GetSize(); a++)
		{
			dwData = adwFailCh.GetAt(a);
			strTemp.Format("[M%02d-CH%02d] ", HIWORD(dwData), LOWORD(dwData)+1);
			strMsg += strTemp;
		}
		strTemp.Format(Fun_FindMsg("OnCmdStopEtc_msg5","IDD_CTSMonPro_FORM"), 
			//@ strTemp.Format("%s 는 입력한 Cycle %d, Step %d를 이미 수행하여 바로 [%s]을 실행합니다.", 
			strMsg, nCycleNo, nStepNo, GetDocument()->GetCmdString(SFT_CMD_STOP));
		strMsg.Format(Fun_FindMsg("OnCmdStopEtc_msg6","IDD_CTSMonPro_FORM"), 
			//@ strMsg.Format("%s 나머지 채널은 Cycle %d, Step %d 까지 완료 후 잠시멈추게 됩니다.\n\n[%s]을 실행하시겠습니까?", 
			strTemp, nCycleNo, nStepNo, GetDocument()->GetCmdString(SFT_CMD_STOP));
	}
	else
	{
		strMsg.Format(	Fun_FindMsg("OnCmdStopEtc_msg7","IDD_CTSMonPro_FORM"), 
			//@ strMsg.Format(	"선택된 채널을 Cycle %d, Step %d 까지 완료 후 잠시멈추게 됩니다.\n\n[%s]을 실행하시겠습니까?", 
						nCycleNo, nStepNo, GetDocument()->GetCmdString(SFT_CMD_STOP));
	}
	
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdStopEtc_msg8","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			CDWordArray OneChArray;
			OneChArray.Add(m_adwTargetChArray.GetAt(index));
			SendCmdToModule(OneChArray, SFT_CMD_STOP, nCycleNo, nStepNo);	
//			GetDocument()->SendCommand(m_nCurrentModuleID, &OneChArray, SFT_CMD_STOP, nCycleNo, nStepNo);	
		}
	}			
}

void CCTSMonProView::OnUpdateCmdStopEtc(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
			pCmdUI->Enable(UpdateUIChList(SFT_CMD_STOP, FALSE));		
// 		}
// 		else
// 		{
// 			pCmdUI->Enable(FALSE);
// 		}
	}
}

void CCTSMonProView::OnCmdStopCancel() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 
		return;
	}
	
	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_RESET_RESERVED_CMD)  == FALSE)
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_RESET_RESERVED_CMD)+"을 전송 가능한 채널이 없습니다.");
		return;
	}
	
	CString strMsg;
	strMsg.Format(Fun_FindMsg("OnCmdStopCancel_msg1","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_RESET_RESERVED_CMD));
	//@ strMsg.Format("선택된 채널의 현재 예약된 [%s]을 취소 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_RESET_RESERVED_CMD));
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdStopCancel_msg2","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		CCyclerChannel *pChInfo;
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(index);
			CDWordArray OneChArray;
			OneChArray.Add(dwData);
//			m_nCurrentModuleID = HIWORD(dwData);		//20090601 KHS
			pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
			if(pChInfo && pChInfo->IsStopReserved())
			{
				SendCmdToModule(OneChArray, SFT_CMD_RESET_RESERVED_CMD);	
			}
		}
	}
}

void CCTSMonProView::OnUpdateCmdStopCancel(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
			pCmdUI->Enable(UpdateUIChList(SFT_CMD_RESET_RESERVED_CMD, FALSE));		
// 		}
// 		else
// 		{
// 			pCmdUI->Enable(FALSE);
// 		}
	}
}

void CCTSMonProView::OnCmdPauseStep() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 
		return;
	}
	
	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_PAUSE)+Fun_FindMsg("OnCmdPauseStep_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_PAUSE)+"을 전송 가능한 채널이 없습니다.");
		return;
	}
	
	CString strMsg;
	strMsg.Format(Fun_FindMsg("OnCmdPauseStep_msg2","IDD_CTSMonPro_FORM"), 
		//@ strMsg.Format("선택된 채널의 작업중인 현재 Step을 완료 후 %s 됩니다.\n\n[%s]을 실행하시겠습니까?", 
		GetDocument()->GetCmdString(SFT_CMD_PAUSE), GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdPauseStep_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		//여러 채널을 동시에 보내지 않고 1채널씩 전송한다.
		CCyclerChannel *pChInfo;
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(index);
			CDWordArray OneChArray;
			OneChArray.Add(dwData);
//			m_nCurrentModuleID = HIWORD(dwData);		//20090601 KHS
			pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
			if(pChInfo)
			{
				SendCmdToModule(OneChArray, SFT_CMD_PAUSE, pChInfo->GetCurCycleCount(), pChInfo->GetStepNo());	
				//GetDocument()->SendCommand(m_nCurrentModuleID, &OneChArray, SFT_CMD_PAUSE, pChInfo->GetCurCycleCount(), pChInfo->GetStepNo());	
			}
		}
	}		
}

void CCTSMonProView::OnUpdateCmdPauseStep(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
			pCmdUI->Enable(UpdateUIChList(SFT_CMD_PAUSE, FALSE));	
// 		}
// 		else
// 		{
// 			pCmdUI->Enable(FALSE);
// 		}
	}
}

void CCTSMonProView::OnCmdPauseCycle() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 	
		return;
	}
	
	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_PAUSE)+Fun_FindMsg("OnCmdPauseCycle_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_PAUSE)+"을 전송 가능한 채널이 없습니다.");
		return;
	}
	
	CString strMsg;
	strMsg.Format(Fun_FindMsg("OnCmdPauseCycle_msg2","IDD_CTSMonPro_FORM"), 
		//@ strMsg.Format("선택된 채널의 작업중인 현재 Cycle 반복을 완료 후 %s 됩니다.\n\n[%s]을 실행하시겠습니까?", 
		GetDocument()->GetCmdString(SFT_CMD_PAUSE), GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdPauseCycle_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		//여러 채널을 동시에 보내지 않고 1채널씩 전송한다.
		CCyclerChannel *pChInfo;
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(index);
			CDWordArray OneChArray;
			OneChArray.Add(dwData);
//			m_nCurrentModuleID = HIWORD(dwData);		//20090601 KHS
			pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
			if(pChInfo)
			{
				//현재 Cycle의 최종 Step번호를 찾는다.
				CScheduleData schData(pChInfo->GetScheduleFile());
				int nStepNo = 0;
				for(int step = pChInfo->GetStepNo(); step<=schData.GetStepSize(); step++)
				{
					CStep *pStep = schData.GetStepData(step-1);
					if(pStep && pStep->m_type == PS_STEP_LOOP)
					{
						nStepNo = step;
						break;
					}
				}
				if(nStepNo)	
				{
					SendCmdToModule(OneChArray, SFT_CMD_PAUSE, pChInfo->GetCurCycleCount(), nStepNo);	
				}
			}
		}
	}			
}

void CCTSMonProView::OnUpdateCmdPauseCycle(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
			pCmdUI->Enable(UpdateUIChList(SFT_CMD_PAUSE, FALSE));	
// 		}
// 		else
// 		{
// 			pCmdUI->Enable(FALSE);
// 		}
	}
}

void CCTSMonProView::OnCmdPauseEtc() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 	
		return;
	}

	int nCycleNo = 0;
	int nStepNo = 0;
	
	//예약할 Cycle 번호와 Step번호 입력
	CNoInputDlg dlg;
	if(IDOK != dlg.DoModal())
	{
		return;
	}
	nCycleNo = dlg.m_nCycleNo;
	nStepNo = dlg.m_nStepNo;

	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_PAUSE)+Fun_FindMsg("OnCmdPauseEtc_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_PAUSE)+"을 전송 가능한 채널이 없습니다.");
		return;
	}

	//입력된 CycleNo, StepNo 유효성 검사
	CString strMsg;
	CCyclerChannel *pChInfo;
	CDWordArray adwFailCh;
	DWORD dwData;

	for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
	{
		dwData = m_adwTargetChArray.GetAt(index);
//		m_nCurrentModuleID = HIWORD(dwData);		//20090601 KHS
		pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
		if(nCycleNo < pChInfo->GetCurCycleCount())	//이미 완료한 cycle
		{
			adwFailCh.Add(m_adwTargetChArray.GetAt(index));
		}
		else if(nCycleNo == pChInfo->GetCurCycleCount())
		{
			if( nStepNo < pChInfo->GetStepNo())		//이미 완료한 Step
			{
				adwFailCh.Add(m_adwTargetChArray.GetAt(index));
			}
		}
		
		//현재 cycle수 보다 큰값이 입력되었는지 확인 
		CScheduleData schData(pChInfo->GetScheduleFile());
		//1. step no check
		if(nStepNo >= schData.GetStepSize())	//End Step으로는 갈수 없다.
		{
			strMsg.Format(Fun_FindMsg("OnCmdPauseEtc_msg2","IDD_CTSMonPro_FORM"), 
				//@ strMsg.Format(	"CH %d는 적용된 Step은 %d Step입니다. %d 미만값을 입력하십시요.", 
							m_adwTargetChArray.GetAt(index)+1,
							schData.GetStepSize(),
							schData.GetStepSize()
							);

		}

		//2. cycle check
		if(nCycleNo >schData.GetCurrentStepCycle(nStepNo-1))
		{
			strMsg.Format(Fun_FindMsg("OnCmdPauseEtc_msg3","IDD_CTSMonPro_FORM"), 
				//@ strMsg.Format(	"CH %d는 Step %d를 %d회 시행 하도록 설정되어 있습니다. %d이하 값을 입력하십시요.", 
							m_adwTargetChArray.GetAt(index)+1, 
							nStepNo, 
							schData.GetCurrentStepCycle(nStepNo-1), 
							schData.GetCurrentStepCycle(nStepNo-1)
						 );
			AfxMessageBox(strMsg);
			return;
		}
	}

	if(adwFailCh.GetSize() > 0)
	{
		CString strTemp;
		strMsg.Empty();
		for(int a = 0; a<adwFailCh.GetSize(); a++)
		{
			dwData = adwFailCh.GetAt(a);
			strTemp.Format("[M%02d-CH%02d] ", HIWORD(dwData), LOWORD(dwData)+1);
			strMsg += strTemp;
		}
		strTemp.Format(Fun_FindMsg("OnCmdPauseEtc_msg4","IDD_CTSMonPro_FORM"), 
			//@ strTemp.Format("%s 는 입력한 Cycle %d, Step %d를 이미 수행하여 바로 [%s]을 실행합니다.", 
			strMsg, nCycleNo, nStepNo, GetDocument()->GetCmdString(SFT_CMD_PAUSE));
		strMsg.Format(Fun_FindMsg("OnCmdPauseEtc_msg5","IDD_CTSMonPro_FORM"), 
			//@ strMsg.Format("%s 나머지 채널은 Cycle %d, Step %d 까지 완료 후 잠시멈추게 됩니다.\n\n[%s]을 실행하시겠습니까?", 
			strTemp, nCycleNo, nStepNo, GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	}
	else
	{
		strMsg.Format(	Fun_FindMsg("OnCmdPauseEtc_msg6","IDD_CTSMonPro_FORM"), 
			//@ strMsg.Format(	"선택된 채널을 Cycle %d, Step %d 까지 완료 후 잠시멈추게 됩니다.\n\n[%s]을 실행하시겠습니까?", 
						nCycleNo, nStepNo, GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	}
	
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdPauseEtc_msg7","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			CDWordArray OneChArray;
			OneChArray.Add(m_adwTargetChArray.GetAt(index));
			SendCmdToModule(OneChArray, SFT_CMD_PAUSE, nCycleNo, nStepNo);
//			GetDocument()->SendCommand(m_nCurrentModuleID, &OneChArray, SFT_CMD_PAUSE, nCycleNo, nStepNo);	
		}
	}			
}

void CCTSMonProView::OnUpdateCmdPauseEtc(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
			pCmdUI->Enable(UpdateUIChList(SFT_CMD_PAUSE, FALSE));		
// 		}
// 		else
// 		{
// 			pCmdUI->Enable(FALSE);
// 		}
	}
}

void CCTSMonProView::OnCmdPauseCancel() 
{
	// TODO: Add your command handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		//Module 명령 지원안함 
		return;
	}
	
	//선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_RESET_RESERVED_CMD)  == FALSE)
	{
		AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_RESET_RESERVED_CMD)+Fun_FindMsg("OnCmdPauseCancel_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox(GetDocument()->GetCmdString(SFT_CMD_RESET_RESERVED_CMD)+"을 전송 가능한 채널이 없습니다.");
		return;
	}
	
	CString strMsg;
	strMsg.Format(Fun_FindMsg("OnCmdPauseCancel_msg2","IDD_CTSMonPro_FORM"), GetDocument()->GetCmdString(SFT_CMD_RESET_RESERVED_CMD));
	//@ strMsg.Format("선택된 채널의 현재 예약된 [%s]을 취소 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_RESET_RESERVED_CMD));
	if(IDYES == MessageBox(strMsg, Fun_FindMsg("OnCmdPauseCancel_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ if(IDYES == MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	{
		CCyclerChannel *pChInfo;
		for(int index =0; index < m_adwTargetChArray.GetSize(); index++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(index);
			CDWordArray OneChArray;
			//OneChArray.Add(m_adwTargetChArray.GetAt(index));
			OneChArray.Add(dwData);
//			m_nCurrentModuleID = HIWORD(dwData);		//20090601 KHS
			//pChInfo = GetDocument()->GetChannelInfo(m_nCurrentModuleID, m_adwTargetChArray.GetAt(index));
			pChInfo = GetDocument()->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
			if(pChInfo && pChInfo->IsPauseReserved())
			{
				SendCmdToModule(OneChArray, SFT_CMD_RESET_RESERVED_CMD);
				//GetDocument()->SendCommand(m_nCurrentModuleID, &OneChArray, SFT_CMD_RESET_RESERVED_CMD);	
			}
		}
	}
}

void CCTSMonProView::OnUpdateCmdPauseCancel(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		pCmdUI->Enable(FALSE);		
	}
	else
	{
		//Version Check
// 		CCyclerModule *pModuleInfo;
// 		pModuleInfo = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
// 		if(pModuleInfo && pModuleInfo->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
// 		{
 			pCmdUI->Enable(UpdateUIChList(SFT_CMD_RESET_RESERVED_CMD, FALSE));		
// 		}
// 		else
// 		{
//			pCmdUI->Enable(FALSE);		
//		}
	}
}

void CCTSMonProView::SetSelectedChNo(int nModuleID, int nChannelIndex)
{
	CString strMsg;
	
	if(nChannelIndex < 0)
	{
/*
		if(m_tabCtrl.GetCurSel() == TAB_CH_LIST)
		{
			GetDlgItem(IDC_SEL_CH_STATIC)->GetWindowText(strMsg);
		}
		else
		{
			GetDlgItem(IDC_GRID_SEL_CH_STATIC)->GetWindowText(strMsg);
		}
*/
		if(strMsg.IsEmpty())	return;

		nChannelIndex = atol(strMsg.Mid(3))-1;		//Ch 1 형태 표기에서 채널 번호만 추출 
		
		if(nChannelIndex < 0)	return;

	}
	else
	{
		int nStartCh = GetStartChNo(nModuleID);
		strMsg.Format("Ch %d", nStartCh + nChannelIndex);
		
	}

	GetDlgItem(IDC_GRID_SEL_CH_STATIC)->SetWindowText(strMsg);
	CString strTemp;

	if (nModuleID < 1) 
		strTemp.Format("ALL-%s", strMsg);
	else 
		strTemp.Format("Unit%d-%s", nModuleID, strMsg);
	m_ctrlLabelChInfo.SetText(strTemp);
	
	UpdateChInfoList();

	CCyclerChannel *pChInfo = GetDocument()->GetChannelInfo(nModuleID, nChannelIndex);
	if(pChInfo)
	{
		long lCycleNo, lStepNo;
		pChInfo->GetReservedStep(lCycleNo, lStepNo);
		strMsg.Empty();
		if(pChInfo->IsStopReserved())
		{
			strMsg.Format(Fun_FindMsg("SetSelectedChNo_msg1","IDD_CTSMonPro_FORM"), lStepNo, lCycleNo , GetDocument()->GetCmdString(SFT_CMD_STOP));
			//@ strMsg.Format("명령예약: Step %d, Cycle %d 까지 완료후 %s 예정", lStepNo, lCycleNo , GetDocument()->GetCmdString(SFT_CMD_STOP));
			m_ReservedCmdStatic.SetBkColor(RGB(255, 192, 192));
			m_ReservedCmdStatic.FlashBackground(TRUE);
		}
		else if(pChInfo->IsPauseReserved())
		{
			strMsg.Format(Fun_FindMsg("SetSelectedChNo_msg2","IDD_CTSMonPro_FORM"), lStepNo, lCycleNo, GetDocument()->GetCmdString(SFT_CMD_PAUSE));
			//@ strMsg.Format("명령예약: Step %d, Cycle %d 까지 완료후 %s 예정", lStepNo, lCycleNo, GetDocument()->GetCmdString(SFT_CMD_PAUSE));
			m_ReservedCmdStatic.SetBkColor(RGB(192, 192, 255));
			m_ReservedCmdStatic.FlashBackground(TRUE);
		}
		else
		{
			m_ReservedCmdStatic.SetBkColor(GetSysColor(COLOR_3DFACE));
			m_ReservedCmdStatic.FlashBackground(FALSE);
		}

		pCurrentChannel = pChInfo;
		GetDlgItem(IDC_RESERVED_CMD_STATIC)->SetWindowText(strMsg);
	}
}


void CCTSMonProView::OnSimpleTest() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	CString strTestPath, strContent, strLogMsg, strTestName, strScheduleFileName;

	//1. 선택된 채널 중 Run 명령이 전송가능한 채널을 Update 한다.
	// 확인 Message 창이나 공정 선택 창이 떠있는 동안 모듈의 상태가 변하여서 전송가능 못하 수도 있으므로 Check

	//다른 입력창들이 많으므로 확이 하지 않는다.
	//2. Check to user
	//if(IDYES!=MessageBox("작업 시작을 실행하시겠습니까?", "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	//	return;

	//3. 스케쥴 목록에서 스케쥴 선택
	//   1) 한개의 시험은 모두 같은 조건이 할당된 채널들이어야 한다.
	//   2) 다른 조건이 할당된 채널이 같은 시험이 되지 않도록 시험 시작시에 선택된 채널에 언제나 시험 조건을 새로 전송한다.
	//

	CDWordArray adwPauseCh;
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;
	//int nItem;

	//BOOL	bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);
	BOOL	bDownAllRawDataBefRun = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DownAllRawDataBefRun", TRUE); //lyj 20201023 간이충방전 시 이전 데이터 다운

	UINT nModuleID = 0;
	UINT nChIndex = 0;

	//////////////////////////////////////////////////////////////////////////
	//m_awTargetChArray을 구함
	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnSimpleTest_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
#ifndef _DEBUG
		return;
#endif

	}
	//////////////////////////////////////////////////////////////////////////

	int nSelCount = m_adwTargetChArray.GetSize();

	CPtrArray	aPtrSelCh;
	//	CWordArray *aSelChArray = new CWordArray[nSelModuleCount];
	CString strLotNo;
	CString strTemp;
	BOOL bStartStepOption = TRUE;
	///선택한 채널들이 이전에 정상적으로 끝났는지 확인한다.
	///시작 명령을 취소하면 나중에 Run명령을 전송하면 안된다.

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		if (pChannel->GetState() == PS_STATE_RUN)
		{
			strTemp.Format(Fun_FindMsg("OnSimpleTest_msg2","IDD_CTSMonPro_FORM"),nModuleID,nChIndex+1);
			//@ strTemp.Format("Module : %d, Ch %d 채널을 시작 할 수 없습니다. 채널을 다시 선택 하세요.",nModuleID,nChIndex+1);
			AfxMessageBox(strTemp);
			return;
		}
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

		// 		if(pChannel->GetVoltage() < fMinVoltage)
		// 		{
		// 			//역전압이나 최저 설정 전압보다 작을 경우 리턴시킴 -> 향후 YesNo 로 선택할 수 있도록 해야하지 않을까? kjh
		// 			CString strErrorMsg;
		// 			strErrorMsg.Format("채널 %d에 최저 설정 전압보다 작습니다. \nCell의 방향 및 연결 상태를 확인하세요",
		// 				nChIndex+1);
		// 			AfxMessageBox(strErrorMsg);
		// #ifndef _DEBUG
		// 			return;
		// #endif
		// 		}

		//Step Skip 기능 가능 여부 검사 
		if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSIONV3)		bStartStepOption = FALSE;

		// 		if(bCheckPrevData)
		// 		{
		// 			CWordArray wOneCh;
		// 			wOneCh.Add((WORD)nChIndex);
		// 			
		// 			//작업 정보 입력전에 검사 하여야 한다. (중복되 폴더가 있으면 삭제 되므로 )
		// 			//이전 시험이 data 손실 구간 정보를 Update한다.
		// 			//CCyclerChannel::IsDataLoss() 호출 이전에 최종 정보로 Update 하기 위해 
		// 			pMD->UpdateDataLossState(&wOneCh);	//data량이 많을시 Loading 시간 측정 필요
		// 			//pMD->UpdateDataSimpleLossState(&wOneCh);	//20080318 Data가 많을 경우 많은 시간이 소요되므로 간단하게 변경
		// 			
		// 			if(pChannel->IsDataLoss() == TRUE)
		// 			{
		// 				strLogMsg.Format("%s의 Channel %d 에서 시행한 이전 시험 [%s]에 data 손실 구간이 존재합니다.", pDoc->GetModuleName(nModuleID), nChIndex+1, pChannel->GetTestName()); 
		// 				strLogMsg += "Data를 복구하기 위해서는 [도구]->[Data 복구...]를 시행하기기 바랍니다.";
		// 				strLogMsg += "복구하지 않고 새로운 시험을 시작하게 되면 이전 손실된 data는 복구할 수 없게됩니다.\n";
		// 				strLogMsg += "새로운 작업을 시작 하시겠습니까?";
		// 				int nUserSel = MessageBox(strLogMsg, "Data 손실", MB_ICONQUESTION|MB_YESNOCANCEL);
		// 				if(nUserSel == IDCANCEL)	
		// 				{
		// 					return;		//Run 명령 취소 
		// 				}
		// 				else if(nUserSel == IDNO)
		// 				{
		// 					//시작 명령을 보내기 위한 선택 목록에서 제외 하여야 한다.
		// 					continue;	//Data를 복구함 
		// 				}
		// 			}
		// 		}

		//잠시 멈춤 Channel
		if(pChannel->GetState() == PS_STATE_PAUSE)
		{
			adwPauseCh.Add(MAKELONG(nChIndex, nModuleID));
			TRACE("Pause Module %d ch %d\n", nModuleID, nChIndex);
		}

		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);
		strLotNo = pChannel->GetTestSerial();
	}

	//만약 Pause인 채널이 있으면 Stop명령을 보낸다.
	if(adwPauseCh.GetSize() > 0)
	{
		if(AfxMessageBox(Fun_FindMsg("OnSimpleTest_msg3","IDD_CTSMonPro_FORM"), MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
			//@ if(AfxMessageBox("선택한 채널의 이전 작업을 초기화 하고 새로운 작업을 설정 하시겠습니까?", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
		{
			return;
		}
		if(SendCmdToModule(adwPauseCh, SFT_CMD_STOP) == FALSE)	
		{
			AfxMessageBox(Fun_FindMsg("OnSimpleTest_msg4","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox("작업 완료 처리 실패");
			return;
		}
	}
	//이전시험 정상 종료 여부 확인 끝
	//////////////////////////////////////////////////////////////////////////


	if(bDownAllRawDataBefRun) //lyj 20201023 간이충방전 시 이전 데이터 다운 
	{
		//ksj 20160728 이전 작업의 SBC StepEndData를 다운로드 한다.
		pDoc->m_nFtpDownStepEndModuleID = nModuleID;
		pDoc->m_nFtpDownStepEndChannelIndex = nChIndex;
		//yulee 20190420 이전 작업의 SBC Data를 다운로드 한다.
		//pDoc->m_bRunFtpDownStepEnd = TRUE;
		pDoc->m_bRunFtpDownAllSBCData = TRUE;
		pDoc->m_FTPFileDownRetryCnt = 0;
		pDoc->m_bSucessFtpDownAllSBCData = FALSE;
		pDoc->m_bRunFtpDownStepEnd = TRUE;
		pDoc->m_ucFtpDownIndex = pDoc->m_ucFtpDownIndex | (1 << nChIndex) ; //lyj 20200527 다운로드 개선
	}

	((CMainFrame *)AfxGetMainWnd())->m_bBlockFlag = TRUE;

	//새롭게 시행할 공정을 선택한다.
	//////////////////////////////////////////////////////////////////////////
	// 	CSelectScheduleDlg	dlg(GetDocument());
	// 	//CStartDlg	dlg1(GetDocument(), bModuleCommnad, this);
	// 	CStartDlg	dlg1(GetDocument(), this);
	// 	dlg1.m_bEnableStepSkip = bStartStepOption;
	// 	dlg1.m_strLot = strLotNo;
	// 	CScheduleData	scheduleInfo;
	// 
	// 	Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
	// 
	// 	int nRtn = 0;
	// 	int nModelPK = 0;
	// 	int nTestPK = 0;
	// 	while(1)
	// 	{
	// 		dlg.m_nSelModelID = nModelPK;
	// 		dlg.m_nSelScheduleID = nTestPK;
	// 		
	// 		if(IDOK != dlg.DoModal())
	// 		{
	// 			break;
	// 		}
	// 		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
	// 
	// 		nModelPK = dlg.m_nSelModelID;		//선택 모델 PK
	// 		nTestPK = dlg.m_nSelScheduleID;		//선택 스케쥴 PK
	// 
	// 		//4. 선택한 공정의 조건을 DataBase에서 Load한다.
	// 		if(scheduleInfo.SetSchedule(pDoc->GetDataBaseName(), nModelPK, nTestPK) == FALSE)
	// 		{
	// 			strLogMsg.Format("공정조건 불러오기 실패!!!");
	// 			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
	// 			break;
	// 		}
	// 		
	// 		//5. 시험명 및 기타 정보 입력 받아 시험명으로 폴더를 생성한다. 
	// 		dlg1.SetInfoData(&scheduleInfo , &aPtrSelCh);
	// 		dlg1.DoModal();
	// 		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
	// 
	// 		if(dlg1.m_nReturn == IDCANCEL)
	// 		{
	// 			break;
	// 		}
	// 		else if(dlg1.m_nReturn == IDOK)
	// 		{
	// 			nRtn = 1;
	// 			break;
	// 		}
	// 		//else if(dlg1.m_nReturn == IDRETRY)
	// 	}
	// 
	// 	if(nRtn == 0)
	// 	{
	// 		return;
	// 	}
	// 	//////////////////////////////////////////////////////////////////////////
	// 
	// 	//20070831 KJH 스케줄의 Step 값을 확인한다.
	// 	if(GetDocument()->ScheduleValidityCheck(&scheduleInfo, &m_adwTargetChArray) < 0)
	// 	{
	// 		AfxMessageBox(GetDocument()->GetLastErrorString());
	// 		return;
	// 	}

	CSimpleTestDlg dlg2;
	if(dlg2.DoModal() != IDOK)
	{
		return;
	}

	//20070831 KJH 스케줄의 Step 값을 확인한다.
	if(GetDocument()->ScheduleValidityCheck(&dlg2.m_ScheduleInfo, &m_adwTargetChArray) < 0)
	{			
		AfxMessageBox(GetDocument()->GetLastErrorString());
		return;
	}

	CStep * pStep;
	pStep = NULL;
	if (dlg2.m_ScheduleInfo.GetStepSize() > 1)
	{
		pStep = dlg2.m_ScheduleInfo.GetStepData(1);

	}

	//////////////////////////////////////////////////////////////////////////
	// 		CProgressWnd ProgressWnd(AfxGetMainWnd(), "전송중...", TRUE);
	// 		progressWnd.GoModal();
	// 		progressWnd.SetRange(0, 100);
	// 		progressWnd.SetPos(0);

	CProgressWnd * pPogressWnd = new CProgressWnd;
	pPogressWnd->Create(AfxGetMainWnd(),Fun_FindMsg("OnSimpleTest_msg5","IDD_CTSMonPro_FORM"),TRUE);
	//@ pPogressWnd->Create(AfxGetMainWnd(),"전송중...",TRUE);
	pPogressWnd->SetRange(0,10000);
	pPogressWnd->SetPos(0);
	pPogressWnd->Hide();
	//////////////////////////////////////////////////////////////////////////

	if (pStep != NULL && pStep->m_fReportTime != 0)		//ljb 20101227
	{
		//////////////////////////////////////////////////////////////////////////
		//간이 충방전 이지만 데이터를 기록 한다.
		CStartDlg	dlg1(GetDocument(), this);
		dlg1.m_bEnableStepSkip = bStartStepOption;
		//dlg1.m_strLot = strLotNo;
		CScheduleData	scheduleInfo;

		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

		int nRtn = 0;
		int nModelPK = 0;
		int nTestPK = 0;
		while(1)
		{
			//5. 시험명 및 기타 정보 입력 받아 시험명으로 폴더를 생성한다. 
			dlg1.SetInfoData(&dlg2.m_ScheduleInfo , &aPtrSelCh);
			dlg1.DoModal();
			Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

			if(dlg1.m_nReturn == IDCANCEL)
			{
				break;
			}
			else if(dlg1.m_nReturn == IDOK)
			{
				nRtn = 1;
				pPogressWnd->Show();
				break;
			}
		}

		if(nRtn == 0)
		{
			delete pPogressWnd;
			return;
		}
		//////////////////////////////////////////////////////////////////////////

		//5. 입력한 작업명과 저장 위치 확인 
		// c:\\Test_Name 형태 
		strTestPath = dlg1.GetTestPath();
		ASSERT(!strTestPath.IsEmpty());
		strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);

		CDWordArray adwChArray;
		nSelCount = aPtrSelCh.GetSize();
		int tCh;
		for(tCh = 0; tCh <nSelCount ; tCh++)
		{
			pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

			if(pChannel == NULL)
			{
				TRACE(Fun_FindMsg("OnSimpleTest_msg6","IDD_CTSMonPro_FORM"));
				//@ TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
				continue;
			}
			nChIndex = pChannel->GetChannelIndex();
			nModuleID = pChannel->GetModuleID();
			pMD = pDoc->GetModuleInfo(nModuleID);

			//최종 상태 다시 Check
			// 			if(pDoc->CheckCmdState(nModuleID, nChIndex, SFT_CMD_RUN) == FALSE)			
			// 			{
			// 				TRACE("M%dCH%d는 작업을 시작할 수 없는 상태 입니다.\n", nModuleID, nChIndex+1);
			// 				strTemp.Format("M%dCH%d는 작업을 시작할 수 없는 상태 입니다.\n", nModuleID, nChIndex+1);
			// 				pDoc->WriteSysLog(strTemp);
			// 				continue;
			// 			}

			adwChArray.Add(MAKELONG(nChIndex, nModuleID));

			//6. 선택된 Channel을 시험준비 한다.
			//strLogMsg.Format(Fun_FindMsg("OnSimpleTest_msg7","IDD_CTSMonPro_FORM"), nChIndex+1, pDoc->GetModuleName(nModuleID));	//20151217 edit
			strLogMsg.Format(Fun_FindMsg("OnSimpleTest_msg7","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nModuleID), nChIndex+1);	//ksj 20200605
			//@ strLogMsg.Format("%s 의 채널 %d 결과 폴더 생성 중 ...", pDoc->GetModuleName(nModuleID), nChIndex+1);
			pPogressWnd->SetText(strLogMsg);
			//pPogressWnd->SetPos(int((float)tCh/(float)nSelCount*10.0f));

			//Cancel 처리
			if(pPogressWnd->PeekAndPump() == FALSE)
			{
				delete pPogressWnd;
				return;
			}

			//CStartDlg::OnOk()에서 test_name folder는 생성한다.
			//c:\\Test_Name\\M01C01 형태
			int nStartChNo = GetStartChNo(nModuleID);
			strContent.Format("%s\\M%02dCh%02d[%03d]", strTestPath, nModuleID, nChIndex+1, nStartChNo+nChIndex);

			//6.1 Channel 폴더를 생성시킨다.
			if( ::CreateDirectory(strContent, NULL) == FALSE )
			{
				strLogMsg.Format(Fun_FindMsg("OnSimpleTest_msg8","IDD_CTSMonPro_FORM"), strContent);
				//@ strLogMsg.Format("Data 저장 폴더 생성 실패(%s)", strContent);
				AfxMessageBox(strLogMsg);
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				delete pPogressWnd;
				return;
			}
			else
			{
				//Step start 저장 폴더 생성 2006/04/03 Step초기 0.1초 data 저장 폴더 
				strLogMsg.Format("%s\\StepStart", strContent);

				//6.1.1 Step start 폴더를 생성시킨다.
				if( ::CreateDirectory(strLogMsg, NULL) == FALSE )
				{
					strContent.Format(Fun_FindMsg("OnSimpleTest_msg9","IDD_CTSMonPro_FORM"), strLogMsg);
					//@ strContent.Format("Step start 저장용 폴더 생성 실패(%s)", strLogMsg);
					pDoc->WriteSysLog(strContent, CT_LOG_LEVEL_NORMAL);
				}

				strLogMsg.Format(Fun_FindMsg("OnSimpleTest_msg10","IDD_CTSMonPro_FORM"), strContent);
				//@ strLogMsg.Format("Data 저장 폴더 생성 성공(%s)", strContent);
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_SYSTEM);
			}

			//6.2 각 폴더에 스케줄 파일을 생성한다.
			// c:\\Test_Name\M01C01\Test_Name.sch
			strScheduleFileName = strContent + "\\" + strTestName+ ".sch";
			if(FALSE == scheduleInfo.SaveToFile(strScheduleFileName,&dlg2.m_ScheduleInfo))
			{
				strLogMsg.Format(Fun_FindMsg("OnSimpleTest_msg11","IDD_CTSMonPro_FORM"), strTestName);
				//@ strLogMsg.Format("%s Schedule 파일 생성 실패", strTestName);
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				continue;
			}

			pChannel->ResetData();
			//6.3 시험명 설정
			pChannel->SetFilePath(strContent);			
			//Set Test Lot No
			pChannel->SetTestSerial(dlg1.m_strLot, dlg1.m_strWorker, dlg1.m_strComment);
			pChannel->LoadSaveItemSet();

			//6.4 Aux Title 파일 생성
			pChannel->SetAuxTitleFile(strContent, strTestName);

			//6.5 Can Title 파일 생성
			pChannel->SetCanTitleFile(strContent, strTestName);
			Sleep(10);	//모듈 상태 전이를 위한 시간 지연 
		}

		//////////////////////////////////////////////////////////////
		// 7. SBC에 Command를 전송한다.
		//		1) 시험 조건을 모듈로 전송한다.
		//		2) 선택 채널에 시작 명령을 전송한다.
		//		3) 작업 정보를 임시파일(.\\Temp\\M01C01.tmp)에 저장한다. (Pause에서 연속 작업이 가능하도록...)

		//Start Cycle, Step 번호 지정 
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

		if(pDoc->SendStartCmd(pPogressWnd, &adwChArray, &dlg2.m_ScheduleInfo, dlg1.m_nStartCycle, dlg1.m_nStartStep,dlg1.m_nStartOptChamber) == FALSE)
		{
			AfxMessageBox(strTestName + Fun_FindMsg("OnSimpleTest_msg12","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox(strTestName + " 작업 시작을 실패 하였습니다.");
		}
		else
		{
			//////////////////////////////////////////////////yulee 20181202
			//TRACE("Selected Ch %d\n", pSelChArray->GetSize());
			int i;
			for(int i = 0; i< aPtrSelCh.GetSize(); i++)
			{
				CCyclerChannel *pCh = (CCyclerChannel *)aPtrSelCh.GetAt(i);			

				CString RegName;
				RegName.Format("OnProgressSimpleTest_%d", pCh->m_iCh);

				AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, RegName, 1);
			}
			//////////////////////////////////////////////////
			//AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "OnProgressSimpleTest", 1);
			strTemp.Format(Fun_FindMsg("OnSimpleTest_msg13","IDD_CTSMonPro_FORM"));
			//@ strTemp.Format("스케쥴 전송 완료");
			pPogressWnd->SetText(strTemp);
			pPogressWnd->SetPos(100);
		}

		Sleep(200);
		delete pPogressWnd;
		return;
	}
	else
	{
		//5. 입력한 작업명과 저장 위치 확인 
		strTestPath.Empty();
		strTestName.Empty();

		//////////////////////////////////////////////////////////////////////////
		// 		CProgressWnd progressWnd(AfxGetMainWnd(), "전송중...", TRUE);
		// 		progressWnd.GoModal();
		// 		progressWnd.SetRange(0, 100);
		// 		progressWnd.SetPos(0);
		//////////////////////////////////////////////////////////////////////////

		CDWordArray adwChArray;
		nSelCount = aPtrSelCh.GetSize();
		int tCh;
		for(tCh = 0; tCh <nSelCount ; tCh++)
		{
			pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

			if(pChannel == NULL)
			{
				TRACE(Fun_FindMsg("OnSimpleTest_msg14","IDD_CTSMonPro_FORM"));
				//@ TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
				continue;
			}
			nChIndex = pChannel->GetChannelIndex();
			nModuleID = pChannel->GetModuleID();
			pMD = pDoc->GetModuleInfo(nModuleID);

			//최종 상태 다시 Check
			if(pDoc->CheckCmdState(nModuleID, nChIndex, SFT_CMD_RUN) == FALSE)			
			{
				TRACE(Fun_FindMsg("OnSimpleTest_msg15","IDD_CTSMonPro_FORM"), nModuleID, nChIndex+1);
				//@ TRACE("M%dCH%d는 작업을 시작할 수 없는 상태 입니다.\n", nModuleID, nChIndex+1);
				strTemp.Format(Fun_FindMsg("OnSimpleTest_msg16","IDD_CTSMonPro_FORM"), nModuleID, nChIndex+1);
				//@ strTemp.Format("M%dCH%d는 작업을 시작할 수 없는 상태 입니다.\n", nModuleID, nChIndex+1);
				pDoc->WriteSysLog(strTemp);
				continue;
			}

			adwChArray.Add(MAKELONG(nChIndex, nModuleID));

			//6. 선택된 Channel을 시험준비 한다.
			//			pPogressWnd->SetPos(0);
			// 			strLogMsg.Format("%s 의 채널 %d에 시작 명령 전송...", pDoc->GetModuleName(nModuleID), nChIndex+1);
			// 			pPogressWnd->SetText(strLogMsg);
			// 			pPogressWnd->SetPos(int((float)tCh/(float)nSelCount*100.0f));

			//Cancel 처리
			if(pPogressWnd->PeekAndPump() == FALSE)
			{
				delete pPogressWnd;
				return;
			}
			pChannel->ResetData();

			Sleep(10);	//모듈 상태 전이를 위한 시간 지연 
		}

		//////////////////////////////////////////////////////////////
		// 7. SBC에 Command를 전송한다.
		//		1) 시험 조건을 모듈로 전송한다.
		//		2) 선택 채널에 시작 명령을 전송한다.
		//		3) 작업 정보를 임시파일(.\\Temp\\M01C01.tmp)에 저장한다. (Pause에서 연속 작업이 가능하도록...)

		//Start Cycle, Step 번호 지정 
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 

		//2014.12.10 간이 충방전 모드
		pDoc->m_bSimpleTest = TRUE;
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Mux Link Option", 0);	//ljb 20151217 add 김재호 요청

		pPogressWnd->Show();
		if(pDoc->SendStartCmd(pPogressWnd, &adwChArray, &dlg2.m_ScheduleInfo) == FALSE)
		{
			AfxMessageBox(strTestName + Fun_FindMsg("OnSimpleTest_msg17","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox(strTestName + " 작업 시작을 실패 하였습니다.");
		}
		else
		{

			//////////////////////////////////////////////////yulee 20181202
			//TRACE("Selected Ch %d\n", pSelChArray->GetSize());

			for(int i=0; i< aPtrSelCh.GetSize(); i++)
			{
				CCyclerChannel *pCh = (CCyclerChannel *)aPtrSelCh.GetAt(i);

				CString RegName;
				RegName.Format("OnProgressSimpleTest_%d", pCh->m_iCh);

				AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, RegName, 1);
			}
			//////////////////////////////////////////////////	
			//AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "OnProgressSimpleTest", 1);
			strTemp.Format(Fun_FindMsg("OnSimpleTest_msg18","IDD_CTSMonPro_FORM"));
			//@ strTemp.Format("스케쥴 전송 완료");
			pPogressWnd->SetText(strTemp);
			pPogressWnd->SetPos(100);
		}

		//2014.12.10 간이 충방전 모드 해체
		pDoc->m_bSimpleTest = FALSE;

		Sleep(200);
		delete pPogressWnd;
	}

}

void CCTSMonProView::OnUpdateSimpleTest(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
// 		CCyclerModule *pMD;
// 		int nModuleID;
// 		BOOL bAble = FALSE;
// 		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 		while(pos)
// 		{
// 			int item = m_wndModuleList.GetNextSelectedItem(pos);
// 			nModuleID  = m_wndModuleList.GetItemData(item);		
// 			pMD = GetDocument()->GetModuleInfo(nModuleID);
// 			if(pMD)
// 			{
// 				if(pMD->GetState() == PS_STATE_LINE_OFF)
// 				{
// 					bAble = FALSE;
// 					
// 					break;
// 				}
// 				else
// 				{
// 					bAble = TRUE;
// 				}
// 			}
// 			else 
// 			{
// 				bAble = FALSE;
// 				
// 				break;
// 			}
// 		}
		pCmdUI->Enable(FALSE);	
	}
	else
	{
#ifdef _DEBUG
		pCmdUI->Enable(TRUE);
#else
		pCmdUI->Enable(UpdateUIChList(SFT_CMD_RUN, FALSE));		
#endif
	}
}

void CCTSMonProView::OnUpdateParallelConfig(CCmdUI* pCmdUI) 
{
	if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Max Parallel Count", 0))
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
// #ifdef _DEBUG
// 	pCmdUI->Enable(TRUE);
// #else
// 	pCmdUI->Enable(FALSE);
// #endif

	
}

void CCTSMonProView::OnParallelConfig() 
{
//	CLoginManagerDlg LoginDlg;
//	if(LoginDlg.DoModal() != IDOK) return;

// 	CParallelConfigDlg dlg(GetDocument());	
// 	dlg.DoModal();		

	//ksj 20200122 : ch attribute 수신시 dialog 에 통지 할 수 있도록 모달리스로 창 생성하고, 포인터 저장.
	//CParallelConfigDlg dlg(GetDocument());	
	
	if(m_pParallelConfigDlg)
	{
		delete m_pParallelConfigDlg;
		m_pParallelConfigDlg = NULL;
	}


/*m_pParallelConfigDlg = new CParallelConfigDlg(GetDocument());
	m_pParallelConfigDlg->Create(IDD_PARALLEL_DLG,this); //다국어화 필요
	m_pParallelConfigDlg->CenterWindow();
	m_pParallelConfigDlg->ShowWindow(SW_SHOW);*/

	//ksj 20200825 : 병렬 설정창 다국어화
	int nLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	UINT uiTemplate = IDD_PARALLEL_DLG;
	switch(nLanguage)
	{
	case 1:
		uiTemplate = IDD_PARALLEL_DLG;
		break;
	case 2:
		uiTemplate = IDD_PARALLEL_DLG_ENG;
		break;
	case 3:
		uiTemplate = IDD_PARALLEL_DLG_PL;
		break;
	}

	m_pParallelConfigDlg = new CParallelConfigDlg(GetDocument());
	m_pParallelConfigDlg->Create(uiTemplate,this); //다국어화 필요
	m_pParallelConfigDlg->CenterWindow();
	m_pParallelConfigDlg->ShowWindow(SW_SHOW);
	//ksj end
	
}

void CCTSMonProView::OnAuxDataConfig() 
{
	// 	CLoginManagerDlg LoginDlg;
	// 	if(LoginDlg.DoModal() != IDOK) return;
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnAuxDataConfig_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	//////////////////////////////////////////////////////////////////////////
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnAuxDataConfig_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("1 채널만 선택 하세요. ");
		return;
	}
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		//		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	return;
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			AfxMessageBox(Fun_FindMsg("OnAuxDataConfig_msg3","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox("접속 중인 모듈이 아닙니다. ");
#ifndef _DEBUG
			return;
#endif
		}
	}
	////////////////////////////////////////////////////////////////////////
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	if(pChInfo != NULL)
	{
		if(	pChInfo->GetState() != PS_STATE_IDLE && 
			pChInfo->GetState() != PS_STATE_STANDBY &&
			pChInfo->GetState() != PS_STATE_READY &&
			pChInfo->GetState() != PS_STATE_END
			)
		{
			AfxMessageBox(Fun_FindMsg("OnAuxDataConfig_msg4","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox("운영중인 채널은 변경 할 수 없습니다.");
#ifndef _DEBUG
			return;
#endif
		}
	}
	
	CAuxDataDlg dlg(GetDocument());	
	if (dlg.DoModal() == IDOK)
	{
		GetDocument()->Fun_GetArryAuxDivision(m_uiArryAuxDivition);	//ljb 2011222 이재복 //////////
		GetDocument()->Fun_GetArryAuxStringName(m_strArryAuxName);	//ljb 2011222 이재복 //////////
		m_wndAuxList.DeleteAllItems();
		//		UpdateAuxListData();
	}
}

void CCTSMonProView::OnUpdateAuxDataConfig(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
	//pCmdUI->Enable(FALSE);
	
}

void CCTSMonProView::InitAuxList()
{
 	//GetDocument()->Fun_GetArryAuxDivision(m_uiArryAuxDivition);	//ljb 2011222 이재복 //////////
 	//GetDocument()->Fun_GetArryAuxStringName(m_strArryAuxName);	//ljb 2011222 이재복 //////////

	CRect rect;
	m_wndAuxList.GetClientRect(&rect);

	CImageList m_image;

	m_image.Create(0,19,ILC_COLORDDB,1,0); 

	m_wndAuxList.SetImageList(&m_image,LVSIL_SMALL); 

	DWORD style = 	m_wndAuxList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;

	m_wndAuxList.SetExtendedStyle(style); 
	m_wndAuxList.SetFont(&m_lfChListFont);
	
	//ksj 20200117 : 주석처리
	/*if(m_bUseCan == FALSE)
	{
		m_wndAuxList.InsertColumn(1, _T("No"), LVCFMT_CENTER, 50);
		m_wndAuxList.InsertColumn(2, _T(Fun_FindMsg("InitAuxList_msg1","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);
		//@ m_wndAuxList.InsertColumn(2, _T("이름"), LVCFMT_CENTER, 200);
		m_wndAuxList.InsertColumn(3, _T(Fun_FindMsg("InitAuxList_msg2","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 120);
		//@ m_wndAuxList.InsertColumn(3, _T("값"), LVCFMT_CENTER, 120);
		m_wndAuxList.InsertColumn(4, _T(Fun_FindMsg("InitAuxList_msg3","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(4, _T("안전상한값"), LVCFMT_CENTER, 90);
		m_wndAuxList.InsertColumn(5, _T(Fun_FindMsg("InitAuxList_msg4","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(5, _T("안전하한값"), LVCFMT_CENTER, 90);
		m_wndAuxList.InsertColumn(6, _T(Fun_FindMsg("InitAuxList_msg5","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(6, _T("종료상한값"), LVCFMT_CENTER, 90);
		m_wndAuxList.InsertColumn(7, _T(Fun_FindMsg("InitAuxList_msg6","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(7, _T("종료하한값"), LVCFMT_CENTER, 90);
	}
	else
	{
		m_wndAuxList.InsertColumn(1, _T("No"), LVCFMT_CENTER, 50);
		m_wndAuxList.InsertColumn(2, _T(Fun_FindMsg("InitAuxList_msg7","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 108);
		//@ m_wndAuxList.InsertColumn(2, _T("이름"), LVCFMT_CENTER, 108);
		m_wndAuxList.InsertColumn(3, _T(Fun_FindMsg("InitAuxList_msg8","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 80);
		//@ m_wndAuxList.InsertColumn(3, _T("값"), LVCFMT_CENTER, 80);
		m_wndAuxList.InsertColumn(4, _T(Fun_FindMsg("InitAuxList_msg9","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
		//@ m_wndAuxList.InsertColumn(4, _T("안전상한값"), LVCFMT_CENTER, 60);
		m_wndAuxList.InsertColumn(5, _T(Fun_FindMsg("InitAuxList_msg10","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
		//@ m_wndAuxList.InsertColumn(5, _T("안전하한값"), LVCFMT_CENTER, 60);
		m_wndAuxList.InsertColumn(6, _T(Fun_FindMsg("InitAuxList_msg11","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
		//@ m_wndAuxList.InsertColumn(6, _T("종료상한값"), LVCFMT_CENTER, 60);
		m_wndAuxList.InsertColumn(7, _T(Fun_FindMsg("InitAuxList_msg12","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
		//@ m_wndAuxList.InsertColumn(7, _T("종료하한값"), LVCFMT_CENTER, 60);
	}
	m_wndAuxList.InsertColumn(8, _T(Fun_FindMsg("InitAuxList_msg13","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//ljb 20101004
	//@ m_wndAuxList.InsertColumn(8, _T("분류 1"), LVCFMT_CENTER, 200);		//ljb 20101004
	m_wndAuxList.InsertColumn(9, _T(Fun_FindMsg("InitAuxList_msg14","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//ljb 20101004
	//@ m_wndAuxList.InsertColumn(9, _T("분류 2"), LVCFMT_CENTER, 200);		//ljb 20101004
	m_wndAuxList.InsertColumn(10, _T(Fun_FindMsg("InitAuxList_msg15","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//ljb 20101004
	//@ m_wndAuxList.InsertColumn(10, _T("분류 3"), LVCFMT_CENTER, 200);		//ljb 20101004
	//$1013
	//m_wndAuxList.InsertColumn(11, _T("Thermistor Type"), LVCFMT_CENTER, 200);	//ljb 20160504 add
	m_wndAuxList.InsertColumn(11, Fun_FindMsg("InitAuxList_msg16","IDD_CTSMonPro_FORM"), LVCFMT_CENTER, 200);	//ljb 20160504 add //&&
	//m_wndAuxList.InsertColumn(12, _T("CellBalance State"), LVCFMT_CENTER, 200);	//cny 181023 add
	m_wndAuxList.InsertColumn(12, Fun_FindMsg("InitAuxList_msg17","IDD_CTSMonPro_FORM"), LVCFMT_CENTER, 200);	//cny 181023 add //&&
	*/
	//m_wndAuxList.InsertColumn(4, _T("채널"), LVCFMT_CENTER, 100);
	//m_wndAuxList.GetHeaderCtrl()->EnableWindow(FALSE);


	//ksj 20200117 : 신규 aux vent 안전조건 출력.
	if(m_bUseCan == FALSE)
	{
		m_wndAuxList.InsertColumn(1, _T("No"), LVCFMT_CENTER, 40);
		m_wndAuxList.InsertColumn(2, _T(Fun_FindMsg("InitAuxList_msg1","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 108);
		//@ m_wndAuxList.InsertColumn(2, _T("이름"), LVCFMT_CENTER, 200);
		m_wndAuxList.InsertColumn(3, _T(Fun_FindMsg("InitAuxList_msg2","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 70);
		//@ m_wndAuxList.InsertColumn(3, _T("값"), LVCFMT_CENTER, 120);

		//ksj 20200117 : 고정 안전 상한/하한 값 표시 (vent)
		if(m_bUseVentSafety)
		{
		//m_wndAuxList.InsertColumn(4, "VENT상한", LVCFMT_CENTER, 68);
			m_wndAuxList.InsertColumn(4, _T(Fun_FindMsg("InitAuxList_msg18","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 75); //lyj 20200716
		//m_wndAuxList.InsertColumn(5, "VENT하한", LVCFMT_CENTER, 68);
			m_wndAuxList.InsertColumn(5, _T(Fun_FindMsg("InitAuxList_msg19","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 75); //lyj 20200716
		}
		else
		{
			//m_wndAuxList.InsertColumn(4, "VENT상한", LVCFMT_CENTER, 0);
			m_wndAuxList.InsertColumn(4, _T(Fun_FindMsg("InitAuxList_msg18","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 0); //lyj 20200716
			//m_wndAuxList.InsertColumn(5, "VENT하한", LVCFMT_CENTER, 0);
			m_wndAuxList.InsertColumn(5, _T(Fun_FindMsg("InitAuxList_msg19","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 0); //lyj 20200716
		}		
		//ksj end

		m_wndAuxList.InsertColumn(6, _T(Fun_FindMsg("InitAuxList_msg3","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(4, _T("안전상한값"), LVCFMT_CENTER, 90);
		m_wndAuxList.InsertColumn(7, _T(Fun_FindMsg("InitAuxList_msg4","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(5, _T("안전하한값"), LVCFMT_CENTER, 90);

		//ksj 20200120 : 안전상한 걸릴시 vent 사용 여부
		if(m_bUseVentSafety)
		{

			//m_wndAuxList.InsertColumn(8, "VENT사용", LVCFMT_CENTER, 50);
			m_wndAuxList.InsertColumn(8, _T(Fun_FindMsg("InitAuxList_msg20","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 50); //lyj 20200716
		}
		else
		{

			//m_wndAuxList.InsertColumn(8, "VENT사용", LVCFMT_CENTER, 0);
			m_wndAuxList.InsertColumn(8, _T(Fun_FindMsg("InitAuxList_msg20","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 0); //lyj 20200716
		}
		//ksj end

		m_wndAuxList.InsertColumn(9, _T(Fun_FindMsg("InitAuxList_msg5","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(6, _T("종료상한값"), LVCFMT_CENTER, 90);
		m_wndAuxList.InsertColumn(10, _T(Fun_FindMsg("InitAuxList_msg6","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 95);
		//@ m_wndAuxList.InsertColumn(7, _T("종료하한값"), LVCFMT_CENTER, 90);
	}
	else
	{
		m_wndAuxList.InsertColumn(1, _T("No"), LVCFMT_CENTER, 40);
		m_wndAuxList.InsertColumn(2, _T(Fun_FindMsg("InitAuxList_msg7","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 108);
		//@ m_wndAuxList.InsertColumn(2, _T("이름"), LVCFMT_CENTER, 108);
		m_wndAuxList.InsertColumn(3, _T(Fun_FindMsg("InitAuxList_msg8","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 80);

		//ksj 20200117 : 고정 안전 상한/하한 값 표시 (vent)
		if(m_bUseVentSafety)
		{
			//m_wndAuxList.InsertColumn(4, "VENT상한", LVCFMT_CENTER, 68);
			m_wndAuxList.InsertColumn(4, _T(Fun_FindMsg("InitAuxList_msg18","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 75); //lyj 20200716
			//m_wndAuxList.InsertColumn(5, "VENT하한", LVCFMT_CENTER, 68);
			m_wndAuxList.InsertColumn(5, _T(Fun_FindMsg("InitAuxList_msg19","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 75); //lyj 20200716
		}
		else
		{
			//m_wndAuxList.InsertColumn(4, "VENT상한", LVCFMT_CENTER, 0);
			m_wndAuxList.InsertColumn(4, _T(Fun_FindMsg("InitAuxList_msg18","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 0); //lyj 20200716
			//m_wndAuxList.InsertColumn(5, "VENT하한", LVCFMT_CENTER, 0);
			m_wndAuxList.InsertColumn(5, _T(Fun_FindMsg("InitAuxList_msg19","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 0); //lyj 20200716
		}	
		//ksj end

		//@ m_wndAuxList.InsertColumn(3, _T("값"), LVCFMT_CENTER, 80);
		m_wndAuxList.InsertColumn(6, _T(Fun_FindMsg("InitAuxList_msg9","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
	
		//@ m_wndAuxList.InsertColumn(4, _T("안전상한값"), LVCFMT_CENTER, 60);
		m_wndAuxList.InsertColumn(7, _T(Fun_FindMsg("InitAuxList_msg10","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
		//@ m_wndAuxList.InsertColumn(5, _T("안전하한값"), LVCFMT_CENTER, 60);

		//ksj 20200120 : 안전상한 걸릴시 vent 사용 여부
		if(m_bUseVentSafety)
		{
			//m_wndAuxList.InsertColumn(8, "VENT사용", LVCFMT_CENTER, 50);
			m_wndAuxList.InsertColumn(8, _T(Fun_FindMsg("InitAuxList_msg20","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 50); //lyj 20200716
		}
		else
		{
			//m_wndAuxList.InsertColumn(8, "VENT사용", LVCFMT_CENTER, 0);
			m_wndAuxList.InsertColumn(8, _T(Fun_FindMsg("InitAuxList_msg20","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 0); //lyj 20200716
		}
		//ksj end

		m_wndAuxList.InsertColumn(9, _T(Fun_FindMsg("InitAuxList_msg11","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
		//@ m_wndAuxList.InsertColumn(6, _T("종료상한값"), LVCFMT_CENTER, 60);
		m_wndAuxList.InsertColumn(10, _T(Fun_FindMsg("InitAuxList_msg12","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 65);
		//@ m_wndAuxList.InsertColumn(7, _T("종료하한값"), LVCFMT_CENTER, 60);
	}
	m_wndAuxList.InsertColumn(11, _T(Fun_FindMsg("InitAuxList_msg13","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//ljb 20101004
	//@ m_wndAuxList.InsertColumn(8, _T("분류 1"), LVCFMT_CENTER, 200);		//ljb 20101004
	m_wndAuxList.InsertColumn(12, _T(Fun_FindMsg("InitAuxList_msg14","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//ljb 20101004
	//@ m_wndAuxList.InsertColumn(9, _T("분류 2"), LVCFMT_CENTER, 200);		//ljb 20101004
	m_wndAuxList.InsertColumn(13, _T(Fun_FindMsg("InitAuxList_msg15","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//ljb 20101004
	//@ m_wndAuxList.InsertColumn(10, _T("분류 3"), LVCFMT_CENTER, 200);		//ljb 20101004
	//$1013
	//m_wndAuxList.InsertColumn(11, _T("Thermistor Type"), LVCFMT_CENTER, 200);	//ljb 20160504 add
	m_wndAuxList.InsertColumn(14, Fun_FindMsg("InitAuxList_msg16","IDD_CTSMonPro_FORM"), LVCFMT_CENTER, 200);	//ljb 20160504 add //&&
	//m_wndAuxList.InsertColumn(12, _T("CellBalance State"), LVCFMT_CENTER, 200);	//cny 181023 add
	m_wndAuxList.InsertColumn(15, Fun_FindMsg("InitAuxList_msg17","IDD_CTSMonPro_FORM"), LVCFMT_CENTER, 200);	//cny 181023 add //&&
}

void CCTSMonProView::InitChInfoList()
{
	CRect rect;
	m_wndChInfoList.GetClientRect(&rect);

	int nColumnWidth = 95;
	
	CImageList m_image;

	m_image.Create(0,19,ILC_COLORDDB,1,0); 

	m_wndChInfoList.SetImageList(&m_image,LVSIL_SMALL); 


	DWORD style = 	m_wndChInfoList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;

	m_wndChInfoList.SetExtendedStyle(style); 
	m_wndChInfoList.SetFont(&m_lfChListFont);
	
	m_wndChInfoList.InsertColumn(1, _T(Fun_FindMsg("InitChInfo_msg1","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, nColumnWidth);
	//@ m_wndChInfoList.InsertColumn(1, _T("항목"), LVCFMT_CENTER, nColumnWidth);
	m_wndChInfoList.InsertColumn(2, _T(Fun_FindMsg("InitChInfo_msg2","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, rect.Width() - nColumnWidth-17);
	//@ m_wndChInfoList.InsertColumn(2, _T("값"), LVCFMT_CENTER, rect.Width() - nColumnWidth-17);

	//+2015.9.22 USY Add 
	for(UINT nTmp = 0; nTmp <PS_MAX_ITEM_NUM; nTmp++ )
	{
		arrayChInfo[nTmp] = -1;
	}
	//-
	CString strTemp;
					
	arrayChInfo[0] = PS_STEP_NO ;		
	arrayChInfo[1] = PS_STEP_TIME ;		
	arrayChInfo[2] = PS_TOT_TIME ;		
	arrayChInfo[3] = PS_STATE;			
	arrayChInfo[4] = PS_VOLTAGE;		
	arrayChInfo[5] = PS_CURRENT;		
	arrayChInfo[6] = PS_CAPACITY;
	arrayChInfo[7] = PS_CHARGE_CAP;
	arrayChInfo[8] = PS_DISCHARGE_CAP;
	arrayChInfo[9] = PS_CAPACITANCE;
	arrayChInfo[10] = PS_IMPEDANCE;		
	arrayChInfo[11] = PS_WATT;			
	arrayChInfo[12] = PS_WATT_HOUR;		
	arrayChInfo[13] = PS_CHARGE_WH;		
	arrayChInfo[14] = PS_DISCHARGE_WH;		
	arrayChInfo[15] = PS_CUR_CYCLE;		
	arrayChInfo[16] = PS_TOT_CYCLE;		
	arrayChInfo[17] = PS_ACC_CYCLE1;				//ljb v1009
	arrayChInfo[18] = PS_TEST_NAME;		
	arrayChInfo[19] = PS_SCHEDULE_NAME; 
	arrayChInfo[20] = PS_LOT_NO;
	arrayChInfo[21] = PS_AVG_VOLTAGE;
	arrayChInfo[22] = PS_AVG_CURRENT;	
	arrayChInfo[23] = PS_CHANNEL_NO;	
	arrayChInfo[24] = PS_MODULE_NO;	
	arrayChInfo[25] = PS_CODE;
	arrayChInfo[26] = PS_GRADE_CODE;
	arrayChInfo[27] = PS_STEP_TYPE;
	arrayChInfo[28] = PS_DATA_SEQ;		
	arrayChInfo[29] = PS_CV_TIME;		
	arrayChInfo[30] = PS_SYNC_DATE;
	arrayChInfo[31] = PS_AUX_COMM_TEMP;		//ljb v100A
	arrayChInfo[32] = PS_AUX_COMM_VOLT;		//ljb v100A
	arrayChInfo[33] = PS_CAN_COMM_MASTER;	//ljb v100A
	arrayChInfo[34] = PS_CAN_COMM_SLAVE;	//ljb v100A
	
	arrayChInfo[35] = PS_VOLTAGE_INPUT;	//ljb v100A
	arrayChInfo[36] = PS_VOLTAGE_POWER;	//ljb v100A
	arrayChInfo[37] = PS_VOLTAGE_BUS;	//ljb v100A
	if (m_bUseCbank)
	{
		arrayChInfo[38] = PS_CBANK;	//ljb v100A
	}
	
//	arrayChInfo[23] = PS_CHARGE_CAP;	
//	arrayChInfo[24] = PS_DISCHARGE_CAP;		
//	arrayChInfo[25] = PS_CAPACITY_SUM;		
//	arrayChInfo[20] = PS_START_TIME;	
//	arrayChInfo[21] = PS_END_TIME;	
//	arrayChInfo[26] = PS_TEMPERATURE;
//	arrayChInfo[31] = PS_AUX_VOLTAGE;		
//	arrayChInfo[33] = PS_METER_DATA;	

	int nSelect;
	for( nSelect = 0; nSelect < PS_MAX_ITEM_NUM-2; nSelect++ )
	{
		strTemp.Format("Item%02d", arrayChInfo[nSelect]+1);
		strTemp = AfxGetApp()->GetProfileString("Settings", strTemp);

		//ksj 20201223 : 버그 수정 임시코드, 
		//레지스트리에서 Item40을 바꿔도 사용자 설정에서 또 이상한 값으로 덮어씌움. 사용자 설정쪽 수정해야되지만 임시로 강제 수정.
		if(arrayChInfo[nSelect]+1 == 40)
			strTemp.Format("Sync Date");
		//ksj end
		if(!strTemp.IsEmpty())
		{			
			m_wndChInfoList.InsertItem(nSelect, strTemp);
		}
	}

	//m_wndChInfoList.GetHeaderCtrl()->EnableWindow(FALSE);
}

//현재 선택된 Ch에 대한 Monitoring Aux Data를 리스트에 보여준다.
void CCTSMonProView::UpdateAuxListData()
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pMD;
	CCyclerChannel *lpChannelInfo;
		
	float fMinVolt=0, fMaxVolt=0;
	float fMinTemp=0, fMaxTemp=0;
	float fMinTher=0, fMaxTher=0;
	float fMinTherHumi=0, fMaxTherHumi=0; //ksj 20200116 : V1016

	UINT nModuleID=0,nChannelID=0;
	
	UINT mdID;
	if (m_nCurrentModuleID == 0)
	{
		nChannelID = m_nCurrentChannelNo;
		int nTotMDCnt = pDoc->GetInstallModuleCount();
		for(int s =0; s<nTotMDCnt; s++)
		{
			nModuleID = s+1;
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				if (pMD->GetTotalChannel() < nChannelID + 1)
				{
					nChannelID = nChannelID - pMD->GetTotalChannel();
				}
				else
				{
					break;
				}
			}
		}
	}
	else
	{
		nModuleID = m_nCurrentModuleID;
		nChannelID = m_nCurrentChannelNo;
	}
	
	CString strItem,strTemp;
	CCyclerModule *lpModule = GetDocument()->GetModuleInfo(nModuleID);
	if(lpModule == NULL)	return;

	//현재 채널 정보
	lpChannelInfo =  lpModule->GetChannelInfo(nChannelID);
	if(lpChannelInfo == NULL)	return;

	m_ctrlLal_CellDeltaVolt = lpChannelInfo->m_strCellDeltaVolt;	//ljb 20150806
	UpdateData(FALSE);

	//ljb 201137 이재복 //////////
	if (lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE) return;

	//int nAuxListCount = lpChannelInfo->GetAuxCount();
	int nAuxListCount = m_wndAuxList.GetItemCount();
	int nAuxPos=0;

	SFT_AUX_DATA * pAuxData = lpChannelInfo->GetAuxData();
	if(pAuxData == NULL)
	{			
		//yulee 20180927 
		CString strInitValue;
		strInitValue.Format("0.0");
		m_LabAuxTempMax	= strInitValue;
		m_LabAuxTempMin	= strInitValue;
		m_LabAuxVoltMax	= strInitValue;
		m_LabAuxVoltMin	= strInitValue;
		m_LabAuxTherMax	= strInitValue;
		m_LabAuxTherMin	= strInitValue;
		m_LabAuxTherHumiMax = strInitValue; //ksj 20200120
		m_LabAuxTherHumiMin = strInitValue; //ksj 20200120
		return;
	}
	
	//TRACE("Aux Count = %d \n", lpChannelInfo->GetAuxCount());
	BOOL bAuxFirstTemp(TRUE), bAuxFirstVolt(TRUE), bAuxFirstTher(TRUE);
	BOOL bAuxFirstTherHumi(TRUE); //ksj 20200116 : V1016
	BOOL bCellBAL=FALSE;
	int nCellBal_No=0;

	TRACE("[[[[[[[[[[[[[[[[[[Ch%d, AuxCount : %d\n",lpChannelInfo->GetChannelIndex()+1,lpChannelInfo->GetAuxCount());

	CString strFormat;		
	strFormat.Format("%%.%df",m_nMaxAuxFloatingPoint); //ksj 20201112 : 소수점 아래 표시 개수 옵션 추가.

	for(int i = 0; i < lpChannelInfo->GetAuxCount(); i++)
	{
		CChannelSensor * pSensor;
		pSensor = lpModule->GetAuxData(pAuxData[i].auxChNo-1, pAuxData[i].auxChType);

		if(pSensor == NULL)
		{
			CString strMsg;
			strMsg.Format(Fun_FindMsg("UpdateAuxListData_msg","IDD_CTSMonPro_FORM"),
				//@ strMsg.Format("Error : 설정되지 않은 외부데이터에 접근(ChNo : %d, Aux Ch : %d, Aux Type : %d, Aux Count : %d)",
				m_nCurrentChannelNo+1, pAuxData[i].auxChNo, pAuxData[i].auxChType, lpChannelInfo->GetAuxCount());
			lpChannelInfo->WriteLog(strMsg);
			return;
		}

		if(nAuxListCount == 0)		//스크롤이 위로 올라가는 것을 방지하기 위해 List가 비어있을 경우만 추가한다.
		{
			switch(pAuxData[i].auxChType)
			{
			case 0: strItem.Format("T%d",pAuxData[i].auxChNo) ;	break;
			case 1: strItem.Format("V%d",pAuxData[i].auxChNo) ;	break;
			//case 2: strItem.Format("TH%d",pAuxData[i].auxChNo);	break;
			case 2: strItem.Format("TH%d",pAuxData[i].auxChNo - lpModule->GetMaxVoltage());	break;
			case 3: strItem.Format("H%d",pAuxData[i].auxChNo - lpModule->GetMaxVoltage() - lpModule->GetMaxTemperatureTh());	break; //ksj 20200120
			}
			m_wndAuxList.InsertItem(i, strItem);
			continue;
		}

		m_wndAuxList.SetItemText(i, 1, pSensor->GetAuxName());

		float fValue = GetDocument()->UnitTrans( pAuxData[i].lValue, FALSE, pAuxData[i].auxChType);
		//strItem.Format("%0.3f", fValue);
		strItem.Format(strFormat, fValue); //ksj 20201112

			if(pAuxData[i].auxChType == PS_AUX_TYPE_VOLTAGE)	//ljb 20170829 add
			{
				//if (pAuxData[i].auxChNo < 51) //yulee 20180419
				//if (pAuxData[i].auxChNo < 96) //ksj 20200702 : 주석처리함. 97번 넘는 전압은 계산이 안됨.
				{
					if (bAuxFirstVolt) 
					{
						fMinVolt = fMaxVolt = fValue;
						bAuxFirstVolt = FALSE;
					}
					else
					{
						if (fMinVolt > fValue) fMinVolt = fValue;
						if (fMaxVolt < fValue) fMaxVolt = fValue;
					}
					//strTemp.Format("%0.3f", fMinVolt);
					strTemp.Format(strFormat, fMinVolt); //ksj 20201112
					m_LabAuxVoltMin = strTemp;
					//strTemp.Format("%0.3f", fMaxVolt);
					strTemp.Format(strFormat, fMaxVolt); //ksj 20201112
					m_LabAuxVoltMax = strTemp;
				}

			}
			else if(pAuxData[i].auxChType == PS_AUX_TYPE_TEMPERATURE)
			{
				if (bAuxFirstTemp) 
				{
					fMinTemp = fMaxTemp = fValue;
					bAuxFirstTemp = FALSE;
				}
				else
				{
					if (fMinTemp > fValue) fMinTemp = fValue;
					if (fMaxTemp < fValue) fMaxTemp = fValue;
				}
				//strTemp.Format("%0.3f", fMinTemp);
				strTemp.Format(strFormat, fMinTemp); //ksj 20201112
				m_LabAuxTempMin = strTemp;
				//strTemp.Format("%0.3f", fMaxTemp);
				strTemp.Format(strFormat, fMaxTemp); //ksj 20201112

				m_LabAuxTempMax = strTemp;
			}
			else if(pAuxData[i].auxChType == PS_AUX_TYPE_TEMPERATURE_TH)
			{
				if (bAuxFirstTher) 
				{
					fMinTher = fMaxTher = fValue;
					bAuxFirstTher = FALSE;
				}
				else
				{
					if (fMinTher > fValue) fMinTher = fValue;
					if (fMaxTher < fValue) fMaxTher = fValue;
				}			
				//strTemp.Format("%0.3f", fMinTher);
				//strTemp.Format(strTemp, fMinTher);  //ksj 20201112
				strTemp.Format(strFormat, fMinTher);  //ksj 20210630 : 오타 수정
				m_LabAuxTherMin = strTemp;
				//strTemp.Format("%0.3f", fMaxTher);
				//strTemp.Format(strTemp, fMaxTher);  //ksj 20201112
				strTemp.Format(strFormat, fMaxTher);  //ksj 20210630 : 오타 수정
				m_LabAuxTherMax = strTemp;
			}
			//ksj 20200116 : 습도 추가
			else if(pAuxData[i].auxChType == PS_AUX_TYPE_HUMIDITY)
			{
				if (bAuxFirstTherHumi) 
				{
					fMinTherHumi = fMaxTherHumi = fValue;
					bAuxFirstTherHumi = FALSE;
				}
				else
				{
					if (fMinTherHumi > fValue) fMinTherHumi = fValue;
					if (fMaxTherHumi < fValue) fMaxTherHumi = fValue;
				}			
				//strTemp.Format("%0.3f", fMinTherHumi);
				//strTemp.Format(strTemp, fMinTherHumi);  //ksj 20201112
				strTemp.Format(strFormat, fMinTherHumi);  //ksj 20210630 : 오타 수정
				m_LabAuxTherHumiMin = strTemp;
				//strTemp.Format("%0.3f", fMaxTherHumi);
				//strTemp.Format(strTemp, fMaxTherHumi);  //ksj 20201112
				strTemp.Format(strFormat, fMaxTherHumi);  //ksj 20210630 : 오타 수정
				//m_LabAuxTherHumiMax = strTemp;
			}

		if(pAuxData[i].auxChType == PS_AUX_TYPE_VOLTAGE)
		{
			//2014.09.26 레지스트리에서 사용유무 설정.
			if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseAuxEmgVoltage", FALSE))
			{	
				float Voltage =atof(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "AuxEmgVoltageVal", "5.0"));
				float minusVoltage =atof(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "AuxEmgVoltageminusVal", "-5.0"));				
				if((fValue > Voltage) || (minusVoltage > fValue) )
				{	// 기존본은 5.0			
					if(bShowEmgDlg == FALSE)
					{
						SFT_EMG_DATA *lpData = new SFT_EMG_DATA;
						lpData->lCode = 1001; //알람코드는 고정.
						lpData->lValue = fValue;
						//sprintf(lpData->szName,"%d",pAuxData[i].chNo);
						bShowEmgDlg = TRUE;
						::PostMessage(m_hWnd, SFTWM_MODULE_EMG, (WPARAM)nModuleID, (LPARAM)lpData);
												
						//delete lpData;
					}								
				}				
			}
		}
						
		m_wndAuxList.SetItemText(i, 2, strItem);	
		
		STF_MD_AUX_SET_DATA AuxSetData = pSensor->GetAuxData();

#if 0 //ksj 20201112 : 주석처리
		//ksj 20200117 : 고정 상한/하한 표시, 단위 처리 확인 필요.
		strItem.Format("%f", float(GetDocument()->UnitTrans(AuxSetData.vent_upper, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		m_wndAuxList.SetItemText(i, 3, strItem);

		strItem.Format("%f", float(GetDocument()->UnitTrans(AuxSetData.vent_lower, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		m_wndAuxList.SetItemText(i, 4, strItem);
		//ksj end
		
		strItem.Format("%f", float(GetDocument()->UnitTrans(AuxSetData.lMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 3, strItem);		
		m_wndAuxList.SetItemText(i, 5, strItem);		

		strItem.Format("%f", float(GetDocument()->UnitTrans(AuxSetData.lMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 4, strItem);		
		m_wndAuxList.SetItemText(i, 6, strItem);	

		//ksj 20200120
		strItem.Format("%s", (AuxSetData.vent_use_flag)?"O":"X"); //vent 안전기능 사용 여부
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		m_wndAuxList.SetItemText(i, 7, strItem);
		//ksj end

		strItem.Format("%f", float(GetDocument()->UnitTrans(AuxSetData.lEndMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 5, strItem);
		m_wndAuxList.SetItemText(i, 8, strItem); //ksj 20200117

		strItem.Format("%f", float(GetDocument()->UnitTrans(AuxSetData.lEndMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 6, strItem);
		m_wndAuxList.SetItemText(i, 9, strItem);
#endif

/*#ifdef _DEBUG //ksj 20201112 : test
		m_nMaxAuxFloatingPoint = 2;
		AuxSetData.vent_upper = 1234.567891;
		AuxSetData.vent_lower = 1234.567891;
		AuxSetData.lMaxData = 1234.567891;
		AuxSetData.lMinData = 1234.567891;
		AuxSetData.lEndMaxData = 1234.567891;
		AuxSetData.lEndMinData = 1234.567891;
#endif*/

		//ksj 20201112 : 소수점 아래 표현 개수 변경 옵션 추가.//////////////////////////////////////////////////////////////////		
		strItem.Format(strFormat, float(GetDocument()->UnitTrans(AuxSetData.vent_upper, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		m_wndAuxList.SetItemText(i, 3, strItem);

		strItem.Format(strFormat, float(GetDocument()->UnitTrans(AuxSetData.vent_lower, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		m_wndAuxList.SetItemText(i, 4, strItem);

		strItem.Format(strFormat, float(GetDocument()->UnitTrans(AuxSetData.lMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 3, strItem);		
		m_wndAuxList.SetItemText(i, 5, strItem);		

		strItem.Format(strFormat, float(GetDocument()->UnitTrans(AuxSetData.lMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 4, strItem);		
		m_wndAuxList.SetItemText(i, 6, strItem);	

		strItem.Format("%s", (AuxSetData.vent_use_flag)?"O":"X"); //vent 안전기능 사용 여부
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		m_wndAuxList.SetItemText(i, 7, strItem);

		strItem.Format(strFormat, float(GetDocument()->UnitTrans(AuxSetData.lEndMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 5, strItem);
		m_wndAuxList.SetItemText(i, 8, strItem); //ksj 20200117

		strItem.Format(strFormat, float(GetDocument()->UnitTrans(AuxSetData.lEndMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		//m_wndAuxList.SetItemText(i, 6, strItem);
		m_wndAuxList.SetItemText(i, 9, strItem);
		//ksj end : 소수점 아래 표현 개수 변경 옵션 추가. 끝//////////////////////////////////////////////////////////////////

 		//int nAux_division;
 		//nAux_division = AuxSetData.funtion_division1;
		strItem = Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division1);
		//if (nAux_division == 0)
		//	strItem.Format("NONE", nAux_division);
		//else if (nAux_division == ID_AUX_HIGH_LOW)
		//	strItem.Format("HIGH or LOW", nAux_division);
		//else if (nAux_division == ID_AUX_HIGH)
		//	strItem.Format("HIGH", nAux_division);
		//else if (nAux_division == ID_AUX_LOW)
		//	strItem.Format("LOW", nAux_division);
		//else
		//	strItem.Format("%d",nAux_division);

		//m_wndAuxList.SetItemText(i, 7, strItem);
		m_wndAuxList.SetItemText(i, 10, strItem);

		strItem = Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division2);
		//m_wndAuxList.SetItemText(i, 8, strItem);
		m_wndAuxList.SetItemText(i, 11, strItem);

		strItem = Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division3);
		//m_wndAuxList.SetItemText(i, 9, strItem);
		m_wndAuxList.SetItemText(i, 12, strItem);

		if (pAuxData[i].auxChType != 2)
			strItem = "";
		else
			strItem = Fun_GetFunctionString(ID_AUX_THERMISTOR_TYPE, AuxSetData.auxTempTableType);
		//m_wndAuxList.SetItemText(i, 10, strItem);	
		m_wndAuxList.SetItemText(i, 13, strItem);	

		//cny-------------------------------------------------		
		if(pSensor->GetAuxType()==PS_AUX_TYPE_VOLTAGE)
		{
			bCellBAL=FALSE;
			if(g_AppInfo.iCellBalAuxDiv3>0)
			{
				if(g_AppInfo.iCellBalAuxDiv3==AuxSetData.funtion_division3)
				{
					bCellBAL=TRUE;
				}
			}
			else
			{
				bCellBAL=TRUE;
			}
			if( bCellBAL==TRUE && 
				lpChannelInfo->m_CtrlClient.m_iState==ID_SOCKET_STAT_CONN)
			{
				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
				int module_installed_ch=atoi(strTemp);
					
				if( module_installed_ch>0 &&
					module_installed_ch>nCellBal_No)
				{
					char ch_state=lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[nCellBal_No].ch_state;
						
					strItem.Format(_T("%c"),ch_state);
					//m_wndAuxList.SetItemText(i, 11, strItem);
					m_wndAuxList.SetItemText(i, 14, strItem);
				}
				else
				{
					//m_wndAuxList.SetItemText(i, 11, _T(""));
					m_wndAuxList.SetItemText(i, 14, _T(""));
				}
			}
			else
			{
				//m_wndAuxList.SetItemText(i, 11, _T(""));
				m_wndAuxList.SetItemText(i, 14, _T(""));
			}
			nCellBal_No++;
		}		
		//-------------------------------------------------
	}	
	//yulee 20180927 
	if((m_nPreChannelNo != m_nCurrentChannelNo) || (lpChannelInfo->GetAuxCount() == 0))
	{		
		CString strInitValue;
		strInitValue.Format("0.0");
		m_LabAuxTempMax	= strInitValue;
		m_LabAuxTempMin	= strInitValue;
		m_LabAuxVoltMax	= strInitValue;
		m_LabAuxVoltMin	= strInitValue;
		m_LabAuxTherMax	= strInitValue;
		m_LabAuxTherMin	= strInitValue;
	}
    m_nPreChannelNo = m_nCurrentChannelNo;
	if (lpChannelInfo->GetAuxCount() == 0)
	{
		m_wndAuxList.DeleteAllItems();
	}
	Invalidate(FALSE);
}


void CCTSMonProView::InitLabel(int nFontSize)
{
	UpdateData();

	SetLedUnitAll();
	//yulee 20181019
	int chCnt = m_wndChannelList.GetItemCount();

	if(chCnt == 1) //yulee 20181029
	{
		m_ctrlLabelVoltage.SetFontSize(nFontSize);
		m_ctrlLabelImpedance.SetFontSize(nFontSize);
		m_ctrlLabelCurrent.SetFontSize(nFontSize);
		
		m_ctrlLabelCh.SetFontSize(nFontSize);
		m_ctrlLabelCapasity.SetFontSize(nFontSize);
		m_ctrlLabelStepTime.SetFontSize(nFontSize);
		m_ctrlLabelTotalTime.SetFontSize(nFontSize);
		
		m_ctrlLabelWatt.SetFontSize(nFontSize);
		m_ctrlLabelWattHour.SetFontSize(nFontSize);
		
		m_ctrlLabelVUnit.SetFontSize(nFontSize);
		m_ctrlLabelAUnit.SetFontSize(nFontSize);
		m_ctrlLabelCUnit.SetFontSize(nFontSize);
		m_ctrlLabelWUnit.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit.SetFontSize(nFontSize);
		
		m_ctrlLabelChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);
		
		m_ctrlLabelBmsChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);
		
		m_ctrlLabelChState.SetFontSize(15)
			//.SetTextColor(RGB(200,50,50))
			//.SetBkColor(RGB(255,0,0))
			//.SetGradientColor(RGB(255,255,255))
			//.SetFontBold(TRUE)
			//.SetGradient(TRUE)
			.SetSunken(TRUE)
			.SetFontName("HY헤드라인M");

			GetDlgItem(IDC_STA_CH1INFOBOXINCH2)->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_VOLTAGE      )->ShowWindow(TRUE);
			GetDlgItem(IDC_STATIC_VOLTAGE     )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_V_UNIT       )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_CURRENT      )->ShowWindow(TRUE);
			GetDlgItem(IDC_STATIC_CURRENT     )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_A_UNIT       )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_CAPASITOR    )->ShowWindow(TRUE);
			GetDlgItem(IDC_STATIC_CAPASITOR   )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_C_UNIT       )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_WATT         )->ShowWindow(TRUE);
			GetDlgItem(IDC_STATIC_WATT        )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_W_UNIT       )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_WATTHOUR     )->ShowWindow(TRUE);
			GetDlgItem(IDC_STATIC_WATTHOUR    )->ShowWindow(TRUE);
			GetDlgItem(IDC_LABEL_WH_UNIT      )->ShowWindow(TRUE);


			GetDlgItem(IDC_STA_CH1INFOBOXINCH4)->ShowWindow(FALSE);
			GetDlgItem(IDC_STA_CH2INFOBOXINCH4)->ShowWindow(FALSE);
			GetDlgItem(IDC_STA_CH2INFOBOXINCH2)->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_VOLTAGE2     )->ShowWindow(FALSE);
			GetDlgItem(IDC_STATIC_VOLTAGE2    )->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_V_UNIT1      )->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_CURRENT2     )->ShowWindow(FALSE);
			GetDlgItem(IDC_STATIC_CURRENT2    )->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_A_UNIT1      )->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_CAPASITOR2   )->ShowWindow(FALSE);
			GetDlgItem(IDC_STATIC_CAPASITOR2  )->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_C_UNIT1      )->ShowWindow(FALSE);

			GetDlgItem(IDC_STA_CH3INFOBOX		)->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_VOLTAGE3		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_STATIC_VOLTAGE3		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_V_UNIT2		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_CURRENT3		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_STATIC_CURRENT3		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_A_UNIT2		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_CAPASITOR3		)->ShowWindow(FALSE);
			GetDlgItem(IDC_STATIC_CAPASITOR3	)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_C_UNIT2		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_STA_CH4INFOBOX		)->ShowWindow(FALSE);
			GetDlgItem(IDC_LABEL_VOLTAGE4		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_STATIC_VOLTAGE4		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_V_UNIT3		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_CURRENT4		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_STATIC_CURRENT4		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_A_UNIT3		)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_CAPASITOR4		)->ShowWindow(FALSE);
			GetDlgItem(IDC_STATIC_CAPASITOR4	)->ShowWindow(FALSE);	
			GetDlgItem(IDC_LABEL_C_UNIT3		)->ShowWindow(FALSE);	
			
			HideOrShowButton();
			//HideVentClose();	
	}
	
	else if(chCnt == 2)
	{
		nFontSize = 15;
		m_ctrlLabelVoltage.SetFontSize(nFontSize);
	    m_ctrlLabelImpedance.SetFontSize(nFontSize);
		m_ctrlLabelCurrent.SetFontSize(nFontSize);
	
		m_ctrlLabelCh.SetFontSize(nFontSize);
		m_ctrlLabelCapasity.SetFontSize(nFontSize);
		m_ctrlLabelStepTime.SetFontSize(nFontSize);
		m_ctrlLabelTotalTime.SetFontSize(nFontSize);

		m_ctrlLabelCurrent2.SetFontSize(nFontSize);
		m_ctrlLabelVoltage2.SetFontSize(nFontSize);
		m_ctrlLabelCapasity2.SetFontSize(nFontSize);

		m_ctrlLabelWatt.SetFontSize(nFontSize);
		m_ctrlLabelWatt2.SetFontSize(nFontSize);
		m_ctrlLabelWattHour.SetFontSize(nFontSize);
		m_ctrlLabelWattHour2.SetFontSize(nFontSize);

		m_ctrlLabelVUnit.SetFontSize(nFontSize);
		m_ctrlLabelVUnit2.SetFontSize(nFontSize);
		m_ctrlLabelAUnit.SetFontSize(nFontSize);
		m_ctrlLabelAUnit2.SetFontSize(nFontSize);
		m_ctrlLabelCUnit.SetFontSize(nFontSize);
		m_ctrlLabelCUnit2.SetFontSize(nFontSize);
		m_ctrlLabelWUnit.SetFontSize(nFontSize);
		m_ctrlLabelWUnit2.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit2.SetFontSize(nFontSize);

		m_ctrlLabelChInfo.SetFontSize(nFontSize)
						 .SetTextColor(RGB(50, 100, 200))
						 .SetBkColor(RGB(120,170,255))
						 .SetGradientColor(RGB(255,255,255))
						 .SetFontBold(TRUE)
						 .SetGradient(TRUE)
					 .SetFontName(Fun_FindMsg("InitLabel_msg1","IDD_CTSMonPro_FORM"))
					 //@ .SetFontName("HY헤드라인M")
						 .SetSunken(TRUE);

		m_ctrlLabelBmsChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
		.SetFontName(Fun_FindMsg("InitLabel_msg2","IDD_CTSMonPro_FORM"))
		//@ .SetFontName("HY헤드라인M")
			.SetSunken(TRUE);

		m_ctrlLabelChState.SetFontSize(15)
						  //.SetTextColor(RGB(200,50,50))
						  //.SetBkColor(RGB(255,0,0))
					 	  //.SetGradientColor(RGB(255,255,255))
						  //.SetFontBold(TRUE)
						  //.SetGradient(TRUE)
						  .SetSunken(TRUE)
						  .SetFontName("HY헤드라인M");

		GetDlgItem(IDC_STA_CH1INFOBOXINCH4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH3INFOBOX	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_VOLTAGE3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_VOLTAGE3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_V_UNIT2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CURRENT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CURRENT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_A_UNIT2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CAPASITOR3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CAPASITOR3)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_C_UNIT2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH4INFOBOX	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_VOLTAGE4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_VOLTAGE4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_V_UNIT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CURRENT4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CURRENT4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_A_UNIT3	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_CAPASITOR4	)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CAPASITOR4)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_C_UNIT3	)->ShowWindow(FALSE);

		GetDlgItem(IDC_STA_CH1INFOBOXINCH2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR2)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT1	)->ShowWindow(TRUE);

		GetDlgItem(IDC_LABEL_WATT			)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATT			)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_W_UNIT			)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATTHOUR		)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATTHOUR		)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WH_UNIT		)->ShowWindow(TRUE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATT2			)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATT2			)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_W_UNIT1		)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WATTHOUR2		)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_WATTHOUR2		)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_WH_UNIT1		)->ShowWindow(TRUE);
		
		GetDlgItem(IDC_BTN_DATA_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_GRAPH_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_CHECK_COND		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_START			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_PAUSE			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_NEXT_STEP		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_STOP			    )->ShowWindow(TRUE);
		if(m_bUseWorkReserv) GetDlgItem(IDC_BTN_RESERV			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_VIEW_REALGRAPH	)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_EDITOR			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_ANAL				)->ShowWindow(TRUE);

		HideOrShowButton();
		//HideVentClose();	
		
		int nGap = 0;
		int nGapTimes = 0;
		int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);//yulee 20190706 2Ch 표시
		if(nSelLanguage == 1)//2CH
		{
			nGap = 260;
		}
		else if(nSelLanguage == 2)
		{
			nGap = 260;
		}
		else if(nSelLanguage == 3)
		{
			nGap = 260;
		}

		nGapTimes = nGap *1;
		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH2, IDC_STA_CH2INFOBOXINCH2, nGapTimes);//yulee 20181019
	
		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_WATT, IDC_LABEL_WATT2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_WATT, IDC_STATIC_WATT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_W_UNIT, IDC_LABEL_W_UNIT1, nGapTimes);

		MoveWindowWithVerGap(IDC_LABEL_WATTHOUR, IDC_LABEL_WATTHOUR2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_WATTHOUR, IDC_STATIC_WATTHOUR2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_WH_UNIT, IDC_LABEL_WH_UNIT1, nGapTimes);

	}
	else if(chCnt == 4)
	{
		m_ctrlLabelImpedance.SetFontSize(nFontSize);
		m_ctrlLabelCh.SetFontSize(nFontSize);
		
		m_ctrlLabelVoltage.SetFontSize(nFontSize);
		m_ctrlLabelCurrent.SetFontSize(nFontSize);
		m_ctrlLabelCapasity.SetFontSize(nFontSize);
		
		m_ctrlLabelCurrent2.SetFontSize(nFontSize);
		m_ctrlLabelVoltage2.SetFontSize(nFontSize);
		m_ctrlLabelCapasity2.SetFontSize(nFontSize);
		
		m_ctrlLabelVoltage3.SetFontSize(nFontSize); //yulee 20180905
		m_ctrlLabelCurrent3.SetFontSize(nFontSize);
		m_ctrlLabelCapasity3.SetFontSize(nFontSize);
		
		m_ctrlLabelCurrent4.SetFontSize(nFontSize);
		m_ctrlLabelVoltage4.SetFontSize(nFontSize);
		m_ctrlLabelCapasity4.SetFontSize(nFontSize);
		
		
		m_ctrlLabelStepTime.SetFontSize(nFontSize);
		m_ctrlLabelTotalTime.SetFontSize(nFontSize);
		
		m_ctrlLabelWatt.SetFontSize(nFontSize);
		m_ctrlLabelWatt2.SetFontSize(nFontSize);
		m_ctrlLabelWattHour.SetFontSize(nFontSize);
		m_ctrlLabelWattHour2.SetFontSize(nFontSize);
		
		m_ctrlLabelVUnit.SetFontSize(nFontSize);
		m_ctrlLabelVUnit2.SetFontSize(nFontSize);
		m_ctrlLabelAUnit.SetFontSize(nFontSize);
		m_ctrlLabelAUnit2.SetFontSize(nFontSize);
		m_ctrlLabelCUnit.SetFontSize(nFontSize);
		m_ctrlLabelCUnit2.SetFontSize(nFontSize);
		
		m_ctrlLabelVUnit3.SetFontSize(nFontSize); //yulee 20180905
		m_ctrlLabelVUnit4.SetFontSize(nFontSize);
		m_ctrlLabelAUnit3.SetFontSize(nFontSize);
		m_ctrlLabelAUnit4.SetFontSize(nFontSize);
		m_ctrlLabelCUnit3.SetFontSize(nFontSize);
		m_ctrlLabelCUnit4.SetFontSize(nFontSize);
		
		
		m_ctrlLabelWUnit.SetFontSize(nFontSize);
		m_ctrlLabelWUnit2.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit.SetFontSize(nFontSize);
		m_ctrlLabelWhUnit2.SetFontSize(nFontSize);
		
		m_ctrlLabelChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);
		
		m_ctrlLabelBmsChInfo.SetFontSize(nFontSize)
			.SetTextColor(RGB(50, 100, 200))
			.SetBkColor(RGB(120,170,255))
			.SetGradientColor(RGB(255,255,255))
			.SetFontBold(TRUE)
			.SetGradient(TRUE)
			.SetFontName("HY헤드라인M")
			.SetSunken(TRUE);
		
		m_ctrlLabelChState.SetFontSize(15)
			//.SetTextColor(RGB(200,50,50))
			//.SetBkColor(RGB(255,0,0))
			//.SetGradientColor(RGB(255,255,255))
			//.SetFontBold(TRUE)
			//.SetGradient(TRUE)
			.SetSunken(TRUE)
.SetFontName(Fun_FindMsg("InitLabel_msg3","IDD_CTSMonPro_FORM"));
	//@ 					  .SetFontName("HY헤드라인M");


		GetDlgItem(IDC_STA_CH1INFOBOXINCH4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH4	)->ShowWindow(TRUE);

		GetDlgItem(IDC_STA_CH3INFOBOX	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR3)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STA_CH4INFOBOX	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT3	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR4	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR4)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT3	)->ShowWindow(TRUE);

		GetDlgItem(IDC_STA_CH1INFOBOXINCH2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_VOLTAGE2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_V_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CURRENT2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_A_UNIT1	)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_CAPASITOR2	)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CAPASITOR2)->ShowWindow(TRUE);
		GetDlgItem(IDC_LABEL_C_UNIT1	)->ShowWindow(TRUE);
		
		GetDlgItem(IDC_STA_CH1INFOBOXINCH2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATT			)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATT			)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_W_UNIT			)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATTHOUR		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATTHOUR		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WH_UNIT		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STA_CH2INFOBOXINCH2	)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATT2			)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATT2			)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_W_UNIT1		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WATTHOUR2		)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_WATTHOUR2		)->ShowWindow(FALSE);
		GetDlgItem(IDC_LABEL_WH_UNIT1		)->ShowWindow(FALSE);
		
		GetDlgItem(IDC_BTN_DATA_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_GRAPH_VIEW		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_CHECK_COND		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_START			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_PAUSE			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_NEXT_STEP		)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_STOP			    )->ShowWindow(TRUE);
		if(m_bUseWorkReserv) GetDlgItem(IDC_BTN_RESERV			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BTN_VIEW_REALGRAPH	)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_EDITOR			)->ShowWindow(TRUE);
		GetDlgItem(IDC_BUT_ANAL				)->ShowWindow(TRUE);
		
		HideOrShowButton();
		//HideVentClose();	

		int nGap = 0;
		int nGapTimes = 0;
		int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);//yulee 20190706 4Ch 표시
		if(nSelLanguage == 1)//4CH
		{
			nGap = 150;
		}
		else if(nSelLanguage == 2)
		{
			nGap = 150;
		}
		else if(nSelLanguage == 3)
		{
			nGap = 150;
		}
		nGapTimes = nGap *1;
		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH4, IDC_STA_CH2INFOBOXINCH4, nGapTimes);//yulee 20181019
		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT1, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT1, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT1, nGapTimes);

		nGapTimes = nGap *2;
		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH4, IDC_STA_CH3INFOBOX, nGapTimes);//yulee 20181019
		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE3, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT3, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT2, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR3, nGapTimes);
		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR3, nGapTimes);
		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT2, nGapTimes);

		nGapTimes = nGap *3;
 		MoveWindowWithVerGap(IDC_STA_CH1INFOBOXINCH4, IDC_STA_CH4INFOBOX, nGapTimes);//yulee 20181022
 		MoveWindowWithVerGap(IDC_LABEL_VOLTAGE, IDC_LABEL_VOLTAGE4, nGapTimes);
 		MoveWindowWithVerGap(IDC_STATIC_VOLTAGE, IDC_STATIC_VOLTAGE4, nGapTimes);
 		MoveWindowWithVerGap(IDC_LABEL_V_UNIT, IDC_LABEL_V_UNIT3, nGapTimes);
 		MoveWindowWithVerGap(IDC_LABEL_CURRENT, IDC_LABEL_CURRENT4, nGapTimes);
 		MoveWindowWithVerGap(IDC_STATIC_CURRENT, IDC_STATIC_CURRENT4, nGapTimes);
 		MoveWindowWithVerGap(IDC_LABEL_A_UNIT, IDC_LABEL_A_UNIT3, nGapTimes);
 		MoveWindowWithVerGap(IDC_LABEL_CAPASITOR, IDC_LABEL_CAPASITOR4, nGapTimes);
 		MoveWindowWithVerGap(IDC_STATIC_CAPASITOR, IDC_STATIC_CAPASITOR4, nGapTimes);
 		MoveWindowWithVerGap(IDC_LABEL_C_UNIT, IDC_LABEL_C_UNIT3, nGapTimes);
	}

	UpdateData(FALSE);
}

void CCTSMonProView::MoveWindowWithVerGap(unsigned UpperID, unsigned LowerID, int Gap) //yulee 20181019
{
	CWnd* pWnd = NULL;
	CWnd* pWndCh1 = NULL;
	
	CRect rectTmp, rectCh1;
	pWnd = GetDlgItem(UpperID);
	pWndCh1 = GetDlgItem(LowerID);
	
	if(pWndCh1->GetSafeHwnd())
	{
		pWnd->GetWindowRect(rectTmp);
		ScreenToClient(&rectTmp);
		pWndCh1->GetWindowRect(rectCh1);
		ScreenToClient(&rectCh1);
		
// 		int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
// 		if(nSelLanguage == 1)
// 		{
			pWndCh1->MoveWindow(rectTmp.left, rectTmp.top+Gap, rectTmp.Width(), rectTmp.Height());
// 		}
// 		else if(nSelLanguage == 2)
// 		{
// 			pWndCh1->MoveWindow(rectTmp.left, rectCh1.top, rectTmp.Width(), rectTmp.Height());
// 		}

	}
}

void CCTSMonProView::OnBtnStart() 
{
	OnCmdRun();	
}

void CCTSMonProView::OnBtnPause() 
{
	OnCmdPause();	
}

void CCTSMonProView::OnBtnNextStep() 
{
	OnCmdNextStep();
	
}

void CCTSMonProView::OnBtnStop() 
{
	OnCmdStop();	
}

void CCTSMonProView::UpdateChInfoList()
{
	int chCnt = m_wndChannelList.GetItemCount();
	CCyclerChannel *lpChannelInfo, *lpChannelInfo2, *lpChannelInfo3, *lpChannelInfo4;//yulee 20180905
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pMD;
	
	UINT nModuleID=0,nChannelID=0;

	UINT mdID;
	if (m_nCurrentModuleID == 0)
	{
		nChannelID = m_nCurrentChannelNo;
		int nTotMDCnt = pDoc->GetInstallModuleCount();
		for(int s =0; s<nTotMDCnt; s++)
		{
			nModuleID = s+1;
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				if (pMD->GetTotalChannel() < nChannelID + 1)
				{
					nChannelID = nChannelID - pMD->GetTotalChannel();
				}
				else
				{
					break;
				}
			}
		}
	}
	else
	{
		nModuleID = m_nCurrentModuleID;
		nChannelID = m_nCurrentChannelNo;
	}
	
	if (m_nCurrentChannelNo < 0) return;

	CCyclerModule *lpModule = pDoc->GetModuleInfo(nModuleID);
	CScheduleData schData;
	char szBuff[128];

	if(chCnt == 1)
	{
		lpChannelInfo = lpModule->GetChannelInfo(nChannelID);
		if(lpChannelInfo == NULL)	return;
		
		lpChannelInfo = lpModule->GetChannelInfo(0);
		if(lpChannelInfo != NULL)
		{
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
			m_ctrlStaticVoltage.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));
			m_ctrlStaticCurrent.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
			m_ctrlStaticCapasity.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWatt(), PS_WATT));
			m_ctrlStaticWatt.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWattHour(), PS_WATT_HOUR));
			m_ctrlStaticWattHour.Display(szBuff);
		}
	}
	else if(chCnt == 2)
	{

		lpChannelInfo = lpModule->GetChannelInfo(nChannelID);
		if(lpChannelInfo == NULL)	return;

		lpChannelInfo = lpModule->GetChannelInfo(0);
		if(lpChannelInfo != NULL)
		{
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
			m_ctrlStaticVoltage.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));
			m_ctrlStaticCurrent.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
			m_ctrlStaticCapasity.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWatt(), PS_WATT));
			m_ctrlStaticWatt.Display(szBuff);
			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWattHour(), PS_WATT_HOUR));
			m_ctrlStaticWattHour.Display(szBuff);
		}

		if (1 < lpModule->GetTotalChannel())
		{
			lpChannelInfo = lpModule->GetChannelInfo(0);
			lpChannelInfo2 = lpModule->GetChannelInfo(1);
			if(lpChannelInfo != NULL)
			{
				if (lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)
				{
					sprintf(szBuff, "0.000");
					m_ctrlStaticVoltage2.Display(szBuff);
					m_ctrlStaticCurrent2.Display(szBuff);
					m_ctrlStaticCapasity2.Display(szBuff);
					m_ctrlStaticWatt2.Display(szBuff);
					m_ctrlStaticWattHour2.Display(szBuff);
				}
				else
				{
					if(lpChannelInfo2 != NULL)
					{
						sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetVoltage(), PS_VOLTAGE));
						m_ctrlStaticVoltage2.Display(szBuff);
						sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetCurrent(), PS_CURRENT));
						m_ctrlStaticCurrent2.Display(szBuff);
						sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetCapacity(), PS_CAPACITY));
						m_ctrlStaticCapasity2.Display(szBuff);
						sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetWatt(), PS_WATT));
						m_ctrlStaticWatt2.Display(szBuff);
						sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetWattHour(), PS_WATT_HOUR));
						m_ctrlStaticWattHour2.Display(szBuff);
					}
				}
			}	
		}
	}
	else if(chCnt == 4) //yulee 20181029
	{

	lpChannelInfo = lpModule->GetChannelInfo(nChannelID);
	if(lpChannelInfo == NULL)	return;

	lpChannelInfo = lpModule->GetChannelInfo(0);
	lpChannelInfo3 = lpModule->GetChannelInfo(2);
	if(lpChannelInfo != NULL)
	{
		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage(), PS_VOLTAGE));
		m_ctrlStaticVoltage.Display(szBuff);
		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCurrent(), PS_CURRENT));
		m_ctrlStaticCurrent.Display(szBuff);
		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
		m_ctrlStaticCapasity.Display(szBuff);
		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWatt(), PS_WATT));
		m_ctrlStaticWatt.Display(szBuff);
		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWattHour(), PS_WATT_HOUR));
		m_ctrlStaticWattHour.Display(szBuff);

		//yulee 20180905
		if(lpChannelInfo3 != NULL)
		{
			if((lpChannelInfo3->IsParallel() && lpChannelInfo->m_bMaster == TRUE && lpChannelInfo3->m_bMaster == FALSE)
			&&	(lpChannelInfo3->GetMasterChannelNum() == 1))
			{
				sprintf(szBuff, "0.000");
				m_ctrlStaticVoltage3.Display(szBuff);
				m_ctrlStaticCurrent3.Display(szBuff);
				m_ctrlStaticCapasity3.Display(szBuff);

			}
			else
			{
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo3->GetVoltage(), PS_VOLTAGE));
				m_ctrlStaticVoltage3.Display(szBuff);
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo3->GetCurrent(), PS_CURRENT));
				m_ctrlStaticCurrent3.Display(szBuff);
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo3->GetCapacity(), PS_CAPACITY));
				m_ctrlStaticCapasity3.Display(szBuff);
			}

		}

	}

	if (1 < lpModule->GetTotalChannel())
	{
		lpChannelInfo = lpModule->GetChannelInfo(0);
		lpChannelInfo2 = lpModule->GetChannelInfo(1);
		if(lpChannelInfo != NULL)
		{
			if (lpChannelInfo2->IsParallel() && lpChannelInfo2->m_bMaster == FALSE)
			{
				sprintf(szBuff, "0.000");
				m_ctrlStaticVoltage2.Display(szBuff);
				m_ctrlStaticCurrent2.Display(szBuff);
				m_ctrlStaticCapasity2.Display(szBuff);
				m_ctrlStaticWatt2.Display(szBuff);
				m_ctrlStaticWattHour2.Display(szBuff);
			}
			else
			{
				if(lpChannelInfo2 != NULL)
				{
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetVoltage(), PS_VOLTAGE));
					m_ctrlStaticVoltage2.Display(szBuff);
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetCurrent(), PS_CURRENT));
					m_ctrlStaticCurrent2.Display(szBuff);
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetCapacity(), PS_CAPACITY));
					m_ctrlStaticCapasity2.Display(szBuff);
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetWatt(), PS_WATT));
					m_ctrlStaticWatt2.Display(szBuff);
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo2->GetWattHour(), PS_WATT_HOUR));
					m_ctrlStaticWattHour2.Display(szBuff);
				}
			}
		}
		//추가 
		lpChannelInfo3 = lpModule->GetChannelInfo(2);
		lpChannelInfo4 = lpModule->GetChannelInfo(3);
		if(lpChannelInfo3 != NULL)
			if (lpChannelInfo4->IsParallel() && lpChannelInfo4->m_bMaster == FALSE)
			{
				sprintf(szBuff, "0.000");
				m_ctrlStaticVoltage4.Display(szBuff);
				m_ctrlStaticCurrent4.Display(szBuff);
				m_ctrlStaticCapasity4.Display(szBuff);
			}
			else
			{
				if(lpChannelInfo4 != NULL)
				{
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo4->GetVoltage(), PS_VOLTAGE));
					m_ctrlStaticVoltage4.Display(szBuff);
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo4->GetCurrent(), PS_CURRENT));
					m_ctrlStaticCurrent4.Display(szBuff);
					sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo4->GetCapacity(), PS_CAPACITY));
					m_ctrlStaticCapasity4.Display(szBuff);
				}
			}
		}
	}
		
	//ljb 2011517 채널 인포에 선택된 채널 정보 표시 
	CString strTemp;
	if (lpModule->GetState() == PS_STATE_LINE_OFF )	
	{
		//strTemp = "MUX 상태";
		strTemp = Fun_FindMsg("CTSMonProView_UpdateChInfoList_msg1","IDD_CTSMonPro_FORM");//&&
		m_ctrlLabelBmsChInfo.SetText(strTemp);
	}
	else
	{
		lpChannelInfo = lpModule->GetChannelInfo(nChannelID);
		
		m_lChMuxState = lpChannelInfo->GetMuxUseState();
		m_lChMuxBackupInfo = lpChannelInfo->GetMuxBackupInfo();
// 		if (m_lChMuxState == 0) 
// 		{
// 			strTemp = "MUX사용 안함";
// 		}
// 		else
//		{
			if (m_lChMuxBackupInfo == 0) strTemp = "MUX OPEN State";
			else if (m_lChMuxBackupInfo == 1) strTemp = "MUX A Selected";
			else if (m_lChMuxBackupInfo == 2) strTemp = "MUX B Selected";
			else strTemp.Format("Mux Use : %d,Mux Backup %d",m_lChMuxState,m_lChMuxBackupInfo);
//		}
		m_ctrlLabelBmsChInfo.SetText(strTemp);
	}

// 	if (m_lChOutState != m_lOldChOutState)
// 	{
// 		m_lOldChOutState = m_lChOutState;
// 		if (m_lChOutState != 0)
// 		{
// 			if (m_lChOutState & 0x00000001)	strTemp = "Key On State ";
// 			if (m_lChOutState & 0x00000002)	strTemp += "Charge On State ";
// 			if (m_lChOutState & 0x00000004)	strTemp += "Relay On State";
// 		}
// 		else
// 		{
// 			strTemp = "BMA OFF";
// 		}
// 		//m_ctrlLabelBmsChInfo.SetText(strTemp);
// 	}


	for( int nSelect = 0; nSelect < PS_MAX_ITEM_NUM-2; nSelect++ )
	{
		int nArray = arrayChInfo[nSelect];
		switch(nArray)
		{
		//case PS_ISOLATION_DATA:		sprintf(szBuff, "Ch %d", nChannelID+1);break;
		case PS_STATE:			sprintf(szBuff, "%s", ::PSGetStateMsg(lpChannelInfo->GetState(), lpChannelInfo->GetStepType())); break;
		case PS_VOLTAGE:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage(), nArray));break;								
		case PS_VOLTAGE_INPUT:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage_Input(), nArray));break;
		case PS_VOLTAGE_POWER:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage_Power(), nArray));break;
		case PS_VOLTAGE_BUS:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetVoltage_Bus(), nArray));break;
		case PS_CURRENT:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCurrent(), nArray));break;								
		case PS_CAPACITY:		sprintf(szBuff, "%s",pDoc->ValueString(lpChannelInfo->GetCapacity(), nArray));break;								
		case PS_CHARGE_CAP:		sprintf(szBuff, "%s",pDoc->ValueString(lpChannelInfo->GetChargeAh(), nArray));break;			
		case PS_DISCHARGE_CAP:	sprintf(szBuff, "%s",pDoc->ValueString(lpChannelInfo->GetDisChargeAh(), nArray));break;
		case PS_CAPACITANCE:	sprintf(szBuff, "%s",pDoc->ValueString(lpChannelInfo->GetCapacitance(), nArray));break;
		case PS_IMPEDANCE:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetImpedance(), nArray));break;								
		case PS_CODE:			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCellCode(), nArray));break;
		case PS_STEP_TIME:
			{				
				if (lpChannelInfo->GetStepTimeDay() > 0) 
					strTemp.Format("D%d %s",lpChannelInfo->GetStepTimeDay(), pDoc->ValueString(lpChannelInfo->GetStepTime(), nArray));
				else
					strTemp = pDoc->ValueString(lpChannelInfo->GetStepTime(), nArray);				
				sprintf(szBuff, "%s", strTemp);
				m_ctrlStaticStepTime.DisplayTimeString(szBuff);
				break;
			}
		case PS_TOT_TIME:
			{				
				if (lpChannelInfo->GetTotTimeDay() > 0) 
					strTemp.Format("D%d %s",lpChannelInfo->GetTotTimeDay(), pDoc->ValueString(lpChannelInfo->GetTotTime(), nArray));
				else
					strTemp = pDoc->ValueString(lpChannelInfo->GetTotTime(), nArray);
				
				sprintf(szBuff, "%s", strTemp);
				m_ctrlStaticTotalTime.DisplayTimeString(szBuff);

				break;			
			}
		case PS_CV_TIME:
			{				
				if (lpChannelInfo->GetCVTimeDay() > 0) 
					strTemp.Format("D%d %s",lpChannelInfo->GetCVTimeDay(), pDoc->ValueString(lpChannelInfo->GetCVTime(), nArray));
				else
					strTemp = pDoc->ValueString(lpChannelInfo->GetCVTime(), nArray);
				
				sprintf(szBuff, "%s", strTemp);
								
				break;
			}

		case PS_GRADE_CODE:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetGradeCode(), nArray));break;		
		case PS_STEP_NO:		sprintf(szBuff, "%d", lpChannelInfo->GetStepNo());break;			
		case PS_WATT:			sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWatt(), nArray));break;				
		case PS_WATT_HOUR:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetWattHour(), nArray));break;		
		case PS_CHARGE_WH:		sprintf(szBuff, "%s",pDoc->ValueString(lpChannelInfo->GetChargeWh(), nArray));break;			
		case PS_DISCHARGE_WH:	sprintf(szBuff, "%s",pDoc->ValueString(lpChannelInfo->GetDisChargeWh(), nArray));break;
		case PS_AUX_TEMPERATURE:sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetTemperature(), nArray));break;		
		case PS_AUX_TEMPERATURE_TH:sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetTemperature_TH(), nArray));break;			//20180612 yulee
		case PS_AUX_VOLTAGE:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAuxVoltage(), nArray));break;					//ljb 201007		
		case PS_OVEN_TEMPERATURE:sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->m_fOvenTemp, nArray));break;		
		case PS_OVEN_HUMIDITY:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->m_fOvenHumidity, nArray));break;					//ljb 201007		
		case PS_STEP_TYPE:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetStepType(), nArray));break;		
		case PS_CUR_CYCLE:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCurCycleCount(), nArray));break;		
		case PS_TOT_CYCLE:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetTotalCycleCount(), nArray));break;		
		case PS_ACC_CYCLE1:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAccCycleCount1(), nArray));break;				//ljb v1009
		case PS_ACC_CYCLE2:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAccCycleCount2(), nArray));break;				//ljb v1009
		case PS_ACC_CYCLE3:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAccCycleCount3(), nArray));break;				//ljb v1009
		case PS_ACC_CYCLE4:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAccCycleCount4(), nArray));break;				//ljb v1009
		case PS_ACC_CYCLE5:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAccCycleCount5(), nArray));break;				//ljb v1009
		case PS_MULTI_CYCLE1:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetMultiCycleCount1(), nArray));break;				//ljb v1009
		case PS_MULTI_CYCLE2:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetMultiCycleCount2(), nArray));break;				//ljb v1009
		case PS_MULTI_CYCLE3:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetMultiCycleCount3(), nArray));break;				//ljb v1009
		case PS_MULTI_CYCLE4:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetMultiCycleCount4(), nArray));break;				//ljb v1009
		case PS_MULTI_CYCLE5:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetMultiCycleCount5(), nArray));break;				//ljb v1009
		case PS_TEST_NAME:		sprintf(szBuff, "%s", lpChannelInfo->GetTestName());break;		
		case PS_SCHEDULE_NAME:	sprintf(szBuff, "%s", lpChannelInfo->GetScheduleName());break;			
		case PS_CHANNEL_NO:		sprintf(szBuff, "Ch %d", nChannelID+1);break;
					
		case PS_MODULE_NO:		sprintf(szBuff, "%s", GetDocument()->GetModuleName(nModuleID));break;		
		case PS_LOT_NO:			sprintf(szBuff, "%s",lpChannelInfo->GetTestSerial());break;					
		case PS_AVG_CURRENT:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAvgCurrent(), PS_CURRENT));break;		
		case PS_AVG_VOLTAGE:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetAvgVoltage(), PS_VOLTAGE));break;		
		case PS_CAPACITY_SUM:	sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCapacitySum(), PS_CAPACITY));break;
		case PS_CHILLER_REF_TEMP:sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->m_fOvenTemp, nArray));break;		
		case PS_CHILLER_CUR_TEMP:sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->m_fOvenTemp, nArray));break;		

		//ljb add for v100B
		case PS_AUX_COMM_TEMP:
			if (lpChannelInfo->GetCommState() & 0x00000001)
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg1","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "통신 이상");
			else
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg2","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "정상");
			break;
			//sprintf(szBuff, "%d", lpChannelInfo->GetCommState());break;
		case PS_AUX_COMM_VOLT: 
			if ((lpChannelInfo->GetCommState() >> 1) & 0x00000001)
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg3","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "통신 이상");
			else
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg4","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "정상");
			break;
			//sprintf(szBuff, "%d", lpChannelInfo->GetCommState());break;
		case PS_CAN_COMM_MASTER: 
			if ((lpChannelInfo->GetCommState() >> 2) & 0x00000001)
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg5","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "통신 이상");
			else
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg6","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "정상");
			break;
			//sprintf(szBuff, "%d", lpChannelInfo->GetCommState());break;
		case PS_CAN_COMM_SLAVE : 
			if ((lpChannelInfo->GetCommState() >> 3) & 0x00000001)
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg7","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "통신 이상");
			else
				sprintf(szBuff, "%s", Fun_FindMsg("UpdateChInfoList_msg8","IDD_CTSMonPro_FORM"));
			//@ sprintf(szBuff, "%s", "정상");
			break;
			//sprintf(szBuff, "%d", lpChannelInfo->GetCommState());break;
		case PS_SYNC_DATE:		
			sprintf(szBuff, "%s", lpChannelInfo->GetSyncDateTimeString());break;

/*			case PS_CHARGE_CAP:		
			if(lpChannelInfo->GetStepType() == PS_STEP_CHARGE)
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
			else
				sprintf(szBuff, "%s", pDoc->ValueString(0.0, PS_CAPACITY));
			break;	  
		case PS_DISCHARGE_CAP:
			if(lpChannelInfo->GetStepType() == PS_STEP_DISCHARGE)
				sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetCapacity(), PS_CAPACITY));
			else
				sprintf(szBuff, "%s", pDoc->ValueString(0.0, PS_CAPACITY));
			break;	 
*/			case PS_METER_DATA:		sprintf(szBuff, "%s", pDoc->ValueString(lpChannelInfo->GetMeterValue(), nArray));break;		
//		case PS_START_TIME:		sprintf(szBuff, "%s", lpChannelInfo->GetStartTime());break;		
//		case PS_END_TIME:		sprintf(szBuff, "%s", lpChannelInfo->GetEndTime());break;		

			

		}
		m_wndChInfoList.SetItemText(nSelect, 1, szBuff);
		ZeroMemory(szBuff,sizeof(szBuff));
	}
	return;
}

void CCTSMonProView::InitStaticCounter()
{//yulee 20181019
	int chCnt = m_wndChannelList.GetItemCount();
	int nPadSize = 0;
	
	if(chCnt == 1)
	{
		nPadSize = 8;
	}
	else if(chCnt == 2)
	{
		nPadSize = 8;
	}
	else if(chCnt == 4)
	{
		nPadSize = 9;
	}

	m_ctrlStaticVoltage.SetDraw3DBar(FALSE);
	m_ctrlStaticVoltage.SetFormatString("%.3f");
	m_ctrlStaticVoltage.SetBlankPadding(nPadSize);//yulee 20180905
	m_ctrlStaticVoltage.DisplayFloat(0.0f);
	m_ctrlStaticVoltage.SetColours(RGB(0,255,0));

	m_ctrlStaticCurrent.SetDraw3DBar(FALSE);
	m_ctrlStaticCurrent.SetFormatString("%.3f");
	m_ctrlStaticCurrent.SetBlankPadding(nPadSize);//yulee 20180905
	m_ctrlStaticCurrent.DisplayFloat(0.0f);
	m_ctrlStaticCurrent.SetColours(RGB(0,255,0));

	m_ctrlStaticCapasity.SetDraw3DBar(FALSE);
	m_ctrlStaticCapasity.SetFormatString("%.3f");
	m_ctrlStaticCapasity.SetBlankPadding(nPadSize);//yulee 20180905
	m_ctrlStaticCapasity.DisplayFloat(0.0f);
	m_ctrlStaticCapasity.SetColours(RGB(0,255,0));

	m_ctrlStaticWatt.SetDraw3DBar(FALSE);
	m_ctrlStaticWatt.SetFormatString("%.3f");
	m_ctrlStaticWatt.SetBlankPadding(nPadSize);
	m_ctrlStaticWatt.DisplayFloat(0.0f);
	m_ctrlStaticWatt.SetColours(RGB(0,255,0));

	m_ctrlStaticWattHour.SetDraw3DBar(FALSE);
	m_ctrlStaticWattHour.SetFormatString("%.3f");
	m_ctrlStaticWattHour.SetBlankPadding(nPadSize);
	m_ctrlStaticWattHour.DisplayFloat(0.0f);
	m_ctrlStaticWattHour.SetColours(RGB(0,255,0));

	m_ctrlStaticVoltage2.SetDraw3DBar(FALSE);
	m_ctrlStaticVoltage2.SetFormatString("%.3f");
	m_ctrlStaticVoltage2.SetBlankPadding(nPadSize); //yulee 20180905
	m_ctrlStaticVoltage2.DisplayFloat(0.0f);
	m_ctrlStaticVoltage2.SetColours(RGB(0,255,0));	

	m_ctrlStaticCurrent2.SetDraw3DBar(FALSE);
	m_ctrlStaticCurrent2.SetFormatString("%.3f");
	m_ctrlStaticCurrent2.SetBlankPadding(nPadSize); //yulee 20180905
	m_ctrlStaticCurrent2.DisplayFloat(0.0f);
	m_ctrlStaticCurrent2.SetColours(RGB(0,255,0));

	m_ctrlStaticCapasity2.SetDraw3DBar(FALSE);
	m_ctrlStaticCapasity2.SetFormatString("%.3f");
	m_ctrlStaticCapasity2.SetBlankPadding(nPadSize); //yulee 20180905
	m_ctrlStaticCapasity2.DisplayFloat(0.0f);
	m_ctrlStaticCapasity2.SetColours(RGB(0,255,0));

	m_ctrlStaticWatt2.SetDraw3DBar(FALSE);
	m_ctrlStaticWatt2.SetFormatString("%.3f");
	m_ctrlStaticWatt2.SetBlankPadding(nPadSize);
	m_ctrlStaticWatt2.DisplayFloat(0.0f);
	m_ctrlStaticWatt2.SetColours(RGB(0,255,0));

	m_ctrlStaticWattHour2.SetDraw3DBar(FALSE);
	m_ctrlStaticWattHour2.SetFormatString("%.3f");
	m_ctrlStaticWattHour2.SetBlankPadding(nPadSize);
	m_ctrlStaticWattHour2.DisplayFloat(0.0f);
	m_ctrlStaticWattHour2.SetColours(RGB(0,255,0));

	if(chCnt == 4)
	{
		m_ctrlStaticVoltage3.SetDraw3DBar(FALSE);
		m_ctrlStaticVoltage3.SetFormatString("%.3f");
		m_ctrlStaticVoltage3.SetBlankPadding(nPadSize);//yulee 20180905
		m_ctrlStaticVoltage3.DisplayFloat(0.0f);
		m_ctrlStaticVoltage3.SetColours(RGB(0,255,0));
	
		m_ctrlStaticCurrent3.SetDraw3DBar(FALSE);
		m_ctrlStaticCurrent3.SetFormatString("%.3f");
		m_ctrlStaticCurrent3.SetBlankPadding(nPadSize);//yulee 20180905
		m_ctrlStaticCurrent3.DisplayFloat(0.0f);
		m_ctrlStaticCurrent3.SetColours(RGB(0,255,0));
	
		m_ctrlStaticCapasity3.SetDraw3DBar(FALSE);
		m_ctrlStaticCapasity3.SetFormatString("%.3f");
		m_ctrlStaticCapasity3.SetBlankPadding(nPadSize);//yulee 20180905
		m_ctrlStaticCapasity3.DisplayFloat(0.0f);
		m_ctrlStaticCapasity3.SetColours(RGB(0,255,0));

		m_ctrlStaticVoltage4.SetDraw3DBar(FALSE);
		m_ctrlStaticVoltage4.SetFormatString("%.3f");
		m_ctrlStaticVoltage4.SetBlankPadding(nPadSize);//yulee 20180905
		m_ctrlStaticVoltage4.DisplayFloat(0.0f);
		m_ctrlStaticVoltage4.SetColours(RGB(0,255,0));
	
		m_ctrlStaticCurrent4.SetDraw3DBar(FALSE);
		m_ctrlStaticCurrent4.SetFormatString("%.3f");
		m_ctrlStaticCurrent4.SetBlankPadding(nPadSize);//yulee 20180905
		m_ctrlStaticCurrent4.DisplayFloat(0.0f);
		m_ctrlStaticCurrent4.SetColours(RGB(0,255,0));
	
		m_ctrlStaticCapasity4.SetDraw3DBar(FALSE);
		m_ctrlStaticCapasity4.SetFormatString("%.3f");
		m_ctrlStaticCapasity4.SetBlankPadding(nPadSize);//yulee 20180905
		m_ctrlStaticCapasity4.DisplayFloat(0.0f);
		m_ctrlStaticCapasity4.SetColours(RGB(0,255,0));
	}
	


	//Channel 이 1개일 경우 
	if(m_wndChannelList.GetItemCount() == 1)
	{
		m_ctrlStaticVoltage2.SetColours(RGB(128,128,128));
		m_ctrlStaticCurrent2.SetColours(RGB(128,128,128));
		m_ctrlStaticCapasity2.SetColours(RGB(128,128,128));
		m_ctrlStaticWatt2.SetColours(RGB(128,128,128));
		m_ctrlStaticWattHour2.SetColours(RGB(128,128,128));

		if(chCnt == 4)
		{
			m_ctrlStaticVoltage3.SetColours(RGB(128,128,128)); //yulee 20180905
			m_ctrlStaticCurrent3.SetColours(RGB(128,128,128));
			m_ctrlStaticCapasity3.SetColours(RGB(128,128,128));

			m_ctrlStaticVoltage4.SetColours(RGB(128,128,128));
			m_ctrlStaticCurrent4.SetColours(RGB(128,128,128));
			m_ctrlStaticCapasity4.SetColours(RGB(128,128,128));
		}
	}

	m_ctrlStaticStepTime.DisplayTime(00,00,00,00, "%H:%M:%S");
	m_ctrlStaticTotalTime.DisplayTime(00,00,00,00, "%H:%M:%S");
}

void CCTSMonProView::OnBtnDataView() 
{
	OnDataView();
	
}

void CCTSMonProView::OnBtnGraphView() 
{
	OnGraphView();
	
}

void CCTSMonProView::OnBtnCheckCond() 
{
	OnBtnTestview();
	
}

void CCTSMonProView::OnBtnViewLog() 
{
	// TODO: Add your control notification handler code here
	GetDocument()->SendScheduleToModuleTest();
	
}




void CCTSMonProView::UpdateChListForSlave(int nListIndex)
{
	LVITEM lvItem;
	int nColCount = CItemOnDisplay::GetItemNum();
	char szBuff[128];
	sprintf(szBuff, "0");

	for(int nI = 0; nI < nColCount; nI++)
	{
		lvItem.iItem = nListIndex;
		lvItem.iSubItem = nI;
		lvItem.pszText = szBuff;
		lvItem.iImage = -1;
		m_wndChannelList.SetItem(&lvItem);
	}

/*	int nColIndex = CItemOnDisplay::FindItemIndexOfTitle(m_pItem, PS_STATE);
	if( nColIndex >= 0 )
	{		
		
		sprintf(szBuff, "%s", "병렬 모드");
	}*/
		
	
}

/// 캔정보를 설정
void CCTSMonProView::OnCanConfig() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCanConfig_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnCanConfig_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			AfxMessageBox(Fun_FindMsg("OnCanConfig_msg3","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox("접속 중인 모듈이 아닙니다. ");
#ifndef _DEBUG
			//return;
#endif
		}
	}		

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;

	CSettingCanDlg dlgCan(GetDocument());
	CSettingCanFdDlg dlgCanFd(GetDocument());

	int nCanBoardType=0;
	nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
	if (nCanBoardType == 0)
	{
		dlgCan.nSelCh = nChIndex;
		dlgCan.nSelModule = nModuleID;
		dlgCan.nLINtoCANSelect = 0;
		dlgCan.m_strTitle = "CAN RX Setting";
		//if(m_bUseCanMux) dlgCan.m_strTitle = "CAN A RX Setting"; //ksj 20201208 //ksj 20201223 : 기능 취소됨
		dlgCan.DoModal();
	}
	else
	{
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 0;
		//dlgCan.m_strTitle = "CAN RX Setting";
		dlgCanFd.m_strTitle = "CAN RX Setting"; //lyj 20200730 설정 저장 시 title 값으로 descript 저장
		//if(m_bUseCanMux) dlgCanFd.m_strTitle = "CAN A RX Setting"; //ksj 20201208 //ksj 20201223 : 기능 취소됨
		dlgCanFd.DoModal();
	}

	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	m_wndCanList.DeleteAllItems();

	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	

	if (m_nCanMasterIDType == 1)
	{
		m_wndCanList.InsertColumn(3,"ID(DEC)",LVCFMT_CENTER,60);
	}                                                                                                                                                                                    
	else
	{
		m_wndCanList.InsertColumn(3,"ID(HEX)",LVCFMT_CENTER,60);
	}
	m_wndCanList.DeleteColumn(4);
}

void CCTSMonProView::OnCanConfigLintoCANRX() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANRX_msg1","IDD_CTSMonPro_FORM")); //&&
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANRX_msg2","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANRX_msg3","IDD_CTSMonPro_FORM")); //&&
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	CSettingCanDlg dlgCan(GetDocument());
	//CSettingCanDlg dlgCanFd(GetDocument());
	CSettingCanFdDlg dlgCanFd(GetDocument()); //lyj 20200217
	
	int nCanBoardType=0;
	nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
	if (nCanBoardType == 0)
	{
		dlgCan.nSelCh = nChIndex;
		dlgCan.nSelModule = nModuleID;
		dlgCan.nLINtoCANSelect = 1;
		dlgCan.m_strTitle = "LinToCAN RX Setting";
		//if(m_bUseCanMux) dlgCan.m_strTitle = "CAN B RX Setting"; //ksj 20201208 //ksj 20201223 : 기능 취소됨
		dlgCan.DoModal();
	}
	else
	{
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 1;
		dlgCanFd.m_strTitle = "LinToCAN RX Setting";
		//if(m_bUseCanMux) dlgCanFd.m_strTitle = "CAN B RX Setting"; //ksj 20201208 //ksj 20201223 : 기능 취소됨
		dlgCanFd.DoModal();
	}
		
	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	m_wndCanList.DeleteAllItems();
	
	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	
	
	if (m_nCanMasterIDType == 1)
	{
		m_wndCanList.InsertColumn(3,"ID(DEC)",LVCFMT_CENTER,60);
	}
	else
	{
		m_wndCanList.InsertColumn(3,"ID(HEX)",LVCFMT_CENTER,60);
	}
	m_wndCanList.DeleteColumn(4);
}

void CCTSMonProView::OnCanConfigLintoCANTX() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANTX_msg1","IDD_CTSMonPro_FORM")); //&&
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANTX_msg2","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANTX_msg3","IDD_CTSMonPro_FORM")); //&&
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	CSettingCanDlg_Transmit dlgCan(GetDocument());
	CSettingCanFdDlg_Transmit dlgCanFd(GetDocument());
	
	int nCanBoardType=0;
	nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
// 	if (nCanBoardType == 0) //yulee 20181127 무조건 CAN FD
// 	{
// 		dlgCan.nSelCh = nChIndex;
// 		dlgCan.nSelModule = nModuleID;
// 		dlgCan.nLINtoCANSelect = 1;
// 		dlgCan.DoModal();
// 	}
// 	else
// 	{
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 1;
		dlgCanFd.m_strTitle = "LinToCAN TX Setting";
		//if(m_bUseCanMux) dlgCanFd.m_strTitle = "CAN B TX Setting"; //ksj 20201208  //ksj 20201223 : 기능 취소됨
		dlgCanFd.DoModal();

/*	}*/


	
	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	m_wndCanList.DeleteAllItems();
	
	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	
	
	if (m_nCanMasterIDType == 1)
	{
		m_wndCanList.InsertColumn(3,"ID(DEC)",LVCFMT_CENTER,60);
	}
	else
	{
		m_wndCanList.InsertColumn(3,"ID(HEX)",LVCFMT_CENTER,60);
	}
	m_wndCanList.DeleteColumn(4);
}

void CCTSMonProView::OnUpdateCanConfig(CCmdUI* pCmdUI) 
{
	/*//ksj 20200215 : 메뉴 노출 여부  //ksj 20201223 : 기능 취소됨
	//if(m_bUseCanSettingDlg == 0) 
	if(m_bUseCanSettingDlg == 0 && m_bUseCanMux == FALSE)  //ksj 20201208
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	//ksj 20201208 : Can Mux 기능 사용시 메뉴 이름 강제 변경.
	if(m_bUseCanMux == TRUE)
	{
		pCmdUI->SetText("CAN A Receive Setting");
	}*/


	int nModuleID=0;
	int nSelCnt=0;
	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
	while(pos)
	{
		nModuleID = m_wndModuleList.GetItemData(m_wndModuleList.GetNextSelectedItem(pos));
		nSelCnt++;
	}
	if (nSelCnt > 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else if (nSelCnt<=0)
	{
		return;
	}

	SFT_MD_SYSTEM_DATA * pSys =  ::SFTGetModuleSysData(nModuleID);

	//2014.10.15 모듈이 접속안되있을 경우 예외처리.
	if(pSys == NULL){
		pCmdUI->Enable(FALSE);
	}

	//하위 버전 호환을 위해 (CAN 사용 안함)
	if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION5 || m_bUseCan == FALSE)
		pCmdUI->Enable(FALSE);	
	else
		pCmdUI->Enable(m_bUseCan);	
	

	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANTX_msg3","IDD_CTSMonPro_FORM")); //&&
//			AfxMessageBox("접속 중인 모듈이 아닙니다. ");
//#ifndef _DEBUG
			return;
//#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;

	SFT_CAN_COMMON_DATA commonData = pMD->GetCANCommonData(nChIndex, PS_CAN_TYPE_MASTER);

	if(commonData.can_lin_select == 0)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnUpdateCanConfigTransmit(CCmdUI* pCmdUI) 
{
	/*//ksj 20200215 : 메뉴 노출 여부 //ksj 20201223 : 기능 취소됨
	//if(m_bUseCanSettingDlg == 0) 
	if(m_bUseCanSettingDlg == 0 && m_bUseCanMux == FALSE)  //ksj 20201208
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	//ksj 20201208 : Can Mux 기능 사용시 메뉴 이름 강제 변경.
	if(m_bUseCanMux == TRUE)
	{
		pCmdUI->SetText("CAN A Transmit Setting");
	}*/

	int nModuleID=0;
	int nSelCnt=0;
	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
	while(pos)
	{
		nModuleID = m_wndModuleList.GetItemData(m_wndModuleList.GetNextSelectedItem(pos));
		nSelCnt++;
	}
	if (nSelCnt > 1)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	else if (nSelCnt<=0)
	{
		return;
	}

	SFT_MD_SYSTEM_DATA * pSys =  ::SFTGetModuleSysData(nModuleID);

	//2014.10.15 모듈이 접속안되있을 경우 예외처리.
	if(pSys == NULL){
		pCmdUI->Enable(FALSE);
	}

	//하위 버전 호환을 위해 (CAN 사용 안함)
	if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION5 || m_bUseCan == FALSE)
		pCmdUI->Enable(FALSE);	
	else
		pCmdUI->Enable(m_bUseCan);	


	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}

	CCyclerModule * pMD;		

	int nPreModuleID= -1, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANTX_msg3","IDD_CTSMonPro_FORM")); //&&
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			//#ifndef _DEBUG
			return;
			//#endif
		}
	}		

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;

	SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nChIndex, PS_CAN_TYPE_MASTER);
	if(commonData.can_lin_select == 0)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::InitCanList()
{
	CImageList m_image;

	CRect rect;
	m_wndCanList.GetClientRect(&rect);
	m_image.Create(0,19,ILC_COLORDDB,1,0); 

	m_wndCanList.SetImageList(&m_image,LVSIL_SMALL); 

	DWORD style = 	m_wndCanList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_SUBITEMIMAGES;

	m_wndCanList.SetExtendedStyle(style); 
	m_wndCanList.SetFont(&m_lfChListFont);
	
	m_wndCanList.InsertColumn(1, _T("No"), LVCFMT_CENTER, 50);
	m_wndCanList.InsertColumn(2, _T(Fun_FindMsg("InitCanList_msg1","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);
	//@ m_wndCanList.InsertColumn(2, _T("이름"), LVCFMT_CENTER, 200);
	m_wndCanList.InsertColumn(3, _T(Fun_FindMsg("InitCanList_msg2","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 90);
	//@ m_wndCanList.InsertColumn(3, _T("값"), LVCFMT_CENTER, 90);
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0)==0)	//Hex
		m_wndCanList.InsertColumn(4, _T("ID(Hex)"), LVCFMT_CENTER, 60);
	else
		m_wndCanList.InsertColumn(4, _T("ID(Dec)"), LVCFMT_CENTER, 60);
	m_wndCanList.InsertColumn(4, _T(Fun_FindMsg("InitCanList_msg3","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 60);		//ljb 20101004
	//@ m_wndCanList.InsertColumn(4, _T("안전상한값"), LVCFMT_CENTER, 60);		//ljb 20101004
	m_wndCanList.InsertColumn(5, _T(Fun_FindMsg("InitCanList_msg4","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 60);		//ljb 20101004
	//@ m_wndCanList.InsertColumn(5, _T("안전하한값"), LVCFMT_CENTER, 60);		//ljb 20101004
	m_wndCanList.InsertColumn(6, _T(Fun_FindMsg("InitCanList_msg5","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 60);		//ljb 20101004
	//@ m_wndCanList.InsertColumn(6, _T("종료상한값"), LVCFMT_CENTER, 60);		//ljb 20101004
	m_wndCanList.InsertColumn(7, _T(Fun_FindMsg("InitCanList_msg6","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 60);		//ljb 20101004
	//@ m_wndCanList.InsertColumn(7, _T("종료하한값"), LVCFMT_CENTER, 60);		//ljb 20101004
	m_wndCanList.InsertColumn(8, _T(Fun_FindMsg("InitCanList_msg7","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//2009-02-08 ljb add
	//@ m_wndCanList.InsertColumn(8, _T("분류 1"), LVCFMT_CENTER, 200);		//2009-02-08 ljb add
	m_wndCanList.InsertColumn(9, _T(Fun_FindMsg("InitCanList_msg8","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//2009-02-08 ljb add
	//@ m_wndCanList.InsertColumn(9, _T("분류 2"), LVCFMT_CENTER, 200);		//2009-02-08 ljb add
	m_wndCanList.InsertColumn(10, _T(Fun_FindMsg("InitCanList_msg9","IDD_CTSMonPro_FORM")), LVCFMT_CENTER, 200);		//2009-02-08 ljb add
	//@ m_wndCanList.InsertColumn(10, _T("분류 3"), LVCFMT_CENTER, 200);		//2009-02-08 ljb add


	//m_wndCanList.GetHeaderCtrl()->EnableWindow(FALSE);
}

///수신된 캔 정보를 갱신한다.
void CCTSMonProView::UpdateCANListData()
{
	CCyclerChannel *lpChannelInfo;
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pMD;
	
	UINT nModuleID=0,nChannelID=0;
	
	UINT mdID;
	if (m_nCurrentModuleID == 0)
	{
		nChannelID = m_nCurrentChannelNo;
		int nTotMDCnt = pDoc->GetInstallModuleCount();
		for(int s =0; s<nTotMDCnt; s++)
		{
			nModuleID = s+1;
			mdID = pDoc->GetModuleID(s);
			pMD =  pDoc->GetCyclerMD(mdID);
			if(pMD)
			{
				if (pMD->GetTotalChannel() < nChannelID + 1)
				{
					nChannelID = nChannelID - pMD->GetTotalChannel();
				}
				else
				{
					break;
				}
			}
		}
	}
	else
	{
		nModuleID = m_nCurrentModuleID;
		nChannelID = m_nCurrentChannelNo;
	}

	CString strItem,strBmsRdy;
	CCyclerModule *lpModule = GetDocument()->GetModuleInfo(nModuleID);
	if(lpModule == NULL)	return;

	//현재 채널 정보
	lpChannelInfo =  lpModule->GetChannelInfo(nChannelID);
	if(lpChannelInfo == NULL)	return;

	//무슨의미인지 모르겠음..
	if (lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE) return;

	//m_wndCanList.DeleteAllItems();
	int nCanListCount = m_wndCanList.GetItemCount();	//리스트가 비어있는지 확인하는 변수

	SFT_CAN_DATA * pCanData = lpChannelInfo->GetCanData();	
	if(pCanData == NULL) return;

	CString strFormat;		
	strFormat.Format("%%.%df",m_nMaxCanFloatingPoint); //ksj 20201112

	for(int i = 0; i < lpModule->GetInstalledCanMaster(nChannelID); i++)
	{
		SFT_CAN_SET_DATA canData = lpModule->GetCANSetData(nChannelID, PS_CAN_TYPE_MASTER, i);

		if(nCanListCount == 0)		//스크롤이 위로 올라가는 것을 방지하기 위해 List가 비어있을 경우만 추가한다.
		{
			strItem.Format("M%d", i+1);
			m_wndCanList.InsertItem(i, strItem);
		}
		m_wndCanList.SetItemText(i, 1, canData.name);
				
		if(pCanData[i].data_type == 0 || pCanData[i].data_type == 1 || pCanData[i].data_type == 2)
		{
/*#ifdef _DEBUG //ksj 20201112 : test
		//m_nMaxAuxFloatingPoint = 2;
		pCanData[i].canVal.fVal[0] = 1234.567891;
#endif*/
			//strItem.Format("%.6f", pCanData[i].canVal.fVal[0]);
			strItem.Format(strFormat, pCanData[i].canVal.fVal[0]); //ksj 20201112
			if (canData.function_division > 100 && canData.function_division < 150)
				strItem.Format("%02x%02x%02x%02x", (BYTE)pCanData[i].canVal.strVal[0], (BYTE)pCanData[i].canVal.strVal[1], (BYTE)pCanData[i].canVal.strVal[2], (BYTE)pCanData[i].canVal.strVal[3]);
			
			strItem.TrimRight("0");
			strItem.TrimRight(".");
		}
		else if(pCanData[i].data_type == 3){
			strItem.Format("%s", pCanData[i].canVal.strVal);
		}
		else if (pCanData[i].data_type == 4){
			strItem.Format("%x", (UINT)pCanData[i].canVal.fVal[0]);			
		}

		//이부분에 추가
		//m_wndCanList.SetItemText(i, 2, strItem);
		if ((lpChannelInfo->GetCommState() >> 2) & 0x00000001) //ksj 20201218 : can master 통신이상
		{
			CString strTemp;
			//strTemp.Format("통신이상(%s)",strItem);
			strTemp.Format("%s(%s)", _T(Fun_FindMsg("UpdateChInfoList_msg3","IDD_CTSMonPro_FORM")), strItem);
			m_wndCanList.SetItemText(i, 2, strTemp);
		}
		else //정상
		{
			m_wndCanList.SetItemText(i, 2, strItem);
		}

		if (m_nCanMasterIDType == 1)
			//strItem.Format("%ld", canData.canID);
			strItem.Format("%u", canData.canID); //ksj 20200323 : unsinged 타입으로 출력. extended id 대응.
		else
			strItem.Format("%x", canData.canID);

		m_wndCanList.SetItemText(i, 3, strItem);		
		
		if (canData.fault_upper != 0)
		{
			strItem.Format("%.3f", canData.fault_upper);
			m_wndCanList.SetItemText(i, 4, strItem);
		}
		if (canData.fault_lower != 0)
		{
			strItem.Format("%.3f", canData.fault_lower);
			m_wndCanList.SetItemText(i, 5, strItem);
		}
		if (canData.end_upper != 0)
		{
			strItem.Format("%.3f", canData.end_upper);
			m_wndCanList.SetItemText(i, 6, strItem);
		}
		if (canData.end_lower != 0)
		{
			strItem.Format("%.3f", canData.end_lower);
			m_wndCanList.SetItemText(i, 7, strItem);
		}

		strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
		m_wndCanList.SetItemText(i, 8, strItem);

		strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
		m_wndCanList.SetItemText(i, 9, strItem);

		strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
		m_wndCanList.SetItemText(i, 10, strItem);
	}
	
	int nIndex = lpModule->GetInstalledCanMaster(nChannelID);
	for(int i = 0; i < lpModule->GetInstalledCanSlave(nChannelID); i++)
	{
		SFT_CAN_SET_DATA canData = lpModule->GetCANSetData(nChannelID, PS_CAN_TYPE_SLAVE, i);
		
		if(nCanListCount == 0)		//스크롤이 위로 올라가는 것을 방지하기 위해 List가 비어있을 경우만 추가한다.
		{
			strItem.Format("S%d", i+1);
			m_wndCanList.InsertItem(i+nIndex, strItem);
		}
		m_wndCanList.SetItemText(i+nIndex, 1, canData.name);
		
		if(pCanData[i+nIndex].data_type == 0 || pCanData[i+nIndex].data_type == 1 || pCanData[i+nIndex].data_type == 2 
			|| pCanData[i].data_type == 4)
		{
			strItem.Format("%.6f", pCanData[i+nIndex].canVal.fVal[0]);
			strItem.TrimRight("0");	
			strItem.TrimRight(".");
		}
		else if(pCanData[i+nIndex].data_type == 3)
			strItem.Format("%s", pCanData[i+nIndex].canVal.strVal);

		//m_wndCanList.SetItemText(i+nIndex, 2, strItem);	
		if ((lpChannelInfo->GetCommState() >> 3) & 0x00000001) //ksj 20201218 : can slave 통신이상
		{
			CString strTemp;
			//strTemp.Format("통신이상(%s)",strItem);
			strTemp.Format("%s(%s)", _T(Fun_FindMsg("UpdateChInfoList_msg3","IDD_CTSMonPro_FORM")), strItem);
			m_wndCanList.SetItemText(i+nIndex, 2, strTemp);	
		}
		else //정상
		{
			m_wndCanList.SetItemText(i+nIndex, 2, strItem);
		}

		if (m_nCanMasterIDType == 1)
			strItem.Format("%ld", canData.canID);
		else
			strItem.Format("%x", canData.canID);
		m_wndCanList.SetItemText(i+nIndex, 3, strItem);	
		
		if (canData.fault_upper != 0)
		{
			strItem.Format("%.3f", canData.fault_upper);
			m_wndCanList.SetItemText(i+nIndex, 4, strItem);
		}
		if (canData.fault_lower != 0)
		{
			strItem.Format("%.3f", canData.fault_lower);
			m_wndCanList.SetItemText(i+nIndex, 5, strItem);
		}
		if (canData.end_upper != 0)
		{
			strItem.Format("%.3f", canData.end_upper);
			m_wndCanList.SetItemText(i+nIndex, 6, strItem);
		}
		if (canData.end_lower != 0)
		{
			strItem.Format("%.3f", canData.end_lower);
			m_wndCanList.SetItemText(i+nIndex, 7, strItem);
		}

		if (canData.function_division != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
			m_wndCanList.SetItemText(i+nIndex, 8, strItem);
		}
		
		if (canData.function_division2 != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
			m_wndCanList.SetItemText(i+nIndex, 9, strItem);
		}
		
		if (canData.function_division3 != 0)
		{
			strItem.Format("%s",Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
			m_wndCanList.SetItemText(i+nIndex, 10, strItem);
		}
	}
}

void CCTSMonProView::OnUpdateBcrSet(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
//	pCmdUI->Enable(FALSE);//lmh 20111107 2단챔버할때는 필요없음
	
}

//DEL void CCTSMonProView::OnUpdateJigTag(CCmdUI* pCmdUI) 
//DEL {
//DEL 	// TODO: Add your command update UI handler code here
//DEL 	pCmdUI->Enable(FALSE);
//DEL 	
//DEL }

void CCTSMonProView::OnOvenWnd() 
{
	//Edited by KBH 20080821
	if(m_pDetailOvenDlg1 == NULL)
	{
		m_pDetailOvenDlg1 = new CDetailOvenDlg(GetDocument());
		m_pDetailOvenDlg1->m_SelectChamberId = 0;
		//m_pDetailOvenDlg1->Create(IDD_DETAIL_OVEN_DLG, this);
		m_pDetailOvenDlg1->Create( //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
			IDD_DETAIL_OVEN_DLG, this); //&&
		m_pDetailOvenDlg1->ShowWindow(SW_SHOW);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", TRUE);
	}
	else
	{
		if(m_pDetailOvenDlg1->IsWindowVisible())
		{
			m_pDetailOvenDlg1->ShowWindow(SW_HIDE);
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
		}
		else
		{
			m_pDetailOvenDlg1->ShowWindow(SW_SHOW);
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", TRUE);
		}
	}
}

void CCTSMonProView::OnUpdateOvenWnd(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	if(bUseOven  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	//Added by KBH 20080821
	if( m_pDetailOvenDlg1 && 
		m_pDetailOvenDlg1->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
	
}

void CCTSMonProView::OnWorkSTShow() 
{
	BOOL bUsePlacePlate;
	
	bUsePlacePlate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", FALSE);
	// yulee 20181002
	if(m_pWorkStationSignDlg == NULL)
	{
		if(bUsePlacePlate)
		{
			m_pWorkStationSignDlg = new CWorkStationSignDlg();
			m_pWorkStationSignDlg->Create(IDD_WORKSTATIONSIGN_DLG, this);
			m_pWorkStationSignDlg->ShowWindow(SW_SHOW);
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", TRUE);	
		}
		else
		{
			//AfxMessageBox("관리자에게 문의하세요. : Registry 설정 Off");
		}
	}
	else
	{
		m_pWorkStationSignDlg->DestroyWindow();
		delete m_pWorkStationSignDlg;
		m_pWorkStationSignDlg = NULL;
		
		m_pWorkStationSignDlg = new CWorkStationSignDlg();
		m_pWorkStationSignDlg->Create(IDD_WORKSTATIONSIGN_DLG, this);
		m_pWorkStationSignDlg->ShowWindow(SW_SHOW);
	}
	
}


void CCTSMonProView::SetLedUnitAll()
{
	CString strTemp;
	CString strName;

	strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V");
	strName.Format(Fun_FindMsg("SetLedUnitAll_msg1","IDD_CTSMonPro_FORM"));	
	//@ strName.Format("전압");	
	m_ctrlLabelVoltage.SetWindowText(strName);
	m_ctrlLabelVoltage2.SetWindowText(strName);
	m_ctrlLabelVUnit.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelVUnit2.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelVoltage3.SetWindowText(strName); //yulee 20180905
	m_ctrlLabelVoltage4.SetWindowText(strName);
	m_ctrlLabelVUnit3.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelVUnit4.SetWindowText(strTemp.Left(strTemp.Find(' ')));
				
	strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "A");
	strName.Format(Fun_FindMsg("SetLedUnitAll_msg2","IDD_CTSMonPro_FORM"));	
	//@ strName.Format("전류");	
	m_ctrlLabelCurrent.SetWindowText(strName);
	m_ctrlLabelCurrent2.SetWindowText(strName);
	m_ctrlLabelAUnit.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelAUnit2.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelCurrent3.SetWindowText(strName);
	m_ctrlLabelCurrent4.SetWindowText(strName);
	m_ctrlLabelAUnit3.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelAUnit4.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	
	strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "C Unit", "Ah");
	strName.Format(Fun_FindMsg("SetLedUnitAll_msg3","IDD_CTSMonPro_FORM"));	
	//@ strName.Format("용량");	
	m_ctrlLabelCapasity.SetWindowText(strName);
	m_ctrlLabelCapasity2.SetWindowText(strName);
	m_ctrlLabelCUnit.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelCUnit2.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelCapasity3.SetWindowText(strName);
	m_ctrlLabelCapasity4.SetWindowText(strName);
	m_ctrlLabelCUnit3.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelCUnit4.SetWindowText(strTemp.Left(strTemp.Find(' ')));

	strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "W Unit", "W");
	strName.Format(Fun_FindMsg("SetLedUnitAll_msg4","IDD_CTSMonPro_FORM"));	
	//@ strName.Format("전력");	
	m_ctrlLabelWatt.SetWindowText(strName);
	m_ctrlLabelWatt2.SetWindowText(strName);
	m_ctrlLabelWUnit.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelWUnit2.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	
	strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Wh Unit", "Wh");
	strName.Format(Fun_FindMsg("SetLedUnitAll_msg5","IDD_CTSMonPro_FORM"));	
	//@ strName.Format("전력량");	
	m_ctrlLabelWattHour.SetWindowText(strName);
	m_ctrlLabelWattHour2.SetWindowText(strName);
	m_ctrlLabelWhUnit.SetWindowText(strTemp.Left(strTemp.Find(' ')));
	m_ctrlLabelWhUnit2.SetWindowText(strTemp.Left(strTemp.Find(' ')));
}

///메인 메뉴 캔전송설정
void CCTSMonProView::OnCanConfigTransmit() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCanConfigTransmit_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}

	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnCanConfigTransmit_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("1 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	return;
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			AfxMessageBox(Fun_FindMsg("OnCanConfigTransmit_msg3","IDD_CTSMonPro_FORM"));
			//@ AfxMessageBox("접속 중인 모듈이 아닙니다. ");
#ifndef _DEBUG
			//return;
#endif
		}
	}

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	CSettingCanDlg_Transmit dlgCan(GetDocument());
	CSettingCanFdDlg_Transmit dlgCanFd(GetDocument());

	int nCanBoardType=0;
	nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
	//if (nCanBoardType == 0) //yulee 20181127 - 무조건 CAN FD
	//{
	//	dlgCan.nSelCh = nChIndex;
	//	dlgCan.nSelModule = nModuleID;
	//	dlgCan.nLINtoCANSelect = 0;
	//	dlgCan.DoModal();
	//}
	//else
	//{
	
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 0;
		dlgCanFd.m_strTitle = "CAN TX Setting";
		//if(m_bUseCanMux) dlgCanFd.m_strTitle = "CAN A TX Setting"; //ksj 20201208 //ksj 20201223 : 기능 취소됨

		dlgCanFd.DoModal();

	//}

	m_wndCanList.DeleteAllItems();		
}


LRESULT CCTSMonProView::OnModuleBmsResultDataReceived(WPARAM wParam, LPARAM lParam)
{
	CString	strTemp;
	TRACE("Module %d BMS Result data received\n", (int)wParam);
	
	STF_MD_CAN_SET_DATA *pBmsData = (STF_MD_CAN_SET_DATA *)lParam;
	if(pBmsData == NULL)	return 0;
//	HANDLE	handle = AfxGetInstanceHandle();		//자신의 핸들 얻기
//	HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");
	
	switch (pBmsData->iFunctionDivision)
	{
	case	51:	
		TRACE("Module %d BMS Fan test Result data receive\n", (int)wParam);
		//ljb Fan Test complete
		//SFT_BMS_FAN_TEST_RESPONSE m_FanTest;
		// 		CCTSMonProDoc *pDoc = GetDocument();
		// 		ZeroMemory(m_FanTest.ModuleID,sizeof(m_FanTest.ModuleID));
		// 		strcpy(m_FanTest.ModuleID,(char *)(LPCTSTR)pDoc->GetModuleName((int)wParam));
		// 		m_FanTest.iFan1=(int)pBmsData->fbms_data[0];
		// 		m_FanTest.iFan2=(int)pBmsData->fbms_data[1];
		// 		m_FanTest.iFan3=(int)pBmsData->fbms_data[2];
// 		if(hWnd)
// 		{
// 			COPYDATASTRUCT CpStructData;
// 			CpStructData.dwData = WM_MY_MESSAGE_CTS_FAN_TEST_RESULT;		
// 			CpStructData.cbData = sizeof(SFT_BMS_FAN_TEST_RESPONSE);
// 			CpStructData.lpData = (PVOID)&m_Bms_Result_Fan_Test;
// 			::SendMessageA(hWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&CpStructData);
// //			AfxMessageBox(strTemp);
// //			AfxMessageBox("BMA 프로그램으로 FAN TEST 결과 전송함");
// 		}
		break;
	case	101:
		TRACE("Module %d bms CPU ID check test command send(OK)\n", (int)wParam);
		//CPU ID check complete
//		UpdateCANListData();
// 		if(hWnd)
// 		{
// 			COPYDATASTRUCT CpStructData;
// 			CpStructData.dwData = WM_MY_MESSAGE_CTS_CPU_TEST_RESULT;		
// 			CpStructData.cbData = sizeof(SFT_BMS_CPU_ID_RESPONSE);
// 			CpStructData.lpData = (PVOID)&m_Bms_Result_Cpu_Test;
// 			::SendMessageA(hWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&CpStructData);
// 			TRACE("HardwareSn = %s, SoftwareSn = %s",m_Bms_Result_Cpu_Test.Vehicle_ManufacturerECU_HardwareNum,m_Bms_Result_Cpu_Test.SubSystem_ECU_SoftwareNum);
// 			strTemp.Format("HardwareSn = %s, SoftwareSn = %s",m_Bms_Result_Cpu_Test.Vehicle_ManufacturerECU_HardwareNum,m_Bms_Result_Cpu_Test.SubSystem_ECU_SoftwareNum);
// //			strTemp.Format("Cpu Test result = %s,%s,%s",m_Bms_Result_Cpu_Test.SubSystem_ECU_SoftwareNum,m_Bms_Result_Cpu_Test.Vehicle_ManufacturerECU_HardwareNum);
// //			AfxMessageBox(strTemp);
// //			AfxMessageBox("BMA 프로그램 으로 CPU ID Check 결과 전송함");
// 		}
		
		break;
	}
	return 1;
}

LRESULT CCTSMonProView::OnModuleMuxCommandReceived(WPARAM wParam, LPARAM lParam)
{
	TRACE("Module %d MUX Command data received\n", (int)wParam);
	int nModule;
	nModule = (int)wParam;

//	SFT_MUX_SELECT_RESPONSE *pMuxData = (SFT_MUX_SELECT_RESPONSE *)lParam;
//	if(pMuxData == NULL)	return;

	SFT_MUX_SELECT_RESPONSE sMuxResponse;
	memcpy(&sMuxResponse,(SFT_MUX_SELECT_RESPONSE *)lParam,sizeof(SFT_MUX_SELECT_RESPONSE));

	CString strTemp, strMsg;
	if (sMuxResponse.response) 
		strTemp = "OK";
	else  
		strTemp = "NG";
	strMsg.Format("Channel : %d \n state : %s \n (setting : %d, state : %d, fault_flag %d)",sMuxResponse.chNum, strTemp, sMuxResponse.setting, sMuxResponse.state, sMuxResponse.fault_flag);
	
	GetDocument()->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);				

	if(m_nFlagMuxResv == 2) //yulee 20181025_1
		AfxMessageBox(strMsg);
	else if(m_nFlagMuxResv == 1) //예약으로 시작 할 때
	{
		m_ResvMuxRecvChNum = sMuxResponse.chNum;
		m_strResvMuxRecvState = strTemp;
		m_ResvMuxRecvSet = sMuxResponse.setting;
		m_strMuxSetRetMsg.Format("%s", strMsg);
	}
	return 1;
}

LRESULT CCTSMonProView::OnRTTableCommandReceived(WPARAM wParam, LPARAM lParam)//yulee 20181122
{
	m_pRTtableSetDlg->SetSBCRTTableAckRecv((int)wParam);
	return 1;

}

LRESULT CCTSMonProView::OnRTHumiTableCommandReceived(WPARAM wParam, LPARAM lParam) //ksj 20200207
{
	m_pRTtableSetDlg->SetSBCRTHumiTableAckRecv((int)wParam);
	return 1;

}

LRESULT CCTSMonProView::OnModuleBmsCommandReceived(WPARAM wParam, LPARAM lParam)
{
	TRACE("Module %d bms test data received\n", (int)wParam);
	
	STF_MD_CAN_SET_DATA *pBmsData = (STF_MD_CAN_SET_DATA *)lParam;
	if(pBmsData == NULL)	return 0;
	HANDLE	handle = AfxGetInstanceHandle();		//자신의 핸들 얻기
	HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");

	switch (pBmsData->iFunctionDivision)
	{
	case	51:	
		TRACE("Module %d bms Fan test command sending (OK)\n", (int)wParam);
//		AfxMessageBox("Sending ok (FAN Test)");
		break;
	case	101:
		TRACE("Module %d bms CPU ID CHECK test command sending (OK)\n", (int)wParam);
//		AfxMessageBox("Sending ok (CPU ID CHECK Test)");
		break;
	}
	
// 	CString strTemp;
//	strTemp.Format("[(%f)(%f)(%f)]",pBmsData->fbms_data[0],pBmsData->fbms_data[1], pBmsData->fbms_data[2]);
//	pDoc->GetModuleName((int)wParam), pBmsData->fbms_data[0],pBmsData->fbms_data[1], pBmsData->fbms_data[2]);
// 	UpdateData(TRUE);
// 	m_ctrlLab_FanTest=strTemp;
// 	UpdateData(FALSE);
	return 1;
}

// void CCTSMonProView::OnUpdateBmsTest(CCmdUI* pCmdUI) 
// {
// 	pCmdUI->Enable(FALSE);	//우선은 사용안함. 사용하게 되면 Enable만 하면 됨 kjh 20080911
// 	
// }

void CCTSMonProView::OnTelnetModule() 
{
	// TODO: Add your command handler code here
	int nModuleID = m_nCurrentModuleID;

	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
	int nSel = m_wndModuleList.GetNextSelectedItem(pos);
	if(nSel >= 0)
	{
		nModuleID = m_wndModuleList.GetItemData(nSel);
	}
	else
	{
		return;
	}

	CCyclerModule * pMD = GetDocument()->GetModuleInfo(nModuleID);

	if(pMD)
	{
 		CString strIP(pMD->GetIPAddress());
 		if(strIP.IsEmpty() == FALSE)
 		{
 			char cmdline[128];
 			CString strUser(AfxGetApp()->GetProfileString("Ftp Set", "User ID", "sbc"));
 			sprintf(cmdline, "telnet %s -l %s", strIP, strUser);
 			GetDocument()->ExecuteProgram(cmdline);
 		}
	}
	else
	{
		AfxMessageBox(Fun_FindMsg("OnTelnetModule_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("접속할 Unit을 먼저 선택 하십시요");
	}	
}

void CCTSMonProView::OnPingModule() 
{
	// TODO: Add your command handler code here
	int nModuleID = m_nCurrentModuleID;

	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
	int nSel = m_wndModuleList.GetNextSelectedItem(pos);

	if(nSel >= 0)
	{
		nModuleID = m_wndModuleList.GetItemData(nSel);
	}
	else
	{
		return;
	}

	CCyclerModule * pMD = GetDocument()->GetModuleInfo(nModuleID);
	if(pMD)
	{
		CString strIP(pMD->GetIPAddress());
		if(strIP.IsEmpty() == FALSE)
		{
			char cmdline[128];
			CString strUser(AfxGetApp()->GetProfileString("Ftp Set", "User ID", "sbc"));
			sprintf(cmdline, "ping %s -t", strIP);
			GetDocument()->ExecuteProgram(cmdline);
		}
	}
	else
	{

		AfxMessageBox(Fun_FindMsg("OnPingModule_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("검사할 Unit을 먼저 선택 하십시요");
	}		
}
BOOL CCTSMonProView::Fun_CpuTest(UINT iChannel)
{
	m_wndChannelList.SetItemState( iChannel,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
	
	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");
		return FALSE;
	}
	
	STF_MD_CAN_SET_DATA canCommandData;
	canCommandData.iFunctionDivision=SFT_CMD_CAN_CPU_TEST;
	canCommandData.ui_Reserved=0x00;

	BOOL bFlag = GetDocument()->SendCanDatatoModule(m_nCurrentModuleID,&canCommandData);
	
	return	TRUE;
}
BOOL CCTSMonProView::Fun_FanTest(UINT iChannel)
{
	if (m_nCurrentModuleID < 1)
	{
		AfxMessageBox(Fun_FindMsg("Fun_FanTest_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업할 모듈을 선택 하세요.");
		return FALSE;
	}
	//SendMessageA(m_wndChannelList.m_hWnd,LB_SETSEL,1,0);
	m_wndChannelList.SetItemState( iChannel,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
	
	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");
		return FALSE;
	}

	STF_MD_CAN_SET_DATA canCommandData;
	canCommandData.iFunctionDivision=SFT_CMD_CAN_FAN_TEST;
	canCommandData.ui_Reserved=0x00;

	BOOL bFlag = GetDocument()->SendCanDatatoModule(m_nCurrentModuleID,&canCommandData);
	return	TRUE;
}
CString CCTSMonProView::Fun_getAppPath()
{
    HMODULE hModule=::GetModuleHandle(NULL); // handle of current module
    
    CString strExeFileName;
    VERIFY(::GetModuleFileName(hModule, strExeFileName.GetBuffer(_MAX_PATH),_MAX_PATH));
	
    char Drive[_MAX_DRIVE];
    char Path[_MAX_PATH];
    //char Filename[_MAX_FNAME];
    //char Ext[_MAX_EXT];
	
    _splitpath(strExeFileName, Drive, Path, NULL,NULL);//Filename, Ext);
	
    return CString(Drive)+CString(Path); // has trailing backslash
}

BOOL CCTSMonProView::Fun_BMASimpleProcess(CString strUserPath, CString strLotID)
{
// 	CCTSMonProDoc *pDoc = GetDocument();
// 	
// 	CString strTestPath, strContent, strLogMsg, strTestName, strScheduleFileName;
// 
// 	//1. 선택된 채널 중 Run 명령이 전송가능한 채널을 Update 한다.
// 	// 확인 Message 창이나 공정 선택 창이 떠있는 동안 모듈의 상태가 변하여서 전송가능 못하 수도 있으므로 Check
// 
// 	//다른 입력창들이 많으므로 확이 하지 않는다.
// 	//2. Check to user
// //	if(IDYES!=MessageBox("작업 시작을 실행하시겠습니까?", "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
// //		return;
// 
// 	//3. 스케쥴 목록에서 스케쥴 선택
// 	//   1) 한개의 시험은 모두 같은 조건이 할당된 채널들이어야 한다.
// 	//   2) 다른 조건이 할당된 채널이 같은 시험이 되지 않도록 시험 시작시에 선택된 채널에 언제나 시험 조건을 새로 전송한다.
// 	//
// 	
// 	CDWordArray adwChArray;
// 	CCyclerModule	*pMD;
// 	CCyclerChannel	*pChannel;
// 
// 	int nItem;
// 	
// 	CString strFileName = Fun_getAppPath();
// 	strFileName += "\\bma_work_condition.pne";
// 	if (!Fun_BMASimpleFileLoad(strFileName)){
		AfxMessageBox(Fun_FindMsg("Fun_BMASimpleProcess_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("bma_work_condition.pne 읽기 에러");
// 		return FALSE;	//ljb  파일에서 시나리오 읽어 에러
// 	}
// //	BOOL	bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);
// 	
// 	UINT nModuleID = 0;
// 	UINT nChIndex = 0;
// 	
// 	//channel 첫번째 선택
// //	m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
// 	
// 	
// 	//실행 가능한 채널 검사
// 	// 	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
// 	// 	{
// 	// 		AfxMessageBox("충/방전 실행 가능한 채널이 없습니다.");
// 	// 		//PostMessageA();
// 	// 		HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");
// 	// 		if(hWnd)
// 	// 		{
// 	// 			//::PostMessageA(hWnd, WM_SIZING, WMSZ_BOTTOM, NULL);
// 	// 			::PostMessageA(hWnd, WM_MY_MESSAGE_CTS_WORK_FAIL, NULL, NULL);
// 	// 		}
// 	// 		
// 	// 		return FALSE;
// 	// 	}
// 	
// 	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)//ljb SFT_CMD_RUN
// 	{
		AfxMessageBox(Fun_FindMsg("Fun_BMASimpleProcess_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
// 		return FALSE;
// 	}
// 	
// 	//////////////////////////////////////////////////////////////////////////
// 	//모듈전체에 전송하는 명령 (선택한 채널 리스트를 구한다.)
// // 	if(&m_wndModuleList == (CListCtrl *)GetFocus())
// // 	{
// // 		//선택한 모듈 리스트를 전달한다.
// // 		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// // 		while(pos)
// // 		{
// // 			nModuleID = m_wndModuleList.GetItemData(m_wndModuleList.GetNextSelectedItem(pos));
// // 			m_aTargetChArray.Add((WORD)nModuleID);
// // 		}
// // 	}
// // 	else	//한개 모듈에 선택 채널에 전송 
// // 	{
// // 		if(UpdateSelectedChList(SFT_CMD_NONE)  == FALSE)//ljb SFT_CMD_RUN
// // 		{
// // 			AfxMessageBox("전송 가능한 채널이 없습니다.");
// // 			return FALSE;
// // 		}
// // 	}
// 	//////////////////////////////////////////////////////////////////////////
// 	CPtrArray	aPtrSelCh;
// 	int nSelCount = 0;
// 
// 	int nSelModuleCount = m_wndModuleList.GetSelectedCount();
// 	if(nSelModuleCount < 1)	
// 	{
		AfxMessageBox(Fun_FindMsg("Fun_BMASimpleProcess_msg3","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("모듈 리스트에서 모듈을 먼저 선택 하십시요.");
// 		return FALSE;
// 	}
// 	
// 	BOOL bStartStepOption = TRUE;
// 	///선택한 채널들이 이전에 정상적으로 끝났는지 확인한다.
// 	///시작 명령을 취소하면 나중에 Run명령을 전송하면 안된다.
// 	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 	while(pos)
// 	{
// 		
// 		nItem = m_wndModuleList.GetNextSelectedItem(pos);
// 		nModuleID = m_wndModuleList.GetItemData(nItem);
// 		pMD = pDoc->GetModuleInfo(nModuleID);
// 		
// 		//Step Skip 기능 가능 여부 검사 
// 		if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSIONV3)		bStartStepOption = FALSE;
// 		
// 		adwChArray.RemoveAll();
// 		
// 		//모듈 명령일 경우 전송가능한 채널 리스트를 구한다.
// // 		if(bModuleCommnad)
// // 		{
// // 			//전송 가능한 채널 List를 구한다.
// // 			for(WORD ch =0; ch<pMD->GetTotalChannel(); ch++)
// // 			{
// // 				if(pDoc->CheckCmdState(nModuleID, (UINT)ch, SFT_CMD_RUN))
// // 				{
// // 					awChArray.Add(ch);
// // 				}
// // 			}
// // 		}
// // 		else //한개 모듈에 대한 선택 채널 전송일 경우 선택 채널을 구한다.
// // 		{
// // 			awChArray.Copy(m_aTargetChArray);
// // 			ASSERT(nModuleID == m_nCurrentModuleID);
// // 		}
// 		
// 		//작업 정보 입력전에 검사 하여야 한다. (중복되 폴더가 있으면 삭제 되므로 )
// 		//이전 시험이 data 손실 구간 정보를 Update한다.
// 		//CCyclerChannel::IsDataLoss() 호출 이전에 최종 정보로 Update 하기 위해 
// 
// //ljb 임시
// // 		if(bCheckPrevData)
// // 		{
// // 			pMD->UpdateDataLossState(&adwChArray);	//data량이 많을시 Loading 시간 측정 필요
// // 		}
// 		
// 		int nTotSize = adwChArray.GetSize();
// 		CDWordArray adwPauseCh;
// 		for(int i=0; i<nTotSize; i++)
// 		{
// 			nChIndex = adwChArray[i];
// 			////새로운 작업 시작전에 이전 Data가 손실 구간이 있는지 확인 한다.
// 			pChannel = pDoc->GetChannelInfo(nModuleID, nChIndex);
// 			if(pChannel != NULL)
// 			{
// 				if(pChannel->GetState() == PS_STATE_PAUSE)
// 				{
// 					adwPauseCh.Add(nChIndex);
// 					TRACE("Pause ch %d\n", nChIndex);
// 				}
// 				aPtrSelCh.Add(pChannel);
// 			}
// 		}
// 		
// 		//만약 Pause인 채널이 있으면 Stop명령을 보낸다.
// //ljb 임시
// // 		if(adwPauseCh.GetSize() > 0)
// // 		{
// // 			// 			if(AfxMessageBox("선택한 채널의 이전 작업을 초기화 하고 새로운 작업을 설정 하시겠습니까?", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
// // 			// 			{
// // 			// 				delete[] aSelChArray;
// // 			// 				aSelChArray = NULL;
// // 			// 				return FALSE;
// // 			// 			}
// // 			
// // 			if(pDoc->SendCommand(nModuleID, &adwPauseCh, SFT_CMD_STOP) == FALSE)	
// // 			{
// // 				AfxMessageBox("STOP 명령 작업 완료 처리 실패");
// // 				return	FALSE;
// // 			}
// // 		}
// 		
// 		nSelCount++;
// 	}
// 	//이전시험 정상 종료 여부 확인 끝
// 	//////////////////////////////////////////////////////////////////////////
// 	
// 	//20070831 KJH 스케줄의 Step 값을 확인한다.
// // 	if(pDoc->ScheduleValidityCheck(&m_BMAScheduleInfo, aSelChArray, m_nCurrentModuleID) < 0)
// // 	{			
// // 		AfxMessageBox(GetDocument()->GetLastErrorString());
// // 		return FALSE;
// // 	}
// 
// 	//5. 입력한 작업명과 저장 위치 확인 
// 	// c:\\Test_Name 형태 
// 	strTestPath = strUserPath;
// 	ASSERT(!strTestPath.IsEmpty());
// 	//strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);
// 	strTestName=strLotID;
// 	
// 	//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("Fun_BMASimpleProcess_msg4","IDD_CTSMonPro_FORM"), TRUE);
	//@ CProgressWnd progressWnd(AfxGetMainWnd(), "전송중...", TRUE);
//     progressWnd.GoModal();
// 	progressWnd.SetRange(0, 100);
// 	//////////////////////////////////////////////////////////////////////////
// 	
// // 	CDWordArray adwChArray;
// 	pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 	nSelCount = 0;
// // 	while(pos)
// // 	{
// // 		nItem = m_wndModuleList.GetNextSelectedItem(pos);
// // 		nModuleID = m_wndModuleList.GetItemData(nItem);
// // 		pMD = pDoc->GetModuleInfo(nModuleID);
// // 		
// // //ljb 임시		awChArray.RemoveAll();
// // 		
// // 		//복구를 하고자 하는 채널은 제외된 채널 선택으로 갱신한다.
// // 		if(nSelCount >= nSelModuleCount)		break;
// // 		//awChArray.Append(aSelChArray[nSelCount++]);
// // 		
// // 		//6. 선택된 Channel을 시험준비 한다.
// // 		progressWnd.SetPos(0);
// // 		int nTotSize = awChArray.GetSize();
// // 		for(int i=0; i<nTotSize; i++)
// // 		{
// // 			nChIndex = awChArray[i];
// // 			
// // 			strLogMsg.Format("%s 의 채널 %d에 시작 명령 전송...", pDoc->GetModuleName(nModuleID), nChIndex+1);
// // 			progressWnd.SetText(strLogMsg);
// // 			progressWnd.SetPos(int((float)i/(float)nTotSize*100.0f));
// // 			
// // 			//Cancel 처리
// // 			if(progressWnd.PeekAndPump() == FALSE)				break;
// // 			
// // 			////새로운 작업 시작전에 이전 Data가 손실 구간이 있는지 확인 한다.
// // 			// 				pChannel = pDoc->GetChannelInfo(nModuleID, nChIndex);
// // 			// 				if(pChannel == NULL)
// // 			// 				{
// // 			// 					TRACE("Channel data 요청 함수 error 발생\n");
// // 			// 					continue;
// // 			// 				}
// // 			
// // 			CString fullName;
// // 			fullName.Format("%s\\%s",strUserPath,strLotID);
// // 			
// // 			//CStartDlg::OnOk()에서 test_name folder는 생성한다.
// // 			//폴더 생성 실패
// // 			if(FALSE == ::CreateDirectory(fullName, NULL))
// // 			{
// // 				DWORD dwCode = ::GetLastError();
// // 				if(dwCode == ERROR_ALREADY_EXISTS)	//이미 존재하는 폴더
// // 				{
// // 					//기존 폴더 삭제
// // 					//삭제되면 손실된 Data가 있는 채널도 복구할 수 없게 됨
// // 					//data량에 따라 지연됨 
// // 					
// // 					CProgressWnd *pProgressWnd = new CProgressWnd;
// // 					pProgressWnd->Create(AfxGetMainWnd(), "삭제중...", TRUE);
// // 					pProgressWnd->Show();
// // 					if(DeletePath(fullName, pProgressWnd) == FALSE)
// // 					{
// // 						MessageBox(fullName+"를 삭제 할 수 없습니다.(폴더가 열려 있거나 파일이 사용중 입니다.)", "생성실패", MB_OK|MB_ICONSTOP);				
// // 						return FALSE;
// // 					}
// // 					pProgressWnd->Hide();
// // 					delete pProgressWnd;
// // 					
// // 					//재 생성
// // 					if(FALSE == ::CreateDirectory(fullName, NULL))
// // 					{
// // 						MessageBox(fullName+"을 생성할 수 없습니다.", "생성실패", MB_OK|MB_ICONSTOP);
// // 						return FALSE;
// // 					}
// // 				}
// // 				else if(dwCode == ERROR_PATH_NOT_FOUND)		//존재 하지 않는 폴더
// // 				{
// // 					//cf. Create all intermediate directories on the path
// // 					//			int SHCreateDirectoryEx	(	HWND hwnd,
// // 					//										LPCTSTR pszPath,
// // 					//										SECURITY_ATTRIBUTES *psa
// // 					//									);
// // 					//Direcotry 및 파일명 검사.
// // 					MessageBox(fullName+"가 존재하지 않습니다. 먼저 저장위치를 생성하십시요.",  "생성실패", MB_OK|MB_ICONSTOP);
// // 					return FALSE;
// // 				}
// // 			}
// // 			
// // 			int nStartChNo = GetStartChNo(nModuleID);
// // 			strContent.Format("%s\\M%02dCh%02d[%03d]", fullName, nModuleID, nChIndex+1, nStartChNo+nChIndex);
// // 			
// // 			//6.1 Channel 폴더를 생성시킨다.
// // 			if( ::CreateDirectory(strContent, NULL) == FALSE )
// // 			{
// // 				strLogMsg.Format("Data 저장 폴더 생성 실패(%s)", strContent);
// // 				AfxMessageBox(strLogMsg);
// // 				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
// // 				return FALSE;
// // 			}
// // 			else
// // 			{
// // 				//Step start 저장 폴더 생성 2006/04/03 Step초기 0.1초 data 저장 폴더 
// // 				strLogMsg.Format("%s\\StepStart", strContent);
// // 				
// // 				//6.1.1 Step start 폴더를 생성시킨다.
// // 				if( ::CreateDirectory(strLogMsg, NULL) == FALSE )
// // 				{
// // 					strContent.Format("Step start 저장용 폴더 생성 실패(%s)", strLogMsg);
// // 					pDoc->WriteSysLog(strContent, CT_LOG_LEVEL_NORMAL);
// // 				}
// // 				
// // 				strLogMsg.Format("Data 저장 폴더 생성 성공(%s)", strContent);
// // 				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_SYSTEM);
// // 			}
// // 			
// // 			//6.2 각 폴더에 스케줄 파일을 생성한다.
// // 			// c:\\Test_Name\M01C01\Test_Name.sch
// // 			//strScheduleFileName = strContent + "\\" + strTestName+ ".sch";
// // 			strScheduleFileName = strContent + "\\" + strTestName+ ".sch";
// // 			
// // 			if(FALSE == m_BMAScheduleInfo.SaveToFile(strScheduleFileName))
// // 			{
// // 				strLogMsg.Format("%s Schedule 파일 생성 실패", strTestName);
// // 				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
// // 				continue;
// // 			}
// // 			
// // 			//6.3 시험명 설정
// // 			pChannel->ResetData();
// // 			//결과 파일 저장위치 설정 //c:\\Test_Name\M01C01\Test_Name.sch
// // 			//Log 파일 생성
// // 			//***** 이후로 작업로그 기록 가능 
// // 			pChannel->SetFilePath(strContent);
// // 			//Set Test Lot No
// // 			// 				pChannel->SetTestSerial(dlg1.m_strLot, dlg1.m_strWorker, dlg1.m_strComment);
// // 			//Save Item Setting
// // 			pChannel->LoadSaveItemSet();
// // 			
// // 			//6.4 Aux Title 파일 생성
// // 			pChannel->SetAuxTitleFile(strContent, strTestName);
// // 			//6.5 Can Title 파일 생성
// // 			pChannel->SetCanTitleFile(strContent, strTestName);
// // 			
// // 			Sleep(10);	//모듈 상태 전이를 위한 시간 지연 
// // 			}
// // 			progressWnd.SetPos(100);
// // 			
// // 			//////////////////////////////////////////////////////////////
// // 			// 7. SBC에 Command를 전송한다.
// // 			//		1) 시험 조건을 모듈로 전송한다.
// // 			//		2) 선택 채널에 시작 명령을 전송한다.
// // 			//		3) 작업 정보를 임시파일(.\\Temp\\M01C01.tmp)에 저장한다. (Pause에서 연속 작업이 가능하도록...)
// // 			
// // 			//Start Cycle, Step 번호 지정 
// // 			Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
// // 			//if(pDoc->SendStartCmd((nModuleID, &awChArray, &m_BMAScheduleInfo, dlg1.m_nStartCycle, dlg1.m_nStartStep) == FALSE)
// // 			if(pDoc->SendStartCmd(nModuleID, &awChArray, &m_BMAScheduleInfo, 0, 0) == FALSE)
// // 			{
// // 				AfxMessageBox(strTestName + " 작업 시작을 실패 하였습니다.");
// // 				return FALSE;
// // 				continue;
// // 			}
// // 		}

	return TRUE;
	//////////////////////////////////////////////////////////////////////////

}

BOOL CCTSMonProView::DeletePath(CString strPath, CProgressWnd *pPrgressWnd )
{
	CWaitCursor aWait;
    
	if(strPath.IsEmpty())	return TRUE;
    
	CFileFind finder;
    BOOL bContinue = TRUE;
	
	CString strTemp(strPath);
    if(strTemp.Right(1) != _T("\\"))
        strTemp += _T("\\");
	
    strTemp += _T("*.*");
    bContinue = finder.FindFile(strTemp);
	
    while(bContinue)
    {
        bContinue = finder.FindNextFile();
        if(finder.IsDots()) // Ignore this item.
        {
            continue;
        }
        else if(finder.IsDirectory()) // Delete all sub item.
        {
            if(DeletePath(finder.GetFilePath()) == FALSE)	return FALSE;
        }
        else // Delete file.
        {
			strTemp = finder.GetFilePath();
			if(pPrgressWnd)
			{
				pPrgressWnd->SetText(strTemp+Fun_FindMsg("DeletePath_msg","IDD_CTSMonPro_FORM"));
				//@ pPrgressWnd->SetText(strTemp+" 삭제중...");
			}
			
            if(::DeleteFile((LPCTSTR)strTemp) == 0)
			{
				return FALSE;
			}
        }
		
		if(pPrgressWnd)
		{
			if(pPrgressWnd->PeekAndPump()== FALSE)	return FALSE;
		}
		
    }
    finder.Close();
	
    if(::RemoveDirectory((LPCTSTR)strPath) == 0)
		return FALSE;
	
	return TRUE;
}
int CCTSMonProView::GetFindCharCount(CString pString, char pChar) 
{ 
	int length = pString.GetLength();
	int find_count = 0; 
	
	for(int i = 0; i < length; i++)
	{ 
		if(pString[i] == pChar) 
			find_count++; 
	} 
	
	return find_count; 
} 
BOOL CCTSMonProView::Fun_BMASimpleFileLoad(CString strFileName)
{
	// TODO: Add extra validation here
// 	CString strReadData;
// 	CStringArray strArryStep;
// 	CStdioFile sourceFile;
//     CFileException ex;
// 	BOOL	bFirst;
// 	bFirst=FALSE;
// 	strArryStep.RemoveAll();
//     if(!sourceFile.Open(strFileName,CFile::modeRead,&ex))
//     {
//         CString strMsg;
//         ex.GetErrorMessage((LPSTR)(LPCSTR)strMsg,1024);
//         //AfxMessageBox(strMsg);
//         return FALSE;
//     }
//     else
//     {
//         BOOL bIsNotEOL;    // 라인이 존재하는지 확인.
//         while(TRUE)
//         {
//             bIsNotEOL=sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.
//             if(!bIsNotEOL)
//                 break;
// 			if (strReadData.Find("//") >=0 ) continue;
// 			strArryStep.Add(strReadData);
//         }
//     }
//     sourceFile.Close();
// 	//---------------------
// 	m_BMAScheduleInfo.ResetData();
// 
// 	//Make Schedule data;
// 	FILE_STEP_PARAM_V100C stepData;
// 	CString strTemp;
// 
// 
// 	//Add step1(Advance step Step)
// 	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100C));
// 	stepData.chType = PS_STEP_ADV_CYCLE;
// 	stepData.chStepNo = 0;
// 	m_BMAScheduleInfo.AddStep(&stepData);
// 
// 	//시나리오 셋팅 시작
// 	for (int i=0 ; i < strArryStep.GetSize(); i++)
// 	{
// 		ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100C));
// 		if (strArryStep.GetAt(i) == "PS_STEP_ADV_CYCLE") continue;
// 		if (strArryStep.GetAt(i) == "PS_STEP_LOOP")
// 		{
// 			stepData.chType = PS_STEP_LOOP;
// 			stepData.nLoopInfoCycle = 1;
// 			stepData.nLoopInfoGotoStep = strArryStep.GetSize();
// 			stepData.chStepNo = i-1;
// 			m_BMAScheduleInfo.AddStep(&stepData);
// 			break;
// 		}
// 		if (strArryStep.GetAt(i).Find("PS_STEP_REST") >= 0)
// 		{
// 			if (strArryStep.GetAt(i).Find(',') < 0) return FALSE;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),1,',');
// 			stepData.chType = (BYTE)PS_STEP_REST;
// 			stepData.chStepNo = i-1;
// 			stepData.fEndTime=atof(strTemp);
// 			if (bFirst)
// 				stepData.fReportTime = 0.1;			//1초 간격으로 저장 0.1 은 100ms
// 			else
// 			{
// 				stepData.fReportTime = 1;			//1초 간격으로 저장 0.1 은 100ms
// 				bFirst=TRUE;
// 			}
// 			m_BMAScheduleInfo.AddStep(&stepData);
// 			continue;
// 		}
// 		if (strArryStep.GetAt(i).Find("PS_STEP_CHARGE") >= 0)
// 		{
// 			if (GetFindCharCount(strArryStep.GetAt(i),',') != 8) return	FALSE;
// 			stepData.chType = (BYTE)PS_STEP_CHARGE;
// 			stepData.chMode = (BYTE)PS_MODE_CC;
// 			stepData.chStepNo = i-1;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),1,',');
// 			stepData.fVref_Charge = atof(strTemp) * 1000;
// 			stepData.fVref_DisCharge = 0;
// 			
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),2,',');
// 			stepData.fIref = atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),3,',');
// 			stepData.fEndV = atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),4,',');
// 			stepData.fEndTime=atof(strTemp);
// 
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),5,',');		//안전조건	전압
// 			stepData.fVLimitLow=atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),6,',');
// 			stepData.fVLimitHigh=atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),7,',');		//안전조건	전류
// 			stepData.fILimitLow=atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),8,',');
// 			stepData.fILimitHigh=atof(strTemp) * 1000;
// 
// 			if (bFirst)
// 				stepData.fReportTime = 0.1;			//1초 간격으로 저장 0.1 은 100ms
// 			else
// 			{
// 				stepData.fReportTime = 1;			//1초 간격으로 저장 0.1 은 100ms
// 			}
// 			m_BMAScheduleInfo.AddStep(&stepData);
// 			continue;
// 		}
// 		if (strArryStep.GetAt(i).Find("PS_STEP_DISCHARGE") >= 0)
// 		{
// 			if (GetFindCharCount(strArryStep.GetAt(i),',') != 8) return	FALSE;
// 			stepData.chType = (BYTE)PS_STEP_DISCHARGE;
// 			stepData.chMode = (BYTE)PS_MODE_CC;
// 			stepData.chStepNo = i-1;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),1,',');
// 			stepData.fVref_Charge = 0;
// 			stepData.fVref_DisCharge = atof(strTemp) * 1000;
// 
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),2,',');
// 			stepData.fIref = atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),3,',');
// 			stepData.fEndV_L = atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),4,',');
// 			stepData.fEndTime=atof(strTemp);
// 
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),5,',');		//안전조건	전압
// 			stepData.fVLimitLow=atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),6,',');
// 			stepData.fVLimitHigh=atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),7,',');		//안전조건	전류
// 			stepData.fILimitLow=atof(strTemp) * 1000;
// 			AfxExtractSubString(strTemp,strArryStep.GetAt(i),8,',');
// 			stepData.fILimitHigh=atof(strTemp) * 1000;
// 
// 			stepData.fReportTime = 0.1;			//1초 간격으로 저장 0.1 은 100ms
// 			m_BMAScheduleInfo.AddStep(&stepData);
// 			continue;
// 		}
// 	}
// 	//Add step(END Step)
// 	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100C));
// 	stepData.chType = PS_STEP_END;
// 	stepData.chStepNo = i-1;
// 	m_BMAScheduleInfo.AddStep(&stepData);	
// 
// 	//시나리오 셋팅 종료
// //	AfxMessageBox(strFileName);
	return TRUE;

}

void CCTSMonProView::OnCmdRunUpdate() 
{
}

void CCTSMonProView::Fun_BMAWorkStop()
{
	SendCmdToModule(m_adwTargetChArray, SFT_CMD_STOP);
}

CString CCTSMonProView::Fun_GetAuxTemperature(CCyclerModule *pModule, CCyclerChannel *pCyclerCh, int nChannel)
{
//  	CCyclerModule *lpModule = GetDocument()->GetModuleInfo(m_nCurrentModuleID);
//  	if(lpModule == NULL)	return "--.-";

	SFT_AUX_DATA * pAuxData = pCyclerCh->GetAuxData();
	if(pAuxData == NULL) return "--.-";
	
	int nTemperPos=-1;
	//첫번째 온도 Aux 찾기
	for(int i = 0; i < pCyclerCh->GetAuxCount(); i++)
	{
		//if (pAuxData[0].chType != 0) return "--.-"; 
		if (pAuxData[i].auxChType == 0) 
		{
			nTemperPos=i;
			break;
		}
		else
			continue;
	}
	if (nTemperPos < 0) return "--.-";
	
	if (pCyclerCh->GetAuxCount())
	{
		CChannelSensor * pSensor = pModule->GetAuxData(pAuxData[nTemperPos].auxChNo-1, pAuxData[nTemperPos].auxChType);
		if(pSensor == NULL)	return "--.-";

		CString strItem;
		float fValue = GetDocument()->UnitTrans( pAuxData[nTemperPos].lValue, FALSE, pAuxData[nTemperPos].auxChType);
		strItem.Format("%0.1f", fValue);
		return strItem;
	}
	return "--.-";
}

void CCTSMonProView::Fun_CmdStop() 
{
	CString strMsg;
	//Stop시 먼저 Pause로 보내고 완전종료 할 것인지 확인하는 방식으로 변경
	//1. 선택된 채널 중 Pause 명령이 전송가능한 채널을 Update 한다.
	CDWordArray arTargetCh;
	// 	if(UpdateSelectedChList(SFT_CMD_PAUSE))
	// 	{
	// 		strMsg.Format("진행중인 시험을 중지 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	// 		if(IDYES!=MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	// 			return;
	// 		
	// 		//2. Send Pause Command
	// 		GetDocument()->SendCommand(m_nCurrentModuleID, &m_aTargetChArray, SFT_CMD_PAUSE);
	// 		
	// 		
	// 		//상태가 전이 되기를 기다린다.
	// 		Sleep(200);
	// 		strMsg.Format("선택된 채널이 모두 [%s]상태로 변경되었습니다. 진행중인 시험을 [%s] 시킵니다.\n\n[%s]을 실행하시겠습니까?", 
	// 			GetDocument()->GetCmdString(SFT_CMD_PAUSE), 
	// 			GetDocument()->GetCmdString(SFT_CMD_STOP), 
	// 			GetDocument()->GetCmdString(SFT_CMD_STOP) 
	// 			);
	// 	}
	// 	else
	// 	{
	// 		strMsg.Format("진행중인 시험을 [%s] 시킵니다.\n\n[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_STOP), GetDocument()->GetCmdString(SFT_CMD_STOP));
	// 	}
	
	//3. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}
	
	
	//4. Send Stop Command
	//상태가 전이 되기를 기다린다.
	SendCmdToModule(m_adwTargetChArray, SFT_CMD_STOP);
	
// 	for(int i = 0 ; i < m_adwTargetChArray.GetSize(); i++)
// 	{								
// 		arTargetCh.Add(MAKELONG(i, m_nCurrentModuleID));					
// 	}
	
	if(GetDocument()->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl)
	{
		//20071114 Oven Control	
		GetDocument()->CheckOvenStopStateAndSendStopCmd(arTargetCh, 1);	//모든 연동 Channel이 Stop 상태이고 Oven이 동작중인 경우 
	}
	else if(GetDocument()->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl) //20180515 yulee
	{
		GetDocument()->CheckOvenStopStateAndSendStopCmd(m_adwTargetChArray, 2);	//모든 연동 Channel이 Stop 상태이고 Oven이 동작중인 경우 
	}
	
}

BOOL CCTSMonProView::Fun_MuxChangeStart(CString strComm, int nOption, CDWordArray *pSelChArray)
{
// 	if (m_nCurrentModuleID < 1)
// 	{
// 		AfxMessageBox("작업할 모듈을 선택 하세요.");
// 		return FALSE;
// 	}
// 	m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
// 	
// 	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
// 	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
// 	{
// 		//AfxMessageBox("전송 가능한 채널이 없습니다.");
// 		return FALSE;
// 	}
	

	SFT_MUX_SELECT sMuxCommand;
	ZeroMemory(&sMuxCommand,sizeof(SFT_MUX_SELECT));
	sMuxCommand.out_mux = nOption;
	if (strComm == "MUX") 
		sMuxCommand.command = 1;
	else if(strComm == "LOAD") 
		sMuxCommand.command = 2;
	else
		sMuxCommand.command = 0;

	CWordArray awSelCh;
	int nModuleID;
	int nChIndex;
	int nCount = pSelChArray->GetSize();
	awSelCh.RemoveAll();
	for (int ch=0; ch<nCount; ch++)
	{
		DWORD dwData = pSelChArray->GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		awSelCh.Add(nChIndex);
	}

	BOOL bFlag = GetDocument()->SetMuxCommandDataToModule(nModuleID,&awSelCh,&sMuxCommand);
	return bFlag;
}

BOOL CCTSMonProView::Fun_IsolationStart(int nCh, int nMainch)
{
	if (m_nCurrentModuleID < 1)
	{
		AfxMessageBox(Fun_FindMsg("Fun_IsolationStart_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업할 모듈을 선택 하세요.");
		return FALSE;
	}
	//m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED); //ksj 20210104 : 주석처리
	
	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");
		return FALSE;
	}

	SFT_DAQ_ISOLATION sDaqIsolation;
	ZeroMemory(&sDaqIsolation,sizeof(SFT_DAQ_ISOLATION));
	//sDaqIsolation.bISO = 1;
	sDaqIsolation.bISO = 0; //lyj 20201203
	sDaqIsolation.div_ch = nCh;
	sDaqIsolation.div_MainCh = nMainch;
	
	BOOL bFlag = GetDocument()->SetDAQIsolationDataToModule(m_nCurrentModuleID,&sDaqIsolation);
	return bFlag;

}

/*BOOL CCTSMonProView::Fun_IsolationEnd(){*/
BOOL CCTSMonProView::Fun_IsolationEnd(int nCh, int nMainch){
// 	if (m_nCurrentModuleID < 1)
// 	{
// 		AfxMessageBox(Fun_FindMsg("Fun_IsolationEnd_msg","IDD_CTSMonPro_FORM"));
// 		//@ AfxMessageBox("작업할 모듈을 선택 하세요.");
// 		return FALSE;
// 	}
// 	m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
// 	
// 	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
// 	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
// 	{
// 		//AfxMessageBox("전송 가능한 채널이 없습니다.");
// 		return FALSE;
// 	}
// 	
	SFT_DAQ_ISOLATION sDaqIsolation;
	ZeroMemory(&sDaqIsolation,sizeof(SFT_DAQ_ISOLATION));
	sDaqIsolation.bISO = 0;
	sDaqIsolation.div_ch = nCh;
	sDaqIsolation.div_MainCh = nMainch;
	
	BOOL bFlag = GetDocument()->SetDAQIsolationDataToModule(m_nCurrentModuleID,&sDaqIsolation);
	return bFlag; 
}

//ksj 20210105 : 일반 절연 함수 분리
BOOL CCTSMonProView::Fun_IsolationStartNormal(int nCh)
{
	if (m_nCurrentModuleID < 1)
	{
		AfxMessageBox(Fun_FindMsg("Fun_IsolationStart_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업할 모듈을 선택 하세요.");
		return FALSE;
	}


	SFT_DAQ_ISOLATION sDaqIsolation;
	ZeroMemory(&sDaqIsolation,sizeof(SFT_DAQ_ISOLATION));
	sDaqIsolation.bISO = 1; // 0: 비절연 실행,   1: 절연 실행
	sDaqIsolation.div_ch = nCh;
	sDaqIsolation.div_MainCh = 0; //미사용

	BOOL bFlag = GetDocument()->SetDAQIsolationDataToChannel(m_nCurrentModuleID,&sDaqIsolation); //ksj 20210104 : 채널단위로 전송. 기존SetDAQIsolationDataToModule에는 절연/비절연 커맨드가 모듈 단위 전송이었으나, 모듈단위로 전송하면 SBC 1번채널이 동작중이면 설비가 동작중이라고 명령 처리가 안된다.

	return bFlag;
}

//ksj 20210105 : 일반 비절연 함수 분리
BOOL CCTSMonProView::Fun_IsolationEndNormal(int nCh)
{	
	if (m_nCurrentModuleID < 1)
	{
		AfxMessageBox(Fun_FindMsg("Fun_IsolationStart_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업할 모듈을 선택 하세요.");
		return FALSE;
	}

	SFT_DAQ_ISOLATION sDaqIsolation;
	ZeroMemory(&sDaqIsolation,sizeof(SFT_DAQ_ISOLATION));
	sDaqIsolation.bISO = 0; // 0: 비절연 실행,   1: 절연 실행
	sDaqIsolation.div_ch = nCh;
	sDaqIsolation.div_MainCh = 0; //미사용

	BOOL bFlag = GetDocument()->SetDAQIsolationDataToChannel(m_nCurrentModuleID,&sDaqIsolation); //ksj 20210104 : 채널단위로 전송. 기존SetDAQIsolationDataToModule에는 절연/비절연 커맨드가 모듈 단위 전송이었으나, 모듈단위로 전송하면 SBC 1번채널이 동작중이면 설비가 동작중이라고 명령 처리가 안된다.

	return bFlag; 
}

//ljb 2011222 이재복 //////////
CString CCTSMonProView::Fun_GetFunctionString(UINT nType, int nDivision)
{
	if (nDivision == 0) return "None [0]";	
	CString strItem;
	int i;
	//int j;
	if (nType == ID_CAN_TYPE)
	{
		for( int i=0; i < m_uiArryCanDivition.GetSize(); i++)
		{
			if (m_uiArryCanDivition.GetAt(i) == nDivision)
			{
				strItem.Format("%s [%d]", m_strArryCanName.GetAt(i), m_uiArryCanDivition.GetAt(i));
				return strItem;
			}
		}
		strItem.Format("Unknown [%d]", nDivision);
	}
	else if (nType == ID_AUX_TYPE)
	{
		for( int i=0; i < m_uiArryAuxDivition.GetSize(); i++)
		{
			if (m_uiArryAuxDivition.GetAt(i) == nDivision)
			{
				strItem.Format("%s [%d]", m_strArryAuxName.GetAt(i), m_uiArryAuxDivition.GetAt(i));
				return strItem;
			}
		}
		strItem.Format("Unknown [%d]", nDivision);
	}
	else if (nType == ID_AUX_THERMISTOR_TYPE)
	{
			if (nDivision == 1) strItem = "Th 1";
			if (nDivision == 2) strItem = "Th 2";
			if (nDivision == 3) strItem = "Th 3";
			if (nDivision == 4) strItem = "Th 4";
	}

	return strItem;
}

void CCTSMonProView::OnButEditor() 
{
	// Get Current Directory Path
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
	
	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));
	
	CString strPatternPath, strDataPath;
	strPatternPath.Format("%s\\CTSEditorPro.exe", path);
	GetDocument()->ExecuteProgram(strPatternPath, "", "", "CTSEditorPro");
}

void CCTSMonProView::OnButAnal() 
{
	ExeCTSAnal();	
}

void CCTSMonProView::OnButAllView() 
{
	SetModuleSelChanged(0);
	
} 

BOOL CCTSMonProView::SendChamberCmdToModule(CDWordArray * adwTargetCh, int nCmd, BOOL bSetFlag)
{
	CCTSMonProDoc *pDoc = GetDocument();
	CString strMsg;
	
	CWordArray chArray;
	DWORD dwData;
	CWaitCursor wait;
	int nPrevModule = 0, nModuleID, nChannelIndex;
	int nCurModuleID = 0;
	
	for(int s=0; s<adwTargetCh->GetSize(); s++)
	{
		dwData = adwTargetCh->GetAt(s);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		
		if(s == 0)	nPrevModule = nModuleID;		
		
		//모듈 별로 구별하여 전송한다.
		//if(nPrevModule != nModuleID || s == arTargetCh.GetSize()-1)
		if(nPrevModule != nModuleID)
		{
			nCurModuleID = nPrevModule;
			
			//전송 가능한 채널이 있으면 전송 
			if(chArray.GetSize() > 0)
			{
				if(nCmd == SFT_CMD_CHAMBER_USING)
				{
					if (pDoc->SendCommand(nCurModuleID, &chArray, nCmd, bSetFlag, 0))
					{
					}
					else
					{
						//AfxMessageBox(Fun_FindMsg("SendChamberCmdToModule_msg1","IDD_CTSMonPro_FORM"));
						//@ AfxMessageBox("pDoc->SendCommand() 전송 실패2");
						//strMsg.Format("pDoc->SendCommand() 전송 실패1");
						strMsg.Format(Fun_FindMsg("CTSMonProView_SendChamberCmdToModule_msg1","IDD_CTSMonPro_FORM"));//&&
						pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
					}
				}
			}
			else
			{
				strMsg.Format(Fun_FindMsg("SendChamberCmdToModule_msg2","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				//@ strMsg.Format("%s는 [%s]명령 전송가능한 채널이 없습니다.", pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
			}
			chArray.RemoveAll();
		}
		chArray.Add(nChannelIndex);
		nPrevModule = nModuleID;
		
		if(s == adwTargetCh->GetSize()-1)
		{
			nCurModuleID = 	nModuleID;
			
			//전송 가능한 채널이 있으면 전송 
			if(chArray.GetSize() > 0)
			{
				if(nCmd == SFT_CMD_CHAMBER_USING)
				{
					if (pDoc->SendCommand(nCurModuleID, &chArray, nCmd, bSetFlag, 0))
					{
					}
					else
					{
						//AfxMessageBox(Fun_FindMsg("SendChamberCmdToModule_msg3","IDD_CTSMonPro_FORM"));
						//@ AfxMessageBox("pDoc->SendCommand() 전송 실패3");
						//strMsg.Format("pDoc->SendCommand() 전송 실패3");
						strMsg.Format(Fun_FindMsg("CTSMonProView_SendChamberCmdToModule_msg2","IDD_CTSMonPro_FORM"));//&&
						pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
					}
				}
			}
			else
			{
				strMsg.Format(Fun_FindMsg("SendChamberCmdToModule_msg4","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				//@ strMsg.Format("%s는 [%s]명령 전송가능한 채널이 없습니다.", pDoc->GetModuleName(nCurModuleID), pDoc->GetCmdString(nCmd));
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
			}
			
		}
	}
	return TRUE;
}

void CCTSMonProView::OnCmdSetChamberUsing() 
{
	BOOL bRet;
	//1. 선택된 채널 중 Clear State 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdSetChamberUsing_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}
	
	//2. Check to user
	if(IDYES!=MessageBox(Fun_FindMsg("OnCmdSetChamberUsing_msg2","IDD_CTSMonPro_FORM"), 
		//@ if(IDYES!=MessageBox("챔버 연동 명령을 전송 하시겠습니까?", 
		Fun_FindMsg("OnCmdSetChamberUsing_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@ "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
		return;
	
	//3. Send Command
	SendChamberCmdToModule(&m_adwTargetChArray, SFT_CMD_CHAMBER_USING,TRUE);
	//////////////////////////////////////////////////////////////	
}

void CCTSMonProView::OnCmdReleaseChamberUsing() 
{
	//1. 선택된 채널 중 Clear State 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdReleaseChamberUsing_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("전송 가능한 채널이 없습니다.");
		return;
	}
	
	//2. Check to user
	if(IDYES!=MessageBox(Fun_FindMsg("OnCmdReleaseChamberUsing_msg2","IDD_CTSMonPro_FORM"), 
		//@ if(IDYES!=MessageBox("초기화를 실행하시겠습니까?", 
		Fun_FindMsg("OnCmdReleaseChamberUsing_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
		//@  "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
		return;
	
	//3. Send Command
	SendChamberCmdToModule(&m_adwTargetChArray, SFT_CMD_CHAMBER_USING,FALSE);
	//////////////////////////////////////////////////////////////		
}

void CCTSMonProView::OnCmdChamberContinue() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	CDWordArray		aPauseCh;
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;
	
	UINT nModuleID = 0;
	UINT nChIndex = 0;
	//////////////////////////////////////////////////////////////////////////
	//m_awTargetChArray을 구함
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdChamberContinue_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}
	//////////////////////////////////////////////////////////////////////////
	int nSelCount = m_adwTargetChArray.GetSize();
	
	CPtrArray	aPtrSelCh;
	CString strTemp;
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		
		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);
	}
	
	if(aPtrSelCh.GetSize() <= 0)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdChamberContinue_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}
	
	
	CDWordArray awChArray;
	nSelCount = aPtrSelCh.GetSize();
	int tCh;
	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);
		
		if(pChannel == NULL)
		{
			TRACE(Fun_FindMsg("OnCmdChamberContinue_msg3","IDD_CTSMonPro_FORM"));
			//@ TRACE("pChannel == NULL -> 이상\n");
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);
		
		awChArray.Add(MAKELONG(nChIndex, nModuleID));
	}
	
	if(SendCmdToModule(awChArray, SFT_CMD_CHAMBER_CONTINUE) == FALSE)	
	{
		AfxMessageBox(Fun_FindMsg("OnCmdChamberContinue_msg4","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("명령 CHAMBER CONTINUE 처리 실패");
		return;
	}		
}

void CCTSMonProView::OnCmdCycleContinue() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	CDWordArray		aPauseCh;
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;
	
	UINT nModuleID = 0;
	UINT nChIndex = 0;
	//////////////////////////////////////////////////////////////////////////
	//m_awTargetChArray을 구함
	if(UpdateSelectedChList(SFT_CMD_CONTINUE)  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdChamberContinue_msg5","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}
	//////////////////////////////////////////////////////////////////////////
	int nSelCount = m_adwTargetChArray.GetSize();	
	
	CPtrArray	aPtrSelCh;
	CString strTemp;
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("channel code : %d\n",pChannel->GetCellCode());
		//ljb 수정 해야 함. 챔버연동과 동시 설정 하면 챔버연동 상태로 올라옴.
		if (pChannel->GetCellCode() != PS_STATE_PAUSE_CYCLE_END)		//88
		{
			
			strTemp.Format(Fun_FindMsg("OnCmdCycleContinue_msg1","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(pMD->GetModuleID()),nChIndex+1);
			//@ strTemp.Format(" %s 에 Channel %d 는 Cycler 완료후 대기 상태가 아닙니다.", pDoc->GetModuleName(pMD->GetModuleID()),nChIndex+1);
			AfxMessageBox(strTemp);
			return;
		}
		
		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);
	}
	
	if(aPtrSelCh.GetSize() <= 0)
	{
		AfxMessageBox(Fun_FindMsg("OnCmdCycleContinue_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}
	
	
	CDWordArray awChArray;
	nSelCount = aPtrSelCh.GetSize();
	int tCh;
	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);
		
		if(pChannel == NULL)
		{
			TRACE(Fun_FindMsg("OnCmdCycleContinue_msg3","IDD_CTSMonPro_FORM"));
			//@ TRACE("pChannel == NULL -> 이상\n");
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);
		
		awChArray.Add(MAKELONG(nChIndex, nModuleID));
	}
	
	if(SendCmdToModule(awChArray, SFT_CMD_CYCLE_PAUSE_CONTINUE) == FALSE)	
	{
		AfxMessageBox(Fun_FindMsg("OnCmdCycleContinue_msg4","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("Cycle 완료 후 Continue 명령 -> 처리 실패");
		return;
	}			
}

//DEL void CCTSMonProView::OnCmdChamberRun() 
//DEL {
//DEL 	// TODO: Add your command handler code here
//DEL 	CCTSMonProDoc * pDoc = GetDocument();
//DEL 	CString strTemp;
//DEL 
//DEL 	if (pDoc->m_ctrlOven.GetUseOven(0))
//DEL 	{
//DEL 		if (pDoc->m_ctrlOven.SendRunCmd(0,0) == FALSE)
//DEL 		{
//DEL 			strTemp = "챔버 동작 실행 명령 실패!!!";
//DEL 			AfxMessageBox(strTemp);
//DEL 		}
//DEL 	}	
//DEL }

//DEL void CCTSMonProView::OnCmdChamberStop() 
//DEL {
//DEL 	// TODO: Add your command handler code here
//DEL 	CCTSMonProDoc * pDoc = GetDocument();
//DEL 	CString strTemp;
//DEL 	
//DEL 	if (pDoc->m_ctrlOven.GetUseOven(0))
//DEL 	{
//DEL 		if (pDoc->m_ctrlOven.SendStopCmd(0,0) == FALSE)
//DEL 		{
//DEL 			strTemp = "챔버 동작 중단 명령 실패(REST)!!!";
//DEL 			AfxMessageBox(strTemp);
//DEL 		}
//DEL 	}	
//DEL }

//DEL void CCTSMonProView::OnCmdChamberGetTemp() 
//DEL {
//DEL 	CCTSMonProDoc * pDoc = GetDocument();
//DEL 	CString strTemp;
//DEL 	strTemp.Empty();
//DEL 	float fSetOvenValue;
//DEL 	fSetOvenValue = 0;
//DEL 	
//DEL 	if (pDoc->m_ctrlOven.GetUseOven(0))
//DEL 	{
//DEL 		if (pDoc->m_ctrlOven.GetRunWorkMode(0) == FALSE)		// FALSE = PROG 모드, TRUE = FIX 모드
//DEL 		{
//DEL 			//Prog 모드에서 설정 값 읽기
//DEL 			fSetOvenValue = pDoc->m_ctrlOven.GetProgTemperature(0); // Prog 모드 온도 목표치
//DEL 			strTemp.Format("챔버 Prog 모드 -> 목표 설정값 (%.1f)",fSetOvenValue);
//DEL 		}
//DEL 		else
//DEL 		{
//DEL 			//Fix 모드로에서 설정 값 일기
//DEL 			fSetOvenValue = pDoc->m_ctrlOven.GetRefTemperature(0); // 오븐의 현재 온도
//DEL 			strTemp.Format("챔버 Fix 모드 -> 목표 설정값 (%.1f)",fSetOvenValue);
//DEL 		}
//DEL 	}
//DEL 	if (strTemp.IsEmpty() == FALSE) AfxMessageBox(strTemp);	
//DEL }

//DEL void CCTSMonProView::OnCmdChamberModeChange() 
//DEL {
//DEL 	CCTSMonProDoc * pDoc = GetDocument();
//DEL 	CString strTemp;
//DEL 	strTemp.Empty();
//DEL 	
//DEL 	if (pDoc->m_ctrlOven.GetUseOven(0))
//DEL 	{
//DEL 		//2.현재 챔버 모드 확인
//DEL 		if (pDoc->m_ctrlOven.GetRunWorkMode(0))		// FALSE = PROG 모드, TRUE = FIX 모드
//DEL 		{
//DEL 			//챔버 Fix 모드 ->	Prog Mode 변환
//DEL 			if (pDoc->m_ctrlOven.SetFixMode(0,0,FALSE) == FALSE)
//DEL 			{
//DEL 				strTemp = "챔버 PROG 모드로 변환 실패(REST)!!!";
//DEL 			}
//DEL 		}
//DEL 		else
//DEL 		{
//DEL 			//챔버 Prog 모드 ->	Fix Mode 변환
//DEL 			if (pDoc->m_ctrlOven.SetFixMode(0,0,TRUE) == FALSE)
//DEL 			{
//DEL 				strTemp = "챔버 Fix 모드로 변환 실패(REST)!!!";
//DEL 			}
//DEL 		}
//DEL 	}
//DEL 	if (strTemp.IsEmpty() == FALSE) AfxMessageBox(strTemp);
//DEL }

void CCTSMonProView::OnButStopBuzzer() 
{
	//m_awTargetChArray을 구함
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnButStopBuzzer_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	//////////////////////////////////////////////////////////////////////////
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		//		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	continue;
		if (pMD->GetState() == PS_STATE_LINE_OFF) continue;
		if (nPreModuleID != nModuleID)
		{
			pMD->SendCommand(SFT_CMD_STOP_BUZZER,NULL,0);
			nPreModuleID = nModuleID;
		}
	}		
// 	CCyclerModule *pMD;
// 	int nModuleID;
// 	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 	while(pos)
// 	{
// 		int item = m_wndModuleList.GetNextSelectedItem(pos);
// 		nModuleID  = m_wndModuleList.GetItemData(item);
// 		pMD = GetDocument()->GetModuleInfo(nModuleID);
// 		if(pMD)
// 		{
// 			if(pMD->GetState() == PS_STATE_LINE_OFF) continue;
// 			pMD->SendCommand(SFT_CMD_STOP_BUZZER,NULL,0);
// 		}
// 	}
}

void CCTSMonProView::OnButResetAlarm() 
{
	//m_awTargetChArray을 구함
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnButResetAlarm_msg","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 
		
	}
	//////////////////////////////////////////////////////////////////////////
	CCyclerModule * pMD;		
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		//		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	continue;
		if (pMD->GetState() == PS_STATE_LINE_OFF) continue;
		if (nPreModuleID != nModuleID)
		{
			pMD->SendCommand(SFT_CMD_RESET_ALARM,NULL,0);
			nPreModuleID = nModuleID;
		}
	}	

// 	CCyclerModule *pMD;
// 	int nModuleID;
// 	POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
// 	while(pos)
// 	{
// 		int item = m_wndModuleList.GetNextSelectedItem(pos);
// 		nModuleID  = m_wndModuleList.GetItemData(item);
// 		pMD = GetDocument()->GetModuleInfo(nModuleID);
// 		if(pMD)
// 		{
// 			if(pMD->GetState() == PS_STATE_LINE_OFF) continue;
// 			pMD->SendCommand(SFT_CMD_RESET_ALARM,NULL,0);
// 		}
// 	}	
}

BOOL CCTSMonProView::GetSelModuleChannel(int &nModuleID, int &nChannelIndex)
{
	DWORD dwData = 0;
	if(m_nCurTabIndex == TAB_CH_LIST)
	{
		POSITION pos1 = m_wndChannelList.GetFirstSelectedItemPosition();
		int nSel = m_wndChannelList.GetNextSelectedItem(pos1);
		if(nSel >=  0)
		{
			dwData = m_wndChannelList.GetItemData(nSel);
			nModuleID = HIWORD(dwData);
			nChannelIndex = LOWORD(dwData);
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		CCellList *cellList = m_Grid.GetSelectedCellList();
		POSITION	pos = cellList->GetHeadPosition();
		if(pos == NULL)		return FALSE;
		CCellID rowcol = cellList->GetNext(pos);
		dwData = m_Grid.GetItemData(rowcol.row, rowcol.col);
	}
	nModuleID = HIWORD(dwData);
	nChannelIndex = LOWORD(dwData);
	
	return TRUE;
}


void CCTSMonProView::OnButSetComm() 
{
	// TODO: Add your control notification handler code here
	if(m_wndMeterDlg == NULL)
	{
		m_wndMeterDlg = new CMeterSetDlg(this);
		m_wndMeterDlg->Create(IDD_METER_CONTIG_DIALOG, this);
		m_wndMeterDlg->SetDocument(GetDocument());
	}
	m_wndMeterDlg->ShowWindow(SW_SHOW);	
}

void CCTSMonProView::OnButJigTag() 
{
	// TODO: Add your control notification handler code here
	//ljb MainFram Menu에 등록 하고 아래 주석을 복사하여 사용함
	//바코드로 JIG 자동 인식 하는 기능
	
	// 	CJigIDRegDlg *pDlg = new CJigIDRegDlg;
	// 	pDlg->m_pDoc = (CCTSMonProDoc *)GetActiveDocument();
	// 	
	// 	pDlg->DoModal();
	// 	
// 	delete pDlg;	
}

void CCTSMonProView::OnBtnViewRealgraph() 
{
// 	float fTemp;
// 	fTemp = 1.35;
// 
// 	CString strTemp;
// 	CString strFormat;
// // 	strFormat.Format("|%%10.2f|");
// // 	strTemp.Format(strFormat,fTemp);
// 	strTemp.Format("|%7.2f|",fTemp);
// 	AfxMessageBox(strTemp);

// 	int fInt;
// 	fInt=12345;
// 	CString strTemp2;
// 	strTemp2.Format("|%010d|",fInt);
// 	AfxMessageBox(strTemp2);


	ExeCTSRealGraphic();	
}

// void CCTSMonProView::OnRealTimeGraphView() 
// {
// 	ExeCTSRealGraphic();	//ljb 201159 실시간 그래프 
// }

void CCTSMonProView::OnUpdateOvenWnd2(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", FALSE);
	if(bUseOven  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	//Added by KBH 20080821
	if(m_pDetailOvenDlg2 && m_pDetailOvenDlg2->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}	
}

void CCTSMonProView::OnOvenWnd2() 
{
	// TODO: Add your command handler code here
	//ljb 20111117
	if(m_pDetailOvenDlg2 == NULL)
	{
		m_pDetailOvenDlg2 = new CDetailOvenDlg(GetDocument());
		//m_pDetailOvenDlg2->Create(IDD_DETAIL_OVEN_DLG, this);
		m_pDetailOvenDlg2->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
		IDD_DETAIL_OVEN_DLG, this);//&&
		m_pDetailOvenDlg2->ShowWindow(SW_SHOW);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", TRUE);
	}
	else
	{
		if(m_pDetailOvenDlg2->IsWindowVisible())
		{
			m_pDetailOvenDlg2->ShowWindow(SW_HIDE);
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", FALSE);
		}
		else
		{
			m_pDetailOvenDlg2->ShowWindow(SW_SHOW);
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", TRUE);
		}
	}	
}

void CCTSMonProView::OnBtnDaqIsolationStart() 
{
	// TODO: Add your control notification handler code here
	CString strMsg;
	//strMsg.Format("절연 상태로 전환 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	strMsg.Format("절연 상태로 전환 하시겠습니까?");
	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	
	//Fun_IsolationStart(1,0);
	Fun_IsolationStartNormal(1); //ksj 20210105 : 일반 절연/비절연 함수 분리
	
}

void CCTSMonProView::OnBtnDaqIsolationEnd() 
{
	// TODO: Add your control notification handler code here
	
 	CString strMsg;
 	//strMsg.Format("비절연 상태로 전환 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	strMsg.Format("비절연 상태로 전환 하시겠습니까?");
	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) {
 		return;
	}
 	//Fun_IsolationEnd(1,0);
	Fun_IsolationEndNormal(1); //ksj 20210105 : 일반 절연/비절연 함수 분리	
}
//lmh 20120113 병렬모드 설정
void CCTSMonProView::OnParallelCon() 
{
	// TODO: Add your command handler code here
	//SetParallelData();
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	CCyclerModule * pMD;
	CCyclerChannel * pChInfo;
	//SFT_MD_PARALLEL_DATA * pParaData;


	POSITION pos1 = m_wndChannelList.GetFirstSelectedItemPosition();
	int nSel = m_wndChannelList.GetSelectedCount();
	if(nSel % 2 != 0)
	{
		AfxMessageBox(Fun_FindMsg("OnparallelCon_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("짝수개의 채널을 선택하십시요.");
		return;
	}
	if(UpdateSelectedChList()  == FALSE) //ksj 20171123 : 선택 채널 업데이트 추가
	{
		//$1013 AfxMessageBox("선택된 채널이 없습니다.");
		AfxMessageBox(Fun_FindMsg("OnparallelCon_msg4","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	nSel = m_wndChannelList.GetNextSelectedItem(pos1);
	//WORD dwData;
	DWORD dwData; //ksj 20171123 : 데이터 형 오류 수정
	int nModuleID = -1;
	int nChannelIndex = 0;
	CWordArray aw;
	for(int ch = 0; ch <m_adwTargetChArray.GetSize(); ch++)
	{
		dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		
		pMD = pDoc->GetModuleInfo(nModuleID);//모듈 정보를 가져온다.
		if(pMD)
		{
			pChInfo = pMD->GetChannelInfo(nChannelIndex);//채널 정보를 가져온다.
			if(pChInfo)
			{
				if((pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE))
				{
					AfxMessageBox(Fun_FindMsg("OnParallelCon_msg2","IDD_CTSMonPro_FORM"));
					//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					return;
				}
				if(!pChInfo->IsParallel()) //병렬 연결 유무 검사
				{
					aw.Add(nChannelIndex + 1);//채널 정보는 +1 해서 넣는다 0번채널 -> 1번채널
				}
			}
			
		}
		nSel = m_wndChannelList.GetNextSelectedItem(pos1);
	}
	if(aw.GetSize() == 0 || aw.GetSize() != m_wndChannelList.GetSelectedCount()) 
	{
		AfxMessageBox(Fun_FindMsg("OnParallelCon_msg3","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("채널 정보가 하나도 없습니다.!!");
		return; //채널정보가 없다면 종료
	}
	//또는 선택된 갯수와 병렬로 안된 채널의 갯수가 맞지 않으면 종료

	//yulee 20190315
	//한 채널이라도 동작 중에는 메뉴의 병렬기능 사용
	BOOL bOneChRunPause;
	bOneChRunPause = FALSE;
	for(int i = 0 ; i < pMD->GetTotalChannel(); i++)
	{
		CCyclerChannel * pChInfo2 =  pMD->GetChannelInfo(i);
		if((pChInfo2->GetState() == PS_STATE_RUN) || (pChInfo2->GetState() == PS_STATE_PAUSE))
		{
			bOneChRunPause = TRUE;
			break;
		}
	}
	if(bOneChRunPause == TRUE)
	{
		AfxMessageBox("At least one channel is working. Please use menu function(Menu->Setting->Merge mode setting).");
		return;
	}

	SFT_MD_PARALLEL_DATA parallelData;
	ZeroMemory(&parallelData, sizeof(SFT_MD_PARALLEL_DATA));
	parallelData.chNoMaster = aw.GetAt(0);
	parallelData.chNoSlave[0] = aw.GetAt(1);

	if(aw.GetSize() == _SFT_MAX_PARALLEL_CHANNEL_4)
	{
		parallelData.chNoSlave[1] = aw.GetAt(2);
		parallelData.chNoSlave[2] = aw.GetAt(3);
	}
	parallelData.bParallel = 1;					//병렬 모드로 셋팅
	
	pMD->SetParallelData(parallelData);

	pDoc->SendParallelDatatoModule(nModuleID) ;

	
}

void CCTSMonProView::OnUpdateParallelCon(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	int iCnt = m_wndChannelList.GetSelectedCount();
	if(iCnt > 1) pCmdUI->Enable(TRUE);
	else pCmdUI->Enable(FALSE);

	//yulee 20190315
	//한 채널이라도 동작 중에는 메뉴의 병렬기능 사용
	BOOL bOneChRunPause;
	bOneChRunPause = FALSE;
	CCyclerModule * pMD;
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	int nModuleID = 1;
	pMD = pDoc->GetModuleInfo(nModuleID);//모듈 정보를 가져온다.
	for(int i = 0 ; i < pMD->GetTotalChannel(); i++)
	{
		CCyclerChannel * pChInfo2 =  pMD->GetChannelInfo(i);
		if((pChInfo2->GetState() == PS_STATE_RUN) || (pChInfo2->GetState() == PS_STATE_PAUSE))
		{
			bOneChRunPause = TRUE;
			break;
		}
	}
	if(bOneChRunPause == TRUE)
	{
		//AfxMessageBox("At least one channel is working. Please use menu function(Menu->Setting->Merge mode setting).");
		pCmdUI->Enable(FALSE);
		return;
	}
}

void CCTSMonProView::OnParallelDiscon() //lmh 20120113 병렬모드 해제
{
	// TODO: Add your command handler code here
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	CCyclerModule * pMD;
	CCyclerChannel * pChInfo;
	//SFT_MD_PARALLEL_DATA * pParaData;
	
	POSITION pos1 = m_wndChannelList.GetFirstSelectedItemPosition();
	int nSel = m_wndChannelList.GetSelectedCount();
	if(nSel % 2 != 0)
	{
		AfxMessageBox(Fun_FindMsg("OnParallelDiscon_msg1","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("짝수개의 채널을 선택하십시요.");
		return;
	}
	if(UpdateSelectedChList()  == FALSE) //ksj 20171123 : 선택 채널 업데이트 추가
	{
		// AfxMessageBox("선택된 채널이 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("OnParallelDiscon_msg3","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	nSel = m_wndChannelList.GetNextSelectedItem(pos1);
	//WORD dwData;
	DWORD dwData; //ksj 20171123 : 데이터 형 버그 수정
	int nModuleID = 1;
	int nChannelIndex = 0;
	CWordArray aw;
	for(int ch = 0; ch <m_adwTargetChArray.GetSize(); ch++)
	{
		dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		
		//pMD = pDoc->GetModuleInfo(pDoc->GetModuleID(nModuleID));//모듈 정보를 가져온다.
		pMD = pDoc->GetModuleInfo(nModuleID);//모듈 정보를 가져온다. //ksj 20171123
		if(pMD)
		{
			pChInfo = pMD->GetChannelInfo(nChannelIndex);//채널 정보를 가져온다.
			if(pChInfo)
			{
				if((pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE))
				{
					AfxMessageBox(Fun_FindMsg("OnParallelDiscon_msg2","IDD_CTSMonPro_FORM"));
					//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					return;
				}
				if(pChInfo->IsParallel()) //병렬 연결 유무 검사
				{
					aw.Add(nChannelIndex + 1);//채널 정보는 +1 해서 넣는다 0번채널 -> 1번채널
				}
			}
			
		}
		nSel = m_wndChannelList.GetNextSelectedItem(pos1);
	}
	if(aw.GetSize() == 0 || aw.GetSize() != m_wndChannelList.GetSelectedCount()) return; //채널정보가 없다면 종료
	//또는 선택된 갯수와 병렬로 안된 채널의 갯수가 맞지 않으면 종료

	
	BOOL bOneChRunPause;
	bOneChRunPause = FALSE;
	for(int i = 0 ; i < pMD->GetTotalChannel(); i++)
	{
		CCyclerChannel * pChInfo2 =  pMD->GetChannelInfo(i);
		if((pChInfo2->GetState() == PS_STATE_RUN) || (pChInfo2->GetState() == PS_STATE_PAUSE))
		{
			bOneChRunPause = TRUE;
			break;
		}
	}
	if(bOneChRunPause == TRUE)
	{
		AfxMessageBox("At least one channel is working. Please use menu function(Menu->Setting->Merge mode setting).");
		return;
	}

	pMD->SetAllParallelReset();	

	
	pDoc->SendParallelDatatoModule(nModuleID) ;
	
}

void CCTSMonProView::OnUpdateParallelDiscon(CCmdUI* pCmdUI) 
{
	//yulee 20190315
	//한 채널이라도 동작 중에는 메뉴의 병렬기능 사용
	BOOL bOneChRunPause;
	bOneChRunPause = FALSE;
	CCyclerModule * pMD;
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	int nModuleID = 1;
	pMD = pDoc->GetModuleInfo(nModuleID);//모듈 정보를 가져온다.
	for(int i = 0 ; i < pMD->GetTotalChannel(); i++)
	{
		CCyclerChannel * pChInfo2 =  pMD->GetChannelInfo(i);
		if((pChInfo2->GetState() == PS_STATE_RUN) || (pChInfo2->GetState() == PS_STATE_PAUSE))
		{
			bOneChRunPause = TRUE;
			break;
		}
	}
	if(bOneChRunPause == TRUE)
	{
		//AfxMessageBox("At least one channel is working. Please use menu function(Menu->Setting->Merge mode setting).");
		pCmdUI->Enable(FALSE);
		return;
	}


	// TODO: Add your command update UI handler code here

	//CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	//CCyclerModule * pMD;
	CCyclerChannel * pChInfo;
	//SFT_MD_PARALLEL_DATA * pParaData;
	BOOL bEnable = FALSE;
	
	POSITION pos1 = m_wndChannelList.GetFirstSelectedItemPosition();
	int nSel = m_wndChannelList.GetNextSelectedItem(pos1);
	WORD dwData;
	//int nModuleID;
	int nChannelIndex;

	if(UpdateSelectedChList()  == FALSE) //ksj 20171123 : 선택 채널 업데이트 추가
	{
		//AfxMessageBox("선택된 채널이 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("OnUpdateParallelDiscon_msg1","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	
	for(int ch = 0; ch <m_adwTargetChArray.GetSize(); ch++)
	{
		dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		
		pMD = pDoc->GetModuleInfo(pDoc->GetModuleID(nModuleID));//모듈 정보를 가져온다.
		if(pMD)
		{
			pChInfo = pMD->GetChannelInfo(nChannelIndex);//채널 정보를 가져온다.
			if(pChInfo)
			{
				if(pChInfo->IsParallel()) //병렬이라면
					bEnable = TRUE;//wile문을 도는동안 한개라도 병렬이라면
			}
			
		}
		

		//병렬 연결 유무 검사
		nSel = m_wndChannelList.GetNextSelectedItem(pos1);
	}

	pCmdUI->Enable(bEnable);
	
}


//DEL void CCTSMonProView::OnCmdIsolationStart() 
//DEL {
//DEL 	// TODO: Add your command handler code here
//DEL 	CString strMsg;
//DEL 	strMsg.Format("절연 상태로 전환 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
//DEL 	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
//DEL 		return;
//DEL 	Fun_IsolationStart();
//DEL 	
//DEL }

void CCTSMonProView::OnCmdIsolationEnd() 
{
	// TODO: Add your command handler code here
// 	CString strMsg;
// 	strMsg.Format("비절연 상태로 전환 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
// 	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
// 		return;
// 	Fun_IsolationEnd();
	
}

void CCTSMonProView::OnWorkPneManage() 
{

	//CLoginManagerDlg LoginDlg;
#ifndef _DEBUG
	CLoginManagerDlg LoginDlg(PMS_SUPERVISOR, TRUE); //ksj 20200203 : PNE 전용 로그인 권한
	if(LoginDlg.DoModal() != IDOK) return;
#endif

	CRegeditManage dlgReg;
	dlgReg.DoModal();	
}

void CCTSMonProView::OnUartConfig() 
{
	if (UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("OnUartConfig_msg1","IDD_CTSMonPro_FORM")); //&&
		return; 		
	}
	
	// CSettingDlg 에서 복사
	//////////////////////////////////////////////////////////////////////////
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1 채널만 선택 하세요. ");
		AfxMessageBox(Fun_FindMsg("OnUartConfig_msg2","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		//		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	return;
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnUartConfig_msg3","IDD_CTSMonPro_FORM")); //&&
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	/////////////////////////////////////
	
	CUartConvDlg dlg(GetDocument()) ;
	dlg.nSelCh = nChIndex;
	dlg.nSelModule = nModuleID;
	
	m_pUartConvDlg = &dlg ;
	dlg.DoModal();	
	
	m_pUartConvDlg = NULL ;
}

void CCTSMonProView::OnLinConfig() 
{
	if (UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("OnLinConfig_msg1","IDD_CTSMonPro_FORM")); //&&
		return; 		
	}
	
	// CSettingDlg 에서 복사
	//////////////////////////////////////////////////////////////////////////
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1 채널만 선택 하세요. ");
		AfxMessageBox(Fun_FindMsg("OnLinConfig_msg2","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		//		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	return;
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnLinConfig_msg3","IDD_CTSMonPro_FORM")); //&&
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	/////////////////////////////////////
	
	CLinConvDlg dlg(GetDocument()) ;
	dlg.nSelCh = nChIndex;
	dlg.nSelModule = nModuleID;
	
	m_pLinConvDlg = &dlg ;
	dlg.DoModal();	
	
	m_pLinConvDlg = NULL ;
}

void CCTSMonProView::OnModbusConfig() 
{
	if (UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("OnModbusConfig_msg1","IDD_CTSMonPro_FORM"));
		return; 		
	}
	
	// CSettingDlg 에서 복사
	//////////////////////////////////////////////////////////////////////////
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1 채널만 선택 하세요. ");
		AfxMessageBox(Fun_FindMsg("OnModbusConfig_msg2","IDD_CTSMonPro_FORM"));
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		//		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	return;
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnModbusConfig_msg3","IDD_CTSMonPro_FORM"));
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	/////////////////////////////////////
	
	CModbusConvDlg dlg(GetDocument()) ;
	dlg.nSelCh = nChIndex;
	dlg.nSelModule = nModuleID;
	
	m_pModbusConvDlg = &dlg ;
	dlg.DoModal();	
	
	m_pModbusConvDlg = NULL ;
}

void CCTSMonProView::OnButReconnectLoad1() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	UpdateData();
	m_ctrlLoaderState_1 = "";
	UpdateData(FALSE);
	pDoc->m_ctrlLoader1.Fun_ReConnect(1);
	
}

void CCTSMonProView::OnButReconnectLoad2() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	
	UpdateData();
	m_ctrlLoaderState_2 = "";
	UpdateData(FALSE);
	pDoc->m_ctrlLoader2.Fun_ReConnect(2);	
	//pDoc->m_nSendingModeComm_ch1 = ID_LOAD_ON;
	
}

void CCTSMonProView::OnCmdCham1Set() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	pDoc->m_ChamberCtrl.m_ctrlOven1.SetTemperature(0,0, 25);
}

void CCTSMonProView::OnCmdCham2Set() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	pDoc->m_ChamberCtrl.m_ctrlOven2.SetTemperature(0,0, 25);
}

void CCTSMonProView::OnButMuxChoice() 
{
	SetMuxChoice(1);
	m_nFlagMuxResv = 2;
// 
// 		// TODO: Add your control notification handler code here
// 		UpdateData();
// 		CString strTemp;
// 		CString strMsg;
// 		int nChoice=0;
// 		
// 		GetDlgItem(IDC_CBO_MUX)->GetWindowText(strTemp);
// 		if (strTemp == "ALL OFF")
// 		{
// 			nChoice = 0;
// 		}
// 		else if (strTemp == "MUX A")
// 		{
// 			nChoice = 1;
// 		}
// 		else if (strTemp == "MUX B")
// 		{
// 			nChoice = 2;
// 		}
// 		else
// 		{
// 			AfxMessageBox("변경할 상태를 선택 하세요.");
// 			return;
// 		}
// 		if (m_nCurrentModuleID < 1)
// 		{
// 			AfxMessageBox("작업할 모듈을 선택 하세요.");
// 			return;
// 		}
// 	
// 	//	m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
// 	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
// 	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
// 	{
// 		AfxMessageBox("동작 중인 채널이 있으면 변경할 수 없습니다.");
// 		return ;
// 	}
// 	CCTSMonProDoc *pDoc = GetDocument();
// 	CCyclerModule	*pMD;
// 	CCyclerChannel	*pChannel;
// 	UINT nModuleID = 0;
// 	UINT nOldModuleID = 0;
// 	UINT nChIndex = 0;
// 	int ch;
// 	int nSelCount = m_adwTargetChArray.GetSize();
// 	
// 	for(ch = 0; ch<nSelCount; ch++)
// 	{
// 		DWORD dwData = m_adwTargetChArray.GetAt(ch);
// 		nModuleID = HIWORD(dwData);
// 		nChIndex = LOWORD(dwData);
// 		pMD = pDoc->GetModuleInfo(nModuleID);
// 		pChannel = pMD->GetChannelInfo(nChIndex);
// 		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
// 		
// 		if (pChannel->GetState() == PS_STATE_RUN)
// 		{
// 			strTemp.Format("Module : %d, Ch %d 은 동작 중 입니다. MUX변경을 할 수 없습니다.",nModuleID,nChIndex+1);
// 			AfxMessageBox(strTemp);
// 			return;
// 		}
// 		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
// 	}
// 	
// 	strMsg.Format("%s 명령을 전송 하시겠습니까?", strTemp);
// 	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) {
// 		return;
// 	}
// 	
// 	CDWordArray adwChArray;
// 	//	int nSelCount = m_adwTargetChArray.GetSize();
// 	adwChArray.RemoveAll();
// 	for(ch = 0; ch<nSelCount; ch++)
// 	{
// 		DWORD dwData = m_adwTargetChArray.GetAt(ch);
// 		nModuleID = HIWORD(dwData);
// 		nChIndex = LOWORD(dwData);
// 		if (nModuleID != nOldModuleID)
// 		{
// 			if (adwChArray.GetSize() > 0)
// 			{
// 				Fun_MuxChangeStart("MUX", nChoice,&adwChArray);
// 			}
// 			nOldModuleID = nModuleID;
// 			adwChArray.RemoveAll();
// 		}
// 		adwChArray.Add(MAKELONG(nChIndex, nModuleID));
// 	}
// 	if (adwChArray.GetSize() > 0)
// 	{
// 		Fun_MuxChangeStart("MUX", nChoice,&adwChArray);
// 	}
	
}

BOOL CCTSMonProView::SetMuxChoice(int nType, int ResvMuxValue, CString ResvAdwTargetChArray) //yulee 20181025
{

	CString strTemp;
	CString strMsg;
	int nChoice=0;
	if(nType == 1)
	{
		// TODO: Add your control notification handler code here
		UpdateData();

		
		GetDlgItem(IDC_CBO_MUX)->GetWindowText(strTemp);
		if (strTemp == "ALL OFF")
		{
			nChoice = 0;
		}
		else if (strTemp == "MUX A")
		{
			nChoice = 1;
		}
		else if (strTemp == "MUX B")
		{
			nChoice = 2;
		}
		else
		{
			//AfxMessageBox("변경할 상태를 선택 하세요.");
			AfxMessageBox(Fun_FindMsg("SetMuxChoice_msg1","IDD_CTSMonPro_FORM")); //&&
			return 0;
		}
		if (m_nCurrentModuleID < 1)
		{
			//AfxMessageBox("작업할 모듈을 선택 하세요.");
			AfxMessageBox(Fun_FindMsg("SetMuxChoice_msg2","IDD_CTSMonPro_FORM")); //&&
			return 0;
		}
	}
	else if(nType == 2)
	{
		nChoice = ResvMuxValue;
	}

		//	m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
		//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
		if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
		{
			// AfxMessageBox("동작 중인 채널이 있으면 변경할 수 없습니다.");//$1013
			AfxMessageBox(Fun_FindMsg("SetMuxChoice_msg3","IDD_CTSMonPro_FORM")); //&&
			return 0;
		}
		CCTSMonProDoc *pDoc = GetDocument();
		CCyclerModule	*pMD;
		CCyclerChannel	*pChannel;
		UINT nModuleID = 0;
		UINT nOldModuleID = 0;
		UINT nChIndex = 0;
		int ch;
		int nSelCount = m_adwTargetChArray.GetSize();
		
		if(nType == 1)
		{
			for(ch = 0; ch<nSelCount; ch++)
			{	
				
				DWORD dwData = m_adwTargetChArray.GetAt(ch);
				nModuleID = HIWORD(dwData);
				nChIndex = LOWORD(dwData);
				pMD = pDoc->GetModuleInfo(nModuleID);
				pChannel = pMD->GetChannelInfo(nChIndex);
				TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
				
				if (pChannel->GetState() == PS_STATE_RUN)
				{
					//strTemp.Format("Module : %d, Ch %d 은 동작 중 입니다. MUX변경을 할 수 없습니다.",nModuleID,nChIndex+1);
					strTemp.Format(Fun_FindMsg("SetMuxChoice_msg4","IDD_CTSMonPro_FORM"),nModuleID,nChIndex+1); //&&
					AfxMessageBox(strTemp);
					return 0;
				}
				TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
			}


			//strMsg.Format("%s 명령을 전송 하시겠습니까?", strTemp);//$1013
			strMsg.Format(Fun_FindMsg("SetMuxChoice_msg5","IDD_CTSMonPro_FORM"), strTemp);//&&
			
			//if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) {
			if(IDYES != MessageBox(strMsg, Fun_FindMsg("SetMuxChoice_msg6","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) { //&&
				return 0;
			}
			
			CDWordArray adwChArray;
			//	int nSelCount = m_adwTargetChArray.GetSize();
			adwChArray.RemoveAll();
			for(ch = 0; ch<nSelCount; ch++)
			{
				DWORD dwData = m_adwTargetChArray.GetAt(ch);
				nModuleID = HIWORD(dwData);
				nChIndex = LOWORD(dwData);
				if (nModuleID != nOldModuleID)
				{
					if (adwChArray.GetSize() > 0)
					{
						Fun_MuxChangeStart("MUX", nChoice,&adwChArray);
					}
					nOldModuleID = nModuleID;
					adwChArray.RemoveAll();
				}
				adwChArray.Add(MAKELONG(nChIndex, nModuleID));
			}
			if (adwChArray.GetSize() > 0)
			{
				Fun_MuxChangeStart("MUX", nChoice,&adwChArray);
				return 1;
			}
		}
		else if(nType == 2)
		{
			DWORD dwData = atof(ResvAdwTargetChArray);
			nModuleID = HIWORD(dwData);
			nChIndex = LOWORD(dwData);
			pMD = pDoc->GetModuleInfo(nModuleID);
			pChannel = pMD->GetChannelInfo(nChIndex);
			TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
			
			if (pChannel->GetState() == PS_STATE_RUN)
			{
				strTemp.Format(Fun_FindMsg("SetMuxChoice_msg4","IDD_CTSMonPro_FORM"),nModuleID,nChIndex+1); //&&
				AfxMessageBox(strTemp);//$1013
				return 0;
			}
				TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

				CDWordArray adwChArray;
				//	int nSelCount = m_adwTargetChArray.GetSize();
				adwChArray.RemoveAll();

				dwData = atof(ResvAdwTargetChArray);
				nModuleID = HIWORD(dwData);
				nChIndex = LOWORD(dwData);
				if (nModuleID != nOldModuleID)
				{
					if (adwChArray.GetSize() > 0)
					{
						Fun_MuxChangeStart("MUX", nChoice,&adwChArray);
					}
					nOldModuleID = nModuleID;
					adwChArray.RemoveAll();
				}
				adwChArray.Add(MAKELONG(nChIndex, nModuleID));

				if (adwChArray.GetSize() > 0)
				{
					Fun_MuxChangeStart("MUX", nChoice,&adwChArray);
					return 1;
			}
		}

	return 0;//cny
}

void CCTSMonProView::OnCmdChInit() 
{
	CLoginManagerDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;	
	
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	CCyclerModule * pMD;
	CCyclerChannel * pChInfo;
	
	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("OnCmdChInit_msg1","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
	if (m_adwTargetChArray.GetSize() > 1) 
	{
		//AfxMessageBox("1채널만 선택 하세요.");//$1013
		AfxMessageBox(Fun_FindMsg("OnCmdChInit_msg2","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
	WORD dwData;
	int nModuleID = -1;
	int nChannelIndex = 0;
	CWordArray aw;
	for(int ch = 0; ch <m_adwTargetChArray.GetSize(); ch++)
	{
		dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		
		pMD = pDoc->GetModuleInfo(pDoc->GetModuleID(nModuleID));//모듈 정보를 가져온다.
		if(pMD)
		{
			if (pMD->GetState() == PS_STATE_LINE_OFF) 
			{
				//AfxMessageBox("모듈 접속후 진행 하세요.");//$1013
				AfxMessageBox(Fun_FindMsg("OnCmdChInit_msg3","IDD_CTSMonPro_FORM"));//&&
				return;
			}
			pChInfo = pMD->GetChannelInfo(nChannelIndex);//채널 정보를 가져온다.
			nModuleID = pChInfo->GetModuleID();
			if(pChInfo)
			{
				if((pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE))
				{
					//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");//$1013
					AfxMessageBox(Fun_FindMsg("OnCmdChInit_msg4","IDD_CTSMonPro_FORM"));//&&
					return;
				}
			}
		}
		aw.Add(nChannelIndex);
		//nSel = m_wndChannelList.GetNextSelectedItem(pos1);
	}
	
	pDoc->InitChScheduleToModuleFTP(nModuleID,&aw) ;
	

	OnVentClear(0, FALSE); //ksj 20201112 : 채널 초기화시 벤트 클리어도 내리도록 추가.
}

BOOL CCTSMonProView::InsertWorkInfo(UINT nModuleID, UINT nChannelIndex, CString strScheduleName, CString strTestName, CString strTestPath, CString strTrayNo, CString strTestSerial, CString strLotNo)
{
	CDaoDatabase  db;
	int nCount = 0;
	
	try
	{
		db.Open(GetDocument()->GetWorkInfoDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	CString strSQL;
	strSQL = "SELECT COUNT(ID) FROM work_info";
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		data = rs.GetFieldValue(0);
		nCount = data.lVal;
	}
	rs.Close();
	
	CTime StartTime = CTime::GetCurrentTime();
	
	CString strEQP_Code = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB EQP CODE", "PNE");
	CString strBlock_No = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB Block No", "1");
	
	strSQL.Format("INSERT INTO work_info(ID, START_TIME, EQP_CODE, BLOCK_CODE, SCHEDULE_NAME, TRAY_ID, CELL_ID, MODULE_NUM, CHANNEL_NUM, TEST_NAME, SAVE_PATH, RESERVE1) VALUES (%d, '%s', '%s', '%s', '%s', '%s', '%s', %d, %d, '%s', '%s', '%s')",
		nCount+1, StartTime.Format("%Y-%m-%d %H:%M:%S"), strEQP_Code, strBlock_No, strScheduleName, strTrayNo, strTestSerial, nModuleID, nChannelIndex, strTestName, strTestPath,strLotNo);
	try
	{
		db.Execute(strSQL);
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	return TRUE;
}

void CCTSMonProView::OnUpdateCmdChInit(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CCTSMonProView::OnGotoStep() 
{
	CString tmpStr;
	//tmpStr.Format("충방전기 단독 동작 시를 위한 기능입니다.\n 모든 외부 장비의 연동 동작엔 적합하지 않습니다. \n 진행하시겠습니까?");
	tmpStr.Format(Fun_FindMsg("OnGotoStep_msg3","IDD_CTSMonPro_FORM"));//&&
	if(AfxMessageBox(tmpStr, MB_OKCANCEL) == IDOK)
	{

	}
	else
	{
		return;
	}

	AfxMessageBox(Fun_FindMsg("OnGotoStep_msg3","IDD_CTSMonPro_FORM"));//yulee 20190715

	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	CCyclerModule * pMD;
	CCyclerChannel * pChannel;

	CString strMsg;
	//1. 선택된 채널 중 NEXT 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_NEXTSTEP)  == FALSE)
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");
		AfxMessageBox(Fun_FindMsg("OnGotoStep_msg1","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
	if (m_adwTargetChArray.GetSize() > 1)
	{
		//AfxMessageBox("1채널만 선택 하세요.");//$1013
		AfxMessageBox(Fun_FindMsg("OnGotoStep_msg2","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	int ch,nModuleID, nChIndex;
	for(ch = 0; ch<m_adwTargetChArray.GetSize(); ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
	}

	CGotoStepDlg	dlg(GetDocument());
	dlg.m_nModuleID  = nModuleID;
	dlg.m_nChannelID = nChIndex;

	if (dlg.DoModal() == IDOK)
	{
// 		m_FPreFickerInfo.nCode = 0 ; //lyj 20201007 Flicker 초기화
// 		m_FPreFickerInfo.nPriority = 3 ; //lyj 20201007 Flicker 초기화
// 		m_FPreFickerInfo.nAutoBuzzerStop = 0; //ksj 20201013 : 초기화 추가
		InitPreFlickerInfo(); //ksj 20201013
		SetTimer(TIMER_ALARM_ENABLE, 1000, NULL);
	}	
}

void CCTSMonProView::OnUpdateGotoStep(CCmdUI* pCmdUI) 
{
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
		CCyclerModule *pMD;
		int nModuleID;
		BOOL bAble = FALSE;
		POSITION pos = m_wndModuleList.GetFirstSelectedItemPosition();
		while(pos)
		{
			int item = m_wndModuleList.GetNextSelectedItem(pos);
			nModuleID  = m_wndModuleList.GetItemData(item);
			pMD = GetDocument()->GetModuleInfo(nModuleID);
			if(pMD)
			{
				if(pMD->GetState() == PS_STATE_LINE_OFF)
				{
					bAble = FALSE;
					break;
				}
				else
				{
					bAble = TRUE;
				}
			}
			else 
			{
				bAble = FALSE;
				break;
			}
		}
		//m_ctrlNextStep.EnableWindow(bAble);
		pCmdUI->Enable(bAble);		
		
	}
	else
	{
		//m_ctrlNextStep.EnableWindow(UpdateUIChList(SFT_CMD_NEXTSTEP, FALSE));
		pCmdUI->Enable(UpdateUIChList(SFT_CMD_NEXTSTEP, FALSE));		
	}		
}

void CCTSMonProView::OnButLoadChoice() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CString strTemp;
	CString strMsg;
	int nChoice=0;

	GetDlgItem(IDC_CBO_LOAD)->GetWindowText(strTemp);
	if (strTemp == "EDLC")
	{
		nChoice = 0;
	}
	else if (strTemp == "CELL")
	{
		nChoice = 1;
	}
	else
	{
		//AfxMessageBox("변경할 LOAD TYPE을 선택 하세요.");
		AfxMessageBox(Fun_FindMsg("OnButLoadChoice_msg1","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	if (m_nCurrentModuleID < 1)
	{
		//AfxMessageBox("작업할 모듈을 선택 하세요.");
		AfxMessageBox(Fun_FindMsg("OnButLoadChoice_msg2","IDD_CTSMonPro_FORM")); //&&
		return;
	}
//	m_wndChannelList.SetItemState( 0,LVIS_SELECTED | LVIS_FOCUSED ,LVIS_SELECTED | LVIS_FOCUSED);
	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.
	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		//AfxMessageBox("동작 중인 채널이 있으면 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("OnButLoadChoice_msg3","IDD_CTSMonPro_FORM")); //&&
		return ;
	}
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule	*pMD;
	CCyclerChannel	*pChannel;
	UINT nModuleID = 0;
	UINT nOldModuleID = 0;
	UINT nChIndex = 0;
	int ch;
	int nSelCount = m_adwTargetChArray.GetSize();

	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		
		if (pChannel->GetState() == PS_STATE_RUN)
		{
			//strTemp.Format("Module : %d, Ch %d 은 동작 중 입니다. LOAD TYPE을 변경할 수 없습니다.",nModuleID,nChIndex+1);
			strTemp.Format(Fun_FindMsg("OnButLoadChoice_msg4","IDD_CTSMonPro_FORM"),nModuleID,nChIndex+1); //&&
			AfxMessageBox(strTemp);
			return;
		}
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
	}
	
	//strMsg.Format("%s 명령을 전송 하시겠습니까?", strTemp);
	strMsg.Format(Fun_FindMsg("OnButLoadChoice_msg5","IDD_CTSMonPro_FORM"), strTemp); //&&
	//if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) {
	if(IDYES != MessageBox(strMsg, Fun_FindMsg("OnButLoadChoice_msg6","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) { //&&
		return;
	}

	CDWordArray adwChArray;
	adwChArray.RemoveAll();
	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		if (nModuleID != nOldModuleID)
		{
			if (adwChArray.GetSize() > 0)
			{
				Fun_MuxChangeStart("LOAD", nChoice,&adwChArray);
			}
			nOldModuleID = nModuleID;
			adwChArray.RemoveAll();
		}
		adwChArray.Add(MAKELONG(nChIndex, nModuleID));
	}
	if (adwChArray.GetSize() > 0)
	{
		Fun_MuxChangeStart("LOAD", nChoice,&adwChArray);
	}

}
//ksj 20160426 작업 예약 기능 추가.
void CCTSMonProView::OnCmdRunQueue() 
{
	m_pReserveDlg->CenterWindow(this);
	m_pReserveDlg->ReserveAdd();
	m_pReserveDlg->ShowWindow(SW_SHOWNORMAL);	
	m_pReserveDlg->SetForegroundWindow();	
}

//ksj 20160426 기존 작업시작 로직에 작업 예약 기능 추가.
// nOption = 0:기존 작업시작과 동일, 1:작업예약, 2:예약된 작업 실행.
int CCTSMonProView::RunProcess(int nOption, LPVOID pReservedWork /*= NULL */, int ReserveID)
{

	//작업 시작 잠금 옵션
	/*BOOL bLock = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLockForRun",FALSE);	//ljb 20150316
	if (bLock)
	{
		CLoginManagerDlg LoginDlg;
		if(LoginDlg.DoModal() != IDOK) return RUN_FAIL;
	}*/

	//작업 예약 시작시 Parameter 확인.
	if(nOption == WORK_START_RESERV_RUN && pReservedWork == NULL)
	{	
		ASSERT(pReservedWork);			
	}

	//변수 선언 및 초기화
	CCTSMonProDoc* pDoc = GetDocument();
	CPtrArray	aPtrSelCh;
	CString strTestName, strScheduleFileName;		
	BOOL bStartStepOption = TRUE;
	CString strLotNo;	
	int nModelPK = 0;
	int nTestPK = 0;
	CDWordArray adwChArray;
	BOOL	bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);	
	CScheduleData	scheduleInfo;
	CProgressWnd	progressWnd;
	CStartDlgEx		startDlg(GetDocument(), this);
	startDlg.m_bEnableStepSkip = bStartStepOption;
	startDlg.m_strLot = strLotNo;


	//전송 가능 채널 업데이트
	if(!RunProcess_ChUpdate(nOption, &bStartStepOption, &strLotNo, &aPtrSelCh))	
	{
		//pDoc->WriteSysLog("채널 업데이트 실패", CT_LOG_LEVEL_NORMAL);
		pDoc->WriteSysLog(Fun_FindMsg("CTSMonProView_RunProcess_msg1","IDD_CTSMonPro_FORM"), CT_LOG_LEVEL_NORMAL);//&&
		return RUN_FAIL;
	}
	//yulee 20181026
	int nResvMux;
	
	if(nOption == WORK_START_RESERV_RUN)
	{
		LPS_WORK_RESERVATION_DATA Work = (LPS_WORK_RESERVATION_DATA) pReservedWork;
		nResvMux = (int)(Work->uiMuxOption);
	}
	

	//스케쥴 선택 후 작업 준비 및 파일 생성.
	if(nOption == WORK_START_NORMAL || nOption == WORK_START_RESERV)
	{
		if(!RunProcess_PrepareSchedule(nOption, &startDlg, &scheduleInfo,
										&aPtrSelCh, &adwChArray, 
										&strTestName, &strScheduleFileName,
										&progressWnd, &nModelPK, &nTestPK))
		{
			//pDoc->WriteSysLog("스케쥴 준비 실패", CT_LOG_LEVEL_NORMAL);
			pDoc->WriteSysLog(Fun_FindMsg("CTSMonProView_RunProcess_msg2","IDD_CTSMonPro_FORM"), CT_LOG_LEVEL_NORMAL);//&&
			return RUN_FAIL;
		}
	}
	else if(nOption == WORK_START_RESERV_RUN) //예약작업시작
	{
		/////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////
		//MUX 설정 후 진행
		//yulee 20181025_1
		//1. pReservedWork의 pReservedWork->uiMuxOption 값으로 0이면 무시 1, 2면 MUX A, B 설정 후 진행 
		//2. MUX 설정 실패 시 return RUN_FAIL 로 처리

// 		int nResvMux;
// 
// 		LPS_WORK_RESERVATION_DATA Work = (LPS_WORK_RESERVATION_DATA) pReservedWork;
// 		nResvMux = (int)(Work->uiMuxOption);

		if((nResvMux > 0) && (nResvMux < 3))
		{
				m_nFlagMuxResv = 1; //Mux 사용하는 예약으로 공정 시작 시 Mux receive message 처리용.
			if(SetMuxFunc(pReservedWork) != 1)
			{
				return RUN_FAIL;
			}
			m_nOption_Mux = nOption; //yulee 20181025_1
			m_pReservedWork_Mux = pReservedWork;//yulee 20181025_1
			m_nReserveID = ReserveID;//yulee 20181025_1
			m_ResvMuxSetRet = 3;
			KillTimer(TIMER_RESRV_MUX_RECV);
			struct ST_TIMER_RESV_MUXRECV{
				static void CALLBACK TimeResvMUXRecvRetry(HWND hwnd, UINT iMsg, UINT_PTR wParam, DWORD lParam)
				{
					::KillTimer(hwnd, wParam);
					CCTSMonProView *dlg = (CCTSMonProView*)CWnd::FromHandle(hwnd);
					if(!dlg) return;


					dlg->m_ResvMuxSetRet = dlg->Fun_Chk_RESRV_MUX_REC(dlg->m_nOption_Mux, dlg->m_pReservedWork_Mux, dlg->m_nReserveID);

						
				}};
			SetTimer(TIMER_RESRV_MUX_RECV, 1000, ST_TIMER_RESV_MUXRECV::TimeResvMUXRecvRetry);
			Wait_Milli(2000);
		}
		else if(nResvMux == 0)
		{
			m_nFlagMuxResv = 0;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////
		if(m_nFlagMuxResv != 1)
			m_pReserveDlg->RestoreReservedWork(pReservedWork, &scheduleInfo, &adwChArray, &startDlg,&nModelPK,&nTestPK);
	}

	if((nOption == WORK_START_RESERV_RUN) && ((nResvMux > 0) && (nResvMux < 3))) //yulee 20181026 Mux 설정한 예약기능에서의 처리
	{
		if(m_ResvMuxSetRet == 0)
			return RUN_FAIL;
		else if(m_ResvMuxSetRet == 1)
			return RUN_OK;
	}

	
	if(nOption != WORK_START_RESERV) //예약할때는 챔버 실행 안함.
	{
		if(!RunProcess_Chamber(nOption,GetDocument(),&startDlg,&adwChArray,&progressWnd))
		{
			//pDoc->WriteSysLog("챔버 준비 실패", CT_LOG_LEVEL_NORMAL);
			pDoc->WriteSysLog(Fun_FindMsg("CTSMonProView_RunProcess_msg3","IDD_CTSMonPro_FORM"), CT_LOG_LEVEL_NORMAL);//&&
			return RUN_FAIL;
		}
	}
	
	//작업 시작 또는 예약 저장. //20180427 yulee 예약시작 시 패턴 path 가져오기에 대한 수정
	if(!RunProcess_WorkStart(nOption,&progressWnd,&adwChArray,&scheduleInfo,&startDlg,strTestName,nModelPK,nTestPK,strScheduleFileName, ReserveID))
	{
		//GetDocument()->WriteSysLog("작업 실패", CT_LOG_LEVEL_NORMAL);
		GetDocument()->WriteSysLog(Fun_FindMsg("CTSMonProView_RunProcess_msg4","IDD_CTSMonPro_FORM"), CT_LOG_LEVEL_NORMAL);//&&
		return RUN_FAIL;
	}
	
	return RUN_OK;
}

void CCTSMonProView::Wait_Milli(DWORD dwMillisecond) //yulee 20181026
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();
	
	while(GetTickCount() - dwStart < dwMillisecond)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return;
}

BOOL CCTSMonProView::SetMuxFunc(LPVOID pWork)//yulee 20181025
{
	ASSERT(pWork);
	
	LPS_WORK_RESERVATION_DATA pReservedWork = (LPS_WORK_RESERVATION_DATA) pWork;

	//Mux 설정 
	//return SetMuxChoice(2, pReservedWork->uiMuxOption, pReservedWork->strAdwChArray);
	return SetMuxChoice(2, pReservedWork->uiMuxOption, pReservedWork->szAdwChArray);	// 200313 HKH Memory Leak 수정
}

BOOL CCTSMonProView::RunProcess_ChUpdate(int nOption, BOOL* bStartStepOption, CString* strLotNo, CPtrArray* aPtrSelCh)
{	
	CDWordArray adwPauseCh;
	CCTSMonProDoc *pDoc = GetDocument();	
	BOOL	bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);
			
	UINT nModuleID = 0;
	UINT nChIndex = 0;
	CString strLogMsg;
	CCyclerModule*	pMD;
	CCyclerChannel* pChannel;

	//m_awTargetChArray을 구함
	if(nOption == WORK_START_NORMAL) //ksj 20160426 일반 작업 시작인 경우 채널이 준비되어야 시작 가능하다.
	{
		if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
		{
//#ifndef _DEBUG
				if(nOption == WORK_START_NORMAL)
					//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
					AfxMessageBox(Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg1","IDD_CTSMonPro_FORM"));//&&

				return RUN_FAIL;
//#endif
		}
	}
	else if(nOption == WORK_START_RESERV) //ksj 20160426 작업 예약인 경우 현재 전송 가능 여부 관계 없이 선택한 채널은 모두 예약한다.
	{
		if(UpdateSelectedChList(SFT_CMD_NONE)  == FALSE)
		{
#ifndef _DEBUG
			//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
			AfxMessageBox(Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg2","IDD_CTSMonPro_FORM"));//&&
			return RUN_FAIL;
#endif
		}
	}	
 	//////////////////////////////////////////////////////////////////////////

	int nSelCount = m_adwTargetChArray.GetSize();

	//CPtrArray	aPtrSelCh;
	//CString strLotNo;
	CString strTemp;

	int  nMinVoltage;
	
	if(m_bOverChargerSet) //yulee 20180227 OverCharger //yulee 20191017 OverChargeDischarger Mark
	{
		float OverChargerMinVolt = 0;
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);
		nMinVoltage = -OverChargerMinVolt;//yulee 20180810 //atoi(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Min Voltage", "-30000")); //mV 단위
	}
	else
		nMinVoltage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Min Voltage", -500); //mV 단위
	//int  nMinVoltage = atoi(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Min Voltage", "-500")); //mV 단위
	float fMinVoltage = nMinVoltage * 1000;
	
	///선택한 채널들이 이전에 정상적으로 끝났는지 확인한다.
	///시작 명령을 취소하면 나중에 Run명령을 전송하면 안된다.

	//ljb 20130514 add 모듈별 작업 준비 중인지 체크
	int ch;
	int nCheckModuleID = 0;

	if(nOption == WORK_START_NORMAL || nOption == WORK_START_RESERV_RUN) //ksj 20160426 작업 시작인 경우만 체크
	{
		for(ch = 0; ch<nSelCount; ch++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(ch);
			nModuleID = HIWORD(dwData);
			if(ch == 0)
			{
				nCheckModuleID = nModuleID;
				if (pDoc->Fun_CheckReadyDlg(nCheckModuleID) == TRUE)
				{
					//strLogMsg.Format("ReadyDlg 이상 입니다. (module ID : %d)",nCheckModuleID);
					strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg3","IDD_CTSMonPro_FORM"),nCheckModuleID);//&&
					if(nOption == WORK_START_NORMAL)
						AfxMessageBox(strLogMsg);
					return RUN_FAIL;
				}
			}
			if (nCheckModuleID != nModuleID)
			{
				nCheckModuleID = nModuleID;
				if (pDoc->Fun_CheckReadyDlg(nCheckModuleID) == TRUE)
				{
					//strLogMsg.Format("ReadyDlg 이상 입니다. (module ID : %d)",nCheckModuleID);
					strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg4","IDD_CTSMonPro_FORM"),nCheckModuleID);//&&
					if(nOption == WORK_START_NORMAL)
						AfxMessageBox(strLogMsg);
					return RUN_FAIL;
				}
			}
		}

		
	}

	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		
		if (pChannel->GetState() == PS_STATE_RUN
						&& nOption == WORK_START_NORMAL) //ksj 20160426 일반 작업시작인 경우만 상태 체크
		{
			//strTemp.Format("Module : %d, Ch %d 채널을 시작 할 수 없습니다. 채널을 다시 선택 하세요.",nModuleID,nChIndex+1);
			strTemp.Format(Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg5","IDD_CTSMonPro_FORM"),nModuleID,nChIndex+1);//&&
			AfxMessageBox(strTemp);
			return RUN_FAIL;
		}
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
		

		//Step Skip 기능 가능 여부 검사 
		if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSIONV3)		bStartStepOption = FALSE;
		
		if(bCheckPrevData
				&& nOption == WORK_START_NORMAL) //ksj 20160426 일반 작업 시작일때만 체크 (현재 예약된 작업의 경우 손실 체크 안함)
		{
			CWordArray wOneCh;
			wOneCh.Add((WORD)nChIndex);
			
			//작업 정보 입력전에 검사 하여야 한다. (중복되 폴더가 있으면 삭제 되므로 )
			//이전 시험이 data 손실 구간 정보를 Update한다.
			//CCyclerChannel::IsDataLoss() 호출 이전에 최종 정보로 Update 하기 위해 
			pMD->UpdateDataLossState(&wOneCh);	//data량이 많을시 Loading 시간 측정 필요
			//pMD->UpdateDataSimpleLossState(&wOneCh);	//20080318 Data가 많을 경우 많은 시간이 소요되므로 간단하게 변경
			
			if(pChannel->IsDataLoss() == TRUE)
			{
				//strLogMsg.Format("%s의 Channel %d 에서 시행한 이전 시험 [%s]에 data 손실 구간이 존재합니다.", pDoc->GetModuleName(nModuleID), nChIndex+1, pChannel->GetTestName()); 
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg6","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nModuleID), nChIndex+1, pChannel->GetTestName()); //&&
				//strLogMsg += "Data를 복구하기 위해서는 [도구]->[Data 복구...]를 시행하기기 바랍니다.";
				strLogMsg += Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg7","IDD_CTSMonPro_FORM");//&&
				//strLogMsg += "복구하지 않고 새로운 시험을 시작하게 되면 이전 손실된 data는 복구할 수 없게됩니다.\n";
				strLogMsg += Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg8","IDD_CTSMonPro_FORM");//&&
				//strLogMsg += "새로운 작업을 시작 하시겠습니까?";
				strLogMsg += Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg9","IDD_CTSMonPro_FORM"); //&&
				//int nUserSel = MessageBox(strLogMsg, "Data 손실", MB_ICONQUESTION|MB_YESNOCANCEL);
				int nUserSel = MessageBox(strLogMsg, Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg10","IDD_CTSMonPro_FORM"), MB_ICONQUESTION|MB_YESNOCANCEL);//&&
				if(nUserSel == IDCANCEL)	
				{
					return RUN_FAIL;		//Run 명령 취소 
				}
				else if(nUserSel == IDNO)
				{
					//시작 명령을 보내기 위한 선택 목록에서 제외 하여야 한다.
					continue;	//Data를 복구함 
				}
			}
		}
		
		//잠시 멈춤 Channel
		if(pChannel->GetState() == PS_STATE_PAUSE
					 && nOption == WORK_START_NORMAL) //ksj 20160426 일반 작업 시작일때만 잠시 멈춤되어있는 채널 체크.
		{
			adwPauseCh.Add(MAKELONG(nChIndex, nModuleID));
			TRACE("Pause Module %d ch %d\n", nModuleID, nChIndex);
		}
		
		//선택된 Channel 정보를 Add한다.
		aPtrSelCh->Add(pChannel);
		*strLotNo = pChannel->GetTestSerial();
	}
	
	//만약 Pause인 채널이 있으면 Stop명령을 보낸다.
	if(adwPauseCh.GetSize() > 0)
	{
		//if(AfxMessageBox("선택한 채널의 이전 작업을 초기화 하고 새로운 작업을 설정 하시겠습니까?", MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)
		if(AfxMessageBox(Fun_FindMsg("CTSMonProView_RunProcess_ChUpdate_msg11","IDD_CTSMonPro_FORM"), MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2) == IDNO)//&&
		{
			return RUN_FAIL;
		}
		if(SendCmdToModule(adwPauseCh, SFT_CMD_STOP) == FALSE)	
		{
			//AfxMessageBox("작업 완료 처리 실패");
			AfxMessageBox("작업 완료 처리 실패");//&&
			return RUN_FAIL;
		}
	}
	//이전시험 정상 종료 여부 확인 끝
	//////////////////////////////////////////////////////////////////////////

	if(nOption == WORK_START_NORMAL)
		((CMainFrame *)AfxGetMainWnd())->m_bBlockFlag = TRUE;

	return RUN_OK;
}

//스케쥴 선택 및 파일생성 작업.
int CCTSMonProView::RunProcess_PrepareSchedule(int nOption, CStartDlgEx* pStartDlg, CScheduleData* pScheduleInfo, CPtrArray* aPtrSelCh,
											  CDWordArray* adwChArray, CString* strTestName, CString* strScheduleFileName,
											  CProgressWnd* pProgressWnd, int* nModelPK, int* nTestPK)
{
	CSelectScheduleDlg dlg(GetDocument());
	CString strTemp;
	CString strTestPath, strContent, strLogMsg;
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule*	pMD;
	CCyclerChannel*	pChannel;

	Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
	
	int nRtn = 0;

	while(1)
	{
		dlg.m_nSelModelID = *nModelPK;
		dlg.m_nSelScheduleID = *nTestPK;
		
		if(IDOK != dlg.DoModal())
		{
			break;
		}
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
		
		*nModelPK = dlg.m_nSelModelID;		//선택 모델 PK
		*nTestPK = dlg.m_nSelScheduleID;		//선택 스케쥴 PK
		
		//4. 선택한 공정의 조건을 DataBase에서 Load한다.
		if(pScheduleInfo->SetSchedule(pDoc->GetDataBaseName(), *nModelPK, *nTestPK) == FALSE)
		{
			//strLogMsg.Format("공정조건 불러오기 실패!!!");
			strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg1","IDD_CTSMonPro_FORM"));//&&
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			break;
		}
		else
		{
			TRACE("%f\n",pScheduleInfo->m_CellCheck.fMaxV);
			TRACE("%f\n",pScheduleInfo->m_CellCheck.fMinV);
			TRACE("%f\n",pScheduleInfo->m_CellCheck.fCellDeltaV);
			TRACE("%f\n",pScheduleInfo->m_CellCheck.fMaxI);
			
			if (pScheduleInfo->Fun_ISEmptyCommSafety() == -1)
			{
				//strLogMsg.Format("시험 안전조건이 설정 되어 있지 않습니다. !!!");
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg2","IDD_CTSMonPro_FORM"));//&&
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				AfxMessageBox(strLogMsg);
				break;
			}
			if (pScheduleInfo->Fun_ISEmptyCommSafety() == -2) //20150811 edit
			{
				//strLogMsg.Format("시험 안전조건 값이 설정 되어 있지 않습니다. (최대전압, 최대전류, 최대 셀전압편차) !!!");
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg3","IDD_CTSMonPro_FORM"));//&&
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				AfxMessageBox(strLogMsg);
				break;
			}
			if (pScheduleInfo->Fun_ISEmptyCommSafety() == -3)	// 211015 HKH 절연모드 충전 관련 기능 보완
			{
				//strLogMsg.Format("시험 안전조건 값이 설정 되어 있지 않습니다. (최소전압) !!!");
				strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg12","IDD_CTSMonPro_FORM"));//&&
				pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				AfxMessageBox(strLogMsg);
				break;
			}
			/*TRACE(_T("pattern Schdule Path : %s"), pScheduleInfo)*/

		}
		
		//5. 시험명 및 기타 정보 입력 받아 시험명으로 폴더를 생성한다. 
		if(nOption == WORK_START_RESERV) //ksj 20160426	
			pStartDlg->m_bReserveMode = TRUE;	//예약모드로 창 띄우기
		else
			pStartDlg->m_bReserveMode = FALSE;
		
		pStartDlg->SetInfoData(pScheduleInfo , aPtrSelCh);
		pStartDlg->DoModal();		
		
		
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
		
		if(pStartDlg->m_nReturn == IDCANCEL)
		{
			break;
		}
		else if(pStartDlg->m_nReturn == IDOK)
		{
			nRtn = 1;
			break;
		}
	}
	
	if(nRtn == 0)
	{
		return RUN_FAIL;
	}

	//////////////////////////////////////////////////////////////////////////
	
	//20070831 KJH 스케줄의 Step 값을 확인한다.
	if(GetDocument()->ScheduleValidityCheck(pScheduleInfo, &m_adwTargetChArray) < 0)
	{
		AfxMessageBox(GetDocument()->GetLastErrorString());
		return RUN_FAIL;
	}
	
	//5. 입력한 작업명과 저장 위치 확인 
	// c:\\Test_Name 형태 
	strTestPath = pStartDlg->GetTestPath();
	ASSERT(!strTestPath.IsEmpty());
	*strTestName = strTestPath.Mid(strTestPath.ReverseFind('\\')+1);
	
	//시험에 적용된 Pattern File 저장 폴더 생성 ljb 20130502 add
	strLogMsg.Format("%s\\Pattern", strTestPath);
	
	//5.1 Pattern File 폴더를 생성시킨다.
	if( ::CreateDirectory(strLogMsg, NULL) == FALSE )
	{
		//strContent.Format("Pattern 저장용 폴더 생성 실패(%s)", strLogMsg);
		strContent.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg4","IDD_CTSMonPro_FORM"), strLogMsg);//&&
		pDoc->WriteSysLog(strContent, CT_LOG_LEVEL_NORMAL);
	}
	//strLogMsg.Format("Data 저장 폴더 생성 성공(%s)", strContent);
	strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg5","IDD_CTSMonPro_FORM"), strContent);//&&
	pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_SYSTEM);

	//////////////////////////////////////////////////////////////////////////
	//CProgressWnd progressWnd(AfxGetMainWnd(), "전송중...", TRUE);	
	if(nOption == WORK_START_NORMAL)
	{
		//pProgressWnd->Create(AfxGetMainWnd(), "전송중...", TRUE);
		pProgressWnd->Create(AfxGetMainWnd(), Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg6","IDD_CTSMonPro_FORM"), TRUE);//&&
		pProgressWnd->GoModal();
		pProgressWnd->SetRange(0, 100);
	}	
	//////////////////////////////////////////////////////////////////////////
	
//	CDWordArray adwChArray;
	int nSelCount = aPtrSelCh->GetSize();
	int nChIndex;
	int nModuleID; 
	int tCh;
	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh->GetAt(tCh);
		
		if(pChannel == NULL)
		{
			TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);		
	
		adwChArray->Add(MAKELONG(nChIndex, nModuleID));
		
		//6. 선택된 Channel을 시험준비 한다.
		pProgressWnd->SetPos(0);
		//strLogMsg.Format("%s 의 채널 %d 결과 폴더 생성...", pDoc->GetModuleName(nModuleID), nChIndex+1);
		strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg7","IDD_CTSMonPro_FORM"), pDoc->GetModuleName(nModuleID), nChIndex+1);//&&
		pProgressWnd->SetText(strLogMsg);
		pProgressWnd->SetPos(int((float)tCh/(float)nSelCount*100.0f));
		
		
		//Cancel 처리
		if(pProgressWnd->PeekAndPump() == FALSE)				break;
		
		
		//CStartDlg::OnOk()에서 test_name folder는 생성한다.
		//c:\\Test_Name\\M01C01 형태
		int nStartChNo = GetStartChNo(nModuleID);
		strContent.Format("%s\\M%02dCh%02d[%03d]", strTestPath, nModuleID, nChIndex+1, nStartChNo+nChIndex);
		
		//6.1 Channel 폴더를 생성시킨다.
		if (pDoc->ForceDirectory(strContent) == FALSE)
		{
			//strLogMsg.Format("Data 저장 폴더 생성 실패(%s)", strContent);
			strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg8","IDD_CTSMonPro_FORM"), strContent);//&&
			AfxMessageBox(strLogMsg);
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			return 0;
		}
		else
		{
			strLogMsg.Format("%s\\StepStart", strContent);
			pDoc->ForceDirectory(strLogMsg);
		}
		Sleep(100);
		
		//6.2 각 폴더에 스케줄 파일을 생성한다.
		// c:\\Test_Name\M01C01\Test_Name.sch
		*strScheduleFileName = strContent + "\\" + *strTestName+ ".sch";

		if(FALSE == pScheduleInfo->SaveToFile(*strScheduleFileName))
		{
			//strLogMsg.Format("%s Schedule 파일 생성 실패", strTestName);
			strLogMsg.Format(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg9","IDD_CTSMonPro_FORM"), strTestName);//&&
			pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
			continue;
		}
		
		if(nOption != WORK_START_RESERV)
		{
			pChannel->ResetData();
			//6.3 시험명 설정
			pChannel->SetFilePath(strContent);	//ljb 20130626 수정 (함수내부)		
			
			//Set Test Lot No
			pChannel->SetTestSerial(pStartDlg->m_strLot, pStartDlg->m_strWorker, pStartDlg->m_strComment,pChannel->m_strCellDeltaVolt);
			pChannel->LoadSaveItemSet();
		}
		//ksj 20160610 로그 추가
		if(nOption == WORK_START_RESERV)
			//pChannel->WriteLog("작업 예약");
			pChannel->WriteLog(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg10","IDD_CTSMonPro_FORM"));//&&
		else if(nOption == WORK_START_NORMAL)
			//pChannel->WriteLog("작업 시작");
			pChannel->WriteLog(Fun_FindMsg("CTSMonProView_RunProcess_PrepareSchedule_msg11","IDD_CTSMonPro_FORM"));//&&
		
		//6.4 Aux Title 파일 생성
		pChannel->SetAuxTitleFile(strContent, *strTestName);
		//6.5 Can Title 파일 생성
		pChannel->SetCanTitleFile(strContent, *strTestName);
		Sleep(100);	//모듈 상태 전이를 위한 시간 지연 
		
		//ljb 20130404 CTS File 생성
		pChannel->WriteCtsFile();
		
		
	}	

	
	return RUN_OK;
}

//ksj 20160610
int CCTSMonProView::RunProcess_Chamber(int nOption, CCTSMonProDoc* pDoc, CStartDlgEx* pStartDlg, CDWordArray* adwChArray, CProgressWnd* pProgressWnd)
{

	int nSelCount = adwChArray->GetSize();
	int nChIndex;
	int nModuleID; 
	int tCh;
	DWORD dwData;
	CCyclerChannel* pChannel;
	//CCyclerModule* pMD;

	CString strTemp;
	COvenCtrl	*pCtrlOven;
	int			nOvenPortLine;
	CDWordArray awOvenArray;
	//int nChIndex = pChannel->GetChannelIndex();

	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		dwData = adwChArray->GetAt(tCh);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);

		pChannel = (CCyclerChannel *) pDoc->GetChannelInfo(nModuleID, nChIndex);
		
		if(pChannel == NULL)
		{
			TRACE("Channel data 요청 함수 error 발생 or Channel MES Data 이상\n");
			continue;
		}

		//////////////////////////////////////////////////////////////////////////
		// + BW KIM 2014.02.14
		int nChamberOperationType=0; // 0 : 삼성, 1 : SK 
		// 기본적으로 0을 사용한다. 1채널이든 2채널 장비든 1개의 챔버를 사용한다. 1은 KTL 과 SK 에서 2ch 장비에서 채널별로 챔버를 연동시키고자 할 때 사용한다.
		nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 0);
		
		if(nChamberOperationType == 0)
		{
			//천안 삼성용 20140122
			if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == TRUE)
			{
				pCtrlOven = &(pDoc->m_ChamberCtrl.m_ctrlOven1);
				pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
				nOvenPortLine = 1;
			}
			else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == TRUE)
			{
				pCtrlOven = &(pDoc->m_ChamberCtrl.m_ctrlOven2);
				pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
				nOvenPortLine = 2;				
			}
		}
		else
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리  //yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
			{
			//대전 SK용 20140122
				if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == true && nChIndex == 0) //yulee 20181213 병렬 mark
				{
					pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven1;
					pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
					nOvenPortLine = 1;
				}
				else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == true && nChIndex == 1) //yulee 20181213 병렬 mark
				{
					pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
					pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
					nOvenPortLine = 2;				
				}
			}
//#else  
else
			{
				//대전 SK용 20140122 //20180510 yulee 채널 개수와 챔버 컨트롤러의 개수에 따른 동작 
				//CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
				CCyclerModule * pMD = GetDocument()->GetModuleInfo(nModuleID); //yulee 20180729
				int nTotalCount = pMD->GetTotalChannel();
				if(nTotalCount == 2)//충방전기2채널 - 오븐 콘트롤러 2채널 사용 시 각각 제어 할 수 있도록  
				{
					if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == TRUE && nChIndex == 0)
					{
						pCtrlOven = &(pDoc->m_ChamberCtrl.m_ctrlOven1);
						pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
						nOvenPortLine = 1;
					}
					else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == TRUE && nChIndex == 1)
					{
						pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
						pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
						nOvenPortLine = 2;				
					}
				}
				else if(nTotalCount == 4) //충방전기4채널 - 오븐 콘트롤러 2채널 사용 시 각각 제어 할 수 있도록 1,2채널은 1컨트롤러 3,4채널은 2 컨트롤러  
				{
					if (pDoc->m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl == TRUE && (nChIndex == 0 || nChIndex == 1))
					{
						pCtrlOven = &(pDoc->m_ChamberCtrl.m_ctrlOven1);
						pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
						nOvenPortLine = 1;
				}
				else if (pDoc->m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl == TRUE && (nChIndex == 2 ||nChIndex == 3))
					{
						pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
						pCtrlOven->GetOvenIndexList(*adwChArray,awOvenArray);
						nOvenPortLine = 2;				
					}
				}
			}
//#endif
		}
		
		//Send Oven Start Command
		pChannel->ChamberValueInit();
		for(int iOvenCnt = 0; iOvenCnt < awOvenArray.GetSize(); iOvenCnt++)
		{
			dwData = 0;
			dwData = awOvenArray.GetAt(iOvenCnt);
			if(pCtrlOven->m_bUseOvenCtrl)
			{
				pChannel->m_nStartOptChamber = pStartDlg->m_nStartOptChamber;
				if (pStartDlg->m_nStartOptChamber == 1)	//ljb 챔버 Fix Mode
				{
					pDoc->OvenCheckFixOption(nOvenPortLine, pStartDlg->m_uiChamberCheckFixTime,pStartDlg->m_fChamberDeltaFixTemp);
					pDoc->OvenStartStateAndSendStartCmd(nOvenPortLine, 1,0,pStartDlg->m_fChamberFixTemp,dwData);
					pChannel->m_uiChamberCheckFixTime = pStartDlg->m_uiChamberCheckFixTime;
					pChannel->m_fChamberDeltaFixTemp = pStartDlg->m_fChamberDeltaFixTemp;
					pChannel->m_fchamberFixTemp;
				}
				else if (pStartDlg->m_nStartOptChamber == 2)	//ljb 챔버 Prog Mode
				{
					pDoc->OvenStartStateAndSendStartCmd(nOvenPortLine, 2,pStartDlg->m_uiChamberPatternNum,0,dwData);
					pChannel->m_uichamberPatternNum = pStartDlg->m_uiChamberPatternNum;
				}
				else if (pStartDlg->m_nStartOptChamber == 3)	//ljb 챔버 스케쥴 연동 Mode
				{
					pStartDlg->m_uiChamberCheckTime;	//챔버 온도 비교 시작 시간 1초는 == 100
					pStartDlg->m_fChamberDeltaTemp;	//챔버 비교 온도
					
					pDoc->OvenCheckOption(nOvenPortLine, pStartDlg->m_uiChamberCheckTime,pStartDlg->m_fChamberDeltaTemp);
					pDoc->OvenStartStateAndSendStartCmd(nOvenPortLine, 3,99,0,dwData);
					
					pChannel->m_uiChamberCheckTime = pStartDlg->m_uiChamberCheckTime;
					pChannel->m_fchamberDeltaTemp = pStartDlg->m_fChamberDeltaTemp;					
				}
				else if(pStartDlg->m_nStartOptChamber == 5)	//yulee 20180821 //챔버 사용 안함(챔버 중지)
				{
					//1. 채널이 할당된 챔버의 번호를 가져온다. 20180515 yulee 
					//2. 그 챔버가 동작 중인지를 확인한다. 
					//3. 동작 중이면 챔버 관련 컨트롤러를 비활성화 시킨다. 
					
					
					// 					int nModuleID, nChIndex, nOvenIndex, nOvenID = -1, i;
					// 					for(int i = 0; i< aPtrSelCh.GetSize(); i++)
					// 					{
					//	CCyclerChannel *pCh = (CCyclerChannel *)aPtrSelCh.GetAt(i);
					int nOvenID = -1;
					nModuleID = pChannel->GetModuleID();
					nChIndex = pChannel->GetChannelIndex();
					//			}
					
					
					
					CDWordArray adwSelChList, adwOvenList;
					pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndexList(adwSelChList , adwOvenList);
					
					COvenCtrl *pCtrlOven;
					nOvenID = pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndex(nModuleID, nChIndex);
					if(nOvenID != -1)
					{
						nOvenID = pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndex(nModuleID, nChIndex);
						pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven1;
					}
					else
					{
						nOvenID = pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndex(nModuleID, nChIndex);
						if(nOvenID != -1)
						{
							nOvenID =pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndex(nModuleID, nChIndex);
							pCtrlOven = &pDoc->m_ChamberCtrl.m_ctrlOven2;
						}
					}
					
					
					BOOL bOvenRun = FALSE;
					BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven", FALSE);
					
					if(bUseOven != FALSE)
					{
						if(pCtrlOven->GetRun(nOvenID))	//챔버 동작 중
						{
							if(pCtrlOven->SendStopCmd(0, nOvenID) == FALSE)
							{
								//strTemp = "챔버 STOP 명령 실패!!!";
								strTemp = Fun_FindMsg("CTSMonProView_RunProcess_Chamber_msg1","IDD_CTSMonPro_FORM");//&&
								pDoc->WriteSysLog(strTemp);
							}
						}
					}
				}
			}
			//if (pStartDlg->m_nStartOptChamber != 4) && pStartDlg->m_uiChamberDelayTime > 0) //yulee 20180821 //챔버 사용 안함(챔버 중지)
			if (((pStartDlg->m_nStartOptChamber != 4) && (pStartDlg->m_nStartOptChamber != 5)) &&  pStartDlg->m_uiChamberDelayTime > 0)
			{
				//strTemp.Format("챔버 Run 후 대기 중...");
				strTemp.Format(Fun_FindMsg("CTSMonProView_RunProcess_Chamber_msg2","IDD_CTSMonPro_FORM"));//&&
				pProgressWnd->SetText(strTemp);
				
				int nRtn = 0;
				CReadyRunDlg Readydlg(GetDocument());
				while(1)
				{
					Readydlg.m_nReayTime = pStartDlg->m_uiChamberDelayTime;
					
					if(IDOK == Readydlg.DoModal())
					{
						nRtn = 1;
						break;
					}
					break;
				}
				if(nRtn == 0)
				{
					return FALSE;
				}
			}
			
			//챔버 동작 확인
			//if (pStartDlg->m_nStartOptChamber != 4) //yulee 20180821 //챔버 사용 안함(챔버 중지)
			if ((pStartDlg->m_nStartOptChamber != 4) && (pStartDlg->m_nStartOptChamber != 5))
			{			
				if(pCtrlOven->GetRun(dwData) == FALSE)	//챔버 동작 중
				{
					if(nOption == WORK_START_NORMAL) //ksj 20160610 조건추가 - 일반 작업 시작일때만 확인 메세지 띄움.
					{
						//strTemp = "챔버 동작을 확인 하세요. 충방전을 진행 하시겠습니까 ?";
						strTemp = Fun_FindMsg("CTSMonProView_RunProcess_Chamber_msg3","IDD_CTSMonPro_FORM");//&&
						//if(IDNO == MessageBox(strTemp, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
						if(IDNO == MessageBox(strTemp, Fun_FindMsg("CTSMonProView_RunProcess_Chamber_msg4","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) //&&
						{
							return FALSE;
						}
					}
				}
			}
		}
	}		

	return TRUE;
}

int CCTSMonProView::RunProcess_WorkStart(int nOption, CProgressWnd* pProgressWnd, CDWordArray* pAdwChArray, CScheduleData* pScheduleInfo, CStartDlgEx* pStartDlg, CString strTestName, int nModelPK, int nTestPK, CString strSchPath, int ReserveID)
{
	CCTSMonProDoc* pDoc = GetDocument();
	CString strTemp;
	//작업 시작인 경우 작업 시작.
	if(nOption == WORK_START_NORMAL || nOption == WORK_START_RESERV_RUN)
	{
		Sleep(200);	//모듈 상태 전이를 위한 시간 지연 
		
		//충방전 Start	
		if(pDoc->SendStartCmd(pProgressWnd, pAdwChArray, pScheduleInfo, pStartDlg->m_nStartCycle, pStartDlg->m_nStartStep,pStartDlg->m_nStartOptChamber, nOption, ReserveID) == FALSE)
		{
			//bRunOK = FALSE;
			if(nOption == WORK_START_NORMAL)
				//AfxMessageBox(strTestName + " 작업 시작을 실패 하였습니다.");
				AfxMessageBox(strTestName + Fun_FindMsg("CTSMonProView_RunProcess_WorkStart_msg1","IDD_CTSMonPro_FORM"));//&&

			//GetDocument()->WriteSysLog(strTestName + " 작업 시작을 실패 하였습니다.", CT_LOG_LEVEL_NORMAL);
			GetDocument()->WriteSysLog(strTestName + Fun_FindMsg("CTSMonProView_RunProcess_WorkStart_msg2","IDD_CTSMonPro_FORM"), CT_LOG_LEVEL_NORMAL);//&&
			return RUN_FAIL;
		}
		else
		{
			//strTemp.Format("스케쥴 전송 완료");
			strTemp.Format(Fun_FindMsg("CTSMonProView_RunProcess_WorkStart_msg3","IDD_CTSMonPro_FORM"));//&&
			pProgressWnd->SetText(strTemp);
			pProgressWnd->SetPos(100);
		}
		
		Sleep(200);	
		
		if(nOption == WORK_START_NORMAL)
			((CMainFrame *)AfxGetMainWnd())->m_bBlockFlag = FALSE;
		
		return RUN_OK;
	}
	//작업 예약인 경우 변수를 DB에 저장만 해놓는다.
	else if(nOption == WORK_START_RESERV)
	{
		if(m_pReserveDlg == NULL)
		{
			//AfxMessageBox("작업 예약 초기화 실패.");
			AfxMessageBox(Fun_FindMsg("CTSMonProView_RunProcess_WorkStart_msg4","IDD_CTSMonPro_FORM"));//&&
			
			return RUN_FAIL;
		}
		
		if(!m_pReserveDlg->AddReservData(pScheduleInfo,nModelPK,nTestPK,pAdwChArray,pStartDlg,strSchPath)) //차후 작업 시작시 필요한 정보를 DB에 저장하기 위해 넘겨줌.
			return RUN_FAIL;
		
		
		

		return RESERV_OK;
	}
	return RUN_FAIL;
}

void CCTSMonProView::OnRunQueueView() 
{
	m_pReserveDlg->CenterWindow(this);
	m_pReserveDlg->ShowWindow(SW_SHOWNORMAL);	
	m_pReserveDlg->SetForegroundWindow();
}

void CCTSMonProView::OnBtnReserv() 
{
	OnRunQueueView();	
}

//ksj 20160726 외부센서 안전조건 체크
BOOL CCTSMonProView::CheckAuxSafetyCond()
{

	CCyclerModule *pMD;
	CCyclerChannel *pChannel;
	int nSelCount = m_adwTargetChArray.GetSize();
	int ch;
	int nModuleID, nChIndex;
	CCTSMonProDoc *pDoc = GetDocument();
	CString strMsg, strTmp;
	int i;
	int nErrCnt = 0;

	for(ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

		SFT_AUX_DATA * pAuxData = pChannel->GetAuxData();
		if(pAuxData == NULL)
		{
			//strMsg.Format("M%02dC%02d 외부데이터 불러오기 실패.", nModuleID, nChIndex + 1);
			strMsg.Format(Fun_FindMsg("CTSMonProView_CheckAuxSafetyCond_msg1","IDD_CTSMonPro_FORM"), nModuleID, nChIndex + 1);//&&
			return TRUE; //할당된 외부데이터가 없는 경우. FALSE를 하면 작업 시작을 안하므로 TRUE로 준다.
			//return FALSE;
		}
		
		for(int i = 0; i < pChannel->GetAuxCount(); i++)
		{
			CChannelSensor * pSensor = pMD->GetAuxData(pAuxData[i].auxChNo-1, pAuxData[i].auxChType);			
			
			if(pSensor == NULL)
			{
				CString strMsg;
				//strMsg.Format("Error : 설정되지 않은 외부데이터에 접근(ChNo : %d, Aux Ch : %d, Aux Type : %d, Aux Count : %d)",
				strMsg.Format(Fun_FindMsg("CTSMonProView_CheckAuxSafetyCond_msg2","IDD_CTSMonPro_FORM"),//&&
					m_nCurrentChannelNo+1, pAuxData[i].auxChNo, pAuxData[i].auxChType, pChannel->GetAuxCount());
				AfxMessageBox(strMsg);
				return FALSE;
			}

			STF_MD_AUX_SET_DATA AuxSetData = pSensor->GetAuxData();
		
			if(	pAuxData[i].lValue < AuxSetData.lMinData || pAuxData[i].lValue > AuxSetData.lMaxData //안전조건을 벗어나거나

				//|| (pAuxData[i].auxChType != PS_AUX_TYPE_TEMPERATURE  && AuxSetData.lEndMinData == 0) //하한 값이 0인 경우 입력안된걸로 판단.(온도는 제외)
				|| (pAuxData[i].auxChType != PS_AUX_TYPE_TEMPERATURE &&
				    pAuxData[i].auxChType != PS_AUX_TYPE_TEMPERATURE_TH && 
					pAuxData[i].auxChType != PS_AUX_TYPE_HUMIDITY && AuxSetData.lEndMinData == 0) //lyj 20200629 조건체크시 써미스터/습도 종료하한0 제외 추가
				|| AuxSetData.lMaxData == 0) //상한 안전 조건이 없으면 작업 시작 안함.
			{			
			
				CString strItem;
				switch(pAuxData[i].auxChType)
				{
					case PS_AUX_TYPE_TEMPERATURE: strItem.Format("T%d",pAuxData[i].auxChNo);	break;
					case PS_AUX_TYPE_VOLTAGE: strItem.Format("V%d",pAuxData[i].auxChNo);	break;
					//case PS_AUX_TYPE_TEMPERATURE_TH: strItem.Format("V%d",pAuxData[i].auxChNo);	break; //20180607 yulee
					//case PS_AUX_TYPE_TEMPERATURE_TH: strItem.Format("TH%d",pAuxData[i].auxChNo);	break; //ksj 20200203
					case PS_AUX_TYPE_TEMPERATURE_TH: strItem.Format("TH%d",pAuxData[i].auxChNo - pMD->GetMaxVoltage());	break; //lyj 20200629
					//case PS_AUX_TYPE_HUMIDITY: strItem.Format("H%d",pAuxData[i].auxChNo);	break; //ksj 20200116 : V1016 습도
					case PS_AUX_TYPE_HUMIDITY: strItem.Format("H%d",pAuxData[i].auxChNo - pMD->GetMaxVoltage() - pMD->GetMaxTemperatureTh()  );	break; //lyj 20200629
				}

				if(nErrCnt) strTmp += ", ";
				strTmp += strItem;
				nErrCnt++;
			}
			
		}
	}

	if(nErrCnt)
	{
		//strMsg.Format("외부센서 (%s)의 안전조건을 확인해주세요.",strTmp);
		strMsg.Format(Fun_FindMsg("CTSMonProView_CheckAuxSafetyCond_msg3","IDD_CTSMonPro_FORM"),strTmp);//&&
		//AfxMessageBox(strMsg);
		//MessageBox(strMsg,"안전조건 확인",MB_OK|MB_ICONWARNING);
		MessageBox(strMsg,Fun_FindMsg("CTSMonProView_CheckAuxSafetyCond_msg4","IDD_CTSMonPro_FORM"),MB_OK|MB_ICONWARNING);//&&
		return FALSE;
	}

	return TRUE;
		
}


//ksj 20160728 작업 완료 메세지 핸들러
//수신된 채널데이터에서 상태가 작업중->대기로 전이 될때 완료로 판단해서 통지한다.
//채널데이터 수신부에서 감지하여 통지하므로 채널 번호는 알 수 있지만 어떤 모듈인지 구분이 불가능.
LRESULT CCTSMonProView::OnChannelTestComplete(WPARAM wParam /*완료 채널 번호*/, LPARAM lParam/*해당 채널 객체 포인터*/)
{
	//int nChNo = (WORD) wParam; //채널데이터의 채널번호는 1부터 시작된다.
	char nChNo = (WORD) wParam; //
	CCTSMonProDoc* pDoc = GetDocument();
	int nModuleID;
	int nTotalModule = pDoc->GetInstallModuleCount();
	CCyclerModule *pMD = NULL;
	CCyclerChannel *pChannel = NULL;

	for(nModuleID = 1; nModuleID <= pDoc->GetInstallModuleCount(); nModuleID++) //ksj 20160728 어떤 모듈인지 이 이벤트에서는 구분할 수 없으므로 모든 모듈 검색하며 일치하는 채널 찾기
	{
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChNo - 1);

		if(pChannel->GetChannel() == (CChannel*) lParam) //ksj 20160728 작업 완료 통지 메세지를 보낸 채널을 찾아서 보냄.
		{
			//ksj 20160801 쓰레드로 Step End Data 다운로드
			pDoc->m_nFtpDownStepEndModuleID = nModuleID; //다운로드할 모듈 설정
			pDoc->m_nFtpDownStepEndChannelIndex = nChNo - 1; //다운로드할 채널 설정 
			pDoc->m_ucFtpDownIndex = pDoc->m_ucFtpDownIndex | (1 << (nChNo-1)) ; //lyj 20200526 다운로드 개선
			pChannel->SetFtpAllDown(FALSE); //lyj 20200601 다운로드 개선 작업완료 후 다운로드
			pDoc->m_bRunFtpDownStepEnd = TRUE; //스레드 루프 실행

			//ksj 20200907 : [#JSonMon] 작업 완료 될때 Json 파일 갱신. 주기적인 업데이트 호출 주기에 따라 굳이 안넣어도 됨.
			//모듈 단위로 파일이 생성되므로, 개별 채널 상태 바뀔때마다 기록하는 것은 무리가 있을 수 있음. Pack은 채널 수 적어서 넣어볼만 함.
			if(m_pJsonMon) m_pJsonMon->UpdateJsonMon(nModuleID);
			//ksj end
			break;
		}		
 	}	

	InitPreFlickerInfo(); //ksj 20201015

	return 1;
}

//ksj 20200601 작업 완료 메세지 핸들러
//수신된 채널데이터에서 상태가 작업중->Pause로 전이 될때 통지한다.
//채널데이터 수신부에서 감지하여 통지하므로 채널 번호는 알 수 있지만 어떤 모듈인지 구분이 불가능.
LRESULT CCTSMonProView::OnChannelTestPause(WPARAM wParam /*완료 채널 번호*/, LPARAM lParam/*해당 채널 객체 포인터*/)
{
	int nChNo = (WORD) wParam; //채널데이터의 채널번호는 1부터 시작된다.
	CCTSMonProDoc* pDoc = GetDocument();
	int nModuleID;
	int nTotalModule = pDoc->GetInstallModuleCount();
	CCyclerModule *pMD = NULL;
	CCyclerChannel *pChannel = NULL;


	for(nModuleID = 1; nModuleID <= pDoc->GetInstallModuleCount(); nModuleID++) //ksj 20160728 어떤 모듈인지 이 이벤트에서는 구분할 수 없으므로 모든 모듈 검색하며 일치하는 채널 찾기
	{
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChNo - 1);

		if(pChannel->GetChannel() == (CChannel*) lParam) //작업 일시정지 통지 메세지를 보낸 채널을 찾아서 보냄.
		{		
			theApp.Fun_SbcWorkPause(pChannel); //ksj 20200601

			//ksj 20200907 : [#JSonMon] 일시정지 될때 Json 파일 갱신. 주기적인 업데이트 호출 주기에 따라 굳이 안넣어도 됨.
			//모듈 단위로 파일이 생성되므로, 개별 채널 상태 바뀔때마다 기록하는 것은 무리가 있을 수 있음. Pack은 채널 수 적어서 넣어볼만 함.
			if(m_pJsonMon) m_pJsonMon->UpdateJsonMon(nModuleID);
			//ksj end
			break;
		}		
	}	

	InitPreFlickerInfo(); //ksj 20201015

	return 1;
}

int CCTSMonProView::Fun_Chk_RESRV_MUX_REC(int nOption, LPVOID pReservedWork,int ReserveID)//yulee 20181025_1
{
	TRACE(m_strMuxSetRetMsg); //$1013
	if(m_strMuxSetRetMsg.IsEmpty() == FALSE)
	{
		KillTimer(TIMER_RESRV_MUX_RECV_WAIT);
	}
	else
	{
		return 2;
	}
	
	/*
	Channel : 1 
	state : NG 
	 (setting : 1, state : 0, fault_flag 2)
	*/

	LPS_WORK_RESERVATION_DATA pWork = (LPS_WORK_RESERVATION_DATA) pReservedWork;

	UINT nModuleID = 0;
	UINT nChIndex = 0;
	//int ch;
	int nSelCount = m_adwTargetChArray.GetSize();
	
	// 200313 HKH Memory Leak 수정
	//TRACE(pWork->strAdwChArray);
	//DWORD dwData = atof(pWork->strAdwChArray);
	TRACE(pWork->szAdwChArray);
	DWORD dwData = atof(pWork->szAdwChArray);
	nModuleID = HIWORD(dwData);
	nChIndex = LOWORD(dwData);

	TRACE("Module %d, channel %d\n",  nModuleID, nChIndex);

	if(((m_ResvMuxRecvChNum-1) == nChIndex) &&
		(m_strResvMuxRecvState.Find("OK") > -1) &&
		(m_ResvMuxRecvSet == 1))
	{
		//작업 예약 시작시 Parameter 확인.
		if(nOption == WORK_START_RESERV_RUN && pWork == NULL)
		{	
			ASSERT(pWork);			
		}
		
		//변수 선언 및 초기화
		CCTSMonProDoc* pDoc = GetDocument();
		CPtrArray	aPtrSelCh;
		CString strTestName, strScheduleFileName;		
		BOOL bStartStepOption = TRUE;
		CString strLotNo;	
		int nModelPK = 0;
		int nTestPK = 0;
		CDWordArray adwChArray;
		BOOL	bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);	
		CScheduleData	scheduleInfo;
		CProgressWnd	progressWnd;
		CStartDlgEx		startDlg(GetDocument(), this);
		startDlg.m_bEnableStepSkip = bStartStepOption;
		startDlg.m_strLot = strLotNo;
		
		
		//전송 가능 채널 업데이트
		if(!RunProcess_ChUpdate(nOption, &bStartStepOption, &strLotNo, &aPtrSelCh))	
		{
			//pDoc->WriteSysLog("채널 업데이트 실패", CT_LOG_LEVEL_NORMAL);//$1013
			pDoc->WriteSysLog(Fun_FindMsg("Fun_Chk_RESRV_MUX_REC_msg1","IDD_CTSMonPro_FORM"), CT_LOG_LEVEL_NORMAL);//&&
			//	return RUN_FAIL;
		}
		
		m_pReserveDlg->RestoreReservedWork(pWork, &scheduleInfo, &adwChArray, &startDlg,&nModelPK,&nTestPK);
		
		
		//작업 시작 또는 예약 저장. //20180427 yulee 예약시작 시 패턴 path 가져오기에 대한 수정
		if(!RunProcess_WorkStart(nOption,&progressWnd,&adwChArray,&scheduleInfo,&startDlg,strTestName,nModelPK,nTestPK,strScheduleFileName, ReserveID))
		{
			//GetDocument()->WriteSysLog("작업 실패", CT_LOG_LEVEL_NORMAL);
			GetDocument()->WriteSysLog(Fun_FindMsg("CTSMonProView_Fun_Chk_RESRV_MUX_REC_msg1","IDD_CTSMonPro_FORM"), CT_LOG_LEVEL_NORMAL);//&&
			//	return RUN_FAIL;
		}
		return 1;
	}
	else
	{
		KillTimer(TIMER_RESRV_MUX_RECV_WAIT);
		return 0;
	}

}

//ksj 20160801 날짜 변경 체크. (일반 Cycler에서 가져온 코드)
void CCTSMonProView::Fun_DayChangeCheck()
{
	CCTSMonProDoc *pDoc = GetDocument();
	CString strDay = GWin::win_CurrentTime("filelog");
	if(m_tmpDay != strDay)	{
		if (pDoc->m_bWorkingFtpDown == FALSE)
		{
			pDoc->m_bDayChange = TRUE;
			m_tmpDay = strDay;
		}
		return;
	}
	
	m_tmpDay = strDay;
	
	if (pDoc->m_bWorkingFtpDown == FALSE)	pDoc->m_bRunFtpDown = TRUE;		//1분에 한번 씩 체크 한다. (작업 완료 했고 FTP down 했는지 체크)
}


//yulee 20190427SBC Data Down Done check
void CCTSMonProView::Fun_SBCDataDownDoneCheck()
{
	CCTSMonProDoc *pDoc = GetDocument();
	if (pDoc->m_bWorkingFtpDown == FALSE)
	{
		if (pDoc->m_bRunFtpDownAllSBCData == FALSE)
		{
			KillTimer(TIMER_SBCDATADOWN_DONE_CHECK);
			/*m_pUserInputStopNGoDlg->Fun_CloseWnd();*/
		}
		return;
	}
}

void CCTSMonProView::OnCmdSafetyCondUpade() 
{
	CString strMsg;
	CWorkWarnin WorkWarningDlg;
	//strMsg = "안전조건을 변경 하시겠습니까?";//$1013
	strMsg = Fun_FindMsg("OnparallelCon_msg5","IDD_CTSMonPro_FORM");//&&
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;

	//ljb 20170730
// 	CLoginManagerDlg LoginDlg;
// 	if(LoginDlg.DoModal() != IDOK) return;
	
	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.

	//if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_SAFETY_DATA_UPDATE)  == FALSE) //ksj 20200213 : Stop 명령 조건이 변경되어, 별도로 DATA_UPDATE 조건 추가.	
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("OnparallelCon_msg6","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
// 	strMsg.Format("[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
// 	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
// 		return;
	
	//3. Send Command
	//if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE)) AfxMessageBox("전송 실패");	

	if (m_adwTargetChArray.GetSize() > 1)
	{
		//AfxMessageBox("1채널만 선택 하세요.");//$1013
		AfxMessageBox(Fun_FindMsg("OnparallelCon_msg7","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	GetDocument()->Fun_SafetyUpdateExe(m_adwTargetChArray, GetDesktopWindow());
}

void CCTSMonProView::OnCmdSchStepUpdate() 
{
	CString strMsg;
	CWorkWarnin WorkWarningDlg;
	//strMsg = "STEP 조건을 변경 하시겠습니까?";//$1013
	strMsg = Fun_FindMsg("OnCmdSchStepUpdate_msg1","IDD_CTSMonPro_FORM");//&&
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;
	
	//ljb 20170730
	// 	CLoginManagerDlg LoginDlg;
	// 	if(LoginDlg.DoModal() != IDOK) return;
	
	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.

	//if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_STEP_DATA_UPDATE)  == FALSE) //ksj 20200213 : Stop 명령 조건이 변경되어, 별도로 DATA_UPDATE 조건 추가.	
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("OnCmdSchStepUpdate_msg2","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
	// 	strMsg.Format("[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	// 	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	// 		return;
	
	//3. Send Command
	//if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE)) AfxMessageBox("전송 실패");	
	
	if (m_adwTargetChArray.GetSize() > 1)
	{
		//AfxMessageBox("1채널만 선택 하세요.");//$1013
		AfxMessageBox(Fun_FindMsg("OnCmdSchStepUpdate_msg3","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
	GetDocument()->Fun_SchUpdateExe(m_adwTargetChArray, GetDesktopWindow());
}

void CCTSMonProView::OnCmdSchAllStepUpdate() 
{
	CString strMsg;
	CWorkWarnin WorkWarningDlg;
	//strMsg = "스케쥴을 변경 하시겠습니까?";//$1013
	strMsg = Fun_FindMsg("OnCmdSchAllStepUpdate_msg1","IDD_CTSMonPro_FORM");//&&
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;
	
	//ljb 20170730
	// 	CLoginManagerDlg LoginDlg;
	// 	if(LoginDlg.DoModal() != IDOK) return;
	
	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다.

	//if(UpdateSelectedChList(SFT_CMD_STOP)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_STEP_DATA_UPDATE)  == FALSE) //ksj 20200213 : Stop 명령 조건이 변경되어, 별도로 DATA_UPDATE 조건 추가.	
	{
		//AfxMessageBox("전송 가능한 채널이 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("OnCmdSchAllStepUpdate_msg2","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
	// 	strMsg.Format("[%s]을 실행하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	// 	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) 
	// 		return;
	
	//3. Send Command
	//if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE)) AfxMessageBox("전송 실패");	
	
	if (m_adwTargetChArray.GetSize() > 1)
	{
		//AfxMessageBox("1채널만 선택 하세요.");//$1013
		AfxMessageBox(Fun_FindMsg("OnCmdSchAllStepUpdate_msg3","IDD_CTSMonPro_FORM"));//&&
		return;
	}
	
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	CCyclerModule * pMD;
	CCyclerChannel * pChannel;
	
	int ch,nModuleID, nChIndex;
	for(ch = 0; ch<m_adwTargetChArray.GetSize(); ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());
	}
	Fun_SchAllUpdate(pChannel->GetTestPath(), pChannel->GetChFilePath());		//전체 스케쥴 업데이트
}

BOOL CCTSMonProView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default
//	CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
//	CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
	// 	HANDLE	handle = AfxGetInstanceHandle();		//자신의 핸들 얻기
	// 	HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");
	// TODO: Add your message handler code here and/or call default
	ASSERT(pCopyDataStruct);
	
	//BMA 에서 전송되어 온 Message (Bar Code)
	switch (pCopyDataStruct->dwData)
	{
	case SFTWM_SAFETY_INFO_DATA:
		AfxMessageBox("recv safety info data");
		break;
		
	}	
	return CFormView::OnCopyData(pWnd, pCopyDataStruct);
}

void CCTSMonProView::OnCellbalSet() 
{
	onCellbalShow(1);
}

void CCTSMonProView::onCellbalShow(int iType) 
{
	if(iType==1)
	{
		if(m_pDlgCellBal == NULL)
		{
			m_pDlgCellBal = new Dlg_CellBAL();
			//m_pDlgCellBal->Create(IDD_DIALOG_CELLBAL, this);
			m_pDlgCellBal->Create(//&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_CELLBAL): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_CELLBAL_ENG): //&&
				IDD_DIALOG_CELLBAL, this);//&&
			m_pDlgCellBal->ShowWindow(SW_SHOW);
		}
		if(m_pDlgCellBal == NULL) return;
		
		if(m_pDlgCellBal->IsWindowVisible())
		{
			m_pDlgCellBal->ShowWindow(SW_HIDE);
		}
		else
		{
			m_pDlgCellBal->CenterWindow(this);
			m_pDlgCellBal->ShowWindow(SW_SHOW);
		}
	}
	else if(iType==2)
	{
		if(g_AppInfo.iCellBalShow==1)
		{
			if(m_pDlgCellBal == NULL) return;
			
			m_pDlgCellBal->CenterWindow(this);
			m_pDlgCellBal->ShowWindow(SW_SHOW);
		}
	}
}


void CCTSMonProView::OnMenuWndPwr() 
{
	onPwrSupplyDeal(1);
}

void CCTSMonProView::OnUpdateMenuWndPwr(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUsePower = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1 , "Use", 0);
	if(bUsePower == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end


	if( m_pPwrSupplyStat1 && 
		m_pPwrSupplyStat1->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}	
}

void CCTSMonProView::OnMenuWndPwr2() 
{
	onPwrSupplyDeal(2);	
}

void CCTSMonProView::OnUpdateMenuWndPwr2(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUsePower = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2 , "Use", 0);
		if(bUsePower == 0) 
		{
			if (pCmdUI->m_pMenu!=NULL)
			{
				pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
				return;
			}
		}
		//ksj end

	if( m_pPwrSupplyStat2 && 
		m_pPwrSupplyStat2->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}	
}

void CCTSMonProView::OnMenuWndChiller1() 
{
	onChillerDeal(1,1);//Visible
}

void CCTSMonProView::OnUpdateMenuWndChiller1(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	if( m_pDetailChillerDlg1 && 
		m_pDetailChillerDlg1->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnMenuWndChiller2() 
{
	onChillerDeal(2,1);//Visible
}

void CCTSMonProView::OnUpdateMenuWndChiller2(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	if( m_pDetailChillerDlg2 && 
		m_pDetailChillerDlg2->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnMenuWndChiller3() 
{
	onChillerDeal(3,1);//Visible
}

void CCTSMonProView::OnUpdateMenuWndChiller3(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	if( m_pDetailChillerDlg3 && 
		m_pDetailChillerDlg3->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnMenuWndChiller4() 
{
	onChillerDeal(4,1);//Visible
}

void CCTSMonProView::OnUpdateMenuWndChiller4(CCmdUI* pCmdUI) 
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	if( m_pDetailChillerDlg4 && 
		m_pDetailChillerDlg4->IsWindowVisible())
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::onOvenUse(int nItem) 
{
	BOOL bUseOven;

	CString RegNameUse, strKeyConfig;
	RegNameUse.Format("Use Oven %d", nItem);
	strKeyConfig = CT_CONFIG_REG_SEC;
	
	if(nItem == 1)
	{
		bUseOven = AfxGetApp()->GetProfileInt(strKeyConfig, RegNameUse	, FALSE);

		if(bUseOven)
		{
			if(m_pDetailOvenDlg1 == NULL)
			{
				GetDocument()->m_nOvenPortLine = 1;
				m_pDetailOvenDlg1 = new CDetailOvenDlg(GetDocument());
				m_pDetailOvenDlg1->m_OvenLoc.Format(_T("Chamber 1")); //yulee 20190208
				//m_pDetailOvenDlg1->Create(IDD_DETAIL_OVEN_DLG, this);
				m_pDetailOvenDlg1->Create(//&&
					(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
					(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
					(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
					IDD_DETAIL_OVEN_DLG, this);//&&
			}
			m_pDetailOvenDlg1->ShowWindow(SW_SHOW);

			//ksj 20200130 : 주석처리.
			//CRect rectTemp;
			//m_pDetailOvenDlg1->GetWindowRect(rectTemp);
			//::SetWindowPos(m_pDetailOvenDlg1->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height()+100, SWP_SHOWWINDOW); //ksj 20200130 : 주석처리.
			
			m_pDetailOvenDlg1->onSizeSave(3);
		}
	}
	else if(nItem == 2)
	{
		bUseOven = AfxGetApp()->GetProfileInt(strKeyConfig, RegNameUse	, FALSE);

		if(bUseOven)
		{
			if(m_pDetailOvenDlg2 == NULL)
			{
				GetDocument()->m_nOvenPortLine = 2;
				m_pDetailOvenDlg2 = new CDetailOvenDlg(GetDocument());
				m_pDetailOvenDlg2->m_OvenLoc.Format(_T("Chamber 2")); //yulee 20190208
				//m_pDetailOvenDlg2->Create(IDD_DETAIL_OVEN_DLG, this);
				m_pDetailOvenDlg2->Create(//&&
					(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
					(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
					(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
					IDD_DETAIL_OVEN_DLG, this);//&&
			}
			m_pDetailOvenDlg2->ShowWindow(SW_SHOW);

			//ksj 20200130 : 주석처리.
			//CRect rectTemp;
			//m_pDetailOvenDlg2->GetWindowRect(rectTemp);
			//::SetWindowPos(m_pDetailOvenDlg2->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height()+100, SWP_SHOWWINDOW); //ksj 20200130 : 주석처리.
			m_pDetailOvenDlg2->onSizeSave(3);  
		}
	}
}

void CCTSMonProView::onUsePlacePlate()
{
	//yulee 20181002 화면에 작업 장소 표시 
	BOOL bUsePlacePlate;	
	bUsePlacePlate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", FALSE);

	if(bUsePlacePlate)
	{
		if(m_pWorkStationSignDlg == NULL)
		{
			m_pWorkStationSignDlg = new CWorkStationSignDlg();
			m_pWorkStationSignDlg->Create(IDD_WORKSTATIONSIGN_DLG, this);
		}
		m_pWorkStationSignDlg->ShowWindow(SW_SHOW);
		CRect rectTemp;
		m_pWorkStationSignDlg->GetWindowRect(rectTemp);
		::SetWindowPos(m_pWorkStationSignDlg->m_hWnd, HWND_TOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
		m_pWorkStationSignDlg->ShowWindow(SW_SHOWMINIMIZED); //yulee 20181005
	}
}

void CCTSMonProView::onPwrSupplyUse(int iType) 
{
	if(iType==1)
	{//PowerSupply1
		BOOL bUsePwrSupply = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1, "Use", FALSE);
		if(bUsePwrSupply)
		{
			onPwrSupplyDeal(iType,1);
		}
	}
	else if(iType==2)
	{//PowerSupply2
		BOOL bUsePwrSupply = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2, "Use", FALSE);
		if(bUsePwrSupply)
		{
			onPwrSupplyDeal(iType,1);
		}
	}
}

void CCTSMonProView::onPwrSupplyDeal(int iType,int iVisible) 
{
	if(iType==1)
	{//PowerSupply1
		if(m_pPwrSupplyStat1 == NULL)
		{
			m_pPwrSupplyStat1 = new Dlg_PwrSupplyStat(GetDocument());
			//m_pPwrSupplyStat1->Create(IDD_DIALOG_PWRSUPPLY_STAT, this);
			m_pPwrSupplyStat1->Create(//&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_PWRSUPPLY_STAT): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_PWRSUPPLY_STAT_ENG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DIALOG_PWRSUPPLY_STAT_PL): //&&
				IDD_DIALOG_PWRSUPPLY_STAT, this);//&&
			m_pPwrSupplyStat1->m_nItem=0;
			
			CRect rectTemp;
			m_pPwrSupplyStat1->GetWindowRect(rectTemp);
			::SetWindowPos(m_pPwrSupplyStat1->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
			m_pPwrSupplyStat1->onSizeSave(3);
		}
		if(iVisible==1)
		{
			m_pPwrSupplyStat1->ShowWindow(SW_SHOW);	
			return;
		}
		if(iVisible==0)
		{
			m_pPwrSupplyStat1->ShowWindow(SW_HIDE);	
			return;
		}
		if(m_pPwrSupplyStat1->IsWindowVisible())
		{
			m_pPwrSupplyStat1->ShowWindow(SW_HIDE);				
		}
		else
		{
			m_pPwrSupplyStat1->ShowWindow(SW_SHOW);			
		}
	}
	else if(iType==2)
	{//PowerSupply2
		if(m_pPwrSupplyStat2 == NULL)
		{
			m_pPwrSupplyStat2 = new Dlg_PwrSupplyStat(GetDocument());
			//m_pPwrSupplyStat2->Create(IDD_DIALOG_PWRSUPPLY_STAT, this);
			m_pPwrSupplyStat2->Create(//&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_PWRSUPPLY_STAT): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_PWRSUPPLY_STAT_ENG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DIALOG_PWRSUPPLY_STAT_PL): //&&
				IDD_DIALOG_PWRSUPPLY_STAT, this);//&&
			m_pPwrSupplyStat2->m_nItem=1;
			
			CRect rectTemp;
			m_pPwrSupplyStat2->GetWindowRect(rectTemp);
			::SetWindowPos(m_pPwrSupplyStat2->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
			m_pPwrSupplyStat2->onSizeSave(3);
		}
		if(iVisible==1)
		{
			m_pPwrSupplyStat2->ShowWindow(SW_SHOW);	
			return;
		}
		if(iVisible==0)
		{
			m_pPwrSupplyStat2->ShowWindow(SW_HIDE);	
			return;
		}
		if(m_pPwrSupplyStat2->IsWindowVisible())
		{
			m_pPwrSupplyStat2->ShowWindow(SW_HIDE);			
		}
		else
		{
			m_pPwrSupplyStat2->ShowWindow(SW_SHOW);			
		}
	}
}


void CCTSMonProView::onChillerUse(int iType) 
{
	if(iType==1)
	{//Chiller1
		BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1, "Use", FALSE);
		if(bUseChiller)
		{
			onChillerDeal(iType,1);
		}
	}
	else if(iType==2)
	{//Chiller2
		BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2, "Use", FALSE);
		if(bUseChiller)
		{
			onChillerDeal(iType,1);
		}
	}
	else if(iType==3)
	{//Chiller3
		BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3, "Use", FALSE);
		if(bUseChiller)
		{
			onChillerDeal(iType,1);
		}
	}
	else if(iType==4)
	{//Chiller4
		BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4, "Use", FALSE);
		if(bUseChiller)
		{
			onChillerDeal(iType,1);
		}
	}
}

void CCTSMonProView::onChillerDeal(int iType,int iVisible)
{
	if(iType==1)
	{
		if(m_pDetailChillerDlg1 == NULL)
		{
			GetDocument()->m_nOvenPortLine = 11;
			m_pDetailChillerDlg1 = new CDetailOvenDlg(GetDocument());
			m_pDetailChillerDlg1->m_OvenLoc.Format(_T("Chiller 1")); //yulee 20190208
			//m_pDetailChillerDlg1->Create(IDD_DETAIL_OVEN_DLG, this);
			m_pDetailChillerDlg1->Create(//&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
			IDD_DETAIL_OVEN_DLG, this);//&&

			m_pDetailChillerDlg1->ShowWindow(SW_SHOW);
			CRect rectTemp;
			m_pDetailChillerDlg1->GetWindowRect(rectTemp);
			::SetWindowPos(m_pDetailChillerDlg1->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
			m_pDetailChillerDlg1->onSizeSave(3);
		}
		if(iVisible==1)
		{
			m_pDetailChillerDlg1->ShowWindow(SW_SHOW);	
			return;
		}
		if(iVisible==0)
		{
			m_pDetailChillerDlg1->ShowWindow(SW_HIDE);	
			return;
		}
		
		if(m_pDetailChillerDlg1->IsWindowVisible())
		{
			m_pDetailChillerDlg1->ShowWindow(SW_HIDE);				
		}
		else
		{
			m_pDetailChillerDlg1->ShowWindow(SW_SHOW);			
		}
	}
	else if(iType==2)
	{
		if(m_pDetailChillerDlg2 == NULL)
		{
			GetDocument()->m_nOvenPortLine = 12;
			m_pDetailChillerDlg2 = new CDetailOvenDlg(GetDocument());
			m_pDetailChillerDlg2->m_OvenLoc.Format(_T("Chiller 2")); //yulee 20190208
			//m_pDetailChillerDlg2->Create(IDD_DETAIL_OVEN_DLG, this);
			m_pDetailChillerDlg2->Create(//&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
				IDD_DETAIL_OVEN_DLG, this);//&&

			m_pDetailChillerDlg2->ShowWindow(SW_SHOW);
			CRect rectTemp;
			m_pDetailChillerDlg2->GetWindowRect(rectTemp);
			::SetWindowPos(m_pDetailChillerDlg2->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);	
			m_pDetailChillerDlg2->onSizeSave(3);
		}
		if(iVisible==1)
		{
			m_pDetailChillerDlg2->ShowWindow(SW_SHOW);	
			return;
		}
		if(iVisible==0)
		{
			m_pDetailChillerDlg2->ShowWindow(SW_HIDE);	
			return;
		}
		if(m_pDetailChillerDlg2->IsWindowVisible())
		{
			m_pDetailChillerDlg2->ShowWindow(SW_HIDE);				
		}
		else
		{
			m_pDetailChillerDlg2->ShowWindow(SW_SHOW);			
		}
	}
	else if(iType==3)
	{
		if(m_pDetailChillerDlg3 == NULL)
		{
			GetDocument()->m_nOvenPortLine = 13;
			m_pDetailChillerDlg3 = new CDetailOvenDlg(GetDocument());
			//m_pDetailChillerDlg3->Create(IDD_DETAIL_OVEN_DLG, this);
			m_pDetailChillerDlg3->Create(//&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
				IDD_DETAIL_OVEN_DLG, this);//&&

			m_pDetailChillerDlg3->ShowWindow(SW_SHOW);
			CRect rectTemp;
			m_pDetailChillerDlg3->GetWindowRect(rectTemp);
			::SetWindowPos(m_pDetailChillerDlg3->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
			m_pDetailChillerDlg3->onSizeSave(3);
		}
		if(iVisible==1)
		{
			m_pDetailChillerDlg3->ShowWindow(SW_SHOW);	
			return;
		}
		if(iVisible==0)
		{
			m_pDetailChillerDlg3->ShowWindow(SW_HIDE);	
			return;
		}
		if(m_pDetailChillerDlg3->IsWindowVisible())
		{
			m_pDetailChillerDlg3->ShowWindow(SW_HIDE);				
		}
		else
		{
			m_pDetailChillerDlg3->ShowWindow(SW_SHOW);			
		}
	}
	else if(iType==4)
	{
		if(m_pDetailChillerDlg4 == NULL)
		{
			GetDocument()->m_nOvenPortLine = 14;
			m_pDetailChillerDlg4 = new CDetailOvenDlg(GetDocument());
			//m_pDetailChillerDlg4->Create(IDD_DETAIL_OVEN_DLG, this);
			m_pDetailChillerDlg4->Create(//&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DETAIL_OVEN_DLG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DETAIL_OVEN_DLG_ENG): //&&
				(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_DETAIL_OVEN_DLG_PL): //&&
				IDD_DETAIL_OVEN_DLG, this);//&&
		
			m_pDetailChillerDlg4->ShowWindow(SW_SHOW);
			CRect rectTemp;
			m_pDetailChillerDlg4->GetWindowRect(rectTemp);
			::SetWindowPos(m_pDetailChillerDlg4->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
			m_pDetailChillerDlg4->onSizeSave(3);
		}
		if(iVisible==1)
		{
			m_pDetailChillerDlg4->ShowWindow(SW_SHOW);	
			return;
		}
		if(iVisible==0)
		{
			m_pDetailChillerDlg4->ShowWindow(SW_HIDE);	
			return;
		}
		if(m_pDetailChillerDlg4->IsWindowVisible())
		{
			m_pDetailChillerDlg4->ShowWindow(SW_HIDE);				
		}
		else
		{
			m_pDetailChillerDlg4->ShowWindow(SW_SHOW);			
		}
	}
}

void CCTSMonProView::OnBtnDaqIsolationEndA1() 
{
	// TODO: Add your control notification handler code here
	
	CString strMsg;
	strMsg.Format("Non-Isolation(A), Continue?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "Confirm Windows", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	//Fun_IsolationEnd_Ch(1);
	Fun_IsolationEnd(1,1);	
}

void CCTSMonProView::OnBtnDaqIsolationEndB1() 
{
	// TODO: Add your control notification handler code here
	
	CString strMsg;
	strMsg.Format("Non-Isolation(B), Continue?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "Confirm Windows", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	//Fun_IsolationEnd_Ch(2);
	Fun_IsolationEnd(2,1);	
}

void CCTSMonProView::OnBtnDaqIsolationEndA2() 
{
	// TODO: Add your control notification handler code here
	
	CString strMsg;
	strMsg.Format("Non-Isolation(A), Continue?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "Confirm Windows", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	//Fun_IsolationEnd_Ch(1);	
	Fun_IsolationEnd(1,2);
}

void CCTSMonProView::OnBtnDaqIsolationEndB2() 
{
	CString strMsg;
	strMsg.Format("Non-Isolation(B), Continue?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "Confirm Windows", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	//Fun_IsolationEnd_Ch(2);	
	Fun_IsolationEnd(2,2);
}

void CCTSMonProView::OnBtnDaqIsolationStartAb1() 
{
	// TODO: Add your control notification handler code here
	CString strMsg;
	strMsg.Format("Isolation(A, B), Continue?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "Confirm Windows", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	//Fun_IsolationStart_Ch(0);
	Fun_IsolationStart(0, 1);
}

void CCTSMonProView::OnBtnDaqIsolationStartAb2() 
{
	// TODO: Add your control notification handler code here
	CString strMsg;
	strMsg.Format("Isolation(A, B), Continue?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "Confirm Windows", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	//Fun_IsolationStart_Ch(0);
	Fun_IsolationStart(0,2);
}


//BOOL CCTSMonProView::DownAllRTTableFromSBC()  //yulee 20181114
BOOL CCTSMonProView::DownAllRTTableFromSBC(int nSelectedRtType)  //ksj 20200207
{	
	//현재 모듈이 접속이 안되어 있으면 안내 창을 띄우고 기능 진행 안함.

	if(UpdateSelectedChList()  == FALSE)//$1013
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return 0; 		
	}

	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");//$1013
		return 0;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return 0;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			AfxMessageBox(Fun_FindMsg("OnCanConfigLintoCANTX_msg3","IDD_CTSMonPro_FORM")); //&&
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");//$1013
//#ifndef _DEBUG
			return 0;
//#endif
		}
	}		




	//※FTP 접속
	//int nModuleID; 
	nModuleID = 1;
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	//CCyclerModule *pMD;
	
	CString strIPAddress;
	
	pMD = pDoc->GetModuleInfo(nModuleID);
	strIPAddress.Format("%s", pMD->GetIPAddress());
	// TODO: Add your control notification handler code here
	CString srcFileName, destFileName;
	CString strTemp;
	
	if(strIPAddress.IsEmpty())
	{
		//strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	nModuleID);//$1013
		strTemp.Format(Fun_FindMsg("DownAllRTTableFromSBC_msg1","IDD_CTSMonPro_FORM"),	nModuleID);//&&
		AfxMessageBox(strTemp);
		return 0;
	}
	
	//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), "Thermistor Table Name, LastWriteTime...", TRUE, TRUE, TRUE);
	progressWnd.SetRange(0, 100);
	//////////////////////////////////////////////////////////////////////////
	
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	//Get ip address
	
	//////////////////////////////////////////////////////////////////////////
	//	strIPAddress = "192.168.9.10";
	//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
	//////////////////////////////////////////////////////////////////////////
	
	//strTemp.Format("Module %d에 접속 시도 중입니다.(IP %s)", nModuleID, strIPAddress);//$1013
	strTemp.Format(Fun_FindMsg("DownAllRTTableFromSBC_msg2","IDD_CTSMonPro_FORM"), nModuleID, strIPAddress);//&&
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		//m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", nModuleID);//$1013
		m_strLastErrorString.Format(Fun_FindMsg("DownAllRTTableFromSBC_msg3","IDD_CTSMonPro_FORM"), nModuleID);//&&
		delete pDownLoad;
		return 0;
	}
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	//※FTP 파일 다운
	
	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalTempFolder(szBuff);
	
	//strRemoteFileName.Format("/root/cycler_data/config/parameter/th_table//th_table_01.txt");
	//ksj 20200207
	if(nSelectedRtType == PS_AUX_TYPE_HUMIDITY)
	{
		strRemoteFileName.Format("/root/cycler_data/config/parameter/humidity_table//th_table_01.txt");
	}
	else //PS_AUX_TYPE_TEMPERATURE
	{
		strRemoteFileName.Format("/root/cycler_data/config/parameter/th_table//th_table_01.txt");
	}
	
	CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
	CString strRemoteFolder;
	//strRemoteFolder.Format("/root/cycler_data/config/parameter/th_table//");
	//strRemoteFolder.Format("/root/cycler_data/config/parameter/th_table//th_table_01.txt");


	//ksj 20200207
	CString strDownPath;
	if(nSelectedRtType == PS_AUX_TYPE_HUMIDITY)
	{
		strRemoteFolder.Format("/root/cycler_data/config/parameter/humidity_table//th_table_01.txt");
		strDownPath.Format("%s\\RT_table\\Humidity",strCurPath);
		pDoc->ForceDirectory(strDownPath);  //ksj 20200207 : 없으면 생성
	}
	else //PS_AUX_TYPE_TEMPERATURE
	{
		strRemoteFolder.Format("/root/cycler_data/config/parameter/th_table//th_table_01.txt");
		strDownPath.Format("%s\\RT_table",strCurPath);
		pDoc->ForceDirectory(strDownPath); //ksj 20200207 : 없으면 생성
	}
	
	//해당 디렉토리가 없으면
// 	CString strDownPath;
// 	strDownPath.Format("%s\\RT_table",strCurPath);

	CFileFind file;
	CString strFile = "*.*", strChkPath;
	strChkPath.Format("%s\\%s", strDownPath, strFile);
	BOOL bResult = file.FindFile(strChkPath);

	if(!bResult)
	{
		bResult = CreateDirectory(strDownPath + "\\", NULL);
		if(!bResult)
			return 0;
	}
	
	int nDownCount = pDownLoad->DownLoadAllFile(strDownPath, strRemoteFolder, nModuleID);
	if( nDownCount < 1)
	{
		m_strLastErrorString.Format("RT_table download failed");
		delete pDownLoad;
		return 0;
	}
	else
	{
		strTemp.Format("FTP Download count = %d",nDownCount);
		TRACE(strTemp);
	}
	
	delete pDownLoad;
	return 1;
}


//BOOL CCTSMonProView::UploadRTTableFromSBC(int nFileNum, CString strFileName, int nTypeOper, int nRtType)
BOOL CCTSMonProView::UploadRTTableFromSBC(int nFileNum, CString strFileName, int nTypeOper, int nRtType) //ksj 20200207
{
	//현재 모듈이 접속이 안되어 있으면 안내 창을 띄우고 기능 진행 안함.

	if(UpdateSelectedChList()  == FALSE)//$1013
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("UploadRTTableFromSBC_msg1","IDD_CTSMonPro_FORM")); //&&
		return 0; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요.");//$1013
		AfxMessageBox(Fun_FindMsg("UploadRTTableFromSBC_msg2","IDD_CTSMonPro_FORM")); //&&	
		return 0;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return 0;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");//$1013
			AfxMessageBox(Fun_FindMsg("UploadRTTableFromSBC_msg3","IDD_CTSMonPro_FORM")); //&&	
//#ifndef _DEBUG
			return 0;
//#endif
		}
	}		
	//※FTP 접속
	//int nModuleID; 
	nModuleID = 1;
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	//CCyclerModule *pMD;
	
	CString strIPAddress;
	
	pMD = pDoc->GetModuleInfo(nModuleID);
	strIPAddress.Format("%s", pMD->GetIPAddress());
	// TODO: Add your control notification handler code here
	CString srcFileName, destFileName;
	CString strTemp;
	
	if(strIPAddress.IsEmpty())
	{
		//strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	nModuleID);//$1013
		strTemp.Format(Fun_FindMsg("DownAllRTTableFromSBC_msg1","IDD_CTSMonPro_FORM"),	nModuleID);//&&
		AfxMessageBox(strTemp);
		return 0;
	}
	
	//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), "Thermistor Table Name, LastWriteTime...", TRUE, TRUE, TRUE);
	progressWnd.SetRange(0, 100);
	//////////////////////////////////////////////////////////////////////////
	
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	//Get ip address
	
	//////////////////////////////////////////////////////////////////////////
	//	strIPAddress = "192.168.9.10";
	//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
	//////////////////////////////////////////////////////////////////////////
	
	//strTemp.Format("Module %d에 접속 시도 중입니다.(IP %s)", nModuleID, strIPAddress);//$1013
	strTemp.Format(Fun_FindMsg("UploadRTTableFromSBC_msg5","IDD_CTSMonPro_FORM"), nModuleID, strIPAddress);//&&
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		//m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", nModuleID);//$1013
		m_strLastErrorString.Format(Fun_FindMsg("UploadRTTableFromSBC_msg6","IDD_CTSMonPro_FORM"), nModuleID);//&&
		delete pDownLoad;
		return 0;
	}
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	//※FTP 업로드할 파일 가져오기 
	int nUploadFileNum;
	CString strArrFileName[10];
	nUploadFileNum = 0;
	if(nTypeOper == 0)
	{
		for(int i=0; i<nFileNum; i++)
		{
			if(i < 10)
				strArrFileName[i].Format("%s_%02d.txt", strFileName.Left(strFileName.Find('_')), i+1);
			else if(i == 10)
				strArrFileName[i].Format("%s_%d.txt", strFileName.Left(strFileName.Find('_')), i+1);
		}
	}
	else if(nTypeOper == 1)
	{
		strArrFileName[0].Format("%s", strFileName);
		nUploadFileNum = 1;
	}

	
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	//※FTP 파일 업로드
	for(int i=0; i< nUploadFileNum; i++)
	{
		char szBuff[_MAX_PATH];
		::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
		CString strRemoteFileName, strLocalTempFolder(szBuff);

		//strRemoteFileName.Format("/root/cycler_data/config/parameter/th_table//%s", strArrFileName[i]);
		if(nRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
			strRemoteFileName.Format("/root/cycler_data/config/parameter/humidity_table//%s", strArrFileName[i]);
		else
			strRemoteFileName.Format("/root/cycler_data/config/parameter/th_table//%s", strArrFileName[i]);
	
		
		CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
		CString strUploadFile;
		//strRemoteFolder.Format("/root/cycler_data/config/parameter/th_table//");
		//strUploadFile.Format("/root/cycler_data/config/parameter/th_table");
		if(nRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
			strUploadFile.Format("/root/cycler_data/config/parameter/humidity_table");
		else
			strUploadFile.Format("/root/cycler_data/config/parameter/th_table");
		
		
		CString strPCsideFile;
		//strPCsideFile.Format("%s\\RT_table\\%s",strCurPath, strArrFileName[i]);
		if(nRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
			strPCsideFile.Format("%s\\RT_table\\Humidity\\%s",strCurPath, strArrFileName[i]);
		else
			strPCsideFile.Format("%s\\RT_table\\%s",strCurPath, strArrFileName[i]);
		
		int nDownCount = pDownLoad->UploadFile(strPCsideFile, strRemoteFileName, nModuleID);
		if( nDownCount < 1)
		{
			//m_strLastErrorString.Format("RT_table download failed");
			m_strLastErrorString.Format(" RT_table download failed Rtn:%d",nDownCount); //lyj 20210106 RT테이블 업로드 실패 로그 강화
			pDoc->WriteLog(m_strLastErrorString); //lyj 20210106 RT테이블 업로드 실패 로그 강화
			delete pDownLoad;
			return 0;
		}
		else
		{
			//strTemp.Format("FTP Download count = %d",nDownCount);
			strTemp.Format("RTTable Upload %s",strRemoteFileName); //lyj 20210106 RT테이블 업로드 로그 강화
			pDoc->WriteLog(strTemp); //lyj 20210106 RT테이블 업로드 로그 강화
			TRACE(strTemp);
		}
		
		delete pDownLoad;
	}
	return 1;
}


//BOOL CCTSMonProView::DeleteRTTableNameDB()  //yulee 20181120
BOOL CCTSMonProView::DeleteRTTableNameDB(int nDIvision)  //ksj 20200207
{
	CString strPackMDB;
	strPackMDB = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC ,"Path");	//Get Current Folder(CTSMon Folder)
	
	
// 	switch(nLanguage)20190701
// 	{
// 	case 1 : strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000.mdb"; break;
// 	case 2 : strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	case 3 : strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000_PL.mdb"; break;
// 	default : strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	}
		
	strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000.mdb";

	CDaoDatabase  dbPack;
	try
	{
		dbPack.Open(strPackMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CString strSQL;
	
	int nTmpI;//yulee 20181120

	//nTmpI = 3;	
	//strSQL.Format("DELETE FROM DivisionCode WHERE Division = %d", nTmpI);
	strSQL.Format("DELETE FROM DivisionCode WHERE Division = %d", nDIvision); //ksj 20200207
	
	dbPack.Execute(strSQL);

	
	dbPack.CanTransact();
	dbPack.Close();
	
	//CDaoTableDefInfo 
	CDaoFieldInfo  fieldinfo;
	return 1;
}

BOOL CCTSMonProView::UpdateRTTableDB(int nCode, CString strCodeName, int nDivision, CString strReserved)  //yulee 20181120
{
	CString strPackMDB;
	strPackMDB = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC ,"Path");	//Get Current Folder(CTSMon Folde)
	
// 	switch(nLanguage)20190701
// 	{
// 	case 1 : strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000.mdb"; break;
// 	case 2 : strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	case 3 : strPackMDB = strPackMDB+ "\\DataBase\\"+ "Pack_Schedule_2000_PL.mdb"; break;
// 	default : strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000_EN.mdb"; break;
// 	}
	
	strPackMDB = strPackMDB+ "\\DataBase\\" + "Pack_Schedule_2000.mdb";

	CDaoDatabase  dbPack;
	try
	{
		dbPack.Open(strPackMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CString strSQL;

	strSQL.Format("INSERT INTO DivisionCode(Code,Code_Name,Division,Description) VALUES (%d, '%s', %d, '%s')", nCode, strCodeName, nDivision, strReserved);
	dbPack.Execute(strSQL);

	dbPack.CanTransact();
	dbPack.Close();
	
	//CDaoTableDefInfo 
	CDaoFieldInfo  fieldinfo;
	return 1;
}

//BOOL CCTSMonProView::SetRTTableSendToModule(int nSelModule, SFT_RT_TABLE_SEND_END *RT_TableSendEnd)  //yulee 20181120
BOOL CCTSMonProView::SetRTTableSendToModule(int nSelModule, void *RT_TableSendEnd, int nRtType)  //ksj 20200207
{

	//GetDocument()->SetRTTableSendEndSignalToModule(m_nCurrentModuleID, RT_TableSendEnd);
	//return 1;

	return GetDocument()->SetRTTableSendEndSignalToModule(m_nCurrentModuleID, RT_TableSendEnd, nRtType); //ksj 20200207	
}

BOOL CCTSMonProView::GetThTableLastWriteTime(CString strTargetFileName, CTime &retCtime)
{
	//※FTP 접속
	int nModuleID; 
	nModuleID = 1;
	CCTSMonProDoc* pDoc = (CCTSMonProDoc *)GetDocument();
	CCyclerModule *pMD;
	
	CString strIPAddress;
	
	pMD = pDoc->GetModuleInfo(nModuleID);
	strIPAddress.Format("%s", pMD->GetIPAddress());
	// TODO: Add your control notification handler code here
	CString srcFileName, destFileName;
	CString strTemp;
	
	if(strIPAddress.IsEmpty())
	{
		//strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	nModuleID);//$1013
		strTemp.Format(Fun_FindMsg("GetThTableLastWriteTime_msg1","IDD_CTSMonPro_FORM"),	nModuleID);//&&
		AfxMessageBox(strTemp);
		return 0;
	}
	
	//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), "Thermistor Table Name, LastWriteTime...", TRUE, TRUE, TRUE);
	progressWnd.SetRange(0, 100);
	//////////////////////////////////////////////////////////////////////////
	
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	//Get ip address
	
	//////////////////////////////////////////////////////////////////////////
	//	strIPAddress = "192.168.9.10";
	//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
	//////////////////////////////////////////////////////////////////////////
	
	//strTemp.Format("Module %d에 접속 시도 중입니다.(IP %s)", nModuleID, strIPAddress);
	strTemp.Format(Fun_FindMsg("CTSMonProView_GetThTableLastWriteTime_msg1","IDD_CTSMonPro_FORM"), nModuleID, strIPAddress);//&&
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		//m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", nModuleID);
		m_strLastErrorString.Format(Fun_FindMsg("CTSMonProView_GetThTableLastWriteTime_msg2","IDD_CTSMonPro_FORM"), nModuleID);//&&
		delete pDownLoad;
		return 0;
	}
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	//※FTP 파일 다운
	
	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalTempFolder(szBuff);
	
	strRemoteFileName.Format("/root/cycler_data/config/parameter/th_table//");
	strRemoteFileName += strTargetFileName; 

	if(pDownLoad->GetLastWriteTimeByName(strRemoteFileName, retCtime) == FALSE) //yulee 20181029
	{
		//m_strLastErrorString.Format("시간 정보 요청을 실패 하였습니다.");
		m_strLastErrorString.Format(Fun_FindMsg("CTSMonProView_GetThTableLastWriteTime_msg3","IDD_CTSMonPro_FORM"));//&&
		delete pDownLoad;
		TRACE("%s download fail\n", strRemoteFileName);
		return 0;
	}
	else
		return 1;
}

void CCTSMonProView::OnMenubarThtableSet() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg1","IDD_CTSMonPro_FORM")); //&&
#ifdef _DEBUG
#else
		return; 		
#endif
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg2","IDD_CTSMonPro_FORM")); //&&
		return;
	}

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");//$1013
			AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3","IDD_CTSMonPro_FORM")); //&&

#ifndef _DEBUG
			return;

#endif
		}
	}		
	onRTtableShow(1);
}

void CCTSMonProView::onRTtableShow(int iType)
{
	if(iType==1)
	{
		if(m_pRTtableSet == NULL)
		{
			m_pRTtableSet = new CRTTableSet();
			switch(gi_Language) //20190630
			{
			case 1 : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG, this); break;
			case 2 : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG_ENG, this); break;
			case 3 : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG_PL, this); break;
			default : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG_ENG, this); break;
			}
			//m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG, this);
			m_pRTtableSet->CenterWindow(this);
			m_pRTtableSet->ShowWindow(SW_SHOW);

		}
		else
		{
			if(m_pRTtableSet->IsWindowVisible())
			{
				m_pRTtableSet->ShowWindow(SW_HIDE);
			}
			else
			{
				m_pRTtableSet = new CRTTableSet(); 
				switch(gi_Language) //20190630
				{
				case 1 : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG, this); break;
				case 2 : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG_ENG, this); break;
				case 3 : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG_PL, this); break;
				default : m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG_ENG, this); break;
				}
				//m_pRTtableSet->Create(IDD_RTTABLE_SET_DLG, this);
				m_pRTtableSet->CenterWindow(this);
 				m_pRTtableSet->ShowWindow(SW_SHOW);
			}
		}
	}
}

void CCTSMonProView::OnBtnEmg() 
{
	CString strMsg;
	CWorkWarnin WorkWarningDlg;
	//strMsg = "비상정지(Emgergency)를 진행하시겠습니까?";
	strMsg = Fun_FindMsg("CTSMonProView_OnBtnEmg_msg1","IDD_CTSMonPro_FORM");//&&
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL) return;

// 	//1. 선택된 채널 중 Stop 명령이 전송가능한 채널을 Update 한다. //yulee 20190706 무조건 Stop 시킨다.
// 	if(UpdateSelectedChListEMG(SFT_CMD_PAUSE)  == FALSE)
// 	{
// 		//AfxMessageBox("전송 가능한 채널이 없습니다.");
// 		AfxMessageBox(Fun_FindMsg("OnBtnEmg_msg1")); //&&
// 		return;
// 	}
		
	int nEmgType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "EMG BTN Type",0); //ksj 20201222 : 0: PAUSE 1: EMG Shutdown

	if(nEmgType == 0)
	{
		//2. Send Command
		if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_PAUSE)) //AfxMessageBox("전송 실패");		
			AfxMessageBox(Fun_FindMsg("OnBtnEmg_msg2","IDD_CTSMonPro_FORM")); //&&
	}
	else //ksj 20201222 : 삼성 SDI는 EMG Shutdown 처리 필요. 셧다운 커맨드 있는 설비에서만 동작함.!!!
	{
		//2. Send Command
		if (FALSE == SendCmdToModule(m_adwTargetChArray, SFT_CMD_USER_EMG)) //AfxMessageBox("전송 실패");		
			AfxMessageBox(Fun_FindMsg("OnBtnEmg_msg2","IDD_CTSMonPro_FORM")); //&&
	}	
}

void CCTSMonProView::OnUpdateCanLintocanRx(CCmdUI* pCmdUI) 
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseLinCanSettingDlg  == 0) 
	//if(m_bUseLinCanSettingDlg == 0 && m_bUseCanMux == FALSE)  //ksj 20201208
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
	/*
	//ksj 20201208 : Can Mux 기능 사용시 메뉴 이름 강제 변경. //ksj 20201223 : 기능 취소됨
	if(m_bUseCanMux == TRUE)
	{
		pCmdUI->SetText("CAN B Receive Setting");
	}*/

	
	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("먼저 선택 채널을 선택 하십시요.");//$1013
		AfxMessageBox(Fun_FindMsg("OnUpdateCanLintocanRx_msg1","IDD_CTSMonPro_FORM")); //&&
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			//AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3","IDD_CTSMonPro_FORM")); //&&
//#ifndef _DEBUG
			return;
//#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	SFT_CAN_COMMON_DATA commonData = pMD->GetCANCommonData(nChIndex, PS_CAN_TYPE_MASTER);
	if (commonData.can_lin_select == 1)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnUpdateCanLintocanTx(CCmdUI* pCmdUI) 
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseLinCanSettingDlg  == 0) 
	//if(m_bUseLinCanSettingDlg == 0 && m_bUseCanMux == FALSE)  //ksj 20201208
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
	/*
	//ksj 20201208 : Can Mux 기능 사용시 메뉴 이름 강제 변경. //ksj 20201223 : 기능 취소됨
	if(m_bUseCanMux == TRUE)
	{
		pCmdUI->SetText("CAN B Transmit Setting");
	}*/

	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("먼저 선택 채널을 선택 하십시요.");//$1013
		//AfxMessageBox(Fun_FindMsg("OnUpdateCanLintocanTx_msg1","IDD_CTSMonPro_FORM")); //&&
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			//AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3")); //&&
//#ifndef _DEBUG
			return;
//#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nChIndex, PS_CAN_TYPE_MASTER);
	if (commonData.can_lin_select == 1)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::onShowMessage(int iType,CString strTitle,CString strMessage)
{
	if(iType==1)
	{
		if(m_strWarnTitle.IsEmpty()==FALSE && m_strWarnTitle!=_T("")) return;
		if(m_strWarnMessage.IsEmpty()==FALSE && m_strWarnMessage!=_T("")) return;

		m_strWarnTitle=strTitle;
		m_strWarnMessage=strMessage;

		struct ST_TIMER_SHOWMESSAGE{
		static void CALLBACK TimeProcShowMessage(HWND hwnd,UINT iMsg,UINT_PTR wParam,DWORD lParam)
		{
			::KillTimer(hwnd,wParam);
			CCTSMonProView *dlg = (CCTSMonProView*)CWnd::FromHandle(hwnd);
			if(!dlg) return;

			dlg->onShowMessage(2,dlg->m_strWarnTitle,dlg->m_strWarnMessage);
		}};
		SetTimer(1023,100,ST_TIMER_SHOWMESSAGE::TimeProcShowMessage);
	}
	else
	{
		CShowEmgDlg dlg(this);
		dlg.SetWarnData(strTitle,strMessage);
		dlg.DoModal();

		m_strWarnTitle=_T("");
		m_strWarnMessage=_T("");
	}
}


void CCTSMonProView::OnBTNBuzzerStop() //yulee 20190531 추가
{
	// TODO: Add your control notification handler code here
	OnButStopBuzzer();
	KillTimer(TIMER_WINDOW_FLICKERING);
	m_FlagBackgroundColor = 1;
// 	m_FPreFickerInfo.nCode = 0; //lyj 20201007 초기화
// 	m_FPreFickerInfo.nPriority = 3; //lyj 20201007 초기화
// 	m_FPreFickerInfo.nAutoBuzzerStop = 0; //ksj 20201013 : 초기화 추가
	InitPreFlickerInfo(); //ksj 20201013

	sndPlaySound(NULL, SND_ASYNC);
	CFormView::OnInitialUpdate();
}

HBRUSH CCTSMonProView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) //yulee 20190531
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);
	
	// TODO:  여기서 DC의 특성을 변경합니다.

	switch(nCtlColor){
	case CTLCOLOR_DLG:
		{
			if(m_FlagBackgroundColor == 1)
			{
				return (HBRUSH)hbr;
			}
			else if(m_FlagBackgroundColor == 0)
			{
				return (HBRUSH)m_backBrush;
			}
			break;
		}
	}
	return hbr;
}

void CCTSMonProView::Fun_WindowFlickering()
{

	CCTSMonProDoc *pDoc = GetDocument();
	
	if(m_FlagBackgroundColor == 0)
	{
		m_FlagBackgroundColor = 1;
		CFormView::OnInitialUpdate();
	}
	else if(m_FlagBackgroundColor == 1)
	{
		m_FlagBackgroundColor = 0;
		CFormView::OnInitialUpdate();
	}
	
}

void CCTSMonProView::OnCanConfigRS485RX() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3","IDD_CTSMonPro_FORM")); //&&
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	CSettingCanDlg dlgCan(GetDocument());
	//CSettingCanDlg dlgCanFd(GetDocument());
	CSettingCanFdDlg dlgCanFd(GetDocument()); //lyj 20200728
	
	int nCanBoardType=0;
	nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
	if (nCanBoardType == 0)
	{
		dlgCan.nSelCh = nChIndex;
		dlgCan.nSelModule = nModuleID;
		dlgCan.nLINtoCANSelect = 2;
		dlgCan.DoModal();
	}
	else if (nCanBoardType == 1)

	{
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 2;
		dlgCanFd.m_strTitle = "RS485 RX Setting";
		dlgCanFd.DoModal();

	}
		
	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	m_wndCanList.DeleteAllItems();
	
	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	
	
	if (m_nCanMasterIDType == 1)
	{
		m_wndCanList.InsertColumn(3,"ID(DEC)",LVCFMT_CENTER,60);
	}
	else
	{
		m_wndCanList.InsertColumn(3,"ID(HEX)",LVCFMT_CENTER,60);
	}
	m_wndCanList.DeleteColumn(4);
}

void CCTSMonProView::OnCanConfigRS485TX() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3","IDD_CTSMonPro_FORM"));
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	CSettingCanDlg_Transmit dlgCan(GetDocument());
	CSettingCanFdDlg_Transmit dlgCanFd(GetDocument());
	
	int nCanBoardType=0;
	nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
// 	if (nCanBoardType == 0) //yulee 20181127 무조건 CAN FD
// 	{
// 		dlgCan.nSelCh = nChIndex;
// 		dlgCan.nSelModule = nModuleID;
// 		dlgCan.nLINtoCANSelect = 2;
// 		dlgCan.DoModal();
// 	}
// 	else
// 	{
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 2;
		dlgCanFd.m_strTitle = "RS485 TX Setting";
		dlgCanFd.DoModal();
//	}


	
	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	m_wndCanList.DeleteAllItems();
	
	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	
	
	if (m_nCanMasterIDType == 1)
	{
		m_wndCanList.InsertColumn(3,"ID(DEC)",LVCFMT_CENTER,60);
	}
	else
	{
		m_wndCanList.InsertColumn(3,"ID(HEX)",LVCFMT_CENTER,60);
	}
	m_wndCanList.DeleteColumn(4);
}

void CCTSMonProView::OnUpdateRS485Rx(CCmdUI* pCmdUI) 
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseRS485SettingDlg == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	
	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("먼저 선택 채널을 선택 하십시요.");
		//return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			//AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3"));
//#ifndef _DEBUG
			return;
//#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	SFT_CAN_COMMON_DATA commonData = pMD->GetCANCommonData(nChIndex, PS_CAN_TYPE_MASTER);
	if (commonData.can_lin_select == 2)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnUpdateRS485Tx(CCmdUI* pCmdUI) 
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseRS485SettingDlg == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end

	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("먼저 선택 채널을 선택 하십시요.");
		//return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			//AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3"));
//#ifndef _DEBUG
			return;
//#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nChIndex, PS_CAN_TYPE_MASTER);
	if (commonData.can_lin_select == 2)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnCanConfigSMBusRX() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3","IDD_CTSMonPro_FORM")); //&&
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	CSettingCanDlg dlgCan(GetDocument());
	//CSettingCanDlg dlgCanFd(GetDocument());
	CSettingCanFdDlg dlgCanFd(GetDocument()); //lyj 20200728
	
	//int nCanBoardType=0;
	//nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
	/*if (nCanBoardType == 0)
	{
		dlgCan.nSelCh = nChIndex;
		dlgCan.nSelModule = nModuleID;
		dlgCan.nLINtoCANSelect = 3;
		dlgCan.DoModal();
	}
	else if (nCanBoardType == 1)

	{*/
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 3;
		dlgCanFd.m_strTitle = "SMBus RX Setting";
		dlgCanFd.DoModal();

	//}
		
	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	m_wndCanList.DeleteAllItems();
	
	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	
	
	if (m_nCanMasterIDType == 1)
	{
		m_wndCanList.InsertColumn(3,"ID(DEC)",LVCFMT_CENTER,60);
	}
	else
	{
		m_wndCanList.InsertColumn(3,"ID(HEX)",LVCFMT_CENTER,60);
	}
	m_wndCanList.DeleteColumn(4);
}

void CCTSMonProView::OnCanConfigSMBusTX() 
{
	if(UpdateSelectedChList()  == FALSE)
	{
		AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return; 		
	}
	
	CCyclerModule * pMD;		
	
	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	
	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3","IDD_CTSMonPro_FORM"));
#ifndef _DEBUG
			//return;
#endif
		}
	}		
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;
	
	CSettingCanDlg_Transmit dlgCan(GetDocument());
	CSettingCanFdDlg_Transmit dlgCanFd(GetDocument());
	
	int nCanBoardType=0;
	nCanBoardType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type",0); //ljb 20181119 CAN Board에 따라 UI 변경
// 	if (nCanBoardType == 0) //yulee 20181127 무조건 CAN FD
// 	{
// 		dlgCan.nSelCh = nChIndex;
// 		dlgCan.nSelModule = nModuleID;
// 		dlgCan.nLINtoCANSelect = 3;
// 		dlgCan.DoModal();
// 	}
// 	else
// 	{
		dlgCanFd.nSelCh = nChIndex;
		dlgCanFd.nSelModule = nModuleID;
		dlgCanFd.nLINtoCANSelect = 3;
		dlgCanFd.m_strTitle = "SMBus TX Setting";
		dlgCanFd.DoModal();

/*	}*/


	
	GetDocument()->Fun_GetArryCanDivision(m_uiArryCanDivition);	//ljb 2011222 이재복 //////////
	GetDocument()->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	m_wndCanList.DeleteAllItems();
	
	m_nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);	
	
	if (m_nCanMasterIDType == 1)
	{
		m_wndCanList.InsertColumn(3,"ID(DEC)",LVCFMT_CENTER,60);
	}
	else
	{
		m_wndCanList.InsertColumn(3,"ID(HEX)",LVCFMT_CENTER,60);
	}
	m_wndCanList.DeleteColumn(4);
}

void CCTSMonProView::OnUpdateSMBusRx(CCmdUI* pCmdUI) 
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseSMBusSettingDlg == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end


	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("먼저 선택 채널을 선택 하십시요.");
		//return; 		
	}

	CCyclerModule * pMD;		

	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			//AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3"));
			//#ifndef _DEBUG
			return;
			//#endif
		}
	}		

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;

	SFT_CAN_COMMON_DATA commonData = pMD->GetCANCommonData(nChIndex, PS_CAN_TYPE_MASTER);
	if (commonData.can_lin_select == 3)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CCTSMonProView::OnUpdateSMBusTx(CCmdUI* pCmdUI) 
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseSMBusSettingDlg == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end


	//yulee 20181124
	if(UpdateSelectedChList()  == FALSE)
	{
		//AfxMessageBox("먼저 선택 채널을 선택 하십시요.");
		//return; 		
	}

	CCyclerModule * pMD;		

	int nPreModuleID= -1,nModuleID=0, nChIndex=0;
	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount > 1) 
	{
		//AfxMessageBox("1개의 채널만 선택 하세요. ");
		return;
	}
	else if (nSelCount <= 0)
	{
		return;
	}

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);		
		pMD = GetDocument()->GetModuleInfo(nModuleID);	
		if(pMD == NULL)	
		{
			return;
		}
		if (pMD->GetState() == PS_STATE_LINE_OFF)
		{
			//AfxMessageBox("접속 중인 모듈이 아닙니다. ");
			//AfxMessageBox(Fun_FindMsg("OnMenubarThtableSet_msg3"));
			//#ifndef _DEBUG
			return;
			//#endif
		}
	}		

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
	if(pChInfo == NULL) return;

	SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nChIndex, PS_CAN_TYPE_MASTER);
	if (commonData.can_lin_select == 3)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

//void CCTSMonProView::OnVentClear()
//ksj 20200122 : Close/Open Flag 추가.
// 0: close 1: open
void CCTSMonProView::OnVentClear(BYTE byCloseOpenFlag, BOOL bAsk) 
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;

	UINT nModuleID = 0;
	UINT nChIndex = 0;

	//m_awTargetChArray을 구함
	//if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_VENT_CLEAR)  == FALSE)
	{
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}

	int nSelCount = m_adwTargetChArray.GetSize();

	CPtrArray	aPtrSelCh;
	CString strTemp;
	CString strChList; //ksj 20200203	

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);

		if( pMD->GetState() == PS_STATE_LINE_OFF ) return;

		pChannel = pMD->GetChannelInfo(nChIndex);

		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);

		//ksj 20200203 : 선택한 채널 목록 문자열 생성
		strTemp.Format("Ch%d, ", nChIndex+1);
		strChList += strTemp;
		//ksj end
	}

	if(aPtrSelCh.GetSize() <= 0)
	{
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}

	
	CWordArray awChArray;
	nSelCount = aPtrSelCh.GetSize();
	int tCh;
	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

		if(pChannel == NULL)
		{
			//TRACE(Fun_FindMsg("OnCmdChamberContinue_msg3","IDD_CTSMonPro_FORM"));
			//@ TRACE("pChannel == NULL -> 이상\n");
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);
		if (pChannel->GetState() != PS_STATE_RUN)
		{
			awChArray.Add(nChIndex);
		}
	}
	if (awChArray.GetSize() > 0)
	{
		//ksj 20200203 : Vent Open/Close 대상 경고
		CString strMsg;
		CWorkWarnin WorkWarningDlg;
		//strMsg = "비상정지(Emgergency)를 진행하시겠습니까?";
		//strMsg = Fun_FindMsg("CTSMonProView_OnBtnEmg_msg1","IDD_CTSMonPro_FORM");//&&
		if(byCloseOpenFlag)
		{
			strMsg.Format("Vent Open 하시겠습니까? (%s)",strChList); //다국어화 필요
		}
		else
		{
			strMsg.Format("Vent Close 하시겠습니까? (%s)",strChList); //다국어화 필요
		}
		
		if(bAsk) //ksj 20201112 : 조건 추가.
		{
			WorkWarningDlg.Fun_SetMessage(strMsg);
			if (WorkWarningDlg.DoModal() == IDCANCEL) return;
		}		
		//ksj end

		//pDoc->SendCommand(nModuleID, &awChArray, SFT_CMD_VENT_CLEAR);
		pDoc->SendCommand(nModuleID, &awChArray, SFT_CMD_VENT_CLEAR, byCloseOpenFlag); //ksj 20200121 : cycle no를 vent clear인지 open인지 구분 플래그로 전달
	}
}


void CCTSMonProView::OnCmdIsolationEnable() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;
	UINT nModuleID = 0;
	UINT nChIndex = 0;
	CPtrArray	aPtrSelCh;
	CString strTemp;

	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount>1)
	{
		AfxMessageBox(Fun_FindMsg("ExeCTSRealGraphic_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("1채널만 선택 하세요.");
		return;
	}

	CString strMsg;
	strMsg.Format(Fun_FindMsg("CTSMonProView_Isolation1","IDD_CTSMonPro_FORM"));
	if(IDYES != MessageBox(strMsg, Fun_FindMsg("OnClearState_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
	{
		return;
	}
	else
	{

		for(int ch = 0; ch<nSelCount; ch++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(ch);
			nModuleID = HIWORD(dwData);
			nChIndex = LOWORD(dwData);
			pMD = pDoc->GetModuleInfo(nModuleID);

			if( pMD->GetState() == PS_STATE_LINE_OFF ) return;

			pChannel = pMD->GetChannelInfo(nChIndex);

			//선택된 Channel 정보를 Add한다.
			aPtrSelCh.Add(pChannel);
		}

		if(aPtrSelCh.GetSize() <= 0)
		{
			//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
			return;
		}

		CWordArray awChArray;
		nSelCount = aPtrSelCh.GetSize();
		int tCh;
		for(tCh = 0; tCh <nSelCount ; tCh++)
		{
			pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

			if(pChannel == NULL)
			{
				//TRACE(Fun_FindMsg("OnCmdChamberContinue_msg3","IDD_CTSMonPro_FORM"));
				//@ TRACE("pChannel == NULL -> 이상\n");
				continue;
			}
			nChIndex = pChannel->GetChannelIndex();
			nModuleID = pChannel->GetModuleID();
			pMD = pDoc->GetModuleInfo(nModuleID);
			if (pChannel->GetState() != PS_STATE_RUN)
			{
				awChArray.Add(nChIndex);
			}
		}
		if (awChArray.GetSize() > 0)
		{
			if (pDoc->SendCommand(nModuleID, &awChArray, SFT_CMD_DAQ_ISOLATION_COMM_REQUEST, 1))
			{
				CString strOut;
				strOut.Format("CH%d_Isolation_use", pChannel->GetChannelIndex());
				AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, strOut, 1);

				AfxMessageBox(Fun_FindMsg("OnCmdPause_msg4","IDD_CTSMonPro_FORM"));
			}
			else
			{
				AfxMessageBox(Fun_FindMsg("OnCmdPause_msg5","IDD_CTSMonPro_FORM"));
			}
		}
	}
}

void CCTSMonProView::OnCmdIsolationDisable() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;
	UINT nModuleID = 0;
	UINT nChIndex = 0;

	int nSelCount = m_adwTargetChArray.GetSize();
	if (nSelCount>1)
	{
		AfxMessageBox(Fun_FindMsg("ExeCTSRealGraphic_msg2","IDD_CTSMonPro_FORM"));
		//@ AfxMessageBox("1채널만 선택 하세요.");
		return;
	}

	CString strMsg;
	strMsg.Format(Fun_FindMsg("CTSMonProView_Isolation2","IDD_CTSMonPro_FORM"));
	if(IDYES != MessageBox(strMsg, Fun_FindMsg("OnClearState_msg3","IDD_CTSMonPro_FORM"), MB_YESNO|MB_ICONQUESTION)) 
	{
		return;
	}
	else
	{

		CPtrArray	aPtrSelCh;
		CString strTemp;

		for(int ch = 0; ch<nSelCount; ch++)
		{
			DWORD dwData = m_adwTargetChArray.GetAt(ch);
			nModuleID = HIWORD(dwData);
			nChIndex = LOWORD(dwData);
			pMD = pDoc->GetModuleInfo(nModuleID);

			if( pMD->GetState() == PS_STATE_LINE_OFF ) return;

			pChannel = pMD->GetChannelInfo(nChIndex);

			if(pChannel)
			{
				if((pChannel->GetState() == PS_STATE_RUN || pChannel->GetState() == PS_STATE_PAUSE))
				{
					AfxMessageBox(Fun_FindMsg("OnParallelCon_msg2","IDD_CTSMonPro_FORM"));
					//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					return;
				}
			}

			//선택된 Channel 정보를 Add한다.
			aPtrSelCh.Add(pChannel);
		}

		CWordArray awChArray;
		nSelCount = aPtrSelCh.GetSize();
		int tCh;
		for(tCh = 0; tCh <nSelCount ; tCh++)
		{
			pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

			if(pChannel == NULL)
			{
				//TRACE(Fun_FindMsg("OnCmdChamberContinue_msg3","IDD_CTSMonPro_FORM"));
				//@ TRACE("pChannel == NULL -> 이상\n");
				continue;
			}
			nChIndex = pChannel->GetChannelIndex();
			nModuleID = pChannel->GetModuleID();
			pMD = pDoc->GetModuleInfo(nModuleID);
			if (pChannel->GetState() != PS_STATE_RUN)
			{
				awChArray.Add(nChIndex);
			}
		}
		if (awChArray.GetSize() > 0)
		{
			SFT_DAQ_ISOLATION sDaqIsolation;
			ZeroMemory(&sDaqIsolation,sizeof(SFT_DAQ_ISOLATION));
			sDaqIsolation.bISO = 1;
			sDaqIsolation.div_ch = 0;
			sDaqIsolation.div_MainCh = 0;

			if (pDoc->SendCommand(nModuleID, &awChArray, SFT_CMD_DAQ_ISOLATION_COMM_REQUEST))
			{
				CString strOut;
				strOut.Format("CH%d_Isolation_use", pChannel->GetChannelIndex());
				AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, strOut, 0);

				AfxMessageBox(Fun_FindMsg("OnCmdPause_msg4","IDD_CTSMonPro_FORM"));
			}
			else
			{
				AfxMessageBox(Fun_FindMsg("OnCmdPause_msg5","IDD_CTSMonPro_FORM"));
			}
		}
	}
}

void CCTSMonProView::OnUpdateIsolationEnable(CCmdUI* pCmdUI) 
{
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
	}
	else	
	{
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDisplayIsolation", 0) == 0)
		{
			pCmdUI->Enable(FALSE);	
		}
		else
		{
			CCTSMonProDoc *pDoc = GetDocument();
			CCyclerChannel	*pChannel;
			CCyclerModule	*pMD;
			UINT nModuleID = 0;
			UINT nChIndex = 0;
			int nSelCount = m_adwTargetChArray.GetSize();

			CPtrArray	aPtrSelCh;
			CString strTemp;

			for(int ch = 0; ch<nSelCount; ch++)
			{
				DWORD dwData = m_adwTargetChArray.GetAt(ch);
				nModuleID = HIWORD(dwData);
				nChIndex = LOWORD(dwData);
				pMD = pDoc->GetModuleInfo(nModuleID);

				pChannel = pMD->GetChannelInfo(nChIndex);

				//선택된 Channel 정보를 Add한다.
				aPtrSelCh.Add(pChannel);
			}

			CString strOut;
			strOut.Format("CH%d_Isolation_use", pChannel->GetChannelIndex());
			int ChUseIsolation = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, strOut, 0);

			if (ChUseIsolation)
			{
				pCmdUI->Enable(FALSE);	
			}
			else
			{
				pCmdUI->Enable(TRUE);	
			}
		}
	}
}


void CCTSMonProView::OnUpdateIsolationDisable(CCmdUI* pCmdUI) 
{
	if(&m_wndModuleList == (CListCtrl *)GetFocus())
	{
	}
	else	
	{
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDisplayIsolation", 0) == 0)
		{
			pCmdUI->Enable(FALSE);	
		}
		else
		{
			CCTSMonProDoc *pDoc = GetDocument();
			CCyclerChannel	*pChannel;
			CCyclerModule	*pMD;
			UINT nModuleID = 0;
			UINT nChIndex = 0;
			int nSelCount = m_adwTargetChArray.GetSize();

			CPtrArray	aPtrSelCh;
			CString strTemp;

			for(int ch = 0; ch<nSelCount; ch++)
			{
				DWORD dwData = m_adwTargetChArray.GetAt(ch);
				nModuleID = HIWORD(dwData);
				nChIndex = LOWORD(dwData);
				pMD = pDoc->GetModuleInfo(nModuleID);

				pChannel = pMD->GetChannelInfo(nChIndex);

				//선택된 Channel 정보를 Add한다.
				aPtrSelCh.Add(pChannel);
			}

			CString strOut;
			strOut.Format("CH%d_Isolation_use", pChannel->GetChannelIndex());
			int ChUseIsolation = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, strOut, 0);
			if (ChUseIsolation)
			{
				pCmdUI->Enable(TRUE);	
			}
			else
			{
				pCmdUI->Enable(FALSE);	
			}
		}
	}
}

void CCTSMonProView::OnBnClickedButCbankOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;

	UINT nModuleID = 0;
	UINT nChIndex = 0;

	//m_awTargetChArray을 구함
	//if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	if(UpdateSelectedChList()  == FALSE)
	{
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}

	int nSelCount = m_adwTargetChArray.GetSize();

	CPtrArray	aPtrSelCh;
	CString strTemp;

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);

		if( pMD->GetState() == PS_STATE_LINE_OFF ) 
		{
			AfxMessageBox("장비와 연결되어 있지 않습니다.");
			return;
		}

		pChannel = pMD->GetChannelInfo(nChIndex);

		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);
	}

	if(aPtrSelCh.GetSize() <= 0)
	{
		AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}
	if(aPtrSelCh.GetSize() > 1)
	{
		AfxMessageBox("1Ch만 선택하십시요.");
		return;
	}

	CWordArray awChArray;
	nSelCount = aPtrSelCh.GetSize();
	int tCh;
	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

		if(pChannel == NULL)
		{
			//TRACE(Fun_FindMsg("OnCmdChamberContinue_msg3","IDD_CTSMonPro_FORM"));
			//@ TRACE("pChannel == NULL -> 이상\n");
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);
		if (pChannel->GetState() != PS_STATE_RUN)
		{
			awChArray.Add(nChIndex);
		}
		else
		{
			AfxMessageBox("동작중인 채널은 수정할 수 없습니다.");
			return;
		}
	}
	if (awChArray.GetSize() > 0)
	{
		CString strMsg;
		strMsg.Format("Ch%d 채널에 C_BANK On 명령어를 전송 하시겠습니까? \n(주의!! 배터리 연결을 반드시 해제 후 설정하세요)", nChIndex+1);
		if(IDYES == MessageBox(strMsg, "C_BANK ON", MB_YESNO|MB_ICONSTOP)) 
		{
			int nRtn = 0;
			SEND_CMD_CBANK_ON *pstCbankOn = new SEND_CMD_CBANK_ON;
			ZeroMemory(pstCbankOn, sizeof(SEND_CMD_CBANK_ON));
			pstCbankOn->flag = 1;	//on : 1, off : 0

			nRtn = pMD->SendCommand(SFT_CMD_CBANK, nChIndex, pstCbankOn, sizeof(SEND_CMD_CBANK_ON));
			TRACE("nCmdID : %d, pstCbankOn->flag%d\n", SFT_CMD_CBANK,  pstCbankOn->flag);
			delete pstCbankOn;
		}
	}
}

void CCTSMonProView::OnBnClickedButCbankOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerChannel	*pChannel;
	CCyclerModule	*pMD;

	UINT nModuleID = 0;
	UINT nChIndex = 0;

	//m_awTargetChArray을 구함
	//if(UpdateSelectedChList(SFT_CMD_PAUSE)  == FALSE)
	if(UpdateSelectedChList(SFT_CMD_CBANK)  == FALSE)
	{
		//@ AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}

	int nSelCount = m_adwTargetChArray.GetSize();

	CPtrArray	aPtrSelCh;
	CString strTemp;

	for(int ch = 0; ch<nSelCount; ch++)
	{
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);

		if( pMD->GetState() == PS_STATE_LINE_OFF ) 
		{
			AfxMessageBox("장비와 연결되어 있지 않습니다.");
			return;
		}

		pChannel = pMD->GetChannelInfo(nChIndex);

		//선택된 Channel 정보를 Add한다.
		aPtrSelCh.Add(pChannel);
	}

	if(aPtrSelCh.GetSize() <= 0)
	{
		AfxMessageBox("작업 시작 가능한 채널이 없습니다. 먼저 선택 채널을 선택 하십시요.");
		return;
	}
	if(aPtrSelCh.GetSize() > 1)
	{
		AfxMessageBox("1Ch만 선택하십시요.");
		return;
	}

	CWordArray awChArray;
	nSelCount = aPtrSelCh.GetSize();
	int tCh;
	for(tCh = 0; tCh <nSelCount ; tCh++)
	{
		pChannel = (CCyclerChannel *)aPtrSelCh.GetAt(tCh);

		if(pChannel == NULL)
		{
			//TRACE(Fun_FindMsg("OnCmdChamberContinue_msg3","IDD_CTSMonPro_FORM"));
			//@ TRACE("pChannel == NULL -> 이상\n");
			continue;
		}
		nChIndex = pChannel->GetChannelIndex();
		nModuleID = pChannel->GetModuleID();
		pMD = pDoc->GetModuleInfo(nModuleID);
		if (pChannel->GetState() != PS_STATE_RUN)
		{
			awChArray.Add(nChIndex);
		}
		else
		{
			AfxMessageBox("동작중인 채널은 수정할 수 없습니다.");
			return;
		}
	}
	if (awChArray.GetSize() > 0)
	{
		//pDoc->SendCommand(nModuleID, &awChArray, SFT_CMD_VENT_CLEAR);
		int nRtn = 0;
		SEND_CMD_CBANK_ON *pstCbankOn = new SEND_CMD_CBANK_ON;
		ZeroMemory(pstCbankOn, sizeof(SEND_CMD_CBANK_ON));
		pstCbankOn->flag = 0;	//on : 1, off : 0

		nRtn = pMD->SendCommand(SFT_CMD_CBANK, nChIndex, pstCbankOn, sizeof(SEND_CMD_CBANK_ON));
		TRACE("nCmdID : %d, pstCbankOn->flag%d\n", SFT_CMD_CBANK,  pstCbankOn->flag);
		delete pstCbankOn;
	}
}

// void CCTSMonProView::HideVentClose()
// {
// 	BOOL bUseVentBtn = AfxGetApp()->GetProfileInt("Config","USE VENT BUTTON",1);//yulee 20190909 Vent 없는 장비 처리
// 	if(bUseVentBtn == FALSE)
// 	{
// 		m_ctrlVentClear.ShowWindow(SW_HIDE);
// 	}
// 	else if(bUseVentBtn == TRUE)
// 	{
// 		m_ctrlVentClear.ShowWindow(SW_SHOW);
// 	}
// 
// }

void CCTSMonProView::HideOrShowButton()
{
	//int nUseAllBtn = AfxGetApp()->GetProfileInt("Config","USE ALL BUTTON",7);//yulee 20190909 Vent 없는 장비 처리
	int nUseAllBtn = AfxGetApp()->GetProfileInt("Config","USE ALL BUTTON",15);//ksj 20200122 : vent open 추가

	BOOL bUseButton;

	if(nUseAllBtn>0)
		bUseButton = TRUE;
	else if(nUseAllBtn == 0)
		bUseButton = FALSE;

	/*
	  1st Bit       2nd Bit       3rd Bit    4th bit
	Vent Close // Buzzer Stop // Emg Stop // Vent OPen

	2진수 3자리를 십진수로 바꿔서 
	모두 Show : 111 : 7
	Vent close와 Buzzer stop만 show : 110 : 3
	Vent close만 Show : 100 : 1

	*/

	int binary[MAIN_VIEW_BUTTON_COUNT] = {0,};

	if(bUseButton == true)
	{
		for(int i=0; i<MAIN_VIEW_BUTTON_COUNT; i++)
		{
			binary[i] = nUseAllBtn%2;
			nUseAllBtn = nUseAllBtn/2;
		}
	}

	BOOL bShow = 0;
	if(bUseButton == TRUE)
	{
		for(int y=0; y<MAIN_VIEW_BUTTON_COUNT; y++)
		{
			if(binary[y] == TRUE)
				bShow = SW_SHOW;
			if(y==0)
			{
				m_ctrlVentClear.ShowWindow(bShow);
			}
			else if(y==1)
			{
				m_ctrlButBuzzerStop.ShowWindow(bShow);
			}
			else if(y==2)
			{
				m_ctrlEMG.ShowWindow(bShow);
			}
			//ksj 20200122 : Vent Open 버튼 추가
			else if(y==3)
			{
				m_ctrlVentOpen.ShowWindow(bShow);
			}
			//ksj end
			bShow = 0;
		}
	}
	else if(bUseButton == FALSE)
	{
		m_ctrlVentClear.ShowWindow(bShow);
		m_ctrlButBuzzerStop.ShowWindow(bShow);
		m_ctrlEMG.ShowWindow(bShow);
		m_ctrlVentOpen.ShowWindow(bShow); //ksj 20200122
	}
}

void CCTSMonProView::OnCmdChill1TestSet()
{
	CCTSMonProDoc *pDoc = GetDocument();
	pDoc->m_chillerCtrl[0].m_ctrlOven.SetTemperature(0,0,25);
}


void CCTSMonProView::OnCmdChill2TestSet()
{
	CCTSMonProDoc *pDoc = GetDocument();
	pDoc->m_chillerCtrl[1].m_ctrlOven.SetTemperature(0,0,25);	
}

void CCTSMonProView::OnCmdPump1TestSet()
{
	CCTSMonProDoc *pDoc = GetDocument();
	int bPumpFlag;
		bPumpFlag = 1;
	pDoc->m_chillerCtrl[0].m_ctrlOven.SetTemperature(0,0,1,bPumpFlag);	
}


void CCTSMonProView::OnCmdPump2TestSet()
{
	CCTSMonProDoc *pDoc = GetDocument();
	int bPumpFlag;
		bPumpFlag = 1;
	pDoc->m_chillerCtrl[1].m_ctrlOven.SetTemperature(0,0,1,bPumpFlag);	
}


//ksj 20200116 : 화면에 Aux Fix 안전 조건 갱신
void CCTSMonProView::UpdateAuxSafetyCond(void)
{
	/*float fltCellVolt = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", 4600);
	float fltFixTempHigh = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", 80);

	CString strTemp;

	strTemp.Format("외부온도상한:%.0f'C",fltFixTempHigh);
	GetDlgItem(IDC_STATIC_AUX_SAFETY_TEMP)->SetWindowText(strTemp);
	
	strTemp.Format("외부전압상한:%.3fV",fltCellVolt/1000.0f);
	GetDlgItem(IDC_STATIC_AUX_SAFETY_VOLT)->SetWindowTextA(strTemp);*/
}

//ksj 20200116 : Aux Vent 안전 기능 설정 창 추가
void CCTSMonProView::OnAuxVentSafetyConfig()
{

	//CLoginManagerDlg LoginDlg;
	CLoginManagerDlg LoginDlg(PMS_SUPERVISOR); //ksj 20200203 : 권한 설정. 관리자 이상만 접근 가능
	if(LoginDlg.DoModal() != IDOK) return;

	CAuxVentSafetyDlg AuxVentSafetyDlg(GetDocument());
	AuxVentSafetyDlg.DoModal();	
}


//ksj 20200120 : Vent Clear 함수가 구현이 안되어있어 다른 v1015 코드에서 발췌 추가.
void CCTSMonProView::OnBnClickedBtnVentClear()
{
	BOOL bUseVentBtn = AfxGetApp()->GetProfileInt("Config","USE VENT BUTTON",1);//yulee 20190909 Vent 없는 장비 처리
	if(bUseVentBtn == TRUE)
	{
		//OnVentClear();
		OnVentClear(0); //ksj 2020012 : Vent Close
	}
	else
	{
		AfxMessageBox(_T("VENT 기능이 없는 장비입니다."));
	}
}

//ksj 20200122 : Vent Open 명령 추가.
void CCTSMonProView::OnBnClickedBtnVentOpen()
{
	BOOL bUseVentBtn = AfxGetApp()->GetProfileInt("Config","USE VENT BUTTON",1);//yulee 20190909 Vent 없는 장비 처리
	if(bUseVentBtn == TRUE)
	{
		OnVentClear(1); //Open
	}
	else
	{
		AfxMessageBox(_T("VENT 기능이 없는 장비입니다."));
	}
}

UINT ThreadTest(LPVOID pParam)
{
	CCTSMonProView* pView = (CCTSMonProView*) pParam;	
	CCTSMonProDoc* pDoc = (CCTSMonProDoc*) pView->GetDocument();
	int nRet, nRet2;
	float fRefTemp1 = 0.0f, fCurTemp1 = 0.0f;
	float fRefTemp2 = 0.0f, fCurTemp2 = 0.0f;
	int nCHNO = 0;
	CString strTemp;
	

	//모드 버스 조지기
	float fTemp = 24;
	int nTotalTestCount = 0;
	while(pView->m_bThreadTest)
	{
		if(fTemp > 30) fTemp = 24;
		fTemp += 0.1;
		//pDoc->m_chillerCtrl[0].m_ctrlOven.SetTemperature(0,0,fTemp);

		BOOL bflag = FALSE;
		int nCount=0;
		int nRetry = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "nChillerRetryNum", 10);


		strTemp.Format(_T("###########Summury ::::: TotalCnt: %d, fTemp: %f\n"),
			nTotalTestCount++,fTemp);

		GLog::log_All("OvenCtrl","DEBUG",strTemp);


		//while(nCount<10)
		while(nCount<nRetry)
		{
			nRet=pDoc->m_chillerCtrl[0].m_ctrlOven.SetTemperature(0,0,fTemp);
			Wait(1000);
			nRet2=pDoc->m_chillerCtrl[0].m_ctrlOven.RequestCurrentValue(0,0);

			fRefTemp2=pDoc->m_chillerCtrl[0].m_ctrlOven.GetRefTemperature(0);
			fCurTemp2=pDoc->m_chillerCtrl[0].m_ctrlOven.GetCurTemperature(0);
		
			int nTemp1 = (int)((fTemp+0.05)*10);
			int nTemp2 = (int)((fRefTemp2+0.05)*10);
			strTemp.Format(_T("###########count:%d chiller Ret:%d Ret2:%d MySetTemp:%f(%d) ChillerSetTemp:%f(%d)\n"),
				nCount+1,nRet,nRet2,
				fTemp,nTemp1,
				fRefTemp2,nTemp2);

			GLog::log_All("OvenCtrl","DEBUG",strTemp);
			TRACE(strTemp);

			nCount++;		

			if(nTemp1==nTemp2)
			{
				strTemp.Format("SetTemp retry cnt : %d",nCount);
				GLog::log_All("OvenCtrl","DEBUG",strTemp);
				bflag=TRUE;
				break;
			}	

			Wait(300);
		}

		if(!bflag)
		{
			strTemp.Format("!!!!!!SetTemp retry cnt : %d/%d",nCount,nRetry);
			GLog::log_All("OvenCtrl","DEBUG",strTemp);

			AfxMessageBox(strTemp);
		}


		Sleep(200);
	}

	AfxMessageBox("쓰레드 종료");

	return 0;
}

void CCTSMonProView::OnBnClickedButtonTestbtn()
{

// 	AfxMessageBox("테스트");


 	/*CInputBox theInputBox(this);
 	CString strUserInput("10");
 	theInputBox.Show("테스트를 위한 가상 알람창을 띄웁니다. 알람코드를 입력해주세요. (정수입력)", strUserInput);


	SFT_EMG_DATA *lpData = new SFT_EMG_DATA;
	//lpData->lCode = 10;
	lpData->lCode = atoi(strUserInput);
	lpData->lValue = 1;
	lpData->reserved = 2;
	
	this->PostMessage(SFTWM_MODULE_EMG, 1, (LPARAM)lpData);*/
	
	/*if(m_bThreadTest) m_bThreadTest = FALSE;
	else m_bThreadTest = TRUE;	

	if(m_bThreadTest)
		AfxBeginThread(ThreadTest,this);*/


	//((CMainFrame*)AfxGetMainWnd())->m_pExitProgressWnd->ShowWindow(SW_SHOW);


	/*CCTSMonProDoc *pDoc = GetDocument();
	pDoc->m_chillerCtrl[1].m_ctrlOven.ReConnect(0,0);*/

	//ksj 20190306 : 프로그램 강제로 죽이기
	CMainFrame* pTEST = NULL;

	pTEST->m_bAutoExit = FALSE;
}


//ksj 20200122 : 채널 Attribute 수신 처리 핸들러
LRESULT CCTSMonProView::OnChannelAttributeReceived(WPARAM wParam , LPARAM lParam)
{
	//병렬 설정창에 현재 CH Attribute를 SBC로 수신받아 최신상태로 갱신되었음을 통지한다.
	CCTSMonProDoc* pDoc = (CCTSMonProDoc*) GetDocument();
	
	if(pDoc) pDoc->WriteLog("Ch Attribute Received");

	if(m_pParallelConfigDlg)
	{
		if(m_pParallelConfigDlg->IsWindowVisible())
		{
			m_pParallelConfigDlg->SendMessage(SFTWM_CH_ATTRIBUTE_RECEIVE, wParam, lParam);
		}		
	}	
	return 1;
}

void CCTSMonProView::OnUpdateAuxVentSafetyConfig(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	if(m_bUseVentSafety == 0) //ksj 20200212
	{
		if (pCmdUI->m_pMenu!=NULL)
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
	}
}

// ksj 20200214 : 옵션에 따라 CTSMonProView내의 기능 활성 비활성화 
int CCTSMonProView::EnableFunctions(void)
{
	//ksj 20200214 : 통신 설정 노출 여부
	m_bUseVentSafety = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Vent Safety", 1); //ksj 20200214 : V1016 Vent 안전기능 노출 여부
	m_bUseCanSettingDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanSettingDlg", 1); //ksj 20200215 : Can 설정 노출 여부
	m_bUseLinCanSettingDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use LinCanSettingDlg", 1); //ksj 20200215 : LintoCan 설정 노출 여부
	m_bUseRS485SettingDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use RS485SettingDlg", 1); //ksj 20200215 : RS485 설정 노출 여부
	m_bUseSMBusSettingDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use SMBusSettingDlg", 1); //ksj 20200215 : SMBus 설정 노출 여부
	m_bUseLinSettingDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use LinSettingDlg", 1); //ksj 20200215 : Lin 설정 노출 여부
	m_bUseModbusSettingDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use ModbusSettingDlg", 1); //ksj 20200215 : Modbus 설정 노출 여부
	m_bUseUartSettingDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use UartSettingDlg", 1); //ksj 20200215 : UART 설정 노출 여부

	//m_bUseCanMux = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanMux", FALSE); //ksj 2021208 : Can Mux 기능 사용 여부 //ksj 20201223 : 취소됨

	
	
	//ksj 20200214 : AuxH 습도 기능 노출 여부
	m_bUseAuxH = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseAuxH", 0); //습도 기능 사용 여부
	GetDlgItem(IDC_STATIC_MIN_MAX_HUMIDITY)->ShowWindow(m_bUseAuxH);
	GetDlgItem(IDC_LAB_AUX_THER_HUMI_MAX)->ShowWindow(m_bUseAuxH);
	GetDlgItem(IDC_LAB_AUX_THER_HUMI_MIN)->ShowWindow(m_bUseAuxH);	
	////


	return 0;
}


void CCTSMonProView::OnUpdateLinConfig(CCmdUI *pCmdUI)
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseLinSettingDlg == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateModbusConfig(CCmdUI *pCmdUI)
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseModbusSettingDlg == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateUartConfig(CCmdUI *pCmdUI)
{
	//ksj 20200215 : 메뉴 노출 여부
	if(m_bUseUartSettingDlg == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


//ksj 20200702 : 화면 갱신 주기에 맞춰 채널코드를 검사한다
//채널코드별 예외처리 기능 추가.
//메인쓰레드이므로, 처리 오래걸리는 작업은 주의 요함.
BOOL CCTSMonProView::ChannelCodeCheck(void)
{
	BOOL bUseChCodeCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseChCodeCheck", 0); //ksj 20200702 : 채널코드 체크 기능 활성화 여부
	if(!bUseChCodeCheck) return 0;

	CCTSMonProDoc *pDoc = GetDocument();

	int nInstalledModule = pDoc->GetInstallModuleCount();

	for(int nModuleIdx=0;nModuleIdx<nInstalledModule;nModuleIdx++)
	{
		int nModuleID = pDoc->GetModuleID(nModuleIdx);
		CCyclerModule* pModule = pDoc->GetModuleInfo(nModuleID);

		if(pModule)
		{
			int nTotalChannel = pModule->GetTotalChannel();

			for(int nChannelIndex=0;nChannelIndex<nTotalChannel;nChannelIndex++)
			{
				CCyclerChannel* pChannel = pModule->GetChannelInfo(nChannelIndex);

				if(pChannel)
				{
					int nChannelCode = pChannel->GetCellCode();

					CheckNetworkError(nModuleID, nChannelIndex, nChannelCode); //ksj 20200702 : 네트워크 이상 발생시 처리 함수	

					//그 밖에 필요한 처리 있는 경우 아래 추가
				}

			}
		}

	}

	return 1;
}


//ksj 20200702 : 네트워크 에러 발생시 자동 작업 계속 명령 실행 기능 추가
//일시적인 네트워크 이상으로 통신 두절 일시정지 발생한 경우 자동으로 작업 계속하도록 한다
//재시작 시도 횟수. 해당 채널에 n회(10회) 이상 통신두절이 발생하면 더이상 재시도 하지 않도록 한다.
//작업 신규 시작시 재시도 횟수 클리어 해줘야함.  ResetNetworkErrorCount();
//통신 두절이 n분(10분?) 이상 지속되었던 경우 작업 계속 시키지 않는다. 너무 오랜시간 일시정지되어있던 경우 전압 회복등 시험에 변수 발생 가능.
BOOL CCTSMonProView::CheckNetworkError(int nModuleID, int nChannelIndex, int nChannelCode)
{
	BOOL bUseNetworkErrorContinue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseNetworkErrorContinue", 0); //ksj 20200702 : 네트워크 에러 발생시 자동 작업 계속 여부 옵션. (기본 비활성)
	if(!bUseNetworkErrorContinue) return 0;


	int nMaxNetworkErrorTimer = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "MaxNetworkErrorTimer", 10); //ksj 20200702 : 화면 갱신주기 10번 만큼 해당코드 유지하면 네트워크 이상으로 판단
	int nMaxNetworkErrorContinue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "MaxNetworkErrorContinue", 10); //ksj 20200702 : 작업 끝날때까지 10회 이상 네트워크 에러 발생하면 중단.
	int nNetworkErrorContinueSec = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NetworkErrorContinueSec", 600); //ksj 20200703 : 10분이상 통신두절이 지속되었던 경우에는 자동 작업계속 안시킴
	
	CCTSMonProDoc *pDoc = GetDocument();
	CString strTemp;

	CCyclerModule* pMD = pDoc->GetModuleInfo(nModuleID);
	if(pMD == NULL) return 0; //모듈 객체 유효성 체크
	CCyclerChannel* pCH = pMD->GetChannelInfo(nChannelIndex);
	if(pCH == NULL) return 0; //채널 객체 유효성 체크

// #ifdef _DEBUG
// 	nChannelCode = 109;
// 	nNetworkErrorContinueSec = 60;
// 	TRACE("C: %s, D: %s\n",pMD->m_timeConnected.Format("%m/%d %H:%M:%S"),pMD->m_timeDisconnected.Format("%m/%d %H:%M:%S"));
// #endif

	if(nChannelCode != 109 || pMD->GetState() == PS_STATE_LINE_OFF || pCH->GetState() != PS_STATE_PAUSE) //네트워크 이상이 맞는지 코드 체크. 설비 접속된 상태에서 네트워크 이상 코드 떠있는 경우.
	{
		pCH->m_nNetworkErrorTimer = 0; //네트워크 이상이 아니면 타이머 리셋하고 리턴.

// 		strTemp.Format("test %d %d %d",nChannelCode,pMD->GetState(),pCH->GetState());
// 		pCH->WriteLog(strTemp); //채널로그
		return 0;
	}	

	if(pCH->m_nNetworkErrorTimer++ > nMaxNetworkErrorTimer) //10 times (화면 10회갱신) 동안 네트워크 이상 유지하는 경우 네트워크 이상으로 판단.
	{
		//네트워크 이상 발생횟수가 일정이상 되면 자동 작업계속 안시킴.
		if(pCH->m_nNetworkErrorCnt >= nMaxNetworkErrorContinue)
		{
			//n회 이상 네트워크 이상 자동 작업 계속한 경우 문제가 심각하므로, 더 진행시키지 않는다.
			TRACE("Max Network Error(M%dC%d)\n",nModuleID,nChannelIndex);

// 			strTemp.Format("Max Network Error(M%dC%d)\n",nModuleID,nChannelIndex);
// 			pCH->WriteLog(strTemp); //채널로그
			return 0;
		}	
		
		//통신두절 지속시간이 너무 길면 자동 작업계속 안시킴.
		COleDateTimeSpan timeSpan;
		timeSpan = pMD->m_timeConnected - pMD->m_timeDisconnected;
		int nDisconnectSeconds = timeSpan.GetTotalSeconds();
		//10분 이상 통신 두절시 자동 작업 계속 안함.
		//프로그램 껏다 킨 경우에도 동작 안시킴.
		//GUI 켜져있는 상태에서 통신 끊어졌다 붙은 경우에만 자동 작업 계속
		if((nDisconnectSeconds > nNetworkErrorContinueSec) || (pMD->m_nConnectCount <= 1))
		{
			TRACE("DisconnectSeconds %d/%d (%s,%s)\n",
				nDisconnectSeconds,
				nNetworkErrorContinueSec,
				pMD->m_timeConnected.Format("%m/%d %H:%M:%S"),
				pMD->m_timeDisconnected.Format("%m/%d %H:%M:%S"));

// 			strTemp.Format("DisconnectSeconds %d/%d (%s,%s)\n",
// 				nDisconnectSeconds,
// 				nNetworkErrorContinueSec,
// 				pMD->m_timeConnected.Format("%m/%d %H:%M:%S"),
// 				pMD->m_timeDisconnected.Format("%m/%d %H:%M:%S"));
// 			pCH->WriteLog(strTemp); //채널로그

			return 0;
		}

		//자동작업 계속시키기
		pCH->m_nNetworkErrorCnt++; //네트워크 이상 횟수 1회 증가
		pCH->m_nNetworkErrorTimer = 0; //타이머 리셋				

		int nRtn = 0;
		WORD wState = pCH->GetState();
		WORD wCode = pCH->GetCellCode();

		if(wState == PS_STATE_PAUSE && wCode == 109) //채널코드가 109 네트워크에러 이고, 채널이 일시정지일때만 continue 전송.
		{
			nRtn = pMD->SendCommand(SFT_CMD_CONTINUE, nChannelIndex); //작업 계속 전송.
			//로그 기록
			strTemp.Format("M%dC%d Network Error Check (%d, %d/%d)",nModuleID,nChannelIndex+1,nRtn,pCH->m_nNetworkErrorCnt,nMaxNetworkErrorContinue);			
			TRACE(strTemp + "\n");
		}
		else
		{
			//로그 기록
			strTemp.Format("M%dC%d Network Error Check/state:%d code:%d (%d, %d/%d)",nModuleID,nChannelIndex+1,wState,wCode,nRtn,pCH->m_nNetworkErrorCnt,nMaxNetworkErrorContinue);
			TRACE(strTemp + "\n");
		}

		pCH->WriteLog(strTemp); //채널로그
		pDoc->WriteSysLog(strTemp); //시스템로그

		return nRtn;
	}

	return 0;

}

//ksj 20200901 : [#JSonMon] Json 모니터링 기능 초기화.
int CCTSMonProView::InitJsonMon(void)
{	
	BOOL bInitJsonMon = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "InitJsonMon", TRUE); //Json 모니터링 기능 초기화 여부. (우선 기본적으로 쓰레드는 생성해놓도록 TRUE를 기본 값으로 둔다)
	int  nJsonMonPeriod = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "JsonMon UpdateCycleSec", 10); //Json 상태 자동 갱신 주기 (초)
	CString strJsonMonPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "JsonMon Path", ""); //경로가 레지스트리에 할당되어있는 경우 해당 경로로 지정. 비어있으면 프로그램 설치 경로 아래의 state 폴더가 기본 폴더로 지정됨.

 	if(bInitJsonMon)
 	{
		if(m_pJsonMon == NULL)
		{
			m_pJsonMon = new CJsonMon((CCTSMonProDoc*)GetDocument(),this,(CMainFrame*)AfxGetMainWnd());
			if(!strJsonMonPath.IsEmpty()) //경로가 레지스트리에 할당되어있는 경우 해당 경로로 지정. 비어있으면 프로그램 설치 경로 아래의 state 폴더가 기본 폴더로 지정됨.
			{
				m_pJsonMon->SetJsonPath(strJsonMonPath);
			}
			m_pJsonMon->RunJsonMon(nJsonMonPeriod*1000);
		}
	}

	return 0;
}

void CCTSMonProView::OnFlickerConfig() //lyj 20200824
{
	//CLoginManagerDlg LoginDlg;
// 	CLoginManagerDlg LoginDlg(PMS_SUPERVISOR); //ksj 20200203 : 권한 설정. 관리자 이상만 접근 가능
// 	if(LoginDlg.DoModal() != IDOK) return;

	CFlickerConfigDlg FlickerConfigDlg(GetDocument());

	if(FlickerConfigDlg.DoModal() == IDOK)
	{
	}
	CCTSMonProDoc *pDoc = GetDocument();
	pDoc->LoadFlickerTable();

	 //m_FlagBackgroundColor = FlickerConfigDlg.GetChildData());
}


//ksj 20201013 : 변수 초기화. 재호출 가능한 함수로 분리.
void CCTSMonProView::InitPreFlickerInfo(void)
{
// 	m_FPreFickerInfo.nCode = 0; //lyj 20201007 Flicker 초기화
// 	m_FPreFickerInfo.nPriority = 3; //lyj 20201007 Flicker 초기화
// 	m_FPreFickerInfo.nAutoBuzzerStop = 0; //ksj 20201013 : 초기화 추가


	ZeroMemory(&m_stPreFickerInfo,sizeof(FLICKER_INFO)); //전부 0으로 초기화	
	m_stPreFickerInfo.nPriority = 999; //우선 순위 최하위 초기화
	m_stPreFickerInfo.dwColor = RGB(255,0,0); //색상도 빨강으로 초기화
}


//ksj 20201013 : 채널 깜빡임 색상 선택 쓰레드.
//기존 코드는 채널 수가 늘어나거나 채널 코드 수가 늘어나면 메인쓰레드에 딜레이를 줄 수 있다.
//쓰레드로 분리하여 메인쓰레드에 영향을 최소한으로 주도록 한다.
//공유 자원 관련하여 접근 요주의.
UINT CCTSMonProView::ThreadSetFlickerColor(LPVOID pParam)
{
	CCTSMonProView* pView = (CCTSMonProView*) pParam;
	CCTSMonProDoc* pDoc = pView->GetDocument();
	CString strTemp;

	Sleep(3000);

	while(pView->m_bThreadSetFlickerColor)
	{
		CCyclerModule* pHighestPriorityModule = NULL;
		CCyclerChannel* pHighestPriorityChannel= NULL;
		BOOL bIsPause = FALSE;
		BOOL bUpdatePriority = FALSE;

		for(int nModuleIdx=0; nModuleIdx<pDoc->GetInstallModuleCount() && pView->m_bThreadSetFlickerColor; nModuleIdx++)
		{
			int nModuleID = pDoc->GetModuleID(nModuleIdx);
			CCyclerModule* pMD = pDoc->GetModuleInfo(nModuleID);
			if(!pMD) continue;

			for(int nChannelIndex=0; nChannelIndex<pMD->GetTotalChannel() && pView->m_bThreadSetFlickerColor; nChannelIndex++)
			{
				CCyclerChannel* lpChannelInfo = pMD->GetChannelInfo(nChannelIndex);
				if(!lpChannelInfo) continue;

				if (lpChannelInfo->GetState() == PS_STATE_PAUSE) 
				{
					bIsPause = TRUE;
					//lyj 20201007 Flicker s ================
					int nChannelCode = lpChannelInfo->GetCellCode();					

					if(pView->m_stPreFickerInfo.nCode != nChannelCode)
					{ 
						/////////////////////////////////////////////////////////////////////////////////////////
						//m_FlickerInfoList 가 설정 변경등으로 인해 갱신되는 동안에는 실행되면 안됨. 크리티컬 섹션등 필요.
						pDoc->m_csFlickerInfoList.Lock(); //ksj 20201013

						POSITION pos = pDoc->m_FlickerInfoList.GetHeadPosition();
						FLICKER_INFO FlickerInfo;

						while(pos)
						{
							FlickerInfo = (FLICKER_INFO)pDoc->m_FlickerInfoList.GetNext(pos);

							if(nChannelCode == FlickerInfo.nCode)
							{
								if(pView->m_stPreFickerInfo.nPriority > FlickerInfo.nPriority) // 우선순위 검사
								{
									bUpdatePriority = TRUE; //우선순위 변경이 일어났다.

									pView->m_stPreFickerInfo = FlickerInfo; //최우선 순위 정보 저장			

									pHighestPriorityModule = pMD; //알람 최우선 순위 모듈 저장
									pHighestPriorityChannel = lpChannelInfo; //알람 최우선 순위 채널 저장
								}
								break;			

							}
						}
						//크리티컬 섹션 종료.
						pDoc->m_csFlickerInfoList.Unlock();  //ksj 20201013
						//////////////////////////////////////////////////////////////////////////////////////////
					}	
					//lyj 20201007 Flicker e ================
				}
			}
		}

		if(bIsPause)
		{			
			//브러시 객체 보호용 크리티컬 섹션 필요? 일단 보류
			//pView->m_csFlickerColorBrush.Lock(); //ksj 20201013
			
			if(pView->m_backBrush.GetSafeHandle())
			{
				pView->m_backBrush.DeleteObject();
			}
			pView->m_backBrush.CreateSolidBrush(pView->m_stPreFickerInfo.dwColor);

			//크리티컬 섹션 종료. 일단 보류
			//pView->m_csFlickerColorBrush.Unlock(); //ksj 20201013

			//가장 우선순위 알람이 변경되었고, 해당 알람에 부저 스탑 플래그가 켜있으면 부저 스탑 명령 전송.	
			if(bUpdatePriority == TRUE && pView->m_stPreFickerInfo.nAutoBuzzerStop == TRUE)
			{
				if(pHighestPriorityModule)
				{
					strTemp.Format("AutoBuzzerStop(ChCode:%d)",pView->m_stPreFickerInfo.nCode);
					pDoc->WriteLog(strTemp);
					pHighestPriorityModule->SendCommand(SFT_CMD_STOP_BUZZER,NULL,0); //SBC에 부저 스탑 명령 전송.
				}			
			}
			
		}		
		Sleep(2000);
	}

	if(pView->m_backBrush.GetSafeHandle())
	{
		pView->m_backBrush.DeleteObject();
	}	
	
	return 0;
}

//ksj 20201112
int CCTSMonProView::UpdateUserConfig(void)
{
	//ksj 20201112 : 모니터링시 표시하는 Can,Aux 소수점 아래 최대 자리수 변경 기능 추가. (충북TP 요청)
	m_nMaxAuxFloatingPoint = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "MaxAuxFloatingPoint", 3);
	m_nMaxCanFloatingPoint = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "MaxCanFloatingPoint", 6);

	return 0;
}

void CCTSMonProView::OnUpdateCmdCham1Set(CCmdUI *pCmdUI)
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	if(bUseOven  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateCmdCham2Set(CCmdUI *pCmdUI)
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", FALSE);
	if(bUseOven  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateCmdChill1TestSet(CCmdUI *pCmdUI)
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateCmdChill2TestSet(CCmdUI *pCmdUI)
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateCmdPump1TestSet(CCmdUI *pCmdUI)
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateCmdPump2TestSet(CCmdUI *pCmdUI)
{
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseChiller = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2 , "Use", 0);
	if(bUseChiller  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnUpdateCellbalSet(CCmdUI *pCmdUI)
{
	
	//ksj 20201116 : 메뉴 노출 여부
	BOOL bUseCellBAL = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC , "UseCellBAL",TRUE);
	if(bUseCellBAL  == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
	//ksj end
}


void CCTSMonProView::OnBnClickedBtnDaqIsolationStart2()
{
	CString strMsg;
	strMsg.Format("절연 상태로 전환 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}

	//Fun_IsolationStart(2,0);
	Fun_IsolationStartNormal(2); //ksj 20210105 : 일반 절연/비절연 함수 분리
}


void CCTSMonProView::OnBnClickedBtnDaqIsolationEnd2()
{
	CString strMsg;
	strMsg.Format("비절연 상태로 전환 하시겠습니까?", GetDocument()->GetCmdString(SFT_CMD_PAUSE));
	if(IDYES != MessageBox(strMsg, "전송 확인", MB_YESNO|MB_ICONQUESTION)) {
		return;
	}
	//Fun_IsolationEnd(2,0);
	Fun_IsolationEndNormal(2); //ksj 20210105 : 일반 절연/비절연 함수 분리

}

//ksj 20201211 : 커넥션 시퀀스 진행 상황창 띄우기.
void CCTSMonProView::OnConnSeqLogView()
{
	if(m_nCurrentModuleID-1 >= SFT_MAX_MODULE_NUM || m_nCurrentModuleID-1 < 0)
	{
		return;
	}
	if(m_pDlgConnSeq[m_nCurrentModuleID-1])
	{
		m_pDlgConnSeq[m_nCurrentModuleID-1]->ResetTransparent();
		m_pDlgConnSeq[m_nCurrentModuleID-1]->ShowWindow(SW_SHOW);
	}
}

//ksj 20201211 : 커넥션 시퀀스 진행 상황창 메뉴 표시 조건
void CCTSMonProView::OnUpdateConnSeqLogView(CCmdUI *pCmdUI)
{
	BOOL bUse = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Display ConnSeq", TRUE);
	if(bUse == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
}
void CCTSMonProView::EnableExeBtn(BOOL bIsEnable_)
{
	GetDlgItem(IDC_BUT_EDITOR)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_ANAL)->EnableWindow(FALSE);
	GetDlgItem(ID_MANAGE_PATTERN)->EnableWindow(FALSE);
	GetDlgItem(ID_EXE_DATA_VIEW)->EnableWindow(FALSE);
}

/*
void CCTSMonProView::OnAutoUpdaetReserve() 
{
	CCTSMonProDoc *pDoc = GetDocument();
	CString sMsg;
	CString strPath;

	// Get Current Directory Path
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(NULL,szBuff,_MAX_PATH);

	strPath = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'))+"\\GuiUpdater.exe";

	//if (PathFileExists("C:\\Program Files (x86)\\PNE CTSPack\\GuiUpdater.exe") == FALSE)
	if (PathFileExists(strPath) == FALSE)
	{
		//sMsg.Format("[AutoUpdaetReserve] GUI 설치 경로에 GuiUpdater.exr가 존재하지 않습니다");
		sMsg.Format("GUI 설치 경로(%s) \n 에 GuiUpdater.exe가 존재하지 않습니다",szBuff); //lyj 20210722
		AfxMessageBox(sMsg);

		return;
	}

	if (CheckActiveGuiProcess(strPath) == FALSE)
	{
		sMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약 기능 실행 실패");
		pDoc->WriteLog(sMsg);

		return;
	}

	CDlgUpdateReserve cDlg(pDoc);

	if (cDlg.DoModal() == 0)
	{
		sMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약 종료.");
		pDoc->WriteLog(sMsg);
		return; //lyj 20210722
	}
	else
	{
		//::SendMessage(this->m_hWnd, WM_CLOSE, NULL, NULL);
		((CMainFrame *)AfxGetMainWnd())->m_bAutoExit = true;
		AfxGetMainWnd()->PostMessage(WM_CLOSE);
		return; //lyj 20210722
	}

	//::SendMessage(this->m_hWnd, WM_CLOSE, NULL, NULL);
	//((CMainFrame *)AfxGetMainWnd())->m_bAutoExit = true;
	//AfxGetMainWnd()->PostMessage(WM_CLOSE);
}

void CCTSMonProView::OnUpdateAutoUpdaetReserve(CCmdUI* pCmdUI) 
{
	//BOOL bUse = (int)(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Develop Test", 0));
	BOOL bUse = (int)(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Auto Update", 0)); //lyj 20210722

	//pCmdUI->Enable(bUse);
	//if (!bUse)	pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
	if (!bUse) 
	{
		//pCmdUI->m_pMenu->ModifyMenuA(pCmdUI->m_nID, MF_BYCOMMAND, pCmdUI->m_nID, "Disable");
		pCmdUI->Enable(FALSE); //lyj 20210722
	}
}

BOOL CCTSMonProView::CheckActiveGuiProcess(CString strPath)
{
	//CString sGuiProcess[2] = {"CTSEditorPro.exe", "CTSAnalyzerPro.exe"};
	CArray<CString ,CString&> atParam;

	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 processEntry32;

	CString strPath_INI = strPath.Mid(0, strPath.ReverseFind('\\'))+"\\GuiUpdater.ini";
	CString strData;

	CStdioFile cfile;

	if(cfile.Open(strPath_INI,CFile::modeRead ))
	{
		while(cfile.ReadString(strData))
		{
			atParam.Add(strData);
		}
	}
	else
	{
		AfxMessageBox("GuiUpdater.ini CAN NOT OPEN !");
		return FALSE;
	}
	cfile.Close();
	//GetPrivateProfileString("Prog", "Name", NULL, szData, 512, strPath_INI );

	if(hProcessSnap == INVALID_HANDLE_VALUE)
	{
		exit(EXIT_FAILURE);
	}

	processEntry32.dwSize = sizeof(PROCESSENTRY32);

	if( !Process32First(hProcessSnap, &processEntry32) )
	{
		CloseHandle(hProcessSnap);
		exit(EXIT_FAILURE); 
	}

	int i = 0;
	CString strProcess;
	while(Process32Next(hProcessSnap, &processEntry32))
	{
		for(i = 0; i < atParam.GetSize(); i++)
		{
			strProcess = atParam.GetAt(i);
			if(strProcess.Compare(processEntry32.szExeFile) == 0)
			{
				strProcess = strProcess + " 가 동작중입니다. 프로그램을 종료해주세요!";
				AfxMessageBox(strProcess);
				return FALSE;
			}
		}
		
		if (sGuiProcess[0].Compare(processEntry32.szExeFile) == 0)
		{
			AfxMessageBox(_T("CTSEditorPro가 동작중입니다. 프로그램을 종료해주세요"));
			return FALSE;
		}
		else if (sGuiProcess[1].Compare(processEntry32.szExeFile) == 0)
		{
			AfxMessageBox(_T("CTSAnalyzerPro가 동작중입니다. 프로그램을 종료해주세요"));
			return FALSE;
		}
		
	}

	return TRUE;
}
// //lyj 20210721 LGES 이식 e ===================================================================

*/
void CCTSMonProView::Fun_AutoUpdate()
{
	CCTSMonProDoc *pDoc = GetDocument();
	CString strMsg;
	CString strTemp , strUse;
	BOOL	bStartUpdate;
	CUIntArray arrModuleReady;


	strTemp = pDoc->m_strCurPath + "\\Update";
	if (pDoc->file_Finder(strTemp) == FALSE)
	{
		if(pDoc->ForceDirectory(strTemp) == FALSE)
		{	
			log_All(_T("auto"),_T("update"),_T("Failed to create update folder"));
			//AfxMessageBox("Unable to Create  "+strTemp);
			return;
		};
	}

	strUse = pDoc->m_strCurPath + "\\Update\\use.txt"; //자동업데이트 사용유무

	if (PathFileExists(strUse) == FALSE)
	{
		return;
	}
	else
	{
		strTemp = pDoc->m_strCurPath + "\\GuiUpdater.exe";
		if (PathFileExists(strTemp) == FALSE)
		{
			//strMsg.Format("GUI 설치 경로(%s) \n 에 GuiUpdater.exe가 존재하지 않습니다",strTemp);
			log_All(_T("auto"),_T("update"),_T("Not on the path ") + strTemp);
			//AfxMessageBox(strMsg);
			return;
		}

		if(!Fun_ChkRunChannel()) //RUN 중인 채널이 없으면
		{
			if(m_bUpdateRunning) {return;}
			
			m_bUpdateRunning = TRUE;

			CDlgUpdateReserve cDlg(pDoc);

			if (cDlg.DoModal() == 0)
			{
				strMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약 종료.");
				pDoc->WriteLog(strMsg);
				m_bUpdateRunning = FALSE;
				return;
			}
			else
			{ 
				DeleteFile(strUse);
;				m_bUpdateRunning = FALSE;
				return;
			}

		}

// 		CDlgUpdateReserve cDlg(pDoc);
// 
// 		if (cDlg.DoModal() == 0)
// 		{
// 			strMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약 종료.");
// 			pDoc->WriteLog(strMsg);
// 			return; //lyj 20210722
// 		}
// 		else
// 		{
// 			//::SendMessage(this->m_hWnd, WM_CLOSE, NULL, NULL);
// // 			((CMainFrame *)AfxGetMainWnd())->m_bAutoExit = true;
// // 			AfxGetMainWnd()->PostMessage(WM_CLOSE);
// 			return; //lyj 20210722
// 		}
	}	
}

BOOL CCTSMonProView::Fun_ChkRunChannel()
{
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule *pMD = NULL;
	CCyclerChannel *pCH = NULL;
	int nChRunCnt = 0;

	for (int nMdIndex = 1; nMdIndex <= pDoc->GetInstallModuleCount(); nMdIndex++) //lyj 20210722
	{
		pMD = pDoc->GetModuleInfo(nMdIndex);

		nChRunCnt = 0;

		if (pMD)
		{
			for (int nChIndex = 0; nChIndex < pMD->GetTotalChannel(); nChIndex++)
			{
				pCH = pMD->GetChannelInfo(nChIndex); 

				if (pCH)
				{
					if (pCH->GetState() == PS_STATE_RUN)
					{
						nChRunCnt++;
					}
					if( pCH->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY ) //챔버연동대기 제외
					{
						nChRunCnt++;
					}
				}
			}
		}
		if (nChRunCnt > 0)
		{
			//arrModuleReady.Add(nMdIndex);
			return TRUE; //해당 모듈에 상태가 RUN인 채널이 있는 경우 return TRUE:
		}
	}

	return FALSE; //RUN인 채널이 없는 경우 return FALSE:
}

void CCTSMonProView::OnButIsolChoice()
{
	CString strMsg, strTemp;
	CString strBranch;
	int nBranch = 0;

	UpdateData();
	GetDlgItem(IDC_CBO_ISOL)->GetWindowText(strBranch);

	bool bUseIsolationCMD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCMD", 1); //lyj 20210913 0: SFT_CMD_DAQ_ISOLATION_COMM_REQUEST  1: SFT_CMD_OUTPUT_CH_CHANGE_REQUEST 

	if(strBranch == "Branch A")
	{
		nBranch = 1;
	}
	else if(strBranch == "Branch B")
	{
		nBranch = 2;
	}
	else if(strBranch == "Branch C")
	{
		nBranch = 3;
	}
	else if(strBranch == "Branch D")
	{
		nBranch = 4;
	}
	else if(strBranch == "Non Isolation")
	{
		nBranch = 1;
	}
	else if(strBranch == "All Isolation")
	{
		nBranch = 0; //절연
	}
	else if(strBranch == "비절연")
	{
		nBranch = 1;
	}
	else if(strBranch == "절연")
	{
		nBranch = 0; //절연
	}
	else
	{
		AfxMessageBox("Please Select Branch!");
		return;
	}

	if(UpdateSelectedChList(SFT_CMD_RUN)  == FALSE)
	{
		// AfxMessageBox("동작 중인 채널이 있으면 변경할 수 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("SetMuxChoice_msg3","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	if (m_nCurrentModuleID < 1)
	{
		//AfxMessageBox("작업할 모듈을 선택 하세요.");
		AfxMessageBox(Fun_FindMsg("SetMuxChoice_msg2","IDD_CTSMonPro_FORM")); //&&
		return;
	}
	CCTSMonProDoc *pDoc = GetDocument();
	CCyclerModule	*pMD;
	CCyclerChannel	*pChannel;
	UINT nModuleID = 0;
	UINT nOldModuleID = 0;
	UINT nChIndex = 0;
	int ch;
	int nSelCount = m_adwTargetChArray.GetSize();

	for(ch = 0; ch<nSelCount; ch++)
	{	
		DWORD dwData = m_adwTargetChArray.GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);
		pMD = pDoc->GetModuleInfo(nModuleID);
		pChannel = pMD->GetChannelInfo(nChIndex);
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

		if (pChannel->GetState() == PS_STATE_RUN)
		{
			strTemp.Format("Module : %d, Ch %d is Running , Cannot Change Isolation.",nModuleID,nChIndex+1);
			AfxMessageBox(strTemp);
			return;
		}
		TRACE("Module %d, channel %d\n",  pChannel->GetModuleID(), pChannel->GetChannelIndex());

		strMsg.Format("CH%d %s, Continue?", nChIndex+1,strBranch);
		if(IDYES != MessageBox(strMsg, "Confirm Windows", MB_YESNO|MB_ICONQUESTION)) {
			continue;;
		}

		if(bUseIsolationCMD) //SFT_CMD_OUTPUT_CH_CHANGE_REQUEST 분기절연
		{
			if(nBranch) //비절연
			{
				Fun_IsolationEnd(nBranch,nChIndex+1);
			}
			else //절연
			{
				Fun_IsolationStart(nBranch,nChIndex+1);
			}
		}
		else //SFT_CMD_DAQ_ISOLATION_COMM_REQUEST 기존절연 잘 사용하지 않는다
		{
			if(nBranch) //비절연
			{
				Fun_IsolationEndNormal(nChIndex+1);
			}
			else //절연
			{
				Fun_IsolationStartNormal(nChIndex+1);
			}
		}
	}

	// 211015 HKH 절연모드 충전 관련 기능 보완
	int nCurSel = m_ctrlCboISOL.GetCurSel();
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Select IsolChoice", nCurSel);
	
}
