#if !defined(AFX_SettingCanFdDlg_H__F3A1266F_DEAB_45DD_9B4D_0705D0393C0F__INCLUDED_)
#define AFX_SettingCanFdDlg_H__F3A1266F_DEAB_45DD_9B4D_0705D0393C0F__INCLUDED_

#include "MyGridWnd.h"	// Added by ClassView
#include "CTSMonProDoc.h"

#include "TabCtrlEx.h"
#include "afxwin.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SettingCanFdDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSettingCanFdDlg dialog
// typedef struct CanReceiveIndexDecimaltoHex{
// 	short usIndex;
// 	bool isChecked;
// } CAN_RECEIVE_INDEX_DE_TO_HEX;

#define DBC_COL_COUNT			22

#define DBC_COL_NO				0
#define DBC_COL_ID				1
#define DBC_COL_NAME			2
#define DBC_COL_STARTBIT		3
#define DBC_COL_BITCOUNT		4
#define DBC_COL_BYTE_ORDER		5
#define DBC_COL_DATATYPE		6
#define DBC_COL_FACTOR			7
#define DBC_COL_OFFSET			8
#define DBC_COL_FAULT_UPPER		9
#define DBC_COL_FAULT_LOWER		10
#define DBC_COL_END_UPPER		11
#define DBC_COL_END_LOWER		12
#define DBC_COL_DEFAULT			13
#define DBC_COL_SENT_TIME		14
#define DBC_COL_DIVISION1		15
#define DBC_COL_DIVISION2		16
#define DBC_COL_DIVISION3		17
#define DBC_COL_STARTBIT2		18
#define DBC_COL_BITCOUNT2		19
#define DBC_COL_BYTE_ORDER2		20
#define DBC_COL_DATATYPE2		21
#define DBC_COL_DEFAULT2		22

class CSettingCanFdDlg : public CDialog
{
// Construction
public:

	void Fun_ConvertData(CString strSignal, S_CAN_DBC_DATA *sCanData);
	int  nSelCh;
	int  nSelModule;
	int	 nLINtoCANSelect; //yulee 20181114 0:CAN 1:LIN
	int  nMasterIdType; //2014.08.12 탭 마스터/슬레이브 구분 값.
	BOOL IsCanValHex;
	BOOL IsChange;
	CString Fun_FindCanName(int nDivision);
	void Fun_ChangeSizeControl(UINT uiMaster, UINT uiSlave);
	void Fun_ChangeViewControl(BOOL bFlag);
	void InitParamTab();
	void ReDrawMasterGrid();
	void EnableAllControl(BOOL IsEnable);
	void InitLabel();
	void EnableMaskFilter(BOOL bFlag, int nType);
	void IsChangeFlag(BOOL IsFlag);
	long StringToHex32(CString strHex);	
	void UpdateCommonData();
	void UpdateGridData();
	void InitGridSlave();	
	BOOL LoadCANConfig(CString strLoadPath);
	BOOL LoadCANDbcConfig(CString strLoadPath);
	BOOL LoadCANDbcConfig2(CString strLoadPath);
	BOOL LoadCANDbcConfig2Slave(CString strLoadPath);
	BOOL SaveCANConfig(CString strSavePath);
	void InitGridMaster();
	void SetGridCanDivisionEnable(int nMsterIdType,int nRow,BOOL bFlag); //2014.08.12 그리드 캔 디비전 Enable 설정
	CSettingCanFdDlg(CCTSMonProDoc * pDoc, CWnd* pParent = NULL);   // standard constructor
	
	CString m_strTitle;
// Dialog Data
	//{{AFX_DATA(CSettingCanFdDlg)
	enum { IDD = IDD_CAN_FD_SETTING_DLG , IDD2 = IDD_CAN_FD_SETTING_DLG_ENG  , IDD3 = IDD_CAN_FD_SETTING_DLG_PL };
	CComboBox	m_ctrlBmsTerminalSlave;
	CComboBox	m_ctrlBmsCrcTypeSlave;
	CComboBox	m_ctrlBmsCrcType;
	CComboBox	m_ctrlBmsTerminal;
	CComboBox	m_ctrlDataRateSlave;
	CComboBox	m_ctrlDataRate;
	CButton	c_btn_decimal;
	CButton	c_btn_hex;
	CComboBox	m_ctrlBmsSJWSlave;
	CComboBox	m_ctrlBmsSJW;
	CString	m_strBmsSJW;
	CString	m_strBmsSJWSlave;
	CComboBox	m_ctrlBmsTypeSlave;
	CComboBox	m_ctrlBmsType;
	CString	m_strBmsType;
	CString	m_strBmsTypeSlave;
	/*CTabCtrl*/	CTabCtrlEx m_ctrlParamTab;
	//CTabCtrlEx m_ctrlFrameSetting;
	CLabel	m_ctrlLabelChMsg;
	CLabel	m_ctrlLabelChNo;
	CLabel	m_ctrlLabelMsg;
	CComboBox	m_ctrlBaudRateSlave;
	CComboBox	m_ctrlBaudRate;
	CString	m_strCanID;
	BOOL	m_ckExtID;
	BOOL	m_ckUserFilter;
	CString	m_strMask1;
	CString	m_strFilter1_1;
	CString	m_strFilter1_2;
	CString	m_strMask2;
	CString	m_strFilter2_1;
	CString	m_strFilter2_2;
	CString	m_strFilter2_3;
	CString	m_strFilter2_4;
	CString	m_strCanIDSlave;
	CString	m_strMask1Slave;
	CString	m_strMask2Slave;
	CString	m_strFilter1_1Slave;
	CString	m_strFilter1_2Slave;
	CString	m_strSelectCh;
	BOOL	m_ckExtIDSlave;
	BOOL	m_ckUserFilterSlave;
	CString	m_strFilter2_1Slave;
	CString	m_strFilter2_2Slave;
	CString	m_strFilter2_3Slave;
	CString	m_strFilter2_4Slave;
	int		m_nMasterIDType;
	CString m_strType;
	int		m_nProtocolVer;
	CString m_strDescript;
	BOOL	m_ckCanFD;
	BOOL	m_ckCanFD_Slave;
	//}}AFX_DATA

	int nBeforeMasterIdType;	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingCanFdDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSettingCanFdDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnLoadCanSetup();
	afx_msg void OnBtnSaveCanSetup();
	afx_msg void OnCheckUserFilter();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnCheckUserFilterSlave();
	afx_msg void OnSelchangeComboCanBaudrateSlave();
	afx_msg void OnSelchangeComboCanBaudrate();
	afx_msg void OnBtnAddMaster();
	afx_msg void OnBtnDelMaster();
	afx_msg void OnBtnAddSlave();
	afx_msg void OnBtnDelSlave();
	afx_msg void OnChangeEdit1();
	afx_msg void OnChangeEdit10();
	afx_msg void OnChangeEdit2();
	afx_msg void OnChangeEdit3();
	afx_msg void OnChangeEdit4();
	afx_msg void OnChangeEdit5();
	afx_msg void OnChangeEdit6();
	afx_msg void OnChangeEdit7();
	afx_msg void OnChangeEdit8();
	afx_msg void OnChangeEdit9();
	afx_msg void OnChangeSlaveMask1();
	afx_msg void OnChangeSlaveMask1Filter1();
	afx_msg void OnChangeSlaveMask1Filter2();
	afx_msg void OnChangeSlaveMask2();
	afx_msg void OnChangeSlaveMask2Filter1();
	afx_msg void OnChangeSlaveMask2Filter2();
	afx_msg void OnChangeSlaveMask2Filter3();
	afx_msg void OnChangeSlaveMask2Filter4();
	afx_msg void OnChangeEditSlaveCanid();
	afx_msg void OnChangeEditSlaveCv();
	afx_msg void OnCheckExtMode();
	afx_msg void OnCheckExtModeSlave();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	afx_msg void OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButNotUse();
	afx_msg void OnBtnLoadCanDbc();
	afx_msg void OnCheckCanFd();
	afx_msg void OnCheckCanFdSlave();
	afx_msg void OnSelchangeComboCanDatarate();
	afx_msg void OnSelchangeComboCanTerminalM();
	afx_msg void OnSelchangeComboCanCrcM();
	afx_msg void OnSelchangeComboCanSjwSlave();
	afx_msg void OnSelchangeComboCanBmsTypeSlave();
	afx_msg void OnSelchangeComboCanDatarateSlave();
	afx_msg void OnSelchangeComboCanTerminalS();
	afx_msg void OnSelchangeComboCanCrcS();
	afx_msg void OnSelchangeComboCanSjw();
	afx_msg void OnSelchangeComboCanBmsType();
	afx_msg void OnBtnLoadCanDbcSlave();
	//}}AFX_MSG
	afx_msg LRESULT OnGridBeginEdit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridPaste(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLBGridClick(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridLeftCell(WPARAM wParam, LPARAM lParam); //2014.08.12 이벤트 추가.
	
	DECLARE_MESSAGE_MAP()	
private:
	CUIntArray m_uiArryCanCode;
	CStringArray m_strArryCanName;

	CMyGridWnd m_wndCanGrid;
	CMyGridWnd m_wndCanGridSlave;
	CCTSMonProDoc * m_pDoc;
public:
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	CStatic m_ctrlFrameSetting; //lyj 20200317 CanFDDlg 최대화
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnCbnEditchangeComboCanSjw();
	afx_msg void OnCbnEditchangeComboCanBmsType();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SettingCanFdDlg_H__F3A1266F_DEAB_45DD_9B4D_0705D0393C0F__INCLUDED_)
