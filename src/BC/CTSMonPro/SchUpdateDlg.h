#if !defined(AFX_SCHUPDATEDLG_H__9CFFC284_D546_4C7D_A501_8AE5D6AF264D__INCLUDED_)
#define AFX_SCHUPDATEDLG_H__9CFFC284_D546_4C7D_A501_8AE5D6AF264D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SchUpdateDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSchUpdateDlg dialog
#define MIN_TIME_INTERVAL	100
//ksj 20170808 : 쓰레드에 넘겨줄 파라미터
typedef struct _thread_send_command_param
{
	CCyclerModule* pMD;
	int nCommand;
	//int nChIndex;
	CWordArray SelectCh;
	SFT_STEP_INFO_REQUEST* pStepRequest; 
	int nSize;
}ST_SEND_COMMAND_THREAD_PARAM;
//ksj end

class CSchUpdateDlg : public CDialog
{
// Construction
public:	
	void UpdateList();
	void InitList();
	void EnableUpdate();
	void DisableUpdate();
	void SetDefaultStepNo(UINT nStepNo);
	void InitDataUnit();
	//void Fun_SendSchUpdaeCmd();
	BOOL Fun_SendSchUpdaeCmd();
	void SetReceiveStepInfoData(SFT_STEP_CONDITION_V3 *stepInfoData);  //lyj 20200214 LG v1015 이상
	CSchUpdateDlg(int nModuleID, int nChIndex, CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor

	CCTSMonProDoc * m_pDoc;
	int m_nModuleID;
	int m_nChIndex;
	SFT_STEP_INFO_REQUEST sStepRequest;
	SFT_STEP_CONDITION_V3 m_sStepInfoData; //lyj 20200214 LG v1015 이상
	static UINT SendCommandThread(LPVOID lpParam); //ksj 20170808
	CImageList m_imgList;
	int m_nCustomMsec; //ksj 20171103 : spin 컨트롤 증감 값 옵션처리
// Dialog Data
	//{{AFX_DATA(CSchUpdateDlg)
	enum { IDD = IDD_SCH_UPDATE_DLG , IDD2 = IDD_SCH_UPDATE_DLG_ENG , IDD3 = IDD_SCH_UPDATE_DLG_PL };
	CListCtrl	m_ctrlList;
	CDateTimeCtrl	m_ctrlRecordTime;
	CDateTimeCtrl	m_ctrlEndTime;
	CComboBox	m_combo_step_mode;
	CComboBox	m_combo_step_type;
	UINT	m_edit_step_no;
	UINT	m_edit_step_mode;
	CString	m_edit_step_ref_I;
	CString	m_edit_step_P;
	UINT	m_edit_step_type;
	CString	m_edit_step_end_wh;
	CString	m_edit_step_end_volt_min;
	CString	m_edit_step_end_volt_max;
	UINT	m_edit_step_end_time;
	CString	m_edit_step_end_power;
	CString	m_edit_step_end_Ah;
	CString	m_edit_step_ref_d_volt;
	CString	m_edit_step_ref_c_volt;
	CString	m_edit_step_end_I;
	UINT	m_edit_step_end_time_day;
	CString	m_edit_safety_c_high;
	CString	m_edit_safety_c_low;
	CString	m_edit_safety_i_high;
	CString	m_edit_safety_i_low;
	CString	m_edit_safety_z_high;
	CString	m_edit_safety_z_low;
	CString	m_edit_safety_v_high;
	CString	m_edit_safety_v_low;
	CString	m_edit_record_delta_i;
	CString	m_edit_record_delta_v;
	int		m_edit_record_time_ms;
	UINT	m_edit_step_end_time_ms;
//	CString	m_edit_safety_delta_v_step;
	CString m_edit_cell_delta_v_step;	//lyj 20200214
	CString m_edit_step_auxt_delta;	//lyj 20200214
	CString m_edit_step_auxth_delta;	//lyj 20200214
	CString m_edit_step_auxt_auxth_delta;	//lyj 20200214
	CString m_edit_step_auxv_time;	//lyj 20200214
	CString m_edit_step_auxv_delta;	//lyj 20200214
	/*CButton m_chk_step_delta_aux_v;
	CButton m_chk_step_delta_aux_temp;
	CButton m_chk_step_delta_aux_th;
	CButton m_chk_step_delta_aux_t;
	CButton m_chk_step_aux_v;*/
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSchUpdateDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSchUpdateDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButStepSchRequest();
	afx_msg void OnSelchangeComboStepType();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDeltaposSpinEndTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinRecordTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeComboStepMode();
	afx_msg void OnChangeEditStepNo();
	afx_msg void OnEditchangeComboStepType();
	afx_msg void OnEditchangeComboStepMode();
	afx_msg void OnChangeEditStepCVolt();
	afx_msg void OnChangeEditStepDVolt();
	afx_msg void OnChangeEditStepRefI();
	afx_msg void OnChangeEditStepRefP();
	afx_msg void OnChangeEditStepEndVoltH();
	afx_msg void OnChangeEditStepEndVoltL();
	afx_msg void OnChangeEditStepEndI();
	afx_msg void OnChangeEditStepEndTimeDay();
	afx_msg void OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEditMs();
	afx_msg void OnChangeEditStepEndAh();
	afx_msg void OnChangeEditStepEndWh();
	afx_msg void OnChangeEditStepEndP();
	afx_msg void OnChangeEditSafetyVLow();
	afx_msg void OnChangeEditSafetyVHigh();
	afx_msg void OnChangeEditSafetyILow();
	afx_msg void OnChangeEditSafetyIHigh();
	afx_msg void OnChangeEditSafetyCLow();
	afx_msg void OnChangeEditSafetyCHigh();
	afx_msg void OnChangeEditSafetyImpLow();
	afx_msg void OnChangeEditSafetyImpHigh();
	afx_msg void OnDatetimechangeDatetimepickerRecordTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEditRecordTimeMs();
	afx_msg void OnChangeEditRecordDeltaV();
	afx_msg void OnChangeEditRecordDeltaA();	
	afx_msg void OnClickStepList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickUpdateStepList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEditStepDeltaVolt();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int m_nCurStepIndex;
	BOOL m_bUpdate;
	BOOL UpdateSchStep();
	void WriteSchUpdateLog(SFT_STEP_CONDITION_V3* pSrc, SFT_STEP_CONDITION_V3* pDst); //lyj 20200214 LG v1015 이상
	UINT m_nDefaultStepNo;
	double m_fVUnit;
	double m_fIUnit;
	double m_fCUnit;
	double m_fWUnit;
	double m_fWhUnit;

	int m_nVDec;
	int m_nIDec;
	int m_nCDec;
	int m_nWDec;
	int m_nWhDec;
	int m_nVCellDeltaDEC;//yulee 20190531_3

// 	bool bchk_step_delta_aux_temp;
// 	bool bchk_step_delta_aux_th;
// 	bool bchk_step_delta_aux_t;
// 	bool bchk_step_aux_v;

	CCyclerChannel *m_pCH; //ksj 20170810
public:
	afx_msg void OnEnChangeEditCellDeltaVStep();
	afx_msg void OnEnChangeEditStepAuxtDelta();
	afx_msg void OnEnChangeEditStepAuxthDelta();
	afx_msg void OnEnChangeEditStepAuxtAuxthDelta();
	afx_msg void OnEnChangeEditStepAuxvTime();
	afx_msg void OnEnChangeEditStepAuxvDelta();
	afx_msg void OnBnClickedChkStepDeltaAuxV();
	afx_msg void OnBnClickedChkStepDeltaAuxTemp();
	afx_msg void OnBnClickedChkStepDeltaAuxTh();
	afx_msg void OnBnClickedChkStepDeltaAuxT();
	afx_msg void OnBnClickedChkStepAuxV();

	BOOL m_bChk_step_delta_aux_v;
	BOOL m_bChk_step_delta_aux_temp;
	BOOL m_bChk_step_delta_aux_th;
	BOOL m_bChk_step_delta_aux_t;
	BOOL m_bChk_step_aux_v;
	afx_msg void OnBnClickedStaticVent();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCHUPDATEDLG_H__9CFFC284_D546_4C7D_A501_8AE5D6AF264D__INCLUDED_)
