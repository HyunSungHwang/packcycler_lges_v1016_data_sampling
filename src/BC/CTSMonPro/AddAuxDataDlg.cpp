// AddAuxDataDlg.cpp : implementation file
//

#include "stdafx.h" 
 


#include "ctsmonpro.h"
#include "AddAuxDataDlg.h"
#include "AuxDataDlg.h"
#include "LoginManagerDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddAuxDataDlg dialog


//CAddAuxDataDlg::CAddAuxDataDlg(CWnd * pParent, CCyclerModule * pMD /*=NULL*/)
//	: CDialog(CAddAuxDataDlg::IDD, pParent)
CAddAuxDataDlg::CAddAuxDataDlg(CWnd * pParent, CCyclerModule * pMD /*=NULL*/)
	: CDialog(
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAddAuxDataDlg::IDD):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAddAuxDataDlg::IDD2):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CAddAuxDataDlg::IDD3):
		(CAddAuxDataDlg::IDD),pParent
			)

			, m_bUseVentLink(FALSE)
{
	//{{AFX_DATA_INIT(CAddAuxDataDlg)
	m_strSensorName = _T("");
	m_strEndReq = _T("");
	m_fSensorMin = 0.0f;
	m_fSensorMAx = 0.0f;
	m_fEndMin = 0.0f;
	m_fEndMax = 0.0f;
	//}}AFX_DATA_INIT
	nSensorType = 0;
	this->pMD = pMD;
	nSensorCh = 0;
	this->bModify = FALSE;
}


void CAddAuxDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddAuxDataDlg)
	DDX_Control(pDX, IDC_COMBO_SENSOR_AUX_TYPE, m_ctrlSensorAuxThrType);
	DDX_Control(pDX, IDC_COMBO_SENSOR_FUNCTION3, m_ctrlSensorFUNC3);
	DDX_Control(pDX, IDC_COMBO_SENSOR_FUNCTION2, m_ctrlSensorFUNC2);
	DDX_Control(pDX, IDC_EDIT_SENSOR_NAME, m_ctrlSensName);
	DDX_Control(pDX, IDC_LIST_SELECT_SENSOR, m_ctrlSensorList);
	DDX_Control(pDX, IDC_COMBO_SENSOR_TYPE, m_ctrlSensorCB);
	DDX_Control(pDX, IDC_COMBO_SENSOR_FUNCTION, m_ctrlSensorFUNC1);
	DDX_Text(pDX, IDC_EDIT_SENSOR_NAME, m_strSensorName);
	DDX_Text(pDX, IDC_EDIT_MIN_VALUE2, m_strEndReq);
	DDX_Text(pDX, IDC_EDIT_MIN_VALUE, m_fSensorMin);
	DDX_Text(pDX, IDC_EDIT_MAX_VALUE, m_fSensorMAx);
	DDX_Text(pDX, IDC_EDIT_END_MIN_VALUE, m_fEndMin);
	DDX_Text(pDX, IDC_EDIT_END_MAX_VALUE, m_fEndMax);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_VENT_LINK, m_bUseVentLink);
}


BEGIN_MESSAGE_MAP(CAddAuxDataDlg, CDialog)
	//{{AFX_MSG_MAP(CAddAuxDataDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_SENSOR_TYPE, OnSelchangeComboSensorType)
	ON_CBN_EDITCHANGE(IDC_COMBO_SENSOR_FUNCTION, OnEditchangeComboSensorFunction)
	ON_EN_SETFOCUS(IDC_EDIT_MAX_VALUE, OnSetfocusEditMaxValue)
	ON_EN_SETFOCUS(IDC_EDIT_MIN_VALUE, OnSetfocusEditMinValue)
	ON_EN_KILLFOCUS(IDC_EDIT_MAX_VALUE, OnKillfocusEditMaxValue)
	ON_EN_KILLFOCUS(IDC_EDIT_MIN_VALUE, OnKillfocusEditMinValue)
	ON_BN_CLICKED(IDCANCEL_ADDAUXDATA, OnCancel)
	ON_EN_KILLFOCUS(IDC_EDIT_END_MAX_VALUE, OnKillfocusEditEndMaxValue)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddAuxDataDlg message handlers

BOOL CAddAuxDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	UpdateData();
	m_bLogin = FALSE;

	InitFunCombo();		//ljb 2011222 이재복 ////////////Combo Box 초기화
	InitSensorSelectCombo(); //ksj 20200214 : 센서 목록 combo box 초기화

	if(!bModify)
	{
		InitList();
		UpdateList();
		m_fTempLow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxTempLow", 0);			//20150811 ljb
		m_fTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxTempHigh", 80);			//20150811 ljb
		m_fVoltageLow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxVolLow", 1500);		//20150811 ljb
		m_fVoltageHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxVolHigh", 4300);		//20150811 ljb
		
		UpdateData(FALSE);
		
		if ( m_ctrlSensorList.GetItemCount() > 0)
		{
			CString strTemp = m_ctrlSensorList.GetItemText(0, 1);
			if (strTemp == Fun_FindMsg("OnlnitDialog_msg1","IDD_ADD_AUXDATA"))
				//@ if (strTemp == "온도센서")
			{
				m_fSensorMAx = m_fTempHigh;
				m_fSensorMin = m_fTempLow;
				
			}
			else if (strTemp == Fun_FindMsg("OnlnitDialog_msg2","IDD_ADD_AUXDATA"))
				//@ else if (strTemp == "전압측정")
			{
				if(m_fVoltageHigh != 0)  m_fSensorMAx = m_fVoltageHigh /1000;
				if(m_fVoltageLow != 0)  m_fSensorMin = m_fVoltageLow /1000;
			}
			//else if (strTemp == "써미스터")
			//else if (strTemp == "Thermistor") //언어파일작업필요
			else if (strTemp == Fun_FindMsg("OnlnitDialog_msg5","IDD_ADD_AUXDATA")) //&&
			{
				m_fSensorMAx = m_fVoltageHigh *1000000;
				m_fSensorMin = m_fVoltageLow *1000000;
			}
			else
			{
				m_fSensorMAx = 0;
				m_fSensorMin = 0;
			}
		}
		UpdateData(FALSE);

		return TRUE; 
	}
	else
	{
		GetDlgItem(IDC_COMBO_SENSOR_TYPE)->EnableWindow(FALSE);
		GetDlgItem(IDC_LIST_SELECT_SENSOR)->EnableWindow(FALSE);
		int nSel = Fun_GetFunctionAuxPos(nSensorFunction1);
		//if (nSel > 23) nSel-=6;
		m_ctrlSensorFUNC1.SetCurSel(nSel);

		nSel = Fun_GetFunctionAuxPos(nSensorFunction2);
		//if (nSel > 23) nSel-=6;
		m_ctrlSensorFUNC2.SetCurSel(nSel);

		nSel = Fun_GetFunctionAuxPos(nSensorFunction3);
		//if (nSel > 23) nSel-=6;
		m_ctrlSensorFUNC3.SetCurSel(nSel);
		//if (nSel > 23) nSel-=6;
		//m_ctrlSensorFUNC1.SetCurSel(nSel);
		//
		//nSel = Fun_GetFunctionAuxPos(nSensorFunction2);
		//if (nSel > 23) nSel-=6;
		//m_ctrlSensorFUNC2.SetCurSel(nSel);
		//
		//nSel = Fun_GetFunctionAuxPos(nSensorFunction3);
		//if (nSel > 23) nSel-=6;
		//m_ctrlSensorFUNC3.SetCurSel(nSel);
		
		m_ctrlSensorAuxThrType.SetCurSel(Fun_GetFunctionAuxThrPos(nSensorAuxthrType));
				
// 		m_editSensorFUNC1 = nSensorFunction;
// 		m_editSensorFUNC2 = nSensorFunction2;
// 		m_editSensorFUNC3 = nSensorFunction3;
		//m_ctrlSensorFUNC.SetCurSel(nSensorFunction);
		GetDlgItem(IDOK)->SetWindowText(Fun_FindMsg("OnlnitDialog_msg3","IDD_ADD_AUXDATA"));
		//@ GetDlgItem(IDOK)->SetWindowText("수정");
		this->SetWindowText(Fun_FindMsg("OnlnitDialog_msg4","IDD_ADD_AUXDATA"));
		//@ this->SetWindowText("외부 데이터 수정");
	}

	m_ctrlSensName.SetSel(0, -1);	
	
	m_bUseVentSafety = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Vent Safety", 1); //ksj 20200212

	//ksj 20200212 : v1016 vent 안전기능 사용 안하는 경우 체크 박스 숨김.
	if(m_bUseVentSafety == 0) 
	{
		m_bUseVentLink = FALSE;
		GetDlgItem(IDC_CHECK_VENT_LINK)->ShowWindow(SW_HIDE);		
	}

	UpdateData(FALSE);
	return TRUE; 

	///////////////////////////////////////////////////////////////////////////////////////////////////
}

void CAddAuxDataDlg::OnOK() 
{	
	UpdateData();
	if(m_ctrlSensorFUNC1.GetCurSel() != LB_ERR)	nSensorFunction1 = m_ctrlSensorFUNC1.GetItemData(m_ctrlSensorFUNC1.GetCurSel());
	if(m_ctrlSensorFUNC2.GetCurSel() != LB_ERR)	nSensorFunction2 = m_ctrlSensorFUNC2.GetItemData(m_ctrlSensorFUNC2.GetCurSel());
	if(m_ctrlSensorFUNC3.GetCurSel() != LB_ERR)	nSensorFunction3 = m_ctrlSensorFUNC3.GetItemData(m_ctrlSensorFUNC3.GetCurSel());
	if(m_ctrlSensorAuxThrType.GetCurSel() != LB_ERR)	nSensorAuxthrType = m_ctrlSensorAuxThrType.GetItemData(m_ctrlSensorAuxThrType.GetCurSel());

	if(!bModify)
	{
		nSensorType = m_ctrlSensorCB.GetCurSel();

		POSITION pos = m_ctrlSensorList.GetFirstSelectedItemPosition();

		if(pos)
		{
			int nIndex = m_ctrlSensorList.GetNextSelectedItem(pos);

			nSensorCh = atoi(m_ctrlSensorList.GetItemText(nIndex, 0));

			if(m_strSensorName == "")
				m_strSensorName.Format("%s %s",m_ctrlSensorList.GetItemText(nIndex, 1),  m_ctrlSensorList.GetItemText(nIndex, 0));
			UpdateData(FALSE);
			CDialog::OnOK();
		}
		else
		{
			AfxMessageBox("센서를 선택해야 합니다.");
			return;
		}
	}

	UpdateData(FALSE);
	CDialog::OnOK();
}

//리스트에 센서정보를 등록한다.
void CAddAuxDataDlg::UpdateList()
{
	UpdateData();
	CString strTemp;

	if(pMD == NULL)		return;
		
	for(int i = 0 ; i < pMD->GetMaxSensorType(); i++)
	{
		int nCount =0;
		switch(i)
		{
			case 0: nCount = pMD->GetMaxTemperature(); break;
			case 1: nCount = pMD->GetMaxVoltage(); break;
			case 2: nCount = pMD->GetMaxTemperatureTh(); break;
			case 3: nCount = pMD->GetMaxHumidity(); break; //ksj 20200116 : V1016 써미스터 습도
		}
		for(int j = 0 ; j < nCount; j++)
		{
			CChannelSensor * pSensor = pMD->GetAuxData(j, i);

			if(pSensor == NULL) continue;
			
			if(pSensor->GetInstall() == TRUE && pSensor->GetMasterChannel() == 0 && pSensor->GetAuxType() == m_ctrlSensorCB.GetCurSel())
			{				
				int nIndex = m_ctrlSensorList.GetItemCount();
				strTemp.Format("%d", pSensor->GetAuxChannelNumber());
				m_ctrlSensorList.InsertItem(nIndex,  strTemp);

				switch(pSensor->GetAuxType())
				{
				//case PS_AUX_TYPE_TEMPERATURE:	strTemp = "Temperature Sensors"; break;
				case PS_AUX_TYPE_TEMPERATURE:	strTemp = Fun_FindMsg("UpdateList_msg1","IDD_ADD_AUXDATA"); break;
					//@ case PS_AUX_TYPE_TEMPERATURE:	strTemp = "온도센서"; break;
				//case PS_AUX_TYPE_VOLTAGE:		strTemp = "voltage Meassure"; break;
				case PS_AUX_TYPE_VOLTAGE:		strTemp = Fun_FindMsg("UpdateList_msg2","IDD_ADD_AUXDATA"); break;
					//@ case PS_AUX_TYPE_VOLTAGE:		strTemp = "전압측정"; break;
				//case PS_AUX_TYPE_TEMPERATURE_TH:	strTemp = "Thermistor"; break;
				case PS_AUX_TYPE_TEMPERATURE_TH:	strTemp = Fun_FindMsg("AuxDataDlg_UpdateList_msg1","IDD_ADD_AUXDATA"); break;//&&

				case PS_AUX_TYPE_HUMIDITY:	strTemp = "습도센서"; break;//ksj 20200116 : 습도 추가. 다국어화 필요.
				default:break;
				}
				
				m_ctrlSensorList.SetItemText(nIndex,1,  strTemp);

			}
		}
		
	}	
}

void CAddAuxDataDlg::OnSelchangeComboSensorType() 
{
	m_ctrlSensorList.DeleteAllItems();
	UpdateList();	

	//UpdateData(FALSE);  //ksj 20200207 : 주석처리
	
	//CString strTemp = m_ctrlSensorList.GetItemText(0, 1);
	CString strTemp;
	m_ctrlSensorCB.GetLBText(m_ctrlSensorCB.GetCurSel(), strTemp); //ksj 20200207

	if (strTemp == Fun_FindMsg("OnSelchangeComboSensorType_msg1","IDD_ADD_AUXDATA"))
		//@ if (strTemp == "온도센서")
	{
		m_fSensorMAx = m_fTempHigh;
		m_fSensorMin = m_fTempLow;

		nSensorType = PS_AUX_TYPE_TEMPERATURE; //ksj 20200207 : InitRtTableCombo
	}
	else if (strTemp == Fun_FindMsg("OnSelchangeComboSensorType_msg2","IDD_ADD_AUXDATA"))
		//@ else if (strTemp == "전압측정")
	{
		if(m_fVoltageHigh != 0)  m_fSensorMAx = m_fVoltageHigh /1000;
		if(m_fVoltageLow != 0)  m_fSensorMin = m_fVoltageLow /1000;

		nSensorType = PS_AUX_TYPE_VOLTAGE; //ksj 20200207 : InitRtTableCombo
	}
	//else if (strTemp == "Thermistor") //언어파일작업필요.
	else if (strTemp == Fun_FindMsg("OnSelchangeComboSensorType_msg3","IDD_ADD_AUXDATA")) //언어파일작업필요.//&&
	{
		m_fSensorMAx = m_fVoltageHigh *1000;
		m_fSensorMin = m_fVoltageLow *1000;
		
		nSensorType = PS_AUX_TYPE_TEMPERATURE_TH; //ksj 20200207 : InitRtTableCombo
	}
	else if (strTemp == "습도센서") //ksj 20200207 : 언어파일 작업필요.
	{
		m_fSensorMAx = 0;
		m_fSensorMin = 0;

		nSensorType = PS_AUX_TYPE_HUMIDITY; //ksj 20200207 : InitRtTableCombo
	}
	else
	{
		m_fSensorMAx = 0;
		m_fSensorMin = 0;
	}
			
	InitRtTableCombo(); //ksj 20200207

	UpdateData(FALSE);
}
void CAddAuxDataDlg::OnEditchangeComboSensorFunction() 
{
	// TODO: Add your control notification handler code here
	UpdateList();		
}

void CAddAuxDataDlg::InitList()
{
	m_ctrlSensorCB.SetCurSel(0);
	
	//리스트 초기화////////////////////////////////////////////////////////////////////////////
	CRect rect;
	m_ctrlSensorList.GetClientRect(&rect);
	
	m_ctrlSensorList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES); 
	
	int n_dummy =  m_ctrlSensorList.InsertColumn(0, "Dummy", LVCFMT_CENTER, 100);	//Dummy Column
	
	m_ctrlSensorList.InsertColumn(1, _T("Channel"), LVCFMT_CENTER, 50);
	//m_ctrlSensorList.InsertColumn(1, _T(Fun_FindMsg("InitList_msg1")), LVCFMT_CENTER, 50);
	//@ m_ctrlSensorList.InsertColumn(1, _T("채널"), LVCFMT_CENTER, 50);
	m_ctrlSensorList.InsertColumn(2, _T("kind of Sensors"), LVCFMT_CENTER, rect.Width() - 50);
	//m_ctrlSensorList.InsertColumn(2, _T(Fun_FindMsg("InitList_msg2")), LVCFMT_CENTER, rect.Width() - 50);
	//@ m_ctrlSensorList.InsertColumn(2, _T("센서 종류"), LVCFMT_CENTER, rect.Width() - 50);
	m_ctrlSensorList.DeleteColumn(n_dummy);		//Remove Dummy Column
}


void CAddAuxDataDlg::InitFunCombo()
{
	CString strTemp;
	m_ctrlSensorFUNC1.ResetContent();
	m_ctrlSensorFUNC2.ResetContent();
	m_ctrlSensorFUNC3.ResetContent();
	m_ctrlSensorAuxThrType.ResetContent();

	int nIndex=1;
	m_ctrlSensorFUNC1.AddString("NONE");
	m_ctrlSensorFUNC1.SetItemData(0, 0);
	m_ctrlSensorFUNC2.AddString("NONE");
	m_ctrlSensorFUNC2.SetItemData(0, 0);
	m_ctrlSensorFUNC3.AddString("NONE");
	m_ctrlSensorFUNC3.SetItemData(0, 0);

	int i;
	for (i=0; i<m_uiArryAuxCode.GetSize(); i++)
	{//yulee 20181216
		
		//m_ctrlSensorFUNC1.AddString(m_strArryAuxName.GetAt(i));
		strTemp.Format("%s [%d]",m_strArryAuxName.GetAt(i), m_uiArryAuxCode.GetAt(i)); //ksj 20201005 : real code 값도 표시
		m_ctrlSensorFUNC1.AddString(strTemp); //ksj 20201005
		m_ctrlSensorFUNC1.SetItemData(i+1, m_uiArryAuxCode.GetAt(i));

		//m_ctrlSensorFUNC2.AddString(m_strArryAuxName.GetAt(i));
		strTemp.Format("%s [%d]",m_strArryAuxName.GetAt(i), m_uiArryAuxCode.GetAt(i)); //ksj 20201005 : real code 값도 표시
		m_ctrlSensorFUNC2.AddString(strTemp); //ksj 20201005
		m_ctrlSensorFUNC2.SetItemData(i+1, m_uiArryAuxCode.GetAt(i));

		//m_ctrlSensorFUNC3.AddString(m_strArryAuxName.GetAt(i));
		strTemp.Format("%s [%d]",m_strArryAuxName.GetAt(i), m_uiArryAuxCode.GetAt(i)); //ksj 20201005 : real code 값도 표시
		m_ctrlSensorFUNC3.AddString(strTemp); //ksj 20201005
		m_ctrlSensorFUNC3.SetItemData(i+1, m_uiArryAuxCode.GetAt(i));
		
	}


	//ksj 20200207 주석 처리
	/*m_ctrlSensorAuxThrType.AddString("NONE");
	m_ctrlSensorAuxThrType.SetItemData(0, 0);
	for (i=0; i<m_uiArryAuxThrCode.GetSize(); i++)
	{	
		m_ctrlSensorAuxThrType.AddString(m_strArryAuxThrName.GetAt(i));
		//CString strTmp;
		//strTmp.Format("%d",i+1);
		//m_ctrlSensorAuxThrType.AddString(strTmp);
		m_ctrlSensorAuxThrType.SetItemData(i+1, m_uiArryAuxThrCode.GetAt(i));
		//m_ctrlSensorAuxThrType.SetItemData(i+1, i+1);
	}*/

	InitRtTableCombo(); //ksj 20200207

	

	// 	int nCnt = 5;
	// 	for (int i=201; i<251 ; i++)
	// 	{
	// 		strTemp.Format("TYPE %d",i);
	// 		m_ctrlSensorFUNC1.AddString(strTemp);
	// 		m_ctrlSensorFUNC1.SetItemData(nCnt, i);
	// 
	// 		m_ctrlSensorFUNC2.AddString(strTemp);
	// 		m_ctrlSensorFUNC2.SetItemData(nCnt, i);
	// 
	// 		m_ctrlSensorFUNC3.AddString(strTemp);
	// 		m_ctrlSensorFUNC3.SetItemData(nCnt, i);
	// 		nCnt++;
	// 	}
	m_ctrlSensorFUNC1.SetCurSel(0);
	m_ctrlSensorFUNC2.SetCurSel(0);
	m_ctrlSensorFUNC3.SetCurSel(0);

	m_ctrlSensorAuxThrType.SetCurSel(0);
}

void CAddAuxDataDlg::Fun_SetArryAuxDivision(CUIntArray & uiArryAuxCode)
{
	m_uiArryAuxCode.Copy(uiArryAuxCode);
}

void CAddAuxDataDlg::Fun_SetArryAuxString(CStringArray & strArryAuxName)
{
	m_strArryAuxName.Copy(strArryAuxName);
}

void CAddAuxDataDlg::Fun_SetArryAuxThrDivision(CUIntArray & uiArryAuxThrCode)
{
	m_uiArryAuxThrCode.Copy(uiArryAuxThrCode);
}

void CAddAuxDataDlg::Fun_SetArryAuxThrString(CStringArray & strArryAuxThrName)
{
	m_strArryAuxThrName.Copy(strArryAuxThrName);
}

//ksj 20200207
void CAddAuxDataDlg::Fun_SetArryAuxHumiDivision(CUIntArray & uiArryAuxHumiCode)
{
	m_uiArryAuxHumiCode.Copy(uiArryAuxHumiCode);
}

//ksj 20200207
void CAddAuxDataDlg::Fun_SetArryAuxHumiString(CStringArray & strArryAuxHumiName)
{
	m_strArryAuxHumiName.Copy(strArryAuxHumiName);
}

int CAddAuxDataDlg::Fun_GetFunctionAuxPos(int nCode)
{
	for (int i=0; i<m_uiArryAuxCode.GetSize();i++)
	{
		if (nCode == m_uiArryAuxCode.GetAt(i))
		{
			return i+1;
		}
	}
	return 0;
}
int CAddAuxDataDlg::Fun_GetFunctionAuxThrPos(int nCode)
{
	for (int i=0; i<m_uiArryAuxThrCode.GetSize();i++)
	{
		if (nCode == m_uiArryAuxThrCode.GetAt(i))
		{
			return i+1;
		}
	}
	return 0;
}

void CAddAuxDataDlg::OnSetfocusEditMaxValue() 
{
	// TODO: Add your control notification handler code here
	m_fMaxValue = m_fSensorMAx;
}

void CAddAuxDataDlg::OnSetfocusEditMinValue() 
{
	// TODO: Add your control notification handler code here
	m_fMinValue = m_fSensorMin;
}

void CAddAuxDataDlg::OnKillfocusEditMaxValue() 
{
	
	// TODO: Add your control notification handler code here

	//int nUseAuxSetLogin = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use AuxSetLogin", 0);			//20170224 : 로그인 옵션 처리.
	int nUseAuxSetLogin = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use SafetyLogin", 0);			//ksj 20180305 : 로그인 옵션 처리.

	//yulee 20181220 
	 int nUseSafetyFixTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHigh", -50);

	//ksj 20180305 : 로그인창 띄우는 조건 변경
	if(nUseAuxSetLogin && nSensorType == PS_AUX_TYPE_TEMPERATURE) //온도 센서인 경우 처리 추가 (참고: nSensorType은 맴버 변수다;;;)
	{			
		UpdateData(); 
		float fTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxTempHigh", 80);	
		//if(m_fSensorMAx > fTempHigh) //ksj 20180305 : 기본 값 온도상한 값(80)을 초과 하는 경우에만 로그인창.
		if(m_fSensorMAx > fTempHigh && fTempHigh != 0.0f) //ksj 20200304
		{			
			if (m_bLogin == FALSE)
			{
				CLoginManagerDlg LoginDlg;
				if(LoginDlg.DoModal() != IDOK)
				{
					m_fSensorMAx = m_fMaxValue;
					UpdateData(FALSE);
					return;
				}
				m_bLogin = TRUE;
			}
		}
			
	}
	else if(nUseAuxSetLogin) //기존 코드 그대로
	{
		if (m_bLogin == FALSE)
		{
			CLoginManagerDlg LoginDlg;
			if(LoginDlg.DoModal() != IDOK)
			{
				m_fSensorMAx = m_fMaxValue;
				UpdateData(FALSE);
				return;
			}
			m_bLogin = TRUE;
		}	
	}
	
	//yulee 20181220
// 	if((nUseSafetyFixTempHigh > -50) && 
// 		((nSensorType == PS_AUX_TYPE_TEMPERATURE) || (nSensorType == PS_AUX_TYPE_TEMPERATURE_TH)))
	if((nUseSafetyFixTempHigh > -50) && 
		(nUseSafetyFixTempHigh != 0) &&  //ksj 20200304
		((nSensorType == PS_AUX_TYPE_TEMPERATURE) || (nSensorType == PS_AUX_TYPE_TEMPERATURE_TH)))
	{
		if(m_fSensorMAx > nUseSafetyFixTempHigh) //기본 안전 조건 보다 크다면 알림창과 함께 최고 값으로 입력.
		{
			CString tmpStr; 
			//$1013 tmpStr.Format(_T("입력 값은 %d을 넘을 수 없습니다."), nUseSafetyFixTempHigh);
			tmpStr.Format(Fun_FindMsg("AddAuxDataDlg_OnKillfocusEditMaxValue_msg1","IDD_ADD_AUXDATA"), nUseSafetyFixTempHigh); //&&
			AfxMessageBox(tmpStr);
			m_fSensorMAx = nUseSafetyFixTempHigh;
			UpdateData(FALSE);
			return;
		}
	}
	else
	{
		if(m_fSensorMAx > 80.0) //기본 안전 조건 보다 크다면 알림창과 함께 최고 값으로 입력.
		{
			CString tmpStr;
			//tmpStr.Format(_T("입력 값은 80을 넘을 수 없습니다."));
			tmpStr.Format(Fun_FindMsg("AddAuxDataDlg_OnKillfocusEditMaxValue_msg2","IDD_ADD_AUXDATA"));//&&
			AfxMessageBox(tmpStr);
			m_fSensorMAx = 80.0;
			UpdateData(FALSE);
			return;
		}
	}
}

void CAddAuxDataDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	if(nSensorType == 2)
	{
		GetDlgItem(IDC_STATIC_AUX_TEMP_TYPE)->ShowWindow(TRUE);
		GetDlgItem(IDC_COMBO_SENSOR_AUX_TYPE)->ShowWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_STATIC_AUX_TEMP_TYPE)->ShowWindow(FALSE);
		GetDlgItem(IDC_COMBO_SENSOR_AUX_TYPE)->ShowWindow(FALSE);
	}
}

void CAddAuxDataDlg::OnKillfocusEditMinValue() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	float fTemp = m_fSensorMin;


	//int nUseAuxSetLogin = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use AuxSetLogin", 0);			//20170224 : 로그인 옵션 처리.
	int nUseAuxSetLogin = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use SafetyLogin", 0);			//ksj 20180305 : 로그인 옵션 처리.
	
	if(nUseAuxSetLogin)
	{
		if (m_bLogin == FALSE)
		{
			CLoginManagerDlg LoginDlg;
			if(LoginDlg.DoModal() != IDOK)
			{
				m_fSensorMin = m_fMinValue;
				UpdateData(FALSE);
				return;
			}
			m_bLogin = TRUE;
		}	
	}
}

void CAddAuxDataDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CAddAuxDataDlg::OnKillfocusEditEndMaxValue() 
{
	//yulee 20181220 
	int nUseSafetyFixTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHigh", -50);

	//yulee 20181220
// 	if((nUseSafetyFixTempHigh > -50) && 
// 		((nSensorType == PS_AUX_TYPE_TEMPERATURE) || (nSensorType == PS_AUX_TYPE_TEMPERATURE_TH)))
	if((nUseSafetyFixTempHigh > -50) && 
		(nUseSafetyFixTempHigh != 0) && //ksj 20200304 : 조건 추가
		((nSensorType == PS_AUX_TYPE_TEMPERATURE) || (nSensorType == PS_AUX_TYPE_TEMPERATURE_TH)))
	{
		if(m_fEndMax > nUseSafetyFixTempHigh) //기본 안전 조건 보다 크다면 알림창과 함께 최고 값으로 입력.
		{
			CString tmpStr;
			//$1013 tmpStr.Format(_T("입력 값은 %d을 넘을 수 없습니다."), nUseSafetyFixTempHigh);
			tmpStr.Format(Fun_FindMsg("AddAuxDataDlg_OnKillfocusEditEndMaxValue_msg1","IDD_ADD_AUXDATA"), nUseSafetyFixTempHigh);//&&
			AfxMessageBox(tmpStr);
			m_fEndMax = nUseSafetyFixTempHigh;
			UpdateData(FALSE);
			return;
		}
	}	
	else
	{
		if(m_fEndMax > 80.0) //기본 안전 조건 보다 크다면 알림창과 함께 최고 값으로 입력.
		{
			CString tmpStr;
			//tmpStr.Format(_T("입력 값은 80을 넘을 수 없습니다."));
			tmpStr.Format(Fun_FindMsg("AddAuxDataDlg_OnKillfocusEditEndMaxValue_msg2","IDD_ADD_AUXDATA"));//&&
			AfxMessageBox(tmpStr);
			m_fEndMax = 80.0;
			UpdateData(FALSE);
			return;
		}
	}
}


//ksj 20200207
void CAddAuxDataDlg::InitRtTableCombo(void)
{
	int i=0;
	m_ctrlSensorAuxThrType.ResetContent();

	if(nSensorType == PS_AUX_TYPE_HUMIDITY)
	{		
		//ksj 20200207 : 습도 센서인경우 온도 테이블 리스트에 습도 리스트를 채워 넣는다.
		GetDlgItem(IDC_STATIC_AUX_TEMP_TYPE)->SetWindowText("Aux Humidity Type");		

		m_ctrlSensorAuxThrType.AddString("NONE");
		m_ctrlSensorAuxThrType.SetItemData(0, 0);
		for (i=0; i<m_uiArryAuxHumiCode.GetSize(); i++)
		{	
			m_ctrlSensorAuxThrType.AddString(m_strArryAuxHumiName.GetAt(i));
			m_ctrlSensorAuxThrType.SetItemData(i+1, m_uiArryAuxHumiCode.GetAt(i));
		}
	}
	else
	{
		GetDlgItem(IDC_STATIC_AUX_TEMP_TYPE)->SetWindowText("Aux Temperature Type"); //ksj 20200207

		m_ctrlSensorAuxThrType.AddString("NONE");
		m_ctrlSensorAuxThrType.SetItemData(0, 0);
		for (i=0; i<m_uiArryAuxThrCode.GetSize(); i++)
		{	
			m_ctrlSensorAuxThrType.AddString(m_strArryAuxThrName.GetAt(i));
			//CString strTmp;
			//strTmp.Format("%d",i+1);
			//m_ctrlSensorAuxThrType.AddString(strTmp);
			m_ctrlSensorAuxThrType.SetItemData(i+1, m_uiArryAuxThrCode.GetAt(i));
			//m_ctrlSensorAuxThrType.SetItemData(i+1, i+1);
		}
	}

	m_ctrlSensorAuxThrType.SetCurSel(0);
}

//ksj 20200215 : 센서 선택 드롭리스트 추가
int CAddAuxDataDlg::InitSensorSelectCombo(void)
{
	BOOL bUseAuxH = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseAuxH", 0); //습도 기능 사용 여부

	m_ctrlSensorCB.ResetContent();
	m_ctrlSensorCB.AddString(Fun_FindMsg("OnlnitDialog_msg1","IDD_ADD_AUXDATA")); //온도센서
	m_ctrlSensorCB.AddString(Fun_FindMsg("OnlnitDialog_msg2","IDD_ADD_AUXDATA")); //전압측정
	m_ctrlSensorCB.AddString(Fun_FindMsg("OnlnitDialog_msg5","IDD_ADD_AUXDATA")); //써미스터	
	if(bUseAuxH) m_ctrlSensorCB.AddString("습도센서"); //습도센서 //언어파일작업필요
	
	return 0;
}
