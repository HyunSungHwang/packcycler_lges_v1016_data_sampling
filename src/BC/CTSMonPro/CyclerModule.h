// CyclerModule.h: interface for the CCyclerModule class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CYCLERMODULE_H__59A815E8_4853_45FE_8249_DAC0154CF8DD__INCLUDED_)
#define AFX_CYCLERMODULE_H__59A815E8_4853_45FE_8249_DAC0154CF8DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CyclerChannel.h"
#include "atlcomtime.h"
//#include "CTSMonProDoc.h"	// Added by ClassView

class CCyclerModule  
{

public:
	LPARAM m_ThreadBuf;			//20110109
	SFT_CAN_COMMON_TRANS_DATA GetCANCommonTransData(int nChIndex, int nType);
	SFT_MD_CAN_TRANS_INFO_DATA * GetCanTransAllData();
	SFT_CAN_SET_TRANS_DATA GetCANSetTransData(int nChIndex, int nType, int nCanIndex);
	void SetCANCommonTransData(int nChIndex, SFT_CAN_COMMON_TRANS_DATA commonData, int nType);
	void SaveCanTransConfig();
	void SetCANSetTransData(int nChIndex, SFT_CAN_SET_TRANS_DATA *pCanSetData, int nCount);
	void MakeCANTrans();
	int GetInstalledCanTransSlave(int nChIndex);
	int GetInstalledCanTransMaster(int nChIndex);
	void SaveCanConfig();
	SFT_MD_CAN_INFO_DATA * GetCanAllData();
	void SetCANCommonData(int nChIndex, SFT_CAN_COMMON_DATA commonData, int nType);
	void SetCANSetData(int nChIndex, SFT_CAN_SET_DATA * pCanSetData, int nCount);
	int GetInstalledCanSlave(int nChIndex);
	int GetInstalledCanMaster(int nChIndex);
	void MakeCAN();
	SFT_CAN_SET_DATA GetCANSetData(int nChIndex, int nType, int nCanIndex);
	SFT_CAN_COMMON_DATA GetCANCommonData(int nChIndex, int nType = 0 /*0:Master, 1:Slave*/);
	void SaveAuxConfig();
	void RemoveAllAuxCh();
	void CencleAuxData();
	void ApplyAuxData();
	void SetAllParallelReset();
	void GetCanAuxList(CChannelSensor *pList);
	void GetVoltageAuxList(CChannelSensor *pList);
	void GetTemperatureAuxList(CChannelSensor * pList);
	void GetHumiAuxList(CChannelSensor *pList); //ksj 20200206 : v1016 습도
	void SetInstalledAux();
	void MakeAuxData();
	int GetMaxVoltage();
	int GetMaxTemperature();
	int GetMaxTemperatureTh();
	int GetMaxHumidity(); //ksj 20200116 : V1016 습도
	
	CChannelSensor * GetAuxData(int nSensorIndex, int nSensorType);

	void RemoveAuxData(STF_MD_AUX_SET_DATA delAux);

	int GetMaxSensorType();
	int GetTotalSensor();
	
	void SetParallelData(SFT_MD_PARALLEL_DATA paralData);
	SFT_MD_PARALLEL_DATA * GetParallelData();
	int GetParallelCount();
	void MakeParallel();

	UINT GetProtocolVer();
	int UpdateDataLossState(CWordArray *pSelCh = NULL, BOOL bOnLineMode = TRUE);
	//PC Down에 의해 PC Program 재시작 이전 시험정보를 Loading 했는지 여부 
	void	ResetUnSafetyShutdownCheck()		{	m_bCheckUnSafetyShutdown = FALSE;	};
	BOOL	IsCheckedUnSafetyShutdown()		{	return m_bCheckUnSafetyShutdown;	};
	BOOL CheckRunningChannel();
	CString GetConnectionElapsedTime();
	CString GetConnectedTime();
	CString GetIPAddress();
	int		GetCurrentRangeCount();
	int		GetVoltageRangeCount();
	float	GetVoltageSpec(int nRange = CAL_RANGE1);
	float	GetCurrentSpec(int nRange = CAL_RANGE1);
	void	SetModuleID(int nModuleIndex, int nModuleID, SFT_SYSTEM_PARAM	*pParam);
	CString GetCmdErrorMsg(int nCode);
	int		SendCommand(UINT nCmd, int nChIndex, LPVOID lpData = NULL, UINT nSize = 0);
	int		SendCommand(UINT nCmd, CWordArray *apChList = NULL, LPVOID lpData = NULL, UINT nSize = 0);
	int		SendCommand2(UINT nCmd, CWordArray *apChList = NULL, LPVOID lpData = NULL, UINT nSize = 0, UINT nWaitTime = 0);
	int		SendCommand(UINT nCmd, LPVOID lpData = NULL, UINT nSize = 0);
	WORD	GetState();
	BOOL	MakeChannel(UINT nTotalCh);
	CCyclerChannel * GetChannelInfo(int nChannelIndex);
	
	CCyclerModule();
	virtual ~CCyclerModule();

	UINT	GetTotalChannel()	{	if(m_lpChannelInfo == NULL)	return 0;	return m_nTotalChannel;	}
	int		GetModuleID()		{		return m_nModuleID;								}

	void SetSendScheduleState(BOOL bState){	SFTGetModule(m_nModuleID)->SetSendScheduleState(bState); } //ksj 20201215
	BOOL GetSendScheduleState(){ return SFTGetModule(m_nModuleID)->GetSendScheduleState(); } //ksj 20201215

protected:
	BOOL m_bCheckUnSafetyShutdown;
	UINT	m_nModuleID;
//	CString m_strDataBaseName;
	CString	m_strConfigDir;

	void RemoveChannel();
	UINT m_nTotalChannel;
	CCyclerChannel *m_lpChannelInfo;

	SFT_MD_PARALLEL_DATA  m_parallelData[_SFT_MAX_PARALLEL_COUNT];	//병렬 모드 저장용
	SFT_MD_CAN_INFO_DATA  m_canData;
	SFT_MD_CAN_TRANS_INFO_DATA  m_canTransData;
	int nInstalledMasterCANCount[_SFT_MAX_INSTALL_CH_COUNT];
	int nInstalledSlaveCANCount[_SFT_MAX_INSTALL_CH_COUNT];
	int nInstalledMasterCANTransCount[_SFT_MAX_INSTALL_CH_COUNT];
	int nInstalledSlaveCANTransCount[_SFT_MAX_INSTALL_CH_COUNT];
	int nParallelCount;						//병렬 모드 갯수

	//최종 저장용
	CChannelSensor * pAuxTemp;
	CChannelSensor * pAuxVolt;
	CChannelSensor * pAuxTempTh;
	CChannelSensor * pAuxHumi; //ksj 20200116

	//임시 저장용
	CChannelSensor * pAuxTemp2;
	CChannelSensor * pAuxVolt2;
	CChannelSensor * pAuxTempTh2;
	CChannelSensor * pAuxHumi2; //ksj 20200116
	int nTotalSensor;

public:
	// ksj 20200703 : 디스커넥트된 타임 체크
	COleDateTime m_timeDisconnected;
	COleDateTime m_timeConnected;
	// ksj 20200703 : 접속 횟수 카운팅
	int m_nConnectCount;
};

#endif // !defined(AFX_CYCLERMODULE_H__59A815E8_4853_45FE_8249_DAC0154CF8DD__INCLUDED_)
