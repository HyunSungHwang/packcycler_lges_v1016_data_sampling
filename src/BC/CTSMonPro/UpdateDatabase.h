/*
데이터 베이스 자동 업데이트 코드를 다시 재정리 하기 위한 클래스 생성
CTSMonProDoc에 구현했던 코드 정리.
2020-10-13 Kang Se-jun
*/


#pragma once

class CCTSMonProDoc;
class CUpdateDatabase
{
private:
	CCTSMonProDoc* m_pDoc;
	int UpdateDefaultUser(void);
	int UpdateChannelcode_v1016(void);
	int UpdateMDBAuxH(void);
	int UpdateMDBSequence(void);
	int UpdateMDBFlicker(void);
	int GetMDBUserIDCount(CDaoDatabase& db, CString strUserID);
	int GetMDBChannelCodeCount(CDaoDatabase& db, CString strTable, int nChannelCode);
	int ExecuteQueryDB(CStringArray& saSQL, CString strDatabaseName, BOOL bFailContinue = TRUE);
	int UpdateMDBChiller();
	int UpdateMDBPowerSupply();

public:
	CUpdateDatabase(CCTSMonProDoc* pDoc);
	~CUpdateDatabase(void);

	int UpdateDatabase(void);
	
};

