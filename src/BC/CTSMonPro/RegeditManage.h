#include "afxwin.h"
#if !defined(AFX_REGEDITMANAGE_H__FA7E57CD_C35D_4322_AF06_815DEEECBE50__INCLUDED_)
#define AFX_REGEDITMANAGE_H__FA7E57CD_C35D_4322_AF06_815DEEECBE50__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegeditManage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRegeditManage dialog

class CRegeditManage : public CDialog
{
// Construction
public:
	CRegeditManage(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRegeditManage)
	enum { IDD = IDD_REGEDIT_MANAGE , IDD2 = IDD_REGEDIT_MANAGE_ENG , IDD3 = IDD_REGEDIT_MANAGE_PL };
	BOOL	m_check1;
	BOOL	m_check2;
	BOOL	m_check3;
	BOOL	m_check4;
	BOOL	m_check5;
	BOOL	m_check6;
	BOOL	m_check7;
	BOOL	m_check8;
	BOOL	m_check9;
	UINT	m_edit1;
	UINT	m_edit2;
	UINT	m_edit3;
	UINT	m_edit4;
	int		m_edit5;
	BOOL	m_check10;
	BOOL	m_check11;
	BOOL	m_check12;
	BOOL	m_check13;
	BOOL	m_check14;
	BOOL	m_check15; //lyj 20210913 분기
	BOOL	m_check16; //lyj 20210913 기존 명렁어 사용여부
	BOOL	m_check17; // 211015 HKH 절연모드 충전 관련 기능 보완
	BOOL	m_bCheckVoltage;
	BOOL	m_bUseAuxSetLogin;
	BOOL	m_bUseBackupSbcResult;
	BOOL	m_bUseBackupSbcStepEnd;
	BOOL	m_bCheckAuxSafeCond;
	BOOL	m_bUseOnlyMasterCheck;
	BOOL	m_bCheckShowSCHAllStepUpdateInPopup; //yulee 20181216
	BOOL	m_CheckUseTempSensor;
	BOOL	m_CheckUseAllButton;
	BOOL	m_CheckUseVebtCloseButton;
	BOOL	m_bUsePowerCableCheck;		// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegeditManage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRegeditManage)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckUseTempsensor2();
	// Menu Enable or Disable
	BOOL m_CheckUseExtDevMenu;

	afx_msg void OnBnClickedCheck17();	// 211015 HKH 절연모드 충전 관련 기능 보완
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGEDITMANAGE_H__FA7E57CD_C35D_4322_AF06_815DEEECBE50__INCLUDED_)
