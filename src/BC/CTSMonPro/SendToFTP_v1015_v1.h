#ifndef __SendToFTP_v1015_v1__
#define __SendToFTP_v1015_v1__

#pragma once


#include "CTSMonProDoc.h"

class CSendToFTP_v1015_v1
{
public:
	CSendToFTP_v1015_v1(void);
	~CSendToFTP_v1015_v1(void);
	

	BOOL SendSchFTP_v1015_v1(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, int nOption,int ReserveID, CCTSMonProDoc* pDoc);
	BOOL SendSchFTP2_v1015_v1(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, CCTSMonProDoc* pDoc);
	
	//ksj 20201005 : 작업 시작전 스케쥴 Aux 조건 검증 기능 추가
	BOOL PreAuxConditionCheck(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, CCTSMonProDoc* pDoc); 
	//ksj 20201016 : 작업 시작전 스케쥴 Can 조건 검증 기능 추가
	BOOL PreCanConditionCheck(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, CCTSMonProDoc* pDoc); 
};

#endif