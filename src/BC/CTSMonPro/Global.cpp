// Global.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "Global.h"

#include "MainFrm.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Global

Global::Global()
{
}

Global::~Global()
{
}


BEGIN_MESSAGE_MAP(Global, CWnd)
	//{{AFX_MSG_MAP(Global)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Global message handlers


void Global::app_GetConfig(CString AppPath)
{//basic info	
	g_AppInfo.AppPath=AppPath;
	
	g_AppInfo.strDbip=GIni::ini_GetFile(g_AppIni,_T("DB"),_T("DBIP"),_T(""));
	g_AppInfo.strDbport=GIni::ini_GetFile(g_AppIni,_T("DB"),_T("DBPORT"),_T(""));
	g_AppInfo.strDbname=GIni::ini_GetFile(g_AppIni,_T("DB"),_T("DBNAME"),_T("pnesk"));
	
	g_AppInfo.strDbid=GIni::ini_GetFile(g_AppIni,_T("DB"),_T("DBID"),_T(""));
	g_AppInfo.strDbpwd=GIni::ini_GetFile(g_AppIni,_T("DB"),_T("DBPWD"),_T(""));
	g_AppInfo.strDbpwd=GStr::str_Decrypt2(g_AppInfo.strDbpwd);
	
	g_AppInfo.strDbauto=GIni::ini_GetFile(g_AppIni,_T("DB"),_T("AUTO"),_T("0"));
	g_AppInfo.strDbstate=GIni::ini_GetFile(g_AppIni,_T("DB"),_T("STATE"),_T("0"));
    	
	g_AppInfo.strDeviceid=AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB EQP CODE", "PNE");
	g_AppInfo.strBlockid=AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB Block No", "1");	
	
	g_AppInfo.iState=1;	
	g_AppInfo.bChBackTimeStart=FALSE;

//yulee 20190705 碍力捍纺 可记 贸府 Mark
	g_AppInfo.iPType=AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SBC_PTYPE",0);//1-Parallel CH   //yulee 20190705 FP Mark
	//cny----------------------------
	g_AppInfo.iEnd=0;
	g_AppInfo.iCellBalShow=AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_SHOW", 1);
	g_AppInfo.iCellBalLog=AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_LOG", 1);
	g_AppInfo.iCellBalLog=AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_LOG_RUNINRESULT", 1);
	g_AppInfo.iCellBalAuxDiv3=AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_AUXDIV3", 0);

	g_AppInfo.strCellBal_CfgFile.Format(_T("%s\\%s"),g_AppPath,COMMON_CELLBAL_CFG_FILE);
	g_AppInfo.strSetLanuageFile.Format(_T("%s\\%s"),g_AppPath,PROGRAM_LANGUAGE_SET);

	g_AppInfo.iPwrSupply_CmdWaitTime=AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "PWRSUPPLY_CMDWAITTIME", 200);
}


CHINFO* Global::ch_GetChList(int nModuleID,int  nChannelIndex)
{
	int ncount=g_AppInfo.ChList.GetCount();	
	if(ncount>0)
	{
		CHINFO *chinfo=NULL;
		POSITION pos =  g_AppInfo.ChList.GetHeadPosition();
		
		for(int i=0;i<ncount;i++)
		{
			chinfo=(CHINFO*)g_AppInfo.ChList.GetNext(pos);
			if(!chinfo) continue;
			
			if(chinfo->nModuleID==nModuleID &&      //module
				chinfo->pRecord.chNo==nChannelIndex) //channel
			{								
				return chinfo;
			}
		}
	}
	
	CHINFO *chinfo=new CHINFO;	
	chinfo->nModuleID=nModuleID;
	chinfo->pRecord.chNo=(unsigned char)nChannelIndex;
	
	g_AppInfo.ChList.AddTail(chinfo);
	return chinfo;
}

void Global::ch_SetChReset()
{
	int ncount=g_AppInfo.ChList.GetCount();
	if(ncount>0)
	{
		CHINFO *chinfo=NULL;
		POSITION pos =  g_AppInfo.ChList.GetHeadPosition();
		
		for(int i=0;i<ncount;i++)
		{
			chinfo=(CHINFO*)g_AppInfo.ChList.GetNext(pos);
			if(!chinfo) continue;
			
			chinfo->nomonitor="";
		}
	}
}

CHINFO* Global::ch_GetChList(int nChannelNum)
{
	int ncount=g_AppInfo.ChList.GetCount();
	if(ncount>0)
	{
		CHINFO *chinfo=NULL;
		POSITION pos=g_AppInfo.ChList.GetHeadPosition();
		
		for(int i=0;i<ncount;i++)
		{
			chinfo=(CHINFO*)g_AppInfo.ChList.GetNext(pos);
			if(!chinfo) continue;
			
			if(chinfo->nChannelIndex==nChannelNum) //channel
			{				
				return chinfo;
			}
		}
	}
	return NULL;
}


CString Global::GetChInfo(int iCHNO,CString strKey)
{
	CString CHNO;
	CHNO.Format(_T("CH%d"),iCHNO);
	
	return GIni::ini_GetFile(g_AppInfo.strCellBal_CfgFile,CHNO,strKey,_T(""));
}

void Global::SetChInfo(int iCHNO,CString strKey,CString strValue)
{
	CString CHNO;
	CHNO.Format(_T("CH%d"),iCHNO);
	
	GIni::ini_SetFile(g_AppInfo.strCellBal_CfgFile,CHNO,strKey,strValue);
}

CString Global::onGetCHAR(char *buf,int nLen,int nStart,CString strSplit,int iTpChar)
{
	CString strValue=_T("");
	CString strTemp;
	
	for(int i=nStart;i<nLen;i++)
	{
		strTemp.Format(_T("%c"),(BYTE)buf[i]);
		
		if(iTpChar==1)
		{
			if(buf[i]==' ') continue;//blank remove
			if(GStr::str_ChkNumeric(strTemp)==TRUE)
			{
				strTemp.Format(_T("%02d"),(BYTE)buf[i]-'A'+1);
			}
		}
		else if(iTpChar==2)
		{
			strTemp.Format(_T("%d"),(BYTE)buf[i]);
		}
		else if(iTpChar==3)
		{
			strTemp.Format(_T("%02X"),(BYTE)buf[i]);
		}
		strValue+=strTemp+strSplit;
	}
	strValue.TrimRight(_T("&"));
	return strValue;
}


CString Global::onGetBYTE(BYTE *buf,int nLen,int nStart,CString strSplit,int iTpChar)
{
	CString strValue=_T("");
	CString strTemp;
	
	for(int i=nStart;i<nLen;i++)
	{		
		if(iTpChar==4)
		{
			if(buf[i]==0x02) continue;
			if(buf[i]==0x0D) continue;
			if(buf[i]==0x0A) continue;
		}
		strTemp.Format(_T("%c"),buf[i]);
		
		if(iTpChar==1)
		{
			if(buf[i]==' ') continue;//blank remove
			if(GStr::str_ChkNumeric(strTemp)==TRUE)
			{
				strTemp.Format(_T("%02d"),buf[i]-'A'+1);
			}
		}
		else if(iTpChar==2)
		{
			strTemp.Format(_T("%d"),buf[i]);
		}
		else if(iTpChar==3)
		{
			strTemp.Format(_T("%02X"),buf[i]);
		}
		strValue+=strTemp+strSplit;
	}
	strValue.TrimRight(_T("&"));
	return strValue;
}

void Global::onSbcCh_GetList(CComboBox *cbo)
{
	if(!cbo) return;
	cbo->ResetContent();

	if(!mainView) return;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return;
	
	CCyclerModule *pMD=NULL;
	CCyclerChannel *lpChannelInfo=NULL;

	cbo->AddString(_T("0"));
	
	for ( UINT nI = 0; nI < (UINT)pDoc->GetInstallModuleCount(); nI++ )
	{
		pMD = pDoc->GetModuleInfo(pDoc->GetModuleID(nI));
		if(pMD == NULL) continue;
		
		int nModuleID=pMD->GetModuleID();
		CString ModuleNO=GStr::str_IntToStr(nModuleID);
		
		for(UINT ch =0; ch<pMD->GetTotalChannel(); ch++)
		{
			lpChannelInfo = pMD->GetChannelInfo(ch);
			if(lpChannelInfo == NULL) break;
			
			//int nChannelIndex=lpChannelInfo->GetChannelIndex();

			CString CHNO=GStr::str_IntToStr(lpChannelInfo->m_iCh);
			cbo->AddString(CHNO);		
		}
	}
}

CCyclerChannel* Global::onSbcCh_GetCHInfo(int iType,int nCHNO)
{
	if(!mainView) return NULL;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return NULL;
	
	CCyclerModule *pMD=NULL;
	CCyclerChannel *lpChannelInfo=NULL;
		
	for ( UINT nI = 0; nI < (UINT)pDoc->GetInstallModuleCount(); nI++ )
	{
		pMD = pDoc->GetModuleInfo(pDoc->GetModuleID(nI));
		if(pMD == NULL) continue;
		
		int nModuleID=pMD->GetModuleID();
		CString ModuleNO=GStr::str_IntToStr(nModuleID);
		
		for(UINT ch =0; ch<pMD->GetTotalChannel(); ch++)
		{
			lpChannelInfo = pMD->GetChannelInfo(ch);
			if(lpChannelInfo == NULL) break;
			
			int iCHNO=lpChannelInfo->m_iCh;
			int iState=lpChannelInfo->GetState();

			if(iType==1)
			{
				if(nCHNO==iCHNO) return lpChannelInfo;
			}
		}
	}
	return NULL;
}


void Global::onCopyChar(char *buf,CString strValue,int nSize)
{	
	int nLen=strValue.GetLength();
	if(nLen<1) return;
	
	for(int i=0;i<nSize;i++)
	{
		if( nLen > i )
		{
			buf[i]=(char)strValue[i];
		}
	}
}


void Global::onCopyCharArray(char *srcbuf,char *tgrbuf,int nSize)
{	
	for(int i=0;i<nSize;i++)
	{
		tgrbuf[i]=srcbuf[i];
	}
}

void Global::Fun_ChBackTimeClear(CTypedPtrList<CPtrList, CHBACKTIME*> &ListData)
{
	int nCount=ListData.GetCount();
	if(nCount<1) return;
	
	CHBACKTIME *pChBackTime=NULL;
	for(int i=0;i<nCount;i++)
    {
		POSITION pos = ListData.GetHeadPosition();
		if(!pos) continue;
		
		pChBackTime=(CHBACKTIME*)ListData.GetAt(pos);
		if(!pChBackTime) continue;
		
		delete pChBackTime;
		pChBackTime=NULL;
		ListData.RemoveAt(pos);
	}
	ListData.RemoveAll();
}

void Global::onLoadModeCopy(S_P1_CMD_BODY_LOAD_MODE_SET &cmdBodyLoadModeSet,
							S_P1_CMD_BODY_MODULE_INFO_REPLY &cmdBodyModuleInfoReply)
{
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.load_mode_set,cmdBodyLoadModeSet.load_mode_set,2);
	
	memset(cmdBodyLoadModeSet.reserved1,'0',sizeof(cmdBodyLoadModeSet.reserved1));
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.load_mode_value,cmdBodyLoadModeSet.load_mode_value,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.signal_sens_time,cmdBodyLoadModeSet.signal_sens_time,2);
	
	memset(cmdBodyLoadModeSet.reserved2,'0',sizeof(cmdBodyLoadModeSet.reserved2));
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.availability_voltage_upper,cmdBodyLoadModeSet.availability_voltage_upper,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.under_voltage,cmdBodyLoadModeSet.under_voltage,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.over_current,cmdBodyLoadModeSet.over_current,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.over_temp,cmdBodyLoadModeSet.over_temp,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.availability_voltage_lower,cmdBodyLoadModeSet.availability_voltage_lower,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.ch_cond_end_current,cmdBodyLoadModeSet.ch_cond_end_current,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.ch_cond_end_time,cmdBodyLoadModeSet.ch_cond_end_time,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.ch_cond_detection,cmdBodyLoadModeSet.ch_cond_detection,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.ch_cond_release,cmdBodyLoadModeSet.ch_cond_release,10);
	
	mainGlobal.onCopyCharArray(cmdBodyModuleInfoReply.ch_cond_auto_stop_time,cmdBodyLoadModeSet.ch_cond_auto_stop_time,10);
}

CHBACKTIME* Global::Fun_ChBackTimeFind(CTypedPtrList<CPtrList, CHBACKTIME*> &ListData,int iBackCH)
{	
	int nCount=ListData.GetCount();
	if(nCount<1) return NULL;
	
	CHBACKTIME *pChBackTime=NULL;
	POSITION pos = ListData.GetHeadPosition();
	if(!pos) return FALSE;
	
	for(int i=0;i<nCount;i++)
	{
		pChBackTime=(CHBACKTIME*)ListData.GetNext(pos);
		if(!pChBackTime) continue;
		
		if(pChBackTime->nChannelIndex==(iBackCH-1))
		{
			return pChBackTime;
		}
	}
	return NULL;
}