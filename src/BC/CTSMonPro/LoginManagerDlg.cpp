// LoginManagerDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "LoginManagerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginManagerDlg dialog


//CLoginManagerDlg::CLoginManagerDlg(CWnd* pParent /*=NULL*/)
CLoginManagerDlg::CLoginManagerDlg(long lAuth, BOOL bPneOnly, CWnd* pParent /*=NULL*/) //ksj 20200203 : 권한 설정, PNE 전용 여부.
//	: CDialog(CLoginManagerDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CLoginManagerDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CLoginManagerDlg::IDD2):
	(CLoginManagerDlg::IDD), pParent)
	, m_lAuth(0)
	, m_bPneOnly(FALSE)
{ 
	//{{AFX_DATA_INIT(CLoginManagerDlg)
	m_strUserID = _T("");
	m_strPassword = _T("");
	ZeroMemory(&m_LoginInfo,sizeof(SCH_LOGIN_INFO)); //ksj 20200203
	m_bPneOnly = bPneOnly; //ksj 20200203
	m_lAuth = lAuth; //ksj 20200203

	//}}AFX_DATA_INIT
}


void CLoginManagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoginManagerDlg)
	DDX_Text(pDX, IDC_LOGINID, m_strUserID);
	DDX_Text(pDX, IDC_PASSWORD, m_strPassword);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLoginManagerDlg, CDialog)
	//{{AFX_MSG_MAP(CLoginManagerDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CLoginManagerDlg::OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoginManagerDlg message handlers

BOOL CLoginManagerDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateData();
	m_strUserID = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Manager Login ID", "admin" );
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginManagerDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	CDaoDatabase  db;
	
	CString strCurPath,strDataBaseName;
	CString strUser,strPsw;
	strUser = m_strUserID;
	strPsw = m_strPassword;
	strUser.MakeUpper();
	strPsw.MakeUpper();

	//if (strUser == "PNE" && strPsw == "PNESOLUTION!")
	if (strUser == "PNE" && strPsw == "PNESOLUTION!1") //ksj 20200203 : 비번 변경
	{
		UpdateData(FALSE);		
		CDialog::OnOK();
		return;
	}
	//ksj 20200203 : 로그인 시도한 계정이 PNE 계정이 아니지만, 권한이 PNE 전용 인경우
	else if (m_bPneOnly)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_LOGON_DLG"));
		//@ AfxMessageBox("관리자 ID와 Password를 확인 하세요.");
		return;
	}


	strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
	if(strCurPath.IsEmpty())	
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_LOGON_DLG"));
		//@ AfxMessageBox("프로그램 설치 정보를 찾을 수 없습니다.");
		return ;
	}
	
	//DataBase 폴더 생성

// 	switch(nLanguage) 20190701
// 	{
// 	case 1: strDataBaseName = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME; break;
// 	case 2: strDataBaseName = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	case 3: strDataBaseName = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_PL; break;
// 	default : strDataBaseName = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	}

	//ksj 20200203 : 주석처리
	/*strDataBaseName = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME;
	
	try
	{
		db.Open(strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return ;
	}	
	
	
	CString strSQL;
	strSQL.Format("SELECT * FROM User Where UserID='%s' and Password='%s'", m_strUserID,m_strPassword);
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return ;
	}	
	
	//if(!rs.IsBOF() && !rs.IsEOF())
	if(!rs.IsEOF())
	{
		//사용 있음
		data = rs.GetFieldValue(0);
		AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "Manager Login ID", m_strUserID );

	}
	else
	{
		//사용자 없음
		rs.Close();
		db.Close();
		UpdateData(FALSE);
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_LOGON_DLG"));
		//@ AfxMessageBox("관리자 ID와 Password를 확인 하세요.");
		return;
	}
	
	rs.Close();
	db.Close();
	UpdateData(FALSE);	*/	

	//ksj 20200203 : 권한 별 로그인 기능 추가
	SCH_LOGIN_INFO	loginData;
	if(SearchUser(strUser, TRUE, strPsw, &loginData) == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_LOGON_DLG"));
		//@ AfxMessageBox("관리자 ID와 Password를 확인 하세요.");
		return;
	}

	if((loginData.nPermission & m_lAuth) != m_lAuth) //권한 부족
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_LOGON_DLG"));
		//@ AfxMessageBox("관리자 ID와 Password를 확인 하세요.");
		return;
	}
	
	UpdateData(FALSE);
	//ksj end

	CDialog::OnOK();
}

void CLoginManagerDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

//ksj 20200203 : 함수 추가 (CTSEditor 발췌)
BOOL CLoginManagerDlg::SearchUser(CString strUserID, BOOL bPassWordCheck, CString strPassword, SCH_LOGIN_INFO *pUserData)
{
	CUserRecordSet rs;

	rs.m_strFilter.Format("[UserID] = '%s'", strUserID);

	try
	{
		rs.Open();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		return FALSE;
	}

	if(bPassWordCheck)
	{
		//if(rs.m_Password != strPassword)
		CString strTmpPassword = rs.m_Password;
		strTmpPassword.MakeUpper(); //대문자로 바꿔서 비교.

		if(strTmpPassword != strPassword)
		{
			rs.Close();
			return FALSE;
		}
	}

	if(pUserData != NULL)
	{
		CString strTemp;
		pUserData->nPermission = rs.m_Authority;
		sprintf(pUserData->szDescription, rs.m_Description.operator const char*());
		sprintf(pUserData->szUserName, rs.m_Name.operator const char*());
		sprintf(pUserData->szPassword, rs.m_Password.operator const char*());
		strTemp = rs.m_RegistedDate.Format();
		sprintf(pUserData->szRegistedDate, strTemp.operator const char*());
		sprintf(pUserData->szLoginID, rs.m_UserID.operator const char*());
		pUserData->bUseAutoLogOut = rs.m_AutoLogOut;
		pUserData->lAutoLogOutTime = rs.m_AutoLogOutTime;
	}
	rs.Close();
	return TRUE;
}