// SettingCanFdDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "SettingCanFdDlg.h"
#include "RealEditCtrl.h"
#include "MyGridWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define CAN_COL_COUNT			22

#define CAN_COL_NO				0
#define CAN_COL_ID				1
#define CAN_COL_NAME			2
#define CAN_COL_STARTBIT		3
#define CAN_COL_BITCOUNT		4
#define CAN_COL_BYTE_ORDER		5
#define CAN_COL_DATATYPE		6
#define CAN_COL_FACTOR			7
#define CAN_COL_OFFSET			8
#define CAN_COL_FAULT_UPPER		9
#define CAN_COL_FAULT_LOWER		10
#define CAN_COL_END_UPPER		11
#define CAN_COL_END_LOWER		12
#define CAN_COL_DEFAULT			13
#define CAN_COL_SENT_TIME		14
#define CAN_COL_DIVISION1		15
#define CAN_COL_DIVISION2		16
#define CAN_COL_DIVISION3		17
#define CAN_COL_STARTBIT2		18
#define CAN_COL_BITCOUNT2		19
#define CAN_COL_BYTE_ORDER2		20
#define CAN_COL_DATATYPE2		21
#define CAN_COL_DEFAULT2		22

#define TAB_MASTER		0
#define TAB_SLAVE		1

#define ID_CAN_PAUSE			1801		//일시정지 사용
#define ID_CAN_MESSAGE			1802		//메시지 표시(SBC -> PC(EMG Message로 받음 ))
#define ID_CAN_PAUSE_MESSAGE	1803		//PAUSE & 메시지 표시
#define ID_CAN_WARNING			1804		//BMS Warning
#define ID_CAN_ERROR			1805		//BMS Error
#define ID_CAN_REST_OFF			1806		//BMS REST_OFF

#define ID_CAN_CELL_VTG_ALL		151
#define ID_CAN_CELL_VTG_HIGH	152
#define ID_CAN_CELL_VTG_LOW		153
#define ID_CAN_TEMPE_ALL		154
#define ID_CAN_TEMPE_HIGH		155
#define ID_CAN_TEMPE_LOW		156
#define ID_CAN_BMS_FLT			157
#define ID_CAN_SOC_ALL			158
#define ID_CAN_SOC_HIGH			159
#define ID_CAN_SOC_LOW			160
#define ID_CAN_USER_RUN			301
#define ID_CAN_USER_STOP		302
#define ID_CAN_USER_PAUSE		303
#define ID_CAN_USER_CONTINUE	304
#define ID_CAN_USER_NEXT		305
#define ID_CAN_USER_GOTO		306
#define ID_CAN_USER_STATE		307

/////////////////////////////////////////////////////////////////////////////
// CSettingCanFdDlg dialog
typedef struct CONFIGDATA
{
	char canID[8];
	char ExtID;
	char BaudRate;
	long CV;
	char UserFilterMode;
	char Mask1[8];
	char Filter1_1[8];
	char Filter1_2[8];
	char Mask2[8];
	char Filter2_1[8];
	char Filter2_2[8];
	char Filter2_3[8];
	char Filter2_4[8];	
}ConfigFile;

CSettingCanFdDlg::CSettingCanFdDlg(CCTSMonProDoc * pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CSettingCanFdDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSettingCanFdDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSettingCanFdDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSettingCanFdDlg::IDD3):
	(CSettingCanFdDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSettingCanFdDlg)
	m_strCanID = _T("");
	m_ckExtID = FALSE;
	m_ckUserFilter = FALSE;
	m_strMask1 = _T("");
	m_strFilter1_1 = _T("");
	m_strFilter1_2 = _T("");
	m_strMask2 = _T("");
	m_strFilter2_1 = _T("");
	m_strFilter2_2 = _T("");
	m_strFilter2_3 = _T("");
	m_strFilter2_4 = _T("");
// 	m_strBaudRate = _T("");
// 	m_strBaudRateSlave = _T("");
	m_strCanIDSlave = _T("");
	m_strMask1Slave = _T("");
	m_strMask2Slave = _T("");
	m_strFilter1_1Slave = _T("");
	m_strFilter1_2Slave = _T("");
	m_strSelectCh = _T("");
	m_ckExtIDSlave = FALSE;
	m_ckUserFilterSlave = FALSE;
	m_strFilter2_1Slave = _T("");
	m_strFilter2_2Slave = _T("");
	m_strFilter2_3Slave = _T("");
	m_strFilter2_4Slave = _T("");
	m_nMasterIDType = 0;
	m_strType = _T("");
	m_nProtocolVer = 0;
	m_strDescript = _T   ("");
	m_ckCanFD = FALSE;
	m_ckCanFD_Slave = FALSE;
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	IsChange = FALSE;
	nBeforeMasterIdType = 0;
	nMasterIdType = 0;
	nLINtoCANSelect = 0; //ksj 20210210 : 초기화 추가.
	}        


void CSettingCanFdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingCanFdDlg)
	DDX_Control(pDX, IDC_COMBO_CAN_TERMINAL_S, m_ctrlBmsTerminalSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_CRC_S, m_ctrlBmsCrcTypeSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_CRC_M, m_ctrlBmsCrcType);
	DDX_Control(pDX, IDC_COMBO_CAN_TERMINAL_M, m_ctrlBmsTerminal);
	DDX_Control(pDX, IDC_COMBO_CAN_DATARATE_SLAVE, m_ctrlDataRateSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_DATARATE, m_ctrlDataRate);
	DDX_Control(pDX, IDC_BTN_ADD_MASTER, c_btn_decimal);
	DDX_Control(pDX, IDC_RADIO3, c_btn_hex);
	DDX_Control(pDX, IDC_COMBO_CAN_SJW_SLAVE, m_ctrlBmsSJWSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_SJW, m_ctrlBmsSJW);
	DDX_CBString(pDX, IDC_COMBO_CAN_SJW, m_strBmsSJW);
	DDX_CBString(pDX, IDC_COMBO_CAN_SJW_SLAVE, m_strBmsSJWSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_TYPE_SLAVE, m_ctrlBmsTypeSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_TYPE, m_ctrlBmsType);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_TYPE, m_strBmsType);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_TYPE_SLAVE, m_strBmsTypeSlave);
	DDX_Control(pDX, IDC_PARAM_TAB, m_ctrlParamTab);
	DDX_Control(pDX, IDC_STATIC_CH_MSG, m_ctrlLabelChMsg);
	DDX_Control(pDX, IDC_STATIC_SEL_CH_CAN, m_ctrlLabelChNo);
	DDX_Control(pDX, IDC_LABEL_MSG, m_ctrlLabelMsg);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE_SLAVE, m_ctrlBaudRateSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE, m_ctrlBaudRate);
	DDX_Text(pDX, IDC_EDIT1, m_strCanID);
	DDV_MaxChars(pDX, m_strCanID, 8);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE, m_ckExtID);
	DDX_Check(pDX, IDC_CHECK_USER_FILTER, m_ckUserFilter);
	DDX_Text(pDX, IDC_EDIT2, m_strMask1);
	DDV_MaxChars(pDX, m_strMask1, 8);
	DDX_Text(pDX, IDC_EDIT3, m_strFilter1_1);
	DDV_MaxChars(pDX, m_strFilter1_1, 8);
	DDX_Text(pDX, IDC_EDIT4, m_strFilter1_2);
	DDV_MaxChars(pDX, m_strFilter1_2, 8);
	DDX_Text(pDX, IDC_EDIT5, m_strMask2);
	DDV_MaxChars(pDX, m_strMask2, 8);
	DDX_Text(pDX, IDC_EDIT6, m_strFilter2_1);
	DDV_MaxChars(pDX, m_strFilter2_1, 8);
	DDX_Text(pDX, IDC_EDIT7, m_strFilter2_2);
	DDV_MaxChars(pDX, m_strFilter2_2, 8);
	DDX_Text(pDX, IDC_EDIT8, m_strFilter2_3);
	DDV_MaxChars(pDX, m_strFilter2_3, 8);
	DDX_Text(pDX, IDC_EDIT9, m_strFilter2_4);
	DDV_MaxChars(pDX, m_strFilter2_4, 8);
	DDX_Text(pDX, IDC_EDIT_SLAVE_CANID, m_strCanIDSlave);
	DDV_MaxChars(pDX, m_strCanIDSlave, 8);
	DDX_Text(pDX, IDC_SLAVE_MASK1, m_strMask1Slave);
	DDV_MaxChars(pDX, m_strMask1Slave, 8);
	DDX_Text(pDX, IDC_SLAVE_MASK2, m_strMask2Slave);
	DDV_MaxChars(pDX, m_strMask2Slave, 8);
	DDX_Text(pDX, IDC_SLAVE_MASK1_FILTER1, m_strFilter1_1Slave);
	DDV_MaxChars(pDX, m_strFilter1_1Slave, 8);
	DDX_Text(pDX, IDC_SLAVE_MASK1_FILTER2, m_strFilter1_2Slave);
	DDV_MaxChars(pDX, m_strFilter1_2Slave, 8);
	DDX_Text(pDX, IDC_STATIC_SEL_CH_CAN, m_strSelectCh);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE_SLAVE, m_ckExtIDSlave);
	DDX_Check(pDX, IDC_CHECK_USER_FILTER_SLAVE, m_ckUserFilterSlave);
	DDX_Text(pDX, IDC_SLAVE_MASK2_FILTER1, m_strFilter2_1Slave);
	DDV_MaxChars(pDX, m_strFilter2_1Slave, 8);
	DDX_Text(pDX, IDC_SLAVE_MASK2_FILTER2, m_strFilter2_2Slave);
	DDV_MaxChars(pDX, m_strFilter2_2Slave, 8);
	DDX_Text(pDX, IDC_SLAVE_MASK2_FILTER3, m_strFilter2_3Slave);
	DDV_MaxChars(pDX, m_strFilter2_3Slave, 8);
	DDX_Text(pDX, IDC_SLAVE_MASK2_FILTER4, m_strFilter2_4Slave);
	DDV_MaxChars(pDX, m_strFilter2_4Slave, 8);
	DDX_Radio(pDX, IDC_RADIO3, m_nMasterIDType);
	DDX_Check(pDX, IDC_CHECK_CAN_FD, m_ckCanFD);
	DDX_Check(pDX, IDC_CHECK_CAN_FD_SLAVE, m_ckCanFD_Slave);
	DDX_Control(pDX, IDC_FRAME_SETTING, m_ctrlFrameSetting); //lyj 20200317 CanFDDlg 최대화
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSettingCanFdDlg, CDialog)
	//{{AFX_MSG_MAP(CSettingCanFdDlg)
	ON_BN_CLICKED(IDC_BTN_LOAD_CAN_SETUP, OnBtnLoadCanSetup)
	ON_BN_CLICKED(IDC_BTN_SAVE_CAN_SETUP, OnBtnSaveCanSetup)
	ON_BN_CLICKED(IDC_CHECK_USER_FILTER, OnCheckUserFilter)
	ON_BN_CLICKED(IDC_CHECK_USER_FILTER_SLAVE, OnCheckUserFilterSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BAUDRATE_SLAVE, OnSelchangeComboCanBaudrateSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BAUDRATE, OnSelchangeComboCanBaudrate)
	ON_BN_CLICKED(IDC_BTN_ADD_MASTER, OnBtnAddMaster)
	ON_BN_CLICKED(IDC_BTN_DEL_MASTER, OnBtnDelMaster)
	ON_BN_CLICKED(IDC_BTN_ADD_SLAVE, OnBtnAddSlave)
	ON_BN_CLICKED(IDC_BTN_DEL_SLAVE, OnBtnDelSlave)
	ON_EN_CHANGE(IDC_EDIT1, OnChangeEdit1)
	ON_EN_CHANGE(IDC_EDIT10, OnChangeEdit10)
	ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit2)
	ON_EN_CHANGE(IDC_EDIT3, OnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT4, OnChangeEdit4)
	ON_EN_CHANGE(IDC_EDIT5, OnChangeEdit5)
	ON_EN_CHANGE(IDC_EDIT6, OnChangeEdit6)
	ON_EN_CHANGE(IDC_EDIT7, OnChangeEdit7)
	ON_EN_CHANGE(IDC_EDIT8, OnChangeEdit8)
	ON_EN_CHANGE(IDC_EDIT9, OnChangeEdit9)
	ON_EN_CHANGE(IDC_SLAVE_MASK1, OnChangeSlaveMask1)
	ON_EN_CHANGE(IDC_SLAVE_MASK1_FILTER1, OnChangeSlaveMask1Filter1)
	ON_EN_CHANGE(IDC_SLAVE_MASK1_FILTER2, OnChangeSlaveMask1Filter2)
	ON_EN_CHANGE(IDC_SLAVE_MASK2, OnChangeSlaveMask2)
	ON_EN_CHANGE(IDC_SLAVE_MASK2_FILTER1, OnChangeSlaveMask2Filter1)
	ON_EN_CHANGE(IDC_SLAVE_MASK2_FILTER2, OnChangeSlaveMask2Filter2)
	ON_EN_CHANGE(IDC_SLAVE_MASK2_FILTER3, OnChangeSlaveMask2Filter3)
	ON_EN_CHANGE(IDC_SLAVE_MASK2_FILTER4, OnChangeSlaveMask2Filter4)
	ON_EN_CHANGE(IDC_EDIT_SLAVE_CANID, OnChangeEditSlaveCanid)
	ON_EN_CHANGE(IDC_EDIT_SLAVE_CV, OnChangeEditSlaveCv)
	ON_BN_CLICKED(IDC_CHECK_EXT_MODE, OnCheckExtMode)
	ON_BN_CLICKED(IDC_CHECK_EXT_MODE_SLAVE, OnCheckExtModeSlave)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	ON_NOTIFY(TCN_SELCHANGE, IDC_PARAM_TAB, OnSelchangeParamTab)
	ON_BN_CLICKED(IDC_BUT_NOT_USE, OnButNotUse)
	ON_BN_CLICKED(IDC_BTN_LOAD_CAN_DBC, OnBtnLoadCanDbc)
	ON_BN_CLICKED(IDC_CHECK_CAN_FD, OnCheckCanFd)
	ON_BN_CLICKED(IDC_CHECK_CAN_FD_SLAVE, OnCheckCanFdSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_DATARATE, OnSelchangeComboCanDatarate)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_TERMINAL_M, OnSelchangeComboCanTerminalM)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_CRC_M, OnSelchangeComboCanCrcM)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_SJW_SLAVE, OnSelchangeComboCanSjwSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BMS_TYPE_SLAVE, OnSelchangeComboCanBmsTypeSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_DATARATE_SLAVE, OnSelchangeComboCanDatarateSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_TERMINAL_S, OnSelchangeComboCanTerminalS)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_CRC_S, OnSelchangeComboCanCrcS)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_SJW, OnSelchangeComboCanSjw)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BMS_TYPE, OnSelchangeComboCanBmsType)
	ON_BN_CLICKED(IDC_BTN_LOAD_CAN_DBC_SLAVE, OnBtnLoadCanDbcSlave)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnGridBeginEdit)
	ON_MESSAGE(WM_GRID_PASTE, OnGridPaste)
	ON_MESSAGE(WM_GRID_CLICK, OnLBGridClick)
	ON_MESSAGE(WM_GRID_ENDEDIT, OnGridEndEditing)
	ON_MESSAGE(WM_GRID_MOVEROW, OnGridLeftCell)	
	ON_WM_SYSCOMMAND()
	ON_WM_SIZE()
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_SJW, &CSettingCanFdDlg::OnCbnEditchangeComboCanSjw)
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_BMS_TYPE, &CSettingCanFdDlg::OnCbnEditchangeComboCanBmsType)
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_SJW_SLAVE, &CSettingCanFdDlg::OnCbnEditchangeComboCanSjw)
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_BMS_TYPE_SLAVE, &CSettingCanFdDlg::OnCbnEditchangeComboCanBmsType)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingCanFdDlg message handlers

BOOL CSettingCanFdDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_nMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);
	
	//2014.08.12 캔 수신데이터 타입 hex타입 사용여부.
	IsCanValHex = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCanValHex", FALSE);	
	
	//if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnInitDialog_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
	m_pDoc->Fun_GetArryCanDivision(m_uiArryCanCode);	
	m_pDoc->Fun_GetArryCanStringName(m_strArryCanName);	

	InitParamTab();
	InitGridMaster();
	InitGridSlave();
	InitLabel();


	m_strCanID = "FFFFFFFF";
	m_strCanIDSlave = "FFFFFFFF";
	//m_strSelectCh.Format("%d 번", nSelCh+1);
	m_strSelectCh.Format(Fun_FindMsg("SettingCanFdDlg_OnInitDialog_msg2","IDD_CAN_FD_SETTING_DLG"), nSelCh+1);//&&
	UpdateData(FALSE);
	m_ctrlBaudRate.SetCurSel(3);
	m_ctrlBaudRateSlave.SetCurSel(3);

	m_ctrlBmsType.SetCurSel(0);
	m_ctrlBmsTypeSlave.SetCurSel(0);	
	m_ctrlBmsSJW.SetCurSel(2);
	m_ctrlBmsSJWSlave.SetCurSel(2);
	
	CString strName; 
	strName.Format("%s","CAN RX Setting "); //lyj 20200214
	this->SetWindowText(strName); 
	//this->SetWindowText(m_strTitle);	
	UpdateCommonData();
	UpdateGridData();

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if(pMD == NULL)	return FALSE;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return FALSE;
	
	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 	
	{
		//m_ctrlLabelMsg.SetText("동작중인 채널은 수정할 수 없습니다.");
		m_ctrlLabelMsg.SetText(Fun_FindMsg("SettingCanFdDlg_OnInitDialog_msg3","IDD_CAN_FD_SETTING_DLG"));//&&
		EnableAllControl(FALSE);		
	}

	/*if (gst_CanTxSave.bEnable == TRUE)
	{
		if (gst_CanTxSave.CanType == nLINtoCANSelect)
		{
			m_ctrlBaudRate.SetCurSel(gst_CanTxSave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->SetCheck(gst_CanTxSave.CanRxExtendedIDMode);
			m_ctrlBmsSJW.SetCurSel(gst_CanTxSave.SJW);
			m_ctrlBmsType.SetCurSel(gst_CanTxSave.BMSType);
		}
		memcpy(&gst_CanRxSave, &gst_CanTxSave, sizeof(CAN_RX_SAVE));
	}

	if (gst_CanTxSaveSlave.bEnable == TRUE)
	{
		if (gst_CanTxSaveSlave.CanType == nLINtoCANSelect)
		{
			m_ctrlBaudRateSlave.SetCurSel(gst_CanTxSaveSlave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->SetCheck(gst_CanTxSaveSlave.CanRxExtendedIDMode);
			m_ctrlBmsSJWSlave.SetCurSel(gst_CanTxSaveSlave.SJW);
			m_ctrlBmsTypeSlave.SetCurSel(gst_CanTxSaveSlave.BMSType);
		}
		memcpy(&gst_CanRxSaveSlave, &gst_CanTxSaveSlave, sizeof(CAN_RX_SAVE));
	}*/


	//ksj 20210210 : ??? 위에 왜 주석처리 해놨지??.??? 다시 살림...
	//요주의 관찰 필요.
	//ksj 20210712 : 무슨 기능인지 이력도 없고, 현재 사용에 방해가 되므로 일단 제거함
	//향후 필요한 경우 정확이 어떻게 필요한지 다시 파악하여 재구현 하더라도 일단 제거
	/*
	if (gst_CanTxSave.bEnable == TRUE)
	{
		if (gst_CanTxSave.CanType == nLINtoCANSelect)
		{
			m_ctrlBaudRate.SetCurSel(gst_CanTxSave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->SetCheck(gst_CanTxSave.CanRxExtendedIDMode);
			//m_ctrlBmsSJW.SetCurSel(gst_CanTxSave.SJW);
			//m_ctrlBmsType.SetCurSel(gst_CanTxSave.BMSType);

			//ksj 20210210 : List에 없는 값은 SetCurSel로 선택 안됨.
			//없는 값은 SetWindowText로 값을 강제로 넣어줘야함.
			CString strTemp;
			strTemp.Format("%d",gst_CanRxSave.SJW);
			m_ctrlBmsSJW.SetWindowText(strTemp);
			strTemp.Format("%d",gst_CanRxSave.BMSType);
			m_ctrlBmsType.SetWindowText(strTemp);
			//ksj end	

		}
		memcpy(&gst_CanRxSave, &gst_CanTxSave, sizeof(CAN_RX_SAVE));
	}

	if (gst_CanTxSaveSlave.bEnable == TRUE)
	{
		if (gst_CanTxSaveSlave.CanType == nLINtoCANSelect)
		{
			m_ctrlBaudRateSlave.SetCurSel(gst_CanTxSaveSlave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->SetCheck(gst_CanTxSaveSlave.CanRxExtendedIDMode);
			//m_ctrlBmsSJWSlave.SetCurSel(gst_CanTxSaveSlave.SJW);
			//m_ctrlBmsTypeSlave.SetCurSel(gst_CanTxSaveSlave.BMSType);

			//ksj 20210210 : List에 없는 값은 SetCurSel로 선택 안됨.
			//없는 값은 SetWindowText로 값을 강제로 넣어줘야함.
			CString strTemp;
			strTemp.Format("%d",gst_CanRxSaveSlave.SJW);
			m_ctrlBmsSJWSlave.SetWindowText(strTemp);
			strTemp.Format("%d",gst_CanRxSaveSlave.BMSType);
			m_ctrlBmsTypeSlave.SetWindowText(strTemp);
			//ksj end
		}
		memcpy(&gst_CanRxSaveSlave, &gst_CanTxSaveSlave, sizeof(CAN_RX_SAVE));
	}*/
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSettingCanFdDlg::InitGridMaster()
{
	m_wndCanGrid.SubclassDlgItem(IDC_CAN_GRID, this);
	m_wndCanGrid.Initialize();

	CRect rect;
	m_wndCanGrid.GetParam()->EnableUndo(FALSE);
	
	m_wndCanGrid.SetRowHeight(0,0,50); //lyj 20200317
	//m_wndCanGrid.SetRowCount(1);
	m_wndCanGrid.SetRowCount(0); //ksj 20210305
	m_wndCanGrid.SetColCount(CAN_COL_COUNT);
	m_wndCanGrid.GetClientRect(&rect);
	m_wndCanGrid.SetColWidth(CAN_COL_NO			, CAN_COL_NO			, 30);
	m_wndCanGrid.SetColWidth(CAN_COL_ID			, CAN_COL_ID			, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_NAME		, CAN_COL_NAME			, 120);
	m_wndCanGrid.SetColWidth(CAN_COL_STARTBIT	, CAN_COL_STARTBIT		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_BITCOUNT	, CAN_COL_BITCOUNT		, 60);
	//m_wndCanGrid.SetColWidth(CAN_COL_BYTE_ORDER	, CAN_COL_BYTE_ORDER	, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_BYTE_ORDER	, CAN_COL_BYTE_ORDER	, 62); //lyj 20200317 컬럼사이즈 조정
	m_wndCanGrid.SetColWidth(CAN_COL_DATATYPE	, CAN_COL_DATATYPE		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_FACTOR		, CAN_COL_FACTOR		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_OFFSET		, CAN_COL_OFFSET		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_FAULT_UPPER, CAN_COL_FAULT_UPPER	, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_FAULT_LOWER, CAN_COL_FAULT_LOWER	, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_END_UPPER	, CAN_COL_END_UPPER		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_END_LOWER	, CAN_COL_END_LOWER		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_DEFAULT		, CAN_COL_DEFAULT		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_SENT_TIME		, CAN_COL_SENT_TIME		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION1		, CAN_COL_DIVISION1		, 150);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION2		, CAN_COL_DIVISION2		, 150);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION3		, CAN_COL_DIVISION3		, 150);	
	m_wndCanGrid.SetColWidth(CAN_COL_STARTBIT2		, CAN_COL_STARTBIT2		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_BITCOUNT2		, CAN_COL_BITCOUNT2		, 80);
	m_wndCanGrid.SetColWidth(CAN_COL_BYTE_ORDER2	, CAN_COL_BYTE_ORDER2	, 80);
	m_wndCanGrid.SetColWidth(CAN_COL_DATATYPE2		, CAN_COL_DATATYPE2		, 80);
	m_wndCanGrid.SetColWidth(CAN_COL_DEFAULT2		, CAN_COL_DEFAULT2		, 100);	
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_NO			),	CGXStyle().SetValue("No"));
	
	if(m_nMasterIDType == 1)
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	else
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
		
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_NAME			),	CGXStyle().SetValue("Name"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT		),	CGXStyle().SetValue("StartBit"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT		),	CGXStyle().SetValue("BitCount"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER	),	CGXStyle().SetValue("ByteOrder"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE		),	CGXStyle().SetValue("DataType"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_FACTOR		),	CGXStyle().SetValue("Factor"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_OFFSET		),	CGXStyle().SetValue("Offset"));
	/*m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_FAULT_UPPER	),	CGXStyle().SetValue("안전상한"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_FAULT_LOWER	),	CGXStyle().SetValue("안전하한"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_END_UPPER		),	CGXStyle().SetValue("종료상한"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_END_LOWER		),	CGXStyle().SetValue("종료하한"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue("설정값"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_SENT_TIME		),	CGXStyle().SetValue("수신 시간(ms)"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue("분류 1"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue("분류 2"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue("분류 3"));
	*/
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_FAULT_UPPER	),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg1","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_FAULT_LOWER	),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg2","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_END_UPPER		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg3","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_END_LOWER		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg4","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg5","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_SENT_TIME		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg6","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg7","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg8","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg9","IDD_CAN_FD_SETTING_DLG")));//&&

	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT2		),	CGXStyle().SetValue("& StartBit"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT2		),	CGXStyle().SetValue("& BitCount"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER2	),	CGXStyle().SetValue("& ByteOrder"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE2		),	CGXStyle().SetValue("& DataType"));
	//m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT2		),	CGXStyle().SetValue("& 설정값"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT2		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg10","IDD_CAN_FD_SETTING_DLG")));//&&

	m_wndCanGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndCanGrid.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(0),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_ID),
		CGXStyle()
  			.SetControl(GX_IDS_CTRL_EDIT)
	);
	//Edit Ctrl////////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_SENT_TIME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// 		.SetChoiceList(_T("USER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\n"))
// 		.SetValue(_T(""))
// 	);

// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// 		.SetChoiceList(_T("Cell CV\r\nCapacity Volt\r\nFan Test\r\nECU ID\r\n"))
// 		.SetValue(_T(""))
// 		);

	m_wndCanGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndCanGrid, IDS_CTRL_REALEDIT));
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	char str[12], str2[7], str3[5];

	sprintf(str, "%%d", 1);		
	//sprintf(str2, "%d", 2);	//정수부 //yulee 20180329 권준구c 요청 정수부 크기 2->3 
	sprintf(str2, "%d", 3);		//정수부 2->3

	//.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63")) 63 -> 511로 변경 
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("511"))
			//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~511)"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg11","IDD_CAN_FD_SETTING_DLG")))//&&
			.SetValue(0L)
			
	);
	//.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63")) 63 -> 511로 변경
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT2),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("511"))
		//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~511)"))
		.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg11","IDD_CAN_FD_SETTING_DLG")))//&&
		.SetValue(0L)
		
		);

	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(1L)
	);
	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT2),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(0L)
		);

	sprintf(str, "%%.%dlf", 10);	//실수 포멧			ljb 20160825 박진호 요청 6->10
	sprintf(str2, "%d", 6);		    //정수부 크기		
	sprintf(str3, "%d", 10);		//소수부 크기		ljb 20160825 박진호 요청 6->10

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_FACTOR),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1L)
	);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);

	//default 전송 값, 기본값자 자리수 셋팅
	sprintf(str3, "%d", 6);		//소숫점 이하		// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str, "%%.%dlf", 6);						// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str2, "%d", 8);		//정수부			//ljb 12->8  [8/29/2011 XNOTE]
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT2),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);

	//division 전송 값, 기본값자 자리수 셋팅
	sprintf(str, "%%d", 2);		
	sprintf(str2, "%d", 4);		//정수부
	
	//전송 속도 기본값자 자리수 셋팅
	sprintf(str, "%%d", 1);		//정수부
	sprintf(str2, "%d", 5);		
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_SENT_TIME),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(0L)
		);

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
// 		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("1000"))
// 		.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))		
		.SetValue(0L)
		);

	sprintf(str3, "%d", 6);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 8);		//정수부

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_FAULT_UPPER, CAN_COL_END_LOWER),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(0L)
	);

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
				.SetValue(_T("Intel"))
		);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
		.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
		.SetValue(_T("Intel"))
		);
	if(IsCanValHex){
		m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
			CGXStyle().SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
			.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\nHex"))				
			.SetValue(_T("Unsigned"))
			
		);
	}else{
		m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
			CGXStyle().SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
			.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))				
			.SetValue(_T("Unsigned"))			
		);
	}
	
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
		//////////////////////////////////////////////////////////////////////////
		// + BW KIM 2014.01.23 100C 에서는 일단 이거 쓴다.
		// .SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
		.SetChoiceList(_T("Unsigned"))		
		// -
		//////////////////////////////////////////////////////////////////////////

		//.SetChoiceList(_T("Unsigned\r\nSigned\r\n"))
		.SetValue(_T("Unsigned"))
		);

	//ljb 2011222 이재복 S //////////
	CString strComboItem,strTemp;
	strComboItem = "None [0]\r\n";
	for (int i=0; i < m_strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",m_strArryCanName.GetAt(i),m_uiArryCanCode.GetAt(i));
		strComboItem = strComboItem + strTemp;
	}
	//////////////////////////////////////////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		//		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetControl(GX_IDS_CTRL_COMBOBOX)			//쓰기도 가능 //yulee 20190714 check
//		.SetChoiceList(_T("NONE\r\nCELL ALL\r\nCELL HIGH\r\nCELL LOW\r\nTEMP ALL\r\nTEMP HIGH\r\nTEMP LOW\r\nBMS Flt\r\nSOC ALL\r\nSOC HIGH\r\nSOC LOW\r\n"))
//		.SetChoiceList(_T("NONE\r\nCAN WARNING\r\nCAN ERROR\r\nCAN REST_OFF\r\nCAN PAUSE\r\nCAN MESSAGE\r\nCAN PAUSE_MESSAGE\r\nCELL ALL\r\nCELL HIGH\r\nCELL LOW\r\nTEMP ALL\r\nTEMP HIGH\r\nTEMP LOW\r\nBMS Flt\r\nSOC ALL\r\nSOC HIGH\r\nSOC LOW\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
//		.SetChoiceList(_T("NONE\r\nCAN WARNING (1804)\r\nCAN ERROR (1805)\r\nCAN REST_OFF (1806)\r\nCAN PAUSE (1801)\r\nCAN MESSAGE (1802)\r\nCAN PAUSE & MESSAGE (1803)\r\n"))
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);
}

void CSettingCanFdDlg::OnBtnLoadCanSetup() 
{
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
					"csv", "csv");

	CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	if(IDOK == pDlg.DoModal())
	{
		if(m_wndCanGrid.GetRowCount() > 0)
			m_wndCanGrid.RemoveRows(1, m_wndCanGrid.GetRowCount());
		if(m_wndCanGridSlave.GetRowCount() > 0)
			m_wndCanGridSlave.RemoveRows(1, m_wndCanGridSlave.GetRowCount());
		if(LoadCANConfig(pDlg.GetPathName()) == TRUE)
		{

			CString strCommType;
			if(nLINtoCANSelect == 0)
				strCommType.Format(_T("CAN"));
			else if(nLINtoCANSelect == 1)
				strCommType.Format(_T("LinToCAN"));
			else if(nLINtoCANSelect == 2)
				strCommType.Format(_T("RS485"));
			else if(nLINtoCANSelect == 3)
				strCommType.Format(_T("SMbus"));

			CString strName;
			//strName.Format(Fun_FindMsg("OnBtnLoadCanSetup_msg","IDD_CAN_SETTING_DLG"),  strCommType, pDlg.GetFileName());
			//CString strName;
			////strName.Format("CAN 설정 - %s",  pDlg.GetFileName());
			//strName.Format(Fun_FindMsg("SettingCanFdDlg_OnBtnLoadCanSetup_msg1","IDD_CAN_FD_SETTING_DLG"),  pDlg.GetFileName());//&&
			strName.Format("CAN Receive - %s",  pDlg.GetFileName()); //lyj 20200213
			this->SetWindowText(strName);

			IsChangeFlag(TRUE);
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}

	
}

void CSettingCanFdDlg::OnBtnSaveCanSetup() 
{
	UpdateData();
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\DataBase\\Config", szCurDir);

	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
					"csv", "csv");


	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		if(SaveCANConfig(pDlg.GetPathName()) == TRUE)
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
	}
	
}


BOOL CSettingCanFdDlg::SaveCANConfig(CString strSavePath)
{
	UpdateData();
	CFile file;
	CString strData,strSaveData;
	CString strTitle, strTitle1;
	int i = 0;
	strSaveData.Empty();

	//Start Save Master/////////////////////////////////////////////////////////////////////////
	strTitle1 = "**Data Info**\r\nTYPE,VERSION,DESCRIPT\r\n";

	//strTitle = "**Master Configuration**\r\nID(Hex),BaudRate,Ext ID,CV Value,User Filter Mode,Mask1(Hex),Filter1(Hex),Filter2(Hex),Mask2(Hex),Filter1(Hex),Filter2(Hex),Filter3(Hex),Filter4(Hex)\r\n";
	strTitle = "**Master Configuration**\r\nID(Hex),BaudRate,Ext ID,CAN FD,User Filter Mode,Mask1(Hex),Filter1(Hex),Filter2(Hex),Mask2(Hex),Filter1(Hex),Filter2(Hex),Filter3(Hex),Filter4(Hex),Bms Type,Bms SJW,CAN FD DataRate,CAN Terminal,CAN CRC\r\n";

	TRY
	{
		if(strSavePath.Find('.') < 0)
			strSavePath += ".csv";

		if(file.Open(strSavePath, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			//AfxMessageBox("파일을 생성할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_SaveCANConfig_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
			return FALSE;
		}

		file.Write(strTitle1, strTitle1.GetLength());	//Title1

//		strData.Format("%s,", m_strType);
		strData.Format("RECEIVE,");
		file.Write(strData, strData.GetLength()); //Type

//		strData.Format("%d,", m_nProtocolVer);
//		strData.Format("3,");				      //ljb 201008 2->3(SentTime,분류 2,분류3 추가)
		strData.Format("4,");				      //ljb 20180326 3->4( CAN 2.0, CAN FD 구별)
		file.Write(strData, strData.GetLength()); //ProtocolVer
		m_strDescript = m_strTitle; //lyj 20200730 설정 저장 시 title 값으로 descript 저장

		strData.Format("%s,\r\n\r\n", m_strDescript);
//		strData.Format("PNE CAN CONFIG FILE,\r\n\r\n");
		file.Write(strData, strData.GetLength()); //Descript

		file.Write(strTitle, strTitle.GetLength());	//Title 

		strSaveData.Format("%s,", m_strCanID);
//		file.Write(strData, strData.GetLength()); //ID

		m_ctrlBaudRate.GetWindowText(strData);	//ljb 20180326
// 		if(m_strBaudRate == "125 kbps")
// 			strData.Format("%d,", 125);
// 		else if(m_strBaudRate == "250 kbps")
// 			strData.Format("%d,", 250);
// 		else if(m_strBaudRate == "500 kbps")
// 			strData.Format("%d,", 500);
// 		else if(m_strBaudRate == "1 Mbps")
// 			strData.Format("%d,", 1000);
		strSaveData += strData + ",";
//		file.Write(strData, strData.GetLength()); //BaudRate

		strData.Format("%d,",m_ckExtID);	
		strSaveData += strData;
//		file.Write(strData, strData.GetLength()); //Ext ID

		//strData="0,";
		strData.Format("%d,", m_ckCanFD);	//ljb 20180326
		strSaveData += strData;
//		file.Write(strData, strData.GetLength());	// CV Value -> ljb 20180326 edit CAN 2.0, CAN FD 구별 인자로 사용

		strData.Format("%d,",m_ckUserFilter);	
		strSaveData += strData;
//		file.Write(strData, strData.GetLength()); //UserFilter
		
		if(m_strMask1 == "")	strData.Format("0,");	//Mask1
		else					strData.Format("%s,",m_strMask1);	//Mask1
		strSaveData += strData;
//		file.Write(strData, strData.GetLength());

		if(m_strFilter1_1 == "")	strData.Format("0,");	//Filter1_1
		else					strData.Format("%s,",m_strFilter1_1);	//Filter1_1
		strSaveData += strData;
//		file.Write(strData, strData.GetLength()); 

		if(m_strFilter1_2 == "")	strData.Format("0,");	//Filter1_2
		else					strData.Format("%s,",m_strFilter1_2);	//Filter1_2
		strSaveData += strData;
//		file.Write(strData, strData.GetLength()); 

		if(m_strMask2 == "")	strData.Format("0,");	//Mask2
		else					strData.Format("%s,",m_strMask2);	//Mask2
		strSaveData += strData;
//		file.Write(strData, strData.GetLength()); 

		if(m_strFilter2_1 == "")	strData.Format("0,");	//Filter1_1
		else					strData.Format("%s,",m_strFilter2_1);	//Filter1_1
		strSaveData += strData;
//		file.Write(strData, strData.GetLength());

		if(m_strFilter2_2 == "")	strData.Format("0,");	//Filter1_2
		else					strData.Format("%s,",m_strFilter2_2);	//Filter1_2
		strSaveData += strData;
//		file.Write(strData, strData.GetLength()); 

		if(m_strFilter2_3 == "")	strData.Format("0,");	//Filter1_1
		else					strData.Format("%s,",m_strFilter2_3);	//Filter1_1
		strSaveData += strData;
//		file.Write(strData, strData.GetLength());

		if(m_strFilter2_4 == "")	strData.Format("0,");	//Filter1_2
		else					strData.Format("%s,",m_strFilter2_4);	//Filter1_2
		strSaveData += strData;
//		file.Write(strData, strData.GetLength()); 

		strData.Format("%s,",m_strBmsType);	//BMS Type
		strSaveData += strData;
		strData.Format("%s,",m_strBmsSJW);	//BMS SJW
		strSaveData += strData;
		m_ctrlDataRate.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",";
	
		m_ctrlBmsTerminal.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",";

		m_ctrlBmsCrcType.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",\r\n\r\n";

		file.Write(strSaveData, strSaveData.GetLength()); //Command Save

		if(m_nMasterIDType == 0)		//Hex
			strTitle = "ID(Hex),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,안전상한,안전하한,종료상한,종료하한,설정값,수신 시간,분류값 1,분류값 2,분류값 3, &StartBit,&BitCount,&ByteOrder,&DataType,&설정값\r\n";
			//strTitle = Fun_FindMsg("SettingCanFdDlg_SaveCANConfig_msg2","IDD_CAN_FD_SETTING_DLG");//&&
		else							//Dec
			strTitle = "ID(Dec),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,안전상한,안전하한,종료상한,종료하한,설정값,수신 시간,분류값 1,분류값 2,분류값 3, &StartBit,&BitCount,&ByteOrder,&DataType,&설정값\r\n";
			//strTitle = Fun_FindMsg("SettingCanFdDlg_SaveCANConfig_msg3","IDD_CAN_FD_SETTING_DLG");//&&
		file.Write(strTitle, strTitle.GetLength());	//Title 

		for(i=1; i<=m_wndCanGrid.GetRowCount(); i++)
		{
			for(int j = 1; j <=m_wndCanGrid.GetColCount(); j++)
			{
				strData.Format("%s,",m_wndCanGrid.GetValueRowCol(i, j));
				file.Write(strData, strData.GetLength()); 
			}
			strData.Format("\r\n");
			file.Write(strData, strData.GetLength()); //줄바꿈
		}
		strData.Format("\r\n");
		file.Write(strData, strData.GetLength());	//줄바꿈
		
		//End Save Master/////////////////////////////////////////////////////////////////////////
		//Start Save Slave/////////////////////////////////////////////////////////////////////////
		strTitle = "**Slave Configuration**\r\nID(Hex),BaudRate,Ext ID,CAN FD,User Filter Mode,Mask1(Hex),Filter1(Hex),Filter2(Hex),Mask2(Hex),Filter1(Hex),Filter2(Hex),Filter3(Hex),Filter4(Hex),Bms Type,Bms SJW,CAN FD DataRate\r\n";	
		file.Write(strTitle, strTitle.GetLength());	//Title 

		strSaveData.Format("%s,", m_strCanIDSlave);

		m_ctrlBaudRateSlave.GetWindowText(strData);		//ljb 20180326
// 		if(m_strBaudRateSlave == "125 kbps")
// 			strData.Format("%d,", 125);
// 		else if(m_strBaudRateSlave == "250 kbps")
// 			strData.Format("%d,", 250);
// 		else if(m_strBaudRateSlave == "500 kbps")
// 			strData.Format("%d,", 500);
// 		else if(m_strBaudRateSlave == "1 Mbps")
// 			strData.Format("%d,", 1000);

		strSaveData += strData + ",";

		strData.Format("%d,",m_ckExtIDSlave);	
		strSaveData += strData;

		//strData="0,";
		strData.Format("%d,", m_ckCanFD_Slave);	//ljb 20180326
		strSaveData += strData;
		//		file.Write(strData, strData.GetLength());	// CV Value -> ljb 20180326 edit CAN 2.0, CAN FD 구별 인자로 사용

		strData.Format("%d,",m_ckUserFilterSlave);	
		strSaveData += strData;
		
		if(m_strMask1Slave == "")	strData.Format("0,");	//Mask1
		else					strData.Format("%s,",m_strMask1Slave);	//Mask1
		strSaveData += strData;

		if(m_strFilter1_1Slave == "")	strData.Format("0,");	//Filter1_1
		else					strData.Format("%s,",m_strFilter1_1Slave);	//Filter1_1
		strSaveData += strData;

		if(m_strFilter1_2Slave == "")	strData.Format("0,");	//Filter1_2
		else					strData.Format("%s,",m_strFilter1_2Slave);	//Filter1_2
		strSaveData += strData;

		if(m_strMask2Slave == "")	strData.Format("0,");	//Mask2
		else					strData.Format("%s,",m_strMask2Slave);	//Mask2
		strSaveData += strData;

		if(m_strFilter2_1Slave == "")	strData.Format("0,");	//Filter1_1
		else					strData.Format("%s,",m_strFilter2_1Slave);	//Filter1_1
		strSaveData += strData;

		if(m_strFilter2_2Slave == "")	strData.Format("0,");	//Filter1_2
		else					strData.Format("%s,",m_strFilter2_2Slave);	//Filter1_2
		strSaveData += strData;

		if(m_strFilter2_3Slave == "")	strData.Format("0,");	//Filter1_1
		else					strData.Format("%s,",m_strFilter2_3Slave);	//Filter1_1
		strSaveData += strData;

		if(m_strFilter2_4Slave == "")	strData.Format("0,");	//Filter1_2
		else					strData.Format("%s,",m_strFilter2_4Slave);	//Filter1_2
		strSaveData += strData;

		strData.Format("%s,",m_strBmsTypeSlave);	//BMS Type
		strSaveData += strData;
		strData.Format("%s,",m_strBmsSJWSlave);	//BMS SJW
		strSaveData += strData;
		
		m_ctrlDataRateSlave.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",";
		
		m_ctrlBmsTerminalSlave.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",";
		
		m_ctrlBmsCrcTypeSlave.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",\r\n\r\n";
		
		file.Write(strSaveData, strSaveData.GetLength()); //Bms Type
		
		if(m_nMasterIDType == 0)		//Hex
			strTitle = "ID(Hex),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,안전상한,안전하한,종료상한,종료하한,설정값,수신 시간,분류값 1,분류값 2,분류값 3, &StartBit,&BitCount,&ByteOrder,&DataType,&설정값\r\n";
			//strTitle = Fun_FindMsg("SettingCanFdDlg_SaveCANConfig_msg2","IDD_CAN_FD_SETTING_DLG");//&&
		else							//Dec
			strTitle = "ID(Dec),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,안전상한,안전하한,종료상한,종료하한,설정값,수신 시간,분류값 1,분류값 2,분류값 3, &StartBit,&BitCount,&ByteOrder,&DataType,&설정값\r\n";
			//strTitle = Fun_FindMsg("SettingCanFdDlg_SaveCANConfig_msg3","IDD_CAN_FD_SETTING_DLG");//&&

		file.Write(strTitle, strTitle.GetLength());	//Title 

		for(i=1; i<=m_wndCanGridSlave.GetRowCount(); i++)
		{
			for(int j = 1; j <= m_wndCanGridSlave.GetColCount(); j++)
			{
				strData.Format("%s,",m_wndCanGridSlave.GetValueRowCol(i, j));
				file.Write(strData, strData.GetLength()); 
			}
			strData.Format("\r\n");
			file.Write(strData, strData.GetLength()); //줄바꿈
		}
	}
	//End Save Slave/////////////////////////////////////////////////////////////////////////	
	
	
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
	
	file.Close();
	return TRUE;
}

BOOL CSettingCanFdDlg::LoadCANConfig(CString strLoadPath)
{
	CString strData;
	CStdioFile aFile;
	CFileException e;
	int nIndex, nPos = 0;
	int nTemp;

	CString strTitle = "ID(Hex),BaudRate,Ext ID,CV Value,User Filter Mode,Mask1(Hex),Filter1(Hex),Filter2(Hex),Mask2(Hex),Filter1(Hex),Filter2(Hex),Filter3(Hex),Filter4(Hex)";
	CString strTitle3 = "ID(Hex),BaudRate,Ext ID,CV Value,User Filter Mode,Mask1(Hex),Filter1(Hex),Filter2(Hex),Mask2(Hex),Filter1(Hex),Filter2(Hex),Filter3(Hex),Filter4(Hex),Bms Type,Bms SJW";
	CString strTitle4 = "ID(Hex),BaudRate,Ext ID,CAN FD,User Filter Mode,Mask1(Hex),Filter1(Hex),Filter2(Hex),Mask2(Hex),Filter1(Hex),Filter2(Hex),Filter3(Hex),Filter4(Hex),Bms Type,Bms SJW,CAN FD DataRate";

	if( !aFile.Open( strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   //AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		   AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_LoadCANConfig_msg1","IDD_CAN_FD_SETTING_DLG"));//&& 
		   return FALSE;
	}

	CStringList		strDataList;
	CStringList		strItemList;
	aFile.ReadString(strData);	//Version Check

	if(strData.Find("Data") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data
		
		
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);		//TRANS인지 RECEIVE인지 Check
			m_strType = strTemp;

			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);				//1: 1006 Ver 추후 버전 업그레이드에 따라 추가
			
			if(strTemp.IsEmpty()) 
				m_nProtocolVer = 0;
			else
				m_nProtocolVer = atoi(strTemp);

			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strDescript = strTemp;
		}

		EnableMaskFilter(m_ckUserFilter, PS_CAN_TYPE_MASTER);
			
		// Data Info에서 Type에 따라 Load할 Data를 결정	
		if(m_strType != "RECEIVE")
		{
			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_LoadCANConfig_msg2","IDD_CAN_FD_SETTING_DLG"));//&&
			m_nProtocolVer = 0;
			return FALSE;
		}

		nPos = 0;
		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Master or Slave
	}	

	if(strData.Find("Master") > 0)		// Ver 0x1006 이전
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.

		//구버전을 읽어올 경우 TRANS인지 RECEIVE인지 Check
// 		if(strData != strTitle)
// 		{
// 			AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
// 			m_nProtocolVer = 0;
// 			return FALSE;
// 		}
//		if (m_nProtocolVer < 3 && strData != strTitle)
//		{
//			AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
//			m_nProtocolVer = 0;
//			return FALSE;
//		}
//		else if (m_nProtocolVer == 3 && strData != strTitle3)
//		{
//			AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
//			m_nProtocolVer = 0;
//			return FALSE;
//		}
		
		aFile.ReadString(strData);	//Data

		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strCanID = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "125" || strTemp == "125K" || strTemp == "125KBPS"){
				m_ctrlBaudRate.SetCurSel(0);}
			else if(strTemp == "250" || strTemp == "250K" || strTemp == "250KBPS"){
				m_ctrlBaudRate.SetCurSel(1);}
			else if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
				m_ctrlBaudRate.SetCurSel(2);}
			else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){ //yulee 20181119
				m_ctrlBaudRate.SetCurSel(3);}
			else {
				m_ctrlBaudRate.SetCurSel(0);
			}
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckExtID = atoi(strTemp);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //

			if(bUseCANFD)
				m_ckCanFD = atoi(strTemp);		//ljb 20180326 CAN2.0,  CAN FD
			else
				m_ckCanFD = 0;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			//if (m_ckCanFD) 
			//{
			//	GetDlgItem(IDC_COMBO_CAN_DATARATE)->EnableWindow(TRUE);
			//	GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(FALSE);
			//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->EnableWindow(TRUE);
			//	GetDlgItem(IDC_COMBO_CAN_CRC_M)->EnableWindow(TRUE);
			//
			//	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			//	if(bUseCANFD)
			//	{
			//
			//	}
			//	else
			//	{
			//		GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
			//	}
			//}
			//else
			//{
			//	GetDlgItem(IDC_COMBO_CAN_DATARATE)->EnableWindow(FALSE);
			//	GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(TRUE);
			//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->EnableWindow(TRUE); //20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
			//	GetDlgItem(IDC_COMBO_CAN_CRC_M)->EnableWindow(FALSE); 
			//
			//	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			//	if(bUseCANFD)
			//	{
			//		
			//	}
			//	else
			//	{
			//		GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
			//	}
			//}
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckUserFilter = atoi(strTemp);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strMask1 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter1_1 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter1_2 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strMask2 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_1 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_2 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_3 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_4 = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 201008 BMS Type add
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strBmsType = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
		
			//ljb 201009 BMS SJW add
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strBmsSJW = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 20180326 CAN FD DataRate
			strTemp="";
			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			
			bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
				if(bUseCANFD)
				{
					if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
						m_ctrlDataRate.SetCurSel(0);}
					else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
						m_ctrlDataRate.SetCurSel(1);}
					else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
						m_ctrlDataRate.SetCurSel(2);}
					else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
						m_ctrlDataRate.SetCurSel(3);}
					else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
						m_ctrlDataRate.SetCurSel(4);}
					else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
						m_ctrlDataRate.SetCurSel(5);}
					else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
						m_ctrlDataRate.SetCurSel(6);}
					else {
						//m_ctrlBaudRate.SetCurSel(0);
						m_ctrlDataRate.SetCurSel(0); //lyj 20200218 can fd receive 불러오기 시 BaudRate 0으로 셋되는 현상.
					}
				}
				else
				{

				}
// 			if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
// 				m_ctrlDataRate.SetCurSel(0);}
// 			else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
// 				m_ctrlDataRate.SetCurSel(1);}
// 			else if(strTemp == "1" || strTemp == "1M" || strTemp == "1MBPS"){
// 				m_ctrlDataRate.SetCurSel(2);}
// 			else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
// 				m_ctrlDataRate.SetCurSel(3);}
// 			else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
// 				m_ctrlDataRate.SetCurSel(4);}
// 			else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
// 				m_ctrlDataRate.SetCurSel(5);}
// 			else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
// 				m_ctrlDataRate.SetCurSel(6);}
// 			else {
// 				m_ctrlBaudRate.SetCurSel(0);
// 				}
						nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 20180327 BMS Terminal
			strTemp="";
			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "OPEN"){
				m_ctrlBmsTerminal.SetCurSel(0);}
			else if(strTemp == "120OHM"){
				m_ctrlBmsTerminal.SetCurSel(1);}
			else
				m_ctrlBmsTerminal.SetCurSel(1);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 20180327 BMS CRC
			strTemp="";
			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "NON"){
				m_ctrlBmsCrcType.SetCurSel(0);}
			else if(strTemp == "CRC"){
				m_ctrlBmsCrcType.SetCurSel(1);}
			else
				m_ctrlBmsCrcType.SetCurSel(1);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);		
		}

		if (m_nProtocolVer < 3 && strData != strTitle)
		{
			m_strBmsType = "0";
			m_strBmsSJW = "0";
		}

		EnableMaskFilter(m_ckUserFilter, PS_CAN_TYPE_MASTER);

		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Title
		
		nPos = strData.Find("Name", 0);
		if(nPos > 4)	//ID가 Hex또는 Decimal인지 구분.. 이 방법밖에 없나??
		{
			CString strType;
			nPos = strData.Find("(", 0);
			strType = strData.Mid(nPos+1, strData.Find(")", nPos+1)-(nPos+1));
			strType.MakeUpper();
			if(strType == "HEX")
				m_nMasterIDType = 0;
			else
				m_nMasterIDType = 1;
		}
		else		//(HEX), (DEC) 가 없으면 무조건 DEC
			m_nMasterIDType = 1;


		UpdateData(FALSE);
		
		nPos = 0;

		while(aFile.ReadString(strData))	//Data
		{
			if(strData == "" || strData.Left(10) == ",,,,,,,,,,")
				break;
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			
			int nCol = 1;
			
			m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, nCol);
			int nRow = m_wndCanGrid.GetRowCount();
			while(p0 != -1)
			{			
				CString strCanData = strData.Mid(p1, p0 - p1);
				if (strCanData == "") 
				{
					p0 = -1;
					break;
				}
				if(m_nProtocolVer == 0)
				{
					if(nCol != CAN_COL_OFFSET)
					{
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else if (m_nProtocolVer == 1)
				{
					if(nCol != CAN_COL_DIVISION1)
					{
						nTemp = atoi(strCanData);
						if (nTemp  == ID_CAN_CELL_VTG_ALL) strCanData = "CELL ALL";
						else if (nTemp  == ID_CAN_CELL_VTG_HIGH) strCanData = "CELL HIGH";
						else if (nTemp  == ID_CAN_CELL_VTG_LOW) strCanData = "CELL LOW";
						else if (nTemp  == ID_CAN_TEMPE_ALL) strCanData = "TEMP ALL";
						else if (nTemp  == ID_CAN_TEMPE_HIGH) strCanData = "TEMP HIGH";
						else if (nTemp  == ID_CAN_TEMPE_LOW) strCanData = "TEMP LOW";
						else if (nTemp  == ID_CAN_BMS_FLT) strCanData = "BMS Flt";
						else if (nTemp  == ID_CAN_SOC_ALL) strCanData = "SOC ALL";
						else if (nTemp  == ID_CAN_SOC_HIGH) strCanData = "SOC HIGH";
						else if (nTemp  == ID_CAN_SOC_LOW) strCanData = "SOC LOW";
						else if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
						else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
						else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
						else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
						else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
						else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
						else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
						TRACE("%s \n",strCanData);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else if (m_nProtocolVer == 2)
				{
					if(nCol == CAN_COL_SENT_TIME)
					{
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), "0");
					}
					else
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
				}
				else if (m_nProtocolVer == 3 || m_nProtocolVer == 4)
				{
					//2014.08.12 캔리시브 로드시 Enable 설정.
					if((nCol == CAN_COL_DATATYPE && strCanData == "Hex") 
						||(nCol == CAN_COL_DATATYPE && strCanData == "String") ){
						SetGridCanDivisionEnable(0,nRow,FALSE); //2014.08.12
					}					
					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);

					

				}
				p1 = p0 +1;
				p0 = strData.Find(",", p1);
			}
		}	

	}

	aFile.ReadString(strData);	//Master or Slave
	
	if(strData.Find("Slave") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data
		
		
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strCanIDSlave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "125" || strTemp == "125K" || strTemp == "125KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(0);}
			else if(strTemp == "250" || strTemp == "250K" || strTemp == "250KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(1);}
			else if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(2);}
			else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
				m_ctrlBaudRateSlave.SetCurSel(3);}
			else {
				m_ctrlBaudRateSlave.SetCurSel(0);
			}
// 			if(strTemp == "125")
// 				m_strBaudRateSlave.Format("%s", "125 kbps");
// 			else if(strTemp == "250")
// 				m_strBaudRateSlave.Format("%s", "250 kbps");
// 			else if(strTemp == "500")
// 				m_strBaudRateSlave.Format("%s", "500 kbps");
// 			else
// 				m_strBaudRateSlave.Format("%s", "1 Mbps");

			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckExtIDSlave = atoi(strTemp);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			if(bUseCANFD)
				m_ckCanFD_Slave = atoi(strTemp);			//ljb 20180326 CAN FD
			else
				m_ckCanFD_Slave = 0;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			//if (m_ckCanFD) 
			//{
			//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->EnableWindow(TRUE);
			//	GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(FALSE);
			//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->EnableWindow(TRUE);
			//	GetDlgItem(IDC_COMBO_CAN_CRC_S)->EnableWindow(TRUE);
			//	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			//	if(bUseCANFD)
			//	{
			//	}
			//	else
			//	{
			//		GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
			//	}
			//}
			//else
			//{
			//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->EnableWindow(FALSE);
			//	GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(TRUE);
			//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->EnableWindow(TRUE); //20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
			//	GetDlgItem(IDC_COMBO_CAN_CRC_S)->EnableWindow(FALSE); 
			//	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			//	if(bUseCANFD)
			//	{
			//	}
			//	else
			//	{
			//		GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			//		GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
			//	}
			//}
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckUserFilterSlave = atoi(strTemp);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strMask1Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter1_1Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter1_2Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strMask2Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_1Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_2Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_3Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strFilter2_4Slave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 201008 BMS Type add
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strBmsTypeSlave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 201008 BMS SJW add
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strBmsSJWSlave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 20180326 CAN FD DataRate
			strTemp="";
			strTemp = strData.Mid(nPos, nIndex - nPos);
			// 			if(strTemp == "500" || strTemp == "500 k" || strTemp == "500 K" || strTemp == "500 kbps || strTemp == "500 Kbps")
			// 				m_ctrlDataRate.SetCurSel(0);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();

			bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			if(bUseCANFD)
			{
				if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
					m_ctrlDataRateSlave.SetCurSel(0);}
				else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
					m_ctrlDataRateSlave.SetCurSel(1);}
				else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
					m_ctrlDataRateSlave.SetCurSel(2);}
				else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
					m_ctrlDataRateSlave.SetCurSel(3);}
				else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
					m_ctrlDataRateSlave.SetCurSel(4);}
				else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
					m_ctrlDataRateSlave.SetCurSel(5);}
				else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
					m_ctrlDataRateSlave.SetCurSel(6);}
				else {
					m_ctrlDataRateSlave.SetCurSel(0);
				}
			}
			else
			{

			}
// 			if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
// 				m_ctrlDataRateSlave.SetCurSel(0);}
// 			else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
// 				m_ctrlDataRateSlave.SetCurSel(1);}
// 			else if(strTemp == "1" || strTemp == "1M" || strTemp == "1MBPS"){
// 				m_ctrlDataRateSlave.SetCurSel(2);}
// 			else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
// 				m_ctrlDataRateSlave.SetCurSel(3);}
// 			else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
// 				m_ctrlDataRateSlave.SetCurSel(4);}
// 			else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
// 				m_ctrlDataRateSlave.SetCurSel(5);}
// 			else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
// 				m_ctrlDataRateSlave.SetCurSel(6);}
// 			else {
// 				m_ctrlDataRateSlave.SetCurSel(0);
// 			}
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			//ljb 20180327 BMS Terminal
			strTemp="";
			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "OPEN"){
				m_ctrlBmsTerminalSlave.SetCurSel(0);}
			else if(strTemp == "120OHM"){
				m_ctrlBmsTerminalSlave.SetCurSel(1);}
			else
				m_ctrlBmsTerminalSlave.SetCurSel(1);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			//ljb 20180327 BMS CRC
			strTemp="";
			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "NON"){
				m_ctrlBmsCrcTypeSlave.SetCurSel(0);}
			else if(strTemp == "CRC"){
				m_ctrlBmsCrcTypeSlave.SetCurSel(1);}
			else
				m_ctrlBmsCrcTypeSlave.SetCurSel(1);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
		}

		if (m_nProtocolVer < 3 && strData != strTitle)
		{
			m_strBmsTypeSlave = "0";
			m_strBmsSJWSlave = "0";
		}

		EnableMaskFilter(m_ckUserFilterSlave, PS_CAN_TYPE_SLAVE);
		UpdateData(FALSE);

		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		
		nPos = 0;

		while(aFile.ReadString(strData))	//Data
		{

			//2014.08.22 캔 리시브 슬레이브를 사용하지않을시 그리드 적용하지않음.
			BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);
			if(!isSlave){
				break;
			}
			
			if(strData == "" || strData.Left(10) == ",,,,,,,,,,")
				break;
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			
			int nCol = 1;

			m_wndCanGridSlave.InsertRows(m_wndCanGridSlave.GetRowCount()+1, nCol);
			int nRow = m_wndCanGridSlave.GetRowCount();
			while(p0 != -1)
			{			
				CString strCanData = strData.Mid(p1, p0 - p1);
				if (strCanData == "")
				{
					p0 = -1;
					break;
				}
				if(m_nProtocolVer == 0)
				{
					if(nCol != CAN_COL_OFFSET)
					{
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else if (m_nProtocolVer == 1)
				{
					if(nCol != CAN_COL_DIVISION1)
					{
						nTemp = atoi(strCanData);
						if (nTemp  == ID_CAN_CELL_VTG_ALL) strCanData = "CELL ALL";
						else if (nTemp  == ID_CAN_CELL_VTG_HIGH) strCanData = "CELL HIGH";
						else if (nTemp  == ID_CAN_CELL_VTG_LOW) strCanData = "CELL LOW";
						else if (nTemp  == ID_CAN_TEMPE_ALL) strCanData = "TEMP ALL";
						else if (nTemp  == ID_CAN_TEMPE_HIGH) strCanData = "TEMP HIGH";
						else if (nTemp  == ID_CAN_TEMPE_LOW) strCanData = "TEMP LOW";
						else if (nTemp  == ID_CAN_BMS_FLT) strCanData = "BMS Flt";
						else if (nTemp  == ID_CAN_SOC_ALL) strCanData = "SOC ALL";
						else if (nTemp  == ID_CAN_SOC_HIGH) strCanData = "SOC HIGH";
						else if (nTemp  == ID_CAN_SOC_LOW) strCanData = "SOC LOW";
						else if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
						else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
						else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
						else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
						else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
						else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
						else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
						
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else
				{
					//2014.08.12 캔리시브 로드시 Enable 설정.
					if((nCol == CAN_COL_DATATYPE && strCanData == "Hex") 
						||(nCol == CAN_COL_DATATYPE && strCanData == "String") ){
						SetGridCanDivisionEnable(1,nRow,FALSE); //2014.08.12
					}

					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
				}

				p1 = p0 +1;
				p0 = strData.Find(",", p1);
			}				

		}	
	}//END if(strData.Find("Slave") > 0) 	

	aFile.Close();

	m_nProtocolVer = 0;

	return TRUE;
	
}

BOOL CSettingCanFdDlg::LoadCANDbcConfig(CString strLoadPath)
{
	CString strData;
	CStdioFile aFile;
	CFileException e;	
	CString strDbcId, strData_next;
	CString strDbcName, strDbcStartBit, strDbcBitCount, strDbcByteOrder, strDbcDataType, strDbcFactor, strDbcOffset;
	CString strDbcName_next, strDbcStartBit_next, strDbcBitCount_next, strDbcByteOrder_next, strDbcDataType_next, strDbcFactor_next, strDbcOffset_next;
	
// 	S_CAN_DBC_DATA sCanData;
// 
// 	CStringData sData;
// 	sData.
	
	if( !aFile.Open(strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		//AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_LoadCANConfig_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
		return FALSE;
	}
	//////////////////////////////////////////////////////////////데이터 파싱하는 부분/////////////////////////////////////////////////////////
	while(aFile.ReadString(strData))	//Data
	{		
		TRACE("%s\n", strData);

		int nIndex, nPos = 0;

 		nIndex = strData.Find("BO_ ", nPos);
		
		while(nIndex != -1)
		{
			strDbcId = strData.Mid(nPos+4,4);	
	
			aFile.ReadString(strData);

			int C0, C1 = 0;
			
			C0 = strData.Find("SG_ ",C1);

			while (C0 != -1)
			{
				strData_next = strData.Mid(C1+5);
				
				AfxExtractSubString(strDbcName,strData_next,0,':');
				AfxExtractSubString(strDbcName_next,strData_next,1,':');
								
				AfxExtractSubString(strDbcStartBit,strDbcName_next,0,'|');
				AfxExtractSubString(strDbcStartBit_next,strDbcName_next,1,'|');

				AfxExtractSubString(strDbcBitCount,strDbcStartBit_next,0,'@');
				AfxExtractSubString(strDbcBitCount_next, strDbcStartBit_next,1,'@');
				
				if (strDbcBitCount_next.Find('+')==TRUE)
				{
					AfxExtractSubString(strDbcByteOrder,strDbcBitCount_next,0,'+');
					AfxExtractSubString(strDbcByteOrder_next,strDbcBitCount_next,1,'+');
				}else
				{
					AfxExtractSubString(strDbcByteOrder,strDbcBitCount_next,0,'-');
					AfxExtractSubString(strDbcByteOrder_next,strDbcBitCount_next,1,'-');
				}
				
				strDbcStartBit.Replace((" "),NULL);
				strDbcBitCount_next.Replace((" "),NULL);
				strDbcBitCount_next.Delete(0,1);

				AfxExtractSubString(strDbcDataType,strDbcBitCount_next,0,'(');
				AfxExtractSubString(strDbcDataType_next,strDbcBitCount_next,1,'(');

				AfxExtractSubString(strDbcFactor,strDbcDataType_next,0,',');
				AfxExtractSubString(strDbcFactor_next,strDbcDataType_next,1,',');

				AfxExtractSubString(strDbcOffset,strDbcFactor_next,0,')');
				AfxExtractSubString(strDbcOffset_next,strDbcFactor_next,1,')');

				int nCol = 1;
				int nRow = m_wndCanGrid.GetRowCount()+1;

				m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, nCol);
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_ID), strDbcId);
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_NAME), strDbcName);
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_STARTBIT), strDbcStartBit);
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BITCOUNT), strDbcBitCount);
				
				if (strDbcByteOrder.Find('0')==TRUE)
				{
					m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER), "Intel");
				}else
				{
					m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER), "Motorola");
				}

				if(strDbcDataType.Find('-')==TRUE)
				{
					m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE), "Unsigned");
				}else
				{
					m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE), "Signed");
				}

				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_FACTOR), strDbcFactor);
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_OFFSET), strDbcOffset);

				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_FAULT_UPPER), "0.00000");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_FAULT_LOWER), "0.00000");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_END_UPPER), "0.00000");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_END_LOWER), "0.00000");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DEFAULT), "0.000000");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_SENT_TIME), "0");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION1), "None[0]");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION2), "None[0]");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION3), "None[0]");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_STARTBIT2), "0");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BITCOUNT2), "0");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER2), "Intel");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE2), "Unsigned");
				m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DEFAULT2), "0.000000");				
								
				aFile.ReadString(strData);
				C0 = strData.Find("SG_ ",C1);

			}

			nPos = nIndex + 5;
			nIndex = strData.Find("BO_ ", nPos);
		}	
	}
	
	aFile.Close();

	return TRUE;
	
}

BOOL CSettingCanFdDlg::LoadCANDbcConfig2(CString strLoadPath)
{
	int nIDtype = -1; //0 -> hex 1 -> DEC 
	CString strData, strDbcId, strSignal;
	CStdioFile aFile;
	CFileException e;	
	
	CStringArray arryStrData;
	CUIntArray   arryCanID;
	UINT		 uiCanID;

	if( !aFile.Open(strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		//AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_LoadCANConfig_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
		return FALSE;
	}
	//////////////////////////////////////////////////////////////데이터 파싱하는 부분/////////////////////////////////////////////////////////
	int nIndex = 0, nPos = 0;
	int nIndex2 = 0, nPos2 = 0;
	while(aFile.ReadString(strData))	//Data
	{		
		//TRACE("%s\n", strData);
 		nPos = 0;
		nIndex = strData.Find("BO_ ", nPos);
		if (nIndex > 2)
		{
			continue;
		}
		
		while(nIndex != -1)
		{
			strData = strData.Right(strData.GetLength()-4);
			nIndex = strData.Find(" ", nPos);

			strDbcId = strData.Left(nIndex);
			strDbcId.Replace(" ","");	//공백제거
			//uiCanID = atol(strDbcId);
			uiCanID = strtoul(strDbcId,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.
			uiCanID &= 0x1fffffff; //ksj 20200325 : Can Extended ID 29bit 까지만 사용하도록 마스크 필터링.
			arryCanID.Add(uiCanID);
			//TRACE("CAN ID(dec) %d\n", uiCanID);
			

			aFile.ReadString(strData);
			nPos2 = 0;
			nIndex2= strData.Find("SG_ ",nPos2);
			while (nIndex2 != -1)
			{
				//TRACE("%s\n", strData);
				strData = strData.Right(strData.GetLength()-(nIndex2+4));
				nIndex = strData.Find("]", nPos);
				strSignal = strData.Left(nIndex+1);
				strSignal.Replace(" ","");	//공백제거
				//strData.Format("%d,%s",uiCanID,strSignal);
				strData.Format("%u,%s",uiCanID,strSignal); //ksj 20200323

				arryStrData.Add(strData);
				//TRACE("%s\n", strData);
								
				aFile.ReadString(strData);
				nIndex2 = strData.Find("SG_ ",nPos2);
			}

			nIndex = strData.Find("BO_ ", nPos);
		}	
	}
	
	aFile.Close();
	
	int nTotCount = arryStrData.GetSize();
	S_CAN_DBC_DATA *sCanData;
	sCanData = new S_CAN_DBC_DATA[nTotCount];
	

	int i,j;
	for( int i=0; i<arryStrData.GetSize();i++)
	{
		strData.Format("%s", arryStrData.GetAt(i));
		TRACE("%s\n", strData);
		ZeroMemory(&sCanData[i], sizeof(S_CAN_DBC_DATA));
		Fun_ConvertData(strData, &sCanData[i]);
		if(i == 0)
		{
			if(strData.Find("x") > -1)	
				nIDtype = 0;
			else 
				nIDtype = 1;
		}
	}
	
//	int nStartBit=0;
	int bitCount;
	for( int i=0; i<arryCanID.GetSize();i++)
	{
		for (bitCount=0; bitCount < 65; bitCount++)
		{
			for (j=0; j<arryStrData.GetSize();j++)
			{
				//if (sCanData[j].can_id == arryCanID.GetAt(i))
				if (sCanData[j].uiCan_id == arryCanID.GetAt(i)) //ksj 20200323
				{
					if (bitCount == sCanData[j].startBit)
					{
						//strData.Format("id=%d, start= %d\n",sCanData[j].can_id,sCanData[j].startBit);
						strData.Format("id=%u, start= %d\n",sCanData[j].uiCan_id,sCanData[j].startBit); //ksj 20200323
						TRACE("%s \n", strData);

						//grid에 추가 하기
						int nCol = 1;
						int nRow = m_wndCanGrid.GetRowCount()+1;
						
						m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, nCol);
						
						//20180517 yulee 현재 설정 가져오기 
						//현재 설정 가져오기 
						//문서 첫 번째 데이터 확인으로 
						//0 -> hex 표현 1 -> dec 표현 
						if(m_nMasterIDType != nIDtype)
						{
							if(nIDtype == 0)
							{
								CheckRadioButton(IDC_RADIO3, IDC_RADIO4, IDC_RADIO3);
								m_nMasterIDType = nIDtype;
							}
							else
							{
								CheckRadioButton(IDC_RADIO3, IDC_RADIO4, IDC_RADIO4);
								m_nMasterIDType = nIDtype;
							}
						}

						//strDbcId.Format("%d",sCanData[j].can_id);
						strDbcId.Format("%u",sCanData[j].uiCan_id); //ksj 20200323
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_ID), strDbcId);

						strData.Format("%s",sCanData[j].signal_name);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_NAME), strData);
						strData.Format("%d",sCanData[j].startBit);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_STARTBIT), strData);
						strData.Format("%d",sCanData[j].length);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BITCOUNT), strData);
							
						if (sCanData[j].byteOrder == 0)
							m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER), "Motorola");
						else
							m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER), "Intel");
						
						if (sCanData[j].valueType == 0)
							m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE), "Signed");
						else
							m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE), "Unsigned");
						
						strData.Format("%f",sCanData[j].factor);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_FACTOR), strData);
						strData.Format("%f",sCanData[j].offset);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_OFFSET), strData);
						
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_FAULT_UPPER), "0.00000");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_FAULT_LOWER), "0.00000");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_END_UPPER), "0.00000");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_END_LOWER), "0.00000");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DEFAULT), "0.000000");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_SENT_TIME), "0");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION1), "None[0]");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION2), "None[0]");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION3), "None[0]");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_STARTBIT2), "0");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BITCOUNT2), "0");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER2), "Intel");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE2), "Unsigned");
						m_wndCanGrid.SetValueRange(CGXRange(nRow, DBC_COL_DEFAULT2), "0.000000");				
					}
					
				}
				else
					continue;
			}

		}
	}
	
// 	if(m_nMasterIDType != nIDtype)
// 	{
// 			ReDrawMasterGrid();
// 			m_nMasterIDType =	nIDtype;
// 	}
	return TRUE;
	
}

BOOL CSettingCanFdDlg::LoadCANDbcConfig2Slave(CString strLoadPath)
{
	int nIDtype = -1; //0 -> hex 1 -> DEC
	CString strData, strDbcId, strSignal;
	CStdioFile aFile;
	CFileException e;	
	
	CStringArray arryStrData;
	CUIntArray   arryCanID;
	UINT		 uiCanID;

	if( !aFile.Open(strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		//AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_LoadCANConfig_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
		return FALSE;
	}
	//////////////////////////////////////////////////////////////데이터 파싱하는 부분/////////////////////////////////////////////////////////
	int nIndex = 0, nPos = 0;
	int nIndex2 = 0, nPos2 = 0;
	while(aFile.ReadString(strData))	//Data
	{		
		//TRACE("%s\n", strData);
 		nPos = 0;
		nIndex = strData.Find("BO_ ", nPos);
		if (nIndex > 2)
		{
			continue;
		}
		
		while(nIndex != -1)
		{
			strData = strData.Right(strData.GetLength()-4);
			nIndex = strData.Find(" ", nPos);

			strDbcId = strData.Left(nIndex);
			strDbcId.Replace(" ","");	//공백제거
			//uiCanID = atol(strDbcId);
			uiCanID = strtoul(strDbcId,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.
			uiCanID &= 0x1fffffff; //ksj 20200325 : Can Extended ID 29bit 까지만 사용하도록 마스크 필터링.
			arryCanID.Add(uiCanID);
			//TRACE("CAN ID(dec) %d\n", uiCanID);
			

			aFile.ReadString(strData);
			nPos2 = 0;
			nIndex2= strData.Find("SG_ ",nPos2);
			while (nIndex2 != -1)
			{
				//TRACE("%s\n", strData);
				strData = strData.Right(strData.GetLength()-(nIndex2+4));
				nIndex = strData.Find("]", nPos);
				strSignal = strData.Left(nIndex+1);
				strSignal.Replace(" ","");	//공백제거
				strData.Format("%d,%s",uiCanID,strSignal);
				arryStrData.Add(strData);
				//TRACE("%s\n", strData);
								
				aFile.ReadString(strData);
				nIndex2 = strData.Find("SG_ ",nPos2);
			}

			nIndex = strData.Find("BO_ ", nPos);
		}	
	}
	
	aFile.Close();
	
	int nTotCount = arryStrData.GetSize();
	S_CAN_DBC_DATA *sCanData;
	sCanData = new S_CAN_DBC_DATA[nTotCount];
	

	int i,j;
	for( int i=0; i<arryStrData.GetSize();i++)
	{
		strData.Format("%s", arryStrData.GetAt(i));
		TRACE("%s\n", strData);
		ZeroMemory(&sCanData[i], sizeof(S_CAN_DBC_DATA));
		Fun_ConvertData(strData, &sCanData[i]);
		if(i == 0)
		{
			if(strData.Find("x") > -1)	
				nIDtype = 0;
			else 
				nIDtype = 1;
		}
	}
	
//	int nStartBit=0;
	int bitCount;
	for( int i=0; i<arryCanID.GetSize();i++)
	{
		for (bitCount=0; bitCount < 65; bitCount++)
		{
			for (j=0; j<arryStrData.GetSize();j++)
			{
				//if (sCanData[j].can_id == arryCanID.GetAt(i))
				if (sCanData[j].uiCan_id == arryCanID.GetAt(i)) //ksj 20200323
				{
					if (bitCount == sCanData[j].startBit)
					{
						//strData.Format("id=%d, start= %d\n",sCanData[j].can_id,sCanData[j].startBit);
						strData.Format("id=%u, start= %d\n",sCanData[j].uiCan_id,sCanData[j].startBit); //ksj 20200323
						TRACE("%s \n", strData);

						//grid에 추가 하기
						int nCol = 1;
						int nRow = m_wndCanGridSlave.GetRowCount()+1;
						
						m_wndCanGridSlave.InsertRows(m_wndCanGridSlave.GetRowCount()+1, nCol);
						//20180517 yulee 현재 설정 가져오기 
						//현재 설정 가져오기 
						//문서 첫 번째 데이터 확인으로 
						//0 -> hex 표현 1 -> dec 표현 
						if(m_nMasterIDType != nIDtype)
						{
							if(nIDtype == 0)
							{
								CheckRadioButton(IDC_RADIO3, IDC_RADIO4, IDC_RADIO3);
								m_nMasterIDType = nIDtype;
							}
							else
							{
								CheckRadioButton(IDC_RADIO3, IDC_RADIO4, IDC_RADIO4);
								m_nMasterIDType = nIDtype;
							}
						}

						//strDbcId.Format("%d",sCanData[j].can_id);
						strDbcId.Format("%u",sCanData[j].uiCan_id); //ksj 20200323
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_ID), strDbcId);

						strData.Format("%s",sCanData[j].signal_name);
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_NAME), strData);
						strData.Format("%d",sCanData[j].startBit);
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_STARTBIT), strData);
						strData.Format("%d",sCanData[j].length);
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_BITCOUNT), strData);
							
						if (sCanData[j].byteOrder == 0)
							m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER), "Motorola");
						else
							m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER), "Intel");
						
						if (sCanData[j].valueType == 0)
							m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE), "Signed");
						else
							m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE), "Unsigned");
						
						strData.Format("%f",sCanData[j].factor);
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_FACTOR), strData);
						strData.Format("%f",sCanData[j].offset);
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_OFFSET), strData);
						
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_FAULT_UPPER), "0.00000");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_FAULT_LOWER), "0.00000");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_END_UPPER), "0.00000");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_END_LOWER), "0.00000");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DEFAULT), "0.000000");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_SENT_TIME), "0");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION1), "None[0]");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION2), "None[0]");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DIVISION3), "None[0]");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_STARTBIT2), "0");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_BITCOUNT2), "0");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_BYTE_ORDER2), "Intel");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DATATYPE2), "Unsigned");
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, DBC_COL_DEFAULT2), "0.000000");				
					}
					
				}
				else
					continue;
			}

		}
	}
	return TRUE;
	
}

void CSettingCanFdDlg::OnCheckUserFilter() 
{
	UpdateData();
	if(m_ckUserFilter == TRUE)
		m_strCanID = "FFFFFFFE";
	else
		m_strCanID = "FFFFFFFF";
	
	EnableMaskFilter(m_ckUserFilter, PS_CAN_TYPE_MASTER);
	IsChangeFlag(TRUE);

	UpdateData(FALSE);	
}

void CSettingCanFdDlg::InitGridSlave()
{
	m_wndCanGridSlave.SubclassDlgItem(IDC_CAN_GRID_SLAVE, this);
	m_wndCanGridSlave.Initialize();

	CRect rect;
	m_wndCanGridSlave.GetParam()->EnableUndo(FALSE);
	//2014.08.21 캔 리시브 슬레이브 사용하지않을시 기본1행 삽입 하지않는다.
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);	
	if(isSlave == TRUE){
		//m_wndCanGridSlave.SetRowCount(1);
		m_wndCanGridSlave.SetRowCount(0); //ksj 20210305
	}
	m_wndCanGridSlave.SetRowHeight(0,0,50); //lyj 20190704
	//m_wndCanGridSlave.SetRowCount(1);
	m_wndCanGridSlave.SetColCount(CAN_COL_COUNT);
	m_wndCanGridSlave.GetClientRect(&rect);
	m_wndCanGridSlave.SetColWidth(CAN_COL_NO	,		CAN_COL_NO			, 30);
	m_wndCanGridSlave.SetColWidth(CAN_COL_ID		,	CAN_COL_ID			, 60);
//	m_wndCanGridSlave.SetColWidth(CAN_COL_NAME		,	CAN_COL_NAME		, rect.Width() - 690);
	m_wndCanGridSlave.SetColWidth(CAN_COL_NAME		,	CAN_COL_NAME		, 120);
	m_wndCanGridSlave.SetColWidth(CAN_COL_STARTBIT	,	CAN_COL_STARTBIT	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BITCOUNT	,	CAN_COL_BITCOUNT	, 60);
	//m_wndCanGridSlave.SetColWidth(CAN_COL_BYTE_ORDER ,	CAN_COL_BYTE_ORDER	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BYTE_ORDER ,	CAN_COL_BYTE_ORDER	, 62); //lyj 20200317 컬럼사이즈 조정
	m_wndCanGridSlave.SetColWidth(CAN_COL_DATATYPE	,	CAN_COL_DATATYPE	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_FACTOR	,	CAN_COL_FACTOR		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_OFFSET	,	CAN_COL_OFFSET		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_FAULT_UPPER,	CAN_COL_FAULT_UPPER	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_FAULT_LOWER,	CAN_COL_FAULT_LOWER	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_END_UPPER	,	CAN_COL_END_UPPER	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_END_LOWER	,	CAN_COL_END_LOWER	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DEFAULT		, CAN_COL_DEFAULT		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_SENT_TIME		, CAN_COL_SENT_TIME		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION1		, CAN_COL_DIVISION1		, 150);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION2		, CAN_COL_DIVISION2		, 150);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION3		, CAN_COL_DIVISION3		, 150);

	//**************ljb 20131213 add 동일아이디 구분 위해 추가 ********************************
	m_wndCanGridSlave.SetColWidth(CAN_COL_STARTBIT2		, CAN_COL_STARTBIT2		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BITCOUNT2		, CAN_COL_BITCOUNT2		, 80);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BYTE_ORDER2	, CAN_COL_BYTE_ORDER2	, 80);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DATATYPE2		, CAN_COL_DATATYPE2		, 80);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DEFAULT2		, CAN_COL_DEFAULT2		, 100);
	//******************************************************************************************

	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_NO			),	CGXStyle().SetValue("No"));
	if(m_nMasterIDType == 1)
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	else
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
	//m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_NAME			),	CGXStyle().SetValue("Name"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT		),	CGXStyle().SetValue("StartBit"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT		),	CGXStyle().SetValue("BitCount"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER	),	CGXStyle().SetValue("ByteOrder"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE		),	CGXStyle().SetValue("DataType"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_FACTOR		),	CGXStyle().SetValue("Factor"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_OFFSET		),	CGXStyle().SetValue("Offset"));
	/*
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_FAULT_UPPER	),	CGXStyle().SetValue("안전상한"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_FAULT_LOWER	),	CGXStyle().SetValue("안전하한"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_END_UPPER	),	CGXStyle().SetValue("종료상한"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_END_LOWER	),	CGXStyle().SetValue("종료하한"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue("설정값"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_SENT_TIME		),	CGXStyle().SetValue("수신 시간(ms)"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue("분류 1"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue("분류 2"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue("분류 3"));
	*/
	
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_FAULT_UPPER	),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg1","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_FAULT_LOWER	),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg2","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_END_UPPER	),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg3","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_END_LOWER	),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg4","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg5","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_SENT_TIME		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg6","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg7","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg8","IDD_CAN_FD_SETTING_DLG")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg9","IDD_CAN_FD_SETTING_DLG")));//&&
	
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT2		),	CGXStyle().SetValue("& StartBit"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT2		),	CGXStyle().SetValue("& BitCount"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER2	),	CGXStyle().SetValue("& ByteOrder"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE2		),	CGXStyle().SetValue("& DataType"));
	//m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT2		),	CGXStyle().SetValue("& 설정값"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT2		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg10","IDD_CAN_FD_SETTING_DLG")));//&&
		
	m_wndCanGridSlave.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndCanGridSlave.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_NO),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_ID),
		CGXStyle()
  			.SetControl(GX_IDS_CTRL_EDIT)
	);
	//Edit Ctrl////////////////////////////////////////
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_SENT_TIME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	m_wndCanGridSlave.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndCanGridSlave, IDS_CTRL_REALEDIT));
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	char str[12], str2[7], str3[5];

	sprintf(str, "%%d", 1);		
	//sprintf(str2, "%d", 2);	//정수부 //yulee 20180329 권준구c 요청 정수부 크기 2->3 
	sprintf(str2, "%d", 3);		//정수부 2->3

	//.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63")) 63 -> 511로 변경
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("511"))
			//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~511)"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg11","IDD_CAN_FD_SETTING_DLG"))//&&
			.SetValue(0L)
			
	);
	//.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63")) 63 -> 511로 변경
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT2),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("511"))
		//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~511)"))
		.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("SettingCanFdDlg_InitGridMaster_msg11","IDD_CAN_FD_SETTING_DLG"))//&&
		.SetValue(0L)
		
		);

	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(1L)
	);
	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT2),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(0L)
	);

	sprintf(str, "%%.%dlf", 10);	//실수 포멧			ljb 20160825 박진호 요청 6->10
	sprintf(str2, "%d", 6);		    //정수부 크기		
	sprintf(str3, "%d", 10);		//소수부 크기		ljb 20160825 박진호 요청 6->10

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_FACTOR),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1L)
	);
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);

	//default 전송 값, 기본값자 자리수 셋팅
	sprintf(str3, "%d", 6);		//소숫점 이하		// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str, "%%.%dlf", 6);						// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str2, "%d", 8);		//정수부			//ljb 12->8  [8/29/2011 XNOTE]

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT2),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);

	//division 전송 값, 기본값자 자리수 셋팅
	sprintf(str, "%%d", 2);		
	sprintf(str2, "%d", 4);		//정수부
	
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1, CAN_COL_DIVISION3),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("1000"))
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
		.SetValue(0L)
		);

	sprintf(str3, "%d", 6);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 8);		//정수부

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_FAULT_UPPER, CAN_COL_END_LOWER),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(0L)
	);

	///////////////////////////////////////////////////

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
				.SetValue(_T("Intel"))
		);
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
		.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
		.SetValue(_T("Intel"))
		);
	if(IsCanValHex){
		m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
				CGXStyle().SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
						  .SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\nHex\r\n"))				
						  .SetValue(_T("Unsigned"))
		);
	}else{
		m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
			CGXStyle().SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
			.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))				
			.SetValue(_T("Unsigned"))
		);
	}
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE2),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
		.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
		//.SetChoiceList(_T("Unsigned\r\nSigned\r\n"))
		.SetValue(_T("Unsigned"))
		);

	//전송 속도 기본값자 자리수 셋팅
	sprintf(str, "%%d", 1);		//정수부
	sprintf(str2, "%d", 5);		
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_SENT_TIME),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(0L)
		);

	//ljb 2011222 이재복 S //////////
	CString strComboItem,strTemp;
	strComboItem = "None [0]\r\n";
	for (int i=0; i < m_strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",m_strArryCanName.GetAt(i),m_uiArryCanCode.GetAt(i));
		strComboItem = strComboItem + strTemp;		
	}
	//////////////////////////////////////////////////////////////////////////
	
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1, CAN_COL_DIVISION3),
		CGXStyle()
		//		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetControl(GX_IDS_CTRL_COMBOBOX)			//쓰기도 가능 //yulee 20190714 check
//		.SetChoiceList(_T("NONE\r\nCELL ALL\r\nCELL HIGH\r\nCELL LOW\r\nTEMP ALL\r\nTEMP HIGH\r\nTEMP LOW\r\nBMS Flt\r\nSOC ALL\r\nSOC HIGH\r\nSOC LOW\r\n"))
//		.SetChoiceList(_T("NONE\r\nCELL ALL\r\nCELL HIGH\r\nCELL LOW\r\nTEMP ALL\r\nTEMP HIGH\r\nTEMP LOW\r\nBMS Flt\r\nSOC ALL\r\nSOC HIGH\r\nSOC LOW\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
//		.SetChoiceList(_T("NONE\r\nCAN WARNING\r\nCAN ERROR\r\nCAN REST_OFF\r\nCAN PAUSE\r\nCAN MESSAGE\r\nCAN PAUSE_MESSAGE\r\nCELL ALL\r\nCELL HIGH\r\nCELL LOW\r\nTEMP ALL\r\nTEMP HIGH\r\nTEMP LOW\r\nBMS Flt\r\nSOC ALL\r\nSOC HIGH\r\nSOC LOW\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
//		.SetChoiceList(_T("NONE\r\nCAN WARNING\r\nCAN ERROR\r\nCAN REST_OFF\r\nCAN PAUSE\r\nCAN MESSAGE\r\nCAN PAUSE & MESSAGE\r\n"))
//		.SetChoiceList(_T("NONE\r\nCAN WARNING (1804)\r\nCAN ERROR (1805)\r\nCAN REST_OFF (1806)\r\nCAN PAUSE (1801)\r\nCAN MESSAGE (1802)\r\nCAN PAUSE & MESSAGE (1803)\r\n"))
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);

// 	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_CVFLAG),
// 		CGXStyle()
// 			.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 			.SetValue(0L)
// 	);

	////////////////////////////////////////////////////
}

void CSettingCanFdDlg::UpdateGridData()
{
	int nRow= 0;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if(pMD == NULL)	return;	

	for(int nMaster = 0; nMaster < pMD->GetInstalledCanMaster(nSelCh); nMaster++)
	{

		CString strTemp;
		nRow = m_wndCanGrid.GetRowCount();
		m_wndCanGrid.InsertRows(++nRow, 1); //ksj 20210305
		SFT_CAN_SET_DATA canData = pMD->GetCANSetData(nSelCh, PS_CAN_TYPE_MASTER, nMaster);	

		CString strName, strID; //yulee 20190911
		strName.Format("%s", canData.name);
		if(strName.CompareNoCase(_T("$")) == 0)
		{
			break;
		}

		if(m_nMasterIDType == 0)	//Hex Type 이면
			strTemp.Format("%X", canData.canID);	
		else
			//strTemp.Format("%ld", canData.canID);
			strTemp.Format("%u", canData.canID); //ksj 20200323 : unsined 타입 출력으로 변경.

		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_ID), strTemp);		
		strTemp.Format("%s", canData.name);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_NAME), strTemp);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT), canData.startBit);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT), canData.bitCount);

		if(canData.byte_order == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Intel");
		else
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Motorola");

		if(canData.data_type == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Unsigned");
		else if(canData.data_type == 1)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Signed");
		else if(canData.data_type == 2)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Float");
		else if(canData.data_type == 3){
			SetGridCanDivisionEnable(0,nRow,FALSE); //2014.08.12
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "String");	
		}
		else{
			SetGridCanDivisionEnable(0,nRow,FALSE); //2014.08.12 
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Hex");	
		}
		
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_FACTOR), canData.factor_multiply);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_OFFSET), canData.factor_Offset);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_FAULT_UPPER), canData.fault_upper);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_FAULT_LOWER), canData.fault_lower);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_END_UPPER), canData.end_upper);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_END_LOWER), canData.end_lower);	
// 		if(nMaster+1 < pMD->GetInstalledCanMaster(nSelCh))
// 			m_wndCanGrid.InsertRows(nRow+1, 1);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DEFAULT), canData.default_fValue);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_SENT_TIME), canData.sentTime);

		strTemp = Fun_FindCanName(canData.function_division);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), strTemp);

		strTemp = Fun_FindCanName(canData.function_division2);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), strTemp);

		strTemp = Fun_FindCanName(canData.function_division3);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), strTemp);

	
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT2), canData.startBit2);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT2), canData.bitCount2);
		
		if(canData.byte_order2 == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER2), "Intel");
		else
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER2), "Motorola");
		
		if(canData.data_type2 == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE2), "Unsigned");
		else if(canData.data_type2 == 1)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE2), "Signed");
		else if(canData.data_type2 == 2)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE2), "Float");
		else
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE2), "String");	
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DEFAULT2), canData.default_fValue2);
	}

	
	//2014.08.22 캔 리시브 슬레이브를 사용하지않을시 그리드 적용하지않음.
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);
	if(isSlave == FALSE){
		return;
	}
	
	for(int nSlave = 0; nSlave < pMD->GetInstalledCanSlave(nSelCh); nSlave++)
	{
		CString strTemp;
		nRow = m_wndCanGridSlave.GetRowCount();
		m_wndCanGridSlave.InsertRows(++nRow, 1); //ksj 20210305
		SFT_CAN_SET_DATA canData = pMD->GetCANSetData(nSelCh, PS_CAN_TYPE_SLAVE, nSlave);
		
		CString strName, strID; //yulee 20190911
		strName.Format("%s", canData.name);
		if(strName.CompareNoCase(_T("$")) == 0)
		{
			break;
		}

		if(m_nMasterIDType == 0)	//Hex Type 이면
			strTemp.Format("%X", canData.canID);	
		else
			//strTemp.Format("%ld", canData.canID);
			strTemp.Format("%u", canData.canID); //ksj 20200323 : unsined 타입 출력으로 변경.

		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_ID), strTemp);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_NAME), canData.name);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT), canData.startBit);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT), canData.bitCount);
		if(canData.byte_order == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Intel");
		else
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Motorola");
		if(canData.data_type == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Unsigned");
		else if(canData.data_type == 1)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Signed");
		else if(canData.data_type == 2)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Float");
		else if(canData.data_type == 3){
			SetGridCanDivisionEnable(1,nRow,FALSE); //2014.08.12
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "String");	
		}
		else{
			SetGridCanDivisionEnable(1,nRow,FALSE); //2014.08.12
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Hex");	
		}
		
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_FACTOR), canData.factor_multiply);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_OFFSET), canData.factor_Offset);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_FAULT_UPPER), canData.fault_upper);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_FAULT_LOWER), canData.fault_lower);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_END_UPPER), canData.end_upper);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_END_LOWER), canData.end_lower);	
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DEFAULT), canData.default_fValue);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_SENT_TIME), canData.sentTime);

		strTemp = Fun_FindCanName(canData.function_division);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division2);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division3);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), strTemp);
// 		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), canData.function_division);
// 		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), canData.function_division2);
// 		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), canData.function_division3);

		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT2), canData.startBit2);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT2), canData.bitCount2);
		
		if(canData.byte_order2 == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER2), "Intel");
		else
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER2), "Motorola");
		
		if(canData.data_type2 == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE2), "Unsigned");
		
// 		if(nSlave+1 < pMD->GetInstalledCanSlave(nSelCh)){
// 			m_wndCanGridSlave.InsertRows(nRow+1, 1);
//		}
	}
	
}

void CSettingCanFdDlg::UpdateCommonData()
{
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if(pMD == NULL)	return;
	
	CString strData;
	SFT_CAN_COMMON_DATA commonData = pMD->GetCANCommonData(nSelCh, PS_CAN_TYPE_MASTER);

	//ljb 20180323
	m_ctrlBaudRate.ResetContent();
	m_ctrlBaudRate.AddString("125 kbps");
	m_ctrlBaudRate.SetItemData(0,0);
	m_ctrlBaudRate.AddString("250 kbps");
	m_ctrlBaudRate.SetItemData(1,1);
	m_ctrlBaudRate.AddString("500 kbps");
	m_ctrlBaudRate.SetItemData(2,2);
	m_ctrlBaudRate.AddString("1 Mbps");
	m_ctrlBaudRate.SetItemData(3,3);
	m_ctrlBaudRate.SetCurSel(commonData.can_baudrate);
	//m_ctrlBaudRate.GetWindowText(m_strBaudRate);

	m_ctrlDataRate.ResetContent();
	m_ctrlDataRate.AddString("500 kbps");
	m_ctrlDataRate.SetItemData(0,0);
	m_ctrlDataRate.AddString("833 kbps");
	m_ctrlDataRate.SetItemData(1,1);
	m_ctrlDataRate.AddString("1 Mbps");
	m_ctrlDataRate.SetItemData(2,2);
	m_ctrlDataRate.AddString("1.5 Mbps");
	m_ctrlDataRate.SetItemData(3,3);
	m_ctrlDataRate.AddString("2 Mbps");
	m_ctrlDataRate.SetItemData(4,4);
	m_ctrlDataRate.AddString("3 Mbps");
	m_ctrlDataRate.SetItemData(5,5);
	m_ctrlDataRate.AddString("4 Mbps");
	m_ctrlDataRate.SetItemData(6,6);


	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
	if(bUseCANFD)
		m_ctrlDataRate.SetCurSel(commonData.can_datarate);
	else
		m_ctrlDataRate.SetCurSel(0);
	//m_ctrlDataRate.GetWindowText(m_strBaudRate);

	m_ctrlBmsTerminal.ResetContent();
	m_ctrlBmsTerminal.AddString("Open");
	m_ctrlBmsTerminal.SetItemData(0,0);
	m_ctrlBmsTerminal.AddString("120 Ohm");
	m_ctrlBmsTerminal.SetItemData(1,1);
	if(bUseCANFD)//yulee 20181030
	{
		m_ctrlBmsTerminal.SetCurSel(commonData.terminal_r);
		//m_ctrlBmsTerminal.SetCurSel(1); //lyj 20200722 default 1
	}
	else
	{
		m_ctrlBmsTerminal.SetCurSel(1);
	}
	
	m_ctrlBmsCrcType.ResetContent();
	m_ctrlBmsCrcType.AddString("Non");
	m_ctrlBmsCrcType.SetItemData(0,0);
	m_ctrlBmsCrcType.AddString("ISO");
	m_ctrlBmsCrcType.SetItemData(1,1);
	if(bUseCANFD)
	{
		m_ctrlBmsCrcType.SetCurSel(commonData.crc_type);
		//m_ctrlBmsCrcType.SetCurSel(1); //lyj 20200722 default 1
	}
	else
	{
		m_ctrlBmsCrcType.SetCurSel(1);
	}

	m_strCanID.Format("%lX", commonData.controller_canID);		//khs 20081118
	m_ckExtID = commonData.extended_id;
	m_ckCanFD = commonData.can_fd_flag;
	if (commonData.can_fd_flag)
	{
		GetDlgItem(IDC_COMBO_CAN_DATARATE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_CRC_M)->EnableWindow(TRUE);

		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(bUseCANFD)
		//{
		//
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(TRUE);
		//	GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(TRUE);
		//}
	}
	else
	{
		GetDlgItem(IDC_COMBO_CAN_DATARATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->EnableWindow(TRUE);//20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
		GetDlgItem(IDC_COMBO_CAN_CRC_M)->EnableWindow(FALSE); 

		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(bUseCANFD)
		//{
		//
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//}
	}

	if(commonData.mask[0] != 0 || commonData.mask[1] != 0)
	{
		m_ckUserFilter = 1;
		m_strMask1.Format("%lX", commonData.mask[0]);
		m_strFilter1_1.Format("%lX", commonData.filter[0]);
		m_strFilter1_2.Format("%lX", commonData.filter[1]);
		m_strMask2.Format("%X", commonData.mask[1]);
		m_strFilter2_1.Format("%lX", commonData.filter[2]);
		m_strFilter2_2.Format("%lX", commonData.filter[3]);
		m_strFilter2_3.Format("%lX", commonData.filter[4]);
		m_strFilter2_4.Format("%lX", commonData.filter[5]);	
	}
	else
		m_ckUserFilter = 0;
	
	EnableMaskFilter(m_ckUserFilter, PS_CAN_TYPE_MASTER);
	
// 	m_ctrlBmsType.GetLBText(commonData.bms_type, m_strBmsType);
// 	m_ctrlBmsSJW.GetLBText(commonData.bms_sjw, m_strBmsSJW);
	//ksj 20210210 :
	m_strBmsType.Format("%d",commonData.bms_type);
	m_strBmsSJW.Format("%d",commonData.bms_sjw);

// 	m_ctrlBmsType.SetWindowText(m_strBmsType);
// 	m_ctrlBmsSJW.SetWindowText(m_strBmsSJW);		
	//ksj end
	

	//Slave Setting
	commonData = pMD->GetCANCommonData(nSelCh, PS_CAN_TYPE_SLAVE);
	m_strCanIDSlave.Format("%lX", commonData.controller_canID);		//khs 20081118
	m_ckExtIDSlave = commonData.extended_id;
	m_ckCanFD_Slave = commonData.can_fd_flag;
	if (commonData.can_fd_flag)
	{
		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_CRC_S)->EnableWindow(TRUE);

		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(bUseCANFD)
		//{
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//}
	}
	else
	{
		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->EnableWindow(TRUE);//20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
		GetDlgItem(IDC_COMBO_CAN_CRC_S)->EnableWindow(FALSE); 

		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(bUseCANFD)
		//{
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//}
	}

	if(commonData.mask[0] != 0 || commonData.mask[1] != 0)
	{
		m_ckUserFilterSlave = 1;
		m_strMask1Slave.Format("%lX", commonData.mask[0]);
		m_strFilter1_1Slave.Format("%lX", commonData.filter[0]);
		m_strFilter1_2Slave.Format("%lX", commonData.filter[1]);
		m_strMask2Slave.Format("%X", commonData.mask[1]);
		m_strFilter2_1Slave.Format("%lX", commonData.filter[2]);
		m_strFilter2_2Slave.Format("%lX", commonData.filter[3]);
		m_strFilter2_3Slave.Format("%lX", commonData.filter[4]);
		m_strFilter2_4Slave.Format("%lX", commonData.filter[5]);
		
	}
	else
		m_ckUserFilterSlave = 0;
	EnableMaskFilter(m_ckUserFilterSlave, PS_CAN_TYPE_SLAVE);

	//ljb 20180323
	m_ctrlBaudRateSlave.ResetContent();
	m_ctrlBaudRateSlave.AddString("125 kbps");
	m_ctrlBaudRateSlave.SetItemData(0,0);
	m_ctrlBaudRateSlave.AddString("250 kbps");
	m_ctrlBaudRateSlave.SetItemData(1,1);
	m_ctrlBaudRateSlave.AddString("500 kbps");
	m_ctrlBaudRateSlave.SetItemData(2,2);
	m_ctrlBaudRateSlave.AddString("1 Mbps");
	m_ctrlBaudRateSlave.SetItemData(3,3);
	m_ctrlBaudRateSlave.SetCurSel(commonData.can_baudrate);
	//m_ctrlBaudRateSlave.GetWindowText(m_strBaudRateSlave);

	m_ctrlDataRateSlave.ResetContent();
	m_ctrlDataRateSlave.AddString("500 kbps");
	m_ctrlDataRateSlave.SetItemData(0,0);
	m_ctrlDataRateSlave.AddString("833 kbps");
	m_ctrlDataRateSlave.SetItemData(1,1);
	m_ctrlDataRateSlave.AddString("1 Mbps");
	m_ctrlDataRateSlave.SetItemData(2,2);
	m_ctrlDataRateSlave.AddString("1.5 Mbps");
	m_ctrlDataRateSlave.SetItemData(3,3);
	m_ctrlDataRateSlave.AddString("2 Mbps");
	m_ctrlDataRateSlave.SetItemData(4,4);
	m_ctrlDataRateSlave.AddString("3 Mbps");
	m_ctrlDataRateSlave.SetItemData(5,5);
	m_ctrlDataRateSlave.AddString("4 Mbps");
	m_ctrlDataRateSlave.SetItemData(6,6);	
	if(bUseCANFD)//yulee 20181030
		m_ctrlDataRateSlave.SetCurSel(commonData.can_datarate);
	else
		m_ctrlDataRateSlave.SetCurSel(0);
	
	//m_ctrlDataRateSlave.GetWindowText(m_strBaudRateSlave);

	m_ctrlBmsTerminalSlave.ResetContent();
	m_ctrlBmsTerminalSlave.AddString("Open");
	m_ctrlBmsTerminalSlave.SetItemData(0,0);
	m_ctrlBmsTerminalSlave.AddString("120 Ohm");
	m_ctrlBmsTerminalSlave.SetItemData(1,1);
	if(bUseCANFD)
	{
		m_ctrlBmsTerminalSlave.SetCurSel(commonData.terminal_r);
		//m_ctrlBmsTerminalSlave.SetCurSel(1); //lyj 20200722 deafult 1
	}
	else
	{
		m_ctrlBmsTerminalSlave.SetCurSel(1);
	}
	
	m_ctrlBmsCrcTypeSlave.ResetContent();
	m_ctrlBmsCrcTypeSlave.AddString("Non");
	m_ctrlBmsCrcTypeSlave.SetItemData(0,0);
	m_ctrlBmsCrcTypeSlave.AddString("ISO");
	m_ctrlBmsCrcTypeSlave.SetItemData(1,1);
	if(bUseCANFD)
	{
		m_ctrlBmsCrcTypeSlave.SetCurSel(commonData.crc_type);
		//m_ctrlBmsCrcTypeSlave.SetCurSel(1); //lyj 20200722 deafult 1
	}
	else
	{
		m_ctrlBmsCrcTypeSlave.SetCurSel(1);
	}

// 	m_ctrlBmsTypeSlave.GetLBText(commonData.bms_type, m_strBmsTypeSlave);
// 	m_ctrlBmsSJWSlave.GetLBText(commonData.bms_sjw, m_strBmsSJWSlave);
	//ksj 20210210 :
	m_strBmsTypeSlave.Format("%d",commonData.bms_type);
	m_strBmsSJWSlave.Format("%d",commonData.bms_sjw);

// 	m_ctrlBmsTypeSlave.SetWindowText(m_strBmsTypeSlave);
// 	m_ctrlBmsSJWSlave.SetWindowText(m_strBmsSJWSlave);		
	//ksj end

	//if(bUseCANFD)
	//{
	//	
	//}
	//else
	//{
	//	GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	//}

	//ksj 20210115 : CAN FD 체크 풀려 있으면 기본 값 강제 초기화 추가.
	//혹시 문제 생길 수 있으므로, 기능 비활성화 가능하도록 일단 옵션처리.
	BOOL bUseInitCanFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "bUseInitCanFD", TRUE);
	if(bUseInitCanFD)
	{
		if(m_ckCanFD == FALSE)
		{
			m_ctrlDataRate.SetCurSel(0);
			m_ctrlBmsTerminal.SetCurSel(1);
			m_ctrlBmsCrcType.SetCurSel(1);
		}

		if(m_ckCanFD_Slave == FALSE)
		{
			m_ctrlDataRateSlave.SetCurSel(0);
			m_ctrlBmsTerminalSlave.SetCurSel(1);
			m_ctrlBmsCrcTypeSlave.SetCurSel(1);
		}	
	}
	//ksj end

	UpdateData(FALSE);

	//ksj 20210502 : UpdateData 이후에 호출되어야한다.
	m_ctrlBmsType.SetWindowText(m_strBmsType);
	m_ctrlBmsSJW.SetWindowText(m_strBmsSJW);	
	m_ctrlBmsTypeSlave.SetWindowText(m_strBmsTypeSlave);
	m_ctrlBmsSJWSlave.SetWindowText(m_strBmsSJWSlave);
	//ksj end
}

void CSettingCanFdDlg::OnOK() 
{
	UpdateData();
	CString strTemp,strCode;
	int		nLength;
	int i;
	//////////////////////////////////////////////////////////////////////////
	//ljb 20110119 분류코드 체크 -> 20110222 Editor 쪽으로 넘김
	//////////////////////////////////////////////////////////////////////////

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", m_nMasterIDType);

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return;
	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END	)
	{
		//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}
	UpdateData();

	if ((m_wndCanGrid.GetRowCount() + m_wndCanGridSlave.GetRowCount()) > 512)
	{
		//AfxMessageBox("CAN 설정은 512개를 초과 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg2","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}

	SFT_MD_SYSTEM_DATA * pSysData = ::SFTGetModuleSysData(nSelModule);
	if (pSysData->byCanCommType == 0 && m_ckExtID && m_ckCanFD_Slave)
	{
		//AfxMessageBox("CAN FD 설정을 할 수 없는 충방전기 입니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg3","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}

	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));
	SFT_CAN_COMMON_DATA commonData;
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//Master Setting
	ZeroMemory(&commonData, sizeof(SFT_CAN_COMMON_DATA));
	if(m_ctrlBaudRate.GetCurSel() != LB_ERR)
	{
		commonData.can_baudrate = m_ctrlBaudRate.GetItemData(m_ctrlBaudRate.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Master BaudRate 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg4","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}
	if (m_ckCanFD)
	{
		if(m_ctrlDataRate.GetCurSel() != LB_ERR)
		{
			commonData.can_datarate = m_ctrlDataRate.GetItemData(m_ctrlDataRate.GetCurSel());
		}
		else
		{
			//AfxMessageBox("Master DataRate 설정을 다시하세요.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg5","IDD_CAN_FD_SETTING_DLG"));//&&
			return;
		}
	}
	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181127
	if(bUseCANFD)
	{
		commonData.can_fd_flag = m_ckCanFD;
	}
	else
	{
		commonData.can_fd_flag = 0;
	}
//	commonData.can_fd_flag = m_ckCanFD;	//ljb 20180326 add
	commonData.extended_id	= m_ckExtID;
	commonData.controller_canID = strtoul(m_strCanID,(char **)NULL,  16);
// 	commonData.bms_type = m_ctrlBmsType.GetCurSel();
// 	commonData.bms_sjw = m_ctrlBmsSJW.GetCurSel();
	//ksj 20210210
	m_ctrlBmsType.GetWindowText(strTemp);
	commonData.bms_type = atoi(strTemp);
	m_ctrlBmsSJW.GetWindowText(strTemp);
	commonData.bms_sjw = atoi(strTemp);
	//ksj end
	commonData.can_datarate = m_ctrlDataRate.GetCurSel();	//20180329 yulee add
	commonData.terminal_r = m_ctrlBmsTerminal.GetCurSel();
	commonData.crc_type = m_ctrlBmsCrcType.GetCurSel();

	//ljb 20181115 S CAN,LIN select 
	commonData.can_lin_select	= nLINtoCANSelect;
	//ljb 20181115 E CAN,LIN select 

	if(m_ckUserFilter == TRUE)
	{
		commonData.mask[0] = strtoul(m_strMask1,(char **)NULL,  16);	
		commonData.mask[1] = strtoul(m_strMask2,(char **)NULL,  16);
		commonData.filter[0] = strtoul(m_strFilter1_1,(char **)NULL,  16);
		commonData.filter[1] = strtoul(m_strFilter1_2,(char **)NULL,  16);
		commonData.filter[2] = strtoul(m_strFilter2_1,(char **)NULL,  16);
		commonData.filter[3] = strtoul(m_strFilter2_2,(char **)NULL,  16);
		commonData.filter[4] = strtoul(m_strFilter2_3,(char **)NULL,  16);
		commonData.filter[5] = strtoul(m_strFilter2_4,(char **)NULL,  16);
	}
	
	
	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_MASTER);	
	memcpy(&canSetData.canCommonData, &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Slave Setting
	//	commonData.can_baudrate = m_ctrlBaudRateSlave.GetCurSel();
	ZeroMemory(&commonData, sizeof(SFT_CAN_COMMON_DATA));
	if(m_ctrlBaudRateSlave.GetCurSel() != LB_ERR)
	{
		commonData.can_baudrate = m_ctrlBaudRateSlave.GetItemData(m_ctrlBaudRateSlave.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Slave BaudRate 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg6","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}
	if (m_ckCanFD_Slave)
	{
		if(m_ctrlDataRateSlave.GetCurSel() != LB_ERR)
		{
			commonData.can_datarate = m_ctrlDataRateSlave.GetItemData(m_ctrlDataRateSlave.GetCurSel());
		}
		else
		{
			//AfxMessageBox("Slave DataRate 설정을 다시하세요.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg7","IDD_CAN_FD_SETTING_DLG"));//&&
			return;
		}
	}
	bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181127
	if(bUseCANFD)
	{
		commonData.can_fd_flag = m_ckCanFD_Slave;	//ljb 20180326 add
	}
	else
	{
		commonData.can_fd_flag = 0;	//ljb 20180326 add
	}
//	commonData.can_fd_flag = m_ckCanFD_Slave;	//ljb 20180326 add
	commonData.extended_id	= m_ckExtIDSlave;
	commonData.controller_canID = strtoul(m_strCanIDSlave,(char **)NULL,  16);
// 	commonData.bms_type = m_ctrlBmsTypeSlave.GetCurSel();
// 	commonData.bms_sjw = m_ctrlBmsSJWSlave.GetCurSel();
	//ksj 20210210
	m_ctrlBmsTypeSlave.GetWindowText(strTemp);
	commonData.bms_type = atoi(strTemp);
	m_ctrlBmsSJWSlave.GetWindowText(strTemp);
	commonData.bms_sjw = atoi(strTemp);
	//ksj end
	commonData.can_datarate = m_ctrlDataRateSlave.GetCurSel(); //20180329 yulee add
	commonData.terminal_r = m_ctrlBmsTerminalSlave.GetCurSel();
	commonData.crc_type = m_ctrlBmsCrcTypeSlave.GetCurSel();
	//ljb 20181115 S CAN,LIN select 
	commonData.can_lin_select	= nLINtoCANSelect;
	//ljb 20181115 E CAN,LIN select 

	if(m_ckUserFilterSlave == TRUE)
	{
		commonData.mask[0] = strtoul(m_strMask1Slave,(char **)NULL,  16);
		commonData.mask[1] = strtoul(m_strMask2Slave,(char **)NULL,  16);
		commonData.filter[0] = strtoul(m_strFilter1_1Slave,(char **)NULL,  16);
		commonData.filter[1] = strtoul(m_strFilter1_2Slave,(char **)NULL,  16);
		commonData.filter[2] = strtoul(m_strFilter2_1Slave,(char **)NULL,  16);
		commonData.filter[3] = strtoul(m_strFilter2_2Slave,(char **)NULL,  16);
		commonData.filter[4] = strtoul(m_strFilter2_3Slave,(char **)NULL,  16);
		commonData.filter[5] = strtoul(m_strFilter2_4Slave,(char **)NULL,  16);
	}
	
	int nSelectIndex=0;	//ljb 2011222 이재복 //////////
	int nMasterCount = 0;
	SFT_CAN_SET_DATA canData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);

	for(int i = 0; i<m_wndCanGrid.GetRowCount(); i++)
	{		
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_ID);
		if(strTemp == "")
		{
			//CString strMsg;
			//strMsg.Format("Master %d 번째열에 ID가 없습니다.", i+1 );
			//AfxMessageBox(strMsg);
			//return;
			continue;
		}
// 		if(strTemp == "0") //20180528 yulee //ksj 20210705 : 주석처리하여 0입력 가능하게 허용. 0입력 필요하다고함.
// 		{
// 			continue;
// 		}
		//canData[i].canID = strtoul(strTemp,(char **)NULL,  16);
		if(m_nMasterIDType == 0)		//16진수로 되어있으면 10진수로 변환
		{
			CString str10;
			
			str10.Format("%d", strtoul(strTemp , NULL, 16));
			strTemp = str10;
		}
		//canData[i].canID = atol(strTemp);
		canData[i].canID = strtoul(strTemp,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.

		canData[i].canType = PS_CAN_TYPE_MASTER;
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_NAME);
		strTemp.TrimLeft();
		if(strTemp.IsEmpty()) 	strTemp.Format("CAN %d",i+1); //ksj 20210305 : 이름이 없으면 임의로 넣음.
		sprintf(canData[i].name, strTemp);		
		canData[i].startBit = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_STARTBIT));
		canData[i].bitCount = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BITCOUNT));
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER);
		if(strTemp == "Motorola")
			canData[i].byte_order = 1;
		else
			canData[i].byte_order = 0;		
		
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DATATYPE);
		if(strTemp == "Unsigned")
			canData[i].data_type = 0;
		else if(strTemp == "Signed")
			canData[i].data_type = 1;
		else if(strTemp == "Float")
			canData[i].data_type = 2;
		else if(strTemp == "String")
			canData[i].data_type = 3;
		else if(strTemp == "Hex")
			canData[i].data_type = 4;

		canData[i].factor_multiply = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_FACTOR));
		canData[i].factor_Offset = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_OFFSET));
		
//		canData[i].cvFlag = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_CVFLAG));
		canData[i].fault_upper = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_FAULT_UPPER));
		canData[i].fault_lower = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_FAULT_LOWER));
		canData[i].end_upper = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_END_UPPER));
		canData[i].end_lower = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_END_LOWER));
		canData[i].default_fValue = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DEFAULT));
		canData[i].sentTime = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_SENT_TIME));

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canData[i].function_division = atoi(strCode);

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canData[i].function_division2 = atoi(strCode);

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canData[i].function_division3 = atoi(strCode);

		//ljb 20131213 add **********************************************************************
		canData[i].startBit2 = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_STARTBIT2));
		canData[i].bitCount2 = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BITCOUNT2));
		if (canData[i].bitCount2 > 32) 
		{
			//AfxMessageBox("Master &bitCount를 32 bit 이상 설정 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg8","IDD_CAN_FD_SETTING_DLG"));//&&
			return;
		}
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER2);
		if(strTemp == "Motorola")
			canData[i].byte_order2 = 1;
		else
			canData[i].byte_order2 = 0;		
		
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DATATYPE2);
		if(strTemp == "Unsigned")
			canData[i].data_type2 = 0;
		else if(strTemp == "Signed")
			canData[i].data_type2 = 1;
		else if(strTemp == "Float")
			canData[i].data_type2 = 2;
		else if(strTemp == "String")
			canData[i].data_type2 = 3;
		canData[i].default_fValue2 = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DEFAULT2));
		//****************************************************************************************

		nMasterCount++;		
	}

	int nSlaveCount = 0;
	for(int i = 0; i<m_wndCanGridSlave.GetRowCount(); i++)
	{		
		int nIndex = i + nMasterCount;
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_ID);
		if(strTemp == "")
		{
			continue;
		}
		//canData[nIndex].canID = strtoul(strTemp,(char **)NULL,  16);

		if(m_nMasterIDType == 0)		//16진수로 되어있으면 10진수로 변환
		{
			CString str10;
			
			str10.Format("%d", strtoul(strTemp , NULL, 16));
			strTemp = str10;
		}

		//canData[nIndex].canID = atol(strTemp);
		canData[nIndex].canID = strtoul(strTemp,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.
		canData[nIndex].canType = PS_CAN_TYPE_SLAVE;
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_NAME);
		strTemp.TrimLeft();
		if(strTemp.IsEmpty()) 	strTemp.Format("CAN %d",i+1); //ksj 20210305 : 이름이 없으면 임의로 넣음.
		sprintf(canData[nIndex].name, strTemp);		
		canData[nIndex].startBit = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_STARTBIT));
		canData[nIndex].bitCount = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BITCOUNT));
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER);
		if(strTemp == "Motorola")
			canData[nIndex].byte_order = 1;
		else
			canData[nIndex].byte_order = 0;		
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DATATYPE);
		if(strTemp == "Unsigned")
			canData[nIndex].data_type = 0;
		else if(strTemp == "Signed")
			canData[nIndex].data_type = 1;
		else if(strTemp == "Float")
			canData[nIndex].data_type = 2;
		else if(strTemp == "String")
			canData[nIndex].data_type = 3;
		
		canData[nIndex].factor_multiply = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_FACTOR));
		canData[nIndex].factor_Offset = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_OFFSET));
//		canData[nIndex].cvFlag =		atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_CVFLAG));
		canData[nIndex].fault_upper =	atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_FAULT_UPPER));
		canData[nIndex].fault_lower =	atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_FAULT_LOWER));
		canData[nIndex].end_upper =		atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_END_UPPER));
		canData[nIndex].end_lower =		atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_END_LOWER));
		canData[nIndex].default_fValue =		atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DEFAULT));
		canData[nIndex].sentTime =		atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_SENT_TIME));

		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canData[nIndex].function_division = atoi(strCode);

		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canData[nIndex].function_division2 = atoi(strCode);

		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canData[nIndex].function_division3 = atoi(strCode);

		//ljb 20131213 add ************************************************************************************
		canData[nIndex].startBit2 = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_STARTBIT2));
		canData[nIndex].bitCount2 = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BITCOUNT2));
		if (canData[nIndex].bitCount2 > 32) 
		{
			//AfxMessageBox("Slave &bitCount를 32 bit 이상 설정 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg8","IDD_CAN_FD_SETTING_DLG"));//&&
			return;
		}
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER2);
		if(strTemp == "Motorola")
			canData[nIndex].byte_order2 = 1;
		else
			canData[nIndex].byte_order2 = 0;		
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DATATYPE2);
		if(strTemp == "Unsigned")
			canData[nIndex].data_type2 = 0;
		//////////////////////////////////////////////////////////////////////////
		// + BW KIM 2014.01.23 100C 에서는 일단 이거 쓴다.
// 		else if(strTemp == "Signed")
// 			canData[nIndex].data_type2 = 1;
// 		else if(strTemp == "Float")
// 			canData[nIndex].data_type2 = 2;
// 		else if(strTemp == "String")
// 			canData[nIndex].data_type2 = 3;
		// -
		//////////////////////////////////////////////////////////////////////////
		canData[nIndex].default_fValue2 =		atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DEFAULT2));

		//*******************************************************************************************************

		nSlaveCount++;			
	}

	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_SLAVE);
	memcpy(&canSetData.canCommonData[1], &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008
	
	pMD->SetCANSetData(nSelCh, canData, nSlaveCount+nMasterCount);
	memcpy(&canSetData.canSetData, &canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);	//ljb 201008

	//SFT_RCV_CMD_CAN_SET canSetData;
	//ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CAN_SET));
// 	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
// 	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));
	
// 	SFT_MD_CAN_INFO_DATA * canInforData = pMD->GetCanAllData();
// 	memcpy(&canSetData.canCommonData, pMD->GetCANCommonData(), sizeof(canSetData.canCommonData));
// 	memcpy(&canSetData.canSetData, canInforData->canSetAllData.canSetData, sizeof(canSetData.canSetData));
	

	TRACE("CAN commdata Size = %d, size data = %d, total size = %d \n",sizeof(canSetData.canCommonData[0]),sizeof(canSetData.canSetData[0]), sizeof(canSetData));
	if(int nRth = m_pDoc->SetCANDataToModule(nSelModule, &canSetData, nSelCh) == FALSE)
	{
		//AfxMessageBox("CAN 정보 설정에 실패했습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnOK_msg9","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}

	IsChange = FALSE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);

	pMD->SaveCanConfig();

	//gst_CanRxSave.CanType = 1; // // <<-- 이거 왜 1로 고정해놨냐??? 알려주라 좀.... 2021.07.12
	gst_CanRxSave.CanType = nLINtoCANSelect; //ksj 20210712 : 그냥 tx쪽 함수 1고정이 아니라 변수로 변경. 이게 무슨 역할을 하는지 정보가 없어서 문제가 있을지 모르겠다. 1고정일때는 문제가 있어서 일단 고쳐봄.
	gst_CanRxSave.CanRxBaudrate = m_ctrlBaudRate.GetCurSel();
	gst_CanRxSave.CanRxExtendedIDMode = ((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->GetCheck();
// 	gst_CanRxSave.SJW = m_ctrlBmsSJW.GetCurSel();
// 	gst_CanRxSave.BMSType =m_ctrlBmsType.GetCurSel();
	//ksj 20210210
	m_ctrlBmsType.GetWindowText(strTemp);
	gst_CanRxSave.BMSType = atoi(strTemp);
	m_ctrlBmsSJW.GetWindowText(strTemp);
	gst_CanRxSave.SJW = atoi(strTemp);
	//ksj end
	gst_CanRxSave.bEnable = TRUE;

	//gst_CanRxSaveSlave.CanType = 1; // // <<-- 이거 왜 1로 고정해놨냐??? 알려주라 좀.... 2021.07.12
	gst_CanRxSaveSlave.CanType = nLINtoCANSelect; //ksj 20210712 : 그냥 tx쪽 함수 1고정이 아니라 변수로 변경. 이게 무슨 역할을 하는지 정보가 없어서 문제가 있을지 모르겠다. 1고정일때는 문제가 있어서 일단 고쳐봄.
	gst_CanRxSaveSlave.CanRxBaudrate = m_ctrlBaudRateSlave.GetCurSel();
	gst_CanRxSaveSlave.CanRxExtendedIDMode = ((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->GetCheck();
// 	gst_CanRxSaveSlave.SJW = m_ctrlBmsSJWSlave.GetCurSel();
// 	gst_CanRxSaveSlave.BMSType = m_ctrlBmsTypeSlave.GetCurSel();
	//ksj 20210210
	m_ctrlBmsTypeSlave.GetWindowText(strTemp);
	gst_CanRxSaveSlave.BMSType = atoi(strTemp);
	m_ctrlBmsSJWSlave.GetWindowText(strTemp);
	gst_CanRxSaveSlave.SJW = atoi(strTemp);
	//ksj end
	gst_CanRxSaveSlave.bEnable = TRUE;

	//CDialog::OnOK();
}

void CSettingCanFdDlg::OnCancel() 
{
	if(IsChange)
	{
		//if(AfxMessageBox("CAN 정보가 변경되었습니다.\r\n작업을 취소하시겠습니까?", MB_YESNO) != IDYES)
		if(AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnCancel_msg1","IDD_CAN_FD_SETTING_DLG"), MB_YESNO) != IDYES)//&&
			return;
	}
	IsChange = FALSE;
	
	CDialog::OnCancel();

}

LRESULT CSettingCanFdDlg::OnGridBeginEdit(WPARAM wParam, LPARAM lParam)
{
	

	IsChangeFlag(TRUE);
	return 1;
}

void CSettingCanFdDlg::OnCheckUserFilterSlave() 
{
	UpdateData();
	if(m_ckUserFilterSlave == TRUE)
		m_strCanIDSlave = "FFFFFFFE";
	else
		m_strCanIDSlave = "FFFFFFFF";
	
	EnableMaskFilter(m_ckUserFilterSlave, PS_CAN_TYPE_SLAVE);

	IsChangeFlag(TRUE);

	UpdateData(FALSE);	
	
}

void CSettingCanFdDlg::OnSelchangeComboCanBaudrateSlave() 
{
	IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanBaudrate() 
{	
	IsChangeFlag(TRUE);
}

//String을 Long형 Hex값으로 변환한다.
long CSettingCanFdDlg::StringToHex32(CString strHex)
{
	long lReturn = 0;
	if(strHex!="")
	{
		char nTemp;
		for(int i = 0 ; i < strHex.GetLength(); i++)
		{
			char pChar = strHex[i];
			sscanf (&pChar, _T("%x"), &nTemp);
			lReturn = lReturn << 4;
			lReturn |= nTemp;
			
		}
	}
	return lReturn;
}

void CSettingCanFdDlg::OnBtnAddMaster() 
{
	CRowColArray ra;
	m_wndCanGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndCanGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, 1);

	
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnBtnDelMaster() 
{
	CRowColArray ra;
	m_wndCanGrid.GetSelectedRows(ra);

	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndCanGrid.RemoveRows(ra[i], ra[i]);
	}

	IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnBtnAddSlave() 
{
	CRowColArray ra;
	m_wndCanGridSlave.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndCanGridSlave.InsertRows(ra[0] + 1, 1);
	else
		m_wndCanGridSlave.InsertRows(m_wndCanGridSlave.GetRowCount()+1, 1);

	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnBtnDelSlave() 
{
	CRowColArray ra;
	m_wndCanGridSlave.GetSelectedRows(ra);

	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0)	//Header 삭제불가
			continue;
		m_wndCanGridSlave.RemoveRows(ra[i], ra[i]);
	}

	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit1() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit10() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit2() 
{
/*	UpdateData();
	CString strData = m_strMask1.Right(1);
	long uHex = strtol(strData, (char **)NULL, 16);
	if((uHex == 0 && strData != "0") || uHex > 0x0F)
	{
		//AfxMessageBox("Hex값만 입력 가능합니다.");
		m_strMask1.Delete(m_strMask1.GetLength()-1, 1);
		UpdateData(FALSE);
		return;
	}
*/	
	
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit3() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit4() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit5() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit6() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit7() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit8() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEdit9() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask1() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask1Filter1()
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask1Filter2() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask2() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask2Filter1() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask2Filter2() 
{
	IsChange = TRUE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask2Filter3() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeSlaveMask2Filter4() 
{
	IsChange = TRUE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);
	
}

void CSettingCanFdDlg::OnChangeEditSlaveCanid() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnChangeEditSlaveCv() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnCheckExtMode() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::OnCheckExtModeSlave() 
{
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg::IsChangeFlag(BOOL IsFlag)
{
	IsChange = IsFlag;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);	
/*#ifdef NDEBUG  //ksj 20210115 : 주석처리함
 	if(pMD == NULL || pMD->GetState() == PS_STATE_LINE_OFF)	
 	{
 		return;
 	}
 	else
#endif NDEBUG
	{
		GetDlgItem(IDOK)->EnableWindow(IsChange);
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}*/

	//ksj 20210115 : SAVE 버튼만 활성화
	if(pMD == NULL || pMD->GetState() == PS_STATE_LINE_OFF)	
	{
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}
	else
	{
		GetDlgItem(IDOK)->EnableWindow(IsChange);
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}
}

void CSettingCanFdDlg::EnableMaskFilter(BOOL bFlag, int nType)
{
	if(nType == PS_CAN_TYPE_MASTER)
	{
//		GetDlgItem(IDC_EDIT1)->EnableWindow(!bFlag);		
		GetDlgItem(IDC_EDIT2)->EnableWindow(bFlag);		
		GetDlgItem(IDC_EDIT3)->EnableWindow(bFlag);		
		GetDlgItem(IDC_EDIT4)->EnableWindow(bFlag);		
		GetDlgItem(IDC_EDIT5)->EnableWindow(bFlag);		
		GetDlgItem(IDC_EDIT6)->EnableWindow(bFlag);		
		GetDlgItem(IDC_EDIT7)->EnableWindow(bFlag);		
		GetDlgItem(IDC_EDIT8)->EnableWindow(bFlag);		
		GetDlgItem(IDC_EDIT9)->EnableWindow(bFlag);
	}
	else if(nType == PS_CAN_TYPE_SLAVE)
	{
		GetDlgItem(IDC_EDIT_SLAVE_CANID)->EnableWindow(!bFlag);		
		GetDlgItem(IDC_SLAVE_MASK1)->EnableWindow(bFlag);		
		GetDlgItem(IDC_SLAVE_MASK1_FILTER1)->EnableWindow(bFlag);
		GetDlgItem(IDC_SLAVE_MASK1_FILTER2)->EnableWindow(bFlag);		
		GetDlgItem(IDC_SLAVE_MASK2)->EnableWindow(bFlag);		
		GetDlgItem(IDC_SLAVE_MASK2_FILTER1)->EnableWindow(bFlag);		
		GetDlgItem(IDC_SLAVE_MASK2_FILTER2)->EnableWindow(bFlag);		
		GetDlgItem(IDC_SLAVE_MASK2_FILTER3)->EnableWindow(bFlag);		
		GetDlgItem(IDC_SLAVE_MASK2_FILTER4)->EnableWindow(bFlag);
	}
	
}

/*
---------------------------------------------------------
---------
-- Filename		: SettingCanFdDlg.cpp 
-- Description	: 
-- Author		: 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description

----------------------------------------------------------
*/
LRESULT CSettingCanFdDlg::OnLBGridClick(WPARAM wParam, LPARAM lParam)
{
	return 1;
}

/*
---------------------------------------------------------
---------
-- Filename		: SettingCanFdDlg.cpp 
-- Description	: 그리드 포커스 이동시 이벤트
-- Author		: 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description

  ----------------------------------------------------------
*/
LRESULT CSettingCanFdDlg::OnGridLeftCell(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	
	ROWCOL nCol = LOWORD(wParam); //컬럼 		
	ROWCOL nRow = HIWORD(wParam); //열	
	
	if(nRow < 0){
		return 0;
	}
	
	if(nCol == CAN_COL_DATATYPE){
		CString strData = pGrid ->GetValueRowCol(nRow,nCol);
		
		if(strData == "Hex" ||strData == "String"){						
			SetGridCanDivisionEnable(nMasterIdType,nRow,FALSE);			
		}else{
			SetGridCanDivisionEnable(nMasterIdType,nRow,TRUE);
		}
		
		return 1;
	}		
}
/*
---------------------------------------------------------
---------
-- Filename		: SettingCanFdDlg.cpp 
-- Description	: 
-- Author		: 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description

  ----------------------------------------------------------
*/
void CSettingCanFdDlg::SetGridCanDivisionEnable(int nMsterIdType,int nRow,BOOL bFlag){
	
	if(nMsterIdType == 0){
		m_wndCanGrid.SetStyleRange(CGXRange(nRow, CAN_COL_DIVISION1),CGXStyle().SetEnabled(bFlag));
		m_wndCanGrid.SetStyleRange(CGXRange(nRow, CAN_COL_DIVISION2),CGXStyle().SetEnabled(bFlag));
		m_wndCanGrid.SetStyleRange(CGXRange(nRow, CAN_COL_DIVISION3),CGXStyle().SetEnabled(bFlag));
	}else{		
		m_wndCanGridSlave.SetStyleRange(CGXRange(nRow, CAN_COL_DIVISION1),CGXStyle().SetEnabled(bFlag));
		m_wndCanGridSlave.SetStyleRange(CGXRange(nRow, CAN_COL_DIVISION2),CGXStyle().SetEnabled(bFlag));
		m_wndCanGridSlave.SetStyleRange(CGXRange(nRow, CAN_COL_DIVISION3),CGXStyle().SetEnabled(bFlag));
	}
}
/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	: 
-- Author		: 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description

  ----------------------------------------------------------
*/
LRESULT CSettingCanFdDlg::OnGridEndEditing(WPARAM wParam, LPARAM lParam)
{
	return 1;
}


LRESULT CSettingCanFdDlg::OnGridPaste(WPARAM wParam, LPARAM lParam)
{
	IsChangeFlag(TRUE);
	return 1;
}

void CSettingCanFdDlg::InitLabel()
{
	m_ctrlLabelMsg.SetTextColor(RGB(255, 0, 0))
					.FlashText(TRUE)
					.SetFontAlign(0)
					.SetFontName("HY헤드라인M");//yulee 20190405 check
					
	m_ctrlLabelChNo.SetTextColor(RGB(0, 0, 255))
					.SetFontName("HY헤드라인M");//yulee 20190405 check
	m_ctrlLabelChMsg.SetTextColor(RGB(0, 0, 0))
					.SetFontName("HY헤드라인M");//yulee 20190405 check
						
				
}

void CSettingCanFdDlg::EnableAllControl(BOOL IsEnable)
{
	//m_wndCanGrid.EnableWindow(IsEnable);
	//m_wndCanGridSlave.EnableWindow(IsEnable);
	m_wndCanGrid.EnableEdit(IsEnable);
	m_wndCanGridSlave.EnableEdit(IsEnable);

//	GetDlgItem(IDC_EDIT1)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_DATARATE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_CRC_M)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_BMS_TYPE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_EXT_MODE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_CAN_FD)->EnableWindow(IsEnable);
	GetDlgItem(IDC_EDIT10)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_USER_FILTER)->EnableWindow(IsEnable);
//	GetDlgItem(IDC_EDIT_SLAVE_CANID)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_BAUDRATE_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_CRC_S)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_BMS_TYPE_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_CAN_FD_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_EDIT_SLAVE_CV)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_USER_FILTER_SLAVE)->EnableWindow(IsEnable);

	GetDlgItem(IDC_BTN_ADD_MASTER)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_DEL_MASTER)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_ADD_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_DEL_SLAVE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_LOAD_CAN_SETUP)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_LOAD_CAN_DBC)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_LOAD_CAN_DBC_SLAVE)->EnableWindow(IsEnable);
	
	//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
	//if(!bUseCANFD)
	//{
	//	
	//}
	//else
	//{
	//	GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(TRUE);
	//}
}

void CSettingCanFdDlg::ReDrawMasterGrid()
{
	
	

	nBeforeMasterIdType = m_nMasterIDType;

	UpdateData();
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.01.23 이전 선택과 현재 선택이 같으면 종료

// 이것을 가지고 수정
// 	int nTabNum = m_ctrlParamTab.GetCurSel();
// 	TRACE("Tab %d Selected\n", nTabNum);
// 	
// 	switch(nTabNum)
// 	{
// 		
// 	case TAB_MASTER:		//안전 조건 
// 		Fun_ChangeViewControl(TRUE);
// 		break;
// 		
// 	case TAB_SLAVE:		//Grading조건
// 		Fun_ChangeViewControl(FALSE);
// 		break;
// 		
// 	default:	//TAB_RECORD_VAL	//Reporting 조건
// 		Fun_ChangeViewControl(TRUE);
// 	}	
	

	if(m_nMasterIDType == 1 && nBeforeMasterIdType == 1 )
		return;

	if(m_nMasterIDType == 0 && nBeforeMasterIdType == 0 )
		return;
	
	//TRACE("ing...\n");
	// -
	//////////////////////////////////////////////////////////////////////////
	
	if(m_nMasterIDType == 1)
	{
		nBeforeMasterIdType = 1;
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	}
	else
	{
		nBeforeMasterIdType = 0;
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
	}

	int i = 0;
	CString strID, strTemp;
	for(i = 1; i <= m_wndCanGrid.GetRowCount(); i++)
	{
		strTemp = m_wndCanGrid.GetValueRowCol(i, CAN_COL_ID);
		if(m_nMasterIDType == 0)
		{
			strID.Format("%X", strtoul(strTemp , NULL, 10));
		}
		else
		{
			//strID.Format("%d", strtoul(strTemp , NULL, 16));
			strID.Format("%u", strtoul(strTemp , NULL, 16)); //ksj 20200323
		}
		m_wndCanGrid.SetValueRange(CGXRange(i, CAN_COL_ID), strID);
	}

	for(i = 1; i <= m_wndCanGridSlave.GetRowCount(); i++)
	{
		strTemp = m_wndCanGridSlave.GetValueRowCol(i, CAN_COL_ID);
		if(m_nMasterIDType == 0)
		{
			strID.Format("%X", strtoul(strTemp , NULL, 10));
		}
		else
		{
			//strID.Format("%d", strtoul(strTemp , NULL, 16));
			strID.Format("%u", strtoul(strTemp , NULL, 16)); //ksj 20200323
		}
		m_wndCanGridSlave.SetValueRange(CGXRange(i, CAN_COL_ID), strID);
	}
}



void CSettingCanFdDlg::OnRadio3() 
{
	ReDrawMasterGrid();
}

void CSettingCanFdDlg::OnRadio4() 
{
	ReDrawMasterGrid();
}



void CSettingCanFdDlg::InitParamTab()
{
	m_ctrlParamTab.ModifyStyle(TCS_BOTTOM|TCS_MULTILINE|TCS_VERTICAL|TCS_BUTTONS, TCS_OWNERDRAWFIXED|TCS_FIXEDWIDTH);

	TC_ITEM item;
	//item.mask = TCIF_TEXT|TCIF_IMAGE;
	//item.iImage = 0;
	item.mask = TCIF_TEXT;	
	item.pszText = _T("Master");
	m_ctrlParamTab.InsertItem(TAB_MASTER, &item);

	//2014.07.07 슬레이브 사용유무
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);	
	if(isSlave == TRUE){
		item.pszText = "Slave";	
		m_ctrlParamTab.InsertItem(TAB_SLAVE, &item);
	}
	m_ctrlParamTab.SetColor(RGB(0,0,0), RGB(255,255,255), RGB(240,240,240), RGB(240,240,240));	// 글자색, 바깥쪽 라인, 내부 색깔, 이미지쪽 라인

	Fun_ChangeSizeControl(IDC_BTN_ADD_MASTER,IDC_BTN_ADD_SLAVE);
	Fun_ChangeSizeControl(IDC_BTN_DEL_MASTER,IDC_BTN_DEL_SLAVE);
	Fun_ChangeSizeControl(IDC_CAN_GRID,IDC_CAN_GRID_SLAVE);
//	Fun_ChangeSizeControl(IDC_EDIT1,IDC_EDIT_SLAVE_CANID);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BAUDRATE,IDC_COMBO_CAN_BAUDRATE_SLAVE);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_DATARATE,IDC_COMBO_CAN_DATARATE_SLAVE);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_TERMINAL_M,IDC_COMBO_CAN_TERMINAL_S);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_CRC_M,IDC_COMBO_CAN_CRC_S);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BMS_TYPE,IDC_COMBO_CAN_BMS_TYPE_SLAVE);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_SJW,IDC_COMBO_CAN_SJW_SLAVE);
	Fun_ChangeSizeControl(IDC_CHECK_EXT_MODE,IDC_CHECK_EXT_MODE_SLAVE);
	Fun_ChangeSizeControl(IDC_CHECK_CAN_FD,IDC_CHECK_CAN_FD_SLAVE);
	Fun_ChangeSizeControl(IDC_CHECK_USER_FILTER,IDC_CHECK_USER_FILTER_SLAVE);
	Fun_ChangeSizeControl(IDC_EDIT2,IDC_SLAVE_MASK1);
	Fun_ChangeSizeControl(IDC_EDIT3,IDC_SLAVE_MASK1_FILTER1);
	Fun_ChangeSizeControl(IDC_EDIT4,IDC_SLAVE_MASK1_FILTER2);
	Fun_ChangeSizeControl(IDC_EDIT5,IDC_SLAVE_MASK2);
	Fun_ChangeSizeControl(IDC_EDIT6,IDC_SLAVE_MASK2_FILTER1);
	Fun_ChangeSizeControl(IDC_EDIT7,IDC_SLAVE_MASK2_FILTER2);
	Fun_ChangeSizeControl(IDC_EDIT8,IDC_SLAVE_MASK2_FILTER3);
	Fun_ChangeSizeControl(IDC_EDIT9,IDC_SLAVE_MASK2_FILTER4);
	Fun_ChangeSizeControl(IDC_BTN_LOAD_CAN_DBC,IDC_BTN_LOAD_CAN_DBC_SLAVE);

	//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
	//if(!bUseCANFD)
	//{
	//	
	//}
	//else
	//{
	//	GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	//}
}

void CSettingCanFdDlg::OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nTabNum = m_ctrlParamTab.GetCurSel();
	TRACE("Tab %d Selected\n", nTabNum);

	switch(nTabNum)
	{

	case TAB_MASTER:		//안전 조건 
		Fun_ChangeViewControl(TRUE);
		nMasterIdType = 0;
		break;

	case TAB_SLAVE:		//Grading조건
		Fun_ChangeViewControl(FALSE);
		nMasterIdType = 1;
		break;
		
	//default:	//TAB_RECORD_VAL	//Reporting 조건
	//	Fun_ChangeViewControl(TRUE);
	}	


	*pResult = 0;
}

void CSettingCanFdDlg::Fun_ChangeViewControl(BOOL bFlag)
{
	if(bFlag)
	{
		//GetDlgItem(IDC_FRAME_SETTING)->SetWindowText("Master 설정");
		GetDlgItem(IDC_FRAME_SETTING)->SetWindowText(Fun_FindMsg("SettingCanFdDlg_Fun_ChangeViewControl_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
		GetDlgItem(IDC_BTN_ADD_MASTER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DEL_MASTER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAN_GRID)->ShowWindow(SW_SHOW);
//		GetDlgItem(IDC_EDIT1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_DATARATE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_CRC_M)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_SJW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_EXT_MODE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_CAN_FD)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_USER_FILTER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT5)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT6)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT7)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT8)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT9)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_LOAD_CAN_DBC)->ShowWindow(SW_SHOW);
// 		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_ADD_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_DEL_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAN_GRID_SLAVE)->ShowWindow(SW_HIDE);
//		GetDlgItem(IDC_EDIT_SLAVE_CANID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_CRC_S)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_CAN_FD_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_USER_FILTER_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK1_FILTER1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK1_FILTER2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_LOAD_CAN_DBC_SLAVE)->ShowWindow(SW_HIDE);

		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(!bUseCANFD)
		//{
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//}
	}
	else
	{
		//GetDlgItem(IDC_FRAME_SETTING)->SetWindowText("Slave 설정");
		GetDlgItem(IDC_FRAME_SETTING)->SetWindowText(Fun_FindMsg("SettingCanFdDlg_Fun_ChangeViewControl_msg2","IDD_CAN_FD_SETTING_DLG"));//&&
		GetDlgItem(IDC_BTN_ADD_MASTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_DEL_MASTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAN_GRID)->ShowWindow(SW_HIDE);
//		GetDlgItem(IDC_EDIT1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_DATARATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_CRC_M)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_SJW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_EXT_MODE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_CAN_FD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_USER_FILTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT6)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT7)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT8)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT9)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_LOAD_CAN_DBC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_ADD_SLAVE)->ShowWindow(SW_SHOW);

		GetDlgItem(IDC_BTN_ADD_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DEL_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAN_GRID_SLAVE)->ShowWindow(SW_SHOW);
//		GetDlgItem(IDC_EDIT_SLAVE_CANID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_CRC_S)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_CAN_FD_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_USER_FILTER_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK1_FILTER1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK1_FILTER2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SLAVE_MASK2_FILTER4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_LOAD_CAN_DBC_SLAVE)->ShowWindow(SW_SHOW);

		//AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//
		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(bUseCANFD)
		//{
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//}
	}
}

void CSettingCanFdDlg::Fun_ChangeSizeControl(UINT uiMaster, UINT uiSlave)
{
	CRect	rect;
	GetDlgItem(uiMaster)->GetWindowRect(rect);
	ScreenToClient(&rect);
	GetDlgItem(uiSlave)->SetWindowPos(NULL,rect.left,rect.top,rect.Width(),rect.Height(),SWP_HIDEWINDOW);

}

CString CSettingCanFdDlg::Fun_FindCanName(int nDivision)
{
	CString strName;
	int nSelectPos, nFindPos;
	for (int i=0; i < m_uiArryCanCode.GetSize() ; i++)
	{
		if (nDivision == m_uiArryCanCode.GetAt(i))
		{
			strName.Format("%s [%d]", m_strArryCanName.GetAt(i), nDivision);
			return strName;
		}
	}
	strName.Format("None [%d]", nDivision);
	return strName;
}

void CSettingCanFdDlg::OnButNotUse() 
{
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return;
	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END	)
	{
		//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnButNotUse_msg1","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}


	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_DATA commonData;
	ZeroMemory(&commonData, sizeof(SFT_CAN_COMMON_DATA));
	
	for(int i=0;i<2;i++) //lyj 20200722 can clear 시 0이 아닌 default 값으로 내리기
	{
		canSetData.canCommonData[i].can_fd_flag = 0 ;
		canSetData.canCommonData[i].can_datarate = 0; 
		canSetData.canCommonData[i].terminal_r = 1; 
		canSetData.canCommonData[i].crc_type = 1;
	}

	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_MASTER);
	Sleep(200);
	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_SLAVE);
	Sleep(200);

	SFT_CAN_SET_DATA canData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);

	pMD->SetCANSetData(nSelCh, canData, 0);

		//AfxMessageBox("All setting is clear. When you open this window, every can data will be cleared");

	//TRACE("CAN commdata Size = %d, size data = %d, total size = %d \n",sizeof(canSetData.canCommonData[0]),sizeof(canSetData.canSetData[0]), sizeof(canSetData));
	if(int nRth = m_pDoc->SetCANDataToModule(nSelModule, &canSetData, nSelCh) == FALSE)
	{
		//AfxMessageBox("CAN 정보 설정에 실패했습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_OnButNotUse_msg2","IDD_CAN_FD_SETTING_DLG"));//&&
		return;
	}

	pMD->SaveCanConfig();

}

void CSettingCanFdDlg::OnBtnLoadCanDbc() 
{
	CString strTemp;
	strTemp.Format("dbc file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"dbc", "dbc");
	
	CFileDialog pDlg(TRUE, "dbc", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "CAN DBC file";
	if(IDOK == pDlg.DoModal())
	{
		if(m_wndCanGrid.GetRowCount() > 0)
			m_wndCanGrid.RemoveRows(1, m_wndCanGrid.GetRowCount());
		if(m_wndCanGridSlave.GetRowCount() > 0)
			m_wndCanGridSlave.RemoveRows(1, m_wndCanGridSlave.GetRowCount());
		if(LoadCANDbcConfig2(pDlg.GetPathName()) == TRUE)
		{
			CString strName;
			//strName.Format("CAN 설정 - %s",  pDlg.GetFileName());
			strName.Format(Fun_FindMsg("SettingCanFdDlg_OnBtnLoadCanDbc_msg1","IDD_CAN_FD_SETTING_DLG"),  pDlg.GetFileName());//&&
			this->SetWindowText(strName);
			
			IsChangeFlag(TRUE);
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}	
}

void CSettingCanFdDlg::Fun_ConvertData(CString strSignal, S_CAN_DBC_DATA *sCanData)
{
	int nIndex=0, nIndex2=0, nPos=0, nPos2=0;
	CString strTemp;

	nIndex = strSignal.Find(",",nPos);
	strTemp = strSignal.Left(nIndex);	//CAN ID
	//sCanData->can_id = atol(strTemp);
	sCanData->uiCan_id = strtoul(strTemp,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.

	strSignal = strSignal.Right(strSignal.GetLength()-(nIndex+1));
	nIndex = strSignal.Find(":",nPos);
	strTemp = strSignal.Left(nIndex);	//name
	sprintf(sCanData->signal_name,"%s", strTemp); 

	strSignal = strSignal.Right(strSignal.GetLength()-(nIndex+1));
	nIndex = strSignal.Find("|",nPos);
	strTemp = strSignal.Left(nIndex);	//start bit
	sCanData->startBit = atol(strTemp);
	
	strSignal = strSignal.Right(strSignal.GetLength()-(nIndex+1));
	nIndex = strSignal.Find("@",nPos);
	strTemp = strSignal.Left(nIndex);	//length
	sCanData->length = atol(strTemp);

	strSignal = strSignal.Right(strSignal.GetLength()-(nIndex+1));
	strTemp = strSignal.Left(1);	//byte order
	sCanData->byteOrder = atol(strTemp);	//0 : Motorola, 1: intel
	
	int iDivide1=0, iDivide2=0, iAddValue=0;
	if (strTemp == "0")	//Motorola
	{
		//sanData->startBit = sCanData->startBit - sCanData->length + 1;	//모토로라 방식 으로 변환 해야 함.
		//ljb 20180509 add start ///////////////////////////////////////
		if (sCanData->length >= 8)
		{
			iDivide1 = sCanData->length % 8;
			if (iDivide1 == 0)
			{
				iDivide1 = sCanData->length / 8;
				if (iDivide1 < 2) 
					iDivide2 = sCanData->length - 8;
				else 
					iDivide2 = (iDivide1-1) * 8;
					//iDivide2 = sCanData->length - ((iDivide1-1) * 8);
				sCanData->startBit = sCanData->startBit + iDivide2 - 8 +1;
			}
			else
			{
				iDivide1 = sCanData->length / 8;
				iDivide2 = iDivide1 * 8;
				iAddValue = sCanData->length - iDivide2;
				sCanData->startBit = sCanData->startBit + iDivide2 - iAddValue +1;
			}
		}
		else
		{
			//length 가 8보다 작다
			sCanData->startBit = sCanData->startBit - sCanData->length +1;
		}
		//ljb 20180509 add end ////////////////////////////////////////
	}
	
	strSignal = strSignal.Right(strSignal.GetLength()-1);
	strTemp = strSignal.Left(1);	//value type
	if (strTemp == "+") sCanData->valueType = 1;  //unsigned
	else sCanData->valueType = 0;  //signed
	
	strSignal = strSignal.Right(strSignal.GetLength()-2);
	nIndex = strSignal.Find(",",nPos);
	strTemp = strSignal.Left(nIndex);	//Factor
	sCanData->factor = atof(strTemp);
	
	strSignal = strSignal.Right(strSignal.GetLength()-(nIndex+1));
	nIndex = strSignal.Find(")",nPos);
	strTemp = strSignal.Left(nIndex);	//Offset
	sCanData->offset = atof(strTemp);
}

void CSettingCanFdDlg::OnCheckCanFd() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	if (m_ckCanFD)
	{
		GetDlgItem(IDC_COMBO_CAN_DATARATE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_CRC_M)->EnableWindow(TRUE);
	
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		if(bUseCANFD)
		{
			
		}
		else
		{
			/*GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
					GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);*/
		}
	}
	else
	{
		m_ctrlDataRate.SetCurSel(0); //lyj 20200228 can fd 체크 해제 시 deafult 값
		m_ctrlBmsTerminal.SetCurSel(1); //lyj 20200228 can fd 체크 해제 시 deafult 값
		m_ctrlBmsCrcType.SetCurSel(1); //lyj 20200228 can fd 체크 해제 시 deafult 값

		GetDlgItem(IDC_COMBO_CAN_DATARATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_SJW)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_M)->EnableWindow(TRUE); //20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
		GetDlgItem(IDC_COMBO_CAN_CRC_M)->EnableWindow(FALSE);
	
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		if(bUseCANFD)
		{
			
		}
		else
		{
			/*GetDlgItem(IDC_CHECK_CAN_FD			)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_DATARATE   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_CRC_M      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
					GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);*/
		}
	}
	IsChangeFlag(TRUE);

	UpdateData(FALSE);
}

void CSettingCanFdDlg::OnCheckCanFdSlave() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	
	if (m_ckCanFD_Slave) 
	{
		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_CRC_S)->EnableWindow(TRUE);
	
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		if(bUseCANFD)
		{
		}
		else
		{
			/*GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
					GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);*/
		}
	}
	else
	{
		m_ctrlDataRateSlave.SetCurSel(0); //lyj 20200228 can fd 체크 해제 시 deafult 값
		m_ctrlBmsTerminalSlave.SetCurSel(1); //lyj 20200228 can fd 체크 해제 시 deafult 값
		m_ctrlBmsCrcTypeSlave.SetCurSel(1); //lyj 20200228 can fd 체크 해제 시 deafult 값

		GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_SJW_SLAVE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_TERMINAL_S)->EnableWindow(TRUE); //20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
		GetDlgItem(IDC_COMBO_CAN_CRC_S)->EnableWindow(FALSE); 
	
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		if(bUseCANFD)
		{
		}
		else
		{
			/*GetDlgItem(IDC_CHECK_CAN_FD_SLAVE        )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_DATARATE_SLAVE  )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_TERMINAL_S      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_CAN_CRC_S           )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_COMBO_CAN_DATARATE)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
					GetDlgItem(IDC_STATIC_CAN_CRC_M        )->ShowWindow(bUseCANFD);*/
		}
	}
			IsChangeFlag(TRUE);
	
	UpdateData(FALSE);	
}

void CSettingCanFdDlg::OnSelchangeComboCanDatarate() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanTerminalM() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanCrcM() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanSjwSlave() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanBmsTypeSlave() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanDatarateSlave() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanTerminalS() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanCrcS() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg::OnSelchangeComboCanSjw() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);	
}

void CSettingCanFdDlg::OnSelchangeComboCanBmsType() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);	
}

void CSettingCanFdDlg::OnBtnLoadCanDbcSlave() 
{
	CString strTemp;
	strTemp.Format("dbc file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"dbc", "dbc");
	
	CFileDialog pDlg(TRUE, "dbc", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "CAN DBC file";
	if(IDOK == pDlg.DoModal())
	{
		if(m_wndCanGrid.GetRowCount() > 0)
			m_wndCanGrid.RemoveRows(1, m_wndCanGrid.GetRowCount());
		if(m_wndCanGridSlave.GetRowCount() > 0)
			m_wndCanGridSlave.RemoveRows(1, m_wndCanGridSlave.GetRowCount());
		if(LoadCANDbcConfig2Slave(pDlg.GetPathName()) == TRUE)
		{
			CString strName;
			//strName.Format("CAN 설정 - %s",  pDlg.GetFileName());
			strName.Format(Fun_FindMsg("SettingCanFdDlg_OnBtnLoadCanDbc_msg1","IDD_CAN_FD_SETTING_DLG"),  pDlg.GetFileName());//&&
			this->SetWindowText(strName);
			
			IsChangeFlag(TRUE);
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}	
}

void CSettingCanFdDlg::OnSysCommand(UINT nID, LPARAM lParam) //lyj 20200317 CanFDDlg 최대화
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	/*if(nID == SC_MAXIMIZE || nID == 0xF012) //0xF012 창더블클릭이벤트
	{
		CRect rect;
		
		//m_ctrlParamTab.GetClientRect(&rect);
		m_ctrlParamTab.GetWindowRect(&rect);
		m_ctrlParamTab.MoveWindow(0,41,rect.Width()+600,rect.Height()+300,TRUE);

		m_ctrlFrameSetting.GetWindowRect(&rect);
		m_ctrlFrameSetting.MoveWindow(8,200,rect.Width()+600,rect.Height()+300,TRUE);

		m_wndCanGrid.GetWindowRect(&rect);
		m_wndCanGrid.MoveWindow(13,255,rect.Width()+620,rect.Height()+300);

		m_wndCanGridSlave.GetWindowRect(&rect);
		m_wndCanGridSlave.MoveWindow(13,255,rect.Width()+620,rect.Height()+300);

	}
	if(nID == SC_RESTORE || nID == 0xF122) //0xF012 창더블클릭이벤트
	{
		CRect rect;

		m_ctrlParamTab.GetWindowRect(&rect);
		m_ctrlParamTab.MoveWindow(0,41,rect.Width()-600,rect.Height()-300,TRUE);

		m_ctrlFrameSetting.GetWindowRect(&rect);
		m_ctrlFrameSetting.MoveWindow(8,200,rect.Width()-600,rect.Height()-300,TRUE);

		m_wndCanGrid.GetWindowRect(&rect);
		m_wndCanGrid.MoveWindow(13,255,rect.Width()-620,rect.Height()-300);

		m_wndCanGridSlave.GetWindowRect(&rect);
		m_wndCanGridSlave.MoveWindow(13,255,rect.Width()-620,rect.Height()-300);
	}*/

	CDialog::OnSysCommand(nID, lParam);
}


//ksj 20200325 : 최대화 처리 추가. 기존 OnSysCommand 처리 방식이 매끄럽지 않아 수정.
void CSettingCanFdDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	//CRect rect;
	CRect GetMainRect;

	GetClientRect(&GetMainRect);
	TRACE("%d, %d",GetMainRect.Width(),GetMainRect.Height());

	/*SIZE s;
	ZeroMemory(&s, sizeof(SIZE));
	s.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN); //스크린 사이즈 x
	s.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN); //스크린 사이즈 y*/

	if(m_ctrlParamTab.GetSafeHwnd())
	{
		//m_ctrlParamTab.GetClientRect(&rect);
		m_ctrlParamTab.MoveWindow(0,41,GetMainRect.Width()-10,GetMainRect.Height()-50);
	}
	
	if(m_ctrlFrameSetting.GetSafeHwnd())
	{
		//m_ctrlFrameSetting.GetClientRect(&rect);
		m_ctrlFrameSetting.MoveWindow(8,200,GetMainRect.Width()-20,GetMainRect.Height()-210);
	}

	if(m_wndCanGrid.GetSafeHwnd())
	{
		//m_wndCanGrid.GetClientRect(&rect);
		m_wndCanGrid.MoveWindow(13,255,GetMainRect.Width()-25,GetMainRect.Height()-265);
	}

	if(m_wndCanGridSlave.GetSafeHwnd())
	{
		//m_wndCanGridSlave.GetClientRect(&rect);
		m_wndCanGridSlave.MoveWindow(13,255,GetMainRect.Width()-25,GetMainRect.Height()-265);
	}
}


void CSettingCanFdDlg::OnCbnEditchangeComboCanSjw()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	IsChangeFlag(TRUE);
}


void CSettingCanFdDlg::OnCbnEditchangeComboCanBmsType()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	IsChangeFlag(TRUE);
}
