#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "afxdialogex.h"

// CDlgConnectionSequence 대화 상자입니다.
typedef BOOL (WINAPI *lpfnSetLayeredWindowAttributes)(HWND hWnd, COLORREF crKey, BYTE bAlpha, DWORD dwFlags);

class CDlgConnectionSequence : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgConnectionSequence)

public:
	CDlgConnectionSequence(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlgConnectionSequence();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_CONNECTION_SEQUENCE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CLabel m_labText;
	CListBox m_listLog;
	CProgressCtrl m_ctrlProgress;
	virtual BOOL OnInitDialog();
	int SetText(CString strText);
	int AddLog(CString strLog);
	int SetFlashText(BOOL bActivate);	
	int SetPos(int nPos);
	void ResetTransparent();

	lpfnSetLayeredWindowAttributes m_pSetLayeredWindowAttributes;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	int m_nTranspCount;
};
