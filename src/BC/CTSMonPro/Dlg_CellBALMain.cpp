// Dlg_CellBALMain.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "Dlg_CellBALMain.h"

#include "MainFrm.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"

#include "Global.h"
#include "Dlg_CellBALLoadModeSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALMain dialog


Dlg_CellBALMain::Dlg_CellBALMain(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_CellBALMain::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_CellBALMain::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_CellBALMain::IDD3):
	(Dlg_CellBALMain::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_CellBALMain)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_hSbcDealThread=NULL;
	m_iSelectCH=0;
	m_lpSelectChInfo=NULL;
}


void Dlg_CellBALMain::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_CellBALMain)
	DDX_Control(pDX, IDC_LIST_CELLBAL_CH, m_ListCellBalCH);
	DDX_Control(pDX, IDC_LIST_CELLBAL_LOG, m_ListCellBalLog);
	DDX_Control(pDX, IDC_LIST_CELLBAL, m_ListCellBal);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_CellBALMain, CDialog)
	//{{AFX_MSG_MAP(Dlg_CellBALMain)
	ON_WM_CLOSE()
	ON_MESSAGE(UM_LISTTEXT,OnListEditText)	
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUT_CELLBAL_CONN, OnButCellbalConn)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_DISCON, OnButCellbalDiscon)
	ON_LBN_DBLCLK(IDC_LIST_CELLBAL_LOG, OnDblclkListCellbalLog)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_TEST, OnButCellbalTest)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_STOP, OnButCellbalStop)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CELLBAL, OnDblclkListCellbal)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_MODINFOREQ, OnButCellbalModinforeq)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_RESET, OnButCellbalReset)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_OPMODESET, OnButCellbalOpmodeset)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_LOADMODSET, OnButCellbalLoadmodset)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_RUN, OnButCellbalRun)
	ON_BN_CLICKED(IDC_BUT_CELLBALCH_RUN, OnButCellbalchRun)
	ON_BN_CLICKED(IDC_BUT_CELLBALCH_STOP, OnButCellbalchStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALMain message handlers

BOOL Dlg_CellBALMain::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mainDlgCellBalMain=this;
	
	onInitObject();
	
	DWORD hThreadId=0;	
	m_hSbcDealThread = CreateThread(NULL, 0, SbcDealThread,
		            (LPVOID)this, 0, &hThreadId);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void Dlg_CellBALMain::OnOK() 
{
	//CDialog::OnOK();
}

void Dlg_CellBALMain::OnCancel() 
{
	//CDialog::OnCancel();
}

void Dlg_CellBALMain::OnClose() 
{
	//CDialog::OnClose();
}

BOOL Dlg_CellBALMain::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP)
	{
		if(pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_RETURN)
		{					  
			return FALSE;	
		}					
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_CellBALMain::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	CRect wrect,rect;
	GetClientRect(wrect);
	wrect.DeflateRect(0,50,0,0);

	if(IsWindow(m_ListCellBal.m_hWnd))
	{				
		rect=wrect;
		rect.bottom=rect.bottom/2;
		rect.right=rect.right*2/3;
		m_ListCellBal.MoveWindow(rect,TRUE);

		rect=wrect;
		rect.top=rect.bottom/2;
		m_ListCellBalCH.MoveWindow(rect,TRUE);

		rect=wrect;
		rect.left=rect.right*2/3;
		rect.bottom=rect.bottom/2;
		m_ListCellBalLog.MoveWindow(rect,TRUE);
	}
}

void Dlg_CellBALMain::onInitObject() 
{
	//----------------------------------
	CString list[ID_LIST_CELLBAL_COUNT]={
	_T("No"),_T("MODULE"),_T("CH"),
	_T("IP"),_T("PORT"),_T("USE"),_T("ConnState"),
	_T("op_state"),_T("op_mode"),_T("module_installed_ch"),
	_T("module_run_time"),_T("internal_temp"),_T("load_mode_set"),
	_T("reserved2"),_T("load_mode_value"),_T("active_time"),_T("reserved3"),
	_T("availability_voltage_upper"),_T("under_voltage"),_T("over_current"),
	_T("over_temp"),_T("availability_voltage_lower"),_T("ch_cond_end_current"),
	_T("ch_cond_end_time"),_T("ch_cond_detection"),_T("ch_cond_release"),
	_T("ch_cond_auto_stop_time"),_T("DateTime") };

	int width[ID_LIST_CELLBAL_COUNT] = {
	50,80,40,
	100,0,50,80,
	70,70,150,
	120,120,120,
	120,120,120,120,
	120,120,120,
	120,120,150,
	150,150,150,
	180,150};
	GList::list_Init(1,&m_ListCellBal,ID_LIST_CELLBAL_COUNT,list,width);
	m_ListCellBal.SetEditMode(ID_LIST_CELLBAL_IP,1);//IP
	m_ListCellBal.SetEditMode(ID_LIST_CELLBAL_PORT,1);//PORT
	m_ListCellBal.SetEditMode(ID_LIST_CELLBAL_USE,3,_T("0|1|"));//USE
	m_ListCellBal.m_bUserMode=TRUE;
	//--------------------------------------

	CString listch[ID_LIST_CELLBALCH_COUNT]={
	_T("No"),_T("CH"),
	_T("ch_state"),_T("reserved1"),_T("voltage"),
	_T("current"),_T("run_time"),_T("reserved2"),
	_T("reserved3"),_T("DateTime")};

	int widthch[ID_LIST_CELLBALCH_COUNT] = {
	40,40,
	70,100,100,
	100,100,100,
	100,150 };
	GList::list_Init(1,&m_ListCellBalCH,ID_LIST_CELLBALCH_COUNT,listch,widthch);

	//--------------------------------------
	onSbc_Deal(ID_CMD_SBC_CH_SETINFO);
	onSbc_Deal(ID_CMD_CELLBAL_LIST_INIT);
}

void Dlg_CellBALMain::onLog(CString strlog,CString stype)
{	
	if(strlog.IsEmpty() || strlog==_T("")) return;
	
	strlog.Replace(_T("\r"),_T(""));
	strlog.Replace(_T("\n"),_T(""));
	
	CString strTime=GWin::win_CurrentTime(_T("fulllogm"));
	CString stp=GStr::str_Blank(stype,10,_T(""));
	
	CString strTemp;
	strTemp.Format(_T("%s %s %s"),
		            strTime,
					stp,
		            strlog);	
	m_ListCellBalLog.AddString(strTemp);
	GWin::win_SetListBoxHori(&m_ListCellBalLog,strTemp);
}

LRESULT Dlg_CellBALMain::OnListEditText(WPARAM wParam,LPARAM lParam)
{	
	LV_DISPINFO *dispinfo=(LV_DISPINFO*)lParam;
	if(!dispinfo) return 0;
	
	int nRow=dispinfo->item.iItem;		
	if(nRow<0) return 0;
	
	int nCol=dispinfo->item.iSubItem;	
	
	if((int)wParam==m_ListCellBal.GetDlgCtrlID())
	{
		if(nCol==ID_LIST_CELLBAL_IP)
		{//IP
			if(onSbc_Deal(ID_CMD_CELLBAL_LIST_SETDATA,nRow,nCol)==FALSE)
			{
				m_ListCellBal.SetItemText(nRow,nCol,m_ListCellBal.DefaultText);
			}
		}
		else if(nCol==ID_LIST_CELLBAL_PORT)
		{//PORT
			if(onSbc_Deal(ID_CMD_CELLBAL_LIST_SETDATA,nRow,nCol)==FALSE)
			{
				m_ListCellBal.SetItemText(nRow,nCol,m_ListCellBal.DefaultText);
			}
		}
		else if(nCol==ID_LIST_CELLBAL_USE)
		{//USE
			CString USE=m_ListCellBal.GetItemText(nRow,nCol);

			if(onSbc_Deal(ID_CMD_CELLBAL_LIST_SETDATA,nRow,nCol)==FALSE)
			{
				if(_ttoi(USE)==1)
				{
					int nCount=onSbc_Deal(ID_CMD_CELLBAL_USE_COUNT);
					if(nCount>MAX_CLIENT_COUNT)
					{
						MSGWARN(this->m_hWnd,MSG_MAXCLIENT,TITLE_WARN);
						
						USE=_T("0");
						m_ListCellBal.SetItemText(nRow,nCol,USE);
						onSbc_Deal(3,nRow,nCol);
					}
				}
			}
		}
	}
	return 1;
}

CString Dlg_CellBALMain::onConnState(int iStat)
{	
	if(iStat==ID_SOCKET_STAT_CONN) return _T("Connect");
	
	return _T("Disconn");
}

int Dlg_CellBALMain::onSbc_Deal(int iType,int nRow,int nCol,CString strVal)
{
	if( iType==ID_CMD_CELLBAL_LIST_INIT)
	{
		m_ListCellBal.DeleteAllItems();
	}	

	S_P1_CMD_BODY_LOAD_MODE_SET cmdBodyLoadModeSet;
	memset(&cmdBodyLoadModeSet,0,sizeof(S_P1_CMD_BODY_LOAD_MODE_SET));

    if(iType==ID_CMD_TO_LOAD_MODE_SET)
	{
		//------------------------------------------
		m_lpSelectChInfo=NULL;
		onSbc_Deal(ID_CMD_CELLBAL_GETCHANNEL,nRow);

		int iState_Conn=ID_SOCKET_STAT_NONE;
		if(m_lpSelectChInfo)
		{
            iState_Conn=m_lpSelectChInfo->m_CtrlClient.m_iState;
			mainGlobal.onLoadModeCopy(cmdBodyLoadModeSet,
					                  m_lpSelectChInfo->m_CtrlClient.m_cmdBodyModuleInfoReply);
		}
		//------------------------------------------
		Dlg_CellBALLoadModeSet dlg;
		dlg.m_iState_Conn=iState_Conn;
		dlg.m_cmdBodyLoadModeSet=&cmdBodyLoadModeSet;
		if(dlg.DoModal()!=IDOK) return FALSE;
	}
	
	if(!mainView) return 0;
	CCTSMonProDoc *pDoc = mainView->GetDocument();
	if(!pDoc)  return 0;
	
	CCyclerModule *pMD=NULL;
	CCyclerChannel *lpChannelInfo=NULL;
	
	int nCount=0;
	int nColumn=m_ListCellBal.GetColumnCount();
	int nColumnCH=m_ListCellBalCH.GetColumnCount();
	CString strTemp,strTemp2;
	
	for ( UINT nI = 0; nI < (UINT)pDoc->GetInstallModuleCount(); nI++ )
	{
		pMD = pDoc->GetModuleInfo(pDoc->GetModuleID(nI));
		if(pMD == NULL) continue;
		
		int nModuleID=pMD->GetModuleID();
		CString ModuleNO=GStr::str_IntToStr(nModuleID);
		
		for(UINT ch =0; ch<pMD->GetTotalChannel(); ch++)
		{
			lpChannelInfo = pMD->GetChannelInfo(ch);
			if(lpChannelInfo == NULL) break;
		
			int nChannelIndex=lpChannelInfo->GetChannelIndex();
			int iCHNO=lpChannelInfo->m_iCh;
			CString CHNO=GStr::str_IntToStr(lpChannelInfo->m_iCh);

			if(iType==ID_CMD_SBC_CH_SETINFO)
			{//Channel Set Info
				CString IP   = mainGlobal.GetChInfo(iCHNO,_T("CELLBAL_IP"));
				CString PORT = mainGlobal.GetChInfo(iCHNO,_T("CELLBAL_PORT"));
				CString USE  = mainGlobal.GetChInfo(iCHNO,_T("CELLBAL_USE"));
				if(USE.IsEmpty() || USE==_T(""))
				{
					USE=_T("0");
				}
				if(IP.IsEmpty() || IP==_T(""))
				{
					if(iCHNO==1) IP=_T("192.168.9.120");
					if(iCHNO==2) IP=_T("192.168.9.121");
					if(iCHNO==3) IP=_T("192.168.9.122");
					if(iCHNO==4) IP=_T("192.168.9.123");
				}
				if(PORT.IsEmpty() || PORT==_T(""))
				{
					PORT=_T("1976");
				}				
				//----------------------------------------
				lpChannelInfo->m_CtrlClient.m_pWnd            = this;
				lpChannelInfo->m_CtrlClient.m_pChannel        = lpChannelInfo;
				lpChannelInfo->m_CtrlClient.m_strCellBal_IP   = IP;
				lpChannelInfo->m_CtrlClient.m_strCellBal_PORT = PORT;
				lpChannelInfo->m_CtrlClient.m_iCellBal_USE    = atoi(USE);
				lpChannelInfo->m_MainCtrl.m_pChannel          = lpChannelInfo;
				//----------------------------------------
				//mainGlobal.SetChInfo(iCHNO,_T("CELLBAL_IP"),IP);
				//mainGlobal.SetChInfo(iCHNO,_T("CELLBAL_PORT"),PORT);
				//mainGlobal.SetChInfo(iCHNO,_T("CELLBAL_USE"),USE);
                //----------------------------------------
				lpChannelInfo->m_CtrlClient.onDealThreadStart();
				lpChannelInfo->m_MainCtrl.onDealThreadStart();
			}
			else if(iType==ID_CMD_CELLBAL_LIST_INIT)
			{//CellBAL List Init
				int nCol=0;
				CString *listdata=new CString[nColumn];
				listdata[nCol++]=GStr::str_IntToStr(++nCount);
				listdata[nCol++]=ModuleNO;
				listdata[nCol++]=CHNO;
				listdata[nCol++]=lpChannelInfo->m_CtrlClient.m_strCellBal_IP;
				listdata[nCol++]=lpChannelInfo->m_CtrlClient.m_strCellBal_PORT;
				listdata[nCol++]=GStr::str_IntToStr(lpChannelInfo->m_CtrlClient.m_iCellBal_USE);
				listdata[nCol++]=onConnState(lpChannelInfo->m_CtrlClient.m_iState);

				GList::list_Insert(0,&m_ListCellBal,nColumn,listdata,FALSE);
				m_ListCellBal.SetItemData(0,MAKELONG(nChannelIndex, nModuleID));
			}
			else if(iType==ID_CMD_CELLBAL_LIST_SETDATA)
			{//CellBAL List SetData
				CString strCHNO=m_ListCellBal.GetItemText(nRow,ID_LIST_CELLBAL_CH);
				if(strCHNO!=CHNO) continue;

			    CString strData=m_ListCellBal.GetItemText(nRow,nCol);
				if(nCol==ID_LIST_CELLBAL_IP)
				{
					if(lpChannelInfo->m_CtrlClient.m_iState>ID_SOCKET_STAT_NONE)
					{
						MSGWARN(this->m_hWnd,MSG_EXISTCONN,TITLE_WARN);
						return TRUE;
					}
					lpChannelInfo->m_CtrlClient.m_strCellBal_IP = strData;
					mainGlobal.SetChInfo(iCHNO,_T("CELLBAL_IP"),strData);
					return TRUE;
				}
				else if(nCol==ID_LIST_CELLBAL_PORT)
				{
					if(lpChannelInfo->m_CtrlClient.m_iState>ID_SOCKET_STAT_NONE)
					{
						MSGWARN(this->m_hWnd,MSG_EXISTCONN,TITLE_WARN);
						return TRUE;
					}
					lpChannelInfo->m_CtrlClient.m_strCellBal_PORT = strData;
					mainGlobal.SetChInfo(iCHNO,_T("CELLBAL_PORT"),strData);
					return TRUE;
				}
				else if(nCol==ID_LIST_CELLBAL_USE)
				{
					lpChannelInfo->m_CtrlClient.m_iCellBal_USE = atoi(strData);
					mainGlobal.SetChInfo(iCHNO,_T("CELLBAL_USE"),strData);
					return TRUE;
				}
			}
			else if(iType==ID_CMD_CELLBAL_USE_COUNT)
			{//CellBAL USE Count
				if(lpChannelInfo->m_CtrlClient.m_iCellBal_USE==1) nCount++;	
			}
			else if(iType==ID_CMD_CELLBAL_GETCHANNEL)
			{//CellBAL GetChannel
				CString strCHNO=m_ListCellBal.GetItemText(nRow,ID_LIST_CELLBAL_CH);
				if(strCHNO!=CHNO) continue;
			
				m_lpSelectChInfo=lpChannelInfo;
			}
			else if(iType==ID_CMD_CELLBAL_STATE)
			{//CellBAL State
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;

				CString STAT=onConnState(lpChannelInfo->m_CtrlClient.m_iState);
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_CONNSTATE,STAT);

				strTemp.Format(_T("%c"),lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.op_state);
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_op_state,strTemp);

				strTemp.Format(_T("%c"),lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.op_mode);
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_op_mode,strTemp);
				
				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_module_installed_ch,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_run_time,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_module_run_time,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.internal_temp,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_internal_temp,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.load_mode_set,2,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_load_mode_set,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.reserved2,2,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_reserved2,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.load_mode_value,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_load_mode_value,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.signal_sens_time,2,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_signal_sens_time,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.reserved3,8,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_reserved3,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.availability_voltage_upper,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_availability_voltage_upper,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.under_voltage,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_under_voltage,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.over_current,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_over_current,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.over_temp,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_over_temp,strTemp);
				
				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.availability_voltage_lower,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_availability_voltage_lower,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_cond_end_current,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_ch_cond_end_current,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_cond_end_time,10,0,_T(""));//ASC
				if(atoi(strTemp) > 600) //yulee 20190117
				{
					int TmpiValue = atoi(strTemp);
					TmpiValue -= 600;
					strTemp.Format(_T("%07d"), TmpiValue);
				}
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_ch_cond_end_time,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_cond_detection,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_ch_cond_detection,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_cond_release,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_ch_cond_release,strTemp);

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_cond_auto_stop_time,10,0,_T(""));//ASC
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_ch_cond_auto_stop_time,strTemp);

				strTemp=GWin::win_CurrentTime(_T("full"));
				m_ListCellBal.SetItemText(nRow,ID_LIST_CELLBAL_DATE_TIME,strTemp);
			}
			else if(iType==ID_CMD_CELLBALCH_LIST_INIT)
			{//CellBAL CH List Init
				CString strCHNO=m_ListCellBal.GetItemText(nRow,ID_LIST_CELLBAL_CH);
				if(strCHNO!=CHNO) continue;

				if(lpChannelInfo->m_CtrlClient.m_iState!=ID_SOCKET_STAT_CONN)
				{
					AfxMessageBox(MSG_DISCONNSTATE);
					break;
				}
				m_ListCellBalCH.DeleteAllItems();	
				m_iSelectCH=iCHNO;

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
				int module_installed_ch=atoi(strTemp);
					
				module_installed_ch = AfxGetApp()->GetProfileInt("Config","CellBalancerChViewNum",24);  //yulee 20190717

				for(int i=0;i<module_installed_ch;i++)
				{
					int nCol=0;
					CString *listdata=new CString[nColumnCH];
					listdata[nCol++]=GStr::str_IntToStr(++nCount);
					listdata[nCol++]=CHNO;

					strTemp.Format(_T("%c"),lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].ch_state);
					listdata[nCol++]=strTemp;

					strTemp.Format(_T("%c"),lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].reserved1);
					listdata[nCol++]=strTemp;

					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].voltage,10,0,_T(""));//ASC
					listdata[nCol++]=strTemp;

					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].current,10,0,_T(""));//ASC
					listdata[nCol++]=strTemp;

					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].run_time,10,0,_T(""));//ASC
					listdata[nCol++]=strTemp;

					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].reserved2,10,0,_T(""));//ASC
					listdata[nCol++]=strTemp;

					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].reserved3,10,0,_T(""));//ASC
					listdata[nCol++]=strTemp;

					strTemp=GWin::win_CurrentTime(_T("full"));
					listdata[nCol++]=strTemp;

					GList::list_Insert(0,&m_ListCellBalCH,nColumnCH,listdata,FALSE);
				}
				break;
			}
			else if(iType==ID_CMD_CELLBALCH_STATE)
			{//CellBAL CH State
				if(m_iSelectCH!=iCHNO) continue;
				if(lpChannelInfo->m_CtrlClient.m_iState!=ID_SOCKET_STAT_CONN) break;
				
				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
				int module_installed_ch=atoi(strTemp);

				module_installed_ch = AfxGetApp()->GetProfileInt("Config","CellBalancerChViewNum",24); //yulee 20190717
				
				for(int i=0;i<module_installed_ch;i++)
				{
					CString strNo=GStr::str_IntToStr(i+1);
					int nRow=GList::list_GetSameText2(&m_ListCellBalCH,strNo,CHNO,ID_LIST_CELLBALCH_NO,ID_LIST_CELLBALCH_CH);
					if(nRow<0) continue;

					int nCol=ID_LIST_CELLBALCH_ch_state;
					
					strTemp.Format(_T("%c"),lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].ch_state);
					m_ListCellBalCH.SetItemText(nRow,nCol++,strTemp);
					
					strTemp.Format(_T("%c"),lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].reserved1);
					m_ListCellBalCH.SetItemText(nRow,nCol++,strTemp);
					
					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].voltage,10,0,_T(""));//ASC
					m_ListCellBalCH.SetItemText(nRow,nCol++,strTemp);
					
					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].current,10,0,_T(""));//ASC
					m_ListCellBalCH.SetItemText(nRow,nCol++,strTemp);
					
					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].run_time,10,0,_T(""));//ASC
					m_ListCellBalCH.SetItemText(nRow,nCol++,strTemp);
					
					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].reserved2,10,0,_T(""));//ASC
					m_ListCellBalCH.SetItemText(nRow,nCol++,strTemp);
					
					strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].reserved3,10,0,_T(""));//ASC
					m_ListCellBalCH.SetItemText(nRow,nCol++,strTemp);

					strTemp=GWin::win_CurrentTime(_T("full"));
					m_ListCellBalCH.SetItemText(nRow,ID_LIST_CELLBALCH_DATE_TIME,strTemp);
				}
				break;
			}
			else if(iType==ID_CMD_CONNECT)
			{//Conn
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;

				CString IP   =lpChannelInfo->m_CtrlClient.m_strCellBal_IP;
				CString PORT =lpChannelInfo->m_CtrlClient.m_strCellBal_PORT;
				if(lpChannelInfo->m_CtrlClient.onConnect(this,_ttoi(PORT),IP)==FALSE)
				{
					strTemp.Format(_T("%s(%s:%s)"),MSG_CONNFAILED,IP,PORT);
					MSGWARN(this->m_hWnd,strTemp,TITLE_WARN);
				}
				else 
				{
					int iUSE=lpChannelInfo->m_CtrlClient.m_iCellBal_USE;
					lpChannelInfo->m_CtrlClient.m_iReConn=iUSE;
				}
			}
			else if(iType==ID_CMD_DISCONN)
			{//DisConn
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;

				lpChannelInfo->m_CtrlClient.m_iReConn=0;
				lpChannelInfo->m_CtrlClient.onCloseClientSocket();
			}
			else if(iType==ID_CMD_CONN_STATE)
			{//State
				CString strCHNO=m_ListCellBal.GetItemText(nRow,ID_LIST_CELLBAL_CH);
				if(strCHNO!=CHNO) continue;

				return lpChannelInfo->m_CtrlClient.m_iState;
			}
			else if(iType==ID_CMD_TO_MODULE_INFO_REQUEST)
			{//P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;
				
				lpChannelInfo->m_CtrlClient.onSendCommand(P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST);
			}
			else if(iType==ID_CMD_TO_RESET)
			{//P1_CMD_TO_MACHINE_RESET
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;
				
				lpChannelInfo->m_CtrlClient.onSendCommand(P1_CMD_TO_MACHINE_RESET);
			}
			else if(iType==ID_CMD_TO_OP_MODE_SET)
			{//P1_CMD_TO_MACHINE_OP_MODE_SET
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;
				if(strVal.GetLength()<1) return FALSE;
				
				lpChannelInfo->m_CtrlClient.onSetMachine_op_mode_set(strVal[0]);
			}
			else if(iType==ID_CMD_TO_LOAD_MODE_SET)
			{//P1_CMD_TO_MACHINE_LOAD_MODE_SET
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;
							
				int nSize=sizeof(S_P1_CMD_BODY_LOAD_MODE_SET);

				char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
				memset(szData,0,sizeof(szData));
				memcpy(szData,&cmdBodyLoadModeSet,nSize);

				lpChannelInfo->m_CtrlClient.onSendCommand(P1_CMD_TO_MACHINE_LOAD_MODE_SET,0,nSize,szData);
			}
			else if(iType==ID_CMD_TO_RUN)
			{//P1_CMD_TO_MACHINE_RUN
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;
				
				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
				int module_installed_ch=atoi(strTemp);
				if(module_installed_ch<1) break;

				CString strVal;
				lpChannelInfo->m_CtrlClient.onSetMachine_run_ch(module_installed_ch,strVal);				
			}
			else if(iType==ID_CMD_TO_STOP)
			{//P1_CMD_TO_MACHINE_STOP
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				if(m_ListCellBal.GetCheck(nRow)==FALSE) continue;

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
				int module_installed_ch=atoi(strTemp);
				if(module_installed_ch<1) break;
				
				CString strVal;
				lpChannelInfo->m_CtrlClient.onSetMachine_stop_ch(module_installed_ch,strVal);
			}
			else if(iType==ID_CMD_TO_CH_RUN)
			{//P1_CMD_TO_MACHINE_RUN
				if(m_iSelectCH!=iCHNO) continue;
				if(lpChannelInfo->m_CtrlClient.m_iState!=ID_SOCKET_STAT_CONN) break;				

				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;

				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));//ASC
				int module_installed_ch=atoi(strTemp);
				if(module_installed_ch<1) break;
				
				S_P1_CMD_BODY_RUN cmdBodyRun;
				memset(&cmdBodyRun,0,sizeof(S_P1_CMD_BODY_RUN));
				int nSize=sizeof(S_P1_CMD_BODY_RUN);
				
				strVal=_T("");
				for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
				{
					strTemp=_T("0");
					char ch_state=lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].ch_state;
					if(ch_state=='R') strTemp=_T("1");////////////////////////////

					if(module_installed_ch>i)
					{
						CString strNO=GStr::str_IntToStr(i+1);
						nRow=GList::list_GetSameText2(&m_ListCellBalCH,strNO,CHNO,ID_LIST_CELLBALCH_NO,ID_LIST_CELLBALCH_CH);
						if(nRow<0) continue;
						
						if(m_ListCellBalCH.GetCheck(nRow)==TRUE)
						{
							strTemp=_T("1");
						}
					}
					strVal+=strTemp;
				}
				mainGlobal.onCopyChar(cmdBodyRun.flag.chFlag,strVal,PS_MAX_CELLBAL_COUNT);
				
				char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
				memset(szData,0,sizeof(szData));
				memcpy(szData,&cmdBodyRun,nSize);
				
				lpChannelInfo->m_CtrlClient.onSendCommand(P1_CMD_TO_MACHINE_RUN,0,nSize,szData);
			}
			else if(iType==ID_CMD_TO_CH_STOP)
			{//P1_CMD_TO_MACHINE_STOP
				if(m_iSelectCH!=iCHNO) continue;
				if(lpChannelInfo->m_CtrlClient.m_iState!=ID_SOCKET_STAT_CONN) break;				
				
				nRow=GList::list_GetSameText(&m_ListCellBal,CHNO,ID_LIST_CELLBAL_CH);
				if(nRow<0) continue;
				
				strTemp=mainGlobal.onGetCHAR(lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));
				int module_installed_ch=atoi(strTemp);
				if(module_installed_ch<1) break;
				
				S_P1_CMD_BODY_STOP cmdBodyStop;
				memset(&cmdBodyStop,0,sizeof(S_P1_CMD_BODY_STOP));
				int nSize=sizeof(S_P1_CMD_BODY_STOP);
				
				strVal=_T("");
				for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
				{
					strTemp=_T("0");
					char ch_state=lpChannelInfo->m_CtrlClient.m_cmdBodyModuleInfoReply.ch_data[i].ch_state;
					
					
					BOOL IsChStop;
					IsChStop = FALSE;
					if(ch_state=='S') IsChStop = TRUE;
					else
						IsChStop = FALSE;
					//if(ch_state=='S') strTemp=_T("1");////////////////////////////
					
					if(module_installed_ch>i)
					{
						CString strNO=GStr::str_IntToStr(i+1);
						nRow=GList::list_GetSameText2(&m_ListCellBalCH,strNO,CHNO,ID_LIST_CELLBALCH_NO,ID_LIST_CELLBALCH_CH);
						if(nRow<0) continue;
						
						if(IsChStop == FALSE)
						{
							if(m_ListCellBalCH.GetCheck(nRow)==TRUE)
							{
								strTemp=_T("1");
							}
						}
// 						if(m_ListCellBalCH.GetCheck(nRow)==TRUE)
// 						{
// 							strTemp=_T("1");
// 						}
					}
					strVal+=strTemp;
					strTemp = "0";//yulee 20190226 Add
				}
				mainGlobal.onCopyChar(cmdBodyStop.flag.chFlag,strVal,PS_MAX_CELLBAL_COUNT);
				
				char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
				memset(szData,0,sizeof(szData));
				memcpy(szData,&cmdBodyStop,nSize);
				
				lpChannelInfo->m_CtrlClient.onSendCommand(P1_CMD_TO_MACHINE_STOP,0,nSize,szData);
			}
		}
	}
	return nCount;
}


void Dlg_CellBALMain::OnDblclkListCellbal(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LPNMITEMACTIVATE pitem = (LPNMITEMACTIVATE) pNMHDR;
	int nRow = pitem->iItem;
	int nCol = pitem->iSubItem;

	if(nRow>-1 && nCol>-1)
	{
		if(onSbc_Deal(ID_CMD_CONN_STATE,nRow,nCol)==ID_SOCKET_STAT_CONN)
		{
			onSbc_Deal(ID_CMD_CELLBALCH_LIST_INIT,nRow,nCol);
		}
		else
		{
			if( nCol==ID_LIST_CELLBAL_IP ||
				nCol==ID_LIST_CELLBAL_PORT ||
				nCol==ID_LIST_CELLBAL_USE)
			{
				m_ListCellBal.onCreateObject(nRow,nCol);
			}
		}
	}
	*pResult = 0;
}

DWORD WINAPI Dlg_CellBALMain::SbcDealThread(LPVOID arg)
{
	Dlg_CellBALMain *wnd=(Dlg_CellBALMain *)arg;
	if(!wnd) return 0;
	
	CString strTemp;

	strTemp.Format(_T("SbcDealThread Start"));
	log_All(_T("cellbal"),_T("thread"),strTemp);

	while(1)
	{
		if(g_AppInfo.iEnd==1) break;
				
		wnd->onSbc_Deal(ID_CMD_CELLBAL_STATE);//SBC CH
		wnd->onSbc_Deal(ID_CMD_CELLBALCH_STATE);//CELLBAL CH

		Sleep(1000);
	}
	
	strTemp.Format(_T("SbcDealThread End"));
	log_All(_T("cellbal"),_T("thread"),strTemp);
	return 0;
}

void Dlg_CellBALMain::OnButCellbalConn() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_CONNTRY,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_CONNECT);
}

void Dlg_CellBALMain::OnButCellbalDiscon() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_DISCONNTRY,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_DISCONN);
}

void Dlg_CellBALMain::OnButCellbalModinforeq() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_CMD_MODINFOREQ,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_TO_MODULE_INFO_REQUEST);
}

void Dlg_CellBALMain::OnButCellbalReset() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_CMD_RESET,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_TO_RESET);
}

void Dlg_CellBALMain::OnButCellbalOpmodeset() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	int nRet=::MessageBox(this->m_hWnd, MSG_CMD_OPMODESET, TITLE_CONN, MB_ICONQUESTION|MB_YESNOCANCEL);
    
	CString strValue=_T("");
	if(nRet==IDYES)
	{
		strValue=_T("A");//Auto
	}
	else if(nRet==IDNO)
	{
		strValue=_T("M");//Manual
	}
	else
	{
		return;
	}
	onSbc_Deal(ID_CMD_TO_OP_MODE_SET,-1,-1,strValue);
}

void Dlg_CellBALMain::OnButCellbalLoadmodset() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount!=1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECTSINGLE,TITLE_WARN);
		return;
	}
	int nRow=GList::list_SelCheckBox(&m_ListCellBal);
	if(nRow<0)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}

	onSbc_Deal(ID_CMD_TO_LOAD_MODE_SET,nRow);
}

void Dlg_CellBALMain::OnButCellbalRun() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_CMD_RUN,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_TO_RUN);
}

void Dlg_CellBALMain::OnButCellbalStop() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBal);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_CMD_STOP,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_TO_STOP);
}


void Dlg_CellBALMain::OnButCellbalchRun() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBalCH);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_CMD_RUN,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_TO_CH_RUN);
}

void Dlg_CellBALMain::OnButCellbalchStop() 
{
	int nCount=GList::list_SelCheckBoxCount(&m_ListCellBalCH);
	if(nCount<1)
	{
		MSGWARN(this->m_hWnd,MSG_SELECT,TITLE_WARN);
		return;
	}
	if(MSGQUEST(this->m_hWnd,MSG_CMD_STOP,TITLE_CONN)==IDNO) return;	
	
	onSbc_Deal(ID_CMD_TO_CH_STOP);
}

void Dlg_CellBALMain::OnDblclkListCellbalLog() 
{	
	m_ListCellBalLog.ResetContent();
}

void Dlg_CellBALMain::OnButCellbalTest() 
{	
}
