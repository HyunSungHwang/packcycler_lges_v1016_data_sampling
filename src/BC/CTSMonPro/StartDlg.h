#if !defined(AFX_STARTDLG_H__1F6781B3_D7B4_4F36_BB6F_2BCA5EC01489__INCLUDED_)
#define AFX_STARTDLG_H__1F6781B3_D7B4_4F36_BB6F_2BCA5EC01489__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StartDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStartDlg dialog
#include  "CTSMonProDoc.h"
#include "XPButton.h"

#define MAX_RECENT_TEST_NAME	10
#define RECENT_NAME_REG_SECTION		"Recent Test"

class CStartDlg : public CDialog
{
// Construction
public:

	BOOL m_bEnableStepSkip;
	BOOL m_nReturn;
	UINT	m_nStartStep;
	UINT    m_nStartOptChamber;			//챔버 동작 모드
	UINT	m_uiChamberCheckTime;		//챔버 온도 비교 시작 시간
	UINT	m_uiChamberCheckFixTime;	//챔버 온도 비교 시작 시간

	void Fun_CheckUseOven();
	void UpdateOvenMode();
	void InitGrid();

	BOOL m_bChamberContinue; //2014.11.21 챔버 대기모드 사용유무 추가
	BOOL m_IsChamberContinue;//2014.11.21 챔버 대기모드 사용유무 추가	
	BOOL m_IsMuxLink;		 //20151111   Mux연동 사용유무 추가	

//	BOOL m_bModuleCmd;
	void SetInfoData(CScheduleData *pSchedule, CPtrArray *aPtrCh);

	CString GetTestPath();
	BOOL DeletePath(CString strPath, CProgressWnd *pPrgressWnd = NULL);
	CString GetFileName();
	int CheckFileName();
	CStartDlg(CCTSMonProDoc *pDoc, CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CStartDlg)
	enum { IDD = IDD_START_DLG , IDD2 = IDD_START_DLG_ENG};
	CComboBox	m_CboMuxChoiceRsv;
	CComboBox	m_ctrlCboCanCheck;
	CDateTimeCtrl	m_ctrlCheckFixTime;
	CDateTimeCtrl	m_ctrlCheckTime;
	CComboBox	m_StepCombo;
	CComboBox	m_CycleCombo;
	CXPButton	m_btnCancel;
	CXPButton	m_btnOK;
	CXPButton	m_btnPrev;
	CLabel	m_strDestination;
	CComboBox	m_ctrlTestNameCombo;
	CString	m_strWorker;
	CString	m_strLot;
	CString	m_strComment;
	CString	m_strDataPath;
	UINT	m_nStartCycle;
	float	m_fChamberFixTemp;
	UINT	m_uiChamberPatternNum;
	float	m_fChamberDeltaTemp;
	UINT	m_uiChamberDelayTime;
	float	m_fChamberDeltaFixTemp;
	BOOL	m_bIsCanModeCheck;
	BOOL	m_chkLoadLink;
	BOOL	m_bIsMuxLink;
	BOOL	m_chkChamberContinue;
	BOOL	m_chkPwrSplyOffWorkEnd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStartDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	WORD tmpWrdPt;

// Implementation
protected:
	CGridCtrl m_Grid;
	CPtrArray		*m_paPtrCh;
	void AddStepInCycle(int nCycle = 0);
	CCTSMonProDoc *m_pDoc;
	CStringList m_strRecentNameList;
	void UpdateState();
	CString m_strTestName;

	CScheduleData *m_pSchedule;
//	CWordArray		*m_pSelChArray;

	// Generated message map functions
	//{{AFX_MSG(CStartDlg)
	afx_msg void OnEditchangeTestNameCombo();
	virtual BOOL OnInitDialog();
	afx_msg void OnEditupdateTestNameCombo();
	afx_msg void OnSelchangeTestNameCombo();
	virtual void OnOK();
	afx_msg void OnPrevButton();
	virtual void OnCancel();
	afx_msg void OnSchDetailButton();
	afx_msg void OnDataFolderButton();
	afx_msg void OnSelchangeCycleCombo();
	afx_msg void OnChkChamberFix();
	afx_msg void OnChkChamberProg();
	afx_msg void OnChkChamberStep();
	afx_msg void OnChkChamberNo();
	afx_msg void OnChkChamberNoAndStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARTDLG_H__1F6781B3_D7B4_4F36_BB6F_2BCA5EC01489__INCLUDED_)
