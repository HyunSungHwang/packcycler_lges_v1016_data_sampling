# Microsoft Developer Studio Project File - Name="CTSMonPro" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CTSMonPro - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CTSMonPro.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CTSMonPro.mak" CFG="CTSMonPro - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CTSMonPro - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CTSMonPro - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CTSMonPro - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../../obj/CTSMonPro_Release/"
# PROP Intermediate_Dir "../../../obj/CTSMonPro_Release/"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W4 /GR /GX /Zi /O2 /I "C:\PNESolution\mysql\include" /I ".\canconv" /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXEXT" /D "_MBCS" /D "DYNAMIC_XLDRIVER_DLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib /nologo /stack:0x200000 /subsystem:windows /incremental:yes /map /debug /machine:I386 /out:"../../../bin/Release/CTSMonitorPro.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "CTSMonPro - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../../obj/CTSMonPro_Debug/"
# PROP Intermediate_Dir "../../../obj/CTSMonPro_Debug/"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W4 /Gm /GR /GX /ZI /Od /I "C:\PNESolution\mysql\include" /I ".\canconv" /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXEXT" /D "DYNAMIC_XLDRIVER_DLL" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib /nologo /subsystem:windows /map /debug /machine:I386 /out:"../../../bin/Debug/CTSMonitorPro.exe" /pdbtype:sept /MAPINFO:EXPORTS /MAPINFO:LINES
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "CTSMonPro - Win32 Release"
# Name "CTSMonPro - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "CANConv"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CANConv\CanConvBroker.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\CanConvBroker.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\CppSQLite3.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\CANConv\CppSQLite3.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\debug.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\LinCanConvBroker.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\LinCanConvBroker.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\LinConvDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\LinConvDlg.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\LinSettingDBDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\LinSettingDBDialog.h
# End Source File
# Begin Source File

SOURCE=".\CANConv\modbus-rtu-private.h"
# End Source File
# Begin Source File

SOURCE=".\CANConv\modbus-rtu.h"
# End Source File
# Begin Source File

SOURCE=".\CANConv\modbus-tcp.h"
# End Source File
# Begin Source File

SOURCE=".\CANConv\modbus-version.h"
# End Source File
# Begin Source File

SOURCE=.\CANConv\modbus.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\ModbusCanConvBroker.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\ModbusCanConvBroker.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\ModbusConvDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\ModbusConvDlg.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\ModbusSettingDBDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\ModbusSettingDBDialog.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\Pcomm.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\sqlite3.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\UartCanConvBroker.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\UartCanConvBroker.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\UartConvDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\uartconvdlg.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\UartFunctions.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\UartFunctions.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\vxlapi.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\xlCANFunctions.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\xlCANFunctions.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\xlCANStandAloneFunc.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\xlCANStandAloneFunc.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\xlLINFunctions.cpp
# End Source File
# Begin Source File

SOURCE=.\CANConv\xlLINFunctions.h
# End Source File
# Begin Source File

SOURCE=.\CANConv\xlLoadlib.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\CANConv\modbus.lib
# End Source File
# Begin Source File

SOURCE=.\CANConv\vxlapi.lib
# End Source File
# Begin Source File

SOURCE=.\CANConv\sqlite3.lib
# End Source File
# End Group
# Begin Source File

SOURCE=.\AccuracySetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AddAuxDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AlarmReadyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AuxDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CalDataExportDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CalibrationData.cpp
# End Source File
# Begin Source File

SOURCE=.\CalibratorDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CaliPoint.cpp
# End Source File
# Begin Source File

SOURCE=.\CaliSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\BCLib\SRC\PSServer\Channel.cpp
# End Source File
# Begin Source File

SOURCE=.\ChCaliData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\BCLib\SRC\PSCommon\ChData.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorListBox.cpp
# End Source File
# Begin Source File

SOURCE=.\cTree.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSMonPro.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSMonPro.rc
# End Source File
# Begin Source File

SOURCE=.\CTSMonProDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\CTSMonProView.cpp
# End Source File
# Begin Source File

SOURCE=.\CyclerChannel.cpp
# End Source File
# Begin Source File

SOURCE=.\CyclerModule.cpp
# End Source File
# Begin Source File

SOURCE=.\DCLoad.cpp
# End Source File
# Begin Source File

SOURCE=.\DetailOvenDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DisplayItemEditDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBAL.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBALLoadModeSet.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBALMain.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBALOpt.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_Dblogin.cpp
# End Source File
# Begin Source File

SOURCE=.\Dlg_PwrSupplyStat.cpp
# End Source File
# Begin Source File

SOURCE=.\FileSaveSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FolderDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\ftp.cpp
# End Source File
# Begin Source File

SOURCE=.\FtpDownLoad.cpp
# End Source File
# Begin Source File

SOURCE=.\Global.cpp
# End Source File
# Begin Source File

SOURCE=.\GotoStepDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GridComboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\IPSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ItemOnDisplay.cpp
# End Source File
# Begin Source File

SOURCE=.\JigIDRegDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LedButton.cpp
# End Source File
# Begin Source File

SOURCE=.\ListCtrlEx.cpp
# End Source File
# Begin Source File

SOURCE=.\ListEditCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\LogDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginManagerDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LogSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MaintenanceDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MenuCH.cpp
# End Source File
# Begin Source File

SOURCE=.\MeterSetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MiniDump.cpp
# End Source File
# Begin Source File

SOURCE=..\..\BCLib\SRC\PSServer\MinMaxData.cpp
# End Source File
# Begin Source File

SOURCE=.\MltiTree.cpp
# End Source File
# Begin Source File

SOURCE=..\..\BCLib\SRC\PSServer\Module.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModuleIndexTable.cpp
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\NoInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OvenCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ParallelConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.cpp
# End Source File
# Begin Source File

SOURCE=.\ReadyRunDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.Cpp
# End Source File
# Begin Source File

SOURCE=.\RegeditManage.cpp
# End Source File
# Begin Source File

SOURCE=.\ReserveDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RestoreDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RTTableSet.cpp
# End Source File
# Begin Source File

SOURCE=.\SafetyUpdateDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SchUpdateDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectScheduleDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelUnitDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SerialConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SerialPort.cpp
# End Source File
# Begin Source File

SOURCE=.\SettingCanChangeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SettingCanDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SettingCanDlg_Transmit.cpp
# End Source File
# Begin Source File

SOURCE=.\SettingCanFdDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SettingCanFdDlg_Transmit.cpp
# End Source File
# Begin Source File

SOURCE=.\SetupOven.cpp
# End Source File
# Begin Source File

SOURCE=.\ShowEmgDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SimpleTestDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StartDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StartDlgEx.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TextInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TrayInputDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UnitComLogDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UserConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\VoltageCheckDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WarningDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WorkStationSignDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WorkWarnin.cpp
# End Source File
# Begin Source File

SOURCE=.\XPButton.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AccuracySetDlg.h
# End Source File
# Begin Source File

SOURCE=.\AddAuxDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\AlarmReadyDlg.h
# End Source File
# Begin Source File

SOURCE=.\AuxDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\CalDataExportDlg.h
# End Source File
# Begin Source File

SOURCE=.\CalibrationData.h
# End Source File
# Begin Source File

SOURCE=.\CalibratorDlg.h
# End Source File
# Begin Source File

SOURCE=.\CaliPoint.h
# End Source File
# Begin Source File

SOURCE=.\CaliSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChCaliData.h
# End Source File
# Begin Source File

SOURCE=.\ColorListBox.h
# End Source File
# Begin Source File

SOURCE=.\cTree.h
# End Source File
# Begin Source File

SOURCE=.\CTSMonPro.h
# End Source File
# Begin Source File

SOURCE=.\CTSMonProDoc.h
# End Source File
# Begin Source File

SOURCE=.\CTSMonProView.h
# End Source File
# Begin Source File

SOURCE=.\CyclerChannel.h
# End Source File
# Begin Source File

SOURCE=.\CyclerModule.h
# End Source File
# Begin Source File

SOURCE=.\DCLoad.h
# End Source File
# Begin Source File

SOURCE=.\DetailOvenDlg.h
# End Source File
# Begin Source File

SOURCE=.\DisplayItemEditDlg.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBAL.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBALLoadModeSet.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBALMain.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_CellBALOpt.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_Dblogin.h
# End Source File
# Begin Source File

SOURCE=.\Dlg_PwrSupplyStat.h
# End Source File
# Begin Source File

SOURCE=.\FileSaveSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\FolderDialog.h
# End Source File
# Begin Source File

SOURCE=..\..\MyLib\Include\FormData.h
# End Source File
# Begin Source File

SOURCE=.\ftp.h
# End Source File
# Begin Source File

SOURCE=.\FtpDownLoad.h
# End Source File
# Begin Source File

SOURCE=.\Global.h
# End Source File
# Begin Source File

SOURCE=.\GotoStepDlg.h
# End Source File
# Begin Source File

SOURCE=.\GridComboBox.h
# End Source File
# Begin Source File

SOURCE=.\IPSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\ItemOnDisplay.h
# End Source File
# Begin Source File

SOURCE=.\JigIDRegDlg.h
# End Source File
# Begin Source File

SOURCE=.\LedButton.h
# End Source File
# Begin Source File

SOURCE=.\ListCtrlEx.h
# End Source File
# Begin Source File

SOURCE=.\ListEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\LogDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginManagerDlg.h
# End Source File
# Begin Source File

SOURCE=.\LogSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MaintenanceDlg.h
# End Source File
# Begin Source File

SOURCE=..\COMMON\Ctrl\GridCtrl_src\MemDC.h
# End Source File
# Begin Source File

SOURCE=.\MenuCH.h
# End Source File
# Begin Source File

SOURCE=.\MeterSetDlg.h
# End Source File
# Begin Source File

SOURCE=.\MiniDump.h
# End Source File
# Begin Source File

SOURCE=.\MltiTree.h
# End Source File
# Begin Source File

SOURCE=.\ModuleAddDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModuleIndexTable.h
# End Source File
# Begin Source File

SOURCE=.\MyGridWnd.h
# End Source File
# Begin Source File

SOURCE=.\NameHeader.h
# End Source File
# Begin Source File

SOURCE=.\NameInc.h
# End Source File
# Begin Source File

SOURCE=.\NameRes.h
# End Source File
# Begin Source File

SOURCE=.\NoInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\OvenCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ParallelConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrntScreen.h
# End Source File
# Begin Source File

SOURCE=.\ReadyRunDlg.h
# End Source File
# Begin Source File

SOURCE=.\RealEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\RegeditManage.h
# End Source File
# Begin Source File

SOURCE=.\ReserveDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RestoreDlg.h
# End Source File
# Begin Source File

SOURCE=.\RTTableSet.h
# End Source File
# Begin Source File

SOURCE=.\SafetyUpdateDlg.h
# End Source File
# Begin Source File

SOURCE=.\SchUpdateDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelectScheduleDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelUnitDlg.h
# End Source File
# Begin Source File

SOURCE=.\serialconfigdlg.h
# End Source File
# Begin Source File

SOURCE=.\SerialPort.h
# End Source File
# Begin Source File

SOURCE=.\SettingCanChangeDlg.h
# End Source File
# Begin Source File

SOURCE=.\SettingCanDlg.h
# End Source File
# Begin Source File

SOURCE=.\SettingCanDlg_Transmit.h
# End Source File
# Begin Source File

SOURCE=.\SettingCanFdDlg.h
# End Source File
# Begin Source File

SOURCE=.\SettingCanFdDlg_Transmit.h
# End Source File
# Begin Source File

SOURCE=.\SetupOven.h
# End Source File
# Begin Source File

SOURCE=.\ShowEmgDlg.h
# End Source File
# Begin Source File

SOURCE=.\SimpleTestDlg.h
# End Source File
# Begin Source File

SOURCE=.\StartDlg.h
# End Source File
# Begin Source File

SOURCE=.\StartDlgEx.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\Temp.h
# End Source File
# Begin Source File

SOURCE=.\TextInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\TrayInputDlg.h
# End Source File
# Begin Source File

SOURCE=.\UnitComLogDlg.h
# End Source File
# Begin Source File

SOURCE=.\UserConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\VoltageCheckDlg.h
# End Source File
# Begin Source File

SOURCE=.\WarningDlg.h
# End Source File
# Begin Source File

SOURCE=.\whdump.h

!IF  "$(CFG)" == "CTSMonPro - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "CTSMonPro - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\WorkStationSignDlg.h
# End Source File
# Begin Source File

SOURCE=.\WorkWarnin.h
# End Source File
# Begin Source File

SOURCE=.\XPButton.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=".\res\ADP�ΰ�.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Be Screen.ico"
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap5.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap6.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap7.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cali_res.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cell_sta.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Computer colours.ico"
# End Source File
# Begin Source File

SOURCE=".\res\Computer kalied.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 189.ico"
# End Source File
# Begin Source File

SOURCE=".\res\ComputerMisc 209.ico"
# End Source File
# Begin Source File

SOURCE=.\res\Copy.bmp
# End Source File
# Begin Source File

SOURCE=.\res\CTSMonPro.ico
# End Source File
# Begin Source File

SOURCE=.\res\CTSMonPro.rc2
# End Source File
# Begin Source File

SOURCE=.\res\CTSMonProDoc.ico
# End Source File
# Begin Source File

SOURCE=".\res\Device Driver MPD.ico"
# End Source File
# Begin Source File

SOURCE=.\res\discharg.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Exit.ico
# End Source File
# Begin Source File

SOURCE=.\res\Graph.bmp
# End Source File
# Begin Source File

SOURCE=.\res\greenButton.bmp
# End Source File
# Begin Source File

SOURCE=.\res\icon.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=".\res\JPEG Image File JPG.ico"
# End Source File
# Begin Source File

SOURCE=.\res\LOG.bmp
# End Source File
# Begin Source File

SOURCE=.\res\logo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MONITOR3.ICO
# End Source File
# Begin Source File

SOURCE=.\res\N62.ico
# End Source File
# Begin Source File

SOURCE=.\res\Pause.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Pencil.ico
# End Source File
# Begin Source File

SOURCE=.\res\PNELogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red_btn_.bmp
# End Source File
# Begin Source File

SOURCE=.\res\setup.bmp
# End Source File
# Begin Source File

SOURCE=.\res\SmallCross.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Splash16.bmp
# End Source File
# Begin Source File

SOURCE=.\Splsh16.bmp
# End Source File
# Begin Source File

SOURCE=.\res\START.bmp
# End Source File
# Begin Source File

SOURCE=.\res\State16.bmp
# End Source File
# Begin Source File

SOURCE=.\res\State24bit.bmp
# End Source File
# Begin Source File

SOURCE=.\res\State256.bmp
# End Source File
# Begin Source File

SOURCE=.\res\state_ic.bmp
# End Source File
# Begin Source File

SOURCE=.\res\STOP.BMP
# End Source File
# Begin Source File

SOURCE=.\res\TABLE2.BMP
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\TOOLS2.ICO
# End Source File
# Begin Source File

SOURCE=.\res\tree_bit.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Undo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Wanning1.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Windows Config File INI.ico"
# End Source File
# Begin Source File

SOURCE=.\res\YellowBar.bmp
# End Source File
# End Group
# Begin Group "Global"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Global\fileRecord\fileRecord.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\fileRecord\fileRecord.h
# End Source File
# Begin Source File

SOURCE=.\Global\GFile.h
# End Source File
# Begin Source File

SOURCE=.\Global\GGdc.h
# End Source File
# Begin Source File

SOURCE=.\Global\GList.h
# End Source File
# Begin Source File

SOURCE=.\Global\GLog.h
# End Source File
# Begin Source File

SOURCE=.\Global\GMath.h
# End Source File
# Begin Source File

SOURCE=.\Global\GNet.h
# End Source File
# Begin Source File

SOURCE=.\Global\GReg.h
# End Source File
# Begin Source File

SOURCE=.\Global\GShell.h
# End Source File
# Begin Source File

SOURCE=.\Global\GStr.h
# End Source File
# Begin Source File

SOURCE=.\Global\GTime.h
# End Source File
# Begin Source File

SOURCE=.\Global\GTree.h
# End Source File
# Begin Source File

SOURCE=.\Global\GWin.h
# End Source File
# Begin Source File

SOURCE=.\Global\IniParse\IniParser.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\IniParse\IniParser.h
# End Source File
# Begin Source File

SOURCE=.\Global\Mesg.h
# End Source File
# End Group
# Begin Group "Mysql"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Global\Mysql\MySql.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\Mysql\Mysql.h
# End Source File
# Begin Source File

SOURCE=.\Global\Mysql\PneApp.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\Mysql\PneApp.h
# End Source File
# End Group
# Begin Group "ChillerCtrl"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ChillerCtrl\ChillerCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ChillerCtrl\ChillerCtrl.h
# End Source File
# End Group
# Begin Group "PwrSupplyCtrl"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\PwrSupplyCtrl\PwrSupplyCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\PwrSupplyCtrl\PwrSupplyCtrl.h
# End Source File
# End Group
# Begin Group "gxListCtrl"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxComboCell.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxComboCell.h
# End Source File
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxEditCell.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxEditCell.h
# End Source File
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxListCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxListCtrl.h
# End Source File
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxPickerCell.cpp
# End Source File
# Begin Source File

SOURCE=.\Global\gxListCtrl\gxPickerCell.h
# End Source File
# End Group
# Begin Group "CellBALCtrl"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CellBALCtrl\CtrlSocketClient.cpp
# End Source File
# Begin Source File

SOURCE=.\CellBALCtrl\CtrlSocketClient.h
# End Source File
# Begin Source File

SOURCE=.\CellBALCtrl\CtrlSocketHeader.h
# End Source File
# End Group
# Begin Group "MainCtrl"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\MainCtrl\MainCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\MainCtrl\MainCtrl.h
# End Source File
# End Group
# Begin Group "MultiLang"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Global\MultiLang\GlobalLang.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Global\MultiLang\GlobalLang.h
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Global\MultiLang\util\XIni.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Global\MultiLang\util\XIni.h
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Global\MultiLang\util\XMultiLang.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Global\MultiLang\util\XMultiLang.h
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Global\MultiLang\util\XToken.h
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\CANConv\Pcomm.lib
# End Source File
# End Target
# End Project
# Section CTSMonPro : {72ADFD78-2C39-11D0-9903-00A0C91BC942}
# 	1:10:IDB_SPLASH:102
# 	2:21:SplashScreenInsertKey:4.0
# End Section
