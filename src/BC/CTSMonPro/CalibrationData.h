// CalibrationData.h: interface for the CCalibrationData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALIBRATIONDATA_H__A89BD6CE_1CAC_415B_9EBD_7110F98EC6A6__INCLUDED_)
#define AFX_CALIBRATIONDATA_H__A89BD6CE_1CAC_415B_9EBD_7110F98EC6A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CaliPoint.h"
#include "ChCaliData.h"

#define CAL_DATA_EMPTY	0
#define CAL_DATA_SAVED	1
#define CAL_DATA_MODIFY	2
#define CAL_DATA_ERROR	3	

#define CAL_FILE_ID		20050726	

class CCalibrationData  
{
public:
	BOOL IsLoadedData();
	void SetState(int state)	{ m_nState = state;	}
	void SetEdited();
	int GetState();
	CString GetUpdateTime();
	BOOL SaveDataToFile(CString strFileName);
	BOOL LoadDataFromFile(CString strFileName);
	CCalibrationData();
	virtual ~CCalibrationData();

	CCaliPoint	m_PointData;		//현재 모듈에 설정된 Calibration 조건 
	CChCaliData	m_ChCaliData;		//채널의 교정값 

private:
	int m_nState;						//현재 Data의 상태 (Empty/Saved/Modify/...)
	CString m_strCalTime;
};

#endif // !defined(AFX_CALIBRATIONDATA_H__A89BD6CE_1CAC_415B_9EBD_7110F98EC6A6__INCLUDED_)
