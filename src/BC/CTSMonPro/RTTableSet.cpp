// RTTableSet.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "MainFrm.h"
#include "CTSMonProView.h"
#include "RTTableSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// CRTTableSet dialog


CRTTableSet::CRTTableSet(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CRTTableSet::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CRTTableSet::IDD2):	
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CRTTableSet::IDD3):
	(CRTTableSet::IDD), pParent)
	, m_nSelectedRtType(0)
{
	//{{AFX_DATA_INIT(CRTTableSet)
		// NOTE: the ClassWizard will add member initialization here
	m_bRetCnt = 0;
	m_strBackupFileName = "";
	//}}AFX_DATA_INIT
}


void CRTTableSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRTTableSet)
	DDX_Control(pDX, IDC_PROGRESS, m_ctrlRTtableProgress);
	DDX_Control(pDX, IDC_LIST_RTTABLE, m_listRTable);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_COMBO_RT_TYPE, m_cmbRtType);
}


BEGIN_MESSAGE_MAP(CRTTableSet, CDialog)
	//{{AFX_MSG_MAP(CRTTableSet)
	ON_BN_CLICKED(IDC_BUT_RTTABLE_ADD, OnButRttableAdd)
	ON_BN_CLICKED(IDC_BUT_RTTABLE_MODI, OnButRttableModi)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(IDC_COMBO_RT_TYPE, &CRTTableSet::OnCbnSelchangeComboRtType)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRTTableSet message handlers

BOOL CRTTableSet::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CWinApp* pApp = AfxGetApp();
	CRect rect;
		
	CString strTemp;

	//ksj 20200207 : 습도 타입 추가
	m_cmbRtType.AddString("Temperature");
	m_cmbRtType.SetItemData(0,PS_AUX_TYPE_TEMPERATURE_TH);
	m_cmbRtType.AddString("Humidity");
	m_cmbRtType.SetItemData(1,PS_AUX_TYPE_HUMIDITY);
	m_cmbRtType.SetCurSel(0); //기본 온도 선택
	m_nSelectedRtType = PS_AUX_TYPE_TEMPERATURE_TH;

	//ksj 20200214 : 레지스트리 옵션에 따라 기능 활성 비활성
	EnableFuntions();	

	//strTemp.Format("Info:SBC에서 정보를 가져옵니다.");//$1013
	strTemp.Format(Fun_FindMsg("OnInitDialog","IDD_RTTABLE_SET_DLG"));//&&
	GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);

	InitList();
	

	if(UpdateRtTable()) //ksj 20200207
	{
		//상태바 생성
		static UINT BASED_CODE indicators[] =   
		{  
			ID_RESERV_DLG_INDICATOR1,  
			ID_RESERV_DLG_INDICATOR2//,
			//ID_RESERV_DLG_INDICATOR3       
		}; 

		BOOL bAddBtn = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "RTTable Add Button", 0);
		if(bAddBtn == 1)
		{
			GetDlgItem(IDC_BUT_RTTABLE_ADD)->ShowWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_BUT_RTTABLE_ADD)->ShowWindow(FALSE);
		}

		// 	m_StatusBar.Create(this);
		// 	m_StatusBar.SetIndicators(indicators, 2);
		/*	m_StatusBar.SetPaneText(0,"예약 작업 멈춤");	*/

		GetClientRect(&rect);

		//대화 상자 크기 불러오기
		rect.top = 0;
		//rect.bottom = pApp->GetProfileInt(CT_CONFIG_REG_SEC,"RTEdit Window Height",300);
		rect.bottom = pApp->GetProfileInt(CT_CONFIG_REG_SEC,"RTEdit Window Height",350);//20190630
		rect.left = 0;
		rect.right = pApp->GetProfileInt(CT_CONFIG_REG_SEC,"RTEdit Window Width",680);
		MoveWindow(&rect);

		//Column 크기 불러오기
		int i;
		int nColumnCnt = m_listRTable.GetHeaderCtrl()->GetItemCount();
		int* pData = new int[nColumnCnt];	
		UINT nBytes;
		ZeroMemory(pData,sizeof(int)*nColumnCnt);
		pApp->GetProfileBinary(CT_CONFIG_REG_SEC,"RTEdit Column Width",(LPBYTE*)&pData,&nBytes);

		if(pData)
		{
			for(int i = 0;i<nColumnCnt;i++)
			{
				if(pData[i])
				{
					m_listRTable.SetColumnWidth(i,pData[i]);
				}		
			}	
		}	
		//strTemp.Format("Info:SBC에서 Table 정보 가져오기를 완료하였습니다.");
		strTemp.Format(Fun_FindMsg("RTTableSet_OnInitDialog_msg2","IDD_RTTABLE_SET_DLG"));//&&
		GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);

		delete pData;
	}
/*
	InitRTTableData(); //Initialize
	if(SetRTTableList() != FALSE)
	{
		UpdateTableInfo();
		SetRTtableNametoDB();
	
		//상태바 생성
		static UINT BASED_CODE indicators[] =   
		{  
			ID_RESERV_DLG_INDICATOR1,  
				ID_RESERV_DLG_INDICATOR2//,
				//ID_RESERV_DLG_INDICATOR3       
		}; 

		BOOL bAddBtn = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "RTTable Add Button", 0);
		if(bAddBtn == 1)
		{
			GetDlgItem(IDC_BUT_RTTABLE_ADD)->ShowWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_BUT_RTTABLE_ADD)->ShowWindow(FALSE);
		}

	// 	m_StatusBar.Create(this);
	// 	m_StatusBar.SetIndicators(indicators, 2);
	/*	m_StatusBar.SetPaneText(0,"예약 작업 멈춤");	*/
	/*	
		GetClientRect(&rect);
		
		//대화 상자 크기 불러오기
 		rect.top = 0;
 		//rect.bottom = pApp->GetProfileInt(CT_CONFIG_REG_SEC,"RTEdit Window Height",300);
		rect.bottom = pApp->GetProfileInt(CT_CONFIG_REG_SEC,"RTEdit Window Height",350);//20190630
 		rect.left = 0;
 		rect.right = pApp->GetProfileInt(CT_CONFIG_REG_SEC,"RTEdit Window Width",680);
		MoveWindow(&rect);
		
		//Column 크기 불러오기
		int i;
		int nColumnCnt = m_listRTable.GetHeaderCtrl()->GetItemCount();
		int* pData = new int[nColumnCnt];	
		UINT nBytes;
		ZeroMemory(pData,sizeof(int)*nColumnCnt);
		pApp->GetProfileBinary(CT_CONFIG_REG_SEC,"RTEdit Column Width",(LPBYTE*)&pData,&nBytes);
		
		if(pData)
		{
			for(int i = 0;i<nColumnCnt;i++)
			{
				if(pData[i])
				{
					m_listRTable.SetColumnWidth(i,pData[i]);
				}		
			}	
		}	
		//strTemp.Format("Info:SBC에서 Table 정보 가져오기를 완료하였습니다.");
		strTemp.Format(Fun_FindMsg("RTTableSet_OnInitDialog_msg2","IDD_RTTABLE_SET_DLG"));//&&
		GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
		
		delete pData;
		return TRUE;
	}
	else
	{
		return FALSE;
	}*/
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRTTableSet::InitList()
{
	
	m_listRTable.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	
	//m_listRTable.InsertColumn(LIST_RT_INDEX,"순서",LVCFMT_CENTER,80);
	m_listRTable.InsertColumn(LIST_RT_INDEX,Fun_FindMsg("RTTableSet_InitListmsg1","IDD_RTTABLE_SET_DLG"),LVCFMT_CENTER,80);//&&
	//m_listRTable.InsertColumn(LIST_RT_STATE,"상태",LVCFMT_CENTER,80);
	m_listRTable.InsertColumn(LIST_RT_STATE,Fun_FindMsg("RTTableSet_InitListmsg2","IDD_RTTABLE_SET_DLG"),LVCFMT_CENTER,80);//&&
	m_listRTable.InsertColumn(LIST_RT_TABLE_NAME,"Table Name",LVCFMT_CENTER,140);
	//m_listRTable.InsertColumn(LIST_RT_UPLOAD_TIME,"최근 작업 시간",LVCFMT_CENTER,100);
	m_listRTable.InsertColumn(LIST_RT_UPLOAD_TIME,Fun_FindMsg("RTTableSet_InitListmsg3","IDD_RTTABLE_SET_DLG"),LVCFMT_CENTER,100);//&&
	m_listRTable.InsertColumn(LIST_RT_TABLE_SBC_NAME,"Table Name(SBC)",LVCFMT_CENTER,150);
	//m_listRTable.InsertColumn(LIST_RT_RESERVED,"비고",LVCFMT_CENTER,100);
	m_listRTable.InsertColumn(LIST_RT_RESERVED,Fun_FindMsg("RTTableSet_InitListmsg4","IDD_RTTABLE_SET_DLG"),LVCFMT_CENTER,100);//&&
}

void CRTTableSet::InitRTTableData()
{

	CString strTmp;
	strTmp.Format("-");
	for(int i=0; i<LIST_MAX_FILE_NUM; i++)
	{
		m_pArrRTtable[i].nIndex = i;
		m_pArrRTtable[i].nState = 0;
		memset(m_pArrRTtable[i].charTableName, 0, sizeof(char)* NAME_CHAR_MAX_LENGTH);
		memset(m_pArrRTtable[i].charWorkTime, 0, sizeof(char)* TIME_CHAR_MAX_LENGTH);
		memset(m_pArrRTtable[i].charTableNameForSBC, 0, sizeof(char)* NAME_CHAR_MAX_LENGTH);
		memset(m_pArrRTtable[i].charReserved, 0, sizeof(char)* NAME_CHAR_MAX_LENGTH);
	}
}

BOOL CRTTableSet::SetRTTableList()
{
	DeleteRTTablesFolder();
	if(DownRTtableFromSBC()==FALSE)
		return 0;
//	{
		//return 0;
		//return 1;
	//}
	SetRTTableInfo();
	return 1;
}

BOOL CRTTableSet::DeleteRTTablesFolder()
{
	CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
	CString strPath;
	//strPath.Format("%s\\RT_table\\*.txt",strCurPath);

	//ksj 20200207
	if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY)
	{
		strPath.Format("%s\\RT_table\\Humidity\\*.txt",strCurPath);
	}
	else //PS_AUX_TYPE_TEMPERATURE
	{
		strPath.Format("%s\\RT_table\\*.txt",strCurPath);
	}
	
	CFileFind finder;
	BOOL bFileFind = finder.FindFile(strPath);
	
	if(bFileFind == 1)//yulee 20181101 지정 폴더에 파일이 있다면...
	{
		//지정 폴더의 위치의 파일로 이동
		CString strFileLoc;
		
		while(bFileFind == 1)
		{
			bFileFind = finder.FindNextFile();
			if(finder.IsArchived())
			{			
				//strFileLoc.Format("%s\\RT_table\\%s", strCurPath, finder.GetFileName());
				//ksj 20200207
				if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY)
				{
					strFileLoc.Format("%s\\RT_table\\Humidity\\%s", strCurPath, finder.GetFileName());
				}
				else //PS_AUX_TYPE_TEMPERATURE
				{
					strFileLoc.Format("%s\\RT_table\\%s", strCurPath, finder.GetFileName());
				}
				DeleteFile(strFileLoc);
			}
		}
	}
	else
	{
		//업데이트 할 파일이 없음
	}
	return 1;
}

BOOL CRTTableSet::DownRTtableFromSBC()
{
	CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
	CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
	
	//if(pView->DownAllRTTableFromSBC() == FALSE)
	if(pView->DownAllRTTableFromSBC(m_nSelectedRtType) == FALSE) //ksj 20200207
	{
		/*AfxMessageBox("RT_table 다운에 실패하였습니다.");*/
		return 0;
	}
	return 1;
}

void CRTTableSet::SetRTTableInfo()
{
	int nIdx, nSat;
	CString strName, strNameSBC, strTimeTime;

	if(ReadRTTableInfo() == TRUE)
	{
		SetRTTableInfoGrid();
	}

}

BOOL CRTTableSet::ReadFileInfo(CString strTargetFileName, CString &strFilePath, CString &strFileName, int nTypeOper)  //yulee 20181121
{
	// TODO: Add your control notification handler code here
	CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
	CString strPath;
	//strPath.Format("%s\\RT_table\\*.txt",strCurPath);

	if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200211
		strPath.Format("%s\\RT_table\\Humidity\\*.txt",strCurPath);
	else
		strPath.Format("%s\\RT_table\\*.txt",strCurPath);
	
	CFileFind finder;
	BOOL bFileFind = finder.FindFile(strPath);
	int nIdx=-1;
	
	
	if(bFileFind == 1)//yulee 20181101 지정 폴더에 파일이 있다면...
	{
		//지정 폴더의 위치의 파일로 이동
		BOOL bSetLocPath;
		
		CString strChk, strLastWriteTime, strWorkTime, strTableNameForSBC;
		CTime cLastWriteTime;
		BOOL bGetLastWriteTime;
		
		CStdioFile fp;
		CFile file;
		CString strFileNameChk, strFileLoc, strLine;
		CString strTmp;
		
		while(bFileFind == 1)
		{
			bFileFind = finder.FindNextFile();
			if(finder.IsArchived())
			{
// 				bGetLastWriteTime = finder.GetLastWriteTime(cLastWriteTime);
// 				strLastWriteTime = cLastWriteTime.Format(_T("%Y-%m-%d %H:%M:%S"));

				CTime tm = CTime::GetCurrentTime();
				strWorkTime = tm.Format(_T("%Y-%m-%d %H:%M:%S"));

				strFileNameChk = finder.GetFileName();
				strChk.Format("%s, lastWriteTime : %s", strFileNameChk, strWorkTime);
				TRACE(strChk);
				
				//strFileLoc.Format("%s\\RT_table\\%s", strCurPath, finder.GetFileName());

				if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
					strFileLoc.Format("%s\\RT_table\\humidity\\%s", strCurPath, finder.GetFileName());
				else
					strFileLoc.Format("%s\\RT_table\\%s", strCurPath, finder.GetFileName());

				file.Open(strFileLoc, CFile::modeRead);
				CArchive ar(&file, CArchive::load);
				if(ar.ReadString(strLine))
				{
					if(strLine.GetLength() == 0)
					{
						return 0;
					}
					else
					{
						TRACE(strLine);
					}
				}
				ar.Close();
				file.Close();
				
				if(strFileNameChk.Compare(strTargetFileName) == 0)
				{
					strFilePath = finder.GetFilePath();
					strFileName = finder.GetFileName();
					return 1;
				}
				nIdx++;
			}
			if(nIdx == m_nTotalRTtableCnt)
				return 0;
		}
	}
	else
	{
		//업데이트 할 파일이 없음
	}
	return 1;
}

BOOL CRTTableSet::ReadRTTableInfo(int nTypeOper)  //yulee 20181118
{//Read file from RT_table
	// TODO: Add your control notification handler code here
	CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
	CString strPath;

	if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
	{
		strPath.Format("%s\\RT_table\\humidity\\*.txt",strCurPath);
	}
	else
	{
		strPath.Format("%s\\RT_table\\*.txt",strCurPath);	
	}	

	CFileFind finder;
	BOOL bFileFind = finder.FindFile(strPath);
	int nIdx=-1;

	
	if(bFileFind == 1)//yulee 20181101 지정 폴더에 파일이 있다면...
	{
		//지정 폴더의 위치의 파일로 이동
		BOOL bSetLocPath;
		
		CString strChk, strLastWriteTime, /*strWorkTime,*/ strTableNameForSBC;
		CTime cLastWriteTime;
		BOOL bGetLastWriteTime;

		CStdioFile fp;
		CFile file;
		CString strFileName, strFileLoc, strLine;
				CString strTmp;



		while(bFileFind == 1)
		{
			bFileFind = finder.FindNextFile();
			if(finder.IsArchived())
			{
// 				bGetLastWriteTime = finder.GetLastWriteTime(cLastWriteTime);
// 				strLastWriteTime = cLastWriteTime.Format(_T("%Y-%m-%d %H:%M:%S"));
				CTime tm = CTime::GetCurrentTime();
				CString strWorkTime;
				strWorkTime = tm.Format(_T("%Y-%m-%d %H:%M:%S"));

				strFileName = finder.GetFileName();
				strChk.Format("%s, lastWriteTime : %s", strFileName, strWorkTime);
				TRACE(strChk);
				
				//strFileLoc.Format("%s\\RT_table\\%s", strCurPath, finder.GetFileName());
				if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
				{
					strFileLoc.Format("%s\\RT_table\\humidity\\%s", strCurPath, finder.GetFileName());
				}
				else
				{
					strFileLoc.Format("%s\\RT_table\\%s", strCurPath, finder.GetFileName());
				}	
				file.Open(strFileLoc, CFile::modeRead);
				CArchive ar(&file, CArchive::load);
				if(ar.ReadString(strLine))
				{
					if(strLine.GetLength() == 0)
					{
						return 0;
					}
					else
					{
						TRACE(strLine);
					}
				}
				ar.Close();
				file.Close();
				
				if(nIdx > -1)
				{
					m_pArrRTtable[nIdx].nIndex = nIdx+1;
					if(nTypeOper == RT_TABLE_INIT_ALL)
						m_pArrRTtable[nIdx].nState = RT_STATE_LATEST;
					else if(nTypeOper == RT_TABLE_UPDATE)
					{
						if(m_nTotalRTtableCnt == nIdx)
							m_pArrRTtable[nIdx].nState = RT_STATE_ADD;
						else
							m_pArrRTtable[nIdx].nState = RT_STATE_LATEST;
					}
					sprintf(m_pArrRTtable[nIdx].charTableName,"%s", strLine.Left(strLine.GetLength()));
					
					/*strWorkTime.Format("-");*/
					CTime tm = CTime::GetCurrentTime();
					CString strWorkTime;
					strWorkTime= tm.Format(_T("%Y-%m-%d %H:%M:%S"));
					if(nTypeOper == RT_TABLE_UPDATE)
					{
						if(m_nTotalRTtableCnt == nIdx)
							strLastWriteTime = strWorkTime;
					}
					sprintf(m_pArrRTtable[nIdx].charWorkTime,"%s", strWorkTime.Left(strWorkTime.GetLength()));
					sprintf(m_pArrRTtable[nIdx].charTableNameForSBC,"%s", strFileName.Left(strFileName.GetLength()));
					strLine.Format("-");
					sprintf(m_pArrRTtable[nIdx].charReserved,"%s", strLine.Left(strLine.GetLength()));
				}

				
				nIdx++;
				/*bSetLocPath = finder.FindNextFile();*/
			}
		}
		m_nTotalRTtableCnt= nIdx;
	}
	else
	{
		//업데이트 할 파일이 없음
	}
	return 1;
}

BOOL CRTTableSet::SetRTTableInfoGrid()  //yulee 20181118
{
	m_listRTable.DeleteAllItems();

	CString strTmp;		//table name for user //CString strModelName

	LPS_RTTABLE_DATA pRTTableListData;
	COleVariant data;
	int nI;
	nI = 0;
	
	while(m_pArrRTtable[nI].nState > RT_STATE_EMPTY)
	{
		pRTTableListData = new S_RTTABLE_DATA;
		pRTTableListData->nIndex = m_pArrRTtable[nI].nIndex; //index

		pRTTableListData->nState = m_pArrRTtable[nI].nState; //nState

		strTmp.Format("%s",m_pArrRTtable[nI].charTableName); //Table Name
		sprintf(pRTTableListData->charTableName,"%s", strTmp.Left(strTmp.GetLength()));

		strTmp.Format("%s",m_pArrRTtable[nI].charWorkTime); //Upload Time
		sprintf(pRTTableListData->charWorkTime,"%s", strTmp.Left(strTmp.GetLength()));

		strTmp.Format("%s",m_pArrRTtable[nI].charTableNameForSBC); //Table Name in SBC
		sprintf(pRTTableListData->charTableNameForSBC,"%s", strTmp.Left(strTmp.GetLength()));

		strTmp.Format("%s",m_pArrRTtable[nI].charReserved); //Reserved
		sprintf(pRTTableListData->charReserved,"%s", strTmp.Left(strTmp.GetLength()));

		SetListItem(nI, pRTTableListData);

		nI++;
	}
	return 1;
}

BOOL CRTTableSet::SetListItem(int nI, LPS_RTTABLE_DATA pReservData)
{
	LVITEM lvItem;
	TCHAR szBuff[256] = {0,};
	CString strTmp;
	
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;

	lvItem.iItem = nI;
	lvItem.iSubItem = LIST_RT_INDEX;

	sprintf(szBuff,"%d", pReservData->nIndex);
	lvItem.pszText = szBuff; 		
	m_listRTable.InsertItem(&lvItem);


	lvItem.iSubItem = LIST_RT_STATE;
	sprintf(szBuff,"%s", ConvertStateCodeToString(pReservData->nState));
	lvItem.pszText = szBuff; 		
	m_listRTable.SetItem(&lvItem);
		
	lvItem.iSubItem = LIST_RT_TABLE_NAME;
	sprintf(szBuff,"%s", pReservData->charTableName);
	lvItem.pszText = szBuff; 
	m_listRTable.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_RT_UPLOAD_TIME;
	sprintf(szBuff,"%s", pReservData->charWorkTime);
	lvItem.pszText = szBuff; 
	m_listRTable.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_RT_TABLE_SBC_NAME;
	sprintf(szBuff,"%s", pReservData->charTableNameForSBC);
	lvItem.pszText = szBuff; 
	m_listRTable.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_RT_RESERVED;
	sprintf(szBuff,"%s", pReservData->charReserved);
	lvItem.pszText = szBuff; 
	m_listRTable.SetItem(&lvItem);

	return TRUE;
}

/*
#define RESERV_STATE_EMPTY			0 //공란
#define RESERV_STATE_LATEST			1 //최신
#define RESERV_STATE_MODIFIED		2 //수정
#define RESERV_STATE_ADD			3 //추가
#define RT_STATE_ONTEST			4 //사용 중
*/

//상태 코드를 문자열로 반환
CString CRTTableSet::ConvertStateCodeToString(int nState)
{
	switch(nState)
	{
	case 0:
		//return "공란";	
		return Fun_FindMsg("RTTableSet_ConvertStateCodeToString_msg1","IDD_RTTABLE_SET_DLG");	//&&
	case 1:
		//return "최신";	
		return Fun_FindMsg("RTTableSet_ConvertStateCodeToString_msg2","IDD_RTTABLE_SET_DLG");	//&&
	case 2:
		//return "교체";
		return Fun_FindMsg("RTTableSet_ConvertStateCodeToString_msg3","IDD_RTTABLE_SET_DLG");	//&&
	case 3:
		//return "추가";
		return Fun_FindMsg("RTTableSet_ConvertStateCodeToString_msg4","IDD_RTTABLE_SET_DLG");	//&&
	case 4:
		//return "사용 중";
		return Fun_FindMsg("RTTableSet_ConvertStateCodeToString_msg5","IDD_RTTABLE_SET_DLG");	//&&
	
	default:
		//return "알 수 없음";
		return Fun_FindMsg("RTTableSet_ConvertStateCodeToString_msg6","IDD_RTTABLE_SET_DLG");	//&&
	}
}

BOOL CRTTableSet::UpdateTableInfo()  //yulee 20181118
{
	BOOL bUsingRTTable[LIST_MAX_FILE_NUM];
	if(CheckUsingTable(bUsingRTTable) != FALSE)
	{
		for(int i=0; i<m_nTotalRTtableCnt; i++)
		{
			if(bUsingRTTable[i] == TRUE)
				m_pArrRTtable[i].nState = RT_STATE_ONTEST;
		}
	}
	SetRTTableInfoGrid();

	return 1;
}
BOOL CRTTableSet::CheckUsingTable(BOOL *bRTtable)  //yulee 20181118
{

	int arrRetChkCh1[LIST_MAX_FILE_NUM], arrRetChkCh2[LIST_MAX_FILE_NUM], arrRetChkCh3[LIST_MAX_FILE_NUM], arrRetChkCh4[LIST_MAX_FILE_NUM];
	for(int i=0; i<LIST_MAX_FILE_NUM; i++)
	{
		arrRetChkCh1[i] = 0;
		arrRetChkCh2[i] = 0;
		arrRetChkCh3[i] = 0;
		arrRetChkCh4[i] = 0;
	}

	CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
	CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
	if(pView->CheckAuxRTtableSet(arrRetChkCh1, arrRetChkCh2, arrRetChkCh3, arrRetChkCh4) == FALSE)
		return 0;
	else
	{
		for(int i=0; i<LIST_MAX_FILE_NUM;i++)
		{
			if((arrRetChkCh1[i-1] == TRUE)||
				(arrRetChkCh2[i-1] == TRUE)||
				(arrRetChkCh3[i-1] == TRUE)||
				(arrRetChkCh4[i-1] == TRUE))
			{
				bRTtable[i-1] = TRUE;
			}
		}
	}
	return 1;
}

void CRTTableSet::OnButRttableAdd() 
{
	m_ctrlRTtableProgress.SetPos(0);
	CString strTemp;
	//strTemp.Format("Info:Table 추가하기를 진행합니다.");
	strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg1","IDD_RTTABLE_SET_DLG"));//&&
	GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);

	SetWndEnable(0);
	if(m_nTotalRTtableCnt == LIST_MAX_FILE_NUM)
	{
		//AfxMessageBox("더 이상 추가 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg2","IDD_RTTABLE_SET_DLG"));//&&
		SetWndEnable(1);
		//strTemp.Format("Info:Table 추가하기를 실패하였습니다.");
		strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg3","IDD_RTTABLE_SET_DLG"));//&&
		GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
		return;
	}

	CString strFile;
	CString strFileName;

	CFileDialog pDlg(TRUE, "txt", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "txt file(*.txt)|*.txt|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = "PNE CTSMonitor - RTtable Add";
	if(IDOK == pDlg.DoModal())
	{
 		strFile = pDlg.GetPathName();
		strFileName = pDlg.GetFileName();
	}

	int nErrFlag, nErrNum = 0;
	CString ErrLoc;
	if(RTTableFormatCheck(strFile, nErrNum, ErrLoc) == FALSE)
	{
/*		AfxMessageBox("파일의 형식이 맞지 않습니다.");*/
		//strTemp.Format("Info:Table 변경하기를 실패하였습니다.");
		strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg4","IDD_RTTABLE_SET_DLG"));//&&
		GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
		//strTemp.Format("파일의 형식이 맞지 않습니다. \n이상 위치 %s(줄)", ErrLoc);
		strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg5","IDD_RTTABLE_SET_DLG"), ErrLoc); //&&
		AfxMessageBox(strTemp);
		SetWndEnable(1);
		m_ctrlRTtableProgress.SetPos(0);
		return;
	}

	m_ctrlRTtableProgress.SetPos(20);
	CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");	
	CString strDestPath;
	strDestPath.Format("%s\\RT_table",strCurPath);

	CString strTmp, strCopyFileName;
	if(m_nTotalRTtableCnt < (LIST_MAX_FILE_NUM)-1)
	{
		strTmp.Format("\\th_table_%02d.txt", m_nTotalRTtableCnt+1);
		strCopyFileName.Format("th_table_%02d.txt", m_nTotalRTtableCnt+1);
	}
	else if(m_nTotalRTtableCnt == (LIST_MAX_FILE_NUM)-1)
	{
		strTmp.Format("\\th_table_%d.txt", m_nTotalRTtableCnt+1);
		strCopyFileName.Format("th_table_%02d.txt", m_nTotalRTtableCnt+1);
	}



	strDestPath += strTmp;

	BOOL bCopySuccess = CopyFile(strFile, strDestPath, TRUE);
	if(!bCopySuccess)
	{
		CString tmpStr;
		//tmpStr.Format("같은 이름의 파일이 존재 합니다. \n 덮어쓰시겠습니까?");
		tmpStr.Format(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg6","IDD_RTTABLE_SET_DLG"));//&&
		if(AfxMessageBox(tmpStr, MB_OKCANCEL) == IDOK)
			CopyFile(strFile, strDestPath, FALSE);
		else
		{
			SetWndEnable(1);
			return;
		}
	}

	m_ctrlRTtableProgress.SetPos(40);
	CString tmpStr;
	tmpStr.Format("BefName : %s, AftName : %s", strFile, strDestPath);
	TRACE(tmpStr);

	if(ReadRTTableInfo(RT_TABLE_UPDATE) == TRUE)
	{
		if(SetRTTableInfoGrid() == TRUE)
		{
			m_ctrlRTtableProgress.SetPos(60);
			strTemp.Format("AddFile");
			if(SendRTTableInfotoSBC(strCopyFileName, strTemp, RT_WORK_STATE_ON_ADD) == FALSE)
			{
				//실패 시 원복
				//strTemp.Format("Info:Table 추가하기를 실패하였습니다.");
				strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg7","IDD_RTTABLE_SET_DLG")); //&&
				GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
				return;
			}
			else
			{
				//strTemp.Format("Info:Table 추가하기를 완료하였습니다.");
				strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableAdd_msg8","IDD_RTTABLE_SET_DLG"));//&&
				GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
				m_ctrlRTtableProgress.SetPos(100);
				return;
			}
		}
	}
}

void CRTTableSet::OnButRttableModi() 
{
	m_ctrlRTtableProgress.SetPos(0);
	CString strTemp;
	//strTemp.Format("Info:Table 교체하기를 진행합니다.");
	strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableModi_msg1","IDD_RTTABLE_SET_DLG"));//&&
	GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);

	POSITION pos = m_listRTable.GetFirstSelectedItemPosition();
	int nItem;
	
	while(pos)
	{
		nItem = m_listRTable.GetNextSelectedItem(pos);	
		BOOL bSelect=FALSE;
		if((nItem >= 0) && (nItem <= m_nTotalRTtableCnt))
				bSelect = TRUE;
		if((m_pArrRTtable[nItem].charTableName != NULL) && (bSelect ==TRUE))
		{
			if(m_pArrRTtable[nItem].nState == RT_STATE_ONTEST)
			{
				//AfxMessageBox("실험에 사용 중입니다. 교체하실 수 없습니다.");
				AfxMessageBox(Fun_FindMsg("RTTableSet_OnButRttableModi_msg2","IDD_RTTABLE_SET_DLG"));//&&
				return;
			}
			else
			{
				//1. 선택한 파일을 백업 한다.
				SetWndEnable(0);
				CString strBackFileName, strFileDir, strFileName, strBackupFilePath;
				strBackFileName.Format("%s", m_pArrRTtable[nItem].charTableNameForSBC);
				if(BackupFileforChange(strBackFileName, strFileDir, strFileName, strBackupFilePath) == FALSE)
				{
					//AfxMessageBox("기존 파일 백업에 실패하였습니다.");
					AfxMessageBox(Fun_FindMsg("RTTableSet_OnButRttableModi_msg3","IDD_RTTABLE_SET_DLG"));//&&
					//strTemp.Format("Info:Table 교체하기를 실패하였습니다.");
					strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableModi_msg4","IDD_RTTABLE_SET_DLG"));//&&
					GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
					return;
				}
				else
				{
					m_ctrlRTtableProgress.SetPos(20);
					//2. 새로운 파일 복사, 이름변경
					if(CopyNewFileforChange(m_pArrRTtable[nItem].nIndex) == FALSE)
					{
						//strTemp.Format("Info:Table 교체하기가 취소되었습니다.");
						strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableModi_msg5","IDD_RTTABLE_SET_DLG"));//&&
						GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
						SetWndEnable(1);
						if(DeleteFile(strBackupFilePath) == TRUE)
							return;	
					}
					else
					{
						//3. 배열 업데이트
						if(ChangeRTTableArr(m_pArrRTtable[nItem].nIndex, strFileDir, strFileName) == FALSE)
						{
						}
						else
						{
							m_ctrlRTtableProgress.SetPos(40);
							if(SetRTTableInfoGrid() == FALSE)
							{
							}
							else
							{
								//5. SBC 업데이트
								//6. mdb 업데이트
								m_ctrlRTtableProgress.SetPos(60);
								if(SendRTTableInfotoSBC(strFileName, strBackFileName, RT_WORK_STATE_ON_CHANGE) == FALSE)
								{//실패 시 원복
									
									//AfxMessageBox("기존 파일 원상복구를 진행합니다.");
									AfxMessageBox(Fun_FindMsg("RTTableSet_OnButRttableModi_msg6","IDD_RTTABLE_SET_DLG"));//&&
									strTemp.Format("Info:Table 원상복구 중입니다.");
									GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
									if(BackupFileforChange(strBackFileName, strFileDir, strFileName, strBackupFilePath, 1) == FALSE)
									{
										//AfxMessageBox("기존 파일 원상복구에 실패하였습니다.");
										AfxMessageBox(Fun_FindMsg("RTTableSet_OnButRttableModi_msg7","IDD_RTTABLE_SET_DLG"));//&&
										return;
									}
									return;
								}
								else
								{
									m_ctrlRTtableProgress.SetPos(100);
									//strTemp.Format("Info:Table 교체하기를 완료하였습니다.");
									strTemp.Format(Fun_FindMsg("RTTableSet_OnButRttableModi_msg8","IDD_RTTABLE_SET_DLG"));//&&
									GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
									return;	
								}
							}
						}
					}
				}
			}
		}
		else
		{
			//AfxMessageBox("교체하실 항목(Table)을\n 리스트에서 선택해 주시기 바랍니다.");
			AfxMessageBox(Fun_FindMsg("RTTableSet_OnButRttableModi_msg9","IDD_RTTABLE_SET_DLG")); //&&
			return;
		}
	}

	if(!pos)
	{
		//AfxMessageBox("교체하실 항목(Table)을\n 리스트에서 선택해 주시기 바랍니다.");
		AfxMessageBox(Fun_FindMsg("RTTableSet_OnButRttableModi_msg10","IDD_RTTABLE_SET_DLG"));//&&
		return;
	}
}

BOOL CRTTableSet::SendRTTableInfotoSBC(CString strFileName, CString &strBackupFileNname, int nTypeOper)
{
	CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
	CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
	//if(pView->UploadRTTableFromSBC(1, strFileName, RT_TABLE_UPDATE) == FALSE)
	if(pView->UploadRTTableFromSBC(1, strFileName, RT_TABLE_UPDATE, m_nSelectedRtType) == FALSE) //ksj 20200207
	{
		//AfxMessageBox("추가한 테이블 업로드에 실패하였습니다.");
		AfxMessageBox(Fun_FindMsg("RTTableSet_SendRTTableInfotoSBC_msg1","IDD_RTTABLE_SET_DLG"));//&&
		SetWndEnable(1);
	}
	else
	{
		//SBC에 RT_Table 업로드 했다고 알리기 
				
		/*SFT_RT_TABLE_SEND_END RT_TableSendEnd;
		ZeroMemory(&RT_TableSendEnd, sizeof(SFT_RT_TABLE_SEND_END));
		
 		CString strTableName, strTableNum;
		int nI1, nI2;
		nI1 = strFileName.GetLength();
		nI2 = nI1 - (strFileName.Find('.')-2);
 		strTableName.Format("%s", strFileName.Right(nI2));
		strTableNum.Format("%s", strTableName.Left(2));
		RT_TableSendEnd.TableNum = atoi(strTableNum);
		
		int nSelModule = 1;
		
		CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
		CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
		//if(pView->SetRTTableSendToModule(nSelModule, &RT_TableSendEnd) == FALSE)
		if(pView->SetRTTableSendToModule(nSelModule, &RT_TableSendEnd, m_nSelectedRtType) == FALSE) //ksj 20200207
		{
			//AfxMessageBox("RTtable 정보 설정에 실패했습니다.(SBC 전송)");
			AfxMessageBox(Fun_FindMsg("RTTableSet_SendRTTableInfotoSBC_msg2","IDD_RTTABLE_SET_DLG"));//&&
			SetWndEnable(1);
			return 0;
		}*/

		//ksj 20200207
		LPVOID pTableSendEnd = NULL;
		if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY)
		{
			SFT_RT_HUMI_TABLE_SEND_END RT_TableSendEnd;
			ZeroMemory(&RT_TableSendEnd, sizeof(SFT_RT_HUMI_TABLE_SEND_END));

			CString strTableName, strTableNum;
			int nI1, nI2;
			nI1 = strFileName.GetLength();
			nI2 = nI1 - (strFileName.Find('.')-2);
			strTableName.Format("%s", strFileName.Right(nI2));
			strTableNum.Format("%s", strTableName.Left(2));
			RT_TableSendEnd.TableNum = atoi(strTableNum);

			pTableSendEnd = &RT_TableSendEnd;
		}
		else
		{
			SFT_RT_TABLE_SEND_END RT_TableSendEnd;
			ZeroMemory(&RT_TableSendEnd, sizeof(SFT_RT_TABLE_SEND_END));

			CString strTableName, strTableNum;
			int nI1, nI2;
			nI1 = strFileName.GetLength();
			nI2 = nI1 - (strFileName.Find('.')-2);
			strTableName.Format("%s", strFileName.Right(nI2));
			strTableNum.Format("%s", strTableName.Left(2));
			RT_TableSendEnd.TableNum = atoi(strTableNum);
		
			pTableSendEnd = &RT_TableSendEnd;
		}

		int nSelModule = 1; //이거 위험. 모듈 하나만 고려되어있네

		CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
		CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
		//if(pView->SetRTTableSendToModule(nSelModule, &RT_TableSendEnd) == FALSE)
		if(pView->SetRTTableSendToModule(nSelModule, pTableSendEnd, m_nSelectedRtType) == FALSE) //ksj 20200207
		{
			//AfxMessageBox("RTtable 정보 설정에 실패했습니다.(SBC 전송)");
			AfxMessageBox(Fun_FindMsg("RTTableSet_SendRTTableInfotoSBC_msg2","IDD_RTTABLE_SET_DLG"));//&&
			SetWndEnable(1);
			return 0;
		}


		m_bRetCnt = 0;
		m_strBackupFileName = strBackupFileNname;
		m_ctrlRTtableProgress.SetPos(80);
		if(nTypeOper==RT_WORK_STATE_ON_ADD)
		{
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "RTTable Recv Ret", 2); //yulee 20181126
			KillTimer(TIMER_RTTABLE_SET_SBC_RECV);
			struct ST_TIMER_RTTABLE_SET_SBC_RECV{
				static void CALLBACK TimeRttableSetRecvCheckRetry(HWND hwnd, UINT iMsg, UINT_PTR wParam, DWORD lParam)
				{
					::KillTimer(hwnd, wParam);
					CRTTableSet *dlg = (CRTTableSet*)CWnd::FromHandle(hwnd);
					if(!dlg) return;
					
					
					int bRet= 0;
					bRet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "RTTable Recv Ret", 2);
					if(bRet == 1)
					{
						dlg->SetRTtableNametoDB();
					}
					else if(bRet == 0)
					{
						dlg->ResetRTWnd(dlg->m_strBackupFileName);
					}
					else if(bRet == 2)
					{
						dlg->m_bRetCnt++;
						if(dlg->m_bRetCnt > SBC_RETRUN_COUNT)
						{
							dlg->ResetRTWnd(dlg->m_strBackupFileName);
						}
					}
					
				}};
				SetTimer(TIMER_RTTABLE_SET_SBC_RECV, 500, ST_TIMER_RTTABLE_SET_SBC_RECV::TimeRttableSetRecvCheckRetry);
		}
		else if(nTypeOper==RT_WORK_STATE_ON_CHANGE)
		{
			AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "RTTable Recv Ret", 2); //yulee 20181126
			KillTimer(TIMER_RTTABLE_SET_SBC_RECV);
			struct ST_TIMER_RTTABLE_SET_SBC_RECV{
				static void CALLBACK TimeRttableSetRecvCheckRetry(HWND hwnd, UINT iMsg, UINT_PTR wParam, DWORD lParam)
				{
					::KillTimer(hwnd, wParam);
					CRTTableSet *dlg = (CRTTableSet*)CWnd::FromHandle(hwnd);
					if(!dlg) return;
					
					
					int bRet = 2;
					bRet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "RTTable Recv Ret", 2);
					if(bRet == 1)
					{
						dlg->SetRTtableNametoDB();
					}
					else if(bRet == 0)
					{
						dlg->ResetRTtable(dlg->m_strBackupFileName);
					}
					else if(bRet == 2)
					{
						dlg->m_bRetCnt++;
						if(dlg->m_bRetCnt > SBC_RETRUN_COUNT)
						{
							dlg->ResetRTtable(dlg->m_strBackupFileName);
						}
					}
					
				}};
				SetTimer(TIMER_RTTABLE_SET_SBC_RECV, 500, ST_TIMER_RTTABLE_SET_SBC_RECV::TimeRttableSetRecvCheckRetry);
		}
	  }
}

void CRTTableSet::SetSBCRTTableAckRecv(int retFromSBC)
{
// 	SFT_MUX_SELECT_RESPONSE sMuxResponse;
// 	memcpy(&sMuxResponse,(SFT_MUX_SELECT_RESPONSE *)lParam,sizeof(SFT_MUX_SELECT_RESPONSE));
	
	if(retFromSBC == 1)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "RTTable Recv Ret", 1);
	}
	else if(retFromSBC == 0)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "RTTable Recv Ret", 0);
	}
	else
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "RTTable Recv Ret", 2);
	}

	return;
}

//ksj 20200207 : 무슨용도? 기존 온도 로직이랑 짝맞춰 구현.
void CRTTableSet::SetSBCRTHumiTableAckRecv(int retFromSBC)
{
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "RTHumiTable Recv Ret", retFromSBC);
	
	return;
}


void CRTTableSet::SetWndEnable(int nTypeOper)
{
	if(nTypeOper == 1)
	{
		GetDlgItem(IDC_BUT_RTTABLE_ADD)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUT_RTTABLE_MODI)->EnableWindow(TRUE);
		m_ctrlRTtableProgress.SetPos(0);
	}
	else if(nTypeOper == 0)
	{
		GetDlgItem(IDC_BUT_RTTABLE_ADD)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_RTTABLE_MODI)->EnableWindow(FALSE);
	}
}

void CRTTableSet::SetRTtableNametoDB()
{
	KillTimer(TIMER_RTTABLE_SET_SBC_RECV);
	SetWndEnable(1);
	CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
	CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
	int nCode, nDIvision;
	CString strCodeName;

	if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
	{
		nDIvision = 4;
	}
	else
	{
		nDIvision = 3;
	}

	BOOL bRet;
	//bRet = pView->DeleteRTTableNameDB();
	bRet = pView->DeleteRTTableNameDB(nDIvision);

	if(bRet == TRUE)
	{
		for(int i=0; i< m_nTotalRTtableCnt; i++)
		{
			nCode = m_pArrRTtable[i].nIndex; //index	
			strCodeName = m_pArrRTtable[i].charTableName;
			//nDIvision = 3;

			CString sReserved;
			sReserved.Format("Update");

			if(pView->UpdateRTTableDB(nCode, strCodeName, nDIvision, sReserved) == FALSE)
			{
				//실패 시 예외처리
				return;
			}
		}
	}
}

void CRTTableSet::ResetRTtable(CString &strName)
{
	KillTimer(TIMER_RTTABLE_SET_SBC_RECV);
	SetWndEnable(1);
	m_bRetCnt = 0;

	CString strBackFileName, strFileDir, strFileName, strBackupFilePath, strTemp;
	//AfxMessageBox("업로드에 실패하여 기존 파일 원상복구를 진행합니다.");
	AfxMessageBox(Fun_FindMsg("RTTableSet_ResetRTtable_msg1","IDD_RTTABLE_SET_DLG"));//&&
	//strTemp.Format("Info:Table 원상복구 중입니다.");
	strTemp.Format(Fun_FindMsg("RTTableSet_ResetRTtable_msg2","IDD_RTTABLE_SET_DLG"));//&&
	GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
	if(BackupFileforChange(strName, strFileDir, strFileName, strBackupFilePath, 1) == FALSE)
	{
		//AfxMessageBox("기존 파일 원상복구에 실패하였습니다.");
		AfxMessageBox(Fun_FindMsg("RTTableSet_ResetRTtable_msg3","IDD_RTTABLE_SET_DLG"));//&&
		return;
	}
	else
	{
		if(SetRTTableList() != FALSE)
		{
			UpdateTableInfo();
			SetRTtableNametoDB();
		}
	//	SetRTTableInfo();
	//	UpdateTableInfo();
	//	SetRTtableNametoDB();
		//strTemp.Format("Info:SBC 업로드에 실패하여 Table 원상복구하였습니다.");
		strTemp.Format(Fun_FindMsg("RTTableSet_ResetRTtable_msg4","IDD_RTTABLE_SET_DLG"));//&&
		GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
	}
}

void CRTTableSet::ResetRTWnd(CString &strName)
{
	KillTimer(TIMER_RTTABLE_SET_SBC_RECV);
	SetWndEnable(1);
	m_bRetCnt = 0;
	CString strTemp;
	//strTemp.Format("Info:SBC 업로드에 실패하여 Table 정보를 다시 가져왔습니다.");
	strTemp.Format(Fun_FindMsg("RTTableSet_ResetRTWnd_msg1","IDD_RTTABLE_SET_DLG"));//&&
	GetDlgItem(IDC_STATIC_WORKSTATUS)->SetWindowText(strTemp);
	if(SetRTTableList() != FALSE)
	{
		UpdateTableInfo();
		SetRTtableNametoDB();
	}
}

BOOL CRTTableSet::BackupFileforChange(CString &strReadFileName, CString &FilePath, CString &FileName, CString &BackupFilePath, int nTypeOper)
{
	ReadFileInfo(strReadFileName, FilePath, FileName);
	
	CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");	
	
// 	CString strRTtablePath;
// 	strRTtablePath.Format("%s\\RT_table",strCurPath);
// 	
// 	CString strSourceFile;
// 	strSourceFile.Format("%s\\%s", strRTtablePath, FileName);
// 	
// 	CString strDest, strDestPath;
// 	strDest.Format("%s\\RT_table\\Tmp", strCurPath);
// 	strDestPath.Format("%s\\%s", strDest ,FileName); //백업 주소 //C:\Program Files (x86)\PNE CTSPack\RT_table\Tmp 
// 	BackupFilePath = strDestPath;


	//ksj 20200207
	CString strRTtablePath;
	CString strSourceFile;
	CString strDest, strDestPath;
	if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY)
	{
		CString strRTtablePath;
		strRTtablePath.Format("%s\\RT_table\\Humidity",strCurPath);
		GFile::file_ForceDirectory(strRTtablePath); //ksj 20200207				
		strSourceFile.Format("%s\\%s", strRTtablePath, FileName);
		 	
		strDest.Format("%s\\RT_table\\Humidity\\Tmp", strCurPath);
		strDestPath.Format("%s\\%s", strDest ,FileName); //백업 주소 //C:\Program Files (x86)\PNE CTSPack\RT_table\Tmp 
		BackupFilePath = strDestPath;
		GFile::file_ForceDirectory(strDest); //ksj 20200207
	}
	else //PS_AUX_TYPE_TEMPERATURE
	{
		CString strRTtablePath;
		strRTtablePath.Format("%s\\RT_table",strCurPath);
		GFile::file_ForceDirectory(strRTtablePath); //ksj 20200207			
		strSourceFile.Format("%s\\%s", strRTtablePath, FileName);
		 	
		strDest.Format("%s\\RT_table\\Tmp", strCurPath);
		strDestPath.Format("%s\\%s", strDest ,FileName); //백업 주소 //C:\Program Files (x86)\PNE CTSPack\RT_table\Tmp 
		BackupFilePath = strDestPath;
		GFile::file_ForceDirectory(strDest); //ksj 20200207
	}



	//해당 디렉토리가 없으면
	CFileFind file;
	CString strFile = "*.*", strChkPath;
	strChkPath.Format("%s\\%s", strDest, strFile);
	BOOL bResult = file.FindFile(strChkPath);

	if(!bResult)
	{
		bResult = CreateDirectory(strDest + "\\", NULL);
		if(!bResult)
			return 0;
	}

	BOOL bCopySuccess;
	if(nTypeOper == 0)
	{
		bCopySuccess = CopyFile(strSourceFile, strDestPath, FALSE);//무조건 기존 파일 덮어쓰기
	}
	else if(nTypeOper == 1)
	{
		bCopySuccess = CopyFile(strDestPath, strSourceFile, FALSE);//무조건 기존 파일 덮어쓰기
	}
	if(bCopySuccess == FALSE)
		return 0;
	CString tmpStr;
	tmpStr.Format("BefName : %s, AftName : %s", strSourceFile, strDestPath);
	TRACE(tmpStr);

	return 1;
}

BOOL CRTTableSet::CopyNewFileforChange(int nIndex)
{
	CString strFile;
	CString strFileName;
	
	CFileDialog pDlg(TRUE, "txt", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "txt file(*.txt)|*.txt|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = "PNE CTSMonitor - RTtable Add";
	if(IDOK == pDlg.DoModal())
	{
		strFile = pDlg.GetPathName();
		strFileName = pDlg.GetFileName();
	}
	else
	{
		return 0;
	}
	
	CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");	
	CString strDestPath;
//	strDestPath.Format("%s\\RT_table",strCurPath);

	//ksj 20200207
	if(m_nSelectedRtType == PS_AUX_TYPE_HUMIDITY)
	{
		strDestPath.Format("%s\\Humidity_table",strCurPath);
	}
	else //PS_AUX_TYPE_TEMPERATURE
	{
		strDestPath.Format("%s\\RT_table",strCurPath);
	}
	//ksj end

	CString strCopyFileName;
	if(m_nTotalRTtableCnt < (LIST_MAX_FILE_NUM)-1)
		strCopyFileName.Format("\\th_table_%02d.txt", nIndex);
	else if(m_nTotalRTtableCnt == (LIST_MAX_FILE_NUM)-1)
		strCopyFileName.Format("\\th_table_%02d.txt", nIndex); //lyj 20210112

	
	
	strDestPath += strCopyFileName;
	
	BOOL bCopySuccess = CopyFile(strFile, strDestPath, TRUE);
	if(!bCopySuccess)
	{
		CString tmpStr;
		//tmpStr.Format("파일을 덮어쓰시겠습니까?");
		tmpStr.Format(Fun_FindMsg("RTTableSet_CopyNewFileforChange_msg1","IDD_RTTABLE_SET_DLG"));//&&
		if(AfxMessageBox(tmpStr, MB_OKCANCEL) == IDOK)
			CopyFile(strFile, strDestPath, FALSE);
		else
		{
			SetWndEnable(1);
			return 0;
		}
	}

	int nErrFlag, nErrNum = 0;
	CString ErrLoc;
	if(RTTableFormatCheck(strFile, nErrNum, ErrLoc) == FALSE)
	{
		
		//AfxMessageBox("파일의 형식이 맞지 않습니다.");
		AfxMessageBox(Fun_FindMsg("RTTableSet_CopyNewFileforChange_msg2","IDD_RTTABLE_SET_DLG"));//&&
		CString tmpStr;
		//tmpStr.Format("이상 위치 %s(줄)", ErrLoc);
		tmpStr.Format(Fun_FindMsg("RTTableSet_CopyNewFileforChange_msg3","IDD_RTTABLE_SET_DLG"), ErrLoc);//&&
		AfxMessageBox(tmpStr);
		return 0;
	}
	
	CString tmpStr;
	tmpStr.Format("BefName : %s, AftName : %s", strFile, strDestPath);
	TRACE(tmpStr);
	return 1;
}

BOOL CRTTableSet::ChangeRTTableArr(int &nIndex, CString &strFilePath, CString &strFileName)
{
	CFile file;
	CString strLine;
	CString strWorkTime;
	file.Open(strFilePath, CFile::modeRead);
	CArchive ar(&file, CArchive::load);
	if(ar.ReadString(strLine))
	{
		if(strLine.GetLength() == 0)
		{
			return 0;
		}
		else
		{
			TRACE(strLine);
		}
	}
	ar.Close();
	file.Close();
	int nIArr;
	nIArr = nIndex -1;
	
	m_pArrRTtable[nIArr].nIndex = nIndex;
	m_pArrRTtable[nIArr].nState = RT_STATE_MODIFIED;
	sprintf(m_pArrRTtable[nIArr].charTableName,"%s", strLine.Left(strLine.GetLength()));
	
	CTime tm = CTime::GetCurrentTime();
	strWorkTime = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	sprintf(m_pArrRTtable[nIArr].charWorkTime,"%s", strWorkTime.Left(strWorkTime.GetLength()));
	sprintf(m_pArrRTtable[nIArr].charTableNameForSBC,"%s", strFileName.Left(strFileName.GetLength()));
	strLine.Format("-");
	sprintf(m_pArrRTtable[nIArr].charReserved,"%s", strLine.Left(strLine.GetLength()));
}

BOOL CRTTableSet::IsSpaceInStr(CString &strCheck)
{
	int nStrLen = strCheck.GetLength();
	char* st= new char[nStrLen];
	char  ch;
	BOOL bIsSpace;
	CString strTmp;
	strcpy(st, strCheck.GetBuffer(0));

	bIsSpace = 0;
	for(int i=0; i<strCheck.GetLength(); i++)
	{
		ch = st[i];
		if(isspace(ch))
		{
			//strTmp.Format("%c(x%02x)는 공백문자.\n", ch, ch);
			strTmp.Format(Fun_FindMsg("RTTableSet_IsSpaceInStr_msg1","IDD_RTTABLE_SET_DLG"), ch, ch);//&&
			TRACE(strTmp);
			bIsSpace = TRUE;
			return TRUE;
		}
		else
		{
			
		}
		if(i == nStrLen)
		{
			if(bIsSpace == TRUE)
				return TRUE;
			else
				return FALSE;
		}
	}
	return FALSE;
}

BOOL CRTTableSet::RTTableFormatCheck(CString &strFilePath, int &nErrNum, CString &ErrLoc)
{

	int nErrFlag=0;
	CFile file;
	CString strLine, strPreLine;
	int nILine = 1;
	strLine.Format("");
	BOOL bErrRet;
	CString strWorkTime;
	file.Open(strFilePath, CFile::modeRead);
	CArchive ar(&file, CArchive::load);
	CString tmpStr1, tmpStr2, tmpStr3;
	ErrLoc= "";

	while(ar.ReadString(strLine))
	{
		if(strLine.GetLength() == 0)
		{
			if((nILine !=6) && (nILine !=11))
				return 0;
		}
		else
		{
			TRACE(strLine);
			if(nILine == 1)
			{
				bErrRet = IsSpaceInStr(strLine);
				if(bErrRet)
				{
					nErrFlag = 1;
					nErrNum++;
					tmpStr3.Format("%d,", nILine);
					ErrLoc += tmpStr3;
				}
			}
// 			else if((nILine ==6)||(nILine ==11))
// 			{
// 				if(strLine != "")
// 				{
// 					nErrFlag = 1;
// 					nErrNum++;
// 					tmpStr3.Format("%d,", nILine);
// 					ErrLoc += tmpStr3;
// 				}
// 			}
			else if((nILine >= 7)&&(nILine <=10))
			{
				if(nILine == 7)
				{
					if(strLine.Compare("Bias_type\t:\t0") != 0)
					{
						nErrFlag = 1;
						nErrNum++;
						tmpStr3.Format("%d,", nILine);
						ErrLoc += tmpStr3;
					}
				}
				else if(nILine == 8)
				{
					if(strLine.Compare("Vref[mV]\t:\t5000") != 0)
					{
						nErrFlag = 1;
						nErrNum++;
						tmpStr3.Format("%d,", nILine);
						ErrLoc += tmpStr3;
					}
				}
				else if(nILine == 9)
				{
					if(strLine.Compare("R1[ohm]\t\t:\t8200") != 0)
					{
						nErrFlag = 1;
						nErrNum++;
						tmpStr3.Format("%d,", nILine);
						ErrLoc += tmpStr3;
					}
				}
				else if(nILine == 10)
				{
					if(strLine.Compare("R2[ohm]\t\t:\t0") != 0)
					{
						nErrFlag = 1;
						nErrNum++;
						tmpStr3.Format("%d,", nILine);
						ErrLoc += tmpStr3;
					}
				}
			}
			else if(nILine ==12)
			{
				if(strLine.Compare("temp_degreeC\tR_kOhm") != 0)
				{
					nErrFlag = 1;
					nErrNum++;
					tmpStr3.Format("%d,", nILine);
					ErrLoc += tmpStr3;
				}
			}
			else if(nILine > 12)
			{
				tmpStr1.Format("%s", strLine.Left(strLine.Find('\t')));
				tmpStr2.Format("%s", strLine.Mid(strLine.Find('\t')+1));
				
				for(int i=0; i<tmpStr1.GetLength(); i++)
				{
					if((isdigit(tmpStr1.GetAt(i)) == FALSE) && (tmpStr1.GetAt(i) != 055) && (tmpStr1.GetAt(i) != 056))
					{
						nErrFlag = 1;
						nErrNum++;
						tmpStr3.Format("%d,", nILine);
						ErrLoc += tmpStr3;
					}
				}
				for(int i=0; i<tmpStr2.GetLength(); i++)
				{
					if((isdigit(tmpStr2.GetAt(i)) == FALSE) && (tmpStr2.GetAt(i) != 055) && (tmpStr2.GetAt(i) != 056))
					{
						nErrFlag = 1;
						nErrNum++;
						tmpStr3.Format("%d,", nILine);
						ErrLoc += tmpStr3;
					}
				}
			}
		strPreLine = strLine;
		}
		nILine++;
	}

	if(nILine > (RT_TABLE_MAX_LINE-12))
	{
		nErrFlag = 1;
		nErrNum++;
		tmpStr3.Format("Max Line over(max line:%s),", (RT_TABLE_MAX_LINE-12));
		ErrLoc += tmpStr3;
	}
	if(strPreLine.Compare("9999") != 0)
	{
		nErrFlag = 1;
		nErrNum++;
		tmpStr3.Format("Last Line Error(last line tmep degreeC:9999)");
		ErrLoc += tmpStr3;
	}
	
	ar.Close();
	file.Close();

	if(nErrFlag)
		return 0;
	else
		return 1;
}

BOOL CRTTableSet::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	CDialog::OnDestroy();
	
	CRect rect;
	GetWindowRect(&rect);
	CWinApp* pApp = AfxGetApp();
	
	pApp->WriteProfileInt(CT_CONFIG_REG_SEC, "RTEdit Window Height",rect.Width());
	pApp->WriteProfileInt(CT_CONFIG_REG_SEC, "RTEdit Window Width",rect.Height());
	
	int i;
	int nColumnCnt = m_listRTable.GetHeaderCtrl()->GetItemCount();
	int* pData = new int[nColumnCnt];
	
	ZeroMemory(pData,sizeof(int)*nColumnCnt);
	
	for(int i = 0;i<nColumnCnt;i++)
	{
		pData[i] = m_listRTable.GetColumnWidth(i);
	}
	
	pApp->WriteProfileBinary(CT_CONFIG_REG_SEC,"RTEdit Column Width",(LPBYTE)pData,sizeof(int)*nColumnCnt);
	
	delete pData;

	return 1;
}



void CRTTableSet::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	int iProgress = GetDlgItemInt(IDC_PROGRESS);
	CDialog::OnClose();
}

void CRTTableSet::OnCbnSelchangeComboRtType()
{
	m_nSelectedRtType = m_cmbRtType.GetItemData(m_cmbRtType.GetCurSel());
	UpdateRtTable();
}


//ksj 20200207
BOOL CRTTableSet::UpdateRtTable(void)
{
	
	CWinApp* pApp = AfxGetApp();
	CRect rect;
	CString strTemp;

	InitRTTableData();
	if(SetRTTableList() != FALSE)
	{
		UpdateTableInfo();
		SetRTtableNametoDB();

		return TRUE;
	}

	return FALSE;
}

// ksj 20200214 : 옵션에 따라 CRTTableSet내의 기능 활성 비활성화 
int CRTTableSet::EnableFuntions(void)
{
	//ksj 20200214 : AuxH 습도 기능 노출 여부
	BOOL bUseAuxH = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseAuxH", 0); //습도 기능 사용 여부

	GetDlgItem(IDC_STATIC_RT_TYPE)->ShowWindow(bUseAuxH);
	GetDlgItem(IDC_COMBO_RT_TYPE)->ShowWindow(bUseAuxH);	
	////
	
	return 0;
}
