// CyclerChannel.cpp: implementation of the CCyclerChannel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CyclerChannel.h"

//20110319 KHS//////////////
#include "CTSMonProDoc.h"
#include "CyclerModule.h"
#include "MainFrm.h"
////////////////////////////

#include <io.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define _MAX_DISK_WIRTE_COUNT	256
#define  ID_MAX_FILE_SIZE		1400000		//10 -> 10 kb, 100 -> 100 kb, 1000 -> 1M , 1400000


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCyclerChannel::CCyclerChannel(UINT nModuleID, UINT nChannelIndex):m_nModuleID(nModuleID), m_nChannelIndex(nChannelIndex)
	, m_nNetworkErrorCnt(0)
	, m_nNetworkErrorTimer(0)
{
	m_nModuleID = 0;
	m_bDataLoss = FALSE;
	m_bDataLossLogCnt = FALSE;			//20120210 KHS

	m_fCapacitySum = 0;
	m_fMeterValue = 0.0f;
	
	m_pScheduleData = NULL;
	m_bScheduleData=FALSE;
	m_nAuxCount = 0;
	m_nTempAuxCount = 0;
	m_nVoltAuxCount = 0;
	m_nTempThAuxCount = 0;
	m_nHumiAuxCount = 0; //ksj 20200116
	m_bParallel = FALSE;
	m_bMaster = FALSE;
	m_uMasterCh = 0;
	m_fOvenTemp = 0.0f;
	m_fOvenHumidity = 0.0f;
	m_bSendNgState = FALSE;
	m_bCompleteWork = FALSE;		//ljb 201101 자동 복구용 플래그(하나만 복구 실행)
	m_bFileSaving = FALSE;			//20110109 KHS

	LoadSaveItemSet();

	m_bOvenLinkChargeDisCharge = false;
	m_nStartOptChamber = 0;
	m_fchamberFixTemp = 0;
	m_fChamberDeltaFixTemp = 0;
	m_fchamberDeltaTemp = 0;
	m_uichamberPatternNum = 0;
	m_uiChamberDelayTime = 0;
	m_uiChamberID = 0;
	m_uiChamberCheckFixTime = 0;
	m_uiChamberCheckTime = 0;

	m_strLossTime = _T("");		//20120210 KHS

	m_iCh=0;
	m_strNmmodule="";
	m_strCellDeltaVolt = "0";

	m_bUserComplete = FALSE; //lyj 20201028
	m_bPauseBeforeStop = FALSE; //lyj 20210714
}

CCyclerChannel::CCyclerChannel()
{	
	m_nModuleID = 0;
	m_bDataLoss = FALSE;
	m_bDataLossLogCnt = FALSE;			//20120210 KHS

	m_fCapacitySum = 0;
	m_fMeterValue =0.0f;
	m_pScheduleData = NULL;
	m_bScheduleData=FALSE;
	m_nAuxCount = 0;
	m_nTempAuxCount = 0;
	m_nVoltAuxCount = 0;
	m_nTempThAuxCount = 0;
	m_nHumiAuxCount = 0; //ksj 20200116
	m_nCanCount	= 0;
	m_nCanTransCount=0;
	m_bParallel = FALSE;
	m_bMaster = FALSE;
	m_uMasterCh = 0;
	m_bFileSaving = FALSE;			//20110109 KHS

	LoadSaveItemSet();
	m_bOvenLinkChargeDisCharge = false;
	m_nStartOptChamber = 0;
	m_fchamberFixTemp = 0;
	m_fChamberDeltaFixTemp = 0;
	m_fchamberDeltaTemp = 0;
	m_uichamberPatternNum = 0;
	m_uiChamberDelayTime = 0;
	m_uiChamberID = 0;
	m_uiChamberCheckFixTime = 0;
	m_uiChamberCheckTime = 0;

	m_strLossTime = _T("");		//20120210 KHS

	m_iCh=0;
	m_strNmmodule="";

	m_strScheduleFileNameForSbc = "";
	m_strScheduleFileNameForSbc2 = "";
	m_strCellDeltaVolt = "0";

	m_bUserComplete = FALSE; //lyj 20201028
	m_bPauseBeforeStop = FALSE;

	m_bUseGetLastDataCache = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use GetLastData Cache", FALSE); //ksj 20210604 : GetLastData 캐시 기능 사용 여부
	ResetGetLastDataCache();
}

CCyclerChannel::~CCyclerChannel()
{
	//아직 파일로 저장되지 않고 버퍼에 남아있는 data를 모두 저장한다.
	//CheckAndSaveDataB();
	
	if(m_pScheduleData!=NULL && m_bScheduleData==TRUE)
	{
		delete m_pScheduleData;
		m_bScheduleData=FALSE;
	}
	m_pScheduleData = NULL;	

	//cny----------------
	m_MainCtrl.onRelease();

	//외부 데이터 메모리 해제//////////////////////////////////////////////////
	/*if(auxList.GetCount() > 0)
	{
		POSITION pos = auxList.GetHeadPosition();
		while(pos)
		{
			CChannelSensor * pTemp = (CChannelSensor *)auxList.GetAt(pos);
			delete pTemp;
			auxList.GetNext(pos);

		}
	}
	auxList.RemoveAll();*/
	///////////////////////////////////////////////////////////////////////////
}

void CCyclerChannel::ChamberValueInit()
{//
	// 
	m_bOvenLinkChargeDisCharge = false;
	m_nStartOptChamber = 0;
	m_fchamberFixTemp = 0;
	m_fChamberDeltaFixTemp = 0;
	m_fchamberDeltaTemp = 0;
	m_uichamberPatternNum = 0;
	m_uiChamberDelayTime = 0;
	m_uiChamberID = 0;
	m_uiChamberCheckFixTime = 0;
	m_uiChamberCheckTime = 0;
	//m_bChiller_DESChiller = 0;
}

CChannel * CCyclerChannel::GetChannel()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
}

void CCyclerChannel::SetID(UINT nModuleID, UINT nChIndex)
{
	m_nModuleID = nModuleID;
	m_nChannelIndex = nChIndex;
}

BYTE CCyclerChannel::GetCbank()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (BYTE)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CBANK);
}

long CCyclerChannel::GetVoltage()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_VOLTAGE);
}

long CCyclerChannel::GetVoltage_Input()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_VOLTAGE_INPUT);
}

long CCyclerChannel::GetVoltage_Power()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_VOLTAGE_POWER);
}

long CCyclerChannel::GetVoltage_Bus()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_VOLTAGE_BUS);
}

long CCyclerChannel::GetCurrent()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CURRENT);
}

long CCyclerChannel::GetCapacity()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITY);
}

ULONG CCyclerChannel::GetStepTimeDay()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (ULONG)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_TIME_DAY);
}

ULONG CCyclerChannel::GetStepTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (ULONG)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_TIME);
}

ULONG CCyclerChannel::GetTotTimeDay()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (ULONG)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_TOT_TIME_DAY);
}

ULONG CCyclerChannel::GetTotTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (ULONG)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_TOT_TIME);
}

WORD CCyclerChannel::GetState()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STATE);
}

WORD CCyclerChannel::GetStepNo()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_NO);
}

//ksj 20200625 : Mode 추가.
WORD CCyclerChannel::GetStepMode()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_MODE);
}

WORD CCyclerChannel::GetStepType()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_TYPE);
}

int CCyclerChannel::GetTotalCycleCount()
{
	return (int)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_TOT_CYCLE);

}

int CCyclerChannel::GetCurCycleCount()
{
	return (int)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CUR_CYCLE);
}

long CCyclerChannel::GetWattHour()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_WATT_HOUR);
}

long CCyclerChannel::GetWatt()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_WATT);
}

long CCyclerChannel::GetAvgVoltage()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AVG_VOLTAGE);
}

long CCyclerChannel::GetAvgCurrent()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AVG_CURRENT);
}

long CCyclerChannel::GetTemperature()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AUX_TEMPERATURE);
}

long CCyclerChannel::GetTemperature_TH() //20180612 yulee
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AUX_TEMPERATURE_TH);
}

long CCyclerChannel::GetAuxVoltage()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_AUX_VOLTAGE);
}

long CCyclerChannel::GetCommState()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_COMM_STATE);
}

long CCyclerChannel::GetChOutputState()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAN_OUTPUT_STATE);
}

long CCyclerChannel::GetChInputState()
{
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAN_INPUT_STATE);
}


//★★ ljb 2010326  ★★★★★★★★
long CCyclerChannel::GetChamberUsing()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CHAMBER_USING);
}

//초기 step에서 현재 step까지의 용량의 총합
long CCyclerChannel::GetCapacitySum()
{
	WORD type = GetStepType();
	if(type == PS_STEP_CHARGE)
	{
		return long(m_fCapacitySum + ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITY));
	}
	else if ( type == PS_STEP_DISCHARGE || type == PS_STEP_IMPEDANCE)
	{
		return long(m_fCapacitySum - ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITY));
	}

	return (long)m_fCapacitySum;
}


//return 
CString CCyclerChannel::GetTestName()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return m_strTestName;
}

long CCyclerChannel::GetImpedance()
{
	return (long)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_IMPEDANCE);
}


UINT CCyclerChannel::GetStackedSize()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	
	if(pChannel == NULL)	return 0;

	SFT_MD_SYSTEM_DATA * pSys = ::SFTGetModuleSysData(m_nModuleID);

	//if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION)
	if(pSys->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20200121
		return pChannel->GetSaveDataStackSizeA();
	else
		return pChannel->GetSaveDataStackSize();
}   

BOOL CCyclerChannel::CreateResultDataFile(CString strFileName)
{
	return CreateResultFileA(strFileName);
}

BOOL CCyclerChannel::CreateResultFileA(CString strFileName)
{
	CFile f;
	CFileException e;

	if(strFileName.IsEmpty())	return FALSE;

	if( !f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   return FALSE;
	}

	LPPS_FILE_ID_HEADER pFileHeader = new PS_FILE_ID_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	pFileHeader->nFileID = PS_ADP_RECORD_FILE_ID;
	pFileHeader->nFileVersion = _ADP_RECORD_FILE_VER;
	CTime t = CTime::GetCurrentTime();
	sprintf(pFileHeader->szCreateDateTime, "%s", t.Format("%y/%m/%d %H:%M:%S"));
	sprintf(pFileHeader->szDescrition, "%s", "PNE record data file.");

	f.Write(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	delete pFileHeader;

	//ljb 20101222 CCyclerChannel::LoadSaveItemSet()에 의해 설정된 Save Item을 Write 한다.
	f.Write(&m_FileRecordSetHeader, sizeof(PS_RECORD_FILE_HEADER));
	f.Flush();
	f.Close();

	m_strDataList.RemoveAll();
	
	//WriteLog(strFileName+" 생성");
	WriteLog(strFileName+Fun_FindMsg("CyclerChannel_CreateResultFileA_msg1","CYCLERCHANNEL"));//&&

	return TRUE;
}

//ljb 20180223 add file 읽어서 손실 있는지 체크
BOOL CCyclerChannel::Fun_CheckLossData(CString strFileName, long nLastIndex)	//데이터 down이 필요하면 FALSE 리턴
{
// 	CFile f;
// 	CFileException e;
	
	FILE* fp;
	int nRawFileSize   = 0 ;
	int nRawRecordSize = 0 ;
	long nRecordTotalCount = 0;

	if(strFileName.IsEmpty())	return FALSE;
	
	fp = fopen(strFileName, "rb");
	if( !fp)
	{
		return FALSE;
	}

	PS_RAW_FILE_HEADER RawHeader ;
	ZeroMemory(&RawHeader, sizeof(RawHeader)) ;

	// .1. 파일의 길이 계산
	nRawFileSize = filelength(fileno(fp));
	TRACE("Header size = %d",sizeof(PS_RAW_FILE_HEADER));
	
	if (nRawFileSize == sizeof(PS_RAW_FILE_HEADER))
	{
		fclose(fp);
		fp = NULL ;
		return FALSE ;
	}
	else
	{
		ASSERT(nRawFileSize > sizeof(PS_RAW_FILE_HEADER)) ;
		
		// .2. header 는 따로 파일에서 읽어
		fread(&RawHeader, sizeof(RawHeader), 1, fp );
		
		// .3. 레코드의 갯수 계산
		nRawRecordSize = sizeof(float) * RawHeader.rsHeader.nColumnCount;
		nRecordTotalCount = (nRawFileSize - sizeof(PS_RAW_FILE_HEADER)) / nRawRecordSize ;
		fclose( fp );
		fp = NULL ;
	}
	if (nRecordTotalCount == nLastIndex ) 
		return TRUE;
	else
		return FALSE;
}

//파일에 저장된 1줄의 String을 Parsing하여 step Data를 추출 한다.
//No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,Time,Code,Grade,Voltage,Current,Capacity,WattHour,DCIR,
// strvalue => data1,data2,data3,data4,data5,data6,data7,
int CCyclerChannel::ParsingChDataString(CString strValue, PS_STEP_END_RECORD &sData)
{
	CString strTemp, strSource;
	strSource = strValue;

	//int nStepNo = 0;
	int index;
	
	//No
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chStepNo = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Index From
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nIndexFrom = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//IndexTo
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nIndexTo = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;
	
	//Current Cycle
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nCurrentCycleNum = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;
	
	//TotalCycle
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.nTotalCycleNum = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Type
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chStepType = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Time
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fStepTime = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Code
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chCode = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Grade
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.chGradeCode = (BYTE)atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//Voltage
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fVoltage = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//current
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fCurrent = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//capacity
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fCapacity = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//WattHour
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fWattHour = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	//DCIR
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		sData.fImpedance = atof((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else	return FALSE;

	return TRUE;
}

void CCyclerChannel::SetTestName(CString strTestName)
{
	m_strTestName = strTestName;
}

void CCyclerChannel::SetFilePath(CString strPath, BOOL bReset)
{
	//c:\\testname\\M01C02
	m_strFilePath = strPath;		//set the file path;

	CString strTemp;
	int index = m_strFilePath.ReverseFind('\\');
	if(index >=0 )
	{
		strTemp = m_strFilePath.Left(index);		//c:\\testname

		index = strTemp.ReverseFind('\\');
		if(index >=0 )
		{
			SetTestName(strTemp.Mid(index+1));															//m_strTestName = testname
			SetScheduleFile(m_strFilePath+"\\"+m_strTestName+".sch");									//m_strScheduleFileName = c:\\testname\\M01C02\\testname.sch
			SetLogFile(m_strFilePath+"\\"+m_strTestName+".log");										//m_strLogFileName = "c:\\testname\\M01C02\\testname.log";
			SetChamberLogFile(m_strFilePath+"\\"+m_strTestName+"_chamber.log");							//m_strLogFileName = "c:\\testname\\M01C02\\testname.log";
			SetSchUpdateLogFile(m_strFilePath+"\\"+m_strTestName+"_update.log"); //ksj 20170810
			SetScheduleFileForSbc(m_strFilePath+"\\StepStart"+"\\sbc_schedule_info.sch");				
			SetScheduleFileForSbc2(m_strFilePath+"\\StepStart"+"\\sbc_schedule_info_wait_goto.sch");	
			SetPwrSupplyLogFile(m_strFilePath+"\\"+m_strTestName+"_pwrsupply.log");	
			SetChillerLogFile(m_strFilePath+"\\"+m_strTestName+"_chiller.log");	
			SetPumpLogFile(m_strFilePath+"\\"+m_strTestName+"_pump.log");	
			SetCellBALLogFile(m_strFilePath+"\\"+m_strTestName+"_cellbal.log");	

			//Set Schedule Name
			CScheduleData *pSchedule = new CScheduleData;
			pSchedule->SetSchedule(m_strScheduleFileName);
			SetScheduleName(pSchedule->GetScheduleName());				//m_strScheduleName
			delete pSchedule;
					
			m_chFileDataSet.SetPath(m_strFilePath);
//			m_chFileDataSet.ReLoadData();	//ljb			//ljb 20120629 삭제 많은 데이터가 있을 경우 메모리 부족 나타남
		}
	}
	
	//Update Test Start Time
	if(bReset)
		UpdateStartTime();	
	//ksj 20200702 : 파일에 기록되어있는 시간을 작업 시작 시간으로 설정한다.
	else
		UpdateStartTimeFromCts(m_strFilePath);
	//ksj end
}


BYTE CCyclerChannel::GetGradeCode()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (char)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_GRADE_CODE);
}

WORD CCyclerChannel::GetCellCode()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return (WORD)::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CODE);
}

CCalibrationData * CCyclerChannel::GetCaliData()
{
	return &m_CaliData;
}

CString CCyclerChannel::GetFilePath()
{
	return m_strFilePath;
}

CString CCyclerChannel::GetGradeString()
{
	CString msg;
	msg.Format("%c", GetGradeCode());
	return msg;
}

BOOL CCyclerChannel::GetRecordIndex(CString strString, long &lFrom, long &lTo)
{
	CString strTemp, strSource;
	strSource = strString;

//	int nLineNo = 0;
	int index;
	
	//No
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
//		nLineNo = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else return FALSE;

	//IndexFrom
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		lFrom = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else return FALSE;

	//IndexTo
	if((index = strSource.Find(","))>=0)
	{
		strTemp = strSource.Left(index);
		lTo = atol((LPSTR)(LPCTSTR)strTemp);

		strTemp = strSource.Mid(index+1);				//, 제외
		strSource = strTemp;
	}
	else return FALSE;
	
	return TRUE;
}

int CCyclerChannel::CompletedTest()
{
	SFT_MD_SYSTEM_DATA * pSysData = ::SFTGetModuleSysData(m_nModuleID);
	//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION){
	if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015){ //ksj 20200121
		return CheckAndSaveData();
	}
		
	return CheckAndSaveData_v100D(); 
}

CString CCyclerChannel::GetTestPath()
{
	if(m_strFilePath.IsEmpty())		return "";

	int index = m_strFilePath.ReverseFind('\\');
	if(index < 0)			return	"";

	return m_strFilePath.Left(index);
}

//새로운 시험의 시작
void CCyclerChannel::ResetData()
{
	m_bDataLossLogCnt = FALSE;			//20120210 KHS
	m_bDataLoss = FALSE;
	m_bLockFileUpdate = FALSE;

	m_strDataLossRangeList.RemoveAll();
	m_strDataList.RemoveAll();

	m_strDescript.Empty();
	m_strUserID.Empty();
	m_strTestSerial.Empty();			//Test Serial or Lot No
	m_strScheduleFileName.Empty();		//C:\\Test\M01C01\Test.sch
	m_strTestName.Empty();				//Test 명 
	m_strFilePath.Empty();				//C:\\Test\M01C01
	m_strScheduleName.Empty();

	m_strTrayNo.Empty();				//20160314 add Test Tray
	m_strBcrNo.Empty();					//20160314 add Test Bcr

	//m_chFileDataSet.ResetData();
	//CCalibrationData	m_CaliData;			//채널의 교정값 
	//COleDateTime	m_startTime;
	//CModuleIndexTable	m_RestoreFileIndexTable;

	m_fCapacitySum = 0;
	m_fMeterValue = 0.0f;
	//m_bChiller_DESChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDESChiller", 0); //lyj 20201102 DES칠러 펌프 소숫점

	m_MainCtrl.onSbcRunInit();//cny 190111
	
	::SFTInitChannel(m_nModuleID, m_nChannelIndex);

	ResetNetworkErrorCount(); //ksj 20200702
	ResetGetLastDataCache(); //ksj 20210604
}

//.\\Temp\\에서 자기 채널의 임시파일(가장 마지막에 시행한 시험정보가 있음)을 읽어 Setting한다.
BOOL CCyclerChannel::LoadInfoFromTempFile()
{
		
	CString strTemp, strMsg, strFileName;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp.Format("%s", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));

	strFileName.Format("%s\\Temp\\M%02dC%02d.tmp", strTemp, m_nModuleID, m_nChannelIndex+1);
			
	CT_TEMP_FILE_DATA tempData;
	ZeroMemory(&tempData, sizeof(CT_TEMP_FILE_DATA));

	FILE *fp = fopen(strFileName, "rt");

	if(fp == NULL)		return FALSE;
	
	//File Path
	fread(&tempData, sizeof(CT_TEMP_FILE_DATA), 1, fp);

	//SetFilePath(tempData.szFilePath);

	int nUseResetStartTime = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use ResetStartTime", 0); //ksj 20200702
	SetFilePath(tempData.szFilePath,nUseResetStartTime); //ksj 20200702 : 시간 기준 설정 변경, 0이면 마지막 시험의 CTS파일에서 시작시간을 불러온다. 불안정할 경우 끌 수 있도록 옵션처리

	SetTestSerial(tempData.szLot, tempData.szWorker, tempData.szDescript);

	//2006/6/12 명령어 예약 정보 기록 
	int lTempData;
	if(fread(&lTempData, sizeof(long), 1, fp) > 0)
	{
		m_lReservedCycleNo = lTempData;
		if(fread(&lTempData, sizeof(long), 1, fp) > 0)
		{
			m_lReservedStepNo = lTempData;
		}
	}

	fclose(fp);

	return TRUE;
}

//ljb 2011329 이재복 ////////// add
BOOL CCyclerChannel::RestoreLostDataFromFolder(CString strSBCFolder,CString strFolder, int nModuleID, int nChannelID)
{
	CString srcFileName, destFileName;
	CString strTemp;

//////////////////////////////////////////////////////////////////////////
	//CProgressWnd progressWnd(AfxGetMainWnd(), "복구중...", TRUE, TRUE, TRUE);
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg1","CYCLERCHANNEL"), TRUE, TRUE, TRUE);//&&
	progressWnd.SetRange(0, 100);
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//	strIPAddress = "192.168.9.10";
//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
//////////////////////////////////////////////////////////////////////////
	
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;
	CString strAuxTempLocalFileName, strAuxVoltLocalFileName, strCanMLocalFileName, strCanSLocalFileName;
	CString strLocalTempFolder(szBuff);
	CString strStartIndexFile, strEndIndexFile, strStepEndFile;
	CString strAuxTStepEndFile, strAuxVStepEndFile, strCanMStepEndFile, strCanSStepEndFile; 


	//1. 시작 Index파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d Index 시작 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg2","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex);//&&
	progressWnd.SetPos(10);
	progressWnd.SetText(strTemp);

//	strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_start.csv", m_nChannelIndex+1);
	strRemoteFileName.Format("%s\\savingFileIndex_start.csv", strSBCFolder);
	//strStartIndexFile = strLocalTempFolder + "\\savingFileIndex_start.csv";
	strStartIndexFile = strRemoteFileName;	//ljb 2011329 이재복 //////////

	//2. 최종 Index 파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d Index 마지막 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg3","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex);//&&
	progressWnd.SetPos(20);
	progressWnd.SetText(strTemp);

	strRemoteFileName.Format("%s\\savingFileIndex_last.csv",strSBCFolder);
//	strEndIndexFile = strLocalTempFolder + "\\savingFileIndex_last.csv";
	strEndIndexFile = strRemoteFileName;	//ljb 2011329 이재복 //////////

	//3. Step End Data 파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d 각 Step의 완료 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg4","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex);//&&
	progressWnd.SetPos(30);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		return FALSE;
	}

	//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData.csv", m_nChannelIndex+1);
	strRemoteFileName.Format("%s\\ch%03d_SaveEndData.csv", strSBCFolder, m_nChannelIndex);
	//strStepEndFile = strLocalTempFolder + "\\stepEndData.csv";
	strStepEndFile = strRemoteFileName;		//ljb 2011329 이재복 //////////

	//Aux용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
	if(m_chFileDataSet.m_nAuxTempCount > 0)
	{
		//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strRemoteFileName.Format("%s\\ch%03d_SaveEndData_auxT.csv", strSBCFolder, m_nChannelIndex);
		strAuxTStepEndFile = strLocalTempFolder + "\\stepAuxTEndData.csv";
		strAuxTStepEndFile = strRemoteFileName;	//ljb 2011329 이재복 //////////

	}
	
	if(m_chFileDataSet.m_nAuxVoltCount > 0)
	{
		//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strRemoteFileName.Format("%s\\ch%03d_SaveEndData_auxV.csv", strSBCFolder, m_nChannelIndex);
		strAuxVStepEndFile = strLocalTempFolder + "\\stepAuxVEndData.csv";
		strAuxVStepEndFile = strRemoteFileName; //ljb 2011329 이재복 //////////
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Can용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
	if(m_chFileDataSet.m_nCanMasterCount > 0)
	{
		//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strRemoteFileName.Format("%s\\ch%03d_SaveEndData_canMaster.csv", strSBCFolder, m_nChannelIndex);
		strCanMStepEndFile = strLocalTempFolder + "\\stepCanMEndData.csv";
		strCanMStepEndFile = strRemoteFileName;	//ljb 2011329 이재복 //////////
	}
	
	if(m_chFileDataSet.m_nCanSlaveCount > 0)
	{
		//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strRemoteFileName.Format("%s\\ch%03d_SaveEndData_canSlave.csv", strSBCFolder, m_nChannelIndex);
		strCanSStepEndFile = strLocalTempFolder + "\\stepCanSEndData.csv";
		strCanSStepEndFile = strRemoteFileName;	//ljb 2011329 이재복 //////////
	}
	
//////////////////////////////////////////////////////////////////////////
//	strStartIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_start.csv";
//	strEndIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_last.csv";
//////////////////////////////////////////////////////////////////////////

	//4. Module쪽 Index 파일과 PC쪽 결과 파일을 비교하면서 손실구간을 검색한다.
	//PC쪽에 Data가 하나도 저장안되었을 경우도 복구 가능하도록 
	//현재 작업중인 경우 결과를 파일에 저장하지 않고 Buffer에 쌓고 있도록 한다.
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	//strTemp.Format("손실된 구간을 검색중입니다.");
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg5","CYCLERCHANNEL"));//&&
	progressWnd.SetPos(40);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		//m_strLastErrorString.Format("M%02dCH%02d 사용자에 의해 취소됨", m_nModuleID, m_nChannelIndex);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg6","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}

//손실구간 검색
//	long lLostDataCount =0;
// 	BOOL nRtn = SearchLostDataRange(strStartIndexFile, strEndIndexFile, lLostDataCount, &progressWnd);
// 	if(nRtn == FALSE)
// 	{
// 		m_strLastErrorString.Format("M%02dCH%02d 손실구간 검색 실패!!!", m_nModuleID, m_nChannelIndex);
// 		LockFileUpdate(bLockFile);
// 		return FALSE;	//실제 Data 분석 
// 	}
// 
// 	if(lLostDataCount <= 0)
// 	{
// 		LockFileUpdate(bLockFile);
// 		m_strLastErrorString.Format("M%02dCH%02d 손실된 data 정보가 없습니다.", m_nModuleID, m_nChannelIndex);
// 		return TRUE;	//실제 Data 분석 
// 	}

	//4. 손실된 구간이 있을 경우 해당 구간의 복구용 파일을 Module에서 Download한다.
	BOOL bDownLoadOK = FALSE;
// m_strFilePath = "C:\\DATA\\Test_Restore\\M01Ch01[001]";
// m_strTestName = "Test_Restore";

	srcFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;				//복구하고자 하는 파일
	destFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_RAW_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcAuxFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_AUX_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destAuxFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_AUX_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destAuxFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcCanFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_CAN_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destCanFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_CAN_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destCanFileName);		//Backup할 파일명이 있으면 삭제 

//////////////////////////////////////////////////////////////////////////
//	srcFileName = "C:\\test1\\M01Ch01\\Test1.cyc";
//	destFileName = "C:\\test1\\M01Ch01\\Test1_BackUp.cyc";
//////////////////////////////////////////////////////////////////////////

	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cyc 파일용
	FILE *fpAuxSourceFile = NULL, *fpAuxDestFile = NULL;	//aux 파일용
	FILE *fpCanSourceFile = NULL, *fpCanDestFile = NULL;	//can 파일용

	//cyc용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////

	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg7","CYCLERCHANNEL"), destFileName);//&&
		LockFileUpdate(bLockFile);
		return FALSE; 
	}

	int nColSize = 0;
	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)
		{
			//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg23","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpDestFile);
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg24","CYCLERCHANNEL"), srcFileName);//&&
		LockFileUpdate(bLockFile);
		fclose(fpDestFile);
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg8","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			return FALSE;	
		}
		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	
	if(m_chFileDataSet.GetAuxColumnCount() > 0)			
	{
		fpAuxDestFile = fopen(destAuxFileName, "wb");
		if(fpAuxDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg9","CYCLERCHANNEL"), destAuxFileName);//&&
			return FALSE; 
		}

		fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			
			//if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.GetAuxColumnCount()) == FALSE)
			if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.m_nAuxVoltCount, m_chFileDataSet.m_nAuxTempCount) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg10","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxDestFile);
				return FALSE;	 
			}
			fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		}
		
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg11","CYCLERCHANNEL"), srcAuxFileName);//&&
			fclose(fpAuxDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxSourceFile) > 0)
		{
			if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg12","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxSourceFile);
				fclose(fpAuxDestFile);
				return FALSE;	
			}
		}
		nAuxColSize = auxFileHeader.auxHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//CAN용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	if(m_chFileDataSet.GetCanColumnCount() > 0)			
	{
		fpCanDestFile = fopen(destCanFileName, "wb");
		if(fpCanDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg13","CYCLERCHANNEL"), destCanFileName);//&&
			return FALSE; 
		}

		fpCanSourceFile = fopen(srcCanFileName, "rb");
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			if(CreateCanFileA(srcCanFileName, 0, m_chFileDataSet.GetCanColumnCount()) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg14","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanDestFile);
				return FALSE;	 
			}
			fpCanSourceFile = fopen(srcCanFileName, "rb");
		}
		
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg15","CYCLERCHANNEL"), srcCanFileName);//&&
			fclose(fpCanDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg16","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanSourceFile);
				fclose(fpCanDestFile);
				return FALSE;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	//파일이 잘못되었을 경우 
	if(nColSize < 1 || nColSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		//m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", srcFileName, nColSize);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg17","CYCLERCHANNEL"), srcFileName, nColSize);//&&
		LockFileUpdate(bLockFile);
		fclose(fpSourceFile);
		fclose(fpDestFile);
		return FALSE;	//실제 Data 분석 
	}

	//strTemp.Format("M%02dCH%02d 손실된 구간을 복구중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg18","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(50);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	UINT nFileSeq = 0;
	ULONG nSequenceNo = 1;
	int nBackupFileNo = 0;
	//File Read Buffer
	float *pfSrcBuff = new float[nColSize];
	float *pfBuff = new float[nColSize];
	float *pfAuxSrcBuff = NULL;
	CAN_VALUE *pCanSrcBuff = NULL;
	if(nAuxColSize > 0)
		pfAuxSrcBuff = new float[nAuxColSize];
	if(nCanColSize > 0)
		pCanSrcBuff = new CAN_VALUE[nCanColSize];
	
	BOOL bCanceled = FALSE;
	BOOL bRun = TRUE;
	int nCount = 0;
	int nPos;
	int nSaveTotSize = (ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 

	int nAuxCount = 0, nCanCount = 0;

	while(bRun)
	{
		//Progress 50~90구간 사용 
		nPos = 50 + int(40.0f*(float)nCount/(float)nSaveTotSize);

//		TRACE("TotLost Count %d, nCount %d, Pos %d\n", nSaveTotSize, nCount, nPos);

		progressWnd.SetPos(nPos);
		
		//Canceled 
		if(progressWnd.PeekAndPump() == FALSE)
		{
			bCanceled = TRUE;
			break;
		}
		nCount++;
		
		//PC쪽 파일은 순서대로 읽는다.
		size_t rdSize = fread(pfSrcBuff, sizeof(float), nColSize, fpSourceFile );

		//Aux,Can 파일도 Raw 파일의 인덱스를 따라간다.
		if(nAuxColSize)
			fread(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxSourceFile);
		if(nCanColSize)
			fread(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanSourceFile);

		if(rdSize <	1)	//PC쪽 파일을 다 읽었을 경우 모듈의 최종 Index와 같은지 확인 한다. 모듈쪽 저장 인텍스가 더크면 뒤쪽 data 최종 복구한다.
		{	
			nFileSeq = nSaveTotSize;//(ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 
			bRun = FALSE;	//PC쪽은 다 읽었음 
		}
		else
		{
			nFileSeq = (ULONG)pfSrcBuff[0];
		}

				
		//Index 검사
		long nStartNo = 0, nEndNo = 0, lLossCount =0;
		
		//PC쪽 검사 결과 data loss range detected	
		if(nFileSeq > nSequenceNo || bRun == FALSE)		
		{
			nStartNo = nSequenceNo;
			while(nFileSeq > nSequenceNo)
			{
				nSequenceNo++;
			}
			nEndNo = nSequenceNo-1;

			int nRestordDataSize = 0;
			lLossCount = nEndNo - nStartNo+1;
			long lTemp = 0;
			while(lLossCount > 0)	//1개의 손실 구간이 모두 복구됨 
			{
				//5. 손실된 부분에 해당하는 복구용 파일을 Index 파일에서 찾아낸다.
				nStartNo += nRestordDataSize;
				lTemp = m_RestoreFileIndexTable.FindFirstMachFileNo(nStartNo);	
				if(lTemp < 1)	
				{
					//현재 손실 구간을 SBC에서 Dowload한 파일목록에서 발견하지 못했을 경우, 
					//기타 오류로 복구 불가시는 현재구간은 복구를 생략한다.
					nRestordDataSize = lLossCount;
					lLossCount = 0;
					break;
				}
				
				//이전에 DownLoad한 파일에 손상된 구간이 있을 경우는 Dowload를 생략한다.
				if(nBackupFileNo != lTemp)
				{
					nBackupFileNo = lTemp;
					//6. 복구용 파일을 DownLoad 받는다.
					//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
					strRemoteFileName.Format("%s\\ch%03d_SaveData%03d.csv", strSBCFolder, m_nChannelIndex+1, nBackupFileNo);
					strLocalFileName = strLocalTempFolder + "\\ChRawData.csv";
					strLocalFileName = strRemoteFileName;	//ljb 2011329 이재복 //////////
					bDownLoadOK = TRUE;
					if(m_chFileDataSet.m_nAuxTempCount > 0)
					{
						//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strRemoteFileName.Format("%s\\ch%03d_SaveData%03d_auxT.csv", strSBCFolder, m_nChannelIndex+1, nBackupFileNo);
						strAuxTempLocalFileName = strLocalTempFolder + "\\ChAuxTData.csv";
						strAuxTempLocalFileName = strRemoteFileName;	//ljb 2011329 이재복 //////////
					}
					if(m_chFileDataSet.m_nAuxVoltCount > 0)
					{
						//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strRemoteFileName.Format("%s\\ch%03d_SaveData%03d_auxV.csv", strSBCFolder, m_nChannelIndex+1, nBackupFileNo);
						strAuxVoltLocalFileName = strLocalTempFolder + "\\ChAuxVData.csv";
						strAuxVoltLocalFileName = strRemoteFileName;	//ljb 2011329 이재복 //////////
					}
					if(m_chFileDataSet.m_nCanMasterCount > 0)
					{
						//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strRemoteFileName.Format("%s\\ch%03d_SaveData%03d_canMaster.csv", strSBCFolder, m_nChannelIndex+1, nBackupFileNo);
						strCanMLocalFileName = strLocalTempFolder + "\\ChCanMData.csv";
						strCanMLocalFileName = strRemoteFileName;	//ljb 2011329 이재복 //////////
					}
					if(m_chFileDataSet.m_nCanSlaveCount > 0)
					{
						//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strRemoteFileName.Format("%s\\ch%03d_SaveData%03d_canSlave.csv", strSBCFolder, m_nChannelIndex+1, nBackupFileNo);
						strCanSLocalFileName = strLocalTempFolder + "\\ChCanSData.csv";
						strCanSLocalFileName = strRemoteFileName;	//ljb 2011329 이재복 //////////
					}
				}

				//7. DownLoad 파일로 복구 한다.
				// 7.1 data가 부족하면 다음 파일을 DownLoad한다.
				if(bDownLoadOK)
				{					
					//복구 시작
					nRestordDataSize = WriteRestoreRawFile(strLocalFileName, fpDestFile, FileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nAuxColSize)
						WriteRestoreAuxFile(strAuxTempLocalFileName, strAuxVoltLocalFileName, fpAuxDestFile, auxFileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nCanColSize)
						WriteRestoreCanFile(strCanMLocalFileName, strCanSLocalFileName, fpCanDestFile, canFileHeader, nStartNo, nEndNo, nBackupFileNo);
					

				}
				//strTemp.Format("Data 복구 실시, 시작Index: %d, 복수 수량:%d개",  nStartNo, nRestordDataSize);
				strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg19","CYCLERCHANNEL"),  nStartNo, nRestordDataSize);//&&
				WriteLog(strTemp);
				lLossCount -= nRestordDataSize;	//if(lLossCount > 0) Download한 파일에 복구 data가 모두 없고 다음파일까지 이어짐 
			}
			
			if(bRun)
			{
				//PC쪽 읽을 data가 아직 남아 있으면 손실 구간 가장 마지막 다음 data까지 읽혔으므로 
				//한줄더 읽어들인 data를 저장한다.
				if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destFileName);
				}
				if(nAuxColSize > 0)
				{
					if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destAuxFileName);
					}
				}
				if(nCanColSize > 0)
				{
					if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destCanFileName);
					}
				}
			}	
			nSequenceNo = nFileSeq;	//손실된 구간 복구 완료 
		}
		else if(nFileSeq < nSequenceNo)	//Module index 오류
		{
			//PC와 모듈의 시험이 불일치 하거나 모듈에 저장된 파일의 index가 잘못 되었을 경우 
			//TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);
			TRACE(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg20","CYCLERCHANNEL"), nFileSeq, nSequenceNo);//&&

			//m_strLastErrorString.Format("M%02dCH%02d 모듈 정보와 Host 정보가 불일치 합니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg21","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&

			if(pfBuff)
			{
				delete [] pfBuff;
				pfBuff = NULL;
			}

			if(pfSrcBuff)
			{
				delete[] pfSrcBuff;
				pfSrcBuff = NULL;
			}
			if(pfAuxSrcBuff)
			{
				delete [] pfAuxSrcBuff;
				pfAuxSrcBuff = NULL;
			}
			if(pCanSrcBuff)
			{
				delete [] pCanSrcBuff;
				pCanSrcBuff = NULL;
			}

			if(fpDestFile)		fflush(fpDestFile);
			if(fpAuxDestFile)	fflush(fpAuxDestFile);
			if(fpCanDestFile)	fflush(fpCanDestFile);

			if(fpSourceFile) fclose(fpSourceFile);
			if(fpDestFile) fclose(fpDestFile);
			if(fpAuxSourceFile) fclose(fpAuxSourceFile);
			if(fpAuxDestFile) fclose(fpAuxDestFile);
			if(fpCanSourceFile) fclose(fpCanSourceFile);
			if(fpCanDestFile) fclose(fpCanDestFile);

			_unlink(destFileName);		//PC와 모듈이 맞지 않는 정보일 경우 복구 할 수 없음 
			
			LockFileUpdate(bLockFile);

			return FALSE;
		}
		else
		{
			//PC에서 정상적인 원본에서 임시 복구 파일로 Data를 복사한다.
			if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
			{
				TRACE("Backup File write Error %s\n", destFileName);
			}
			if(nAuxColSize > 0)
			{
				if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destAuxFileName);
				}
			}
			if(nCanColSize > 0)
			{
				if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destCanFileName);
				}
			}

		
		}	//정상
		nSequenceNo++;

	} //while(bRun) END

	if(pfBuff)
	{
		delete [] pfBuff;
		pfBuff = NULL;
	}

	if(pfSrcBuff)
	{
		delete[] pfSrcBuff;
		pfSrcBuff = NULL;
	}

	if(pfAuxSrcBuff)
	{
		delete [] pfAuxSrcBuff;
		pfAuxSrcBuff = NULL;
	}
	if(pCanSrcBuff)
	{
		delete [] pCanSrcBuff;
		pCanSrcBuff = NULL;
	}

	if(fpDestFile)		fflush(fpDestFile);
	if(fpAuxDestFile)	fflush(fpAuxDestFile);
	if(fpCanDestFile)	fflush(fpCanDestFile);

	if(fpSourceFile) fclose(fpSourceFile);
	if(fpDestFile) fclose(fpDestFile);

	if(fpAuxSourceFile) fclose(fpAuxSourceFile);
	if(fpAuxDestFile)	fclose(fpAuxDestFile);

	if(fpCanSourceFile) fclose(fpCanSourceFile);
	if(fpCanDestFile) fclose(fpCanDestFile);

	if(bCanceled)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

// m_strFilePath = "C:\\DATA\\Pack_Data_v100B_LG_박재성";
// m_strTestName = "00207594_110328_MOKA_7series_sample2_Capa@25oC_with_fan\\M01Ch01[001]\\00207594_110328_MOKA_7series_sample2_Capa@25oC_with_fan";
// 
// srcFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;				//복구하고자 하는 파일
// //destFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_RAW_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
// destFileName = srcFileName;
// 
// CString srcAuxFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_AUX_FILE_NAME_EXT;				//복구하고자 하는 파일
// CString destAuxFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_AUX_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
// srcAuxFileName=destAuxFileName;
// 
// CString srcCanFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_CAN_FILE_NAME_EXT;				//복구하고자 하는 파일
// CString destCanFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_CAN_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
// destCanFileName=srcCanFileName;
// 
// BOOL bLockFile = LockFileUpdate(TRUE);

	//복구된 파일을 이용하여 rpt 파일을 생성한다.
	CString strNewTableFile, strOrgTableFile;
	strNewTableFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_RESULT_FILE_NAME_EXT;

	//Aux,Can용 결과 데이터 파일
	CString strNewAuxTabelFile, strOrgAuxTableFile;
	strNewAuxTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_AUX_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgAuxTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_AUX_RESULT_FILE_NAME_EXT;

	CString strNewCanTabelFile, strOrgCanTableFile;
	strNewCanTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_CAN_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgCanTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_CAN_RESULT_FILE_NAME_EXT;

	_unlink(strNewTableFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewAuxTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewCanTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 

//////////////////////////////////////////////////////////////////////////
//	strNewTableFile = "C:\\test1\\M01Ch01\\Test1_BackUp.cts";
//	strOrgTableFile = "C:\\test1\\M01Ch01\\Test1.cts";
//////////////////////////////////////////////////////////////////////////

	//strTemp.Format("M%02dCH%02d 손실된 Index 정보를 갱신중입니다.", m_nModuleID, m_nChannelIndex);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg22","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex);//&&
	progressWnd.SetPos(90);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	
//	strStepEndFile = "C:\\DATA\\RESTORE\\ch01\\ch001_SaveEndData.csv";
	//Backup이 완료된 Raw파일을 이용하여 rpt 파일을 새로 생성	
	if(RemakePCIndexFile(destFileName, strStepEndFile, strNewTableFile, strOrgTableFile) == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
	if(nAuxColSize > 0)
	{
		if(RemakeAuxPCIndexFile(destAuxFileName, strAuxTStepEndFile, strAuxVStepEndFile, strNewAuxTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	if(nCanColSize > 0)
	{
		if(RemakeCanPCIndexFile(destCanFileName, strCanMStepEndFile, strCanSStepEndFile, strNewCanTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	//strTemp.Format("M%02dCH%02d 복구용 임시 파일을 삭제중입니다.", m_nModuleID, m_nChannelIndex);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg23","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex);//&&
	progressWnd.SetPos(95);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
	//원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	

	sprintf(szFrom, "%s", strOrgTableFile);
//	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgAuxTableFile);
//	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgCanTableFile);
//	::DeleteFile(szFrom);
	
	ZeroMemory(szFrom, _MAX_PATH);			//Double NULL Terminate
	sprintf(szFrom, "%s", srcFileName);
//	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcAuxFileName);
//	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcCanFileName);
//	::DeleteFile(szFrom);


	progressWnd.SetPos(98);
	//strTemp.Format("M%02dCH%02d 복구 완료중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg24","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetText(strTemp);
	//Backup한 파일이름을 바꾼다.
	if(rename(destFileName, srcFileName) != 0)
	{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg25","CYCLERCHANNEL"), srcFileName);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	if(nAuxColSize > 0)
	{
		if(rename(destAuxFileName, srcAuxFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg26","CYCLERCHANNEL"), srcAuxFileName);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewAuxTabelFile, strOrgAuxTableFile) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgAuxTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg27","CYCLERCHANNEL"), strOrgAuxTableFile);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}	

	if(nCanColSize > 0)
	{
		if(rename(destCanFileName, srcCanFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg28","CYCLERCHANNEL"), srcCanFileName);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewCanTabelFile, strOrgCanTableFile) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgCanTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg29","CYCLERCHANNEL"), strOrgCanTableFile);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}
	

	if(rename(strNewTableFile, strOrgTableFile) != 0)
	{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgTableFile);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg30","CYCLERCHANNEL"), strOrgTableFile);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}


	LockFileUpdate(bLockFile);
	
	//strTemp.Format("M%02dCH%02d 복구를 완료 하였습니다.", m_nModuleID, m_nChannelIndex);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg31","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex);//&&
	progressWnd.SetPos(100);
	progressWnd.SetText(strTemp);
	progressWnd.PeekAndPump();
	
	Sleep(200);

	m_bDataLoss = FALSE;
	//WriteLog("Data 복구 완료");
	WriteLog(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg32","CYCLERCHANNEL"));//&&
	
	return TRUE;
}
/*
---------------------------------------------------------
---------
-- Filename		:	CyclerChannel.cpp 
-- Description	:	결과 데이터 전체 복구 실행.
--					1. 필히 FTP다운로드를 진행후 복구 해야함 
--					   기존에 다운로드 되있는 데이터가 존재한다면 기존 데이터가 저장됩니다.
--				
-- Author		:	
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CCyclerChannel::RestoreLostAll(CString strSBCFolder, CString strFileFolder, int nModuleID, int nChannelID)
{
	//CProgressWnd progressWnd(AfxGetMainWnd(), "전체 복구중...", TRUE, TRUE, TRUE);
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("CyclerChannel_RestoreLostAll_msg1","CYCLERCHANNEL"), TRUE, TRUE, TRUE);//&&
	progressWnd.SetRange(0, 100);
	
	//1.스텝엔드 데이터 데이터셋 로딩.
	m_chFileDataSet.SetPath(strSBCFolder);
	m_chFileDataSet.ReLoadData();

	CString strTemp;
	CString strRemoteFileName; 
	CString strStartIndexFile,strEndIndexFile; // Index파일
	CString strStepEndFile,strAuxTStepEndFile,strAuxVStepEndFile,strCanMStepEndFile,strCanSStepEndFile;

	//strTemp.Format("M%02dCH%02d Index 정보를 읽고 있습니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostAll_msg2","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(10);
	progressWnd.SetText(strTemp);
		
	strStartIndexFile.Format("%s\\savingFileIndex_start.csv", strSBCFolder);
	
	strEndIndexFile.Format("%s\\savingFileIndex_last.csv",strSBCFolder);
				
	strStepEndFile.Format("%s\\ch%03d_SaveEndData.csv", strSBCFolder, nChannelID);
		
	strAuxTStepEndFile.Format("%s\\ch%03d_SaveEndData_auxT.csv", strSBCFolder, nChannelID);
	
	strAuxVStepEndFile.Format("%s\\ch%03d_SaveEndData_auxV.csv", strSBCFolder, nChannelID);
	
	strCanMStepEndFile.Format("%s\\ch%03d_SaveEndData_canMaster.csv", strSBCFolder, nChannelID);
	
	strCanSStepEndFile.Format("%s\\ch%03d_SaveEndData_canSlave.csv", strSBCFolder, nChannelID);

	//strTemp.Format("M%02dCH%02d 각 Step의 완료 정보를 읽고 있습니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostAll_msg3","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(20);
	progressWnd.SetText(strTemp);

	
	
	return TRUE;
}

/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	결과 데이터 완전 복구.
-- Author		:	2014.10.20 이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CCyclerChannel::RestoreLostDataFromFolder2(CString strSBCFolder,CString strFolder, int nModuleID, int nChannelID)
{
	CString strTestFolder,srcFileName, destFileName;
	CString strTemp;

	strTestFolder.Format("%s",strFolder.Mid(0, strFolder.ReverseFind('\\')));
	//0. 결과데이터의 스탭앤드를 읽어드린다. 
	//   해더값을사용함.
	m_chFileDataSet.SetPath(strTestFolder);
	m_chFileDataSet.ReLoadData();
		
	srcFileName = strFolder;
		
	//CProgressWnd progressWnd(AfxGetMainWnd(), "복구중...", TRUE, TRUE, TRUE);
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg1","CYCLERCHANNEL"), TRUE, TRUE, TRUE);//&&
	progressWnd.SetRange(0, 100);

	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;
	CString strAuxTempLocalFileName, strAuxVoltLocalFileName;
	CString	strCanMLocalFileName, strCanSLocalFileName;
	CString strLocalTempFolder(szBuff);
	CString strStartIndexFile, strEndIndexFile, strStepEndFile;
	CString strAuxTStepEndFile, strAuxVStepEndFile;
	CString strCanMStepEndFile, strCanSStepEndFile; 
	
	//strTemp.Format("M%02dCH%02d Index 시작 정보를 읽고 있습니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg2","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(10);
	progressWnd.SetText(strTemp);
	
	// 1. 시작~종료 인덱스 파일
	strStartIndexFile.Format("%s\\savingFileIndex_start.csv", strSBCFolder);

	//strTemp.Format("M%02dCH%02d Index 마지막 정보를 읽고 있습니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg3","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(20);
	progressWnd.SetText(strTemp);

	// 2. 마지막 인덱스 파일
	strEndIndexFile.Format("%s\\savingFileIndex_last.csv",strSBCFolder);

	//strTemp.Format("M%02dCH%02d 각 Step의 완료 정보를 읽고 있습니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg4","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(30);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		return FALSE;
	}

	//ksj 20171117 : 주석처리
// 	if (nChannelID > 1)
// 	{
// 		// 3. 결과 스탭앤드 파일.
// 		strStepEndFile.Format("%s\\ch%03d_SaveEndData.csv", strSBCFolder, nChannelID+1);
// 		
// 		//3.1 Aux용 스탭앤드 데이터
// 		if(m_chFileDataSet.m_nAuxTempCount > 0)
// 		{
// 			strAuxTStepEndFile.Format("%s\\ch%03d_SaveEndData_auxT.csv", strSBCFolder, nChannelID+1);		
// 		}
// 		
// 		if(m_chFileDataSet.m_nAuxVoltCount > 0)
// 		{
// 			strAuxVStepEndFile.Format("%s\\ch%03d_SaveEndData_auxV.csv", strSBCFolder, nChannelID+1);
// 			
// 		}
// 		
// 		//3.2 Can용 스탭앤드 File
// 		if(m_chFileDataSet.m_nCanMasterCount > 0)
// 		{
// 			strCanMStepEndFile.Format("%s\\ch%03d_SaveEndData_canMaster.csv", strSBCFolder, nChannelID+1);
// 		}
// 		
// 		if(m_chFileDataSet.m_nCanSlaveCount > 0)
// 		{
// 			strCanSStepEndFile.Format("%s\\ch%03d_SaveEndData_canSlave.csv", strSBCFolder, nChannelID+1);
// 		}
// 	}
// 	else
	{
		// 3. 결과 스탭앤드 파일.
		strStepEndFile.Format("%s\\ch%03d_SaveEndData.csv", strSBCFolder, nChannelID);
		
		//3.1 Aux용 스탭앤드 데이터
		if(m_chFileDataSet.m_nAuxTempCount > 0)
		{
			strAuxTStepEndFile.Format("%s\\ch%03d_SaveEndData_auxT.csv", strSBCFolder, nChannelID);		
		}
		
		if(m_chFileDataSet.m_nAuxVoltCount > 0)
		{
			strAuxVStepEndFile.Format("%s\\ch%03d_SaveEndData_auxV.csv", strSBCFolder, nChannelID);
			
		}
		
		//3.2 Can용 스탭앤드 File
		if(m_chFileDataSet.m_nCanMasterCount > 0)
		{
			strCanMStepEndFile.Format("%s\\ch%03d_SaveEndData_canMaster.csv", strSBCFolder, nChannelID);
		}
		
		if(m_chFileDataSet.m_nCanSlaveCount > 0)
		{
			strCanSStepEndFile.Format("%s\\ch%03d_SaveEndData_canSlave.csv", strSBCFolder, nChannelID);
		}
	}
	
	//4. Module쪽 Index 파일과 PC쪽 결과 파일을 비교하면서 손실구간을 검색한다.
	//PC쪽에 Data가 하나도 저장안되었을 경우도 복구 가능하도록 
	//현재 작업중인 경우 결과를 파일에 저장하지 않고 Buffer에 쌓고 있도록 한다.
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	//strTemp.Format("손실된 구간을 검색중입니다.");
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg5","CYCLERCHANNEL"));//&&
	progressWnd.SetPos(40);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		//m_strLastErrorString.Format("M%02dCH%02d 사용자에 의해 취소됨", m_nModuleID, nChannelID);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg6","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
	//4. 손실된 구간이 있을 경우 해당 구간의 복구용 파일을 Module에서 Download한다.
	BOOL bDownLoadOK = FALSE;

	//5. 복사본 파일을 만든다. raw데이터  cyc aux can
	destFileName.Format("%s_backup.cyc",srcFileName.Mid(0, srcFileName.ReverseFind('.')));
	_unlink(destFileName);
	
	CString srcAuxFileName;
	CString destAuxFileName;
	srcAuxFileName.Format("%s.aux",srcFileName.Mid(0, srcFileName.ReverseFind('.')));
	destAuxFileName.Format("%s_Backup.aux",srcFileName.Mid(0, srcFileName.ReverseFind('.')));
	_unlink(destAuxFileName);		
	
	CString srcCanFileName;		
	CString destCanFileName;
	srcCanFileName.Format("%s.can",srcFileName.Mid(0, srcFileName.ReverseFind('.')));
	destCanFileName.Format("%s_Backup.can",srcFileName.Mid(0, srcFileName.ReverseFind('.')));	
	_unlink(destCanFileName);		
	

	FILE *fpSourceFile = NULL, *fpDestFile = NULL;			//cyc 파일용
	FILE *fpAuxSourceFile = NULL, *fpAuxDestFile = NULL;	//aux 파일용
	FILE *fpCanSourceFile = NULL, *fpCanDestFile = NULL;	//can 파일용

	//5.1 cyc_back 파일 열기.
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg7","CYCLERCHANNEL"), destFileName);//&&
		LockFileUpdate(bLockFile);
		return FALSE; 
	}
	//5.2 cyc원본데이터를 열어서 헤더를 복사한다.
	int nColSize = 0;
	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)
		{
			//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg8","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpDestFile);
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg9","CYCLERCHANNEL"), srcFileName);//&&
		LockFileUpdate(bLockFile);
		fclose(fpDestFile);
		return FALSE;	//실제 Data 분석 
	}
		
	//5.3 cyc Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg10","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			return FALSE;	
		}
		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	
	//6.1 aux 결과데이터 원본 해더 복사.
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	
	if(m_chFileDataSet.m_nAuxColumnCount > 0)			
	{
		fpAuxDestFile = fopen(destAuxFileName, "wb");
		if(fpAuxDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg11","CYCLERCHANNEL"), destAuxFileName);//&&
			return FALSE; 
		}

		fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		if(fpAuxSourceFile == NULL)	
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.m_nAuxVoltCount, m_chFileDataSet.m_nAuxTempCount) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg12","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxDestFile);
				return FALSE;	 
			}
			fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		}
		
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg13","CYCLERCHANNEL"), srcAuxFileName);//&&
			fclose(fpAuxDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//6.2 aux 해더 복사.
		if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxSourceFile) > 0)
		{
			if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg14","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxSourceFile);
				fclose(fpAuxDestFile);
				return FALSE;	
			}
		}
		nAuxColSize = auxFileHeader.auxHeader.nColumnCount;
	}
	
	
	
	//7.1 can 원본 해더 복사.
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	if(m_chFileDataSet.m_nCanColumnCount > 0)			
	{
		fpCanDestFile = fopen(destCanFileName, "wb");
		if(fpCanDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg15","CYCLERCHANNEL"), destCanFileName);//&&
			return FALSE; 
		}

		fpCanSourceFile = fopen(srcCanFileName, "rb");
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			if(CreateCanFileA(srcCanFileName, 0, m_chFileDataSet.GetCanColumnCount()) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg16","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanDestFile);
				return FALSE;	 
			}
			fpCanSourceFile = fopen(srcCanFileName, "rb");
		}
		
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg17","CYCLERCHANNEL"), srcCanFileName);//&&
			fclose(fpCanDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//7.2 can 해더 복사.
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg18","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanSourceFile);
				fclose(fpCanDestFile);
				return FALSE;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;
	}
	
		
	//8.1 파일이 잘못되었을 경우 
	if(nColSize < 1 || nColSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		//m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", srcFileName, nColSize);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg19","CYCLERCHANNEL"), srcFileName, nColSize);//&&
		LockFileUpdate(bLockFile);
		if (fpSourceFile != NULL) fclose(fpSourceFile);
		if (fpDestFile != NULL) fclose(fpDestFile);
		if (fpAuxSourceFile != NULL) fclose(fpAuxSourceFile);
		if (fpAuxDestFile != NULL) fclose(fpAuxDestFile);
		if (fpCanSourceFile != NULL) fclose(fpCanSourceFile);
		if (fpCanDestFile != NULL) fclose(fpCanDestFile);

		return FALSE;	//실제 Data 분석 
	}

	//strTemp.Format("M%02dCH%02d 해더 파일을 복사중입니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg20","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(50);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//9.0 파일 복구 시작.
	
	UINT nFileSeq = 0;
	ULONG nSequenceNo = 1;
	int nBackupFileNo = 0;
	
	float *pfSrcBuff = new float[nColSize];
	float *pfBuff = new float[nColSize];
	float *pfAuxSrcBuff = NULL;
	CAN_VALUE *pCanSrcBuff = NULL;

	if(nAuxColSize > 0){
		pfAuxSrcBuff = new float[nAuxColSize];
	}
	if(nCanColSize > 0){
		pCanSrcBuff = new CAN_VALUE[nCanColSize];
	}
	
	BOOL bCanceled = FALSE;
	BOOL bRun = TRUE;
	int nCount = 0;
	int nPos;

	//9.1 모듈에서 downloading한 index파일을 parsing한다.
	m_RestoreFileIndexTable.SetFileName(strStartIndexFile, strEndIndexFile);
	if(m_RestoreFileIndexTable.LoadTable() == FALSE)
	{
		//m_strLastErrorString.Format("모듈의 index 정보를 loading할수 없습니다.");	
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg21","CYCLERCHANNEL"));	//&&
		fclose(fpSourceFile);
		fclose(fpDestFile);

		return FALSE;
	}
	int nSaveTotSize = (ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 

	int nAuxCount = 0, nCanCount = 0;

	while(bRun)
	{
		//9.2 프로세스  구간 설정. 
		nPos = 50 + int(40.0f*(float)nCount/(float)nSaveTotSize);
		progressWnd.SetPos(nPos);		
		if(progressWnd.PeekAndPump() == FALSE)
		{
			bCanceled = TRUE;
			break;
		}
		nCount++;
		
		//9.3 cyc파일을 읽는다. (미리 cyc파일을 해더만 복사해  놓자)
		size_t rdSize = fread(pfSrcBuff, sizeof(float), nColSize, fpSourceFile );
		
		//9.4 aux,can 파일을 읽는다. 
		if(nAuxColSize){
			fread(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxSourceFile);
		}
		if(nCanColSize){
			fread(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanSourceFile);
		}
		
		//9.5 최종 파일을 다읽었거나 파일이 해더만 존재하는경우.
		//	  PC쪽 파일을 다 읽었을 경우 모듈의 최종 Index와 같은지 확인 한다. 모듈쪽 저장 인텍스가 더크면 뒤쪽 data 최종 복구한다.
		if(rdSize <	1)	
		{	
			nFileSeq = nSaveTotSize;	//모듈의 최종 저장 Index를 구한다. 
			bRun = FALSE;				//PC쪽은 다 읽었음 
		}
		else
		{
			nFileSeq = (ULONG)pfSrcBuff[0];
		}
				
		long nStartNo = 0, nEndNo = 0, lLossCount =0;
		
		//9.6 최종 
		if(nFileSeq > nSequenceNo || bRun == FALSE)		
		{
			int nRestordDataSize = 0;

			nStartNo = nSequenceNo;

			// 9.6.1 최종 SequenceNo를 구한다.
			while(nFileSeq > nSequenceNo)
			{
				nSequenceNo++;
			}
			
			nEndNo = nSequenceNo-1; //최종 SequenceNo.

			lLossCount = nEndNo - nStartNo + 1;

			long lTemp = 0;
			while(lLossCount > 0)	//1개의 손실 구간이 모두 복구됨 
			{
				//9.6.2 손실된 부분에 해당하는 복구용 파일을 Index 파일에서 찾아낸다.
				nStartNo += nRestordDataSize; // 1 ~  시작
				lTemp = m_RestoreFileIndexTable.FindFirstMachFileNo(nStartNo);	
				if(lTemp < 1)	
				{
					//현재 손실 구간을 SBC에서 Dowload한 파일목록에서 발견하지 못했을 경우, 
					//기타 오류로 복구 불가시는 현재구간은 복구를 생략한다.
					nRestordDataSize = lLossCount;
					lLossCount = 0;
					break;
				}
				
				//9.6.3 백업 넘버 설정
				if(nBackupFileNo != lTemp)
				{
					nBackupFileNo = lTemp;

					bDownLoadOK = TRUE; //이미 SBC 백업 파일을 다운로드해놓은 상태입니다.

					strLocalFileName.Format("%s\\ch%03d_SaveData%03d.csv", strSBCFolder, nChannelID, nBackupFileNo);
					
					strAuxTempLocalFileName.Format("%s\\ch%03d_SaveData%03d_auxT.csv", strSBCFolder, nChannelID, nBackupFileNo);

					strAuxVoltLocalFileName.Format("%s\\ch%03d_SaveData%03d_auxV.csv", strSBCFolder, nChannelID, nBackupFileNo);
					
					strCanMLocalFileName.Format("%s\\ch%03d_SaveData%03d_canMaster.csv", strSBCFolder, nChannelID, nBackupFileNo);
				
					strCanSLocalFileName.Format("%s\\ch%03d_SaveData%03d_canSlave.csv", strSBCFolder, nChannelID, nBackupFileNo);

					if(m_chFileDataSet.m_nAuxTempCount > 0)
					{
						
					}
					if(m_chFileDataSet.m_nAuxVoltCount > 0)
					{
						
					}
					if(m_chFileDataSet.m_nCanMasterCount > 0)
					{
						
					}
					if(m_chFileDataSet.m_nCanSlaveCount > 0)
					{
						
					}
				}

				//9.6.4 이미 파일이 존재함.
				if(bDownLoadOK)
				{					
					//복구 시작
					nRestordDataSize = WriteRestoreRawFile(strLocalFileName, fpDestFile, FileHeader, nStartNo, nEndNo, nBackupFileNo);

					if(nAuxColSize){
						WriteRestoreAuxFile(strAuxTempLocalFileName, strAuxVoltLocalFileName, fpAuxDestFile, auxFileHeader, nStartNo, nEndNo, nBackupFileNo);
					}
 						
					if(nCanColSize){
						WriteRestoreCanFile(strCanMLocalFileName, strCanSLocalFileName, fpCanDestFile, canFileHeader, nStartNo, nEndNo, nBackupFileNo);
					}					
				}
				
				//strTemp.Format("Data 복구 실시, 시작Index: %d, 복수 수량:%d개",  nStartNo, nRestordDataSize);
				strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg22","CYCLERCHANNEL"),  nStartNo, nRestordDataSize);//&&
				WriteLog(strTemp);
				lLossCount -= nRestordDataSize;
			}
	
			nSequenceNo = nFileSeq;	//손실된 구간 복구 완료 
		}
		else if(nFileSeq < nSequenceNo)	//Module index 오류 //ksj 20200127 : 추가. RestoreLostDataFromFolder에는 있는데 여기엔 없다.
		{
			//PC와 모듈의 시험이 불일치 하거나 모듈에 저장된 파일의 index가 잘못 되었을 경우 
			//TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);
			TRACE(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg20","CYCLERCHANNEL"), nFileSeq, nSequenceNo);//&&

			//m_strLastErrorString.Format("M%02dCH%02d 모듈 정보와 Host 정보가 불일치 합니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder_msg21","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&

			if(pfBuff)
			{
				delete [] pfBuff;
				pfBuff = NULL;
			}

			if(pfSrcBuff)
			{
				delete[] pfSrcBuff;
				pfSrcBuff = NULL;
			}
			if(pfAuxSrcBuff)
			{
				delete [] pfAuxSrcBuff;
				pfAuxSrcBuff = NULL;
			}
			if(pCanSrcBuff)
			{
				delete [] pCanSrcBuff;
				pCanSrcBuff = NULL;
			}

			if(fpDestFile)		fflush(fpDestFile);
			if(fpAuxDestFile)	fflush(fpAuxDestFile);
			if(fpCanDestFile)	fflush(fpCanDestFile);

			if(fpSourceFile) fclose(fpSourceFile);
			if(fpDestFile) fclose(fpDestFile);
			if(fpAuxSourceFile) fclose(fpAuxSourceFile);
			if(fpAuxDestFile) fclose(fpAuxDestFile);
			if(fpCanSourceFile) fclose(fpCanSourceFile);
			if(fpCanDestFile) fclose(fpCanDestFile);

			_unlink(destFileName);		//PC와 모듈이 맞지 않는 정보일 경우 복구 할 수 없음 

			LockFileUpdate(bLockFile);

			return FALSE;
		}
		else //ksj 20200127 : 정상 원본 복사 누락 추가. RestoreLostDataFromFolder에는 있는데 여기엔 없다.
		{
			//PC에서 정상적인 원본에서 임시 복구 파일로 Data를 복사한다.
			if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
			{
				TRACE("Backup File write Error %s\n", destFileName);
			}
			if(nAuxColSize > 0)
			{
				if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destAuxFileName);
				}
			}
			if(nCanColSize > 0)
			{
				if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destCanFileName);
				}
			}


		}	//정상
		nSequenceNo++;

	} //while(bRun) END

	//10.1 cyc,can,aux 결과데이터 생성완료.
	if(pfBuff)
	{
		delete [] pfBuff;
		pfBuff = NULL;
	}

	if(pfSrcBuff)
	{
		delete[] pfSrcBuff;
		pfSrcBuff = NULL;
	}

	if(pfAuxSrcBuff)
	{
		delete [] pfAuxSrcBuff;
		pfAuxSrcBuff = NULL;
	}
	if(pCanSrcBuff)
	{
		delete [] pCanSrcBuff;
		pCanSrcBuff = NULL;
	}

	if(fpDestFile)		fflush(fpDestFile);
	if(fpAuxDestFile)	fflush(fpAuxDestFile);
	if(fpCanDestFile)	fflush(fpCanDestFile);

	if(fpSourceFile) fclose(fpSourceFile);
	if(fpDestFile) fclose(fpDestFile);

	if(fpAuxSourceFile) fclose(fpAuxSourceFile);
	if(fpAuxDestFile)	fclose(fpAuxDestFile);

	if(fpCanSourceFile) fclose(fpCanSourceFile);
	if(fpCanDestFile) fclose(fpCanDestFile);

	if(bCanceled)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
	CString strNewAuxTabelFile, strOrgAuxTableFile;
	CString strNewCanTabelFile, strOrgCanTableFile;
	CString strNewTableFile, strOrgTableFile;
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
// if(g_AppInfo.iPType==1) //lyj 20200518
// 	{
// 
// 	}
// //#else
// else
	{
	//11.0 ats,ctc,cts 각 스탭엔드 기록. 
	strOrgAuxTableFile.Format("%s.ats",srcFileName.Mid(0, srcFileName.ReverseFind('.')));
	strNewAuxTabelFile.Format("%s_Backup.ats",srcFileName.Mid(0, srcFileName.ReverseFind('.')));

	strOrgCanTableFile.Format("%s.ctc",srcFileName.Mid(0, srcFileName.ReverseFind('.')));
	strNewCanTabelFile.Format("%s_Backup.ctc",srcFileName.Mid(0, srcFileName.ReverseFind('.')));

	strOrgTableFile.Format("%s.cts",srcFileName.Mid(0, srcFileName.ReverseFind('.')));
	strNewTableFile.Format("%s_Backup.cts",srcFileName.Mid(0, srcFileName.ReverseFind('.')));

	_unlink(strNewAuxTabelFile);		
	_unlink(strNewCanTabelFile);		
	_unlink(strNewTableFile);	
	}	



	//strTemp.Format("M%02dCH%02d 손실된 Index 정보를 갱신중입니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg25","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(90);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
// #ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark 
// 	if(g_AppInfo.iPType==1) //lyj 20200518
// 		{
// 		}
// 	#else
// 	else
 		{
		if(RemakePCIndexFile(destFileName, strStepEndFile, strNewTableFile, strOrgTableFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	
		if(nAuxColSize > 0)
		{
			if(RemakeAuxPCIndexFile(strOrgAuxTableFile, strAuxTStepEndFile, strAuxVStepEndFile, strNewAuxTabelFile) == FALSE)
			{
				LockFileUpdate(bLockFile);
				return FALSE;
			}
		}

		if(nCanColSize > 0)
		{
			if(RemakeCanPCIndexFile(strOrgCanTableFile, strCanMStepEndFile, strCanSStepEndFile, strNewCanTabelFile) == FALSE)
			{
				LockFileUpdate(bLockFile);
				return FALSE;
			}
		}
	}
//#endif
	//12. 원본 파일을 삭제합니다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
// if(g_AppInfo.iPType==1) //lyj 20200518
// 	{
// 	}
// //#else
// else
	{
		ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
		sprintf(szFrom, "%s", strOrgTableFile);
		::DeleteFile(szFrom);
	}
//#endif
	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcFileName);
	::DeleteFile(szFrom);
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
// if(g_AppInfo.iPType==1) //lyj 20200518
// 	{
// 	}
// //#else
// else
	{
		ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
		sprintf(szFrom, "%s", strOrgAuxTableFile);
		::DeleteFile(szFrom);

		ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
		sprintf(szFrom, "%s", strOrgCanTableFile);
		::DeleteFile(szFrom);
	}
//#endif
	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcAuxFileName);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcCanFileName);
	::DeleteFile(szFrom);


	


	progressWnd.SetPos(98);
	//strTemp.Format("M%02dCH%02d 복구 완료중입니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg26","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetText(strTemp);
	
	if(nAuxColSize > 0)
	{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
// 	if(g_AppInfo.iPType==1) //lyj 20200518
// 		{
// 		}
// //#else
// else
		{
			if(rename(strNewAuxTabelFile, strOrgAuxTableFile) != 0)
			{
				//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg27","CYCLERCHANNEL"), srcAuxFileName);//&&
				LockFileUpdate(bLockFile);
				return FALSE;
			}
		}
//#endif

		if(rename(destAuxFileName, srcAuxFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg27","CYCLERCHANNEL"), srcAuxFileName);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}	

	if(nCanColSize > 0)
	{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
// if(g_AppInfo.iPType==1) //lyj 20200518
// 		{
// 		}
// //#else
// else
		{
			if(rename(strNewCanTabelFile, strOrgCanTableFile) != 0)
			{
				//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg27","CYCLERCHANNEL"), srcCanFileName);//&&
				LockFileUpdate(bLockFile);
				return FALSE;
			}
		}
//#endif
		if(rename(destCanFileName, srcCanFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg27","CYCLERCHANNEL"), srcCanFileName);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}

	}
	
	if(rename(destFileName, srcFileName) != 0)
	{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", destFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg27","CYCLERCHANNEL"), destFileName);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
// if(g_AppInfo.iPType==1) //lyj 20200518
// 	{
// 	}
// //#else
// else
 	{
		if(rename(strNewTableFile, strOrgTableFile) != 0)
		{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strNewTableFile);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg27","CYCLERCHANNEL"), strNewTableFile);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
		}
	}
//#endif
	LockFileUpdate(bLockFile);
	
	//strTemp.Format("M%02dCH%02d 복구를 완료 하였습니다.", m_nModuleID, nChannelID);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg28","CYCLERCHANNEL"), m_nModuleID, nChannelID);//&&
	progressWnd.SetPos(100);
	progressWnd.SetText(strTemp);
	progressWnd.PeekAndPump();
	
	Sleep(200);

	m_bDataLoss = FALSE;
	//WriteLog("Data 복구 완료");
	WriteLog(Fun_FindMsg("CyclerChannel_RestoreLostDataFromFolder2_msg29","CYCLERCHANNEL"));//&&
	
	return TRUE;
}

BOOL CCyclerChannel::Fun_DownloadData(CString strIPAddress, CString strLocalFolder,UINT nModuleID, UINT nChIndex, BOOL bAlarmFlag)
{
	CString strTemp;
	if(strIPAddress.IsEmpty())
	{
		//strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	nModuleID);
		strTemp.Format(Fun_FindMsg("CyclerChannel_Fun_DownloadData_msg1","CYCLERCHANNEL"),	nModuleID);//&&
		AfxMessageBox(strTemp);
		return 0;
	}
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		//m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", nModuleID);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_Fun_DownloadData_msg2","CYCLERCHANNEL"), nModuleID);//&&
		delete pDownLoad;
		return FALSE;
	}
	
	CString strRemoteFolder;

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
		//ksj 20171025 : 주석처리.
		if (nChIndex == 2) nChIndex = 3;	//ljb 20170630 add 강제로 채널 변경
	}
//#endif
	strRemoteFolder.Format("cycler_data//resultData//group1//data0//ch%03d//", nChIndex);
	//strStartIndexFile = strLocalTempFolder + "\\savingFileIndex_start.csv";

	int nDownCount = pDownLoad->DownLoadAllFile(strLocalFolder, strRemoteFolder, nModuleID, nChIndex);
	if( nDownCount < 1)
	{
		//m_strLastErrorString.Format("M%02dCH%02d down load 실패.", nModuleID, nChIndex);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_Fun_DownloadData_msg3","CYCLERCHANNEL"), nModuleID, nChIndex);//&&
		delete pDownLoad;
		return FALSE;
	}
	else
	{
		strTemp.Format("FTP Download count = %d",nDownCount);
		if (bAlarmFlag) AfxMessageBox(strTemp);
	}

	delete pDownLoad;
	return TRUE;
}

// 기존 파일을 읽어서 손상된 구간은 Module에서 DownLoad한다.
BOOL CCyclerChannel::RestoreLostData(CString strIPAddress)
{
	CString srcFileName, destFileName;
	CString strTemp;

	if(strIPAddress.IsEmpty())
	{
		//strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	m_nModuleID);
		strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg1","CYCLERCHANNEL"),	m_nModuleID);//&&
		AfxMessageBox(strTemp);
		return 0;
	}

//////////////////////////////////////////////////////////////////////////
	//CProgressWnd progressWnd(AfxGetMainWnd(), "복구중...", TRUE, TRUE, TRUE);
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("CyclerChannel_RestoreLostData_msg2","CYCLERCHANNEL"), TRUE, TRUE, TRUE);//&&
	progressWnd.SetRange(0, 100);
//////////////////////////////////////////////////////////////////////////

	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	//Get ip address

//////////////////////////////////////////////////////////////////////////
//	strIPAddress = "192.168.9.10";
//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
//////////////////////////////////////////////////////////////////////////
	
	//strTemp.Format("Module %d에 접속 시도 중입니다.(IP %s)", m_nModuleID, strIPAddress);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg3","CYCLERCHANNEL"), m_nModuleID, strIPAddress);//&&
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	BOOL bConnect = pDownLoad->ConnectFtpSession(strIPAddress);
	if(bConnect == FALSE)
	{
		//m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", m_nModuleID);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg4","CYCLERCHANNEL"), m_nModuleID);//&&
		delete pDownLoad;
		return FALSE;
	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;
	CString strAuxTempLocalFileName, strAuxVoltLocalFileName, strCanMLocalFileName, strCanSLocalFileName;
	CString strLocalTempFolder(szBuff);
	CString strStartIndexFile, strEndIndexFile, strStepEndFile;
	CString strAuxTStepEndFile, strAuxVStepEndFile, strCanMStepEndFile, strCanSStepEndFile; 

	//1. 시작 Index파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d Index 시작 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg5","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(10);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

//	strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", m_nChannelIndex+1);

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
		//ksj 20171117 : 주석처리
		if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_start.csv", m_nChannelIndex+2);	//ljb 20170829 add
		else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_start.csv", m_nChannelIndex+1);	//ljb 20170829 add
	}
//#else
else
	{
		//ksj 20171117 : 원래 기능으로 복원
		strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_start.csv", m_nChannelIndex+1);
	
	}
//#endif
	strStartIndexFile = strLocalTempFolder + "\\savingFileIndex_start.csv";
	if(pDownLoad->DownLoadFile(strStartIndexFile, strRemoteFileName) < 1)
	{
		//m_strLastErrorString.Format("M%02dCH%02d 시작 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg6","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
		delete pDownLoad;//$1013
		return FALSE;
	}

	//2. 최종 Index 파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d Index 마지막 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg7","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(20);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
		if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_last.csv", m_nChannelIndex+2); //ljb 20170829 add
		else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_last.csv", m_nChannelIndex+1);
	}
//#else
else
	{
		//ksj 20171117 : 원래 기능으로 복원
		strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_last.csv", m_nChannelIndex+1);
	}
//#endif
	strEndIndexFile = strLocalTempFolder + "\\savingFileIndex_last.csv";
	if(pDownLoad->DownLoadFile(strEndIndexFile, strRemoteFileName) < 1)
	{
		//가끔 download 실패가 발생한다.
		//m_strLastErrorString.Format("M%02dCH%02d 최종 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg8","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
		delete pDownLoad;
		TRACE("%s download fail\n", strRemoteFileName);
		return FALSE;
	}

	//3. Step End Data 파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d 각 Step의 완료 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg9","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(30);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		delete pDownLoad;
		return FALSE;
	}

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
		//if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData.csv", m_nChannelIndex+2);//ljb 20170829 add
		if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData.csv", m_nChannelIndex+2,m_nChannelIndex+2); //lyj 20201020 KTL
		//else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData.csv", m_nChannelIndex+1);
		else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData.csv", m_nChannelIndex+1,m_nChannelIndex+1); //lyj 20201020 KTL 경로 수정
	}
//#else
else
	{
	//ksj 20171117 : 원래 기능으로 복원
	//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData.csv", m_nChannelIndex+1);
	strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData.csv", m_nChannelIndex+1,m_nChannelIndex+1); //lyj 20201020 KTL 경로 수정
	}
//#endif
	strStepEndFile = strLocalTempFolder + "\\stepEndData.csv";

	if(pDownLoad->DownLoadFile(strStepEndFile, strRemoteFileName) < 1)
	{
		//가끔 download 실패가 발생한다.//$1013
		//m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg10","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
		delete pDownLoad;
		TRACE("%s download fail\n", strRemoteFileName);
		return FALSE;
	}

	//Aux용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
	m_chFileDataSet.ReLoadData();		//ljb 20150123 add
	if(m_chFileDataSet.m_nAuxTempCount > 0)
	{
//	#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
			//ksj 20171117 : 주석처리
		//if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+2, m_nChannelIndex+2);//ljb 20170829 add
		//if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+2);//lyj 20200518																															
		if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+2,m_nChannelIndex+2); //lyj 20201020 KTL 경로 수정
		//else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		//else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+1); //lyj 20200518
		else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+1,m_nChannelIndex+1); //lyj 20201020 KTL 경로 수정
	}
//#else
else
	{
		//ksj 20171117 : 원래 기능으로 복원
		//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1); //lyj 20201020 KTL 경로 수정
	}
//#endif
		strAuxTStepEndFile = strLocalTempFolder + "\\stepAuxTEndData.csv";
		if(pDownLoad->DownLoadFile(strAuxTStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.
			//m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg11","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}
	}
	
	if(m_chFileDataSet.m_nAuxVoltCount > 0)
	{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			//ksj 20171117 : 주석처리
			//if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+2, m_nChannelIndex+2);//ljb 20170829 add
			//else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1);//ljb 20170829 add
// 			if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+2);//lyj 20200518
// 			else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1); //lyj 20200518  
			if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+2,m_nChannelIndex+2);//lyj 20201020 KTL 경로 수정
			else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1,m_nChannelIndex+1); //lyj 20201020 KTL 경로 수정
		}
//#else
else
		{
			//ksj 20171117 : 원래 기능으로 복원
			//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1);
			//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1); //lyj 20200518																														 
			strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1); //lyj 20201020 KTL 경로 수정
		}
//#endif
		strAuxVStepEndFile = strLocalTempFolder + "\\stepAuxVEndData.csv";
		if(pDownLoad->DownLoadFile(strAuxVStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.
			//m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg12","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Can용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
	if(m_chFileDataSet.m_nCanMasterCount > 0)
	{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			//ksj 20171117 : 주석처리
			//if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+2, m_nChannelIndex+2);//ljb 20170829 add
			//else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1);
// 			if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+2);//lyj 20200518
// 			else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1); //lyj 20200518																																					   
			if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+2, m_nChannelIndex+2);//lyj 20201020 KTL 경로수정
			else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1); //lyj 20201020 KTL 경로수정	
//#else
		}
else
		{
			//ksj 20171117 : 원래 기능으로 복원
			//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1);
			//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1); //lyj 20200518																															  
			strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1); //lyj 20201020 KTL 경로수정																														  
		}
//#endif
		strCanMStepEndFile = strLocalTempFolder + "\\stepCanMEndData.csv";
		if(pDownLoad->DownLoadFile(strCanMStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.
			//m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg13","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}

	}
	
	if(m_chFileDataSet.m_nCanSlaveCount > 0)
	{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			//ksj 20171117 : 주석처리
			//if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+2, m_nChannelIndex+2);//ljb 20170829 add
			//else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1);
// 			if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+2 ); //lyj 20200518
// 			else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1 );//lyj 20200518
			if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+2,m_nChannelIndex+2 ); //lyj 20201020 KTL 경로수정		
			else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1,m_nChannelIndex+1 ); //lyj 20201020 KTL 경로수정		
		}
//#else
else
		{
			//ksj 20171117 : 원래 기능으로 복원
			//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1);
			//strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1);	//lyj 20200518																														 
			strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1,m_nChannelIndex+1);	//lyj 20201020 KTL 경로수정																												 
		}
//#endif
		strCanSStepEndFile = strLocalTempFolder + "\\stepCanSEndData.csv";
		if(pDownLoad->DownLoadFile(strCanSStepEndFile, strRemoteFileName) < 1)
		{
			//가끔 download 실패가 발생한다.//$1013
			//m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg14","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
			delete pDownLoad;
			TRACE("%s download fail\n", strRemoteFileName);
			return FALSE;
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////
//	strStartIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_start.csv";
//	strEndIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_last.csv";
//////////////////////////////////////////////////////////////////////////

	//4. Module쪽 Index 파일과 PC쪽 결과 파일을 비교하면서 손실구간을 검색한다.
	//PC쪽에 Data가 하나도 저장안되었을 경우도 복구 가능하도록 
	//현재 작업중인 경우 결과를 파일에 저장하지 않고 Buffer에 쌓고 있도록 한다.
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	strTemp.Format("손실된 구간을 검색중입니다.");
	progressWnd.SetPos(40);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		//m_strLastErrorString.Format("M%02dCH%02d 사용자에 의해 취소됨", m_nModuleID, m_nChannelIndex+1);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg15","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
		LockFileUpdate(bLockFile);//$1013
		delete pDownLoad;
		return FALSE;
	}
#if 0 //ksj 20210419 : 비활성화 처리. 이미 손실 구간이 있으니 복구 버튼이 활성화 되었을 것이므로, 손실 구간 중복 검색 피해서 시간 낭비 줄인다.

	long lLostDataCount =0;
	BOOL nRtn = SearchLostDataRange(strStartIndexFile, strEndIndexFile, lLostDataCount, &progressWnd);
	if(nRtn == FALSE)
	{
		//m_strLastErrorString.Format("M%02dCH%02d 손실구간 검색 실패!!!", m_nModuleID, m_nChannelIndex+1);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg16","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
		LockFileUpdate(bLockFile);//$1013
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}

	if(lLostDataCount <= 0)
	{
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		//m_strLastErrorString.Format("M%02dCH%02d 손실된 data 정보가 없습니다.", m_nModuleID, m_nChannelIndex+1);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg17","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
		return TRUE;	//실제 Data 분석 //$1013
	}
#endif

	//4. 손실된 구간이 있을 경우 해당 구간의 복구용 파일을 Module에서 Download한다.
//	long nSquenceNo = 0;
	BOOL bDownLoadOK = FALSE;

	srcFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;				//복구하고자 하는 파일
	destFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_RAW_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcAuxFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_AUX_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destAuxFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_AUX_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destAuxFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcCanFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_CAN_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destCanFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_CAN_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destCanFileName);		//Backup할 파일명이 있으면 삭제 

//////////////////////////////////////////////////////////////////////////
//	srcFileName = "C:\\test1\\M01Ch01\\Test1.cyc";
//	destFileName = "C:\\test1\\M01Ch01\\Test1_BackUp.cyc";
//////////////////////////////////////////////////////////////////////////

	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cyc 파일용
	FILE *fpAuxSourceFile = NULL, *fpAuxDestFile = NULL;	//aux 파일용
	FILE *fpCanSourceFile = NULL, *fpCanDestFile = NULL;	//can 파일용

	//cyc용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////

	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg18","CYCLERCHANNEL"), destFileName);//&&
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE; 
	}

	int nColSize = 0;
	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)//$1013
		{
			//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg19","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpDestFile);
			delete pDownLoad;
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg20","CYCLERCHANNEL"), srcFileName);//&&
		LockFileUpdate(bLockFile);
		fclose(fpDestFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg21","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			delete pDownLoad;
			return FALSE;	
		}
		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	
	if(m_chFileDataSet.GetAuxColumnCount() > 0)			
	{
		fpAuxDestFile = fopen(destAuxFileName, "wb");
		if(fpAuxDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg22","CYCLERCHANNEL"), destAuxFileName);//&&
			return FALSE; 
		}

		fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			
			//if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.GetAuxColumnCount()) == FALSE)
			if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.m_nAuxVoltCount, m_chFileDataSet.m_nAuxTempCount) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg23","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxDestFile);
				return FALSE;	 
			}
			fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		}
		
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg24","CYCLERCHANNEL"), srcAuxFileName);//&&
			fclose(fpAuxDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxSourceFile) > 0)
		{
			if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg25","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxSourceFile);
				fclose(fpAuxDestFile);
				return FALSE;	
			}
		}
		nAuxColSize = auxFileHeader.auxHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//CAN용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	if(m_chFileDataSet.GetCanColumnCount() > 0)			
	{
		fpCanDestFile = fopen(destCanFileName, "wb");
		if(fpCanDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg26","CYCLERCHANNEL"), destCanFileName);//&&
			return FALSE; 
		}

		fpCanSourceFile = fopen(srcCanFileName, "rb");
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			if(CreateCanFileA(srcCanFileName, 0, m_chFileDataSet.GetCanColumnCount()) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg27","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanDestFile);
				return FALSE;	 
			}
			fpCanSourceFile = fopen(srcCanFileName, "rb");
		}
		
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg28","CYCLERCHANNEL"), srcCanFileName);//&&
			fclose(fpCanDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg29","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanSourceFile);
				fclose(fpCanDestFile);
				return FALSE;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	//파일이 잘못되었을 경우 
	//결과 데이터 해더 사이즈 비교.
	if(nColSize < 1 || nColSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		//m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", srcFileName, nColSize);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg30","CYCLERCHANNEL"), srcFileName, nColSize);//&&
		LockFileUpdate(bLockFile);
		fclose(fpSourceFile);
		fclose(fpDestFile);
		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}

	//strTemp.Format("M%02dCH%02d 손실된 구간을 복구중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg31","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(50);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		delete pDownLoad;
		return FALSE;
	}

	UINT nFileSeq = 0;
	ULONG nSequenceNo = 1;
	int nBackupFileNo = 0;
	//File Read Buffer
	float *pfSrcBuff = new float[nColSize];
	float *pfBuff = new float[nColSize];
	float *pfAuxSrcBuff = NULL;
	CAN_VALUE *pCanSrcBuff = NULL;
	if(nAuxColSize > 0)
		pfAuxSrcBuff = new float[nAuxColSize];
	if(nCanColSize > 0)
		pCanSrcBuff = new CAN_VALUE[nCanColSize];
	
	BOOL bCanceled = FALSE;
	BOOL bRun = TRUE;
	int nCount = 0;
	int nPos;
	int nSaveTotSize = (ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 

	int nAuxCount = 0, nCanCount = 0;

	while(bRun)
	{
		//Progress 50~90구간 사용 
		nPos = 50 + int(40.0f*(float)nCount/(float)nSaveTotSize);

//		TRACE("TotLost Count %d, nCount %d, Pos %d\n", nSaveTotSize, nCount, nPos);

		progressWnd.SetPos(nPos);
		
		//Canceled 
		if(progressWnd.PeekAndPump() == FALSE)
		{
			bCanceled = TRUE;
			break;
		}
		nCount++;
		
		//PC쪽 파일은 순서대로 읽는다.
		size_t rdSize = fread(pfSrcBuff, sizeof(float), nColSize, fpSourceFile );

		//Aux,Can 파일도 Raw 파일의 인덱스를 따라간다.
		if(nAuxColSize)
			fread(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxSourceFile);
		if(nCanColSize)
			fread(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanSourceFile);

		if(rdSize <	1)	//PC쪽 파일을 다 읽었을 경우 모듈의 최종 Index와 같은지 확인 한다. 모듈쪽 저장 인텍스가 더크면 뒤쪽 data 최종 복구한다.
		{	
			nFileSeq = nSaveTotSize;//(ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 
			bRun = FALSE;	//PC쪽은 다 읽었음 
		}
		else
		{
			nFileSeq = (ULONG)pfSrcBuff[0];
		}

				
		//Index 검사
		long nStartNo = 0, nEndNo = 0, lLossCount =0;
		
		//PC쪽 검사 결과 data loss range detected	
		
		if(nFileSeq > nSequenceNo || bRun == FALSE)		
		{
			//nFileSeq 크기대문에 nSequenceNo 부터는 손실된데이터가 된다.
			nStartNo = nSequenceNo;
			//기존 파일보다 크면 차이나는 구간 만큼 카운트한다.
			while(nFileSeq > nSequenceNo)
			{
				nSequenceNo++;
			}
			nEndNo = nSequenceNo-1;

			int nRestordDataSize = 0;
			lLossCount = nEndNo - nStartNo+1;
			long lTemp = 0;
			while(lLossCount > 0)	//1개의 손실 구간이 모두 복구됨 
			{
				//5. 손실된 부분에 해당하는 복구용 파일을 Index 파일에서 찾아낸다.
				nStartNo += nRestordDataSize;
				lTemp = m_RestoreFileIndexTable.FindFirstMachFileNo(nStartNo);	
				if(lTemp < 1)	
				{
					//현재 손실 구간을 SBC에서 Dowload한 파일목록에서 발견하지 못했을 경우, 
					//기타 오류로 복구 불가시는 현재구간은 복구를 생략한다.
					nRestordDataSize = lLossCount;
					lLossCount = 0;
					break;
				}
				
				//이전에 DownLoad한 파일에 손상된 구간이 있을 경우는 Dowload를 생략한다.
				if(nBackupFileNo != lTemp)
				{
					nBackupFileNo = lTemp;
					//6. 복구용 파일을 DownLoad 받는다.
					
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
					{
						//ksj 20171117 : 주석
						if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d.csv", m_nChannelIndex+2, m_nChannelIndex+2, nBackupFileNo);//ljb 20170829 add
						else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
					}
//#else
else
					{
						//ksj 20171117 : 원래 기능으로 복원
						strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
					}
//#endif
				
					strLocalFileName = strLocalTempFolder + "\\ChRawData.csv";
					bDownLoadOK = TRUE;
					if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) == FALSE)
					{
						//복구용 파일이 존재하지 않을 경우(복구최대시간경과로 삭제된 경우나 기타 오류로 복구 불가)
						//DownLoad 실패시 손실 시작부터 파일에 기록된 가장 마지막 크기 만큼 뛰어 넘고 복구 생략 
						nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);
						bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nAuxTempCount > 0)
					{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
						{
							//ksj 20171117 : 주석처리
							if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxT.csv", m_nChannelIndex+2, m_nChannelIndex+2, nBackupFileNo);//ljb 20170829 add
							else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						}
//#else
else	
						//ksj 20171117 : 원래 기능으로 복원
							strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
//#endif
					}
						strAuxTempLocalFileName = strLocalTempFolder + "\\ChAuxTData.csv";
						if(pDownLoad->DownLoadFile(strAuxTempLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;

					if(m_chFileDataSet.m_nAuxVoltCount > 0)
					{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
						{
						//ksj 20171117 : 주석처리
							if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxV.csv", m_nChannelIndex+2, m_nChannelIndex+2, nBackupFileNo);//ljb 20170829 add
							else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);						
						}
//#else
else
						{
						//ksj 20171117 : 원래 기능으로 복원
							strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						}
//#endif

						strAuxVoltLocalFileName = strLocalTempFolder + "\\ChAuxVData.csv";
						if(pDownLoad->DownLoadFile(strAuxVoltLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nCanMasterCount > 0)
					{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
						{
						//ksj 20171117 : 주석처리
							if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canMaster.csv", m_nChannelIndex+2, m_nChannelIndex+2, nBackupFileNo);//ljb 20170829 add
							else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						}
//#else
else
						{
							//ksj 20171117 : 원래 기능으로 복원
							strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						}
//#endif
						strCanMLocalFileName = strLocalTempFolder + "\\ChCanMData.csv";
						if(pDownLoad->DownLoadFile(strCanMLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nCanSlaveCount > 0)
					{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
						{
						//ksj 20171117 : 주석처리
							if (m_nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canSlave.csv", m_nChannelIndex+2, m_nChannelIndex+2, nBackupFileNo);//ljb 20170829 add
							else strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						}
//#else
else
						{
						//ksj 20171117 : 원래 기능으로 복원
							strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						}
//#endif
						
						strCanSLocalFileName = strLocalTempFolder + "\\ChCanSData.csv";
						if(pDownLoad->DownLoadFile(strCanSLocalFileName, strRemoteFileName) == FALSE)
							bDownLoadOK = FALSE;
					}
				}

				//7. DownLoad 파일로 복구 한다.
				// 7.1 data가 부족하면 다음 파일을 DownLoad한다.
				if(bDownLoadOK)
				{					
					//복구 시작
					nRestordDataSize = WriteRestoreRawFile(strLocalFileName, fpDestFile, FileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nAuxColSize)
						WriteRestoreAuxFile(strAuxTempLocalFileName, strAuxVoltLocalFileName, fpAuxDestFile, auxFileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nCanColSize)
						WriteRestoreCanFile(strCanMLocalFileName, strCanSLocalFileName, fpCanDestFile, canFileHeader, nStartNo, nEndNo, nBackupFileNo);
					

				}
				//strTemp.Format("Data 복구 실시, 시작Index: %d, 복수 수량:%d개",  nStartNo, nRestordDataSize);
				strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg32","CYCLERCHANNEL"),  nStartNo, nRestordDataSize);//&&
				WriteLog(strTemp);
				lLossCount -= nRestordDataSize;	//if(lLossCount > 0) Download한 파일에 복구 data가 모두 없고 다음파일까지 이어짐 
			}
			
			if(bRun)
			{
				//PC쪽 읽을 data가 아직 남아 있으면 손실 구간 가장 마지막 다음 data까지 읽혔으므로 
				//한줄더 읽어들인 data를 저장한다.
				if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destFileName);
				}
				if(nAuxColSize > 0)
				{
					if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destAuxFileName);
					}
				}
				if(nCanColSize > 0)
				{
					if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destCanFileName);
					}
				}
			}	
			nSequenceNo = nFileSeq;	//손실된 구간 복구 완료 
		}
		else if(nFileSeq < nSequenceNo)	//Module index 오류
		{
			//PC와 모듈의 시험이 불일치 하거나 모듈에 저장된 파일의 index가 잘못 되었을 경우 
			//TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);
			TRACE(Fun_FindMsg("CyclerChannel_RestoreLostData_msg33","CYCLERCHANNEL"), nFileSeq, nSequenceNo);//&&

			//m_strLastErrorString.Format("M%02dCH%02d 모듈 정보와 Host 정보가 불일치 합니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg34","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&

			if(pfBuff)
			{
				delete [] pfBuff;
				pfBuff = NULL;
			}

			if(pfSrcBuff)
			{
				delete[] pfSrcBuff;
				pfSrcBuff = NULL;
			}
			if(pfAuxSrcBuff)
			{
				delete [] pfAuxSrcBuff;
				pfAuxSrcBuff = NULL;
			}
			if(pCanSrcBuff)
			{
				delete [] pCanSrcBuff;
				pCanSrcBuff = NULL;
			}

			if(fpDestFile)		fflush(fpDestFile);
			if(fpAuxDestFile)	fflush(fpAuxDestFile);
			if(fpCanDestFile)	fflush(fpCanDestFile);

			if(fpSourceFile) fclose(fpSourceFile);
			if(fpDestFile) fclose(fpDestFile);
			if(fpAuxSourceFile) fclose(fpAuxSourceFile);
			if(fpAuxDestFile) fclose(fpAuxDestFile);
			if(fpCanSourceFile) fclose(fpCanSourceFile);
			if(fpCanDestFile) fclose(fpCanDestFile);

			delete pDownLoad;

			_unlink(destFileName);		//PC와 모듈이 맞지 않는 정보일 경우 복구 할 수 없음 
			
			LockFileUpdate(bLockFile);

			return FALSE;
		}
		else
		{
			//PC에서 정상적인 원본에서 임시 복구 파일로 Data를 복사한다.
			if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
			{
				TRACE("Backup File write Error %s\n", destFileName);
			}
			if(nAuxColSize > 0)
			{
				if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destAuxFileName);
				}
			}
			if(nCanColSize > 0)
			{
				if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destCanFileName);
				}
			}

		
		}	//정상
		nSequenceNo++;

	} //while(bRun) END

	if(pfBuff)
	{
		delete [] pfBuff;
		pfBuff = NULL;
	}

	if(pfSrcBuff)
	{
		delete[] pfSrcBuff;
		pfSrcBuff = NULL;
	}

	if(pfAuxSrcBuff)
	{
		delete [] pfAuxSrcBuff;
		pfAuxSrcBuff = NULL;
	}
	if(pCanSrcBuff)
	{
		delete [] pCanSrcBuff;
		pCanSrcBuff = NULL;
	}

	if(fpDestFile)		fflush(fpDestFile);
	if(fpAuxDestFile)	fflush(fpAuxDestFile);
	if(fpCanDestFile)	fflush(fpCanDestFile);

	if(fpSourceFile) fclose(fpSourceFile);
	if(fpDestFile) fclose(fpDestFile);

	if(fpAuxSourceFile) fclose(fpAuxSourceFile);
	if(fpAuxDestFile)	fclose(fpAuxDestFile);

	if(fpCanSourceFile) fclose(fpCanSourceFile);
	if(fpCanDestFile) fclose(fpCanDestFile);
	delete pDownLoad;

	if(bCanceled)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//복구된 파일을 이용하여 rpt 파일을 생성한다.
	CString strNewTableFile, strOrgTableFile;
	strNewTableFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_RESULT_FILE_NAME_EXT;

	//Aux,Can용 결과 데이터 파일
	CString strNewAuxTabelFile, strOrgAuxTableFile;
	strNewAuxTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_AUX_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgAuxTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_AUX_RESULT_FILE_NAME_EXT;

	CString strNewCanTabelFile, strOrgCanTableFile;
	strNewCanTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_CAN_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgCanTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_CAN_RESULT_FILE_NAME_EXT;

	_unlink(strNewTableFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewAuxTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewCanTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 

//////////////////////////////////////////////////////////////////////////
//	strNewTableFile = "C:\\test1\\M01Ch01\\Test1_BackUp.cts";
//	strOrgTableFile = "C:\\test1\\M01Ch01\\Test1.cts";
//////////////////////////////////////////////////////////////////////////

	//strTemp.Format("M%02dCH%02d 손실된 Index 정보를 갱신중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg35","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(90);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//Backup이 완료된 Raw파일을 이용하여 rpt 파일을 새로 생성	
	if(RemakePCIndexFile(destFileName, strStepEndFile, strNewTableFile, strOrgTableFile) == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	//ljb 에러부분
	if(nAuxColSize > 0)
	{
		if(RemakeAuxPCIndexFile(destAuxFileName, strAuxTStepEndFile, strAuxVStepEndFile, strNewAuxTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	if(nCanColSize > 0)
	{
		if(RemakeCanPCIndexFile(destCanFileName, strCanMStepEndFile, strCanSStepEndFile, strNewCanTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	//strTemp.Format("M%02dCH%02d 복구용 임시 파일을 삭제중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg36","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(95);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
	//원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	

	sprintf(szFrom, "%s", strOrgTableFile);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgAuxTableFile);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgCanTableFile);
	::DeleteFile(szFrom);
	
	ZeroMemory(szFrom, _MAX_PATH);			//Double NULL Terminate
	sprintf(szFrom, "%s", srcFileName);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcAuxFileName);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcCanFileName);
	::DeleteFile(szFrom);


	progressWnd.SetPos(98);
	//strTemp.Format("M%02dCH%02d 복구 완료중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg37","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetText(strTemp);
	//Backup한 파일이름을 바꾼다.
	if(rename(destFileName, srcFileName) != 0)
	{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg38","CYCLERCHANNEL"), srcFileName);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	if(nAuxColSize > 0)
	{
		if(rename(destAuxFileName, srcAuxFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg38","CYCLERCHANNEL"), srcAuxFileName); //&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewAuxTabelFile, strOrgAuxTableFile) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgAuxTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg38","CYCLERCHANNEL"), strOrgAuxTableFile); //&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}	

	if(nCanColSize > 0)
	{
		if(rename(destCanFileName, srcCanFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg38","CYCLERCHANNEL"), srcCanFileName);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewCanTabelFile, strOrgCanTableFile) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgCanTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg38","CYCLERCHANNEL"), strOrgCanTableFile);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}
	
	if(rename(strNewTableFile, strOrgTableFile) != 0)
	{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgTableFile);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg38","CYCLERCHANNEL"), strOrgTableFile);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}


	LockFileUpdate(bLockFile);
	
	//strTemp.Format("M%02dCH%02d 복구를 완료 하였습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_msg39","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(100);
	progressWnd.SetText(strTemp);
	progressWnd.PeekAndPump();
	
	Sleep(200);

	m_bDataLoss = FALSE;
	m_bDataLossLogCnt = FALSE;			//20120210 KHS
	//WriteLog("Data 복구 완료");
	WriteLog(Fun_FindMsg("CyclerChannel_RestoreLostData_msg40","CYCLERCHANNEL"));//&&
	
	return TRUE;
}


//현재 진행중인 결과 파일을 최종 Data로 갱신한다.
//파일 용량이 많은 경우 시간지연이 발생할 수 있음 
BOOL CCyclerChannel::ReloadReaultData(BOOL bIncludeWorkingStep)
{
	BOOL bLockFile = LockFileUpdate(TRUE);
	//2014.11.05  Create - > CreateTable변경
	BOOL nRtn = m_chFileDataSet.CreateTable(bIncludeWorkingStep);
		
	LockFileUpdate(bLockFile);

	if(nRtn == FALSE)
	{
		//m_strLastErrorString = "최종 결과 파일 Loadig 실패";
		m_strLastErrorString = Fun_FindMsg("CyclerChannel_ReloadReaultData_msg1","CYCLERCHANNEL");//&&
	}

	return nRtn;
}

//	PC에 저장된 결과 파일을 검색하여 Data 손실 구간을 검색한다.
//	bIncludeInitCh =>PC쪽의 정보를 찾을 수 없을 경우 복구 여부 
//	2014.10.09 파일분할 방식 100B -> 100D로 업데이트 되면서 문제가 발생함.
BOOL CCyclerChannel::SearchLostDataRange(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd, BOOL bIncludeInitCh )
{
	char szBuff[1000];
	CString strTemp;
	
	//strTemp.Format("데이터 손실 구간 검색 시작. ");
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg1","CYCLERCHANNEL"));//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
	
	lLostDataCount = 0;
	
	m_strDataLossRangeList.RemoveAll();	

	#ifdef _DEBUG
		DWORD dwTime = GetTickCount();
	#endif

	if(strStartIndexFile.IsEmpty() || strEndIndexFile.IsEmpty())
	{
		//m_strLastErrorString = "시작 Index와 종료 Index 파일명 없음";
		m_strLastErrorString = Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg2","CYCLERCHANNEL");//&&

		//최종 시험 정보를 Loading한다.
		if(LoadInfoFromTempFile()== FALSE)	
		{
			//m_strLastErrorString.Format("Host의 최종 시험 정보를 loading할 수 없습니다.");
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg3","CYCLERCHANNEL"));//&&
			m_bDataLoss = FALSE;
			m_bDataLossLogCnt = FALSE;			//20120210 KHS
			return FALSE;
			}

		return FALSE;
	}

	#ifdef _DEBUG
		TRACE("===========================>CH%03d 1: [%dmsec]\n", m_nChannelIndex, GetTickCount()-dwTime);
	#endif

	//PC쪽 결과 파일을 Loading한다.
	//동일명으로 시험을 하였을시는 이미 파일이 삭제된 상태 
	if(ReloadReaultData() == FALSE)
	{
		//PC쪽에 아무 파일도 생성되어 있지 않으면 복구 할 수 없다.(새로은 시험은 하면서 덮어 쓰기 하였거나 해당 폴더가 삭제된 경우)
		if(bIncludeInitCh == FALSE)	
		{
			//m_strLastErrorString.Format("Host의 최종 시험 파일을 찾을 수 없습니다.");
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg4","CYCLERCHANNEL"));//&&
			return FALSE;	
		}		 
	}

	//strTemp.Format("PC 결과 파일 로드 완료. ");
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg5","CYCLERCHANNEL"));//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);	

	#ifdef _DEBUG
		TRACE("===========================>CH%03d 2: [%dmsec]\n", m_nChannelIndex, GetTickCount()-dwTime);
	#endif

	//모듈에서 downloading한 index파일을 parsing한다.
	m_RestoreFileIndexTable.SetFileName(strStartIndexFile, strEndIndexFile);
	if(m_RestoreFileIndexTable.LoadTable() == FALSE)
	{
		//m_strLastErrorString.Format("모듈의 index 정보를 loading할수 없습니다.");	
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg6","CYCLERCHANNEL"));	//&&
		return FALSE;
	}
	
	//strTemp.Format("PC 결과데이터 손실구간 검색 시작. ");
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg7","CYCLERCHANNEL"));//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

	long lDataCount = 0;
	fltPoint *pData = NULL;
	ULONG nSequenceNo = 1;	
	long nTotCount = m_chFileDataSet.GetTotalRecordCount();
	long nDataCount = 0;
	
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	if(pProgressWnd)		pProgressWnd->SetPos(0, TRUE);
	//PC Data를 순차적으로 확인하여 빠진 부분 List를 작성한다.
	UINT nFileSeq = 1;
	int nTableSize = m_chFileDataSet.GetTableSize();
	
	//strTemp.Format("테이블 구간 검색 전체 레코드 %d / 전체 사이즈 %d .",nTotCount,nTableSize);
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg8","CYCLERCHANNEL"),nTotCount,nTableSize);//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

	for(int lTableIndex =0; lTableIndex < nTableSize ; lTableIndex++)
	{		
		INT nCount = m_chFileDataSet.GetDataCount(lTableIndex) ;
		
		int nStartNo = 0, nEndNo = 0;
		//2014.11.05 기존 검색 방식에서 파일분할로 변경된 검색 방식으로 변경.
		for (int index = 0; index < nCount; index++){	
			
			fltPoint fltNo = m_chFileDataSet.GetDatumOfTable(lTableIndex, RS_COL_SEQ, index) ;
			
			nFileSeq = fltNo.y;		//sequence no
			
			if(nFileSeq > nSequenceNo)			//data loss range detected
			{
			
				nStartNo = nSequenceNo;
				while(nFileSeq > nSequenceNo)
				{
					nSequenceNo++;
				}
				
				
				nEndNo = nSequenceNo;
				//구간시작~구간끝, 해당 Step, 해당 시간 				
				strTemp.Format("%d~%d, %d", nStartNo, nEndNo-1, m_chFileDataSet.GetTableNo(lTableIndex));
				m_strDataLossRangeList.AddTail(strTemp);
				lLostDataCount += (nEndNo-nStartNo)+1;
				
				TRACE("Data loss index %d~%d(%d)\n", nStartNo, nEndNo-1, nEndNo- nStartNo+1);	
				
			}
			else if(nFileSeq < nSequenceNo)	//Module index 오류
			{
				//m_strLastErrorString.Format("Data index 이상 구간이 존재 합니다.(File:%d/Seq. No:%d)", nFileSeq, nSequenceNo);	
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg9","CYCLERCHANNEL"), nFileSeq, nSequenceNo);	//&&
				
				//AfxMessageBox("Data 파일 Index오류가 있습니다.(Module Index 오류)");
				//TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);
				TRACE(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg10","CYCLERCHANNEL"), nFileSeq, nSequenceNo);//&&
				//Index가 중복되어 저장되어 있음
			}
			
			//else	{}	//정상
			nSequenceNo++;
			
			if(pProgressWnd)
			{
				if(nTotCount > 0)
				{
					pProgressWnd->SetPos( (float)nDataCount++/(float)nTotCount*100.0f, TRUE);
				}
				
				if(pProgressWnd->PeekAndPump(TRUE) == FALSE)
				{
					if(pData)
					{
						delete [] pData;
						pData = NULL;
					}
					LockFileUpdate(bLockFile);
					
					ReloadReaultData(); //ksj 20210419: 메모리 초기화 목적으로 호출 추가.
					return FALSE;
				}
			}

		}					
		m_chFileDataSet.DeleteTableData(lTableIndex); //ksj 20210419 : 테이블 index 바뀔때마다 이전 데이터는 날려준다. (메모리 과도한 사용 방지)


		if(pData)
		{
			delete [] pData;
			pData = NULL;
		}
	}


	if(pProgressWnd)		pProgressWnd->SetPos(100, TRUE);


#ifdef _DEBUG
	TRACE("===========================>CH%03d 4: [%dmsec]\n", m_nChannelIndex, GetTickCount()-dwTime);
#endif
	
	LockFileUpdate(bLockFile);
	
	if(lLostDataCount > 0)		m_bDataLoss = TRUE;
	else						m_bDataLoss = FALSE;	
			
	//strTemp.Format("손식구간 검색 완료.");
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg11","CYCLERCHANNEL"));//&& 
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

	ReloadReaultData(); //ksj 20210419: 메모리 초기화 목적으로 호출 추가.

	return TRUE;//lLostDataCount;
}

BOOL CCyclerChannel::SearchLostDataRange2(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd, BOOL bIncludeInitCh )
{
	CString strTemp;
	lLostDataCount = 0;
	
	m_strDataLossRangeList.RemoveAll();	
	
	//모듈에서 downloading한 index파일을 parsing한다.
	m_RestoreFileIndexTable.SetFileName(strStartIndexFile, strEndIndexFile);
	if(m_RestoreFileIndexTable.LoadTable() == FALSE)
	{
		//m_strLastErrorString.Format("모듈의 index 정보를 loading할수 없습니다.");	
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange2_msg1","CYCLERCHANNEL"));	//&&
		return FALSE;
	}
	
	long lDataCount = 0;
	fltPoint *pData = NULL;
	ULONG nSequenceNo = 1;
	long lTotTime = 0, lTimeSecond = 0;
	long lPrevChTime = 0;

	long nTotCount = m_chFileDataSet.GetTotalRecordCount();
	long nDataCount = 0;
	
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	if(pProgressWnd)		pProgressWnd->SetPos(0, TRUE);
	//PC Data를 순차적으로 확인하여 빠진 부분 List를 작성한다.
	UINT nFileSeq = 1;
	int nTableSize = m_chFileDataSet.GetTableSize();
	for(int lTableIndex =0; lTableIndex < nTableSize ; lTableIndex++)
	{
		pData =  m_chFileDataSet.GetDataOfTable(lTableIndex, lDataCount, "Time", RS_COL_SEQ);
		if(pData == NULL)	continue;

		//Index 검사
		int nStartNo = 0, nEndNo = 0;
		for(int i=0; i<lDataCount; i++)
		{
			nFileSeq = (ULONG)pData[i].y;		//sequence no
//			if(i>0)			saveTime = (ULONG)(pData[i].x-pData[i-1].x);

			if(nFileSeq > nSequenceNo)			//data loss range detected
			{
				lTimeSecond = lTotTime + lPrevChTime;	
				nStartNo = nSequenceNo;
				while(nFileSeq > nSequenceNo)
				{
					nSequenceNo++;
				}

				nEndNo = nSequenceNo;
				//구간시작~구간끝, 해당 Step, 해당 시간 
//				strTemp.Format("%d~%d, %d~%d, %d", nStartNo, nEndNo-1, lTimeSecond, lTimeSecond + ((nEndNo-nStartNo)+1)*saveTime ,m_chFileDataSet.GetTableNo(lTableIndex));
				strTemp.Format("%d~%d, %d", nStartNo, nEndNo-1, m_chFileDataSet.GetTableNo(lTableIndex));
				m_strDataLossRangeList.AddTail(strTemp);
				lLostDataCount += (nEndNo-nStartNo)+1;
				
				TRACE("Data loss index %d~%d(%d)\n", nStartNo, nEndNo-1, nEndNo- nStartNo+1);	

			}
			else if(nFileSeq < nSequenceNo)	//Module index 오류
			{
				//m_strLastErrorString.Format("Data index 이상 구간이 존재 합니다.(File:%d/Seq. No:%d)", nFileSeq, nSequenceNo);	
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange2_msg2","CYCLERCHANNEL"), nFileSeq, nSequenceNo);	//&&

				//AfxMessageBox("Data 파일 Index오류가 있습니다.(Module Index 오류)");
				//TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);
				TRACE(Fun_FindMsg("CyclerChannel_SearchLostDataRange2_msg3","CYCLERCHANNEL"), nFileSeq, nSequenceNo);//&&
				//Index가 중복되어 저장되어 있음
			}
			lPrevChTime = (ULONG)pData[i].x;
			//else	{}	//정상
			nSequenceNo++;

			if(pProgressWnd)
			{
				if(nTotCount > 0)
				{
					pProgressWnd->SetPos( (float)nDataCount++/(float)nTotCount*100.0f, TRUE);
				}

				if(pProgressWnd->PeekAndPump(TRUE) == FALSE)
				{
					if(pData)
					{
						delete [] pData;
						pData = NULL;
					}
					LockFileUpdate(bLockFile);
					return FALSE;
				}
			}
		}

		lTotTime += UINT(m_chFileDataSet.GetLastDataOfTable(lTableIndex, "Time"));

		if(pData)
		{
			delete [] pData;
			pData = NULL;
		}
	}
	
	if(pProgressWnd)		pProgressWnd->SetPos(100, TRUE);



	nSequenceNo = m_RestoreFileIndexTable.GetLastCount();
	if(nFileSeq < nSequenceNo)		//모듈에 기록된 최종 Index가 PC Index보다 클 경우 마지막 Data가 손실되었다.
	{
		int nStepNo = 2;	//PC쪽에 저장된 data가 없을 경우 Step 번호를 2번으로 한다.(Step 1은 Adevance cycle step)
		nFileSeq++;
		if(nTableSize > 0)	
		{
			nStepNo =  m_chFileDataSet.GetTableNo(nTableSize-1);
		}
		strTemp.Format("%d~%d, %d", nFileSeq, nSequenceNo, nStepNo);
		TRACE("Data loss index %d~%d(%d)\n", nFileSeq, nSequenceNo, nSequenceNo- nFileSeq+1);	
		lLostDataCount += (nSequenceNo- nFileSeq)+1;
		m_strDataLossRangeList.AddTail(strTemp);
	}
	else if(nFileSeq > nSequenceNo)
	{
		//TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch하거나 모듈 최종 파일이 없슴 .(%d/%d)\n", nFileSeq, nSequenceNo);
		TRACE(Fun_FindMsg("CyclerChannel_SearchLostDataRange2_msg4","CYCLERCHANNEL"), nFileSeq, nSequenceNo);//&&
	}
	//else if(nFileSeq == nModuleLastIndex)	//정상
	LockFileUpdate(bLockFile);
	
	if(lLostDataCount > 0)		m_bDataLoss = TRUE;
	else						m_bDataLoss = FALSE;	
				
	return TRUE;//lLostDataCount;
}

//	PC에 저장된 결과 파일을 검색하여 Data 손실 구간을 검색한다.
//	bIncludeInitCh =>PC쪽의 정보를 찾을 수 없을 경우 복구 여부 
//	20180223 손실 검색 로직 수정 (cyc file에서 strEndIndexFile 비교)
BOOL CCyclerChannel::SearchLostDataRange3(CString strStartIndexFile, CString strEndIndexFile, long &lLostDataCount, CProgressWnd *pProgressWnd, BOOL bIncludeInitCh )
{
	char szBuff[1000];
	CString strTemp;
	
	//strTemp.Format("데이터 손실 구간 검색 시작. ");//$1013
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg1","CYCLERCHANNEL"));//$1013//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
	
	lLostDataCount = 0;
	
	#ifdef _DEBUG
		DWORD dwTime = GetTickCount();
	#endif

	if(strStartIndexFile.IsEmpty() || strEndIndexFile.IsEmpty())
	{
		//m_strLastErrorString = "시작 Index와 종료 Index 파일명 없음";
		m_strLastErrorString = Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg2","CYCLERCHANNEL");//&&
		return FALSE;
	}

	//모듈에서 downloading한 index파일을 parsing한다.
	m_RestoreFileIndexTable.SetFileName(strStartIndexFile, strEndIndexFile);
	if(m_RestoreFileIndexTable.LoadTable() == FALSE)
	{
		//m_strLastErrorString.Format("모듈의 index 정보를 loading할수 없습니다.");	
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg3","CYCLERCHANNEL"));	//&&
		return FALSE;
	}
	
	//strTemp.Format("PC 결과데이터 손실 검색 시작. ");
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg4","CYCLERCHANNEL"));//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	long lDataCount = 0;
	fltPoint *pData = NULL;
	ULONG nSequenceNo = 1;	
	long nTotCount = m_RestoreFileIndexTable.GetLastCount();	//ljb 20180223 마지막  시퀀스 번호
	long nDataCount = 0;
	
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	if(pProgressWnd)		pProgressWnd->SetPos(0, TRUE);
	
	//strTemp.Format("File size 전체 사이즈 %d .",nTotCount);
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg5","CYCLERCHANNEL"),nTotCount);//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

	//cyc file size 비교 (m_strResultFileName)
	//파일명 생성 
	CString strResultFile;
	strResultFile.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_RAW_FILE_NAME_EXT);

	if (Fun_CheckLossData(strResultFile, nTotCount) == FALSE)
	{
		lLostDataCount++;	//data down이 필요함.
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if(pProgressWnd)		pProgressWnd->SetPos(100, TRUE);

	
	LockFileUpdate(bLockFile);
	
	if(lLostDataCount > 0)		m_bDataLoss = TRUE;
	else						m_bDataLoss = FALSE;	
			
	//strTemp.Format("손식구간 검색 완료.");
	strTemp.Format(Fun_FindMsg("CyclerChannel_SearchLostDataRange_msg6","CYCLERCHANNEL"));//&&
	sprintf(szBuff, strTemp);
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

	return TRUE;
}


CString CCyclerChannel::GetResultFileName()
{
	return m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;
}

//strRawFileName : 복구된 raw data file
//strNewFileName : 새롭게 생성할 Table Index File
//strStepEndFile : 모듈에서 Download한 복구용 Step End file
//strOrgTableFile : 원본 Table Index file
BOOL CCyclerChannel::RemakePCIndexFile(CString strBackUpRawFile, CString strStepEndFile, CString strNewTableFile, CString strOrgTableFile)
{
	//ljb7 수정해야 함
	//파일명 설정 여부 확인 
	if(strBackUpRawFile.IsEmpty() || strNewTableFile.IsEmpty() || strStepEndFile.IsEmpty())
	{
		return FALSE;
	}

	CString		strTemp;
	COleDateTime t = COleDateTime::GetCurrentTime();

	CStdioFile resultFile;
	CFile		fRS;
	CFileStatus fs;
	CFileException e;

	//////////////////////////////////////////////////////////////////////////
	//복구된 Raw Data와 모듈에서 download한 복구용 Table file을 이용하여 PC용 Table 파일을 생성한다.
	//파일을 Open 한다.
	if( !fRS.Open( strBackUpRawFile, CFile::modeRead, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   //m_strLastErrorString.Format("%s 파일을 열수 없습니다.", strBackUpRawFile);
	   m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakePCIndexFile_msg1","CYCLERCHANNEL"), strBackUpRawFile);//&&
	   return FALSE;
	}

	CString strStartTime;
	int nColumnSize = 0;
	PS_RAW_FILE_HEADER *pFileHeader = new PS_RAW_FILE_HEADER;
	if(fRS.Read(pFileHeader, sizeof(PS_RAW_FILE_HEADER)) > 0)
	{
		strStartTime = pFileHeader->fileHeader.szCreateDateTime;		//ljb 20170418 add
		nColumnSize = pFileHeader->rsHeader.nColumnCount;
	}
	delete pFileHeader;
	
	//파일이 잘못되었을 경우 
	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
	   //m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", strBackUpRawFile, nColumnSize);
	   m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakePCIndexFile_msg2","CYCLERCHANNEL"), strBackUpRawFile, nColumnSize);//&&
		fRS.Close();
		return FALSE;	//실제 Data 분석 
	}

	//모듈에서 전송받은 Step End 파일 Open
	CStdioFile stepEndFile;
	CFileException ex;
	CString strReadData;
	//FILE *fp = fopen(strStepEndFile, "rt");
 	//if(fp == NULL)
	if(!stepEndFile.Open(strStepEndFile,CFile::modeRead,&ex))
	{
		//m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakePCIndexFile_msg3","CYCLERCHANNEL"), strBackUpRawFile);//&&
		return FALSE;	//실제 Data 분석 
	}
	BOOL bIsNotEOL;				// 라인이 존재하는지 확인.
	bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다.

	//char temp[23][20];
	//for(int i = 0 ; i < 23; i++)
	//{
	//	fscanf(fp, "%s", temp[i]);
	//}
	
	float *pfBuff = new float[nColumnSize];
	size_t rsSize = sizeof(float)*nColumnSize;
	long nStartIndex = 0, nEndIndex = 0;
	//int nTemp;
	PS_STEP_END_RECORD chTempData, *pChTempData;
	PS_STEP_END_RECORD chLastData;
	ZeroMemory(&chLastData, sizeof(PS_STEP_END_RECORD));

	CPtrArray apTableDataList;
	
	int lReadData[PS_RESTORE_MAX_ITEM_NUM] = { 0 };
	int nSelect;
	//int nData1;
	//////////////////////////////////////////////////////////////////////////
	//resultIndex, state, type, mode, select, code, grade, stepNo, Vsens, Isens, 
	//capacity, watt, wattHour, runTime,totalRunTime, z, temp, press, 
	//totalCycle, currentCycle, avgV, avgI
	while(TRUE)
	{
		strReadData.Empty();
		ZeroMemory(lReadData,sizeof(lReadData));
		bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 문자열씩 읽는다.
		if(!bIsNotEOL)	break;

		//while(fscanf(fp, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d, %d", 
		//				&lReadData[PS_DATA_SEQ], &lReadData[PS_TOT_TIME],&lReadData[PS_STEP_TIME],&lReadData[PS_STATE], 
		//				&lReadData[PS_STEP_TYPE], &nTemp, &nSelect,	&lReadData[PS_CODE], &lReadData[PS_GRADE_CODE], 
		//				&lReadData[PS_STEP_NO], &lReadData[PS_VOLTAGE], &lReadData[PS_CURRENT],	&lReadData[PS_CAPACITY], 
		//				&lReadData[PS_WATT], &lReadData[PS_WATT_HOUR], &lReadData[PS_IMPEDANCE], &lReadData[PS_TEMPERATURE], 
		//				&lReadData[PS_AUX_VOLTAGE], &nData1, &lReadData[PS_TOT_CYCLE], &lReadData[PS_CUR_CYCLE],
		//				&lReadData[PS_AVG_VOLTAGE], &lReadData[PS_AVG_CURRENT] ) > 0 ) 
	
		if (Fun_ConverToString(strReadData, lReadData, nSelect))
		{
			while(fRS.Read(pfBuff, rsSize) > 0)		//data sequence에 해당하는 data가 저장된 순서 index를 구한다.
			{	
				if((ULONG)pfBuff[0] < lReadData[PS_DATA_SEQ])
				{
					nEndIndex++;
				}
				else //if((ULONG)pfBuff[0] >= nSeqNo)
				{	
					if((ULONG)pfBuff[0] > lReadData[PS_DATA_SEQ])	//정확히 일치하는 Data가 없을 경우는 손실된 상태 이전까지가 step data
					{
						nEndIndex--;
					}

					pChTempData = new PS_STEP_END_RECORD;
					ZeroMemory(pChTempData, sizeof(PS_STEP_END_RECORD));
					pChTempData->chNo 			=	m_nChannelIndex+1;				//Channel No
					pChTempData->chStepNo		=	lReadData[PS_STEP_NO];			//StepNo
					pChTempData->chState		=	lReadData[PS_STATE];			//State
					pChTempData->chStepType		=	lReadData[PS_STEP_TYPE];		//Type
					pChTempData->chDataSelect	=	nSelect;
					pChTempData->chCode			= 	lReadData[PS_CODE];				//Code
					pChTempData->chGradeCode	=	lReadData[PS_GRADE_CODE];		//Grade
					pChTempData->chCommState	=	lReadData[PS_COMM_STATE];		//Comm State 20100914
					pChTempData->nIndexFrom		=	nStartIndex;					//IndexFrom	
					pChTempData->nIndexTo		=	nEndIndex;						//IndexTo
					pChTempData->nCurrentCycleNum = lReadData[PS_CUR_CYCLE];		//CurCycle
					pChTempData->nTotalCycleNum	=	lReadData[PS_TOT_CYCLE];		//TotalCycle
					pChTempData->lSaveSequence	=	lReadData[PS_DATA_SEQ];			//

					pChTempData->nAccCycleGroupNum1	=	lReadData[PS_ACC_CYCLE1];			//
					pChTempData->nAccCycleGroupNum2	=	lReadData[PS_ACC_CYCLE2];			//
					pChTempData->nAccCycleGroupNum3	=	lReadData[PS_ACC_CYCLE3];			//
					pChTempData->nAccCycleGroupNum4	=	lReadData[PS_ACC_CYCLE4];			//
					pChTempData->nAccCycleGroupNum5	=	lReadData[PS_ACC_CYCLE5];			//
					pChTempData->nMultiCycleGroupNum1	=	lReadData[PS_MULTI_CYCLE1];			//
					pChTempData->nMultiCycleGroupNum2	=	lReadData[PS_MULTI_CYCLE2];			//
					pChTempData->nMultiCycleGroupNum3	=	lReadData[PS_MULTI_CYCLE3];			//
					pChTempData->nMultiCycleGroupNum4	=	lReadData[PS_MULTI_CYCLE4];			//
					pChTempData->nMultiCycleGroupNum5	=	lReadData[PS_MULTI_CYCLE5];			//
					pChTempData->lSyncDate		= 	lReadData[PS_SYNC_DATE];		//ljb 20140116 add

					pChTempData->fVoltageInput	=	lReadData[PS_VOLTAGE_INPUT];		//ljb2011
					pChTempData->fVoltagePower	=	lReadData[PS_VOLTAGE_POWER];		//ljb2011
					pChTempData->fVoltageBus	=	lReadData[PS_VOLTAGE_BUS];			//ljb2011
					pChTempData->chOutputState	=	lReadData[PS_CAN_OUTPUT_STATE];		//ljb2011
					pChTempData->chInputState	=	lReadData[PS_CAN_INPUT_STATE];		//ljb2011

					pChTempData->fVoltage		= 	ConvertSbcToPCUnit(lReadData[PS_VOLTAGE], PS_VOLTAGE);			//Voltage
					pChTempData->fCurrent		= 	ConvertSbcToPCUnit(lReadData[PS_CURRENT], PS_CURRENT);			//Current
					pChTempData->fCapacity		= 	ConvertSbcToPCUnit(lReadData[PS_CAPACITY], PS_CAPACITY);		//Capacity
					pChTempData->fWatt			=	ConvertSbcToPCUnit(lReadData[PS_WATT], PS_WATT);		
					pChTempData->fWattHour		= 	ConvertSbcToPCUnit(lReadData[PS_WATT_HOUR], PS_WATT_HOUR);		//WattHour
					pChTempData->fStepTime_Day	= 	ConvertSbcToPCUnit(lReadData[PS_STEP_TIME_DAY], PS_SYNC_DATE);		//Time		//ljb 20150122 add
					pChTempData->fStepTime		= 	ConvertSbcToPCUnit(lReadData[PS_STEP_TIME], PS_STEP_TIME);		//Time
					pChTempData->fTotalTime_Day	= 	ConvertSbcToPCUnit(lReadData[PS_TOT_TIME_DAY], PS_SYNC_DATE);		//Time		//ljb 20150122 add
					pChTempData->fTotalTime		= 	ConvertSbcToPCUnit(lReadData[PS_TOT_TIME], PS_TOT_TIME);		//TotTime
					pChTempData->fImpedance		=	ConvertSbcToPCUnit(lReadData[PS_IMPEDANCE], PS_IMPEDANCE);		//IR
					pChTempData->fOvenTemperature	= 	0.0f;	//ConvertSbcToPCUnit(lReadData[PS_TEMPERATURE], PS_TEMPERATURE);	//Chamber Temp(OVEN)
					pChTempData->fOvenHumidity		= 	0.0f;	//ConvertSbcToPCUnit(lReadData[PS_TEMPERATURE], PS_TEMPERATURE);	//AuxVoltage -> Chamber Humidity
					
					pChTempData->fChillerRefTemp	= 	0.0f;	//ConvertSbcToPCUnit(lReadData[PS_TEMPERATURE], PS_TEMPERATURE);	//Chamber Temp(OVEN)
					pChTempData->fChillerCurTemp	= 	0.0f;	//ConvertSbcToPCUnit(lReadData[PS_TEMPERATURE], PS_TEMPERATURE);	//Chamber Temp(OVEN)
// 					pChTempData->iChillerRefPump	= 	0;	    //ConvertSbcToPCUnit(lReadData[PS_TEMPERATURE], PS_TEMPERATURE);	//Chamber Temp(OVEN)
// 					pChTempData->iChillerCurPump	= 	0;	    //ConvertSbcToPCUnit(lReadData[PS_TEMPERATURE], PS_TEMPERATURE);	//Chamber Temp(OVEN)
					pChTempData->fChillerRefPump	= 	0.0f; //lyj 20201102 DES칠러 소숫점 
					pChTempData->fChillerCurPump	= 	0.0f; //lyj 20201102 DES칠러 소숫점 
					pChTempData->dCellBALChData     =   0.0f;
					pChTempData->fPwrSetVoltage     =   0.0f; //yulee 20190514 cny
					pChTempData->fPwrCurVoltage     =   0.0f;
					
					pChTempData->fAvgVoltage	= 	ConvertSbcToPCUnit(lReadData[PS_AVG_VOLTAGE], PS_AVG_VOLTAGE);	//AvgVoltage
					pChTempData->fAvgCurrent	= 	ConvertSbcToPCUnit(lReadData[PS_AVG_CURRENT], PS_AVG_CURRENT);	//AvgVoltage

					pChTempData->fChargeAh		= 	ConvertSbcToPCUnit(lReadData[PS_CHARGE_CAP], PS_CHARGE_CAP);		//Capacity//lmh 20120306
					pChTempData->fDisChargeAh	= 	ConvertSbcToPCUnit(lReadData[PS_DISCHARGE_CAP], PS_DISCHARGE_CAP);		//Capacity//lmh 20120306
					pChTempData->fCapacitance	= 	ConvertSbcToPCUnit(lReadData[PS_CAPACITANCE], PS_CAPACITY);		//Capacity
					pChTempData->fChargeWh		= 	ConvertSbcToPCUnit(lReadData[PS_CHARGE_WH], PS_WATT_HOUR);		//Capacity
					pChTempData->fDisChargeWh	= 	ConvertSbcToPCUnit(lReadData[PS_DISCHARGE_WH], PS_WATT_HOUR);		//Capacity
					pChTempData->fCVTime_Day	= 	ConvertSbcToPCUnit(lReadData[PS_CV_TIME_DAY], PS_SYNC_DATE);		//Time		//ljb 20150122 add
					pChTempData->fCVTime		= 	ConvertSbcToPCUnit(lReadData[PS_CV_TIME], PS_STEP_TIME);		//Capacity
					//pChTempData->fSyncDate		= 	ConvertSbcToPCUnit(lReadData[PS_SYNC_DATE], PS_SYNC_DATE);		//Data
					pChTempData->fSyncTime		= 	ConvertSbcToPCUnit(lReadData[PS_SYNC_TIME], PS_SYNC_TIME);		//Time
					apTableDataList.Add(pChTempData);

					nEndIndex++;
					nStartIndex = nEndIndex;
					TRACE("StartIndex : %d\n",nStartIndex);
					break;
				}
			}//while(fRS.Read(pfBuff, rsSize) > 0)	 END
		}
	}	
	if (pfBuff != NULL)							delete[]	pfBuff;		
	if (fRS.m_hFile != CFile::hFileNull)		fRS.Close();

	//if (fp)										fclose(fp);
	stepEndFile.Close();
	//////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////
	// PC용 Table File을 새로 생성한다.
	if( !fRS.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	{
		//생성 실패
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
			//m_strLastErrorString.Format("%s를 생성할 수 없습니다.", strNewTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakePCIndexFile_msg4","CYCLERCHANNEL"), strNewTableFile);//&&
		
			//생성 실패시 Delete all step data
			int nSize = apTableDataList.GetSize();
			for(int a=0; a<nSize; a++)
			{
				pChTempData = (PS_STEP_END_RECORD *)apTableDataList.GetAt(0);
				if(pChTempData)
				{
					delete pChTempData;
					pChTempData = NULL;
				}
				apTableDataList.RemoveAt(0);
			}
			return FALSE;
	}

	//Make file header
	//strOrgTableFile 에서 file header를 가져올지 
	//아니면 새롭게 생성할지 
	PS_TEST_RESULT_FILE_HEADER *pHeader = new PS_TEST_RESULT_FILE_HEADER;
	ZeroMemory(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
	pHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
	pHeader->fileHeader.nFileVersion = PS_FILE_VERSION;
	sprintf(pHeader->fileHeader.szCreateDateTime, "%s", t.Format());
	sprintf(pHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
	//sprintf(pHeader->testHeader.szStartTime, "%s", m_startTime.Format());
	sprintf(pHeader->testHeader.szStartTime, "%s", strStartTime);	//ljb 20170418 add 복구시 날자가 이상하게 수정 되는 현상 수정
	sprintf(pHeader->testHeader.szEndTime, "%s", t.Format());
	sprintf(pHeader->testHeader.szSerial, "%s", m_strTestSerial);
	sprintf(pHeader->testHeader.szUserID, "%s", m_strUserID);
	sprintf(pHeader->testHeader.szDescript, "%s", m_strDescript);	//기존에 최종 정보가 이미 로딩되어 있다.
	sprintf(pHeader->testHeader.szTray_CellBcr, "%s,%s", m_strTrayNo, m_strBcrNo);	//ljb 20160404 add
	// 	pHeader->testHeader.nRecordSize = 30;		//ljb 201007	21 -> 30
	// 	pHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
	// 	pHeader->testHeader.wRecordItem[1] = PS_CURRENT;
	// 	pHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
	// 	pHeader->testHeader.wRecordItem[3] = PS_WATT;
	// 	pHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
	// 	pHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
	// 	pHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
	// 	pHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
	// 	pHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;	//ljb 20100726
	// 	pHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;		//ljb 20100726
	// 	pHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
	// 	pHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
	// 	pHeader->testHeader.wRecordItem[12] = PS_CHARGE_CAP;
	// 	pHeader->testHeader.wRecordItem[13] = PS_DISCHARGE_CAP;
	// 	pHeader->testHeader.wRecordItem[14] = PS_CAPACITANCE;
	// 	pHeader->testHeader.wRecordItem[15] = PS_CHARGE_WH;
	// 	pHeader->testHeader.wRecordItem[16] = PS_DISCHARGE_WH;
	// 	pHeader->testHeader.wRecordItem[17] = PS_CV_TIME;
	// 	pHeader->testHeader.wRecordItem[18] = PS_SYNC_DATE;
	// 	pHeader->testHeader.wRecordItem[19] = PS_SYNC_TIME;
	// 	pHeader->testHeader.wRecordItem[20] = PS_ACC_CYCLE1;
	// 	pHeader->testHeader.wRecordItem[21] = PS_ACC_CYCLE2;
	// 	pHeader->testHeader.wRecordItem[22] = PS_ACC_CYCLE3;
	// 	pHeader->testHeader.wRecordItem[23] = PS_ACC_CYCLE4;
	// 	pHeader->testHeader.wRecordItem[24] = PS_ACC_CYCLE5;
	// 	pHeader->testHeader.wRecordItem[25] = PS_MULTI_CYCLE1;
	// 	pHeader->testHeader.wRecordItem[26] = PS_MULTI_CYCLE2;
	// 	pHeader->testHeader.wRecordItem[27] = PS_MULTI_CYCLE3;
	// 	pHeader->testHeader.wRecordItem[28] = PS_MULTI_CYCLE4;
	// 	pHeader->testHeader.wRecordItem[29] = PS_MULTI_CYCLE5;

	//ksj 20200127 : 주석처리.
	/*
	pHeader->testHeader.nRecordSize = 43;			//ljb 201007	21 -> 30 -> 201012 (34) =>41 => 43 //yulee 20190514 cny
	pHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
	pHeader->testHeader.wRecordItem[1] = PS_CURRENT;
	pHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
	pHeader->testHeader.wRecordItem[3] = PS_WATT;
	pHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
	pHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
	pHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
	pHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
	pHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
	pHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
	pHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
	pHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
	pHeader->testHeader.wRecordItem[12] = PS_CHARGE_CAP;
	pHeader->testHeader.wRecordItem[13] = PS_DISCHARGE_CAP;
	pHeader->testHeader.wRecordItem[14] = PS_CAPACITANCE;
	pHeader->testHeader.wRecordItem[15] = PS_CHARGE_WH;
	pHeader->testHeader.wRecordItem[16] = PS_DISCHARGE_WH;
	pHeader->testHeader.wRecordItem[17] = PS_CV_TIME;
	pHeader->testHeader.wRecordItem[18] = PS_SYNC_DATE;
	pHeader->testHeader.wRecordItem[19] = PS_SYNC_TIME;
	pHeader->testHeader.wRecordItem[20] = PS_ACC_CYCLE1;
	pHeader->testHeader.wRecordItem[21] = PS_ACC_CYCLE2;
	pHeader->testHeader.wRecordItem[22] = PS_ACC_CYCLE3;
	pHeader->testHeader.wRecordItem[23] = PS_ACC_CYCLE4;
	pHeader->testHeader.wRecordItem[24] = PS_ACC_CYCLE5;
	pHeader->testHeader.wRecordItem[25] = PS_MULTI_CYCLE1;
	pHeader->testHeader.wRecordItem[26] = PS_MULTI_CYCLE2;
	pHeader->testHeader.wRecordItem[27] = PS_MULTI_CYCLE3;
	pHeader->testHeader.wRecordItem[28] = PS_MULTI_CYCLE4;
	pHeader->testHeader.wRecordItem[29] = PS_MULTI_CYCLE5;
	pHeader->testHeader.wRecordItem[30] = PS_COMM_STATE;		//ljb 201009
	pHeader->testHeader.wRecordItem[31] = PS_VOLTAGE_INPUT;		//ljb 201012
	pHeader->testHeader.wRecordItem[32] = PS_VOLTAGE_POWER;		//ljb 201012
	pHeader->testHeader.wRecordItem[33] = PS_VOLTAGE_BUS;		//ljb 201012
	pHeader->testHeader.wRecordItem[34] = PS_CAN_OUTPUT_STATE;	//ljb 20101222
	pHeader->testHeader.wRecordItem[35] = PS_CAN_INPUT_STATE;	//ljb 20101222
	pHeader->testHeader.wRecordItem[36] = PS_CHILLER_REF_TEMP;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[37] = PS_CHILLER_CUR_TEMP;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[38] = PS_CHILLER_REF_PUMP;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[39] = PS_CHILLER_CUR_PUMP;	//ljb 20170912 add
#if 0 //ksj 20200127 : wRecordItem size는 [40]이므로 아래 코드는 오버 플로우 발생시킴. 일단 비활성 후 향후 개선 필요.
	pHeader->testHeader.wRecordItem[40] = PS_CELLBAL_CH_DATA;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[41] = PS_PWRSUPPLY_SETVOLT;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[42] = PS_PWRSUPPLY_CURVOLT;	//ljb 20170912 add
#endif
	*/
	//ksj 20200127 : 복구 아이템 항목 수정. wRecordItem은 [40] 이라 상기 코드의 40~42는 오버플로우임.
	//WriteResultListFile_V100B 하고 항목 일치 시킴
	pHeader->testHeader.nRecordSize = 35;			//ljb 201007	21 -> 30 -> 201012 (34) -> 20131208 39=>33=>35
	pHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
	pHeader->testHeader.wRecordItem[1] = PS_CURRENT;
	pHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
	pHeader->testHeader.wRecordItem[3] = PS_WATT;
	pHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
	pHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
	pHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
	pHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
	pHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
	pHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
	pHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
	pHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
	pHeader->testHeader.wRecordItem[12] = PS_CHARGE_CAP;
	pHeader->testHeader.wRecordItem[13] = PS_DISCHARGE_CAP;
	pHeader->testHeader.wRecordItem[14] = PS_CAPACITANCE;
	pHeader->testHeader.wRecordItem[15] = PS_CHARGE_WH;
	pHeader->testHeader.wRecordItem[16] = PS_DISCHARGE_WH;
	pHeader->testHeader.wRecordItem[17] = PS_CV_TIME;			//ljb 2011418 이재복 ////////// 기본으로 save
	pHeader->testHeader.wRecordItem[18] = PS_SYNC_DATE;			//ljb 2011418 이재복 ////////// 기본으로 save
	pHeader->testHeader.wRecordItem[19] = PS_SYNC_TIME;			//ljb 2011418 이재복 ////////// 기본으로 save
	pHeader->testHeader.wRecordItem[20] = PS_VOLTAGE_INPUT;		//ljb 201012
	pHeader->testHeader.wRecordItem[21] = PS_VOLTAGE_POWER;		//ljb 201012
	pHeader->testHeader.wRecordItem[22] = PS_VOLTAGE_BUS;		//ljb 201012
	pHeader->testHeader.wRecordItem[23] = PS_STEP_TIME_DAY;		//ljb 20131208 add
	pHeader->testHeader.wRecordItem[24] = PS_TOT_TIME_DAY;		//ljb 20131208 add
	pHeader->testHeader.wRecordItem[25] = PS_CV_TIME_DAY;		//ljb 20131208 add
	pHeader->testHeader.wRecordItem[26] = PS_LOAD_VOLT;			//ljb 20150430 add
	pHeader->testHeader.wRecordItem[27] = PS_LOAD_CURR;			//ljb 20150430 add
	pHeader->testHeader.wRecordItem[28] = PS_CHILLER_REF_TEMP;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[29] = PS_CHILLER_CUR_TEMP;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[30] = PS_CHILLER_REF_PUMP;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[31] = PS_CHILLER_CUR_PUMP;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[32] = PS_CELLBAL_CH_DATA;	//ljb 20170912 add
	pHeader->testHeader.wRecordItem[33] = PS_PWRSUPPLY_SETVOLT;	//ljb 20170912 add//yulee 20190514 cny
	pHeader->testHeader.wRecordItem[34] = PS_PWRSUPPLY_CURVOLT;	//ljb 20170912 add
	//ksj end

	//Write file header
	fRS.Write(pHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
	delete pHeader;

	//Save new data
	int nSize = apTableDataList.GetSize();
	for(int a=0; a<nSize; a++)
	{
		pChTempData = (PS_STEP_END_RECORD *)apTableDataList.GetAt(0);
		if(pChTempData)
		{
			fRS.Write(pChTempData, sizeof(PS_STEP_END_RECORD));
			memcpy(&chLastData, pChTempData, sizeof(PS_STEP_END_RECORD));
			delete pChTempData;
			pChTempData = NULL;
		}
		apTableDataList.RemoveAt(0);
	}
	fRS.Flush();
	fRS.Close();	
	//////////////////////////////////////////////////////////////////////////
	
	//rp$ 파일 수정 
	//가장 마지막 step 완료의 EndIndex와 TempData의 Start Index를 비교
	//같은 cycle, 같은 step번호가 아니며(1개 step 전체 가 없을 경우 발생함 ) temp의 start index 수정 
	CString 	strCurTempFile;
	strCurTempFile.Format("%s\\%s.rp$",  m_strFilePath, m_strTestName);
	if(chLastData.chStepType == PS_STEP_END)
	{
		unlink(strCurTempFile);
	}
	else
	{
		if(chLastData.chStepNo > 0 )
		{

			CStringList aTempDataList;	
			if( CFile::GetStatus(strCurTempFile, fs))		 
			{
				if( !resultFile.Open( strCurTempFile, CFile::modeRead|CFile::shareDenyWrite, &e ) )
				{
					#ifdef _DEBUG
					   afxDump << "File could not be opened " << e.m_cause << "\n";
					#endif
					   return FALSE;
				}

				//TRACE("Read all data from %s\n", strFileName);
				while(resultFile.ReadString(strTemp))
				{
					if(!strTemp.IsEmpty())	aTempDataList.AddTail(strTemp);
				}
				resultFile.Close();
			}

			//////////////////////////////////////////////////////////////////////////
		
			//2. 최종 시험 data를 Parsing 한다.
			if(aTempDataList.GetCount() >= 3)	
			{
				ZeroMemory(&chTempData, sizeof(PS_STEP_END_RECORD));
				//ZeroMemory(&chLastData, sizeof(CT_STEP_END_RECORD));
				//2.1 data parsing
				POSITION pos = aTempDataList.GetTailPosition();
				strTemp = aTempDataList.GetNext(pos);
				
				//CTable dataTable(aTempDataList.GetNext(pos) , strTemp);
				ParsingChDataString(strTemp, chTempData);
				//ParsingChDataString(strTableDataList.GetTail(), chLastData);

				//if( chLastData.chStepNo != chTempData.chStepNo || chLastData.nTotalCycleNum != chTempData.nTotalCycleNum)
				//{
					if(chLastData.nIndexTo != (chTempData.nIndexFrom+1))
					{
						chTempData.nIndexFrom = chLastData.nIndexTo+1;
						//yulee 20190514 cny
//						strTemp.Format(	"%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%d,%d,%f,%.2f,%.2f", 
						strTemp.Format(	"%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%f,%.2f,%.2f",  //lyj 20201102 DES칠러 소숫점 
							chTempData.chStepNo,			//StepNo
							chTempData.nIndexFrom,			//IndexFrom	
							chTempData.nIndexTo,			//IndexTo
							chTempData.nCurrentCycleNum,	//CurCycle
							chTempData.nTotalCycleNum,		//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
							chTempData.chStepType,			//Type
							chTempData.chState,				//State
							chTempData.fStepTime,			//Time
							chTempData.fTotalTime,			//TotTime
							chTempData.chCode,				//Code
							chTempData.chGradeCode,			//Grade
							chTempData.fVoltage, 			//Voltage
							chTempData.fCurrent, 			//Current
							chTempData.fCapacity, 			//Capacity
							chTempData.fWattHour, 			//WattHour
							chTempData.fImpedance,			//IR
							chTempData.fOvenTemperature, 	//Chamber Temp(OVEN)
							chTempData.fOvenHumidity, 		//Chamber Humidity(OVEN)
							chTempData.fAvgVoltage,			//AvgVoltage
							chTempData.fAvgCurrent,			//AvgVoltage
							chTempData.lSaveSequence,
							chTempData.fChargeAh,									
							chTempData.fDisChargeAh,
							chTempData.fCapacitance,
							chTempData.fChargeWh,
							chTempData.fDisChargeWh,
							chTempData.fCVTime,
							chTempData.lSyncDate,
							chTempData.fSyncTime,
							chTempData.nAccCycleGroupNum1,
							chTempData.nAccCycleGroupNum2,
							chTempData.nAccCycleGroupNum3,
							chTempData.nAccCycleGroupNum4,
							chTempData.nAccCycleGroupNum5,
							chTempData.nMultiCycleGroupNum1,
							chTempData.nMultiCycleGroupNum2,
							chTempData.nMultiCycleGroupNum3,
							chTempData.nMultiCycleGroupNum4,
							chTempData.nMultiCycleGroupNum5,
							chTempData.fChillerRefTemp,
							chTempData.fChillerCurTemp,
// 							chTempData.iChillerRefPump,
// 							chTempData.iChillerCurPump,
							chTempData.fChillerRefPump, //lyj 20201102 DES칠러 소숫점 
							chTempData.fChillerCurPump, //lyj 20201102 DES칠러 소숫점 
							chTempData.dCellBALChData,
							chTempData.fPwrSetVoltage,
							chTempData.fPwrCurVoltage
						);
						
						FILE *fp = fopen(strCurTempFile, "wt");
						if(fp)
						{	
							pos = aTempDataList.GetHeadPosition();
							CString str1, str2;
							str1 = aTempDataList.GetNext(pos);
							str2 = aTempDataList.GetNext(pos);

							fprintf(fp, "%s\n%s\n%s", str1, str2, strTemp);

							fclose(fp);
						}
					}
				//}
			}	
		}
	}

	return TRUE;

}

BOOL CCyclerChannel::LockFileUpdate(BOOL bLock)
{
	BOOL bRtn = m_bLockFileUpdate; 
	m_bLockFileUpdate = bLock;	
	return bRtn;
}


//모든 결과는 m단위로 저장한다.
float CCyclerChannel::ConvertSbcToPCUnit(long lData, long nItem)
{
	CString strTemp;
	float fData = 0.0f;
	switch(nItem)
	{
	case		PS_STATE		:	fData = float(lData);			break;
	case		PS_VOLTAGE		:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위	
	case		PS_CURRENT		:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
#ifdef _EDLC_TEST_SYSTEM
	case		PS_CAPACITANCE	://lmh 20120229 add
	case		PS_CAPACITY		:	fData = float(lData);			break;		//m단위		//u단위(전류종속 단위)
#else
	case		PS_CAPACITANCE	://lmh 20120229 add
	case		PS_CAPACITY		:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
#endif
	case		PS_IMPEDANCE	:	fData = LONG2FLOAT(lData);		break;		//u단위		
	case		PS_CODE			:	fData = float(lData);			break;
	case		PS_STEP_TIME	:	fData = PCTIME(lData);			break;		//10msec
	case		PS_TOT_TIME		:	fData = PCTIME(lData);			break;		//10msec
	case		PS_GRADE_CODE	:	fData = float(lData);			break;
	case		PS_STEP_NO		:	fData = float(lData);			break;
	case		PS_WATT			:	fData = float(lData);			break;		//mWatt		//u단위(전류종속 단위)
	case		PS_WATT_HOUR	:	fData = float(lData);			break;		//mWh		//u단위(전류종속 단위)
	case		PS_AUX_TEMPERATURE	:	fData = float(lData);			break;		//m℃
	case		PS_AUX_VOLTAGE	:	fData = LONG2FLOAT(lData);		break;		//m~
	case		PS_OVEN_TEMPERATURE	:	fData = float(lData);			break;		//m℃
	case		PS_OVEN_HUMIDITY	:	fData = float(lData);			break;		//m%
	case		PS_STEP_TYPE	:	fData = float(lData);			break;
	case		PS_CUR_CYCLE	:	fData = float(lData);			break;
	case		PS_TOT_CYCLE	:	fData = float(lData);			break;
	case		PS_TEST_NAME	:	fData = float(lData);			break;
	case		PS_SCHEDULE_NAME:	fData = float(lData);			break;
	case		PS_CHANNEL_NO	:	fData = float(lData);			break;
	case		PS_MODULE_NO	:	fData = float(lData);			break;
	case		PS_LOT_NO		:	fData = float(lData);			break;
	case		PS_DATA_SEQ		:	fData = float(lData);			break;
	case		PS_AVG_CURRENT	:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
	case		PS_AVG_VOLTAGE	:	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위(전류종속 단위)
	case		PS_SYNC_DATE	:
	case		PS_SYNC_TIME	:
	{
			strTemp.Format("%d",lData);
			fData = atof(strTemp);
			break;
	}
	case		PS_CHILLER_REF_TEMP	:	fData = float(lData);	break;		//m℃
	case		PS_CHILLER_CUR_TEMP	:	fData = float(lData);	break;		//m℃
	case		PS_CHILLER_REF_PUMP	:	fData = float(lData);	break;		//m℃
	case		PS_CHILLER_CUR_PUMP	:	fData = float(lData);	break;		//m℃
	case		PS_CELLBAL_CH_DATA	:   fData = float(lData);   break;
	case		PS_PWRSUPPLY_SETVOLT :  fData = float(lData);   break;//yulee 20190514 cny
	case		PS_PWRSUPPLY_CURVOLT :  fData = float(lData);   break;
	
	//case		PS_SYNC_TIME	    :	fData = float(lData);			break;		//ljb 20130604 add
	case		PS_VOLTAGE_INPUT    :	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위	
	case		PS_VOLTAGE_POWER    :	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위	
	case		PS_VOLTAGE_BUS	    :	fData = LONG2FLOAT(lData);		break;		//u단위		//n단위	
	case		PS_COMM_STATE	    :	fData = float(lData);			break;
	case		PS_CAN_OUTPUT_STATE	:	fData = float(lData);			break;
	case		PS_CAN_INPUT_STATE	:	fData = float(lData);			break;
	default:	fData = float(lData);								break;
	}
	return fData;
}

//모든 결과는 m단위로 저장한다.
long CCyclerChannel::ConvertPCUnitToSbc(float fData, long nItem)
{
	long lData = 0;
	switch(nItem)
	{
	case		PS_STATE		:	lData = long(fData);			break;
	case		PS_VOLTAGE		:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위	
	case		PS_CURRENT		:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
#ifdef _EDLC_TEST_SYSTEM
	case		PS_CAPACITY		:	lData = float(fData);			break;		//m단위		//u단위(전류종속 단위)
#else
	case		PS_CAPACITY		:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
#endif
	case		PS_IMPEDANCE	:	lData = FLOAT2LONG(fData);		break;		//u단위		
	case		PS_CODE			:	lData = long(fData);			break;
	case		PS_STEP_TIME	:	lData = MDTIME(fData);			break;		//10msec
	case		PS_TOT_TIME		:	lData = MDTIME(fData);			break;		//10msec
	case		PS_GRADE_CODE	:	lData = long(fData);			break;
	case		PS_STEP_NO		:	lData = long(fData);			break;
	case		PS_WATT			:	lData = long(fData);			break;		//mWatt		//u단위(전류종속 단위)
	case		PS_WATT_HOUR	:	lData = long(fData);			break;		//mWh		//u단위(전류종속 단위)
	case		PS_AUX_TEMPERATURE	:	lData = FLOAT2LONG(fData);		break;		//m℃
	case		PS_AUX_VOLTAGE		:	lData = FLOAT2LONG(fData);		break;		//m~
	case		PS_STEP_TYPE	:	lData = long(fData);			break;
	case		PS_CUR_CYCLE	:	lData = long(fData);			break;
	case		PS_TOT_CYCLE	:	lData = long(fData);			break;
	case		PS_TEST_NAME	:	lData = long(fData);			break;
	case		PS_SCHEDULE_NAME:	lData = long(fData);			break;
	case		PS_CHANNEL_NO	:	lData = long(fData);			break;
	case		PS_MODULE_NO	:	lData = long(fData);			break;
	case		PS_LOT_NO		:	lData = long(fData);			break;
	case		PS_DATA_SEQ		:	lData = long(fData);			break;
	case		PS_AVG_CURRENT	:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
	case		PS_AVG_VOLTAGE	:	lData = FLOAT2LONG(fData);		break;		//u단위		//n단위(전류종속 단위)
	case		PS_COMM_STATE	:	lData = long(fData);			break;
	case		PS_CAN_OUTPUT_STATE	:	lData = long(fData);			break;
	case		PS_CAN_INPUT_STATE	:	lData = long(fData);			break;
	default:	lData = long(fData);								break;
	}
	return lData;
}

CString CCyclerChannel::GetLastErrorString()
{
	return	m_strLastErrorString;
}


//SBC에서 전달되어온 mili second data를 저장한다.
//ljb 2011524 메모리 복사해서 File 복사하는 것으로 수정함
BOOL CCyclerChannel::SaveStepMiliSecData()
{
	CChannel *pCh = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pCh == NULL)		return FALSE;

	SFT_MSEC_CH_DATA_INFO *pMiliSecInfo = new SFT_MSEC_CH_DATA_INFO;
	ZeroMemory(pMiliSecInfo,sizeof(SFT_MSEC_CH_DATA_INFO));
	memcpy(pMiliSecInfo,&pCh->m_StepStartDataInfo,sizeof(SFT_MSEC_CH_DATA_INFO));
	memset(&pCh->m_StepStartDataInfo,0,sizeof(SFT_MSEC_CH_DATA_INFO));				//cycler channel 초기화
	//pMiliSecInfo =  &pCh->m_StepStartDataInfo;
	if(pMiliSecInfo->lDataCount < 1 || pMiliSecInfo->lDataCount > MAX_MSEC_DATA_POINT
		|| pMiliSecInfo->lTotCycNo == 0 || pMiliSecInfo->lStepNo == 0 )
	{
		delete pMiliSecInfo;
		return FALSE;
	}

	SFT_MSEC_CH_DATA *pMSecData = new SFT_MSEC_CH_DATA[pMiliSecInfo->lDataCount];
	ZeroMemory(pMSecData,sizeof(SFT_MSEC_CH_DATA)*pMiliSecInfo->lDataCount);
	memcpy(pMSecData,&pCh->m_aStepStartData,sizeof(SFT_MSEC_CH_DATA)*pMiliSecInfo->lDataCount);
	memset(pCh->m_aStepStartData,0,sizeof(SFT_MSEC_CH_DATA)*MAX_MSEC_DATA_POINT);	//cycler channel 초기화
	//pMSecData = (SFT_MSEC_CH_DATA *)pCh->m_aStepStartData;

// 	//과부하시(2000개 이상파일 생성시) Event가 지연되어 같은 data에 2번 발생하는 경우 처리 
// 	//최소한 data 소실은 발생하더라도 중복 data가 저장되는 현상은 방지
// 	if(pMiliSecInfo->lDataCount < 1 || pMiliSecInfo->lDataCount > MAX_MSEC_DATA_POINT
// 		|| pMiliSecInfo->lTotCycNo == 0 || pMiliSecInfo->lStepNo == 0 )
// 	{
// 		//가끔 부하가 걸리면 같은 data에서 2번의 저장 이벤트가 발생 함
// 		//따라서 저장후에 lTotCycNo, lStepNo를 0으로 reset 시키고 이들이 0이면 저장하지 않음 
// 		TRACE("Data write fail!!!!!!!!!!!!!!!!, %d\n", pMiliSecInfo->lDataCount);
// 		return FALSE;
// 	}

	TRACE("!!!!!!!!!!!!!!!!!!!!!=================> START\n");

	CString strTemp, strFile;
	strFile.Format("%s\\StepStart\\%s_C%06d_S%02d.csv", m_strFilePath, m_strTestName, pMiliSecInfo->lTotCycNo, pMiliSecInfo->lStepNo);

	BOOL bFind = FALSE;
	
//	CFileFind ff;
//	bFind = ff.FindFile(strTemp);	// 파일이 많으면 시간이 오래 걸림 

	//<io.h>
	if(_access((LPCTSTR)strFile, 0) != -1)	bFind = TRUE;
	
	//중복 파일일 경우 뒷부분에 이어서 저장 아니면 새로 생성
	CString strLine;
	if(bFind == FALSE)
	{
#ifdef _EDLC_TEST_SYSTEM
		strLine.Format("time(sec),S%d_V(mV),S%d_I(mA),\n", pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo);
#else
		strLine.Format("time(sec),S%d_V(mV),S%d_I(mA),S%d_C(mAh),S%d_WH(mWh)\n", pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo, pMiliSecInfo->lStepNo);
#endif
	}

	for(int i=0; i<pMiliSecInfo->lDataCount; i++)
	{
		if(pMSecData[i].lSec < 0)	//음수로 표기되는 시간은 단위가 1msec 이다.
		{
#ifdef _EDLC_TEST_SYSTEM
			strTemp.Format("%.3f,%.4f,%.4f,\n", -(float)pMSecData[i].lSec/1000.0f,
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT)
												//ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
												);
#else
			strTemp.Format("%.3f,%.4f,%.4f,%.4f,%.4f\n", -(float)pMSecData[i].lSec/1000.0f,
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT),
												ConvertSbcToPCUnit(pMSecData[i].lData3, PS_CAPACITY),
												ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
												);
#endif
		}
		else
		{
#ifdef _EDLC_TEST_SYSTEM
			strTemp.Format("%.2f,%.4f,%.4f,\n",	ConvertSbcToPCUnit(pMSecData[i].lSec, PS_STEP_TIME),
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT)
												//ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
											);
#else
			strTemp.Format("%.2f,%.4f,%.4f,%.4f,%.4f\n",	ConvertSbcToPCUnit(pMSecData[i].lSec, PS_STEP_TIME),
												ConvertSbcToPCUnit(pMSecData[i].lData1, PS_VOLTAGE),
												ConvertSbcToPCUnit(pMSecData[i].lData2, PS_CURRENT),
												ConvertSbcToPCUnit(pMSecData[i].lData3, PS_CAPACITY),
												ConvertSbcToPCUnit(pMSecData[i].lData4, PS_WATT_HOUR)
											);
#endif
		}

		strLine += strTemp;

	}
	
	FILE *fp = fopen(strFile, "at+");
	if(fp)
	{
		fprintf(fp, strLine);
		fclose(fp);
// 		pMiliSecInfo->lDataCount = 0;	//0으로 reset하면 안됨(메모리 할당이 계속 이루어 지므로)
// 		pMiliSecInfo->lTotCycNo = 0;
// 		pMiliSecInfo->lStepNo = 0;
		delete pMiliSecInfo;
		delete [] pMSecData;

//#ifdef _DEBUG
//		if(m_nChannelIndex == 0)
//		{
//			static a = 1;
//			TRACE("3[%03d] ===> Data save end\n", a++);
//			TRACE("Save time %d msec\n",  GetTickCount()-dTime);
//		}
//#endif
		TRACE("!!!!!!!!!!!!!!!!!!!!!=================> END\n");

		return TRUE;
	}

	delete pMiliSecInfo;
	delete [] pMSecData;
	return FALSE;
}

//현재까지의 누적 용량을 Update한다.
float CCyclerChannel::AddTotalCapacity(WORD nType, float fCapacity)
{
	if(nType == PS_STEP_CHARGE)
	{
		m_fCapacitySum += fCapacity;
	}
	else if ( nType == PS_STEP_DISCHARGE || nType == PS_STEP_IMPEDANCE)
	{
		m_fCapacitySum -= fCapacity;
	}

	return m_fCapacitySum;
}

//작업에 대한 Log 기록 
void CCyclerChannel::WriteLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);

	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();

	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile )	aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}

//ksj 20170810 : 스케쥴 Update 작업에 대한 Log 기록 
void CCyclerChannel::WriteSchUpdateLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);
	
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;

	if ( aFileFinder.FindFile(m_strSchUpdateLogFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strSchUpdateLogFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile )	aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}

void CCyclerChannel::WriteChamberLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);
	
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogChamberFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogChamberFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile )	aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}

void CCyclerChannel::WritePwrSupplyLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);
	
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogPwrSupplyFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogPwrSupplyFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile ) aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}


void CCyclerChannel::WriteChillerLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);
	
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogChillerFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogChillerFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile ) aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}

void CCyclerChannel::WritePumpLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);
	
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogPumpFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogPumpFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile ) aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}

void CCyclerChannel::WriteCellBALLog(CString strLogMsg)
{
	CString strMsg;
	CTime tm = CTime::GetCurrentTime();
	strMsg.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogMsg);
	
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(m_strLogCellBALFileName) == FALSE )
	{
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;


	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(m_strLogCellBALFileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile ) aFile.SeekToEnd();
		aFile.WriteString(strMsg + "\n");
		aFile.Close();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
}

BOOL CCyclerChannel::IsStopReserved()
{
	if(SFTGetReservedCmd(m_nModuleID, m_nChannelIndex) == 1)	return TRUE;

	return FALSE;
}

BOOL CCyclerChannel::IsPauseReserved()
{
	if(SFTGetReservedCmd(m_nModuleID, m_nChannelIndex) == 2)	return TRUE;
	
	return FALSE;
}


float CCyclerChannel::GetItemData(PS_STEP_END_RECORD &chData, WORD wItem)
{
	float fData = 0.0f;
	switch(wItem)
	{
		case		PS_STATE		    :	fData = float(chData.chState);	        break;
		case		PS_VOLTAGE		    :	fData = chData.fVoltage;		        break;		//u단위		//n단위	
		case		PS_VOLTAGE_INPUT	:	fData = chData.fVoltageInput;	        break;		//u단위		//n단위	
		case		PS_VOLTAGE_POWER	:	fData = chData.fVoltagePower;	        break;		//u단위		//n단위	
		case		PS_VOLTAGE_BUS		:	fData = chData.fVoltageBus;		        break;		//u단위		//n단위	
		case		PS_CURRENT		    :	fData = chData.fCurrent;		        break;		//u단위		//n단위(전류종속 단위)
		case		PS_CAPACITY		    :	fData = chData.fCapacity;		        break;		//u단위		//n단위(전류종속 단위)
		case		PS_IMPEDANCE	    :	fData = chData.fImpedance;		        break;		//u단위		
		case		PS_CODE			    :
			fData = float(chData.chCode);
			break;
		case		PS_STEP_TIME_DAY	:	fData = chData.fStepTime_Day;		    break;		//ljb 20131205
		case		PS_STEP_TIME	    :	fData = chData.fStepTime;				break;		//10msec
		case		PS_TOT_TIME_DAY		:	fData = chData.fTotalTime_Day;		    break;		//ljb 20131205
		case		PS_TOT_TIME		    :	fData = chData.fTotalTime;				break;		//10msec
		case		PS_GRADE_CODE	    :	fData = float(chData.chGradeCode);		break;
		case		PS_COMM_STATE	    :	fData = float(chData.chCommState);		break;	//ljb comm state
		case		PS_CAN_OUTPUT_STATE	:	fData = float(chData.chOutputState);	break;	//ljb 20101222 Output state
		case		PS_CAN_INPUT_STATE	:	fData = float(chData.chInputState);		break;	//ljb 20101222 Input state
		case		PS_STEP_NO		    :	fData = float(chData.chStepNo);			break;
		case		PS_WATT			    :	fData = chData.fWatt;					break;		//mWatt		//u단위(전류종속 단위)
		case		PS_WATT_HOUR	    :	fData = chData.fWattHour;				break;		//mWh		//u단위(전류종속 단위)
		//case		PS_TEMPERATURE	    :	fData = chData.fTemparature;	        break;		//m℃
		case		PS_OVEN_TEMPERATURE	:	fData = chData.fOvenTemperature;	    break;		//m℃	//20080829 kjh Oven 온도를 저장 //Chamber Temp(OVEN)
		case		PS_OVEN_HUMIDITY	:	fData = chData.fOvenHumidity;		    break;		//ljb 20100726 챔버 습도 저장
		case		PS_STEP_TYPE	    :	fData = float(chData.chStepType);		break;
		case		PS_CUR_CYCLE	    :	fData = float(chData.nCurrentCycleNum);	break;
		case		PS_TOT_CYCLE	    :	fData = float(chData.nTotalCycleNum);	break;
		//case		PS_TEST_NAME	    :	fData = float(lData);			        break;
		//case		PS_SCHEDULE_NAME    :	fData = float(lData);			        break;
		case		PS_CHANNEL_NO	    :	fData = float(chData.chNo);				break;
		case		PS_MODULE_NO	    :	fData = float(m_nModuleID);				break;
		//case		PS_LOT_NO		    :	fData = float(lData);			        break;
		case		PS_DATA_SEQ		    :	fData = float(chData.lSaveSequence);	break;
		case		PS_AVG_CURRENT	    :	fData = chData.fAvgCurrent;				break;		//u단위		//n단위(전류종속 단위)
		case		PS_AVG_VOLTAGE	    :	fData = chData.fAvgVoltage;				break;		//u단위		//n단위(전류종속 단위)
		case		PS_METER_DATA	    :	fData = (float)m_fMeterValue;			break;
		case		PS_CHARGE_CAP	    :	fData = chData.fChargeAh;				break;
		case		PS_DISCHARGE_CAP	:	fData = chData.fDisChargeAh;		    break;
		case		PS_CAPACITANCE	    :	fData = chData.fCapacitance;	        break;
		case		PS_CHARGE_WH	    :	fData = chData.fChargeWh;		        break;
		case		PS_DISCHARGE_WH	    :	fData = chData.fDisChargeWh;	        break;
		case		PS_CV_TIME_DAY	    :	fData = chData.fCVTime_Day;		        break;	//ljb 20131205
		case		PS_CV_TIME		    :	fData = chData.fCVTime;			        break;
		case		PS_SYNC_DATE	    :	fData = float(chData.lSyncDate);		break;
		case		PS_SYNC_TIME	    :	fData = chData.fSyncTime;		        break;
		case		PS_ACC_CYCLE1	    :	fData = float(chData.nAccCycleGroupNum1);	break;
		case		PS_ACC_CYCLE2	    :	fData = float(chData.nAccCycleGroupNum2);	break;
		case		PS_ACC_CYCLE3	    :	fData = float(chData.nAccCycleGroupNum3);	break;
		case		PS_ACC_CYCLE4	    :	fData = float(chData.nAccCycleGroupNum4);	break;
		case		PS_ACC_CYCLE5	    :	fData = float(chData.nAccCycleGroupNum5);	break;
		case		PS_MULTI_CYCLE1	    :	fData = float(chData.nMultiCycleGroupNum1);	break;
		case		PS_MULTI_CYCLE2	    :	fData = float(chData.nMultiCycleGroupNum2);	break;
		case		PS_MULTI_CYCLE3	    :	fData = float(chData.nMultiCycleGroupNum3);	break;
		case		PS_MULTI_CYCLE4	    :	fData = float(chData.nMultiCycleGroupNum4);	break;
		case		PS_MULTI_CYCLE5	    :	fData = float(chData.nMultiCycleGroupNum5);	break;
		case		PS_LOAD_VOLT    	:	fData = chData.fLoadVoltage;		        break;		
		case		PS_LOAD_CURR    	:	fData = chData.fLoadCurrent;		        break;		
		case		PS_CHILLER_REF_TEMP	:	fData = chData.fChillerRefTemp;		        break;
		case		PS_CHILLER_CUR_TEMP	:	fData = chData.fChillerCurTemp;		        break;
// 		case		PS_CHILLER_REF_PUMP	:	fData = float(chData.iChillerRefPump);		break;
// 		case		PS_CHILLER_CUR_PUMP	:	fData = float(chData.iChillerCurPump);	    break;
		case		PS_CHILLER_REF_PUMP	:	fData = chData.fChillerRefPump;		break; //lyj 20201102 DES칠러 소숫점 
		case		PS_CHILLER_CUR_PUMP	:	fData = chData.fChillerCurPump;	    break; //lyj 20201102 DES칠러 소숫점 
		case		PS_CELLBAL_CH_DATA	:	fData = float(chData.dCellBALChData);       break;		
		case		PS_PWRSUPPLY_SETVOLT :	fData = chData.fPwrSetVoltage;              break;		
		case		PS_PWRSUPPLY_CURVOLT :	fData = chData.fPwrCurVoltage;              break;		//yulee 20190514 cny
	}
	return fData;
}

void CCyclerChannel::SetSaveItemList(CWordArray &awItem)
{
	if(awItem.GetSize() < 1)	return;
	ZeroMemory(&m_FileRecordSetHeader, sizeof(PS_RECORD_FILE_HEADER));	

 	m_FileRecordSetHeader.nColumnCount = awItem.GetSize()+1;
	m_FileRecordSetHeader.awColumnItem[0] = PS_DATA_SEQ;					//Data Index Sequence
	for(int i=0; i<awItem.GetSize(); i++)
	{
		m_FileRecordSetHeader.awColumnItem[1+i] = awItem.GetAt(i);
	}
	//m_FileRecordSetHeader.nColumnCount = i;		//ljb 20101230 수정

}

void CCyclerChannel::LoadSaveItemSet()
{
	//CWordArray ItemArray;
	m_SaveItemArray.RemoveAll();
	
	//if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Time Day", TRUE))	
	m_SaveItemArray.Add(PS_STEP_TIME_DAY);		//ljb 20131203 add
	//if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Time", TRUE))	
	m_SaveItemArray.Add(PS_STEP_TIME);
	//if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage", TRUE))	
	m_SaveItemArray.Add(PS_VOLTAGE);
	//if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Current", TRUE))	
	m_SaveItemArray.Add(PS_CURRENT);
	
	m_SaveItemArray.Add(PS_COMM_STATE);		//ljb 20100914
	m_SaveItemArray.Add(PS_CODE); //ksj 20200601 : CODE 값 저장하도록 추가.

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Output State", TRUE)) m_SaveItemArray.Add(PS_CAN_OUTPUT_STATE);		//ljb 20101222
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Input State", TRUE)) m_SaveItemArray.Add(PS_CAN_INPUT_STATE);		//ljb 20101222

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Input", TRUE))	m_SaveItemArray.Add(PS_VOLTAGE_INPUT);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Power", TRUE))	m_SaveItemArray.Add(PS_VOLTAGE_POWER);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Bus", TRUE))		m_SaveItemArray.Add(PS_VOLTAGE_BUS);

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Capacity", TRUE))	
	{
		m_SaveItemArray.Add(PS_CAPACITY);
		m_SaveItemArray.Add(PS_CHARGE_CAP);
		m_SaveItemArray.Add(PS_DISCHARGE_CAP);
		//m_SaveItemArray.Add(PS_CAPACITANCE); //ksj 20200601 : 빼고 대신 CODE 넣음.
	}
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "WattHour", TRUE))
	{
		m_SaveItemArray.Add(PS_WATT);		//ljb 20130423 add
		m_SaveItemArray.Add(PS_WATT_HOUR);
		m_SaveItemArray.Add(PS_CHARGE_WH);
		m_SaveItemArray.Add(PS_DISCHARGE_WH);
	}
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgCurrent", TRUE))		m_SaveItemArray.Add(PS_AVG_CURRENT);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgVoltage", TRUE))		m_SaveItemArray.Add(PS_AVG_VOLTAGE);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenTemperature", TRUE)) m_SaveItemArray.Add(PS_OVEN_TEMPERATURE);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenHumidity", TRUE))	m_SaveItemArray.Add(PS_OVEN_HUMIDITY);
	
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Meter", FALSE))			m_SaveItemArray.Add(PS_METER_DATA);
				
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "CVTime"    , TRUE))
	{
		m_SaveItemArray.Add(PS_CV_TIME_DAY);		//ljb 20150514 add
		m_SaveItemArray.Add(PS_CV_TIME);
	}
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "SyncTime", TRUE))														//ljb 2011418 이재복 //////////
	{
		m_SaveItemArray.Add(PS_SYNC_DATE);
		m_SaveItemArray.Add(PS_SYNC_TIME);
	}

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderVoltage", TRUE)) m_SaveItemArray.Add(PS_LOAD_VOLT);
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderCurrent", TRUE)) m_SaveItemArray.Add(PS_LOAD_CURR);

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerRefTemp", TRUE)) m_SaveItemArray.Add(PS_CHILLER_REF_TEMP);	//ljb 20170912 add
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerCurTemp", TRUE)) m_SaveItemArray.Add(PS_CHILLER_CUR_TEMP);	//ljb 20170912 add
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerRefPump", TRUE)) m_SaveItemArray.Add(PS_CHILLER_REF_PUMP);	//ljb 20170912 add
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerCurPump", TRUE)) m_SaveItemArray.Add(PS_CHILLER_CUR_PUMP);	//ljb 20170912 add
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "CellBALChData" , TRUE)) m_SaveItemArray.Add(PS_CELLBAL_CH_DATA);		//ljb 20170912 add
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "PwrSupplySetVolt" , TRUE)) m_SaveItemArray.Add(PS_PWRSUPPLY_SETVOLT);//ljb 20170912 add//yulee 20190514 cny
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "PwrSupplyCurVolt" , TRUE)) m_SaveItemArray.Add(PS_PWRSUPPLY_CURVOLT);//ljb 20170912 add	
	
	//SetSaveItemList(ItemArray);
	//if(awItem.GetSize() < 1)	return;
	ZeroMemory(&m_FileRecordSetHeader, sizeof(PS_RECORD_FILE_HEADER));	
	
	m_FileRecordSetHeader.nColumnCount = m_SaveItemArray.GetSize()+1;
	m_FileRecordSetHeader.awColumnItem[0] = PS_DATA_SEQ;					//Data Index Sequence
	for(int i=0; i<m_SaveItemArray.GetSize(); i++)
	{
		m_FileRecordSetHeader.awColumnItem[1+i] = m_SaveItemArray.GetAt(i);
	}
}

BOOL CCyclerChannel::SaveRunInfoToTempFile()
{
	CT_TEMP_FILE_DATA tempData;
	ZeroMemory(&tempData, sizeof(CT_TEMP_FILE_DATA));

	CString strTemp, strMsg;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));;

	FILE *fp;
	strMsg.Format("%s\\Temp\\M%02dC%02d.tmp", strTemp, m_nModuleID, m_nChannelIndex+1);
	fp = fopen(strMsg, "wt");

	if(fp == NULL)	return FALSE;
	
	sprintf(tempData.szFilePath, "%s", GetFilePath());
	sprintf(tempData.szLot, "%s", GetTestSerial());
	sprintf(tempData.szWorker, "%s", GetWorkerName());
	sprintf(tempData.szDescript, "%s", GetTestDescript());
	sprintf(tempData.szCellDeltaVolt, "%s", m_strCellDeltaVolt);	//ljb 20150806 add

	fwrite(&tempData, sizeof(CT_TEMP_FILE_DATA), 1, fp);

	//2006/6/12 명령어 예약 정보 기록 
	fwrite(&m_lReservedCycleNo, sizeof(long), 1, fp);
	fwrite(&m_lReservedStepNo, sizeof(long), 1, fp);

	fclose(fp);

	return TRUE;
}

//현재 적용된 Schedule 정보
CScheduleData* CCyclerChannel::GetScheduleData()
{
	//if(m_strScheduleFileName.IsEmpty())	return NULL;	//ljb 20151212 임시 주석
	if(m_strScheduleFileName.IsEmpty())	m_strScheduleFileName = "c:\\schedule.sch";
	if(m_pScheduleData == NULL)
	{
		m_pScheduleData = new CScheduleData;

		m_pScheduleData->SetSchedule(m_strScheduleFileName);
		m_bScheduleData=TRUE;
	}
	return m_pScheduleData;
}

void CCyclerChannel::SetScheduleFile(CString strFileName)
{
	m_strScheduleFileName = strFileName; 
	if(m_pScheduleData != NULL)
	{
		delete m_pScheduleData;			//ljb 20130626 주석 처리 (있으면 작업 시작시 에러 난다)
		m_pScheduleData = NULL;
		m_bScheduleData=FALSE;
	}
}

CString CCyclerChannel::GetStartTime()
{
	CString strTime = m_startTime.Format();
	return strTime;
}

BOOL CCyclerChannel::GetSaveData(PS_STEP_END_RECORD &sChData)//kjh aux
{
/*	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	SFT_CH_DATA sSaveChData;
	if(pChannel->PopSaveData(sSaveChData)== FALSE)	return FALSE;

	sChData.chNo		= sSaveChData.chNo;					// Channel Number
	sChData.chState		= sSaveChData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType	= sSaveChData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect = sSaveChData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		= sSaveChData.chCode;
	sChData.chStepNo	= sSaveChData.chStepNo;
	sChData.chGradeCode = sSaveChData.chGradeCode;

	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.lCurrent, PS_CURRENT);
	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.lCapacity, PS_CAPACITY);
	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.lWattHour, PS_WATT_HOUR);
	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.ulStepTime, PS_STEP_TIME),			// 이번 Step 진행 시간
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.ulTotalTime, PS_TOT_TIME),			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
	sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.lTemparature, PS_TEMPERATURE);
	sChData.fPressure	= 0.0f;
	sChData.nCurrentCycleNum = sSaveChData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.nTotalCycleNum;
	sChData.lSaveSequence	= sSaveChData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.lAvgCurrent, PS_CURRENT);
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;*/
	return TRUE;
}

/*
BOOL CCyclerChannel::GetSaveDataB(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData)
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;
	
	SFT_VARIABLE_CH_DATA_A sSaveChData;
	ZeroMemory(&sSaveChData, sizeof(SFT_VARIABLE_CH_DATA_A));
	if(pChannel->PopSaveDataA(sSaveChData)== FALSE)	return FALSE;
//	CString strLog;
//	strLog.Format("Pop : 채널번호 : %d, Aux Count : %d", sSaveChData.chData.chNo, sSaveChData.chData.nAuxCount);
//	WriteLog(strLog);
	
	sChData.chNo		= sSaveChData.chData.chNo;					// Channel Number
	sChData.chState		= sSaveChData.chData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType	= sSaveChData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect = sSaveChData.chData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		= sSaveChData.chData.chCode;
	sChData.chStepNo	= sSaveChData.chData.chStepNo;
	sChData.chGradeCode = sSaveChData.chData.chGradeCode;
	
	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.chData.lCurrent, PS_CURRENT);
	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.chData.lCapacity, PS_CAPACITY);
	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.chData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.chData.lWattHour, PS_WATT_HOUR);
	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.chData.ulStepTime, PS_STEP_TIME),			// 이번 Step 진행 시간
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.chData.ulTotalTime, PS_TOT_TIME),			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.chData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
//	sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.chData.lTemparature, PS_TEMPERATURE);
	sChData.fPressure	= 0.0f;
	sChData.nCurrentCycleNum = sSaveChData.chData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.chData.nTotalCycleNum;
	sChData.lSaveSequence	= sSaveChData.chData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.chData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.chData.lAvgCurrent, PS_CURRENT);
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;

	
	sAuxData.auxCount = sSaveChData.chData.nAuxCount;
	sAuxData.auxData->chNo = sSaveChData.chData.chNo;					// Channel Number
	for(int i = 0; i < sAuxData.auxCount; i++)
	{
		sAuxData.auxData[i].chNo = sSaveChData.chData.chNo;
		sAuxData.auxData[i].chAuxNo = sSaveChData.auxData[i].chNo;
		sAuxData.auxData[i].chType = sSaveChData.auxData[i].chType;
		sAuxData.auxData[i].fValue = sSaveChData.auxData[i].lValue;
	}
	TRACE("하위버전 Save\r\n");
	return TRUE;
}
*/

//ksj 20210604 : GetLastData 캐시 값 초기화
int CCyclerChannel::ResetGetLastDataCache()
{
	m_bIsGetLastDataCached = FALSE; //캐시 초기화

	ZeroMemory(&m_stLastDataCache, sizeof(PS_STEP_END_RECORD));
	m_strCacheStartT.Empty();
	m_strCacheSerial.Empty();
	m_strCacheUser.Empty();
	m_strCacheDescript.Empty();

	return 0;
}

//ksj 20210604 : GetLastData 캐시 값 업데이트
int CCyclerChannel::SetGetLastDataCache(CString &strStartT, CString &strSerial, CString &strUser, CString &strDescript, PS_STEP_END_RECORD &sChData)
{
	memcpy(&m_stLastDataCache, &sChData, sizeof(PS_STEP_END_RECORD));
	m_strCacheStartT = strStartT;
	m_strCacheSerial = strSerial;
	m_strCacheUser = strUser;
	m_strCacheDescript = strDescript;

	m_bIsGetLastDataCached = TRUE; //캐시됨. 표시

	return 0;
}

//return start index
ULONG CCyclerChannel::GetLastData(CString &strStartT, CString &strSerial, CString &strUser, CString &strDescript, PS_STEP_END_RECORD &sChData)
{
	//ksj 20210604 : 메모리에 rp$ 값이 캐시 되어있는 경우 불필요하게 디스크 엑세스를 하지 않고 메모리 값을 리턴한다.
	if(m_bUseGetLastDataCache && m_bIsGetLastDataCached) 
	{
		strStartT = m_strCacheStartT;
		strSerial = m_strCacheSerial;
		strUser = m_strCacheUser;
		strDescript = m_strCacheDescript;

		return m_stLastDataCache.nIndexFrom;
	}
	//ksj end

	CStdioFile resultFile;
	CFileStatus fs;
	CFileException e;

	CString strCurTempFile, strTemp;
	//////////////////////////////////////////////////////////////////////////
	//1. 임시 파일(.rp$)을 읽어 들인다. (Total 3 Line)
	//Line 1: 시작 시간 종류 시간
	//Line 2: column 목록 
	//Line 2: 최종 data
	CStringList aTempDataList;
	ZeroMemory(&sChData, sizeof(PS_STEP_END_RECORD));

	//최종 data 저장 임시 파일 
	strCurTempFile.Format("%s\\%s.rp$",  m_strFilePath, m_strTestName);//.rp$
	
	if(resultFile.Open( strCurTempFile, CFile::modeRead|CFile::shareDenyWrite, &e ))
	{
		while(resultFile.ReadString(strTemp))
		{
			if(!strTemp.IsEmpty())	aTempDataList.AddTail(strTemp);
		}
		resultFile.Close();
		//2. 최종 시험 data를 Parsing 한다.
		if(aTempDataList.GetCount() >= 3)	
		{
			POSITION pos;
			//2.1 data parsing
			pos = aTempDataList.GetTailPosition();
			strTemp = aTempDataList.GetNext(pos);
			
			//CTable dataTable(aTempDataList.GetNext(pos) , strTemp);
			ParsingChDataString(strTemp, sChData);
		
			//2.2 Start Time and End Time Parsing
			int p1, p2, s;
			pos = aTempDataList.GetHeadPosition();
			// 첫줄에서
			// Start Time 정보 추출 
			strTemp   = m_strDataList.GetNext(pos);
			s  = strTemp.Find("StartT");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strStartT = strTemp.Mid(p1+1,p2-p1-1);
			
			// Test Serial 정보 추출 
			s  = strTemp.Find("Serial");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strSerial = strTemp.Mid(p1+1,p2-p1-1);
			
			// UserID 정보 추출 
			s  = strTemp.Find("User");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strUser = strTemp.Mid(p1+1,p2-p1-1);
			
			// Comment 정보 추출 
			s  = strTemp.Find("Descript");
			p1 = strTemp.Find('=', s);
			p2 = strTemp.Find(',', s);
			strDescript = strTemp.Mid(p1+1,p2-p1-1);
		}		
	}
	else
	{
		WriteLog("Temporary result file open fail.");

		//load default data
		strStartT = m_startTime.Format();
		strSerial = m_strTestSerial;
		strUser = m_strUserID;
		strDescript = m_strDescript;
	}

	return sChData.nIndexFrom;
}

void CCyclerChannel::UpdateLastData(UINT nStartIndex, UINT nEndIndex, CString strStartT, CString strSerial, CString strUser, CString strDescript, PS_STEP_END_RECORD sChData)
{
	CString strCurTempFile, strDataString;
	strCurTempFile.Format("%s\\%s.rp$",  m_strFilePath, m_strTestName);						//.rp$
	
	//시험이 완료 되었으면 임시 파일을 삭제한다.
	if(sChData.chStepType == PS_STEP_END)
	{
		unlink(strCurTempFile);
		//m_bCompleteWork = TRUE;
		//WriteLog(m_strTestName+" 작업 완료");
		WriteLog(m_strTestName+Fun_FindMsg("CyclerChannel_UpdateLastData_msg1","CYCLERCHANNEL"));//&&

		ResetGetLastDataCache(); //ksj 20210604 : 캐시 리셋
	}
	else	//시험이 완료되지 않았으면 다음 step 저장을 위해 Index를 저장한다. Step 번호를 증가 시키고 시작 Index를 저장한다.
	{
		//6. 최종 임시파일은 갱신한다.
		//No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,Code,Grade,Voltage,Current,Capacity,WattHour,IR,Temp,Press,AvgVoltage,AvgCurrent,	
		//#define TABEL_FILE_COLUMN_HEAD 	"No,IndexFrom,IndexTo,CurCycle,TotalCycle,Type,State,Time,TotTime,
		//Voltage,Current,Capacity,Power,WattHour,IR,OvenT,OvenH,VoltageAverage,CurrentAverage,Sequence,
		//ChargeAh,DisChargeAh,Capacitance,ChargeWh,DisChargeWh,CVTime,SyncDate,SyncTime,
		//AccCycle1,AccCycle2,AccCycle3,AccCycle4,AccCycle5,MultiCycle1,MultiCycle2,MultiCycle3,MultiCycle4,MultiCycle5,
		//CommState,Voltage Input,Voltage Power,Voltage Bus,OutputState,InputState,Code,Grade,Press"
//yulee 20190514 cny
/*		strDataString.Format("%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f,%d,%d,%d,%d,%.2f,%.2f,%d,%d,%f,%.2f,%.2f",*/
		strDataString.Format("%d,%d,%d,%d,%d,%d,%d,%.1f,%.1f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%.2f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f,%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%f,%.2f,%.2f", //lyj 20201102 DES칠러 소숫점 
									sChData.chStepNo,			//StepNo
									nStartIndex,				//IndexFrom	
									nEndIndex,					//IndexTo
									sChData.nCurrentCycleNum,	//CurCycle
									sChData.nTotalCycleNum,		//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
									sChData.chStepType,			//Type
									sChData.chState,			//State
									sChData.fStepTime,			//Time
									sChData.fTotalTime,			//TotTime
									sChData.fVoltage, 			//Voltage
									sChData.fCurrent, 			//Current
									sChData.fCapacity, 			//Capacity
									sChData.fWattHour, 			//WattHour
									sChData.fImpedance,			//IR
									sChData.fOvenTemperature, 	//Chamber Temp(OVEN)
									sChData.fOvenHumidity, 		//Chamber Humidity(OVEN)
									sChData.fAvgVoltage,		//AvgVoltage
									sChData.fAvgCurrent,		//AvgVoltage	
									sChData.lSaveSequence,
									sChData.fChargeAh,									
									sChData.fDisChargeAh,
									sChData.fCapacitance,
									sChData.fChargeWh,
									sChData.fDisChargeWh,
									sChData.fCVTime,
									sChData.lSyncDate,
									sChData.fSyncTime,
									sChData.nAccCycleGroupNum1,
									sChData.nAccCycleGroupNum2,
									sChData.nAccCycleGroupNum3,
									sChData.nAccCycleGroupNum4,
									sChData.nAccCycleGroupNum5,
									sChData.nMultiCycleGroupNum1,
									sChData.nMultiCycleGroupNum2,
									sChData.nMultiCycleGroupNum3,
									sChData.nMultiCycleGroupNum4,
									sChData.nMultiCycleGroupNum5,
									sChData.chCommState,
									sChData.fVoltageInput,
									sChData.fVoltagePower,
									sChData.fVoltageBus,
									sChData.chOutputState,
									sChData.chInputState,
									sChData.chCode,				//Code
									sChData.chGradeCode,		//Grade
									sChData.fChillerRefTemp,
									sChData.fChillerCurTemp,
// 									sChData.iChillerRefPump,
// 									sChData.iChillerCurPump,
 									sChData.fChillerRefPump, //lyj 20201102 DES칠러 소숫점 
									sChData.fChillerCurPump, //lyj 20201102 DES칠러 소숫점 
									sChData.dCellBALChData,
									sChData.fPwrSetVoltage,//yulee 20190514 cny
									sChData.fPwrCurVoltage
						            );
		//CommState,Voltage Input,Voltage Power,Voltage Bus,OutputState,InputState,Code,Grade,Press"
		//현재 step의 진행된 부분까지 tmp파일에 저장한다.

		FILE *fp = fopen(strCurTempFile, "wt");
		if(fp)
		{
			CString strLine1, strLine2;
			COleDateTime t = COleDateTime::GetCurrentTime();
			strLine1.Format("StartT=%s, EndT=%s, Serial=%s, User=%s, Descript=%s", strStartT, t.Format(), strSerial, strUser, strDescript);
			strLine2 = TABEL_FILE_COLUMN_HEAD;
			fprintf(fp, "%s\n%s\n%s", strLine1, strLine2, strDataString);
			fclose(fp);

			//ksj 20210604 : 메모리에 최종 rp$ 값을 업데이트 한다.
			sChData.nIndexFrom = nStartIndex;
			sChData.nIndexTo = nEndIndex;
			SetGetLastDataCache(strStartT, strSerial, strUser, strDescript, sChData);
		}
		//TRACE("%s 파일 Udpated(%d Step Saved)\n", m_strResultStepFileName, strDataList.GetCount()-1);		
	}
	//////////////////////////////////////////////////////////////////////////	
}

//20110319 KHS
void CCyclerChannel::UpdateModuleSpec()
{
	CString strCurTempFile, strDataString;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");
	
	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;
		strCurTempFile.Format("%s\\%s.ini",  strDataSharedPath, m_strTestName);		//.ini	wri
	}
	else
	{
		strCurTempFile.Format("%s\\M%02dCH%02d.ini",  strDataSharedPath, m_nModuleID, GetChannelIndex()+1);		//.ini
	}
	
	CMainFrame *pMain=(CMainFrame *)AfxGetMainWnd();
	CCTSMonProDoc *pDoc=(CCTSMonProDoc *)pMain->GetActiveDocument();
	CCyclerModule *lpModule;
	lpModule = pDoc->GetModuleInfo(pDoc->GetModuleID(m_nModuleID-1));
	float fVoltage = (lpModule->GetVoltageSpec(0))/1000.0f;
	float fCurrent = (lpModule->GetCurrentSpec())/1000.0f;
	
	strDataString.Format("%d,%d,%.1f,%.1f", m_nModuleID, GetChannelIndex()+1, fVoltage, fCurrent); 
	
	//ini파일에 저장한다.
	FILE *fp = fopen(strCurTempFile, "wt");
	if(fp)
	{
		CString strLine;
		strLine = TABLE_FILE_COLUMN_HEAD3;
		fprintf(fp, "%s\n%s", strLine, strDataString);
		fclose(fp);
	}
}

//20110319 KHS
void CCyclerChannel::UpdateChannelState()
{
	CString strCurTempFile, strDataString;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");
	CString strCode;

	CFileStatus fs;
	COleDateTime t = COleDateTime::GetCurrentTime();
	
	PS_STEP_END_RECORD prevData;
	CString strStartTime, strTestSerial, strUser, strDescript, strTrayNo;
	long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
	
	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;
		strCurTempFile.Format("%s\\%s.txt", strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}
	else
	{
		strCurTempFile.Format("%s\\%s_M%02dCH%02d.txt",  strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}
	
	//lyj 20201028 s ==========================
	if(GetUserComplete() == TRUE)		//153 : 작업완료
	{		
		//strCode.Format("완료 Step(25)");

		strCode.Format("%s",ValueString(25, PS_CODE));

		CString strLog;
		strLog.Format("UpdateChannelState : %s -> End", ValueString(GetCellCode(), PS_CODE));
		WriteLog(strLog);
	}
	else
	{
		strCode = ValueString(GetCellCode(), PS_CODE);
	}
	//lyj 20201028 e ==========================

	//최종 파일을 갱신한다.
	//Module, Channel, StartTime, EndTime, StepNo, StepType, StateCode, StepTime, TotalTime,
	//Voltage, Current, Capacity, Power, WattHour, Impedance, Temp, CurCycle, TotalCycle, AvgVoltage, AvgCurrent, ChargeCapacity, DischargeCapacity
	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s",		//20110319 KHS  Temp 포함
//	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s",			//20110319 KHS  Temp 미포함
		m_nModuleID,													//ModuleNo
		GetChannelIndex()+1,											//ChNo
		strStartTime,													//StartT
		t.Format(),														//EndT	
		GetStepNo(),													//StepNo
		::PSGetTypeMsg(GetStepType()),									//Type
		//ValueString(GetCellCode(), PS_CODE),							//Code
		strCode,														//Code //lyj 20201028 
		TimeValueString(GetStepTime()),									//Time
		TimeValueString(GetTotTime()),									//TotTime
		ValueString(GetVoltage(), PS_VOLTAGE), 							//Voltage
		ValueString(GetCurrent(), PS_CURRENT), 							//Current
		ValueString(GetCapacity(), PS_CAPACITY),						//Capacity
		ValueString(GetWatt(), PS_WATT),								//Power
		ValueString(GetWattHour(), PS_WATT_HOUR),						//WattHour
		ValueString(GetImpedance(), PS_IMPEDANCE),						//IR
		"0",															//Temp
//		ValueString(GetTemperature(), PS_TEMPERATURE),					//Temp
		GetCurCycleCount(),												//CurCycle
		GetTotalCycleCount(),											//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
		ValueString(GetAvgVoltage(), PS_AVG_VOLTAGE),					//AvgVoltage
		ValueString(GetAvgCurrent(), PS_AVG_CURRENT),					//AvgCurrent
		ValueString(GetChargeAh(), PS_CHARGE_CAP),						//CharCap
		ValueString(GetDisChargeAh(), PS_DISCHARGE_CAP)					//DischarCap
		);
	//현재 step의 진행된 부분까지 txt파일에 저장한다.
	FILE *fp = fopen(strCurTempFile, "wt");
	if(fp)
	{
		CString strLine;
		strLine = TABLE_FILE_COLUMN_HEAD2;
		fprintf(fp, "%s\n%s", strLine, strDataString);
		fclose(fp);
	}
}

//20120210 KHS UpdateChannelState에 Data Loss 추가
void CCyclerChannel::UpdateChannelLossState()
{
	CString strCurTempFile, strDataString;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");		//20100620 KHS
	CString strCode;

	CFileStatus fs;
	COleDateTime t = COleDateTime::GetCurrentTime();
	
	PS_STEP_END_RECORD prevData;
	CString strStartTime, strTestSerial, strUser, strDescript, strTrayNo;
	long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
	
	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;		//20100620 KHS
		strCurTempFile.Format("%s\\%s.txt",  strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}
	else
	{
		strCurTempFile.Format("%s\\%s_M%02dCH%02d.txt",  strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}
	
	//lyj 20201028 s ==========================
	if(GetUserComplete() == TRUE)		//153 : 작업완료
	{
		//strCode.Format("완료 Step(25)");

		strCode.Format("%s",ValueString(25, PS_CODE));

		CString strLog;
		strLog.Format("UpdateChannelLossState : %s -> End", ValueString(GetCellCode(), PS_CODE));
		WriteLog(strLog);
	}
	else
	{
		strCode = ValueString(GetCellCode(), PS_CODE);
	}
	//lyj 20201028 e ==========================

	//최종 파일을 갱신한다.
	//Module, Channel, StartTime, 
	//EndTime, StepNo, StepType, StateCode, StepTime, TotalTime,
	//Voltage, Current, Capacity, Power, WattHour, Impedance, Temp, 
	//CurCycle, TotalCycle, AvgVoltage, AvgCurrent, ChargeCapacity, DischargeCapacity
	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s",		//20110319 KHS  Temp 포함
//	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s",			//20110319 KHS  Temp 미포함
		m_nModuleID,													//ModuleNo
		GetChannelIndex()+1,											//ChNo
		strStartTime,													//StartT
		t.Format(),														//EndT	
		GetStepNo(),													//StepNo
		::PSGetTypeMsg(GetStepType()),									//Type
		//ValueString(GetCellCode(), PS_CODE),							//Code
		strCode,														//Code //lyj 20201028
		TimeValueString(GetStepTime()),									//Time
		TimeValueString(GetTotTime()),									//TotTime
		ValueString(GetVoltage(), PS_VOLTAGE), 							//Voltage
		ValueString(GetCurrent(), PS_CURRENT), 							//Current
		ValueString(GetCapacity(), PS_CAPACITY),						//Capacity
		ValueString(GetWatt(), PS_WATT),								//Power
		ValueString(GetWattHour(), PS_WATT_HOUR),						//WattHour
		ValueString(GetImpedance(), PS_IMPEDANCE),						//IR
		"0",															//Temp
//		ValueString(GetTemperature(), PS_TEMPERATURE),					//Temp
		GetCurCycleCount(),												//CurCycle
		GetTotalCycleCount(),											//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
		ValueString(GetAvgVoltage(), PS_AVG_VOLTAGE),					//AvgVoltage
		ValueString(GetAvgCurrent(), PS_AVG_CURRENT),					//AvgCurrent
		ValueString(GetChargeAh(), PS_CHARGE_CAP),						//CharCap
		ValueString(GetDisChargeAh(), PS_DISCHARGE_CAP),				//DischarCap
		m_strLossTime,													//Loss Time
		"Data Loss Detected"											//Data Loss
		);
	TRACE("Channel State ========== StepTime : %s, TotalTime : %s\n",
		TimeValueString(GetStepTime()), TimeValueString(GetTotTime()));
	
	//현재 step의 진행된 부분까지 txt파일에 저장한다.
	FILE *fp = fopen(strCurTempFile, "wt");
	if(fp)
	{
		CString strLine;
		strLine = TABLE_FILE_COLUMN_HEAD3;
		fprintf(fp, "%s\n%s", strLine, strDataString);
		fclose(fp);
	}
}

//20120210 KHS  StepEnd일 경우는 End 값으로 표시
void CCyclerChannel::UpdateChannelEndState(CString strStartTime, PS_STEP_END_RECORD channelData)
{
	CString strCurTempFile, strDataString;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");		//20100620 KHS
	CString strCode;

	CFileStatus fs;
	COleDateTime t = COleDateTime::GetCurrentTime();
	
	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;		//20100620 KHS
		strCurTempFile.Format("%s\\%s.txt", strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}
	else
	{
		strCurTempFile.Format("%s\\%s_M%02dCH%02d.txt",  strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}	
	
	//lyj 20201028 s ==========================
	if(GetUserComplete() == TRUE)		//153 : 작업완료
	{
		//strCode.Format("완료 Step(25)");

		strCode.Format("%s",ValueString(25, PS_CODE));

		CString strLog;
		strLog.Format("UpdateChannelEndState : %s -> End", ValueString(GetCellCode(), PS_CODE));
		WriteLog(strLog);
	}
	else
	{
		TRACE("#########UpdateChannelEndState()");
		strCode = ValueString(GetCellCode(), PS_CODE);
	}
	//lyj 20201028 e ==========================

	//최종 파일을 갱신한다.
	//Module, Channel, StartTime, EndTime, StepNo, StepType, StateCode, StepTime, TotalTime,
	//Voltage, Current, Capacity, Power, WattHour, Impedance, Temp, CurCycle, TotalCycle, AvgVoltage, AvgCurrent, ChargeCapacity, DischargeCapacity
	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s",		//20110319 KHS  Temp 포함
//	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s",			//20110319 KHS  Temp 미포함
		m_nModuleID,													//ModuleNo
		GetChannelIndex()+1,											//ChNo
		strStartTime,													//StartT
		t.Format(),														//EndT	
		channelData.chStepNo,											//StepNo
		::PSGetTypeMsg(channelData.chStepType),							//Type
		//ValueString(channelData.chCode, PS_CODE),						//Code
		strCode,														//Code //lyj 20201028
		TimeValueString(GetStepTime()),									//Time
		TimeValueString(GetTotTime()),									//TotTime
		ValueString(GetVoltage(), PS_VOLTAGE), 							//Voltage
		ValueString(GetCurrent(), PS_CURRENT), 							//Current
		ValueString(GetCapacity(), PS_CAPACITY),						//Capacity
		ValueString(GetWatt(), PS_WATT),								//Power
		ValueString(GetWattHour(), PS_WATT_HOUR),						//WattHour
		ValueString(GetImpedance(), PS_IMPEDANCE),						//IR
		"0",															//Temp
//		ValueString(GetTemperature(), PS_TEMPERATURE),					//Temp
		GetCurCycleCount(),												//CurCycle
		GetTotalCycleCount(),											//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
		ValueString(GetAvgVoltage(), PS_AVG_VOLTAGE),					//AvgVoltage
		ValueString(GetAvgCurrent(), PS_AVG_CURRENT),					//AvgCurrent
		ValueString(GetChargeAh(), PS_CHARGE_CAP),						//CharCap
		ValueString(GetDisChargeAh(), PS_DISCHARGE_CAP)					//DischarCap
		);
	
	//현재 step의 진행된 부분까지 txt파일에 저장한다.
	FILE *fp = fopen(strCurTempFile, "wt");
	if(fp)
	{
		CString strLine;
		strLine = TABLE_FILE_COLUMN_HEAD2;
		fprintf(fp, "%s\n%s", strLine, strDataString);
		fclose(fp);
	}	
}

//20120210 KHS  StepEnd일 경우는 End 값으로 표시
void CCyclerChannel::UpdateChannelEndLossState(CString strStartTime, PS_STEP_END_RECORD channelData)
{
	CString strCurTempFile, strDataString;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");		//20100620 KHS
	CString strCode;

	CFileStatus fs;
	COleDateTime t = COleDateTime::GetCurrentTime();
	
	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;		//20100620 KHS
		strCurTempFile.Format("%s\\%s.txt",  strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}
	else
	{
		strCurTempFile.Format("%s\\%s_M%02dCH%02d.txt",  strDataSharedPath, m_strTestName, m_nModuleID, GetChannelIndex()+1);		//.txt
	}
	
	//lyj 20201028 s ==========================
	if(GetUserComplete() == TRUE)
	{
		//strCode.Format("완료 Step(25)");

		strCode.Format("%s",ValueString(25, PS_CODE));

		CString strLog;
		strLog.Format("UpdateChannelEndLossState : %s -> End", ValueString(GetCellCode(), PS_CODE));
		WriteLog(strLog);
	}
	else
	{
		strCode = ValueString(GetCellCode(), PS_CODE);
	}
	//lyj 20201028 e ==========================

	//최종 파일을 갱신한다.
	//Module, Channel, StartTime, 
	//EndTime, StepNo, StepType, StateCode, StepTime, TotalTime,
	//Voltage, Current, Capacity, Power, WattHour, Impedance, Temp, 
	//CurCycle, TotalCycle, AvgVoltage, AvgCurrent, ChargeCapacity, DischargeCapacity
	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s",		//20110319 KHS  Temp 포함
//	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s",			//20110319 KHS  Temp 미포함
		m_nModuleID,													//ModuleNo
		GetChannelIndex()+1,											//ChNo
		strStartTime,													//StartT
		t.Format(),														//EndT	
		channelData.chStepNo,											//StepNo
		::PSGetTypeMsg(channelData.chStepType),							//Type
		//ValueString(channelData.chCode, PS_CODE),						//Code
		strCode,														//Code //lyj 20201028
		TimeValueString(GetStepTime()),									//Time
		TimeValueString(GetTotTime()),									//TotTime
		ValueString(GetVoltage(), PS_VOLTAGE), 							//Voltage
		ValueString(GetCurrent(), PS_CURRENT), 							//Current
		ValueString(GetCapacity(), PS_CAPACITY),						//Capacity
		ValueString(GetWatt(), PS_WATT),								//Power
		ValueString(GetWattHour(), PS_WATT_HOUR),						//WattHour
		ValueString(GetImpedance(), PS_IMPEDANCE),						//IR
		"0",															//Temp
//		ValueString(GetTemperature(), PS_TEMPERATURE),					//Temp
		GetCurCycleCount(),												//CurCycle
		GetTotalCycleCount(),											//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
		ValueString(GetAvgVoltage(), PS_AVG_VOLTAGE),					//AvgVoltage
		ValueString(GetAvgCurrent(), PS_AVG_CURRENT),					//AvgCurrent
		ValueString(GetChargeAh(), PS_CHARGE_CAP),						//CharCap
		ValueString(GetDisChargeAh(), PS_DISCHARGE_CAP),				//DischarCap
		m_strLossTime,													//Loss Time
		"Data Loss Detected"											//Data Loss
		);
	TRACE("Channel State ========== StepTime : %s, TotalTime : %s\n",
		TimeValueString(GetStepTime()), TimeValueString(GetTotTime()));
	
	//현재 step의 진행된 부분까지 txt파일에 저장한다.
	FILE *fp = fopen(strCurTempFile, "wt");
	if(fp)
	{
		CString strLine;
		strLine = TABLE_FILE_COLUMN_HEAD3;
		fprintf(fp, "%s\n%s", strLine, strDataString);
		fclose(fp);
	}
}

void CCyclerChannel::SaveStepEndData(CString strStartT, PS_STEP_END_RECORD sChData)
{
	CString strCurTempFile, strDataString, strEndT;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");

	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;
		strCurTempFile.Format("%s\\%s_StepEnd.csv",  strDataSharedPath, m_strTestName);			//.csv
	}
	else
	{
		strCurTempFile.Format("%s\\%s_StepEnd_M%02dCH%02d.csv",  strDataSharedPath, m_strTestName, m_nModuleID, sChData.chNo);			//.csv
	}
	
	CMainFrame *pMain=(CMainFrame *)AfxGetMainWnd();
//	CCTSMonProDoc *pDoc=(CCTSMonProDoc *)pMain->GetActiveDocument();	//ljb 2011321 이재복 //////////

	//make end time
	/*COleDateTime t = COleDateTime::GetCurrentTime();
	t.ParseDateTime(strStartT);

	long lDay = (long)sChData.fTotalTime_Day;		//ljb 20131208 add
	float fTotTime = sChData.fTotalTime;

	long lHour = (long)fTotTime;
	long lMinute = lHour % 3600;		lHour = lHour / 3600;				
	long lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime(lDay, lHour, lMinute, lSecond);
	t = t+rTime;
	TRACE("%d Day %d:%d:%d\n", lDay, lHour, lMinute, lSecond);*/

	//make step start time
	/*COleDateTime t1 = COleDateTime::GetCurrentTime();
	t1.ParseDateTime(strStartT);
	fTotTime = sChData.fTotalTime-sChData.fStepTime;
	if(fTotTime < 0.0f)	fTotTime = 0.0f;

	lDay = (long)sChData.fTotalTime_Day - (long)sChData.fStepTime_Day;		//ljb 20131208 add
	lHour = (long)fTotTime;
	lMinute = lHour % 3600;		lHour = lHour / 3600;				
	lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime1(lDay, lHour, lMinute, lSecond);
	t1 = t1+rTime1;*/

	//ksj 20201014 :
	//스텝 시작시간 종료시간 다시 만들어야 한다
	//시험 시작시간(PC기준파일생성시간)에 오차가 있고, 토탈타임과 스텝타임은 일시정지 시간을 포함하고 있지 않아서 오차가 크다.
	//기존 스텝 시작시간 : 시험 시작시간 + (토탈타임-스텝타임) 
	//기존 스텝 종료시간 : 시험 시작시간 + 토탈타임
	//
	//신규 스텝 시작시간 : 싱크타임 - 스텝타임 (여전히 스텝타임에 일시정지등이 포함되어 있지 않아 시작 시간은 오차 있음)
	//신규 스텝 종료시간 : 싱크타임
	//	

	//스텝 엔드 타임 생성. (SBC 싱크타임을 그대로 쓴다)
	CString strT;
	CString strTemp;
	COleDateTime timeStepEnd = COleDateTime::GetCurrentTime();
	COleDateTime timeStepStart = COleDateTime::GetCurrentTime();

	strT.Format("%d",sChData.lSyncDate);
	if (strT.GetLength() >= 8 ) strTemp.Format("%s-%s-%s %s", strT.Left(4), strT.Mid(4, 2), strT.Right(2), ValueString(((double)((int)sChData.fSyncTime)), PS_SYNC_TIME)); //소수점 아래 값 버리도록 수정. 간혹 59.88초 등 반올림이 일어나는 경우 60초가되어, 오류 발생
	else strTemp ="";
	
	BOOL bTimeParsingRes = timeStepEnd.ParseDateTime(strTemp); //종료시간

	//시작시간 계산 (종료시간 - 스텝시간) 부정확.
	long lDay = (long)sChData.fStepTime_Day;
	long lHour = (long)sChData.fStepTime;
	long lMinute = lHour % 3600; 
	lHour = lHour / 3600;				
	long lSecond = lMinute % 60;
	lMinute = lMinute / 60;

	if(bTimeParsingRes)
	{
		COleDateTimeSpan stepTime(lDay, lHour, lMinute, lSecond);
		timeStepStart = timeStepEnd-stepTime; //종료시간 - 스텝시간
	}
	TRACE("Step End ========== StartTime : %s, EndTime : %s\n", timeStepStart.Format(), timeStepEnd.Format());
	//ksj end/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//TRACE("Step End ========== StartTime : %s, EndTime : %s\n", t1.Format(), t.Format());

//	CCyclerModule *lpModule;
//	CCyclerChannel *lpChannelInfo;

	//ljb 20140110 start
	CString strStepTime;
	if (sChData.fStepTime_Day > 0)
		strStepTime.Format("%.0fD %s", sChData.fStepTime_Day,ValueString(sChData.fStepTime, PS_STEP_TIME));
	else
		strStepTime.Format("%s",ValueString(sChData.fStepTime, PS_STEP_TIME));

	CString strTotalTime;
	if (sChData.fTotalTime_Day > 0)
		strTotalTime.Format("%.0fD %s", sChData.fTotalTime_Day,ValueString(sChData.fTotalTime, PS_STEP_TIME));
	else
		strTotalTime.Format("%s",ValueString(sChData.fTotalTime, PS_STEP_TIME));
	//ljb 20140110 end

	//최종 파일을 갱신한다.
	//Module, Channel, StartTime, EndTime, StepNo, StepType, StateCode, StepTime, TotalTime,
	//Voltage, Current, Capacity, Power, WattHour, Impedance, Temp, CurCycle, TotalCycle, AvgVoltage, AvgCurrent, ChargeCapacity, DischargeCapacity
//	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s",		//20110319 KHS  Temp 포함
//	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%.0f,%s,%.0f,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s",			//20110319 KHS  Temp 미포함
	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s",			//ljb 20140110 edit Day포함해서 하나의 string로 변경
		m_nModuleID,														//ModuleNo
		sChData.chNo,														//ChNo
// 		t1.Format(),														//StartT
// 		t.Format(),															//EndT	
		timeStepStart.Format(),
		timeStepEnd.Format(),
		sChData.chStepNo,													//StepNo
		::PSGetTypeMsg(sChData.chStepType),									//Type
		ValueString(sChData.chCode, PS_CODE),								//Code
		strStepTime,
		strTotalTime,
// 		sChData.fStepTime_Day,						//ljb 20131208 add
// 		ValueString(sChData.fStepTime, PS_STEP_TIME),						//Time
// 		sChData.fTotalTime,							//ljb 20131208 add
// 		ValueString(sChData.fTotalTime, PS_TOT_TIME),						//TotTime
		ValueString(sChData.fVoltage, PS_VOLTAGE), 							//Voltage
		ValueString(sChData.fCurrent, PS_CURRENT), 							//Current
		ValueString(sChData.fCapacity, PS_CAPACITY),						//Capacity
		ValueString(sChData.fWatt, PS_WATT),								//Power
		ValueString(sChData.fWattHour, PS_WATT_HOUR),						//WattHour
		ValueString(sChData.fImpedance, PS_IMPEDANCE),						//IR
//		ValueString(sChData.fTemparature, PS_TEMPERATURE),					//Temp
		sChData.nCurrentCycleNum,											//CurCycle
		sChData.nTotalCycleNum,												//TotalCycle => Cycle별 파일 생성시는 FileNo로 사용됨
		ValueString(sChData.fAvgVoltage, PS_AVG_VOLTAGE),					//AvgVoltage
		ValueString(sChData.fAvgCurrent, PS_AVG_CURRENT),					//AvgCurrent
		ValueString(sChData.fChargeAh, PS_CHARGE_CAP),						//CharCap
		ValueString(sChData.fDisChargeAh, PS_DISCHARGE_CAP)					//DischarCap
		);
	
	CStdioFile file;
	CString strLineBuffer, strLine;
	strLine = TABLE_FILE_COLUMN_HEAD2;
	
	TRY
	{
		if(file.Open(strCurTempFile, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate) == FALSE)
		{
			//WriteLog(strCurTempFile + " 파일을 열 수 없습니다.");
			WriteLog(strCurTempFile + Fun_FindMsg("CyclerChannel_SaveStepEndData_msg1","CYCLERCHANNEL"));//&&
			return;
		}
		
		file.ReadString(strLineBuffer);
		if(strLineBuffer != strLine || strLineBuffer == "")
		{
			file.WriteString(strLine);
			file.WriteString("\n");
		}
		file.SeekToEnd();				//끝 위치를 찾는다.
		
		file.WriteString(strDataString);
		file.WriteString("\n");
	}
	CATCH(CFileException, ex)
	{
		ex->ReportError();
		file.Close();
	}
	END_CATCH
		
	file.Close();
}

void CCyclerChannel::SaveStepEndAuxData(CString strStartT, PS_STEP_END_RECORD sChData, PS_AUX_RAW_DATA sAuxData)
{
	CString strCurTempFile, strDataString, strEndT;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");

	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;
		strCurTempFile.Format("%s\\%s_StepEndAux.csv",  strDataSharedPath, m_strTestName);			//.csv
	}
	else
	{
		strCurTempFile.Format("%s\\%s_StepEndAux_M%02dCH%02d.csv",  strDataSharedPath, m_strTestName, m_nModuleID, sChData.chNo);			//.csv
	}
	
	CMainFrame *pMain=(CMainFrame *)AfxGetMainWnd();
//	CCTSMonProDoc *pDoc=(CCTSMonProDoc *)pMain->GetActiveDocument();	//ljb 2011321 이재복 //////////

	//make end time
	COleDateTime t = COleDateTime::GetCurrentTime();
	t.ParseDateTime(strStartT);

	long lDay = (long)sChData.fTotalTime_Day;		//ljb 20131208 add
	float fTotTime = sChData.fTotalTime;

	//long lDay = (long)fTotTime/86400;		
	//long lHour = (long)fTotTime%86400;
	long lHour = (long)fTotTime;
	long lMinute = lHour % 3600;		lHour = lHour / 3600;				
	long lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime(lDay, lHour, lMinute, lSecond);
	t = t+rTime;
	TRACE("%d Day %d:%d:%d\n", lDay, lHour, lMinute, lSecond);

	//make step start time
	COleDateTime t1 = COleDateTime::GetCurrentTime();
	t1.ParseDateTime(strStartT);
	fTotTime = sChData.fTotalTime-sChData.fStepTime;
	if(fTotTime < 0.0f)	fTotTime = 0.0f;
	//lDay = (long)fTotTime/86400;		
	lDay = (long)sChData.fTotalTime_Day - (long)sChData.fStepTime_Day;		//ljb 20131208 add
	//lHour = (long)fTotTime%86400;
	lHour = (long)fTotTime;
	lMinute = lHour % 3600;		lHour = lHour / 3600;				
	lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime1(lDay, lHour, lMinute, lSecond);
	t1 = t1+rTime1;

	TRACE("Step End ========== StartTime : %s, EndTime : %s\n", t1.Format(), t.Format());

//	CCyclerModule *lpModule;
//	CCyclerChannel *lpChannelInfo;

	//ljb 20140110 start
	CString strStepTime;
	if (sChData.fStepTime_Day > 0)
		strStepTime.Format("%.0fD %s", sChData.fStepTime_Day,ValueString(sChData.fStepTime, PS_STEP_TIME));
	else
		strStepTime.Format("%s",ValueString(sChData.fStepTime, PS_STEP_TIME));

	CString strTotalTime;
	if (sChData.fTotalTime_Day > 0)
		strTotalTime.Format("%.0fD %s", sChData.fTotalTime_Day,ValueString(sChData.fTotalTime, PS_STEP_TIME));
	else
		strTotalTime.Format("%s",ValueString(sChData.fTotalTime, PS_STEP_TIME));
	//ljb 20140110 end

	CString strLineBuffer, strLine;
	strLine = TABLE_FILE_COLUMN_HEAD5; 

	//yulee 20181124
	//1. 컬럼을 세팅된 숫자 만큼 
	//			AuxT+"AuxT Max", "AuxT Min" + "AuxT Dev",
	//			AuxV+"AuxV Max", "AuxV Min" + "AuxV Dev",
	//			AuxTH+"AuxTH Max", "AuxTH Min" + "AuxTH Dev" 개수 만큼 만든다. 
	//2. 데이터에도 Aux 숫자에 맞추서 포함 시킨다. 

	int nIAuxT=0, nIAuxV=0, nIAuxTH=0; 
	int nIAuxHumi=0; //ksj 20200116
	CString strAuxName;
	for(int i=0; i<sAuxData.auxCount; i++)
	{
		switch(sAuxData.auxData[i].chType)
		{
		case 0:
			  {
			//	  strAuxName.Format("[%003d]%s(온도센서)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_TEMP);
				  nIAuxT++;
				  continue;
			  };
		case 1:
			  {
			//	  strAuxName.Format("[%003d]%s(전압센서)(V)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_VOLT);
				  nIAuxV++;
				  continue;
			  };
		case 2:
			  {
			//	  strAuxName.Format("[%003d]%s(써미스터)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_TEMPTH);
				  nIAuxTH++;
				  continue;
			  };
		case PS_AUX_TYPE_HUMIDITY: //ksj 20200116
			{
				nIAuxHumi++;
				continue;
			};
		default:
			;
		}
	}

	//int nIAuxColumnCnt;
	int nAuxCnt, nIAuxTCnt=0, nIAuxVCnt=0, nIAuxTHCnt=0;
	int nIAuxHumiCnt=0;//ksj 20200116
	CString tmpStr, strAuxValue, strAuxValuePre, strAuxColumn = "";
	FLOAT fltTmpValue, fltAuxTempMin, fltAuxTempMax, fltAuxVoltMin, fltAuxVoltMax, fltAuxTempTHMin, fltAuxTempTHMax;
	CString strAuxVValue,strAuxTValue,strAuxTHValue,strAuxHValue;
	FLOAT fltAuxHumiMin, fltAuxHumiMax; //ksj 20200116
	strAuxValue.Format("");
	nAuxCnt = sAuxData.auxCount;

	if(nIAuxT > 0)
		nIAuxT += 3;
	if(nIAuxV > 0)
		nIAuxV += 3;
	if(nIAuxTH > 0)
		nIAuxTH += 3;
	if(nIAuxHumi > 0) //ksj 20200116
		nIAuxHumi += 3;

	int nType = 0;//(Aux)
	for(int i=0; i<sAuxData.auxCount; i++)
	{		
		switch(sAuxData.auxData[i].chType)
		{
		case 0:
			{
				strAuxName.Format("[%003d]%s(%s)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_TEMP, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

				nIAuxTCnt++;
				if(nIAuxT>0)
				{
					fltAuxTempMin=fltAuxTempMax=0;
					//이름을 Column에 포함 시킨다.
					tmpStr.Format("%s,", strAuxName);
					strAuxColumn += tmpStr;
					if(i==0)
					{
						strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}
					else
					{
						strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}
					fltAuxTempMax = max(fltTmpValue, fltAuxTempMax);
					fltAuxTempMin = min(fltTmpValue, fltAuxTempMin);

					strAuxValuePre = strAuxValue;
					
					if(nIAuxT==(nIAuxTCnt+3))
					{
						tmpStr.Format("Aux Temp. Max,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempMax);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux Temp. Min,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempMin);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux Temp. Dev,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempMax-fltAuxTempMin);
						strAuxValuePre = strAuxValue;
					}	
				}
				break;
			}
		case 1:
			{
				strAuxName.Format("[%003d]%s(%s)(V)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_VOLT, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

				nIAuxVCnt++;
				if(nIAuxV>0)
				{
					fltAuxVoltMin=fltAuxVoltMax=0;
					//이름을 Column에 포함 시킨다.
					tmpStr.Format("%s,", strAuxName);

					strAuxColumn += tmpStr;
					if(i==0)
					{
						strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}
					else
					{
						strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}

	
					fltAuxVoltMax = max(fltTmpValue, fltAuxVoltMax);
					fltAuxVoltMin = min(fltTmpValue, fltAuxVoltMin);

					strAuxValuePre = strAuxValue;
					
					if(nIAuxV==(nIAuxVCnt+3))
					{
						tmpStr.Format("Aux Volt. Max,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxVoltMax);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux Volt. Min,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxVoltMin);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux Volt. Dev,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxVoltMax-fltAuxVoltMin);
						strAuxValuePre = strAuxValue;
					}	
				}
				break;
			}
		case 2:
			{
				strAuxName.Format("[%003d]%s(%s)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_TEMPTH, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

				nIAuxTHCnt++;
				if(nIAuxTH>0)
				{
					fltAuxTempTHMin=fltAuxTempTHMax=0;
					//이름을 Column에 포함 시킨다.
					tmpStr.Format("%s,", strAuxName);
					strAuxColumn += tmpStr;
					if(i==0)
					{
						strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}
					else
					{
						strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}
					strAuxValuePre = strAuxValue;
					
					fltAuxTempTHMax = max(fltTmpValue, fltAuxTempTHMax);
					fltAuxTempTHMin = min(fltTmpValue, fltAuxTempTHMin);
					
					if(nIAuxTH==(nIAuxTHCnt+3))
					{
						tmpStr.Format("Aux TempTH. Max,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempTHMax);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux TempTH. Min,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempTHMin);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux TempTH. Dev,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempTHMax-fltAuxTempTHMin);
						strAuxValuePre = strAuxValue;
					}	
				}
				break;
			}
		case PS_AUX_TYPE_HUMIDITY: //ksj 20200116
			{
				strAuxName.Format("[%003d]%s(%s)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_HUMI, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

				nIAuxHumiCnt++;
				if(nIAuxHumi>0)
				{
					fltAuxHumiMin=fltAuxHumiMax=0;
					//이름을 Column에 포함 시킨다.
					tmpStr.Format("%s,", strAuxName);
					strAuxColumn += tmpStr;
					if(i==0)
					{
						strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}
					else
					{
						strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
						fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
					}
					strAuxValuePre = strAuxValue;

					fltAuxHumiMax = max(fltTmpValue, fltAuxHumiMax);
					fltAuxHumiMin = min(fltTmpValue, fltAuxHumiMin);
					
					if(nIAuxHumi==(nIAuxHumiCnt+3))
					{
						tmpStr.Format("Aux Humi. Max,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxHumiMax);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux Humi. Min,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxHumiMin);
						strAuxValuePre = strAuxValue;
						tmpStr.Format("Aux Humi. Dev,");
						strAuxColumn += tmpStr;
						strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxHumiMax-fltAuxHumiMin);
						strAuxValuePre = strAuxValue;
					}	
				}
				break;
			}
		}
	}

	strLine += ",";
	strLine += strAuxColumn;

	//최종 파일을 갱신한다.
	//Module, Channel, StartTime, EndTime, StepNo, StepType, StateCode, StepTime, TotalTime,
	//Voltage, Current, 
	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s",			
		m_nModuleID,														//ModuleNo
		sChData.chNo,														//ChNo
		t1.Format(),														//StartT
		t.Format(),															//EndT	
		sChData.chStepNo,													//StepNo
		::PSGetTypeMsg(sChData.chStepType),									//Type
		ValueString(sChData.chCode, PS_CODE),								//Code
		strStepTime,														//Step Time
		strTotalTime,														//Total Time
		ValueString(sChData.fVoltage, PS_VOLTAGE), 							//Voltage
		ValueString(sChData.fCurrent, PS_CURRENT), 							//Current
		strAuxValue															//AuxStrValue
		);



	CStdioFile file;

	TRY
	{
		if(file.Open(strCurTempFile, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate) == FALSE)
		{
			//WriteLog(strCurTempFile + " 파일을 열 수 없습니다.");
			WriteLog(strCurTempFile + Fun_FindMsg("CyclerChannel_SaveStepEndData_msg1","CYCLERCHANNEL"));//&&
			return;
		}
		
		file.ReadString(strLineBuffer);
		if(strLineBuffer != strLine || strLineBuffer == "")
		{
			file.WriteString(strLine);
			file.WriteString("\n");
		}
		file.SeekToEnd();				//끝 위치를 찾는다.
		
		file.WriteString(strDataString);
		file.WriteString("\n");
	}
	CATCH(CFileException, ex)
	{
		ex->ReportError();
		file.Close();
	}
	END_CATCH
		
	file.Close();
}

void CCyclerChannel::SaveStepEndCANData(CString strStartT, PS_STEP_END_RECORD sChData, PS_CAN_RAW_DATA sCANData)
{
	CString strCurTempFile, strDataString, strEndT;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");
	
	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;
		strCurTempFile.Format("%s\\%s_StepEndCAN.csv",  strDataSharedPath, m_strTestName);			//.csv
	}
	else
	{
		strCurTempFile.Format("%s\\%s_StepEndCAN_M%02dCH%02d.csv",  strDataSharedPath, m_strTestName, m_nModuleID, sChData.chNo);			//.csv
	}
	
	CMainFrame *pMain=(CMainFrame *)AfxGetMainWnd();
//	CCTSMonProDoc *pDoc=(CCTSMonProDoc *)pMain->GetActiveDocument();	//ljb 2011321 이재복 //////////

	//make end time
	COleDateTime t = COleDateTime::GetCurrentTime();
	t.ParseDateTime(strStartT);

	long lDay = (long)sChData.fTotalTime_Day;		//ljb 20131208 add
	float fTotTime = sChData.fTotalTime;

	//long lDay = (long)fTotTime/86400;		
	//long lHour = (long)fTotTime%86400;
	long lHour = (long)fTotTime;
	long lMinute = lHour % 3600;		lHour = lHour / 3600;				
	long lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime(lDay, lHour, lMinute, lSecond);
	t = t+rTime;
	TRACE("%d Day %d:%d:%d\n", lDay, lHour, lMinute, lSecond);

	//make step start time
	COleDateTime t1 = COleDateTime::GetCurrentTime();
	t1.ParseDateTime(strStartT);
	fTotTime = sChData.fTotalTime-sChData.fStepTime;
	if(fTotTime < 0.0f)	fTotTime = 0.0f;
	//lDay = (long)fTotTime/86400;		
	lDay = (long)sChData.fTotalTime_Day - (long)sChData.fStepTime_Day;		//ljb 20131208 add
	//lHour = (long)fTotTime%86400;
	lHour = (long)fTotTime;
	lMinute = lHour % 3600;		lHour = lHour / 3600;				
	lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime1(lDay, lHour, lMinute, lSecond);
	t1 = t1+rTime1;

	TRACE("Step End ========== StartTime : %s, EndTime : %s\n", t1.Format(), t.Format());

//	CCyclerModule *lpModule;
//	CCyclerChannel *lpChannelInfo;

	//ljb 20140110 start
	CString strStepTime;
	if (sChData.fStepTime_Day > 0)
		strStepTime.Format("%.0fD %s", sChData.fStepTime_Day,ValueString(sChData.fStepTime, PS_STEP_TIME));
	else
		strStepTime.Format("%s",ValueString(sChData.fStepTime, PS_STEP_TIME));

	CString strTotalTime;
	if (sChData.fTotalTime_Day > 0)
		strTotalTime.Format("%.0fD %s", sChData.fTotalTime_Day,ValueString(sChData.fTotalTime, PS_STEP_TIME));
	else
		strTotalTime.Format("%s",ValueString(sChData.fTotalTime, PS_STEP_TIME));
	//ljb 20140110 end

	CString strLineBuffer, strLine;
	strLine = TABLE_FILE_COLUMN_HEAD5; 

	//yulee 20181124
	//1. 컬럼을 세팅된 숫자 만큼 
	//			CAN이 세팅된 만큼 컬럼을 추가한다.
	//2. 데이터에도 Aux 숫자에 맞추서 포함 시킨다. 
	//			데이터도 숫자에 맞춰 추가한다.
	int nIAuxT=0, nIAuxV=0, nIAuxTH=0; 
	CString strCANName;
	//<sCANData.canCount

	int nCANCnt=0;
	CString tmpStr, strTmpCANValue, strCANValue, strCANValuePre, strCANColumn = "";
	//FLOAT fltTmpValue;
	strCANValue.Format("");

	int nType=1;//(CAN)
	for(int i=0; i<sCANData.canCount; i++)
	{
		if(sCANData.canCount > 0)
		{
			//이름을 Column에 포함 시킨다.
			CString strCANTitle;
			strCANTitle = getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType);
			strCANTitle.TrimLeft();
			tmpStr.Format("[%003d]CAN(%s),", i+1, strCANTitle);
			strCANColumn += tmpStr;
			if(i == 0)
			{
// 				strTmpCANValue.Format("%f,", sCANData.canData[i].canVal.fVal);
// 				strCANValue = strTmpCANValue;

				//ksj 20200818 : CAN 값 오류 수정. 모니터링 화면과 동일하게 저장하도록 변경. 향후 데이터 타입에 따라 저장방식 분기 필요할 수 있음.
				strTmpCANValue.Format("%.6f,", sCANData.canData[i].canVal.fVal[0]);
				strTmpCANValue.TrimRight("0");
				strTmpCANValue.TrimRight(".");
				strCANValue = strTmpCANValue;
			}
			else
			{
// 				strTmpCANValue.Format("%s%f,", strCANValue, sCANData.canData[i].canVal.fVal);
// 				strCANValue = strTmpCANValue;

				//ksj 20200818 : CAN 값 오류 수정. 모니터링 화면과 동일하게 저장하도록 변경. 향후 데이터 타입에 따라 저장방식 분기 필요할 수 있음.
				strTmpCANValue.Format("%s%.6f,", strCANValue, sCANData.canData[i].canVal.fVal[0]);
				strTmpCANValue.TrimRight("0");
				strTmpCANValue.TrimRight(".");
				strCANValue = strTmpCANValue;
			}
		}
	}


	strLine += ",";
	strLine += strCANColumn;

	//최종 파일을 갱신한다.
	//Module, Channel, StartTime, EndTime, StepNo, StepType, StateCode, StepTime, TotalTime,
	//Voltage, Current, 
	strDataString.Format("%d,%d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s",			
		m_nModuleID,														//ModuleNo
		sChData.chNo,														//ChNo
		t1.Format(),														//StartT
		t.Format(),															//EndT	
		sChData.chStepNo,													//StepNo
		::PSGetTypeMsg(sChData.chStepType),									//Type
		ValueString(sChData.chCode, PS_CODE),								//Code
		strStepTime,														//Step Time
		strTotalTime,														//Total Time
		ValueString(sChData.fVoltage, PS_VOLTAGE), 							//Voltage
		ValueString(sChData.fCurrent, PS_CURRENT), 							//Current
		strCANValue															//CANStrValue
		);



	CStdioFile file;

	TRY
	{
		if(file.Open(strCurTempFile, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate) == FALSE)
		{
			//WriteLog(strCurTempFile + " 파일을 열 수 없습니다.");
			WriteLog(strCurTempFile + Fun_FindMsg("CyclerChannel_SaveStepEndCANData_msg1","CYCLERCHANNEL"));//&&
			return;
		}
		
		file.ReadString(strLineBuffer);
		if(strLineBuffer != strLine || strLineBuffer == "")
		{
			file.WriteString(strLine);
			file.WriteString("\n");
		}
		file.SeekToEnd();				//끝 위치를 찾는다.
		
		file.WriteString(strDataString);
		file.WriteString("\n");
	}
	CATCH(CFileException, ex)
	{
		ex->ReportError();
		file.Close();
	}
	END_CATCH
		
	file.Close();
}

//ksj 20200521 : 하나의 파일에 raw, aux, can Data 모두 포함 하여 저장.
void CCyclerChannel::SaveStepEndDataMerge(CString strStartT, PS_STEP_END_RECORD sChData, PS_AUX_RAW_DATA sAuxData, PS_CAN_RAW_DATA sCANData)

//ksj 20201013 : 스텝엔드 구조체 변경
//스텝의 시작시간 종료시간 계산 방식에 문제가 있어, 실제 스텝 종료시간 정보를 추가로 얻어오기 위해 구조체 변경
//스텝 시작 종료시간을 스텝종료시간 - 스텝시간으로 계산한다.
//void CCyclerChannel::SaveStepEndDataMerge(CString strStartT, PS_STEP_END_RECORD_EXTRA sChDataExtra, PS_AUX_RAW_DATA sAuxData, PS_CAN_RAW_DATA sCANData)
{
	CString strCurTempFile, strDataString, strEndT;
	CString strDataStringAux, strDataStringCan;
	CString strDataSharedPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Share Path", "");

	//ksj 20201013
	/*PS_STEP_END_RECORD sChData;
	memcpy(&sChData,&sChDataExtra.stEndRecord,sizeof(PS_STEP_END_RECORD)); //기존 스텝엔드 정보를 그대로 생성.*/
	//ksj end
	
	if(strDataSharedPath.IsEmpty())
	{
		strDataSharedPath = m_strFilePath;
		strCurTempFile.Format("%s\\%s_StepEndTot.csv",  strDataSharedPath, m_strTestName);			//.csv
	}
	else
	{
		strCurTempFile.Format("%s\\%s_StepEndTot_M%02dCH%02d.csv",  strDataSharedPath, m_strTestName, m_nModuleID, sChData.chNo);			//.csv
	}

	CMainFrame *pMain=(CMainFrame *)AfxGetMainWnd();
	//	CCTSMonProDoc *pDoc=(CCTSMonProDoc *)pMain->GetActiveDocument();	//ljb 2011321 이재복 //////////

	//make end time
	/*COleDateTime t = COleDateTime::GetCurrentTime();
	t.ParseDateTime(strStartT);

	long lDay = (long)sChData.fTotalTime_Day;		//ljb 20131208 add
	float fTotTime = sChData.fTotalTime;

	long lHour = (long)fTotTime;
	long lMinute = lHour % 3600;		lHour = lHour / 3600;				
	long lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime(lDay, lHour, lMinute, lSecond);
	t = t+rTime;
	TRACE("%d Day %d:%d:%d\n", lDay, lHour, lMinute, lSecond);*/

	//make step start time
	/*COleDateTime t1 = COleDateTime::GetCurrentTime();
	t1.ParseDateTime(strStartT);
	fTotTime = sChData.fTotalTime-sChData.fStepTime;
	if(fTotTime < 0.0f)	fTotTime = 0.0f;

	lDay = (long)sChData.fTotalTime_Day - (long)sChData.fStepTime_Day;		//ljb 20131208 add
	lHour = (long)fTotTime;
	lMinute = lHour % 3600;		lHour = lHour / 3600;				
	lSecond = lMinute % 60;
	lMinute = lMinute / 60;
	COleDateTimeSpan rTime1(lDay, lHour, lMinute, lSecond);
	t1 = t1+rTime1;*/

	//ksj 20201014 :
	//스텝 시작시간 종료시간 다시 만들어야 한다
	//시험 시작시간(PC기준파일생성시간)에 오차가 있고, 토탈타임과 스텝타임은 일시정지 시간을 포함하고 있지 않아서 오차가 크다.
	//기존 스텝 시작시간 : 시험 시작시간 + (토탈타임-스텝타임) 
	//기존 스텝 종료시간 : 시험 시작시간 + 토탈타임
	//
	//신규 스텝 시작시간 : 싱크타임 - 스텝타임 (여전히 스텝타임에 일시정지등이 포함되어 있지 않아 시작 시간은 오차 있음)
	//신규 스텝 종료시간 : 싱크타임
	//	

	//스텝 엔드 타임 생성. (SBC 싱크타임을 그대로 쓴다)
	CString strT;
	CString strTemp;
	COleDateTime timeStepEnd = COleDateTime::GetCurrentTime();
	COleDateTime timeStepStart = COleDateTime::GetCurrentTime();

	strT.Format("%d",sChData.lSyncDate);
	if (strT.GetLength() >= 8 ) strTemp.Format("%s-%s-%s %s", strT.Left(4), strT.Mid(4, 2), strT.Right(2), ValueString(((double)((int)sChData.fSyncTime)), PS_SYNC_TIME)); //소수점 아래 값 버리도록 수정. 간혹 59.88초 등 반올림이 일어나는 경우 60초가되어, 오류 발생
	else strTemp ="";

	BOOL bTimeParsingRes = timeStepEnd.ParseDateTime(strTemp); //종료시간

	//시작시간 계산 (종료시간 - 스텝시간) 부정확.
	long lDay = (long)sChData.fStepTime_Day;
	long lHour = (long)sChData.fStepTime;
	long lMinute = lHour % 3600; 
	lHour = lHour / 3600;				
	long lSecond = lMinute % 60;
	lMinute = lMinute / 60;

	if(bTimeParsingRes)
	{
		COleDateTimeSpan stepTime(lDay, lHour, lMinute, lSecond);
		timeStepStart = timeStepEnd-stepTime; //종료시간 - 스텝시간
	}
	TRACE("Step End ========== StartTime : %s, EndTime : %s\n", timeStepStart.Format(), timeStepEnd.Format());
	//ksj end/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//TRACE("Step End ========== StartTime : %s, EndTime : %s\n", t1.Format(), t.Format());

	//	CCyclerModule *lpModule;
	//	CCyclerChannel *lpChannelInfo;

	//ljb 20140110 start
	CString strStepTime;
	if (sChData.fStepTime_Day > 0)
		strStepTime.Format("%.0fD %s", sChData.fStepTime_Day,ValueString(sChData.fStepTime, PS_STEP_TIME));
	else
		strStepTime.Format("%s",ValueString(sChData.fStepTime, PS_STEP_TIME));

	CString strTotalTime;
	if (sChData.fTotalTime_Day > 0)
		strTotalTime.Format("%.0fD %s", sChData.fTotalTime_Day,ValueString(sChData.fTotalTime, PS_STEP_TIME));
	else
		strTotalTime.Format("%s",ValueString(sChData.fTotalTime, PS_STEP_TIME));
	//ljb 20140110 end

	CString strLineBuffer, strLine;
	//strLine = TABLE_FILE_COLUMN_HEAD6;
	strLine.Format("채널,작업명,스케쥴명,Step,Type,코드,시작시간(종료-스텝시간),종료시간,총시간,스텝시간,총 Cycle,현재 Cycle,전압(%s),전류(%s),용량(%s),충전용량(%s),방전용량(%s),Power(%s),WattHour(%s),충전WattHour(%s),방전WattHour(%s),평균전압(%s),평균전류(%s),IR(%s)",
					m_UnitTrans.GetUnitString(PS_VOLTAGE),
					m_UnitTrans.GetUnitString(PS_CURRENT),
					m_UnitTrans.GetUnitString(PS_CAPACITY),
// 					m_UnitTrans.GetUnitString(PS_CHARGE_CAP),
// 					m_UnitTrans.GetUnitString(PS_DISCHARGE_CAP),
					m_UnitTrans.GetUnitString(PS_CAPACITY),
 					m_UnitTrans.GetUnitString(PS_CAPACITY),
					m_UnitTrans.GetUnitString(PS_WATT),
					m_UnitTrans.GetUnitString(PS_WATT_HOUR),
					m_UnitTrans.GetUnitString(PS_WATT_HOUR),
					m_UnitTrans.GetUnitString(PS_WATT_HOUR),
					m_UnitTrans.GetUnitString(PS_VOLTAGE),
					m_UnitTrans.GetUnitString(PS_VOLTAGE),
					m_UnitTrans.GetUnitString(PS_IMPEDANCE)	
		);


	//최종 파일을 갱신한다.
	//대전/오창 LGC 요청 항목들이므로, 혹시 다른 고객사 요구나 다른 담당자 요구로 변경하는 경우 주의 요함.
	//raw 데이터 생성
	/*strDataString.Format("%d,%s,%s,%s,%s,%s,%s",		
		sChData.nCurrentCycleNum,											//Cycle No
		::PSGetTypeMsg(sChData.chStepType),									//Type	
		t.Format(),															//EndT						
		ValueString(sChData.fVoltage, PS_VOLTAGE), 							//Voltage
		ValueString(sChData.fCurrent, PS_CURRENT), 							//Current
		ValueString(sChData.fCapacity, PS_CAPACITY),						//Capacity		
		ValueString(sChData.fWattHour, PS_WATT_HOUR)						//WattHour		
		);*/

	strDataString.Format("M%02dCh%02d,%s,%s,%d,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",		
		m_nModuleID,														//ModuleNo
		sChData.chNo,														//ChNo
		GetTestName(),
		GetScheduleName(),
		sChData.chStepNo,													//StepNo
		::PSGetTypeMsg(sChData.chStepType),									//Type
		ValueString(sChData.chCode, PS_CODE),								//Code
// 		t1.Format(),														//StartT
// 		t.Format(),															//EndT			
		timeStepStart.Format(), //ksj 20201014
		timeStepEnd.Format(), //ksj 20201014
		strTotalTime,
		strStepTime,
		sChData.nTotalCycleNum,
		sChData.nCurrentCycleNum,
		ValueString(sChData.fVoltage, PS_VOLTAGE), 							//Voltage
		ValueString(sChData.fCurrent, PS_CURRENT), 							//Current
		ValueString(sChData.fCapacity, PS_CAPACITY),						//Capacity
// 		ValueString(sChData.fChargeAh, PS_CHARGE_CAP),						//CharCap
// 		ValueString(sChData.fDisChargeAh, PS_DISCHARGE_CAP),					//DischarCap
		ValueString(sChData.fChargeAh, PS_CAPACITY),						//CharCap
		ValueString(sChData.fDisChargeAh, PS_CAPACITY),					//DischarCap
		ValueString(sChData.fWatt, PS_WATT),								//Power
		ValueString(sChData.fWattHour, PS_WATT_HOUR),						//WattHour
		ValueString(sChData.fChargeWh, PS_WATT_HOUR),						//WattHour
		ValueString(sChData.fDisChargeWh, PS_WATT_HOUR),					//WattHour		
		//ValueString(sChData.fAvgVoltage, PS_AVG_VOLTAGE),					//AvgVoltage
		//ValueString(sChData.fAvgCurrent, PS_AVG_CURRENT),					//AvgCurrent
		ValueString(sChData.fAvgVoltage, PS_VOLTAGE),					//AvgVoltage
		ValueString(sChData.fAvgCurrent, PS_VOLTAGE),					//AvgCurrent
		ValueString(sChData.fImpedance, PS_IMPEDANCE)						//IR		
		);

	//AUX 데이터 생성
	if(sAuxData.auxCount > 0)
	{
		int nIAuxT=0, nIAuxV=0, nIAuxTH=0; 
		int nIAuxHumi=0; //ksj 20200116
		CString strAuxName;
		for(int i=0; i<sAuxData.auxCount; i++)
		{
			switch(sAuxData.auxData[i].chType)
			{
			case PS_AUX_TYPE_TEMPERATURE:
				{
					nIAuxT++;
					continue;
				};
			case PS_AUX_TYPE_VOLTAGE:
				{
					nIAuxV++;
					continue;
				};
			case PS_AUX_TYPE_TEMPERATURE_TH:
				{
					nIAuxTH++;
					continue;
				};
			case PS_AUX_TYPE_HUMIDITY: //ksj 20200116
				{
					nIAuxHumi++;
					continue;
				};
			default:
				;
			}
		}

		//int nIAuxColumnCnt;
		int nAuxCnt, nIAuxTCnt=0, nIAuxVCnt=0, nIAuxTHCnt=0;
		int nIAuxHumiCnt=0;//ksj 20200116
		CString tmpStr, strAuxValue, strAuxValuePre, strAuxColumn = "";
		FLOAT fltTmpValue, fltAuxTempMin, fltAuxTempMax, fltAuxVoltMin, fltAuxVoltMax, fltAuxTempTHMin, fltAuxTempTHMax;
		CString strAuxVValue,strAuxTValue,strAuxTHValue,strAuxHValue;
		FLOAT fltAuxHumiMin, fltAuxHumiMax; //ksj 20200116
		strAuxValue.Format("");
		nAuxCnt = sAuxData.auxCount;

		if(nIAuxT > 0)
			nIAuxT += 3;
		if(nIAuxV > 0)
			nIAuxV += 3;
		if(nIAuxTH > 0)
			nIAuxTH += 3;
		if(nIAuxHumi > 0) //ksj 20200116
			nIAuxHumi += 3;

		int nType = 0;//(Aux)

		for(int i=0; i<sAuxData.auxCount; i++)
		{				
			switch(sAuxData.auxData[i].chType)
			{
			case 0:
				{
					strAuxName.Format("[%003d]%s(%s)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_TEMP, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

					nIAuxTCnt++;
					if(nIAuxT>0)
					{
						fltAuxTempMin=fltAuxTempMax=0;
						//이름을 Column에 포함 시킨다.
						tmpStr.Format("%s,", strAuxName);
						strAuxColumn += tmpStr;
						if(i==0)
						{
							strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
						else
						{
							strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
						fltAuxTempMax = max(fltTmpValue, fltAuxTempMax);
						fltAuxTempMin = min(fltTmpValue, fltAuxTempMin);

						strAuxValuePre = strAuxValue;							

						/*if(nIAuxT==(nIAuxTCnt+3))
						{
							tmpStr.Format("Aux Temp. Max,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempMax);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux Temp. Min,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempMin);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux Temp. Dev,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempMax-fltAuxTempMin);
							strAuxValuePre = strAuxValue;
						}	*/
					}
					break;
				}
			case 1:
				{
					strAuxName.Format("[%003d]%s(%s)(V)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_VOLT, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

					nIAuxVCnt++;
					if(nIAuxV>0)
					{
						fltAuxVoltMin=fltAuxVoltMax=0;
						//이름을 Column에 포함 시킨다.
						tmpStr.Format("%s,", strAuxName);

						strAuxColumn += tmpStr;
						if(i==0)
						{
							strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
						else
						{
							strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
								
						fltAuxVoltMax = max(fltTmpValue, fltAuxVoltMax);
						fltAuxVoltMin = min(fltTmpValue, fltAuxVoltMin);

						strAuxValuePre = strAuxValue;

						/*if(nIAuxV==(nIAuxVCnt+3))
						{
							tmpStr.Format("Aux Volt. Max,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxVoltMax);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux Volt. Min,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxVoltMin);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux Volt. Dev,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxVoltMax-fltAuxVoltMin);
							strAuxValuePre = strAuxValue;
						}	*/
					}
					break;
				}
			case 2:
				{
					strAuxName.Format("[%003d]%s(%s)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_TEMPTH, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

					nIAuxTHCnt++;
					if(nIAuxTH>0)
					{
						fltAuxTempTHMin=fltAuxTempTHMax=0;
						//이름을 Column에 포함 시킨다.
						tmpStr.Format("%s,", strAuxName);
						strAuxColumn += tmpStr;
						if(i==0)
						{
							strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
						else
						{
							strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
						strAuxValuePre = strAuxValue;

						fltAuxTempTHMax = max(fltTmpValue, fltAuxTempTHMax);
						fltAuxTempTHMin = min(fltTmpValue, fltAuxTempTHMin);
								
						/*if(nIAuxTH==(nIAuxTHCnt+3))
						{
							tmpStr.Format("Aux TempTH. Max,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempTHMax);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux TempTH. Min,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempTHMin);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux TempTH. Dev,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxTempTHMax-fltAuxTempTHMin);
							strAuxValuePre = strAuxValue;
						}	*/
					}
					break;
				}
			case PS_AUX_TYPE_HUMIDITY: //ksj 20200116
				{
					strAuxName.Format("[%003d]%s(%s)('C)", sAuxData.auxData[i].chAuxNo, RS_COL_AUX_HUMI, getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType));

					nIAuxHumiCnt++;
					if(nIAuxHumi>0)
					{
						fltAuxHumiMin=fltAuxHumiMax=0;
						//이름을 Column에 포함 시킨다.
						tmpStr.Format("%s,", strAuxName);
						strAuxColumn += tmpStr;
						if(i==0)
						{
							strAuxValue.Format("%f,", (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
						else
						{
							strAuxValue.Format("%s%f,", strAuxValuePre, (sAuxData.auxData[i].fValue)/1000);
							fltTmpValue = (sAuxData.auxData[i].fValue)/1000;
						}
						strAuxValuePre = strAuxValue;

						fltAuxHumiMax = max(fltTmpValue, fltAuxHumiMax);
						fltAuxHumiMin = min(fltTmpValue, fltAuxHumiMin);
								
						/*if(nIAuxHumi==(nIAuxHumiCnt+3))
						{
							tmpStr.Format("Aux Humi. Max,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxHumiMax);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux Humi. Min,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxHumiMin);
							strAuxValuePre = strAuxValue;
							tmpStr.Format("Aux Humi. Dev,");
							strAuxColumn += tmpStr;
							strAuxValue.Format("%s%.1f,", strAuxValuePre, fltAuxHumiMax-fltAuxHumiMin);
							strAuxValuePre = strAuxValue;
						}	*/
					}
					break;
				}
					
			}				
						
		}

		strLine += ",";
		strLine += strAuxColumn;	

		strDataStringAux.Format(",%s",strAuxValue);
	}
	
	//CAN 데이터 생성
	if(sCANData.canCount > 0)
	{
		int nIAuxT=0, nIAuxV=0, nIAuxTH=0; 
		CString strCANName;
		//<sCANData.canCount

		int nCANCnt=0;
		CString tmpStr, strTmpCANValue, strCANValue, strCANValuePre, strCANColumn = "";
		//FLOAT fltTmpValue;
		strCANValue.Format("");

		int nType=1;//(CAN)
		for(int i=0; i<sCANData.canCount; i++)
		{
			if(sCANData.canCount > 0)
			{
				//이름을 Column에 포함 시킨다.
				CString strCANTitle;
				strCANTitle = getAuxCANTitleFromTLT(strDataSharedPath, m_strTestName , m_nModuleID, i, nType);
				strCANTitle.TrimLeft();
				tmpStr.Format("[%003d]CAN(%s),", i+1, strCANTitle);
				strCANColumn += tmpStr;
				if(i == 0)
				{
					strTmpCANValue.Format("%f,", sCANData.canData[i].canVal.fVal);
					strCANValue = strTmpCANValue;
				}
				else
				{
					strTmpCANValue.Format("%s%f,", strCANValue, sCANData.canData[i].canVal.fVal);
					strCANValue = strTmpCANValue;
				}
			}
		}


		strLine += ",";
		strLine += strCANColumn;

		strDataStringCan.Format(",%s",strCANValue);
	}

	CStdioFile file;
	CString strDataOut;
	TRY
	{
		if(file.Open(strCurTempFile, CFile::modeCreate|CFile::modeReadWrite|CFile::modeNoTruncate) == FALSE)
		{
			//WriteLog(strCurTempFile + " 파일을 열 수 없습니다.");
			WriteLog(strCurTempFile + Fun_FindMsg("CyclerChannel_SaveStepEndData_msg1","CYCLERCHANNEL"));//&&
			return;
		}

		file.ReadString(strLineBuffer);
		if(strLineBuffer != strLine || strLineBuffer == "")
		{
			file.WriteString(strLine);
			file.WriteString("\n");
		}
		file.SeekToEnd();				//끝 위치를 찾는다.

		strDataOut.Format("%s%s%s",strDataString,strDataStringAux,strDataStringCan);
		file.WriteString(strDataOut);
		file.WriteString("\n");
	}
	CATCH(CFileException, ex)
	{
		ex->ReportError();
		file.Close();
	}
	END_CATCH

	file.Close();
}

int CCyclerChannel::GetFindCharIndexCount(CString parm_string, char parm_find_char ,int parm_find_index) 
{ 
	int length = parm_string.GetLength(), find_index = 0 , find_count = 0; 

	for(int i = 0; i < length; i++)
	{ 
		if(parm_string[i] == parm_find_char)
		{
			if(find_index == parm_find_index)
			{
				return find_count;
			}
			find_index++;
		}
		find_count++; 
	} 
	return find_count; 
} 

CString CCyclerChannel::getAuxCANTitleFromTLT(CString strTestFilePath, CString strTestName, int ModuleID, int iCANLineNo, int nTypeOper)
{
	//cny------------------------
	//return _T("");
    //------------------------
	int nILine=0;
	CString strTemp, strLine, strRetPars;
	if(nTypeOper == 0)
	{
		strTemp.Format("%s\\%sAUX.%s", strTestFilePath, strTestName, "tlt"); 
	}
	else if(nTypeOper == 1)
	{
		strTemp.Format("%s\\%sCAN.%s", strTestFilePath, strTestName, "tlt"); 
	}
	//--------------------------
	CFileFind finder;
	BOOL bWork=finder.FindFile(strTemp);		
	finder.Close();	
	if(bWork==FALSE) return _T("");
	//--------------------------

	CFile file;
	file.Open(strTemp, CFile::modeRead);	
	CArchive ar(&file, CArchive::load);

	while(ar.ReadString(strLine))
	{
		if(strLine.GetLength() == 0)
		{
			strRetPars.Format("Error");
			TRACE(strRetPars);
			return strRetPars;
		}
		else
		{
			TRACE(strLine);
			if(nILine == iCANLineNo)
			{
				if(nTypeOper == 0)
				{
					AfxExtractSubString(strRetPars, strLine, 2, ',');
				}
				else if(nTypeOper == 1)
				{
					AfxExtractSubString(strRetPars, strLine, 3, ',');
				}

				break;
			}
		}
		nILine++;
	}

	ar.Close();
	file.Close();

	return strRetPars;
}


//20110319 KHS
CString CCyclerChannel::ValueString(double dData, int item, BOOL bUnit)
{
	return m_UnitTrans.ValueString(dData, item, bUnit);
}

//20120210 KHS  시간 Value만 Convert
CString CCyclerChannel::TimeValueString(long lData)
{
	CString strMsg;
	long Value = lData;
	//double dTemp;
	//char szBuff[32];
	//int nTimeUnit;
	
	// 	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Time Unit", "0"));
	// 	nTimeUnit = atol(szBuff);
	
	//sbc에서 10msec단위 전송되어 온다.
	CTimeSpan timeSpan((ULONG)PCTIME(Value));
	// 	if(nTimeUnit == 1)	//sec 표시
	// 	{
	// 		strMsg.Format("%.1f", PCTIME(Value));
	// 	}
	// 	else if(nTimeUnit == 2)	//Minute 표시
	// 	{
	// 		strMsg.Format("%.2f", PCTIME(Value)/60.0f);
	// 	}
	// 	else
	// 	{
	if(timeSpan.GetDays() > 0)
	{
		strMsg =  timeSpan.Format("%Dd %H:%M:%S");
	}
	else
	{
		strMsg = timeSpan.Format(" %H:%M:%S");
	}
	//	}
	
	return strMsg;
}

SFT_AUX_DATA * CCyclerChannel::GetAuxData()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetAuxData();
}
int	CCyclerChannel::GetAuxCount()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetAuxCount();
}


BOOL CCyclerChannel::CreateAuxFileA(CString strFileName, int nFileIndex, int nAuxVoltCount, int nAuxTempCount)
{
	CFile f;
	CFileException e;
	
	if(strFileName.IsEmpty())	return FALSE;
	
	if( !f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		return FALSE;
	}
	
	LPPS_FILE_ID_HEADER pFileHeader = new PS_FILE_ID_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	pFileHeader->nFileID = PS_PNE_AUX_FILE_ID;
	pFileHeader->nFileVersion = _PNE_AUX_FILE_VER;
	CTime t = CTime::GetCurrentTime();
	sprintf(pFileHeader->szCreateDateTime, "%s", t.Format("%y/%m/%d %H:%M:%S"));
	sprintf(pFileHeader->szDescrition, "%s", "PNE aux data file.");
	
	f.Write(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	delete pFileHeader;
	
	
	//2. Aux File Header 생성
	PS_AUX_DATA_FILE_HEADER fileAuxSetHeader;
	ZeroMemory(&fileAuxSetHeader, sizeof(PS_AUX_DATA_FILE_HEADER));

	//fileAuxSetHeader.nColumnCount = nAuxVoltCount + nAuxTempCount+ GetAuxTempThCount();  //ljb 20160707 add
	fileAuxSetHeader.nColumnCount = nAuxVoltCount + nAuxTempCount+ GetAuxTempThCount() + GetAuxHumiThCount();  //ksj 20200116
	TRACE("Aux ColumnCount : %d\r\n", fileAuxSetHeader.nColumnCount);
	
	//fileAuxSetHeader.awColumnItem[0] = PS_AUX_OWNER_CH_NO;				//chNo
	
	int i;
	//for(int i = 0; i < nAuxTempCount + nAuxVoltCount+ GetAuxTempThCount(); i++)
	for(int i = 0; i < nAuxTempCount + nAuxVoltCount+ GetAuxTempThCount() + GetAuxHumiThCount(); i++) //ksj 20200116 : 확인 필요
	{
		if (i < nAuxTempCount)
			fileAuxSetHeader.awColumnItem[i] = PS_AUX_TEMPERATURE;						//aux Temp Value
		else
			fileAuxSetHeader.awColumnItem[i] = PS_AUX_VOLTAGE;						//aux Volt Value
	}
	
	f.Write(&fileAuxSetHeader, sizeof(PS_AUX_DATA_FILE_HEADER));
	
	f.Flush();
	f.Close();
	
	m_strDataList.RemoveAll();
	
	//WriteLog(strFileName+" 생성");
	WriteLog(strFileName+Fun_FindMsg("CyclerChannel_CreateAuxFileA_msg1","CYCLERCHANNEL"));//&&
	
	return TRUE;
}

//사용안함 - 20080104 KJH
float CCyclerChannel::GetAuxItemData(PS_AUX_RAW_DATA auxData, WORD wItem, int nAuxIndex)
{

	float fData = 0.0f;
	/*switch(wItem)
	{			
	case PS_AUX_OWNER_CH_NO:	fData = float(auxData.auxData[nAuxIndex].chNo); break;
	case PS_AUX_CHANNEL_NO:	fData = float(auxData.auxData[nAuxIndex].chAuxNo); break;
	case PS_AUX_TYPE:	fData = float(auxData.auxData[nAuxIndex].chType); break;
	case PS_AUX_VALUE:	fData = float(auxData.auxData[nAuxIndex].fValue); break;
	}*/

	return fData;
}

int CCyclerChannel::GetChFile(CFile & fRS, PS_RAW_FILE_HEADER & rawHeader)
{
	CString  strSaveFileName;	//ljb 20140429 저장 파일명	
	CFileStatus fs;
	CFileException e;
	
	//파일명 생성 
	m_strResultFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_RAW_FILE_NAME_EXT);
	strSaveFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_RAW_FILE_NAME_EXT);			//ljb 20140429 저장 파일명	
	
	//파일 존재 여부를 검사하여 없으면 생성한다.
	if( CFile::GetStatus(strSaveFileName, fs) == FALSE) 
	{
		//파일 없음
		if(CreateResultFileA(strSaveFileName) == FALSE)
		{
			TRACE("%s를 생성할 수 없습니다.", strSaveFileName);
			return -21 ;
		}
	}
	
	//파일 존재하면 용량 체크 해서 분할 Size 이상이면 새로 생성 한다.
	int nFileNo=0;
	long dFileSize=0;
	if( CFile::GetStatus(strSaveFileName, fs) == TRUE) {}
	
	//파일 있음 용량 체크
	dFileSize = fs.m_size;
	nFileNo = dFileSize / (1024 * ID_MAX_FILE_SIZE);		//File 분할 사이즈
	TRACE("File Size = %d \n",dFileSize);
	if (nFileNo == 0)
	{
		//파일을 Open 한다.
		if (fRS.m_hFile == CFile::hFileNull)
		{
			if( !fRS.Open( strSaveFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
			{
				#ifdef _DEBUG
					afxDump << "File could not be opened " << e.m_cause << "\n";
				#endif

				return -22;
			}
		}
		
		//skip file header
		//read record column set
		if( fRS.Read(&rawHeader, sizeof(PS_RAW_FILE_HEADER)) != sizeof(PS_RAW_FILE_HEADER))
		{
			fRS.Close();
			return -23;
		}
		
		fRS.SeekToEnd();
		return 0;
	}
	
	
	for (int iFileCount=1;iFileCount < 1000; iFileCount++)
	{
		strSaveFileName.Format("%s\\%s.%s%03d", m_strFilePath, m_strTestName,PS_RAW_FILE_NAME_EXT,iFileCount);	//ljb 20140429 File 분할 파일 이름
		if( CFile::GetStatus(strSaveFileName, fs) == FALSE) 
		{
			//파일 없음
			if(CreateResultFileA(strSaveFileName) == FALSE)
			{
				TRACE("%s를 생성할 수 없습니다.", strSaveFileName);
				return -21 ;
			}
		}
		if( CFile::GetStatus(strSaveFileName, fs) == TRUE) {}
		//파일 있음 용량 체크
		dFileSize = fs.m_size;
		nFileNo = dFileSize / (1024 * ID_MAX_FILE_SIZE);		//File 분할 사이즈
		TRACE("File Size = %d \n",dFileSize);
		if (nFileNo == 0) break;
	}
	
	//파일을 Open 한다.
	if (fRS.m_hFile == CFile::hFileNull)
	{
		if( !fRS.Open( strSaveFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
		{
			#ifdef _DEBUG
				afxDump << "File could not be opened " << e.m_cause << "\n";
			#endif

			return -22;
		}
	}
	
	//skip file header
	//read record column set
	if( fRS.Read(&rawHeader, sizeof(PS_RAW_FILE_HEADER)) != sizeof(PS_RAW_FILE_HEADER))
	{
		fRS.Close();
		return -23;
	}
	
	fRS.SeekToEnd();
	return 0;
}


int CCyclerChannel::GetAuxFile(CFile * pFile, PS_AUX_FILE_HEADER * auxHeader)
{
	CFileStatus	fAuxStatus;		
	CFileException e;

	if(GetTotalAuxCount() < 0 || GetTotalAuxCount() > _SFT_MAX_MAPPING_AUX)
 		return -11;

	CString  m_strAuxFileName;

	//m_strAuxFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_AUX_FILE_NAME_EXT);
	//파일명 생성 
	m_strAuxFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_AUX_FILE_NAME_EXT);

	//파일 존재 여부를 검사하여 없으면 생성한다.
	if(CFile::GetStatus(m_strAuxFileName, fAuxStatus) == FALSE)
	{
 		if(CreateAuxFileA(m_strAuxFileName, 0, GetAuxVoltCount(),GetAuxTempCount()) ==FALSE)
 		{
 			TRACE("%s를 생성할 수 없습니다.", m_strAuxFileName);
 			return -12;
 		}
	}

	//파일 존재하면 용량 체크 해서 분할 Size 이상이면 새로 생성 한다.
	int nFileNo=0;
	long dFileSize=0;
	if( CFile::GetStatus(m_strAuxFileName, fAuxStatus) == TRUE) {}
	
	//파일 있음 용량 체크
	dFileSize = fAuxStatus.m_size;
	nFileNo = dFileSize / (1024 * ID_MAX_FILE_SIZE);		//File 분할 사이즈
	TRACE("AUX File Size = %d \n",dFileSize);
	if (nFileNo == 0)
	{
		//파일을 Open 한다.
		if (pFile->m_hFile == CFile::hFileNull)
		{
 			if( !pFile->Open( m_strAuxFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
 			{
 			#ifdef _DEBUG
 				afxDump << "File could not be opened " << e.m_cause << "\n";
 			#endif
 				return -13;
 			}
		}
 		
 		//skip file header
 		
 		//read record column set
 		if( pFile->Read(auxHeader, sizeof(PS_AUX_FILE_HEADER)) != sizeof(PS_AUX_FILE_HEADER))
 		{
 			pFile->Close();
 			return -14;
 		}
 		pFile->SeekToEnd();
 		
 		return 0;
	}

	//ljb 20140508 add  start ///////////////////////////////////////////////////////////////////////////////////////
	for (int iFileCount=1;iFileCount < 1000; iFileCount++)
	{
		m_strAuxFileName.Format("%s\\%s.%s%03d", m_strFilePath, m_strTestName,PS_AUX_FILE_NAME_EXT,iFileCount);	//ljb 20140429 File 분할 파일 이름
		if( CFile::GetStatus(m_strAuxFileName, fAuxStatus) == FALSE) 
		{
			//파일 없음
			if(CreateAuxFileA(m_strAuxFileName, 0, GetAuxVoltCount(),GetAuxTempCount()) ==FALSE)
			{
				TRACE("%s를 생성할 수 없습니다.", m_strAuxFileName);
				return -12;
 			}
		}
		if( CFile::GetStatus(m_strAuxFileName, fAuxStatus) == TRUE) {}
		//파일 있음 용량 체크
		dFileSize = fAuxStatus.m_size;
		nFileNo = dFileSize / (1024 * ID_MAX_FILE_SIZE);		//File 분할 사이즈
		TRACE("File Size = %d \n",dFileSize);
		if (nFileNo == 0) break;
	}
	
	//파일을 Open 한다.
	if (pFile->m_hFile == CFile::hFileNull)
	{
		if( !pFile->Open( m_strAuxFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
		{
#ifdef _DEBUG
			afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
			return -13;
		}
	}
	
	//skip file header
	
	//read record column set
	if( pFile->Read(auxHeader, sizeof(PS_AUX_FILE_HEADER)) != sizeof(PS_AUX_FILE_HEADER))
	{
		pFile->Close();
		return -14;
	}
	pFile->SeekToEnd();
	
 		return 0;
	//ljb 20140508 add  end ///////////////////////////////////////////////////////////////////////////////////////
 }



//스케줄 시작시 시험 폴더안에 Aux Title 파일을 복사한다.
void CCyclerChannel::SetAuxTitleFile(CString strAuxPath, CString strTestName)
{
//  	char path[MAX_PATH];
//     GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
//  	CString strTemp(path);
	CString strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path"); //ksj 20210507
	CString strPath;
	strTemp += "\\";
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strAuxPath.Mid(strAuxPath.ReverseFind('\\')+1, 7), PS_AUX_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("%sAUX.%s", strTestName, "tlt"); 
	CString strNew = strAuxPath+"\\"+strTemp;

	
		
   	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return;

	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	if(CopyFile(strPath, strNew, TRUE) == FALSE)
	{
		AfxMessageBox(strTemp+" Data 복사에 실패했습니다.");//$1013
		return;
	}
	//WriteLog("Aux Title 파일 복사 완료");
	WriteLog(Fun_FindMsg("CyclerChannel_SetAuxTitleFile_msg1","CYCLERCHANNEL"));//&&
	
	//ksj 20200223 : aux 설정 정보를 채널로그에 기록한다.
	WriteAuxStateLog();
	//ksj end
}

void CCyclerChannel::SetParallel(BOOL bParallel, BOOL bMaster)
{
	m_bParallel = bParallel;
	m_bMaster = bMaster;

}

BOOL CCyclerChannel::IsParallel()
{
	return m_bParallel;
}

int CCyclerChannel::GetMasterChannelNum()
{
	return m_uMasterCh;
}

void CCyclerChannel::SetMasterChannelNum(unsigned char uMaster)
{
	m_uMasterCh = uMaster;
}

SFT_CAN_DATA * CCyclerChannel::GetCanData()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetCanData();
}

int CCyclerChannel::GetCanCount()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	return pChannel->GetCanCount();
}

BOOL CCyclerChannel::GetSaveDataC(PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA & sCanData)
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;
	
	SFT_VARIABLE_CH_DATA_V1004 sSaveChData;
	ZeroMemory(&sSaveChData, sizeof(SFT_VARIABLE_CH_DATA_V1004));
	if(pChannel->PopSaveData(sSaveChData)== FALSE)	return FALSE;
	//CString strLog;
	//strLog.Format("Pop : 채널번호 : %d, Aux Count : %d", sSaveChData.chData.chNo, sSaveChData.chData.nAuxCount);
	//WriteLog(strLog);
	
	sChData.chNo		= sSaveChData.chData.chNo;					// Channel Number
	sChData.chState		= sSaveChData.chData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType	= sSaveChData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect = sSaveChData.chData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		= sSaveChData.chData.chCode;
	sChData.chStepNo	= sSaveChData.chData.chStepNo;
	sChData.chGradeCode = sSaveChData.chData.chGradeCode;
	
	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.chData.lCurrent, PS_CURRENT);
	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.chData.lCapacity, PS_CAPACITY);
	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.chData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.chData.lWattHour, PS_WATT_HOUR);
	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.chData.ulStepTime, PS_STEP_TIME),			// 이번 Step 진행 시간
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.chData.ulTotalTime, PS_TOT_TIME),			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.chData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
	//sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.chData.lTemparature, PS_TEMPERATURE);
	sChData.fOvenTemperature = 0.0f;
	sChData.fOvenHumidity	 = 0.0f;
	
	sChData.fChillerRefTemp	 = 0.0f;
	sChData.fChillerCurTemp	 = 0.0f;
// 	sChData.iChillerRefPump	 = 0;
// 	sChData.iChillerCurPump	 = 0;
	sChData.fChillerRefPump	 = 0; //lyj 20201102 DES칠러 소숫점 
	sChData.fChillerCurPump	 = 0; //lyj 20201102 DES칠러 소숫점 
	sChData.dCellBALChData   = 0.0f;
	sChData.fPwrSetVoltage   = 0.0f;//yulee 20190514 cny
	sChData.fPwrCurVoltage   = 0.0f;
	
	sChData.nCurrentCycleNum = sSaveChData.chData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.chData.nTotalCycleNum;
	sChData.lSaveSequence	= sSaveChData.chData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.chData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.chData.lAvgCurrent, PS_CURRENT);
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;

	//TRACE("GetSaveDataC -> Capacity : %f, WattHour : %f\n",sChData.fCapacity, sChData.fWattHour);
	
	sAuxData.auxCount = sSaveChData.chData.nAuxCount;
	sAuxData.auxData->chNo = sSaveChData.chData.chNo;					// Channel Number
	for(int i = 0; i < sAuxData.auxCount; i++)
	{
		sAuxData.auxData[i].chNo = sSaveChData.chData.chNo;
		sAuxData.auxData[i].chAuxNo = sSaveChData.auxData[i].auxChNo;
		sAuxData.auxData[i].chType = sSaveChData.auxData[i].auxChType;
		if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TEMPERATURE);
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_VOLTAGE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_VOLTAGE);
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE_TH) //20180607 yulee 
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TYPE_TEMPERATURE_TH);
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : V1016 습도. 향후 단위 변환 체크 필요
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TYPE_HUMIDITY);


	}
	sCanData.canCount = sSaveChData.chData.nCanCount;
//	TRACE("Can Count : %d\r\n", sCanData.canCount);
	for(int i = 0 ; i < sCanData.canCount; i++)
	{
		sCanData.canData[i].chNo = sSaveChData.chData.chNo;
		sCanData.canData[i].canType	= sSaveChData.canData[i].canType;		
		memcpy(&sCanData.canData[i].canVal, &sSaveChData.canData[i].canVal, sizeof(CAN_VALUE));
	}
	
	return TRUE;
}

//2007/10/17 
//1. CAN 추가
//2. ITEM 개수 128 -> 온도 : 256, 전압 : 256, Master CAN : 256, Slave CAN : 256 으로 변경

int CCyclerChannel::CheckAndSaveData()
{
	//파일명 설정 여부 확인 
	if(m_strFilePath.IsEmpty())
	{
		return 0;
	}

	//외부에서 파일 조작을 할 경우file update를 Lock 시킨 후 update 실시하기 위함 
	if(m_bLockFileUpdate == TRUE)
	{
		TRACE("Ch %d file update is locked\n", m_nChannelIndex+1);
		return 0;
	}

	//저장할 Data가 없으면 return
	if(GetStackedSize() < 1)	return 0;

	m_bFileSaving = TRUE;			//20110109 KHS
	int nRtn = 1;

	//Add 2006/8/18
	//Load last temp data
	//////////////////////////////////////////////////////////////////////////
	PS_STEP_END_RECORD prevData;
	CString strStartTime, strTestSerial, strUser, strDescript;
	long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
	TRACE("Last data start index %d\n", lStartIndex);
	//////////////////////////////////////////////////////////////////////////

	//TRACE("Channel %d data save start\n", m_nChannelIndex+1);
	
	//2007/08/09 Aux Data 저장용//////////////////////////////////
	//채널 데이터가 저장하는 방식을 따른다.

	PS_AUX_RAW_DATA sAuxData;
	float *pfAuxBuff = NULL;
	size_t auxSize;
	size_t auxLineSize;
	CFile auxFile;
	PS_AUX_FILE_HEADER auxHeader;
	int nAuxColumnSize;
	//int  nAuxFileCount;
	//PS_AUX_RAW_DATA auxData;
	

	nRtn = GetAuxFile(&auxFile, &auxHeader);
	if(nRtn < 0)	
	{
		return nRtn;
	}
	
	nAuxColumnSize = auxHeader.auxHeader.nColumnCount;		//save data setting
	
	if(nAuxColumnSize < 0 || nAuxColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
	{
		CString strMsg;
		strMsg.Format("AuxColumn : %d", nAuxColumnSize);
		WriteLog(strMsg);
		auxFile.Close();
		return -1;
	}
		
	if(nAuxColumnSize > 0)
	{
		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
		auxLineSize =  sizeof(float)*nAuxColumnSize;
		pfAuxBuff = new float[nAuxColumnSize*_MAX_DISK_WIRTE_COUNT];
		auxSize = auxLineSize*_MAX_DISK_WIRTE_COUNT;
		
		//Line 수를 Count
		int nAuxBodySize = auxFile.GetLength()-sizeof(PS_AUX_FILE_HEADER);
		int nAuxRsCount = nAuxBodySize / auxLineSize ;
		if(nAuxRsCount < 0 || nAuxBodySize % auxLineSize)
		{
			//		TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
			auxFile.Close();
			return -2;
		}
	}
	
	//////////////////////////////////////////////////////////////
	//2007/10/17 CAN Data 저장용//////////////////////////////////
	//채널 데이터가 저장하는 방식을 따른다.

	PS_CAN_RAW_DATA sCanData;
	SFT_CAN_VALUE *pCanBuff = NULL;
	size_t canSize;
	size_t canLineSize;
	CFile canFile;
	PS_CAN_FILE_HEADER canHeader;
	int nCanColumnSize;
	//int  nCanFileCount;
	//PS_CAN_RAW_DATA canData;
	
	nRtn = GetCanFile(&canFile, &canHeader);
	if(nRtn < 0)	
	{
		return nRtn;
	}
	nCanColumnSize = canHeader.canHeader.nColumnCount;		//save data setting
	
	if(nCanColumnSize < 0 || nCanColumnSize > PS_MAX_CAN_SAVE_FILE_COLUMN)
	{
		CString strMsg;
		strMsg.Format("CanColumn : %d", nCanColumnSize);
		WriteLog(strMsg);
		canFile.Close();
		return -1;
	}
		
	if(nCanColumnSize > 0)
	{
		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
		canLineSize =  sizeof(SFT_CAN_VALUE)*nCanColumnSize;
		pCanBuff = new SFT_CAN_VALUE[nCanColumnSize*_MAX_DISK_WIRTE_COUNT];
		canSize = canLineSize*_MAX_DISK_WIRTE_COUNT;
		
		//Line 수를 Count
		int nCanBodySize = canFile.GetLength()-sizeof(PS_CAN_FILE_HEADER);
		int nCanRsCount = nCanBodySize / canLineSize ;
		if(nCanRsCount < 0 || nCanBodySize % canLineSize)
		{
			CString strMsg;
			strMsg.Format("CAN File Size Error (CanColumn : %d)", nCanColumnSize);
			WriteLog(strMsg);
			canFile.Close();
			return -2;
		}
	}
	
	//////////////////////////////////////////////////////////////
	
	//채널 데이터 저장용///////////////////////////////////////////////
	PS_RAW_FILE_HEADER rawHeader;
	CFile fRS;
	PS_STEP_END_RECORD	sChResultData;
	float *pfBuff;
	size_t rsSize, lineSize;

	nRtn = GetChFile(fRS,rawHeader);
	if(nRtn < 0)
	{
		return nRtn;
	}
	
	int nColumnSize = rawHeader.rsHeader.nColumnCount;		//save data setting

	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		auxFile.Close();
		canFile.Close();
		fRS.Close();
		return -3;
	}

	//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
	lineSize = sizeof(float)*nColumnSize;
	pfBuff = new float[nColumnSize*_MAX_DISK_WIRTE_COUNT];
	rsSize = lineSize*_MAX_DISK_WIRTE_COUNT;

	//Line 수를 Count
	int nBodySize = fRS.GetLength()-sizeof(PS_RAW_FILE_HEADER);
	int nRsCount = nBodySize / lineSize ;
	if(nRsCount < 0 || nBodySize % lineSize)
	{
		//TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
		auxFile.Close();
		canFile.Close();
		fRS.Close();
		return -4;
	}
	
	int nStackCount = 0;
	
	#ifdef _DEBUG
		DWORD dwStart = GetTickCount();
	#endif
	
	while(GetSaveDataC(sChResultData, sAuxData, sCanData))	//저장할 Data가 있을 경우 Stack에서 1개 Pop 한다.
	{
		//sChResultData.lSaveSequence => 1 Base No
		if((sChResultData.lSaveSequence-1)  > nRsCount)		//Data loss 발생
		{
			//중간부 Data 손실 여부만 확인 가능하다. 즉 손실 이후 최소 한번이상의 결과 Data가 갱신되지 않으면 
			//자동 검출 불가하다. 
			m_bDataLoss = TRUE;					 
//			TRACE("*****************>>>>>>>>>>>>>> Data loss detected(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
			nRtn = 0;
		}
		else if((sChResultData.lSaveSequence-1)  < nRsCount)	//Index가 역전된 경우 
		{
			//작업중에 복구 명령을 수행하면 Download와 복구 중에 발생한 Buffering data가 중복되게 된다.
			//이럴경우 현재 송신된 data(버퍼에 있던 data)는 이미 모듈에서 download하여 복구한 data이므로 
			//저장하지 않고 Skip한다.
//			nPreviousStepNo = sChResultData.chStepNo;
//			nPrevTotalCycleNo = sChResultData.nTotalCycleNum;
//			TRACE("*****************>>>>>>>>>>>>>> Data index ???(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
			continue;
		}
		else												//정상적인 data
		{
//			TRACE("*****************>>>>>>>>>>>>>> Data index %d received\n", sChResultData.lSaveSequence);
			m_bDataLoss = FALSE;
		}

		//Data 저장시 모든 단위를 m단위로 저장한다. (실처리 항목은 최소로 한다.) 파일 Size 문제...
		for(int dataIndex = 0; dataIndex <nColumnSize ; dataIndex++)
 		{
 			pfBuff[dataIndex + nStackCount * nColumnSize] = GetItemData(sChResultData, rawHeader.rsHeader.awColumnItem[dataIndex]);	//Index 5 is WH	

			//ksj 20170209 : float 정밀도로 인해 값 왜곡이 있어 PS_SYNC_DATE는 따로처리.
			if(rawHeader.rsHeader.awColumnItem[dataIndex] == PS_SYNC_DATE) 
			{	
				memcpy((void*)&pfBuff[dataIndex + nStackCount * nColumnSize], &sChResultData.lSyncDate, sizeof(long)); //ksj 20170209 : float 변수에 캐스팅 안한 그대로의 long 값을 복사해 넣는다.
			}
			/*else
			{				
				float fItemData = GetItemData(sChResultData, rawHeader.rsHeader.awColumnItem[dataIndex]);
				memcpy((void*)&pfBuff[dataIndex + nStackCount * nColumnSize], &fItemData, sizeof(float));
			}*/

 		}
	
	//	int nAuxIndex = 0;
		for(int nAuxDataIndex = 0; nAuxDataIndex < nAuxColumnSize ; nAuxDataIndex++)
		{
		//	pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = GetAuxItemData(sAuxData, auxHeader.auxHeader.awColumnItem[nAuxDataIndex], nAuxIndex);					
		//	if(auxHeader.auxHeader.awColumnItem[nAuxDataIndex] == PS_AUX_VALUE)
		//		nAuxIndex++;
			pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = sAuxData.auxData[nAuxDataIndex].fValue;
		}	

	//	int nCanIndex = 0;
		for(int nCanDataIndex = 0; nCanDataIndex < nCanColumnSize ; nCanDataIndex++)
		{
			//plCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] = GetCanItemData(sCanData, canHeader.canHeader.awColumnItem[nCanDataIndex], nCanIndex);								
			//if(canHeader.canHeader.awColumnItem[nCanDataIndex] == PS_CAN_VALUE)
			//	nCanIndex++;
			memcpy(&pCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] , &sCanData.canData[nCanDataIndex].canVal, sizeof(SFT_CAN_VALUE));

			
		}	
//		TRACE("StackCount : %d\r\n", nStackCount);
//		TRACE("CAN Data : %f, %f, %f\r\n", pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal);
	
//		TRACE("V:%d/I:%d/C:%d\n", sChResultData.lVoltage,sChResultData.lCurrent,sChResultData.lCapacity );

		//////////////////////////////////////////////////////////////////////////
		//2006/8/4/
		//result file save type mode.

		//2. Binary Mode file, TextMode temp. file
		lStartIndex = WriteResultListFileA(lStartIndex, nRsCount, sChResultData, sAuxData, sCanData);
		if(lStartIndex < 0)
		{
			nRtn = -5;
		}


		//////////////////////////////////////////////////////////////////////////
		nRsCount++;
		nStackCount++;

		//Disk write 성능 향상을 위해 한꺼번에 기록 하도록 수정 
		if(nStackCount >= _MAX_DISK_WIRTE_COUNT)
		{
			fRS.Write(pfBuff, rsSize);

			if(nAuxColumnSize > 0)
			{
				auxFile.Write(pfAuxBuff, auxSize);
				
				//	strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
				//		pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
				//		pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
				
			//	TRACE(strMsg);
			}
			if(nCanColumnSize > 0)
			{
				canFile.Write(pCanBuff, canSize);
				CString strMsg;
				//strMsg.Format("ChNo %d : Stack >= MAX 파일에 기록(Aux 수 : %d)(CAN 수 : %d)",
				strMsg.Format(Fun_FindMsg("CyclerChannel_CheckAndSaveData_msg1","CYCLERCHANNEL"),//&&
					m_nChannelIndex+1,nAuxColumnSize,nCanColumnSize
					) ;
				WriteLog(strMsg);
			}
			

			nStackCount = 0;
		}
	}

	//Disk access 횟수를 줄이기 위해 한꺼번에 저장 
	if(nStackCount > 0)
	{
		fRS.Write(pfBuff, sizeof(float)*nColumnSize*nStackCount);

		if(nAuxColumnSize > 0)
		{
			auxFile.Write(pfAuxBuff, sizeof(float)*nAuxColumnSize*nStackCount);
			//CString strMsg;
			//strMsg.Format("Save 1:%0.3f, %0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f,%0.3f, %0.3f, %0.3f\r\n",
			//	pfAuxBuff[0][0],pfAuxBuff[0][1],pfAuxBuff[0][2],pfAuxBuff[0][3],pfAuxBuff[0][4],pfAuxBuff[0][5],pfAuxBuff[0][6],
			//	pfAuxBuff[0][7],pfAuxBuff[0][8],pfAuxBuff[0][9]);
			
			//	TRACE(strMsg);
		}
		if(nCanColumnSize > 0)
		{
			canFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*nCanColumnSize*nStackCount);

			CString strMsg;
				//strMsg.Format("ChNo %d : Stack >= MAX 파일에 기록(Aux 수 : %d)(CAN 수 : %d)",
				strMsg.Format(Fun_FindMsg("CyclerChannel_CheckAndSaveData_msg2","CYCLERCHANNEL"),//&&
					m_nChannelIndex+1,nAuxColumnSize,nCanColumnSize
					) ;
			WriteLog(strMsg);
		}
		
	
	
		//2006/8/18
		//Update Last temp file
		UpdateLastData(UINT(lStartIndex), nRsCount-1, strStartTime, strTestSerial, strUser, strDescript, sChResultData);
	}

#ifdef _DEBUG
	if(m_nChannelIndex == 0)
		TRACE("Channel %d data save time : %dms elapsed\n",  m_nChannelIndex+1, GetTickCount()-dwStart);
#endif

//	if(sChResultData.chNo == 1)
//		TRACE("===========================>CH%03d-S02d: [%dmsec]\n", sChResultData.chNo, sChResultData.chStepNo, GetTickCount()-dwTime);

	//
	if(pfBuff != NULL)
	{	
		delete[]	pfBuff;		
		pfBuff = NULL;
	}
	if(pfAuxBuff != NULL)
	{
		delete [] pfAuxBuff;
		pfAuxBuff = NULL;
	}
	if(pCanBuff != NULL)
	{
		delete [] pCanBuff;
		pCanBuff = NULL;
	}
		
	if (fRS.m_hFile != CFile::hFileNull)
	{
		fRS.Flush();
		fRS.Close();
		auxFile.Flush();
		auxFile.Close();
		canFile.Flush();
		canFile.Close();
	}
	TRACE("Channel %d 결과 Data 저장\n", m_nChannelIndex+1);

	return nRtn;
}

long CCyclerChannel::GetCanItemData(PS_CAN_RAW_DATA canData, WORD wItem, int nCanIndex)
{

	long lData = 0;
	switch(wItem)
	{			
	case PS_CAN_OWNER_CH_NO:	lData = float(canData.canData[nCanIndex].chNo); break;
	case PS_CAN_CHANNEL_NO:	lData = float(nCanIndex); break;
	case PS_CAN_TYPE:	lData = float(canData.canData[nCanIndex].canType); break;
//	case PS_CAN_VALUE:	lData = float(canData.canData[nCanIndex].canVal); break;
	}

	return lData;
}

int CCyclerChannel::GetCanFile(CFile *pFile, PS_CAN_FILE_HEADER *canHeader)
{
	CFileStatus	fCanStatus;		
	CFileException e;
	
	CString  m_strCanFileName;	//ljb 20140429 저장 파일명	
	
	//m_strCanFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_CAN_FILE_NAME_EXT);
	//파일명 생성 
	m_strCanFileName.Format("%s\\%s.%s", m_strFilePath, m_strTestName, PS_CAN_FILE_NAME_EXT);
	
	//파일 존재 여부를 검사하여 없으면 생성한다.
	if(CFile::GetStatus(m_strCanFileName, fCanStatus) == FALSE)
	{
		if(CreateCanFileA(m_strCanFileName, 0, GetCanCount()) ==FALSE)
		{
			TRACE("%s를 생성할 수 없습니다.", m_strCanFileName);
			return -12;
		}
	}
	
	//파일 존재하면 용량 체크 해서 분할 Size 이상이면 새로 생성 한다.
	int nFileNo=0;
	long dFileSize=0;
	if( CFile::GetStatus(m_strCanFileName, fCanStatus) == TRUE) {}
	
	//파일 있음 용량 체크
	dFileSize = fCanStatus.m_size;
	nFileNo = dFileSize / (1024 * ID_MAX_FILE_SIZE);		//File 분할 사이즈
	TRACE("CAN File Size = %d \n",dFileSize);
	if (nFileNo == 0)
	{
		//파일을 Open 한다.
		if (pFile->m_hFile == CFile::hFileNull)
		{
			if( !pFile->Open( m_strCanFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
			{
#ifdef _DEBUG
				afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
				return -13;
			}
		}
		
		//skip file header	
		//read record column set
		if( pFile->Read(canHeader, sizeof(PS_CAN_FILE_HEADER)) != sizeof(PS_CAN_FILE_HEADER))
		{
			pFile->Close();
			return -14;
		}
		pFile->SeekToEnd();
		
		return 0;
	}
	
	//ljb 20140508 add  start ///////////////////////////////////////////////////////////////////////////////////////
	for (int iFileCount=1;iFileCount < 1000; iFileCount++)
	{
		m_strCanFileName.Format("%s\\%s.%s%03d", m_strFilePath, m_strTestName,PS_CAN_FILE_NAME_EXT,iFileCount);	//ljb 20140429 File 분할 파일 이름
		if( CFile::GetStatus(m_strCanFileName, fCanStatus) == FALSE) 
		{
			//파일 없음
			if(CreateCanFileA(m_strCanFileName, 0, GetCanCount()) ==FALSE)
			{
				TRACE("%s를 생성할 수 없습니다.", m_strCanFileName);
				return -12;
			}
		}
		if( CFile::GetStatus(m_strCanFileName, fCanStatus) == TRUE) {}
		//파일 있음 용량 체크
		dFileSize = fCanStatus.m_size;
		nFileNo = dFileSize / (1024 * ID_MAX_FILE_SIZE);		//File 분할 사이즈
		TRACE("File Size = %d \n",dFileSize);
		if (nFileNo == 0) break;
	}
	
	//파일을 Open 한다.
	if (pFile->m_hFile == CFile::hFileNull)
	{
		if( !pFile->Open( m_strCanFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e ) )
		{
#ifdef _DEBUG
			afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
			return -13;
		}
	}
	
	//skip file header	
	//read record column set
	if( pFile->Read(canHeader, sizeof(PS_CAN_FILE_HEADER)) != sizeof(PS_CAN_FILE_HEADER))
	{
		pFile->Close();
		return -14;
	}
	pFile->SeekToEnd();
	return 0;
	//ljb 20140508 add  end ///////////////////////////////////////////////////////////////////////////////////////
}

BOOL CCyclerChannel::CreateCanFileA(CString strFileName, int nFileIndex, int nCanCount)
{
	CFile f;
	CFileException e;
	int nCount = 0;

	if(strFileName.IsEmpty())	return FALSE;
	
	if( !f.Open( strFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		return FALSE;
	}
	
	LPPS_FILE_ID_HEADER pFileHeader = new PS_FILE_ID_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	pFileHeader->nFileID = PS_PNE_CAN_FILE_ID;
	pFileHeader->nFileVersion = _PNE_CAN_FILE_VER;
	CTime t = CTime::GetCurrentTime();
	sprintf(pFileHeader->szCreateDateTime, "%s", t.Format("%y/%m/%d %H:%M:%S"));
	sprintf(pFileHeader->szDescrition, "%s", "PNE CAN data file.");
	
	f.Write(pFileHeader, sizeof(PS_FILE_ID_HEADER));
	delete pFileHeader;
	
	
	//2. Can File Header 생성
	PS_CAN_DATA_FILE_HEADER fileCanSetHeader;
	ZeroMemory(&fileCanSetHeader, sizeof(PS_CAN_DATA_FILE_HEADER));

	nCount = nCanCount;


	fileCanSetHeader.nColumnCount = nCount;
	TRACE("Can ColumnCount : %d\r\n", fileCanSetHeader.nColumnCount);
	
	for(int i = 0; i < nCount; i++)
	{
		fileCanSetHeader.awColumnItem[i] = PS_CAN_VALUE;			//CanValue1
	}
	
	
	f.Write(&fileCanSetHeader, sizeof(PS_CAN_DATA_FILE_HEADER));
	
	f.Flush();
	f.Close();
	
	m_strDataList.RemoveAll();
	
	//WriteLog(strFileName+" 생성");
	WriteLog(strFileName+Fun_FindMsg("CyclerChannel_CreateCanFileA_msg1","CYCLERCHANNEL"));//&&
	
	return TRUE;
}

void CCyclerChannel::SetCanCount(int nCount)
{
	this->m_nCanCount = nCount;
}

//ljb 20130404 작업 시작시 CTS 파일 생성	for Analyzer 프로그램에서 처음 STEP 진행 중 멈춰도 첫 스텝이 보이도록 하기 위해 추가
long CCyclerChannel::WriteCtsFile()
{
	DWORD dwStartTime = GetTickCount();
	
	if(m_strFilePath.IsEmpty())
	{
		TRACE("File path is empty");
		return -1;
	}
	
	CFileStatus fs;
	CFileException e;
	COleDateTime t = COleDateTime::GetCurrentTime();
	long nStartIndex=0, nEndIndex=0;
	CString strTemp;
	CFile rltData;
	
	CString strStartTime, strEndTime, strTestSerial, strUser, strDescript, strLineNo;
	strStartTime = m_startTime.Format();
	strTestSerial = m_strTestSerial;
	strUser = m_strUserID;
	strDescript = m_strDescript;
	
	//결과 파일명
	m_strResultStepFileName.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_RESULT_FILE_NAME_EXT);		//.
//	nStartIndex = lStartNo;
	//////////////////////////////////////////////////////////////////////////
	
	//3. Step end이면 결과 파일에 저장한다. 
	//////////////////////////////////////////////////////////////////////////
//	nEndIndex = nLineNo;
// 	if(sChData.chDataSelect == SFT_SAVE_STEP_END)
// 	{
// 		//20110319 KHS////////////////////////////////////////////////////////////////////////////////////////////
// 		PS_STEP_END_RECORD prevData;
// 		CString strStartTime, strTestSerial, strUser, strDescript, strTrayNo;
// 		long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
// 		SaveStepEndData(strStartTime, sChData);
// 		//20120210 KHS
// 		if(m_bDataLoss == TRUE)		UpdateChannelEndLossState(strStartTime, sChData);
// 		else						UpdateChannelEndState(strStartTime, sChData);
// 		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//3.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		 
		{
			PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = PS_FILE_VERSION;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
			sprintf(pFileHeader->testHeader.szStartTime, "%s", strStartTime);
			sprintf(pFileHeader->testHeader.szEndTime, "%s", t.Format());
			sprintf(pFileHeader->testHeader.szSerial, "%s", strTestSerial);
			sprintf(pFileHeader->testHeader.szUserID, "%s", strUser);
			sprintf(pFileHeader->testHeader.szDescript, "%s", strDescript);
			sprintf(pFileHeader->testHeader.szTray_CellBcr, "%s,%s", m_strTrayNo, m_strBcrNo);	//ljb 20160404 add
//yulee 20190514 cny
			pFileHeader->testHeader.nRecordSize = 35;	//ljb 20131208 edit 12->26 Day반영 ljb 20150527 26->28=>33=>35
			pFileHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[1] = PS_CURRENT;
			pFileHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
			pFileHeader->testHeader.wRecordItem[3] = PS_WATT;
			pFileHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
			pFileHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
			pFileHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
			pFileHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
			pFileHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
			pFileHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
			pFileHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
			pFileHeader->testHeader.wRecordItem[12] = PS_CHARGE_CAP;
			pFileHeader->testHeader.wRecordItem[13] = PS_DISCHARGE_CAP;
			pFileHeader->testHeader.wRecordItem[14] = PS_CAPACITANCE;
			pFileHeader->testHeader.wRecordItem[15] = PS_CHARGE_WH;
			pFileHeader->testHeader.wRecordItem[16] = PS_DISCHARGE_WH;
			pFileHeader->testHeader.wRecordItem[17] = PS_CV_TIME;			//ljb 2011418 이재복 ////////// 기본으로 save
			pFileHeader->testHeader.wRecordItem[18] = PS_SYNC_DATE;			//ljb 2011418 이재복 ////////// 기본으로 save
			pFileHeader->testHeader.wRecordItem[19] = PS_SYNC_TIME;			//ljb 2011418 이재복 ////////// 기본으로 save
			pFileHeader->testHeader.wRecordItem[20] = PS_VOLTAGE_INPUT;		//ljb 201012
			pFileHeader->testHeader.wRecordItem[21] = PS_VOLTAGE_POWER;		//ljb 201012
			pFileHeader->testHeader.wRecordItem[22] = PS_VOLTAGE_BUS;		//ljb 201012
			pFileHeader->testHeader.wRecordItem[23] = PS_STEP_TIME_DAY;		//ljb 20131208 add
			pFileHeader->testHeader.wRecordItem[24] = PS_TOT_TIME_DAY;		//ljb 20131208 add
			pFileHeader->testHeader.wRecordItem[25] = PS_CV_TIME_DAY;		//ljb 20131208 add
			pFileHeader->testHeader.wRecordItem[26] = PS_LOAD_VOLT;			//ljb 20150430 add
			pFileHeader->testHeader.wRecordItem[27] = PS_LOAD_CURR;			//ljb 20150430 add
			pFileHeader->testHeader.wRecordItem[28] = PS_CHILLER_REF_TEMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[29] = PS_CHILLER_CUR_TEMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[30] = PS_CHILLER_REF_PUMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[31] = PS_CHILLER_CUR_PUMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[32] = PS_CELLBAL_CH_DATA;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[33] = PS_PWRSUPPLY_SETVOLT;	//ljb 20170912 add//yulee 20190514 cny
			pFileHeader->testHeader.wRecordItem[34] = PS_PWRSUPPLY_CURVOLT;	//ljb 20170912 add
			
			if(rltData.Open(m_strResultStepFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				rltData.Write(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			rltData.Close();
			//nStartIndex = 0; 
		}//End if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		

	return 1;
}

long CCyclerChannel::WriteResultListFileA(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData)
{
	DWORD dwStartTime = GetTickCount();

	if(m_strFilePath.IsEmpty())
	{
		TRACE("File path is empty");
		return -1;
	}

	CFileStatus fs;
	CFileException e;
	COleDateTime t = COleDateTime::GetCurrentTime();
	long nStartIndex=0, nEndIndex=0;
	CString strTemp;
	CFile rltData;

	CString strStartTime, strEndTime, strTestSerial, strUser, strDescript, strLineNo;
	strStartTime = m_startTime.Format();
	strTestSerial = m_strTestSerial;
	strUser = m_strUserID;
	strDescript = m_strDescript;

	//결과 파일명
	m_strResultStepFileName.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_RESULT_FILE_NAME_EXT);		//.
	nStartIndex = lStartNo;
	//////////////////////////////////////////////////////////////////////////

	//3. Step end이면 결과 파일에 저장한다. 
	//////////////////////////////////////////////////////////////////////////
	nEndIndex = nLineNo;
	if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		//20110319 KHS////////////////////////////////////////////////////////////////////////////////////////////
		PS_STEP_END_RECORD prevData;
		CString strStartTime, strTestSerial, strUser, strDescript, strTrayNo;
		long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
		SaveStepEndData(strStartTime, sChData);
		//20120210 KHS
		if(m_bDataLoss == TRUE)		UpdateChannelEndLossState(strStartTime, sChData);
		else						UpdateChannelEndState(strStartTime, sChData);
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		//3.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		 
		{
			PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = PS_FILE_VERSION;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
			sprintf(pFileHeader->testHeader.szStartTime, "%s", strStartTime);
			sprintf(pFileHeader->testHeader.szEndTime, "%s", t.Format());
			sprintf(pFileHeader->testHeader.szSerial, "%s", strTestSerial);
			sprintf(pFileHeader->testHeader.szUserID, "%s", strUser);
			sprintf(pFileHeader->testHeader.szDescript, "%s", strDescript);
			pFileHeader->testHeader.nRecordSize = 12;
			pFileHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[1] = PS_CURRENT;
			pFileHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
			pFileHeader->testHeader.wRecordItem[3] = PS_WATT;
			pFileHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
			pFileHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
			pFileHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
			pFileHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
			pFileHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
			pFileHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
			pFileHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
			
			if(rltData.Open(m_strResultStepFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				rltData.Write(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;

			nStartIndex = 0; 
		}//End if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		
		//3.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!rltData.Open(m_strResultStepFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = rltData.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			{
				//End Time 갱신
				PS_TEST_FILE_HEADER *pFileHeader = new PS_TEST_FILE_HEADER;
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Read(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				sprintf(pFileHeader->szEndTime, "%s", t.Format());
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Write(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				delete pFileHeader;

				//가장 마지막 data read
				LPPS_STEP_END_RECORD pDataRecord = new PS_STEP_END_RECORD;
				rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
				rltData.Read(pDataRecord, sizeof(PS_STEP_END_RECORD));

				//같은 종류의 step이 이미 저장되어 있을 경우 
				//마지막을 현재 data로 하여 갱신한다.
				//작업 진행중에 data 복구를 실시할 경우 발생할 수 있다.
				if(pDataRecord->chStepNo == sChData.chStepNo && pDataRecord->nTotalCycleNum == sChData.nTotalCycleNum)
				{
					rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
					nStartIndex = pDataRecord->nIndexFrom;
				}
				else
				{
					rltData.Seek(0, CFile::end);
					//////////////////////////////////////////////////////////////////////////
					//debugging check
					//손실 data 복구시 모듈에서 아직 전송되지 않은 더 많은 data를 이미 복구해 버리면 발생할 수 있다.
					//
					if(nStartIndex != pDataRecord->nIndexTo+1)
					{
						strTemp.Format("Ch %d End file index error Step %d, Total Cycle %d/%d. (Prev range : %d~%d, Next start : %d), Size %d", 
										m_nChannelIndex+1, pDataRecord->chStepNo, sChData.nTotalCycleNum, pDataRecord->nTotalCycleNum,
										pDataRecord->nIndexFrom, pDataRecord->nIndexTo, nStartIndex, fsize
							);
						WriteLog(strTemp);
#ifdef _DEBUG
						AfxMessageBox(strTemp);
#endif
					}
					//////////////////////////////////////////////////////////////////////////

					//시작 Index는 이전 Step완료의 마지막 Index에서 시작 
					nStartIndex = pDataRecord->nIndexTo+1;
				}
				delete pDataRecord;
			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);
				nStartIndex = 0;
				if(rltData.m_hFile != NULL)	rltData.Close();
				return -1;
			}
		}	//End else

		sChData.nIndexFrom = nStartIndex;
		sChData.nIndexTo = nEndIndex;
	
		//결과 파일을 갱신한다.
		if(rltData.m_hFile != NULL)
		{
			long fsize = 0;

			rltData.Write(&sChData, sizeof(PS_STEP_END_RECORD));
			rltData.Flush();
			fsize = rltData.GetLength();
			rltData.Close();

			strTemp.Format("Step %d [%s]Completion (%d~%d). %dmsec/Size: %d",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sChData.nIndexFrom, sChData.nIndexTo, GetTickCount()-dwStartTime, fsize);
			WriteLog(strTemp);
		}
		
		//용량 합산 표시를 위한 total용량 갱신 
		AddTotalCapacity(sChData.chStepType, (float)ConvertPCUnitToSbc(sChData.fCapacity, PS_CAPACITY));

		//다음 Step의 시작 Index 생성 
		nStartIndex = nEndIndex+1;

		//Send result to database
		//20070305 BY KBH
		//Send message to database client 
		//////////////////////////////////////////////////////////////////////////
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DataBaseUpdate", FALSE))
		{
			CString strTemp;
			TCHAR szCurDir[MAX_PATH];
			::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
			strTemp.Format("%s\\DataProServer.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));

			//Message를 전송한다.
			HWND hWnd = ::FindWindow(NULL, "CTSDataPro Server");
			if(hWnd)
			{
				char szData[MAX_PATH];
				ZeroMemory(szData, MAX_PATH);
				sprintf(szData, "%s", m_strFilePath);
				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = sChData.chStepNo;		
				CpStructData.cbData = m_strFilePath.GetLength();
				CpStructData.lpData = szData;
				::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				
//				sprintf(szData, "Send M%d C%d step %d data to data base server", m_nModuleID, m_nChannelIndex+1, sChData.chStepNo);
//				::SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szData);
			}
			//////////////////////////////////////////////////////////////////////////
		}
	} //if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	//4 Aux Result File를 생성한다.
	if(sAuxData.auxCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		CFile auxFile;
		CString strResultAuxFile;
		strResultAuxFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_AUX_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)		 
		{
			PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = _PNE_AUX_FILE_VER;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			
		
			pFileHeader->auxHeader.nColumnCount = sAuxData.auxCount;
	
			for(int i = 0; i < sAuxData.auxCount; i++)
			{
				pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
			}
			
			if(auxFile.Open(strResultAuxFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Aux Result File create \r\n");

		}//End if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!auxFile.Open(strResultAuxFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = auxFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_AUX_FILE_HEADER))
			{
				//End Time 갱신
				PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
				auxFile.Seek(sizeof(PS_AUX_FILE_HEADER), CFile::begin);
				auxFile.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				float * pfBuff = new float[sAuxData.auxCount];
				auxFile.Seek(fsize - sizeof(float)*sAuxData.auxCount, CFile::begin);
				auxFile.Read(pfBuff, sizeof(float)*sAuxData.auxCount);
				delete [] pfBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(auxFile.m_hFile != NULL)	auxFile.Close();
				return -1;
			}
			TRACE("기존 Aux Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(auxFile.m_hFile != NULL)
		{
			long fsize = 0;

			float * pfBuff = new float[sAuxData.auxCount];
			for(int i = 0 ; i < sAuxData.auxCount; i++)
				pfBuff[i] = sAuxData.auxData[i].fValue;
			auxFile.Write(pfBuff, sizeof(float)*sAuxData.auxCount);
			auxFile.Flush();
			auxFile.Close();
			

			strTemp.Format("Step %d [%s] Aux Count : %d Completion",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sAuxData.auxCount);
			WriteLog(strTemp);
			TRACE(strTemp+"\r\n");
// 			TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
// 								pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);
			delete [] pfBuff;
		}
	
	}
	//5. Can Result File를 생성한다.
	if(sCanData.canCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		CFile CanFile;
		CString strResultCanFile;
		strResultCanFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_CAN_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultCanFile, fs) == FALSE)		 
		{
			PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_CAN_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_CAN_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = 0x1000;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Can end data file.");			
		
			pFileHeader->canHeader.nColumnCount = sCanData.canCount;
	
			for(int i = 0; i < sCanData.canCount; i++)
			{
				pFileHeader->canHeader.awColumnItem[i] = PS_CAN_VALUE;			//CanValue1
			}
			
			if(CanFile.Open(strResultCanFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				CanFile.Write(pFileHeader, sizeof(PS_CAN_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Can Result File create\r\n");

		}//End if( CFile::GetStatus(strResultCanFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!CanFile.Open(strResultCanFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = CanFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_CAN_FILE_HEADER))
			{
				//End Time 갱신
				PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
				CanFile.Seek(sizeof(PS_CAN_FILE_HEADER), CFile::begin);
				CanFile.Read(pFileHeader, sizeof(PS_CAN_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				long * plBuff = new long[sCanData.canCount];
				CanFile.Seek(fsize - sizeof(long)*sCanData.canCount, CFile::begin);
				CanFile.Read(plBuff, sizeof(long)*sCanData.canCount);
				delete [] plBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(CanFile.m_hFile != NULL)	CanFile.Close();
				return -1;
			}
			TRACE("기존 Can Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(CanFile.m_hFile != NULL)
		{
			long fsize = 0;

			SFT_CAN_VALUE * pCanBuff = new SFT_CAN_VALUE[sCanData.canCount];
			for(int i = 0 ; i < sCanData.canCount; i++)
				memcpy(&pCanBuff[i], &sCanData.canData[i].canVal, sizeof(SFT_CAN_VALUE));
			CanFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*sCanData.canCount);
			CanFile.Flush();
			CanFile.Close();
			

			strTemp.Format("Step %d [%s] Can Count : %d Completion",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sCanData.canCount);
			WriteLog(strTemp);
			TRACE(strTemp+"\r\n");
 			//TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
 			//					pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal, pCanBuff[3].fVal, pCanBuff[4].fVal,
 			//					pCanBuff[5].fVal, pCanBuff[6].fVal, pCanBuff[7].fVal);
			delete [] pCanBuff;
		}
	
	}

	//////////////////////////////////////////////////////////////////////////
	//else	//Delta t, Delta V, Delta I, Delta T etc... 일 경우는 Start Index는 변화 없습

	return nStartIndex;
}

long CCyclerChannel::WriteResultListFile_V100B(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData)
//ksj 20201013 : 스텝엔드 추가 정보 구조체 추가.
//신규로 sChDataExtra 추가하고, sChDataExtra가 sChData의 정보를 다 포함하고 있으나,
//기존 구조 유지하기 위해 sChData를 제거 하지 않고 그대로 사용하도록 함.
//long CCyclerChannel::WriteResultListFile_V100B(UINT lStartNo, UINT nLineNo, PS_STEP_END_RECORD &sChData, PS_AUX_RAW_DATA &sAuxData, PS_CAN_RAW_DATA &sCanData, PS_STEP_END_RECORD_EXTRA &sChDataExtra) 
{
	DWORD dwStartTime = GetTickCount();

	if(m_strFilePath.IsEmpty())
	{
		TRACE("File path is empty");
		return -1;
	}

	CFileStatus fs;
	CFileException e;
	COleDateTime t = COleDateTime::GetCurrentTime();
	long nStartIndex=0, nEndIndex=0;
	CString strTemp;
	CFile rltData;

	CString strStartTime, strEndTime, strTestSerial, strUser, strDescript, strLineNo;
	strStartTime = m_startTime.Format();
	strTestSerial = m_strTestSerial;
	strUser = m_strUserID;
	strDescript = m_strDescript;

	//결과 파일명
	m_strResultStepFileName.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_RESULT_FILE_NAME_EXT);		//.
	nStartIndex = lStartNo;
	//////////////////////////////////////////////////////////////////////////

	//3. Step end이면 결과 파일에 저장한다. 
	//////////////////////////////////////////////////////////////////////////
	nEndIndex = nLineNo;
	if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		//20110319 KHS////////////////////////////////////////////////////////////////////////////////////////////
		PS_STEP_END_RECORD prevData;
		CString strStartTime, strTestSerial, strUser, strDescript, strTrayNo;
		long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
		SaveStepEndData(strStartTime, sChData);
		//20120210 KHS
		if(m_bDataLoss == TRUE)		UpdateChannelEndLossState(strStartTime, sChData);
		else						UpdateChannelEndState(strStartTime, sChData);
		//////////////////////////////////////////////////////////////////////////////////////////////////////////

		//3.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		 
		{
			PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_ADP_STEP_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = PS_FILE_VERSION;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler step end data file.");			
			sprintf(pFileHeader->testHeader.szStartTime, "%s", strStartTime);
			sprintf(pFileHeader->testHeader.szEndTime, "%s", t.Format());
			sprintf(pFileHeader->testHeader.szSerial, "%s", strTestSerial);
			sprintf(pFileHeader->testHeader.szUserID, "%s", strUser);
			sprintf(pFileHeader->testHeader.szDescript, "%s", strDescript);
			sprintf(pFileHeader->testHeader.szTray_CellBcr, "%s,%s", m_strTrayNo, m_strBcrNo);	//ljb 20160404 add
//yulee 20190514 cny
			pFileHeader->testHeader.nRecordSize = 35;			//ljb 201007	21 -> 30 -> 201012 (34) -> 20131208 39=>33=>35
			pFileHeader->testHeader.wRecordItem[0] = PS_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[1] = PS_CURRENT;
			pFileHeader->testHeader.wRecordItem[2] = PS_CAPACITY;
			pFileHeader->testHeader.wRecordItem[3] = PS_WATT;
			pFileHeader->testHeader.wRecordItem[4] = PS_WATT_HOUR;
			pFileHeader->testHeader.wRecordItem[5] = PS_STEP_TIME;
			pFileHeader->testHeader.wRecordItem[6] = PS_TOT_TIME;
			pFileHeader->testHeader.wRecordItem[7] = PS_IMPEDANCE;
			pFileHeader->testHeader.wRecordItem[8] = PS_OVEN_TEMPERATURE;
			pFileHeader->testHeader.wRecordItem[9] = PS_OVEN_HUMIDITY;
			pFileHeader->testHeader.wRecordItem[10] = PS_AVG_VOLTAGE;
			pFileHeader->testHeader.wRecordItem[11] = PS_AVG_CURRENT;
			pFileHeader->testHeader.wRecordItem[12] = PS_CHARGE_CAP;
			pFileHeader->testHeader.wRecordItem[13] = PS_DISCHARGE_CAP;
			pFileHeader->testHeader.wRecordItem[14] = PS_CAPACITANCE;
			pFileHeader->testHeader.wRecordItem[15] = PS_CHARGE_WH;
			pFileHeader->testHeader.wRecordItem[16] = PS_DISCHARGE_WH;
			pFileHeader->testHeader.wRecordItem[17] = PS_CV_TIME;			//ljb 2011418 이재복 ////////// 기본으로 save
			pFileHeader->testHeader.wRecordItem[18] = PS_SYNC_DATE;			//ljb 2011418 이재복 ////////// 기본으로 save
			pFileHeader->testHeader.wRecordItem[19] = PS_SYNC_TIME;			//ljb 2011418 이재복 ////////// 기본으로 save
			pFileHeader->testHeader.wRecordItem[20] = PS_VOLTAGE_INPUT;		//ljb 201012
			pFileHeader->testHeader.wRecordItem[21] = PS_VOLTAGE_POWER;		//ljb 201012
			pFileHeader->testHeader.wRecordItem[22] = PS_VOLTAGE_BUS;		//ljb 201012
			pFileHeader->testHeader.wRecordItem[23] = PS_STEP_TIME_DAY;		//ljb 20131208 add
			pFileHeader->testHeader.wRecordItem[24] = PS_TOT_TIME_DAY;		//ljb 20131208 add
			pFileHeader->testHeader.wRecordItem[25] = PS_CV_TIME_DAY;		//ljb 20131208 add
			pFileHeader->testHeader.wRecordItem[26] = PS_LOAD_VOLT;			//ljb 20150430 add
			pFileHeader->testHeader.wRecordItem[27] = PS_LOAD_CURR;			//ljb 20150430 add
			pFileHeader->testHeader.wRecordItem[28] = PS_CHILLER_REF_TEMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[29] = PS_CHILLER_CUR_TEMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[30] = PS_CHILLER_REF_PUMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[31] = PS_CHILLER_CUR_PUMP;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[32] = PS_CELLBAL_CH_DATA;	//ljb 20170912 add
			pFileHeader->testHeader.wRecordItem[33] = PS_PWRSUPPLY_SETVOLT;	//ljb 20170912 add//yulee 20190514 cny
			pFileHeader->testHeader.wRecordItem[34] = PS_PWRSUPPLY_CURVOLT;	//ljb 20170912 add
			
			if(rltData.Open(m_strResultStepFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				rltData.Write(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;

			nStartIndex = 0; 
			m_bCompleteWork = FALSE;	//ljb
		}//End if( CFile::GetStatus(m_strResultStepFileName, fs) == FALSE)		
		//3.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!rltData.Open(m_strResultStepFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = rltData.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize == sizeof(PS_TEST_RESULT_FILE_HEADER))			//ljb 20130404 헤더만 저장 되어 있을 경우 처리 추가
			{
				//End Time 갱신
				PS_TEST_FILE_HEADER *pFileHeader = new PS_TEST_FILE_HEADER;
				
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Read(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				sprintf(pFileHeader->szEndTime, "%s", t.Format());
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Write(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				delete pFileHeader;
				rltData.Seek(0, CFile::end);
			}
			else if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			{
				//End Time 갱신
				PS_TEST_FILE_HEADER *pFileHeader = new PS_TEST_FILE_HEADER;

				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Read(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				sprintf(pFileHeader->szEndTime, "%s", t.Format());
				rltData.Seek(sizeof(PS_FILE_ID_HEADER), CFile::begin);
				rltData.Write(pFileHeader, sizeof(PS_TEST_FILE_HEADER));
				delete pFileHeader;

				//가장 마지막 data read
				LPPS_STEP_END_RECORD pDataRecord = new PS_STEP_END_RECORD;
				rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
				rltData.Read(pDataRecord, sizeof(PS_STEP_END_RECORD));

				//같은 종류의 step이 이미 저장되어 있을 경우 
				//마지막을 현재 data로 하여 갱신한다.
				//작업 진행중에 data 복구를 실시할 경우 발생할 수 있다.
				if(pDataRecord->chStepNo == sChData.chStepNo && pDataRecord->nTotalCycleNum == sChData.nTotalCycleNum && pDataRecord->lSaveSequence == sChData.lSaveSequence) //ljb 20170808 edit
				{
					rltData.Seek(fsize - sizeof(PS_STEP_END_RECORD), CFile::begin);
					nStartIndex = pDataRecord->nIndexFrom;
				}
				else
				{
					rltData.Seek(0, CFile::end);
					//////////////////////////////////////////////////////////////////////////
					//debugging check
					//손실 data 복구시 모듈에서 아직 전송되지 않은 더 많은 data를 이미 복구해 버리면 발생할 수 있다.
					//
					if(nStartIndex != pDataRecord->nIndexTo+1)
					{
						strTemp.Format("Ch %d End file index error Step %d, Total Cycle %d/%d. (Prev range : %d~%d, Next start : %d), Size %d", 
										m_nChannelIndex+1, pDataRecord->chStepNo, sChData.nTotalCycleNum, pDataRecord->nTotalCycleNum,
										pDataRecord->nIndexFrom, pDataRecord->nIndexTo, nStartIndex, fsize );
						WriteLog(strTemp);

						#ifdef _DEBUG
							AfxMessageBox(strTemp);
						#endif
					}
					//////////////////////////////////////////////////////////////////////////

					//시작 Index는 이전 Step완료의 마지막 Index에서 시작 
					nStartIndex = pDataRecord->nIndexTo+1;
				}
				delete pDataRecord;
			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);
				nStartIndex = 0;
				if(rltData.m_hFile != NULL)	rltData.Close();
				return -1;
			}
		}	//End else

		sChData.nIndexFrom = nStartIndex;
		sChData.nIndexTo = nEndIndex;
	
		//결과 파일을 갱신한다.
		if(rltData.m_hFile != NULL)
		{
			long fsize = 0;

			rltData.Write(&sChData, sizeof(PS_STEP_END_RECORD));
			rltData.Flush();
			fsize = rltData.GetLength();
			rltData.Close();

			strTemp.Format("Step %d [%s]Completion (%d~%d). %dmsec/Size: %d",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sChData.nIndexFrom, sChData.nIndexTo, GetTickCount()-dwStartTime, fsize);
			WriteLog(strTemp);

			theApp.Fun_ReceiveStepEnd_Data(this,&sChData);

			//작업 완료 설정
			//if (::PSGetTypeMsg(sChData.chStepType) == "완료") m_bCompleteWork = TRUE;	//ljb
			
			//진행 상태 전달		
			//HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");   //add ljb 2008-10-10 
			//if(hWnd)
			//{
			//	char szData[MAX_PATH];
			//	ZeroMemory(szData, MAX_PATH);
			//	sprintf(szData, "%s", strTemp);
			//	COPYDATASTRUCT CpStructData;
			//	CpStructData.dwData = WM_MY_MESSAGE_CTS_STEP_END_DATA;		
			//	CpStructData.cbData = strTemp.GetLength();
			//	CpStructData.lpData = szData;
			//	::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			//}
		}
		
		//용량 합산 표시를 위한 total용량 갱신 
		AddTotalCapacity(sChData.chStepType, (float)ConvertPCUnitToSbc(sChData.fCapacity, PS_CAPACITY));

		//다음 Step의 시작 Index 생성 
		nStartIndex = nEndIndex+1;

		//Send result to database
		//20070305 BY KBH
		//Send message to database client 
		//////////////////////////////////////////////////////////////////////////
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DataBaseUpdate", FALSE))
		{
			CString strTemp;
			TCHAR szCurDir[MAX_PATH];
			::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
			strTemp.Format("%s\\DataProServer.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));

			//Message를 전송한다.
			HWND hWnd = ::FindWindow(NULL, "CTSDataPro Server");
			if(hWnd)
			{
				char szData[MAX_PATH];
				ZeroMemory(szData, MAX_PATH);
				sprintf(szData, "%s", m_strFilePath);
				COPYDATASTRUCT CpStructData;
				CpStructData.dwData = sChData.chStepNo;		
				CpStructData.cbData = m_strFilePath.GetLength();
				CpStructData.lpData = szData;
				::SendMessage(hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
				
				//sprintf(szData, "Send M%d C%d step %d data to data base server", m_nModuleID, m_nChannelIndex+1, sChData.chStepNo);
				//::SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(m_nChannelIndex, m_nModuleID)), (LPARAM)szData);
			}
			//////////////////////////////////////////////////////////////////////////
		}
	} //if(sChData.chDataSelect == SFT_SAVE_STEP_END)

	//4 Aux Result File를 생성한다.
	if(sAuxData.auxCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		///////////////////////////////////////////////////////////////////////
		//yulee 20181202
		BOOL bRetSimpleTest;
		CString RegName;
		RegName.Format("OnProgressSimpleTest_%d", sChData.chNo);
		bRetSimpleTest = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RegName, 0);
		if(bRetSimpleTest == 0)
		{
			PS_STEP_END_RECORD prevData;
			CString strStartTime, strTestSerial, strUser, strDescript, strTrayNo;
			long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
			SaveStepEndAuxData(strStartTime, sChData, sAuxData);
		}
		///////////////////////////////////////////////////////////////////////

		CFile auxFile;
		CString strResultAuxFile;
		strResultAuxFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_AUX_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)		 
		{
			PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = _PNE_AUX_FILE_VER;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			
		
			pFileHeader->auxHeader.nColumnCount = sAuxData.auxCount;
	
			for(int i = 0; i < sAuxData.auxCount; i++)
			{
				pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
			}
			
			if(auxFile.Open(strResultAuxFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Aux Result File create \r\n");

		}//End if( CFile::GetStatus(strResultAuxFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!auxFile.Open(strResultAuxFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = auxFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_AUX_FILE_HEADER))
			{
				//End Time 갱신
				PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
				auxFile.Seek(sizeof(PS_AUX_FILE_HEADER), CFile::begin);
				auxFile.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				float * pfBuff = new float[sAuxData.auxCount];
				auxFile.Seek(fsize - sizeof(float)*sAuxData.auxCount, CFile::begin);
				auxFile.Read(pfBuff, sizeof(float)*sAuxData.auxCount);
				delete [] pfBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(auxFile.m_hFile != NULL)	auxFile.Close();
				return -1;
			}
			TRACE("Aux Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(auxFile.m_hFile != NULL)
		{
			long fsize = 0;

			float * pfBuff = new float[sAuxData.auxCount];
			for(int i = 0 ; i < sAuxData.auxCount; i++)
				pfBuff[i] = sAuxData.auxData[i].fValue;
			auxFile.Write(pfBuff, sizeof(float)*sAuxData.auxCount);
			auxFile.Flush();
			auxFile.Close();
			

			strTemp.Format("Step %d [%s] Aux Count : %d Completion",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sAuxData.auxCount);
			WriteLog(strTemp);
			//TRACE(strTemp+"\r\n");
			//TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",pfBuff[0], pfBuff[1], pfBuff[2], pfBuff[3], pfBuff[4], pfBuff[5], pfBuff[6], pfBuff[7]);
			delete [] pfBuff;
		}	
	}

	//5. Can Result File를 생성한다.
	if(sCanData.canCount > 0 && sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		///////////////////////////////////////////////////////////////////////
		//yulee 20181202

		BOOL bRetSimpleTest;
		CString RegName;
		RegName.Format("OnProgressSimpleTest_%d", sChData.chNo);
		bRetSimpleTest = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RegName, 0);
		if(bRetSimpleTest == 0)
		{
			PS_STEP_END_RECORD prevData;
			CString strStartTime, strTestSerial, strUser, strDescript, strTrayNo;
			long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);
			SaveStepEndCANData(strStartTime, sChData, sCanData);
		}
		///////////////////////////////////////////////////////////////////////

		CFile CanFile;
		CString strResultCanFile;
		strResultCanFile.Format("%s\\%s.%s", m_strFilePath ,m_strTestName, PS_CAN_RESULT_FILE_NAME_EXT);
		//4.1 기존 결과 파일이 존재하지 않으면 생성 
		if( CFile::GetStatus(strResultCanFile, fs) == FALSE)		 
		{
			PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
			ZeroMemory(pFileHeader, sizeof(PS_CAN_FILE_HEADER));
			pFileHeader->fileHeader.nFileID = PS_PNE_CAN_RESULT_FILE_ID;
			pFileHeader->fileHeader.nFileVersion = 0x1000;
			sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
			sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Can end data file.");			
		
			pFileHeader->canHeader.nColumnCount = sCanData.canCount;
	
			for(int i = 0; i < sCanData.canCount; i++)
			{
				pFileHeader->canHeader.awColumnItem[i] = PS_CAN_VALUE;			//CanValue1
			}
			
			if(CanFile.Open(strResultCanFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e))
			{
				CanFile.Write(pFileHeader, sizeof(PS_CAN_FILE_HEADER));	
			}
			else
			{
				delete pFileHeader;
				return -1;
			}
			delete pFileHeader;
			TRACE("Can Result File create \r\n");

		}//End if( CFile::GetStatus(strResultCanFile, fs) == FALSE)

		//4.2 기존 결과 파일이 존재하면 최종 시간 갱신 및 최종 저장 data와 비교  
		else
		{
			long fsize = 0;
			if(!CanFile.Open(strResultCanFile, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			{
				return -1;
			}
			fsize = CanFile.SeekToEnd();

			//최소 1개 이상의 record가 저장되어 있을 경우 가장 마지막 data를 읽어 들임 
			if(fsize >= sizeof(PS_CAN_FILE_HEADER))
			{
				//End Time 갱신
				PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
				CanFile.Seek(sizeof(PS_CAN_FILE_HEADER), CFile::begin);
				CanFile.Read(pFileHeader, sizeof(PS_CAN_FILE_HEADER));

				delete pFileHeader;
				//가장 마지막 data read
				long * plBuff = new long[sCanData.canCount];
				CanFile.Seek(fsize - sizeof(long)*sCanData.canCount, CFile::begin);
				CanFile.Read(plBuff, sizeof(long)*sCanData.canCount);
				delete [] plBuff;

			}//End if(fsize >= sizeof(PS_TEST_RESULT_FILE_HEADER)+sizeof(PS_STEP_END_RECORD))
			//File Error
			else
			{
				strTemp.Format("Step %d result save file. File header error. Total Cycle %d. Size %d", 
								sChData.chStepNo, sChData.nTotalCycleNum,
								fsize
								);
				WriteLog(strTemp);

				if(CanFile.m_hFile != NULL)	CanFile.Close();
				return -1;
			}
			TRACE("기존 Can Result File Open\r\n");
		}	//End else

		//결과 파일을 갱신한다.
		if(CanFile.m_hFile != NULL)
		{
			long fsize = 0;

			SFT_CAN_VALUE * pCanBuff = new SFT_CAN_VALUE[sCanData.canCount];
			for(int i = 0 ; i < sCanData.canCount; i++)
				memcpy(&pCanBuff[i], &sCanData.canData[i].canVal, sizeof(SFT_CAN_VALUE));
			CanFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*sCanData.canCount);
			CanFile.Flush();
			CanFile.Close();
			

			strTemp.Format("Step %d [%s] Can Count : %d Completion",  
				sChData.chStepNo, ::PSGetTypeMsg(sChData.chStepType), sCanData.canCount);
			WriteLog(strTemp);
			//TRACE(strTemp+"\r\n");
 			//TRACE("%f,%f,%f,%f,%f,%f,%f,%f\r\n",
 			//					pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal, pCanBuff[3].fVal, pCanBuff[4].fVal,
 			//					pCanBuff[5].fVal, pCanBuff[6].fVal, pCanBuff[7].fVal);
			delete [] pCanBuff;
		}
	
	}

	//lyj 20200521 //ksj 20200521 : 일부 수정.
	BOOL bUseMergeCSV = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseMergedStepEndCSV", FALSE);	
	if(sChData.chDataSelect == SFT_SAVE_STEP_END && bUseMergeCSV)
	{
		SaveStepEndDataMerge(strStartTime, sChData, sAuxData , sCanData); //ksj 20200521 : 수정.
	}
	
	//////////////////////////////////////////////////////////////////////////
	//else	//Delta t, Delta V, Delta I, Delta T etc... 일 경우는 Start Index는 변화 없습

	//ksj 20201008 : 채널 로그추가. 간혹 AUX 또는 CAN 스텝엔드가 손상 되는 것 추적용.
	//aux랑 can 개수가 제대로 들어오는지 확인.
	if(sChData.chDataSelect == SFT_SAVE_STEP_END)
	{
		strTemp.Format("Step %d. Data Count AUX:%d, CAN:%d", 
			sChData.chStepNo,
			sAuxData.auxCount,
			sCanData.canCount
			);
		WriteLog(strTemp);
	}
	//ksj end

	return nStartIndex;
}

CString CCyclerChannel::GetEndTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	

	CTimeSpan tempTime((ULONG)PCTIME(::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_STEP_TIME)));
	
//	int nDay = m_startTime.GetDay() + tempTime.GetDays();
//	int nHour = m_startTime.GetHour() + tempTime.GetHour();
//	int nMin = m_startTime.GetMinute() + tempTime.GetMinutes();
//	int nSec = m_startTime.GetSecond() + tempTime.GetSeconds();

	COleDateTime sumTime;
	sumTime.SetTime(m_startTime.GetHour() + tempTime.GetHours(), m_startTime.GetMinute() + tempTime.GetMinutes(), 
		m_startTime.GetSecond() + tempTime.GetSeconds());

	CString strTime = sumTime.Format();
	//strTime.Format()
/*	if(timeSpan.GetDays() > 0)
	{
		strMsg =  timeSpan.Format("%Dd %H:%M:%S");
	}
	else
	{
		strMsg = timeSpan.Format("%H:%M:%S");
	}
	}*/
	return strTime;
	

}

void CCyclerChannel::SetCanTitleFile(CString strCanPath, CString strTestName)
{	
 	//char path[MAX_PATH];
    //GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
 	//CString strTemp(path);
	CString strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path"); //ksj 20210507
	CString strPath;
	strTemp += "\\";
	strPath.Format("%s\\DataBase\\Config\\", strTemp.Left(strTemp.ReverseFind('\\')));
	strTemp.Format("%s.%s", strCanPath.Mid(strCanPath.ReverseFind('\\')+1, 7), PS_CAN_FILE_NAME_EXT);
	strPath += strTemp;
	strTemp.Format("%sCAN.%s", strTestName, "tlt"); 
	CString strNew = strCanPath+"\\"+strTemp;
	
		
   	CFileFind afinder;
	if(afinder.FindFile(strPath) == FALSE)	return;

	afinder.FindNextFile();
	strPath = afinder.GetFilePath();
	
	if(CopyFile(strPath, strNew, TRUE) == FALSE)
	{
		AfxMessageBox(strTemp+" Data copy fail.");
		return;
	}
	WriteLog("File copy complete. (CAN Title)");

	//ksj 20200223 : Can 설정 정보를 채널로그에 기록한다.
	WriteCanStateLog();
	//ksj end
}

int CCyclerChannel::GetTotalAuxCount()
{
	//return m_nTempAuxCount + m_nVoltAuxCount + m_nTempThAuxCount;
	return m_nTempAuxCount + m_nVoltAuxCount + m_nTempThAuxCount + m_nHumiAuxCount; //ksj 20200116
}

int CCyclerChannel::GetAuxVoltCount()
{
	return m_nVoltAuxCount;
}

int CCyclerChannel::GetAuxTempCount()
{
	return m_nTempAuxCount;
}

int CCyclerChannel::GetAuxTempThCount()
{
	return m_nTempThAuxCount;
}

int CCyclerChannel::GetAuxHumiThCount()
{
	return m_nHumiAuxCount;
}

/*
BOOL CCyclerChannel::GetFileRawData(PS_RAW_FILE_HEADER &FileHeader, FILE * fpDestFile, FILE * fpSourceFile,
									  CString destFileName, CString srcFileName)
{
	BOOL bLockFile = TRUE;
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		return FALSE; 
	}

	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)
		{
			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			fclose(fpDestFile);
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		fclose(fpDestFile);
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			return FALSE;	
		}
	}
	return TRUE;
}

BOOL CCyclerChannel::GetAuxFileRawData(PS_AUX_FILE_HEADER &FileHeader, FILE *fpDestFile, FILE *fpSourceFile,
									   CString destFileName, CString srcFileName)
{
	BOOL bLockFile = TRUE;
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		return FALSE; 
	}

	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateAuxFileA(srcFileName, 0, m_chFileDataSet.GetAuxColumnCount()) == FALSE)
		{
			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			fclose(fpDestFile);
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		fclose(fpDestFile);
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	if(fread(&FileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpDestFile) < 1)
		{
			m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			return FALSE;	
		}
	}
	return TRUE;
}
*/
/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	결과데이터 파일 복구.
-- Author		:
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
int CCyclerChannel::WriteRestoreRawFile(CString strLocalFileName, FILE *fpDestFile, PS_RAW_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo)
{
	int nRestordDataSize = 0;
	//int lReadData[PS_MAX_ITEM_NUM] = { 0 };
	int lReadData[PS_RESTORE_MAX_ITEM_NUM] = { 0 };		//ljb 201007
	int nAuxCount = 0, nCanCount = 0;
	//char szBuff[128];

	float* pfBuff = new float[FileHeader.rsHeader.nColumnCount];

	CStdioFile sourceFile;
	CFileException ex;
	CString strReadData;
	
// 	FILE *fpBackUp = fopen(strLocalFileName,"rt");
// 	if(fpBackUp)
	if(sourceFile.Open(strLocalFileName,CFile::modeRead,&ex))
	{
		int nData1 = 0, nData2 = 0, nData3 = 0;
		BOOL bIsNotEOL;				// 라인이 존재하는지 확인.
		BOOL bFind = FALSE;
// 		char temp[23][20];
// 		for(int i = 0 ; i < 23; i++)
// 		{
// 			fscanf(fpBackUp, "%s", temp[i]);
// 		}

		//ljb 201007  Title 주석 -> 버림
		//index, totalTime, stepTime, state, type, mode, select, code, grade, stepNo, Vsens, Isens, charge_AmpareHour, discharge_AmpareHour, capacitance, watt, charge_WattHour, discharge_WattHour, impedance, reservedCmd, external_comm_state(201012 추가), auxCount, canCount, totalCycle, elementCycle, accCycle1, accCycle2, accCycle3, accCycle4, accCycle5, multiCycle1, multiCycle2, multiCycle3, multiCycle4, multiCycle5, averageV, averageI, cvTime, realDate, realClock, Vinput(201012 ljb add), VPower(201012 ljb add), VBus(201012 ljb add)
		bIsNotEOL=sourceFile.ReadString(strReadData);    // 한 LINE 읽는다.
		
		while(TRUE)
		{
			strReadData.Empty();
			ZeroMemory(lReadData,sizeof(lReadData));
			bIsNotEOL=sourceFile.ReadString(strReadData);    // 한 문자열씩 읽는다.
			if(!bIsNotEOL)	break;

			//모듈에서 가져온 복구파일을 읽어 들인다.			
			if (Fun_ConverToString(strReadData, lReadData,nData2))
			{
				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
				if(lReadData[PS_DATA_SEQ] >= nStartNo && lReadData[PS_DATA_SEQ] <= nEndNo)
				{
					bFind = TRUE;
					//복구 실시 
					for(int a =0; a<FileHeader.rsHeader.nColumnCount; a++)
					{
						if( ( PS_WATT_HOUR == FileHeader.rsHeader.awColumnItem[a] || PS_CAPACITY == FileHeader.rsHeader.awColumnItem[a] ) &&
							(float)ConvertSbcToPCUnit(lReadData[FileHeader.rsHeader.awColumnItem[a]], FileHeader.rsHeader.awColumnItem[a]) == 0)
						{
							float fTemp = (float)ConvertSbcToPCUnit(lReadData[FileHeader.rsHeader.awColumnItem[a+1]], FileHeader.rsHeader.awColumnItem[a+1]);
							float fTemp2 = (float)ConvertSbcToPCUnit(lReadData[FileHeader.rsHeader.awColumnItem[a+2]], FileHeader.rsHeader.awColumnItem[a+2]);
							pfBuff[a] = fTemp + fTemp2;
						}
						//ksj 20170209 : 날짜 값 왜곡 현상 수정. PS_SYNC_DATE는 float 변수에 long 형으로 값을 복원한다.
						else if(PS_SYNC_DATE == FileHeader.rsHeader.awColumnItem[a] )
						{
							ULONG ulSyncDate = lReadData[FileHeader.rsHeader.awColumnItem[a]];
							memcpy(&pfBuff[a], &ulSyncDate, sizeof(long));
						}
						//ksj end ------------------------------------------------------------------
						else{
							pfBuff[a] = (float)ConvertSbcToPCUnit(lReadData[FileHeader.rsHeader.awColumnItem[a]], FileHeader.rsHeader.awColumnItem[a]);
						}
					}
					
					if(fwrite(pfBuff, sizeof(float), FileHeader.rsHeader.nColumnCount, fpDestFile) < 1)
					{
						//TRACE("Backup File write Error %s\n", destFileName);
						//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
						//break;
					}
					nRestordDataSize++;
//								TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
				}
			}
			else
			{
				//bRead = FALSE;
				break;
			}
										
		}	//end While

		//SBC Index 파일에는 있는데 실제 파일에 존재 하지 않으면(SBC 저장 오류) 복구수가 없을 수 있다.
		if(nRestordDataSize < 1)
		{
			nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
		}
		//fclose(fpBackUp);
		sourceFile.Close();
		delete [] pfBuff;
	}
	else 
	{	//SBC에서 download한 backup 파일 open 실패 
		nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
	}
	return nRestordDataSize;
}

int CCyclerChannel::WriteRestoreAuxFile(CString strTFileName, CString strVFileName, FILE *fpDestFile, PS_AUX_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo)
{
	int nRestordDataSize = 0;
	int nAuxCount = 0;
	int nDataIndex = 0;
// 	FILE *fpTBackUp = NULL;
// 	FILE *fpVBackUp = NULL;

	BOOL bOpenTBackUp(FALSE),bOpenVBackUp(FALSE);
	CStdioFile TBackUpFile;
	CStdioFile VBackUpFile;
	CFileException ex1;
	CFileException ex2;
	CString strReadData1,strReadData2;
	BOOL bIsNotEOL1(FALSE);    // 라인이 존재하는지 확인.
	BOOL bIsNotEOL2(FALSE);    // 라인이 존재하는지 확인.

	long lBuff[PS_MAX_AUX_SAVE_FILE_COLUMN] = {0,};
	float* pfBuff = new float[FileHeader.auxHeader.nColumnCount];
	//ZeroMemory(pfBuff,sizeof(float)*FileHeader.auxHeader.nColumnCount);

	int nAuxTemp=0,nAuxVolt=0;
	for (int i=0; i<FileHeader.auxHeader.nColumnCount; i++)
	{
		if (FileHeader.auxHeader.awColumnItem[i] == PS_AUX_TEMPERATURE) nAuxTemp++;
		if (FileHeader.auxHeader.awColumnItem[i] == PS_AUX_VOLTAGE) nAuxVolt++;

	}



	//ljb7
	if(nAuxTemp > 0)
	{
		//fpTBackUp = fopen(strTFileName,"rt");
		bOpenTBackUp = TBackUpFile.Open(strTFileName,CFile::modeRead,&ex1);
	}
	if(nAuxVolt > 0)
	{
		//fpVBackUp = fopen(strVFileName,"rt");
		bOpenVBackUp = VBackUpFile.Open(strVFileName,CFile::modeRead,&ex2);
	}

	//if(fpTBackUp || fpVBackUp)
	if(bOpenTBackUp || bOpenVBackUp)
	{
		int nIndex = 0, nData1 = 0, nData2 = 0;
		ULONG ulIndex;
		BOOL bFind = FALSE;
//		char temp[PS_MAX_AUX_SAVE_FILE_COLUMN][20];
		
		if(bOpenTBackUp)
		{
// 			for(int i = 0 ; i < m_chFileDataSet.m_nAuxTempCount+3; i++)
// 				fscanf(fpTBackUp, "%s", temp[i]);
			//ljb 201007 Title Line 버림
			bIsNotEOL1=TBackUpFile.ReadString(strReadData1);    // 한 문자열씩 읽는다.
		}
		
		if(bOpenVBackUp)
		{
// 			for(int i = 0 ; i < m_chFileDataSet.m_nAuxVoltCount+3; i++)
// 				fscanf(fpVBackUp, "%s", temp[i]);
			//ljb 201007 Title Line 버림
			bIsNotEOL2=VBackUpFile.ReadString(strReadData2);    // 한 문자열씩 읽는다.
		}

		while(1)
		{
			ulIndex = 0;
			//모듈에서 가져온 복구파일을 읽어 들인다.
			//////////////////////////////////////////////////////////////////////////
			//index, totalTime, stepTime, value(1), value(2), value(3)
			//ZeroMemory(pfBuff,sizeof(float)*FileHeader.auxHeader.nColumnCount);
			if(bOpenTBackUp)
			{
				//fscanf(fpTBackUp, "%d, %d, %d,", &nIndex, &nData1, &nData2);
				bIsNotEOL1=TBackUpFile.ReadString(strReadData1);    // 한 문자열씩 읽는다.
				if (bIsNotEOL1)
				{ 
					Fun_ConverToStringAux(strReadData1, lBuff, 0, m_chFileDataSet.m_nAuxTempCount,ulIndex);
				}
				else
					break;
			}

			if(bOpenVBackUp)
			{
				//fscanf(fpVBackUp, "%d, %d, %d,", &nIndex, &nData1, &nData2);
				bIsNotEOL2=VBackUpFile.ReadString(strReadData2);    // 한 문자열씩 읽는다.
				if (bIsNotEOL2)
				{ 
					//Fun_ConverToStringAux(strReadData2, lBuff, m_chFileDataSet.m_nAuxTempCount, m_chFileDataSet.m_nAuxVoltCount,ulIndex);
					Fun_ConverToStringAux(strReadData2, lBuff, nAuxTemp, nAuxVolt,ulIndex);
				}
				else
					break;

			}

			//memcpy(pfBuff,lBuff,FileHeader.auxHeader.nColumnCount);
			for(int iIndex=0 ; iIndex < FileHeader.auxHeader.nColumnCount ; iIndex++)
			{
				//Type에 따라 데이터 변경
				WORD wType = FileHeader.auxHeader.awColumnItem[iIndex];
				pfBuff[iIndex] = (float)Fun_ConvertToFloat(wType, lBuff[iIndex]);
			}

			if (bIsNotEOL1 || bIsNotEOL2 )
			{
				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
				if( ulIndex >= nStartNo && ulIndex <= nEndNo )
				{
					bFind = TRUE;
					//복구 실시 
					
					if(fwrite(pfBuff, sizeof(float), FileHeader.auxHeader.nColumnCount, fpDestFile) < 1)
					{
						//TRACE("Backup File write Error %s\n", destFileName);
						//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
						//break;
					}
					nRestordDataSize++;
					//if(pfBuff[0] == 10) TRACE("Aux Data Restore : 10\r\n");
					//TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
				}
			}
			if(ulIndex >= nEndNo) break;
			
		}	//end While
		if(bOpenTBackUp) TBackUpFile.Close();
		if(bOpenVBackUp)VBackUpFile.Close();
		
		// 		if(fpTBackUp)	fclose(fpTBackUp);
		// 		if(fpVBackUp)	fclose(fpVBackUp);
		if(pfBuff != NULL)	delete [] pfBuff;
	}
	return nRestordDataSize;
}
			
			
// 			if(nIndex == 300)
// 				nIndex = nIndex+0;
// 		
// 				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
// 			if(nIndex >= nStartNo && nIndex <= nEndNo)
// 			{
// 				bFind = TRUE;
// 				//복구 실시 
// 									
// 				if(fwrite(pfBuff, sizeof(float), FileHeader.auxHeader.nColumnCount, fpDestFile) < 1)
// 				{
// 					//TRACE("Backup File write Error %s\n", destFileName);
// 					//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
// 					//break;
// 				}
// 				nRestordDataSize++;
// 				if(nIndex == 1052)
// 					TRACE("Aux Data Restore : %d\r\n", nIndex);
// 					//TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
// 			}
// 			if(nIndex >= nEndNo)
// 				break;
										


int CCyclerChannel::WriteRestoreCanFile(CString strMFileName, CString strSFileName, FILE *fpDestFile, PS_CAN_FILE_HEADER &FileHeader, int nStartNo, int nEndNo, int nBackupFileNo)
{
	int nRestordDataSize = 0;
	int nDataIndex = 0;
	long	lHigh =0, lLow =0;
	ULONG ulIndex;
// 	FILE *fpMBackUp = NULL;
// 	FILE *fpSBackUp = NULL;

	BOOL bOpenMBackUp(FALSE),bOpenSBackUp(FALSE);
	CStdioFile MBackUpFile;
	CStdioFile SBackUpFile;
	CFileException ex1;
	CFileException ex2;
	CString strReadData1,strReadData2;
	BOOL bIsNotEOL1(FALSE);    // 라인이 존재하는지 확인.
	BOOL bIsNotEOL2(FALSE);    // 라인이 존재하는지 확인.

	CAN_VALUE lBuff[PS_MAX_CAN_SAVE_FILE_COLUMN] = {0,};
	//CAN_VALUE* pBuff = new CAN_VALUE[FileHeader.canHeader.nColumnCount];
	
	//ljb7
	if(m_chFileDataSet.m_nCanMasterCount > 0)
	{
		//fpTBackUp = fopen(strTFileName,"rt");
		bOpenMBackUp = MBackUpFile.Open(strMFileName,CFile::modeRead,&ex1);
	}
	if(m_chFileDataSet.m_nCanSlaveCount > 0)
	{
		//fpVBackUp = fopen(strVFileName,"rt");
		bOpenSBackUp = SBackUpFile.Open(strSFileName,CFile::modeRead,&ex2);
	}
	
// 	if(m_chFileDataSet.m_nCanMasterCount > 0)
// 		fpMBackUp = fopen(strMFileName,"rt");
// 	if(m_chFileDataSet.m_nCanSlaveCount > 0)
// 		fpSBackUp = fopen(strSFileName,"rt");
// 
// 	if(fpMBackUp || fpSBackUp)
// 	{
	if(bOpenMBackUp || bOpenSBackUp)
	{
		int nIndex = 0, nData1 = 0, nData2 = 0;
		BOOL bFind = FALSE;
//		char temp[PS_MAX_CAN_SAVE_FILE_COLUMN][20];
		
// 		if(fpMBackUp)
// 		{
// 			for(int i = 0 ; i < m_chFileDataSet.m_nCanMasterCount+3; i++)
// 				fscanf(fpMBackUp, "%s", temp[i]);
// 		}
// 		if(fpSBackUp)
// 		{
// 			for(int i = 0 ; i < m_chFileDataSet.m_nCanSlaveCount+3; i++)
// 				fscanf(fpSBackUp, "%s", temp[i]);
// 		}
		if (bOpenMBackUp)
			bIsNotEOL1=MBackUpFile.ReadString(strReadData1);    // 한 문자열씩 읽는다.
		if (bOpenSBackUp)
			bIsNotEOL2=SBackUpFile.ReadString(strReadData2);    // 한 문자열씩 읽는다.

		while(1)
		{
			ulIndex = 0;

			//모듈에서 가져온 복구파일을 읽어 들인다.
			//////////////////////////////////////////////////////////////////////////
			//index, totalTime, stepTime, value(1), value(2), value(3), value(4), value(5), value(6)
			if(bOpenMBackUp)
			{
				bIsNotEOL1=MBackUpFile.ReadString(strReadData1);    // 한 문자열씩 읽는다.
				if (bIsNotEOL1)
				{ 
					Fun_ConverToStringCan(strReadData1, lBuff, 0, m_chFileDataSet.m_nCanMasterCount,ulIndex);
				}
				else
					break;
			}

			if(bOpenSBackUp)
			{
				bIsNotEOL2=SBackUpFile.ReadString(strReadData2);    // 한 문자열씩 읽는다.
				if (bIsNotEOL2)
				{ 
					Fun_ConverToStringCan(strReadData2, lBuff, m_chFileDataSet.m_nCanMasterCount, m_chFileDataSet.m_nCanSlaveCount,ulIndex);
				}
				else
					break;
			}

			if (bIsNotEOL1 || bIsNotEOL2 )
			{
				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
				if( ulIndex >= nStartNo && ulIndex <= nEndNo)
				{
					bFind = TRUE;
					//복구 실시 
					
					if(fwrite(lBuff, sizeof(CAN_VALUE), FileHeader.canHeader.nColumnCount, fpDestFile) < 1)
					{
						//TRACE("Backup File write Error %s\n", destFileName);
						//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
						//break;
					}
					nRestordDataSize++;
					//if(pfBuff[0] == 10) TRACE("Aux Data Restore : 10\r\n");
					//TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
				}
			}
			if(ulIndex >= nEndNo) break;
		}	//end While
		if(bOpenMBackUp) MBackUpFile.Close();
		if(bOpenSBackUp) SBackUpFile.Close();
	}
	return nRestordDataSize;
}
// 			if(fpMBackUp)
// 			{
// 				fscanf(fpMBackUp, "%d, %d, %d,", &nIndex, &nData1, &nData2);
// 				for(nDataIndex = 0 ; nDataIndex < m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
// 				{
// 			//		fscanf(fpMBackUp, "%d, %d", &lHigh, &lLow);
// 			//		lBuff[nDataIndex].lVal[1] = lHigh;
// 			//		lBuff[nDataIndex].lVal[0] = lLow;
// 					fscanf(fpMBackUp, "%f,", &lBuff[nDataIndex].fVal[0]);
// 					
// 				}
// 			}

// 			if(fpSBackUp)
// 			{
// 				fscanf(fpSBackUp, "%d, %d, %d,", &nIndex, &nData1, &nData2);
// 				for(; nDataIndex < m_chFileDataSet.m_nCanSlaveCount+m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
// 				{
// 			//		fscanf(fpSBackUp, "%d, %d", &lHigh, &lLow);
// 			//		lBuff[nDataIndex].lVal[1] = lHigh;
// 			//		lBuff[nDataIndex].lVal[0] = lLow;
// 					fscanf(fpSBackUp, "%f,", &lBuff[nDataIndex].fVal[0]);
// 				}
// 			}		
		
				//복구 해야할 구간의 data이면 복구용 파일에 write 한다.
// 			if(nIndex >= nStartNo && nIndex <= nEndNo)
// 			{
// 				bFind = TRUE;
// 				//복구 실시 
// 									
// 				if(fwrite(lBuff, sizeof(CAN_VALUE), FileHeader.canHeader.nColumnCount, fpDestFile) < 1)
// 				{
// 					//TRACE("Backup File write Error %s\n", destFileName);
// 					//nRestordDataSize = m_RestoreFileIndexTable.GetRawDataSize(nSquenceNo);	//DownLoad한 backup 파일에서 더이상 복구 불가하다.
// 					//break;
// 				}
// 				nRestordDataSize++;
// 			
// //								TRACE("Data restord %d(%d)\n", lReadData[PS_DATA_SEQ], nRestordDataSize);
// 			}
// 			if(nIndex >= nEndNo)
// 				break;
// 										
// 		}	//end While
// 
// 		if(fpMBackUp)	fclose(fpMBackUp);
// 		if(fpSBackUp)	fclose(fpSBackUp);
// 	}
// 	return nRestordDataSize;
// }

BOOL CCyclerChannel::RemakeAuxPCIndexFile(CString strBackUpRawFile, CString strAuxTStepEndFile, CString strAuxVStepEndFile, CString strNewTableFile)
{
	//파일명 설정 여부 확인 
	if(strBackUpRawFile.IsEmpty() || strNewTableFile.IsEmpty())
	{
		return FALSE;
	}

	COleDateTime t = COleDateTime::GetCurrentTime();
	CFile		fRS;
	CFileException e;

	//////////////////////////////////////////////////////////////////////////
	//복구된 Raw Data와 모듈에서 download한 복구용 Table file을 이용하여 PC용 Table 파일을 생성한다.
	//파일을 Open 한다.
	if( !fRS.Open( strBackUpRawFile, CFile::modeRead, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   m_strLastErrorString.Format("%s Open file fail.", strBackUpRawFile);
	   return FALSE;
	}

	int nColumnSize = 0;
	PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
	if(fRS.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER)) > 0)
	{
		nColumnSize = pFileHeader->auxHeader.nColumnCount;
	}
	
	delete pFileHeader;
	fRS.Close();
	
	//파일이 잘못되었을 경우 
	if(nColumnSize < 1 || nColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
	{
	   //m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", strBackUpRawFile, nColumnSize);
	   m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeAuxPCIndexFile_msg1","CYCLERCHANNEL"), strBackUpRawFile, nColumnSize);//&&
		return FALSE;	//실제 Data 분석 
	}
	
	int nTotalVolCount = 0;

	nTotalVolCount = m_chFileDataSet.m_nAuxVoltCount + m_chFileDataSet.m_nAuxTempTHCount; //ksj 20210806 : 현재 기준, 습도 센서 SBC에서 데이터 기록 안하는 듯 함. 협의해서 늘려야함.
	//모듈에서 전송받은 Step End 파일 Open
	char temp[PS_MAX_AUX_SAVE_FILE_COLUMN][20];
	
	FILE *pAuxTFile = NULL;
	FILE *pAuxVFile = NULL;
	if(strAuxTStepEndFile != "")
	{
		pAuxTFile = fopen(strAuxTStepEndFile, "rt");
		if(pAuxTFile == NULL)
		{//$1013
			//m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeAuxPCIndexFile_msg2","CYCLERCHANNEL"), strBackUpRawFile);//&&
			fclose(pAuxTFile);
			return FALSE;	//실제 Data 분석 
		}

		for(int i = 0 ; i < m_chFileDataSet.m_nAuxTempCount + 5 ; i++)	//ljb 20140114 edit 3->5
		{
			fscanf(pAuxTFile, "%s,", temp[i]);
		}
	}
	if(strAuxVStepEndFile != "")
	{
		pAuxVFile = fopen(strAuxVStepEndFile, "rt");
		if(pAuxVFile == NULL)
		{//$1013
			//m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeAuxPCIndexFile_msg3","CYCLERCHANNEL"), strBackUpRawFile);//&&
			fclose(pAuxTFile);
			fclose(pAuxVFile);
			return FALSE;	//실제 Data 분석 
		}
		//for( int i = 0; i < m_chFileDataSet.m_nAuxVoltCount + 5; i++)	//ljb 20140114 edit 3->5
		for( int i = 0; i < nTotalVolCount + 5; i++)	//ksj 20210806
		{
			fscanf(pAuxVFile, "%s,", temp[i]);
		}
	}
		
	float *pfBuff = new float[nColumnSize];	
	int lReadData[PS_MAX_AUX_SAVE_FILE_COLUMN] = { 0 };
	int nIndex, nData1, nData2, nData3, nData4;	
	
	CFile auxFile;
	if( !auxFile.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	{
		//생성 실패
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
			m_strLastErrorString.Format("%s create fail.", strNewTableFile);
		
			return FALSE;
	}

	pFileHeader = new PS_AUX_FILE_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
	pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
	pFileHeader->fileHeader.nFileVersion = 0x1000;
	sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
	sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			

	pFileHeader->auxHeader.nColumnCount = nColumnSize;

	for(int i = 0; i < nColumnSize; i++)
	{
		pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
	}

	auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
	
	int nDataIndex = 0 ;
	while (1)
	{
		if(pAuxTFile)
		{
			if(fscanf(pAuxTFile, "%d, %d, %d, %d, %d,", &nIndex, &nData1, &nData2, &nData3, &nData4) > 0)
			{
				for(nDataIndex = 0 ; nDataIndex < m_chFileDataSet.m_nAuxTempCount; nDataIndex++)
				{
					fscanf(pAuxTFile, "%d,", &lReadData[nDataIndex]);
					pfBuff[nDataIndex] = lReadData[nDataIndex];
				}
			}
			else
				break;
		}
		if(pAuxVFile)
		{
			if(fscanf(pAuxVFile, "%d, %d, %d, %d, %d,", &nIndex, &nData1, &nData2, &nData3, &nData4) > 0)
			{				
				//for(nDataIndex ; nDataIndex < m_chFileDataSet.m_nAuxVoltCount + m_chFileDataSet.m_nAuxTempCount; nDataIndex++)
				for(nDataIndex ; nDataIndex < nTotalVolCount + m_chFileDataSet.m_nAuxTempCount; nDataIndex++) //ksj 20210806
				{
					fscanf(pAuxVFile, "%d,", &lReadData[nDataIndex]);
					pfBuff[nDataIndex] = lReadData[nDataIndex];
				}
			}
			else
				break;
		}
		
		auxFile.Write(pfBuff, sizeof(float) * nColumnSize);
	}
	auxFile.Close();
	if(pAuxTFile)	fclose(pAuxTFile);
	if(pAuxVFile)	fclose(pAuxVFile);
	delete [] pfBuff;
	delete [] pFileHeader;

	return TRUE;
}

BOOL CCyclerChannel::RemakeAuxPCIndexFile2(CString strBackUpRawFile, CString strAuxTStepEndFile, CString strAuxVStepEndFile, CString strNewTableFile)
{
	//파일명 설정 여부 확인 
	if(strBackUpRawFile.IsEmpty() || strNewTableFile.IsEmpty())
	{
		return FALSE;
	}

	COleDateTime t = COleDateTime::GetCurrentTime();
	CFile		fRS;
	CFileException e;

	//////////////////////////////////////////////////////////////////////////
	//복구된 Raw Data와 모듈에서 download한 복구용 Table file을 이용하여 PC용 Table 파일을 생성한다.
	//파일을 Open 한다.
	if( !fRS.Open( strBackUpRawFile, CFile::modeRead, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   m_strLastErrorString.Format("%s Open file fail.", strBackUpRawFile);
	   return FALSE;
	}

	int nColumnSize = 0;
	PS_AUX_FILE_HEADER *pFileHeader = new PS_AUX_FILE_HEADER;
	if(fRS.Read(pFileHeader, sizeof(PS_AUX_FILE_HEADER)) > 0)
	{
		nColumnSize = pFileHeader->auxHeader.nColumnCount;
	}
	
	delete pFileHeader;
	fRS.Close();
	
	//파일이 잘못되었을 경우 
	if(nColumnSize < 1 || nColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
	{//$1013
	   //m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", strBackUpRawFile, nColumnSize);
	   m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeAuxPCIndexFile2_msg1","CYCLERCHANNEL"), strBackUpRawFile, nColumnSize);//&&
		return FALSE;	//실제 Data 분석 
	}
	

	//모듈에서 전송받은 Step End 파일 Open
	int nAuxTempCnt=0,nAuxVoltCnt=0;
	char temp[PS_MAX_AUX_SAVE_FILE_COLUMN][20];
	
	FILE *pAuxTFile = NULL;
	FILE *pAuxVFile = NULL;
	if(strAuxTStepEndFile != "")
	{
		pAuxTFile = fopen(strAuxTStepEndFile, "rt");
		if(pAuxTFile == NULL)
		{//$1013
			//m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeAuxPCIndexFile2_msg2","CYCLERCHANNEL"), strBackUpRawFile);//&&
			fclose(pAuxTFile);
			return FALSE;	//실제 Data 분석 
		}

		for(int i = 0 ; i < m_chFileDataSet.m_nAuxTempCount +3 ; i++)
		{
			fscanf(pAuxTFile, "%s,", temp[i]);
			nAuxTempCnt++;
		}
	}
	if(strAuxVStepEndFile != "")
	{
		pAuxVFile = fopen(strAuxVStepEndFile, "rt");
		if(pAuxVFile == NULL)
		{//$1013
			//m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeAuxPCIndexFile2_msg3","CYCLERCHANNEL"), strBackUpRawFile);//&&
			fclose(pAuxTFile);
			fclose(pAuxVFile);
			return FALSE;	//실제 Data 분석 
		}

		for( int i = 0; i < m_chFileDataSet.m_nAuxVoltCount +3; i++)
		{
			fscanf(pAuxVFile, "%s,", temp[i]);
			nAuxVoltCnt++;
		}
	}
		
	float *pfBuff = new float[nColumnSize];	
	int lReadData[PS_MAX_AUX_SAVE_FILE_COLUMN] = { 0 };
	int nIndex, nData1, nData2;	
	
	CFile auxFile;
	if( !auxFile.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	{
		//생성 실패
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif//$1013
			//m_strLastErrorString.Format("%s를 생성할 수 없습니다.", strNewTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeAuxPCIndexFile2_msg4","CYCLERCHANNEL"), strNewTableFile);//&&
		
			return FALSE;
	}

	pFileHeader = new PS_AUX_FILE_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_AUX_FILE_HEADER));
	pFileHeader->fileHeader.nFileID = PS_PNE_AUX_RESULT_FILE_ID;
	pFileHeader->fileHeader.nFileVersion = 0x1000;
	sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
	sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Aux end data file.");			

	pFileHeader->auxHeader.nColumnCount = nColumnSize;

	for(int i = 0; i < nColumnSize; i++)
	{
		pFileHeader->auxHeader.awColumnItem[i] = PS_AUX_VALUE;			//auxValue
	}

	auxFile.Write(pFileHeader, sizeof(PS_AUX_FILE_HEADER));	
	
	int nDataIndex = 0 ;
	while (1)
	{
		if(pAuxTFile)
		{
			if(fscanf(pAuxTFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			{
				for(nDataIndex = 0 ; nDataIndex < nAuxTempCnt; nDataIndex++)
				{
					fscanf(pAuxTFile, "%d,", &lReadData[nDataIndex]);
					pfBuff[nDataIndex] = lReadData[nDataIndex];
				}
			}
			else
				break;
		}
		if(pAuxVFile)
		{
			if(fscanf(pAuxVFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			{
				for(nDataIndex ; nDataIndex < nAuxVoltCnt + nAuxTempCnt; nDataIndex++)
				{
					fscanf(pAuxVFile, "%d,", &lReadData[nDataIndex]);
					pfBuff[nDataIndex] = lReadData[nDataIndex];
				}
			}
			else
				break;
		}
		
		auxFile.Write(pfBuff, sizeof(float) * nColumnSize);
	}
	auxFile.Close();
	if(pAuxTFile)	fclose(pAuxTFile);
	if(pAuxVFile)	fclose(pAuxVFile);
	delete [] pfBuff;
	delete [] pFileHeader;

	return TRUE;
}

BOOL CCyclerChannel::RemakeCanPCIndexFile(CString strBackUpRawFile, CString strCanMStepEndFile, CString strCanSStepEndFile, CString strNewTableFile)
{
	//파일명 설정 여부 확인 
	if(strBackUpRawFile.IsEmpty() || strNewTableFile.IsEmpty())
	{
		return FALSE;
	}

	COleDateTime t = COleDateTime::GetCurrentTime();
	CFile		fRS;
	CFileException e;

	//////////////////////////////////////////////////////////////////////////
	//복구된 Raw Data와 모듈에서 download한 복구용 Table file을 이용하여 PC용 Table 파일을 생성한다.
	//파일을 Open 한다.
	if( !fRS.Open( strBackUpRawFile, CFile::modeRead, &e ) )
	{
	#ifdef _DEBUG
	   afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	   m_strLastErrorString.Format("%s Open fail.", strBackUpRawFile);
	   return FALSE;
	}

	int nColumnSize = 0;
	PS_CAN_FILE_HEADER *pFileHeader = new PS_CAN_FILE_HEADER;
	if(fRS.Read(pFileHeader, sizeof(PS_CAN_FILE_HEADER)) > 0)
	{
		nColumnSize = pFileHeader->canHeader.nColumnCount;
	}
	
	delete pFileHeader;
	fRS.Close();
	
	//파일이 잘못되었을 경우 
	if(nColumnSize < 1 || nColumnSize > PS_MAX_CAN_SAVE_FILE_COLUMN)
	{//$1013
	   //m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", strBackUpRawFile, nColumnSize);
	   m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeCanPCIndexFile_msg1","CYCLERCHANNEL"), strBackUpRawFile, nColumnSize);//&&
		return FALSE;	//실제 Data 분석 
	}
	

	//모듈에서 전송받은 Step End 파일 Open
	char temp[PS_MAX_CAN_SAVE_FILE_COLUMN][20];
	
	FILE *pCanMFile = NULL;
	FILE *pCanSFile = NULL;
	if(strCanMStepEndFile != "")
	{
		pCanMFile = fopen(strCanMStepEndFile, "rt");
		if(pCanMFile == NULL)
		{//$1013
			//m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeCanPCIndexFile_msg2","CYCLERCHANNEL"), strBackUpRawFile);//&&
			fclose(pCanMFile);
			return FALSE;	//실제 Data 분석 
		}

		//for(int i = 0 ; i < m_chFileDataSet.m_nCanMasterCount +3 ; i++)
		for(int i = 0 ; i < m_chFileDataSet.m_nCanMasterCount + 5 ; i++) //ksj 20210806 : 3->5 오류 수정
		{
			fscanf(pCanMFile, "%s,", temp[i]);
		}
	}
	if(strCanSStepEndFile != "")
	{
		pCanSFile = fopen(strCanSStepEndFile, "rt");
		if(pCanSFile == NULL)
		{//$1013
			//m_strLastErrorString.Format("모듈의 step end 정보를 열수 없습니다.", strBackUpRawFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RemakeCanPCIndexFile_msg3","CYCLERCHANNEL"), strBackUpRawFile);//&&
			fclose(pCanMFile);
			fclose(pCanSFile);
			return FALSE;	//실제 Data 분석 
		}

		//for( int i = 0; i < m_chFileDataSet.m_nCanSlaveCount +3; i++)
		for( int i = 0; i < m_chFileDataSet.m_nCanSlaveCount + 5; i++)  //ksj 20210806 : 3->5 오류 수정
		{
			fscanf(pCanSFile, "%s,", temp[i]);
		}
	}
		
	//CAN *pfBuff = new float[nColumnSize];	
	CAN_VALUE lReadData[PS_MAX_CAN_SAVE_FILE_COLUMN] = { 0 };
	int nIndex, nData1, nData2;	
	int nData3, nData4; //ksj 20210806 : 누락 사항 추가.
	
	CFile canFile;
	if( !canFile.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	{
		//생성 실패
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
			m_strLastErrorString.Format("%s create fail.", strNewTableFile);
		
			return FALSE;
	}

	pFileHeader = new PS_CAN_FILE_HEADER;
	ZeroMemory(pFileHeader, sizeof(PS_CAN_FILE_HEADER));
	pFileHeader->fileHeader.nFileID = PS_PNE_CAN_RESULT_FILE_ID;
	pFileHeader->fileHeader.nFileVersion = 0x1000;
	sprintf(pFileHeader->fileHeader.szCreateDateTime, "%s", t.Format());
	sprintf(pFileHeader->fileHeader.szDescrition, "PNE Cycler Can end data file.");			

	pFileHeader->canHeader.nColumnCount = nColumnSize;

	for(int i = 0; i < nColumnSize; i++)
	{
		pFileHeader->canHeader.awColumnItem[i] = PS_CAN_VALUE;			//auxValue
	}

	canFile.Write(pFileHeader, sizeof(PS_CAN_FILE_HEADER));	
	
	int nDataIndex = 0 ;
	while (1)
	{
		if(pCanMFile != NULL)
		{
			//if(fscanf(pCanMFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			if(fscanf(pCanMFile, "%d, %d, %d, %d, %d,", &nIndex, &nData1, &nData2, &nData3, &nData4) > 0) //ksj 20210806 : 오류 수정
			{
				for(nDataIndex = 0 ; nDataIndex < m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
				{
					if (fscanf(pCanMFile, "%f,", &lReadData[nDataIndex].fVal[0]) > 0)
					{
						//정상
					}
					else
					{
						break;		//읽기 이상
					}
				}
			}
			else
			{
				break;		//읽기 이상
			}
		}
		if(pCanSFile != NULL)
		{
			//if(fscanf(pCanSFile, "%d, %d, %d,", &nIndex, &nData1, &nData2) > 0)
			if(fscanf(pCanSFile, "%d, %d, %d, %d, %d,", &nIndex, &nData1, &nData2, &nData3, &nData4) > 0) //ksj 20210806 : 오류 수정
			{
				for(nDataIndex ; nDataIndex < m_chFileDataSet.m_nCanSlaveCount + m_chFileDataSet.m_nCanMasterCount; nDataIndex++)
				{
					//if (fscanf(pCanSFile, "%d,", &lReadData[nDataIndex].fVal[0]) >0)
					if (fscanf(pCanSFile, "%f,", &lReadData[nDataIndex].fVal[0]) > 0) //ksj 20210806 : float 형으로 변경
					{
						//정상
					}
					else
					{
						break;	//읽기 이상
					}
				}
			}
			else
			{
				break;		//읽기 이상
			}
		}
		
		canFile.Write(&lReadData, sizeof(CAN_VALUE) * nColumnSize);
	}
	canFile.Close();
	if(pCanMFile)	fclose(pCanMFile);
	if(pCanSFile)	fclose(pCanSFile);
	//delete [] pfBuff;
	delete [] pFileHeader;

	return TRUE;
}

//2008/07/24  lChargeAh, lDisChargeAh, lCapacitance, lChargeWh, lDisChargeWh, ulCVTime, szSyncTime 추가
BOOL CCyclerChannel::GetSaveDataD(PS_STEP_END_RECORD &sChData, 
								  PS_AUX_RAW_DATA &sAuxData, 
								  PS_CAN_RAW_DATA &sCanData, 
								  PS_OVEN_RAW_DATA &sOvenData, 
								  PS_LOAD_RAW_DATA &sLoaderData,
								  PS_OVEN_RAW_DATA &sChillerData,
								  PS_CELLBAL_RAW_DATA &sCellBALData,
								  PS_PWRSUPPLY_RAW_DATA &sPwrSupplyData/*,//yulee 20190514 cny
								  PS_STEP_END_RECORD_EXTRA &sChDataExtra //ksj 20201013 : 버전업된 스텝엔드 구조체 추가.*/
								  )
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	CChannel *pChannel = ::SFTGetChannelData(m_nModuleID, m_nChannelIndex);
	if(pChannel == NULL)	return FALSE;

	CString strTemp;
	//double dTemp;
	SFT_VARIABLE_CH_DATA sSaveChData;
	ZeroMemory(&sSaveChData, sizeof(SFT_VARIABLE_CH_DATA));
	if(pChannel->PopSaveData(sSaveChData)== FALSE)	return FALSE;	
	
	sChData.chNo		  = sSaveChData.chData.chNo;					// Channel Number
	sChData.chState		  = sSaveChData.chData.chState;				// Run, Stop(Manual, Error), End
	sChData.chStepType    = sSaveChData.chData.chStepType;			// Charge, Discharge, OCV, Impedance(AC or DC), Rest
	sChData.chDataSelect  = sSaveChData.chData.chDataSelect;			// For Display Data, For Saving Data
	sChData.chCode		  = sSaveChData.chData.chCode;
	sChData.chStepNo	  = sSaveChData.chData.chStepNo;
	sChData.chGradeCode   = sSaveChData.chData.chGradeCode;
	sChData.chCommState   = sSaveChData.chData.chCommState;			//ljb 20100909
	sChData.chOutputState = sSaveChData.chData.chOutputState;		//ljb 20101222
	sChData.chInputState  = sSaveChData.chData.chInputState;			//ljb 20101222
	
	sChData.fVoltage	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage, PS_VOLTAGE);				// Result Data...
	sChData.fCurrent	= ConvertSbcToPCUnit(sSaveChData.chData.lCurrent, PS_CURRENT);

	sChData.fCapacity	= ConvertSbcToPCUnit(sSaveChData.chData.lChargeAh+sSaveChData.chData.lDisChargeAh, PS_CAPACITY);
	sChData.fChargeAh	= ConvertSbcToPCUnit(sSaveChData.chData.lChargeAh, PS_CAPACITY);
	sChData.fDisChargeAh= ConvertSbcToPCUnit(sSaveChData.chData.lDisChargeAh, PS_CAPACITY);
	sChData.fCapacitance= ConvertSbcToPCUnit(sSaveChData.chData.lCapacitance, PS_CAPACITY);

	sChData.fWatt		= ConvertSbcToPCUnit(sSaveChData.chData.lWatt, PS_WATT);
	sChData.fWattHour	= ConvertSbcToPCUnit(sSaveChData.chData.lChargeWh+sSaveChData.chData.lDisChargeWh, PS_WATT_HOUR);

	sChData.fChargeWh	 = ConvertSbcToPCUnit(sSaveChData.chData.lChargeWh, PS_WATT_HOUR);
	sChData.fDisChargeWh = ConvertSbcToPCUnit(sSaveChData.chData.lDisChargeWh, PS_WATT_HOUR);
	sChData.fCVTime_Day	 = sSaveChData.chData.ulCVDay;	//ljb 20131203 add
	sChData.fCVTime		 = ConvertSbcToPCUnit(sSaveChData.chData.ulCVTime, PS_STEP_TIME);
	//sChData.fSyncDate	 = ConvertSbcToPCUnit(sSaveChData.chData.lSyncTime[0], PS_SYNC_DATE);
	//sChData.fSyncTime	 = ConvertSbcToPCUnit(sSaveChData.chData.lSyncTime[1], PS_SYNC_TIME);
	//strTemp.Format("%d",sSaveChData.chData.lSyncTime[0]);	// PS_SYNC_DATE);
	//dTemp = atol(strTemp);
 	//sChData.fSyncDate	= LONG2FLOAT(sSaveChData.chData.lSyncTime[0]);	// PS_SYNC_DATE);
	//sChData.fSyncDate	= (float)sSaveChData.chData.lSyncTime[0]*1000;	// PS_SYNC_DATE);	//ljb 20131210 edit
	sChData.lSyncDate	= sSaveChData.chData.lSyncTime[0];	// PS_SYNC_DATE);	//ljb 20131210 edit

	//strTemp.Format("%d",sSaveChData.chData.lSyncTime[1]);	// PS_SYNC_DATE);
	//dTemp = atol(strTemp);
	sChData.fSyncTime	= LONG2FLOAT(sSaveChData.chData.lSyncTime[1]);	// PS_SYNC_TIME	

	//TRACE("Date = %d, Time = %d \n",sSaveChData.chData.lSyncTime[0], sSaveChData.chData.lSyncTime[1]);
	//TRACE("Date = %d, Time = %f \n",sChData.lSyncDate, sChData.fSyncTime);

	sChData.fStepTime_Day	= sSaveChData.chData.ulStepDay;			//ljb 20131203 add
	sChData.fStepTime	= ConvertSbcToPCUnit(sSaveChData.chData.ulStepTime, PS_STEP_TIME);			// 이번 Step 진행 시간
	sChData.fTotalTime_Day = sSaveChData.chData.ulTotalDay;			//ljb 20131203 add
	sChData.fTotalTime = ConvertSbcToPCUnit(sSaveChData.chData.ulTotalTime, PS_TOT_TIME);			// 시험 Total 진행 시간
	sChData.fImpedance	= ConvertSbcToPCUnit(sSaveChData.chData.lImpedance, PS_IMPEDANCE);			// Impedance (AC or DC)
	//sChData.fTemparature = ConvertSbcToPCUnit(sSaveChData.chData.lTemparature, PS_TEMPERATURE);
	sChData.fOvenTemperature = sSaveChData.ovenData.fCurTemper;		//℃ //ljb 201012 챔버 온도를 저장
	sChData.fOvenHumidity	 = sSaveChData.ovenData.fCurHumidity;	//%	 //ljb 201012 챔버 습도 저장
	sChData.nCurrentCycleNum = sSaveChData.chData.nCurrentCycleNum;
	sChData.nTotalCycleNum = sSaveChData.chData.nTotalCycleNum;
	sChData.nAccCycleGroupNum1	= sSaveChData.chData.nAccCycleGroupNum1;		//ljb v1009
	sChData.nAccCycleGroupNum2	= sSaveChData.chData.nAccCycleGroupNum2;		
	sChData.nAccCycleGroupNum3	= sSaveChData.chData.nAccCycleGroupNum3;		
	sChData.nAccCycleGroupNum4	= sSaveChData.chData.nAccCycleGroupNum4;		
	sChData.nAccCycleGroupNum5	= sSaveChData.chData.nAccCycleGroupNum5;		
	sChData.nMultiCycleGroupNum1	= sSaveChData.chData.nMultiCycleGroupNum1;		//ljb v1009
	sChData.nMultiCycleGroupNum2	= sSaveChData.chData.nMultiCycleGroupNum2;		
	sChData.nMultiCycleGroupNum3	= sSaveChData.chData.nMultiCycleGroupNum3;		
	sChData.nMultiCycleGroupNum4	= sSaveChData.chData.nMultiCycleGroupNum4;		
	sChData.nMultiCycleGroupNum5	= sSaveChData.chData.nMultiCycleGroupNum5;		
	sChData.lSaveSequence	= sSaveChData.chData.lSaveSequence;			// Expanded Field
	sChData.fAvgVoltage = ConvertSbcToPCUnit(sSaveChData.chData.lAvgVoltage, PS_VOLTAGE);
	sChData.fAvgCurrent = ConvertSbcToPCUnit(sSaveChData.chData.lAvgCurrent, PS_CURRENT);
	//ljb 201011
	sChData.fVoltageInput = ConvertSbcToPCUnit(sSaveChData.chData.lVoltage_Input, PS_VOLTAGE_INPUT);	// ljb 201011
	sChData.fVoltagePower	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage_Power, PS_VOLTAGE_POWER);	// ljb 201011
	sChData.fVoltageBus	= ConvertSbcToPCUnit(sSaveChData.chData.lVoltage_Bus, PS_VOLTAGE_BUS);			// ljb 201011
	sChData.nIndexFrom = 0;
	sChData.nIndexTo = 0;

	//TRACE("GetSaveDataC -> Capacity : %f, WattHour : %f\n",sChData.fCapacity, sChData.fWattHour);
	
	sAuxData.auxCount = sSaveChData.chData.nAuxCount;
	sAuxData.auxData->chNo = sSaveChData.chData.chNo;					// Channel Number
	for(int i = 0; i < sAuxData.auxCount; i++)
	{
		sAuxData.auxData[i].chNo = sSaveChData.chData.chNo;
		sAuxData.auxData[i].chAuxNo = sSaveChData.auxData[i].auxChNo;
		sAuxData.auxData[i].chType = sSaveChData.auxData[i].auxChType;
		if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TEMPERATURE);
		//else //20180607 yulee 주석처리
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_VOLTAGE)
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_VOLTAGE);
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_TEMPERATURE_TH) //20180607 yulee
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TYPE_TEMPERATURE_TH);
		else if(sAuxData.auxData[i].chType == PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : V1016 써미스터 온도. 향후 단위 변환 체크 필요
			sAuxData.auxData[i].fValue = ConvertSbcToPCUnit(sSaveChData.auxData[i].lValue, PS_AUX_TYPE_HUMIDITY);


	}
	sCanData.canCount = sSaveChData.chData.nCanCount;
	//TRACE("Can Count : %d\r\n", sCanData.canCount);
	for(int i = 0 ; i < sCanData.canCount; i++)
	{
		sCanData.canData[i].chNo = sSaveChData.chData.chNo;
		sCanData.canData[i].canType	= sSaveChData.canData[i].canType;
		
		memcpy(&sCanData.canData[i].canVal, &sSaveChData.canData[i].canVal, sizeof(CAN_VALUE));
	}

	//ljb 201008
	sOvenData.fCurTemper = sSaveChData.ovenData.fCurTemper;
	sOvenData.fRefTemper = sSaveChData.ovenData.fRefTemper;
	sOvenData.fCurHumidity = sSaveChData.ovenData.fCurHumidity;
	sOvenData.fRefHumidity = sSaveChData.ovenData.fRefHumidity;
	
	//ljb 20150427
	sLoaderData.fVoltage = sSaveChData.loaderData.fVoltage;
	sLoaderData.fCurrent = sSaveChData.loaderData.fCurrent;
	sLoaderData.fSetResis = sSaveChData.loaderData.fSetResis;
	sLoaderData.fSetPower = sSaveChData.loaderData.fSetPower;

	//cny 181106
	sChillerData.fRefTemper = sSaveChData.chillerData.fRefTemper;
	sChillerData.fCurTemper = sSaveChData.chillerData.fCurTemper;
// 	sChillerData.iRefPump   = sSaveChData.chillerData.iRefPump;
// 	sChillerData.iCurPump   = sSaveChData.chillerData.iCurPump;
	sChillerData.fRefPump   = sSaveChData.chillerData.fRefPump; //lyj 20201102 DES칠러 펌프 소숫점 s ==============
	sChillerData.fCurPump   = sSaveChData.chillerData.fCurPump; //lyj 20201102 DES칠러 펌프 소숫점 s ==============

	for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
	{
		sCellBALData.ch_state[i]=sSaveChData.cellBALData.ch_state[i];
	}
	sPwrSupplyData.fCurVoltage = sSaveChData.pwrSupplyData.fCurVoltage;//yulee 20190514 cny
	sPwrSupplyData.fSetVoltage = sSaveChData.pwrSupplyData.fSetVoltage;

	//ksj 20210308 : SYNC_TIME 시간 단위 오류 디버깅용 로그 추가
	if(sChData.chDataSelect == SFT_SAVE_STEP_END) //스텝엔드 때만 로그 남김.
	{
		strTemp.Format("StepEnd SyncTime : %d %d (%d %.2f)",sSaveChData.chData.lSyncTime[0], sSaveChData.chData.lSyncTime[1], sChData.lSyncDate, sChData.fSyncTime); //변환 전후 값 둘다 로그 남김
		WriteLog(strTemp);
	}
	//ksj end

	return TRUE;
}


//2008/7/24		1. Cell CV 추가	2. 동기화 시간 데이터 추가
//2010/9/14     ljb 통신상태 추가
//2010/12/22	ljb Out State, Input State 상태 정보 추가
int CCyclerChannel::CheckAndSaveData_v100D()
{
	// now it critical!
	//EnterCriticalSection(&m_csCommunicationSync);
 	if(m_strFilePath.IsEmpty())		
 	{
		//파일명 없으면 리턴
		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);
		return 0;
 	}
 
 	//외부에서 파일 조작을 할 경우file update를 Lock 시킨 후 update 실시하기 위함 
 	if(m_bLockFileUpdate == TRUE)
 	{
 		TRACE("Ch %d file update is locked\n", m_nChannelIndex+1);
		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);
		return 0;
 	}
 
 	//저장할 Data가 없으면 return
 	if(GetStackedSize() < 1)	return 0;
 
	m_bFileSaving = TRUE;		//ljb 201101
 
 	//Add 2006/8/18
 	PS_STEP_END_RECORD prevData;
 	CString strStartTime, strTestSerial, strUser, strDescript, strTemp;
	
 	long lStartIndex = GetLastData(strStartTime, strTestSerial, strUser, strDescript, prevData);  	
 	
 	//2007/08/09 Aux Data 저장용//////////////////////////////////
 	//채널 데이터가 저장하는 방식을 따른다.
 
 	PS_AUX_RAW_DATA sAuxData;
 	float *pfAuxBuff = NULL;
 	size_t auxSize;
 	size_t auxLineSize;
 	CFile auxFile;
 	PS_AUX_FILE_HEADER auxHeader;
 	
	int nAuxColumnSize;
	int nCheck;
	
	nCheck = GetAuxFile(&auxFile, &auxHeader);
	if(nCheck < 0)
	{
		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);
		return nCheck;
	}
	nAuxColumnSize = auxHeader.auxHeader.nColumnCount;		//save data setting

 	if(nAuxColumnSize < 0 || nAuxColumnSize > PS_MAX_AUX_SAVE_FILE_COLUMN)
 	{
 		CString strMsg;
 		strMsg.Format("AuxColumn : %d", nAuxColumnSize);
 		WriteLog(strMsg);
 		auxFile.Close();

		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);
		return -1;
 	}
 		
 	if(nAuxColumnSize > 0)
 	{
 		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
 		auxLineSize =  sizeof(float)*nAuxColumnSize;
 		pfAuxBuff = new float[nAuxColumnSize*_MAX_DISK_WIRTE_COUNT];
 		auxSize = auxLineSize*_MAX_DISK_WIRTE_COUNT;
 		
 		//Line 수를 Count
 		int nAuxBodySize = auxFile.GetLength()-sizeof(PS_AUX_FILE_HEADER);
 		int nAuxRsCount = nAuxBodySize / auxLineSize ;
 		if(nAuxRsCount < 0 || nAuxBodySize % auxLineSize)
 		{
 			//TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
 			auxFile.Close();
			// release critical section
			//LeaveCriticalSection(&m_csCommunicationSync);

			//lyj 20210910 메모리누수 s ===========
			DeleteBuffer(pfAuxBuff,NULL,NULL);
			//lyj 20210910 메모리누수 e ===========

			return -2;
 		}
 	}
 	
 	//////////////////////////////////////////////////////////////
 	//2007/10/17 CAN Data 저장용//////////////////////////////////
 	//채널 데이터가 저장하는 방식을 따른다.
 
 	PS_CAN_RAW_DATA sCanData;
 	SFT_CAN_VALUE *pCanBuff = NULL;
 	size_t canSize;
 	size_t canLineSize;
 	CFile canFile;
 	PS_CAN_FILE_HEADER canHeader;

	int nRtn = 1;
	int nCanColumnSize;

	nRtn = GetCanFile(&canFile, &canHeader);
	if(nRtn < 0)	
	{
		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);

		//lyj 20210910 메모리누수 s ===========
		DeleteBuffer(pfAuxBuff,NULL,NULL);
		//lyj 20210910 메모리누수 e ===========

		return nRtn;
	}
 	nCanColumnSize = canHeader.canHeader.nColumnCount;		//save data setting
 	
 	if(nCanColumnSize < 0 || nCanColumnSize > PS_MAX_CAN_SAVE_FILE_COLUMN)
 	{
 		CString strMsg;
 		strMsg.Format("CanColumn : %d", nCanColumnSize);
 		WriteLog(strMsg);
 		canFile.Close();
		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);

		//lyj 20210910 메모리누수 s ===========
		DeleteBuffer(pfAuxBuff,NULL,NULL);
		//lyj 20210910 메모리누수 e ===========

		return -1;
 	}
 		
 	if(nCanColumnSize > 0)
 	{
 		//disk access를 줄이기 위해 100개씩 한꺼번에 저장한다.
 		canLineSize =  sizeof(SFT_CAN_VALUE)*nCanColumnSize;
 		pCanBuff = new SFT_CAN_VALUE[nCanColumnSize*_MAX_DISK_WIRTE_COUNT];
 		canSize = canLineSize*_MAX_DISK_WIRTE_COUNT;
 		
 		//Line 수를 Count
 		int nCanBodySize = canFile.GetLength()-sizeof(PS_CAN_FILE_HEADER);
 		int nCanRsCount = nCanBodySize / canLineSize ;
 		if(nCanRsCount < 0 || nCanBodySize % canLineSize)
 		{
 			CString strMsg;
 			strMsg.Format("CAN File Size Error (CanColumn : %d)", nCanColumnSize);
 			WriteLog(strMsg);
 			canFile.Close();
			// release critical section
			//LeaveCriticalSection(&m_csCommunicationSync);
			
			//lyj 20210910 메모리누수 s ===========
			DeleteBuffer(pfAuxBuff,pCanBuff,NULL);
			//lyj 20210910 메모리누수 e ===========

			return -2;
 		}
 	} 
	
 	//////////////////////////////////////////////////////////////
 	PS_OVEN_RAW_DATA    sOvenData;	  //ljb	
	PS_LOAD_RAW_DATA    sLoaderData;  //ljb 20150427 add	
	PS_OVEN_RAW_DATA    sChillerData; //ljb
	PS_CELLBAL_RAW_DATA sCellBALData; //cny 181106	
	PS_PWRSUPPLY_RAW_DATA sPwrSupplyData; //cny 181106//yulee 20190514 cny
 	//////////////////////////////////////////////////////////////
	
	//채널 데이터 저장용///////////////////////////////////////////////
 	PS_RAW_FILE_HEADER rawHeader;
 	CFile fRS;
 	PS_STEP_END_RECORD	sChResultData;
 	float *pfBuff;
 	size_t rsSize, lineSize;
 
	nCheck = GetChFile(fRS,rawHeader);
 	if(nCheck < 0)
 	{
		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);
		
		//lyj 20210910 메모리누수 s ===========
		DeleteBuffer(pfAuxBuff,pCanBuff,NULL);
		//lyj 20210910 메모리누수 e ===========

		return nCheck;
 	}
 	
 	int nColumnSize = rawHeader.rsHeader.nColumnCount;		//save data setting
 	if(nColumnSize < 1 || nColumnSize > PS_MAX_FILE_SAVE_ITEM_NUM)
 	{
 		auxFile.Close();
 		canFile.Close();
 		fRS.Close();
		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);

		//lyj 20210910 메모리누수 s ===========
		DeleteBuffer(pfAuxBuff,pCanBuff,NULL);
		//lyj 20210910 메모리누수 e ===========

 		return -3;
 	}
 
 	//disk access를 줄이기 위해 256개씩 한꺼번에 저장한다.
 	lineSize = sizeof(float)*nColumnSize;
 	pfBuff = new float[nColumnSize*_MAX_DISK_WIRTE_COUNT];
 	rsSize = lineSize*_MAX_DISK_WIRTE_COUNT;
 
 	//Line 수를 Count
 	int nBodySize = fRS.GetLength()-sizeof(PS_RAW_FILE_HEADER);
 	int nRsCount = nBodySize / lineSize;

	if(nRsCount < 0 || nBodySize % lineSize)
	{
		//TRACE("*****************>>>>>>>>>>>>>> File Size Error\n");
		auxFile.Close();
		canFile.Close();
		fRS.Close();

		// release critical section
		//LeaveCriticalSection(&m_csCommunicationSync);

		//lyj 20210910 메모리누수 s ===========
		DeleteBuffer(pfAuxBuff,pCanBuff,pfBuff);
		//lyj 20210910 메모리누수 e ===========

		return -4;
 	}
	
	int   iC=0;
	int   nStackCount = 0;
	INT64 lVal_CellBAL=0;
	INT64 lCellBALChData=0;

	CString strMsg;
 
	 #ifdef _DEBUG
 		DWORD dwStart = GetTickCount();
	 #endif
 	
	//ljb 201117 임시 채널 1에 챔버1 데이터 가져오기
 	//SFT_CHAMBER_VALUE *m_psChamData;
 	//m_psChamData = ::SFTGetChamberData(m_nModuleID,0);	//ljb 20131030 edit -> 20131118 주석처리
 	
//yulee 20190514 cny
	while(GetSaveDataD(sChResultData, sAuxData, sCanData, sOvenData, sLoaderData, sChillerData, sCellBALData, sPwrSupplyData))	//저장할 Data가 있을 경우 Stack에서 1개 Pop 한다.
 	{
 		//sChResultData.fOvenTemperature = sOvenData.fCurTemper;	//ljb 201008
 		//sChResultData.fOvenHumidity = sOvenData.fCurHumidity;	//ljb 201008
		sChResultData.fOvenTemperature = sOvenData.fCurTemper;	//ljb 201008 -> 20131118 edit
		sChResultData.fOvenHumidity = sOvenData.fCurHumidity;	//ljb 201008 -> 20131118 edit

		sChResultData.fLoadVoltage = sLoaderData.fVoltage;	//ljb 20150427
		sChResultData.fLoadCurrent = sLoaderData.fCurrent;	//ljb 20150427

		sChResultData.fChillerRefTemp = sChillerData.fRefTemper;	//cny 181106
		sChResultData.fChillerCurTemp = sChillerData.fCurTemper;	//cny 181106
// 		sChResultData.iChillerRefPump = sChillerData.iRefPump;	    //cny 181106
// 		sChResultData.iChillerCurPump = sChillerData.iCurPump;	    //cny 181106
		sChResultData.fChillerRefPump = sChillerData.fRefPump;	    //lyj 20201102 DES칠러 소숫점 
		sChResultData.fChillerCurPump = sChillerData.fCurPump;	    //lyj 20201102 DES칠러 소숫점 
        
		lCellBALChData=0;
		for(iC=0;iC<PS_MAX_CELLBAL_COUNT;iC++)
		{
			lVal_CellBAL=0;
			if( sCellBALData.ch_state[iC]=='R' ||
				sCellBALData.ch_state[iC]=='P')
			{
				lVal_CellBAL=1;
			}
			lVal_CellBAL=lVal_CellBAL<<iC;
            lCellBALChData |= lVal_CellBAL;
		}		
		lCellBALChData=lCellBALChData<<1;
		sChResultData.dCellBALChData=double(lCellBALChData);
		
		sChResultData.fPwrSetVoltage=sPwrSupplyData.fSetVoltage;//yulee 20190514 cny
		sChResultData.fPwrCurVoltage=sPwrSupplyData.fCurVoltage;
			
		//sChResultData.lSaveSequence => 1 Base No
 		if((sChResultData.lSaveSequence-1)  > nRsCount)		//Data loss 발생
 		{
 			//중간부 Data 손실 여부만 확인 가능하다. 즉 손실 이후 최소 한번이상의 결과 Data가 갱신되지 않으면 
 			//자동 검출 불가하다. 
 			m_bDataLoss = TRUE;					 
 			//TRACE("*****************>>>>>>>>>>>>>> Data loss detected(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
 			nRtn = 0;
 		}
 		else if((sChResultData.lSaveSequence-1)  < nRsCount)	//Index가 역전된 경우 
 		{
 			//작업중에 복구 명령을 수행하면 Download와 복구 중에 발생한 Buffering data가 중복되게 된다.
 			//이럴경우 현재 송신된 data(버퍼에 있던 data)는 이미 모듈에서 download하여 복구한 data이므로 
 			//저장하지 않고 Skip한다. 
   	    	//TRACE("*****************>>>>>>>>>>>>>> Data index ???(%d/%d)\n", sChResultData.lSaveSequence, nRsCount);
 			continue;
 		}
 		else												//정상적인 data
 		{
 			//TRACE("*****************>>>>>>>>>>>>>> Data index %d received\n", sChResultData.lSaveSequence);
 			m_bDataLoss = FALSE;
 		}
 
 		//Data 저장시 모든 단위를 m단위로 저장한다. (실처리 항목은 최소로 한다.) 파일 Size 문제...
 		for(int dataIndex = 0; dataIndex <nColumnSize ; dataIndex++)
 		{
 			pfBuff[dataIndex + nStackCount * nColumnSize] = GetItemData(sChResultData, rawHeader.rsHeader.awColumnItem[dataIndex]);	//Index 5 is WH	

			//ksj 20170209 : float 정밀도로 인해 값 왜곡이 있어 PS_SYNC_DATE는 따로처리.
			if(rawHeader.rsHeader.awColumnItem[dataIndex] == PS_SYNC_DATE) 
			{	
				memcpy((void*)&pfBuff[dataIndex + nStackCount * nColumnSize], &sChResultData.lSyncDate, sizeof(long)); //ksj 20170209 : float 변수에 캐스팅 안한 그대로의 long 값을 복사해 넣는다.
			}
			/*else
			{				
				float fItemData = GetItemData(sChResultData, rawHeader.rsHeader.awColumnItem[dataIndex]);
				memcpy((void*)&pfBuff[dataIndex + nStackCount * nColumnSize], &fItemData, sizeof(float));
			}*/

 		}		
 	
 		//int nAuxIndex = 0;
 		for(int nAuxDataIndex = 0; nAuxDataIndex < nAuxColumnSize ; nAuxDataIndex++)
 		{
 			//pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = GetAuxItemData(sAuxData, auxHeader.auxHeader.awColumnItem[nAuxDataIndex], nAuxIndex);					
 			//if(auxHeader.auxHeader.awColumnItem[nAuxDataIndex] == PS_AUX_VALUE)
 			//	nAuxIndex++;
 			pfAuxBuff[nAuxDataIndex + nStackCount * nAuxColumnSize] = sAuxData.auxData[nAuxDataIndex].fValue;
 		}	
 
 		//int nCanIndex = 0;
 		for(int nCanDataIndex = 0; nCanDataIndex < nCanColumnSize ; nCanDataIndex++)
 		{
 			//plCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] = GetCanItemData(sCanData, canHeader.canHeader.awColumnItem[nCanDataIndex], nCanIndex);								
 			//if(canHeader.canHeader.awColumnItem[nCanDataIndex] == PS_CAN_VALUE)
 			//	nCanIndex++;			
 			memcpy(&pCanBuff[nCanDataIndex + nStackCount * nCanColumnSize] , &sCanData.canData[nCanDataIndex].canVal, sizeof(SFT_CAN_VALUE));			
 		}
		
 		//TRACE("StackCount : %d\r\n", nStackCount);
 		//TRACE("CAN Data : %f, %f, %f\r\n", pCanBuff[0].fVal, pCanBuff[1].fVal, pCanBuff[2].fVal);	
 		//TRACE("V:%d/I:%d/C:%d\n", sChResultData.lVoltage,sChResultData.lCurrent,sChResultData.lCapacity );
 
		//ljb Step End 기록 -> WriteResultListFile_V100B
 		lStartIndex = WriteResultListFile_V100B(lStartIndex, nRsCount, sChResultData, sAuxData, sCanData);
		//lStartIndex = WriteResultListFile_V100B(lStartIndex, nRsCount, sChResultData, sAuxData, sCanData); //ksj 20201013 : 스텝엔드 구조체 변경.

 		if(lStartIndex < 0)
 		{
 			nRtn = -5;
 		}
 
 		//////////////////////////////////////////////////////////////////////////
 		nRsCount++;
 		nStackCount++;
 
 		//Disk write 성능 향상을 위해 한꺼번에 기록 하도록 수정 
 		if(nStackCount >= _MAX_DISK_WIRTE_COUNT)
 		{
			fRS.Write(pfBuff, sizeof(float)*nColumnSize*nStackCount);
			
			if(nAuxColumnSize > 0)	auxFile.Write(pfAuxBuff, sizeof(float)*nAuxColumnSize*nStackCount);
 			if(nCanColumnSize > 0)	canFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*nCanColumnSize*nStackCount);

			UpdateLastData(UINT(lStartIndex), nRsCount - 1, strStartTime, strTestSerial, strUser, strDescript, sChResultData);

			nStackCount = 0;
 		}
 	}// while(GetSaveDataD(sChResultData, sAuxData, sCanData))

	//20120210 KHS  데이터 손실 Log////////////////////////////////////////
	if(m_bDataLoss == TRUE && m_bDataLossLogCnt == FALSE)
	{
		//20110824 KHS////////////////////////////////////
		COleDateTime t = COleDateTime::GetCurrentTime();
		m_strLossTime = t.Format();
		/////////////////////////////////////////////////
		
		strTemp.Format("CH %02d Data Loss Detected", sChResultData.chNo);//, nDataLossLogCnt);
		WriteLog(strTemp);
		m_bDataLossLogCnt = TRUE;
	}
	///////////////////////////////////////////////////////////////////////
 
	if(m_bDataLoss == TRUE)		UpdateChannelEndLossState(strStartTime, sChResultData); //lyj 20201028
	else						UpdateChannelEndState(strStartTime, sChResultData); //lyj 20201028

 	//Disk access 횟수를 줄이기 위해 한꺼번에 저장 
 	if(nStackCount > 0)
 	{
 		fRS.Write(pfBuff, sizeof(float)*nColumnSize*nStackCount);
 
 		if(nAuxColumnSize > 0)	auxFile.Write(pfAuxBuff, sizeof(float)*nAuxColumnSize*nStackCount);
 		if(nCanColumnSize > 0)	canFile.Write(pCanBuff, sizeof(SFT_CAN_VALUE)*nCanColumnSize*nStackCount);
		
		
 		UpdateLastData(UINT(lStartIndex), nRsCount -1, strStartTime, strTestSerial, strUser, strDescript, sChResultData);
 	}
			
 #ifdef _DEBUG
 	if(m_nChannelIndex == 0)
 		TRACE("Channel %d data save time : %dms elapsed\n",  m_nChannelIndex+1, GetTickCount()-dwStart);
 #endif
   
	if(pfBuff != NULL)
 	{	
 		delete[]	pfBuff;		
 		pfBuff = NULL;
 	}
 	if(pfAuxBuff != NULL)
 	{
 		delete [] pfAuxBuff;
 		pfAuxBuff = NULL;
 	}
 	if(pCanBuff != NULL)
 	{
 		delete [] pCanBuff;
 		pCanBuff = NULL;
 	}
 		
 	if (fRS.m_hFile != CFile::hFileNull)
 	{
 		fRS.Flush();
 		fRS.Close();
 		auxFile.Flush();
 		auxFile.Close();
 		canFile.Flush();
 		canFile.Close();
 	}

 	TRACE("Channel %d 결과 Data 저장\n", m_nChannelIndex+1);

	m_bFileSaving = FALSE;		//ljb 201101
	
	// release critical section
//	LeaveCriticalSection(&m_csCommunicationSync);

	return nRtn;
}

long CCyclerChannel::GetChargeAh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CHARGE_CAP);
}

long CCyclerChannel::GetDisChargeAh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_DISCHARGE_CAP);
}

long CCyclerChannel::GetCapacitance()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CAPACITANCE);
}

long CCyclerChannel::GetChargeWh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CHARGE_WH);
}

long CCyclerChannel::GetDisChargeWh()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_DISCHARGE_WH);
}

long CCyclerChannel::GetCVTimeDay()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CV_TIME_DAY);
}

long CCyclerChannel::GetCVTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_CV_TIME);
}

long CCyclerChannel::GetAccCycleCount1()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE1);
}
long CCyclerChannel::GetAccCycleCount2()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE2);
}
long CCyclerChannel::GetAccCycleCount3()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE3);
}
long CCyclerChannel::GetAccCycleCount4()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE4);
}
long CCyclerChannel::GetAccCycleCount5()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_ACC_CYCLE5);
}
long CCyclerChannel::GetMultiCycleCount1()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE1);
}
long CCyclerChannel::GetMultiCycleCount2()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE2);
}
long CCyclerChannel::GetMultiCycleCount3()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE3);
}
long CCyclerChannel::GetMultiCycleCount4()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE4);
}
long CCyclerChannel::GetMultiCycleCount5()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MULTI_CYCLE5);
}

long CCyclerChannel::GetMuxUseState()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MUX_USE_STATE);
}
long CCyclerChannel::GetMuxBackupInfo()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_MUX_BACKUP_INFO);
}

long CCyclerChannel::GetSyncDate()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_DATE);
}

long CCyclerChannel::GetSyncTime()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	return ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_TIME);
}

CString CCyclerChannel::GetSyncDateTimeString()
{
	ASSERT(m_nModuleID >  0);		//Must call SetID() before use this function
	long lDate = ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_DATE);
	long lTime = ::SFTGetChannelValue(m_nModuleID, m_nChannelIndex, PS_SYNC_TIME);
	
	char buffer[20];
	CString strDateTime;
	if(lDate == 0 || lTime == 0)
	{
		SYSTEMTIME st;

		GetLocalTime(&st);  
// 		sprintf(buffer, "*%d-%02d-%02d %02d:%02d:%02d.%03d" , st.wYear, st.wMonth, st.wDay,
// 			st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		sprintf(buffer, "%d-%02d-%02d %02d:%02d:%02d" , st.wYear, st.wMonth, st.wDay,
			st.wHour, st.wMinute, st.wSecond);
		strDateTime.Format("%s", buffer);
		return strDateTime;
	}
	CString strTemp1, strTemp2;
	ltoa(lDate, buffer, 10);
	strTemp1.Format("%s", buffer);
	strDateTime.Format("%s-%s-%s", strTemp1.Left(4), strTemp1.Mid(4, 2), strTemp1.Right(2));
	strTemp1 = strDateTime;
		//strDateTime.Format("%s-%s-%s %s:%s:%s.%s")

	ltoa(lTime, buffer, 10);
	strTemp2.Format("%09s", buffer);
	strDateTime.Format("%s %s:%s:%s.%s", strTemp1, strTemp2.Left(2), strTemp2.Mid(2,2),
		strTemp2.Mid(4,2), strTemp2.Right(3));
	return strDateTime;

}

void CCyclerChannel::SetCanTransCount(int nCount)
{
	this->m_nCanTransCount = nCount;
}

BOOL CCyclerChannel::Fun_ConverToStringCan(CString & strData, CAN_VALUE * pReadData, int nStartPos, int nCanCount,ULONG & ulIndex)
{
	int count = 0;
	//long fData = 0.0f;
	float fData = 0.0f;		//ljb 20130709 edit
	int nSavePos = 0;
	char seps[] = ",";
	char *token;
	//	char pszColl[] = "aaa,bbb,ccc,ddd,eee,fff,ggg";
	char szColl[20];
	CString strTemp;
	token = strtok( (char *) (LPCTSTR)strData, seps );
	while(token != NULL)
	{
		strcpy(szColl, token);
		//printf("%s\n", szColl);
		nSavePos = count + nStartPos;
		count++;
		token = strtok(NULL, seps );
		fData = atof(szColl);
		if (count == 1) ulIndex = fData;
		//if (count == 2) *(pReadData+PS_TOT_TIME) = iData;
		//if (count == 3) *(pReadData+PS_STEP_TIME) = iData;
		if (count > 5)	//ljb 20140114
		{
			nSavePos -= 5;
			pReadData[nSavePos].fVal[0] = fData;
		}
	}	
	return TRUE;
}

BOOL CCyclerChannel::Fun_ConverToStringAux(CString & strData, long * pReadData, int nStartPos, int nAuxCount,ULONG & ulIndex)
{
	int count = 0;
	long lData = 0;
	int nSavePos = 0;
	char seps[] = ",";
	char *token;
	//	char pszColl[] = "aaa,bbb,ccc,ddd,eee,fff,ggg";
	char szColl[20];
	token = strtok( (char *) (LPCTSTR)strData, seps );
	while(token != NULL)
	{
		strcpy(szColl, token);
		//printf("%s\n", szColl);
		nSavePos = count + nStartPos;
		count++;
		token = strtok(NULL, seps );
		lData = atol(szColl);
		if (count == 1) ulIndex = lData;
		//if (count == 2) *(pReadData+PS_TOT_TIME) = iData;
		//if (count == 3) *(pReadData+PS_STEP_TIME) = iData;
		if (count > 5)	//ljb 20140114 edit 3 -> 5
		{
			nSavePos -= 5;
			*(pReadData+nSavePos) = lData;
		}
	}	
	return TRUE;
}

BOOL CCyclerChannel::Fun_ConverToString(CString &strData, int * lReadData, int & nSelect)
{
	int count = 0;
	int iData = 0;
	char seps[] = ",";
	char *token;
	char szColl[20];
	token = strtok( (char *) (LPCTSTR)strData, seps );
	while(token != NULL)
	{
		strcpy(szColl, token);
		//printf("%s\n", szColl);
		count++;
		token = strtok(NULL, seps );
		iData = atoi(szColl);
		if (count == 1) 
		{
			*(lReadData+PS_DATA_SEQ) = iData;
			//if(iData == 64754)	TRACE("%d\n",iData);
		}
		if (count == 2) *(lReadData+PS_TOT_TIME_DAY) = iData;
		if (count == 3) *(lReadData+PS_TOT_TIME) = iData;
		if (count == 4) *(lReadData+PS_STEP_TIME_DAY) = iData;
		if (count == 5) *(lReadData+PS_STEP_TIME) = iData;
		if (count == 6) *(lReadData+PS_STATE) = iData;
		if (count == 7) *(lReadData+PS_STEP_TYPE) = iData;
		//if (count == 8) lReadData+PS_DATA_SEQ = iData;			//mode
		if (count == 9) nSelect = iData;
		if (count == 10) *(lReadData+PS_CODE) = iData;
		if (count == 11) *(lReadData+PS_GRADE_CODE) = iData;
		if (count == 12) *(lReadData+PS_STEP_NO) = iData;
		if (count == 13) *(lReadData+PS_VOLTAGE) = iData;
		if (count == 14) *(lReadData+PS_CURRENT) = iData;
		if (count == 15) *(lReadData+PS_CHARGE_CAP) = LONG2FLOAT(iData);
		if (count == 16) *(lReadData+PS_DISCHARGE_CAP) = LONG2FLOAT(iData);
		if (count == 17) *(lReadData+PS_CAPACITANCE) = LONG2FLOAT(iData);	//ljb 2011412 이재복 ////////// PS_CAPACITY->PS_CAPACITANCE
		if (count == 18) *(lReadData+PS_WATT) = iData;
		if (count == 19) *(lReadData+PS_CHARGE_WH) = iData;
		if (count == 20) *(lReadData+PS_DISCHARGE_WH) = iData;
		if (count == 21) *(lReadData+PS_IMPEDANCE) = iData;
		//if (count == 22) *(lReadData+) = iData;					//reserved Cmd
		if (count == 23) *(lReadData+PS_COMM_STATE) = iData;		//comm state
		if (count == 24) *(lReadData+PS_CAN_OUTPUT_STATE) = iData;					//Channel Output State
		if (count == 25) *(lReadData+PS_CAN_INPUT_STATE) = iData;					//Channel Input State
		//if (count == 26) *(lReadData+) = iData;					//AUX count
		//if (count == 27) *(lReadData+) = iData;					//CAN count
		if (count == 28) *(lReadData+PS_TOT_CYCLE) = iData;
		if (count == 29) *(lReadData+PS_CUR_CYCLE) = iData;
		if (count == 30) *(lReadData+PS_ACC_CYCLE1) = iData;
		if (count == 31) *(lReadData+PS_ACC_CYCLE2) = iData;
		if (count == 32) *(lReadData+PS_ACC_CYCLE3) = iData;
		if (count == 33) *(lReadData+PS_ACC_CYCLE4) = iData;
		if (count == 34) *(lReadData+PS_ACC_CYCLE5) = iData;
		if (count == 35) *(lReadData+PS_MULTI_CYCLE1) = iData;
		if (count == 36) *(lReadData+PS_MULTI_CYCLE2) = iData;
		if (count == 37) *(lReadData+PS_MULTI_CYCLE3) = iData;
		if (count == 38) *(lReadData+PS_MULTI_CYCLE4) = iData;
		if (count == 39) *(lReadData+PS_MULTI_CYCLE5) = iData;
		if (count == 40) *(lReadData+PS_AVG_VOLTAGE) = iData;
		if (count == 41) *(lReadData+PS_AVG_CURRENT) = iData;
		if (count == 42) *(lReadData+PS_CV_TIME_DAY) = iData;
		if (count == 43) *(lReadData+PS_CV_TIME) = iData;
		if (count == 44) *(lReadData+PS_SYNC_DATE) = iData;
		if (count == 45) *(lReadData+PS_SYNC_TIME) = iData;
		if (count == 46) *(lReadData+PS_VOLTAGE_INPUT) = iData;
		if (count == 47) *(lReadData+PS_VOLTAGE_POWER) = iData;
		if (count == 48) *(lReadData+PS_VOLTAGE_BUS) = iData;
	}
	
	return TRUE;
}

float CCyclerChannel::Fun_ConvertToFloat(WORD wType, long ulData)
{
	float fTemp;
	fTemp = ConvertSbcToPCUnit(ulData, wType);

//	fTemp = (float)ulData;
	return fTemp;

}

BOOL CCyclerChannel::Fun_GetFirstNgState()
{
	return m_bSendNgState;
}

void CCyclerChannel::Fun_SetFirstNgState(BOOL bSetFlag)
{
	m_bSendNgState = bSetFlag;	
}

BOOL CCyclerChannel::RestoreLostData_lmh_localTest(CString strIPAddress)
{
	//strIPAddress == "192.168.5.84";
	CString srcFileName, destFileName;
	CString strTemp;
	m_nModuleID = 1;
	m_nChannelIndex = 0;

	if(strIPAddress.IsEmpty())
	{
		//strTemp.Format("Module %d ip address를 찾을 수 없습니다.",	m_nModuleID);
		strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg1","CYCLERCHANNEL"),	m_nModuleID);//&&
		AfxMessageBox(strTemp);
		return 0;
	}

//////////////////////////////////////////////////////////////////////////
	//CProgressWnd progressWnd(AfxGetMainWnd(), "복구중...", TRUE, TRUE, TRUE);
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg2","CYCLERCHANNEL"), TRUE, TRUE, TRUE);//&&
	progressWnd.SetRange(0, 100);
//////////////////////////////////////////////////////////////////////////

//	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	//Get ip address

//////////////////////////////////////////////////////////////////////////
//	strIPAddress = "192.168.9.10";
//	m_chFileDataSet.SetPath("C:\\test1\\M01Ch01");
//////////////////////////////////////////////////////////////////////////
	
	//strTemp.Format("Module %d에 접속 시도 중입니다.(IP %s)", m_nModuleID, strIPAddress);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg3","CYCLERCHANNEL"), m_nModuleID, strIPAddress);//&&
	progressWnd.SetPos(0);
	progressWnd.SetText(strTemp);
	
	BOOL bConnect = TRUE;
	//pDownLoad->ConnectFtpSession(strIPAddress);
// 	if(bConnect == FALSE)
// 	{
// 		m_strLastErrorString.Format("Module %d에 접속할 수 없습니다.", m_nModuleID);
// 		delete pDownLoad;
// 		return FALSE;
// 	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strRemoteFileName, strLocalFileName;
	CString strAuxTempLocalFileName, strAuxVoltLocalFileName, strCanMLocalFileName, strCanSLocalFileName;
	CString strLocalTempFolder(szBuff);
	CString strStartIndexFile, strEndIndexFile, strStepEndFile;
	CString strAuxTStepEndFile, strAuxVStepEndFile, strCanMStepEndFile, strCanSStepEndFile; 

	//1. 시작 Index파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d Index 시작 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg4","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(10);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
//		delete pDownLoad;
		return FALSE;
	}
	strLocalTempFolder = "F:\\source\\PNE\\Cycler(Pack)\\Cycler(Pack)_v100B_v1001_002_LG_2단챔버\\bin\\Debug\\Restore\\M01Ch01";

//	strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", m_nChannelIndex+1);
	strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_start.csv", m_nChannelIndex+1);
	strStartIndexFile = strLocalTempFolder + "\\savingFileIndex_start.csv";
// 	if(pDownLoad->DownLoadFile(strStartIndexFile, strRemoteFileName) < 1)
// 	{
// 		m_strLastErrorString.Format("M%02dCH%02d 시작 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
// 		delete pDownLoad;
// 		return FALSE;
// 	}


	//2. 최종 Index 파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d Index 마지막 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg5","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(20);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
//		delete pDownLoad;
		return FALSE;
	}
	
	strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//savingFileIndex_last.csv", m_nChannelIndex+1);
	strEndIndexFile = strLocalTempFolder + "\\savingFileIndex_last.csv";
// 	if(pDownLoad->DownLoadFile(strEndIndexFile, strRemoteFileName) < 1)
// 	{
// 		//가끔 download 실패가 발생한다.
// 		m_strLastErrorString.Format("M%02dCH%02d 최종 Index 정보 요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
// 		delete pDownLoad;
// 		TRACE("%s download fail\n", strRemoteFileName);
// 		return FALSE;
// 	}

	//3. Step End Data 파일을 DownLoad한다.
	//strTemp.Format("M%02dCH%02d 각 Step의 완료 정보를 읽고 있습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg6","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(30);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
//		delete pDownLoad;
		return FALSE;
	}

	strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData.csv", m_nChannelIndex+1);
	strStepEndFile = strLocalTempFolder + "\\ch001_SaveEndData.csv";

// 	if(pDownLoad->DownLoadFile(strStepEndFile, strRemoteFileName) < 1)
// 	{
// 		//가끔 download 실패가 발생한다.
// 		m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
// 		delete pDownLoad;
// 		TRACE("%s download fail\n", strRemoteFileName);
// 		return FALSE;
// 	}

	//Aux용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
// 	if(m_chFileDataSet.m_nAuxTempCount > 0)
// 	{
		strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strAuxTStepEndFile = strLocalTempFolder + "\\ch001_SaveEndData_auxT.csv";
// 		if(pDownLoad->DownLoadFile(strAuxTStepEndFile, strRemoteFileName) < 1)
// 		{
// 			//가끔 download 실패가 발생한다.
// 			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
// 			delete pDownLoad;
// 			TRACE("%s download fail\n", strRemoteFileName);
// 			return FALSE;
// 		}
/*	}*/
	
// 	if(m_chFileDataSet.m_nAuxVoltCount > 0)
// 	{
		strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strAuxVStepEndFile = strLocalTempFolder + "\\ch001_SaveEndData_auxV.csv";
// 		if(pDownLoad->DownLoadFile(strAuxVStepEndFile, strRemoteFileName) < 1)
// 		{
// 			//가끔 download 실패가 발생한다.
// 			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
// 			delete pDownLoad;
// 			TRACE("%s download fail\n", strRemoteFileName);
// 			return FALSE;
// 		}
/*	}*/
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Can용 EndData File/////////////////////////////////////////////////////////////////////////////////////////////////////
// 	if(m_chFileDataSet.m_nCanMasterCount > 0)
// 	{
		strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strCanMStepEndFile = strLocalTempFolder + "\\stepCanMEndData.csv";
// 		if(pDownLoad->DownLoadFile(strCanMStepEndFile, strRemoteFileName) < 1)
// 		{
// 			//가끔 download 실패가 발생한다.
// 			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
// 			delete pDownLoad;
// 			TRACE("%s download fail\n", strRemoteFileName);
// 			return FALSE;
// 		}
// 
// 	}
// 	
// 	if(m_chFileDataSet.m_nCanSlaveCount > 0)
// 	{
		strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d_SaveEndData_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1);
		strCanSStepEndFile = strLocalTempFolder + "\\stepCanSEndData.csv";
// 		if(pDownLoad->DownLoadFile(strCanSStepEndFile, strRemoteFileName) < 1)
// 		{
// 			//가끔 download 실패가 발생한다.
// 			m_strLastErrorString.Format("M%02dCH%02d Step 종료 정보요청을 실패 하였습니다.", m_nModuleID, m_nChannelIndex+1);
// 			delete pDownLoad;
// 			TRACE("%s download fail\n", strRemoteFileName);
// 			return FALSE;
// 		}
/*	}*/
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////
//	strStartIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_start.csv";
//	strEndIndexFile = "C:\\test1\\M01Ch01\\SBC\\savingFileIndex_last.csv";
//////////////////////////////////////////////////////////////////////////

	//4. Module쪽 Index 파일과 PC쪽 결과 파일을 비교하면서 손실구간을 검색한다.
	//PC쪽에 Data가 하나도 저장안되었을 경우도 복구 가능하도록 
	//현재 작업중인 경우 결과를 파일에 저장하지 않고 Buffer에 쌓고 있도록 한다.
	BOOL bLockFile = LockFileUpdate(TRUE);
	
	//strTemp.Format("손실된 구간을 검색중입니다.");
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg7","CYCLERCHANNEL"));//&&
	progressWnd.SetPos(40);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		//m_strLastErrorString.Format("M%02dCH%02d 사용자에 의해 취소됨", m_nModuleID, m_nChannelIndex+1);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg8","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
		LockFileUpdate(bLockFile);
//		delete pDownLoad;
		return FALSE;
	}

	long lLostDataCount =0;
	BOOL nRtn = SearchLostDataRange(strStartIndexFile, strEndIndexFile, lLostDataCount, &progressWnd);
// 	if(nRtn == FALSE)
// 	{
// 		m_strLastErrorString.Format("M%02dCH%02d 손실구간 검색 실패!!!", m_nModuleID, m_nChannelIndex+1);
// 		LockFileUpdate(bLockFile);
// //		delete pDownLoad;
// 		return FALSE;	//실제 Data 분석 
// 	}
// 
// 	if(lLostDataCount <= 0)
// 	{
// 		LockFileUpdate(bLockFile);
// //		delete pDownLoad;
// 		m_strLastErrorString.Format("M%02dCH%02d 손실된 data 정보가 없습니다.", m_nModuleID, m_nChannelIndex+1);
// 		return TRUE;	//실제 Data 분석 
// 	}

	//4. 손실된 구간이 있을 경우 해당 구간의 복구용 파일을 Module에서 Download한다.
//	long nSquenceNo = 0;
	BOOL bDownLoadOK = FALSE;

	srcFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_RAW_FILE_NAME_EXT;				//복구하고자 하는 파일
	destFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_RAW_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcAuxFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_AUX_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destAuxFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_AUX_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destAuxFileName);		//Backup할 파일명이 있으면 삭제 

	CString srcCanFileName = m_strFilePath +"\\"+ m_strTestName + "."+PS_CAN_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destCanFileName = m_strFilePath +"\\"+ m_strTestName + "_Backup."+PS_CAN_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destCanFileName);		//Backup할 파일명이 있으면 삭제 

//////////////////////////////////////////////////////////////////////////
//	srcFileName = "C:\\test1\\M01Ch01\\Test1.cyc";
//	destFileName = "C:\\test1\\M01Ch01\\Test1_BackUp.cyc";
//////////////////////////////////////////////////////////////////////////

	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cyc 파일용
	FILE *fpAuxSourceFile = NULL, *fpAuxDestFile = NULL;	//aux 파일용
	FILE *fpCanSourceFile = NULL, *fpCanDestFile = NULL;	//can 파일용

	//cyc용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////

	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg9","CYCLERCHANNEL"), destFileName);//&&
		LockFileUpdate(bLockFile);
//		delete pDownLoad;
		return FALSE; 
	}

	int nColSize = 0;
	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		if(CreateResultFileA(srcFileName) == FALSE)
		{
			//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg10","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpDestFile);
//			delete pDownLoad;
			return FALSE;	 
		}
		fpSourceFile = fopen(srcFileName, "rb");
	}
	
	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	{
		//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg11","CYCLERCHANNEL"), srcFileName);//&&
		LockFileUpdate(bLockFile);
		fclose(fpDestFile);
//		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}
		
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg12","CYCLERCHANNEL"), srcFileName);//&&
			LockFileUpdate(bLockFile);
			fclose(fpSourceFile);
			fclose(fpDestFile);
//			delete pDownLoad;
			return FALSE;	
		}
		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	
	if(m_chFileDataSet.GetAuxColumnCount() > 0)			
	{
		fpAuxDestFile = fopen(destAuxFileName, "wb");
		if(fpAuxDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg13","CYCLERCHANNEL"), destAuxFileName);//&&
			return FALSE; 
		}

		fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			
			//if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.GetAuxColumnCount()) == FALSE)
			if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.m_nAuxVoltCount, m_chFileDataSet.m_nAuxTempCount) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg14","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxDestFile);
				return FALSE;	 
			}
			fpAuxSourceFile = fopen(srcAuxFileName, "rb");
		}
		
		if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg15","CYCLERCHANNEL"), srcAuxFileName);//&&
			fclose(fpAuxDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxSourceFile) > 0)
		{
			if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg16","CYCLERCHANNEL"), srcAuxFileName);//&&
				fclose(fpAuxSourceFile);
				fclose(fpAuxDestFile);
				return FALSE;	
			}
		}
		nAuxColSize = auxFileHeader.auxHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//CAN용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	if(m_chFileDataSet.GetCanColumnCount() > 0)			
	{
		fpCanDestFile = fopen(destCanFileName, "wb");
		if(fpCanDestFile == NULL)
		{
			//m_strLastErrorString.Format("복구용 backup파일(%s)를 열수 없습니다.", destCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg17","CYCLERCHANNEL"), destCanFileName);//&&
			return FALSE; 
		}

		fpCanSourceFile = fopen(srcCanFileName, "rb");
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
			//단 조건 파일은 복구 할 수 없다.
			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
			if(CreateCanFileA(srcCanFileName, 0, m_chFileDataSet.GetCanColumnCount()) == FALSE)
			{
				//m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg18","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanDestFile);
				return FALSE;	 
			}
			fpCanSourceFile = fopen(srcCanFileName, "rb");
		}
		
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			//m_strLastErrorString.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg19","CYCLERCHANNEL"), srcCanFileName);//&&
			fclose(fpCanDestFile);
			return FALSE;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanDestFile) < 1)
			{
				//m_strLastErrorString.Format("파일 정보를 열수 없습니다.", srcCanFileName);
				m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg20","CYCLERCHANNEL"), srcCanFileName);//&&
				fclose(fpCanSourceFile);
				fclose(fpCanDestFile);
				return FALSE;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	//파일이 잘못되었을 경우 
	if(nColSize < 1 || nColSize > PS_MAX_FILE_SAVE_ITEM_NUM)
	{
		//m_strLastErrorString.Format("%s Header column 정보가 잘못 되었습니다.(%d)", srcFileName, nColSize);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg21","CYCLERCHANNEL"), srcFileName, nColSize);//&&
		LockFileUpdate(bLockFile);
		fclose(fpSourceFile);
		fclose(fpDestFile);
//		delete pDownLoad;
		return FALSE;	//실제 Data 분석 
	}

	//strTemp.Format("M%02dCH%02d 손실된 구간을 복구중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg22","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(50);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
//		delete pDownLoad;
		return FALSE;
	}

	UINT nFileSeq = 0;
	ULONG nSequenceNo = 1;
	int nBackupFileNo = 0;
	//File Read Buffer
	float *pfSrcBuff = new float[nColSize];
	float *pfBuff = new float[nColSize];
	float *pfAuxSrcBuff = NULL;
	CAN_VALUE *pCanSrcBuff = NULL;
	if(nAuxColSize > 0)
		pfAuxSrcBuff = new float[nAuxColSize];
	if(nCanColSize > 0)
		pCanSrcBuff = new CAN_VALUE[nCanColSize];
	
	BOOL bCanceled = FALSE;
	BOOL bRun = TRUE;
	int nCount = 0;
	int nPos;
	int nSaveTotSize = (ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 

	int nAuxCount = 0, nCanCount = 0;

	while(bRun)
	{
		//Progress 50~90구간 사용 
		nPos = 50 + int(40.0f*(float)nCount/(float)nSaveTotSize);

//		TRACE("TotLost Count %d, nCount %d, Pos %d\n", nSaveTotSize, nCount, nPos);

		progressWnd.SetPos(nPos);
		
		//Canceled 
		if(progressWnd.PeekAndPump() == FALSE)
		{
			bCanceled = TRUE;
			break;
		}
		nCount++;
		
		//PC쪽 파일은 순서대로 읽는다.
		size_t rdSize = fread(pfSrcBuff, sizeof(float), nColSize, fpSourceFile );

		//Aux,Can 파일도 Raw 파일의 인덱스를 따라간다.
		if(nAuxColSize)
			fread(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxSourceFile);
		if(nCanColSize)
			fread(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanSourceFile);

		if(rdSize <	1)	//PC쪽 파일을 다 읽었을 경우 모듈의 최종 Index와 같은지 확인 한다. 모듈쪽 저장 인텍스가 더크면 뒤쪽 data 최종 복구한다.
		{	
			nFileSeq = nSaveTotSize;//(ULONG)m_RestoreFileIndexTable.GetLastCount()+1;	//모듈의 최종 저장 Index를 구한다. 
			bRun = FALSE;	//PC쪽은 다 읽었음 
		}
		else
		{
			nFileSeq = (ULONG)pfSrcBuff[0];
		}

				
		//Index 검사
		long nStartNo = 0, nEndNo = 0, lLossCount =0;
		
		//PC쪽 검사 결과 data loss range detected	
		if(nFileSeq > nSequenceNo || bRun == FALSE)		
		{
			nStartNo = nSequenceNo;
			while(nFileSeq > nSequenceNo)
			{
				nSequenceNo++;
			}
			nEndNo = nSequenceNo-1;

			int nRestordDataSize = 0;
			lLossCount = nEndNo - nStartNo+1;
			long lTemp = 0;
			while(lLossCount > 0)	//1개의 손실 구간이 모두 복구됨 
			{
				//5. 손실된 부분에 해당하는 복구용 파일을 Index 파일에서 찾아낸다.
				nStartNo += nRestordDataSize;
				lTemp = m_RestoreFileIndexTable.FindFirstMachFileNo(nStartNo);	
				if(lTemp < 1)	
				{
					//현재 손실 구간을 SBC에서 Dowload한 파일목록에서 발견하지 못했을 경우, 
					//기타 오류로 복구 불가시는 현재구간은 복구를 생략한다.
					nRestordDataSize = lLossCount;
					lLossCount = 0;
					break;
				}
				
				//이전에 DownLoad한 파일에 손상된 구간이 있을 경우는 Dowload를 생략한다.
				if(nBackupFileNo != lTemp)
				{
					nBackupFileNo = lTemp;
					//6. 복구용 파일을 DownLoad 받는다.
					strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
					strLocalFileName = strLocalTempFolder + "\\ChRawData.csv";
					bDownLoadOK = TRUE;
// 					if(pDownLoad->DownLoadFile(strLocalFileName, strRemoteFileName) == FALSE)
// 					{
						//복구용 파일이 존재하지 않을 경우(복구최대시간경과로 삭제된 경우나 기타 오류로 복구 불가)
						//DownLoad 실패시 손실 시작부터 파일에 기록된 가장 마지막 크기 만큼 뛰어 넘고 복구 생략 
						nRestordDataSize += m_RestoreFileIndexTable.GetRawDataSize(nBackupFileNo, nStartNo);
//						bDownLoadOK = FALSE;
/*					}*/
					if(m_chFileDataSet.m_nAuxTempCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxT.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strAuxTempLocalFileName = strLocalTempFolder + "\\ChAuxTData.csv";
//						if(pDownLoad->DownLoadFile(strAuxTempLocalFileName, strRemoteFileName) == FALSE)
//							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nAuxVoltCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_auxV.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strAuxVoltLocalFileName = strLocalTempFolder + "\\ChAuxVData.csv";
// 						if(pDownLoad->DownLoadFile(strAuxVoltLocalFileName, strRemoteFileName) == FALSE)
// 							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nCanMasterCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canMaster.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strCanMLocalFileName = strLocalTempFolder + "\\ChCanMData.csv";
// 						if(pDownLoad->DownLoadFile(strCanMLocalFileName, strRemoteFileName) == FALSE)
// 							bDownLoadOK = FALSE;
					}
					if(m_chFileDataSet.m_nCanSlaveCount > 0)
					{
						strRemoteFileName.Format("cycler_data//resultData//group1//data0//ch%03d//ch%03d_SaveData%03d_canSlave.csv", m_nChannelIndex+1, m_nChannelIndex+1, nBackupFileNo);
						strCanSLocalFileName = strLocalTempFolder + "\\ChCanSData.csv";
// 						if(pDownLoad->DownLoadFile(strCanSLocalFileName, strRemoteFileName) == FALSE)
// 							bDownLoadOK = FALSE;
					}
				}

				//7. DownLoad 파일로 복구 한다.
				// 7.1 data가 부족하면 다음 파일을 DownLoad한다.
				if(bDownLoadOK)
				{					
					//복구 시작
					nRestordDataSize = WriteRestoreRawFile(strLocalFileName, fpDestFile, FileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nAuxColSize)
						WriteRestoreAuxFile(strAuxTempLocalFileName, strAuxVoltLocalFileName, fpAuxDestFile, auxFileHeader, nStartNo, nEndNo, nBackupFileNo);
					if(nCanColSize)
						WriteRestoreCanFile(strCanMLocalFileName, strCanSLocalFileName, fpCanDestFile, canFileHeader, nStartNo, nEndNo, nBackupFileNo);
					

				}
				strTemp.Format("Data resotre action, start Index: %d, count:%d",  nStartNo, nRestordDataSize);
				WriteLog(strTemp);
				lLossCount -= nRestordDataSize;	//if(lLossCount > 0) Download한 파일에 복구 data가 모두 없고 다음파일까지 이어짐 
			}
			
			if(bRun)
			{
				//PC쪽 읽을 data가 아직 남아 있으면 손실 구간 가장 마지막 다음 data까지 읽혔으므로 
				//한줄더 읽어들인 data를 저장한다.
				if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destFileName);
				}
				if(nAuxColSize > 0)
				{
					if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destAuxFileName);
					}
				}
				if(nCanColSize > 0)
				{
					if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
					{
						TRACE("Backup File write Error %s\n", destCanFileName);
					}
				}
			}	
			nSequenceNo = nFileSeq;	//손실된 구간 복구 완료 
		}
		else if(nFileSeq < nSequenceNo)	//Module index 오류
		{
			//PC와 모듈의 시험이 불일치 하거나 모듈에 저장된 파일의 index가 잘못 되었을 경우 
			TRACE("******************Module data 저장 Index에 오류가 있거나 시험 Mismatch.(%d/%d)\n", nFileSeq, nSequenceNo);

			//m_strLastErrorString.Format("M%02dCH%02d 모듈 정보와 Host 정보가 불일치 합니다.", m_nModuleID, m_nChannelIndex+1);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg23","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&

			if(pfBuff)
			{
				delete [] pfBuff;
				pfBuff = NULL;
			}

			if(pfSrcBuff)
			{
				delete[] pfSrcBuff;
				pfSrcBuff = NULL;
			}
			if(pfAuxSrcBuff)
			{
				delete [] pfAuxSrcBuff;
				pfAuxSrcBuff = NULL;
			}
			if(pCanSrcBuff)
			{
				delete [] pCanSrcBuff;
				pCanSrcBuff = NULL;
			}

			if(fpDestFile)		fflush(fpDestFile);
			if(fpAuxDestFile)	fflush(fpAuxDestFile);
			if(fpCanDestFile)	fflush(fpCanDestFile);

			if(fpSourceFile) fclose(fpSourceFile);
			if(fpDestFile) fclose(fpDestFile);
			if(fpAuxSourceFile) fclose(fpAuxSourceFile);
			if(fpAuxDestFile) fclose(fpAuxDestFile);
			if(fpCanSourceFile) fclose(fpCanSourceFile);
			if(fpCanDestFile) fclose(fpCanDestFile);

//			delete pDownLoad;

			_unlink(destFileName);		//PC와 모듈이 맞지 않는 정보일 경우 복구 할 수 없음 
			
			LockFileUpdate(bLockFile);

			return FALSE;
		}
		else
		{
			//PC에서 정상적인 원본에서 임시 복구 파일로 Data를 복사한다.
			if(fwrite(pfSrcBuff, sizeof(float), nColSize, fpDestFile) < 1)
			{
				TRACE("Backup File write Error %s\n", destFileName);
			}
			if(nAuxColSize > 0)
			{
				if(fwrite(pfAuxSrcBuff, sizeof(float), nAuxColSize, fpAuxDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destAuxFileName);
				}
			}
			if(nCanColSize > 0)
			{
				if(fwrite(pCanSrcBuff, sizeof(CAN_VALUE), nCanColSize, fpCanDestFile) < 1)
				{
					TRACE("Backup File write Error %s\n", destCanFileName);
				}
			}

		
		}	//정상
		nSequenceNo++;

	} //while(bRun) END

	if(pfBuff)
	{
		delete [] pfBuff;
		pfBuff = NULL;
	}

	if(pfSrcBuff)
	{
		delete[] pfSrcBuff;
		pfSrcBuff = NULL;
	}

	if(pfAuxSrcBuff)
	{
		delete [] pfAuxSrcBuff;
		pfAuxSrcBuff = NULL;
	}
	if(pCanSrcBuff)
	{
		delete [] pCanSrcBuff;
		pCanSrcBuff = NULL;
	}

	if(fpDestFile)		fflush(fpDestFile);
	if(fpAuxDestFile)	fflush(fpAuxDestFile);
	if(fpCanDestFile)	fflush(fpCanDestFile);

	if(fpSourceFile) fclose(fpSourceFile);
	if(fpDestFile) fclose(fpDestFile);

	if(fpAuxSourceFile) fclose(fpAuxSourceFile);
	if(fpAuxDestFile)	fclose(fpAuxDestFile);

	if(fpCanSourceFile) fclose(fpCanSourceFile);
	if(fpCanDestFile) fclose(fpCanDestFile);
//	delete pDownLoad;

	if(bCanceled)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//복구된 파일을 이용하여 rpt 파일을 생성한다.
	CString strNewTableFile, strOrgTableFile;
	strNewTableFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_RESULT_FILE_NAME_EXT;

	//Aux,Can용 결과 데이터 파일
	CString strNewAuxTabelFile, strOrgAuxTableFile;
	strNewAuxTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_AUX_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgAuxTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_AUX_RESULT_FILE_NAME_EXT;

	CString strNewCanTabelFile, strOrgCanTableFile;
	strNewCanTabelFile = m_strFilePath +"\\"+ m_strTestName + "_Backup." + PS_CAN_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	strOrgCanTableFile = m_strFilePath +"\\"+ m_strTestName + "." + PS_CAN_RESULT_FILE_NAME_EXT;

	_unlink(strNewTableFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewAuxTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 
	_unlink(strNewCanTabelFile);		//Backup할 파일명과 동일명의 파일이 존재하면 삭제 

//////////////////////////////////////////////////////////////////////////
//	strNewTableFile = "C:\\test1\\M01Ch01\\Test1_BackUp.cts";
//	strOrgTableFile = "C:\\test1\\M01Ch01\\Test1.cts";
//////////////////////////////////////////////////////////////////////////

	//strTemp.Format("M%02dCH%02d 손실된 Index 정보를 갱신중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg24","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(90);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	//Backup이 완료된 Raw파일을 이용하여 rpt 파일을 새로 생성	
	if(RemakePCIndexFile(destFileName, strStepEndFile, strNewTableFile, strOrgTableFile) == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	//ljb 에러부분
	if(nAuxColSize > 0)
	{
		if(RemakeAuxPCIndexFile(destAuxFileName, strAuxTStepEndFile, strAuxVStepEndFile, strNewAuxTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	if(nCanColSize > 0)
	{
		if(RemakeCanPCIndexFile(destCanFileName, strCanMStepEndFile, strCanSStepEndFile, strNewCanTabelFile) == FALSE)
		{
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}

	//strTemp.Format("M%02dCH%02d 복구용 임시 파일을 삭제중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg25","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(95);
	progressWnd.SetText(strTemp);
	if(progressWnd.PeekAndPump() == FALSE)
	{
		LockFileUpdate(bLockFile);
		return FALSE;
	}
	
	//원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	

	sprintf(szFrom, "%s", strOrgTableFile);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgAuxTableFile);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
	sprintf(szFrom, "%s", strOrgCanTableFile);
	::DeleteFile(szFrom);
	
	ZeroMemory(szFrom, _MAX_PATH);			//Double NULL Terminate
	sprintf(szFrom, "%s", srcFileName);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcAuxFileName);
	::DeleteFile(szFrom);

	ZeroMemory(szFrom, _MAX_PATH);
	sprintf(szFrom, "%s", srcCanFileName);
	::DeleteFile(szFrom);


	progressWnd.SetPos(98);
	//strTemp.Format("M%02dCH%02d 복구 완료중입니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg26","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetText(strTemp);
	//Backup한 파일이름을 바꾼다.
	if(rename(destFileName, srcFileName) != 0)
	{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg27","CYCLERCHANNEL"), srcFileName);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}

	if(nAuxColSize > 0)
	{
		if(rename(destAuxFileName, srcAuxFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg27","CYCLERCHANNEL"), srcAuxFileName);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewAuxTabelFile, strOrgAuxTableFile) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgAuxTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg27","CYCLERCHANNEL"), strOrgAuxTableFile);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}	

	if(nCanColSize > 0)
	{
		if(rename(destCanFileName, srcCanFileName) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg27","CYCLERCHANNEL"), srcCanFileName);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
		if(rename(strNewCanTabelFile, strOrgCanTableFile) != 0)
		{
			//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgCanTableFile);
			m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg27","CYCLERCHANNEL"), strOrgCanTableFile);//&&
			LockFileUpdate(bLockFile);
			return FALSE;
		}
	}
	

	if(rename(strNewTableFile, strOrgTableFile) != 0)
	{
		//m_strLastErrorString.Format("%s 파일명을 변경할 수 없습니다.", strOrgTableFile);
		m_strLastErrorString.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg27","CYCLERCHANNEL"), strOrgTableFile);//&&
		LockFileUpdate(bLockFile);
		return FALSE;
	}


	LockFileUpdate(bLockFile);
	
	//strTemp.Format("M%02dCH%02d 복구를 완료 하였습니다.", m_nModuleID, m_nChannelIndex+1);
	strTemp.Format(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg28","CYCLERCHANNEL"), m_nModuleID, m_nChannelIndex+1);//&&
	progressWnd.SetPos(100);
	progressWnd.SetText(strTemp);
	progressWnd.PeekAndPump();
	
	Sleep(200);

	m_bDataLoss = FALSE;
	m_bDataLossLogCnt = FALSE;			//20120210 KHS
	//WriteLog("Data 복구 완료");
	WriteLog(Fun_FindMsg("CyclerChannel_RestoreLostData_lmh_localTest_msg29","CYCLERCHANNEL"));//&&
	
	return TRUE;
}

CString CCyclerChannel::GetDataPath()
{
	CString strTemp, strMsg, strFileName, strDataPath;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\'));;
	
	strFileName.Format("%s\\Temp\\M%02dC%02d.tmp", strTemp, m_nModuleID, m_nChannelIndex+1);
	
	CT_TEMP_FILE_DATA tempData;
	ZeroMemory(&tempData, sizeof(CT_TEMP_FILE_DATA));
	
	FILE *fp = fopen(strFileName, "rt");
	
	if(fp == NULL)		return "";
	
	//File Path
	fread(&tempData, sizeof(CT_TEMP_FILE_DATA), 1, fp);
	
	strDataPath = tempData.szFilePath;
	
	fclose(fp);
	
	return strDataPath;
}

void CCyclerChannel::CycSendCommand(int nCmd)
{	
	theApp.Fun_CycSendCommand(this,nCmd);
}

//ksj 20200203 : 현재 옥스 상태를 채널로그에 남긴다.
void CCyclerChannel::WriteAuxStateLog(void)
{
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "WriteChannelAuxLog", 1) == 0) return;

	CMainFrame *pMain=(CMainFrame *)AfxGetMainWnd();
	CCTSMonProDoc *pDoc=(CCTSMonProDoc *)pMain->GetActiveDocument();
	CCyclerModule* pModule = pDoc->GetModuleInfo(GetModuleID());
	if(!pModule) return;

// 	CStringArray strArrTherTableName;
// 	CStringArray strArrHumiTableName;
// 	pDoc->Fun_GetArryAuxThrStringName(strArrTherTableName); //ksj 20201110 : 써미스터 테이블명 가져오기
// 	pDoc->Fun_GetArryAuxHumiStringName(strArrHumiTableName); //ksj 20201110 : 습도 테이블명 가져오기
	
	//할당된 Aux 개수
	CString strLog;
	//strLog.Format("Aux Count: %d, Temp: %d, Volt: %d, Th: %d", GetAuxCount(), GetAuxVoltCount(), GetAuxTempCount(), GetAuxTempThCount());
	strLog.Format("Aux Count: %d, Temp: %d, Volt: %d, Th: %d", GetAuxCount(), GetAuxTempCount(),  GetAuxVoltCount(), GetAuxTempThCount()); //ksj 20201112 : temp volt 순서 뒤바뀐 것 수정.
	WriteLog(strLog);
	
	SFT_AUX_DATA * pAuxData = GetAuxData();
	if(pAuxData == NULL) return;
	
	CString strTList;
	CString strVList;
	CString strThList;
	CString strHList;
	CString strItem;
	CString strTemp;
	int nVoltCount = pModule->GetMaxVoltage();
	int nThCount = pModule->GetMaxTemperatureTh();

	for(int i = 0; i < GetAuxCount(); i++)
	{	
		int nAuxTempTableType = 0;
		int nAuxTempTableCount = 0;
		strItem.Empty();
		strTemp.Empty();

		switch(pAuxData[i].auxChType)
		{
			case PS_AUX_TYPE_TEMPERATURE:
				strItem.Format("T%d, ",pAuxData[i].auxChNo);
				strTList += strItem;
				break;
			case PS_AUX_TYPE_VOLTAGE: 
				strItem.Format("V%d, ",pAuxData[i].auxChNo);	
				strVList += strItem;
				break;
			case PS_AUX_TYPE_TEMPERATURE_TH: 
				//strItem.Format("TH%d, ",pAuxData[i].auxChNo);										
				strItem.Format("TH%d, ",pAuxData[i].auxChNo-nVoltCount); //ksj 20201110 : 써미스터 번호 오류 수정				
				strThList += strItem;
				break;			
			case PS_AUX_TYPE_HUMIDITY:  //ksj 20201110 : 습도 센서 추가.
				strItem.Format("H%d, ",pAuxData[i].auxChNo-nVoltCount-nThCount);						
				strHList += strItem;
				break;			
		}
	}

	if(!strVList.IsEmpty()) WriteLog(strVList);
	if(!strTList.IsEmpty()) WriteLog(strTList);
	if(!strThList.IsEmpty()) WriteLog(strThList);	
	if(!strHList.IsEmpty()) WriteLog(strHList);	

	//ksj 20201110 : aux 상세 정보 로그 추가
	CString strFullAuxInfo;
	strFullAuxInfo = "##Aux Information\n";

	for(int i = 0; i < GetAuxCount(); i++)
	{		
		CChannelSensor * pSensor;
		pSensor = pModule->GetAuxData(pAuxData[i].auxChNo-1, pAuxData[i].auxChType);

		if(pSensor == NULL)
		{
			break;
		}

		strItem.Format("Aux%d, ", i+1);
		strFullAuxInfo += strItem;
						
		strFullAuxInfo += pSensor->GetAuxName();
		strFullAuxInfo += ", ";

		float fValue = pDoc->UnitTrans( pAuxData[i].lValue, FALSE, pAuxData[i].auxChType);
		strItem.Format("value:%f, ", fValue);
		strFullAuxInfo += strItem;

		STF_MD_AUX_SET_DATA AuxSetData = pSensor->GetAuxData();

		strItem.Format("VentMax:%f, ", float(pDoc->UnitTrans(AuxSetData.vent_upper, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		strFullAuxInfo += strItem;

		strItem.Format("VentMin:%f, ", float(pDoc->UnitTrans(AuxSetData.vent_lower, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		strFullAuxInfo += strItem;

		strItem.Format("SafeMax:%f, ", float(pDoc->UnitTrans(AuxSetData.lMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		strFullAuxInfo += strItem;

		strItem.Format("SafeMin:%f, ", float(pDoc->UnitTrans(AuxSetData.lMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		strFullAuxInfo += strItem;

		strItem.Format("vent:%d, ",AuxSetData.vent_use_flag);
		strFullAuxInfo += strItem;

		strItem.Format("EndMax:%f, ", float(pDoc->UnitTrans(AuxSetData.lEndMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		strFullAuxInfo += strItem;

		strItem.Format("EndMin:%f, ", float(pDoc->UnitTrans(AuxSetData.lEndMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		strFullAuxInfo += strItem;


		strItem = pDoc->Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division1);
		strFullAuxInfo += strItem;
		strItem = pDoc->Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division2);
		strFullAuxInfo += strItem;
		strItem = pDoc->Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division3);
		strFullAuxInfo += strItem;

		strFullAuxInfo += " ";
		if (pAuxData[i].auxChType != 2)
		{
			strItem = "";
		}
		else
		{
			strItem.Format("RtTableNo:%d",AuxSetData.auxTempTableType);
		}
		strFullAuxInfo += strItem;

		strFullAuxInfo += "\n";
	}
		
	if(!strFullAuxInfo.IsEmpty()) WriteLog(strFullAuxInfo);	
	
	
}

//ksj 20200203 : 현재 캔 상태를 채널로그에 남긴다.
void CCyclerChannel::WriteCanStateLog(void)
{
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "WriteChannelCanLog", 1) == 0) return;

	CMainFrame *pMain=(CMainFrame *)AfxGetMainWnd();
	CCTSMonProDoc *pDoc=(CCTSMonProDoc *)pMain->GetActiveDocument();
	CCyclerModule* pModule = pDoc->GetModuleInfo(GetModuleID());
	if(!pModule) return;

	//할당된 Can 개수
	CString strLog;
	CString strItem;
	CString strTemp;
	strLog.Format("Can Count: %d", GetCanCount());
	WriteLog(strLog);	

	//ksj 20201112 : 캔 설정 상태도 남기기
	SFT_CAN_COMMON_DATA commonData[2];
	commonData[0]	= pModule->GetCANCommonData(GetChannelIndex(), PS_CAN_TYPE_MASTER);
	commonData[1]	= pModule->GetCANCommonData(GetChannelIndex(), PS_CAN_TYPE_SLAVE);

	/*
	unsigned char		can_baudrate;			//0:125k, 1: 250k, 2:500k, 3: 1M
	unsigned char		extended_id;			//0:unused, 1:used
	unsigned char		bms_type;				//ljb 201008 v100A -> default:0
	unsigned char		bms_sjw;				//ljb 20100909 default : 0 , select 1~4

	long				controller_canID;
	long				mask[2];
	long				filter[6];

	//ljb 20181115 S 구조체 변경
	//long				l_Reserved[3];
	unsigned char		can_fd_flag;  //0:can 2.0B, 1: can_fd
	unsigned char		can_datarate; //0:500K, 1: 833K, 2:1M, 3:1.5M, 4:2M, 5.3M
	unsigned char		terminal_r;   //0:open, 1:120ohm<default>
	unsigned char		crc_type;	  //0:non iso crc, 1:iso crc<default>

	unsigned char		can_lin_select;	  //0:can, 1:Lin
	*/
	int i=0;
	for(int commDataIdx=0;commDataIdx<2;commDataIdx++)
	{
		strTemp.Format("[%s] baud:%d, extId:%d, bmsType:%d, bmsSjw:%d, controllerCandID:%d, fd_flag:%d, datarate:%d, term_r:%d, crc:%d , canlinSel:%d "
			, (commDataIdx==0)?"CAN Master":"CAN Slave"
			, commonData[commDataIdx].can_baudrate
			, commonData[commDataIdx].extended_id
			, commonData[commDataIdx].bms_type
			, commonData[commDataIdx].bms_sjw
			, commonData[commDataIdx].controller_canID
			, commonData[commDataIdx].can_fd_flag
			, commonData[commDataIdx].can_datarate
			, commonData[commDataIdx].terminal_r
			, commonData[commDataIdx].crc_type
			, commonData[commDataIdx].can_lin_select);

		WriteLog(strTemp);	

		strLog.Empty();
	
		for(i=0;i<2;i++)
		{
			strTemp.Format("mask[%d]:%d, ",i,commonData[commDataIdx].mask[i]);
			strLog+=strTemp;
		}

		for(i=0;i<6;i++)
		{
			strTemp.Format("filter[%d]:%d, ",i,commonData[commDataIdx].filter[i]);
			strLog+=strTemp;
		}
		WriteLog(strLog);	
	}
	
	


	//ksj 20201110 : can 상세 정보 로그 추가
	CString strFullCanInfo;
	strFullCanInfo = "##Can Information\n";

	SFT_CAN_DATA * pCanData = GetCanData();				
	if(pCanData)
	{				
		int nChannelID = GetChannelIndex();

		for(int i = 0; i < pModule->GetInstalledCanMaster(nChannelID); i++)
		{
			SFT_CAN_SET_DATA canData = pModule->GetCANSetData(nChannelID, PS_CAN_TYPE_MASTER, i);

			strItem.Format("M%d, ", i+1);
			strFullCanInfo += strItem;

			strItem.Format("Name:%s, ",canData.name);
			strFullCanInfo += strItem;
			
			strItem.Empty();
			if(pCanData[i].data_type == 0 || pCanData[i].data_type == 1 || pCanData[i].data_type == 2)
			{
				strItem.Format("value:%.6f,", pCanData[i].canVal.fVal[0]);
				if (canData.function_division > 100 && canData.function_division < 150)
					strItem.Format("value:%02x%02x%02x%02x,", (BYTE)pCanData[i].canVal.strVal[0], (BYTE)pCanData[i].canVal.strVal[1], (BYTE)pCanData[i].canVal.strVal[2], (BYTE)pCanData[i].canVal.strVal[3]);

				strItem.TrimRight("0");
				strItem.TrimRight(".");
			}
			else if(pCanData[i].data_type == 3){
				strItem.Format("value:%s, ", pCanData[i].canVal.strVal);
			}
			else if (pCanData[i].data_type == 4){
				strItem.Format("value:0x%x, ", (UINT)pCanData[i].canVal.fVal[0]);			
			}
			strFullCanInfo += strItem;

			strItem.Format("canID:%u(0x%x), ", canData.canID, canData.canID);
			strFullCanInfo += strItem;		

			strItem.Format("Safety_Max:%.3f, ", canData.fault_upper);				
			strFullCanInfo += strItem;	

			strItem.Format("Safety_Min:%.3f, ", canData.fault_lower);
			strFullCanInfo += strItem;	

			strItem.Format("End_max:%.3f, ", canData.end_upper);
			strFullCanInfo += strItem;

			strItem.Format("End_min:%.3f, ", canData.end_lower);
			strFullCanInfo += strItem;

			strItem.Format("Division_Code1:%s,",pDoc->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
			strFullCanInfo += strItem;

			strItem.Format("Division_Code2:%s,",pDoc->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
			strFullCanInfo += strItem;

			strItem.Format("Division_Code3:%s,",pDoc->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
			strFullCanInfo += strItem;

			strFullCanInfo += "\n";			
		}
		if(!strFullCanInfo.IsEmpty()) WriteLog(strFullCanInfo);	
		strFullCanInfo.Empty();
		strFullCanInfo += "\n";		

		int nIndex = pModule->GetInstalledCanMaster(nChannelID);
		for(int i = 0; i < pModule->GetInstalledCanSlave(nChannelID); i++)
		{			
			SFT_CAN_SET_DATA canData = pModule->GetCANSetData(nChannelID, PS_CAN_TYPE_SLAVE, i);

			strItem.Format("S%d, ", i+1);
			strFullCanInfo += strItem;

			strItem.Format("Name:%s, ",canData.name);
			strFullCanInfo += strItem;

			strItem.Empty();
			if(pCanData[i].data_type == 0 || pCanData[i].data_type == 1 || pCanData[i].data_type == 2)
			{
				strItem.Format("value:%.6f,", pCanData[i].canVal.fVal[0]);
				if (canData.function_division > 100 && canData.function_division < 150)
					strItem.Format("value:%02x%02x%02x%02x,", (BYTE)pCanData[i].canVal.strVal[0], (BYTE)pCanData[i].canVal.strVal[1], (BYTE)pCanData[i].canVal.strVal[2], (BYTE)pCanData[i].canVal.strVal[3]);

				strItem.TrimRight("0");
				strItem.TrimRight(".");
			}
			else if(pCanData[i].data_type == 3){
				strItem.Format("value:%s, ", pCanData[i].canVal.strVal);
			}
			else if (pCanData[i].data_type == 4){
				strItem.Format("value:0x%x, ", (UINT)pCanData[i].canVal.fVal[0]);			
			}
			strFullCanInfo += strItem;

			strItem.Format("canID:%u(0x%x), ", canData.canID, canData.canID);
			strFullCanInfo += strItem;		

			strItem.Format("Safety_Max:%.3f, ", canData.fault_upper);				
			strFullCanInfo += strItem;	

			strItem.Format("Safety_Min:%.3f, ", canData.fault_lower);
			strFullCanInfo += strItem;	

			strItem.Format("End_max:%.3f, ", canData.end_upper);
			strFullCanInfo += strItem;

			strItem.Format("End_min:%.3f, ", canData.end_lower);
			strFullCanInfo += strItem;

			strItem.Format("Division_Code1:%s,",pDoc->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
			strFullCanInfo += strItem;

			strItem.Format("Division_Code2:%s,",pDoc->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
			strFullCanInfo += strItem;

			strItem.Format("Division_Code3:%s,",pDoc->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
			strFullCanInfo += strItem;

			strFullCanInfo += "\n";
		}

		if(!strFullCanInfo.IsEmpty()) WriteLog(strFullCanInfo);					
	}
}

//ksj 20200203 : 현재 병렬 상태를 채널 로그에 남긴다.
void CCyclerChannel::WriteParallelStateLog(SFT_MD_PARALLEL_DATA* pParallelData)
{
	if(!pParallelData) return;

	//할당된 Can 개수
	CString strLog;
	strLog.Format("Master: %d, Slave1: %d, Slave2: %d, Slave3: %d, UseParallel: %d"
											, pParallelData->chNoMaster
											, pParallelData->chNoSlave[0]
											, pParallelData->chNoSlave[1]
											, pParallelData->chNoSlave[2]
											, pParallelData->bParallel
											);
	WriteLog(strLog);	
}


//ksj 20200702 : CTS 파일에서 작업시작 시간 가져온다.
BOOL CCyclerChannel::UpdateStartTimeFromCts(CString strCtsFilePath)
{
	CFileFind chFinder;
	CScheduleData *pSchData = NULL;
	BOOL bWorking1 = chFinder.FindFile(strCtsFilePath);
	COleDateTime timeStart = COleDateTime::GetCurrentTime(); //초기 값은 현재 시간으로.

	while (bWorking1)
	{
		bWorking1 = chFinder.FindNextFile();
		if (chFinder.IsDots())	continue;

		if (chFinder.IsDirectory())
		{
			CString strPath = chFinder.GetFilePath();

			char *buffer = NULL;
			CFileFind afinder;
			int FileSize = 0;
			if(afinder.FindFile(strPath+"\\*."+PS_RESULT_FILE_NAME_EXT))
			{
				afinder.FindNextFile();
				CString strFilePathName = afinder.GetFilePath();
				FILE *fpTemp = fopen(strFilePathName , "rb" );

				if (fpTemp)
				{
					PS_TEST_RESULT_FILE_HEADER *pHeader = new PS_TEST_RESULT_FILE_HEADER;
					ZeroMemory(pHeader,sizeof(PS_TEST_RESULT_FILE_HEADER));
					int iRawSize = sizeof(PS_TEST_RESULT_FILE_HEADER);

					fread(pHeader,iRawSize,1,fpTemp);

					timeStart.ParseDateTime(pHeader->testHeader.szStartTime);	//파일 시간으로 갱신	

					CString strTemp;
					strTemp.Format("work start time: %s",timeStart.Format());
					WriteLog(strTemp);

					if (pHeader != NULL)
					{
						delete pHeader;
						pHeader = NULL;
					}
					if (buffer != NULL)
					{
						delete buffer;
						buffer = NULL;
					}
					fclose( fpTemp );

					m_startTime = timeStart; //갱신
					return TRUE;
				}
			}			
		}
	}

	return FALSE;
}


//ksj 20200702 : 네트워크 에러 카운터 및 타이머 초기화
//네트워크 에러 일시정지 발생시 작업계속 해주는 기능 일정량 이상 실행되면 더 이상 작업계속 하지 않는다.
//신규 작업 시작할때 초기화 해줘야함. 
void CCyclerChannel::ResetNetworkErrorCount(void)
{
	m_nNetworkErrorCnt = 0; //네트워크 이상 횟수 1회 증가
	m_nNetworkErrorTimer = 0; //타이머 리셋				
}

void CCyclerChannel::DeleteBuffer(float * pfAuxBuff , SFT_CAN_VALUE * pCanBuff , float * pfBuff)
{
 	if(pfAuxBuff != NULL)
 	{
 		delete [] pfAuxBuff;
 		pfAuxBuff = NULL;
 	}
 	if(pCanBuff != NULL)
 	{
 		delete [] pCanBuff;
 		pCanBuff = NULL;
 	}
	if(pfBuff != NULL)
	{	
		delete[] pfBuff;		
		pfBuff = NULL;
	}
}
