#if !defined(AFX_SIMPLETESTDLG_H__E7E41A55_878D_4737_A579_20AA6DEB074D__INCLUDED_)
#define AFX_SIMPLETESTDLG_H__E7E41A55_878D_4737_A579_20AA6DEB074D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SimpleTestDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSimpleTestDlg dialog

class CSimpleTestDlg : public CDialog
{
// Construction
public:
	
	CSimpleTestDlg(CWnd* pParent = NULL);   // standard constructor
	CScheduleData	m_ScheduleInfo;
	BOOL			m_bOverChargerSet;  //yulee 20191017 OverChargeDischarger Mark


// Dialog Data
	//{{AFX_DATA(CSimpleTestDlg)
	enum { IDD = IDD_SIMPLE_TEST_DLG , IDD2 = IDD_SIMPLE_TEST_DLG_ENG ,IDD3 = IDD_SIMPLE_TEST_DLG_PL };
	CDateTimeCtrl	m_ctrlSaveTime;
	CDateTimeCtrl	m_ctrlEndTime;
	BOOL	m_ckSimpleSave;
	CString	m_strIRef;
	CString	m_strEndI;
	CString	m_strVref;
	CString	m_strEndV;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSimpleTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSimpleTestDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnOutofmemorySpin1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMPLETESTDLG_H__E7E41A55_878D_4737_A579_20AA6DEB074D__INCLUDED_)
