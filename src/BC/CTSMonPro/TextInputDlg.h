#if !defined(AFX_TEXTINPUTDLG_H__E7E8CC64_3560_4244_B8B5_6743C8C0553C__INCLUDED_)
#define AFX_TEXTINPUTDLG_H__E7E8CC64_3560_4244_B8B5_6743C8C0553C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTextInputDlg dialog

class CTextInputDlg : public CDialog
{
// Construction
public:
	CTextInputDlg(CString strPrevText, 
				  BYTE byMode = 0x00, 
				  CRect* rRect = NULL, 
				  CWnd* pParent = NULL);   // standard constructor
	bool IsValidPassword() { return m_bRightAdmin; }
// Dialog Data
	//{{AFX_DATA(CTextInputDlg)
	enum { IDD = IDD_TEXTINPUT_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	BYTE	m_byMode;
	CRect	m_rWindowRect;
	CString m_strData;
	bool	m_bRightAdmin;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextInputDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTextInputDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusEdit1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXTINPUTDLG_H__E7E8CC64_3560_4244_B8B5_6743C8C0553C__INCLUDED_)
