#if !defined(AFX_DLG_DBLOGIN_H__17B4F74D_3AE9_4752_A623_7C875D520EC9__INCLUDED_)
#define AFX_DLG_DBLOGIN_H__17B4F74D_3AE9_4752_A623_7C875D520EC9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Dblogin.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_Dblogin dialog

class Dlg_Dblogin : public CDialog
{
// Construction
public:	
	Dlg_Dblogin(CWnd* pParent = NULL);   // standard constructor

	void InitObject();
	BOOL chkDBTest(BOOL bCreate);

// Dialog Data
	//{{AFX_DATA(Dlg_Dblogin)
	enum { IDD = IDD_DIALOG_DBLOGIN , IDD2 = IDD_DIALOG_DBLOGIN_ENG ,IDD3 = IDD_DIALOG_DBLOGIN_PL };
	CEdit	m_EditDbname;
	CEdit m_EditDbip;
	CEdit m_EditDbport;
	CEdit m_EditDbid;
	CEdit m_EditDbpwd;
	CButton m_ChkAuto;		
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_Dblogin)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_Dblogin)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	afx_msg void OnButDbtest();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_DBLOGIN_H__17B4F74D_3AE9_4752_A623_7C875D520EC9__INCLUDED_)
