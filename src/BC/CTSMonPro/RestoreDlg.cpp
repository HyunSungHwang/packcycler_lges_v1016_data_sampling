// RestoreDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "RestoreDlg.h"

#include "CTSMonProDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRestoreDlg dialog


CRestoreDlg::CRestoreDlg(CCTSMonProDoc *pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CRestoreDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CRestoreDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CRestoreDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CRestoreDlg::IDD3):
	(CRestoreDlg::IDD), pParent)

{
	//{{AFX_DATA_INIT(CRestoreDlg)
	m_bLineOffInclude = FALSE;
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	m_iModuleListCount = 0;
	m_iChannelListCount = 0;
	for(int iCnt=0;iCnt < 255; iCnt++)
	{
		m_iModuleList[iCnt] = 0;
		m_iChannelList[iCnt] = 0;
	}
}


void CRestoreDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRestoreDlg)
	DDX_Control(pDX, IDC_COMBO_CH, m_ctrlChannel);
	DDX_Control(pDX, IDC_COMBO_MD, m_ctrlModule);
	DDX_Control(pDX, IDC_RESTORE_SEL_CH_BUTTON, m_btnRestore);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_UPDATE_BUTTON, m_btnUpdate);
	DDX_Control(pDX, IDC_LIST3, m_ctrlRangeList);
	DDX_Control(pDX, IDC_DATA_LOSS_CH_LIST, m_ctrlChList);
	DDX_Control(pDX, IDC_RESTORE_CAUTION_STATIC, m_CautionText);
	DDX_Check(pDX, IDC_CHECK1, m_bLineOffInclude);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRestoreDlg, CDialog)
	//{{AFX_MSG_MAP(CRestoreDlg)
	ON_BN_CLICKED(IDC_RESTORE_SEL_CH_BUTTON, OnRestoreSelChButton)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DATA_LOSS_CH_LIST, OnItemchangedDataLossChList)
	ON_BN_CLICKED(IDC_UPDATE_BUTTON, OnSearchUpdateButton)
	ON_CBN_SELCHANGE(IDC_COMBO_MD, OnSelchangeComboMd)
	ON_BN_CLICKED(IDC_RESTORE_LOCAL_FOLDER_BUTTON, OnRestoreLocalFolderButton)
	ON_BN_CLICKED(IDC_RESTORE_LOCAL_FOLDER_BUTTON2, OnRestoreLocalFolderButton2)
	ON_BN_CLICKED(IDC_BTN_RESTORE_CTS_FILE, OnBtnRestoreCtsFile)
	ON_BN_CLICKED(IDC_BTN_RESTORE_CYC_CHECK, OnBtnRestoreCycCheck)
	ON_BN_CLICKED(IDC_BNT_DOWN_RESTORE_DATA, OnBntDownRestoreData)
	ON_BN_CLICKED(IDC_BUT_RESTORE2, OnButRestore2)
	ON_BN_CLICKED(IDC_BUT_FILE_DIV, OnButFileDiv)
	ON_BN_CLICKED(IDC_BUT_DIV2, OnButDiv2)
	ON_BN_CLICKED(IDC_BTN_RESTORE_CTS_FILE2, OnBtnRestoreCtsFile2)
	ON_BN_CLICKED(IDC_RESTORE_LOCAL_FOLDER_BUTTON3, OnRestoreLocalFolderButton3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRestoreDlg message handlers

BOOL CRestoreDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

//	m_CautionText.SetTextColor(RGB(255, 0 , 0));
//	m_CautionText.SetFontAlign(0);
	
	// TODO: Add extra initialization here
	InitChList();
	InitRangeList();

	CString strTemp;
	int nID;
	m_ctrlModule.AddString("== All ==");
	m_ctrlModule.SetItemData(0, 0);

	int nTotMdCount = m_pDoc->GetInstallModuleCount();
	for(int md =0; md<nTotMdCount; md++)
	{
		nID = m_pDoc->GetModuleID(md);
		m_ctrlModule.AddString(m_pDoc->GetModuleName(nID));
		m_ctrlModule.SetItemData(md+1, nID);
	}
	m_ctrlModule.SetCurSel(0);


	m_ctrlChannel.AddString("== All ==");
	m_ctrlChannel.SetItemData(0, 0);
	m_ctrlChannel.SetCurSel(0);


// #ifdef _DEBUG
// 	GetDlgItem(IDC_RESTORE_SEL_CH_BUTTON)->EnableWindow(TRUE);
// #endif
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRestoreDlg::InitChList()
{
//	m_stateImage.Create(IDB_CELL_STATE_ICON,19,17,RGB(255,255,255));
//	m_ctrlChList.SetImageList(&m_stateImage, LVSIL_SMALL);

	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Administrator", FALSE))
	{
		GetDlgItem(IDC_BNT_DOWN_RESTORE_DATA)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_BTN_RESTORE_CTS_FILE)->ShowWindow(SW_SHOW); //2014.10.20 숨김처리
//		GetDlgItem(IDC_BTN_RESTORE_CYC_CHECK)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RESTORE_LOCAL_FOLDER_BUTTON2)->ShowWindow(SW_SHOW);
		
	}
	//Channel selection List
	m_ctrlChList.InsertColumn(0, "MD",  LVCFMT_LEFT, 40);
	m_ctrlChList.InsertColumn(1, "CH",  LVCFMT_LEFT, 40);
	m_ctrlChList.InsertColumn(2, Fun_FindMsg("InitChList_msg1","IDD_DATA_RESTORE_DLG"),  LVCFMT_LEFT, 150);
	//@ m_ctrlChList.InsertColumn(2, "시험명",  LVCFMT_LEFT, 150);
	m_ctrlChList.InsertColumn(3, Fun_FindMsg("InitChList_msg2","IDD_DATA_RESTORE_DLG"),  LVCFMT_LEFT, 60);
	//@ m_ctrlChList.InsertColumn(3, "손실구간수",  LVCFMT_LEFT, 60);
	DWORD dwExStyle = m_ctrlChList.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;// | LVS_EX_SUBITEMIMAGES;
	m_ctrlChList.SetExtendedStyle( dwExStyle );
}

void CRestoreDlg::InitRangeList()
{
	//Channel selection List
	m_ctrlRangeList.InsertColumn(0, "No",  LVCFMT_LEFT, 30);
	m_ctrlRangeList.InsertColumn(1, "Step",  LVCFMT_RIGHT, 30);
	m_ctrlRangeList.InsertColumn(2, Fun_FindMsg("InitRangeList_msg1","IDD_DATA_RESTORE_DLG"),  LVCFMT_RIGHT, 100);
	//@ m_ctrlRangeList.InsertColumn(2, "손실구간",  LVCFMT_RIGHT, 100);
	m_ctrlRangeList.InsertColumn(3, Fun_FindMsg("InitRangeList_msg2","IDD_DATA_RESTORE_DLG"),  LVCFMT_LEFT, 90);
	//@ m_ctrlRangeList.InsertColumn(3, "시간구간",  LVCFMT_LEFT, 90);
	DWORD dwExStyle = m_ctrlRangeList.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;// | LVS_EX_SUBITEMIMAGES;
	m_ctrlRangeList.SetExtendedStyle( dwExStyle );
}


void CRestoreDlg::OnRestoreSelChButton() 
{
	// TODO: Add your control notification handler code here

 	int nModuleID, nChIndex;
	int nSel;
	CString strMsg;

	GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_RESTORE_SEL_CH_BUTTON)->EnableWindow(FALSE);
	
	POSITION	pos = m_ctrlChList.GetFirstSelectedItemPosition();
	CWordArray  aResoredSelArray;
	while(pos)
	{
		nSel = m_ctrlChList.GetNextSelectedItem(pos);

		nModuleID = atol(m_ctrlChList.GetItemText(nSel, 0));
		nChIndex = atol(m_ctrlChList.GetItemText(nSel, 1))-1;
		
		CCyclerModule *pMD;
		CCyclerChannel *pCh;

		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD)
		{
			pCh = pMD->GetChannelInfo(nChIndex);

			//20170517 add 일시정지 일때만 진행 하도록 수정
			if( pCh->GetState() == PS_STATE_IDLE || 
				pCh->GetState() == PS_STATE_STANDBY ||
				pCh->GetState() == PS_STATE_END	||
				pCh->GetState() == PS_STATE_READY ||
				pCh->GetState() == PS_STATE_PAUSE)	
			{

			}
			else
			{
// 				AfxMessageBox("동작중인 채널은 데이터 복구시 오류가 발생 할수 있습니다. (잠시멈춤 or 완료 후 진행하세요.)");
// 				return;

				//ksj 20201016 : 동작중인 채널도 복구 가능하도록 일단 선택권을 줌. 
				if(MessageBox("동작중인 채널은 데이터 복구시 오류가 발생 할수 있습니다. (잠시멈춤 or 완료 후 진행을 권장드립니다.)\n계속하시겠습니까?","경고",MB_ICONWARNING|MB_YESNO) == IDNO)
					return;
			}

			if(pCh)		//손실된 Data가 존재할 경우 
			{

				pCh->WriteLog(Fun_FindMsg("OnRestoreSelChButton_msg1","IDD_DATA_RESTORE_DLG"));
				//@ pCh->WriteLog("손실 data 복구 시도");
				if(pCh->RestoreLostData(pMD->GetIPAddress()) == FALSE)
				{
					strMsg.Format(Fun_FindMsg("OnRestoreSelChButton_msg2","IDD_DATA_RESTORE_DLG"), pCh->GetLastErrorString());
					//@ strMsg.Format("손실 data 복구 실패(%s)", pCh->GetLastErrorString());
					pCh->WriteLog(strMsg);
					strMsg.Format(Fun_FindMsg("OnRestoreSelChButton_msg3","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1, pCh->GetLastErrorString());
					//@ strMsg.Format("%s Channel %d data 복구 실패(%s)", m_pDoc->GetModuleName(nModuleID), nChIndex+1, pCh->GetLastErrorString());
				}
				else
				{
					strMsg.Format(Fun_FindMsg("OnRestoreSelChButton_msg4","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1);
					//@ strMsg.Format("%s Channel %d data 복구 성공", m_pDoc->GetModuleName(nModuleID), nChIndex+1);
					pCh->WriteLog(Fun_FindMsg("OnRestoreSelChButton_msg5","IDD_DATA_RESTORE_DLG"));
					//@ pCh->WriteLog("손실 data 복구 성공");
					aResoredSelArray.Add(nSel);
				}
				m_pDoc->WriteSysLog(strMsg);
			}
		}
	}
	
	//복구 완성한 목록 삭제
	for(int a = aResoredSelArray.GetSize(); a>0 ; a--)
	{
		m_ctrlChList.DeleteItem(aResoredSelArray[a-1]);
	}
	
	nSel = m_ctrlChList.GetItemCount();
	if( nSel > 0)
	{
		strMsg.Format(Fun_FindMsg("OnRestoreSelChButton_msg1","IDD_DATA_RESTORE_DLG"), nSel);
		//@ strMsg.Format("%d개의 손실된 채널 발견", nSel);
		GetDlgItem(IDC_RESTORE_SEL_CH_BUTTON)->EnableWindow(TRUE);
	}
	else
	{
		strMsg.Format(Fun_FindMsg("OnRestoreSelChButton_msg2","IDD_DATA_RESTORE_DLG"), nSel);
		//@ strMsg.Format("Data가 손실된 채널이 없습니다.", nSel);
	}
	GetDlgItem(IDC_LOSS_COUNT_STATIC)->SetWindowText(strMsg);

	GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(TRUE);

	m_ctrlRangeList.DeleteAllItems();
}

void CRestoreDlg::OnItemchangedDataLossChList(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	int nModuleID, nChIndex;
	POSITION pos = m_ctrlChList.GetFirstSelectedItemPosition();
    int nSel = m_ctrlChList.GetNextSelectedItem(pos);
	if(nSel >=  0)
	{
		nModuleID = atol(m_ctrlChList.GetItemText(nSel, 0));
		nChIndex = atol(m_ctrlChList.GetItemText(nSel, 1))-1;
		
		FindLossDataRange(nModuleID, nChIndex);
	}
	*pResult = 0;
}

int CRestoreDlg::FindLossDataRange(int nModuleID, int nChIndex)
{
	CString strTemp, strMsg;	
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;//|LVIF_IMAGE;
	
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	CStringList *pLostRangeList;

	m_ctrlRangeList.DeleteAllItems();

	pMD = m_pDoc->GetModuleInfo(nModuleID);
	if(pMD)
	{
		pCh = pMD->GetChannelInfo(nChIndex);
		if(pCh)		//손실된 Data가 존재할 경우 
		{
			pLostRangeList = pCh->GetLostDataRangeList();
			POSITION pos = pLostRangeList->GetHeadPosition();
			int i =0;
			while(pos)
			{
				lvItem.iItem = i;
				lvItem.iSubItem = 0;
				strTemp.Format("%d", i+1);
				lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
				m_ctrlRangeList.InsertItem(&lvItem);

				strTemp = pLostRangeList->GetNext(pos);
				//시작 구간 ~ 끝 구간, Step 번호
				strMsg = strTemp.Mid(strTemp.Find(',')+1);
				m_ctrlRangeList.SetItemText(lvItem.iItem, 1, strMsg);

				strMsg = strTemp.Left(strTemp.Find(','));
				m_ctrlRangeList.SetItemText(lvItem.iItem, 2, strMsg);
				i++;
			}
		}
	}
	return m_ctrlChList.GetItemCount();
}

void CRestoreDlg::OnSearchUpdateButton() 
{
	// TODO: Add your control notification handler code here

	UpdateData();

	CString strTitle;
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	int nModuleID;
	int nCount = 0;

	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;

	m_ctrlChList.DeleteAllItems();

	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	int nID = m_ctrlModule.GetItemData(nIndex);
	CWordArray aModule;
	if(nID < 1)		//All Module
	{
		int nTotMdCount = m_pDoc->GetInstallModuleCount();
		for(int md =0; md<nTotMdCount; md++)
		{
			aModule.Add(m_pDoc->GetModuleID(md));
		}
	}
	else
	{
		aModule.Add(nID);
	}

	GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(FALSE);


	for(int md =0; md<aModule.GetSize(); md++)


	{
		nModuleID = aModule.GetAt(md);

		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD)
		{
			nIndex = m_ctrlChannel.GetCurSel();
			if(nIndex == CB_ERR)
			{ 
				nID = 0;
			}
			else
			{
				nID = m_ctrlChannel.GetItemData(nIndex);
			}
			CWordArray aChannel;
			if(nID < 1)		//All Module
			{
				for(int ch =0; ch<pMD->GetTotalChannel(); ch++)
				{
					aChannel.Add(ch);
				}
			}
			else
			{
				aChannel.Add(nID-1);
			}
			
			//All Channel check
			if(pMD->UpdateDataLossState(&aChannel, !m_bLineOffInclude) > 0)
			{
				for(int ch =0; ch < aChannel.GetSize(); ch++)
				{
					pCh = pMD->GetChannelInfo(aChannel.GetAt(ch));
					if(pCh &&  pCh->GetLostDataRangeSize() > 0)	
					{
						lvItem.iItem = nCount++;
						lvItem.iSubItem = 0;
						strTitle.Format("%d", nModuleID);
						lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
						m_ctrlChList.InsertItem(&lvItem);
						m_ctrlChList.SetItemData(lvItem.iItem, nModuleID);

						strTitle.Format("%d", aChannel.GetAt(ch)+1);
						m_ctrlChList.SetItemText(lvItem.iItem, 1, strTitle);

						m_ctrlChList.SetItemText(lvItem.iItem, 2, pCh->GetTestName());
						
						strTitle.Format("%d", pCh->GetLostDataRangeSize());
						m_ctrlChList.SetItemText(lvItem.iItem, 3, strTitle);
					}
				}
			}
		}
	}

	nCount = m_ctrlChList.GetItemCount();

	m_ctrlRangeList.DeleteAllItems();

	if( nCount > 0)
	{
		GetDlgItem(IDC_RESTORE_SEL_CH_BUTTON)->EnableWindow(TRUE);
		strTitle.Format(Fun_FindMsg("OnSearchUpdateButton_msg1","IDD_DATA_RESTORE_DLG"), nCount);
		//@ strTitle.Format("%d개의 손실된 채널 발견", nCount);
	}
	else
	{
		GetDlgItem(IDC_RESTORE_SEL_CH_BUTTON)->EnableWindow(FALSE);
		strTitle.Format(Fun_FindMsg("OnSearchUpdateButton_msg2","IDD_DATA_RESTORE_DLG"), nCount);
		//@ strTitle.Format("Data가 손실된 채널이 없습니다.", nCount);
	}

	GetDlgItem(IDC_LOSS_COUNT_STATIC)->SetWindowText(strTitle);

	GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(TRUE);

}

void CRestoreDlg::OnSelchangeComboMd() 
{
	// TODO: Add your control notification handler code here
	CString strTemp;
	int nID = 0;

	m_ctrlChannel.ResetContent();
	m_ctrlChannel.AddString("== All ==");
	m_ctrlChannel.SetItemData(0, 0);

	int index = m_ctrlModule.GetCurSel();
	if(index != CB_ERR)
	{
		nID = m_ctrlModule.GetItemData(index);
	}

	CCyclerModule	*pMD = m_pDoc->GetModuleInfo(nID);
	if(pMD)
	{
		//All Channel check
		for(int ch = 1; ch<=pMD->GetTotalChannel(); ch++)
		{
			strTemp.Format("Channel %d", ch);
			m_ctrlChannel.AddString(strTemp);
			m_ctrlChannel.SetItemData(ch, ch);
		}
	}
	m_ctrlChannel.SetCurSel(0);
}

void CRestoreDlg::LossDataAddModuleAndChannel(int nModuleID, int nChIndex)
{
	if(m_iModuleListCount == 0)
	{
		m_iModuleList[0] = nModuleID;
		m_iModuleListCount++;
		
		m_iChannelList[0] = nChIndex;
		m_iChannelListCount++;
	}
	else
	{
		for(int iCnt = 0; iCnt < m_iChannelListCount; iCnt++)
		{
			if(m_iChannelList[iCnt] == nChIndex) return;
		}
		m_iModuleList[m_iModuleListCount] = nModuleID;
		m_iModuleListCount++;
		
		m_iChannelList[m_iChannelListCount] = nChIndex;
		m_iChannelListCount++;
	}
}
BOOL CRestoreDlg::AutoRepair(int nModuleID, int nChIndex)
{
	BOOL bRtnValue = FALSE;
	
	//int nModuleID, nChIndex;
	//int nSel;
	CString strMsg;
	
	GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_RESTORE_SEL_CH_BUTTON)->EnableWindow(FALSE);
	
// 	for(nSel =0; nSel < m_iChannelListCount; nSel++)
// 	{
		
// 		nModuleID = m_iModuleList[nSel];
// 		nChIndex = m_iChannelList[nSel];
		
		CCyclerModule *pMD;
		CCyclerChannel *pCh;
		
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD)
		{
			pCh = pMD->GetChannelInfo(nChIndex);
			if(pCh)		//손실된 Data가 존재할 경우 
			{
				pCh->WriteLog(Fun_FindMsg("AutoRepair_msg1","IDD_DATA_RESTORE_DLG"));
				//@ pCh->WriteLog("손실 data 복구 시도");
				if(pCh->RestoreLostData(pMD->GetIPAddress()) == FALSE)
				{
					strMsg.Format(Fun_FindMsg("AutoRepair_msg2","IDD_DATA_RESTORE_DLG"), pCh->GetLastErrorString());
					//@ strMsg.Format("손실 data 복구 실패(%s)", pCh->GetLastErrorString());
					pCh->WriteLog(strMsg);
					strMsg.Format(Fun_FindMsg("AutoRepair_msg3","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1, pCh->GetLastErrorString());
					//@ strMsg.Format("%s Channel %d data 복구 실패(%s)", m_pDoc->GetModuleName(nModuleID), nChIndex+1, pCh->GetLastErrorString());
				}
				else
				{
					strMsg.Format(Fun_FindMsg("AutoRepair_msg4","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1);
					//@ strMsg.Format("%s Channel %d data 복구 성공", m_pDoc->GetModuleName(nModuleID), nChIndex+1);
					pCh->WriteLog(Fun_FindMsg("AutoRepair_msg5","IDD_DATA_RESTORE_DLG"));
					//@ pCh->WriteLog("손실 data 복구 성공");
					bRtnValue = TRUE;
				}
				m_pDoc->WriteSysLog(strMsg);
			}
		}
	//}
	
	
	GetDlgItem(IDC_LOSS_COUNT_STATIC)->SetWindowText(strMsg);
	
	GetDlgItem(IDC_UPDATE_BUTTON)->EnableWindow(TRUE);
	
	return bRtnValue;
}

void CRestoreDlg::OnRestoreLocalFolderButton() 
{
	int nModuleID;
	int nChIndex;
	CString strTemp, strSBCFolder,strFolder, strMsg;

	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton_msg1","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("모듈을 선택 하세요");
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);

	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton_msg2","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("채널을 선택 하세요");
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);

	//strFolder = 실제 저장된 경로와 동일 하게 폴더 만들고 복구할 데이터를 넣는다.
	//CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";
	strSBCFolder = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore" + "\\";
	strTemp.Format("M%02dCh%02d",nModuleID,nChIndex);
	strSBCFolder += strTemp;
	strSBCFolder = "C:\\A_Restore\\SBC";

	//채널 정보에서 가져옴
	//strFolder = "C:\\DATA\Pack_Data_v100B_LG_박재성\\00207594_110328_MOKA_7series_sample2_Capa@25oC_with_fan\\M01Ch01[001]";
// 	nModuleID = 1;
// 	nChIndex = 0;
	
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	pMD = m_pDoc->GetModuleInfo(nModuleID);
	if(pMD)
	{
		pCh = pMD->GetChannelInfo(nChIndex);
		pCh->LoadInfoFromTempFile();				//Temp File Load

		if(pCh->RestoreLostDataFromFolder(strSBCFolder,strFolder,nModuleID,nChIndex))
		{
			strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg3","IDD_DATA_RESTORE_DLG"), pCh->GetLastErrorString());
			//@ strMsg.Format("Folder 손실 data 복구 실패(%s)", pCh->GetLastErrorString());
			pCh->WriteLog(strMsg);
			strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg4","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1, pCh->GetLastErrorString());
			//@ strMsg.Format("Folder %s Channel %d data 복구 실패(%s)", m_pDoc->GetModuleName(nModuleID), nChIndex+1, pCh->GetLastErrorString());
			AfxMessageBox(strMsg);
		}
		else
		{
			strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg5","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex+1);
			//@ strMsg.Format("Folder %s Channel %d data 복구 성공", m_pDoc->GetModuleName(nModuleID), nChIndex+1);
			pCh->WriteLog(Fun_FindMsg("OnRestoreLocalFolderButton_msg6","IDD_DATA_RESTORE_DLG"));
			//@ pCh->WriteLog("Folder 손실 data 복구 성공");
			AfxMessageBox(strMsg);
		}
	}

	
}

void CRestoreDlg::OnRestoreLocalFolderButton2() 
{
	int nModuleID;
	int nChIndex;
	CString strCycFile,destFileName,strTemp, strSBCFolder,strFolder, strMsg;

	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton_msg7","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("모듈을 선택 하세요");
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton_msg8","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("채널을 선택 하세요");
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);
	
	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
	CFileDialog pDlg(TRUE, "cyc", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cyc file(*.cyc)|*.cyc|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = "The resulting data files location";
	//pDlg.m_ofn.lpstrTitle = Fun_FindMsg("OnRestoreLocalFolderButton_msg9");
	//@ pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strTemp = pDlg.GetPathName();
	strCycFile = strTemp;
	destFileName.Format("%s_backup.cyc",strCycFile.Mid(0, strTemp.ReverseFind('.')));
	
	//ksj 20160802 선택한 cyc 하위 폴더 restore에서 데이타를 찾음.
	strSBCFolder.Format("%s\\restore",destFileName.Mid(0,destFileName.ReverseFind('\\')));
	
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	pMD = m_pDoc->GetModuleInfo(nModuleID);

// 	for(int i=0; i< 2; i++) //yulee 20190707
// 	{
	int i;
		if(pMD)
		{
			//2014.10.15 주석 처리.
			pCh = pMD->GetChannelInfo(nChIndex-1);
			if(pCh == NULL) return;
			//pCh = pMD->GetChannelInfo(1);

			if(pCh->RestoreLostDataFromFolder2(strSBCFolder,strCycFile,nModuleID,nChIndex) == FALSE)
			{
				i = -1;
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg10","IDD_DATA_RESTORE_DLG"), pCh->GetLastErrorString(), i+1);
				//@ strMsg.Format("Folder 손실 data 복구 실패(%s)", pCh->GetLastErrorString());
				pCh->WriteLog(strMsg);
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg11","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex, pCh->GetLastErrorString());
				//@ strMsg.Format("Folder %s Channel %d data 복구 실패(%s)", m_pDoc->GetModuleName(nModuleID), nChIndex, pCh->GetLastErrorString());
				AfxMessageBox(strMsg);
			}
			else
			{
				i = 1;
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg12","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex, i+1);
				//@ strMsg.Format("Folder %s Channel %d data 복구 성공", m_pDoc->GetModuleName(nModuleID), nChIndex);
				pCh->WriteLog(Fun_FindMsg("OnRestoreLocalFolderButton_msg13","IDD_DATA_RESTORE_DLG"));
				//@ pCh->WriteLog("Folder 손실 data 복구 성공");
				AfxMessageBox(strMsg);
			}
		}
/*	}*/


	

	//원본 파일을 삭제한다.
	//2014.10.20 주석 처리. RestoreLostDataFromFolder2 에서 파일명을 전부 변경처리함.
	/*
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
	
	sprintf(szFrom, "%s", strCycFile);
	::DeleteFile(szFrom);
	if(rename(destFileName, strCycFile) != 0)
	{
		strMsg.Format("%s 파일명을 변경할 수 없습니다.", destFileName);
		//LockFileUpdate(bLockFile);
		AfxMessageBox(strMsg);
		return ;
	}
	*/
	return;
/***********************************************************************************************************/
	CString srcFileName,strTestName,strTestFolder, strRawDataFolder;;
//	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");

// 	CFileDialog pDlg(TRUE, "cyc", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cyc file(*.cyc)|*.cyc|All Files(*.*)|*.*|");
// 	pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
// 	if(IDOK != pDlg.DoModal())
// 	{
// 		return;
// 	}
// 	strTemp = pDlg.GetFileName();
// 	strTestName = strTemp.Mid(0, strTemp.ReverseFind('.'));
// 	strTemp = pDlg.GetPathName();
// 	strTestFolder = strTemp.Mid(0, strTemp.ReverseFind('\\'));	
	


// 	CFileDialog pDlg2(TRUE, "csv", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "csv file(*.csv)|*.csv|All Files(*.*)|*.*|");
// 	pDlg2.m_ofn.lpstrTitle = "복구 데이터 파일 위치";
// 	if(IDOK != pDlg2.DoModal())
// 	{
// 		return;
// 	}
// 	strTemp = pDlg2.GetPathName();
//	strRawDataFolder = strTemp.Mid(0, strTemp.ReverseFind('\\'));
	strRawDataFolder = "C:\\A_Restore\\SBC";


// 	CFile		fRS;
// 	CFileStatus fs;
// 	CFileException e;	
// 	if( !fRS.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
// 	{
// 		//생성 실패
// 	}



	//******************  처음 부터 복구 하기 ********************
// 	CCyclerModule *pMD;
// 	CCyclerChannel *pCh;
//	int nModuleID, nChIndex;

//	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton_msg14","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("모듈을 선택 하세요");
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton_msg15","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("채널을 선택 하세요");
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);


//	CString srcFileName,destFileName;
	srcFileName = strTestFolder +"\\"+ strTestName + "."+PS_RAW_FILE_NAME_EXT;				//복구하고자 하는 파일
	destFileName = strTestFolder +"\\"+ strTestName + "_Backup."+PS_RAW_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 
	
	CString srcAuxFileName = strTestFolder +"\\"+ strTestName + "."+PS_AUX_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destAuxFileName = strTestFolder +"\\"+ strTestName + "_Backup."+PS_AUX_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destAuxFileName);		//Backup할 파일명이 있으면 삭제 
	
	CString srcCanFileName = strTestFolder +"\\"+ strTestName + "."+PS_CAN_FILE_NAME_EXT;				//복구하고자 하는 파일
	CString destCanFileName = strTestFolder +"\\"+ strTestName + "_Backup."+PS_CAN_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destCanFileName);		//Backup할 파일명이 있으면 삭제 


//////////////////////////////////////////////////////////////////////////
//	srcFileName = "C:\\test1\\M01Ch01\\Test1.cyc";
//	destFileName = "C:\\test1\\M01Ch01\\Test1_BackUp.cyc";
//////////////////////////////////////////////////////////////////////////
//	CString strMsg;
	CFileFind finder;
	int nColSize = 0;

	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cyc 파일용
	FILE *fpAuxSourceFile = NULL, *fpAuxDestFile = NULL;	//aux 파일용
	FILE *fpCanSourceFile = NULL, *fpCanDestFile = NULL;	//can 파일용

	fpSourceFile = fopen(srcFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
		//단 조건 파일은 복구 할 수 없다.
		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD)
		{
			pCh = pMD->GetChannelInfo(nChIndex);
			if(pCh->CreateResultDataFile(srcFileName) == FALSE)
			//if(pCh->CreateResultFileA(srcFileName) == FALSE)
			{
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg16","IDD_DATA_RESTORE_DLG"), srcFileName);
				//@ strMsg.Format("%s 생성을 실패 하였습니다.", srcFileName);
				//fclose(fpDestFile);
				AfxMessageBox(strMsg);
				return;	 
			}

			fpSourceFile = fopen(srcFileName, "rb");
			if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
			{
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg17","IDD_DATA_RESTORE_DLG"), srcFileName);
				//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
				AfxMessageBox(strMsg);
				return;	//실제 Data 분석 
			}
		}
	}

	//cyc용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg18","IDD_DATA_RESTORE_DLG"), destFileName);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		AfxMessageBox(strMsg);
		return; 
	}

// 	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
// 	{
// 		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
// 		//단 조건 파일은 복구 할 수 없다.
// 		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
// 		if(CreateResultFileA(srcFileName) == FALSE)
// 		{
// 			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
// 			LockFileUpdate(bLockFile);
// 			fclose(fpDestFile);
// 			delete pDownLoad;
// 			return FALSE;	 
// 		}
// 		fpSourceFile = fopen(srcFileName, "rb");
// 		return;
// 	}
	

		
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg19","IDD_DATA_RESTORE_DLG"), srcFileName);
			//@ strMsg.Format("파일 정보를 열수 없습니다.", srcFileName);
			AfxMessageBox(strMsg);
			return;	
		}
		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////

	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	if(finder.FindFile(srcAuxFileName) == TRUE)
	{
		
// 		if(m_chFileDataSet.GetAuxColumnCount() > 0)			
// 		{
			fpAuxDestFile = fopen(destAuxFileName, "wb");
			if(fpAuxDestFile == NULL)
			{
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg20","IDD_DATA_RESTORE_DLG"), destAuxFileName);
				//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destAuxFileName);
				AfxMessageBox(strMsg);
				return; 
			}

			fpAuxSourceFile = fopen(srcAuxFileName, "rb");
// 			if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
// 			{
// 				//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
// 				//단 조건 파일은 복구 할 수 없다.
// 				//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
// 				
// 				//if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.GetAuxColumnCount()) == FALSE)
// 				if(CreateAuxFileA(srcAuxFileName, 0, m_chFileDataSet.m_nAuxVoltCount, m_chFileDataSet.m_nAuxTempCount) == FALSE)
// 				{
// 					m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcAuxFileName);
// 					fclose(fpAuxDestFile);
// 					return FALSE;	 
// 				}
// 				fpAuxSourceFile = fopen(srcAuxFileName, "rb");
// 			}
			
			if(fpAuxSourceFile == NULL)	//PC쪽에 파일이 없을 경우
			{
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg21","IDD_DATA_RESTORE_DLG"), srcAuxFileName);
				//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", srcAuxFileName);
				fclose(fpAuxDestFile);
				AfxMessageBox(strMsg);
				return;	//실제 Data 분석 
			}
				
			//Header 복사 
			if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxSourceFile) > 0)
			{
				if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpAuxDestFile) < 1)
				{
					strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg22","IDD_DATA_RESTORE_DLG"), srcAuxFileName);
					//@ strMsg.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
					fclose(fpAuxSourceFile);
					fclose(fpAuxDestFile);
					AfxMessageBox(strMsg);
					return;	
				}
			}
			nAuxColSize = auxFileHeader.auxHeader.nColumnCount;
//		}
	}

	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//CAN용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	if(finder.FindFile(srcAuxFileName) == TRUE)
	{
// 	if(m_chFileDataSet.GetCanColumnCount() > 0)			
// 	{
		fpCanDestFile = fopen(destCanFileName, "wb");
		if(fpCanDestFile == NULL)
		{
			strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg23","IDD_DATA_RESTORE_DLG"), destCanFileName);
			//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destCanFileName);
			AfxMessageBox(strMsg);
			return; 
		}

		fpCanSourceFile = fopen(srcCanFileName, "rb");
// 		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
// 		{
// 			//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
// 			//단 조건 파일은 복구 할 수 없다.
// 			//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
// 			if(CreateCanFileA(srcCanFileName, 0, m_chFileDataSet.GetCanColumnCount()) == FALSE)
// 			{
// 				m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcCanFileName);
// 				fclose(fpCanDestFile);
// 				return FALSE;	 
// 			}
// 			fpCanSourceFile = fopen(srcCanFileName, "rb");
// 		}
		
		if(fpCanSourceFile == NULL)	//PC쪽에 파일이 없을 경우
		{
			strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg24","IDD_DATA_RESTORE_DLG"), srcCanFileName);
			//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
			fclose(fpCanDestFile);
			AfxMessageBox(strMsg);
			return;	//실제 Data 분석 
		}
			
		//Header 복사 
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpCanDestFile) < 1)
			{
				strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton_msg25","IDD_DATA_RESTORE_DLG"), srcCanFileName);
				//@ strMsg.Format("파일 정보를 열수 없습니다.", srcCanFileName);
				fclose(fpCanSourceFile);
				fclose(fpCanDestFile);
				AfxMessageBox(strMsg);
				return;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
// 	CFileFind finder;
// 	if(finder.FindFile(strExcelPath) == FALSE)
// 	{
// 		strExcelPath = "C:\\Program Files\\Microsoft Office\\OFFICE11\\EXCEL.EXE";
// 		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
// 		pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";
// 		if(IDOK != pDlg.DoModal())
// 		{
// 			return	"";
// 		}
// 		strExcelPath = pDlg.GetPathName();
// 		AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC,"Excel Path", strExcelPath);
// 	}
// 	return strExcelPath;	
}

void CRestoreDlg::OnBtnRestoreCtsFile() 
{
	int nModuleID;
	int nChIndex;
	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnBtnRestoreCtsFile_msg1","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("모듈을 선택 하세요");
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnBtnRestoreCtsFile_msg2","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("채널을 선택 하세요");
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);
	
	CString strTemp, strRawDataFolder,strFolder, strMsg;
	CString strRawDataFolder_auxT,strRawDataFolder_canM,strRawDataFolder_auxV,strRawDataFolder_canS;
	CString strCtsFile,strTestName,strTestFolder;
	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
	CString srcFileName,destFileName;
	
	CFileDialog pDlg(TRUE, "cts", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cts file(*.cts)|*.cts|All Files(*.*)|*.*|");
	CString tmpTitle = "";
	tmpTitle.Format("%s", Fun_FindMsg("OnBtnRestoreCtsFile_msg3","IDD_DATA_RESTORE_DLG"));
	pDlg.m_ofn.lpstrTitle = tmpTitle;
	//@ pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strTestName = pDlg.GetFileName();
	strTemp = pDlg.GetPathName();
	strCtsFile = strTemp;
	destFileName.Format("%s_backup.cts",strCtsFile.Mid(0, strTemp.ReverseFind('.')));
	_unlink(destFileName);		//CTS Backup할 파일명이 있으면 삭제 

	//Restore용 csv 경로
	strFolder = strTemp.Mid(0, strTemp.ReverseFind('\\'));
	strRawDataFolder = strFolder + "\\" + "Restore" + "\\";
	strTemp.Format("ch%03d_SaveEndData.csv",nChIndex);
	strRawDataFolder += strTemp;
	
	strRawDataFolder_auxT = strFolder + "\\" + "Restore" + "\\";
	strTemp.Format("ch%03d_SaveEndData_auxT.csv",nChIndex);
	strRawDataFolder_auxT += strTemp;
	strRawDataFolder_auxV = strFolder + "\\" + "Restore" + "\\";
	strTemp.Format("ch%03d_SaveEndData_auxV.csv",nChIndex);
	strRawDataFolder_auxV += strTemp;
	
	strRawDataFolder_canM = strFolder + "\\" + "Restore" + "\\";
	strTemp.Format("ch%03d_SaveEndData_canMaster.csv",nChIndex);
	strRawDataFolder_canM += strTemp;
	strRawDataFolder_canS = strFolder + "\\" + "Restore" + "\\";
	strTemp.Format("ch%03d_SaveEndData_canSlave.csv",nChIndex);
	strRawDataFolder_canS += strTemp;

	//CTS 복구용 파일 OPEN
	CStdioFile stepEndFile;
	CFileException ex;
	CString strReadData;
	if(!stepEndFile.Open(strRawDataFolder,CFile::modeRead,&ex))
	{
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile_msg4","IDD_DATA_RESTORE_DLG"), strRawDataFolder);
		//@ strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	BOOL bIsNotEOL;				// 라인이 존재하는지 확인.
	bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line
	
	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cts 파일용
	fpSourceFile = fopen(strCtsFile, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
	
				strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile_msg5","IDD_DATA_RESTORE_DLG"), srcFileName);
				//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
				AfxMessageBox(strMsg);
				return;	//실제 Data 분석 
	}
	
	//cts용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile_msg6","IDD_DATA_RESTORE_DLG"), destFileName);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		AfxMessageBox(strMsg);
		return; 
	}
	
	//cts Header 복사 
 	PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER; 
 	fread(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER),1 , fpSourceFile);
 	//pFileHeader->fileHeader.szCreateDateTime = "";
 	//sprintf(pFileHeader->testHeader.szStartTime,"%s","2017-04-19 오후 4:53:38");		//ljb 20170511 임시 날자 지정

	if(fwrite(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fpDestFile) < 1)
	{
		strMsg.Format("cts Header write error", srcFileName);
		AfxMessageBox(strMsg);
		return;	
	}

	CString strItem;
	int nOldIndex=0;

	PS_STEP_END_RECORD *pRecord = new PS_STEP_END_RECORD;
	while (stepEndFile.ReadString(strReadData))				// 라인이 존재하는지 확인.
	{
		ZeroMemory(pRecord,sizeof(PS_STEP_END_RECORD));

		pRecord->chNo = nChIndex;		//1 base
		pRecord->nIndexFrom = nOldIndex;

		AfxExtractSubString(strItem,strReadData,0,',');		//index
		pRecord->nIndexTo = atol(strItem)-1;

		nOldIndex = atol(strItem) ;
		pRecord->lSaveSequence = atol(strItem);

		AfxExtractSubString(strItem,strReadData,1,',');		//total Time day
		pRecord->fTotalTime_Day = atof(strItem);
		AfxExtractSubString(strItem,strReadData,2,',');		//total Time
		pRecord->fTotalTime = PCTIME(atol(strItem));

		AfxExtractSubString(strItem,strReadData,3,',');		//step Time day
		pRecord->fStepTime_Day = atof(strItem);
		AfxExtractSubString(strItem,strReadData,4,',');		//step Time
		pRecord->fStepTime = PCTIME(atol(strItem));
		
		AfxExtractSubString(strItem,strReadData,5,',');		//chState
		pRecord->chState=atol(strItem);
		
		AfxExtractSubString(strItem,strReadData,6,',');		//type
		pRecord->chStepType = atoi(strItem);

//		AfxExtractSubString(strItem,strReadData,7,',');		//mode
//		AfxExtractSubString(strItem,strReadData,8,',');		//select
		pRecord->chDataSelect = 1;

		AfxExtractSubString(strItem,strReadData,9,',');		//code
		pRecord->chCode = atoi(strItem);

//		AfxExtractSubString(strItem,strReadData,10,',');		//grade code	

		AfxExtractSubString(strItem,strReadData,11,',');		//step No
		pRecord->chStepNo = atoi(strItem);

		AfxExtractSubString(strItem,strReadData,12,',');	//V
		pRecord->fVoltage = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,13,',');	//I
		pRecord->fCurrent = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,14,',');	//charge Ah
		pRecord->fChargeAh = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,15,',');	//DisCharge Ah
		pRecord->fDisChargeAh = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,16,',');	//Capacitance
		pRecord->fCapacitance = atof(strItem);
		AfxExtractSubString(strItem,strReadData,17 ,',');	//watt
		pRecord->fWatt = atof(strItem);
		AfxExtractSubString(strItem,strReadData,18 ,',');	//charge Wh
		pRecord->fChargeWh = atof(strItem);
		AfxExtractSubString(strItem,strReadData,19 ,',');	//discharge Wh
		pRecord->fDisChargeWh = atof(strItem);
		AfxExtractSubString(strItem,strReadData,20 ,',');	//Impedance
		pRecord->fImpedance = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,27 , ',');//lmh add total cycler
		pRecord->nTotalCycleNum = atol(strItem);
		AfxExtractSubString(strItem,strReadData,28 , ',');//lmh add  elementCycle
		pRecord->nCurrentCycleNum = atol(strItem);
		AfxExtractSubString(strItem,strReadData,29 , ',');
		pRecord->nAccCycleGroupNum1 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,30 , ',');
		pRecord->nAccCycleGroupNum2 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,31 , ',');
		pRecord->nAccCycleGroupNum3 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,32 , ',');
		pRecord->nAccCycleGroupNum4 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,33 , ',');
		pRecord->nAccCycleGroupNum5 = atol(strItem);

		
		AfxExtractSubString(strItem,strReadData,39 ,',');
		pRecord->fAvgVoltage = LONG2FLOAT(atol(strItem));//lmh 20120228 fAvgCurrent->fAvgVoltage로 변경
		AfxExtractSubString(strItem,strReadData,40 ,',');
		pRecord->fAvgCurrent = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,41 ,',');	//cv time day
		pRecord->fCVTime_Day = atol(strItem);
		AfxExtractSubString(strItem,strReadData,42 ,',');
		pRecord->fCVTime = PCTIME(atol(strItem));
		AfxExtractSubString(strItem,strReadData,43 ,',');
		pRecord->lSyncDate = atol(strItem);
		AfxExtractSubString(strItem,strReadData,44 ,',');
		pRecord->fSyncTime = atof(strItem);

		if(fwrite(pRecord, sizeof(PS_STEP_END_RECORD), 1, fpDestFile) < 1)
		{
			strMsg.Format("cts body write error", srcFileName);
			AfxMessageBox(strMsg);
			return;	
		}
	}
	
	delete pFileHeader;
	delete pRecord;

	stepEndFile.Close();

	fclose(fpSourceFile);
	fclose(fpDestFile);

	//cts 원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
	
	sprintf(szFrom, "%s", strCtsFile);
	::DeleteFile(szFrom);				//복구전 파일 삭제
	if(rename(destFileName, strCtsFile) != 0)
	{
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile_msg7","IDD_DATA_RESTORE_DLG"), srcFileName);
		//@ strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		//LockFileUpdate(bLockFile);
		AfxMessageBox(strMsg);
		return ;
	}
/*************************************** ats 복구 ************************************/
	CString srcAuxFileName;
	srcAuxFileName.Format("%s.ats",strCtsFile.Mid(0, strCtsFile.ReverseFind('.'))); 
	destFileName.Format("%s_backup.ats",strCtsFile.Mid(0, strTemp.ReverseFind('.')));
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 
	
	//모듈에서 전송받은 Aux Step End 파일 Open
	if(!stepEndFile.Open(strRawDataFolder_auxT,CFile::modeRead,&ex))
	{
		strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_auxT);
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
		bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line

	CStdioFile stepEndFile2;
	CFileException ex2;
	CString strReadData2;
	BOOL bIsNotEOL2;

	if(!stepEndFile2.Open(strRawDataFolder_auxV,CFile::modeRead,&ex2))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_auxV);
		strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg1","IDD_DATA_RESTORE_DLG"), strRawDataFolder_auxV);//&&
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
		bIsNotEOL2=stepEndFile2.ReadString(strReadData2);    // 한 LINE 읽는다. -> Title Line

	BOOL bRestore = TRUE;
	fpSourceFile = fopen(srcAuxFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		
		//strMsg.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		//AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
		bRestore = FALSE;		//복구 안함.
	}
	
	//Ats용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	if (bRestore)
	{
		fpDestFile = fopen(destFileName, "wb");
		if(fpDestFile == NULL)
		{
			//strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
			strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg2","IDD_DATA_RESTORE_DLG"), destFileName);//&&
			AfxMessageBox(strMsg);
			//return; 
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	int nAuxT_index=0; 
	int nAuxV_index=0;

	//Aux File Header 복사 
	if (bRestore)
	{
		if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpSourceFile) > 0)
		{
			if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpDestFile) < 1)
			{
				//strMsg.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
				strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg3","IDD_DATA_RESTORE_DLG"), srcAuxFileName);//&&
				fclose(fpSourceFile);
				fclose(fpDestFile);
				//			return FALSE;	
			}
		}
		nAuxColSize = auxFileHeader.auxHeader.nColumnCount;

		if (nAuxColSize >0)
		{
			float* pfBuff = new float[nAuxColSize];
			while (stepEndFile.ReadString(strReadData))				// 라인이 존재하는지 확인.
			{
				stepEndFile2.ReadString(strReadData2);

				ZeroMemory(pfBuff,sizeof(float)*nAuxColSize);
			
				for (int jj=0; jj<nAuxColSize; jj++)
				{
					if (auxFileHeader.auxHeader.awColumnItem[jj]==PS_AUX_TEMPERATURE) //AuxT
					{
						AfxExtractSubString(strItem,strReadData,nAuxT_index+5,',');		//index
						pfBuff[jj] = atof(strItem);
						nAuxT_index++;
					}
					else if (auxFileHeader.auxHeader.awColumnItem[jj]==PS_AUX_VOLTAGE) //AuxV
					{
						AfxExtractSubString(strItem,strReadData2,nAuxV_index+5,',');		//index
						pfBuff[jj] = atof(strItem);
						nAuxV_index++;
					}

				}
				if(fwrite(pfBuff, sizeof(float)*nAuxColSize, 1, fpDestFile) < 1)
				{
					strMsg.Format("ats body write error", srcFileName);
					AfxMessageBox(strMsg);
					return;	
				}
				nAuxT_index=0;
				nAuxV_index=0;

			}
			
			stepEndFile.Close();
			stepEndFile2.Close();

			fclose(fpSourceFile);
			fclose(fpDestFile);
		
		//cts 원본 파일을 삭제한다.
		//	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
			ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
			ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
			
			sprintf(szFrom, "%s", srcAuxFileName);
			::DeleteFile(szFrom);
			if(rename(destFileName, srcAuxFileName) != 0)
			{
				//strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);//$1013
				strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile_msg9","IDD_DATA_RESTORE_DLG"), srcAuxFileName); //&&
				//LockFileUpdate(bLockFile);
				AfxMessageBox(strMsg);
				return ;
			}
		}
	}
	else
	{
		stepEndFile.Close();
		stepEndFile2.Close();
	}

/*************************************** can 복구 ************************************/
	CString srcCanFileName;
	//   srcCanFileName.Format("%s.ctc",strCtsFile.Mid(0, strTemp.ReverseFind('.'))); 
	srcCanFileName.Format("%s.ctc",strCtsFile.Mid(0, strCtsFile.ReverseFind('.'))); //yulee 20190319
	destFileName.Format("%s_backup.ctc",strCtsFile.Mid(0, strTemp.ReverseFind('.')));
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 
	
	int nCanM_ColSize = 0;
	int nCanS_ColSize = 0;

	//모듈에서 전송받은 Step End 파일 Open
	if(!stepEndFile.Open(strRawDataFolder_canM,CFile::modeRead,&ex))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_canM);
		strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg1","IDD_DATA_RESTORE_DLG"), strRawDataFolder_canM);//&&
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
	{
		bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line
		if (bIsNotEOL) 
		{
			nCanM_ColSize = str_Count(strReadData,",");
			nCanM_ColSize = nCanM_ColSize -4;
		}
	}

// 	CStdioFile stepEndFile2;
// 	CFileException ex2;
// 	CString strReadData2;
// 	BOOL bIsNotEOL2;
	if(!stepEndFile2.Open(strRawDataFolder_canS,CFile::modeRead,&ex2))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_canS);
		strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg1","IDD_DATA_RESTORE_DLG"), strRawDataFolder_canS);//&&
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
	{
		bIsNotEOL2=stepEndFile2.ReadString(strReadData2);    // 한 LINE 읽는다. -> Title Line
		if (bIsNotEOL2)
		{
			nCanS_ColSize = str_Count(strReadData2,",");
			nCanS_ColSize = nCanS_ColSize -4;
		}
	}
	
	bRestore = TRUE;

	fpSourceFile = fopen(srcCanFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		
		//strMsg.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
		//AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
		bRestore = FALSE;
	}
	
	//cts용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	if (bRestore)
	{
		fpDestFile = fopen(destFileName, "wb");
		if(fpDestFile == NULL)
		{
			//strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
			strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg2","IDD_DATA_RESTORE_DLG"), destFileName);//&&
			AfxMessageBox(strMsg);
			//return; 
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//ctc용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	int nCanM_index=0; 
	int nCanS_index=0;
	
	if (bRestore)
	{
		//CAN File Header 복사 
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpDestFile) < 1)
			{
				//strMsg.Format("파일 정보를 열수 없습니다.", destFileName);
				strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg3","IDD_DATA_RESTORE_DLG"), destFileName);//&&
				fclose(fpSourceFile);
				fclose(fpDestFile);
				//			return FALSE;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;

		if (nCanColSize >0)
		{
			SFT_CAN_VALUE * pCanBuff = new SFT_CAN_VALUE[nCanColSize];
			//float* pfBuff = new float[nAuxColSize];
			while (stepEndFile.ReadString(strReadData))				// 라인이 존재하는지 확인.
			{
				stepEndFile2.ReadString(strReadData2);

				ZeroMemory(pCanBuff,sizeof(SFT_CAN_VALUE)*nCanColSize);
			
				for (int jj=0; jj<nCanColSize; jj++)
				{
					if (canFileHeader.canHeader.awColumnItem[jj]==PS_CAN_VALUE) //master
					{
						if (jj < nCanM_ColSize)
						{
							AfxExtractSubString(strItem,strReadData,nCanM_index+5,',');		//index
							pCanBuff[jj].fVal[0] = atof(strItem);
							nCanM_index++;

						}
						else
						{
							AfxExtractSubString(strItem,strReadData2,nCanS_index+5,',');		//index
							pCanBuff[jj].fVal[0] = atof(strItem);
							nCanS_index++;
						}
					}
				}
				if(fwrite(pCanBuff, sizeof(SFT_CAN_VALUE)*nCanColSize, 1, fpDestFile) < 1)
				{
					strMsg.Format("ctc body write error", srcFileName);
					AfxMessageBox(strMsg);
					return;	
				}
				nCanM_index=0;
				nCanS_index=0;

			}
			
			fclose(fpSourceFile);
			fclose(fpDestFile);

			stepEndFile.Close();
			stepEndFile2.Close();

			
			//cts 원본 파일을 삭제한다.
	// 		char szFrom[_MAX_PATH], szTo[_MAX_PATH];
			ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
			ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
			
			sprintf(szFrom, "%s", srcCanFileName);
			::DeleteFile(szFrom);
			if(rename(destFileName, srcCanFileName) != 0)
			{
				//strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
				strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile_msg4","IDD_DATA_RESTORE_DLG"), srcCanFileName);//&&
				//LockFileUpdate(bLockFile);
				AfxMessageBox(strMsg);
				return ;
			}
		}
	}
	else
	{
		stepEndFile.Close();
		stepEndFile2.Close();
	}
	
	return;		
}

void CRestoreDlg::OnBtnRestoreCycCheck() 
{
	// TODO: Add your control notification handler code here
	CString strCtsFile,strTestName,strTestFolder, strRawDataFolder,strTemp;;
	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
	CString srcFileName,destFileName,strMsg;
	
	CFileDialog pDlg(TRUE, "cyc", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cyc file(*.cyc)|*.cyc|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = Fun_FindMsg("OnBtnRestoreCycCheck_msg8","IDD_DATA_RESTORE_DLG");
	//@ pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strTestName = pDlg.GetFileName();
	strTemp = pDlg.GetPathName();
	strCtsFile = strTemp;
	destFileName.Format("%s_backup.cyc",strCtsFile.Mid(0, strTemp.ReverseFind('.')));
	
	
	// 	strTestFolder = strTemp.Mid(0, strTemp.ReverseFind('\\'));	
	// 	srcFileName = strTestFolder +"\\"+ strTestName + "."+PS_RESULT_FILE_NAME_EXT;				//복구하고자 하는 파일
	// 	destFileName = strTestFolder +"\\"+ strTestName + "_Backup."+PS_RESULT_FILE_NAME_EXT;		//복구해서 임시로 생성하는 파일
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 
	
	// 	CFile		fRS;
	// 	CFileStatus fs;
	// 	CFileException e;	
	// 	if( !fRS.Open( strNewTableFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &e ) )
	// 	{
	// 		//생성 실패
	// 	}
	
	
	//모듈에서 전송받은 Step End 파일 Open
// 	strRawDataFolder = "C:\\A_Restore\\SBC\\ch001_SaveEndData.csv";
// 	CStdioFile stepEndFile;
// 	CFileException ex;
// 	CString strReadData;
// 	if(!stepEndFile.Open(strRawDataFolder,CFile::modeRead,&ex))
// 	{
// 		strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder);
// 		AfxMessageBox(strMsg);
// 		return;	//실제 Data 분석 
// 	}
// 	BOOL bIsNotEOL;				// 라인이 존재하는지 확인.
// 	bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line
	
	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cyc 파일용
	fpSourceFile = fopen(strCtsFile, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCycCheck_msg1","IDD_DATA_RESTORE_DLG"), srcFileName);
		//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	
	//cyc용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnBtnTestoreCycCheck_msg2","IDD_DATA_RESTORE_DLG"), destFileName);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		AfxMessageBox(strMsg);
		return; 
	}
	
	// 	if(fpSourceFile == NULL)	//PC쪽에 파일이 없을 경우
	// 	{
	// 		//PC쪽에 아무 파일이 존재 하지 않아도 복구가 가능하도록 한다.
	// 		//단 조건 파일은 복구 할 수 없다.
	// 		//복구할 위치의 폴더가 없거나 파일이 이니 존재 하면 복구 할수 없다.
	// 		if(CreateResultFileA(srcFileName) == FALSE)
	// 		{
	// 			m_strLastErrorString.Format("%s 생성을 실패 하였습니다.", srcFileName);
	// 			LockFileUpdate(bLockFile);
	// 			fclose(fpDestFile);
	// 			delete pDownLoad;
	// 			return FALSE;	 
	// 		}
	// 		fpSourceFile = fopen(srcFileName, "rb");
	// 		return;
	// 	}
	
	
	
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader;
	if(fread(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			strMsg.Format(Fun_FindMsg("OnBtnTestoreCycCheck_msg3","IDD_DATA_RESTORE_DLG"), srcFileName);
			//@ strMsg.Format("파일 정보를 열수 없습니다.", srcFileName);
			AfxMessageBox(strMsg);
			return;	
		}
//		nColSize = FileHeader.rsHeader.nColumnCount;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////

	fclose(fpSourceFile);
	fclose(fpDestFile);
	
	//cyc 원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
	
	sprintf(szFrom, "%s", strCtsFile);
	::DeleteFile(szFrom);
	if(rename(destFileName, strCtsFile) != 0)
	{
		strMsg.Format(Fun_FindMsg("OnBtnTestoreCycCheck_msg4","IDD_DATA_RESTORE_DLG"), srcFileName);
		//@ strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		//LockFileUpdate(bLockFile);
		AfxMessageBox(strMsg);
		return ;
	}
	
	return;			
}

void CRestoreDlg::OnBntDownRestoreData() 
{
	int nModuleID;
	int nChIndex;
	CString strTemp, strSBCFolder,strFolder, strMsg;
	
	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnBtnDownRestoreData_msg1","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("모듈을 선택 하세요");
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnBntDownRestoreData_msg2","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("채널을 선택 하세요");
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);
	
	//strFolder = 실제 저장된 경로와 동일 하게 폴더 만들고 복구할 데이터를 넣는다.
	//CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";
	strSBCFolder = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore" + "\\";
	
	strTemp.Format("M%02dCh%02d",nModuleID,nChIndex);
	strSBCFolder += strTemp;
	
	if (m_pDoc->file_Finder(strSBCFolder) == FALSE) m_pDoc->ForceDirectory(strSBCFolder);

	if (Fun_RemoveDir(strSBCFolder) == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnBntDownRestoreData_msg3","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("폴더 삭제 오류");
		return;
	}
	
	//채널 정보에서 가져옴
	//strFolder = "C:\\DATA\Pack_Data_v100B_LG_박재성\\00207594_110328_MOKA_7series_sample2_Capa@25oC_with_fan\\M01Ch01[001]";
	// 	nModuleID = 1;
	// 	nChIndex = 0;
	
	CCyclerModule *pMD=NULL;
	CCyclerChannel *pCh=NULL;
	pMD = m_pDoc->GetModuleInfo(nModuleID);
	if(pMD)
	{
// 		pCh = pMD->GetChannelInfo(nChIndex);
// 		if(pCh)		//손실된 Data가 존재할 경우 
// 		{
// 		
		BOOL bRet = FALSE;		//yulee 20190427
		CWordArray aSelChArray;
		aSelChArray.Add((WORD)nChIndex);	//ljb IsFtpDataAllDownOK() 값이 FALE
		int CheckCntNum = 0;
		int SBCTotalFileNum = 0;
			int nDownNum = 0;
			bRet = m_pDoc->Fun_FtpDownSelChannelAllSBCData(m_pDoc->m_nFtpDownStepEndModuleID, &aSelChArray, CheckCntNum, SBCTotalFileNum, nDownNum);

// 			pCh->WriteLog("SBC DATA down 시도");
			if(pCh->Fun_DownloadData(pMD->GetIPAddress(),strSBCFolder,nModuleID, nChIndex) == FALSE)
			{
				strMsg.Format(Fun_FindMsg("OnBntDownRestoreData_msg4","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex);
				//@ strMsg.Format("Module: %s, channel : %d down 실패", m_pDoc->GetModuleName(nModuleID), nChIndex);
//				pCh->WriteLog("손실 data 복구 실패");
			}
			else
			{
				strMsg.Format(Fun_FindMsg("OnBntDownRestoreData_msg5","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex);
				//@ strMsg.Format("Module: %s, channel : %d down 성공", m_pDoc->GetModuleName(nModuleID), nChIndex);
//				pCh->WriteLog("손실 data 복구 성공");
			}
			m_pDoc->Fun_MakeOrEditFTPDownResultFile(m_pDoc->m_nFtpDownStepEndModuleID,  nChIndex,  bRet, SBCTotalFileNum, CheckCntNum, nDownNum);
			m_pDoc->WriteSysLog(strMsg);
//		}
	}
	
// 	pMD = m_pDoc->GetModuleInfo(nModuleID);
// 	if(pMD)
// 	{
// 		pCh = pMD->GetChannelInfo(nChIndex);
// 		//pCh->LoadInfoFromTempFile();				//Temp File Load
// 		
// 		if(pCh->RestoreLostDataFromFolder(strSBCFolder,strFolder,nModuleID,nChIndex))
// 		{
// 			strMsg.Format("Folder 손실 data 복구 실패(%s)", pCh->GetLastErrorString());
// 			pCh->WriteLog(strMsg);
// 			strMsg.Format("Folder %s Channel %d data 복구 실패(%s)", m_pDoc->GetModuleName(nModuleID), nChIndex+1, pCh->GetLastErrorString());
// 			AfxMessageBox(strMsg);
// 		}
// 		else
// 		{
// 			strMsg.Format("Folder %s Channel %d data 복구 성공", m_pDoc->GetModuleName(nModuleID), nChIndex+1);
// 			pCh->WriteLog("Folder 손실 data 복구 성공");
// 			AfxMessageBox(strMsg);
// 		}
// 	}	
}

BOOL CRestoreDlg::Fun_RemoveDir(CString strPath)
{
	CFileFind finder;
	BOOL bWorking = TRUE;
	CString strDirFile = strPath + CString("\\*.*");
	bWorking = finder.FindFile(strDirFile);
	while(bWorking)
	{
		bWorking = finder.FindNextFile();
		if(finder.IsDots()) continue;
		if(finder.IsDirectory())    Fun_RemoveDir(finder.GetFilePath());
		else    ::DeleteFile(finder.GetFilePath());
	}
	finder.Close();
	//::RemoveDirectory(strPath);
	return TRUE;
}

void CRestoreDlg::OnButRestore2() 
{
	// TODO: Add your control notification handler code here
	CString strTestName,strTestFolder, strRawDataFolder,strTemp;
	CString strMsg;

	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
	CString stratsFile,strauxFile,strctsFile,strcycFile;
	CString destFileName_ats,destFileName_aux,destFileName_cts,destFileName_cyc;
	
	CFileDialog pDlg(TRUE, "cyc", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cyc file(*.cyc)|*.cyc|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = Fun_FindMsg("OnBntDownRestoreData_msg6","IDD_DATA_RESTORE_DLG");
	//@ pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	
	strTestName = pDlg.GetFileName();
	strTemp = pDlg.GetPathName();
	//strCtsFile = strTemp;
	stratsFile.Format("%s.ats",strTemp.Mid(0, strTemp.ReverseFind('.')));
	strauxFile.Format("%s.aux",strTemp.Mid(0, strTemp.ReverseFind('.')));
	strctsFile.Format("%s.cts",strTemp.Mid(0, strTemp.ReverseFind('.')));
	strcycFile.Format("%s.cyc",strTemp.Mid(0, strTemp.ReverseFind('.')));
	
	destFileName_ats.Format("%s_backup.ats",strTemp.Mid(0, strTemp.ReverseFind('.')));
	destFileName_aux.Format("%s_backup.aux",strTemp.Mid(0, strTemp.ReverseFind('.')));
	destFileName_cts.Format("%s_backup.cts",strTemp.Mid(0, strTemp.ReverseFind('.')));
	destFileName_cyc.Format("%s_backup.cyc",strTemp.Mid(0, strTemp.ReverseFind('.')));

	_unlink(destFileName_ats);		//Backup할 파일명이 있으면 삭제 
	_unlink(destFileName_aux);		//Backup할 파일명이 있으면 삭제 
	_unlink(destFileName_cts);		//Backup할 파일명이 있으면 삭제 
	_unlink(destFileName_cyc);		//Backup할 파일명이 있으면 삭제 
		
	FILE *fpSourceFile = NULL, *fpDestFile = NULL;

	/// 1. ats file copy /////////////////////////////////////////////////////////////////////
	fpSourceFile = fopen(stratsFile, "rb");		//cyc 파일용
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg1","IDD_DATA_RESTORE_DLG"), stratsFile);
		//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", stratsFile);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	//cyc용 원본, 임시 파일을 연다.
	fpDestFile = fopen(destFileName_ats, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg2","IDD_DATA_RESTORE_DLG"), destFileName_ats);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName_ats);
		AfxMessageBox(strMsg);
		return; 
	}
	//Header 복사 
	PS_AUX_FILE_HEADER FileHeader_ats;
	if(fread(&FileHeader_ats, sizeof(PS_AUX_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader_ats, sizeof(PS_AUX_FILE_HEADER), 1, fpDestFile) < 1)
		{
			strMsg.Format(Fun_FindMsg("OnButRestore2_msg3","IDD_DATA_RESTORE_DLG"), stratsFile);
			//@ strMsg.Format("파일 정보를 열수 없습니다.", stratsFile);
			AfxMessageBox(strMsg);
			return;	
		}
	}
	fclose(fpSourceFile);
	fclose(fpDestFile);
	///////////////////////////////////////////////////////////////////////////////////////////

	/// 2. aux file copy /////////////////////////////////////////////////////////////////////
	fpSourceFile = fopen(strauxFile, "rb");		//cyc 파일용
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg4","IDD_DATA_RESTORE_DLG"), strauxFile);
		//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", strauxFile);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	//cyc용 원본, 임시 파일을 연다.
	fpDestFile = fopen(destFileName_aux, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg5","IDD_DATA_RESTORE_DLG"), destFileName_aux);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName_aux);
		AfxMessageBox(strMsg);
		return; 
	}
	//Header 복사 
	PS_AUX_FILE_HEADER FileHeader_aux;
	if(fread(&FileHeader_aux, sizeof(PS_AUX_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader_aux, sizeof(PS_AUX_FILE_HEADER), 1, fpDestFile) < 1)
		{
			strMsg.Format(Fun_FindMsg("OnButRestore2_msg6","IDD_DATA_RESTORE_DLG"), strauxFile);
			//@ strMsg.Format("파일 정보를 열수 없습니다.", strauxFile);
			AfxMessageBox(strMsg);
			return;	
		}
	}
	fclose(fpSourceFile);
	fclose(fpDestFile);
	///////////////////////////////////////////////////////////////////////////////////////////
	
	/// 3. cts file copy /////////////////////////////////////////////////////////////////////
	fpSourceFile = fopen(strctsFile, "rb");		//cyc 파일용
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg7","IDD_DATA_RESTORE_DLG"), strctsFile);
		//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", strctsFile);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	//cyc용 원본, 임시 파일을 연다.
	fpDestFile = fopen(destFileName_cts, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg8","IDD_DATA_RESTORE_DLG"), destFileName_cts);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName_cts);
		AfxMessageBox(strMsg);
		return; 
	}
	//Header 복사 
	PS_TEST_RESULT_FILE_HEADER FileHeader_cts;

	if(fread(&FileHeader_cts, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader_cts, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fpDestFile) < 1)
		{
			strMsg.Format(Fun_FindMsg("OnButRestore2_msg9","IDD_DATA_RESTORE_DLG"), strctsFile);
			//@ strMsg.Format("파일 정보를 열수 없습니다.", strctsFile);
			AfxMessageBox(strMsg);
			return;	
		}
	}
	fclose(fpSourceFile);
	fclose(fpDestFile);
	///////////////////////////////////////////////////////////////////////////////////////////

	/// 4. cyc file copy /////////////////////////////////////////////////////////////////////
	fpSourceFile = fopen(strcycFile, "rb");		//cyc 파일용
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg10","IDD_DATA_RESTORE_DLG"), strcycFile);
		//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", strcycFile);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	//cyc용 원본, 임시 파일을 연다.
	fpDestFile = fopen(destFileName_cyc, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnButRestore2_msg11","IDD_DATA_RESTORE_DLG"), destFileName_cyc);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName_cyc);
		AfxMessageBox(strMsg);
		return; 
	}
	//Header 복사 
	PS_RAW_FILE_HEADER FileHeader_cyc;
	if(fread(&FileHeader_cyc, sizeof(PS_RAW_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&FileHeader_cyc, sizeof(PS_RAW_FILE_HEADER), 1, fpDestFile) < 1)
		{
			strMsg.Format(Fun_FindMsg("OnButRestore2_msg12","IDD_DATA_RESTORE_DLG"), strcycFile);
			//@ strMsg.Format("파일 정보를 열수 없습니다.", strcycFile);
			AfxMessageBox(strMsg);
			return;	
		}
	}
	fclose(fpSourceFile);
	fclose(fpDestFile);
	///////////////////////////////////////////////////////////////////////////////////////////

	//cyc 원본 파일을 삭제한다.
// 	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
// 	ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
// 	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
// 	
// 	sprintf(szFrom, "%s", strCtsFile);
// 	::DeleteFile(szFrom);
// 	if(rename(destFileName_cyc, strcycFile) != 0)
// 	{
// 		strMsg.Format("%s 파일명을 변경할 수 없습니다.", destFileName_cyc);
// 		//LockFileUpdate(bLockFile);
// 		AfxMessageBox(strMsg);
// 		return ;
// 	}
		
}

void CRestoreDlg::OnButFileDiv() 
{
	// TODO: Add your control notification handler code here
	int nModuleID;
	int nChIndex;
	CString strTemp, strRawDataFolder,strFolder, strMsg;
	
	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnButFileDiv_msg1","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("모듈을 선택 하세요");
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		AfxMessageBox(Fun_FindMsg("OnButFileDiv_msg2","IDD_DATA_RESTORE_DLG"));
		//@ AfxMessageBox("채널을 선택 하세요");
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);
	
	//strFolder = 실제 저장된 경로와 동일 하게 폴더 만들고 복구할 데이터를 넣는다.
	strRawDataFolder = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore" + "\\";
	strTemp.Format("M%02dCh%02d\\ch%03d_SaveEndData.csv",nModuleID,nChIndex,nChIndex);
	strRawDataFolder += strTemp;
	
	CString strCtsFile,strTestName,strTestFolder;
	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
	CString srcFileName,destFileName;
	
	CFileDialog pDlg(TRUE, "cts", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cts file(*.cts)|*.cts|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = Fun_FindMsg("OnButFileDiv_msg3","IDD_DATA_RESTORE_DLG");
	//@ pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strTestName = pDlg.GetFileName();
	strTemp = pDlg.GetPathName();


	int nSeqNo = 1;
	strCtsFile = strTemp;
	destFileName.Format("%s_backup_%d.cts",strCtsFile.Mid(0, strTemp.ReverseFind('.')),nSeqNo);
	
	
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 

	CStdioFile stepEndFile;
	CFileException ex;
	CString strReadData;
	if(!stepEndFile.Open(strRawDataFolder,CFile::modeRead,&ex))
	{
		strMsg.Format(Fun_FindMsg("OnButFileDiv_msg4","IDD_DATA_RESTORE_DLG"), strRawDataFolder);
		//@ strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	BOOL bIsNotEOL;				// 라인이 존재하는지 확인.
	bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line
	
	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cts 파일용
	fpSourceFile = fopen(strCtsFile, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
	
				strMsg.Format(Fun_FindMsg("OnButFileDiv_msg5","IDD_DATA_RESTORE_DLG"), srcFileName);
				//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
				AfxMessageBox(strMsg);
				return;	//실제 Data 분석 
	}
	
	//cts용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnButFileDiv_msg6","IDD_DATA_RESTORE_DLG"), destFileName);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		AfxMessageBox(strMsg);
		return; 
	}
	
	//cts Header 복사 
	PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
	fread(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER),1 , fpSourceFile);
	if(fwrite(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fpDestFile) < 1)
	{
		strMsg.Format("cts Header write error", srcFileName);
		AfxMessageBox(strMsg);
		return;	
	}

	
	CString strItem;
	int nOldIndex=0;

	PS_STEP_END_RECORD *pRecord = new PS_STEP_END_RECORD;
	while (stepEndFile.ReadString(strReadData))				// 라인이 존재하는지 확인.
	{
		ZeroMemory(pRecord,sizeof(PS_STEP_END_RECORD));

		pRecord->chState=2;
		pRecord->nIndexFrom = nOldIndex;

		AfxExtractSubString(strItem,strReadData,0,',');		//index
		pRecord->nIndexTo = atol(strItem)-1;

		nOldIndex = atol(strItem) ;
		pRecord->lSaveSequence = atol(strItem);

		AfxExtractSubString(strItem,strReadData,1,',');		//total Time
		pRecord->fTotalTime = PCTIME(atol(strItem));
		AfxExtractSubString(strItem,strReadData,2,',');		//step Time
		pRecord->fStepTime = PCTIME(atol(strItem));
															//state 3
		
		AfxExtractSubString(strItem,strReadData,4,',');		//type
		pRecord->chStepType = atoi(strItem);
		AfxExtractSubString(strItem,strReadData,7,',');		//code
		pRecord->chCode = atoi(strItem);
		
		AfxExtractSubString(strItem,strReadData,9,',');		//step No
		pRecord->chNo = 1;
		pRecord->chStepNo = atoi(strItem);

		AfxExtractSubString(strItem,strReadData,10,',');	//V
		pRecord->fVoltage = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,11,',');	//I
		pRecord->fCurrent = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,12,',');	//charge Ah
		pRecord->fChargeAh = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,13,',');	//DisCharge Ah
		pRecord->fDisChargeAh = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,14,',');	//Capacitance
		pRecord->fCapacitance = atof(strItem);
		AfxExtractSubString(strItem,strReadData,15 ,',');	//watt
		pRecord->fWatt = atof(strItem);
		AfxExtractSubString(strItem,strReadData,16 ,',');	//charge Wh
		pRecord->fChargeWh = atof(strItem);
		AfxExtractSubString(strItem,strReadData,17 ,',');	//discharge Wh
		pRecord->fDisChargeWh = atof(strItem);
		AfxExtractSubString(strItem,strReadData,18 ,',');	//Impedance
		pRecord->fImpedance = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,25 , ',');//lmh add total cycler
		pRecord->nTotalCycleNum = atol(strItem);
		AfxExtractSubString(strItem,strReadData,26 , ',');//lmh add  elementCycle
		pRecord->nCurrentCycleNum = atol(strItem);


		
		AfxExtractSubString(strItem,strReadData,37 ,',');
		pRecord->fAvgVoltage = LONG2FLOAT(atol(strItem));//lmh 20120228 fAvgCurrent->fAvgVoltage로 변경
		AfxExtractSubString(strItem,strReadData,38 ,',');
		pRecord->fAvgCurrent = LONG2FLOAT(atol(strItem));


		if (pRecord->nIndexFrom > 9000000 && pRecord->nIndexFrom < 9100000 )
		{
			TRACE("======> nIndexFrom : %d \n", pRecord->nIndexFrom);
		}
		if (pRecord->nIndexFrom > 12000000 && pRecord->nIndexFrom < 12100000 )
		{
			TRACE("======> nIndexFrom : %d \n", pRecord->nIndexFrom);
		}
		if (pRecord->nIndexFrom > 15000000 && pRecord->nIndexFrom < 15100000 )
		{
			TRACE("======> nIndexFrom : %d \n", pRecord->nIndexFrom);
		}
		if (pRecord->nIndexFrom > 18000000 && pRecord->nIndexFrom < 18100000 )
		{
			TRACE("======> nIndexFrom : %d \n", pRecord->nIndexFrom);
		}
		if (pRecord->nIndexFrom > 21000000 && pRecord->nIndexFrom < 21100000 )
		{
			TRACE("======> nIndexFrom : %d \n", pRecord->nIndexFrom);
		}
		if (pRecord->nIndexFrom > 24000000 && pRecord->nIndexFrom < 24100000 )
		{
			TRACE("======> nIndexFrom : %d \n", pRecord->nIndexFrom);
		}

		if (pRecord->nIndexFrom == 3001715 || pRecord->nIndexFrom == 6000440 || pRecord->nIndexFrom == 9001977 ||
			pRecord->nIndexFrom == 12001704 || pRecord->nIndexFrom == 15000200 || pRecord->nIndexFrom == 18000215 ||
			pRecord->nIndexFrom == 21001608 || pRecord->nIndexFrom == 24002257 )
		{
			fclose(fpDestFile);

			nSeqNo++;
			destFileName.Format("%s_backup_%d.cts",strCtsFile.Mid(0, strTemp.ReverseFind('.')),nSeqNo);

			fpDestFile = fopen(destFileName, "wb");
			if(fpDestFile == NULL)
			{
				strMsg.Format(Fun_FindMsg("OnButFileDiv_msg7","IDD_DATA_RESTORE_DLG"), destFileName);
				//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
				AfxMessageBox(strMsg);
				return; 
			}

			//Header write
			if(fwrite(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fpDestFile) < 1)
			{
				strMsg.Format("cts Header write error", srcFileName);
				AfxMessageBox(strMsg);	
			}
			if(fwrite(pRecord, sizeof(PS_STEP_END_RECORD), 1, fpDestFile) < 1)
			{
				strMsg.Format("cts body write error", srcFileName);
				AfxMessageBox(strMsg);
				return;	
			}
		}
		else if(fwrite(pRecord, sizeof(PS_STEP_END_RECORD), 1, fpDestFile) < 1)
		{
			strMsg.Format("cts body write error", srcFileName);
			AfxMessageBox(strMsg);
			return;	
		}
	}
	
	delete pFileHeader;
	delete pRecord;

	fclose(fpSourceFile);
	fclose(fpDestFile);

	//cts 원본 파일을 삭제한다.
// 	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
// 	ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
// 	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
// 	
// 	sprintf(szFrom, "%s", strCtsFile);
// 	::DeleteFile(szFrom);
// 	if(rename(destFileName, strCtsFile) != 0)
// 	{
// 		strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
// 		//LockFileUpdate(bLockFile);
// 		AfxMessageBox(strMsg);
// 		return ;
// 	}
}

void CRestoreDlg::OnButDiv2() 
{
	int nModuleID;
	int nChIndex;
	nModuleID = nChIndex = 1;
	
// 	int nIndex = m_ctrlModule.GetCurSel();
// 	if(nIndex == CB_ERR)	return;
// 	if (nIndex < 1) 
// 	{
// 		AfxMessageBox("모듈을 선택 하세요");
// 		return;
// 	}
//	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
// 	nIndex = m_ctrlChannel.GetCurSel();
// 	if(nIndex == CB_ERR)	return;
// 	if (nIndex < 1) 
// 	{
// 		AfxMessageBox("채널을 선택 하세요");
// 		return;
// 	}
//	nChIndex = m_ctrlChannel.GetItemData(nIndex);
	
	CString strTemp,strFolder, strMsg;
	CString  strRawDataFolder_auxT, strRawDataFolder_auxV;
	CStdioFile stepEndFile_auxT, stepEndFile_auxV;
	CFileException ex1, ex2;
	CString strReadData_auxT, strReadData_auxV;
	BOOL bIsNotEOL1, bIsNotEOL2;
	
	//SBC 복구용 파일
	strFolder = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore" + "\\";
	
	strTemp.Format("M%02dCh%02d\\ch%03d_SaveEndData_auxT.csv",nModuleID,nChIndex,nChIndex);
	strRawDataFolder_auxT = strFolder + strTemp;

	strTemp.Format("M%02dCh%02d\\ch%03d_SaveEndData_auxV.csv",nModuleID,nChIndex,nChIndex);
	strRawDataFolder_auxV = strFolder + strTemp;
	

	//결과 파일
	CString strAtsFile,strTestName,strTestFolder;
	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
	CString srcFileName,destFileName;
	
	CFileDialog pDlg(TRUE, "ats", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "ats file(*.ats)|*.ats|All Files(*.*)|*.*|");
	pDlg.m_ofn.lpstrTitle = Fun_FindMsg("OnButDiv2_msg1","IDD_DATA_RESTORE_DLG");
	//@ pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
	if(IDOK != pDlg.DoModal())
	{
		return;
	}
	strTestName = pDlg.GetFileName();
	strTemp = pDlg.GetPathName();
	
	
	int nSeqNo = 1;

	//결과 파일명
	strAtsFile = strTemp;
	destFileName.Format("%s_backup_%d.ats",strAtsFile.Mid(0, strTemp.ReverseFind('.')),nSeqNo);

	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 

/*************************************** ats 복구 ************************************/
	
	if(!stepEndFile_auxT.Open(strRawDataFolder_auxT,CFile::modeRead,&ex1))
	{
		strMsg.Format(Fun_FindMsg("OnButDiv2_msg2","IDD_DATA_RESTORE_DLG"), strRawDataFolder_auxT);
		//@ strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_auxT);
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
		bIsNotEOL1=stepEndFile_auxT.ReadString(strReadData_auxT);    // 한 LINE 읽는다. -> Title Line

	if(!stepEndFile_auxV.Open(strRawDataFolder_auxV,CFile::modeRead,&ex2))
	{
		strMsg.Format(Fun_FindMsg("OnButDiv2_msg3","IDD_DATA_RESTORE_DLG"), strRawDataFolder_auxV);
		//@ strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_auxV);
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
		bIsNotEOL2=stepEndFile_auxV.ReadString(strReadData_auxV);    // 한 LINE 읽는다. -> Title Line


	//결과 파일 source ats, desfile ats
	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cts 파일용
	fpSourceFile = fopen(strAtsFile, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		strMsg.Format(Fun_FindMsg("OnButDiv2_msg4","IDD_DATA_RESTORE_DLG"), strAtsFile);
		//@ strMsg.Format("%s 파일 정보를 열수 없습니다.", strAtsFile);
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	
	//cts용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		strMsg.Format(Fun_FindMsg("OnButDiv2_msg5","IDD_DATA_RESTORE_DLG"), destFileName);
		//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		AfxMessageBox(strMsg);
		//return; 
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	//Aux File Header 복사 
	if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpSourceFile) > 0)
	{
		if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpDestFile) < 1)
		{
			strMsg.Format(Fun_FindMsg("OnButDiv2_msg6","IDD_DATA_RESTORE_DLG"), destFileName);
			//@ strMsg.Format("파일 정보를 열수 없습니다.", destFileName);
			fclose(fpSourceFile);
			fclose(fpDestFile);
			return;
		}
	}
	nAuxColSize = auxFileHeader.auxHeader.nColumnCount;

	CString strItem;
	int nOldIndex=0;
	if (nAuxColSize >0)
	{
		float* pfBuff;
		PS_STEP_END_RECORD *pRecord;
		while (stepEndFile_auxT.ReadString(strReadData_auxT))				// 라인이 존재하는지 확인.
		{
			pfBuff = new float[nAuxColSize];
			pRecord = new PS_STEP_END_RECORD;
			ZeroMemory(pRecord,sizeof(PS_STEP_END_RECORD));
		
			pRecord->chState=2;
			pRecord->nIndexFrom = nOldIndex;
			
			AfxExtractSubString(strItem,strReadData_auxT,0,',');		//index
			pRecord->nIndexTo = atol(strItem)-1;
			
			nOldIndex = atol(strItem) ;
			pRecord->lSaveSequence = atol(strItem);
			
			AfxExtractSubString(strItem,strReadData_auxT,1,',');		//total Time
			pRecord->fStepTime = PCTIME(atol(strItem));
			AfxExtractSubString(strItem,strReadData_auxT,2,',');		//step Time
			pRecord->fTotalTime = PCTIME(atol(strItem));

			AfxExtractSubString(strItem,strReadData_auxT,3,',');		//1
			pfBuff[0] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,4,',');		//2
			pfBuff[1] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,5,',');		//3
			pfBuff[2] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,6,',');		//4
			pfBuff[3] = atoi(strItem);

			stepEndFile_auxV.ReadString(strReadData_auxV);
			AfxExtractSubString(strItem,strReadData_auxT,3,',');		//volt 1
			pfBuff[4] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,4,',');		//volt 1
			pfBuff[5] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,5,',');		//volt 1
			pfBuff[6] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,6,',');		//volt 1
			pfBuff[7] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,7,',');		//volt 1
			pfBuff[8] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,8,',');		//volt 1
			pfBuff[9] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,9,',');		//volt 1
			pfBuff[10] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,10,',');		//volt 1
			pfBuff[11] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,11,',');		//volt 1
			pfBuff[12] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,12,',');		//volt 1
			pfBuff[13] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,13,',');		//volt 1
			pfBuff[14] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,14,',');		//volt 1
			pfBuff[15] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,15,',');		//volt 1
			pfBuff[16] = atoi(strItem);
			AfxExtractSubString(strItem,strReadData_auxT,16,',');		//volt 1
			pfBuff[17] = atoi(strItem);

			if (pRecord->nIndexFrom == 3001715 || pRecord->nIndexFrom == 6000440 || pRecord->nIndexFrom == 9001977 ||
				pRecord->nIndexFrom == 12001704 || pRecord->nIndexFrom == 15000200 || pRecord->nIndexFrom == 18000215 ||
				pRecord->nIndexFrom == 21001608 || pRecord->nIndexFrom == 24002257 )
			{
				fclose(fpDestFile);
				
				nSeqNo++;
				destFileName.Format("%s_backup_%d.ats",strAtsFile.Mid(0, strAtsFile.ReverseFind('.')),nSeqNo);
				
				fpDestFile = fopen(destFileName, "wb");
				if(fpDestFile == NULL)
				{
					strMsg.Format(Fun_FindMsg("OnButDiv2_msg7","IDD_DATA_RESTORE_DLG"), destFileName);
					//@ strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
					AfxMessageBox(strMsg);
					return; 
				}
				
				//Header write
				if(fwrite(&auxFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fpDestFile) < 1)
				{
					strMsg.Format("ats Header write error", srcFileName);
					AfxMessageBox(strMsg);	
				}
				if(fwrite(pfBuff, sizeof(float) * nAuxColSize, 1, fpDestFile) < 1)
				{
					strMsg.Format("ats body write error", srcFileName);
					AfxMessageBox(strMsg);
					return;	
				}
			}
			else if(fwrite(pfBuff, sizeof(float) * nAuxColSize, 1, fpDestFile) < 1)
			{
				strMsg.Format("ats body write error", srcFileName);
				AfxMessageBox(strMsg);
				return;	
			}
			delete [] pfBuff;
		}
		
		//delete auxFileHeader;
		delete pRecord;
		
		fclose(fpSourceFile);
		fclose(fpDestFile);
		
		//cts 원본 파일을 삭제한다.
// 		char szFrom[_MAX_PATH], szTo[_MAX_PATH];
// 		ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
// 		ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
// 		
// 		sprintf(szFrom, "%s", strCtsFile);
// 		::DeleteFile(szFrom);
// 		if(rename(destFileName, strCtsFile) != 0)
// 		{
// 			strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
// 			//LockFileUpdate(bLockFile);
// 			AfxMessageBox(strMsg);
// 			return ;
// 		}

	}	
}

int CRestoreDlg::str_Count(CString tmpstr,CString searchstr)//$1013
{	
    int tcount=0;
    int dcount=-1;
	
    for(int i=0; i<tmpstr.GetLength();i++){
		dcount=tmpstr.Find(searchstr,dcount+1);
		if(dcount>-1) ++tcount;
		else if(dcount==-1) break;
	}	
	return tcount;
}

void CRestoreDlg::OnBtnRestoreCtsFile2() 
{
	// TODO: Add your control notification handler code here
	CCyclerModule *pMD;
	CCyclerChannel *pCh;

	int nModuleID;
	int nChIndex;
	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		//AfxMessageBox("모듈을 선택 하세요");
		AfxMessageBox(Fun_FindMsg("OnBtnRestoreCtsFile2_msg1","IDD_DATA_RESTORE_DLG")); //&&
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		//AfxMessageBox("채널을 선택 하세요");
		AfxMessageBox(Fun_FindMsg("OnBtnRestoreCtsFile2_msg1","IDD_DATA_RESTORE_DLG")); //&&
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);

	pMD = m_pDoc->GetModuleInfo(nModuleID);
	pCh = pMD->GetChannelInfo(nIndex-1);
	
	if (pMD == NULL || pCh == NULL || pMD->GetState() == PS_STATE_LINE_OFF)
	{
		return;
	}
	CString strDownPath,strTemp,strMsg;
	strDownPath.Format("%s\\Restore",pCh->GetChFilePath());

	//strFolder = 실제 저장된 경로와 동일 하게 폴더 만들고 복구할 데이터를 넣는다.
	//strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore" + "\\";
	
	
	if (m_pDoc->file_Finder(strDownPath) == FALSE) m_pDoc->ForceDirectory(strDownPath);
	
	if (Fun_RemoveDir(strDownPath) == FALSE)
	{
		//AfxMessageBox("폴더 삭제 오류");
		AfxMessageBox(Fun_FindMsg("OnBtnRestoreCtsFile2_msg3","IDD_DATA_RESTORE_DLG")); //&&
		return;
	}
	
//	CCyclerModule *pMD;
//	CCyclerChannel *pCh;
//	pMD = m_pDoc->GetModuleInfo(nModuleID);
	if(pMD)
	{
		if(pCh->Fun_DownloadData(pMD->GetIPAddress(),strDownPath,nModuleID, nChIndex, FALSE) == FALSE)
		{
			//strMsg.Format("Module: %s, channel : %d down 실패", m_pDoc->GetModuleName(nModuleID), nChIndex);
			strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg4","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex); //&&
		}
		else
		{
			//strMsg.Format("Module: %s, channel : %d down 성공", m_pDoc->GetModuleName(nModuleID), nChIndex);
			strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg5","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex); //&&
		}
		m_pDoc->WriteSysLog(strMsg);
	}
	
	//step end 복구 구현 부분
	CString strRawDataFolder,strFolder;
	CString strRawDataFolder_auxT,strRawDataFolder_canM,strRawDataFolder_auxV,strRawDataFolder_canS;
	CString strCtsFile,strTestName,strTestFolder;
	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
	CString srcFileName,destFileName;
	
// 	CFileDialog pDlg(TRUE, "cts", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cts file(*.cts)|*.cts|All Files(*.*)|*.*|");
// 	pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
// 	if(IDOK != pDlg.DoModal())
// 	{
// 		return;
// 	}
// 	strTestName = pDlg.GetFileName();
// 	strTemp = pDlg.GetPathName();

	strTestName.Format("%s.cts", pCh->GetTestName());
	strTemp.Format("%s\\%s.cts", pCh->GetChFilePath(),pCh->GetTestName());
	
	strCtsFile = strTemp;
	destFileName.Format("%s_backup.cts",strCtsFile.Mid(0, strTemp.ReverseFind('.')));
	_unlink(destFileName);		//CTS Backup할 파일명이 있으면 삭제 

	//Restore용 csv 경로
	if (nChIndex > 1)
	{
		strFolder = strTemp.Mid(0, strTemp.ReverseFind('\\'));
		strRawDataFolder = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData.csv",nChIndex+1);
		strRawDataFolder += strTemp;
		
		strRawDataFolder_auxT = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_auxT.csv",nChIndex+1);
		strRawDataFolder_auxT += strTemp;
		strRawDataFolder_auxV = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_auxV.csv",nChIndex+1);
		strRawDataFolder_auxV += strTemp;
		
		strRawDataFolder_canM = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_canMaster.csv",nChIndex+1);
		strRawDataFolder_canM += strTemp;
		strRawDataFolder_canS = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_canSlave.csv",nChIndex+1);
		strRawDataFolder_canS += strTemp;

	}
	else
	{
		strFolder = strTemp.Mid(0, strTemp.ReverseFind('\\'));
		strRawDataFolder = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData.csv",nChIndex);
		strRawDataFolder += strTemp;
		
		strRawDataFolder_auxT = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_auxT.csv",nChIndex);
		strRawDataFolder_auxT += strTemp;
		strRawDataFolder_auxV = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_auxV.csv",nChIndex);
		strRawDataFolder_auxV += strTemp;
		
		strRawDataFolder_canM = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_canMaster.csv",nChIndex);
		strRawDataFolder_canM += strTemp;
		strRawDataFolder_canS = strFolder + "\\" + "Restore" + "\\";
		strTemp.Format("ch%03d_SaveEndData_canSlave.csv",nChIndex);
		strRawDataFolder_canS += strTemp;
	}

	//CTS 복구용 파일 OPEN
	CStdioFile stepEndFile;
	CFileException ex;
	CString strReadData;
	if(!stepEndFile.Open(strRawDataFolder,CFile::modeRead,&ex))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder);
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg6","IDD_DATA_RESTORE_DLG"), strRawDataFolder); //&&
		AfxMessageBox(strMsg);
		return;	//실제 Data 분석 
	}
	BOOL bIsNotEOL;				// 라인이 존재하는지 확인.
	bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line
	
	FILE *fpSourceFile = NULL, *fpDestFile = NULL;		//cts 파일용
	fpSourceFile = fopen(strCtsFile, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
				//strMsg.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
				strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg7","IDD_DATA_RESTORE_DLG"), srcFileName); //&&
				AfxMessageBox(strMsg);
				return;	//실제 Data 분석 
	}
	
	//cts용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	fpDestFile = fopen(destFileName, "wb");
	if(fpDestFile == NULL)
	{
		//strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg8","IDD_DATA_RESTORE_DLG"), destFileName); //&&
		AfxMessageBox(strMsg);
		return; 
	}
	
	//cts Header 복사 
	PS_TEST_RESULT_FILE_HEADER *pFileHeader = new PS_TEST_RESULT_FILE_HEADER;
	fread(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER),1 , fpSourceFile);
	//pFileHeader->fileHeader.szCreateDateTime = "";
	//sprintf(pFileHeader->testHeader.szStartTime,"%s","2017-04-19 오후 4:53:38");		//ljb 20170511 임시 날자 지정
	if(fwrite(pFileHeader, sizeof(PS_TEST_RESULT_FILE_HEADER), 1, fpDestFile) < 1)
	{
		strMsg.Format("cts Header write error", srcFileName);
		AfxMessageBox(strMsg);
		return;	
	}

	CString strItem;
	int nOldIndex=0;

	PS_STEP_END_RECORD *pRecord = new PS_STEP_END_RECORD;
	while (stepEndFile.ReadString(strReadData))				// 라인이 존재하는지 확인.
	{
		ZeroMemory(pRecord,sizeof(PS_STEP_END_RECORD));

		pRecord->chNo = nChIndex;		//1 base
		pRecord->nIndexFrom = nOldIndex;

		AfxExtractSubString(strItem,strReadData,0,',');		//index
		pRecord->nIndexTo = atol(strItem)-1;

		nOldIndex = atol(strItem) ;
		pRecord->lSaveSequence = atol(strItem);

		AfxExtractSubString(strItem,strReadData,1,',');		//total Time day
		pRecord->fTotalTime_Day = atof(strItem);
		AfxExtractSubString(strItem,strReadData,2,',');		//total Time
		pRecord->fTotalTime = PCTIME(atol(strItem));

		AfxExtractSubString(strItem,strReadData,3,',');		//step Time day
		pRecord->fStepTime_Day = atof(strItem);
		AfxExtractSubString(strItem,strReadData,4,',');		//step Time
		pRecord->fStepTime = PCTIME(atol(strItem));
		
		AfxExtractSubString(strItem,strReadData,5,',');		//chState
		pRecord->chState=atol(strItem);
		
		AfxExtractSubString(strItem,strReadData,6,',');		//type
		pRecord->chStepType = atoi(strItem);

//		AfxExtractSubString(strItem,strReadData,7,',');		//mode
//		AfxExtractSubString(strItem,strReadData,8,',');		//select
		pRecord->chDataSelect = 1;

		AfxExtractSubString(strItem,strReadData,9,',');		//code
		pRecord->chCode = atoi(strItem);

//		AfxExtractSubString(strItem,strReadData,10,',');		//grade code	

		AfxExtractSubString(strItem,strReadData,11,',');		//step No
		pRecord->chStepNo = atoi(strItem);

		AfxExtractSubString(strItem,strReadData,12,',');	//V
		pRecord->fVoltage = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,13,',');	//I
		pRecord->fCurrent = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,14,',');	//charge Ah
		pRecord->fChargeAh = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,15,',');	//DisCharge Ah
		pRecord->fDisChargeAh = LONG2FLOAT(atol(strItem));
		AfxExtractSubString(strItem,strReadData,16,',');	//Capacitance
		pRecord->fCapacitance = atof(strItem);
		AfxExtractSubString(strItem,strReadData,17 ,',');	//watt
		pRecord->fWatt = atof(strItem);
		AfxExtractSubString(strItem,strReadData,18 ,',');	//charge Wh
		pRecord->fChargeWh = atof(strItem);
		AfxExtractSubString(strItem,strReadData,19 ,',');	//discharge Wh
		pRecord->fDisChargeWh = atof(strItem);
		AfxExtractSubString(strItem,strReadData,20 ,',');	//Impedance
		pRecord->fImpedance = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,27 , ',');//lmh add total cycler
		pRecord->nTotalCycleNum = atol(strItem);
		AfxExtractSubString(strItem,strReadData,28 , ',');//lmh add  elementCycle
		pRecord->nCurrentCycleNum = atol(strItem);
		AfxExtractSubString(strItem,strReadData,29 , ',');
		pRecord->nAccCycleGroupNum1 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,30 , ',');
		pRecord->nAccCycleGroupNum2 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,31 , ',');
		pRecord->nAccCycleGroupNum3 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,32 , ',');
		pRecord->nAccCycleGroupNum4 = atol(strItem);
		AfxExtractSubString(strItem,strReadData,33 , ',');
		pRecord->nAccCycleGroupNum5 = atol(strItem);

		
		AfxExtractSubString(strItem,strReadData,39 ,',');
		pRecord->fAvgVoltage = LONG2FLOAT(atol(strItem));//lmh 20120228 fAvgCurrent->fAvgVoltage로 변경
		AfxExtractSubString(strItem,strReadData,40 ,',');
		pRecord->fAvgCurrent = LONG2FLOAT(atol(strItem));

		AfxExtractSubString(strItem,strReadData,41 ,',');	//cv time day
		pRecord->fCVTime_Day = atol(strItem);
		AfxExtractSubString(strItem,strReadData,42 ,',');
		pRecord->fCVTime = PCTIME(atol(strItem));
		AfxExtractSubString(strItem,strReadData,43 ,',');
		pRecord->lSyncDate = atol(strItem);
		AfxExtractSubString(strItem,strReadData,44 ,',');
		pRecord->fSyncTime = atof(strItem);

		if(fwrite(pRecord, sizeof(PS_STEP_END_RECORD), 1, fpDestFile) < 1)
		{
			strMsg.Format("cts body write error", srcFileName);
			AfxMessageBox(strMsg);
			return;	
		}
	}
	
	delete pFileHeader;
	delete pRecord;

	stepEndFile.Close();

	fclose(fpSourceFile);
	fclose(fpDestFile);

	//cts 원본 파일을 삭제한다.
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
	
	sprintf(szFrom, "%s", strCtsFile);
	::DeleteFile(szFrom);				//복구전 파일 삭제
	if(rename(destFileName, strCtsFile) != 0)
	{
		//strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcFileName);
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg9","IDD_DATA_RESTORE_DLG"), srcFileName); //&&
		//LockFileUpdate(bLockFile);
		AfxMessageBox(strMsg);
		return ;
	}
/*************************************** ats 복구 ************************************/
	CString srcAuxFileName;
	srcAuxFileName.Format("%s.ats",strCtsFile.Mid(0, strCtsFile.ReverseFind('.'))); 
	destFileName.Format("%s_backup.ats",strCtsFile.Mid(0, strTemp.ReverseFind('.')));
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 
	
	//모듈에서 전송받은 Aux Step End 파일 Open
	if(!stepEndFile.Open(strRawDataFolder_auxT,CFile::modeRead,&ex))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_auxT);
		strMsg.Format(Fun_FindMsg("RestoreDlg_OnBtnRestoreCtsFile2_msg1","IDD_DATA_RESTORE_DLG"), strRawDataFolder_auxT);//&&
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
		bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line

	CStdioFile stepEndFile2;
	CFileException ex2;
	CString strReadData2;
	BOOL bIsNotEOL2;

	if(!stepEndFile2.Open(strRawDataFolder_auxV,CFile::modeRead,&ex2))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_auxV);
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg10","IDD_DATA_RESTORE_DLG"), strRawDataFolder_auxV); //&&
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
		bIsNotEOL2=stepEndFile2.ReadString(strReadData2);    // 한 LINE 읽는다. -> Title Line

	BOOL bRestore = TRUE;
	fpSourceFile = fopen(srcAuxFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		
		//strMsg.Format("%s 파일 정보를 열수 없습니다.", srcFileName);
		//AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
		bRestore = FALSE;		//복구 안함.
	}
	
	//Ats용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	if (bRestore)
	{
		fpDestFile = fopen(destFileName, "wb");
		if(fpDestFile == NULL)
		{
			//strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
			strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg11","IDD_DATA_RESTORE_DLG"), destFileName); //&&
			AfxMessageBox(strMsg);
			//return; 
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//aux용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_AUX_FILE_HEADER auxFileHeader;
	int nAuxColSize = 0;
	int nAuxT_index=0; 
	int nAuxV_index=0;

	//Aux File Header 복사 
	if (bRestore)
	{
		if(fread(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpSourceFile) > 0)
		{
			if(fwrite(&auxFileHeader, sizeof(PS_AUX_FILE_HEADER), 1, fpDestFile) < 1)
			{
				//strMsg.Format("파일 정보를 열수 없습니다.", srcAuxFileName);
				strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg12","IDD_DATA_RESTORE_DLG"), srcAuxFileName); //&&
				fclose(fpSourceFile);
				fclose(fpDestFile);
				//			return FALSE;	
			}
		}
		nAuxColSize = auxFileHeader.auxHeader.nColumnCount;

		if (nAuxColSize >0)
		{
			float* pfBuff = new float[nAuxColSize];
			while (stepEndFile.ReadString(strReadData))				// 라인이 존재하는지 확인.
			{
				stepEndFile2.ReadString(strReadData2);

				ZeroMemory(pfBuff,sizeof(float)*nAuxColSize);
			
				for (int jj=0; jj<nAuxColSize; jj++)
				{
					if (auxFileHeader.auxHeader.awColumnItem[jj]==PS_AUX_TEMPERATURE) //AuxT
					{
						AfxExtractSubString(strItem,strReadData,nAuxT_index+5,',');		//index
						pfBuff[jj] = atof(strItem);
						nAuxT_index++;
					}
					else if (auxFileHeader.auxHeader.awColumnItem[jj]==PS_AUX_VOLTAGE) //AuxV
					{
						AfxExtractSubString(strItem,strReadData2,nAuxV_index+5,',');		//index
						pfBuff[jj] = atof(strItem);
						nAuxV_index++;
					}

				}
				if(fwrite(pfBuff, sizeof(float)*nAuxColSize, 1, fpDestFile) < 1)
				{
					strMsg.Format("ats body write error", srcFileName);
					AfxMessageBox(strMsg);
					return;	
				}
				nAuxT_index=0;
				nAuxV_index=0;

			}
			
			stepEndFile.Close();
			stepEndFile2.Close();

			fclose(fpSourceFile);
			fclose(fpDestFile);
		
		//cts 원본 파일을 삭제한다.
		//	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
			ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
			ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
			
			sprintf(szFrom, "%s", srcAuxFileName);
			::DeleteFile(szFrom);
			if(rename(destFileName, srcAuxFileName) != 0)
			{
				//strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcAuxFileName);
				strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg13","IDD_DATA_RESTORE_DLG"), srcAuxFileName); //&&
				//LockFileUpdate(bLockFile);
				AfxMessageBox(strMsg);
				return ;
			}
		}
	}
	else
	{
		stepEndFile.Close();
		stepEndFile2.Close();
	}

/*************************************** can 복구 ************************************/
	CString srcCanFileName;
	//   srcCanFileName.Format("%s.ctc",strCtsFile.Mid(0, strTemp.ReverseFind('.'))); 
	srcCanFileName.Format("%s.ctc",strCtsFile.Mid(0, strCtsFile.ReverseFind('.'))); //yulee 20190319
	destFileName.Format("%s_backup.ctc",strCtsFile.Mid(0, strTemp.ReverseFind('.')));
	_unlink(destFileName);		//Backup할 파일명이 있으면 삭제 
	
	int nCanM_ColSize = 0;
	int nCanS_ColSize = 0;

	//모듈에서 전송받은 Step End 파일 Open
	if(!stepEndFile.Open(strRawDataFolder_canM,CFile::modeRead,&ex))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_canM);
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg14","IDD_DATA_RESTORE_DLG"), strRawDataFolder_canM); //&&
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
	{
		bIsNotEOL=stepEndFile.ReadString(strReadData);    // 한 LINE 읽는다. -> Title Line
		if (bIsNotEOL) 
		{
			nCanM_ColSize = str_Count(strReadData,",");
			nCanM_ColSize = nCanM_ColSize -4;
		}
	}

// 	CStdioFile stepEndFile2;
// 	CFileException ex2;
// 	CString strReadData2;
// 	BOOL bIsNotEOL2;
	if(!stepEndFile2.Open(strRawDataFolder_canS,CFile::modeRead,&ex2))
	{
		//strMsg.Format("%s,모듈의 step end 정보를 열수 없습니다.", strRawDataFolder_canS);
		strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg15","IDD_DATA_RESTORE_DLG"), strRawDataFolder_canS); //&&
		AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
	}
	else
	{
		bIsNotEOL2=stepEndFile2.ReadString(strReadData2);    // 한 LINE 읽는다. -> Title Line
		if (bIsNotEOL2)
		{
			nCanS_ColSize = str_Count(strReadData2,",");
			nCanS_ColSize = nCanS_ColSize -4;
		}
	}
	
	bRestore = TRUE;

	fpSourceFile = fopen(srcCanFileName, "rb");
	if(fpSourceFile == NULL)	//PC쪽에 파일이 Open
	{
		
		//strMsg.Format("%s 파일 정보를 열수 없습니다.", srcCanFileName);
		//AfxMessageBox(strMsg);
		//return;	//실제 Data 분석 
		bRestore = FALSE;
	}
	
	//cts용 원본, 임시 파일을 연다.//////////////////////////////////////////////////////////////////
	if (bRestore)
	{
		fpDestFile = fopen(destFileName, "wb");
		if(fpDestFile == NULL)
		{
			//strMsg.Format("복구용 backup파일(%s)를 열수 없습니다.", destFileName);
			strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg16","IDD_DATA_RESTORE_DLG"), destFileName); //&&
			AfxMessageBox(strMsg);
			//return; 
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//ctc용 원본, 임시 파일을 연다///////////////////////////////////////////////////////////////////
	PS_CAN_FILE_HEADER canFileHeader;
	int nCanColSize = 0;
	int nCanM_index=0; 
	int nCanS_index=0;
	
	if (bRestore)
	{
		//CAN File Header 복사 
		if(fread(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpSourceFile) > 0)
		{
			if(fwrite(&canFileHeader, sizeof(PS_CAN_FILE_HEADER), 1, fpDestFile) < 1)
			{
				//strMsg.Format("파일 정보를 열수 없습니다.", destFileName);
				strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg17","IDD_DATA_RESTORE_DLG"), destFileName); //&&
				fclose(fpSourceFile);
				fclose(fpDestFile);
				//			return FALSE;	
			}
		}
		nCanColSize = canFileHeader.canHeader.nColumnCount;

		if (nCanColSize >0)
		{
			SFT_CAN_VALUE * pCanBuff = new SFT_CAN_VALUE[nCanColSize];
			//float* pfBuff = new float[nAuxColSize];
			while (stepEndFile.ReadString(strReadData))				// 라인이 존재하는지 확인.
			{
				stepEndFile2.ReadString(strReadData2);

				ZeroMemory(pCanBuff,sizeof(SFT_CAN_VALUE)*nCanColSize);
			
				for (int jj=0; jj<nCanColSize; jj++)
				{
					if (canFileHeader.canHeader.awColumnItem[jj]==PS_CAN_VALUE) //master
					{
						if (jj < nCanM_ColSize)
						{
							AfxExtractSubString(strItem,strReadData,nCanM_index+5,',');		//index
							pCanBuff[jj].fVal[0] = atof(strItem);
							nCanM_index++;

						}
						else
						{
							AfxExtractSubString(strItem,strReadData2,nCanS_index+5,',');		//index
							pCanBuff[jj].fVal[0] = atof(strItem);
							nCanS_index++;
						}
					}
				}
				if(fwrite(pCanBuff, sizeof(SFT_CAN_VALUE)*nCanColSize, 1, fpDestFile) < 1)
				{
					strMsg.Format("ctc body write error", srcFileName);
					AfxMessageBox(strMsg);
					return;	
				}
				nCanM_index=0;
				nCanS_index=0;

			}
			
			fclose(fpSourceFile);
			fclose(fpDestFile);

			stepEndFile.Close();
			stepEndFile2.Close();

			
			//cts 원본 파일을 삭제한다.
	// 		char szFrom[_MAX_PATH], szTo[_MAX_PATH];
			ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
			ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
			
			sprintf(szFrom, "%s", srcCanFileName);
			::DeleteFile(szFrom);
			if(rename(destFileName, srcCanFileName) != 0)
			{
				//strMsg.Format("%s 파일명을 변경할 수 없습니다.", srcCanFileName);
				strMsg.Format(Fun_FindMsg("OnBtnRestoreCtsFile2_msg18","IDD_DATA_RESTORE_DLG"), srcCanFileName); //&&
				//LockFileUpdate(bLockFile);
				AfxMessageBox(strMsg);
				return ;
			}
		}
	}
	else
	{
		stepEndFile.Close(); 
		stepEndFile2.Close();
	}
	//AfxMessageBox("STEP END 복구 완료");
	AfxMessageBox(Fun_FindMsg("OnBtnRestoreCtsFile2_msg19","IDD_DATA_RESTORE_DLG")); //&&
	return;		
}

void CRestoreDlg::OnRestoreLocalFolderButton3() 
{
	int nModuleID;
	int nChIndex;
	CString strCycFile,destFileName,strTemp, strSBCFolder,strFolder, strMsg;

	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		//AfxMessageBox("모듈을 선택 하세요");
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton3_msg1","IDD_DATA_RESTORE_DLG")); //&&
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);
	
	nIndex = m_ctrlChannel.GetCurSel();
	if(nIndex == CB_ERR)	return;
	if (nIndex < 1) 
	{
		//AfxMessageBox("채널을 선택 하세요");
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton3_msg2","IDD_DATA_RESTORE_DLG")); //&&
		return;
	}
	nChIndex = m_ctrlChannel.GetItemData(nIndex);
	
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	pMD = m_pDoc->GetModuleInfo(nModuleID);
	pCh = pMD->GetChannelInfo(nIndex-1);
	
	if (pMD == NULL || pCh == NULL || pMD->GetState() == PS_STATE_LINE_OFF)
	{
		//AfxMessageBox("모듈 접속 후 하세요.");
		AfxMessageBox(Fun_FindMsg("OnRestoreLocalFolderButton3_msg3","IDD_DATA_RESTORE_DLG")); //&&
		return;
	}
	

// 	CString strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path");
// 	CFileDialog pDlg(TRUE, "cyc", strDataPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "cyc file(*.cyc)|*.cyc|All Files(*.*)|*.*|");
// 	pDlg.m_ofn.lpstrTitle = "결과 데이터 파일 위치";
// 	if(IDOK != pDlg.DoModal())
// 	{
// 		return;
// 	}
// 	strTemp = pDlg.GetPathName();
// 	strCycFile = strTemp;

	strTemp.Format("%s\\%s.cyc", pCh->GetChFilePath(),pCh->GetTestName());
	strCycFile.Format("%s\\%s.cyc", pCh->GetChFilePath(),pCh->GetTestName());

	destFileName.Format("%s_backup.cyc",strCycFile.Mid(0, strTemp.ReverseFind('.')));
	
	//ksj 20160802 선택한 cyc 하위 폴더 restore에서 데이타를 찾음.
	strSBCFolder.Format("%s\\restore",destFileName.Mid(0,destFileName.ReverseFind('\\')));
	

		
	if(pCh->RestoreLostDataFromFolder2(strSBCFolder,strCycFile,nModuleID,nChIndex) == FALSE)
	{
		//strMsg.Format("Folder 손실 data 복구 실패(%s)", pCh->GetLastErrorString());
		strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton3_msg4","IDD_DATA_RESTORE_DLG"), pCh->GetLastErrorString()); //&&
		pCh->WriteLog(strMsg);
		//strMsg.Format("Folder %s Channel %d data 복구 실패(%s)", m_pDoc->GetModuleName(nModuleID), nChIndex, pCh->GetLastErrorString());
		strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton3_msg5","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex, pCh->GetLastErrorString()); //&&
		AfxMessageBox(strMsg);
	}
	else
	{

		//strMsg.Format("Folder %s Channel %d data 복구 성공", m_pDoc->GetModuleName(nModuleID), nChIndex);
		strMsg.Format(Fun_FindMsg("OnRestoreLocalFolderButton3_msg6","IDD_DATA_RESTORE_DLG"), m_pDoc->GetModuleName(nModuleID), nChIndex); //&&
		//pCh->WriteLog("Folder 손실 data 복구 성공");
		pCh->WriteLog(Fun_FindMsg("OnRestoreLocalFolderButton3_msg7","IDD_DATA_RESTORE_DLG")); //&&
		AfxMessageBox(strMsg);
	}

	

	//원본 파일을 삭제한다.
	//2014.10.20 주석 처리. RestoreLostDataFromFolder2 에서 파일명을 전부 변경처리함.
	/*
	char szFrom[_MAX_PATH], szTo[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);		//Double NULL Terminate
	ZeroMemory(szTo, _MAX_PATH);		//Double NULL Terminate	
	
	sprintf(szFrom, "%s", strCycFile);
	::DeleteFile(szFrom);
	if(rename(destFileName, strCycFile) != 0)
	{
		strMsg.Format("%s 파일명을 변경할 수 없습니다.", destFileName);
		//LockFileUpdate(bLockFile);
		AfxMessageBox(strMsg);
		return ;
	}
	*/
	return;
}