// FtpDownLoad.cpp: implementation of the CFtpDownLoad class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "FtpDownLoad.h"
#include "CyclerModule.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFtpDownLoad::CFtpDownLoad()
{
	m_pInternetSession = NULL;
	m_pInternetSession = new CInternetSession(_T("ADPFtpDownLoad/1.0"));
}

CFtpDownLoad::~CFtpDownLoad()
{
	if(m_pFtpConnection)
	{
		m_pFtpConnection->Close();
		delete m_pFtpConnection;
		m_pFtpConnection= NULL;
	}

	if(m_pInternetSession)
	{
		m_pInternetSession->Close();
		delete m_pInternetSession;
		m_pInternetSession = NULL;
	}
}

BOOL CFtpDownLoad::GetLastWriteTimeByName(CString strRemoteFile, CTime &TimeLastWrite) //yulee 20181026_1
{
	ASSERT(m_pFtpConnection);
	CString strDownFile,strDownPath;

	if(strRemoteFile.IsEmpty())	return 0;
	
	strDownPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);	//ljb
	strDownPath.Replace("//","/");
	strDownFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb
	if((strDownFile.IsEmpty() == FALSE)||(strDownFile != _T(""))) //yulee 20190610_1
	{
		TRACE("Remote File Path =  %s\n", strDownPath);
		TRACE("Remote File Name =  %s\n", strDownFile);
	}
	else
	{
		TRACE("strDownFile is empty");
		return FALSE;
	}

	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->SetCurrentDirectory(strDownPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection); //yulee 20181029

	BOOL bOK = pDebugFileFind->FindFile("*");
	//	BOOL bOK = pDebugFileFind->FindFile(strDownFath);
	BOOL bContinue(FALSE);
	
	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("FTP File = %s, FindFile = %s \n",pDebugFileFind->GetFileName(), strDownFile);
		if (pDebugFileFind->GetFileName() == strDownFile)
		{
			bContinue = TRUE;
			break;
		}
		//		TRACE("%s\n", pDebugFileFind->GetFilePath());
	}
	try		//Ftp Connection
	{
#ifdef _DEBUG
		//		BOOL aa = m_pFtpConnection->SetCurrentDirectory("resultData//ch01//");
		//		BOOL aa = m_pFtpConnection->SetCurrentDirectory("formation_data/monitoringData/group1/");
		//		aa = m_pFtpConnection->SetCurrentDirectory("formation_data");
		//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
		//		TRACE("FTP current directory is %s\n", szBuff);
		//		aa = m_pFtpConnection->SetCurrentDirectory("monitoringData");
		//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
		//		TRACE("FTP current directory is %s\n", szBuff);
		//		if(aa == FALSE)
		//		{
		//			return -6;
		//		}
		//
		//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
		//		TRACE("FTP current directory is %s\n", szBuff);
		
#endif		
		CFtpFileFind finder(m_pFtpConnection);
		//		BOOL bContinue = finder.FindFile(strRemoteFile);	
		
		if(bContinue)
		{
			// 			bContinue = finder.FindNextFile();
			// 			if(!finder.IsDirectory())
			// 			{
			//			m_pFtpConnection->SetCurrentDirectory(strDownPath);
			m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
			TRACE("FTP current directory is %s\n", szBuff);
			if(pDebugFileFind->GetLastWriteTime(TimeLastWrite) == FALSE)
			{
				TRACE("%s Get time fail", strRemoteFile);
				finder.Close();

				pDebugFileFind->Close();
				
				delete pDebugFileFind;
				pDebugFileFind = NULL;
				return 0;	//실패 
			}
			finder.Close();
			// 			}
			// 			else	//direcotry
			// 			{
			// 				TRACE(" %s is directory\n", strRemoteFile);
			// 				finder.Close();
			// 				return -3;	//
			// 			}
		}
		else	// file not found
		{
			TRACE("File not found\n");
			finder.Close();

			pDebugFileFind->Close();
			
			delete pDebugFileFind;
			pDebugFileFind = NULL;
			return 0;	//실패 
		}
	}
	catch(CInternetException *pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz, 1024);
		pEx->Delete();
		
		//AfxMessageBox("FTP Connection Error");
		
		//AfxMessageBox(sz);
		pDebugFileFind->Close();
		
		delete pDebugFileFind;
		pDebugFileFind = NULL;
		return 0;	//실패 
	}

	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;


 	CString tmpStr;
 	tmpStr = TimeLastWrite.Format(_T("%Y-%m-%d %H:%M:%S"));
	return 1;	//성공 			
}

//DownLoad file form module
int CFtpDownLoad::DownLoadFile(CString strLoacalFile, CString strRemoteFile, BOOL bOverWrite)
{

	CWaitCursor cursor;	

	//ljb7
	if(strLoacalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	CString strDownFile,strDownPath;

	strDownPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);	//ljb
	strDownPath.Replace("//","/");
	strDownFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb
	if((strDownFile.IsEmpty() == FALSE)||(strDownFile != _T(""))) //yulee 20190610_1
	{
		TRACE("Remote File Path =  %s\n", strDownPath);
		TRACE("Remote File Name =  %s\n", strDownFile);
	}
	else
	{
		TRACE("strDownFile is empty");
		return 0;
	}

#ifdef _DEBUG
#endif
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->SetCurrentDirectory(strDownPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);
	
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
//	BOOL bOK = pDebugFileFind->FindFile(strDownFath);
	BOOL bContinue(FALSE);

	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("FTP File = %s, FindFile = %s \n",pDebugFileFind->GetFileName(), strDownFile);
		if (pDebugFileFind->GetFileName() == strDownFile)
		{
			bContinue = TRUE;
			break;
		}
//		TRACE("%s\n", pDebugFileFind->GetFilePath());
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;


	try		//Ftp Connection
	{
#ifdef _DEBUG
//		BOOL aa = m_pFtpConnection->SetCurrentDirectory("resultData//ch01//");
//		BOOL aa = m_pFtpConnection->SetCurrentDirectory("formation_data/monitoringData/group1/");
//		aa = m_pFtpConnection->SetCurrentDirectory("formation_data");
//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
//		TRACE("FTP current directory is %s\n", szBuff);
//		aa = m_pFtpConnection->SetCurrentDirectory("monitoringData");
//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
//		TRACE("FTP current directory is %s\n", szBuff);
//		if(aa == FALSE)
//		{
//			return -6;
//		}
//
//		m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
//		TRACE("FTP current directory is %s\n", szBuff);

#endif		
		CFtpFileFind finder(m_pFtpConnection);
//		BOOL bContinue = finder.FindFile(strRemoteFile);	

		if(bContinue)
		{
// 			bContinue = finder.FindNextFile();
// 			if(!finder.IsDirectory())
// 			{
//			m_pFtpConnection->SetCurrentDirectory(strDownPath);
			m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
			TRACE("FTP current directory is %s\n", szBuff);
				if(m_pFtpConnection->GetFile(strDownFile , strLoacalFile, !bOverWrite) == FALSE)
				{
					TRACE("%s down load fail to %s\n", strRemoteFile, strLoacalFile);
					finder.Close();
					return -2;	//실패 
				}
				finder.Close();
// 			}
// 			else	//direcotry
// 			{
// 				TRACE(" %s is directory\n", strRemoteFile);
// 				finder.Close();
// 				return -3;	//
// 			}
		}
		else	// file not found
		{
			TRACE("File not found\n");
			finder.Close();
			return -4;	//실패 
		}
	}
	catch(CInternetException *pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz, 1024);
	    pEx->Delete();

		//AfxMessageBox("FTP Connection Error");

		//AfxMessageBox(sz);
		return -5;	//실패 
	}
	return 1;	//성공 		
}

//DownLoad file form module
int CFtpDownLoad::DownLoadFile2(CString strLocalFile, CString strRemoteFile, BOOL bOverWrite)
{
	CWaitCursor cursor;	
	
	if(strLocalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	CString strDownFile, strDownPath, strSaveFileName, strNewDownFile;
	
	strDownPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);
	strDownPath.Replace("//","/");
	strDownFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);
	if((strDownFile.IsEmpty() == FALSE)||(strDownFile != _T(""))) //yulee 20190610_1
	{
		TRACE("Remote File Path =  %s\n", strDownPath);
		TRACE("Remote File Name =  %s\n", strDownFile);
	}
	else
	{
		TRACE("strDownFile is empty");
		return 0;
	}
	
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	
	m_pFtpConnection->SetCurrentDirectory("/root");
	m_pFtpConnection->SetCurrentDirectory(strDownPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("%s\n", pDebugFileFind->GetFilePath());
		
		if(pDebugFileFind->GetFileName() == strDownFile)
		{
			if(m_pFtpConnection->GetFile(strDownFile , strLocalFile, !bOverWrite) == FALSE)
			{
				TRACE("%s down load fail to %s\n", strRemoteFile, strLocalFile);
			}
			break;
		}
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;
	
	return 1;	//성공 		
}

//DownLoad All file form module
int CFtpDownLoad::DownLoadAllFile(CString strLoacalFile, CString strRemoteFile ,UINT nModuleID, UINT nChIndex, BOOL bOverWrite)
{
	CWaitCursor cursor;	
	int nDownCnt=0;
	//ljb7
	if(strLoacalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	CString strDownFile,strDownPcFile,strDownPath;//yulee 20190705 강제병렬 옵션 처리 Mark

	strDownPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);	//ljb
	strDownPath.Replace("//","/");
/*	strDownFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb*/
// 	if((strDownFile.IsEmpty() == FALSE)||(strDownFile != _T(""))) //yulee 20190610_1
// 	{
// 		TRACE("Remote File Path =  %s\n", strDownPath);
// 		TRACE("Remote File Name =  %s\n", strDownFile);
// 	}
// 	else
// 	{
// 		TRACE("strDownFile is empty");
// 		return 0;
// 	}

#ifdef _DEBUG
#endif
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->SetCurrentDirectory(strDownPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);
	
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
//	BOOL bOK = pDebugFileFind->FindFile(strDownFath);
	BOOL bContinue(FALSE);

	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("FTP File = %s, FindFile = %s \n",pDebugFileFind->GetFileName(), strDownFile);

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			//yulee 20181213 병렬 mark
			strDownPcFile = strDownFile = pDebugFileFind->GetFileName();
			if (strDownPcFile.Left(5) == "ch003")
			{
				strDownPcFile.Replace("ch003","ch002");	//ljb 20171215 채널이름 변환해서 down
			}
		
			strDownPath.Format("%s\\%s",strLoacalFile,strDownPcFile);
		}
//#else
else
		{
			strDownFile = pDebugFileFind->GetFileName();
			strDownPath.Format("%s\\%s",strLoacalFile,strDownFile);
		}
//#endif

		if(m_pFtpConnection->GetFile(strDownFile , strDownPath, !bOverWrite) == FALSE)
		{
			TRACE("%s down load fail to %s\n", strRemoteFile, strLoacalFile);
			//finder.Close();
			//return -2;	//실패 
		}
		else
			nDownCnt++;
		
		if (bOK == FALSE) break;

// 		if (pDebugFileFind->GetFileName() == strDownFile)
// 		{
// 			bContinue = TRUE;
// 			break;
// 		}
//		TRACE("%s\n", pDebugFileFind->GetFilePath());
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;

	return nDownCnt;	//성공 		

}
//DownLoad All file form module
 int CFtpDownLoad::DownLoadAllFile3_ProgressBar(CString strLocalFile, CString strRemoteFile ,UINT nModuleID, UINT nChIndex, int nSBCFileNoOrg, int nSBCFileNoNew, int nTotNum, int nDwnCnt, BOOL bOverWrite)
 { 
	//////////////////////////////////////////////////////////////////////////
	CProgressWnd progressWnd(AfxGetMainWnd(), "이전 공정 SBC Data Download...", TRUE);
   	progressWnd.GoModal();
	progressWnd.SetRange(0, 100);
	progressWnd.CenterWindow(); //ksj 20200922 : 가운데로 이동 시키기 추가
	CString strLogMsg;
	//////////////////////////////////////////////////////////////////////////
	CWaitCursor cursor;	
	int nDownCnt=0;
	int nCntNum=0;
	//ljb7
	if(strLocalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	CString strDownFile,strDownPath,strSaveFileName, strNewDownFile;
	int nFileNo = 1;
	int nFileNoMinus = nSBCFileNoNew - nSBCFileNoOrg;//3-26
	
	strDownPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);	//ljb
	strDownPath.Replace("//","/");
	strDownFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb
	TRACE("Remote File Path =  %s\n", strDownPath);
	TRACE("Remote File Name =  %s\n", strDownFile);

	#ifdef _DEBUG
	#endif

	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	
	m_pFtpConnection->SetCurrentDirectory("/root");
	m_pFtpConnection->SetCurrentDirectory(strDownPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);
	
	ULONGLONG  m_filesize_Remote = 0;        
	BOOL	   m_bLastWriteTime(FALSE);
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
	BOOL bContinue(FALSE);
	CTime tCreateTime;
	//////////////////////////////////////////////////////////////////////////
	progressWnd.SetPos(1);
	//strLogMsg.Format("Download 준비 중...");
	strLogMsg.Format("이전 시험의 데이터 백업 준비 중입니다. 잠시기다려주세요."); //ksj 20200922
	progressWnd.SetText(strLogMsg);
	//////////////////////////////////////////////////////////////////////////
	__int64   nFileSize_Local;
	int fp;
	struct _stati64 stStat;

	UINT nIndex;
	//CFileFind aFileFinder;
	CString strTemp;

	strTemp.Format(_T("down bOK:%d local:%s remote:%s M%02dCH%02d tot:%d overwrite:%d \
nFileNoMinus:%d nSBCFileNoNew:%d nSBCFileNoOrg:%d"),
	bOK,strLocalFile,strRemoteFile,
	nModuleID,nChIndex,nTotNum,bOverWrite,
	nFileNoMinus,nSBCFileNoNew,nSBCFileNoOrg);

	GLog::log_Save(_T("down"),_T("sbc"),strTemp,NULL,TRUE);

	while(bOK)
	{
		nIndex = 0;
		nFileSize_Local = 0;
		bOK = pDebugFileFind->FindNextFile();
		strDownFile = pDebugFileFind->GetFileName();

 		if(g_AppInfo.iPType==1 ) //lyj 20200518
 		{
			if(nChIndex >1)
 			{
				if(strDownFile.Left( 5 )== "ch003")
			    {
				 strDownPath = strDownFile;
				 strDownPath.Delete( 0, 5 );
				 strDownPath.Insert( 0, "\\ch002" );
				 strDownPath.Insert(0, strLocalFile);
			    }
			}
			else
			{
				strDownPath.Format("%s\\%s", strLocalFile, strDownFile);
			}
 		}
		else
		{
			strDownPath.Format("%s\\%s", strLocalFile, strDownFile);
		}

		strSaveFileName = strDownFile.Left(strDownFile.Find('.'));

		//SBC 파일명 규칙이 두자리든 네자리든 읽어올 수 있도록 수정
		nFileNo =  atoi(strSaveFileName.Mid(strSaveFileName.ReverseFind('a')+1));

		TRACE("FTP File = %s, DownFolder = %s , FileName = %s \n",pDebugFileFind->GetFileName(), strLocalFile, strSaveFileName);
		
		m_filesize_Remote = pDebugFileFind->GetLength();
		//pDebugFileFind->GetCreationTime(tLastWrite);						//일반적인 FTP서버에서는  CreationTime은 전달이 안됨니다.	
		m_bLastWriteTime = pDebugFileFind->GetLastWriteTime(tCreateTime);	//오직 LastWriteTime만 넘어 옵니다.
		
		//strDownPath.Format("%s\\%s", strLocalFile, strDownFile); //lyj 20200518
			
		
		fp = _open (strDownPath, _O_RDONLY | _O_BINARY);
		if (fp > 0) 
		{
			_fstati64 (fp, &stStat);
			nFileSize_Local = stStat.st_size;
			_close(fp);
		}

		
		if(m_filesize_Remote != (ULONGLONG)nFileSize_Local )
		{
			if (strDownFile.Find("Index") > 0 || strDownFile.Find("EndData") > 0  )
			{
				if(m_pFtpConnection->GetFile(strDownFile , strDownPath, !bOverWrite) == FALSE)
				{
					TRACE("%s down load fail to %s\n", strRemoteFile, strLocalFile);
				}
				else
				{
					nDownCnt++;
				}
			}
			else if(strDownFile.Find("SaveData") > 0)
			{
				//ksj 20200121 : 주석처리
				/*//파일이 8개 이상일 때만 파일명을 변경한다.
				if( nFileNoMinus != 0 &&
					strDownFile.Find("_auxT") < 0 &&
					strDownFile.Find("_auxV") < 0 &&
					strDownFile.Find("_canMaster") < 0 &&
					strDownFile.Find("_canSlave") < 0 )
				{
					nFileNo = nFileNoMinus + nFileNo; 
					if(nFileNo > nSBCFileNoNew) 
					{
						nFileNo = nFileNo - 7;
					}
					
					strNewDownFile.Format("ch%03d_SaveData%04d.csv", nChIndex, nFileNo);
					strDownPath.Format("%s\\%s", strLocalFile, strNewDownFile);
				}*/
				
				if(m_pFtpConnection->GetFile(strDownFile , strDownPath, !bOverWrite) == FALSE)
				{
					TRACE("%s down load fail to %s\n", strRemoteFile, strLocalFile);
				}
				else
				{
					nDownCnt++;
				}
				//////////////////////////////////////////////////////////////////////////
				if((nCntNum > 0) && (nTotNum > 0))
				{
					double DblTmpProValue;
					DblTmpProValue = (((double)nCntNum/(double)nTotNum)*100.0);
					progressWnd.SetPos((int)(DblTmpProValue));
					strLogMsg.Format("Download 중...%d개 중 %d개 처리", nTotNum, nCntNum);
				}
				progressWnd.SetText(strLogMsg);
				//////////////////////////////////////////////////////////////////////////
			}
		}
		nCntNum++;	

		strTemp.Format(_T("down_%d/%d bOK:%d ftp:%s(%I64u) local:%s(%I64d)"),
	                   nCntNum,nTotNum,bOK,
		               strDownFile,m_filesize_Remote,
					   strDownPath,nFileSize_Local);
		GLog::log_Save(_T("down"),_T("sbc"),strTemp,NULL,TRUE);

		if (bOK == FALSE) break;
	}
	
	pDebugFileFind->Close();
		
	delete pDebugFileFind;
	pDebugFileFind = NULL;
	//////////////////////////////////////////////////////////////////////////
	progressWnd.SetPos(100);
	strLogMsg.Format("Download 완료");
	progressWnd.SetText(strLogMsg);
	//////////////////////////////////////////////////////////////////////////
	return nCntNum;	//성공 				
 }
 //DownLoad All file form module
 int CFtpDownLoad::GetFileTotNum(CString strLocalFile, CString strRemoteFile ,UINT nModuleID, UINT nChIndex, BOOL bOverWrite)
 {
	 CWaitCursor cursor;	
	 int nFileCnt=0;
	 //ljb7
	 if(strLocalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	 
	 //first call ConnectFtpSession()
	 ASSERT(m_pFtpConnection);
	 CString strDownFile,strDownPath;
	 
	 strDownPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);	//ljb
	 strDownPath.Replace("//","/");
	 strDownFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb
	 TRACE("Remote File Path =  %s\n", strDownPath);
	 TRACE("Remote File Name =  %s\n", strDownFile);
	 
	 #ifdef _DEBUG
 	#endif

	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->SetCurrentDirectory(strDownPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);
	
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
	BOOL bContinue(FALSE);

	CString strTemp;

	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("FTP File = %s, FindFile = %s \n",pDebugFileFind->GetFileName(), strDownFile);

		nFileCnt++;

		strTemp.Format(_T("down_%d/ bOK:%d ftp:%s %s"),
						nFileCnt,bOK,
						pDebugFileFind->GetFileName(),
						strDownFile);
		GLog::log_Save(_T("down"),_T("sbc"),strTemp,NULL,TRUE);

		if (bOK == FALSE) break;
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;

	return nFileCnt;	//성공 		

}

//DownLoad All file form module
int CFtpDownLoad::DownLoadAllFile2(CString strLocalFile, CString strRemoteFile ,UINT nModuleID, UINT nChIndex, int nSBCFileNoOrg, int nSBCFileNoNew, BOOL bOverWrite)
{
	CWaitCursor cursor;	
	int nDownCnt=0;
	//ljb7
	if(strLocalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	CString strDownFile,strDownPath,strSaveFileName, strNewDownFile;
	int nFileNo = 1;
	int nFileNoMinus = nSBCFileNoNew - nSBCFileNoOrg;
	
	strDownPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);	//ljb
	strDownPath.Replace("//","/");
	strDownFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb
	if((strDownFile.IsEmpty() == FALSE)||(strDownFile != _T(""))) //yulee 20190610_1
	{
		TRACE("Remote File Path =  %s\n", strDownPath);
		TRACE("Remote File Name =  %s\n", strDownFile);
	}
	else
	{
		TRACE("strDownFile is empty");
		return 0;
	}

	
#ifdef _DEBUG
#endif
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	
	m_pFtpConnection->SetCurrentDirectory("/root");
	m_pFtpConnection->SetCurrentDirectory(strDownPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);
	
	ULONGLONG  m_filesize_Remote = 0;        
	BOOL	   m_bLastWriteTime(FALSE);
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
	BOOL bContinue(FALSE);
	CTime tCreateTime;

	__int64   nFileSize_Local;
	int fp;
	struct _stati64 stStat;

	UINT nIndex;
	//CFileFind aFileFinder;

	while(bOK)
	{
		nIndex = 0;
		nFileSize_Local = 0;
		bOK = pDebugFileFind->FindNextFile();
		strDownFile = pDebugFileFind->GetFileName();
		strSaveFileName = strDownFile.Left(strDownFile.Find('.'));

		//SBC 파일명 규칙이 두자리든 네자리든 읽어올 수 있도록 수정
		nFileNo =  atoi(strSaveFileName.Mid(strSaveFileName.ReverseFind('a')+1));

		TRACE("FTP File = %s, DownFolder = %s , FileName = %s \n",pDebugFileFind->GetFileName(), strLocalFile, strSaveFileName);
		
		m_filesize_Remote = pDebugFileFind->GetLength();
		//pDebugFileFind->GetCreationTime(tLastWrite);						//일반적인 FTP서버에서는  CreationTime은 전달이 안됨니다.	
		m_bLastWriteTime = pDebugFileFind->GetLastWriteTime(tCreateTime);	//오직 LastWriteTime만 넘어 옵니다.
		
		strDownPath.Format("%s\\%s", strLocalFile, strDownFile);
		
		fp = _open (strDownPath, _O_RDONLY | _O_BINARY);
		if (fp > 0) 
		{
			_fstati64 (fp, &stStat);
			nFileSize_Local = stStat.st_size;
			_close(fp);
		}
		

 		if (nFileSize_Local != m_filesize_Remote)
 		{
			if (strDownFile.Find("Index") > 0 || strDownFile.Find("EndData") > 0  )
			{
				if(m_pFtpConnection->GetFile(strDownFile , strDownPath, !bOverWrite) == FALSE)
				{
					TRACE("%s down load fail to %s\n", strRemoteFile, strLocalFile);
				}
				else
					nDownCnt++;
			}
			else if(strDownFile.Find("SaveData") > 0)
			{
				//파일이 8개 이상일 때만 파일명을 변경한다.
				if(nFileNoMinus != 0)
				{
					nFileNo = nFileNoMinus + nFileNo;
					if(nFileNo > nSBCFileNoNew)
					{
						nFileNo = nFileNo - 7;
					}

					strNewDownFile.Format("ch%02d_SaveData%04d.csv", nChIndex, nFileNo);
					strDownPath.Format("%s\\%s", strLocalFile, strNewDownFile);
				}
				
				if(m_pFtpConnection->GetFile(strDownFile , strDownPath, !bOverWrite) == FALSE)
				{
					TRACE("%s down load fail to %s\n", strRemoteFile, strLocalFile);
				}
				else
					nDownCnt++;
				
			}
		}
	
		if (bOK == FALSE) break;
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;
	
	return nDownCnt;	//성공 		
}
//connect ftp to module
BOOL CFtpDownLoad::ConnectFtpSession(CString strIPAddress, CString strID, CString strPWD)
{
	// TODO: Add your control notification handler code here

	//IP Address Check
	if(strIPAddress.IsEmpty())
	{
		//AfxMessageBox("접속 주소를 찾을 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("FtpDownLoad_ConnectFtpSession_msg1","FTPDOWNLOAD"));//&&
		return FALSE;
	}

//	CString strID, strPwd;
//	strID = AfxGetApp()->GetProfileString(FTP_REG_CONFIG, "Login ID", "root");				//root 접근 권한으로 접속
//	strPwd = AfxGetApp()->GetProfileString(FTP_REG_CONFIG, "Login Password", "dusrnth");	//"연구소"

	ASSERT(m_pInternetSession);

	CWaitCursor cursor;	
	
	try		//Ftp Connection
	{
		m_pFtpConnection = m_pInternetSession->GetFtpConnection(strIPAddress, strID, strPWD);
    }
	catch(CInternetException *pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz, 1024);
	    pEx->Delete();
		m_pFtpConnection = NULL;
		return FALSE;
	}
	return TRUE;
}

CString CFtpDownLoad::GetCurrentDirecotry()
{
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	CString strFolder(szBuff);
	return strFolder;

}

BOOL CFtpDownLoad::SetCurrentDirecotry(CString strCurDir)
{
	ASSERT(m_pFtpConnection);
	return m_pFtpConnection->SetCurrentDirectory(strCurDir);
}

//Upload file form module //yulee 20181120
int CFtpDownLoad::UploadFile(CString strLoacalFile, CString strRemoteFile, BOOL bOverWrite)
{

	CWaitCursor cursor;	

	//ljb7
	if(strLoacalFile.IsEmpty() || strRemoteFile.IsEmpty())	return -1;
	
	//first call ConnectFtpSession()
	ASSERT(m_pFtpConnection);
	CString strUploadFile,strUploadPath, strDestFile,strDestPath;

	strUploadPath = strLoacalFile.Mid(0,strLoacalFile.ReverseFind('\\'));	//ljb
	strUploadPath.Replace("//","/");
	strUploadFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb
	if((strUploadFile.IsEmpty() == FALSE)||(strUploadFile != _T(""))) //yulee 20190610_1
	{
		TRACE("Upload File Path =  %s\n", strUploadPath);
		TRACE("Upload File Name =  %s\n", strUploadFile);
	}
	else
	{
		TRACE("strUploadFile is empty");
		return 0;
	}

	strDestPath = strRemoteFile.Mid(0,strRemoteFile.ReverseFind('/')-1);	//ljb
	strDestPath.Replace("//","/");
	strDestFile = strRemoteFile.Mid(strRemoteFile.ReverseFind('/')+1);	//ljb
	if((strDestFile.IsEmpty() == FALSE)||(strDestFile != _T(""))) //yulee 20190610_1
	{
		TRACE("Upload File Path =  %s\n", strDestPath);
		TRACE("Upload File Name =  %s\n", strDestFile);
	}
	else
	{
		TRACE("strDestFile is empty");
		return 0;
	}

#ifdef _DEBUG
#endif
	char szBuff[_MAX_PATH];
	DWORD nLength = _MAX_PATH;
	m_pFtpConnection->SetCurrentDirectory(strDestPath);
	m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
	TRACE("FTP current directory is %s\n", szBuff);
	
	CFtpFileFind *pDebugFileFind = new CFtpFileFind(m_pFtpConnection);
	BOOL bOK = pDebugFileFind->FindFile("*");
//	BOOL bOK = pDebugFileFind->FindFile(strDownFath);
	BOOL bSameFile(FALSE);

	while(bOK)
	{
		bOK = pDebugFileFind->FindNextFile();
		TRACE("FTP File = %s, FindFile = %s \n",pDebugFileFind->GetFileName(), strDestFile);
 		if (pDebugFileFind->GetFileName() == strUploadFile)
 		{
 			bSameFile = TRUE;
 			break;
 		}
		TRACE("%s\n", pDebugFileFind->GetFilePath());
	}
	pDebugFileFind->Close();
	
	delete pDebugFileFind;
	pDebugFileFind = NULL;

	try		//Ftp Connection
	{
		CFtpFileFind finder(m_pFtpConnection);
		BOOL bContinue = finder.FindFile(strRemoteFile);	

//		if(bContinue)
//		{
			m_pFtpConnection->GetCurrentDirectory(szBuff, &nLength);
			TRACE("FTP current directory is %s\n", szBuff);
			if(m_pFtpConnection->PutFile(strLoacalFile, strRemoteFile, !bOverWrite) == FALSE)
			{
				TRACE("%s down load fail to %s\n", strRemoteFile, strLoacalFile);
					finder.Close();
					return -2;	//실패 
				}
				finder.Close();
// 		}
// 		else	// file not found
// 		{
// 			TRACE("File not found\n");
// 			finder.Close();
// 			return -4;	//실패 
// 		}
	}
	catch(CInternetException *pEx)
	{
		TCHAR sz[1024];
		pEx->GetErrorMessage(sz, 1024);
	    pEx->Delete();

		//AfxMessageBox("FTP Connection Error");

		//AfxMessageBox(sz);
		return -5;	//실패 
	}
	return 1;	//성공 		
}