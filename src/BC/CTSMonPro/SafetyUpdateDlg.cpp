// SafetyUpdateDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "CTSMonProView.h"
#include "SafetyUpdateDlg.h"
#include "CTSMonProDoc.h"
#include "SchUpdateDlg.h"
#include "WorkWarnin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSafetyUpdateDlg dialog

class CCTSMonProDoc;

CSafetyUpdateDlg::CSafetyUpdateDlg(int nModuleID,int nChIndex, CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSafetyUpdateDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSafetyUpdateDlg::IDD2):	
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSafetyUpdateDlg::IDD3):	
 	(CSafetyUpdateDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSafetyUpdateDlg)
	m_edit_capa_max = _T("0");
	m_edit_cell_dev_max = _T("0");
	m_edit_current_max = _T("0");
	m_edit_power_max = _T("0");
	m_edit_volt_max = _T("0");
	m_edit_volt_min = _T("0");
	m_edit_wh_max = _T("0");
	m_edit_delta_std_min_v = _T("0"); //lyj 20200214
	m_edit_delta_std_max_v = _T("0"); //lyj 20200214
	m_edit_delta_min_v = _T("0"); //lyj 20200214
	m_edit_delta_v = _T("0"); //lyj 20200214
	m_edit_delta_max_v = _T("0"); //lyj 20200214

	//}}AFX_DATA_INIT

	ASSERT(pDoc);
	m_nModuleID = nModuleID;
	m_nChIndex = nChIndex;
	m_pDoc = pDoc;

	m_pCH = pDoc->GetChannelInfo(nModuleID, nChIndex);
}


void CSafetyUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSafetyUpdateDlg)
	DDX_Text(pDX, IDC_EDIT_CAPA_MAX, m_edit_capa_max);
	DDX_Text(pDX, IDC_EDIT_CELL_DEV_MAX, m_edit_cell_dev_max);
	DDX_Text(pDX, IDC_EDIT_CURRENT_MAX, m_edit_current_max);
	DDX_Text(pDX, IDC_EDIT_POWER_MAX, m_edit_power_max);
	DDX_Text(pDX, IDC_EDIT_VOLT_MAX, m_edit_volt_max);
	DDX_Text(pDX, IDC_EDIT_VOLT_MIN, m_edit_volt_min);
	DDX_Text(pDX, IDC_EDIT_WH_MAX, m_edit_wh_max);
	DDX_Text(pDX, IDC_EDIT_SAFETY_CELL_DELTA_STD_MIN_V, m_edit_delta_std_min_v);
	DDX_Text(pDX, IDC_EDIT_SAFETY_CELL_DELTA_STD_MAX_V, m_edit_delta_std_max_v);
	DDX_Text(pDX, IDC_EDIT_SAFETY_CELL_DELTA_MIN_V, m_edit_delta_min_v);
	DDX_Text(pDX, IDC_EDIT_IDC_SAFETY_CELL_DELTA_V, m_edit_delta_v);
	DDX_Text(pDX, IDC_EDIT_SAFETY_CELL_DELTA_MAX_V, m_edit_delta_max_v);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSafetyUpdateDlg, CDialog)
	//{{AFX_MSG_MAP(CSafetyUpdateDlg)
	ON_BN_CLICKED(IDC_BUT_SAFETY_REQUEST, OnButSafetyRequest)
	ON_WM_SHOWWINDOW()
	ON_EN_CHANGE(IDC_EDIT_VOLT_MIN, OnChangeEditVoltMin)
	ON_EN_CHANGE(IDC_EDIT_VOLT_MAX, OnChangeEditVoltMax)
	ON_EN_CHANGE(IDC_EDIT_POWER_MAX, OnChangeEditPowerMax)
	ON_EN_CHANGE(IDC_EDIT_CAPA_MAX, OnChangeEditCapaMax)
	ON_EN_CHANGE(IDC_EDIT_CURRENT_MAX, OnChangeEditCurrentMax)
	ON_EN_CHANGE(IDC_EDIT_WH_MAX, OnChangeEditWhMax)
	ON_EN_CHANGE(IDC_EDIT_CELL_DEV_MAX, OnChangeEditCellDevMax)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_EDIT_SAFETY_CELL_DELTA_MIN_V, &CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaMinV)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_CELL_DELTA_STD_MIN_V, &CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaStdMinV)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_CELL_DELTA_STD_MAX_V, &CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaStdMaxV)
	ON_EN_CHANGE(IDC_EDIT_IDC_SAFETY_CELL_DELTA_V, &CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaV)
	ON_EN_CHANGE(IDC_EDIT_SAFETY_CELL_DELTA_MAX_V, &CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaMaxV)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSafetyUpdateDlg message handlers

BOOL CSafetyUpdateDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	ZeroMemory(&m_sSafetyInfoData,sizeof(SFT_CMD_SAFETY_UPDATE_REPLY));

	InitDataUnit();

	//ksj 20170809 : 데이터 단위, 라벨 초기화	
	CString strUnit;
	char szBuff[64];
	char szDecimalPoint[32], szUnit[32];
	CString strTemp1, strTemp2;
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_VUNIT1)->SetWindowText(szUnit);
	GetDlgItem(IDC_STATIC_VUNIT2)->SetWindowText(szUnit);
//	GetDlgItem(IDC_STATIC_VUNIT3)->SetWindowText(szUnit);

	m_nVDec = atoi(szDecimalPoint);
		
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "A 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_IUNIT)->SetWindowText(szUnit);
	m_nIDec = atoi(szDecimalPoint);
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "C Unit", "Ah 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_CUNIT)->SetWindowText(szUnit);
	m_nCDec = atoi(szDecimalPoint);
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "W Unit", "W 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
//	GetDlgItem(IDC_STATIC_WUNIT)->SetWindowText(szUnit);
	m_nWDec = atoi(szDecimalPoint);	
	
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Wh Unit", "Wh 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_WHUNIT)->SetWindowText(szUnit);
	m_nWhDec = atoi(szDecimalPoint);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSafetyUpdateDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);


	CString strMsg;
	CWorkWarnin WorkWarningDlg;
	//strMsg = "안전조건 변경 실행 하시겠습니까?";
	strMsg = Fun_FindMsg("SafetyUpdateDlg_OnOK_msg1","IDD_SAFETY_UPDATE_DLG");//&&
	WorkWarningDlg.Fun_SetMessage(strMsg);
	if (WorkWarningDlg.DoModal() == IDCANCEL)
	{
		ShowWindow(SW_HIDE); //창이 뒤에 숨어 있으므로 아예 숨겼다가 다시 보여주기.
		ShowWindow(SW_SHOW);
		return;
	}

/*	m_sSafetyInfoData.sTestSafetySet.lVtgLow = m_edit_volt_min;
	m_sSafetyInfoData.sTestSafetySet.lVtgHigh =m_edit_volt_max;
	m_sSafetyInfoData.sTestSafetySet.lVtgCellDelta= m_edit_cell_dev_max;
	m_sSafetyInfoData.sTestSafetySet.lCapHigh = m_edit_capa_max;
	m_sSafetyInfoData.sTestSafetySet.lWattHigh = m_edit_power_max;
	m_sSafetyInfoData.sTestSafetySet.lWattHourHigh = m_edit_wh_max;*/

	
	SFT_CMD_SAFETY_UPDATE_REPLY sOrgSafetyInfoData;
	memcpy(&sOrgSafetyInfoData, &m_sSafetyInfoData, sizeof(SFT_CMD_SAFETY_UPDATE_REPLY));

	m_sSafetyInfoData.sTestSafetySet.lVtgLow = (long)(atof(m_edit_volt_min) / m_fVUnit);
	m_sSafetyInfoData.sTestSafetySet.lVtgHigh = (long)(atof(m_edit_volt_max)/ m_fVUnit);
	//m_sSafetyInfoData.sTestSafetySet.lVtgCellDelta = (long)(atof(m_edit_cell_dev_max) / m_fVUnit); //lyj 20200214
	m_sSafetyInfoData.sTestSafetySet.lCrtHigh = (long)(atof(m_edit_current_max) / m_fIUnit);	
	m_sSafetyInfoData.sTestSafetySet.lCapHigh = (long)(atof(m_edit_capa_max) / m_fCUnit);
	m_sSafetyInfoData.sTestSafetySet.lWattHigh = (long)(atof(m_edit_power_max) / m_fWUnit);
	m_sSafetyInfoData.sTestSafetySet.lWattHourHigh = (long)(atof(m_edit_wh_max) / m_fWhUnit);
	m_sSafetyInfoData.sTestSafetySet.faultCompAuxV = (long)(atof(m_edit_delta_min_v) / m_fVUnit); //lyj 20200214
	m_sSafetyInfoData.sTestSafetySet.faultCompAuxV2 = (long)(atof(m_edit_delta_v) / m_fVUnit); //lyj 20200214
	m_sSafetyInfoData.sTestSafetySet.faultCompAuxV3 = (long)(atof(m_edit_delta_max_v) / m_fVUnit);	//lyj 20200214
	m_sSafetyInfoData.sTestSafetySet.faultCompAux_Refv1 = (long)(atof(m_edit_delta_std_min_v) / m_fVUnit); //lyj 20200214
	m_sSafetyInfoData.sTestSafetySet.faultCompAux_Refv2 = (long)(atof(m_edit_delta_std_max_v) / m_fVUnit);	 //lyj 20200214

	//Fun_SendSafetyCmd();

	if(!Fun_SendSafetyCmd())
	{
		ShowWindow(SW_HIDE); //창이 뒤에 숨어 있으므로 아예 숨겼다가 다시 보여주기.
		ShowWindow(SW_SHOW);

		//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("시험 안전조건 Update 실패.")); //ksj 20170823
		GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SafetyUpdateDlg_OnOK_msg2","IDD_SAFETY_UPDATE_DLG")); //ksj 20170823//&&
		return;
	}

	UpdateSchSafety();
	WriteSafetyUpdateLog(&sOrgSafetyInfoData, &m_sSafetyInfoData); //업데이트 로그 기록.

	DisableUpdate();
	//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("시험 안전조건 Update 완료.")); //ksj 20170823
	GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SafetyUpdateDlg_OnOK_msg3","IDD_SAFETY_UPDATE_DLG")); //ksj 20170823 //&&

	//CDialog::OnOK();
}

//void CSafetyUpdateDlg::Fun_SendSafetyCmd()
BOOL CSafetyUpdateDlg::Fun_SendSafetyCmd() //ksj 20170810 : 반환값 추가
{
	CWaitCursor cursor;	
	
	CString strMsg;
	CCyclerModule *pMD;
	//CCyclerChannel *pCH;
	//CCalibrationData *pChCalData;
	
	pMD = m_pDoc->GetCyclerMD(m_nModuleID);
	if(pMD == NULL)		return FALSE;

	int nRtn;
	if((nRtn = pMD->SendCommand(SFT_CMD_SAFETY_DATA_UPDATE, m_nChIndex, &m_sSafetyInfoData, sizeof(SFT_CMD_SAFETY_UPDATE_REPLY))) != SFT_ACK)	//Body 있는 Command
	{
		//AfxMessageBox("command 전송 실패");
		AfxMessageBox(Fun_FindMsg("SafetyUpdateDlg_Fun_SendSafetyCmd_msg1","IDD_SAFETY_UPDATE_DLG"));//&&
		return FALSE;
	}

	return TRUE;
}

void CSafetyUpdateDlg::OnButSafetyRequest() 
{
	// TODO: Add your control notification handler code here
	CWaitCursor cursor;	
	
	CString strMsg;
	CCyclerModule *pMD;
	//CCyclerChannel *pCH;
	//CCalibrationData *pChCalData;
	
	pMD = m_pDoc->GetCyclerMD(m_nModuleID);
	if(pMD == NULL)		return;

	//int nRtn = 0;
	/*CWordArray SelectCh;
	SelectCh.Add(m_nChIndex);*/

//	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	/*nRtn = m_pDoc->SendCommand(m_nModuleID, &SelectCh, SFT_CMD_SAFETY_DATA_REQUEST,  0, 0);		//Body 없는 Command
	if (nRtn != SFT_ACK)
	{
		strMsg.Format("%s Safety Request 전송에 실패... (%s)",
			m_pDoc->GetModuleName(m_nModuleID), pMD->GetCmdErrorMsg(nRtn));
		m_pDoc->WriteSysLog(strMsg);
	}	*/		

	//nRtn = pMD->SendCommand(SFT_CMD_SAFETY_DATA_REQUEST , DAQCommandData, sizeof(SFT_DAQ_ISOLATION));
	//if((nRtn = pMD->SendCommand(SFT_CMD_CHANNEL_CAN_SET_TRANS_DATA, nChIndex, pCanData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET))) != SFT_ACK)	//Body 있는 Command

	//ksj 20170809 : SendCommand를 사용할 경우 ACK 수신 대기를 10초간 하게 되므로 블록킹 상태에 걸리게 된다.
	//쓰레드로 명령을 전송하여 블록킹을 회피한다.
	ST_SEND_COMMAND_THREAD_PARAM* pParam = new ST_SEND_COMMAND_THREAD_PARAM;
	
	pParam->pMD = pMD;
	pParam->nCommand = SFT_CMD_SAFETY_DATA_REQUEST;
	pParam->SelectCh.Add((WORD)m_nChIndex);
	pParam->pStepRequest = 0;
	pParam->nSize = 0;
	
	AfxBeginThread(SendCommandThread,pParam);
	//ksj end

	if(m_pCH) m_pCH->WriteSchUpdateLog("Safety Request"); //ksj 201708010 : 로그 기록

	//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("시험 안전조건 요청....")); //ksj 20170823
	GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SafetyUpdateDlg_OnButSafetyRequest_msg1","IDD_SAFETY_UPDATE_DLG")); //ksj 20170823//&&
	
 }

//ksj 20170808 : SendCommand를 사용할 경우 ACK 수신 대기를 10초간 하게 되므로 블록킹 상태에 걸리게 된다.
//쓰레드로 명령을 전송하여 블록킹을 회피한다.
UINT CSafetyUpdateDlg::SendCommandThread(LPVOID lpParam)
{
	ST_SEND_COMMAND_THREAD_PARAM *pSendCommandParam = (ST_SEND_COMMAND_THREAD_PARAM *) lpParam;
	
	
	if(pSendCommandParam == NULL)
		return 0;
	
	if(pSendCommandParam->pMD == NULL)
		return 0;
	
	
	pSendCommandParam->pMD->SendCommand(pSendCommandParam->nCommand,
		//pSendCommandParam->nChIndex,
		&pSendCommandParam->SelectCh,
		pSendCommandParam->pStepRequest,
		pSendCommandParam->nSize);
	
	delete pSendCommandParam;
	
	return 1;
}

void CSafetyUpdateDlg::SetReceiveInfoData(SFT_CMD_SAFETY_UPDATE_REPLY *safetyInfoData)
{
	UpdateData(TRUE);
	memcpy(&m_sSafetyInfoData, safetyInfoData, sizeof(SFT_CMD_SAFETY_UPDATE_REPLY));
	/*m_edit_volt_min = m_sSafetyInfoData.sTestSafetySet.lVtgLow;
	m_edit_volt_max = m_sSafetyInfoData.sTestSafetySet.lVtgHigh;
	m_edit_cell_dev_max = m_sSafetyInfoData.sTestSafetySet.lVtgCellDelta;
	m_edit_capa_max = m_sSafetyInfoData.sTestSafetySet.lCapHigh;
	m_edit_power_max = m_sSafetyInfoData.sTestSafetySet.lWattHigh;
	m_edit_wh_max = m_sSafetyInfoData.sTestSafetySet.lWattHourHigh;*/

	CString strTemp;
	strTemp.Format(_T("%%.%df"),m_nVDec);
	m_edit_volt_min.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.lVtgLow * m_fVUnit);	
	m_edit_volt_max.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.lVtgHigh * m_fVUnit);
	//m_edit_cell_dev_max.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.lVtgCellDelta * m_fVUnit); //lyj 20200214
	strTemp.Format(_T("%%.%df"),m_nIDec);
	m_edit_current_max.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.lCrtHigh * m_fIUnit);
	strTemp.Format(_T("%%.%df"),m_nCDec);
	m_edit_capa_max.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.lCapHigh * m_fCUnit);
	strTemp.Format(_T("%%.%df"),m_nWDec);
	m_edit_power_max.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.lWattHigh * m_fWUnit);
	strTemp.Format(_T("%%.%df"),m_nWhDec);
	m_edit_wh_max.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.lWattHourHigh * m_fWhUnit);
	strTemp.Format(_T("%%.%df"),m_nVDec);
	m_edit_delta_std_min_v.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.faultCompAux_Refv1* m_fVUnit);	//lyj 20200214
	m_edit_delta_std_max_v.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.faultCompAux_Refv2* m_fVUnit);	//lyj 20200214
	m_edit_delta_min_v.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.faultCompAuxV* m_fVUnit);	//lyj 20200214
	m_edit_delta_v.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.faultCompAuxV2* m_fVUnit);	//lyj 20200214
	m_edit_delta_max_v.Format(strTemp,m_sSafetyInfoData.sTestSafetySet.faultCompAuxV3* m_fVUnit);	//lyj 20200214


	UpdateData(FALSE);
//	AfxMessageBox("rec safety info");

	if(m_pCH) m_pCH->WriteSchUpdateLog("recv safety info"); //ksj 201708010 : 로그 기록

	DisableUpdate();
	//GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(_T("시험 안전조건 수신 완료.")); //ksj 20170823
	GetDlgItem(IDC_STATIC_EVENT)->SetWindowText(Fun_FindMsg("SafetyUpdateDlg_SetReceiveInfoData_msg1","IDD_SAFETY_UPDATE_DLG")); //ksj 20170823//&&

}

BOOL CSafetyUpdateDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN )
	{
		switch( pMsg->wParam )
		{
		case VK_ESCAPE :
			{
				//				OnOK();
				return FALSE;
			}
		case VK_RETURN:
			{
				return FALSE;
			}
		}
	}	
	return CDialog::PreTranslateMessage(pMsg);
}

//ksj 20170807 : 값 단위 초기화
void CSafetyUpdateDlg::InitDataUnit()
{
	CString strUnit;
	
	m_fVUnit = 1.0f;
	m_fIUnit = 1.0f;
	m_fCUnit = 1.0f;
	m_fWUnit = 1.0f;
	m_fWhUnit = 1.0f;
	
	GetDlgItem(IDC_STATIC_VUNIT1)->GetWindowText(strUnit);
	
	if(strUnit == "V")
	{		
		m_fVUnit = 0.000001f;
	}
	if(strUnit == "mV")
	{		
		m_fVUnit = 0.001f;
	}
	
	GetDlgItem(IDC_STATIC_IUNIT)->GetWindowText(strUnit);
	
	if(strUnit == "A")
	{
		m_fIUnit = 0.000001f;
	}
	if(strUnit == "mA")
	{
		m_fIUnit = 0.001f;
	}
	else if(strUnit == "nA")
	{
		m_fIUnit = 1000.0f;
	}
	
	GetDlgItem(IDC_STATIC_CUNIT)->GetWindowText(strUnit);
	
	if(strUnit == "Ah")
	{
		m_fCUnit = 0.000001f;
	}
	else if(strUnit == "mAh")
	{
		m_fCUnit = 0.001f;
	}
	else if(strUnit == "nAh")
	{
		m_fCUnit = 1000.0f;
	}
	
//	GetDlgItem(IDC_STATIC_WUNIT)->GetWindowText(strUnit);
	
	if(strUnit == "KW" || strUnit == "kW")
	{
		m_fWUnit = 0.000001f ;		
	}
	else if(strUnit == "W")
	{
		m_fWUnit = 0.001 ;
	}
	else if(strUnit == "uW")
	{
		m_fWUnit = 1000.0f ;
	}
	
	GetDlgItem(IDC_STATIC_WHUNIT)->GetWindowText(strUnit);
	
	if(strUnit == "KWh" || strUnit == "kWh")
	{
		m_fWhUnit = 0.000001f ;	
	}
	else if(strUnit == "Wh")
	{
		m_fWhUnit = 0.001;
	}
	else if(strUnit == "uWh")
	{
		m_fWhUnit = 1000.0f;
	}
}

void CSafetyUpdateDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	//ksj 20170809 : 창 오픈시 자동 요청
	if(bShow == TRUE)
	{
		OnButSafetyRequest();
	}
}

//ksj 20170810 : Update 시 이전 값과 현재 값 변경 로그 기록.
void CSafetyUpdateDlg::WriteSafetyUpdateLog(SFT_CMD_SAFETY_UPDATE_REPLY* pSrc, SFT_CMD_SAFETY_UPDATE_REPLY* pDst)
{
	//CCyclerModule *pMD;
	//CCyclerChannel *pCH = m_pDoc->GetChannelInfo(m_nModuleID, m_nChIndex);
	
	if(m_pCH == NULL)		return;
	
	CString strLog;
	
	strLog.Format(_T("-------------------- Safety Update Log ------------------\n\
				 VtgLow : %d -> %d\n\
				 VtgHigh : %d -> %d\n\
//				 VtgCellDelta : %d -> %d\n\
				 CrtHigh : %d -> %d\n\
				 CapHigh : %d -> %d\n\
				 WattHigh : %d -> %d\n\
				 WattHourHigh : %d -> %d\n\n"),
				 pSrc->sTestSafetySet.lVtgLow, pDst->sTestSafetySet.lVtgLow,
				 pSrc->sTestSafetySet.lVtgHigh, pDst->sTestSafetySet.lVtgHigh,
//				 pSrc->sTestSafetySet.lVtgCellDelta, pDst->sTestSafetySet.lVtgCellDelta, //lyj 20200214
				 pSrc->sTestSafetySet.lCrtHigh, pDst->sTestSafetySet.lCrtHigh,
				 pSrc->sTestSafetySet.lCapHigh, pDst->sTestSafetySet.lCapHigh,
				 pSrc->sTestSafetySet.lWattHigh, pDst->sTestSafetySet.lWattHigh,
				 pSrc->sTestSafetySet.lWattHourHigh, pDst->sTestSafetySet.lWattHourHigh					
							 );
	
	
	m_pCH->WriteSchUpdateLog(strLog);

	
}

//ksj 20170811 : 기존 SCH 파일을 읽어 공통 안전 조건만 변경하여 업데이트한다.
BOOL CSafetyUpdateDlg::UpdateSchSafety()
{
	CString strTemp;
	CString strLogMsg;
	CString strContent = m_pCH->GetDataPath();
	CTime tTime = CTime::GetCurrentTime();
	CScheduleData stScheduleData;
	CELL_CHECK_DATA* pSafety = NULL;
	CString strLatestSchFileName = m_pCH->GetDataPath() + "\\" + m_pCH->GetTestName() + ".sch";
	CString strUpdateFileName;
	CString strTime;
	strTime.Format(_T("%d%02d%02d_%02d%02d%02d"),tTime.GetYear(),tTime.GetMonth(),tTime.GetDay(),tTime.GetHour(),tTime.GetMinute(),tTime.GetSecond());	
	
	strLogMsg.Format("%s\\Update", strContent);	
	::CreateDirectory(strLogMsg, NULL);	
	strLogMsg.Format("%s\\Update\\Update_%s", strContent, strTime);
	strContent	= strLogMsg;
	
	if(stScheduleData.SetSchedule(strLatestSchFileName)) //마지막 업데이트된 sch 파일(대표 sch)을 open 한다.
	{
		pSafety = stScheduleData.GetCellCheckParam();

		if(pSafety == NULL) return FALSE;		
		COleDateTime  date = COleDateTime::GetCurrentTime();
		sprintf((char*)stScheduleData.GetScheduleEditTime(), "%s", date.Format()); //ksj 20170811 : 스케쥴 변경 시간도 현재 시간으로 바꾼다.
		//안전 조건 변조
		pSafety->fMinV = m_sSafetyInfoData.sTestSafetySet.lVtgLow / 1000.0f;
		pSafety->fMaxV = m_sSafetyInfoData.sTestSafetySet.lVtgHigh / 1000.0f;
//		pSafety->fCellDeltaV = m_sSafetyInfoData.sTestSafetySet.lVtgCellDelta / 1000.0f; //lyj 20200214
		pSafety->fMaxI = m_sSafetyInfoData.sTestSafetySet.lCrtHigh / 1000.0f;
		pSafety->fMaxC = m_sSafetyInfoData.sTestSafetySet.lCapHigh / 1000.0f;
		pSafety->lMaxW = (long)(m_sSafetyInfoData.sTestSafetySet.lWattHigh * 1000.0f);
		pSafety->lMaxWh = m_sSafetyInfoData.sTestSafetySet.lWattHourHigh;

		/////////////////

		pSafety->fSTDCellDeltaMinV = m_sSafetyInfoData.sTestSafetySet.faultCompAux_Refv1 / 1000.0f; //ksj 20200216 : v1015 누락 조건 추가
		pSafety->fSTDCellDeltaMaxV = m_sSafetyInfoData.sTestSafetySet.faultCompAux_Refv2 / 1000.0f; //ksj 20200216 : v1015 누락 조건 추가
		pSafety->fCellDeltaMinV = m_sSafetyInfoData.sTestSafetySet.faultCompAuxV / 1000.0f; //ksj 20200216 : v1015 누락 조건 추가
		pSafety->fCellDeltaV = m_sSafetyInfoData.sTestSafetySet.faultCompAuxV2 / 1000.0f; //ksj 20200216 : v1015 누락 조건 추가
		pSafety->fCellDeltaMaxV = m_sSafetyInfoData.sTestSafetySet.faultCompAuxV3 / 1000.0f; //ksj 20200216 : v1015 누락 조건 추가

		////////////////////////


		//안전조건 변조 끝--- sbc 적용 여부에 관계 없이 sch 파일을 변조하므로 sbc와 싱크가 안맞을수 있다.
		
		if( ::CreateDirectory(strContent, NULL) == FALSE )
		{
			//strTemp.Format("Update 저장용 폴더 생성 실패(%s)", strLogMsg);
			strTemp.Format(Fun_FindMsg("SafetyUpdateDlg_UpdateSchSafety_msg1","IDD_SAFETY_UPDATE_DLG"), strLogMsg);//&&
			m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		else //폴더 생성 성공.
		{
			strUpdateFileName = strContent + "\\" + m_pCH->GetTestName() + ".sch";
			
			if(FALSE == stScheduleData.SaveToFile(strUpdateFileName)) //변조한 파일을 Update 폴더아래
			{
				//strLogMsg.Format("%s Schedule 파일 생성 실패", strUpdateFileName);
				strLogMsg.Format(Fun_FindMsg("SafetyUpdateDlg_UpdateSchSafety_msg2","IDD_SAFETY_UPDATE_DLG"), strUpdateFileName);//&&
				m_pDoc->WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			else //파일 저장 성공
			{
				DeleteFile(strLatestSchFileName); //상위 폴더의 대표 sch 제거
				CopyFile(strUpdateFileName,strLatestSchFileName,FALSE); //Update 된 sch 파일로 대체
			}		
		}		
	}
	else
	{
		return FALSE;
	}
	//
	
	return TRUE;
}


//ksj 20170823 UPDATE 가능 여부를 FALSE 로 바꾼다.
void CSafetyUpdateDlg::DisableUpdate()
{
	m_bUpdate = FALSE; 
	GetDlgItem(IDOK)->EnableWindow(m_bUpdate);
}


void CSafetyUpdateDlg::EnableUpdate()
{
	m_bUpdate = TRUE; 
	GetDlgItem(IDOK)->EnableWindow(m_bUpdate);
}

void CSafetyUpdateDlg::OnChangeEditVoltMin() 
{
	EnableUpdate();		
}

void CSafetyUpdateDlg::OnChangeEditVoltMax() 
{
	EnableUpdate();	
}

void CSafetyUpdateDlg::OnChangeEditPowerMax() 
{
	EnableUpdate();
}

void CSafetyUpdateDlg::OnChangeEditCapaMax() 
{
	EnableUpdate();
}

void CSafetyUpdateDlg::OnChangeEditCurrentMax() 
{
	EnableUpdate();	
}

void CSafetyUpdateDlg::OnChangeEditWhMax() 
{
	EnableUpdate();	
}

void CSafetyUpdateDlg::OnChangeEditCellDevMax() 
{
	EnableUpdate();	
}

void CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaMinV()
{
	EnableUpdate();	
}


void CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaStdMinV()
{
	EnableUpdate();	
}


void CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaStdMaxV()
{
	EnableUpdate();	
}


void CSafetyUpdateDlg::OnEnChangeEditIdcSafetyCellDeltaV()
{
	EnableUpdate();	
}


void CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaV()
{
	EnableUpdate();	
}


void CSafetyUpdateDlg::OnEnChangeEditSafetyCellDeltaMaxV()
{
	EnableUpdate();	
}
