#if !defined(AFX_SETTINGCANCHANGEDLG_H__FE23BBAB_AD0C_4146_B5D2_BD7FD52AA833__INCLUDED_)
#define AFX_SETTINGCANCHANGEDLG_H__FE23BBAB_AD0C_4146_B5D2_BD7FD52AA833__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SettingCanChangeDlg.h : header file
//
#include "CTSMonProDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CSettingCanChangeDlg dialog

class CSettingCanChangeDlg : public CDialog
{
// Construction
public:

	int nSelCh;
	int nSelModule;
	int nLastPos;
	void UpdateGridData();
	CSettingCanChangeDlg(CCTSMonProDoc * pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSettingCanChangeDlg)
	enum { IDD = IDD_CAN_COMMAND_CHANGE , IDD2 = IDD_CAN_COMMAND_CHANGE_ENG ,IDD3 = IDD_CAN_COMMAND_CHANGE_PL };
	CComboBox	m_ctrlCanName1;
	CComboBox	m_ctrlCanName2;
	CComboBox	m_ctrlCanName3;
	CComboBox	m_ctrlCanName4;
	CComboBox	m_ctrlCanName5;
	CComboBox	m_ctrlCanName6;
	CComboBox	m_ctrlCanName7;
	CComboBox	m_ctrlCanName8;
	CComboBox	m_ctrlCanName9;
	CComboBox	m_ctrlCanName10;
	float	m_fCanValue_1;
	float	m_fCanValue_2;
	float	m_fCanValue_3;
	float	m_fCanValue_4;
	float	m_fCanValue_5;
	float	m_fCanValue_6;
	float	m_fCanValue_7;
	float	m_fCanValue_8;
	float	m_fCanValue_9;
	float	m_fCanValue_10;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingCanChangeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSettingCanChangeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCboCanName1();
	afx_msg void OnSelchangeCboCanName2();
	afx_msg void OnSelchangeCboCanName3();
	afx_msg void OnSelchangeCboCanName4();
	afx_msg void OnSelchangeCboCanName5();
	afx_msg void OnSelchangeCboCanName6();
	afx_msg void OnSelchangeCboCanName7();
	afx_msg void OnSelchangeCboCanName8();
	afx_msg void OnSelchangeCboCanName9();
	afx_msg void OnSelchangeCboCanName10();
	afx_msg void OnButSend();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CCTSMonProDoc * m_pDoc;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETTINGCANCHANGEDLG_H__FE23BBAB_AD0C_4146_B5D2_BD7FD52AA833__INCLUDED_)
