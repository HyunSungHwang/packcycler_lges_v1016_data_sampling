// UserConfigDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "UserConfigDlg.h"

#include "DisplayItemEditDlg.h"
#include "CTSMonProView.h"
#include "ItemOnDisplay.h"

#include "Global.h"
#include "FileSaveSetDlg.h"
#include "LoginManagerDlg.h"
#include "Dlg_Dblogin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserConfigDlg dialog


CUserConfigDlg::CUserConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CUserConfigDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CUserConfigDlg::IDD2):	
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CUserConfigDlg::IDD3):
	(CUserConfigDlg::IDD), pParent)
	, m_nMaxAuxFloatingPoint(0)
	, m_nMaxCanFloatingPoint(0)
{
	//{{AFX_DATA_INIT(CUserConfigDlg)
	m_nGirdColSize = 8;
	m_nChTimer = 0;
	m_nMdTimer = 0;
	m_bCheckPrevData = FALSE;
	m_bDBUpdate = FALSE;
	m_bUseCan = FALSE;
	m_bAutoStopChamber = FALSE;
	m_bUseAux = FALSE;
	//}}AFX_DATA_INIT
}


void CUserConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserConfigDlg)
	DDX_Control(pDX, IDC_CHK_MONITORING, m_ChkMonitoring);
	DDX_Control(pDX, IDC_SAVE_ITEM_LIST, m_SaveItemList);
	DDX_Control(pDX, IDC_WH_PRESI_COMBO, m_ctrlWhDecimal);
	DDX_Control(pDX, IDC_W_PRESI_COMBO, m_ctrlWDecimal);
	DDX_Control(pDX, IDC_WH_UNIT_COMBO, m_ctrlWhUnit);
	DDX_Control(pDX, IDC_W_UNIT_COMBO, m_ctrlWUnit);
	DDX_Control(pDX, IDC_FONT_REAULT_STATIC, m_ctrFontResult);
	DDX_Control(pDX, IDC_TOP_COLUMN_LIST, m_wndColumnList);
	DDX_Control(pDX, IDC_TIME_UNIT_COMBO, m_ctrlTimeUnit);
	DDX_Control(pDX, IDC_C_PRESI_COMBO, m_ctrlCDecimal);
	DDX_Control(pDX, IDC_I_PRESI_COMBO, m_ctrlIDecimal);
	DDX_Control(pDX, IDC_V_PRESI_COMBO, m_ctrlVDecimal);
	DDX_Control(pDX, IDC_C_UNIT_COMBO, m_ctrlCUnit);
	DDX_Control(pDX, IDC_I_UNIT_COMBO, m_ctrlIUnit);
	DDX_Control(pDX, IDC_V_UNIT_COMBO, m_ctrlVUnit);
	DDX_Text(pDX, IDC_GRID_COL_SIZE_EDIT, m_nGirdColSize);
	DDV_MinMaxUInt(pDX, m_nGirdColSize, 1, 128);
	DDX_Text(pDX, IDC_CH_TIMER_EDIT, m_nChTimer);
	DDV_MinMaxUInt(pDX, m_nChTimer, 1, 3600);
	DDX_Text(pDX, IDC_MD_TIMER_EDIT, m_nMdTimer);
	DDV_MinMaxUInt(pDX, m_nMdTimer, 1, 3600);
	DDX_Check(pDX, IDC_UNSAFE_DATA_CHECK, m_bCheckPrevData);
	DDX_Check(pDX, IDC_CHECK_DB_UPDATE, m_bDBUpdate);
	DDX_Check(pDX, IDC_CHECK_USE_CAN, m_bUseCan);
	DDX_Check(pDX, IDC_CHECK_CHAMBER_STOP, m_bAutoStopChamber);
	DDX_Check(pDX, IDC_CHECK_USE_AUX, m_bUseAux);

	DDX_Text(pDX, IDC_EDIT_AUX_FLOATING_POINT, m_nMaxAuxFloatingPoint); //ksj 20201112
	DDV_MinMaxUInt(pDX, m_nMaxAuxFloatingPoint, 0, 6);
	DDX_Text(pDX, IDC_EDIT_CAN_FLOATING_POINT, m_nMaxCanFloatingPoint); //ksj 20201112
	DDV_MinMaxUInt(pDX, m_nMaxCanFloatingPoint, 0, 6);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CUserConfigDlg)
	ON_BN_CLICKED(IDC_ADD_ITEM_BUTTON, OnAddItemButton)
	ON_BN_CLICKED(IDC_DELETE_ITEM_BUTTON, OnDeleteItemButton)
	ON_NOTIFY(NM_DBLCLK, IDC_TOP_COLUMN_LIST, OnDblclkTopColumnList)
	ON_BN_CLICKED(IDC_GRID_FONT_BTN, OnGridFontBtn)
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_ITEM_SET_BUTTON, OnItemSetButton)
	ON_BN_CLICKED(IDC_BUT_DB, OnButDb)
	ON_BN_CLICKED(IDC_CHK_MONITORING, OnChkMonitoring)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserConfigDlg message handlers

BOOL CUserConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	InitColimnListCtrl();
	
	char szBuff[32], szUnit[16], szDecimalPoint[16];

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlVUnit.SelectString(0, szUnit);
	m_ctrlVDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlIUnit.SelectString(0,szUnit);
	m_ctrlIDecimal.SelectString(0,szDecimalPoint);

	#ifdef _EDLC_TEST_SYSTEM
		m_ctrlCUnit.ResetContent();
		m_ctrlCUnit.AddString("F");
		m_ctrlCUnit.AddString("mF");
		m_ctrlCUnit.AddString("uF");
	#endif

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlCUnit.SelectString(0,szUnit);
	m_ctrlCDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlWUnit.SelectString(0,szUnit);
	m_ctrlWDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlWhUnit.SelectString(0,szUnit);
	m_ctrlWhDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Time Unit", "0"));
	m_ctrlTimeUnit.SetCurSel(atol(szBuff));

	m_ctrFontResult.SetUserFont(&m_afont);

	m_nGirdColSize =  AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "GridColumnSize", 8);
	
	m_nChTimer = 1;//AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Channel Timer", 2);
	m_nMdTimer = 1;//AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Module Timer", 2);

	m_bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);
	m_bDBUpdate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DataBaseUpdate", FALSE);

	m_bUseAux = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseAUX", FALSE);
	m_bUseCan = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCAN", FALSE);

	m_nMaxAuxFloatingPoint = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "MaxAuxFloatingPoint", 3); //ksj 20201112 : 모니터링화면에서 Aux 값 소수점 아래 표시 개수
	m_nMaxCanFloatingPoint = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "MaxCanFloatingPoint", 6); //ksj 20201112 : 모니터링화면에서 Can 값 소수점 아래 표시 개수

	m_bAutoStopChamber = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoOvenStop", TRUE);

	CString strEQP_Code = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB EQP CODE", "PNE");
	CString strBlock_No = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB Block No", "1");
	GetDlgItem(IDC_EDIT_EQP_CODE)->SetWindowText(strEQP_Code);
	GetDlgItem(IDC_EDIT_BLOCK_NO)->SetWindowText(strBlock_No);

	LoadSaveItemList();

	if(g_AppInfo.strDbstate==_T("1"))   m_ChkMonitoring.SetCheck(TRUE);	
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUserConfigDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();

	char szBuff[32], szUnit[16], szDecimalPoint[16];
	m_ctrlVUnit.GetLBText(m_ctrlVUnit.GetCurSel(), szUnit);
	m_ctrlVDecimal.GetLBText(m_ctrlVDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "V Unit", szBuff);

	m_ctrlIUnit.GetLBText(m_ctrlIUnit.GetCurSel(), szUnit);
	m_ctrlIDecimal.GetLBText(m_ctrlIDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "I Unit", szBuff);

	m_ctrlCUnit.GetLBText(m_ctrlCUnit.GetCurSel(), szUnit);
	m_ctrlCDecimal.GetLBText(m_ctrlCDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "C Unit", szBuff);

	m_ctrlWUnit.GetLBText(m_ctrlWUnit.GetCurSel(), szUnit);
	m_ctrlWDecimal.GetLBText(m_ctrlWDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "W Unit", szBuff);

	m_ctrlWhUnit.GetLBText(m_ctrlWhUnit.GetCurSel(), szUnit);
	m_ctrlWhDecimal.GetLBText(m_ctrlWhDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "Wh Unit", szBuff);

	sprintf(szBuff, "%d", m_ctrlTimeUnit.GetCurSel());
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "Time Unit", szBuff);

	UpdateColumnSetting();

	AfxGetApp()->WriteProfileBinary(CT_CONFIG_REG_SEC, "GridFont",(LPBYTE)&m_afont, sizeof(LOGFONT));
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "GridColumnSize", m_nGirdColSize);
	
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Channel Timer", m_nChTimer);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Module Timer", m_nMdTimer);

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", m_bCheckPrevData);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "DataBaseUpdate", m_bDBUpdate);

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseAUX", m_bUseAux);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseCAN", m_bUseCan);

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "MaxAuxFloatingPoint", m_nMaxAuxFloatingPoint); //ksj 20201112 : 모니터링화면에서 Aux 값 소수점 아래 표시 개수
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "MaxCanFloatingPoint", m_nMaxCanFloatingPoint); //ksj 20201112 : 모니터링화면에서 Can 값 소수점 아래 표시 개수


	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AutoOvenStop", m_bAutoStopChamber);

	CString strEQP_Code;
	GetDlgItem(IDC_EDIT_EQP_CODE)->GetWindowText(strEQP_Code);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "DB EQP CODE",strEQP_Code);
	g_AppInfo.strDeviceid=strEQP_Code;
	
	CString strBlock_No; 
	GetDlgItem(IDC_EDIT_BLOCK_NO)->GetWindowText(strBlock_No);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "DB Block No",strBlock_No);
	g_AppInfo.strBlockid=strBlock_No;

	mainGlobal.ch_SetChReset();

	UpdateData(FALSE);

	CDialog::OnOK();
}

//1. 순서 결정
//2. 표기 여부 결정
//3. 표기명 결정
//4. 표기폭 결정
void CUserConfigDlg::InitColimnListCtrl()
{
	//m_pChStsSmallImage = new CImageList;
	//m_pChStsSmallImage->Create(IDB_CHECK,16,5,RGB(255,255,255));
	//m_wndColumnList.SetImageList(m_pChStsSmallImage,LVSIL_SMALL);

	DWORD dwExStyle = m_wndColumnList.GetExtendedStyle();
	dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;//|LVS_EX_CHECKBOXES ;
	m_wndColumnList.SetExtendedStyle(dwExStyle);
	
	m_wndColumnList.InsertColumn(0, "No", LVCFMT_LEFT, 30);
	m_wndColumnList.InsertColumn(1, Fun_FindMsg("InitColimnListCtrl_msg1","IDD_USER_CONFIG_DLG"), LVCFMT_LEFT, 100);
	//@ m_wndColumnList.InsertColumn(1, "Data 목록", LVCFMT_LEFT, 100);
	m_wndColumnList.InsertColumn(2, Fun_FindMsg("InitColimnListCtrl_msg2","IDD_USER_CONFIG_DLG"), LVCFMT_LEFT, 120);
	//@ m_wndColumnList.InsertColumn(2, "표기명", LVCFMT_LEFT, 120);
	m_wndColumnList.InsertColumn(3, "Width", LVCFMT_CENTER, 50);
	

	CCTSMonProApp *pApp = (CCTSMonProApp *)AfxGetApp();
 
	LVITEM lvItem;	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;
	CString strTitle;

	CString strTemp;
	//int nChannelDPItemNum = AfxGetApp()->GetProfileInt("DisplayItem", "ItemNum", 4);
	
	int nCount = 0;

	CItemOnDisplay *pItem = ((CCTSMonProView *)m_pParentWnd)->m_pItem;

	for( int nI = 0; nI < CItemOnDisplay::GetItemNum(); nI++ )
	{
		strTitle.Format("Col%02d", nI+1);
		strTitle = pApp->GetProfileString("DisplayItem", strTitle);

		if(!strTitle.IsEmpty())
		{
			int nItemNo = atol(strTitle.Right(2))-1;
			//m_wndColumnList.SetCheck(nItemNo);
			if(nItemNo >= 0 && nItemNo < PS_MAX_ITEM_NUM)
			{
				strTemp.Format("%d", nCount+1);
				lvItem.iItem = nCount;
				lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
				lvItem.iSubItem = 0;
				lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
				m_wndColumnList.InsertItem(&lvItem);

				m_wndColumnList.SetItemData(nCount, nItemNo);
			
				//
				m_wndColumnList.SetItemText(nCount, 1, ::PSGetItemName(pItem[nI].GetItemNo()));

				//사용자 표기명 출력
				//strTemp = pApp->GetProfileString("Settings", strTitle);	
				m_wndColumnList.SetItemText(nCount, 2, pItem[nI].GetTitle());
				
				//사용자 설정 폭 설정 
				//strTemp.Format("Width%02d", nItemNo+1);
				//strTemp.Format("%d", pApp->GetProfileInt("DisplayItem", strTemp, 50));
				strTemp.Format("%d", pItem[nI].GetWidth());
				m_wndColumnList.SetItemText(nCount, 3, strTemp);

				nCount++;
			}
		}
	}

	strTemp.Format(Fun_FindMsg("InitColimnListCtrl_msg3","IDD_USER_CONFIG_DLG"), m_wndColumnList.GetItemCount());
	//@ strTemp.Format("%d 개의 Data가 설정되었습니다.", m_wndColumnList.GetItemCount());
	GetDlgItem(IDC_REGISTED_COUNT_STATIC)->SetWindowText(strTemp);

}

void CUserConfigDlg::OnAddItemButton() 
{
	//POSITION pos = m_wndColumnList.GetFirstSelectedItemPosition();
	//int nItem = m_wndColumnList.GetNextSelectedItem(pos);
	//int nDataItemNo =  m_wndColumnList.GetItemData(nItem);
	//CString strTitle = m_wndColumnList.GetItemText(nItem, 1);	//Title
	//int nWidth = atol(m_wndColumnList.GetItemText(nItem, 2));	//width
	
	CDisplayItemEditDlg dlg(this);
	//dlg.SetData(nItem, nDataItemNo, strTitle, nWidth);
	
	CString strTemp;

	for(int i =0; i<m_wndColumnList.GetItemCount(); i++)
	{
		strTemp.Format("[%d]", m_wndColumnList.GetItemData(i));
		dlg.m_strRegistedItem += strTemp;
	}

	if(dlg.DoModal() == IDOK)
	{
		if(m_wndColumnList.GetItemCount()+1 < dlg.m_nSortNo)
		{
			dlg.m_nSortNo = m_wndColumnList.GetItemCount()+1;
		}
		LVITEM lvItem;	ZeroMemory(&lvItem, sizeof(LVITEM));
		lvItem.mask = LVIF_TEXT;
		lvItem.iItem = dlg.m_nSortNo-1;
		lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
		lvItem.iSubItem = 0;
		//lvItem.pszText = (LPSTR);
		m_wndColumnList.InsertItem(&lvItem);
		m_wndColumnList.SetItemData(lvItem.iItem, dlg.m_nItemNo);

		m_wndColumnList.SetItemText(lvItem.iItem, 1, ::PSGetItemName(dlg.m_nItemNo));	
		m_wndColumnList.SetItemText(lvItem.iItem, 2, dlg.m_strTitle);
		strTemp.Format("%d", dlg.m_nWidth);
		m_wndColumnList.SetItemText(lvItem.iItem, 3, strTemp);

		m_wndColumnList.SetItemState(lvItem.iItem, LVIS_SELECTED, LVIS_SELECTED);
	}

	UpdateColumnListCount();
}

void CUserConfigDlg::OnDeleteItemButton() 
{
	POSITION pos = m_wndColumnList.GetFirstSelectedItemPosition();
	while(pos)
	{
		int nItem = m_wndColumnList.GetNextSelectedItem(pos);
		m_wndColumnList.DeleteItem(nItem);
		pos = m_wndColumnList.GetFirstSelectedItemPosition();
	}
	UpdateColumnListCount();
}

void CUserConfigDlg::UpdateColumnSetting()
{
	int nChannelDPItemNum = m_wndColumnList.GetItemCount();
	CString strTemp, strItem;
	for(int i =0; i<nChannelDPItemNum; i++)
	{
		//사용자 설정 이름
		strItem.Format("Item%02d", m_wndColumnList.GetItemData(i)+1);
		AfxGetApp()->WriteProfileString("Settings", strItem, m_wndColumnList.GetItemText(i, 2));

		strTemp.Format("Col%02d", i+1);
		AfxGetApp()->WriteProfileString("DisplayItem", strTemp, strItem);

		strTemp.Format("Width%02d", i+1);
		AfxGetApp()->WriteProfileInt("DisplayItem", strTemp,  atol(m_wndColumnList.GetItemText(i, 3)));
	}

	AfxGetApp()->WriteProfileInt("DisplayItem", "ItemNum", nChannelDPItemNum);
}

void CUserConfigDlg::OnDblclkTopColumnList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	POSITION pos = m_wndColumnList.GetFirstSelectedItemPosition();

	int nItem = m_wndColumnList.GetNextSelectedItem(pos);
	
	CDisplayItemEditDlg dlg(this);
	dlg.m_nEditMode = TRUE;
	dlg.m_nSortNo = nItem +1;
	dlg.m_nItemNo =  m_wndColumnList.GetItemData(nItem);
	dlg.m_strTitle = m_wndColumnList.GetItemText(nItem, 2);	//Title
	dlg.m_nWidth = atol(m_wndColumnList.GetItemText(nItem, 3));	//width

	CString strTemp;
	for(int i =0; i<m_wndColumnList.GetItemCount(); i++)
	{
		strTemp.Format("[%d]", m_wndColumnList.GetItemData(i));
		dlg.m_strRegistedItem += strTemp;
	}

	if(dlg.DoModal() == IDOK)
	{
		//위치 변경을 위하여 현재 선택된 Item 삭제 
		m_wndColumnList.DeleteItem(nItem);


		if(m_wndColumnList.GetItemCount()+1 < dlg.m_nSortNo)
		{
			dlg.m_nSortNo = m_wndColumnList.GetItemCount()+1;
		}
		LVITEM lvItem;	ZeroMemory(&lvItem, sizeof(LVITEM));
		lvItem.mask = LVIF_TEXT;
		lvItem.iItem = dlg.m_nSortNo-1;
		lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
		lvItem.iSubItem = 0;
		//lvItem.pszText = (LPSTR)::PSGetItemName(dlg.m_nItemNo);
		m_wndColumnList.InsertItem(&lvItem);
		m_wndColumnList.SetItemData(lvItem.iItem, dlg.m_nItemNo);
		
		m_wndColumnList.SetItemText(lvItem.iItem, 1, ::PSGetItemName(dlg.m_nItemNo));
		m_wndColumnList.SetItemText(lvItem.iItem, 2, dlg.m_strTitle);
		strTemp.Format("%d", dlg.m_nWidth);
		m_wndColumnList.SetItemText(lvItem.iItem, 3, strTemp);

		m_wndColumnList.SetItemState(lvItem.iItem, LVIS_SELECTED, LVIS_SELECTED);

		UpdateColumnListCount();
	}	
	
	*pResult = 0;
}

void CUserConfigDlg::UpdateColumnListCount()
{
	CString strTemp;
	for(int i =0; i<m_wndColumnList.GetItemCount(); i++)
	{
		strTemp.Format("%d", i+1);
		m_wndColumnList.SetItemText(i, 0, strTemp);
	}
	strTemp.Format(Fun_FindMsg("UpdateColumnListCount_msg","IDD_USER_CONFIG_DLG"), m_wndColumnList.GetItemCount());
	//@ strTemp.Format("%d 개의 Data가 설정되었습니다.", m_wndColumnList.GetItemCount());
	GetDlgItem(IDC_REGISTED_COUNT_STATIC)->SetWindowText(strTemp);
}

void CUserConfigDlg::OnGridFontBtn() 
{
	// TODO: Add your control notification handler code here
	//LOGFONT afont; 
	//CFontDialog aDlg(&afont);
	
	CFontDialog aDlg(&m_afont);
	if(aDlg.DoModal()==IDOK)
	{
		aDlg.GetCurrentFont(&m_afont);
		m_ctrFontResult.SetUserFont(&m_afont);
	}	
}

void CUserConfigDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your message handler code here and/or call default
	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CUserConfigDlg::OnItemSetButton() 
{
	// TODO: Add your control notification handler code here
	CLoginManagerDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;

	CFileSaveSetDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		m_SaveItemList.ResetContent();
		LoadSaveItemList();
	}
}

void CUserConfigDlg::LoadSaveItemList()
{
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Time", TRUE))			m_SaveItemList.AddString(::PSGetItemName(PS_STEP_TIME));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage", TRUE))			m_SaveItemList.AddString(::PSGetItemName(PS_VOLTAGE));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Current", TRUE))			m_SaveItemList.AddString(::PSGetItemName(PS_CURRENT));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Capacity", TRUE))		m_SaveItemList.AddString(::PSGetItemName(PS_CAPACITY));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Watt", TRUE))			m_SaveItemList.AddString(::PSGetItemName(PS_WATT));	//ljb 20130423 add
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "WattHour", TRUE))		m_SaveItemList.AddString(::PSGetItemName(PS_WATT_HOUR));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgCurrent", TRUE))		m_SaveItemList.AddString(::PSGetItemName(PS_AVG_CURRENT));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgVoltage", TRUE))		m_SaveItemList.AddString(::PSGetItemName(PS_AVG_VOLTAGE));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenTemperature", FALSE))m_SaveItemList.AddString(::PSGetItemName(PS_OVEN_TEMPERATURE));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenHumidity", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_OVEN_HUMIDITY));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Meter", FALSE))			m_SaveItemList.AddString(::PSGetItemName(PS_METER_DATA));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "CVTime", TRUE))
	{
		m_SaveItemList.AddString(::PSGetItemName(PS_CV_TIME_DAY));
		m_SaveItemList.AddString(::PSGetItemName(PS_CV_TIME));
	}
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "SyncTime", TRUE))
	{
		m_SaveItemList.AddString(::PSGetItemName(PS_SYNC_DATE));	//ljb 2011418 이재복 //////////
		m_SaveItemList.AddString(::PSGetItemName(PS_SYNC_TIME));	//ljb 2011418 이재복 //////////
	}

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Input", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_VOLTAGE_INPUT));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Power", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_VOLTAGE_POWER));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Bus", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_VOLTAGE_BUS));	

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Output State", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_CAN_OUTPUT_STATE));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Input State", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_CAN_INPUT_STATE));

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderVoltage", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_LOAD_VOLT));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderCurrent", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_LOAD_CURR));

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerRefTemp", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_CHILLER_REF_TEMP));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerCurTemp", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_CHILLER_CUR_TEMP));

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerRefPump", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_CHILLER_REF_PUMP));
	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "ChillerCurPump", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_CHILLER_CUR_PUMP));

	if(AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "CellBALChData", FALSE))	m_SaveItemList.AddString(::PSGetItemName(PS_CELLBAL_CH_DATA));
}

void CUserConfigDlg::OnButDb() 
{
	Dlg_Dblogin dlg;
	dlg.DoModal();	
}

void CUserConfigDlg::OnChkMonitoring() 
{
	BOOL bchk=m_ChkMonitoring.GetCheck();
	
	g_AppInfo.strDbstate=GStr::str_IntToStr(bchk);
	GIni::ini_SetFile(g_AppIni,_T("DB"),_T("STATE"),g_AppInfo.strDbstate);
}