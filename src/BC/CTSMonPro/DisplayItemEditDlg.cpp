// DisplayItemEditDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "DisplayItemEditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDisplayItemEditDlg dialog


CDisplayItemEditDlg::CDisplayItemEditDlg(CWnd* pParent /*=NULL*/)
//	: CDialog(CDisplayItemEditDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CDisplayItemEditDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CDisplayItemEditDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CDisplayItemEditDlg::IDD3):
	(CDisplayItemEditDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CDisplayItemEditDlg)
	m_strTitle = _T("");
	m_nSortNo = 1;
	m_nWidth = 50;
	//}}AFX_DATA_INIT
	m_nItemNo = 0;
	m_nEditMode = FALSE;
}


void CDisplayItemEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDisplayItemEditDlg)
	DDX_Control(pDX, IDC_DATA_ITEM_COMBO, m_ctrlItemCombo);
	DDX_Text(pDX, IDC_COLUMN_TITLE_EDIT, m_strTitle);
	DDX_Text(pDX, IDC_SORT_NO_EDIT, m_nSortNo);
	DDX_Text(pDX, IDC_WIDTH_EDIT, m_nWidth);
	DDV_MinMaxUInt(pDX, m_nWidth, 10, 200);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDisplayItemEditDlg, CDialog)
	//{{AFX_MSG_MAP(CDisplayItemEditDlg)
	ON_CBN_SELCHANGE(IDC_DATA_ITEM_COMBO, OnSelchangeDataItemCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDisplayItemEditDlg message handlers

BOOL CDisplayItemEditDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CString strTemp;
	int nCount = 0;
	int nDefualtItem = 0;
	for( int nI = 0; nI < PS_MAX_ITEM_NUM; nI++ )
	{
		//이미 등록되어 있는 item은 표기 하지 않음 
		strTemp.Format("[%d]", nI);
		if(!m_nEditMode)
		{
			if(m_strRegistedItem.Find(strTemp) > -1)
			{
					m_nSortNo++;
					continue;
				
			}
		}

		//
		strTemp.Format("Item%02d", nI+1);
		strTemp = AfxGetApp()->GetProfileString("Settings", strTemp);
		if(!strTemp.IsEmpty())
		{
			strTemp = (LPSTR)::PSGetItemName(nI);
			if(!strTemp.IsEmpty())
			{
				m_ctrlItemCombo.AddString(strTemp);
				if(m_nItemNo == nI)
				{
					nDefualtItem = nCount;
				}
				m_ctrlItemCombo.SetItemData(nCount++, nI);

			}
		}
	}
	m_ctrlItemCombo.SetCurSel(nDefualtItem);
	OnSelchangeDataItemCombo();
	
	if(m_nEditMode)	m_ctrlItemCombo.EnableWindow(FALSE);
	
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDisplayItemEditDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	int nIndex = m_ctrlItemCombo.GetCurSel();
	if(nIndex == CB_ERR)	return;

	if(m_strTitle.IsEmpty() || m_strTitle.GetLength() > 32)
	{
		MessageBox(Fun_FindMsg("OnOK_msg1","IDD_DSP_ITEM"), "Error", MB_OK|MB_ICONSTOP);
		//@ MessageBox("Title명이 입력되지 않았거나 너무 깁니다.", "Error", MB_OK|MB_ICONSTOP);
		GetDlgItem(IDC_COLUMN_TITLE_EDIT)->SetFocus();
		return;
	}

	if(m_nSortNo < 1 || m_nSortNo > PS_MAX_ITEM_NUM)
	{
		CString strtemp;
		strtemp.Format(Fun_FindMsg("OnOK_msg2","IDD_DSP_ITEM"), PS_MAX_ITEM_NUM);
		//@ strtemp.Format("표기 위치 설정이 잘못 되었습니다.(1~%d)", PS_MAX_ITEM_NUM);
		MessageBox(strtemp, "Error", MB_OK|MB_ICONSTOP);
		GetDlgItem(IDC_SORT_NO_EDIT)->SetFocus();
		return;
	}
		
	m_nItemNo = m_ctrlItemCombo.GetItemData(nIndex);

	CDialog::OnOK();
}



void CDisplayItemEditDlg::OnSelchangeDataItemCombo() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_ctrlItemCombo.GetCurSel();
	if(nSel != CB_ERR)
	{
		CString str;
		m_ctrlItemCombo.GetLBText(nSel, str);
//		if(m_strTitle.IsEmpty())		
		m_strTitle = str;
		UpdateData(FALSE);
		((CEdit *)GetDlgItem(IDC_COLUMN_TITLE_EDIT))->SetSel(0, -1);
		GetDlgItem(IDC_COLUMN_TITLE_EDIT)->SetFocus();
	}
}