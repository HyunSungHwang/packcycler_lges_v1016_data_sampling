// CTSMonProView.h : interface of the CCTSMonProView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSMonProVIEW_H__E478C60A_2580_4FB8_BCDC_0F8D28D8A258__INCLUDED_)
#define AFX_CTSMonProVIEW_H__E478C60A_2580_4FB8_BCDC_0F8D28D8A258__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LedButton.h"
#include "ColorListbox.h"
#include "ItemOnDisplay.h"
#include "MenuCH.h"
#include "MeterSetDlg.h"
#include "ListCtrlEx.h"
#include "DetailOvenDlg.h"
#include "RestoreDlg.h"
#include "WorkStationSignDlg.h"
#include "RTTableSet.h"//yulee 20181114
#include "Dlg_CellBAL.h"
#include "Dlg_PwrSupplyStat.h"
#include "ColorListCtrl.h" //ksj 20200120
#include "JsonMon.h" //ksj 20200901

#include "ButtonSc.h"
#include "afxwin.h"
#include "afxmt.h"
#include "xpbutton.h"
#include "dlgconnectionsequence.h"

#define TAB_STATE	 10
#define TAB_VOLTAGE	 11
#define TAB_CURRENT  12
#define TAB_CAPACITY 13
#define TAB_GRADE    14
#define TAB_TEST_NAME	15	

#define TAB_BMS_CONTROL	2
#define TAB_CH_LIST		1
#define TAB_DEFAULT		0

#define	TIMER_DISPLAY_CHANNEL			101
#define	TIMER_DISPLAY_MODULE			102
#define TIMER_DELAY_TIMER				103
#define TIMER_MONITORING				104			//20110319 KHS
#define TIMER_DAY_CHANGE_CHECK			105			//ksj 20160801
#define TIMER_RESRV_MUX_RECV_WAIT		106			//yulee 20181025_1
#define TIMER_RESRV_MUX_RECV			107			//yulee 20181025_1
#define TIMER_SBCDATADOWN_DONE_CHECK	108			//yulee 20190427
#define TIMER_WINDOW_FLICKERING			109			//yulee 20190531
#define TIMER_ALARM_ENABLE				110			//yulee 20190531
#define TIMER_AUTO_UPDATE				111			//lyj 20210809

#define MAX_SBC_STEP_END_DATA_FILE			7		//ksj 20160728

#define nTOT_BTN_NUM 4 //ksj 20200122 : 위치이동.
#define MAIN_VIEW_BUTTON_COUNT nTOT_BTN_NUM //ksj 20200122

class CCTSMonProDoc;
class CReserveDlg;	//ksj 20160426
class CStartDlgEx;
class CSelectScheduleDlg;

///////////////////////////
///////////////////////////
class MyObject
{
public:
    BOOL mIsOpened;
};
///////////////////////////
///////////////////////////

class CParallelConfigDlg; //ksj 20200122
class CCTSMonProView : public CFormView
{
protected: // create from serialization only
	CCTSMonProView();
	DECLARE_DYNCREATE(CCTSMonProView)
	CMenuCH		m_ControlMenu;			
	CMenuCH		m_PauseSubMenu;			
	CMenuCH		m_StopSubMenu;	
	BOOL		m_onPauseLog1; //yulee 20181008-1 로그 보강 
	BOOL		m_onPauseLog2; //yulee 20181008-1 로그 보강
	BOOL		m_onPauseLog3; //yulee 20181008-1 로그 보강
	BOOL		m_onPauseLog4; //yulee 20181008-1 로그 보강

	BOOL m_AlramSoundEnable;

public:
	//{{AFX_DATA(CCTSMonProView)
	enum { IDD = IDD_CTSMonPro_FORM, IDD2 = IDD_CTSMonPro_FORM_ENG , IDD3 = IDD_CTSMonPro_FORM_PL };

	CButtonSc		m_ctrlVentOpen; //ksj 20200122 : vent open 버튼 추가 (v1016. 오창 LGC 2공장 요청)
	CButtonSc		m_ctrlVentClear;
	CButtonSc		m_ctrlButBuzzerStop;
	CButtonSc		m_ctrlEMG;
	CXPButton	m_ctrlReserv;
	CXPButton	m_ctrlViewRealGraph;
	CLabel	m_ctrlLabelBmsChInfo;
	CXPButton	m_ctrlAllView;
	CXPButton	m_ctrlViewLog;
	CXPButton	m_ctrlEditor;
	CXPButton	m_ctrlAnal;
	CXPButton	m_ctrlCbankOn;
	CXPButton	m_ctrlCbankOff;
	CTabCtrl	m_tabCtrl;
	CLabel	m_ctrlLabelWhUnit2;
	CLabel	m_ctrlLabelWhUnit;
	CLabel	m_ctrlLabelWUnit2;
	CLabel	m_ctrlLabelWUnit;
	CStaticCounter	m_ctrlStaticWattHour2;
	CStaticCounter	m_ctrlStaticWattHour;
	CStaticCounter	m_ctrlStaticWatt2;
	CStaticCounter	m_ctrlStaticWatt;
	CLabel	m_ctrlLabelWattHour2;
	CLabel	m_ctrlLabelWattHour;
	CLabel	m_ctrlLabelWatt2;
	CLabel	m_ctrlLabelWatt;
	CListCtrl	m_wndCanList;
	CXPButton	m_ctrlBtnDataView;
	CXPButton	m_ctrlBtnCheckCond;
	CXPButton	m_ctrlGraphView;
	CLabel	m_ctrlLabelChState;
	CLabel	m_ctrlLabelChInfo;
	CLabel	m_ctrlLabelTotalTime;
	CLabel	m_ctrlLabelStepTime;
	CStaticCounter	m_ctrlStaticTotalTime;
	CStaticCounter	m_ctrlStaticStepTime;
	CXPButton	m_ctrlPause;
	CXPButton	m_ctrlStop;
	CXPButton	m_ctrlStart;
	CXPButton	m_ctrlNextStep;
	CLabel	m_ctrlLabelCh;
	CLabel	m_ctrlLabelImpedance;
	CLabel	m_ctrlLabelVoltage;
	CLabel	m_ctrlLabelCurrent;
	CLabel	m_ctrlLabelCapasity;
	CStaticCounter	m_ctrlStaticVoltage;
	CStaticCounter	m_ctrlStaticCurrent;
	CStaticCounter	m_ctrlStaticCapasity;
	CLabel	m_ctrlLabelVUnit;
	CLabel	m_ctrlLabelAUnit;
	CLabel	m_ctrlLabelCUnit;
	CLabel	m_ctrlLabelVoltage2;
	CLabel	m_ctrlLabelCurrent2;
	CLabel	m_ctrlLabelCapasity2;
	CStaticCounter	m_ctrlStaticVoltage2;
	CStaticCounter	m_ctrlStaticCurrent2;
	CStaticCounter	m_ctrlStaticCapasity2;
	CLabel	m_ctrlLabelVUnit2;
	CLabel	m_ctrlLabelAUnit2;
	CLabel	m_ctrlLabelCUnit2;
	CLabel	m_ctrlLabelVoltage3;
	CLabel	m_ctrlLabelCurrent3;
	CLabel	m_ctrlLabelCapasity3;
	CStaticCounter	m_ctrlStaticVoltage3;
	CStaticCounter	m_ctrlStaticCurrent3;
	CStaticCounter	m_ctrlStaticCapasity3;
	CLabel	m_ctrlLabelVUnit3;
	CLabel	m_ctrlLabelAUnit3;
	CLabel	m_ctrlLabelCUnit3;
	CLabel	m_ctrlLabelVoltage4;
	CLabel	m_ctrlLabelCurrent4;
	CLabel	m_ctrlLabelCapasity4;
	CStaticCounter	m_ctrlStaticVoltage4;
	CStaticCounter	m_ctrlStaticCurrent4;
	CStaticCounter	m_ctrlStaticCapasity4;
	CLabel	m_ctrlLabelVUnit4;
	CLabel	m_ctrlLabelAUnit4;
	CLabel	m_ctrlLabelCUnit4;
	CStaticCounter	m_ctrlStaticImpedance;
	CStaticCounter	m_ctrlStaticCh;
	CListCtrl	m_wndChInfoList;

	//CListCtrl	m_wndAuxList;
	CColorListCtrl	m_wndAuxList; //ksj 20200120
	CLabel	m_ReservedCmdStatic;
	CListBox	m_ctrlLogList;
	CString	m_ctrlLoaderState_1;
	CString	m_ctrlLoaderState_2;
	CString	m_ctrlLal_CellDeltaVolt;
	CString	m_LabAuxTempMax;
	CString	m_LabAuxTempMin;
	CString	m_LabAuxVoltMax;
	CString	m_LabAuxVoltMin;
	CString	m_LabAuxTherMax;
	CString	m_LabAuxTherMin;
	//}}AFX_DATA
// Attributes
public:
//	SEC3DTabWnd		m_tabWnd;	

	SFT_BMA_CHANNEL_INFO		m_Bma_Channel_Info;
	SFT_BMS_FAN_TEST_RESPONSE	m_Bms_Result_Fan_Test;
	SFT_BMS_CPU_ID_RESPONSE		m_Bms_Result_Cpu_Test;
	CReserveDlg*				m_pReserveDlg;			//ksj 20160426
	CRTTableSet*				m_pRTTableSetDlg;		//yulee 20181118

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSMonProView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
public:
	typedef enum 
	{
		PS1_NONE  = 0,
		PS1_PLAY  = 1,
		PS1_PAUSE = 2,
		PS1_END   = 3
	} PLAYSTATE;

	//yulee 20181114
	CRTTableSet *m_pRTtableSet;
	
	//cny
	Dlg_CellBAL *m_pDlgCellBal;
	CString m_strWarnTitle;
	CString m_strWarnMessage;
	void onCellbalShow(int iType);
	void onRTtableShow(int iType);//yulee 20181114
	BOOL onSbcNextStep(int nModuleID,int nChannelIndex);
	BOOL onSbcPause(int nModuleID,int nChannelIndex);
	void onShowMessage(int iType,CString strTitle,CString strMessage);

	PLAYSTATE m_PlayState;
	HANDLE    m_hState;	
	void      onInit();
	void      onDbInit();
	void      onMonitoringDeal();
	void      onMonitoringDb();
	static UINT WINAPI StateThread( LPVOID wnd );


	CString Fun_GetFunctionString(UINT nType, int nDivision);
	BOOL Fun_MuxChangeStart(CString strComm, int nOption, CDWordArray *pSelChArray);
	CString Fun_GetAuxTemperature(CCyclerModule *lpModule, CCyclerChannel *pCyclerCh, int nChannel);
	void Fun_BMAWorkStop();
	BOOL Fun_CpuTest(UINT iChannel);
	BOOL Fun_FanTest(UINT iChannel);
	CString Fun_getAppPath();
	int  GetFindCharCount(CString pString, char pChar) ;
	BOOL DeletePath(CString strPath, CProgressWnd *pPrgressWnd = NULL);
	BOOL Fun_BMASimpleFileLoad(CString strFileName);
	BOOL Fun_BMASimpleProcess(CString strTestPath, CString strLotID);

	void Fun_CmdStop();
	void SetLedUnitAll();
	BOOL m_bUseAux;
	BOOL m_bUseCan;
	BOOL m_nUseCan;
	BOOL bShowEmgDlg; //2014.09.26 EMG 알람창 상태.
	BOOL m_bUseIsolation;
	BOOL m_bUseLoader;
	BOOL m_bUseMux;
	BOOL m_bUseLoadType;
	BOOL m_bOverChargerSet;  //yulee 20191017 OverChargeDischarger Mark
	int  m_nFlagMuxResv; //yulee 20181025_1
	CString m_strMuxSetRetMsg; //yulee 20181025_1
	BOOL m_bUseCbank;			//LJH(19.10.14)::CBank 관련 추가
	BOOL m_bAccCycleOffSet;	//lyj 20201013 세인 ENG 누적사이클 화면표시 +1
	BOOL m_bUseWorkReserv;
	BOOL m_bUpdateRunning;

	void UpdateCANListData();
	void InitCanList();
	void UpdateChListForSlave(int nListIndex);
	CString m_strLastErrorString;
	void InitStaticCounter();
	void UpdateChInfoList();
	void InitLabel(int nFontSize);
	void UpdateAuxListData();
	void InitChInfoList();
	void InitAuxList();
	BOOL GetSelModuleChannel(int &nModuleID, int &nChannelIndex);
	void UpdateGridRowTopCell();
	void	LockUpdate()	{	m_bUpdateLock = TRUE;	}
	void	UnLockUpdate()	{	m_bUpdateLock = FALSE;	}

//	void SendCmdToModule(int nCmd);
	BOOL SendCmdToModule(CDWordArray &adwTargetCh, int nCmd, int nCycleNo = 0, int nStepNo = 0);
	BOOL SendChamberCmdToModule(CDWordArray * adwTargetCh, int nCmd, BOOL bSetFlag);

	void ReDrawChListCtrl(int nModuleID = 0);
	BOOL ReDrawGrid();
	void SetModuleSelChanged(int nModuleID);
	void UpdateChListData();
	void UpdateGridData();
	BOOL GetGridModuleChannel(const int nRow, const int nCol, UINT &nModuleID, UINT &nChIndex);
	BOOL UpdateSelectedChList(UINT nCmdID = SFT_CMD_NONE, BOOL bUpdate = TRUE);
	BOOL UpdateSelectedChListEMG(UINT nCmdID/*= SFT_CMD_NONE*/, BOOL bUpdate = TRUE/*= TRUE*/);
	BOOL UpdateUIChList(UINT nCmdID = SFT_CMD_NONE, BOOL bUpdate = TRUE);	//ljb UI update 용
	
	void OnTimerFuncPollingControlServer();

	void ResetFuncAllBut();
	void MoveWindowWithVerGap(unsigned UpperID, unsigned LowerID, int Gap); //yulee 20181019
	void ResetChinfoListHeightInCh4(unsigned CtrlID, int Gap); //yulee 20181022
	void ResetFuncBut(unsigned CtrlID, int nRow, int nColumn, int nChNum, int nSizeType = 1); //yulee 20181022

	void 	OnUpdateChannelList();
	void 	OnUpdateModuleList();
	void	RemakeChannelList();	//config dialog이후에 채널 리스트 컨트롤을 다시 그린다.
	BOOL    GetThTableLastWriteTime(CString strTargetFileName, CTime &retCtime); //yulee 20181101

	void	SetConnectionStatus(bool bConnect);
	void 	CloseThis(BYTE byUnitNo);

	BOOL	CheckAuxListSet(BOOL &bTemp, BOOL &bTher); //20181114 yulee	
	BOOL	CheckCANListSet(BOOL &bCANTemp); //20181114 yulee

	BOOL	CheckTempTherCANTemp(BOOL &bTempChk, BOOL &bTherChk, BOOL &bCANTempChk); //yulee 20181114	
	BOOL	CheckAuxRTtableSet(int *ArrChkRet1, int *ArrChkRet2, int *ArrChkRet3, int *ArrChkRet4); //20181118 yulee
	
	CCTSMonProDoc* 	GetDocument();
	void 	OnDisplayChannel();

	virtual ~CCTSMonProView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const; 
#endif

	CItemOnDisplay*		m_pItem;

public:
	int RunProcess_WorkStart(int nOption, CProgressWnd* pProgressWnd, CDWordArray* pAdwChArray, CScheduleData* pScheduleInfo, CStartDlgEx* pStartDlg, CString strTestName, int nModelPK, int nTestPK, CString strSchPath, int ReserveID = 0);
	int RunProcess_Chamber(int nOption, CCTSMonProDoc* pDoc, CStartDlgEx* pStartDlg, CDWordArray* adwChArray, CProgressWnd* pProgressWnd);
	int RunProcess_PrepareSchedule(int nOption, CStartDlgEx* pStartDlg, CScheduleData* pScheduleInfo, CPtrArray* aPtrSelCh, CDWordArray* adwChArray, CString* strTestName, CString* strScheduleFileName, CProgressWnd* pProgressWnd, int* nModelPK, int* nTestPK);
	BOOL RunProcess_ChUpdate(int nOption, BOOL* bStartStepOption, CString* strLotNo, CPtrArray* aPtrSelCh);
	BOOL SetMuxFunc(LPVOID pWork); //yulee 20181025
	int RunProcess(int nOption = 0, LPVOID pReservedWork = NULL, int ReserveID = 0);
	BOOL Fun_IsolationStart(int nCh, int nMainch);
	BOOL Fun_IsolationEnd(int nCh, int nMainCh);
	BOOL Fun_IsolationStartNormal(int nCh); //ksj 20210105 : 일반 절연/비절연용 함수 분리
	BOOL Fun_IsolationEndNormal(int nCh); //ksj 20210105 : 일반 절연/비절연용 함수 분리

	void Fun_DayChangeCheck();
	void Fun_WindowFlickering();//yulee 20190531
	int Fun_Chk_RESRV_MUX_REC(int nOption, LPVOID pReservedWork,int ReserveID);//yulee 20181025_1
	void Fun_SBCDataDownDoneCheck();//yulee 20190427
	LRESULT OnChannelTestComplete(WPARAM wParam, LPARAM lParam);
	LRESULT OnChannelAttributeReceived(WPARAM wParam, LPARAM lParam); //ksj 20200122
	LRESULT OnChannelTestPause(WPARAM wParam, LPARAM lParam); //ksj 20200601
	BOOL CheckAuxSafetyCond();
	void Fun_SchAllUpdate(CString strTestPath, CString strContent);
	BOOL SetMuxChoice(int nType, int ResvMuxValue = -1, CString ResvAdwTargetChArray = ""); //yulee 20181025
	void Wait_Milli(DWORD dwMillisecond);//yulee 20181026
	//BOOL DownAllRTTableFromSBC();  //yulee 20181114
	BOOL DownAllRTTableFromSBC(int nSelectedRtType);  //ksj 20200207	
	//BOOL UploadRTTableFromSBC(int nFileNum = 0, CString strFileName = "", int nTypeOper = 0);
	BOOL UploadRTTableFromSBC(int nFileNum, CString strFileName, int nTypeOper, int nRtType); //ksj 20200207
	BOOL UpdateRTTableDB(int nCode, CString strCodeName, int nDivision, CString strReserved);
	//BOOL SetRTTableSendToModule(int nSelModule, SFT_RT_TABLE_SEND_END *RT_TableSendEnd);
	BOOL SetRTTableSendToModule(int nSelModule, void *RT_TableSendEnd, int nRtType); //ksj 20200207
	//BOOL DeleteRTTableNameDB();
	BOOL DeleteRTTableNameDB(int nDIvision);  //ksj 20200207
	void Fun_AutoUpdate(); //lyj 20210809
	BOOL Fun_ChkRunChannel(); //lyj 20210809

	int m_nOption_Mux; //yulee 20181025_1
	LPVOID m_pReservedWork_Mux;//yulee 20181025_1
	int m_nReserveID;//yulee 20181025_1
	//	strMsg.Format("Channel : %d \n state : %s \n (setting : %d, state : %d, fault_flag %d)",sMuxResponse.chNum, strTemp, sMuxResponse.setting, sMuxResponse.state, sMuxResponse.fault_flag);
	int m_ResvMuxRecvChNum;//yulee 20181025_1
	CString m_strResvMuxRecvState;//yulee 20181025_1
	int m_ResvMuxRecvSet;//yulee 20181025_1

	int m_ResvMuxSetRet;//yulee 20181026
	int m_FlagBackgroundColor; //yulee 20190531 //lyj 20200916 주석처리
	FLICKER_INFO m_stPreFickerInfo; //lyj 20201007 Flicker

	//cny 201809
	void onOvenUse(int nItem);//yulee 20190608
	void onUsePlacePlate();

	void onPwrSupplyUse(int iType);
	void onPwrSupplyDeal(int iType,int iVisible=-1);
	void onChillerUse(int iType);
	void onChillerDeal(int iType,int iVisible=-1);

private:
	CUIntArray	m_uiArryCanDivition;	//ljb 2011218 이재복 //////////
	CStringArray m_strArryCanName;		//ljb 2011218 이재복 //////////
	CUIntArray	m_uiArryAuxDivition;	//ljb 2011218 이재복 //////////
	CStringArray m_strArryAuxName;		//ljb 2011218 이재복 //////////
	CUIntArray	m_uiArryAuxThrDivition;	//ljb 20160504 이재복 //////////
	CStringArray m_strArryAuxThrName;	//ljb 20160504 이재복 //////////

	CUIntArray	m_uiArryAuxHumiDivition;	//ksj 20200207
	CStringArray m_strArryAuxHumiName;	//ksj 20200207

protected:

	//cny 201809
	Dlg_PwrSupplyStat *m_pPwrSupplyStat1;
	Dlg_PwrSupplyStat *m_pPwrSupplyStat2;
    //cny 201809
	CDetailOvenDlg *m_pDetailChillerDlg1;
	CDetailOvenDlg *m_pDetailChillerDlg2;
	CDetailOvenDlg *m_pDetailChillerDlg3;
	CDetailOvenDlg *m_pDetailChillerDlg4;

	CDetailOvenDlg * m_pDetailOvenDlg1;
	CDetailOvenDlg * m_pDetailOvenDlg2;
	CWorkStationSignDlg * m_pWorkStationSignDlg; //yulee 20181002
	int					arrayChInfo[PS_MAX_ITEM_NUM];
	CMeterSetDlg *m_wndMeterDlg;
	BOOL GridTextMode(BOOL bText = TRUE);
	int	GetStartChNo(int nModuleID);
	LOGFONT				m_lfGridFont;
	CFont				m_lfChListFont;
	
	BOOL				m_bUpdateLock;
	CGridCtrl m_Grid;
	CString				m_strLogFileName;
	CString				m_strDebugFileName;
	CString				m_strChLogFileName;

	UINT				m_nCurrentModuleID;
	int					m_nCurrentChannelNo;
	int					m_nPreChannelNo; //yulee 20181009
	CCyclerChannel		*pCurrentChannel;
	CLedButton			m_wndLedButton;

	//CListCtrl			m_wndChannelList;
	CListCtrlEx			m_wndChannelList;
	CListCtrl			m_wndModuleList;

	CImageList*			m_pChStsSmallImage;
	CImageList*			m_pChLineSts;
	CImageList		m_TreeImageList;	//Tab Image List
	CScheduleData	m_BMAScheduleInfo;
	
	void 	InitModuleList();	//Module List 초기화 
	void  	InitChannelList();	//Channel List 초기화 

	int				m_nCanMasterIDType;

	long	m_lChMuxState;		//ljb 20151111
	long	m_lChMuxBackupInfo;	//ljb 20151111
// Generated message map functions
protected:
	BOOL InsertWorkInfo(UINT nModuleID, UINT nChannelIndex, CString strScheduleName, CString strTestName, CString strTestPath, CString strTrayNo, CString strTestSerial, CString strLotNo);
	void SetSelectedChNo(int nModuleID, int nChannelIndex = -1);
	void SetCurTab(int nTab);
	void ExeCTSAnal(int nType = 0);
	void ExeCTSRealGraphic();			//ljb 201159 
	UINT m_nChannelUpdateInterval;
	UINT m_nModuleUpdateInterval;
	UINT m_nMonitoringInterval;			//20110319 KHS
	void CopyDataToClipBoard(CListCtrl *pListCtrl);
	UINT m_nDisplayCol;
	UINT m_nCurTabIndex;
//	CDWordArray m_aSelChArray,m_aSelChArray2;
	CDWordArray m_adwTargetChArray;
	//,m_awTargetChArray2;
	BOOL m_bRestoring;		//ljb 손실 데이터 복구중(TRUE)
	BOOL InitTabCtrl();
	BOOL InitGrid();
	void DisplayLblChange();//yulee 20181019

	CRestoreDlg* m_pRestoreDlg;	

	// Added by 224
	void CheckPrevProcessStatusAndReprocess() ;
	CModbusConvDlg* m_pModbusConvDlg ;
	CLinConvDlg* m_pLinConvDlg ;
	CUartConvDlg* m_pUartConvDlg ;
	
	CString			m_tmpDay;		 //ksj 20160801

	CRTTableSet* m_pRTtableSetDlg;
	CBrush m_backBrush;//.CreateSolidBrush(RGB(25, 25, 25));



	//void OnVentClear();
	void OnVentClear(BYTE byCloseOpenFlag = 0, BOOL bAsk = TRUE); //ksj 20200122 : Close/Open Flag 추가.
	void HideOrShowButton();
	//void HideVentClose();
	

	//{{AFX_MSG(CCTSMonProView)
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBtnTestview();
	afx_msg void OnCheckCond();
	afx_msg void OnConfig();
	afx_msg void OnCaliSetting();
	afx_msg void OnUpdateCheckCond(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCaliSetting(CCmdUI* pCmdUI);
	afx_msg void OnRclickListChannel(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDblclkListModule(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClearState();
	afx_msg void OnUpdateClearState(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdRun(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdPause(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdContinue(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdStop(CCmdUI* pCmdUI);
	afx_msg void OnCmdRun();
	afx_msg void OnCmdPause();
	afx_msg void OnCmdContinue();
	afx_msg void OnCmdStop();
	afx_msg void OnManagePattern();
	afx_msg void OnGetdispinfoListChannel(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGetdispinfoListModule(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridRButtonUp(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridLButtonUp(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridConfig();
	afx_msg void OnCalibration();
	afx_msg void OnLogWnd();
	afx_msg void OnUpdateGridConfig(CCmdUI* pCmdUI);
	afx_msg void OnDataView();
	afx_msg void OnFileSave();
	afx_msg void OnDestroy();
	afx_msg void OnItemchangedListModule(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnSelectAll();
	afx_msg void OnUpdateSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnItemchangedListChannel(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickListModule(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateNextStep(CCmdUI* pCmdUI);
	afx_msg void OnCmdNextStep();
	afx_msg void OnUserConfig();
	afx_msg void OnDataRestore();
	afx_msg void OnUpdateDataRestore(CCmdUI* pCmdUI);
	afx_msg void OnGraphView();
	afx_msg void OnExeDataView();
	afx_msg void OnWorkLogView();
	afx_msg void OnWorkFolderView(); //lyj 20210108 
	afx_msg void OnCmdStopCancel();
	afx_msg void OnUpdateCmdStopCancel(CCmdUI* pCmdUI);
	afx_msg void OnCmdStopStep();
	afx_msg void OnCmdStopCycle();
	afx_msg void OnCmdStopEtc();
	afx_msg void OnCmdPauseStep();
	afx_msg void OnCmdPauseCycle();
	afx_msg void OnCmdPauseEtc();
	afx_msg void OnCmdPauseCancel();
	afx_msg void OnUpdateCmdStopStep(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdStopCycle(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdStopEtc(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdPauseStep(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdPauseCycle(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdPauseEtc(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCmdPauseCancel(CCmdUI* pCmdUI);
	afx_msg void OnSimpleTest();
	afx_msg void OnUpdateSimpleTest(CCmdUI* pCmdUI);
	afx_msg void OnUpdateParallelConfig(CCmdUI* pCmdUI);
	afx_msg void OnParallelConfig();
	afx_msg void OnAuxDataConfig();
	afx_msg void OnUpdateAuxDataConfig(CCmdUI* pCmdUI);
	afx_msg void OnBtnStart();
	afx_msg void OnBtnStop();
	afx_msg void OnBtnNextStep();
	afx_msg void OnBtnDataView();
	afx_msg void OnBtnGraphView();
	afx_msg void OnBtnCheckCond();
	afx_msg void OnCanConfig();
	afx_msg void OnCanConfigLintoCANRX();
	afx_msg void OnCanConfigLintoCANTX();
	afx_msg void OnCanConfigRS485RX();
	afx_msg void OnCanConfigRS485TX();
	afx_msg void OnCanConfigSMBusRX();
	afx_msg void OnCanConfigSMBusTX();
	afx_msg void OnUpdateCanConfig(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBcrSet(CCmdUI* pCmdUI);
	afx_msg void OnOvenWnd();
	afx_msg void OnUpdateOvenWnd(CCmdUI* pCmdUI);
	afx_msg void OnCanConfigTransmit();
	afx_msg void OnUpdateCanConfigTransmit(CCmdUI* pCmdUI);
	afx_msg void OnTelnetModule();
	afx_msg void OnPingModule();
	afx_msg void OnCmdRunUpdate();
	afx_msg void OnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButAnal();
	afx_msg void OnButEditor();
	afx_msg void OnBtnViewLog();
	afx_msg void OnButAllView();
	afx_msg void OnBtnPause();
	afx_msg void OnCmdSetChamberUsing();
	afx_msg void OnCmdReleaseChamberUsing();
	afx_msg void OnCmdChamberContinue();
	afx_msg void OnCmdCycleContinue();
	afx_msg void OnButStopBuzzer();
	afx_msg void OnButResetAlarm();
	afx_msg void OnButSetComm();
	afx_msg void OnButJigTag();
	afx_msg void OnUpdateOvenWnd2(CCmdUI* pCmdUI);
	afx_msg void OnOvenWnd2();
	afx_msg void OnWorkSTShow();
	afx_msg void OnBtnDaqIsolationStart();
	afx_msg void OnBtnDaqIsolationEnd();
	afx_msg void OnParallelCon();
	afx_msg void OnUpdateParallelCon(CCmdUI* pCmdUI);
	afx_msg void OnParallelDiscon();
	afx_msg void OnUpdateParallelDiscon(CCmdUI* pCmdUI);
	afx_msg void OnCmdIsolationEnd();
	afx_msg void OnBtnViewRealgraph();
	afx_msg void OnWorkPneManage();
	afx_msg void OnUartConfig();
	afx_msg void OnLinConfig();
	afx_msg void OnModbusConfig();
	afx_msg void OnButReconnectLoad1();
	afx_msg void OnButReconnectLoad2();
	afx_msg void OnCmdCham1Set();
	afx_msg void OnCmdCham2Set();
	afx_msg void OnButMuxChoice();
	afx_msg void OnCmdChInit();
	afx_msg void OnUpdateCmdChInit(CCmdUI* pCmdUI);
	afx_msg void OnGotoStep();
	afx_msg void OnUpdateGotoStep(CCmdUI* pCmdUI);
	afx_msg void OnButLoadChoice();
	afx_msg void OnCmdRunQueue();
	afx_msg void OnRunQueueView();
	afx_msg void OnBtnReserv();
	afx_msg void OnCmdSafetyCondUpade();
	afx_msg void OnCmdSchStepUpdate() ;
	afx_msg void OnUpdateIsolationEnable(CCmdUI* pCmdUI);
	afx_msg void OnCmdIsolationEnable();
	afx_msg void OnUpdateIsolationDisable(CCmdUI* pCmdUI);
	afx_msg void OnCmdIsolationDisable();
	afx_msg void OnCmdSchAllStepUpdate();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnBtnDaqIsolationEndA2();
	afx_msg void OnBtnDaqIsolationEndB2();
	afx_msg void OnBtnDaqIsolationStartAb2();
	afx_msg void OnBtnDaqIsolationEndA1();
	afx_msg void OnBtnDaqIsolationEndB1();
	afx_msg void OnBtnDaqIsolationStartAb1();
	afx_msg void OnButTest();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnButton7();
	afx_msg void OnCellbalSet();
	afx_msg void OnMenuWndPwr();
	afx_msg void OnUpdateMenuWndPwr(CCmdUI* pCmdUI);
	afx_msg void OnMenuWndPwr2();
	afx_msg void OnUpdateMenuWndPwr2(CCmdUI* pCmdUI);
	afx_msg void OnMenuWndChiller1();
	afx_msg void OnUpdateMenuWndChiller1(CCmdUI* pCmdUI);
	afx_msg void OnMenuWndChiller2();
	afx_msg void OnUpdateMenuWndChiller2(CCmdUI* pCmdUI);
	afx_msg void OnMenuWndChiller3();
	afx_msg void OnUpdateMenuWndChiller3(CCmdUI* pCmdUI);
	afx_msg void OnMenuWndChiller4();
	afx_msg void OnUpdateMenuWndChiller4(CCmdUI* pCmdUI);
	afx_msg void OnMenubarThtableSet();
	afx_msg void OnButton8();
	afx_msg void OnBtnEmg();
	afx_msg void OnUpdateCanLintocanRx(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCanLintocanTx(CCmdUI* pCmdUI);\
	
	afx_msg void OnUpdateRS485Rx(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRS485Tx(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSMBusRx(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSMBusTx(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUartConfig(CCmdUI* pCmdUI);
	afx_msg void OnBTNBuzzerStop();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	afx_msg LRESULT OnChannelDataReceived(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMSecChannelDataReceived(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleConnected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleDisconnected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleConnecting(WPARAM wParam, LPARAM lParam); //ksj 20201209
	afx_msg LRESULT OnSetCalResultData(WPARAM wParam, LPARAM LPARAM);
	afx_msg LRESULT OnModuleEmergency(WPARAM wParam, LPARAM LPARAM);
	afx_msg void OnGridSelectChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg LRESULT OnModuleBmsCommandReceived(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleBmsResultDataReceived(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnModuleMuxCommandReceived(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRTTableCommandReceived(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRTHumiTableCommandReceived(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnCmdReadyRun(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCmdReadyNgRun(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnSetStepInfoData(WPARAM wParam, LPARAM LPARAM);
	afx_msg LRESULT OnSetSafetyInfoData(WPARAM wParam, LPARAM LPARAM);
	afx_msg void OnNMDblclkListChannel(NMHDR *pNMHDR, LRESULT *pResult);
	// 210603 HKH 자동 업데이트 예약기능 //lyj 20210721 LGES 이식 s ================ //lyj 202210809타이머 사용으로 변경
	//afx_msg void OnAutoUpdaetReserve(); 
	//afx_msg void OnUpdateAutoUpdaetReserve(CCmdUI* pCmdUI);
	////BOOL CheckActiveGuiProcess();
	//BOOL CheckActiveGuiProcess(CString strPath); //lyj 20210722
	// 210603 HKH 자동 업데이트 예약기능 //lyj 20210721 LGES 이식 e ================
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedButCbankOn();
	afx_msg void OnBnClickedButCbankOff();
	afx_msg void OnCmdChill1TestSet();
	afx_msg void OnCmdChill2TestSet();
	afx_msg void OnCmdPump1TestSet();
	afx_msg void OnCmdPump2TestSet();
	void UpdateAuxSafetyCond(void);
	afx_msg void OnAuxVentSafetyConfig();
	afx_msg void OnFlickerConfig(); //lyj 20200824
	CString m_LabAuxTherHumiMax; //ksj 20200116
	CString m_LabAuxTherHumiMin; //ksj 20200116
	afx_msg void OnBnClickedBtnVentClear();
	afx_msg void OnBnClickedBtnVentOpen();
	afx_msg void OnBnClickedButtonTestbtn();
	CParallelConfigDlg* m_pParallelConfigDlg; //ksj 20200122

	
	BOOL m_bUseVentSafety; // ksj 20200212 : V1016 Vent 안전 기능 사용 여부
	BOOL m_bUseAuxH; //ksj 20200215 : Aux Humidity 사용 여부
	BOOL m_bUseCanSettingDlg; //ksj 20200215 : Can 설정 노출 여부
	BOOL m_bUseLinCanSettingDlg; //ksj 20200215 : LintoCan 설정 노출 여부
	BOOL m_bUseRS485SettingDlg; //ksj 20200215 : RS485 설정 노출 여부
	BOOL m_bUseSMBusSettingDlg; //ksj 20200215 : SMBus 설정 노출 여부
	BOOL m_bUseLinSettingDlg; //ksj 20200215 : Lin 설정 노출 여부
	BOOL m_bUseModbusSettingDlg; //ksj 20200215 : Modbus 설정 노출 여부
	BOOL m_bUseUartSettingDlg; //ksj 20200215 : UART 설정 노출 여부

	//BOOL m_bUseCanMux; //ksj 20201208: CAN Mux 기능 사용 여부  //ksj 20201223 : 기능 취소됨

	afx_msg void OnUpdateAuxVentSafetyConfig(CCmdUI *pCmdUI);
	// ksj 20200214 : 옵션에 따라 CTSMonProView내의 기능 활성 비활성화 
	int EnableFunctions(void);

	afx_msg void OnUpdateLinConfig(CCmdUI *pCmdUI);
	afx_msg void OnUpdateModbusConfig(CCmdUI *pCmdUI);
	CJsonMon* m_pJsonMon; //ksj 20200901 : [#JSonMon] JSON 모니터링 파일 생성 클래스 포인터 선언.
	int InitJsonMon(void); //ksj 20200901 : [#JSonMon] JSON 모니터링 클래스 초기화 함수.
	BOOL CheckNetworkError(int nModuleID, int nChannelIndex, int nChannelCode);
    BOOL ChannelCodeCheck(void);
	void InitPreFlickerInfo(void);
	static UINT ThreadSetFlickerColor(LPVOID pParam);
	BOOL m_bThreadSetFlickerColor;
	CWinThread* m_pThreadSetFlickerColor;
	BOOL m_bThreadTest;
	// ksj 20201112 : 모니터링시 표시하는 can,aux 값의 소수점 아래 자리 표시 최대 개수 옵션 추가
	int m_nMaxAuxFloatingPoint;
	int m_nMaxCanFloatingPoint;
	int UpdateUserConfig(void);
	afx_msg void OnUpdateCmdCham1Set(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCmdCham2Set(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCmdChill1TestSet(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCmdChill2TestSet(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCmdPump1TestSet(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCmdPump2TestSet(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCellbalSet(CCmdUI *pCmdUI);
	CXPButton m_btnIsoEndA1;
	CXPButton m_btnIsoEndB1;
	CXPButton m_btnIsoStartAB1;
	CXPButton m_btnIsoEndA2;
	CXPButton m_btnIsoEndB2;
	CXPButton m_btnIsoStartAB2;
	afx_msg void OnBnClickedBtnDaqIsolationStart2();
	afx_msg void OnBnClickedBtnDaqIsolationEnd2();
	CXPButton m_btnIsoCh1;
	CXPButton m_btnIsoCh2;
	CXPButton m_btnNIsoCh1;
	CXPButton m_btnNIsoCh2;
	//ksj 20201209
	CDlgConnectionSequence* m_pDlgConnSeq[SFT_MAX_MODULE_NUM];

	afx_msg void OnConnSeqLogView();
	afx_msg void OnUpdateConnSeqLogView(CCmdUI *pCmdUI);
	// 210603 HKH 자동 업데이트 예약기능   //lyj 20210721 LGES 이식
	void EnableExeBtn(BOOL bIsEnable_);
	afx_msg void OnButIsolChoice();	//lyj 20210913 
	CComboBox m_ctrlCboISOL;	//lyj 20210913
};

#ifndef _DEBUG  // debug version in CTSMonProView.cpp
inline CCTSMonProDoc* CCTSMonProView::GetDocument()
   { return (CCTSMonProDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
extern CCTSMonProView *mainView;
extern CBrush m_backBrush;
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSMonProVIEW_H__E478C60A_2580_4FB8_BCDC_0F8D28D8A258__INCLUDED_)
