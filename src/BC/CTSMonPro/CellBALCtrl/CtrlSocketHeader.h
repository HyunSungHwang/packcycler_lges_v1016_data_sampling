//-----------------------------------------------------
#define WM_REMOTE_COMMAND  		              WM_USER+5000
//-----------------------------------------------------
#define REMOTE_MAX_BUFFER_SIZE	              1024
#define LOCALE_MAX_BUFFER_SIZE	              2048
//#define MACHINE_MAX_SIZE	                  32
//-----------------------------------------------------
#define ID_SOCKET_STAT_NONE                   0
#define ID_SOCKET_STAT_CONN                   1
#define ID_SOCKET_STAT_STOP                   2
//client -> server-------------------------------------
#define P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST _T("001")   //body_size = 0
#define P1_CMD_TO_MACHINE_RESET				  _T("002")   //body_size = 0
#define P1_CMD_TO_MACHINE_OP_MODE_SET		  _T("003")   //body_size = 4
#define P1_CMD_TO_MACHINE_LOAD_MODE_SET		  _T("004")   //body_size = 24+40+60 = 124
#define P1_CMD_TO_MACHINE_RUN				  _T("005")   //body_size = 38
#define P1_CMD_TO_MACHINE_STOP				  _T("006")   //body_size = 38

#define P1_CMD_TO_MACHINE_PARALLEL_STATE	  _T("007")   //body_size = 36
#define P1_CMD_TO_MACHINE_FIRMWARE_VERSION_REQUEST	 _T("008") //body_size = 0

//server -> client-------------------------------------
#define P1_CMD_TO_PC_MODULE_INFO_REPLY		  _T("001")   //body_size = 24+24+40+60+(52*32) = 1812
#define P1_CMD_TO_PC_RESET_REPLY			  _T("002")   //body_size = 0
#define P1_CMD_TO_MACHINE_OP_MODE_SET_REPLY	  _T("003")   //body_size = 0
#define P1_CMD_TO_PC_LOAD_MODE_SET_REPLY	  _T("004")   //body_size = 0
#define P1_CMD_TO_PC_RUN_REPLY				  _T("005")   //body_size = 0
#define P1_CMD_TO_PC_STOP_REPLY				  _T("006")   //body_size = 0

#define P1_CMD_TO_PC_PARALLEL_STATE_REPLY	  _T("007")	  //body_size = 0
#define P1_CMD_TO_PC_FIRMWARE_VERSION_REPLY   _T("008")	  //body_size = 24
//header_size = 24-------------------------------------
typedef struct s_p1_cmd_header_tag
{
	char		direction;
				//"$" : pc to machine, "%" : machine to pc
	char		cmd_id[3];
				//"001"~"999"
	char		cmd_serial[4];
				//"0000"~"9999"
	char		body_size[4];
				//"0000"~"9999"
	char		cmd_response;
				//"0" : none, "1" : ok, "2"~ : error code
	char		reserved1[3];
				//"000"
	char		reserved2[4];
				//"0000"
} S_P1_CMD_HEADER;

typedef struct s_p1_ch_data_tag 
{
	char		ch_state;
				//"S" : STOP, "R" : RUN, "P" : PAUSE
				//"P" : PAUSE는 Balancing 진행 중 Release 조건에 의한 일시 정지임
				//STOP 명령을 수신하면 "P" -> "S"로 바뀜
				//"P"에서 Detection 조건에 의해 "R"로 전환될 수 있음
	char		reserved1;
				//"0"
	char		voltage[10];
				//"+3800000uV"
	char		current[10];
				//"-0300000uA"
	char		run_time[10];
				//"0003600sec"
	char		reserved2[10];
				//"0000000000"
	char		reserved3[10];
				//"0000000000"
} S_P1_CH_DATA;

typedef struct s_p1_cmd_body_module_info_reply_tag 
{
	char		op_state;
				//"S" : STOP, "R" : RUN
				//channel이 모두 STOP 이면 "S", 한개라도 RUN 이면 "R"
	char		op_mode;
				//"A" : auto operation (ch_cond 조건에 따라 동작)
				//"M" : manual operation (ch_code 조건 미적용,
				// 순수 P1_CMD_TO_MACHINE_RUN, _STOP 명령에 의한 동작)
	char		module_installed_ch[2];
				//"24" or "32"
	char		module_run_time[10];
				//"0003600sec" = 1시간이 지나면 전체 자동 STOP
				//"0000000sec" 이면 해당 조건 미적용
				//op_state = "S" 이면 refresh됨
	char		internal_temp[10];
				//"+0040.00dC"
	
	char		load_mode_set[2];
				//"CR"
				//reserved "CC", "CV", "CP"
	char		reserved2[2];
				//"00"
	char		load_mode_value[10];
				//"-0010000mR" = 10ohm, 가능범위 = 10ohm ~ 100ohm
				//reserved "-3000000uA", "+2000000uV", "-0015000mW"
				//reserved "-3000000uA", "+2000000uV", "-0015000mW"
	//char		reserved3[10];//kjg_181025						
				//"0000000000"	
				
	char		signal_sens_time[2];						
				//"15" = 1.5초 이상 조건이 만족하면 동작(RUN, PAUSE)					
	char		reserved3[8];						
				//"00000000"					
/*
	char		over_voltage[10];
				//"+4300000uV"*/ //yulee 20190207 Cell Balance
	char		availability_voltage_upper[10];
				//"+4000000uV" = 4.0V 이하일 경우에만 해당 채널 Balancing 가능

	char		under_voltage[10];
				//"+2400000uV" -> Balancer에서 기능 없음
	char		over_current[10];
				//"-0500000uA" -> Balancer에서 기능 없음
	char		over_temp[10];
				//"+0050.00dC" -> Balancer에서 기능 없음
/*	
	char		ch_cond_end_voltage[10];
				//"+3500000uV" = 3.5V 이상일 경우에만 해당 채널 Balancing 가능*/ //yulee 20190207 Cell Balance

	char		availability_voltage_lower[10];
				//"+3500000uV" = 3.5V 이상일 경우에만 해당 채널 Balancing 가능
				//"+0000000uV" or "-0000000uV" 이면 해당 조건 미적용

	char		ch_cond_end_current[10];
				//"-0010000uA" = 방전 10mA 미만일 경우 해당 채널 자동 STOP
	char		ch_cond_end_time[10];
				//"0003600sec" = 1시간이 지나면 해당 채널 자동 STOP
	char		ch_cond_detection[10];
				//"+0030000uV" = 30mV
				//(max - min 차이가 30mV 이상일 경우 Balancing 진행)
	char		ch_cond_release[10];
				//"+0020000uV" = 20mV
				//(max - min 차이가 20mV 미만일 경우 Balancing PAUSE)
	char		ch_cond_auto_stop_time[10];
				//모든 채널이 PAUSE인 상태가 ch_cond_auto_stop_time이 경과하는 동안
				// 유지되면 자동으로 STOP
				//"0000000sec" 이면 해당 조건 미적용
	
	S_P1_CH_DATA	ch_data[32];

} S_P1_CMD_BODY_MODULE_INFO_REPLY;

typedef struct s_p1_cmd_body_load_mode_set_tag 
{
	char		load_mode_set[2];
				//"CR"
				//reserved "CC", "CV", "CP"
	char		reserved1[2];
				//"00"
	char		load_mode_value[10];
				//"-0010000mR" = 10ohm, 가능범위 = 10ohm ~ 100ohm
				//reserved "-3000000uA", "+2000000uV", "-0015000mW"
				//reserved "-3000000uA", "+2000000uV", "-0015000mW"
	//char		reserved2[10];//kjg_181025				
				//"0000000000"		
	
	char		signal_sens_time[2];						
				//"15" = 1.5초 이상 조건이 만족하면 동작(RUN, PAUSE)					
	char		reserved2[8];						
				//"00000000"
	
/*
	char		over_voltage[10];
				//"+4300000uV"*/ //yulee 20190207 Cell Balance
	char		availability_voltage_upper[10];
				//"+4000000uV" = 4.0V 이하일 경우에만 해당 채널 Balancing 가능

	char		under_voltage[10];
				//"+2400000uV" -> Balancer에서 기능 없음
	char		over_current[10];
				//"-0500000uA" -> Balancer에서 기능 없음
	char		over_temp[10];
				//"+0050.00dC" -> Balancer에서 기능 없음
	
/*	
	char		ch_cond_end_voltage[10];
				//"+3500000uV" = 3.5V 이상일 경우에만 해당 채널 Balancing 가능*/ //yulee 20190207 Cell Balance

	char		availability_voltage_lower[10];
				//"+3500000uV" = 3.5V 이상일 경우에만 해당 채널 Balancing 가능
				//"+0000000uV" or "-0000000uV" 이면 해당 조건 미적용

	char		ch_cond_end_current[10];
				//"-0010000uA" = 방전 10mA 미만일 경우 해당 채널 자동 STOP
				//"+0000000uA" or "-0000000uA" 이면 해당 조건 미적용
	char		ch_cond_end_time[10];
				//"0003600sec" = 1시간이 지나면 해당 채널 자동 STOP
				//"0000000sec" 이면 해당 조건 미적용
				//ch_state = "S" 이면 refresh됨
	char		ch_cond_detection[10];
				//"+0030000uV" = 30mV
				//(max - min 차이가 30mV 이상일 경우 Balancing 진행)
	char		ch_cond_release[10];
				//"+0020000uV" = 20mV
				//(max - min 차이가 20mV 미만일 경우 Balancing PAUSE)
	char		ch_cond_auto_stop_time[10];
				//모든 채널이 PAUSE인 상태가 ch_cond_auto_stop_time이 경과하는 동안
				// 유지되면 자동으로 STOP
				//"0000000sec" 이면 해당 조건 미적용
} S_P1_CMD_BODY_LOAD_MODE_SET;

typedef struct s_p1_cmd_body_op_mode_set_tag 
{
	char		op_mode;
				//"A" : auto operation, "M" : manual operation
	char		reserved1[3];
				//"000"
} S_P1_CMD_BODY_OP_MODE_SET;

typedef struct s_p1_ch_flag_tag
{
	char		chFlag[32];
				//"0" : do not action, "1" : action
	char		reserved1[4];
				//"0000"
} S_P1_CH_FLAG;

typedef struct s_p1_cmd_body_run_tag 
{
	S_P1_CH_FLAG	flag;

} S_P1_CMD_BODY_RUN;

typedef struct s_p1_cmd_body_stop_tag 
{
	S_P1_CH_FLAG	flag;
	
} S_P1_CMD_BODY_STOP;




// typedef struct _REMOTE_CMD_HEADER
// {
// 	BYTE byCmdID;   //Command ID
// 	BYTE byDataNo;  //Current Continue_num (1024byte Split)
// 	BYTE byDataCnt; //Total Continue_num (1024byte Split)
// } ST_REMOTE_CMD_HEADER;

//#define REMOTE_MAX_BODY_SIZE	    1024
/*#define REMOTE_MAX_BUFFER_SIZE	    REMOTE_MAX_BODY_SIZE+12+10 //1024+12+@  (Body+Header+Reserved)
//----------------------------------
#define REMOTE_MAX_WORKSTART_BCR_SIZE 106
#define REMOTE_MAX_STEP_SIZE		  512
#define REMOTE_MAX_RESULT_PATH_SIZE   256
#define REMOTE_MAX_READ_BCR_SIZE      104
//----------------------------------
#define ID_RESULT_NG                9001
//----------------------------------
#define ID_DATA_MAXCHDATA           158
#define ID_DATA_MAXAUXCOUNT         10
#define ID_DATA_MAXCANCOUNT         20
//----------------------------------
#define REMOTE_STX_PNE_TO_REMOTE    0x02
#define REMOTE_STX_REMOTE_TO_PNE    0x08
#define REMOTE_ETX				    0x03
//----------------------------------
#define REMOTE_NACK				    0x00
#define REMOTE_ACK				    0x01
//----------------------------------
#define REMOTE_CMD_NG_PACKET_SIZE   0x1001 //Packet Size Error
#define REMOTE_CMD_NG_INVALID_CMD   0x1002 //Wrong Command Recv
#define REMOTE_CMD_NG_STX		    0x1003 //STX Error
#define REMOTE_CMD_NG_ETX		    0x1004 //ETX Error
#define REMOTE_CMD_NG_CRC16		    0x1005 //crc16 Error
//----------------------------------
#define REMOTE_CMD_STATUS_DATA		0x11  //PNE    -> REMOTE (ACK)
#define REMOTE_CMD_WORK_START		0x21  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_WORK_PAUSE		0x22  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_WORK_END			0x23  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_ISOLATION		0x24  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_WORK_CONTINUE 	0x25  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_SEQBX            0x31  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_3497             0x32  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_5520             0x33  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_PWR              0x34  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_BCR              0x35  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_BMS_RELAY        0x36  //REMOTE -> PNE    (ACK)
#define REMOTE_CMD_CLIENT_CLOSE		0x99  //socket client close
//----------------------------------
#define REMOTE_CMD_CLIENT_REMOVE    0x101
//----------------------------------
#define ID_SBC_CHSTATE_IDLE         0x0000
#define ID_SBC_CHSTATE_STANDBY      0x0001
#define ID_SBC_CHSTATE_RUN          0x0002
#define ID_SBC_CHSTATE_PAUSE        0x0003
#define ID_SBC_CHSTATE_LINE_OFF     0x0020
#define ID_SBC_CHSTATE_STOP         0x0025
#define ID_SBC_CHSTATE_END          0x0026
#define ID_SBC_CHSTATE_READY        0x0029
//----------------------------------
#define REMOTE_STEP_TYPE_CHARGE		0x01
#define REMOTE_STEP_TYPE_DISCHARGE	0x02
#define REMOTE_STEP_TYPE_REST		0x03
#define REMOTE_STEP_TYPE_OCV		0x04
#define REMOTE_STEP_TYPE_PAUSE		0x05
#define REMOTE_STEP_TYPE_DISCONNECT	0x06
//----------------------------------
#define REMOTE_STEP_MODE_CC			0x01
#define REMOTE_STEP_MODE_CV			0x02
#define REMOTE_STEP_MODE_CP			0x03
//----------------------------------
#define ID_SOCKET_STAT_NONE         0
#define ID_SOCKET_STAT_CONN         1
#define ID_SOCKET_STAT_RUN          2
#define ID_SOCKET_STAT_END          3
//----------------------------------
typedef struct _MySocket
{
	int     nItem;
	SOCKET	Socket;
	char    ipAddr[255];
	int     iPort;

	int     iDeviceNum;
	int     iState;
	LPVOID	wnd;
	HANDLE  hThread;
	HANDLE  hMutex; 
} MySocket;

typedef struct _REMOTE_CMD_HEADER
{
	BYTE byCmdID;   //Command ID
	BYTE byDataNo;  //Current Continue_num (1024byte Split)
	BYTE byDataCnt; //Total Continue_num (1024byte Split)
} ST_REMOTE_CMD_HEADER;

typedef struct _REMOTE_SCH_STEP_DATA
{
	USHORT nStepNum;
	UCHAR  chStepType;
	UCHAR  chMode;
	UINT   nRefVoltage;
	UINT   nRefCurrent;
	UINT   nRefPower;
	UINT   nRecordTime;
	UINT   nCutoffTime;
	UINT   nCutoffVoltage;
	UINT   nCutoffCurrent;
	UINT   cutoff_cond_Ah;
	UINT   nSafetyVoltageH;
	UINT   nSafetyVoltageL;
	UINT   nSafetyCurrentH;
	UINT   nSafetyCurrentL;
} ST_REMOTE_SCH_STEP_DATA;

typedef struct _REMOTE_SCHEDULE_DATA
{
	char   szBarcode[REMOTE_MAX_WORKSTART_BCR_SIZE];
	USHORT nChannelID;
	UINT   nStepSize; //Step Size	
	ST_REMOTE_SCH_STEP_DATA astStepData[REMOTE_MAX_STEP_SIZE]; 

} ST_REMOTE_SCHEDULE_DATA;

typedef struct _REMOTE_WORKSTART_RES
{
	USHORT  response;
	USHORT  channelno;
	char    szTestPath[REMOTE_MAX_RESULT_PATH_SIZE];

}ST_REMOTE_WORKSTART_RES;

typedef struct _REMOTE_WORKPAUSE_REQ
{
	USHORT  channelno;
	USHORT  reserved;

}ST_REMOTE_WORKPAUSE_REQ;

typedef struct _REMOTE_WORKPAUSE_RES
{
	USHORT  response;
	USHORT  channelno;

}ST_REMOTE_WORKPAUSE_RES;

typedef struct _REMOTE_WORKEND_REQ
{
	USHORT  channelno;
	USHORT  reserved;

}ST_REMOTE_WORKEND_REQ;

typedef struct _REMOTE_WORKEND_RES
{
	USHORT  response;
	USHORT  channelno;

}ST_REMOTE_WORKEND_RES;

typedef struct _REMOTE_WORKCONTINUE_REQ
{
	USHORT  channelno;
	USHORT  reserved;

}ST_REMOTE_WORKCONTINUE_REQ;

typedef struct _REMOTE_WORKCONTINUE_RES
{
	USHORT  response;
	USHORT  channelno;

}ST_REMOTE_WORKCONTINUE_RES;

typedef struct _REMOTE_ISOLATION_REQ
{
	USHORT  channelno;
	USHORT  command;

}ST_REMOTE_ISOLATION_REQ;

typedef struct _REMOTE_ISOLATION_RES
{
	USHORT  response;
	USHORT  channelno;

}ST_REMOTE_ISOLATION_RES;

typedef struct _REMOTE_SEQBX_REQ
{
	BYTE   deviceNum;
	BYTE   reserved1;
	USHORT reserved2;
	BYTE   command[8];

}ST_REMOTE_SEQBX_REQ;

typedef struct _REMOTE_SEQBX_RES
{	
	BYTE   response;
	BYTE   deviceNum;
	USHORT reserved1;
	BYTE   data[8];

}ST_REMOTE_SEQBX_RES;

typedef struct _REMOTE_3497_REQ
{
	BYTE   deviceNum;
	BYTE   type;
	USHORT channel;	

}ST_REMOTE_3497_REQ;

typedef struct _REMOTE_3497_RES
{
	BYTE   response;
	BYTE   deviceNum;
	USHORT reserved;
	FLOAT  data;

}ST_REMOTE_3497_RES;

typedef struct _REMOTE_5520_REQ
{
	BYTE   deviceNum;
	BYTE   reserved1;
	USHORT reserved2;
	USHORT ntime;
	USHORT nvolt;

}ST_REMOTE_5520_REQ;

typedef struct _REMOTE_5520_RES
{
	BYTE   response;
	BYTE   deviceNum;
	USHORT reserved;
	FLOAT  data;

}ST_REMOTE_5520_RES;

typedef struct _REMOTE_PWR_REQ
{
	BYTE   deviceNum;
	BYTE   command;
	USHORT nvalue;

}ST_REMOTE_PWR_REQ;

typedef struct _REMOTE_PWR_RES
{
	BYTE   response;
	BYTE   deviceNum;
	USHORT command;
	FLOAT  data1;
	FLOAT  data2;

}ST_REMOTE_PWR_RES;

typedef struct _REMOTE_BCR_REQ
{
	BYTE   deviceNum;
	BYTE   command;	
	USHORT reserved;	

}ST_REMOTE_BCR_REQ;

typedef struct _REMOTE_BCR_RES
{
	BYTE   response;
	BYTE   deviceNum;
	USHORT reserved[2];	
	BYTE   data[REMOTE_MAX_READ_BCR_SIZE];

}ST_REMOTE_BCR_RES;

typedef struct _REMOTE_BMSRELAY_REQ
{
	BYTE   deviceNum;
	BYTE   command;	
	BYTE   bmstype;	
	BYTE   reserved;	

}ST_REMOTE_BMSRELAY_REQ;

typedef struct _REMOTE_BMSRELAY_RES
{
	BYTE   response;
	BYTE   deviceNum;
	BYTE   relaystate;	
	BYTE   bmstype;	
	long   data;

}ST_REMOTE_BMSRELAY_RES;

typedef struct _REMOTE_WORK_START_PARAM
{
	UINT  nResult;
	UINT  nState;

	ST_REMOTE_SCHEDULE_DATA    pScheduleData;
	ST_REMOTE_WORKPAUSE_REQ    pWorkPause;
	ST_REMOTE_WORKEND_REQ      pWorkEnd;
	ST_REMOTE_WORKCONTINUE_REQ pWorkContinue;
	ST_REMOTE_ISOLATION_REQ    pIsolation;

	ST_REMOTE_SEQBX_REQ        pSeqbx;
	ST_REMOTE_3497_REQ         p3497;
	ST_REMOTE_5520_REQ         p5520;
	ST_REMOTE_PWR_REQ          pPwr;
	ST_REMOTE_BCR_REQ          pBcr;
	ST_REMOTE_BMSRELAY_REQ     pBmsRelay;

}ST_REMOTE_WORK_START_PARAM;

typedef struct _REMOTE_CH_DATA
{
	BYTE chNo; 
	BYTE chStepNO;
	BYTE chState;		
	BYTE chType;       
	BYTE chMode;
	BYTE chSaveSelect; //0: Monitoring :1 Step End
	BYTE chCode;
	BYTE reserved;
	ULONG ulSaveSeqNum;
	long lVoltage;
	long lCurrent;
	long lCapacity;
	long lWatt;
	long lWattHour;
	ULONG ulStepTime;
	ULONG ulTotalTime;
	long lAux[10];
	long lCan[20];

}ST_REMOTE_CH_DATA;
*/