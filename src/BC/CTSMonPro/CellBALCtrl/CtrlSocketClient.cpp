// CtrlSocketClient.cpp : 구현 파일입니다.
//

#include "stdafx.h" 
 

#include "CtrlSocketClient.h"

#define SD_RECEIVE      0x00
#define SD_SEND         0x01
#define SD_BOTH         0x02

#include "../CTSMonPro.h"
#include "../CTSMonProDoc.h"
#include "../CTSMonProView.h"

#include "../Global.h"

// CtrlSocketClient

CtrlSocketClient::CtrlSocketClient()
{
	m_pWnd=NULL;
	m_bInit=FALSE;
	m_iState=ID_SOCKET_STAT_NONE;
	m_iModuleInfoReqStop=0;

	m_clientSocket=INVALID_SOCKET;

	m_hClientThread=NULL;
	m_hDealThread=NULL;
	m_pChannel=NULL;
	m_iReConn=0;
	m_iSerialNo=0;
	m_nCHNO=0;

	m_strCellBal_IP   = _T("");
	m_strCellBal_PORT = _T("");
	m_iCellBal_USE    = 0;	

	memset(&m_cmdBodyModuleInfoReply,0,sizeof(m_cmdBodyModuleInfoReply));
}

CtrlSocketClient::~CtrlSocketClient()
{
}

// CtrlSocketClient 메시지 처리기입니다.
void CtrlSocketClient::onLogSave(CString type,CString log,int iType)
{	
	int nCHNO=onGetChannelNo();
	CString strLogType=onGetLogType();
	
	CString strLog;
	strLog.Format(_T("CH:%d %s"),nCHNO,log);

	log_All(strLogType,type,strLog);

	if(iType==1)
	{
		Fun_Log(strLog,type);
		return;
	}	
}

void CtrlSocketClient::onError(CString str)
{
	LPVOID lpMsgBuf;
	FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER|
		           FORMAT_MESSAGE_FROM_SYSTEM,
	           	   NULL, WSAGetLastError(),
		           MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		           (LPTSTR)&lpMsgBuf, 0, NULL);

	CString strError = (LPCTSTR)lpMsgBuf;
	LocalFree(lpMsgBuf);

	CString strTemp;
	strTemp.Format(_T("%s:%s"),str,strError);
	onLogSave(_T("error"),strTemp);
}

BOOL CtrlSocketClient::onInit()
{
	if(m_bInit==TRUE) return TRUE;

	WSADATA wsa;
	if(WSAStartup(MAKEWORD(2,2), &wsa) != 0)
	{
		onLogSave(_T("client"),_T("WS2_32.DLL Init Fail."));
		return m_bInit=FALSE;
	}
	return m_bInit=TRUE;
}

void CtrlSocketClient::onEnd()
{
	m_bInit=FALSE;
	WSACleanup();	
}

void CtrlSocketClient::onDealThreadStart()
{
	DWORD hDealThreadId=0;	
	m_hDealThread = CreateThread(NULL, 0, DealThread,
		                         (LPVOID)this, 0, &hDealThreadId);	   
}

BOOL CtrlSocketClient::onConnect(CWnd *pWnd,int iPort,CString strIpaddr)
{
	//CString strTemp;
	//strTemp.Format(_T("connect state:%d port:%d ip:%s"),m_iState,iPort,strIpaddr);
	//onLogSave(_T("client"),strTemp);

	if(pWnd) m_pWnd=pWnd;
	if(m_iState>ID_SOCKET_STAT_NONE) return TRUE;
	if(iPort<1)                      return FALSE;
	if(strIpaddr.IsEmpty() || strIpaddr==_T("")) return FALSE;

	m_clientSocket = socket(AF_INET, SOCK_STREAM, 0); // socket()
	if(m_clientSocket == INVALID_SOCKET)
	{
		onError(_T("socket"));
		return FALSE;
	}

	int option = FALSE;//NAGLE  on/off
	setsockopt( m_clientSocket,
				IPPROTO_TCP,
				TCP_NODELAY,
				(const char*)&option,
				sizeof(option));

	char ipaddr[255]={0,};

	#ifdef _UNICODE	
		int isize = WideCharToMultiByte(CP_ACP, 0, strIpaddr, -1, 0, 0, 0, 0)+1;	
		::memset(ipaddr,0,isize);//////////////////
		int iret=WideCharToMultiByte(CP_ACP,0,strIpaddr,strIpaddr.GetLength(),ipaddr,isize,NULL,NULL);
	#else
		strcpy(ipaddr,strIpaddr);
	#endif

	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(iPort);
	serveraddr.sin_addr.s_addr = inet_addr(ipaddr);
		
	int retval = connect(m_clientSocket, (SOCKADDR *)&serveraddr, sizeof(serveraddr)); //commented by ksj 20210824 : 이부분에서 접속 실패시 TCP 고갈 발생.
	if(retval == SOCKET_ERROR)
	{
		onError(_T("connect"));
		shutdown(m_clientSocket, SD_BOTH);  //ksj 20210824 : TCP 고갈 이슈 관련하여 shutdown 추가.
		closesocket(m_clientSocket); //ksj 20210824 : TCP 고갈 이슈 관련하여 close 추가.
		return FALSE;
	}	
	//---------------------------
	m_iState=ID_SOCKET_STAT_CONN;
	//---------------------------

	DWORD hClientThreadId=0;	
	m_hClientThread = CreateThread(NULL, 0, ClientThread,
		                          (LPVOID)this, 0, &hClientThreadId);

    if(m_hClientThread == NULL)
	{
		onLogSave(_T("client"),_T("thread start failed"),1);
		onCloseClientSocket();
		return FALSE;
	}
	onLogSave(_T("client"),_T("connect ok"),1);
	return TRUE;
}

void CtrlSocketClient::onCloseSocket(SOCKET &TempSocket)
{
	//shutdown(TempSocket,SD_BOTH); 
	//closesocket(TempSocket);

	DWORD code = ::shutdown( TempSocket, SD_BOTH ); // 연결종료신호 보내줌 
    if( code != SOCKET_ERROR )
	{// Wait for socket to fail (ie closed by other end) 
      fd_set readfds; 
      fd_set errorfds; 
      timeval timeout; 
      
	  FD_ZERO( &readfds ); 
      FD_ZERO( &errorfds ); 
      FD_SET( TempSocket, &readfds ); 
      FD_SET( TempSocket, &errorfds ); 
      timeout.tv_sec  = 0; 
      timeout.tv_usec = 0; 
      ::select( 1, &readfds, NULL, &errorfds, &timeout ); 
	}  
	code = ::closesocket( TempSocket ); 
    TempSocket = INVALID_SOCKET; 
}

void CtrlSocketClient::onCloseClientSocket()
{		
	if(m_iState==ID_SOCKET_STAT_NONE)  return;
	if(m_clientSocket==INVALID_SOCKET)
	{
		//-----------------------------
		m_iState=ID_SOCKET_STAT_NONE;
		//-----------------------------
		return;
	}
	m_iState=ID_SOCKET_STAT_NONE;
	GWin::win_ThreadEnd2(m_hClientThread);
	onCloseSocket(m_clientSocket);
	m_clientSocket=INVALID_SOCKET;
	memset(&m_cmdBodyModuleInfoReply,0,sizeof(m_cmdBodyModuleInfoReply));
	
	onLogSave(_T("client"),_T("client stop ok"),1);
}

int CtrlSocketClient::onGetChannelNo()
{		
	if(m_nCHNO>0) return m_nCHNO;

	if(m_pChannel)
	{
		m_nCHNO=m_pChannel->m_iCh;
	}	
	return m_nCHNO;
}

CString CtrlSocketClient::onGetLogType()
{
	int nCHNO=onGetChannelNo();
	
	CString strLogType;
	strLogType.Format(_T("cellbal_%d"),nCHNO);
    return strLogType;
}

DWORD WINAPI CtrlSocketClient::DealThread(LPVOID arg)
{
	CtrlSocketClient *wnd=(CtrlSocketClient *)arg;
	if(!wnd) return 0;

	Sleep(3000);
	
	CString strTemp;
	
	if(wnd->m_iCellBal_USE==1)
	{
		wnd->m_iReConn=1;
	}	
	
	strTemp.Format(_T("Deal Thread Start USE:%d(%d) %s:%s"),
		           wnd->m_iCellBal_USE,
				   wnd->m_iReConn,
				   wnd->m_strCellBal_IP,
				   wnd->m_strCellBal_PORT);
	wnd->onLogSave(_T("client"),strTemp);
	
	int nCount_10=0;//1second
	int nCount_5=0;//500milisecond
    
	while(1)
	{
		if(g_AppInfo.iEnd==1) break;

		////////////////////////////////////////////////////////////
		if(nCount_10>=10)
		{//1second
			nCount_10=0;
			//reconnect------------------------------
			if(wnd->m_iReConn==1)
			{				
				wnd->onConnect( wnd->m_pWnd,
					            _ttoi(wnd->m_strCellBal_PORT),
							    wnd->m_strCellBal_IP);
			}
			//get machine state---------------------
			if(wnd->m_iModuleInfoReqStop!=1)
			{
				wnd->onSendCommand(P1_CMD_TO_MACHINE_MODULE_INFO_REQUEST);
			}			
			//--------------------------------------		
		}
		////////////////////////////////////////////////////////////
		if(nCount_5>=5)
		{//500milisecond
			nCount_5=0;
		}
	    ////////////////////////////////////////////////////////////



		if(wnd->m_iState==ID_SOCKET_STAT_STOP)
		{
			wnd->onCloseClientSocket();
		}
		////////////////////////////////////////////////////////////
		nCount_10++;
		nCount_5++;
		Sleep(100);//100milisecond
	}

	strTemp.Format(_T("Deal Thread End"));
	wnd->onLogSave(_T("client"),strTemp);
	return 1;
}

//data receive
DWORD WINAPI CtrlSocketClient::ClientThread(LPVOID arg)
{
	CtrlSocketClient *wnd=(CtrlSocketClient *)arg;
	if(!wnd) return 0;
		
	DWORD   nLen=REMOTE_MAX_BUFFER_SIZE;	
	char    szMessage[REMOTE_MAX_BUFFER_SIZE];

	int     retval=0;
	CString strTemp,strMessage;

	FD_SET	SocketSet;
	struct	timeval	timeout;

	// Set the Time Out Value
	timeout.tv_sec = 0;	    // Seconds
	timeout.tv_usec = 0;	// Micro Seconds

	// Set the Socket Set
	SocketSet.fd_count = 1;
	SocketSet.fd_array[1] = wnd->m_clientSocket;
	
	strTemp.Format(_T("Client Thread Start"));
	wnd->onLogSave(_T("thread"),strTemp);	

	BOOL bRet=FALSE;
	
	while(1)
	{		
		if(wnd->m_clientSocket==INVALID_SOCKET) break;

		// Wait for the Timeout period for incoming data
		retval = select(0,&SocketSet,NULL,NULL,&timeout);
		if (retval != 0)
		{
			nLen=REMOTE_MAX_BUFFER_SIZE;
			bRet=wnd->Recv(wnd->m_clientSocket,szMessage,nLen);
			if(bRet==FALSE) break;
			
			for(int i=0;i<nLen;i++)
			{
				strTemp.Format(_T("%c"),(BYTE)szMessage[i]);
				if( szMessage[i]=='%' ||
					szMessage[i]=='$')
				{//Recv
					strMessage=strTemp;
				}
				else
				{
					strMessage+=strTemp;
					
					if(strMessage.Find(_T("!!!!"))>-1)
					{
						wnd->onRecvDeal(strMessage);
						strMessage=_T("");
					}
				}
			}
		}
		else
		{
			break;
		}
	}	
	wnd->m_iState=ID_SOCKET_STAT_STOP;
	strTemp.Format(_T("Client Thread End retval:%d bret:%d)"),retval,bRet);
	wnd->onLogSave(_T("thread"),strTemp);
	return 1;
}

int CtrlSocketClient::onChkHeader(char *pData,int nLen,S_P1_CMD_HEADER &cmdHeader)
{
	
	int nPos = 0;
	int nSize = 0;
   
	nSize=sizeof(cmdHeader);
	memset(&cmdHeader,0,nSize);
	memcpy(&cmdHeader,pData,nSize);
	nPos=nSize;

	if( cmdHeader.direction!='%' )
	{
		return -1;
	}
	CString str_body_size=mainGlobal.onGetCHAR(cmdHeader.body_size,4,0,_T(""));	
	nPos += atoi(str_body_size);

	char szChecksum[4]={0,};
	nSize=sizeof(szChecksum);
	memset(&szChecksum,0,nSize);
	memcpy(szChecksum,pData+nPos,nSize);
    CString strChecksum=mainGlobal.onGetCHAR(szChecksum,4,0,_T(""));	

	int iValue=onChksum(0,pData,nPos);//careful
	if(iValue!=atoi(strChecksum))
	{
		return -2;
	}
	nPos +=4;//checksum
	
	char szEnd[4]={0,};
	nSize=sizeof(szEnd);
	memset(&szEnd,0,nSize);
	memcpy(szEnd,pData+nPos,nSize);
	
	//int nCHNO=onGetChannelNo();
	//CString strLogType=onGetLogType();
	//CString strTemp;
	//strTemp.Format(_T("CHNO:%d %c%c%c%c bodysize:%s checksum:%d %s"),
	//	             nCHNO,szEnd[0],szEnd[1],szEnd[2],szEnd[3],str_body_size,
	//			     iValue,strChecksum);	
	//log_All(strLogType,_T("recv"),strTemp);

	if( szEnd[0]=='!' && 
		szEnd[1]=='!' && 
		szEnd[2]=='!' && 
		szEnd[3]=='!')
	{
		return 1;
	}
	return 0;
}

BOOL CtrlSocketClient::onRecvDeal(CString strData)
{	
	CString strTemp,strLog;

	int nCHNO=onGetChannelNo();
	CString strLogType=onGetLogType();
	//-----------------------------------------------
	strLog.Format(_T("CH:%d recv data:%s"),nCHNO,strData);	
	//-----------------------------------------------

	int nPos = 0;
	int nSize = 0;

	char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
	memset(szData,0,sizeof(szData));
	strcpy(szData,strData);
    nSize=strData.GetLength();

	//-------------------------------------
	S_P1_CMD_HEADER cmdHeader;
	int nRet=onChkHeader(szData,nSize,cmdHeader);	
	if(nRet!=1)
	{
		GLog::log_Save(strLogType,_T("recv"),strLog,NULL,TRUE);
		//---------------------------------------------------------
		strTemp.Format(_T("header error ret:%d"),nRet);
		onLogSave(_T("recv"),strTemp,1);	
		return FALSE;
	}
	//-------------------------------------
	CString str_cmd_id=mainGlobal.onGetCHAR(cmdHeader.cmd_id,3,0,_T(""));
	//-------------------------------------
	nPos=sizeof(S_P1_CMD_HEADER);
#ifdef _DEBUG
	//ksj 20200731 : 패킷 로그 남기기
	CString strLog2;
	strLog2.Format("Cmd ID : [%s] Packet(size:%d) string:", str_cmd_id, nSize);	
	for(int i=0;i<nSize;i++)
	{
		strTemp.Format("%c",szData[i]);
		strLog2 += strTemp;
	}
	GLog::log_Save("TEST",_T("recv"),strLog2,NULL,TRUE);
#endif

	if(str_cmd_id==P1_CMD_TO_PC_MODULE_INFO_REPLY)
	{
		if(g_AppInfo.iCellBalLog==1)
		{
			GLog::log_Save(strLogType,_T("recv"),strLog,NULL,TRUE);
		}
		//---------------------------------------------------------
		nSize=sizeof(m_cmdBodyModuleInfoReply);
		memset(&m_cmdBodyModuleInfoReply,0,nSize);
		memcpy(&m_cmdBodyModuleInfoReply,szData+nPos,nSize);
		return TRUE;
	}
	//---------------------------------------------------------
	GLog::log_Save(strLogType,_T("recv"),strLog,NULL,TRUE);
	//---------------------------------------------------------
	if(str_cmd_id==P1_CMD_TO_PC_RESET_REPLY)
	{//body_size = 0		
		strTemp.Format(_T("reset response:%c"),cmdHeader.cmd_response);
		onLogSave(_T("recv"),strTemp,1);
		return TRUE;
	}
	if(str_cmd_id==P1_CMD_TO_MACHINE_OP_MODE_SET_REPLY)
	{//body_size = 0
		strTemp.Format(_T("op mode set response:%c"),cmdHeader.cmd_response);
		onLogSave(_T("recv"),strTemp,1);
		return TRUE;
	}
	if(str_cmd_id==P1_CMD_TO_PC_LOAD_MODE_SET_REPLY)
	{//body_size = 0
		strTemp.Format(_T("load mode set response:%c"),cmdHeader.cmd_response);
		onLogSave(_T("recv"),strTemp,1);
		return TRUE;
	}
	if(str_cmd_id==P1_CMD_TO_PC_RUN_REPLY)
	{//body_size = 0
		strTemp.Format(_T("run response:%c"),cmdHeader.cmd_response);
		onLogSave(_T("recv"),strTemp,1);
		return TRUE;
	}
	if(str_cmd_id==P1_CMD_TO_PC_STOP_REPLY)
	{//body_size = 0
		strTemp.Format(_T("stop response:%c"),cmdHeader.cmd_response);
		onLogSave(_T("recv"),strTemp,1);
		return TRUE;
	}
	return FALSE;
}

BOOL CtrlSocketClient::onSend(char *pData,DWORD iLength)
{
	return Send(m_clientSocket,pData,iLength);
}

BOOL CtrlSocketClient::Send(SOCKET Socket,char *pData,DWORD iLength)
{	
	if(m_iState!=ID_SOCKET_STAT_CONN) return FALSE;
	if(Socket==INVALID_SOCKET)        return FALSE;

	BOOL flag=FALSE;
   
    int retval = send(Socket,pData,iLength,0);
	if(retval == 0 || retval == SOCKET_ERROR) flag=FALSE;
	else                                      flag=TRUE;

	return flag;
}

BOOL CtrlSocketClient::onRecv(char *pData,DWORD &iLength)
{
	return Recv(m_clientSocket,pData,iLength);
}

BOOL CtrlSocketClient::Recv(SOCKET Socket,char *pData,DWORD &iLength)
{	
	if(m_iState!=ID_SOCKET_STAT_CONN) return FALSE;	
	if(Socket==INVALID_SOCKET)        return FALSE;
	
	BOOL flag=FALSE;    
    memset(pData,0,iLength);

    int retval = recv(Socket,pData,iLength,0);
	iLength=retval;

	if(retval == 0 || retval == SOCKET_ERROR) flag=FALSE;
	else                                      flag=TRUE;
		
    return flag;
}

int CtrlSocketClient::onChksum(int iType,char *TSendData,int tsize)
{	
	int temp=0;	
	for(int i=0 ; i<tsize ; i++)
	{
		if(iType==1 && (i>=12 && i<=15)) continue;

		temp = temp + TSendData[i];
	}
    temp=(int)(temp%10000);	
	return temp;
}


int CtrlSocketClient::onSendCommand(CString strCmdID,int iresponse,UINT nSize,char *pData)
{
/*#ifdef _DEBUG //ksj 20201210: 디버깅용 브레이크 포인트
	if(strCmdID == P1_CMD_TO_MACHINE_RUN)
	{
		TRACE("P1_CMD_TO_MACHINE_RUN");
	}
#endif*/
	if(m_iState!=ID_SOCKET_STAT_CONN)  return 0;	
	if(m_clientSocket==INVALID_SOCKET) return 0;

	CString strTemp,strVal;
	S_P1_CMD_HEADER cmdHeader;
	memset(&cmdHeader,0,sizeof(cmdHeader));
	
	if(m_iSerialNo>9999)
	{
		m_iSerialNo=0;
	}	
	strCmdID=strCmdID.Mid(0,3);
	
	//client -> server	
	mainGlobal.onCopyChar(&cmdHeader.direction     ,_T("$")  ,1);	
 	mainGlobal.onCopyChar(cmdHeader.cmd_id         ,strCmdID ,3);

	strVal=GStr::str_IntToStr(m_iSerialNo,4);
	mainGlobal.onCopyChar(cmdHeader.cmd_serial     ,strVal ,4);

	strVal=GStr::str_IntToStr(nSize,4);
	mainGlobal.onCopyChar(cmdHeader.body_size      ,strVal ,4);
	
    strVal=GStr::str_IntToStr(iresponse);
	mainGlobal.onCopyChar(&cmdHeader.cmd_response ,strVal ,1);

	strVal=GStr::str_IntToStr(0,3);
	mainGlobal.onCopyChar(cmdHeader.reserved1     ,strVal ,3);

	strVal=GStr::str_IntToStr(0,4);
	mainGlobal.onCopyChar(cmdHeader.reserved2     ,strVal ,4);
	
	char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
	memset(szData,0,sizeof(szData));

	int nHeader_Len=sizeof(S_P1_CMD_HEADER);
	memcpy(szData,&cmdHeader,nHeader_Len);	
	int nTotal=nHeader_Len;	
	//----------------------------------
	if(nSize>0)
	{
		for(int i=0;i<nSize;i++)
		{
			szData[nTotal++]=pData[i];
		}
	}
	//----------------------------------
	int iValue=onChksum(0,szData,nTotal);//careful
	CString checksum=GStr::str_IntToStr(iValue,4);
	for(int iC=0;iC<4;iC++)
	{
		szData[nTotal++]=(char)checksum[iC];
	}
	//----------------------------------
	for(int iE=0;iE<4;iE++)
	{
		szData[nTotal++]='!';
	}
	//----------------------------------
 	CString strData=mainGlobal.onGetCHAR(szData,nTotal,0,_T(""));
 
	//strTemp.Format(_T("%s|%d|%d"),strValue,strValue.GetLength(),nHeader_Len);
	//AfxMessageBox(strTemp);

	BOOL bRet=onSend(szData,nTotal);
	int  nCHNO=onGetChannelNo();
	CString strLogType=onGetLogType();

	strTemp.Format(_T("CH:%d send data:%s ret:%d len:%d"),
		            nCHNO,strData,bRet,nTotal);
	//----------------------------------------------
	BOOL bLog=TRUE;
	if(strCmdID==P1_CMD_TO_PC_MODULE_INFO_REPLY)
	{
		bLog=g_AppInfo.iCellBalLog;
	}
	if(bLog==TRUE)
	{
		GLog::log_Save(strLogType,_T("send"),strTemp,NULL,TRUE);
	}	
	//----------------------------------
	m_iSerialNo++;

#ifdef _DEBUG
	//ksj 20200731 : 패킷 로그 남기기
	CString strLog;	
	strLog.Format("Cmd ID : [%s] Packet(size:%d) string: ", strCmdID, nTotal);	

	for(int i=0;i<nTotal;i++)
	{
		strTemp.Format("%c",szData[i]);
		strLog += strTemp;
	}
	GLog::log_Save("TEST",_T("send"),strLog,NULL,TRUE);
#endif

	return bRet;
}

int CtrlSocketClient::onGetMachine_ch_state(int ich)
{
	return m_cmdBodyModuleInfoReply.ch_data[ich].ch_state;
}

int CtrlSocketClient::onGetMachine_module_installed_ch()
{
	CString strTemp=mainGlobal.onGetCHAR(m_cmdBodyModuleInfoReply.module_installed_ch,2,0,_T(""));
	int module_installed_ch=atoi(strTemp);
	
	return module_installed_ch;
}


CString CtrlSocketClient::onGetStrMachine_ch_state(int ich)
{
	char ch_state=m_cmdBodyModuleInfoReply.ch_data[ich].ch_state;
	if(ch_state=='S') return _T("STOP");
	if(ch_state=='R') return _T("RUN");
	if(ch_state=='P') return _T("PAUSE");

	return _T("");
}

int CtrlSocketClient::onSetMachine_op_mode_set(char op_mode)
{
	S_P1_CMD_BODY_OP_MODE_SET cmdBodyOpModeSet;
	int nSize=sizeof(cmdBodyOpModeSet);
	memset(&cmdBodyOpModeSet,0,nSize);
	
	cmdBodyOpModeSet.op_mode=op_mode;
	
	mainGlobal.onCopyChar(cmdBodyOpModeSet.reserved1 ,GStr::str_IntToStr(0,3) ,3);

	char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
	memset(szData,0,sizeof(szData));
	memcpy(szData,&cmdBodyOpModeSet,nSize);
	
	int nRet=onSendCommand(P1_CMD_TO_MACHINE_OP_MODE_SET,0,nSize,szData);	

	int nCHNO=onGetChannelNo();
	CString strLogType=onGetLogType();

	CString strTemp;
	strTemp.Format(_T("CH:%d send op_mode:%c nret:%d"),
		           nCHNO,op_mode,nRet);	
	log_All(strLogType,_T("set"),strTemp);	
	return nRet;	
}

CString CtrlSocketClient::onGetStrMachine_ch_state()
{
	CString strVal=_T("");
	CString strTemp=_T("");
	for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
	{				
		strTemp.Format(_T("%c"),m_cmdBodyModuleInfoReply.ch_data[i].ch_state);
		strVal+=strTemp;
	}
	return strVal;
}

int CtrlSocketClient::onSetMachine_run_ch(int module_installed_ch,CString &strVal,int iType,int max_ch)
{
	strVal=_T("");
	if(module_installed_ch<1) return 0;
	
	CString strTemp=_T("");
	for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
	{
		strTemp=_T("0");
		//------------------------
		if(iType==0)
		{
			if(module_installed_ch>i)
			{
				strTemp=_T("1");
			}
		}
		else if(iType==1)
		{
			if(max_ch>i)
			{
				strTemp=_T("1");
			}
		}
		//------------------------
		strVal+=strTemp;
	}	
	return onSetMachine_run(module_installed_ch,strVal);
}

int CtrlSocketClient::onSetMachine_run(int module_installed_ch,CString strChState)
{
	if(module_installed_ch<1) return 0;
	
	CString strTemp=_T("");
	S_P1_CMD_BODY_RUN cmdBodyRun;
	memset(&cmdBodyRun,0,sizeof(S_P1_CMD_BODY_RUN));
	int nSize=sizeof(S_P1_CMD_BODY_RUN);
		
	mainGlobal.onCopyChar(cmdBodyRun.flag.chFlag,strChState,PS_MAX_CELLBAL_COUNT);
	
	char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
	memset(szData,0,sizeof(szData));
	memcpy(szData,&cmdBodyRun,nSize);
	
	int nRet=onSendCommand(P1_CMD_TO_MACHINE_RUN,0,nSize,szData);	
	
	int nCHNO=onGetChannelNo();	
	CString strLogType=onGetLogType();
	
	strTemp.Format(_T("CH:%d send run:%s nret:%d"),
		           nCHNO,strChState,nRet);
	log_All(strLogType,_T("run"),strTemp);
	return nRet;
}

int CtrlSocketClient::onSetMachine_stop_ch(int module_installed_ch,CString &strVal,int iType,int max_ch)
{
	strVal=_T("");
	if(module_installed_ch<1) return 0;
			
	CString strTemp=_T("");
	for(int i=0;i<PS_MAX_CELLBAL_COUNT;i++)
	{
		strTemp=_T("0");
		//------------------------
		if(iType==0)
		{			
			if(module_installed_ch>i)
			{
				strTemp=_T("1");
			}
		}
		else if(iType==1)
		{
			if(max_ch>i)
			{
				strTemp=_T("1");
			}
		}
		strVal+=strTemp;
		//------------------------
	}
	return onSetMachine_stop(module_installed_ch,strVal);
}


int CtrlSocketClient::onSetMachine_stop(int module_installed_ch,CString strChState)
{
	if(module_installed_ch<1) return 0;
	
	CString strTemp=_T("");
	S_P1_CMD_BODY_STOP cmdBodyStop;
	memset(&cmdBodyStop,0,sizeof(S_P1_CMD_BODY_STOP));
	int nSize=sizeof(S_P1_CMD_BODY_STOP);
				
	mainGlobal.onCopyChar(cmdBodyStop.flag.chFlag,strChState,PS_MAX_CELLBAL_COUNT);
				
	char szData[LOCALE_MAX_BUFFER_SIZE]={0,};
	memset(szData,0,sizeof(szData));
	memcpy(szData,&cmdBodyStop,nSize);
	
	int nRet=onSendCommand(P1_CMD_TO_MACHINE_STOP,0,nSize,szData);
	
	int nCHNO=onGetChannelNo();	
	CString strLogType=onGetLogType();
	
	strTemp.Format(_T("CH:%d send stop:%s nret:%d"),
					nCHNO,strChState,nRet);
	log_All(strLogType,_T("stop"),strTemp);	
	return nRet;
}