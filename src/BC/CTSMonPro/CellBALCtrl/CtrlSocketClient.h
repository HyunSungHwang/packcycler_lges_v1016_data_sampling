#pragma once

//#include <winsock2.h>
//#pragma comment(lib, "ws2_32.lib")

class CCyclerChannel;

// CtrlSocketClient

class CtrlSocketClient
{	
public:
	CtrlSocketClient();
	virtual ~CtrlSocketClient();
	
	BOOL    m_bInit;
	int     m_iState;
	int     m_iModuleInfoReqStop;
	SOCKET  m_clientSocket;
	CWnd    *m_pWnd;

	HANDLE  m_hClientThread;
	HANDLE  m_hDealThread;

	CCyclerChannel *m_pChannel;
	S_P1_CMD_BODY_MODULE_INFO_REPLY m_cmdBodyModuleInfoReply;

	int     m_iReConn;	
    long    m_iSerialNo;
    int     m_nCHNO;

	CString m_strCellBal_IP;
	CString m_strCellBal_PORT;
	int     m_iCellBal_USE;	
	
	BOOL onInit();
	void onEnd();
	BOOL onConnect(CWnd *pWnd,int iPort,CString strIpaddr);
	void onCloseClientSocket();
	void onCloseSocket(SOCKET &TempSocket);
	void onError(CString str);
	void onLogSave(CString type,CString log,int iType=0);
	void onDealThreadStart();

	BOOL  m_fReceiving,m_fSending;
	DWORD Receive(SOCKET Socket,char *pData,DWORD iLength);
	BOOL  Transmit(SOCKET MySocket,char *pData,DWORD dwLength);
	BOOL  onSend(char *pData,DWORD iLength);
    BOOL  Send(SOCKET Socket,char *pData,DWORD iLength);
	BOOL  onRecv(char *pData,DWORD &iLength);
	BOOL  Recv(SOCKET Socket,char *pData,DWORD &iLength);
	
	BOOL  onRecvDeal(CString strData);
	int   onChkHeader(char *pData,int nLen,S_P1_CMD_HEADER &cmdHeader);
	
	static DWORD WINAPI ClientThread(LPVOID arg);
	static DWORD WINAPI DealThread(LPVOID arg);

	int     onGetChannelNo();
	CString onGetLogType();
	int     onChksum(int iType,char *TSendData,int tsize);	
	int     onSendCommand(CString strCmdID,int iresponse=0,UINT nSize=0,char *pData=NULL);	

	int     onGetMachine_ch_state(int ich);
	CString onGetStrMachine_ch_state(int ich);
	int     onSetMachine_op_mode_set(char op_mode);
	CString onGetStrMachine_ch_state();
	int     onGetMachine_module_installed_ch();

	int     onSetMachine_run(int module_installed_ch,CString strChState);
	int     onSetMachine_stop(int module_installed_ch,CString strChState);
	int     onSetMachine_run_ch(int module_installed_ch,CString &strVal,int iType=0,int max_ch=0);
	int     onSetMachine_stop_ch(int module_installed_ch,CString &strVal,int iType=0,int max_ch=0);
};

