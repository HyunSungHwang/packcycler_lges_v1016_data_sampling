// CtrlSocketServer.cpp : 구현 파일입니다.
//

#include "stdafx.h" 
 

#include "CtrlSocketServer.h"

#if defined(JTP_SOCKET)
	#include "Dlg_Main.h"
#else
	#include "Dlg_MainTest.h"	
#endif
#include "Global.h"

// CtrlSocketServer

CtrlSocketServer::CtrlSocketServer()
{
	m_pWnd=NULL;
	m_bInit=FALSE;
	m_bStart=FALSE;
	m_iSendState=0;//Send Start
	m_bReceiving=FALSE;

	m_serverSocket=INVALID_SOCKET;
}

CtrlSocketServer::~CtrlSocketServer()
{
}

// CtrlSocketServer 메시지 처리기입니다.

void CtrlSocketServer::onLogSave(CString type,CString log,int iType)
{	
	log_All(_T("server"),type,log);

	if(iType==1)
	{
		Fun_Log(log,type);		
		return;
	}	
}

// 소켓 함수 오류 출력 후 종료
void CtrlSocketServer::onError(CString str)
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|
		          FORMAT_MESSAGE_FROM_SYSTEM,
			      NULL, WSAGetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				  (LPTSTR)&lpMsgBuf, 0, NULL);
	CString strError = (LPCTSTR)lpMsgBuf;
	LocalFree(lpMsgBuf);
	
	CString strTemp;
	strTemp.Format(_T("%s:%s"),str,strError);
	onLogSave(_T("server"),strTemp);
}

BOOL CtrlSocketServer::onInit()
{
	if(m_bInit==TRUE) return TRUE;
	
	WSADATA wsa;
	if(WSAStartup(MAKEWORD(2,2), &wsa) != 0)
	{
		onLogSave(_T("server"),_T("WS2_32.DLL Init Fail."));
		return m_bInit=FALSE;
	}

	return m_bInit=TRUE;
}

void CtrlSocketServer::onEnd()
{
	m_bInit=FALSE;
	WSACleanup();	
}

void  CtrlSocketServer::onSendMessage(int nCmd,WPARAM wParam,LPARAM lParam)
{
	if(!m_pWnd) return;
	
	m_pWnd->SendMessage(nCmd,wParam,lParam);
}

BOOL CtrlSocketServer::onOpen(CWnd *pWnd,int iPort)
{
	m_pWnd=pWnd;
	if(m_bStart==TRUE) return TRUE;
	
	m_serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(m_serverSocket == INVALID_SOCKET)
	{
		onError(_T("socket"));
		return FALSE;
	}
	
	int option = FALSE;//NAGLE  on/off
	setsockopt( m_serverSocket,
				IPPROTO_TCP,
				TCP_NODELAY,
				(const char*)&option,
				sizeof(option));

	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(iPort);
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	int retval = bind(m_serverSocket, (SOCKADDR *)&serveraddr, sizeof(serveraddr));
	if(retval == SOCKET_ERROR)
	{
		onError(_T("bind"));
		return FALSE;
	}

	retval = listen(m_serverSocket, SOMAXCONN);
	if(retval == SOCKET_ERROR)
	{
		onError(_T("listen"));
	    return FALSE;
	}
	//---------------------
	m_bStart=TRUE;
	//---------------------
	DWORD hServerThreadId=0;	
	m_hServerThread = CreateThread(NULL, 0, ServerThread,
		                          (LPVOID)this, 0, &hServerThreadId);
    if(m_hServerThread == NULL)
	{
		onLogSave(_T("server"),_T("thread start failed"),1);
		onCloseServerSocket();
		return FALSE;
	}
	onLogSave(_T("server"),_T("server start ok"),1);
	return TRUE;
}

void CtrlSocketServer::onCloseSocket(SOCKET &TempSocket)
{
	if(TempSocket==INVALID_SOCKET) return;
	
	//shutdown(TempSocket,SD_BOTH); 
	//closesocket(TempSocket);

	DWORD code = ::shutdown( TempSocket, SD_BOTH ); //Connect End Signal
	if( code != SOCKET_ERROR )
	{// Wait for socket to fail (IE closed by other end) 
		fd_set readfds; 
		fd_set errorfds; 
		timeval timeout; 

		FD_ZERO( &readfds ); 
		FD_ZERO( &errorfds ); 
		FD_SET( TempSocket, &readfds ); 
		FD_SET( TempSocket, &errorfds ); 
		timeout.tv_sec  = 0; 
		timeout.tv_usec = 0; 
		::select( 1, &readfds, NULL, &errorfds, &timeout ); 
	}  
	code = ::closesocket( TempSocket ); 
	TempSocket = INVALID_SOCKET; 
}

void CtrlSocketServer::onCloseServerSocket()
{	
	if(m_bStart==FALSE) return;
	if(m_serverSocket==INVALID_SOCKET) return;

	m_bStart=FALSE;//----------------	
	onClientListRemove(0);

	GWin::win_ThreadEnd2(m_hServerThread);
    onCloseSocket(m_serverSocket);

	m_hServerThread=NULL;
	m_serverSocket=INVALID_SOCKET;

	onLogSave(_T("server"),_T("server stop ok"),1);
}

// void CtrlSocketServer::win_ThreadEnd2(HANDLE &hThread,BOOL bTerm)
// {
// 	if(hThread!=INVALID_HANDLE_VALUE) 
// 	{	
// 		int   nCount=0;
// 		DWORD dwExitCode=0;
// 		DWORD dwReturn=0;
// 		CString strTemp;
// 
// 		while(nCount<100)
// 		{
// 			dwExitCode=0;
// 			GetExitCodeThread(hThread, &dwExitCode );
// 
// 			strTemp.Format(_T("count:%d thread exit code:%d"),nCount,dwExitCode);
// 			onLogSave(_T("server"),strTemp,1);
// 			
// 			if (dwExitCode != STILL_ACTIVE) break;
// 
// 			if(nCount>10 && bTerm==TRUE)
// 			{
// 				TerminateThread(hThread,0);	
// 				break;
// 			}
// 			dwReturn=::WaitForSingleObject( hThread, 100);
// 			if(dwReturn==WAIT_OBJECT_0) break;//Thread End
// 
// 			nCount++;
// 		}
// 		CloseHandle(hThread);
// 	}
// 	hThread=NULL;
// }

void CtrlSocketServer::onCloseClientSocket(MySocket *pMySocket)
{
	if(!pMySocket) return;
	if(pMySocket->Socket==INVALID_SOCKET) return;
	
	m_iSendState=1;//Send Stop
	pMySocket->iState=ID_SOCKET_STAT_NONE;

	GWin::win_ThreadEnd2(pMySocket->hThread);
	onCloseSocket(pMySocket->Socket);
	
	pMySocket->hThread=NULL;
	pMySocket->Socket=INVALID_SOCKET;
	m_iSendState=0;//Send Start

	CString strTemp;
	strTemp.Format(_T("close client item:%d ipaddr:%s port:%d"),
					pMySocket->nItem,
					GStr::str_CharToCString(pMySocket->ipAddr),
					pMySocket->iPort);
	onLogSave(_T("server"),strTemp,1);
}

int CtrlSocketServer::onClientListCount()
{
	int nCount=m_clientSocketList.GetSize();

	return nCount;
}

MySocket* CtrlSocketServer::onClientCreate()
{
	int nCount=m_clientSocketList.GetSize();	
	if(nCount>0)
	{
		POSITION pos = m_clientSocketList.GetHeadPosition();
		if(pos)
		{
			MySocket *pMySocket=NULL;
			for(int i=0;i<nCount;i++)
			{
				pMySocket=(MySocket*)m_clientSocketList.GetNext(pos);
				if(!pMySocket) continue;
				if(pMySocket->iState>0) continue;
				if(pMySocket->Socket!=INVALID_SOCKET) continue;

				memset(pMySocket->ipAddr,0,sizeof(pMySocket->ipAddr));
				return pMySocket;
			}
		}
	}
	MySocket *pMySocket=new MySocket;
	memset(pMySocket,0,sizeof(pMySocket));
	memset(pMySocket->ipAddr,0,sizeof(pMySocket->ipAddr));
	
	pMySocket->nItem=m_clientSocketList.GetSize()+1;
	m_clientSocketList.AddTail(pMySocket);//New
	return pMySocket;
}

int CtrlSocketServer::onClientList(CStringArray &ClientList)
{
	int nCount=m_clientSocketList.GetSize();
	ClientList.RemoveAll();

	int nConnectCount=0;
	CString strTemp;

	POSITION pos = m_clientSocketList.GetHeadPosition();
	if(pos)
	{
		MySocket *pMySocket=NULL;
		for(int i=0;i<nCount;i++)
		{
			pMySocket=(MySocket*)m_clientSocketList.GetNext(pos);
			if(!pMySocket) continue;

			if(pMySocket->iState==ID_SOCKET_STAT_RUN)
			{
				nConnectCount++;

				strTemp.Format(_T("connect ipaddr:%s port:%d"),
								GStr::str_CharToCString(pMySocket->ipAddr),
								pMySocket->iPort);
				ClientList.Add(strTemp);
			}
		}
	}
	return nConnectCount;
}

MySocket *CtrlSocketServer::onClientFind(int nItem)
{
	int nCount=m_clientSocketList.GetSize();
	if(nCount<1) return NULL;

	MySocket *pMySocket=NULL;
	POSITION pos1, pos2;

	for(pos1 = m_clientSocketList.GetHeadPosition(); (pos2 = pos1) != NULL;)
	{
		pMySocket = (MySocket*) m_clientSocketList.GetNext(pos1);
		if(!pMySocket) continue;

		if(pMySocket->nItem==nItem) return pMySocket;
	}
	return NULL;
}

void CtrlSocketServer::onClientListRemove(int iType,MySocket *mySocket)
{
	int nCount=m_clientSocketList.GetSize();
	if(nCount<1) return;

	MySocket *pMySocket=NULL;
	POSITION pos1, pos2;

	for(pos1 = m_clientSocketList.GetHeadPosition(); (pos2 = pos1) != NULL;)
	{
		pMySocket = (MySocket*) m_clientSocketList.GetNext(pos1);
		if(!pMySocket) continue;

		if(iType==0)
		{
			onCloseClientSocket(pMySocket);

			if(pMySocket)
			{
				delete pMySocket;
				pMySocket=NULL;
			}
			m_clientSocketList.RemoveAt(pos2);
		}
		else if(iType==1)
		{
			if( pMySocket==mySocket)
			{
				if(pMySocket)
				{
					delete pMySocket;
					pMySocket=NULL;
				}
				m_clientSocketList.RemoveAt(pos2);	
				break;
			}
		}
	}
	if(iType==0)
	{
		m_clientSocketList.RemoveAll();
	}
}

void CtrlSocketServer::onClientCloseList()
{
	int nCount=m_clientCloseList.GetCount();
	if(nCount<1) return;

	MySocket *pMySocket=NULL;
	POSITION pos1, pos2;

	for(pos1 = m_clientCloseList.GetHeadPosition(); (pos2 = pos1) != NULL;)
	{
		pMySocket = (MySocket*) m_clientCloseList.GetNext(pos1);
		if(!pMySocket) continue;

		onCloseClientSocket(pMySocket);
		m_clientCloseList.RemoveAt(pos2);
	}
}

DWORD WINAPI CtrlSocketServer::ServerThread(LPVOID arg)
{
	CtrlSocketServer *wnd=(CtrlSocketServer *)arg;
	if(!wnd) return 0;

	SOCKET      clientsock;
	SOCKADDR_IN clientaddr;	
	int         addrlen;
	CString     strTemp;
	
	while(1)
	{ 		
		if(g_AppInfo.iEnd==1)    break;
		if(wnd->m_bStart==FALSE) break;
		if(wnd->m_serverSocket==INVALID_SOCKET) break;
				
		addrlen = sizeof(clientaddr);//accept()
		//Blocking=>thread force stop=>TerminateThread
		clientsock = accept(wnd->m_serverSocket, (SOCKADDR *)&clientaddr, &addrlen);
		if(clientsock == INVALID_SOCKET)
		{
			wnd->onError(_T("accept"));
			break;
		}
		int nClient=wnd->onSetClient(clientsock,clientaddr);
		if(nClient<1)
		{
			wnd->onCloseSocket(clientsock);			
		}
	}

	strTemp.Format(_T("ServerThread end"));
	wnd->onLogSave(_T("server"),strTemp);	
	return 0;
}

CString CtrlSocketServer::onGetIPAddr()
{
	char name[255]={0,};
	CString ip=_T("");
    PHOSTENT hostinfo; 

	if(gethostname(name,sizeof(name))==0)
	{
	      if((hostinfo=gethostbyname(name))!=NULL) 
		  {
             ip=inet_ntoa(*(struct in_addr *)*hostinfo->h_addr_list); 
		  }
    }
	return ip;
}

CString CtrlSocketServer::onClientInfo(in_addr addr,int port)
{	
	char szAddr[255]={0,};
	strcpy(szAddr, inet_ntoa(addr));
	
	CString strIpAddr=GStr::str_CharToCString(szAddr);	
	
	CString strTemp;
    strTemp.Format(_T("connect client ipaddr:%s port:%d"), 
		  	        strIpAddr, port);
	onLogSave(_T("server"), strTemp,1);

	return strIpAddr;
}

int CtrlSocketServer::onSetClient(SOCKET sock,struct sockaddr_in client)
{
	int     iPort=client.sin_port;
	CString strIpAddr=onClientInfo(client.sin_addr,iPort);

	//if( strIpAddr==_T("127.0.0.1") ||
	//	  strIpAddr==GetIPAddr())
	//{
	//	onLogSave(_T("server"),_T("cannot use same PC"));		
	//	return 0;
	//}

	MySocket *pMySocket=onClientCreate();	
	GStr::str_CStringToChar(strIpAddr,pMySocket->ipAddr);
	pMySocket->Socket=sock;
	pMySocket->wnd=this;
	pMySocket->iPort=iPort;
	pMySocket->iState=ID_SOCKET_STAT_CONN;

	DWORD dwThreadId=0;
	pMySocket->hThread = CreateThread(NULL,0,ClientThread,
				                      (LPVOID)pMySocket,0,&dwThreadId);
	if(pMySocket->hThread == NULL)
	{
		onLogSave(_T("server"),_T("thread start failed."),1);
		onCloseClientSocket(pMySocket);
		return 0;
	}
	return 1;
}

DWORD WINAPI CtrlSocketServer::ClientThread(LPVOID arg)
{
    MySocket *pMySocket = (MySocket *)arg;
	if(!pMySocket) return 0;

	CtrlSocketServer *wnd=(CtrlSocketServer *)pMySocket->wnd;
	if(!wnd) return 0;

	int     nLen=REMOTE_MAX_BUFFER_SIZE;	
	char	szMessage[REMOTE_MAX_BUFFER_SIZE]={0,};
	
	int     retval;
	CString strTemp;

	FD_SET	SocketSet;
	struct	timeval	timeout;

	// Set the Time Out Value
	timeout.tv_sec = 0;	    // Seconds
	timeout.tv_usec = 0;	// Micro Seconds
	
	// Set the Socket Set
	SocketSet.fd_count = 1;
	SocketSet.fd_array[1] = pMySocket->Socket;
	
	pMySocket->iState=ID_SOCKET_STAT_RUN;
	CString strIP=GStr::str_CharToCString(pMySocket->ipAddr);
	
	GQueue mQueue;
	int    mQueue_Len=0;
	int    mQueue_Ret=0;
	BYTE   mQueue_Buff[RS232_BUFSIZE]={0,};

	while(1)
	{
		if(g_AppInfo.iEnd)       break;	
		if(wnd->m_bStart==FALSE) break;
		if(!pMySocket)           break;
		if(pMySocket->Socket==INVALID_SOCKET)     break;
		if(pMySocket->iState!=ID_SOCKET_STAT_RUN) break;

		// Wait for the Timeout period for incoming data
		retval = select(0,&SocketSet,NULL,NULL,&timeout);
		if (retval != 0)
		{
			nLen=REMOTE_MAX_BUFFER_SIZE;
			BOOL bflag=wnd->Recv(pMySocket->Socket,szMessage,nLen);
			if(bflag==FALSE) break;

			/*mQueue.PutByte((BYTE*)szMessage,nLen);
			mQueue_Len=mQueue.GetSize();
			mQueue_Ret=wnd->onChkHeader((char*)mQueue.buff,mQueue_Len);
			if(mQueue_Ret>0)
			{
				nLen=mQueue_Ret;
				memset(szMessage,0,sizeof(szMessage));
				memset(mQueue_Buff,0,sizeof(mQueue_Buff));
				
				memcpy(szMessage,mQueue.buff,nLen);
				if(mQueue_Len>nLen)
				{
					memcpy(mQueue_Buff,mQueue.buff+nLen,mQueue_Len-nLen);
					mQueue.Clear();
					mQueue.PutByte(mQueue_Buff,mQueue_Len-nLen);
				}
				wnd->onRecvDeal(pMySocket,szMessage,nLen);
			}*/

			wnd->onRecvDeal(pMySocket,szMessage,nLen);
		}
		else
		{
			break;
		}
	}
	strTemp.Format(_T("ClientThread end client item:%d ipaddr:%s port:%d"),
		           pMySocket->nItem,strIP,pMySocket->iPort);
	wnd->onLogSave(_T("server"),strTemp);
	//-----------------------------------------
	pMySocket->iState=ID_SOCKET_STAT_END;
	wnd->m_clientCloseList.AddTail(pMySocket);
	wnd->onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_CLIENT_CLOSE,0);
	return 0;
}

BOOL CtrlSocketServer::Send(SOCKET Socket,char *pData,int iLength)
{
	if(m_bStart==FALSE) return FALSE;
	if(Socket==INVALID_SOCKET) return FALSE;

	BOOL flag=FALSE;
	
	m_Lock.Lock();
    int retval = send(Socket,pData,iLength,0);	
	m_Lock.Unlock();

	if(retval == 0 || retval == SOCKET_ERROR) flag=FALSE;
	else                                      flag=TRUE;
		
	return flag;
}

BOOL CtrlSocketServer::Recv(SOCKET Socket,char *pData,int &iLength)
{
	if(m_bStart==FALSE) return FALSE;
	if(Socket==INVALID_SOCKET) return FALSE;

	BOOL flag=FALSE;
    memset(pData,0,iLength);

    int retval = recv(Socket,pData,iLength,0);	
	iLength=retval;

	if(retval == 0 || retval == SOCKET_ERROR) flag=FALSE;
	else                                      flag=TRUE;
		
    return flag;
}

// Receive Data
DWORD CtrlSocketServer::Receive(SOCKET Socket,char *pData,int iLength)
{
	// Overlapped Structure for Send
	WSAOVERLAPPED	olRecv;

	// Event Handling
	WSAEVENT	gheventOlSock;
	WSAEVENT	eventArray[2];

	// WSABuf for WSASend
	WSABUF		buffRecv;

	// WSASend Variables
	DWORD		dwRet,dwNumBytes,dwFlags;
	int			nWSAError;

	// Number of Bytes Received
	DWORD		iNumBytes = 0;

	// Error Variables
	CString		strError;

	// Create a Signaling Event for Send Completion
	gheventOlSock = WSACreateEvent();
	eventArray[0] = gheventOlSock;

	// Initialize the Overlapped Send Structure
	ZeroMemory(&olRecv,sizeof(WSAOVERLAPPED));

	// Set the Signaling Event for the Send Overlapped Structure
	olRecv.hEvent= gheventOlSock;

	// Set the Buffer Information
	buffRecv.len = iLength;
	buffRecv.buf = pData;

	// Set the Receiving Flag
	m_bReceiving = TRUE;

	// Loop Until All Data Received
	while (TRUE)
	{
		dwFlags = 0;
		dwNumBytes = 0;

		if ((dwRet = WSARecv(Socket,&buffRecv,1,&dwNumBytes,&dwFlags,&olRecv,NULL)) == SOCKET_ERROR) 
		{
			nWSAError = WSAGetLastError();
			if (nWSAError != ERROR_IO_PENDING)
			{
				strError.Format(_T("WSARecv failed with error %d"),nWSAError);
				onLogSave(_T("server"),strError);
				return SOCKET_ERROR;
			}
		}

		//  eventArray contains gheventOlSock. 
		//  gheventOlSock is set in olRecv only when no completion function is used. 
		//  When the overlapped recv completes, gheventOlSock is going to signal.
		//
		//  For overlapped recv with completion function, gheventOlSock is not set in 
		//  olSend. So gheventOlSock is not going to signal, however the last param of 
		//  WSAWaitForMultipleEvents is going to put this thread into an aler table wait state. 
		//  When overlapped Recv completes and completion
		//  function gets called, this Wait returns.

		if (WSAWaitForMultipleEvents(1,eventArray,FALSE,WSA_INFINITE,FALSE) == WSA_WAIT_FAILED) 
		{
			nWSAError = WSAGetLastError();
			strError.Format(_T("WSAWaitForMultipleEvents failed %d"), nWSAError);
			onLogSave(_T("server"),strError);
		} 

		// Reset the gheventOlSock
		WSAResetEvent(eventArray[0]);

		// Get the Number of Bytes Received
		if (WSAGetOverlappedResult(Socket,&olRecv,&dwNumBytes,FALSE,&dwFlags) == TRUE)
		{
			if (dwNumBytes == 0)
				break;
		} 
		else 
		{
			nWSAError = WSAGetLastError();
			strError.Format(_T("WSAGetOverlappedResult failed with error %d"), nWSAError);
			onLogSave(_T("server"),strError);
			return SOCKET_ERROR;
		}
		
		// An overlapped recv completes doesn't necessary mean all the data to recv are recv. 
		buffRecv.len -= dwNumBytes;
		iNumBytes = iNumBytes + dwNumBytes;
		if (buffRecv.len == 0) 
			break;
		else 
			buffRecv.buf += dwNumBytes;
	}

	// Set the Receiving Flag
	m_bReceiving = FALSE;

	// Return the Number of Bytes Received
	return iNumBytes;
}

// Transmit Data
BOOL CtrlSocketServer::Transmit(SOCKET MySocket,char *pData,int iLength)
{
	// Overlapped Structure for Send
	WSAOVERLAPPED	olSend;

	// Event Handling
	WSAEVENT	gheventOlSock;
	WSAEVENT	eventArray[2];

	// WSABuf for WSASend
	WSABUF		buffSend;

	// WSASend Variables
	DWORD		dwRet,dwNumBytes,dwFlags;
	int			nWSAError;

	// Error Variables
	CString strError;

	// Create a Signaling Event for Send Completion
	gheventOlSock = WSACreateEvent();
	eventArray[0] = gheventOlSock;

	// Initialize the Overlapped Send Structure
	ZeroMemory(&olSend, sizeof(WSAOVERLAPPED));

	// Set the Signaling Event for the Send Overlapped Structure
	olSend.hEvent= gheventOlSock;

	// Set the Buffer Information
	buffSend.len = iLength;
	buffSend.buf = pData;

	// Send in a Continuous until dwSendLen Bytes are Sent
	while (TRUE)  //we will send in a loop until nSendSize bytes are sent
	{
		if ((dwRet = WSASend(MySocket,&buffSend,1,&dwNumBytes,0,&olSend,NULL)) == SOCKET_ERROR)
		{
			// Expect IO Pending until all data is sent
			nWSAError= WSAGetLastError();
			if (nWSAError != ERROR_IO_PENDING)
			{
				strError.Format(_T("WSASend failed with error %d"),nWSAError);
				onLogSave(_T("server"),strError);
			}
		}

		//
		//  eventArray contains gheventOlSock.
		//  gheventOlSock is set in olSend only when no completion function is used.
		//  When the overlapped send completes, gheventOlSock is going to  signal.
		//
		//  For overlapped send with completion function, gheventOlSock is not set in
		//  olSend. So gheventOlSock is not going to signal, however the last param
		//  of WSAWaitForMultipleEvents is going to put this thread into an alertable
		//  wait state. When overlapped Send completes and completion
		//  function gets called, this Wait returns.
		//

		if (WSAWaitForMultipleEvents(1,eventArray,FALSE,WSA_INFINITE,FALSE) == WSA_WAIT_FAILED)
		{
			nWSAError= WSAGetLastError();
			strError.Format(_T("WSAWaitForMultipleEvents failed %d"),nWSAError);
			onLogSave(_T("server"),strError);
		} 

		//
		// Reset the gheventOlSock
		//
		WSAResetEvent(eventArray[0]);

		if (WSAGetOverlappedResult(MySocket,&olSend,&dwNumBytes,FALSE,&dwFlags) == FALSE)
		{
			nWSAError= WSAGetLastError();
			strError.Format(_T("WSAGetOverlappedResult failed with error %d"),nWSAError);
			onLogSave(_T("server"),strError);
		}

		// An overlapped write completes doesn't necessary mean all the data to send are sent. 
		buffSend.len -= dwNumBytes;
		if (buffSend.len == 0) 
			break;
		else 
			buffSend.buf += dwNumBytes;
	}

	// Close the Signaling Event
	WSACloseEvent(gheventOlSock);

	return TRUE;
}

int CtrlSocketServer::onChkHeader(char *pBuff,int nLen)
{	
	BYTE stx = 0;
	ST_REMOTE_CMD_HEADER stHeader;	
	ZeroMemory(&stHeader,sizeof(ST_REMOTE_CMD_HEADER));
	UINT dataLen = 0;
	BYTE bodyData[REMOTE_MAX_BODY_SIZE] = {0,};
	USHORT crc16 = 0;
	BYTE res = 0;
	BYTE etx = 0;

	//Parsing
	int nPos = 0;

	//stx
	memcpy(&stx,pBuff+nPos,sizeof(BYTE));
	nPos += sizeof(BYTE);

	if(	stx != REMOTE_STX_REMOTE_TO_PNE)
	{
		return -1;
	}

	//Header
	memcpy(&stHeader,pBuff+nPos,sizeof(ST_REMOTE_CMD_HEADER));
	nPos += sizeof(ST_REMOTE_CMD_HEADER);

	//Body Length
	memcpy(&dataLen,pBuff+nPos,sizeof(UINT));
	nPos += sizeof(UINT);

	//Body Length Copy
	if(dataLen == 0 || dataLen >= (UINT)(REMOTE_MAX_BODY_SIZE + nPos))
	{
		return -2;
	}
	memcpy(bodyData,pBuff+nPos,dataLen);
	nPos += dataLen;

	//crc16
	memcpy(&crc16,pBuff+nPos,sizeof(crc16));
	nPos += sizeof(crc16);

	//crc16 check
	if(g_AppInfo.iCheckCRC16==1)
	{
		USHORT crc16_comp = crc16_modbus((BYTE*)pBuff,nPos-sizeof(crc16));

		if(crc16_comp != crc16)
		{		
			return -3;
		}
	}

	//Reserved
	memcpy(&res,pBuff+nPos,sizeof(BYTE));
	nPos += sizeof(BYTE);

	//etx
	memcpy(&etx,pBuff+nPos,sizeof(BYTE));
	nPos += sizeof(BYTE);

	//etx Check
	if(	etx != REMOTE_ETX)
	{
		return -4;
	}
	return nPos;
}

BOOL CtrlSocketServer::onRecvDeal(MySocket *pMySocket,char *pBuff,int nLen)
{
	if(!pMySocket) return FALSE;
	if(!pBuff)     return FALSE;
	if(nLen<1)     return FALSE;

	CString strRecvData;
	CString strRecvDataHex=mainGlobal.onGetCHAR(pBuff,nLen,0,_T(" "),3);//HEX
	strRecvData.Format(_T("recvdata(len:%d):%s"),nLen,strRecvDataHex);

	CString strTemp;
	strTemp.Format(_T("recv client item:%d ipaddr:%s port:%d %s"),
		           pMySocket->nItem,
		           GStr::str_CharToCString(pMySocket->ipAddr),
				   pMySocket->iPort,
				   strRecvData);//Hex
	if(g_AppInfo.iSbcStateLog==1)
	{
		GLog::log_Save(_T("server_d"),_T("recv"),strTemp,NULL,TRUE);
	}
	BYTE stx = 0;
	ST_REMOTE_CMD_HEADER stHeader;	
	ZeroMemory(&stHeader,sizeof(ST_REMOTE_CMD_HEADER));
	UINT dataLen = 0;
	BYTE bodyData[REMOTE_MAX_BODY_SIZE] = {0,};
	USHORT crc16 = 0;
	BYTE res = 0;
	BYTE etx = 0;

	//Parsing
	int nPos = 0;

	//stx
	memcpy(&stx,pBuff+nPos,sizeof(BYTE));
	nPos += sizeof(BYTE);

	if(	stx != REMOTE_STX_REMOTE_TO_PNE)
	{
		onLogSave(_T("server"),_T("send REMOTE_CMD_NG_PACKET_SIZE ")+strRecvDataHex);	
		SendNACK(pMySocket,REMOTE_NACK,REMOTE_CMD_NG_STX);
		return FALSE;
	}
	
	//Header
	memcpy(&stHeader,pBuff+nPos,sizeof(ST_REMOTE_CMD_HEADER));
	nPos += sizeof(ST_REMOTE_CMD_HEADER);

	//Body Length
	memcpy(&dataLen,pBuff+nPos,sizeof(UINT));
	nPos += sizeof(UINT);

	//Body Length Copy
	if(dataLen == 0 || dataLen >= (UINT)(REMOTE_MAX_BODY_SIZE + nPos))
	{
		onLogSave(_T("server"),_T("send REMOTE_CMD_NG_PACKET_SIZE ")+strRecvDataHex);	
		SendNACK(pMySocket,stHeader.byCmdID, REMOTE_CMD_NG_PACKET_SIZE);
		return FALSE;
	}
	memcpy(bodyData,pBuff+nPos,dataLen);
	nPos += dataLen;

	//crc16
	memcpy(&crc16,pBuff+nPos,sizeof(crc16));
	nPos += sizeof(crc16);

	//crc16 check
	if(g_AppInfo.iCheckCRC16==1)
	{
		USHORT crc16_comp = crc16_modbus((BYTE*)pBuff,nPos-sizeof(crc16));
	
		if(crc16_comp != crc16)
		{		
			strTemp.Format(_T("send REMOTE_CMD_NG_CRC16 recv: 0x%02x calc : 0x%02x %s"),
				           crc16,crc16_comp,
						   strRecvDataHex);
			onLogSave(_T("server"),strTemp);	

			SendNACK(pMySocket,stHeader.byCmdID, REMOTE_CMD_NG_CRC16);
			return FALSE;
		}
	}

	//Reserved
	memcpy(&res,pBuff+nPos,sizeof(BYTE));
	nPos += sizeof(BYTE);

	//etx
	memcpy(&etx,pBuff+nPos,sizeof(BYTE));
	nPos += sizeof(BYTE);
	
	//etx Check
	if(	etx != REMOTE_ETX)
	{
		onLogSave(_T("server"),_T("send REMOTE_CMD_NG_ETX ")+strRecvDataHex);
		SendNACK(pMySocket,stHeader.byCmdID, REMOTE_CMD_NG_ETX);		
		return FALSE;
	}

	//----------------------------------------------
	if(stHeader.byCmdID==REMOTE_CMD_STATUS_DATA)
	{
		return TRUE;
	}
	//----------------------------------------------
	CString strBodyHex=mainGlobal.onGetBYTE(bodyData,dataLen,0,_T(" "),3);//HEX
	
	strTemp.Format(_T("recv client no:%d ipaddr:%s port:%d cmdid : 0x%02x, packsize:%d, bodydata(len:%d):%s %s"),
		           pMySocket->nItem,
		           GStr::str_CharToCString(pMySocket->ipAddr),
				   pMySocket->iPort,
				   stHeader.byCmdID, nPos, 
				   dataLen,strBodyHex,
				   strRecvDataHex);
	onLogSave(_T("server"),strTemp);
	////////////////////////////////////////////////////////////////////
	CString strValue;
	BYTE szData[512]={0,}; 
	UINT nNGOK = 0;
	UINT nSize = 0;
	UINT nResult = 0;
	ST_REMOTE_WORK_START_PARAM param;
	memset(&param,0,sizeof(ST_REMOTE_WORK_START_PARAM));

	switch(stHeader.byCmdID)
	{
		case REMOTE_NACK:
		{
			nSize = sizeof(UINT);
			memcpy(&nNGOK,bodyData,sizeof(UINT));

			strTemp.Format(_T("recv REMOTE_NACK cmd id : 0x%02x, response:%u"),
				          stHeader.byCmdID, nNGOK);
			onLogSave(_T("server"),strTemp);
			return TRUE;//-----------------------------
			break;
		}
		case REMOTE_ACK:
		{
			nSize = sizeof(UINT);
			memcpy(&nNGOK,bodyData,sizeof(UINT));

			strTemp.Format(_T("recv REMOTE_NACK cmd id : 0x%02x, response:%u"),
				          stHeader.byCmdID, nNGOK);
			onLogSave(_T("server"),strTemp);
			return TRUE;//-----------------------------
			break;
		}
		case REMOTE_CMD_STATUS_DATA:
		{
			nSize = sizeof(UINT);
			memcpy(&nNGOK,bodyData,sizeof(UINT));

			strTemp.Format(_T("recv REMOTE_CMD_STATUS_DATA cmd id : 0x%02x, response:%u"),
				           stHeader.byCmdID, dataLen, nNGOK);
			if(g_AppInfo.iSbcStateLog==1)
			{
				GLog::log_Save(_T("server_d"),_T("recv"),strTemp,NULL,TRUE);
			}
			return TRUE;//-----------------------------
			break;
		}
		case REMOTE_CMD_WORK_START:
		{
			g_AppInfo.strSbcPath=_T("");
			nResult = onCmdWorkStart(bodyData, dataLen, param);
	
			strTemp.Format(_T("recv REMOTE_CMD_WORK_START cmd id : 0x%02x response:%u \
sbc channel:%d path:%s"),
			stHeader.byCmdID, nResult,
			param.pScheduleData.nChannelID,
			g_AppInfo.strSbcPath);
			onLogSave(_T("server"),strTemp);
			//-----------------------------
			if(nResult==ID_RESULT_NG) nResult=0;//NG

			ST_REMOTE_WORKSTART_RES pWorkStartRes;
			ZeroMemory(&pWorkStartRes,sizeof(pWorkStartRes));

			//Response
			nSize = sizeof(pWorkStartRes);
			pWorkStartRes.response=nResult;
			pWorkStartRes.channelno=param.pScheduleData.nChannelID;
			GStr::str_CStringToChar(g_AppInfo.strSbcPath,pWorkStartRes.szTestPath);

			memcpy(szData,&pWorkStartRes,nSize);
			break;
		}
		case REMOTE_CMD_WORK_PAUSE:
		{
			nResult = onCmdWorkPause(bodyData, dataLen, param);
			
			strTemp.Format(_T("recv REMOTE_CMD_WORK_PAUSE cmd id : 0x%02x response:%u \
channel:%d"),
			stHeader.byCmdID, nResult,
			param.pWorkPause.channelno);
			onLogSave(_T("server"),strTemp);
			//-----------------------------
			if(nResult==ID_RESULT_NG) nResult=0;//NG

			//Response
			ST_REMOTE_WORKPAUSE_RES pWorkPauseRes;
			ZeroMemory(&pWorkPauseRes,sizeof(pWorkPauseRes));

			nSize = sizeof(ST_REMOTE_WORKPAUSE_RES);
			pWorkPauseRes.response=nResult;
			pWorkPauseRes.channelno=param.pWorkPause.channelno;

			memcpy(szData,&pWorkPauseRes,nSize);
			break;
		}
		case REMOTE_CMD_WORK_END:
		{
			nResult = onCmdWorkEnd(bodyData, dataLen, param);
			
			strTemp.Format(_T("recv REMOTE_CMD_WORK_END cmd id : 0x%02x response:%u \
channel:%d"),
			stHeader.byCmdID, nResult,
			param.pWorkEnd.channelno);
			onLogSave(_T("server"),strTemp);	
			//-----------------------------
			if(nResult==ID_RESULT_NG) nResult=0;//NG

			//Response
			ST_REMOTE_WORKEND_RES pWorkEndRes;
			ZeroMemory(&pWorkEndRes,sizeof(pWorkEndRes));

			nSize = sizeof(ST_REMOTE_WORKEND_RES);
			pWorkEndRes.response=nResult;
			pWorkEndRes.channelno=param.pWorkEnd.channelno;

			memcpy(szData,&pWorkEndRes,nSize);
			break;
		}
		case REMOTE_CMD_WORK_CONTINUE:
		{
			nResult = onCmdWorkContinue(bodyData, dataLen, param);
			
			strTemp.Format(_T("recv REMOTE_CMD_WORK_CONTINUE cmd id : 0x%02x response:%u \
channel:%d"),
			stHeader.byCmdID, nResult,
			param.pWorkContinue.channelno);
			onLogSave(_T("server"),strTemp);	
			//-----------------------------
			if(nResult==ID_RESULT_NG) nResult=0;//NG

			//Response
			ST_REMOTE_WORKCONTINUE_RES pWorkContinueRes;
			ZeroMemory(&pWorkContinueRes,sizeof(pWorkContinueRes));

			nSize = sizeof(ST_REMOTE_WORKCONTINUE_RES);
			pWorkContinueRes.response=nResult;
			pWorkContinueRes.channelno=param.pWorkEnd.channelno;

			memcpy(szData,&pWorkContinueRes,nSize);
			break;
		}
		case REMOTE_CMD_ISOLATION: 
		{
			nResult = onCmdWorkIsolation(bodyData, dataLen, param);
		
			strTemp.Format(_T("recv REMOTE_CMD_ISOLATION cmd id : 0x%02x response:%u \
command:%d channelno:%d"),
			stHeader.byCmdID, nResult,
			param.pIsolation.command,
			param.pIsolation.channelno);
			onLogSave(_T("server"),strTemp);
			//-----------------------------
			if(nResult<1) return TRUE;//Wait
			if(nResult==ID_RESULT_NG) nResult=0;//NG

			//Response
			ST_REMOTE_ISOLATION_RES pIsolationRes;
			ZeroMemory(&pIsolationRes,sizeof(pIsolationRes));

			nSize = sizeof(ST_REMOTE_ISOLATION_RES);
			pIsolationRes.response=nResult;
			pIsolationRes.channelno=param.pIsolation.channelno;

			memcpy(szData,&pIsolationRes,nSize);
			break;
		}
		case REMOTE_CMD_SEQBX: 
		{
			nResult = onCmdSeqBox(pMySocket,bodyData, dataLen, param);

			strValue=mainGlobal.onGetBYTE(param.pSeqbx.command,8,0,_T(""),4);//STR
			strTemp.Format(_T("recv REMOTE_CMD_SEQBX cmd id : 0x%02x response:%u \
devicenum:%d command:%s"),
			stHeader.byCmdID, nResult,
			param.pSeqbx.deviceNum,
			strValue);
			onLogSave(_T("server"),strTemp);
			//-----------------------------
			if(nResult<1) return TRUE;//Wait
		
			//Response
			ST_REMOTE_SEQBX_RES seqbxRes;
			ZeroMemory(&seqbxRes,sizeof(ST_REMOTE_SEQBX_RES));
			seqbxRes.response=nResult;
			seqbxRes.deviceNum=param.pSeqbx.deviceNum;

			nSize = sizeof(ST_REMOTE_SEQBX_RES);
			memcpy(szData,&seqbxRes,nSize);
			break;
		}
		case REMOTE_CMD_3497: 
		{
			nResult = onCmd3497(pMySocket,bodyData, dataLen, param);

			strTemp.Format(_T("recv REMOTE_CMD_3497 cmd id : 0x%02x response:%u \
devicenum:%d type:%d channel:%d"),
			stHeader.byCmdID, nResult,
			param.p3497.deviceNum,
			param.p3497.type,
			param.p3497.channel);
			onLogSave(_T("server"),strTemp);	
			//-----------------------------
			if(nResult<1) return TRUE;//Wait
			
			//Response
			ST_REMOTE_3497_RES p3497Res;
			ZeroMemory(&p3497Res,sizeof(ST_REMOTE_3497_RES));
			p3497Res.response=nResult;
			p3497Res.deviceNum=param.p3497.deviceNum;

			nSize = sizeof(ST_REMOTE_3497_RES);
			memcpy(szData,&p3497Res,nSize);
			break;
		}
		case REMOTE_CMD_5520: 
		{
			nResult = onCmd5520(pMySocket,bodyData, dataLen, param);

			strTemp.Format(_T("recv REMOTE_CMD_5520 cmd id : 0x%02x response:%u \
devicenum:%d time:%d volt:%d"),
			stHeader.byCmdID, nResult,
			param.p5520.deviceNum,
			param.p5520.ntime,
			param.p5520.nvolt);
			onLogSave(_T("server"),strTemp);	
			//-----------------------------
			if(nResult<1) return TRUE;//Wait
			if(nResult==ID_RESULT_NG) nResult=0;//NG

			//Response
			ST_REMOTE_5520_RES p5520Res;
			ZeroMemory(&p5520Res,sizeof(ST_REMOTE_5520_RES));
			p5520Res.response=nResult;
			p5520Res.deviceNum=param.p5520.deviceNum;

			nSize = sizeof(ST_REMOTE_5520_RES);
			memcpy(szData,&p5520Res,nSize);
			break;
		}
		case REMOTE_CMD_PWR: 
		{
			nResult = onCmdPwr(pMySocket,bodyData, dataLen, param);

			strTemp.Format(_T("recv REMOTE_CMD_PWR cmd id : 0x%02x response:%u \
devicenum:%d command:%d setvalue:%d"),
			stHeader.byCmdID, nResult,
			param.pPwr.deviceNum,
			param.pPwr.command,
			param.pPwr.nvalue);
			onLogSave(_T("server"),strTemp);	
			//-----------------------------
			if(nResult<1) return TRUE;//Wait

			//Response
			ST_REMOTE_PWR_RES pwrRes;
			ZeroMemory(&pwrRes,sizeof(ST_REMOTE_PWR_RES));
			pwrRes.response=nResult;
			pwrRes.deviceNum=param.pPwr.deviceNum;

			nSize = sizeof(ST_REMOTE_PWR_RES);
			memcpy(szData,&pwrRes,nSize);
			break;
		}
		case REMOTE_CMD_BCR: 
		{
			nResult = onCmdBcr(pMySocket,bodyData, dataLen, param);
			
			strTemp.Format(_T("recv REMOTE_CMD_BCR cmd id : 0x%02x response:%u \
devicenum:%d command:%d"),
			stHeader.byCmdID, nResult,
			param.pBcr.deviceNum,
			param.pBcr.command);
			onLogSave(_T("server"),strTemp);	
			//-----------------------------
			if(nResult<1) return TRUE;//Wait

			//Response
			ST_REMOTE_BCR_RES bcrRes;
			ZeroMemory(&bcrRes,sizeof(ST_REMOTE_BCR_RES));
			bcrRes.response=nResult;
			bcrRes.deviceNum=param.pBcr.deviceNum;

			nSize = sizeof(ST_REMOTE_BCR_RES);
			memcpy(szData,&bcrRes,nSize);
			break;
		}
		case REMOTE_CMD_BMS_RELAY: 
		{
			nResult = onCmdBmsRelay(pMySocket,bodyData, dataLen, param);
			
			strTemp.Format(_T("recv REMOTE_CMD_BMS_RELAY cmd id : 0x%02x response:%u \
devicenum:%d command:%d bmstype:%d"),
			stHeader.byCmdID, nResult,
			param.pBmsRelay.deviceNum,
			param.pBmsRelay.command,
			param.pBmsRelay.bmstype);
			onLogSave(_T("server"),strTemp);	
			//-----------------------------
			if(nResult<1) return TRUE;//Wait

			//Response
			ST_REMOTE_BMSRELAY_RES bmsrelayRes;
			ZeroMemory(&bmsrelayRes,sizeof(ST_REMOTE_BMSRELAY_RES));
			bmsrelayRes.response=nResult;
			bmsrelayRes.deviceNum=param.pBmsRelay.deviceNum;

			nSize = sizeof(ST_REMOTE_BMSRELAY_RES);
			memcpy(szData,&bmsrelayRes,nSize);
			break;
		}
		default:
		{
			onLogSave(_T("server"),_T("send REMOTE_CMD_NG_INVALID_CMD"));	
			SendNACK(pMySocket,REMOTE_NACK, REMOTE_CMD_NG_INVALID_CMD);
			return TRUE;//-----------------------------
			break;
		}
	}
	return SendCommand(pMySocket,stHeader.byCmdID,1,1,nSize,szData);
}

UINT CtrlSocketServer::onCmdWorkStart(BYTE* pData, UINT nDataLen, ST_REMOTE_WORK_START_PARAM &param)
{
	//Data Length Check
	//Step Data Length = Data Len - BARCODE Len - Channel No
	int nStepLen = nDataLen - REMOTE_MAX_WORKSTART_BCR_SIZE - sizeof(USHORT); 

	if(nStepLen%sizeof(ST_REMOTE_SCH_STEP_DATA) != 0)
	{
		return REMOTE_CMD_NG_PACKET_SIZE; 
		//Step Data Mismatch => Data Loss, Data Split
	}
	UINT nStepSize = nStepLen/sizeof(ST_REMOTE_SCH_STEP_DATA); //Step Count

	//////////////////////
	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));	
	ZeroMemory(&param.pScheduleData,sizeof(ST_REMOTE_SCHEDULE_DATA));
	
	int nPos = 0;
	//BARCODE Copy
	memcpy(param.pScheduleData.szBarcode, pData, sizeof(param.pScheduleData.szBarcode));
	nPos += sizeof(param.pScheduleData.szBarcode);
	
	//ch_num
	memcpy(&param.pScheduleData.nChannelID, pData+nPos, sizeof(USHORT));
	nPos += sizeof(USHORT);
	
	//Step Count
	param.pScheduleData.nStepSize = nStepSize;
	
	//Step Copy
	for(UINT nStep=0; nStep<nStepSize && nStep<REMOTE_MAX_STEP_SIZE; nStep++)
	{
		memcpy(&param.pScheduleData.astStepData[nStep],pData+nPos,sizeof(ST_REMOTE_SCH_STEP_DATA));
		nPos += sizeof(ST_REMOTE_SCH_STEP_DATA);
	}
	param.nState=REMOTE_CMD_WORK_START;
	param.nResult=1;//NG
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_WORK_START,(LPARAM)&param);
	param.nState=0;
	return param.nResult;
}

UINT CtrlSocketServer::onCmdWorkPause(BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_WORKPAUSE_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pWorkPause,sizeof(ST_REMOTE_WORKPAUSE_REQ));

	memcpy(&param.pWorkPause,pData,sizeof(param.pWorkPause));

	param.nResult=1;//NG
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_WORK_PAUSE,(LPARAM)&param);
	return param.nResult;
}

UINT CtrlSocketServer::onCmdWorkEnd(BYTE* pData, UINT nDataLen, ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_WORKEND_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pWorkEnd,sizeof(ST_REMOTE_WORKEND_REQ));

	memcpy(&param.pWorkEnd,pData,sizeof(param.pWorkEnd));
	
	param.nResult=1;//NG
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_WORK_END,(LPARAM)&param);
	return param.nResult;
}

UINT CtrlSocketServer::onCmdWorkContinue(BYTE* pData, UINT nDataLen, ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_WORKCONTINUE_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pWorkContinue,sizeof(ST_REMOTE_WORKCONTINUE_REQ));

	memcpy(&param.pWorkContinue,pData,sizeof(param.pWorkContinue));

	param.nResult=1;//NG
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_WORK_CONTINUE,(LPARAM)&param);
	return param.nResult;
}

UINT CtrlSocketServer::onCmdWorkIsolation(BYTE* pData, UINT nDataLen, ST_REMOTE_WORK_START_PARAM &param)
{
 	if(nDataLen != sizeof(UINT))
	{
		return REMOTE_CMD_NG_PACKET_SIZE; 
	}
	int nIsolation = 0;
	memcpy(&nIsolation, pData, nDataLen);

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pIsolation,sizeof(ST_REMOTE_ISOLATION_REQ));
	
	memcpy(&param.pIsolation,pData,sizeof(param.pIsolation));
	
	param.nResult=0;//Wait
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_ISOLATION,(LPARAM)&param);
	return param.nResult;
}

UINT CtrlSocketServer::onCmdSeqBox(MySocket *pMySocket, BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_SEQBX_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pSeqbx,sizeof(ST_REMOTE_SEQBX_REQ));

	memcpy(&param.pSeqbx,pData,sizeof(param.pSeqbx));
 	pMySocket->iDeviceNum=param.pSeqbx.deviceNum;

	param.nResult=0;//Wait
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_SEQBX,(LPARAM)&param);
	return param.nResult;
}

void CtrlSocketServer::onResBcr(int iDeviceNum,BYTE iResponse,BYTE* pData, UINT nDataLen)
{
	ST_REMOTE_BCR_RES pBcrRes;
	ZeroMemory(&pBcrRes,sizeof(ST_REMOTE_BCR_RES));

	pBcrRes.response=iResponse;//0-OK,1-Comm Error
	pBcrRes.deviceNum=iDeviceNum;
	memcpy(pBcrRes.data,pData,nDataLen);

	CString strTemp;
	strTemp.Format(_T("bcr response :%d devicenum:%d barcode:%s"),
		           iResponse,iDeviceNum,GStr::str_CharToCString((char*)pData));
	onLogSave(_T("bcr"),strTemp,1);

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_BCR,1,1,sizeof(ST_REMOTE_BCR_RES),(BYTE*)&pBcrRes,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(iDeviceNum,pBuffer,nLen);
	}
	if(pBuffer) delete []pBuffer;
}

void CtrlSocketServer::onResSeqBox(int iDeviceNum,BYTE iResponse,BYTE* pData, UINT nDataLen)
{
	ST_REMOTE_SEQBX_RES pSeqboxRes;
	ZeroMemory(&pSeqboxRes,sizeof(ST_REMOTE_SEQBX_RES));

	pSeqboxRes.response=iResponse;//0-OK,1-Comm Error
	pSeqboxRes.deviceNum=iDeviceNum;
	memcpy(pSeqboxRes.data,pData,nDataLen);

	CString strTemp;
	strTemp.Format(_T("insp response :%d devicenum:%d data:%s"),
		           iResponse,iDeviceNum,GStr::str_CharToCString((char*)pData));
	onLogSave(_T("insp"),strTemp,1);

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_SEQBX,1,1,sizeof(ST_REMOTE_SEQBX_RES),(BYTE*)&pSeqboxRes,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(iDeviceNum,pBuffer,nLen);
	}
	if(pBuffer) delete []pBuffer;
}

UINT CtrlSocketServer::onCmd3497(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_3497_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.p3497,sizeof(ST_REMOTE_3497_REQ));

	memcpy(&param.p3497,pData,sizeof(param.p3497));
	pMySocket->iDeviceNum=param.p3497.deviceNum;

	param.nResult=0;//Wait
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_3497,(LPARAM)&param);
	return param.nResult;
}

void CtrlSocketServer::onRes3497(int iDeviceNum,BYTE iResponse,float fValue)
{
	ST_REMOTE_3497_RES p3497Res;
	ZeroMemory(&p3497Res,sizeof(ST_REMOTE_3497_RES));

	p3497Res.response=iResponse;//0-OK,1-Comm Error
	p3497Res.deviceNum=iDeviceNum;
	p3497Res.data=fValue;

	CString strTemp;
	strTemp.Format(_T("3497 response :%d devicenum:%d data:%f"),
		           iResponse,iDeviceNum,
				   fValue);
	onLogSave(_T("3497"),strTemp,1);

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_3497,1,1,sizeof(ST_REMOTE_3497_RES),(BYTE*)&p3497Res,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(iDeviceNum,pBuffer,nLen);	
	}
	if(pBuffer) delete []pBuffer;
}

UINT CtrlSocketServer::onCmd5520(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_5520_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.p5520,sizeof(ST_REMOTE_5520_REQ));

	memcpy(&param.p5520,pData,sizeof(param.p5520));
	pMySocket->iDeviceNum=param.p5520.deviceNum;

	param.nResult=0;//Wait
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_5520,(LPARAM)&param);
	return param.nResult;
}

void CtrlSocketServer::onRes5520(int iDeviceNum,BYTE iResponse,float fValue)
{
	ST_REMOTE_5520_RES p5520Res;
	ZeroMemory(&p5520Res,sizeof(ST_REMOTE_5520_RES));

	p5520Res.response=iResponse;//0-OK,1-Comm Error
	p5520Res.deviceNum=iDeviceNum;
	p5520Res.data=fValue;

	CString strTemp;
	strTemp.Format(_T("5520 response :%d devicenum:%d data:%f"),
		           iResponse,iDeviceNum,
				   fValue);
	onLogSave(_T("5520"),strTemp,1);

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_5520,1,1,sizeof(ST_REMOTE_5520_RES),(BYTE*)&p5520Res,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(iDeviceNum,pBuffer,nLen);	
	}
	if(pBuffer) delete []pBuffer;
}

UINT CtrlSocketServer::onCmdPwr(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_PWR_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pPwr,sizeof(ST_REMOTE_PWR_REQ));
	
	memcpy(&param.pPwr,pData,sizeof(param.pPwr));
	pMySocket->iDeviceNum=param.pPwr.deviceNum;

	param.nResult=0;//Wait
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_PWR,(LPARAM)&param);
	return param.nResult;
}

void CtrlSocketServer::onResPwr(int iDeviceNum,BYTE iResponse,USHORT iCommand,float fVolt,float fCurr)
{
	ST_REMOTE_PWR_RES pPwrRes;
	ZeroMemory(&pPwrRes,sizeof(ST_REMOTE_PWR_RES));

	pPwrRes.response=iResponse;//0-OK,1-Comm Error
	pPwrRes.deviceNum=iDeviceNum;
	pPwrRes.command=iCommand;
	pPwrRes.data1=fVolt;
	pPwrRes.data2=fCurr;

	CString strTemp;
	strTemp.Format(_T("pwr response :%d devicenum:%d command:%d volt:%f current:%f"),
		           iResponse,iDeviceNum,iCommand,fVolt,fCurr);
	onLogSave(_T("pwr"),strTemp,1);

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_PWR,1,1,sizeof(ST_REMOTE_PWR_RES),(BYTE*)&pPwrRes,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(iDeviceNum,pBuffer,nLen);	
	}
	if(pBuffer) delete []pBuffer;
}

void CtrlSocketServer::onResBmsRelay(int iDeviceNum,BYTE iResponse,BYTE iRelayState,BYTE iBmsType,long lData)
{
	ST_REMOTE_BMSRELAY_RES pBmsRelayRes;
	ZeroMemory(&pBmsRelayRes,sizeof(ST_REMOTE_BMSRELAY_RES));

	pBmsRelayRes.response=iResponse;//0-OK,1-Comm Error
	pBmsRelayRes.deviceNum=iDeviceNum;
	pBmsRelayRes.relaystate=iRelayState;
	pBmsRelayRes.bmstype=iBmsType;
	pBmsRelayRes.data=lData;

	CString strTemp;
	strTemp.Format(_T("bms_relay response :%d devicenum:%d relaystate:%d bmstype:%d data:%ld"),
		           iResponse,iDeviceNum,iRelayState,iBmsType,lData);
	onLogSave(_T("bms"),strTemp,1);

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_BMS_RELAY,1,1,sizeof(ST_REMOTE_PWR_RES),(BYTE*)&pBmsRelayRes,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(iDeviceNum,pBuffer,nLen);	
	}
	if(pBuffer) delete []pBuffer;
}

void CtrlSocketServer::onResIsolation(int iChannelNo,USHORT iResponse)
{
	ST_REMOTE_ISOLATION_RES pIsolationRes;
	ZeroMemory(&pIsolationRes,sizeof(ST_REMOTE_ISOLATION_RES));

	pIsolationRes.response=iResponse;
	pIsolationRes.channelno=iChannelNo;
	
	CString strTemp;
	strTemp.Format(_T("isolation response :%d channelno:%d"),
		           iResponse,iChannelNo);
	onLogSave(_T("isolation"),strTemp,1);

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_ISOLATION,1,1,sizeof(ST_REMOTE_ISOLATION_RES),(BYTE*)&pIsolationRes,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(0,pBuffer,nLen);
	}
	if(pBuffer) delete []pBuffer;
}

UINT CtrlSocketServer::onCmdBcr(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_BCR_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pBcr,sizeof(ST_REMOTE_BCR_REQ));

	memcpy(&param.pBcr,pData,sizeof(param.pBcr));
	pMySocket->iDeviceNum=param.pBcr.deviceNum;

	param.nResult=0;//Wait
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_BCR,(LPARAM)&param);
	return param.nResult;
}

UINT CtrlSocketServer::onCmdBmsRelay(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param)
{
	if(nDataLen != sizeof(ST_REMOTE_BMSRELAY_REQ))
	{
		return REMOTE_CMD_NG_PACKET_SIZE;
	}

	ZeroMemory(&param,sizeof(ST_REMOTE_WORK_START_PARAM));
	ZeroMemory(&param.pBmsRelay,sizeof(ST_REMOTE_BMSRELAY_REQ));

	memcpy(&param.pBmsRelay,pData,sizeof(param.pBmsRelay));
	pMySocket->iDeviceNum=param.pBmsRelay.deviceNum;

	param.nResult=0;//Wait
	onSendMessage(WM_REMOTE_COMMAND, REMOTE_CMD_BMS_RELAY,(LPARAM)&param);
	return param.nResult;
}

void CtrlSocketServer::onResNACK(BYTE byCmdID, UINT nCode)
{
	BYTE buff[256]={0,};
	memset(buff,0,sizeof(buff));

	UINT nSize=sizeof(UINT);
	memcpy(buff,&nCode,sizeof(UINT));

	UINT nLen=0;
	BYTE *pBuffer=GetCommand(byCmdID,1,1,nSize,buff,nLen);
	if(pBuffer && nLen>0)
	{
		onClientSend(0,pBuffer,nLen);
	}
	if(pBuffer) delete []pBuffer;
}

void CtrlSocketServer::onSendSbcAllChState()
{
	if(m_bStart==FALSE)       return;
	if(onClientListCount()<1) return;

	CCyclerModule *pMD=NULL;
	CCyclerChannel *lpChannelInfo=NULL;
	int iModuleID=0;
	int nChannelID=0;

	for ( UINT nI = 0; nI < (UINT)theApp.GetInstallModuleCount(); nI++ )
	{
		if(g_AppInfo.iEnd==1) break;

		pMD = theApp.GetModuleInfo(theApp.GetModuleID(nI));
		if(pMD == NULL) return;

		int iModuleID=pMD->GetModuleID();
	
		for(UINT ch =0; ch<pMD->GetTotalChannel(); ch++)
		{
			if(g_AppInfo.iEnd==1) break;

			CCyclerChannel *lpChannelInfo = pMD->GetChannelInfo(ch);
			if(lpChannelInfo == NULL) break;

			nChannelID=lpChannelInfo->GetChannelIndex();
			if(onSendSbcChState(iModuleID,nChannelID)==FALSE) break;
		}
	}
}

BOOL CtrlSocketServer::onChkSbcChState()
{
	if(m_bStart==FALSE)    return FALSE;//Socket Init Failed
	if(m_iSendState==1)    return FALSE;//Send Stop

	return TRUE;
}

BOOL CtrlSocketServer::onSendSbcChState(int nModuleID,int nChannelID)
{	
	//---------------------------------------------
	if(onChkSbcChState()==FALSE) return FALSE;
	//---------------------------------------------

	ST_REMOTE_CH_DATA stChannelData;
	ZeroMemory(&stChannelData,sizeof(ST_REMOTE_CH_DATA));
	
	CCyclerModule* pModule = theApp.GetModuleInfo(nModuleID);
	if(!pModule) return FALSE;

	CCyclerChannel* pChannel = pModule->GetChannelInfo(nChannelID);
	if(!pChannel) return FALSE;

	int iMODSTATE=pModule->GetState();

	//-------------------------------------------------
	stChannelData.chNo         = pChannel->GetChannelIndex() + 1;
	stChannelData.chStepNO     = (BYTE)pChannel->GetStepNo();
	stChannelData.chState      = theApp.onGetCHState(iMODSTATE,pChannel->GetState());
	stChannelData.chType       = (BYTE)pChannel->GetStepType();
	stChannelData.chMode       = theApp.onGetStepMode(pChannel->GetScheduleData(),pChannel->GetStepNo());	
	stChannelData.chSaveSelect = 0; //Monitoring
	stChannelData.chCode       = (BYTE)pChannel->GetCellCode();
	stChannelData.reserved     = 0;
	stChannelData.ulSaveSeqNum = pChannel->GetChannel()->GetSaveSequence();
	stChannelData.lVoltage     = pChannel->GetVoltage()/1000;//mV
	stChannelData.lCurrent     = pChannel->GetCurrent()/1000;//mA
	stChannelData.lCapacity    = (pChannel->GetChargeAh()+pChannel->GetDisChargeAh())/1000;//mAh	
	stChannelData.lWatt        = pChannel->GetWatt()/1000;//mAh
	stChannelData.lWattHour    = (pChannel->GetChargeWh()+pChannel->GetDisChargeWh())/1000;//mWh
	stChannelData.ulStepTime   = pChannel->GetStepTime();
	stChannelData.ulTotalTime  = pChannel->GetStepTime();

	CString strTemp;
	strTemp.Format(_T("volt:%ld current:%ld"),
						stChannelData.lVoltage,
						stChannelData.lCurrent);
	if(g_AppInfo.iSbcStateLog==1)
	{
		GLog::log_Save(_T("server_d"),_T("send"),strTemp,NULL,TRUE);
	}
	
	//aux data
	int nAuxCnt = pChannel->GetAuxCount();
	SFT_AUX_DATA* pAuxData = pChannel->GetAuxData();
	if(pAuxData && nAuxCnt > 0)
	{
		for(int nAux=0; nAux < nAuxCnt && nAux < 10; nAux++)
		{
			float fValue = mainGlobal.UnitTrans( (float)pAuxData[nAux].lValue, 
				                                 FALSE, 
				                                 pAuxData[nAux].chType);
			stChannelData.lAux[nAux] = (long)(fValue*1000.f);
		}
	}
	//can data					
	int nCanCnt = pChannel->GetCanCount();
	SFT_CAN_DATA * pCanData = pChannel->GetCanData();
	for(int nCan=0; nCan < nCanCnt && nCan < 20; nCan++)
	{
		stChannelData.lCan[nCan] = (long)(pCanData[nCan].canVal.fVal[0]*1000.f);
	}
	//-------------------------------------------------	
	UINT nLen=0;
	BOOL bRet=FALSE;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_STATUS_DATA,1,1,sizeof(ST_REMOTE_CH_DATA),(BYTE*)&stChannelData,nLen);
	if(pBuffer && nLen>0)
	{
		bRet=onClientSend(0, pBuffer, nLen, 1);//CHSTATE
	}
	if(pBuffer) delete []pBuffer;
	//-------------------------------------------------
	return bRet;
}

BOOL CtrlSocketServer::onSendSbcStepEnd( int nModuleID,
	                                     int nChannelID,
	                                     PS_STEP_END_RECORD *sStepEnd,
	                                     PS_AUX_RAW_DATA *sAuxData,
										 PS_CAN_RAW_DATA *sCanData)
{
	ST_REMOTE_CH_DATA stChannelData;
	ZeroMemory(&stChannelData,sizeof(ST_REMOTE_CH_DATA));

	CCyclerModule* pModule = theApp.GetModuleInfo(nModuleID);
	if(!pModule) return FALSE;

	CCyclerChannel* pChannel = pModule->GetChannelInfo(nChannelID);
	if(!pChannel) return FALSE;

	//-------------------------------------------------
	stChannelData.chNo         = sStepEnd->chNo;
	stChannelData.chStepNO     = (BYTE)sStepEnd->chStepNo;
	stChannelData.chState      = sStepEnd->chCommState;
	stChannelData.chType       = sStepEnd->chStepType;
	stChannelData.chMode       = theApp.onGetStepMode(pChannel->GetScheduleData(),sStepEnd->chNo);	
	stChannelData.chSaveSelect = 1; //StepEnd
	stChannelData.chCode       = (BYTE)sStepEnd->chCode;
	stChannelData.reserved     = 0;
	stChannelData.ulSaveSeqNum = sStepEnd->lSaveSequence;
	stChannelData.lVoltage     = (long)sStepEnd->fVoltage;
	stChannelData.lCurrent     = (long)sStepEnd->fCurrent;
	stChannelData.lCapacity    = (long)(sStepEnd->fChargeAh+sStepEnd->fDisChargeAh);
	stChannelData.lWatt        = (long)sStepEnd->fWatt;
	stChannelData.lWattHour    = (long)(sStepEnd->fChargeWh+ sStepEnd->fDisChargeWh);
	stChannelData.ulStepTime   = (ULONG)sStepEnd->fStepTime;
	stChannelData.ulTotalTime  = (ULONG)sStepEnd->fTotalTime;

	CString strTemp;
	strTemp.Format(_T("step end chno:%d volt:%ld current:%ld"),
		            stChannelData.chNo,
					stChannelData.lVoltage,
					stChannelData.lCurrent);
	log_All(_T("sbc"),_T("stepend"),strTemp);

	//aux data
	int nAuxCnt = sAuxData->auxCount;
	SFT_AUX_DATA* pAuxData = pChannel->GetAuxData();
	if(pAuxData && nAuxCnt > 0)
	{
		for(int nAux=0; nAux < nAuxCnt && nAux < 10; nAux++)
		{
			float fValue = mainGlobal.UnitTrans( (float)pAuxData[nAux].lValue, 
				                                 FALSE, 
												 pAuxData[nAux].chType);
			stChannelData.lAux[nAux] = (long)(fValue*1000.f);//MILI-unit
		}
	}
	//can data
	int nCanCnt = sCanData->canCount;
	SFT_CAN_DATA * pCanData = pChannel->GetCanData();
	for(int nCan=0; nCan < nCanCnt && nCan < 20; nCan++)
	{
		stChannelData.lCan[nCan] = (long)(sCanData->canData[nCan].canVal.fVal[0]*1000.f);//MILI-unit
	}
	//-------------------------------------------------	
	UINT nLen=0;
	BOOL bRet=FALSE;
	BYTE *pBuffer=GetCommand(REMOTE_CMD_STATUS_DATA,1,1,sizeof(ST_REMOTE_CH_DATA),(BYTE*)&stChannelData,nLen);
	if(pBuffer && nLen>0)
	{
		m_iSendState=1;//Send Stop
		bRet=onClientSend(0, pBuffer, nLen, 2);//STEPEND
		m_iSendState=0;//Send Start
	}
	if(pBuffer) delete []pBuffer;
	//-------------------------------------------------
	return bRet;
}

BOOL CtrlSocketServer::onClientSend(int iDeviceNum,BYTE *pBuffer,UINT nLen,int iType)
{
	POSITION pos = m_clientSocketList.GetHeadPosition();
	if(!pos) return FALSE;

	CString   strTemp;
	BOOL      bRet=FALSE;
	MySocket *pMySocket=NULL;

	CString strType=_T("");
	if(iType==1)      strType=_T("chstate");
	else if(iType==2) strType=_T("stepend");

	int nCount=m_clientSocketList.GetSize();
	for(int i=0;i<nCount;i++)
	{
		if(g_AppInfo.iEnd==1) break;
		//--------------------------------------------------
		if(iType==1 && onChkSbcChState()==FALSE) return FALSE;//CHSTATE
		//--------------------------------------------------
		pMySocket=(MySocket*)m_clientSocketList.GetNext(pos);
		if(!pMySocket) continue;
		if(pMySocket->Socket==INVALID_SOCKET)     continue;
		if(pMySocket->iState!=ID_SOCKET_STAT_RUN) continue;

		if( iDeviceNum>0 &&
			iDeviceNum!=pMySocket->iDeviceNum) continue;

		bRet=Send(pMySocket->Socket,(char*)pBuffer,nLen);
		//---------------------------		
		if(g_AppInfo.iSbcStateLog==1)
		{//NOT CHSTATE
			strTemp.Format(_T("send ret_1:%d item:%d tp:%s(%d) ipaddr:%s len:%u data(hex):%s "),
						bRet,pMySocket->nItem,strType,iType,
						GStr::str_CharToCString(pMySocket->ipAddr), 
						nLen,mainGlobal.onGetBYTE(pBuffer,nLen,0,_T(" "),3));//Hex
			GLog::log_Save(_T("server_d"),_T("send"),strTemp,NULL,TRUE);
		}
		//---------------------------
		if(iDeviceNum<1)
		{
			Sleep(30);
		}
		//---------------------------
	}
	return TRUE;
}

USHORT CtrlSocketServer::crc16_modbus(const BYTE *buf, int len)
{
	int j;
	BYTE i;

	USHORT crc = 0xffff; //seed
	for ( j = 0; j < len; j++)
	{
		BYTE b = (BYTE)buf[j];
		for ( i = 0; i < 8; i++)
		{
			crc = ((b ^ (BYTE)crc) & 1) ? ((crc >> 1) ^ 0xA001) : (crc >> 1);
			b >>= 1;
		}
	}
	return crc;
}

int CtrlSocketServer::SendCommand(MySocket *pMySocket,int byCmdID,int iCurrStep,int iTotStep,UINT nSize,BYTE *pData)
{
	if(!pMySocket) return 0;

	UINT nLen=0;
	BOOL bRet=FALSE;

	BYTE *pBuffer=GetCommand(byCmdID,iCurrStep,iTotStep,nSize,pData,nLen);	
	if(pBuffer && nLen>0)
	{
		bRet=Send(pMySocket->Socket,(char*)pBuffer,nLen);
	}
	CString strTemp;
	strTemp.Format(_T("send ret_2:%d client item:%d ipaddr:%s len:%u data(hex):%s "),
		           bRet,
				   pMySocket->nItem,
				   GStr::str_CharToCString(pMySocket->ipAddr), 
		           nLen,mainGlobal.onGetBYTE(pBuffer,nLen,0,_T(" "),3));//Hex
	GLog::log_Save(_T("server_d"),_T("send"),strTemp,NULL,TRUE);

	if(pBuffer) delete []pBuffer;
	return bRet;
}

BYTE* CtrlSocketServer::GetCommand(int byCmdID,int iCurrStep,int iTotStep,UINT nSize,BYTE *pData,UINT &nPos)
{
	//stx(1) + header(3) + length(4) + bodySize(nSize) + crc(2) + res(1) + etx(1)
	int nPackSize = sizeof(BYTE) + sizeof(ST_REMOTE_CMD_HEADER) + sizeof(UINT) + nSize + sizeof(USHORT) + sizeof(BYTE) + sizeof(BYTE);
	BYTE* pBuffer = new BYTE[nPackSize];
	ZeroMemory(pBuffer,nPackSize);

	BYTE stx = 0;
	ST_REMOTE_CMD_HEADER stHeader;	
	ZeroMemory(&stHeader,sizeof(ST_REMOTE_CMD_HEADER));	
	UINT dataLen = nSize; 
	USHORT crc16;
	BYTE res = 0;
	BYTE etx = REMOTE_ETX;

	stx = REMOTE_STX_PNE_TO_REMOTE;
	stHeader.byCmdID = byCmdID;
	stHeader.byDataNo = 1;
	stHeader.byDataCnt = 1; //Split Send

	//Copy
	nPos = 0;	
	memcpy(pBuffer+nPos,&stx,sizeof(BYTE));
	nPos += sizeof(BYTE);

	memcpy(pBuffer+nPos,&stHeader,sizeof(ST_REMOTE_CMD_HEADER));
	nPos += sizeof(ST_REMOTE_CMD_HEADER);

	memcpy(pBuffer+nPos,&dataLen,sizeof(UINT));
	nPos += sizeof(UINT);

	memcpy(pBuffer+nPos,pData,dataLen);
	nPos += dataLen;

	crc16 = crc16_modbus((BYTE*)pBuffer,nPos);
	memcpy(pBuffer+nPos,&crc16,sizeof(crc16));
	nPos += sizeof(crc16);

	memcpy(pBuffer+nPos,&res,sizeof(BYTE));
	nPos += sizeof(BYTE);

	memcpy(pBuffer+nPos,&etx,sizeof(BYTE));
	nPos += sizeof(BYTE);
	
	return pBuffer;
}

BOOL CtrlSocketServer::SendNACK(MySocket *pMySocket,BYTE byCmdID, UINT nCode)
{
	BYTE buff[256]={0,};
	memset(buff,0,sizeof(buff));

	memcpy(buff,&nCode,sizeof(UINT));

	return SendCommand(pMySocket,byCmdID,1,1,sizeof(UINT),buff);
}