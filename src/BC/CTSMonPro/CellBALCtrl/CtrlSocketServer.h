#pragma once

#include <winsock2.h>
#include <process.h>

// CtrlSocketServer

class CtrlSocketServer
{	
public:
	CtrlSocketServer();
	virtual ~CtrlSocketServer();
	
	CWnd   *m_pWnd;
	BOOL   m_bInit,m_bStart;	
	
	int    m_iSendState;
	SOCKET m_serverSocket;
	HANDLE m_hServerThread;
	
	MyCriticalSection m_Lock;
	CTypedPtrList<CPtrList, MySocket*> m_clientSocketList;
	CTypedPtrList<CPtrList, MySocket*> m_clientCloseList;
	
	BOOL onInit();
	void onEnd();
	BOOL onOpen(CWnd *pWnd,int iPort);
	void onCloseSocket(SOCKET &TempSocket);
	void onCloseServerSocket();
	void onCloseClientSocket(MySocket *pMySocket);
	void onError(CString str);

	CString onGetIPAddr();
	int     onSetClient(SOCKET sock,struct sockaddr_in client);
	CString onClientInfo(in_addr addr,int port);
	void    onLogSave(CString type,CString log,int iType=0);

	MySocket* onClientCreate();
	void      onClientListRemove(int iType,MySocket *mySocket=NULL);
	int       onClientListCount();
	int       onClientList(CStringArray &ClientList);
	MySocket* onClientFind(int nItem);
	void      onClientCloseList();

	BOOL  m_bReceiving;
	DWORD Receive(SOCKET Socket,char *pData,int iLength);
	BOOL  Transmit(SOCKET Socket,char *pData,int iLength);

    BOOL  Send(SOCKET Socket,char *pData,int iLength);	
	BOOL  Recv(SOCKET Socket,char *pData,int &iLength);
	BOOL  onRecvDeal(MySocket *pMySocket,char *pBuff,int nLen);
	int   onChkHeader(char *pBuff,int nLen);
	BOOL  onClientSend(int iDeviceNum,BYTE *pBuffer,UINT nLen,int iType=0);
	void  onSendMessage(int nCmd,WPARAM wParam,LPARAM lParam);

	static DWORD WINAPI ServerThread(LPVOID arg);
	static DWORD WINAPI ClientThread(LPVOID arg);

	USHORT crc16_modbus(const BYTE *buf, int len);
	BYTE*  GetCommand(int byCmdID,int iCurrStep,int iTotStep,UINT nSize,BYTE *pData,UINT &nPos);
	int    SendCommand(MySocket *pMySocket,int byCmdID,int iCurrStep,int iTotStep,UINT nSize,BYTE *pData);
	BOOL   SendNACK(MySocket *pMySocket,BYTE byCmdID, UINT nCode);

	UINT onCmdWorkStart(BYTE* pData, UINT nDataLen, ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmdWorkPause(BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmdWorkEnd(BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmdWorkIsolation(BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmdWorkContinue(BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);

	UINT onCmdSeqBox(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmd3497(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmd5520(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmdPwr(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmdBcr(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);
	UINT onCmdBmsRelay(MySocket *pMySocket,BYTE* pData, UINT nDataLen,ST_REMOTE_WORK_START_PARAM &param);

	void onResBcr(int iDeviceNum,BYTE iResponse,BYTE* pData, UINT nDataLen);
	void onResSeqBox(int iDeviceNum,BYTE iResponse,BYTE* pData, UINT nDataLen);
	void onRes3497(int iDeviceNum,BYTE iResponse,float fValue=0);
	void onRes5520(int iDeviceNum,BYTE iResponse,float fValue=0);
	void onResPwr(int iDeviceNum,BYTE iResponse,USHORT iCommand=0,float fVolt=0,float fCurrent=0);
	void onResBmsRelay(int iDeviceNum,BYTE iResponse,BYTE iRelayState,BYTE iBmsType,long lData);
	void onResIsolation(int iChannelNo,USHORT iResponse);
	void onResNACK(BYTE byCmdID, UINT nCode);

	void onSendSbcAllChState();
	BOOL onChkSbcChState();
	BOOL onSendSbcChState(int nModuleID,int nChannelID);	
	BOOL onSendSbcStepEnd( int nModuleID,int nChannelID,
		                   PS_STEP_END_RECORD *sStepEnd,
						   PS_AUX_RAW_DATA *sAuxData,
						   PS_CAN_RAW_DATA *sCanData);
};

extern CtrlSocketServer  mainCtrlServer;