// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__869AD821_003F_4525_BAA6_155AF25DD39B__INCLUDED_)
#define AFX_MAINFRM_H__869AD821_003F_4525_BAA6_155AF25DD39B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "./PwrSupplyCtrl/PwrSupplyCtrl.h"
#include "serialconfigdlg.h"
#include "SerialPort.h"
#include "WarningDlg.h"

#include "TrayInputDlg.h"
#include "ThreadExitProgress.h" //ksj 20201023

typedef struct {
	char strPath[256];
	char strBarCode[256];
} CTSWORKVALUE;

typedef struct {
	unsigned int nSendMode;
	unsigned int nReceiveMode;
} CTSISOVALUE;

#define CHAMBER_TIMER	3000

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	bool m_bAutoExit;
	CTSWORKVALUE*	m_pCtsValue;
	CTSISOVALUE*	m_pISOValue;
	CTSISOVALUE		m_ISOValue;
	COPYDATASTRUCT	msg;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL m_bBlockFlag;

	//BOOL SetOvenFixMode(int nIndex, BOOL bSet);
	//BOOL RequestOvenCurrentValue(int nIndex);
	//BOOL SetOvenTSlop(int nIndex, float fDataT);
	//BOOL SetOvenHSlop(int nIndex, float fDataH);
	//BOOL SetOvenTemperature(int nIndex, float fDataT);
	//BOOL SetOvenHumidity(int nIndex, float fDataH);
	//BOOL SendOvenStopCmd(int nIndex);
	//BOOL SendOvenRunCmd(int nIndex);
	void BCRScaned(CString strTag);
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:  // control bar embedded members
	CWarningDlg * m_pWarringDlg;			//ljb 201010
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	
	CTrayInputDlg *m_pTrayInputDlg;

	CSerialPort	m_SerialPort1;
	CSerialPort	m_SerialPort2;
	
	void CheckUsePort();		
	BOOL onOpenPort_Bcr1(int iType=1);
	BOOL onOpenPort_Bcr2(int iType=1);
	BOOL onOpenPort_Load1(int iType=1);
	BOOL onOpenPort_Load2(int iType=1);
	BOOL onOpenPort_Chamber(int nItem, int iType=1);
	BOOL onOpenPort_Power(int nItem,int iType=1);
	BOOL onOpenPort_Chiller(int nItem,int iType=1);

	//cny 201809
	CPwrSupplyCtrl m_PwrSupplyCtrl[MAX_POWER_COUNT];

	int  onGetSerialPowerNo(int nCh,int &nOutp);
	BOOL onGetSerialPowerGetSbcCh(int nItem,int &nCHA,int &nCHB);
	int  onGetSerialChillerNo(int nCh);
	int  onGetSerialChamberNo(int nCh);

	CThreadExitProgress* m_pThreadExitProgress;

    //Generated message map functions
protected:
	CString m_strReceiveBuff1;
	UINT	m_uLogCnt;
	char m_szRxBuff[1024];
	
	UINT m_nRxBuffCnt;
	
	//	BOOL m_bSerialConnected;
	
	
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnExit();
	afx_msg void OnFilePrint();	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnUnitLog();
	afx_msg void OnSerialSet();
	//}}AFX_MSG
	afx_msg LRESULT OnWriteLogMsg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCommunication(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTagIdScaned(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnWorkStop(WPARAM wParam, LPARAM lParam);	
	DECLARE_MESSAGE_MAP()
public:
	CString m_strVersion;
	void SetExitProgress(int nPos);
	void ShowExitProgress(void);
};

/////////////////////////////////////////////////////////////////////////////
extern CMainFrame *mainFrm;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__869AD821_003F_4525_BAA6_155AF25DD39B__INCLUDED_)
