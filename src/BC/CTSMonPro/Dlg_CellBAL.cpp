// Dlg_CellBAL.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "Dlg_CellBAL.h"

#include "MainFrm.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBAL dialog


Dlg_CellBAL::Dlg_CellBAL(CWnd* pParent /*=NULL*/)
//	: CDialog(Dlg_CellBAL::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_CellBAL::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_CellBAL::IDD2):
	(Dlg_CellBAL::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_CellBAL)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_DlgCellBalOpt=NULL;
	m_DlgCellBalMain=NULL;
}

// 200313 HKH Memory Leak ����
Dlg_CellBAL::~Dlg_CellBAL()
{
	if (m_DlgCellBalMain != NULL)
	{
		m_DlgCellBalMain->DestroyWindow();
		delete m_DlgCellBalMain;
		m_DlgCellBalMain = NULL;
	}
}


void Dlg_CellBAL::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_CellBAL)
	DDX_Control(pDX, IDC_STATIC_AREA, m_StaticArea);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_CellBAL, CDialog)
	//{{AFX_MSG_MAP(Dlg_CellBAL)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUT_CELLBAL_OPT, OnButCellbalOpt)
	ON_BN_CLICKED(IDC_BUT_CELLBAL_LIST, OnButCellbalList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBAL message handlers

BOOL Dlg_CellBAL::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mainDlgCellBal=this;
	
	OnButCellbalList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_CellBAL::OnOK() 
{	
	//CDialog::OnOK();
}

void Dlg_CellBAL::OnCancel() 
{	
	//CDialog::OnCancel();
}

void Dlg_CellBAL::OnClose() 
{	
	ShowWindow(SW_HIDE);
	//CDialog::OnClose();
}

BOOL Dlg_CellBAL::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP)
	{
		if(pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_RETURN)
		{					  
			return FALSE;	
		}					
	} 
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_CellBAL::onDlgHide()
{
	if(m_DlgCellBalOpt)  m_DlgCellBalOpt->ShowWindow(SW_HIDE);
	if(m_DlgCellBalMain) m_DlgCellBalMain->ShowWindow(SW_HIDE);
}

void Dlg_CellBAL::OnButCellbalOpt() 
{
	onDlgCellBalOpt();
}

void Dlg_CellBAL::OnButCellbalList() 
{		
	onDlgCellBalMain();
}

void Dlg_CellBAL::onDlgCellBalMain() 
{
	onDlgHide();
	
	if(m_DlgCellBalMain == NULL)
	{
		m_DlgCellBalMain = new Dlg_CellBALMain();
		m_DlgCellBalMain->Create(
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_CELLBAL_MAIN):
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_CELLBAL_MAIN_ENG):
			IDD_DIALOG_CELLBAL_MAIN, &m_StaticArea);
		
		CRect rect;
		m_StaticArea.GetClientRect(rect);
		m_DlgCellBalMain->MoveWindow(rect,TRUE);
	}
	if(m_DlgCellBalMain == NULL) return;
	
	m_DlgCellBalMain->ShowWindow(SW_SHOW);
}


LRESULT Dlg_CellBAL::onDlgCellBalOpt() 
{
	onDlgHide();
	
	if(m_DlgCellBalOpt == NULL)
	{
		m_DlgCellBalOpt = new Dlg_CellBALOpt();
  		m_DlgCellBalOpt->Create(
  			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_DIALOG_CELLBAL_OPT)
 			:(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_DIALOG_CELLBAL_OPT_ENG):
 			IDD_DIALOG_CELLBAL_OPT, &m_StaticArea);
		
		CRect rect;
		m_StaticArea.GetClientRect(rect);
		m_DlgCellBalOpt->MoveWindow(rect,TRUE);
	}
	if(m_DlgCellBalOpt == NULL) return 0;
	
	m_DlgCellBalOpt->SetValue();
	m_DlgCellBalOpt->ShowWindow(SW_SHOW);

	return 1;
}
