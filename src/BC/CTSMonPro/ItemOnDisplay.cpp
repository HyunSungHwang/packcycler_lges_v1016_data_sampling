// ItemOnDisplay.cpp: implementation of the CItemOnDisplay class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 
#include "CTSMonPro.h"
#include "ItemOnDisplay.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
long CItemOnDisplay::m_lNumOfItems = 0;
CString CItemOnDisplay::REG_SECTION_NAME = _T("DisplayItem");
CItemOnDisplay::CItemOnDisplay()
{
	m_pItemList = NULL;
}

CItemOnDisplay::~CItemOnDisplay()
{
	if(m_pItemList != NULL)
	{
		delete [] m_pItemList;
		m_pItemList = NULL;
	}
}

//Registry의 [DisplayItem]에서 표기할 Column을 읽는다.
CItemOnDisplay* 
CItemOnDisplay::CreateItemsOnDisplay()
{
	CString strTitle, strData;
	CStringArray strItemArray;
	CCTSMonProApp* pApp = (CCTSMonProApp *)AfxGetApp();

	for( int nI = 0; nI < PS_MAX_ITEM_NUM; nI++ )
	{
		strTitle.Format("Item%02d", nI+1);
		strData = pApp->GetProfileString("Settings", strTitle);
		if( strData.IsEmpty() )
		{
			strData = (LPSTR)::PSGetItemName(nI);			
			if(!strData.IsEmpty())	pApp->WriteProfileString("Settings", strTitle, strData);
		}
		strItemArray.Add(strData);
	}		
	
	
	CItemOnDisplay *pItemOnDisplay = NULL;

	int nWidth;
	m_lNumOfItems = 0;
	int nItemNum = pApp->GetProfileInt("DisplayItem", "ItemNum", -1);		//Display 항목이 설정되어 있지 않음면 
	
	if( nItemNum <= 0 )	//default Item;
	{
		m_lNumOfItems = 5;
		pItemOnDisplay = new CItemOnDisplay[m_lNumOfItems];
		pApp->WriteProfileInt(REG_SECTION_NAME, _T("ItemNum"), m_lNumOfItems);

		strTitle.Format("Item%02d", PS_CHANNEL_NO+1);	
		nWidth = 60;
		pItemOnDisplay[0].m_strItemTitle = "Channel";
		pItemOnDisplay[0].m_nBlockWidth = nWidth;
		pItemOnDisplay[0].m_nItemNo = PS_CHANNEL_NO;
		pApp->WriteProfileString(REG_SECTION_NAME, _T("Col01"), strTitle);
		pApp->WriteProfileInt(REG_SECTION_NAME, _T("Width01"), nWidth);

		strTitle.Format("Item%02d", PS_STATE+1);	
		nWidth = 50;
		pItemOnDisplay[1].m_strItemTitle = "State";
		pItemOnDisplay[1].m_nBlockWidth = nWidth;
		pItemOnDisplay[1].m_nItemNo = PS_STEP_TYPE;
		pApp->WriteProfileString(REG_SECTION_NAME, _T("Col02"), strTitle);		
		pApp->WriteProfileInt(REG_SECTION_NAME, _T("Width02"), nWidth);

		strTitle.Format("Item%02d", PS_VOLTAGE+1);	
		nWidth = 60;
		pItemOnDisplay[2].m_strItemTitle = "Voltage";
		pItemOnDisplay[2].m_nBlockWidth = nWidth;
		pItemOnDisplay[2].m_nItemNo = PS_VOLTAGE;
		pApp->WriteProfileString(REG_SECTION_NAME, _T("Col03"), strTitle);
		pApp->WriteProfileInt(REG_SECTION_NAME, _T("Width03"), nWidth);

		strTitle.Format("Item%02d", PS_CURRENT+1);	
		nWidth = 60;
		pItemOnDisplay[3].m_strItemTitle = "Current";
		pItemOnDisplay[3].m_nBlockWidth = nWidth;
		pItemOnDisplay[3].m_nItemNo = PS_CURRENT;
		pApp->WriteProfileString(REG_SECTION_NAME, _T("Col04"), strTitle);
		pApp->WriteProfileInt(REG_SECTION_NAME, _T("Width04"), nWidth);
	
		strTitle.Format("Item%02d", PS_CAPACITY+1);	
		nWidth = 60;
		pItemOnDisplay[4].m_strItemTitle = "Capacity";
		pItemOnDisplay[4].m_nBlockWidth = nWidth;
		pItemOnDisplay[4].m_nItemNo = PS_CAPACITY+1;
		pApp->WriteProfileString(REG_SECTION_NAME, _T("Col05"), strTitle);
		pApp->WriteProfileInt(REG_SECTION_NAME, _T("Width05"), nWidth);

	}
	else
	{
		CString strItemNo;
		int nItemNo;
		pItemOnDisplay = new CItemOnDisplay[nItemNum];
		for( int nI = 0; nI < nItemNum; nI++ )
		{
			strItemNo.Format("Col%02d", nI+1);
			strTitle = pApp->GetProfileString(REG_SECTION_NAME, strItemNo);
			if(strTitle.IsEmpty())		break;

			nItemNo = atol(strTitle.Right(2));
			if(nItemNo < 1 || nItemNo > strItemArray.GetSize())
			{
				break;
			}
			pItemOnDisplay[m_lNumOfItems].m_nItemNo = nItemNo-1;
			pItemOnDisplay[m_lNumOfItems].m_strItemTitle = strItemArray.GetAt(pItemOnDisplay[m_lNumOfItems].m_nItemNo);
			strItemNo.Format("Width%02d", nI+1);
			nWidth = pApp->GetProfileInt(REG_SECTION_NAME, strItemNo, 50);
			pItemOnDisplay[m_lNumOfItems].m_nBlockWidth = nWidth;
			
			m_lNumOfItems++;
		}
	}

	return pItemOnDisplay;
}

void CItemOnDisplay::DeleteAllItemOnDisplay(CItemOnDisplay *pItemOnDisplay)
{
	if(pItemOnDisplay == NULL)	return;

/*	int nItemNum = pItemOnDisplay->GetItemNum();
	CString strItemNo;
	CCTSMonProApp* pApp = (CCTSMonProApp *)AfxGetApp();
	for( int nI = 0; nI < nItemNum; nI++ )
	{
		strItemNo.Format("Width%02d", nI+1);
		pApp->WriteProfileInt(REG_SECTION_NAME, 
							  strItemNo, 
							  pItemOnDisplay[nI].GetWidth());
	}
*/	delete [] pItemOnDisplay;
	pItemOnDisplay = NULL;
	m_lNumOfItems = 0;
}

int CItemOnDisplay::FindItemIndexOfTitle(CItemOnDisplay* pItem, WORD nITem)
{
	for( int nI = 0; nI < m_lNumOfItems; nI++ )
	{
		if( pItem[nI].m_nItemNo == nITem )
		{
			return nI;
		}
	}

	return -1;
}

//void CItemOnDisplay::LoadImageIndex(LVITEM &lvItem, int nChannelState)
//{
//	switch(nChannelState)
//	{
//		case CH_STATE_IDLE :	lvItem.iImage = EP_IMAGE_INDEX_IDLE;
//			break;
//		case CH_STATE_INIT :	lvItem.iImage = EP_IMAGE_INDEX_IDLE;
//			break;
//		case CH_STATE_RUN :		lvItem.iImage = EP_IMAGE_INDEX_RUN;
//			break;
//		case CH_STATE_STOP :	lvItem.iImage = EP_IMAGE_INDEX_STOP;
//			break;
//		case CH_STATE_PAUSE :	lvItem.iImage = EP_IMAGE_INDEX_PAUSE;
//			break;
//		case CH_STATE_STANDBY :	lvItem.iImage = EP_IMAGE_INDEX_IDLE;
//			break;	
//		case CH_STATE_CALI :	lvItem.iImage = EP_IMAGE_INDEX_CALIBRATION;
//			break;
//		case CH_STATE_ERROR :	lvItem.iImage = EP_IMAGE_INDEX_ERROR;
//			break;
//		default:				lvItem.iImage = EP_IMAGE_INDEX_OFFLINE;
//	}
//	lvItem.mask = LVIF_TEXT | LVIF_IMAGE;
//}