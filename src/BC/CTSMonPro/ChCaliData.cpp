// ChCaliData.cpp: implementation of the CChCaliData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "ChCaliData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChCaliData::CChCaliData()
{

}

CChCaliData::~CChCaliData()
{

}

void CChCaliData::GetVCalData(WORD wRange, WORD nType,WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= SFT_MAX_VOLTAGE_RANGE || wPoint>= MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCalData[wRange][nType][wPoint].dAdData;
	dMeterData = m_VCalData[wRange][nType][wPoint].dMeterData;
}

void CChCaliData::GetVCheckData(WORD wRange, WORD nType,WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= SFT_MAX_VOLTAGE_RANGE || wPoint>= MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCheckData[wRange][nType][wPoint].dAdData;
	dMeterData = m_VCheckData[wRange][nType][wPoint].dMeterData;
}

void CChCaliData::GetICalData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= SFT_MAX_CURRENT_RANGE || wPoint>= MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICalData[wRange][wPoint].dAdData;
	dMeterData = m_ICalData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetICheckData(WORD wRange, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wRange >= SFT_MAX_CURRENT_RANGE || wPoint>= MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICheckData[wRange][wPoint].dAdData;
	dMeterData = m_ICheckData[wRange][wPoint].dMeterData;
}

void CChCaliData::GetVCalData(WORD nType, WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCalData[0][nType][wPoint].dAdData;
	dMeterData = m_VCalData[0][nType][wPoint].dMeterData;
}

void CChCaliData::GetVCheckData(WORD nType,WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_VCheckData[0][nType][wPoint].dAdData;
	dMeterData = m_VCheckData[0][nType][wPoint].dMeterData;
}

void CChCaliData::GetICalData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= MAX_CALIB_SET_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICalData[0][wPoint].dAdData;
	dMeterData = m_ICalData[0][wPoint].dMeterData;
}

void CChCaliData::GetICheckData(WORD wPoint, double &dAdData, double &dMeterData)
{
	if(wPoint>= MAX_CALIB_CHECK_POINT)
	{
		dAdData = 0.0;
		dMeterData = 0.0;
	}
	
	dAdData = m_ICheckData[0][wPoint].dAdData;
	dMeterData = m_ICheckData[0][wPoint].dMeterData;
}	 


void CChCaliData::SetVCalData(WORD wRange, WORD nType,WORD wPoint, double dAdData, double dMeterData)
{
	m_VCalData[wRange][nType][wPoint].dAdData = dAdData;
	m_VCalData[wRange][nType][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetVCheckData(WORD wRange, WORD nType, WORD wPoint, double dAdData, double dMeterData)
{
	m_VCheckData[wRange][nType][wPoint].dAdData = dAdData;
	m_VCheckData[wRange][nType][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetICalData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_ICalData[wRange][wPoint].dAdData = dAdData;
	m_ICalData[wRange][wPoint].dMeterData = dMeterData;
}

void CChCaliData::SetICheckData(WORD wRange, WORD wPoint, double dAdData, double dMeterData)
{
	m_ICheckData[wRange][wPoint].dAdData = dAdData;
	m_ICheckData[wRange][wPoint].dMeterData = dMeterData;
}

	 

BOOL CChCaliData::ReadDataFromFile(FILE *fp)
{
	if(fp == NULL)	return FALSE;
	if(fread(m_VCalData, sizeof(m_VCalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_VCheckData, sizeof(m_VCheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_ICalData, sizeof(m_ICalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fread(m_ICheckData, sizeof(m_ICheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CChCaliData::WriteDataToFile(FILE *fp)
{
	if(fp == NULL)	return FALSE;
	if(fwrite(m_VCalData, sizeof(m_VCalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_VCheckData, sizeof(m_VCheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICalData, sizeof(m_ICalData), 1, fp) < 1)
	{
		return FALSE;
	}
	if(fwrite(m_ICheckData, sizeof(m_ICheckData), 1, fp) < 1)
	{
		return FALSE;
	}
	return TRUE;
}
