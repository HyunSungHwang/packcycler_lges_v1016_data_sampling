// RegeditManage.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "RegeditManage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegeditManage dialog


CRegeditManage::CRegeditManage(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CRegeditManage::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CRegeditManage::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CRegeditManage::IDD3):
	(CRegeditManage::IDD), pParent)
{
	//{{AFX_DATA_INIT(CRegeditManage)
	m_check1 = FALSE;
	m_check2 = FALSE;
	m_check3 = FALSE;
	m_check4 = FALSE;
	m_check5 = FALSE;
	m_check6 = FALSE;
	m_check7 = FALSE;
	m_check8 = FALSE;
	m_check9 = FALSE;
	m_edit1 = 0;
	m_edit2 = 0;
	m_edit3 = 0;
	m_edit4 = 0;
	m_edit5 = 0;
	m_check10 = FALSE;
	m_check11 = FALSE;
	m_check12 = FALSE;
	m_check13 = FALSE;
	m_check14 = FALSE;
	m_check15 = FALSE;
	m_check16 = FALSE;
	m_check17 = FALSE;
	m_bCheckVoltage = FALSE;
	m_bUseAuxSetLogin = FALSE;
	m_bUseBackupSbcResult = FALSE;
	m_bUseBackupSbcStepEnd = FALSE;
	m_bCheckAuxSafeCond = FALSE;
	m_bUseOnlyMasterCheck = FALSE;
	m_bCheckShowSCHAllStepUpdateInPopup = FALSE;
	m_CheckUseTempSensor = FALSE;
	m_CheckUseExtDevMenu = FALSE;
	m_CheckUseAllButton = FALSE;
	m_CheckUseVebtCloseButton = FALSE;
	m_bUsePowerCableCheck = FALSE;		// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지
	//}}AFX_DATA_INIT
}


void CRegeditManage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegeditManage)
	DDX_Check(pDX, IDC_CHECK1, m_check1);
	DDX_Check(pDX, IDC_CHECK2, m_check2);
	DDX_Check(pDX, IDC_CHECK3, m_check3);
	DDX_Check(pDX, IDC_CHECK4, m_check4);
	DDX_Check(pDX, IDC_CHECK5, m_check5);
	DDX_Check(pDX, IDC_CHECK6, m_check6);
	DDX_Check(pDX, IDC_CHECK7, m_check7);
	DDX_Check(pDX, IDC_CHECK8, m_check8);
	DDX_Check(pDX, IDC_CHECK9, m_check9);
	DDX_Text(pDX, IDC_EDIT1, m_edit1);
	DDX_Text(pDX, IDC_EDIT2, m_edit2);
	DDX_Text(pDX, IDC_EDIT3, m_edit3);
	DDX_Text(pDX, IDC_EDIT4, m_edit4);
	DDX_Text(pDX, IDC_EDIT5, m_edit5);
	DDX_Check(pDX, IDC_CHECK10, m_check10);
	DDX_Check(pDX, IDC_CHECK11, m_check11);
	DDX_Check(pDX, IDC_CHECK12, m_check12);
	DDX_Check(pDX, IDC_CHECK13, m_check13);
	DDX_Check(pDX, IDC_CHECK14, m_check14);
	DDX_Check(pDX, IDC_CHECK15, m_check15);
	DDX_Check(pDX, IDC_CHECK16, m_check16);
	DDX_Check(pDX, IDC_CHECK17, m_check17);
	DDX_Check(pDX, IDC_CHECK_VOLTAGE, m_bCheckVoltage);
	DDX_Check(pDX, IDC_USE_AUX_SET_LOGIN, m_bUseAuxSetLogin);
	DDX_Check(pDX, IDC_USE_BACKUP_SBC_RESULT, m_bUseBackupSbcResult);
	DDX_Check(pDX, IDC_USE_BACKUP_SBC_STEP_END, m_bUseBackupSbcStepEnd);
	DDX_Check(pDX, IDC_CHECK_AUX_SAFE_COND, m_bCheckAuxSafeCond);
	DDV_MinMaxUInt(pDX, m_edit4, 0, 1);
	DDX_Check(pDX, IDC_USE_CAN_CHECK_MODE_MASTER, m_bUseOnlyMasterCheck);
	DDX_Check(pDX, IDC_USE_SCH_ALL_STEP_UPDATE_POPUPMENU, m_bCheckShowSCHAllStepUpdateInPopup);
	DDX_Check(pDX, IDC_CHECK_USE_TEMPSENSOR, m_CheckUseTempSensor);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_USE_EXT_DEV_MENU, m_CheckUseExtDevMenu);

	DDX_Check(pDX, IDC_CHECK_USE_CHAM_VENT, m_CheckUseAllButton);
	DDX_Check(pDX, IDC_CHECK_USE_ALL_BUTTON, m_CheckUseVebtCloseButton);
	DDX_Check(pDX, IDC_CHECK_USE_POWER_CABLE_CHECK, m_bUsePowerCableCheck);	// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지
}


BEGIN_MESSAGE_MAP(CRegeditManage, CDialog)
	//{{AFX_MSG_MAP(CRegeditManage)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CHECK_USE_TEMPSENSOR2, &CRegeditManage::OnBnClickedCheckUseTempsensor2)
	ON_BN_CLICKED(IDC_CHECK17, &CRegeditManage::OnBnClickedCheck17)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegeditManage message handlers

BOOL CRegeditManage::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_check1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Administrator", FALSE);
	m_check2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven", FALSE);
	m_check3 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	m_check4 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", FALSE);
	m_check5 = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", TRUE);
	m_check6 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseChamberContinue",FALSE);
	m_check7 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);	
	m_check8 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCanValHex", FALSE);	
	m_check9 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCanModeCheck",FALSE);
	m_check10 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLockForRun",FALSE);
	m_check11 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseMuxLink",FALSE);
	m_check12 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLoader",FALSE);
	m_check13 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolation",FALSE);
	m_check14 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLoad Type",FALSE);
	m_check15 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationBranch",FALSE); //lyj 20210913
	m_check16 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCMD",1);		//lyj 20210913
	m_CheckUseTempSensor = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "TempSensorCheck", TRUE); //yulee 20190218
	m_CheckUseExtDevMenu = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "USE EXT DEV MENU", TRUE);

	m_edit1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UserLimitPower", 0);	//ljb 20130722 add
	m_edit2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Max Parallel Count", 0);
	m_edit3 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Count", 4);   //ljb 20170720 add
	m_edit4 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", 1);   //ljb 20170720 add

	// 211015 HKH 절연모드 충전 관련 기능 보완
	m_check17 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Isolation Safety",0);
	m_edit5 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Safety Start Voltage", 5); 
	if (m_check17)	GetDlgItem(IDC_EDIT5)->EnableWindow(TRUE);
	else			GetDlgItem(IDC_EDIT5)->EnableWindow(FALSE);

	//ksj 20170224
	m_bCheckVoltage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check Voltage", 0);
	m_bUseBackupSbcResult = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Backup Sbc Result", 0);
	m_bUseBackupSbcStepEnd = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Backup Sbc StepEnd", 1);
	m_bUseAuxSetLogin = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use AuxSetLogin", 1);
	m_bCheckAuxSafeCond = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Check AuxSafeCond", 0);
	

	m_bUseOnlyMasterCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCanCheckModeOnlyMaster", FALSE);

	//yulee 20181216
	m_bCheckShowSCHAllStepUpdateInPopup = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSchAllStepUpdate", 0);

	//m_CheckUseAllButton = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use All Button", 0);
	//m_CheckUseAllButton = AfxGetApp()->GetProfileInt("Config","USE ALL BUTTON",15);//ksj 20200122 : vent open 추가 //lyj 20200909 
	//lyj 20200909 Use All Button 버그수정 s =================
	int iButtonCheck =0;
	iButtonCheck = AfxGetApp()->GetProfileInt("Config","USE ALL BUTTON",15);
	if(iButtonCheck >6)
	{
		m_CheckUseAllButton = 1;
	}
	else
	{
		m_CheckUseAllButton = 0;
	}
	//lyj 20200909 Use All Button 버그수정 e =================
	m_CheckUseVebtCloseButton = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Vent Close", 0);

	m_bUsePowerCableCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseOutCableCheck", 0);	// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRegeditManage::OnOK() 
{
	UpdateData(TRUE);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Administrator", m_check1);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven", m_check2);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", m_check3);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", m_check4);
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "ChamberOperationType", m_check5);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseChamberContinue",m_check6);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseSlave", m_check7);	
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseCanValHex", m_check8);	
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseCanModeCheck",m_check9);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseLockForRun", m_check10);		//ljb 20150316 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseMuxLink", m_check11);			//ljb 20151111 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseLoader", m_check12);			//ljb 20151218 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseIsolation", m_check13);			//ljb 20151218 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseLoad Type", m_check14);			//ljb 20160122 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseIsolationBranch", m_check15);			//lyj 20210913
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseIsolationCMD", m_check16);			//lyj 20210913
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "TempSensorCheck", m_CheckUseTempSensor); //yulee 20190218
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "USE EXT DEV MENU", m_CheckUseExtDevMenu);

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UserLimitPower", m_edit1);	//ljb 20130722 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Max Parallel Count", m_edit2);

	//ksj 20170224
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Check Voltage", m_bCheckVoltage);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Backup Sbc Result", m_bUseBackupSbcResult);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Backup Sbc StepEnd", m_bUseBackupSbcStepEnd);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use AuxSetLogin", m_bUseAuxSetLogin);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Check AuxSafeCond", m_bCheckAuxSafeCond);

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Count", m_edit3);	//ljb 20170720 add
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", m_edit4);	//ljb 20170720 add

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseCanCheckModeOnlyMaster", m_bUseOnlyMasterCheck);

	//yulee 20181216
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseSchAllStepUpdate", m_bCheckShowSCHAllStepUpdateInPopup);

	//AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use All Button", m_CheckUseAllButton);
	//lyj 20200909 Use All Button 버그수정 s =================
	if(m_CheckUseAllButton)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use All Button", 15);
	}
	else
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use All Button", 6);
	}
	//lyj 20200909 Use All Button 버그수정 e =================
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Vent Close", m_CheckUseVebtCloseButton);

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "UseOutCableCheck", m_bUsePowerCableCheck);	// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지

	// 211015 HKH 절연모드 충전 관련 기능 보완
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Isolation Safety", m_check17);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Safety Start Voltage", m_edit5);

	UpdateData(FALSE);	
	CDialog::OnOK();
}


void CRegeditManage::OnBnClickedCheckUseTempsensor2()
{
	// TODO:
}

// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지
void CRegeditManage::OnBnClickedCheck17()
{
	UpdateData();

	if (m_check17)	GetDlgItem(IDC_EDIT5)->EnableWindow(TRUE);
	else			GetDlgItem(IDC_EDIT5)->EnableWindow(FALSE);
}
