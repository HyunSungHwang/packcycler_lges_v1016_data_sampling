#if !defined(AFX_FILESAVESETDLG_H__215E2310_93C9_49DF_9EBE_328FFBB2BD48__INCLUDED_)
#define AFX_FILESAVESETDLG_H__215E2310_93C9_49DF_9EBE_328FFBB2BD48__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FileSaveSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFileSaveSetDlg dialog

class CFileSaveSetDlg : public CDialog
{
// Construction
public:
	void InitList();
	CFileSaveSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFileSaveSetDlg)
	enum { IDD = IDD_FILE_SAVE_DLG , IDD2 = IDD_FILE_SAVE_DLG_ENG , IDD3 = IDD_FILE_SAVE_DLG_PL };
	BOOL	m_bTime;
	BOOL	m_bVoltage;
	BOOL	m_bCurrent;
	BOOL	m_bCapacity;
	BOOL	m_bWattHour;
	BOOL	m_bAvgCurrent;
	BOOL	m_bAvgVoltage;
	BOOL	m_bOvenTemperature;
	BOOL	m_bMeterValue;
	BOOL	m_bCVTime;
	BOOL	m_bSyncTime;
	BOOL	m_bOvenHumidity;
	BOOL	m_bVoltageInput;
	BOOL	m_bVoltagePower;
	BOOL	m_bVoltageBus;
	BOOL	m_bOutputState;
	BOOL	m_bInputState;
	BOOL	m_bLoaderVolt;
	BOOL	m_bLoaderCurr;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileSaveSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFileSaveSetDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILESAVESETDLG_H__215E2310_93C9_49DF_9EBE_328FFBB2BD48__INCLUDED_)
