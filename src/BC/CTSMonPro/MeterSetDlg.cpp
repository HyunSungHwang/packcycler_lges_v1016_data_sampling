// MeterSetDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "MeterSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMeterSetDlg dialog


CMeterSetDlg::CMeterSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CMeterSetDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CMeterSetDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CMeterSetDlg::IDD3):
	(CMeterSetDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CMeterSetDlg)
	m_nPort = 0;
	m_nMeterType = 0;
	m_nOperator = 2;
	m_fConvertData = 1.0f;
	m_nDataItem = 0;
	m_pDoc = NULL;
	//}}AFX_DATA_INIT
}


void CMeterSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMeterSetDlg)
	DDX_Control(pDX, IDC_CHANNEL_COMBO, m_ctrlChannel);
	DDX_Control(pDX, IDC_MODULE_COMBO, m_ctrModuleID);
	DDX_CBIndex(pDX, IDC_PORT_COMBO, m_nPort);
	DDX_CBIndex(pDX, IDC_TYPE_COMBO, m_nMeterType);
	DDX_CBIndex(pDX, IDC_OP_COMBO, m_nOperator);
	DDX_Text(pDX, IDC_CONVERT_EDIT, m_fConvertData);
	DDX_CBIndex(pDX, IDC_OUT_COMBO, m_nDataItem);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMeterSetDlg, CDialog)
	//{{AFX_MSG_MAP(CMeterSetDlg)
	ON_BN_CLICKED(IDC_COM_SET_BUTTON, OnComSetButton)
	ON_CBN_SELCHANGE(IDC_MODULE_COMBO, OnSelchangeModuleCombo)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CONNECT_BUTTON, OnConnectButton)
	ON_BN_CLICKED(IDC_DISCONNECT_BUTTON, OnDisconnectButton)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_COMM_RXCHAR, OnCommunication)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMeterSetDlg message handlers

void CMeterSetDlg::OnComSetButton() 
{
	// TODO: Add your control notification handler code here
	m_SerialConfig.DoModal();
}

BOOL CMeterSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_SerialConfig.onLoadConfig();
	
	InitControl();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMeterSetDlg::InitControl()
{	
	CDaoDatabase  db;
	COleVariant data;
	CString strData = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
	//DataBase 폴더 생성

//	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
// 	if(nSelLanguage ==1 ) 20190701
// 	{ strTemp = strData + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME; }
// 	else if(nSelLanguage == 2)
// 	{ strTemp = strData + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; }
// 	else if(nSelLanguage == 3)	
// 	{ strTemp = strData + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_PL; }
// 	else { strTemp = strData + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; }

	CString strTemp = strData + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME;

	strData = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Module Name", "Module");
	
	try
	{
		db.Open(strTemp);

		CString strSQL("SELECT ModuleID, InstallChCount FROM SystemConfig ORDER BY ModuleID");
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		
		//Load Module Configuration
		int nCnt = 0;
		while(!rs.IsEOF())
		{
			rs.GetFieldValue("ModuleID", data);
			if(VT_NULL == data.vt)		break;

			strTemp.Format("%s %d", strData, data.lVal);
			m_ctrModuleID.AddString(strTemp);
			m_ctrModuleID.SetItemData(nCnt++  , data.lVal);
			
			rs.GetFieldValue("InstallChCount", data);
			if(VT_NULL != data.vt)
			{
				m_aInstallCh.Add((WORD)data.lVal);
			}
			else
			{
				m_aInstallCh.Add(SFT_DEFAULT_BD_PER_MD*SFT_DEFAULT_CH_PER_BD);
			}						
			rs.MoveNext();
		}
		rs.Close();
		db.Close();	
	}
	catch (CDaoException *e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
	}
}

void CMeterSetDlg::OnSelchangeModuleCombo() 
{
	// TODO: Add your control notification handler code here
	m_ctrlChannel.ResetContent();
	
	int a = m_ctrModuleID.GetCurSel();
	if(a >= 0 || a > m_aInstallCh.GetSize())
	{
		CString strTemp;
		for(int i=0; i<m_aInstallCh.GetAt(a); i++)
		{
			strTemp.Format("Ch %d", i+1);
			m_ctrlChannel.AddString(strTemp);
		}
	}
}

void CMeterSetDlg::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	CString buf;
	UpdateData(TRUE);
	
	if(m_SerialPort[0].IsMonitoring())
	{
		buf.Format("READ?\n");
		m_SerialPort[0].WriteToPort((LPSTR)(LPCTSTR)buf);
		
		//Read
/*		buf = m_SerialPort[0].ReadString();
		float fData1 = atof(buf);
		fData1 = ConvertData(fData1);

		int nSel = m_ctrModuleID.GetCurSel();
		if(nSel != CB_ERR)
		{
			CCyclerChannel *pChannel = m_pDoc->GetChannelInfo(m_ctrModuleID.GetItemData(nSel), m_ctrlChannel.GetCurSel());
			if(pChannel)
			{
				pChannel->SetMeterValue(fData1);
			}
		}
*/		
	}

	if(m_SerialPort[1].IsMonitoring())
	{
		buf.Format("READ?\n");
		m_SerialPort[1].WriteToPort((LPSTR)(LPCTSTR)buf);
		
/*		buf = m_SerialPort[1].ReadString();
		float fData2 = atof(buf);
		fData2 = ConvertData(fData2);
*/
	}

	CDialog::OnTimer(nIDEvent);
}

void CMeterSetDlg::OnConnectButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	int nSel = m_ctrModuleID.GetCurSel();
	if(nSel == CB_ERR)
	{
		AfxMessageBox(Fun_FindMsg("OnConnectButton_msg1","IDD_METER_CONTIG_DIALOG"));
		//@ AfxMessageBox("Data 적용 모듈이 선택되지 않았습니다.");
		return;
	}
	nSel = m_ctrlChannel.GetCurSel();
	if(nSel == CB_ERR)
	{
		AfxMessageBox(Fun_FindMsg("OnConnectButton_msg2","IDD_METER_CONTIG_DIALOG"));
		//@ AfxMessageBox("Data 적용 채널이 선택되지 않았습니다.");
		return;
	}

	if(ConnectPort(m_nPort) == FALSE)
	{
		CString str;
		GetDlgItem(IDC_PORT_COMBO)->GetWindowText(str);
		AfxMessageBox(str+Fun_FindMsg("OnConnectButton_msg3","IDD_METER_CONTIG_DIALOG"), MB_OK|MB_ICONSTOP);
		//@ AfxMessageBox(str+" 접속에 실패 하였습니다.", MB_OK|MB_ICONSTOP);
	}
	
	SetTimer(1234, 1000, NULL);
	GetDlgItem(IDC_CONNECT_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_DISCONNECT_BUTTON)->EnableWindow(TRUE);
}

BOOL CMeterSetDlg::ConnectPort(int nPort)
{
	ASSERT(m_pDoc);		//First call SetDocument();

	BOOL bConnect = FALSE;
	if(nPort < 0 || nPort >= 2 )	return FALSE;

	CString buf;
	SERIAL_CONFIG *pPortCfg = &m_SerialConfig.m_SerialConfig[nPort];
	BOOL flag = m_SerialPort[nPort].InitPort(this->m_hWnd,
				pPortCfg->nPortNum, 
				pPortCfg->nPortBaudRate,
				pPortCfg->portParity,
				pPortCfg->nPortDataBits,
				pPortCfg->nPortStopBits,
				pPortCfg->nPortEvents,
				pPortCfg->nPortBuffer
			);

	if( flag == FALSE )
	{
		bConnect = FALSE;
	}
	else
	{
	//	m_strReceived.Empty();
		m_SerialPort[nPort].StartMonitoring("\r\n");
		
		
		buf.Format("SYST:REM\n");
		m_SerialPort[nPort].WriteToPort((LPCTSTR)buf);
		Sleep(200);

	//	buf.Format("CONF:VOLT:DC DEF, DEF\n");
	//	m_Port.WriteToPort((LPCTSTR)buf);
	//	Sleep(200);

		buf.Format("SAMPLE:COUNT 1\n");
		m_SerialPort[nPort].WriteToPort((LPCTSTR)buf);
		bConnect = TRUE;
	}
	return bConnect;
}

void CMeterSetDlg::OnDisconnectButton() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_nPort < 0 || m_nPort >= 2 )	return;

	m_SerialPort[m_nPort].DisConnect();	

	KillTimer(1234);
	GetDlgItem(IDC_CONNECT_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_DISCONNECT_BUTTON)->EnableWindow(FALSE);

	int nSel = m_ctrModuleID.GetCurSel();
	if(nSel != CB_ERR)
	{
		CCyclerChannel *pChannel = m_pDoc->GetChannelInfo(m_ctrModuleID.GetItemData(nSel), m_ctrlChannel.GetCurSel());
		if(pChannel)
		{
			pChannel->SetMeterValue(0.0);
		}
	}


}

float CMeterSetDlg::ConvertData(float fData)
{
	float fRltData = 0.0f;
	switch(m_nOperator)
	{
	case 0:		fRltData = fData+m_fConvertData;	break;
	case 1:		fRltData = fData-m_fConvertData;	break;
	case 2:		fRltData = fData*m_fConvertData;	break;
	case 3:		fRltData = fData/m_fConvertData;	break;
	default :	fRltData = fData;	break;
	}

	return fRltData;
}

void CMeterSetDlg::SetDocument(CCTSMonProDoc *pDoc)
{
	ASSERT(pDoc);
	m_pDoc = pDoc;
}

LRESULT CMeterSetDlg::OnCommunication(WPARAM wParam, LPARAM lParam)
{
	char data = (char)wParam;
	TRACE("%c", (char)data);

	//Port 1처리
	if( m_SerialConfig.m_SerialConfig[0].nPortNum == UINT(lParam))
	{
		if(data == '\n')
		{
			float fData1 = (float)atof(m_strReceiveBuff);
			
			fData1 = ConvertData(fData1);
			int nSel = m_ctrModuleID.GetCurSel();
			if(nSel != CB_ERR)
			{
				CCyclerChannel *pChannel = m_pDoc->GetChannelInfo(m_ctrModuleID.GetItemData(nSel), m_ctrlChannel.GetCurSel());
				if(pChannel)
				{
					pChannel->SetMeterValue(fData1);
				}
			}

		}
		else
		{
			m_strReceiveBuff += data;
		}
	}
	
	return 0;
}