

//#include "ColorBox.h"
#include "MyGridWnd.h"
#include "GridComboBox.h"

#define COL_FLICKER_CODE		0
#define COL_FLICKER_MESSAGE		1
#define COL_FLICKER_COLOR		2
#define COL_FLICKER_PRIORITY	3
#define COL_FLICKER_BUZZERSTOP	4 //ksj 20201013
#define COL_FLICKER_USE			5

#pragma once
// FlickerConfigDlg 대화 상자입니다.

class CCTSMonProDoc;
class CFlickerConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CFlickerConfigDlg)

public:
	CFlickerConfigDlg(CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFlickerConfigDlg();
	virtual BOOL OnInitDialog();
	BOOL InitFlickerGrid();
	CString LoadDatabase(CString strSQL);
	BOOL ExecuteSQL(CString strSQL);	
	UINT MyMessageBox(char* szText, UINT nType);

	CMyGridWnd m_wndFlickerGrid;
	CGXComboBox* m_pBuzzerStopSelCombo;//ksj 20201013
	CGXComboBox* m_pPrioritySelCombo;//ksj 20201013

	CGridComboBox *m_pTestTypeCombo;
	CBrush m_backBrush;
	int m_iPrirority;
	int m_iUse;
	int m_iRow; //use컬럼 1인row 수
	int m_iStartRow; //시작 row
	int m_iEndRow;   //끝 row
	COLORREF m_cColor;
	BOOL m_bUseAdmin;
	CCTSMonProDoc* m_pDoc; //ksj 20201012

	CString m_strCodeDatabaseName;
// 대화 상자 데이터입니다.
	enum { IDD = IDD_FLICKER_CONFIG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	void OnShowChangeUse(CCmdUI* pCmdUI);
	afx_msg void OnBnClickedButton2();
	afx_msg LONG OnGridDbClicked(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam);
	afx_msg void OnColorUpdateALL();
	afx_msg void OnColorUseSetting();
	afx_msg LRESULT OnSelDragRowsDrop(WPARAM wParam, LPARAM lParam);
	afx_msg void OnChangeColor();
	afx_msg void OnChangePriority();
	afx_msg void OnChangeUse();
	afx_msg void OnChangeALL();
	afx_msg void OnBnClickedOk();
};
