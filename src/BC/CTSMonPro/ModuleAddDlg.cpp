// ModuleAddDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "ModuleAddDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg dialog


CModuleAddDlg::CModuleAddDlg(BOOL bModeSel /*=FALSE*/, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CModuleAddDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CModuleAddDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CModuleAddDlg::IDD3):
	(CModuleAddDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CModuleAddDlg)
	m_nModuleID = 1;
	//}}AFX_DATA_INIT
	m_bModeSel = bModeSel;
	m_strModuleName = "Module";
}


void CModuleAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModuleAddDlg)
	DDX_Control(pDX, IDC_MODULE_SEL_COMBO, m_ctrlModuleSel);
	DDX_Text(pDX, IDC_MODULENUM, m_nModuleID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModuleAddDlg, CDialog)
	//{{AFX_MSG_MAP(CModuleAddDlg)
	ON_EN_CHANGE(IDC_MODULENUM, OnChangeModulenum)
	ON_CBN_SELCHANGE(IDC_MODULE_SEL_COMBO, OnSelchangeModuleSelCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModuleAddDlg message handlers

void CModuleAddDlg::OnChangeModulenum() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox(Fun_FindMsg("OnChangeModulenum_msg","IDD_MODULE_ADD_DLG"));
		//@ AfxMessageBox("입력 가능한 모듈 ID가 아닙니다.");
	}
}

void CModuleAddDlg::OnOK() 
{
	// TODO: Add extra validation here
	int index =m_ctrlModuleSel.GetCurSel();
	if(index >= 0)
		m_nModuleID = m_ctrlModuleSel.GetItemData(index);	

	if(m_nModuleID <1 || m_nModuleID > 0x7FFF)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_MODULE_ADD_DLG"));
		//@ AfxMessageBox("입력 가능한 모듈 ID가 아닙니다.");
		return;
	}
	CDialog::OnOK();
}

BOOL CModuleAddDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(!m_bModeSel)
	{
		GetDlgItem(IDC_MODULE_SEL_COMBO)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MODULENUM)->SetFocus();
	}
	else
	{
		GetDlgItem(IDC_MODULENUM)->ShowWindow(SW_HIDE);

//		//Registry에서 PowerFormation DataBase 경로를 검색 
//		long rtn;
//		HKEY hKey = 0;
//		BYTE buf[512], buf2[512];
//		DWORD size = 511;
//		DWORD type;
//		
//		CString strDBPath;
//
//		rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\Formation System\\PowerFormation\\Path", 0, KEY_READ, &hKey);
//		if(ERROR_SUCCESS == rtn)
//		{	
//			//PowerFormation DataBase 경로를 읽어온다. 
//			rtn = ::RegQueryValueEx(hKey, "DataBase", NULL, &type, buf, &size);
//			::RegCloseKey(hKey);
//			
//			//
//			if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
//				strDBPath.Format("%s\\%s", buf, FORM_SET_DATABASE_NAME);
//		}
//
//		rtn = ::RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\Formation System\\PowerFormation\\FormSetting", 0, KEY_READ, &hKey);
//		if(ERROR_SUCCESS == rtn)
//		{
//			rtn = ::RegQueryValueEx(hKey, "Module Name", NULL, &type, buf, &size);
//			if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
//			{
//				m_strModuleName = buf;
//			}
//			size = 255;
//			rtn = ::RegQueryValueEx(hKey, "Group Name", NULL, &type, buf, &size);
//			if(ERROR_SUCCESS == rtn && (type == REG_SZ || type == REG_EXPAND_SZ))
//			{
//				m_strGroupName = buf;
//			}
//			size = 255;
//			rtn = ::RegQueryValueEx(hKey, "Module Per Rack", NULL, &type, buf, &size);
//			if(ERROR_SUCCESS == rtn && type == REG_DWORD)
//			{
//				sprintf((char *)buf2, "%ld", *(long *)buf);
//				m_nModulePerRack = atol((LPCTSTR)buf2);
//			}
//			size = 255;
//			rtn = ::RegQueryValueEx(hKey, "Use Rack Index", NULL, &type, buf, &size);
//			if(ERROR_SUCCESS == rtn && type == REG_DWORD)
//			{
//				sprintf((char *)buf2, "%ld", *(long *)buf);
//				m_bUseRackIndex = atol((LPCTSTR)buf2);
//			}
//			size = 255;
//			rtn = ::RegQueryValueEx(hKey, "Use Group", NULL, &type, buf, &size);
//			if(ERROR_SUCCESS == rtn && type == REG_DWORD)
//			{
//				sprintf((char *)buf2, "%ld", *(long *)buf);
//				m_bUseGroupSet = atol((LPCTSTR)buf2);
//			}
//			::RegCloseKey(hKey);
//		}

		int nCount = 0;
		CString strTemp;
		int nSelIndex = 0;
		CDaoDatabase  db;
		CString strDBPath;
		CString strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", ".\\");
	
		//DataBase 폴더 생성

//		int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

// 		switch(nSelLanguage) 20190701
// 		{
// 			case 1: strDBPath = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME; break;
// 			case 2: strDBPath = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; break;
// 			case 3: strDBPath = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_PL; break;
// 			default : strDBPath = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; break;
// 		}

		strDBPath = strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME;
		
		try
		{
			db.Open(strDBPath);

			CString strSQL("SELECT ModuleID FROM SystemConfig ORDER BY ModuleID");
			CDaoRecordset rs(&db);
				rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
				while(!rs.IsEOF())
				{
					COleVariant data = rs.GetFieldValue(0);
					strTemp.Format("%s", ModuleName(data.lVal));
					m_ctrlModuleSel.AddString(strTemp);
					
					if(data.lVal == m_nModuleID)
						nSelIndex = nCount;
					
					m_ctrlModuleSel.SetItemData(nCount++, data.lVal);
					rs.MoveNext();
				}
			rs.Close();
			db.Close();	
		}
		catch (CDaoException *e)
		{
		//	e->GetErrorMessage();
			e->Delete();
		}

		m_ctrlModuleSel.SetCurSel(nSelIndex);					//기본 Module 선택
		m_nModuleID = m_ctrlModuleSel.GetItemData(nSelIndex);	//현재 선택된 module ID
	}
		
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CModuleAddDlg::GetModuleID()
{
	return m_nModuleID;
}


CString CModuleAddDlg::ModuleName(int nModuleID, int /*nGroupIndex*/)
{
	CString strName;
	strName.Format("%s %d", m_strModuleName, nModuleID);
	return strName;
}

void CModuleAddDlg::OnSelchangeModuleSelCombo() 
{
	// TODO: Add your control notification handler code here
	int index =m_ctrlModuleSel.GetCurSel();
	if(index >= 0)
		m_nModuleID = m_ctrlModuleSel.GetItemData(index);	
	UpdateData(FALSE);
}

void CModuleAddDlg::SetModuleName(CString strName)
{
	m_strModuleName = strName;
}