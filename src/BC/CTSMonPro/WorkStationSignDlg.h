#if !defined(AFX_WORKSTATIONSIGNDLG_H__752DD309_EC56_4064_B19E_0441E44CF9B6__INCLUDED_)
#define AFX_WORKSTATIONSIGNDLG_H__752DD309_EC56_4064_B19E_0441E44CF9B6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WorkStationSignDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWorkStationSignDlg dialog
#include "resource.h"
class CWorkStationSignDlg : public CDialog
{
// Construction
public:
	CWorkStationSignDlg(CWnd* pParent = NULL);   // standard constructor
	CLabel	m_CntrlPlaceSign;
// Dialog Data
	//{{AFX_DATA(CWorkStationSignDlg)
	enum { IDD = IDD_WORKSTATIONSIGN_DLG };
	
	//CString	m_PlaceSign;
	//}}AFX_DATA

	void SetWinPos (CRect rect);
	void CWorkStationSignDlg::InitControlLbl();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkStationSignDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
  	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_VIRTUAL
	
// Implementation
protected:
// 	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//  afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	// Generated message map functions
	//{{AFX_MSG(CWorkStationSignDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKSTATIONSIGNDLG_H__752DD309_EC56_4064_B19E_0441E44CF9B6__INCLUDED_)
