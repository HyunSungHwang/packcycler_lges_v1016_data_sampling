#if !defined(AFX_LISTCTRLEX_H__6C4627A3_A693_41B4_ABE0_E142DA035480__INCLUDED_)
#define AFX_LISTCTRLEX_H__6C4627A3_A693_41B4_ABE0_E142DA035480__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListCtrlEx.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx window
#include <Afxtempl.h> 

#define IDC_STATIC_LIST WM_USER

class CListCtrlEx : public CListCtrl
{
// Construction
private:
	CBitmap bitmapHead;
	CBitmap bitmapBody;
	CBitmap bitmapTail;
	CArray<CStatic*,CStatic*> m_StaticList;
	int m_TypeList[256];
public:
	CListCtrlEx();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListCtrlEx)
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
		int m_StaticColumn;
	
	virtual ~CListCtrlEx();
	

	// Generated message map functions
protected:
	//{{AFX_MSG(CListCtrlEx)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	int InsertItem(const LVITEM * pItem, int nType);
	int InsertItem(const LVITEM * pItem);
	BOOL SetItem(const LVITEM * pItem, int nType);
	BOOL SetItem(const LVITEM * pItem);
	void SetTailBitmap(UINT nBitmapID);
	void SetBodyBitmap(UINT nBitmapID);
	void SetHeadBitmap(UINT nBitmapID);
	afx_msg void OnPaint();;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTCTRLEX_H__6C4627A3_A693_41B4_ABE0_E142DA035480__INCLUDED_)
