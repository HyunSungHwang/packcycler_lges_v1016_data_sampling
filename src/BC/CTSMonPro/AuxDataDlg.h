#if !defined(AFX_AUXDATADLG_H__1A36640B_1707_460D_B1FF_AA9CBB49C453__INCLUDED_)
#define AFX_AUXDATADLG_H__1A36640B_1707_460D_B1FF_AA9CBB49C453__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AuxDataDlg.h : header file
//
#include "CTSMonProDoc.h"
#include "cTree.h"
/////////////////////////////////////////////////////////////////////////////
// CAuxDataDlg dialog

#define IMAGE_ROOT_TREE_ITEM 0
#define IMAGE_UNIT_LINEON_TREE_ITEM 1
#define IMAGE_UNIT_LINEOFF_TREE_ITEM 12
#define IMAGE_AUX_TREE_ITEM 3
#define IMAGE_TEMPORARY_AUX_TREE_ITEM 5

class CAuxDataDlg : public CDialog
{
// Construction
private :
	CUIntArray m_uiArryAuxCode;		//ljb 2011222 이재복 //////////
	CStringArray m_strArryAuxName;	//ljb 2011222 이재복 //////////

	CUIntArray m_uiArryAuxThrCode;		//ljb 20160504 이재복 //////////
	CStringArray m_strArryAuxThrName;	//ljb 20160504 이재복 //////////

	CUIntArray m_uiArryAuxHumiCode;		//ksj 20200207
	CStringArray m_strArryAuxHumiName;	//ksj 20200207

	BOOL bColor[_SFT_MAX_MAPPING_AUX];
	BOOL IsChanged;
	HTREEITEM hRoot;
public:
	void Fun_ApplaySafety(long lCellMax, long lCellMin, long lAuxTempMax, long lAuxTempMin);
	CString Fun_GetAuxCodeName(int iCode);
	CString Fun_GetAuxThrCodeName(int iCode);
	CString Fun_GetAuxHumiCodeName(int iCode); //ksj 20200207
	void UpdateRemainAux(int nMoudle, HTREEITEM hUnit, HTREEITEM hItem, BOOL IsDelete);
	void ModifyAux();
	void UpdateRemainAllAux(int nCurrentModule, HTREEITEM hItem);
	void SaveAuxConfig();
	CString strSaveDir;
	int GetAuxType(CString strAuxName);
	CString GetAuxTypeName(int nAuxType);
	void UpdateAuxTreeItem(HTREEITEM hIem);
	void AddList(CChannelSensor * pSensor, BOOL bColor);
	void AddSensorListAuxItem(HTREEITEM hAuxItem, int nMasterCh);
	//BOOL SendAllAuxDataToModule();
	BOOL SendAllAuxDataToModule(BOOL bReset = FALSE); //ksj 20210512 : reset 기능 추가.
	void SendAuxDataCh(HTREEITEM hItem);
	void SendAuxDataModule(HTREEITEM hItem);
	CChannelSensor * GetSensorData(CString strItemName);
	int GetModuleID(CString strModuleName);
	//bFlag = TRUE : ADD Node //bFlag = FALSE : Delete Node
	void UpdateChannelAuxData(int nModuleID, int nSensorIndex, CChannelSensor * chSensorData, BOOL bInstall);
	void AddSensorListChannelItem(HTREEITEM hChItem);
	int GetStartChNo(int nModuleID);
	void UpdateChannelTree(int nCurrentModule, HTREEITEM hItem);
	void InitTreeList();
	void InitAuxDataMovePart();
	CAuxDataDlg(CCTSMonProDoc * pDoc, CWnd* pParent = NULL);   // standard constructor
	HTREEITEM FindTreeData(CTreeCtrl* pTree, HTREEITEM hitem, CString strTagetNM);
	HTREEITEM FindTreNodeStateSelected(CTreeCtrl* pTree, HTREEITEM hitem); //20180424 yulee add


// Dialog Data
	//{{AFX_DATA(CAuxDataDlg)
	enum { IDD = IDD_AUXDATA_DLG , IDD2 = IDD_AUXDATA_DLG_ENG , IDD3 = IDD_AUXDATA_DLG_PL };
	CComboBox	m_CboChList;
	CListCtrl	m_ctrlSensorList;
	cTree	m_ctrlAuxTree;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAuxDataDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAuxDataDlg)
	afx_msg void OnBtnAdd();
	afx_msg void OnBtnDel();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangedAuxTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonApply();
	afx_msg void OnClose();
	afx_msg void OnBtnModify();
	afx_msg void OnCustomdrawMyList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBtnAllApply();
	afx_msg void OnBtnCarbonApply();
	afx_msg void OnBtnLtoApply();
	afx_msg void OnBtnAllApplyTherSensor();
	afx_msg void OnBtnMove();
	//}}AFX_MSG
	afx_msg LRESULT OnItemDragAndDrop(WPARAM wParam, LPARAM LPARAM);
	DECLARE_MESSAGE_MAP()

private:
	CImageList * m_ModuleImage;
	CCTSMonProDoc * m_pDoc;
	UINT nCurrentModule;
public:
	afx_msg void OnBnClickedBtnInitialize();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUXDATADLG_H__1A36640B_1707_460D_B1FF_AA9CBB49C453__INCLUDED_)
