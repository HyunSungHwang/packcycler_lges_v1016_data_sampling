// SettingCanFdDlg_Transmit.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "SettingCanFdDlg_Transmit.h"
#include "RealEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSettingCanFdDlg_Transmit dialog
#define CAN_COL_NO				0
#define CAN_COL_ID				1
#define CAN_COL_TIME			2
#define CAN_COL_NAME			3
#define CAN_COL_STARTBIT		4
#define CAN_COL_BITCOUNT		5
#define CAN_COL_DLC				6	//ljb 20180323 add
#define CAN_COL_BYTE_ORDER		7
#define CAN_COL_DATATYPE		8
#define CAN_COL_FACTOR			9
#define CAN_COL_OFFSET			10
#define CAN_COL_DEFAULT			11
#define CAN_COL_DIVISION1		12
#define CAN_COL_DIVISION2		13
#define CAN_COL_DIVISION3		14


#define TAB_MASTER		0
#define TAB_SLAVE		1

#define ID_CAN_USER_RUN			301
#define ID_CAN_USER_STOP		302
#define ID_CAN_USER_PAUSE		303
#define ID_CAN_USER_CONTINUE	304
#define ID_CAN_USER_NEXT		305
#define ID_CAN_USER_GOTO		306
#define ID_CAN_USER_STATE		307

typedef struct CONFIGDATA
{
	char canID[8];
	char ExtID;
	char BaudRate;
	long CapVtg;
}ConfigFile;

CSettingCanFdDlg_Transmit::CSettingCanFdDlg_Transmit(CCTSMonProDoc * pDoc, CWnd* pParent /*=NULL*/)
//: CDialog(CSettingCanFdDlg_Transmit::IDD, pParent)
: CDialog(
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSettingCanFdDlg_Transmit::IDD):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSettingCanFdDlg_Transmit::IDD2):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSettingCanFdDlg_Transmit::IDD3):
		  (CSettingCanFdDlg_Transmit::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSettingCanFdDlg_Transmit)
	m_ckExtID = FALSE;
	m_strSelectCh = _T("");
	m_strCanID = _T("");
	m_strCanIDSlave = _T("");
	m_ckExtIDSlave = FALSE;
	m_nMasterIDType = -1;
	m_strType = _T("");
	m_nProtocolVer = 0;
	m_strDescript = _T("");
	m_edit_chargeOnVolt = 0.0f;
	m_edit_OnDelayTime = 0;
	m_edit_KeyonVoltMax = 0.0f;
	m_edit_KeyonVoltMin = 0.0f;
	m_edit_BmsRestartTime = 0.0f;
	m_ckCanFD = FALSE;
	m_ckCanFD_Slave = FALSE;
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	IsChange = FALSE;
	m_lMinCanTxTime = 20;
	m_bUseCanTxTimeLimit = TRUE;
}


void CSettingCanFdDlg_Transmit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingCanFdDlg_Transmit)
	DDX_Control(pDX, IDC_COMBO_TX_CAN_CRC_S, m_ctrlTXBMSCRCTypeSlave);
	DDX_Control(pDX, IDC_COMBO_TX_CAN_CRC_M, m_ctrlTXBMSCRCType);
	DDX_Control(pDX, IDC_COMBO_TX_CAN_TERMINAL_M2, m_ctrlBMSTXTerminalSlave);
	DDX_Control(pDX, IDC_COMBO_TX_CAN_TERMINAL_M, m_ctrlBMSTXTerminal);
	DDX_Control(pDX, IDC_COMBO_TX_CAN_DATARATE2, m_ctrlDataRateSlave);
	DDX_Control(pDX, IDC_COMBO_TX_CAN_DATARATE, m_ctrlDataRate);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_SJW_SLAVE, m_ctrlBmsSJWSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_SJW, m_ctrlBmsSJW);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_SJW, m_strBmsSJW);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_SJW_SLAVE, m_strBmsSJWSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_TYPE_SLAVE, m_ctrlBmsTypeSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_TYPE, m_ctrlBmsType);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_TYPE, m_strBmsType);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_TYPE_SLAVE, m_strBmsTypeSlave);
	DDX_Control(pDX, IDC_PARAM_TAB, m_ctrlParamTab);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE_SLAVE, m_ctrlBaudRateSlave);
	DDX_Control(pDX, IDC_STATIC_CH_MSG, m_ctrlLabelChMsg);
	DDX_Control(pDX, IDC_STATIC_SEL_CH_CAN, m_ctrlLabelChNo);
	DDX_Control(pDX, IDC_LABEL_MSG, m_ctrlLabelMsg);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE, m_ctrlBaudRate);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE, m_ckExtID);
	DDX_Text(pDX, IDC_STATIC_SEL_CH_CAN, m_strSelectCh);
	DDX_Text(pDX, IDC_EDIT_CAN_ID, m_strCanID);
	DDX_Text(pDX, IDC_EDIT_SLAVE_CANID, m_strCanIDSlave);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE_SLAVE, m_ckExtIDSlave);
	DDX_Radio(pDX, IDC_RADIO3, m_nMasterIDType);
	DDX_Text(pDX, IDC_EDIT_CHARGE_ON_VOLT, m_edit_chargeOnVolt);
	DDX_Text(pDX, IDC_EDIT_ON_DELAY_TIME, m_edit_OnDelayTime);
	DDX_Text(pDX, IDC_EDIT_KEYON_VOLT_MAX, m_edit_KeyonVoltMax);
	DDX_Text(pDX, IDC_EDIT_KEYON_VOLT_MIN, m_edit_KeyonVoltMin);
	DDX_Text(pDX, IDC_EDIT_BMS_RESTART_TIME, m_edit_BmsRestartTime);
	DDX_Check(pDX, IDC_CHECK_TX_CAN_FD, m_ckCanFD);
	DDX_Check(pDX, IDC_CHECK_TX_CAN_FD2, m_ckCanFD_Slave);
	//}}AFX_DATA_MAP
}



BEGIN_MESSAGE_MAP(CSettingCanFdDlg_Transmit, CDialog)
	//{{AFX_MSG_MAP(CSettingCanFdDlg_Transmit)
	ON_BN_CLICKED(IDC_BTN_LOAD_CAN_SETUP, OnBtnLoadCanSetup)
	ON_BN_CLICKED(IDC_BTN_ADD_MASTER, OnBtnAddMaster)
	ON_BN_CLICKED(IDC_BTN_DEL_MASTER, OnBtnDelMaster)
	ON_BN_CLICKED(IDC_BTN_SAVE_CAN_SETUP, OnBtnSaveCanSetup)
	ON_BN_CLICKED(IDC_BTN_ADD_SLAVE, OnBtnAddSlave)
	ON_BN_CLICKED(IDC_BTN_DEL_SLAVE, OnBtnDelSlave)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	ON_NOTIFY(TCN_SELCHANGE, IDC_PARAM_TAB, OnSelchangeParamTab)
	ON_BN_CLICKED(IDC_BUT_NOT_USE, OnButNotUse)
	ON_BN_CLICKED(IDC_CHECK_TX_CAN_FD, OnCheckTxCanFd)
	ON_BN_CLICKED(IDC_CHECK_TX_CAN_FD2, OnCheckTxCanFd2)
	ON_BN_CLICKED(IDC_CHECK_EXT_MODE, OnCheckExtMode)
	ON_BN_CLICKED(IDC_CHECK_EXT_MODE_SLAVE, OnCheckExtModeSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BAUDRATE, OnSelchangeComboCanBaudrate)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BMS_SJW, OnSelchangeComboCanBmsSjw)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BMS_TYPE, OnSelchangeComboCanBmsType)
	ON_CBN_SELCHANGE(IDC_COMBO_TX_CAN_DATARATE, OnSelchangeComboTxCanDatarate)
	ON_CBN_SELCHANGE(IDC_COMBO_TX_CAN_TERMINAL_M, OnSelchangeComboTxCanTerminalM)
	ON_CBN_SELCHANGE(IDC_COMBO_TX_CAN_CRC_M, OnSelchangeComboTxCanCrcM)
	ON_CBN_SELCHANGE(IDC_COMBO_TX_CAN_DATARATE2, OnSelchangeComboTxCanDatarate2)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BMS_SJW_SLAVE, OnSelchangeComboCanBmsSjwSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BMS_TYPE_SLAVE, OnSelchangeComboCanBmsTypeSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_CAN_BAUDRATE_SLAVE, OnSelchangeComboCanBaudrateSlave)
	ON_CBN_SELCHANGE(IDC_COMBO_TX_CAN_TERMINAL_M2, OnSelchangeComboTxCanTerminalM2)
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnGridBeginEdit)
	ON_MESSAGE(WM_GRID_PASTE, OnGridPaste)
	ON_CBN_SELCHANGE(IDC_COMBO_TX_CAN_CRC_S, OnSelchangeComboTxCanCrcS)
	//}}AFX_MSG_MAP
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_BMS_SJW, &CSettingCanFdDlg_Transmit::OnCbnEditchangeComboCanBmsSjw)
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_BMS_TYPE, &CSettingCanFdDlg_Transmit::OnCbnEditchangeComboCanBmsType)
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_BMS_SJW_SLAVE, &CSettingCanFdDlg_Transmit::OnCbnEditchangeComboCanBmsSjw)
	ON_CBN_EDITCHANGE(IDC_COMBO_CAN_BMS_TYPE_SLAVE, &CSettingCanFdDlg_Transmit::OnCbnEditchangeComboCanBmsType)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingCanFdDlg_Transmit message handlers

BOOL CSettingCanFdDlg_Transmit::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_nMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterTransIDType", 0);
	
	m_bUseCanTxTimeLimit= AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Can Tx Time Limit", TRUE); //ksj 20210305 : CAN 전송속도에 제한을 둘지 옵션
	m_lMinCanTxTime = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Min Can Tx Time", 20); //ksj 20210305 : 기본 값 레지스트리 세팅 (최소 값)
	

	//ljb 2011221 이재복 //////////
	//if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnInitDialog_msg1","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
	m_pDoc->Fun_GetArryCanDivision(uiArryCanCode);		//ljb 2011222 이재복 //////////
	m_pDoc->Fun_GetArryCanStringName(strArryCanName);	//ljb 2011222 이재복 //////////

	InitParamTab();
	InitGridMaster();
	InitGridSlave();
	InitLabel();

	m_strCanID = "FFFFFFFF";
	m_strCanIDSlave = "FFFFFFFF";
	//m_fCapVtg = 0.000;
	//m_fCapVtgSlave = 0.000;
	//m_fMcuTemp = 0.000;
	//m_fMcuTempSlave = 0.000;
	//m_strSelectCh.Format("%d 번", nSelCh+1);
	m_strSelectCh.Format(Fun_FindMsg("SettingCanFdDlg_Transmit_OnInitDialog_msg2","IDD_CAN_FD_SETTING_TRANSMIT"), nSelCh+1);//&&
	UpdateData(FALSE);
	m_ctrlBaudRate.SetCurSel(3);
	m_ctrlBaudRateSlave.SetCurSel(3);
	
	m_ctrlBmsType.SetCurSel(0);
	m_ctrlBmsTypeSlave.SetCurSel(0);

	m_ctrlBmsSJW.SetCurSel(2);
	m_ctrlBmsSJWSlave.SetCurSel(2);

	this->SetWindowText(m_strTitle);	
	UpdateCommonData();
	UpdateGridData();	
	
	//OnCheckTxCanFd(); //20180525 yulee 주석처리 

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if(pMD == NULL)	return FALSE;
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return FALSE;
	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
		//if(pMD->GetState() == PS_STATE_LINE_OFF)
	{
		//m_ctrlLabelMsg.SetText("동작중인 채널은 수정할 수 없습니다.");
		m_ctrlLabelMsg.SetText(Fun_FindMsg("SettingCanFdDlg_Transmit_OnInitDialog_msg4","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		EnableAllControl(FALSE);
		//return;
	}

	/*if (gst_CanRxSave.bEnable == TRUE)
	{
		if (gst_CanRxSave.CanType == 1)
		{
			m_ctrlBaudRate.SetCurSel(gst_CanRxSave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->SetCheck(gst_CanRxSave.CanRxExtendedIDMode);
			m_ctrlBmsSJW.SetCurSel(gst_CanRxSave.SJW);
			m_ctrlBmsType.SetCurSel(gst_CanRxSave.BMSType);
		}
		memcpy(&gst_CanTxSave, &gst_CanRxSave, sizeof(CAN_RX_SAVE));
	}

	if (gst_CanRxSaveSlave.bEnable == TRUE)
	{
		if (gst_CanRxSaveSlave.CanType == 1)
		{
			m_ctrlBaudRateSlave.SetCurSel(gst_CanRxSaveSlave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->SetCheck(gst_CanRxSaveSlave.CanRxExtendedIDMode);
			m_ctrlBmsSJWSlave.SetCurSel(gst_CanRxSaveSlave.SJW);
			m_ctrlBmsTypeSlave.SetCurSel(gst_CanRxSaveSlave.BMSType);
		}
		memcpy(&gst_CanTxSaveSlave, &gst_CanRxSaveSlave, sizeof(CAN_RX_SAVE));
	}*/
		
	//ksj 20210210 : ??? 위에 왜 주석처리 해놨지??.??? 다시 살림...
	//요주의 관찰 필요.
	//ksj 20210712 : 무슨 기능인지 이력도 없고, 현재 사용에 방해가 되므로 일단 제거함
	//향후 필요한 경우 정확이 어떻게 필요한지 다시 파악하여 재구현 하더라도 일단 제거
	/*if (gst_CanRxSave.bEnable == TRUE)
	{
		//if (gst_CanRxSave.CanType == 1)
		if (gst_CanRxSave.CanType == nLINtoCANSelect)  //ksj 20210712 : 기존 코드 의도를 잘 모르는데 CAN RX 설정 창쪽 코드랑 동일하게 일단 고쳐봄.
		{
			m_ctrlBaudRate.SetCurSel(gst_CanRxSave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->SetCheck(gst_CanRxSave.CanRxExtendedIDMode);
			m_ctrlBmsSJW.SetCurSel(gst_CanRxSave.SJW);
			m_ctrlBmsType.SetCurSel(gst_CanRxSave.BMSType);

			//ksj 20210203 : List에 없는 값은 SetCurSel로 선택 안됨.
			//없는 값은 SetWindowText로 값을 강제로 넣어줘야함.
			CString strTemp;
			strTemp.Format("%d",gst_CanRxSave.SJW);
			m_ctrlBmsSJW.SetWindowText(strTemp);
			strTemp.Format("%d",gst_CanRxSave.BMSType);
			m_ctrlBmsType.SetWindowText(strTemp);
			//ksj end	
		}
		memcpy(&gst_CanTxSave, &gst_CanRxSave, sizeof(CAN_RX_SAVE));
	}

	if (gst_CanRxSaveSlave.bEnable == TRUE)
	{
		//if (gst_CanRxSaveSlave.CanType == 1)
		if (gst_CanRxSaveSlave.CanType == nLINtoCANSelect) //ksj 20210712 : 기존 코드 의도를 잘 모르는데 CAN RX 설정 창쪽 코드랑 동일하게 일단 고쳐봄.
		{
			m_ctrlBaudRateSlave.SetCurSel(gst_CanRxSaveSlave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->SetCheck(gst_CanRxSaveSlave.CanRxExtendedIDMode);
			m_ctrlBmsSJWSlave.SetCurSel(gst_CanRxSaveSlave.SJW);
			m_ctrlBmsTypeSlave.SetCurSel(gst_CanRxSaveSlave.BMSType);

			//ksj 20210203 : List에 없는 값은 SetCurSel로 선택 안됨.
			//없는 값은 SetWindowText로 값을 강제로 넣어줘야함.
			CString strTemp;
			strTemp.Format("%d",gst_CanRxSaveSlave.SJW);
			m_ctrlBmsSJWSlave.SetWindowText(strTemp);
			strTemp.Format("%d",gst_CanRxSaveSlave.BMSType);
			m_ctrlBmsTypeSlave.SetWindowText(strTemp);
			//ksj end
		}
		memcpy(&gst_CanTxSaveSlave, &gst_CanRxSaveSlave, sizeof(CAN_RX_SAVE));
	}*/

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
void CSettingCanFdDlg_Transmit::EnableAllControl(BOOL IsEnable)
{
	//m_wndCanGrid.EnableWindow(IsEnable);
	//m_wndCanGridSlave.EnableWindow(IsEnable);
	m_wndCanGrid.EnableEdit(IsEnable);
		
//	GetDlgItem(IDC_EDIT_CAN_ID)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_EXT_MODE)->EnableWindow(IsEnable);
	
	GetDlgItem(IDC_CHECK_TX_CAN_FD)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_TX_CAN_CRC_M)->EnableWindow(IsEnable);
	
	GetDlgItem(IDC_EDIT_CAPVTG)->EnableWindow(IsEnable);
	
	GetDlgItem(IDC_BTN_ADD_MASTER)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_DEL_MASTER)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_LOAD_CAN_SETUP)->EnableWindow(IsEnable);

	//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
	//if(!bUseCANFD)
	//{
	//	
	//}
	//else
	//{
	//	GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(TRUE);
	//	GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(TRUE);
	//}

	//ksj 20200213 : 위에 왜 주석처리 되어있고 if 조건이 바뀌어있는지 모르겠다.
    //뭔가 비정상동작하도록 꼬아놓은 것 같아 개선 수정.
	//ShowCanFDCtrl(); 
	Fun_ChangeViewControl(TRUE); //ksj 20200310 : Master Tab 선택되어있는 것만 활성화

}
void CSettingCanFdDlg_Transmit::InitLabel()
{
	m_ctrlLabelMsg.SetTextColor(RGB(255, 0, 0))
		.FlashText(TRUE)
		.SetFontAlign(0)
		.SetFontName("HY헤드라인M");//yulee 20190405 check
	
	m_ctrlLabelChNo.SetTextColor(RGB(0, 0, 255))
		.SetFontName("HY헤드라인M");//yulee 20190405 check
	m_ctrlLabelChMsg.SetTextColor(RGB(0, 0, 0))
		.SetFontName("HY헤드라인M");//yulee 20190405 check
	
				
}
void CSettingCanFdDlg_Transmit::IsChangeFlag(BOOL IsFlag)
{
	IsChange = IsFlag;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);	
/*#ifdef NDEBUG //ksj 20210115 : 주석처리함
	if(pMD == NULL || pMD->GetState() == PS_STATE_LINE_OFF)	
	{
		return;
	}
	else
#endif NDEBUG
	{
		GetDlgItem(IDOK)->EnableWindow(IsChange);
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}
	*/

	//ksj 20210115 : SAVE 버튼만 활성화
	if(pMD == NULL || pMD->GetState() == PS_STATE_LINE_OFF)	
	{
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}
	else
	{
		GetDlgItem(IDOK)->EnableWindow(IsChange);
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}
}
LRESULT CSettingCanFdDlg_Transmit::OnGridPaste(WPARAM wParam, LPARAM lParam)
{
	IsChangeFlag(TRUE);
	return 1;
}

LRESULT CSettingCanFdDlg_Transmit::OnGridBeginEdit(WPARAM wParam, LPARAM lParam)
{
	IsChangeFlag(TRUE);
	return 1;
}
void CSettingCanFdDlg_Transmit::InitGridMaster()			
{	

	//ljb	2008-09-04 CAN_COL_McuTempFlag 추가
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;
	
	SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nSelCh, PS_CAN_TYPE_MASTER);

	m_wndCanGrid.SubclassDlgItem(IDC_CAN_GRID, this);
	m_wndCanGrid.Initialize();

	CRect rect;
	m_wndCanGrid.GetParam()->EnableUndo(FALSE);
	
	m_wndCanGrid.SetRowHeight(0,0,50); //lyj 20200317 컬럼사이즈 조정
	//m_wndCanGrid.SetRowCount(1);
	m_wndCanGrid.SetRowCount(0); //ksj 20210305
	m_wndCanGrid.SetColCount(CAN_COL_DIVISION3);		//ljb
	m_wndCanGrid.GetClientRect(&rect);
	m_wndCanGrid.SetColWidth(CAN_COL_NO			, CAN_COL_NO			, 30);
	m_wndCanGrid.SetColWidth(CAN_COL_ID			, CAN_COL_ID			, 60);
	//m_wndCanGrid.SetColWidth(CAN_COL_TIME		, CAN_COL_TIME			, 80);
	m_wndCanGrid.SetColWidth(CAN_COL_TIME		, CAN_COL_TIME			, 82); //lyj 20200317 컬럼사이즈 조정
	//m_wndCanGrid.SetColWidth(CAN_COL_NAME		, CAN_COL_NAME			, rect.Width() - 690);
	m_wndCanGrid.SetColWidth(CAN_COL_NAME		, CAN_COL_NAME			, 120);
	m_wndCanGrid.SetColWidth(CAN_COL_STARTBIT	, CAN_COL_STARTBIT		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_BITCOUNT	, CAN_COL_BITCOUNT		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_DLC		, CAN_COL_DLC			, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_BYTE_ORDER	, CAN_COL_BYTE_ORDER	, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_DATATYPE	, CAN_COL_DATATYPE		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_FACTOR		, CAN_COL_FACTOR		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_OFFSET		, CAN_COL_OFFSET		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_DEFAULT	, CAN_COL_DEFAULT		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION1	, CAN_COL_DIVISION1		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION2	, CAN_COL_DIVISION2		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION3	, CAN_COL_DIVISION3		, 100);
	

	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_NO			),	CGXStyle().SetValue("No"));
	if(m_nMasterIDType == 1)
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	else
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));

	//m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue("전송속도(ms)"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg1","IDD_CAN_FD_SETTING_TRANSMIT")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_NAME			),	CGXStyle().SetValue("Name"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT		),	CGXStyle().SetValue("StartBit"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT		),	CGXStyle().SetValue("BitCount"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DLC			),	CGXStyle().SetValue("DLC"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER	),	CGXStyle().SetValue("ByteOrder"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE		),	CGXStyle().SetValue("DataType"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_FACTOR		),	CGXStyle().SetValue("Factor"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_OFFSET		),	CGXStyle().SetValue("Offset"));
	/*
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue("설정값"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue("분류 1"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue("분류 2"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue("분류 3"));
	*/
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg2","IDD_CAN_FD_SETTING_TRANSMIT")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg3","IDD_CAN_FD_SETTING_TRANSMIT")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg4","IDD_CAN_FD_SETTING_TRANSMIT")));//&&
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg5","IDD_CAN_FD_SETTING_TRANSMIT")));//&&
	
	m_wndCanGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndCanGrid.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(0),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_ID, CAN_COL_TIME),
		CGXStyle()
  			.SetControl(GX_IDS_CTRL_EDIT)
	);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// 		.SetChoiceList(_T("USER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\n"))
// 		.SetValue(_T(""))
// 	);
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_EDIT)
// 		);

	m_wndCanGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndCanGrid, IDS_CTRL_REALEDIT));
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	
	char str[12], str2[7], str3[5];

	sprintf(str, "%%d", 1);		
	//sprintf(str2, "%d", 2);	//정수부 //yulee 20180329 권준구c 요청 정수부 크기 2->3 
	sprintf(str2, "%d", 3);		//정수부 2->3

	//.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63")) 63 -> 511로 변경
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("511"))
			//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~511)"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg6","IDD_CAN_FD_SETTING_TRANSMIT"))//&&
			.SetValue(0L)
			
	);

	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(1L)
	);

	//DLC
// 	if (commonData.can_fd_flag == 0)
// 	{
// 		m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DLC),
// 			CGXStyle()
// 			.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
// 			.SetChoiceList(_T("0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n"))
// 			.SetValue(_T("8"))
// 			);
// 	}
// 	else
// 	{
		m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DLC),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX) //20180329 yulee 권준구c 요청 - 키보드로 입력가능하도록(콤보박스 리스트에 해당하는 값만 입력 가능)
			.SetChoiceList(_T("0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n12\r\n16\r\n20\r\n24\r\n32\r\n48\r\n64\r\n"))
			.SetValue(_T("8"))
			);
//	}

	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
	   CGXStyle()
		   .SetControl(IDS_CTRL_REALEDIT)
		   .SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		   .SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		   .SetValue(1L)
	);

	//전송 속도 기본값자 자리수 셋팅
	sprintf(str, "%%d", 1);		//정수부
	sprintf(str2, "%d", 5);		
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_TIME),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		//.SetValue(10L)
		.SetValue(m_lMinCanTxTime) //ksj 20210305
	);

	sprintf(str, "%%.%dlf", 10);	//실수 포멧			ljb 20160825 박진호 요청 6->10
	sprintf(str2, "%d", 6);		    //정수부 크기		ljb 20160825 박진호 요청 6->7
	sprintf(str3, "%d", 10);		//소수부 크기		ljb 20160825 박진호 요청 6->10

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_FACTOR),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1L)
	);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);
	//기본 전송 값, 기본값자 자리수 셋팅
	sprintf(str3, "%d", 6);		//소숫점 이하		// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str, "%%.%dlf", 6);						// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str2, "%d", 8);		//정수부			//ljb 12->8  [8/29/2011 XNOTE]
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);

	//division 전송 값, 기본값자 자리수 셋팅
	sprintf(str, "%%d", 2);		
	sprintf(str2, "%d", 4);		//정수부
	
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("1000"))
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
		.SetValue(0L)
		);


// 	sprintf(str3, "%d", 6);		//소숫점 이하
// 	sprintf(str, "%%.%dlf", 5);		
// 	sprintf(str2, "%d", 8);		//정수부
// 
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_FAULT_UPPER, CAN_COL_END_LOWER),
// 		CGXStyle()
// 			.SetControl(IDS_CTRL_REALEDIT)
// 			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
// 			.SetValue(0L)
// 	);

// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_CVFLAG),
// 		CGXStyle()
// 			.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 			.SetValue(0L)
// 	);
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_CapVtgFlag),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 		.SetValue(0L)
// 	);
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_McuTempFlag),			
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 		.SetValue(0L)
// 		);
	///////////////////////////////////////////////////

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
				.SetValue(_T("Intel"))
		);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
				//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\n"))
				.SetValue(_T("Unsigned"))
		);

	//ljb 2011222 이재복 S //////////
// 	CString strComboItem,strTemp;
// 	strComboItem = "None[0]\r\n";
// 	for (int i=0; i < strArryCanName.GetSize() ; i++)
// 	{
// 		strTemp.Format("[%d]",uiArryCanCode.GetAt(i));
// 		strComboItem = strComboItem + strArryCanName.GetAt(i) + "\r\n";		
// 	}
	CString strComboItem,strTemp,strTemp2;
	strComboItem = "None [0]\r\n";
	for (int i=0; i < strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",strArryCanName.GetAt(i),uiArryCanCode.GetAt(i));
		strTemp2 = strTemp.Left(2);
		strTemp2.MakeUpper();

		if (strTemp2 == "TX") strComboItem = strComboItem + strTemp;
	}
	//////////////////////////////////////////////////////////////////////////
	
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		//		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetControl(GX_IDS_CTRL_COMBOBOX)			//쓰기도 가능 //yulee 20190714 check
// 		.SetChoiceList(_T("NONE\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
//		.SetChoiceList(strComboItem)
//		.SetChoiceList(_T("NONE\r\n"))
		.SetChoiceList(strComboItem)
		.SetValue(_T("None [0]"))
		);

}

void CSettingCanFdDlg_Transmit::OnOK() 
{
	CString strTemp,strCode;
	int		nLength;

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CanMaterTransIDType", m_nMasterIDType);

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return;
	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END )
	{
		//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg1","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}
// 	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
// 	{
// 		AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
// 		return;
// 	}

	UpdateData();

	SFT_TRANS_CMD_CHANNEL_CAN_SET canTransSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canTransSetData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_TRANS_DATA commonTransData;
	ZeroMemory(&commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));

	if(m_ctrlBaudRate.GetCurSel() != LB_ERR)
	{
		commonTransData.can_baudrate = m_ctrlBaudRate.GetItemData(m_ctrlBaudRate.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Master BaudRate 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg2","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}
	if(m_ctrlDataRate.GetCurSel() != LB_ERR)
	{
		commonTransData.can_datarate = m_ctrlDataRate.GetItemData(m_ctrlDataRate.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Master DataRate 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg3","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}
	if(m_ctrlBMSTXTerminal.GetCurSel() != LB_ERR)
	{
		commonTransData.terminal_r = m_ctrlBMSTXTerminal.GetItemData(m_ctrlBMSTXTerminal.GetCurSel());
	}
	else
	{
		//AfxMessageBox("종단 저항 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg4","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}
	if(m_ctrlTXBMSCRCType.GetCurSel() != LB_ERR)
	{
		commonTransData.crc_type = m_ctrlTXBMSCRCType.GetItemData(m_ctrlTXBMSCRCType.GetCurSel());
	}
	else
	{
		//AfxMessageBox("CRC 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg5","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}

	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181127
	if(bUseCANFD)
	{
		commonTransData.can_fd_flag = m_ckCanFD;
	}
	else
	{
		commonTransData.can_fd_flag = 0;
	}
//	commonTransData.can_fd_flag = m_ckCanFD;		//ljb 20180326 add
	commonTransData.extended_id	= m_ckExtID;
	commonTransData.controller_canID = strtoul(m_strCanID,(char **)NULL,  16);
	//commonTransData.bms_type = m_ctrlBmsType.GetCurSel();
	//commonTransData.bms_sjw = m_ctrlBmsSJW.GetCurSel();

	//ksj 20210210 : GetCurSel은 목록에 없는 값을 가져오지는 못한다. 실제로 입력된 값을 불러와서 처리.
	m_ctrlBmsType.GetWindowText(strTemp);
	commonTransData.bms_type = atoi(strTemp);
	m_ctrlBmsSJW.GetWindowText(strTemp);
	commonTransData.bms_sjw = atoi(strTemp);
	//ksj end

	commonTransData.can_datarate = m_ctrlDataRate.GetCurSel();	//20180329 yulee add
	//commonTransData.terminal_r = m_ctrlTXBMSCRCType.GetCurSel();
	commonTransData.terminal_r = m_ctrlBMSTXTerminal.GetCurSel(); //ksj 20200306
	commonTransData.crc_type = m_ctrlTXBMSCRCType.GetCurSel();
	//ljb 20181115 S CAN,LIN select 
	commonTransData.can_lin_select	= nLINtoCANSelect;
	//ljb 20181115 E CAN,LIN select 

	//ksj 20200310 : CAN FD 사용 안할때는 강제로 값 고정하여 전송
	if(!bUseCANFD)
	{
		commonTransData.can_datarate = 0;
		commonTransData.terminal_r	= 1;
		commonTransData.crc_type	= 1;
	}
	//ksj end

	//ljb 201011
	commonTransData.can_function_division[0] = 1801;		//charge on volt
	commonTransData.can_function_division[1] = 1802;		//on delay time
	commonTransData.can_function_division[2] = 1803;		//key on volt min
	commonTransData.can_function_division[3] = 1804;		//key on volt max
	commonTransData.can_function_division[4] = 1805;		//BMS restart time
	commonTransData.can_compare_type[0] = 3;	// 3: ==
	commonTransData.can_compare_type[1] = 3;	
	commonTransData.can_compare_type[2] = 3;	
	commonTransData.can_compare_type[3] = 3;	
	commonTransData.can_compare_type[4] = 3;	

	commonTransData.can_data_type[0] = 2;	// 2:float
	commonTransData.can_data_type[1] = 2;	
	commonTransData.can_data_type[2] = 2;
	commonTransData.can_data_type[3] = 2;
	commonTransData.can_data_type[4] = 2;

	commonTransData.fcan_Value[0] = m_edit_chargeOnVolt;
	commonTransData.fcan_Value[1] = (float)m_edit_OnDelayTime;
	commonTransData.fcan_Value[2] = m_edit_KeyonVoltMin;
	commonTransData.fcan_Value[3] = m_edit_KeyonVoltMax;
	commonTransData.fcan_Value[4] = m_edit_BmsRestartTime;
	
	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_MASTER);
	memcpy(&canTransSetData.canCommonData, &commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));	//ljb 201008

	// SLAVE Setting ////////////////////////////////////////////////////////////////////////////////////////////
	ZeroMemory(&commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));
	if(m_ctrlBaudRateSlave.GetCurSel() != LB_ERR)
	{
		commonTransData.can_baudrate = m_ctrlBaudRateSlave.GetItemData(m_ctrlBaudRateSlave.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Slave BaudRate 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg6","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}
	if(m_ctrlDataRateSlave.GetCurSel() != LB_ERR)
	{
		commonTransData.can_datarate = m_ctrlDataRateSlave.GetItemData(m_ctrlDataRateSlave.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Slave DataRate 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg7","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}
	if(m_ctrlBMSTXTerminalSlave.GetCurSel() != LB_ERR)
	{
		commonTransData.terminal_r = m_ctrlBMSTXTerminalSlave.GetItemData(m_ctrlBMSTXTerminalSlave.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Slave 종단 저항 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg8","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}
	if(m_ctrlTXBMSCRCTypeSlave.GetCurSel() != LB_ERR)
	{
		commonTransData.crc_type = m_ctrlTXBMSCRCTypeSlave.GetItemData(m_ctrlTXBMSCRCTypeSlave.GetCurSel());
	}
	else
	{
		//AfxMessageBox("Slave CRC type 설정을 다시하세요.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg9","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}

	bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181127
	if(bUseCANFD)
	{
		commonTransData.can_fd_flag = m_ckCanFD_Slave;
	}
	else
	{
		commonTransData.can_fd_flag = 0;
	}
//	commonTransData.can_fd_flag = m_ckCanFD_Slave;	//ljb 20180326 add
	commonTransData.extended_id	= m_ckExtIDSlave;
//	commonTransData.fCapVtg	= m_fCapVtgSlave;
//	commonTransData.fMcuTemp	= m_fMcuTempSlave;
	commonTransData.controller_canID = strtoul(m_strCanIDSlave,(char **)NULL,  16);
// 	commonTransData.bms_type = m_ctrlBmsTypeSlave.GetCurSel();
// 	commonTransData.bms_sjw = m_ctrlBmsSJWSlave.GetCurSel();

	//ksj 20210210 : GetCurSel은 목록에 없는 값을 가져오지는 못한다. 실제로 입력된 값을 불러와서 처리.
	m_ctrlBmsTypeSlave.GetWindowText(strTemp);
	commonTransData.bms_type = atoi(strTemp);
	m_ctrlBmsSJWSlave.GetWindowText(strTemp);
	commonTransData.bms_sjw = atoi(strTemp);
	//ksj end

	commonTransData.can_datarate = m_ctrlDataRateSlave.GetCurSel();	//20180329 yulee add
	//commonTransData.terminal_r = m_ctrlTXBMSCRCTypeSlave.GetCurSel();
	commonTransData.terminal_r = m_ctrlBMSTXTerminalSlave.GetCurSel(); //ksj 20200306 : CRC에서 잘못가져오던 것 수정.
	commonTransData.crc_type = m_ctrlTXBMSCRCTypeSlave.GetCurSel();
	//ljb 20181115 S CAN,LIN select 
	commonTransData.can_lin_select	= nLINtoCANSelect;
	//ljb 20181115 E CAN,LIN select 

	//ksj 20200310 : CAN FD 사용 안할때는 강제로 값 고정하여 전송
	if(!bUseCANFD)
	{
		commonTransData.can_datarate = 0;
		commonTransData.terminal_r	= 1;
		commonTransData.crc_type	= 1;
	}
	//ksj end

	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_SLAVE);
	memcpy(&canTransSetData.canCommonData[1], &commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));	//ljb 201008

	int nMasterCount = 0;
	SFT_CAN_SET_TRANS_DATA canTransData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canTransData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);

	BOOL bCanTxTimeCheck = FALSE; //ksj 20210305

	for(int i=0; i<m_wndCanGrid.GetRowCount(); i++)
	{		
		CString strTemp;
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_ID);
		if(strTemp == "")
		{
			continue;
		}
// 		if(strTemp == "0") //20180528 yulee  //ksj 20210705 : 주석처리하여 0입력 가능하게 허용. 0입력 필요하다고함.
// 		{
// 			continue;
// 		}
		//canData[i].canID = strtoul(strTemp,(char **)NULL,  16);
		if(m_nMasterIDType == 0)		//16진수로 되어있으면 10진수로 변환
		{
			CString str10;
			
			str10.Format("%d", strtoul(strTemp , NULL, 16));
			strTemp = str10;
		}
		//canTransData[i].canID = atol(strTemp);
		canTransData[i].canID = strtoul(strTemp,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.
		canTransData[i].canType = PS_CAN_TYPE_MASTER;
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_NAME);	
		strTemp.TrimLeft();
		if(strTemp.IsEmpty()) 	strTemp.Format("CAN %d",i+1); //ksj 20210305 : 이름이 없으면 임의로 넣음.
		sprintf(canTransData[i].name, strTemp);		
		canTransData[i].startBit = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_STARTBIT));
		canTransData[i].bitCount = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BITCOUNT));
		//ljb 20180326 add DLC 추가
		canTransData[i].dlc = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DLC));

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER);
		if(strTemp == "Motorola")
			canTransData[i].byte_order = 1;
		else
			canTransData[i].byte_order = 0;		
		
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DATATYPE);
		if(strTemp == "Unsigned")
			canTransData[i].data_type = 0;
		else if(strTemp == "Signed")
			canTransData[i].data_type = 1;
		else if(strTemp == "Float")
			canTransData[i].data_type = 2;
		else if(strTemp == "String")
			canTransData[i].data_type = 3;

		canTransData[i].factor_multiply = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_FACTOR));
		canTransData[i].factor_Offset = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_OFFSET));
		
		canTransData[i].default_fValue = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DEFAULT));


		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1)) //yulee 20181119
		{
			//strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION1);
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[i].function_division = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division = atoi(strTemp);
		}

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[i].function_division2 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division2 = atoi(strTemp);
		}

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[i].function_division3 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division3 = atoi(strTemp);
		}



/*
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[i].function_division = atoi(strCode);
		
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[i].function_division2 = atoi(strCode);
		
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[i].function_division3 = atoi(strCode);
		*/
		canTransData[i].send_time = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_TIME));

		//ksj 20210305 : Can 전송속도 입력 Validation 추가.
		if(m_bUseCanTxTimeLimit) //Validation 여부.
		{
			//최소 허용 입력치 미만의 값은 최소값으로 강제 변경 (SBC 기능상 제한)
			if(canTransData[i].send_time < m_lMinCanTxTime)
			{
				canTransData[i].send_time = m_lMinCanTxTime;
				m_wndCanGrid.SetValueRange(CGXRange(i+1, CAN_COL_TIME), m_lMinCanTxTime);

				bCanTxTimeCheck = TRUE;
			}

			//20ms 의 배수만 입력 허용. (SBC 기능상 제한)
			if(canTransData[i].send_time % 20 != 0)
			{
// 				strTemp.Format("No %d 항목의 전송속도를 확인해주세요.\n전송속도는 20ms의 배수로만 입력 가능합니다.",i+1);
// 				AfxMessageBox(strTemp);

				//자동으로 20의 배수로 변환함. (버림)
				canTransData[i].send_time = int(canTransData[i].send_time/20)*20;
				m_wndCanGrid.SetValueRange(CGXRange(i+1, CAN_COL_TIME), canTransData[i].send_time);

				bCanTxTimeCheck = TRUE;
			}
		}		
		//ksj end
		nMasterCount++;
	}

	//ksj 20210305
	if(bCanTxTimeCheck)
	{
		//자동으로 보정이 이루어진뒤 안내.
		//다국어 번역 필요
	//	strTemp.Format("전송속도는 최소 %dms 이상, 20ms의 배수로만 입력 가능합니다.",m_lMinCanTxTime);
	//	AfxMessageBox(strTemp);
	}

	int nSlaveCount = 0;
	for(int i=0; i<m_wndCanGridSlave.GetRowCount(); i++)
	{		
		int nIndex = i + nMasterCount;
		CString strTemp;
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_ID);
		if(strTemp == "")
		{
			continue;
		}
		//canData[nIndex].canID = strtoul(strTemp,(char **)NULL,  16);
		if(m_nMasterIDType == 0)		//16진수로 되어있으면 10진수로 변환
		{
			CString str10;
			
			str10.Format("%d", strtoul(strTemp , NULL, 16));
			strTemp = str10;
		}
		//canTransData[nIndex].canID = atol(strTemp);
		canTransData[nIndex].canID = strtoul(strTemp,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.
		canTransData[nIndex].canType = PS_CAN_TYPE_SLAVE;
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_NAME);
		strTemp.TrimLeft();
		if(strTemp.IsEmpty()) 	strTemp.Format("CAN %d",i+1); //ksj 20210305 : 이름이 없으면 임의로 넣음.
		sprintf(canTransData[nIndex].name, strTemp);		
		canTransData[nIndex].startBit = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_STARTBIT));
		canTransData[nIndex].bitCount = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BITCOUNT));
		canTransData[nIndex].dlc = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DLC));	//ljb 20180326 add
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER);
		if(strTemp == "Motorola")
			canTransData[nIndex].byte_order = 1;
		else
			canTransData[nIndex].byte_order = 0;		
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DATATYPE);
		if(strTemp == "Unsigned")
			canTransData[nIndex].data_type = 0;
		else if(strTemp == "Signed")
			canTransData[nIndex].data_type = 1;
		else if(strTemp == "Float")
			canTransData[nIndex].data_type = 2;		
		else if(strTemp == "String")
			canTransData[nIndex].data_type = 3;
		
		canTransData[nIndex].factor_multiply = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_FACTOR));
		canTransData[nIndex].factor_Offset = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_OFFSET));
		
		canTransData[nIndex].send_time = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_TIME));
		canTransData[nIndex].default_fValue = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DEFAULT));


		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1)) //yulee 20181119
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[nIndex].function_division = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division = atoi(strTemp);
		}
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[nIndex].function_division2 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division2 = atoi(strTemp);
		}
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[nIndex].function_division3 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division3 = atoi(strTemp);
		}



/*

		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[nIndex].function_division = atoi(strCode);
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[nIndex].function_division2 = atoi(strCode);
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[nIndex].function_division3 = atoi(strCode);
		*/
		nSlaveCount++;
		
	}
	
	pMD->SetCANSetTransData(nSelCh, canTransData, nSlaveCount+nMasterCount);
	memcpy(&canTransSetData.canSetData, &canTransData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);	//ljb 201008

	if(int nRth = m_pDoc->SetCANTransDataToModule(nSelModule, &canTransSetData, nSelCh) == FALSE)
	{
		//AfxMessageBox("CAN Trnas 정보 설정에 실패했습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg10","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}

	IsChange = FALSE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);

	pMD->SaveCanTransConfig();	

	gst_CanTxSave.CanType = nLINtoCANSelect;
	gst_CanTxSave.CanRxBaudrate = m_ctrlBaudRate.GetCurSel();
	gst_CanTxSave.CanRxExtendedIDMode = ((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->GetCheck();
	gst_CanTxSave.SJW = m_ctrlBmsSJW.GetCurSel();
	gst_CanTxSave.BMSType = m_ctrlBmsType.GetCurSel();
	gst_CanTxSave.bEnable = TRUE;

	gst_CanTxSaveSlave.CanType = nLINtoCANSelect;
	gst_CanTxSaveSlave.CanRxBaudrate = m_ctrlBaudRateSlave.GetCurSel();
	gst_CanTxSaveSlave.CanRxExtendedIDMode = ((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->GetCheck();
	gst_CanTxSaveSlave.SJW = m_ctrlBmsSJWSlave.GetCurSel();
	gst_CanTxSaveSlave.BMSType = m_ctrlBmsTypeSlave.GetCurSel();
	gst_CanTxSaveSlave.bEnable = TRUE;

//	CDialog::OnOK();
}

//SBC 에서 Master CAN 설정 데이터 가져 오기
void CSettingCanFdDlg_Transmit::UpdateCommonData()
{

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	
	if(pMD == NULL)	return;
	
		CString strData;
		SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nSelCh, PS_CAN_TYPE_MASTER);
		m_strCanID.Format("%lX", commonData.controller_canID);		//khs 20081118
		m_ckExtID = commonData.extended_id;

		//ljb 20180323
		m_ctrlBaudRate.ResetContent();
		m_ctrlBaudRate.AddString("125 kbps");
		m_ctrlBaudRate.SetItemData(0,0);
		m_ctrlBaudRate.AddString("250 kbps");
		m_ctrlBaudRate.SetItemData(1,1);
		m_ctrlBaudRate.AddString("500 kbps");
		m_ctrlBaudRate.SetItemData(2,2);
		m_ctrlBaudRate.AddString("1 Mbps");
		m_ctrlBaudRate.SetItemData(3,3);
		m_ctrlBaudRate.SetCurSel(commonData.can_baudrate);

		m_ctrlDataRate.ResetContent();
		m_ctrlDataRate.AddString("500 kbps");
		m_ctrlDataRate.SetItemData(0,0);
		m_ctrlDataRate.AddString("833 kbps");
		m_ctrlDataRate.SetItemData(1,1);
		m_ctrlDataRate.AddString("1 Mbps");
		m_ctrlDataRate.SetItemData(2,2);
		m_ctrlDataRate.AddString("1.5 Mbps");
		m_ctrlDataRate.SetItemData(3,3);
		m_ctrlDataRate.AddString("2 Mbps");
		m_ctrlDataRate.SetItemData(4,4);
		m_ctrlDataRate.AddString("3 Mbps");
		m_ctrlDataRate.SetItemData(5,5);
		m_ctrlDataRate.AddString("4 Mbps");
		m_ctrlDataRate.SetItemData(6,6);
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		if(bUseCANFD)
			m_ctrlDataRate.SetCurSel(commonData.can_datarate);
		else
			m_ctrlDataRate.SetCurSel(0);
// #ifdef _DEBUG
// 		commonData.bms_type = -324324324234;
// 		commonData.bms_sjw = -2134234324324;
// #endif
// 		m_ctrlBmsType.GetLBText(commonData.bms_type, m_strBmsType);
// 		m_ctrlBmsSJW.GetLBText(commonData.bms_sjw, m_strBmsSJW);

		//ksj 20210210 :
		m_strBmsType.Format("%d",commonData.bms_type);
		m_strBmsSJW.Format("%d",commonData.bms_sjw);
		
// 		m_ctrlBmsType.SetWindowText(m_strBmsType);
// 		m_ctrlBmsSJW.SetWindowText(m_strBmsSJW);		
		//ksj end

		m_ctrlBMSTXTerminal.ResetContent();
		m_ctrlBMSTXTerminal.AddString("Open");
		m_ctrlBMSTXTerminal.SetItemData(0,0);
		m_ctrlBMSTXTerminal.AddString("120 Ohm");
		m_ctrlBMSTXTerminal.SetItemData(1,1);
		if(bUseCANFD)
			m_ctrlBMSTXTerminal.SetCurSel(commonData.terminal_r);
			//m_ctrlBMSTXTerminal.SetCurSel(1); //lyj 20200722 default 1
		else
			m_ctrlBMSTXTerminal.SetCurSel(1);
		
		m_ctrlTXBMSCRCType.ResetContent();
		m_ctrlTXBMSCRCType.AddString("Non");
		m_ctrlTXBMSCRCType.SetItemData(0,0);
		m_ctrlTXBMSCRCType.AddString("ISO");
		m_ctrlTXBMSCRCType.SetItemData(1,1);
		if(bUseCANFD)
			m_ctrlTXBMSCRCType.SetCurSel(commonData.crc_type);
			//m_ctrlTXBMSCRCType.SetCurSel(1); //lyj 20200722 default 1
		else
			m_ctrlTXBMSCRCType.SetCurSel(1);

		m_ckCanFD = commonData.can_fd_flag;
		if (commonData.can_fd_flag)
		{
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->EnableWindow(TRUE); //20180329 yulee
			GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_M)->EnableWindow(TRUE);
			BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			//if(!bUseCANFD)
			if(bUseCANFD) //ksj 20200213 : 조건 이상함. 최초 커밋 시점 코드로 롤백
			{
				
			}
			else
			{
				GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(TRUE);
			}
		}
		else
		{
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->EnableWindow(FALSE); //20180329 yulee
			GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M)->EnableWindow(TRUE); //20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_M)->EnableWindow(FALSE);
			BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
			//if(!bUseCANFD)
			if(bUseCANFD) //ksj 20200213 : 조건 이상함. 최초 커밋 시점 코드로 롤백
			{
				
			}
			else
			{
				GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
				GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
			}
		}

		//ljb 201012
		m_edit_chargeOnVolt = commonData.fcan_Value[0];
		m_edit_OnDelayTime = commonData.fcan_Value[1];
		m_edit_KeyonVoltMin = commonData.fcan_Value[2];
		m_edit_KeyonVoltMax = commonData.fcan_Value[3];
		m_edit_BmsRestartTime = commonData.fcan_Value[4];


		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		//SLAVE Setting
		commonData = pMD->GetCANCommonTransData(nSelCh, PS_CAN_TYPE_SLAVE);
		m_strCanIDSlave.Format("%lX", commonData.controller_canID);		//khs 20081118
		m_ckExtIDSlave = commonData.extended_id;
		//ljb 20180323

		m_ctrlBaudRateSlave.ResetContent();
		m_ctrlBaudRateSlave.AddString("125 kbps");
		m_ctrlBaudRateSlave.SetItemData(0,0);
		m_ctrlBaudRateSlave.AddString("250 kbps");
		m_ctrlBaudRateSlave.SetItemData(1,1);
		m_ctrlBaudRateSlave.AddString("500 kbps");
		m_ctrlBaudRateSlave.SetItemData(2,2);
		m_ctrlBaudRateSlave.AddString("1 Mbps");
		m_ctrlBaudRateSlave.SetItemData(3,3);
		m_ctrlBaudRateSlave.SetCurSel(commonData.can_baudrate);
		
		m_ctrlDataRateSlave.ResetContent();
		m_ctrlDataRateSlave.AddString("500 kbps");
		m_ctrlDataRateSlave.SetItemData(0,0);
		m_ctrlDataRateSlave.AddString("833 kbps");
		m_ctrlDataRateSlave.SetItemData(1,1);
		m_ctrlDataRateSlave.AddString("1 Mbps");
		m_ctrlDataRateSlave.SetItemData(2,2);
		m_ctrlDataRateSlave.AddString("1.5 Mbps");
		m_ctrlDataRateSlave.SetItemData(3,3);
		m_ctrlDataRateSlave.AddString("2 Mbps");
		m_ctrlDataRateSlave.SetItemData(4,4);
		m_ctrlDataRateSlave.AddString("3 Mbps");
		m_ctrlDataRateSlave.SetItemData(5,5);
		m_ctrlDataRateSlave.AddString("4 Mbps");
		m_ctrlDataRateSlave.SetItemData(6,6);
		if(bUseCANFD)
 			m_ctrlDataRateSlave.SetCurSel(commonData.can_datarate);
		else
			m_ctrlDataRateSlave.SetCurSel(0);

		m_ctrlBMSTXTerminalSlave.ResetContent();
		m_ctrlBMSTXTerminalSlave.AddString("Open");
		m_ctrlBMSTXTerminalSlave.SetItemData(0,0);
		m_ctrlBMSTXTerminalSlave.AddString("120 Ohm");
		m_ctrlBMSTXTerminalSlave.SetItemData(1,1);
		if(bUseCANFD)
			m_ctrlBMSTXTerminalSlave.SetCurSel(commonData.terminal_r);
			//m_ctrlBMSTXTerminalSlave.SetCurSel(1); //lyj 20200722 default 1
		else
			m_ctrlBMSTXTerminalSlave.SetCurSel(1);
		
		m_ctrlTXBMSCRCTypeSlave.ResetContent();
		m_ctrlTXBMSCRCTypeSlave.AddString("Non");
		m_ctrlTXBMSCRCTypeSlave.SetItemData(0,0);
		m_ctrlTXBMSCRCTypeSlave.AddString("ISO");
		m_ctrlTXBMSCRCTypeSlave.SetItemData(1,1);
		if(bUseCANFD)
			m_ctrlTXBMSCRCTypeSlave.SetCurSel(commonData.crc_type);
			//m_ctrlTXBMSCRCTypeSlave.SetCurSel(1); //lyj 20200722 default 1
		else
			m_ctrlTXBMSCRCTypeSlave.SetCurSel(1);

		//m_ctrlDataRateSlave.GetWindowText(m_strBaudRateSlave);
		//m_ctrlBaudRate.GetLBText(commonData.can_baudrate, m_strBaudRate);

// 		m_ctrlBmsTypeSlave.GetLBText(commonData.bms_type, m_strBmsTypeSlave);
// 		m_ctrlBmsSJWSlave.GetLBText(commonData.bms_sjw, m_strBmsSJWSlave);
		//ksj 20210210 :
		m_strBmsTypeSlave.Format("%d",commonData.bms_type);
		m_strBmsSJWSlave.Format("%d",commonData.bms_sjw);

// 		m_ctrlBmsTypeSlave.SetWindowText(m_strBmsTypeSlave);
// 		m_ctrlBmsSJWSlave.SetWindowText(m_strBmsSJWSlave);		
		//ksj end

		m_ckCanFD_Slave = commonData.can_fd_flag;
		if (commonData.can_fd_flag)
		{
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(TRUE); //20180329 yulee
			GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_S)->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(FALSE); //20180329 yulee
			GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2)->EnableWindow(TRUE);//20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_S)->EnableWindow(FALSE);
		}


		//ksj 20210115 : CAN FD 체크 풀려 있으면 기본 값 강제 초기화 추가.
		//혹시 문제 생길 수 있으므로, 기능 비활성화 가능하도록 일단 옵션처리.
		BOOL bUseInitCanFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "bUseInitCanFD", TRUE);
		if(bUseInitCanFD)
		{
			if(m_ckCanFD == FALSE)
			{
				m_ctrlDataRate.SetCurSel(0);
				m_ctrlBMSTXTerminal.SetCurSel(1);
				m_ctrlTXBMSCRCType.SetCurSel(1);
			}

			if(m_ckCanFD_Slave == FALSE)
			{
				m_ctrlDataRateSlave.SetCurSel(0);
				m_ctrlBMSTXTerminalSlave.SetCurSel(1);
				m_ctrlTXBMSCRCTypeSlave.SetCurSel(1);
			}	
		}		
		//ksj end
		
		UpdateData(FALSE);

		//ksj 20210502 : UpdateData 이후에 호출되어야한다.
		m_ctrlBmsType.SetWindowText(m_strBmsType);
		m_ctrlBmsSJW.SetWindowText(m_strBmsSJW);	
		m_ctrlBmsTypeSlave.SetWindowText(m_strBmsTypeSlave);
		m_ctrlBmsSJWSlave.SetWindowText(m_strBmsSJWSlave);
		//ksj end

}

//SBC 에서 CAN 데이터 가져 오기
void CSettingCanFdDlg_Transmit::UpdateGridData()
{
	int nRow= 0;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if(pMD == NULL)	return;
	
	for(int nMaster = 0; nMaster < pMD->GetInstalledCanTransMaster(nSelCh); nMaster++)
	{
		CString strTemp;
		nRow = m_wndCanGrid.GetRowCount();
		m_wndCanGrid.InsertRows(++nRow, 1); //ksj 20210305
		SFT_CAN_SET_TRANS_DATA canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nMaster);
		//		strTemp.Format("%ld", canData.canID);

		CString strName, strID; //yulee 20190911
		strName.Format("%s", canData.name);
		if(strName.CompareNoCase(_T("$")) == 0)
		{
			break;
		}

		if(m_nMasterIDType == 0)	//Hex Type 이면
			strTemp.Format("%X", canData.canID);	
		else
			strTemp.Format("%ld", canData.canID);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_ID), strTemp);		
		strTemp.Format("%s", canData.name);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_NAME), strTemp);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_TIME),  canData.send_time);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT), canData.startBit);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT), canData.bitCount);
		//20180403 yulee Add DLC 입력
 		int tmpDLCArr[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 12, 16, 20, 24, 32, 48, 64};
 		for(int i=0; i<16; i++)
 		{
			int tmpInt = canData.dlc;
 			if(tmpDLCArr[i] == tmpInt)
 			{
		 		CString tmpStr;
				tmpStr.Format(_T("%d"), tmpDLCArr[i]);
				m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DLC), (_T("%s"), tmpStr)); //20180402 yulee Add
 			}
		}
		if(canData.byte_order == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Intel");
		else
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Motorola");
		
		if(canData.data_type == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Unsigned");
		else if(canData.data_type == 1)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Signed");
		else if(canData.data_type == 2)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Float");
		else
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "String");	

		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_FACTOR), canData.factor_multiply);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_OFFSET), canData.factor_Offset);
		long lTemp = (long)canData.default_fValue;
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DEFAULT), lTemp);

		strTemp = Fun_FindCanName(canData.function_division); //yulee 20181220
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division2);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division3);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), strTemp);
/*
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), canData.function_division);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), canData.function_division2);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), canData.function_division3);
*/
// 		lTemp = (long)canData.CapVtgFlag;
// 		if (lTemp == 1)
// 			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_CapVtgFlag), (long)1);
// 		if (lTemp == 2)
// 			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_McuTempFlag), (long)1);
		
// 		if(nMaster+1 < pMD->GetInstalledCanTransMaster(nSelCh)) //ksj 20210305 : 주석처리
// 			m_wndCanGrid.InsertRows(nRow+1, 1); //ksj 20210305 : 주석처리
	}
	
	//2014.08.22 캔 슬레이브를 사용하지않을시 그리드 적용하지않음.
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);
	if(isSlave == FALSE){
		return;
	}

	for(int nSlave = 0; nSlave < pMD->GetInstalledCanTransSlave(nSelCh); nSlave++)
	{
		CString strTemp;
		nRow = m_wndCanGridSlave.GetRowCount();
		m_wndCanGridSlave.InsertRows(++nRow, 1); //ksj 20210305
		SFT_CAN_SET_TRANS_DATA canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSlave);
		//		strTemp.Format("%ld", canData.canID);

		CString strName, strID; //yulee 20190911
		strName.Format("%s", canData.name);
		if(strName.CompareNoCase(_T("$")) == 0)
		{
			break;
		}

		if(m_nMasterIDType == 0)	//Hex Type 이면
			strTemp.Format("%X", canData.canID);	
		else
			strTemp.Format("%ld", canData.canID);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_ID), strTemp);		
		strTemp.Format("%s", canData.name);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_NAME), strTemp);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_TIME),  canData.send_time);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT), canData.startBit);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT), canData.bitCount);
		//20180403 yulee Add DLC 입력
		int tmpDLCArr[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 12, 16, 20, 24, 32, 48, 64};
		for(int i=0; i<16; i++)
		{
			int tmpInt = canData.dlc;
			if(tmpDLCArr[i] == tmpInt)
			{
				CString tmpStr;
				tmpStr.Format(_T("%d"), tmpDLCArr[i]);
				m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DLC), (_T("%s"), tmpStr)); //20180402 yulee Add
			}
		}
		if(canData.byte_order == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Intel");
		else
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Motorola");
		
		if(canData.data_type == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Unsigned");
		else if(canData.data_type == 1)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Signed");
		else if(canData.data_type == 2)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Float");
		else
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "String");	
		
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_FACTOR), canData.factor_multiply);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_OFFSET), canData.factor_Offset);
		long lTemp = (long)canData.default_fValue;
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DEFAULT), lTemp);

		strTemp = Fun_FindCanName(canData.function_division); //yulee 20181220
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division2);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division3);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), strTemp);


//		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), canData.function_division);
//		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), canData.function_division2);
//		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), canData.function_division3);
// 		lTemp = (long)canData.CapVtgFlag;
// 		if (lTemp == 1)
// 			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_CapVtgFlag), (long)1);
// 		if (lTemp == 2)
// 			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_McuTempFlag), (long)1);
		
// 		if(nSlave+1 < pMD->GetInstalledCanTransSlave(nSelCh)) //ksj 20210305 : 주석처리
// 			m_wndCanGridSlave.InsertRows(nRow+1, 1); //ksj 20210305 : 주석처리
	}
}

CString CSettingCanFdDlg_Transmit::Fun_FindCanName(int nDivision) //yulee 20181220
{
	CString strName;
	int nSelectPos, nFindPos;
	for (int i=0; i < uiArryCanCode.GetSize() ; i++)
	{
		if (nDivision == uiArryCanCode.GetAt(i))
		{
			strName.Format("%s [%d]", strArryCanName.GetAt(i), nDivision);
			return strName;
		}
	}
	strName.Format("None [%d]", nDivision);
	return strName;
}

void CSettingCanFdDlg_Transmit::OnBtnLoadCanSetup() 
{
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");
	
	CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	if(IDOK == pDlg.DoModal())
	{
		if(m_wndCanGrid.GetRowCount() > 0)
			m_wndCanGrid.RemoveRows(1, m_wndCanGrid.GetRowCount());
		if(m_wndCanGridSlave.GetRowCount() > 0)
			m_wndCanGridSlave.RemoveRows(1, m_wndCanGridSlave.GetRowCount());
		if(LoadCANConfig(pDlg.GetPathName()) == TRUE)
		{
			CString strName;
			strName.Format("CAN Transmit - %s",  pDlg.GetFileName());
			this->SetWindowText(strName);
			
			IsChangeFlag(TRUE);
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}	
}

void CSettingCanFdDlg_Transmit::OnBtnAddMaster() 
{
	CRowColArray ra;
	m_wndCanGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndCanGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, 1);
	
	
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg_Transmit::OnBtnDelMaster() 
{
	CRowColArray ra;
	m_wndCanGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndCanGrid.RemoveRows(ra[i], ra[i]);
	}
	
	IsChangeFlag(TRUE);	
}

void CSettingCanFdDlg_Transmit::OnBtnSaveCanSetup() 
{
	UpdateData();
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\DataBase\\Config", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");		//ljb 2009-09-04
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		if(SaveCANConfig(pDlg.GetPathName()) == TRUE)
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
	}	
}
BOOL CSettingCanFdDlg_Transmit::SaveCANConfig(CString strSavePath)
{
	CFile file;
	CString strData,strSaveData;
	CString strTitle, strTitle1;
	int i = 0;
	strSaveData.Empty();

	//Start Save Master/////////////////////////////////////////////////////////////////////////
	
	strTitle1 = "**Data Info**\r\nTYPE,VERSION,DESCRIPT\r\n";

	strTitle = "**Master Transmit Configuration**\r\nID(Hex),BaudRate,Ext ID,Bms Type,Bms SJW,CAN FD,CAN FD DataRate,TML Res.,CRC\r\n";

	TRY
	{
		if(strSavePath.Find('.') < 0)
			strSavePath += ".csv";

		if(file.Open(strSavePath, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			//AfxMessageBox("파일을 생성할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_SaveCANConfig_msg1","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
			return FALSE;
		}

		file.Write(strTitle1, strTitle1.GetLength());	//Title1
		
//		strData.Format("%s,", m_strType);
		strData.Format("TRANS,");
		file.Write(strData, strData.GetLength()); //Type
		
//		strData.Format("%d,", m_nProtocolVer);
		strData.Format("4,");		//ljb 201008 2->3 분류2,분류3 추가	//ljb 20180326 3-> 4 CAN FD
		file.Write(strData, strData.GetLength()); //ProtocolVer
		m_strDescript = m_strTitle; //lyj 20200730 설정 저장 시 title 값으로 descript 저장

		strData.Format("%s,\r\n\r\n", m_strDescript);
//		strData.Format("PNE CAN CONFIG FILE,\r\n\r\n");
		file.Write(strData, strData.GetLength()); //Descript

		file.Write(strTitle, strTitle.GetLength());	//Title 

//		strTitle = "**Master Transmit Configuration**\r\nID(Hex),BaudRate,Ext ID,Bms Type,Bms SJW,CAN FD,CAN FD DataRate\r\n";
		strSaveData.Format("%s,", m_strCanID);

		m_ctrlBaudRate.GetWindowText(strData);	//ljb 20180326
// 		if(m_strBaudRate == "125 kbps")
// 			strData.Format("%d,", 125);
// 		else if(m_strBaudRate == "250 kbps")
// 			strData.Format("%d,", 250);
// 		else if(m_strBaudRate == "500 kbps")
// 			strData.Format("%d,", 500);
// 		else if(m_strBaudRate == "1 Mbps")
// 			strData.Format("%d,", 1000);
		//strData.Format("%s,", m_strBaudRate);	

		strSaveData += strData + ",";

		strData.Format("%d,",m_ckExtID);	//Ext ID
		strSaveData += strData;

		strData.Format("%s,",m_strBmsType);	//BMS Type
		strSaveData += strData;

		strData.Format("%s,",m_strBmsSJW);	//BMS SJW
		strSaveData += strData;

		strData.Format("%d,",m_ckCanFD);	//CAN FD
		strSaveData += strData;

		m_ctrlDataRate.GetWindowText(strData);	//ljb 20180326 CAN FD DataRate
		strSaveData += strData + ",";;

		m_ctrlBMSTXTerminal.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",";
		
		m_ctrlTXBMSCRCType.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",\r\n\r\n";
		
// 		strData.Format("%.3f,",m_edit_chargeOnVolt);	//Charge On Volt
// 		strSaveData += strData;
// 		strData.Format("%d,",m_edit_OnDelayTime);	//On Delay Time
// 		strSaveData += strData;
// 		strData.Format("%.3f,",m_edit_KeyonVoltMin);	//Key On Volt Min
// 		strSaveData += strData;
// 		strData.Format("%.3f,",m_edit_KeyonVoltMax);	//Key On Volt Max
// 		strSaveData += strData;
// 		strData.Format("%.3f,\r\n\r\n",m_edit_BmsRestartTime);	//BMS restart time
		strSaveData += strData;

		file.Write(strSaveData, strSaveData.GetLength()); //Bms Type

		if(m_nMasterIDType == 0)		//Hex
			strTitle = "ID(Hex),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
			//strTitle = Fun_FindMsg("SettingCanFdDlg_Transmit_SaveCANConfig_msg2","IDD_CAN_FD_SETTING_TRANSMIT");//&&
		else							//Dec
			strTitle = "ID(Dec),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
			//strTitle = Fun_FindMsg("SettingCanFdDlg_Transmit_SaveCANConfig_msg3","IDD_CAN_FD_SETTING_TRANSMIT");//&&
		file.Write(strTitle, strTitle.GetLength());	//Title 
		
		for(i=1; i<=m_wndCanGrid.GetRowCount(); i++)
		{
			for(int j = 1; j <=m_wndCanGrid.GetColCount(); j++)
			{
				if(j==CAN_COL_DLC) continue;

				strData.Format("%s,",m_wndCanGrid.GetValueRowCol(i, j));
				file.Write(strData, strData.GetLength()); 
			}
			strData.Format("\r\n");
			file.Write(strData, strData.GetLength()); //줄바꿈
		}
		strData.Format("\r\n");
		file.Write(strData, strData.GetLength()); //줄바꿈
	}
	//End Save Master/////////////////////////////////////////////////////////////////////////

	//Start Save Slave/////////////////////////////////////////////////////////////////////////
	strTitle = "**Slave Transmit Configuration**\r\nID(Hex),BaudRate,Ext ID,Bms Type,Bms SJW,CAN FD,CAN FD DataRate,TML Res.,CRC\r\n";
	
	
	file.Write(strTitle, strTitle.GetLength());	//Title 
	
	strSaveData.Format("%s,", m_strCanIDSlave);
	m_ctrlBaudRateSlave.GetWindowText(strData);	//ljb 20180326
// 	if(m_strBaudRateSlave == "125 kbps")
// 		strData.Format("%d,", 125);
// 	else if(m_strBaudRateSlave == "250 kbps")
// 		strData.Format("%d,", 250);
// 	else if(m_strBaudRateSlave == "500 kbps")
// 		strData.Format("%d,", 500);
// 	else if(m_strBaudRateSlave == "1 Mbps")
// 		strData.Format("%d,", 1000);
	//strData.Format("%s,", m_strBaudRate);	
	strSaveData += strData +",";
//	file.Write(strData, strData.GetLength()); //BaudRate
	
	strData.Format("%d,",m_ckExtIDSlave);	
	strSaveData += strData;
//	file.Write(strData, strData.GetLength()); //Ext ID

	strData.Format("%s,",m_strBmsTypeSlave);	//BMS Type
	strSaveData += strData;

	strData.Format("%s,",m_strBmsSJWSlave);	//BMS SJW
	strSaveData += strData;

	strData.Format("%d,",m_ckCanFD_Slave);	//CAN FD
	strSaveData += strData;
	
	m_ctrlDataRateSlave.GetWindowText(strData);	//ljb 20180326 CAN FD DataRate
	strSaveData += strData+ ",";

	m_ctrlBMSTXTerminalSlave.GetWindowText(strData);	//ljb 20180326
	strSaveData += strData + ",";
	
	m_ctrlTXBMSCRCTypeSlave.GetWindowText(strData);	//ljb 20180326
		strSaveData += strData + ",\r\n\r\n";

	strSaveData += strData;
	
	file.Write(strSaveData, strSaveData.GetLength()); //Bms Type
	
	if(m_nMasterIDType == 0)		//Hex
		strTitle = "ID(Hex),전송속도(ms),Name,StartBit,BitCount,DLC,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		//strTitle = Fun_FindMsg("SettingCanFdDlg_Transmit_SaveCANConfig_msg4","IDD_CAN_FD_SETTING_TRANSMIT");//&&
	else
		strTitle = "ID(Dec),전송속도(ms),Name,StartBit,BitCount,DLC,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		//strTitle = Fun_FindMsg("SettingCanFdDlg_Transmit_SaveCANConfig_msg5","IDD_CAN_FD_SETTING_TRANSMIT");//&&
	file.Write(strTitle, strTitle.GetLength());	//Title 
	
	for(i=1; i<=m_wndCanGridSlave.GetRowCount(); i++)
	{
		for(int j = 1; j <= m_wndCanGridSlave.GetColCount(); j++)
		{
			strData.Format("%s,",m_wndCanGridSlave.GetValueRowCol(i, j));
			file.Write(strData, strData.GetLength()); 
		}
		strData.Format("\r\n");
		file.Write(strData, strData.GetLength()); //줄바꿈
	}
	//End Save Slave/////////////////////////////////////////////////////////////////////////		
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
	
	file.Close();
	return TRUE;
}

BOOL CSettingCanFdDlg_Transmit::LoadCANConfig(CString strLoadPath)
{
	BOOL bDLC = FALSE;
	CString strData;
	CStdioFile aFile;
	CFileException e;
	int nIndex, nPos = 0, nTemp;
	CString strTitle = "ID(Hex),BaudRate,Ext ID,BMS Capacitor voltage,BMS Motor Inverter Temperature";
	CString strTitle2 = "ID(Hex),BaudRate,Ext ID";
	CString strTitle3 = "ID(Hex),BaudRate,Ext ID,Bms Type";
	CString strTitle4 = "ID(Hex),BaudRate,Ext ID,Bms Type,Bms SJW,CAN FD,CAN FD DataRate";

	if( !aFile.Open( strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   //AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		   AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_LoadCANConfig_msg1","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		   return FALSE;
	}

	CStringList		strDataList;
	CStringList		strItemList;
	aFile.ReadString(strData);	//Version Check
	
	if(strData.Find("Data") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data

		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);		//TRANS인지 RECEIVE인지 Check
			m_strType = strTemp;
			
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);				//1: 1006 Ver 추후 버전 업그레이드에 따라 추가
			
			if (strTemp.IsEmpty()) m_nProtocolVer = 0;
			else
				m_nProtocolVer = atoi(strTemp);
// 			if(strTemp == "1" || strTemp == "2")
// 				m_nProtocolVer = atoi(strTemp);
// 			else 
// 				m_nProtocolVer = 0;
			
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strDescript = strTemp;
		}
		
		// Data Info에서 Type에 따라 Load할 Data를 결정	
		if(m_strType != "TRANS")
		{
			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_LoadCANConfig_msg2","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
			m_nProtocolVer = 0;
			return FALSE;
		}

		nPos = 0;
		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Master or Slave
	}	
	
	bDLC = FALSE;
	if(strData.Find("Master") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.

		//구버전을 읽어올 경우 TRANS인지 RECEIVE인지 Check
// 		if (m_nProtocolVer == 2) strTitle = strTitle2;
// 		if (m_nProtocolVer == 3) strTitle = strTitle3;
// 		if (m_nProtocolVer == 4) strTitle = strTitle4;
// 		if(strData != strTitle)
// 		{
// 			AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
// 			m_nProtocolVer = 0;
// 			return FALSE;
// 		}

		aFile.ReadString(strData);	//Data
		
		
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strCanID = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "125" || strTemp == "125K" || strTemp == "125KBPS"){
				m_ctrlBaudRate.SetCurSel(0);}
			else if(strTemp == "250" || strTemp == "250K" || strTemp == "250KBPS"){
				m_ctrlBaudRate.SetCurSel(1);}
			else if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
				m_ctrlBaudRate.SetCurSel(2);}
			else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
				m_ctrlBaudRate.SetCurSel(3);}
			else {
				m_ctrlBaudRate.SetCurSel(0);
			}			
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckExtID = atoi(strTemp);					//Exit ID
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			m_ctrlDataRate.SetCurSel(0);

			if (m_nProtocolVer == 1)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
//				m_fCapVtg = atof(strTemp);
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);

				strTemp = strData.Mid(nPos, nIndex - nPos);
//				m_fMcuTemp = atof(strTemp);
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);
			}

			if (m_nProtocolVer == 3)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
				m_strBmsType = strTemp;		//BMS TYPE
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);
				
				if (nIndex > 0)	//BMS SJW
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_strBmsSJW = strTemp;
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Charge On Volt
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_chargeOnVolt = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//On Delay Time
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_OnDelayTime = atoi(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Key On Volt Min
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_KeyonVoltMin = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Key On Volt Max
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_KeyonVoltMax = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Key On Volt Max
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_BmsRestartTime = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
			}
			if (m_nProtocolVer == 4)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
				m_strBmsType = strTemp;		//BMS TYPE
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);
				
				if (nIndex > 0)	//BMS SJW
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_strBmsSJW = strTemp;
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//CAN FD
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_ckCanFD = atoi(strTemp);
					if (m_ckCanFD) 
					{
						GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->EnableWindow(TRUE);
						//ksj 20200310 : 조건 추가, 불러온 CAN설정 파일에 옵션이 CAN FD를 사용하는 경우 관련 컨트롤도 활성/비활성화.
						GetDlgItem(IDC_COMBO_CAN_BMS_SJW  	)->EnableWindow(FALSE);
						GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->EnableWindow(TRUE);
						//ksj end

						BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
						//if(!bUseCANFD)
						if(bUseCANFD) //ksj 20200213 : 조건 이상함. 최초 커밋 시점 코드로 롤백
						{
						
						}
						else
						{
							GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);

						}		

					}
					else
					{
						GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->EnableWindow(FALSE);
						//ksj 20200310 : 조건 추가, 불러온 CAN설정 파일에 옵션이 CAN FD를 사용하는 경우 관련 컨트롤도 활성/비활성화.
						GetDlgItem(IDC_COMBO_CAN_BMS_SJW  	)->EnableWindow(TRUE);
						GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->EnableWindow(FALSE);
						//ksj end
						BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
						//if(!bUseCANFD)
						if(bUseCANFD) //ksj 20200213 : 조건 이상함. 최초 커밋 시점 코드로 롤백
						{
						
						}
						else
						{
							GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
						}
					}
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
		
				if (nIndex > 0)	//CAN FD DataRate
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					strTemp.Replace(" ", "");
					strTemp.MakeUpper();

					BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
					if(bUseCANFD)
					{
						if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
							m_ctrlDataRate.SetCurSel(0);}
						else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
							m_ctrlDataRate.SetCurSel(1);}
						else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
							m_ctrlDataRate.SetCurSel(2);}
						else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
							m_ctrlDataRate.SetCurSel(3);}
						else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
							m_ctrlDataRate.SetCurSel(4);}
						else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
							m_ctrlDataRate.SetCurSel(5);}
						else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
							m_ctrlDataRate.SetCurSel(6);}
						else {
							m_ctrlDataRate.SetCurSel(0);
						}
					}
					else
					{

					}
// 					if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
// 						m_ctrlDataRate.SetCurSel(0);}
// 					else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
// 						m_ctrlDataRate.SetCurSel(1);}
// 					else if(strTemp == "1" || strTemp == "1M" || strTemp == "1MBPS"|| strTemp == "1000"){
// 						m_ctrlDataRate.SetCurSel(2);}
// 					else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
// 						m_ctrlDataRate.SetCurSel(3);}
// 					else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
// 						m_ctrlDataRate.SetCurSel(4);}
// 					else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
// 						m_ctrlDataRate.SetCurSel(5);}
// 					else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
// 						m_ctrlDataRate.SetCurSel(6);}
// 					else {
// 						m_ctrlDataRate.SetCurSel(0);
// 					}
										nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);



					//ljb 20180327 BMS Terminal
					strTemp="";
					strTemp = strData.Mid(nPos, nIndex - nPos);
					strTemp.Replace(" ", "");
					strTemp.MakeUpper();
					if(strTemp == "OPEN"){
						m_ctrlBMSTXTerminal.SetCurSel(0);}
					else if(strTemp == "120OHM"){
						m_ctrlBMSTXTerminal.SetCurSel(1);}
					else
						m_ctrlBMSTXTerminal.SetCurSel(1);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);

					//ljb 20180327 BMS CRC
					strTemp="";
					strTemp = strData.Mid(nPos, nIndex - nPos);
					strTemp.Replace(" ", "");
					strTemp.MakeUpper();
					if(strTemp == "NON"){
						m_ctrlTXBMSCRCType.SetCurSel(0);}
					else if(strTemp == "CRC"){
						m_ctrlTXBMSCRCType.SetCurSel(1);}
					else
						m_ctrlTXBMSCRCType.SetCurSel(1);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);	
				}
				else
					m_ctrlDataRate.SetCurSel(0);
			}
		}
		if (m_nProtocolVer < 3)
		{
			m_strBmsType = "0";
			m_strBmsSJW = "0";
		}

		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		
		nPos = strData.Find("Name", 0);
		if(nPos > 4)	//ID가 Hex또는 Decimal인지 구분.. 이 방법밖에 없나??
		{
			CString strType;
			nPos = strData.Find("(", 0);
			strType = strData.Mid(nPos+1, strData.Find(")", nPos+1)-(nPos+1));
			strType.MakeUpper();
			if(strType == "HEX")
			{
				m_nMasterIDType = 0;
			}
			else
			{
				m_nMasterIDType = 1;
			}
			if(strData.Find("DLC")>-1)
			{
				bDLC=TRUE;
			}
		}
		else		
		{//(HEX), (DEC) 가 없으면 무조건 DEC
			m_nMasterIDType = 1;
		}
		UpdateData(FALSE);
		
		nPos = 0;
		while(aFile.ReadString(strData))	//Data
		{
			if(strData == "" || strData.Left(10) == ",,,,,,,,,,")
				break;
			//CString strTemp = strData.Mid(nPos, nIndex - nPos);
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			
			int nCol = 1;

			int nCnt = 0;
			m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, nCol);
			int nRow = m_wndCanGrid.GetRowCount();
			while(p0 != -1)
			{		
				CString strCanData;
				nCnt++; //yulee 20181031
				if( nCnt == 6)
				{
					if( bDLC==TRUE)
					{
						strCanData = strData.Mid(p1, p0 - p1);
					}
					else
					{
						 int nTmpDLCValue;
						 nTmpDLCValue = 8;
						 strCanData.Format("%d", nTmpDLCValue);
					}
				}
				else
				{
					strCanData = strData.Mid(p1, p0 - p1);
				}
				if (strCanData == "")
				{
					p0 = -1;
					break;
				}
				if(m_nProtocolVer == 0)
				{
					if(nCol != CAN_COL_OFFSET)
					{
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else if (m_nProtocolVer == 1)
				{
					if(nCol != CAN_COL_DIVISION1)
					{
						//m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
						nTemp = atoi(strCanData);
						if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
						else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
						else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
						else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
						else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
						else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
						else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
						TRACE("%s \n",strCanData);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
						break;
					}
				}
				else if (m_nProtocolVer == 2)
				{
					//m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					nTemp = atoi(strCanData);
					if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
					else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
					else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
					else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
					else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
					else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
					else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
					TRACE("%s \n",strCanData);
					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);

				}
				else if (m_nProtocolVer == 3)
				{
					//m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					nTemp = atoi(strCanData);
					if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
					else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
					else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
					else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
					else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
					else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
					else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
					TRACE("%s \n",strCanData);
					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);

				}
				if (m_nProtocolVer == 4)
				{
					//m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					nTemp = atoi(strCanData);
					if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
					else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
					else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
					else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
					else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
					else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
					else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
					TRACE("%s \n",strCanData);
					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
				}
				if( nCnt == 6)
				{
					if( bDLC==TRUE)
					{
						p1 = p0 +1;
						p0 = strData.Find(",", p1);
					}
					else
					{
						if(m_ckCanFD)
						{
							p1 = p0 +1; //yulee 20181213 CAN TX Import Error
							p0 = strData.Find(",", p1);
						}
					}
				}
				else
				{
					p1 = p0 +1;
					p0 = strData.Find(",", p1);
				}
				//p1 = p0 +1;
				//p0 = strData.Find(",", p1);
			}
		}	

	}

	aFile.ReadString(strData);	//Master or Slave
	
	bDLC=FALSE;
	if(strData.Find("Slave") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data
		
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strCanIDSlave = strTemp;		//CAN ID
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");		//CAN Baud
			strTemp.MakeUpper();
			if(strTemp == "125" || strTemp == "125K" || strTemp == "125KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(0);}
			else if(strTemp == "250" || strTemp == "250K" || strTemp == "250KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(1);}
			else if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(2);}
			else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
				m_ctrlBaudRateSlave.SetCurSel(3);}
			else {
				m_ctrlBaudRateSlave.SetCurSel(0);
			}						
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckExtIDSlave = atoi(strTemp);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			m_ctrlDataRateSlave.SetCurSel(0);

			if (m_nProtocolVer == 3)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
				m_strBmsTypeSlave = strTemp;
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);

				if (nIndex > 0)
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_strBmsSJWSlave = strTemp;
					//nPos = nIndex+1;
					//nIndex = strData.Find(",", nPos);
				}
			}
			if (m_nProtocolVer == 4)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
				m_strBmsTypeSlave = strTemp;		//BMS TYPE
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);
				
				if (nIndex > 0)	//BMS SJW
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_strBmsSJWSlave = strTemp;
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//CAN FD
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_ckCanFD_Slave = atoi(strTemp);
					//if (m_ckCanFD) 
					//{
					//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(TRUE);
					//	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
					//	if(!bUseCANFD)
					//	{
					//		
					//	}
					//	else
					//	{
					//		GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
					//	}
					//}
					//else
					//{
					//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(FALSE);
					//	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
					//	if(!bUseCANFD)
					//	{
					//		
					//	}
					//	else
					//	{
					//		GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
					//		GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
					//	}
					//}

					//ksj 20200213 : 위에 왜 주석처리 되어있고 if 조건이 바뀌어있는지 모르겠다.
				    //뭔가 비정상동작하도록 꼬아놓은 것 같아.
					//예전 코드로 롤백. (최초 커밋)
					if (m_ckCanFD) 
					{
						GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(TRUE);
						//ksj 20200310 : 조건 추가, 불러온 CAN설정 파일에 옵션이 CAN FD를 사용하는 경우 관련 컨트롤도 활성/비활성화.
						GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE 	)->EnableWindow(FALSE);						
						GetDlgItem(IDC_COMBO_TX_CAN_CRC_S      )->EnableWindow(TRUE);
						//ksj end
						BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
						if(bUseCANFD)
						{
							
						}
						else
						{
							GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
						}
					}
					else
					{
						GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(FALSE);
						//ksj 20200310 : 조건 추가, 불러온 CAN설정 파일에 옵션이 CAN FD를 사용하는 경우 관련 컨트롤도 활성/비활성화.
						GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE  	)->EnableWindow(TRUE);
						GetDlgItem(IDC_COMBO_TX_CAN_CRC_S      )->EnableWindow(FALSE);
						//ksj end

						BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
						if(bUseCANFD)
						{
						
						}
						else
						{
							GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
							GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
						}
					}
					//ksj end


					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//CAN FD DataRate
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					strTemp.Replace(" ", "");
					strTemp.MakeUpper();
					BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
					if(bUseCANFD)
					{
						if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
							m_ctrlDataRateSlave.SetCurSel(0);}
						else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
							m_ctrlDataRateSlave.SetCurSel(1);}
						else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
							m_ctrlDataRateSlave.SetCurSel(2);}
						else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
							m_ctrlDataRateSlave.SetCurSel(3);}
						else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
							m_ctrlDataRateSlave.SetCurSel(4);}
						else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
							m_ctrlDataRateSlave.SetCurSel(5);}
						else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
							m_ctrlDataRateSlave.SetCurSel(6);}
						else {
							m_ctrlDataRateSlave.SetCurSel(0);
						}
					}
					else
					{
					}
// 					if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
// 						m_ctrlDataRateSlave.SetCurSel(0);}
// 					else if(strTemp == "833" || strTemp == "833K" || strTemp == "833KBPS"){
// 						m_ctrlDataRateSlave.SetCurSel(1);}
// 					else if(strTemp == "1" || strTemp == "1M" || strTemp == "1MBPS"){
// 						m_ctrlDataRateSlave.SetCurSel(2);}
// 					else if(strTemp == "1.5" || strTemp == "1.5M" || strTemp == "1.5MBPS"){
// 						m_ctrlDataRateSlave.SetCurSel(3);}
// 					else if(strTemp == "2" || strTemp == "2M" || strTemp == "2MBPS"){
// 						m_ctrlDataRateSlave.SetCurSel(4);}
// 					else if(strTemp == "3" || strTemp == "3M" || strTemp == "3MBPS"){
// 						m_ctrlDataRateSlave.SetCurSel(5);}
// 					else if(strTemp == "4" || strTemp == "4M" || strTemp == "4MBPS"){
// 						m_ctrlDataRateSlave.SetCurSel(6);}
// 					else {
// 						m_ctrlDataRateSlave.SetCurSel(0);
// 					}
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
					//ljb 20180327 BMS Terminal
					strTemp="";
					strTemp = strData.Mid(nPos, nIndex - nPos);
					strTemp.Replace(" ", "");
					strTemp.MakeUpper();
					if(strTemp == "OPEN"){
						m_ctrlBMSTXTerminalSlave.SetCurSel(0);}
					else if(strTemp == "120OHM"){
						m_ctrlBMSTXTerminalSlave.SetCurSel(1);}
					else
						m_ctrlBMSTXTerminalSlave.SetCurSel(1);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
					
					//ljb 20180327 BMS CRC
					strTemp="";
					strTemp = strData.Mid(nPos, nIndex - nPos);
					strTemp.Replace(" ", "");
					strTemp.MakeUpper();
					if(strTemp == "NON"){
						m_ctrlTXBMSCRCTypeSlave.SetCurSel(0);}
					else if(strTemp == "CRC"){
						m_ctrlTXBMSCRCTypeSlave.SetCurSel(1);}
					else
						m_ctrlTXBMSCRCTypeSlave.SetCurSel(1);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				else
					m_ctrlDataRateSlave.SetCurSel(0);
			}
		}
		if (m_nProtocolVer < 3) m_strBmsTypeSlave = "0";
		//EnableMaskFilter(m_ckUserFilterSlave, PS_CAN_TYPE_SLAVE);
		UpdateData(FALSE);

		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		
		nPos = strData.Find("Name", 0);
		if(nPos > 4)	//ID가 Hex또는 Decimal인지 구분.. 이 방법밖에 없나??
		{
			CString strType;
			nPos = strData.Find("(", 0);
			strType = strData.Mid(nPos+1, strData.Find(")", nPos+1)-(nPos+1));
			strType.MakeUpper();
			
			if(strData.Find("DLC")>-1)
			{
				bDLC=TRUE;
			}
		}

		nPos = 0;
		while(aFile.ReadString(strData))	//Data
		{
			//2014.08.22 캔 전송 슬레이브를 사용하지않을시 그리드 적용하지않음.
			BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);
			if(isSlave == FALSE){
				break;
			}

			if(strData == "" || strData.Left(10) == ",,,,,,,,,,")
				break;
			//CString strTemp = strData.Mid(nPos, nIndex - nPos);
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			
			int nCol = 1;

			int nCnt = 0;
			m_wndCanGridSlave.InsertRows(m_wndCanGridSlave.GetRowCount()+1, nCol);
			int nRow = m_wndCanGridSlave.GetRowCount();
			while(p0 != -1)
			{			
				CString strCanData;
				nCnt++; //yulee 20181031
				if( nCnt == 6)
				{
					if( bDLC==TRUE)
					{
						strCanData = strData.Mid(p1, p0 - p1);
					}
					else
					{
						 int nTmpDLCValue;
						 nTmpDLCValue = 8;
						 strCanData.Format("%d", nTmpDLCValue);
					}
				}
				else
				{
					strCanData = strData.Mid(p1, p0 - p1);
				}

				if (strCanData == "")
				{
					p0 = -1;
					break;
				}
				if(m_nProtocolVer == 0)
				{
					if(nCol != CAN_COL_OFFSET)
					{
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else if (m_nProtocolVer == 1)
				{
					if(nCol != CAN_COL_DIVISION1)
					{
						m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
						break;
					}
				}
				else if (m_nProtocolVer == 2)
				{
					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
				}
// 				else if (m_nProtocolVer == 3)
// 				{
// 					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
// 				}
// 				else if (m_nProtocolVer == 4)
// 				{
// 					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
// 				}
				else if (m_nProtocolVer == 3)
				{
					//					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					nTemp = atoi(strCanData);
					if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
					else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
					else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
					else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
					else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
					else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
					else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
					TRACE("%s \n",strCanData);
					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					
				}
				if (m_nProtocolVer == 4)
				{
					//					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					nTemp = atoi(strCanData);
					if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
					else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
					else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
					else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
					else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
					else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
					else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
					TRACE("%s \n",strCanData);
					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
				}
				if( nCnt == 6)
				{
					if( bDLC)
					{
						p1 = p0 +1;
						p0 = strData.Find(",", p1);
					}
					else
					{
						if(m_ckCanFD)
						{
							p1 = p0 +1; //yulee 20181213 CAN TX Import Error
							p0 = strData.Find(",", p1);
						}
					}
				}
				else
				{
					p1 = p0 +1;
					p0 = strData.Find(",", p1);
				}
				//p1 = p0 +1;
				//p0 = strData.Find(",", p1);
			}				
		}	
	}//END if(strData.Find("Slave") > 0) 

	aFile.Close();
	return TRUE;
	
}

void CSettingCanFdDlg_Transmit::InitGridSlave()
{
	//ljb	2008-09-04 CAN_COL_McuTempFlag 추가

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;
	
	SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nSelCh, PS_CAN_TYPE_SLAVE);

	m_wndCanGridSlave.SubclassDlgItem(IDC_CAN_GRID_SLAVE, this);
	m_wndCanGridSlave.Initialize();

	CRect rect;
	m_wndCanGridSlave.GetParam()->EnableUndo(FALSE);
	
	//2014.08.21 캔 슬레이브 사용하지않을시 기본1행 삽입 하지않는다.
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);	
	if(isSlave == TRUE){
		//m_wndCanGridSlave.SetRowCount(1);
		m_wndCanGridSlave.SetRowCount(0); //ksj 20210305
	}
	//m_wndCanGridSlave.SetRowCount(1);
	
	m_wndCanGridSlave.SetRowHeight(0,0,50); //lyj 20200317 컬럼사이즈 조정
	m_wndCanGridSlave.SetColCount(CAN_COL_DIVISION3);		//ljb
	m_wndCanGridSlave.GetClientRect(&rect);
	m_wndCanGridSlave.SetColWidth(CAN_COL_NO			, CAN_COL_NO			, 30);
	m_wndCanGridSlave.SetColWidth(CAN_COL_ID			, CAN_COL_ID			, 60);
	//m_wndCanGridSlave.SetColWidth(CAN_COL_TIME			, CAN_COL_TIME			, 80);
	m_wndCanGridSlave.SetColWidth(CAN_COL_TIME			, CAN_COL_TIME			, 82); //lyj 20200317 컬럼사이즈 조정
	//m_wndCanGridSlave.SetColWidth(CAN_COL_NAME		, CAN_COL_NAME			, rect.Width() - 690);
	m_wndCanGridSlave.SetColWidth(CAN_COL_NAME			, CAN_COL_NAME			, 120);
	m_wndCanGridSlave.SetColWidth(CAN_COL_STARTBIT		, CAN_COL_STARTBIT		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BITCOUNT		, CAN_COL_BITCOUNT		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DLC			, CAN_COL_DLC		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BYTE_ORDER	, CAN_COL_BYTE_ORDER	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DATATYPE		, CAN_COL_DATATYPE		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_FACTOR		, CAN_COL_FACTOR		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_OFFSET		, CAN_COL_OFFSET		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DEFAULT		, CAN_COL_DEFAULT		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION1		,CAN_COL_DIVISION1		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION2		,CAN_COL_DIVISION2		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION3		,CAN_COL_DIVISION3		, 100);
	

	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_NO			),	CGXStyle().SetValue("No"));
//	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID"));
	if(m_nMasterIDType == 1)
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	else
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
	//m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue("전송속도(ms)"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg1","IDD_CAN_FD_SETTING_TRANSMIT")));//&&
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_NAME			),	CGXStyle().SetValue("Name"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT		),	CGXStyle().SetValue("StartBit"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT		),	CGXStyle().SetValue("BitCount"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DLC			),	CGXStyle().SetValue("DLC"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER	),	CGXStyle().SetValue("ByteOrder"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE		),	CGXStyle().SetValue("DataType"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_FACTOR		),	CGXStyle().SetValue("Factor"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_OFFSET		),	CGXStyle().SetValue("Offset"));
	/*
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue("설정값"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue("분류 1"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue("분류 2"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue("분류 3"));
	*/
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg2","IDD_CAN_FD_SETTING_TRANSMIT")));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg3","IDD_CAN_FD_SETTING_TRANSMIT")));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg4","IDD_CAN_FD_SETTING_TRANSMIT")));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue(Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg5","IDD_CAN_FD_SETTING_TRANSMIT")));
	m_wndCanGridSlave.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndCanGridSlave.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(0),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_ID, CAN_COL_TIME),
		CGXStyle()
  			.SetControl(GX_IDS_CTRL_EDIT)
	);

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
// 	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// 		.SetChoiceList(_T("USER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\n"))
// 		.SetValue(_T(""))
// 	);

	m_wndCanGridSlave.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndCanGridSlave, IDS_CTRL_REALEDIT));
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	
	char str[12], str2[7], str3[5];
	sprintf(str, "%%d", 1);		
	//sprintf(str2, "%d", 2);	//정수부 //yulee 20180329 권준구c 요청 정수부 크기 2->3 
	sprintf(str2, "%d", 3);		//정수부 2->3

	//.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63")) 63 -> 511로 변경
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("511"))
			//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~511)"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("SettingCanFdDlg_Transmit_InitGridMaster_msg6","IDD_CAN_FD_SETTING_TRANSMIT"))//&&
			.SetValue(0L)
			
	);

	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(1L)
	);
	//DLC
// 	if (commonData.can_fd_flag == 0)
// 	{
// 		m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DLC),
// 			CGXStyle()
// 			.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
// 			.SetChoiceList(_T("0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n"))
// 			.SetValue(_T("8"))
// 			);
// 	}
// 	else
// 	{
		m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DLC),
			CGXStyle()
			.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX) //20180329 yulee 권준구c 요청 - 키보드로 입력가능하도록(콤보박스 리스트에 해당하는 값만 입력 가능)
			.SetChoiceList(_T("0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n12\r\n16\r\n20\r\n24\r\n32\r\n48\r\n64\r\n"))
			.SetValue(_T("8"))
			);
//	}

	//전송 속도 기본값자 자리수 셋팅
	sprintf(str, "%%d", 1);		//정수부
	sprintf(str2, "%d", 5);		
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_TIME),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		//.SetValue(10L)
		.SetValue(m_lMinCanTxTime) //ksj 20210305
	);

	sprintf(str, "%%.%dlf", 10);	//실수 포멧			ljb 20160825 박진호 요청 6->10
	sprintf(str2, "%d", 6);		    //정수부 크기		
	sprintf(str3, "%d", 10);		//소수부 크기		ljb 20160825 박진호 요청 6->10

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_FACTOR),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1L)
	);
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);
	//기본 전송 값, 기본값자 자리수 셋팅
	sprintf(str3, "%d", 6);		//소숫점 이하		// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str, "%%.%dlf", 6);						// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str2, "%d", 8);		//정수부			//ljb 12->8  [8/29/2011 XNOTE]
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);

	//division 전송 값, 기본값자 자리수 셋팅
	sprintf(str, "%%d", 2);		
	sprintf(str2, "%d", 4);		//정수부
	
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1, CAN_COL_DIVISION3),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(0L)
		);

	///////////////////////////////////////////////////

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
				.SetValue(_T("Intel"))
		);
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
				//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\n"))
				.SetValue(_T("Unsigned"))
		);

	//ljb 2011222 이재복 S //////////
// 	CString strComboItem,strTemp;
// 	strComboItem = "None[0]\r\n";
// 	for (int i=0; i < strArryCanName.GetSize() ; i++)
// 	{
// 		strTemp.Format("%s [%d]\r\n",strArryCanName.GetAt(i),uiArryCanCode.GetAt(i));
// //		strTemp.Format("[%d]",uiArryCanCode.GetAt(i));
// 		strComboItem = strComboItem + strArryCanName.GetAt(i) + "\r\n";		
// 	}
// 	//////////////////////////////////////////////////////////////////////////
// 
// 	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// //		.SetChoiceList(_T("NONE\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
// //		.SetChoiceList(strComboItem)
// 		.SetChoiceList(_T("NONE\r\n"))
// 		.SetValue(_T("NONE"))
// 		);
	CString strComboItem,strTemp,strTemp2;
	strComboItem = "None [0]\r\n";
	for (int i=0; i < strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",strArryCanName.GetAt(i),uiArryCanCode.GetAt(i));
		strTemp2 = strTemp.Left(2);
		strTemp2.MakeUpper();
		
		if (strTemp2 == "TX") strComboItem = strComboItem + strTemp;
	}
	//////////////////////////////////////////////////////////////////////////
	
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		//		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetControl(GX_IDS_CTRL_COMBOBOX)			//쓰기도 가능 //yulee 20190714 check
		// 		.SetChoiceList(_T("NONE\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
		//		.SetChoiceList(strComboItem)
		//		.SetChoiceList(_T("NONE\r\n"))
		.SetChoiceList(strComboItem)
		.SetValue(_T("None [0]"))
		);

}

void CSettingCanFdDlg_Transmit::OnBtnAddSlave() 
{
	// TODO: Add your control notification handler code here
	CRowColArray ra;
	m_wndCanGridSlave.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndCanGridSlave.InsertRows(ra[0] + 1, 1);
	else
		m_wndCanGridSlave.InsertRows(m_wndCanGrid.GetRowCount()+1, 1);
	
	
	IsChangeFlag(TRUE);
	
}

void CSettingCanFdDlg_Transmit::OnBtnDelSlave() 
{
	CRowColArray ra;
	m_wndCanGridSlave.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndCanGridSlave.RemoveRows(ra[i], ra[i]);
	}
	
	IsChangeFlag(TRUE);		
}
void CSettingCanFdDlg_Transmit::ReDrawMasterGrid()
{
	UpdateData();
	if(m_nMasterIDType == 1)
	{
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	}
	else
	{
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
	}
	
	int i = 0;
	CString strID, strTemp;
	for(i = 1; i <= m_wndCanGrid.GetRowCount(); i++)
	{
		strTemp = m_wndCanGrid.GetValueRowCol(i, CAN_COL_ID);
		if(m_nMasterIDType == 0)
		{
			strID.Format("%X", strtoul(strTemp , NULL, 10));
		}
		else
		{
			strID.Format("%d", strtoul(strTemp , NULL, 16));
		}
		m_wndCanGrid.SetValueRange(CGXRange(i, CAN_COL_ID), strID);
	}
	
	for(i = 1; i <= m_wndCanGridSlave.GetRowCount(); i++)
	{
		strTemp = m_wndCanGridSlave.GetValueRowCol(i, CAN_COL_ID);
		if(m_nMasterIDType == 0)
		{
			strID.Format("%X", strtoul(strTemp , NULL, 10));
		}
		else
		{
			strID.Format("%d", strtoul(strTemp , NULL, 16));
		}
		m_wndCanGridSlave.SetValueRange(CGXRange(i, CAN_COL_ID), strID);
	}
	
}
void CSettingCanFdDlg_Transmit::OnRadio3() 
{
	// TODO: Add your control notification handler code here
	ReDrawMasterGrid();
	
}

void CSettingCanFdDlg_Transmit::OnRadio4() 
{
	// TODO: Add your control notification handler code here
	ReDrawMasterGrid();
	
}

void CSettingCanFdDlg_Transmit::OnCancel() 
{
	if(IsChange)
	{
		//if(AfxMessageBox("CAN 정보가 변경되었습니다.\r\n작업을 취소하시겠습니까?", MB_YESNO) != IDYES)
		if(AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnCancel_msg1","IDD_CAN_FD_SETTING_TRANSMIT"), MB_YESNO) != IDYES)//&&
			return;
	}
	IsChange = FALSE;
	
	CDialog::OnCancel();
}

void CSettingCanFdDlg_Transmit::InitParamTab()
{
	m_ctrlParamTab.ModifyStyle(TCS_BOTTOM|TCS_MULTILINE|TCS_VERTICAL|TCS_BUTTONS, TCS_OWNERDRAWFIXED|TCS_FIXEDWIDTH);

	TC_ITEM item;
	item.mask = TCIF_TEXT;
	item.pszText = _T("Master");
	m_ctrlParamTab.InsertItem(TAB_MASTER, &item);

	//2014.07.07 슬레이브 사용유무
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);;	
	if(isSlave == TRUE){
		item.pszText = "Slave";	
		m_ctrlParamTab.InsertItem(TAB_SLAVE, &item);
	}
	m_ctrlParamTab.SetColor(RGB(0,0,0), RGB(255,255,255), RGB(240,240,240), RGB(240,240,240));	// 글자색, 바깥쪽 라인, 내부 색깔, 이미지쪽 라인
	
	//Master and Slave button size 같게 설정
	Fun_ChangeSizeControl(IDC_BTN_ADD_MASTER,IDC_BTN_ADD_SLAVE);
	Fun_ChangeSizeControl(IDC_BTN_DEL_MASTER,IDC_BTN_DEL_SLAVE);
	Fun_ChangeSizeControl(IDC_CAN_GRID,IDC_CAN_GRID_SLAVE);
	//Fun_ChangeSizeControl(IDC_EDIT_CAN_ID,IDC_EDIT_SLAVE_CANID);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BAUDRATE,IDC_COMBO_CAN_BAUDRATE_SLAVE);
	Fun_ChangeSizeControl(IDC_COMBO_TX_CAN_DATARATE,IDC_COMBO_TX_CAN_DATARATE2);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BMS_TYPE,IDC_COMBO_CAN_BMS_TYPE_SLAVE);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BMS_SJW,IDC_COMBO_CAN_BMS_SJW_SLAVE);
	Fun_ChangeSizeControl(IDC_CHECK_EXT_MODE,IDC_CHECK_EXT_MODE_SLAVE);
	Fun_ChangeSizeControl(IDC_CHECK_TX_CAN_FD,IDC_CHECK_TX_CAN_FD2);
	Fun_ChangeSizeControl(IDC_COMBO_TX_CAN_TERMINAL_M,IDC_COMBO_TX_CAN_TERMINAL_M2);
	Fun_ChangeSizeControl(IDC_COMBO_TX_CAN_CRC_M,IDC_COMBO_TX_CAN_CRC_S);
	
	//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
	//if(!bUseCANFD)
	//{
	//	
	//}
	//else
	//{
	//	GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
	//	GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
	//}

	//ksj 20200213 : 위에 왜 주석처리 되어있고 if 조건이 바뀌어있는지 모르겠다.
    //뭔가 비정상동작하도록 꼬아놓은 것 같아.
	//예전 코드로 롤백. (최초 커밋)
	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
 	if(bUseCANFD)
 	{
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(FALSE); //ksj 20200310
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(FALSE); //ksj 20200310
 	}
 	else
	{
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(TRUE); //ksj 20200310 : CAN FD 기능 사용안할때는 SJW 강제로 활성화.
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(TRUE); //ksj 20200310
	}

	GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	// 		GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD); //ksj 20200310 : 주석처리. 첫 초기화시 Slave 관련 컨트롤은 보이게 하지 않는다.
	// 		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD); //ksj 20200310 : 주석처리. 첫 초기화시 Slave 관련 컨트롤은 보이게 하지 않는다.
	// 		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD); //ksj 20200310 : 주석처리. 첫 초기화시 Slave 관련 컨트롤은 보이게 하지 않는다.
	// 		GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD); //ksj 20200310 : 주석처리. 첫 초기화시 Slave 관련 컨트롤은 보이게 하지 않는다.

	//ksj end
}

void CSettingCanFdDlg_Transmit::Fun_ChangeViewControl(BOOL bFlag)
{
	if(bFlag)
	{
		GetDlgItem(IDC_FRAME_SETTING)->SetWindowText("Transmit Master Setting");
		GetDlgItem(IDC_BTN_ADD_MASTER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DEL_MASTER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAN_GRID)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_EDIT_CAN_ID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_M)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_EXT_MODE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_TX_CAN_FD)->ShowWindow(SW_SHOW);
		//m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_ADD_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_DEL_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAN_GRID_SLAVE)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_EDIT_SLAVE_CANID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_S)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_TX_CAN_FD2)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_EDIT_CHARGE_ON_VOLT)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_EDIT_ON_DELAY_TIME)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_EDIT_KEYON_VOLT_MIN)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_EDIT_KEYON_VOLT_MAX)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_EDIT_BMS_RESTART_TIME)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL1)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL2)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL3)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL4)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL10)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL5)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL6)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL7)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL8)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_STATIC_LABEL9)->ShowWindow(SW_SHOW);

		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(!bUseCANFD)
		//{
		//	
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
		//}

		//ksj 20200213 : 위에 왜 주석처리 되어있고 if 조건이 바뀌어있는지 모르겠다.
    	//뭔가 비정상동작하도록 꼬아놓은 것 같아.
		//예전 코드로 롤백. (최초 커밋)
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		if(bUseCANFD)
		{
			
		}
		else
		{
			GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
		}
		//ksj end
	}
	else
	{
		GetDlgItem(IDC_FRAME_SETTING)->SetWindowText("Transmit Slave Setting");
		GetDlgItem(IDC_BTN_ADD_MASTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_DEL_MASTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAN_GRID)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_EDIT_CAN_ID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_M)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_EXT_MODE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_TX_CAN_FD)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_BTN_ADD_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DEL_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAN_GRID_SLAVE)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_EDIT_SLAVE_CANID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_S)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_TX_CAN_FD2)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_EDIT_CHARGE_ON_VOLT)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_EDIT_ON_DELAY_TIME)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_EDIT_KEYON_VOLT_MIN)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_EDIT_KEYON_VOLT_MAX)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_EDIT_BMS_RESTART_TIME)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL1)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL2)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL3)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL4)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL10)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL5)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL6)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL7)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL8)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_STATIC_LABEL9)->ShowWindow(SW_HIDE);

		//BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(!bUseCANFD)
		//{
		//	
		//}
		//else
		//{
		//	GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
		//	GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		//}

		//ksj 20200213 : 위에 왜 주석처리 되어있고 if 조건이 바뀌어있는지 모르겠다.
   		//뭔가 비정상동작하도록 꼬아놓은 것 같아.
		//예전 코드로 롤백. (최초 커밋)
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		if(bUseCANFD)
		{
			
		}
		else
		{
			GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		}
		//ksj end
	}
}

void CSettingCanFdDlg_Transmit::Fun_ChangeSizeControl(UINT uiMaster, UINT uiSlave)
{
	CRect	rect;
	GetDlgItem(uiMaster)->GetWindowRect(rect);
	ScreenToClient(&rect);
	GetDlgItem(uiSlave)->SetWindowPos(NULL,rect.left,rect.top,rect.Width(),rect.Height(),SWP_HIDEWINDOW);
	
}

void CSettingCanFdDlg_Transmit::OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nTabNum = m_ctrlParamTab.GetCurSel();
	TRACE("Tab %d Selected\n", nTabNum);
	
	switch(nTabNum)
	{
		
	case TAB_MASTER:		//안전 조건 
		Fun_ChangeViewControl(TRUE);
		break;
		
	case TAB_SLAVE:		//Grading조건
		Fun_ChangeViewControl(FALSE);
		break;
		
	default:	//TAB_RECORD_VAL	//Reporting 조건
		Fun_ChangeViewControl(TRUE);
	}	
	
	*pResult = 0;
}

void CSettingCanFdDlg_Transmit::OnButNotUse() 
{
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return;
	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END )
	{
		//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg1","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}

	SFT_TRANS_CMD_CHANNEL_CAN_SET canTransSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canTransSetData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_TRANS_DATA commonTransData;
	ZeroMemory(&commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));
	
	for(int i=0;i<2;i++) //lyj 20200722 can clear 시 0이 아닌 default 값으로 내리기
	{
		canTransSetData.canCommonData[i].can_fd_flag = 0 ;
		canTransSetData.canCommonData[i].can_datarate = 0; 
		canTransSetData.canCommonData[i].terminal_r = 1; 
		canTransSetData.canCommonData[i].crc_type = 1;
	}

	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_MASTER);
	Sleep(200);
	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_SLAVE);
	Sleep(200);

	SFT_CAN_SET_TRANS_DATA canTransData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canTransData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);
	pMD->SetCANSetTransData(nSelCh, canTransData, 0);

		//AfxMessageBox("All setting is clear. When you open this window, every can data will be cleared");
	
	if(int nRth = m_pDoc->SetCANTransDataToModule(nSelModule, &canTransSetData, nSelCh) == FALSE)
	{
		//AfxMessageBox("CAN Trnas 정보 설정에 실패했습니다.");
		AfxMessageBox(Fun_FindMsg("SettingCanFdDlg_Transmit_OnOK_msg10","IDD_CAN_FD_SETTING_TRANSMIT"));//&&
		return;
	}

	pMD->SaveCanTransConfig();	
}

void CSettingCanFdDlg_Transmit::OnCheckTxCanFd() 
{
	UpdateData();
	
	if (m_ckCanFD) 
	{
		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_M)->EnableWindow(TRUE);

		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(!bUseCANFD)
		if(bUseCANFD) //ksj 20200213 : 조건 이상함. 첫 커밋 시점으로 롤백
		{

		}
		else
		{
			GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		}
	}
	else
	{
		m_ctrlDataRate.SetCurSel(0); //lyj 20200217 can fd 체크 해제 시 deafult 값
		m_ctrlBMSTXTerminal.SetCurSel(1); //lyj 20200217 can fd 체크 해제 시 deafult 값
		m_ctrlTXBMSCRCType.SetCurSel(1); //lyj 20200217 can fd 체크 해제 시 deafult 값

		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M)->EnableWindow(TRUE); //20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_M)->EnableWindow(FALSE);
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(!bUseCANFD)
		if(bUseCANFD) //ksj 20200213 : 조건 이상함. 첫 커밋 시점으로 롤백
		{

		}
		else
		{
			GetDlgItem(IDC_CHECK_TX_CAN_FD2			)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2 )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_S      )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		}
	}
		IsChangeFlag(TRUE);
	
	UpdateData(FALSE);	
}

void CSettingCanFdDlg_Transmit::OnCheckTxCanFd2() 
{
	UpdateData();
	
	if (m_ckCanFD_Slave) 
	{
		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_S)->EnableWindow(TRUE);
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(!bUseCANFD)
		if(bUseCANFD) //ksj 20200213 : 조건 이상함. 첫 커밋 시점으로 롤백
		{
			
		}
		else
		{
			GetDlgItem(IDC_CHECK_TX_CAN_FD2			)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2 )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_S       )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
		}
	}
	
	else
	{
		m_ctrlDataRateSlave.SetCurSel(0); //lyj 20200217 can fd 체크 해제 시 deafult 값
		m_ctrlBMSTXTerminalSlave.SetCurSel(1); //lyj 20200217 can fd 체크 해제 시 deafult 값
		m_ctrlTXBMSCRCTypeSlave.SetCurSel(1); //lyj 20200217 can fd 체크 해제 시 deafult 값

		GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2)->EnableWindow(TRUE);//20180525 yulee, 김재호c - CAN FD 사용되는 장비에서는 CAN FD Check/Uncheck 상관 없이 활성화 
		GetDlgItem(IDC_COMBO_TX_CAN_CRC_S)->EnableWindow(FALSE);
		BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); //yulee 20181030
		//if(!bUseCANFD)
		if(bUseCANFD) //ksj 20200213 : 조건 이상함. 첫 커밋 시점으로 롤백
		{
			
		}
		else
		{
			GetDlgItem(IDC_CHECK_TX_CAN_FD2			)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2 )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_COMBO_TX_CAN_CRC_S       )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
			GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	
		}
	}
		IsChangeFlag(TRUE);
	
	UpdateData(FALSE);		
}

void CSettingCanFdDlg_Transmit::OnCheckExtMode() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnCheckExtModeSlave() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboCanBaudrate() 
{
	// TODO: Add your control notification handler code here
	IsChangeFlag(TRUE);	
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboCanBmsSjw() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboCanBmsType() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboTxCanDatarate() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboTxCanTerminalM() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboTxCanCrcM() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboTxCanDatarate2() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboCanBmsSjwSlave() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboCanBmsTypeSlave() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboCanBaudrateSlave() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboTxCanTerminalM2() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);
}

void CSettingCanFdDlg_Transmit::OnSelchangeComboTxCanCrcS() 
{
	// TODO: Add your control notification handler code here
		IsChangeFlag(TRUE);

}

// ksj 20200213 : CanFD 사용하는 경우에 컨트롤 활성화 기능 개선 추가.
int CSettingCanFdDlg_Transmit::ShowCanFDCtrl(void)
{
	BOOL bUseCANFD = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use CanFD", 0); 
	
	GetDlgItem(IDC_CHECK_TX_CAN_FD			)->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE   )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_CRC_M      )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_CHECK_TX_CAN_FD2        )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_DATARATE2  )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_TERMINAL_M2      )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_TX_CAN_CRC_S           )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_STATIC_TX_CAN_DATARATE_M)->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_STATIC_TX_CAN_TERMINAL_M   )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_STATIC_TX_CAN_CRC_M        )->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->EnableWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->EnableWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->ShowWindow(bUseCANFD);
	GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->ShowWindow(bUseCANFD);

	return 0;
}


void CSettingCanFdDlg_Transmit::OnCbnEditchangeComboCanBmsSjw()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	IsChangeFlag(TRUE);
}


void CSettingCanFdDlg_Transmit::OnCbnEditchangeComboCanBmsType()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	IsChangeFlag(TRUE);
}
