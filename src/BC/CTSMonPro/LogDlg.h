#if !defined(AFX_LOGDLG_H__05CEE3D3_A491_4243_B1E0_469E00BC8684__INCLUDED_)
#define AFX_LOGDLG_H__05CEE3D3_A491_4243_B1E0_469E00BC8684__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogDlg.h : header file
//

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CLogDlg dialog


//���� Dialog
#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED           0x00080000
#define LWA_COLORKEY            0x00000001
#define LWA_ALPHA               0x00000002
#endif // ndef WS_EX_LAYERED

typedef BOOL (WINAPI *lpfnSetLayeredWindowAttributes)(HWND hWnd, COLORREF crKey, BYTE bAlpha, DWORD dwFlags);

class CLogDlg : public CDialog
{
// Construction
public:
	void Fun_SetTitle(CString strTitle);
	void ResetTransparent();
	void SetLogEvent();
	void WriteLog(CString strLogString, INT nLevel = 0);
	void WriteEventLog(CString strLogString, INT nLevel = 0);
	void SetWinPos(CRect rect, BOOL bTopMost);
	CLogDlg(CWnd* pParent = NULL);   // standard constructor
	lpfnSetLayeredWindowAttributes m_pSetLayeredWindowAttributes;

// Dialog Data
	//{{AFX_DATA(CLogDlg)
	enum { IDD = IDD_LOG_DLG , IDD2 = IDD_LOG_DLG_ENG , IDD3 = IDD_LOG_DLG_PL };
	CListBox	m_ctrlLogList;
	BOOL	m_bTopMostFlag;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogDlg)
	public:
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strInstallPath;
	CString m_strLogFileName;
	int m_nHideDelay;
	int m_nLogLevel;
	BOOL m_bUseTranparent;
	DWORD m_dwElapsedTime;
	INT m_nCount;
	BOOL m_bUseAutoShow;

	// Generated message map functions
	//{{AFX_MSG(CLogDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnMostTopCheck();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnLogClear();
	afx_msg void OnLogConfig();
	afx_msg void OnFileViewButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGDLG_H__05CEE3D3_A491_4243_B1E0_469E00BC8684__INCLUDED_)
