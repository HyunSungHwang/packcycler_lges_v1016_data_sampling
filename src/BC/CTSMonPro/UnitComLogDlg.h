#if !defined(AFX_UNITCOMLOGDLG_H__CED53F91_8D8E_43FB_A240_F0D76ED664F2__INCLUDED_)
#define AFX_UNITCOMLOGDLG_H__CED53F91_8D8E_43FB_A240_F0D76ED664F2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UnitComLogDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUnitComLogDlg dialog

class CUnitComLogDlg : public CDialog
{
// Construction
public:
	CString m_strIpAddress;
	CUnitComLogDlg(CString strIpAddress, CWnd* pParent = NULL);   // standard constructor
	

// Dialog Data
	//{{AFX_DATA(CUnitComLogDlg)
	enum { IDD = IDD_SEL_UNIT_LOG_DIALOG , IDD2 = IDD_SEL_UNIT_LOG_DIALOG_ENG , IDD3 = IDD_SEL_UNIT_LOG_DIALOG_PL };
	CListCtrl	m_wndSelList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnitComLogDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUnitComLogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSave();
	afx_msg void OnDblclkSelList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNITCOMLOGDLG_H__CED53F91_8D8E_43FB_A240_F0D76ED664F2__INCLUDED_)
