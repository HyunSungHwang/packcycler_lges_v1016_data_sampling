// ThreadExitProgress.cpp : 구현 파일입니다.
// 2020-10-23, Kang se-jun
// 프로그램 종료시 종료 처리 진행상황을 표시해주기 위한 UI 쓰레드 추가

#include "stdafx.h"
#include "CTSMonPro.h"
#include "ThreadExitProgress.h"


// CThreadExitProgress

IMPLEMENT_DYNCREATE(CThreadExitProgress, CWinThread)

CThreadExitProgress::CThreadExitProgress()
{

}

CThreadExitProgress::~CThreadExitProgress()
{
	if(m_pExitProgressWnd) //ksj 20201116 : 메모리 해제 추가
	{
		m_pExitProgressWnd->CloseWindow();
		m_pExitProgressWnd->DestroyWindow();
		delete m_pExitProgressWnd;
	}
}

BOOL CThreadExitProgress::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.

	m_pExitProgressWnd = new CExitProgressWnd();
	m_pExitProgressWnd->ShowWindow(SW_HIDE);
	m_pExitProgressWnd->UpdateWindow();

	return TRUE;
}

int CThreadExitProgress::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

void CThreadExitProgress::ShowProgressWnd(CWnd* pParent)
{
	if(!m_pExitProgressWnd) return;

	m_pExitProgressWnd->ShowWindow(SW_SHOW);		
	m_pExitProgressWnd->CenterWindow(pParent);		
}


void CThreadExitProgress::SetProgress(int nPos)
{
	if(!m_pExitProgressWnd) return;

	m_pExitProgressWnd->SetProgress(nPos);
}

BEGIN_MESSAGE_MAP(CThreadExitProgress, CWinThread)
END_MESSAGE_MAP()


// CThreadExitProgress 메시지 처리기입니다.
