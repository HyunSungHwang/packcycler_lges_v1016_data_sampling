#if !defined(AFX_REPORTDLG_H__46DCD1B5_B078_4DA1_8252_F8E12A8D71A7__INCLUDED_)
#define AFX_REPORTDLG_H__46DCD1B5_B078_4DA1_8252_F8E12A8D71A7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CReportDlg dialog

class CReportDlg : public CDialog
{
// Construction
public:
	void SetChannelData(bool bChannel[][MAX_CHBOARD_NUM], bool bRefCh[], int nModule[]);
	void Initialize();

	bool m_bChannel[MAX_MODULE_NUM][MAX_CHBOARD_NUM];
	bool m_bRefCh[MAX_CHANNEL_NUM];
	int m_nModule[MAX_MODULE_NUM];

	CReportDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CReportDlg)
	enum { IDD = IDD_CMD_REPORT_DLG , IDD2 = IDD_CMD_REPORT_DLG_ENG};
	CListBox	m_ctrlEnableList;
	CListBox	m_ctrlDisableList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CReportDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTDLG_H__46DCD1B5_B078_4DA1_8252_F8E12A8D71A7__INCLUDED_)
