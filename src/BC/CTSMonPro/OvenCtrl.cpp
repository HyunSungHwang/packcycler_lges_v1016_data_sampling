// OvenCtrl.cpp: implementation of the COvenCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "CTSMonProDoc.h"

#include "OvenCtrl.h"
/*#include "WarningDlg.h"*/

#include "Global.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COvenCtrl::COvenCtrl()
	: m_bRequestLock(FALSE)
{
	m_nInstallCount = 1;
	m_nOvenModelType = CHAMBER_TH500;		//Default TH500 Model;
 	//m_OvenData[0].SetCurTemperature(0.0f);
 	//m_OvenData[1].SetCurTemperature(0.0f);
	m_nRxBuffCnt = 0;
	m_bUseOvenCtrl = FALSE;
	//ZeroMemory(&m_SerialConfig, sizeof(SERIAL_CONFIG)*MAX_OVEN_COUNT);

	for(int i=0; i<MAX_OVEN_COUNT; i++)
	{
		m_pSerial[i]  = NULL;
		m_OvenData[i].SetCurTemperature(0.0f);
	}

	m_hThreadPump=NULL;
	m_iPumpDeviceID=0;
	m_iPumpEmptyCount=0;
	m_iPumpCHNO=0;
	m_iPumpReqStop=0;
	memset(&m_PumpState,0,sizeof(m_PumpState));	
	m_PumpState.nPlay=-1;
	m_PumpState.nMode=-1;
	m_ctx = NULL;//yulee 20190611_1 //yulee 20190614_*
	m_pCS = NULL;//yulee 20190611_1 //yulee 20190614_*
	m_nCurTempValueCnt = 0;//yulee 20190715
	//m_fltCurTemp[MAbCX_FILO_SIZE] = {0,0,0}; //yulee 20190715


	m_PV_temp1	= 0.; 
	m_PV_temp2	= 0.; 
	m_PV_temp3	= 0.;//		
	m_SP_temp1	= 0.; 
	m_SP_temp2	= 0.; 
	m_SP_temp3	= 0.;//	
	m_OPState1	= -1; 
	m_OPState2	= -1; 
	m_OPState3	= -1;//			pcss : Run/Stop 관련 Reg 0:STOP 1:RUN
	m_OPMode1	= 0; 
	m_OPMode2	= 0; 
	m_OPMode3	= 0;//			6. 0:PROG, 1:FIX 관련 Reg

	m_PV_humi	= 0.;
	m_SP_humi	= 0.;
	m_PV_Pump	= 0.;
	m_SP_Pump	= 0.;
	m_OPState_Pump = -1;
	m_OPMode_Pump = 0;//	
	m_nErrModbusCnt = 0; //yulee 20190717

	m_nOvenPortNum = 0;
	m_bChiller_DESChiller = 0; //lyj 20201102 DES칠러
}

COvenCtrl::~COvenCtrl()
{
	//for(int j=0; j<m_apSerial.GetSize(); j++)
	//{
	//	CSerialPort *pPort = (CSerialPort *)m_apSerial.GetAt(j);
	//	if(pPort != NULL)
	//	{
	//		delete pPort;
	//		pPort = NULL;
	//	}
	//}
	//m_apSerial.RemoveAll();

	GWin::win_ThreadEnd2(m_hThreadPump);
}

void COvenCtrl::ClearCurrentData()
{
	for(int i=0; i<MAX_OVEN_COUNT; i++)
	{
		if (m_pSerial[i] != NULL)
		{
			m_OvenData[i].SetCurTemperature(0.0f);
			m_OvenData[i].SetRefTemperature(0.0f);	//ljb 20180219 add
			if(m_nOvenModelType == CHILLER_TEMP2520)
			{
				m_OvenData[i].SetCurTemperatureCh2(0.0f);
				m_OvenData[i].SetRefTemperatureCh2(0.0f);
			}
		}
		if (m_pSerial[i] != NULL)
		{
			m_OvenData[i].SetCurHumidity(0.0f);
			m_OvenData[i].SetRefHumidity(0.0f);	//ljb 20180219 add
		}
	}
}

BOOL COvenCtrl::SetCurrentData(int nIndex, char szBuffData[])
{
	switch(m_nOvenModelType)
	{
	case CHAMBR_WiseCube:
		SetCurrentDataWiseCube(nIndex, szBuffData);
		break;
	case CHAMBER_TD500:
		SetCurrentDataTD500(nIndex, szBuffData);
		break;
	case CHAMBER_TH500:					//TH500
		SetCurrentDataTH500(nIndex, szBuffData);
		break;
	case CHAMBER_TEMI880:
		SetCurrentDataTEMI880(nIndex, szBuffData);
		break;
	case CHAMBER_TEMP880:					//20090113 KHS TEMP880 추가
		SetCurrentDataTEMP880(nIndex, szBuffData);
		break;	
	case CHAMBER_TEMP2300://lmh 20140429 Temp2300 add
		SetCurrentDataTemp2300(nIndex, szBuffData);
		break;
	case CHAMBER_TEMI2300://ljb 20140429 add
		SetCurrentDataTemi2300(nIndex, szBuffData);
		break;
	case CHAMBER_TEMP2500://lmh 20120524 Temp2500 add
		SetCurrentDataTemp2500(nIndex, szBuffData);
		break;
	case CHAMBER_TEMI2500://ljb 20130704 add
		SetCurrentDataTemi2500(nIndex, szBuffData);
		break;
	case CHILLER_TEMP2520://ljb 20180219 add
		SetCurrentDataChillerTEMP2520(nIndex, szBuffData);
		break;
	case CHILLER_ST590:		//ksj 20200401 : ST590 추가.
		SetCurrentDataChillerST590(nIndex, szBuffData); //880이랑 다르면 수정 필요.
		break;	
	default:
		break;
	}

	return TRUE;
}

//yulee 20190611  //yulee 20190614_*
BOOL COvenCtrl::SetCurrentDataByModbus(int nIndex, float CurTempCh1, float SetTempCh1, int OPStateCh1, int OPModeCh1
	, float CurTempCh2, float SetTempCh2, int OPStateCh2, int OPModeCh2
	, float CurTempCh3, float SetTempCh3, int OPStateCh3, int OPModeCh3
	, float CurHumi, float SetHumi, float CurPump, FLOAT SetPump, int OPStatePump, int OPModePump) //yulee 20190605
{   //(0, PV_temp, SP_temp, OPState, OPMode, PV_humi, SP_humi, PV_Pump, SP_Pump, OPState_Pump, OPMode_Pump)
	switch(m_nOvenModelType)
	{
	case CHAMBER_MODBUS_NEX1000: //yulee 20190605
		{
			CurHumi *=0;
			SetHumi *=0;
			CurPump *=0;
			SetPump *=0;
			OPStatePump = -2;
			OPModePump  = -2;
			break;
		}
	case CHAMBER_MODBUS_NEX1100:
		{
			CurHumi *=0;
			SetHumi *=0;
			break;
		}
	case CHAMBER_MODBUS_NEX1200:
		{
			CurPump *=0;
			SetPump *=0;
			OPStatePump = -2;
			OPModePump  = -2;
			break;
		}
		break;
	default:
		break;
	}
	SetCurrentDataNEX(nIndex, CurTempCh1, SetTempCh1, OPStateCh1, OPModeCh1, 
		CurTempCh2, SetTempCh2, OPStateCh2, OPModeCh2,
		CurTempCh3, SetTempCh3, OPStateCh3, OPModeCh3,
		CurHumi, SetHumi, CurPump, SetPump, OPStatePump, OPModePump);

	return TRUE;
}

void COvenCtrl::SetVersionInfo(int nIndex, CString strVer)
{
	m_OvenData[nIndex].SetVersion(strVer);
}

int COvenCtrl::GetOvenIndex(int nModuleID, int nChIndex)
{
	int nID = 0;
	CDWordArray dwChArray;
	DWORD dwSelCh = MAKELONG(nChIndex, nModuleID);
	for(int i=0; i<GetOvenCount(); i++)
	{
		for(int ch = 0; ch<m_OvenData[i].m_awChMapArray.GetSize(); ch++)
		{
			DWORD dwData = m_OvenData[i].m_awChMapArray[ch];
			//20180430 yulee dwData는 챔버 할당할 때의 채널 번호를 Ch1-65536, Ch2-65537, Ch3-65538, Ch4-65539로 표시 
			//dwSelCh은 챔버 번호에 따라서 Line1은 65536, Line2는 65537로 표시 
			if(dwData == dwSelCh)
			{
				return nID;
			}
		}
		nID++;
	}
	return -1;
}

//adwChArray 항목이 설치된  Oven List 를 구한다.
BOOL COvenCtrl::GetOvenIndexList(CDWordArray &adwChArray, CDWordArray &adwOvenIndex)
{
	DWORD dwData;
	int nOvenIndex;
	adwOvenIndex.RemoveAll();
	for(int i=0; i<	adwChArray.GetSize(); i++)
	{
		dwData = adwChArray[i];	
		nOvenIndex =  GetOvenIndex(HIWORD(dwData), LOWORD(dwData));
		if(nOvenIndex >=0)
		{
			BOOL bFind = FALSE;
			for(int j=0; j<adwOvenIndex.GetSize(); j++)
			{
				if((DWORD)nOvenIndex == adwOvenIndex[j])
				{
					bFind = TRUE;
					break;
				}
			}
			if(bFind == FALSE)	adwOvenIndex.Add((DWORD)nOvenIndex);
		}
	}
	return TRUE;
}

BOOL COvenCtrl::GetRun(int nindex)
{
	return m_OvenData[nindex].GetRunState();
}

BOOL COvenCtrl::GetPumpRun(int nindex)
{
	return m_OvenData[nindex].GetRunState2();
}

long COvenCtrl::GetRunWorkMode(int nindex)
{
	return m_OvenData[nindex].GetRunWorkMode();
}

long COvenCtrl::GetRunPumpWorkMode(int nindex)
{
	return m_OvenData[nindex].GetRunWorkMode2();
}

BOOL COvenCtrl::GetPatternSaveDon(int nindex)
{
	return m_OvenData[nindex].GetPatternSaveDone();
}

void COvenCtrl::ResetData(int nIndex)
{
	m_OvenData[nIndex].ResetData();
}

int COvenCtrl::GetRunStepNo(int nIndex)
{
	return m_OvenData[nIndex].GetRunStepNo();
}

void COvenCtrl::SetRunStepNo(int nOvenIndex, int nStepNo)
{
	m_OvenData[nOvenIndex].SetRunStepNo(nStepNo);
}

long COvenCtrl::GetRunTime(int nIndex)
{
	return m_OvenData[nIndex].GetRunTime();
}

//return Value : Command Length
int COvenCtrl::GetReqCurrentValueCmd(int nDeviceNumber, char * szCmd, int & nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'R';
		szCmd[5] = 'C';
		szCmd[6] = 'V';
		szCmd[7] = 0x03;
		szCmd[8] = 0x0d;
		szCmd[9] = 0x0a;

		nRetNum = 10;
		nResLen = 59;
		break;
		// ==============================
		// Model TEMI880
		// ==============================
	case CHAMBER_TEMI880:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '1';
			szCmd[8] = '1';				//ljb 11
			szCmd[9] = ',';
			szCmd[10] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';			//현재 습도 값 Reg (현재 습도 PV)
			szCmd[21] = '0';
			szCmd[22] = '0';
			szCmd[23] = '5';
			szCmd[24] = ',';
			szCmd[25] = '0';			//현재 설정습도 값 Reg (현재 습도 SP)
			szCmd[26] = '0';
			szCmd[27] = '0';
			szCmd[28] = '6';
			szCmd[29] = ',';

			szCmd[30] = '0';			//Run/Stop 관련 Reg	// 1:Run, 2:HOLD, 3: STEP, 4:Stop 관련 Reg
			szCmd[31] = '0';
			szCmd[32] = '1';
			szCmd[33] = '1';
			szCmd[34] = ',';

			szCmd[35] = '0';			// 0:PROG, 1:FIX 관련 Reg
			szCmd[36] = '1';
			szCmd[37] = '0';
			szCmd[38] = '4';
			// 20090527 KHM S
			szCmd[39] = ',';
			szCmd[40] = '0';			//현재 Spark(DI3), Smoke sensor(DI4) 값 Reg (현재 온도 SP)
			szCmd[41] = '0';
			szCmd[42] = '1';
			szCmd[43] = '8';
			
			// 20090713 ljb			for Run Time : Hour
			szCmd[44] = ',';
			szCmd[45] = '0';			
			szCmd[46] = '0';
			szCmd[47] = '2';
			szCmd[48] = '0';
			
			// 20090713 ljb			for Run Time : Min
			szCmd[49] = ',';
			szCmd[50] = '0';			
			szCmd[51] = '0';
			szCmd[52] = '2';
			szCmd[53] = '1';
			
			// 20100604 ljb			for Pattern Done Check
			szCmd[54] = ',';
			szCmd[55] = '1';			
			szCmd[56] = '0';
			szCmd[57] = '0';
			szCmd[58] = '4';
			
			// 20100604 ljb			for Pattern Done Check
			szCmd[59] = ',';
			szCmd[60] = '0';			
			szCmd[61] = '0';
			szCmd[62] = '3';
			szCmd[63] = '6';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 64; i++)
			{
				pcl1 += szCmd[i];
			}
			
			sprintf(&szCmd[64], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[65], "%X", pcl1&0x000f);
			szCmd[66] = 0x0d;
			szCmd[67] = 0x0a;
			
			nRetNum = 68;
			nResLen = 68;
		}
		break;
		// ==============================
		// Model WiseCube
		// ==============================
	case CHAMBR_WiseCube:
		szCmd[0] = '#';
		szCmd[1] = '0';
		szCmd[2] = '2';
		szCmd[3] = 't';
		szCmd[4] = 'p';

		nRetNum = 5;
		nResLen = 11;
		break;
		
		//////////////////////////////////////////////////////////////////////////////		// ==============================
		// 20090113 KHS TEMP880 추가
		// Model TEMP880
		// ==============================
	case CHAMBER_TEMP880:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '9';				//ljb 4->6 -> 8
			szCmd[9] = ',';
			szCmd[10] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '0';
			szCmd[22] = '1';
			szCmd[23] = '1';
// 			szCmd[19] = ',';
// 			szCmd[20] = '0';			// 1:Run, 2:HOLD, 3: STEP, 4:Stop 관련 Reg
// 			szCmd[21] = '1';
// 			szCmd[22] = '0';
// 			szCmd[23] = '1';
			szCmd[24] = ',';
			szCmd[25] = '0';			// 0:PROG, 1:FIX 관련 Reg
			szCmd[26] = '1';
			szCmd[27] = '0';
			szCmd[28] = '4';
			// 20090527 KHM S
			szCmd[29] = ',';
			szCmd[30] = '0';			//현재 Spark(DI3), Smoke sensor(DI4) 값 Reg (현재 온도 SP)
			szCmd[31] = '0';
			szCmd[32] = '1';
			szCmd[33] = '8';
			
			// 20090713 ljb			for Run Time : Hour
			szCmd[34] = ',';
			szCmd[35] = '0';			
			szCmd[36] = '0';
			szCmd[37] = '2';
			szCmd[38] = '0';
			
			// 20090713 ljb			for Run Time : Min
			szCmd[39] = ',';
			szCmd[40] = '0';			
			szCmd[41] = '0';
			szCmd[42] = '2';
			szCmd[43] = '1';
			
			// 20100604 ljb			for Pattern Done Check
			szCmd[44] = ',';
			szCmd[45] = '1';			
			szCmd[46] = '0';
			szCmd[47] = '0';
			szCmd[48] = '4';

			// 20100604 ljb			for Pattern Done Check
			szCmd[49] = ',';
			szCmd[50] = '0';			
			szCmd[51] = '0';
			szCmd[52] = '3';
			szCmd[53] = '6';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 54; i++)
			{
				pcl1 += szCmd[i];
			}
					
			sprintf(&szCmd[54], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[55], "%X", pcl1&0x000f);
			szCmd[56] = 0x0d;
			szCmd[57] = 0x0a;
			
			nRetNum = 58;
			nResLen = 58;
		}
		break;
	case CHAMBER_TEMP2300: //lmh 20120524 chamber temp2500
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '9';				//ljb 4->6 -> 8
			szCmd[9] = ',';
			szCmd[10] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '3';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '0';
			szCmd[22] = '2';
			szCmd[23] = '4';
			szCmd[24] = ',';
			szCmd[25] = '0';			// 0:PROG, 1:FIX 관련 Reg
			szCmd[26] = '1';
			szCmd[27] = '0';
			szCmd[28] = '6';
			// 20090527 KHM S
			szCmd[29] = ',';
			szCmd[30] = '0';			//현재 Spark(DI3), Smoke sensor(DI4) 값 Reg (현재 온도 SP)
			szCmd[31] = '0';
			szCmd[32] = '3';
			szCmd[33] = '0';
			
			// 20090713 ljb			for Run Time : Hour
			szCmd[34] = ',';
			szCmd[35] = '0';			
			szCmd[36] = '0';
			szCmd[37] = '3';
			szCmd[38] = '4';
			
			// 20090713 ljb			for Run Time : Min
			szCmd[39] = ',';
			szCmd[40] = '0';			
			szCmd[41] = '0';
			szCmd[42] = '3';
			szCmd[43] = '5';
			
			// 20100604 ljb			for Pattern Done Check
			szCmd[44] = ',';
			szCmd[45] = '2';			
			szCmd[46] = '1';
			szCmd[47] = '0';
			szCmd[48] = '8';
			
			// 20100604 ljb			for Pattern Done Check
			szCmd[49] = ',';
			szCmd[50] = '0';			
			szCmd[51] = '0';
			szCmd[52] = '6';
			szCmd[53] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 54; i++)
			{
				pcl1 += szCmd[i];
			}
			
			sprintf(&szCmd[54], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[55], "%X", pcl1&0x000f);
			szCmd[56] = 0x0d;
			szCmd[57] = 0x0a;
			
			nRetNum = 58;
			nResLen = 58;
		}
		break;
	case CHAMBER_TEMI2300: //ljb 20130704
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '9';				//ljb 4->6 -> 8 -> 11
			szCmd[9] = ',';
			szCmd[10] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';			//현재 습도 값 Reg (현재 습도 PV)
			szCmd[21] = '0';
			szCmd[22] = '0';
			szCmd[23] = '5';
			szCmd[24] = ',';
			szCmd[25] = '0';			//현재 습도 값 Reg (현재 습도 SP)
			szCmd[26] = '0';
			szCmd[27] = '0';
			szCmd[28] = '6';
			szCmd[29] = ',';

			szCmd[30] = '0';			// 0:PROG, 1:FIX 관련 Reg
			szCmd[31] = '1';
			szCmd[32] = '0';
			szCmd[33] = '4';

			szCmd[34] = ',';			//STOP 1 , Run 2
			szCmd[35] = '0';			
			szCmd[36] = '0';
			szCmd[37] = '1';
			szCmd[38] = '0';
			// 20090527 KHM S
			szCmd[39] = ',';			// ALM STS 알람 시그널 발생 정보를 나타낸다.
			szCmd[40] = '0';			//현재 Spark(DI3), Smoke sensor(DI4) 
			szCmd[41] = '0';
			szCmd[42] = '1';
			szCmd[43] = '3';
			
			// 20090713 ljb			for Run Time : Hour
			szCmd[44] = ',';
			szCmd[45] = '0';		// 운전시간 H 	
			szCmd[46] = '0';
			szCmd[47] = '2';
			szCmd[48] = '4';
			
			// 20090713 ljb			for Run Time : Min
			szCmd[49] = ',';
			szCmd[50] = '0';			
			szCmd[51] = '0';
			szCmd[52] = '2';
			szCmd[53] = '5';
			
// 			// 20100604 ljb			for Pattern Done Check
// 			szCmd[54] = ',';
// 			szCmd[55] = '2';			
// 			szCmd[56] = '1';
// 			szCmd[57] = '0';
// 			szCmd[58] = '8';
// 			
// 			// 20100604 ljb			for Pattern Done Check
// 			szCmd[59] = ',';
// 			szCmd[60] = '0';			
// 			szCmd[61] = '0';
// 			szCmd[62] = '6';
// 			szCmd[63] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 54; i++)
			{
				pcl1 += szCmd[i];
			}
			
			sprintf(&szCmd[54], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[55], "%X", pcl1&0x000f);
			szCmd[56] = 0x0d;
			szCmd[57] = 0x0a;
			
			nRetNum = 58;
			nResLen = 58;
		}
		break;
	case CHAMBER_TEMP2500: //lmh 20120524 chamber temp2500
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
//			szCmd[8] = '9';				//ljb 4->6 -> 8
			szCmd[8] = '5';				//ljb 4->6 -> 8
			szCmd[9] = ',';
			szCmd[10] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '3';
			szCmd[19] = ',';
			szCmd[20] = '0';			// 0:PROG, 1:FIX 관련 Reg		//ljb edit D0010
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '6';			
			szCmd[24] = ',';
			szCmd[25] = '0';			//Run/Stop 관련 Reg				//ljb edit D0010
			szCmd[26] = '0';
			szCmd[27] = '1';
			szCmd[28] = '0';
			// 20090527 KHM S
			szCmd[29] = ',';
			szCmd[30] = '0';			//현재 Spark(DI3), Smoke sensor(DI4) 값 Reg (현재 온도 SP)
			szCmd[31] = '0';
			szCmd[32] = '3';
			szCmd[33] = '0';
			
// 			// 20090713 ljb			for Run Time : Hour
// 			szCmd[34] = ',';
// 			szCmd[35] = '0';			
// 			szCmd[36] = '0';
// 			szCmd[37] = '3';
// 			szCmd[38] = '4';
// 			
// 			// 20090713 ljb			for Run Time : Min
// 			szCmd[39] = ',';
// 			szCmd[40] = '0';			
// 			szCmd[41] = '0';
// 			szCmd[42] = '3';
// 			szCmd[43] = '5';
// 			
// 			// 20100604 ljb			for Pattern Done Check
// 			szCmd[44] = ',';
// 			szCmd[45] = '2';			
// 			szCmd[46] = '1';
// 			szCmd[47] = '0';
// 			szCmd[48] = '8';
// 			
// 			// 20100604 ljb			for Pattern Done Check
// 			szCmd[49] = ',';
// 			szCmd[50] = '0';			
// 			szCmd[51] = '0';
// 			szCmd[52] = '6';
// 			szCmd[53] = '1';
			
			WORD pcl1 = 0;
			
// 			for(int i = 1 ; i < 54; i++)
// 			{
// 				pcl1 += szCmd[i];
// 			}
// 			
// 			sprintf(&szCmd[54], "%X", (pcl1&0x00f0) >> 4);
// 			sprintf(&szCmd[55], "%X", pcl1&0x000f);
// 			szCmd[56] = 0x0d;
// 			szCmd[57] = 0x0a;
// 			
// 			nRetNum = 58;
// 			nResLen = 58;

			for(int i = 1 ; i < 34; i++)
			{
				pcl1 += szCmd[i];
			}
			
			sprintf(&szCmd[34], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[35], "%X", pcl1&0x000f);
			szCmd[36] = 0x0d;
			szCmd[37] = 0x0a;
			
			nRetNum = 38;
			nResLen = 38;
		}
		break;
	case CHAMBER_TEMI2500: //ljb 20130704
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '9';				//ljb 4->6 -> 8 -> 11
			szCmd[9] = ',';
			szCmd[10] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';			//현재 습도 값 Reg (현재 습도 PV)
			szCmd[21] = '0';
			szCmd[22] = '0';
			szCmd[23] = '5';
			szCmd[24] = ',';
			szCmd[25] = '0';			//현재 습도 값 Reg (현재 습도 SP)
			szCmd[26] = '0';
			szCmd[27] = '0';
			szCmd[28] = '6';
			szCmd[29] = ',';

			szCmd[30] = '0';			// 0:PROG, 1:FIX 관련 Reg
			szCmd[31] = '1';
			szCmd[32] = '0';
			szCmd[33] = '4';

			szCmd[34] = ',';			//STOP 1 , Run 2
			szCmd[35] = '0';			
			szCmd[36] = '0';
			szCmd[37] = '1';
			szCmd[38] = '0';
			// 20090527 KHM S
			szCmd[39] = ',';			// ALM STS 알람 시그널 발생 정보를 나타낸다.
			szCmd[40] = '0';			//현재 Spark(DI3), Smoke sensor(DI4) 
			szCmd[41] = '0';
			szCmd[42] = '1';
			szCmd[43] = '3';
			
			// 20090713 ljb			for Run Time : Hour
			szCmd[44] = ',';
			szCmd[45] = '0';		// 운전시간 H 	
			szCmd[46] = '0';
			szCmd[47] = '2';
			szCmd[48] = '4';
			
			// 20090713 ljb			for Run Time : Min
			szCmd[49] = ',';
			szCmd[50] = '0';			
			szCmd[51] = '0';
			szCmd[52] = '2';
			szCmd[53] = '5';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 54; i++)
			{
				pcl1 += szCmd[i];
			}
			
			sprintf(&szCmd[54], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[55], "%X", pcl1&0x000f);
			szCmd[56] = 0x0d;
			szCmd[57] = 0x0a;
			
			nRetNum = 58;
			nResLen = 58;
		}
		break;
		// ==============================
		// Model TEMP2520
		// ==============================
	case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;				//STX
			szCmd[1] = '0';					//TEMP2520의 주소
			szCmd[2] = '1'+nDeviceNumber;	//TEMP2520의 주소
			szCmd[3] = 'R';					//CMD(RRD : D-Register의 Random 읽기(Read)
			szCmd[4] = 'R';					//CMD(RRD : D-Register의 Random 읽기(Read)
			szCmd[5] = 'D';					//CMD(RRD : D-Register의 Random 읽기(Read)
			szCmd[6] = ',';					//,
			szCmd[7] = '0';					//갯수
			szCmd[8] = '8';					//갯수 9개
			szCmd[9] = ',';					//,
			szCmd[10] = '0';				//D-Register 주소 시작
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';			//1. CH1.NPV (Ch 1 현재 온도 값)
			szCmd[14] = ',';
			szCmd[15] = '0';				
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';			//2. CH2.NPV (Ch 2 현재 펌프 값)
			szCmd[19] = ',';
			szCmd[20] = '0';				
			szCmd[21] = '0';
			szCmd[22] = '0';
			szCmd[23] = '3';			//3. CH1.NSP (Ch 1 현재 설정온도 값)
			szCmd[24] = ',';
			szCmd[25] = '0';				
			szCmd[26] = '0';
			szCmd[27] = '0';
			szCmd[28] = '4';			//4. CH2.NSP (Ch 2 현재 설정펌프 값)	
			szCmd[29] = ',';
			szCmd[30] = '0';			
			szCmd[31] = '0';
			szCmd[32] = '1';
			szCmd[33] = '0';			//5. CH1NOW.STS (Ch 1 운전 상태)
			szCmd[34] = ',';			//Run/Stop 관련 Reg
			szCmd[35] = '0';				
			szCmd[36] = '0';
			szCmd[37] = '1';
			szCmd[38] = '1';			//6. CH2NOW.STS (Ch 2 운전 상태)	
			szCmd[39] = ',';			//Run/Stop 관련 Reg
			szCmd[40] = '0';							
			szCmd[41] = '1';
			szCmd[42] = '0';
			szCmd[43] = '6';			//7. CH1 PROG:0 FIX:1		
			szCmd[44] = ',';
			szCmd[45] = '0';
			szCmd[46] = '1';
			szCmd[47] = '0';
			szCmd[48] = '7';			//8. CH2 PROG:0 FIX:1		

			WORD pcl1 = 0;			
			for(int i = 1 ; i < 49; i++)
			{
				pcl1 += szCmd[i];
			}			
			
			sprintf(&szCmd[49], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[50], "%X", pcl1&0x000f);
			
			szCmd[51] = 0x0d;
			szCmd[52] = 0x0a;
			
			nRetNum = 53;
			nResLen = 53;
		}
		break;
		////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	case CHAMBER_TD500: 
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';
			szCmd[3] = '0'+nDeviceNumber;
			szCmd[4] = ',';
			szCmd[5] = 'R';
			szCmd[6] = 'R';
			szCmd[7] = 'D';
			szCmd[8] = ',';
			szCmd[9] = '0';
			szCmd[10] = '4';			// 3 개	
			szCmd[11] = ',';
			szCmd[12] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[13] = '1';
			szCmd[14] = '2';
			szCmd[15] = '6';
			szCmd[16] = ',';
			szCmd[17] = '1';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[18] = '0';
			szCmd[19] = '0';
			szCmd[20] = '0';
			szCmd[21] = ',';
			
			szCmd[22] = '0';			//Run/Stop 관련 Reg
			szCmd[23] = '1';
			szCmd[24] = '5';
			szCmd[25] = '2';

			szCmd[26] = ',';
			szCmd[27] = '1';			// 0:PROG, 1:FIX 관련 Reg
			szCmd[28] = '0';
			szCmd[29] = '2';
			szCmd[30] = '0';
			szCmd[31] = ',';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 32; i++)
			{
				pcl1 += szCmd[i];
			}

			sprintf(&szCmd[32], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[33], "%X", pcl1&0x000f);
			szCmd[34] = 0x0d;
			szCmd[35] = 0x0a;
			
			nRetNum = 36;
			nResLen = 36;
		}
		break;

	// -
	//////////////////////////////////////////////////////////////////////////
	case CHILLER_ST590: //ksj 20200401 : ST590추가. TEMP880꺼 복사함. 세부 내용 수정 필요.
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '4';				
			szCmd[9] = ',';
			szCmd[10] = '0';			//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg (현재 온도 SP)
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '1';
			szCmd[24] = ',';
			szCmd[25] = '0';			// 0:PROG, 1:FIX 관련 Reg
			szCmd[26] = '1';
			szCmd[27] = '0';
			szCmd[28] = '5';

			WORD pcl1 = 0;

			for(int i = 1 ; i < 29; i++)
			{
				pcl1 += szCmd[i];
			}

			sprintf(&szCmd[29], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[30], "%X", pcl1&0x000f);
			szCmd[31] = 0x0d;
			szCmd[32] = 0x0a;

			nRetNum = 33;
			nResLen = 33;
		}
		break;
	default:break;
	}

	return nRetNum;
}

//return Value : Command Length
int COvenCtrl::GetReqPatternDoneCmd(int nDeviceNumber, char * szCmd, int & nResLen)
{
	if(szCmd == NULL) return -1;
	
	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'R';
		szCmd[5] = 'C';
		szCmd[6] = 'V';
		szCmd[7] = 0x03;
		szCmd[8] = 0x0d;
		szCmd[9] = 0x0a;
		
		nRetNum = 10;
		nResLen = 59;
		break;
		// ==============================
		// Model TEMI880
		// ==============================
	case CHAMBER_TEMI880:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '5';
			szCmd[9] = ',';
			szCmd[10] = '0';				//현재 온도 값 Reg
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';				//현재 설정온도 값 Reg
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';				//현재 습도 값 Reg
			szCmd[21] = '0';
			szCmd[22] = '0';
			szCmd[23] = '5';
			szCmd[24] = ',';
			szCmd[25] = '0';				//현재 설정습도 값 Reg
			szCmd[26] = '0';
			szCmd[27] = '0';
			szCmd[28] = '6';
			szCmd[29] = ',';
			szCmd[30] = '0';				//Run/Stop 관련 Reg
			szCmd[31] = '0';
			szCmd[32] = '1';
			szCmd[33] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 34; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[34], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[35], "%X", pcl1&0x000f);
			
			szCmd[36] = 0x0d;
			szCmd[37] = 0x0a;
			
			// TRACE("%x%x\r\n", szCmd[24], szCmd[25]);
			
			nRetNum = 38;
			nResLen = 38;
			
		}
		break;
		// ==============================
		// Model WiseCube
		// ==============================
	case CHAMBR_WiseCube:
		szCmd[0] = '#';
		szCmd[1] = '0';
		szCmd[2] = '2';
		szCmd[3] = 't';
		szCmd[4] = 'p';
		
		nRetNum = 5;
		nResLen = 11;
		break;
		
		//////////////////////////////////////////////////////////////////////////////		// ==============================
		// 20090113 KHS TEMP880 추가
		// Model TEMP880
		// ==============================
	case CHAMBER_TEMP880:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';					//ljb 
			szCmd[9] = ',';
			szCmd[10] = '1';				//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '4';
			
			// 20090527 KHM E
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 18;
		}
		break;
	case CHAMBER_TEMP2300: //lmh 20120524 Temp2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';					//ljb 
			szCmd[9] = ',';
			szCmd[10] = '2';				//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '8';
			
			// 20090527 KHM E
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 18;
		}
		break;
	case CHAMBER_TEMI2300: //ljb 20130704
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';					//ljb 
			szCmd[9] = ',';
			szCmd[10] = '2';				//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '8';
			
			// 20090527 KHM E
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 18;
		}
		break;		
	case CHAMBER_TEMP2500: //lmh 20120524 Temp2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';					//ljb 
			szCmd[9] = ',';
			szCmd[10] = '2';				//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '8';
			
			// 20090527 KHM E
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 18;
		}
		break;
	case CHAMBER_TEMI2500: //ljb 20130704
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';					//ljb 
			szCmd[9] = ',';
			szCmd[10] = '2';				//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '8';
			
			// 20090527 KHM E
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 18;
		}
		break;		
		////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	case CHILLER_TEMP2520: //yulee 20191022
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';					
			szCmd[9] = ',';
			szCmd[10] = '2';				//패턴 설정 완료 응답(Done : 1)
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '8';

			// 20090527 KHM E
			WORD pcl1 = 0;

			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;

			nRetNum = 18;
			nResLen = 18;
		}
		break;		
		////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	case CHAMBER_TD500: //ljb 20130704
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';
			szCmd[3] = '1'+nDeviceNumber;
			szCmd[4] = ',';
			szCmd[5] = 'R';
			szCmd[6] = 'R';
			szCmd[7] = 'D';
			szCmd[8] = ',';
			szCmd[9] = '0';
			szCmd[10] = '1';					//ljb 
			szCmd[11] = ',';
			szCmd[12] = '0';				//현재 온도 값 Reg (현재 온도 PV)
			szCmd[13] = '1';
			szCmd[14] = '2';
			szCmd[15] = '6';
			
			// 20090527 KHM E
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 16; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[16], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[17], "%X", pcl1&0x000f);
			szCmd[18] = 0x0d;
			szCmd[19] = 0x0a;
			
			nRetNum = 20;
			nResLen = 20;
		}
		break;		
	// -
	//////////////////////////////////////////////////////////////////////////
	case CHILLER_ST590: //ksj 20200401 : ST590 추가. TEMP880꺼 복사함. 세부 내용 수정 필요.
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';					//ljb 
			szCmd[9] = ',';
			szCmd[10] = '1';				//현재 온도 값 Reg (현재 온도 PV)
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '4';

			// 20090527 KHM E
			WORD pcl1 = 0;

			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;

			nRetNum = 18;
			nResLen = 18;
		}
		break;
	default:break;
	}
	
	return nRetNum;
 }

void COvenCtrl::SetOvenModelType(int nType)
{
	m_nOvenModelType = nType;
}

void COvenCtrl::SetOvenOndoPSFactor(float fFactorValue)
{
	m_fOvenOndoPVFactor = fFactorValue;
}

void COvenCtrl::SetOvenOndoSPFactor(float fFactorValue)//20170907 YULEE SPFactor의 옵션처리 차 추가
{
	m_fOvenOndoSPFactor = fFactorValue;
}

int COvenCtrl::GetOvenModelType()
{
	return m_nOvenModelType;
}


int COvenCtrl::GetReqStrOvenVersionCmd(int nDeviceNumber, char * szCmd, int & nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
		case CHAMBER_TH500:						//Model TH500 
		{
			//Read current   value
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';
			szCmd[3] = '1'+nDeviceNumber;
			szCmd[4] = 'W';
			szCmd[5] = 'H';
			szCmd[6] = 'O';
			szCmd[7] = 0x03;
			szCmd[8] = 0x0d;
			szCmd[9] = 0x0a;

			nRetNum = 10;
			nResLen = 43;
			break;
		}
		case CHAMBER_TEMI880:						//Model TEMI880
		{
			szCmd[0] = 0x02;
			szCmd[1] = '1'+nDeviceNumber;
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 22;
			
			//szCmd[0] = 0x02;
			//szCmd[1] = '1'+nDeviceNumber;
			//szCmd[2] = 'A';
			//szCmd[3] = 'M';
			//szCmd[4] = 'I';
			//szCmd[5] = ',';
			//szCmd[6] = '2';
			//szCmd[7] = ',';
			//szCmd[8] = '0';
			//szCmd[9] = '0';
			//szCmd[10] = '0';
			//szCmd[11] = '1';
			//szCmd[12] = 0x0d;
			//szCmd[13] = 0x0a;
			//
			//nRetNum = 14;
			//nResLen = 22;
			break;	
		}		
		//20090113 KHS TEMP880 추가
		case CHAMBER_TEMP880:						//Model TEMP880
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 22;
			
			//szCmd[0] = 0x02;
			//szCmd[1] = '1'+nDeviceNumber;
			//szCmd[2] = 'A';
			//szCmd[3] = 'M';
			//szCmd[4] = 'I';
			//szCmd[5] = ',';
			//szCmd[6] = '2';
			//szCmd[7] = ',';
			//szCmd[8] = '0';
			//szCmd[9] = '0';
			//szCmd[10] = '0';
			//szCmd[11] = '1';
			//szCmd[12] = 0x0d;
			//szCmd[13] = 0x0a;
			//
			//nRetNum = 14;
			//nResLen = 22;			
			break;	
		}
		case CHAMBER_TEMP2300:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 22;
			
			break;
		}		
		case CHAMBER_TEMI2300:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			//szCmd[6] = ',';
			//szCmd[7] = '0';
			//szCmd[8] = '1';
			//szCmd[9] = ',';
			//szCmd[10] = '0';
			//szCmd[11] = '0';
			//szCmd[12] = '0';
			//szCmd[13] = '1';
			//WORD pcl1 = 0;
			//
			//for(int i = 1 ; i < 14; i++)
			//{
			//	pcl1 += szCmd[i];
			//}
			//
			//
			//sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			//sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[6] = 0x0d;
			szCmd[7] = 0x0a;
			
			nRetNum = 8;
			nResLen = 22;
			break;
		}		
		case CHAMBER_TEMP2500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}
			
			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;
			
			nRetNum = 18;
			nResLen = 22;			
			break;
		}		
		case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			WORD pcl1 = 0;

			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}

			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;

			nRetNum = 18;
			nResLen = 22;			
			break;	
		}		
		case CHAMBER_TEMI2500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			//szCmd[6] = ',';
			//szCmd[7] = '0';
			//szCmd[8] = '1';
			//szCmd[9] = ',';
			//szCmd[10] = '0';
			//szCmd[11] = '0';
			//szCmd[12] = '0';
			//szCmd[13] = '1';
			//WORD pcl1 = 0;
			//
			//for(int i = 1 ; i < 14; i++)
			//{
			//	pcl1 += szCmd[i];
			//}
			//
			//
			//sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			//sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[6] = 0x0d;
			szCmd[7] = 0x0a;
			
			nRetNum = 8;
			nResLen = 22;
			break;			
		}		
		//////////////////////////////////////////////////////////////////////////
		// + BW KIM 2014.02.26
		case CHAMBER_TD500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';		
			szCmd[3] = '1'+nDeviceNumber;
			szCmd[4] = ',';
			szCmd[5] = 'W';
			szCmd[6] = 'H';
			szCmd[7] = 'O';
			szCmd[8] = ',';
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 9; i++)
			{
				pcl1 += szCmd[i];
			}			
			
			sprintf(&szCmd[9], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[10], "%X", pcl1&0x000f);
			szCmd[11] = 0x0d;
			szCmd[12] = 0x0a;
			
			nRetNum = 13;
			nResLen = 43; // ??  String 으로 넘어오는 부분이 부정확함 여기는 꼭 체크
		
			break;
		}
		case CHILLER_ST590: //ksj 20200401 : ST590 추가. TEMP 880꺼 복사함. 세부 내용 수정 필요.
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'A';
			szCmd[4] = 'M';
			szCmd[5] = 'I';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			WORD pcl1 = 0;

			for(int i = 1 ; i < 14; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[14], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[15], "%X", pcl1&0x000f);
			szCmd[16] = 0x0d;
			szCmd[17] = 0x0a;

			nRetNum = 18;
			nResLen = 22;

			//szCmd[0] = 0x02;
			//szCmd[1] = '1'+nDeviceNumber;
			//szCmd[2] = 'A';
			//szCmd[3] = 'M';
			//szCmd[4] = 'I';
			//szCmd[5] = ',';
			//szCmd[6] = '2';
			//szCmd[7] = ',';
			//szCmd[8] = '0';
			//szCmd[9] = '0';
			//szCmd[10] = '0';
			//szCmd[11] = '1';
			//szCmd[12] = 0x0d;
			//szCmd[13] = 0x0a;
			//
			//nRetNum = 14;
			//nResLen = 22;			
			break;	
		}
		//////////////////////////////////////////////////////////////////////////		
		default:break;
	}
	return nRetNum;
}

void COvenCtrl::SetCurrentDataTH500(int nIndex, char szBuffData[])
{
	com_Data	myData;
	float fData;

	//온도 설정값
	myData.bData[1] = szBuffData[0];		//TSV
	myData.bData[0] = szBuffData[1];
	fData = float(myData.nData)/100.0f-100.0f;
	m_OvenData[nIndex].SetRefTemperature(fData);

	//현재 온도값
	myData.bData[1] = szBuffData[2];		//TPV
	myData.bData[0] = szBuffData[3];
	fData = float(myData.nData)/100.0f-100.0f;
	m_OvenData[nIndex].SetCurTemperature(fData);

	//온도 출력치(%)
	myData.bData[1] = szBuffData[4];		//TMV
	myData.bData[0] = szBuffData[5];
	fData = float(myData.nData)/100.0f;
	
	//습도설정값 
	myData.bData[1] = szBuffData[6];		//HSV
	myData.bData[0] = szBuffData[7];
	fData = float(myData.nData)/10.0f;
	m_OvenData[nIndex].SetRefHumidity(fData);

	//현재 습도값
	myData.bData[1] = szBuffData[8];		//HPV
	myData.bData[0] = szBuffData[9];
	fData = float(myData.nData)/10.0f;
	m_OvenData[nIndex].SetCurHumidity(fData);
	
	//습도 출력치(%)
	myData.bData[1] = szBuffData[10];		//HMV
	myData.bData[0] = szBuffData[11];
	fData = float(myData.nData)/100.0f;

	//memcpy(&nData, &szBuffData[23], 2);		//I/S
	//memcpy(&nData, &szBuffData[25], 2);		//T/S
	//memcpy(&nData, &szBuffData[27], 2);		//A/S
	//memcpy(&nData, &szBuffData[29], 2);		//RY
	//memcpy(&nData, &szBuffData[31], 2);		//O/C
	//memcpy(&nData, &szBuffData[33], 2);		//D/I
	
	//Run Mode
	BYTE	runMode = szBuffData[24];
//	m_OvenData[nIndex].SetRunMode(runMode);
	m_OvenData[nIndex].SetRunState(runMode);

	//Run Time
	myData.bData[1] = szBuffData[25];			//RTH
	myData.bData[0] = szBuffData[26];
	WORD	runHour = myData.nData;
	BYTE	runMin = szBuffData[27];
	BYTE	runSec = szBuffData[28];
	m_OvenData[nIndex].SetRunTime(runHour*3600+runMin*60+runSec);

	//Run state : Run Time의 값이 있다면 Run 중이다.
	if(runHour > 0 || runMin > 0 || runSec > 0)
		m_OvenData[nIndex].SetRunState(TRUE);
	else
		m_OvenData[nIndex].SetRunState(FALSE);

	//Segment run time
	myData.bData[1] = szBuffData[29];			//SRTH
	myData.bData[0] = szBuffData[30];
// 	WORD	segRunHour = myData.nData;
// 	BYTE	segRunMin = szBuffData[31];			//SRTM

	//Segment Full run time
	myData.bData[1] = szBuffData[32];			//SFTH
	myData.bData[0] = szBuffData[33];
// 	WORD	segFullRunHour = myData.nData;
// 	BYTE	segFullRunMin = szBuffData[34];		//SFTM

	//Run Pattern number
	myData.bData[1] = szBuffData[35];			//RPTN
	myData.bData[0] = szBuffData[36];
// 	WORD	runPattenNo = myData.nData;
// 	BYTE	segSegNo = szBuffData[37];			//RSEG

	//Run Pattern repeat count
	myData.bData[1] = szBuffData[38];			//RPTN
	myData.bData[0] = szBuffData[39];
//	WORD	runRptCnt = myData.nData;
	myData.bData[1] = szBuffData[40];			//RPRN
	myData.bData[0] = szBuffData[41];
//	WORD	runRptTotCnt = myData.nData;
	
	//Run loop count
// 	BYTE	runLoopCnt = szBuffData[42];		//RLC
// 	BYTE	runLoopTotCnt = szBuffData[43];		//RLN
// 	BYTE	UDSW = szBuffData[44];				//UDSW

	/*
		//온도 설정값
	myData.bData[1] = szBuffData[11];		//TSV
	myData.bData[0] = szBuffData[12];
	fData = float(myData.nData)/100.0f-100.0f;
	m_OvenData[nIndex].SetRefTemperature(fData);

	//현재 온도값
	myData.bData[1] = szBuffData[13];		//TPV
	myData.bData[0] = szBuffData[14];
	fData = float(myData.nData)/100.0f-100.0f;
	m_OvenData[nIndex].SetCurTemperature(fData);

	//온도 출력치(%)
	myData.bData[1] = szBuffData[15];		//TMV
	myData.bData[0] = szBuffData[16];
	fData = float(myData.nData)/100.0f;
	
	//습도설정값 
	myData.bData[1] = szBuffData[17];		//HSV
	myData.bData[0] = szBuffData[18];
	fData = float(myData.nData)/10.0f;
	m_OvenData[nIndex].SetRefHumidity(fData);

	//현재 습도값
	myData.bData[1] = szBuffData[19];		//HPV
	myData.bData[0] = szBuffData[20];
	fData = float(myData.nData)/10.0f;
	m_OvenData[nIndex].SetCurHumidity(fData);
	
	//습도 출력치(%)
	myData.bData[1] = szBuffData[21];		//HMV
	myData.bData[0] = szBuffData[22];
	fData = float(myData.nData)/100.0f;

	//memcpy(&nData, &szBuffData[23], 2);		//I/S
	//memcpy(&nData, &szBuffData[25], 2);		//T/S
	//memcpy(&nData, &szBuffData[27], 2);		//A/S
	//memcpy(&nData, &szBuffData[29], 2);		//RY
	//memcpy(&nData, &szBuffData[31], 2);		//O/C
	//memcpy(&nData, &szBuffData[33], 2);		//D/I
	
	//Run Mode
	BYTE	runMode = szBuffData[35];
	m_OvenData[nIndex].SetRunMode(runMode);
	

	//Run Time
	myData.bData[1] = szBuffData[36];			//RTH
	myData.bData[0] = szBuffData[37];
	WORD	runHour = myData.nData;
	BYTE	runMin = szBuffData[38];
	BYTE	runSec = szBuffData[39];
	m_OvenData[nIndex].SetRunTime(runHour*3600+runMin*60+runSec);

	//Run state : Run Time의 값이 있다면 Run 중이다.
	if(runHour > 0 || runMin > 0 || runSec > 0)
		m_OvenData[nIndex].SetRunState(TRUE);
	else
		m_OvenData[nIndex].SetRunState(FALSE);

	//Segment run time
	myData.bData[1] = szBuffData[40];			//SRTH
	myData.bData[0] = szBuffData[41];
	WORD	segRunHour = myData.nData;
	BYTE	segRunMin = szBuffData[42];			//SRTM

	//Segment Full run time
	myData.bData[1] = szBuffData[43];			//SFTH
	myData.bData[0] = szBuffData[44];
	WORD	segFullRunHour = myData.nData;
	BYTE	segFullRunMin = szBuffData[45];		//SFTM

	//Run Pattern number
	myData.bData[1] = szBuffData[46];			//RPTN
	myData.bData[0] = szBuffData[47];
	WORD	runPattenNo = myData.nData;
	BYTE	segSegNo = szBuffData[48];			//RSEG

	//Run Pattern repeat count
	myData.bData[1] = szBuffData[49];			//RPTN
	myData.bData[0] = szBuffData[50];
	WORD	runRptCnt = myData.nData;
	myData.bData[1] = szBuffData[51];			//RPRN
	myData.bData[0] = szBuffData[52];
	WORD	runRptTotCnt = myData.nData;
	
	//Run loop count
	BYTE	runLoopCnt = szBuffData[53];		//RLC
	BYTE	runLoopTotCnt = szBuffData[54];		//RLN
	BYTE	UDSW = szBuffData[55];				//UDSW
	*/
	//ASSERT(szBuffData[56] == 0x03);			//ETX
	//ASSERT(szBuffData[57] == 0x0d);			//CR
	//ASSERT(szBuffData[58] == 0x0a);			//LF
}

void COvenCtrl::SetCurrentDataTEMI880(int nIndex, char szBuffData[])
{
	com_TEMI880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
	
	m_OvenData[nIndex].SetRefTemperature(fData);

	//현재 습도값
	myData.bData[3] = szBuffData[10];		//습도 SP
	myData.bData[2] = szBuffData[11];
	myData.bData[1] = szBuffData[12];		
	myData.bData[0] = szBuffData[13];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	fData = (float)(StringToHex32(strTemp)/10.0f);
	
	m_OvenData[nIndex].SetCurHumidity(fData);
	
	//설정 습도값
	myData.bData[3] = szBuffData[15];		//습도 SP
	myData.bData[2] = szBuffData[16];
	myData.bData[1] = szBuffData[17];		
	myData.bData[0] = szBuffData[18];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	fData = (float)(StringToHex32(strTemp)/10.0f);
	
	m_OvenData[nIndex].SetRefHumidity(fData);

	//챔버 상태   1: RUN, 2: HOLD, 3:STEP, 4:STOP
	BYTE runMode = szBuffData[22] & 0x01;			//12
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState(TRUE);
	if(runMode ==0)
		m_OvenData[nIndex].SetRunState(FALSE);
	

	runMode = szBuffData[28] & 0x01;					// 0:PROG, 1:FIX
	if(runMode == 1)
		m_OvenData[nIndex].SetRunWorkMode(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetRunWorkMode(0);

	//챔버 상태  (Emergency)
	myData.bData[3] = szBuffData[30];		
	myData.bData[2] = szBuffData[31];
	myData.bData[1] = szBuffData[32];		
	myData.bData[0] = szBuffData[33];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	m_OvenData[nIndex].SetOvenStatus(lData);

	//ljb add Run Time for samsung
	long lTimeHour=0,lTimeMin=0;
	//Run Time
	
	myData.bData[0] = szBuffData[35];		//Run hour
	myData.bData[1] = szBuffData[36];
	myData.bData[2] = szBuffData[37];		
	myData.bData[3] = szBuffData[38];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeHour = StringToHex32(strTemp);
	//	TRACE("RunTime Hour = %ld\n",lData);
	
	myData.bData[0] = szBuffData[40];		//Run hour
	myData.bData[1] = szBuffData[41];
	myData.bData[2] = szBuffData[42];		
	myData.bData[3] = szBuffData[43];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeMin = StringToHex32(strTemp);
	//	TRACE("RunTime Min = %ld\n",lData);
	m_OvenData[nIndex].SetRunningTime(lTimeHour*60+lTimeMin);

	// Pattern Save Done
	runMode = szBuffData[47] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetPatternSaveDone(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetPatternSaveDone(0);

	//PROG 모드에서 현재 SEGMENT 온도 목표값
	myData.bData[3] = szBuffData[50];		//목표 온도 PV
	myData.bData[2] = szBuffData[51];
	myData.bData[1] = szBuffData[52];		
	myData.bData[0] = szBuffData[53];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝

	
	m_OvenData[nIndex].SetProgTemperature(fData);

// 	com_TEMI880Data	myData;
// 	float fData;
// 
// 	//현재 설정값
// 	myData.bData[3] = szBuffData[0];		//온도 PV
// 	myData.bData[2] = szBuffData[1];
// 	myData.bData[1] = szBuffData[2];		
// 	myData.bData[0] = szBuffData[3];
// 	CString strTemp;
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	long lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	
// 	fData = (float)lData/10.0f;
// 	
// 	m_OvenData[nIndex].SetCurTemperature(fData);
// 
// 	//설정 온도값
// 	myData.bData[3] = szBuffData[5];		//온도 SP
// 	myData.bData[2] = szBuffData[6];
// 	myData.bData[1] = szBuffData[7];		
// 	myData.bData[0] = szBuffData[8];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData/10.0f;
// 	
// 	m_OvenData[nIndex].SetRefTemperature(fData);
// 
// 	//현재 습도값
// 	myData.bData[3] = szBuffData[10];		//습도 SP
// 	myData.bData[2] = szBuffData[11];
// 	myData.bData[1] = szBuffData[12];		
// 	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	fData = (float)(StringToHex32(strTemp)/10.0f);
// 	
// 	m_OvenData[nIndex].SetCurHumidity(fData);
// 	
// 	//설정 습도값
// 	myData.bData[3] = szBuffData[15];		//습도 SP
// 	myData.bData[2] = szBuffData[16];
// 	myData.bData[1] = szBuffData[17];		
// 	myData.bData[0] = szBuffData[18];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	fData = (float)(StringToHex32(strTemp)/10.0f);
// 	
// 	m_OvenData[nIndex].SetRefHumidity(fData);
// 
// 	//Run/Stop 모드
// // 	myData.bData[3] = szBuffData[8];	
// // 	myData.bData[2] = szBuffData[9];
// // 	myData.bData[1] = szBuffData[10];		
// // 	myData.bData[0] = szBuffData[11];
// //	BYTE	runMode = myData.bData[0];
// 	BYTE	runMode = szBuffData[22] & 0x01;
// 	if(runMode == 1)
// 		m_OvenData[nIndex].SetRunState(TRUE);
// 	if(runMode ==0)
// 		m_OvenData[nIndex].SetRunState(FALSE);

}

//20090113 KHS TEMP880 추가
void COvenCtrl::SetCurrentDataTEMP880(int nIndex, char szBuffData[])
{
	com_TEMP880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
		
	m_OvenData[nIndex].SetRefTemperature(fData);

	//챔버 상태   1: RUN, 2: HOLD, 3:STEP, 4:STOP
// 	myData.bData[3] = szBuffData[10];		
// 	myData.bData[2] = szBuffData[11];
// 	myData.bData[1] = szBuffData[12];		
// 	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData;		//20090119 KHS 100.0f -> 10.f로 수정
// 	m_OvenData[nIndex].SetRunState((long)fData);

	BYTE runMode = szBuffData[12] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState(TRUE);
	if(runMode ==0)
		m_OvenData[nIndex].SetRunState(FALSE);
	

	runMode = szBuffData[18] & 0x01;					// 0:PROG, 1:FIX
	if(runMode == 1)
		m_OvenData[nIndex].SetRunWorkMode(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetRunWorkMode(0);

	//챔버 상태  (Emergency)
	myData.bData[3] = szBuffData[20];		
	myData.bData[2] = szBuffData[21];
	myData.bData[1] = szBuffData[22];		
	myData.bData[0] = szBuffData[23];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	m_OvenData[nIndex].SetOvenStatus(lData);

	//ljb add Run Time for samsung
	long lTimeHour=0,lTimeMin=0;
	//Run Time
	
	myData.bData[0] = szBuffData[25];		//Run hour
	myData.bData[1] = szBuffData[26];
	myData.bData[2] = szBuffData[27];		
	myData.bData[3] = szBuffData[28];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeHour = StringToHex32(strTemp);
	//	TRACE("RunTime Hour = %ld\n",lData);
	
	myData.bData[0] = szBuffData[30];		//Run hour
	myData.bData[1] = szBuffData[31];
	myData.bData[2] = szBuffData[32];		
	myData.bData[3] = szBuffData[33];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeMin = StringToHex32(strTemp);
	//	TRACE("RunTime Min = %ld\n",lData);
	m_OvenData[nIndex].SetRunningTime(lTimeHour*60+lTimeMin);

	// Pattern Save Done
	runMode = szBuffData[37] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetPatternSaveDone(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetPatternSaveDone(0);

	//PROG 모드에서 현재 SEGMENT 온도 목표값
	myData.bData[3] = szBuffData[40];		//목표 온도 PV
	myData.bData[2] = szBuffData[41];
	myData.bData[1] = szBuffData[42];		
	myData.bData[0] = szBuffData[43];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝
	
	m_OvenData[nIndex].SetProgTemperature(fData);
}

//ksj 20200401 : ST590 추가.
//SP, PV, run상태, auto/man 상태 4가지만 읽어옴.
void COvenCtrl::SetCurrentDataChillerST590(int nIndex, char szBuffData[])
{
	com_TEMP880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	

	fData = (float)lData/m_fOvenOndoPVFactor;
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
		
	m_OvenData[nIndex].SetRefTemperature(fData);

	BYTE runMode = szBuffData[13] & 0x01;
	if(runMode == 0) //0이 런
		m_OvenData[nIndex].SetRunState(TRUE);
	if(runMode == 1) //1이 스탑
		m_OvenData[nIndex].SetRunState(FALSE);
	

	runMode = szBuffData[18] & 0x01;					// 0:auto, 1:man
	if(runMode == 1)
		m_OvenData[nIndex].SetRunWorkMode(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetRunWorkMode(0);
	
}
//lmh 20120524 Temp2500 add
void COvenCtrl::SetCurrentDataTemp2300(int nIndex, char szBuffData[])
{
	com_TEMP880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝 
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
	
	m_OvenData[nIndex].SetRefTemperature(fData);

	//챔버 상태   1: RUN, 2: HOLD, 3:STEP, 4:STOP
	myData.bData[3] = szBuffData[10];		
	myData.bData[2] = szBuffData[11];
	myData.bData[1] = szBuffData[12];		
	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData;		//20090119 KHS 100.0f -> 10.f로 수정
// 	m_OvenData[nIndex].SetRunState((long)fData);

	BYTE runMode = szBuffData[11] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState(TRUE);
	if(runMode ==0)
		m_OvenData[nIndex].SetRunState(FALSE);
	

	runMode = szBuffData[18] & 0x01;					// 0:PROG, 1:FIX
	if(runMode == 1)
		m_OvenData[nIndex].SetRunWorkMode(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetRunWorkMode(0);

	//챔버 상태  (Emergency)
	myData.bData[3] = szBuffData[20];		
	myData.bData[2] = szBuffData[21];
	myData.bData[1] = szBuffData[22];		
	myData.bData[0] = szBuffData[23];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	m_OvenData[nIndex].SetOvenStatus(lData);

	//ljb add Run Time for samsung
	long lTimeHour=0,lTimeMin=0;
	//Run Time
	
	myData.bData[0] = szBuffData[25];		//Run hour
	myData.bData[1] = szBuffData[26];
	myData.bData[2] = szBuffData[27];		
	myData.bData[3] = szBuffData[28];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeHour = StringToHex32(strTemp);
	//	TRACE("RunTime Hour = %ld\n",lData);
	
	myData.bData[0] = szBuffData[30];		//Run hour
	myData.bData[1] = szBuffData[31];
	myData.bData[2] = szBuffData[32];		
	myData.bData[3] = szBuffData[33];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeMin = StringToHex32(strTemp);
	//	TRACE("RunTime Min = %ld\n",lData);
	m_OvenData[nIndex].SetRunningTime(lTimeHour*60+lTimeMin);

	// Pattern Save Done
	runMode = szBuffData[37] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetPatternSaveDone(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetPatternSaveDone(0);

	//PROG 모드에서 현재 SEGMENT 온도 목표값
	myData.bData[3] = szBuffData[40];		//목표 온도 PV
	myData.bData[2] = szBuffData[41];
	myData.bData[1] = szBuffData[42];		
	myData.bData[0] = szBuffData[43];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝
	
	m_OvenData[nIndex].SetProgTemperature(fData);
}
void COvenCtrl::SetCurrentDataTemp2500(int nIndex, char szBuffData[])
{
	com_TEMP880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝


	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
	
	m_OvenData[nIndex].SetRefTemperature(fData);

	//챔버 상태   1: RUN, 2: HOLD, 3:STEP, 4:STOP
// 	myData.bData[3] = szBuffData[10];		
// 	myData.bData[2] = szBuffData[11];
// 	myData.bData[1] = szBuffData[12];		
// 	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData;		//20090119 KHS 100.0f -> 10.f로 수정
// 	m_OvenData[nIndex].SetRunState((long)fData);

	BYTE runMode = szBuffData[13] & 0x01;				
	if(runMode == 1)
		m_OvenData[nIndex].SetRunWorkMode(1);			//Fix 모드
	else
		m_OvenData[nIndex].SetRunWorkMode(0);			//Prog 모드
	

	runMode = szBuffData[18] & 0x01;					//RUN, STOP Info
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState(FALSE);
	else
		m_OvenData[nIndex].SetRunState(TRUE);

	//챔버 상태  (Emergency)
	myData.bData[3] = szBuffData[20];		
	myData.bData[2] = szBuffData[21];
	myData.bData[1] = szBuffData[22];		
	myData.bData[0] = szBuffData[23];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	m_OvenData[nIndex].SetOvenStatus(lData);

	//ljb add Run Time for samsung
	long lTimeHour=0,lTimeMin=0;
	//Run Time
	
	myData.bData[0] = szBuffData[25];		//Run hour
	myData.bData[1] = szBuffData[26];
	myData.bData[2] = szBuffData[27];		
	myData.bData[3] = szBuffData[28];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeHour = StringToHex32(strTemp);
	//	TRACE("RunTime Hour = %ld\n",lData);
	
	myData.bData[0] = szBuffData[30];		//Run hour
	myData.bData[1] = szBuffData[31];
	myData.bData[2] = szBuffData[32];		
	myData.bData[3] = szBuffData[33];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeMin = StringToHex32(strTemp);
	//	TRACE("RunTime Min = %ld\n",lData);
	m_OvenData[nIndex].SetRunningTime(lTimeHour*60+lTimeMin);

	// Pattern Save Done
	runMode = szBuffData[37] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetPatternSaveDone(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetPatternSaveDone(0);

	//PROG 모드에서 현재 SEGMENT 온도 목표값
	myData.bData[3] = szBuffData[40];		//목표 온도 PV
	myData.bData[2] = szBuffData[41];
	myData.bData[1] = szBuffData[42];		
	myData.bData[0] = szBuffData[43];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝
	
	m_OvenData[nIndex].SetProgTemperature(fData);
}
//lmh 20120524 Temp2500 add
void COvenCtrl::SetCurrentDataTemi2300(int nIndex, char szBuffData[])
{
	com_TEMP880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝 
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/100.0f;		//20090119 KHS 100.0f -> 10.f로 수정
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
	
	m_OvenData[nIndex].SetRefTemperature(fData);

	//현재 습도값
	myData.bData[3] = szBuffData[10];		//습도 SP
	myData.bData[2] = szBuffData[11];
	myData.bData[1] = szBuffData[12];		
	myData.bData[0] = szBuffData[13];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	fData = (float)(StringToHex32(strTemp)/10.0f);
	
	m_OvenData[nIndex].SetCurHumidity(fData);
	
	//설정 습도값
	myData.bData[3] = szBuffData[15];		//습도 SP
	myData.bData[2] = szBuffData[16];
	myData.bData[1] = szBuffData[17];		
	myData.bData[0] = szBuffData[18];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	fData = (float)(StringToHex32(strTemp)/10.0f);
	
	m_OvenData[nIndex].SetRefHumidity(fData);
	
	//챔버 상태   1: RUN, 2: HOLD, 3:STEP, 4:STOP
// 	myData.bData[3] = szBuffData[10];		
// 	myData.bData[2] = szBuffData[11];
// 	myData.bData[1] = szBuffData[12];		
// 	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData;		//20090119 KHS 100.0f -> 10.f로 수정
// 	m_OvenData[nIndex].SetRunState((long)fData);

	BYTE runMode = szBuffData[23] & 0x01;			// 0:PROG, 1:FIX
	if(runMode == 0)
		m_OvenData[nIndex].SetRunWorkMode(0);
	else
		m_OvenData[nIndex].SetRunWorkMode(1);
	

	runMode = szBuffData[28] & 0x01;					
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState(FALSE);		//stop
	else
		m_OvenData[nIndex].SetRunState(TRUE);		//run

	//챔버 상태  (Emergency)
	myData.bData[3] = szBuffData[20];		
	myData.bData[2] = szBuffData[21];
	myData.bData[1] = szBuffData[22];		
	myData.bData[0] = szBuffData[23];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	m_OvenData[nIndex].SetOvenStatus(lData);

	//ljb add Run Time for samsung
	long lTimeHour=0,lTimeMin=0;
	//Run Time
	
	myData.bData[0] = szBuffData[25];		//Run hour
	myData.bData[1] = szBuffData[26];
	myData.bData[2] = szBuffData[27];		
	myData.bData[3] = szBuffData[28];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeHour = StringToHex32(strTemp);
	//	TRACE("RunTime Hour = %ld\n",lData);
	
	myData.bData[0] = szBuffData[30];		//Run hour
	myData.bData[1] = szBuffData[31];
	myData.bData[2] = szBuffData[32];		
	myData.bData[3] = szBuffData[33];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeMin = StringToHex32(strTemp);
	//	TRACE("RunTime Min = %ld\n",lData);
	m_OvenData[nIndex].SetRunningTime(lTimeHour*60+lTimeMin);

	// Pattern Save Done
	runMode = szBuffData[37] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetPatternSaveDone(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetPatternSaveDone(0);

	//PROG 모드에서 현재 SEGMENT 온도 목표값
	myData.bData[3] = szBuffData[40];		//목표 온도 PV
	myData.bData[2] = szBuffData[41];
	myData.bData[1] = szBuffData[42];		
	myData.bData[0] = szBuffData[43];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝
	
	m_OvenData[nIndex].SetProgTemperature(fData);
}

void COvenCtrl::SetCurrentDataTemi2500(int nIndex, char szBuffData[])
{
	com_TEMP880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	
	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝 
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정		온도상수
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
	//	fData = (float)lData/100.0f;		//gd_kim 20160707 온도 단위 바꿈  //20090119 KHS 100.0f -> 10.f로 수정

	m_OvenData[nIndex].SetRefTemperature(fData);

	//현재 습도값
	myData.bData[3] = szBuffData[10];		//습도 SP
	myData.bData[2] = szBuffData[11];
	myData.bData[1] = szBuffData[12];		
	myData.bData[0] = szBuffData[13];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	fData = (float)(StringToHex32(strTemp)/10.0f);
	
	m_OvenData[nIndex].SetCurHumidity(fData);
	
	//설정 습도값
	myData.bData[3] = szBuffData[15];		//습도 SP
	myData.bData[2] = szBuffData[16];
	myData.bData[1] = szBuffData[17];		
	myData.bData[0] = szBuffData[18];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	fData = (float)(StringToHex32(strTemp)/10.0f);
	
	m_OvenData[nIndex].SetRefHumidity(fData);
	
	//챔버 상태   1: RUN, 2: HOLD, 3:STEP, 4:STOP
// 	myData.bData[3] = szBuffData[10];		
// 	myData.bData[2] = szBuffData[11];
// 	myData.bData[1] = szBuffData[12];		
// 	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData;		//20090119 KHS 100.0f -> 10.f로 수정
// 	m_OvenData[nIndex].SetRunState((long)fData);

	BYTE runMode = szBuffData[23] & 0x01;			// 0:PROG, 1:FIX
	if(runMode == 0)
		m_OvenData[nIndex].SetRunWorkMode(0);
	else
		m_OvenData[nIndex].SetRunWorkMode(1);
	

	runMode = szBuffData[28] & 0x01;					
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState(FALSE);		//stop
	else
		m_OvenData[nIndex].SetRunState(TRUE);		//run

	//챔버 상태  (Emergency)
	myData.bData[3] = szBuffData[20];		
	myData.bData[2] = szBuffData[21];
	myData.bData[1] = szBuffData[22];		
	myData.bData[0] = szBuffData[23];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	m_OvenData[nIndex].SetOvenStatus(lData);

	//ljb add Run Time for samsung
	long lTimeHour=0,lTimeMin=0;
	//Run Time
	
	myData.bData[0] = szBuffData[25];		//Run hour
	myData.bData[1] = szBuffData[26];
	myData.bData[2] = szBuffData[27];		
	myData.bData[3] = szBuffData[28];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeHour = StringToHex32(strTemp);
	//	TRACE("RunTime Hour = %ld\n",lData);
	
	myData.bData[0] = szBuffData[30];		//Run hour
	myData.bData[1] = szBuffData[31];
	myData.bData[2] = szBuffData[32];		
	myData.bData[3] = szBuffData[33];
	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
	lTimeMin = StringToHex32(strTemp);
	//	TRACE("RunTime Min = %ld\n",lData);
	m_OvenData[nIndex].SetRunningTime(lTimeHour*60+lTimeMin);

	// Pattern Save Done
	runMode = szBuffData[37] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetPatternSaveDone(1);
	if(runMode ==0)
		m_OvenData[nIndex].SetPatternSaveDone(0);

	//PROG 모드에서 현재 SEGMENT 온도 목표값
	myData.bData[3] = szBuffData[40];		//목표 온도 PV
	myData.bData[2] = szBuffData[41];
	myData.bData[1] = szBuffData[42];		
	myData.bData[0] = szBuffData[43];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
		/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	//20170525 YULEE 수정 끝 
	
	m_OvenData[nIndex].SetProgTemperature(fData);
}

void COvenCtrl::SetCurrentDataNEX(int nIndex, float CurTempCh1, float SetTempCh1, int OPStateCh1, int OPModeCh1,
	float CurTempCh2, float SetTempCh2, int OPStateCh2, int OPModeCh2,
	float CurTempCh3, float SetTempCh3, int OPStateCh3, int OPModeCh3,
	float CurHumi, float SetHumi, //yulee 20190614_*
												float CurPump, float SetPump, int OPStatePump, int OPModePump)
{
	int strModel = GetOvenModelType();
	/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
	{
		CString strLog;
		strLog.Format("SW Filtering : Port:%d Model:%d Ref:%f,%f Cur:%f,%f n9", m_nOvenPortNum, strModel,SetTempCh1,SetTempCh2,CurTempCh1,CurTempCh2);
		GLog::log_All("chiller","debug",strLog);
	}

// 	if(m_nOvenPortNum == 0)
// 	{
// 		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
// 		{
// 			CString strLog;
// 			strLog.Format("SW Filtering : Port:%d", m_nOvenPortNum);
// 			GLog::log_All("chiller","debug",strLog);
// 		}
// 		return;
// 	}
	/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////

	if(GetOvenModelType() == CHAMBER_MODBUS_NEX1000)
	{
		//현재 온도에 대한 Software filtering 
		//온도 값을 float 배열에 3개 데이터를 넣고 
		//값이 들어 올 때 배열의 3개의 평균-들어온 값이 그대로 평균 값이면 
		//가장 최근의 값을 현재 온도로 사용한다. 
		//이유는 통신 끊김으로 간간히 0이 나오는 곳이 있음(LG CWA 500V 장비에서 발생)

		float fltSumCurTemp = 0.0, fltAvgCurTemp = 0.0, fltChkSub = 0.0;

		if(m_nCurTempValueCnt < MAX_FILO_SIZE) //처음에 FILO를 채우는 if문 첫 데이터로 MAX_FILO_SIZE 만큼 채운다.
		{
			if(m_nCurTempValueCnt == 0)
			{
				for(int i=0; i<MAX_FILO_SIZE; i++)
				{
					m_fltCurTemp[i] = CurTempCh1;
					m_nCurTempValueCnt++;
				}
			}
		}
		else//FILO에 MAX_FILO_SIZE 개수 만큼 채워지면
		{
			for(int i=0; i<MAX_FILO_SIZE; i++)
			{
				fltSumCurTemp += m_fltCurTemp[i];
			}
			fltAvgCurTemp = fltSumCurTemp/MAX_FILO_SIZE;//평균 값을 구하고 
			fltSumCurTemp = 0.0;//초기화
			if(fltAvgCurTemp == 0 || fltSumCurTemp == 0)
			{
				m_nCurTempValueCnt = 0;
			}
			else
			{
				fltChkSub = fltAvgCurTemp - CurTempCh1;	//평균 값이랑 빼서 비교했을 때 뺀 값이 다시 원래의 평균 값이면 들어온 값은 0이므로 가장 나중의 값으로 현재 온도 값을 대체
				if(fltChkSub == fltAvgCurTemp)
				{
					CurTempCh1 = m_fltCurTemp[MAX_FILO_SIZE-1];
				}

				for(int i=0; i<MAX_FILO_SIZE; i++) //배열의 값을 최신화 [n+1]의 값을 [n] 저장 배열 마지막 값은 새로운 값으로 저장
				{
					if(i+1 < MAX_FILO_SIZE)
						m_fltCurTemp[i] = m_fltCurTemp[i+1];
					else if(i+1 == MAX_FILO_SIZE)
					{
						m_fltCurTemp[i] = CurTempCh1;
					}
				}
			}
		}




		// 운전 관련 상태정보	
		//0 PROGRAM RUN(프로그램 운전)5
		//1 FIX RUN 정치 운전  
		//2 HOLD 일시정지  
		//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
		//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
		//7 RESET 정지상태 

		m_OvenData[nIndex].SetCurTemperature(CurTempCh1); //현재 온도
		m_OvenData[nIndex].SetRefTemperature(SetTempCh1); //설정 온도

		if(OPStateCh1 != -2) 
		{
			// 		if(OPStateCh1 == 0||OPStateCh1 == 1|| OPStateCh1 == 2||OPStateCh1 == 7)			 //운전 상태
			// 		{
			// 			m_OvenData[nIndex].SetRunState(OPStateCh1);
			// 		}

			if((OPStateCh1 == 1)||(OPStateCh1 == 2))		 //운전 상태
			{
				m_OvenData[nIndex].SetRunState(1);
			}
			else
			{
				m_OvenData[nIndex].SetRunState(0);
			}

		}
		if(OPModeCh1 != -2)		//Fix(정치)/프로그램(패턴)	
		{
			if(OPModeCh1 == 1)
				m_OvenData[nIndex].SetRunWorkMode(1);
			else
				m_OvenData[nIndex].SetRunWorkMode(0);			
		}
	}
	else if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100)
	{
		//현재 온도에 대한 Software filtering 
		//온도 값을 float 배열에 3개 데이터를 넣고 
		//값이 들어 올 때 배열의 3개의 평균-들어온 값이 그대로 평균 값이면 
		//가장 최근의 값을 현재 온도로 사용한다. 
		//이유는 통신 끊김으로 간간히 0이 나오는 곳이 있음(LG CWA 500V 장비에서 발생)


		float fltSumCurTemp = 0.0, fltAvgCurTemp = 0.0, fltChkSub = 0.0;


		if(m_nCurTempValueCnt < MAX_FILO_SIZE) //처음에 FILO를 채우는 if문 첫 데이터로 MAX_FILO_SIZE 만큼 채운다.
		{
			if(m_nCurTempValueCnt == 0)
			{
				for(int i=0; i<MAX_FILO_SIZE; i++)
				{
					m_fltCurTemp[i] = CurTempCh2;
					m_nCurTempValueCnt++;
				}
			}
		}
		else//FILO에 MAX_FILO_SIZE 개수 만큼 채워지면
		{
			for(int i=0; i<MAX_FILO_SIZE; i++)
			{
				fltSumCurTemp += m_fltCurTemp[i];
			}
			fltAvgCurTemp = fltSumCurTemp/MAX_FILO_SIZE;//평균 값을 구하고 
			fltSumCurTemp = 0.0;//초기화
			if(fltAvgCurTemp == 0 || fltSumCurTemp == 0)
			{
				m_nCurTempValueCnt = 0;
			}
			else
			{

				fltChkSub = fltAvgCurTemp - CurTempCh2;	//평균 값이랑 빼서 비교했을 때 뺀 값이 다시 원래의 평균 값이면 들어온 값은 0이므로 가장 나중의 값으로 현재 온도 값을 대체
				if(fltChkSub == fltAvgCurTemp)
				{
					CurTempCh2 = m_fltCurTemp[MAX_FILO_SIZE-1];
				}

				for(int i=0; i<MAX_FILO_SIZE; i++) //배열의 값을 최신화 [n+1]의 값을 [n] 저장 배열 마지막 값은 새로운 값으로 저장
				{
					if(i+1 < MAX_FILO_SIZE)
						m_fltCurTemp[i] = m_fltCurTemp[i+1];
					else if(i+1 == MAX_FILO_SIZE)
					{
						m_fltCurTemp[i] = CurTempCh2;
					}
				}
			}
		}
		// 운전 관련 상태정보	
		//0 PROGRAM RUN(프로그램 운전)5
		//1 FIX RUN 정치 운전  
		//2 HOLD 일시정지  
		//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
		//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
		//7 RESET 정지상태 

		/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
		{
			CString strLog;
			strLog.Format("SW Filtering : Port:%d Model:%d Ref:%f Cur:%f n7", m_nOvenPortNum, strModel,SetTempCh2,CurTempCh2);
			GLog::log_All("chiller","debug",strLog);
		}
		/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////


		m_OvenData[nIndex].SetCurTemperature(CurTempCh2); //현재 온도
		m_OvenData[nIndex].SetRefTemperature(SetTempCh2); //설정 온도

		if(OPStateCh2 != -2) 
		{
			// 		if(OPStateCh1 == 0||OPStateCh1 == 1|| OPStateCh1 == 2||OPStateCh1 == 7)			 //운전 상태
			// 		{
			// 			m_OvenData[nIndex].SetRunState(OPStateCh1);
			// 		}

			if((OPStateCh2 == 1)||(OPStateCh2 == 2))		 //운전 상태
			{
				m_OvenData[nIndex].SetRunState(1);
			}
			else
			{
				m_OvenData[nIndex].SetRunState(0);
			}

		}
		if(OPModeCh2 != -2)		//Fix(정치)/프로그램(패턴)	
		{
			if(OPModeCh2 == 1)
				m_OvenData[nIndex].SetRunWorkMode(1);
			else
				m_OvenData[nIndex].SetRunWorkMode(0);			
		}

		m_PumpState.nSP = SetPump;							//현재 펌프 L/M //yulee 20190613
		m_PumpState.nPV = CurPump;							//설정 펌프 L/M //yulee	20190613
		m_PumpState.fSP = SetPump;							//lyj 20201105 NEX칠러 소숫점
		m_PumpState.fPV = CurPump;							//lyj 20201105 NEX칠러 소숫점

		if(OPModePump != -2)
		{
			//		if(OPModePump == 1)
			m_PumpState.nPlay = ~OPModePump;								//펌프 워크 모드			
			// 		else //동작 중 튜닝 상태 //우선 동작으로 처리
			// 			m_OvenData[nIndex].SetRunWorkMode(1);
		}
	}
	else if(GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
	{
		//현재 온도에 대한 Software filtering 
		//온도 값을 float 배열에 3개 데이터를 넣고 
		//값이 들어 올 때 배열의 3개의 평균-들어온 값이 그대로 평균 값이면 
		//가장 최근의 값을 현재 온도로 사용한다. 
		//이유는 통신 끊김으로 간간히 0이 나오는 곳이 있음(LG CWA 500V 장비에서 발생)
		
		float fltSumCurTemp = 0.0, fltAvgCurTemp = 0.0, fltChkSub = 0.0;


		if(m_nCurTempValueCnt < MAX_FILO_SIZE) //처음에 FILO를 채우는 if문 첫 데이터로 MAX_FILO_SIZE 만큼 채운다.
		{
			if(m_nCurTempValueCnt == 0)
			{
				for(int i=0; i<MAX_FILO_SIZE; i++)
				{
					m_fltCurTemp[i] = CurTempCh3;
					m_nCurTempValueCnt++;
				}
			}
		}
		else//FILO에 MAX_FILO_SIZE 개수 만큼 채워지면
		{
			for(int i=0; i<MAX_FILO_SIZE; i++)
			{
				fltSumCurTemp += m_fltCurTemp[i];
			}
			fltAvgCurTemp = fltSumCurTemp/MAX_FILO_SIZE;//평균 값을 구하고 
			fltSumCurTemp = 0.0;//초기화

			if(fltAvgCurTemp == 0 || fltSumCurTemp == 0)
			{
				m_nCurTempValueCnt = 0;
			}
			else
			{
				fltChkSub = fltAvgCurTemp - CurTempCh3;	//평균 값이랑 빼서 비교했을 때 뺀 값이 다시 원래의 평균 값이면 들어온 값은 0이므로 가장 나중의 값으로 현재 온도 값을 대체
				if(fltChkSub == fltAvgCurTemp)
				{
					CurTempCh3 = m_fltCurTemp[MAX_FILO_SIZE-1];
				}

				for(int i=0; i<MAX_FILO_SIZE; i++) //배열의 값을 최신화 [n+1]의 값을 [n] 저장 배열 마지막 값은 새로운 값으로 저장
				{
					if(i+1 < MAX_FILO_SIZE)
						m_fltCurTemp[i] = m_fltCurTemp[i+1];
					else if(i+1 == MAX_FILO_SIZE)
					{
						m_fltCurTemp[i] = CurTempCh3;
					}
				}
			}
		}



		
		// 운전 관련 상태정보	
		//0 PROGRAM RUN(프로그램 운전)5
		//1 FIX RUN 정치 운전  
		//2 HOLD 일시정지  
		//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
		//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
		//7 RESET 정지상태 

		m_OvenData[nIndex].SetCurTemperature(CurTempCh3); //현재 온도
		m_OvenData[nIndex].SetRefTemperature(SetTempCh3); //설정 온도

		if(OPStateCh3 != -2) 
		{
			// 		if(OPStateCh1 == 0||OPStateCh1 == 1|| OPStateCh1 == 2||OPStateCh1 == 7)			 //운전 상태
			// 		{
			// 			m_OvenData[nIndex].SetRunState(OPStateCh1);
			// 		}

			if((OPStateCh3 == 1)||(OPStateCh3 == 2))		 //운전 상태
			{
				m_OvenData[nIndex].SetRunState(1);
			}
			else
			{
				m_OvenData[nIndex].SetRunState(0);
			}

		}
		if(OPModeCh3 != -2)		//Fix(정치)/프로그램(패턴)	
		{
			if(OPModeCh3 == 1)
				m_OvenData[nIndex].SetRunWorkMode(1);
			else
				m_OvenData[nIndex].SetRunWorkMode(0);			
		}


		m_OvenData[nIndex].SetCurHumidity(CurHumi);		//현재 습도
		m_OvenData[nIndex].SetRefHumidity(SetHumi);		//설정 습도
	}

	/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////


// 	//현재 온도에 대한 Software filtering 
// 	//온도 값을 float 배열에 3개 데이터를 넣고 
// 	//값이 들어 올 때 배열의 3개의 평균-들어온 값이 그대로 평균 값이면 
// 	//가장 최근의 값을 현재 온도로 사용한다. 
// 	//이유는 통신 끊김으로 간간히 0이 나오는 곳이 있음(LG CWA 500V 장비에서 발생)
// 
// 
// 	float fltSumCurTemp = 0.0, fltAvgCurTemp = 0.0, fltChkSub = 0.0;
// 
// 
// 	if(m_nCurTempValueCnt < MAX_FILO_SIZE) //처음에 FILO를 채우는 if문 첫 데이터로 MAX_FILO_SIZE 만큼 채운다.
// 	{
// 		for(int i=0; i<MAX_FILO_SIZE; i++)
// 		{
// 			m_fltCurTemp[i] = CurTempCh1;
// 			m_nCurTempValueCnt++;
// 		}
// 
// 		if(m_nCurTempValueCnt > 1)
// 		{
// 			fltChkSub = m_fltCurTemp[0] - CurTempCh1;
// 			if(fltChkSub == m_fltCurTemp[0])
// 			{
// 				CurTempCh1 = m_fltCurTemp[0];
// 			}
// 		}
// 	}
// 	else//FILO에 MAX_FILO_SIZE 개수 만큼 채워지면
// 	{
// 		for(int i=0; i<MAX_FILO_SIZE; i++)
// 		{
// 			fltSumCurTemp += m_fltCurTemp[i];
// 		}
// 		fltAvgCurTemp = fltSumCurTemp/MAX_FILO_SIZE;//평균 값을 구하고 
// 		fltSumCurTemp = 0.0;//초기화
// 
// 		fltChkSub = fltAvgCurTemp - CurTempCh1;	//평균 값이랑 빼서 비교했을 때 뺀 값이 다시 원래의 평균 값이면 들어온 값은 0이므로 가장 나중의 값으로 현재 온도 값을 대체
// 		if(fltChkSub == fltAvgCurTemp)
// 		{
// 			CurTempCh1 = m_fltCurTemp[MAX_FILO_SIZE-1];
// 		}
// 
// 		for(int i=0; i<MAX_FILO_SIZE; i++) //배열의 값을 최신화 [n+1]의 값을 [n] 저장 배열 마지막 값은 새로운 값으로 저장
// 		{
// 			if(i+1 < MAX_FILO_SIZE)
// 				m_fltCurTemp[i] = m_fltCurTemp[i+1];
// 			else if(i+1 == MAX_FILO_SIZE)
// 			{
// 				m_fltCurTemp[i] = CurTempCh1;
// 			}
// 		}
// 	}
// 
// 
// 
// 
// 	// 운전 관련 상태정보	
// 	//0 PROGRAM RUN(프로그램 운전)5
// 	//1 FIX RUN 정치 운전  
// 	//2 HOLD 일시정지  
// 	//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
// 	//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
// 	//7 RESET 정지상태 
// 
// 	m_OvenData[nIndex].SetCurTemperature(CurTempCh1); //현재 온도
// 	m_OvenData[nIndex].SetRefTemperature(SetTempCh1); //설정 온도
// 
// 	if(OPStateCh1 != -2) 
// 	{
// // 		if(OPStateCh1 == 0||OPStateCh1 == 1|| OPStateCh1 == 2||OPStateCh1 == 7)			 //운전 상태
// // 		{
// // 			m_OvenData[nIndex].SetRunState(OPStateCh1);
// // 		}
// 
// 		if((OPStateCh1 == 1)||(OPStateCh1 == 2))		 //운전 상태
// 		{
// 			m_OvenData[nIndex].SetRunState(1);
// 		}
// 		else
// 		{
// 			m_OvenData[nIndex].SetRunState(0);
// 		}
// 
// 	}
// 	if(OPModeCh1 != -2)		//Fix(정치)/프로그램(패턴)	
// 	{
// 		if(OPModeCh1 == 1)
// 			m_OvenData[nIndex].SetRunWorkMode(1);
// 		else
// 			m_OvenData[nIndex].SetRunWorkMode(0);			
// 	}
// 
// 
// 	m_OvenData[nIndex].SetCurHumidity(CurHumi);		//현재 습도
// 	m_OvenData[nIndex].SetRefHumidity(SetHumi);		//설정 습도
// 	
// 	m_PumpState.nSP = SetPump;							//현재 펌프 L/M //yulee 20190613
// 	m_PumpState.nPV = CurPump;							//설정 펌프 L/M //yulee	20190613
// 	
// 
// 	if(OPModePump != -2)
// 	{
// //		if(OPModePump == 1)
// 			m_PumpState.nPlay = ~OPModePump;								//펌프 워크 모드			
// // 		else //동작 중 튜닝 상태 //우선 동작으로 처리
// // 			m_OvenData[nIndex].SetRunWorkMode(1);
// 	}

/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////

}


void COvenCtrl::SetCurrentDataChillerTEMP2520(int nIndex, char szBuffData[])
{
	com_TEMP2520Data	myData;
	long				lData = 0;
	float				fData = 0.0f;
	CString				strTemp = _T("");
	
	//Ch 1 현재 온도 값
	myData.bData[3] = szBuffData[0];		//Ch 1 현재 온도 PV //Part1
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];
	myData.bData[0] = szBuffData[3];
	strTemp.Format("%c%c%c%c", myData.bData[3], myData.bData[2], myData.bData[1], myData.bData[0]);
	
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	fData = (float)lData/10.0f;
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//Ch 2 현재 펌프 값
	myData.bData[3] = szBuffData[5];		//Ch 2 현재 펌프 PV //Part2
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3], myData.bData[2], myData.bData[1], myData.bData[0]);
	
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	fData = (float)lData/10.0f;
	
	m_OvenData[nIndex].SetCurTemperatureCh2(fData);
	m_PumpState.nPV = (int)fData;
	m_PumpState.fPV = fData; //ksj 20200706
	
	//Ch 1 설정 온도값
	myData.bData[3] = szBuffData[10];		//Ch 1 설정 온도 SP //Part3
	myData.bData[2] = szBuffData[11];
	myData.bData[1] = szBuffData[12];
	myData.bData[0] = szBuffData[13];
	strTemp.Format("%c%c%c%c", myData.bData[3], myData.bData[2], myData.bData[1], myData.bData[0]);
	
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	fData = (float)lData/10.0f;
	
	m_OvenData[nIndex].SetRefTemperature(fData);
	
	//Ch 2 설정 펌프값
	myData.bData[3] = szBuffData[15];		//Ch 2 설정 펌프 SP //Part4
	myData.bData[2] = szBuffData[16];
	myData.bData[1] = szBuffData[17];
	myData.bData[0] = szBuffData[18];
	strTemp.Format("%c%c%c%c", myData.bData[3], myData.bData[2], myData.bData[1], myData.bData[0]);
	
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	fData = (float)lData/10.0f;
	
	m_OvenData[nIndex].SetRefTemperatureCh2(fData);	
	m_PumpState.nSP = (int)fData;
	m_PumpState.fSP = fData; //ksj 20200706
	
	TRACE("Ch 1 동작상태 : %c%c%c%c(%s)\n", szBuffData[20], szBuffData[21], szBuffData[22], szBuffData[23], szBuffData);
	TRACE("Ch 2 동작상태 : %c%c%c%c(%s)\n", szBuffData[25], szBuffData[26], szBuffData[27], szBuffData[28], szBuffData);
	TRACE("에러확인 : %c%c%c%c\n", szBuffData[15], szBuffData[16], szBuffData[17], szBuffData[18]);
	
	//Ch1 Fix/Prog Mode
	BYTE runMode; //fix prog mode set //yulee 20190207
	
	runMode= szBuffData[23] & 0x01;			//Ch1 Run : 1, Stop : 0				
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState(FALSE);		//stop
	else
		m_OvenData[nIndex].SetRunState(TRUE);		//run

	runMode= szBuffData[28] & 0x01;			//Ch2 Run : 1, Stop : 0
	if(runMode == 1)
		m_OvenData[nIndex].SetRunState2(FALSE);		//stop
	else
		m_OvenData[nIndex].SetRunState2(TRUE);		//run
	
	//BYTE 
	runMode = szBuffData[33] & 0x01;		//Ch1 Fix : 1, Prog : 0
	if(runMode == 1)
		m_OvenData[nIndex].SetRunWorkMode(TRUE);
	if(runMode == 0)
		m_OvenData[nIndex].SetRunWorkMode(FALSE);

	//BYTE 
	runMode = szBuffData[38] & 0x01;
	if(runMode == 1)
		m_OvenData[nIndex].SetRunWorkMode2(TRUE);
	if(runMode == 0)
		m_OvenData[nIndex].SetRunWorkMode2(FALSE);

	TRACE("Ch 1 동작모드상태(fix/prog) : %c%c%c%c(%s)\n", szBuffData[30], szBuffData[31], szBuffData[32], szBuffData[33], szBuffData);
	TRACE("Ch 2 동작모드상태(fix/prog) : %c%c%c%c(%s)\n", szBuffData[35], szBuffData[36], szBuffData[37], szBuffData[38], szBuffData);
}

void COvenCtrl::SetCurrentDataTD500(int nIndex, char szBuffData[])
{
	com_TEMP880Data	myData;
	float fData; 
	
	//현재 설정값
	myData.bData[3] = szBuffData[0];		//현재 온도 PV
	myData.bData[2] = szBuffData[1];
	myData.bData[1] = szBuffData[2];		
	myData.bData[0] = szBuffData[3];
	CString strTemp;
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	long lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;	
	/* 20170524 YULEE 수정 차 주석처리 시작
	//20090119 KHS 100.0f -> 10.f로 수정
	fData = (float)lData/10.0f;				//온도상수
	//fData = (float)lData/100.0f; //gd_kim 20160707 온도 단위 바꿈
	 20170524 YULEE 수정 차 주석처리 끝 */

	//20170524 YULEE 수정 시작
	/*if(nIndex == 1)
		fData = (float)lData/OndoFactor1;
	else if(nIndex == 2)
		fData = (float)lData/OndoFactor2;*/
	//20170524 YULEE 수정 끝 

	//20170525 YULEE 수정 시작
	fData = (float)lData/m_fOvenOndoPVFactor;
	
	m_OvenData[nIndex].SetCurTemperature(fData);
	
	//설정 온도값
	myData.bData[3] = szBuffData[5];		//현재 온도 SP
	myData.bData[2] = szBuffData[6];
	myData.bData[1] = szBuffData[7];		
	myData.bData[0] = szBuffData[8];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
	
	m_OvenData[nIndex].SetRefTemperature(fData);

// 	//현재 습도값
// 	myData.bData[3] = szBuffData[10];		//현재 습도 SP
// 	myData.bData[2] = szBuffData[11];
// 	myData.bData[1] = szBuffData[12];		
// 	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
	
	m_OvenData[nIndex].SetCurHumidity(0);

	//설정 습도값
// 	myData.bData[3] = szBuffData[15];		//현재 온도 SP
// 	myData.bData[2] = szBuffData[16];
// 	myData.bData[1] = szBuffData[17];		
// 	myData.bData[0] = szBuffData[18];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;
// 	fData = (float)lData/10.0f;		//20090119 KHS 100.0f -> 10.f로 수정
	
	m_OvenData[nIndex].SetRefHumidity(0);

	//챔버 상태   1: RUN, 2: HOLD, 3:STEP, 4:STOP
	myData.bData[3] = szBuffData[10];		
	myData.bData[2] = szBuffData[11];
	myData.bData[1] = szBuffData[12];		
	myData.bData[0] = szBuffData[13];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
	if(lData > 0x7FFF)	lData -= 0x10000;
	//fData = (float)lData;		// 
    fData = (float)lData/m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가 
	m_OvenData[nIndex].SetRunState((long)fData);

// 	myData.bData[3] = szBuffData[20];		//현재 온도 SP
// 	myData.bData[2] = szBuffData[21];
// 	myData.bData[1] = szBuffData[22];		
// 	myData.bData[0] = szBuffData[23];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
//	BYTE runMode;
//	BYTE runMode = szBuffData[20] & 0x01;
// 	if(runMode == 1)
// 		m_OvenData[nIndex].SetRunState(TRUE);
// 	if(runMode == 1)
// 		m_OvenData[nIndex].SetRunState(FALSE);
// 	else
// 		m_OvenData[nIndex].SetRunState(TRUE);
	

	myData.bData[3] = szBuffData[15];		//현재 Fix, Prgo
	myData.bData[2] = szBuffData[16];
	myData.bData[1] = szBuffData[17];		
	myData.bData[0] = szBuffData[18];
	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
	lData = StringToHex32(strTemp);
//	runMode = szBuffData[18] & 0x01;					// 0:PROG, 1:FIX
	if(lData == 1)
		m_OvenData[nIndex].SetRunWorkMode(0);	//Fix
	if(lData == 0)
		m_OvenData[nIndex].SetRunWorkMode(1);	//prog
	m_OvenData[nIndex].SetRunWorkMode(1);	//Fix


	//챔버 상태  (Emergency)
// 	myData.bData[3] = szBuffData[10];		
// 	myData.bData[2] = szBuffData[11];
// 	myData.bData[1] = szBuffData[12];		
// 	myData.bData[0] = szBuffData[13];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	m_OvenData[nIndex].SetOvenStatus(lData);

	//ljb add Run Time for samsung
//	long lTimeHour=0,lTimeMin=0;
	//Run Time
// 	
// 	myData.bData[0] = szBuffData[25];		//Run hour
// 	myData.bData[1] = szBuffData[26];
// 	myData.bData[2] = szBuffData[27];		
// 	myData.bData[3] = szBuffData[28];
// 	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
// 	lTimeHour = StringToHex32(strTemp);
// 	//	TRACE("RunTime Hour = %ld\n",lData);
// 	
// 	myData.bData[0] = szBuffData[30];		//Run hour
// 	myData.bData[1] = szBuffData[31];
// 	myData.bData[2] = szBuffData[32];		
// 	myData.bData[3] = szBuffData[33];
// 	strTemp.Format("%c%c%c%c", myData.bData[0],myData.bData[1],myData.bData[2],myData.bData[3]);
// 	lTimeMin = StringToHex32(strTemp);
// 	//	TRACE("RunTime Min = %ld\n",lData);
// 	m_OvenData[nIndex].SetRunningTime(lTimeHour*60+lTimeMin);

	// Pattern Save Done
// 	runMode = szBuffData[37] & 0x01;
// 	if(runMode == 1)
// 		m_OvenData[nIndex].SetPatternSaveDone(1);
// 	if(runMode ==0)
// 		m_OvenData[nIndex].SetPatternSaveDone(0);
// 
// 	//PROG 모드에서 현재 SEGMENT 온도 목표값
// 	myData.bData[3] = szBuffData[40];		//목표 온도 PV
// 	myData.bData[2] = szBuffData[41];
// 	myData.bData[1] = szBuffData[42];		
// 	myData.bData[0] = szBuffData[43];
// 	strTemp.Format("%c%c%c%c", myData.bData[3],myData.bData[2],myData.bData[1],myData.bData[0]);
// 	lData = StringToHex32(strTemp);
// 	if(lData > 0x7FFF)	lData -= 0x10000;	
// 	//20090119 KHS 100.0f -> 10.f로 수정
// 	fData = (float)lData/10.0f;
// 	
// 	m_OvenData[nIndex].SetProgTemperature(fData);
}

void COvenCtrl::InsertRxChar(char data, int nOvIndex)
{
	if(m_nRxBuffCnt == 0 && data != 0x02)
	{
		TRACE("Cnt : %d Data : %x\r\n", m_nRxBuffCnt, data);
		m_nRxBuffCnt = 0;
		return;
	}
	
	//ksj 20200723 : debug
	if(m_nRxBuffCnt > 190)
	{
		CString strLog;
		strLog.Format("m_nRxBuffCnt [%d]", m_nRxBuffCnt);
		GLog::log_All("debug","debug",strLog);	
	}				
	//ksj end

	m_szRxBuff[m_nRxBuffCnt++] = data;

	if(m_nRxBuffCnt > 200)
	{
		m_nRxBuffCnt = 0;
	}	


	if(m_nOvenModelType == CHAMBER_TH500 && m_nRxBuffCnt >= 7)	//Model Type TH500
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 3);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;
		

		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[4], 3);
		strCmd = szBuff;

	//	CString stess;
	//	stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d", strCmd, m_nRxBuffCnt, nOvenIndex);
	//	pDoc->WriteSysLog(stess);

		if(strCmd == "RCV" && m_nRxBuffCnt >= 59 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{				
	//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[7], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], 59-11);
					SetCurrentData(nOvenIndex, szBuff);
				}	
				m_nRxBuffCnt = 0;
			}
		}
		
		if(strCmd == "WHO" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);

				memcpy(szBuff, &m_szRxBuff[7], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);

					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
			
				
			}
			
		}

		if((strCmd == "WRP"||strCmd == "WUP")&& m_nRxBuffCnt > 17 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
			
			if(data == 0x0a)
			{				
				memcpy(szBuff, &m_szRxBuff[7], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	
	}
	else if(m_nOvenModelType == CHAMBER_TEMI880 && m_nRxBuffCnt >= 6)	//Model Type TEMI880
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;
		

		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;

		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
	//	TRACE(stess);
	//	pDoc->WriteSysLog(stess);

		
		if(strCmd == "RRD" && m_nRxBuffCnt >= 38 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			
			if(data == 0x0a)
			{
//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);
					
				}		
				m_nRxBuffCnt = 0;
			}
			
			
		}
		
		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "AMI" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);

				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);

					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
				
			}
		}

		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}

		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);

			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	}

	//20090113 KHS TMP880 추가
	else if(m_nOvenModelType == CHAMBER_TEMP880 && m_nRxBuffCnt >= 6)	//Model Type TEMP880
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;
		
		
		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;
		
		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//	pDoc->WriteSysLog(stess);
		
		
		if(strCmd == "RRD" && m_nRxBuffCnt >= 27 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			
			if(data == 0x0a)
			{
//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);
					
				}		
				m_nRxBuffCnt = 0;
			}
			
			
		}
		
		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "AMI" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);
					
					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
				
			}
		}
		
		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}
		
		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
			
			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	}
	else if(m_nOvenModelType == CHAMBER_TEMP2300 && m_nRxBuffCnt >= 6)	//Model Type TEMP2500//lmh 20120524 add
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;
		
		
		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;
		
		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//	pDoc->WriteSysLog(stess);
		
		
		if(strCmd == "RRD" && m_nRxBuffCnt >= 27 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			
			if(data == 0x0a)
			{
				//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);
					
				}		
				m_nRxBuffCnt = 0;
			}
			
			
		}
		
		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "AMI" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);
					
					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
				
			}
		}
		
		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}
		
		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
			
			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	}
	
	else if(m_nOvenModelType == CHAMBER_TEMI2300 && m_nRxBuffCnt >= 6)	//Model Type TEMI2500 
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;
		
		
		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;
		
		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//	pDoc->WriteSysLog(stess);
		
		
		if(strCmd == "RRD" && m_nRxBuffCnt >= 27 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			
			if(data == 0x0a)
			{
				//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);
					
				}		
				m_nRxBuffCnt = 0;
			}
			
			
		}
		
		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "AMI" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);
					
					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
				
			}
		}
		
		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}
		
		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
			
			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	}

	else if(m_nOvenModelType == CHAMBER_TEMP2500 && m_nRxBuffCnt >= 6)	//Model Type TEMP2500//lmh 20120524 add
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;
		
		
		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;
		
		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//	pDoc->WriteSysLog(stess);
		
		if(strCmd == "RRD" && m_nRxBuffCnt >= 27 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				//if(strCmd == ",09,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);
					
				}		
				m_nRxBuffCnt = 0;
			}
			
			
		}
		
		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "AMI" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);
					
					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
				
			}
		}
		
		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}
		
		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
			
			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	}
	
	else if(m_nOvenModelType == CHAMBER_TEMI2500 && m_nRxBuffCnt >= 6)	//Model Type TEMI2500 
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;
		
		
		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;
		
		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//	pDoc->WriteSysLog(stess);
		
		if(strCmd == "RRD" && m_nRxBuffCnt >= 27 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			
			if(data == 0x0a)
			{
				//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);
					
				}		
				m_nRxBuffCnt = 0;
			}
			
			
		}
		
		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "AMI" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);
					
					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
				
			}
		}
		
		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}
		
		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
			
			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	else if(m_nOvenModelType == CHAMBER_TD500 && m_nRxBuffCnt >= 6)	//Model Type  
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 3);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo;
		
		
		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[5], 3);
		strCmd = szBuff;
		
		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//	pDoc->WriteSysLog(stess);
		
		
		if(strCmd == "RRD" && m_nRxBuffCnt >= 31 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			
			if(data == 0x0a)
			{
				//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[8], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[12], m_nRxBuffCnt-12);
					SetCurrentData(nOvenIndex, szBuff);
					
				}		
				m_nRxBuffCnt = 0;
			}
			
			
		}
		
		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "WHO" && m_nRxBuffCnt > 17 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				
				memcpy(szBuff, &m_szRxBuff[8], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[12], m_nRxBuffCnt-11-2);
					
					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;
				
			}
		}
		
		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[8], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}
		
		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
			
			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[8], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[12], m_nRxBuffCnt-10);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
			
		}
	}
	// -
	//////////////////////////////////////////////////////////////////////////
	else if(m_nOvenModelType == CHAMBR_WiseCube && m_nRxBuffCnt >= 10)	//Model Type WiseCube
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		
		int nOvenIndex = nOvIndex;
		

		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[0], 5);
		strCmd = szBuff;

	//	CString stess;
	//	stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d", strCmd, m_nRxBuffCnt, nOvenIndex);
	//	pDoc->WriteSysLog(stess);

		if(strCmd == "#08tp" && m_nRxBuffCnt >= 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			m_nRxBuffCnt = 0;

			//TRACE("Receive command %s from device %d\n", strCmd, nOvenIndex);
			ZeroMemory(szBuff, 64);
			memcpy(szBuff, &m_szRxBuff[5], 6);
			SetCurrentData(nOvenIndex, szBuff);
			m_nRxBuffCnt = 0;
			
		}

		if(strCmd == "#08ts" && m_nRxBuffCnt >= 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			m_nRxBuffCnt = 0;

	//		TRACE("Receive command %s from device %d\n", strCmd, nOvenIndex);
			ZeroMemory(szBuff, 64);
			memcpy(szBuff, &m_szRxBuff[5], 6);
			SetRefData(nOvenIndex, szBuff);
			m_nRxBuffCnt = 0;
			
		}
		
		//Run State
		if(strCmd == "#10OA" && m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			
			memcpy(szBuff, &m_szRxBuff[5], 8);
			WORD runState = atoi(&szBuff[0]);
			m_OvenData[nOvenIndex].SetRunState(runState);
			m_nRxBuffCnt = 0;

			//Run 상태를 읽은 후 설정 온도를 불러온다.
			if(m_pSerial[nOvenIndex]->IsPortOpen())
			{
				
				char szTxBuff[64] = {0,};
				char szRxBuff[64] = {0,};

				int nTxSize, nRxSize;
				

				nTxSize = GetReqOvenReadSPTemperatureCmd(nOvenIndex, szTxBuff, nRxSize);
				
				m_pSerial[nOvenIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize);
			}				
		}
	}
	//Model Type TEMP2520
	else if(m_nOvenModelType == CHILLER_TEMP2520 && m_nRxBuffCnt >= 6)
	{
		CString strCmd;
		//char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.
		char szBuff[1024]; //ksj 20201228 : 버퍼 크기 늘림.
		
		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo - 1;		
		
		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;
		
		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//		pDoc->WriteSysLog(stess);	
		//ksj 20200723 : debug
// 		CString strLog;
// 		GLog::log_All("debug","debug",stess);					
		//ksj end
		
		//수신 된 CH1의 PV(D0001) 값이 50.0이고, CH1의 SP(D0003) 값이 30.0일 경우
		//수신(CheckSum 포함) : [STX]01RRD,OK,01F4,012C18[CR][LF]
		//7개의 상태를 요청했으므로 SUM 이전까지의 버퍼사이즈는 44
		if(strCmd == "RRD" && m_nRxBuffCnt > 44 && nOvenIndex >= 0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//ksj 20200723 : debug
 				CString strLog;
 				strLog.Format("m_nRxBuffCnt %d",m_nRxBuffCnt);
 				GLog::log_All("debug","debug",strLog);					
				//ksj end

				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
 					//ZeroMemory(szBuff, 64);
					/*ZeroMemory(szBuff, sizeof(szBuff)); //ksj 20201228
 					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);*/

					//ksj 20200828 : 오버플로 예외처리 (긴급,임시) 	//ksj 20200831 : 다시 주석처리. 그냥 버퍼 사이즈를 늘렸음. (szBuff 64->256), m_nRxBuffCnt의 사이즈가 200이므로, 이것보다 크게 잡아줌. 
					//ksj 20201228 : 다시 활성화 시킴. 오버플로우 현상 및 프로그램 죽는 현상 다시 보고됨. 안전하게 버퍼 사이즈 넘치지 않도록 필터링.
					ZeroMemory(szBuff, sizeof(szBuff)); //ksj 20201228
					if(m_nRxBuffCnt-10 > sizeof(szBuff)-10) //오버 플로 방지
					{
						//버퍼 오버플로우
						CString strLog;
						strLog.Format("overflow rxBuffCnt %d",m_nRxBuffCnt);
						GLog::log_All("debug","debug",strLog);			
					}
					else
					{
						//기존 코드
						memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
						SetCurrentData(nOvenIndex, szBuff);
					}			
				}		
				m_nRxBuffCnt = 0;
			}
		}
		
		//수신(CheckSum 포함) : [STX]01AMI,OK,TEMP-2020[sp][sp]V00-R0026[CR][LF]
		//SUM 이전까지의 버퍼사이즈는 28
		if(strCmd == "AMI" && m_nRxBuffCnt > 28 && nOvenIndex >= 0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[21], 7);
					
					SetVersionInfo(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
		}
		
		//정치운전시의 CH1.TSP(D0104)와 CH1.SLOPE(D0110)에 데이터를 쓸 경우
		//수신(CheckSum 포함) : [STX]01WRD,OK,[SUM1][SUM2][CR][LF]
		//SUM 이전까지의 버퍼사이즈는 9
		if(strCmd == "WRD"&& m_nRxBuffCnt > 9 && nOvenIndex >= 0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 3);
				strCmd = szBuff;
				if(strCmd == ",OK")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[9], m_nRxBuffCnt-9);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}
		}
	}
	else if(m_nOvenModelType == CHILLER_ST590 && m_nRxBuffCnt >= 6)	//ksj 20200401 : ST590추가. TEMP880꺼 복사함. 세부 내용 수정 필요.
	{
		CString strCmd;
		char szBuff[256]; //char szBuff[64]; //ksj 20200831 : 버퍼 크기 늘림.

		//AddressNo
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[1], 2);
		int nAddNo = atol(szBuff);
		int nOvenIndex = nAddNo -1;


		//Command
		ZeroMemory(szBuff, 32);
		memcpy(szBuff, &m_szRxBuff[3], 3);
		strCmd = szBuff;

		CString stess;
		stess.Format("CMD : %s, RxBuffCnt : %d, Oven Index : %d\r\n", strCmd, m_nRxBuffCnt, nOvenIndex);
		//	pDoc->WriteSysLog(stess);


		if(strCmd == "RRD" && m_nRxBuffCnt >= 27 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{

			if(data == 0x0a)
			{
				//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetCurrentData(nOvenIndex, szBuff);

				}		
				m_nRxBuffCnt = 0;
			}


		}

		//Respone이 어떻게 들어오는지 메뉴얼에 설명이 없다.
		if(strCmd == "AMI" && m_nRxBuffCnt > 11 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				//					TRACE("Receive command %s from device %d\n", strCmd, nAddNo);

				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[11], m_nRxBuffCnt-11-3);

					SetVersionInfo(nOvenIndex, szBuff);
					//CString strTemp;
					//strTemp.Format("Oven %d 온도 조절기 Model명 [%s] 확인됨", nOvenIndex+1, szBuff);
					//m_pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//					SetOvenFixMode(nOvenIndex, TRUE);
				}
				m_nRxBuffCnt = 0;

			}
		}

		if(strCmd == "WSD"&& m_nRxBuffCnt == 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			if(data == 0x0a)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
				}
				m_nRxBuffCnt = 0;
			}
		}

		if(strCmd == "WRD"&& m_nRxBuffCnt >= 13 && nOvenIndex >=0 && nOvenIndex < MAX_OVEN_COUNT)
		{
			//				TRACE("Receive command %s from device %d\n", strCmd, nAddNo);

			if(0x0a == data)
			{
				memcpy(szBuff, &m_szRxBuff[6], 4);
				strCmd = szBuff;
				if(strCmd == ",OK,")
				{
					ZeroMemory(szBuff, 64);
					memcpy(szBuff, &m_szRxBuff[10], m_nRxBuffCnt-10);
					SetRefData(nOvenIndex, szBuff);
				}
				m_nRxBuffCnt = 0;
			}

		}
	}
}

int COvenCtrl::GetReqChillerPumpCmd(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	CString strSetData;
	WORD wData;
	switch(m_nOvenModelType)
	{
	case CHILLER_TEMP2520:
		{
			strSetData.Format("%.1f",fDataT*10.0f);
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결

			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			//if(m_nChannelMapping == 2)		szCmd[2] = '1'+(nDeviceNumber-1);
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '5';										//CH2 펌프 설정 값
			//if(m_nChannelMapping == 2)		szCmd[13] = '5';		
			szCmd[14] = ',';

			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));

			WORD pcl1 = 0;			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}			

			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);

			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;

			nRetNum = 24;
			nResLen = 13;

			//m_nChannelMapping = 0;
			
			//ksj 20200615 : 칠러 로그 추가
			CString strTemp;
			strTemp.Format(_T("CHILLER_TEMP2520 GetReqChillerPumpCmd : %d, %s"),
				wData, szCmd);
			log_All(_T("all"),_T("cmd_chill"),strTemp);	
			//ksj end

		}
		break;
	case CHILLER_ST590: //ST590추가. 유량 펌프는 ST560을 사용하며, ST560과 590은 같은 프로토콜 사용.
		{
			strSetData.Format("%.1f",fDataT*10.0f);
			wData = atoi(strSetData);		

			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '2';
			szCmd[12] = '0';
			szCmd[13] = '1';							//펌프 PV 설정.				
			szCmd[14] = ',';

			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));

			WORD pcl1 = 0;			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}			

			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);

			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;

			nRetNum = 24;
			nResLen = 13;

			//m_nChannelMapping = 0;
		}
		break;
		// -
		//////////////////////////////////////////////////////////////////////////
	default:break;
	}

	return nRetNum;
}

int COvenCtrl::GetReqOvenTemperatureCmd(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen)
{
	//ksj 20200622 : 챔버 SP 값 보정 추가. 칠러일때는 반영 안되도록 해야함. (오창2공장 오류)
	float fSpFactor = 1.0f; //곱해서 보정할 Factor
	BOOL bIsChamber = FALSE; //챔버인지 체크 TRUE: 챔버
	int nModelType = GetOvenModelType();

// 	int nOvenPort1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "PortNo", 0);
// 	int nOvenPort2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "PortNo", 0);

	if(m_nOvenPortNum == 1 || m_nOvenPortNum == 2) //포트 번호로 챔버 판단.
	{		
		CString strTemp;
		strTemp.Format("OvenOndoSPFactor%d",m_nOvenPortNum);
		fSpFactor = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, strTemp, 10);	
		bIsChamber = TRUE; //챔버인지 체크.
	}	
	//ksj end

	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	CString strSetData;
	WORD wData;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		wData = WORD((fDataT+100.0f)*100.0f);
		
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'W';
		szCmd[5] = 'R';
		szCmd[6] = 'P';
		szCmd[7] = ',';
		szCmd[8] = '0';
		szCmd[9] = '0';
		szCmd[10] = '1';
		szCmd[11] = '2';
		szCmd[12] = ',';
		szCmd[13] = HIBYTE(wData);	//60'Setting
		szCmd[14] = LOBYTE(wData);
		szCmd[15] = 0x03;
		szCmd[16] = 0x0d;
		szCmd[17] = 0x0a;

		nRetNum = 18;
		nResLen = 18;
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
		{
			//strSetData.Format("%.1f",fDataT); //lyj 20200227 SP SEND시 OvenOndoSPFactor1,2 레지스트리 참고
			//strSetData.Format("%.1f",fDataT*10.0f); //lyj 20200511 레지스트리 추출 원복
			
			//ksj 20200622 : 챔버 sp 팩터 추가.
			if(bIsChamber)	strSetData.Format("%.1f",fDataT*fSpFactor);	//챔버만 sp 팩터 계산	
			else strSetData.Format("%.1f",fDataT*10.0f); //기존 기본 값 유지. 향후 칠러 SP값 불러오게 수정 등 필요.
			//ksj end			

			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
//			nRetNum = 23;
			nRetNum = 24;
			nResLen = 13;
			
			TRACE("Cmd : %s\n", szCmd);
		}
		break;		
	case CHAMBR_WiseCube:
		{
			long lDataT = fDataT*100;
			szCmd[0] = '#';
			szCmd[1] = '0';
			szCmd[2] = '8';
			szCmd[3] = 'T';
			szCmd[4] = 'S';
			//HEX 값////////////////////////////////////
			if(lDataT > 0)			
				szCmd[5] = '0';
			else
				szCmd[5] = '1';
			//			char szBuff[5] = {0,};

			sprintf(&szCmd[6], "%5ld", lDataT);
			nRetNum = 11;
			nResLen = 0;
	
		}
		break;
	//20090113 KHS TEMP880 추가
	case CHAMBER_TEMP880:						//Model TEMP880
		{
			//strSetData.Format("%.1f",fDataT); //lyj 20200226 SP SEND시 OvenOndoSPFactor1,2 레지스트리 참고
			//strSetData.Format("%.1f",fDataT*10.0f); //lyj 20200511 레지스트리 추출 원복

			//ksj 20200622 : 챔버 sp 팩터 추가.
			if(bIsChamber)	strSetData.Format("%.1f",fDataT*fSpFactor);	//챔버만 sp 팩터 계산	
			else strSetData.Format("%.1f",fDataT*10.0f); //기존 기본 값 유지. 향후 칠러 SP값 불러오게 수정 등 필요.
			//ksj end	

			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결

			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
 			szCmd[10] = '0';
 			szCmd[11] = '1';
 			szCmd[12] = '0';
 			szCmd[13] = '2';
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
//			nRetNum = 23;
			nRetNum = 24;
			nResLen = 13;

			TRACE("Cmd : %s\n", szCmd);
		}
		break;
	case CHAMBER_TEMP2300://lmh 20120524 Temp2500 add
		{
			//strSetData.Format("%.1f",fDataT); //lyj 20200226 SP SEND시 OvenOndoSPFactor1,2 레지스트리 참고
			//strSetData.Format("%.1f",fDataT*10.0f); //lyj 20200511 레지스트리 추출 원복
			
			//ksj 20200622 : 챔버 sp 팩터 추가.
			if(bIsChamber)	strSetData.Format("%.1f",fDataT*fSpFactor);	//챔버만 sp 팩터 계산	
			else strSetData.Format("%.1f",fDataT*10.0f); //기존 기본 값 유지. 향후 칠러 SP값 불러오게 수정 등 필요.
			//ksj end	

			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '4';
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
			//			nRetNum = 23;
			nRetNum = 24;
			nResLen = 13;
			
			TRACE("Cmd : %s\n", szCmd);
		}
		break;
	case CHAMBER_TEMI2300://lmh 20120524 Temp2500 add
		{
			//strSetData.Format("%.1f",fDataT); //lyj 20200226 SP SEND시 OvenOndoSPFactor1,2 레지스트리 참고
			//strSetData.Format("%.1f",fDataT*100.0f); //lyj 20200511 레지스트리 추출 원복

			//ksj 20200622 : 챔버 sp 팩터 추가.
			if(bIsChamber)	strSetData.Format("%.1f",fDataT*fSpFactor);	//챔버만 sp 팩터 계산	
			else strSetData.Format("%.1f",fDataT*100.0f); //기존 기본 값 유지. 향후 칠러 SP값 불러오게 수정 등 필요.
			//ksj end	

			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			//			strSetData.Format("%.2f",fDataT*100.0f);
			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
			//			nRetNum = 23;
			nRetNum = 24;
			nResLen = 13;
			
			TRACE("Cmd : %s\n", szCmd);
		}
		break;
	case CHAMBER_TEMP2500://lmh 20120524 Temp2500 add
		{
			//strSetData.Format("%.1f",fDataT); //lyj 20200226 SP SEND시 OvenOndoSPFactor1,2 레지스트리 참고
			//strSetData.Format("%.1f",fDataT*10.0f); //lyj 20200511 레지스트리 추출 원복

			//ksj 20200622 : 챔버 sp 팩터 추가.
			if(bIsChamber)	strSetData.Format("%.1f",fDataT*fSpFactor);	//챔버만 sp 팩터 계산	
			else strSetData.Format("%.1f",fDataT*10.0f); //기존 기본 값 유지. 향후 칠러 SP값 불러오게 수정 등 필요.
			//ksj end	

			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '4';
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
			//			nRetNum = 23;
			nRetNum = 24;
			nResLen = 13;
			
			TRACE("Cmd : %s\n", szCmd);
		}
		break;
	case CHAMBER_TEMI2500://lmh 20120524 Temp2500 add
		{

			//strSetData.Format("%.1f",fDataT); //lyj 20200226 SP SEND시 OvenOndoSPFactor1,2 레지스트리 참고
			//strSetData.Format("%.1f",fDataT*10.0f); //lyj 20200511 레지스트리 추출 원복

			//ksj 20200622 : 챔버 sp 팩터 추가.
			if(bIsChamber)	strSetData.Format("%.1f",fDataT*fSpFactor);	//챔버만 sp 팩터 계산	
			else strSetData.Format("%.1f",fDataT*10.0f); //기존 기본 값 유지. 향후 칠러 SP값 불러오게 수정 등 필요.
			//ksj end	

			//strSetData.Format("%.1f",fDataT*100.0f);		20170403 온도상수

			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
//			strSetData.Format("%.2f",fDataT*100.0f);
			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
			//			nRetNum = 23;
			nRetNum = 24;
			nResLen = 13;
			
			TRACE("Cmd : %s\n", szCmd);
		}
		break;
		// ==============================
		// Model TEMP2520
		// ==============================
	case CHILLER_TEMP2520:
		{
			wData = WORD(fDataT*10.0f);
			
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			//if(m_nChannelMapping == 2)		szCmd[2] = '1'+(nDeviceNumber-1);
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '4';										//CH1 온도 설정
			//if(m_nChannelMapping == 2)		szCmd[13] = '5';		
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
			nRetNum = 24;
			nResLen = 13;
			
			//m_nChannelMapping = 0;


			//ksj 20200615 : 칠러 로그 추가
			CString strTemp;
			strTemp.Format(_T("CHILLER_TEMP2520 GetReqOvenTemp : %d, %s"),
				wData, szCmd);
			log_All(_T("all"),_T("cmd_chill"),strTemp);	
			//ksj end
		}
		break;
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	case CHAMBER_TD500:
		{
			strSetData.Format("%.1f",fDataT*10.0f);
			//wData = WORD(fSetTemp);		//20090120 KHS 100.0f->10.0f로 수정
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';
			szCmd[3] = '1'+nDeviceNumber;
			szCmd[4] = ',';
			szCmd[5] = 'W';
			szCmd[6] = 'R';
			szCmd[7] = 'D';
			szCmd[8] = ',';
			szCmd[9] = '0';
			szCmd[10] = '1';
			szCmd[11] = ',';
			szCmd[12] = '1';
			szCmd[13] = '0';
			szCmd[14] = '0';
			szCmd[15] = '0';
			szCmd[16] = ',';
			
			sprintf(&szCmd[17], "%02X", HIBYTE(wData));
			sprintf(&szCmd[19], "%02X", LOBYTE(wData));
			szCmd[21] = ',';
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 22; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[22], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[23], "%X", pcl1&0x000f);
			szCmd[24] = 0x0d;
			szCmd[25] = 0x0a;
		
			
			//			nRetNum = 23;
			nRetNum = 26;
			nResLen = 16;
			
			TRACE("Cmd : %s\n", szCmd);
		}
		break;
	//ksj 20200401 : ST590 추가.
	case CHILLER_ST590:						//ST590
		{
			strSetData.Format("%.1f",fDataT); 
			wData = atof(strSetData)*10;	// ksj 20200401 : ST590 임시 테스트.

			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '2';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';

			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));

			WORD pcl1 = 0;

			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;

			//			nRetNum = 23;
			nRetNum = 24;
			nResLen = 13;

			TRACE("Cmd : %s\n", szCmd);
		}
		break;
	// -
	//////////////////////////////////////////////////////////////////////////
	default:break;
	}

	return nRetNum;
}

void COvenCtrl::SetCurrentDataWiseCube(int nIndex, char szBuffData[])
{
	com_WISECUBEData myData;
	float fData;
//	int nDataIndex = 0;

	//"#08tpSXXXXX" : Temp PV 
	//S = 0(+) or 1(-)
	//eg) +120.00°C = "012000"
    //     -20.00°C = "102000"

	ZeroMemory(myData.bData, sizeof(byte)*5);

	fData = atof(szBuffData+1) /100.0f;

	if(szBuffData[0] == 1)
		fData = fData * -1.0f;

	m_OvenData[nIndex].SetCurTemperature(fData);

	//온도 데이터를 읽은 후 Run 상태를 읽어온다.
	if(m_pSerial[nIndex]->IsPortOpen())
	{
		char szTxBuff[] = "#02OA";
		char szRxBuff[64] = {0,};

		int nTxSize = 5;
		int nRxSize = 13;
		
		m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize);
	}
	
}

int COvenCtrl::GetReqPumpRunCmd(int nDeviceNumber, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
		// ==============================
		// Model TEMP2520
		// ==============================
	case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			//if(m_nChannelMapping == 2)		szCmd[2] = '1'+(nDeviceNumber-1);
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '3';											//CH2 Run/Stop 관련 Reg
			//if(m_nChannelMapping == 2)		szCmd[13] = '3';		
			szCmd[14] = ',';
			szCmd[15] = '0';
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';

			WORD pcl1 = 0;		
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}		

			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);

			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;

			nRetNum = 23;
			nResLen = 13;

			//m_nChannelMapping = 0;
		}
		break;
		// -
		//////////////////////////////////////////////////////////////////////////

	default:break;
	}

	return nRetNum;
}


int COvenCtrl::GetReqRunCmd(int nDeviceNumber, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'W';
		szCmd[5] = 'R';
		szCmd[6] = 'P';
		szCmd[7] = ',';
		szCmd[8] = '0';
		szCmd[9] = '0';
		szCmd[10] = '0';
		szCmd[11] = '1';
		szCmd[12] = ',';
		szCmd[13] = 0;
		szCmd[14] = 1;
		szCmd[15] = 0x03;
		szCmd[16] = 0x0d;
		szCmd[17] = 0x0a;

		nRetNum = 18;
		nResLen = 18;
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
		{
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'W';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '1';
		szCmd[9] = ',';
		szCmd[10] = '0';			//Run/Stop 관련 Reg
		szCmd[11] = '1';
		szCmd[12] = '0';
		szCmd[13] = '1';
		szCmd[14] = ',';
		szCmd[15] = '0';			//Run Data
		szCmd[16] = '0';
		szCmd[17] = '0';
		szCmd[18] = '1';
		
		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 19; i++)
		{
			pcl1 += szCmd[i];
		}
		
		
		sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[20], "%X", pcl1&0x000f);
		
		szCmd[21] = 0x0d;
		szCmd[22] = 0x0a;
		
		nRetNum = 23;
		nResLen = 13;
		}
		break;		
	case CHAMBR_WiseCube:
		szCmd[0] = '#';
		szCmd[1] = '0';
		szCmd[2] = '2';
		szCmd[3] = 'O';
		szCmd[4] = 'P';

		nRetNum = 5;
		nResLen = 0;
		break;
		//20090115 KHS
	case CHAMBER_TEMP880:						//Model TEMP880
		{
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'W';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '1';		// 20090527 KHM
		//szCmd[8] = '1';
		szCmd[9] = ',';
		szCmd[10] = '0';			//Run/Stop 관련 Reg
		szCmd[11] = '1';
		szCmd[12] = '0';
		szCmd[13] = '1';
		szCmd[14] = ',';
		szCmd[15] = '0';			//Run Data
		szCmd[16] = '0';
		szCmd[17] = '0';
		szCmd[18] = '1';
							// 20090527 KHM 
		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 19; i++)
		{
			pcl1 += szCmd[i];
		}
		
		
		sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[20], "%X", pcl1&0x000f);
		
		szCmd[21] = 0x0d;
		szCmd[22] = 0x0a;
		
		nRetNum = 23;
		nResLen = 13;
		}
		break;
	case CHAMBER_TEMP2300://lmh 20120524 Temp2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';		// 20090527 KHM
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			// 20090527 KHM 
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMI2300://lmh 20120524 Temp2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';		// 20090527 KHM
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			// 20090527 KHM 
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMP2500://lmh 20120524 Temp2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';		// 20090527 KHM
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			// 20090527 KHM 
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMI2500://lmh 20120524 Temp2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';		// 20090527 KHM
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			// 20090527 KHM 
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
		// ==============================
		// Model TEMP2520
		// ==============================
	case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			//if(m_nChannelMapping == 2)		szCmd[2] = '1'+(nDeviceNumber-1);
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';										//CH1 Run/Stop 관련 Reg
			//if(m_nChannelMapping == 2)		szCmd[13] = '3';		//CH2 Run/Stop 관련 Reg
			szCmd[14] = ',';
			szCmd[15] = '0';
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			
			WORD pcl1 = 0;		
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}		
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
			
			//m_nChannelMapping = 0;
		}
		break;
	//////////////////////////////////////////////////////////////////////////
	// + 2014.02.26
	case CHAMBER_TD500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';
			szCmd[3] = '1'+nDeviceNumber;
			szCmd[4] = ',';
			szCmd[5] = 'W';
			szCmd[6] = 'R';
			szCmd[7] = 'D';
			szCmd[8] = ',';
			szCmd[9] = '0';
			szCmd[10] = '1';		// 20090527 KHM
			szCmd[11] = ',';
			szCmd[12] = '0';			//Run/Stop 관련 Reg
			szCmd[13] = '1';
			szCmd[14] = '5';
			szCmd[15] = '2';
			szCmd[16] = ',';
			szCmd[17] = '0';			//Run Data
			szCmd[18] = '0';
			szCmd[19] = '0';
			szCmd[20] = '1';
			szCmd[21] = ',';
			// 20090527 KHM 
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 22; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[22], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[23], "%X", pcl1&0x000f);
			
			szCmd[24] = 0x0d;
			szCmd[25] = 0x0a;
			
			nRetNum = 26;
			nResLen = 16;
		}
		break;
	case CHILLER_ST590:			//ksj 20200401 : ST590 추가. TEMP880 코드 복사. 세부 내용 수정 필요.
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';		// 20090527 KHM
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '0'; //0이 Run임.
			// 20090527 KHM 
			WORD pcl1 = 0;

			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);

			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;

			nRetNum = 23;
			nResLen = 13;
		}
		break;
	// -
	//////////////////////////////////////////////////////////////////////////

	default:break;
	}

	return nRetNum;
}

int COvenCtrl::GetReqPumpStopCmd(int nDeviceNumber, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
// ==============================
// Model TEMP2520
// ==============================
	case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			//if(m_nChannelMapping == 2)		szCmd[2] = '1'+(nDeviceNumber-1);
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';										//CH1 Run/Stop 관련 Reg
			//if(m_nChannelMapping == 2)		szCmd[13] = '3';		//CH2 Run/Stop 관련 Reg
			szCmd[14] = ',';
			szCmd[15] = '0';
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '4';

			WORD pcl1 = 0;			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}			

			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);

			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;

			nRetNum = 23;
			nResLen = 13;

			//m_nChannelMapping = 0;
		}
		break;
		// -
		//////////////////////////////////////////////////////////////////////////
	default:break;
	}

	return nRetNum;
}

int COvenCtrl::GetReqStopCmd(int nDeviceNumber, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'W';
		szCmd[5] = 'R';
		szCmd[6] = 'P';
		szCmd[7] = ',';
		szCmd[8] = '0';
		szCmd[9] = '0';
		szCmd[10] = '0';
		szCmd[11] = '1';
		szCmd[12] = ',';
		szCmd[13] = 0;
		szCmd[14] = 0;
		szCmd[15] = 0x03;
		szCmd[16] = 0x0d;
		szCmd[17] = 0x0a;

		nRetNum = 18;
		nResLen = 18;
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
		{
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'W';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '1';
		szCmd[9] = ',';
		szCmd[10] = '0';			//Run/Stop 관련 Reg
		szCmd[11] = '1';
		szCmd[12] = '0';
		szCmd[13] = '1';
		szCmd[14] = ',';
		szCmd[15] = '0';			//Run Data
		szCmd[16] = '0';
		szCmd[17] = '0';
		szCmd[18] = '4';
		
		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 19; i++)
		{
			pcl1 += szCmd[i];
		}
		
		
		sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[20], "%X", pcl1&0x000f);
		
		szCmd[21] = 0x0d;
		szCmd[22] = 0x0a;
		
		nRetNum = 23;
		nResLen = 13;
		}
// 		szCmd[0] = 0x02;
// 		szCmd[1] = '1'+nDeviceNumber;
// 		szCmd[2] = 'R';
// 		szCmd[3] = 'R';
// 		szCmd[4] = 'D';
// 		szCmd[5] = ',';
// 		szCmd[6] = '3';
// 		szCmd[7] = ',';
// 		szCmd[8] = '0';				//현재 온도 값 Reg
// 		szCmd[9] = '0';
// 		szCmd[10] = '0';
// 		szCmd[11] = '1';
// 		szCmd[12] = ',';
// 		szCmd[13] = '0';			//현재 설정온도 값 Reg
// 		szCmd[14] = '0';
// 		szCmd[15] = '0';
// 		szCmd[16] = '2';
// 		szCmd[17] = ',';
// 		szCmd[18] = '0';			//Run/Stop 관련 Reg
// 		szCmd[19] = '1';
// 		szCmd[20] = '0';
// 		szCmd[21] = '1';
// 		szCmd[22] = 0x0d;
// 		szCmd[23] = 0x0a;
// 
// 		nRetNum = 24;
// 		nResLen = 27;
		break;		
	case CHAMBR_WiseCube:
		szCmd[0] = '#';
		szCmd[1] = '0';
		szCmd[2] = '2';
		szCmd[3] = 'O';
		szCmd[4] = 'Q';

		nRetNum = 5;
		nResLen = 0;
		break;
		//20090115 KHS
	case CHAMBER_TEMP880:						//Model TEMP880
		{
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'W';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '1';				// 20090527 KHM 
		//szCmd[8] = '1';
		szCmd[9] = ',';
		szCmd[10] = '0';			//Run/Stop 관련 Reg
		szCmd[11] = '1';
		szCmd[12] = '0';
		szCmd[13] = '1';
		szCmd[14] = ',';
		szCmd[15] = '0';			//Run Data
		szCmd[16] = '0';
		szCmd[17] = '0';
		szCmd[18] = '4';
		
		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 19; i++)
		{
			pcl1 += szCmd[i];
		}
		
		
		sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[20], "%X", pcl1&0x000f);
		
		szCmd[21] = 0x0d;
		szCmd[22] = 0x0a;
		
		nRetNum = 23;
		nResLen = 13;
		}
		break;
	case CHAMBER_TEMP2300:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';				// 20090527 KHM 
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '4';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMI2300:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';				// 20090527 KHM 
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '4';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;	
	case CHAMBER_TEMP2500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';				// 20090527 KHM 
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '4';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMI2500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';				// 20090527 KHM 
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '4';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;	

		// ==============================
		// Model TEMP2520
		// ==============================
	case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			//if(m_nChannelMapping == 2)		szCmd[2] = '1'+(nDeviceNumber-1);
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '2';										//CH1 Run/Stop 관련 Reg
			//if(m_nChannelMapping == 2)		szCmd[13] = '3';		//CH2 Run/Stop 관련 Reg
			szCmd[14] = ',';
			szCmd[15] = '0';
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '4';
			
			WORD pcl1 = 0;			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			szCmd[23] = 0x00;
			
			nRetNum = 24;
			nResLen = 13;
			
			//m_nChannelMapping = 0;
		}
		break;;
	case CHILLER_ST590: //ksj 20200401 : ST590추가. TEMP880 복사함. 세부내용 수정 필요.
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';				// 20090527 KHM 
			//szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1'; //ksj 20200401 : 1이 STOP 0이 RUN

			WORD pcl1 = 0;

			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);

			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;

			nRetNum = 23;
			nResLen = 13;
		}
		break;
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	case CHAMBER_TD500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';
			szCmd[3] = '1'+nDeviceNumber;
			szCmd[4] = ',';
			szCmd[5] = 'W';
			szCmd[6] = 'R';
			szCmd[7] = 'D';
			szCmd[8] = ',';
			szCmd[9] = '0';
			szCmd[10] = '1';				// 20090527 KHM 
			//szCmd[8] = '1';
			szCmd[11] = ',';
			szCmd[12] = '0';			//Run/Stop 관련 Reg
			szCmd[13] = '1';
			szCmd[14] = '5';
			szCmd[15] = '2';
			szCmd[16] = ',';
			szCmd[17] = '0';			//Run Data
			szCmd[18] = '0';
			szCmd[19] = '0';
			szCmd[20] = '0';
			szCmd[21] = ',';
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 22; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[22], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[23], "%X", pcl1&0x000f);
			
			szCmd[24] = 0x0d;
			szCmd[25] = 0x0a;
			
			nRetNum = 26;
			nResLen = 16;
		}
		break;
		// -
		//////////////////////////////////////////////////////////////////////////
	default:break;
	}

	return nRetNum;
}

void COvenCtrl::DeleteAllOven()
{
	for(int i = 0 ; i < m_nInstallCount; i++)
	{
		ResetData(i);
	}
	m_nInstallCount = 0;
}

BOOL COvenCtrl::InitSerialPort(int nOvenPortNum)
{
	//SERIAL_CONFIG	m_SerialConfig;
	CSerialPort *pPort= NULL;
	pPort = new CSerialPort();
	ZeroMemory(&m_SerialConfig, sizeof(SERIAL_CONFIG));	//ljb 20180219 add

	//ksj 20201117 : 재초기화 가능하도록 해제 기능 추가.
	if(m_pSerial[0])
	{
		if(m_pSerial[0]->IsPortOpen())	//포트가 열려있으면 닫는다.
		{
			m_pSerial[0]->DisConnect();
			Sleep(1000);
		}

		delete m_pSerial[0];
		m_pSerial[0] = NULL;
	}	
	//ksj end

	m_pSerial[0] =  pPort;
	m_nOvenPortNum = nOvenPortNum; //lyj 20200226

	if (nOvenPortNum == 1)
	{
		m_SerialConfig.nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "PortNo", 1);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "StopBit", 1);
		m_SerialConfig.nPortBuffer = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Buffer", 512);
		m_SerialConfig.portParity = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Parity", 'N');
		m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

		if(pPort) pPort->m_bUseStxFilter = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "UseStxFilter", 0); //ksj 20200319 : 챔버 통신시 stx 앞 쓰레기 값 필터링 기능 사용 여부		 
	}
	else if (nOvenPortNum == 2)
	{
		m_SerialConfig.nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "PortNo", 2);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "StopBit", 1);
		m_SerialConfig.nPortBuffer = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Buffer", 512);
		m_SerialConfig.portParity = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Parity", 'N');
		m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

		if(pPort) pPort->m_bUseStxFilter = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "UseStxFilter", 0); //ksj 20200319 : 챔버 통신시 stx 앞 쓰레기 값 필터링 기능 사용 여부		 
	}
	else if (nOvenPortNum == 11 ||
		     nOvenPortNum == 12 ||
			 nOvenPortNum == 13 ||
			 nOvenPortNum == 14)
	{
		CString strKey=_T("");
		if(nOvenPortNum==11)      strKey=REG_SERIAL_CHILLER1;
		else if(nOvenPortNum==12) strKey=REG_SERIAL_CHILLER2;
		else if(nOvenPortNum==13) strKey=REG_SERIAL_CHILLER3;
		else if(nOvenPortNum==14) strKey=REG_SERIAL_CHILLER4;
		
		m_SerialConfig.nPortNum      = AfxGetApp()->GetProfileInt(strKey, "PortNo", 3);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(strKey, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(strKey, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(strKey, "StopBit", 1);
		m_SerialConfig.nPortBuffer   = AfxGetApp()->GetProfileInt(strKey, "Buffer", 512);
		m_SerialConfig.portParity    = AfxGetApp()->GetProfileInt(strKey, "Parity", 'N');
		m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

		if(pPort) pPort->m_bUseStxFilter = AfxGetApp()->GetProfileInt(strKey, "UseStxFilter", 0); //ksj 20200319 : 챔버 통신시 stx 앞 쓰레기 값 필터링 기능 사용 여부		 
	}

	if(GetOvenModelType() > CHAMBER_MODBUS_NEX_START)
	{
		if (nOvenPortNum == 7 || //yulee 20190611_1 chiller_modbus 1 //yulee 20190614_*
			nOvenPortNum == 8 ||		   //yulee 20190611_1 chiller_modbus 2
			nOvenPortNum == 9 || //ksj 20201106 : 누락 코드 추가. 넥스 컨트롤 칠러는 3~4번 사용이 안되는 상태.
			nOvenPortNum == 10 ) //ksj 20201106 : 누락 코드 추가. 넥스 컨트롤 칠러는 3~4번 사용이 안되는 상태.

		{
			CString strKey=_T("");
			if(nOvenPortNum==7)      strKey=REG_SERIAL_CHILLER1;
			else if(nOvenPortNum==8) strKey=REG_SERIAL_CHILLER2;
			else if(nOvenPortNum==9) strKey=REG_SERIAL_CHILLER3;
			else if(nOvenPortNum==10) strKey=REG_SERIAL_CHILLER4;

			m_SerialConfig.nPortNum      = AfxGetApp()->GetProfileInt(strKey, "PortNo", 5);
			m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(strKey, "BaudRate", 9600);
			m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(strKey, "DataBit", 8);
			m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(strKey, "StopBit", 1);
			m_SerialConfig.nPortBuffer   = AfxGetApp()->GetProfileInt(strKey, "Buffer", 512);
			m_SerialConfig.portParity    = AfxGetApp()->GetProfileInt(strKey, "Parity", 'N');
			m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

			CString str ;
			//str.Format(_T("COM%d"), m_SerialConfig.nPortNum) ;
			str.Format(_T("\\\\.\\COM%d"), m_SerialConfig.nPortNum); //ksj 20201106 : COM10 이상 인식 안되는 현상 수정
			
			m_ctx = NULL ;	
			m_ctx = modbus_new_rtu(str, m_SerialConfig.nPortBaudRate, 'N', m_SerialConfig.nPortDataBits, m_SerialConfig.nPortStopBits) ;
			modbus_set_debug(m_ctx, TRUE); //ksj 20201106
			m_pCS = new CRITICAL_SECTION ;

			InitializeCriticalSection(m_pCS);

			EnterCriticalSection(m_pCS);
			if(m_ctx != NULL)
			{
				//modbus_set_response_timeout(m_ctx,1,0); //ksj 20201019 : 타임아웃 지정.
				modbus_set_slave(m_ctx, 1);
			}
			LeaveCriticalSection(m_pCS);

			if(modbus_connect(m_ctx) != -1)
			{
				TRACE("COM port(modbus) Initialized %d");
				modbus_flush(m_ctx);
				m_bPortOpen = TRUE;
			}
			else
			{
				CString strErrorMsg;
				strErrorMsg.Format("COM %d port(modbus) Initialize Failed! %d", m_SerialConfig.nPortNum, GetLastError());
				//AfxMessageBox(strErrorMsg);
				GLog::log_All("OvenCtrl","error",strErrorMsg); //lyj 20210809
				m_bPortOpen = FALSE;
				return FALSE;
			}

			int mod_len = 10;
			float ch1NowStat  = -1.0 ;
			int16_t* registers;
			registers = (int16_t*) malloc(mod_len * sizeof(int16_t));


			EnterCriticalSection(m_pCS) ;
			int nErrorNo = 0; //ksj 20201018 : 에러코드 백업			
			int rc = modbus_read_registers(m_ctx, 0, mod_len, (uint16_t*)registers);
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.

			LeaveCriticalSection(m_pCS) ;
			//Sleep(100) ;
			if (rc != mod_len)
			{
				//ksj 20201017 : 로그추가
				CString strLog;
				strLog.Format("InitSerialPort::modbus_read_registers port:%d rc(%d) Error:%d,%d(%s)",m_nOvenPortNum, rc, errno, nErrorNo, modbus_strerror (nErrorNo));
				GLog::log_All("OvenCtrl","error",strLog);
				//ksj end

				// 잠시 케이블이 빠져있을 수 있다.

				// 오류카운트 증가 후 종료여부 판단해야함
				//			ASSERT(FALSE) ;
				//		--------------------- Error ----------------------
				TRACE(_T("FAILED (nb points %d), len = %d\n"), rc, mod_len);
				TRACE(_T("failed: %s\n"), CString(modbus_strerror(errno)));

				ch1NowStat = -1 ;
			}
			else
			{
				// 현재 챔버 상태
				// 운전 관련 상태정보	
				//0 PROGRAM RUN(프로그램 운전)
				//1 FIX RUN 정치 운전  
				//2 HOLD 일시정지  
				//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
				//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
				//7 RESET 정지상태 
				ch1NowStat  = registers[OP_ST_R_CH1_NEX1000_1100] / 10. ;
				if(ch1NowStat> -1.0)
				{
					pPort->StartMonitoring();
					for(int i = 0 ; i < m_nInstallCount; i++)
					{
						m_OvenData[i].m_nDisconnectCnt = 0;
					}
				}
			}
			delete registers ;
			return TRUE;
		}
	}
	else
	{
		if(pPort->IsPortOpen())	//포트가 열려있으면 닫는다.
		{
			//	m_Serial[i].StopMonitoring();
			pPort->DisConnect();
			Sleep(1000);
		}

		BOOL flag = pPort->InitPort(AfxGetApp()->m_pMainWnd->m_hWnd,
			m_SerialConfig.nPortNum, 
			m_SerialConfig.nPortBaudRate,
			m_SerialConfig.portParity,
			m_SerialConfig.nPortDataBits,
			m_SerialConfig.nPortStopBits,
			m_SerialConfig.nPortEvents,
			m_SerialConfig.nPortBuffer);

		if( flag == FALSE )
		{
			CString strErrorMsg;				
			strErrorMsg.Format(Fun_FindMsg("OvenCtrl_onPumpInitSerialPort_msg1","OVENCTRL"), m_SerialConfig.nPortNum);
			//strErrorMsg.Format("COM %d port Initialize Failed!", m_SerialConfig.nPortNum);
			//AfxMessageBox(strErrorMsg); //ksj 20201215 : 주석처리. 자동 reconnect 함수에서 initserialport 함수 호출했는데 이 팝업이 떠버리면 연동이 멈춰버릴 수 있다.

			//ksj 20201215 : 메세지 박스 제거하고 대신 로그 추가
			GLog::log_All("OvenCtrl","error",strErrorMsg);
			return FALSE;
		}

		char szTxBuff[32], szRxBuff[1024];
		int nRxSize = 0;
		int nTxSize;

		pPort->StartMonitoring();
		for(int i = 0 ; i < m_nInstallCount; i++)
		{
			ZeroMemory(szRxBuff,sizeof(szRxBuff));
			ZeroMemory(szTxBuff,sizeof(szTxBuff));
			//통신 불능 연속 시간 Rest 한다.
			m_OvenData[i].m_nDisconnectCnt = 0;
			nTxSize = GetReqStrOvenVersionCmd(i, szTxBuff, nRxSize);
			pPort->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize);
		}
		return TRUE;
	}
	return FALSE;



// 	if(pPort->IsPortOpen())	//포트가 열려있으면 닫는다.
// 	{
// 		//	m_Serial[i].StopMonitoring();
// 		pPort->DisConnect();
// 		Sleep(1000);
// 	}
// 
// 	BOOL flag = pPort->InitPort(AfxGetApp()->m_pMainWnd->m_hWnd,
// 								m_SerialConfig.nPortNum, 
// 								m_SerialConfig.nPortBaudRate,
// 								m_SerialConfig.portParity,
// 								m_SerialConfig.nPortDataBits,
// 								m_SerialConfig.nPortStopBits,
// 								m_SerialConfig.nPortEvents,
// 								m_SerialConfig.nPortBuffer);
// 		
// 	if( flag == FALSE )
// 	{
// 		CString strErrorMsg;
// 		//strErrorMsg.Format("COM %d 포트 초기화를 실패하였습니다!", m_SerialConfig.nPortNum);
// 		strErrorMsg.Format(Fun_FindMsg("OvenCtrl_InitSerialPort_msg1"), m_SerialConfig.nPortNum);//&&
// 		AfxMessageBox(strErrorMsg);
// 		return FALSE;
// 	}
// 
// 	char szTxBuff[32], szRxBuff[1024];
// 	int nRxSize = 0;
// 	int nTxSize;
// 
// 	pPort->StartMonitoring();
// 	for(int i = 0 ; i < m_nInstallCount; i++)
// 	{
// 		ZeroMemory(szRxBuff,sizeof(szRxBuff));
// 		ZeroMemory(szTxBuff,sizeof(szTxBuff));
// 		//통신 불능 연속 시간 Rest 한다.
// 		m_OvenData[i].m_nDisconnectCnt = 0;
// 		nTxSize = GetReqStrOvenVersionCmd(i, szTxBuff, nRxSize);
// 		pPort->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize);
// 	}
// 	return TRUE;
}

BOOL COvenCtrl::InitSerialPort_Modbus(int nOvenPortNum, int nChamberCtrlModel)//yulee 20190609
{
	//SERIAL_CONFIG	m_SerialConfig;
	CSerialPort *pPort= NULL;
	pPort = new CSerialPort();
	ZeroMemory(&m_SerialConfig, sizeof(SERIAL_CONFIG));	//ljb 20180219 add
	m_pSerial[0] =  pPort;
	m_nOvenPortNum = nOvenPortNum; //ksj 20200925 : modbus 에서도 real 포트 번호 체크 할 수 있도록 저장.

	if (nOvenPortNum == 1)
	{
		m_SerialConfig.nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "PortNo", 2);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "StopBit", 1);
		m_SerialConfig.nPortBuffer = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Buffer", 512);
		m_SerialConfig.portParity = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Parity", 'N');
		m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

	}
	else if (nOvenPortNum == 2)
	{
		m_SerialConfig.nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "PortNo", 3);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "StopBit", 1);
		m_SerialConfig.nPortBuffer = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Buffer", 512);
		m_SerialConfig.portParity = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Parity", 'N');
		m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
	}
	else if (nOvenPortNum == 11 ||
		nOvenPortNum == 12 ||
		nOvenPortNum == 13 ||
		nOvenPortNum == 14 ||
		nOvenPortNum == 1 ||
		nOvenPortNum == 2)
	{
		CString strKey=_T("");
		if(nOvenPortNum==11)      strKey=REG_SERIAL_CHILLER1;
		else if(nOvenPortNum==12) strKey=REG_SERIAL_CHILLER2;
		else if(nOvenPortNum==13) strKey=REG_SERIAL_CHILLER3;
		else if(nOvenPortNum==14) strKey=REG_SERIAL_CHILLER4;
		else if(nOvenPortNum==1) strKey=REG_SERIAL_CONFIG3;
		else if(nOvenPortNum==2) strKey=REG_SERIAL_CONFIG4;

		m_SerialConfig.nPortNum      = AfxGetApp()->GetProfileInt(strKey, "PortNo", 3);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(strKey, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(strKey, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(strKey, "StopBit", 1);
		m_SerialConfig.nPortBuffer   = AfxGetApp()->GetProfileInt(strKey, "Buffer", 512);
		m_SerialConfig.portParity    = AfxGetApp()->GetProfileInt(strKey, "Parity", 'N');
		m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
	}

	if(pPort->IsPortOpen())	//포트가 열려있으면 닫는다.
	{
		//	m_Serial[i].StopMonitoring();
		pPort->DisConnect();
		Sleep(1000);
	}

// 	BOOL flag = pPort->InitPort(AfxGetApp()->m_pMainWnd->m_hWnd,
// 		m_SerialConfig.nPortNum, 
// 		m_SerialConfig.nPortBaudRate,
// 		m_SerialConfig.portParity,
// 		m_SerialConfig.nPortDataBits,
// 		m_SerialConfig.nPortStopBits,
// 		m_SerialConfig.nPortEvents,
// 		m_SerialConfig.nPortBuffer);
// 
// 	if( flag == FALSE )
// 	{
// 		CString strErrorMsg;
// 		strErrorMsg.Format("COM %d 포트 초기화를 실패하였습니다!", m_SerialConfig.nPortNum);
// 		AfxMessageBox(strErrorMsg);
// 		return FALSE;
// 	}

	CString str ;
	str.Format(_T("COM%d"), m_SerialConfig.nPortNum) ;
	if(m_ctx != NULL)
	{
		m_ctx = NULL;
		//modbus_free(m_ctx);
		//modbus_close(m_ctx);
	}	

	m_ctx = modbus_new_rtu(str, m_SerialConfig.nPortBaudRate, 'N', m_SerialConfig.nPortDataBits, m_SerialConfig.nPortStopBits) ;

	m_pCS = new CRITICAL_SECTION ;
	InitializeCriticalSection(m_pCS);

	EnterCriticalSection(m_pCS);
	if(m_ctx != NULL)
	{
		//modbus_set_response_timeout(m_ctx,1,0); //ksj 20201019 :타임아웃 지정.
		modbus_set_slave(m_ctx, 1);
	}
	LeaveCriticalSection(m_pCS);

	if(modbus_connect(m_ctx) != -1)
	{
	}
	else
	{
		CString strErrorMsg;
		//strErrorMsg.Format("COM %d 포트 초기화를 실패하였습니다!", m_SerialConfig.nPortNum);
		strErrorMsg.Format(Fun_FindMsg("OvenCtrl_InitSerialPort_msg1","OVENCTRL"), m_SerialConfig.nPortNum);//&&
// 		AfxMessageBox(strErrorMsg);
// 		AfxMessageBox("Modbus comm. Initializing fail");
		GLog::log_All("OvenCtrl","State","Modbus comm. Initializing fail" +strErrorMsg); //lyj 20210809
		return FALSE;
	}

	int mod_len = 10;
	float ch1NowStat  = -1.0 ;
	int16_t* registers;
	registers = (int16_t*) malloc(mod_len * sizeof(int16_t));


	EnterCriticalSection(m_pCS) ;
	int nErrorNo = 0; //ksj 20201018 : 에러코드 백업
	int rc = modbus_read_registers(m_ctx, 0, mod_len, (uint16_t*)registers);
	nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
	LeaveCriticalSection(m_pCS) ;
	Sleep(100) ;
	if (rc != mod_len)
	{
		//ksj 20201017 : 로그추가
		CString strLog;
		strLog.Format("InitSerialPort_Modbus::modbus_read_registers port:%d rc(%d) Error:%d,%d(%s)",m_nOvenPortNum, rc, errno, nErrorNo, modbus_strerror (nErrorNo));
		GLog::log_All("OvenCtrl","error",strLog);
		//ksj end

		// 잠시 케이블이 빠져있을 수 있다.

		// 오류카운트 증가 후 종료여부 판단해야함
		//			ASSERT(FALSE) ;
		//		--------------------- Error ----------------------
		TRACE(_T("FAILED (nb points %d), len = %d\n"), rc, mod_len);
		TRACE(_T("failed: %s\n"), CString(modbus_strerror(errno)));

		ch1NowStat = -1 ;
	}
	else
	{
		// 현재 챔버 상태
		// 운전 관련 상태정보	
		//0 PROGRAM RUN(프로그램 운전)
		//1 FIX RUN 정치 운전  
		//2 HOLD 일시정지  
		//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
		//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
		//7 RESET 정지상태 
		ch1NowStat  = registers[OP_ST_R_CH1_NEX1000_1100] / 10. ;
		if(ch1NowStat> -1.0)
		{
			pPort->StartMonitoring();
			m_bPortOpen_Modbus = TRUE;
			for(int i = 0 ; i < m_nInstallCount; i++)
			{
				m_OvenData[i].m_nDisconnectCnt = 0;
			}
		}
	}
	//BOOL bOpen_pPort = pPort->IsPortOpen();
	//BOOL bOpen = m_pSerial[0]->IsPortOpen();
	//modbus_close(m_ctx);
	//modbus_free(m_ctx);
	delete registers ;
	return TRUE;
}

BOOL COvenCtrl::onPumpInitSerialPort(int nOvenPortNum, int nCommType) //yulee 20190614_*
{
	if(nCommType == CHAMBER_MODBUS_NEX_START ||
		nCommType == CHILLER_TEMP2520)
	{
		//---------------------------------------	
		if(!m_hThreadPump)
		{
			DWORD hThreadId=0;
			m_hThreadPump = CreateThread(NULL, 0, PumpDealThread,
				(LPVOID)this, 0, &hThreadId);	
		}
	}
	else
	{
		SERIAL_CONFIG SerialConfig;
		ZeroMemory(&SerialConfig, sizeof(SERIAL_CONFIG));

		if (nOvenPortNum == 11 ||
			nOvenPortNum == 12 ||
			nOvenPortNum == 13 ||
			nOvenPortNum == 14)
		{
			CString strKey=_T("");
			if(nOvenPortNum==11)      strKey=REG_SERIAL_PUMP1;
			else if(nOvenPortNum==12) strKey=REG_SERIAL_PUMP2;
			else if(nOvenPortNum==13) strKey=REG_SERIAL_PUMP3;
			else if(nOvenPortNum==14) strKey=REG_SERIAL_PUMP4;
			else return FALSE;

			SerialConfig.nPortNum      = AfxGetApp()->GetProfileInt(strKey, "PortNo", 3);
			SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(strKey, "BaudRate", 9600);
			SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(strKey, "DataBit", 8);
			SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(strKey, "StopBit", 1);
			SerialConfig.nPortBuffer   = AfxGetApp()->GetProfileInt(strKey, "Buffer", 512);
			SerialConfig.portParity    = AfxGetApp()->GetProfileInt(strKey, "Parity", 'N');
			SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
		}

		if(m_pPumpSerial.IsPortOpen())
		{
			m_pPumpSerial.DisConnect();
			Sleep(1000);
		}	
		BOOL flag = m_pPumpSerial.InitPort(AfxGetApp()->m_pMainWnd->m_hWnd,
			SerialConfig.nPortNum, 
			SerialConfig.nPortBaudRate,
			SerialConfig.portParity,
			SerialConfig.nPortDataBits,
			SerialConfig.nPortStopBits,
			SerialConfig.nPortEvents,
			SerialConfig.nPortBuffer);
		if( flag == FALSE )
		{
		CString strErrorMsg;
		//strErrorMsg.Format("COM %d 포트 초기화를 실패하였습니다!", SerialConfig.nPortNum);
		strErrorMsg.Format(Fun_FindMsg("OvenCtrl_onPumpInitSerialPort_msg1","OVENCTRL"), SerialConfig.nPortNum);//&&
		//AfxMessageBox(strErrorMsg);
		GLog::log_All("OvenCtrl","State", strErrorMsg); //lyj 20210809
		return FALSE;
	}
	//---------------------------------------	
	m_iPumpDeviceID=1;
	memset(&m_PumpState,0,sizeof(m_PumpState));		
	m_PumpState.nPlay=-1;
	m_PumpState.nMode=-1;	
	//---------------------------------------	
	if(!m_hThreadPump)
	{
		DWORD hThreadId=0;
		m_hThreadPump = CreateThread(NULL, 0, PumpDealThread,
			                    (LPVOID)this, 0, &hThreadId);	
	}
	char delemit='\n';
	m_pPumpSerial.StartMonitoring(&delemit,1);
	return TRUE;
	}
}

BOOL COvenCtrl::CloseSerialPort(int nOvenPortNum)
{
	CSerialPort *pPort= NULL;
	pPort = new CSerialPort();
	pPort = m_pSerial[0];

 	//if (nOvenPortNum == 1)
 	//{
 	//	pPort = m_pSerial[0];	
 	//}
 	//else if (nOvenPortNum == 2)
 	//{
 	//}

	if(pPort->IsPortOpen())	
	{//포트가 열려있으면 닫는다.
		//m_Serial[i].StopMonitoring();

		pPort->DisConnect();
		Sleep(1000);
	}
	m_bPortOpen = FALSE;
	m_bPortOpen_Modbus = FALSE;
	modbus_close(m_ctx);
	modbus_free(m_ctx);

	return TRUE;
}

// void COvenCtrl::LoadSerilaConfig()
// {
// 	CString strConfigName;
// 	CString strParity;
// 
// 	for(int i = 0 ; i < m_nInstallCount; i++)
// 	{
// 		ZeroMemory(&m_SerialConfig[i], sizeof(SERIAL_CONFIG));
// 		strConfigName.Format("SerialOven%d", i+1);
// 		
// 		m_SerialConfig[i].nPortNum = AfxGetApp()->GetProfileInt(strConfigName, "PortNo", 2);
// 		m_SerialConfig[i].nPortBaudRate = AfxGetApp()->GetProfileInt(strConfigName, "BaudRate", 9600);
// 		m_SerialConfig[i].nPortDataBits = AfxGetApp()->GetProfileInt(strConfigName, "DataBit", 8);
// 		m_SerialConfig[i].nPortStopBits = AfxGetApp()->GetProfileInt(strConfigName, "StopBit", 1);
// 		m_SerialConfig[i].nPortBuffer = AfxGetApp()->GetProfileInt(strConfigName, "Buffer", 512);
// 		m_SerialConfig[i].portParity = AfxGetApp()->GetProfileInt(strConfigName, "Parity", 'N');
// 		
// // 		strParity = AfxGetApp()->GetProfileString(strConfigName, "Parity", "N");
// // 		m_SerialConfig[i].portParity = strParity.Left(1)[0];
// 
// 		m_SerialConfig[i].nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
// 	}
// }


// void COvenCtrl::SaveSerilaConfig()
// {
// 	CString strConfig;
// 	CString strConfigName;
// 	
// 	for(int i = 0 ; i < SerialPortUsedNum; i++)
// 	{
// 		if(m_SerialConfig[i].nPortNum > 0)
// 		{
// 			strConfigName.Format("Serial%d", i);
// 			
// 			strConfig.Format("%d,%d,%c,%d,%d,%d",
// 				m_SerialConfig[i].nPortNum,
// 				m_SerialConfig[i].nPortBaudRate,
// 				m_SerialConfig[i].portParity,
// 				m_SerialConfig[i].nPortDataBits,
// 				m_SerialConfig[i].nPortStopBits,
// 				m_SerialConfig[i].nPortBuffer
// 				);
// 			
// 			AfxGetApp()->WriteProfileString("OvenSetting", strConfigName, strConfig);
// 		}	
// 	}
// }

int COvenCtrl::GetReqOvenReadSPTemperatureCmd(int nDeviceNumber, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'R';
		szCmd[5] = 'C';
		szCmd[6] = 'V';
		szCmd[7] = 0x03;
		szCmd[8] = 0x0d;
		szCmd[9] = 0x0a;

		nRetNum = 10;
		nResLen = 59;
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
		szCmd[0] = 0x02;
		szCmd[1] = '1'+nDeviceNumber;
		szCmd[2] = 'R';
		szCmd[3] = 'R';
		szCmd[4] = 'D';
		szCmd[5] = ',';
		szCmd[6] = '3';
		szCmd[7] = ',';
		szCmd[8] = '0';				//현재 온도 값 Reg
		szCmd[9] = '0';
		szCmd[10] = '0';
		szCmd[11] = '1';
		szCmd[12] = ',';
		szCmd[13] = '0';			//현재 설정온도 값 Reg
		szCmd[14] = '0';
		szCmd[15] = '0';
		szCmd[16] = '2';
		szCmd[17] = ',';
		szCmd[18] = '0';			//Run/Stop 관련 Reg
		szCmd[19] = '1';
		szCmd[20] = '0';
		szCmd[21] = '1';
		szCmd[22] = 0x0d;
		szCmd[23] = 0x0a;

		nRetNum = 24;
		nResLen = 27;
		break;		
	case CHAMBR_WiseCube:
		szCmd[0] = '#';
		szCmd[1] = '0';
		szCmd[2] = '2';
		szCmd[3] = 't';
		szCmd[4] = 's';

		nRetNum = 5;
		nResLen = 11;
		break;
		//20090117 KHS TEMP880 추가
	case CHAMBER_TEMP880:						//Model TEMP880
		{
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'R';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '3';
		szCmd[9] = ',';
		szCmd[10] = '0';				//현재 온도 값 Reg
		szCmd[11] = '0';
		szCmd[12] = '0';
		szCmd[13] = '1';
		szCmd[14] = ',';
		szCmd[15] = '0';			//현재 설정온도 값 Reg
		szCmd[16] = '0';
		szCmd[17] = '0';
		szCmd[18] = '2';
		szCmd[19] = ',';
		szCmd[20] = '0';			//Run/Stop 관련 Reg
		szCmd[21] = '1';
		szCmd[22] = '0';
		szCmd[23] = '1';

		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 24; i++)
		{
			pcl1 += szCmd[i];
		}
		
		
		sprintf(&szCmd[24], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[25], "%X", pcl1&0x000f);
		szCmd[26] = 0x0d;
		szCmd[27] = 0x0a;
		
		nRetNum = 28;
		nResLen = 28;
		}
		break;	
	case CHAMBER_TEMP2300://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '3';
			szCmd[9] = ',';
			szCmd[10] = '0';				//현재 온도 값 Reg
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '3';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '2';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 24; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[24], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[25], "%X", pcl1&0x000f);
			szCmd[26] = 0x0d;
			szCmd[27] = 0x0a;
			
			nRetNum = 28;
			nResLen = 28;
		}
		break;
	case CHAMBER_TEMI2300://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '3';
			szCmd[9] = ',';
			szCmd[10] = '0';				//현재 온도 값 Reg
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '3';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '2';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 24; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[24], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[25], "%X", pcl1&0x000f);
			szCmd[26] = 0x0d;
			szCmd[27] = 0x0a;
			
			nRetNum = 28;
			nResLen = 28;
		}
		break;
	case CHAMBER_TEMP2500://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '3';
			szCmd[9] = ',';
			szCmd[10] = '0';				//현재 온도 값 Reg
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '3';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '2';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 24; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[24], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[25], "%X", pcl1&0x000f);
			szCmd[26] = 0x0d;
			szCmd[27] = 0x0a;
			
			nRetNum = 28;
			nResLen = 28;
		}
		break;
	case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '6';
			szCmd[9] = ',';
			szCmd[10] = '0';			//1. Ch1 현재 온도 값 Reg
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//2. Ch2 현재 온도 값 Reg
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';			//3. ch1 현재 설정 값 Reg
			szCmd[21] = '0';
			szCmd[22] = '0';
			szCmd[23] = '3';
			szCmd[24] = ',';
			szCmd[25] = '0';			//4. ch2 현재 설정 값 Reg
			szCmd[26] = '0';
			szCmd[27] = '0';
			szCmd[28] = '4';			
			szCmd[29] = ',';
			szCmd[30] = '0';			//5. ch1 Run/Stop 운전 상태
			szCmd[31] = '1';
			szCmd[32] = '0';
			szCmd[33] = '2';
			szCmd[34] = ',';
			szCmd[35] = '0';			//6. ch2 Run/Stop 운전 상태
			szCmd[36] = '1';
			szCmd[37] = '0';
			szCmd[38] = '3';

			WORD pcl1 = 0;

			for(int i = 1 ; i < 39; i++)
			{
				pcl1 += szCmd[i];
			}

			sprintf(&szCmd[39], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[40], "%X", pcl1&0x000f);
			szCmd[41] = 0x0d;
			szCmd[42] = 0x0a;

			nRetNum = 43;
			nResLen = 43;
		}
		break;
	case CHAMBER_TEMI2500://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '3';
			szCmd[9] = ',';
			szCmd[10] = '0';				//현재 온도 값 Reg
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '3';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '2';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 24; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[24], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[25], "%X", pcl1&0x000f);
			szCmd[26] = 0x0d;
			szCmd[27] = 0x0a;
			
			nRetNum = 28;
			nResLen = 28;
		}
		break;
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.03.05
	case CHAMBER_TD500:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '0';
			szCmd[3] = '1'+nDeviceNumber;
			szCmd[4] = ',';
			szCmd[5] = 'R';
			szCmd[6] = 'R';
			szCmd[7] = 'D';
			szCmd[8] = ',';
			szCmd[9] = '0';
			szCmd[10] = '3';
			szCmd[11] = ',';
			szCmd[12] = '0';				//현재 온도 값 Reg
			szCmd[13] = '1';
			szCmd[14] = '2';
			szCmd[15] = '6';
			szCmd[16] = ',';
			szCmd[17] = '1';			//현재 설정온도 값 Reg
			szCmd[18] = '0';
			szCmd[19] = '0';
			szCmd[20] = '0';
			szCmd[21] = ',';
			szCmd[22] = '0';			//Run/Stop 관련 Reg
			szCmd[23] = '1';
			szCmd[24] = '5';
			szCmd[25] = '2';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 26; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[26], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[27], "%X", pcl1&0x000f);
			szCmd[28] = 0x0d;
			szCmd[29] = 0x0a;
			
			nRetNum = 30;
			nResLen = 30;
		}
		break;
	case CHILLER_ST590:		//ksj 20200401 : ST590 추가. TEMP880코드 복사. 세부 내용 수정 필요.
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'R';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '3';
			szCmd[9] = ',';
			szCmd[10] = '0';				//현재 온도 값 Reg
			szCmd[11] = '0';
			szCmd[12] = '0';
			szCmd[13] = '1';
			szCmd[14] = ',';
			szCmd[15] = '0';			//현재 설정온도 값 Reg
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '2';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Run/Stop 관련 Reg
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '1';

			WORD pcl1 = 0;

			for(int i = 1 ; i < 24; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[24], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[25], "%X", pcl1&0x000f);
			szCmd[26] = 0x0d;
			szCmd[27] = 0x0a;

			nRetNum = 28;
			nResLen = 28;
		}
		break;	
	// -
	//////////////////////////////////////////////////////////////////////////
	default:break;
	}

	return nRetNum;
}

void COvenCtrl::SetCurrentTempSPDataWiseCube(int nIndex, char szBuffData[])
{
	com_WISECUBEData myData;
	float fData;
//	int nDataIndex = 0;

	//"#08tpSXXXXX" : Temp PV 
	//S = 0(+) or 1(-)
	//eg) +120.00°C = "012000"
    //     -20.00°C = "102000"

	ZeroMemory(myData.bData, sizeof(byte)*5);

	//현재 설정값
	/*if(szBuffData[1] != ' ')
		myData.bData[4] = szBuffData[1];		//온도 PV
	else
		myData.bData[4] = 0;		//온도 PV
	if(szBuffData[2] != ' ')
		myData.bData[3] = szBuffData[2];		//온도 PV
	if(szBuffData[3] != ' ')
		myData.bData[2] = szBuffData[3];		//온도 PV
	if(szBuffData[4] != ' ')
		myData.bData[1] = szBuffData[4];		//온도 PV
	if(szBuffData[5] != ' ')
		myData.bData[0] = szBuffData[5];		//온도 PV
	

	fData = float(myData.lData)/100.0f;*/

	fData = atof(szBuffData+1) /100.0f;

	if(szBuffData[0] == 1)
		fData = fData * -1.0f;

	m_OvenData[nIndex].SetRefTemperature(fData);


}

BOOL COvenCtrl::SetRefData(int nIndex, char szBuffData[])
{
	switch(m_nOvenModelType)
	{
	case CHAMBR_WiseCube:
		SetCurrentTempSPDataWiseCube(nIndex, szBuffData);
		break;
	case CHAMBER_TD500:
		SetCurrentDataTD500(nIndex,szBuffData);
		break;
	case CHAMBER_TH500:					//TH500
		//SetCurrentDataTH500(nIndex, szBuffData);
		break;
	case CHAMBER_TEMI880:
		//SetCurrentDataTEMI880(nIndex, szBuffData);
		break;
	case CHAMBER_TEMP880:
		//20090119 KHS TEMP880 추가
		SetCurrentDataTEMP880(nIndex, szBuffData);
		break;
	case CHAMBER_TEMP2300://lmh 20120524 Temp2500 add
		SetCurrentDataTemp2300(nIndex, szBuffData);
		break;
	case CHAMBER_TEMI2300://ljb 20130704 add
		SetCurrentDataTemi2300(nIndex, szBuffData);
		break;
	case CHAMBER_TEMP2500://lmh 20120524 Temp2500 add
		SetCurrentDataTemp2500(nIndex, szBuffData);
		break;
	case CHAMBER_TEMI2500://ljb 20130704 add
		SetCurrentDataTemi2500(nIndex, szBuffData);
		break;
	case CHILLER_TEMP2520://ljb 20180219 add
		SetCurrentDataChillerTEMP2520(nIndex, szBuffData);
		break;
	//case CHILLER_TEMP2520://ljb 20180219 add
	//	SetCurrentDataChillerTEMP2520(nIndex, szBuffData);
	//	break;
	//case CHILLER_TEMP2520://ljb 20180219 add
	//	SetCurrentDataChillerTEMP2520(nIndex, szBuffData);
	//	break;
	//case CHILLER_TEMP2520://ljb 20180219 add
	//	SetCurrentDataChillerTEMP2520(nIndex, szBuffData);
	//	break;
	case CHILLER_ST590: //ksj 20200401 : ST590 추가. TEMP880 처리 루틴으로 넘김		
		SetCurrentDataTEMP880(nIndex, szBuffData);
		break;
	default:
		break;
	}

	return TRUE;
}

int COvenCtrl::GetReqOvenFixMode(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'W';
		szCmd[5] = 'U';
		szCmd[6] = 'P';
		szCmd[7] = ',';
		szCmd[8] = '0';
		szCmd[9] = '0';
		szCmd[10] = '0';
		szCmd[11] = '1';
		szCmd[12] = ',';
		szCmd[13] = 0x00;	
		szCmd[14] = 0x01;
		szCmd[15] = 0x03;
		szCmd[16] = 0x0d;
		szCmd[17] = 0x0a;

		nRetNum = 18;
		nResLen = 18;

		//////////////////////////////////////////////////////////////////////////
	
		//////////////////////////////////////////////////////////////////////////
		
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
	
		break;		
	case CHAMBR_WiseCube:
		break;
		//20090119 KHS TEMP880 추가
	case CHAMBER_TEMP880:						//Model TEMP880
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '4';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMP2300://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '6';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMI2300://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '6';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;	
	case CHAMBER_TEMP2500://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '6';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;
	case CHILLER_TEMP2520:
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Fix/Prog
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '6';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Fix Set
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			szCmd[19] = ',';
			szCmd[20] = '0';			//Fix/Prog
			szCmd[21] = '1';
			szCmd[22] = '0';
			szCmd[23] = '7';
			szCmd[24] = ',';
			szCmd[25] = '0';			//Fix Set
			szCmd[26] = '0';
			szCmd[27] = '0';
			szCmd[28] = '1';

			WORD pcl1 = 0;

			for(int i = 1 ; i < 29; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[29], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[30], "%X", pcl1&0x000f);

			szCmd[31] = 0x0d;
			szCmd[32] = 0x0a;

			nRetNum = 33;
			nResLen = 13;
		}
		break;
	case CHAMBER_TEMI2500://lmh 20120524 TEMP2500 add
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '6';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		break;	
	case CHILLER_ST590:  //ksj 20200401 : ST590 추가. TEMP880 코드 복사함. 세부 내용 수정 필요.
		{
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';			//Run/Stop 관련 Reg
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '4';
			szCmd[14] = ',';
			szCmd[15] = '0';			//Run Data
			szCmd[16] = '0';
			szCmd[17] = '0';
			szCmd[18] = '1';

			WORD pcl1 = 0;

			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}


			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);

			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;

			nRetNum = 23;
			nResLen = 13;
		}
		break;
	default:break;
	}

	return nRetNum;
}

int COvenCtrl::GetReqOvenSetTSlop(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	WORD wData = WORD((fDataT+100.0f)*100.0f);
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'W';
		szCmd[5] = 'U';
		szCmd[6] = 'P';
		szCmd[7] = ',';
		szCmd[8] = '0';
		szCmd[9] = '0';
		szCmd[10] = '0';
		szCmd[11] = '4';
		szCmd[12] = ',';
		szCmd[13] = HIBYTE(wData);	
		szCmd[14] = LOBYTE(wData);
		szCmd[15] = 0x03;
		szCmd[16] = 0x0d;
		szCmd[17] = 0x0a;

		nRetNum = 18;
		nResLen = 18;

		//////////////////////////////////////////////////////////////////////////
	
		//////////////////////////////////////////////////////////////////////////
		
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
		{//Model TEMI880
		wData = WORD(fDataT*10.0f);	//
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'W';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '1';
		szCmd[9] = ',';
		szCmd[10] = '0';
		szCmd[11] = '1';
		szCmd[12] = '0';
		szCmd[13] = '6';
		szCmd[14] = ',';
		
		sprintf(&szCmd[15], "%02X", HIBYTE(wData));
		sprintf(&szCmd[17], "%02X", LOBYTE(wData));
		
		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 19; i++)
		{
			pcl1 += szCmd[i];
		}

		
		sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[20], "%X", pcl1&0x000f);
		szCmd[21] = 0x0d;
		szCmd[22] = 0x0a;
		
		nRetNum = 23;
		nResLen = 13;
		}
		
	
		break;		
	case CHAMBR_WiseCube:
	
	default:break;
	}

	return nRetNum;
}

int COvenCtrl::GetReqOvenSetHSlop(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;
	int nRetNum = 0;
	WORD wData = WORD((fDataT+100.0f)*100.0f);
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'W';
		szCmd[5] = 'U';
		szCmd[6] = 'P';
		szCmd[7] = ',';
		szCmd[8] = '0';
		szCmd[9] = '0';
		szCmd[10] = '0';
		szCmd[11] = '5';
		szCmd[12] = ',';
		szCmd[13] = HIBYTE(wData);	
		szCmd[14] = LOBYTE(wData);
		szCmd[15] = 0x03;
		szCmd[16] = 0x0d;
		szCmd[17] = 0x0a;

		nRetNum = 18;
		nResLen = 18;

		//////////////////////////////////////////////////////////////////////////
	
		//////////////////////////////////////////////////////////////////////////
		
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
		{//Model TEMI880
		wData = WORD(fDataT*10.0f);	//
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'W';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '1';
		szCmd[9] = ',';
		szCmd[10] = '0';
		szCmd[11] = '1';
		szCmd[12] = '0';
		szCmd[13] = '7';
		szCmd[14] = ',';
		
		sprintf(&szCmd[15], "%02X", HIBYTE(wData));
		sprintf(&szCmd[17], "%02X", LOBYTE(wData));
		
		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 19; i++)
		{
			pcl1 += szCmd[i];
		}

		
		sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[20], "%X", pcl1&0x000f);
		szCmd[21] = 0x0d;
		szCmd[22] = 0x0a;
		
		nRetNum = 23;
		nResLen = 13;
		}
		
		break;		
		
	case CHAMBR_WiseCube:
	
	default:break;
	}

	return nRetNum;
}

int COvenCtrl::GetReqOvenHumidity(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return -1;

	int nRetNum = 0;
	WORD wData;
	switch(m_nOvenModelType)
	{
	case CHAMBER_TH500:						//Model TH500 
		//Read current   value
		wData = WORD((fDataT)*10.0f);
		
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '0';
		szCmd[3] = '1'+nDeviceNumber;
		szCmd[4] = 'W';
		szCmd[5] = 'R';
		szCmd[6] = 'P';
		szCmd[7] = ',';
		szCmd[8] = '0';
		szCmd[9] = '0';
		szCmd[10] = '1';
		szCmd[11] = '3';
		szCmd[12] = ',';
		szCmd[13] = HIBYTE(wData);	//60'Setting
		szCmd[14] = LOBYTE(wData);
		szCmd[15] = 0x03;
		szCmd[16] = 0x0d;
		szCmd[17] = 0x0a;

		nRetNum = 18;
		nResLen = 18;
		break;
	case CHAMBER_TEMI880:						//Model TEMI880
		{//Model TEMI880
		wData = WORD(fDataT*10.0f);	//
		szCmd[0] = 0x02;
		szCmd[1] = '0';
		szCmd[2] = '1'+nDeviceNumber;
		szCmd[3] = 'W';
		szCmd[4] = 'R';
		szCmd[5] = 'D';
		szCmd[6] = ',';
		szCmd[7] = '0';
		szCmd[8] = '1';
		szCmd[9] = ',';
		szCmd[10] = '0';
		szCmd[11] = '1';
		szCmd[12] = '0';
		szCmd[13] = '3';
		szCmd[14] = ',';
		
		sprintf(&szCmd[15], "%02X", HIBYTE(wData));
		sprintf(&szCmd[17], "%02X", LOBYTE(wData));
		
		WORD pcl1 = 0;
		
		for(int i = 1 ; i < 19; i++)
		{
			pcl1 += szCmd[i];
		}

		
		sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
		sprintf(&szCmd[20], "%X", pcl1&0x000f);
		szCmd[21] = 0x0d;
		szCmd[22] = 0x0a;
		
		nRetNum = 23;
		nResLen = 13;
		}
		
		break;		
	case CHAMBR_WiseCube:
		{
			long lDataT = fDataT*100;
			szCmd[0] = '#';
			szCmd[1] = '0';
			szCmd[2] = '8';
			szCmd[3] = 'T';
			szCmd[4] = 'S';
			//HEX 값////////////////////////////////////
			if(lDataT > 0)			
				szCmd[5] = '0';
			else
				szCmd[5] = '1';
//			char szBuff[5] = {0,};

			sprintf(&szCmd[6], "%5ld", lDataT);
			nRetNum = 11;
			nResLen = 0;
	
		}
		
		break;
	case CHAMBER_TEMI2500:						//Model TEMI2500
		{//Model TEMI880
			wData = WORD(fDataT*10.0f);	//
			szCmd[0] = 0x02;
			szCmd[1] = '0';
			szCmd[2] = '1'+nDeviceNumber;
			szCmd[3] = 'W';
			szCmd[4] = 'R';
			szCmd[5] = 'D';
			szCmd[6] = ',';
			szCmd[7] = '0';
			szCmd[8] = '1';
			szCmd[9] = ',';
			szCmd[10] = '0';
			szCmd[11] = '1';
			szCmd[12] = '0';
			szCmd[13] = '3';
			szCmd[14] = ',';
			
			sprintf(&szCmd[15], "%02X", HIBYTE(wData));
			sprintf(&szCmd[17], "%02X", LOBYTE(wData));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szCmd[i];
			}
			
			
			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			szCmd[21] = 0x0d;
			szCmd[22] = 0x0a;
			
			nRetNum = 23;
			nResLen = 13;
		}
		
		break;		
	default:break;
	}

	return nRetNum;
}


long COvenCtrl::StringToHex32(CString strHex)
{
/*	CString strTemp;
	long lReturn = 0;
	if(strHex!="")
	{
		int nTemp;
		char pChar[10];
		char nData = 0;
		
		sprintf(pChar, "%s", strHex);
		
		for(int i = 0 ; i < 4; i++)
		{			
			nData = pChar[i];
			sscanf (&nData, _T("%x"), &nTemp);
			lReturn = lReturn << 4;
			lReturn |= nTemp;
		}
	}
*/
	char szData[128];
	sprintf(szData, "%s", strHex);
	char *szStopString;
	long val1 = strtol(szData, &szStopString, 16 );
	return val1;

}

BOOL COvenCtrl::SetTemperature(int nIndex, int nDevice, float fDataT, int nChillerPump)
{
	int nModelType = GetOvenModelType();
	int nErrorNo = 0; //ksj 20201018 : 에러코드 백업
//  	if(m_nOvenPortNum >= 1 && m_nOvenPortNum <= 2) //lyj 20200226 SP SEND시 OvenOndoSPFactor1,2 레지스트리 참고 //lyj 20200511 레지스트리 추출 원복
//  	{
//  		if(nModelType < CHAMBER_MODBUS_NEX_START) //MODBUS는 하드코딩
//  		{
//  			CString strTemp;
//  			strTemp.Format("OvenOndoSPFactor%d",m_nOvenPortNum);
//  			fDataT = fDataT*(float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, strTemp, 10);
//  		}
//  
//  	}	

	if(nModelType > CHAMBER_MODBUS_NEX_START) //yulee 20190605 추가 
	{
		RequestSetLock(TRUE); //ksj 20201022 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		BOOL bRet = FALSE;
		fDataT *= 10;
		if(nModelType == CHAMBER_MODBUS_NEX1000)
		{
			ASSERT(m_ctx!= NULL) ;
			//CRITICAL_SECTION* m_pCS = new CRITICAL_SECTION ;
			//InitializeCriticalSection(m_pCS);

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, SP_T_SET_W_CH1_NEX1000_1100, fDataT)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100) ;
			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chamber Run Cmd complete.");
			}
		}
		else if(nModelType == CHAMBER_MODBUS_NEX1100)
		{
			ASSERT(m_ctx!= NULL) ;

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, SP_T_SET_W_CH1_NEX1000_1100, fDataT)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100);
			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chiller, Pump Run Cmd complete.");
			}
		}
		else if(nModelType == CHAMBER_MODBUS_NEX1200)
		{
			ASSERT(m_ctx!= NULL) ;

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, SP_T_SET_RW_NEX1200, fDataT)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100);
			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chiller, Pump Run Cmd complete.");
			}
		}

		if(bRet == TRUE)
		{
			TRACE("Run Cmd complete.");
			
			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//ksj 20201018 : 로그 추가
			CString strLog;				
			strLog.Format("port:%d idx:%d,%d SetTempFail ret :%d, %d(%s)",m_nOvenPortNum, nIndex, nDevice, bRet, nErrorNo, modbus_strerror (nErrorNo));
			GLog::log_All("OvenCtrl","error",strLog);
			ReConnect(nIndex,nDevice);
			//ksj end		
						
			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return FALSE;
		}

		SetLineState(nDevice, TRUE);//lmh 20111115 oven 연동 nIndex -> nDevice
		
		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
	}
// #ifdef _DEBUG
// 	else if(1)
// #else
	else if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
//#endif
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[256], szRxBuff[256], szTxBuffpump[256], szRxBuffpump[256];
		ZeroMemory(szTxBuff, sizeof(szTxBuff));
		ZeroMemory(szRxBuff, sizeof(szRxBuff));
		ZeroMemory(szTxBuffpump, sizeof(szTxBuffpump));
		ZeroMemory(szRxBuffpump, sizeof(szRxBuffpump));

		CString strTemp;
		int nRxSize = 0;

		if(nChillerPump == 0)
		{
			int nTxSize = GetReqOvenTemperatureCmd(nDevice, fDataT, szTxBuff, nRxSize);

		TRACE("온도셋팅 : %s\n", szTxBuff);

			if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
			{
				strTemp.Format("챔버%d 온도 설정 %.2f℃ 전송 성공", nIndex+1, fDataT);
				//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);			

				//ksj 20200615 : 칠러 로그 추가
				log_All(_T("all"),_T("cmd_chill"),strTemp);	
				//ksj end

				RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 202011117 : 잠금 푼다.
				return TRUE;
			}
			else
			{
				strTemp.Format("챔버%d 온도 설정 %.2f℃ 전송 실패", nIndex+1, fDataT);
				//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);			

				//ksj 20200615 : 칠러 로그 추가
				log_All(_T("all"),_T("cmd_chill"),strTemp);	
				GLog::log_All("OvenCtrl","error",strTemp);

				ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
				//ksj end
			}
		}
		else if(nChillerPump == 1) //pump LPM 설정
		{
			int nTxSize = GetReqChillerPumpCmd(nDevice, fDataT, szTxBuffpump, nRxSize);

			TRACE("펌프 LPM셋팅 : %s\n", szTxBuffpump);

			if(nModelType == CHILLER_ST590)  //st560 펌프 시리얼 포트에 별도로 명령 전송.
			{
				//if(m_pSerial[nIndex]->WriteToPort(szTxBuffpump, nTxSize, szRxBuffpump, nRxSize))
				if(m_pPumpSerial.WriteToPort(szTxBuffpump, nTxSize, szRxBuffpump, nRxSize))
				{
					strTemp.Format("칠러 펌프%d LPM 설정 %.2f℃ 전송 성공", nIndex+1, fDataT);
					//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);			

					//ksj 20200615 : 칠러 로그 추가
					log_All(_T("all"),_T("cmd_chill"),strTemp);	

					strTemp.Format(_T("WriteToPort : %s, %s"), szTxBuffpump, szRxBuffpump);
					log_All(_T("all"),_T("cmd_chill"),strTemp);	
					//ksj end

					RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 202011117 : 잠금 푼다.
					return TRUE;
				}
				else
				{
					strTemp.Format("칠러 펌프%d LPM 설정 %.2f℃ 전송 실패", nIndex+1, fDataT);
					//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);			

					//ksj 20200615 : 칠러 로그 추가
					log_All(_T("all"),_T("cmd_chill"),strTemp);	

					strTemp.Format(_T("WriteToPort : %s, %s"), szTxBuffpump, szRxBuffpump);
					log_All(_T("all"),_T("cmd_chill"),strTemp);	
					GLog::log_All("OvenCtrl","error",strTemp);

					ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
					//ksj end
				}
			}
			else  //컨트롤러 하나에서 2채널로 온도 펌프 제어시. TEMP2520 등
			{
				if(m_pSerial[nIndex]->WriteToPort(szTxBuffpump, nTxSize, szRxBuffpump, nRxSize))
				//if(m_pPumpSerial.WriteToPort(szTxBuffpump, nTxSize, szRxBuffpump, nRxSize))
				{
					strTemp.Format("칠러 펌프%d LPM 설정 %.2f℃ 전송 성공", nIndex+1, fDataT);
					//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);			

					//ksj 20200615 : 칠러 로그 추가
					log_All(_T("all"),_T("cmd_chill"),strTemp);	

					strTemp.Format(_T("WriteToPort : %s, %s"), szTxBuffpump, szRxBuffpump);
					log_All(_T("all"),_T("cmd_chill"),strTemp);	
					//ksj end

					RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 202011117 : 잠금 푼다.
					return TRUE;
				}
				else
				{
					strTemp.Format("칠러 펌프%d LPM 설정 %.2f℃ 전송 실패", nIndex+1, fDataT);
					//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);			

					//ksj 20200615 : 칠러 로그 추가
					log_All(_T("all"),_T("cmd_chill"),strTemp);	

					strTemp.Format(_T("WriteToPort : %s, %s"), szTxBuffpump, szRxBuffpump);
					log_All(_T("all"),_T("cmd_chill"),strTemp);	
					GLog::log_All("OvenCtrl","error",strTemp);

					ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
					//ksj end
				}
			}
			
		}

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 202011117 : 잠금 푼다.
	}

/*
		int nTxSize = GetReqOvenTemperatureCmd(nDevice, fDataT, szTxBuff, nRxSize);

		TRACE("온도셋팅 : %s\n", szTxBuff);

		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("챔버%d 온도 설정 %.2f℃ 전송 성공", nIndex+1, fDataT);
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);			
			return TRUE;
		}
		else
		{
			strTemp.Format("챔버%d 온도 설정 %.2f℃ 전송 실패", nIndex+1, fDataT);
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);			
		}
	}	
*/
	//TRACE("Port 가 Open 되어 있지 않습니다.");
	TRACE(Fun_FindMsg("OvenCtrl_SetTemperature_msg4","OVENCTRL"));//&&
	
	RequestSetLock(FALSE); //ksj 202011117 : 잠금 푼다.
	return FALSE;
}

BOOL COvenCtrl::SetHumidity(int nIndex, int nDevice, float fDataH)
{
	int nModelType = GetOvenModelType();
	if(nModelType > CHAMBER_MODBUS_NEX_START) //yulee 20190605 추가 
	{
		RequestSetLock(TRUE); //ksj 20201022 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.
		
		BOOL bRet = FALSE;
		fDataH *= 10;
		if(nModelType == CHAMBER_MODBUS_NEX1200)
		{
		 	ASSERT(m_ctx!= NULL) ;
		 
		 	//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
		 	EnterCriticalSection(m_pCS) ;
		 	bRet = (1 == modbus_write_register((modbus_t*)m_ctx, SP_H_SET_RW_NEX1200, fDataH)) ;
		 	Sleep(100);
		 	LeaveCriticalSection(m_pCS) ;
		 	if(bRet == TRUE)
		 	{
		 		TRACE("Chiller, Pump Run Cmd complete.");
		 	}
		}

		if(bRet == TRUE)
		{
			TRACE("Run Cmd complete.");
						
			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//ksj 20201018 : 로그 추가
			CString strLog;				
			strLog.Format("idx:%d,%d SetHumiFail ret :%d, %d(%s)", nIndex, nDevice, bRet, errno, modbus_strerror (errno));
			GLog::log_All("OvenCtrl","error",strLog);
			ReConnect(nIndex,nDevice);
			//ksj end
						
			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return FALSE;
		}

		SetLineState(nDevice, TRUE);//lmh 20111115 oven 연동 nIndex -> nDevice
				
		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
	}
	else if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[32], szRxBuff[32];

		CString strTemp;
		int nRxSize = 0;

		int nTxSize = GetReqOvenHumidity(nDevice, fDataH, szTxBuff, nRxSize);

		//TRACE("습도셋팅 : %s\n", szTxBuff);
		TRACE(Fun_FindMsg("OvenCtrl_SetHumidity_msg1","OVENCTRL"), szTxBuff);//&&
		
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//strTemp.Format("챔버%d 습도 설정 %.2f%% 전송 성공", nIndex+1, fDataH);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetHumidity_msg2","OVENCTRL"), nIndex+1, fDataH);//&&
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			
			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//strTemp.Format("챔버%d 습도 설정 %.2f%% 전송 실패", nIndex+1, fDataH);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetHumidity_msg3","OVENCTRL"), nIndex+1, fDataH);//&&
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);

			GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
			ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
		}

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}

	RequestSetLock(FALSE); //ksj 20201117 : 잠금 푼다.
	return FALSE;
}

BOOL COvenCtrl::SetHSlop(int nIndex, int nDevice, float fDataH)
{
	if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[32], szRxBuff[32];

		CString strTemp;
		int nRxSize = 0;

		int nTxSize = GetReqOvenSetHSlop(nDevice, fDataH, szTxBuff, nRxSize);

		//TRACE("습도 Slop 셋팅 : %s\n", szTxBuff);
		TRACE(Fun_FindMsg("OvenCtrl_SetHSlop_msg1","OVENCTRL"), szTxBuff);//&&
		
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//strTemp.Format("Oven %d 습도 Slop 설정 %.2f％/분 실행 성공", nIndex+1, fDataH);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetHSlop_msg2","OVENCTRL"), nIndex+1, fDataH);//&&
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//strTemp.Format("Oven %d 온도 설정 %.2f℃ 실행 실패", nIndex+1, fDataH);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetHSlop_msg3","OVENCTRL"), nIndex+1, fDataH);//&&
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);

			GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
			ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
		}

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}

	return FALSE;
}

BOOL COvenCtrl::SetTSlop(int nIndex, int nDevice, float fDataT)
{
	if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		
		
		char szTxBuff[32], szRxBuff[32];

		CString strTemp;
		int nRxSize = 0;

		int nTxSize = GetReqOvenSetTSlop(nDevice, fDataT, szTxBuff, nRxSize);

		//TRACE("온도 Slop 셋팅 : %s\n", szTxBuff);
		TRACE("온도 Slop 셋팅 : %s\n", szTxBuff);//&&
		
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//strTemp.Format("Oven %d 온도 Slop 설정 %.2f℃/분 실행 성공", nIndex+1, fDataT);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetTSlop_msg2","OVENCTRL"), nIndex+1, fDataT);//&&
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//strTemp.Format("Oven %d 온도 설정 %.2f℃ 실행 실패", nIndex+1, fDataT);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetTSlop_msg3","OVENCTRL"), nIndex+1, fDataT);//&&
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);

			GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
			ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
		}

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}
	return FALSE;
}

BOOL COvenCtrl::SendRunCmd(int nIndex, int nDevice)
{
	int nModelType = GetOvenModelType();
	int nErrorNo = 0; //ksj 20201018 : 에러코드 백업

	if(nModelType > CHAMBER_MODBUS_NEX_START) //yulee 20190605 추가 
	{
		RequestSetLock(TRUE); //ksj 20201022 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		BOOL bRet = FALSE;
		if(nModelType > CHAMBER_MODBUS_NEX_START)
		{
			ASSERT(m_ctx!= NULL) ;
			//CRITICAL_SECTION* m_pCS = new CRITICAL_SECTION ;
			//InitializeCriticalSection(m_pCS);

			if(nModelType == CHAMBER_MODBUS_NEX1000)
			{
				//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
				EnterCriticalSection(m_pCS) ;
				bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1000_1100, enum_RUN)) ;
				nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
				Sleep(100) ;
				LeaveCriticalSection(m_pCS) ;
				if(bRet == TRUE)
				{
					TRACE("NEX1000 Run Cmd complete.");
				}
			}
			else if(nModelType == CHAMBER_MODBUS_NEX1100)
			{
				ASSERT(m_ctx!= NULL) ;

				//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
				EnterCriticalSection(m_pCS) ;
				bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1000_1100, enum_RUN)) ;
				nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
				Sleep(100);
				LeaveCriticalSection(m_pCS) ;
				Sleep(100);
				// 			EnterCriticalSection(m_pCS) ;
				// 			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1100, enum_RUN)) ;
				// 			Sleep(100) ;
				// 			LeaveCriticalSection(m_pCS) ;
				if(bRet == TRUE)
				{
					TRACE("NEX1100 Run Cmd complete.");
				}
			}
			else if (CHAMBER_MODBUS_NEX1200)
			{
				//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
				EnterCriticalSection(m_pCS) ;
				bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1200, enum_RUN)) ;
				nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
				Sleep(100) ;
				LeaveCriticalSection(m_pCS) ;
				if(bRet == TRUE)
				{
					TRACE("NEX1200 Run Cmd complete.");
				}
			}

		}

		if(bRet == TRUE)
		{
			TRACE("Chamber Run Cmd complete.");

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//ksj 20201018 : 로그 추가
			CString strLog;				
			strLog.Format("idx:%d,%d RunFail ret :%d, %d(%s)", nIndex, nDevice, bRet, nErrorNo, modbus_strerror (nErrorNo));
			GLog::log_All("OvenCtrl","error",strLog);
			ReConnect(nIndex,nDevice);
			//ksj end

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return FALSE;
		}

		SetLineState(nDevice, TRUE);//lmh 20111115 oven 연동 nIndex -> nDevice
		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
	}
	else if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[32], szRxBuff[1024];
		ZeroMemory(szTxBuff, sizeof(szTxBuff));

		int nRxSize = 0;
		int nTxSize = GetReqRunCmd(nDevice, szTxBuff, nRxSize);
		
		m_nRxBuffCnt = 0;
		//WriteSysLog(szTxBuff, CT_LOG_LEVEL_DEBUG);

		//20081103
		//현재 운영 중인 Step 번호를 Reset 한다.
		//////////////////////////////////////////////////////////////////////////
		SetRunStepNo(nIndex, 0);
		
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//응답은 OnCommunicaiton에서 처리 
			SetLineState(nDevice, TRUE);//lmh 20111115 oven 연동 nIndex -> nDevice

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return TRUE;
		}

		CString strLog;				
		strLog.Format("idx:%d,%d RunFail", nIndex, nDevice);
		GLog::log_All("OvenCtrl","error",strLog);
		ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}
	return FALSE;
}

BOOL COvenCtrl::SendStopCmd(int nIndex, int nDevice)
{
	int nModelType = GetOvenModelType(); //yulee 20190614_*
	int nErrorNo = 0; //ksj 20201018 : 에러코드 백업

	if(nModelType > CHAMBER_MODBUS_NEX_START) //yulee 20190605 추가 
	{
		RequestSetLock(TRUE); //ksj 20201022 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		BOOL bRet = FALSE;
		if(nModelType == CHAMBER_MODBUS_NEX1000)
		{
			ASSERT(m_ctx!= NULL) ;
			//CRITICAL_SECTION* m_pCS = new CRITICAL_SECTION ;
			//InitializeCriticalSection(m_pCS);

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1000_1100, enum_STOP)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100) ;
			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chamber Run Cmd complete.");
			}
		}
		else if(nModelType == CHAMBER_MODBUS_NEX1100)
		{
			ASSERT(m_ctx!= NULL) ;

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1000_1100, enum_STOP)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100);
			LeaveCriticalSection(m_pCS) ;
			//Sleep(100);
			// 			EnterCriticalSection(m_pCS) ;
			// 			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1100, enum_RUN)) ;
			// 			Sleep(100) ;
			// 			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chiller, Pump Stop Cmd complete.");
			}
		}
		else if(nModelType == CHAMBER_MODBUS_NEX1200)
		{
			ASSERT(m_ctx!= NULL) ;

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1200, enum_STOP)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100);
			LeaveCriticalSection(m_pCS) ;
			//Sleep(100);
			// 			EnterCriticalSection(m_pCS) ;
			// 			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_RUN_W_NEX1100, enum_RUN)) ;
			// 			Sleep(100) ;
			// 			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Himidity Run Cmd complete.");
			}
		}

		if(bRet == TRUE)
		{
			TRACE("Run Cmd complete.");

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//ksj 20201018 : 로그 추가
			CString strLog;				
			strLog.Format("port:%d,idx:%d,%d StopFail ret :%d, %d(%s)", m_nOvenPortNum, nIndex, nDevice, bRet, nErrorNo, modbus_strerror (nErrorNo));
			GLog::log_All("OvenCtrl","error",strLog);
			ReConnect(nIndex,nDevice);
			//ksj end

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return FALSE;
		}

		SetLineState(nDevice, TRUE);//lmh 20111115 oven 연동 nIndex -> nDevice
		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
	}
	else if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[32], szRxBuff[1024];

		int nRxSize = 0;
		int nTxSize = GetReqStopCmd(nDevice, szTxBuff, nRxSize);

		m_nRxBuffCnt = 0;
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//응답은 OnCommunicaiton에서 처리 
			SetLineState(nDevice, TRUE);

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return TRUE;
		}	

		CString strLog;				
		strLog.Format("idx:%d,%d StopFail", nIndex, nDevice);
		GLog::log_All("OvenCtrl","error",strLog);
		ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}
	return FALSE;
}

BOOL COvenCtrl::SetFixPumpMode(int nIndex, int nDevice, BOOL bSet) //yulee 20191025 temp2520 
{
	int nModelType = GetOvenModelType();
	int nTxSize = 0;
	int nRxSize = 0;
	CString strTemp;
	if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[50], szRxBuff[50];		
		ZeroMemory(szTxBuff,sizeof(szTxBuff));
		ZeroMemory(szRxBuff,sizeof(szTxBuff));

		if(m_nOvenModelType == CHILLER_TEMP2520)	//Model Type TEMP2500 //lmh 20120524 TEMP2500 add
		{
			//D0107		-> 0 은 PROG Mode, 1 = FIX Mode //임시 1번 채널만 yulee 20190207 (Ch2 -> Tem2520의 펌프)
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '1';			// Write DATA = 01
			szTxBuff[9] = ',';
			szTxBuff[10] = '0';			//MODE
			szTxBuff[11] = '1';
			szTxBuff[12] = '0';
			szTxBuff[13] = '7';
			szTxBuff[14] = ',';
			szTxBuff[15] = '0';			//MODE DATA
			szTxBuff[16] = '0';
			szTxBuff[17] = '0';
			if(bSet)					//FALSE = PROG, TRUE = FIX
			{
				szTxBuff[18] = '1';
			}
			else
			{
				szTxBuff[18] = '0';
			}					
			WORD pcl1 = 0;

			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szTxBuff[i];
			}

			sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
			szTxBuff[21] = 0x0d;
			szTxBuff[22] = 0x0a;

			nTxSize = 23;
			nRxSize = 23;
		}


		//-------------- Send Data --------------------------------------------
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 성공\n", nIndex+1);
			if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg1","OVENCTRL"), nIndex+1);//&&
			//else strTemp.Format("Oven %d PROG 모드 운전 전환 성공\n", nIndex+1);//&&
			else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
			TRACE(strTemp);
			//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 실패\n", nIndex+1);
			if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg3","OVENCTRL"), nIndex+1);//&&
			//else strTemp.Format("Oven %d PROG 모드 운전 전환 실패\n", nIndex+1);
			else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
			TRACE(strTemp);
			//pDoc->WriteSysLog(strTemp);
			
			GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
			ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
		}

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}
	return FALSE;
}


BOOL COvenCtrl::SetFixMode(int nIndex, int nDevice, BOOL bSet) //yulee 20190624 add modbus_NEX
{
	int nModelType = GetOvenModelType();
	int nErrorNo = 0; //ksj 20201018 : 에러코드 백업

	if(nModelType > CHAMBER_MODBUS_NEX_START) //yulee 20190605 추가 
	{
		RequestSetLock(TRUE); //ksj 20201022 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		BOOL bRet = FALSE;
		if(nModelType == CHAMBER_MODBUS_NEX1000)
		{
			ASSERT(m_ctx!= NULL) ;
			//CRITICAL_SECTION* m_pCS = new CRITICAL_SECTION ;
			//InitializeCriticalSection(m_pCS);

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_MODESET_RW_CH1_NEX1000_1100, enum_OP_FixMode)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100) ;
			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chamber Fix Cmd complete.");
			}
		}
		else if(nModelType == CHAMBER_MODBUS_NEX1100) // 1Ch 온도와 2Ch 펌프의 동작은 묶여 있으므로 명령어는 NEX1000과 같다?
		{
			ASSERT(m_ctx!= NULL) ;

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_MODESET_RW_CH1_NEX1000_1100, enum_OP_FixMode)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100);
			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chamber Fix Cmd complete.");
			}
		}
		else if(nModelType == CHAMBER_MODBUS_NEX1200)
		{
			ASSERT(m_ctx!= NULL) ;

			//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
			EnterCriticalSection(m_pCS) ;
			bRet = (1 == modbus_write_register((modbus_t*)m_ctx, OP_CMD_MODESET_RW_NEX1200, enum_OP_FixMode)) ;
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
			Sleep(100);
			LeaveCriticalSection(m_pCS) ;
			if(bRet == TRUE)
			{
				TRACE("Chamber Fix Cmd complete.");
			}
		}

		if(bRet == TRUE)
		{
			TRACE("Run Cmd complete.");

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//ksj 20201018 : 로그 추가
			CString strLog;				
			strLog.Format("port:%d idx:%d,%d SetFixModeFail ret :%d, %d(%s)",m_nOvenPortNum, nIndex, nDevice, bRet, errno, modbus_strerror (errno));
			GLog::log_All("OvenCtrl","error",strLog);
			ReConnect(nIndex,nDevice);
			//ksj end

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
			return FALSE;
		}

		SetLineState(nDevice, TRUE);//lmh 20111115 oven 연동 nIndex -> nDevice

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
	}
	else
	{
		int nTxSize = 0;
		int nRxSize = 0;
		CString strTemp;
		if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
		{
			RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

			char szTxBuff[50], szRxBuff[50], szTxBuffChilTemp2520[50], szRxBuffChilTemp2520[50];		
			ZeroMemory(szTxBuff,sizeof(szTxBuff));
			ZeroMemory(szRxBuff,sizeof(szRxBuff));
			ZeroMemory(szTxBuffChilTemp2520,sizeof(szTxBuffChilTemp2520));
			ZeroMemory(szRxBuffChilTemp2520,sizeof(szRxBuffChilTemp2520));
			if(m_nOvenModelType == CHAMBER_TH500)	//TH-500
			{
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '0';
				szTxBuff[3] = '1'+nDevice;
				szTxBuff[4] = 'W';
				szTxBuff[5] = 'U';
				szTxBuff[6] = 'P';
				szTxBuff[7] = ',';
				szTxBuff[8] = '0';
				szTxBuff[9] = '0';
				szTxBuff[10] = '0';
				szTxBuff[11] = '1';
				szTxBuff[12] = ',';
				szTxBuff[13] = 0x00;
				if(bSet)
				{
					szTxBuff[14] = 0x01;
				}
				else
				{
					szTxBuff[14] = 0x00;
				}
				szTxBuff[15] = 0x03;
				szTxBuff[16] = 0x0d;
				szTxBuff[17] = 0x0a;

				nTxSize = 18;
				nRxSize = 18;

				//////////////////////////////////////////////////////////////////////////			
			}
			else if(m_nOvenModelType == CHAMBER_TEMI880)	//Model Type TEMI880
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '1'+nDevice;
				szTxBuff[3] = 'W';
				szTxBuff[4] = 'R';
				szTxBuff[5] = 'D';
				szTxBuff[6] = ',';
				szTxBuff[7] = '0';
				szTxBuff[8] = '1';			// Write DATA = 01
				szTxBuff[9] = ',';
				szTxBuff[10] = '0';			//MODE
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '4';
				szTxBuff[14] = ',';
				szTxBuff[15] = '0';			//MODE DATA
				szTxBuff[16] = '0';
				szTxBuff[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuff[18] = '1';
				}
				else
				{
					szTxBuff[18] = '0';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuff[i];
				}
			
				sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
				szTxBuff[21] = 0x0d;
				szTxBuff[22] = 0x0a;
			
				nTxSize = 23;
	//			nRxSize = 23;
				nRxSize = 13;
			}	
			else if(m_nOvenModelType == CHAMBR_WiseCube)	//Model Type WiseCube
			{
			
			}		
			else if(m_nOvenModelType == CHAMBER_TEMP880)	//Model Type TEMP880
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '1'+nDevice;
				szTxBuff[3] = 'W';
				szTxBuff[4] = 'R';
				szTxBuff[5] = 'D';
				szTxBuff[6] = ',';
				szTxBuff[7] = '0';
				szTxBuff[8] = '1';			// Write DATA = 01
				szTxBuff[9] = ',';
				szTxBuff[10] = '0';			//MODE
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '4';
				szTxBuff[14] = ',';
				szTxBuff[15] = '0';			//MODE DATA
				szTxBuff[16] = '0';
				szTxBuff[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuff[18] = '1';
				}
				else
				{
					szTxBuff[18] = '0';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuff[i];
				}
			
				sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
				szTxBuff[21] = 0x0d;
				szTxBuff[22] = 0x0a;
			
				nTxSize = 23;
				nRxSize = 23;
			}
			else if(m_nOvenModelType == CHAMBER_TEMP2300)	//Model Type TEMP2300 //lmh 20120524 TEMP2500 add
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '1'+nDevice;
				szTxBuff[3] = 'W';
				szTxBuff[4] = 'R';
				szTxBuff[5] = 'D';
				szTxBuff[6] = ',';
				szTxBuff[7] = '0';
				szTxBuff[8] = '1';			// Write DATA = 01
				szTxBuff[9] = ',';
				szTxBuff[10] = '0';			//MODE
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '6';
				szTxBuff[14] = ',';
				szTxBuff[15] = '0';			//MODE DATA
				szTxBuff[16] = '0';
				szTxBuff[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuff[18] = '1';
				}
				else
				{
					szTxBuff[18] = '0';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuff[i];
				}
			
				sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
				szTxBuff[21] = 0x0d;
				szTxBuff[22] = 0x0a;
			
				nTxSize = 23;
				nRxSize = 23;
			}
			else if(m_nOvenModelType == CHAMBER_TEMI2300)	//Model Type TEMI2300 //ljb 20130708
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '1'+nDevice;
				szTxBuff[3] = 'W';
				szTxBuff[4] = 'R';
				szTxBuff[5] = 'D';
				szTxBuff[6] = ',';
				szTxBuff[7] = '0';
				szTxBuff[8] = '1';			// Write DATA = 01
				szTxBuff[9] = ',';
				szTxBuff[10] = '0';			//MODE
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '4';
				szTxBuff[14] = ',';
				szTxBuff[15] = '0';			//MODE DATA
				szTxBuff[16] = '0';
				szTxBuff[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuff[18] = '1';
				}
				else
				{
					szTxBuff[18] = '0';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuff[i];
				}
			
				sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
				szTxBuff[21] = 0x0d;
				szTxBuff[22] = 0x0a;
			
				nTxSize = 23;
				nRxSize = 23;
			}
			else if(m_nOvenModelType == CHAMBER_TEMP2500)	//Model Type TEMP2500 //lmh 20120524 TEMP2500 add
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '1'+nDevice;
				szTxBuff[3] = 'W';
				szTxBuff[4] = 'R';
				szTxBuff[5] = 'D';
				szTxBuff[6] = ',';
				szTxBuff[7] = '0';
				szTxBuff[8] = '1';			// Write DATA = 01
				szTxBuff[9] = ',';
				szTxBuff[10] = '0';			//MODE
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '6';
				szTxBuff[14] = ',';
				szTxBuff[15] = '0';			//MODE DATA
				szTxBuff[16] = '0';
				szTxBuff[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuff[18] = '1';
				}
				else
				{
					szTxBuff[18] = '0';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuff[i];
				}
			
				sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
				szTxBuff[21] = 0x0d;
				szTxBuff[22] = 0x0a;
			
				nTxSize = 23;
				nRxSize = 23;
			}
			else if(m_nOvenModelType == CHILLER_TEMP2520)	//Model Type TEMP2500 //lmh 20120524 TEMP2500 add
			{
				//D0106		-> 0 은 PROG Mode, 1 = FIX Mode //임시 1번 채널만 yulee 20190207
				szTxBuffChilTemp2520[0] = 0x02;
				szTxBuffChilTemp2520[1] = '0';
				szTxBuffChilTemp2520[2] = '1'+nDevice;
				szTxBuffChilTemp2520[3] = 'W';
				szTxBuffChilTemp2520[4] = 'R';
				szTxBuffChilTemp2520[5] = 'D';
				szTxBuffChilTemp2520[6] = ',';
				szTxBuffChilTemp2520[7] = '0';
				szTxBuffChilTemp2520[8] = '1';			// Write DATA = 01
				szTxBuffChilTemp2520[9] = ',';
				szTxBuffChilTemp2520[10] = '0';			//MODE
				szTxBuffChilTemp2520[11] = '1';
				szTxBuffChilTemp2520[12] = '0';
				szTxBuffChilTemp2520[13] = '6';
				szTxBuffChilTemp2520[14] = ',';
				szTxBuffChilTemp2520[15] = '0';			//MODE DATA
				szTxBuffChilTemp2520[16] = '0';
				szTxBuffChilTemp2520[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuffChilTemp2520[18] = '1';
				}
				else
				{
					szTxBuffChilTemp2520[18] = '0';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuffChilTemp2520[i];
				}
			
				sprintf(&szTxBuffChilTemp2520[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuffChilTemp2520[20], "%X", pcl1&0x000f);
				szTxBuffChilTemp2520[21] = 0x0d;
				szTxBuffChilTemp2520[22] = 0x0a;
			
				nTxSize = 23;
				nRxSize = 23;
			}
			else if(m_nOvenModelType == CHAMBER_TEMI2500)	//Model Type TEMI2500 //ljb 20130708
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '1'+nDevice;
				szTxBuff[3] = 'W';
				szTxBuff[4] = 'R';
				szTxBuff[5] = 'D';
				szTxBuff[6] = ',';
				szTxBuff[7] = '0';
				szTxBuff[8] = '1';			// Write DATA = 01
				szTxBuff[9] = ',';
				szTxBuff[10] = '0';			//MODE
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '4';
				szTxBuff[14] = ',';
				szTxBuff[15] = '0';			//MODE DATA
				szTxBuff[16] = '0';
				szTxBuff[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuff[18] = '1';
				}
				else
				{
					szTxBuff[18] = '0';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuff[i];
				}
			
				sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
				szTxBuff[21] = 0x0d;
				szTxBuff[22] = 0x0a;
			
				nTxSize = 23;
				nRxSize = 23;
			}
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.03.05
			else if(m_nOvenModelType == CHAMBER_TD500)	
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '0';
				szTxBuff[3] = '1'+nDevice;
				szTxBuff[4] = ',';	
				szTxBuff[5] = 'W';
				szTxBuff[6] = 'R';
				szTxBuff[7] = 'D';
				szTxBuff[8] = ',';
				szTxBuff[9] = '0';
				szTxBuff[10] = '1';			// Write DATA = 01
				szTxBuff[11] = ',';
				szTxBuff[12] = '1';			//MODE
				szTxBuff[13] = '0';
				szTxBuff[14] = '2';
				szTxBuff[15] = '0';
				szTxBuff[16] = ',';
				szTxBuff[17] = '0';			//MODE DATA
				szTxBuff[18] = '0';
				szTxBuff[19] = '0';
				if(bSet)					//TRUE = PROG, FALSE = FIX
				{
					szTxBuff[20] = '0';
				}
				else
				{
					szTxBuff[20] = '1';
				}					
				WORD pcl1 = 0;
			
				for(int i = 1 ; i < 21; i++)
				{
					pcl1 += szTxBuff[i];
				}
			
				sprintf(&szTxBuff[21], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[22], "%X", pcl1&0x000f);
				szTxBuff[23] = 0x0d;
				szTxBuff[24] = 0x0a;
			
				nTxSize = 25;
				nRxSize = 25;
			}
			// -
			//////////////////////////////////////////////////////////////////////////
			else if(m_nOvenModelType == CHILLER_ST590)	//ksj 20200401 : ST590 추가. TEMP880 코드 복사. 세부 내용 수정 필요.
			{
				//D0104		-> 0 은 PROG Mode, 1 = FIX Mode
				szTxBuff[0] = 0x02;
				szTxBuff[1] = '0';
				szTxBuff[2] = '1'+nDevice;
				szTxBuff[3] = 'W';
				szTxBuff[4] = 'R';
				szTxBuff[5] = 'D';
				szTxBuff[6] = ',';
				szTxBuff[7] = '0';
				szTxBuff[8] = '1';			// Write DATA = 01
				szTxBuff[9] = ',';
				szTxBuff[10] = '0';			//MODE
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '4';
				szTxBuff[14] = ',';
				szTxBuff[15] = '0';			//MODE DATA
				szTxBuff[16] = '0';
				szTxBuff[17] = '0';
				if(bSet)					//FALSE = PROG, TRUE = FIX
				{
					szTxBuff[18] = '1';
				}
				else
				{
					szTxBuff[18] = '0';
				}					
				WORD pcl1 = 0;

				for(int i = 1 ; i < 19; i++)
				{
					pcl1 += szTxBuff[i];
				}

				sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
				sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
				szTxBuff[21] = 0x0d;
				szTxBuff[22] = 0x0a;

				nTxSize = 23;
				nRxSize = 23;
			}
			else
			{
				RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
				return FALSE;
			}


			if(m_nOvenModelType == CHILLER_TEMP2520)//yulee 20191121
			{
				//-------------- Send Data --------------------------------------------
				if(m_pSerial[nIndex]->WriteToPort(szTxBuffChilTemp2520, nTxSize, szRxBuffChilTemp2520, nRxSize))
				{
					//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 성공\n", nIndex+1);
					if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg1","OVENCTRL"), nIndex+1);//&&
					//else strTemp.Format("Oven %d PROG 모드 운전 전환 성공\n", nIndex+1);//&&
					else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
					TRACE(strTemp);
					//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//챔버 Fix 모드 ->	Prog Mode 변환
					if (SetFixPumpMode(0,nDevice,bSet) == FALSE)
					{
						if(bSet == TRUE)
						{
							strTemp = "챔버의 Pump(Temp2520) Fix 모드로 변환 실패!!!";
							TRACE(strTemp);
						}
						else if(bSet == FALSE)
						{
							strTemp = "챔버의 Pump(Temp2520) Prog 모드로 변환 실패!!!";
							TRACE(strTemp);
						}

					}
					else
					{
						strTemp = "챔버의 Pump(Temp2520) Fix(1), Prog(0) 모드로 변환 성공!!!";
						TRACE(strTemp);
					}

					RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
					return TRUE;
				}
				else
				{
					//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 실패\n", nIndex+1);
					if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg3","OVENCTRL"), nIndex+1);//&&
					//else strTemp.Format("Oven %d PROG 모드 운전 전환 실패\n", nIndex+1);
					else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
					TRACE(strTemp);
					//pDoc->WriteSysLog(strTemp);

					GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
					ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
				}
			}
			else
			{
				//-------------- Send Data --------------------------------------------
				if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
				{
					//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 성공\n", nIndex+1);
					if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg1","OVENCTRL"), nIndex+1);//&&
					//else strTemp.Format("Oven %d PROG 모드 운전 전환 성공\n", nIndex+1);//&&
					else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
					TRACE(strTemp);
					//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
					//챔버 Fix 모드 ->	Prog Mode 변환

					RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
					return TRUE;
				}
				else
				{
					//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 실패\n", nIndex+1);
					if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg3","OVENCTRL"), nIndex+1);//&&
					//else strTemp.Format("Oven %d PROG 모드 운전 전환 실패\n", nIndex+1);
					else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
					TRACE(strTemp);
					//pDoc->WriteSysLog(strTemp);

					GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
					ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
				}
			}


// 			//-------------- Send Data --------------------------------------------
// 			if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
// 			{
// 				//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 성공\n", nIndex+1);
// 				if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg1","OVENCTRL"), nIndex+1);//&&
// 				//else strTemp.Format("Oven %d PROG 모드 운전 전환 성공\n", nIndex+1);//&&
// 				else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
// 				TRACE(strTemp);
// 				//pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
// 				//챔버 Fix 모드 ->	Prog Mode 변환
// 				if (SetFixPumpMode(0,nDevice,bSet) == FALSE)
// 				{
// 					if(bSet == TRUE)
// 					{
// 						strTemp = "챔버의 Pump(Temp2520) Fix 모드로 변환 실패!!!";
// 						TRACE(strTemp);
// 					}
// 					else if(bSet == FALSE)
// 					{
// 						strTemp = "챔버의 Pump(Temp2520) Prog 모드로 변환 실패!!!";
// 						TRACE(strTemp);
// 					}
// 
// 				}
// 				else
// 				{
// 					strTemp = "챔버의 Pump(Temp2520) Fix(1), Prog(0) 모드로 변환 성공!!!";
// 					TRACE(strTemp);
// 				}
// 				return TRUE;
// 			}
// 			else
// 			{
// 				//if (bSet) strTemp.Format("Oven %d FIX 모드 운전 전환 실패\n", nIndex+1);
// 				if (bSet) strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg3","OVENCTRL"), nIndex+1);//&&
// 				//else strTemp.Format("Oven %d PROG 모드 운전 전환 실패\n", nIndex+1);
// 				else strTemp.Format(Fun_FindMsg("OvenCtrl_SetFixMode_msg4","OVENCTRL"), nIndex+1);//&&
// 				TRACE(strTemp);
// 				//pDoc->WriteSysLog(strTemp);
// 			}

		}
		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
		return FALSE;
	}
	RequestSetLock(FALSE); //ksj 20201117 : 잠금 푼다.
	return FALSE;
}

BOOL COvenCtrl::SetRunPatternNo(int nIndex, int nDevice, WORD wNumber)
{
	int nTxSize = 0;
	int nRxSize = 0;
	CString strTemp,strPatternNum;
	if (wNumber > 9999 || wNumber < 0) return FALSE;
	strPatternNum.Format("%04d",wNumber);

	if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[32], szRxBuff[32];		
		ZeroMemory(szTxBuff,sizeof(szTxBuff));
		ZeroMemory(szRxBuff,sizeof(szTxBuff));
		if(m_nOvenModelType == CHAMBER_TH500)	//TH-500
		{
		}
		else if(m_nOvenModelType == CHAMBER_TEMI880)	//Model Type TEMI880
		{
			//D0100		-> 0 은 PROG Mode, 1 = FIX Mode
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '1';			// Write DATA = 01
			szTxBuff[9] = ',';
			szTxBuff[10] = '0';			//Pattern Number
			szTxBuff[11] = '1';
			szTxBuff[12] = '0';
			szTxBuff[13] = '0';
			szTxBuff[14] = ',';
			
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wNumber));	//SegNo -> SP DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wNumber));
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
			szTxBuff[21] = 0x0d;
			szTxBuff[22] = 0x0a;
			
			nTxSize = 23;
			nRxSize = 13;
		}	
		else if(m_nOvenModelType == CHAMBR_WiseCube)	//Model Type WiseCube
		{
		}		
		else if(m_nOvenModelType == CHAMBER_TEMP880)	//Model Type TEMP880
		{
			//D0100		-> 0 은 PROG Mode, 1 = FIX Mode
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '1';			// Write DATA = 01
			szTxBuff[9] = ',';
			szTxBuff[10] = '0';			//Pattern Number
			szTxBuff[11] = '1';
			szTxBuff[12] = '0';
			szTxBuff[13] = '0';
			szTxBuff[14] = ',';
			
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wNumber));	//SegNo -> SP DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wNumber));

// 			strTemp = strPatternNum.Mid(0,1);
// 			sprintf(&szTxBuff[15],"%s",strTemp);			//Pattern 번호
// 			strTemp = strPatternNum.Mid(1,1);
// 			sprintf(&szTxBuff[16],"%s",strTemp);			//Pattern 번호
// 			strTemp = strPatternNum.Mid(2,1);
// 			sprintf(&szTxBuff[17],"%s",strTemp);			//Pattern 번호
// 			strTemp = strPatternNum.Mid(3,1);
// 			sprintf(&szTxBuff[18],"%s",strTemp);			//Pattern 번호

			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
			szTxBuff[21] = 0x0d;
			szTxBuff[22] = 0x0a;
			
			nTxSize = 23;
			nRxSize = 13;

// 			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
// 			sprintf(&szCmd[20], "%X", pcl1&0x000f);
// 			szCmd[21] = 0x0d;
// 			szCmd[22] = 0x0a;
			
// 			nRetNum = 23;
// 			nResLen = 13;

			}
		else if(m_nOvenModelType == CHAMBER_TEMP2300 || m_nOvenModelType == CHAMBER_TEMI2300 
			|| m_nOvenModelType == CHAMBER_TEMP2500 || m_nOvenModelType == CHAMBER_TEMI2500)	//Model Type //lmh 20120524 TEMP2500 add
		{
			//D0100		-> 0 은 PROG Mode, 1 = FIX Mode
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '1';			// Write DATA = 01
			szTxBuff[9] = ',';
			szTxBuff[10] = '0';			//Pattern Number
			szTxBuff[11] = '1';
			szTxBuff[12] = '0';
			szTxBuff[13] = '0';
			szTxBuff[14] = ',';
			
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wNumber));	//SegNo -> SP DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wNumber));
			
			// 			strTemp = strPatternNum.Mid(0,1);
			// 			sprintf(&szTxBuff[15],"%s",strTemp);			//Pattern 번호
			// 			strTemp = strPatternNum.Mid(1,1);
			// 			sprintf(&szTxBuff[16],"%s",strTemp);			//Pattern 번호
			// 			strTemp = strPatternNum.Mid(2,1);
			// 			sprintf(&szTxBuff[17],"%s",strTemp);			//Pattern 번호
			// 			strTemp = strPatternNum.Mid(3,1);
			// 			sprintf(&szTxBuff[18],"%s",strTemp);			//Pattern 번호
			
			WORD pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
			szTxBuff[21] = 0x0d;
			szTxBuff[22] = 0x0a;
			
			nTxSize = 23;
			nRxSize = 13;
			
			// 			sprintf(&szCmd[19], "%X", (pcl1&0x00f0) >> 4);
			// 			sprintf(&szCmd[20], "%X", pcl1&0x000f);
			// 			szCmd[21] = 0x0d;
			// 			szCmd[22] = 0x0a;
			
			// 			nRetNum = 23;
			// 			nResLen = 13;
			
			}
		//////////////////////////////////////////////////////////////////////////
		/// + BW KIM 2014.02.26
		else if(m_nOvenModelType == CHAMBER_TD500)	//Model Type WiseCube
		{
		}		
		// - 
		//////////////////////////////////////////////////////////////////////////
		else
		{
			//strTemp = "Oven Model 설정 오류";
			strTemp = Fun_FindMsg("OvenCtrl_SetRunPatternNo_msg1","OVENCTRL");//&&
			TRACE(strTemp);

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return FALSE;
		}
		
		//-------------- Send Data --------------------------------------------
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//strTemp.Format("Oven %d Pattern Number(%d) 설정 성공\n", nIndex+1,wNumber);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetRunPatternNo_msg2","OVENCTRL"), nIndex+1,wNumber);//&&
			TRACE(strTemp);
			//			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return TRUE;
		}
		else
		{
			//strTemp.Format("Oven %d Pattern Number(%d) 설정 실패\n", nIndex+1,wNumber);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetRunPatternNo_msg3","OVENCTRL"), nIndex+1,wNumber);//&&
			TRACE(strTemp);
			//			pDoc->WriteSysLog(strTemp);

			GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
			ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
		}

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}

	RequestSetLock(FALSE); //ksj 20201117 : 잠금 푼다.
	return FALSE;
}

//SegNo 에 데이터 기록
BOOL COvenCtrl::SetStartProgMode(int nIndex, int nDevice, WORD wPatternNo)
{
	int nTxSize = 0;
	int nRxSize = 0;
	CString strTemp;//,strPatternNo, strSegNo;
//	char strData[20];

	WORD	pcl1;		//CheckSum
	
	if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[100], szRxBuff[100];
		ZeroMemory(szTxBuff,sizeof(szTxBuff));
		ZeroMemory(szRxBuff,sizeof(szTxBuff));
		if(m_nOvenModelType == CHAMBER_TH500)	//TH-500
		{
		}
		else if(m_nOvenModelType == CHAMBER_TEMI880)	//Model Type TEMI880
		{
			//패턴 쓰기
			//PROG 모드에서 T.PV 설정은 Pattern 번호, Segment = 0 으로 설정 후 D1616 에 설정(0:SSP,1:S.PV,2:T.PV)
			//D1003			-> 3 기록
			//D1004 -> 1이 될때 까지 대기 (1이면 쓰기 완료)
			
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '5';						// Write DATA = 6
			szTxBuff[9] = ',';
			
			szTxBuff[10] = '1';						//PatternNo  Address
			szTxBuff[11] = '0';
			szTxBuff[12] = '0';
			szTxBuff[13] = '0';
			szTxBuff[14] = ',';
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wPatternNo));	//SegNo -> SP DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wPatternNo));
			
			szTxBuff[19] = ',';
			szTxBuff[20] = '1';						//SegNo Address
			szTxBuff[21] = '0';
			szTxBuff[22] = '0';
			szTxBuff[23] = '2';
			szTxBuff[24] = ',';
			szTxBuff[25] = '0';						//SegNo Address
			szTxBuff[26] = '0';
			szTxBuff[27] = '0';
			szTxBuff[28] = '0';
			
			szTxBuff[29] = ',';
			szTxBuff[30] = '1';								//SSP, T.PV, S.PV 설정
			szTxBuff[31] = '6';
			szTxBuff[32] = '1';
			szTxBuff[33] = '6';
			szTxBuff[34] = ',';
			szTxBuff[35] = '0';								//DATA
			szTxBuff[36] = '0';
			szTxBuff[37] = '0';
			szTxBuff[38] = '2';
			
			szTxBuff[39] = ',';
			szTxBuff[40] = '1';								//PT_REPEAT  //lmh 추가
			szTxBuff[41] = '6';
			szTxBuff[42] = '0';
			szTxBuff[43] = '3';
			szTxBuff[44] = ',';
			szTxBuff[45] = '0';								//횟수
			szTxBuff[46] = '0';
			szTxBuff[47] = '0';
			szTxBuff[48] = '0';
			
			szTxBuff[49] = ',';
			szTxBuff[50] = '1';								//PT E.MODE  //lmh 추가
			szTxBuff[51] = '6';
			szTxBuff[52] = '1';
			szTxBuff[53] = '9';
			szTxBuff[54] = ',';
			szTxBuff[55] = '0';								//0:종료 , 1:홀드 , 2:연속
			szTxBuff[56] = '0';
			szTxBuff[57] = '0';
			szTxBuff[58] = '0';
			
			pcl1 = 0;
			
			for(int i = 1 ; i < 59; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[59], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[60], "%X", pcl1&0x000f);
			szTxBuff[61] = 0x0d;
			szTxBuff[62] = 0x0a;	
			nTxSize = 63;
			nRxSize = 13;
		}	
		else if(m_nOvenModelType == CHAMBR_WiseCube)	//Model Type WiseCube
		{
		}		
		else if(m_nOvenModelType == CHAMBER_TEMP880)	//Model Type TEMP880
		{
			//패턴 쓰기
			//PROG 모드에서 T.PV 설정은 Pattern 번호, Segment = 0 으로 설정 후 D1616 에 설정(0:SSP,1:S.PV,2:T.PV)
			//D1003			-> 3 기록
			//D1004 -> 1이 될때 까지 대기 (1이면 쓰기 완료)

			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '5';						// Write DATA = 6
			szTxBuff[9] = ',';

			szTxBuff[10] = '1';						//PatternNo  Address
			szTxBuff[11] = '0';
			szTxBuff[12] = '0';
			szTxBuff[13] = '0';
			szTxBuff[14] = ',';
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wPatternNo));	//SegNo -> SP DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wPatternNo));

			szTxBuff[19] = ',';
			szTxBuff[20] = '1';						//SegNo Address
			szTxBuff[21] = '0';
			szTxBuff[22] = '0';
			szTxBuff[23] = '2';
			szTxBuff[24] = ',';
			szTxBuff[25] = '0';						//SegNo Address
			szTxBuff[26] = '0';
			szTxBuff[27] = '0';
			szTxBuff[28] = '0';

			szTxBuff[29] = ',';
			szTxBuff[30] = '1';								//SSP, T.PV, S.PV 설정
			szTxBuff[31] = '6';
			szTxBuff[32] = '1';
			szTxBuff[33] = '6';
			szTxBuff[34] = ',';
			szTxBuff[35] = '0';								//DATA
			szTxBuff[36] = '0';
			szTxBuff[37] = '0';
			szTxBuff[38] = '2';

			szTxBuff[39] = ',';
			szTxBuff[40] = '1';								//PT_REPEAT  //lmh 추가
			szTxBuff[41] = '6';
			szTxBuff[42] = '0';
			szTxBuff[43] = '3';
			szTxBuff[44] = ',';
			szTxBuff[45] = '0';								//횟수
			szTxBuff[46] = '0';
			szTxBuff[47] = '0';
			szTxBuff[48] = '0';

			szTxBuff[49] = ',';
			szTxBuff[50] = '1';								//PT E.MODE  //lmh 추가
			szTxBuff[51] = '6';
			szTxBuff[52] = '1';
			szTxBuff[53] = '9';
			szTxBuff[54] = ',';
			szTxBuff[55] = '0';								//0:종료 , 1:홀드 , 2:연속
			szTxBuff[56] = '0';
			szTxBuff[57] = '0';
			szTxBuff[58] = '0';

			pcl1 = 0;
			
			for(int i = 1 ; i < 59; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[59], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[60], "%X", pcl1&0x000f);
			szTxBuff[61] = 0x0d;
			szTxBuff[62] = 0x0a;	
			nTxSize = 63;
			nRxSize = 13;
		}
		else if(m_nOvenModelType == CHAMBER_TEMP2300 || m_nOvenModelType == CHAMBER_TEMI2300 
			|| m_nOvenModelType == CHAMBER_TEMP2500 || m_nOvenModelType == CHAMBER_TEMI2500)	//Model Type TEMP880//lmh 20120524 TEMP2500 add
		{
			//패턴 쓰기
			//PROG 모드에서 T.PV 설정은 Pattern 번호, Segment = 0 으로 설정 후 D1616 에 설정(0:SSP,1:S.PV,2:T.PV)
			//D1003			-> 3 기록
			//D1004 -> 1이 될때 까지 대기 (1이면 쓰기 완료)
			
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '5';						// Write DATA = 6
			szTxBuff[9] = ',';
			
			szTxBuff[10] = '2';						//PatternNo  Address
			szTxBuff[11] = '1';
			szTxBuff[12] = '0';
			szTxBuff[13] = '1';
			szTxBuff[14] = ',';
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wPatternNo));	//SegNo -> SP DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wPatternNo));
			
			szTxBuff[19] = ',';
			szTxBuff[20] = '2';						//SegNo Address
			szTxBuff[21] = '1';
			szTxBuff[22] = '0';
			szTxBuff[23] = '2';
			szTxBuff[24] = ',';
			szTxBuff[25] = '0';						//SegNo Address
			szTxBuff[26] = '0';
			szTxBuff[27] = '0';
			szTxBuff[28] = '0';
			
			szTxBuff[29] = ',';
			szTxBuff[30] = '2';								//SSP, T.PV, S.PV 설정
			szTxBuff[31] = '1';
			szTxBuff[32] = '4';
			szTxBuff[33] = '5';
			szTxBuff[34] = ',';
			szTxBuff[35] = '0';								//DATA
			szTxBuff[36] = '0';
			szTxBuff[37] = '0';
			szTxBuff[38] = '2';
			
			szTxBuff[39] = ',';
			szTxBuff[40] = '2';								//PT_REPEAT  //lmh 추가
			szTxBuff[41] = '1';
			szTxBuff[42] = '5';
			szTxBuff[43] = '0';
			szTxBuff[44] = ',';
			szTxBuff[45] = '0';								//횟수
			szTxBuff[46] = '0';
			szTxBuff[47] = '0';
			szTxBuff[48] = '0';
			
			szTxBuff[49] = ',';
			szTxBuff[50] = '2';								//PT E.MODE  //lmh 추가
			szTxBuff[51] = '1';
			szTxBuff[52] = '5';
			szTxBuff[53] = '1';
			szTxBuff[54] = ',';
			szTxBuff[55] = '0';								//0:종료 , 1:홀드 , 2:연속
			szTxBuff[56] = '0';
			szTxBuff[57] = '0';
			szTxBuff[58] = '0';
			
			pcl1 = 0;
			
			for(int i = 1 ; i < 59; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[59], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[60], "%X", pcl1&0x000f);
			szTxBuff[61] = 0x0d;
			szTxBuff[62] = 0x0a;	
			nTxSize = 63;
			nRxSize = 13;
		}
		else if(m_nOvenModelType == CHAMBER_TD500)	//Model Type WiseCube
		{
		}	
		else
		{
			//strTemp = "Oven Model 설정 오류";
			strTemp = Fun_FindMsg("OvenCtrl_SetStartProgMode_msg1","OVENCTRL");//&&
			TRACE(strTemp);
			
			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return FALSE;
		}
		
		BOOL bSendOk(FALSE);
		//-------------- Send Data --------------------------------------------		
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("Send Data -> %s \n",szTxBuff);
			TRACE(strTemp);
			//strTemp.Format("Oven %d Pattern = %d PROG 시작 모드 설정-> 설정 성공\n", nIndex+1,wPatternNo);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetStartProgMode_msg2","OVENCTRL"), nIndex+1,wPatternNo);//&&
			TRACE(strTemp);
			//			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			bSendOk = TRUE;
			//return TRUE;
		}
		else
		{
			//strTemp.Format("Oven %d Pattern = %d PROG 시작 모드 설정-> 설정 실패\n", nIndex+1,wPatternNo);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetStartProgMode_msg3","OVENCTRL"), nIndex+1,wPatternNo);//&&
			TRACE(strTemp);
			//			pDoc->WriteSysLog(strTemp);
			bSendOk = FALSE;
		}
		if (bSendOk)
		{
			ZeroMemory(szTxBuff,sizeof(szTxBuff));
			ZeroMemory(szRxBuff,sizeof(szTxBuff));
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '1';						// Write DATA = 01
			szTxBuff[9] = ',';
			if(m_nOvenModelType == CHAMBER_TEMP2300 || m_nOvenModelType == CHAMBER_TEMI2300
				|| m_nOvenModelType == CHAMBER_TEMP2500 || m_nOvenModelType == CHAMBER_TEMI2500)//lmh 20120524 TEMP2500 add
			{
				szTxBuff[10] = '2';						//TRIGGER Address
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '7';
			}
			else
			{
				szTxBuff[10] = '1';						//TRIGGER Address
				szTxBuff[11] = '0';
				szTxBuff[12] = '0';
				szTxBuff[13] = '3';
			}
			
			szTxBuff[14] = ',';
			szTxBuff[15] = '0';						//TRIGGER Data
			szTxBuff[16] = '0';
			szTxBuff[17] = '0';
			szTxBuff[18] = '3';
			pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
			szTxBuff[21] = 0x0d;
			szTxBuff[22] = 0x0a;
			
			nTxSize = 23;
			nRxSize = 13;
			if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
			{
				strTemp.Format("Send Data -> %s \n",szTxBuff);
				TRACE(strTemp);
				//strTemp.Format("Oven %d Pattern = %d, Segment = 0 -> Trigger write 성공)\n", nIndex+1,wPatternNo);
				strTemp.Format(Fun_FindMsg("OvenCtrl_SetStartProgMode_msg4","OVENCTRL"), nIndex+1,wPatternNo);//&&
				TRACE(strTemp);
				//			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);

				RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
				return TRUE;
			}
			else
			{
				//strTemp.Format("Oven %d Pattern = %d, Segment = 0 -> Trigger write 실패)\n", nIndex+1,wPatternNo);
				strTemp.Format(Fun_FindMsg("OvenCtrl_SetStartProgMode_msg5","OVENCTRL"), nIndex+1,wPatternNo);//&&
				TRACE(strTemp);
				//			pDoc->WriteSysLog(strTemp);

				GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
				ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
			}
		}//End trigger Send

		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}


	RequestSetLock(FALSE); //ksj 20201117 : 잠금 푼다.
	return FALSE;
}

//SegNo 에 데이터 기록
BOOL COvenCtrl::SetPatternSegNo(int nIndex, int nDevice,  WORD wPatternNo, WORD wSegNo ,float fSP,float fTSP, float fStime)
{
	int nTxSize = 0;
	int nRxSize = 0;
	CString strTemp;//,strPatternNo, strSegNo;
	CString strSetData;

//	float fTemp;
//	int	  iTemp;
	
//	char strData[20];
	WORD	wData;
	WORD	pcl1;		//CheckSum

	if (wPatternNo > 9999 || wPatternNo < 0) return FALSE;
	if (wSegNo > 9999 || wSegNo < 0) return FALSE;
 	if (fStime < 60) fStime = 1;	//1분
	else
		fStime = fStime/60.0f;

	if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		char szTxBuff[100], szRxBuff[100];
		ZeroMemory(szTxBuff,sizeof(szTxBuff));
		ZeroMemory(szRxBuff,sizeof(szTxBuff));
		if(m_nOvenModelType == CHAMBER_TH500)	//TH-500
		{
		}
		else if(m_nOvenModelType == CHAMBER_TEMI880)	//Model Type TEMI880
		{
			//패턴 쓰기
			//D0102		    -> SegNo 기록
			//D1010 ~ D1012 -> SP, TSP, Time 기록
			//D1003			-> 3 기록
			//D1004 -> 1이 될때 까지 대기 (1이면 쓰기 완료)
			
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '5';						// Write DATA = 5
			szTxBuff[9] = ',';

			szTxBuff[10] = '1';						//PatternNo  Address
			szTxBuff[11] = '0';
			szTxBuff[12] = '0';
			szTxBuff[13] = '0';
			szTxBuff[14] = ',';
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wPatternNo));	//PatternNo DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wPatternNo));

			szTxBuff[19] = ',';
			szTxBuff[20] = '1';						//SegNo Address
			szTxBuff[21] = '0';
			szTxBuff[22] = '0';
			szTxBuff[23] = '2';
			szTxBuff[24] = ',';
			sprintf(&szTxBuff[25], "%02X", HIBYTE(wSegNo));	//SegNo -> SP DATA
			sprintf(&szTxBuff[27], "%02X", LOBYTE(wSegNo));

			szTxBuff[29] = ',';
			szTxBuff[30] = '1';								//온도 SP Address
			szTxBuff[31] = '0';
			szTxBuff[32] = '1';
			szTxBuff[33] = '0';
			szTxBuff[34] = ',';
//			wData = WORD(fSP * 10.0f);		//20090120 KHS 100.0f->10.0f로 수정
			strSetData.Format("%.1f",fSP*10.0f);
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			sprintf(&szTxBuff[35], "%02X", HIBYTE(wData));	//SegNo -> SP DATA
			sprintf(&szTxBuff[37], "%02X", LOBYTE(wData));

			szTxBuff[39] = ',';
			szTxBuff[40] = '1';								//습도 SP Address
			szTxBuff[41] = '0';
			szTxBuff[42] = '1';
			szTxBuff[43] = '1';
			szTxBuff[44] = ',';
// 			wData = WORD(fTSP * 10.0f);		//20090120 KHS 100.0f->10.0f로 수정
			strSetData.Format("%.1f",fTSP*10.0f);
			wData = atoi(strSetData);		//ljb 20101228 형변환으로 인한 소숫점 자리 안 바뀌는 문제 해결
			sprintf(&szTxBuff[45], "%02X", HIBYTE(wData));	//SegNo -> SP DATA
			sprintf(&szTxBuff[47], "%02X", LOBYTE(wData));

			szTxBuff[49] = ',';
			szTxBuff[50] = '1';								//TIME Address
			szTxBuff[51] = '0';
			szTxBuff[52] = '1';
			szTxBuff[53] = '2';
			szTxBuff[54] = ',';
			//fTemp = fStime * 10.0f;
			wData = WORD(fStime);		//20090120 KHS 100.0f->10.0f로 수정
			sprintf(&szTxBuff[55], "%02X", HIBYTE(wData));	//SegNo -> SP DATA
			sprintf(&szTxBuff[57], "%02X", LOBYTE(wData));
/*
			szTxBuff[49] = ',';
			szTxBuff[50] = '0';								//Pattern 반복 횟수
			szTxBuff[51] = '0';
			szTxBuff[52] = '3';
			szTxBuff[53] = '2';
			szTxBuff[54] = ',';
			szTxBuff[55] = '0';								//Pattern 반복 횟수 DATA
			szTxBuff[56] = '0';
			szTxBuff[57] = '0';
			szTxBuff[58] = '1';
					
			pcl1 = 0;			
			for(int i = 1 ; i < 59; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[59], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[60], "%X", pcl1&0x000f);
			szTxBuff[61] = 0x0d;
			szTxBuff[62] = 0x0a;
*/						
			pcl1 = 0;			
			for(int i = 1 ; i < 59; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[59], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[60], "%X", pcl1&0x000f);
			szTxBuff[61] = 0x0d;
			szTxBuff[62] = 0x0a;
			nTxSize = 63;
			nRxSize = 13;
		}	
		else if(m_nOvenModelType == CHAMBR_WiseCube	)	//Model Type WiseCube
		{
		}		
		else if(m_nOvenModelType == CHAMBER_TEMP880)	//Model Type TEMP880
		{
			//패턴 쓰기
			//D0102		    -> SegNo 기록
			//D1010 ~ D1012 -> SP, TSP, Time 기록
			//D1003			-> 3 기록
			//D1004 -> 1이 될때 까지 대기 (1이면 쓰기 완료)
			
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '4';						// Write DATA = 6
			szTxBuff[9] = ',';

			szTxBuff[10] = '1';						//PatternNo  Address
			szTxBuff[11] = '0';
			szTxBuff[12] = '0';
			szTxBuff[13] = '0';
			szTxBuff[14] = ',';
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wPatternNo));	//PatternNo DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wPatternNo));

			szTxBuff[19] = ',';
			szTxBuff[20] = '1';						//SegNo Address
			szTxBuff[21] = '0';
			szTxBuff[22] = '0';
			szTxBuff[23] = '2';
			szTxBuff[24] = ',';
			sprintf(&szTxBuff[25], "%02X", HIBYTE(wSegNo));	//SegNo -> SP DATA
			sprintf(&szTxBuff[27], "%02X", LOBYTE(wSegNo));

			szTxBuff[29] = ',';
			szTxBuff[30] = '1';								//SP Address
			szTxBuff[31] = '0';
			szTxBuff[32] = '1';
			szTxBuff[33] = '0';
			szTxBuff[34] = ',';
			wData = WORD(fSP * 10.0f);		//20090120 KHS 100.0f->10.0f로 수정
			sprintf(&szTxBuff[35], "%02X", HIBYTE(wData));	//SegNo -> SP DATA
			sprintf(&szTxBuff[37], "%02X", LOBYTE(wData));

			szTxBuff[39] = ',';
			szTxBuff[40] = '1';								//TIME Address
			szTxBuff[41] = '0';
			szTxBuff[42] = '1';
			szTxBuff[43] = '2';
			szTxBuff[44] = ',';
			//fTemp = fStime * 10.0f;
			wData = WORD(fStime);		//20090120 KHS 100.0f->10.0f로 수정
			sprintf(&szTxBuff[45], "%02X", HIBYTE(wData));	//SegNo -> SP DATA
			sprintf(&szTxBuff[47], "%02X", LOBYTE(wData));
/*
			szTxBuff[49] = ',';
			szTxBuff[50] = '0';								//Pattern 반복 횟수
			szTxBuff[51] = '0';
			szTxBuff[52] = '3';
			szTxBuff[53] = '2';
			szTxBuff[54] = ',';
			szTxBuff[55] = '0';								//Pattern 반복 횟수 DATA
			szTxBuff[56] = '0';
			szTxBuff[57] = '0';
			szTxBuff[58] = '1';
					
			pcl1 = 0;			
			for(int i = 1 ; i < 59; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[59], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[60], "%X", pcl1&0x000f);
			szTxBuff[61] = 0x0d;
			szTxBuff[62] = 0x0a;
*/						
			pcl1 = 0;			
			for(int i = 1 ; i < 49; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[49], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[50], "%X", pcl1&0x000f);
			szTxBuff[51] = 0x0d;
			szTxBuff[52] = 0x0a;
			nTxSize = 53;
			nRxSize = 13;

		}
		else if(m_nOvenModelType == CHAMBER_TEMP2300 || m_nOvenModelType == CHAMBER_TEMI2300
			|| m_nOvenModelType == CHAMBER_TEMP2500 || m_nOvenModelType == CHAMBER_TEMI2500)	//Model Type TEMP880//lmh 20120524 TEMP2500 add
		{
			//패턴 쓰기
			//D0102		    -> SegNo 기록
			//D1010 ~ D1012 -> SP, TSP, Time 기록
			//D1003			-> 3 기록
			//D1004 -> 1이 될때 까지 대기 (1이면 쓰기 완료)
			
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '4';						// Write DATA = 6
			szTxBuff[9] = ',';

			szTxBuff[10] = '2';						//PatternNo  Address
			szTxBuff[11] = '1';
			szTxBuff[12] = '0';
			szTxBuff[13] = '1';
			szTxBuff[14] = ',';
			sprintf(&szTxBuff[15], "%02X", HIBYTE(wPatternNo));	//PatternNo DATA
			sprintf(&szTxBuff[17], "%02X", LOBYTE(wPatternNo));

			szTxBuff[19] = ',';
			szTxBuff[20] = '2';						//SegNo Address
			szTxBuff[21] = '1';
			szTxBuff[22] = '0';
			szTxBuff[23] = '2';
			szTxBuff[24] = ',';
			sprintf(&szTxBuff[25], "%02X", HIBYTE(wSegNo));	//SegNo -> SP DATA
			sprintf(&szTxBuff[27], "%02X", LOBYTE(wSegNo));

			szTxBuff[29] = ',';
			szTxBuff[30] = '2';								//SP Address
			szTxBuff[31] = '1';
			szTxBuff[32] = '2';
			szTxBuff[33] = '6';
			szTxBuff[34] = ',';
			wData = WORD(fSP * 10.0f);		//20090120 KHS 100.0f->10.0f로 수정
			sprintf(&szTxBuff[35], "%02X", HIBYTE(wData));	//SegNo -> SP DATA
			sprintf(&szTxBuff[37], "%02X", LOBYTE(wData));

			szTxBuff[39] = ',';
			szTxBuff[40] = '2';								//TIME Address
			szTxBuff[41] = '1';
			szTxBuff[42] = '2';
			szTxBuff[43] = '7';
			szTxBuff[44] = ',';
			//fTemp = fStime * 10.0f;
			wData = WORD(fStime);		//20090120 KHS 100.0f->10.0f로 수정
			sprintf(&szTxBuff[45], "%02X", HIBYTE(wData));	//SegNo -> SP DATA
			sprintf(&szTxBuff[47], "%02X", LOBYTE(wData));
/*
			szTxBuff[49] = ',';
			szTxBuff[50] = '0';								//Pattern 반복 횟수
			szTxBuff[51] = '0';
			szTxBuff[52] = '3';
			szTxBuff[53] = '2';
			szTxBuff[54] = ',';
			szTxBuff[55] = '0';								//Pattern 반복 횟수 DATA
			szTxBuff[56] = '0';
			szTxBuff[57] = '0';
			szTxBuff[58] = '1';
					
			pcl1 = 0;			
			for(int i = 1 ; i < 59; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[59], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[60], "%X", pcl1&0x000f);
			szTxBuff[61] = 0x0d;
			szTxBuff[62] = 0x0a;
*/						
			pcl1 = 0;			
			for(int i = 1 ; i < 49; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[49], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[50], "%X", pcl1&0x000f);
			szTxBuff[51] = 0x0d;
			szTxBuff[52] = 0x0a;
			nTxSize = 53;
			nRxSize = 13;

		}
		//////////////////////////////////////////////////////////////////////////
		// + BW KIM 2014.03.05
		else if(m_nOvenModelType == CHAMBER_TD500)
		{

		}
		// -
		//////////////////////////////////////////////////////////////////////////
		else
		{
			//strTemp = "Oven Model 설정 오류";
			strTemp = Fun_FindMsg("OvenCtrl_SetPatternSegNo_msg1","OVENCTRL");//&&
			TRACE(strTemp);

			RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
			return FALSE;
		}
		
		BOOL bSendOk(FALSE);
		//-------------- Send Data --------------------------------------------		
		if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("Send Data -> %s \n",szTxBuff);
			TRACE(strTemp);
			//strTemp.Format("Oven %d Pattern = %d, Segment = %d -> 설정 성공(SP:%.2f, TSP:%.2f, sTime:%.2f)\n", nIndex+1,wPatternNo,wSegNo,fSP,fTSP,fStime);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetPatternSegNo_msg2","OVENCTRL"), nIndex+1,wPatternNo,wSegNo,fSP,fTSP,fStime);//&&
			TRACE(strTemp);
			//			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			bSendOk = TRUE;
			//return TRUE;
			
		}
		else
		{
			//strTemp.Format("Oven %d Pattern = %d, Segment = %d -> 설정 실패(SP:%.2f, TSP:%.2f, sTime:%.2f)\n", nIndex+1,wPatternNo,wSegNo,fSP,fTSP,fStime);
			strTemp.Format(Fun_FindMsg("OvenCtrl_SetPatternSegNo_msg3","OVENCTRL"), nIndex+1,wPatternNo,wSegNo,fSP,fTSP,fStime);//&&
			TRACE(strTemp);
			//pDoc->WriteSysLog(strTemp);
			bSendOk = FALSE;
			
		}
		if (bSendOk)
		{
			ZeroMemory(szTxBuff,sizeof(szTxBuff));
			ZeroMemory(szRxBuff,sizeof(szRxBuff));
			szTxBuff[0] = 0x02;
			szTxBuff[1] = '0';
			szTxBuff[2] = '1'+nDevice;
			szTxBuff[3] = 'W';
			szTxBuff[4] = 'R';
			szTxBuff[5] = 'D';
			szTxBuff[6] = ',';
			szTxBuff[7] = '0';
			szTxBuff[8] = '1';						// Write DATA = 01
			szTxBuff[9] = ',';
			if(m_nOvenModelType == CHAMBER_TEMP2300 || m_nOvenModelType == CHAMBER_TEMI2300
				|| m_nOvenModelType == CHAMBER_TEMP2500 || m_nOvenModelType == CHAMBER_TEMI2500)//lmh 20120524 TEMP2500 add
			{
				szTxBuff[10] = '2';						//TRIGGER Address
				szTxBuff[11] = '1';
				szTxBuff[12] = '0';
				szTxBuff[13] = '7';
			}
			else
			{
				szTxBuff[10] = '1';						//TRIGGER Address
				szTxBuff[11] = '0';
				szTxBuff[12] = '0';
				szTxBuff[13] = '3';
			}
			
			szTxBuff[14] = ',';
			szTxBuff[15] = '0';						//TRIGGER Data
			szTxBuff[16] = '0';
			szTxBuff[17] = '0';
			szTxBuff[18] = '3';
			pcl1 = 0;
			
			for(int i = 1 ; i < 19; i++)
			{
				pcl1 += szTxBuff[i];
			}
			
			sprintf(&szTxBuff[19], "%X", (pcl1&0x00f0) >> 4);
			sprintf(&szTxBuff[20], "%X", pcl1&0x000f);
			szTxBuff[21] = 0x0d;
			szTxBuff[22] = 0x0a;
			
			nTxSize = 23;
			nRxSize = 13;
			if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
			{
				strTemp.Format("Send Data -> %s \n",szTxBuff);
				TRACE(strTemp);
				//strTemp.Format("Oven %d Pattern = %d, Segment = %d -> Trigger write 성공)\n", nIndex+1,wPatternNo,wSegNo);
				strTemp.Format(Fun_FindMsg("OvenCtrl_SetPatternSegNo_msg4","OVENCTRL"), nIndex+1,wPatternNo,wSegNo);//&&
				TRACE(strTemp);
				//			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);

				RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
				return TRUE;
			}
			else
			{
				//strTemp.Format("Oven %d Pattern = %d, Segment = %d -> Trigger write 실패)\n", nIndex+1,wPatternNo,wSegNo);
				strTemp.Format(Fun_FindMsg("OvenCtrl_SetPatternSegNo_msg5","OVENCTRL"), nIndex+1,wPatternNo,wSegNo);//&&
				TRACE(strTemp);
				//			pDoc->WriteSysLog(strTemp);

				GLog::log_All("OvenCtrl","error",strTemp); //ksj 20201117 : 로그 추가
				ReConnect(nIndex,nDevice); //ksj 20201117 : 재접속 추가.
			}
		}//End trigger Send
		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
	}

	RequestSetLock(FALSE); //ksj 20201117 : 잠금 푼다.
	return FALSE;
}

int COvenCtrl::RequestCurrentValue(int nIndex, int nDevice, LPVOID pParam)
{
	if(GetOvenModelType() > CHAMBER_MODBUS_NEX_START) //yulee 20190605 추가 
	{
		SetLineState(nDevice, TRUE);
		// 		if(modbus_connect(m_ctx) == -1)
		// 		{
		// 			AfxMessageBox("Modbus comm. Initializing fail");
		// 			return 0;
		// 		}
		if(m_bPortOpen == FALSE)
		{
			//AfxMessageBox("RequestCurrentValue / m_bPortOpen False");

			//ksj 20201017 : 로그추가
			CString strLog;
			strLog.Format("idx:%d PortOpen: %d", nIndex, m_bPortOpen);
			GLog::log_All("OvenCtrl","OvenCtrl",strLog);
			//ksj end

			return 0;
		}

		EnterCriticalSection(m_pCS);
		if(m_ctx != NULL)
		{
			//modbus_set_response_timeout(m_ctx,1,0); //ksj 20201019 : 타임아웃 지정.
			modbus_set_slave(m_ctx, 1);
		}
		LeaveCriticalSection(m_pCS);
		int mod_len = 115, rc = -1;
		int16_t* registers ;
		registers = (int16_t*) malloc(mod_len * sizeof(int16_t));

// 		float PV_temp1	= 0., PV_temp2	= 0., PV_temp3	= 0.;//		
// 		float SP_temp1	= 0., SP_temp2	= 0., SP_temp3	= 0.;//	
// 		int	OPState1	= -1, OPState2	= -1, OPState3	= -1;//			pcss : Run/Stop 관련 Reg 0:STOP 1:RUN
// 		int OPMode1		= 0, OPMode2	= 0, OPMode3	= 0;//			6. 0:PROG, 1:FIX 관련 Reg



// 		float PV_humi	= 0.;
// 		float SP_humi   = 0.;
// 		float PV_Pump	= 0.;
// 		float SP_Pump	= 0.;
//		int	  OPState_Pump	= -1;
//		int	  OPMode_Pump	= 0	;//	

		//InitializeCriticalSection(m_pCS);

		EnterCriticalSection(m_pCS);
		//if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
		//if(m_bPortOpen == TRUE && m_OvenData[nIndex].m_nDisconnectCnt < 5)
		//if(m_bPortOpen == TRUE && m_OvenData[nIndex].m_nDisconnectCnt < 10)
		if(m_bPortOpen == TRUE && m_OvenData[nIndex].m_nDisconnectCnt < 15)
		{
			int nErrorNo = 0; //ksj 20201018 : 에러코드 백업
			rc =modbus_read_registers(m_ctx, 0, mod_len, (uint16_t*)registers);//두번째 인자 채널 번호
			nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.
		
			//Sleep(100);
			Wait(100);//ksj 20201017

			if(rc == -1)
			{
				//ksj 20201017 : 로그추가
				CString strLog;
				strLog.Format("RequestCurrentValue::modbus_read_registers port:%d idx:%d rc(%d) Error:%d,%d(%s)",m_nOvenPortNum, nIndex, rc, errno, nErrorNo, modbus_strerror (nErrorNo));
				GLog::log_All("OvenCtrl","error",strLog);
				//ksj end
			}

			//ksj 20201017 : 로그 및 리커넥트 추가.
			if(m_OvenData[nIndex].m_nDisconnectCnt >= 5) 
			{
				CString strLog;

				int retVal = ReConnect(nIndex, nDevice); //재접속 시도
				strLog.Format("port:%d, idx:%d RequestCurrentValue/DisconnectCnt: %d Try Reconnect ret :%d",m_nOvenPortNum, nIndex, m_OvenData[nIndex].m_nDisconnectCnt, retVal);
				GLog::log_All("OvenCtrl","error",strLog);

			}
		}
		//ksj 20201018
		else if(m_bPortOpen == TRUE && m_OvenData[nIndex].m_nDisconnectCnt >= 15)
		{
			CString strLog;
			strLog.Format("port:%d, idx:%d RequestCurrentValue/DisconnectCnt: %d", m_nOvenPortNum, nIndex, m_OvenData[nIndex].m_nDisconnectCnt);
			GLog::log_All("OvenCtrl","error",strLog);
		}
		

		LeaveCriticalSection(m_pCS);
		if (rc != mod_len)
		{
			// 잠시 케이블이 빠져있을 수 있다.

			// 오류카운트 증가 후 종료여부 판단해야함
			//			ASSERT(FALSE) ;
			//		--------------------- Error ----------------------
			

			//ksj 20201017 : 로그추가
			CString strLog;
			strLog.Format("port:%d, idx:%d rc(%d) != mod_len(%d), ErrModbusCnt: %d, DisconnectCnt: %d, Error:%d(%s)", m_nOvenPortNum, nIndex, rc, mod_len, m_nErrModbusCnt, m_OvenData[nIndex].m_nDisconnectCnt, errno, modbus_strerror (errno));
			GLog::log_All("OvenCtrl","error",strLog);
			//ksj end


			m_nErrModbusCnt++;


			//ksj 20201017 : 로그 및 리커넥트 추가.
			if(m_nErrModbusCnt >= 5) 
			{
				int retVal = ReConnect(nIndex, nDevice); //재접속 시도
				strLog.Format("#### port:%d, idx:%d RequestCurrentValue/ErrModbusCnt: %d Try Reconnect ret :%d",m_nOvenPortNum, nIndex, m_nErrModbusCnt, retVal);
				GLog::log_All("OvenCtrl","error",strLog);
			}

			//if(m_nErrModbusCnt <4)
			if(m_nErrModbusCnt < 10) //ksj 20201017 : 카운트 늘려봄
			{
				//TRACE(_T("FAILED (nb points %d), len = %d\n"), rc, mod_len);
				//TRACE(_T("(%d) : failed: %s(%d)\n"), this, CString(modbus_strerror(errno)));
				//AfxMessageBox("Fail to read value by modbus");//yulee 20190711
				if(GetOvenModelType() == CHAMBER_MODBUS_NEX1000)
				{
					// 운전 관련 상태정보	
					//0 PROGRAM RUN(프로그램 운전)5
					//1 FIX RUN 정치 운전  
					//2 HOLD 일시정지  
					//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
					//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
					//7 RESET 정지상태 

					m_OvenData[nIndex].SetCurTemperature(m_PV_temp1); //현재 온도
					m_OvenData[nIndex].SetRefTemperature(m_SP_temp1); //설정 온도

					if(m_OPState1 != -2) 
					{
						// 		if(OPStateCh1 == 0||OPStateCh1 == 1|| OPStateCh1 == 2||OPStateCh1 == 7)			 //운전 상태
						// 		{
						// 			m_OvenData[nIndex].SetRunState(OPStateCh1);
						// 		}

						if((m_OPState1 == 1)||(m_OPState1 == 2))		 //운전 상태
						{
							m_OvenData[nIndex].SetRunState(1);
						}
						else
						{
							m_OvenData[nIndex].SetRunState(0);
						}

					}
					if(m_OPMode1 != -2)		//Fix(정치)/프로그램(패턴)	
					{
						if(m_OPMode1 == 1)
							m_OvenData[nIndex].SetRunWorkMode(1);
						else
							m_OvenData[nIndex].SetRunWorkMode(0);			
					}
				}
				else if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100)
				{
					// 운전 관련 상태정보	
					//0 PROGRAM RUN(프로그램 운전)5
					//1 FIX RUN 정치 운전  
					//2 HOLD 일시정지  
					//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
					//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
					//7 RESET 정지상태 

					m_OvenData[nIndex].SetCurTemperature(m_PV_temp2); //현재 온도
					m_OvenData[nIndex].SetRefTemperature(m_SP_temp2); //설정 온도

					if(m_OPState2 != -2) 
					{
						// 		if(OPStateCh1 == 0||OPStateCh1 == 1|| OPStateCh1 == 2||OPStateCh1 == 7)			 //운전 상태
						// 		{
						// 			m_OvenData[nIndex].SetRunState(OPStateCh1);
						// 		}

						if((m_OPState2 == 1)||(m_OPState2 == 2))		 //운전 상태
						{
							m_OvenData[nIndex].SetRunState(1);
						}
						else
						{
							m_OvenData[nIndex].SetRunState(0);
						}

					}
					if(m_OPMode2 != -2)		//Fix(정치)/프로그램(패턴)	
					{
						if(m_OPMode2 == 1)
							m_OvenData[nIndex].SetRunWorkMode(1);
						else
							m_OvenData[nIndex].SetRunWorkMode(0);			
					}

					m_PumpState.nSP = m_SP_Pump;							//현재 펌프 L/M //yulee 20190613
					m_PumpState.nPV = m_PV_Pump;							//설정 펌프 L/M //yulee	20190613
					m_PumpState.fSP = m_SP_Pump;							//lyj 20201105 NEX1100칠러 소숫점
					m_PumpState.fPV = m_PV_Pump;							//lyj 20201105 NEX1100칠러 소숫점

					if(m_OPMode_Pump != -2)
					{
						//		if(OPModePump == 1)
						m_PumpState.nPlay = ~m_OPMode_Pump;								//펌프 워크 모드			
						// 		else //동작 중 튜닝 상태 //우선 동작으로 처리
						// 			m_OvenData[nIndex].SetRunWorkMode(1);
					}
				}
				else if(GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
				{
					// 운전 관련 상태정보	
					//0 PROGRAM RUN(프로그램 운전)5
					//1 FIX RUN 정치 운전  
					//2 HOLD 일시정지  
					//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
					//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
					//7 RESET 정지상태 

					m_OvenData[nIndex].SetCurTemperature(m_PV_temp3); //현재 온도
					m_OvenData[nIndex].SetRefTemperature(m_SP_temp3); //설정 온도

					if(m_OPState3 != -2) 
					{
						// 		if(OPStateCh1 == 0||OPStateCh1 == 1|| OPStateCh1 == 2||OPStateCh1 == 7)			 //운전 상태
						// 		{
						// 			m_OvenData[nIndex].SetRunState(OPStateCh1);
						// 		}

						if((m_OPState3 == 1)||(m_OPState3 == 2))		 //운전 상태
						{
							m_OvenData[nIndex].SetRunState(1);
						}
						else
						{
							m_OvenData[nIndex].SetRunState(0);
						}

					}
					if(m_OPMode3 != -2)		//Fix(정치)/프로그램(패턴)	
					{
						if(m_OPMode3 == 1)
							m_OvenData[nIndex].SetRunWorkMode(1);
						else
							m_OvenData[nIndex].SetRunWorkMode(0);			
					}


					m_OvenData[nIndex].SetCurHumidity(m_PV_humi);		//현재 습도
					m_OvenData[nIndex].SetRefHumidity(m_SP_humi);		//설정 습도
				}
			}

			delete registers; //yulee 20190723 //ksj 20200129 : v1013 코드 발췌
			return FALSE;
		}
		else
		{

			m_nErrModbusCnt = 0;//yulee 20190723 //ksj 20200129 : v1013 코드 발췌
			SetLineState(nDevice, TRUE);
			//TRACE(_T("(%d) : Success (nb points %d), len = %d\n"), this, rc, mod_len);

			int nCntNoValue  = 0;
			for(int i = 0; i< mod_len; i++)
			{
				if(registers[i] == 0)
				{
					nCntNoValue++;
				}
			}

			if(nCntNoValue == mod_len)
			{
				return 0;
			}


			if(GetOvenModelType() == CHAMBER_MODBUS_NEX1000)
			{
				// 현재 설정온도
				m_PV_temp1		= registers[PV_T_R_CH1_NEX1000_1100] / 10.0;				// 현재온도					td_temp  = registers[TD_TEMP] / 10.;				
				m_SP_temp1		= registers[SP_T_R_CH1_NEX1000_1100] / 10.0;				// 설정온도					h_csp = registers[H_CSP] / 10. ;
				m_OPState1	= registers[OP_ST_R_CH1_NEX1000_1100];	// Run Stop 정보				pcss :Run/Stop 관련 Reg 0:STOP 1:프로그 모드 RUN, 2:정치 모드 RUN 
				m_OPMode1		= registers[OP_CMD_MODESET_RW_CH1_NEX1000_1100] ;					// 6. 0:PROG, 1:FIX 관련 Reg
			}
			else if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100)
			{	
				m_PV_temp2		= registers[PV_T_R_CH1_NEX1000_1100] / 10.0;				// 현재온도					td_temp  = registers[TD_TEMP] / 10.;				
				m_SP_temp2		= registers[SP_T_R_CH1_NEX1000_1100] / 10.0;				// 설정온도					h_csp = registers[H_CSP] / 10. ;
				m_OPState2		= registers[OP_ST_R_CH1_NEX1000_1100];	// Run Stop 정보				pcss :Run/Stop 관련 Reg 0:STOP 1:프로그 모드 RUN, 2:정치 모드 RUN 
				m_OPMode2		= (registers[OP_CMD_MODESET_RW_CH1_NEX1000_1100] == 0) ? 0 : 1 ;					// 6. 0:PROG, 1:FIX 관련 Reg
				m_PV_Pump		= registers[PV_T_R_CH2_NEX1100]/10.0;				// 현재온도					td_temp  = registers[TD_TEMP] / 10.;				
				m_SP_Pump		= registers[SP_T_R_CH2_NEX1100]/10.0;				// 설정온도					h_csp = registers[H_CSP] / 10. ;
				m_OPState_Pump= (registers[OP_ST_R_CH2_NEX1100] == 0) ? 0 : 1 ;	// Run Stop 정보				pcss :Run/Stop 관련 Reg 0:STOP 1:RUN
				m_OPMode_Pump	= registers[OP_CMD_MODESET_RW_CH2_NEX1100] ;					// 6. 0:PROG, 1:FIX 관련 Reg

				/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////
				if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NexFilterLog", 0))
				{
					CString strLog;
					strLog.Format("SW Filtering : Ref:%f/Cur:%f n10", m_SP_temp2 , m_PV_temp2 );
					GLog::log_All("chiller","debug",strLog);
				}
				/////////////////////////////////20200924 중국 빈강 NEX1100 디버그/////////////////////////////////

			}
			else if(GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
			{
				m_PV_temp3		= registers[PV_T_R_NEX1200] / 10.0;				// 현재온도					td_temp  = registers[TD_TEMP] / 10.;				
				m_PV_humi		= registers[PV_H_R_NEX1200] / 10.0 ;				// 설정 습도에 관한 정보 필요
				m_SP_temp3		= registers[SP_T_R_NEX1200] / 10.0;				// 설정온도					h_csp = registers[H_CSP] / 10. ;
				m_SP_humi		= registers[SP_H_R_NEX1200] / 10.0;				// 설정습도
				m_OPState3		= (registers[OP_ST_RW_NEX1200] == 0) ? 0 : 1 ;	// Run Stop 정보				pcss :Run/Stop 관련 Reg 0:STOP 1:RUN
				m_OPMode3		= registers[OP_CMD_MODESET_RW_NEX1200] ;					// 6. 0:PROG, 1:FIX 관련 Reg
			}

			SetCurrentDataByModbus(0, m_PV_temp1, m_SP_temp1, m_OPState1, m_OPMode1, 
				m_PV_temp2, m_SP_temp2, m_OPState2, m_OPMode2, 
				m_PV_temp3, m_SP_temp3, m_OPState3, m_OPMode3, m_PV_humi, m_SP_humi,
				m_PV_Pump, m_SP_Pump, m_OPState_Pump, m_OPMode_Pump);
			
		}

		delete registers;
		return 1;
	}
	else
	{
		if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
		{
			char szTxBuff[100], szRxBuff[512];

 			ZeroMemory(szTxBuff,sizeof(szTxBuff));
			ZeroMemory(szRxBuff,sizeof(szRxBuff));

			int nRxSize = 0;
			int nTxSize = GetReqCurrentValueCmd(nDevice, szTxBuff, nRxSize);

			//TRACE("Send Data : %s\n",szTxBuff);

			m_cs.Lock(); //ksj 20200723 : 다중 스레드에서 동시 접근하지 못하도록 잠금;
			m_nRxBuffCnt = 0;
			if(m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
			{
				//응답은 OnCommunicaiton에서 처리 
				SetLineState(nDevice, TRUE);
				//ksj 20200723 : debug
				if(nRxSize > 512)
				{
					CString strLog;
					strLog.Format("%s [%d] %s [%d]", szTxBuff, nTxSize, szRxBuff, nRxSize);
					GLog::log_All("debug","debug",strLog);	
				}				
				//ksj end
				//RxData가 정상이면 
				for(int i=0; i<nRxSize; i++)
				{
					InsertRxChar(szRxBuff[i], nDevice);
				}
				
				m_cs.Unlock(); //ksj 20200723 :  잠금해제;

				return 1;
				
			}//yulee 20190903====================================================
			else
			{

				m_cs.Unlock(); //ksj 20200723 :  잠금해제;
				return 5;
			}//====================================================
		}//yulee 20190903====================================================
		else if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() == FALSE)
		{
			return 3;
		}
		else if(m_OvenData[nIndex].m_nDisconnectCnt >= 5)
		{
			return 4;
		}
		//====================================================
		return 0;//yulee 20190611
	}
	return 0;
}

void COvenCtrl::IncreaseDisconnectTime(int nOvenIndex)
{
	m_OvenData[nOvenIndex].m_nDisconnectCnt++;
}

ULONG COvenCtrl::GetDisconnectCnt(int nOvenIndex)
{
	return	m_OvenData[nOvenIndex].m_nDisconnectCnt;
}

void COvenCtrl::ResetDisconnectCnt(int nOvenIndex)
{
	m_OvenData[nOvenIndex].m_nDisconnectCnt = 0;
}


DWORD WINAPI COvenCtrl::PumpDealThread(LPVOID arg)
{//cny 201809
	COvenCtrl *obj=(COvenCtrl *)arg;
	if(!obj)     return 0;
	
	Sleep(1000);
	int  nTime=200;
	int  nRet=0;
	
	while(1)
	{
		if(g_AppInfo.iEnd==1) break;
		nTime=200;
		int nCtrlModel = 0;
		nCtrlModel = obj->GetOvenModelType();
		if(g_AppInfo.iEnd==1) break;
		nTime=200;
		if(obj->GetOvenModelType() == CHAMBER_MODBUS_NEX1100) //yulee 20190614_*
		{
			if(obj->m_bPortOpen)
			{
				nRet=obj->onPumpCmdDeal();
				if(nRet<=0) nTime=500;
			}
		}
		else
		{
			//---------------------------------
			if(obj->m_pPumpSerial.IsPortOpen())
			{
				nRet=obj->onPumpCmdDeal();
				if(nRet<=0) nTime=500;
				//---------------------------------
			}
		}

		Sleep(nTime);
	}
	return 1;
}

int COvenCtrl::onPumpCmdDeal()
{
	if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100) //yulee 20190614_*
	{
		if(m_bPortOpen == FALSE) return -1;
		int nCount=m_PumpCmdList.GetSize();
		if(nCount<1)
		{
			//------------------------------
			if(m_iPumpReqStop==1) 
				return 0;//Pause //yulee 20190614_*
			//------------------------------
			onPumpGetState();
			return -1;
		}
		CString strTemp;
		CString strData=m_PumpCmdList.GetAt(0);
		if(strData.IsEmpty() || strData==_T(""))
		{
			m_PumpCmdList.RemoveAt(0);
			return 1;
		}
		//---------------------------------------
		m_iPumpEmptyCount=0;//
		//---------------------------------------
		int nCmd=atoi(GStr::str_GetSplit(strData,0,','));
		int nVal=atoi(GStr::str_GetSplit(strData,1,','));
		int nLog=atoi(GStr::str_GetSplit(strData,2,','));
		int nRet=onPumpCmd(nCmd,nVal,nLog);
		//---------------------------------------
		if(nRet<0) return 0;

		m_PumpCmdList.RemoveAt(0);
		return 1;
	}
	else //yulee 20190614_*
	{
		if(m_pPumpSerial.IsPortOpen()==FALSE) return -1;
		int nCount=m_PumpCmdList.GetSize();
		if(nCount<1)
		{
			//------------------------------
			if(m_iPumpReqStop==1) return 0;//Pause
			//------------------------------
			onPumpGetState();
			return -1;
		}
		CString strTemp;
		CString strData=m_PumpCmdList.GetAt(0);
		if(strData.IsEmpty() || strData==_T(""))
		{
			m_PumpCmdList.RemoveAt(0);
			return 1;
		}
		//---------------------------------------
		m_iPumpEmptyCount=0;//
		//---------------------------------------
		int nCmd=atoi(GStr::str_GetSplit(strData,0,','));
		int nVal=atoi(GStr::str_GetSplit(strData,1,','));
		int nLog=atoi(GStr::str_GetSplit(strData,2,','));
		int nRet=onPumpCmd(nCmd,nVal,nLog);
		//---------------------------------------
		if(nRet<0) return 0;

		m_PumpCmdList.RemoveAt(0);
		return 1;	
	}

}

void COvenCtrl::onPumpGetState()
{
	m_iPumpEmptyCount++;
	if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100) //yulee 20190614_*
	{
		return;
	}
	else
	{
		m_iPumpEmptyCount++;
		if(m_iPumpEmptyCount<10) return;
		m_iPumpEmptyCount=0;

		onPumpCmd(ID_PUMP_CMD_READSTATE);
	}
}

void COvenCtrl::onPumpSetCHNO(int nCHNO)
{
	m_iPumpCHNO=nCHNO;//1start
}

CString COvenCtrl::onPumpGetLogType()
{
	int nCHNO=m_iPumpCHNO;

	CString strLogType;
	strLogType.Format(_T("pump_%d"),nCHNO);
	return strLogType;
}

void COvenCtrl::onPumpCmdAdd(int nCmd,int nVal,int nLog)
{
	if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100) //yulee 20190614_*
	{
		if(m_bPortOpen == FALSE) return;
	}
	else
	{
		if(m_pPumpSerial.IsPortOpen()==FALSE) return;
	}


	CString strTemp;
	strTemp.Format(_T("%d,%d,%d,"),nCmd,nVal,nLog);

	m_PumpCmdList.Add(strTemp);
}

int COvenCtrl::onPumpCmd(int nCmd,int nVal,BOOL bLog, int *Value)
{
	m_bChiller_DESChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseDESChiller", 0); //lyj 20201207

	if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100)
	{
		if(m_bPortOpen == FALSE) return -1;
	}
	else
	{
		if(GetOvenModelType() != CHILLER_TEMP2520)
			if(m_pPumpSerial.IsPortOpen()==FALSE) return -1;
	}

	char szTxBuff[1024], szRxBuff[1024];
	int nRxSize = 0;
	int nTxSize = 0;


	int nCmdModbus = -1;
	if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100)
	{
		switch(nCmd)
		{
		case ID_PUMP_CMD_READSTATE:
			{
				EnterCriticalSection(m_pCS);
				
				if(m_ctx != NULL)
				{
					//modbus_set_response_timeout(m_ctx,1,0); //ksj 20201019 : 타임아웃 지정.
					modbus_set_slave(m_ctx, 1);
				}
				LeaveCriticalSection(m_pCS);
				int mod_len = 15, rc = -1;
				int16_t* registers ;
				registers = (int16_t*) malloc(mod_len * sizeof(int16_t));
				int nErrorNo = 0; //ksj 20201018 : 에러코드 백업

				EnterCriticalSection(m_pCS);
				//if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
				//if(m_pSerial[nIndex] && m_pSerial[nIndex]->IsPortOpen() && m_OvenData[nIndex].m_nDisconnectCnt < 5)
				if(m_bPortOpen == TRUE)
				{					
					rc =modbus_read_registers(m_ctx, 0, mod_len, (uint16_t*)registers);//두번째 인자 채널 번호
					nErrorNo = errno; //ksj 20201018 : 에러코드 없어지기전에 백업 해놓기.

					Sleep(100);
				}
				LeaveCriticalSection(m_pCS);
				if (rc != mod_len)
				{
					//ksj 20201017 : 로그추가
					CString strLog;
					strLog.Format("onPumpCmd::modbus_read_registers port:%d rc(%d) Error:%d,%d(%s)",m_nOvenPortNum, rc, errno, nErrorNo, modbus_strerror (nErrorNo));
					GLog::log_All("OvenCtrl","error",strLog);
					//ksj end

/*					OPState = -1 ;*/
					return FALSE;
				}
				else
				{
					//SetLineState(nDevice, TRUE);
					//TRACE(_T("(%d) : Success (nb points %d), len = %d\n"), this, rc, mod_len);
					
					if(GetOvenModelType() == CHAMBER_MODBUS_NEX1100)
					{	
		// 				m_PumpState.nPlay	= registers[PV_T_R_CH1_NEX1000_1100] / 10.0;				// 현재온도					td_temp  = registers[TD_TEMP] / 10.;				
		// 				m_PumpState.nPlay		= registers[SP_T_R_CH1_NEX1000_1100] / 10.0;				// 설정온도					h_csp = registers[H_CSP] / 10. ;
		// 				OPState		= (registers[OP_ST_R_CH1_NEX1000_1100] == 0) ? 0 : 1 ;	// Run Stop 정보				pcss :Run/Stop 관련 Reg 0:STOP 1:RUN
		// 				m_PumpState.nMode		= ~(registers[OP_CMD_MODESET_RW_CH1_NEX1000_1100]) ;					// 6. 0:PROG, 1:FIX 관련 Reg
						m_PumpState.nPlay		= registers[PV_T_R_CH2_NEX1100];				// 현재온도					td_temp  = registers[TD_TEMP] / 10.;				
						m_PumpState.nSP			= registers[SP_T_R_CH2_NEX1100] / 10.0;				// 설정온도					h_csp = registers[H_CSP] / 10. ;
						m_PumpState.fSP			= registers[SP_T_R_CH2_NEX1100] / 10.0;				//lyj 20201105 NEX1100칠러 소숫점
		//				OPState_Pump= (registers[OP_ST_R_CH2_NEX1100] == 0) ? 0 : 1 ;	// Run Stop 정보				pcss :Run/Stop 관련 Reg 0:STOP 1:RUN
		//				OPMode_Pump	= registers[OP_CMD_MODESET_RW_CH2_NEX1100] ;					// 6. 0:PROG, 1:FIX 관련 Reg
					if(Value != NULL)
						*Value =m_PumpState.nSP	;
					}


					/*
					m_PumpState.nSP  =GStr::str_Hex2IntB(strSP);
					m_PumpState.nPlay=GStr::str_Hex2IntB(strNPV);
					m_PumpState.nMode=GStr::str_Hex2IntB(strMode);	
					m_PumpState.nSPNo=GStr::str_Hex2IntB(strNSP);			
					
					  m_PumpState.nSP1=GStr::str_Hex2IntB(strSp1);	
					  m_PumpState.nSP2=GStr::str_Hex2IntB(strSp2);	
					  m_PumpState.nSP3=GStr::str_Hex2IntB(strSp3);	
				m_PumpState.nSP4=GStr::str_Hex2IntB(strSp4);	
		  */
				}
				break;
			}
		case ID_PUMP_CMD_RUN:// 
			{
				nCmdModbus = OP_CMD_MODESET_RW_CH2_NEX1100;
				nVal = enum_RUN;
				break;
			}

		case ID_PUMP_CMD_STOP:// 
			{
				nCmdModbus = OP_CMD_MODESET_RW_CH2_NEX1100;
				nVal = enum_STOP;
				break;
			}

		case ID_PUMP_CMD_AUTO:// - 기능 없음    
		case ID_PUMP_CMD_MANUAL:// - 기능 없음
			{
				return -2;
				break;
			}
		case ID_PUMP_CMD_SETSP1: 
		case ID_PUMP_CMD_SETSP2:
		case ID_PUMP_CMD_SETSP3:
		case ID_PUMP_CMD_SETSP4:
		case ID_PUMP_CMD_SETSPNO:
			{
				nCmdModbus = SP_T_SET_W_CH2_NEX1100;
				//nVal *= 10; //lyj 20201105 NEX1100칠러 소숫점
				break;
			}
		default:
			return -1;
			break;
		}

		RequestSetLock(TRUE); //ksj 20201022 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		

		BOOL bRet = FALSE;
		ASSERT(m_ctx!= NULL) ;
		//CRITICAL_SECTION* m_pCS = new CRITICAL_SECTION ;
		//InitializeCriticalSection(m_pCS);
		
		//		D0104 40104 0067 CH1FIX.TSP CH1 FIX 운전시의 온도 SP 설정 W 
		EnterCriticalSection(m_pCS) ;
		bRet = (1 == modbus_write_register((modbus_t*)m_ctx, nCmdModbus, nVal)) ;
		Sleep(100) ;
		LeaveCriticalSection(m_pCS) ;
		if(bRet == TRUE)
		{
			TRACE("Chamber Run Cmd complete.");
		}
		else
		{
			//ksj 20201018 : 로그 추가
			CString strLog;				
			strLog.Format("onPumpCmdFail(SetSP) ret :%d, %d(%s)", bRet, errno, modbus_strerror (errno));
			GLog::log_All("OvenCtrl","error",strLog);
			ReConnect(0,0);
			//ksj end

			m_pPumpSerial.m_bRecv=TRUE;
		}
		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201022 : 잠금 푼다.
		return bRet;
	}
	else if(GetOvenModelType() == CHILLER_TEMP2520)
	{		
		BOOL bRet = FALSE;

		switch(nCmd)
		{
		case ID_PUMP_CMD_READSTATE:
			{
				bRet = TRUE;
				break;
			}
		case ID_PUMP_CMD_RUN:
			{
				bRet = m_OvenData[0].GetRunState2();		//run	
				break;
			}

		case ID_PUMP_CMD_STOP:// 
			{
				bRet = m_OvenData[0].GetRunState2();		//run	
				break;
			}

		case ID_PUMP_CMD_AUTO:// - 기능 없음    
		case ID_PUMP_CMD_MANUAL:// - 기능 없음
			{
				return -2;
				break;
			}
		case ID_PUMP_CMD_SETSP1: 
		case ID_PUMP_CMD_SETSP2:
		case ID_PUMP_CMD_SETSP3:
		case ID_PUMP_CMD_SETSP4:
		case ID_PUMP_CMD_SETSPNO:
			{
				break;
			}
		default:
			return -1;
			break;
		}

		if(bRet == TRUE)
		{
			TRACE("Chamber Run Cmd complete.");
		}
		else
		{
			m_pPumpSerial.m_bRecv=TRUE;
		}
		return bRet;
	}
	else
	{
		RequestSetLock(TRUE); //ksj 202011117 : RequestCurrentValue 호출 쓰레드에서 리퀘스트 잠시 중단하도록 잠근다. //반드시 잠금 풀고 리턴해야함.		


		ZeroMemory(szRxBuff,sizeof(szRxBuff));
		ZeroMemory(szTxBuff,sizeof(szTxBuff));
		
		if(!m_bChiller_DESChiller) //lyj 20201105 NEX1100칠러 소숫점
		{
			nVal =nVal/10;
		}

		if(onPumpGetCmd(nCmd, nVal,szTxBuff, nTxSize)==0) return 0;
	
 		BOOL bRet=m_pPumpSerial.WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize);
		
		int nCHNO=m_iPumpCHNO;
		CString strLogType=onPumpGetLogType();
		
		CString strTemp;
		strTemp.Format(_T("CHNO:%d ret:%d send(len:%d):%s(%s)"),
			nCHNO,bRet,nTxSize,
			mainGlobal.onGetCHAR(szTxBuff,nTxSize,0,_T("")),
			mainGlobal.onGetCHAR(szTxBuff,nTxSize,0,_T(" "),3));//HEX
		if(bLog==TRUE)
		{
			GLog::log_Save(strLogType,_T("send"),strTemp,NULL,TRUE);
		}
		
		strTemp.Format(_T("CHNO:%d ret:%d recv(len:%d):%s(%s)"),
			nCHNO,bRet,nRxSize,
			mainGlobal.onGetCHAR(szRxBuff,nRxSize,0,_T("")),
			mainGlobal.onGetCHAR(szRxBuff,nRxSize,0,_T(" "),3));//HEX
		if(bLog==TRUE) 
		{
			GLog::log_Save(strLogType,_T("recv"),strTemp,NULL,TRUE);
		}
		
		if(bRet==TRUE)
		{
			m_pPumpSerial.m_bRecv=TRUE;
			onPumpRecvDeal(nCmd,szRxBuff, nRxSize);
		}
		else //ksj 20201117
		{
			//ksj 20201018 : 로그 추가
			CString strLog;				
			strLog.Format("onPumpCmdFail(SetSP)");
			GLog::log_All("OvenCtrl","error",strLog);
			ReConnect(0,0); //ksj 20201117 : 재접속 추가.
		}


		RequestSetLock(FALSE,MODBUS_LOCK_DELAY_TIME_MSEC); //ksj 20201117 : 잠금 푼다.
		return bRet;
	}
	return -1; 
}

void COvenCtrl::onPumpRecvDeal(int nCmd,char *szRxChar, int nRxLen)
{
	if(nRxLen<1) return;
	if(szRxChar==NULL) return;
	//CString strTemp;

	CString strData=mainGlobal.onGetCHAR(szRxChar,nRxLen,0,_T(""));

	//header----------------------------
	if(szRxChar[0]!=0x02) return;

	//DeviceID--------------------------
	if( szRxChar[1]!='0' &&
		szRxChar[2]!='1' ) return;

	//--------------------------
	if(strData.Find(_T("OK"))<0)
	{
	   m_PumpState.nError=1;
       return;
	}	
	m_PumpState.nError=0;

    //Read State------------------------
	if( szRxChar[3]=='R' &&
		szRxChar[4]=='R' &&
		szRxChar[5]=='D')
	{//01RRD,OK,05A9,0064,0000FC
		int nCol=2;
		CString strPV=GStr::str_GetSplit(strData,nCol++,','); //Current
		CString strSP=GStr::str_GetSplit(strData,nCol++,','); //SetValue
		CString strNPV=GStr::str_GetSplit(strData,nCol++,',');//R-Run,S-Stop
		CString strNSP=GStr::str_GetSplit(strData,nCol++,',');//SetSPNo
		CString strMode=GStr::str_GetSplit(strData,nCol++,',');
		CString strSp1=GStr::str_GetSplit(strData,nCol++,',');
		CString strSp2=GStr::str_GetSplit(strData,nCol++,',');
		CString strSp3=GStr::str_GetSplit(strData,nCol++,',');
		CString strSp4=GStr::str_GetSplit(strData,nCol++,',');

		strSp4=strSp4.Mid(0,strSp4.GetLength()-2);
		
		m_PumpState.nPV  =GStr::str_Hex2IntB(strPV);
		m_PumpState.fPV  =(float)m_PumpState.nPV; //lyj 20201102 DES칠러 소숫점 
		if(m_PumpState.nPV == 65535) //yulee 20181226
		{
			m_PumpState.nPV = -1;
		}

		m_PumpState.nSP  =GStr::str_Hex2IntB(strSP);
		m_PumpState.fSP  =(float)m_PumpState.nSP; //lyj 20201102 DES칠러 소숫점 
		m_PumpState.nPlay=GStr::str_Hex2IntB(strNPV);
		m_PumpState.nMode=GStr::str_Hex2IntB(strMode);	
		m_PumpState.nSPNo=GStr::str_Hex2IntB(strNSP);			

		m_PumpState.nSP1=GStr::str_Hex2IntB(strSp1);
		m_PumpState.nSP2=GStr::str_Hex2IntB(strSp2);
		m_PumpState.nSP3=GStr::str_Hex2IntB(strSp3);
		m_PumpState.nSP4=GStr::str_Hex2IntB(strSp4);
		return;
	}

	//Write------------------------
	if( szRxChar[3]=='W' &&
		szRxChar[4]=='R' &&
		szRxChar[5]=='D')
	{//01WRD,OK
		return;
	}
}

int COvenCtrl::onPumpGetCmd(int nCmd,int nVal,char *szCmd, int &nResLen)
{
	if(szCmd == NULL) return 0;
    
	WORD pcl1 = 0;
	nResLen=0;
	memset(szCmd,0,sizeof(szCmd));

	
	if(m_nOvenModelType  == CHILLER_TEMP2520) //yulee 20191028 pump
	{
		switch(nCmd)
		{
		case ID_CHILLER_PUMP_CMD_READSTATE:
			{//STATE READ			
				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '4';			//Read Count
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '2';			//1. CH2.NPV (Ch 2 현재 펌프 값)
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '4';			//2. CH2.NSP (Ch 2 현재 설정펌프 값)
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';
				szCmd[nResLen++] = '1';			//3. CH2NOW.STS (Ch 2 운전 상태)
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '7';			//4. CH2 PROG:0 FIX:1//8. CH2 PROG:0 FIX:1		
				break;
			}
		case ID_CHILLER_PUMP_CMD_RUN:
			{//RUN
				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';//Count
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '1';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '3';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';//RUN
				break;
			}
		case ID_CHILLER_PUMP_CMD_STOP:
			{//STOP
				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';//Count
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '1';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '3';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '4';//STOP
				break;
			}
		case ID_CHILLER_PUMP_CMD_FIX:
			{//Fix
				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';//Count
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '1';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '7';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';//Fix mode set
				break;
			}
		case ID_CHILLER_PUMP_CMD_PROG:
			{//Manual
				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '1';//Count
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '1';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '7';
				szCmd[nResLen++] = ',';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0';//Prog mode set
				break;
			}
		case ID_CHILLER_PUMP_CMD_SETSP1:
			{//SETSP
				CString strTemp;
				strTemp.Format(_T("%04X"),nVal);

				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '0';//Count1
				szCmd[nResLen++] = '1';//Count2
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '2';
				szCmd[nResLen++] = '0';//
				szCmd[nResLen++] = '1';
				szCmd[nResLen++] = ',';//

				for(int i=0;i<strTemp.GetLength();i++)
				{
					szCmd[nResLen++] = strTemp[i];
				}			
				break;
			}
		case ID_CHILLER_PUMP_CMD_SETSP2:
			{//SETSP2
				CString strTemp;
				strTemp.Format(_T("%04X"),nVal);

				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '0';//Count1
				szCmd[nResLen++] = '1';//Count2
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '2';
				szCmd[nResLen++] = '0';//
				szCmd[nResLen++] = '2';
				szCmd[nResLen++] = ',';//

				for(int i=0;i<strTemp.GetLength();i++)
				{
					szCmd[nResLen++] = strTemp[i];
				}			
				break;
			}
		case ID_CHILLER_PUMP_CMD_SETSP3:
			{//SETSP3
				CString strTemp;
				strTemp.Format(_T("%04X"),nVal);

				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '0';//Count1
				szCmd[nResLen++] = '1';//Count2
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '2';
				szCmd[nResLen++] = '0';//
				szCmd[nResLen++] = '3';
				szCmd[nResLen++] = ',';//

				for(int i=0;i<strTemp.GetLength();i++)
				{
					szCmd[nResLen++] = strTemp[i];
				}			
				break;
			}
		case ID_CHILLER_PUMP_CMD_SETSP4:
			{//SETSP4
				CString strTemp;
				strTemp.Format(_T("%04X"),nVal);

				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '0';//Count1
				szCmd[nResLen++] = '1';//Count2
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '2';
				szCmd[nResLen++] = '0';//
				szCmd[nResLen++] = '4';
				szCmd[nResLen++] = ',';//

				for(int i=0;i<strTemp.GetLength();i++)
				{
					szCmd[nResLen++] = strTemp[i];
				}			
				break;
			}
		case ID_CHILLER_PUMP_CMD_SETSPNO:
			{//SETSPNO
				CString strTemp;
				strTemp.Format(_T("%04X"),nVal);

				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '0';//Count1
				szCmd[nResLen++] = '1';//Count2
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '2';
				szCmd[nResLen++] = '0';//
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = ',';//

				for(int i=0;i<strTemp.GetLength();i++)
				{
					szCmd[nResLen++] = strTemp[i];
				}			
				break;
			}
		}
	}
	else
	{
		switch(nCmd)
		{
		case ID_PUMP_CMD_READSTATE:
		{//STATE READ			
			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '9';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '1';//NPV
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';//NSP
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '1';//R-RUN,S-STOP
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';//SPNo
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '1';//Auto,Manual
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '5';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';//GET SP1
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '1';			
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';//GET SP2
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '2';			
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';//GET SP3
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '3';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';//GET SP4
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '4';
			break;
		}
		case ID_PUMP_CMD_RUN:
		{//RUN
			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';//RUN
			break;
		}
		case ID_PUMP_CMD_STOP:
		{//STOP
			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '1';//STOP
			break;
		}
		case ID_PUMP_CMD_AUTO:
		{//AUTO
			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '5';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';//AUTO
			break;
		}
		case ID_PUMP_CMD_MANUAL:
		{//MANUAL
			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '5';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '1';//MANUAL
			break;
		}
		case ID_PUMP_CMD_SETSP1:
		{//SETSP
			CString strTemp;
			strTemp.Format(_T("%04X"),nVal);

			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '1';
			szCmd[nResLen++] = ',';//
            
			for(int i=0;i<strTemp.GetLength();i++)
			{
				szCmd[nResLen++] = strTemp[i];
			}			
			break;
		}
		case ID_PUMP_CMD_SETSP2:
		{//SETSP2
			CString strTemp;
			strTemp.Format(_T("%04X"),nVal);

			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '2';
			szCmd[nResLen++] = ',';//
            
			for(int i=0;i<strTemp.GetLength();i++)
			{
				szCmd[nResLen++] = strTemp[i];
			}			
			break;
		}
		case ID_PUMP_CMD_SETSP3:
		{//SETSP3
			CString strTemp;
			strTemp.Format(_T("%04X"),nVal);

			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '3';
			szCmd[nResLen++] = ',';//
            
			for(int i=0;i<strTemp.GetLength();i++)
			{
				szCmd[nResLen++] = strTemp[i];
			}			
			break;
		}
		case ID_PUMP_CMD_SETSP4:
		{//SETSP4
			CString strTemp;
			strTemp.Format(_T("%04X"),nVal);

			szCmd[nResLen++] = 0x02;
			szCmd[nResLen++] = '0';
			szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
			szCmd[nResLen++] = 'W';
			szCmd[nResLen++] = 'R';
			szCmd[nResLen++] = 'D';
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '0';//Count1
			szCmd[nResLen++] = '1';//Count2
			szCmd[nResLen++] = ',';//
			szCmd[nResLen++] = '2';
			szCmd[nResLen++] = '0';//
			szCmd[nResLen++] = '4';
			szCmd[nResLen++] = ',';//
            
			for(int i=0;i<strTemp.GetLength();i++)
			{
				szCmd[nResLen++] = strTemp[i];
			}			
			break;
		}
		case ID_PUMP_CMD_SETSPNO:
		{//SETSPNO
			CString strTemp;
			strTemp.Format(_T("%04X"),nVal);

				szCmd[nResLen++] = 0x02;
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = '0'+m_iPumpDeviceID;//DeviceID
				szCmd[nResLen++] = 'W';
				szCmd[nResLen++] = 'R';
				szCmd[nResLen++] = 'D';
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '0';//Count1
				szCmd[nResLen++] = '1';//Count2
				szCmd[nResLen++] = ',';//
				szCmd[nResLen++] = '2';
				szCmd[nResLen++] = '0';//
				szCmd[nResLen++] = '0';
				szCmd[nResLen++] = ',';//

				for(int i=0;i<strTemp.GetLength();i++)
				{
					szCmd[nResLen++] = strTemp[i];
				}			
				break;
			}
		}
	}

	for(int i = 1 ; i < nResLen; i++)
	{
		pcl1 += szCmd[i];
	}
	sprintf(&szCmd[nResLen++], "%X", (pcl1&0x00f0) >> 4);
 	sprintf(&szCmd[nResLen++], "%X", pcl1&0x000f);
			
	szCmd[nResLen++] = 0x0D;
	szCmd[nResLen++] = 0x0A;

	return nResLen;
}

// 20200620 dhkim 통신 재연결 함수 추가 //ksj 20201017 : SK소스에서 발췌 수정
BOOL COvenCtrl::ReConnect(int nIndex, int nDevice)
{
	int nModelType = GetOvenModelType();
	if(nModelType > CHAMBER_MODBUS_NEX_START) 
	{
		BOOL bRet = FALSE;
		CString LogTemp;
		int retVal = 0;

		ASSERT(m_ctx!= NULL) ;

		EnterCriticalSection(m_pCS) ;		
		modbus_flush((modbus_t*)m_ctx);
		modbus_close((modbus_t*)m_ctx);
		Wait(1000);
		retVal  =modbus_connect((modbus_t*)m_ctx) ;

		//ksj 20201018 : 로그 추가
		LogTemp.Format("port:%d idx:%d ReConnect retVal: %d Error:%d(%s)", m_nOvenPortNum, nIndex, retVal, errno, modbus_strerror (errno) );
		GLog::log_All("OvenCtrl","error",LogTemp);
		//ksj end

		retVal >= 0 ? bRet  = TRUE : bRet  = FALSE; // -1이 실패 
		LogTemp.Format("ReConnect : %d", retVal);
		LeaveCriticalSection(m_pCS) ;
		Wait(100);	

		return retVal;
	}
	else
	{
		// 시리얼 통신하는 컨트롤러 재연결 로직 추가 필요 테스트 필요

		//if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Allow OvenCtrl Reconnect", 1)) //ksj 20201117 : 혹시 reconnect 문제 있으면 끌 수 있도록 옵션 처리.
		if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Allow OvenCtrl Reconnect", 0)) //ksj 20201215: 일단 비활성화. 필요시 활성화 하자.
		{
			//[0200707]Edited By Skh 채연결 포트 수정
			BOOL retVal = InitSerialPort(m_nOvenPortNum);

			return retVal;
		}		
	}


	return FALSE;
}

//ksj 20201022 : 주기적인 상태 확인 요청 쓰레드에서 명령을 전송하지 않도록
//크리티컬 섹션과는 다른 잠금 함수 추가.
void COvenCtrl::RequestSetLock(bool bLock, int nWaitMiliSec)
{
	m_bRequestLock = bLock;
	Wait(nWaitMiliSec);	//락 풀고 다른 쓰레드에서 바로 접근하지 못하도록 후딜레이를 준다.
}

//ksj 20201022 : 주기적인 상태 확인 요청 쓰레드에서 명령을 전송하지 않도록
//크리티컬 섹션과는 다른 잠금 여부 확인 함수 추가.
BOOL COvenCtrl::RequestGetLock()
{
	return m_bRequestLock;
}
