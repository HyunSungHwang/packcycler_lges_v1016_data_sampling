/** 
 *@file		JsonMon.cpp
 *@brief	JsonMon.cpp
 *@details	[#JSonMon] 설비 모니터링 정보를 JSON 파일로 내보내기 하는 기능을 구현한 클래스.
  *			가능한 다른 소스에 이식이 쉽도록 클래스로 모듈화 하여 구현.
 *@author	Kang se-jun
 *@date		2020-06-25
 ///////////////////////////////////////////////////////////////////////////////////////////
 *최종 수정 버전 정보 (프로그램 변경시 수정)
 *@version	1.0.3(20210701)  JSON 버전 수정시 #define JSON_MON_VERSION 수정 필요.
 *@update	2021-07-01
 *author	Kang se-jun
 */

#include "stdafx.h"
#include "JsonMon.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"
#include "MainFrm.h"

CJsonMon::CJsonMon(CCTSMonProDoc* pDoc, CCTSMonProView* pView, CMainFrame* pMainfrm)
{
	m_pJsonMonThread = NULL;
	m_bJsonMonThread = FALSE;
	m_pDoc = pDoc;
	m_pMainframe = pMainfrm;
	m_pView = pView;
	m_hWaitEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hExitEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);

	int i=0;
	for(i=0;i<JSON_MAX_MODULE_COUNT;i++)
	{
		m_strOnTimeDay[i].Format(JSON_DEFAULT_TIME_DAY); //모듈별 접속 시간(day)
		m_strOnTime[i].Format(JSON_DEFAULT_TIME);	//모듈별 접속 시간
		m_strOffTimeDay[i].Format(JSON_DEFAULT_TIME_DAY);//모듈별 접속해제 시간(day)
		m_strOffTime[i].Format(JSON_DEFAULT_TIME);	//모듈별 접속해제 시간
		m_nPrevModuleOnState[i]; //최종 모듈 접속 상태 0:오프라인, 1:온라인
	}
	
	//기본 JSON 저장 경로는 CTSMon 설치 경로 아래에 state 폴더.
	m_strJsonFilePath.Format("%s\\State",AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "C:\\"));
}


CJsonMon::~CJsonMon(void)
{
	StopJsonMon(); //클래스 소멸시 종료 시퀀스 호출.
}

//ksj 20200902 : 호출시 특정 모듈의 JSON 파일 1회 생성(업데이트)
int CJsonMon::UpdateJsonMon(int nModuleID)
{
	//ksj 20201015
	//모니터링 기능 활성화 여부 상시 체크.
	//프로그램 가동중에도 언제나 옵션 on 만으로 활성화 되도록 수정.
	BOOL bUseJsonMon = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use JsonMon", FALSE); //Json 모니터링 기능 사용 여부 옵션

	if(bUseJsonMon == FALSE) //모니터링 기능 비활성화 되어있으면 Pass
	{
		return 0;
	}
	//ksj end///////

	TRACE("UpdateJsonMon\n");

	STR_UPDATE_THREAD_PARAM* pParam = new STR_UPDATE_THREAD_PARAM;
	ZeroMemory(pParam,sizeof(STR_UPDATE_THREAD_PARAM));
	pParam->pJsonMon = this;
	pParam->nModuleID = nModuleID;

	AfxBeginThread(ThreadCreateMonFile, pParam); //혹시 파일 생성중에 메인쓰레드가 버벅일 수 있으니, 1회 생성도 쓰레드를 이용한다.

	return 1;
}

//ksj 20200902 : JSON 파일 생성 쓰레드
UINT CJsonMon::ThreadCreateMonFile(LPVOID pParam)
{	
	CJsonMon* pJsonMon = ((STR_UPDATE_THREAD_PARAM*)pParam)->pJsonMon;
	int nModuleID = ((STR_UPDATE_THREAD_PARAM*)pParam)->nModuleID;

	pJsonMon->CreateModuleMonFile(); 
	pJsonMon->CreateChannelMonFile(nModuleID);

	
	if(pParam) //ksj 20200909 : 사용 완료한 메모리 삭제 추가
	{
		delete pParam;
		pParam = NULL;
	}

	return 0;
}

//ksj 20200902 : 주기적인 모니터링 JSON 파일 생성 루프 시작.
int CJsonMon::RunJsonMon(int nPeriodMilliTimeSec/* = 10000 */)
{
	if(m_bJsonMonThread) return 1; //이미 쓰레드 Run 중.
	
	m_nPeriodMilliTimeSec = nPeriodMilliTimeSec;
	m_bJsonMonThread = TRUE;
	m_pJsonMonThread = AfxBeginThread(ThreadAutoUpdateMonFile, this);

	return 0;
}

//ksj 20200902 : JSON 파일 생성 쓰레드 중단.
int CJsonMon::StopJsonMon()
{
	if(m_pJsonMonThread == NULL) return 1; //이미 쓰레드 Stop 중.

	m_bJsonMonThread = FALSE;
	SetEvent(m_hWaitEvent); //쓰레드 Sleep딜레이 즉시 중단 이벤트 셋.

	//쓰레드 종료 체크
	ResetEvent(m_hExitEvent); //쓰레드 종료 이벤트 리셋.
	DWORD dwRes = WaitForSingleObject(m_hExitEvent,15000); //쓰레드 끝날때까지 대기. 무한대로 기다릴순 없으니 최대 15초까지만 기다리자.

	if(dwRes == WAIT_OBJECT_0)
	{
		//정상 종료
		//할 것 없음.
	}
	else
	{
		//비정상 종료. (쓰레드 종료안됨, 기타등등)
		//필요한 경우 예외처리 코드 추가
	}

	return 0;
}

//ksj 20200902 : JSON 파일 생성 쓰레드
UINT CJsonMon::ThreadAutoUpdateMonFile(LPVOID pParam)
{
	CJsonMon* pJsonMon = (CJsonMon*)pParam;

	BOOL* pContinue = &pJsonMon->m_bJsonMonThread;
	int nDelay = pJsonMon->m_nPeriodMilliTimeSec;
	int nTotalModuleCount = pJsonMon->m_pDoc->GetInstallModuleCount();

	while(*pContinue)
	{
		//ksj 20201015
		//모니터링 기능 활성화 여부 상시 체크.
		//프로그램 가동중에도 언제나 옵션 on 만으로 활성화 되도록 수정.
		BOOL bUseJsonMon = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use JsonMon", FALSE); //Json 모니터링 기능 사용 여부 옵션

		if(bUseJsonMon == FALSE) //모니터링 기능 비활성화 되어있으면 Pass
		{
			Sleep(5000);
			continue;
		}
		//ksj end///////

		TRACE("ThreadAutoUpdateMonFile\n");
		//전체 모듈 정보 파일 생성
		pJsonMon->CreateModuleMonFile(); 

		//각 모듈별 채널 정보 파일 생성.
		for(int nModuleId=1;nModuleId<=nTotalModuleCount;nModuleId++)
		{
			pJsonMon->CreateChannelMonFile(nModuleId);
		}	

		//Sleep(nDelay);
		ResetEvent(pJsonMon->m_hWaitEvent);
		DWORD dwRes = WaitForSingleObject(pJsonMon->m_hWaitEvent,nDelay);  //Sleep 함수 대체
		if(dwRes == WAIT_OBJECT_0)
		{
			//쓰레드 종료 이벤트 수신시, 딜레이 없이 즉시 종료.
			break;
		}
		else if(dwRes == WAIT_FAILED)
		{
#ifdef _DEBUG
			LPVOID lpMsgBuf;
			DWORD dw = GetLastError(); 

			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				dw,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				(LPTSTR) &lpMsgBuf,
				0, NULL );
			TRACE("WaitForSingleObject Error : %s\n",lpMsgBuf);
#endif
			Sleep(nDelay);
		}
	}	

	//ksj 20201015
	//모니터링 기능 활성화 여부 상시 체크.
	//프로그램 가동중에도 언제나 옵션 on 만으로 활성화 되도록 수정.
	BOOL bUseJsonMon = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use JsonMon", FALSE); //Json 모니터링 기능 사용 여부 옵션

	if(bUseJsonMon) //모니터링 기능 비활성화 되어있으면 Pass
	{
		//ksj 20200907 : 종료시에는 모니터링 JSON파일에 최종적으로 종료 상태로 기록/ 오프라인으로 표시.
		//쓰레드 정상 종료시에만 호출됨. //프로그램 크래시등에도 정상 종료되도록 추가 필요.
		pJsonMon->CreateModuleMonFile(TRUE); 

		for(int nModuleId=1;nModuleId<=nTotalModuleCount;nModuleId++)
		{
			pJsonMon->CreateChannelMonFile(nModuleId, TRUE);
		}	
		//ksj end
	}
	//ksj end///////
	

	pJsonMon->m_pJsonMonThread = NULL;
	SetEvent(pJsonMon->m_hExitEvent); //쓰레드 끝남 이벤트 셋

	return 0;
}


//ksj 20200902 : JSON 파일 저장할 폴더 경로 지정.
void CJsonMon::SetJsonPath(CString strJsonPath)
{
	m_strJsonFilePath = strJsonPath;
}


//ksj 20200625 : 채널 모니터링용 파일을 JSON 파일로 생성한다.
//BOOL bExit는 프로그램 종료 직전에 마지막에만 TRUE로 호출된다.
//bExit가 켜지면, 종료직전에 최종적으로 생성하는 파일이므로 고려하여 값 변경한다.
int CJsonMon::CreateChannelMonFile(int nModuleID, BOOL bExit)
{
	m_cs.Lock();

	Json::Value root;
	CString strTemp, strItem;
	CString strFileName;		
	strFileName.Format("%s\\Module_%d_channel_info.json",m_strJsonFilePath,nModuleID);
	//모듈 정보
	FILE *fp = fopen(strFileName, "wb,ccs=UTF-8");
	if(fp == NULL)
	{
		m_cs.Unlock();
		return FALSE;
	}

	CCyclerModule* pModule = m_pDoc->GetModuleInfo(nModuleID);
	//SFT_MD_SYSTEM_DATA* pSysData = ::SFTGetModuleSysData(nModuleID);
	if(!pModule)
	{
		m_cs.Unlock();
		return FALSE;
	}
	//if(!pSysData) return FALSE;

	//root
	MakeChannelInfoModule(pModule,root,bExit);

	//Channel
	Json::Value ChannelArray;

	if(!bExit) //프로그램 종료시에는 채널 상태 출력 안함.(못함)
	{
		for(int nChIndex = 0; nChIndex < pModule->GetTotalChannel(); nChIndex++)
		{
			CCyclerChannel* pChannel = pModule->GetChannelInfo(nChIndex);
			if(!pChannel) continue;		

			Json::Value Channel;

			//ksj 20200901 : 기능별 분리 리팩토링.
			MakeChannelInfoChannel(pModule,pChannel,Channel);  //채널 기본 정보 출력.		
			MakeChannelInfoAux(pModule,pChannel,Channel);  //AUX 관련 항목 출력.  - Pack 전용
			MakeChannelInfoCan(pModule,pChannel,Channel);  //CAN 관련 항목 출력  - Pack 전용	
			MakeChannelInfoChamber(pModule,pChannel,Channel);  //챔버 항목 출력 (생성 코드는 Pack 코드 전용)
			MakeChannelInfoChiller(pModule,pChannel,Channel); //칠러 관련 항목  - Pack 전용
			MakeChannelInfoPowerSupply(pModule,pChannel,Channel); //파워서플라이 관련 항목  - Pack 전용

			//////////////////////////////////////////////////////////
			ChannelArray.append(Channel);
		}	
	}
	root["Channel"] = ChannelArray;
	
	//ksj 20200901 : 기능별 분리 리팩토링
	MakeChannelInfoTotalChamber(root); //챔버 항목 출력
	MakeChannelInfoTotalChiller(root);  //칠러 관련 항목 - Pack 전용

	//최종 저장
	Json::StyledWriter writer;
	std::string outputConfig = writer.write(root);

	fwrite(outputConfig.c_str(), 1, outputConfig.length(), fp);
	fclose(fp);

	m_cs.Unlock();
	return TRUE;
}

//ksj 20200625 :모듈 모니터링용 파일을 JSON 파일로 생성한다.
//BOOL bExit는 프로그램 종료 직전에 마지막에만 TRUE로 호출된다.
//bExit가 켜지면, 종료직전에 최종적으로 생성하는 파일이므로 고려하여 값 변경한다.
int CJsonMon::CreateModuleMonFile(BOOL bExit)
{	
	m_cs.Lock();

	CString strFileName;
	CString str;
	strFileName.Format("%s\\Module_info.json",m_strJsonFilePath);
	//모듈 정보
	FILE *fp = fopen(strFileName, "wb,ccs=UTF-8");

	if(fp == NULL)
	{
		m_cs.Unlock();
		return FALSE;
	}

	Json::Value root;
	//CString strTemp;
	
	//root	
	MakeModuleInfo(root, bExit); //모듈 정보 JSON 객체 생성
	


	Json::StyledWriter writer;
	std::string outputConfig = writer.write(root);

	fwrite(outputConfig.c_str(), 1, outputConfig.length(), fp);
	fclose(fp);

	m_cs.Unlock();

	return TRUE;
}


//ksj 20200902 : EMG 파일 생성.
//Emg 리스트에 등록된 emg들을 파일로 생성한다.
//EMG 파일은 파일명에 생성일시분초.ms 까지 붙여서 다중 생성.
int CJsonMon::CreateEmgMonFile(int nModuleID, SFT_EMG_DATA* pEmgData)
{	
	//ksj 20201015
	//모니터링 기능 활성화 여부 상시 체크.
	//프로그램 가동중에도 언제나 옵션 on 만으로 활성화 되도록 수정.
	BOOL bUseJsonMon = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use JsonMon", FALSE); //Json 모니터링 기능 사용 여부 옵션

	if(bUseJsonMon == FALSE) //모니터링 기능 비활성화 되어있으면 Pass
	{
		return FALSE;
	}
	//ksj end///////

	if(pEmgData == NULL) return FALSE;
	
	CString strDateTime;	
	SYSTEMTIME lt;
	GetLocalTime(&lt);

	strDateTime.Format("%d_%02d_%02d_%02d_%02d_%02d_%03d",
		lt.wYear,
		lt.wMonth,
		lt.wDay,
		lt.wHour,
		lt.wMinute,
		lt.wSecond,
		lt.wMilliseconds);


	CString strFileName;
	CString str;
	strFileName.Format("%s\\Module_emg_code_%s.json",m_strJsonFilePath,strDateTime);
	//모듈 정보
	FILE *fp = fopen(strFileName, "wb,ccs=UTF-8");

	if(fp == NULL)
	{
		return FALSE;
	}

	Json::Value root;
	MakeEmgInfo(nModuleID, lt, pEmgData, root); //EMG 정보 JSON 객체 생성.

	Json::StyledWriter writer;
	std::string outputConfig = writer.write(root);

	fwrite(outputConfig.c_str(), 1, outputConfig.length(), fp);
	fclose(fp);

	return TRUE;
}




////////////////////////////////////////////////////////////////////////
//가상함수 Make~~~ 함수 구현부 시작
///////////////////////////////////////////////////////////////////////

//ksj 20200909 : 모듈 정보 JSON 객체 생성 함수 분리.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeModuleInfo(Json::Value& root, BOOL bExit)
{
	CString strTemp;
	CString str;

	int nInstalledModuleCnt = m_pDoc->GetInstallModuleCount();

	//root["Module Count"] = nInstalledModuleCnt;
	str.Format("%d",nInstalledModuleCnt);
	root["Module_Count"] = std::string(str);

	//module
	CString strEQP_Code = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB EQP CODE", "PNE");
	CString strEQP_Type = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "EQP TYPE", JSON_MON_DEFAULT_EQP_TYPE);	//ksj 20200915 : 장비 타입 추가. PACK//CELL
	CString strBlock_No = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB Block No", "1");		
	CString strConnectedDay, strConnectedTime;

	Json::Value ModuleArray;

	for(int i=0;i<nInstalledModuleCnt;i++)
	{
		Json::Value Module;

		if(!bExit) //프로그램 가동중 업데이트 항목
		{
			int nModuleID = m_pDoc->GetModuleID(i);
			CCyclerModule* pModule = m_pDoc->GetModuleInfo(nModuleID);			
			if(!pModule) continue;
			SFT_MD_SYSTEM_DATA* pSysData = ::SFTGetModuleSysData(nModuleID);
			if(!pSysData) continue;

			Module["JsonVersion"] = std::string(JSON_MON_VERSION); //ksj 20210701 : JsonMon 버전 정보 추가.
			Module["Eqp_Code"] = std::string(strEQP_Code);
			Module["Eqp_Type"] = std::string(strEQP_Type); //ksj 20200915 : 장비 타입 추가. PACK//CELL
			Module["Block_Num"] = std::string(strBlock_No);
			Module["Sbc_Ip"] = std::string(pModule->GetIPAddress());
			Module["Pc_Ip"] = std::string(m_pDoc->GetLocalIPAddress());
			str.Format("SystemType: %d, ProtocolVer: %x, OSver: %d, ModelName: %s"
				,pSysData->nSystemType,pSysData->nProtocolVersion,pSysData->nOSVersion,pSysData->szModelName);
			Module["Sbc_Sw_Version"] = std::string(str);
			Module["Sbc_Sw_Description"] = std::string(str);
			Module["Pc_Sw_Version"] = std::string(PROGRAM_VER);
			Module["Pc_Sw_Description"] = std::string(PROGRAM_VER);//"PC_SW_Version 주석 test";
			Module["Connect_State"] = (pModule->GetState() != PS_STATE_LINE_OFF && bExit == FALSE)? "on":"off";
			BOOL bRet = GetConnectedElapsedTime(pModule, strConnectedDay, strConnectedTime);			
			Module["Connect_Time_Day"] = std::string(strConnectedDay);
			Module["Connect_Time"] = std::string(strConnectedTime);
			str.Format("%d",nModuleID);
			Module["Module_Id"] = std::string(str);
			str.Format("%d",pSysData->nSystemType);
			Module["System_Type"] = std::string(str);
			str.Format("%x",pSysData->nProtocolVersion);
			Module["Protocol_Version"] = std::string(str);
			Module["Model_Name"] = std::string(pSysData->szModelName);
			str.Format("%d",pSysData->nOSVersion);
			Module["Osversion"] = std::string(str);
			str.Format("%d",pSysData->byCanCommType);
			Module["Can_Type"] = std::string(str);
			str.Format("%d",pSysData->wInstalledBoard);
			Module["Total_Board_Count"] = std::string(str);
			str.Format("%d",pSysData->wChannelPerBoard);
			Module["Channel_Per_Board"] = std::string(str);
			str.Format("%d",pSysData->nInstalledChCount);
			Module["Install_Channel_Count"] = std::string(str);

			str.Format("%d",pModule->GetVoltageRangeCount());
			Module["Volt_Range_Count"] = std::string(str);
			//volt_range
			{
				Json::Value VoltRangeArray;

				for(int r=0; r<pModule->GetVoltageRangeCount(); r++)
				{
					Json::Value VoltRange;
					// 					strTemp.Format("%0.f",pModule->GetVoltageSpec(r));
					// 					VoltRange["spec"] = std::string(strTemp);

					VoltRange = pModule->GetVoltageSpec(r)/1000.0f;
					VoltRangeArray.append(VoltRange);		
				}					

				Module["Volt_Range"] = VoltRangeArray;
			}

			str.Format("%d",pModule->GetCurrentRangeCount());
			Module["Current_Range_Count"] = std::string(str);

			//curr_range
			{
				Json::Value CurrentRangeArray;

				for(int r=0; r<pModule->GetCurrentRangeCount(); r++)
				{
					Json::Value CurrentRange;
					// 					strTemp.Format("%0.f",pModule->GetCurrentSpec(r));
					// 					CurrentRange["spec"] = std::string(strTemp);

					CurrentRange = pModule->GetCurrentSpec(r)/1000.0f;
					CurrentRangeArray.append(CurrentRange);
				}

				Module["Current_Range"] = CurrentRangeArray;				
			}			
			//ksj 20201020 : ontime 추가. (최종 접속 시간)
			if(pModule->GetState() != PS_STATE_LINE_OFF)
			{
				UpdateModuleOnTime(nModuleID);
			}

			//ksj 20201020 : offtime 추가. 모듈 상태가 on->off 되었을때 한번만 갱신.
			//if(pModule->GetState() == PS_STATE_LINE_OFF && m_nPrevModuleOnState[nModuleID-1] != PS_STATE_LINE_OFF)
			if(pModule->GetState() == PS_STATE_LINE_OFF && GetPrevModuleOnState(nModuleID) != PS_STATE_LINE_OFF)
			{
				UpdateModuleOffTime(nModuleID);
			}	

			//ksj 20201020 : 설비 접속/해제 시간 정보를 출력한다.
			Module["On_Time_Day"] = std::string(GetOnTimeDay(nModuleID));
			Module["On_Time"] = std::string(GetOnTime(nModuleID));
			Module["Off_Time_Day"] = std::string(GetOffTimeDay(nModuleID));
			Module["Off_Time"] = std::string(GetOffTime(nModuleID));

			ModuleArray.append(Module);

			//m_nPrevModuleOnState[nModuleID-1] = pModule->GetState(); // 모듈 상태 저장;
			SetPrevModuleOnState(nModuleID, pModule->GetState()); // 모듈 상태 저장;
		}
		else //프로그램 종료시 저장 항목
		{
			//프로그램 종료시에는 CCyclerModule과 SFT_MD_SYSTEM_DATA등이 모두 해제되어 
			//pModule 및 pSysData 접근시 프로그램 죽는다. pModule 및 pSysData 사용할 수 없으므로..
			//종료시 출력항목 별도 생성 하도록 추가.
			//굳이 종료시에는 모든항목 다 생성 안해도 될 듯. Connect_State만 Off로 생성해도 될 듯함.
			int nModuleID = m_pDoc->GetModuleID(i);

			Module["JsonVersion"] = std::string(JSON_MON_VERSION); //ksj 20210701 : JsonMon 버전 정보 추가.
			Module["Eqp_Code"] = std::string(strEQP_Code);
			Module["Block_Num"] = std::string(strBlock_No);
			Module["Sbc_Ip"] = "";
			Module["Pc_Ip"] = std::string(m_pDoc->GetLocalIPAddress());			
			Module["Sbc_Sw_Version"] = "";
			Module["Sbc_Sw_Description"] = "";
			Module["Pc_Sw_Version"] = std::string(PROGRAM_VER);
			Module["Pc_Sw_Description"] = std::string(PROGRAM_VER);//"PC_SW_Version 주석 test";
			Module["Connect_State"] = "off";
			str.Format("%d",i+1);
			Module["Module_Id"] = std::string(str);
			Module["System_Type"] = "";
			Module["Protocol_Version"] = "";
			Module["Model_Name"] = "";
			Module["Osversion"] = "";
			Module["Can_Type"] = "";
			Module["Total_Board_Count"] = "";
			Module["Channel_Per_Board"] = "";
			Module["Install_Channel_Count"] = "";				
			Module["Volt_Range_Count"] = "0";
			Module["Current_Range_Count"] = "0";


			//ksj 20201018 : 최종접속/해제 시간
			UpdateModuleOffTime(nModuleID);
			Module["On_Time_Day"] = std::string(GetOnTimeDay(nModuleID));
			Module["On_Time"] = std::string(GetOnTime(nModuleID));
			Module["Off_Time_Day"] = std::string(GetOffTimeDay(nModuleID));
			Module["Off_Time"] = std::string(GetOffTime(nModuleID));

			ModuleArray.append(Module);
		}

	}		

	root["Module"] = ModuleArray;

	return TRUE;
}


//ksj 20200909 : 모듈 정보 JSON 객체 생성 함수 분리.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeEmgInfo(int nModuleID, SYSTEMTIME& lt, SFT_EMG_DATA* pEmgData, Json::Value& root)
{
	CString strEQP_Code = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB EQP CODE", "PNE");
	CString strDateTime;

	strDateTime.Format("%d-%02d-%02d %02d:%02d:%02d.%03d",
		lt.wYear,
		lt.wMonth,
		lt.wDay,
		lt.wHour,
		lt.wMinute,
		lt.wSecond,
		lt.wMilliseconds);
		
	CString strTemp;
	//root
	{
		root["Eqp_Code"] = std::string(strEQP_Code);

		strTemp.Format("%d",nModuleID);
		root["Module_Id"] = std::string(strTemp);

		strTemp.Format("%d",pEmgData->lCode);
		root["Emg_Code"] = std::string(strTemp);

		strTemp.Format("%d",pEmgData->lValue);
		root["Emg_Value1"] = std::string(strTemp);

		strTemp.Format("%d",pEmgData->reserved);
		root["Emg_Value2"] = std::string(strTemp);

		strTemp.Format("%s",m_pDoc->GetEmgMsg(pEmgData->lCode));
		root["Emg_Description"] = std::string(strTemp);		
		root["Emg_Date_Time"] = std::string(strDateTime);	

	}	

	return TRUE;
}

//ksj 20200901 : Module 기본 정보 Json 객체 생성
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeChannelInfoModule(CCyclerModule* pModule, Json::Value& root, BOOL bExit)
{
	CString strEQP_Code = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "DB EQP CODE", "PNE");	
	CString strEQP_Type = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "EQP TYPE", JSON_MON_DEFAULT_EQP_TYPE);	//ksj 20200915 : 장비 타입 추가. PACK//CELL

	CString strTemp, strItem;
	CString strConnectedDay;
	CString strConnectedTime;	

	int nModuleID = pModule->GetModuleID();	
	if(nModuleID < 1) return 0;

	if(!bExit)
	{
		root["JsonVersion"] = std::string(JSON_MON_VERSION); //ksj 20210701 : JsonMon 버전 정보 추가.
		root["Eqp_Code"] = std::string(strEQP_Code);
		root["Eqp_Type"] = std::string(strEQP_Type); //ksj 20200915 : 장비 타입 추가. PACK//CELL
		strTemp.Format("%d",pModule->GetModuleID());
		root["Module_Id"] = std::string(strTemp);
		//root["Module_Id"] = pModule->GetModuleID();
		root["Module_Connect"] = (pModule->GetState() != PS_STATE_LINE_OFF)? "on":"off";			
		
		BOOL bRet = GetConnectedElapsedTime(pModule, strConnectedDay, strConnectedTime);			
		root["Connect_Time_Day"] = std::string(strConnectedDay);
		root["Connect_Time"] = std::string(strConnectedTime);

		//ksj 20201020 : 설비 접속/해제 시간 정보를 출력한다.
		//정보 갱신은 MakeModuleInfo 함수에서 이루어지므로, 해당 함수에서 갱신이 일어나지 않으면 이 값도 갱신되지 않는다.
		root["On_Time_Day"] = std::string(GetOnTimeDay(nModuleID));
		root["On_Time"] = std::string(GetOnTime(nModuleID));
		root["Off_Time_Day"] = std::string(GetOffTimeDay(nModuleID));
		root["Off_Time"] = std::string(GetOffTime(nModuleID));
		

		strTemp.Format("%d",pModule->GetTotalChannel());
		root["Channel_Count"] = std::string(strTemp);
		//root["Channel_Count"] = pModule->GetTotalChannel();

	}
	else //프로그램 종료시에는 최종 상태 초기화.
	{
		root["JsonVersion"] = std::string(JSON_MON_VERSION); //ksj 20210701 : JsonMon 버전 정보 추가.
		root["Eqp_Code"] = std::string(strEQP_Code);
		strTemp.Format("%d",pModule->GetModuleID());
		root["Module_Id"] = std::string(strTemp);
		root["Module_Connect"] = "off";
		root["Connect_Time_Day"] = JSON_DEFAULT_TIME_DAY;
		root["Connect_Time"] = JSON_DEFAULT_TIME;
		root["Channel_Count"] = "0";

		//ksj 20201018 : 최종접속/해제 시간
		UpdateModuleOffTime(nModuleID);
		root["On_Time_Day"] = std::string(GetOnTimeDay(nModuleID));
		root["On_Time"] = std::string(GetOnTime(nModuleID));
		root["Off_Time_Day"] = std::string(GetOffTimeDay(nModuleID));
		root["Off_Time"] = std::string(GetOffTime(nModuleID));
	}

	return TRUE;
}


//ksj 20200901 : Channel 관련 항목 Json 객체 생성.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeChannelInfoChannel(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel)
{
	CString strTemp, strItem;
	int nChIndex = pChannel->GetChannelIndex();

	strTemp.Format("%d",nChIndex+1);
	Channel["Ch_No"] = std::string(strTemp);
	//Channel["Ch_No"] = nChIndex+1;
	Channel["State"] = std::string(::PSGetStateMsg(pChannel->GetState()));

	if(pChannel->GetState() == PS_STATE_RUN)
		Channel["Type"] = std::string(::PSGetTypeMsg(pChannel->GetStepType()));		
	else
		Channel["Type"] = ""; //ksj 20210701 : RUN 상태가 아닐때는 Type 표시하지 않도록 변경.

	Channel["Mode"] = std::string(m_pDoc->ValueString(pChannel->GetStepMode(),PS_STEP_MODE));
	strTemp.Format("%d",pChannel->GetCellCode());
	Channel["Code"] = std::string(strTemp);
	//Channel["Code"] = pChannel->GetCellCode();
	Channel["Code_Desc"] =  std::string(m_pDoc->ValueString(pChannel->GetCellCode(),PS_CODE)); //ksj 20200924 : 채널코드 설명용 문자열 추가.
	strTemp.Format("%d",pChannel->GetStepNo());
	Channel["Step_No"] = std::string(strTemp);
	//Channel["Step_No"] = pChannel->GetStepNo();			
	strTemp.Format("%.3f",pChannel->GetVoltage()/1000000.0f);
	Channel["Voltage"] = std::string(strTemp);
	//Channel["Voltage"] = pChannel->GetVoltage()/1000000.0f;
	strTemp.Format("%.3f",pChannel->GetCurrent()/1000000.0f);
	Channel["Current"] = std::string(strTemp);
	//Channel["Current"] = pChannel->GetCurrent()/1000000.0f;
	strTemp.Format("%.3f",pChannel->GetChargeAh()/1000000.0f);
	Channel["Charge_Ah"] = std::string(strTemp);
	//Channel["Charge_Ah"] = pChannel->GetChargeAh()/1000000.0f;
	strTemp.Format("%.3f",pChannel->GetDisChargeAh()/1000000.0f);
	Channel["DisCharge_Ah"] = std::string(strTemp);
	//Channel["DisCharge_Ah"] = pChannel->GetDisChargeAh()/1000000.0f;
	strTemp.Format("%.3f",pChannel->GetChargeWh()/1000000.0f);
	Channel["Charge_Wh"] = std::string(strTemp);
	//Channel["Charge_Wh"] = pChannel->GetChargeWh()/1000000.0f;
	strTemp.Format("%.3f",pChannel->GetDisChargeWh()/1000000.0f);
	Channel["DisCharge_Wh"] = std::string(strTemp);
	//Channel["DisCharge_Wh"] = pChannel->GetDisChargeWh()/1000000.0f;
	strTemp.Format("%d",pChannel->GetStepTimeDay());
	Channel["Step_Day"] = std::string(strTemp);
	//Channel["Step_Day"] = UINT(pChannel->GetStepTimeDay());			
	strTemp.Format("%d",pChannel->GetStepTime());
	Channel["Step_Time"] = std::string(strTemp);
	//Channel["Step_Time"] = UINT(pChannel->GetStepTime());			
	strTemp.Format("%d",pChannel->GetTotTimeDay());
	Channel["Total_Day"] = std::string(strTemp);
	//Channel["Total_Day"] = UINT(pChannel->GetTotTimeDay());			
	strTemp.Format("%d",pChannel->GetTotTime());
	Channel["Total_Time"] = std::string(strTemp);
	//Channel["Total_Time"] = UINT(pChannel->GetTotTime());			
	strTemp.Format("%.3f",pChannel->GetImpedance()/1000000.0f);
	Channel["Impedance"] = std::string(strTemp);
	//Channel["Impedance"] = pChannel->GetImpedance()/1000000.0f;		

	if(pChannel->IsParallel() == TRUE && pChannel->m_bMaster == FALSE)
	{
		//ksj 20210701 : 병렬모드이면서, 슬레이브 채널인 경우. 시험명, 스케쥴, 시험 경로 표시 하지 않음. (이전시험 데이터가 표시되므로)
		Channel["Test_Name"] = "";		
		Channel["Schedule_Name"] = "";		
		Channel["Result_Path"] ="";	
	}
	else
	{
		Channel["Test_Name"] = std::string(pChannel->GetTestName());		
		Channel["Schedule_Name"] = std::string(pChannel->GetScheduleName());		
		Channel["Result_Path"] = std::string(pChannel->GetTestPath());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	strTemp.Format("%d", pChannel->GetTotalCycleCount());
	Channel["Total_Cycle_Num"] = std::string(strTemp);
	//Channel["Total_Cycle_Num"] = pChannel->GetTotalCycleCount();
	strTemp.Format("%d", pChannel->GetCurCycleCount());
	Channel["Current_Cycle_Num"] = std::string(strTemp);
	//Channel["Current_Cycle_Num"] = pChannel->GetCurCycleCount();
	strTemp.Format("%.3f", pChannel->GetAvgVoltage()/1000000.0f);
	Channel["Avg_Voltage"] = std::string(strTemp);
	//Channel["Avg_Voltage"] = pChannel->GetAvgVoltage()/1000000.0f;
	strTemp.Format("%.3f", pChannel->GetAvgCurrent()/1000000.0f);
	Channel["Avg_Current"] = std::string(strTemp);
	//Channel["Avg_Current"] = pChannel->GetAvgCurrent()/1000000.0f;
	strTemp.Format("%d", pChannel->GetCVTimeDay());
	Channel["Cv_Day"] = std::string(strTemp);
	//Channel["Cv_Day"] = pChannel->GetCVTimeDay();
	strTemp.Format("%d", pChannel->GetCVTime());
	Channel["Cv_Time"] = std::string(strTemp);
	//Channel["Cv_Time"] = pChannel->GetCVTime();
	strTemp.Format("%d", pChannel->GetSyncDate());
	Channel["Sync_Time_Day"] = std::string(strTemp);
	//Channel["Sync_Time_Day"] = pChannel->GetSyncDate();
	strTemp.Format("%d", pChannel->GetSyncTime());
	Channel["Sync_Time"] = std::string(strTemp);
	//Channel["Sync_Time"] = pChannel->GetSyncTime();

	//ksj 20210701 : 병렬 상태 (신규 항목 추가)
	strTemp.Format("%d", pChannel->IsParallel()); //병렬상태인지
	Channel["IsParallel"] = std::string(strTemp);
	strTemp.Format("%d", pChannel->m_bMaster); //마스터채널인지
	Channel["IsMaster"] = std::string(strTemp);

	return 0;
}

//ksj 20200901 : Aux 관련 항목 Json 객체 생성.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeChannelInfoAux(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel)
{

#ifdef JSON_MON_EXPORT_AUX //AUX 관련 항목 출력.

	CString strTemp, strItem;

	strTemp.Format("%d",pChannel->GetAuxCount());
	Channel["Aux_Count"] = std::string(strTemp);	
	//Channel["Aux_Count"] = pChannel->GetAuxCount();

	//aux
	BOOL bAuxFirstTemp(TRUE), bAuxFirstVolt(TRUE), bAuxFirstTher(TRUE);
	BOOL bAuxFirstTherHumi(TRUE);
	float fMinVolt=0, fMaxVolt=0, fSumVolt=0, fVoltCnt=0;
	float fMinTemp=0, fMaxTemp=0, fSumTemp=0, fTempCnt=0;
	float fMinTher=0, fMaxTher=0, fSumTher=0, fTherCnt=0;
	float fMinHumi=0, fMaxHumi=0, fSumHumi=0, fHumiCnt=0;

	Json::Value AuxArray;			
	SFT_AUX_DATA * pAuxData = pChannel->GetAuxData();

	for(int i = 0; i < pChannel->GetAuxCount(); i++)
	{
		Json::Value Aux;
		CChannelSensor * pSensor;
		pSensor = pModule->GetAuxData(pAuxData[i].auxChNo-1, pAuxData[i].auxChType);

		if(pSensor == NULL)
		{
			break;
		}

		strItem.Format("%d", i+1);
		Aux["Aux_Item"] = std::string(strItem);
		//Aux["Aux_Item"] = i+1;
		Aux["Name"] = std::string(pSensor->GetAuxName());
		float fValue = m_pDoc->UnitTrans( pAuxData[i].lValue, FALSE, pAuxData[i].auxChType);
		strItem.Format("%0.3f", fValue);
		Aux["Value"] = std::string(strItem);
		//				Aux["Value"] = fValue/1000000.0f;

		if(pAuxData[i].auxChType == PS_AUX_TYPE_VOLTAGE)
		{
			//if (pAuxData[i].auxChNo < 96)
			{
				if (bAuxFirstVolt) 
				{
					fMinVolt = fMaxVolt = fValue;
					bAuxFirstVolt = FALSE;
				}
				else
				{
					if (fMinVolt > fValue) fMinVolt = fValue;
					if (fMaxVolt < fValue) fMaxVolt = fValue;
				}

				fSumVolt+=fValue; //ksj 20200629
				fVoltCnt++; //ksj 20200629
			}
		}
		else if(pAuxData[i].auxChType == PS_AUX_TYPE_TEMPERATURE)
		{
			if (bAuxFirstTemp) 
			{
				fMinTemp = fMaxTemp = fValue;
				bAuxFirstTemp = FALSE;
			}
			else
			{
				if (fMinTemp > fValue) fMinTemp = fValue;
				if (fMaxTemp < fValue) fMaxTemp = fValue;
			}

			fSumTemp+=fValue; //ksj 20200629
			fTempCnt++; //ksj 20200629

		}
		else if(pAuxData[i].auxChType == PS_AUX_TYPE_TEMPERATURE_TH)
		{
			if (bAuxFirstTher) 
			{
				fMinTher = fMaxTher = fValue;
				bAuxFirstTher = FALSE;
			}
			else
			{
				if (fMinTher > fValue) fMinTher = fValue;
				if (fMaxTher < fValue) fMaxTher = fValue;
			}		

			fSumTher+=fValue; //ksj 20200629
			fTherCnt++; //ksj 20200629
		}
		else if(pAuxData[i].auxChType == PS_AUX_TYPE_HUMIDITY)
		{
			if (bAuxFirstTherHumi) 
			{
				fMinHumi = fMaxHumi = fValue;
				bAuxFirstTherHumi = FALSE;
			}
			else
			{
				if (fMinHumi > fValue) fMinHumi = fValue;
				if (fMaxHumi < fValue) fMaxHumi = fValue;
			}	

			fSumHumi+=fValue; //ksj 20200629
			fHumiCnt++; //ksj 20200629
		}

		STF_MD_AUX_SET_DATA AuxSetData = pSensor->GetAuxData();

		strItem.Format("%f", float(m_pDoc->UnitTrans(AuxSetData.vent_upper, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		Aux["Fix_Max"] = std::string(strItem);
		//				Aux["Fix_Max"] = AuxSetData.vent_upper/1000000.0f;

		strItem.Format("%f", float(m_pDoc->UnitTrans(AuxSetData.vent_lower, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		Aux["Fix_Min"] = std::string(strItem);
		//				Aux["Fix_Min"] = AuxSetData.vent_lower/1000000.0f;

		strItem.Format("%f", float(m_pDoc->UnitTrans(AuxSetData.lMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		Aux["Safety_Max"] = std::string(strItem);		
		//				Aux["Safety_Max"] = AuxSetData.lMaxData/1000000.0f;	

		strItem.Format("%f", float(m_pDoc->UnitTrans(AuxSetData.lMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		Aux["Safety_Min"] = std::string(strItem);		
		//				Aux["Safety_Min"] = AuxSetData.lMinData/1000000.0f;	

		strItem.Format("%d",AuxSetData.vent_use_flag);
		Aux["Use_Vent_Link"] = std::string(strItem);	
		//Aux["Use_Vent_Link"] = AuxSetData.vent_use_flag;	

		strItem.Format("%f", float(m_pDoc->UnitTrans(AuxSetData.lEndMaxData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		Aux["End_Max"] = std::string(strItem);
		//				Aux["End_Max"] = AuxSetData.lEndMaxData/1000000.0f;

		strItem.Format("%f", float(m_pDoc->UnitTrans(AuxSetData.lEndMinData, FALSE, pSensor->GetAuxType())));
		strItem.TrimRight("0");
		strItem.TrimRight(".");
		Aux["End_Min"] = std::string(strItem);
		//				Aux["End_Min"] = AuxSetData.lEndMinData/1000000.0f;


		strItem = m_pView->Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division1);		
		Aux["Division_Code1"] = std::string(strItem);
		//Aux["Division_Code1"] = AuxSetData.funtion_division1;

		strItem = m_pView->Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division2);
		Aux["Division_Code2"] = std::string(strItem);
		//Aux["Division_Code2"] = AuxSetData.funtion_division2;

		strItem = m_pView->Fun_GetFunctionString(ID_AUX_TYPE, AuxSetData.funtion_division3);
		Aux["Division_Code3"] = std::string(strItem);
		//Aux["Division_Code3"] = AuxSetData.funtion_division3;

		if (pAuxData[i].auxChType != 2)
			strItem = "";
		else
			strItem = m_pView->Fun_GetFunctionString(ID_AUX_THERMISTOR_TYPE, AuxSetData.auxTempTableType);

		Aux["AuxTempTableType"] = std::string(strItem);
		//Aux["auxTempTableType"] = AuxSetData.auxTempTableType;


		AuxArray.append(Aux);
	}

	Channel["Aux"] = AuxArray;


	if(pChannel->GetAuxTempCount() > 0)
	{
		strTemp.Format("%0.3f", fMaxTemp);
		Channel["Aux_Temp_Max"] = std::string(strTemp);			
		strTemp.Format("%0.3f", fMinTemp);
		Channel["Aux_Temp_Min"] = std::string(strTemp);			
		strTemp.Format("%0.3f", fMaxTemp-fMinTemp);
		Channel["Aux_Temp_Dev"] = std::string(strTemp);
		strTemp.Format("%0.3f", (fTempCnt)? fSumTemp/fTempCnt:0);
		Channel["Aux_Temp_Avg"] = std::string(strTemp);
		// 			Channel["Aux_Temp_Max"] = fMaxTemp/1000000.0f;
		// 			Channel["Aux_Temp_Min"] = fMaxTemp/1000000.0f;
		// 			Channel["Aux_Temp_Dev"] = fMaxTemp-fMinTemp;
		// 			Channel["Aux_Temp_Avg"] = (fTempCnt)? fSumTemp/fTempCnt:0;
	}

	if(pChannel->GetAuxVoltCount() > 0)
	{
		strTemp.Format("%0.3f", fMaxVolt);
		Channel["Aux_Volt_Max"] = std::string(strTemp);
		strTemp.Format("%0.3f", fMinVolt);
		Channel["Aux_Volt_Min"] = std::string(strTemp);
		strTemp.Format("%0.3f", fMaxVolt-fMinVolt);
		Channel["Aux_Volt_Dev"] = std::string(strTemp);
		strTemp.Format("%0.3f", (fVoltCnt)? fSumVolt/fVoltCnt:0);
		Channel["Aux_Volt_Avg"] = std::string(strTemp);
		// 			Channel["Aux_Volt_Max"] = fMaxVolt/1000000.0f;
		// 			Channel["Aux_Volt_Min"] = fMinVolt/1000000.0f;
		// 			Channel["Aux_Volt_Dev"] = fMaxVolt-fMinVolt;
		// 			Channel["Aux_Volt_Avg"] = (fVoltCnt)? fSumVolt/fVoltCnt:0;
	}


	if(pChannel->GetAuxTempThCount() > 0)
	{
		strTemp.Format("%0.3f", fMaxTher);
		Channel["Aux_Ther_Max"] = std::string(strTemp);
		strTemp.Format("%0.3f", fMinTher);
		Channel["Aux_Ther_Min"] = std::string(strTemp);
		strTemp.Format("%0.3f", fMaxTher-fMinTher);
		Channel["Aux_Ther_Dev"] = std::string(strTemp);
		strTemp.Format("%0.3f", (fTherCnt)? fSumTher/fTherCnt:0);
		Channel["Aux_Ther_Avg"] = std::string(strTemp);
		// 			Channel["Aux_Ther_Max"] = fMaxTher/1000000.0f;
		// 			Channel["Aux_Ther_Min"] = fMinTher/1000000.0f;
		// 			Channel["Aux_Ther_Dev"] = fMaxTher-fMinTher;
		// 			Channel["Aux_Ther_Avg"] = (fTherCnt)? fSumTher/fTherCnt:0;
	}

	if(pChannel->GetAuxHumiThCount() > 0)
	{
		strTemp.Format("%0.3f", fMaxHumi);
		Channel["Aux_Humi_Max"] = std::string(strTemp);
		strTemp.Format("%0.3f", fMinHumi);
		Channel["Aux_Humi_Min"] = std::string(strTemp);
		strTemp.Format("%0.3f", fMaxHumi-fMinHumi);
		Channel["Aux_Humi_Dev"] = std::string(strTemp);
		strTemp.Format("%0.3f", (fHumiCnt)? fSumHumi/fHumiCnt:0);
		Channel["Aux_Humi_Avg"] = std::string(strTemp);
		// 			Channel["Aux_Humi_Max"] = fMaxHumi/1000000.0f;
		// 			Channel["Aux_Humi_Min"] = fMinHumi/1000000.0f;
		// 			Channel["Aux_Humi_Dev"] = fMaxHumi-fMinHumi;
		// 			Channel["Aux_Humi_Avg"] = (fHumiCnt)? fSumHumi/fHumiCnt:0;
	}


#endif //JSON_MON_EXPORT_AUX 항목 출력 끝

	return 0;
}


//ksj 20200901 : Can 관련 항목 Json 객체 생성.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeChannelInfoCan(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel)
{
#ifdef JSON_MON_EXPORT_CAN //CAN 관련 항목 출력.
	CString strTemp, strItem;
	int nCanMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterIDType", 0);

	//CAN		
	strTemp.Format("%d",pChannel->GetCanCount());
	Channel["Can_Count"] = std::string(strTemp);
	//Channel["Can_Count"] = pChannel->GetCanCount();
	SFT_CAN_DATA * pCanData = pChannel->GetCanData();				
	Json::Value CanArray;
	if(pCanData)
	{				
		int nChannelID = pChannel->GetChannelIndex();

		for(int i = 0; i < pModule->GetInstalledCanMaster(nChannelID); i++)
		{
			Json::Value Can;
			SFT_CAN_SET_DATA canData = pModule->GetCANSetData(nChannelID, PS_CAN_TYPE_MASTER, i);

			strItem.Format("M%d", i+1);
			Can["Can_Item"] = std::string(strItem);

			Can["Name"] = std::string(canData.name);

			if(pCanData[i].data_type == 0 || pCanData[i].data_type == 1 || pCanData[i].data_type == 2)
			{
				strItem.Format("%.6f", pCanData[i].canVal.fVal[0]);
				if (canData.function_division > 100 && canData.function_division < 150)
					strItem.Format("%02x%02x%02x%02x", (BYTE)pCanData[i].canVal.strVal[0], (BYTE)pCanData[i].canVal.strVal[1], (BYTE)pCanData[i].canVal.strVal[2], (BYTE)pCanData[i].canVal.strVal[3]);

				strItem.TrimRight("0");
				strItem.TrimRight(".");
			}
			else if(pCanData[i].data_type == 3){
				strItem.Format("%s", pCanData[i].canVal.strVal);
			}
			else if (pCanData[i].data_type == 4){
				strItem.Format("0x%x", (UINT)pCanData[i].canVal.fVal[0]);			
			}

			Can["Value"] = std::string(strItem);

			if (nCanMasterIDType == 1)
				//strItem.Format("%ld", canData.canID);
				strItem.Format("%u", canData.canID); //ksj 20200323 : unsinged 타입으로 출력. extended id 대응.
			else
				strItem.Format("0x%x", canData.canID);

			Can["Can_Id"] = std::string(strItem);


			strItem.Format("%.3f", canData.fault_upper);				
			Can["Safety_Max"] = std::string(strItem);
			//					Can["Safety_Max"] = canData.fault_upper;

			strItem.Format("%.3f", canData.fault_lower);
			Can["Safety_Min"] = std::string(strItem);
			//					Can["Safety_Min"] = canData.fault_lower;

			strItem.Format("%.3f", canData.end_upper);
			Can["End_max"] = std::string(strItem);
			//					Can["End_max"] = canData.end_upper;

			strItem.Format("%.3f", canData.end_lower);
			Can["End_min"] = std::string(strItem);
			//					Can["End_min"] = canData.end_lower;

			strItem.Format("%s",m_pView->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
			Can["Division_Code1"] = std::string(strItem);
			//					Can["Division_Code1"] = canData.function_division;

			strItem.Format("%s",m_pView->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
			Can["Division_Code2"] = std::string(strItem);
			//					Can["Division_Code2"] = canData.function_division2;

			strItem.Format("%s",m_pView->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
			Can["Division_Code3"] = std::string(strItem);
			//					Can["Division_Code3"] = canData.function_division3;

			CanArray.append(Can);
		}


		int nIndex = pModule->GetInstalledCanMaster(nChannelID);
		for(int i = 0; i < pModule->GetInstalledCanSlave(nChannelID); i++)
		{
			Json::Value Can;
			SFT_CAN_SET_DATA canData = pModule->GetCANSetData(nChannelID, PS_CAN_TYPE_SLAVE, i);

			strItem.Format("S%d", i+1);
			Can["Can_Item"] = std::string(strItem);

			Can["Name"] = std::string(canData.name);

			if(pCanData[i+nIndex].data_type == 0 || pCanData[i+nIndex].data_type == 1 || pCanData[i+nIndex].data_type == 2 
				|| pCanData[i].data_type == 4)
			{
				strItem.Format("%.6f", pCanData[i+nIndex].canVal.fVal[0]);
				strItem.TrimRight("0");	
				strItem.TrimRight(".");
			}
			else if(pCanData[i+nIndex].data_type == 3)
				strItem.Format("%s", pCanData[i+nIndex].canVal.strVal);

			Can["value"] = std::string(strItem);

			if (nCanMasterIDType == 1)
				//strItem.Format("%ld", canData.canID);
				strItem.Format("%u", canData.canID); //ksj 20200323 : unsinged 타입으로 출력. extended id 대응.
			else
				strItem.Format("0x%x", canData.canID);

			Can["Can_Id"] = std::string(strItem);

			strItem.Format("%.3f", canData.fault_upper);				
			Can["Safety_Max"] = std::string(strItem);
			//					Can["Safety_Max"] = canData.fault_upper;

			strItem.Format("%.3f", canData.fault_lower);
			Can["Safety_Min"] = std::string(strItem);
			//					Can["Safety_Min"] = canData.fault_lower;

			strItem.Format("%.3f", canData.end_upper);
			Can["End_Max"] = std::string(strItem);
			//					Can["End_Max"] = canData.end_upper;

			strItem.Format("%.3f", canData.end_lower);
			Can["End_Min"] = std::string(strItem);
			//					Can["End_Min"] = canData.end_lower;

			strItem.Format("%s",m_pView->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division));
			Can["Division_Code1"] = std::string(strItem);
			//Can["divisionCode1"] = canData.function_division;

			strItem.Format("%s",m_pView->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division2));
			Can["Division_Code2"] = std::string(strItem);
			//Can["divisionCode2"] = canData.function_division2;

			strItem.Format("%s",m_pView->Fun_GetFunctionString(ID_CAN_TYPE, canData.function_division3));
			Can["Division_Code3"] = std::string(strItem);
			//Can["divisionCode3"] = canData.function_division3;

			CanArray.append(Can);
		}				
	}
	Channel["Can"] = CanArray;

#endif //JSON_MON_EXPORT_CAN 항목 출력 끝

	return 0;
}


//ksj 20200901 : 챔버 관련 항목 Json 객체 생성.
//서브클래싱 가능하도록 Protected 함수로 분리함.
//챔버 정보 생성은 향후 추가 개선 필요
int CJsonMon::MakeChannelInfoChamber(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel)
{
#ifdef JSON_MON_EXPORT_CHAMBER
	CString strTemp, strItem;
	int nChIndex = pChannel->GetChannelIndex();

	//채널에 할당된 장비에서 정보 가져와야된다.

	//챔버
	//Pack 은 챔버가 이상하게 구현되어있음..
	//챔버와 모듈,채널 관계가 하드코딩+엉망진창. v1017에서는 채널을 챔버에 할당하는 기존 cycler 방식으로 개선 시도해놓았음.
	//이 버전에서는.. 1개의 모듈만 사용. 2채널 or 4채널만 사용하는 것으로 구현되어있어서 그것에 맞게 구현함.
	//향후 챔버 구성에 따라 이 부분 문제 될 수 있음.
	//
	COvenCtrl* pOvenCtrl = NULL;
	if(m_pDoc->m_nChamberOperationType == 0) //한 챔버에 모든 채널 사용. 1번 챔버의 온도 무조건 리턴
	{
		pOvenCtrl = &m_pDoc->m_ChamberCtrl.m_ctrlOven1;
	}
	else
	{
		//채널 개수에따라 다름
		//2채널 장비는 1, 2채널이 각자 챔버하나씩 쓰는 경우.
		//4채널 장비는 12,34채널이 각자 하나씩 쓰는 경우
		int nTotalChCnt = pModule->GetTotalChannel();

		if(nTotalChCnt >= 4)//채널 수 4개
		{
			if(nChIndex < 2)
			{
				pOvenCtrl = &m_pDoc->m_ChamberCtrl.m_ctrlOven1; //1~2ch						
			}
			else
			{
				pOvenCtrl = &m_pDoc->m_ChamberCtrl.m_ctrlOven2; //3~4ch
			}
		}
		else //채널수 2개
		{
			if(nChIndex < 1)
			{
				pOvenCtrl = &m_pDoc->m_ChamberCtrl.m_ctrlOven1; //1ch
			}
			else
			{
				pOvenCtrl = &m_pDoc->m_ChamberCtrl.m_ctrlOven2; //2ch
			}
		}				
	}

	if(pOvenCtrl)
	{
		strTemp.Format("%.1f", pOvenCtrl->GetRefTemperature());
		Channel["Oven_Pv_Temp"] = std::string(strTemp);
		//Channel["Oven_Pv_Temp"] = pOvenCtrl->GetRefTemperature(); //설정온도
		strTemp.Format("%.1f", pOvenCtrl->GetCurTemperature());
		Channel["Oven_Sv_Temp"] = std::string(strTemp);
		//Channel["Oven_Sv_Temp"] = pOvenCtrl->GetCurTemperature(); //목표온도
		strTemp.Format("%.1f", pOvenCtrl->GetRefHumidity());
		Channel["Oven_Pv_Humi"] = std::string(strTemp);
		//Channel["Oven_Pv_Humi"] = pOvenCtrl->GetRefHumidity(); //설정습도
		strTemp.Format("%.1f", pOvenCtrl->GetCurHumidity());
		Channel["Oven_Sv_Humi"] = std::string(strTemp);
		//Channel["Oven_Sv_Humi"] = pOvenCtrl->GetCurHumidity(); //목표습도
	}

#endif //JSON_MON_EXPORT_CHAMBER
	return 0;
}

//ksj 20200901 : 칠러 관련 항목 Json 객체 생성.
int CJsonMon::MakeChannelInfoChiller(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel)
{
#ifdef JSON_MON_EXPORT_CHILLER
	CString strTemp, strItem;
	int nChIndex = pChannel->GetChannelIndex();

	//칠러
	int nItem=m_pMainframe->onGetSerialChillerNo(nChIndex+1); //칠러 번호 찾기.
	float fRefTemp=0, fCurTemp=0;
	int   iRefPump=0, iCurPump=0;
	if(nItem>=0 && nItem<MAX_CHILLER_COUNT)
	{
		fRefTemp=m_pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetRefTemperature();
		fCurTemp=m_pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetCurTemperature();
		iRefPump=m_pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nSP;//REF
		iCurPump=m_pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_PumpState.nPV;//CUR

		strTemp.Format("%.1f", fRefTemp);
		Channel["Chiller_Pv_Temp"] = std::string(strTemp);
		//Channel["Chiller_Pv_Temp"] = fRefTemp; //설정온도
		strTemp.Format("%.1f", fCurTemp);
		Channel["Chiller_Sv_Temp"] = std::string(strTemp);
		//Channel["Chiller_Sv_Temp"] = fCurTemp; //목표온도
		strTemp.Format("%d", iRefPump);
		Channel["Chiller_Pv_Flow"] = std::string(strTemp);
		//Channel["Chiller_Pv_Flow"] = iRefPump; //설정유량
		strTemp.Format("%d", iCurPump);
		Channel["Chiller_Sv_Flow"] = std::string(strTemp);
		//Channel["Chiller_Sv_Flow"] = iCurPump; //목표유량
	}	

#endif //JSON_MON_EXPORT_CHILLER
	return 0;
}

//ksj 20200901 : 파워서플라이 관련 항목 Json 객체 생성.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeChannelInfoPowerSupply(CCyclerModule* pModule, CCyclerChannel* pChannel, Json::Value& Channel)
{
#ifdef JSON_MON_EXPORT_POWERSUPPLY
	CString strTemp, strItem;
	int nChIndex = pChannel->GetChannelIndex();
	CScheduleData *pSchedule=pChannel->GetScheduleData();//pSchedule 포인터 사용시 Null Pointer 체크 주의.	

	//파워서플라이
	int nOutp=0;
	float fCur_Volt=0, fSet_Volt=0;
	int nItem=mainFrm->onGetSerialPowerNo(nChIndex+1,nOutp); //파워 번호 찾기.
	if(nItem>=0 && nItem<MAX_POWER_COUNT && nOutp > 0)
	{
		fCur_Volt=mainFrm->m_PwrSupplyCtrl[nItem].m_fState_Volt[nOutp-1];

		if(pSchedule) //설정전압은 스케쥴에서 가져온다.
		{
			CStep* pStep = pSchedule->GetStepData(pChannel->GetStepNo()-1);
			if(pStep) fSet_Volt=(float)pStep->m_nPwrSupply_Volt/1000.f;
			//else fSet_Volt = -888.88f; //error. 없는 스텝
		}
		else
		{
			//fSet_Volt = -999.99f; //error. 스케쥴 없음.
		}			

		strTemp.Format("%.1f", fSet_Volt);
		Channel["Power_Pv_Volt"] = std::string(strTemp);
		//Channel["Power_Pv_Volt"] = fSet_Volt; //설정파워
		strTemp.Format("%.1f", fCur_Volt);
		Channel["Power_Sv_Volt"] = std::string(strTemp);
		//Channel["Power_Sv_Volt"] = fCur_Volt; //목표파워
	}

#endif //JSON_MON_EXPORT_POWERSUPPLY
	return 0;
}

//ksj 20200901 : 챔버 관련 항목 Json 객체 생성.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeChannelInfoTotalChamber(Json::Value& root)
{
#ifdef JSON_MON_EXPORT_CHAMBER
	CString strTemp, strItem;

	//OVEN
	Json::Value OvenArray;

	BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven", FALSE);
	BOOL bUseOven1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	BOOL bUseOven2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", FALSE);

	int nOvenNumber = 0;
	if(!bUseOven) nOvenNumber = 0;
	else
	{
		if(bUseOven1) nOvenNumber++;
		if(bUseOven2) nOvenNumber++;
	}
	COvenCtrl* pOvenCtrl[2] = {NULL,};
	pOvenCtrl[0] = &m_pDoc->m_ChamberCtrl.m_ctrlOven1;	
	pOvenCtrl[1] = &m_pDoc->m_ChamberCtrl.m_ctrlOven2;

	strTemp.Format("%d",nOvenNumber);
	root["Oven_Number"] = std::string(strTemp);
	//root["Oven_Number"] = nOvenNumber;

	for(int nOvenIdx=0;nOvenIdx<nOvenNumber;nOvenIdx++)
	{
		Json::Value Oven;
		if(pOvenCtrl[nOvenIdx] == NULL) continue;

		strTemp.Format("%d",  nOvenIdx+1);
		Oven["Oven_Room_Number"] = std::string(strTemp);
		//Oven["Oven_Room_Number"] = nOvenIdx+1;
		strTemp.Format("%.1f", pOvenCtrl[nOvenIdx]->GetCurTemperature());
		Oven["Pv_Temp"] = std::string(strTemp);
		//Oven["Pv_Temp"] = pOvenCtrl[nOvenIdx]->GetCurTemperature();
		strTemp.Format("%.1f", pOvenCtrl[nOvenIdx]->GetRefTemperature());
		Oven["Sv_Temp"] = std::string(strTemp);
		//Oven["Sv_Temp"] = pOvenCtrl[nOvenIdx]->GetRefTemperature();
		strTemp.Format("%.1f", pOvenCtrl[nOvenIdx]->GetCurHumidity());
		Oven["Pv_Humi"] = std::string(strTemp);
		//Oven["Pv_Humi"] = pOvenCtrl[nOvenIdx]->GetCurHumidity();
		strTemp.Format("%.1f", pOvenCtrl[nOvenIdx]->GetCurHumidity());
		Oven["Sv_Humi"] = std::string(strTemp);
		//Oven["Sv_Humi"] = pOvenCtrl[nOvenIdx]->GetCurHumidity();
		strTemp.Format("%d",  pOvenCtrl[nOvenIdx]->GetRunWorkMode(0));
		Oven["Oven_Mode"] = std::string(strTemp);
		//Oven["Oven_Mode"] = pOvenCtrl[nOvenIdx]->GetRunWorkMode(0);
		//Oven["Pattern_Number"] = 0; //현재 컨트롤러에서 패턴 정보 불러오는 것 구현 안되어있어서 값 못가져옴.
		Oven["State"] = (pOvenCtrl[nOvenIdx]->GetRun(0))? "RUN":"STOP";
		strTemp.Format("%d",  pOvenCtrl[nOvenIdx]->GetOvenStatus());
		Oven["Alm_Code"] = std::string(strTemp);
		//Oven["Alm_Code"] = pOvenCtrl[nOvenIdx]->GetOvenStatus();

		CDWordArray adwCh;
		pOvenCtrl[nOvenIdx]->GetChannelMapArray(0, adwCh);

		CString strChList;
		strTemp.Empty();
		for(int a = 0; a<adwCh.GetSize(); a++)
		{
			DWORD dwData = adwCh.GetAt(a);
			strTemp.Format("M%02dCH%02d,", HIWORD(dwData),LOWORD(dwData)+1);		
			strChList += strTemp;
		}		
		Oven["Assign_Channel_Num"] = std::string(strChList); //챔버에 할당된 채널 리스트 저장. 그러나 Pack은 동적으로 할당된 채널과 관계없이 하드코딩되어있음. 현재 무의미.

		OvenArray.append(Oven);
	}		
	root["Oven"] = OvenArray;

#endif //JSON_MON_EXPORT_CHAMBER
	return 0;
}

//ksj 20200901 : 칠러 관련 항목 Json 객체 생성.
//서브클래싱 가능하도록 Protected 함수로 분리함.
int CJsonMon::MakeChannelInfoTotalChiller(Json::Value& root)
{
#ifdef JSON_MON_EXPORT_CHILLER
	CString strTemp, strItem;

	//chiller
	Json::Value ChillerArray;

	int nChillerNumber = 0;
	BOOL bUseChiller[MAX_CHILLER_COUNT] = {0,};

	bUseChiller[0] = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1 , "Use", 0);
	bUseChiller[1] = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2 , "Use", 0);
	bUseChiller[2] = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3 , "Use", 0);
	bUseChiller[3] = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4 , "Use", 0);

	for(int nChillerIdx=0; nChillerIdx < MAX_CHILLER_COUNT; nChillerIdx++)
	{
		if(bUseChiller[nChillerIdx] == TRUE)
		{
			Json::Value Chiller;
			strTemp.Format("%d",  nChillerIdx+1);
			Chiller["Chiller_Room_Number"] = std::string(strTemp);
			//Chiller["Chiller_Room_Number"] = nChillerIdx+1;
			strTemp.Format("%.1f", m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetCurTemperature());
			Chiller["Pv_Temp"] = std::string(strTemp);
			//Chiller["Pv_Temp"] = m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetCurTemperature();
			strTemp.Format("%.1f", m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetRefTemperature());
			Chiller["Sv_Temp"] = std::string(strTemp);
			//Chiller["Sv_Temp"] = m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetRefTemperature();
			Chiller["Temp_Mode"] = (m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetRunWorkMode(0))? "FIX":"PROG";
			Chiller["Temp_State"] = (m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetRun(0))? "RUN":"STOP";
			strTemp.Format("%d", m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetOvenStatus());
			Chiller["Temp_Alm_Code"] = std::string(strTemp);
			//Chiller["Temp_Alm_Code"] = m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.GetOvenStatus();
			strTemp.Format("%d", m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nPV);
			Chiller["Pv_Flow"] = std::string(strTemp);
			//Chiller["Pv_Flow"] = m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nPV;
			strTemp.Format("%d", m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nSP);
			Chiller["Sv_Flow"] = std::string(strTemp);
			//Chiller["Sv_Flow"] = m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nSP;
			Chiller["Flow_Mode"] = (!m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nMode)? "FIX":"PROG"; //mode가 fix prog 인지 확실치 않음.
			Chiller["Flow_State"] = (!m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nPlay)? "RUN":"STOP";
			strTemp.Format("%d", m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nError);
			Chiller["Flow_Alm_Code"] = std::string(strTemp);
			//Chiller["Flow_Alm_Code"] = m_pDoc->m_chillerCtrl[nChillerIdx].m_ctrlOven.m_PumpState.nError;
			//strTemp.Format("%d", m_pDoc->m_chillerCtrl[nChillerIdx].m_nCHNO); //ksj 20200909 : 주석처리
			strTemp.Format("M%02dCH%02d", 1, m_pDoc->m_chillerCtrl[nChillerIdx].m_nCHNO); //ksj 20200909 : 단순 채널 번호가 아닌 모듈채널 번호로 표현하도록 변경. 그러나 현재 칠러 연동시 모듈 선택 기능은 없다;;. 향후 다수 모듈을 고려하여 모듈/채널 방식으로 표현
			Chiller["Assign_Channel_Num"] = std::string(strTemp);
			//Chiller["Assign_Channel_Num"] = m_pDoc->m_chillerCtrl[nChillerIdx].m_nCHNO;


			nChillerNumber++; //활성화된 칠러 개수 카운트

			ChillerArray.append(Chiller);
		}
	}	

	strTemp.Format("%d",nChillerNumber);
	root["Chiller_Number"] = std::string(strTemp);
	//root["Chiller_Number"] = nChillerNumber;
	root["Chiller"] = ChillerArray;

#endif //JSON_MON_EXPORT_CHILLER
	return 0;
}


////////////////////////////////////////////////////////////////////////
//가상함수 Make~~~ 함수 구현부 끝
///////////////////////////////////////////////////////////////////////

//ksj 20201020 : 통신 두절 시간 갱신.
//함수가 호출된 시간을 기준으로 통신 두절 시간을 생성한다.
//함수 호출할때 기준으로 시간 갱신됨.
void CJsonMon::UpdateModuleOffTime(int nModuleID)
{
	if(nModuleID < 1) return;

	SYSTEMTIME sysTime;
	::GetLocalTime(&sysTime);  		
	CTime time(sysTime);
	m_strOffTimeDay[nModuleID-1] = time.Format("%Y%m%d"); //접속해제날짜
	m_strOffTime[nModuleID-1] = time.Format("%H%M%S"); //접속해제시간
}

//ksj 20201020 : 접속 시간 갱신.
//최초 SBC 처음 접속했을때 저장해놓은 정보로 갱신한다.
//함수 여러번 호출해도 값 변화 없음
void CJsonMon::UpdateModuleOnTime(int nModuleID)
{
	SYSTEMTIME *pTime = ::SFTGetConnectedTime(nModuleID);	//저장되어있는 접속 시간 가져오기
	if(pTime)
	{
		CTime time(*pTime);
		m_strOnTimeDay[nModuleID-1] = time.Format("%Y%m%d"); //접속날짜
		m_strOnTime[nModuleID-1] = time.Format("%H%M%S"); //접속시간
	}
}

//ksj 20201020 : 해당 모듈의 접속후 경과 시간을 리턴한다.
BOOL CJsonMon::GetConnectedElapsedTime(CCyclerModule* pModule, CString& strConnectedDay, CString& strConnectedTime)
{
	if(!pModule) return FALSE;
	if(pModule->GetState() == PS_STATE_LINE_OFF) return FALSE;
	int nModuleID = pModule->GetModuleID();
	

	SYSTEMTIME *pTime = ::SFTGetConnectedTime(nModuleID);	
	
	if(pTime)
	{
		SYSTEMTIME  curTime; 
		::GetLocalTime(&curTime);

		CTime cononnectTime(*pTime);
		CTime currentTime(curTime);
		CTimeSpan elapsedTime = currentTime - cononnectTime;
		
		strConnectedDay.Format("%d",elapsedTime.GetDays());
		strConnectedTime.Format("%02d%02d%02d",elapsedTime.GetHours(),elapsedTime.GetMinutes(),elapsedTime.GetSeconds());	
		
		return TRUE;
	}

	return FALSE;
}
