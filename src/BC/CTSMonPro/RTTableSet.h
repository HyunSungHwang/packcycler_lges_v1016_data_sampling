#if !defined(AFX_RTTABLESET_H__A7DB61EF_3F7C_42FE_ADD9_F545C71CBF6A__INCLUDED_)
#define AFX_RTTABLESET_H__A7DB61EF_3F7C_42FE_ADD9_F545C71CBF6A__INCLUDED_

#include "CTSMonProDoc.h"
#include "afxwin.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RTTableSet.h : header file
// yulee 20181114 RT table 수정 기능

#define LIST_RT_INDEX			0
#define LIST_RT_STATE			1 //0:최신(SBC와 파일이 같음), 1:수정, 2:추가
#define LIST_RT_TABLE_NAME		2 //고객사에서 사용하는 이름 
#define LIST_RT_UPLOAD_TIME		3 //SBC에 업로드된 시간(YYYY-MM-DD-HH:MM:SS)
#define LIST_RT_TABLE_SBC_NAME	4 //SBC에서 사용하는 Table이름, 예: th_table_00.txt, th_table_01.txt...
#define LIST_RT_RESERVED		5 //그외

#define RT_STATE_EMPTY			0 //공란
#define RT_STATE_LATEST			1 //최신
#define RT_STATE_MODIFIED		2 //수정
#define RT_STATE_ADD			3 //추가
#define RT_STATE_ONTEST			4 //사용 중

#define NAME_CHAR_MAX_LENGTH	16
#define TIME_CHAR_MAX_LENGTH	32

#define RT_WORK_STATE_ON_UPDATE		0
#define RT_WORK_STATE_END_UPDATE	1
#define RT_WORK_STATE_FAIL_UPDATE	2
#define RT_WORK_STATE_ON_ADD		3
#define RT_WORK_STATE_END_ADD		4
#define RT_WORK_STATE_FAIL_ADD		5
#define RT_WORK_STATE_ON_CHANGE		6
#define RT_WORK_STATE_END_CHANGE	7
#define RT_WORK_STATE_FAIL_CHANGE	8
#define RT_WORK_STATE_CLOSE			9


#define RT_TABLE_INIT_ALL		0		
#define RT_TABLE_UPDATE			1

//#define LIST_MAX_FILE_NUM		9 //yulee 20181127 변경
#define LIST_MAX_FILE_NUM		11 //lyj 20200213 최대 RTTable 10개

#define TIMER_RTTABLE_SET_SBC_RECV	100			//yulee 20181120

#define RT_TABLE_MAX_LINE	300			//yulee 20181121

#define RTTABLE_REGISTRY_KEY		"Config"

#define SBC_RETRUN_COUNT         5

//저장할 데이터 구조체
typedef struct _S_WORK_RTTABLE_DATA
{
	int nIndex;
	int nState;
	char charTableName[NAME_CHAR_MAX_LENGTH];		//table name for user //CString strModelName;
	char charWorkTime[TIME_CHAR_MAX_LENGTH];		//SBC Upload time
	char charTableNameForSBC[NAME_CHAR_MAX_LENGTH];	//SBC Table Name 
	char charReserved[NAME_CHAR_MAX_LENGTH];
	
}S_RTTABLE_DATA, *LPS_RTTABLE_DATA;

/////////////////////////////////////////////////////////////////////////////
// CRTTableSet dialog

class CRTTableSet : public CDialog
{
// Construction
public:

	CRTTableSet(CWnd* pParent = NULL);   // standard constructor
	void InitList();
	void InitRTTableData();
	BOOL SetRTTableList();
	BOOL DownRTtableFromSBC();
	void SetRTTableInfo();
	BOOL ReadRTTableInfo(int nTypeOper = 0);
	BOOL SetRTTableInfoGrid();
	BOOL SetListItem(int nI, LPS_RTTABLE_DATA pReservData);
	CString ConvertStateCodeToString(int nState);
	BOOL UpdateTableInfo();  //yulee 20181118
	BOOL CheckUsingTable(BOOL *bRTtable);  //yulee 20181118
	void SetWndEnable(); //yulee 20181120
	void SetRTtableNametoDB();  //yulee 20181120
	void ResetRTtable(CString &strName);
	void ResetRTWnd(CString &strName);
	void SetWndEnable(int nTypeOper);
	BOOL ReadFileInfo(CString strTargetFileName, CString &strFileDir, CString &strFileName, int nTypeOper = 0); //yulee 20181120
	BOOL BackupFileforChange(CString &strReadFileName, CString &FilePath, CString &FileName, CString &BackupFilePath, int nTypeOper = 0);//yulee 20181120
	BOOL CopyNewFileforChange(int nIndex);//yulee 20181120
	BOOL ChangeRTTableArr(int &nIndex, CString &strFilePath, CString &strFileName);//yulee 20181120
	BOOL SendRTTableInfotoSBC(CString strFileName, CString &strBackupFileNname, int nTypeOper);
	BOOL DeleteRTTablesFolder();
	BOOL IsSpaceInStr(CString &strFilePath);
	BOOL RTTableFormatCheck(CString &strFilePath, int &nErrNum, CString &ErrLoc);
	void SetSBCRTTableAckRecv(int retFromSBC);//yulee 20181122
	void SetSBCRTHumiTableAckRecv(int retFromSBC);//ksj 20200207
//	S_RTTABLE_DATA m_pArrRTtable[10];
	S_RTTABLE_DATA m_pArrRTtable[12]; //lyj 20200213 최대 RTTable 10개
	CPtrArray	m_ptrArrRTtableData;
	int		m_nTotalRTtableCnt;
	CString m_strBackupFileName;
	CCTSMonProDoc * m_pDoc;
	int m_bRetCnt;
	
// 	CString strTableName;		//table name for user //CString strModelName;
// 	CString srtUpLoadTime;		//SBC Upload time
// 	CString strTableNameForSBC;	//SBC Table Name 
// 	CString strReserved;

// Dialog Data
	//{{AFX_DATA(CRTTableSet)
	enum { IDD = IDD_RTTABLE_SET_DLG , IDD2 = IDD_RTTABLE_SET_DLG_ENG , IDD3 = IDD_RTTABLE_SET_DLG_PL };
	CProgressCtrl	m_ctrlRTtableProgress;
	CListCtrl m_listRTable;
	//}}AFX_DATA
~CRTTableSet()
{
	free(m_pArrRTtable);
}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRTTableSet)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRTTableSet)
	virtual BOOL OnInitDialog();
	afx_msg void OnButRttableAdd();
	afx_msg void OnButRttableModi();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	// ksj 20200207 : 온도 인지 습도인지 타입 선택
	CComboBox m_cmbRtType;
	afx_msg void OnCbnSelchangeComboRtType();
	int m_nSelectedRtType;
	// ksj 20200207
	BOOL UpdateRtTable(void);
	int EnableFuntions(void);
};



//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RTTABLESET_H__A7DB61EF_3F7C_42FE_ADD9_F545C71CBF6A__INCLUDED_)
