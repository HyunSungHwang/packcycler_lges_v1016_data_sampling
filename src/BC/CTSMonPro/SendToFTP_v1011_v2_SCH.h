#ifndef __SendToFTP_v1011_v2_SCH__
#define __SendToFTP_v1011_v2_SCH__

#pragma once


#include "CTSMonProDoc.h"

class CSendToFTP_v1011_v2_SCH
{
public:
	CSendToFTP_v1011_v2_SCH(void);
	~CSendToFTP_v1011_v2_SCH(void);

	
	BOOL SendSchFTP_v1011_v2_SCH(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, int nOption,int ReserveID, CCTSMonProDoc* pDoc);
};

#endif