#if !defined(AFX_CALRESULTDLG_H__7CB1C9F6_C520_4EDB_AD48_AB3CCF7274F8__INCLUDED_)
#define AFX_CALRESULTDLG_H__7CB1C9F6_C520_4EDB_AD48_AB3CCF7274F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalResultDlg.h : header file
//

#include "ColorListBox.h"
/////////////////////////////////////////////////////////////////////////////
// CCalResultDlg dialog
class CCTSMonProView;
class CCalResultDlg : public CDialog
{
// Construction
public:
	void SetReceiveResultData(LPSFT_CALI_END_DATA pReceivedCalData);
	void SetPointData();
	void MakeList(int nMode);
	void ClearList(int nMode);
	void SetData(int nMode, long lCmdID, long lCmdPoint, long lADVal, long lDVMVal);
//	void SetConfigurationData(CAL_POINT* pCalPoint, CAL_POINT* pCheckPoint, int nModuleIndex, int nChannelIndex);
	void OnCommConnected();
	void OnCommDisconnected();
	void OnCalDataModified();

	CCalResultDlg(CCTSMonProView* pView, CWnd* pParent = NULL);   // standard constructor

	LPSFT_CALI_END_DATA m_pCalResultData;
//	CAL_POINT* m_pCalPoint;
//	CAL_POINT* m_pCheckPoint;
	int m_nModuleID;
	int m_nChannelID;
	CPtrArray m_aCalResultData_H;
	CPtrArray m_aCalResultData_L;
	CPtrArray m_aCalResultData_M;
	CCTSMonProView* m_pView;
// Dialog Data
	//{{AFX_DATA(CCalResultDlg)
	enum { IDD = IDD_CAL_RESULT_DLG , IDD2 = IDD_CAL_RESULT_DLG_ENG};
	CColorListBox	m_wndCheckResult;
	CListCtrl	m_wndResult_H;
	CListCtrl	m_wndResult_L;
	CListCtrl	m_wndResult_M;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalResultDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCalResultDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnSetPoint();
	afx_msg void OnBtnConnect();
	afx_msg void OnBtnDisconnect();
	afx_msg void OnBtnCaliCrtH();
	afx_msg void OnBtnCaliCrtL();
	afx_msg void OnBtnCaliCrtM();
	afx_msg void OnBtnUpdate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALRESULTDLG_H__7CB1C9F6_C520_4EDB_AD48_AB3CCF7274F8__INCLUDED_)
