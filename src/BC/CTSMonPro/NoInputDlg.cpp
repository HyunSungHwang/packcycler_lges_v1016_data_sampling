// NoInputDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "NoInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNoInputDlg dialog


CNoInputDlg::CNoInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CNoInputDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CNoInputDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CNoInputDlg::IDD3):
	(CNoInputDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CNoInputDlg)
	m_nCycleNo = 0;
	m_nStepNo = 0;
	//}}AFX_DATA_INIT
}


void CNoInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNoInputDlg)
	DDX_Text(pDX, IDC_EDIT1, m_nCycleNo);
	DDX_Text(pDX, IDC_EDIT2, m_nStepNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNoInputDlg, CDialog)
	//{{AFX_MSG_MAP(CNoInputDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNoInputDlg message handlers

BOOL CNoInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	if(!m_strTitle.IsEmpty())
	{
		GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(m_strTitle);
	}
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNoInputDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();

	if(m_nCycleNo < 1)		
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_NO_INSERT_DIALOG"));
		//@ AfxMessageBox("Cycle 번호는 0보다 큰값을 입력해야 합니다.");
		GetDlgItem(IDC_EDIT1)->SetFocus();
		return;
	}

	if(m_nStepNo < 1)		
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_NO_INSERT_DIALOG"));
		//@ AfxMessageBox("Step 번호는 0보다 큰값을 입력해야 합니다.");
		GetDlgItem(IDC_EDIT2)->SetFocus();
		return;
	}

	CDialog::OnOK();
}