// WorkWarnin.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "WorkWarnin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWorkWarnin dialog


CWorkWarnin::CWorkWarnin(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CWorkWarnin::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CWorkWarnin::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CWorkWarnin::IDD3):
	(CWorkWarnin::IDD), pParent)
{
	//{{AFX_DATA_INIT(CWorkWarnin)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CWorkWarnin::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWorkWarnin)
	DDX_Control(pDX, IDC_LAB_MESSAGE, m_ctrlLabMessage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWorkWarnin, CDialog)
	//{{AFX_MSG_MAP(CWorkWarnin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWorkWarnin message handlers

BOOL CWorkWarnin::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if(m_strBtn_OK=="")
	{
	}
	else
	{
		GetDlgItem(IDOK)->SetWindowText(m_strBtn_OK);
	}

	if(m_strBtn_Cancle=="")
	{
	}
	else
	{
		GetDlgItem(IDCANCEL)->SetWindowText(m_strBtn_Cancle);
	}

	// TODO: Add extra initialization here
	InitControl();
	DrawText();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CWorkWarnin::InitControl()
{
	//판정 레이블 설정
	m_ctrlLabMessage.SetFontSize(25)
		.SetTextColor(RED)
		.SetBkColor(WHITE)
		//		.SetGradientColor(RGB(255,255,255))
		.SetFontBold(TRUE)
		//		.SetGradient(TRUE)
		.SetFontName(Fun_FindMsg("InitControl_msg","IDD_WORK_WARNING"));
	//@ 	.SetFontName("HY헤드라인M");
	//		.SetSunken(TRUE);
}

void CWorkWarnin::Fun_SetMessage(CString strMsg)
{
	m_strMessage = strMsg;
}

void CWorkWarnin::DrawText()
{
	if (m_strMessage.IsEmpty()) return;
	m_ctrlLabMessage.SetText(m_strMessage)
		//			.SetGradient(FALSE)
		//			.SetBkColor(ORANGE)
		//			.SetTextColor(RGB(255, 255, 255))
		.FlashText(TRUE);
}

void CWorkWarnin::Fun_SetButton_OK(CString strMsg)
{
	m_strBtn_OK.Format("%s",strMsg);
}
void CWorkWarnin::Fun_SetButton_Cancle(CString strMsg)
{
	m_strBtn_Cancle.Format("%s",strMsg);
}
