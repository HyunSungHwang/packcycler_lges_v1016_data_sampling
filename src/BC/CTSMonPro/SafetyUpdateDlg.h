#if !defined(AFX_SAFETYUPDATEDLG_H__59C39EF2_1729_452F_B748_7DAEE78213CD__INCLUDED_)
#define AFX_SAFETYUPDATEDLG_H__59C39EF2_1729_452F_B748_7DAEE78213CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SafetyUpdateDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSafetyUpdateDlg dialog

class CSafetyUpdateDlg : public CDialog
{
// Construction
public:
	void EnableUpdate();
	void DisableUpdate();
	void SetReceiveInfoData(SFT_CMD_SAFETY_UPDATE_REPLY *safetyInfoData);
	//void Fun_SendSafetyCmd();
	BOOL Fun_SendSafetyCmd();
	CSafetyUpdateDlg(int nModuleID,int nChIndex, CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor
	static UINT SendCommandThread(LPVOID lpParam);
	void InitDataUnit();
	void WriteSafetyUpdateLog(SFT_CMD_SAFETY_UPDATE_REPLY* pSrc, SFT_CMD_SAFETY_UPDATE_REPLY* pDst);
	BOOL UpdateSchSafety();
	CCTSMonProDoc * m_pDoc;
	int m_nModuleID;
	int m_nChIndex;
	SFT_CMD_SAFETY_UPDATE_REPLY m_sSafetyInfoData;

// Dialog Data
	//{{AFX_DATA(CSafetyUpdateDlg)
	enum { IDD = IDD_SAFETY_UPDATE_DLG , IDD2 = IDD_SAFETY_UPDATE_DLG_ENG , IDD3 = IDD_SAFETY_UPDATE_DLG_PL };
	CString	m_edit_capa_max;
	CString	m_edit_cell_dev_max;
	CString	m_edit_current_max;
	CString	m_edit_power_max;
	CString	m_edit_volt_max;
	CString	m_edit_volt_min;
	CString	m_edit_wh_max;
	CString m_edit_delta_std_min_v; //lyj 20200214
	CString m_edit_delta_std_max_v; //lyj 20200214
	CString m_edit_delta_min_v; //lyj 20200214
	CString m_edit_delta_v; //lyj 20200214
	CString m_edit_delta_max_v; //lyj 20200214
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSafetyUpdateDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSafetyUpdateDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButSafetyRequest();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnChangeEditVoltMin();
	afx_msg void OnChangeEditVoltMax();
	afx_msg void OnChangeEditPowerMax();
	afx_msg void OnChangeEditCapaMax();
	afx_msg void OnChangeEditCurrentMax();
	afx_msg void OnChangeEditWhMax();
	afx_msg void OnChangeEditCellDevMax();
	afx_msg void OnEnChangeEditSafetyCellDeltaMinV();
	afx_msg void OnEnChangeEditSafetyCellDeltaStdMinV();
	afx_msg void OnEnChangeEditSafetyCellDeltaStdMaxV();
	afx_msg void OnEnChangeEditIdcSafetyCellDeltaV();
	afx_msg void OnEnChangeEditSafetyCellDeltaV();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bUpdate;
	double m_fVUnit;
	double m_fIUnit;
	double m_fCUnit;
	double m_fWUnit;
	double m_fWhUnit;
	
	int m_nVDec;
	int m_nIDec;
	int m_nCDec;
	int m_nWDec;
	int m_nWhDec;
	
	CCyclerChannel *m_pCH; //ksj 20170810

public:
	afx_msg void OnEnChangeEditSafetyCellDeltaMaxV();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAFETYUPDATEDLG_H__59C39EF2_1729_452F_B748_7DAEE78213CD__INCLUDED_)
