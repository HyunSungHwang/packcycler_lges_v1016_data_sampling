// DCLoad.cpp: implementation of the CDCLoad class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "DCLoad.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void CDCLoad::Fun_ReConnect(int nNum)
{
	m_bPortOpen =FALSE;
	Sleep(1000);
//	m_pSerial->StopMonitoring();
//	Sleep(1000);

	InitSerialPortUseOven(nNum);

	// 	for(int i = 0 ; i < m_nInstallCount; i++)
	// 	{
	// 		ZeroMemory(szRxBuff,sizeof(szRxBuff));
	// 		ZeroMemory(szTxBuff,sizeof(szTxBuff));
	// 		//통신 불능 연속 시간 Rest 한다.
	// 		m_OvenData[i].m_nDisconnectCnt = 0;
	// 		nTxSize = GetReqStrOvenVersionCmd(i, szTxBuff, nRxSize);
	// 		pPort->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize);
	// 	}
	m_bPortOpen =TRUE;
}

BOOL CDCLoad::InitSerialPortUseOven(int nNum)
{
	if (m_pSerial)
	{
		delete m_pSerial;
		m_pSerial = NULL;
	}

	m_bPortOpen =FALSE;
	//SERIAL_CONFIG	m_SerialConfig;
	//CSerialPort *pPort= NULL;
	//pPort = new CSerialPort();
	//m_pSerial = pPort; 

	m_pSerial = new CSerialPort();
	if (nNum == 1)
	{
		m_SerialConfig.nPortNum      = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG5, "PortNo", 2);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "StopBit", 1);
		m_SerialConfig.nPortBuffer   = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Buffer", 512);
		m_SerialConfig.portParity    = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Parity", 'N');
		m_SerialConfig.nPortEvents   = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
		
	}
	else if (nNum == 2)
	{
		m_SerialConfig.nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG6, "PortNo", 3);
		m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "BaudRate", 9600);
		m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "DataBit", 8);
		m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "StopBit", 1);
		m_SerialConfig.nPortBuffer = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Buffer", 512);
		m_SerialConfig.portParity = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Parity", 'N');
		m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
	}
	
	if(m_pSerial->IsPortOpen())	//포트가 열려있으면 닫는다.
	{
		//	m_Serial[i].StopMonitoring();
		m_pSerial->DisConnect();
		Sleep(1000);
	}
	
	BOOL flag = m_pSerial->InitPort(AfxGetApp()->m_pMainWnd->m_hWnd,
									m_SerialConfig.nPortNum, 
									m_SerialConfig.nPortBaudRate,
									m_SerialConfig.portParity,
									m_SerialConfig.nPortDataBits,
									m_SerialConfig.nPortStopBits,
									m_SerialConfig.nPortEvents,
									m_SerialConfig.nPortBuffer);
	if( flag == FALSE )
	{
		CString strErrorMsg;
		//strErrorMsg.Format("COM %d 포트 초기화를 실패하였습니다!", m_SerialConfig.nPortNum);
		strErrorMsg.Format(Fun_FindMsg("DCLoad_InitSerialPortUseOven_msg1","DCLOAD"), m_SerialConfig.nPortNum);//&&
		//AfxMessageBox(strErrorMsg);
		onlogsave(_T("state"),strErrorMsg,0); //lyj 20210809
		return FALSE;
	}
	
	//char szTxBuff[32], szRxBuff[1024];
	//int nRxSize = 0;
	//int nTxSize;
	
	m_pSerial->StartMonitoring();
	//for(int i = 0 ; i < m_nInstallCount; i++)
	//{
	//	ZeroMemory(szRxBuff,sizeof(szRxBuff));
	//	ZeroMemory(szTxBuff,sizeof(szTxBuff));
	//	//통신 불능 연속 시간 Rest 한다.
	//	m_OvenData[i].m_nDisconnectCnt = 0;
	//	nTxSize = GetReqStrOvenVersionCmd(i, szTxBuff, nRxSize);
	//	pPort->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize);
	//}
	m_bPortOpen =TRUE;
	return TRUE;
}

BOOL CDCLoad::SetSYSTem()
{
	if (m_bPortOpen != TRUE) return FALSE;
	m_strRecv = m_pSerial->ReadString2(_T("01SYST:REM"));
	TRACE("recv = %s \n", m_strRecv);
	Sleep(2000);
// 	m_strRecv = m_pSerial->ReadString2(_T("01LOAD 0"));
// 	TRACE("recv = %s \n", m_strRecv);
// 	Sleep(1000);
// 	m_strRecv = m_pSerial->ReadString2(_T("01MODE:CP"));
// 	TRACE("recv = %s \n", m_strRecv);
	return TRUE;
}

void CDCLoad::SetLoadOnOff(BOOL bOnOff)
{
	if (m_bPortOpen != TRUE) return;
	if (bOnOff)	m_strRecv = m_pSerial->ReadString2(_T("01LOAD 1"));
	else m_strRecv = m_pSerial->ReadString2(_T("01LOAD 0"));
}
void CDCLoad::SetMode(sl200ModeType mode)
{
	if (m_bPortOpen != TRUE) return;
	switch(mode) {
	case SL_MODE_CC:
		m_strRecv = m_pSerial->ReadString2(_T("01MODE:CC"));
		break;
	case SL_MODE_CV:
		m_strRecv = m_pSerial->ReadString2(_T("01MODE:CV"));
		break;
	case SL_MODE_CR:
		m_strRecv = m_pSerial->ReadString2(_T("01MODE:CR"));
		break;
	case SL_MODE_CP:
		m_strRecv = m_pSerial->ReadString2(_T("01MODE:CP"));
		break;
	}
}
void CDCLoad::SetCurrentValue(float value)
{
	if (m_bPortOpen != TRUE) return;
	CString strTemp;
	strTemp.Format("01SOUR:CURR %.02f",value);
	m_strRecv = m_pSerial->ReadString2(strTemp);
}
void CDCLoad::SetVoltageValue(float value)
{
	if (m_bPortOpen != TRUE) return;
	CString strTemp;
	strTemp.Format("01SOUR:VOLT %.02f",value);
	m_strRecv = m_pSerial->ReadString2(strTemp);
}
void CDCLoad::SetResistanceValue(float value)
{
	if (m_bPortOpen != TRUE) return;
	CString strTemp;
	strTemp.Format("01SOUR:RES %.02f",value);
	m_strRecv = m_pSerial->ReadString2(strTemp);
}
void CDCLoad::SetPowerValue(float value)
{
	if (m_bPortOpen != TRUE) return;
	CString strTemp;
	strTemp.Format("01SOUR:POW %.02f",value);
	m_strRecv = m_pSerial->ReadString2(strTemp);
}

CString	CDCLoad::ReqVersion()
{
	if (m_bPortOpen != TRUE) return m_strVer;
	m_strVer = m_pSerial->ReadString2(_T("01system:version?"));
	return m_strVer;
}
CString	CDCLoad::ReqMode()			//CC/CV/CR/CP
{
	if (m_bPortOpen != TRUE) return m_strMode;
	m_strMode = m_pSerial->ReadString2(_T("01MODE?"));
	return m_strMode;
}
CString CDCLoad::ReqLoadStat()			//LOAD ON/ LOAD OFF
{
	if (m_bPortOpen != TRUE) return  "OFF";
	m_strRecv = m_pSerial->ReadString2(_T("01LOAD?"));
	if (m_strRecv == "1") m_strLoadStat = "ON";
	else  m_strLoadStat = "OFF";
	
	return m_strLoadStat;
}
float CDCLoad::ReqMeasureCurrent()
{
	m_fCurCurr = 0.0f;
	if (m_bPortOpen != TRUE) return m_fCurCurr;
	m_strRecv = m_pSerial->ReadString2(_T("01MEAS:CURR?"));
	
// 	CString strWriteData=_T("01MEAS:CURR?");
// 	int nSendCnt;
// 	char *cSendData;
// 	nSendCnt = strWriteData.GetLength()+2;
// 	cSendData = new char[nSendCnt];
// 	ZeroMemory(cSendData, sizeof(cSendData));
// 	sprintf(cSendData, "%s",strWriteData);
// 	cSendData[nSendCnt-2] = 0x0a;
// 	cSendData[nSendCnt-1] = NULL;
// 	//WriteToPort(cSendData, nSendCnt);
// 
// 	char szTxBuff[100], szRxBuff[512];
// 	
// 	//ZeroMemory(szTxBuff,sizeof(szTxBuff));
// 	ZeroMemory(szRxBuff,sizeof(szRxBuff));
// 	
// 	int nRxSize = 0;
// 	//int nTxSize = nSendCnt;
// 	
// 	TRACE("Send Data : %s\n",cSendData);
// 	//m_nRxBuffCnt = 0;
// 	if(m_pSerial->WriteToPort(cSendData, nSendCnt, szRxBuff, nRxSize))
// 	{
// 		//응답은 OnCommunicaiton에서 처리 
// 		//SetLineState(nDevice, TRUE);
// 		
// 		//RxData가 정상이면 
// 		for(int i=0; i<nRxSize; i++)
// 		{
// 			TRACE("%c,",szRxBuff[i]);
// 			if (szRxBuff[i] == 0x0a) return 0.0f; 
// 			//InsertRxChar(szRxBuff[i], nDevice);
// 		}
// 		
// 		//return TRUE;
// 	}
	
	
	//if (m_strRecv == "Time Out") return 0.0f;
	double dTemp; dTemp = atof(m_strRecv);	
	m_fCurCurr = (float)dTemp;
	return m_fCurCurr;
}
float CDCLoad::ReqMeasureVoltage()
{
	if (m_bPortOpen != TRUE) return 0.0f;
	m_strRecv = m_pSerial->ReadString2(_T("01MEAS:VOLT?"));
	double dTemp; dTemp = atof(m_strRecv);	
	m_fCurVolt = (float)dTemp;
	return m_fCurVolt;
}

float CDCLoad::ReqMeasureResistance()
{
	if (m_bPortOpen != TRUE) return 0.0f;
	m_strRecv = m_pSerial->ReadString2(_T("01MEAS:RES?"));
	double dTemp; dTemp = atof(m_strRecv);	
	m_fCurResitance = (float)dTemp;
	return m_fCurResitance;
}

float CDCLoad::ReqMeasurePower()
{
	if (m_bPortOpen != TRUE) return 0.0f;
	m_strRecv = m_pSerial->ReadString2(_T("01MEAS:POW?"));
	double dTemp; dTemp = atof(m_strRecv);	
	m_fCurPower = (float)dTemp;
	return m_fCurPower;
}

void CDCLoad::onlogsave(CString strtype,CString strlog,int iCHNO)
{
	CString stype;
	stype.Format(_T("dcload_%d"),0);
	if(iCHNO>0)
	{
		stype.Format(_T("dcload_%d_%d"),0,0);
	}	
	log_All(stype,strtype,strlog);
}
