#if !defined(AFX_USERCONFIGDLG_H__6C744267_E347_443F_9E3D_88C9484C616C__INCLUDED_)
#define AFX_USERCONFIGDLG_H__6C744267_E347_443F_9E3D_88C9484C616C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserConfigDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserConfigDlg dialog


class CUserConfigDlg : public CDialog
{
// Construction
public:
	void UpdateColumnSetting();
	void InitColimnListCtrl();
	CUserConfigDlg(CWnd* pParent = NULL);   // standard constructor
	LOGFONT	m_afont;

// Dialog Data
	//{{AFX_DATA(CUserConfigDlg)
	enum { IDD = IDD_USER_CONFIG_DLG , IDD2 = IDD_USER_CONFIG_DLG_ENG , IDD3 = IDD_USER_CONFIG_DLG_PL };
	CButton	m_ChkMonitoring;
	CListBox	m_SaveItemList;
	CComboBox	m_ctrlWhDecimal;
	CComboBox	m_ctrlWDecimal;
	CComboBox	m_ctrlWhUnit;
	CComboBox	m_ctrlWUnit;
	CLabel		m_ctrFontResult;
	CListCtrl	m_wndColumnList;
	CComboBox	m_ctrlTimeUnit;
	CComboBox	m_ctrlCDecimal;
	CComboBox	m_ctrlIDecimal;
	CComboBox	m_ctrlVDecimal;
	CComboBox	m_ctrlCUnit;
	CComboBox	m_ctrlIUnit;
	CComboBox	m_ctrlVUnit;
	UINT	m_nGirdColSize;
	UINT	m_nChTimer;
	UINT	m_nMdTimer;
	BOOL	m_bCheckPrevData;
	BOOL	m_bDBUpdate;
	BOOL	m_bUseCan;
	BOOL	m_bAutoStopChamber;
	BOOL	m_bUseAux;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void LoadSaveItemList();
	void UpdateColumnListCount();

	// Generated message map functions
	//{{AFX_MSG(CUserConfigDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnAddItemButton();
	afx_msg void OnDeleteItemButton();
	afx_msg void OnDblclkTopColumnList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridFontBtn();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnItemSetButton();
	afx_msg void OnButDb();
	afx_msg void OnChkMonitoring();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	int m_nMaxAuxFloatingPoint;
	int m_nMaxCanFloatingPoint;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERCONFIGDLG_H__6C744267_E347_443F_9E3D_88C9484C616C__INCLUDED_)
