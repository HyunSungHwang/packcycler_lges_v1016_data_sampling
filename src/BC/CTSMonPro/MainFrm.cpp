// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CTSMonProDoc.h"
#include "MainFrm.h"
#include "CTSMonProView.h"
#include "LoginManagerDlg.h"

#include "PrntScreen.h"
#include "JigIDRegDlg.h"

#include "SelUnitDlg.h"
#include "UnitComLogDlg.h"

#include "ReserveDlg.h"

#include "../CANConv/modbus.h" //yulee 20190609

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_COMMAND(ID_EXIT, OnExit)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)	
	ON_WM_TIMER()
	ON_COMMAND(ID_UNIT_LOG, OnUnitLog)
	ON_COMMAND(ID_SERIAL_SET, OnSerialSet)
	//}}AFX_MSG_MAP
	ON_MESSAGE(SFTWM_WRITE_LOG, OnWriteLogMsg)
	ON_MESSAGE(WM_COMM_RXCHAR, OnCommunication)
	ON_MESSAGE(SFTWM_TAGID_READED, OnTagIdScaned)
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_MY_MESSAGE_CTS_STOP, OnWorkStop)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_COM_PORT_STATE,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

//ksj 20200203 : CTSEditorPro 사용자 계정 관련 함수 옮겨오면서 필요한 전역 함수 추가.
extern CString g_strDataBaseName;
CString GetDataBaseName()
{
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";
	if(strTemp.IsEmpty())
	{
		return strTemp;
	}
	
	strTemp = strTemp+ "\\" + PS_SCHEDULE_DATABASE_NAME;
	return strTemp;

	return g_strDataBaseName;
}
//ksj end

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
	: m_strVersion(_T(""))
{
	m_bAutoExit = false;	
 	//m_bSerialConnected = FALSE;
	m_pTrayInputDlg = NULL;
	m_nRxBuffCnt = 0;
	m_bBlockFlag = FALSE;
	

	//ksj 20201023 : 종료 진행 상황 창 생성
	m_pThreadExitProgress = new CThreadExitProgress();
	m_pThreadExitProgress->CreateThread();
	//ksj end
}

CMainFrame::~CMainFrame()
{
	if(m_pTrayInputDlg)
	{
		m_pTrayInputDlg->DestroyWindow();
		delete m_pTrayInputDlg;
	}

	//ksj 20201013 : 종료 상황 안내창 삭제
	if(m_pThreadExitProgress)
	{
		m_pThreadExitProgress->PostThreadMessage(WM_QUIT,0,0);
		m_pThreadExitProgress = NULL;
	}
	//ksj end
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
	//	| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
	//	!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	//{
	//	TRACE0("Failed to create toolbar\n");
	//	return -1;      // fail to create
	//}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
			  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
    //m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	//DockControlBar(&m_wndToolBar);

	//Check Bar Code Reader
	//if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Bar Code Reader", FALSE))
	//{
	//	CSerialConfigDlg dlg;
	//	dlg.LoadConfig();
	//	m_SerialConfig = dlg.m_SerialConfig[0];
	//	InitSerialPort(m_SerialConfig);
	//}	

	// CG: The following line was added by the Splash Screen component.
	//CSplashWnd::ShowSplashScreen(this);
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.lpszName = PROGRAM_VER;
	//cs.style &= ~WS_THICKFRAME; 
	cs.style &= ~FWS_ADDTOTITLE;

	//ksj 20160613 작업 예약 기능 레지스트리 옵션 처리.
	if(!AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE, RESERVATION_REGISTRY_KEY_CONFIG_ENABLE_VAL))
	{
		if(cs.hMenu != NULL)
		{
			RemoveMenu(cs.hMenu,ID_RUN_QUEUE_VIEW,MF_BYCOMMAND);
			RemoveMenu(cs.hMenu,ID_CMD_RUN_QUEUE,MF_BYCOMMAND);
		}		
		
	}	

	//ksj 20201116 : 외부장치 테스트 메뉴 제거
	int nUseExtCommTest = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use ExtCommTest", TRUE);
	if(!nUseExtCommTest)
	{
		if(cs.hMenu != NULL)
		{
			RemoveMenu(cs.hMenu,6,MF_BYPOSITION); //6번째 항목이 외부장비 테스트임. 순서 바뀌면 안됨.		
		}		

	}	

	return CFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	mainFrm=this;

	return CFrameWnd::OnCreateClient(lpcs, pContext);
}

void CMainFrame::OnClose() 
{
	if( m_bAutoExit == false )
	{//$1013
		//if( MessageBox("CTSMonPro 프로그램을 종료 하시겠습니까?", 	"프로그램 종료", MB_ICONQUESTION | MB_YESNO ) == IDNO){
		if( MessageBox(Fun_FindMsg("OnCreateClient_msg1","IDR_MAINFRAME"),Fun_FindMsg("OnCreateClient_msg2","IDR_MAINFRAME"), MB_ICONQUESTION | MB_YESNO ) == IDNO){ //&&
			//::SFTCloseFormServer();
			return;			
		}	
	}
	g_AppInfo.iEnd=1;
	
	//ksj 20201023 : 종료 상황 안내창
	ShowExitProgress();
	SetExitProgress(10);
	//ksj end
	
	CFrameWnd::OnClose();
}

void CMainFrame::OnExit() 
{
	OnClose();
}


void CMainFrame::OnFilePrint() 
{
	// TODO: Add your command handler code here
	 CPrntScreen * ScrCap;
     ScrCap = new CPrntScreen("Impossible to print!","Error!");
     ScrCap->DoPrntScreen(1,2,TRUE);
     delete ScrCap;
     ScrCap = NULL;		
}

LRESULT CMainFrame::OnWriteLogMsg(WPARAM wParam, LPARAM lParam) 
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	if(pDoc)
	{
		CString strMsg;
		strMsg.Format("%s CH %d : %s", pDoc->GetModuleName(HIWORD(wParam)), LOWORD(wParam)+1, (LPCTSTR)lParam);
		pDoc->WriteSysLog(strMsg);
	}
	return 1;
}




BOOL CMainFrame::onOpenPort_Bcr1(int iType)
{
	if(iType==0)
	{
		m_SerialPort1.DisConnect();
		return FALSE;
	}
	//--------------------------------------
	CString strTemp;
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();

	//Check BarCode Reader 1
	if(m_SerialPort1.IsPortOpen() == TRUE)
	{
		m_SerialPort1.DisConnect();
	}	
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use BarCode Reader 1", FALSE))
	{
		CSerialConfigDlg dlg;
		dlg.onLoadConfig();

		SERIAL_CONFIG *config=&dlg.m_SerialConfig[0];
		
		if(m_SerialPort1.IsPortOpen())
		{//$1013
			//strTemp.Format("COM %d 포트는 이미 연결 되어 있습니다.", config->nPortNum);
			strTemp.Format(Fun_FindMsg("onOpenPort_Bcr1_msg1","IDR_MAINFRAME"), config->nPortNum); //&&
			AfxMessageBox(strTemp);
			return FALSE;
		}
			
		BOOL flag = m_SerialPort1.InitPort(this->m_hWnd,
											config->nPortNum, 
											config->nPortBaudRate,
											config->portParity,
											config->nPortDataBits,
											config->nPortStopBits,
											config->nPortEvents,
											config->nPortBuffer);			
		if( flag == FALSE )
		{
			//strTemp.Format("COM %d 포트 초기화를 실패하였습니다!", config->nPortNum);
			strTemp.Format(Fun_FindMsg("onOpenPort_Bcr1_msg2","IDR_MAINFRAME"), config->nPortNum); //&&
			//AfxMessageBox(strTemp);
			pDoc->WriteSysLog(strTemp); //lyj 20210809
			return FALSE;
		}			
		m_SerialPort1.StartMonitoring();
			
		int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE);
		if(index >=0 )
		{
			//m_wndStatusBar.SetPaneText(index, "BCR1 통신연결");//$1013
			m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Bcr1_msg3","IDR_MAINFRAME"));//&&
		}
		pDoc = (CCTSMonProDoc *)GetActiveDocument();
			
		//pDoc->WriteSysLog("BCR1 통신 연결됨");		
		pDoc->WriteSysLog(Fun_FindMsg("onOpenPort_Bcr1_msg4","IDR_MAINFRAME")); //&&
		return TRUE;
	}
	return FALSE;
}

BOOL CMainFrame::onOpenPort_Bcr2(int iType)
{
	if(iType==0)
	{
		m_SerialPort2.DisConnect();
		return FALSE;
	}
	//--------------------------------------
	CString strTemp;
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	
	//Check BarCode Reader 2
	if(m_SerialPort2.IsPortOpen() == TRUE)
	{
		m_SerialPort2.DisConnect();
	}	
	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use BarCode Reader 2", FALSE))
	{
		CSerialConfigDlg dlg;
		dlg.onLoadConfig();
		
		SERIAL_CONFIG *config=&dlg.m_SerialConfig[1];
		
		if(m_SerialPort2.IsPortOpen())
		{
			//strTemp.Format("COM %d 포트는 이미 연결 되어 있습니다.", config->nPortNum);//$1013
			strTemp.Format(Fun_FindMsg("onOpenPort_Bcr2_msg1","IDR_MAINFRAME"), config->nPortNum);//&&
			AfxMessageBox(strTemp);
			return FALSE;
		}
		
		BOOL flag = m_SerialPort2.InitPort(this->m_hWnd,
											config->nPortNum, 
											config->nPortBaudRate,
											config->portParity,
											config->nPortDataBits,
											config->nPortStopBits,
											config->nPortEvents,
											config->nPortBuffer);			
		if( flag == FALSE )
		{
			//strTemp.Format("COM %d 포트 초기화를 실패하였습니다!", config->nPortNum);
			strTemp.Format(Fun_FindMsg("onOpenPort_Bcr2_msg2","IDR_MAINFRAME"), config->nPortNum); //&&
			//AfxMessageBox(strTemp);
			pDoc->WriteSysLog(strTemp); //lyj 20210809
			return FALSE;
		}			
		m_SerialPort2.StartMonitoring();
		
		int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE);
		if(index >=0 )
		{
			//m_wndStatusBar.SetPaneText(index, "BCR2 통신연결");//$1013
			m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Bcr2_msg3","IDR_MAINFRAME")); //&&
		}
		pDoc = (CCTSMonProDoc *)GetActiveDocument();
		
		//pDoc->WriteSysLog("BCR2 통신 연결됨");		//$1013
		pDoc->WriteSysLog(Fun_FindMsg("onOpenPort_Bcr2_msg4","IDR_MAINFRAME"));	//&&
		return TRUE;
	}
	return FALSE;
}


BOOL CMainFrame::onOpenPort_Load1(int iType)
{	
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();

	if(iType==0)
	{
		if(pDoc->m_ctrlLoader1.m_pSerial)
		{
			pDoc->m_ctrlLoader1.m_pSerial->DisConnect();
		}
		return FALSE;
	}
	//--------------------------------------
	CString strTemp;
	if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Load 1", FALSE) == TRUE )
	{
		if (pDoc->m_ctrlLoader1.InitSerialPortUseOven(1))
		{
			//AfxMessageBox("Loader 1 Port open ok");
			return TRUE;
		}
		else
		{
			strTemp.Format("Loader 1 RS232 OPEN fail");
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			//AfxMessageBox("Loader 1 Port open fail");
			return FALSE;
		}
	}
	return FALSE;
}

BOOL CMainFrame::onOpenPort_Load2(int iType)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	
	if(iType==0)
	{
		if(pDoc->m_ctrlLoader2.m_pSerial)
		{
			pDoc->m_ctrlLoader2.m_pSerial->DisConnect();
		}
		return FALSE;
	}
	//--------------------------------------
	CString strTemp;
	if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Load 2", FALSE) == TRUE)
	{
		if (pDoc->m_ctrlLoader2.InitSerialPortUseOven(2))
		{
			//AfxMessageBox("Loader 2 Port open ok");
			return TRUE;
		}
		else
		{
			strTemp.Format("Loader 2 RS232 OPEN fail");
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			//AfxMessageBox("Loader 2 Port open fail");
			return FALSE;
		}
	}
	return FALSE;
}

BOOL CMainFrame::onOpenPort_Chamber(int nItem,int iType)
{	
	//----------------------------------------------------
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();	
	if(nItem == 0)
	{
		//if(pOvenCtrl->GetOvenModelType())


		COvenCtrl *pOvenCtrl = &(pDoc->m_ChamberCtrl.m_ctrlOven1);
		//----------------------------------------------------
		int nModel = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3 , "ChamberModel"  , -1);

		if(iType==0)
		{
			if(nModel > CHAMBER_MODBUS_NEX_START)
			{ 
				modbus_t * ctx = pDoc->m_ChamberCtrl.m_ctrlOven1.m_ctx; 
				if(ctx != NULL)
				{
					pOvenCtrl->m_bPortOpen = FALSE;
					pOvenCtrl->SetLineState(0, FALSE);
					modbus_flush(ctx);
					SuspendThread(pDoc->m_ChamberCtrl.m_hThread1);
					modbus_close(ctx);
					modbus_free(ctx);
				}
				return FALSE;
			}
			else
			{
				if( pOvenCtrl->m_pSerial[0] &&
					pOvenCtrl->m_pSerial[0]->IsPortOpen())
				{
					pOvenCtrl->m_pSerial[0]->DisConnect();
				}
				return FALSE;
			}

		}
		//-------------------------------------------------
		if(!mainView) return FALSE;

		CString strTemp;
		pDoc->LoadChamberData(nItem+1);
		mainView->onOvenUse(nItem+1);

		pDoc->m_ChamberCtrl.onSetInit(nItem); //yulee 20190608

		if( pOvenCtrl->m_bUseOvenCtrl )
		{		
			if(nModel > CHAMBER_MODBUS_NEX_START)
			{
				if (pOvenCtrl->InitSerialPort_Modbus(nItem+1, nModel))
				{
					pOvenCtrl->ClearCurrentData();			//★★ ljb 2010513  ★★★★★★★★

					if(pDoc->m_ChamberCtrl.m_hThread1 != NULL)
					{
						//주석처리 by ksj 20201021 : 이것때문에 modbus에서는 쓰레드 중복 생성된다.
						/*CONTEXT* tmpContext = new CONTEXT;
						if(GetThreadContext(pDoc->m_ChamberCtrl.m_hThread1,tmpContext) == TRUE)
						{
							ResumeThread(pDoc->m_ChamberCtrl.m_hThread1);
						}
						else
						{
							pDoc->m_ChamberCtrl.m_hThread1 = NULL;
							pDoc->m_ChamberCtrl.onSetInit(0);
						}

						delete(tmpContext); */
					}
				}
			}
			else
			{
				if (pOvenCtrl->InitSerialPort(nItem+1))
				{
					pOvenCtrl->ClearCurrentData();			//★★ ljb 2010513  ★★★★★★★★					
				}
			}


			if((pOvenCtrl->m_pSerial[0]->IsPortOpen()) || (pOvenCtrl->m_bPortOpen_Modbus == TRUE))
			{
				pOvenCtrl->m_bPortOpen = TRUE;
				//SetTimer(222, CHAMBER_TIMER, NULL);		//주의 !!!! 1초 이하 설정시(모니터링은 반드시 1초) 바로 Stop됨  							
				int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
				if(index >=0  )
				{
					//m_wndStatusBar.SetPaneText(index, "챔버 통신연결");//$1013
					m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg1","IDR_MAINFRAME"));//&&
				}
				return TRUE;
			}
			else
			{
				pOvenCtrl->m_bPortOpen = FALSE;
				//KillTimer( 222 );
				int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
				if( index >= 0 )
				{
					//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");	//$1013
					m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg2","IDR_MAINFRAME"));	//&&
				}	
			}
		}
		else
		{
			pOvenCtrl->m_bPortOpen = FALSE;
			//KillTimer( 222 );
			int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
			if( index >= 0 )
			{
				//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		//$1013
				m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg3","IDR_MAINFRAME"));		//&&
			}	
		}
	}
	else if(nItem == 1)
	{
		COvenCtrl *pOvenCtrl = &(pDoc->m_ChamberCtrl.m_ctrlOven2);
		//----------------------------------------------------
		int nModel = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4 , "ChamberModel"  , -1);
		if(iType==0)
		{

			if(nModel > CHAMBER_MODBUS_NEX_START)
			{
				modbus_t * ctx = pDoc->m_ChamberCtrl.m_ctrlOven2.m_ctx; 
				if(ctx != NULL)
				{
					pOvenCtrl->m_bPortOpen = FALSE;
					pOvenCtrl->SetLineState(0, FALSE);
					modbus_flush(ctx);
					SuspendThread(pDoc->m_ChamberCtrl.m_hThread2);
					modbus_close(ctx);
					modbus_free(ctx);
				}
				return FALSE;
			}
			else
			{
				if( pOvenCtrl->m_pSerial[0] &&
					pOvenCtrl->m_pSerial[0]->IsPortOpen())
				{
					pOvenCtrl->m_pSerial[0]->DisConnect();
				}
				return FALSE;
			}
		}
		//-------------------------------------------------
		if(!mainView) return FALSE;

		CString strTemp;
		pDoc->LoadChamberData(nItem+1);
		mainView->onOvenUse(nItem+1);	
		pDoc->m_ChamberCtrl.onSetInit(nItem); //yulee 20190608

		if( pOvenCtrl->m_bUseOvenCtrl )
		{		
			if(nModel > CHAMBER_MODBUS_NEX_START)
			{
				if (pOvenCtrl->InitSerialPort_Modbus(nItem+1, nModel))
				{
					pOvenCtrl->ClearCurrentData();			//★★ ljb 2010513  ★★★★★★★★

					if(pDoc->m_ChamberCtrl.m_hThread2 != NULL)
					{
						//주석처리 by ksj 20201021 : 이것때문에 modbus에서는 쓰레드 중복 생성된다.
						/*CONTEXT* tmpContext = new CONTEXT;
						if(GetThreadContext(pDoc->m_ChamberCtrl.m_hThread2,tmpContext) == TRUE)
						{
							ResumeThread(pDoc->m_ChamberCtrl.m_hThread2);
						}
						else
						{
							pDoc->m_ChamberCtrl.m_hThread2 = NULL;
							pDoc->m_ChamberCtrl.onSetInit(1);
						}

						delete(tmpContext); */
					}
				}
			}
			else
			{
				if (pOvenCtrl->InitSerialPort(nItem+1))
				{
					pOvenCtrl->ClearCurrentData();			//★★ ljb 2010513  ★★★★★★★★
				}
			}


			if((pOvenCtrl->m_pSerial[0]->IsPortOpen()) || (pOvenCtrl->m_bPortOpen_Modbus == TRUE))
			{
				pOvenCtrl->m_bPortOpen = TRUE;
				//SetTimer(222, CHAMBER_TIMER, NULL);		//주의 !!!! 1초 이하 설정시(모니터링은 반드시 1초) 바로 Stop됨  							
				int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
				if(index >=0  )
				{
					//m_wndStatusBar.SetPaneText(index, "챔버 통신연결");//$1013
					m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg1","IDR_MAINFRAME"));//&&
				}
				return TRUE;
			}
			else
			{
				pOvenCtrl->m_bPortOpen = FALSE;
				//KillTimer( 222 );
				int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
				if( index >= 0 )
				{
					//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");	//$1013
					m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg2","IDR_MAINFRAME"));	//&&
				}	
			}
		}
		else
		{
			pOvenCtrl->m_bPortOpen = FALSE;
			//KillTimer( 222 );
			int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
			if( index >= 0 )
			{
				//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		//$1013
				m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg3","IDR_MAINFRAME"));		//&&
			}	
		}
	}
	return FALSE;
}

// BOOL CMainFrame::onOpenPort_Oven1(int iType)
// {
// 	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
// 	
// 	if(iType==0)
// 	{
// 		if( pDoc->m_ctrlOven1.m_pSerial[0] &&
// 			pDoc->m_ctrlOven1.m_pSerial[0]->IsPortOpen())
// 		{
// 			pDoc->m_ctrlOven1.m_pSerial[0]->DisConnect();
// 		}
// 		return FALSE;
// 	}
//     //----------------------------------------------	
// 	if(!mainView) return FALSE;
// 
// 	CString strTemp;
// 	pDoc->LoadOvenData(1);
// 	mainView->onOven1Use();	
// 	pDoc->m_ChamberCtrl[0].onSetInit(0);
// 
// 	if( pDoc->m_ctrlOven1.m_bUseOvenCtrl )
// 	{		
// 		if (pDoc->m_ctrlOven1.InitSerialPort(1))
// 		{
// 			pDoc->m_ctrlOven1.ClearCurrentData();			//★★ ljb 2010513  ★★★★★★★★
// 		}
// 		
// 		if( pDoc->m_ctrlOven1.m_pSerial[0]->IsPortOpen() )
// 		{
// 			pDoc->m_ctrlOven1.m_bPortOpen = TRUE;
// 			//SetTimer(222, CHAMBER_TIMER, NULL);		//주의 !!!! 1초 이하 설정시(모니터링은 반드시 1초) 바로 Stop됨  							
// 			int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 			if(index >=0  )
// 			{
// 				//m_wndStatusBar.SetPaneText(index, "챔버 통신연결");//$1013
// 				m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg1"));//&&
// 			}
// 			return TRUE;
// 		}
// 		else
// 		{
// 			pDoc->m_ctrlOven1.m_bPortOpen = FALSE;
// 			//KillTimer( 222 );
// 			int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 			if( index >= 0 )
// 			{
// 				//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");	//$1013
// 				m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg2"));	//&&
// 			}	
// 		}
// 	}
// 	else
// 	{
// 		pDoc->m_ctrlOven1.m_bPortOpen = FALSE;
// 		//KillTimer( 222 );
// 		int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 		if( index >= 0 )
// 		{
// 			//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		//$1013
// 			m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven1_msg3"));		//&&
// 		}	
// 	}
// 	return FALSE;
// }

// BOOL CMainFrame::onOpenPort_Oven2(int iType)
// {
// 	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
// 	
// 	if(iType==0)
// 	{
// 		if( pDoc->m_ctrlOven2.m_pSerial[0] &&
// 			pDoc->m_ctrlOven2.m_pSerial[0]->IsPortOpen())
// 		{			
// 			pDoc->m_ctrlOven2.m_pSerial[0]->DisConnect();
// 		}
// 		return FALSE;
// 	}
//     //----------------------------------------------------
// 	if(!mainView) return FALSE;
// 	CString strTemp;	
// 
// 	// 2번째 챔버
// 	pDoc->LoadOvenData(2);
// 	mainView->onOven2Use();	
// 	if( pDoc->m_ctrlOven2.m_bUseOvenCtrl )
// 	{		
// 		if (pDoc->m_ctrlOven2.InitSerialPort(2) == FALSE)
// 		{
// 			pDoc->m_ctrlOven2.ClearCurrentData();			//★★ ljb 2010513  ★★★★★★★★
// 		}
// 		
// 		if( pDoc->m_ctrlOven2.m_pSerial[0]->IsPortOpen() )
// 		{
// 			pDoc->m_ctrlOven2.m_bPortOpen = TRUE;
// 			//SetTimer(333, CHAMBER_TIMER, NULL);		//주의 !!!! 1초 이하 설정시(모니터링은 반드시 1초) 바로 Stop됨 int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 			int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 			if(index >=0  )
// 			{
// 				//m_wndStatusBar.SetPaneText(index, "챔버 통신연결");//$1013
// 				m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven2_msg1"));//&&
// 			}
// 			return TRUE;
// 		}
// 		else
// 		{
// 			pDoc->m_ctrlOven2.m_bPortOpen = FALSE;
// 			//KillTimer( 333 );
// 			int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 			if(index >=0  )
// 			{
// 				//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		
// 				m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven2_msg2"));	//&&
// 			}
// 		}
// 	}
// 	else
// 	{
// 		pDoc->m_ctrlOven2.m_bPortOpen = FALSE;
// 		//KillTimer( 333 );
// 		int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 		if( index >= 0 )
// 		{
// 			//m_wndStatusBar.SetPaneText(index, "챔버 통신두절");	//$1013	
// 			m_wndStatusBar.SetPaneText(index, Fun_FindMsg("onOpenPort_Oven2_msg3"));	//&&
// 		}	
// 	}
// 	return FALSE;
// }

BOOL CMainFrame::onOpenPort_Power(int nItem,int iType)
{	
	//----------------------------------------------------
	nItem=nItem-1;
	//----------------------------------------------------
	if(iType==0)
	{
		mainFrm->m_PwrSupplyCtrl[nItem].m_SerialPort.DisConnect();		
		return FALSE;
	}
	//----------------------------------------------------	
	mainFrm->m_PwrSupplyCtrl[nItem].onSetInit(nItem,this);
	mainFrm->m_PwrSupplyCtrl[nItem].onSetPortSerialPort();		
	return mainFrm->m_PwrSupplyCtrl[nItem].m_SerialPort.IsPortOpen();
}

BOOL CMainFrame::onOpenPort_Chiller(int nItem,int iType) //yulee 20190614_*
{	
	//----------\------------------------------------------
	nItem=nItem-1;
	//----------------------------------------------------
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();	

	// 	//----------------------------------------------------
	// 	nItem=nItem-1;
	// 	//----------------------------------------------------
	// 	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();	


	CString strKey=_T("");
	if(nItem==0)      strKey=REG_SERIAL_CHILLER1;
	else if(nItem==1) strKey=REG_SERIAL_CHILLER2;
	else if(nItem==2) strKey=REG_SERIAL_CHILLER3;
	else if(nItem==3) strKey=REG_SERIAL_CHILLER4;

	int nModel = AfxGetApp()->GetProfileInt(strKey , "ChillerModel"  , -1);


	// 	CString RegAddrTlt;
	// 	RegAddrTlt.Format("SerialChiller%d", )
	// 	int nCtrlModel = AfxGetApp->GetProfileInt()
	//	int nModel = pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetOvenModelType();

	if(nModel > CHAMBER_MODBUS_NEX_START) //yulee 20190614_*
	{
		if(iType==0)
		{
			modbus_t * ctx = pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_ctx; 
			if(ctx != NULL)
			{
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = FALSE;
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.SetLineState(0, FALSE);
				modbus_flush(ctx);
				SuspendThread(pDoc->m_chillerCtrl[nItem].m_hThread);
				modbus_close(ctx);
				modbus_free(ctx);
			}
		}
		else if(iType == 1)
		{
			CString strTemp;
			int nType=nItem+7;

			pDoc->LoadChillerData(nItem+1);
			mainView->onChillerUse(nItem+1);	
			pDoc->m_chillerCtrl[nItem].onSetInit(nItem);

			if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bUseOvenCtrl)
			{
				if (pDoc->m_chillerCtrl[nItem].m_ctrlOven.InitSerialPort(nType))
				{
					//★★ ljb 2010513  ★★★★★★★★
					pDoc->m_chillerCtrl[nItem].m_ctrlOven.ClearCurrentData();
					//if(pDoc->m_chillerCtrl[nItem].m_hThread != NULL)
					//	ResumeThread(pDoc->m_chillerCtrl[nItem].m_hThread);

					/* //주석처리 by ksj 20201019 : 하기 코드 논리 오류 때문에 칠러 쓰레드 중복 생성된다.
					   //쓰레드 중복 생성으로 인해 과도하게 많은 칠러 상태 값 요청 발생. 통신 오류
					if(pDoc->m_chillerCtrl[nItem].m_hThread != NULL)
					{
						CONTEXT* tmpContext = new CONTEXT;
						if(GetThreadContext(pDoc->m_chillerCtrl[nItem].m_hThread,tmpContext) == TRUE)
						{
							ResumeThread(pDoc->m_chillerCtrl[nItem].m_hThread);
						}
						else
						{
							pDoc->m_chillerCtrl[nItem].m_hThread = NULL;
							pDoc->m_chillerCtrl[nItem].onSetInit(0);
						}

						delete(tmpContext); 
					}
					*/  //by ksj 20201018: 주석처리 끝.

					/*					pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPumpPortOpen = TRUE; //yulee 20190613*/
					pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpInitSerialPort(nType, CHAMBER_MODBUS_NEX_START);
				}
			}
			else
			{
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = FALSE;
				/*				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPumpPortOpen = FALSE; //yulee 20190613*/
			}
			return FALSE;
		}
	}
	else
	{
		// 	//----------------------------------------------------
		// 	nItem=nItem-1;
		// 	//----------------------------------------------------
		//CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();	
		if(iType==0)
		{
			//--------------------------------------------------------
			if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0] &&
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0]->IsPortOpen()==TRUE)
			{
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0]->DisConnect();
			}
			//--------------------------------------------------------
			if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pPumpSerial.IsPortOpen()==TRUE)
			{
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pPumpSerial.DisConnect();
			}
			return FALSE;
		}
		//-------------------------------------------------
		if(!mainView) return FALSE;

		CString strTemp;
		//cny 201809
		int nType=11+nItem;

		pDoc->LoadChillerData(nItem+1);
		mainView->onChillerUse(nItem+1);	
		pDoc->m_chillerCtrl[nItem].onSetInit(nItem);

		if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bUseOvenCtrl )
		{
			if (pDoc->m_chillerCtrl[nItem].m_ctrlOven.InitSerialPort(nType))
			{
				//★★ ljb 2010513  ★★★★★★★★
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.ClearCurrentData();
			}
			if(nModel != CHILLER_TEMP2520)
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpInitSerialPort(nType);

			if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0]->IsPortOpen() )
			{
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = TRUE;
				//SetTimer(222, CHAMBER_TIMER, NULL);
				//주의 !!!! 1초 이하 설정시(모니터링은 반드시 1초) 바로 Stop됨  

				//int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
				//if(index >=0  )
				//{
				//	m_wndStatusBar.SetPaneText(index, "챔버 통신연결");
				//}
				return TRUE;
			}
			else
			{
				pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = FALSE;

				//KillTimer( 222 );
				//int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
				//if(index >=0  )
				//{
				//	m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		
				//}			
			}
		}
		else
		{
			pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = FALSE;

			//KillTimer( 222 );
			//int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
			//if( index >= 0 )
			//{
			//	m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		
			//}		
		}
		return FALSE;	
	}
	return FALSE;
}

// BOOL CMainFrame::onOpenPort_Chiller(int nItem,int iType)
// {	
// 	//----------------------------------------------------
// 	nItem=nItem-1;
// 	//----------------------------------------------------
// 	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();	
// 	if(iType==0)
// 	{ 
// 		//--------------------------------------------------------
// 		if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0] &&
// 			pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0]->IsPortOpen()==TRUE)
// 		{
// 			pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0]->DisConnect();
// 		}
// 		//--------------------------------------------------------
// 		if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pPumpSerial.IsPortOpen()==TRUE)
// 		{
// 			pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pPumpSerial.DisConnect();
// 		}
// 		return FALSE;
// 	}
//     //-------------------------------------------------
// 	if(!mainView) return FALSE;
// 	
// 	CString strTemp;
// 	//cny 201809
// 	int nType=11+nItem;
// 
// 	pDoc->LoadChillerData(nItem+1);
// 	mainView->onChillerUse(nItem+1);	
// 	pDoc->m_chillerCtrl[nItem].onSetInit(nItem);
// 	
// 	if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bUseOvenCtrl )
// 	{
// 		if (pDoc->m_chillerCtrl[nItem].m_ctrlOven.InitSerialPort(nType))
// 		{
// 			//★★ ljb 2010513  ★★★★★★★★
// 			pDoc->m_chillerCtrl[nItem].m_ctrlOven.ClearCurrentData();
// 		}
// 		pDoc->m_chillerCtrl[nItem].m_ctrlOven.onPumpInitSerialPort(nType);
// 		
// 		if( pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_pSerial[0]->IsPortOpen() )
// 		{
// 			pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = TRUE;
// 			//SetTimer(222, CHAMBER_TIMER, NULL);
// 			//주의 !!!! 1초 이하 설정시(모니터링은 반드시 1초) 바로 Stop됨  
// 				
// 			//int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 			//if(index >=0  )
// 			//{
// 			//	m_wndStatusBar.SetPaneText(index, "챔버 통신연결");
// 			//}
// 			return TRUE;
// 		}
// 		else
// 		{
// 			pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = FALSE;
// 			
// 			//KillTimer( 222 );
// 			//int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 			//if(index >=0  )
// 			//{
// 			//	m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		
// 			//}			
// 		}
// 	}
// 	else
// 	{
// 		pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen = FALSE;
// 			
// 		//KillTimer( 222 );
// 		//int index = m_wndStatusBar.CommandToIndex(ID_COM_PORT_STATE1);
// 		//if( index >= 0 )
// 		//{
// 		//	m_wndStatusBar.SetPaneText(index, "챔버 통신두절");		
// 		//}		
// 	}
// 	return FALSE;
// }

int CMainFrame::onGetSerialPowerNo(int nCh,int &nOutp)
{//cny 201809
	CString strKey;
	nOutp=0;
	
	for(int i=0;i<MAX_POWER_COUNT;i++)
	{				
		strKey.Format(_T("SerialPower%d"),i+1);
		
		int nUse   = AfxGetApp()->GetProfileInt(strKey, "Use", 0);
		int nChNoA = AfxGetApp()->GetProfileInt(strKey, "ChNoA", 0);
		int nChNoB = AfxGetApp()->GetProfileInt(strKey, "ChNoB", 0);
		
		if( nUse  == 1)
		{
			nOutp=0;
			if( nChNoA == nCh ) nOutp=1; 
			if( nChNoB == nCh ) nOutp=2;
			
			if( nOutp > 0)
			{
				if(m_PwrSupplyCtrl[i].m_SerialPort.IsPortOpen() == TRUE)
				{
					return i;
				}
#ifdef _DEBUG //ksj 20200901 : 디버그 모드에서는 접속 안되어있어도 리턴
				return i;
#endif
			}
		}
	}
	return -1;
}

BOOL CMainFrame::onGetSerialPowerGetSbcCh(int nItem,int &nCHA,int &nCHB)
{//cny 201809
	CString strKey;

	nCHA=0;
	nCHB=0;

	for(int i=0;i<MAX_POWER_COUNT;i++)
	{				
		if(nItem!=i) continue;

		strKey.Format(_T("SerialPower%d"),i+1);
		
		int nUse   = AfxGetApp()->GetProfileInt(strKey, "Use", 0);
		int nChNoA = AfxGetApp()->GetProfileInt(strKey, "ChNoA", 0);
		int nChNoB = AfxGetApp()->GetProfileInt(strKey, "ChNoB", 0);
		
		if( nUse  == 1)
		{
			nCHA=nChNoA;
			nCHB=nChNoB;
			return TRUE;
		}
	}
	return FALSE;
}

int CMainFrame::onGetSerialChillerNo(int nCh)
{//cny 201809
	if(!mainView) return -1;
	CCTSMonProDoc *pDoc=mainView->GetDocument();
	if(!pDoc) return -1;
	
	for(int i=0;i<MAX_CHILLER_COUNT;i++)
	{
		if(pDoc->m_chillerCtrl[i].m_nCHNO==nCh)
		{
			if(pDoc->m_chillerCtrl[i].m_ctrlOven.m_bPortOpen==FALSE)
			{
#ifdef _DEBUG //ksj 20200901 : 디버그 모드에서는 접속 안되어있어도 리턴
				return i;
#else
				return -2;
#endif
			}
			return i;
		}
	}
	return -1;
}

int CMainFrame::onGetSerialChamberNo(int nCh)
{//yulee 20190608
	if(!mainView) return -1;
	CCTSMonProDoc *pDoc=mainView->GetDocument();
	if(!pDoc) return -1;

	for(int i=0;i<MAX_CHAMBER_COUNT;i++)
	{
		if(i == 0)
		{
			if(pDoc->m_ChamberCtrl.mST_OvenContl[i].m_nCHNO==nCh)
			{
				if(pDoc->m_ChamberCtrl.m_ctrlOven1.m_bPortOpen==FALSE)
				{
					return -2;
				}
				return i;
			}
		}
		else if(i == 1)
		{
			if(pDoc->m_ChamberCtrl.mST_OvenContl[i].m_nCHNO==nCh)
			{
				if(pDoc->m_ChamberCtrl.m_ctrlOven2.m_bPortOpen==FALSE)
				{
					return -2;
				}
				return i;
			}	
		}
	}
	return -1;
}

LRESULT CMainFrame::OnCommunication(WPARAM wParam, LPARAM lParam)
{
	//CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();

	char data = (char)wParam;
	TRACE("%c", (char)data);
	
	BOOL bUseBcr1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use BarCode Reader 1", FALSE); //ksj 20200207 : 조건 추가. BCR 활성화 여부에 관계 없이 다른 장비랑 포트 설정이 같으면 값을 수신해버림.

	//BarCode data

	//if(m_SerialPort1.GetPortNo() == UINT(lParam))
	if(m_SerialPort1.GetPortNo() == UINT(lParam) && bUseBcr1) //ksj 20200207
	{
		if(data != 0x0d)
		{
			if(data >= 0x20) m_strReceiveBuff1 += data;
		}
		else
		{
			BCRScaned(m_strReceiveBuff1);
			m_strReceiveBuff1.Empty();
		}
	}
	else if( m_PwrSupplyCtrl[0].m_SerialPort.IsPortOpen() &&
		     m_PwrSupplyCtrl[0].m_SerialPort.GetPortNo() == UINT(lParam))
	{
		if(data == 0x0A)
		{
			m_PwrSupplyCtrl[0].m_SerialPort.m_bRecv=TRUE;
			
			if(m_PwrSupplyCtrl[0].m_SerialPort.m_bMessage==TRUE)
			{
				m_PwrSupplyCtrl[0].m_SerialPort.m_bMessage=FALSE;

				CString strValue;
				BOOL bRet=m_PwrSupplyCtrl[0].m_SerialPort.GetRxStr(strValue,0x0A);
				if(bRet==TRUE)
				{
					AfxMessageBox(strValue);
				}
			}
		}
	}
	else if( m_PwrSupplyCtrl[1].m_SerialPort.IsPortOpen() &&
	     	 m_PwrSupplyCtrl[1].m_SerialPort.GetPortNo() == UINT(lParam))
	{
		if(data == 0x0A)
		{
			m_PwrSupplyCtrl[1].m_SerialPort.m_bRecv=TRUE;
			
			if(m_PwrSupplyCtrl[1].m_SerialPort.m_bMessage==TRUE)
			{
				m_PwrSupplyCtrl[1].m_SerialPort.m_bMessage=FALSE;
				
				CString strValue;
				BOOL bRet=m_PwrSupplyCtrl[1].m_SerialPort.GetRxStr(strValue,0x0A);
				if(bRet==TRUE)
				{
					AfxMessageBox(strValue);
				}
			}
		}
	}
	else
	{
		
	}

	/*//Oven data
	if(m_SerialPort2.GetPortNo() == UINT(lParam))
	{
		pDoc->m_ctrlOven.InsertRxChar(data);
	}
	*/
	return 0;

}
BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	//CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
	//CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
 	//HANDLE	handle = AfxGetInstanceHandle();		//자신의 핸들 얻기
 	//HWND hWnd = ::FindWindow(NULL, "BMA INSPECT");
	// TODO: Add your message handler code here and/or call default
	ASSERT(pCopyDataStruct);
	
	//BMA 에서 전송되어 온 Message (Bar Code)
	switch (pCopyDataStruct->dwData)
	{
		case SFTWM_SAFETY_INFO_DATA:
		{
			AfxMessageBox("recv safety info data");
			break;
		}		
		case WM_MY_MESSAGE_CTS_START:	//ljb 작업 경로명과 바코드가 인자로 옴
		{
			//if(pCopyDataStruct->cbData > 0)
			//{
			//	m_pCtsValue = (CTSWORKVALUE*)pCopyDataStruct->lpData;
			//	
			//	CString	strPath,strBarCode;
			//	strPath.Format("%s",m_pCtsValue->strPath);
			//	strBarCode.Format("%s",m_pCtsValue->strBarCode);
			//	msg.cbData = 0;
			//	msg.lpData = NULL;
			//	if (strPath.IsEmpty() || strBarCode.IsEmpty())
			//	{
			//		msg.dwData = WM_MY_MESSAGE_CTS_START_FAIL;
			//		::SendMessage(hWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&msg);
			//	}
			//	if (hWnd)
			//	{
			//		if (pView->Fun_BMASimpleProcess(strPath,strBarCode))
			//		{
			//			//충.방전 시작 성공
			//			msg.dwData = WM_MY_MESSAGE_CTS_START_OK;
			//			::SendMessage(hWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&msg);
			//			//::PostMessageA(hWnd, WM_MY_MESSAGE_CTS_START, NULL, NULL);
			//		}
			//		else
			//		{
			//			//충.방전 시작 실패
			//			msg.dwData = WM_MY_MESSAGE_CTS_START_FAIL;
			//			::SendMessage(hWnd, WM_COPYDATA, (WPARAM)handle, (LPARAM)&msg);
			//		}
			//	}
			//}
			break;
		}
		case WM_MY_MESSAGE_CTS_ISOLATION:
		{
			if(pCopyDataStruct->cbData > 0)
			{
				m_pISOValue = (CTSISOVALUE*)pCopyDataStruct->lpData;
				
				ZeroMemory(&m_ISOValue,sizeof(CTSISOVALUE));				
			}
			break;
		}
		
	}
	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

void CMainFrame::BCRScaned(CString strTag)
{
	//Read한 Barcode가 Tray 번호인지 Jig 번호인지 구별한다.
	char szBuff[64];
	sprintf(szBuff, "%s", strTag);

	//CWnd *pWnd = GetLastActivePopup();
	HWND hTargetWnd = ::GetActiveWindow();
	if(hTargetWnd == NULL || hTargetWnd == m_hWnd)
	{
		if(m_pTrayInputDlg == NULL)
		{
			m_pTrayInputDlg = new CTrayInputDlg(this);
			m_pTrayInputDlg->m_pDoc = (CCTSMonProDoc *)GetActiveDocument();
			m_pTrayInputDlg->SetUnitInput(TRUE);
			m_pTrayInputDlg->Create(IDD_TRAYNO_DLG, this);
		}
		hTargetWnd = m_pTrayInputDlg->m_hWnd;
	}

	ULONG nTagType = FALSE;
	//1. Location ID로 사용중인지 검사 
	CJigIDRegDlg *pDlg = new CJigIDRegDlg;
	pDlg->m_pDoc = (CCTSMonProDoc *)GetActiveDocument();
	int nModule, nJig;
	if(pDlg->SearchLocation(strTag, nModule, nJig))
	{
		nTagType = TRUE;
	}
	delete pDlg;

	::SendMessage(hTargetWnd, SFTWM_BCR_SCANED, (WPARAM)nTagType, (LPARAM)szBuff);
	TRACE("BCR Scaned :: %s\n", strTag);
}

//DEL void CMainFrame::OnJigTag() 
//DEL {
//DEL 	// TODO: Add your command handler code here
//DEL 	CJigIDRegDlg *pDlg = new CJigIDRegDlg;
//DEL 	pDlg->m_pDoc = (CCTSMonProDoc *)GetActiveDocument();
//DEL 
//DEL 	pDlg->DoModal();
//DEL 
//DEL 	delete pDlg;
//DEL }

LRESULT CMainFrame::OnTagIdScaned(WPARAM wParam, LPARAM lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	if(str.IsEmpty())	return 0;

	int nModuleID = HIWORD(wParam);
	int nChNo = LOWORD(wParam);
	if(nChNo < 1)	nChNo = 1;

	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	CCyclerChannel *pChannel = pDoc->GetChannelInfo(nModuleID, nChNo-1);
	if(pChannel)
	{
		pChannel->SetTestSerial(str);
	}
	return 1;
}

/*
BOOL CMainFrame::SendOvenRunCmd(int nIndex)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	if(pDoc->m_ctrlOven.m_pSerial[nIndex]->IsPortOpen())
	{
		char szTxBuff[32], szRxBuff[1024];
		ZeroMemory(szTxBuff, sizeof(szTxBuff));

		int nRxSize = 0;
		int nTxSize = ((CCTSMonProDoc *)GetActiveDocument())->m_ctrlOven.GetReqRunCmd(nIndex, szTxBuff, nRxSize);
		

		m_nRxBuffCnt = 0;
		pDoc->WriteSysLog(szTxBuff, CT_LOG_LEVEL_DEBUG);

		//20081103
		//현재 운영 중인 Step 번호를 Reset 한다.
		//////////////////////////////////////////////////////////////////////////
		((CCTSMonProDoc *)GetActiveDocument())->m_ctrlOven.SetRunStepNo(nIndex, 0);
		
		if(pDoc->m_ctrlOven.m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//응답은 OnCommunicaiton에서 처리 
			((CCTSMonProDoc *)GetActiveDocument())->m_ctrlOven.SetLineState(nIndex, TRUE);

			return TRUE;
		}
	}
	return FALSE;
}
*/

void CMainFrame::OnTimer(UINT_PTR nIDEvent) // BW KIM 2014.02.13 챔버 관련
{
	CFrameWnd::OnTimer(nIDEvent);
}

/*
BOOL CMainFrame::RequestOvenCurrentValue(int nIndex)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();

	return pDoc->m_ctrlOven.RequestCurrentValue(nIndex);
}
*/
/*
BOOL CMainFrame::SendOvenStopCmd(int nIndex)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	if(pDoc->m_ctrlOven.m_pSerial[nIndex]->IsPortOpen())
	{
		char szTxBuff[32], szRxBuff[1024];

		int nRxSize = 0;
		int nTxSize = ((CCTSMonProDoc *)GetActiveDocument())->m_ctrlOven.GetReqStopCmd(nIndex, szTxBuff, nRxSize);
		

		m_nRxBuffCnt = 0;
		if(pDoc->m_ctrlOven.m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			//응답은 OnCommunicaiton에서 처리 
			((CCTSMonProDoc *)GetActiveDocument())->m_ctrlOven.SetLineState(nIndex, TRUE);

			return TRUE;
		}
		
	}
	return FALSE;
}
*/
/*
BOOL CMainFrame::SetOvenFixMode(int nIndex, BOOL bSet)
{
	if(m_SerialPort2.IsPortOpen())
	{
		char szTxBuff[32], szRxBuff[32];
		szTxBuff[0] = 0x02;
		szTxBuff[1] = '0';
		szTxBuff[2] = '0';
		szTxBuff[3] = '1'+nIndex;
		szTxBuff[4] = 'W';
		szTxBuff[5] = 'U';
		szTxBuff[6] = 'P';
		szTxBuff[7] = ',';
		szTxBuff[8] = '0';
		szTxBuff[9] = '0';
		szTxBuff[10] = '0';
		szTxBuff[11] = '1';
		szTxBuff[12] = ',';
		szTxBuff[13] = 0x00;
		if(bSet)
		{
			szTxBuff[14] = 0x01;
		}
		else
		{
			szTxBuff[14] = 0x00;
		}
		szTxBuff[15] = 0x03;
		szTxBuff[16] = 0x0d;
		szTxBuff[17] = 0x0a;

		int nTxSize = 18;
		int nRxSize = 18;
		CString strTemp;
		CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
		if(m_SerialPort2.WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("Oven %d 정치모드 운전 전환 성공", nIndex+1);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			return TRUE;
		}
		else
		{
			strTemp.Format("Oven %d 정치모드 운전 전환 실패", nIndex+1);
			pDoc->WriteSysLog(strTemp);
		}
	}
	return FALSE;
}
*/
/*
BOOL CMainFrame::SetOvenHumidity(int nIndex, float fDataH)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	if(pDoc->m_ctrlOven.m_pSerial[nIndex]->IsPortOpen())
	{
		char szTxBuff[32], szRxBuff[32];

		CString strTemp;
		int nRxSize = 0;

		int nTxSize = pDoc->m_ctrlOven.GetReqOvenHumidity(nIndex, fDataH, szTxBuff, nRxSize);

		TRACE("습도셋팅 : %s\n", szTxBuff);
		
		if(pDoc->m_ctrlOven.m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("챔버%d 습도 설정 %.2f%% 전송 성공", nIndex+1, fDataH);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			return TRUE;
		}
		else
		{
			strTemp.Format("챔버%d 습도 설정 %.2%% 전송 실패", nIndex+1, fDataH);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
		}
	}
	return FALSE;
}
*/
/*
BOOL CMainFrame::SetOvenTemperature(int nIndex, float fDataT)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	if(pDoc->m_ctrlOven.m_pSerial[nIndex]->IsPortOpen())
	{
		char szTxBuff[32], szRxBuff[32];

		CString strTemp;
		int nRxSize = 0;

		int nTxSize = pDoc->m_ctrlOven.GetReqOvenTemperatureCmd(nIndex, fDataT, szTxBuff, nRxSize);

		TRACE("온도셋팅 : %s\n", szTxBuff);
		
		if(pDoc->m_ctrlOven.m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("챔버%d 온도 설정 %.2f℃ 전송 성공", nIndex+1, fDataT);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			return TRUE;
		}
		else
		{
			strTemp.Format("챔버%d 온도 설정 %.2f℃ 전송 실패", nIndex+1, fDataT);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
		}
	}
	return FALSE;
}
*/
void CMainFrame::CheckUsePort()
{
	CString strTemp;
	
	onOpenPort_Bcr1();
	onOpenPort_Bcr2();
	
	onOpenPort_Load1();
	onOpenPort_Load2();
	
	for(int i0=0;i0<MAX_CHAMBER_COUNT;i0++)
	{
		onOpenPort_Chamber(i0);
	}	
	//onOpenPort_Oven1();
	//onOpenPort_Oven2();
	
	for(int iA=0;iA<MAX_POWER_COUNT;iA++)
	{
		onOpenPort_Power(iA+1);		
	}
	for(int iB=0;iB<MAX_CHILLER_COUNT;iB++)
	{
		onOpenPort_Chiller(iB+1);
	}	
}

/*
BOOL CMainFrame::SetOvenHSlop(int nIndex, float fDataH)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	if(pDoc->m_ctrlOven.m_pSerial[nIndex]->IsPortOpen())
	{
		char szTxBuff[32], szRxBuff[32];

		CString strTemp;
		int nRxSize = 0;

		int nTxSize = pDoc->m_ctrlOven.GetReqOvenSetHSlop(nIndex, fDataH, szTxBuff, nRxSize);

		TRACE("습도 Slop 셋팅 : %s\n", szTxBuff);
		
		if(pDoc->m_ctrlOven.m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("Oven %d 습도 Slop 설정 %.2f％/분 실행 성공", nIndex+1, fDataH);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			return TRUE;
		}
		else
		{
			strTemp.Format("Oven %d 온도 설정 %.2f℃ 실행 실패", nIndex+1, fDataT);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
		}
	}
	return FALSE;
}
*/
/*
BOOL CMainFrame::SetOvenTSlop(int nIndex, float fDataT)
{
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
	if(pDoc->m_ctrlOven.m_pSerial[nIndex]->IsPortOpen())
	{
		char szTxBuff[32], szRxBuff[32];

		CString strTemp;
		int nRxSize = 0;

		int nTxSize = pDoc->m_ctrlOven.GetReqOvenSetTSlop(nIndex, fDataT, szTxBuff, nRxSize);

		TRACE("온도 Slop 셋팅 : %s\n", szTxBuff);
		
		if(pDoc->m_ctrlOven.m_pSerial[nIndex]->WriteToPort(szTxBuff, nTxSize, szRxBuff, nRxSize))
		{
			strTemp.Format("Oven %d 온도 Slop 설정 %.2f℃/분 실행 성공", nIndex+1, fDataT);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_DETAIL);
			return TRUE;
		}
		else
		{
			strTemp.Format("Oven %d 온도 설정 %.2f℃ 실행 실패", nIndex+1, fDataT);
			pDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
		}
	}
	return FALSE;
}
*/

LRESULT CMainFrame::OnWorkStop(WPARAM wParam, LPARAM lParam)
{
	CMainFrame * pFrame = (CMainFrame*)AfxGetMainWnd();
	CCTSMonProView * pView = (CCTSMonProView*)pFrame->GetActiveView();
	pView->Fun_CmdStop();
	return 1;
}

void CMainFrame::OnUnitLog() 
{
	// TODO: Add your command handler code here
	int nModuleID;
	CCTSMonProDoc *pDoc = (CCTSMonProDoc *)GetActiveDocument();
//	CCyclerChannel *pChannel = pDoc->GetChannelInfo(nModuleID, nChNo-1);
	int nInstalledModuleNum = pDoc->GetInstallModuleCount();

	if (nInstalledModuleNum > 0) nModuleID = pDoc->GetModuleID(0);
	else
	{
		//AfxMessageBox("모듈 정보가 없습니다.");
		AfxMessageBox(Fun_FindMsg("OnUnitLog_msg1","IDR_MAINFRAME")); //&&
		return;
	}
	
	CSelUnitDlg *pDlg1;
	pDlg1 = new CSelUnitDlg();
// 	int nModuleID = EPGetModuleID(0);
// 	if(m_pTab->m_pTopView)
// 	{
// 		nModuleID = m_pTab->m_pTopView->GetCurModuleID();
// 	}
	pDlg1->SetModuleID(nModuleID);
	if(pDlg1->DoModal() != IDOK)	
	{
		delete pDlg1;
		pDlg1 = NULL;
		return;
	}
	nModuleID = pDlg1->GetModuleID();
	delete pDlg1;
	pDlg1 =  NULL;
	
// 	CCTSMonDoc *pDoc = (CCTSMonDoc *)GetActiveDocument();
// 	ASSERT(pDoc);	
	
	CString strIP = pDoc->GetModuleIPAddress(nModuleID);
	
	CUnitComLogDlg dlg(strIP, this);
	
	dlg.DoModal();	
}

void CMainFrame::OnSerialSet() 
{
	//임시삭제
	//CLoginManagerDlg LoginDlg;
	//if(LoginDlg.DoModal() != IDOK) return;
	
	CSerialConfigDlg dlg;
	//dlg.m_strTitle = "Serial 통신을 설정합니다.";
	dlg.m_strTitle = Fun_FindMsg("OnSerialSet_msg1","IDR_MAINFRAME"); //&&
	dlg.DoModal();
}

// void CMainFrame::ApplyMultiLang() //yulee 20190220
// {
// 	// 메뉴에 언어 적용
// 	HMENU hMenuOld = GetMenu()->GetSafeHmenu();
// 	CMenu cMenu;
// 	cMenu.LoadMenu(IDR_MAINFRAME);
// 	
// 	// Release 모드 시에는 테스트 메뉴항목 제거 
// #ifndef _DEBUG
// 	int count = cMenu.GetMenuItemCount();
// 	for (int i = 0; i < count; i++)
// 	{
// 		CString str;
// 		if (cMenu.GetMenuString(i, str, MF_BYPOSITION))
// 		{
// 			if (str == "MENU_TEST" || str == "--")
// 			{
// 				cMenu.DeleteMenu(i,  MF_BYPOSITION) ;
// 			}
// 		}
// 	}
// #endif
// 	
// 	//cMenu.DeleteMenu(1, MF_BYPOSITION); //ksj 20170413 : 편집 메뉴 제거
// 	
// 	HMENU hMenu = cMenu.GetSafeHmenu();
// 	g_multilang.TranslateMenuString(hMenu);
// 	SetMenu(&cMenu);
// 	cMenu.Detach();
// 	
// 	::DestroyMenu(hMenuOld);
// }

//ksj 20201023 : 프로그램 종료 진행률 표시
void CMainFrame::SetExitProgress(int nPos)
{	
	if(!m_pThreadExitProgress) return;

	m_pThreadExitProgress->SetProgress(nPos);
}

//ksj 20201023 : 프로그램 종료 진행 상황 표시
void CMainFrame::ShowExitProgress(void)
{
	if(!m_pThreadExitProgress) return;

	m_pThreadExitProgress->ShowProgressWnd(mainView);
}
