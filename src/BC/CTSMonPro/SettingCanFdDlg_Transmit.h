#if !defined(AFX_SettingCanFdDlg_TRANSMIT_H__DE829BB6_D401_4CB7_B0EC_2AD4305A98E3__INCLUDED_)
#define AFX_SettingCanFdDlg_TRANSMIT_H__DE829BB6_D401_4CB7_B0EC_2AD4305A98E3__INCLUDED_

#include "MyGridWnd.h"	// Added by ClassView
#include "CTSMonProDoc.h"

#include "TabCtrlEx.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SettingCanFdDlg_Transmit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSettingCanFdDlg_Transmit dialog

class CSettingCanFdDlg_Transmit : public CDialog
{
// Construction
public:

	CSettingCanFdDlg_Transmit(CCTSMonProDoc * pDoc, CWnd* pParent = NULL);   // standard constructor
	void ReDrawMasterGrid();
	void InitGridMaster();
	void InitLabel();
	void UpdateCommonData();
	void UpdateGridData();
	CString Fun_FindCanName(int nDivision); //yulee 20181220
	void EnableAllControl(BOOL IsEnable);
	void IsChangeFlag(BOOL IsFlag);
	BOOL LoadCANConfig(CString strLoadPath);
	BOOL SaveCANConfig(CString strSavePath);

public:
	void Fun_ChangeSizeControl(UINT uiMaster, UINT uiSlave);
	void Fun_ChangeViewControl(BOOL bFlag);
	void InitParamTab();
	void InitGridSlave();
	BOOL IsChange;
	int nSelCh;
	int nSelModule;
	int nLINtoCANSelect;
	CString m_strTitle;

// Dialog Data
	//{{AFX_DATA(CSettingCanFdDlg_Transmit)
	enum { IDD = IDD_CAN_FD_SETTING_TRANSMIT , IDD2 = IDD_CAN_FD_SETTING_TRANSMIT_ENG , IDD3 = IDD_CAN_FD_SETTING_TRANSMIT_PL };
	CComboBox	m_ctrlTXBMSCRCTypeSlave;
	CComboBox	m_ctrlTXBMSCRCType;
	CComboBox	m_ctrlBMSTXTerminalSlave;
	CComboBox	m_ctrlBMSTXTerminal;
	CComboBox	m_ctrlDataRateSlave;
	CComboBox	m_ctrlDataRate;
	CComboBox	m_ctrlBmsSJWSlave;
	CComboBox	m_ctrlBmsSJW;
	CString	m_strBmsSJW;
	CString	m_strBmsSJWSlave;
	CComboBox	m_ctrlBmsTypeSlave;
	CComboBox	m_ctrlBmsType;
	CString	m_strBmsType;
	CString	m_strBmsTypeSlave;
	/*CTabCtrl*/	CTabCtrlEx m_ctrlParamTab;
	CComboBox	m_ctrlBaudRateSlave;
	CLabel	m_ctrlLabelChMsg;
	CLabel	m_ctrlLabelChNo;
	CLabel	m_ctrlLabelMsg;
	CComboBox	m_ctrlBaudRate;
	BOOL	m_ckExtID;
	CString	m_strSelectCh;
	CString	m_strCanID;
	CString	m_strCanIDSlave;
	BOOL	m_ckExtIDSlave;
	int		m_nMasterIDType;
	CString m_strType;
	int		m_nProtocolVer;
	CString m_strDescript;
	float	m_edit_chargeOnVolt;
	UINT	m_edit_OnDelayTime;
	float	m_edit_KeyonVoltMax;
	float	m_edit_KeyonVoltMin;
	float	m_edit_BmsRestartTime;
	BOOL	m_ckCanFD;
	BOOL	m_ckCanFD_Slave;
	LONG	m_lMinCanTxTime; //ksj 20210305
	BOOL	m_bUseCanTxTimeLimit; //ksj 20210305
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingCanFdDlg_Transmit)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSettingCanFdDlg_Transmit)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBtnLoadCanSetup();
	afx_msg void OnBtnAddMaster();
	afx_msg void OnBtnDelMaster();
	afx_msg void OnBtnSaveCanSetup();
	afx_msg void OnBtnAddSlave();
	afx_msg void OnBtnDelSlave();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	virtual void OnCancel();
	afx_msg void OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButNotUse();
	afx_msg void OnCheckTxCanFd();
	afx_msg void OnCheckTxCanFd2();
	afx_msg void OnCheckExtMode();
	afx_msg void OnCheckExtModeSlave();
	afx_msg void OnSelchangeComboCanBaudrate();
	afx_msg void OnSelchangeComboCanBmsSjw();
	afx_msg void OnSelchangeComboCanBmsType();
	afx_msg void OnSelchangeComboTxCanDatarate();
	afx_msg void OnSelchangeComboTxCanTerminalM();
	afx_msg void OnSelchangeComboTxCanCrcM();
	afx_msg void OnSelchangeComboTxCanDatarate2();
	afx_msg void OnSelchangeComboCanBmsSjwSlave();
	afx_msg void OnSelchangeComboCanBmsTypeSlave();
	afx_msg void OnSelchangeComboCanBaudrateSlave();
	afx_msg void OnSelchangeComboTxCanTerminalM2();
	afx_msg void OnSelchangeComboTxCanCrcS();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnGridBeginEdit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridPaste(WPARAM wParam, LPARAM lParam);
private:
	CUIntArray uiArryCanCode;
	CStringArray strArryCanName;

	CMyGridWnd m_wndCanGrid;
	CMyGridWnd m_wndCanGridSlave;
	CCTSMonProDoc * m_pDoc;

public:
	// ksj 20200213 : CanFD 사용하는 경우에 컨트롤 활성화
	int ShowCanFDCtrl(void);
	afx_msg void OnCbnEditchangeComboCanBmsSjw();
	afx_msg void OnCbnEditchangeComboCanBmsType();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SettingCanFdDlg_TRANSMIT_H__DE829BB6_D401_4CB7_B0EC_2AD4305A98E3__INCLUDED_)
