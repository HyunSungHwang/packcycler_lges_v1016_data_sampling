// CaliPoint.h: interface for the CCaliPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALIPOINT_H__6323937C_1920_4C27_93C2_B5E4D26084A2__INCLUDED_)
#define AFX_CALIPOINT_H__6323937C_1920_4C27_93C2_B5E4D26084A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define MAX_CALIB_SET_POINT		15
#define MAX_CALIB_CHECK_POINT	15		

#define POINT_ALL	0
#define VTG_CAL_SET_POINT	1
#define VTG_CAL_CHECK_POINT	2
#define CRT_CAL_SET_POINT	3
#define CRT_CAL_CHECK_POINT	4

#define CAL_RANGE_ALL	0xffff	
#define CAL_RANGE1	0
#define CAL_RANGE2	1
#define CAL_RANGE3	2
#define CAL_RANGE4	3
#define CAL_RANGE5	4

#define REG_CAL_SECTION	"Calibration"

class CCaliPoint  
{
public:
//	SFT_CALI_SET_POINT GetNetworkFormatPoint();
/*	int m_nChargeDisChargeMode[SFT_MAX_VOLTAGE_RANGE];*/
	BOOL WriteDataToFile(FILE *fp);
	BOOL ReadDataFromFile(FILE *fp);
	void operator = (CCaliPoint& OrgPoint);

	BOOL SavePointData();
	BOOL LoadPointData();
//	BOOL AddPointData(WORD wRange = CAL_RANGE1, int nDataType = POINT_ALL);

	//set Data FTN
	BOOL SetVSetPointData(int nDataCount, double *pdData, WORD wRange = 0);
	BOOL SetVCheckPointData(int nDataCount, double *pdData, WORD wRange = 0);
	BOOL SetISetPointData(int nDataCount, double *pdData, WORD wRange = 0);
	BOOL SetICheckPointData(int nDataCount, double *pdData, WORD wRange = 0);

	//One Range Ftn
	double GetVSetPoint(WORD nPoint);
	double GetVCheckPoint(WORD nPoint);
	double GetISetPoint(WORD nPoint);
	double GetICheckPoint(WORD nPoint);

	//Muti Range Ftn
	double GetVSetPoint(WORD wRange, WORD nPoint);
	double GetVCheckPoint(WORD wRange, WORD nPoint);
	double GetISetPoint(WORD wRange, WORD nPoint);
	double GetICheckPoint(WORD wRange, WORD nPoint);

	void ResetPoint(WORD wRange = CAL_RANGE_ALL, int nDataType = POINT_ALL);
	WORD GetVtgSetPointCount(WORD wRange = CAL_RANGE1)		{		return m_nVCalPointNum[wRange];			};
	WORD GetVtgCheckPointCount(WORD wRange = CAL_RANGE1)	{		return m_nVCheckPointNum[wRange];		};
	WORD GetCrtSetPointCount(WORD wRange = CAL_RANGE1)		{		return m_nICalPointNum[wRange];			};
	WORD GetCrtCheckPointCount(WORD wRange = CAL_RANGE1)	{		return m_nICheckPointNum[wRange];		};
	CCaliPoint();
	virtual ~CCaliPoint();

	double	m_dVDAAccuracy[SFT_MAX_VOLTAGE_RANGE];			//각 Range별 전압 정밀도 
	double	m_dIDAAccuracy[SFT_MAX_VOLTAGE_RANGE];			//각 Range별 전류 정밀도
	double	m_dVADAccuracy[SFT_MAX_CURRENT_RANGE];			//각 Range별 전압 정밀도 
	double	m_dIADAccuracy[SFT_MAX_CURRENT_RANGE];			//각 Range별 전류 정밀도

protected:
	void SortData();
	WORD	m_nVCalPointNum[SFT_MAX_VOLTAGE_RANGE];
	WORD	m_nVCheckPointNum[SFT_MAX_VOLTAGE_RANGE];
	WORD	m_nICalPointNum[SFT_MAX_CURRENT_RANGE];
	WORD	m_nICheckPointNum[SFT_MAX_CURRENT_RANGE];
	double	m_VCalPoint[SFT_MAX_VOLTAGE_RANGE][MAX_CALIB_SET_POINT];
	double	m_VCheckPoint[SFT_MAX_VOLTAGE_RANGE][MAX_CALIB_CHECK_POINT];
	double	m_ICalPoint[SFT_MAX_CURRENT_RANGE][MAX_CALIB_SET_POINT];
	double	m_ICheckPoint[SFT_MAX_CURRENT_RANGE][MAX_CALIB_CHECK_POINT];

/*		WORD	m_nVCalPointNum[3];
	WORD	m_nVCheckPointNum[3];
	WORD	m_nICalPointNum[3];
	WORD	m_nICheckPointNum[3];

	double	m_VCalPoint[3][MAX_CALIB_SET_POINT];
	double	m_VCheckPoint[3][MAX_CALIB_CHECK_POINT];
	double	m_ICalPoint[3][MAX_CALIB_SET_POINT];
	double	m_ICheckPoint[3][MAX_CALIB_CHECK_POINT];*/
};

#endif // !defined(AFX_CALIPOINT_H__6323937C_1920_4C27_93C2_B5E4D26084A2__INCLUDED_)
