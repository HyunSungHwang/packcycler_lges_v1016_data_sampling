#if !defined(AFX_LOGSETDLG_H__0FEAC712_0F9E_4FD8_BFDB_651CF4C13221__INCLUDED_)
#define AFX_LOGSETDLG_H__0FEAC712_0F9E_4FD8_BFDB_651CF4C13221__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogSetDlg dialog

class CLogSetDlg : public CDialog
{
// Construction
public:
	CLogSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLogSetDlg)
	enum { IDD = IDD_LOG_CONFIG_DLG , IDD2 = IDD_LOG_CONFIG_DLG_ENG ,IDD3 = IDD_LOG_CONFIG_DLG_PL };
	BOOL	m_bUseAutoHide;
	UINT	m_nHideDelay;
	int		m_nLogLevel;
	BOOL	m_bAutoShow;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogSetDlg)
	afx_msg void OnUseAutoHideCheck();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGSETDLG_H__0FEAC712_0F9E_4FD8_BFDB_651CF4C13221__INCLUDED_)
