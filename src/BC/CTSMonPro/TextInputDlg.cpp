// TextInputDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "TextInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTextInputDlg dialog
#define	EP_PASSWORD		"dusrnth"

CTextInputDlg::CTextInputDlg(CString strPrevText, BYTE byMode, CRect* rect, CWnd* pParent /*=NULL*/)
	: CDialog(CTextInputDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTextInputDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	if( rect )
		CopyMemory(&m_rWindowRect, rect, sizeof(CRect));
	m_strData		= strPrevText;
	m_byMode		= byMode;
	m_bRightAdmin	= false;
}


void CTextInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTextInputDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTextInputDlg, CDialog)
	//{{AFX_MSG_MAP(CTextInputDlg)
	ON_EN_KILLFOCUS(IDC_EDIT1, OnKillfocusEdit1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextInputDlg message handlers

BOOL CTextInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if( m_byMode == 0x01 )
	{
		ModifyStyleEx(0, 
			WS_EX_CLIENTEDGE|WS_EX_DLGMODALFRAME|WS_EX_WINDOWEDGE);
		CRect rect;
		rect.top = 360;
		rect.left = 420;
		rect.right = rect.left + 260;
		rect.bottom = rect.top + 27;
		m_rWindowRect = rect;
	}
	
	SetDlgItemText(IDC_EDIT1, m_strData);
	MoveWindow(m_rWindowRect);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CTextInputDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( m_byMode == 0x01 )
		{
			((CEdit*)GetDlgItem(IDC_EDIT1))->SetPasswordChar('*');
		}
		switch( pMsg->wParam )
		{
		case VK_RETURN:	
			GetDlgItemText(IDC_EDIT1, m_strData);
			if( m_byMode == 0x01 )
			{
				if( m_strData.Compare(_T(EP_PASSWORD)) == FALSE )
					m_bRightAdmin = true;
			}
			OnOK();
			if( m_byMode == 0x01 )
				m_strData = _T("");
			return FALSE;
		case VK_ESCAPE:
			OnCancel();
			return FALSE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CTextInputDlg::OnKillfocusEdit1() 
{
}
