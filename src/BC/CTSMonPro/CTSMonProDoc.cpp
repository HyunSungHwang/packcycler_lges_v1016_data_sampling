// CTSMonProDoc.cpp : implementation of the CCTSMonProDoc class
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CalibratorDlg.h"
#include "SafetyUpdateDlg.h"
#include "SchUpdateDlg.h"

#include "CTSMonProDoc.h"
#include "CTSMonProView.h"
#include "LogDlg.h"
#include "IPSetDlg.h"
#include "MainFrm.h"

#include "AlarmReadyDlg.h"		//ljb 20130514
//#include "MiniDump.h"			//yulee 20190121
#include "UpdateDatabase.h"		//ksj 20201013

#include "ModbusConvDlg.h"
#include "LinConvDlg.h"
#include "uartconvdlg.h"
#include "Global.h"

/////////////////////////////////////////////////////////////////////////////
#include "SendToFTP_v1011_v2_SCH.h"
#include "SendToFTP_v1013_v1_SCH.h"
#include "SendToFTP_v1014_v1_SCH.h"
#include "SendToFTP_v1015_v1.h"

/////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProDoc

IMPLEMENT_DYNCREATE(CCTSMonProDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSMonProDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSMonProDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProDoc construction/destruction

CCTSMonProDoc::CCTSMonProDoc()
{
	m_nChamberWriteCount1 = 0;
	m_nChamberWriteCount2 = 0;
	
	m_lpCyclerMD = NULL;
	m_pCalibratorDlg = NULL;
	m_pSafetyUpdateDlg = NULL;
	m_pSchUpdateDlg = NULL;

	m_pLogDlg =NULL;
	m_pLogDlg = new CLogDlg();
	//m_pLogDlg->Create(IDD_LOG_DLG);
	m_pLogDlg->Create(//&&
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_LOG_DLG): //&&
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_LOG_DLG_ENG): //&&
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_LOG_DLG_PL): //&&
		IDD_LOG_DLG);//&&
	

	m_pEventLogDlg =NULL;
	m_pEventLogDlg = new CLogDlg();
	//m_pEventLogDlg->Create(IDD_LOG_DLG);
	m_pEventLogDlg->Create(//&&
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_LOG_DLG): //&&
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_LOG_DLG_ENG): //&&
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_LOG_DLG_PL): //&&
		IDD_LOG_DLG);//&&
	m_pEventLogDlg->Fun_SetTitle("Event Log");
		
	m_nSendingModeComm_ch1 = 0;
	m_nSendingModeComm_ch2 = 0;

	//초기 등록된 모듈의 수지정 
	m_nInstalledModuleNo = 4;

	m_bAutoRestartFromPrevUnsafeShutdown = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoRestart", FALSE);
	if( m_bAutoRestartFromPrevUnsafeShutdown == FALSE )
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AutoRestart", 0);
		m_bAutoRestartFromPrevUnsafeShutdown = FALSE;
	}
	
	m_nAutoRestartRemainTime = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoRestartRemainTime", -1);
	m_bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE);
	float OverChargerMinVolt = 0; //yulee 20191017 OverChargeDischarger Mark
	if(m_bOverChargerSet) //yulee 20180810
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);
	if( m_nAutoRestartRemainTime == -1 )
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AutoRestartRemainTime", 30);
		m_nAutoRestartRemainTime = 30;
	}
		
	m_VoltageUnitMode = 0;
	m_CurrentUnitMode = 0;

	m_nTestCondStepNo = 0;
	m_pCalibratorDlg = NULL;
	m_bChContinueNo = FALSE;

	m_nRunCmdDelayTime = 0;

	m_uiOvenCheckTime_1 = 0;		
	m_fOvenDeltaTemp_1 = 0.0f;
	
	m_uiOvenCheckFixTime_1 = 0;		
	m_fOvenDeltaFixTemp_1 = 0.0f;
	//m_pControlSystemSock = NULL;

	//////////////////////////////////////////////////////////////////////////
	//  + BW KIM 2014.03.11
	//m_pAlarmReadyDlg1 = NULL;
	// -
	//////////////////////////////////////////////////////////////////////////
	m_uiOvenCheckTime_2 = 0;		
	m_fOvenDeltaTemp_2 = 0.0f;
	
	m_uiOvenCheckFixTime_2 = 0;		
	m_fOvenDeltaFixTemp_2 = 0.0f;


	m_FileSaveThread	= NULL;
	m_LoaderThread1 = NULL;
	m_LoaderThread2 = NULL;
	m_SBCFileDownThread			= NULL;

	m_OvenThread1 = NULL;
	m_OvenThread2 = NULL;
	
	m_wait_FTP_endTime			= 180;
	m_wait_FTP_endTime_plusPtrn = 0;

	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.28 디버그 모드 에서 프로그램 작업 시작 시 초기화가 되지 않아서 죽음
	m_pAlarmReadyDlg1 = NULL;
	m_pAlarmReadyDlg2 = NULL;
	// - 
	//////////////////////////////////////////////////////////////////////////

	m_bSimpleTest = FALSE;
	m_bUseHLGP = AfxGetApp()->GetProfileInt("Config","UseHLGP",0); //lyj 20200730

	m_bChamberStand = TRUE; // 2014.12.23 챔버 대기 모스 사용 기본 1

	m_bDayChange = FALSE;
	m_bRunFtpDown = FALSE;					//ljb 20150522 add
	m_bWorkingFtpDown = FALSE;				//ljb 20150522 add
	m_awAutoRestoreChArray.RemoveAll();		//자동복구 할 모듈채널 정보 arry
	
	m_nFtpResponseCode = 0;
	
	m_bRunFtpDownStepEnd = FALSE;		//ksj 20160801
	m_nFtpDownStepEndModuleID = -1;		//ksj 20160801
	m_nFtpDownStepEndChannelIndex = -1;	//ksj 20160801
	m_FTPFileDownRetryCnt = 0;
	m_ucFtpDownIndex = 0; //lyj 20200526 다운로드 개선

	m_bRunFtpDownAllSBCData = FALSE;//yulee 20190420 //yulee 20190706
	m_nChamberStopRetry1 = 0; //yulee 20180910 
	m_nChamberStopRetry2 = 0; //yulee 20180910 
	m_nIsChamberStop1 = 0;
	m_nIsChamberStop2 = 0;
	// 200313 HKH Memory Leak 수정 ===============
	m_bFinishFileSave = false;
	m_bFinishFileSaveAck = false;
	m_bFinishSBCFileDown = false;
	m_bFinishSBCFileDownAck = false;
	m_bFinishLoader1 = false;
	m_bFinishLoader1Ack = false;
	m_bFinishLoader2 = false;
	m_bFinishLoader2Ack = false;
	// =============================================
}

CCTSMonProDoc::~CCTSMonProDoc()
{
	RemoveModule();

	//if(m_FileSaveThread) m_FileSaveThread->SuspendThread();
	//if(m_LoaderThread1) m_LoaderThread1->SuspendThread();
	//if(m_LoaderThread2) m_LoaderThread2->SuspendThread();

// 	if(m_OvenThread1) m_OvenThread1->SuspendThread();
	//FinishDocThread();	// 200313 HKH Memory Leak 수정 //ksj 20201023 : 이곳은 주석처리. 이중호출되서 문제인지. 프로그램 종료가 매끄럽게 되지 않는다.
// 	if(m_OvenThread2) m_OvenThread2->SuspendThread();

	if(m_OvenThread1)
	{
		if(m_OvenThread1->m_hThread)
			m_OvenThread1->SuspendThread();
	}
	if(m_OvenThread2)
	{
		if(m_OvenThread2->m_hThread)
			m_OvenThread2->SuspendThread();
	}
}

//새로운 Document 생성시 
BOOL CCTSMonProDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;	

	CString strMsg;

	//Registry 설정값을 읽는다.
	ReadRegistryInfo();

	//DataBase에서 현재 설정된 모듈 수를 검색한다.
	m_nInstalledModuleNo = SearchInstalledModuleCount();	
	if(m_nInstalledModuleNo <=0 )		//DataBase에 설정된 모듈이 없을 경우
	{
		//AfxMessageBox("DataBase에 설정된 정보를 찾을 수 없습니다.");		
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_OnNewDocument_msg1","CTSMonPro_DOC"));		//&&
		return FALSE;
	}

	//Network 초기화 한다. 
	if(InitNetworkConfig() == FALSE)
	{

		//$1013 strMsg.Format("Network를 초기화 할 수 없습니다.(%s)", ::SFTGetErrorString(::SFTGetLastError()));
		strMsg.Format(Fun_FindMsg("OnNewDocument_msg1","CTSMonPro_DOC"), ::SFTGetErrorString(::SFTGetLastError())); //&&
		AfxMessageBox(strMsg); 
		return FALSE;
	}
	
	AssignModuleMemory(m_nInstalledModuleNo);
	ASSERT(m_lpCyclerMD);

	//ksj 20200120 : 프로그램 실행시 DB 업데이트
	UpdateDatabase();

	//ksj 20201013 : 프로그램 실행시 채널코드별 깜빡임 테이블 로드
	LoadFlickerTable(); 

	//DataBase 각 모듈의 설정값을 읽어 들인다.	
	if(SetModuleConfig() == FALSE)
		return FALSE;
	
	//CAN AUX Function division code Load 한다.
//$1013	if (Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN 분류 CODE 가져 오기 실패");
	if (Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("OnNewDocument_msg2","CTSMonPro_DOC")); //&&


	//Server Listen을 시작한다.
	::SFTServerStart();
	
	theFtpConnection = NULL;			//20130426 FTP Point 초기화

	//-------------------
	LoadOvenData(1);
	LoadOvenData(2);
	//-------------------

	m_FileSaveThread = AfxBeginThread(SetChannelDataThread, this);
	m_SBCFileDownThread = AfxBeginThread(ThreadFtpDownload, this);				//ljb 20150520 add Ftp down 쓰레드

	m_LoaderThread1 = AfxBeginThread(ChkLoaderState_1, this);
	m_LoaderThread2 = AfxBeginThread(ChkLoaderState_2, this);

	m_OvenThread1 = AfxBeginThread(ChkOvenState_1, this);		//ljb 20180219 add
	m_OvenThread2 = AfxBeginThread(ChkOvenState_2, this);		//ljb 20180219 add

	//cny------------------
	LoadChillerData(1);
	LoadChillerData(2);
	LoadChillerData(3);
	LoadChillerData(4);
	//-------------------

	return TRUE;
}

//Registry에 설정된 정보를 Loading한다.
BOOL CCTSMonProDoc::ReadRegistryInfo()
{
	//현재 Program이 설치된 Path를 찾는다.
	//만약 Path 정보를 찾지 못하면 설치가 안된 걸로 보고 프로그램을 시작할 수 없다.
	m_strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
	if(m_strCurPath.IsEmpty())	
	{
		AfxMessageBox(Fun_FindMsg("ReadRegistryInfo_msg1","CTSMonPro_DOC"));	//AfxMessageBox("프로그램 설치 정보를 찾을 수 없습니다.");
		return FALSE;
	}
	
	//DataBase 폴더 생성

// 	switch(nLanguage) 20190701
// 	{
// 	case 1: m_strDataBaseName = m_strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME; break;
// 	case 2: m_strDataBaseName = m_strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	case 3: m_strDataBaseName = m_strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_PL; break;
// 	default : m_strDataBaseName = m_strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME_EN; break;
// 	}

	//ksj 20200704 : 시스템 레지스트리 등록
	SetFTPCache();
	SetWindowsMinidump();
	//ksj end


// 	m_strDataBaseName = m_strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME;
// 	m_strCodeDataBaseName = m_strCurPath + "\\DataBase\\" + PS_CODE_DATABASE_NAME; //ksj 20200804
// 	m_strWorkInfoDataBaseName = m_strCurPath + "\\DataBase\\" + PS_WORKINFO_DATABASE_NAME;

	InitDatabasePath(); //ksj 20201012 : 데이터 베이스 경로 설정 함수 분리

	CString strTemp;
	strTemp = m_strCurPath + "\\Log";
	if (file_Finder(strTemp) == FALSE) ForceDirectory(strTemp);

	strTemp = m_strCurPath + "\\Event_Log";
	if (file_Finder(strTemp) == FALSE) ForceDirectory(strTemp);

	//Temp 폴더 생성
	strTemp = m_strCurPath + "\\Temp";
	if (file_Finder(strTemp) == FALSE) ForceDirectory(strTemp);

	//Calibration 폴더 생성
	strTemp = m_strCurPath + "\\Calibration";
	if (file_Finder(strTemp) == FALSE) ForceDirectory(strTemp);

	//ksj 20200702 : Config 폴더 생성
	strTemp = m_strCurPath + "\\Database\\Config";
	if (file_Finder(strTemp) == FALSE) ForceDirectory(strTemp);

	//State 폴더 생성. //ksj 20200901 : Json 모니터링 파일 생성용 폴더
	strTemp = m_strCurPath + "\\State";
	if (file_Finder(strTemp) == FALSE) ForceDirectory(strTemp); //프로그램 설치 경로 아래에 State 폴더 생성.

	//channel의 저장할 Data를 몇개까지 Stack한 다음에 저장할 것인가  
	m_nSaveStackSize = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Stack Size", 1);
	//기본으로 1개 저장할 dAta가 있으면 바로 저장하도록 설정
	if(m_nSaveStackSize == 0)	m_nSaveStackSize = 1;
	
	//Calibration Point Data Loading from registry...
	m_CalPointData.LoadPointData();


	m_bAutoRestartFromPrevUnsafeShutdown = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoRestart", FALSE);

	m_nAutoRestartRemainTime = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoRestartRemainTime", -1);
	if( m_nAutoRestartRemainTime == -1 )
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AutoRestartRemainTime", 30);
		m_nAutoRestartRemainTime = 30;
	}
		
	UpdateUnitSetting();

	//전압 전류 단위 Mode
	m_VoltageUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Vtg Unit Mode", 0);
	m_CurrentUnitMode = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0);

	m_bChContinueNo = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "ChNumberMode", 0);

	m_strModuleName = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Module Name", "Module");

	m_nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 1); // 0 : 모든 채널 하나의 챔버 사용,   1: 채널별 별도의 챔버 사용
	//////////////////////////////////////////////////////////////////////////
	//ljb 2011 챔버 설정 읽어 오기 (프로그램 종료 했다가 다시 실행해도 챔버 체크 하도록)
	m_iOvenStartOption_1 = m_iOvenStartOption_2 = AfxGetApp()->GetProfileInt(OVEN_SETTING, "Start Chamber Option", 0);
	m_uiOvenCheckTime_1 = m_uiOvenCheckTime_2 = AfxGetApp()->GetProfileInt(OVEN_SETTING, "Fix Check Time", 0);
	m_uiOvenCheckFixTime_1 = m_uiOvenCheckFixTime_2 = AfxGetApp()->GetProfileInt(OVEN_SETTING, "Schedule Check Time", 0);

	strTemp = AfxGetApp()->GetProfileString(OVEN_SETTING, "Fix Temp","25.0");
	m_fOvenStartFixTemp_1 = m_fOvenStartFixTemp_2 = atof(strTemp);
	
	strTemp = AfxGetApp()->GetProfileString(OVEN_SETTING, "Delta Fix Temp","1");
	m_fOvenDeltaFixTemp_1 = m_fOvenDeltaFixTemp_2 = atof(strTemp);
	
	strTemp = AfxGetApp()->GetProfileString(OVEN_SETTING, "Delta Temp","1");
	m_fOvenDeltaTemp_1 = m_fOvenDeltaTemp_2 = atof(strTemp);
	
	m_uiOvenPatternNum_1 = m_uiOvenPatternNum_2 = AfxGetApp()->GetProfileInt(OVEN_SETTING, "Pattern No", 1);
	//////////////////////////////////////////////////////////////////////////

	m_nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 1); // 0 : 모든 채널 하나의 챔버 사용,   1: 채널별 별도의 챔버 사용
	
	return TRUE;
}


BOOL CCTSMonProDoc::InitNetworkConfig()
{
	//현재 View를 Message전달 Windows로 한다. 
	POSITION pos = GetFirstViewPosition();
	CCTSMonProView *pView = (CCTSMonProView *)GetNextView(pos);

	//ksj 20200127 : PSServer 로그 경로 설정
	CString strInstallPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "C:\\Program Files (x86)\\PNE CTSPack");
	CString strLogPath;
	SYSTEMTIME systemTime;   // system time
	::GetLocalTime(&systemTime);

	strLogPath.Format("%s\\log\\%d%02d%02d.log", strInstallPath, systemTime.wYear, systemTime.wMonth, systemTime.wDay);	
	SFTSetLogFileName((LPSTR)(LPCTSTR)strLogPath);
	//ksj end

//	return ::SFTOpenFormServer(m_nInstalledModuleNo, pView->m_hWnd); 
//#ifdef _PARALLEL_FORCE 전처리기 주석처리
if(g_AppInfo.iPType==1)
	return ::SFTOpenFormServer(m_nInstalledModuleNo, pView->m_hWnd, g_AppInfo.iPType);    //yulee 20190705 강제병렬 옵션 처리 Mark
//#else
else
	return ::SFTOpenFormServer(m_nInstalledModuleNo, pView->m_hWnd); 
//#endif

}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProDoc serialization

void CCTSMonProDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProDoc diagnostics

#ifdef _DEBUG
void CCTSMonProDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSMonProDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

//로그  Message를 화면에 표시하고 저장한다.
void CCTSMonProDoc::WriteLog(CString strMsg, INT nLevel)
{
 	if(m_pLogDlg == NULL)
	{
		return;
	}
 	m_pLogDlg->WriteLog(strMsg, nLevel);
}

//로그  Message를 화면에 표시하고 저장한다.
void CCTSMonProDoc::WriteEventLog(CString strMsg, INT nLevel)
{
	if(m_pEventLogDlg == NULL)
	{
		return;
	}
	m_pEventLogDlg->WriteEventLog(strMsg, nLevel);
}

//system 관련 로그(통신등)
//설치 폴더\log\20050329.log 로 날짜별로 생성
void CCTSMonProDoc::WriteSysLog(CString strMsg, INT nLevel)
{
	WriteLog(strMsg, nLevel);	
}

//system 관련 Event 로그(error등)
//설치 폴더\Event_log\20050329.log 로 날짜별로 생성
void CCTSMonProDoc::WriteSysEventLog(CString strMsg, INT nLevel)
{
	WriteEventLog(strMsg, nLevel);	
}

//작업 Log 기록 
//결과 폴더\\작업명.log로 생성
void CCTSMonProDoc::WriteWorkLog(int nModuleID, int nChannelIndex, CString strMsg, INT nLevel)
{
	CCyclerChannel *pChannel = GetChannelInfo(nModuleID, nChannelIndex);
	if(pChannel)
	{
		pChannel->WriteLog(strMsg);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProDoc commands
// 모듈이 접속이 시도되어 정상적으로 접속 되었을 경우 
void CCTSMonProDoc::SetModuleLineOn(UINT nModuleNo, LPSFT_MD_SYSTEM_DATA lpSysParam)
{
	ASSERT(lpSysParam);

	CString strLogMsg;

	m_strModuleInfo.Format("%d,%x,%d,%s"
		,lpSysParam->nSystemType,lpSysParam->nProtocolVersion,lpSysParam->nOSVersion,lpSysParam->szModelName);

	//접속된 모듈의 채널 수를 구해서 현재 모듈 구조체의 채널 구조체를 만든다.
	int nChCount = lpSysParam->nInstalledChCount;
	if( nChCount < 0)	return;

	UINT nModuleIndex = GetModuleIndex(nModuleNo);
	ASSERT(nModuleIndex >= 0);

	m_lpCyclerMD[nModuleIndex].MakeChannel(nChCount);
	m_lpCyclerMD[nModuleIndex].MakeAuxData();
	m_lpCyclerMD[nModuleIndex].MakeParallel();

	//if(lpSysParam->nProtocolVersion >= _SFT_PROTOCOL_VERSION)
	if(lpSysParam->nProtocolVersion >= _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : V1015 호환 추가
	{
		m_lpCyclerMD[nModuleIndex].MakeCAN();
		m_lpCyclerMD[nModuleIndex].MakeCANTrans();
	}
	
	//SFTWM_MODULE_CONNECTED -> CAN Board Type write ljb 20181119 add
	//int nCanBoardType = lpSysParam->byCanCommType;
	//nCanBoardType=0;	//GUI에서 임시 처리
	//AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CAN Board Type", nCanBoardType);

	//strLogMsg.Format("%s 접속 완료", GetModuleName(nModuleNo));
	//strLogMsg.Format("%s connected", GetModuleName(nModuleNo));
	strLogMsg.Format(Fun_FindMsg("CTSMonProDoc_SetModuleLineOn_msg1","CTSMonPro_DOC"), GetModuleName(nModuleNo));//&&
	WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
	
	//Module 정보를 dataBase에 저장한다.
	WriteModuleInfoToDataBase(nModuleNo);

	//strLogMsg.Format("%s 디비 저장 완료.", GetModuleName(nModuleNo));
	//strLogMsg.Format("%s database save completion.", GetModuleName(nModuleNo));
	strLogMsg.Format(Fun_FindMsg("CTSMonProDoc_SetModuleLineOn_msg2","CTSMonPro_DOC"), GetModuleName(nModuleNo));//&&
	WriteSysLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
		
	//이전 작업한 최종 정보를 Loading한다.
	CCyclerModule *pMD =GetModuleInfo(nModuleNo);
	if(pMD)
	{
		if(pMD->IsCheckedUnSafetyShutdown() == FALSE)
		{
			pMD->CheckRunningChannel();
		}
	}

	//WriteLog("===SetModuleLineOn complete.",1);
	WriteLog(Fun_FindMsg("CTSMonProDoc_SetModuleLineOn_msg3","CTSMonPro_DOC"),1);//&&
	
}

//모듈과의 접속이 끈어졌을 때
void CCTSMonProDoc::ResetModuleLineState(UINT nModuleNo)
{
	CString strLogMsg;

	//전체 모듈에 대해 검색
	CCyclerChannel *pCh;
	CCyclerModule *pMD = GetModuleInfo(nModuleNo);
	if(pMD)
	{
		//모든 채널에 대해 검색
		for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
		{
			pCh = pMD->GetChannelInfo(ch);
			if(pCh)
			{

				SFT_MD_SYSTEM_DATA * pSysData = ::SFTGetModuleSysData(pMD->GetModuleID());

				//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION)
				if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20200121
				{
					if(pCh->CheckAndSaveData() > 0)		//kjh //2007/07/23
					{
						//$1013 pCh->WriteLog("통신 두절");		//작업중이면 통신 두절 상태 기록
						pCh->WriteLog(Fun_FindMsg("ResetModuleLineState_msg1","CTSMonPro_DOC"));		//작업중이면 통신 두절 상태 기록 //&&
					}
				}
				else //v1015~
				{
					if(pCh->CheckAndSaveData_v100D() > 0)		//kjh //2008/07/28
					{
						//$1013 pCh->WriteLog("통신 두절");		//작업중이면 통신 두절 상태 기록/
						pCh->WriteLog(Fun_FindMsg("ResetModuleLineState_msg1","CTSMonPro_DOC"));		//작업중이면 통신 두절 상태 기록 //&&
					}
				}			
			}
		}

		pMD->ResetUnSafetyShutdownCheck();
	}

	//$1013 strLogMsg.Format("%s 접속 종료", GetModuleName(nModuleNo));
	strLogMsg.Format(Fun_FindMsg("ResetModuleLineState_msg2","CTSMonPro_DOC"), GetModuleName(nModuleNo)); //&&
	WriteLog(strLogMsg, CT_LOG_LEVEL_NORMAL);
}

void CCTSMonProDoc::SetCalResultData(int nModuleID, char* pBuf)
{
	CString strMsg;
	if( m_pCalibratorDlg && pBuf && m_pCalibratorDlg->IsWindowVisible())
	{
		SFT_CALI_END_DATA calResultData;
		CopyMemory(&calResultData, pBuf, sizeof(SFT_CALI_END_DATA));
		m_pCalibratorDlg->SetReceiveResultData(&calResultData);
	}
	else
	{
		//$1013 strMsg.Format("%s 교정모드가 아닌데 교정 완료명령이 수신되었습니다.", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("SetCalResultData_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID)); //&&
		WriteLog(strMsg);
	}
}
void CCTSMonProDoc::SetStepInfoResultData(int nModuleID, int nChannelIndex, char* pBuf)
{
	CString strMsg;
	CString strTemp;
/*
	//ksj 20170808 : 창 새로 생성.
	if(m_pSchUpdateDlg)
	{
	//	if(!m_pSchUpdateDlg->IsWindowVisible()) //창이 떠있지 않은 경우 제거하고 생성.
		{
			delete m_pSchUpdateDlg;
			m_pSchUpdateDlg = NULL;
		}		
	}
	if(m_pSchUpdateDlg == NULL)
	{
		m_pSchUpdateDlg = new CSchUpdateDlg(nModuleID, nChannelIndex, this);
		m_pSchUpdateDlg->Create(IDD_SCH_UPDATE_DLG, AfxGetMainWnd()->GetTopWindow());
#ifdef _DEBUG
		m_pSchUpdateDlg->GetWindowText(strMsg);
		strTemp.Format(_T("%s (M%02dC%02d)"),strMsg,nModuleID,nChannelIndex+1);
		m_pSchUpdateDlg->SetWindowText(strTemp);
#endif
	}
	m_pSchUpdateDlg->ShowWindow(SW_SHOW);
	//ksj end*/

	if( m_pSchUpdateDlg && pBuf && m_pSchUpdateDlg->IsWindowVisible())
	{
		//AfxMessageBox("stepinfo rec");	//20170731
		SFT_STEP_CONDITION_V3 stepInfoData; //lyj 20200214 LG v1015 이상
		CopyMemory(&stepInfoData, pBuf, sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상
		m_pSchUpdateDlg->SetReceiveStepInfoData(&stepInfoData);
	}
	else
	{
		//$1013 strMsg.Format("%s Step Info error 수신되었습니다.", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("SetStepInfoResultData_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID)); //&&
		WriteLog(strMsg);
	}
}
void CCTSMonProDoc::SetSafetyInfoResultData(int nModuleID, char* pBuf)
{
	CString strMsg;
	if( m_pSafetyUpdateDlg && pBuf && m_pSafetyUpdateDlg->IsWindowVisible())
	{
		//AfxMessageBox("safety info rec");	//20170731
 		SFT_CMD_SAFETY_UPDATE_REPLY safetyInfoData;
 		CopyMemory(&safetyInfoData, pBuf, sizeof(SFT_CMD_SAFETY_UPDATE_REPLY));
 		m_pSafetyUpdateDlg->SetReceiveInfoData(&safetyInfoData);
	}
	else
	{
		//$1013 strMsg.Format("%s Safety info error 수신되었습니다.", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("SetSafetyInfoResultData_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID)); //&&
		WriteLog(strMsg);
	}
}

//nSize => not use
void CCTSMonProDoc::SetChannelData(int nModuleID, char  *pBuf , int nSize)
{
	CString strMsg;
	CCyclerChannel	*lpChannelInfo= NULL;		//Channel 
	CCyclerModule *lpModule = GetCyclerMD(nModuleID);
	int nRtn =0;
	int nStackedSize = 0;
	
	if(lpModule == NULL)	return;

	//////////////////////////////////////////////////////////////////////////
	// 쓰레드로 기록하기 위한 데이터 전달
	lpModule->m_ThreadBuf = (LPARAM)pBuf;
	return ;
	//////////////////////////////////////////////////////////////////////////
}

UINT CCTSMonProDoc::SetChannelDataThread(LPVOID pParam) //lmh 저장용 스레드
{
	//return 0;
	//return 0;
	CCTSMonProDoc *pMainDoc = (CCTSMonProDoc*)pParam;
	CString strMsg;
	CCyclerChannel	*lpChannelInfo;
	CCyclerModule *lpModule;
	int nRtn =0;
	int nStackedSize = 0;

	lpModule= NULL;		
	lpChannelInfo= NULL;		//Channel 
	
	int nTotCh =0;
	int nDataStack;
	nDataStack = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Stack Size", 1);	//ljb 201009
	CScheduleData * pSchedule = NULL;
	CStep *pStep = NULL;
	CStep *pStep2 = NULL;

	while(!pMainDoc->m_bFinishFileSave)	// 200313 HKH Memory Leak 수정
	{
		for(int nModuleID = 1; nModuleID < pMainDoc->GetInstallModuleCount()+1 ;nModuleID++)
		{
			lpModule = pMainDoc->GetCyclerMD(nModuleID);
			nRtn =0;
			nStackedSize = 0;
			
			//TRACE("1 module id =%d, Totch = %d \n",nModuleID,lpModule->GetTotalChannel());

			if(lpModule == NULL)	continue;
			if(lpModule->m_ThreadBuf == 0) continue;
			
			if(lpModule->GetState() == PS_STATE_LINE_OFF )	continue;//ljb 20141231 add
			nTotCh = lpModule->GetTotalChannel();
			char *lpDataSel = new char[nTotCh];  
			memcpy(lpDataSel, (char *)lpModule->m_ThreadBuf, nTotCh);
			lpModule->m_ThreadBuf = 0;
			
			if (nDataStack < 30) nDataStack = 30;		//ljb 201009
			
			for( int nChNo = 0; nChNo <nTotCh; nChNo++ )
			{
				//채널 정보를 구한다.
				lpChannelInfo = pMainDoc->GetChannelInfo(nModuleID, nChNo);
				if(lpChannelInfo == NULL) continue;
				if(lpChannelInfo->GetFileLocking()) continue; //파일이 저장중일때는 스텍에 보관
				
				//현재 Step의 저장조건으로 지정 시간동안 쌓이는 Data 수량을 계산한다.
				pSchedule = lpChannelInfo->GetScheduleData();
				
				if(pSchedule != NULL)
				{
					pStep = pSchedule->GetStepData(lpChannelInfo->GetStepNo()-1);
					if(pStep)
					{
						//20090120 KBH
						if(pStep->m_fReportTime >= 1.0f)	//저장조건이 지정되어 있으면 
						{
							nDataStack = int((float)pMainDoc->m_nSaveStackSize/pStep->m_fReportTime);	//수량계산
							if(nDataStack < 1)	nDataStack = 1;				//지정 시간보다 저장 조건 시간이 클경우는 지정시간에 1개씩 저장한다.
						}
					}
				}
				else 
					continue;

				//Stack에 일정량이 쌓이면 저장한다.
				//매순간(ex 1초간격) 저장시 Overload가 있을 수 있기 때문에 10개나 20개 정도 모아졌을때 
				//한꺼번에 저장한다.
				
				nStackedSize = lpChannelInfo->GetStackedSize();
				ASSERT(pMainDoc->m_nSaveStackSize > 0);	//m_nSaveStackSize는 최소 1이상의 값을 갖는다.
				
				//종료 Step이거나 Run이 아니면 Stack의 모든 Data를 저장한다.
				if(	nStackedSize >= nDataStack			//동시 시작한 채널은 동시에 끝나 부하가 많이 걸릴 수 있으므로 분산 저장
					|| (nStackedSize > 0 && lpChannelInfo->GetState() != PS_STATE_RUN) 
					|| lpDataSel[nChNo] == SFT_SAVE_STEP_END)
				{
					if(lpChannelInfo->IsParallel() == FALSE || (lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == TRUE))
					{
						//채널에 저장할 Data가 누적되어있는지 Check한 후 있으면 저장한다.
						SFT_MD_SYSTEM_DATA * pSysData = ::SFTGetModuleSysData(lpModule->GetModuleID());
						//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION)
						if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20200121
							nRtn = lpChannelInfo->CheckAndSaveData();
						else
							nRtn = lpChannelInfo->CheckAndSaveData_v100D();
						//////////////////////////////////////////////////////////////////////////////////
						
						lpChannelInfo->SetFileLocking(FALSE);		//ljb 201101
						
						pStep2 = pSchedule->GetStepData(lpChannelInfo->GetStepNo()); //다음 스텝을 구한다.
						if(pStep2 != NULL)
						{
							if(pStep2->m_type == PS_STEP_CHARGE || pStep2->m_type == PS_STEP_DISCHARGE)
							{
								if(lpChannelInfo->GetChamberUsing() == TRUE && lpDataSel[nChNo] == SFT_SAVE_STEP_END) 
								{
									//CWordArray aw;
									//aw.Add((WORD)nChNo);
									//lpChannelInfo->m_bOvenLinkChargeDisCharge = true; //주석처리 by ksj 20201207 : 무쓸모인것 같음. 지우고나서 혹시 부작용 있을 수 있으니 주의.

									/*//ksj 20201207 : 코멘트 추가
									챔버 연동 모드이고 현재 스텝이 종료되었을때
									다음 스텝이 충전/방전이면 flag를 true 로 바꾸는 것으로 보임.
									근데 이게 true 이면 
									화면 채널 정보에 (챔버연동온도대기멈춤) 이라고 뜨는데,
									그 뒤로 없애는 코드가 없음. 가동중인데도 이렇게 뜨고 의도를 알기 어려움.
									그냥 이 메세지 표시하지 않도록 위 기능 주석처리함.
									//ksj end*/
								}
							}
						}
					
						if(nRtn == 1 && lpDataSel[nChNo] == SFT_SAVE_STEP_END)//lmh 20120120 데이터 자동 복구 모듈 add
						{
							pStep2 = pSchedule->GetStepData(lpChannelInfo->GetStepNo() - 1); //현재 스텝을 구한다.
							BOOL	bCheckPrevData = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckUnsafeData", TRUE);
							CWordArray wOneCh;
							wOneCh.Add((WORD)nChNo);
							if(pStep2 != NULL)
							{
							}
						}

						if(nRtn  < 0)
						{
							strMsg.Format("%s Ch %d Data 저장 실패, (Code %d)", pMainDoc->GetModuleName(nModuleID), nChNo+1, nRtn); //$1013 //yulee 20190405 check
							pMainDoc->WriteLog(strMsg);
						}
						else
						{
							if(nRtn == 1)
							{
								if(lpDataSel[nChNo] == SFT_SAVE_STEP_END)
								{
									strMsg.Format("%s Ch %d Step 완료 수신", pMainDoc->GetModuleName(nModuleID), nChNo+1); //$1013 //yulee 20190405 check
									pMainDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
								}
								
								if(lpChannelInfo->GetState() != PS_STATE_RUN)
								{									
									//$1013 strMsg.Format("%s Ch %d 시험 완료 처리", pMainDoc->GetModuleName(nModuleID), nChNo+1); 
									strMsg.Format("%s Ch %d 시험 완료 처리", pMainDoc->GetModuleName(nModuleID), nChNo+1); //&&번역필요 //yulee 20190405 check
									pMainDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
								}
							}
						}
					}			
				}
			}			
			delete[] lpDataSel;
		}
		Sleep(100); //2014.08.14
	}		
	pMainDoc->m_bFinishFileSaveAck = true;	// 200313 HKH Memory Leak 수정

	return 0;
}

UINT CCTSMonProDoc::ChkLoaderState_1(LPVOID pParam)
{
	CCTSMonProDoc *pMainDoc = (CCTSMonProDoc*)pParam;
	CString strMsg, strLoadOnOff, strLoadMode;
	CCyclerModule	*lpModule;
	CCyclerChannel	*lpChannelInfo;
	
	strLoadOnOff = strLoadMode = "";
	lpModule= NULL;		
	lpChannelInfo= NULL;		//Channel 
	
	int mdIndex =1;
	int nRtn =0;
	int nTotCh =0;
	CStep *pStep = NULL;
	//CStep *pStep2 = NULL;
	
	ZeroMemory(&pMainDoc->m_sLoaderData1,sizeof(SFT_LOADER_VALUE));
	::SFTSetLoaderData(mdIndex,0,&pMainDoc->m_sLoaderData1);
	if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Load 1", FALSE) == FALSE)
	{
		pMainDoc->m_LoaderThread1 = NULL; //ksj 20170302 : 쓰레드 포인터 초기화 추가.
		return 0;
	}

	//TRACE(" ====================>>>>>>>>    Loader Thread start ChkLoaderState_1 \n");
	int nModuleID = 1;
	int nChNo = 0;

	int nRetryCount = 0;
	int nPauseCount = 0;
	float fLoadV =0.0f, fLoadA =0.0f;
	CWordArray chArray;
	chArray.Add(nChNo);
	
	while (!pMainDoc->m_bFinishLoader1)	// 200313 HKH Memory Leak 수정
	{
		if (pMainDoc->m_ctrlLoader1.ReqPortOpen() == TRUE) 
		{
			Sleep(3000);
			pMainDoc->m_ctrlLoader1.SetSYSTem();	
			break;
		}
		Sleep(1000);
	}

	while (!pMainDoc->m_bFinishLoader1)	// 200313 HKH Memory Leak 수정
	{
		Sleep(200);

		if (pMainDoc->m_ctrlLoader1.ReqPortOpen() == FALSE) 
		{
			nRetryCount++;
			if (nRetryCount > 5)
			{
				pMainDoc->m_ctrlLoader1.InitSerialPortUseOven(1);	//재접속
				nRetryCount =0;
			}
			Sleep(2000);
			
			continue;
		}
		if (pMainDoc->m_nSendingModeComm_ch1 != ID_LOAD_NONE) 
		{
			switch (pMainDoc->m_nSendingModeComm_ch1)
			{
			case ID_LOAD_ON:
				pMainDoc->m_ctrlLoader1.SetLoadOnOff(TRUE);	//Load ON
				break;
			case ID_LOAD_OFF:
				pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
				break;
			case ID_LOAD_CP:
				pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CP);
				break;
			case ID_LOAD_CC:
				pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CC);
				break;
			case ID_LOAD_CV:
				pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CV);
				break;
			case ID_LOAD_CR:
				pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CR);
				break;
			}
			Sleep(100);

			pMainDoc->m_nSendingModeComm_ch1 = ID_LOAD_NONE;

			strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
			Sleep(10);
			strLoadMode = pMainDoc->m_ctrlLoader1.ReqMode();
			Sleep(10);
			continue;
		}
		fLoadV = pMainDoc->m_sLoaderData1.fVoltage = pMainDoc->m_ctrlLoader1.ReqMeasureVoltage();
		Sleep(10);
		fLoadA = pMainDoc->m_sLoaderData1.fCurrent = pMainDoc->m_ctrlLoader1.ReqMeasureCurrent();
		Sleep(10);
		TRACE("Loadr volt = %.3f, curr = %.3f, resis = %.3f, power = %.3f \n"
			, pMainDoc->m_sLoaderData1.fVoltage, pMainDoc->m_sLoaderData1.fCurrent
			, pMainDoc->m_sLoaderData1.fSetResis, pMainDoc->m_sLoaderData1.fSetPower);
		::SFTSetLoaderData(mdIndex,0,&pMainDoc->m_sLoaderData1);
		
		if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "USE LINK LOADER 1", FALSE) == FALSE)	//LOAD 연동 인지 확인
		{
			continue;
		}

		lpModule = pMainDoc->GetCyclerMD(nModuleID);
		if(lpModule == NULL)	continue;
		if(lpModule->GetState() == PS_STATE_LINE_OFF )	
		{
			strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
			Sleep(10);
			if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
			Sleep(10);
			strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
			continue;//ljb 20141231 add
		}
		
		//nTotCh = lpModule->GetTotalChannel();
		//채널 정보를 구한다.
		lpChannelInfo = pMainDoc->GetChannelInfo(nModuleID, nChNo);
		if(lpChannelInfo != NULL)
		{
			WORD nMinStepNo;
			//Idle인 상태
			//TRACE("Loader check channel M%02dC%02d state %d \n",lpChannelInfo->GetModuleID(), lpChannelInfo->GetChannelIndex(),lpChannelInfo->GetState());
			if( lpChannelInfo->GetState() == PS_STATE_IDLE || lpChannelInfo->GetState() == PS_STATE_STANDBY ||
				lpChannelInfo->GetState() == PS_STATE_END	|| lpChannelInfo->GetState() == PS_STATE_READY )	
			{								
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				continue;
			}
			else if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)   
			{
				//ljb 20120627 병렬모드 일때도 LOAD OFF
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(500);
				continue;
			}
			else if (lpChannelInfo->GetState() == PS_STATE_MAINTENANCE || lpChannelInfo->GetState() == PS_STATE_PAUSE || lpChannelInfo->GetState() == PS_STATE_PAUSE_CHAMBER_READY)
			{
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
				Sleep(500);
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				continue;
			}
			else
			{		
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();	//ON, OFF
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
				Sleep(10);
				strLoadMode = pMainDoc->m_ctrlLoader1.ReqMode();		//CC,CV,CP,CR
				Sleep(10);

				//현재 스텝 상태와 스케쥴 상태 비교 해서 명령어 전송 한다
				if (lpChannelInfo->GetStepType() == PS_STEP_CHARGE || lpChannelInfo->GetStepType() == PS_STEP_DISCHARGE
					 || lpChannelInfo->GetStepType() == PS_STEP_PATTERN || lpChannelInfo->GetStepType() == PS_STEP_REST
					  || lpChannelInfo->GetStepType() == PS_STEP_IMPEDANCE)
				{
					nMinStepNo = lpChannelInfo->GetStepNo();	//현재 STEP 번호

					CScheduleData *lpSchedule = lpChannelInfo->GetScheduleData();
					if(lpSchedule)
					{							
						CStep *lpStep = lpSchedule->GetStepData(nMinStepNo -1);		//ljb 20140108 add 현재스텝 가져오기
						if (lpStep == NULL) continue;								//ljb 20150612 add
						if (lpStep->nValueLoaderItem == 0) continue;
						if (lpStep->nValueLoaderItem == 2) 
						{
							if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
							Sleep(10);
							if (fLoadV == 0 && fLoadA == 0)  pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF					

							//TRACE("Step Time = %d sec \n", lpChannelInfo->GetStepTime()/100);
							if (((lpChannelInfo->GetStepTime()/100) > 15) && (fLoadV == 0 && fLoadA == 0))
							{
								nPauseCount++;
								//채널 Pause 전송 후 메시지 박스 POP UP (
								if (nPauseCount > 3) 
								{
									nPauseCount=0;
									pMainDoc->SendCommand(nModuleID, &chArray, SFT_CMD_PAUSE);
									lpChannelInfo->WriteLog("Loader 1 에서 응답이 없어서 Pause 명령 전송."); //yulee 20190405 check
								}
							}
							else
							{
								nPauseCount=0;
							}
							continue;
						}

						// int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
						// int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
						// float	fValueLoaderSet;		//설정값 W,A,V,Ohm
						if (lpStep->nValueLoaderItem == 1)
						{
							if (((lpChannelInfo->GetStepTime()/100) > 15) && (fLoadV == 0 && fLoadA == 0))
							{
								nPauseCount++;
								//채널 Pause 전송 후 메시지 박스 POP UP (
								if (nPauseCount > 3) 
								{
									nPauseCount=0;
									pMainDoc->SendCommand(nModuleID, &chArray, SFT_CMD_PAUSE);
									lpChannelInfo->WriteLog("Loader 1 에서 응답이 없어서 Pause 명령 전송."); //yulee 20190405 check
								}
							}
							else
							{
								nPauseCount=0;
							}

							if (lpStep->nValueLoaderMode == 0)
							{
								if (strLoadMode != "CP" )
								{
									pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CP);
									Sleep(100);
									pMainDoc->m_ctrlLoader1.SetPowerValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader1.SetPowerValue(lpStep->fValueLoaderSet);
							}
							else if (lpStep->nValueLoaderMode == 1)
							{
								if (strLoadMode != "CC" )
								{
									pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CC);
									Sleep(100);
									pMainDoc->m_ctrlLoader1.SetCurrentValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader1.SetCurrentValue(lpStep->fValueLoaderSet);
							}
							else if (lpStep->nValueLoaderMode == 2)
							{
								if (strLoadMode != "CV" )
								{
									pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CV);
									Sleep(100);
									pMainDoc->m_ctrlLoader1.SetVoltageValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader1.SetVoltageValue(lpStep->fValueLoaderSet);
							}
							else if (lpStep->nValueLoaderMode == 3)
							{
								if (strLoadMode != "CR" )
								{
									pMainDoc->m_ctrlLoader1.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader1.SetMode(SL_MODE_CR);
									Sleep(100);
									pMainDoc->m_ctrlLoader1.SetResistanceValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader1.SetResistanceValue(lpStep->fValueLoaderSet);
							}
						}

						if (lpStep->nValueLoaderItem == 1 && strLoadOnOff == "OFF")
						{
							pMainDoc->m_ctrlLoader1.SetLoadOnOff(TRUE);//Load ON
							Sleep(500);
							strLoadOnOff = pMainDoc->m_ctrlLoader1.ReqLoadStat();
							Sleep(10);
						}
					}
				}
			}
		} //end if -> if(lpSchedule)
		else
		{
			//Schedule == NULL
// 			strTemp.Format("챔버%d 연동 lpSchedule Error  !!!", nOvenID+1);
// 			WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
		}
	}

	pMainDoc->m_bFinishLoader1Ack = true;	// 200313 HKH Memory Leak 수정

	return 0;
}

UINT CCTSMonProDoc::ChkLoaderState_2(LPVOID pParam)
{
	CCTSMonProDoc *pMainDoc = (CCTSMonProDoc*)pParam;
	CString strMsg, strLoadOnOff, strLoadMode;
	CCyclerModule	*lpModule;
	CCyclerChannel	*lpChannelInfo;
	
	strLoadOnOff = strLoadMode = "";
	lpModule= NULL;		
	lpChannelInfo= NULL;		//Channel 
	
	int mdIndex =1;
	int nRtn =0;
	int nTotCh =0;
	CStep *pStep = NULL;
// 	CStep *pStep2 = NULL;
	
	ZeroMemory(&pMainDoc->m_sLoaderData2,sizeof(SFT_LOADER_VALUE));
	::SFTSetLoaderData(mdIndex,1,&pMainDoc->m_sLoaderData2);			//channel 2에 전송
	if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Load 2", FALSE) == FALSE)
	{
		pMainDoc->m_LoaderThread2 = NULL; //ksj 20170302 : 쓰레드 포인터 초기화 추가.
		return 0;
	}

	//TRACE(" ====================>>>>>>>>    Loader Thread start ChkLoaderState_2 \n");
	int nModuleID = 1;
	int nChNo = 1;		//strart index = 0,   2번째 채널 지정
	
	int nRetryCount = 0;
	int nPauseCount = 0;
	float fLoadV =0.0f, fLoadA =0.0f;
	CWordArray chArray;
	chArray.Add(nChNo);
	
	while (!pMainDoc->m_bFinishLoader2)		// 200313 HKH Memory Leak 수정
	{
		if (pMainDoc->m_ctrlLoader2.ReqPortOpen() == TRUE) 
		{
			Sleep(3000);
			pMainDoc->m_ctrlLoader2.SetSYSTem();	
			break;
		}
		nRetryCount++;
		if (nRetryCount > 5)
		{
			pMainDoc->m_ctrlLoader2.InitSerialPortUseOven(2);	//재접속
			nRetryCount =0;
		}
		Sleep(2000);
		continue;
	}

	while (!pMainDoc->m_bFinishLoader2)	// 200313 HKH Memory Leak 수정
	{
		Sleep(200);

		if (pMainDoc->m_ctrlLoader2.ReqPortOpen() == FALSE) 
		{
			nRetryCount++;
			if (nRetryCount > 5)
			{
				pMainDoc->m_ctrlLoader2.InitSerialPortUseOven(2);	//재접속
				nRetryCount =0;
			}
			Sleep(2000);
			continue;
		}
		if (pMainDoc->m_nSendingModeComm_ch2 != ID_LOAD_NONE) 
		{
			switch (pMainDoc->m_nSendingModeComm_ch2)
			{
			case ID_LOAD_ON:
				pMainDoc->m_ctrlLoader2.SetLoadOnOff(TRUE);	//Load ON
				break;
			case ID_LOAD_OFF:
				pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
				break;
			case ID_LOAD_CP:
				pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CP);
				break;
			case ID_LOAD_CC:
				pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CC);
				break;
			case ID_LOAD_CV:
				pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CV);
				break;
			case ID_LOAD_CR:
				pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CR);
				break;
			}
			Sleep(100);

			pMainDoc->m_nSendingModeComm_ch2 = ID_LOAD_NONE;

			strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
			Sleep(10);
			strLoadMode = pMainDoc->m_ctrlLoader2.ReqMode();
			Sleep(10);
			continue;
		}
		fLoadV = pMainDoc->m_sLoaderData2.fVoltage = pMainDoc->m_ctrlLoader2.ReqMeasureVoltage();
		Sleep(10);
		fLoadA = pMainDoc->m_sLoaderData2.fCurrent = pMainDoc->m_ctrlLoader2.ReqMeasureCurrent();
		Sleep(10);
//  		pMainDoc->m_sLoaderData2.fSetResis = pMainDoc->m_ctrlLoader2.ReqMeasureResistance();
//  		Sleep(10);
//   		pMainDoc->m_sLoaderData2.fSetPower = pMainDoc->m_ctrlLoader2.ReqMeasurePower();
//  		Sleep(10);
		TRACE("Loadr volt = %.3f, curr = %.3f, resis = %.3f, power = %.3f \n"
			, pMainDoc->m_sLoaderData2.fVoltage, pMainDoc->m_sLoaderData2.fCurrent
			, pMainDoc->m_sLoaderData2.fSetResis, pMainDoc->m_sLoaderData2.fSetPower);
		::SFTSetLoaderData(mdIndex,1,&pMainDoc->m_sLoaderData2);	//2번 channel 에 데이터 전송

		if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "USE LINK LOADER 2", FALSE) == FALSE)	//LOAD 연동 인지 확인
		{
			continue;
		}

		lpModule = pMainDoc->GetCyclerMD(nModuleID);
		if(lpModule == NULL)	continue;
		if(lpModule->GetState() == PS_STATE_LINE_OFF )
		{
			strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
			Sleep(10);
			strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
			Sleep(10);
			if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
			Sleep(10);
			strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
			continue;//ljb 20141231 add
		}
		
		//nTotCh = lpModule->GetTotalChannel();
		//채널 정보를 구한다.
		lpChannelInfo = pMainDoc->GetChannelInfo(nModuleID, nChNo);
		if(lpChannelInfo != NULL)
		{
			WORD nMinStepNo;
			//Idle인 상태
			//TRACE("Loader check channel M%02dC%02d state %d \n",lpChannelInfo->GetModuleID(), lpChannelInfo->GetChannelIndex(),lpChannelInfo->GetState());
			if( lpChannelInfo->GetState() == PS_STATE_IDLE || lpChannelInfo->GetState() == PS_STATE_STANDBY ||
				lpChannelInfo->GetState() == PS_STATE_END	|| lpChannelInfo->GetState() == PS_STATE_READY )	
			{								
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				Sleep(10);
				if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				continue;
			}
			else if(lpChannelInfo->IsParallel() && lpChannelInfo->m_bMaster == FALSE)   
			{
				//ljb 20120627 병렬모드 일때도 LOAD OFF
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				Sleep(10);
				if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
				Sleep(500);
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				continue;
			}
			else if (lpChannelInfo->GetState() == PS_STATE_MAINTENANCE || lpChannelInfo->GetState() == PS_STATE_PAUSE || lpChannelInfo->GetState() == PS_STATE_PAUSE_CHAMBER_READY)
			{
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				Sleep(10);
				if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
				Sleep(500);
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				continue;
			}
			else
			{		
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();	//ON, OFF
				Sleep(10);
				strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
				Sleep(10);
				strLoadMode = pMainDoc->m_ctrlLoader2.ReqMode();		//CC,CV,CP,CR
				Sleep(10);

				//현재 스텝 상태와 스케쥴 상태 비교 해서 명령어 전송 한다
				if (lpChannelInfo->GetStepType() == PS_STEP_CHARGE || lpChannelInfo->GetStepType() == PS_STEP_DISCHARGE
					 || lpChannelInfo->GetStepType() == PS_STEP_PATTERN || lpChannelInfo->GetStepType() == PS_STEP_REST
					  || lpChannelInfo->GetStepType() == PS_STEP_IMPEDANCE)
				{
					nMinStepNo = lpChannelInfo->GetStepNo();	//현재 STEP 번호

					CScheduleData *lpSchedule = lpChannelInfo->GetScheduleData();
					if(lpSchedule)
					{							
						CStep *lpStep = lpSchedule->GetStepData(nMinStepNo -1);		//ljb 20140108 add 현재스텝 가져오기
						if (lpStep == NULL) continue;								//ljb 20150612 add

						if (lpStep->nValueLoaderItem == 0) continue;
						if (lpStep->nValueLoaderItem == 2) 
						{
							if (strLoadOnOff == "ON") pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
							Sleep(10);
							if (fLoadV == 0 && fLoadA == 0)  pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF					
							
							//TRACE("Step Time = %d sec \n", lpChannelInfo->GetStepTime()/100);
							if (((lpChannelInfo->GetStepTime()/100) > 15) && (fLoadV == 0 && fLoadA == 0))
							{
								nPauseCount++;
								//채널 Pause 전송 후 메시지 박스 POP UP (
								if (nPauseCount > 3) 
								{
									nPauseCount=0;
									pMainDoc->SendCommand(nModuleID, &chArray, SFT_CMD_PAUSE);
									lpChannelInfo->WriteLog("Loader 2 에서 응답이 없어서 Pause 명령 전송.");//yulee 20190405 check
								}
							}
							else
							{
								nPauseCount=0;
							}
							continue;
						}

						// int		nValueLoaderItem;		//0: 사용안함 , 1: ON,	 2: OFF
						// int		nValueLoaderMode;		//0: CP, 1: CC, 2: CV, 3: CR
						// float	fValueLoaderSet;		//설정값 W,A,V,Ohm
						if (lpStep->nValueLoaderItem == 1)
						{
							if (((lpChannelInfo->GetStepTime()/100) > 15) && (fLoadV == 0 && fLoadA == 0))
							{
								nPauseCount++;
								//채널 Pause 전송 후 메시지 박스 POP UP (
								if (nPauseCount > 3) 
								{
									nPauseCount=0;
									pMainDoc->SendCommand(nModuleID, &chArray, SFT_CMD_PAUSE);
									lpChannelInfo->WriteLog("Loader 2 에서 응답이 없어서 Pause 명령 전송.");//yulee 20190405 check
								}
							}
							else
							{
								nPauseCount=0;
							}

							if (lpStep->nValueLoaderMode == 0)
							{
								if (strLoadMode != "CP" )
								{
									pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CP);
									Sleep(100);
									pMainDoc->m_ctrlLoader2.SetPowerValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader2.SetPowerValue(lpStep->fValueLoaderSet);
							}
							else if (lpStep->nValueLoaderMode == 1)
							{
								if (strLoadMode != "CC" )
								{
									pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CC);
									Sleep(100);
									pMainDoc->m_ctrlLoader2.SetCurrentValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader2.SetCurrentValue(lpStep->fValueLoaderSet);
							}
							else if (lpStep->nValueLoaderMode == 2)
							{
								if (strLoadMode != "CV" )
								{
									pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CV);
									Sleep(100);
									pMainDoc->m_ctrlLoader2.SetVoltageValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader2.SetVoltageValue(lpStep->fValueLoaderSet);
							}
							else if (lpStep->nValueLoaderMode == 3)
							{
								if (strLoadMode != "CR" )
								{
									pMainDoc->m_ctrlLoader2.SetLoadOnOff(FALSE);//Load OFF
									Sleep(500);
									pMainDoc->m_ctrlLoader2.SetMode(SL_MODE_CR);
									Sleep(100);
									pMainDoc->m_ctrlLoader2.SetResistanceValue(lpStep->fValueLoaderSet);
									Sleep(100);
								}
								else
									pMainDoc->m_ctrlLoader2.SetResistanceValue(lpStep->fValueLoaderSet);
							}
						}

						if (lpStep->nValueLoaderItem == 1 && strLoadOnOff == "OFF")
						{
							pMainDoc->m_ctrlLoader2.SetLoadOnOff(TRUE);//Load ON
							Sleep(500);
							strLoadOnOff = pMainDoc->m_ctrlLoader2.ReqLoadStat();
						}
					}
				}
			}
		} //end if -> if(lpSchedule)
		else
		{
			//Schedule == NULL
// 			strTemp.Format("챔버%d 연동 lpSchedule Error  !!!", nOvenID+1);
// 			WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
		}
	}	
	
	pMainDoc->m_bFinishLoader2Ack = true;	// 200313 HKH Memory Leak 수정
}

UINT CCTSMonProDoc::ChkOvenState_1(LPVOID pParam)
{
	CCTSMonProDoc *pMainDoc = (CCTSMonProDoc*)pParam;
	CCyclerModule	*lpModule;
	CCyclerChannel	*lpChannelInfo;
	CString strMsg, strTemp;
	//TRACE(" ====================>>>>>>>>    Oven Thread 1 start ChkOvenState_1 \n");

	static int nDevice = 0;
	int nOvenID;
	long lOvenState;
	int m_uLogCnt=1;

	strTemp.Format("챔버 1 쓰레드 시작. (Oven Model %d)",pMainDoc->m_ctrlOven1.GetOvenModelType());
	pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_SYSTEM);

	while(pMainDoc->m_ctrlOven1.m_bUseOvenCtrl)
	{
		//line 연결 상태 확인을 위해 처음에 FALSE 셋팅
		//pMainDoc->m_ctrlOven1.SetLineState(nOvenID, FALSE);
		
		while(pMainDoc->m_ctrlOven1.m_bPortOpen)
		{
			for (nOvenID=0; nOvenID < pMainDoc->m_ctrlOven1.GetOvenCount() ; nOvenID++)
			{
				//Oven 사용 여부
				if (pMainDoc->m_ctrlOven1.GetUseOven(nOvenID) == FALSE) continue;
				
				//Oven 상태 체크
				lOvenState = pMainDoc->m_ctrlOven1.GetOvenStatus(nOvenID);
				if (lOvenState != 0)
				{
					//전채널 일시 정지 => Warring Message 표시
					//ljb 임시	Fun_AlarmOvenState(nOvenID, lOvenState);
					//return;
				}
				
				if(pMainDoc->m_ctrlOven1.GetDisconnectCnt(nOvenID) < 3) 
				{
					//현재 Oven 통신 가능
					//현재 Oven의 상태값을 Update한다.	
					if (pMainDoc->m_ctrlOven1.RequestCurrentValue(nDevice,nOvenID ) == 0)	//lmh 20111115 2단챔버 연동시 디바이스와 오븐아이디가 바뀌어야함
					{
						pMainDoc->m_ctrlOven1.IncreaseDisconnectTime(nOvenID);	//통신 두절 count 증가
						
						m_uLogCnt++;
						if((m_uLogCnt % 90) == 0)	// log 남기기
						{
							strTemp.Format("챔버 1 에서 응답이 없습니다.(Oven Model %d, Oven ID %d)",pMainDoc->m_ctrlOven1.GetOvenModelType(),nOvenID+1);
							pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_SYSTEM);
							m_uLogCnt = 0;
						}
					}
				}
				else
				{
					//현재 Oven 통신 불능
					//10초 이상이면  전송하지 않음
					pMainDoc->m_ctrlOven1.SetLineState(nOvenID, FALSE);
					pMainDoc->m_ctrlOven1.ClearCurrentData();
					pMainDoc->m_ctrlOven1.ResetDisconnectCnt(nOvenID);					
				}
				
				//Start Dlg가 떠있는동안은 Reset 시킴 지연을 위해 반듯이 필요
// 				if(m_bBlockFlag)
// 				{
// 					pDoc->m_nRunCmdDelayTime = 0;
// 				}
// 				else
// 				{
// 					pDoc->m_nRunCmdDelayTime += (CHAMBER_TIMER/1000);		//2sec timer
// 				}
				
				//Oven 연동 채널이 모두 Oven연동 Pause 상태 일때 -> 챔버 온도 변경 및 Oven 연동 Continue 전송			
				if(pMainDoc->m_ctrlOven1.GetLineState(nOvenID)) 	pMainDoc->CheckOven_Line1(nOvenID);

			}// end for (nOvenID=0; nOvenID < pMainDoc->m_ctrlOven1.GetOvenCount() ; nOvenID++)
			Sleep(2000);

		}// end while(pMainDoc->m_ctrlOven1.m_bPortOpen)
		
		Sleep(1000);
	}
	return 0;
}

UINT CCTSMonProDoc::ChkOvenState_2(LPVOID pParam)
{
	CCTSMonProDoc *pMainDoc = (CCTSMonProDoc*)pParam;
	CCyclerModule	*lpModule;
	CCyclerChannel	*lpChannelInfo;
	CString strMsg, strTemp;
	
	//TRACE(" ====================>>>>>>>>    Oven Thread 2 start ChkOvenState_1 \n");
	
	static int nDevice = 0;
	int nOvenID;
	long lOvenState;
	int m_uLogCnt=1;
	
	
	strTemp.Format("챔버 2 쓰레드 시작. (Oven Model %d)",pMainDoc->m_ctrlOven2.GetOvenModelType());
	pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_SYSTEM);
	
	while(pMainDoc->m_ctrlOven2.m_bUseOvenCtrl)
	{
		//line 연결 상태 확인을 위해 처음에 FALSE 셋팅
		//pMainDoc->m_ctrlOven2.SetLineState(nOvenID, FALSE);
		
		while(pMainDoc->m_ctrlOven2.m_bPortOpen)
		{
			for (nOvenID=0; nOvenID < pMainDoc->m_ctrlOven2.GetOvenCount() ; nOvenID++)
			{
				//Oven 사용 여부
				if (pMainDoc->m_ctrlOven2.GetUseOven(nOvenID) == FALSE) continue;
				
				//Oven 상태 체크
				lOvenState = pMainDoc->m_ctrlOven2.GetOvenStatus(nOvenID);
				if (lOvenState != 0)
				{
					//전채널 일시 정지 => Warring Message 표시
					//ljb 임시	Fun_AlarmOvenState(nOvenID, lOvenState);
					//			return;
				}
				
				if(pMainDoc->m_ctrlOven2.GetDisconnectCnt(nOvenID) < 3) 
				{
					//현재 Oven 통신 가능
					//현재 Oven의 상태값을 Update한다.	
					if (pMainDoc->m_ctrlOven2.RequestCurrentValue(nDevice,nOvenID ) == 0)	//lmh 20111115 2단챔버 연동시 디바이스와 오븐아이디가 바뀌어야함
					{
						pMainDoc->m_ctrlOven2.IncreaseDisconnectTime(nOvenID);	//통신 두절 count 증가
						
						m_uLogCnt++;
						if((m_uLogCnt % 90) == 0)	// log 남기기
						{
							strTemp.Format("챔버 2 에서 응답이 없습니다.(Oven Model %d, Oven ID %d)",pMainDoc->m_ctrlOven2.GetOvenModelType(),nOvenID+1);
							pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_SYSTEM);
							m_uLogCnt = 0;
						}
					}
				}
				else
				{
					//현재 Oven 통신 불능
					//10초 이상이면  전송하지 않음
					pMainDoc->m_ctrlOven2.SetLineState(nOvenID, FALSE);
					pMainDoc->m_ctrlOven2.ClearCurrentData();
					pMainDoc->m_ctrlOven2.ResetDisconnectCnt(nOvenID);					
				}
				
				//Start Dlg가 떠있는동안은 Reset 시킴 지연을 위해 반듯이 필요
				// 				if(m_bBlockFlag)
				// 				{
				// 					pDoc->m_nRunCmdDelayTime = 0;
				// 				}
				// 				else
				// 				{
				// 					pDoc->m_nRunCmdDelayTime += (CHAMBER_TIMER/1000);		//2sec timer
				// 				}
				
				
				//Oven 연동 채널이 모두 Oven연동 Pause 상태 일때 -> 챔버 온도 변경 및 Oven 연동 Continue 전송			
				if(pMainDoc->m_ctrlOven2.GetLineState(nOvenID)) 	pMainDoc->CheckOven_Line2(nOvenID);
				
			}// end for (nOvenID=0; nOvenID < pMainDoc->m_ctrlOven2.GetOvenCount() ; nOvenID++)
			Sleep(2000);
			
		}// end while(pMainDoc->m_ctrlOven2.m_bPortOpen)
		
		Sleep(1000);
	}
	return 0;
}


//BOOL CCTSMonProDoc::SendCommand(UINT nModuleID, CDWordArray *apSelChArray, UINT nCmdID, int nCycleNo, int nStepNo)
BOOL CCTSMonProDoc::SendCommand(UINT nModuleID, CWordArray *apSelChArray, UINT nCmdID, int nCycleNo, int nStepNo)
{
	if(apSelChArray == NULL )	return FALSE;

	CString	strMsg;
	CCyclerModule *pMD;
	int nRtn = 0;

	pMD = GetModuleInfo(nModuleID);

	if(pMD)		//최종 전송 이전에 채널의 상태가 바뀌었을 수도 있으므로 최종 채널 상태 검사 한 후 전송한다.
	{
		if((nCmdID == SFT_CMD_PAUSE || nCmdID == SFT_CMD_STOP || nCmdID == SFT_CMD_RUN) && pMD->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV3)
		{
			SFT_CONTROL_INFO *pConinfo = new SFT_CONTROL_INFO;
			ZeroMemory(pConinfo, sizeof(SFT_CONTROL_INFO));
			pConinfo->lCycleNo = nCycleNo;
			pConinfo->lStepNo = nStepNo;
			nRtn = pMD->SendCommand(nCmdID, apSelChArray, pConinfo, sizeof(SFT_CONTROL_INFO));
			delete pConinfo;		
			//최종 Command 예약상태 Log 기록 기록 
		}
		//2014.12.03 CAN 통신 모드 체크
		else if(nCmdID == SFT_CMD_CAN_MODE_CHECK)
		{
			//★★ ljb 2010323  ★★★★★★★★ 
			SFT_USING_CHAMBER *pChamberBody = new SFT_USING_CHAMBER;
			ZeroMemory(pChamberBody, sizeof(SFT_USING_CHAMBER));
			// 0 : 해제 , 1 : M + S, 2 : Master, 3: Slave
			pChamberBody->bfulseMode = nCycleNo;
			// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지 ============================
			pChamberBody->bChamberStand = nStepNo;
			// 원래 S_P1_RCV_CMD_CAN_COMM_CHECK 구조체를 사용해야하나 코드상 동일한 사이즈의 SFT_USING_CHAMBER 구조체를 사용하고있음
			// 변경하여 할당한 구조체 변수는 다음과 같음
			// bfulseMode --> can_comm_check
			// bChamberStand --> out_cable_check
			// =============================================================================
			
			//if (nCycleNo == 1){
			//	pChamberBody->bfulseMode = 1;	
			//}else{
			//	pChamberBody->bfulseMode = 0;
			//}
			
			//2014.12.18 100D 설치된 사이트 마다 별도로 설정해야됨. 
			bool IsCanModeCheck =  AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC,"UseCanModeCheck",0);
			if(IsCanModeCheck){
				nRtn = pMD->SendCommand(nCmdID, apSelChArray, pChamberBody, sizeof(SFT_USING_CHAMBER));
				TRACE("nCmdID : %d\n", nCmdID);
			}
			
			delete pChamberBody;
		}
		//2015.11.11 MUX LINK 모드
		else if(nCmdID == SFT_CMD_MUX_MODE_CHECK)
		{
			//★★ ljb 2010323  ★★★★★★★★ 
			SFT_USING_CHAMBER *pChamberBody = new SFT_USING_CHAMBER;
			ZeroMemory(pChamberBody, sizeof(SFT_USING_CHAMBER));
			// 0 : 해제 , 1 : 설정
			if (nCycleNo == 1){
				pChamberBody->bfulseMode = 1;	
			}else{
				pChamberBody->bfulseMode = 0;
			}

			
			//2015.11.11 100D 설치된 사이트 마다 별도로 설정해야됨. 
			bool IsMuxModeCheck =  AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC,"UseMuxLink",0);
			if(IsMuxModeCheck){
				nRtn = pMD->SendCommand(nCmdID, apSelChArray, pChamberBody, sizeof(SFT_USING_CHAMBER));
				TRACE("nCmdID : %d\n", nCmdID);
			}
			
			delete pChamberBody;
		}
		else if(nCmdID == SFT_CMD_CHAMBER_USING)
		{
			//★★ ljb 2010323  ★★★★★★★★ 
			SFT_USING_CHAMBER *pChamberBody = new SFT_USING_CHAMBER;
			ZeroMemory(pChamberBody, sizeof(SFT_USING_CHAMBER));
			if (nCycleNo == 1) pChamberBody->bfulseMode = 1;	// 0 : 해제 , 1 : 설정

			BOOL bUseChiller = FALSE;
			bUseChiller = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 0);
			//yulee 20190114 칠러연동
			pChamberBody->bChillerControl = bUseChiller;
			
			//2014.12.23 챔버 대기 모드 값 추가.
			pChamberBody->bChamberStand = m_bChamberStand;			

			nRtn = pMD->SendCommand(nCmdID, apSelChArray, pChamberBody, sizeof(SFT_USING_CHAMBER));
			TRACE("nCmdID : %d\n", nCmdID);
			delete pChamberBody;
		}
		else if (nCmdID == SFT_CMD_VENT_CLEAR)
		{
			SEND_CMD_VENT_CLEAR *pstVentClear = new SEND_CMD_VENT_CLEAR;
			ZeroMemory(pstVentClear, sizeof(SEND_CMD_VENT_CLEAR));
			pstVentClear->bCloseOpenFlag = (BYTE)nCycleNo;
			nRtn = pMD->SendCommand(nCmdID, apSelChArray, pstVentClear, sizeof(SEND_CMD_VENT_CLEAR));
			TRACE("nCmdID : %d\n", nCmdID);
			delete pstVentClear;
		}
		else if (nCmdID == SFT_CMD_DAQ_ISOLATION_COMM_REQUEST)
		{
			SFT_DAQ_ISOLATION *pstDaqIsolation = new SFT_DAQ_ISOLATION;
			ZeroMemory(pstDaqIsolation, sizeof(SFT_DAQ_ISOLATION));
			pstDaqIsolation->bISO = 1;
			pstDaqIsolation->div_ch = 0;
			pstDaqIsolation->div_MainCh = 0;

			nRtn = pMD->SendCommand(nCmdID, apSelChArray, pstDaqIsolation, sizeof(SFT_DAQ_ISOLATION));
			TRACE("nCmdID : %d\n", nCmdID);
		}
		else if (nCmdID == SFT_CMD_USER_EMG) //ksj 20201222
		{
			SFT_USER_EMG *pstUserEmg = new SFT_USER_EMG;
			ZeroMemory(pstUserEmg, sizeof(SFT_USER_EMG));
			pstUserEmg->command = (BYTE)nCycleNo;
			nRtn = pMD->SendCommand(nCmdID, apSelChArray, pstUserEmg, sizeof(SFT_USER_EMG));
			TRACE("nCmdID : %d\n", nCmdID);
		}
		else
		{
			nRtn = pMD->SendCommand(nCmdID, apSelChArray);
		}

		if(nRtn != SFT_ACK)
		{//$1013
			//strMsg.Format("%s에 [%s]명령 전송 실패!!!(%d)", GetModuleName(nModuleID), GetCmdString(nCmdID), nRtn);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendCommand_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID), GetCmdString(nCmdID), nRtn);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		else
		{//$1013
			//strMsg.Format("%s에 [%s]명령 전송 성공", GetModuleName(nModuleID), GetCmdString(nCmdID));
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendCommand_msg2","CTSMonPro_DOC"), GetModuleName(nModuleID), GetCmdString(nCmdID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
			
			for(int ch =0; ch < apSelChArray->GetSize(); ch++)
			{
				CCyclerChannel *pChInfo;
				pChInfo = GetChannelInfo(nModuleID, apSelChArray->GetAt(ch));
				
				if(pChInfo)
				{
					if(nCycleNo != 0 || nStepNo != 0)
					{//$1013
						//strMsg.Format(	"%s 실시(현재 Cycle:%d, Step:%d / 예약 Cycle:%d, Step:%d)", 
						strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendCommand_msg3","CTSMonPro_DOC"), //&&
										GetCmdString(nCmdID),
										pChInfo->GetCurCycleCount(),
										pChInfo->GetStepNo(),
										nCycleNo,
										nStepNo
									 );
						pChInfo->SetReservedStep(nCycleNo, nStepNo);
						pChInfo->SaveRunInfoToTempFile();				//예약 명령상태를 파일로 저장한다.
					}
					else
					{//$1013
						//strMsg.Format("%s 실시", GetCmdString(nCmdID));
						strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendCommand_msg4","CTSMonPro_DOC"), GetCmdString(nCmdID));//&&
					}
					pChInfo->WriteLog(strMsg);

					//lyj 20201028 s =====================
					if(nCmdID == SFT_CMD_STOP)
					{
						pChInfo->SetUserComplete(TRUE);
					}
					else
					{
						pChInfo->SetUserComplete(FALSE);
					}
					//lyj 20201028 e =====================
					pChInfo->CycSendCommand(nCmdID);
				}
				else
				{//$1013
					//strMsg.Format("%s 실시(chinfo(%d) : NULL", GetCmdString(nCmdID),apSelChArray->GetAt(ch));
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendCommand_msg5","CTSMonPro_DOC"), GetCmdString(nCmdID),apSelChArray->GetAt(ch));//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				}
			}
		}

		//최종 상태를 파일로 저장 (재 시작시 Loading한다.)
	}
	else
	{
		//strMsg.Format("%s 정보 요청 실패!!", GetModuleName(nModuleID));//$1013
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendCommand_msg6","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
		WriteLog(strMsg);
		return FALSE;
	}

	return TRUE;
}

void CCTSMonProDoc::OnCloseDocument() 
{
	SetAppExitProgress(20); //ksj 20201023: 종료 진행상황 20%

	//현재 다른이 작업중이면 buffer내용을 모두 저장한다.
	CCyclerModule *pMD; 
	CCyclerChannel *pCh;

	//전체 모듈에 대해 검색
	for(int mdIndex =0; mdIndex < GetInstallModuleCount(); mdIndex++)
	{
		pMD = GetModuleInfo(GetModuleID(mdIndex));
		if(pMD)
		{
			//Line Off이면 검색 안함 
			if(pMD->GetState() == PS_STATE_LINE_OFF)	continue;

			//모든 채널에 대해 검색
			for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
			{
				pCh = pMD->GetChannelInfo(ch);
				if(pCh)
				{
					if(pCh->GetState() == PS_STATE_RUN || pCh->GetState() == PS_STATE_PAUSE)
					{
						//pCh->CheckAndSaveDataB();  //kjh //2007/08/09
						//pCh->CheckAndSaveDataC();	//kjh //2007/10/23
						CompletedTest(pMD->GetModuleID(), ch);	
						//pCh->WriteLog("서버 프로그램을 닫음");//$1013
						pCh->WriteLog(Fun_FindMsg("CTSMonProDoc_OnCloseDocument_msg1","CTSMonPro_DOC"));//&&
					}
				}
			}
		}
	}

	SetAppExitProgress(30); //ksj 20201023: 종료 진행상황 30%	

	//if(m_FileSaveThread) m_FileSaveThread->SuspendThread();

	FinishDocThread();	// 200313 HKH Memory Leak 수정
	

	SetAppExitProgress(50); //ksj 20201023: 종료 진행상황 60%

	//순서 변경하면 안됨
	//1. 교정 Dialog가 있으면 삭제 
	if( m_pCalibratorDlg )
	{
		delete m_pCalibratorDlg;
		m_pCalibratorDlg = NULL;
	}
	
	if( m_pSafetyUpdateDlg )
	{
		delete m_pSafetyUpdateDlg;
		m_pSafetyUpdateDlg = NULL;
	}

	if( m_pSchUpdateDlg )
	{
		delete m_pSchUpdateDlg;
		m_pSchUpdateDlg = NULL;
	}

	//ksj 20201023 : [#JSonMon] 모니터링 JSON 파일 생성 클래스 종료
	//SFTCloseFormServer 보다 먼저 호출되어 삭제되어야한다.
	if(mainView->m_pJsonMon != NULL)
	{
		delete mainView->m_pJsonMon;
		mainView->m_pJsonMon = NULL;
	}
	//ksj end

	SetAppExitProgress(60); //ksj 20201023: 종료 진행상황 60%

	//2. server 를 닫음
	SFTCloseFormServer();


	SetAppExitProgress(70); //ksj 20201023: 종료 진행상황 70%

	//WriteLog("Program closed");
	WriteLog(Fun_FindMsg("CTSMonProDoc_OnCloseDocument_msg2","CTSMonPro_DOC"));//&&
	//CMiniDump::End(); //yulee 20190121

	//3. 로그 dialog를 삭제함 
	// 이후로는 로그를 쓰면 안됨 
	if(m_pLogDlg != NULL)
	{ 
		m_pLogDlg->DestroyWindow();
		delete m_pLogDlg;
		m_pLogDlg = NULL;
	}

 	if(m_pEventLogDlg != NULL)
 	{ 
 //		m_pEventLogDlg->DestroyWindow();
 		delete m_pEventLogDlg;
 		m_pEventLogDlg = NULL;
 	}

	SetAppExitProgress(80); //ksj 20201023: 종료 진행상황 80%

///////////////////////////////////////////////////////////
	CDocument::OnCloseDocument();

}



bool CCTSMonProDoc::PollingControlServer()
{
//	if( m_pControlSystemSock )
//	{
//		EP_CMD_PACKET aPack; ::ZeroMemory(&aPack, sizeof(EP_CMD_PACKET));
//		aPack.byUnitID = m_nRackID;
//		aPack.byCmd = EP_PACKET_POLLING;		
//
//		if( m_pControlSystemSock->Send(&aPack, sizeof(aPack)) == SOCKET_ERROR )
//		{
//			switch(m_pControlSystemSock->GetLastError())
//			{
//			case WSAENOTCONN:
//			case WSAENETRESET:
//				break;
//			default:
//				break;
//			}
//
//			// Logging
//			CString strLogMsg;
//			strLogMsg.Format("Unit %02d::Do not Received Connection Data", m_nRackID);
//			WriteLog(strLogMsg);
//
//			return false;
//		}
//	}
	return true;
}

void CCTSMonProDoc::SetCalCommConnected()
{
	if( m_pCalibratorDlg )
	{
		m_pCalibratorDlg->OnCommConnected();
	}
}

CString CCTSMonProDoc::GetModuleName(int nModuleID)
{
	CString name;
	name.Format("%s %d", m_strModuleName, nModuleID);
	return name;
}

//DataBase에서 설치된 모듈 수를 구한다.
//KBH
UINT CCTSMonProDoc::SearchInstalledModuleCount()
{
	CDaoDatabase  db;
	
	int nInstallCount = 0;

	if(m_strDataBaseName.IsEmpty())		return 0;
	try
	{
		db.Open(m_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	

	CString strSQL;
	strSQL = "SELECT COUNT(ModuleID) FROM SystemConfig";
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		data = rs.GetFieldValue(0);
		nInstallCount = data.lVal;
	}

	if(nInstallCount <= 0)	//Installed Module Number check
	{
		nInstallCount = 0;
	}
	else if(nInstallCount > SFT_MAX_MODULE_NUM)	//Max Module is 32
	{
		nInstallCount = SFT_MAX_MODULE_NUM;
	}

	rs.Close();
	db.Close();

	return nInstallCount;
}

//DataBase에서 각 모듈의 설정값들을 Load한다.
//KBH
BOOL CCTSMonProDoc::SetModuleConfig()
{
	
	CString strTemp;
	CDaoDatabase  db;
	COleVariant data;
	
	try
	{
		int nModuleIndex = 0;
		int nModuleID;
		CString strTemp, strSource;
		int index;

		db.Open(GetDataBaseName());

		CString strSQL("SELECT ModuleID, IP_Address, VSpec, ISpec, ModuleType, InstallChCount, ControllerType, ProgramVer FROM SystemConfig ORDER BY ModuleID");
		CDaoRecordset rs(&db);
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
		
		SFT_SYSTEM_PARAM	*pParam = new SFT_SYSTEM_PARAM;
		
		//Load Module Configuration
		while(!rs.IsEOF() && nModuleIndex < m_nInstalledModuleNo)
		{
			::ZeroMemory(pParam, sizeof(SFT_SYSTEM_PARAM));
			
			rs.GetFieldValue("ModuleID", data);
			if(VT_NULL == data.vt)		break;

			nModuleID = data.lVal;
			
			rs.GetFieldValue("IP_Address", data);
			if(VT_NULL != data.vt)
			{
				strcpy(pParam->szIPAddr, data.pcVal);		
			}
			
			rs.GetFieldValue("VSpec", data);	
			strTemp = data.pcVal;		
			if(VT_NULL == data.vt)
			{
				strTemp.Empty();
			}

			strSource = strTemp;		//5000/2000/1000 형태의 Data 분리
			while((index = strSource.Find("/")) >=0 )
			{
				strTemp = strSource.Left(index);
				pParam->lVSpec[pParam->wVRangeCount++] = FLOAT2LONG(atof(strTemp));
				strTemp = strSource.Mid(index+1);				//, 제외
				strSource = strTemp;
				if(pParam->wVRangeCount >= SFT_MAX_VOLTAGE_RANGE)	break;
			}
			if(!strSource.IsEmpty() && pParam->wVRangeCount < SFT_MAX_VOLTAGE_RANGE)
			{
				pParam->lVSpec[pParam->wVRangeCount++] = FLOAT2LONG(atof(strSource));
			}

			rs.GetFieldValue("ISpec", data);	
			strTemp = data.pcVal;
			if(VT_NULL == data.vt)
			{
				strTemp.Empty();
			}
			
			strSource = strTemp;		//5000/2000/1000 형태의 Data 분리
			while((index = strSource.Find("/")) >=0 )
			{
				strTemp = strSource.Left(index);
				pParam->lISpec[pParam->wIRangeCount++] = FLOAT2LONG(atof(strTemp));
				strTemp = strSource.Mid(index+1);				//, 제외
				strSource = strTemp;
				if(pParam->wIRangeCount >= SFT_MAX_CURRENT_RANGE)	break;
			}
			if(!strSource.IsEmpty() && pParam->wIRangeCount < SFT_MAX_CURRENT_RANGE)
			{
				pParam->lISpec[pParam->wIRangeCount++] = FLOAT2LONG(atof(strSource));
			}
			
			rs.GetFieldValue("ModuleType", data);
			pParam->wModuleType = (WORD)data.lVal;

			
			pParam->lMaxVoltage = long(pParam->lVSpec[0] * 1.1f);	//V Spec 의 110%;
			pParam->lMinVoltage = -1000;							// 1V
			pParam->lMaxCurrent = long(pParam->lISpec[0] * 1.1f);	//I Spec의 110% 
			pParam->lMinCurrent = -pParam->lMaxCurrent;				//I Spec의 -110%	

			rs.GetFieldValue("InstallChCount", data);
			if(VT_NULL != data.vt)
			{
				pParam->wInstallChCount = (WORD)data.lVal;
			}
			else
			{
				pParam->wInstallChCount = SFT_DEFAULT_BD_PER_MD*SFT_DEFAULT_CH_PER_BD;
			}


			pParam->bAutoProcess = FALSE;					// 자동 공정 여부
			pParam->lMaxTemp = 100;						//lOverTemp; 온도 상한치
			pParam->bUseTempLimit = FALSE;				//lLimitTemp; 온도 Limit 사용 여부
			pParam->lMaxGas = 5000;						//lDifferTemp;	가스 상한치
			pParam->bUseGasLimit = FALSE;				//lV24Over;	 가스 Limit 사용 여부
			pParam->nAutoReportInterval = 2000;			//자동 보고 시간 간격 

			//Set Module ID and Parameter
			m_lpCyclerMD[nModuleIndex].SetModuleID(nModuleIndex, nModuleID, pParam);	//default ID and channel structure
						
			rs.MoveNext();
			nModuleIndex++;
		}
		delete pParam;
		rs.Close();
		db.Close();	
	}
	catch (CDaoException *e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	return TRUE;
}

CCyclerChannel * CCTSMonProDoc::GetChannelInfo(int nModuleID/* One Base*/, int nChIndex /*Zero Base*/)
{
	CCyclerModule *pModule = GetModuleInfo(nModuleID);
	if(NULL == pModule)		return NULL;
	ASSERT(nChIndex >= 0);

	return pModule->GetChannelInfo(nChIndex);
}

CCyclerModule * CCTSMonProDoc::GetModuleInfo(UINT nModuleID)
{
	ASSERT(m_lpCyclerMD);
	int nIndex = GetModuleIndex(nModuleID);
	if(nIndex >= 0)
	{
		return &m_lpCyclerMD[nIndex];
	}
	return NULL;
}

void CCTSMonProDoc::RemoveModule()
{
	if(m_lpCyclerMD)
	{
		delete [] m_lpCyclerMD;
		m_lpCyclerMD = NULL;
	}
}

BOOL CCTSMonProDoc::AssignModuleMemory(UINT nTotalModule)
{
	if(nTotalModule < 1)	return FALSE;
	RemoveModule();
	m_lpCyclerMD = new CCyclerModule[nTotalModule];
	return TRUE;
}

INT CCTSMonProDoc::GetModuleIndex(int nModuleID)
{	
	ASSERT(m_lpCyclerMD);
	for(int i=0; i<m_nInstalledModuleNo; i++)
	{
		if(m_lpCyclerMD[i].GetModuleID() == nModuleID)
		{
			return i;
		}
	}
	return -1;
}

UINT	CCTSMonProDoc::GetModuleID(UINT nIndex)
{
	ASSERT(m_lpCyclerMD);
	if(nIndex >= m_nInstalledModuleNo)		return 0;
	return m_lpCyclerMD[nIndex].GetModuleID();
}


CCyclerModule * CCTSMonProDoc::GetCyclerMD(UINT nModuleID)
{
	ASSERT(m_lpCyclerMD);
	int index = GetModuleIndex(nModuleID);
	if(index < 0)	return NULL;

	return &m_lpCyclerMD[index];
}

//해당 모듈과 채널이 현재 명령을 전송가능한 상태인지 검사.
BOOL CCTSMonProDoc::CheckCmdState(UINT nModuleID, UINT nChannelIndex, UINT nCmdID, BOOL bModule)
{
	CCyclerModule *pMD = GetCyclerMD(nModuleID);
	
	//Error
	if(pMD == NULL)		return FALSE;
	
	//Comm Disconnected
	if(pMD->GetState() == PS_STATE_LINE_OFF)
	{
#ifndef _DEBUG
		return FALSE;
#else 
		return TRUE;
#endif
	}
	else
	{
		//모듈 상태를 검사시 
		if(bModule)		return TRUE;
	}
	//HIWORD(dwData), LOWORD(dwData)
	CCyclerChannel *pCH = pMD->GetChannelInfo(nChannelIndex);
	if(pCH == NULL)		return FALSE;
	
	if((pCH->IsParallel() && pCH->m_bMaster == FALSE) )		return FALSE;
	WORD state = pCH->GetState();
	
	switch(nCmdID)
	{
	case SFT_CMD_RUN:
		if(state == PS_STATE_STANDBY || state == PS_STATE_READY || state == PS_STATE_END || state == PS_STATE_IDLE)		return TRUE;
		//준 Stop을 지원하기 위해 
		if(state == PS_STATE_PAUSE)								return TRUE;
		break; //ksj 20160624 break가 없어서 추가함.
	case SFT_CMD_STOP:		
		if(/*state == PS_STATE_RUN ||*/ state == PS_STATE_PAUSE)	return TRUE;//yulee 20190706 Pause 후 종료인데 Run 일 때도 가능하게 한다? 확인 필요	
		break;	
	//ksj 20200213 : 원래 SFT_CMD_STOP 조건과 동일하게 사용되고있었으나
	//위에서 RUN조건을 제거하는 바람에 작업중에 스케쥴 업데이트가 불가능해짐.
	//별도로 조건 추가.
	case SFT_CMD_SAFETY_DATA_UPDATE:
	case SFT_CMD_STEP_DATA_UPDATE: 
		if(state == PS_STATE_RUN || state == PS_STATE_PAUSE)	return TRUE;
		break;
	//ksj end
	case SFT_CMD_PAUSE:		
		if(state == PS_STATE_RUN)								return TRUE;		
		break;
	case SFT_CMD_CONTINUE:	
		if(state == PS_STATE_PAUSE)								return TRUE;		
		break;
	case SFT_CMD_CLEAR:		
		if(state == PS_STATE_STANDBY)							return TRUE;		
		break;
	case SFT_CMD_GOTOSTEP:	
	case SFT_CMD_NEXTSTEP:	
		if(state == PS_STATE_RUN || state == PS_STATE_PAUSE)	return TRUE;		
		break;
	case SFT_CMD_RESET_RESERVED_CMD:
		if(pCH->IsPauseReserved() || pCH->IsStopReserved())		return TRUE;
	break;
	case SFT_CMD_RUN_QUEUE: //ksj 20160613 작업 예약, PAUSE 상태에서는 시작 불가능해야하는데 SFT_CMD_RUN은 시작 가능하도록 되어있어 추가함.
		if(state == PS_STATE_STANDBY || state == PS_STATE_READY || state == PS_STATE_END || state == PS_STATE_IDLE)		return TRUE;		
		break;
	case SFT_CMD_VENT_CLEAR:	
		return TRUE;		
	case SFT_CMD_CBANK:	
		return TRUE;		
		break;
	}
	return FALSE;
}

//해당 모듈과 채널이 현재 명령을 전송가능한 상태인지 검사.
BOOL CCTSMonProDoc::CheckCmdUIState(UINT nModuleID, UINT nChannelIndex, UINT nCmdID, BOOL bModule)
{
	CCyclerModule *pMD = GetCyclerMD(nModuleID);
	
	//Error
	if(pMD == NULL)		return FALSE;
	
	//Comm Disconnected
	if(pMD->GetState() == PS_STATE_LINE_OFF)
	{
		return FALSE;
	}
	else
	{
		//모듈 상태를 검사시 
		if(bModule)		return TRUE;
	}
	//HIWORD(dwData), LOWORD(dwData)
	CCyclerChannel *pCH = pMD->GetChannelInfo(nChannelIndex);
	if(pCH == NULL)		return FALSE;
	
	if((pCH->IsParallel() && pCH->m_bMaster == FALSE) )		return FALSE;
	WORD state = pCH->GetState();
	
	switch(nCmdID)
	{
	case SFT_CMD_RUN:
		if(state == PS_STATE_STANDBY || state == PS_STATE_READY || state == PS_STATE_END || state == PS_STATE_IDLE)		return TRUE;
		//준 Stop을 지원하기 위해 
		if(state == PS_STATE_PAUSE)								return TRUE;
	case SFT_CMD_STOP:		
		if(state == PS_STATE_RUN || state == PS_STATE_PAUSE)	return TRUE;	
		break;	
	case SFT_CMD_PAUSE:		
		if(state == PS_STATE_RUN)								return TRUE;		
		break;
	case SFT_CMD_CONTINUE:	
		if(state == PS_STATE_PAUSE)								return TRUE;		
		break;
	case SFT_CMD_CLEAR:		
		if(state == PS_STATE_STANDBY)							return TRUE;		
		break;
	case SFT_CMD_GOTOSTEP:	
	case SFT_CMD_NEXTSTEP:	
		if(state == PS_STATE_RUN || state == PS_STATE_PAUSE)	return TRUE;		
		break;
	case SFT_CMD_RESET_RESERVED_CMD:
		if(pCH->IsPauseReserved() || pCH->IsStopReserved())		return TRUE;
		break;
	}
	return FALSE;
}

CCaliPoint * CCTSMonProDoc::GetCalPoint()
{
	return &m_CalPointData;
}

//공정 조건을 SBC로 전송한다.
BOOL CCTSMonProDoc::SendScheduleToModule(UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData,CProgressWnd * pProgWnd)
{
	ASSERT(pSelChArray);
	ASSERT(pSchData);

	CString	strMsg;
	CCyclerModule *pMD;
	int nRtn = 0;
	UINT k;

	pMD = GetModuleInfo(nModuleID);
//ljb 임시
//ljb 2011316 이재복 //////////
// 	CStep *pStepData;
// 	
// 	//2. Schedule Step Data Send command
// 	for(k =0; k<pSchData->GetStepSize(); k++)
// 	{
// 		//2.1	Get Send structure step data
// 		pStepData = pSchData->GetStepData(k);
// 		if(pStepData == NULL)	break;
// 		
// 		//2.2 Send schedule send ready command
// 		//			size_t aa = sizeof(stepCon);
// 		//			TRACE("Size of Step %d\n", aa);
// 		
// 		if(pStepData->m_type == PS_STEP_PATTERN) //UserPattern 모드이면 
// 		{
// 			//2.2.1. Load User Pattern and Send that
// 			//Load User Pattern from file
// 			SFT_STEP_SIMUL_CONDITION simulStep, simuData1;	
// 			ZeroMemory(&simulStep, sizeof(SFT_STEP_SIMUL_CONDITION));
// 			
// 			LPVOID lpBuff;
// 			
// 			//memcpy(&simulStep, &GetNetworkFormatStep(pStepData), sizeof(SFT_STEP_SIMUL_CONDITION));
// 			simuData1 = GetNetworkFormatSimulStep(pStepData);
// 			memcpy(&simulStep, &simuData1, sizeof(SFT_STEP_SIMUL_CONDITION));				
// 			
// 			STF_MD_USERPATTERN_DATA patternData;
// 			ZeroMemory(&patternData, sizeof(STF_MD_USERPATTERN_DATA));
// 			if(!LoadPatternDataFromFile(pStepData->m_strPatternFileName, patternData ))	//파일을 읽어온다.
// 			{
// 				strMsg.Format("%s 에 Pattern 데이터 불러오기 실패!!!", GetModuleName(nModuleID));
// 				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 				delete patternData.pPatternData;
// 				return FALSE;
// 			}
// 		}
// 	}
//ljb 2011316 이재복 //////////
	
	if(pMD)		
	{		
		CWaitCursor cursor;	

		//1. Send schedule send ready command
		SFT_STEP_START_INFO stepInfo;
		ZeroMemory(&stepInfo, sizeof(SFT_STEP_START_INFO));
		stepInfo.byTotalStep = (BYTE)pSchData->GetStepSize();
		if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_START, pSelChArray, &stepInfo, sizeof(SFT_STEP_START_INFO))) != SFT_ACK)
		{//$1013
			//strMsg.Format("%s 에 스케쥴 전송 준비 실패!!!(Code %d)", GetModuleName(nModuleID), nRtn);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID), nRtn);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		else
		{//$1013
			//strMsg.Format("%s 에 스케쥴 전송 준비 성공", GetModuleName(nModuleID));
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg2","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
		}

		//Protocal ver 0x1002 
		//2006/04/27
		if(pMD->GetProtocolVer() > _SFT_PROTOCOL_VERSIONV2)
		{
			SFT_TEST_SAFETY_SET_V1 safetyInfo;
			ZeroMemory(&safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1));

			CELL_CHECK_DATA* pCheckData = pSchData->GetCellCheckParam();
			//Voltage Limit
			safetyInfo.lVtgHigh	=	FLOAT2LONG(pCheckData->fMaxV);
			safetyInfo.lVtgLow	=	FLOAT2LONG(pCheckData->fMinV);
			safetyInfo.lVtgCellDelta	= FLOAT2LONG(pCheckData->fCellDeltaV);			//ljb 20150730
			
			//yulee 20181114 Protocol 1013 - 주석처리
			float fltCellVolt = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", 4600);
			float fltFixTempHigh = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", 80);
			safetyInfo.lVtgCellHigh = FLOAT2LONG(fltCellVolt);
			safetyInfo.lTempHigh	= FLOAT2LONG(fltFixTempHigh);

			//Current Limit
			safetyInfo.lCrtHigh	= FLOAT2LONG(pCheckData->fMaxI);

			//Temperature limit
			safetyInfo.lTempHigh = pCheckData->fMaxT;
			safetyInfo.lTempLow = pCheckData->fMinT;
			
			//Capacity Limit
			safetyInfo.lCapHigh	= FLOAT2LONG(pCheckData->fMaxC);			//Vref
			safetyInfo.lWattHigh	= pCheckData->lMaxW;			//Vref
			safetyInfo.lWattHourHigh = pCheckData->lMaxWh;			//Vref
			
			//ljb 20100820
			for (int i=0; i < _SFT_MAX_CAN_AUX_COMPARE_STEP; i++)
			{
				safetyInfo.can_function_division[i] = pCheckData->ican_function_division[i];
				safetyInfo.can_compare_type[i] = pCheckData->cCan_compare_type[i];
				safetyInfo.can_data_type[i] = pCheckData->cCan_data_type[i];
				safetyInfo.fcan_Value[i] = pCheckData->fcan_Value[i];
				safetyInfo.aux_function_division[i] = pCheckData->iaux_function_division[i];
				safetyInfo.aux_compare_type[i] = pCheckData->cAux_compare_type[i];
				safetyInfo.aux_data_type[i] = pCheckData->cAux_data_type[i];
				safetyInfo.faux_Value[i] = pCheckData->faux_Value[i];
			}
			
			if((nRtn = pMD->SendCommand(SFT_CMD_SAFETY_DATA, pSelChArray, &safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1))) != SFT_ACK)
			{//$1013
				//strMsg.Format("%s 에 스케쥴 안전조건 전송 실패!!!(Code %d)", GetModuleName(nModuleID), nRtn);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg3","CTSMonPro_DOC"), GetModuleName(nModuleID), nRtn);//&&
				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			else
			{//$1013
				//strMsg.Format("%s 에 스케쥴 안전조건 전송 성공", GetModuleName(nModuleID));
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg4","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
				WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
			}
		}
		else
		{//$1013
			//strMsg.Format("%s 는 시험 안전조건 적용을 지원하지 않는 버전입니다.", GetModuleName(nModuleID));
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg5","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
		}


		//ljb 2011415 이재복 //////////
		int nProgPos = 0;
		int nProgIncrease = 100 / pSchData->GetStepSize();

		//Protocol ver 0x1002 (End Condition에 EndWatt, EndWattHour 및 Reserved 추가)
		//2006/04/27
		//Protocol Ver 0x1000~0x1001과 호환 안됨(새방, NessCap, LS1차분)
		CStep *pStepData;
		
		//2. Schedule Step Data Send command
		for(k =0; k<pSchData->GetStepSize(); k++)
		{
			//2.1	Get Send structure step data
			pStepData = pSchData->GetStepData(k);
			if(pStepData == NULL)	break;
			
			//2.2 Send schedule send ready command
//			size_t aa = sizeof(stepCon);
//			TRACE("Size of Step %d\n", aa);
	
			if(pStepData->m_type == PS_STEP_PATTERN) //UserPattern 모드이면 
			{
				if (Fun_CheckPatternFile(pStepData->m_strPatternFileName) == FALSE)
				{//$1013
					//strMsg.Format("%s 에 Pattern 데이터 불러오기 실패 (file : %s)!!!", GetModuleName(nModuleID),pStepData->m_strPatternFileName);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg6","CTSMonPro_DOC"), GetModuleName(nModuleID),pStepData->m_strPatternFileName);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strMsg);
					return FALSE;
				}

				//2.2.1. Load User Pattern and Send that
				//Load User Pattern from file
				SFT_STEP_SIMUL_CONDITION simulStep, simuData1;	
				ZeroMemory(&simulStep, sizeof(SFT_STEP_SIMUL_CONDITION));

				LPVOID lpBuff;

				//memcpy(&simulStep, &GetNetworkFormatStep(pStepData), sizeof(SFT_STEP_SIMUL_CONDITION));
				simuData1 = GetNetworkFormatSimulStep(pStepData);
				memcpy(&simulStep, &simuData1, sizeof(SFT_STEP_SIMUL_CONDITION));				

				STF_MD_USERPATTERN_DATA patternData;
				ZeroMemory(&patternData, sizeof(STF_MD_USERPATTERN_DATA));
				if(!LoadPatternDataFromFile(pStepData->m_strPatternFileName, patternData ))	//파일을 읽어온다.
				{//$1013
					//strMsg.Format("%s 에 Pattern 데이터 불러오기 실패!!!", GetModuleName(nModuleID));
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg7","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					delete patternData.pPatternData;
					return FALSE;
				}
		
				if (patternData.lLength > 1080000)
				{//$1013
					//strMsg.Format("%s STEP(%d) Pattern 용량이 큽니다. 분할하여 적용해 주세요.", GetModuleName(nModuleID),pStepData->m_StepIndex+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg8","CTSMonPro_DOC"), GetModuleName(nModuleID),pStepData->m_StepIndex+1);//&&
					AfxMessageBox(strMsg);
					return FALSE;
				}
				TRACE("\n SFT_STEP_SIMUL_CONDITION Size = %d",sizeof(SFT_STEP_SIMUL_CONDITION));
				TRACE("\n Pattern Data Size = %d",patternData.lLength);

				lpBuff = new char[sizeof(SFT_STEP_SIMUL_CONDITION) + patternData.lLength];

				memcpy(lpBuff, &simulStep,sizeof(SFT_STEP_SIMUL_CONDITION) );
				memcpy( (LPBYTE)lpBuff+sizeof(SFT_STEP_SIMUL_CONDITION), patternData.pPatternData, patternData.lLength );
						
				//Send User Pattern Data to SBC
				if((nRtn = pMD->SendCommand(SFT_CMD_USERPATTERN_DATA, pSelChArray, lpBuff, sizeof(SFT_STEP_SIMUL_CONDITION)+patternData.lLength)) != SFT_ACK)
				{//$1013				
					//strMsg.Format("%s 에 Pattern 데이터 전송 완료 실패!(Code : %d)", GetModuleName(nModuleID), nRtn);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg9","CTSMonPro_DOC"), GetModuleName(nModuleID), nRtn);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					delete patternData.pPatternData;
					delete lpBuff;
					return FALSE;
				}
				else
				{//$1013
					//strMsg.Format("%s 에 Pattern 데이터 전송 완료 성공", GetModuleName(nModuleID));
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg10","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
					WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);					
					delete patternData.pPatternData;
					delete lpBuff;
	
				}
			}
			else
			{
				SFT_STEP_CONDITION_V3 stepCon; //lyj 20200214 LG v1015 이상
				
				stepCon = GetNetworkFormatStep_V1015(pStepData); //lyj 20200214 LG v1015 이상
				TRACE("NO : %d, Type : %d",
					pStepData->m_StepIndex, pStepData->m_type);

				if((nRtn = pMD->SendCommand(SFT_CMD_STEP_DATA, pSelChArray, &stepCon, sizeof(stepCon))) != SFT_ACK)
				{//$1013
					//strMsg.Format("%s 에 스케쥴 Step %d 전송 실패!!!(Code %d)", GetModuleName(nModuleID), k+1, nRtn);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg11","CTSMonPro_DOC"), GetModuleName(nModuleID), k+1, nRtn);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					//				return FALSE;
				}
				else
				{//$1013
					//strMsg.Format("%s 에 스케쥴 %d 전송 성공",   GetModuleName(nModuleID), k+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg12","CTSMonPro_DOC"),   GetModuleName(nModuleID), k+1);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
				}
			}

			//ljb 2011415 이재복 //////////
			nProgPos += nProgIncrease;
			pProgWnd->SetPos(nProgPos);
		}

	
		//3. Send schedule send complete
		if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_END, pSelChArray)) != SFT_ACK)
		{//$1013
			//strMsg.Format("%s 에 스케쥴 전송 완료 실패!!!", GetModuleName(nModuleID));
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg13","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		else
		{//$1013
			//strMsg.Format("%s 에 스케쥴 전송 완료 성공", GetModuleName(nModuleID));
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg14","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
		}
		//4. Save temp file that last sent schedule data
		//=>시험 조건을 저장하지 않고 RUN 명령시 시작 정보를 저장(결과 Folder명)
/*		CString strFileName;
		WORD	chIndex;
		for(int a = 0; a<pSelChArray->GetSize(); a++)
		{
			chIndex = pSelChArray->GetAt(a);
			strFileName.Format("%s\\Temp\\M02%dC%02d.tmp", m_strCurPath, nModuleID, chIndex+1);
			pSchData->SaveToFile(strFileName);
		}
*/

	}
	else
	{//$1013
		//strMsg.Format("%s 정보 요청 실패!!", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModule_msg15","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
		WriteLog(strMsg);
		return FALSE;
	}
	return TRUE;
}
/*
---------------------------------------------------------
---------
-- Filename		: 
-- Description	:	공정 조건을 FTP로 전송한다.
-- Author		:		
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 2014.09.11	이민규		usermap 기능을 추가. usermap은 pattern파일 이름과 동일하다.
----------------------------------------------------------
*/
BOOL CCTSMonProDoc::InitChScheduleToModuleFTP(UINT nModuleID, CWordArray *pSelChArray)
{
	CCyclerModule *pMD;
	CCyclerChannel *pChInfo;
	pMD		=	GetModuleInfo(nModuleID);
	
	//0. FTP 접속
	CString strIP = pMD->GetIPAddress();
	if(strIP.IsEmpty())		{
		//AfxMessageBox("모듈 IP 이상");
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_InitChScheduleToModuleFTP_msg1","CTSMonPro_DOC"));//&&
		return FALSE;
	}
	if (Fun_FtpConnect(strIP) == FALSE)		{
		if (Fun_FtpConnect(strIP) == FALSE)		{
			//AfxMessageBox("FTP 접속 실패 !!!");
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_InitChScheduleToModuleFTP_msg2","CTSMonPro_DOC"));//&&
			return FALSE;
		}
	}
	
	//1. FTP 폴더에 run.txt 파일 있는지 체크
	//2. FTP 폴더내 모든 파일 삭제, 없으면 생성
	CString strChangeDir,strMsg;
	int ch;
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pChInfo)
		{
			if(	pChInfo->GetState() == PS_STATE_IDLE || 
				pChInfo->GetState() == PS_STATE_STANDBY ||
				pChInfo->GetState() == PS_STATE_READY ||
				pChInfo->GetState() == PS_STATE_END
				)
			{
				strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
				TRACE("%s \n",strChangeDir);
				
				if (Fun_FtpChangeDir(strChangeDir) == TRUE)
				{
					if (Fun_FtpFileDeleteAll(strChangeDir) == FALSE)
					{
						//strMsg.Format("%s 에서 파일 삭제 에러.(%s)",GetModuleName(nModuleID),strChangeDir);
						strMsg.Format(Fun_FindMsg("CTSMonProDoc_InitChScheduleToModuleFTP_msg3","CTSMonPro_DOC"),GetModuleName(nModuleID),strChangeDir);//&&
						WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
						AfxMessageBox(strMsg);
						//Fun_FtpDisConnect();	//yulee 20190615_2 FTP 접속 종료 추가
					}
				}
				Fun_FtpDisConnect();	//yulee 20190615_2 FTP 접속 종료 추가
			}
		}
	}
	return TRUE;
}
//Normal Test Start
BOOL CCTSMonProDoc::SendScheduleToModuleFTP(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, int nOption,int ReserveID)
{
	ASSERT(pSelChArray);
	ASSERT(pSchData);

	BOOL nRet = FALSE;

	CCyclerModule * pMD = GetModuleInfo(nModuleID);
	if(pMD != NULL)
	{
		//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0);
		UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
		if (SchFileVer == SCHEDULE_FILE_VER_v1014_v1_SCH)
		{
			CSendToFTP_v1014_v1_SCH sendSch;
			nRet = sendSch.SendSchFTP_v1014_v1_SCH(nMaxModuleCnt, nModuleID, pSelChArray, pSchData, pProgressWnd, nOption,ReserveID, this);

			return nRet;
		}
		else if (SchFileVer == SCHEDULE_FILE_VER_v1013_v1_SCH)
		{
			CSendToFTP_v1013_v1_SCH sendSch;
			nRet = sendSch.SendSchFTP_v1013_v1_SCH(nMaxModuleCnt, nModuleID, pSelChArray, pSchData, pProgressWnd, nOption,ReserveID, this);

			return nRet;
		}
		else if (SchFileVer == SCHEDULE_FILE_VER_v1011_v2_SCH)
		{
			CSendToFTP_v1011_v2_SCH sendSch;
			nRet = sendSch.SendSchFTP_v1011_v2_SCH(nMaxModuleCnt, nModuleID, pSelChArray, pSchData, pProgressWnd, nOption,ReserveID, this);

			return nRet;
		}
		else //if (SchFileVer == SCHEDULE_FILE_VER_v1015_v1)
		{
			CSendToFTP_v1015_v1 sendSch;
			nRet = sendSch.SendSchFTP_v1015_v1(nMaxModuleCnt, nModuleID, pSelChArray, pSchData, pProgressWnd, nOption,ReserveID, this);

			return nRet;
		}
	}

#if 0
	//pProgressWnd->SetText(_T("스케줄 가공중 "));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg1"));//&&
	pProgressWnd->SetPos(30);

	CStringArray	arryStrPatternFile;
	CString	strTemp,strMsg,strFileName,strFileName2,strDsc;
	short int	nWaitTimeGotoCnt = 0;	//ljb 20160427 add

	strFileName2.Empty();

	CCyclerModule *pMD;
	CCyclerChannel *pChInfo;
	CStep *pStepData;

	int nRtn = 0;
	int ch = 0;
	int nPatternCount = 0; //스케쥴에 있는 패턴 STEP 갯수
	arryStrPatternFile.RemoveAll();		//SBC용 스케쥴 파일에 저장할 Pattern 파일 이름

	UINT k;
	CUIntArray arryTimeType;
	CUIntArray arryModeType;
	CUIntArray arryPlusMode;
	CUIntArray arryMaxValue;
	
	CUIntArray arryFileSize;
	CUIntArray arryCheckSum;
	
	long lTimeType, lModeType, lPulsMode,lMaxValue;
	ULONG lFileSize=0, lCheckSum=0;
	ULONG lFileSize2=0, lCheckSum2=0;
	float fMaxValue=0;
	int nPos = 0;
	
	pMD		=	GetModuleInfo(nModuleID);

	//현재 시간 구함 
	CString strTimeCheck;
	CTime tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 전송 시작 (SendScheduleToModuleFTP :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg2"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	WriteSysLog(strTimeCheck);

	//0. FTP 접속
	CString strIP = pMD->GetIPAddress();
	if(strIP.IsEmpty())		{
		//AfxMessageBox("모듈 IP 이상");//$1013
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg3"));//&&
		return FALSE;
	}
	if (Fun_FtpConnect(strIP) == FALSE)		{
		if (Fun_FtpConnect(strIP) == FALSE)		{
			//AfxMessageBox("FTP 접속 실패 !!!");//$1013
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg4"));//&&
			return FALSE;
		}
	}

	//pProgressWnd->SetText(_T("접속 완료"));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg5"));//&&
	pProgressWnd->SetPos(40);

	//strMsg.Format(_T("접속 완료."));//$1013
	strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg6"));//&&

	SFT_STEP_START_INFO stepInfo;
	ZeroMemory(&stepInfo, sizeof(SFT_STEP_START_INFO));	

	//+2015.9.23 USY Add For PatternCV
	UINT nParallelCnt = 0;
	//-

	//1. FTP 폴더에 run.txt 파일 있는지 체크
	//2. FTP 폴더내 모든 파일 삭제, 없으면 생성
	CString strChangeDir;
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pChInfo)
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
			{
				//ksj 20180223 : 주석처리
				if (pChInfo->GetChannelIndex() < 1)       
				{
					strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
				}
				else if (pChInfo->GetChannelIndex() == 1)
				{
					strChangeDir.Format("/START_INFO/CH%03d",3);	//ljb 201706 add
				}
			}
//#else
			else
			{
				//ksj 20180223 : 기존 코드 원복
				strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
			}//-----------------------------------------------------------------
//#endif
			if (Fun_FtpChangeDir(strChangeDir) == TRUE)
			{
				if (Fun_FtpFileFind("run.txt") == TRUE)
				{//$1013
					//strMsg.Format("%s SBC에서 작업 중인 채널 입니다.(ch : %d)",GetModuleName(nModuleID), pChInfo->GetChannelIndex()+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg7"),GetModuleName(nModuleID), pChInfo->GetChannelIndex()+1);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strMsg);
					return FALSE;			
				}
				else
				{
					if (Fun_FtpFileDeleteAll(strChangeDir) == FALSE)
					{//$1013
						//strMsg.Format("%s 에서 파일 삭제 에러.(%s)",GetModuleName(nModuleID),strChangeDir);
						strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg8"),GetModuleName(nModuleID),strChangeDir);//&&
						WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
						AfxMessageBox(strMsg);
					}
				}
			}
			//+2015.9.23 USY Add For PatternCV
			if(pChInfo->IsParallel()) nParallelCnt++;
			//-
		}
		else
		{//$1013
			//strMsg.Format("sbc에 Run.txt 파일 이상.(ch : %d)",pChInfo->GetChannelIndex()+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg9"),pChInfo->GetChannelIndex()+1);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			AfxMessageBox(strMsg);
			return FALSE;			
		}
	}
	//pProgressWnd->SetText(_T("패턴의 유무를 확인중입니다."));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg10"));//&&
	pProgressWnd->SetPos(60);

	//3. 전송할 스케쥴 확인 (Pattern 확인)
	if(pSchData->GetStepSize() <= 0)
	{//$1013
		//strMsg.Format("%s 에 전송할 스케줄이 없습니다.!!!", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg11"), GetModuleName(nModuleID));//&&
		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}
	//+2015.9.23 USY Add For PatternCV
	float fMinVoltage;
	if(m_bOverChargerSet) //yulee 20180227  //yulee 20191017 OverChargeDischarger Mark
	{
		float OverChargerMinVolt = 0;
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);
		fMinVoltage= -OverChargerMinVolt;//yulee 20180810
	}
	else
		fMinVoltage= 0.0f;
	float fMaxVoltage = 0.0f;
	float fMaxCurrent = 0.0f;

	
	CString rng, str;
	fMaxVoltage = pMD->GetVoltageSpec()/1000.0f;
	for(int r=0; r<pMD->GetCurrentRangeCount(); r++)
	{
		if((pMD->GetCurrentSpec(r)/1000.0f) > fMaxCurrent)
		{
			fMaxCurrent = pMD->GetCurrentSpec(r)/1000.0f;
		}
	}
	//-

	for(k =0; k<pSchData->GetStepSize(); k++)
	{
		pStepData = pSchData->GetStepData(k);
		if(pStepData == NULL)
		{//$1013
			//strMsg.Format("%s 에 전송할 스케줄 STEP %d 가 NULL 입니다.!!!", GetModuleName(nModuleID),k+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg12"), GetModuleName(nModuleID),k+1);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		
		////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 시작 ////////////////
		lTimeType = lModeType = lPulsMode = lMaxValue = 0;
		lFileSize = lCheckSum = 0;
		strFileName ="";

		//WaitTimeGotoStep count 확인
		if (pStepData->m_nWaitTimeInit > 0 || pStepData->m_nWaitTimeDay > 0 
			|| pStepData->m_nWaitTimeHour > 0 || pStepData->m_nWaitTimeMin > 0  || pStepData->m_nWaitTimeSec > 0)
		{
			nWaitTimeGotoCnt++;
		}
		//2014.09.11 PS_STEP_USER_MAP 추가. 
		//20180427 yulee 예약시작 시 패턴 path 가져오기에 대한 수정
		//예약시작 시 패턴 데이터를 가져오는 pStaepData는 주기적으로 초기화가 이루어지므로 
		//pStepData->m_strPatternFileName은 비어 있음. 
		//따라서 예약 DB에 저장된 스케쥴 path를 통하여 다시 해당 스텝에 대한 패턴 path를 가져와 적용 시켜야함.
		
		if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
		{
			//			2013-06-12			bung			:			wait_FTP_endTime_plusPtrn
			m_wait_FTP_endTime_plusPtrn += 10;
			
			if(nOption == 2) //WORK_START_RESERV_RUN 2로 예약기능에서 사용 중
			{//패턴 해당 path를 sch에서 가져온다
				bool bRet = -1;
				bRet = GetPattPathFromDB(ReserveID, k, strFileName);
				if(bRet == TRUE)
				{
					pStepData->m_strPatternFileName = strFileName;
				}
			}
			else //기존과 동일 
				strFileName = pStepData->m_strPatternFileName;
			nPatternCount++;
			
			//+2015.9.23 USY Add For PatternCV	
			
			if(CheckPatternData(strFileName, fMaxVoltage, fMaxCurrent, nParallelCnt) == FALSE)
			{//$1013
				//strMsg.Format("%s Pattern File 오류", strFileName);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg13"), strFileName);//&&
				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			//-
			
			if (pStepData->m_fValueMax != 0) lMaxValue = pStepData->m_fValueMax / 1000;		//ljb 20130509 add
			else lMaxValue = pStepData->m_fValueMax;										//ljb 20130509 add
			
			//ljb 20130514 메모리로 읽고 CheckSum 후 strDsc로 파일 복사 생성 한다.
			if(pStepData->m_type == PS_STEP_PATTERN){
				strDsc.Format("%s\\Pattern\\sbc_pattern_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}else if(pStepData->m_type == PS_STEP_USER_MAP){ //2014.09.11 PS_STEP_USER_MAP 추가.
				strDsc.Format("%s\\Pattern\\sbc_usermap_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}
			
			
			if(Fun_MemoryCopyFileAndCheckSum(strFileName, strDsc, lFileSize, lCheckSum) == FALSE)
			{//$1013
				//strMsg.Format("%s 에 Simulation File Size, checksum 실패!!! %s (File size = %d, CheckSum = %d", GetModuleName(nModuleID), m_strLastErrorString,lFileSize,lCheckSum);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg14"), GetModuleName(nModuleID), m_strLastErrorString,lFileSize,lCheckSum);//&&
				AfxMessageBox(strMsg);
				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			Sleep(100);	//201305 슬립이 있어야 CSV 파일이 안 깨짐
			//strMsg.Format("%s 에 Simulation File Size, checksum 성공 !!! %s (File size = %d, CheckSum = %d", GetModuleName(nModuleID), m_strLastErrorString,lFileSize,lCheckSum);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg15"), GetModuleName(nModuleID), m_strLastErrorString,lFileSize,lCheckSum);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		
			//TRACE("STEP %d (Pattern Data check 완료)\r\n", pStepData->m_StepIndex);	//$1013			
			TRACE(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg55"), pStepData->m_StepIndex);	//&&
			////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 끝 ////////////////
			arryStrPatternFile.Add(strDsc);
		}
		else
		{
			arryStrPatternFile.Add(strFileName);
		}
		/*
		//2014.09.11 PS_STEP_USER_MAP 추가. 
		if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
		{
//			2013-06-12			bung			:			wait_FTP_endTime_plusPtrn
			m_wait_FTP_endTime_plusPtrn += 10;
	
			strFileName = pStepData->m_strPatternFileName;
			nPatternCount++;

			//+2015.9.23 USY Add For PatternCV	

			if(CheckPatternData(strFileName, fMaxVoltage, fMaxCurrent, nParallelCnt) == FALSE)
			{
				strMsg.Format("%s Pattern File 오류", strFileName);
				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			//-

			if (pStepData->m_fValueMax != 0) lMaxValue = pStepData->m_fValueMax / 1000;		//ljb 20130509 add
			else lMaxValue = pStepData->m_fValueMax;										//ljb 20130509 add

			//ljb 20130514 메모리로 읽고 CheckSum 후 strDsc로 파일 복사 생성 한다.
			if(pStepData->m_type == PS_STEP_PATTERN){
				strDsc.Format("%s\\Pattern\\sbc_pattern_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}else if(pStepData->m_type == PS_STEP_USER_MAP){ //2014.09.11 PS_STEP_USER_MAP 추가.
				strDsc.Format("%s\\Pattern\\sbc_usermap_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}


			if(Fun_MemoryCopyFileAndCheckSum(strFileName, strDsc, lFileSize, lCheckSum) == FALSE)
			{
				strMsg.Format("%s 에 Simulation File Size, checksum 실패!!! %s (File size = %d, CheckSum = %d", GetModuleName(nModuleID), m_strLastErrorString,lFileSize,lCheckSum);
				AfxMessageBox(strMsg);
				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			Sleep(100);	//201305 슬립이 있어야 CSV 파일이 안 깨짐
			strMsg.Format("%s 에 Simulation File Size, checksum 성공 !!! %s (File size = %d, CheckSum = %d", GetModuleName(nModuleID), m_strLastErrorString,lFileSize,lCheckSum);
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);

			TRACE("STEP %d (Pattern Data check 완료)\r\n", pStepData->m_StepIndex);				
		////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 끝 ////////////////
			arryStrPatternFile.Add(strDsc);
		}
		else
		{
			arryStrPatternFile.Add(strFileName);
		}
		*/
		arryTimeType.Add(lTimeType);
		arryModeType.Add(lModeType);
		arryPlusMode.Add(lPulsMode);
		arryMaxValue.Add(lMaxValue);

		arryFileSize.Add(lFileSize);
		arryCheckSum.Add(lCheckSum);
	}

	stepInfo.byTotalStep = pSchData->GetStepSize();
	stepInfo.nPatternStepCount = nPatternCount;
	stepInfo.bWaitTimeGotoCount = nWaitTimeGotoCnt;		//ljb 20160427 add

	//pProgressWnd->SetText(_T("스케줄 전송 준비 완료"));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg16"));//&&
	pProgressWnd->SetPos(70);

	strMsg.Format(" SFT_CMD_SCHEDULE_START :: (1020) before sendcmmand", GetModuleName(nModuleID), nRtn);
	WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
 //			2013-11-28			bung			:			SelChArray Dump S
	CWordArray *pDump_SelChArray;
	pDump_SelChArray = pSelChArray;

	// 스케쥴 시작
	if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_START, pSelChArray, &stepInfo, sizeof(SFT_STEP_START_INFO))) != SFT_ACK)
	{//$1013
		//strMsg.Format("%s 에 스케쥴 전송 준비 실패!!!(Code %d)", GetModuleName(nModuleID), nRtn);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg17"), GetModuleName(nModuleID), nRtn);//&&
		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}
	else
	{//$1013
		//strMsg.Format("%s 에 스케쥴 전송 준비 성공", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg18"), GetModuleName(nModuleID));//&&
		WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	}

	strMsg.Format(" SFT_CMD_SCHEDULE_START :: (1020) after sendcmmand", GetModuleName(nModuleID), nRtn);
	WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);

	SFT_STEP_CONDITION_V3 stepCon; //lyj 20200214 LG v1015 이상
	SFT_STEP_CONDITION_WAIT_TIME stepConWaitTime;	//ljb 20160427 add


	BOOL bUseChiller = FALSE;
	//4. FTP 전송용 스케쥴 파일 생성
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
 		if(pChInfo)
		{
			strFileName = pChInfo->GetScheduleFileForSbc();
						
			//2014.12.10 간이 충방전일경우 저장시간을 설정안할경우. 임시폴더를 생성하고 임시 스케줄 파일을 만들어 공정을 돌린다.
			if(m_bSimpleTest){
				CString sPath;
				sPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC,"Data Path","C:\\");
				//폴더를 생성한다.				
				strFileName.Format("%s\\simpleTest\\M%02dCh%02d[%03d]\\StepStart",sPath,nModuleID,pChInfo->GetChannelIndex() + 1,1);		//ljb 20150508 add
				ForceDirectory(strFileName);
				strFileName.Format("%s\\simpleTest\\M%02dCh%02d[%03d]",sPath,nModuleID,pChInfo->GetChannelIndex() + 1,1);
				pChInfo->SetFilePath(strFileName);
				strFileName.Format("%s\\StepStart\\sbc_schedule_info.sch",strFileName);
				DeleteFile(strFileName);
			}
			
			//strMsg.Format("%s 파일 생성 시작", strFileName);//$1013
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg19"), strFileName);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.21
			//if(FALSE == pSchData->SaveToFile(strFileName, FALSE))
			if(FALSE == pSchData->SaveToFile_ForFTP(strFileName, FALSE))		//sbc로 전송할 sbc_schedule.sch file 생성 (0k byte) 	
			{//$1013
				//strMsg.Format("%s 에 %d 채널 스케쥴 파일 생성 실패.!!!", GetModuleName(nModuleID), ch+1);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg20"), GetModuleName(nModuleID), ch+1);//&&
				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			// -
			if (nWaitTimeGotoCnt > 0)
			{
				strFileName2 = pChInfo->GetScheduleFileForSbc2();					//20160427 sbc_schedule_info_wait_goto.sch
				if(FALSE == pSchData->SaveToFile_ForFTP(strFileName2, FALSE))		//20160427 sbc로 전송할 sbc_schedule_info_wait_goto.sch file 생성 (0k byte) 	
				{
					//strMsg.Format("%s 에 %d 채널 WaitGoto 스케쥴 파일 생성 실패.!!!", GetModuleName(nModuleID), ch+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg21"), GetModuleName(nModuleID), ch+1);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					return FALSE;
				}
			}
			//////////////////////////////////////////////////////////////////////////


			/*UINT nProtocolVer = pChInfo->GetProtocolVersion();
			if (nProtocolVer == _SFT_PROTOCOL_VERSION)
			{
			}*/

			
///////////////////////////////////////스케줄 시작 정보 저장 시작 ////////////////////////////////////////////////////////
			//file에 추가
			CFile rltData;
			CFileException e;
			long fsize = 0;
			
			CFile rltData2;
			CFileException e2;
			long fsize2 = 0;
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.21
			//if(!rltData.Open(strFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			if(!rltData.Open(strFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			// - 
			//////////////////////////////////////////////////////////////////////////
			{//$1013
				//strMsg.Format("%s 에 %d 채널 스케쥴 파일 추가 실패.!!!", GetModuleName(nModuleID), ch+1);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg22"), GetModuleName(nModuleID), ch+1);//&&
				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			fsize = rltData.SeekToEnd();
			rltData.Seek(0, CFile::end);

			if(rltData.m_hFile != NULL)
			{
				rltData.Write(&stepInfo, sizeof(SFT_STEP_START_INFO));
				rltData.Flush();
			}

			//ljb 20160427 waitTimeGoto write
			if (nWaitTimeGotoCnt > 0)
			{
				if(!rltData2.Open(strFileName2, CFile::modeReadWrite|CFile::shareDenyWrite, &e2))
					// - 
					//////////////////////////////////////////////////////////////////////////
				{
					//strMsg.Format("%s 에 %d 채널 스케쥴 파일 추가 실패.!!!", GetModuleName(nModuleID), ch+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg23"), GetModuleName(nModuleID), ch+1);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					return FALSE;
				}
				fsize2 = rltData2.SeekToEnd();
				rltData2.Seek(0, CFile::end);
				
				if(rltData2.m_hFile != NULL)
				{
					rltData2.Write(&stepInfo, sizeof(SFT_STEP_START_INFO));
					rltData2.Flush();
				}
			}
			///////////////////////////////////////안전조건 저장 시작 ////////////////////////////////////////////////////////
			SFT_TEST_SAFETY_SET_V1 safetyInfo;
			ZeroMemory(&safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1));
			
			CELL_CHECK_DATA* pCheckData = pSchData->GetCellCheckParam();
			//Voltage Limit
			safetyInfo.lVtgHigh	=	FLOAT2LONG(pCheckData->fMaxV);
			safetyInfo.lVtgLow	=	FLOAT2LONG(pCheckData->fMinV);
			safetyInfo.lVtgCellDelta	= FLOAT2LONG(pCheckData->fCellDeltaV);		//ljb 20150730
			
			//yulee 20181114 Protocol 1013
			float fltCellVolt = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", 4600);
			float fltFixTempHigh = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", 80);
			safetyInfo.lVtgCellHigh = FLOAT2LONG(fltCellVolt);
			safetyInfo.lTempHigh	= FLOAT2LONG(fltFixTempHigh);

			//Current Limit
			safetyInfo.lCrtHigh	= FLOAT2LONG(pCheckData->fMaxI);
			
			//Temperature limit
			//safetyInfo.lTempHigh = pCheckData->fMaxT;
			safetyInfo.lTempLow = pCheckData->fMinT;
			
			//Capacity Limit
			safetyInfo.lCapHigh	= FLOAT2LONG(pCheckData->fMaxC);			//Vref
			safetyInfo.lWattHigh	= pCheckData->lMaxW;			//Vref
			safetyInfo.lWattHourHigh = pCheckData->lMaxWh;			//Vref
			
			//ljb 20100820
			for (int i=0; i < _SFT_MAX_CAN_AUX_COMPARE_STEP; i++)
			{
				safetyInfo.can_function_division[i] = pCheckData->ican_function_division[i];
				safetyInfo.can_compare_type[i] = pCheckData->cCan_compare_type[i];
				safetyInfo.can_data_type[i] = pCheckData->cCan_data_type[i];
				safetyInfo.fcan_Value[i] = pCheckData->fcan_Value[i];
				safetyInfo.aux_function_division[i] = pCheckData->iaux_function_division[i];
				safetyInfo.aux_compare_type[i] = pCheckData->cAux_compare_type[i];
				safetyInfo.aux_data_type[i] = pCheckData->cAux_data_type[i];
				safetyInfo.faux_Value[i] = pCheckData->faux_Value[i];
			}

			if(rltData.m_hFile != NULL)
			{
				rltData.Write(&safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1));
				rltData.Flush();
			}

			///////////////////////////////////////스텝 데이터 저장 시작 ////////////////////////////////////////////////////////
			CStep *pStepData;

			//2. Schedule Step Data Send command
			for(k =0; k<pSchData->GetStepSize(); k++)
			{
				pStepData = pSchData->GetStepData(k);
				if(pStepData == NULL)	break;
			
				if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)	{
					//+2015.9.22 USY Mod For PatternCV(김재호K)
					//if((pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge <= 0)&& (pStepData->m_mode !=PS_MODE_CV))
					if((pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge < fMinVoltage)&& (pStepData->m_mode !=PS_MODE_CV)) //ksj 20160703 조건 수정.
					{//$1013
						//m_strLastErrorString.Format("Step %d의 충전CV전압이나 방전CV전압이 설정되지 않았습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg24"), k+1); //&&
						AfxMessageBox(m_strLastErrorString);
						return FALSE;
					}
// 					if(pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge <= 0)		{
// 						m_strLastErrorString.Format("Step %d의 충전CV전압이나 방전CV전압이 설정되지 않았습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 						AfxMessageBox(m_strLastErrorString);
// 						return FALSE;
// 					}
					//-
					else	{//$1013
						//if(pStepData->m_fEndV_H > fMinVoltage && pStepData->m_fVref_Charge <= pStepData->m_fEndV_H)	{
						if(pStepData->m_fEndV_H > fMinVoltage && pStepData->m_fVref_Charge <= pStepData->m_fEndV_H && pStepData->m_fEndV_H != 0)	{ //ksj 20160711 조건 수정
							//m_strLastErrorString.Format("Step %d의 충전전압 설정값이 종료전압H보다 낮습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg25"), k+1);//&&
							AfxMessageBox(m_strLastErrorString);
							return FALSE;
						}//$1013
						//if(pStepData->m_fEndV_L > fMinVoltage && pStepData->m_fVref_DisCharge >= pStepData->m_fEndV_L)
						if(pStepData->m_fEndV_L > fMinVoltage && pStepData->m_fVref_DisCharge >= pStepData->m_fEndV_L && pStepData->m_fEndV_L != 0) //ksj 20160711 조건 수정
						{
							//m_strLastErrorString.Format("Step %d의 방전전압 설정값이 종료전압L보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg26"), k+1);//&&
							AfxMessageBox(m_strLastErrorString);
							return FALSE;
						}
					}			
				}

				if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)	
				{
					if(strlen(pStepData->m_strPatternFileName) <= 0)	
					{//$1013
						//m_strLastErrorString.Format("Step %d의 Simulation data file이 지정되지 않았습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg27"), k+1);//&&
						AfxMessageBox(m_strLastErrorString);
						return FALSE;
					}

					if( pStepData->m_fEndDI <= 0 && pStepData->m_fEndDI <= 0 && pStepData->m_fEndW <= 0 &&
						pStepData->m_fEndDV <=0 && pStepData->m_fEndI <=0 && pStepData->m_fEndW <= 0 &&
						pStepData->m_fEndV_H <=fMinVoltage && pStepData->m_fEndV_L <=fMinVoltage && pStepData->m_fEndTime == 0)			
					{
						//m_strLastErrorString.Format("Step %d의 종료 조건이 설정되어 있지 않았습니다.", k+1);			
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg28"), k+1);			//&&
						AfxMessageBox(m_strLastErrorString);
						return FALSE;
					}
				}

				if(pStepData->m_type == PS_STEP_CHARGE)		{//$1013
					if(pStepData->m_fVref_Charge <= fMinVoltage)		{
						//m_strLastErrorString.Format("Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.	(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						//m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg29"), k+1);		//&&
						//Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.(범위:%fmV) 스케줄 파일을 에디터 프로그램에서 확인하세요.
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg29","CTSMonPro_DOC"), k+1, fMinVoltage); //lyj 20200701
						AfxMessageBox(m_strLastErrorString);
						return FALSE;
					}
					if(pStepData->m_fVref_Charge <= pStepData->m_fEndDV)		{//$1013
						//m_strLastErrorString.Format("Step %d의 전압변화 설정값이 너무 큽니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg30"), k+1);			//&&
						AfxMessageBox(m_strLastErrorString);
						return FALSE;				
					}
					
					if(pStepData->m_fVref_Charge <= pStepData->m_fEndV_H)			{//$1013
						//m_strLastErrorString.Format("Step %d의 종료 전압이 설정 전압보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg31"), k+1);			//&&
						AfxMessageBox(m_strLastErrorString);
						return FALSE;
					}
				} //charge
					
				if(pStepData->m_type == PS_STEP_DISCHARGE)			{//$1013
					if(pStepData->m_fVref_DisCharge < fMinVoltage && pStepData->m_fEndV_L < fMinVoltage)		{
							//m_strLastErrorString.Format("Step %d의 전압 설정값이 종료 전압보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							//m_strLastErrorString.Format("Voltage step(%d) setting higher than the end of the voltage. Check on the program schedule a file editor.", k+1);
							m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg56"), k+1);//&&
							AfxMessageBox(m_strLastErrorString);
							return FALSE;
						}
				//	}
				} //Discharge
//$1013
				if(pStepData->m_fTref == 0.0f && (pStepData->m_fStartT !=0.0f || pStepData->m_fEndT != 0.0f))		{
					//m_strLastErrorString.Format("Step %d의 온도 설정값을 입력하지 않고 시작온도나 종료온도가 설정되었습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg32"), k+1);			//&&
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
					
				//+2015.9.22 USY Mod For PatternCV(김재호K)
				if((pStepData->m_mode != PS_MODE_CV) && (pStepData->m_type != PS_STEP_PATTERN))
				{//$1013
					if(  pStepData->m_fVref_Charge < fMinVoltage)		{
						//m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						//m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg33"), k+1);//&&
						//Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:%fmV) 스케줄 파일을 에디터 프로그램에서 확인하세요.
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg33","CTSMonPro_DOC"), k+1 , fMinVoltage); //lyj 20200701
						AfxMessageBox(m_strLastErrorString);
						return FALSE;
					}//$1013
					if(  pStepData->m_fVref_DisCharge < fMinVoltage)		{
						//m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						//m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg34"), k+1);//&&
						//Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:%fmV) 스케줄 파일을 에디터 프로그램에서 확인하세요.
						m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg34","CTSMonPro_DOC"), k+1 , fMinVoltage); //lyj 20200701
						AfxMessageBox(m_strLastErrorString);
						return FALSE;
					}
				}
				//전압 설정값 입력 범위 검사
// 				if(  pStepData->m_fVref_Charge < 0.0f)		{
// 					m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 					AfxMessageBox(m_strLastErrorString);
// 					return FALSE;
// 				}
// 				if(  pStepData->m_fVref_DisCharge < 0.0f)		{
// 					m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 					AfxMessageBox(m_strLastErrorString);
// 					return FALSE;
// 				}
				//-
				//전압 종료값 입력 범위 검사
				if(pStepData->m_fEndV_H < fMinVoltage)					{//$1013
					//m_strLastErrorString.Format("Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					//m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg35"), k+1);//&&
					//Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:%fmV) 스케줄 파일을 에디터 프로그램에서 확인하세요.
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg35","CTSMonPro_DOC"), k+1 , fMinVoltage);//lyj 20200701
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
				//전압 종료값 입력 범위 검사
				if(pStepData->m_fEndV_L < fMinVoltage)					{//$1013
					//m_strLastErrorString.Format("Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					//m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg36"), k+1);//&&
					//Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:%fmV) 스케줄 파일을 에디터 프로그램에서 확인하세요.
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg36","CTSMonPro_DOC"), k+1 , fMinVoltage); //lyj 20200701
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
				//전류 종료값 입력 범위 검사
				if(pStepData->m_fEndI < 0.0f)					{//$1013
					//m_strLastErrorString.Format("Step %d의 종료 전류값이 입력 범위를 벗어났습니다.(범위:0mA) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg37"), k+1);//&&	
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}

				//전압 제한값 입력 범위 검사
				if(pStepData->m_fLowLimitV < 0)				{//$1013
					//m_strLastErrorString.Format("Step %d의 안전 전압 상하한값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg38"), k+1);//&&
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
				
				//전압 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitV > 0.0f && pStepData->m_fLowLimitV > 0.0f && pStepData->m_fHighLimitV < pStepData->m_fLowLimitV)			{
					//m_strLastErrorString.Format("Step %d의 안전 전압 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg39"), k+1);//&&
					AfxMessageBox(m_strLastErrorString);//$1013
					return FALSE;
				}

				//전류 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitI > 0 && pStepData->m_fLowLimitI > 0 && pStepData->m_fHighLimitI < pStepData->m_fLowLimitI)				{
					//m_strLastErrorString.Format("Step %d의 안전 전류 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg40"), k+1);//&&
					AfxMessageBox(m_strLastErrorString);//$1013
					return FALSE;
				}

				//온도 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitTemp > 0 && pStepData->m_fLowLimitTemp > 0 && pStepData->m_fHighLimitTemp < pStepData->m_fLowLimitTemp)						{
					//m_strLastErrorString.Format("Step %d의 안전 온도 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg41"), k+1);//&&
					AfxMessageBox(m_strLastErrorString);//$1013
					return FALSE;
				}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				stepCon = GetNetworkFormatStep(pStepData);

				//stepCon_v7 = GetNetworkFormatStep_v7(pStepData);
				//2014.09.11 PS_STEP_USER_MAP 추가.
				if (stepCon.stepHeader.nStepTypeID == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
				{
					stepCon.bPatternTime = arryTimeType.GetAt(k);
					//stepCon.bPatternType = arryModeType.GetAt(k);
					//stepCon.bPatternMode = arryPlusMode.GetAt(k);
					lMaxValue = arryMaxValue.GetAt(k);

					stepCon.fPatternMaxValue = lMaxValue;
					stepCon.lPatternFileSize = arryFileSize.GetAt(k);		//v1007
					stepCon.lPatternChecksum = arryCheckSum.GetAt(k);		//v1007
				}
				
				stepCon.bCanRxEndNoCheck	= pStepData->m_nNoCheckMode;	//20150825 CAN RX 안전조건, 종료조건 무시	(2014.12.08 이민규 대리 추가)
				stepCon.bCanTxOffMode		= pStepData->m_nCanTxOffMode;	//20150825 CAN TX 안함						(2014.12.08 이민규 대리 추가)
				stepCon.bStepCanCheckMode	= pStepData->m_nCanCheckMode;	//ljb 20150825 add

				//yulee 20190114
				//ljb 20190114 add s 셀밸런스 값이 있으면 SBC로 내리는  Flag를 1로 변경 한다. 
				if (pStepData->m_nCellBal_CircuitRes > 0)
				{
					stepCon.bUseCellBalancing   = TRUE;
				}
				//ljb 20190114 add e 칠러 명령어값이 있으면 SBC로 내리는  Flag를 1로 변경 한다. 

				if(pStepData->m_nChiller_Cmd == 1) //yulee 20190114
				{
					bUseChiller = TRUE;
				}

				//SFT_STEP_CONDITION stepCon;	저장	
				fsize = rltData.SeekToEnd();
				
				if(rltData.m_hFile != NULL)
				{
					rltData.Write(&stepCon, sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상
					rltData.Flush();
				}

				//ljb 20160427 waitTimeGoto write
				if (nWaitTimeGotoCnt > 0)
				{
// 					if (pStepData->m_nWaitTimeInit > 0 || pStepData->m_nWaitTimeDay > 0 
// 						|| pStepData->m_nWaitTimeHour > 0 || pStepData->m_nWaitTimeMin > 0  || pStepData->m_nWaitTimeSec > 0)
// 					{
						//SFT_STEP_CONDITION stepCon;	저장
						ZeroMemory(&stepConWaitTime, sizeof(SFT_STEP_CONDITION_WAIT_TIME));
						stepConWaitTime.nStepNo		= pStepData->m_StepIndex+1;
						stepConWaitTime.byTimieInit = pStepData->m_nWaitTimeInit;
						stepConWaitTime.nWaitDay	= pStepData->m_nWaitTimeDay;
						stepConWaitTime.nWaitHour	= pStepData->m_nWaitTimeHour;
						stepConWaitTime.nWaitMin	= pStepData->m_nWaitTimeMin;
						stepConWaitTime.nWaitSec	= pStepData->m_nWaitTimeSec;

						fsize2 = rltData2.SeekToEnd();
						
						if(rltData2.m_hFile != NULL)
						{
							rltData2.Write(&stepConWaitTime, sizeof(SFT_STEP_CONDITION_WAIT_TIME));
							rltData2.Flush();
						}
//					}
				}

			}

			rltData.Flush();
			rltData.Close();
			
			if (nWaitTimeGotoCnt > 0)
			{
				rltData2.Flush();
				rltData2.Close();
			}
			
			//strMsg.Format("%s 파일 생성 완료", strFileName);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg42"), strFileName);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
///////////////////////////////////////스케줄 시작 정보 저장 종료////////////////////////////////////////////////////////
		}
	}

	if(bUseChiller == TRUE) //yulee 20190114
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 1);
	}
	else
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 0);
	}

	//5. sch 용량, checksum
	if(Fun_MemoryCopyFileAndCheckSum(strFileName, "", lFileSize, lCheckSum) == FALSE)
	{//$1013
		//strMsg.Format("sbc로 전송하는 schedule File Size(%d), checksum(%d) 실패!!!(%s)",lFileSize, lCheckSum, strFileName);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg43"),lFileSize, lCheckSum, strFileName);//&&
		AfxMessageBox(strMsg);
		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}//$1013
	//strMsg.Format("sbc로 전송하는 schedule File Size(%d), checksum(%d) 성공!!!\n(%s)",lFileSize, lCheckSum, strFileName);
	strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg44"),lFileSize, lCheckSum, strFileName);//&&
	WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);

	//ljb 20160427 wait time goto sch 용량, checksum
	if (nWaitTimeGotoCnt > 0)
	{
		if(Fun_MemoryCopyFileAndCheckSum(strFileName2, "", lFileSize2, lCheckSum2) == FALSE)
		{
			//strMsg.Format("sbc로 전송하는 wait time goto schedule File Size(%d), checksum(%d) 실패!!!(%s)",lFileSize2, lCheckSum2, strFileName2);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg45"),lFileSize2, lCheckSum2, strFileName2);//&&
			AfxMessageBox(strMsg);
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		//strMsg.Format("sbc로 전송하는 wait time goto schedule File Size(%d), checksum(%d) 성공!!!\n(%s)",lFileSize2, lCheckSum2, strFileName2);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg46"),lFileSize2, lCheckSum2, strFileName2);//&&
		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
	}

	//Fun_SaveFileSBCbinary(strFileName);	//ljb 20130509 임시로 binary 저장

	SFT_STEP_END_BODY steEnd;
	ZeroMemory(&steEnd, sizeof(SFT_STEP_END_BODY));
	steEnd.lTestCond_File_Size = lFileSize;
	steEnd.lTestCond_File_CheckSum = lCheckSum;
	steEnd.lWaitTimeGoto_File_Size = lFileSize2;	//ljb 20160427 add
	steEnd.lWaitTimeGoto_CheckSum = lCheckSum2;		//ljb 20160427 add


	//pProgressWnd->SetText(_T("스케줄을 전송중입니다."));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg47"));//&&
	pProgressWnd->SetPos(80);


	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 FTP로 전송 시작 (Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg48"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	WriteSysLog(strTimeCheck);
	//6. FTP로 SBC용 스케쥴 파일 전송 (채널별)
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pChInfo)
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
			{
				//ksj 20171027 : 주석처리
				if (pChInfo->GetChannelIndex() < 1)
				{
					strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
				}
				else if (pChInfo->GetChannelIndex() == 1)
				{
					strChangeDir.Format("/START_INFO/CH%03d",3);	//ljb 20170629 add
				}
			}
//#else
else
			{
						//ksj 20171027 : 원복
				strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
			}
//#endif
			if (Fun_FtpChangeDir(strChangeDir) == TRUE)
			{
				strFileName = pChInfo->GetScheduleFileForSbc();
			
				//delete and write Directory point Fun_FtpPutFile() 
				if (Fun_FtpPutFile(strFileName) == TRUE)
				{
					//7. FTP로 SBC용 Pattern File 업로드 (채널별) /////////////////////////////////////////////
					for (k=0; k<arryStrPatternFile.GetSize(); k++)		{
						strFileName = arryStrPatternFile.GetAt(k);
						if (strFileName.IsEmpty()) continue;
						if (Fun_FtpPutFile(strFileName) == TRUE)	{

						}else	{
							strMsg.Format("Pattern File Upload fail (%s)",strFileName);
							WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
							AfxMessageBox(strMsg);
							return FALSE;
						}
					}
					////////////////////////////////////////////////////////////////////////////////////////////
				}else		{
					//strMsg.Format("sch 파일 업로드 실패.(%s)",strChangeDir);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg49"),strChangeDir);//&&
					WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
					AfxMessageBox(strMsg);
					return FALSE;
				}

				//ljb 20160427 waitTimeGoto write
				if (nWaitTimeGotoCnt > 0)
				{
					strFileName2 = pChInfo->GetScheduleFileForSbc2();	//ljb 20160427 add wait time goto sch upload
					if (Fun_FtpPutFile(strFileName2) == TRUE)
					{//$1013
						//strTimeCheck.Format("Wait Time goto 스케쥴 FTP로 전송 시작 (Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
						strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg50"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
						WriteSysLog(strTimeCheck);
					}
					else
					{
						strMsg.Format("wait time goto sch File Upload fail (%s)",strFileName2);
						WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
						AfxMessageBox(strMsg);
						return FALSE;
					}
				}
			}
		}
		else
		{//$1013
			//strMsg.Format("sch 업로드 pChInfo 이상.(ch : %d)",pChInfo->GetChannelIndex()+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg51"),pChInfo->GetChannelIndex()+1);//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
			AfxMessageBox(strMsg);
			return FALSE;			
		}

		nPos = 100 - (100/(ch+1));
		//pProgressWnd->SetText(_T("전송 중입니다."));//$1013
		pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg52"));//&&
		pProgressWnd->SetPos(nPos);
	}
	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 FTP로 전송 완료 (Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg53"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	WriteSysLog(strTimeCheck);

//			2013-09-13			bung			:			resend
 	resteEnd		= steEnd;
 	renModuleID		= nModuleID;

//			2013-11-28			bung			:			startChInfo != endChInfo   
	if(pDump_SelChArray != pSelChArray)	{
		pSelChArray = pDump_SelChArray;
	} 
//			2013-10-21			bung			:			Send_CMD_SCHEDULE_END 

	long lWaitTime = 0;//yulee 20190625
	if(nPatternCount > 0)
		lWaitTime = nPatternCount*2000;

	strTimeCheck.Format("패턴 파일 개수 : %d, Wait Time : %d", nPatternCount, lWaitTime);
	WriteSysLog(strTimeCheck);

	
 	Send_CMD_SCHEDULE_END(pMD,pSelChArray,steEnd,nModuleID, lWaitTime);

	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 전송 종료 (SendScheduleToModuleFTP :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg54"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	WriteSysLog(strTimeCheck);

#endif
	return nRet;
}

void CCTSMonProDoc::Wait_Milli(DWORD dwMillisecond) //yulee 20181026
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();
	
	while(GetTickCount() - dwStart < dwMillisecond)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return;
}

//20180427 yulee 예약시작 시 패턴 path 가져오기에 대한 수정
int CCTSMonProDoc::GetPattPathFromDB(int ReserveID, int nStepNo, CString &strPattPath)
{
	CDaoDatabase db_Reserve;

	CString DBReservePath = GetDataBaseName();
	int nPos = DBReservePath.ReverseFind('\\');
	CString strPath = DBReservePath.Left(nPos);
	DBReservePath.Format("%s\\%s",strPath,"Work_Reservation.mdb");

	if(DBReservePath.IsEmpty())		return 0;
	try
	{
		db_Reserve.Open(DBReservePath);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	
	CString strQuery;

	strQuery.Format("SELECT * FROM Work_Reservation WHERE ID = %d", ReserveID);
	COleVariant data;
	CDaoRecordset rs(&db_Reserve);
	try
	{
		rs.Open(dbOpenSnapshot, strQuery, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	int nModelPK, nTestPK;

	strPattPath = _T("");
	
	int count = 0;
	char seps1[] = " ";
	char seps2[] = ",";
	char *token;
	char szColl[20];
	
	while(!rs.IsEOF())
	{
		rs.GetFieldValue("Scheduleinfo_modelpk",data);		//long	m_StepID;	
		nModelPK = data.lVal;
		rs.GetFieldValue("Scheduleinfo_testpk",data);		//long	m_StepID;	
		nTestPK = data.lVal;

		rs.MoveNext();
	}
	
	rs.Close();


	CDaoDatabase db;
	if(m_strDataBaseName.IsEmpty())		return 0;
	
	try
	{
		db.Open(m_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}


	strQuery.Format("SELECT * FROM Step WHERE TestID = %d ORDER BY StepNo", nTestPK);
	CDaoRecordset rs_StepMDB(&db);
	try
	{
		rs_StepMDB.Open(dbOpenSnapshot, strQuery, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	
	
	CStep *pStepData;
	//CString strPattPath;
	strPattPath = _T("");
		
	while(!rs_StepMDB.IsEOF())
	{
		pStepData = new CStep;
		ASSERT(pStepData);
		rs_StepMDB.GetFieldValue("StepID",data);		//long	m_StepID;	
		pStepData->m_StepID = data.lVal;
		rs_StepMDB.GetFieldValue("StepNo",data);		//long	m_StepNo;
		pStepData->m_StepIndex = BYTE(data.lVal-1);
		rs_StepMDB.GetFieldValue("StepType",data);		//long	m_StepType;
		pStepData->m_type = (BYTE)data.lVal;

		//2014.09.04 패턴 파일과 usermap 파일 이름을 구분하여 저장한다.
		//2014.09.15 mdb Value10으로 만들었지만 다시 Value5를 같이 사용하도록 변경.	
		rs_StepMDB.GetFieldValue("Value5",data);		// Value 5		
		
		if((pStepData->m_StepIndex == nStepNo) && (pStepData->m_type == PS_STEP_PATTERN))
		{
			if(VT_NULL == data.vt)
			{
				strPattPath = "";	//2014.09.15 패턴과 유저맵 경로를 동일하게 사용한다.			
			}
			else{
				strPattPath.Format("%s", data.pcVal); //20180502 yulee 일반시작 시 패턴 path 가져오기 			
			}
		}
		rs_StepMDB.MoveNext();
	}
	
	rs_StepMDB.Close();

	return 1;
	
}
//Update Schedule and Start(Real time procedure)
BOOL CCTSMonProDoc::SendScheduleToModuleFTP2(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd)
{
	ASSERT(pSelChArray);
	ASSERT(pSchData);
	BOOL nRet = FALSE;

	//UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",0); 
	UINT SchFileVer = (UINT)AfxGetApp()->GetProfileInt("Config", "Sch File Version",SCHEDULE_FILE_VER); //ksj 20200825
	if (SchFileVer == SCHEDULE_FILE_VER_v1015_v1)
	{
		CSendToFTP_v1015_v1 sendSch;
		nRet = sendSch.SendSchFTP2_v1015_v1(nMaxModuleCnt, nModuleID, pSelChArray, pSchData, pProgressWnd, this);
	}
	else
	{
		CSendToFTP_v1014_v1_SCH sendSch;
		nRet = sendSch.SendSchFTP2_v1014_v1_SCH(nMaxModuleCnt, nModuleID, pSelChArray, pSchData, pProgressWnd, this);
	}

	return nRet;
}

//			2013-06-11			bung			:			logic	->		func
BOOL CCTSMonProDoc::Send_CMD_SCHEDULE_END(CCyclerModule *pMD, CWordArray *pSelChArray, SFT_STEP_END_BODY steEnd, unsigned int nModuleID, unsigned int nWaitTime )	{
	
	int nRtn;
	CString strMsg;
	
	if(nWaitTime > 0)
	{

// 		//if((nRtn = pMD->SendCommand2(SFT_CMD_SCHEDULE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY)),nWaitTime) == SFT_NACK)	{ //201912 LG 오창2공장 106호 15번 작업시작 안됨 이슈
// 		if((nRtn = pMD->SendCommand2(SFT_CMD_SCHEDULE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY)),nWaitTime) != SFT_ACK)	{ //ksj 20201215
// 		//if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY))) != SFT_ACK)	{//yulee 20191203
// 		//if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY))) == SFT_NACK)	{
// 			
// #ifdef _DEBUG
// #else
// 			strMsg.Format("%s 에 스케쥴 전송 완료 실패!!! SFT_CMD_SCHEDULE_END 실패nRtn:%d 1", GetModuleName(nModuleID),);
// 			//strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
// 			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);//$1013
// 			//AfxMessageBox("스케줄 전송을 완료하지 못 했습니다. 다시 스케줄을 전송하세요.");
// 			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg2","CTSMonPro_DOC"));//&&
// 			return FALSE;//$1013
// #endif
// 		}else		{
// 			//strMsg.Format("%s 에 스케쥴 전송 완료 성공 SFT_CMD_SCHEDULE_END 성공 ", GetModuleName(nModuleID));
// 			strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg3","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
// 			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);//$1013
// 			return TRUE;
// 		}
		if((nRtn = pMD->SendCommand2(SFT_CMD_SCHEDULE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY),nWaitTime)) == SFT_ACK)	
		{ //ksj 20201215
			//strMsg.Format("%s 에 스케쥴 전송 완료 성공 SFT_CMD_SCHEDULE_END 성공 ", GetModuleName(nModuleID));
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg3","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);//$1013
			return TRUE;
		}else{
			strMsg.Format("%s 에 스케쥴 전송 완료 실패!!! SFT_CMD_SCHEDULE_END 실패nRtn:%d 1", GetModuleName(nModuleID),nRtn);
			//strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);//$1013
			//AfxMessageBox("스케줄 전송을 완료하지 못 했습니다. 다시 스케줄을 전송하세요.");
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg2","CTSMonPro_DOC"));//&&
			return FALSE;//$1013
		}
	}
	else
	{

		//if((nRtn = pMD->SendCommand2(SFT_CMD_SCHEDULE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY)),nWaitTime) != SFT_ACK)	{
		if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY))) != SFT_ACK)	{
			strMsg.Format("%s 에 스케쥴 전송 완료 실패!!! SFT_CMD_SCHEDULE_END 실패nRtn:%d 2", GetModuleName(nModuleID),nRtn);
			//strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);//$1013
			//AfxMessageBox("스케줄 전송을 완료하지 못 했습니다. 다시 스케줄을 전송하세요.");
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg2","CTSMonPro_DOC"));//&&
			return FALSE;//$1013
		}else		{
			//strMsg.Format("%s 에 스케쥴 전송 완료 성공 SFT_CMD_SCHEDULE_END 성공 ", GetModuleName(nModuleID));
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END_msg3","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
			WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);//$1013
			return TRUE;
		}
	}

}
BOOL CCTSMonProDoc::Send_CMD_SCHEDULE_END2(CCyclerModule *pMD, CWordArray *pSelChArray, SFT_STEP_END_BODY steEnd, unsigned int nModuleID)	{
	
	int nRtn;
	CString strMsg;
	
	if((nRtn = pMD->SendCommand(SFT_CMD_TESTCOND_UPDATE_END, pSelChArray, &steEnd, sizeof(SFT_STEP_END_BODY))) != SFT_ACK)	{
		//strMsg.Format("%s 에 스케쥴 업데이트 전송 완료 실패!!! SFT_CMD_SCHEDULE_END 실패 ", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END2_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);//$1013
		//AfxMessageBox("스케줄 전송을 완료하지 못 했습니다. 다시 스케줄을 전송하세요.");
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END2_msg2","CTSMonPro_DOC"));//&&
		return FALSE;//$1013
	}else		{
		//strMsg.Format("%s 에 스케쥴 업데이트 전송 완료 성공 SFT_CMD_SCHEDULE_END 성공 ", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_Send_CMD_SCHEDULE_END2_msg3","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
		WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);//$1013
		return TRUE;
	}
}



//////////////////////////////////////////////////////////////////////////
// Run 명령이 같이 전송되는 채널들은 언제나 같은 시험조건으로 시험한다.
// => Run명령 하달시 반드시 시험조건을 새롭게 선택 하도록 한다.
// => 다른 조건이 할당된 채널들을 동시에 Run 시킬 수는 없다.
//////////////////////////////////////////////////////////////////////////
BOOL CCTSMonProDoc::SendStartCmd(CProgressWnd * pProgWnd, CDWordArray *pSelChArray, CScheduleData *pSchData, int nStartCycleNo, int nStartStepNo, int nStartOptChamber, int nOption, int ReserveID)
{
	CString strTemp;
	DWORD dwData;
	WORD nModuleID, nChannelIndex;
	CWordArray awSelCh;
	int nPrevModule = 0;
	int nCurModuleID = 0;
	int nMaxModuleID = 1;	//ljb 20130404 add Progress bar 위치 계산을 위해 추가
	TRACE("Selected Ch %d\n", pSelChArray->GetSize());
	//int nSelCount = pSelChArray->GetSize();

	int i,nPos;
	for(int i = 0; i<pSelChArray->GetSize(); i++)
	{
		dwData = pSelChArray->GetAt(i);
		nModuleID = HIWORD(dwData);
		if(i == 0)	nPrevModule = nModuleID;
		if(nPrevModule != nModuleID)
		{
			nMaxModuleID++;	//ljb 20130404 add 모듈 갯수 카운트
			nPrevModule = nModuleID;
		}
	}

	for(int i = 0; i<pSelChArray->GetSize(); i++)
	{
		dwData = pSelChArray->GetAt(i);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		if(i == 0)	nPrevModule = nModuleID;

		TRACE("Channel Add M%d, CH %d\n", nModuleID, nChannelIndex+1);
		if(nPrevModule != nModuleID)
		{
			nCurModuleID = nPrevModule;			
			
			CCyclerModule	*pMD	=	GetModuleInfo(nModuleID);
			{
//			2013-05-27			bung			:			TT TT
				//////////////////////////////////////////////////////////////////////////
				//strTemp.Format("%s 로 스케쥴 전송 중...", GetModuleName(nCurModuleID));
				strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg9","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&
				pProgWnd->SetText(strTemp);//$1013
				nPos = 100 - (100  / nCurModuleID);
				pProgWnd->SetPos(nPos);
				//////////////////////////////////////////////////////////////////////////

//			2013-09-13			bung			:			resend
				reawSelCh.Copy(awSelCh);
				
				if(SendScheduleToModuleFTP(nMaxModuleID, nCurModuleID, &awSelCh, pSchData,pProgWnd, nOption, ReserveID) == FALSE)			{//v1007 버전
					Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
				Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료
			}

			//챔버연동 Command는 Schedule 전송 후 내려야 한다.
			CWordArray copyselCh;
			CWordArray copyselChChamber;
			copyselCh.Copy(awSelCh);
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
			{
				for (int jj=0; jj < copyselCh.GetSize(); jj++)	//ljb 20190201 add //병렬 mark
				{
					WORD dTemp;
					WORD dData = copyselCh.GetAt(jj);
					if (dData == 0)
					{
						copyselChChamber.Add(0);
						copyselChChamber.Add(1);
					}
					if (dData == 1) 
					{
						copyselChChamber.Add(2);
						copyselChChamber.Add(3);
					}
				}
			
				//ljb 20150609 add 병렬모드 챔버 연동 수정
				CCyclerModule *pModule;
				CCyclerChannel *pChannel;
				pModule	= GetModuleInfo(nCurModuleID);
				int nTotch = pModule->GetTotalChannel();
				int nStartCh = awSelCh.GetAt(0)+1;
				for(WORD ch =nStartCh; ch < nTotch; ch++)
				{
					pChannel = GetChannelInfo(nCurModuleID, ch);
					if(pChannel->IsParallel() && pChannel->m_bMaster == FALSE)
					{
						copyselCh.Add(ch);
						copyselChChamber.Add(2);
						copyselChChamber.Add(3);
					}
				}
			
				if (nStartOptChamber == 1)	//Fix 모드
				{
					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, FALSE, 0);
					Sleep(100);
				}
				else if (nStartOptChamber == 2)	//Program 모드
				{
					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, TRUE, 0);
					Sleep(100);
				}
				else if (nStartOptChamber == 3)	//스케쥴 연동
				{
					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, TRUE, 0);
					Sleep(100);
				}
				else
				{

					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, FALSE, 0);
					Sleep(100);
				}
			}
//#else
			else
			{	
				//ljb 20150609 add 병렬모드 챔버 연동 수정
				CCyclerModule *pModule;
				CCyclerChannel *pChannel;
				pModule	= GetModuleInfo(nCurModuleID);
				int nTotch = pModule->GetTotalChannel();
				int nStartCh = awSelCh.GetAt(0)+1;
				for(WORD ch =nStartCh; ch < nTotch; ch++)
				{
					pChannel = GetChannelInfo(nCurModuleID, ch);
					if(pChannel->m_bMaster == TRUE)//yulee 20190830_1 Master가 두 개-> 병렬이 두개 일 때 앞에 병렬만 설정되도록 수정 뒤에 마스터 만나면 break;
						break;
					if(pChannel->IsParallel() && pChannel->m_bMaster == FALSE)
					{
						copyselCh.Add(ch);
						copyselChChamber.Add(2);
						copyselChChamber.Add(3);
					}
				}
			
				if (nStartOptChamber == 1)	//Fix 모드
				{
					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, FALSE, 0);
					Sleep(100);
				}
				else if (nStartOptChamber == 2)	//Program 모드
				{
					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, TRUE, 0);
					Sleep(100);
				}
				else if (nStartOptChamber == 3)	//스케쥴 연동
				{
					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, TRUE, 0);
					Sleep(100);
				}
				else
				{

					SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, FALSE, 0);
					Sleep(100);
				}
			}
//#endif
			//2014.12.03 캔 통신 체크 모드
			CString RegName;
			RegName.Format("UseCanModeCheck%d%d",nModuleID, nChannelIndex + 1);	
			int nCanModeCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RegName, 0);
			// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지 ==========================================
			//SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CAN_MODE_CHECK, nCanModeCheck, 0);
			int nOutCableCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseOutCableCheck", 0);
			SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CAN_MODE_CHECK, nCanModeCheck, nOutCableCheck);
			// ===========================================================================================
			
			//RegName.Format("UseCanModeCheck%d%d",nModuleID, nChannelIndex + 1);	

			//ljb 20151111 add
			BOOL bMuxLink = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Mux Link Option", 0);
			if (bMuxLink)
			{
				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, TRUE, 0);
			}
			else
			{
				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, FALSE, 0);
			}


			///////ljb 20130514 add SBC에서 준비 완료 메시지를 대기 한다.
			//Dialog 초기화를 위해 새로 생성한다.
			//strTemp.Format("%s 에서 작업 준비 중...", GetModuleName(nCurModuleID));
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg10","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&	
			pProgWnd->SetText(strTemp);
			//nPos = 100 - (100  / nCurModuleID);
			pProgWnd->SetPos(100);


			switch(nCurModuleID)	{
				case	1		:	{
					if(m_pAlarmReadyDlg1)	{
						delete m_pAlarmReadyDlg1;
						m_pAlarmReadyDlg1 = NULL;
					}
					
					if(m_pAlarmReadyDlg1 == NULL) 	{
						m_pAlarmReadyDlg1 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg1->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg1","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg1->Fun_SetTxtMessage(strTemp);
					if (m_pAlarmReadyDlg1->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo) == FALSE)		{
						//AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo"); 
						AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg11","CTSMonPro_DOC")); //&&
					}
					m_pAlarmReadyDlg1->ShowWindow(SW_SHOW);
				}break;
				case	2		:	{
					if(m_pAlarmReadyDlg2)		{
						delete m_pAlarmReadyDlg2;
						m_pAlarmReadyDlg2 = NULL;
					}
					
					if(m_pAlarmReadyDlg2 == NULL)		{
						m_pAlarmReadyDlg2 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg2->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg2","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg2->Fun_SetTxtMessage(strTemp);
					if (m_pAlarmReadyDlg2->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo) == FALSE)	{
						AfxMessageBox("FAIL : m_pAlarmReadyDlg2->Fun_SetRunInfo");
					}
					m_pAlarmReadyDlg2->ShowWindow(SW_SHOW);
				}break;
				case	3		:	{
					if(m_pAlarmReadyDlg3)		{
						delete m_pAlarmReadyDlg3;
						m_pAlarmReadyDlg3 = NULL;
					}
					
					if(m_pAlarmReadyDlg3 == NULL)		{
						m_pAlarmReadyDlg3 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg3->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg3","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg3->Fun_SetTxtMessage(strTemp);
					if (m_pAlarmReadyDlg3->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo) == FALSE)	{
						//AfxMessageBox("FAIL : m_pAlarmReadyDlg3->Fun_SetRunInfo");
						AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg11","CTSMonPro_DOC"));//&&
					}
					m_pAlarmReadyDlg3->ShowWindow(SW_SHOW);
				}break;
				default:	{	
					//AfxMessageBox("SendStartCmd :: don't have module info");
					AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg12","CTSMonPro_DOC"));//&&
				}break;
			}
			awSelCh.RemoveAll();
			Sleep(1000);	//ljb 20130905 add	모듈이 다를 경우 스케쥴 전송 대기
		}

		//
		awSelCh.Add(nChannelIndex);
		nPrevModule = nModuleID;

		if(i == pSelChArray->GetSize()-1)
		{
			nCurModuleID = nPrevModule;

			//////////////////////////////////////////////////////////////////////////
			//strTemp.Format("%s 로 스케쥴 전송 중...", GetModuleName(nCurModuleID));
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg4","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&
			pProgWnd->SetText(strTemp);
			//////////////////////////////////////////////////////////////////////////

//			2013-09-13			bung			:			resend
			reawSelCh.Copy(awSelCh);
			if(SendScheduleToModuleFTP(nMaxModuleID, nCurModuleID, &awSelCh, pSchData, pProgWnd, nOption, ReserveID) == FALSE)		{
				Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료
				return FALSE;
			}
			Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료

			//챔버연동 Command는 Schedule 전송 후 내려야 한다.
			CWordArray copyselCh;
			CWordArray copyselChChamber;
			copyselCh.Copy(awSelCh);

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
		if(g_AppInfo.iPType==1)
		{
			for (int jj=0; jj < copyselCh.GetSize(); jj++)	//ljb 20190201 add //병렬 mark
			{
				WORD dTemp;
				WORD dData = copyselCh.GetAt(jj);
				if (dData == 0)
				{
					copyselChChamber.Add(0);
					copyselChChamber.Add(1);
				}
				if (dData == 1) 
				{
					copyselChChamber.Add(2);
					copyselChChamber.Add(3);
				}
			}
			
			//ljb 20150609 add 병렬모드 챔버 연동 수정
			CCyclerModule *pModule;
			CCyclerChannel *pChannel;
			pModule	= GetModuleInfo(nCurModuleID);
			int nTotch = pModule->GetTotalChannel();
			int nStartCh = awSelCh.GetAt(0)+1;
			for(WORD ch =nStartCh; ch < nTotch; ch++)
			{
				pChannel = GetChannelInfo(nCurModuleID, ch);
				if(pChannel->IsParallel() && pChannel->m_bMaster == FALSE)
				{
					copyselCh.Add(ch);
					copyselChChamber.Add(2);
					copyselChChamber.Add(3);
				}
			}
			
			if (nStartOptChamber == 1)	//Fix 모드
			{
				SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, FALSE, 0);
				Sleep(100);
			}
			else if (nStartOptChamber == 2)	//Program 모드
			{
				SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, FALSE, 0);
				Sleep(100);
			}
			else if (nStartOptChamber == 3)	//스케쥴 연동
			{
				SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, TRUE, 0);
				Sleep(100);
			}
			else
			{
				SendCommand(nCurModuleID, &copyselChChamber, SFT_CMD_CHAMBER_USING, FALSE, 0);
				Sleep(100);
			}
		
		}
//#else
		else
		{
		//ljb 20150609 add 병렬모드 챔버 연동 수정 //yulee 20190830_1
			CCyclerModule *pModule;
			CCyclerChannel *pChannel;
			pModule	= GetModuleInfo(nCurModuleID);
			int nTotch = pModule->GetTotalChannel();
			int nStartCh = awSelCh.GetAt(0)+1;
			for(WORD ch =nStartCh; ch < nTotch; ch++)
			{
				pChannel = GetChannelInfo(nCurModuleID, ch);
				if(pChannel->m_bMaster == TRUE)//yulee 20190830_1 Master가 두 개-> 병렬이 두개 일 때 앞에 병렬만 설정되도록 수정 뒤에 마스터 만나면 break;
					break;
				if(pChannel->IsParallel() && pChannel->m_bMaster == FALSE)
				{
					copyselCh.Add(ch);
				}
			}
			
			if (nStartOptChamber == 1)	//Fix 모드
			{
				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, FALSE, 0);
				Sleep(100);
			}
			else if (nStartOptChamber == 2)	//Program 모드
			{
				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
				Sleep(100);
			}
			else if (nStartOptChamber == 3)	//스케쥴 연동
			{
				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
				Sleep(100);
			}
			else
			{
				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, FALSE, 0);
				Sleep(100);
			}
		}
//#endif


			//2014.12.03 캔 통신 체크 모드
			CString RegName;
			RegName.Format("UseCanModeCheck%d%d",nModuleID, nChannelIndex + 1);	
			int nCanModeCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RegName, 0);
			// 210809 HKH v1016 신성이엔지 파워케이블 조건 감지 ==========================================
			//SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CAN_MODE_CHECK, nCanModeCheck, 0);
			int nOutCableCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseOutCableCheck", 0);
			SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CAN_MODE_CHECK, nCanModeCheck, nOutCableCheck);
			// ===========================================================================================
			
			//ljb 20151111 add
			BOOL bMuxLink = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Mux Link Option", 0);
			if (bMuxLink)
			{
				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, TRUE, 0);
			}
			else
			{
				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, FALSE, 0);
			}
			///////ljb 20130514 add SBC에서 준비 완료 메시지를 대기 한다.
			//Dialog 초기화를 위해 새로 생성한다.
			//strTemp.Format("%s 에서 작업 준비 중...", GetModuleName(nCurModuleID));//$1013
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg5","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&
			pProgWnd->SetText(strTemp);
			//nPos = 100 - (100  / nCurModuleID);
			pProgWnd->SetPos(100);

//			2013-10-21			bung			:			m_pAlarmReadyDlg -> m_pAlarmReadyDlg[]
			switch(nCurModuleID)	{
				case	1	:	{
					if(m_pAlarmReadyDlg1)		{
						delete m_pAlarmReadyDlg1;
						m_pAlarmReadyDlg1 = NULL;
					}
					
					if(m_pAlarmReadyDlg1 == NULL)	{
						m_pAlarmReadyDlg1 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg1->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg6","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg1->Fun_SetTxtMessage(strTemp);
					
					if (m_pAlarmReadyDlg1->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo) == FALSE)	{
						AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo");
					}
				m_pAlarmReadyDlg1->ShowWindow(SW_SHOW);
				}break;
				case	2	:	{
					if(m_pAlarmReadyDlg2)		{
						delete m_pAlarmReadyDlg2;
						m_pAlarmReadyDlg2 = NULL;
					}
					
					if(m_pAlarmReadyDlg2 == NULL)	{
						m_pAlarmReadyDlg2 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg2->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg7","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg2->Fun_SetTxtMessage(strTemp);
					
					if (m_pAlarmReadyDlg2->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo) == FALSE)	{
						AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo");
					}
				m_pAlarmReadyDlg2->ShowWindow(SW_SHOW);
				}break;
				case	3	:	{
					if(m_pAlarmReadyDlg3)		{
						delete m_pAlarmReadyDlg3;
						m_pAlarmReadyDlg3 = NULL;
					}
					
					if(m_pAlarmReadyDlg3 == NULL)	{
						m_pAlarmReadyDlg3 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg3->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendStartCmd_msg8","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg3->Fun_SetTxtMessage(strTemp);
					
					if (m_pAlarmReadyDlg3->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo) == FALSE)	{
						AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo");
					}
				m_pAlarmReadyDlg3->ShowWindow(SW_SHOW);
				}break;
			}
		}
// 		strTemp.Format("%s : Channel %d -> 작업시작 전송 하였습니다.", GetModuleName(nCurModuleID),nChannelIndex+1);
// 		WriteLogTrouble(GetModuleName(nCurModuleID),strTemp);
	}

	return TRUE;

//ljb 20140207 기존 스케쥴 전송 -> 주석처리
// 	for(int i=0; i<pSelChArray->GetSize(); i++)
// 	{
// 		dwData = pSelChArray->GetAt(i);
// 		nModuleID = HIWORD(dwData);
// 		nChannelIndex = LOWORD(dwData);
// 		if(i == 0)	nPrevModule = nModuleID;		
// 		TRACE("Channel Add M%d, CH %d\n", nModuleID, nChannelIndex+1);
// 
// 		//Cancel 처리
// 		if(pProgWnd->PeekAndPump() == FALSE)
// 		{
// 			return FALSE;
// 		}
// 		
// 		if(nPrevModule != nModuleID)
// 		{
// 			nCurModuleID = nPrevModule;			
// 		
// 			if(SendScheduleToModule(nCurModuleID, &awSelCh, pSchData,pProgWnd) == FALSE)
// 			{
// 				return FALSE;
// 			}
// 			//챔버연동 Command는 Schedule 전송 후 내려야 한다.
// 			if (nStartOptChamber == 3)
// 			{
// 				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
// 				Sleep(100);
// 			}
// 			
// 			
// 			if(SendCommand(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo))
// 			{
// 				//작업 시작 정보를 저장한다.(Pause 상태에서 연속작업이 가능하도록...)
// 				if(SaveRunInfoToTempFile(nCurModuleID, &awSelCh) == FALSE)
// 				{
// 					strTemp.Format("%s 작업시작 임시파일 정보저장을 실패 하였습니다.", GetModuleName(nCurModuleID));
// 					WriteSysLog(strTemp);
// 					return FALSE;
// 				}
// 			}
// 			else
// 			{
// 				//ljb 20110422
// 				strTemp.Format("%s 작업을 시작 할 수 없습니다.", GetModuleName(nCurModuleID));
// 				WriteSysLog(strTemp);
// 				AfxMessageBox(strTemp);
// 				return FALSE;
// 			}
// 
// 			awSelCh.RemoveAll();
// 		}
// 		awSelCh.Add(nChannelIndex);
// 		nPrevModule = nModuleID;
// 		
// 		if(i == pSelChArray->GetSize()-1)
// 		{
// 			nCurModuleID = nPrevModule;
// 			if(SendScheduleToModule(nCurModuleID, &awSelCh, pSchData,pProgWnd) == FALSE)
// 			{
// 				return FALSE;
// 			}
// 			//챔버연동 Command는 Schedule 전송 후 내려야 한다.
// 			if (nStartOptChamber == 3)
// 			{
// 				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
// 				Sleep(100);
// 			}
// 			if(SendCommand(nCurModuleID, &awSelCh, SFT_CMD_RUN, nStartCycleNo, nStartStepNo))
// 			{
// 				//작업 시작 정보를 저장한다.(Pause 상태에서 연속작업이 가능하도록...)
// 				if(SaveRunInfoToTempFile(nCurModuleID, &awSelCh) == FALSE)
// 				{
// 					strTemp.Format("%s 작업시작 임시파일 정보저장을 실패 하였습니다.", GetModuleName(nCurModuleID));
// 					WriteSysLog(strTemp);
// 					return FALSE;
// 				}
// 			}
// 			else
// 			{
// 				//ljb 20110422
// 				strTemp.Format("%s 작업을 시작 할 수 없습니다.", GetModuleName(nCurModuleID));
// 				WriteSysLog(strTemp);
// 				AfxMessageBox(strTemp);
// 				return FALSE;
// 			}
// 		}
// 		strTemp.Format("%s : Channel %d -> 작업시작 전송 하였습니다.", GetModuleName(nCurModuleID),nChannelIndex+1);
// 		WriteSysLog(strTemp);
// 	}
// 	return TRUE;
}


BOOL CCTSMonProDoc::SendSchUpdateCmd(CProgressWnd * pProgWnd, CDWordArray *pSelChArray, CScheduleData *pSchData)
{
	CString strTemp;
	DWORD dwData;
	WORD nModuleID, nChannelIndex;
	CWordArray awSelCh;
	int nPrevModule = 0;
	int nCurModuleID = 0;
	int nMaxModuleID = 1;	//ljb 20130404 add Progress bar 위치 계산을 위해 추가
	TRACE("Selected Ch %d\n", pSelChArray->GetSize());

	int i,nPos;
	for(int i = 0; i<pSelChArray->GetSize(); i++)
	{
		dwData = pSelChArray->GetAt(i);
		nModuleID = HIWORD(dwData);
		if(i == 0)	nPrevModule = nModuleID;
		if(nPrevModule != nModuleID)
		{
			nMaxModuleID++;	//ljb 20130404 add 모듈 갯수 카운트
			nPrevModule = nModuleID;
		}
	}

	for(int i = 0; i<pSelChArray->GetSize(); i++)
	{
		dwData = pSelChArray->GetAt(i);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		if(i == 0)	nPrevModule = nModuleID;

		TRACE("Channel Add M%d, CH %d\n", nModuleID, nChannelIndex+1);
		if(nPrevModule != nModuleID)
		{
			nCurModuleID = nPrevModule;			
			
			CCyclerModule	*pMD	=	GetModuleInfo(nModuleID);
			{
				//////////////////////////////////////////////////////////////////////////
				//strTemp.Format("%s 로 스케쥴 전송 중...", GetModuleName(nCurModuleID));//$1013
				strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg1","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&
				pProgWnd->SetText(strTemp);
				nPos = 100 - (100  / nCurModuleID);
				pProgWnd->SetPos(nPos);
				//////////////////////////////////////////////////////////////////////////

				reawSelCh.Copy(awSelCh);
				
				if(SendScheduleToModuleFTP2(nMaxModuleID, nCurModuleID, &awSelCh, pSchData,pProgWnd) == FALSE)			{//v1007 버전
					Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료
					AfxMessageBox(m_strLastErrorString);
					return FALSE;
				}
				Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료
			}

			//챔버연동 Command는 Schedule 전송 후 내려야 한다.
// 			CWordArray copyselCh;
// 			copyselCh.Copy(awSelCh);
			//ljb 20150609 add 병렬모드 챔버 연동 수정
// 			CCyclerModule *pModule;
// 			CCyclerChannel *pChannel;
// 			pModule	= GetModuleInfo(nCurModuleID);
// 			int nTotch = pModule->GetTotalChannel();
// 			int nStartCh = awSelCh.GetAt(0)+1;
// 			for(WORD ch =nStartCh; ch < nTotch; ch++)
// 			{
// 				pChannel = GetChannelInfo(nCurModuleID, ch);
// 				if(pChannel->IsParallel() && pChannel->m_bMaster == FALSE)
// 				{
// 					copyselCh.Add(ch);
// 				}
// 			}
			
// 			if (nStartOptChamber == 1)	//Fix 모드
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, FALSE, 0);
// 				Sleep(100);
// 			}
// 			else if (nStartOptChamber == 2)	//Program 모드
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
// 				Sleep(100);
// 			}
// 			else if (nStartOptChamber == 3)	//스케쥴 연동
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
// 				Sleep(100);
// 			}
// 			else
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, FALSE, 0);
// 				Sleep(100);
// 			}
			
			//2014.12.03 캔 통신 체크 모드
// 			CString RegName;
// 			RegName.Format("UseCanModeCheck%d%d",nModuleID, nChannelIndex + 1);	
// 			int nCanModeCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RegName, 0);
// 			SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CAN_MODE_CHECK, nCanModeCheck, 0);
// 			
// 			//RegName.Format("UseCanModeCheck%d%d",nModuleID, nChannelIndex + 1);	
// 
// 			//ljb 20151111 add
// 			BOOL bMuxLink = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Mux Link Option", 0);
// 			if (bMuxLink)
// 			{
// 				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, TRUE, 0);
// 			}
// 			else
// 			{
// 				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, FALSE, 0);
// 			}


			///////ljb 20130514 add SBC에서 준비 완료 메시지를 대기 한다.
			//Dialog 초기화를 위해 새로 생성한다.
			//strTemp.Format("%s 에서 작업 준비 중...", GetModuleName(nCurModuleID));//$1013
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg2","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&
			pProgWnd->SetText(strTemp);
			//nPos = 100 - (100  / nCurModuleID);
			pProgWnd->SetPos(100);


			switch(nCurModuleID)	{
				case	1		:	{
					if(m_pAlarmReadyDlg1)	{
						delete m_pAlarmReadyDlg1;
						m_pAlarmReadyDlg1 = NULL;
					}
					
					if(m_pAlarmReadyDlg1 == NULL) 	{
						m_pAlarmReadyDlg1 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg1->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg3","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg1->Fun_SetTxtMessage(strTemp);
					if (m_pAlarmReadyDlg1->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, 0, 0) == FALSE)		{
						//AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo");
						AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg8","CTSMonPro_DOC"));//&&
					}
					m_pAlarmReadyDlg1->ShowWindow(SW_SHOW);
				}break;
				case	2		:	{
					if(m_pAlarmReadyDlg2)		{
						delete m_pAlarmReadyDlg2;
						m_pAlarmReadyDlg2 = NULL;
					}
					
					if(m_pAlarmReadyDlg2 == NULL)		{
						m_pAlarmReadyDlg2 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg2->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);//$1013
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg4","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg2->Fun_SetTxtMessage(strTemp);
					if (m_pAlarmReadyDlg2->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, 0, 0) == FALSE)	{
						//AfxMessageBox("FAIL : m_pAlarmReadyDlg2->Fun_SetRunInfo");
						AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg9","CTSMonPro_DOC"));//&&
					}
					m_pAlarmReadyDlg2->ShowWindow(SW_SHOW);
				}break;
				case	3		:	{
					if(m_pAlarmReadyDlg3)		{
						delete m_pAlarmReadyDlg3;
						m_pAlarmReadyDlg3 = NULL;
					}
					
					if(m_pAlarmReadyDlg3 == NULL)		{
						m_pAlarmReadyDlg3 = new CAlarmReadyDlg(nCurModuleID, this);
						m_pAlarmReadyDlg3->Create(IDD_ALARM_MESSAGE_DLG, NULL);
					}
					//strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg5","CTSMonPro_DOC"),nCurModuleID);//&&
					m_pAlarmReadyDlg3->Fun_SetTxtMessage(strTemp);
					if (m_pAlarmReadyDlg3->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, 0, 0) == FALSE)	{
						//AfxMessageBox("FAIL : m_pAlarmReadyDlg3->Fun_SetRunInfo");
						AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg10","CTSMonPro_DOC"));//&&
					}
					m_pAlarmReadyDlg3->ShowWindow(SW_SHOW);
				}break;
				default:	{	
					//AfxMessageBox("SendStartCmd :: don't have module info");
					AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg11","CTSMonPro_DOC"));//&&
				}break;
			}
			awSelCh.RemoveAll();
			Sleep(1000);	//ljb 20130905 add	모듈이 다를 경우 스케쥴 전송 대기
		}

		//
		awSelCh.Add(nChannelIndex);
		nPrevModule = nModuleID;

		if(i == pSelChArray->GetSize()-1)
		{
			nCurModuleID = nPrevModule;

			//////////////////////////////////////////////////////////////////////////
			//strTemp.Format("%s 로 스케쥴 전송 중...", GetModuleName(nCurModuleID));
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg6","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&
			pProgWnd->SetText(strTemp);
			//////////////////////////////////////////////////////////////////////////

//			2013-09-13			bung			:			resend
			reawSelCh.Copy(awSelCh);
			if(SendScheduleToModuleFTP2(nMaxModuleID, nCurModuleID, &awSelCh, pSchData, pProgWnd) == FALSE)		{
				Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료
				return FALSE;
			}
			Fun_FtpDisConnect();	//ljb 20130905 FTP 접속 종료

			//챔버연동 Command는 Schedule 전송 후 내려야 한다.
			CWordArray copyselCh;
			copyselCh.Copy(awSelCh);
			//ljb 20150609 add 병렬모드 챔버 연동 수정
			CCyclerModule *pModule;
			CCyclerChannel *pChannel;
			pModule	= GetModuleInfo(nCurModuleID);
			int nTotch = pModule->GetTotalChannel();
			int nStartCh = awSelCh.GetAt(0)+1;
			for(WORD ch =nStartCh; ch < nTotch; ch++)
			{
				pChannel = GetChannelInfo(nCurModuleID, ch);
				if(pChannel->m_bMaster == TRUE)//yulee 20190830_1 Master가 두 개-> 병렬이 두개 일 때 앞에 병렬만 설정되도록 수정 뒤에 마스터 만나면 break;
					break;
				if(pChannel->IsParallel() && pChannel->m_bMaster == FALSE)
				{
					copyselCh.Add(ch);
				}
			}
			
// 			if (nStartOptChamber == 1)	//Fix 모드
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, FALSE, 0);
// 				Sleep(100);
// 			}
// 			else if (nStartOptChamber == 2)	//Program 모드
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
// 				Sleep(100);
// 			}
// 			else if (nStartOptChamber == 3)	//스케쥴 연동
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, TRUE, 0);
// 				Sleep(100);
// 			}
// 			else
// 			{
// 				SendCommand(nCurModuleID, &copyselCh, SFT_CMD_CHAMBER_USING, FALSE, 0);
// 				Sleep(100);
// 			}

			//2014.12.03 캔 통신 체크 모드
// 			CString RegName;
// 			RegName.Format("UseCanModeCheck%d%d",nModuleID, nChannelIndex + 1);	
// 			int nCanModeCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, RegName, 0);
// 			SendCommand(nCurModuleID, &awSelCh, SFT_CMD_CAN_MODE_CHECK, nCanModeCheck, 0);
			
			//ljb 20151111 add
// 			BOOL bMuxLink = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Mux Link Option", 0);
// 			if (bMuxLink)
// 			{
// 				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, TRUE, 0);
// 			}
// 			else
// 			{
// 				SendCommand(nCurModuleID, &awSelCh, SFT_CMD_MUX_MODE_CHECK, FALSE, 0);
// 			}
			///////ljb 20130514 add SBC에서 준비 완료 메시지를 대기 한다.
			//Dialog 초기화를 위해 새로 생성한다.
			//strTemp.Format("%s 에서 작업 준비 중...", GetModuleName(nCurModuleID));
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_SendSchUpdateCmd_msg7","CTSMonPro_DOC"), GetModuleName(nCurModuleID));//&&
			pProgWnd->SetText(strTemp);
			//nPos = 100 - (100  / nCurModuleID);
			pProgWnd->SetPos(100);

// 			switch(nCurModuleID)	{
// 				case	1	:	{
// 					if(m_pAlarmReadyDlg1)		{
// 						delete m_pAlarmReadyDlg1;
// 						m_pAlarmReadyDlg1 = NULL;
// 					}
// 					
// 					if(m_pAlarmReadyDlg1 == NULL)	{
// 						m_pAlarmReadyDlg1 = new CAlarmReadyDlg(nCurModuleID, this);
// 						m_pAlarmReadyDlg1->Create(IDD_ALARM_MESSAGE_DLG, NULL);
// 					}
// 					strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);
// 					m_pAlarmReadyDlg1->Fun_SetTxtMessage(strTemp);
// 					
// 					if (m_pAlarmReadyDlg1->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, 0, 0) == FALSE)	{
// 						AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo");
// 					}
// 				m_pAlarmReadyDlg1->ShowWindow(SW_SHOW);
// 				}break;
// 				case	2	:	{
// 					if(m_pAlarmReadyDlg2)		{
// 						delete m_pAlarmReadyDlg2;
// 						m_pAlarmReadyDlg2 = NULL;
// 					}
// 					
// 					if(m_pAlarmReadyDlg2 == NULL)	{
// 						m_pAlarmReadyDlg2 = new CAlarmReadyDlg(nCurModuleID, this);
// 						m_pAlarmReadyDlg2->Create(IDD_ALARM_MESSAGE_DLG, NULL);
// 					}
// 					strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);
// 					m_pAlarmReadyDlg2->Fun_SetTxtMessage(strTemp);
// 					
// 					if (m_pAlarmReadyDlg2->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, 0, 0) == FALSE)	{
// 						AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo");
// 					}
// 				m_pAlarmReadyDlg2->ShowWindow(SW_SHOW);
// 				}break;
// 				case	3	:	{
// 					if(m_pAlarmReadyDlg3)		{
// 						delete m_pAlarmReadyDlg3;
// 						m_pAlarmReadyDlg3 = NULL;
// 					}
// 					
// 					if(m_pAlarmReadyDlg3 == NULL)	{
// 						m_pAlarmReadyDlg3 = new CAlarmReadyDlg(nCurModuleID, this);
// 						m_pAlarmReadyDlg3->Create(IDD_ALARM_MESSAGE_DLG, NULL);
// 					}
// 					strTemp.Format("Module %d 에서 작업 준비중 입니다.",nCurModuleID);
// 					m_pAlarmReadyDlg3->Fun_SetTxtMessage(strTemp);
// 					
// 					if (m_pAlarmReadyDlg3->Fun_SetRunInfo(nCurModuleID, &awSelCh, SFT_CMD_RUN, 0, 0) == FALSE)	{
// 						AfxMessageBox("FAIL : m_pAlarmReadyDlg1->Fun_SetRunInfo");
// 					}
// 				m_pAlarmReadyDlg3->ShowWindow(SW_SHOW);
// 				}break;
// 			}
 		}//$1013
// 		strTemp.Format("%s : Channel %d -> 작업시작 전송 하였습니다.", GetModuleName(nCurModuleID),nChannelIndex+1);
// 		WriteLogTrouble(GetModuleName(nCurModuleID),strTemp);
	}
	return TRUE;
}


//Step Data를 Module에 전송하는 구조체로 변환 한다.
SFT_STEP_CONDITION_V2 CCTSMonProDoc::GetNetworkFormatStep(CStep *pStep) //lyj 20200214 LG v1015 미만
{
	SFT_STEP_CONDITION_V2	netStep; //lyj 20200214 LG v1015 미만
	ZeroMemory(&netStep, sizeof(SFT_STEP_CONDITION_V2)); //lyj 20200214 LG v1015 미만
	
	if(pStep != NULL)
	{
		netStep.stepHeader.nStepTypeID	= pStep->m_type;					//Step Type
//		netStep.stepHeader.nStepTypeID	= pStep->m_lProcType;
		netStep.stepHeader.stepNo		= (BYTE)(pStep->m_StepIndex+1);		//StepNo
		netStep.stepHeader.mode			= pStep->m_mode;
		netStep.stepHeader.nSubStep		= 1;								//1개의 Sub Step만 이용한다.2005/08/08
		netStep.stepHeader.bTestEnd		= 0;
//		netStep.stepHeader.bUseSocFlag = pStep->m_bUseActualCapa;
		netStep.stepHeader.bUseCyclePause = pStep->m_bUseCyclePause;		//Cycle 반복후 Pause flag //★★ ljb 201059  ★★★★★★★★

		netStep.stepReferance[0].lVref_Charge	= FLOAT2LONG(pStep->m_fVref_Charge);
		netStep.stepReferance[0].lVref_DisCharge	= FLOAT2LONG(pStep->m_fVref_DisCharge);
		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
		netStep.stepReferance[0].lRref	= FLOAT2LONG(pStep->m_fRref);

		//ljb 20150820 챔버연동 기능 (1: 챔버연동 대기, 0: 챔버연동 대기 없이 진행)
		if (pStep->m_fTref == 0)
			netStep.stepReferance[0].bChamberlinkOption	= 0;
		else
			netStep.stepReferance[0].bChamberlinkOption	= 1;

		//Power 제어값이 들어 있다. (Power 단위는 mW이므로 )
		//CR 제어값이 들어 있다. (CR 모드에서 R은 mOhm)
//		if( (pStep->m_mode == PS_MODE_CP || pStep->m_mode == PS_MODE_CR)&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE))
		//20081229 => Power 단위 변경 
		
// 		if(pStep->m_mode == PS_MODE_CP || pStep->m_mode == PS_MODE_CR)//&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE))
// 		{
//     		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
// 			TRACE("LONG: %ld\r\n", netStep.stepReferance[0].lPref);
// 		}
// 		//전류 단위는 uA단위
// 		else	
// 		{
//      		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
// 			TRACE("MODE : %x, LONG2: %ld\r\n", pStep->m_mode, netStep.stepReferance[0].lIref);
// 			
// 		}
		netStep.stepReferance[0].byRange	= (BYTE)pStep->m_lRange;	
		netStep.stepReferance[0].ulEndTimeDay	= pStep->m_lEndTimeDay;		//ljb 20131212 add
		netStep.stepReferance[0].ulEndTime	= MDTIME(pStep->m_fEndTime);
    	netStep.stepReferance[0].lEndV_H	= FLOAT2LONG(pStep->m_fEndV_H);		//ljb v1009
//		netStep.stepReferance[0].lEndV_L	= FLOAT2LONG(pStep->m_fEndV_L);		//ljb v1009
    	netStep.stepReferance[0].lEndI		= FLOAT2LONG(pStep->m_fEndI);
		netStep.stepReferance[0].ulCVTimeDay	= pStep->m_lCVTimeDay;		//ljb 20131212 add
		netStep.stepReferance[0].ulCVTime	= MDTIME(pStep->m_fCVTime);

#ifdef _EDLC_TEST_SYSTEM
    	netStep.stepReferance[0].lEndC		= long(pStep->m_fEndC);
#else
    	netStep.stepReferance[0].lEndC		= FLOAT2LONG(pStep->m_fEndC);
#endif
		if (pStep->m_type == PS_STEP_LOOP || pStep->m_type == PS_STEP_ADV_CYCLE )
		{
			if (pStep->m_type == PS_STEP_LOOP)			//ljb v1009  LOOP 일 경우
			{
				netStep.stepReferance[0].lGoto		= pStep->m_nLoopInfoGotoStep;	
				netStep.stepReferance[0].lCycleCount= pStep->m_nLoopInfoCycle;	
				netStep.stepReferance[0].lAccCycleCount = pStep->m_nAccLoopInfoCycle;	//20080903 kjh
				netStep.stepReferance[0].lAccCycleGroupID	= pStep->m_nAccLoopGroupID;
				
				netStep.stepReferance[0].lEndV_L	= pStep->m_nMultiLoopInfoCycle;
				netStep.stepReferance[0].bEndC_Goto	= pStep->m_nMultiLoopGroupID;
				netStep.stepReferance[0].bEndWattHour_Goto	= pStep->m_nMultiLoopInfoGotoStep;
				netStep.stepReferance[0].bAccCycleCount_Goto	= pStep->m_nAccLoopInfoGotoStep;
			}
			else		
			{												//ljb v1009  CYCLE 일 경우
				netStep.stepReferance[0].bEndC_Goto	= pStep->m_nMultiLoopGroupID;
			}
		}
		else
		{
			//ljb v1009  Step..Cycle 일 경우
			netStep.stepReferance[0].lGoto		= pStep->m_nGotoStepEndV_H;	
			netStep.stepReferance[0].lCycleCount= pStep->m_nGotoStepEndV_L;	
			netStep.stepReferance[0].lAccCycleCount = pStep->m_nGotoStepEndTime;
			netStep.stepReferance[0].lAccCycleGroupID	= pStep->m_nGotoStepEndCVTime;
			netStep.stepReferance[0].lEndV_L	= FLOAT2LONG(pStep->m_fEndV_L);
			netStep.stepReferance[0].bEndC_Goto	= pStep->m_nGotoStepEndC;
			netStep.stepReferance[0].bEndWattHour_Goto	= pStep->m_nGotoStepEndWh;
			netStep.stepReferance[0].bValueRate_Goto	= pStep->m_nGotoStepEndValue;
		}
		netStep.stepReferance[0].bValueItem = pStep->m_bValueItem;		//ljb v1009 Valu Item
		netStep.stepReferance[0].bValueStepNo = pStep->m_bValueStepNo;
		netStep.stepReferance[0].wValueRate	= pStep->m_fValueRate *100;
		netStep.stepReferance[0].bValueRate_Goto = pStep->m_nGotoStepEndValue;

    	netStep.stepReferance[0].lDeltaV	= FLOAT2LONG(pStep->m_fEndDV);
		netStep.stepReferance[0].lEndWatt   = LONG(pStep->m_fEndW);
		TRACE("lEndWatt: %ld\r\n", netStep.stepReferance[0].lEndWatt);
		netStep.stepReferance[0].lEndWattHour = LONG(pStep->m_fEndWh);
		TRACE("lEndWattHour: %ld\r\n", netStep.stepReferance[0].lEndWattHour);
		netStep.stepReferance[0].lReserved[0] = FLOAT2LONG(pStep->m_fEndDI);
//    	netStep.stepReferance[0].lEndData3	= FLOAT2LONG(pStep->m_fEndDI);

		netStep.lHighV	= FLOAT2LONG(pStep->m_fHighLimitV);
		netStep.lLowV	= FLOAT2LONG(pStep->m_fLowLimitV);
		netStep.lHighI	= FLOAT2LONG(pStep->m_fHighLimitI);
		netStep.lLowI	= FLOAT2LONG(pStep->m_fLowLimitI);
#ifdef _EDLC_TEST_SYSTEM
		netStep.lHighC	= long(pStep->m_fHighLimitC);		//mF단위
		netStep.lLowC	= long(pStep->m_fLowLimitC);		//mF단위
#else
		netStep.lHighC	= FLOAT2LONG(pStep->m_fHighLimitC);		//uAh단위
		netStep.lLowC	= FLOAT2LONG(pStep->m_fLowLimitC);		//uAh단위 
#endif
		netStep.lHighZ	= FLOAT2LONG(pStep->m_fHighLimitImp);
		netStep.lLowZ	= FLOAT2LONG(pStep->m_fLowLimitImp);
		netStep.lHighTemp = FLOAT2LONG(pStep->m_fHighLimitTemp);
		netStep.lLowTemp = FLOAT2LONG(pStep->m_fLowLimitTemp);

//		netStep.stepHeader.				= pStep->m_bGrade;
		//ljb 20100819 add Aux,BMS for v100A
		for (int j=0; j < _SFT_MAX_CAN_AUX_COMPARE_STEP ; j++)
		{
			netStep.stepReferance[0].ican_function_division[j] = pStep->m_ican_function_division[j];
			netStep.stepReferance[0].ican_compare_type[j] = pStep->m_ican_compare_type[j];
			netStep.stepReferance[0].cCan_data_type[j] = pStep->m_ican_data_type[j];
			netStep.stepReferance[0].cCan_branch[j] = pStep->m_ican_branch[j];
			netStep.stepReferance[0].fcan_Value[j] =  pStep->m_fcan_Value[j];
			netStep.stepReferance[0].iaux_function_division[j] =  pStep->m_iaux_function_division[j];
			netStep.stepReferance[0].iaux_compare_type[j] =  pStep->m_iaux_compare_type[j];
			netStep.stepReferance[0].cAux_data_type[j] =  pStep->m_iaux_data_type[j];
			netStep.stepReferance[0].cAux_branch[j] =  pStep->m_iaux_branch[j];
			netStep.stepReferance[0].faux_Value[j] =  pStep->m_faux_Value[j];
			netStep.stepReferance[0].iaux_conti_time[j] =  pStep->m_iaux_conti_time[j];		//ljb 20170824 add
		}
		
		for(int i=0; i<_SFT_MAX_PACKET_COMP_POINT && i<PS_MAX_COMP_POINT; i++)
		{
			netStep.vRamp[i].ulTime		= MDTIME(pStep->m_fCompTimeV[i]);			
			netStep.vRamp[i].lHighData	= FLOAT2LONG(pStep->m_fCompHighV[i]);
			netStep.vRamp[i].lLowData	= FLOAT2LONG(pStep->m_fCompLowV[i]);
			netStep.iRamp[i].ulTime		= MDTIME(pStep->m_fCompTimeI[i]);			
			netStep.iRamp[i].lHighData	= FLOAT2LONG(pStep->m_fCompHighI[i]);
			netStep.iRamp[i].lLowData	= FLOAT2LONG(pStep->m_fCompLowI[i]);
	 	}
		
		netStep.vDelta.lMaxData	= FLOAT2LONG(pStep->m_fDeltaV);
		netStep.vDelta.lMinData = 0;
		netStep.vDelta.ulTime	= MDTIME(pStep->m_fDeltaTimeV);
   		netStep.iDelta.lMaxData	= FLOAT2LONG(pStep->m_fDeltaI);
		netStep.iDelta.lMinData = 0;
   		netStep.iDelta.ulTime	= MDTIME(pStep->m_fDeltaTimeI);
	 
		netStep.conREC.lTime	= MDTIME(pStep->m_fReportTime);
		netStep.conREC.lDeltaV	= FLOAT2LONG(pStep->m_fReportV);
		netStep.conREC.lDeltaI	= FLOAT2LONG(pStep->m_fReportI);
		netStep.conREC.lDeltaT	= FLOAT2LONG(pStep->m_fReportTemp);
		netStep.conREC.lDeltaP	= 0;
		netStep.conREC.lReserved =0;
	 
		netStep.conEDLC.lCapV1		=  FLOAT2LONG(pStep->m_fCapaVoltage1);		
		netStep.conEDLC.lCapV2		=  FLOAT2LONG(pStep->m_fCapaVoltage2);	
		netStep.conEDLC.ulDCRStart	= MDTIME(pStep->m_fDCRStartTime);		
		netStep.conEDLC.ulDCREndT	= MDTIME(pStep->m_fDCREndTime);
		netStep.conEDLC.ulLCStartT	= MDTIME(pStep->m_fLCStartTime);		
		netStep.conEDLC.ulLCEndT	= MDTIME(pStep->m_fLCEndTime);		
	
		//netStep.lReserved[4];	 			
		//pStep->m_StepID;
		int n = pStep->m_Grading.GetGradeStepSize();
		int nGradeStepCnt = pStep->m_Grading.GetGradeStepSize()/2;
		netStep.gradeCondition[0].item = 0;		//20081210 KHS 사용 안함
		netStep.gradeCondition[1].item = 0;		//20081210 KHS 사용 안함	
		netStep.gradeCondition[0].stepCount = nGradeStepCnt;
		netStep.gradeCondition[1].stepCount = nGradeStepCnt;

		GRADE_STEP gradeStep;
		for(int a =0; a<2; a++)
		{
			for(int i=0; i<nGradeStepCnt;i++)
			{
				gradeStep = pStep->m_Grading.GetStepData(nGradeStepCnt*a+i);
				
				//netStep.gradeCondition[0].reserved
				if(!gradeStep.strCode.IsEmpty())
				{
					netStep.gradeCondition[a].step[i].gradeCode = gradeStep.strCode[0];
					netStep.gradeCondition[a].step[i].item = (BYTE)gradeStep.lGradeItem;		//20081210 KHS
				}
				
				//20081210 KHS
				switch (gradeStep.lGradeItem)
				{
				case PS_GRADE_CURRENT:		//20081212 KHS PS_GRADE_FARAD => PS_GRADE_CURRENT로 수정
					netStep.gradeCondition[a].step[i].lMinValue = long(gradeStep.fMin);	//mF단위
					netStep.gradeCondition[a].step[i].lMaxValue = long(gradeStep.fMax);	//mF단위
					break;
				case PS_GRADE_CAPACITY:
#ifdef _EDLC_TEST_SYSTEM
					netStep.gradeCondition[a].step[i].lMinValue = long(gradeStep.fMin);	//mF단위
					netStep.gradeCondition[a].step[i].lMaxValue = long(gradeStep.fMax);	//mF단위
#else
					netStep.gradeCondition[a].step[i].lMinValue = FLOAT2LONG(gradeStep.fMin);
					netStep.gradeCondition[a].step[i].lMaxValue = FLOAT2LONG(gradeStep.fMax);
#endif
					break;
				case PS_GRADE_TIME:
					netStep.gradeCondition[a].step[i].lMinValue = MDTIME(gradeStep.fMin);
					netStep.gradeCondition[a].step[i].lMaxValue = MDTIME(gradeStep.fMax);
					break;
				default :		//voltage, current
					netStep.gradeCondition[a].step[i].lMinValue = FLOAT2LONG(gradeStep.fMin);
					netStep.gradeCondition[a].step[i].lMaxValue = FLOAT2LONG(gradeStep.fMax);
					break;
				}
			}
		}
	}
	return netStep;
}

SFT_STEP_CONDITION_V3 CCTSMonProDoc::GetNetworkFormatStep_V1015(CStep *pStep) //lyj 20200214 LG v1015 이상
{
	SFT_STEP_CONDITION_V3	netStep; //lyj 20200214 LG v1015 이상
	ZeroMemory(&netStep, sizeof(SFT_STEP_CONDITION_V3)); //lyj 20200214 LG v1015 이상

	if(pStep != NULL)
	{
		netStep.stepHeader.nStepTypeID	= pStep->m_type;					//Step Type
		//		netStep.stepHeader.nStepTypeID	= pStep->m_lProcType;
		netStep.stepHeader.stepNo		= (BYTE)(pStep->m_StepIndex+1);		//StepNo
		netStep.stepHeader.mode			= pStep->m_mode;
		netStep.stepHeader.nSubStep		= 1;								//1개의 Sub Step만 이용한다.2005/08/08
		netStep.stepHeader.bTestEnd		= 0;
		//		netStep.stepHeader.bUseSocFlag = pStep->m_bUseActualCapa;
		netStep.stepHeader.bUseCyclePause = pStep->m_bUseCyclePause;		//Cycle 반복후 Pause flag //★★ ljb 201059  ★★★★★★★★

		netStep.stepReferance[0].lVref_Charge	= FLOAT2LONG(pStep->m_fVref_Charge);
		netStep.stepReferance[0].lVref_DisCharge	= FLOAT2LONG(pStep->m_fVref_DisCharge);
		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
		netStep.stepReferance[0].lRref	= FLOAT2LONG(pStep->m_fRref);

		//ljb 20150820 챔버연동 기능 (1: 챔버연동 대기, 0: 챔버연동 대기 없이 진행)
		if (pStep->m_fTref == 0)
			netStep.stepReferance[0].bChamberlinkOption	= 0;
		else
			netStep.stepReferance[0].bChamberlinkOption	= 1;

		//Power 제어값이 들어 있다. (Power 단위는 mW이므로 )
		//CR 제어값이 들어 있다. (CR 모드에서 R은 mOhm)
		//		if( (pStep->m_mode == PS_MODE_CP || pStep->m_mode == PS_MODE_CR)&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE))
		//20081229 => Power 단위 변경 

		// 		if(pStep->m_mode == PS_MODE_CP || pStep->m_mode == PS_MODE_CR)//&& (pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE))
		// 		{
		//     		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
		// 			TRACE("LONG: %ld\r\n", netStep.stepReferance[0].lPref);
		// 		}
		// 		//전류 단위는 uA단위
		// 		else	
		// 		{
		//      		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
		// 			TRACE("MODE : %x, LONG2: %ld\r\n", pStep->m_mode, netStep.stepReferance[0].lIref);
		// 			
		// 		}
		netStep.stepReferance[0].byRange	= (BYTE)pStep->m_lRange;	
		netStep.stepReferance[0].ulEndTimeDay	= pStep->m_lEndTimeDay;		//ljb 20131212 add
		netStep.stepReferance[0].ulEndTime	= MDTIME(pStep->m_fEndTime);
		netStep.stepReferance[0].lEndV_H	= FLOAT2LONG(pStep->m_fEndV_H);		//ljb v1009
		//		netStep.stepReferance[0].lEndV_L	= FLOAT2LONG(pStep->m_fEndV_L);		//ljb v1009
		netStep.stepReferance[0].lEndI		= FLOAT2LONG(pStep->m_fEndI);
		netStep.stepReferance[0].ulCVTimeDay	= pStep->m_lCVTimeDay;		//ljb 20131212 add
		netStep.stepReferance[0].ulCVTime	= MDTIME(pStep->m_fCVTime);

#ifdef _EDLC_TEST_SYSTEM
		netStep.stepReferance[0].lEndC		= long(pStep->m_fEndC);
#else
		netStep.stepReferance[0].lEndC		= FLOAT2LONG(pStep->m_fEndC);
#endif
		if (pStep->m_type == PS_STEP_LOOP || pStep->m_type == PS_STEP_ADV_CYCLE )
		{
			if (pStep->m_type == PS_STEP_LOOP)			//ljb v1009  LOOP 일 경우
			{
				netStep.stepReferance[0].lGoto		= pStep->m_nLoopInfoGotoStep;	
				netStep.stepReferance[0].lCycleCount= pStep->m_nLoopInfoCycle;	
				netStep.stepReferance[0].lAccCycleCount = pStep->m_nAccLoopInfoCycle;	//20080903 kjh
				netStep.stepReferance[0].lAccCycleGroupID	= pStep->m_nAccLoopGroupID;

				netStep.stepReferance[0].lEndV_L	= pStep->m_nMultiLoopInfoCycle;
				netStep.stepReferance[0].bEndC_Goto	= pStep->m_nMultiLoopGroupID;
				netStep.stepReferance[0].bEndWattHour_Goto	= pStep->m_nMultiLoopInfoGotoStep;
				netStep.stepReferance[0].bAccCycleCount_Goto	= pStep->m_nAccLoopInfoGotoStep;
			}
			else		
			{												//ljb v1009  CYCLE 일 경우
				netStep.stepReferance[0].bEndC_Goto	= pStep->m_nMultiLoopGroupID;
			}
			 //lyj 20200730 s [18PSCSA012] HL그린파워 1000V 600A 2CH 600kW_PACKcycler =============================================================
			if (pStep->m_type == PS_STEP_LOOP && m_bUseHLGP )
			{
				if(pStep->m_fEndV_H != 0) //lyj 20200810 HLGP 단위 수정
				{
					netStep.stepReferance[0].lVref_Charge	= FLOAT2LONG(pStep->m_fEndV_H)/1000;
		}
				netStep.stepReferance[0].ulEndTimeDay	= pStep->m_nGotoStepEndV_H;
				netStep.stepReferance[0].lEndI	= FLOAT2LONG(pStep->m_fEndV_L);
				netStep.stepReferance[0].ulEndTime	= pStep->m_nGotoStepEndV_L;

				netStep.stepReferance[0].lDeltaV	= LONG(pStep->m_fEndC);
				netStep.stepReferance[0].ulCVTimeDay	= pStep->m_nGotoStepEndC;
				netStep.stepReferance[0].lEndWatt	= LONG(pStep->m_fEndWh);
				netStep.stepReferance[0].ulCVTime	= pStep->m_nGotoStepEndWh;

				netStep.stepReferance[0].lEndC = 0;
				netStep.stepReferance[0].lEndWattHour = 0;

			}
			 //lyj 20200730 e HLGP =============================================================
		}
		else
		{
			//ljb v1009  Step..Cycle 일 경우
			netStep.stepReferance[0].lGoto		= pStep->m_nGotoStepEndV_H;	
			netStep.stepReferance[0].lCycleCount= pStep->m_nGotoStepEndV_L;	
			netStep.stepReferance[0].lAccCycleCount = pStep->m_nGotoStepEndTime;
			netStep.stepReferance[0].lAccCycleGroupID	= pStep->m_nGotoStepEndCVTime;
			netStep.stepReferance[0].lEndV_L	= FLOAT2LONG(pStep->m_fEndV_L);
			netStep.stepReferance[0].bEndC_Goto	= pStep->m_nGotoStepEndC;
			netStep.stepReferance[0].bEndWattHour_Goto	= pStep->m_nGotoStepEndWh;
			netStep.stepReferance[0].bValueRate_Goto	= pStep->m_nGotoStepEndValue;

			if (pStep->m_type != PS_STEP_END)
			{
				netStep.lCellDeltaVStep			= FLOAT2LONG(pStep->m_fCellDeltaVStep);
				netStep.faultcompAuxTemp		= pStep->m_fStepDeltaAuxTemp;
				netStep.faultcompAuxTh			= FLOAT2LONG(pStep->m_fStepDeltaAuxTH);
				netStep.faultcompAuxT			= FLOAT2LONG(pStep->m_fStepDeltaAuxT);
				netStep.faultcompAuxV			= FLOAT2LONG(pStep->m_fStepAuxV);
				netStep.faultcompAuxV_Time		= pStep->m_fStepDeltaAuxVTime;

				netStep.faultCompAuxV_Vent_flag		= (BYTE)pStep->m_bStepDeltaVentAuxV;
				netStep.faultCompAuxTemp_Vent_flag	= (BYTE)pStep->m_bStepDeltaVentAuxTemp;
				netStep.faultCompAuxTh_Vent_flag	= (BYTE)pStep->m_bStepDeltaVentAuxTh;
				netStep.faultCompAuxT_vent_flag		= (BYTE)pStep->m_bStepDeltaVentAuxT;
				netStep.faultDelta_AuxV_Vent_flag	= (BYTE)pStep->m_bStepVentAuxV;
			}
		}
		netStep.stepReferance[0].bValueItem = pStep->m_bValueItem;		//ljb v1009 Valu Item
		netStep.stepReferance[0].bValueStepNo = pStep->m_bValueStepNo;
		netStep.stepReferance[0].wValueRate	= pStep->m_fValueRate *100;
		netStep.stepReferance[0].bValueRate_Goto = pStep->m_nGotoStepEndValue;

		//lyj 20200807 s [18PSCSA012] HL그린파워 1000V 600A 2CH 600kW_PACKcycler =============================================================
		if (pStep->m_type == PS_STEP_LOOP && m_bUseHLGP )
		{
			;
		}
		else
		{
		netStep.stepReferance[0].lDeltaV	= FLOAT2LONG(pStep->m_fEndDV);
		netStep.stepReferance[0].lEndWatt   = LONG(pStep->m_fEndW);
		TRACE("lEndWatt: %ld\r\n", netStep.stepReferance[0].lEndWatt);
		}
		//lyj 20200807 e ==========================================================================================================================

		netStep.stepReferance[0].lEndWattHour = LONG(pStep->m_fEndWh);
		TRACE("lEndWattHour: %ld\r\n", netStep.stepReferance[0].lEndWattHour);
		netStep.stepReferance[0].lReserved[0] = FLOAT2LONG(pStep->m_fEndDI);
		//    	netStep.stepReferance[0].lEndData3	= FLOAT2LONG(pStep->m_fEndDI);

		netStep.lHighV	= FLOAT2LONG(pStep->m_fHighLimitV);
		netStep.lLowV	= FLOAT2LONG(pStep->m_fLowLimitV);
		netStep.lHighI	= FLOAT2LONG(pStep->m_fHighLimitI);
		netStep.lLowI	= FLOAT2LONG(pStep->m_fLowLimitI);
#ifdef _EDLC_TEST_SYSTEM
		netStep.lHighC	= long(pStep->m_fHighLimitC);		//mF단위
		netStep.lLowC	= long(pStep->m_fLowLimitC);		//mF단위
#else
		netStep.lHighC	= FLOAT2LONG(pStep->m_fHighLimitC);		//uAh단위
		netStep.lLowC	= FLOAT2LONG(pStep->m_fLowLimitC);		//uAh단위 
#endif
		netStep.lHighZ	= FLOAT2LONG(pStep->m_fHighLimitImp);
		netStep.lLowZ	= FLOAT2LONG(pStep->m_fLowLimitImp);
		netStep.lHighTemp = FLOAT2LONG(pStep->m_fHighLimitTemp);
		netStep.lLowTemp = FLOAT2LONG(pStep->m_fLowLimitTemp);

		//		netStep.stepHeader.				= pStep->m_bGrade;
		//ljb 20100819 add Aux,BMS for v100A
		for (int j=0; j < _SFT_MAX_CAN_AUX_COMPARE_STEP ; j++)
		{
			netStep.stepReferance[0].ican_function_division[j] = pStep->m_ican_function_division[j];
			netStep.stepReferance[0].ican_compare_type[j] = pStep->m_ican_compare_type[j];
			netStep.stepReferance[0].cCan_data_type[j] = pStep->m_ican_data_type[j];
			netStep.stepReferance[0].cCan_branch[j] = pStep->m_ican_branch[j];
			netStep.stepReferance[0].fcan_Value[j] =  pStep->m_fcan_Value[j];
			netStep.stepReferance[0].iaux_function_division[j] =  pStep->m_iaux_function_division[j];
			netStep.stepReferance[0].iaux_compare_type[j] =  pStep->m_iaux_compare_type[j];
			netStep.stepReferance[0].cAux_data_type[j] =  pStep->m_iaux_data_type[j];
			netStep.stepReferance[0].cAux_branch[j] =  pStep->m_iaux_branch[j];
			netStep.stepReferance[0].faux_Value[j] =  pStep->m_faux_Value[j];
			netStep.stepReferance[0].iaux_conti_time[j] =  pStep->m_iaux_conti_time[j];		//ljb 20170824 add
		}

		for(int i=0; i<_SFT_MAX_PACKET_COMP_POINT && i<PS_MAX_COMP_POINT; i++)
		{
			netStep.vRamp[i].ulTime		= MDTIME(pStep->m_fCompTimeV[i]);			
			netStep.vRamp[i].lHighData	= FLOAT2LONG(pStep->m_fCompHighV[i]);
			netStep.vRamp[i].lLowData	= FLOAT2LONG(pStep->m_fCompLowV[i]);
			netStep.iRamp[i].ulTime		= MDTIME(pStep->m_fCompTimeI[i]);			
			netStep.iRamp[i].lHighData	= FLOAT2LONG(pStep->m_fCompHighI[i]);
			netStep.iRamp[i].lLowData	= FLOAT2LONG(pStep->m_fCompLowI[i]);
		}

		netStep.vDelta.lMaxData	= FLOAT2LONG(pStep->m_fDeltaV);
		netStep.vDelta.lMinData = 0;
		netStep.vDelta.ulTime	= MDTIME(pStep->m_fDeltaTimeV);
		netStep.iDelta.lMaxData	= FLOAT2LONG(pStep->m_fDeltaI);
		netStep.iDelta.lMinData = 0;
		netStep.iDelta.ulTime	= MDTIME(pStep->m_fDeltaTimeI);

		netStep.conREC.lTime	= MDTIME(pStep->m_fReportTime);
		netStep.conREC.lDeltaV	= FLOAT2LONG(pStep->m_fReportV);
		netStep.conREC.lDeltaI	= FLOAT2LONG(pStep->m_fReportI);
		netStep.conREC.lDeltaT	= FLOAT2LONG(pStep->m_fReportTemp);
		netStep.conREC.lDeltaP	= 0;
		netStep.conREC.lReserved =0;

		netStep.conEDLC.lCapV1		=  FLOAT2LONG(pStep->m_fCapaVoltage1);		
		netStep.conEDLC.lCapV2		=  FLOAT2LONG(pStep->m_fCapaVoltage2);	
		netStep.conEDLC.ulDCRStart	= MDTIME(pStep->m_fDCRStartTime);		
		netStep.conEDLC.ulDCREndT	= MDTIME(pStep->m_fDCREndTime);
		netStep.conEDLC.ulLCStartT	= MDTIME(pStep->m_fLCStartTime);		
		netStep.conEDLC.ulLCEndT	= MDTIME(pStep->m_fLCEndTime);		

		//netStep.lReserved[4];	 			
		//pStep->m_StepID;
		int n = pStep->m_Grading.GetGradeStepSize();
		int nGradeStepCnt = pStep->m_Grading.GetGradeStepSize()/2;
		netStep.gradeCondition[0].item = 0;		//20081210 KHS 사용 안함
		netStep.gradeCondition[1].item = 0;		//20081210 KHS 사용 안함	
		netStep.gradeCondition[0].stepCount = nGradeStepCnt;
		netStep.gradeCondition[1].stepCount = nGradeStepCnt;

		GRADE_STEP gradeStep;
		for(int a =0; a<2; a++)
		{
			for(int i=0; i<nGradeStepCnt;i++)
			{
				gradeStep = pStep->m_Grading.GetStepData(nGradeStepCnt*a+i);

				//netStep.gradeCondition[0].reserved
				if(!gradeStep.strCode.IsEmpty())
				{
					netStep.gradeCondition[a].step[i].gradeCode = gradeStep.strCode[0];
					netStep.gradeCondition[a].step[i].item = (BYTE)gradeStep.lGradeItem;		//20081210 KHS
				}

				//20081210 KHS
				switch (gradeStep.lGradeItem)
				{
				case PS_GRADE_CURRENT:		//20081212 KHS PS_GRADE_FARAD => PS_GRADE_CURRENT로 수정
					netStep.gradeCondition[a].step[i].lMinValue = long(gradeStep.fMin);	//mF단위
					netStep.gradeCondition[a].step[i].lMaxValue = long(gradeStep.fMax);	//mF단위
					break;
				case PS_GRADE_CAPACITY:
#ifdef _EDLC_TEST_SYSTEM
					netStep.gradeCondition[a].step[i].lMinValue = long(gradeStep.fMin);	//mF단위
					netStep.gradeCondition[a].step[i].lMaxValue = long(gradeStep.fMax);	//mF단위
#else
					netStep.gradeCondition[a].step[i].lMinValue = FLOAT2LONG(gradeStep.fMin);
					netStep.gradeCondition[a].step[i].lMaxValue = FLOAT2LONG(gradeStep.fMax);
#endif
					break;
				case PS_GRADE_TIME:
					netStep.gradeCondition[a].step[i].lMinValue = MDTIME(gradeStep.fMin);
					netStep.gradeCondition[a].step[i].lMaxValue = MDTIME(gradeStep.fMax);
					break;
				default :		//voltage, current
					netStep.gradeCondition[a].step[i].lMinValue = FLOAT2LONG(gradeStep.fMin);
					netStep.gradeCondition[a].step[i].lMaxValue = FLOAT2LONG(gradeStep.fMax);
					break;
				}
			}
		}
	}
	return netStep;
}
BOOL CCTSMonProDoc::CompletedTest(UINT nModuleID, UINT nChIndex)
{
	CCyclerChannel	*lpChannelInfo= NULL;		//Channel 

	//채널 정보를 구한다.
	lpChannelInfo = GetChannelInfo(nModuleID, nChIndex);
		
	if(lpChannelInfo == NULL)
	{
		return FALSE;
	}
	
	//채널에 저장할 Data가 누적되어 있을 경우 Check한 저장한다.
	if(lpChannelInfo->CompletedTest() < 0)
	{
		WriteLog(Fun_FindMsg("CompletedTest_msg1","CTSMonPro_DOC"));	//WriteLog("결과 파일 저장 실패"); //&&
		return FALSE;
	}

	return TRUE;
}


void CCTSMonProDoc::CalibrationExe(int nModuleID, CWnd *pParent)
{
	//Dialog 초기화를 위해 새로 생성한다.
	if(m_pCalibratorDlg)
	{
		delete m_pCalibratorDlg;
		m_pCalibratorDlg = NULL;
	}

	if(m_pCalibratorDlg == NULL)
	{
		m_pCalibratorDlg = new CCalibratorDlg(nModuleID, this);
		//m_pCalibratorDlg->Create(IDD_CALIBRATION_DLG, pParent);
		m_pCalibratorDlg->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_CALIBRATION_DLG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_CALIBRATION_DLG_ENG): //&&
			IDD_CALIBRATION_DLG, pParent);//&&
	}
	m_pCalibratorDlg->ShowWindow(SW_SHOW);
}

void CCTSMonProDoc::Fun_SafetyUpdateExe(CDWordArray &adwTargetCh, CWnd *pParent)
{
	CString strMsg;
	
	CWordArray chArray;
	DWORD dwData;
	CWaitCursor wait;
	int nPrevModule = 0, nModuleID, nChannelIndex;
	int nCurModuleID = 0;
	
	for(int s=0; s<adwTargetCh.GetSize(); s++)
	{
		dwData = adwTargetCh.GetAt(s);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
	}

	//Dialog 초기화를 위해 새로 생성한다.
	if(m_pSafetyUpdateDlg)
	{
		delete m_pSafetyUpdateDlg;
		m_pSafetyUpdateDlg = NULL;
	}
	
	if(m_pSafetyUpdateDlg == NULL)
	{
		m_pSafetyUpdateDlg = new CSafetyUpdateDlg(nModuleID,nChannelIndex, this);
		//m_pSafetyUpdateDlg->Create(IDD_SAFETY_UPDATE_DLG, pParent);
		m_pSafetyUpdateDlg->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_SAFETY_UPDATE_DLG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_SAFETY_UPDATE_DLG_ENG): //&&
			IDD_SAFETY_UPDATE_DLG, pParent);//&&
//#ifdef _DEBUG //ksj 20170809
		CString strTemp;
		m_pSafetyUpdateDlg->GetWindowText(strMsg);
		strTemp.Format(_T("%s (M%02dC%02d)"),strMsg,nModuleID,nChannelIndex+1);
		m_pSafetyUpdateDlg->SetWindowText(strTemp);
//#endif
	}
	m_pSafetyUpdateDlg->ShowWindow(SW_SHOW);
}

void CCTSMonProDoc::Fun_SchUpdateExe(CDWordArray &adwTargetCh, CWnd *pParent)
{
	CString strMsg;
	
	CWordArray chArray;
	DWORD dwData;
	CWaitCursor wait;
	int nPrevModule = 0, nModuleID, nChannelIndex;
	int nCurModuleID = 0;
	
	for(int s=0; s<adwTargetCh.GetSize(); s++)
	{
		dwData = adwTargetCh.GetAt(s);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
	}

	//ksj 20170809
	CCyclerChannel* pChannel = GetChannelInfo(nModuleID, nChannelIndex);
	int nDefaultStepNo = pChannel->GetStepNo(); //현재 스텝 가져온다.	
	//ksj end
	
	//Dialog 초기화를 위해 새로 생성한다.
	if(m_pSchUpdateDlg)
	{
		delete m_pSchUpdateDlg;
		m_pSchUpdateDlg = NULL;
	}
	
	if(m_pSchUpdateDlg == NULL)
	{
		m_pSchUpdateDlg = new CSchUpdateDlg(nModuleID, nChannelIndex, this);
		//m_pSchUpdateDlg->Create(IDD_SCH_UPDATE_DLG, pParent);
		m_pSchUpdateDlg->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_SCH_UPDATE_DLG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_SCH_UPDATE_DLG_ENG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_SCH_UPDATE_DLG_PL): //&&
			IDD_SCH_UPDATE_DLG, pParent);//&&
//#ifdef _DEBUG //ksj 20170809
		CString strTemp;
		m_pSchUpdateDlg->GetWindowText(strMsg);
		strTemp.Format(_T("%s (M%02dC%02d)"),strMsg,nModuleID,nChannelIndex+1);
		m_pSchUpdateDlg->SetWindowText(strTemp);
//#endif
	}
	m_pSchUpdateDlg->SetDefaultStepNo(nDefaultStepNo); //ksj 20170809 : 기본스텝 지정.
	m_pSchUpdateDlg->ShowWindow(SW_SHOW);
}

void CCTSMonProDoc::ShowLogDlg()
{
	if(m_pLogDlg == NULL)
	{
		m_pLogDlg = new CLogDlg();
		//m_pLogDlg->Create(IDD_LOG_DLG);
		m_pLogDlg->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_LOG_DLG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_LOG_DLG_ENG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_LOG_DLG_PL): //&&
			IDD_LOG_DLG);//&&
	}
	m_pLogDlg->ShowWindow(SW_SHOW);

	m_pLogDlg->SetLogEvent();

	SetForegroundWindow(m_pLogDlg->m_hWnd);
}

void CCTSMonProDoc::ShowEventLogDlg()
{
	if(m_pEventLogDlg == NULL)
	{
		m_pEventLogDlg = new CLogDlg();
		//m_pEventLogDlg->Create(IDD_LOG_DLG);
		m_pEventLogDlg->Create(//&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDD_LOG_DLG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDD_LOG_DLG_ENG): //&&
			(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDD_LOG_DLG_PL): //&&
			IDD_LOG_DLG);//&&
	}
	m_pEventLogDlg->ShowWindow(SW_SHOW);
	
	m_pEventLogDlg->SetLogEvent();
	
	SetForegroundWindow(m_pEventLogDlg->m_hWnd);
}

//When Module is connected 
//Write Module IP Address to SystemConfig DataBase
BOOL CCTSMonProDoc::WriteModuleInfoToDataBase(int nModuleID)
{
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	if(pMD == NULL)		return FALSE;
	
	CString strVSpec, strISpec, strTemp;

	for(int i =0; i< pMD->GetVoltageRangeCount(); i++)
	{
		strTemp.Format("%d/", (long)pMD->GetVoltageSpec(i));
		strVSpec = strVSpec + strTemp;
	}

	for(int i =0; i< pMD->GetCurrentRangeCount(); i++)
	{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			//20180313 yulee [SKI P1803A035SYS001]1000A 장비 스펙을 500에서 1000으로 강제 변경
			//1,2채널 병렬 3,4채널 병렬로 인해 화면표시에는 2배로 표시 
			strTemp.Format("%d/", (((long)pMD->GetCurrentSpec(i)))*2);
		}
//#else
else
		{
			strTemp.Format("%d/", (long)pMD->GetCurrentSpec(i));
		}
//#endif
		strISpec = strISpec + strTemp;
	}

	CDaoDatabase  db;
	try
	{
  		db.Open(m_strDataBaseName);

		//IP_Address, VSpec, ISpec, ModuleType, InstallChCount, ControllerType, ProgramVer FROM SystemConfig ORDER BY ModuleID");

		CString strSQL;
		strSQL.Format(	"UPDATE SystemConfig SET IP_Address = '%s', VSpec = '%s', ISpec = '%s', InstallChCount = %d, Data4 = '%s' WHERE ModuleID = %d", 
						pMD->GetIPAddress(), 
						strVSpec,
						strISpec,
						pMD->GetTotalChannel(),
						m_strModuleInfo,
						nModuleID
					);
		db.Execute(strSQL);
		db.Close();
	}
	catch(CDaoException *e)
	{
      // Simply show an error message to the user.
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}
	return TRUE;
}

CString CCTSMonProDoc::GetModuleIPAddress(int nModuleID,  BOOL bUserDlg)
{
	CString address;
	//모듈이 접속되어 있으면 직접 구함 
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	if(pMD)
	{
		address = pMD->GetIPAddress();
	}

	//사용자에게 수동 입력 받는다. 
	if(bUserDlg && address.IsEmpty())
	{
		CIPSetDlg dlg;
		if(dlg.DoModal() == IDOK)
		{
			address = dlg.GetIPAddress();
		}
	}

	return address;	
}

BOOL CCTSMonProDoc::ExecuteProgram(CString strProgram, CString strArgument/* = ""*/, CString strClassName /*= ""*/, CString strWindowName/*= ""*/, BOOL bNewExe/* = FALSE*/, 	BOOL bWait/* = FALSE*/)
{
	if(strProgram.IsEmpty())	return FALSE;

//	TCHAR szBuff[_MAX_PATH];
//	::GetModuleFileName(AfxGetApp()->m_hInstance,szBuff,_MAX_PATH);
//	CString path = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\'));

	//새롭게 실행 하지 않으면 주어진 이름으로 검색한다.
	if(!bNewExe)
	{
		HWND FirsthWnd = NULL;
		if(!strWindowName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(NULL, strWindowName);
		}
		if(!strClassName.IsEmpty())
		{
			FirsthWnd = ::FindWindow(strClassName, NULL);
		}
 		if (FirsthWnd)		//실행 되어 있으면 
 		{
 //			::SetActiveWindow(FirsthWnd);	
		::SetForegroundWindow(FirsthWnd);
		return TRUE;
 		}
		//else	//실행된게 없으면 새롭게 실행 
	}

	CString strFullName;
	if(!strArgument.IsEmpty())
	{
		strFullName.Format("%s %s", strProgram, strArgument);
	}
	else
	{
		strFullName.Format("%s", strProgram);
	}

//AfxMessageBox(strFullName);
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	pi;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_SHOWNORMAL;

	BOOL bFlag = ::CreateProcess(	NULL, 
									(LPSTR)(LPCTSTR)strFullName, 
									NULL, 
									NULL, 
									FALSE, 
									NORMAL_PRIORITY_CLASS, 
									NULL, 
									NULL, 
									&stStartUpInfo, 
									&pi
								);
	if(bFlag == FALSE)
	{
		//strFullName += "를 실행할 수 없습니다.";
		strFullName += Fun_FindMsg("CTSMonProDoc_ExecuteProgram_msg1","CTSMonPro_DOC");//&&
		AfxMessageBox(strFullName, MB_OK|MB_ICONSTOP);
		return FALSE;
	}
    
	if(bWait)
	{
		DWORD dwRet = WaitForInputIdle(pi.hProcess, 15000);
		if (WAIT_TIMEOUT != dwRet)  return true;
	}

	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
	
	return TRUE;
}

/*
bool Create(LPCTSTR strDirectory, LPCTSTR strProcess, LPCTSTR strCmdLine)
{
    CString str;
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    memset(&ssi, 0x00, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    str.Format("%s/%s", strDirectory, strProcess);
    if (::CreateProcess(str, (char *)strCmdLine, NULL, NULL, FALSE, 0, NULL, strDirectory, &si, &pi))
    {
        DWORD dwRet = WaitForInputIdle(pi.hProcess, 10000);
        if (WAIT_TIMEOUT != dwRet)
        return true;
    }
	return false;
}

BOOL Terminate(DWORD dwTimeout, HANDLE hProcess)
{
   if (hProcess == NULL)
       return FALSE;
    BOOL dwErr = NO_ERROR;
    DWORD dwExit = 0;
    if (GetExitCodeProcess(hProcess, &dwExit))
    {
       DWORD dwTID, dwCode;
       HANDLE hProcessDup = INVALID_HANDLE_VALUE;
       HANDLE hRT = NULL;
        HINSTANCE hKernel = GetModuleHandle("Kernel32");
        BOOL bDup = DuplicateHandle(GetCurrentProcess(), hProcess, GetCurrentProcess(), &hProcessDup, 
                                PROCESS_ALL_ACCESS, FALSE, 0);

        // Detect the special case where the process is 
        // already dead...
        if (GetExitCodeProcess((bDup) ? hProcessDup : hProcess, &dwCode) && (dwCode == STILL_ACTIVE)) 
        {
           FARPROC pfnExitProc;
            pfnExitProc = GetProcAddress(hKernel, "ExitProcess");
            hRT = CreateRemoteThread((bDup) ? hProcessDup : hProcess, NULL, 0, 
                                    (LPTHREAD_START_ROUTINE)pfnExitProc, (PVOID)dwExit, 0, &dwTID);
 
           if (hRT == NULL)
                dwErr = GetLastError();
        }
        else
        {
           dwErr = ERROR_PROCESS_ABORTED;
        }

        if (hRT)
        {
            // Must wait process to terminate to 
            // guarantee that it has exited...
            if ( WAIT_OBJECT_0 != WaitForSingleObject((bDup) ? hProcessDup : hProcess,  dwTimeout) )
                dwErr = ERROR_PROCESS_ABORTED;
            CloseHandle(hRT);
         }
        if (bDup)
            CloseHandle(hProcessDup);
        }
    else
        {
        TerminateProcess(hProcess, 0);
        WaitForSingleObject(hProcess, dwTimeout);
        }
   return dwErr;
}
*/




BOOL CCTSMonProDoc::ExecuteEditor(CString strModel, CString strTest, BOOL bNewWnd)
{

	CString strTemp, strArgu;
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
//	strTemp = CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')) + "\\CTSEditorPro.exe";
	strTemp.Format("%s\\CTSEditorPro.exe", CString(szCurDir).Mid(0, CString(szCurDir).ReverseFind('\\')));
	strArgu.Format("%s,%s", strModel,  strTest);

	if(ExecuteProgram(strTemp, strArgu, "", "CTSEditorPro", bNewWnd, TRUE)== FALSE)	return FALSE;

	//Argument가 있으면 
	if(!strModel.IsEmpty())
	{
		HWND FirsthWnd;
		FirsthWnd = ::FindWindow(NULL, "CTSEditorPro");
		if (FirsthWnd)
		{
			int nSize = strModel.GetLength()+strTest.GetLength()+2;
			char *pData = new char[nSize];
			sprintf(pData, "%s,%s", strModel, strTest);
			pData[nSize-1] = '\0';

			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = 3;		//CTSMonPro Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
			::SendMessage(FirsthWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}

	}
	return TRUE;
}

//채널 상태를 표시할 아이콘 인덱스를 리턴한다.
//IDB_CH_STS_SMALL_SIGN
//KBH
UINT CCTSMonProDoc::GetStateImageIndex(WORD state, WORD nType)
{
	//TRACE("채널상태 : %X\n",state);

	switch(state)
	{
	case PS_STATE_IDLE		:		return	0x0000;
	case PS_STATE_STANDBY	:		return	0x000C;
	case PS_STATE_PAUSE	:			return 	0x0003;
	case PS_STATE_MAINTENANCE:		return 	0x0008;
	case PS_STATE_RUN		:		
		{
			switch(nType)
			{
					case PS_STEP_CHARGE		:			return 	0x0002;
					case PS_STEP_DISCHARGE	:			return 	0x0001;
					case PS_STEP_OCV		:			return 	0x0007;
					case PS_STEP_REST		:			return 	0x000B;
					case PS_STEP_IMPEDANCE	:			return 	0x000A;
					case PS_STEP_EXT_CAN	:			return 	0x0011;		//ljb 2011322 이재복 //////////
					default					:			return	0x0009;
			}
		}
	case PS_STATE_SELF_TEST:		return 	0x0006;
	case PS_STATE_FAIL		:		return 	0x0006;
	case PS_STATE_CHECK	:			return 	0x0009;
	case PS_STATE_STOP		:		return 	0x0004;
	case PS_STATE_END		:		return 	0x000E;
	case PS_STATE_FAULT	:			return 	0x0005;
	case PS_STATE_NONCELL	:		return 	0x0006;
	case PS_STATE_READY	:			return 	0x0010;
	default:						return	0x0000;
	}

	return 0;
}

/*
CString CCTSMonProDoc::GetStateMsg(WORD State, WORD type)
{
	BYTE index = GetStateColorIndex(State, type);
	
	ASSERT(index < PS_MAX_STATE_COLOR);
	
	PS_COLOR_CONFIG *pColorCfg = GetStateCfg();
	ASSERT(pColorCfg);

	return CString(pColorCfg->stateConfig[index].szMsg);
}
*/

CString CCTSMonProDoc::ValueString(long lData, int item)
{
	CString strMsg, strTemp;
	long Value = lData;
	double dTemp;
	char szTemp[8];


	switch(item)
	{
	case PS_STATE:		
		strMsg = ::PSGetStateMsg((WORD)Value);	
		break;

	 //ksj 20200625 : 모드 추가.
	case PS_STEP_MODE:
		
		if(Value == PS_MODE_CCCV)    strMsg = "CC/CV";
		else if(Value == PS_MODE_CC) strMsg = "CC";		
		else if(Value == PS_MODE_CV) strMsg = "CV";		
		else if(Value == PS_MODE_CP) strMsg = "CP";		
		else if(Value == PS_MODE_PUSE) strMsg = "Pulse";	
		else if(Value == PS_MODE_CR) strMsg = "CR";			
		else if(Value == PS_MODE_CCCP) strMsg = "CC/CP";		
		else strMsg = "";
		break;
	//ksj end
	case PS_VOLTAGE:
	case PS_VOLTAGE_INPUT:
	case PS_VOLTAGE_POWER:
	case PS_VOLTAGE_BUS:
		{
			dTemp = (double)Value;
			//SBC에서 uV 단위로 전송되어 온다.
			if(m_VoltageUnitMode)
			{
				//SBC에서 nA 단위로 전송되어 온다.
				dTemp = dTemp / 1000.0f;		//uV 단위로 변환 
			}

			//voltage
			if(m_strVUnit == "mV")
			{
				dTemp = dTemp / 1000.0f;	//LONG2FLOAT(Value);
			}
			else if(m_strVUnit == "V")
			{
				dTemp = dTemp / 1000000.0f;		//LONG2FLOAT(Value)/1000.0f;
			}
			else if(m_strVUnit == "nV")
			{
				dTemp = dTemp * 1000.0f;	//LONG2FLOAT(Value);
			}
//			else if(m_strVUnit == "uV")
//			{
//				dTemp = float(Value);
//			} 

			if(m_nVDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nVDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
			break;
		}

	case PS_CURRENT:
		{
			//SBC에서 uA 단위로 전송되어 온다.
			dTemp = (double)Value;
			
			if(m_CurrentUnitMode)
			{
				//SBC에서 nA 단위로 전송되어 온다.
				dTemp = dTemp / 1000.0f;		//uA 단위로 변화 
			}
			
			//current
			if(m_strIUnit == "nA")
			{
				dTemp = dTemp*1000.0f;
			}  
			else if(m_strIUnit == "A")
			{
				dTemp = dTemp / 1000000.0f;		// LONG2FLOAT(Value)/1000.0f;
			}
			else if(m_strIUnit == "mA")	//mA
			{
				dTemp = dTemp / 1000.0f;		//LONG2FLOAT(Value);
			}
//			else if(m_strIUnit == "uA")
//			{
//				dTemp = float(Value);
//			}

			if(m_nIDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nIDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
			break;
		}

	case PS_WATT	:
		{
			//SBC에서 mW 단위로 전송되어 온다.
			dTemp = (double)Value;
			
			if(m_CurrentUnitMode)
			{
				//sbc에서 u단위로 전송되어 온다.
				dTemp = dTemp / 1000.0f;		//m 단위로 변경 
			}
			
			//current
			if(m_strWUnit == "W")
			{
				dTemp = dTemp/1000.0f;
			}  
			else if(m_strWUnit == "KW")
			{
				dTemp = dTemp / 1000000.0f;		// LONG2FLOAT(Value)/1000.0f;
			}
			else if(m_strWUnit == "uW")
			{
				dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
			}
//			else if(m_strWUnit == "mW")
//			{
//				dTemp = float(Value);
//			}

			if(m_nWDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nWDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
			break;
		}
		

	case PS_WATT_HOUR:	
	case PS_CHARGE_WH:
	case PS_DISCHARGE_WH:
		{
			//SBC에서 mWh 단위로 전송되어 온다.
			dTemp = (double)Value;
			
			if(m_CurrentUnitMode)
			{
				//sbc에서 u단위로 전송되어 온다.
				dTemp = dTemp / 1000.0f;		//m 단위로 변경 
			}
			
			//current
			if(m_strWhUnit == "Wh")
			{
				dTemp = dTemp/1000.0f;
			}  
			else if(m_strWhUnit == "KWh")
			{
				dTemp = dTemp / 1000000.0f;		// LONG2FLOAT(Value)/1000.0f;
			}
			else if(m_strWhUnit == "uWh")
			{
				dTemp = dTemp * 1000.0f;		// LONG2FLOAT(Value)/1000.0f;
			}
//			else if(m_strWhUnit == "mWh")
//			{
//				dTemp = float(Value);
//			}

			if(m_nWhDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nWhDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
			break;
		}

	case PS_IMPEDANCE:
		//sbc에서 u단위로 전송되어 온다.
		Value = labs(Value);
#ifdef _EDLC_TEST_SYSTEM
		strMsg.Format("%.3f", LONG2FLOAT(Value));
#else
		strMsg.Format("%.1f", LONG2FLOAT(Value));
#endif
		break;

	case PS_CAPACITY:		//capacity
	case PS_CHARGE_CAP:
	case PS_DISCHARGE_CAP:
//yulee 20180308 EDLC 용으로 F 설정 하는 것으로 변경 
#ifdef _EDLC_CELL_
	case PS_CAPACITANCE:
		{
			//sbc에서 Battery일 경우 uAh로 전송되어 온다.
			//sbc에서 EDLC 경우 mF로 전송되어 온다.
			dTemp = (double)Value;
			
			if(m_CurrentUnitMode)
			{
				//sbc에서 nAh로 전송되어 온다.
				dTemp = dTemp / 1000.0f;		//uAh 단위로 변환 
			}
			
			if(m_strCUnit == "kF")	//ljb 20151212 add m_strCUnit == "A" 
			{
				dTemp = dTemp/1000000.0f;	//LONG2FLOAT(Value);
			}
			else if(m_strCUnit == "F")
			{
				dTemp = dTemp/1000.0f;
			}
			else if(m_strCUnit == "uF")
			{
				dTemp = dTemp*1000.0f;
			}
			//else if(m_strCUnit == "uAh" || m_strCUnit == "uF")
			//			{
			//				dTemp = dTemp;
			//			}
			
			if(m_nCDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nCDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
		}
		break;
#else
	case PS_CAPACITANCE:
		{
			//sbc에서 Battery일 경우 uAh로 전송되어 온다.
			//sbc에서 EDLC 경우 mF로 전송되어 온다.
			dTemp = (double)Value;
			
			if(m_CurrentUnitMode)
			{
				//sbc에서 nAh로 전송되어 온다.
				dTemp = dTemp / 1000.0f;		//uAh 단위로 변환 
			}
			
			if(m_strCUnit == "A" || m_strCUnit == "Ah")	//ljb 20151212 add m_strCUnit == "A" 
			{
				dTemp = dTemp/1000000.0f;	//LONG2FLOAT(Value);
			}
			else if(m_strCUnit == "mAh")
			{
				dTemp = dTemp/1000.0f;
			}
			else if(m_strCUnit == "nAh")
			{
				dTemp = dTemp*1000.0f;
			}
			//else if(m_strCUnit == "uAh")
			//			{
			//				dTemp = dTemp;
			//			}
			
			if(m_nCDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nCDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
		}
		break;
#endif



		/*
	case PS_CAPACITANCE:
		{
			//sbc에서 Battery일 경우 uAh로 전송되어 온다.
			//sbc에서 EDLC 경우 mF로 전송되어 온다.
			dTemp = (double)Value;
			
			if(m_CurrentUnitMode)
			{
				//sbc에서 nAh로 전송되어 온다.
				dTemp = dTemp / 1000.0f;		//uAh 단위로 변환 
			}

			if(m_strCUnit == "A" || m_strCUnit == "Ah" || m_strCUnit == "kF")	//ljb 20151212 add m_strCUnit == "A" 
			{
				dTemp = dTemp/1000000.0f;	//LONG2FLOAT(Value);
			}
			else if(m_strCUnit == "mAh" || m_strCUnit == "F")
			{
				dTemp = dTemp/1000.0f;
			}
			else if(m_strCUnit == "nAh" || m_strCUnit == "uF")
			{
				dTemp = dTemp*1000.0f;
			}
			//else if(m_strCUnit == "uAh" || m_strCUnit == "uF")
//			{
//				dTemp = dTemp;
//			}

			if(m_nCDecimal > 0)	//소수점 표기 
			{
				sprintf(szTemp, "%%.%df", m_nCDecimal);
				strMsg.Format(szTemp, dTemp);
			}
			else	//정수 표기 
			{
				strMsg.Format("%d", (LONG)dTemp);
			}
		}
		break;
		*/
	case PS_CODE:	//failureCode
		PSCellCodeMsg((BYTE)Value, strMsg, strTemp);
		break;

	case PS_TOT_TIME:
	case PS_STEP_TIME:
	case PS_CV_TIME:
		{
			//sbc에서 10msec단위 전송되어 온다.
			CTimeSpan timeSpan((ULONG)PCTIME(Value));
			if(m_nTimeUnit == 1)	//sec 표시
			{
				strMsg.Format("%.1f", PCTIME(Value));
			}
			else if(m_nTimeUnit == 2)	//Minute 표시
			{
				strMsg.Format("%.2f", PCTIME(Value)/60.0f);
			}
			else
			{
				if(timeSpan.GetDays() > 0)
				{
					strMsg =  timeSpan.Format("%Dd %H:%M:%S");
				}
				else
				{
					strMsg = timeSpan.Format("%H:%M:%S");
				}
			}
		}
		break;

	case PS_GRADE_CODE:	
		strMsg.Format("%c", (char)Value);
//		strMsg = ::GetSelectCodeName((char)Value);
		break;

	case PS_AUX_TEMPERATURE:
	case PS_AUX_TEMPERATURE_TH:
	case PS_AUX_VOLTAGE:
		//m단위로 전송되어 온다.
		strMsg.Format("%.1f", Value/1000.0f);
		break;
		
	case PS_OVEN_TEMPERATURE:
	case PS_OVEN_HUMIDITY:
	case PS_CHILLER_REF_TEMP:
	case PS_CHILLER_CUR_TEMP:
		//m단위로 전송되어 온다.
		strMsg.Format("%.1f", Value/1000.0f);
		break;

	case PS_CHAMBER_USING:
		{
			if(Value == 1)
				//strMsg.Format("(챔버연동)");//$1013
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_ValueString_msg1","CTSMonPro_DOC"));//&&
			else
				strMsg = "";
			break;
		}
	case PS_STEP_NO:
	default:
		strMsg.Format("%d", Value);
		break;
	}
	
	return strMsg;
}

BOOL CCTSMonProDoc::ValueString_CellCode(long lData, int item, CString *srtName, CString *strDesc) //yulee 20190814
{
	CString strMsg = "", strDBDesc = "",tmpStr ="";
	long Value = lData;
	tmpStr.Format("%lf", Value);

	switch(item)
	{
	case PS_CODE:	//failureCode
		PSCellCodeMsg((BYTE)Value, strMsg, strDBDesc);
		break;
	default:
		srtName->Format("%d", Value);
		break;
	}

	if((lData != 0)&&(srtName->CompareNoCase(tmpStr) == FALSE)&&(strDBDesc.IsEmpty() == FALSE))
	{
		srtName->Format("%s", strMsg);
		strDesc->Format("%s", strDBDesc);
		return TRUE;
	}
	else 
		return FALSE;
}


BOOL CCTSMonProDoc::SetColorConfig()
{
	::PSGetColorCfgData(TRUE);		//pscommon.dll

	return TRUE;
}

CString CCTSMonProDoc::GetCmdString(int nCmd)
{
	CString msg;
	switch(nCmd)
	{
	//case SFT_CMD_RUN:		return "RUN";
	case SFT_CMD_RUN:		return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg1","CTSMonPro_DOC");//&&
	//case SFT_CMD_STOP:		return "STOP";
	case SFT_CMD_STOP:		return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg2","CTSMonPro_DOC");//&&
	//case SFT_CMD_PAUSE:		return "Pause";
	case SFT_CMD_PAUSE:		return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg3","CTSMonPro_DOC");//&&
	//case SFT_CMD_CONTINUE:	return "Continue";
	case SFT_CMD_CONTINUE:	return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg4","CTSMonPro_DOC");//&&
	//case SFT_CMD_NEXTSTEP:	return "NEXT Step";
	case SFT_CMD_NEXTSTEP:	return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg5","CTSMonPro_DOC");//&&
	//case SFT_CMD_GOTOSTEP:	return "GOTO Step";
	case SFT_CMD_GOTOSTEP:	return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg6","CTSMonPro_DOC");//&&
	//case SFT_CMD_RESET_RESERVED_CMD:	return "Cancle Reserved";
	case SFT_CMD_RESET_RESERVED_CMD:	return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg7","CTSMonPro_DOC");//&&
	//case SFT_CMD_BMS_COMM_REQUEST :		return "BMS TEST";		//ljb	2008-09-04
	case SFT_CMD_BMS_COMM_REQUEST :		return Fun_FindMsg("CTSMonProDoc_GetCmdString_msg8","CTSMonPro_DOC");		//ljb	2008-09-04//&&
	default :
			msg.Format("0x%x", nCmd);
	}
	return msg;
}

CString CCTSMonProDoc::GetEmgMsg(long lCode)
{
	CString strMsg;
	//strMsg ="정보 없음";//$1013
	strMsg =Fun_FindMsg("CTSMonProDoc_GetEmgMsg_msg1","CTSMonPro_DOC");//$1013//&&

/*	switch(lCode)
	{
	case PS_EMG_NONE	:				strMsg = "정상";							break;		
	case PS_EMG_AC_FAIL	:				strMsg = "AC 입력 전원 이상(정전)";			break;	
	case PS_EMG_UPS_BAT_FAIL	:		strMsg = "UPS Battery 부족";				break;	
	case PS_EMG_MAIN_EMG_SWITCH	:		strMsg = "Main 비상 스위치 동작";			break;	
	case PS_EMG_SUB_EMG_SWITCH	:		strMsg = "Sub 비상 스위치 동작";			break;	
	case PS_EMG_SMPS_FAIL		:		strMsg = "SMPS 출력 이상(Main NFB 확인 바람)";					break;	
	case PS_EMG_OT_FAIL			:		strMsg = "회로 과열 발생";					break;	
	case PS_EMG_POWER_SWITCH_RUN:		strMsg = "동작중 Power Off Swtich 누름 ";	break;	
	}
*/	CDaoDatabase  db;
	CString	strDescript;

// 	if(m_strDataBaseName.IsEmpty())		return strMsg;
// 	db.Open(m_strDataBaseName);

	//ksj 20200911 : MDB 파일 자동 선택 기능
	CString strMDBPath;
	CFileStatus fs;
	if( CFile::GetStatus(m_strCodeDataBaseName, fs) ) //Pack_Code_2000.mdb 파일이 있으면 해당 파일에서 EMG 코드 읽어온다.
	{
		strMDBPath = m_strCodeDataBaseName;
	}
	else //없으면 기존 파일에서 그대로 읽어옴.
	{
		strMDBPath = m_strDataBaseName;
	}
	
	if(strMDBPath.IsEmpty())		return strMsg;
	db.Open(strMDBPath);	
	//ksj end
	
	//////////////////////////////////////////////////////////////////////////
	//EMGCode Cycler Emg Code Table
	CString strTemp;
	
	switch(gi_Language) //20190701
	{
	case 1 : strTemp.Format("SELECT Message, Description FROM EmgCode_ko WHERE Code = %d", lCode); break;
	case 2 : strTemp.Format("SELECT Message, Description FROM EmgCode_en WHERE Code = %d", lCode); break;
	case 3 : strTemp.Format("SELECT Message, Description FROM EmgCode_pl WHERE Code = %d", lCode); break;
	default : strTemp.Format("SELECT Message, Description FROM EmgCode_en WHERE Code = %d", lCode); break;
	}

	//strTemp.Format("SELECT Message, Description FROM EmgCode WHERE Code = %d", lCode); break;

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strTemp, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return strMsg;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		data = rs.GetFieldValue(0);
		strMsg = data.pbVal;
		data = rs.GetFieldValue(1);
		strDescript = data.pbVal;
		strTemp.Format("%s", strMsg);
	}
	else
	{
		strTemp.Format("%s[Code %d]", strMsg, lCode);
	}

	rs.Close();
	db.Close();
	return strTemp;
}

BOOL CCTSMonProDoc::SaveRunInfoToTempFile(int nModuleID, CWordArray *pSelChArray)
{
	if(pSelChArray == NULL )	return FALSE;

	CCyclerChannel *pCh;

	for(int ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pCh = GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pCh)
		{
			if(pCh->SaveRunInfoToTempFile() == FALSE)
			{
				//pCh->WriteLog("작업 시작 정보 저장 실패");//$1013
				pCh->WriteLog(Fun_FindMsg("CTSMonProDoc_SaveRunInfoToTempFile_msg1","CTSMonPro_DOC"));//&&
			}
			else
			{
				//pCh->WriteLog("작업 시작 정보 저장 성공");//$1013
				pCh->WriteLog(Fun_FindMsg("CTSMonProDoc_SaveRunInfoToTempFile_msg2","CTSMonPro_DOC"));//&&
			}
		}
	}


/*	CString	strMsg;
	CCyclerModule *pMD;
	pMD = GetModuleInfo(nModuleID);
	int chIndex;

	CT_TEMP_FILE_DATA tempData;
	ZeroMemory(&tempData, sizeof(CT_TEMP_FILE_DATA));

	FILE *fp;
	for(int ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		chIndex = pSelChArray->GetAt(ch);
		strMsg.Format("%s\\Temp\\M%02dC%02d.tmp", m_strCurPath, nModuleID, chIndex+1);
		fp = fopen(strMsg, "wt");

		if(fp == NULL)	continue;
		pCh = pMD->GetChannelInfo(chIndex);
		
		if(pCh == FALSE)			continue;
		
		sprintf(tempData.szFilePath, "%s", pCh->GetFilePath());
		sprintf(tempData.szLot, "%s", pCh->GetTestSerial());
		sprintf(tempData.szWorker, "%s", pCh->GetWorkerName());
		sprintf(tempData.szDescript, "%s", pCh->GetTestDescript());
		fwrite(&tempData, sizeof(CT_TEMP_FILE_DATA), 1, fp);
		fclose(fp);

	}
*/
	return TRUE;
}

BOOL CCTSMonProDoc::SendContinueCmd(int nModule, CWordArray *pSelChArray)
{
	if(pSelChArray == NULL )	return FALSE;

	CString	strMsg;
	CCyclerChannel *pCh;
	int chIndex;

	for(int ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		chIndex = pSelChArray->GetAt(ch);
		pCh = GetChannelInfo(nModule, chIndex);
		if(pCh == NULL)			continue;
		
		//최종 상태를 다시 검사, 파일 저장 위치가 없으면 CTSMonPro Program을 다시 시작 한 것이므로 Temp에서 정보 Loading
		//모듈에 채널 Data가 전송되면 void CCTSMonProView::OnChannelDataReceived(WPARAM wParam, LPARAM lParam)에서 처리 
		if(pCh->GetFilePath().IsEmpty())
		{
			pCh->LoadInfoFromTempFile();
		}
		else
		{
			//채널 상태가 이상 ?
		}
	}

	BOOL nRtn = SendCommand(nModule, pSelChArray, SFT_CMD_CONTINUE);
	return nRtn;
}

BOOL CCTSMonProDoc::ExecuteExcel(CString strFileName)
{
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	strTemp = GetExcelPath();
	if(strTemp.IsEmpty())	return FALSE;

	char buff1[_MAX_PATH];
	char buff2[_MAX_PATH];

	int aa =0;
/*		do {
	aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
		if(aa <= 0)
		{
			if(AfxMessageBox("Excel 실행파일 경로가 잘못되었습니다. 경로를 설정 하십시요.", MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			{
				return FALSE;

			}
			strTemp = GetExcelPath();
			if(strTemp.IsEmpty())	return FALSE;
		}
		
	} while( aa <= 0 );	
*/
	
	aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
	aa = GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, _MAX_PATH);
	strTemp.Format("%s %s", buff2, buff1);
	//BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);

	//20180315 yulee office2016 호환
	BOOL bFlag;
	SHELLEXECUTEINFO lpExecInfo;
	memset(&lpExecInfo,0, sizeof(SHELLEXECUTEINFO));
	lpExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strFileName;
	
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST | SEE_MASK_NOCLOSEPROCESS;
	lpExecInfo.hwnd = NULL;
	lpExecInfo.lpVerb = _T("open");
	lpExecInfo.lpDirectory=NULL;
	lpExecInfo.nShow = SW_SHOW;
	lpExecInfo.hInstApp = (HINSTANCE)SE_ERR_DDEFAIL;
	
    bFlag = ShellExecuteEx(&lpExecInfo);

	if(bFlag == FALSE)
	{
		//strTemp.Format("%s를 Open할 수 없습니다.", strFileName);//$1013
		strTemp.Format(Fun_FindMsg("CTSMonProDoc_ExecuteExcel_msg1","CTSMonPro_DOC"), strFileName);//&&
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
	}
	return TRUE;
}

CString CCTSMonProDoc::GetExcelPath()
{
	CString strExcelPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Excel Path");
	TRACE("Excel Path: %s", strExcelPath);
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		strExcelPath = "C:\\Program Files\\Microsoft Office\\OFFICE11\\EXCEL.EXE";
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		//pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";//$1013
		pDlg.m_ofn.lpstrTitle = Fun_FindMsg("CTSMonProDoc_GetExcelPath_msg1","CTSMonPro_DOC");//&&
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC,"Excel Path", strExcelPath);
	}
	return strExcelPath;
}
/*
//모듈이 접속하면 각 채널의 상태를 검사하여
//Run이거나 Pause 이면 이전의 최종 시험 정보를 Loading한다.
void CCTSMonProDoc::CheckRunningChannel(int nModuleID)
{
	CString strMsg;
	CCyclerModule *pMD;
	CCyclerChannel *pCh;
	pMD = GetModuleInfo(nModuleID);
	if(pMD == NULL)		return;

	for(int i=0; i<pMD->GetTotalChannel(); i++)
	{
		pCh = pMD->GetChannelInfo(i);
		if(pCh)
		{
			WORD state = pCh->GetState();
			//채널 상태가 Run이거나 Pause 이면 이전 최종 시험 상태를 Loading한다.
			if(pCh->GetState() == PS_STATE_RUN || pCh->GetState() == PS_STATE_PAUSE)
			{
				strMsg.Format("%s\\Temp\\M%02dC%02d.tmp", m_strCurPath, nModuleID, i+1);
				if(pCh->LoadInfoFromTempFile(strMsg) == FALSE)
				{
					strMsg.Format("%s Ch %d 이전 시험정보 임시파일 Loading 실패", GetModuleName(nModuleID), i+1);
					WriteLog(strMsg);
				}
			}
		}
	}
}
*/

void CCTSMonProDoc::UpdateUnitSetting()
{
	char szBuff[32], szUnit[16], szDecimalPoint[16];

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strVUnit = szUnit;
	m_nVDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strIUnit = szUnit;
	m_nIDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strCUnit = szUnit;
	m_nCDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWUnit = szUnit;
	m_nWDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_strWhUnit = szUnit;
	m_nWhDecimal = atoi(szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Time Unit", "0"));
	m_nTimeUnit = atol(szBuff);
}


CString CCTSMonProDoc::GetDataUnit(int nItem)
{
	switch(nItem)
	{
	case PS_VOLTAGE:	return m_strVUnit;
	case PS_CURRENT:	return m_strIUnit;
	case PS_CHARGE_CAP:
	case PS_DISCHARGE_CAP:
	case PS_CAPACITY_SUM:
	case PS_CAPACITY:	return m_strCUnit;
	case PS_WATT:		return m_strWUnit;
	case PS_WATT_HOUR:	return m_strWhUnit;
	case PS_STEP_TIME:
	case PS_TOT_TIME:	
		if(m_nTimeUnit == 1)	//sec 표시
		{
			return "Sec";
		}
		else if(m_nTimeUnit == 2)	//Minute 표시
		{
			return "Min";
		}
		else
		{
			return "";
		}
	case PS_IMPEDANCE:	return "mΩ";
	default:	return	"";
	}

	return "";
}

//SBC로 병렬 모드 설정을 전송한다.
int CCTSMonProDoc::SendParallelDatatoModule(UINT nModuleID)
{
	int nRtn;

	CCyclerModule * pMD = GetModuleInfo(nModuleID);

	if((nRtn = pMD->SendCommand(SFT_CMD_PARALLEL_DATA , pMD->GetParallelData(), sizeof(SFT_MD_PARALLEL_DATA) * _SFT_MAX_PARALLEL_COUNT)) != SFT_ACK)
	{		
#ifdef _DEBUG //ksj 20200117 :임시 디버깅 코드
		if(nRtn == SFT_TIMEOUT) //디버그모드에서 왜 타임아웃뜨는지.. 일단 임시 허용
			return TRUE; 
#else
		//AfxMessageBox("병렬 모드 설정 명령 전달 실패!");//$1013
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendParallelDatatoModule_msg1","CTSMonPro_DOC"));//&&
		return FALSE;
#endif
	}
	return nRtn;
}


//SBC 로 CAN Command 설정을 전송한다.
int CCTSMonProDoc::SendCanDatatoModule(UINT nModuleID, STF_MD_CAN_SET_DATA * canCommandData)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	nRtn = pMD->SendCommand(SFT_CMD_BMS_COMM_REQUEST , canCommandData, sizeof(STF_MD_CAN_SET_DATA));
// 	{
// 		AfxMessageBox("CAN Data 설정 명령 전달 실패!");
// 		return FALSE;
// 	}
	
	return nRtn;
}
//SBC 로 Aux Data 설정을 전송한다.
int CCTSMonProDoc::SendAuxDatatoModule(UINT nModuleID, STF_MD_AUX_SET_DATA * auxData)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);

	if((nRtn = pMD->SendCommand(SFT_CMD_AUX_SET_DATA , auxData, sizeof(STF_MD_AUX_SET_DATA)*_SFT_MAX_MAPPING_AUX)) != SFT_ACK)
	{
#ifdef _DEBUG //ksj 20200117 :임시 디버깅 코드
		if(nRtn == SFT_TIMEOUT) //디버그모드에서 왜 타임아웃뜨는지.. 일단 임시 허용
			return TRUE; 
#else
		//AfxMessageBox("Aux Data 설정 명령 전달 실패!");//$1013
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendAuxDatatoModule_msg1","CTSMonPro_DOC"));//&&

		if(nRtn == SFT_TIMEOUT) //디버그모드에서 왜 타임아웃뜨는지.. 일단 임시 허용
		{
			CString strTemp;
			strTemp.Format("Response Timeout : %dms",SFT_MSG_TIMEOUT);			
			AfxMessageBox(strTemp);
		}
		else
		{
			CString strTemp;
			strTemp.Format("Response error code : %d",nRtn);
			AfxMessageBox(strTemp);
		}
		return FALSE;
#endif

	}
	
	return nRtn;
}

//SBC 로 Aux Data 설정을 전송한다.
int CCTSMonProDoc::SendAuxDatatoModule1015(UINT nModuleID, STF_MD_AUX_SET_DATA_V1015 * auxData)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	if((nRtn = pMD->SendCommand(SFT_CMD_AUX_SET_DATA , auxData, sizeof(STF_MD_AUX_SET_DATA_V1015)*_SFT_MAX_MAPPING_AUX)) != SFT_ACK)
	{
#ifdef _DEBUG //ksj 20200117 :임시 디버깅 코드
		if(nRtn == SFT_TIMEOUT) //디버그모드에서 왜 타임아웃뜨는지.. 일단 임시 허용
			return TRUE; 
#else
		//AfxMessageBox("Aux Data 설정 명령 전달 실패!");//$1013
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendAuxDatatoModule_msg1","CTSMonPro_DOC"));//&&
		return FALSE;
#endif

	}

	return nRtn;
}

BOOL CCTSMonProDoc::LoadPatternDataFromFileForFtp(int nModuleID, CString strFileName, long &lTimeType, long &lModeType, long &lPlusMode)	//ljb 20130404 add
{
	CCyclerModule * pMD = GetModuleInfo(nModuleID);
	if(pMD == NULL)
	{
		//AfxMessageBox("Module 정보를 알수 없습니다.");//$1013
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_LoadPatternDataFromFileForFtp_msg1","CTSMonPro_DOC"));//&&
		return FALSE;
	}

	long lMaxVoltage = pMD->GetVoltageSpec() / 1000;
	long lMaxCurrent = pMD->GetCurrentSpec() / 1000;
	long lMaxWatt	= lMaxVoltage * lMaxCurrent;
	
	CStdioFile aFile;
	CFileException e;

	if( !aFile.Open( strFileName, CFile::modeRead, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   //m_strLastErrorString = "Simulation 파일 열기에 실패했습니다.";//$1013
		   m_strLastErrorString = Fun_FindMsg("CTSMonProDoc_LoadPatternDataFromFileForFtp_msg2","CTSMonPro_DOC");//&&
		   return FALSE;
	}

	CStringList		strDataList;
	CString			strTemp, strTime, strValue, strData;
	int				nPos = 0;
	int				p1=0, s=0;
	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == "STX")			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			aFile.ReadString(strTemp);			//Read Title

			CString strTimeType = strTemp.Left(strTemp.Find(","));
			if(strTimeType == "T1" || strTimeType == "t1" ||
				strTimeType == "T" || strTimeType == "t")
				lTimeType = 1;//
			else if(strTimeType == "T2" || strTimeType == "t2")
				lTimeType = 2;//

			CString strType = strTemp.Mid(strTemp.Find(",", 0)+1, 1);
			if(strType == "I" || strType == "i")
				lModeType = 1;
			else if(strType == "P" || strType == "p")
				lModeType = 2;

			//에디터에서 시간 역전과 MAX value 값을 체크 한다. Max Line 5만 체크
		}	
	}	
	aFile.Close();

	return TRUE;
}

BOOL CCTSMonProDoc::Fun_MemoryCopyFileAndCheckSum(CString strScrFileName, CString strDscFileName, ULONG &ulFileSize, ULONG &ulCheckSum)
{
	//ljb 20130514 메모리 맵을 이용한 파일정보 가져 오기
	ulFileSize = ulCheckSum = 0;
	HANDLE hFile, hFileMap;
	DWORD dwFileSize,dwCnt;
	char *szbuff = NULL;
	unsigned char cTemp;
//	CString strCode;
	
	// 읽기 전용으로 lpszPathName인 파일을 읽는다.
	hFile = CreateFile((LPSTR)(LPCSTR)strScrFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if( hFile == INVALID_HANDLE_VALUE )
	{
		AfxMessageBox("File Handle invalid value. Failed file open\n", MB_OK | MB_ICONERROR);
		return FALSE;
	}
	
	ulFileSize = dwFileSize = GetFileSize(hFile, NULL);
	
	// dwFileSize 만큼 메모리에 쓴다.
	hFileMap = CreateFileMapping(hFile, NULL, PAGE_WRITECOPY, 0, dwFileSize, NULL);
	if( hFileMap == NULL )
	{
		AfxMessageBox("FileMap Handle is NULL. Failed file open\n", MB_OK | MB_ICONERROR );
		CloseHandle(hFile);
		return FALSE;
	}
	
	szbuff = (char *)MapViewOfFile(hFileMap, FILE_MAP_COPY, 0, 0, 0);

	for (dwCnt=0; dwCnt < dwFileSize; dwCnt++)
	{
		cTemp = (unsigned char)szbuff[dwCnt];
		ulCheckSum += (int)cTemp;			//checksum

	}

	if (strDscFileName.IsEmpty() == FALSE)
	{
		CFile f;
		CFileException e;

		//파일 유뮤 검색
		bool bCreateFile = TRUE;
		UINT nID = 0;
		CFileFind aFileFinder;
		if ( aFileFinder.FindFile(strDscFileName) == TRUE )
		{
			if (DeleteFile(strDscFileName) == FALSE)
			{
				AfxMessageBox("file delete error");
				bCreateFile = FALSE;
			}
		}
		aFileFinder.Close();
	
		if (bCreateFile)
		{
			if( !f.Open( strDscFileName, CFile::modeCreate | CFile::modeWrite, &e ) )
			{
			#ifdef _DEBUG
			   afxDump << "File could not be opened " << e.m_cause << "\n";
			#endif
			   AfxMessageBox("file create error");
			   return FALSE;
			}
			f.Write(szbuff, dwFileSize);
			f.Flush();
			f.Close();
		}
	}

// Anal test		S
	UnmapViewOfFile(szbuff);	//ljb 20130611 add
// Anal test		E	
	CloseHandle(hFileMap);
	CloseHandle(hFile);
	return TRUE;
}

BOOL CCTSMonProDoc::LoadPatternDataFromFileForCheckSum(CString strFileName, long &lFileSize, long &lCheckSum)
{
	FILE *fp;
    unsigned char *buffer = NULL;
	unsigned char cTemp;

	long lTemp=0;
    long FileSize = 0;
	int iCheckSum=0;
	lFileSize = lCheckSum = 0;

	fp = fopen( strFileName, "rb" );		
	if( !fp )
	{
		throw "File Not Found!";		
		return FALSE;
	}
	lFileSize = FileSize = filelength( fileno(fp) );		

	buffer = new unsigned char [FileSize];//lmh 20120612 해제해야함		
	fread( buffer, FileSize, 1, fp );		
	fclose( fp );
	
	if(FileSize < 1) 
	{
		delete buffer;
		buffer = NULL;
		return FALSE;
	}
	for (long lcnt=0; lcnt < FileSize; lcnt++)
	{
		cTemp = buffer[lcnt];
		iCheckSum += (int)cTemp;			//checksum
	}
	lCheckSum = iCheckSum;

	delete buffer;
	buffer = NULL;

	return TRUE;
}

// BOOL CCTSMonProDoc::LoadSbcSchedule(CString strFileName)
// {
// 
// 	long lFileSize,lCheckSum;
// 	CString strScr,strDsc,strMsg;
// 
// 	int i;
// 	for (i=1; i<11; i++)
// 	{
// 	
// 		//결과 폴더에 패턴 파일을 복사 한다.
// 		strScr.Format("c:\\pattern_%d.csv",i);
// 		strDsc.Format("c:\\temp\\sbc_pattern_data_step_%d.csv",i);
// // 			strDsc.Format("%s\\Pattern\\sbc_pattern_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
// 			if( ::CopyFile(strScr,strDsc,FALSE) == FALSE)
// 			{
// 				strMsg.Format("1: Pattern File Copy 실패!!! (%s -> %s)",strScr, strDsc);
// 				AfxMessageBox(strMsg);
// 				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 				return FALSE;
// 			}
// 			strMsg.Format("Pattern 1 : %s 에서 %s 로 복사 성공 !!!", strScr, strDsc);
// 			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 			
// 			//ljb 20130430 File Size, checksum
// 			if(LoadPatternDataFromFileForCheckSum(strDsc, lFileSize, lCheckSum) == FALSE)
// 				//if(LoadPatternDataFromFileForCheckSum(strDsc2, lFileSize, lCheckSum) == FALSE)
// 			{
// 				strMsg.Format(" Simulation File Size, checksum 실패!!! %s (File size = %d, CheckSum = %d",  m_strLastErrorString,lFileSize,lCheckSum);
// 				AfxMessageBox(strMsg);
// 				WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 				return FALSE;
// 			}
// 			strMsg.Format(" Simulation File Size, checksum 성공 !!! %s (File size = %d, CheckSum = %d", m_strLastErrorString,lFileSize,lCheckSum);
// 			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 	}
// 
// 
// 
// 	return TRUE;
// 
// 	FILE *fp = fopen(strFileName, "rb");
// 	if(fp == NULL)
// 	{
// 		AfxMessageBox("file open error");
// 		return FALSE;
// 	}
// /*	PS_FILE_ID_HEADER	FileHeader;
// 	ZeroMemory(&FileHeader, sizeof(PS_FILE_ID_HEADER));
// 	
// 	COleDateTime dateTime = COleDateTime::GetCurrentTime();
// 	sprintf(FileHeader.szCreateDateTime, "%s", dateTime.Format());			//기록 시간
// 	FileHeader.nFileID = SCHEDULE_FILE_ID;									//파일 ID
// 	FileHeader.nFileVersion = SCHEDULE_FILE_VER;							//파일 Version
// 	sprintf(FileHeader.szDescrition, "PNE power supply schedule file.");	//파일 서명 
// 	fwrite(&FileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp);					//기록 실시 
// 	
// 	//모델 정보 기록 
// 	fwrite(&m_ModelData, sizeof(m_ModelData), 1, fp);				//기록 실시 
// 	
// 	//공정 정보 기록 
// 	fwrite(&m_ProcedureData, sizeof(m_ProcedureData), 1, fp);		//기록 실시 
// 	
// 	//cell check 조건 기록 
// 	fwrite(&m_CellCheck, sizeof(m_CellCheck), 1, fp);				//기록 실시 
// */	
// 	PS_FILE_ID_HEADER pFileHeader;
// 	SCHDULE_INFORMATION_DATA	pT1;
// 	SCHDULE_INFORMATION_DATA	pT2;
// 	CELL_CHECK_DATA	pT3;
// 
// 	SFT_STEP_START_INFO		pT4;
// 	SFT_TEST_SAFETY_SET		pT5;
// 	SFT_STEP_CONDITION_v7	pT6;
// 	SFT_STEP_CONDITION_v7	pT7;
// 
// 	CString strTemp;
// // 	strTemp.Format("%d,%d,%d,%d", sizeof(PS_FILE_ID_HEADER), sizeof(SCHDULE_INFORMATION_DATA), sizeof(SCHDULE_INFORMATION_DATA), sizeof(CELL_CHECK_DATA));
// // 	AfxMessageBox(strTemp);
// // 
// // 	strTemp.Format("%d", sizeof(SFT_STEP_START_INFO));
// // 	AfxMessageBox(strTemp);
// 	
// 	if(fread(&pFileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	if(fread(&pT1, sizeof(SCHDULE_INFORMATION_DATA), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	if(fread(&pT2, sizeof(SCHDULE_INFORMATION_DATA), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	if(fread(&pT3, sizeof(CELL_CHECK_DATA), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	if(fread(&pT4, sizeof(SFT_STEP_START_INFO), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	if(fread(&pT5, sizeof(SFT_TEST_SAFETY_SET), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	if(fread(&pT6, sizeof(SFT_STEP_CONDITION_v7), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	if(fread(&pT7, sizeof(SFT_STEP_CONDITION_v7), 1, fp) < 1)
// 	{
// 		fclose(fp);
// 		return FALSE;
// 	}
// 	fclose(fp);
// 	
// 	
// 	
// 	return TRUE;
// }

//Simulation 데이터를 파일로부터 불러온다.
BOOL CCTSMonProDoc::LoadPatternDataFromFile(CString strFileName, STF_MD_USERPATTERN_DATA & patternData)
{
	CFile file;
	char * patternFile;
	TRY
	{
	file.Open(strFileName, CFile::modeRead);
	patternData.lLength = file.GetLength();	
	TRACE("file size = %d \n", patternData.lLength);

	patternFile = new char[patternData.lLength+1];	
	ZeroMemory(patternFile, sizeof(char)*patternData.lLength+1);
	file.Read(patternFile, patternData.lLength);	

	CString strTemp(patternFile);
	CString strPattern(strTemp.Mid(strTemp.ReverseFind('T')));

	strPattern.TrimRight();			//파일끝에 있을지 모를 공백 제거
	strPattern += "\r\nEOF\0";		//파일에 끝에 EOF 문자열 추가

	TRACE("strPattern size = %d \n", strPattern.GetLength());
//	TRACE("%d", strPattern.GetLength());

	//파일의 길이를 4의 배수로 만들기/////////////////////////////////
	long lLength = strPattern.GetLength() % 4;	

	if(lLength !=0)
		patternData.lLength = strPattern.GetLength() + 4 - lLength;
	else
		patternData.lLength = strPattern.GetLength();

	TRACE("patternData size = %d \n", patternData.lLength);
	//////////////////////////////////////////////////////////////////

	patternData.pPatternData = new char[patternData.lLength+1];
	ZeroMemory(patternData.pPatternData, sizeof(char)*patternData.lLength);
	sprintf(patternData.pPatternData,strPattern.GetBuffer(0), sizeof(char)*patternData.lLength );

	file.Close();
	TRACE("\nPattern Length = %d",patternData.lLength);
	TRACE("\nPattern Data = %s",strPattern.Left(50));
	TRACE("\nPattern Data = %s",strPattern.Right(50));

	//ljb 20130423 add 패턴 데이터 정상으로 전송 했는지 확인 용
	strTemp.Format("Pattern file (%s), Length = %d, LeftData = %s, RightData = %s !!!",strFileName,patternData.lLength,strPattern.Left(50),strPattern.Right(50));
	WriteLog(strTemp, CT_LOG_LEVEL_NORMAL);

	strPattern.ReleaseBuffer();

	delete patternFile;

	}
	CATCH(CFileException, e)
	{
		e->ReportError();
		return FALSE;
	}
	END_CATCH

	return TRUE;
}
										
// SFT_STEP_SIMUL_CONDITION CCTSMonProDoc::GetNetworkFormatStepSimul(CStep *pStep)
// {	
// 	SFT_STEP_SIMUL_CONDITION	netStep;
// 	ZeroMemory(&netStep, sizeof(SFT_STEP_SIMUL_CONDITION));
// 	
// 	if(pStep != NULL)
// 	{
// 		netStep.stepHeader.nStepTypeID	= pStep->m_type;					//Step Type
// //		netStep.stepHeader.nStepTypeID	= pStep->m_lProcType;
// 		netStep.stepHeader.stepNo		= (BYTE)(pStep->m_StepIndex+1);		//StepNo
// 		netStep.stepHeader.mode			= pStep->m_mode;
// 		netStep.stepHeader.nSubStep		= 1;								//1개의 Sub Step만 이용한다.2005/08/08
// 		netStep.stepHeader.bTestEnd		= 0;
// //		netStep.stepHeader.bUseSocFlag = pStep->m_bUseActualCapa;
// 
// 		netStep.stepReferance[0].lVref_Charge	= FLOAT2LONG(pStep->m_fVref_Charge);
// 		netStep.stepReferance[0].lVref_DisCharge	= FLOAT2LONG(pStep->m_fVref_DisCharge);
// 		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
// 		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
// 		netStep.stepReferance[0].lRref	= FLOAT2LONG(pStep->m_fRref);
// 		
// 		//ljb 20150820 챔버연동 기능 (1: 챔버연동 대기, 0: 챔버연동 대기 없이 진행)
// 		if (pStep->m_fTref == 0)
// 			netStep.stepReferance[0].bChamberlinkOption	= 0;
// 		else
// 			netStep.stepReferance[0].bChamberlinkOption	= 1;
// 		
// 		//Power 제어값이 들어 있다. (Power 단위는 mW이므로 )
// 		//CR 제어값이 들어 있다. (CR 모드에서 R은 mOhm)
// // 		if( (pStep->m_mode == PS_MODE_CP || pStep->m_mode == PS_MODE_CR)&& pStep->m_type == PS_STEP_CHARGE && pStep->m_type == PS_STEP_DISCHARGE)
// // 		{
// //     		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
// // 		}
// // 		//전류 단위는 uA단위
// // 		else	
// // 		{
// //      		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
// // 		}
// 		netStep.stepReferance[0].byRange	= pStep->m_lRange;	
// 		netStep.stepReferance[0].ulEndTime	= MDTIME(pStep->m_fEndTime);
//     	netStep.stepReferance[0].lEndV_H		= FLOAT2LONG(pStep->m_fEndV_H);		//ljb v1009
// 		netStep.stepReferance[0].lEndV_L		= FLOAT2LONG(pStep->m_fEndV_L);		//ljb v1009
//     	netStep.stepReferance[0].lEndI		= FLOAT2LONG(pStep->m_fEndI);
// #ifdef _EDLC_TEST_SYSTEM
//     	netStep.stepReferance[0].lEndC		= long(pStep->m_fEndC);
// #else
//     	netStep.stepReferance[0].lEndC		= FLOAT2LONG(pStep->m_fEndC);
// #endif
// 		netStep.stepReferance[0].lGoto		= pStep->m_nLoopInfoGotoStep;	
// 		netStep.stepReferance[0].lCycleCount= pStep->m_nLoopInfoCycle;	
//     	netStep.stepReferance[0].lDeltaV	= FLOAT2LONG(pStep->m_fEndDV);
// 		
// 		netStep.stepReferance[0].bValueItem = pStep->m_bValueItem;		//ljb v1009 Valu Item
// 		netStep.stepReferance[0].wValueRate	= pStep->m_fValueRate *100;
// 		netStep.stepReferance[0].bValueStepNo = pStep->m_bValueStepNo;		
// 		netStep.stepReferance[0].bValueRate_Goto = pStep->m_bValueStepNo;
// 
// 		netStep.stepReferance[0].lEndWatt   = LONG(pStep->m_fEndW);
// 		TRACE("lEndWatt(Simul): %ld\r\n", netStep.stepReferance[0].lEndWatt);
// 		netStep.stepReferance[0].lEndWattHour = LONG(pStep->m_fEndWh);
// 		TRACE("lEndWattHour(Simul): %ld\r\n", netStep.stepReferance[0].lEndWattHour);
// 		netStep.stepReferance[0].lReserved[0] = FLOAT2LONG(pStep->m_fEndDI);
// //    	netStep.stepReferance[0].lEndData3	= FLOAT2LONG(pStep->m_fEndDI);
// 
// 		netStep.lHighV	= FLOAT2LONG(pStep->m_fHighLimitV);
// 		netStep.lLowV	= FLOAT2LONG(pStep->m_fLowLimitV);
// 		netStep.lHighI	= FLOAT2LONG(pStep->m_fHighLimitI);
// 		netStep.lLowI	= FLOAT2LONG(pStep->m_fLowLimitI);
// #ifdef _EDLC_TEST_SYSTEM
// 		netStep.lHighC	= long(pStep->m_fHighLimitC);		//mF단위
// 		netStep.lLowC	= long(pStep->m_fLowLimitC);		//mF단위
// #else
// 		netStep.lHighC	= FLOAT2LONG(pStep->m_fHighLimitC);		//uAh단위
// 		netStep.lLowC	= FLOAT2LONG(pStep->m_fLowLimitC);		//uAh단위 
// #endif
// 		netStep.lHighZ	= FLOAT2LONG(pStep->m_fHighLimitImp);
// 		netStep.lLowZ	= FLOAT2LONG(pStep->m_fLowLimitImp);
// 		netStep.lHighTemp = FLOAT2LONG(pStep->m_fHighLimitTemp);
// 		netStep.lLowTemp = FLOAT2LONG(pStep->m_fLowLimitTemp);
// 	 
// 		netStep.conREC.lTime	= MDTIME(pStep->m_fReportTime);
// 		netStep.conREC.lDeltaV	= FLOAT2LONG(pStep->m_fReportV);
// 		netStep.conREC.lDeltaI	= FLOAT2LONG(pStep->m_fReportI);
// 		netStep.conREC.lDeltaT	= FLOAT2LONG(pStep->m_fReportTemp);
// 		netStep.conREC.lDeltaP	= 0;
// 		netStep.conREC.lReserved =0;
// 	 
// 	}
// 	return netStep;
// }


float CCTSMonProDoc::TUnitTrans(float fData, BOOL bSave)
{
	if(bSave)
		return fData * DIGIT_NUMBER_TEMPERATURE;
	else 
		return fData / DIGIT_NUMBER_TEMPERATURE;
}

float CCTSMonProDoc::VUnitTrans(float fData, BOOL bSave)
{
	if(bSave)
		return fData * DIGIT_NUMBER_VOLTAGE;
	else 
		return fData / DIGIT_NUMBER_VOLTAGE;
}


float CCTSMonProDoc::UnitTrans(float fData, BOOL bSave, int nAuxType)
{
	switch(nAuxType)
	{
	case PS_AUX_TYPE_TEMPERATURE:		return TUnitTrans(fData, bSave);
	case PS_AUX_TYPE_VOLTAGE:			return VUnitTrans(fData, bSave);
	case PS_AUX_TYPE_TEMPERATURE_TH:	return VUnitTrans(fData, bSave); // ijb 20160707
	case PS_AUX_TYPE_HUMIDITY:		return VUnitTrans(fData, bSave); //ksj 20200116 : V1016 습도 추가. 단위 처리 향후 확인 필요.
	}
	return 0.0f;
}

UINT CCTSMonProDoc::GetStateImageIndexA(WORD state, WORD nType, WORD nParallelType)
{
	switch(nParallelType)
	{
	case 0:
		switch(state)
		{
			case PS_STATE_IDLE		:		return	0x0000;
			case PS_STATE_STANDBY	:		return	0x000C;
			case PS_STATE_PAUSE	:			return 	0x0003;
			case PS_STATE_MAINTENANCE:		return 	0x0008;
			case PS_STATE_RUN		:		
				{
					switch(nType)
					{
							case PS_STEP_CHARGE		:			return 	0x0002;
							case PS_STEP_DISCHARGE	:			return 	0x0001;
							case PS_STEP_OCV		:			return 	0x0007;
							case PS_STEP_REST		:			return 	0x000B;
							case PS_STEP_IMPEDANCE	:			return 	0x000A;
							case PS_STEP_EXT_CAN	 :			return 	0x0011;		//ljb 2011322 이재복 //////////
							default					:			return	0x0009;
					}
				}
			case PS_STATE_SELF_TEST:		return 	0x0006;
			case PS_STATE_FAIL		:		return 	0x0006;
			case PS_STATE_CHECK	:			return 	0x0009;
			case PS_STATE_STOP		:		return 	0x0004;
			case PS_STATE_END		:		return 	0x000E;
			case PS_STATE_FAULT	:			return 	0x0005;
			case PS_STATE_NONCELL	:		return 	0x0006;
			case PS_STATE_READY	:			return 	0x0010;
			default:						return	0x0000;
		}
	case 1:
		switch(state)
		{
			case PS_STATE_IDLE		:		return	0x0011;
			case PS_STATE_STANDBY	:		return	0x001D;
			case PS_STATE_PAUSE	:			return 	0x0014;
			case PS_STATE_MAINTENANCE:		return 	0x0018;
			case PS_STATE_RUN		:		
				{
					switch(nType)
					{
							case PS_STEP_CHARGE		:			return 	0x0013;
							case PS_STEP_DISCHARGE	:			return 	0x0012;
							case PS_STEP_OCV		:			return 	0x0018;
							case PS_STEP_REST		:			return 	0x001C;
							case PS_STEP_IMPEDANCE	:			return 	0x001B;
							case PS_STEP_EXT_CAN	 :			return 	0x0011;		//ljb 2011322 이재복 //////////
							default					:			return	0x001A;
					}
				}
			case PS_STATE_SELF_TEST:		return 	0x0017;
			case PS_STATE_FAIL		:		return 	0x0017;
			case PS_STATE_CHECK	:			return 	0x001A;
			case PS_STATE_STOP		:		return 	0x0015;
			case PS_STATE_END		:		return 	0x001F;
			case PS_STATE_FAULT	:			return 	0x0016;
			case PS_STATE_NONCELL	:		return 	0x0017;
			case PS_STATE_READY	:			return 	0x0021;
			default:						return	0x0011;
		}
	case 2:
		switch(state)
		{
			case PS_STATE_IDLE		:		return	0x0022;
			case PS_STATE_STANDBY	:		return	0x002E;
			case PS_STATE_PAUSE	:			return 	0x0025;
			case PS_STATE_MAINTENANCE:		return 	0x002A;
			case PS_STATE_RUN		:		
				{
					switch(nType)
					{
							case PS_STEP_CHARGE		:			return 	0x0024;
							case PS_STEP_DISCHARGE	:			return 	0x0023;
							case PS_STEP_OCV		:			return 	0x0029;
							case PS_STEP_REST		:			return 	0x002D;
							case PS_STEP_IMPEDANCE	:			return 	0x002C;
							case PS_STEP_EXT_CAN	:			return 	0x0011;		//ljb 2011322 이재복 //////////
							default					:			return	0x002B;
					}
				}
			case PS_STATE_SELF_TEST:		return 	0x0028;
			case PS_STATE_FAIL		:		return 	0x0028;
			case PS_STATE_CHECK	:			return 	0x002B;
			case PS_STATE_STOP		:		return 	0x0026;
			case PS_STATE_END		:		return 	0x0030;
			case PS_STATE_FAULT	:			return 	0x0027;
			case PS_STATE_NONCELL	:		return 	0x0028;
			case PS_STATE_READY	:			return 	0x0032;
			default:						return	0x0022;
		}
	case 3:
				switch(state)
		{
			case PS_STATE_IDLE		:		return	0x0033;
			case PS_STATE_STANDBY	:		return	0x003E;
			case PS_STATE_PAUSE	:			return 	0x0035;
			case PS_STATE_MAINTENANCE:		return 	0x003B;
			case PS_STATE_RUN		:		
				{
					switch(nType)
					{
							case PS_STEP_CHARGE		:			return 	0x0035;
							case PS_STEP_DISCHARGE	:			return 	0x0034;
							case PS_STEP_OCV		:			return 	0x003A;
							case PS_STEP_REST		:			return 	0x003E;
							case PS_STEP_IMPEDANCE	:			return 	0x003D;
							case PS_STEP_EXT_CAN	:			return 	0x0011;		//ljb 2011322 이재복 //////////
							default					:			return	0x003C;
					}
				}
			case PS_STATE_SELF_TEST:		return 	0x0039;
			case PS_STATE_FAIL		:		return 	0x0039;
			case PS_STATE_CHECK	:			return 	0x003C;
			case PS_STATE_STOP		:		return 	0x0037;
			case PS_STATE_END		:		return 	0x0041;
			case PS_STATE_FAULT	:			return 	0x0038;
			case PS_STATE_NONCELL	:		return 	0x0039;
			case PS_STATE_READY	:			return 	0x0043;
			default:						return	0x0033;
		}

	}
	

	return 0;

}

SFT_STEP_SIMUL_CONDITION CCTSMonProDoc::GetNetworkFormatSimulStep(CStep *pStep)
{
	SFT_STEP_SIMUL_CONDITION	netStep;
	ZeroMemory(&netStep, sizeof(SFT_STEP_SIMUL_CONDITION));
	
	if(pStep != NULL)
	{
		netStep.stepHeader.nStepTypeID	= pStep->m_type;					//Step Type
		netStep.stepHeader.stepNo		= (BYTE)(pStep->m_StepIndex+1);		//StepNo
		netStep.stepHeader.mode			= pStep->m_mode;
		netStep.stepHeader.nSubStep		= 1;								//1개의 Sub Step만 이용한다.2005/08/08
		netStep.stepHeader.bTestEnd		= 0;
//		netStep.stepHeader.bUseSocFlag = pStep->m_bUseActualCapa;

		netStep.stepReferance[0].lVref_Charge	= FLOAT2LONG(pStep->m_fVref_Charge);
		netStep.stepReferance[0].lVref_DisCharge	= FLOAT2LONG(pStep->m_fVref_DisCharge);
		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
		netStep.stepReferance[0].lRref	= FLOAT2LONG(pStep->m_fRref);

		//ljb 20150820 챔버연동 기능 (1: 챔버연동 대기, 0: 챔버연동 대기 없이 진행)
		if (pStep->m_fTref == 0)
			netStep.stepReferance[0].bChamberlinkOption	= 0;
		else
			netStep.stepReferance[0].bChamberlinkOption	= 1;
		
		//Power 제어값이 들어 있다. (Power 단위는 mW이므로 )
		//CR 제어값이 들어 있다. (CR 모드에서 R은 mOhm)
// 		if( pStep->m_mode == PS_MODE_CP)
// 		{
//     		netStep.stepReferance[0].lPref	= LONG(pStep->m_fPref);
// 		}
// 		//전류 단위는 uA단위
// 		else	
// 		{
//      		netStep.stepReferance[0].lIref	= FLOAT2LONG(pStep->m_fIref);
// 		}
		netStep.stepReferance[0].byRange	= pStep->m_lRange;	
		netStep.stepReferance[0].ulEndTime	= MDTIME(pStep->m_fEndTime);
    	netStep.stepReferance[0].lEndV_H	= FLOAT2LONG(pStep->m_fEndV_H);		//ljb v1009
    	netStep.stepReferance[0].lEndI		= FLOAT2LONG(pStep->m_fEndI);
#ifdef _EDLC_TEST_SYSTEM
    	netStep.stepReferance[0].lEndC		= long(pStep->m_fEndC);
#else
    	netStep.stepReferance[0].lEndC		= FLOAT2LONG(pStep->m_fEndC);
#endif

		if (pStep->m_type == PS_STEP_LOOP || pStep->m_type == PS_STEP_ADV_CYCLE )
		{
			if (pStep->m_type == PS_STEP_LOOP)			//ljb v1009  LOOP 일 경우
			{
				netStep.stepReferance[0].lGoto		= pStep->m_nLoopInfoGotoStep;	
				netStep.stepReferance[0].lCycleCount= pStep->m_nLoopInfoCycle;	
				netStep.stepReferance[0].lAccCycleCount = pStep->m_nAccLoopInfoCycle;	//20080903 kjh
				netStep.stepReferance[0].lAccCycleGroupID	= pStep->m_nAccLoopGroupID;
				
				netStep.stepReferance[0].lEndV_L	= pStep->m_nMultiLoopInfoCycle;
				netStep.stepReferance[0].bEndC_Goto	= pStep->m_nMultiLoopGroupID;
				netStep.stepReferance[0].bEndWattHour_Goto	= pStep->m_nMultiLoopInfoGotoStep;
				netStep.stepReferance[0].bAccCycleCount_Goto	= pStep->m_nAccLoopInfoGotoStep;
			}
			else		
			{												//ljb v1009  CYCLE 일 경우
				netStep.stepReferance[0].bEndC_Goto	= pStep->m_nMultiLoopGroupID;
			}
		}
		else
		{
			//ljb v1009  Step..Cycle 일 경우
			netStep.stepReferance[0].lGoto		= pStep->m_nGotoStepEndV_H;	
			netStep.stepReferance[0].lCycleCount= pStep->m_nGotoStepEndV_L;	
			netStep.stepReferance[0].lAccCycleCount = pStep->m_nGotoStepEndTime;
			netStep.stepReferance[0].lAccCycleGroupID	= pStep->m_nGotoStepEndCVTime;
			netStep.stepReferance[0].lEndV_L	= FLOAT2LONG(pStep->m_fEndV_L);
			netStep.stepReferance[0].bEndC_Goto	= pStep->m_nGotoStepEndC;
			netStep.stepReferance[0].bEndWattHour_Goto	= pStep->m_nGotoStepEndWh;
			netStep.stepReferance[0].bValueRate_Goto	= pStep->m_nGotoStepEndValue;
		}
		netStep.stepReferance[0].bValueItem = pStep->m_bValueItem;		//ljb v1009 Valu Item
		netStep.stepReferance[0].bValueStepNo = pStep->m_bValueStepNo;
		netStep.stepReferance[0].wValueRate	= pStep->m_fValueRate *100;
		netStep.stepReferance[0].bValueRate_Goto = pStep->m_nGotoStepEndValue;
		
		netStep.stepReferance[0].lDeltaV	= FLOAT2LONG(pStep->m_fEndDV);
		netStep.stepReferance[0].lEndWatt   = LONG(pStep->m_fEndW);
		TRACE("lEndWatt: %ld\r\n", netStep.stepReferance[0].lEndWatt);
		netStep.stepReferance[0].lEndWattHour = LONG(pStep->m_fEndWh);
		TRACE("lEndWattHour: %ld\r\n", netStep.stepReferance[0].lEndWattHour);
		netStep.stepReferance[0].lReserved[0] = FLOAT2LONG(pStep->m_fEndDI);

		netStep.lHighV	= FLOAT2LONG(pStep->m_fHighLimitV);
		netStep.lLowV	= FLOAT2LONG(pStep->m_fLowLimitV);
		netStep.lHighI	= FLOAT2LONG(pStep->m_fHighLimitI);
		netStep.lLowI	= FLOAT2LONG(pStep->m_fLowLimitI);
#ifdef _EDLC_TEST_SYSTEM
		netStep.lHighC	= long(pStep->m_fHighLimitC);		//mF단위
		netStep.lLowC	= long(pStep->m_fLowLimitC);		//mF단위
#else
		netStep.lHighC	= FLOAT2LONG(pStep->m_fHighLimitC);		//uAh단위
		netStep.lLowC	= FLOAT2LONG(pStep->m_fLowLimitC);		//uAh단위 
#endif
		netStep.lHighZ	= FLOAT2LONG(pStep->m_fHighLimitImp);
		netStep.lLowZ	= FLOAT2LONG(pStep->m_fLowLimitImp);
		netStep.lHighTemp = FLOAT2LONG(pStep->m_fHighLimitTemp);
		netStep.lLowTemp = FLOAT2LONG(pStep->m_fLowLimitTemp);

//		netStep.lCellDeltaVStep = FLOAT2LONG(pStep->m_fCellDeltaVStep);//yulee 20190531_3

		//ljb 20100819 add Aux,BMS for v100A
		for (int j=0; j < _SFT_MAX_CAN_AUX_COMPARE_STEP ; j++)
		{
			netStep.stepReferance[0].ican_function_division[j] = pStep->m_ican_function_division[j];
			netStep.stepReferance[0].ican_compare_type[j] = pStep->m_ican_compare_type[j];
			netStep.stepReferance[0].cCan_data_type[j] = pStep->m_ican_data_type[j];
			netStep.stepReferance[0].cCan_branch[j] = pStep->m_ican_branch[j];
			netStep.stepReferance[0].fcan_Value[j] =  pStep->m_fcan_Value[j];
			netStep.stepReferance[0].iaux_function_division[j] =  pStep->m_iaux_function_division[j];
			netStep.stepReferance[0].iaux_compare_type[j] =  pStep->m_iaux_compare_type[j];
			netStep.stepReferance[0].cAux_data_type[j] =  pStep->m_iaux_data_type[j];
			netStep.stepReferance[0].cAux_branch[j] =  pStep->m_iaux_branch[j];
			netStep.stepReferance[0].faux_Value[j] =  pStep->m_faux_Value[j];
			netStep.stepReferance[0].iaux_conti_time[j] =  pStep->m_iaux_conti_time[j];		//ljb 20170824 add
		}


		netStep.conREC.lTime	= MDTIME(pStep->m_fReportTime);
		netStep.conREC.lDeltaV	= FLOAT2LONG(pStep->m_fReportV);
		netStep.conREC.lDeltaI	= FLOAT2LONG(pStep->m_fReportI);
		netStep.conREC.lDeltaT	= FLOAT2LONG(pStep->m_fReportTemp);
		netStep.conREC.lDeltaP	= 0;
		netStep.conREC.lReserved =0;
	}
	return netStep;
}

int CCTSMonProDoc::SetCANDataToModule(UINT nModuleID, SFT_RCV_CMD_CHANNEL_CAN_SET * pCanData, int nChIndex)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);

	//khs 20081118
	if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSION6)
	{
		SFT_RCV_CMD_CAN_SET1 *preVData = new SFT_RCV_CMD_CAN_SET1;
		memcpy(&preVData->canCommonData, &pCanData->canCommonData, sizeof(preVData->canCommonData));

		if((nRtn = pMD->SendCommand(SFT_CMD_CAN_SET_DATA , nChIndex, preVData, sizeof(SFT_RCV_CMD_CAN_SET1))) != SFT_ACK)
		{
			delete preVData;
			CString strMsg;
			//strMsg.Format("통신 Data 설정 명령 전달 실패! Return Value : %x", nRtn);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANDataToModule_msg1","CTSMonPro_DOC"), nRtn,_SFT_PROTOCOL_VERSION6);//&&
			AfxMessageBox(strMsg);
			return FALSE;
		}
		delete preVData;
	}
	else if(pMD->GetProtocolVer() == _SFT_PROTOCOL_VERSION10)
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CAN_SET_DATA , nChIndex, pCanData, sizeof(SFT_RCV_CMD_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			//strMsg.Format("통신 Data 설정 명령 전달 실패! Return Value : %x", nRtn);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANDataToModule_msg2","CTSMonPro_DOC"), nRtn, _SFT_PROTOCOL_VERSION10);//&&
			AfxMessageBox(strMsg);
			return FALSE;
		}
	}
	//else if(pMD->GetProtocolVer() == _SFT_PROTOCOL_VERSION)
	else if(pMD->GetProtocolVer() == _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : v1015 호환 추가
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CHANNEL_CAN_SET_DATA , nChIndex, pCanData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			//strMsg.Format("통신 Data 설정 명령 전달 실패(CH : %d)! Return Value : %x", nChIndex, nRtn);
			//strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANDataToModule_msg3","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION);//&&
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANDataToModule_msg3","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION_1015);//&&
			AfxMessageBox(strMsg);
			return FALSE;
		}

	}
	else if(pMD->GetProtocolVer() == _SFT_PROTOCOL_VERSION)
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CHANNEL_CAN_SET_DATA , nChIndex, pCanData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			//strMsg.Format("통신 Data 설정 명령 전달 실패(CH : %d)! Return Value : %x", nChIndex, nRtn);
			//strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANDataToModule_msg3","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION);//&&
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANDataToModule_msg3","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION);//&&
			AfxMessageBox(strMsg);
			return FALSE;
		}

	}
	else
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CHANNEL_CAN_SET_DATA , nChIndex, pCanData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			//strMsg.Format("통신 Data 설정 명령 전달 실패(CH : %d)! Return Value : %x", nChIndex, nRtn);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANDataToModule_msg3","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION_ELSE);//&&
			AfxMessageBox(strMsg);
			return FALSE;
		}

	}
	
	return nRtn;
}

//ljb 20100910 for v100A
int CCTSMonProDoc::SetCANChangeDataToModule(UINT nModuleID, SFT_SEND_CMD_CAN_CHANGE_SET * pCanChangeData, int nChIndex)
{
	CString strMsg;
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	
	if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSION10)
	{
		//strMsg.Format("프로그램 버전이 맞지 않습니다.(ver : %x)", pMD->GetProtocolVer());
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANChangeDataToModule_msg1","CTSMonPro_DOC"), pMD->GetProtocolVer());//&&
		AfxMessageBox(strMsg);
		return FALSE;
	}
	else
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_BMS_CHANGE , nChIndex, pCanChangeData, sizeof(SFT_SEND_CMD_CAN_CHANGE_SET))) != SFT_ACK)
		{
			CString strMsg;
			//strMsg.Format("통신 Data 설정 명령 전달 실패(CH : %d)! Return Value : %x", nChIndex, nRtn);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetCANChangeDataToModule_msg2","CTSMonPro_DOC"), nChIndex, nRtn);//&&
			AfxMessageBox(strMsg);
			return FALSE;
		}
		
	}	
	return nRtn;
}

//하위 버전 호환용
int CCTSMonProDoc::SendAuxDatatoModuleA(UINT nModuleID, STF_MD_AUX_SET_DATA_A *auxData)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	if((nRtn = pMD->SendCommand(SFT_CMD_AUX_SET_DATA , auxData, sizeof(STF_MD_AUX_SET_DATA_A)*128)) != SFT_ACK)
	{
		//AfxMessageBox("Aux Data 설정 명령 전달 실패!");
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendAuxDatatoModuleA_msg1","CTSMonPro_DOC"));//&&
		return FALSE;
	}
	
	return nRtn;
}

int CCTSMonProDoc::ScheduleValidityCheck(CScheduleData *pSchedule, CDWordArray *pdwTargetCh)
{
	CCyclerModule * pMD;
	CCyclerChannel * pChInfo;
	SFT_MD_PARALLEL_DATA * pParaData;

	int nPreModuleID = -1;
	int nModuleID=0, nChIndex = 0;
	for(int ch = 0; ch<pdwTargetCh->GetSize(); ch++)
	{
		DWORD dwData = pdwTargetCh->GetAt(ch);
		nModuleID = HIWORD(dwData);
		nChIndex = LOWORD(dwData);

		if (nPreModuleID != nModuleID)
		{
			pMD = GetModuleInfo(nModuleID);
			if(pMD == NULL)
			{
				//AfxMessageBox("Module 정보를 알수 없습니다.");
				AfxMessageBox(Fun_FindMsg("CTSMonProDoc_ScheduleValidityCheck_msg1","CTSMonPro_DOC"));//&&
				return -1;
			}
			pParaData = pMD->GetParallelData();
			nPreModuleID = nModuleID;
		}

		pChInfo = pMD->GetChannelInfo(nChIndex);
		if(pChInfo == NULL) continue;
// 		if(pChInfo == NULL)
// 		{
// 			AfxMessageBox("채널 정보를 알수 없습니다.");
// 			return -1;
// 		}

		long lMaxVoltage = pMD->GetVoltageSpec();
		long lMaxCurrent;
		long lMaxCurrent2;
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			lMaxCurrent = 0;
			lMaxCurrent2 = 0;
			lMaxCurrent = (pMD->GetCurrentSpec())*2; //20180315 yulee 500A에서 1000A로 변경한 소스 Ch1, 2 -> Ch 1, Ch3, 4 -> 2 따라서 *2 
			lMaxCurrent2 = (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC,"Max Charge Current", 0))*2; //20180315 yulee 500A에서 1000A로 변경한 소스 Ch1, 2 -> Ch 1, Ch3, 4 -> 2 따라서 *2
		}
//#else
		else
		{
			lMaxCurrent = 0;
			lMaxCurrent2 = 0;
			lMaxCurrent = pMD->GetCurrentSpec();
			lMaxCurrent2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC,"Max Charge Current", 0);
		}
//#endif
		if(lMaxCurrent2 == 0) lMaxCurrent2 = lMaxCurrent;

		if(pChInfo->IsParallel())	//병렬모드
		{
			for(int j = 0; j < _SFT_MAX_PARALLEL_COUNT; j++)
			{

				if(nChIndex+1 == pParaData[j].chNoMaster && pParaData[j].bParallel == TRUE)
				{
					int nParCount = 1;
					if(pParaData[j].chNoSlave[0] != 0)
						nParCount++;
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark 
					//if(g_AppInfo.iPType==0)
					if(g_AppInfo.iPType==0) //lyj 20200519 병렬 시 조건검사 버그 수정
					{
						if(pParaData[j].chNoSlave[1] != 0)
							nParCount++;
						if(pParaData[j].chNoSlave[2] != 0) //yulee 20180803 4Ch 대응 
							nParCount++;
					}
//#endif			
					
					
					for(int nStep = 0; nStep < pSchedule->GetStepSize(); nStep++)
					{
						int nMaxV;
						int nMaxC;
						CStep * pStep = pSchedule->GetStepData(nStep);
						if(pStep->m_type == PS_STEP_CHARGE)
						{
							if(lMaxCurrent2 != 0)
								nMaxC = lMaxCurrent2 * nParCount;
							else
								nMaxC = lMaxCurrent2;							
						}
						else
						{
							if(lMaxCurrent2 != 0)
								nMaxC = lMaxCurrent2 * nParCount;
							else
								nMaxC = lMaxCurrent2;   //ljb 20190219 
						}

						if(lMaxVoltage != 0 && pStep->m_mode != PS_MODE_CP)
							nMaxV = lMaxVoltage * nParCount;
						else
							nMaxV = lMaxVoltage;
						
						int nRtn;
						TRACE("Input Ch: %d Step : %d => MaxV : %d, MaxC : %d\r\n", nChIndex+1, nStep, nMaxV, nMaxC);

						if((nRtn = StepValidityCheck(pStep, nMaxV, nMaxC)) < 0)
						{
							return nRtn;
						}
					}					
				}				
			}
		}
		else						//단일모드
		{
			for(int nStep = 0; nStep < pSchedule->GetStepSize(); nStep++)
			{
				int nRtn;
				CStep * pStep = pSchedule->GetStepData(nStep);

				if(pStep->m_type == PS_STEP_CHARGE)
				{
					if((nRtn = StepValidityCheck(pStep, lMaxVoltage, lMaxCurrent2)) < 0)
						return nRtn;
				}
				else
				{
					if((nRtn = StepValidityCheck(pStep, lMaxVoltage, lMaxCurrent2)) < 0) //ljb 20190219  lMaxCurrent -> lMaxCurrent2
						return nRtn;
				}
				
			}
		}	
		pChInfo->m_strCellDeltaVolt.Format("%.0f", pSchedule->GetCellDeltaVolt());	//ljb 20150806 add 셀전압편차값 입력
	}
	return 0;
}

int CCTSMonProDoc::StepValidityCheck(CStep *pStep, long lMaxVoltage, long lMaxCurrent)
{
	ASSERT(pStep);
	CString strMsg;
	
	int fMinVoltage;
	if(m_bOverChargerSet) //yulee 20191017 OverChargeDischarger Mark
	{		
		float OverChargerMinVolt = 0;
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);
		fMinVoltage = -OverChargerMinVolt;//yulee 20180810
	}
	else
		fMinVoltage = 0.0f;

	

//////////////////////////////////////////////////////////////////////////

	//DC Impedance 측정일 경우 설정값을 입력 하여야 한다.
	if(pStep->m_type == PS_STEP_IMPEDANCE && pStep->m_mode == PS_MODE_DCIMP)
	{
		if(pStep->m_fIref > lMaxCurrent)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg1","CTSMonPro_DOC"), pStep->m_StepIndex+1);	//m_strLastErrorString.Format("Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다.", pStep->m_StepIndex);								
			return -1;
		}

	} //Impedance 
	//2014.09.11 PS_STEP_USER_MAP 추가.
	if((pStep->m_type == PS_STEP_PATTERN || pStep->m_type == PS_STEP_USER_MAP)  && pStep->m_mode != PS_MODE_CP)
	{
		//+2015.9.21 USY Mod for PatternCV(김재호K)
		if(pStep->m_type != PS_STEP_PATTERN)
		{
			if(pStep->m_lValueLimitHigh > lMaxCurrent)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg2","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxCurrent);	//m_strLastErrorString.Format("Step %d의 전류값이 설정되지 않았거나 범위를 벗어났습니다.(0~%ldmA)", pStep->m_StepIndex, lMaxCurrent);
				return -1;
			}
		}
// 		if(pStep->m_lValueLimitHigh > lMaxCurrent)
// 		{
// 			m_strLastErrorString.Format("Step %d의 전류값이 설정되지 않았거나 범위를 벗어났습니다.(0~%ldmA)", pStep->m_StepIndex, lMaxCurrent);								
// 			return -1;
// 		}
		//-

	}
	//2014.09.11 PS_STEP_USER_MAP 추가.
	if((pStep->m_type == PS_STEP_PATTERN || pStep->m_type == PS_STEP_USER_MAP) && pStep->m_mode == PS_MODE_CP)
	{
		if(pStep->m_lValueLimitHigh > (float)lMaxCurrent/ 1000.0f * lMaxVoltage || pStep->m_lValueLimitLow < 0.0f)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg3","CTSMonPro_DOC"), pStep->m_StepIndex+1, //m_strLastErrorString.Format("Step %d의 CP 전력값 입력 범위가 벗어났습니다.(0 ~ %ldW)", pStep->m_StepIndex, 
				lMaxCurrent/1000 * lMaxVoltage/1000);			
			return -1;
		}

	}

	if(pStep->m_type == PS_STEP_CHARGE || pStep->m_type == PS_STEP_DISCHARGE)
	{

		//전류 설정값은 반드시 입력해야 한다.
		if(pStep->m_mode != PS_MODE_CP && pStep->m_mode != PS_MODE_CR)
		{
			if(pStep->m_fIref < 0 || pStep->m_fIref >lMaxCurrent)
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg4","CTSMonPro_DOC"), pStep->m_StepIndex+1, 0, lMaxCurrent);			//m_strLastErrorString.Format("Step %d의 전류값이 설정 되지 않았거나 범위를 벗어났습니다.(%d~%dmA)", pStep->m_StepIndex, 0, lMaxCurrent);			
				return -1;
			}
			
		}
		else	//fPRef에 CP, CR제어값이 들어 있다.
		{
			if(PS_MODE_CP == pStep->m_mode && (pStep->m_fPref > (float)lMaxCurrent/ 1000.0f * lMaxVoltage *2  || pStep->m_fPref <= 0.0f))
			{
				m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg5","CTSMonPro_DOC"), pStep->m_StepIndex+1,  (float)lMaxCurrent/1000.0f * lMaxVoltage *2);			//m_strLastErrorString.Format("Step %d의 CP 전력값 입력 범위가 벗어났습니다.(0 ~ %.1fW)", pStep->m_StepIndex,  (float)lMaxCurrent/1000.0f * lMaxVoltage *2);			
				return -1;
			}
		}
	}

	
	if(pStep->m_type == PS_STEP_CHARGE)
	{
		//End 전압을 정격전압보다 최소 10mV이하 낮게 설정하여야 한다.
		//End 전압은 CC모드 입력에서만 가능한데 CC동작을 위해서는 CV 전압이 더 높게 설정되어야 하므로 
		if(pStep->m_fEndV_H >= lMaxVoltage-10)
		{	//ljb v1009
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg6","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);			//m_strLastErrorString.Format("Step %d의 종료 전압(High)이 정격 전압(%dmV)에 비해 높게 설정 되었습니다. 정격전압보다 최소 10mV이하 범위값으로 설정하십시오.", pStep->m_StepIndex, lMaxVoltage);			
			return -1;				
		}

		//End 전압은 CC모드 입력에서만 가능한데 CC동작을 위해서는 CV 전압이 더 높게 설정되어야 하므로 
		if(pStep->m_fEndV_L >= lMaxVoltage-10)
		{	//ljb v1009
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg7","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);			//m_strLastErrorString.Format("Step %d의 종료 전압(Low)이 정격 전압(%dmV)에 비해 높게 설정 되었습니다. 정격전압보다 최소 10mV이하 범위값으로 설정하십시오.", pStep->m_StepIndex, lMaxVoltage);			
			return -1;				
		}

		if(pStep->m_fVref_Charge <= fMinVoltage|| pStep->m_fVref_Charge > lMaxVoltage)
//			|| pStep->m_fVref_DisCharge <= 0.0f || pStep->m_fVref_DisCharge > lMaxVoltage)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg8","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);			//m_strLastErrorString.Format("Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);			
			return -1;
		}
	} //charge
	

	//+2015.9.21 USY Mod For PatternCV(김재호K)
	if(pStep->m_type != PS_STEP_PATTERN)
	{
		//Charge 전압 설정값 입력 범위 검사
		if( pStep->m_fVref_Charge > lMaxVoltage || pStep->m_fVref_Charge < fMinVoltage)
		{
			strMsg.Format("1: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fVref_Charge, lMaxVoltage);
			TRACE(strMsg);
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg9","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);//m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
			return -1;
		}
		//DisCharge 전압 설정값 입력 범위 검사
		if( pStep->m_fVref_DisCharge > lMaxVoltage || pStep->m_fVref_DisCharge < fMinVoltage)
		{
			strMsg.Format("1: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fVref_DisCharge, lMaxVoltage);
			TRACE(strMsg);
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg10","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);//m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
			return -1;
		}
	}
// 	//Charge 전압 설정값 입력 범위 검사
//  	if( pStep->m_fVref_Charge > lMaxVoltage || pStep->m_fVref_Charge < 0.0f)
// 	{
// 		strMsg.Format("1: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fVref_Charge, lMaxVoltage);
// 		TRACE(strMsg);
// 		m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
// 		return -1;
// 	}
// 	//DisCharge 전압 설정값 입력 범위 검사
// 	if( pStep->m_fVref_DisCharge > lMaxVoltage || pStep->m_fVref_DisCharge < 0.0f)
// 	{
// 		strMsg.Format("1: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fVref_DisCharge, lMaxVoltage);
// 		TRACE(strMsg);
// 		m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
// 		return -1;
// 	}
	//-
/*	//전압 종료값 입력 범위 검사
	if(pStep->m_type != PS_STEP_PATTERN && pStep->m_fEndV > lMaxVoltage || pStep->m_fEndV < 0.0f)
	{
		strMsg.Format("2: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fVref, lMaxVoltage);
		TRACE(strMsg);
		m_strLastErrorString.Format("Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0~%dmV)", i+1, lMaxVoltage);
		return -7;
	}
	//전류 종료값 입력 범위 검사
	if(pStep->m_type != PS_STEP_PATTERN && pStep->m_fEndI > lMaxCurrent || pStep->m_fEndI < 0.0f)
	{
		strMsg.Format("3: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fVref, lMaxVoltage);
		TRACE(strMsg);
		m_strLastErrorString.Format("Step %d의 종료 전류값이 입력 범위를 벗어났습니다.(범위:0~%dmA)", i+1, lMaxCurrent);
		return -8;
	}*/
	//2014.09.11 PS_STEP_USER_MAP 추가.
	if((pStep->m_type == PS_STEP_PATTERN || pStep->m_type == PS_STEP_USER_MAP) && pStep->m_fEndV_H > lMaxVoltage || pStep->m_fEndV_H < fMinVoltage)		//ljb v1009
	{
// 		strMsg.Format("2: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fEndV_H, lMaxVoltage);
// 		TRACE(strMsg);
		//+2015.9.21 USY Mod For PatternCV(김재호K)
		if(pStep->m_type != PS_STEP_PATTERN)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg11","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);//m_strLastErrorString.Format("Step %d의 종료 전압값H가 입력 범위를 벗어났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
			return -7;
		}
		
// 		m_strLastErrorString.Format("Step %d의 종료 전압값H가 입력 범위를 벗어났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
// 		return -7;
		//-
	}
	//2014.09.11 PS_STEP_USER_MAP 추가.
	if((pStep->m_type == PS_STEP_PATTERN || pStep->m_type == PS_STEP_USER_MAP) && pStep->m_fEndV_L > lMaxVoltage || pStep->m_fEndV_L < fMinVoltage)
	{
// 		strMsg.Format("2: Step HighLimitV : %f, MaxVoltage : %ld\r\n", pStep->m_fEndV_L, lMaxVoltage);
// 		TRACE(strMsg);
		//+2015.9.21 USY Mod For PatternCV(김재호K)
		if(pStep->m_type != PS_STEP_PATTERN)
		{
			m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg12","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);//m_strLastErrorString.Format("Step %d의 종료 전압값L이 입력 범위를 벗어났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
			return -7;
		}
// 		m_strLastErrorString.Format("Step %d의 종료 전압값L이 입력 범위를 벗어났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
// 		return -7;
		//-
	}

	//전압 제한값 입력 범위 검사
// 	if(pStep->m_fHighLimitV > lMaxVoltage || pStep->m_fLowLimitV < 0)
// 	{
// 		m_strLastErrorString.Format("Step %d의 안전 전압 상하한값이 입력 범위를 벗어났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
// 		return -1;
// 	}
	//ljb 2011210 이재복 //////////
	if(pStep->m_fLowLimitV > lMaxVoltage || pStep->m_fLowLimitV < 0)
	{
		m_strLastErrorString.Format(Fun_FindMsg("StepValidityCheck_msg13","CTSMonPro_DOC"), pStep->m_StepIndex+1, lMaxVoltage);//m_strLastErrorString.Format("Step %d의 안전 전압 하한값이 입력 범위를 벗어났습니다.(범위:0~%dmV)", pStep->m_StepIndex, lMaxVoltage);
		return -1;
	}
	
	return 0;
}

CString CCTSMonProDoc::GetLastErrorString()
{
	return this->m_strLastErrorString;
}

void CCTSMonProDoc::LoadOvenData(int nItem)
{
	//////////////////////////////////////////////////////////////////////////
	//★★ ljb 2010325  ★★★★★★★★
	DWORD dwItem[MAX_OVEN_COUNT][64];
	
	LPVOID pData_1 = NULL;
	LPVOID pData_2 = NULL;
	UINT nSize = 0;
	
	//CString strUseOven;
	BOOL bUseOven;	
	CDWordArray adwCh; 

	int nRtn;
	int iCnt,nSelModel, nMaxOven;
	float fFactorValue = (0.0f); //20170525 YULEE 추가

	//-------------------------------------------------------------------------
    if(nItem==1)
	{
		nSelModel = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "ChamberModel",3);
		nMaxOven = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "ChamberMaxCount",1);
		bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	float fFactorValue = (0.0f); //20170525 YULEE 추가
		fFactorValue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoFactor1", FALSE); //20170525 YULEE OvenOndoFactor1 가져오기 
	float fFactorSPValue = (0.0f); //20170907 YULEE SPFactor의 옵션처리 차 추가 //레지스트리 값 읽어오기
	fFactorSPValue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoSPFactor1", 10);

		if (bUseOven)
		{
			memset(dwItem, 0, sizeof(dwItem));
			nRtn = AfxGetApp()->GetProfileBinary(REG_SERIAL_CONFIG3, "OvenChMap", (LPBYTE *)&pData_1, &nSize);
			if(nSize > 0 && nRtn == TRUE)	
			{
				memcpy(dwItem, pData_1, nSize);
			}

			for (iCnt = 0; iCnt < nMaxOven; iCnt++)
			{
				m_ctrlOven1.SetUseOven(iCnt, bUseOven);
				
				adwCh.RemoveAll();
				for(int j = 0 ; j < 64; j++)
				{
					if(dwItem[iCnt][j] != 0)
						adwCh.Add(dwItem[iCnt][j]);
				}
				if(adwCh.GetSize() > 0)
				{
					m_ctrlOven1.SetChannelMapArray(iCnt, adwCh);
				}
			}
			m_ctrlOven1.m_bUseOvenCtrl = TRUE;	//설정된 Oven중 한개라도 사용하면 OvenCtrl 사용
			m_ctrlOven1.m_bPortOpen = FALSE;	//ljb 20180219 add RS232 Port Open 상태
			m_ctrlOven1.SetOvenCount(nMaxOven);
			m_ctrlOven1.SetOvenModelType(nSelModel);
			m_ctrlOven1.SetOvenOndoPSFactor(fFactorValue);
			m_ctrlOven1.SetOvenOndoSPFactor(fFactorSPValue); //20170907 YULEE SPFactor의 옵션처리 차 추가
			delete [] pData_1;
		}
		//if (nOvenCount > 0)
		//{	
		//	m_ctrlOven1.LoadSerilaConfig();
 		//}
	}
	//-------------------------------------------------------------------------
	else if(nItem==2)
	{
		nSelModel = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "ChamberModel",3);
		nMaxOven = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "ChamberMaxCount",1);
		bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", FALSE);
		fFactorValue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoFactor2", FALSE); //20170525 YULEE OvenOndoFactor2 가져오기 
	//20170907 YULEE SPFactor의 옵션처리 차 추가 //레지스트리 값 읽어오기
	float fFactorSPValue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoSPFactor2", 10);

		if (bUseOven)
		{
			memset(dwItem, 0, sizeof(dwItem));
			nRtn = AfxGetApp()->GetProfileBinary(REG_SERIAL_CONFIG4, "OvenChMap", (LPBYTE *)&pData_2, &nSize);
			if(nSize > 0 && nRtn == TRUE)	
			{
				memcpy(dwItem, pData_2, nSize);
			}
			
			for (iCnt = 0; iCnt < nMaxOven; iCnt++)
			{
				m_ctrlOven2.SetUseOven(iCnt, bUseOven);
				
				adwCh.RemoveAll();
				for(int j = 0 ; j < 64; j++)
				{
					if(dwItem[iCnt][j] != 0)
						adwCh.Add(dwItem[iCnt][j]);
				}
				if(adwCh.GetSize() > 0)
				{
					m_ctrlOven2.SetChannelMapArray(iCnt, adwCh);			
				}
			}
			m_ctrlOven2.m_bUseOvenCtrl = TRUE;	//설정된 Oven중 한개라도 사용하면 OvenCtrl 사용
		m_ctrlOven2.m_bPortOpen	= FALSE;	//ljb 20180219 add RS232 Port Open 상태
			m_ctrlOven2.SetOvenCount(nMaxOven);
			m_ctrlOven2.SetOvenModelType(nSelModel);
			m_ctrlOven2.SetOvenOndoPSFactor(fFactorValue);
			m_ctrlOven2.SetOvenOndoSPFactor(fFactorSPValue); //20170907 YULEE SPFactor의 옵션처리 차 추가
			delete [] pData_2;
		}	
	} 	
}

void CCTSMonProDoc::CheckUsePort()
{
	((CMainFrame *)AfxGetMainWnd())->CheckUsePort();
}	


int CCTSMonProDoc::SetMuxCommandDataToModule(UINT nModuleID, CWordArray *pSelCh, SFT_MUX_SELECT * MuxCommandData)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	CString strMsg;
	
	if (MuxCommandData->command == 1)
	{
		MuxCommandData->command = 0;
		nRtn = pMD->SendCommand(SFT_CMD_MUX_SELECT_COMM_REQUEST, pSelCh, MuxCommandData, sizeof(SFT_MUX_SELECT));
		//strMsg.Format("%s 에 Mux 명령 전송", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetMuxCommandDataToModule_msg1","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
	}
	else if (MuxCommandData->command == 2)
	{
		MuxCommandData->command = 0;
		if((nRtn = pMD->SendCommand(SFT_CMD_LOAD_TYPE_SET_COMM_REQUEST, pSelCh, MuxCommandData, sizeof(SFT_MUX_SELECT))) != SFT_ACK)	
		{
			//strMsg.Format("%s 에 LOAD Set 명령 전송 실패, 0:EDLC, 1:CELL (%d)", GetModuleName(nModuleID),MuxCommandData->out_mux);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetMuxCommandDataToModule_msg2","CTSMonPro_DOC"), GetModuleName(nModuleID),MuxCommandData->out_mux);//&&
			//AfxMessageBox("LOAD Type Set 명령 전송 실패.");
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SetMuxCommandDataToModule_msg3","CTSMonPro_DOC"));//&&
		}
		else
		{
			//strMsg.Format("%s 에 LOAD Set 명령 전송 성공, 0:EDLC, 1:CELL (%d)", GetModuleName(nModuleID),MuxCommandData->out_mux);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetMuxCommandDataToModule_msg4","CTSMonPro_DOC"), GetModuleName(nModuleID),MuxCommandData->out_mux);//&&
			//AfxMessageBox("LOAD Type Set 명령 전송 성공.");
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SetMuxCommandDataToModule_msg5","CTSMonPro_DOC"));//&&
		}
	}
	else
	{
		nRtn = pMD->SendCommand(SFT_CMD_MUX_SELECT_COMM_REQUEST, pSelCh, MuxCommandData, sizeof(SFT_MUX_SELECT));
		//strMsg.Format("%s 에 Mux 명령 전송", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SetMuxCommandDataToModule_msg6","CTSMonPro_DOC"), GetModuleName(nModuleID));//&&
	}
	
	WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
	return nRtn;
}

// int CCTSMonProDoc::SetLINCANSelectCommandDataToModule(UINT nModuleID, int nSelCh, SFT_LINCAN_SELECT * LINCANSelect)
// {
// 	int nRtn = 0;
// 	CCyclerModule *pMD = GetModuleInfo(nModuleID);
// 	CString strMsg;
// 	
// 	if (LINCANSelect->SelectMode == 0)
// 	{
// 		nRtn = pMD->SendCommand(SFT_CMD_LIN_CAN_SELECT_REQUEST, nSelCh, LINCANSelect, sizeof(SFT_LINCAN_SELECT));
// 		strMsg.Format("%s 에 Mux 명령 전송", GetModuleName(nModuleID));
// 	}
// 	else if (LINCANSelect->SelectMode == 1)
// 	{
// 		if((nRtn = pMD->SendCommand(SFT_CMD_LIN_CAN_SELECT_REQUEST, nSelCh, LINCANSelect, sizeof(SFT_LINCAN_SELECT))) != SFT_ACK)	
// 		{
// 			strMsg.Format("%s 에 LIN/CAN Select 명령 전송 실패, 0:CAN, 1:LIN (%d)", GetModuleName(nModuleID),LINCANSelect->SelectMode);
// 			AfxMessageBox("LOAD Type Set 명령 전송 실패.");
// 		}
// 		else
// 		{
// 			strMsg.Format("%s 에 LIN/CAN Select 명령 전송 성공, 0:CAN, 1:LIN (%d)", GetModuleName(nModuleID),LINCANSelect->SelectMode);
// 			AfxMessageBox("LOAD Type Set 명령 전송 성공.");
// 		}
// 	}
// 	else
// 	{
// 		nRtn = pMD->SendCommand(SFT_CMD_LIN_CAN_SELECT_REQUEST, nSelCh, LINCANSelect, sizeof(SFT_LINCAN_SELECT));
// 		strMsg.Format("%s 에 LIN/CAN Select 명령 전송 성공", GetModuleName(nModuleID));
// 	}
// 	
// 	WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 	return nRtn;
// }
	
int CCTSMonProDoc::SetDAQIsolationDataToModule(UINT nModuleID, SFT_DAQ_ISOLATION * DAQCommandData)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	//nRtn = pMD->SendCommand(SFT_CMD_DAQ_ISOLATION_COMM_REQUEST , DAQCommandData, sizeof(SFT_DAQ_ISOLATION));
	nRtn = pMD->SendCommand(SFT_CMD_OUTPUT_CH_CHANGE_REQUEST , DAQCommandData, sizeof(SFT_DAQ_ISOLATION)); //lyj 20201203
	// 	{
	// 		AfxMessageBox("CAN Data 설정 명령 전달 실패!");
	// 		return FALSE;
	// 	}
	CString strMsg;
	strMsg.Format("SetDAQIsolationDataToModule,ISO:%d CH:%d,Rtn: %d",DAQCommandData->bISO,DAQCommandData->div_ch,nRtn); //lyj 20201203
	WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
	return nRtn;
	
// 	int nRtn = 0;
// 	CCyclerModule *pMD = GetModuleInfo(nModuleID);
// 	if(pMD->GetProtocolVer() < _SFT_PROTOCOL_VERSION)
// 	{
// 		return -1;
// 	}
// 	else
// 	{
// 		if((nRtn = pMD->SendCommand(SFT_CMD_DAQ_ISOLATION_COMM_REQUEST, nChIndex, pDAQData, sizeof(SFT_DAQ_ISOLATION))) != SFT_ACK)
// 		{
// 			CString strMsg;
// 			if (pDAQData->bISO == 0)
// 				strMsg.Format("비절연 설정 명령 전달 실패! Return Value : %x", nRtn);
// 			if (pDAQData->bISO == 1)
// 				strMsg.Format("절연 설정 명령 전달 실패! Return Value : %x", nRtn);
// 
// 			AfxMessageBox(strMsg);
// 			return FALSE;
// 		}
// 		/*
// 		if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_START, pSelChArray, &stepInfo, sizeof(SFT_STEP_START_INFO))) != SFT_ACK)
// 		{
// 		strMsg.Format("%s 에 스케쥴 전송 준비 실패!!!(Code %d)", GetModuleName(nModuleID), nRtn);
// 		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 		return FALSE;
// 		}
// 		else
// 		{
// 		strMsg.Format("%s 에 스케쥴 전송 준비 성공", GetModuleName(nModuleID));
// 		WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
// 		}
// 		*/
// 	}
// 	return nRtn;
}

//ksj 20210104: 옵션 추가. 0: 일반절연 / 1: 분기절연
//기존 SetDAQIsolationDataToModule는 SendCommand Header에 채널정보를 포함하지 않고, 모듈단위로 전송하도록 되어있으나,
//SBC가 내부에서 Header의 채널을 가지고 해당 채널 작업시작 가능 여부 판단을 하고 있음.
//만약 1번채널에서 작업중이면 SetDAQIsolationDataToModule은 다른채널에서 사용 불가.
//채널 독립적으로 동작하도록 SetDAQIsolationDataToChannel 함수 추가함.
int CCTSMonProDoc::SetDAQIsolationDataToChannel(UINT nModuleID, SFT_DAQ_ISOLATION * DAQCommandData, int nOption) 
{
	CString strMsg;
	int nRtn = 0;

	//모듈 Validation
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	if(pMD == NULL)
	{
		strMsg.Format("Send Isolation to Channel Failure. Module Error %d",nModuleID);		
		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		AfxMessageBox(strMsg);
		return 0;
	}

	
	int nChannelIndex = -1; 	
	if(nOption == 0) //일반 절연
	{
		//채널 Validation
		nChannelIndex = DAQCommandData->div_ch-1;  //분기 절연은 ch이 충방전기 채널 번호
		CCyclerChannel *pCH = pMD->GetChannelInfo(nChannelIndex);
		if(pCH == NULL)
		{
			strMsg.Format("Send Isolation to Channel Failure. Channel Error ISO:%d CH:%d,Rtn: %d , Option:%d ",DAQCommandData->bISO,DAQCommandData->div_ch,nRtn ,nOption); //lyj 20210913 Option 로그 추가 
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			AfxMessageBox(strMsg);
			return 0;	
		}
		//절연 명령 전송
		nRtn = pMD->SendCommand(SFT_CMD_DAQ_ISOLATION_COMM_REQUEST , nChannelIndex, DAQCommandData, sizeof(SFT_DAQ_ISOLATION));
	}
	else //분기 절연
	{
		//채널 Validation
		nChannelIndex = DAQCommandData->div_MainCh-1; //분기 절연은 MainCh이 충방전기 채널 번호
		CCyclerChannel *pCH = pMD->GetChannelInfo(nChannelIndex);
		if(pCH == NULL)
		{
			strMsg.Format("Send Isolation to Channel Failure. Channel Error ISO:%d CH:%d,Rtn: %d ,Option:%d",DAQCommandData->bISO,DAQCommandData->div_ch,nRtn,nOption); //lyj 20210913 Option 로그 추가 
			WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			AfxMessageBox(strMsg);
			return 0;	
		}
		//절연 명령 전송
		nRtn = pMD->SendCommand(SFT_CMD_OUTPUT_CH_CHANGE_REQUEST , nChannelIndex, DAQCommandData, sizeof(SFT_DAQ_ISOLATION));
	}

	if(nRtn != SFT_ACK)
	{
		strMsg.Format("Send Isolation to Channel Failure. (Opt:%d ISO:%d CH:%d Rtn:%d)",nOption,DAQCommandData->bISO,DAQCommandData->div_ch,nRtn);
		WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		AfxMessageBox(strMsg);
		return FALSE;
	}

	return nRtn;
}

//yulee 20181120
//int CCTSMonProDoc::SetRTTableSendEndSignalToModule(UINT nModuleID, SFT_RT_TABLE_SEND_END *RT_Table_End_Flag)
int CCTSMonProDoc::SetRTTableSendEndSignalToModule(UINT nModuleID, void *RT_Table_End_Flag, int nRtType) //ksj 20200207
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);

	//nRtn = pMD->SendCommand(SFT_CMD_RT_TABLE_UPLOAD_END_REQUEST , NULL, RT_Table_End_Flag, sizeof(SFT_RT_TABLE_SEND_END));

	if(nRtType == PS_AUX_TYPE_HUMIDITY) //ksj 20200207
	{
		nRtn = pMD->SendCommand(SFT_CMD_RT_HUMI_TABLE_UPLOAD_END_REQUEST , NULL, RT_Table_End_Flag, sizeof(SFT_RT_HUMI_TABLE_SEND_END));
	}
	else
	{
		nRtn = pMD->SendCommand(SFT_CMD_RT_TABLE_UPLOAD_END_REQUEST , NULL, RT_Table_End_Flag, sizeof(SFT_RT_TABLE_SEND_END));
	}	
	
	return nRtn;
}

int CCTSMonProDoc::SetCANTransDataToModule(UINT nModuleID, SFT_TRANS_CMD_CHANNEL_CAN_SET *pCanData, int nChIndex)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);

	//khs 20081118
	if(pMD->GetProtocolVer() <= _SFT_PROTOCOL_VERSION6)
	{
		SFT_TRANS_CMD_CAN_SET1 *preVData = new SFT_TRANS_CMD_CAN_SET1;
		memcpy(&preVData->canCommonData, &pCanData->canCommonData, sizeof(preVData->canCommonData));
// 		for(int i=0; i<_SFT_MAX_INSTALL_CH_COUNT;i++)
// 		{
// 			for(int j=0; j<_SFT_MAX_MAPPING_CAN;j++)
// 			{
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].bitCount = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].bitCount;
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].byte_order = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].byte_order;
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].canID = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].canID;
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].canType = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].canType;
// //				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].CapVtgFlag = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].CapVtgFlag;
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].data_type = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].data_type;
// //				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].default_value = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].default_value;
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].factor_multiply = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].factor_multiply;
// 				strcpy(preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].name, pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].name);
// //				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].reserved2 = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].reserved2;
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].send_time = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].send_time;
// 				preVData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].startBit = pCanData->canSetData[_SFT_MAX_INSTALL_CH_COUNT][_SFT_MAX_MAPPING_CAN].startBit;
// 			}			
// 		}
		
		if((nRtn = pMD->SendCommand(SFT_CMD_CAN_SET_TRANS_DATA , nChIndex, preVData, sizeof(SFT_TRANS_CMD_CAN_SET1))) != SFT_ACK)
		{
			delete preVData;
			CString strMsg;
			strMsg.Format(Fun_FindMsg("SetCANTransDataToModule_msg1","CTSMonPro_DOC"), nRtn, _SFT_PROTOCOL_VERSION6);//strMsg.Format("통신 Data 설정 명령 전달 실패! Return Value : %x", nRtn);
			AfxMessageBox(strMsg);
			return FALSE;
		}
		delete preVData;
	}
	else if(pMD->GetProtocolVer() == _SFT_PROTOCOL_VERSION10)
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CAN_SET_TRANS_DATA, nChIndex, pCanData, sizeof(SFT_TRANS_CMD_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			strMsg.Format(Fun_FindMsg("SetCANTransDataToModule_msg1","CTSMonPro_DOC"), nRtn, _SFT_PROTOCOL_VERSION10);//strMsg.Format("통신 Data 설정 명령 전달 실패! Return Value : %x", nRtn);
			AfxMessageBox(strMsg);
			return FALSE;
		}
	}
	else if(pMD->GetProtocolVer() == _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : v1015 지원 추가
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CHANNEL_CAN_SET_TRANS_DATA, nChIndex, pCanData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			strMsg.Format(Fun_FindMsg("SetCANTransDataToModule_msg2","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION);//strMsg.Format("통신 Data 설정 명령 전달 실패(CH : %d)! Return Value : %x", nChIndex, nRtn);
			AfxMessageBox(strMsg);
			return FALSE;
		}

	}
	else if(pMD->GetProtocolVer() == _SFT_PROTOCOL_VERSION)
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CHANNEL_CAN_SET_TRANS_DATA, nChIndex, pCanData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			strMsg.Format(Fun_FindMsg("SetCANTransDataToModule_msg2","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION);//strMsg.Format("통신 Data 설정 명령 전달 실패(CH : %d)! Return Value : %x", nChIndex, nRtn);
			AfxMessageBox(strMsg);
			return FALSE;
		}

	}
	else
	{
		if((nRtn = pMD->SendCommand(SFT_CMD_CHANNEL_CAN_SET_TRANS_DATA, nChIndex, pCanData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET))) != SFT_ACK)
		{
			CString strMsg;
			strMsg.Format(Fun_FindMsg("SetCANTransDataToModule_msg2","CTSMonPro_DOC"), nChIndex, nRtn, _SFT_PROTOCOL_VERSION_ELSE);//strMsg.Format("통신 Data 설정 명령 전달 실패(CH : %d)! Return Value : %x", nChIndex, nRtn);
			AfxMessageBox(strMsg);
			return FALSE;
		}

	}
	
	
	return nRtn;
}

//Oven내 step변화를 감지 하고 다음 온도를 전송함
// => Oven 연동 채널에 대해서 모든 채널이 Pause(Oven 연동 대기) 이면 Oven 온도 변경 후 Oven 연동 Continue 전송
//=> 한개의 Oven에서 Oven연동으로 시작 한 채널은 Oven연동으로 시작한 첫번째 채널의 스케쥴을 따라감
// Oven연동 Channel이 모두 Paue(Oven 연동 대기)로 변했을 경우 새로운 온도 명령을 전송한다.

BOOL CCTSMonProDoc::CheckOven_Line1(int nOvenID)
{
	m_nChamberWriteCount1++;	//3초 타이머
	/////////////////////////////////////////////////////////////////////
	//ljb 201012 챔버 데이터 PSERVER.DLL로 전송
	CString strTemp;
	ZeroMemory(&m_sChamData_1,sizeof(SFT_CHAMBER_VALUE));

	strTemp.Format("%.1f",m_ctrlOven1.GetCurTemperature(nOvenID));
	m_sChamData_1.fCurTemper = atof(strTemp);
	m_sChamData_1.fRefTemper = m_ctrlOven1.GetRefTemperature(nOvenID); // 오븐의 설정 온도 데이터를 채널에 저장

	if (m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2500)
	{
		m_sChamData_1.fCurHumidity = m_ctrlOven1.GetCurHumidity(nOvenID); // 오븐의 현재 습도 데이터를 채널에 저장
		m_sChamData_1.fRefHumidity = m_ctrlOven1.GetRefHumidity(nOvenID); // 오븐의 설정 습도 데이터를 채널에 저장
	}	
	TRACE("Oven1 -> CurTemp = %.3f, RefTemp = %.3f",m_sChamData_1.fCurTemper,m_sChamData_1.fRefTemper);

	int mdIndex =1;
	//::SFTSetChamberData(mdIndex,0,&m_sChamData_1);		//챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 1//yulee 20190304 
	
	if(m_nChamberOperationType == 0)		//m_nChamberOperationType = 0 이면 한 챔버에 ch이 모두 있음.			
	{	
		//모든 모듈 - 채널에 값을 넣어 줘야 함.
		::SFTSetChamberData(mdIndex,1,&m_sChamData_1); //챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 2
	}
	//////////////////////////////////////////////////////////////////////////
	m_iOvenStartOption_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Start Chamber Option", 0);
	m_uiOvenCheckTime_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Fix Check Time", 0);
	m_uiOvenCheckFixTime_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Schedule Check Time", 0);
	
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Fix Temp","25.0");
	m_fOvenStartFixTemp_1 = atof(strTemp);
	
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Delta Fix Temp","1");
	m_fOvenDeltaFixTemp_1 = atof(strTemp);
	
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Delta Temp","1");
	m_fOvenDeltaTemp_1 = atof(strTemp);
	
	m_uiOvenPatternNum_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Pattern No", 1);
	
	//m_iOvenContinueType_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Chamber Continue Type", 0);		//ljb 20180226 add

	if (m_iOvenStartOption_1 < 1) return FALSE;		//ljb 챔버 연동 안함
	if (m_iOvenStartOption_1 > 3) return FALSE;		//ljb 챔버 연동 안함
	
	//ljb Oven 연동
	int nLinkChamberCount=0;		//ljb 챔버 연동 채널 갯수
	int nReadyNextChamberCount=0;	//ljb 챔버 연동 대기 채널 갯수	(nLinkChamberCount 와 같으면 다음 명령 전송)
	CDWordArray adwChArray;			//ljb 챔버 연동 모듈,채널 갯수
	BOOL	bOverTempSpec(FALSE);

	CCyclerModule *pMD; 
	CCyclerChannel *pCh;
	int nChannelIndex=0;	//ljb
	int nMinStepNo = 0x7fffffff;
	int nTotalCh = 2;					//기본 1채널만 해당됨
	
	//전체 모듈에 대해 챔버연동 대기 채널 검색
 	//for(mdIndex =1; mdIndex <= GetInstallModuleCount(); mdIndex++)
 	//{
		pMD = GetModuleInfo(mdIndex);
		if(pMD)
		{
			//Line Off
			if(pMD->GetState() == PS_STATE_LINE_OFF) return FALSE;
			
			if(m_nChamberOperationType == 0)	//nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 1);
			{
				nTotalCh = pMD->GetTotalChannel();	////1챔버에 모든 채널이 있음
			}
			for(int ch =0; ch < nTotalCh; ch++)
			{
				::SFTSetChamberData(mdIndex,ch,&m_sChamData_1);		//ljb 20151120 add	채널별로 챔버 데이터 전송
				pCh = pMD->GetChannelInfo(ch);
				if(pCh)
				{
					if(nOvenID == m_ctrlOven1.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
					{	
						//Idle인 상태
						if( pCh->GetState() == PS_STATE_IDLE || pCh->GetState() == PS_STATE_STANDBY ||
							pCh->GetState() == PS_STATE_END	|| pCh->GetState() == PS_STATE_READY )	
						{								
							//continue;
						}
						else if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
						{
							//ljb 20120627 병렬모드 일때도 skip
							TRACE("병렬모드 skip \n");
							//continue;
						}
						else if (pCh->GetState() == PS_STATE_MAINTENANCE)
						{
							//continue;
						}
						
						else if((pCh->GetChamberUsing() == TRUE) && ( pCh->GetStepType()  == PS_STEP_EXT_CAN)) 
						{//20180704 yulee External CAN으로 챔버 제어
							// 챔버 사용 중, External CAN 스텝이면 다음과 같은 동작을 한다.
							// CAN Ddata 중 코드 1700으로 등록된 값(Ref Temp.) 가져온다. 
							// 챔버의 먼저 설정된 값(SP)이 설정 할 온도(RT)와 같으면 Skip
							// 현재 챔버 온도(CT)와 설정 온도(RT)를 비교한다. - 비슷하면 skip 
							// SP와 RT가 다르면 RT가 0이 아닐 경우 챔버 온도 설정(m_ctrlOven1.SetTemperature)을 한다. 
							
							//CAN 데이터에서 값 가져오기 
							SFT_CAN_DATA * pCanData = pCh->GetCanData();
							if(pCanData == NULL) return FALSE;
							
							float fCanTref = 0.0;
							CString strItem = ("");
							for(int j = 0; j < pCh->GetCanCount(); j++)
							{
								if(pCanData[j].function_division == 1700)
								{
									if(pCanData[j].data_type == 0 || pCanData[j].data_type == 1 || pCanData[j].data_type == 2)
									{
										strItem.Format("%.6f", pCanData[j].canVal.fVal[0]);
										if (pCanData[j].function_division > 100 && pCanData[j].function_division < 150)
											strItem.Format("%02x%02x%02x%02x", (BYTE)pCanData[j].canVal.strVal[0], (BYTE)pCanData[j].canVal.strVal[1], (BYTE)pCanData[j].canVal.strVal[2], (BYTE)pCanData[j].canVal.strVal[3]);
										
										strItem.TrimRight("0");
										strItem.TrimRight(".");
									}
									else if(pCanData[j].data_type == 3){
										strItem.Format("%s", pCanData[j].canVal.strVal);
									}
									else if (pCanData[j].data_type == 4){
										strItem.Format("%x", (UINT)pCanData[j].canVal.fVal[0]);	
									}
									fCanTref = atof(strItem);
									break;
								}
							}
							
							//if(((strItem != NULL) || (strItem != (""))) && (fCanTref != 0.0))
							if((strItem != ("")) && (fCanTref != 0.0))
							{ 
								float fChamberSPTemp = 0.0;
								fChamberSPTemp = m_ctrlOven1.GetRefTemperature(nOvenID);
								if(fChamberSPTemp != fCanTref)
								{
									if ((m_sChamData_1.fCurTemper < (fCanTref - 0.2)) || (m_sChamData_1.fCurTemper > (fCanTref + 0.2)))
									{
										if(m_ctrlOven1.SetTemperature(0,nOvenID, fCanTref))
										{
											strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) \r\n",pCh->GetStepNo(), fCanTref, m_sChamData_1.fCurTemper, m_sChamData_1.fRefTemper );
											pCh->WriteLog(strTemp);
										}
										else
										{	strTemp.Format("STEP %d 챔버 온도설정 명령 전송실패 \r\n",pCh->GetStepNo());
											pCh->WriteLog(strTemp);
										}
									}
								}
							}
						}
						else
						{
							//Fix 모드에서 온도 범위 초과 체크
							if (m_iOvenStartOption_1 == 1 && m_uiOvenCheckFixTime_1 != 0)
							{
								if (m_ctrlOven1.GetRun(m_ctrlOven1.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetTotTime() > m_uiOvenCheckFixTime_1)//lmh 201111 chk 챔버 연동 가능하게 변경해야함
								{
									if ((fabs(m_sChamData_1.fRefTemper - m_sChamData_1.fCurTemper)) > m_fOvenDeltaFixTemp_1)
									{
										bOverTempSpec = TRUE;
										strTemp.Format("모듈(%s) 채널(%d) => Fix Mode 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,m_sChamData_1.fRefTemper,m_sChamData_1.fCurTemper,m_fOvenDeltaTemp_1);
										pCh->WriteLog(strTemp);
										WriteSysEventLog(strTemp);
									}
								}
								if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
								{
									adwChArray.Add(MAKELONG(ch, mdIndex));									
								}
								else
								{
								}
							}

							//스케쥴 연동
							if (m_iOvenStartOption_1 == 3 && pCh->GetChamberUsing())
							{
								//진행 시간과 온도 편차가 설정 값보다 크면 챔버 정지 및 충방전기 일시 정지, 챔버가 Prog 모드일 경우는?
								if (m_uiOvenCheckTime_1 != 0)
								{
									if (m_ctrlOven1.GetRun(m_ctrlOven1.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetStepTime() > m_uiOvenCheckTime_1)
									{
										if ((fabs(m_sChamData_1.fRefTemper - m_sChamData_1.fCurTemper)) > m_fOvenDeltaTemp_1)
										{
											bOverTempSpec = TRUE;
											strTemp.Format("모듈(%s) 채널(%d) => 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,m_sChamData_1.fRefTemper,m_sChamData_1.fCurTemper,m_fOvenDeltaTemp_1);
											pCh->WriteLog(strTemp);
											WriteSysEventLog(strTemp);
										}
									}
								}

								if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
								{
									nLinkChamberCount ++;
									adwChArray.Add(MAKELONG(ch, mdIndex));
								}
								else
								{

								}
								if (m_nChamberWriteCount1 > 20)
								{
									strTemp.Format("nLinkChamberCount %d, CellCode %d, Ch State %d ,Step No,%d, Step Type, %d,chamber Temp, PV, %.2f, SV, %.2f,chamber Humi, PV, %.2f, SV, %.2f",
										nLinkChamberCount, pCh->GetCellCode(),pCh->GetState(), pCh->GetStepNo(),pCh->GetStepType(),m_sChamData_1.fCurTemper,m_sChamData_1.fRefTemper,m_sChamData_1.fCurHumidity,m_sChamData_1.fRefHumidity);
									pCh->WriteChamberLog(strTemp);
									m_nChamberWriteCount1 = 0;
								}
							}

						}

						if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
						{
							//ljb 20120627 병렬모드 일때도 skip
							TRACE("병렬모드 skip \n");
							//continue;
						}
						else if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)	// 87 챔버연동 대기
						{
							nReadyNextChamberCount ++;
							nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호
						}
					}//end if(nOvenID == m_ctrlOven1.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
				}//end if (pCh)
			}//end for (ch)
		}//end if (pMD)
	//}//end for (mdIndex)
	
	//챔버연동으로 Pause 된 채널이 없으면 리턴
	if (m_iOvenStartOption_1 == 3 && nLinkChamberCount == 0 ) return FALSE;

	POSITION pos = GetFirstViewPosition();
	CCTSMonProView *pView = (CCTSMonProView *)GetNextView(pos);
	//일시 정지 및 챔버 STOP
	if (bOverTempSpec)
	{
		//★★ ljb 2010326  ★★★★★★★★ //챔버 연동된 채널에 Contiue 전송
		pView->SendCmdToModule(adwChArray, SFT_CMD_PAUSE);
		//1. 챔버 STOP
		if (m_ctrlOven1.SendStopCmd(0,nOvenID) == FALSE)
		{
			strTemp = "챔버 1 챔버 동작 중단 명령 실패(REST)!!!";
			WriteSysLog(strTemp);
		}
		strTemp.Format("챔버 1 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",m_sChamData_1.fRefTemper,m_sChamData_1.fCurTemper,m_fOvenDeltaTemp_1);
		WriteSysEventLog(strTemp);
		//AfxMessageBox(strTemp);
	}

	//스케줄 연동이 아니면 리턴 한다.
	if (m_iOvenStartOption_1 != 3 ) return FALSE;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//챔버 연동 명령어 전송 부분
	//-------------------------------------------------------
	// 	CCyclerModule *pMD;		//상위에 선언되어 있음.
	// 	CCyclerChannel *pCh;	//상위에 선언되어 있음.

	int nUsingChamberCh = 0, nRunStepNo = -1;

	DWORD dwData;
	BOOL nRtn = FALSE;	
	CString RegName;
	//BOOL bChamperContinue(FALSE);	//챔버연동 대기 플래그 값 

	pCh = NULL;
	nUsingChamberCh = adwChArray.GetSize();
	if (nReadyNextChamberCount > 0)
	{
		dwData = adwChArray.GetAt(0);
		pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));

		if(pCh)
		{
			nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호
						
			CScheduleData *lpSchedule = pCh->GetScheduleData();		//ljb 20180219 메모리로 업로드 하고 읽어 오는 구조로 변경 해야 할듯 함.
			if(lpSchedule)
			{							
				//CStep *lpStep = lpSchedule->GetStepData(nMinStepNo);
				CStep *lpStep = lpSchedule->GetStepData(nMinStepNo -1);		//ljb 20140108 add 현재스텝 가져오기

				//RegName.Format("UseOvenChamberContinue%d",pCh->GetChannelIndex() + 1);	
				//bChamperContinue = AfxGetApp()->GetProfileInt(OVEN_SETTING, RegName, 0);	//챔버대기 옵션 확인.	//ljb 20180212 default 를 0으로 변경
				RegName.Format("OvenContinue_%d",pCh->GetChannelIndex() + 1);	
				BOOL bChamberContinue;
				bChamberContinue = AfxGetApp()->GetProfileInt(OVEN_SETTING, RegName, 0);		
				
				if(bChamberContinue == FALSE)
				{
					if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
					{
						strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
						pCh->WriteLog(strTemp);
					}
					
					if(lpStep->m_fTref != 0.0f)
					{ 
						m_ctrlOven1.SetTemperature(0,nOvenID, lpStep->m_fTref);								
						strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_1.fCurTemper, m_sChamData_1.fRefTemper,bChamberContinue );
						pCh->WriteLog(strTemp);
					}

					if (m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2500)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							m_ctrlOven1.SetHumidity(0,nOvenID, lpStep->m_fHref);								
							strTemp.Format("STEP %d 챔버 설정습도 %.2f (챔버 현재습도: %.2f / 챔버 현재설정습도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fHref, m_sChamData_1.fCurHumidity, m_sChamData_1.fRefHumidity,bChamberContinue );
							pCh->WriteLog(strTemp);
						}
					}

					//strTemp.Format("%.1f",m_ctrlOven1.GetRefTemperature(nOvenID));		//ljb 20170719 add
					//fCurOvenSetTemp = atof(strTemp);
					//if (lpStep->m_fTref == fCurOvenSetTemp)
					//if ( ((lpStep->m_fTref - 0.2) < fCurOvenSetTemp) && (fCurOvenSetTemp < (lpStep->m_fTref + 0.2)))		//ljb 20180212 수정 float 변환으로 인한 설정값 이상을 의심
					//{
					//	pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	
					//	strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData.fCurTemper, m_sChamData.fRefTemper,bChamperContinue );
					//	lpChannel->WriteLog(strTemp);
					//	return TRUE;
					//}
 					//else
 					//	return FALSE;

					return TRUE;
				}//end if(m_iOvenContinueType_1 == FALSE)	

				if(lpStep->m_type == PS_STEP_LOOP)
				{			
					pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	//ljb 20130710 add
					strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) LOOP \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_1.fCurTemper, m_sChamData_1.fRefTemper );
					pCh->WriteLog(strTemp);
					return TRUE;
				}

				//ljb 20180219	// 챔버 온도 대기시 온도 비교 하는 부분 (현재 온도가 비슷하면 Continue 한다)
				if ( (m_sChamData_1.fCurTemper >= (lpStep->m_fTref - 0.2)) && 
					 (m_sChamData_1.fCurTemper <= (lpStep->m_fTref + 0.2)))
				{
					if (m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI880 || 
						m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2300 || 
						m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2500)
					{
						if ( (m_sChamData_1.fCurHumidity >= (lpStep->m_fHref - 0.2)) && 
							 (m_sChamData_1.fCurHumidity <= (lpStep->m_fHref + 0.2)))
						{
							if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
							{
								strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
								pCh->WriteLog(strTemp);
							}
							else
							{
								strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
								pCh->WriteLog(strTemp);
							}
							strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_1.fCurTemper, m_sChamData_1.fRefTemper);
							pCh->WriteLog(strTemp);
							return TRUE;
						}
					}
					else
					{
						if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
						{
							strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
							pCh->WriteLog(strTemp);
						}
						else
						{
							strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
							pCh->WriteLog(strTemp);
						}

						strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_1.fCurTemper, m_sChamData_1.fRefTemper);
						pCh->WriteLog(strTemp);
						return TRUE;
					}
				}//end if ((m_sChamData_1.fCurTemper >= (lpStep->m_fTref - 0.2)) && (m_sChamData_1.fCurTemper <= (lpStep->m_fTref + 0.2)))
			
				if(lpStep == NULL || lpStep->m_type == PS_STEP_END)
				{
					//챔버 STOP
					if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoOvenStop", FALSE))
						OvenStopStateAndSendStopCmd(1);			//챔버 1라인 
					return TRUE;
				}
				else
				{
					//float fSetOvenTempValue;
					//float fSetOvenHumValue; //ksj 20160310

					//if (lpStep->m_bUseChamberProg){
					//	fSetOvenTempValue = m_ctrlOven1.GetProgTemperature(nOvenID); // Prog 모드 온도 목표치						
					//	//ksj 20160310 Prog 모드에 습도값 넣어야하는가???
					//	fSetOvenHumValue = 0.0f; //일단 0으로 둠.					
					//}
					//else{				
					//	fSetOvenTempValue = m_ctrlOven1.GetRefTemperature(nOvenID); // 오븐의 현재 온도
					//	fSetOvenHumValue = m_ctrlOven1.GetRefHumidity(nOvenID);		//ksj 20160310 오븐 현재 습도.
					//}

					//ljb 설정 온도가 맞는지 비교 하는 부분 (목표 값으로 설정 되어 있으면 리턴 한다. 목표값이 아니면 아래로 내려서 설정값을 변경 한다.)
					if (lpStep->m_fTref == m_sChamData_1.fRefTemper || lpStep->m_fTref == 0.0f)
					{
						if (m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2500)
						{
							if (lpStep->m_fHref == m_sChamData_1.fRefHumidity || lpStep->m_fHref == 0)
							{
								return FALSE;
							}
						}
						else
						{
							return FALSE;	
						}	
					}

					//ljb 20180219 add 온도 및 습도 변경.
					if(lpStep->m_fTref != 0.0f)	m_ctrlOven1.SetTemperature(0,nOvenID, lpStep->m_fTref);								
					if (m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven1.GetOvenModelType() == CHAMBER_TEMI2500)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							m_ctrlOven1.SetHumidity(0,nOvenID, lpStep->m_fHref);								
						}
					}

					return TRUE;
				}
				// 여기서 종료------------------------------------------------------------------------------------------
				////////////////////////////////////////////////////////////////////////////////////////////////////////

					//습도 조건 추가. //ksj 20160310
					//if ((lpStep->m_fTref == fSetOvenTempValue && lpStep->m_fHref == fSetOvenHumValue) ||
					//	(lpStep->m_fTref == fSetOvenTempValue && lpStep->m_fHref == 0.0f) ||
					//	(lpStep->m_fTref == 0.0f && lpStep->m_fHref == fSetOvenHumValue) ||
					//	(lpStep->m_fTref == 0.0f && lpStep->m_fHref == 0.0f))
					//{
					//	return FALSE;
					//}

					TRACE("오븐셋팅 (온도: %.2f / 습도:%.2f)\r\n",lpStep->m_fTref, lpStep->m_fHref );
					if (lpStep->m_bUseChamberProg)
					{
						//챔버 Prog 모드로 운영
						//1. 챔버 STOP
						if (m_ctrlOven1.SendStopCmd(0,nOvenID) == FALSE)
						{
							strTemp = "챔버 동작 중단 명령 실패(REST)!!!";
							WriteSysLog(strTemp);
						}
						//2.현재 챔버 모드 확인
						if (m_ctrlOven1.GetRunWorkMode(m_ctrlOven1.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())))		// FALSE = PROG 모드, TRUE = FIX 모드
						{
							//챔버 Fix 모드 ->	Prog Mode 변환
							if (m_ctrlOven1.SetFixMode(0,nOvenID,FALSE) == FALSE)
							{
								strTemp = "챔버 PROG 모드로 변환 실패(REST)!!!";
								WriteSysLog(strTemp);
							}
						}
						//3. Pattern 99번에 Segment 1에 설정
						TRACE("STEP END TIME = %f\n",lpStep->m_fEndTime);
						if (m_ctrlOven1.SetPatternSegNo(0,nOvenID,99,1,lpStep->m_fTref,10.0,lpStep->m_fEndTime))	//시간은 분으로 입력해야 함
						{
							strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
						}
						else
						{
							strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
						}
						pCh->WriteLog(strTemp);
						WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
						//4. 챔버 Run
						if (m_ctrlOven1.SendRunCmd(0,nOvenID) == FALSE)
						{
							strTemp = "챔버 동작 실행 명령 실패(REST)!!!";
							WriteSysLog(strTemp);
						}
					}
					else
					{
						//챔버 Fix 모드로 운영
						//1.현재 챔버 모드 확인
						if (m_ctrlOven1.GetRunWorkMode(m_ctrlOven1.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) == FALSE)		// FALSE = PROG 모드, TRUE = FIX 모드
						{
							//챔버 Prog 모드
							//1. 챔버 STOP
							if (m_ctrlOven1.SendStopCmd(0,nOvenID) == FALSE)
							{
								strTemp = "Line 1 챔버 동작 중단 명령 실패(REST)!!!";
								WriteSysLog(strTemp);
							}
							//2.FIX 모드로 전환
							if (m_ctrlOven1.SetFixMode(0,nOvenID,TRUE) == FALSE)
							{
								strTemp = "Line 1 챔버 FIX 모드로 변환 실패!!!";
								WriteSysLog(strTemp);
							}
							//3.챔버 RUN
							if (m_ctrlOven1.SendRunCmd(0,nOvenID) == FALSE)
							{
								strTemp = "Line 1 챔버 동작 실행 명령 실패!!!";
								WriteSysLog(strTemp);
							}
						}

						//FIX 모드 챔버 온도 설정
						if(lpStep->m_fTref != 0.0f)
						{
							if(MAKE2LONG(lpStep->m_fTref*100.0f) != MAKE2LONG(m_ctrlOven1.GetRefTemperature(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
							{	
								nRtn = TRUE;
								if(m_ctrlOven1.SetTemperature(0,nOvenID, lpStep->m_fTref))
								{
									strTemp.Format("챔버(%d) 현재 SV %.2f℃ 에서 설정 SV %.2f℃ 로 전송 성공 (Step %d 설정 온도는 %.2f℃) ", nOvenID+1, m_ctrlOven1.GetRefTemperature(nOvenID), lpStep->m_fTref, lpStep->m_StepIndex+1,lpStep->m_fTref);
								}
								else
								{
									strTemp.Format("챔버%d 온도 설정 변경 실패 !!! Step %d 설정온도 = %.2f℃/ 챔버 설정 온도 %.2f℃ 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, m_ctrlOven1.GetRefTemperature(nOvenID));
								}
								//WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
								pCh->WriteLog(strTemp);
							}
						
							//Oven 소속된 모든 Channel에 온도 변경 Event 기록  
							//strTemp.Format("챔버(%d) 현재  온도 설정 변경 Step %d 설정온도 = %.2f℃ / 현재 설정 온도 %.2f℃ 변경 완료", nOvenID+1, lpStep->m_StepIndex+1,  lpStep->m_fTref, m_ctrlOven1.GetRefTemperature(nOvenID));
							//WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);							
							strTemp.Format("챔버(%d) 현재 SV %.2f℃ 에서 설정 SV %.2f℃ 로 변경 완료 (Step %d 설정 온도는 %.2f℃) ", nOvenID+1, m_ctrlOven1.GetRefTemperature(nOvenID), lpStep->m_fTref, lpStep->m_StepIndex+1,lpStep->m_fTref);
							//lpChannel->WriteLog(strTemp);
							
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
										//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1)// && lpChannel->GetState() == PS_STATE_RUN)// && lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						}

						//온도 변화 Rate 기록 
						if(lpStep->m_fTrate != 0.0f)		//0.0f이면 이전 상태 유지 
						{	
							nRtn = TRUE;
							if(m_ctrlOven1.SetTSlop(0,nOvenID, lpStep->m_fTrate))
							{
								strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							}
							else
							{
								strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							}							
							WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
							
							//Oven에 소속된 모든 Channel에 온도 변경 Event 기록  
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
									//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						}

						//습도 Ref 설정 
						if(lpStep->m_fHref != 0.0f) 
						{

							if(MAKE2LONG(lpStep->m_fHref*100.0f) != MAKE2LONG(m_ctrlOven1.GetRefHumidity(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
							{
								nRtn = TRUE;
								if(m_ctrlOven1.SetHumidity(0,nOvenID, lpStep->m_fHref))
								{
									strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, m_ctrlOven1.GetRefHumidity(nOvenID));
								}
								else
								{
									strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, m_ctrlOven1.GetRefHumidity(nOvenID));
								}
								WriteSysLog(strTemp);
							}

							//Ovne에 소속된 모든 Channel에 습도 변경 Event 기록  
							strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 변경 완료", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, m_ctrlOven1.GetRefHumidity(nOvenID));
							WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);	
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
									//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						}

						//습도 Slop 설정 
						if(lpStep->m_fHrate != 0.0f)		//0.0f이면 이전 상태 유지 
						{	
							nRtn = TRUE;
							//((CMainFrame *)AfxGetMainWnd())->SetOvenHSlop(a, lpStep->m_fHrate);	
							if(m_ctrlOven1.SetHSlop(0,nOvenID, lpStep->m_fHrate))
							{
								strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							}
							else
							{
								strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							}
							WriteSysLog(strTemp);							
							
							//Ovne에 소속된 모든 Channel에 온도 변경 Event 기록  
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
									//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						} //end if -> //습도 Slop 설정
					}//end if (lpStep->m_bUseChamberProg) else
				//} //end if -> if(lpStep == NULL ) else
				} //end if -> if(lpSchedule)
			else
			{
				//Schedule == NULL
				strTemp.Format("챔버%d 연동 lpSchedule Error  !!!", nOvenID+1);
				WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			}
		}//end if (nReadyNextChamberCount > 0)
	}
	return TRUE;
}

BOOL CCTSMonProDoc::CheckOven_Line2(int nOvenID)
{
	m_nChamberWriteCount2++;	//3초 타이머
	/////////////////////////////////////////////////////////////////////
	//ljb 201012 챔버 데이터 PSERVER.DLL로 전송
	CString strTemp;
	ZeroMemory(&m_sChamData_2,sizeof(SFT_CHAMBER_VALUE));

	strTemp.Format("%.1f",m_ctrlOven2.GetCurTemperature(nOvenID));
	m_sChamData_2.fCurTemper = atof(strTemp);
	m_sChamData_2.fRefTemper = m_ctrlOven2.GetRefTemperature(nOvenID); // 오븐의 설정 온도 데이터를 채널에 저장

	if (m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2500)
	{
		m_sChamData_2.fCurHumidity = m_ctrlOven2.GetCurHumidity(nOvenID); // 오븐의 현재 습도 데이터를 채널에 저장
		m_sChamData_2.fRefHumidity = m_ctrlOven2.GetRefHumidity(nOvenID); // 오븐의 설정 습도 데이터를 채널에 저장
	}	
	TRACE("Oven2 -> CurTemp = %.3f, RefTemp = %.3f",m_sChamData_2.fCurTemper,m_sChamData_2.fRefTemper);

	int mdIndex =1;
	//챔버설정에 할당된 채널에 모두 보내야 함. (추 후 수정)
//	::SFTSetChamberData(mdIndex,1,&m_sChamData_2);		//챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 2 //yulee 20190304
	
	
	//if(m_nChamberOperationType == 0)		//m_nChamberOperationType = 0 이면 한 챔버에 ch이 모두 있음.			
	//{	
	//	::SFTSetChamberData(mdIndex,1,&m_sChamData_2); //챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 2
	//}
	//////////////////////////////////////////////////////////////////////////

	m_iOvenStartOption_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Start Chamber Option", 0);
	m_uiOvenCheckTime_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Fix Check Time", 0);
	m_uiOvenCheckFixTime_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Schedule Check Time", 0);
	
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG4, "Fix Temp","25.0");
	m_fOvenStartFixTemp_2 = atof(strTemp);
	
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG4, "Delta Fix Temp","1");
	m_fOvenDeltaFixTemp_2 = atof(strTemp);
	
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG4, "Delta Temp","1");
	m_fOvenDeltaTemp_2 = atof(strTemp);
	
	m_uiOvenPatternNum_2 = AfxGetApp()->GetProfileInt(OVEN_SETTING, "Pattern No", 1);
	//m_iOvenContinueType_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Chamber Continue Type", 0);		//ljb 20180226 add


	if (m_iOvenStartOption_2 < 1) return FALSE;		//ljb 챔버 연동 안함
	if (m_iOvenStartOption_2 > 3) return FALSE;		//ljb 챔버 연동 안함
	
	//ljb Oven 연동
	int nLinkChamberCount=0;		//ljb 챔버 연동 채널 갯수
	int nReadyNextChamberCount=0;	//ljb 챔버 연동 대기 채널 갯수	(nLinkChamberCount 와 같으면 다음 명령 전송)
	CDWordArray adwChArray;			//ljb 챔버 연동 모듈,채널 갯수
	BOOL	bOverTempSpec(FALSE);

	CCyclerModule *pMD; 
	CCyclerChannel *pCh;
	int nChannelIndex=0;	//ljb
	int nMinStepNo = 0x7fffffff;
	int nTotalCh = 4;					//기본 1채널만 해당됨
	
	//전체 모듈에 대해 챔버연동 대기 채널 검색
	//for(mdIndex =1; mdIndex <= GetInstallModuleCount(); mdIndex++)
	//{
		pMD = GetModuleInfo(mdIndex);
		if(pMD)
		{
			//Line Off
			if(pMD->GetState() == PS_STATE_LINE_OFF) return FALSE;

			if(m_nChamberOperationType == 0)	//nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 1);
			{
				return FALSE;
				//nTotalCh = pMD->GetTotalChannel();	////1챔버에 모든 채널이 있음
			}
			for(int ch =1; ch <= nTotalCh; ch++)		//2번 채널 하나만 비교 한다.
			/*for(int ch =1; ch <= pMD->GetTotalChannel(); ch++)		//2번 채널 하나만 비교 한다.*/
			{
				::SFTSetChamberData(mdIndex,ch,&m_sChamData_2);		//ljb 20151120 add	채널별로 챔버 데이터 전송
				pCh = pMD->GetChannelInfo(ch);
				if(pCh)
				{
					if(nOvenID == m_ctrlOven2.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
					{					
						//Idle인 상태
						if( pCh->GetState() == PS_STATE_IDLE || pCh->GetState() == PS_STATE_STANDBY ||
							pCh->GetState() == PS_STATE_END	|| pCh->GetState() == PS_STATE_READY )	
						{								
							//continue;
						}
						else if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
						{
							//ljb 20120627 병렬모드 일때도 skip
							TRACE("병렬모드 skip \n");
							//continue;
						}
						else if (pCh->GetState() == PS_STATE_MAINTENANCE)
						{
							//continue;
						}
						
						else if((pCh->GetChamberUsing() == TRUE) && ( pCh->GetStepType()  == PS_STEP_EXT_CAN)) 
						{//20180704 yulee External CAN으로 챔버 제어
							// 챔버 사용 중, External CAN 스텝이면 다음과 같은 동작을 한다.
							// CAN Ddata 중 코드 1700으로 등록된 값(Ref Temp.) 가져온다. 
							// 챔버의 먼저 설정된 값(SP)이 설정 할 온도(RT)와 같으면 Skip
							// 현재 챔버 온도(CT)와 설정 온도(RT)를 비교한다. - 비슷하면 skip 
							// SP와 RT가 다르면 RT가 0이 아닐 경우 챔버 온도 설정(m_ctrlOven1.SetTemperature)을 한다. 
							
							//CAN 데이터에서 값 가져오기 
							SFT_CAN_DATA * pCanData = pCh->GetCanData();
							if(pCanData == NULL) return FALSE;
							
							float fCanTref = 0.0;
							CString strItem = ("");
							for(int j = 0; j < pCh->GetCanCount(); j++)
							{
								if(pCanData[j].function_division == 1700)
								{
									if(pCanData[j].data_type == 0 || pCanData[j].data_type == 1 || pCanData[j].data_type == 2)
									{
										strItem.Format("%.6f", pCanData[j].canVal.fVal[0]);
										if (pCanData[j].function_division > 100 && pCanData[j].function_division < 150)
											strItem.Format("%02x%02x%02x%02x", (BYTE)pCanData[j].canVal.strVal[0], (BYTE)pCanData[j].canVal.strVal[1], (BYTE)pCanData[j].canVal.strVal[2], (BYTE)pCanData[j].canVal.strVal[3]);
										
										strItem.TrimRight("0");
										strItem.TrimRight(".");
									}
									else if(pCanData[j].data_type == 3){
										strItem.Format("%s", pCanData[j].canVal.strVal);
									}
									else if (pCanData[j].data_type == 4){
										strItem.Format("%x", (UINT)pCanData[j].canVal.fVal[0]);	
									}
									fCanTref = atof(strItem);
									break;
								}
							}
							
							//if(((strItem != NULL) || (strItem != (""))) && (fCanTref != 0.0))
							if((strItem != ("")) && (fCanTref != 0.0))
							{ 
								float fChamberSPTemp = 0.0;
								fChamberSPTemp = m_ctrlOven2.GetRefTemperature(nOvenID);
								if(fChamberSPTemp != fCanTref)
								{
									if ((m_sChamData_2.fCurTemper < (fCanTref - 0.2)) || (m_sChamData_2.fCurTemper > (fCanTref + 0.2)))
									{
										if(m_ctrlOven2.SetTemperature(0,nOvenID, fCanTref))
										{
											strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) \r\n",pCh->GetStepNo(), fCanTref, m_sChamData_2.fCurTemper, m_sChamData_2.fRefTemper );
											pCh->WriteLog(strTemp);
										}
										else
										{	strTemp.Format("STEP %d 챔버 온도설정 명령 전송실패 \r\n",pCh->GetStepNo());
										pCh->WriteLog(strTemp);
										}
									}
								}
							}
						}
						
						else
						{
							//Fix 모드에서 온도 범위 초과 체크
							if (m_iOvenStartOption_2 == 1 && m_uiOvenCheckFixTime_2 != 0)
							{
								if (m_ctrlOven2.GetRun(m_ctrlOven2.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetTotTime() > m_uiOvenCheckFixTime_2)//lmh 201111 chk 챔버 연동 가능하게 변경해야함
								{
									if ((fabs(m_sChamData_2.fRefTemper - m_sChamData_2.fCurTemper)) > m_fOvenDeltaFixTemp_2)
									{
										bOverTempSpec = TRUE;//$1013
										strTemp.Format("모듈(%s) 채널(%d) => Fix Mode 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,m_sChamData_2.fRefTemper,m_sChamData_2.fCurTemper,m_fOvenDeltaTemp_2);
										pCh->WriteLog(strTemp);
										WriteSysEventLog(strTemp);
									}
								}
								if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
								{
									adwChArray.Add(MAKELONG(ch, mdIndex));									
								}
								else
								{
								}
							}

							//스케쥴 연동
							if (m_iOvenStartOption_2 == 3 && pCh->GetChamberUsing())
							{
								//진행 시간과 온도 편차가 설정 값보다 크면 챔버 정지 및 충방전기 일시 정지, 챔버가 Prog 모드일 경우는?
								if (m_uiOvenCheckTime_2 != 0)
								{
									if (m_ctrlOven2.GetRun(m_ctrlOven2.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetStepTime() > m_uiOvenCheckTime_2)
									{
										if ((fabs(m_sChamData_2.fRefTemper - m_sChamData_2.fCurTemper)) > m_fOvenDeltaTemp_2)
										{
											bOverTempSpec = TRUE;//$1013
											strTemp.Format("모듈(%s) 채널(%d) => 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,m_sChamData_2.fRefTemper,m_sChamData_2.fCurTemper,m_fOvenDeltaTemp_2);
											pCh->WriteLog(strTemp);
											WriteSysEventLog(strTemp);
										}
									}
								}

								if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
								{
									nLinkChamberCount ++;
									adwChArray.Add(MAKELONG(ch, mdIndex));
								}
								else
								{

								}
								if (m_nChamberWriteCount2 > 20)
								{
									strTemp.Format("nLinkChamberCount %d, CellCode %d, Ch State %d ,Step No,%d, Step Type, %d,chamber Temp, PV, %.2f, SV, %.2f,chamber Humi, PV, %.2f, SV, %.2f",
										nLinkChamberCount, pCh->GetCellCode(),pCh->GetState(), pCh->GetStepNo(),pCh->GetStepType(),m_sChamData_2.fCurTemper,m_sChamData_2.fRefTemper,m_sChamData_2.fCurHumidity,m_sChamData_2.fRefHumidity);
									pCh->WriteChamberLog(strTemp);
									m_nChamberWriteCount2 = 0;
								}
							}

						}

						if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
						{
							//ljb 20120627 병렬모드 일때도 skip
							TRACE("병렬모드 skip \n");
							//continue;
						}
						else if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)	// 87 챔버연동 대기
						{
							nReadyNextChamberCount ++;
							nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호
						}
					}//end if(nOvenID == m_ctrlOven2.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
				}//end if (pCh)
			}//end for (ch)
		}//end if (pMD)
	//}//end for (mdIndex)
	
	//챔버연동으로 Pause 된 채널이 없으면 리턴
	if (m_iOvenStartOption_2 == 3 && nLinkChamberCount == 0 ) return FALSE;

	POSITION pos = GetFirstViewPosition();
	CCTSMonProView *pView = (CCTSMonProView *)GetNextView(pos);
	//일시 정지 및 챔버 STOP
	if (bOverTempSpec)
	{
		//★★ ljb 2010326  ★★★★★★★★ //챔버 연동된 채널에 Contiue 전송
		pView->SendCmdToModule(adwChArray, SFT_CMD_PAUSE);
		//1. 챔버 STOP
		if (m_ctrlOven2.SendStopCmd(0,nOvenID) == FALSE)
		{
			strTemp = "챔버 2 챔버 동작 중단 명령 실패(REST)!!!";//$1013
			WriteSysLog(strTemp);
		}//$1013
		strTemp.Format("챔버 2 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",m_sChamData_2.fRefTemper,m_sChamData_2.fCurTemper,m_fOvenDeltaTemp_2);
		WriteSysEventLog(strTemp);
		//AfxMessageBox(strTemp);
	}

	//스케줄 연동이 아니면 리턴 한다.
	if (m_iOvenStartOption_2 != 3 ) return FALSE;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//챔버 연동 명령어 전송 부분
	//-------------------------------------------------------
	// 	CCyclerModule *pMD;		//상위에 선언되어 있음.
	// 	CCyclerChannel *pCh;	//상위에 선언되어 있음.

	int nUsingChamberCh = 0, nRunStepNo = -1;

	DWORD dwData;
	BOOL nRtn = FALSE;	
	CString RegName;
	//BOOL bChamperContinue(FALSE);	//챔버연동 대기 플래그 값 

	pCh = NULL;
	nUsingChamberCh = adwChArray.GetSize();
	if (nReadyNextChamberCount > 0)
	{
		dwData = adwChArray.GetAt(0);
		pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));

		if(pCh)
		{
			nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호
						
			CScheduleData *lpSchedule = pCh->GetScheduleData();		//ljb 20180219 메모리로 업로드 하고 읽어 오는 구조로 변경 해야 할듯 함.
			if(lpSchedule)
			{							
				CStep *lpStep = lpSchedule->GetStepData(nMinStepNo -1);		//ljb 20140108 add 현재스텝 가져오기
				
				//yulee 20181113
				RegName.Format("OvenContinue_%d",pCh->GetChannelIndex() + 1);	
				BOOL bChamberContinue;
				bChamberContinue = AfxGetApp()->GetProfileInt(OVEN_SETTING, RegName, 0);
				

				if(bChamberContinue == FALSE)
				{											
					if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
					{//$1013
						strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
						pCh->WriteLog(strTemp);
					}
					
					if(lpStep->m_fTref != 0.0f)
					{ 
						m_ctrlOven2.SetTemperature(0,nOvenID, lpStep->m_fTref);//$1013						
						strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_2.fCurTemper, m_sChamData_2.fRefTemper,bChamberContinue );
						pCh->WriteLog(strTemp);
					}

					if (m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2500)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							m_ctrlOven2.SetHumidity(0,nOvenID, lpStep->m_fHref);//$1013								
							strTemp.Format("STEP %d 챔버 설정습도 %.2f (챔버 현재습도: %.2f / 챔버 현재설정습도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fHref, m_sChamData_2.fCurHumidity, m_sChamData_2.fRefHumidity,bChamberContinue );
							pCh->WriteLog(strTemp);
						}
					}

					//strTemp.Format("%.1f",m_ctrlOven2.GetRefTemperature(nOvenID));		//ljb 20170719 add
					//fCurOvenSetTemp = atof(strTemp);
					//if (lpStep->m_fTref == fCurOvenSetTemp)
					//if ( ((lpStep->m_fTref - 0.2) < fCurOvenSetTemp) && (fCurOvenSetTemp < (lpStep->m_fTref + 0.2)))		//ljb 20180212 수정 float 변환으로 인한 설정값 이상을 의심
					//{
					//	pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	
					//	strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData.fCurTemper, m_sChamData.fRefTemper,bChamperContinue );
					//	lpChannel->WriteLog(strTemp);
					//	return TRUE;
					//}
 					//else
 					//	return FALSE;

					return TRUE;
				}//end if(m_iOvenContinueType_2 == FALSE)

				if(lpStep->m_type == PS_STEP_LOOP)
				{//$1013
					pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	//ljb 20130710 add
					strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) LOOP \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_2.fCurTemper, m_sChamData_2.fRefTemper );
					pCh->WriteLog(strTemp);
					return TRUE;
				}

				//ljb 20180219	// 챔버 온도 대기시 온도 비교 하는 부분
				if ((m_sChamData_2.fCurTemper >= (lpStep->m_fTref - 0.2)) && (m_sChamData_2.fCurTemper <= (lpStep->m_fTref + 0.2)))
				{
					if (m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2500)
					{
						if ((m_sChamData_2.fCurHumidity >= (lpStep->m_fHref - 0.2)) && (m_sChamData_2.fCurHumidity <= (lpStep->m_fHref + 0.2)))
						{
							if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
							{//$1013
								strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
								pCh->WriteLog(strTemp);
							}
							else
							{
								strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
								pCh->WriteLog(strTemp);
							}
							strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_2.fCurTemper, m_sChamData_2.fRefTemper);
							pCh->WriteLog(strTemp);
							return TRUE;
						}
					}
					else
					{
						if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
						{//$1013
							strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
							pCh->WriteLog(strTemp);
						}
						else
						{
							strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
							pCh->WriteLog(strTemp);
						}

						strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData_2.fCurTemper, m_sChamData_2.fRefTemper);
						pCh->WriteLog(strTemp);
						return TRUE;
					}
				}
				
				if(lpStep == NULL || lpStep->m_type == PS_STEP_END)
				{
					//챔버 STOP
					if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoOvenStop", FALSE))
						OvenStopStateAndSendStopCmd(1);			//챔버 1라인 
					return TRUE;
				}
				else
				{
					//ljb 설정 온도가 맞는지 비교 하는 부분 (목표 값으로 설정 되어 있으면 리턴 한다. 목표값이 아니면 아래로 내려서 설정값을 변경 한다.)
					if (lpStep->m_fTref == m_sChamData_2.fRefTemper || lpStep->m_fTref == 0.0f)
					{
						if (m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2500)
						{
							if (lpStep->m_fHref == m_sChamData_2.fRefHumidity || lpStep->m_fHref == 0)
							{
								return FALSE;
							}
						}
						else
						{
							return FALSE;	
						}
						
					}


					//ljb 20180219 add 온도 및 습도 변경.
					if(lpStep->m_fTref != 0.0f)	m_ctrlOven2.SetTemperature(0,nOvenID, lpStep->m_fTref);								
					if (m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI880 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2300 || m_ctrlOven2.GetOvenModelType() == CHAMBER_TEMI2500)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							m_ctrlOven2.SetHumidity(0,nOvenID, lpStep->m_fHref);								
						}
					}

					return TRUE;
				}
				// 여기서 종료------------------------------------------------------------------------------------------
				////////////////////////////////////////////////////////////////////////////////////////////////////////

					TRACE("오븐셋팅 (온도: %.2f / 습도:%.2f)\r\n",lpStep->m_fTref, lpStep->m_fHref );
					if (lpStep->m_bUseChamberProg)
					{
						//챔버 Prog 모드로 운영
						//1. 챔버 STOP
						if (m_ctrlOven2.SendStopCmd(0,nOvenID) == FALSE)
						{
							strTemp = "챔버 동작 중단 명령 실패(REST)!!!";//$1013
							WriteSysLog(strTemp);
						}
						//2.현재 챔버 모드 확인
						if (m_ctrlOven2.GetRunWorkMode(m_ctrlOven2.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())))		// FALSE = PROG 모드, TRUE = FIX 모드
						{
							//챔버 Fix 모드 ->	Prog Mode 변환
							if (m_ctrlOven2.SetFixMode(0,nOvenID,FALSE) == FALSE)
							{
								strTemp = "챔버 PROG 모드로 변환 실패(REST)!!!";//$1013
								WriteSysLog(strTemp);
							}
						}
						//3. Pattern 99번에 Segment 1에 설정
						TRACE("STEP END TIME = %f\n",lpStep->m_fEndTime);
						if (m_ctrlOven2.SetPatternSegNo(0,nOvenID,99,1,lpStep->m_fTref,10.0,lpStep->m_fEndTime))	//시간은 분으로 입력해야 함
						{//$1013
							strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
						}
						else
						{//$1013
							strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
						}
						pCh->WriteLog(strTemp);
						WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
						//4. 챔버 Run
						if (m_ctrlOven2.SendRunCmd(0,nOvenID) == FALSE)
						{
							strTemp = "Line 2 챔버 동작 실행 명령 실패(REST)!!!";
							WriteSysLog(strTemp);
						}
					}
					else
					{
						//챔버 Fix 모드로 운영
						//1.현재 챔버 모드 확인
						if (m_ctrlOven2.GetRunWorkMode(m_ctrlOven2.GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) == FALSE)		// FALSE = PROG 모드, TRUE = FIX 모드
						{
							//챔버 Prog 모드
							//1. 챔버 STOP
							if (m_ctrlOven2.SendStopCmd(0,nOvenID) == FALSE)
							{
								strTemp = "Line 2 챔버 동작 중단 명령 실패(REST)!!!";
								WriteSysLog(strTemp);
							}
							//2.FIX 모드로 전환
							if (m_ctrlOven2.SetFixMode(0,nOvenID,TRUE) == FALSE)
							{
								strTemp = "Line 2 챔버 FIX 모드로 변환 실패!!!";
								WriteSysLog(strTemp);
							}
							//3.챔버 RUN
							if (m_ctrlOven2.SendRunCmd(0,nOvenID) == FALSE)
							{
								strTemp = "Line 2 챔버 동작 실행 명령 실패!!!";
								WriteSysLog(strTemp);
							}
						}

						//FIX 모드 챔버 온도 설정
						if(lpStep->m_fTref != 0.0f)
						{
							if(MAKE2LONG(lpStep->m_fTref*100.0f) != MAKE2LONG(m_ctrlOven2.GetRefTemperature(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
							{	
								nRtn = TRUE;
								if(m_ctrlOven2.SetTemperature(0,nOvenID, lpStep->m_fTref))
								{
									strTemp.Format("Line 2 챔버%d 온도 설정 변경 성공 Step %d =>%.2f℃/%.2f℃ 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, m_ctrlOven2.GetRefTemperature(nOvenID));
								}
								else
								{
									strTemp.Format("Line 2 챔버%d 온도 설정 변경 실패 !!! Step %d =>%.2f℃/%.2f℃ 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, m_ctrlOven2.GetRefTemperature(nOvenID));
								}
								WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
								//strTemp.Format("Line 2 챔버 온도 설정 변경 %d/%d", MAKE2LONG(lpStep->m_fTref*100.0f), MAKE2LONG(m_ctrlOven2.GetRefTemperature(a)*100.0f));
							}
						
							//Oven 소속된 모든 Channel에 온도 변경 Event 기록  
							//strTemp.Format("챔버(%d) 현재  온도 설정 변경 Step %d 설정온도 = %.2f℃ / 현재 설정 온도 %.2f℃ 변경 완료", nOvenID+1, lpStep->m_StepIndex+1,  lpStep->m_fTref, m_ctrlOven2.GetRefTemperature(nOvenID));
							//WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);		//$1013					
							strTemp.Format("챔버(%d) 현재 SV %.2f℃ 에서 설정 SV %.2f℃ 로 변경 완료 (Step %d 설정 온도는 %.2f℃) ", nOvenID+1, m_ctrlOven2.GetRefTemperature(nOvenID), lpStep->m_fTref, lpStep->m_StepIndex+1,lpStep->m_fTref);
							//lpChannel->WriteLog(strTemp);
							
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
										//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1)// && lpChannel->GetState() == PS_STATE_RUN)// && lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						}

						//온도 변화 Rate 기록 
						if(lpStep->m_fTrate != 0.0f)		//0.0f이면 이전 상태 유지 
						{	
							nRtn = TRUE;
							if(m_ctrlOven2.SetTSlop(0,nOvenID, lpStep->m_fTrate))
							{//$1013
								strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							}
							else
							{
								strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							}							
							WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
							
							//Oven에 소속된 모든 Channel에 온도 변경 Event 기록  
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
									//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						}

						//습도 Ref 설정 
						if(lpStep->m_fHref != 0.0f) 
						{

							if(MAKE2LONG(lpStep->m_fHref*100.0f) != MAKE2LONG(m_ctrlOven2.GetRefHumidity(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
							{
								nRtn = TRUE;
								if(m_ctrlOven2.SetHumidity(0,nOvenID, lpStep->m_fHref))
								{//$1013
									strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, m_ctrlOven2.GetRefHumidity(nOvenID));
								}
								else
								{
									strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, m_ctrlOven2.GetRefHumidity(nOvenID));
								}
								WriteSysLog(strTemp);
							}

							//Ovne에 소속된 모든 Channel에 습도 변경 Event 기록  
							strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 변경 완료", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, m_ctrlOven2.GetRefHumidity(nOvenID));
							WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);	
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
									//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						}

						//습도 Slop 설정 
						if(lpStep->m_fHrate != 0.0f)		//0.0f이면 이전 상태 유지 
						{	
							nRtn = TRUE;
							//((CMainFrame *)AfxGetMainWnd())->SetOvenHSlop(a, lpStep->m_fHrate);	
							if(m_ctrlOven2.SetHSlop(0,nOvenID, lpStep->m_fHrate))
							{//$1013
								strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							}
							else
							{
								strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							}
							WriteSysLog(strTemp);							
							
							//Ovne에 소속된 모든 Channel에 온도 변경 Event 기록  
							for(int b=0; b<adwChArray.GetSize(); b++)
							{
								dwData = adwChArray.GetAt(b);
								pCh = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pCh)
								{
									//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
									if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
									{
										pCh->WriteLog(strTemp);
									}
								}
							}
						} //end if -> //습도 Slop 설정
					}//end if (lpStep->m_bUseChamberProg) else
				//} //end if -> if(lpStep == NULL ) else
				} //end if -> if(lpSchedule)
			else
			{
				//Schedule == NULL
				strTemp.Format("챔버%d 연동 lpSchedule Error  !!!", nOvenID+1);
				WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			}
		}//end if (nReadyNextChamberCount > 0)
	}
	return TRUE;
}

//주어진 Modue/Channel list에서 Oven 상태 검사 후 Stop 명령 전송
void CCTSMonProDoc::CheckOvenStopStateAndSendStopCmd(CDWordArray &adwCheckChArray, int nChNo)
{
	CString strTemp;
	CDWordArray awOvenArray;

	if (nChNo == 1)
	{
		int	 nOvenID=1;
		m_ChamberCtrl.m_ctrlOven1.GetOvenIndexList(adwCheckChArray, awOvenArray);		//해당하는 Oven List를 구함
		
		DWORD dwData;
		//	strTemp.Format("Oven Size : %d", awOvenArray.GetSize());
		//	WriteSysLog(strTemp);
		for(int a = 0; a<awOvenArray.GetSize(); a++)
		{
			nOvenID = a+1;
			dwData = awOvenArray.GetAt(a);
			if(m_ChamberCtrl.m_ctrlOven1.GetRun(dwData) == FALSE)	
			{//$1013
			//strTemp.Format("챔버 %d 동작중이 아님 Time (%ld)", dwData+1, m_ChamberCtrl.m_ctrlOven1.GetRunTime(dwData));
			strTemp.Format(Fun_FindMsg("CheckOvenStopStateAndSendStopCmd_msg1","CTSMonPro_DOC"), dwData+1, m_ChamberCtrl.m_ctrlOven1.GetRunTime(dwData)); 
				WriteSysLog(strTemp);
				continue;		
			}
			
			CDWordArray adwChArray;
			m_ChamberCtrl.m_ctrlOven1.GetChannelMapArray(dwData, adwChArray);			//Oven에 속한 Channel을 구함
			
			CCyclerChannel *lpChannel;
			BOOL bCanStop = TRUE;
			
			for(int b=0; b<adwChArray.GetSize(); b++)
			{
				dwData = adwChArray.GetAt(b);
				
				//SBC 상태 전이가 늦으므로(Stop으로 전이 했다고 보고) 선택된 Channel을 제외한 나머지 Channel만 검사한다.
				//adwCheckChArray에 포함된 Channel은 제외한다. (이미 Stop명령을 보낸상태 이므로 )
				BOOL bFindCh = FALSE;
				for(int c = 0; c<adwCheckChArray.GetSize(); c++)
				{
					if(adwCheckChArray[c] == dwData)
					{
						bFindCh = TRUE;
						lpChannel = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
						if(lpChannel)
						{
							lpChannel->SetUseOvenMode(FALSE);
							
							//	strTemp.Format("Oven Stop Cmd : Ch : %d", lpChannel->GetChannelIndex());
							//	WriteSysLog(strTemp);
						}
						break;
					}
				}
				if(bFindCh)	continue;
				
				lpChannel = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
				if(lpChannel)
				{
					//이미 Stop명령이 하달된 상태이므로 선택된 Cahnnel은 Stop 상태 이어야 한다.
					//Run 중인 Channel은 다른 Channel
					if((lpChannel->GetState() == PS_STATE_RUN || lpChannel->GetState() == PS_STATE_PAUSE) && lpChannel->GetUseOvenMode())
					{
						bCanStop = FALSE;	
						TRACE("bCanStop Set False\n");
						
					}
				}
			}
			
			//		strTemp.Format("Can Stop : %d", bCanStop);
			//		WriteSysLog(strTemp);
			TRACE("bCanStop : %d\n",  bCanStop);
			if(bCanStop && m_ChamberCtrl.m_ctrlOven1.GetRun(awOvenArray.GetAt(a)) == TRUE)
			{
			//strTemp.Format("챔버%d 는 작업중인 Channel이 없습니다. 챔버 동작을 멈추시겠습니까?", awOvenArray.GetAt(a)+1);
			strTemp.Format(Fun_FindMsg("CheckOvenStopStateAndSendStopCmd_msg2","CTSMonPro_DOC"), awOvenArray.GetAt(a)+1);
				if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDYES)
				{
					//((CMainFrame *)AfxGetMainWnd())->SendOvenStopCmd(awOvenArray.GetAt(a));
					if(m_ChamberCtrl.m_ctrlOven1.SendStopCmd(0,awOvenArray.GetAt(a)))    // 20090527 KHM 챔버중단?? 동작??
					{
					strTemp.Format(Fun_FindMsg("CheckOvenStopStateAndSendStopCmd_msg3","CTSMonPro_DOC"), awOvenArray.GetAt(a)+1);	//strTemp.Format("챔버 %d 동작 중단 명령 전송", awOvenArray.GetAt(a)+1);
					}
					else
					{//$1013
						//strTemp.Format("챔버 %d 동작 중단 명령 실패!!!", awOvenArray.GetAt(a)+1);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOvenStopStateAndSendStopCmd_msg1","CTSMonPro_DOC"), awOvenArray.GetAt(a)+1);//&&
					}
					WriteSysLog(strTemp);
				}
			}
		}
	}
	else if(nChNo == 2)
	{
		int	 nOvenID=1;
		m_ChamberCtrl.m_ctrlOven2.GetOvenIndexList(adwCheckChArray, awOvenArray);		//해당하는 Oven List를 구함
		
		DWORD dwData;
		//	strTemp.Format("Oven Size : %d", awOvenArray.GetSize());
		//	WriteSysLog(strTemp);
		for(int a = 0; a<awOvenArray.GetSize(); a++)
		{
			nOvenID = a+1;
			dwData = awOvenArray.GetAt(a);
			if(m_ChamberCtrl.m_ctrlOven2.GetRun(dwData) == FALSE)	
			{//$1013
				//strTemp.Format("챔버 %d 동작중이 아님 Time (%ld)", dwData+1, m_ChamberCtrl.m_ctrlOven2.GetRunTime(dwData));
				strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOvenStopStateAndSendStopCmd_msg2","CTSMonPro_DOC"), dwData+1, m_ChamberCtrl.m_ctrlOven2.GetRunTime(dwData));//&&
				WriteSysLog(strTemp);
				continue;		
			}
			
			CDWordArray adwChArray;
			m_ChamberCtrl.m_ctrlOven2.GetChannelMapArray(dwData, adwChArray);			//Oven에 속한 Channel을 구함
			
			CCyclerChannel *lpChannel;
			BOOL bCanStop = TRUE;
			
			for(int b=0; b<adwChArray.GetSize(); b++)
			{
				dwData = adwChArray.GetAt(b);
				
				//SBC 상태 전이가 늦으므로(Stop으로 전이 했다고 보고) 선택된 Channel을 제외한 나머지 Channel만 검사한다.
				//adwCheckChArray에 포함된 Channel은 제외한다. (이미 Stop명령을 보낸상태 이므로 )
				BOOL bFindCh = FALSE;
				for(int c = 0; c<adwCheckChArray.GetSize(); c++)
				{
					if(adwCheckChArray[c] == dwData)
					{
						bFindCh = TRUE;
						lpChannel = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
						if(lpChannel)
						{
							lpChannel->SetUseOvenMode(FALSE);
							
							//	strTemp.Format("Oven Stop Cmd : Ch : %d", lpChannel->GetChannelIndex());
							//	WriteSysLog(strTemp);
						}
						break;
					}
				}
				if(bFindCh)	continue;
				
				lpChannel = GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
				if(lpChannel)
				{
					//이미 Stop명령이 하달된 상태이므로 선택된 Cahnnel은 Stop 상태 이어야 한다.
					//Run 중인 Channel은 다른 Channel
					if((lpChannel->GetState() == PS_STATE_RUN || lpChannel->GetState() == PS_STATE_PAUSE) && lpChannel->GetUseOvenMode())
					{
						bCanStop = FALSE;	
						TRACE("bCanStop Set False\n");
						
					}
				}
			}
			
			//		strTemp.Format("Can Stop : %d", bCanStop);
			//		WriteSysLog(strTemp);
			TRACE("bCanStop : %d\n",  bCanStop);
			if(bCanStop && m_ChamberCtrl.m_ctrlOven2.GetRun(awOvenArray.GetAt(a)) == TRUE)
			{
				//strTemp.Format("챔버%d 는 작업중인 Channel이 없습니다. 챔버 동작을 멈추시겠습니까?", awOvenArray.GetAt(a)+2);
				strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOvenStopStateAndSendStopCmd_msg3","CTSMonPro_DOC"), awOvenArray.GetAt(a)+2);//&&
				if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDYES)
				{
					//((CMainFrame *)AfxGetMainWnd())->SendOvenStopCmd(awOvenArray.GetAt(a));
					if(m_ChamberCtrl.m_ctrlOven2.SendStopCmd(0,awOvenArray.GetAt(a)))    // 20090527 KHM 챔버중단?? 동작??
					{//$1013
						//strTemp.Format("챔버 %d 동작 중단 명령 전송", awOvenArray.GetAt(a)+1);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOvenStopStateAndSendStopCmd_msg4","CTSMonPro_DOC"), awOvenArray.GetAt(a)+1);//&&
					}
					else
					{
						//strTemp.Format("챔버 %d 동작 중단 명령 실패!!!", awOvenArray.GetAt(a)+1);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOvenStopStateAndSendStopCmd_msg5","CTSMonPro_DOC"), awOvenArray.GetAt(a)+1);//&&
					}
					WriteSysLog(strTemp);
				}
			}
		}
	}

}

void CCTSMonProDoc::OvenStartStateAndSendStartCmd(int nOvenPort,int nStartOption, UINT nPatternNum, float fTemp, int nOvenId)
{
	//TRACE("Chamber Start : StartOption(%d),PatternNum(%d),설정온도(%.3f) \n",nStartOption,nPatternNum,fTemp);
	TRACE(Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg1","CTSMonPro_DOC"),nStartOption,nPatternNum,fTemp);//&&
	COvenCtrl *pCtrlOven;

	//20131030 add ///////////////
	if (nOvenPort == 1)
	{
		pCtrlOven = &(m_ChamberCtrl.m_ctrlOven1);
		m_iOvenStartOption_1 = nStartOption;
		m_fOvenStartFixTemp_1 = fTemp;
		m_uiOvenPatternNum_1 = nPatternNum;
	}
	else if (nOvenPort == 2)
	{
		pCtrlOven = &(m_ChamberCtrl.m_ctrlOven2);
		m_iOvenStartOption_2 = nStartOption;
		m_fOvenStartFixTemp_2 = fTemp;
		m_uiOvenPatternNum_2 = nPatternNum;
	} 
	else
		pCtrlOven = NULL;

	if (pCtrlOven == NULL) return;

	CString strTemp;
	if(pCtrlOven->GetRun(nOvenId))	//챔버 동작 중
	{
		if(pCtrlOven->SendStopCmd(0,nOvenId) == FALSE)
		{
			//strTemp = "챔버 STOP 명령 실패!!!";
			strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg2","CTSMonPro_DOC");//&&
			WriteSysLog(strTemp);
			return;
		}
	}
	
	//ljb 수정 해야 함
	if (nStartOption == 1) //Fix Mode
	{
		//1.FIX 모드로 전환
		if (pCtrlOven->SetFixMode(0,nOvenId,TRUE) == FALSE)
		{
			//strTemp = "챔버 FIX 모드로 변환 실패!!!";//$1013
			strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg3","CTSMonPro_DOC");//$1013//&&
			WriteSysLog(strTemp);
		}
		if(pCtrlOven->SetTemperature(0,nOvenId, fTemp))
		{//$1013
			//strTemp.Format("Start 챔버 온도 설정 변경 =>%.2f℃ 전송 성공", fTemp);
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg4","CTSMonPro_DOC"), fTemp);//&&
			WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			Sleep(100);
			
			float fFixModeHumi = 0;
			fFixModeHumi = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenFixModeHumidity", 0.0); //lyj 20210812 FIXMODE 연동 시 습도 0으로 설정 109호 류정훈 선임 요청

			//pCtrlOven->SetHumidity(0,nOvenId, 10.0);		//ljb 습도 보이게 입력 
			pCtrlOven->SetHumidity(0,nOvenId, fFixModeHumi); //lyj 20210812 FIXMODE 연동 시 습도 0으로 설정 109호 류정훈 선임 요청
			Sleep(100);
			//2.챔버 RUN
			if (pCtrlOven->SendRunCmd(0,nOvenId) == FALSE)
			{//$1013
				//strTemp = "챔버 동작 실행 명령 실패!!!";
				strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg5","CTSMonPro_DOC");//&&
				WriteSysLog(strTemp);
			}
		}
		else
		{//$1013
			//strTemp.Format("Start 챔버 온도 설정 변경 =>%.2f℃ 전송 실패!!!", fTemp);
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg6","CTSMonPro_DOC"), fTemp);//&&
			WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
		}
		return;
	}
	else if (nStartOption == 2) //Prog Mode
	{
		//1. Prog Mode 변환
		if (pCtrlOven->SetFixMode(0,nOvenId,FALSE) == FALSE)
		{//$1013
			//strTemp = "챔버 PROG 모드로 변환 실패(REST)!!!";
			strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg7","CTSMonPro_DOC");//&&
			WriteSysLog(strTemp);
		}
		//2. Pattern 번호 설정
		if (pCtrlOven->SetRunPatternNo(0,nOvenId,nPatternNum))	//시작 패턴 번호 변경
		{//$1013
			//strTemp.Format("챔버 Pattern 번호 변경 => Pattern Num : %d 전송 성공", nPatternNum);
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg8","CTSMonPro_DOC"), nPatternNum);//&&
			WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			
			Sleep(100);
			//3. 챔버 Run
			if (pCtrlOven->SendRunCmd(0,nOvenId) == FALSE)
			{//$1013
				//strTemp = "챔버 동작 실행 명령 실패(REST)!!!";
				strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg9","CTSMonPro_DOC");//&&
				WriteSysLog(strTemp);
			}
		}
		else
		{//$1013
			//strTemp.Format("챔버 Pattern 번호 변경 => Pattern Num : %d 전송 실패!!!", nPatternNum);
			strTemp.Format(Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg10","CTSMonPro_DOC"), nPatternNum);//&&
		}
		return;
	}
	else if (nStartOption == 3) //Schedule 연동
	{
		if(pCtrlOven->GetOvenModelType() != CHILLER_TEMP2520)
		{
			//pCtrlOven->SetRunPatternNo(0,nOvenId,99);
			//pCtrlOven->SetStartProgMode(0,nOvenId,99);
		}


		Sleep(100);//yulee 20190207
		//1. Fix Mode 변환
		if (pCtrlOven->SetFixMode(0,nOvenId,TRUE) == FALSE) //yulee 20190830_1
		{
			//strTemp = "챔버 FIX 모드로 변환 실패!!!";//$1013
			strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg11","CTSMonPro_DOC");//$1013//&&
			WriteSysLog(strTemp);
		}
		//yulee 20190207
		Sleep(100);
		
		if(pCtrlOven->SendRunCmd(0,nOvenId))
		{
			//strTemp = "챔버 시작 명령 성공>> ";//$1013
			strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg12","CTSMonPro_DOC");//$1013//&&
		}
		else
		{
			//strTemp = "챔버 시작 명령 실패!!!";//$1013
			strTemp = Fun_FindMsg("CTSMonProDoc_OvenStartStateAndSendStartCmd_msg13","CTSMonPro_DOC");//$1013//&&
		}
		WriteSysLog(strTemp);
	}
	else if (nStartOption == 4)		//챔버연동 안함
	{
		return;
	}
}

void CCTSMonProDoc::OvenCheckOption(int nOvenPort, UINT nCheckTime,float fDeltaTemp)
{
	if (nOvenPort == 1)
	{
		m_uiOvenCheckTime_1 = nCheckTime;		//ljb 201012		온도 비교 시간 1초 = 100
		m_fOvenDeltaTemp_1 = fDeltaTemp;		//ljb 201012		허용 온도 비교차
	}
	else
	{
		m_uiOvenCheckTime_2 = nCheckTime;		//ljb 201012		온도 비교 시간 1초 = 100
		m_fOvenDeltaTemp_2 = fDeltaTemp;		//ljb 201012		허용 온도 비교차
	}
}

void CCTSMonProDoc::OvenCheckFixOption(int nOvenPort, UINT nCheckTime,float fDeltaTemp)
{
	if (nOvenPort == 1)
	{
		m_uiOvenCheckFixTime_1 = nCheckTime;		//ljb 201101		온도 비교 시간 1초 = 100
		m_fOvenDeltaFixTemp_1 = fDeltaTemp;		//ljb 201101		허용 온도 비교차
	}
	else if (nOvenPort == 2)
	{
		m_uiOvenCheckFixTime_2 = nCheckTime;		//ljb 201101		온도 비교 시간 1초 = 100
		m_fOvenDeltaFixTemp_2 = fDeltaTemp;		//ljb 201101		허용 온도 비교차
	}
}

void CCTSMonProDoc::OvenStopStateAndSendStopCmd(int nOvenPort)
{	
	DWORD dwData;
	CString strTemp;
	BOOL bFlag(TRUE);
	for(int iOvenCnt = 0; iOvenCnt < m_ChamberCtrl.m_ctrlOven1.GetOvenCount() ; iOvenCnt++)
	{
		if (nOvenPort == 1)	bFlag = m_ChamberCtrl.m_ctrlOven1.GetRun(iOvenCnt);
		if (nOvenPort == 2)	bFlag = m_ChamberCtrl.m_ctrlOven2.GetRun(iOvenCnt);

		if(bFlag)
		{
			
			//2014.11.21 챔버 무조건 정지
			if (nOvenPort == 1)	bFlag = m_ChamberCtrl.m_ctrlOven1.SendStopCmd(0,iOvenCnt);
			if (nOvenPort == 2)	bFlag = m_ChamberCtrl.m_ctrlOven2.SendStopCmd(0,iOvenCnt);

			/*
			strTemp = "챔버 동작을 멈추시겠습니까?";
			if(AfxMessageBox(strTemp, MB_ICONQUESTION|MB_YESNO) == IDYES)
			{
				if (nOvenPort == 1)	bFlag = m_ctrlOven1.SendStopCmd(0,iOvenCnt);
				if (nOvenPort == 2)	bFlag = m_ctrlOven2.SendStopCmd(0,iOvenCnt);
				if( bFlag)   // 20090527 KHM 챔버중단?? 동작??
				{
					strTemp = "챔버 동작 중단 명령 전송 성공";
				}
				else
				{
					strTemp = "챔버 동작 중단 명령 실패!!!";
				}
				WriteSysLog(strTemp);
			}
			*/
		}
	}
}

//ksj 20210727 : 폴더 복사 함수 추가
int CCTSMonProDoc::CopyDirectory(CString szFrom, CString szTo)
{
	HANDLE hSrch;
	WIN32_FIND_DATA wfd;
	BOOL bResult = TRUE;
	TCHAR WildCard[MAX_PATH];
	TCHAR SrcFile[MAX_PATH];
	TCHAR DestFile[MAX_PATH];
	wsprintf(WildCard, "%s\\*.*", szFrom);
	CreateDirectory(szTo, NULL);
	hSrch = FindFirstFile(WildCard, &wfd);
	if (hSrch == INVALID_HANDLE_VALUE)
		return FALSE; 

	while (bResult)
	{
		wsprintf(SrcFile, "%s\\%s", szFrom, wfd.cFileName);
		wsprintf(DestFile, "%s\\%s", szTo, wfd.cFileName); // 서브 디렉토리가 발견되면 서브 디렉토리를 복사한다.
		if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{ 
			if (lstrcmp(wfd.cFileName, ".") && lstrcmp(wfd.cFileName, ".."))
			{
				CopyDirectory(SrcFile, DestFile);
			}
		}
		else
		{
			CopyFile(SrcFile, DestFile, FALSE); 
		}
		bResult = FindNextFile(hSrch, &wfd);
	}
	FindClose(hSrch);
	return TRUE;
}

BOOL CCTSMonProDoc::ForceDirectory(LPCTSTR lpDirectory)
{
	CString szDirectory = lpDirectory;
	szDirectory.TrimRight("\\");
	
	if ((GetFilePath(szDirectory) == szDirectory) ||
		(FileExists(szDirectory) == -1))
		return TRUE;
	if (!ForceDirectory(GetFilePath(szDirectory)))
		return FALSE;
	if (!CreateDirectory(szDirectory, NULL))
		return FALSE;
	return TRUE;
	
}
int CCTSMonProDoc::FileExists(LPCTSTR lpszName)
{
	CFileFind fileFind;
	if (!fileFind.FindFile(lpszName))
	{
		if (DirectoryExists(lpszName))  // if root ex. "C:\"
			return -1;
		return 0;
	}
	fileFind.FindNextFile();
	return fileFind.IsDirectory() ? -1 : 1;
}
CString CCTSMonProDoc::GetFilePath(LPCTSTR lpszFilePath)
{
	TCHAR szDir[_MAX_DIR];
	TCHAR szDrive[_MAX_DRIVE];
	_tsplitpath(lpszFilePath, szDrive, szDir, NULL, NULL);
	
	return  CString(szDrive) + CString(szDir);
}
BOOL CCTSMonProDoc::DirectoryExists(LPCTSTR lpszDir)
{
	char curPath[_MAX_PATH];   /* Get the current working directory: */
	if (!_getcwd(curPath, _MAX_PATH))
		return FALSE;
	if (_chdir(lpszDir))	// retruns 0 if error
		return FALSE;
	_chdir(curPath);
	return TRUE;
}
//file find
BOOL CCTSMonProDoc::file_Finder(CString str_path)
{
	BOOL bWork;
	CFileFind finder;
    bWork=finder.FindFile(str_path);		
	
	finder.Close();
	
    return bWork;
}

BOOL CCTSMonProDoc::Fun_AllPause()
{
	CCyclerModule *pMD;
	CCyclerChannel *pCH;
	CWordArray chArray;
	CString strMsg;
	
	CWaitCursor wait;
	CDWordArray arTargetCh;
	
	//전체 모듈에 대해 챔버연동 대기 채널 검색
	for(int mdIndex =1; mdIndex <= GetInstallModuleCount(); mdIndex++)
	{
		pMD = GetModuleInfo(mdIndex);
		if(pMD)
		{
			//Line Off
			if(pMD->GetState() == PS_STATE_LINE_OFF) continue;
			
			//모든 채널에 대해 검색
			for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
			{
				pCH = pMD->GetChannelInfo(ch);
				if(pCH == NULL) continue;
				
				if((pCH->IsParallel() && pCH->m_bMaster == FALSE) )		return FALSE;
				WORD state = pCH->GetState();
				if(state == PS_STATE_STANDBY || state == PS_STATE_READY || state == PS_STATE_END || state == PS_STATE_IDLE)
					chArray.Add(ch);;
					//준 Stop을 지원하기 위해 
				if(state == PS_STATE_PAUSE)
					chArray.Add(ch);
			}
			//if (SendCommand(mdIndex, &chArray, SFT_CMD_PAUSE) == FALSE) return FALSE;
			if (SendCommand(mdIndex, &chArray, SFT_CMD_STOP) == FALSE) return FALSE;
		}
	}
	return TRUE;
}

void CCTSMonProDoc::Fun_AddLossModuleData(UINT nModule, UINT nChannel)
{
// 	if(m_pRestoreDlg == NULL)
// 	{
// 		m_pRestoreDlg = new CRestoreDlg(this);
// 		m_pRestoreDlg->LossDataAddModuleAndChannel(nModule,nChannel);
// 	}
// 	else
// 	{
// 		m_pRestoreDlg->LossDataAddModuleAndChannel(nModule,nChannel);
// 	}

}

BOOL CCTSMonProDoc::Fun_ReloadDivisionCode()
{
	//	CString strTempMDB;
	// 	strTempMDB = AfxGetApp()->GetProfileString(EDITOR_REG_SECTION ,"DataBase");	//Get Current Folder(CTSMon Folde)
	// 	strTempMDB = strTempMDB+ "\\" + "Pack_Schedule_2000.mdb";
	
	m_uiArryCanDivition.RemoveAll();	//ljb 2011218 이재복 //////////
	m_strArryCanName.RemoveAll();		//ljb 2011218 이재복 //////////

	m_uiArryAuxDivition.RemoveAll();	//ljb 2011218 이재복 //////////
	m_strArryAuxName.RemoveAll();		//ljb 2011218 이재복 //////////
	
	m_uiArryAuxThrDivition.RemoveAll();		//ljb 20160504 이재복 //////////
	m_strArryAuxThrName.RemoveAll();		//ljb 20160504 이재복 //////////

	m_uiArryAuxHumiDivition.RemoveAll();		//ksj 20200207
	m_strArryAuxHumiName.RemoveAll();		//ksj 20200207

// 	m_strCurPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path", "");
// 	if(m_strCurPath.IsEmpty())	
// 	{
// 		AfxMessageBox("프로그램 설치 정보를 찾을 수 없습니다.");
// 		return FALSE;
// 	}
// 	
// 	//DataBase 폴더 생성
// 	m_strDataBaseName = m_strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME;
	
	CDaoDatabase  db;
	try
	{
		//db.Open(::GetDataBaseName());
		db.Open(m_strDataBaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL,strInsertSQL;
	CString strCodeName,strDesc;
	UINT uiCode, uiDivision;
	strSQL.Format("SELECT * FROM DivisionCode order by Code");	//ljb 20170929 edit
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	
	// 	CRowColArray ra;
	// 	int nRow = 1;
	CString strTemp;
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue("Code");
			uiCode = data.lVal;
			//strTemp.Format("%d",iCode);
			
			data = rs.GetFieldValue("Code_Name");
			if(VT_NULL != data.vt) strCodeName.Format("%s", data.pbVal);
			else strCodeName.Empty();
			
			data = rs.GetFieldValue("Division");
			uiDivision = data.lVal;
			
			data = rs.GetFieldValue("Description");
			if(VT_NULL != data.vt) strDesc.Format("%s", data.pbVal);
			else strDesc.Empty();
			
			if (uiDivision == ID_CAN_TYPE)			//can
			{
				m_uiArryCanDivition.Add(uiCode);
				m_strArryCanName.Add(strCodeName);
			}
			else if  (uiDivision == ID_AUX_TYPE)	//aux
			{
				m_uiArryAuxDivition.Add(uiCode);
				m_strArryAuxName.Add(strCodeName);
			}
			else if  (uiDivision == ID_AUX_THERMISTOR_TYPE)	//aux thermistor
			{
				m_uiArryAuxThrDivition.Add(uiCode);
				m_strArryAuxThrName.Add(strCodeName);
			}
			else if  (uiDivision == ID_AUX_HUMIDITY_TYPE)	//ksj 20200207 : v1016 : 습도 센서 추가
			{
				m_uiArryAuxHumiDivition.Add(uiCode);
				m_strArryAuxHumiName.Add(strCodeName);
			}
			rs.MoveNext();
		}	
	}
	rs.Close();
	db.Close();
	return TRUE;
}

void CCTSMonProDoc::Fun_GetArryCanDivision(CUIntArray & arryCanDivision)
{
	arryCanDivision.Copy(m_uiArryCanDivition);
}
void CCTSMonProDoc::Fun_GetArryCanStringName(CStringArray & arryCanStringName)
{
	arryCanStringName.Copy(m_strArryCanName);
}
void CCTSMonProDoc::Fun_GetArryAuxDivision(CUIntArray & arryAuxDivision)
{
	arryAuxDivision.Copy(m_uiArryAuxDivition);
}
void CCTSMonProDoc::Fun_GetArryAuxStringName(CStringArray & arryAuxStringName)
{
	arryAuxStringName.Copy(m_strArryAuxName);
}
void CCTSMonProDoc::Fun_GetArryAuxThrDivision(CUIntArray & arryAuxThrDivision)
{
	arryAuxThrDivision.Copy(m_uiArryAuxThrDivition);
}
void CCTSMonProDoc::Fun_GetArryAuxThrStringName(CStringArray & arryAuxThrStringName)
{
	arryAuxThrStringName.Copy(m_strArryAuxThrName);
}
//ksj 20200207
void CCTSMonProDoc::Fun_GetArryAuxHumiDivision(CUIntArray & arryAuxThrDivision)
{
	arryAuxThrDivision.Copy(m_uiArryAuxHumiDivition);
}
//ksj 20200207
void CCTSMonProDoc::Fun_GetArryAuxHumiStringName(CStringArray & arryAuxThrStringName)
{
	arryAuxThrStringName.Copy(m_strArryAuxHumiName);
}

//20110319 KHS Monitoring Data Text Save
void CCTSMonProDoc::UpdateTextMonitoringFile()
{
	CCyclerModule *pMD; 
	CCyclerChannel *pCh;
	
	for(int m=0; m<GetInstallModuleCount();m++)
	{
		pMD = GetModuleInfo(GetModuleID(m));
		if(pMD)
		{
			for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
			{
				pCh = pMD->GetChannelInfo(ch);
				if(pCh)
				{
					if(pCh->GetTestPath().IsEmpty() == FALSE)					
					{
						//lyj 20200821 s 완료 후 해당 파일 업데이트 안함 
// 						CString strTemp;
// 						strTemp.Format("%s", ::PSGetTypeMsg(pCh->GetStepType()));
// 						if(strTemp != "작업중")
						if (pCh->GetStepType() == PS_STEP_CHARGE || pCh->GetStepType() == PS_STEP_DISCHARGE ||
							pCh->GetStepType() == PS_STEP_PATTERN || pCh->GetStepType() == PS_STEP_IMPEDANCE ||
							pCh->GetStepType() == PS_STEP_OCV || pCh->GetStepType() == PS_STEP_REST || 
							pCh->GetStepType() == PS_STEP_USER_MAP || pCh->GetStepType() == PS_STEP_EXT_CAN)
						//lyj 20200821 e 완료 후 해당 파일 업데이트 안함 
						{
							pCh->UpdateModuleSpec();
							//20120210 KHS
							if(pCh->m_bDataLoss == TRUE)	pCh->UpdateChannelLossState();
							else							pCh->UpdateChannelState();
						}
					}
				}
			}
		}
	}
}

//ljb 20130124 Pattern File 있는지 체크
BOOL CCTSMonProDoc::Fun_CheckPatternFile(CString strFile)
{
	//파일 유뮤 검색
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(strFile) == FALSE )
	{
		return FALSE;
	}
	aFileFinder.Close();
	
	return TRUE;
}

BOOL CCTSMonProDoc::SendScheduleToModuleTest()
{
	CString strFile,strMsg;
	STF_MD_USERPATTERN_DATA patternData;
	ZeroMemory(&patternData, sizeof(STF_MD_USERPATTERN_DATA));

	for (int i=1; i<13; i++)
	{
		strFile.Format("c:\\pattern_%d.csv",i);
		if(!LoadPatternDataFromFile(strFile, patternData ))	//파일을 읽어온다.
		{
			//strMsg.Format("%s 에 Pattern 데이터 불러오기 실패!!!", GetModuleName(nModuleID));
			//WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			delete patternData.pPatternData;
			continue;
		}
		
		//20130225				if (patternData.lLength > )
		TRACE("\n SFT_STEP_SIMUL_CONDITION Size = %d",sizeof(SFT_STEP_SIMUL_CONDITION));
		TRACE("\n Pattern Data Size = %d \n\n",patternData.lLength);
	}

	return TRUE;
	
}

BOOL CCTSMonProDoc::Fun_FtpConnect(CString strIP)
{
	int responseCode=0;
	char cmdOpen[250] = "";
	char cmdLogin[250] = "";
	char cmdLCD[200] = "";
	char cmdCD[200] = "";
	CString m_workingDir;
	CString strOpen;
	strOpen.Format("open %s",strIP);
	
	ZeroMemory(cmdOpen,sizeof(cmdOpen));
	ZeroMemory(cmdLogin,sizeof(cmdLogin));
	ZeroMemory(cmdCD,sizeof(cmdCD));
	strcpy(	cmdOpen, "open 192.168.5.71" );
	strcpy(	cmdLogin, "user root:dusrnth" );
	strcpy(	cmdCD, "cd /root" );
	m_workingDir = "/root";
	
	
	theFtpConnection = new ftp;
	//responseCode=theFtpConnection->DoOpen(cmdOpen);
	responseCode=theFtpConnection->DoOpen((LPSTR)(LPCSTR)strOpen);
	if(responseCode==INVALID_SOCKET)
	{
		//AfxMessageBox("unable to find server");
		delete theFtpConnection;
		theFtpConnection=NULL; //ksj 20200707
		return FALSE;
	}
	if(responseCode!=220)
	{
		//AfxMessageBox("unable to connect to server");
		theFtpConnection->DoClose();
		delete theFtpConnection;
		theFtpConnection=NULL; //ksj 20200707
		return FALSE;
	}
	responseCode=theFtpConnection->DoLogin(cmdLogin);
	if(responseCode!=230)
	{
		//AfxMessageBox("UserName/Password not correct");
		theFtpConnection->DoClose();
		delete theFtpConnection;
		theFtpConnection=NULL; //ksj 20200707
		return FALSE;
	}
	responseCode= theFtpConnection->DoCD(cmdCD);
	if(responseCode!=250)
	{
		//AfxMessageBox("can't change Directory");
		theFtpConnection->DoClose();
		delete theFtpConnection;
		theFtpConnection=NULL; //ksj 20200707
		return FALSE;
	}
	
	theFtpConnection->DoBinary();
	return TRUE;
}

void CCTSMonProDoc::Fun_FtpDisConnect()
{
	if(!theFtpConnection) return; //ksj 20180308 : NULL 포인터 처리 추가.
	theFtpConnection->DoClose();
	delete theFtpConnection;
	theFtpConnection = NULL;
}

BOOL CCTSMonProDoc::Fun_FtpChangeDir(CString strPath)
{
	if (theFtpConnection == NULL) return FALSE;
	int responseCode=0;
	CString strMovePath,strMakePath;
	CString strFullPath,strTemp;
	int nFind;

	strMovePath = "cd /root" + strPath;		//   /schedule/ch001
	responseCode=theFtpConnection->DoCD((LPSTR)(LPCSTR)strMovePath);
	if(responseCode!=250)
	{
		//1. /root로 이동
		strMovePath = "cd /root";
		responseCode=theFtpConnection->DoCD((LPSTR)(LPCSTR)strMovePath);
		if(responseCode!=250)
		{
			//AfxMessageBox("can't change Directory");
			//theFtpConnection->DoPWD();		
			return FALSE;
		}

		if (strPath.Left(1) == "/") strPath = strPath.Right(strPath.GetLength()-1);		//  왼쪽 / 제거
		while(strPath.Find('/') > -1)
		{
			nFind = strPath.Find('/');
			strMakePath = strPath.Left(nFind);
			strMovePath = strMovePath + "/" + strMakePath;
			strPath = strPath.Right(strPath.GetLength() - (nFind + 1));

			//2. 폴더 있는지 확인 하고 이동
			responseCode=theFtpConnection->DoCD((LPSTR)(LPCSTR)strMovePath);
			if(responseCode!=250)
			{
				//3. 폴더 생성
				responseCode= theFtpConnection->DoMakeDir((LPSTR)(LPCSTR)strMakePath);
				if(responseCode!=257)
				{
					//AfxMessageBox("can't create Directory");
					AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_FtpChangeDir_msg1","CTSMonPro_DOC"));//&&
					return FALSE;
				}
			}
			//4. 폴더 있는지 확인 하고 이동
			responseCode=theFtpConnection->DoCD((LPSTR)(LPCSTR)strMovePath);
			if(responseCode!=250)
			{
				//AfxMessageBox("can't change Directory");
				//theFtpConnection->DoPWD();		
				return FALSE;
			}
		}

		//4. 마지막 폴더 생성
		strMakePath = strPath;
		responseCode= theFtpConnection->DoMakeDir((LPSTR)(LPCSTR)strMakePath);
		if(responseCode!=257)
		{
			//AfxMessageBox("can't create Directory");
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_FtpChangeDir_msg1","CTSMonPro_DOC"));//&&
			return FALSE;
		}
		//5. 마지막 폴더로 이동
		strMovePath = strMovePath + "/" + strMakePath;
		responseCode=theFtpConnection->DoCD((LPSTR)(LPCSTR)strMovePath);
		if(responseCode!=250)
		{
			//AfxMessageBox("can't change Directory");
			//theFtpConnection->DoPWD();		
			return FALSE;
		}	
	}

	return TRUE;
}

BOOL CCTSMonProDoc::Fun_FtpFileList()
{
	if (theFtpConnection == NULL) return FALSE;
	char *line;
	FILE *fptr=NULL;
	char seps[]   = "|";
	char *token;
	int len;
	char *permission,*lnk,*owner,*grp,*size,*date1,*date2,*date3,*name;

	CString sText,strFileName;

	theFtpConnection->DoList("ls -l");
	if( (fptr = fopen( "data.txt", "r" )) == NULL )
	{
		printf( "The file 'data2' was not opened\n" );
		return FALSE;
	}
	char *datef=(char *)calloc(51, sizeof( char ));
	line=(char *)calloc(512, sizeof( char ));
	
	while( fgets(line, 512, fptr) != NULL ) {
	if(*line !='t' )
	{
		token = strtok( line, seps );
		if(token)
			permission=token;

		token = strtok( NULL, seps );
		if(token)
			lnk=token;

		token = strtok( NULL, seps );
		if(token)
			owner=token;

		token = strtok( NULL, seps );
		if(token)
			grp=token;

		token = strtok( NULL, seps );
		if(token)
			size=token;

		token = strtok( NULL, seps );
		if(token)
			date1=token;

		token = strtok( NULL, seps );
		if(token)
			date2=token;

		token = strtok( NULL, seps );
		if(token)
			date3=token;
			   
		token = strtok( NULL, seps );
		if(token) //to handle the case of ls -l in which 
		{         //username and grp get merge if username is big.
			name=token;
		}
		else
		{
			name=date3;
			date3=date2;
			date2=date1;
			date1=size;
			size=grp;
		}
		 strcpy( datef, date1 );
		   strcat( datef, " ");
		   strcat( datef, date2 );
		   strcat( datef, " ");
		   strcat( datef, date3 );
		
			len=strlen(name);
			name[len-1]=0;
		// Add Directories
		if(*line=='d' )
		{
			if(*name != '.' && name != "..")
			{
// 				m_Listing.SetFolderType(1);
// 				(void)m_Listing.AddItem( name,size,datef,permission);
				strFileName.Format("Folder : %s",name);
				AfxMessageBox(strFileName);

 			}
		}
		else
		{
// 			if(name != "." && name != "..")
// 			{
// 				m_Listing.SetFolderType(0);
// 				(void)m_Listing.AddItem( name,size,datef,permission);
// 			}	
			strFileName.Format("File : %s",name);
			AfxMessageBox(strFileName);
		}
	 }
     memset(line,'\0',80); 
	}
	fclose(fptr);  /* Close the file */
	free(datef);
	free(line);
	_unlink("data.txt");

	return TRUE;
}

BOOL CCTSMonProDoc::Fun_FtpFileFind(CString strFindFile)
{
	if (theFtpConnection == NULL) return FALSE;
	char *line;
	FILE *fptr=NULL;
	char seps[]   = "|";
	char *token;
	int len;
	char *permission,*lnk,*owner,*grp,*size,*date1,*date2,*date3,*name;

	CString sText,strFileName;

	theFtpConnection->DoList("ls -l");
	if( (fptr = fopen( "data.txt", "r" )) == NULL )
	{
		printf( "The file 'data2' was not opened\n" );
		return FALSE;
	}
	char *datef=(char *)calloc(51, sizeof( char ));
	line=(char *)calloc(512, sizeof( char ));
	
	while( fgets(line, 512, fptr) != NULL ) {
	if(*line !='t' )
	{
		token = strtok( line, seps );
		if(token)
			permission=token;

		token = strtok( NULL, seps );
		if(token)
			lnk=token;

		token = strtok( NULL, seps );
		if(token)
			owner=token;

		token = strtok( NULL, seps );
		if(token)
			grp=token;

		token = strtok( NULL, seps );
		if(token)
			size=token;

		token = strtok( NULL, seps );
		if(token)
			date1=token;

		token = strtok( NULL, seps );
		if(token)
			date2=token;

		token = strtok( NULL, seps );
		if(token)
			date3=token;
			   
		token = strtok( NULL, seps );
		if(token) //to handle the case of ls -l in which 
		{         //username and grp get merge if username is big.
			name=token;
		}
		else
		{
			name=date3;
			date3=date2;
			date2=date1;
			date1=size;
			size=grp;
		}
		 strcpy( datef, date1 );
		   strcat( datef, " ");
		   strcat( datef, date2 );
		   strcat( datef, " ");
		   strcat( datef, date3 );
		
			len=strlen(name);
			name[len-1]=0;
		// Add Directories
		if(*line=='d' )
		{
			if(*name != '.' && name != "..")
			{
// 				m_Listing.SetFolderType(1);
// 				(void)m_Listing.AddItem( name,size,datef,permission);
//				strFileName.Format("%s",name);
//				AfxMessageBox(strFileName);

 			}
		}
		else
		{
// 			if(name != "." && name != "..")
// 			{
// 				m_Listing.SetFolderType(0);
// 				(void)m_Listing.AddItem( name,size,datef,permission);
// 			}	
			strFileName.Format("%s",name);
			if (strFindFile.Compare(strFileName) == 0)
			{
				//AfxMessageBox(strFileName);
				return TRUE;
			}
		}
	 }
     memset(line,'\0',80); 
	}
	fclose(fptr);  /* Close the file */
	free(datef);
	free(line);
	_unlink("data.txt");

	return FALSE;
}

BOOL CCTSMonProDoc::Fun_FtpFileDeleteAll(CString strPath)
{
	if (theFtpConnection == NULL) return FALSE;
	CString strMovePath;
	int responseCode;

	strMovePath = "cd /root" + strPath;		//   /schedule/ch001
	responseCode=theFtpConnection->DoCD((LPSTR)(LPCSTR)strMovePath);
	if(responseCode!=250)
	{
		//AfxMessageBox("지정한 폴더가 없습니다.");
		return FALSE;
	}

	CStringArray arryStrFileName;
	arryStrFileName.RemoveAll();

	char *line;
	FILE *fptr=NULL;
	char seps[]   = "|";
	char *token;
	int len;
	char *permission,*lnk,*owner,*grp,*size,*date1,*date2,*date3,*name;

	CString sText,strFileName;

	theFtpConnection->DoList("ls -l");
	if( (fptr = fopen( "data.txt", "r" )) == NULL )
	{
		printf( "The file 'data2' was not opened\n" );
		return FALSE;
	}
	char *datef=(char *)calloc(51, sizeof( char ));
	line=(char *)calloc(512, sizeof( char ));
	
	while( fgets(line, 512, fptr) != NULL ) {
	if(*line !='t' )
	{
		token = strtok( line, seps );
		if(token)
			permission=token;

		token = strtok( NULL, seps );
		if(token)
			lnk=token;

		token = strtok( NULL, seps );
		if(token)
			owner=token;

		token = strtok( NULL, seps );
		if(token)
			grp=token;

		token = strtok( NULL, seps );
		if(token)
			size=token;

		token = strtok( NULL, seps );
		if(token)
			date1=token;

		token = strtok( NULL, seps );
		if(token)
			date2=token;

		token = strtok( NULL, seps );
		if(token)
			date3=token;
			   
		token = strtok( NULL, seps );
		if(token) //to handle the case of ls -l in which 
		{         //username and grp get merge if username is big.
			name=token;
		}
		else
		{
			name=date3;
			date3=date2;
			date2=date1;
			date1=size;
			size=grp;
		}
		 strcpy( datef, date1 );
		   strcat( datef, " ");
		   strcat( datef, date2 );
		   strcat( datef, " ");
		   strcat( datef, date3 );
		
			len=strlen(name);
			name[len-1]=0;
		// Add Directories
		if(*line=='d' )
		{
			if(*name != '.' && name != "..")
			{
// 				m_Listing.SetFolderType(1);
// 				(void)m_Listing.AddItem( name,size,datef,permission);
//				strFileName.Format("%s",name);
//				AfxMessageBox(strFileName);

 			}
		}
		else
		{
// 			if(name != "." && name != "..")
// 			{
// 				m_Listing.SetFolderType(0);
// 				(void)m_Listing.AddItem( name,size,datef,permission);
// 			}	
			strFileName.Format("%s",name);
			arryStrFileName.Add(strFileName);
		}
	 }
     memset(line,'\0',80); 
	}
	fclose(fptr);  /* Close the file */
	free(datef);
	free(line);
	_unlink("data.txt");

	if (arryStrFileName.GetSize() > 0)
	{
		for (int i=0; i<arryStrFileName.GetSize(); i++)
		{
			strFileName = arryStrFileName.GetAt(i);
			theFtpConnection->DoDelete( (LPSTR)(LPCSTR)strFileName);
		}
	}
	
	return TRUE;
}

BOOL CCTSMonProDoc::Fun_FtpPutFile(CString strFullPath)
{
	if (theFtpConnection == NULL) return FALSE;
	CString strPath;
	CString strFileName;
	int nPos;


	nPos = strFullPath.ReverseFind('\\');
	if (nPos < 0) return FALSE;

	strPath = strFullPath.Left(nPos);
	strFileName = strFullPath.Right(strFullPath.GetLength() - (nPos+1));

	int iResult = _chdir ((LPSTR)(LPCSTR)strPath);	//PC Path 설정
	int rectcode= theFtpConnection->DoPut((LPSTR)(LPCSTR)strFileName);

	if(rectcode !=226)	{
		//AfxMessageBox("Don't have sufficient permissions");
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_FtpPutFile_msg1","CTSMonPro_DOC"));//&&
		//delete theFtpConnection;
		return FALSE;
	}

//			2013-09-11			bung			:			pc path
	strPath.Format("C:\\");
	iResult = _chdir ((LPCTSTR)strPath);	//PC Path 설정

	return TRUE;
}
void CCTSMonProDoc::Fun_SaveFileSBCbinary(CString strFile)
{
	
	FILE *fp;
    unsigned char *buffer = NULL;
	unsigned char cTemp;
	long lTemp=0;
    long FileSize = 0;
//	lFileSize = lCheckSum = 0;
	
	fp = fopen( strFile, "rb" );		
	if( !fp )
	{
		throw "File Not Found!";		
		return ;
	}
	
	FileSize = filelength( fileno(fp) );		
	
	// 	fseek(fp,0,SEEK_END);
	// 	lFileSize = FileSize = ftell(fp);
	
	buffer = new unsigned char [FileSize];//lmh 20120612 해제해야함		
	fread( buffer, FileSize, 1, fp );		
	fclose( fp );
	
	//if(strlen(buffer) < 1) 
	if(FileSize < 1) 
	{
		delete buffer;
		buffer = NULL;
		return ;
	}
// 	for (long lcnt=0; lcnt < FileSize; lcnt++)
// 	{
// 		cTemp = buffer[lcnt];
// 		lCheckSum += cTemp;			//checksum
// 	}
// 	
//////////////////////////////////////////////////////////////////////////////////
	CString strSaveFileName,strTemp,strTemp2,strSaveData;
	strSaveFileName = "c:\\sbc_schedule_check_bin.txt";
	strSaveData.Empty();

	//파일 유뮤 검색
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;

	nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	if ( aFileFinder.FindFile(strSaveFileName) == FALSE )
	{
	}
	else
	{
		::DeleteFile(strSaveFileName);	//파일 삭제

// 		nID = CFile::modeWrite | CFile::shareDenyNone;
// 		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(strSaveFileName, nID) == FALSE )
		return;
	
	try 
	{
// 		if( bExistFile )	
// 			aFile.SeekToEnd();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}

	long lcnt;
	for (lcnt=0; lcnt < FileSize; lcnt++)
	{
		strTemp.Format("%02x(%d) ",buffer[lcnt],buffer[lcnt]);
		if ((lcnt % 10) == 0) strSaveData += "\n";
		strSaveData += strTemp;

// 		cTemp = buffer[lcnt];
// 		lCheckSum += cTemp;			//checksum
	}
	aFile.WriteString(strSaveData + "\n\n");

	strSaveData.Empty();
	long lCheckSum=0;
	long lminSum = 0;
	for (lcnt=0; lcnt < FileSize; lcnt++)
	{
		cTemp = buffer[lcnt];
		lCheckSum += (int)cTemp;
		lminSum += (int)cTemp;

		if ((lcnt % 10) == 0)
		{
			strTemp2.Format("sum : %d,total %d \n", lminSum, lCheckSum );
			strSaveData += strTemp2;
			lminSum = 0;
			continue;
		}

		strTemp.Format("%d ",buffer[lcnt],lCheckSum);
		strSaveData += strTemp;
		
		// 		cTemp = buffer[lcnt];
		// 		lCheckSum += cTemp;			//checksum
	}
	aFile.WriteString(strSaveData + "\n\n");

	aFile.Close();

	delete buffer;
	buffer = NULL;

}

//			2013-10-21			bung			:			receive 1040 / send cmd run
void CCTSMonProDoc::Fun_ReadyFromSbc(int nModuleID,int nResponse)
{
	CString strData;
	strData.Format("Receive Module ID : %d (sbc ready for RUN), response : %d (1:OK, 2:NG) ", nModuleID,nResponse);
	WriteLog(strData, CT_LOG_LEVEL_NORMAL);
	
	// 	for(int i= 0; i< MAX_MODUEL_RESEND; i++)	{
	// 		m_pAlarmReadyDlg
	// 	}
	
	switch(nModuleID)	{
	case	1	:	{
		if( m_pAlarmReadyDlg1)	{			// && m_pAlarmReadyDlg1->IsWindowVisible())
			if (nResponse == 1)		{
				if (m_pAlarmReadyDlg1->Fun_RunTest(nModuleID) < 0)	{
					//AfxMessageBox("작업 시작(RUN) 실패");
					AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_ReadyFromSbc_msg1","CTSMonPro_DOC"));//&&					
				}
			}else	{
				//AfxMessageBox("작업 시작(SBC NACK) 실패");
				AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_ReadyFromSbc_msg2","CTSMonPro_DOC"));//&&
			}
			m_pAlarmReadyDlg1->Fun_CloseDlg();
			Sleep(100);
			delete m_pAlarmReadyDlg1;
			m_pAlarmReadyDlg1 = NULL;
		}
					}break;
	case	2	:	{
		if( m_pAlarmReadyDlg2)	{			// && m_pAlarmReadyDlg1->IsWindowVisible())
			if (nResponse == 1)		{
				if (m_pAlarmReadyDlg2->Fun_RunTest(nModuleID) < 0)	{
					//AfxMessageBox("작업 시작(RUN) 실패");
					AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_ReadyFromSbc_msg3","CTSMonPro_DOC"));//&&
				}
			}else	{
				//AfxMessageBox("작업 시작(SBC NACK) 실패");
				AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_ReadyFromSbc_msg4","CTSMonPro_DOC"));//&&
			}
			m_pAlarmReadyDlg2->Fun_CloseDlg();
			Sleep(100);
			delete m_pAlarmReadyDlg2;
			m_pAlarmReadyDlg2 = NULL;
		}
					}break;
	case	3	:	{
		if( m_pAlarmReadyDlg3)	{			// && m_pAlarmReadyDlg1->IsWindowVisible())
			if (nResponse == 1)		{
				if (m_pAlarmReadyDlg3->Fun_RunTest(nModuleID) < 0)	{
					//AfxMessageBox("작업 시작(RUN) 실패");
					AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_ReadyFromSbc_msg5","CTSMonPro_DOC"));//&&
				}
			}else	{
				//AfxMessageBox("작업 시작(SBC NACK) 실패");
				AfxMessageBox(Fun_FindMsg("CTSMonProDoc_Fun_ReadyFromSbc_msg6","CTSMonPro_DOC"));//&&
			}
			m_pAlarmReadyDlg3->Fun_CloseDlg();
			Sleep(100);
			delete m_pAlarmReadyDlg3;
			m_pAlarmReadyDlg1 = NULL;
		}
					}break;
	default:	{
		
				}break;
	}
}

//			2013-09-11			bung			:			sch send fail. modaless dlg close : memory delete
BOOL CCTSMonProDoc::Fun_CheckReadyDlg(int nModuleID)
{
	switch(nModuleID)	{
	case		1		:		{
		if (m_pAlarmReadyDlg1 == NULL)	{
			return FALSE;
		}

		delete m_pAlarmReadyDlg1;

		m_pAlarmReadyDlg1 = NULL;

		
		if (m_pAlarmReadyDlg1 != NULL)		{
			return TRUE;
		}else	{
			return FALSE;
		}
								}break;
	case		2		:		{
		if (m_pAlarmReadyDlg2 == NULL)	{
			return FALSE;
		}
		
		delete m_pAlarmReadyDlg2;
		m_pAlarmReadyDlg2 = NULL;
		
		if (m_pAlarmReadyDlg2 != NULL)		{
			return TRUE;
		}else	{
			return FALSE;
		}
								}break;
	case		3		:		{
		if (m_pAlarmReadyDlg3 == NULL)	{
			return FALSE;
		}
		
		delete m_pAlarmReadyDlg3;
		m_pAlarmReadyDlg3 = NULL;
		
		if (m_pAlarmReadyDlg3 != NULL)		{
			return TRUE;
		}else	{
			return FALSE;
		}
								}break;
	default		:		{
		
						}break;
	}
	return TRUE;
}

void CCTSMonProDoc::Log_write(CString strLogString, int option)
{
	CString	strLog_FileName;
	CString	strLog_FilePath;
	CString	strLog_FolderPath;
	CString strLog;
	CString	loginlogFolder;
	CTime tm = CTime::GetCurrentTime();
	strLog.Format("%s :: %s", tm.Format("%Y/%m/%d %H:%M:%S"), strLogString);	
	
	switch(option)	{
	case	LOG_OPTION_CHAMBER			:	{
		loginlogFolder.Format("1_Chamber");
											}break;
	case	LOG_OPTION_STATE_STEPTYPE	:	{
		loginlogFolder.Format("2_State_steptype");
											}break;
	case	LOG_OPTION_RECEIVE			:	{
		loginlogFolder.Format("3_recevie");
											}break;
	case	LOG_OPTION_SEND				:	{
		loginlogFolder.Format("4_Send");
											}break;
	default		:	{
		loginlogFolder.Format(" ");
					}break;
	}
	strLog_FilePath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path");
	strLog_FileName.Format("%s\\Log\\%s\\%s-%s.log", strLog_FilePath, loginlogFolder, loginlogFolder, tm.Format("%Y%m%d"));
	
	//파일 유뮤 검색
	bool bExistFile = false;
	UINT nID = 0;
	CFileFind aFileFinder;
	if ( aFileFinder.FindFile(strLog_FileName) == FALSE )	{
		int i = strLog_FileName.ReverseFind('\\');
		strLog_FolderPath = strLog_FileName.Mid(0,i);
		CreateDirectory(strLog_FolderPath,NULL);
		nID = CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone;
	}
	else
	{
		nID = CFile::modeWrite | CFile::shareDenyNone;
		bExistFile = true;
	}
	aFileFinder.Close();
	
	//로그 파일 기록 
	CStdioFile aFile;
	if( aFile.Open(strLog_FileName, nID) == FALSE )
		return;
	
	try 
	{
		if( bExistFile )	
			aFile.SeekToEnd();
	} 
	catch (CFileException *pEx)
	{
		pEx->Delete();
	}
	aFile.WriteString(strLog + "\n");
	aFile.Close();
}

// Added by 224 (2015/03/25) : UART-CAN Convert 쓰레드 구동
BOOL CCTSMonProDoc::BeginUartCanConvThread(int nSelCh, int nSelModule, CString strDBName)
{
	//------------------------ SBC init ------------------------
#if 0
	CCyclerModule* pMD;
	pMD = GetModuleInfo(nSelModule);
	
	if (pMD == NULL)
	{
		return FALSE ;
	}
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}
#endif
	//------------------------ SBC init ------------------------

	// 각 모듈에는 2개의 채널이 붙어 있다고 가정한다.
	INT nDevID = (nSelModule-1) * 2 + nSelCh ;
	ASSERT(nDevID >= 0 && nDevID < 4) ;

	// convert items 저장
	// lcc_array에 할당된 메모리는는 m_UartCanConv[] 내에서 삭제 할 것이다.
	CArray<PUCC_INFO, PUCC_INFO> ucc_array ;
	m_pUartDlg->GetUartConvInfo(ucc_array) ;

	CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO> schedule_array ;
	m_pUartDlg->GetScheduleInfo(schedule_array ) ;

// 	CArray<PLINID_INFO, PLINID_INFO> linid_array ;
// 	m_pUartDlg->GetLinIDInfo(linid_array) ;

	// can baudrate
	unsigned long bitrate[] = {125000, 250000, 500000, 1000000};
	INT nIdx = m_pUartDlg->m_CAN_Baudrate.GetCurSel() ;
	INT nCanBaudrate = bitrate[nIdx] ;
	
	m_UartCanConv[nDevID].m_pRxList = &m_pUartDlg->m_ctlRxList ;

	INT nComPort = 8 ;
	INT nBaudrate = 19200 ;
	INT nDataBit  = 8 ;
	INT nParity = 0 ;
	INT nStopBit = 1 ;

	BOOL bReturn = 
		m_UartCanConv[nDevID].BeginUartCanConvThread(nDevID
												 , strDBName
												 , nComPort, nBaudrate, nDataBit, nParity, nStopBit
												 , ucc_array
												 , schedule_array
												 , m_pUartDlg->m_Channel.GetCurSel()
												 , nCanBaudrate
												 ) ;

	// 메모리는 제거하지 않는다.
	ucc_array.RemoveAll() ;
	schedule_array.RemoveAll() ;

	return bReturn ;
}


void CCTSMonProDoc::StopUartCanConvThread(INT nDevID)
{
	ASSERT(nDevID >= 0 && nDevID < 4) ;
	m_UartCanConv[nDevID].StopUartCanConvThread() ;
}

// Added by 224 (2014/10/09) : LIN-CAN Convert 쓰레드 구동
BOOL CCTSMonProDoc::BeginLinCanConvThread(int nSelCh, int nSelModule, CString strDBName)
{
	//------------------------ SBC init ------------------------
	
	CCyclerModule* pMD;
	pMD = GetModuleInfo(nSelModule);
	
	if (pMD == NULL)
	{
		return FALSE ;
	}
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}
	//------------------------ SBC init ------------------------

	// 각 모듈에는 2개의 채널이 붙어 있다고 가정한다.
	INT nDevID = (nSelModule-1) * 2 + nSelCh ;
	ASSERT(nDevID >= 0 && nDevID < 4) ;

	// convert items 저장
	// lcc_array에 할당된 메모리는는 m_LinCanConv[] 내에서 삭제 할 것이다.
	CArray<PLCC_INFO, PLCC_INFO> lcc_array ;
	m_pLinDlg->GetLinConvInfo(lcc_array) ;

	CArray<PSCHEDULER_INFO, PSCHEDULER_INFO> schedule_array ;
	m_pLinDlg->GetScheduleInfo(schedule_array ) ;

	CArray<PLINID_INFO, PLINID_INFO> linid_array ;
	m_pLinDlg->GetLinIDInfo(linid_array) ;

	// can baudrate
	unsigned long bitrate[] = {125000, 250000, 500000, 1000000};
	INT nIdx = m_pLinDlg->m_CAN_Baudrate.GetCurSel() ;
	INT nCanBaudrate = bitrate[nIdx] ;
	
	m_LinCanConv[nDevID].m_pRxList = &m_pLinDlg->m_ctlRxList ;

	BOOL bReturn = 
		m_LinCanConv[nDevID].BeginLinCanConvThread(nDevID
												 , strDBName
// 												 , m_pLinDlg->m_nLinID
												 , m_pLinDlg->m_ctrlLinChannel.GetCurSel()
												 , m_pLinDlg->m_ctrlLinSpeed.GetItemData(m_pLinDlg->m_ctrlLinSpeed.GetCurSel())
												 , linid_array
												 , lcc_array
												 , schedule_array
												 , m_pLinDlg->m_Channel.GetCurSel()
												 , nCanBaudrate
												 ) ;

	// 메모리는 제거하지 않는다.
	lcc_array.RemoveAll() ;
	schedule_array.RemoveAll() ;

	return bReturn ;
/*
//	CString strTemp,strCode;
// 	int		nLength;
// 	int i;

	// Pause 상태에서 재시작 하는 경우 처리
	if (m_nLinMode == LCC_PAUSE)
	{
		m_nLinMode = LCC_PLAY ;
		return TRUE ;
	}

	// GUI에 로그 파일 작성하기 위한 모듈
	HWND hwnd = FindWindow(NULL, "Lin Config") ;

	//------------------------ SBC init ------------------------

	CCyclerModule* pMD;
	pMD = GetModuleInfo(nSelModule);
	
	if (pMD == NULL)
	{
		return FALSE ;
	}

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}
	//------------------------ SBC init ------------------------





	//------------------------ LIN init ------------------------

	CString str ;

	m_nLinID        = m_pLinDlg->m_nLinID ;
	INT nChannelIdx = m_pLinDlg->m_ctrlLinChannel.GetCurSel() ;
	INT nSpeed      = m_pLinDlg->m_ctrlLinSpeed.GetItemData(m_pLinDlg->m_ctrlLinSpeed.GetCurSel()) ;

	if (m_pLIN == NULL)
	{
		m_pLIN = new CLINFunctions ;
	}

	m_pLIN->m_pRXBox     = &m_pLinDlg->m_ctlRxList;
	m_pLIN->m_pStatusBox = &m_pLinDlg->m_ctlRxList;
	XLstatus xlStatus = m_pLIN->LINGetDevice() ;

	if (xlStatus != XL_SUCCESS)
	{
		TRACE("Unable to allocate libmodbus context\n");

		if (hwnd)
		{
			m_pLinDlg->m_ctlRxList.AddString("ERROR: You need LINCabs or LINPiggy's!") ;
			m_pLinDlg->m_ctlRxList.SetCurSel(m_pLinDlg->m_ctlRxList.GetCount()-1);  
		}
		
		ASSERT(FALSE) ;
		return FALSE ;
	}
	
	xlStatus = m_pLIN->LINInit(this, m_pLinDlg->m_nLinID);
	
	if (xlStatus == XL_SUCCESS)
	{
		
	}
	else if (xlStatus == XL_ERR_INVALID_ACCESS)
	{
		AfxMessageBox("ERROR: You need INIT access!", MB_ICONSTOP);
		xlStatus = m_pLIN->LINClose();
	}
	else
	{
		AfxMessageBox("ERROR in INIT !", MB_ICONSTOP);
		xlStatus = m_pLIN->LINClose();
		if (xlStatus)
		{
			AfxMessageBox("ERROR in closing port !", MB_ICONSTOP);
		}
		return FALSE ;
	}	

	//------------------------ can init ------------------------
	ASSERT(m_pCAN == NULL) ;
	m_pCAN = new CCANFunctions ;

	// init the CAN hardware
	if (m_pCAN->CANInit())
	{
		ASSERT(FALSE) ;
// 		m_ctlCanHardware.ResetContent();
// 		m_ctlCanHardware.AddString("<ERROR> no HW!");
// 		m_ctlTxList.AddString("You need two CANcabs/CANpiggy's");
// 		m_btnOnBus.EnableWindow(FALSE);

		if (hwnd)
		{
			m_pLinDlg->m_ctlTxList.AddString("You need two CANcabs/CANpiggy's") ;
			m_pLinDlg->m_ctlTxList.SetCurSel(m_pLinDlg->m_ctlTxList.GetCount()-1);  
		}

		delete m_pCAN ;
		m_pCAN = NULL ;
		return FALSE ;
	}

	// channel 선택
	m_nChannel = m_pLinDlg->m_Channel.GetCurSel() ;

	// go on bus
	int           nIndex=0;
//	XLstatus      xlStatus;
	unsigned long bitrate[5] = {125000, 250000, 500000, 1000000};
		
//	nIndex = m_CAN_Baudrate.GetCurSel();
	int nIdx = m_pLinDlg->m_CAN_Baudrate.GetCurSel() ;
	xlStatus = m_pCAN->CANGoOnBus(bitrate[nIdx]);
	if (!xlStatus)
	{
		m_bSendCANData = TRUE ;
	}
	else
	{
		ASSERT(FALSE) ;
		m_bSendCANData = FALSE ;
	}

	//------------------------ can init ------------------------

	//------------------------ LIN 스케쥴러 init -------------------------

	m_bLinOnBus = TRUE ;
	linCreateSchedulerThread() ;

	
	//----------------- converting array 설정 ----------------- 
	//
	// clearing
	for (INT nLoop = 0; nLoop < lcc_array.GetSize(); nLoop++)
	{
		LCC_INFO* ptr = lcc_array.GetAt(nLoop) ;
		delete ptr ;
	}
	
	lcc_array.RemoveAll() ;

//	m_pLinDlg->GetLinConvInfo(lcc_array) ;
	//----------------- converting array 설정 ----------------- 


	m_nLinMode = LCC_PLAY ;
	m_strLinSettingName = strDBName ;
*/

	return TRUE ;
}

//extern BOOL g_bThreadRun;

void CCTSMonProDoc::StopLinCanConvThread(INT nDevID)
{
	ASSERT(nDevID >= 0 && nDevID < 4) ;
	m_LinCanConv[nDevID].StopLinCanConvThread() ;
}

// Added by 224 (2014/04/23) : Modbus-CAN Convert 쓰레드 구동
BOOL CCTSMonProDoc::BeginMCCThread(int nSelCh, int nSelModule, CString strDBName)
{

	//------------------------ SBC init ------------------------

	CCyclerModule* pMD;
	pMD = GetModuleInfo(nSelModule);
	
	if (pMD == NULL)
	{
		return FALSE ;
	}

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}
	//------------------------ SBC init ------------------------


	//------------------------- 모드버스 설정값 가져오기 ------------------------------------
	// 각 모듈에는 2개의 채널이 붙어 있다고 가정한다.
	INT nDevID = (nSelModule-1) * 2 + nSelCh ;
	ASSERT(nDevID >= 0 && nDevID < 8) ;
	
	m_ModbusCanConv[nDevID].m_pRxList = &m_pModbusDlg->m_ctlRxList ;


	//------------------------------------------------------------------
	// Added by 224 (2014/12/05) :
	// 비정상 종료 후 재시작시 최종공정 자동 실행하게 구성
	// Modbus 시작을 GUI 에서 하지 않을 수 있으므로, DOC 에서 ModubsDlg 에서 가지고 있는
	// 속성값들을 전달하여야 한다.
	// mcc_array 값, 채널값, TCP/COM, IP/baudrate...

	MODBUS_INFO info ;
	ZeroMemory(&info, sizeof(MODBUS_INFO)) ;
	
	CModbusConvDlg::GetModbusCommInfo(strDBName, &info) ;

	INT nCanBaudrate = info.can_baudrate ;

/*
	// can baudrate
	unsigned long bitrate[] = {125000, 250000, 500000, 1000000};
	INT nIdx = m_pModbusDlg->m_CAN_Baudrate.GetCurSel() ;
	INT nCanBaudrate = bitrate[nIdx] ;


	m_pModbusDlg->GetDlgItemText(IDC_DEVICE_COMMUNICATION, info.comm, 16) ;

	unsigned long baudrate[] = { 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200 };
	INT sel = m_pModbusDlg->m_ctrlMBBaudrate.GetCurSel() ;
	info.Baudrate = baudrate[sel] ;

	switch (m_pModbusDlg->m_ctrlParitybit.GetCurSel())
	{
	case 0:
		info.Parity = 'N' ; 
		break ;
	case 1:
		info.Parity = 'O' ; 
		break ;
	case 2:
		info.Parity = 'E' ; 
		break ;
	default:
		ASSERT(FALSE) ;
		info.Parity = 'N' ; 
		break ;
	}
	
	info.Databit = (m_pModbusDlg->m_ctrlDatabits.GetCurSel() == 0) ? 7 : 8 ;
	info.Stopbit = (m_pModbusDlg->m_ctrlStopbit.GetCurSel() == 0) ? 1 : 2 ;
	strcpy(info.IPAddr, m_pModbusDlg->m_strAddr) ;
	info.Port     = m_pModbusDlg->m_nPort ;
	info.MBAddr = m_pModbusDlg->m_nMBAddr ;
	info.MBLen  = m_pModbusDlg->m_nMBLen ;
*/
	//------------------------------------------------------------------

	// convert items 저장
	// lcc_array에 할당된 메모리는는 m_LinCanConv[] 내에서 삭제 할 것이다.
	CArray<PMCC_INFO, PMCC_INFO> mcc_array ;
	CModbusConvDlg::GetModbusConvInfo(strDBName, mcc_array) ;

	BOOL bReturn = 
		m_ModbusCanConv[nDevID].BeginModbusCanConvThread(nDevID
													   , &info
													   , strDBName
													   , mcc_array) ;
//													   , m_pModbusDlg->m_Channel.GetCurSel()
//													   , nCanBaudrate) ;

	// 연결 실패한 경우는 메모리를 삭제한다.
	if (!bReturn)
	{
		for (INT nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
		{
			MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
			delete ptr ;
		}
	}

	// 메모리는 제거하지 않는다.
	mcc_array.RemoveAll() ;

	return bReturn ;

}

void CCTSMonProDoc::PauseMCCThread(INT nDevID)
{
	// Pause 플래그를 세트한다.
	
	ASSERT(nDevID >= 0 && nDevID < 4) ;
	m_ModbusCanConv[nDevID].PauseModbusCanConvThread() ;
//	m_nModbusMode = MCC_PAUSE ;
}

void CCTSMonProDoc::StopMCCThread(INT nDevID)
{
	ASSERT(nDevID >= 0 && nDevID < 4) ;
	m_ModbusCanConv[nDevID].StopModbusCanConvThread() ;

}

void CCTSMonProDoc::ClearUartAllShowMessageFlag()
{
	TRACE("----------------- ClearUartAllShowMessageFlag() called \n") ;
	
	for (int nIdx = 0; nIdx < MAX_UART_SET; nIdx++)
	{
		if (m_UartCanConv[nIdx].m_bShowMessage)
		{
			//			EnterCriticalSection(&m_csShowMessage) ;
			// 활성화된 브로커인 경우 약간의 시간을 두어서 메시지 출력을 보류한다.
			
			m_UartCanConv[nIdx].DoNotShowMessage() ;
			// 함수 내에서 플래그를 해제한다
			ASSERT(m_UartCanConv[nIdx].m_bShowMessage == FALSE) ;
			
			m_UartCanConv[nIdx].m_bShowMessage = FALSE ;
			
			//			LeaveCriticalSection(&m_csShowMessage) ;
			
			Sleep(500) ;
			
		}
		m_UartCanConv[nIdx].m_bShowMessage = FALSE ;
		m_UartCanConv[nIdx].m_pRxList = NULL ;
		m_UartCanConv[nIdx].m_pTxList = NULL ;
	}
	
	TRACE("----------------- ClearAllShowMessageFlag() exited \n") ;
}

void CCTSMonProDoc::SetUartShowMessageFlag(INT nDevID, CListBox* pRx, CListBox* pTx)
{
	ClearUartAllShowMessageFlag() ;
	
	m_UartCanConv[nDevID].m_pRxList = pRx ;
	m_UartCanConv[nDevID].m_pTxList = pTx ;
	m_UartCanConv[nDevID].m_bShowMessage = TRUE ;
	
}

void CCTSMonProDoc::ClearLinAllShowMessageFlag()
{
	TRACE("----------------- ClearLinAllShowMessageFlag() called \n") ;

	for (int nIdx = 0; nIdx < MAX_LIN_SET; nIdx++)
	{
		if (m_LinCanConv[nIdx].m_bShowMessage)
		{
//			EnterCriticalSection(&m_csShowMessage) ;
			// 활성화된 브로커인 경우 약간의 시간을 두어서 메시지 출력을 보류한다.

			m_LinCanConv[nIdx].DoNotShowMessage() ;
			// 함수 내에서 플래그를 해제한다
			ASSERT(m_LinCanConv[nIdx].m_bShowMessage == FALSE) ;
			
			m_LinCanConv[nIdx].m_bShowMessage = FALSE ;

//			LeaveCriticalSection(&m_csShowMessage) ;

			Sleep(500) ;

		}
		m_LinCanConv[nIdx].m_bShowMessage = FALSE ;
		m_LinCanConv[nIdx].m_pRxList = NULL ;
		m_LinCanConv[nIdx].m_pTxList = NULL ;
	}

	TRACE("----------------- ClearAllShowMessageFlag() exited \n") ;
}

void CCTSMonProDoc::SetLinShowMessageFlag(INT nDevID, CListBox* pRx, CListBox* pTx)
{
	ClearLinAllShowMessageFlag() ;

	m_LinCanConv[nDevID].m_pRxList = pRx ;
	m_LinCanConv[nDevID].m_pTxList = pTx ;
	m_LinCanConv[nDevID].m_bShowMessage = TRUE ;

}

void CCTSMonProDoc::ClearModbusAllShowMessageFlag()
{
	TRACE("----------------- ClearModbusAllShowMessageFlag() called \n") ;
	
	for (int nIdx = 0; nIdx < MAX_MODBUS_SET; nIdx++)
	{
		if (m_ModbusCanConv[nIdx].m_bShowMessage)
		{
			//			EnterCriticalSection(&m_csShowMessage) ;
			// 활성화된 브로커인 경우 약간의 시간을 두어서 메시지 출력을 보류한다.
			
			m_ModbusCanConv[nIdx].DoNotShowMessage() ;
			// 함수 내에서 플래그를 해제한다
			ASSERT(m_ModbusCanConv[nIdx].m_bShowMessage == FALSE) ;
			
			m_ModbusCanConv[nIdx].m_bShowMessage = FALSE ;
			
			//			LeaveCriticalSection(&m_csShowMessage) ;
			
			Sleep(500) ;
			
		}
		m_ModbusCanConv[nIdx].m_bShowMessage = FALSE ;
		m_ModbusCanConv[nIdx].m_pRxList = NULL ;
		m_ModbusCanConv[nIdx].m_pTxList = NULL ;
	}
	
	TRACE("----------------- ClearAllShowMessageFlag() exited \n") ;
}

void CCTSMonProDoc::SetModbusShowMessageFlag(INT nDevID, CListBox* pRx, CListBox* pTx)
{
	ClearModbusAllShowMessageFlag() ;
	
	m_ModbusCanConv[nDevID].m_pRxList = pRx ;
	m_ModbusCanConv[nDevID].m_pTxList = pTx ;
	m_ModbusCanConv[nDevID].m_bShowMessage = TRUE ;
	
}

//+2015.9.23 USY Add For PatternCV
BOOL CCTSMonProDoc::CheckPatternData(CString strFile, float fMaxVoltage, float fMaxCurrent, UINT nParallelCnt)
{
	CString	strErrorMsg;
	CString strTemp, str;
	CStdioFile aFile;
	CFileException e;	

	if(nParallelCnt > 0)
	{
		nParallelCnt++;
		fMaxCurrent = fMaxCurrent * nParallelCnt;
	}
	
	if( !aFile.Open( strFile, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
#endif
		//strErrorMsg.Format("%s 파일을 열 수 없습니다.", strFile);
		strErrorMsg.Format(Fun_FindMsg("CTSMonProDoc_CheckPatternData_msg1","CTSMonPro_DOC"), strFile);//&&
		AfxMessageBox(strErrorMsg);
		return FALSE;
	}
	
	CStringList		strDataList;
	CString strMode,strValue;
	strMode = "";

	while(aFile.ReadString(strTemp))
	{
		if(strTemp.Left(3) == "STX")			//여기서부터 실제 Simulation Data -> 이 문자 위로는 주석문(처리안함)
		{
			int p1=0, s=0;
			//1. Title//////////////////////////////////////////////////////////////
			if(aFile.ReadString(strTemp))
			{
				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);
						//m_TitleList.AddTail(str);
						s  = p1+1;
					}
				}
				str = strTemp.Mid(s);
				str.TrimLeft(' '); str.TrimRight(' ');
				strMode = str;
			}
			else
			{
				//strErrorMsg.Format("%s 파일 데이터가 올바르지 않습니다.", strFile);
				strErrorMsg.Format(Fun_FindMsg("CTSMonProDoc_CheckPatternData_msg2","CTSMonPro_DOC"), strFile);//&&
				AfxMessageBox(strErrorMsg);
				return FALSE;
			}
			////////////////////////////////////////////////////////////////////////
			float fVoltage;
			float fChgI;
			float fDisI;
			UINT nRow = 1;
			while(aFile.ReadString(strTemp))
			{
				if (strTemp == "") break;

				p1 = s = 0;

				while(p1!=-1)
				{
					p1 = strTemp.Find(',', s);
					if(p1!=-1)
					{
						str = strTemp.Mid(s, p1-s);			//시간
						s = p1+1;
					}
				}
				if (strMode == "V")
				{//CV에 대해서만 장비설정 최대전류와, 최대전압과, 현재 각 라인별 데이터를 비교하자.

					AfxExtractSubString(strValue,strTemp,1,','); fVoltage = atof(strValue);
					AfxExtractSubString(strValue,strTemp,2,','); fChgI = atof(strValue);
					AfxExtractSubString(strValue,strTemp,3,','); fDisI = atof(strValue);

					if(fVoltage > fMaxVoltage)
					{
						//strErrorMsg.Format("%s 파일의 %d번째 줄의 전압이 설정 최대전압(%.0f(V))보다 큽니다.\n에디트 버튼을 눌러서 수정하세요", strFile, nRow, fMaxVoltage);
						strErrorMsg.Format(Fun_FindMsg("CTSMonProDoc_CheckPatternData_msg3","CTSMonPro_DOC"), strFile, nRow, fMaxVoltage);//&&
						aFile.Close();
						AfxMessageBox(strErrorMsg);
						return FALSE;
					}
					if(fChgI > fMaxCurrent)
					{
						//strErrorMsg.Format("%s 파일의 %d번째 줄의 충전전류가 설정 최대전류(%.0f(A))보다 큽니다.\n에디트 버튼을 눌러서 수정하세요", strFile, nRow, fMaxCurrent);
						strErrorMsg.Format(Fun_FindMsg("CTSMonProDoc_CheckPatternData_msg4","CTSMonPro_DOC"), strFile, nRow, fMaxCurrent);//&&
						aFile.Close();
						AfxMessageBox(strErrorMsg);
						return FALSE;
					}
					if(abs(fDisI) > fMaxCurrent)
					{
						//strErrorMsg.Format("%s 파일의 %d번째 줄의 방전전류가 설정 최대전류(%.0f(A))보다 큽니다.\n에디트 버튼을 눌러서 수정하세요", strFile, nRow, fMaxCurrent);
						strErrorMsg.Format(Fun_FindMsg("CTSMonProDoc_CheckPatternData_msg5","CTSMonPro_DOC"), strFile, nRow, fMaxCurrent);//&&
						aFile.Close();
						AfxMessageBox(strErrorMsg);
						return FALSE;
					}
				}
				nRow++;
			}
		}	
	}
	aFile.Close();
	return TRUE;
}
//-


//ksj 20160624 AuxData 요청 함수 (Aux 값 변경 후 최신 정보로 갱신하기 위해 추가) //사용 안함.
/*int CCTSMonProDoc::SendAuxDataRequest(UINT nModuleID)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);

	//SFT_CMD_AUX_INFO_REQUEST는 ACK가 없는듯. SendCommand 후 TimeOut때까지 기다리는 지연시간이 발생.
	pMD->SendCommand(SFT_CMD_AUX_INFO_REQUEST,NULL,0); 
	
	return TRUE;
}*/
//ksj 20160728 SBCRaw Data 다운로드를 위해 추가.
BOOL CCTSMonProDoc::Fun_FtpGetFile(CString strFullPath, CString strFtpFileName)
{
	if (theFtpConnection == NULL) return FALSE;
	CString strPath;
	CString strFileName;
	int nPos;
	
	nPos = strFullPath.ReverseFind('\\');
	if (nPos < 0) return FALSE;
	
	strPath = strFullPath.Left(nPos);
	strFileName = strFullPath.Right(strFullPath.GetLength() - (nPos+1));
	
	int iResult = _chdir ((LPSTR)(LPCSTR)strPath);	//PC Path 설정
	int rectcode= theFtpConnection->DoGet((LPSTR)(LPCSTR)strFileName);
	
	if(rectcode !=226)	{
		//AfxMessageBox("Don't have sufficient permissions");
		//delete theFtpConnection;
		return FALSE;
	}
	
	strPath.Format("C:\\");
	iResult = _chdir ((LPCTSTR)strPath);	//PC Path 설정
	
	return TRUE;
}

//ksj 20160801 SBC 결과 데이터 백업 쓰레드 (일반 Cycler 에서 가져온 코드 적용)
UINT CCTSMonProDoc::ThreadFtpDownload(LPVOID pParam) 
{
	CCTSMonProDoc	*pMainDoc = (CCTSMonProDoc*)pParam;
	CCyclerChannel	*lpChannelInfo= NULL;					//Channel 
	CCyclerModule	*lpModule = NULL;

	int nTotCh = 0;
	int nChNo = 0;
	int nCheckCntNum = 0;
	int nDownNum = 0;
	int SBCTotalFileNum = 0;
	CString strMsg;
	//BOOL bSbcResultBackup = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Backup Sbc Result", FALSE); //ksj 20160801 전체 데이터 백업 기능 기본 비활성
	BOOL bSbcResultBackup = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Backup Sbc Result", TRUE); //ksj 20200121 전체 데이터 백업 기능 기본 활성화
	BOOL bSbcStepEndBackup = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Backup Sbc StepEnd", TRUE); //ksj 20160801 Step End Data 백업 기능 기본 활성화
	BOOL bSbcAllSBCDataBackup = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DownAllRawDataBefRun", TRUE); //yulee 20190420

	strMsg = "(FTP Down Thread Start !!)" ;
	pMainDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	TRACE("%s \n",strMsg);

	CString strTime=_T("");
	CString strTemp;
	int     iBackCH=0;

	while (!pMainDoc->m_bFinishSBCFileDown)	// 200313 HKH Memory Leak 수정
	{
		if(bSbcResultBackup) //ksj 20160801 옵션 처리 추가.
		{
			if (pMainDoc->m_bDayChange == TRUE)
			{
				pMainDoc->m_bDayChange = FALSE;		//ljb 20150520 add
				if (pMainDoc->m_bWorkingFtpDown == TRUE) 
				{
					Sleep(100);
					continue;
				}
				pMainDoc->m_bWorkingFtpDown = TRUE;	//ljb 20150522 FTP down 작업 시작
				for(int nModuleID = 1; nModuleID <= (int)pMainDoc->GetInstallModuleCount(); nModuleID++)	
				{
					lpModule = pMainDoc->GetCyclerMD(nModuleID);
					if(lpModule == NULL)
					{
						strMsg.Format("lpModule == NULL"); //lyj 20201023 다운로드 로그 강화
						pMainDoc->WriteLog(strMsg);
						Sleep(10);
						continue;
					}
					if(lpModule->GetState() == PS_STATE_LINE_OFF)
					{
						strMsg.Format("FTPDowndload Module : PS_STATE_LINE_OFF"); //lyj 20201023 다운로드 로그 강화
						pMainDoc->WriteLog(strMsg); 
						continue;		//ljb 20150520 임시 , 주석 처리하면 접속 안해도 FTP 다운 진행 함.
					}
					if (pMainDoc->Fun_FtpDownAllChannelData(nModuleID) == TRUE)
					{
						
					}
					else 
					{

					}

				}
				pMainDoc->m_bWorkingFtpDown = FALSE;	//ljb 20150522 FTP down 작업 완료
				continue;
			}
			
			if (pMainDoc->m_bRunFtpDown == TRUE)
			{
				pMainDoc->m_bRunFtpDown = FALSE;		//ljb 20150520 add
				if (pMainDoc->m_bWorkingFtpDown == TRUE) 
				{
					Sleep(100);
					continue;
				}
				
				CWordArray aSelChArray;
				pMainDoc->m_bWorkingFtpDown = TRUE;	//ljb 20150522 FTP down 작업 시작
				for(int nModuleID = 1; nModuleID <= (int)pMainDoc->GetInstallModuleCount(); nModuleID++)	
					//for(int nModuleID = 1; nModuleID <= 1; nModuleID++)	//ljb 20150520 임시 
				{
					aSelChArray.RemoveAll();
					lpModule = pMainDoc->GetCyclerMD(nModuleID);
					if(lpModule == NULL)
					{
						strMsg.Format("lpModule == NULL"); //lyj 20201023 다운로드 로그 강화
						pMainDoc->WriteLog(strMsg);
						Sleep(10);
						continue;
					}
					if(lpModule->GetState() == PS_STATE_LINE_OFF)
					{
						strMsg.Format("FTPDowndload Module : PS_STATE_LINE_OFF"); //lyj 20201023 다운로드 로그 강화
						pMainDoc->WriteLog(strMsg); 
						continue;		//ljb 20150520 임시 , 주석 처리하면 접속 안해도 FTP 다운 진행 함.
					}
					nTotCh = lpModule->GetTotalChannel(); 
					for(nChNo = 0 ; nChNo <nTotCh; nChNo++ )		
					{
						lpChannelInfo = pMainDoc->GetChannelInfo(nModuleID, nChNo);
						if(lpChannelInfo == NULL)	continue;
						if (lpChannelInfo->GetTestName() == "") continue;
						if (lpChannelInfo->IsFtpDataAllDownOK() == TRUE) continue;	//작업 완료 후 FTP down 완료 했음
						
						if( lpChannelInfo->GetState() == PS_STATE_IDLE || lpChannelInfo->GetState() == PS_STATE_STANDBY ||					
							lpChannelInfo->GetState() == PS_STATE_END	|| lpChannelInfo->GetState() == PS_STATE_READY )	
						{
							aSelChArray.Add((WORD)nChNo);	//ljb IsFtpDataAllDownOK() 값이 FALE
						}
						else
						{
							continue;
						}
					}
					
					if (aSelChArray.GetSize() > 0)
					{						
						if (pMainDoc->Fun_FtpDownSelChannelData(nModuleID,&aSelChArray) == TRUE)
						{

						}
						else
						{

						}
					}
					else
					{
						TRACE("FTP download count = 0\n");
					}
				}
				pMainDoc->m_bWorkingFtpDown = FALSE;	//ljb 20150522 FTP down 작업 완료
				continue;
			}
		}
		

		//ksj 20160801 공정 완료시 SBC Step End Data 다운로드 기능 추가.
		if(bSbcStepEndBackup) //옵션 처리
		{
			if (pMainDoc->m_bRunFtpDownStepEnd == TRUE)
			{
				if (pMainDoc->m_bWorkingFtpDown == TRUE) 
				{
					Sleep(100);
					continue;
				}			
				pMainDoc->m_bWorkingFtpDown = TRUE;
				
				//다운로드 작업 수행.
				pMainDoc->Fun_FtpDownSelStepEndData(pMainDoc->m_nFtpDownStepEndModuleID, pMainDoc->m_nFtpDownStepEndChannelIndex);
				pMainDoc->m_bRunFtpDownStepEnd = FALSE;	
				//pMainDoc->m_nFtpDownStepEndModuleID = -1; //lyj 20200526 다운로드 개선 주석
				//pMainDoc->m_nFtpDownStepEndChannelIndex = -1; //lyj 20200526 다운로드 개선 주석
				
				pMainDoc->m_bWorkingFtpDown = FALSE;			
			}
		}		

		//yulee 공정 시작 시 전 실험 SBC SBC Data 다운로드 기능 추가. //yulee 20190420
		if(bSbcAllSBCDataBackup) //옵션 처리
		{

			//unsigned char ucChannelIndex = pMainDoc->m_ucFtpDownIndex;
			CWordArray aSelChArray ;

			for(int i =7;i>=0;i--)
			{
				if((pMainDoc->m_ucFtpDownIndex >> i) & 0x01)
				{
					aSelChArray.Add((WORD)(i)); //ch1 = 0 베이스
				}
			}
		
			if (pMainDoc->m_bRunFtpDownAllSBCData == TRUE)
			{
				//yulee 20190424
				//해당 결과 폴더를 들어가 RawData down 파일 CSV 파일을 읽는다. 
				//완료 여부를 확인한다. 
				//파일이 없거나 완료여부가 0이면 다운을 실시한다.
				//일반시작은 완료가 0 이면 안내 메세지 후 진행
				//예약시작은 완료가 0이면 메세지 없이 다운

// 				CWordArray aSelChArray;	//lyj 2020527 다운로드 개선 주석처리
// 				aSelChArray.Add((WORD)pMainDoc->m_nFtpDownStepEndChannelIndex);	//ljb IsFtpDataAllDownOK() 값이 FALE //lyj 2020527 다운로드 개선 주석처리

				pMainDoc->Fun_ChkCompleteDownAllSBCFile(pMainDoc->m_nFtpDownStepEndModuleID, &aSelChArray);

				//if(pMainDoc->Fun_ChkCompleteDownAllSBCFile(pMainDoc->m_nFtpDownStepEndModuleID, &aSelChArray) == FALSE) //lyj 20210713 주석처리
				if(aSelChArray.GetSize() > 0) //lyj 20210713
				{
					pMainDoc->m_bSucessFtpDownAllSBCData = FALSE;
					BOOL bRet;
					bRet = FALSE;
					if (pMainDoc->m_bWorkingFtpDown == TRUE) 
					{
						Sleep(100);
						continue;
					}			
					pMainDoc->m_bWorkingFtpDown = TRUE;

					//다운로드 작업 수행.
					bRet = pMainDoc->Fun_FtpDownSelChannelAllSBCData(pMainDoc->m_nFtpDownStepEndModuleID, &aSelChArray, nCheckCntNum, SBCTotalFileNum, nDownNum);
					//pMainDoc->m_bRunFtpDownAllSBCData = FALSE;	
					pMainDoc->m_bWorkingFtpDown = FALSE;	
					//aSelChArray.RemoveAll(); //lyj 20200527 다운로드 개선 주석처리
					
					if(bRet == TRUE) //yulee 20190427_1
					{
						pMainDoc->m_bSucessFtpDownAllSBCData = TRUE;
						//pMainDoc->Fun_MakeOrEditFTPDownResultFile(pMainDoc->m_nFtpDownStepEndModuleID,  pMainDoc->m_nFtpDownStepEndChannelIndex,  bRet, SBCTotalFileNum, nCheckCntNum, nDownNum); //lyj 20210713 위치이동
						pMainDoc->m_bRunFtpDownAllSBCData = FALSE;
						pMainDoc->m_nFtpDownStepEndModuleID = -1;
						pMainDoc->m_nFtpDownStepEndChannelIndex = -1;
					}
					else 
					{
						pMainDoc->m_FTPFileDownRetryCnt++;
						pMainDoc->m_nFtpDownStepEndModuleID = pMainDoc->m_nFtpDownStepEndModuleID;
						pMainDoc->m_nFtpDownStepEndChannelIndex = pMainDoc->m_nFtpDownStepEndChannelIndex;
						pMainDoc->m_bRunFtpDownAllSBCData = TRUE;
						pMainDoc->m_bSucessFtpDownAllSBCData = FALSE;

						if(pMainDoc->m_FTPFileDownRetryCnt == 3)
						{
							pMainDoc->m_bRunFtpDownAllSBCData = FALSE;
							pMainDoc->m_bSucessFtpDownAllSBCData = FALSE;
						}
						else
						{
							continue;
						}
					}					
				}
				if(pMainDoc->m_FTPFileDownRetryCnt == 3)
				{
					pMainDoc->m_bSucessFtpDownAllSBCData = FALSE;
				}
				else if(pMainDoc->m_FTPFileDownRetryCnt <=2)
				{
					pMainDoc->m_bSucessFtpDownAllSBCData = TRUE;
				}
				pMainDoc->m_nFtpDownStepEndModuleID = -1;
				pMainDoc->m_nFtpDownStepEndChannelIndex = -1;
				//pMainDoc->m_bRunFtpDownAllSBCData = FALSE; //lyj 20200527 다운로드 개선 주석처리
				
				pMainDoc->m_ucFtpDownIndex = pMainDoc->m_ucFtpDownIndex & 0x0; //lyj 20200527 다운로드 개선
				aSelChArray.RemoveAll();

				if(aSelChArray.IsEmpty()) //lyj 20200527 다운로드 개선
				{
					pMainDoc->m_bRunFtpDownAllSBCData = FALSE;
				}
					
			}

			if(aSelChArray.IsEmpty())
			{
				pMainDoc->m_ucFtpDownIndex = pMainDoc->m_ucFtpDownIndex & 0x0; //lyj 20200527 다운로드 개선
			}

			/*
			//--------------------------------------------
			if(iBackCH==0)
			{
				strTime=GWin::win_CurrentTime(_T("scen"));
				if( strTime==_T("23:59") ||
					g_AppInfo.bChBackTimeStart==TRUE)
				{
					iBackCH=1;
					strTemp.Format(_T("backtime start time:%s forcestart:%d"),strTime,g_AppInfo.bChBackTimeStart);
					GLog::log_Save(_T("down"),_T("sbc"),strTemp,NULL,TRUE);
				}
			}
			if(iBackCH>0)
			{
				CHBACKTIME *ChBackTime=mainGlobal.Fun_ChBackTimeFind(g_AppInfo.ChBackTimeList,iBackCH);
				if(ChBackTime)
				{
					pMainDoc->m_nFtpDownStepEndModuleID = ChBackTime->nModuleID;
					pMainDoc->m_nFtpDownStepEndChannelIndex = ChBackTime->nChannelIndex;
					pMainDoc->m_bRunFtpDownAllSBCData = TRUE;
					pMainDoc->m_FTPFileDownRetryCnt = 0;
					pMainDoc->m_bSucessFtpDownAllSBCData = FALSE;

					iBackCH++;
				}
				else
				{
					strTime=GWin::win_CurrentTime(_T("scen"));
					if(strTime!=_T("23:59"))
					{
						iBackCH=0;
						g_AppInfo.bChBackTimeStart=FALSE;

						strTemp.Format(_T("backtime end time:%s forcestart:%d"),strTime,g_AppInfo.bChBackTimeStart);
						GLog::log_Save(_T("down"),_T("sbc"),strTemp,NULL,TRUE);
					}
				}
			}
			*/
			//--------------------------------------------
		}		
		Sleep(1000);
	}//end while (TRUE)
 	
	pMainDoc->m_bFinishSBCFileDownAck = true;	// 200313 HKH Memory Leak 수정

 	return 0;
}

BOOL CCTSMonProDoc::Fun_MakeOrEditFTPDownResultFile(int nModuleID, int nChannelIndex, BOOL bRet, int SBCTotalFileNum, int CheckCntNum, int DownNum)
{
	CString strWorkTime, strRestoreFolder, strDownFolder, strFileName;
	CTime tm = CTime::GetCurrentTime();
	strWorkTime = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	CCyclerChannel	*lpChannelInfo= NULL;					//Channel

	lpChannelInfo = GetChannelInfo(nModuleID, nChannelIndex);
	if(lpChannelInfo == NULL)	return 0;
	if (lpChannelInfo->GetTestName() == "") return 0;
	strRestoreFolder.Format("%s", lpChannelInfo->GetDataPath());
	strDownFolder.Format("%s\\Restore", strRestoreFolder);		//FTP Down Folder 명
	strFileName.Format("%s\\SBCDataDown.txt", strDownFolder);

	CStdioFile CStdfile;
	if(CStdfile.Open(strFileName, CFile::modeCreate|CFile::modeNoTruncate|CFile::modeWrite|CFile::typeText)==FALSE)
	{
		return FALSE;
	}
	
	CStdfile.SeekToEnd();

	CString tmpStr;
	tmpStr.Format("(시간),(Module),(CH),(저장 Dir),(다운완료여부),(SBC상 파일 개수),(처리된(다운) 파일 개수)\n");
	CStdfile.Write(tmpStr, tmpStr.GetLength());
	tmpStr.Format("%s, %d, %d, %s, %d, %d, %d(Down:%d)\n", strWorkTime, nModuleID, (nChannelIndex+1), strDownFolder, bRet, SBCTotalFileNum, CheckCntNum, DownNum);
	CStdfile.Write(tmpStr, tmpStr.GetLength());
	CStdfile.Close();
	
	return TRUE;
}

BOOL CCTSMonProDoc::Fun_ChkCompleteDownAllSBCFile(int nModuleID, CWordArray *pSelChArray) //yulee 20190424
{
	CCTSMonProDoc *pMainDoc=mainView->GetDocument(); //lyj 20200527 lyj 다운로드 개선
	CCyclerChannel	*lpChannelInfo= NULL;					//Channel 
	CCyclerModule	*lpModule = NULL;
	CString strRawDataFolder, strDownFolder;
	CStringArray ArrStr;
	CString strLine;
	BOOL bresult =FALSE; //lyj 20200527;
	int nTemp =0; 
	ArrStr.RemoveAll();
	
	int nChNo = 0;
	CString strMsg;
	int nChSize = pSelChArray->GetSize(); //lyj 20200527 다운로드 개선

	lpModule = GetCyclerMD(nModuleID);
	if(lpModule == NULL)
	{
		Sleep(10);
		return FALSE;
	}
	
 	//for(nChNo = 1; nChNo <= nChSize; nChNo++) 
	for(nChNo ; nChNo < nChSize; nChNo++)  //lyj 20210720 
 	{
		//lpChannelInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(nChNo-1-nTemp));
		lpChannelInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(nChNo-nTemp)); //lyj 20210713 
		if(lpChannelInfo == NULL || lpChannelInfo->GetTestName() == "")
		{
			pSelChArray->RemoveAt(nChNo-nTemp); //lyj 20210713
			nTemp++;
			continue;
		}

		strRawDataFolder.Format("%s", lpChannelInfo->GetDataPath());
		strDownFolder.Format("%s\\Restore", strRawDataFolder);		//FTP Down Folder 명
		CString strTemp;
		strTemp.Format("SBCDataDown.%s", "txt"); 
		CString strNew = strDownFolder+"\\"+strTemp;

		CFileFind afinder;
		if(afinder.FindFile(strNew) ==  TRUE)
		{
			CFile file;
			int nILine = 1;
			strLine.Format("");
			BOOL bErrRet;
			CString strWorkTime;
			file.Open(strNew, CFile::modeRead);
			CArchive ar(&file, CArchive::load);
			
			//CString tmpStr, tmpStrTime, tmpStrMod, tmpStrCh, tmpStrTestName, tmpStrComplete, tmpSBCFileNum, tmpDownNum;
			//					(시간),(Module),(CH),(저장 Dir),(다운완료여부),(SBC상 파일 개수),(다운받은 파일 개수)
			int cntLine = 0;
			
			while(ar.ReadString(strLine))
			{
				if(strLine.GetLength() == 0)
				{
					break;
				}
				if(cntLine >= 1)
				{
					ArrStr.RemoveAll();
					Fun_GetTokenSBCAllDown(strLine, ArrStr);	
				}
				cntLine++;
				nILine++;
			}
		}
		else
		{
			//break;
			continue;
		}

		if(ArrStr.GetSize() > 5)
		{
			CString tmpStr;
			tmpStr.Format("%s", ArrStr.GetAt(4));
			TRACE(tmpStr);
			if(atoi(ArrStr.GetAt(4))  == TRUE)
			{
				//pMainDoc->m_ucFtpDownIndex = (pMainDoc->m_ucFtpDownIndex ^ (1 << pSelChArray->GetAt(nChNo-1-nTemp))); //lyj 20200527 다운로드 개선
				pMainDoc->m_ucFtpDownIndex = (pMainDoc->m_ucFtpDownIndex ^ (1 << pSelChArray->GetAt(nChNo-nTemp))); //lyj 20200527 다운로드 개선
				//pSelChArray->RemoveAt(pSelChArray->GetAt(nChNo-1)); //lyj 20200527 다운로드 개선 //lyj 20210713 다시 주석
				pSelChArray->RemoveAt(nChNo-nTemp); //lyj 20210713
				nTemp++;
			}
			else if(atoi(ArrStr.GetAt(4))  == FALSE)
			{
				bresult = FALSE;
			}
		}
		else
			return bresult;
	} 
		
	return bresult;

// 	if(ArrStr.GetSize() > 5)
// 	{
// 		CString tmpStr;
// 		tmpStr.Format("%s", ArrStr.GetAt(4));
// 		TRACE(tmpStr);
// 		if(atoi(ArrStr.GetAt(4))  == TRUE)
// 		{
// 			return TRUE;
// 		}
// 		else if(atoi(ArrStr.GetAt(4))  == FALSE)
// 		{
// 			return FALSE;
// 		}
// 	}
// 	else
// 		return FALSE;
}

BOOL CCTSMonProDoc::Fun_GetTokenSBCAllDown(CString strLine, CStringArray &ArrStr) //yulee 20190424
{
	int cnt = 0;
	CString tmpStr;
	while(1)
	{
		if(AfxExtractSubString(tmpStr, strLine, cnt++, ','))
		{
			ArrStr.Add(tmpStr); 
		}
		else
			break;
	}
	return 1;
}

//ksj 20160801 SBC 결과 데이터 백업 (일반 Cycler 에서 가져온 코드 적용)
BOOL CCTSMonProDoc::Fun_FtpDownAllChannelData(int nModuleID)
{
	CCyclerChannel	*lpChannelInfo= NULL;					//Channel 
	CCyclerModule	*lpModule = NULL;
	CString strRawDataFolder, strDownFolder;
	CString strRemoteFolder;
	CString strRemoteFileName, strStartIndexFile, strLastIndexFile, strStepEndFile, strRawDataFile;
	int nChannelIndex;

	//	strRawDataFolder = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore";
	
	int nTotCh = 0;
	int nChNo = 0;
	CString strMsg;
	
	strMsg.Format("Module %d : FTP All Down Start !!",nModuleID);
	WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	TRACE("%s \n",strMsg);
	
	lpModule = GetCyclerMD(nModuleID);
	if(lpModule == NULL)
	{
		Sleep(10);
		return FALSE;
	}
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(lpModule->GetIPAddress());
	if(bConnect == FALSE)
	{
		strMsg.Format("Module %d: Cannot Connect.", nModuleID);
		WriteLog(strMsg); //lyj 20201023 다운로드 로그 강화
		delete pDownLoad;
		TRACE("%s \n",strMsg);
		return FALSE;
	}
	
	nTotCh = lpModule->GetTotalChannel(); 
	for(nChNo = 1 ; nChNo <=nTotCh; nChNo++ )		
	{
		lpChannelInfo = GetChannelInfo(nModuleID, nChNo-1);
		if(lpChannelInfo == NULL)	continue;
		if (lpChannelInfo->GetTestName() == "") continue;
		strRawDataFolder.Format("%s", lpChannelInfo->GetDataPath());
		strDownFolder.Format("%s\\Restore", strRawDataFolder);		//FTP Down Folder 명

		//ksj 20200305 : 데이터 다운로드시 채널로그에도 로그 남기기
		strMsg.Format("Module %d : FTP All Down Start !!",nModuleID);
		if(lpChannelInfo) lpChannelInfo->WriteLog(strMsg);		
		//ksj end

		//lyj //lyj 20200601 다운로드 개선
		nChannelIndex = lpChannelInfo->GetChannelIndex()+1;
		strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", nChannelIndex);
		strRemoteFolder.Format("cycler_data//resultData//ch%02d//", nChannelIndex);
		//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
		if(g_AppInfo.iPType==1)
		{
			if(nChannelIndex>1)
			{
				nChannelIndex = lpChannelInfo->GetChannelIndex()+2;
				strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", nChannelIndex);
				strRemoteFolder.Format("cycler_data//resultData//ch%02d//", nChannelIndex);
			}
		}

		if (ForceDirectory(strDownFolder) == TRUE)
		{
			//Start Index
			//strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", lpChannelInfo->GetChannelIndex()+1); //lyj 20200601 다운로드 개선 주석처리
			strStartIndexFile.Format("%s\\savingFileIndex_start.csv", strDownFolder);
			if(pDownLoad->DownLoadFile2(strStartIndexFile, strRemoteFileName) < 1)
			{
				//strMsg.Format("M%02dCH%02d [%s] file download fail.", nModuleID, lpChannelInfo->GetChannelIndex()+1, strStartIndexFile);
				strMsg.Format("M%02dCH%02d [%s] file download fail.", nModuleID, nChannelIndex, strStartIndexFile); //lyj 20200601 다운로드 개선
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);
			}
			
			int nRawDataCount = 0;
			int nSBCFileNo = 0;
			nRawDataCount = ParsingStartIndexFile(strStartIndexFile, nSBCFileNo);

			int nTotNumFTPFile = 0;//yulee 20190529
			int nTotNumRestoreFile = 0; //lyj 20200601s

			//nTotNumFTPFile = pDownLoad->GetFileTotNum(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1, TRUE);
			nTotNumFTPFile = pDownLoad->GetFileTotNum(strDownFolder, strRemoteFolder, nModuleID, nChannelIndex, TRUE); //lyj 20200601 다운로드 개선
			int bDownCnt = 0;
			
			//strRemoteFolder.Format("cycler_data//resultData//ch%02d//", lpChannelInfo->GetChannelIndex()+1); //lyj 20200601 다운로드 개선 주석처리
			int nDownCnt = 0;

// 			//lyj 20200601 다운로드 개선
// 			nTotNumRestoreFile = GetDirFilesNum(strDownFolder); //Restore 폴더안의 파일개수
// 			
// 			if(nTotNumFTPFile+1 == nTotNumRestoreFile) //nTotNumFTPFile+1은 sbcdownload.txt포함
// 			{
// 				strMsg.Format("M%02dCH%02d PC File count : %d, SBC File count = %d", nModuleID, nTotNumRestoreFile ,nTotNumFTPFile);
// 				WriteLog(strMsg);
// 				TRACE("%s \n",strMsg);
// 
// 				if(lpChannelInfo) lpChannelInfo->WriteLog(strMsg);
// 				continue;
// 			}
			
			int nDownCount = pDownLoad->DownLoadAllFile3_ProgressBar(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1,  nSBCFileNo, nRawDataCount, nTotNumFTPFile, nDownCnt, TRUE);

		//	int nDownCount = pDownLoad->DownLoadAllFile2(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1, nSBCFileNo, nRawDataCount);
			if( nDownCount < 1)
			{
				//strMsg.Format("M%02dCH%02d down load File count 0.", nModuleID, lpChannelInfo->GetChannelIndex()+1);
				strMsg.Format("M%02dCH%02d down load File count 0.", nModuleID, nChannelIndex); //lyj 20200601 다운로드 개선
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);

				//ksj 20200305 : 데이터 다운로드시 채널로그에도 로그 남기기
				if(lpChannelInfo) lpChannelInfo->WriteLog(strMsg);		
				//ksj end

				continue;
			}
			else
			{
				//strMsg.Format("M%02dCH%02d FTP Download count = %d", nModuleID, lpChannelInfo->GetChannelIndex()+1 ,nDownCount);
				strMsg.Format("M%02dCH%02d FTP Download count = %d", nModuleID, nChannelIndex ,nDownCount); //lyj 20200601 다운로드 개선
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);
				
				//ksj 20200305 : 데이터 다운로드시 채널로그에도 로그 남기기
				if(lpChannelInfo) lpChannelInfo->WriteLog(strMsg);		
				//ksj end
			}			
		}
		else
		{
			strMsg.Format("Create fail (%s)", strDownFolder);
			WriteLog(strMsg);
			TRACE("%s \n",strMsg);
			continue;
		}
	}
	
	delete pDownLoad;
	
	strMsg.Format("Module %d : FTP All Down END !!",nModuleID);
	WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	TRACE("%s \n",strMsg);
	return TRUE;
}

//ksj 20160801 SBC 결과 데이터 백업 (일반 Cycler 에서 가져온 코드 적용)
BOOL CCTSMonProDoc::Fun_FtpDownSelChannelData(int nModuleID, CWordArray *pSelChArray)
{
	CCyclerChannel	*lpChannelInfo= NULL;					//Channel 
	CCyclerModule	*lpModule = NULL;
	CString strRawDataFolder, strDownFolder;
	CString strRemoteFolder;
	CString strRemoteFileName, strStartIndexFile, strLastIndexFile, strStepEndFile, strRawDataFile;
	
	//	strRawDataFolder = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore";
	
	int nTotCh = 0;
	int nChNo = 0;
	int nChannelIndex = 0; //lyj 20200528 다운로드 개선
	CString strMsg;
	
	strMsg.Format("Module %d : FTP Select Down Start !!",nModuleID);
	WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	TRACE("%s \n",strMsg);
	
	lpModule = GetCyclerMD(nModuleID);
	if(lpModule == NULL)
	{
		Sleep(10);
		return FALSE;
	}
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(lpModule->GetIPAddress());
	if(bConnect == FALSE)
	{
		strMsg.Format("Module %d: Cannot Connect.", nModuleID);
		delete pDownLoad;
		TRACE("%s \n",strMsg);
		return FALSE;
	}
	
	for(nChNo = 1; nChNo <= pSelChArray->GetSize(); nChNo++)
	{
		lpChannelInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(nChNo-1));
		if(lpChannelInfo == NULL)	continue;
		if (lpChannelInfo->GetTestName() == "") continue;
		strRawDataFolder.Format("%s", lpChannelInfo->GetDataPath());
		strDownFolder.Format("%s\\Restore", strRawDataFolder);		//FTP Down Folder 명

		//ksj 20200305 : 데이터 다운로드시 채널로그에도 로그 남기기
		strMsg.Format("Module %d : FTP Select Down Start !!",nModuleID);
		if(lpChannelInfo) lpChannelInfo->WriteLog(strMsg);		
		//ksj end

		if (ForceDirectory(strDownFolder) == TRUE)
		{
 			//Start Index
			//lyj 20200526 다운로드 개선
			nChannelIndex = lpChannelInfo->GetChannelIndex()+1;
			strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", nChannelIndex);
			strRemoteFolder.Format("cycler_data//resultData//ch%02d//", nChannelIndex);
			//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
			{
				if(nChannelIndex>1)
				{
					nChannelIndex = lpChannelInfo->GetChannelIndex()+2;
					strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", nChannelIndex);
					strRemoteFolder.Format("cycler_data//resultData//ch%02d//", nChannelIndex);
				}
			}
			//#else

			strStartIndexFile.Format("%s\\savingFileIndex_start.csv", strDownFolder);
			if(pDownLoad->DownLoadFile2(strStartIndexFile, strRemoteFileName) < 1)
			{
				//strMsg.Format("M%02dCH%02d [%s] file download fail.", nModuleID, lpChannelInfo->GetChannelIndex()+1, strStartIndexFile);
				strMsg.Format("M%02dCH%02d [%s] file download fail.", nModuleID, nChannelIndex, strStartIndexFile); //lyj 20200528 다운로드 개선
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);
			}
			
			int nRawDataCount = 0;
			int nSBCFileNo = 0;
			nRawDataCount = ParsingStartIndexFile(strStartIndexFile, nSBCFileNo);
			
			int nTotNumFTPFile = 0;//yulee 20190529
			//nTotNumFTPFile = pDownLoad->GetFileTotNum(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1, TRUE);
			nTotNumFTPFile = pDownLoad->GetFileTotNum(strDownFolder, strRemoteFolder, nModuleID, nChannelIndex, TRUE); //lyj 20200528 다운로드 개선
			int bDownCnt = 0;
			
			//strRemoteFolder.Format("cycler_data//resultData//ch%02d//", lpChannelInfo->GetChannelIndex()+1); //lyj 20200528 다운로드 개선 주석처리
			int nDownCnt = 0;
			int nDownCount = pDownLoad->DownLoadAllFile3_ProgressBar(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1,  nSBCFileNo, nRawDataCount, nTotNumFTPFile, nDownCnt, TRUE);
			
			BOOL bRet =FALSE;

			if(nTotNumFTPFile==nDownCount)
			{
				bRet = TRUE;
				Fun_MakeOrEditFTPDownResultFile(nModuleID,  pSelChArray->GetAt(nChNo-1), bRet, nTotNumFTPFile, nDownCount, 0); //lyj 20200528 다운로드 개선
				lpChannelInfo->SetFtpAllDown(TRUE);
			}
			else
			{
				bRet =FALSE;
				Fun_MakeOrEditFTPDownResultFile(nModuleID,  pSelChArray->GetAt(nChNo-1), bRet, nTotNumFTPFile, nDownCount, 0); //lyj 20200528 다운로드 개선
			}
			

		//	int nDownCount = pDownLoad->DownLoadAllFile2(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1, nSBCFileNo, nRawDataCount);
			if( nDownCount < 1)
			{
				strMsg.Format("M%02dCH%02d down load File count 0.", nModuleID, lpChannelInfo->GetChannelIndex()+1);
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);		

				//ksj 20200305 : 데이터 다운로드시 채널로그에도 로그 남기기
				if(lpChannelInfo)lpChannelInfo->WriteLog(strMsg);		
				//ksj end
			}
			else
			{
				strMsg.Format("M%02dCH%02d FTP Download count = %d", nModuleID, lpChannelInfo->GetChannelIndex()+1 ,nDownCount);
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);

				//ksj 20200305 : 데이터 다운로드시 채널로그에도 로그 남기기
				if(lpChannelInfo)lpChannelInfo->WriteLog(strMsg);		
				//ksj end
			}
			m_awAutoRestoreChArray.Add(MAKELONG(lpChannelInfo->GetChannelIndex(), nModuleID));		//ljb 20150522 손실체크 및 자동 복구 위해 추가 함.
		}
		else
		{
			strMsg.Format("Create fail (%s)", strDownFolder);
			WriteLog(strMsg);
			TRACE("%s \n",strMsg);
		}
		//lpChannelInfo->SetFtpAllDown(TRUE); //lyj 20200508 다운로드 개선
	}
	delete pDownLoad;
	
	strMsg.Format("Module %d : FTP Select Down END !!",nModuleID);
	WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	TRACE("%s \n",strMsg);
	return TRUE;
}

//yulee 20190420
BOOL CCTSMonProDoc::Fun_FtpDownSelChannelAllSBCData(int nModuleID, CWordArray *pSelChArray, int &nChkNum, int &nTotalSBCFileNum, int &nDownNum)
{//yulee 20190424 SBC해당 디렉토리에 파일 개수 확인 다운 시 카운트하여 비교한 후에 두 개수가 맞으면 return값 1 아니면 0
	int nDoneCount = 0;
	int nDownCnt = 0; //yulee 20190527
	int nTotNumFTPFile = 0;

	CCTSMonProDoc *pMainDoc=mainView->GetDocument(); //lyj 20200527 lyj 다운로드 개선
	CCyclerChannel	*lpChannelInfo= NULL;//Channel 
	CCyclerModule	*lpModule = NULL;
	CString strRawDataFolder, strDownFolder;
	CString strRemoteFolder;
	CString strRemoteFileName, strStartIndexFile, strLastIndexFile, strStepEndFile, strRawDataFile;
	
	//strRawDataFolder = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "Restore";
	
	int nTotCh = 0;
	int nChNo = 0;
	int nChannelIndex = 0;
	int nChSize = pSelChArray->GetSize(); //lyj 20200527 다운로드 개선

	CString strMsg;
	
	strMsg.Format("Module %d : FTP Select Down Start !!",nModuleID);
	WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	TRACE("%s \n",strMsg);
	
	lpModule = GetCyclerMD(nModuleID);
	if(lpModule == NULL)
	{
		Sleep(10);
		return FALSE;
	}
	CFtpDownLoad *pDownLoad = new CFtpDownLoad;
	BOOL bConnect = pDownLoad->ConnectFtpSession(lpModule->GetIPAddress());
	if(bConnect == FALSE)
	{
		strMsg.Format("Module %d: Cannot Connect.", nModuleID);
		delete pDownLoad;
		TRACE("%s \n",strMsg);
		return FALSE;
	}

	//for(nChNo = 1; nChNo <= pSelChArray->GetSize(); nChNo++)
	for(nChNo = 1; nChNo <= nChSize; nChNo++)
	{
		lpChannelInfo = GetChannelInfo(nModuleID, pSelChArray->GetAt(nChNo-1));
		if(lpChannelInfo == NULL)	continue;
		if (lpChannelInfo->GetTestName() == "") continue;

		//lyj 20200526 다운로드 개선
		nChannelIndex = lpChannelInfo->GetChannelIndex()+1;
		strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", lpChannelInfo->GetChannelIndex()+1);

		if(g_AppInfo.iPType==1)
		{
			if(nChannelIndex>1)
			{
				nChannelIndex = lpChannelInfo->GetChannelIndex()+2;
				strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", nChannelIndex);
			}
		}


		strRawDataFolder.Format("%s", lpChannelInfo->GetDataPath());
		strDownFolder.Format("%s\\Restore", strRawDataFolder);		//FTP Down Folder 명
		if (ForceDirectory(strDownFolder) == TRUE)
		{
			//Start Index
			//strRemoteFileName.Format("cycler_data//resultData//ch%02d//savingFileIndex_start.csv", lpChannelInfo->GetChannelIndex()+1); //lyj 20200526 다운로드 개선 주석처리
			strStartIndexFile.Format("%s\\savingFileIndex_start.csv", strDownFolder);
			if(pDownLoad->DownLoadFile2(strStartIndexFile, strRemoteFileName) < 1)
			{
				//strMsg.Format("M%02dCH%02d [%s] file download fail.", nModuleID, lpChannelInfo->GetChannelIndex()+1, strStartIndexFile);
				strMsg.Format("M%02dCH%02d [%s] file download fail.", nModuleID, nChannelIndex, strStartIndexFile); //lyj 20200526 다운로드 개선
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);
			}
			
			int nRawDataCount = 0;
			int nSBCFileNo = 0;
			nRawDataCount = ParsingStartIndexFile(strStartIndexFile, nSBCFileNo);
			
			strMsg.Format(_T("CH:%d down ftp:%s local:%s sbcfileno:%d rawdatacount:%d"),
				          lpChannelInfo->m_iChannelNo,strRemoteFileName,strStartIndexFile,nSBCFileNo,nRawDataCount);
			GLog::log_Save(_T("down"),_T("sbc"),strMsg,NULL,TRUE);

			//strRemoteFolder.Format("cycler_data//resultData//ch%02d//", lpChannelInfo->GetChannelIndex()+1);
			strRemoteFolder.Format("cycler_data//resultData//ch%02d//", nChannelIndex); //lyj 20200526 다운로드 개선

			//nTotNumFTPFile = pDownLoad->GetFileTotNum(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1, TRUE);
			nTotNumFTPFile = pDownLoad->GetFileTotNum(strDownFolder, strRemoteFolder, nModuleID, nChannelIndex, TRUE); //lyj 20200526 다운로드 개선

			//nDoneCount = pDownLoad->DownLoadAllFile3_ProgressBar(strDownFolder, strRemoteFolder, nModuleID, lpChannelInfo->GetChannelIndex()+1, nSBCFileNo, nRawDataCount, nTotNumFTPFile, nDownCnt, TRUE);
			nDoneCount = pDownLoad->DownLoadAllFile3_ProgressBar(strDownFolder, strRemoteFolder, nModuleID, nChannelIndex, nSBCFileNo, nRawDataCount, nTotNumFTPFile, nDownCnt, TRUE); //lyj 20200526 다운로드 개선
			

			if( nDoneCount < 1)
			{
				//strMsg.Format("M%02dCH%02d down load File count 0.", nModuleID, lpChannelInfo->GetChannelIndex()+1);
				strMsg.Format("M%02dCH%02d down load File count 0.", nModuleID, nChannelIndex);  //lyj 20200526 다운로드 개선
				WriteLog(strMsg);
				TRACE("%s \n",strMsg);
			}
			else
			{
				//strMsg.Format("M%02dCH%02d FTP Download count = %d, Total File Num = %d", nModuleID, lpChannelInfo->GetChannelIndex()+1 ,nDoneCount, nTotNumFTPFile);
				strMsg.Format("M%02dCH%02d FTP Download count = %d, Total File Num = %d", nModuleID, nChannelIndex ,nDoneCount, nTotNumFTPFile); //lyj 20200526 다운로드 개선
				WriteLog(strMsg);
				nChkNum = nDoneCount;
				nTotalSBCFileNum = nTotNumFTPFile;
				TRACE("%s \n",strMsg);

				//ksj 20200305 : 데이터 다운로드시 채널로그에도 로그 남기기
				if(lpChannelInfo) lpChannelInfo->WriteLog(strMsg);		
				//ksj end

				pMainDoc->m_ucFtpDownIndex = (pMainDoc->m_ucFtpDownIndex ^ (1 << pSelChArray->GetAt(nChNo-1))); //lyj 20200527 다운로드 개선
				//pSelChArray->RemoveAt(pSelChArray->GetAt(nChNo-1)-1); //lyj 20200527 다운로드 개선
				pMainDoc->Fun_MakeOrEditFTPDownResultFile(nModuleID,  nChannelIndex-1, true, nTotNumFTPFile, nTotNumFTPFile, nDoneCount); //lyj 20210713 위치이동
			}
			m_awAutoRestoreChArray.Add(MAKELONG(lpChannelInfo->GetChannelIndex(), nModuleID));		//ljb 20150522 손실체크 및 자동 복구 위해 추가 함.
		}
		else
		{
			strMsg.Format("Create fail (%s)", strDownFolder);
			WriteLog(strMsg);
			TRACE("%s \n",strMsg);
		}
		//lpChannelInfo->SetFtpAllDown(TRUE); //lyj 20200528 다운로드 개선 이전작업 데이터 다운로드 완료 시 TRUE , TRUE 이면 작업완료 시 루프를 타지않아서 주석처리
	}
	delete pDownLoad;
	
	strMsg.Format("Module %d : FTP Select Down END !!",nModuleID);
	WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	TRACE("%s \n",strMsg);
	// 	progressWnd.SetPos(100);
	// 	strLogMsg.Format("완료 중...");
	// 	progressWnd.SetText(strLogMsg);
	if(nChkNum == nTotalSBCFileNum)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
//ksj 20160801 SBC 결과 데이터 백업 (일반 Cycler 에서 가져온 코드 적용)
int CCTSMonProDoc::ParsingStartIndexFile(CString strStartFileName, int &nSBCFileNo)
{
	char szBuff1[128], szBuff2[128], szBuff3[128], szBuff4[128], szBuff5[128], szBuff6[128];
	int nTemp1 = 0, nTemp2 = 0, nTemp3 = 0, nStartNo = 0, nLastNo = 0, nFileNo = 0, nMaxFileindex = 0;//yulee 20190707
	int nCount = 0;
	FILE *fp = NULL;
	//fp = fopen(strStartFileName, "rt");
	fp = fopen(strStartFileName, "rt");
	if(fp == NULL)		return false;
	
	if(fp)
	{
// 		//저장구조 : fileIndex 1, resultIndex 1,  maxFileIndex 1, open_year 5, open_month 12, open_day 13 //yulee 20190707
		CString tmpStr;
		while( fscanf(fp, "%s %d, %s %d, %s %d, %s %d, %s %d, %s %d",  
			szBuff1, &nFileNo, 
			szBuff2, &nStartNo, 
			szBuff3, &nMaxFileindex, 
			szBuff4, &nTemp1, 
			szBuff5, &nTemp2, 
			szBuff6, &nTemp3) > 0) // fread(szBuff1, sizeof(szBuff1), 1, fp) == 0)
		{
			nCount++;
		}fclose(fp);

// 		//저장구조 : fileIndex 1, resultIndex 1, open_year 5, open_month 12, open_day 13
// 		while( fscanf(fp, "%s %d, %s %d, %s %d, %s %d, %s %d, %s %d", 
// 			szBuff, &nFileNo,
// 			szBuff, &nStartNo,
// 			szBuff, &nMaxFileindex,
// 			szBuff, &nTemp1,
// 			szBuff, &nTemp2,
// 			szBuff, &nTemp3 ) > 0)
// 		{
// 			nCount++;
// 		}
// 		fclose(fp);
	}
	else
	{
		TRACE("Module index start file %d open fail\n", strStartFileName);
		return -1;
	}
	
	nSBCFileNo = nFileNo;
	return nCount;
}

//ksj 20160801 SBC Step End Data FTP Download
BOOL CCTSMonProDoc::Fun_FtpDownSelStepEndData(int nModuleID, int nChannelIndex, int nOption) //nOption 0:덮어쓰기 1: 존재하면 Pass
{	
	if(nModuleID == -1 || nChannelIndex == -1) return FALSE;
	
	int i, j;
	int nTotalModule = GetInstallModuleCount();
	CCyclerModule *pMD = NULL;
	CCyclerChannel *pChannel = NULL;
	CString strTmp, strMsg;	
	
	pMD = GetCyclerMD(nModuleID);			
	pChannel = pMD->GetChannelInfo(nChannelIndex);
	
	if(pMD->GetState() == PS_STATE_LINE_OFF)
		return FALSE;
	
	int nChannelNum = nChannelIndex;
	
	if( pChannel->GetState()!= PS_STATE_RUN) //작업중 아님.
	{			
		strTmp.Format("%s\\Restore",pChannel->GetDataPath());
		
		if(ForceDirectory(strTmp))
		{
			CFileFind fileFinder;
			CString strFileName[MAX_SBC_STEP_END_DATA_FILE];	
			CString strFileNameFtp[MAX_SBC_STEP_END_DATA_FILE];	
			BOOL bFileDLFlag[MAX_SBC_STEP_END_DATA_FILE]= {0,};
			CString strDnPath;
			CString strResultPath;
			
			//PC에 복사할 File명
			strFileName[0].Format("ch%03d_SaveEndData.csv", nChannelIndex + 1);
			strFileName[1].Format("ch%03d_SaveEndData_auxT.csv", nChannelIndex + 1); 
			strFileName[2].Format("ch%03d_SaveEndData_auxV.csv", nChannelIndex + 1); 
			strFileName[3].Format("ch%03d_SaveEndData_canMaster.csv", nChannelIndex + 1);
			strFileName[4].Format("ch%03d_SaveEndData_canSlave.csv", nChannelIndex + 1); 	
			strFileName[5].Format("savingFileIndex_start.csv"); 
			strFileName[6].Format("savingFileIndex_last.csv"); 
			
			//다운 받을 Step End Data File들..
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
			{
				if (nChannelIndex ==  1) nChannelNum = 2;	//ljb 20170825 ch번호 변경 1->3  //ksj 20171027 : 주석처리
			}
//#endif
			strFileNameFtp[0].Format("ch%03d_SaveEndData.csv", nChannelNum + 1);
			strFileNameFtp[1].Format("ch%03d_SaveEndData_auxT.csv", nChannelNum + 1); 
			strFileNameFtp[2].Format("ch%03d_SaveEndData_auxV.csv", nChannelNum + 1); 
			strFileNameFtp[3].Format("ch%03d_SaveEndData_canMaster.csv", nChannelNum + 1);
			strFileNameFtp[4].Format("ch%03d_SaveEndData_canSlave.csv", nChannelNum + 1); 	
			strFileNameFtp[5].Format("savingFileIndex_start.csv"); 
			strFileNameFtp[6].Format("savingFileIndex_last.csv"); 
			
			//nOption이 1인 경우 미리확인하여 PC에 파일이 모두 존재하면 중단한다.
			int nExistCnt = 0;
			for(j = 0; j < MAX_SBC_STEP_END_DATA_FILE; j++)
			{
				strDnPath.Format("%s\\%s", strTmp, strFileName[j]);
				
				bFileDLFlag[j] = fileFinder.FindFile(strDnPath);
				
				if(nOption == 1 && bFileDLFlag[j])
				{
					nExistCnt++;
				}
			}
			
			if(nExistCnt == MAX_SBC_STEP_END_DATA_FILE)
				return FALSE;
			
			CFtpDownLoad *pDownLoad = new CFtpDownLoad;
			BOOL bConnect = pDownLoad->ConnectFtpSession(pMD->GetIPAddress());
			if(bConnect == FALSE)
			{
				strMsg.Format("Module %d: Cannot Connect.", nModuleID);
				delete pDownLoad;
				TRACE("%s \n",strMsg);
				return FALSE;
			}
			
			strResultPath.Format("cycler_data//resultData//ch%02d//", nChannelNum + 1);//ljb 20170825 ch번호 변경 1->3 //ksj 20171027 : 위에 nChannelNum = 2; 주석처리 하였으므로 그냥  놔둠.

			
			for(j = 0; j < MAX_SBC_STEP_END_DATA_FILE; j++)
			{
				if(nOption == 1 && bFileDLFlag[j]) //옵션이 1이고 이미 다운받은 파일이 존재하면 다운 안함
					continue;
				
				strDnPath.Format("%s\\%s", strTmp, strFileName[j]);
				if(pDownLoad->DownLoadFile2(strDnPath, strResultPath + strFileNameFtp[j]) < 1)
				{
					strMsg.Format("M%02dCH%02d [%s] file download fail.", nModuleID, pChannel->GetChannelIndex()+1, strDnPath);
					WriteLog(strMsg);
					TRACE("%s \n",strMsg);
				}
				
			}
			delete pDownLoad;		
		}		
	}
	return TRUE;
}


void CCTSMonProDoc::LoadChillerData(int nItem)
{
	DWORD dwItem[MAX_OVEN_COUNT][64];
	LPVOID pData = NULL;
	UINT nSize = 0;
	
	BOOL bUseOven;	
	CDWordArray adwCh; 
	
	int   nRtn;
	int   iCnt,nSelModel, nMaxOven;
    float fPVFactorValue = (10.0f); 
	float fSPFactorValue = (10.0f); 
	int   iCHNO=0;
	
	CString strKey;
	for(int i=0;i<MAX_CHILLER_COUNT;i++)
	{
		//-----------------------------
		if((i+1)!=nItem) continue;
		//-----------------------------
		if(i==0)      strKey=REG_SERIAL_CHILLER1;
		else if(i==1) strKey=REG_SERIAL_CHILLER2;
		else if(i==2) strKey=REG_SERIAL_CHILLER3;
		else if(i==3) strKey=REG_SERIAL_CHILLER4;
		else continue;
		
		bUseOven       = AfxGetApp()->GetProfileInt(strKey  , "Use"             , FALSE);
		nSelModel      = AfxGetApp()->GetProfileInt(strKey  , "ChillerModel"    ,3);
		nMaxOven       = AfxGetApp()->GetProfileInt(strKey  , "ChillerMaxCount" ,1);
		iCHNO          = AfxGetApp()->GetProfileInt(strKey  , "ChNo", 0);		
		fPVFactorValue   = (float)AfxGetApp()->GetProfileInt(strKey  , "OvenOndoFactor", 10); 
		fSPFactorValue   = (float)AfxGetApp()->GetProfileInt(strKey  , "OvenOndoSPFactor", 10); 
		//-------------------------------------------------------------------------------------
		m_chillerCtrl[i].m_nCHNO=iCHNO;
		m_chillerCtrl[i].m_ctrlOven.onPumpSetCHNO(iCHNO);
		//-------------------------------------------------------------------------------------
		if (bUseOven)
		{			
			memset(dwItem, 0, sizeof(dwItem));
			nRtn = AfxGetApp()->GetProfileBinary(strKey, "OvenChMap", (LPBYTE *)&pData, &nSize);
			if(nSize > 0 && nRtn == TRUE)	
			{
				memcpy(dwItem, pData, nSize);
			}
			
			for (iCnt = 0; iCnt < nMaxOven; iCnt++)
			{
				m_chillerCtrl[i].m_ctrlOven.SetUseOven(iCnt, bUseOven);
				
				adwCh.RemoveAll();
				for(int j = 0 ; j < 64; j++)
				{
					if(dwItem[iCnt][j] != 0)
						adwCh.Add(dwItem[iCnt][j]);
				}
				if(adwCh.GetSize() > 0)
				{
					m_ChamberCtrl.m_ctrlOven1.SetChannelMapArray(iCnt, adwCh);			
				}
			}						
			m_chillerCtrl[i].m_ctrlOven.m_bUseOvenCtrl = TRUE;	//설정된 Oven중 한개라도 사용하면 OvenCtrl 사용
			m_chillerCtrl[i].m_ctrlOven.m_bPortOpen = FALSE;	//ljb 20180219 add RS232 Port Open 상태
			m_chillerCtrl[i].m_ctrlOven.SetOvenCount(nMaxOven);
			m_chillerCtrl[i].m_ctrlOven.SetOvenModelType(nSelModel);
			m_chillerCtrl[i].m_ctrlOven.SetOvenOndoPSFactor(fPVFactorValue);
			m_chillerCtrl[i].m_ctrlOven.SetOvenOndoSPFactor(fSPFactorValue);
			delete [] pData;
		}
	}
}

void CCTSMonProDoc::LoadChamberData(int nItem)
{
	DWORD dwItem[MAX_OVEN_COUNT][64];
	LPVOID pData = NULL;
	UINT nSize = 0;

	BOOL bUseOven;	
	CDWordArray adwCh; 

	int   nRtn;
	int   iCnt,nSelModel, nMaxOven;
	float fPVFactorValue = (10.0f); 
	float fSPFactorValue = (10.0f); 
	int   iCHNO=0;

	CString strKey, strKeyConfig, RegNameUse, RegNamePVFactor, RegNameSPFactor;
	for(int i=0;i<MAX_CHAMBER_COUNT;i++)
	{
		//-----------------------------
		if((i+1)!=nItem) continue;
		//-----------------------------
		if(i==0)      strKey=REG_SERIAL_CONFIG3;
		else if(i==1) strKey=REG_SERIAL_CONFIG4;
		else continue;
		strKeyConfig = CT_CONFIG_REG_SEC;
		RegNameUse.Format("Use Oven %d", i+1);
		RegNamePVFactor.Format("OvenOndoFactor%d", i+1);
		RegNameSPFactor.Format("OvenOndoSPFactor%d", i+1);


		bUseOven		= AfxGetApp()->GetProfileInt(strKeyConfig, RegNameUse	, FALSE);
		nSelModel		= AfxGetApp()->GetProfileInt(strKey, "ChamberModel"		, 3);
		nMaxOven		= AfxGetApp()->GetProfileInt(strKey, "ChamberMaxCount"	, 1);
		iCHNO			= AfxGetApp()->GetProfileInt(strKey, "ChNo"				, 0);		
		fPVFactorValue	= AfxGetApp()->GetProfileInt(strKeyConfig, RegNamePVFactor	, FALSE); 
		fSPFactorValue	= AfxGetApp()->GetProfileInt(strKeyConfig, RegNameSPFactor	, 10);

		//-------------------------------------------------------------------------------------
		m_ChamberCtrl.mST_OvenContl[i].m_nCHNO=iCHNO;
		//-------------------------------------------------------------------------------------
		if (bUseOven)
		{	
			if(nItem == 1)
			{		
				memset(dwItem, 0, sizeof(dwItem));
				nRtn = AfxGetApp()->GetProfileBinary(strKey, "OvenChMap", (LPBYTE *)&pData, &nSize);
				if(nSize > 0 && nRtn == TRUE)	
				{
					memcpy(dwItem, pData, nSize);
				}

				for (iCnt = 0; iCnt < nMaxOven; iCnt++)
				{
					m_ChamberCtrl.m_ctrlOven1.SetUseOven(iCnt, bUseOven);

					adwCh.RemoveAll();
					for(int j = 0 ; j < 64; j++)
					{
						if(dwItem[iCnt][j] != 0)
							adwCh.Add(dwItem[iCnt][j]);
					}
					if(adwCh.GetSize() > 0)
					{
						m_ChamberCtrl.m_ctrlOven1.SetChannelMapArray(iCnt, adwCh);			
					}
				}						
				m_ChamberCtrl.m_ctrlOven1.m_bUseOvenCtrl = TRUE;	//설정된 Oven중 한개라도 사용하면 OvenCtrl 사용
				m_ChamberCtrl.m_ctrlOven1.m_bPortOpen = FALSE;	//ljb 20180219 add RS232 Port Open 상태
				m_ChamberCtrl.m_ctrlOven1.SetOvenCount(nMaxOven);
				m_ChamberCtrl.m_ctrlOven1.SetOvenModelType(nSelModel);
				m_ChamberCtrl.m_ctrlOven1.SetOvenOndoPSFactor(fPVFactorValue);
				m_ChamberCtrl.m_ctrlOven1.SetOvenOndoSPFactor(fSPFactorValue);			
				delete [] pData;
			}
			else
			{
				memset(dwItem, 0, sizeof(dwItem));
				nRtn = AfxGetApp()->GetProfileBinary(strKey, "OvenChMap", (LPBYTE *)&pData, &nSize);
				if(nSize > 0 && nRtn == TRUE)	
				{
					memcpy(dwItem, pData, nSize);
				}

				for (iCnt = 0; iCnt < nMaxOven; iCnt++)
				{
					m_ChamberCtrl.m_ctrlOven2.SetUseOven(iCnt, bUseOven);

					adwCh.RemoveAll();
					for(int j = 0 ; j < 64; j++)
					{
						if(dwItem[iCnt][j] != 0)
							adwCh.Add(dwItem[iCnt][j]);
					}
					if(adwCh.GetSize() > 0)
					{
						m_ChamberCtrl.m_ctrlOven2.SetChannelMapArray(iCnt, adwCh);			
					}
				}						
				m_ChamberCtrl.m_ctrlOven2.m_bUseOvenCtrl = TRUE;	//설정된 Oven중 한개라도 사용하면 OvenCtrl 사용
				m_ChamberCtrl.m_ctrlOven2.m_bPortOpen = FALSE;	//ljb 20180219 add RS232 Port Open 상태
				m_ChamberCtrl.m_ctrlOven2.SetOvenCount(nMaxOven);
				m_ChamberCtrl.m_ctrlOven2.SetOvenModelType(nSelModel);
				m_ChamberCtrl.m_ctrlOven2.SetOvenOndoPSFactor(fPVFactorValue);
				m_ChamberCtrl.m_ctrlOven2.SetOvenOndoSPFactor(fSPFactorValue);			
				delete [] pData;
			}
		}
	}
}

//ksj 20200120 : DB 자동 업데이트 함수
int CCTSMonProDoc::UpdateDatabase(void)
{
	/*UpdateDefaultUser();
	UpdateChannelcode_v1016();
	UpdateMDBAuxH(); //ksj 20200211 : AuxH 관련 항목 자동 업데이트
	UpdateMDBSequence(); //lyj 20200520 : Sequence 관련 항목 자동 업데이트
	UpdateMDBChiller(); //ksj 20200910 : 칠러 항목 자동 업데이트
	UpdateMDBPowerSupply(); //ksj 20200910 : 파워 서플라이 항목 자동 업데이트.
	UpdateMDBFlicker(); //lyj 20201005 */

	CUpdateDatabase udb(this);
	udb.UpdateDatabase(); //ksj 20201013 : MDB 자동 업데이트 기능. 별도의 클래스로 분리 코드 정리.

	return 0;
}

#if 0
//ksj 20191104 : 쿼리 연속 실행 함수 추가.
//ksj 20201012 : 쿼리 실패시 이후 쿼리 계속 진행 여부 옵션 추가 
//bFailContinue TRUE인 경우 계속 실행, FALSE 인 경우 스탑.
//int CCTSMonProDoc::ExecuteQueryDB(CStringArray& saSQL, CString strDatabaseName)
int CCTSMonProDoc::ExecuteQueryDB(CStringArray& saSQL, CString strDatabaseName, BOOL bFailContinue)
{
	CString strTemp;
	BOOL breturn = TRUE;
	CDaoDatabase  db;	
	try
	{
		//db.Open(GetDataBaseName());
		db.Open(strDatabaseName);
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		WriteLog(strTemp);
		e->Delete();
		return 0;
	}

	int i = 0;
	for(i=0;i<saSQL.GetCount();i++)
	{
		strTemp = saSQL.GetAt(i);

		try
		{
			db.Execute(strTemp);
		}
		catch (CDaoException* e)
		{
			TRACE("%s\n",e->m_pErrorInfo->m_strDescription);		
#ifdef _DEBUG
			strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
#else  //릴리즈 버전에서는 에러로 표시 하지 않는다.
			if(e->m_pErrorInfo->m_lErrorCode == 3380)
			{
				strTemp.Format("DB Update Result Code(%d) : 이미 DB업데이트 완료됨.",e->m_pErrorInfo->m_lErrorCode);
			}
			else
			{
				strTemp.Format("DB Update Result Code(%d)",e->m_pErrorInfo->m_lErrorCode);
			}		
#endif
			WriteLog(strTemp);

			e->Delete();
			breturn = FALSE;
			if(!bFailContinue) break; //ksj 20201012 : bFailContinue FALSE 인 경우 다음 쿼리 실행 중단.
		}
	}	

	db.Close();		

	Sleep(300);
	
	return breturn;
}


//ksj 20200120 : 기본 계정 자동 생성.
#define UPDATE_USER_CNT 4
int CCTSMonProDoc::UpdateDefaultUser()
{
	CString strTemp;
	CString strTableName;
	char szUserID[UPDATE_USER_CNT][256] = {
		"lgc", //lg 관리자 계정
		"lguser", //lg 일반 계정
		"su", //관리자 계정
		"user" //일반 계정
	}; 

	char szPassword[UPDATE_USER_CNT][256] = {
		"lgchem!",
		"lguser",
		"su!",
		"user"
	};

	char szName[UPDATE_USER_CNT][256] = {
		"lgc",
		"lguser",
		"su",
		"user"
	};

	int nAuthority[UPDATE_USER_CNT] = {
		PMS_SUPERVISOR, //admin,	
		PS_USER_OPERATOR, //작업자
		PMS_SUPERVISOR, //admin,	
		PS_USER_OPERATOR //작업자
	};



	CDaoDatabase  db;	
	try
	{
		db.Open(GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		WriteLog(strTemp);
		e->Delete();
		return 0;
	}

	int i = 0;
	for(i=0;i<UPDATE_USER_CNT;i++)
	{
		try
		{
			if(GetMDBUserIDCount(db, szUserID[i]) == 0) //기존에 등록된 User가 없으면 insert
			{
				strTemp.Format("INSERT INTO User(UserID,Password,Name,Authority) VALUES ('%s', '%s', '%s',%d)",szUserID[i],szPassword[i],szName[i],nAuthority[i]);
				db.Execute(strTemp);
			}
			else //있으면 업데이트
			{

			}
		}
		catch (CDaoException* e)
		{
			TRACE("%s\n",e->m_pErrorInfo->m_strDescription);		
#ifdef _DEBUG
			strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
#else  //릴리즈 버전에서는 에러로 표시 하지 않는다.
			if(e->m_pErrorInfo->m_lErrorCode == 3380)
			{
				strTemp.Format("DB Update Result Code(%d) : 이미 DB업데이트 완료됨.",e->m_pErrorInfo->m_lErrorCode);
			}
			else
			{
				strTemp.Format("DB Update Result Code(%d)",e->m_pErrorInfo->m_lErrorCode);
			}		
#endif
			WriteLog(strTemp);

			e->Delete();
		}
	}	

	db.Close();		

	Sleep(300);


	return 1;
}

//ksj 20200120 : v1016 신규 채널 코드 SQL 쿼리 생성
#define UPDATE_CHANNEL_CODE_V1016_CNT 9
int CCTSMonProDoc::UpdateChannelcode_v1016()
{
	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	CString strTemp;
	CString strTableName;
	char szMsg[UPDATE_CHANNEL_CODE_V1016_CNT][256];
	char szDesc[UPDATE_CHANNEL_CODE_V1016_CNT][256];
	int naCode[UPDATE_CHANNEL_CODE_V1016_CNT]; 

/*	char szMsg[UPDATE_CHANNEL_CODE_V1016_CNT][256] = {
		//"정상", 
		"normal", //lyj 20200716 추후 수정필요.
		//"외부데이터 고정 상한", //246
		"External data fixed upper limit", //246
		//"외부데이터 고정 하한", //247
		"External data fixed lower limit", //247
		//"VENT 수동 오픈",//248
		"VENT manual open",//248
		//"외부 습도상한", //249
		"External humidity upper limit", //249
		//"외부 습도하한", //250
		"Low external humidity", //250
		//"외부 습도상한 종료", //68
		"External humidity upper limit", //68
		//"외부 습도하한 종료" //69
		"External low humidity end" //69
	}; 

	char szDesc[UPDATE_CHANNEL_CODE_V1016_CNT][256] = {
		//"정상 Cell(Code 20 ~ 79)",
		"Normal Cell (Code 20 ~ 79)", //lyj 20200716 
		//"비공정 및 공정 상태에서 외부데이터 고정 상한 검출", //246
		"External fixed upper limit detection in non-process and process conditions", //246
		//"비공정 및 공정 상태에서 외부데이터 고정 하한 검출", //247
		"Detecting lower limit of external data in non-process and process conditions", //247
		//"UI 화면의 VENT OPEN 버튼을 눌러 VENT를 수동 오픈", //248
		"Press the VENT OPEN button on the UI screen to manually open the VENT", //248
		//"외부데이터 설정의 습도상한 조건 감지", //249
		"Detecting upper humidity condition of external data setting", //249
		//"외부데이터 설정의 습도하한 조건 감지", //250
		"Detecting the lower humidity condition of external data setting", //250
		//"외부데이터 설정의 습도상한 조건에 의해 종료됨", //68
		"Exit due to external data setting humidity upper limit condition", //68
		//"외부데이터 설정의 습도상한 조건에 의해 종료됨" //69
		"Exit due to external data setting upper humidity limit condition" //69
		};

	int naCode[UPDATE_CHANNEL_CODE_V1016_CNT] =		{
		0,
		246, //246
		247,//247
		248, //248
		249, //249
		250, //250
		68, //68
		69 //69

	}; */	

	//언어별 테이블 선택
	/*switch(nSelLanguage) //20190701
	{
	case 1 : strTableName.Format("ChannelCode_ko"); break;
	case 2:  strTableName.Format("ChannelCode_en"); break;
	case 3 :  strTableName.Format("ChannelCode_pl"); break;
	default : strTableName.Format("ChannelCode_en"); break;
	}*/
	strTableName = GetChannelCodeTableLangName(); //ksj 20201012

	//ksj 20200804 : 언어별 업데이트 문자 선택
	switch(nSelLanguage)
	{
	case 1 : //kor
		{
			naCode[0] = 0; //code
			strcpy(szMsg[0],"정상"); //msg
			strcpy(szDesc[0],"정상 Cell(Code 20 ~ 79)"); //desc
			
			naCode[1] = 246; //code
			strcpy(szMsg[1],"외부데이터 고정 상한"); //msg
			strcpy(szDesc[1],"비공정 및 공정 상태에서 외부데이터 고정 상한 검출"); //desc

			naCode[2] = 247; //code
			strcpy(szMsg[2],"외부데이터 고정 하한"); //msg
			strcpy(szDesc[2],"비공정 및 공정 상태에서 외부데이터 고정 하한 검출"); //desc

			naCode[3] = 248; //code
			strcpy(szMsg[3],"VENT 수동 오픈"); //msg
			strcpy(szDesc[3],"UI 화면의 VENT OPEN 버튼을 눌러 VENT를 수동 오픈"); //desc

			naCode[4] = 249; //code
			strcpy(szMsg[4],"외부 습도상한"); //msg
			strcpy(szDesc[4],"외부데이터 설정의 습도상한 조건 감지"); //desc

			naCode[5] = 250; //code
			strcpy(szMsg[5],"외부 습도하한"); //msg
			strcpy(szDesc[5],"외부데이터 설정의 습도하한 조건 감지"); //desc

			naCode[6] = 68; //code
			strcpy(szMsg[6],"외부 습도상한 종료"); //msg
			strcpy(szDesc[6],"외부데이터 설정의 습도상한 조건에 의해 종료됨"); //desc

			naCode[7] = 69; //code
			strcpy(szMsg[7],"외부 습도하한 종료"); //msg
			strcpy(szDesc[7],"외부데이터 설정의 습도상한 조건에 의해 종료됨"); //desc		

			//ksj 20201012 : 챔버 연동 대기 채널코드가 없음. 자동 추가되도록 함.
			naCode[8] = 87; //code
			strcpy(szMsg[8],"챔버연동 대기"); //msg
			strcpy(szDesc[8],"챔버연동 대기"); //desc		

		}
		break;
	default : //기타 등등
		{
			naCode[0] = 0; //code
			strcpy(szMsg[0],"OK"); //msg
			strcpy(szDesc[0],"Normal Cell (Code 20 ~ 79)"); //desc

			naCode[1] = 246; //code
			strcpy(szMsg[1],"External data fixed upper limit"); //msg
			strcpy(szDesc[1],"External fixed upper limit detection in non-process and process conditions"); //desc

			naCode[2] = 247; //code
			strcpy(szMsg[2],"External data fixed lower limit"); //msg
			strcpy(szDesc[2],"Detecting lower limit of external data in non-process and process conditions"); //desc

			naCode[3] = 248; //code
			strcpy(szMsg[3],"VENT manual open"); //msg
			strcpy(szDesc[3],"Press the VENT OPEN button on the UI screen to manually open the VENT"); //desc

			naCode[4] = 249; //code
			strcpy(szMsg[4],"External humidity upper limit"); //msg
			strcpy(szDesc[4],"Detecting upper humidity condition of external data setting"); //desc

			naCode[5] = 250; //code
			strcpy(szMsg[5],"Low external humidity"); //msg
			strcpy(szDesc[5],"Detecting the lower humidity condition of external data setting"); //desc

			naCode[6] = 68; //code
			strcpy(szMsg[6],"External humidity upper limit"); //msg
			strcpy(szDesc[6],"Exit due to external data setting humidity upper limit condition"); //desc

			naCode[7] = 69; //code
			strcpy(szMsg[7],"External low humidity end"); //msg
			strcpy(szDesc[7],"Exit due to external data setting upper humidity limit condition"); //desc		

			//ksj 20201012 : 챔버 연동 대기 채널코드가 없음. 자동 추가되도록 함.
			naCode[8] = 87; //code
			strcpy(szMsg[8],"Chamber interlock standby"); //msg
			strcpy(szDesc[8],"Chamber interlock standby"); //desc	
		}
	}



	CDaoDatabase  db;	
	try
	{
		db.Open(GetCodeDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}

	int i = 0;
	for(i=0;i<UPDATE_CHANNEL_CODE_V1016_CNT;i++)
	{
		try
		{
			if(GetMDBChannelCodeCount(db, strTableName, naCode[i]) == 0) //기존에 등록된 코드가 없으면 insert
			{
				strTemp.Format("INSERT INTO %s(Code,Message,Description) VALUES (%d, '%s', '%s')",strTableName,naCode[i],szMsg[i],szDesc[i]);
				db.Execute(strTemp);
			}			
		}
		catch (CDaoException* e)
		{
			TRACE("%s\n",e->m_pErrorInfo->m_strDescription);		
#ifdef _DEBUG
			strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
#else  //릴리즈 버전에서는 에러로 표시 하지 않는다.
			if(e->m_pErrorInfo->m_lErrorCode == 3380)
			{
				//strTemp.Format("DB Update Result Code(%d) : 이미 DB업데이트 완료됨.",e->m_pErrorInfo->m_lErrorCode);
				strTemp.Format("DB Update Result Code(%d) : DB update is already completed.",e->m_pErrorInfo->m_lErrorCode); //lyj 20200716
			}
			else
			{
				strTemp.Format("DB Update Result Code(%d)",e->m_pErrorInfo->m_lErrorCode);
			}		
#endif
			WriteLog(strTemp);

			e->Delete();
		}
	}	

	db.Close();		

	Sleep(300);

	return 1;
}


//ksj 20200122 : Select 쿼리로 찾은 아이템 개수 리턴
int CCTSMonProDoc::GetMDBChannelCodeCount(CDaoDatabase& db, CString strTable, int nChannelCode)
{
	CString strSQL;
	CString strTemp;

	strSQL.Format("SELECT Code, Message, Description FROM %s WHERE Code=%d ORDER BY Code",strTable,nChannelCode);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	

	//쿼리에서 직접 카운트 값 얻을 수 있으나 시간 관계상 일반 select 후 개수 따로 카운팅.
	int nCount = 0;
	if(!rs.IsBOF() && !rs.IsEOF())
	{	
		while(!rs.IsEOF())
		{
			nCount++;
			rs.MoveNext();
		}
	}
	
	return nCount;
}

//ksj 20200122 : Select 쿼리로 찾은 UserID 개수 리턴
int CCTSMonProDoc::GetMDBUserIDCount(CDaoDatabase& db, CString strUserID)
{
	CString strSQL;
	CString strTemp;

	strSQL.Format("SELECT * FROM User WHERE UserID='%s' ORDER BY index",strUserID);


	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	

	//쿼리에서 직접 카운트 값 얻을 수 있으나 시간 관계상 일반 select 후 개수 따로 카운팅.
	int nCount = 0;
	if(!rs.IsBOF() && !rs.IsEOF())
	{	
		while(!rs.IsEOF())
		{
			nCount++;
			rs.MoveNext();
		}
	}

	return nCount;
}


// ksj 20200211 : MDB에 자동으로 습도센서 AuxH를 추가한다
int CCTSMonProDoc::UpdateMDBAuxH(void)
{
	//DB 오픈
	CString strTemp;
	CDaoDatabase  db;

	try
	{
		db.Open(GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		WriteLog(strTemp);
		e->Delete();
		db.Close();
		return 0;
	}

	//property에 AuxH가 있는지 스캔
	CString strSQL;

	strSQL.Format("SELECT Index, Name FROM Property WHERE Name='Humidity'");

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	int nCount = 0;
	int nPropertyIndex = 0;
	CStringArray strArray;
	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//없으면 추가하고 인덱스중 가장 큰값 +1로 설정

	//가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM Property");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nPropertyIndex = data.lVal+1;

		strArray.RemoveAll();
		strSQL.Format("INSERT INTO Property(Index, Name, UnitNotation, UnitTransfer) VALUES (%d, '%s', '%s', %f)"
						, nPropertyIndex, "Humidity", "%%", 1.0f);

		strArray.Add(strSQL);			
		//ExecuteQueryDB(strArray); //Property 에 Humidity 추가.
		ExecuteQueryDB(strArray, GetDataBaseName()); //Property 에 Humidity 추가. //ksj 20201012
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//AxisInfo에 AuxH가 있는지 스캔 
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='AuxH'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	//있으면 중단
	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//없으면 추가하고 인덱스 저장
	//가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM AxisInfo");

	int nAxisIndex = 0;
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nAxisIndex = data.lVal+1;

		strArray.RemoveAll();
		strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
			, nAxisIndex, "AuxH", "AuxH", nPropertyIndex);

		strArray.Add(strSQL);			
		//ExecuteQueryDB(strArray); //AxisInfo 에 AuxH 추가.
		ExecuteQueryDB(strArray, GetDataBaseName()); //Property 에 AuxH 추가. //ksj 20201012
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();
	//Time축 뒤에 인덱스 추가.
	strSQL.Format("SELECT YAxisIndexList FROM AxisInfo WHERE Index=0");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	//있으면 업데이트
	if(!rs.IsBOF() && !rs.IsEOF()) 
	{	
		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)
		{
			strTemp.Format("%s%d,", data.pbVal,nAxisIndex);

			strArray.RemoveAll();
			strSQL.Format("UPDATE AxisInfo SET YAxisIndexList='%s' WHERE Index=0",strTemp);
			strArray.Add(strSQL);
			//ExecuteQueryDB(strArray);
			ExecuteQueryDB(strArray, GetDataBaseName()); //ksj 20201012
		}		
	}	


	rs.Close();
	db.Close();

	return 0;
}

//lyj 20200520 : MDB에 자동으로 AxisInfo에 Sequence를 추가한다
int CCTSMonProDoc::UpdateMDBSequence(void)
{
	//DB 오픈
	CString strTemp;
	CDaoDatabase  db;

	try
	{
		db.Open(GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		WriteLog(strTemp);
		e->Delete();
		db.Close();
		return 0;
	}

	int nCount = 0;
	int nPropertyIndex = 0;
	int nAxisInfo = 0;
	CString strSQL, strYAxisTime;
	CStringArray strArray;

	//Property 테이블에 index 찾기
	strSQL.Format("SELECT Index, Name FROM Property WHERE Name='Index'");

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//없으면 추가하고 인덱스중 가장 큰값 +1로 설정

	//Property 가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM Property");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nPropertyIndex = data.lVal+1;

		strArray.RemoveAll();
		strSQL.Format("INSERT INTO Property(Index, Name, UnitNotation, UnitTransfer) VALUES (%d, '%s', '%s', %d)"
			, nPropertyIndex, "Index", "Index", 1);
		strArray.Add(strSQL);			
		//ExecuteQueryDB(strArray); //Property 에 Index 추가.
		ExecuteQueryDB(strArray, GetDataBaseName()); //ksj 20201012
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//AxisInfo에 Sequence가 있는지 스캔

	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='Sequence'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//없으면 추가하고 인덱스중 가장 큰값 +1로 설정

	//AxisInfo 가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM AxisInfo");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{
		data = rs.GetFieldValue(0);
		nAxisInfo = data.lVal+1;

		rs.Close();

		//Time의 YAxisIndexList 찾기
		strSQL.Empty();
		strSQL.Format("SELECT YAxisIndexList FROM AxisInfo WHERE Name='Time' ");

		try
		{
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			db.Close();
			return 0;
		}

		if(!rs.IsBOF() && !rs.IsEOF())
		{	
			data = rs.GetFieldValue(0);
			strYAxisTime.Format("%d,%s",0,data.bstrVal);

			strSQL.Empty();
			strArray.RemoveAll();
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, IsUsedForXAxis, YAxisIndexList, PropertyIndex) VALUES (%d, '%s', '%s',%d,'%s', %d)"
				, nAxisInfo, "Sequence", "Sequence", 1, strYAxisTime , nPropertyIndex);

			strArray.Add(strSQL);			
			//ExecuteQueryDB(strArray); 
			ExecuteQueryDB(strArray, GetDataBaseName()); //ksj 20201012
		}
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}
	rs.Close();




	rs.Close();
	db.Close();

	return 0;
}

int CCTSMonProDoc::UpdateMDBFlicker(void)  //lyj 20201005
{
	//DB 오픈
	CString strTemp , strSQL;
	CStringArray strArray;
	BOOL bresult = 0;


	// 	int ilanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	// 	switch (ilanguage)
	// 	{
	// 		case 1: strTemp.Format("%s","ChannelCode_ko");break; 
	// 		case 2: strTemp.Format("%s","ChannelCode_en");break;
	// 		case 3: strTemp.Format("%s","ChannelCode_pl");break;
	// 		default : strTemp.Format("%s","ChannelCode_ko");break;
	// 	}
	strTemp = GetChannelCodeTableLangName(); //ksj 20201012

	strArray.RemoveAll();
	strSQL.Format("ALTER TABLE %s ADD Color int ",strTemp);
	strArray.Add(strSQL);			
	strSQL.Format("ALTER TABLE %s ADD Priority int ",strTemp);
	strArray.Add(strSQL);			
	//strSQL.Format("ALTER TABLE %s ADD Use int ",strTemp);
	strSQL.Format("ALTER TABLE %s ADD ShowColorTable int ",strTemp);
	strArray.Add(strSQL);			
	strSQL.Format("ALTER TABLE %s ADD UseAutoBuzzerStop int ",strTemp);
	strArray.Add(strSQL);
	//bresult = ExecuteQueryDB(strArray);
	//strSQL.Format(	"UPDATE SystemConfig SET IP_Address = '%s', VSpec = '%s', ISpec = '%s', InstallChCount = %d, Data4 = '%s' WHERE ModuleID = %d", 
	strSQL.Format("UPDATE %s SET Color=%d, Priority=1, ShowColorTable=1, UseAutoBuzzerStop=0",strTemp,RGB(255,0,0)); //ksj 20201012 : 기본 값 지정
	strArray.Add(strSQL);	

	strSQL.Format("UPDATE %s SET Color=%d, Priority=2, ShowColorTable=1, UseAutoBuzzerStop=1 WHERE Code=27 or Code=87",strTemp,RGB(255,242,0)); //ksj 20201012 : 기본 값 지정. 사용자 일시정지, 챔버 연동 대기는 별도로 기본값 지정
	strArray.Add(strSQL);	

	bresult = ExecuteQueryDB(strArray, GetCodeDataBaseName(), FALSE); //ksj 20201012

	if(!bresult)
	{
		LoadFlickerTable();
	}

	return 0;
}
#endif

//ksj 20200122 : SBC에 ChAttribute 정보 요청 보냄.
int CCTSMonProDoc::SendChAttributeRequest(int nModuleID)
{
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	if(!pMD)
	{
		return FALSE;
	}

	int nRtn = pMD->SendCommand(SFT_CMD_CH_ATTRIBUTE_REQUEST,NULL,0);	
	
	return nRtn;
}

// 200313 HKH Memory Leak 수정
bool CCTSMonProDoc::FinishDocThread()
{
	int nCnt, nTimeOutCnt, nSleep;

	// ex) nTimeOutCnt == 50, nSleep == 100 -> 50 * 100ms = 5000ms TimeOut
	nTimeOutCnt = 20;
	nSleep = 100;

	// Thread Finish Flag On
	m_bFinishFileSave = true;
	m_bFinishSBCFileDown = true;
	m_bFinishLoader1 = true;
	m_bFinishLoader2 = true;

	// 1. SetChannelDataThread
	for (nCnt = 0; nCnt < nTimeOutCnt; nCnt++)
	{
		if (m_bFinishFileSaveAck)	break;

		Sleep(nSleep);
	}

	// 2. ThreadFtpDownload
	for (nCnt = 0; nCnt < nTimeOutCnt; nCnt++)
	{
		if (m_bFinishSBCFileDownAck)	break;
		
		Sleep(nSleep);
	}

	// 3. ChkLoaderState_1
	for (nCnt = 0; nCnt < nTimeOutCnt; nCnt++)
	{
		if (m_bFinishLoader1Ack)	break;

		Sleep(nSleep);
	}

	// 4. ChkLoaderState_2
	for (nCnt = 0; nCnt < nTimeOutCnt; nCnt++)
	{
		if (m_bFinishLoader2Ack)	break;

		Sleep(nSleep);
	}

	if (!m_bFinishFileSaveAck)		TRACE("Finish SetChannelDataThread Fail\n");
	if (!m_bFinishSBCFileDownAck)	TRACE("Finish ThreadFtpDownload Fail\n");
	if (!m_bFinishLoader1Ack)		TRACE("Finish ChkLoaderState_1 Fail\n");
	if (!m_bFinishLoader2Ack)		TRACE("Finish ChkLoaderState_2 Fail\n");

	return true;

}


int CCTSMonProDoc::LoadFlickerTable() //lyj 20201005
{
	CDaoDatabase  db;

	// Get Current Directory Path
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(NULL,szBuff,_MAX_PATH);

	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	CString strTableName = GetChannelCodeTableLangName(); //ksj 20201012

	try
	{
		db.Open(GetCodeDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		CString strSQL;
// 		switch(nSelLanguage) //20190701
// 		{
// 		case 1 : strSQL.Format("SELECT Code, Color, Priority FROM ChannelCode_ko WHERE Use =1 ORDER BY Code"); break;
// 		case 2:  strSQL.Format("SELECT Code, Color, Priority FROM ChannelCode_en WHERE Use =1 ORDER BY Code"); break;
// 		case 3 :  strSQL.Format("SELECT Code, Color, Priority FROM ChannelCode_pl WHERE Use =1 ORDER BY Code"); break;
// 		default : strSQL.Format("SELECT Code, Color, Priority FROM ChannelCode_ko WHERE Use =1 ORDER BY Code"); break;
// 		}
		strSQL.Format("SELECT Code, Color, Priority, UseAutoBuzzerStop FROM %s WHERE ShowColorTable = 1 ORDER BY Code", strTableName); //ksj 20201012

		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		FLICKER_INFO FlickerInfo;		
		
		m_csFlickerInfoList.Lock(); //ksj 20201013 : 크리티컬 섹션 추가
		m_FlickerInfoList.RemoveAll(); //ksj 20201012 : 초기화 추가

		while(!rs.IsEOF())
		{
			ZeroMemory(&FlickerInfo, sizeof(FLICKER_INFO));
			data = rs.GetFieldValue(0);
			FlickerInfo.nCode = data.iVal;

			data = rs.GetFieldValue(1);
			FlickerInfo.dwColor = data.ulVal;

			data = rs.GetFieldValue(2);			
			FlickerInfo.nPriority = data.iVal;

			data = rs.GetFieldValue(3);			 
			FlickerInfo.nAutoBuzzerStop = data.iVal; //ksj 20201013 : 부저 자동 중단 기능 추가.

			m_FlickerInfoList.AddTail(FlickerInfo);
			rs.MoveNext();
		}
		m_csFlickerInfoList.Unlock(); //ksj 20201013 : 크리티컬 섹션 추가
	}

	rs.Close();
	db.Close();

	return TRUE;
}

int CCTSMonProDoc::GetDirFilesNum(CString dirName)
{
    int count = 0;
    CFileFind finder;
    BOOL bWorking = finder.FindFile(dirName + "/*.*");
    while(bWorking)
    {
     bWorking = finder.FindNextFile();
	  if(finder.IsDots())
	  {
	 	 continue;
	  }
	 count++;
    }
     finder.Close();

	 return count;
}

//ksj 20180219 : FTP 다운로드시 캐시에서 가져오는 현상 제거 함수. //ksj 20200706
//윈도우즈 옵션을 강제 변경한다.
BOOL CCTSMonProDoc::SetFTPCache(void)
{

	HKEY hKey = NULL;
	TCHAR path[1000];
	TCHAR regSubKeyPath[1000] = _T("Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings");

	TCHAR valueName[256] = _T("SyncMode5"); //캐시 삭제 옵션
	DWORD byValue = 3; //페이지를 열때마다.

	if(RegCreateKeyEx(HKEY_CURRENT_USER, regSubKeyPath, 0, NULL,REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		::RegSetValueEx(hKey, valueName, 0, REG_DWORD, (BYTE*)&byValue, sizeof(DWORD));
		::RegCloseKey(hKey);  		

	}

	return TRUE;
}

//ksj 20200706 : 크래시 덤프 (미니덤프) 레지스트리를 시스템에 자동 등록한다.
//권한관련 문제인지/?? 레지스트리 키 값이 제대로 안먹힌다 수정 필요.
BOOL CCTSMonProDoc::SetWindowsMinidump(void)
{
	return FALSE; // 일단 비활성화


	HKEY hKey = NULL;
	char path[1000];
	char regSubKeyPath[1000] = _T("SOFTWARE\\Microsoft\\Windows\\Windows Error Reporting\\LocalDumps");

	char valueName[256] = _T("DumpFolder"); //덤프 경로 설정
	char strValue[256] = _T("%%SystemRoot%%\Minidump"); //c:\windows\Minidump //기본값.
	DWORD byValue = 0;

	if(RegCreateKeyEx(HKEY_LOCAL_MACHINE, regSubKeyPath, 0, NULL,REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		int nRtn = ::RegSetValueEx(hKey, valueName, 0, REG_EXPAND_SZ, (BYTE*)&strValue, sizeof(BYTE)*strlen(strValue));
		::RegCloseKey(hKey);  	

		TRACE("##%d\n",nRtn);
	}

	strncpy(valueName,"DumpCount",256);
	byValue = 20; //20개

	if(RegCreateKeyEx(HKEY_LOCAL_MACHINE, regSubKeyPath, 0, NULL,REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		int nRtn = ::RegSetValueEx(hKey, valueName, 0, REG_DWORD, (BYTE*)&byValue, sizeof(DWORD));
		::RegCloseKey(hKey); 

		TRACE("##%d\n",nRtn);
	}

	strncpy(valueName,"DumpType",256);
	byValue = 2;

	if(RegCreateKeyEx(HKEY_LOCAL_MACHINE, regSubKeyPath, 0, NULL,REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL) == ERROR_SUCCESS)
	{
		int nRtn = ::RegSetValueEx(hKey, valueName, 0, REG_DWORD, (BYTE*)&byValue, sizeof(DWORD));
		::RegCloseKey(hKey);  

		TRACE("##%d\n",nRtn);
	}

	return TRUE;
}

//ksj 20200819 : 자동 교정 기능 이식
BOOL CCTSMonProDoc::SetCalibrationStartEnd(int nModuleID, SFT_DAQ_ISOLATION * DAQCommandData)
{
	int nRtn = 0;
	CCyclerModule *pMD = GetModuleInfo(nModuleID);
	nRtn = pMD->SendCommand(SFT_CMD_CALI_START_END , DAQCommandData, sizeof(SFT_DAQ_ISOLATION));
	
	return nRtn;

}


//ksj 20201012 : 언어 설정에 맞는 채널코드 테이블 이름 리턴
CString CCTSMonProDoc::GetChannelCodeTableLangName(void)
{
	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	CString strTableName;

	//언어별 테이블 선택
	switch(nSelLanguage)
	{
	case 1 : strTableName.Format("ChannelCode_ko"); break;
	case 2:  strTableName.Format("ChannelCode_en"); break;
	case 3 :  strTableName.Format("ChannelCode_pl"); break;
	default : strTableName.Format("ChannelCode_en"); break;
	}

	return strTableName;
}


//ksj 20201012 : 데이터베이스 경로 초기화
int CCTSMonProDoc::InitDatabasePath(void)
{
	m_strDataBaseName = m_strCurPath + "\\DataBase\\" + PS_SCHEDULE_DATABASE_NAME;
	m_strCodeDataBaseName = m_strCurPath + "\\DataBase\\" + PS_CODE_DATABASE_NAME; //ksj 20200804
	m_strWorkInfoDataBaseName = m_strCurPath + "\\DataBase\\" + PS_WORKINFO_DATABASE_NAME;


	CString strTemp;

	CDaoDatabase  db;	
	try
	{
		db.Open(GetCodeDataBaseName()); //채널 코드 전용 DB("Pack_Code_2000.mdb")가 있으면 사용;
	}
	catch (CDaoException* e)
	{
		//없거나 문제 있으면 원래 데이터 베이스("Pack_Schedule_2000.mdb") 사용
		m_strCodeDataBaseName = m_strDataBaseName;
	}

	return 0;
}

//ksj 20200625 : 로컬 PC IP 리턴
CString CCTSMonProDoc::GetLocalIPAddress(void)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	char name[255];
	CString ip;
	PHOSTENT hostinfo;
	wVersionRequested = MAKEWORD( 2, 0);

	if( WSAStartup ( wVersionRequested, &wsaData ) == 0)
	{
		if(gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}			
		}	
		WSACleanup();
	}
	return ip;
}


//ljb 2011222 이재복 ////////// //ksj 20201005 : CTSMonProView.cpp 구현 함수를 CTSMonProDoc으로 복사.
CString CCTSMonProDoc::Fun_GetFunctionString(UINT nType, int nDivision)
{
	if (nDivision == 0) return "None [0]";	
	CString strItem;
	int i;
	//int j;
	if (nType == ID_CAN_TYPE)
	{
		for( int i=0; i < m_uiArryCanDivition.GetSize(); i++)
		{
			if (m_uiArryCanDivition.GetAt(i) == nDivision)
			{
				strItem.Format("%s [%d]", m_strArryCanName.GetAt(i), m_uiArryCanDivition.GetAt(i));
				return strItem;
			}
		}
		strItem.Format("Unknown [%d]", nDivision);
	}
	else if (nType == ID_AUX_TYPE)
	{
		for( int i=0; i < m_uiArryAuxDivition.GetSize(); i++)
		{
			if (m_uiArryAuxDivition.GetAt(i) == nDivision)
			{
				strItem.Format("%s [%d]", m_strArryAuxName.GetAt(i), m_uiArryAuxDivition.GetAt(i));
				return strItem;
			}
		}
		strItem.Format("Unknown [%d]", nDivision);
	}
	else if (nType == ID_AUX_THERMISTOR_TYPE)
	{
		if (nDivision == 1) strItem = "Th 1";
		if (nDivision == 2) strItem = "Th 2";
		if (nDivision == 3) strItem = "Th 3";
		if (nDivision == 4) strItem = "Th 4";
	}

	return strItem;
}

//ksj 20201023 : 종료 상황 안내창///////////////////////////////////////////////////////
void CCTSMonProDoc::SetAppExitProgress(int nPos)
{	
	CMainFrame* pMainFram = (CMainFrame*) AfxGetMainWnd();

	if(pMainFram)
		pMainFram->SetExitProgress(nPos);

}
////////////////////////////////////////////////////////////////////////////////////////

//ksj 20201115 : SYSTEMTIME 비교 함수 추가 (인터넷 발췌)
double CCTSMonProDoc::CompareSystemTime( PSYSTEMTIME pTargetTime, PSYSTEMTIME pCompareTime )
{
	tm tmTime1, tmTime2;
	time_t timeTime1, timeTime2;

	tmTime1.tm_sec = pTargetTime->wSecond;
	tmTime1.tm_min = pTargetTime->wMinute;
	tmTime1.tm_hour = pTargetTime->wHour;
	tmTime1.tm_mday = pTargetTime->wDay;
	tmTime1.tm_mon = pTargetTime->wMonth - 1;
	tmTime1.tm_year = pTargetTime->wYear - 1900;
	tmTime1.tm_isdst = 0;
	timeTime1 = ::mktime( &tmTime1 );



	tmTime2.tm_sec = pCompareTime->wSecond;
	tmTime2.tm_min = pCompareTime->wMinute;
	tmTime2.tm_hour = pCompareTime->wHour;
	tmTime2.tm_mday = pCompareTime->wDay;
	tmTime2.tm_mon = pCompareTime->wMonth - 1;
	tmTime2.tm_year = pCompareTime->wYear - 1900;
	tmTime2.tm_isdst = 0;
	timeTime2 = ::mktime( &tmTime2 );

	// time2와 time1의 second 차이를 double 형으로 리턴
	return ::difftime( timeTime1, timeTime2 );

}