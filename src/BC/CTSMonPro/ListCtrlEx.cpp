// ListCtrlEx.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ListCtrlEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx

CListCtrlEx::CListCtrlEx()
{
	m_StaticColumn = 0;
	memset(m_TypeList, 0, sizeof(int)*256);
}

CListCtrlEx::~CListCtrlEx()
{
	int Count=(int)m_StaticList.GetSize();
	for(int i=0;i<Count;i++)
	{
		CStatic* pControl=m_StaticList.GetAt(0);
		pControl->DestroyWindow();
		delete pControl;
		pControl = NULL;
		m_StaticList.RemoveAt(0);
	}
	m_StaticList.RemoveAll();
}


BEGIN_MESSAGE_MAP(CListCtrlEx, CListCtrl)
	//{{AFX_MSG_MAP(CListCtrlEx)
	ON_WM_CTLCOLOR_REFLECT()
		ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx message handlers



void CListCtrlEx::OnPaint()
{
	int Top=GetTopIndex();
	int Total=GetItemCount();
	int PerPage=GetCountPerPage();
	int LastItem=((Top+PerPage)>Total)?Total:Top+PerPage;

	// if the count in the list os nut zero delete all the progress controls and them procede
	{
		int Count=(int)m_StaticList.GetSize();
		for(int i=0;i<Count;i++)
		{
			CStatic* pControl=m_StaticList.GetAt(0);
			pControl->DestroyWindow();
			delete pControl;
			m_StaticList.RemoveAt(0);
		}
	}

	CHeaderCtrl* pHeader=GetHeaderCtrl();
	for(int i=Top;i<LastItem;i++)
	{
		CRect ColRt;
		pHeader->GetItemRect(m_StaticColumn,&ColRt);
		// get the rect
		CRect rt;
		GetItemRect(i,&rt,LVIR_LABEL);
		rt.top+=1;
		rt.bottom-=1;
		rt.left = 1;
		int Width=ColRt.Width();
		rt.right=rt.left+Width-4;
	

		CStatic* pControl=new CStatic();
		pControl->Create("", WS_CHILD|WS_VISIBLE|SS_BITMAP, rt, this, IDC_STATIC_LIST + i);

		if(m_TypeList[i] == 1)
			pControl->SetBitmap(bitmapHead);
		if( m_TypeList[i] == 2)
			pControl->SetBitmap(bitmapBody);
		if(m_TypeList[i] == 3)
			pControl->SetBitmap(bitmapTail);
		// create the progress control and set their position
	
	
		pControl->ShowWindow(SW_SHOWNORMAL);
		// add them to the list
		m_StaticList.Add(pControl);
	}
	CListCtrl::OnPaint();
}

HBRUSH CListCtrlEx::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	// TODO: Change any attributes of the DC here
	
	// TODO: Return a non-NULL brush if the parent's handler should not be called
	return NULL;
}

HBRUSH CListCtrlEx::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CListCtrl::OnCtlColor(pDC, pWnd, nCtlColor);
	
	for(int i = 0 ; i < GetItemCount(); i++)
	{
		if(pWnd->GetDlgCtrlID()==IDC_STATIC_LIST + i)
		{
			pDC->SetBkMode(TRANSPARENT);
			return (HBRUSH)::GetStockObject(NULL_BRUSH);
		}
	}
	
	
	// TODO: Return a different brush if the default is not desired
	return hbr;
}

void CListCtrlEx::SetHeadBitmap(UINT nBitmapID)
{
	bitmapHead.LoadBitmap(nBitmapID);
}

void CListCtrlEx::SetBodyBitmap(UINT nBitmapID)
{
	bitmapBody.LoadBitmap(nBitmapID);
}

void CListCtrlEx::SetTailBitmap(UINT nBitmapID)
{
	bitmapTail.LoadBitmap(nBitmapID);
}

BOOL CListCtrlEx::SetItem(const LVITEM *pItem, int nType)
{
	m_TypeList[pItem->iItem] = nType;
	return CListCtrl::SetItem(pItem);
}

BOOL CListCtrlEx::SetItem(const LVITEM *pItem)
{
	return CListCtrl::SetItem(pItem);
}

int CListCtrlEx::InsertItem(const LVITEM *pItem, int nType)
{
	m_TypeList[pItem->iItem] = nType;
	return CListCtrl::InsertItem(pItem);
}

int CListCtrlEx::InsertItem(const LVITEM *pItem)
{
	return CListCtrl::InsertItem(pItem);
}
