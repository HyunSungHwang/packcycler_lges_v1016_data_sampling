#if !defined(AFX_NOINPUTDLG_H__628C6240_8E88_4421_893B_FD475A15F1F3__INCLUDED_)
#define AFX_NOINPUTDLG_H__628C6240_8E88_4421_893B_FD475A15F1F3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NoInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNoInputDlg dialog

class CNoInputDlg : public CDialog
{
// Construction
public:
	
	CString m_strTitle;
	CNoInputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNoInputDlg)
	enum { IDD = IDD_NO_INSERT_DIALOG , IDD2 = IDD_NO_INSERT_DIALOG_ENG ,IDD3 = IDD_NO_INSERT_DIALOG_PL };
	int		m_nCycleNo;
	int		m_nStepNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNoInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNoInputDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NOINPUTDLG_H__628C6240_8E88_4421_893B_FD475A15F1F3__INCLUDED_)
