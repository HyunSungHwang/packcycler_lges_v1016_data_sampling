// fileRecord.h: interface for the CfileRecord class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILERECORD_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
#define AFX_FILERECORD_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class fileRecord  
{
public:
	fileRecord();
	virtual ~fileRecord();
	
	CStringArray m_rd_fd;
	CStringArray m_rd_data;
	CString m_strSplit;

	int onSetFD(CString strData,CString strSplit);
	int onSetData(CString strData);

	int GetFDNo(CString strFD);
	CString onGetData(CString strFD,int nRow);

protected:

};

#endif // !defined(AFX_FILERECORD_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
