// fileRecord.cpp: implementation of the CfileRecord class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "fileRecord.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

fileRecord::fileRecord()
{	
	m_rd_fd.RemoveAll();
	m_rd_data.RemoveAll();
}

fileRecord::~fileRecord()
{
	
}

int fileRecord::onSetFD(CString strData,CString strSplit)
{
	m_strSplit=strSplit;
	int nSplitCount=GStr::str_Count(strData,strSplit)+1;
	m_rd_fd.RemoveAll();
	m_rd_data.RemoveAll();

	for(int i=0;i<nSplitCount;i++)
	{
		CString strFD=GStr::str_GetSplit(strData,i,'.');
		if(strFD.IsEmpty() || strFD==_T("")) continue;

		m_rd_fd.Add(strFD);
	}
	return m_rd_fd.GetSize();
}

int fileRecord::onSetData(CString strData)
{
	m_rd_data.Add(strData);
	
	return m_rd_data.GetSize();		
}


int fileRecord::GetFDNo(CString strFD)
{
	int fdsize=m_rd_fd.GetSize();
	if(fdsize<1) return -1;
	
	for(int i=0;i<fdsize;i++)
	{
		CString fd=m_rd_fd.GetAt(i);
		if(strFD==fd) return i;
	}
	return -1;
}

CString fileRecord::onGetData(CString strFD,int nRow)
{
	int datasize=m_rd_data.GetSize();
	if(datasize<1)    return _T("");
	if(nRow>datasize) return _T("");

	int nFD=GetFDNo(strFD);
	if(nFD<0) return _T("");

	CString strData=m_rd_data.GetAt(nRow);
	int nSplitCount=GStr::str_Count(strData,m_strSplit);
	if(nFD>nSplitCount) return _T("");

	return GStr::str_GetSplit(strData,nFD,m_strSplit[0]);
}
