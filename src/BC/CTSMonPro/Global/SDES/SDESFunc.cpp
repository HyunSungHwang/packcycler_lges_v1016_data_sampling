#include "stdafx.h" 
 

#include ".\SDESFunc.h"




//////////////////////////////////////
CSDESFunc::CSDESFunc(void)
{
}

CSDESFunc::~CSDESFunc(void)
{
}


UINT CSDESFunc::ATOH(LPCTSTR String,int Len)
{	
	int Count = 0;
	UINT R = 0;
	while (*String)
	{
		if (*String >='0' && *String <='9') R = R * 16 + (int)(*String - 48);
		else
		if (*String >='a' && *String <='f') R = R * 16 + (int)(*String - 87);
		else
		if (*String >='A' && *String <='F') R = R * 16 + (int)(*String - 55);
		else 
		{
			String++;
			return R;
		}
		Count++;
		if (Count == Len) break;
		String++;
	}
	return R;
}


CString CSDESFunc::HEXTOSTR(BYTE* data, int nCount)
{
	CString st = _T("");
	CString str = _T("");
	for(int i = 0; i < nCount; i++)
	{
		st.Format(_T("%02X"), data[i]);
		str += st;
	}
	return str;
}

int CSDESFunc::STRTOHEX(CString str, int nBufSize, BYTE* Buf)
{
	int nCount = str.GetLength() / 2;
	if(nCount==0) return 0;
	if(nBufSize < nCount) return -1;
	
	CString temp;
	for(int i = 0; i < nCount; i++)
	{
		temp = str.Mid(i*2, 2);		
		Buf[i] = ATOH(temp, 2);
	}
	return nCount;
}

void CSDESFunc::MACTOPID(BYTE *sMAC, char *buffer)
{
	BYTE MAC[6];memcpy(&MAC, sMAC, sizeof(BYTE)*6);

	int i=0;
	for(int i = 0; i<6; i++)				//바이트당 문자를 앞뒤로 바꿈
	{									//AB => BA
		BYTE temp1, temp2;
		temp1 = (MAC[i] << 4);
		temp2 = (MAC[i] >> 4);
		MAC[i] = temp1 + temp2;

	}

	for(int i = 0; i<3; i++)					//전체 문자열을 바이트 단위로 뒤집기
	{									//01-32-54-76-98-BA => BA-98-76-54-32-01
		BYTE temp;
		temp = MAC[i];
		MAC[i] = MAC[5-i];
		MAC[5-i] = temp;
	}

	DWORD d1 = (MAC[0] << 16) | (MAC[1] << 8) | MAC[2];
	DWORD d2 = (MAC[3] << 16) | (MAC[4] << 8) | MAC[5];

	sprintf(buffer, "%08d-%08d", d1, d2);
}

void CSDESFunc::PIDTOMAC(BYTE *MAC, TCHAR *buffer)
{
	TCHAR *pdest;
	int  first, second, i=0;

	#ifdef _UNICODE	
		pdest = wcsstr(buffer, _T("-"));
	#else
		pdest = strstr(buffer, _T("-"));
	#endif	
	first = pdest - buffer;			//첫번째 스트링 길이

	while(buffer[i]!='\0')
		i++;
	second = i;						//두번째 스트링 길이
	
	CString str1, str2;
	for(int i = 0; i<first; i++)
		str1 += buffer[i];
	for(i=first+1; i<second; i++)
		str2 += buffer[i];

	DWORD s1 = _ttoi(str1);
	DWORD s2 = _ttoi(str2);

	for(int i = 0; i<3; i++)
		MAC[i] = (BYTE)((s1 >> (2-i)*8) & 0xFF);
	for(i=3; i<6; i++)
		MAC[i] = (BYTE)((s2 >> (5-i)*8) & 0xFF);

	for(int i = 0; i<6; i++)				//바이트당 문자를 앞뒤로 바꿈
	{									//AB => BA
		BYTE temp1, temp2;
		temp1 = (MAC[i] << 4);
		temp2 = (MAC[i] >> 4);
		MAC[i] = temp1 + temp2;
	}

	for(int i = 0; i<3; i++)					//전체 문자열을 바이트 단위로 뒤집기
	{									//01-32-54-76-98-BA => BA-98-76-54-32-01
		BYTE temp;
		temp = MAC[i];
		MAC[i] = MAC[5-i];
		MAC[5-i] = temp;
	}
}

void CSDESFunc::LShift5(BYTE *data)
{
	BYTE add = (*data & (0x01 << 4) ) ? 1 : 0;
	*data = *data << 1;
	*data |= add;

	*data = (*data & 0x1F);
}

void CSDESFunc::LShift1(BYTE *data)
{
	LShift5(data);
}

void CSDESFunc::LShift2(BYTE *data)
{
	LShift5(data);
	LShift5(data);
}


WORD CSDESFunc::ProcP10(WORD key)
{
	WORD outp10 = 0;
	int p10[10]={3,5,2,7,4,10,1,9,8,6};

	for( int i = 0; i < 10; i++)
	{
		WORD p1 = (0x0400 >> p10[i]);
		WORD p2 = (key & p1) ? (0x0200 >> i) : 0; 
		outp10 |= p2;
	}
	return outp10;
}


BYTE CSDESFunc::ProcP8(BYTE b1, BYTE b2)
{
	WORD key = b1 << 5 | b2;	
	BYTE outp8 = 0;
	int p8[8]={6,3,7,4,8,5,10,9};

	for( int i = 0; i < 8; i++)
	{
		WORD p1 = (0x0400 >> p8[i]);
		WORD p2 = (key & p1) ? (0x080 >> i) : 0; 
		outp8 |= p2;
	}
	return outp8;
}

BYTE CSDESFunc::ProcIP(BYTE key)
{
	BYTE outIP = 0;
	int IP[8]={2,6,3,1,4,8,5,7};

	for( int i = 0; i < 8; i++)
	{
		BYTE p1 = (0x80 >> (IP[i]-1));
		BYTE p2 = (key & p1) ? (0x80 >> i) : 0; 
		outIP |= p2;
	}
	return outIP;
}


BYTE CSDESFunc::ProcEP(BYTE key)
{
	BYTE outEP = 0;
	int EP[8]={4,1,2,3,2,3,4,1};
	for( int i = 0; i < 8; i++)
	{
		BYTE p1 = (0x80 >> (EP[i]+3));
		BYTE p2 = (key & p1) ? (0x80 >> i) : 0; 
		outEP |= p2;
	}
	return outEP;
}


BYTE CSDESFunc::ProcXOR(BYTE b1, BYTE b2)
{
	return b1^b2;
}


BYTE CSDESFunc::ProcS0(BYTE key)
{
	BYTE outS0 = 0;
	int S0[4][4]={1,0,3,2,3,2,1,0,0,2,1,3,3,1,3,2};
	int rows, cols;

	rows = (((key) & 0x08) >> 2) | ((key) & 0x01);
	cols = ((key >> 1) & 0x03);

	outS0 = S0[rows][cols];
	return outS0;
}


BYTE CSDESFunc::ProcS1(BYTE key)
{
	BYTE outS1 = 0;
	int S1[4][4]={0,1,2,3,2,0,1,3,3,0,1,0,2,1,0,3};
	int rows, cols;

	rows = (((key) & 0x08) >> 2) | ((key) & 0x01);
	cols = ((key >> 1) & 0x03);

	outS1 = S1[rows][cols];
	return outS1;
}



BYTE CSDESFunc::ProcP4(BYTE b1, BYTE b2)
{
	b1 = b1 << 2 | b2;
	BYTE outP4 = 0;
	int P4[4]={2,4,3,1};

	for( int i = 0; i < 4; i++)
	{
		BYTE p1 = (0x08 >> (P4[i]-1));
		BYTE p2 = (b1 & p1) ? (0x08 >> i) : 0; 
		outP4 |= p2;
	}

	return outP4;
}


BYTE CSDESFunc::ProcIPin(BYTE b1, BYTE b2)
{
	b1 = ((b1 << 4) | b2);
	BYTE outIPin = 0;
	int IPin[8]={4,1,3,5,7,2,8,6};

	for( int i = 0; i < 8; i++)
	{
		BYTE p1 = (0x80 >> (IPin[i]-1));
		BYTE p2 = (b1 & p1) ? (0x80 >> i) : 0; 
		outIPin |= p2;
	}

	return outIPin;
}

void CSDESFunc::MakeSubkey()
{
	WORD key = 0x1128;	//0x0282;	//키

	WORD outp10 = ProcP10(key);		//p10

	BYTE b1, b2;					// 5,5
	b1 = HI5BYTE(outp10);
	b2 = LO5BYTE(outp10);

	LShift1(&b1);					//LS-1
	LShift1(&b2);

	m_K1 = ProcP8(b1, b2);			//P8

	LShift2(&b1);					//LS-2
	LShift2(&b2);

	m_K2 = ProcP8(b1, b2);			//P8
}

//inside start
BYTE CSDESFunc::inside(BYTE b1, BYTE b2, BYTE key)
{ 
	BYTE outEP = ProcEP(b2);					//E/P
	
	BYTE outXORA = ProcXOR(outEP, key);			//XORA

	BYTE XORb1, XORb2;							// 4,4
	XORb1 = HI4BYTE(outXORA); // S1로 간다.
	XORb2 = LO4BYTE(outXORA); // S2로 간다.

	BYTE outS0 = ProcS0(XORb1);					//S0
	BYTE outS1 = ProcS1(XORb2);					//S1

	BYTE outP4 = ProcP4(outS0, outS1);			//P4

	BYTE outXORB = ProcXOR(b1, outP4);			//XORB

	return outXORB;
}
//inside end


BYTE CSDESFunc::encode(BYTE plaintext, BYTE key1, BYTE key2)
{
	BYTE outIP = ProcIP(plaintext);						//IP

	BYTE left, right;									// 4,4
	left = HI4BYTE(outIP);
	right = LO4BYTE(outIP);

	left = inside(left, right, key1);					//내부 루틴 반복
	right = inside(right, left, key2);
	
	BYTE outIPin = ProcIPin(right, left);

	return outIPin;
}

BYTE CSDESFunc::decode(BYTE ciphertext, BYTE key1, BYTE key2)
{
	BYTE outIP = ProcIP(ciphertext);					//IP

	BYTE left, right;									// 4,4
	left = HI4BYTE(outIP);
	right = LO4BYTE(outIP);

	left = inside(left, right, key2);					//내부 루틴 반복

	right = inside(right, left, key1);
	
	BYTE outIPin = ProcIPin(right, left);

	return outIPin;
}

void CSDESFunc::WholeEncode(int n, BYTE *plaintext, BYTE *ciphertext)
{
	MakeSubkey();			//기본키 K1, K2 생성
	BYTE key1 = m_K1;			//0xF2;
	BYTE key2 = m_K2;			//0xD1;
	CString str;

	for(int i=0; i<n; i++)
	{
		ciphertext[i] = encode( *(plaintext+i), key1, key2);	//암호문 생성
		key1 = plaintext[i];						//key1 값을 이전 평문으로 대체
		key2 = ciphertext[i];						//key2 값을 이전 암호문으로 대체
	}
}

void CSDESFunc::WholeDecode(int n, BYTE *ciphertext, BYTE *plaintext)
{
	MakeSubkey();			//기본키 K1, K2 생성
	BYTE key1 = m_K1;//0xF2;
	BYTE key2 = m_K2;//0xD1;
	CString str;

	for(int i=0; i<n; i++)
	{
		plaintext[i] = decode( *(ciphertext+i), key1, key2);	//평문 생성
		key1 = plaintext[i];						//key1 값을 이전 평문으로 대체
		key2 = ciphertext[i];						//key2 값을 이전 암호문으로 대체
	}
}

void CSDESFunc::Makeplaintext(int code1, int code2, BYTE *MAC, int year, int month, int date, int duration, char *key)
{
	BYTE plaintext[15] = {0x00};
	srand((unsigned)time(NULL));

	BYTE RND = (rand()%256);
	CString str;

	plaintext[0] = RND;
	plaintext[1] = code1;
	plaintext[2] = code2;
	plaintext[3] = (year>>8) & 0xFF;
	plaintext[4] = year & 0xFF;
	plaintext[5] = month;
	plaintext[6] = date;
	plaintext[7] = (duration>>8) & 0xFF;
	plaintext[8] = duration & 0xFF;
	plaintext[9] = MAC[0];
	plaintext[10] = MAC[1];
	plaintext[11] = MAC[2];
	plaintext[12] = MAC[3];
	plaintext[13] = MAC[4];
	plaintext[14] = MAC[5];

	str = HEXTOSTR(plaintext, 15);

	BYTE ciphertext[15]={0x00};
	WholeEncode(15, plaintext, ciphertext);
	
	str = HEXTOSTR(ciphertext, 15);
	str = deviden(str, 5);
		
	#ifdef _UNICODE			
		int isize = WideCharToMultiByte(CP_ACP, 0, str, -1, 0, 0, 0, 0);			 	
		int iret=WideCharToMultiByte(CP_ACP,0,str,str.GetLength(),key,isize,NULL,NULL);		
	#else
		strcpy(key, str);
	#endif
}

CString CSDESFunc::deviden(CString str, int n)
{
	int nLength = str.GetLength();
	for(int i=nLength; i>n; i-=n)		//n자리 마다 - 찍는 부분
	{
		str.Insert(i-n,_T("-"));
	}
	TRACE("%s\n", str);
	return str;
}



CString CSDESFunc::INSERTSTRING(CString str, CString sub, int nStep)
{
	if(nStep <= 0) return _T("");

	int nLength=str.GetLength();
	int j=0;
	int k=0;
	for(j=nLength;j>nStep;j-=nStep)//nStep자리 마다 문자 찍는 부분
	{
		str.Insert(j-nStep,sub);
	}

	return str;
}


BOOL CSDESFunc::PID2MAC(CString strPID, CString &strMAC)
{
	strMAC = _T("");
	
	int n = strPID.Find(_T("-"));
	int m1 = _ttoi(strPID.Left(n));
	int m2 = _ttoi(strPID.Mid(n+1));

	if(strPID.GetLength() != 17 || m1 > 16777215 || m2 > 16777215)
	{
		AfxMessageBox(_T("ProductID 값이 잘못되었습니다."));
		return FALSE;
	}

	TCHAR szPID[36];
	BYTE mac[6];
		
	_tcscpy(szPID, strPID);
		
	PIDTOMAC(mac, szPID);
	
	strMAC = HEXTOSTR(mac, 6);
	
	strMAC = INSERTSTRING(strMAC, _T("-"), 2);

	return TRUE;
}

BOOL CSDESFunc::MAC2PID(CString strMAC, CString &strPID)
{
	strPID = _T("");

	char szPID[256];//="12228726-05517825";
	BYTE mac[6];

	strMAC.Replace(_T("-"),_T(""));
	STRTOHEX(strMAC, 6, mac);

	MACTOPID(mac, szPID);
	strPID = szPID;

	return TRUE;

}

CString CSDESFunc::GenerateCDkey(int nType,int nCode,CString strMac,CString strProductID,
						         CTime RegularTime,
								 CTime TrialStartTime,CTime TrialEndTime) 
{
	//제품구분
	//인증방식
	
	int nDuration = 0;	
	int nYear  = RegularTime.GetYear();//키생성년
	int nMonth = RegularTime.GetMonth();//키생성월
	int nDay   = RegularTime.GetDay();//키생성일
	if(nYear<1)  return _T("");
	if(nMonth<1) return _T("");
	if(nDay<1)   return _T("");

	switch(nCode)
	{
		case 10: //인증방식:정품
		{							
			nDuration = 0;
			break;
		}	
		case 11: //인증방식:날짜평가		
		{											
			CTimeSpan ts = TrialEndTime - TrialStartTime;
			nDuration = (int)ts.GetDays() + 1;			
			if(nDuration<1) return _T("");
			break;
		}
		default : 
		{
			return _T("");
		}
	}

	CString strCDKey=_T("");
	CSDESFunc sf;	
	if(!sf.CreateCDKEY(nType, nCode, strMac, nYear, nMonth, nDay, nDuration, strCDKey))	
	{	  
	  return _T("");
	}
	return strCDKey;	
}

BOOL CSDESFunc::CreateCDKEY(int nCode1, int nCode2, CString strMAC
							, int nYear, int nMonth, int nDay, int nDuration
							, CString &strCDKEY)
{
	BYTE mac[6];
	strMAC.Replace(_T("-"),_T(""));
	
	STRTOHEX(strMAC, 6, mac);

	char key[36];
	Makeplaintext(nCode1, nCode2, mac, nYear, nMonth, nDay, nDuration, key);

	strCDKEY = key;
	return TRUE;
}

void CSDESFunc::GetEvalutionMac(CStringArray &MacData)
{
	MacData.Add(_T("AC-EB-DA-8D-4C-DD")); //평가판
	MacData.Add(_T("EB-AC-4C-9F-DA-8D")); //평가판
	MacData.Add(_T("7A-EB-A0-8D-3C-99")); //평가판
	MacData.Add(_T("AC-E0-DA-3C-B5-BB")); //평가판
	MacData.Add(_T("3C-E0-DA-AC-B5-9F")); //평가판
	MacData.Add(_T("0A-E0-3A-EE-B5-8F")); //평가판
}

BOOL CSDESFunc::ChkEvalutionMac(CStringArray &MacData,CString mac)
{	
	if(MacData.GetSize()<1)          return FALSE;
	if(mac.IsEmpty() || mac==_T("")) return FALSE;

	for(int i=0;i<MacData.GetSize();i++)
	{
		CString tmac=MacData.GetAt(i);
		tmac.Replace(_T("-"),_T(""));	

		if(tmac==mac) return TRUE;		
	}	
	return FALSE;
}

void CSDESFunc::SetCDKeyInit()
{
	m_CDKey.bFlag=FALSE;
	m_CDKey.scdkey=_T("");
	m_CDKey.smac=_T("");
	m_CDKey.type=0;
	m_CDKey.code=0;
	m_CDKey.year=0;
	m_CDKey.month=0;
	m_CDKey.day=0;
	m_CDKey.duration=0;
	for(int i=0;i<6;i++) m_CDKey.mac[i]=0x00;
	
	m_CDKey.period=_T("");
	m_CDKey.dtcreate=_T("");
	m_CDKey.setc=_T("");
}

CString CSDESFunc::GetType(int iType)
{
	if(iType==100)      return _T("dual monitor");
	else if(iType==101) return _T("multi monitor");	
	else if(iType==102) return _T("광주화장장");

	return _T("");
}

int CSDESFunc::GetType(CComboBox *cbo)
{
	int nType = cbo->GetCurSel();

	if(nType==0)      return 100;//dualmonitor
	else if(nType==1) return 101;//multimonitor
	else if(nType==2) return 102;//광주화장장

	return -1;
}

CString CSDESFunc::GetCode(int iCode)
{
	if(iCode==10)      return _T("정품");
	else if(iCode==11) return _T("평가판(날짜)");
	
	return _T("");
}

BOOL CSDESFunc::ChkCDKey(int iType,CString CDKey,CString Mac)
{		
	SetCDKeyInit();	
	
	if(Mac.IsEmpty() || Mac==_T(""))       return FALSE;			
	if(CDKey.IsEmpty() || CDKey==_T(""))   return FALSE;		
	if(CDKey.GetLength()!=35)              return FALSE;
	
	m_CDKey.scdkey=CDKey;
	m_CDKey.smac=Mac;	

	BYTE byteMacAddress[256] = {0x00};	
	::memcpy(byteMacAddress,GStr::str_MacByteA(Mac),6);
	::memcpy(m_CDKey.mac,byteMacAddress,6); 

	CString plain = CDKey;//암호문 -> 평문 으로 복호화하기
	plain.Replace(_T("-"), _T(""));

	BYTE plaintext[15]={0x00};
	BYTE ciphertext[15]={0x00};
	if(plain.GetLength() != 30)
	{
		m_CDKey.setc.Format(_T("CDKey Encodeing Error."));
		return FALSE;
	}
	
	STRTOHEX(plain, 15, ciphertext);	
	WholeDecode(15, ciphertext, plaintext);
	
	BYTE nType, nCode;
	int year, month, date, duration;
	nType=plaintext[1];//제품종류
	nCode=plaintext[2];//구분(정품,평가)
	year=(plaintext[3] << 8) | plaintext[4];
	month=plaintext[5];
	date=plaintext[6];
	duration=(plaintext[7] << 8) | plaintext[8];
		
	m_CDKey.type=nType;
	m_CDKey.code=nCode;
	m_CDKey.year=year;
	m_CDKey.month=month;
	m_CDKey.day=date;
	m_CDKey.duration=duration;

	BYTE mac[6];
	memcpy(&mac, &plaintext[9], sizeof(BYTE)*6);
	CString strdecMac=HEXTOSTR(mac,6);

	//제품 
	if(nType!=iType)
	{
		m_CDKey.setc.Format(_T("Product Type Error."));
		return FALSE;
	}
	
	CString strMac=Mac;
	strMac.Replace(_T("-"),_T(""));

	if(nCode==10) 
	{//정품
		 if(strMac!=strdecMac)
		 {//Mac Error
			 m_CDKey.setc.Format(_T("Mac Address Error."));
			 return FALSE;
		 }		 
		 return TRUE;
	}
    else if(nCode==11) 
	{//평가판	
		if(strMac!=strdecMac)
		{//Mac Error			 
			CStringArray MacData;
			GetEvalutionMac(MacData);
			BOOL bflag=ChkEvalutionMac(MacData,strdecMac);
			if(bflag==FALSE)
			{//Mac Error
				 m_CDKey.setc.Format(_T("Mac Address Error."));
				 return FALSE;
			}
		}
	
		//time server	
		CSNTPClient sntp;
		SYSTEMTIME st;
		if(sntp.GetKorTime(st)==FALSE)
		{			
			m_CDKey.setc.Format(_T("Time Server Error."));
			return FALSE;
		}
		
		if( (1980 <= year && year <=2099)
			&& (1 <= month && month<= 12)
			&& (1 <= date && date<= 31)
			&& (0 <= duration && duration<= 0xFFFF)	)	
		{
			COleDateTime dtS(year, month, date,0,0,0), dtN(st), dtE;//duration 계산
			COleDateTimeSpan sp;
			sp.SetDateTimeSpan(duration-1,0,0,0);
			dtE = dtS+sp;
			sp = dtE - dtN;
			int pday = sp.GetDays() + 1;

			m_CDKey.dtcreate.Format(_T("%d-%d-%d"),
				                   dtS.GetYear(), dtS.GetMonth(), dtS.GetDay());

			m_CDKey.period.Format(_T("%d-%d-%d ~ %d-%d-%d"),
				                   dtS.GetYear(), dtS.GetMonth(), dtS.GetDay(), 
								   dtE.GetYear(), dtE.GetMonth(), dtE.GetDay());

			if(pday > 0)
			{					
				m_CDKey.setc.Format(_T("There are %d days to go.\r\n(%d-%d-%d ~ %d-%d-%d)"), pday,
													dtS.GetYear(), dtS.GetMonth(), dtS.GetDay(), 
											        dtE.GetYear(), dtE.GetMonth(), dtE.GetDay());				
				return TRUE;
			}
			else
			{	
				m_CDKey.setc.Format(_T("Authentication date has been expired."));				
				return FALSE;
			}						
		}
	}
	return FALSE;	
}