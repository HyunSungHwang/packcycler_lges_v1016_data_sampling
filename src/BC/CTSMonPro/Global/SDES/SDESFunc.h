#pragma once

#include <nb30.h>
#pragma comment (lib, "netapi32") //라이브러리 추가

#define HI5BYTE(w)   ((BYTE) (((WORD) (w) >> 5) & 0x1F)) 
#define LO5BYTE(w)   ((BYTE) (((WORD) (w) >> 0) & 0x1F)) 
#define HI4BYTE(w)   ((BYTE) (((BYTE) (w) >> 4) & 0x0F)) 
#define LO4BYTE(w)   ((BYTE) (((BYTE) (w) >> 0) & 0x0F)) 

typedef struct tagCDKEY
{
	BOOL bFlag;
	CString scdkey;
	CString smac;
	BYTE type;
	BYTE code;
	BYTE mac[6];
	int year;
	int month;
	int day;
	int duration;

	CString dtcreate;
	CString period;
	CString setc;

} S_CDKEY, *LPS_CDKEY;

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
class CSDESFunc
{
public:

	BYTE    m_K1, m_K2;
	S_CDKEY m_CDKey;

	void    SetCDKeyInit();
	CString GetType(int iType);
	int     GetType(CComboBox *cbo);
	CString GetCode(int iCode);
	

	void GetEvalutionMac(CStringArray &MacData);
    BOOL ChkEvalutionMac(CStringArray &MacData,CString mac);

	BOOL ChkCDKey(int iType,CString CDKey,CString Mac);

	CString GenerateCDkey(int nType,int nCode,CString strMac,CString strProductID,
				         CTime RegularTime,
						 CTime TrialStartTime,CTime TrialEndTime); 
	BOOL CreateCDKEY(int nCode1, int nCode2, CString strMAC, 
		             int nYear, int nMonth, int nDay, int nDuration, 
					 CString &strCDKEY);

	CString INSERTSTRING(CString str, CString sub, int nStep);
	BOOL    PID2MAC(CString strPID, CString &strMAC);
	BOOL    MAC2PID(CString strMAC, CString &strPID);
	void	Makeplaintext(int code1, int code2, BYTE *MAC, int year, int month, int date, int duration, char *key);
	CString deviden(CString str, int n);

	UINT	ATOH (LPCTSTR String,int Len);	
	CString HEXTOSTR(BYTE* data, int nCount);
	void    LShift5(BYTE *data);
	void    LShift1(BYTE *data);
	void    LShift2(BYTE *data);
	WORD    ProcP10(WORD key);
	BYTE    ProcP8(BYTE b1, BYTE b2);
	BYTE    ProcIP(BYTE key);
	BYTE    ProcEP(BYTE key);
	BYTE    ProcXOR(BYTE b1, BYTE b2);
	BYTE    ProcS0(BYTE key);
	BYTE    ProcS1(BYTE key);
	BYTE    ProcP4(BYTE b1, BYTE b2);
	BYTE    ProcIPin(BYTE b1, BYTE b2);
	void    MakeSubkey();
	BYTE    inside(BYTE b1, BYTE b2, BYTE key);
	BYTE    encode(BYTE plaintext, BYTE key1, BYTE key2);
	void    WholeEncode(int n, BYTE *plaintext, BYTE *ciphertext);
	void    MakeCiphertext(char *key, char* plain);
	BYTE    decode(BYTE ciphertext, BYTE key1, BYTE key2);
	void	PWDecode(char *buffer, int nSize);
	void	PWEncode(char *buffer, int nSize);
	void	MACTOPID(BYTE *MAC, char *buffer);
	void	PIDTOMAC(BYTE *MAC, TCHAR *buffer);	
	int		STRTOHEX(CString str, int nBufSize, BYTE* Buf);
	void	WholeDecode(int n, BYTE *ciphertext, BYTE *plaintext);
	
	CSDESFunc(void);
	~CSDESFunc(void);
};
