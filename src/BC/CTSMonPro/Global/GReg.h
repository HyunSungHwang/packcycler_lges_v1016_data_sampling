
//#include <shlwapi.h>
//#include <atlbase.h>

//#include <intshcut.h>

#define HKLM	HKEY_LOCAL_MACHINE 
#define HKCU	HKEY_CURRENT_USER
#define HKCR	HKEY_CLASSES_ROOT
#define HBHO    _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects")

#pragma comment(lib, "shlwapi.lib")

class GReg
{
public:   
	GReg();
    ~GReg();
    
	/*static CString reg_GetRegStr(CWinApp *winApp,CString session,CString entry,CString defalt)
	{			
		CString strBuff=winApp->GetProfileString(session, entry, defalt);		

		return strBuff;
	}

	static void reg_SetRegStr(CWinApp *winApp,CString session,CString entry,CString svalue)
	{			
		winApp->WriteProfileString(session, entry, svalue);		
	}

	static HKEY reg_CreateKey(HKEY hKey,CString skey) 
	{
		HKEY phKey=reg_GetKey(hKey,skey);
		if(phKey) return phKey;

        DWORD dwDisp;        
		DWORD Result=::RegCreateKeyEx(hKey,
			                          skey,
									  0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&phKey,&dwDisp);
        if(Result!=ERROR_SUCCESS) return NULL;

		return phKey;

		//CRegKey h;
		//h.Create(hKey,skey);		
		//return h.Detach();		
	} 
	
	static HKEY reg_GetKey(HKEY hKey,CString skey)
	{
		HKEY Key;		
		DWORD Result=::RegOpenKey(hKey, //HKEY_CURRENT_USER,HKEY_LOCAL_MACHINE,
								  skey,&Key);
    
		if(Result!=ERROR_SUCCESS) return NULL;

		return Key;
	}
	
	static BOOL reg_ChkKey(HKEY hKey,CString skey,CString stype,BOOL bClose=FALSE)
	{
		HKEY phKey=reg_GetKey(hKey,skey);
		if(phKey==NULL) return FALSE;

		DWORD size=1024;
		DWORD type=0;
		#ifdef _UNICODE			   			
			wchar_t  data[1024]={0};
		#else
		    char  data[1024]={0};		
		#endif

	    BOOL bflag=TRUE;
		DWORD Result=::SHQueryValueEx(phKey, stype, NULL, &type, (LPBYTE)data, &size);	
		if(Result!=ERROR_SUCCESS) bflag=FALSE;
		
		if(bClose==TRUE) RegCloseKey(phKey);	
		return bflag;
	}

	static CString reg_GetStrValue(HKEY hKey,CString stype,BOOL bClose=FALSE)
	{//String read
		ULONG size = 1024;
		DWORD type=REG_SZ;

		#ifdef _UNICODE			   			
			wchar_t  data[1024]={0};
		#else
		    char  data[1024]={0};		
		#endif
	
		DWORD Result=::SHQueryValueEx(hKey, stype, NULL, &type, (LPBYTE)&data, &size);	
		if(Result!=ERROR_SUCCESS) return _T(""); 
	
		if(bClose==TRUE) RegCloseKey(hKey);		
		//RegCloseKey�ϸ� �ȉ�
		return data;
	} 

	static CString reg_GetStrValue(HKEY hKey,CString sSubKey,CString stype,BOOL bClose=FALSE)
	{//String read		
		HKEY phKey=reg_GetKey(hKey,sSubKey);	
		if(phKey==NULL) return _T("");
		
		CString value=reg_GetStrValue(phKey,stype,FALSE); 				
		//RegCloseKey�ϸ� �ȉ�
		return value;
	}
	
	static BOOL reg_SetStrValue(HKEY hKey,CString stype,CString data,BOOL bClose=FALSE)
	{//String save
		DWORD type=REG_SZ;

		#ifdef _UNICODE			   			
			DWORD size = sizeof(TCHAR)*(_tcslen(data)+1);
		#else
		    DWORD size = data.GetLength();
		#endif
		
		DWORD Result=::RegSetValueEx(hKey,stype,NULL,type,(CONST BYTE*)(LPCTSTR)data,size);;
		if(Result!=ERROR_SUCCESS) return FALSE; 
		
		if(bClose==TRUE) RegCloseKey(hKey);
		//RegCloseKey�ϸ� �ȉ�
		return TRUE;
	}

	static BOOL reg_SetEStrValue(HKEY hKey,CString stype,CString data,BOOL bClose=FALSE)
	{//String save
		DWORD type=REG_EXPAND_SZ;

		#ifdef _UNICODE			   			
			DWORD size = sizeof(TCHAR)*(_tcslen(data)+1);
		#else
		    DWORD size = data.GetLength();
		#endif
	
		DWORD Result=::RegSetValueEx(hKey,stype,NULL,type,(CONST BYTE*)(LPCTSTR)data,size);  
		if(Result!=ERROR_SUCCESS) return FALSE; 
				
		if(bClose==TRUE) RegCloseKey(hKey);//RegCloseKey not use	
		return TRUE;
	}

	static BOOL reg_SetEStrValue(HKEY hKey,CString sSubKey,CString stype,CString value,BOOL bClose=FALSE)
	{//Expand String save	
		HKEY phKey=reg_GetKey(hKey,sSubKey);
	
		if(phKey==NULL) phKey=reg_CreateKey(hKey,sSubKey);
		if(phKey==NULL) return FALSE;

		return reg_SetEStrValue(phKey,stype,value,bClose); 
		//RegCloseKey not use	
	}

	static BOOL reg_SetStrValue(HKEY hKey,CString sSubKey,CString stype,CString value,BOOL bClose=FALSE)
	{//String save		
		HKEY phKey=reg_GetKey(hKey,sSubKey);
	
		if(phKey==NULL) phKey=reg_CreateKey(hKey,sSubKey);
		if(phKey==NULL) return FALSE;
		
		BOOL bflag=reg_SetStrValue(phKey,stype,value,bClose); 
		//RegCloseKey not use			
		return bflag;
	}

	static DWORD reg_GetDwordValue(HKEY hKey,CString stype,BOOL bClose=FALSE)
	{//DWORD Read
		DWORD data=0;
		DWORD size=sizeof(data); 
		DWORD type=REG_DWORD;

		DWORD Result=::SHQueryValueEx(hKey, stype, NULL, &type, (LPVOID)&data, &size);  
		if(Result!=ERROR_SUCCESS) return -1; 
	
		if(bClose==TRUE) RegCloseKey(hKey); //RegCloseKey not use		
		return data;
	}
	static DWORD reg_GetDwordValue(HKEY hKey,CString sSubKey,CString stype,BOOL bClose=FALSE)
	{//DWORD read		
		HKEY phKey=reg_GetKey(hKey,sSubKey);
	
		if(phKey==NULL) phKey=reg_CreateKey(hKey,sSubKey);
		if(phKey==NULL) return FALSE;

		return reg_GetDwordValue(phKey,stype,bClose); 
		//RegCloseKey not use		
	}

	static BOOL reg_SetDwordValue(HKEY hKey,CString stype,DWORD data,BOOL bClose=FALSE)
	{//DWORD save
		DWORD size=sizeof(data); 
		DWORD type=REG_DWORD;

		DWORD Result=::RegSetValueEx(hKey,stype,NULL,type,(LPBYTE)&data,sizeof(data));  
		if(Result!=ERROR_SUCCESS) return FALSE; 
		
		if(bClose==TRUE) RegCloseKey(hKey); //RegCloseKey not use	
		return TRUE;
	}

	static BOOL reg_SetDwordValue(HKEY hKey,CString sSubKey,CString stype,DWORD data,BOOL bClose=FALSE)
	{//DWORD save
		HKEY phKey=reg_GetKey(hKey,sSubKey);
	
		if(phKey==NULL) phKey=reg_CreateKey(hKey,sSubKey);
		if(phKey==NULL) return FALSE;

		return reg_SetDwordValue(phKey,stype,data,bClose); 		
		//RegCloseKey not use			
	}

	static CString reg_GetBinValue(HKEY hKey,CString stype,BOOL bClose=FALSE)
	{//Binary read
		DWORD size=1024; 
		DWORD type=REG_BINARY;					
		BYTE  data[1024]={0};		
				
		DWORD Result=::SHQueryValueEx(hKey, stype, NULL, &type, (LPBYTE)data, &size);  
		if(Result!=ERROR_SUCCESS) return _T(""); 
			
		if(bClose==TRUE) RegCloseKey(hKey); //RegCloseKey not use		
		if(size<1) return _T("");

		CString str=_T("");		
		
		for(int i=0;i<(int)size;i++)
		{
			CString tmp;
			tmp.Format(_T("%d,"),data[i]);
		    str=str+tmp;
		}		
		return str;
	}

	static CString reg_GetBinValue(HKEY hKey,CString sSubKey,CString stype,BOOL bClose=FALSE)
	{//Binary read
		HKEY phKey=reg_GetKey(hKey,sSubKey);
	
		if(phKey==NULL) phKey=reg_CreateKey(hKey,sSubKey);		
		if(phKey==NULL) return _T("");

		return reg_GetBinValue(phKey,stype,bClose); 
		//RegCloseKey not use		
	}

	static BOOL reg_SetBinValue(HKEY hKey,CString stype,BYTE *data,BOOL bClose=FALSE)
	{//Binary save		
		DWORD size=strlen((char*)data); //sizeof(data); 
		DWORD type=REG_BINARY;
		
		DWORD Result=::RegSetValueEx(hKey,stype,NULL,type,(LPBYTE)data,size);  
		if(Result!=ERROR_SUCCESS) return FALSE; 
		
		if(bClose==TRUE) RegCloseKey(hKey); //RegCloseKey not use	
		return TRUE;
	}

	static BOOL reg_SetBinValue(HKEY hKey,CString sSubKey,CString stype,BYTE *data,BOOL bClose=FALSE)
	{//Binary save
		HKEY phKey=reg_GetKey(hKey,sSubKey);
	
		if(phKey==NULL) phKey=reg_CreateKey(hKey,sSubKey);
		if(phKey==NULL) return FALSE;

		return reg_SetBinValue(phKey,stype,data,bClose); 		
		//RegCloseKey not use	
	}

	static BOOL reg_SetMultiValue(HKEY hKey,CString stype,CString value,BOOL bClose=FALSE)
	{//Multi String save
		DWORD size=value.GetLength(); 
		DWORD type=REG_MULTI_SZ;

		unsigned char keyname[600];
		memset(keyname,0,sizeof(keyname));
		memcpy(keyname,value,size);
		keyname[++size]=0;
		keyname[++size]=0;

		DWORD Result=::RegSetValueEx(hKey,stype,NULL,type,keyname,size);
		if(Result!=ERROR_SUCCESS) return FALSE; 
		
		if(bClose==TRUE) RegCloseKey(hKey); //RegCloseKey not use	
		return TRUE;
	}

	static BOOL reg_SetMultiValue(HKEY hKey,CString sSubKey,CString stype,CString value,BOOL bClose=FALSE)
	{//Multi String save		
		HKEY phKey=reg_GetKey(hKey,sSubKey);
	
		if(phKey==NULL) phKey=reg_CreateKey(hKey,sSubKey);
		if(phKey==NULL) return FALSE;

		return reg_SetMultiValue(phKey,stype,value,bClose); 		
		//RegCloseKey not use	
	}

    ///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	static CString reg_GetDllFilePath(CString sclassid)
	{//Ŭ���� ���̵��� �н� ���ϱ�
		if(sclassid==_T("")) return _T("");
			
		HKEY subhKey=reg_GetKey(HKEY_CLASSES_ROOT, _T("CLSID\\")+sclassid+_T("\\InprocServer32") );
		if(subhKey==NULL) return _T("");

		CString data=reg_GetStrValue(subhKey,_T(""));
		::RegCloseKey(subhKey);

		return data;
	}

	static void reg_SetAutoExe(HKEY hKey,CString stype,CString value)
	{//������ ���۽� �ڵ� ������Ʈ HKEY_LOCAL_MACHINE
		HKEY phKey=reg_GetKey(hKey,
							 _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));	
		if(phKey==NULL) return;

		reg_SetStrValue(phKey,stype,value);
		::RegCloseKey(phKey);
	}

	static void reg_SetAutoExeDel(HKEY hKey,CString stype)
	{//������ ���۽� �ڵ� ������Ʈ ���� HKEY_LOCAL_MACHINE
		HKEY phKey=reg_GetKey(hKey,
							 _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));	
		if(phKey==NULL) return;

		::RegDeleteValue(phKey,stype);
		::RegCloseKey(phKey);
	}
			
	static int reg_GetKeyList(HKEY hKey,CStringArray &Data)
	{//������Ʈ���� Ű�� ���ϱ�
		Data.RemoveAll();

		TCHAR    achKey[_MAX_PATH];             // buffer for subkey name
		TCHAR    achClass[MAX_PATH] = TEXT(""); // buffer for class name 
		DWORD    cchClassName = MAX_PATH;		// size of class string 
		DWORD    cSubKeys=0;					// number of subkeys 
		DWORD    cbMaxSubKey;					// longest subkey size 
		DWORD    cchMaxClass;					// longest class string 
		DWORD    cValues;						// number of values for key 
		DWORD    cchMaxValue;					// longest value name 
		DWORD    cbMaxValueData;				// longest value data 
		DWORD    cbSecurityDescriptor;			// size of security descriptor 
		FILETIME ftLastWriteTime;				// last write time 
		DWORD    cbName;                        // size of name string 
 
		// Get the class name and the value count. 
		DWORD retCode = RegQueryInfoKey(hKey,                    // key handle 
										achClass,                // buffer for class name 
										&cchClassName,           // size of class string 
										NULL,                    // reserved 
										&cSubKeys,               // number of subkeys 
										&cbMaxSubKey,            // longest subkey size 
										&cchMaxClass,            // longest class string 
										&cValues,                // number of values for this key 
										&cchMaxValue,            // longest value name 
										&cbMaxValueData,         // longest value data 
										&cbSecurityDescriptor,   // security descriptor 
										&ftLastWriteTime);       // last write time 
		if (retCode!=ERROR_SUCCESS) return 0;

		for (int i=0; i<(int)cSubKeys; i++) 
		{ 
		   cbName = _MAX_PATH;
		   retCode = RegEnumKeyEx(hKey, i,achKey,&cbName,NULL,NULL,NULL,&ftLastWriteTime); 
		   if (retCode == ERROR_SUCCESS) 
		   {   
			   Data.Add(achKey);
		   }
		}
		return cSubKeys;
	}

	static int reg_GetValueList(HKEY hKey,CStringArray &Data)
	{//����Ʈ���� �� ���ϱ�
		Data.RemoveAll();

		DWORD cValues;                          // number of values for key 
		TCHAR achValue[_MAX_PATH]; 
		DWORD cchValue = _MAX_PATH; 
		TCHAR data[_MAX_PATH];
		DWORD size = _MAX_PATH;
		DWORD retCode=RegQueryInfoKey(hKey,        // key handle 
									  NULL,        // buffer for class name 
									  NULL,        // size of class string 
									  NULL,        // reserved 
									  NULL,        // number of subkeys 
									  NULL,        // longest subkey size 
									  NULL,        // longest class string 
									  &cValues,    // number of values for this key 
									  NULL,        // longest value name 
									  NULL,        // longest value data 
									  NULL,		   // security descriptor 
									  NULL);       // last write time 

		if (retCode!=ERROR_SUCCESS) return 0;
			 
		for( int i=0; i<(int)cValues; i++ )
		{
			cchValue = _MAX_PATH; 
			achValue[0] = '\0'; 
			size = _MAX_PATH;
			data[0] = '\0';
			retCode = RegEnumValue(hKey, i,achValue,&cchValue,NULL,NULL,
											 (PBYTE)data,&size );
			if( retCode == ERROR_SUCCESS )
			{			
				Data.Add(achValue);
			}
		}
		return cValues;
	}

	static HRESULT reg_OverrideClassesRoot(HKEY hKeyBase, CString szOverrideKey)
	{
		HKEY hKey;
		LONG l = RegOpenKey(hKeyBase, szOverrideKey, &hKey);
		
		if (l == ERROR_SUCCESS)
		{
			l = RegOverridePredefKey(HKEY_CLASSES_ROOT, hKey);

			RegCloseKey(hKey);
		}

		return HRESULT_FROM_WIN32(l);
	}

	/*static HRESULT reg_RisterDllA(CString szDll, BOOL flag)
	{
		CString szFunction =  flag ? _T("DllRegisterServer") : _T("DllUnregisterServer");
		HRESULT hr = S_OK;
		
		HMODULE hMod = LoadLibrary(szDll);

		if (hMod != NULL)
		{
			typedef HRESULT (_stdcall *DLLPROC)();
			DLLPROC pfnDllProc = reinterpret_cast<DLLPROC>(GetProcAddress(hMod,(CStringA)szFunction));
			
			if (pfnDllProc)
			{
				if (flag==TRUE)
				{//Override HKEY_CLASSES_ROOT									
					hr = reg_OverrideClassesRoot(HKEY_CURRENT_USER, _T("Software\\Classes"));
				}
				if (SUCCEEDED(hr)) hr = (*pfnDllProc)();
			} else 
			{
				hr = HRESULT_FROM_WIN32(GetLastError());
			}
			FreeLibrary(hMod);
		} else 
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}	
		
		return hr;
	}

	
	static BOOL reg_RisterDllB(CString pszDllName,BOOL bRegister)
	{// Attempt to locate the dll and call its DllRegisterServer or DllUnregisterServer function
		BOOL bRet = FALSE;

		// Test this full path and file name for existance
		DWORD dwAttr = GetFileAttributes(pszDllName);
		if (dwAttr != 0xFFFFFFFF)
		{
			// Try to load the dll
			HMODULE hDll = LoadLibrary(pszDllName);
			if (hDll)
			{
				// Get the pointer to the DllRegisterServer function
				typedef HRESULT (WINAPI* PFNDllServer)();
				PFNDllServer pDllServer = NULL;
				if (bRegister)
					pDllServer = (PFNDllServer)GetProcAddress(hDll,"DllRegisterServer");
				else
					pDllServer = (PFNDllServer)GetProcAddress(hDll,"DllUnregisterServer");
				
				if (pDllServer)
				{
					// Attempt to register the dll
					(pDllServer)();
					bRet = TRUE;
				}
				// Unload the dll
				FreeLibrary(hDll);
			}
		}

		return bRet;
	}
	
	static BOOL reg_RisterDllC(CString DllName, BOOL bReg)
	{
		HRESULT (STDAPICALLTYPE * lpDllEntryPoint)(void);
		HINSTANCE hLib;

		// DLL ������ �޸𸮷� �о���δ�.
		if (FAILED(OleInitialize(NULL))) return FALSE;
		hLib=LoadLibraryEx(DllName, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
		if (hLib < (HINSTANCE)HINSTANCE_ERROR) return FALSE;

		// ��ġ, �Ǵ� ���� �Լ��� ������ ã�´�.
		if (bReg == TRUE) {
			(FARPROC&)lpDllEntryPoint = GetProcAddress(hLib, "DllRegisterServer");
		} else {
			(FARPROC&)lpDllEntryPoint = GetProcAddress(hLib, "DllUnregisterServer");
		}

		// �Լ��� ȣ���Ѵ�.
		if (lpDllEntryPoint == NULL)      return FALSE;
		if (FAILED((*lpDllEntryPoint)())) return FALSE;
		FreeLibrary(hLib);
		OleUninitialize();

		return FALSE;
	}

	static BOOL reg_RisterDll(CString szDll, BOOL flag)
	{			
		if(SUCCEEDED(reg_RisterDllA(szDll,flag))) return TRUE;		
		if(reg_RisterDllB(szDll,flag)==TRUE)      return TRUE;
		if(reg_RisterDllC(szDll,flag)==TRUE)      return TRUE;       		    	

		return FALSE;
	}
	
	static void reg_RisterDllCmd(CString szDll, BOOL flag)
	{//regsvr32 ���� ���
		CString path=_T("");
		path.Format(_T("%s%s%s"),_T("\""),szDll,_T("\""));
		
		CString sExe=_T("");

		if(flag==TRUE) sExe.Format(_T("regsvr32 /s /c %s"),path);
		else           sExe.Format(_T("regsvr32 /s /u %s"),path);

		WinExec((CStringA)sExe,SW_HIDE);		
	}

	
	static void reg_RisterDllList(CString path,BOOL flag)
	{//regsvr32 folder file			        
		CFileFind finder;
		DWORD fsize=0;
		BOOL bWorking = finder.FindFile(path);
		
		while (bWorking)
		{	  
		  bWorking = finder.FindNextFile();	
		  
		  if(finder.GetFileName()==_T(".") || finder.GetFileName()==_T("..")) continue;
		  if(finder.GetFileName()==_T("Thumbs.db")) continue;

		  if(finder.IsDirectory()==FALSE)
		  {//file
			 reg_RisterDllCmd(finder.GetFilePath(),flag);   	  	  
		  }
		}
		finder.Close();
	}

	static void reg_UninstallDel(HKEY hKey,CString sKey)
	{//uninstall reg delete
	   CString uninstall=_T("Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall");

	   HKEY phKey=GReg::reg_GetKey(hKey,uninstall);
	   if(phKey)
	   {
 			::RegDeleteKey(phKey,sKey);
			::RegCloseKey(phKey);
	   }
	}

	static void reg_SetUninstall(HKEY hKey,CString sKey,CString title,CString value) 
	{//uninstall reg create
				
		CString uninstall=_T("Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\")+sKey;

		HKEY phKey=GReg::reg_GetKey(hKey,uninstall);
		if(phKey==NULL) phKey=reg_CreateKey(hKey,uninstall);
				
		reg_SetStrValue(phKey,_T("DisplayName"),title);
		reg_SetStrValue(phKey,_T("UninstallString"),value);
	}

	static LONG reg_AllDelKey(HKEY hKey, LPCTSTR lpSubKey) 
	{//��� Ű ����
		HKEY newKey;		
		LONG Result;
		DWORD Size;
		FILETIME FileTime;

		#ifdef _UNICODE			   			
			wchar_t newSubKey[MAX_PATH];
		#else
		    char newSubKey[MAX_PATH];		
		#endif

		RegOpenKeyEx(hKey, lpSubKey, 0, KEY_ALL_ACCESS, &newKey);
		Result = ERROR_SUCCESS;

		while(TRUE) 
		{
			Size = MAX_PATH;
			Result = RegEnumKeyEx(newKey, 0, newSubKey, &Size, NULL, NULL, NULL, &FileTime);
			if (Result != ERROR_SUCCESS) break;

			Result = reg_AllDelKey(newKey, newSubKey);
			if (Result  != ERROR_SUCCESS) break;
		}
		RegCloseKey(newKey);

		return RegDeleteKey(hKey, lpSubKey);
	}

	
	static void reg_SetDelKey(HKEY hKey,CString sSubKey,CString sGuid)
	{//REG ����
		HKEY phKey=reg_GetKey(hKey,sSubKey); 
		if(phKey==NULL) return;
				
		HKEY shKey=reg_GetKey(phKey,sGuid);	
		if(shKey==NULL) return;

		reg_AllDelKey(phKey, sGuid);

		::RegCloseKey(phKey);
	}

	static BOOL reg_ChkReisterDll(CString sclassid)
	{//Ŭ���� ���̵� ���� ����
		if(sclassid==_T("")) return FALSE;
		
		HKEY subhKey=reg_GetKey(HKEY_CLASSES_ROOT, _T("CLSID\\") + sclassid);
		if(subhKey) return TRUE;

		return FALSE;
	}
	
	static void reg_SetToolbarInit(HKEY hKey)
	{//IE ���� �ʱ�ȭ HKEY_CURRENT_USER
		HKEY phKey=reg_GetKey(hKey,
							 _T("Software\\Microsoft\\Internet Explorer\\Toolbar\\WebBrowser")); 
		if(phKey==NULL) return;
			
 		::RegDeleteValue(phKey,_T("ITBar7Layout"));
		::RegDeleteValue(phKey,_T("ITBarLayout"));
		
		::RegCloseKey(phKey);
		reg_RisterDllCmd(_T("browseui.dll"),TRUE);
	}
	
	static void reg_SetToolBarDel(HKEY hKey,CString classid)
	{//IE ���� ���� HKEY_LOCAL_MACHINE
		HKEY phKey=reg_GetKey(hKey,
							 _T("Software\\Microsoft\\Internet Explorer\\Extensions"));	
		if(phKey==NULL) return;

		HKEY shKey=reg_GetKey(phKey,classid);	
		if(shKey==NULL) return;

		RegDeleteKey(phKey,classid);
		RegCloseKey(phKey);
	}
	
	static void reg_SetSearchScopes(HKEY hKey,CString classid,CString title,CString surl) 
	{//�˻� ������ HKEY_CURRENT_USER
		if(surl.IsEmpty() || surl==_T("")) return;//�˻� �ּҰ� ������

		HKEY phKey=reg_GetKey(hKey,
			                  _T("Software\\Microsoft\\Internet Explorer\\SearchScopes"));
		if(phKey==NULL) return;
 
		CString dScope=reg_GetStrValue(phKey,_T("DefaultScope"));	
		//if(dScope==classid) return;
 
		reg_SetStrValue(phKey,_T("DefaultScope"),classid);
 		 
		HKEY shKey=reg_CreateKey(phKey,classid);		
		if(shKey==NULL) return;  
		  		 
		reg_SetDwordValue(shKey,_T("Codepage"),0xfde9);		
		reg_SetStrValue(shKey,_T("DisplayName"),title);
		reg_SetStrValue(shKey,_T("URL"),surl);

		::RegCloseKey(shKey);
		::RegCloseKey(phKey); 
	}

	static void reg_Timewait(HKEY hKey) 
	{//Socket Port Close�� �� Open ��� �ð�
		HKEY hkresult;
		DWORD data=0x00;
		DWORD size = sizeof(data);
		DWORD type=REG_DWORD;

		::RegOpenKeyEx(hKey,//HKEY_LOCAL_MACHINE 
					   _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters"),
					   NULL,KEY_ALL_ACCESS,&hkresult);    
		::RegQueryValueEx(hkresult,_T("TcpTimedWaitDelay"),NULL,&type,(LPBYTE)&data,&size); 	

		if(data!=0x1E)
		{
		  data=0x1E;		 
		  ::RegSetValueEx(hkresult,_T("TcpTimedWaitDelay"),NULL,REG_DWORD,(LPBYTE)&data,sizeof(data)); 
		}
		::RegCloseKey(hkresult);
	}

	static CString reg_GetIEVersion(HKEY hKey)
	{//IE Version
		HKEY phKey=reg_GetKey(hKey,//HKEY_LOCAL_MACHINE 
							  _T("Software\\Microsoft\\Internet Explorer"));
		if(phKey==NULL) return _T("");

		CString data=reg_GetStrValue(phKey,_T("Version"));
		::RegCloseKey(phKey);
		
		return data; 
	}

	static void reg_SetSearchHook(HKEY hKey,CString stype,CString value)
	{//������ �⺻ �˻��� ���� �� �߰� HKEY_CURRENT_USER
		HKEY phKey=GReg::reg_GetKey(hKey,
							        _T("SOFTWARE\\Microsoft\\Internet Explorer\\URLSearchHooks"));	
		if(phKey==NULL) return;

		CStringArray Data;
		int count=reg_GetValueList(phKey,Data);
		
		for(int i=0;i<count;i++){	
			CString subValue=Data.GetAt(i);
			if(subValue.IsEmpty() || subValue==_T("")) continue;
			::RegDeleteValue(phKey,subValue);
		}

		reg_SetStrValue(phKey,stype,value);   	
		::RegCloseKey(phKey);
	}

	static void reg_SetBHOEnable(HKEY hKey,CString sGuid)
	{//������ ����� ��� HKEY_CURRENT_USER
		HKEY phKey=GReg::reg_GetKey(hKey,
							        _T("Software\\Microsoft\\Windows\\CurrentVersion\\Ext\\Settings")); 
		if(phKey==NULL) return;

		HKEY shKey=GReg::reg_GetKey(phKey,sGuid);	
		if(shKey==NULL) return;
		
		::RegDeleteKey(phKey,sGuid); 

		::RegCloseKey(phKey);
	}

	static HRESULT reg_SetInfoBand(CLSID clsid)
	{//���̵�� ��� 		
		CoInitialize(NULL);

		ICatRegister *pcr = NULL; 
		HRESULT hr = CoCreateInstance(CLSID_StdComponentCategoriesMgr, 
									  NULL,CLSCTX_INPROC_SERVER,
									  IID_ICatRegister, (LPVOID*) &pcr);
		if(SUCCEEDED(hr)) 
		{		
			hr = pcr->RegisterClassImplCategories(clsid, 1, &(CATID)CATID_InfoBand);
			pcr->Release();
		}			
		CoUninitialize();

		return hr;
	}

	static HRESULT reg_SetUnInfoBand(CLSID clsid)
	{//���̵�� ���� 
		CoInitialize( NULL );

		ICatRegister *pcr = NULL;
		
		HRESULT hr = ::CoCreateInstance(CLSID_StdComponentCategoriesMgr, 
										NULL, CLSCTX_INPROC_SERVER,
										IID_ICatRegister, (LPVOID*)&pcr );
		if( SUCCEEDED( hr ) && pcr ){		
			hr = pcr->UnRegisterClassImplCategories(clsid, 1, &(CATID)CATID_InfoBand);				
			pcr->Release();
		}
		::CoUninitialize();

		return hr;
	} 
		
	static void reg_SetToolbar(HKEY hKey,CString classid)
	{//Toolbar ��� TOOL_GUID HKLM
		HKEY hKeyLocal = NULL;
		RegCreateKeyEx(hKey, 
			           _T("Software\\Microsoft\\Internet Explorer\\Toolbar"), 
					   0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKeyLocal, NULL);
		RegSetValueEx(hKeyLocal, classid, 0, REG_BINARY, NULL, 0);
		RegCloseKey(hKeyLocal);
	}

	static void reg_SetUnToolbar(HKEY hKey,CString classid)
	{//Toolbar ���� HKLM
		HKEY hKeyLocal = NULL;
		RegCreateKeyEx(hKey, 
			           _T("Software\\Microsoft\\Internet Explorer\\Toolbar"), 
			                    0, 
								NULL, 
								REG_OPTION_NON_VOLATILE, 
								KEY_WRITE, 
								NULL, 
								&hKeyLocal, 
								NULL);
		RegDeleteValue(hKeyLocal, classid);
		RegCloseKey(hKeyLocal);
	} 
	
	static void SetRegistryBho(HKEY hKey,CString classid)
	{//Browser Helper Object ��� HKLM
		HKEY hKeyLocal = NULL;
		RegCreateKeyEx(hKey, 
			           _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects\\"), 
			           0, 
					   NULL, 
					   REG_OPTION_NON_VOLATILE, 
					   KEY_WRITE, 
					   NULL, 
					   &hKeyLocal, 
					   NULL);
		RegCreateKey(hKeyLocal,classid,&hKeyLocal);
		RegCloseKey(hKeyLocal);
	}

	static void reg_StopBho(HKEY hKey,CString classid)
	{//������ ����� ���� ��� HKCU
		HKEY phKey=reg_CreateKey(hKey,
			                     _T("Software\\Microsoft\\Windows\\CurrentVersion\\Ext\\Settings\\")+classid);
		if(phKey==NULL)	return;
		
		reg_SetDwordValue(phKey,_T("Flags"),1);
		reg_SetStrValue(phKey,_T("Version"),_T("*"));   			  
		::RegCloseKey(phKey);				 		
	}

	static void reg_IEStartPage(HKEY hKey,CString value1,CString value2)
	{//IE start page HKCU
		HKEY phKey=reg_CreateKey(hKey,
			                     _T("Software\\Microsoft\\Internet Explorer\\Main"));
		if(phKey==NULL)	return;

		if(value1!=_T("")) reg_SetStrValue(phKey,_T("Start Page"),value1);   	
		if(value2!=_T("")) reg_SetStrValue(phKey,_T("Secondary Start Page"),value2);  

		::RegCloseKey(phKey);
	}

	static void reg_IEPopup(CString sUrl)
	{//ie popup allow
		if(reg_ChkKey(HKCU,_T("Software\\Microsoft\\Internet Explorer\\New Windows\\Allow"),
					  sUrl)==TRUE) return;
			
		BYTE *data=new BYTE[1];
		::memset(data,0,1);		
		reg_SetBinValue(HKCU,_T("Software\\Microsoft\\Internet Explorer\\New Windows\\Allow"),
					    sUrl,data);				
		delete data;
	}

	static void reg_Cdautorun(DWORD value)
	{//cd auto start
		HKEY hkresult;
		DWORD data=0x00;
		DWORD size = sizeof(data);
		DWORD type=REG_DWORD; //Autorun  

		::RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
					   _T("SYSTEM\\CurrentControlSet\\Services\\Cdrom"),
					   NULL,KEY_ALL_ACCESS,&hkresult);    
		::RegQueryValueEx(hkresult,_T("Autorun"),NULL,&type,(LPBYTE)&data,&size); 		

		if(data!=value){
		  if(value==1) data=0x01;
		  else data=0x00;
		  ::RegSetValueEx(hkresult,_T("Autorun"),NULL,REG_DWORD,(LPBYTE)&data,sizeof(data)); 
		}
		::RegCloseKey(hkresult);
	}

	static void reg_Dragwindow(BOOL flag)
	{//drag window(DragFullWindows)
		SystemParametersInfo(SPI_SETDRAGFULLWINDOWS,flag,NULL,SPIF_SENDWININICHANGE);
	}

	static void reg_Pptshow(DWORD value)
	{//ppt explorer���� ����
		HKEY phKey=reg_GetKey(HKLM,_T("Software\\CLASSES"));			
		if(phKey==NULL) return;

		HKEY hKey8=reg_CreateKey(phKey,_T("PowerPoint.Show.8"));
		if(hKey8) GReg::reg_SetDwordValue(hKey8,_T("BrowserFlags"),value);
						
		HKEY hKey12=reg_CreateKey(phKey,_T("PowerPoint.Show.12"));
		if(hKey12) GReg::reg_SetDwordValue(hKey12,_T("BrowserFlags"),value);
		
		::RegCloseKey(hKey8);
		::RegCloseKey(hKey12);
		::RegCloseKey(phKey);
	}

	static void reg_FileExt(CString skey)
	{//ppt explorer���� ����
		HKEY phKey=reg_CreateKey(HKCR,skey);			
		if(phKey==NULL) return;

		GReg::reg_SetStrValue(phKey,_T(""),_T("PowerPoint.SlideShow.8"));
		GReg::reg_SetStrValue(phKey,_T("Content Type"),_T("application/vnd.ms-powerpoint"));
				
		::RegCloseKey(phKey);
	}

	static void reg_Firewall(CString strExe,CString className)
	{//��ȭ������
		GReg::reg_SetStrValue(HKLM,
			_T("SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile\\AuthorizedApplications\\List"),
			strExe,strExe+_T(":*:Enabled:")+className);
	}

	static void reg_Firewall(BOOL bFlag)
	{//��ȭ������
		GReg::reg_SetDwordValue(HKLM,
			_T("SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile"),
			_T("EnableFirewall"),bFlag);
	}

	static BOOL reg_GetFirewall()
	{//��ȭ������
		return GReg::reg_GetDwordValue(HKLM,
				_T("SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile"),
				_T("EnableFirewall"));		
	}

	static DWORD reg_GetGDiProcessHandleQuota()
	{//GDI process handle ����(default-10000,max-65536) 
		return reg_GetDwordValue(HKLM,
				_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows"),
				_T("GDIProcessHandleQuota"),TRUE);
	}

	static DWORD reg_GetUserProcessHandleQuota()
	{//USER process handle ����(default-10000,max-65536) 
		return reg_GetDwordValue(HKLM,
				_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows"),
				_T("USERProcessHandleQuota"),TRUE);
	}
	
	static BOOL reg_SetGDiProcessHandleQuota(DWORD dwValue)
	{//GDI process handle ����(basic-10000,max-65536) 
		return GReg::reg_SetDwordValue(HKLM,
				_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows"),
				_T("GDIProcessHandleQuota"),dwValue);		
	}

	static BOOL reg_SetUserProcessHandleQuota(DWORD dwValue)
	{//USER process handle ����(default-10000,max-65536) 
		return reg_SetDwordValue(HKLM,
				_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows"),
				_T("USERProcessHandleQuota"),dwValue,TRUE);
	}

	static CString reg_GetGDiMemory()
	{
	    return reg_GetStrValue(HKLM,
		           _T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\SubSystems"),
				   _T("Windows"),TRUE);
	}

	static BOOL reg_SetGDiMemory(CString smemory)
	{//set gdi memory(default-3072, 8192);
		CString svalue=reg_GetGDiMemory();
		if(svalue.IsEmpty() || svalue==_T("")) return FALSE;

		CString sa=GStr::str_BeforeSplit(svalue,2,_T("="));	
		sa=GStr::str_AfterSplit(sa,1,_T(" "));
		
		CString sb=GStr::str_GetSplit(svalue,2,'=');
		CString sc=GStr::str_AfterSplit(svalue,3,_T("="));

		CString v1=GStr::str_GetSplit(sb,0,',');
		CString v2=GStr::str_GetSplit(sb,1,',');//value
		CString v3=GStr::str_GetSplit(sb,2,',');
		
		CString str=_T("");
		str.Format(_T("%s %s=%s,%s,%s=%s"),_T("%SystemRoot%\\system32\\csrss.exe"),sa,v1,smemory,v3,sc);//"8192"
		return reg_SetEStrValue(HKLM,
			                    _T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\SubSystems"),
			                    _T("Windows"),str,TRUE);
		//%SystemRoot%\system32\csrss.exe ObjectDirectory=\Windows SharedSection=1024,3072,512 Windows=
		//On SubSystemType=Windows ServerDll=basesrv,1 ServerDll=winsrv:UserServerDllInitialization,3 ServerDll=
        //winsrv:ConServerDllInitialization,2 ProfileControl=Off MaxRequestThreads=16
	}

	static DWORD reg_GetSessionViewSize()
	{//terminal service
	    return reg_GetDwordValue(HKLM,
		           _T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Memory Management"),
				   _T("SessionViewSize"),TRUE);
	}

	static BOOL reg_SetSessionViewSize(DWORD dwValue)
	{//terminal service 48M,
	    return reg_SetDwordValue(HKLM,
		           _T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Memory Management"),
				   _T("SessionViewSize"),dwValue,TRUE);
	}

	static DWORD reg_GetSessionPoolSize()
	{//terminal service
	    return reg_GetDwordValue(HKLM,
		           _T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Memory Management"),
				   _T("SessionPoolSize"),TRUE);
	}

	static BOOL reg_SetSessionPoolSize(DWORD dwValue)
	{//terminal service 4M,
	    return reg_SetDwordValue(HKLM,
		           _T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Memory Management"),
				   _T("SessionPoolSize"),dwValue,TRUE);
	}

	static void reg_EnvPath(HKEY hKey,CString subKey,CString value)
	{		
		CString sPath=reg_GetStrValue(hKey,subKey,_T("Path"));		
		if(sPath.IsEmpty() || sPath==_T("")) return;

		sPath.TrimRight(_T(";"));		
		if(sPath.Find(value)<0)
		{
			sPath=sPath+_T(";")+value;							    
			GReg::reg_SetStrValue(hKey,subKey,_T("Path"),sPath,TRUE);
		}
	}

	static void reg_SetDelValue(HKEY hKey,CString subKey,CString stype)
	{//
		HKEY phKey=reg_GetKey(hKey,subKey);	
		if(phKey==NULL) return;

		::RegDeleteValue(phKey,stype);
		::RegCloseKey(phKey);
	}

	static void reg_SetIEAddrStateHide(DWORD value)
	{//o(hide),3(show)
		reg_SetDwordValue(HKCU,
		                  _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\\Zones\\3"),
						  _T("2104"),
						  value,TRUE);
	}

	static void reg_SetErrReport(DWORD DoReport,DWORD ShowUI,DWORD AllOrNone=1,DWORD IncludeKernelFaults=1)
	{//DoReport:0 ,ShowUI:0		
		reg_SetDwordValue(HKLM,
		                  _T("SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting"),
						  _T("DoReport"),
						  DoReport,TRUE);
		reg_SetDwordValue(HKLM,
		                  _T("SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting"),
						  _T("ShowUI"),
						  ShowUI,TRUE);
		if(DoReport==1)
		{
			//program 1(use),3(not)
			reg_SetDwordValue(HKLM,
		                  _T("SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting"),
						  _T("AllOrNone"),
						  AllOrNone,TRUE);
			//windowsos 1(use),0(not)
			reg_SetDwordValue(HKLM,
		                  _T("SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting"),
						  _T("IncludeKernelFaults"),
						  IncludeKernelFaults,TRUE);
		}
	}

	static void reg_SetHangTimeout(CString sTimeout=_T("1000"))
	{//1000 default:5000
		reg_SetStrValue(HKCU,
		                  _T("Control Panel\\Desktop"),
						  _T("HungAppTimeout"),
						  sTimeout,TRUE);
	}

	static void reg_SetAutoEndTasks(CString svalue=_T("1"))
	{//1 force kill default:0
		reg_SetStrValue(HKCU,
		                  _T("Control Panel\\Desktop"),
						  _T("AutoEndTasks"),
						  svalue,TRUE);
	}


	static void reg_SetWaitTimeout(CString sTimeout=_T("100"))
	{//100 default:20000 window off wait time
		reg_SetStrValue(HKLM,
		                  _T("SYSTEM\\CurrentControlSet\\Control"),
						  _T("WaitToKillServiceTimeout"),
						  sTimeout,TRUE);
	}

	 
	static void reg_SetCrashControl(DWORD AutoReboot=1,DWORD LogEvent=1,DWORD SendAlert=0,DWORD CrashDumpEnabled=0)
	{//
		//rebooting
		reg_SetDwordValue(HKLM,
		                  _T("SYSTEM\\ControlSet001\\Control\\CrashControl"),
						  _T("AutoReboot"),
						  AutoReboot,TRUE);
		//event log
		reg_SetDwordValue(HKLM,
		                  _T("SYSTEM\\ControlSet001\\Control\\CrashControl"),
						  _T("LogEvent"),
						  LogEvent,TRUE);

		//send alert
		reg_SetDwordValue(HKLM,
		                  _T("SYSTEM\\ControlSet001\\Control\\CrashControl"),
						  _T("SendAlert"),
						  SendAlert,TRUE);
		//dump file save
		reg_SetDwordValue(HKLM,
		                  _T("SYSTEM\\ControlSet001\\Control\\CrashControl"),
						  _T("CrashDumpEnabled"),
						  CrashDumpEnabled,TRUE);
		
		
	}

	static BOOL reg_SetScreenSave(CString sval)
	{//Screen Save Active 0(noactive),1(active)
	    return reg_SetStrValue(HKCU,
		           _T("Control Panel\\Desktop"),
				   _T("ScreenSaveActive"),sval,TRUE);		
	}

	static BOOL reg_SetScreenSaverIsSecure(CString sval)
	{//ScreenSaverIsSecure 0,1
	    return reg_SetStrValue(HKCU,
		           _T("Control Panel\\Desktop"),
				   _T("ScreenSaverIsSecure"),sval,TRUE);		
	}

	static BOOL reg_SetScreenSaveTimeOut(CString sval)
	{//ScreenSaveTimeOut 600 10minute
	    return reg_SetStrValue(HKCU,
		           _T("Control Panel\\Desktop"),
				   _T("ScreenSaveTimeOut"),sval,TRUE);		
	}

	static BOOL reg_SetScreenSaveExe(CString sval)
	{//ScreenSaveExe
	    return reg_SetStrValue(HKCU,
		           _T("Control Panel\\Desktop"),
				   _T("SCRNSAVE.EXE"),sval,TRUE);		
	}

	static BOOL reg_SetTileWallpaper(CString stile,CString style)
	{//TileWallpaper  0(center), 1(tile), 0(stretch)
	 //WallpaperStyle 0(center), 0(tile), 2(stretch)
		reg_SetStrValue(HKCU,
		           _T("Control Panel\\Desktop"),
				   _T("TileWallpaper"),stile,TRUE);		

		return reg_SetStrValue(HKCU,
		           _T("Control Panel\\Desktop"),
				   _T("WallpaperStyle"),style,TRUE);				
	}

	static BOOL reg_SetWallpaper(CString sval)
	{//Wallpaper
	    return reg_SetStrValue(HKCU,
		           _T("Control Panel\\Desktop"),
				   _T("Wallpaper"),sval,TRUE);		
	}

	static BOOL reg_SetWUpdateRebStop()
	{//window update rebooting stop
		return reg_SetDwordValue(HKLM,
		                  _T("SOFTWARE\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU"),
						  _T("NoAutoRebootWithLoggedOnUsers"),
						  1,TRUE);
	}*/

} ;
