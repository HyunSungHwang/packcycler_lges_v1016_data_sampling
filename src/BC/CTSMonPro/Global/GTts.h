#include <atlbase.h> 
extern CComModule _Module; 
#include <atlcom.h> 

#include <sapi.h>
#include <sphelper.h>

#pragma comment(lib,"sapi.lib")

class GTts
{
public:   	
        			
	BOOL m_bStart;
	DWORD m_SpeakFlags;
	CComPtr <ISpVoice>	cpVoice;
	CComPtr <ISpStream>	cpStream;
	CSpStreamFormat		cAudioFmt;

	CComPtr <ISpObjectToken>	  cpToken;
	CComPtr <IEnumSpObjectTokens> cpEnum;

	GTts()
	{
		m_bStart=FALSE;
		m_SpeakFlags=0;

		cpVoice=NULL;
		cpStream=NULL;	

		cpToken=NULL;
		cpEnum=NULL;
	}
	
	~GTts()
	{

	}

	BOOL tts_Init()
	{
		if(m_bStart==TRUE) return TRUE;

		HRESULT hr = cpVoice.CoCreateInstance( CLSID_SpVoice );
		if(FAILED(hr)) return FALSE;

		//Set the audio format
		hr = SpEnumTokens(SPCAT_VOICES, L"language=412", L"VW Yumi",  &cpEnum);
		if(FAILED(hr)) return FALSE;
	
		//Get the closest token
		hr = cpEnum ->Next(1, &cpToken, NULL);
		if(FAILED(hr)) return FALSE;
	
		//set the voice 	
		hr = cpVoice->SetVoice( cpToken);
		if(FAILED(hr)) return FALSE;
	
		//set the output to the default audio device
		hr = cpVoice->SetOutput( NULL, TRUE );
		if(FAILED(hr)) return FALSE;
	
		return m_bStart=TRUE;
	}

    BOOL tts_Speech(CString stext)
	{
		if(!m_bStart) return FALSE;
		if(!cpVoice) return FALSE;
		
		//Speak the text file (assumed to exist)			
        HRESULT hr = cpVoice->Speak(stext.AllocSysString(), m_SpeakFlags, NULL);
		if(FAILED(hr)) return FALSE;
	
		return TRUE;
	}

	int tts_GetState()
	{
		if(!m_bStart) return 0;
		if(!cpVoice) return 0;

		SPVOICESTATUS pStatus;
		HRESULT hr = cpVoice->GetStatus(&pStatus, NULL); 
		if(FAILED(hr)) return 0;

		if(pStatus.dwRunningState == SPRS_IS_SPEAKING) return 1;
		return 0;
	}



	void tts_Async()
	{
		m_SpeakFlags|=SPF_ASYNC;
	}

	void tts_Close()
	{
		m_bStart=FALSE;

		if(cpEnum)
		{
			cpEnum.Release();
			cpEnum=NULL;
		}
		if(cpToken)
		{
			cpToken.Release();
			cpToken=NULL;
		}

		if(cpStream)
		{
			cpStream.Release();
			cpStream=NULL;	
		}
		if(cpVoice)
		{
			cpVoice.Release();
			cpVoice=NULL;
		}			
	}
	

	

} ;

/*
http://msdn.microsoft.com/en-us/library/ms717252(v=vs.85).aspx
typedef enum SPEAKFLAGS
{
    //--- SpVoice flags
    SPF_DEFAULT,
    SPF_ASYNC,
    SPF_PURGEBEFORESPEAK,
    SPF_IS_FILENAME,
    SPF_IS_XML,
    SPF_IS_NOT_XML,
    SPF_PERSIST_XML,

    //--- Normalizer flags
    SPF_NLP_SPEAK_PUNC,

    //--- TTS Format
    SPF_PARSE_SAPI,
    SPF_PARSE_SSML,
    SPF_PARSE_AUTODETECT

    //--- Masks
    SPF_NLP_MASK,
    SPF_PARSE_MASK,
    SPF_VOICE_MASK,
    SPF_UNUSED_FLAGS
} SPEAKFLAGS;*/
