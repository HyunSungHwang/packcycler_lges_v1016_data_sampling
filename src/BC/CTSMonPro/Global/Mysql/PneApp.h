#pragma once

#include "MySql.h"

//! 데이터베이스 쿼리 클래스
class PneApp
{
public:
    
	MySql m_MySql;
	CTime m_StartTime;

	void ReConnectTime();
	void ReConnectError();

	BOOL CreateTbMonitor();
	
	BOOL SetStateInsert(CTypedPtrList<CPtrList, CHINFO*> &ChList);
	BOOL SetMonitoring(CHINFO *chinfo);
	
public:
	PneApp(void);
	virtual ~PneApp(void);

};

extern PneApp mainPneApp;