#include "stdafx.h" 
 

#include "SqliteUtil.h"


SqliteUtil::SqliteUtil(void)
{	
	m_szDbFile=_T("");
	m_bOpen=FALSE;
	m_bErrMesg=FALSE;
}

SqliteUtil::~SqliteUtil(void)
{
	slite_close();
}

void SqliteUtil::log_save(CString sType,CString sLog)
{ 	
	if(sType.IsEmpty() || sType=="") return;
	if(sLog.IsEmpty() || sLog=="")   return;

    GLog::log_Save(_T("lite"),sType,sLog,NULL,TRUE);
}

BOOL SqliteUtil::slite_open(CString AppPath,CString sfile,BOOL bCreate)
{   
	GFile::file_ForceDirectory(AppPath);
	m_szDbFile=AppPath+"\\"+sfile;
	m_bOpen=m_sqlite.Open(m_szDbFile);
	
	if(m_bOpen==TRUE)
	{
		if(bCreate)
		{
			CreateTbWorkinfo();
			CreateTbWorkevent();
			CreateTbWorkstate();
			CreateTbWorkstep();
		}
		return TRUE;
	}
	log_save("lite1",m_sqlite.m_szErrorText);
	return FALSE;
}

CSqlStatement* SqliteUtil::GetExecute(CString sql)
{
	if(m_bOpen==FALSE) return NULL;

	return m_sqlite.Statement(sql);
}

BOOL SqliteUtil::SetExecute(CString sql)
{
	if(m_bOpen==FALSE) return FALSE;

	if(sql.Find("CREATE")<0)
	{//sql save(create except)
		log_save("sql",sql);
	}

	BOOL bflag=m_sqlite.DirectStatement(sql);
	if(bflag==TRUE) return TRUE;
	
	log_save("sql",m_sqlite.m_szErrorText);
	return bflag;	
}


BOOL SqliteUtil::SetEditData1(CString table,CString n1,CString v1,CString n2,CString v2)
{
	if(m_bOpen==FALSE) return FALSE;
	
    CString sql="";
	sql.Format("UPDATE %s SET \
	            %s='%s' \
	            WHERE %s='%s' ",table,n1,v1,n2,v2);

	return SetExecute(sql);
}

CString SqliteUtil::GetData1(CString table,CString n1,CString v1,CString f)
{
	CString sql=_T("");
	sql.Format(_T("SELECT %f FROM %s WHERE %s='%s'"),f,table,n1,v1);

	CString svalue="";
	CSqlStatement *stmt=GetExecute(sql);
	if (stmt != NULL)
    {
	  if (stmt->NextRow()) svalue=stmt->ValueString(0);
	  delete stmt;
	}
	return svalue;
}

CString SqliteUtil::GetData2(CString table,CString n1,CString v1,CString n2,CString v2,CString f)
{
	CString sql=_T("");
	sql.Format(_T("SELECT %f FROM %s WHERE %s='%s' AND %s='%s'"),f,table,n1,v1,n2,v2);

	CString svalue="";
	CSqlStatement *stmt=GetExecute(sql);
	if (stmt != NULL)
    {
	  if (stmt->NextRow()) svalue=stmt->ValueString(0);
	  delete stmt;
	}
	return svalue;
}

CString SqliteUtil::GetData3(CString table,CString n1,CString v1,CString n2,CString v2,CString n3,CString v3,CString f)
{
	CString sql=_T("");
	sql.Format(_T("SELECT %f FROM %s WHERE %s='%s' AND %s='%s' AND %s='%s'"),f,table,n1,v1,n2,v2,n3,v3);

	CString svalue="";
	CSqlStatement *stmt=GetExecute(sql);
	if (stmt != NULL)
    {
	  if (stmt->NextRow()) svalue=stmt->ValueString(0);
	  delete stmt;
	}
	return svalue;
}

BOOL SqliteUtil::SetDelete(CString table)
{
	if(m_bOpen==FALSE) return FALSE;
	
    CString sql="";
	sql.Format("DELETE FROM %s' ",table);
	
	return SetExecute(sql);
}

BOOL SqliteUtil::SetDelete1(CString table,CString n1,CString v1)
{
	if(m_bOpen==FALSE) return FALSE;
	
    CString sql="";
	sql.Format("DELETE FROM %s WHERE %s='%s' ",table,n1,v1);
	
	return SetExecute(sql);
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


BOOL SqliteUtil::CreateTbWorkinfo()
{//worinfo Table Create		
	CString table=_T("WORKINFO");
	if(slite_tableExist(table)==TRUE) return TRUE;
		
CString sql;
sql.Format(_T("CREATE TABLE %s (          \
NOWORK INTEGER PRIMARY KEY AUTOINCREMENT, \
DEVICEID CHAR(10),                        \
BLOCKID CHAR(5),                          \
LOTID CHAR(30),                           \
CELLID CHAR(30),                          \
CHANNEL CHAR(2),                          \
MODULEID CHAR(2),                         \
CHANNELIDX CHAR(2),                       \
NMMODULE CHAR(20),                        \
DTSTART CHAR(10),                         \
NMWORK CHAR(100),                         \
WORKER CHAR(50),                          \
IP CHAR(30),                              \
SCHEDNM CHAR(100),                        \
SCHEDFILE CHAR(255),                      \
SCHEDSIZE CHAR(10),                       \
STAT CHAR(1),                             \
DTREG CHAR(20) )"),table);

	return SetExecute(sql);
}

BOOL SqliteUtil::CreateTbWorkevent()
{//workevent Table Create	 7002,7005	
	CString table=_T("WORKEVENT");
	if(slite_tableExist(table)==TRUE) return TRUE;

CString sql;
sql.Format(_T("CREATE TABLE %s (           \
NOEVENT INTEGER PRIMARY KEY AUTOINCREMENT, \
DEVICEID CHAR(10),                         \
BLOCKID CHAR(5),                           \
LOTID CHAR(30),                            \
CELLID CHAR(30),                           \
CHANNEL CHAR(2),                           \
MODULEID CHAR(2),                          \
CHANNELIDX CHAR(2),                        \
SAMPLENO CHAR(30),                         \
NMWORK CHAR(100),                          \
TPSTATE CHAR(4),                           \
STAT CHAR(1),                              \
DTREG CHAR(20) )"),table);

	return SetExecute(sql);
}

BOOL SqliteUtil::CreateTbWorkstate()
{//workstat Table Create 7004	
	CString table=_T("WORKSTATE");
	if(slite_tableExist(table)==TRUE) return TRUE;

CString sql;
sql.Format(_T("CREATE TABLE %s (           \
NOSTATE INTEGER PRIMARY KEY AUTOINCREMENT, \
DEVICEID CHAR(10),                         \
BLOCKID CHAR(5),                           \
LOTID CHAR(30),                            \
CELLID CHAR(30),                           \
CHANNEL CHAR(2),                           \
MODULEID CHAR(2),                          \
CHANNELIDX CHAR(2),                        \
TPSTATE CHAR(1),                           \
STAT CHAR(1),                              \
DTREG CHAR(20) )"),table);
	   
	return SetExecute(sql);	
}

BOOL SqliteUtil::CreateTbWorkstep()
{//workstep Table Create		
	CString table=_T("WORKSTEP");
	if(slite_tableExist(table)==TRUE) return TRUE;

CString sql1;
sql1.Format(_T("CREATE TABLE %s (         \
NOSTEP INTEGER PRIMARY KEY AUTOINCREMENT, \
DEVICEID CHAR(10),                        \
BLOCKID CHAR(5),                          \
LOTID CHAR(30),                           \
CELLID CHAR(30),                          \
CHANNEL CHAR(2),                          \
MODULEID CHAR(2),                         \
CHANNELIDX CHAR(2),                       \
SAMPLENO CHAR(30),                        \
NMWORK CHAR(100),                         \
CYCLENO CHAR(10),                         \
STEPNO CHAR(10),"),table);

CString sql2;
sql2.Format(_T("CHNO CHAR(5),   \
CHSTEPNO CHAR(5),               \
CHSTATE CHAR(5),                \
CHSTEPTYPE CHAR(5),             \
CHSTEPTYPESTR CHAR(10),         \
LSTEP_SEQ CHAR(11),             \
CHDATASELECT CHAR(11),          \
CHCODE CHAR(11),                \
CHGRADECODE CHAR(11),           \
CHRESERVED CHAR(11),            \
START_TIME CHAR(20),            \
END_TIME CHAR(20),              \
NINDEXFROM CHAR(11),            \
NINDEXTO CHAR(11),              \
NCURRENTCYCLENUM CHAR(11),      \
NTOTALCYCLENUM CHAR(11),        \
LSAVESEQUENCE CHAR(11),         \
LRESERVED CHAR(11),             \
NGOTOCYCLENUM CHAR(20),         \
FVOLTAGE CHAR(20),              \
FCURRENT CHAR(20),              \
FCHARGE_AMPAREHOUR CHAR(20),    \
FDISCHARGE_AMPAREHOUR CHAR(20), \
FWATT CHAR(20),                 \
FCHARGE_WATTHOUR CHAR(20),      \
FDISCHARGE_WATTHOUR CHAR(20),   \
FSTEPTIME_DAY CHAR(20),"));

CString sql3;
sql3.Format(_T("FSTEPTIME CHAR(20), \
FTOTALTIME_DAY CHAR(20),            \
FTOTALTIME CHAR(20),                \
FIMPEDANCE CHAR(20),                \
FTEMPARATURE CHAR(20),              \
FTEMPARATURE2 CHAR(20),             \
FTEMPARATURE3 CHAR(20),             \
FOVENTEMPARATURE CHAR(20),          \
CHUSINGCHAMBER CHAR(11),            \
CRECORDTIMENO CHAR(11),             \
RESERVED1 CHAR(11),                 \
RESERVED2 CHAR(11),                 \
FPRESSURE CHAR(20),                 \
FAVGVOLTAGE CHAR(20),               \
FAVGCURRENT CHAR(20),               \
FREALDATE CHAR(20),                 \
FREALCLOCK CHAR(20),                \
FDEBUG1 CHAR(20),                   \
FDEBUG2 CHAR(20),                   \
FTEMPARATURE_MIN CHAR(20),          \
FTEMPARATURE_MAX CHAR(20),          \
FTEMPARATURE_AVG CHAR(20),          \
FTEMPARATURE2_MIN CHAR(20),         \
FTEMPARATURE2_MAX CHAR(20),         \
FTEMPARATURE2_AVG CHAR(20),         \
FTEMPARATURE3_MIN CHAR(20),         \
FTEMPARATURE3_MAX CHAR(20),         \
FTEMPARATURE3_AVG CHAR(20),         \
FSTEP_CC_TIME_DAY CHAR(20),         \
FSTEP_CC_TIME CHAR(20),             \
FSTEP_CV_TIME_DAY CHAR(20),         \
FSTEP_CV_TIME CHAR(20),             \
FCHARGE_CC_AMPAREHOUR CHAR(20),     \
FCHARGE_CV_AMPAREHOUR CHAR(20),     \
FDISCHARGE_CC_AMPAREHOUR CHAR(20),  \
FDISCHARGE_CV_AMPAREHOUR CHAR(20),  \
FSTARTOCV CHAR(20),                 \
STAT CHAR(1),                       \
DTREG CHAR(20)                      \
)"));

	CString sql=sql1+sql2+sql3;
	return SetExecute(sql);
}

BOOL SqliteUtil::slite_tableExist(CString stable)
{
	if(m_bOpen==FALSE) return FALSE;

	CString sql;
	sql.Format("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='%s'",stable);

	int nVal=0;
	CSqlStatement* stmt = m_sqlite.Statement(sql);	
	if (stmt != NULL)
	{
		if(stmt->NextRow()) nVal=stmt->ValueInt(0);
      
		delete stmt;
	}
	else
	{
		log_save("lite4",m_sqlite.m_szErrorText);
	}
	return nVal;
}

void SqliteUtil::slite_close()
{	
	if(m_bOpen==FALSE) return;

	m_sqlite.Close();
}

BOOL SqliteUtil::InsertWorkinfo(CString deviceid,CString blockid,CString lotid,CString cellid,
							CString channel, CString moduleid, CString channelidx,
							CString nmmodule,CString nowdt,CString nmwork,
							CString worker,CString ip,CString schednm,CString schedfile,int stat)
{
	CString schData=_T("");
	CString schSize=_T("0");
	if(GFile::file_Finder(schedfile)==TRUE)
	{		
		schSize.Format(_T("%d"),GFile::file_SizeA(schedfile));		
	}
	CString nowdttime=GWin::win_CurrentTime("full");

	nmwork=GStr::str_MutibyteToUtf8(nmwork);
	worker=GStr::str_MutibyteToUtf8(worker);
	schednm=GStr::str_MutibyteToUtf8(schednm);
	schedfile=GStr::str_MutibyteToUtf8(schedfile);

	////////////////////////////////////////////////	
	CString sql=_T("");
	sql.Format(_T("INSERT INTO WORKINFO \
					(DEVICEID,BLOCKID,LOTID,CELLID,CHANNEL,MODULEID,CHANNELIDX,NMMODULE,DTSTART,NMWORK,WORKER,IP, \
					SCHEDNM,SCHEDSIZE,SCHEDFILE,STAT,DTREG) \
					VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s', \
						'%s','%s','%s','%d','%s')"),
						deviceid,blockid,lotid,cellid,channel,moduleid,channelidx,nmmodule,nowdt,nmwork,worker,ip,
						schednm,schSize,schedfile,stat,nowdttime);

	GLog::log_Save("lite","sql",sql,NULL,TRUE);
	return SetExecute(sql);	
}


BOOL SqliteUtil::InsertWorkevent(CString deviceid,CString blockid,CString lotid,CString cellid,
							 CString channel, CString moduleid, CString channelidx,
							 CString sampleno,CString nmwork,CString tpstate,int stat)
{		
	CString nowdttime=GWin::win_CurrentTime("full");
	////////////////////////////////////////////////		
	CString sql=_T("");
	sql.Format(_T("INSERT INTO WORKEVENT \
					(DEVICEID,BLOCKID,LOTID,CELLID,CHANNEL,MODULEID,CHANNELIDX, \
					SAMPLENO,NMWORK,TPSTATE,STAT,DTREG) \
					VALUES ('%s','%s','%s','%s','%s','%s','%s', \
						    '%s','%s','%s','%d','%s')"),
						deviceid,blockid,lotid,cellid,channel,moduleid,channelidx,
						sampleno,nmwork,tpstate,stat,nowdttime);

	GLog::log_Save("lite","sql",sql,NULL,TRUE);
	return SetExecute(sql);		
}


BOOL SqliteUtil::InsertWorkstate(CString deviceid,CString blockid,CString lotid,CString cellid,
							 CString channel, CString moduleid, CString channelidx,
							 CString tpstate,int stat)
{	
	CString nowdttime=GWin::win_CurrentTime("full");
	////////////////////////////////////////////////	
	CString sql=_T("");
	sql.Format(_T("INSERT INTO WORKSTATE \
					(DEVICEID,BLOCKID,LOTID,CELLID,CHANNEL,MODULEID,CHANNELIDX, \
					TPSTATE,STAT,DTREG) \
					VALUES ('%s','%s','%s','%s','%s','%s','%s', \
						    '%s','%d','%s')"),
						deviceid,blockid,lotid,cellid,channel,moduleid,channelidx,
						tpstate,stat,nowdttime);

    GLog::log_Save("lite","sql",sql,NULL,TRUE);
	return SetExecute(sql);		
}


BOOL SqliteUtil::InsertWorkStepEnd(CString deviceid,CString blockid,CString lotid,CString cellid,
							 CString channel, CString moduleid, CString channelidx,CString sampleno,
							 CString nmwork,CString cycleno,CString stepno,PS_DATA_END ps_DataEnd,int stat)
{		
	CString nowdttime=GWin::win_CurrentTime("full");
	
	nmwork=GStr::str_MutibyteToUtf8(nmwork);
	ps_DataEnd.chsteptypestr=GStr::str_MutibyteToUtf8(ps_DataEnd.chsteptypestr);

	////////////////////////////////////////////////		
CString sql=_T("");
sql.Format(_T("INSERT INTO %s \
(DEVICEID,BLOCKID,LOTID,CELLID,CHANNEL,MODULEID,CHANNELIDX,SAMPLENO,NMWORK,CYCLENO,STEPNO,\
CHNO,CHSTEPNO,CHSTATE,CHSTEPTYPE,CHSTEPTYPESTR,LSTEP_SEQ,CHDATASELECT,CHCODE,CHGRADECODE,CHRESERVED,\
START_TIME,END_TIME,\
NINDEXFROM,NINDEXTO,NCURRENTCYCLENUM,NTOTALCYCLENUM,LSAVESEQUENCE,LRESERVED,NGOTOCYCLENUM,\
FVOLTAGE,FCURRENT,FCHARGE_AMPAREHOUR,FDISCHARGE_AMPAREHOUR,FWATT,FCHARGE_WATTHOUR,\
FDISCHARGE_WATTHOUR,FSTEPTIME_DAY,FSTEPTIME,FTOTALTIME_DAY,FTOTALTIME,\
FIMPEDANCE,FTEMPARATURE,FTEMPARATURE2,FTEMPARATURE3,FOVENTEMPARATURE,CHUSINGCHAMBER,\
CRECORDTIMENO,RESERVED1,RESERVED2,FPRESSURE,\
FAVGVOLTAGE,FAVGCURRENT,FREALDATE,FREALCLOCK,FDEBUG1,FDEBUG2,\
FTEMPARATURE_MIN,FTEMPARATURE_MAX,FTEMPARATURE_AVG,\
FTEMPARATURE2_MIN,FTEMPARATURE2_MAX,FTEMPARATURE2_AVG,\
FTEMPARATURE3_MIN,FTEMPARATURE3_MAX,FTEMPARATURE3_AVG,\
FSTEP_CC_TIME_DAY,FSTEP_CC_TIME,FSTEP_CV_TIME_DAY,FSTEP_CV_TIME,\
FCHARGE_CC_AMPAREHOUR,FCHARGE_CV_AMPAREHOUR,FDISCHARGE_CC_AMPAREHOUR,FDISCHARGE_CV_AMPAREHOUR,FSTARTOCV,\
STAT,DTREG) \
VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',\
'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',\
'%s','%s',\
'%s','%s','%s','%s','%s','%s','%s',\
'%s','%s','%s','%s','%s',\
'%s','%s','%s','%s','%s',\
'%s','%s','%s','%s','%s','%s',\
'%s','%s','%s','%s',\
'%s','%s','%s','%s','%s','%s',\
'%s','%s','%s',\
'%s','%s','%s',\
'%s','%s','%s',\
'%s','%s','%s','%s',\
'%s','%s','%s','%s','%s','%s',\
'%s','%s')"),"WORKSTEP",
deviceid,blockid,lotid,cellid,channel,moduleid,channelidx,sampleno,nmwork,cycleno,stepno,
ps_DataEnd.chno,ps_DataEnd.chstepno,ps_DataEnd.chstate,ps_DataEnd.chsteptype,ps_DataEnd.chsteptypestr,ps_DataEnd.lstep_seq,ps_DataEnd.chdataselect,ps_DataEnd.chcode,ps_DataEnd.chgradecode,ps_DataEnd.chreserved,
ps_DataEnd.start_time,ps_DataEnd.end_time,
ps_DataEnd.nindexfrom,ps_DataEnd.nindexto,ps_DataEnd.ncurrentcyclenum,ps_DataEnd.ntotalcyclenum,ps_DataEnd.lsavesequence,ps_DataEnd.lreserved,ps_DataEnd.ngotocyclenum,
ps_DataEnd.fvoltage,ps_DataEnd.fcurrent,ps_DataEnd.fcharge_amparehour,ps_DataEnd.fdischarge_amparehour,ps_DataEnd.fwatt,ps_DataEnd.fcharge_watthour,
ps_DataEnd.fdischarge_watthour,ps_DataEnd.fsteptime_day,ps_DataEnd.fsteptime,ps_DataEnd.ftotaltime_day,ps_DataEnd.ftotaltime,
ps_DataEnd.fimpedance,ps_DataEnd.ftemparature,ps_DataEnd.ftemparature2,ps_DataEnd.ftemparature3,ps_DataEnd.foventemparature,ps_DataEnd.chusingchamber,
ps_DataEnd.crecordtimeno,ps_DataEnd.reserved1,ps_DataEnd.reserved2,ps_DataEnd.fpressure,
ps_DataEnd.favgvoltage,ps_DataEnd.favgcurrent,ps_DataEnd.frealdate,ps_DataEnd.frealclock,ps_DataEnd.fdebug1,ps_DataEnd.fdebug2,
ps_DataEnd.ftemparature_min,ps_DataEnd.ftemparature_max,ps_DataEnd.ftemparature_avg,
ps_DataEnd.ftemparature2_min,ps_DataEnd.ftemparature2_max,ps_DataEnd.ftemparature2_avg,
ps_DataEnd.ftemparature3_min,ps_DataEnd.ftemparature3_max,ps_DataEnd.ftemparature3_avg,
ps_DataEnd.fstep_cc_time_day,ps_DataEnd.fstep_cc_time,ps_DataEnd.fstep_cv_time_day,ps_DataEnd.fstep_cv_time,
ps_DataEnd.fcharge_cc_amparehour,ps_DataEnd.fcharge_cv_amparehour,ps_DataEnd.fdischarge_cc_amparehour,ps_DataEnd.fdischarge_cv_amparehour,ps_DataEnd.fstartocv,
GStr::str_IntToStr(stat),nowdttime);

	GLog::log_Save("lite","sql",sql,NULL,TRUE);
	return SetExecute(sql);	
}

