// EditCell.cpp : implementation file
//

#include "stdafx.h" 
 

#include "gxListCtrl.h"
#include "gxPickerCell.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// gxPickerCell

gxPickerCell::gxPickerCell (gxListCtrl* pCtrl, int iItem, int iSubItem, CString sInitText,CString sMode)
:   bEscape (FALSE)
{
    pListCtrl = pCtrl;
    Item = iItem;
    SubItem = iSubItem;
    InitText = sInitText;	

	m_sMode=sMode;
}

gxPickerCell::~gxPickerCell()
{
}

BEGIN_MESSAGE_MAP(gxPickerCell, CDateTimeCtrl)
    //{{AFX_MSG_MAP(gxPickerCell)
    ON_WM_KILLFOCUS()
    ON_WM_NCDESTROY()
    ON_WM_CHAR()
    ON_WM_CREATE()
    ON_WM_GETDLGCODE()
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// gxPickerCell message handlers

void gxPickerCell::SetListText()
{
    CString Text;
    GetWindowText (Text);
	if(Text.Find(';',0)>-1) return;

    // Send Notification to parent of ListView ctrl
    LV_DISPINFO dispinfo;
    dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
    dispinfo.hdr.idFrom = GetDlgCtrlID();
    dispinfo.hdr.code = LVN_ENDLABELEDIT;

    dispinfo.item.mask = LVIF_TEXT;
    dispinfo.item.iItem = Item;
    dispinfo.item.iSubItem = SubItem;
    //dispinfo.item.pszText = bEscape ? NULL : (LPSTR)(LPCSTR)Text; //LPTSTR ((LPCTSTR) Text);

	#ifdef _UNICODE					
		dispinfo.item.pszText = Text.AllocSysString(); //LPTSTR ((LPCTSTR) Text);	
	#else
		dispinfo.item.pszText = (LPSTR)(LPCSTR)Text; //LPTSTR ((LPCTSTR) Text);
	#endif	
	
    dispinfo.item.cchTextMax = Text.GetLength();

	CWnd *pWnd=GetParent()->GetParent();
    if(pWnd) pWnd->SendMessage(WM_NOTIFY,  GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);
	if(pWnd) pWnd->SendMessage(UM_LISTTEXT,GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);
	PostMessage(WM_CLOSE);
}

BOOL gxPickerCell::PreTranslateMessage (MSG* pMsg) 
{
    if (pMsg->message == WM_KEYDOWN)
    {
	    if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_DELETE || 
			pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_TAB || 
			pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN || GetKeyState (VK_CONTROL))
			{
				::TranslateMessage (pMsg);
				::DispatchMessage (pMsg);
				return TRUE;		    	// DO NOT process further
			}
    }

    return CDateTimeCtrl::PreTranslateMessage (pMsg);
}

void gxPickerCell::OnKillFocus (CWnd* pNewWnd) 
{
    CDateTimeCtrl::OnKillFocus(pNewWnd);
	
    SetListText();    
}

void gxPickerCell::OnNcDestroy() 
{
    CDateTimeCtrl::OnNcDestroy();
    
    delete this;
}

void gxPickerCell::OnKeyDown (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    // Up and down are in the OnKeyDown so that the user can hold down the arrow
    // keys to scroll through the entries.
    BOOL Control = GetKeyState (VK_CONTROL) < 0;
    
	switch (nChar)
	{
		case VK_UP :
		{			
			if (Item > 0 )
			{
				pListCtrl->onCreatePicker(Item - 1, SubItem);				
			}
			return;
		}
		case VK_DOWN :
		{
			pListCtrl->onCreatePicker(Item + 1, SubItem);				
			return;
		}
		case VK_HOME :
		{
			if (!Control) break;

			pListCtrl->onCreatePicker(0, SubItem);				
			return;
		}
		case VK_END :
		{
			if (!Control) break;

			int Count = pListCtrl->GetItemCount() - 1;

			pListCtrl->onCreatePicker(Count, SubItem);			
			return;
		}
    }
    
    CDateTimeCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}

void gxPickerCell::OnKeyUp (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    switch (nChar)
    {
		case VK_NEXT :
		{
			int Count = pListCtrl->GetItemCount();
			int NewItem = Item + pListCtrl->GetCountPerPage();
			if (Count > NewItem)
				pListCtrl->EditSubItem (NewItem, SubItem);
			else
				pListCtrl->EditSubItem (Count - 1, SubItem);
			return;
		}
		case VK_PRIOR :
		{
			int NewItem = Item - pListCtrl->GetCountPerPage();
			if (NewItem > 0)
				pListCtrl->EditSubItem (NewItem, SubItem);
			else
				pListCtrl->EditSubItem (0, SubItem);
			return;
		}
    }
    
    CDateTimeCtrl::OnKeyUp (nChar, nRepCnt, nFlags);
}

void gxPickerCell::OnChar (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    BOOL Shift = GetKeyState (VK_SHIFT) < 0;
    switch (nChar){
		case VK_ESCAPE :
		{
			if (nChar == VK_ESCAPE) bEscape = TRUE;
			GetParent()->SetFocus();
			return;
		}
		case VK_RETURN :
		{
			SetListText();
			pListCtrl->SetFocus();
			//pListCtrl->EditSubItem (Item+1, 0);
			//GetParent()->SetFocus();
			return;
		}
		/*case VK_TAB :
		{
			if (Shift){
				if (SubItem > 0 && SubItem==2) pListCtrl->EditSubItem (Item, SubItem - 1);
				else {
					if (Item > 0 && SubItem==2) pListCtrl->EditSubItem (Item - 1, 2);
				}
			} else {
				if (SubItem < 2 && SubItem==2) pListCtrl->EditSubItem (Item, SubItem + 1);
				//else pListCtrl->EditSubItem (Item + 1, 0);
				pListCtrl->EditSubItem (Item + 1, 2);
			}
			return;
		}*/
    }

    CDateTimeCtrl::OnChar (nChar, nRepCnt, nFlags);

    // Resize edit control if needed
    // Get text extent
    CString Text;

    GetWindowText (Text);
    CWindowDC DC (this);
    CFont *pFont = GetParent()->GetFont();
    CFont *pFontDC = DC.SelectObject (pFont);
    CSize Size = DC.GetTextExtent (Text);
    DC.SelectObject (pFontDC);
    Size.cx += 5;			   	// add some extra buffer

    // Get client rect
    CRect Rect, ParentRect;
    GetClientRect (&Rect);
    GetParent()->GetClientRect (&ParentRect);

    // Transform rect to parent coordinates
    ClientToScreen (&Rect);
    GetParent()->ScreenToClient (&Rect);

    // Check whether control needs to be resized and whether there is space to grow
    if (Size.cx > Rect.Width()){
		if (Size.cx + Rect.left < ParentRect.right ){
			Rect.right = Rect.left + Size.cx;
		} else {
			Rect.right = ParentRect.right;
		}
		MoveWindow (&Rect);
    }
}

int gxPickerCell::OnCreate (LPCREATESTRUCT lpCreateStruct) 
{
    if (CDateTimeCtrl::OnCreate (lpCreateStruct) == -1)
		return -1;

    // Set the proper font
    CFont* Font = GetParent()->GetFont();
    SetFont (Font);		
      	
	SetFocus();  
    return 0;
}

UINT gxPickerCell::OnGetDlgCode() 
{
    return CDateTimeCtrl::OnGetDlgCode() | DLGC_WANTARROWS | DLGC_WANTTAB;
}

void gxPickerCell::SetTextValue() 
{
	if(m_sMode==_T("HH:mm:ss"))
	{		
		int ivalue=(int)GTime::time_GetSecStr(InitText);
		
		GTime::time_SetSecDataPicker(this,ivalue);	
	}
	else if(m_sMode==_T("HH:mm"))
	{
		CString shh=GStr::str_GetSplit(InitText,0,':');
		CString smm=GStr::str_GetSplit(InitText,1,':');

		GTime::time_SetHourDataPicker(this,shh,smm);		
	}
}
