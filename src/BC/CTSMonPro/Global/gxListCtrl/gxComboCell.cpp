// EditCell.cpp : implementation file
//

#include "stdafx.h" 
 

#include "gxListCtrl.h"
#include "gxComboCell.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// gxComboCell

gxComboCell::gxComboCell (gxListCtrl* pCtrl, int iItem, int iSubItem, CString sInitText)
:   bEscape (FALSE)
{
    pListCtrl = pCtrl;
    Item = iItem;
    SubItem = iSubItem;
    InitText = sInitText;	
}

gxComboCell::~gxComboCell()
{
}

BEGIN_MESSAGE_MAP(gxComboCell, CComboBox)
    //{{AFX_MSG_MAP(gxComboCell)
    ON_WM_KILLFOCUS()
    ON_WM_NCDESTROY()
    ON_WM_CHAR()
    ON_WM_CREATE()
    ON_WM_GETDLGCODE()
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// gxComboCell message handlers

void gxComboCell::SetListText()
{
    CString Text;
    GetWindowText (Text);
	
	if(Text.Find(';',0)>-1) return;

    // Send Notification to parent of ListView ctrl
    LV_DISPINFO dispinfo;
    dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
    dispinfo.hdr.idFrom = GetDlgCtrlID();
    dispinfo.hdr.code = LVN_ENDLABELEDIT;

    dispinfo.item.mask = LVIF_TEXT;
    dispinfo.item.iItem = Item;
    dispinfo.item.iSubItem = SubItem;
    //dispinfo.item.pszText = bEscape ? NULL : (LPSTR)(LPCSTR)Text; //LPTSTR ((LPCTSTR) Text);

	#ifdef _UNICODE					
		dispinfo.item.pszText = Text.AllocSysString(); //LPTSTR ((LPCTSTR) Text);	
	#else
		dispinfo.item.pszText = (LPSTR)(LPCSTR)Text; //LPTSTR ((LPCTSTR) Text);
	#endif	
	
    dispinfo.item.cchTextMax = Text.GetLength();

	CWnd *pWnd=GetParent()->GetParent();
    if(pWnd) pWnd->SendMessage(WM_NOTIFY,  GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);
	if(pWnd) pWnd->SendMessage(UM_LISTTEXT,GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);
	PostMessage(WM_CLOSE);
}

BOOL gxComboCell::PreTranslateMessage (MSG* pMsg) 
{
    if (pMsg->message == WM_KEYDOWN)
    {
	    if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_DELETE || 
			pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_TAB || 
			pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN || GetKeyState (VK_CONTROL))
			{
				::TranslateMessage (pMsg);
				::DispatchMessage (pMsg);
				return TRUE;		    	// DO NOT process further
			}
    }

    return CComboBox::PreTranslateMessage (pMsg);
}

void gxComboCell::OnKillFocus (CWnd* pNewWnd) 
{
    CComboBox::OnKillFocus(pNewWnd);
	
    SetListText();
}

void gxComboCell::OnNcDestroy() 
{
    CComboBox::OnNcDestroy();
    
    delete this;
}

void gxComboCell::OnKeyDown (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    // Up and down are in the OnKeyDown so that the user can hold down the arrow
    // keys to scroll through the entries.
    BOOL Control = GetKeyState (VK_CONTROL) < 0;
    
	switch (nChar)
	{
		case VK_UP :
		{			
			if (Item > 0 )
			{
				pListCtrl->onCreateCombo(Item - 1, SubItem);				
			}
			return;
		}
		case VK_DOWN :
		{
			pListCtrl->onCreateCombo(Item + 1, SubItem);				
			return;
		}
		case VK_HOME :
		{
			if (!Control) break;

			pListCtrl->onCreateCombo(0, SubItem);				
			return;
		}
		case VK_END :
		{
			if (!Control) break;

			int Count = pListCtrl->GetItemCount() - 1;

			pListCtrl->onCreateCombo(Count, SubItem);			
			return;
		}
    }
    
    CComboBox::OnKeyDown(nChar, nRepCnt, nFlags);
}

void gxComboCell::OnKeyUp (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    switch (nChar)
    {
		case VK_NEXT :
		{
			int Count = pListCtrl->GetItemCount();
			int NewItem = Item + pListCtrl->GetCountPerPage();
			if (Count > NewItem)
				pListCtrl->EditSubItem (NewItem, SubItem);
			else
				pListCtrl->EditSubItem (Count - 1, SubItem);
			return;
		}
		case VK_PRIOR :
		{
			int NewItem = Item - pListCtrl->GetCountPerPage();
			if (NewItem > 0)
				pListCtrl->EditSubItem (NewItem, SubItem);
			else
				pListCtrl->EditSubItem (0, SubItem);
			return;
		}
    }
    
    CComboBox::OnKeyUp (nChar, nRepCnt, nFlags);
}

void gxComboCell::OnChar (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    BOOL Shift = GetKeyState (VK_SHIFT) < 0;
    switch (nChar){
		case VK_ESCAPE :
		{
			if (nChar == VK_ESCAPE) bEscape = TRUE;
			GetParent()->SetFocus();
			return;
		}
		case VK_RETURN :
		{
			SetListText();
			pListCtrl->SetFocus();
			//pListCtrl->EditSubItem (Item+1, 0);
			//GetParent()->SetFocus();
			return;
		}
		/*case VK_TAB :
		{
			if (Shift){
				if (SubItem > 0 && SubItem==2) pListCtrl->EditSubItem (Item, SubItem - 1);
				else {
					if (Item > 0 && SubItem==2) pListCtrl->EditSubItem (Item - 1, 2);
				}
			} else {
				if (SubItem < 2 && SubItem==2) pListCtrl->EditSubItem (Item, SubItem + 1);
				//else pListCtrl->EditSubItem (Item + 1, 0);
				pListCtrl->EditSubItem (Item + 1, 2);
			}
			return;
		}*/
    }

    CComboBox::OnChar (nChar, nRepCnt, nFlags);

    // Resize edit control if needed
    // Get text extent
    CString Text;

    GetWindowText (Text);
    CWindowDC DC (this);
    CFont *pFont = GetParent()->GetFont();
    CFont *pFontDC = DC.SelectObject (pFont);
    CSize Size = DC.GetTextExtent (Text);
    DC.SelectObject (pFontDC);
    Size.cx += 5;			   	// add some extra buffer

    // Get client rect
    CRect Rect, ParentRect;
    GetClientRect (&Rect);
    GetParent()->GetClientRect (&ParentRect);

    // Transform rect to parent coordinates
    ClientToScreen (&Rect);
    GetParent()->ScreenToClient (&Rect);

    // Check whether control needs to be resized and whether there is space to grow
    if (Size.cx > Rect.Width()){
		if (Size.cx + Rect.left < ParentRect.right ){
			Rect.right = Rect.left + Size.cx;
		} else {
			Rect.right = ParentRect.right;
		}
		MoveWindow (&Rect);
    }
}

int gxComboCell::OnCreate (LPCREATESTRUCT lpCreateStruct) 
{
    if (CComboBox::OnCreate (lpCreateStruct) == -1)
		return -1;

    // Set the proper font
    CFont* Font = GetParent()->GetFont();
    SetFont (Font);
   
    SetFocus();
    return 0;
}

UINT gxComboCell::OnGetDlgCode() 
{
    return CComboBox::OnGetDlgCode() | DLGC_WANTARROWS | DLGC_WANTTAB;
}