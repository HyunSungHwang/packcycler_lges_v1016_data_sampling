// JigIDRegDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "JigIDRegDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJigIDRegDlg dialog


CJigIDRegDlg::CJigIDRegDlg(CWnd* pParent /*=NULL*/)
//	: CDialog(CJigIDRegDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CJigIDRegDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CJigIDRegDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CJigIDRegDlg::IDD3):
	(CJigIDRegDlg::IDD),pParent)
{
	//{{AFX_DATA_INIT(CJigIDRegDlg)
	m_strTagID = _T("");
	//}}AFX_DATA_INIT
}


void CJigIDRegDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJigIDRegDlg)
	DDX_Control(pDX, IDC_COMBO2, m_ctrlJig);
	DDX_Control(pDX, IDC_COMBO1, m_ctrlModule);
	DDX_Control(pDX, IDC_LIST1, m_wndList);
	DDX_Text(pDX, IDC_EDIT1, m_strTagID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJigIDRegDlg, CDialog)
	//{{AFX_MSG_MAP(CJigIDRegDlg)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	//}}AFX_MSG_MAP
	ON_MESSAGE(SFTWM_BCR_SCANED, OnBcrscaned)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJigIDRegDlg message handlers

BOOL CJigIDRegDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ASSERT(m_pDoc);
	
	// TODO: Add extra initialization here

	m_wndList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES); 

	//Column 3개 생성 
	char szTitle[][30] = 
	{
		"BarCode Tag", "Unit No", "Channel No"
	};
	//각 column 폭 지정 
	int nWidth[7] = 
	{
		150, 150, 150 
	};
	for(int i = 0; i < 3; i++)
	{
		m_wndList.InsertColumn( i+1,
									  szTitle[i], 
									  LVCFMT_CENTER, 
									  nWidth[i], 
									  i);
	}

	LoadingData();

	for(int i=0; i<m_pDoc->GetInstallModuleCount(); i++)
	{
		int nModuleID =  m_pDoc->GetModuleID(i);
		m_ctrlModule.AddString(m_pDoc->GetModuleName(nModuleID));
		m_ctrlModule.SetItemData(i, nModuleID);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CJigIDRegDlg::OnOK() 
{
	// TODO: Add extra validation here

	int nJigNo, nModuleID;
	CString strData, strID;

	if(m_strTagID.IsEmpty())
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_JIG_LOCATION_DLG"));
		//@ AfxMessageBox("Tag ID가 입력되지 않았습니다.");
	}
	strID = m_strTagID;

	//Jig No
	int nIndex = m_ctrlModule.GetCurSel();
	if(nIndex < 0)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_JIG_LOCATION_DLG"));		
		//@ AfxMessageBox("Unit 번호가 입력되지 않았습니다.");		
		return;
	}
	nModuleID = m_ctrlModule.GetItemData(nIndex);

	//Channel No
	nIndex = m_ctrlJig.GetCurSel();
	if(nIndex < 0)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg3","IDD_JIG_LOCATION_DLG"));
		//@ AfxMessageBox("Channel 번호가 입력되지 않았습니다.");
		return;
	}
	m_ctrlJig.GetLBText(nIndex, strData);
	nJigNo = atol(strData);

	if(SaveJigInfoToDB(strID, nModuleID, nJigNo))
	{
		LoadingData();
	}
}

//Save new jig mapping to database
BOOL CJigIDRegDlg::SaveJigInfoToDB(CString strID, int nModuleID, int nJigNo)
{
	CDaoDatabase  db;

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL;
	strSQL.Format("SELECT ID FROM JigID WHERE ID = '%s'", strID);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	if(!rs.IsBOF() && !rs.IsEOF())
	{
		//Update 
		strSQL.Format("UPDATE JigID SET ModuleID = %d, JigNo = %d WHERE ID = '%s'", nModuleID, nJigNo, strID);
	}
	else
	{
		//Insert
		strSQL.Format("INSERT INTO JigID VALUES ( '%s', %d, %d)", strID, nModuleID, nJigNo);
	}
	db.Execute(strSQL);
	rs.Close();
	db.Close();

	return TRUE;
}

LRESULT CJigIDRegDlg::OnBcrscaned(WPARAM wParam,LPARAM lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	if(str.IsEmpty())	return 0;

	m_strTagID = str;

	UpdateData(FALSE);
	return 1;
}

BOOL CJigIDRegDlg::DeleteJigIDInfo(CString strJigID)
{
	if(strJigID.IsEmpty())	return FALSE;
	
	CDaoDatabase  db;

	try
	{
		CString strSQL;
		strSQL.Format("DELETE FROM JigID WHERE ID = '%s'", strJigID);
		db.Open(m_pDoc->GetDataBaseName());
		db.Execute(strSQL);
		db.Close();
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	return TRUE;

}

BOOL CJigIDRegDlg::SearchLocation(CString strID, int &nModuleID, int &nJigID)
{
	ASSERT(m_pDoc);

	CDaoDatabase  db;
	nModuleID = 0;
	nJigID = 0;

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	
	CString strSQL, strTemp;
	strSQL.Format("SELECT ModuleID, JigNo FROM JigID WHERE ID = '%s'", strID);
//	strSQL.Format("SELECT ModuleID, No INTO FROM JigID WHERE ID = '%s'", strID);

	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			nModuleID = data.lVal;
			data = rs.GetFieldValue(1);
			nJigID = data.lVal;
		}
		rs.Close();
		db.Close();
		
		if(nModuleID < 1 || nJigID < 1)
		{
			return FALSE;
			TRACE("Find error\n");
		}
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	

	return TRUE;
}

CString CJigIDRegDlg::SearchLocation(int nModuleID, int nJigID)
{
	CDaoDatabase  db;
	CString strSQL, strTemp;
	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return strTemp;
	}	
	
	strSQL.Format("SELECT ID FROM JigID WHERE ModuleID = %d AND JigNo = %d", nModuleID, nJigID);
//	strSQL.Format("SELECT ModuleID, No INTO FROM JigID WHERE ID = '%s'", strID);

	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		if(!rs.IsBOF() && !rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(0);
			strTemp = data.pcVal;
		}
		rs.Close();
		db.Close();
	}
	catch (CDaoException* e)
	{
//		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
	}	
	
	return strTemp;
}

int CJigIDRegDlg::LoadingData()
{
	char szName[128];
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;

	m_wndList.DeleteAllItems();

	CDaoDatabase  db;
	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		e->Delete();
		return 0;
	}	
	
	int nCnt = 0;
	CString strSQL, strTemp;
	strSQL = "SELECT ID, ModuleID, JigNo FROM JigID ORDER BY ModuleID";
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		while(!rs.IsEOF())
		{
			COleVariant data;
			data = rs.GetFieldValue(1);
			int nModuleID = data.lVal;
			data = rs.GetFieldValue(2);
			int nJigID = data.lVal;
			data = rs.GetFieldValue(0);
			strTemp = data.pcVal;

			sprintf(szName, "%s", strTemp);
			lvItem.iItem = nCnt;
			lvItem.iSubItem = 0;
			lvItem.pszText = szName;
			//lvItem.iImage = I_IMAGECALLBACK;
			m_wndList.InsertItem(&lvItem);
			m_wndList.SetItemData(lvItem.iItem,nModuleID);		//==>LVN_ITEMCHANGED 를 발생 기킴 
			
			sprintf(szName, "%s", m_pDoc->GetModuleName(nModuleID));
			m_wndList.SetItemText(nCnt, 1, szName);
			sprintf(szName, "%d", nJigID);
			m_wndList.SetItemText(nCnt, 2, szName);

			rs.MoveNext();
		}
		rs.Close();
	}
	catch (CDaoException* e)
	{
		e->Delete();
	}	
	db.Close();	

	return 0;
}

void CJigIDRegDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	m_ctrlJig.ResetContent();

	int nSel = m_ctrlModule.GetCurSel();
	if(nSel >=0)
	{
		int nModuleID = m_ctrlModule.GetItemData(nSel);
		
		CCyclerModule *pModule = m_pDoc->GetModuleInfo(nModuleID);
		if(pModule)
		{
			CString str;
			for(int c =0; c<pModule->GetTotalChannel(); c++)
			{
				str.Format("%d", c+1);
				m_ctrlJig.AddString(str);
			}
		}
	}
}