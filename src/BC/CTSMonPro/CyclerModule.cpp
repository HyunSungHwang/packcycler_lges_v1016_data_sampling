// CyclerModule.cpp: implementation of the CCyclerModule class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CyclerModule.h"

//#include <FtpDownLoad.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//#define MAX_SENSOR_TYPE	3	//0 : 온도센서, 1: 전압센서, 2:CAN 통신
#define MAX_SENSOR_TYPE	4	//0 : 온도센서, 1: 전압센서, 2:CAN 통신or온도TH    3:습도TH
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCyclerModule::CCyclerModule()
	: m_timeDisconnected(COleDateTime::GetCurrentTime())
	, m_timeConnected(COleDateTime::GetCurrentTime())
	, m_nConnectCount(0)
{
	m_lpChannelInfo = NULL;
	m_nModuleID = 0;
	m_nTotalChannel = 0;
	m_bCheckUnSafetyShutdown = FALSE;
	pAuxTemp = NULL;
	pAuxVolt = NULL;
	pAuxTempTh = NULL;
	pAuxHumi = NULL; //ksj 20200116

	pAuxTemp2 = NULL;
	pAuxVolt2 = NULL;
	pAuxTempTh2 = NULL;
	pAuxHumi2 = NULL; //ksj 20200116
	nParallelCount = 0;
	m_ThreadBuf = 0;			//쓰레드 버퍼 초기화

	ZeroMemory(&nInstalledMasterCANCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledSlaveCANCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledMasterCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledSlaveCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&m_canTransData, sizeof(SFT_MD_CAN_TRANS_INFO_DATA));
	ZeroMemory(&m_canData, sizeof(SFT_MD_CAN_INFO_DATA));
}

CCyclerModule::~CCyclerModule()
{
	RemoveChannel();	
	RemoveAllAuxCh();
}

CCyclerChannel * CCyclerModule::GetChannelInfo(int nChannelIndex)
{
	//ASSERT(m_lpChannelInfo != NULL);	//Must call MakeChannel() before use this function
	if(nChannelIndex < m_nTotalChannel)
		return &m_lpChannelInfo[nChannelIndex];

	return NULL;
}

BOOL CCyclerModule::MakeChannel(UINT nTotalCh)
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before
	if(nTotalCh < 1)	return FALSE;
	
	
	//채널 수가 변경 되었거나 최초일 경우  
	if(nTotalCh != m_nTotalChannel || m_lpChannelInfo == NULL)
	{		
		RemoveChannel(); //기본적으로 채널은 모두 초기화
		m_lpChannelInfo = new CCyclerChannel[nTotalCh];
		ASSERT(m_lpChannelInfo);
		for(WORD i=0; i<nTotalCh; i++)
		{
			m_lpChannelInfo[i].SetID(m_nModuleID, i);
		}
		m_nTotalChannel = nTotalCh;
	}
	else
	{
		for(WORD i=0; i<nTotalCh; i++)
		{
			m_lpChannelInfo[i].ResetData();
		}
	}
	return TRUE;
}

void CCyclerModule::RemoveChannel()
{
	if(m_lpChannelInfo != NULL)
	{
		delete [] m_lpChannelInfo;

	}
	m_lpChannelInfo = NULL;
}

WORD CCyclerModule::GetState()
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before
	return ::SFTGetModuleState(m_nModuleID);
}

// apChList: Selected Channel Index Array
int CCyclerModule::SendCommand2(UINT nCmd, CWordArray *apChList, LPVOID lpData, UINT nSize, UINT nWaitTime)
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before

	SFT_CH_SEL_DATA selBitData;
	ZeroMemory(&selBitData, sizeof(selBitData));		//Channel 선택 Bit 정보

	if(apChList != NULL)
	{
		if(apChList->GetSize() <= 0)	return SFT_NACK;
	
		DWORD dwBitMask = 0x00000001;
		WORD nChIndex;
		for(int i =0; i<apChList->GetSize(); i++)
		{
			nChIndex = apChList->GetAt(i);
			
			//ksj 20171025 : 주석처리.
			
			TRACE("chindex = %d \n",nChIndex);
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
			{
				if (nChIndex == 1) nChIndex = 2;		//ljb 20170629 강제로 채널 변경 //병렬 mark
			}
//#endif
			if( nChIndex < sizeof(DWORD)*8 )
			{
				selBitData.nSelBitData[0] |= dwBitMask << nChIndex;
			}
			else
			{
				selBitData.nSelBitData[1] |= dwBitMask << (nChIndex-sizeof(DWORD)*8);
			}
		}
	}
	else	//Send Command to Module
	{

	}
	/*if(nCmd == SFT_CMD_USERPATTERN_DATA)
	{
		SFT_STEP_SIMUL_CONDITION temp;
		memcpy(&temp, lpData, nSize);
		int i = 0;
		i = 1;
	}*/
	return ::SFTSendCommand(m_nModuleID,  nCmd, &selBitData, lpData, nSize, nWaitTime);
}

int CCyclerModule::SendCommand(UINT nCmd, CWordArray *apChList, LPVOID lpData, UINT nSize)
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before

	SFT_CH_SEL_DATA selBitData;
	ZeroMemory(&selBitData, sizeof(selBitData));		//Channel 선택 Bit 정보

	if(apChList != NULL)
	{
		if(apChList->GetSize() <= 0)	return SFT_NACK;
	
		DWORD dwBitMask = 0x00000001;
		WORD nChIndex;
		for(int i =0; i<apChList->GetSize(); i++)
		{
			nChIndex = apChList->GetAt(i);
			
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
			{
				//ksj 20171025 : 주석처리.
				TRACE("chindex = %d \n",nChIndex);
				if (nCmd != SFT_CMD_CHAMBER_USING)
				{
					if (nChIndex == 1) nChIndex = 2;		//ljb 20170629 강제로 채널 변경
				}
			}
//#endif
			
			if( nChIndex < sizeof(DWORD)*8 )
			{
				selBitData.nSelBitData[0] |= dwBitMask << nChIndex;
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
				{
					//20180313 yulee 추가 4ch에서 2ch 처럼 보이기 소스 
					if (nCmd != SFT_CMD_CHAMBER_USING)
					{
						if(nChIndex == 2)
						{
							selBitData.nSelBitData[1] |= dwBitMask << (nChIndex-sizeof(DWORD)*8);
						}
					}
					//20180313 yulee 추가 4ch에서 2ch 처럼 보이기 소스 끝
				}
//#endif
			}
			else
			{
				selBitData.nSelBitData[1] |= dwBitMask << (nChIndex-sizeof(DWORD)*8);
			}
		}
	}
	else	//Send Command to Module
	{

	}
	/*if(nCmd == SFT_CMD_USERPATTERN_DATA)
	{
		SFT_STEP_SIMUL_CONDITION temp;
		memcpy(&temp, lpData, nSize);
		int i = 0;
		i = 1;
	}*/
	return ::SFTSendCommand(m_nModuleID,  nCmd, &selBitData, lpData, nSize);
}

// nChIndex: Selected Channel Index
int CCyclerModule::SendCommand(UINT nCmd, int nChIndex, LPVOID lpData, UINT nSize)
{
	ASSERT(m_nModuleID >0);		//Call  SetModuleID before

	SFT_CH_SEL_DATA selBitData;
	ZeroMemory(&selBitData, sizeof(selBitData));		//Channel 선택 Bit 정보
	
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
	{
		//ksj 20171025 : 주석처리.
		TRACE("chindex = %d \n",nChIndex);
		if (nChIndex == 1) nChIndex = 2;		//ljb 20170629 강제로 채널 변경
	}
//#endif
	
	if(nChIndex >= 0)
	{
		DWORD dwBitMask = 0x00000001;
		if( nChIndex < sizeof(DWORD)*8 )
		{
			selBitData.nSelBitData[0] |= dwBitMask << nChIndex;
		}
		else
		{
			selBitData.nSelBitData[1] |= dwBitMask << (nChIndex-sizeof(DWORD)*8);
		}
	}
	return ::SFTSendCommand(m_nModuleID,  nCmd, &selBitData, lpData, nSize);

}

// nChIndex: Selected Channel Index
int CCyclerModule::SendCommand(UINT nCmd, LPVOID lpData, UINT nSize)
{
	return SendCommand(nCmd, NULL, lpData, nSize);
}

CString CCyclerModule::GetCmdErrorMsg(int nCode)
{
	return "UnKnown";
}

void CCyclerModule::SetModuleID(int nModuleIndex, int nModuleID, SFT_SYSTEM_PARAM	*pParam)
{
	ASSERT(nModuleIndex >=0);
	ASSERT(pParam);

	m_nModuleID = nModuleID;
	::SFTSetSysParam(nModuleIndex, nModuleID, pParam);
	
	//Make default Channel
	MakeChannel(::SFTGetModule(nModuleID)->GetChannelCount());
}

float CCyclerModule::GetVoltageSpec(int nRange /*= CAL_RANGE1*/)
{
//	SFTGetSysParam(int nModuleID);
	
		SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
		if(pSys == NULL)						return 0.0;
		if(nRange < 0 || nRange >= 5)			return 0.0;
		float fSpec = (float)pSys->nVoltageSpec[nRange];
/*		if(pSys->byVoltageUnitType == 1)	//nano unit
		{
			fSpec /= 1000.0f; 
		}
*/		return LONG2FLOAT(fSpec);
}

float CCyclerModule::GetCurrentSpec(int nRange /*= CAL_RANGE1*/)
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	if(pSys == NULL)						return 0.0;
	if(nRange < 0 || nRange >= 5)			return 0.0;

	float fSpec = (float)pSys->nCurrentSpec[nRange];
/*	if(pSys->byCurrentUnitType == 1)	//nano unit
	{
		fSpec /= 1000.0f; 
	}
*/	return LONG2FLOAT(fSpec);
}

int	CCyclerModule::GetCurrentRangeCount()
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	if(pSys == NULL)	0;

	if(pSys->wCurrentRange < 1 || pSys->wCurrentRange > 5)	return 0;	//Network변수는 반드시 Check한다.
	return pSys->wCurrentRange;
}

int		CCyclerModule::GetVoltageRangeCount()
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	if(pSys == NULL)	0;
	if(pSys->wVoltageRange < 1 || pSys->wVoltageRange > 5)	return 0;
	return pSys->wVoltageRange;
}

CString CCyclerModule::GetIPAddress()
{
	CString address(::SFTGetIPAddress(m_nModuleID));
	return address;
}


CString CCyclerModule::GetConnectedTime()
{
	SYSTEMTIME *pTime = ::SFTGetConnectedTime(m_nModuleID);	
	if(pTime == FALSE)	return "";

	CTime time(*pTime);
	return time.Format("%Y/%m/%d %H:%M:%S");
}

CString CCyclerModule::GetConnectionElapsedTime()
{
	SYSTEMTIME *pTime = ::SFTGetConnectedTime(m_nModuleID);	
	if(pTime == FALSE)	return "";
	
	SYSTEMTIME  curTime; 
	::GetLocalTime(&curTime);
	
	CTime cononnectTime(*pTime);
	CTime currentTime(curTime);
	CTimeSpan elapsedTime = currentTime - cononnectTime;

	return elapsedTime.Format("%DDay %H:%M:%S");
}

BOOL CCyclerModule::CheckRunningChannel()
{
	CString strMsg;
	CCyclerChannel *pCh;

	for(WORD i=0; i< GetTotalChannel(); i++)
	{
		pCh = GetChannelInfo(i);
		if(pCh)
		{
//			모듈이 접속하면 무조건 최종 시험 정보를 Loading한다.
//			WORD state = pCh->GetState();
//			if(pCh->GetState() == PS_STATE_RUN || pCh->GetState() == PS_STATE_PAUSE)
//			{
				if(pCh->LoadInfoFromTempFile() == FALSE)
				{
//					strMsg.Format("%s Ch %d 이전 시험정보 임시파일 Loading 실패", GetModuleName(nModuleID), i+1);
//					WriteLog(strMsg);
				}//$1013
				//pCh->WriteLog("시험 정보 로드 완료.");
				pCh->WriteLog(Fun_FindMsg("CyclerModule_CheckRunningChannel_msg1","CYCLERMODULE"));//&&
//			}
		}
	}	

	m_bCheckUnSafetyShutdown = TRUE;
	return TRUE;
}

//모듈에 있는 Save Last Index 파일을 DownLoad하여 
//PC 저장파일 Index와 비교하여 손실여부를 확인한다.
int CCyclerModule::UpdateDataLossState(CWordArray *pSelCh, BOOL bOnLineMode)
{

#ifdef _DEBUG
		DWORD dwStart = GetTickCount();
#endif		

	if(bOnLineMode)		//모듈이 Online(즉 Network가 확실히 연결된 경우)시만 접속함 => 시간 지연 없에기 위해 
	{
		if( GetState() == PS_STATE_LINE_OFF)
		{
			return 0;
		}
	}

	CString strTemp;
	CWordArray aSelectCh;
	if(pSelCh == NULL)
	{
		for(WORD i=0; i<m_nTotalChannel; i++)
		{
			aSelectCh.Add(i);
		}
	}
	else
	{
		for(WORD k=0; k<pSelCh->GetSize(); k++)
		{
			aSelectCh.Add(pSelCh->GetAt(k));
		}
	}
	
	int nLostRange = 0;
	CString strIP = GetIPAddress();
	if(strIP.IsEmpty())	
	{
		return 0;
	}
	//$1013
	//CProgressWnd progressWnd(AfxGetMainWnd(), "Data 손실 검사...", TRUE, TRUE, TRUE);
	CProgressWnd progressWnd(AfxGetMainWnd(), Fun_FindMsg("CyclerModule_UpdateDataLossState_msg1","CYCLERMODULE"), TRUE, TRUE, TRUE);//&&
	progressWnd.SetRange(0, 100);
//$1013
	//strTemp.Format("모듈 %d에 이전 작업 정보 요청 중...", m_nModuleID);
	strTemp.Format(Fun_FindMsg("CyclerModule_UpdateDataLossState_msg2","CYCLERMODULE"), m_nModuleID);//&&
	progressWnd.SetText(strTemp);
	CFtpDownLoad *pDownLoad = new CFtpDownLoad();
	
	if(pDownLoad->ConnectFtpSession(strIP)== FALSE)
	{
		delete pDownLoad;
		return 0;
	}

	char szBuff[_MAX_PATH];
	::GetTempPath(_MAX_PATH, szBuff);		//Get Windows temp. folder
	CString strLocalTempFolder(szBuff);
	CString strRemoteFileName, strStartIndexFile, strLastIndexFile;

	progressWnd.SetPos(10);
	int nChannelIndex;
	int nTotSelCount = aSelectCh.GetSize();
	long lDataTemp = 0;
	for(int k=0; k<nTotSelCount; k++)
	{
		nChannelIndex = aSelectCh.GetAt(k);//$1013
		//strTemp.Format("모듈 %d의 채널 %d 최종 작업 정보 분석 중...", m_nModuleID, nChannelIndex+1);
		strTemp.Format(Fun_FindMsg("CyclerModule_UpdateDataLossState_msg3","CYCLERMODULE"), m_nModuleID, nChannelIndex+1);//&&
		progressWnd.SetText(strTemp);
		progressWnd.SetPos(10+int((float)k/(float)nTotSelCount*90.0f));
				
		if(progressWnd.PeekAndPump() == FALSE)	
			break;

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
					//ksj 20171117 : 주석처리
		if (nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1/data0//ch%03d//savingFileIndex_start.csv", nChannelIndex+2);	//ljb 20170829 add
		else strRemoteFileName.Format("cycler_data//resultData//group1/data0//ch%03d//savingFileIndex_start.csv", nChannelIndex+1);
	
		}
//#else
else
		{
			//ksj 20171117 : 원복
		strRemoteFileName.Format("cycler_data//resultData//group1/data0//ch%03d//savingFileIndex_start.csv", nChannelIndex+1);
				
				
		}
//#endif

		strStartIndexFile.Format("%s\savingFileIndex_start.csv", strLocalTempFolder, nChannelIndex);
//$1013
		//strTemp.Format("공정 시작전 기존공정 손실검사 모듈 %d의 채널 %d savingFileIndex_start 다운로드중...(%s)", m_nModuleID, nChannelIndex+1,strStartIndexFile);
		strTemp.Format(Fun_FindMsg("CyclerModule_UpdateDataLossState_msg4","CYCLERMODULE"), m_nModuleID, nChannelIndex+1,strStartIndexFile);//&&
		sprintf(szBuff, strTemp);
		SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

		if(pDownLoad->DownLoadFile(strStartIndexFile, strRemoteFileName) < 1)
		{//$1013
			//sprintf(szBuff, "결과 data 시작 정보 Index 요청실패...");
			sprintf(szBuff, Fun_FindMsg("CyclerModule_UpdateDataLossState_msg5","CYCLERMODULE"));//&&
			SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
			continue;
		}

//#ifdef _PARALLEL_FORCE 전처리기 주석처리 //yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			if (nChannelIndex == 1) strRemoteFileName.Format("cycler_data//resultData//group1/data0//ch%03d//savingFileIndex_last.csv", nChannelIndex+2);	//ljb 20170829 add
			else strRemoteFileName.Format("cycler_data//resultData//group1/data0//ch%03d//savingFileIndex_last.csv", nChannelIndex+1);
			strLastIndexFile.Format("%s\savingFileIndex_last.csv", strLocalTempFolder, nChannelIndex); //lyj 20200601 복구 개선
			
		}
//#else
else
		{							//ksj 20171117 : 원복
			strRemoteFileName.Format("cycler_data//resultData//group1/data0//ch%03d//savingFileIndex_last.csv", nChannelIndex+1);
			strLastIndexFile.Format("%s\savingFileIndex_last.csv", strLocalTempFolder, nChannelIndex);		
		}	
//#endif
		//$1013
		//strTemp.Format("공정 시작전 기존공정 손실검사 모듈 %d의 채널 %d savingFileIndex_last 다운로드중...(%s)", m_nModuleID, nChannelIndex+1,strLastIndexFile);
		strTemp.Format(Fun_FindMsg("CyclerModule_UpdateDataLossState_msg6","CYCLERMODULE"), m_nModuleID, nChannelIndex+1,strLastIndexFile);//&&
		sprintf(szBuff, strTemp);
		SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

		if(pDownLoad->DownLoadFile(strLastIndexFile, strRemoteFileName) < 1)
		{//$1013
			//sprintf(szBuff, "결과 data 종료 정보 Index 요청실패...");
			sprintf(szBuff, Fun_FindMsg("CyclerModule_UpdateDataLossState_msg7","CYCLERMODULE"));//&&
			SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
			continue;
		}		
//$1013
		//strTemp.Format("손실테이터 검사시작 모듈 %d의 채널 %d ...", m_nModuleID, nChannelIndex+1);
		strTemp.Format(Fun_FindMsg("CyclerModule_UpdateDataLossState_msg8","CYCLERMODULE"), m_nModuleID, nChannelIndex+1);//&&
		sprintf(szBuff, strTemp);
		SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
		
		//SBC에서 Download한 파일과 PC파일을 비교하여 손실구간이 있는지 확인한다.
		if(m_lpChannelInfo[nChannelIndex].SearchLostDataRange(strStartIndexFile, strLastIndexFile, lDataTemp, &progressWnd) == TRUE)
		{
			nLostRange += lDataTemp;
		}
		else
		{//$1013
			//sprintf(szBuff, "손실구간 검색 실패.(%s)", m_lpChannelInfo[nChannelIndex].GetLastErrorString());
			sprintf(szBuff, Fun_FindMsg("CyclerModule_UpdateDataLossState_msg9","CYCLERMODULE"), m_lpChannelInfo[nChannelIndex].GetLastErrorString());//&&
			SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);
			TRACE("%s\n", strTemp);
		}
			
	}
		
	progressWnd.SetPos(100);
	delete pDownLoad;

#ifdef _DEBUG
		dwStart = GetTickCount() - dwStart;
		TRACE("%%%%%%%%%%%%%%%%%%%%%%%%%% FTP Check %d msec%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n", dwStart);
#endif	
	
	//sprintf(szBuff, "손실구간 검색 완료.");//$1013
	sprintf(szBuff, Fun_FindMsg("CyclerModule_UpdateDataLossState_msg10","CYCLERMODULE"));//$1013//&&
	SendMessage(AfxGetMainWnd()->m_hWnd, SFTWM_WRITE_LOG, (WPARAM)(MAKELONG(nChannelIndex, m_nModuleID)), (LPARAM)szBuff);

	return nLostRange;
}

UINT CCyclerModule::GetProtocolVer()
{
	SFT_MD_SYSTEM_DATA *pSys = ::SFTGetModuleSysData(m_nModuleID);
	return pSys->nProtocolVersion;
}

void CCyclerModule::SetParallelData(SFT_MD_PARALLEL_DATA paralData)
{
	SFT_MD_PARALLEL_DATA tmpParallelData;
	memcpy(&tmpParallelData, &m_parallelData[paralData.chNoMaster - 1], sizeof(SFT_MD_PARALLEL_DATA)); //yulee 20190830

	if(paralData.bParallel == 1)
	{
		//SFT_MD_PARALLEL_DATA tmpParallelData;
		//memcpy(&tmpParallelData, &m_parallelData[paralData.chNoMaster - 1], sizeof(SFT_MD_PARALLEL_DATA)); //yulee 20190830
		memcpy(&m_parallelData[paralData.chNoMaster - 1], &paralData, sizeof(SFT_MD_PARALLEL_DATA)); 
		//parallel의 병렬에 관한 값만 &paralData에서 &m_parallelData[paralData.chNoMaster - 1]로 저장한다.
		//memcpy로 하면 기존에 챔버 연동 등등의 값이 &paralData값으로 변경된다.



		m_parallelData[paralData.chNoSlave[0] -1].chNoMaster = 0;
		m_parallelData[paralData.chNoSlave[0] -1].bParallel = 0;
		if (paralData.chNoSlave[1] != 0)
		{
			m_parallelData[paralData.chNoSlave[1] -1].chNoMaster = 0;
			m_parallelData[paralData.chNoSlave[1] -1].bParallel = 0;

			//ksj 20200214 : Slave 채널은 Slave를 갖지 못하도록 수정
			//강제병렬 기능시 2+2 = 4 병렬시 간혹 slave 채널이 slave를 갖고 있어 오동작하는 현상 수정
			// 예) 4채널 병렬시 3번채널이 slave를 4로 가지고 있을때 전류가 3/4 만 출력됨
			// 이 수정으로 인해 혹시 다른 병렬 이슈가 발생할 수도 있으므로 주의
			for(int nSlaveIdx=0;nSlaveIdx<3;nSlaveIdx++)
				m_parallelData[paralData.chNoSlave[1] -1].chNoSlave[nSlaveIdx] = 0;
			//ksj end
		}
		if (paralData.chNoSlave[2] != 0)
		{
			m_parallelData[paralData.chNoSlave[2] -1].chNoMaster = 0;
			m_parallelData[paralData.chNoSlave[2] -1].bParallel = 0;

			//ksj 20200214 : Slave 채널은 Slave를 갖지 못하도록 수정
			//강제병렬 기능시 2+2 = 4 병렬시 간혹 slave 채널이 slave를 갖고 있어 오동작하는 현상 수정
			// 예) 4채널 병렬시 3번채널이 slave를 4로 가지고 있을때 전류가 3/4 만 출력됨
			// 이 수정으로 인해 혹시 다른 병렬 이슈가 발생할 수도 있으므로 주의
			for(int nSlaveIdx=0;nSlaveIdx<3;nSlaveIdx++)
				m_parallelData[paralData.chNoSlave[2] -1].chNoSlave[nSlaveIdx] = 0;
			//ksj end
		}
		CString strTmp;
		strTmp.Format("master %d, slave1 %d, slave2 %d, slave3 %d", 
			paralData.chNoMaster, 
			paralData.chNoSlave[0],
			paralData.chNoSlave[1],
			paralData.chNoSlave[2]);
		log_All(_T("exe"),_T("SetParallelData"),strTmp);
	}
	else
	{
		m_parallelData[paralData.chNoMaster - 1].chNoSlave[0] = 0;
		m_parallelData[paralData.chNoMaster - 1].bParallel = 0;
		m_parallelData[paralData.chNoSlave[0] -1].chNoMaster = paralData.chNoSlave[0] -1;
		m_parallelData[paralData.chNoSlave[0] -1].bParallel = 0;
		if (paralData.chNoSlave[1] != 0)
		{
			m_parallelData[paralData.chNoSlave[1] -1].chNoMaster = paralData.chNoSlave[1] -1;
			m_parallelData[paralData.chNoSlave[1] -1].bParallel = 0;

			//ksj 20200214 : Slave 채널은 Slave를 갖지 못하도록 수정
			// 이 수정으로 인해 혹시 다른 병렬 이슈가 발생할 수도 있으므로 주의
			for(int nSlaveIdx=0;nSlaveIdx<3;nSlaveIdx++)
				m_parallelData[paralData.chNoSlave[1] -1].chNoSlave[nSlaveIdx] = 0;
			//ksj end
		}
		if (paralData.chNoSlave[2] != 0)
		{
			m_parallelData[paralData.chNoSlave[2] -1].chNoMaster = paralData.chNoSlave[2] -1;
			m_parallelData[paralData.chNoSlave[2] -1].bParallel = 0;

			//ksj 20200214 : Slave 채널은 Slave를 갖지 못하도록 수정
			// 이 수정으로 인해 혹시 다른 병렬 이슈가 발생할 수도 있으므로 주의
			for(int nSlaveIdx=0;nSlaveIdx<3;nSlaveIdx++)
				m_parallelData[paralData.chNoSlave[2] -1].chNoSlave[nSlaveIdx] = 0;
			//ksj end
		}
		
	}

	//===================================================================================================//yulee 20190830 
	m_parallelData[paralData.chNoMaster - 1].reserved[0] = tmpParallelData.reserved[0];
	m_parallelData[paralData.chNoMaster - 1].reserved[1] = tmpParallelData.reserved[1];
	m_parallelData[paralData.chNoMaster - 1].reserved[2] = tmpParallelData.reserved[2];

	m_parallelData[paralData.chNoMaster - 1].reserved2[0] = tmpParallelData.reserved2[0];
	m_parallelData[paralData.chNoMaster - 1].reserved2[1] = tmpParallelData.reserved2[1];
	m_parallelData[paralData.chNoMaster - 1].reserved2[2] = tmpParallelData.reserved2[2];
	m_parallelData[paralData.chNoMaster - 1].reserved2[3] = tmpParallelData.reserved2[3];
	//===================================================================================================//yulee 20190830 

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			//ksj 20171123 : 주석처리
			int i;
			int nParallelCnt=0;
			for( int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
			{
				if(m_parallelData[i].bParallel == TRUE) nParallelCnt++;
			}

			if (nParallelCnt == 2)
			{
				for( int i = 0 ; i < GetTotalChannel(); i++)
				{
					//CCyclerChannel * pCh = GetChannelInfo(nParallelCnt-1);
					CCyclerChannel * pCh = GetChannelInfo(i);
					pCh->SetParallel(FALSE, FALSE);
					//pCh->SetMasterChannelNum(i+1);
				}
			}
			else
			{
				for( int i = 0 ; i < GetTotalChannel(); i++)
				{
					CCyclerChannel * pCh = GetChannelInfo(i);
					if(i+1 == paralData.chNoMaster)
						pCh->SetParallel(paralData.bParallel, paralData.bParallel);
					if(i+1 == paralData.chNoSlave[0])
					{
						pCh->SetParallel(paralData.bParallel, FALSE);
						pCh->SetMasterChannelNum(paralData.chNoMaster);
					}
					if(i+1 == paralData.chNoSlave[1])
					{
						pCh->SetParallel(paralData.bParallel, FALSE);
						pCh->SetMasterChannelNum(paralData.chNoMaster);
					}
					if(i+1 == paralData.chNoSlave[2])
					{
						pCh->SetParallel(paralData.bParallel, FALSE);
						pCh->SetMasterChannelNum(paralData.chNoMaster);
					}
				}
			}
		}
//#else
else
		{
					//ksj 20171123 : 원상 복구
			for(int i = 0 ; i < GetTotalChannel(); i++)
			{
				CCyclerChannel * pCh = GetChannelInfo(i);
				if(i+1 == paralData.chNoMaster)
					pCh->SetParallel(paralData.bParallel, paralData.bParallel);
				if(i+1 == paralData.chNoSlave[0])
				{
					pCh->SetParallel(paralData.bParallel, FALSE);
					pCh->SetMasterChannelNum(paralData.chNoMaster);
				}
				if(i+1 == paralData.chNoSlave[1])
				{
					pCh->SetParallel(paralData.bParallel, FALSE);
					pCh->SetMasterChannelNum(paralData.chNoMaster);
				}
				if(i+1 == paralData.chNoSlave[2])
				{
					pCh->SetParallel(paralData.bParallel, FALSE);
					pCh->SetMasterChannelNum(paralData.chNoMaster);
				}
			}
		}
//#endif
}

//kjh Temp 
//파일로 부터 Aux 데이터를 불러온다.


int CCyclerModule::GetTotalSensor()
{
	return this->nTotalSensor;
}


int CCyclerModule::GetMaxSensorType()
{
	return MAX_SENSOR_TYPE;
}

void CCyclerModule::RemoveAuxData(STF_MD_AUX_SET_DATA delAux)
{
	switch(delAux.auxType)
	{
	case 0: pAuxTemp2[delAux.auxChNo-1].RemoveAuxData();	break; 
	case 1: pAuxVolt2[delAux.auxChNo-1].RemoveAuxData();	break; 
	case 2: pAuxTempTh2[delAux.auxChNo-1].RemoveAuxData();	break;
	case 3: pAuxHumi2[delAux.auxChNo-1].RemoveAuxData();	break; //ksj 20200116
	default: break;
	}

}


CChannelSensor * CCyclerModule::GetAuxData(int nSensorIndex, int nSensorType)
{
	switch(nSensorType)
	{
	case 0: 
		if(!pAuxTemp2)	return NULL; //ksj 20200129

		if(nSensorIndex < GetMaxTemperature() && nSensorIndex >= 0)
			return &pAuxTemp2[nSensorIndex]; 
		else
		{
			return NULL;
		}
	case 1: 
		if(!pAuxVolt2)	return NULL; //ksj 20200129

		if(nSensorIndex < GetMaxVoltage() && nSensorIndex >= 0)
			return &pAuxVolt2[nSensorIndex]; 
		else
			return NULL;
	case 2:
		if(!pAuxTempTh2) return NULL; //ksj 20200129

		if(nSensorIndex < GetMaxVoltage() + GetMaxTemperatureTh() && nSensorIndex >= 0)
			return &pAuxTempTh2[nSensorIndex-GetMaxVoltage()]; 
		else
			return NULL;
	case 3: //ksj 20200116 : v1016 습도. 확인 필요. 가구현
		if(nSensorIndex < GetMaxVoltage() + GetMaxTemperatureTh() + GetMaxHumidity() && nSensorIndex >= 0)
		{
#ifdef _DEBUG
			int nMaxTempTh = GetMaxTemperatureTh();
			int nMaxVol = GetMaxVoltage();
			int nIdx = nSensorIndex - nMaxVol - nMaxTempTh;
#endif
			return &pAuxHumi2[nSensorIndex-GetMaxVoltage()-GetMaxTemperatureTh()];
		}
		else
			return NULL;
	default : return NULL; 
	}
}

void CCyclerModule::MakeParallel()
{	
	memcpy(m_parallelData, ::SFTGetParallelData(m_nModuleID), sizeof(SFT_MD_PARALLEL_DATA)* _SFT_MAX_PARALLEL_COUNT);
	
	int i;
	int nParallelCnt=0;
	for(int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
	{
		if(m_parallelData[i].bParallel == TRUE) nParallelCnt++;
	}

	for(int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
	{
	
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
		{
			if (nParallelCnt == 1)
			{
				if(m_parallelData[i].bParallel == TRUE)
				{
					CCyclerChannel *pCh = GetChannelInfo(m_parallelData[i].chNoMaster - 1);
					//marster
					pCh->SetParallel(m_parallelData[i].bParallel, TRUE);
				
					//ljb 20120705 ch1
					pCh = GetChannelInfo(m_parallelData[i].chNoSlave[0] - 1);
					pCh->SetParallel(m_parallelData[i].bParallel, FALSE);
					pCh->SetMasterChannelNum(m_parallelData[i].chNoMaster);
				
				}
			}
		}
//#else
else
		{
			if(m_parallelData[i].bParallel == TRUE)
			{
				CCyclerChannel *pCh = GetChannelInfo(m_parallelData[i].chNoMaster - 1);
				if(!pCh) continue; //ksj 20171025
				//marster
				pCh->SetParallel(m_parallelData[i].bParallel, TRUE);
				
				//ljb 20120705 ch1
				pCh = GetChannelInfo(m_parallelData[i].chNoSlave[0] - 1);
				if(pCh != NULL)//yulee 20190712
				{
					pCh->SetParallel(m_parallelData[i].bParallel, FALSE);
					pCh->SetMasterChannelNum(m_parallelData[i].chNoMaster);
				}



				//20180305 yulee Ch 숫자에 따른 병렬 표시 
				int nTotCh=0;
				nTotCh = GetTotalChannel();
					
				if(nTotCh <= 2)
				{

				} 
				else if(nTotCh == 4)
				{		
					//ljb 20120705 ch2
					if (m_parallelData[i].chNoSlave[1] != 0)
					{
						pCh = GetChannelInfo(m_parallelData[i].chNoSlave[1] - 1);
						pCh->SetParallel(m_parallelData[i].bParallel, FALSE);
						pCh->SetMasterChannelNum(m_parallelData[i].chNoMaster);
					}
					//ljb 20120705 ch3
					if (m_parallelData[i].chNoSlave[2] != 0)
					{
						pCh = GetChannelInfo(m_parallelData[i].chNoSlave[2] - 1);
						pCh->SetParallel(m_parallelData[i].bParallel, FALSE);
						pCh->SetMasterChannelNum(m_parallelData[i].chNoMaster);
	 				}	
				}
			}
		}	
//#endif
	}	
}

//병렬 연결 정보를 파일로부터 읽어온다.


int CCyclerModule::GetParallelCount()
{
	return nParallelCount;
}

SFT_MD_PARALLEL_DATA * CCyclerModule::GetParallelData()
{
	return m_parallelData;
}


int CCyclerModule::GetMaxTemperature()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL){
		return 0;
	}
	//2014.09.29	
	return pAuxInfo->wInstalledTemp;
	/*
#ifndef _DEBUG
	return pAuxInfo->wInstalledTemp;
#else
	return 2;
#endif
	*/
}

int CCyclerModule::GetMaxVoltage()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL){
		return 0;
	}
	//2014.09.29	
	return pAuxInfo->wInstalledVolt;
	/*
#ifndef _DEBUG
	return pAuxInfo->wInstalledVolt;
#else
	return 2;
#endif
	*/
}

int CCyclerModule::GetMaxTemperatureTh()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL)						return 0;
	
	return pAuxInfo->wInstalledTempTh;
}

//ksj 20200116 : V1016 습도
int CCyclerModule::GetMaxHumidity()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL)						return 0;

	return pAuxInfo->wInstalledHumi;
}

void CCyclerModule::MakeAuxData()
{
	int nAuxType = 0;

	if(pAuxTemp != NULL)
	{
		delete [] pAuxTemp;
		delete [] pAuxTemp2;
		pAuxTemp = pAuxTemp2 = NULL;
	}
	if(pAuxVolt != NULL)
	{
		delete [] pAuxVolt;
		delete [] pAuxVolt2;
		pAuxVolt = pAuxVolt2 = NULL;
	}
	if(pAuxTempTh != NULL)
	{
		delete [] pAuxTempTh;
		delete [] pAuxTempTh2;
		pAuxTempTh = pAuxTempTh2 = NULL;

	}
	//ksj 20200116
	if(pAuxHumi != NULL)
	{
		delete [] pAuxHumi;
		delete [] pAuxHumi2;
		pAuxHumi = pAuxHumi2 = NULL;

	}
	//ksj end
	for(nAuxType = 0 ; nAuxType < MAX_SENSOR_TYPE; nAuxType++)
	{
		
		switch(nAuxType)
		{
		case 0:		if(GetMaxTemperature() > 0)
					{
						pAuxTemp = new CChannelSensor[GetMaxTemperature()];
						
						for(int i = 0 ; i < GetMaxTemperature(); i++)
						{
							STF_MD_AUX_SET_DATA *auxData = new STF_MD_AUX_SET_DATA;
							ZeroMemory(auxData, sizeof(STF_MD_AUX_SET_DATA));
							auxData->auxType = nAuxType;
							auxData->auxChNo = i+1;
							pAuxTemp[i].SetAuxData(auxData);
							pAuxTemp[i].SetInstall(TRUE);
							pAuxTemp[i].SetMasterChannel(0);
							delete auxData;
						}
				
						pAuxTemp2 = new CChannelSensor[GetMaxTemperature()];
						memcpy(pAuxTemp2, pAuxTemp, sizeof(CChannelSensor) * GetMaxTemperature());
					}
					break;
		case 1:		if(GetMaxVoltage() > 0)
					{
						pAuxVolt = new CChannelSensor[GetMaxVoltage()];				
						for(int i = 0 ; i < GetMaxVoltage(); i++)
						{
							STF_MD_AUX_SET_DATA *auxData = new STF_MD_AUX_SET_DATA;
							ZeroMemory(auxData, sizeof(STF_MD_AUX_SET_DATA));
							auxData->auxType = nAuxType;
							auxData->auxChNo = i+1;
							pAuxVolt[i].SetAuxData(auxData);
							pAuxVolt[i].SetInstall(TRUE);
							pAuxVolt[i].SetMasterChannel(0);
							delete auxData;
						}

						pAuxVolt2 = new CChannelSensor[GetMaxVoltage()];
						memcpy(pAuxVolt2, pAuxVolt, sizeof(CChannelSensor) * GetMaxVoltage());
					}
					break;
		case 2:		if(GetMaxTemperatureTh() > 0)
					{
						pAuxTempTh  = new CChannelSensor[GetMaxTemperatureTh()];
						for(int i = 0 ; i < GetMaxTemperatureTh(); i++)
						{
							STF_MD_AUX_SET_DATA *auxData = new STF_MD_AUX_SET_DATA;
							ZeroMemory(auxData, sizeof(STF_MD_AUX_SET_DATA));
							auxData->auxType = nAuxType;
							//auxData->auxChNo = i + 1;
							auxData->auxChNo = i + 1 + GetMaxVoltage(); //ksj 20160627
							pAuxTempTh[i].SetAuxData(auxData);
							pAuxTempTh[i].SetInstall(TRUE);
							pAuxTempTh[i].SetMasterChannel(0);
							delete auxData;
						}

						pAuxTempTh2  = new CChannelSensor[GetMaxTemperatureTh()];
						memcpy(pAuxTempTh2, pAuxTempTh, sizeof(CChannelSensor) * GetMaxTemperatureTh());
					}
					break;
		case 3:		if(GetMaxHumidity() > 0) //ksj 20200116 : v1016 습도 추가. 가구현. 확인 필요.
					{
						pAuxHumi  = new CChannelSensor[GetMaxHumidity()];
						for(int i = 0 ; i < GetMaxHumidity(); i++)
						{
							STF_MD_AUX_SET_DATA *auxData = new STF_MD_AUX_SET_DATA;
							ZeroMemory(auxData, sizeof(STF_MD_AUX_SET_DATA));
							auxData->auxType = nAuxType;
							//auxData->auxChNo = i + 1;
							auxData->auxChNo = i + 1 + GetMaxVoltage() + GetMaxTemperatureTh();
							pAuxHumi[i].SetAuxData(auxData);
							pAuxHumi[i].SetInstall(TRUE);
							pAuxHumi[i].SetMasterChannel(0);
							delete auxData;
						}

						pAuxHumi2  = new CChannelSensor[GetMaxHumidity()];
						memcpy(pAuxHumi2, pAuxHumi, sizeof(CChannelSensor) * GetMaxHumidity());
					}
					break;
		default : break; 
		}
	}
	
	SetInstalledAux();
}

void CCyclerModule::SetInstalledAux()
{
	SFT_MD_AUX_INFO_DATA *pAuxInfo = ::SFTGetAuxInfoData(m_nModuleID);
	if(pAuxInfo == NULL)						return;

	for(int i = 0 ; i < _SFT_MAX_MAPPING_AUX; i++)
	{
		if(pAuxInfo->auxData[i].chNo > 0)
		{
			CChannelSensor * pSensor = GetAuxData(pAuxInfo->auxData[i].auxChNo-1, pAuxInfo->auxData[i].auxType);
			if(pSensor == NULL)
				continue;

			STF_MD_AUX_SET_DATA auxData;
			memcpy(&auxData, &pAuxInfo->auxData[i], sizeof(STF_MD_AUX_SET_DATA));
			pSensor->SetAuxData(&auxData);
			pSensor->SetMasterChannel(pAuxInfo->auxData[i].chNo);

		}
		
	}
	ApplyAuxData();
}

void CCyclerModule::GetTemperatureAuxList(CChannelSensor *pList)
{
	memcpy(pList, pAuxTemp, sizeof(CChannelSensor)*GetMaxTemperature());
}

void CCyclerModule::GetVoltageAuxList(CChannelSensor *pList)
{
	memcpy(pList, pAuxVolt, sizeof(CChannelSensor)*GetMaxTemperature());
}

void CCyclerModule::GetCanAuxList(CChannelSensor *pList)
{
	memcpy(pList, pAuxTempTh, sizeof(CChannelSensor)*GetMaxTemperatureTh());
}

//ksj 20200206
void CCyclerModule::GetHumiAuxList(CChannelSensor *pList)
{
	memcpy(pList, pAuxHumi, sizeof(CChannelSensor)*GetMaxTemperatureTh());
}

void CCyclerModule::SetAllParallelReset()
{
	int i = 0;
	BOOL bSkipReserveInit;
	for(int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
	{
		//yulee 20190315 동작 중인 채널은 reserved, reserved2를 초기화 하지 않는다. 
		bSkipReserveInit = FALSE;

		if( i < GetTotalChannel())
		{
			CCyclerChannel * pCh = GetChannelInfo(i);
			if((pCh->GetState() == PS_STATE_RUN) || (pCh->GetState() == PS_STATE_PAUSE))
			{
				bSkipReserveInit = TRUE;
				CString tmpStr;
				tmpStr.Format(_T("Detect that working ch while merge mode setting Ch: %d Working. Attr. Initialize Skip(1) :%d"), i+1, bSkipReserveInit); 
				pCh->WriteLog(tmpStr);
				tmpStr.Format(_T("Ch attr. reserved[0]: %d, reserved[1]: %d, reserved[2]: %d"), 
					m_parallelData[i].reserved[0], m_parallelData[i].reserved[1], m_parallelData[i].reserved[2]); 
				pCh->WriteLog(tmpStr);
				tmpStr.Format(_T("Ch attr. reserved2[0]: %d, reserved2[1]: %d, reserved2[2]: %d, reserved2[3]: %d"), 
					m_parallelData[i].reserved2[0], m_parallelData[i].reserved2[1], m_parallelData[i].reserved2[2], m_parallelData[i].reserved2[3]);  
				pCh->WriteLog(tmpStr);

				if(bSkipReserveInit == FALSE)
				{
					//m_parallelData[i].bParallel = 0;
					//m_parallelData[i].chNoMaster = i+1;
					//ZeroMemory(m_parallelData[i].chNoSlave, sizeof(char)*3);

					ZeroMemory(m_parallelData[i].reserved, sizeof(char)*3);		
					ZeroMemory(m_parallelData[i].reserved2, sizeof(char)*4);		//ljb 20150608 add 초기화 추가 20151112 mux 개발중
					if( i < GetTotalChannel())
					{
						CCyclerChannel * pCh = GetChannelInfo(i);
						CString tmpStr;
						tmpStr.Format(_T("Detect that working ch while merge mode setting Ch: %d Working. Attr. After initialize"), i+1); 
						pCh->WriteLog(tmpStr);
						tmpStr.Format(_T("Ch attr. reserved[0]: %d, reserved[1]: %d, reserved[2]: %d"), 
							m_parallelData[i].reserved[0], m_parallelData[i].reserved[1], m_parallelData[i].reserved[2]); 
						pCh->WriteLog(tmpStr);
						tmpStr.Format(_T("Ch attr. reserved2[0]: %d, reserved2[1]: %d, reserved2[2]: %d, reserved2[3]: %d"), 
							m_parallelData[i].reserved2[0], m_parallelData[i].reserved2[1], m_parallelData[i].reserved2[2], m_parallelData[i].reserved2[3]);  
						pCh->WriteLog(tmpStr);
					}
				}
			}

			m_parallelData[i].bParallel = 0;
			m_parallelData[i].chNoMaster = i+1;
			ZeroMemory(m_parallelData[i].chNoSlave, sizeof(char)*3);
		}
	}

	for(int i = 0 ; i < GetTotalChannel(); i++)
	{
		CCyclerChannel * pCh = GetChannelInfo(i);	
		if(!pCh->m_bMaster)
			pCh->SetMasterChannelNum(0);
		pCh->SetParallel(FALSE, FALSE);


	}
}
/*
void CCyclerModule::SetAllParallelReset()
{
	int i = 0;
	BOOL bSkipReserveInit;
	for(int i = 0 ; i < _SFT_MAX_PARALLEL_COUNT; i++)
	{
		//yulee 20190315 동작 중인 채널은 reserved, reserved2를 초기화 하지 않는다. 
		bSkipReserveInit = FALSE;
		
		if( i < GetTotalChannel())
		{
			CCyclerChannel * pCh = GetChannelInfo(i);
			if((pCh->GetState() == PS_STATE_RUN) || (pCh->GetState() == PS_STATE_PAUSE))
			{
				bSkipReserveInit = TRUE;
				CString tmpStr;
				tmpStr.Format(_T("Detect that working ch while merge mode setting Ch: %d Working. Attr. Initialize Skip(1) :%d"), i+1, bSkipReserveInit); 
				pCh->WriteLog(tmpStr);
				tmpStr.Format(_T("Ch attr. reserved[0]: %d, reserved[1]: %d, reserved[2]: %d"), 
					m_parallelData[i].reserved[0], m_parallelData[i].reserved[1], m_parallelData[i].reserved[2]); 
				pCh->WriteLog(tmpStr);
				tmpStr.Format(_T("Ch attr. reserved2[0]: %d, reserved2[1]: %d, reserved2[2]: %d, reserved2[3]: %d"), 
					m_parallelData[i].reserved2[0], m_parallelData[i].reserved2[1], m_parallelData[i].reserved2[2], m_parallelData[i].reserved2[3]);  
				pCh->WriteLog(tmpStr);
			}
		}

		
		m_parallelData[i].bParallel = 0;
		m_parallelData[i].chNoMaster = i+1;
		ZeroMemory(m_parallelData[i].chNoSlave, sizeof(char)*3);
		if(bSkipReserveInit == FALSE)
		{
			ZeroMemory(m_parallelData[i].reserved, sizeof(char)*3);		
			ZeroMemory(m_parallelData[i].reserved2, sizeof(char)*4);		//ljb 20150608 add 초기화 추가 20151112 mux 개발중
			if( i < GetTotalChannel())
			{
				CCyclerChannel * pCh = GetChannelInfo(i);
				CString tmpStr;
				tmpStr.Format(_T("Detect that working ch while merge mode setting Ch: %d Working. Attr. After initialize"), i+1); 
				pCh->WriteLog(tmpStr);
				tmpStr.Format(_T("Ch attr. reserved[0]: %d, reserved[1]: %d, reserved[2]: %d"), 
				m_parallelData[i].reserved[0], m_parallelData[i].reserved[1], m_parallelData[i].reserved[2]); 
				pCh->WriteLog(tmpStr);
				tmpStr.Format(_T("Ch attr. reserved2[0]: %d, reserved2[1]: %d, reserved2[2]: %d, reserved2[3]: %d"), 
				m_parallelData[i].reserved2[0], m_parallelData[i].reserved2[1], m_parallelData[i].reserved2[2], m_parallelData[i].reserved2[3]);  
				pCh->WriteLog(tmpStr);
			}
		}
		
	}

	for(int i = 0 ; i < GetTotalChannel(); i++)
	{
		CCyclerChannel * pCh = GetChannelInfo(i);	
		if(!pCh->m_bMaster)
			pCh->SetMasterChannelNum(0);
		pCh->SetParallel(FALSE, FALSE);
		
	
	}
}
*/
void CCyclerModule::ApplyAuxData()
{
	memcpy(pAuxTemp, pAuxTemp2, sizeof(CChannelSensor) * GetMaxTemperature());
	memcpy(pAuxVolt, pAuxVolt2, sizeof(CChannelSensor) * GetMaxVoltage());
	memcpy(pAuxTempTh, pAuxTempTh2, sizeof(CChannelSensor) * GetMaxTemperatureTh());			//ljb 20160621
	memcpy(pAuxHumi, pAuxHumi2, sizeof(CChannelSensor) * GetMaxHumidity());			//ksj 20200116 : v1016 온도 추가.

	int i = 0;
	for(int i = 0 ; i < GetTotalChannel(); i++)
	{
		CCyclerChannel * pChannel = GetChannelInfo(i);
		if(pChannel == NULL) return;
		pChannel->m_nTempAuxCount = 0;
		pChannel->m_nVoltAuxCount = 0;
		pChannel->m_nTempThAuxCount = 0;
		pChannel->m_nHumiAuxCount = 0; //ksj 20200116
	}
	for(int i = 0 ; i < GetMaxTemperature(); i++)
	{
		if(pAuxTemp[i].GetMasterChannel() > 0)
		{
			CCyclerChannel * pChannel = GetChannelInfo(pAuxTemp[i].GetMasterChannel()-1);
			//20081227 KHS
			if(pChannel == NULL) return;
			pChannel->m_nTempAuxCount++;
		}
	}

	for(int i = 0 ; i < GetMaxVoltage(); i++)
	{
		if(pAuxVolt[i].GetMasterChannel() > 0)
		{
			CCyclerChannel * pChannel = GetChannelInfo(pAuxVolt[i].GetMasterChannel()-1);
			//20081227 KHS
			if(pChannel == NULL) return;
			pChannel->m_nVoltAuxCount++;
		}
	}
	for(int i = 0 ; i < GetMaxTemperatureTh(); i++)
	{
		if(pAuxTempTh[i].GetMasterChannel() > 0)
		{
			CCyclerChannel * pChannel = GetChannelInfo(pAuxTempTh[i].GetMasterChannel()-1);
			//20081227 KHS
			if(pChannel == NULL) return;
			pChannel->m_nTempThAuxCount++;
		}
	}
	//ksj 20200116 : 습도 추가.
	for(int i = 0 ; i < GetMaxHumidity(); i++)
	{
		if(pAuxHumi[i].GetMasterChannel() > 0)
		{
			CCyclerChannel * pChannel = GetChannelInfo(pAuxHumi[i].GetMasterChannel()-1);
			if(pChannel == NULL) return;
			pChannel->m_nHumiAuxCount++;
		}
	}
	SaveAuxConfig();
}

void CCyclerModule::CencleAuxData()
{
	memcpy(pAuxTemp2, pAuxTemp, sizeof(CChannelSensor) * GetMaxTemperature());
	memcpy(pAuxVolt2, pAuxVolt, sizeof(CChannelSensor) * GetMaxVoltage());
	memcpy(pAuxTempTh2, pAuxTempTh, sizeof(CChannelSensor) * GetMaxTemperatureTh()); //ksj 20200206 : 추가. 써미스터 할당하고 캔슬하는 경우 사라지는 현상 수정.,//lyj 20200429
	memcpy(pAuxHumi2, pAuxHumi, sizeof(CChannelSensor) * GetMaxHumidity()); //ksj 20200206 //lyj 20200429
}

void CCyclerModule::RemoveAllAuxCh()
{
	if(pAuxTemp != NULL)
	{
		delete [] pAuxTemp;
		delete [] pAuxTemp2;
		pAuxTemp = pAuxTemp2 = NULL;
	}
	if(pAuxVolt != NULL)
	{
		delete [] pAuxVolt;
		delete [] pAuxVolt2;
		pAuxVolt = pAuxVolt2 = NULL;
	}
	if(pAuxTempTh != NULL)
	{
		delete [] pAuxTempTh;
		delete [] pAuxTempTh2;
		pAuxTempTh = pAuxTempTh2 = NULL;
		
	}

	//ksj 20200206
	if(pAuxHumi != NULL)
	{
		delete [] pAuxHumi;
		delete [] pAuxHumi2;
		pAuxHumi = pAuxHumi2 = NULL;

	}
}


void CCyclerModule::SaveAuxConfig()
{
	CString strBuff;
	char szBuff[900000];//lmh 20120321 5000->10000으로 변경		//ljb 20120712 10000 -> 900000
	CString strFileName;
	CFile file;
	CFileFind aFind;

	CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";

	CString strFileFind = strSaveDir + "\\Config\\*.aux";
	strFileFind.Format("%s\\Config\\M%02dCh*.aux", strSaveDir, GetModuleID());//lmh 다른 모듈 파일 삭제 금지
	BOOL bFind = aFind.FindFile(strFileFind);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}


	int i = 0;
	for(int nCh = 0 ; nCh < GetTotalChannel(); nCh++)
	{
		if(GetChannelInfo(nCh)->GetTotalAuxCount() <= 0) continue;

		for(int i = 0 ; i < GetMaxTemperature(); i++)
		{
			if(pAuxTemp[i].GetAuxName() != "") //lyj 20200714 tlt 파일 대응(이름이 없으면 통과한다)
			{
				if(pAuxTemp[i].GetMasterChannel() == nCh+1)
				{
					CString strTemp;
					//ljb 2010-06-22 Aux Name 추가 Save
					strTemp.Format("%d,%d,%s\r\n", pAuxTemp[i].GetAuxChannelNumber(), pAuxTemp[i].GetAuxType(), pAuxTemp[i].GetAuxName());
					strBuff += strTemp;				
				}
			}
		}

		for(int i = 0 ; i < GetMaxVoltage(); i++)
		{
			if(pAuxVolt[i].GetAuxName() != "") //lyj 20200714 tlt 파일 대응(이름이 없으면 통과한다)
			{
				if(pAuxVolt[i].GetMasterChannel() == nCh+1)
				{
					CString strTemp;
					//ljb 2010-06-22 Aux Name 추가 Save
					strTemp.Format("%d,%d,%s\r\n", pAuxVolt[i].GetAuxChannelNumber(), pAuxVolt[i].GetAuxType(), pAuxVolt[i].GetAuxName());
					strBuff += strTemp;
				}
			}
		}

		for(int i = 0 ; i < GetMaxTemperatureTh(); i++)
		{
			if(pAuxTempTh[i].GetAuxName() != "") //lyj 20200714 tlt 파일 대응(이름이 없으면 통과한다)
			{
				if(pAuxTempTh[i].GetMasterChannel() == nCh+1)
				{
					CString strTemp;
					//ljb 2010-06-22 Aux Name 추가 Save
					//strTemp.Format("%d,%d,%s\r\n", pAuxTempTh[i].GetAuxChannelNumber(), pAuxTempTh[i].GetAuxType(), pAuxTempTh[i].GetAuxName());
					//20180607 yulee 주석처리 1로 하드코딩된 것을 2로 설정(pAuxVolt[i].GetAuxType()이 1임) //strTemp.Format("%d,%d,%s\r\n", pAuxTempTh[i].GetAuxChannelNumber(), 1, pAuxTempTh[i].GetAuxName());	//20160710 edit Analyzer Excel 내보내기를 위해 Type을 1(voltage)로 고정
					strTemp.Format("%d,%d,%s\r\n", pAuxTempTh[i].GetAuxChannelNumber(), 2, pAuxTempTh[i].GetAuxName());	//20160710 edit Analyzer Excel 내보내기를 위해 Type을 1(voltage)로 고정

					strBuff += strTemp;
				}
			}
		}

		//ksj 20200116
		for(int i = 0 ; i < GetMaxHumidity(); i++)
		{
			if(pAuxHumi[i].GetAuxName() != "") //lyj 20200714 tlt 파일 대응(이름이 없으면 통과한다)
			{
				if(pAuxHumi[i].GetMasterChannel() == nCh+1)
				{
					CString strTemp;
					strTemp.Format("%d,%d,%s\r\n", pAuxHumi[i].GetAuxChannelNumber(), 3, pAuxHumi[i].GetAuxName());

					strBuff += strTemp;
				}
			}
		}
		//ksj end
	
		strBuff += "\0";

		TRY
		{
			ZeroMemory(&szBuff, 900000);
			
			strFileName.Format("%s\\Config\\M%02dCh%02d.aux", strSaveDir, GetModuleID(),nCh+1);
		

			if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite))
			{
				sprintf(szBuff, strBuff, strBuff.GetLength());
				int nLength = strBuff.GetLength();
				file.Write(szBuff, sizeof(char)*nLength);
				file.Close();
			}


			strBuff = "";
		}CATCH(CFileException, e)
		{
			e->ReportError();
			return;
		}
		END_CATCH

	}
}

SFT_CAN_COMMON_DATA CCyclerModule::GetCANCommonData(int nChIndex, int nType)
{
	return m_canData.canSetAllData.canCommonData[nChIndex][nType-1];
}

SFT_CAN_SET_DATA CCyclerModule::GetCANSetData(int nChIndex, int nType, int nCanIndex)
{
	if(nType == PS_CAN_TYPE_MASTER)
		return m_canData.canSetAllData.canSetData[nChIndex][nCanIndex];
	else if(nType == PS_CAN_TYPE_SLAVE)
		return m_canData.canSetAllData.canSetData[nChIndex][nInstalledMasterCANCount[nChIndex] + nCanIndex];
}

void CCyclerModule::MakeCAN()
{
	ZeroMemory(&m_canData, sizeof(SFT_MD_CAN_INFO_DATA));
	memcpy(&m_canData, ::SFTGetCANInfoData(m_nModuleID), sizeof(SFT_MD_CAN_INFO_DATA));
	ZeroMemory(&nInstalledMasterCANCount, _SFT_MAX_INSTALL_CH_COUNT); //_SFT_MAX_INSTALL_CH_COUNT 8개
	ZeroMemory(&nInstalledSlaveCANCount, _SFT_MAX_INSTALL_CH_COUNT);

	for(int i = 0 ; i < m_nTotalChannel; i++)
	{
		GetChannelInfo(i)->SetCanCount(m_canData.installedCanCount[i]);
		for(int j = 0 ; j < m_canData.installedCanCount[i]; j++)
		{
			if(m_canData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_MASTER)
				nInstalledMasterCANCount[i]++;
			else if(m_canData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_SLAVE)
				nInstalledSlaveCANCount[i]++;
			else
			{
				TRACE("정의되지 않은 CAN 데이터에 접근합니다.");
				break;
			}
		}

		TRACE("ch %d 마스터캔 카운트 : %d \n",i, nInstalledMasterCANCount[i]);
		TRACE("ch %d 슬래이브캔 카운트 : %d \n",i, nInstalledSlaveCANCount[i]);
	}
	SaveCanConfig();
}

int CCyclerModule::GetInstalledCanMaster(int nChIndex)
{
	return nInstalledMasterCANCount[nChIndex];
}

int CCyclerModule::GetInstalledCanSlave(int nChIndex)
{
	return nInstalledSlaveCANCount[nChIndex];
}

void CCyclerModule::SetCANSetData(int nChIndex, SFT_CAN_SET_DATA *pCanSetData, int nCount)
{
	int nMaster, nSlave;
	nMaster = nSlave = 0;
	for(int i = 0; i < nCount; i++)
	{
		if(pCanSetData[i].canType == PS_CAN_TYPE_MASTER)
			nMaster++;
		else if(pCanSetData[i].canType == PS_CAN_TYPE_SLAVE)
			nSlave++;
	}
	nInstalledMasterCANCount[nChIndex] = nMaster;
	nInstalledSlaveCANCount[nChIndex] = nSlave;
	m_canData.installedCanCount[nChIndex] = nCount;
	
	GetChannelInfo(nChIndex)->SetCanCount(m_canData.installedCanCount[nChIndex]);
	
	memcpy(&m_canData.canSetAllData.canSetData[nChIndex], pCanSetData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);
}

void CCyclerModule::SetCANCommonData(int nChIndex, SFT_CAN_COMMON_DATA commonData, int nType)
{
	memcpy(&m_canData.canSetAllData.canCommonData[nChIndex][nType-1], &commonData, sizeof(SFT_CAN_COMMON_DATA));
}

SFT_MD_CAN_INFO_DATA * CCyclerModule::GetCanAllData()
{
	return &m_canData;
}

void CCyclerModule::SaveCanConfig()
{
	CString strBuff;
	char szBuff[90000];		//ljb 20120712 5000 -> 900000
	CString strFileName;
	CFile file;
	CFileFind aFind;

	CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";

	CString strFileFind = strSaveDir + "\\Config\\*.can";
	strFileFind.Format("%s\\Config\\M%02dCh*.can", strSaveDir, GetModuleID());//lmh 다른 모듈 파일 삭제 금지
	BOOL bFind = aFind.FindFile(strFileFind);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}

	for(int nCh = 0 ; nCh < GetTotalChannel(); nCh++)
	{
		int i = 0;
		for(int i = 0 ; i < nInstalledMasterCANCount[nCh]; i++)
		{
			CString strTemp;
/*			strTemp.Format("%d, %d, %d\r\n",
				i, m_canData.canSetAllData.canSetData[nCh][i].canType,
				m_canData.canSetAllData.canSetData[nCh][i].data_type);*/
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			//ljb 20180419 add
			strTemp.Format(_T("%s"),m_canData.canSetAllData.canSetData[nCh][i].name);
			if (strTemp != "$")
			{

			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canData.canSetAllData.canSetData[nCh][i].canType,
				m_canData.canSetAllData.canSetData[nCh][i].data_type,
				m_canData.canSetAllData.canSetData[nCh][i].name);
			strBuff+=strTemp;
			}
		}
	
		int nIndex = nInstalledMasterCANCount[nCh];
		for(int i = 0; i < nInstalledSlaveCANCount[nCh]; i++)
		{
			CString strTemp;
//			strTemp.Format("%d, %d, %d\r\n",
//				i, m_canData.canSetAllData.canSetData[nCh][i+nIndex].canType,
//				m_canData.canSetAllData.canSetData[nCh][i+nIndex].data_type);
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			//ljb 20180419 add
			strTemp.Format(_T("%s"),m_canData.canSetAllData.canSetData[nCh][i+nIndex].name);
			//if (strTemp != "$")                    
			if (strTemp != "$" || strTemp != "") //lyj 20200714 tlt 파일 대응(이름이 없으면 통과한다)
			{

			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canData.canSetAllData.canSetData[nCh][i+nIndex].canType,
				m_canData.canSetAllData.canSetData[nCh][i+nIndex].data_type,
				m_canData.canSetAllData.canSetData[nCh][i+nIndex].name);
			strBuff+=strTemp;
			}
		}
		strBuff += "\0";

		TRY
		{
			ZeroMemory(&szBuff, 90000);
			
			strFileName.Format("%s\\Config\\M%02dCh%02d.can", strSaveDir, GetModuleID(),nCh+1);
		

			if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite))
			{
				sprintf(szBuff, strBuff, strBuff.GetLength());
				int nLength = strBuff.GetLength();
				file.Write(szBuff, sizeof(char)*nLength);
				file.Close();
			}


			strBuff = "";
		}CATCH(CFileException, e)
		{
			e->ReportError();
			return;
		}
		END_CATCH

	}
}

int CCyclerModule::GetInstalledCanTransMaster(int nChIndex)
{
	return nInstalledMasterCANTransCount[nChIndex];
}

int CCyclerModule::GetInstalledCanTransSlave(int nChIndex)
{
	return nInstalledSlaveCANTransCount[nChIndex];
}

void CCyclerModule::MakeCANTrans()
{
	ZeroMemory(&m_canTransData, sizeof(SFT_MD_CAN_TRANS_INFO_DATA));
	memcpy(&m_canTransData, ::SFTGetCANTransInfoData(m_nModuleID), sizeof(SFT_MD_CAN_TRANS_INFO_DATA));
	ZeroMemory(&nInstalledMasterCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	ZeroMemory(&nInstalledSlaveCANTransCount, _SFT_MAX_INSTALL_CH_COUNT);
	
	for(int i = 0 ; i < m_nTotalChannel; i++)
	{
		GetChannelInfo(i)->SetCanTransCount(m_canTransData.installedCanCount[i]);
		for(int j = 0 ; j < m_canTransData.installedCanCount[i]; j++)
		{
			TRACE("m_canTransData = %d\r\n",m_canTransData.installedCanCount[i]);
			if(m_canTransData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_MASTER)
				nInstalledMasterCANTransCount[i]++;
			else if(m_canTransData.canSetAllData.canSetData[i][j].canType == PS_CAN_TYPE_SLAVE)
				nInstalledSlaveCANTransCount[i]++;
			else
			{
				TRACE("정의되지 않은 CAN 데이터에 접근합니다.\r\n");
				break;
			}
		}
	}
	SaveCanTransConfig();
}

void CCyclerModule::SetCANSetTransData(int nChIndex, SFT_CAN_SET_TRANS_DATA *pCanSetData, int nCount)
{
	int nMaster, nSlave;
	nMaster = nSlave = 0;
	for(int i = 0; i < nCount; i++)
	{
		if(pCanSetData[i].canType == PS_CAN_TYPE_MASTER)
			nMaster++;
		else if(pCanSetData[i].canType == PS_CAN_TYPE_SLAVE)
			nSlave++;
	}
	nInstalledMasterCANTransCount[nChIndex] = nMaster;
	nInstalledSlaveCANTransCount[nChIndex] = nSlave;
	m_canTransData.installedCanCount[nChIndex] = nCount;
	
	GetChannelInfo(nChIndex)->SetCanTransCount(m_canTransData.installedCanCount[nChIndex]);
	
	memcpy(&m_canTransData.canSetAllData.canSetData[nChIndex], pCanSetData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);
}

void CCyclerModule::SaveCanTransConfig()
{
	CString strBuff;
	char szBuff[9000];			//ljb 20120712 500 -> 900000
	CString strFileName;
	CFile file;
	CFileFind aFind;

	CString strSaveDir = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path") + "\\" + "DataBase";

	CString strFileFind = strSaveDir + "\\Config\\*.trs";
	strFileFind.Format("%s\\Config\\M%02dCh*.trs", strSaveDir, GetModuleID());//lmh 다른 모듈 파일 삭제 금지
	BOOL bFind = aFind.FindFile(strFileFind);
	while(bFind)
	{
		bFind = aFind.FindNextFile();
		DeleteFile(aFind.GetFilePath());
	}

	for(int nCh = 0 ; nCh < GetTotalChannel(); nCh++)
	{
		int i = 0;
		for(int i = 0 ; i < nInstalledMasterCANTransCount[nCh]; i++)
		{
			CString strTemp;
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canTransData.canSetAllData.canSetData[nCh][i].canType,
				m_canTransData.canSetAllData.canSetData[nCh][i].data_type,
				m_canTransData.canSetAllData.canSetData[nCh][i].name);
			strBuff+=strTemp;
		}
	
		int nIndex = nInstalledMasterCANTransCount[nCh];
		for(int i = 0; i < nInstalledSlaveCANTransCount[nCh]; i++)
		{
			CString strTemp;
//			strTemp.Format("%d, %d, %d\r\n",
//				i, m_canData.canSetAllData.canSetData[nCh][i+nIndex].canType,
//				m_canData.canSetAllData.canSetData[nCh][i+nIndex].data_type);
			//can no, canType, can data type, can name 형식으로 변경 20080409 kjh
			strTemp.Format("%d, %d, %d, %s\r\n",
				i, m_canTransData.canSetAllData.canSetData[nCh][i+nIndex].canType,
				m_canTransData.canSetAllData.canSetData[nCh][i+nIndex].data_type,
				m_canTransData.canSetAllData.canSetData[nCh][i+nIndex].name);
			strBuff+=strTemp;
		}
		strBuff += "\0";

		TRY
		{
			ZeroMemory(&szBuff, 9000);
			
			strFileName.Format("%s\\Config\\M%02dCh%02d.trs", strSaveDir, GetModuleID(),nCh+1);
		

			if(file.Open(strFileName, CFile::modeCreate|CFile::modeWrite))
			{
				sprintf(szBuff, strBuff, strBuff.GetLength());
				int nLength = strBuff.GetLength();
				file.Write(szBuff, sizeof(char)*nLength);
				file.Close();
			}


			strBuff = "";
		}CATCH(CFileException, e)
		{
			e->ReportError();
			return;
		}
		END_CATCH

	}
}

void CCyclerModule::SetCANCommonTransData(int nChIndex, SFT_CAN_COMMON_TRANS_DATA commonData, int nType)
{
	memcpy(&m_canTransData.canSetAllData.canCommonData[nChIndex][nType-1], &commonData, sizeof(SFT_CAN_COMMON_TRANS_DATA));
}

SFT_CAN_SET_TRANS_DATA CCyclerModule::GetCANSetTransData(int nChIndex, int nType, int nCanIndex)
{
	if(nType == PS_CAN_TYPE_MASTER)
		return m_canTransData.canSetAllData.canSetData[nChIndex][nCanIndex];
	else if(nType == PS_CAN_TYPE_SLAVE)
		return m_canTransData.canSetAllData.canSetData[nChIndex][nInstalledMasterCANTransCount[nChIndex] + nCanIndex];
}

SFT_MD_CAN_TRANS_INFO_DATA * CCyclerModule::GetCanTransAllData()
{
	return &m_canTransData;
}

SFT_CAN_COMMON_TRANS_DATA CCyclerModule::GetCANCommonTransData(int nChIndex, int nType)
{
	return m_canTransData.canSetAllData.canCommonData[nChIndex][nType-1];
}