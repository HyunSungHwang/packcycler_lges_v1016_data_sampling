// TrayInputDlg.cpp : implementation file
//
//IDC_TITLE_STATIC
#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "TrayInputDlg.h"
#include "JigIDRegDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTrayInputDlg dialog


CTrayInputDlg::CTrayInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CTrayInputDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CTrayInputDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CTrayInputDlg::IDD3):
	(CTrayInputDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CTrayInputDlg)
	m_strTrayNo = _T("");
	m_strUnitNo = _T("");
	//}}AFX_DATA_INIT
	m_nTimer = 0;
	m_ParentWnd = pParent;

	m_bModeLocation = FALSE;

	m_nSelJigNo = 1;
	m_nSelModuleID = 0;
}


void CTrayInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrayInputDlg)
	DDX_Control(pDX, IDC_JIG_LIST_COMBO, m_ctrlJigListCombo);
	DDX_Control(pDX, IDC_TITLE_STATIC, m_ctrlTitle);
	DDX_Text(pDX, IDC_EDIT1, m_strTrayNo);
	DDX_Text(pDX, IDC_EDIT2, m_strUnitNo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTrayInputDlg, CDialog)
	//{{AFX_MSG_MAP(CTrayInputDlg)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_CBN_SELCHANGE(IDC_JIG_LIST_COMBO, OnSelchangeJigListCombo)
	ON_EN_CHANGE(IDC_EDIT1, OnChangeEdit1)
	ON_MESSAGE(SFTWM_BCR_SCANED, OnBcrscaned)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTrayInputDlg message handlers

//읽는 순서 Tray BCR => Jig BCR 
/*
BOOL CTrayInputDlg::ReadTrayNo(CString strData)
{
	ShowWindow(SW_SHOW);	

	if(m_nTimer > 0)
	{
		KillTimer(100);
	}


	CString strMsg;
	if(SearchTrayID(strData) == FALSE)
	{
		strMsg.Format("[%s]는 등록되어있지 않은 Tray입니다.\n먼저 등록하십시요." , strData);
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}

	m_strTrayNo = strData;
	m_strUnitNo.Empty();
	m_ctrlJigListCombo.SetCurSel(-1);

	m_ctrlTitle.FlashText(FALSE);
	m_ctrlTitle.SetTextColor(RGB(0, 0, 0));

	if(m_bModeLocation)
	{
		m_ctrlTitle.SetText("작업 위치를 선택하십시요.");
	}

	UpdateData(FALSE);
	return TRUE;
}

//BCR에 의해 지그ID가 읽히 경우 
BOOL CTrayInputDlg::ReadUnitNo(CString strData)
{
	if(m_bModeLocation == FALSE)	return FALSE;

	ShowWindow(SW_SHOW);	

	CString strMsg;
	int nModuleID = 0, nJigID = 0;
	if(SearchLocation(strData, nModuleID, nJigID) == FALSE)
	{
		strMsg.Format("[%s]는 등록되어있지 위치 정보입니다.\n위치정보를 먼저 등록하십시요." , strData);
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}
		
	//Set Combo Ctrl
	//
	int bFind = FALSE;
	DWORD nID = MAKELONG(nJigID, nModuleID) ;
	for(int i=0; i<m_ctrlJigListCombo.GetCount(); i++)
	{
		if(m_ctrlJigListCombo.GetItemData(i) == nID)
		{
			m_ctrlJigListCombo.SetCurSel(i);
			bFind = TRUE;
			break;
		}
	}
	if(bFind == FALSE)
	{
		m_ctrlJigListCombo.SetCurSel(-1);
		GetDlgItem(IDC_EDIT2)->SetWindowText("????????");
		
		AfxMessageBox("모듈에 존재하지 않는 위치 정보 입니다.");
		return FALSE;
	}
	//

	m_strUnitNo = strData;
	UpdateData(FALSE);

	if(m_strTrayNo.IsEmpty())
	{
		//Error
		m_ctrlTitle.SetText("Tray 번호를 먼저 입력하십시요.");
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);

		SetHideDelayTime(5000);

		m_strTrayNo.Empty();
		m_strUnitNo.Empty();
		return FALSE;
	}
	else
	{
		//DataBase에서 지정 위치 검색
		m_ctrlTitle.FlashText(FALSE);
		m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
		m_ctrlTitle.SetText("입력이 완료 되었습니다.");
	
		SendTrayNoReadMsg(m_strTrayNo, nModuleID, nJigID);
		
		//reset data
		SetHideDelayTime(2000);
	}

	m_strTrayNo.Empty();
	m_strUnitNo.Empty();
	return TRUE;
}
*/

//읽는 순서 Jig BCR => Tray BCR
BOOL CTrayInputDlg::ReadTrayNo(CString strData)
{
	ShowWindow(SW_SHOW);	

	CString strMsg;
/*	if(SearchTrayID(strData) == FALSE)
	{
		strMsg.Format("[%s]는 등록되어있지 않은 Tray입니다.\n먼저 등록하십시요." , strData);
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}
*/	m_strTrayNo = strData;

	int nIndex = m_ctrlJigListCombo.GetCurSel();
	DWORD dwID = 0;
	int nModuleID = 0, nJigID = 0;
	if(nIndex < 0)
	{
		m_strUnitNo.Empty();
	}
	else
	{
		dwID = m_ctrlJigListCombo.GetItemData(nIndex);
		nModuleID = HIWORD(dwID);
		nJigID = LOWORD(dwID);
	}

	UpdateData(FALSE);

	if(m_bModeLocation == TRUE)
	{
		if(m_strUnitNo.IsEmpty())
		{
			//Error
			m_ctrlTitle.SetText(Fun_FindMsg("ReadTrayNo_msg1","IDD_TRAYNO_DLG"));
			//@ m_ctrlTitle.SetText("작업 위치(Jig Bar Code)를 먼저 지정하십시요.");
			m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
			m_ctrlTitle.FlashText(TRUE);

			SetHideDelayTime(5000);

			m_strTrayNo.Empty();
			m_strUnitNo.Empty();
			return FALSE;
		}
		else
		{
			//DataBase에서 지정 위치 검색
			m_ctrlTitle.FlashText(FALSE);
			m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
			m_ctrlTitle.SetText(Fun_FindMsg("ReadTrayNo_msg2","IDD_TRAYNO_DLG"));
			//@ m_ctrlTitle.SetText("입력이 완료 되었습니다.");
		
			SendTrayNoReadMsg(m_strTrayNo, nModuleID, nJigID);
			
			//reset data
			SetHideDelayTime(2000);
		}

		m_strTrayNo.Empty();
		m_strUnitNo.Empty();
	}

	return TRUE;
}

//BCR에 의해 지그ID가 읽히 경우 
BOOL CTrayInputDlg::ReadUnitNo(CString strData)
{
	if(m_bModeLocation == FALSE)	return FALSE;

	ShowWindow(SW_SHOW);	

	if(m_nTimer > 0)
	{
		KillTimer(100);
	}

	CString strMsg;
	int nModuleID = 0, nJigID = 0;
	if(SearchLocation(strData, nModuleID, nJigID) == FALSE)
	{
		strMsg.Format(Fun_FindMsg("ReadUnitNo_msg1","IDD_TRAYNO_DLG") , strData);
		//@ strMsg.Format("[%s]는 등록되어있지 위치 정보입니다.\n위치정보를 먼저 등록하십시요." , strData);
		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return FALSE;
	}

	//Set Combo Ctrl
	int bFind = FALSE;
	DWORD nID = MAKELONG(nJigID, nModuleID) ;
	for(int i=0; i<m_ctrlJigListCombo.GetCount(); i++)
	{
		if(m_ctrlJigListCombo.GetItemData(i) == nID)
		{
			m_ctrlJigListCombo.SetCurSel(i);
			bFind = TRUE;
			break;
		}
	}
	if(bFind == FALSE)
	{
		m_ctrlJigListCombo.SetCurSel(-1);
		GetDlgItem(IDC_EDIT2)->SetWindowText("????????");
		
		AfxMessageBox(Fun_FindMsg("ReadUnitNo_msg2","IDD_TRAYNO_DLG"));
		//@ AfxMessageBox("존재하지 않는 위치 정보 입니다.");
		return FALSE;
	}
	//

	m_strUnitNo = strData;

	m_strTrayNo.Empty();
	m_ctrlTitle.FlashText(FALSE);
	m_ctrlTitle.SetTextColor(RGB(0, 0, 0));

	m_ctrlTitle.SetText(Fun_FindMsg("ReadUnitNo_msg3","IDD_TRAYNO_DLG"));
	//@ m_ctrlTitle.SetText("Cell 번호를 입력 하십시요.");

	UpdateData(FALSE);
	return TRUE;
	
}
//////////////////////////////////////////////////////////////////////////

void CTrayInputDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

void CTrayInputDlg::SetHideDelayTime(int nMiliSec)
{
	if(m_nTimer > 0)
	{
		KillTimer(100);
	}
	m_nTimer = SetTimer(100, nMiliSec, NULL);
}

void CTrayInputDlg::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	ShowWindow(SW_HIDE);

	//Reset Timer
	KillTimer(100);
	m_nTimer = 0;

	CDialog::OnTimer(nIDEvent);
}

LRESULT CTrayInputDlg::OnBcrscaned(WPARAM wParam,LPARAM lParam)
{
	char *data = (char *)lParam;
	CString str(data);
	
	if(str.IsEmpty())	return 0;

	if(wParam == TRUE)
	{
		ReadUnitNo(str);	
	}
	else
	{
		ReadTrayNo(str);	
	}

	return 1;
}

void CTrayInputDlg::SendTrayNoReadMsg(CString strTray, int nModuleID, int nJigNo)
{
	if(m_ParentWnd != NULL)
	{
		static BAR_TAG_INFOMATION tagData;
		memset(&tagData, 0, sizeof(tagData));
		sprintf(tagData.szTagID, m_strTrayNo);

		::PostMessage(m_ParentWnd->m_hWnd, SFTWM_TAGID_READED, 
			MAKELONG(nJigNo, nModuleID), (LPARAM)&tagData);			//Send to Parent Wnd to Module State change
	}
}

void CTrayInputDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	CString strMsg;
	if(m_strTrayNo.IsEmpty())
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_TRAYNO_DLG"));
		//@ AfxMessageBox("Cell 번호가 입력되지 않았습니다.");
		GetDlgItem(IDC_EDIT1)->SetFocus();
		return;
	}

/*	if(SearchTrayID(m_strTrayNo) == FALSE)
	{
		strMsg.Format("[%s]는 등록되어있지 않은 Tray입니다.\n먼저 등록하십시요." , m_strTrayNo);
		//GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(strMsg);

		m_ctrlTitle.SetText(strMsg);
		m_ctrlTitle.SetTextColor(RGB(255, 0, 0));
		m_ctrlTitle.FlashText(TRUE);
		return;
	}
*/
	if(m_bModeLocation)
	{
		int nSel = m_ctrlJigListCombo.GetCurSel();
		if(nSel != CB_ERR)
		{
			int nID = m_ctrlJigListCombo.GetItemData(nSel);
			m_nSelModuleID = HIWORD(nID);
			m_nSelJigNo = LOWORD(nID);
		}
		else
		{
			strMsg.Format(Fun_FindMsg("OnOK_msg2","IDD_TRAYNO_DLG"));
			//@ strMsg.Format("작업 위치가 선택되지 않았습니다.");
			AfxMessageBox(strMsg);
			return;
		}

		SendTrayNoReadMsg(m_strTrayNo, m_nSelModuleID, m_nSelJigNo);
	}

	CDialog::OnOK();
}

BOOL CTrayInputDlg::SearchLocation(CString strID, int &nModuleID, int &nJigID)
{
	CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
	pDlg->m_pDoc = m_pDoc;
	
	int nMD, nJig;
	if(pDlg->SearchLocation(strID, nMD, nJig))
	{
		delete pDlg;
		nModuleID = nMD;
		nJigID = nJig;
		return TRUE;
	}

	delete pDlg;
	nModuleID = 0;
	nJig = 0;
	return FALSE;
}

BOOL CTrayInputDlg::SearchTrayID(CString strID)
{
	CDaoDatabase  db;
	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	CString strSQL, strTemp;
	strSQL.Format("SELECT TraySerial FROM Tray WHERE TrayNo = '%s'", strID);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	if(rs.IsBOF() || rs.IsEOF())
	{
		rs.Close();
		db.Close();
	
		return FALSE;
	}
	rs.Close();
	db.Close();
	return TRUE;
}

void CTrayInputDlg::OnSelchangeJigListCombo() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_nTimer > 0)
	{
		KillTimer(100);
	}
	
	int nSel = m_ctrlJigListCombo.GetCurSel();
	if(nSel != CB_ERR)
	{
		int nID = m_ctrlJigListCombo.GetItemData(nSel);
		int nMD = HIWORD(nID);
		int nJig = LOWORD(nID);

		CJigIDRegDlg *pDlg = new CJigIDRegDlg(this);
		pDlg->m_pDoc = m_pDoc;
		CString strID = pDlg->SearchLocation(nMD, nJig);

		if(strID.IsEmpty())
		{
			//수동 입력된 상태(DB에 위치정보가 존재하지 않음)
			strID.Format(Fun_FindMsg("OnSelchangeJigListCombo_msg","IDD_TRAYNO_DLG"), m_pDoc->GetModuleName(nMD), nJig);
			//@ strID.Format("%s의 지그 %d에 위치 정보 ID가 존재하지 않습니다. 등록하십시요.", m_pDoc->GetModuleName(nMD), nJig);
			//AfxMessageBox(strID);
			strID = "????????";
		}
		else
		{

		}

		m_strUnitNo = strID;
		UpdateData(FALSE);
		delete pDlg;
	}
}

BOOL CTrayInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString str;
	ASSERT(m_pDoc);

	int nInstallMD = m_pDoc->GetInstallModuleCount();

	int nCnt = 0;
	int nDefaultIndex = -1;

	for(int i=0; i<nInstallMD; i++)
	{

		int nModuleID = m_pDoc->GetModuleID(i);
		CCyclerModule *pModule = m_pDoc->GetCyclerMD(nModuleID);

		for(int j=0; j<pModule->GetTotalChannel(); j++)
		{
			int nJigID = j+1;
			str.Format("%s - CH %d", m_pDoc->GetModuleName(nModuleID), j+1);
			m_ctrlJigListCombo.AddString(str);
			
			//default selection
			if(nModuleID == m_nSelModuleID && nJigID == m_nSelJigNo)
			{
				nDefaultIndex = nCnt;
			}
			long lID = MAKELONG(nJigID, nModuleID);
			m_ctrlJigListCombo.SetItemData(nCnt++, lID);
		}
	}
	if(m_ctrlJigListCombo.GetCount() == 1)
	{
		nDefaultIndex = 0;
	}
	m_ctrlJigListCombo.SetCurSel(nDefaultIndex);
	OnSelchangeJigListCombo();

	GetDlgItem(IDC_JIG_LIST_COMBO)->EnableWindow(m_bModeLocation);
	
	if(m_strTitleText.IsEmpty() == FALSE)
	{
		GetDlgItem(IDC_TITLE_STATIC)->SetWindowText(m_strTitleText);
	}

	GetDlgItem(IDC_EDIT1)->SetFocus();
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTrayInputDlg::SetTitle(CString str)
{
	m_strTitleText = str;
}

void CTrayInputDlg::SetUnitInput(BOOL bUnit)
{
	m_bModeLocation = bUnit;
}


void CTrayInputDlg::OnChangeEdit1() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_nTimer > 0)
	{
		KillTimer(100);
	}
}

void CTrayInputDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	m_strTrayNo.Empty();
	m_strUnitNo.Empty();
	m_ctrlJigListCombo.SetCurSel(-1);

	m_ctrlTitle.FlashText(FALSE);
	m_ctrlTitle.SetTextColor(RGB(0, 0, 0));
	m_ctrlTitle.SetText(Fun_FindMsg("OnCancel_msg","IDD_TRAYNO_DLG"));
	//@ m_ctrlTitle.SetText("작업할 번호를 입력하십시요.");

	UpdateData(FALSE);
	CDialog::OnCancel();
}