// FtpDownLoad.h: interface for the CFtpDownLoad class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FTPDOWNLOAD_H__E9C1FD96_7D5F_48C4_AFF6_9A2611105C7C__INCLUDED_)
#define AFX_FTPDOWNLOAD_H__E9C1FD96_7D5F_48C4_AFF6_9A2611105C7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxinet.h>

#define FTP_REG_CONFIG	"Ftp Set"

class CFtpDownLoad  
{
public:

	BOOL SetCurrentDirecotry(CString strCurDir);
	CString GetCurrentDirecotry();
	CFtpConnection   *m_pFtpConnection;         //Ftp 명령 연결 컨트롤 클래스
	BOOL ConnectFtpSession(CString strIPAddress, CString strID = "root", CString strPWD = "dusrnth");
	int DownLoadFile(CString strLoacalFile, CString strRemoteFile, BOOL bOverWrite = TRUE);
	int UploadFile(CString strLoacalFile, CString strRemoteFile, BOOL bOverWrite = TRUE);
	int DownLoadFile2(CString strLocalFile, CString strRemoteFile, BOOL bOverWrite = TRUE);
	int DownLoadAllFile(CString strLoacalFile, CString strRemoteFile,UINT nModuleID, UINT nChIndex = -1, BOOL bOverWrite = TRUE);
	int GetFileTotNum(CString strLocalFile, CString strRemoteFile ,UINT nModuleID, UINT nChIndex, BOOL bOverWrite);	//yulee 20190424
	int DownLoadAllFile2(CString strLocalFile, CString strRemoteFile,UINT nModuleID, UINT nChIndex, int nSBCFileNoOrg, int nSBCFileNoNew, BOOL bOverWrite = TRUE);	//ljb 20150315 add
	int DownLoadAllFile3_ProgressBar(CString strLocalFile, CString strRemoteFile,UINT nModuleID, UINT nChIndex, int nSBCFileNoOrg, int nSBCFileNoNew, int nTotNum, int nDwnCnt, BOOL bOverWrite = TRUE);//yulee 20190427
	BOOL GetLastWriteTimeByName(CString strRemoteFile, CTime &TimeLastWrite);
	CFtpDownLoad();
	virtual ~CFtpDownLoad();

protected:
	CInternetSession *m_pInternetSession;          //인터넷 세션 클래스
};

#endif // !defined(AFX_FTPDOWNLOAD_H__E9C1FD96_7D5F_48C4_AFF6_9A2611105C7C__INCLUDED_)
