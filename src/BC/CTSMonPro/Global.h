#if !defined(AFX_GLOBAL_H__5A153FB7_B378_4A49_80E3_AA8E4371868F__INCLUDED_)
#define AFX_GLOBAL_H__5A153FB7_B378_4A49_80E3_AA8E4371868F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Global.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Global window

class Global : public CWnd
{
// Construction
public:
	Global();

	void app_GetConfig(CString AppPath);

	CHINFO* ch_GetChList(int nModuleID,int  nChannelIndex);
	CHINFO* ch_GetChList(int nChannelNum);
	void    ch_SetChReset();

	CString GetChInfo(int iCHNO,CString strKey);
	void    SetChInfo(int iCHNO,CString strKey,CString strValue);

	CString onGetCHAR(char *buf,int nLen,int nStart=0,CString strSplit=_T(" "),int iTpChar=0);
	CString onGetBYTE(BYTE *buf,int nLen,int nStart=0,CString strSplit=_T(" "),int iTpChar=0);

	void            onSbcCh_GetList(CComboBox *cbo);
	CCyclerChannel* onSbcCh_GetCHInfo(int iType,int nCHNO);

    void    onCopyChar(char *buf,CString strValue,int nSize);
	void    onCopyCharArray(char *srcbuf,char *tgrbuf,int nSize);
	
	void    onLoadModeCopy( S_P1_CMD_BODY_LOAD_MODE_SET &cmdBodyLoadModeSet,
		                    S_P1_CMD_BODY_MODULE_INFO_REPLY &cmdBodyModuleInfoReply);
	void         Fun_ChBackTimeClear(CTypedPtrList<CPtrList, CHBACKTIME*> &ListData);
	CHBACKTIME*  Fun_ChBackTimeFind(CTypedPtrList<CPtrList, CHBACKTIME*> &ListData,int iBackCH);
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Global)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~Global();

	// Generated message map functions
protected:
	//{{AFX_MSG(Global)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern Global mainGlobal;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLOBAL_H__5A153FB7_B378_4A49_80E3_AA8E4371868F__INCLUDED_)
