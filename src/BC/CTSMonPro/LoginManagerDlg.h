#if !defined(AFX_LOGINMANAGERDLG_H__F0366A27_0B0E_43C6_975A_F9622F57AE2A__INCLUDED_)
#define AFX_LOGINMANAGERDLG_H__F0366A27_0B0E_43C6_975A_F9622F57AE2A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LoginManagerDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLoginManagerDlg dialog
#include "../../../BCLib/Include/DataEditor.h" //ksj 20200203
#include "../../../../DataBase/DataBaseClass/Dao/UserRecordSet.h" //ksj 20200203

class CLoginManagerDlg : public CDialog
{
// Construction
public:
	//CLoginManagerDlg(CWnd* pParent = NULL);   // standard constructor
	CLoginManagerDlg(long lAuth = PS_USER_OPERATOR/*작업자 이상*/, BOOL bPneOnly = FALSE/*no pne*/, CWnd* pParent = NULL);   // standard constructor //ksj 20200203	

// Dialog Data
	//{{AFX_DATA(CLoginManagerDlg)
	enum { IDD = IDD_LOGON_DLG , IDD2 = IDD_LOGON_DLG_ENG };
	CString	m_strUserID;
	CString	m_strPassword;
	SCH_LOGIN_INFO	m_LoginInfo; //ksj 20200203
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoginManagerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLoginManagerDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	BOOL SearchUser(CString strUserID, BOOL bPassWordCheck, CString strPassword, SCH_LOGIN_INFO *pUserData); //ksj 20200203	

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	// ksj 20200203 : 권한 설정
	long m_lAuth;
	// ksj 20200203 : PNE만 로그인 가능 여부
	BOOL m_bPneOnly;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGINMANAGERDLG_H__F0366A27_0B0E_43C6_975A_F9622F57AE2A__INCLUDED_)
