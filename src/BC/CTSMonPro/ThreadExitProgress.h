#pragma once

#include "ExitProgressWnd.h"

// CThreadExitProgress

class CThreadExitProgress : public CWinThread
{
	DECLARE_DYNCREATE(CThreadExitProgress)

public:
	CThreadExitProgress();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CThreadExitProgress();

	void ShowProgressWnd(CWnd* pParent);
	void SetProgress(int nPos);
	CExitProgressWnd* m_pExitProgressWnd;	
	virtual BOOL InitInstance();
	virtual int ExitInstance();

protected:
	DECLARE_MESSAGE_MAP()
};


