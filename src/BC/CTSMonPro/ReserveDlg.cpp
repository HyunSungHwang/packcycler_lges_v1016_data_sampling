/////////////////////////////////////////////////////////
// ReserveDlg.cpp
// ksj 20160426 작업 예약 및 실행 기능
//
/////////////////////////////////////////////////////////


#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "ReserveDlg.h"

#include "CTSMonProDoc.h"
#include "CTSMonProView.h"
#include "SelectScheduleDlg.h"
#include "StartDlgEx.h"
#include "WorkWarnin.h"

#include "Global.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReserveDlg dialog


CReserveDlg::CReserveDlg(CCTSMonProDoc* pDoc, CCTSMonProView* pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CReserveDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CReserveDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CReserveDlg::IDD3):
	(CReserveDlg::IDD), pParent)
{

	//{{AFX_DATA_INIT(CReserveDlg)
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	m_pParent = pParent;
	m_bWorking = FALSE;
	m_bDraging = FALSE;
	m_pTopPriorityWork = NULL;
	m_nRunFailCnt = 1;
	m_SearchCh = 1;
}


CReserveDlg::~CReserveDlg()	//	200317 HKH Memory Leak 관련 수정
{
	ResetReservDataArray();
}

void CReserveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReserveDlg)
	DDX_Control(pDX, IDC_BUTTON_DOWN, m_btnDown);
	DDX_Control(pDX, IDC_BUTTON_UP, m_btnUp);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDC_BUTTON_RESERVE_DEL, m_btnWorkDel);
	DDX_Control(pDX, IDC_BUTTON_RESERVE_ADD, m_btnWorkAdd);
	DDX_Control(pDX, IDC_WORK_START_STOP, m_btnWorkStart);
	DDX_Control(pDX, IDC_PROGRESS, m_ctrlProgress);
	DDX_Control(pDX, IDC_LIST_RUN_QUEUE, m_listReserveQueue);
 	DDX_Control(pDX, IDC_CHK_RESERV_CH1, m_bRESRV_Ch01_ON_OFF);
 	DDX_Control(pDX, IDC_CHK_RESERV_CH2, m_bRESRV_Ch02_ON_OFF);
 	DDX_Control(pDX, IDC_CHK_RESERV_CH3, m_bRESRV_Ch03_ON_OFF);
 	DDX_Control(pDX, IDC_CHK_RESERV_CH4, m_bRESRV_Ch04_ON_OFF);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReserveDlg, CDialog)
	//{{AFX_MSG_MAP(CReserveDlg)
	ON_BN_CLICKED(IDC_BUTTON_RESERVE_ADD, OnButtonReserveAdd)
	ON_BN_CLICKED(IDC_BUTTON_TEST, OnButtonTest)
	ON_BN_CLICKED(IDC_WORK_START_STOP, OnWorkStartStop)
	ON_WM_TIMER()
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_RUN_QUEUE, OnBegindragListRunQueue)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUp)
	ON_BN_CLICKED(IDC_BUTTON_DOWN, OnButtonDown)
	ON_BN_CLICKED(IDC_BUTTON_RESERVE_DEL, OnButtonReserveDel)
	ON_WM_RBUTTONDOWN()
	ON_NOTIFY(NM_RCLICK, IDC_LIST_RUN_QUEUE, OnRclickListRunQueue)
	ON_COMMAND(ID_WORK_LOG_VIEW, OnWorkLogView)
	ON_COMMAND(ID_CHECK_COND, OnCheckCond)
	ON_COMMAND(ID_DATA_VIEW, OnDataView)
	ON_COMMAND(ID_GRAPH_VIEW, OnGraphView)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_DESTROY()
	ON_COMMAND(ID_RESERV_CANCEL, OnReservCancel)
	ON_COMMAND(ID_CLEAR, OnClear)
	ON_NOTIFY(LVN_BEGINRDRAG, IDC_LIST_RUN_QUEUE, OnBeginrdragListRunQueue)
	ON_WM_LBUTTONDOWN()
	//ON_BN_CLICKED(IDC_WORK_START_TEST, OnWorkStartTest)
	ON_BN_CLICKED(IDC_BUTTON_RESERVE_RESTART, OnButtonReserveRestart)
	ON_BN_CLICKED(IDC_CHK_RESERV_CH1, OnRadReservCh1)
	ON_BN_CLICKED(IDC_CHK_RESERV_CH2, OnRadReservCh2)
	ON_BN_CLICKED(IDC_CHK_RESERV_CH3, OnRadReservCh3)
	ON_BN_CLICKED(IDC_CHK_RESERV_CH4, OnRadReservCh4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReserveDlg message handlers

BOOL CReserveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here


	CWinApp* pApp = AfxGetApp();
	CRect rect;

	InitList();
	InitDatabase();
	RequeryReservList();


	//상태바 생성
	static UINT BASED_CODE indicators[] =   
    {  
			ID_RESERV_DLG_INDICATOR1,  
			ID_RESERV_DLG_INDICATOR2//,
			//ID_RESERV_DLG_INDICATOR3       
    }; 
	
	m_StatusBar.Create(this);
	m_StatusBar.SetIndicators(indicators, 2);
	
	GetClientRect(&rect);
	//m_StatusBar.SetPaneInfo(0, ID_RESERV_DLG_INDICATOR1, SBPS_NORMAL, rect.Width()/3);	
	//m_StatusBar.SetPaneInfo(1, ID_RESERV_DLG_INDICATOR2, SBPS_NORMAL, rect.Width()/3);	
	//m_StatusBar.SetPaneInfo(2, ID_RESERV_DLG_INDICATOR3, SBPS_NORMAL, rect.Width()/3);
	
	//m_StatusBar.SetPaneText(0,"예약 작업 멈춤");	
	//m_StatusBar.SetPaneText(0,"Scheduler Stopped");	
	m_StatusBar.SetPaneText(0,Fun_FindMsg("OnInitDialog_msg1","IDD_WORK_RESERVE_DLG"));	

	//레지스트리 불러오기
	m_nDelayedStartWaitTime = pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"Start Delay",5);

	m_bRESRV_Ch01_ON_OFF.SetCheck(pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"RESEV_CH1_ONOFF",0));//yulee 20190503
	m_bRESRV_Ch02_ON_OFF.SetCheck(pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"RESEV_CH2_ONOFF",0));	
	m_bRESRV_Ch03_ON_OFF.SetCheck(pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"RESEV_CH3_ONOFF",0));
	m_bRESRV_Ch04_ON_OFF.SetCheck(pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"RESEV_CH4_ONOFF",0));

	//대화 상자 크기 불러오기
	rect.top = 0;
	rect.bottom = 	pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"Window Height",400);
	rect.left = 0;
	/*rect.right = pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"Window Width",1280);*/
	rect.right = pApp->GetProfileInt(RESERVATION_REGISTRY_KEY,"Window Width",1545); //yulee 20181029
	MoveWindow(&rect);

	//Column 크기 불러오기
// 	int i;
// 	int nColumnCnt = m_listReserveQueue.GetHeaderCtrl()->GetItemCount();
// 	int* pData = new int[nColumnCnt];	
// 	UINT nBytes;
// 	ZeroMemory(pData,sizeof(int)*nColumnCnt);
// 	pApp->GetProfileBinary(RESERVATION_REGISTRY_KEY,"Column Width",(LPBYTE*)&pData,&nBytes);
// 
// 	if(pData)
// 	{
// 		for(int i = 0;i<nColumnCnt;i++)
// 		{
// 			if(pData[i])
// 			{
// 				m_listReserveQueue.SetColumnWidth(i,pData[i]);
// 			}		
// 		}	
// 	}	
	
//	delete pData;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CReserveDlg::InitList()
{

	m_listReserveQueue.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	/*
	m_listReserveQueue.InsertColumn(LIST_INDEX_PRIORITY,"순서",LVCFMT_CENTER,				40	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_STATE,"상태",LVCFMT_CENTER,					80	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_CHANNEL,"채널",LVCFMT_CENTER,				80	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_MODEL_NAME,"목록명",LVCFMT_CENTER,			150	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_SCH_NAME,"스케쥴명",LVCFMT_CENTER,			150	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_WORK_NAME,"작업명",LVCFMT_CENTER,			240	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_WORKER_NAME,"예약자",LVCFMT_CENTER,			100	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_RESERVED_TIME,"예약시간",LVCFMT_CENTER,		120	);	
	m_listReserveQueue.InsertColumn(LIST_INDEX_START_TIME,"시작시간",LVCFMT_CENTER,			120	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_LOT,"Cell 번호",LVCFMT_CENTER,				80	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_COMMENT,"설명",LVCFMT_CENTER,				100	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_CHAMBER,"챔버 연동",LVCFMT_CENTER,			80	);*/ //&&
	
	/*m_listReserveQueue.InsertColumn(LIST_INDEX_PRIORITY,"Priority",LVCFMT_CENTER,80);
	m_listReserveQueue.InsertColumn(LIST_INDEX_STATE,"State",LVCFMT_CENTER,80);
	m_listReserveQueue.InsertColumn(LIST_INDEX_CHANNEL,"Ch",LVCFMT_LEFT,140);
	m_listReserveQueue.InsertColumn(LIST_INDEX_MODEL_NAME,"Model",LVCFMT_CENTER,150);
	m_listReserveQueue.InsertColumn(LIST_INDEX_SCH_NAME,"Schedule",LVCFMT_CENTER,150);
	m_listReserveQueue.InsertColumn(LIST_INDEX_WORK_NAME,"Operation Name",LVCFMT_CENTER,240);
	m_listReserveQueue.InsertColumn(LIST_INDEX_WORKER_NAME,"Operator",LVCFMT_CENTER,120);
	m_listReserveQueue.InsertColumn(LIST_INDEX_RESERVED_TIME,"Scheduled time",LVCFMT_CENTER,160);	
	m_listReserveQueue.InsertColumn(LIST_INDEX_START_TIME,"start time",LVCFMT_CENTER,160);
	m_listReserveQueue.InsertColumn(LIST_INDEX_LOT,"Cell No",LVCFMT_LEFT,80);
	m_listReserveQueue.InsertColumn(LIST_INDEX_COMMENT,"Comment",LVCFMT_LEFT,100);
	m_listReserveQueue.InsertColumn(LIST_INDEX_CHAMBER,"Chamber Link",LVCFMT_LEFT,80);*/

	m_listReserveQueue.InsertColumn(LIST_INDEX_PRIORITY,Fun_FindMsg("InitList_msg1","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,		40	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_STATE,Fun_FindMsg("InitList_msg2","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,		80	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_CHANNEL,Fun_FindMsg("InitList_msg3","IDD_WORK_RESERVE_DLG"),LVCFMT_LEFT,		80	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_MODEL_NAME,Fun_FindMsg("InitList_msg4","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,	150	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_SCH_NAME,Fun_FindMsg("InitList_msg5","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,		150	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_WORK_NAME,Fun_FindMsg("InitList_msg6","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,	240	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_WORKER_NAME,Fun_FindMsg("InitList_msg7","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,	100	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_RESERVED_TIME,Fun_FindMsg("InitList_msg8","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,120	);	
	m_listReserveQueue.InsertColumn(LIST_INDEX_START_TIME,Fun_FindMsg("InitList_msg9","IDD_WORK_RESERVE_DLG"),LVCFMT_CENTER,	120	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_LOT,Fun_FindMsg("InitList_msg10","IDD_WORK_RESERVE_DLG"),LVCFMT_LEFT,			80	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_COMMENT,Fun_FindMsg("InitList_msg11","IDD_WORK_RESERVE_DLG"),LVCFMT_LEFT,		100	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_CHAMBER,Fun_FindMsg("InitList_msg12","IDD_WORK_RESERVE_DLG"),LVCFMT_LEFT,		80	);
	m_listReserveQueue.InsertColumn(LIST_INDEX_MUX,"MUX",LVCFMT_CENTER,70); //yulee 20181025 //$1013
}

void CReserveDlg::OnButtonReserveAdd() 
{
	CString strTemp;
	
	/*strTemp.Format(_T(Fun_FindMsg("OnButtonTest_msg1","IDD_WORK_RESERVE_DLG")));
	//strTemp.Format(_T("Caution!, Reserving on the seleted in channel information."));
	AfxMessageBox(strTemp);
	strTemp.Format(_T("Add Clicked"));*/
	log_All(_T("resv"),_T("sbc"),strTemp);
	ReserveAdd();
}

BOOL CReserveDlg::RequeryReservList()
{
	CDaoDatabase  db;

	m_listReserveQueue.DeleteAllItems();

	try
	{
		db.Open(m_strDatabaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	

	CString strSQL;
	strSQL = "SELECT * FROM Work_Reservation ORDER BY PRIORITY ASC";
	//strSQL = "SELECT * FROM Work_Reservation WHERE STATE=0 ORDER BY PRIORITY ASC";
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	int nI = 0;
	//char szBuff[128];
	if(rs.IsBOF())
	{
		rs.Close();
		return TRUE;
	}

	ResetReservDataArray();
	LPS_WORK_RESERVATION_DATA pReservData = NULL;
	//pReservData = new S_WORK_RESERVATION_DATA;		// 200313 HKH Memory Leak 수정

	while(!rs.IsEOF())
	{
		pReservData = new S_WORK_RESERVATION_DATA;		//ksj 20200707 : 원래 위치는 여기에서 메모리 할당 생성해줘야한다. 위에서 잘 못 수정해놓음.
		ZeroMemory(pReservData, sizeof(S_WORK_RESERVATION_DATA));

		data = rs.GetFieldValue(0);				//index
		pReservData->nIndex = data.lVal;
		data = rs.GetFieldValue(1);				//Priority
		pReservData->nPriority = data.lVal;
		data = rs.GetFieldValue(2);				//State
		pReservData->nState = data.lVal;
		data = rs.GetFieldValue(3);				//Selected Module + Channel ASWCHARRAY
		//pReservData->strAdwChArray = data.pcVal;
		sprintf_s(pReservData->szAdwChArray, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(4);				//module name
		//pReservData->strModelName = data.pcVal;
		//sprintf_s(pReservData->szAdwChArray, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		sprintf_s(pReservData->szModelName, sizeof(TCHAR) * 256, _T("%s"), data.pcVal); //ksj 20200707 : 오타 수정
		data = rs.GetFieldValue(5);				//schedule name
		//pReservData->strSchName = data.pcVal;
		sprintf_s(pReservData->szSchName, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(6);				//test name SCHEDULE_NAME
		//pReservData->strTestName = data.pcVal;
		sprintf_s(pReservData->szTestName, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(7);				//worker name
		//pReservData->strWorkerName = data.pcVal;
		sprintf_s(pReservData->szWorkerName, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(8);				//reserved time
		//pReservData->strReservedTime = data.pcVal;
		sprintf_s(pReservData->szReservedTime, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(9);				//start time
		//pReservData->strStartTime = data.pcVal;
		sprintf_s(pReservData->szStartTime, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(10);				//end time
		//pReservData->strEndTime = data.pcVal;
		sprintf_s(pReservData->szEndTime, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(11);				//ModelPK
		pReservData->nScheduleInfoModelPK = data.lVal;
		data = rs.GetFieldValue(12);				//TestPK
		pReservData->nScheduleInfoTestPK = data.lVal;
		data = rs.GetFieldValue(13);				//start cycle
		pReservData->nStartCycle = data.lVal;
		data = rs.GetFieldValue(14);				//start step
		pReservData->nStartStep = data.lVal;
		data = rs.GetFieldValue(15);				//chamber option START_OPT_CHAMBER
		pReservData->nStartOptChamber = data.lVal;
		data = rs.GetFieldValue(16);				//LOT	
		//pReservData->strLot = data.pcVal;
		sprintf_s(pReservData->szLot, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(17);				//COMMENT
		//pReservData->strComment = data.pcVal;
		sprintf_s(pReservData->szComment, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(18);				//DATA_PATH				
		//pReservData->strDataPath = data.pcVal;
		sprintf_s(pReservData->szDataPath, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(19);				//SCH_PATH	
		//pReservData->strSchPath = data.pcVal;
		sprintf_s(pReservData->szSchPath, sizeof(TCHAR) * 256, _T("%s"), data.pcVal);
		data = rs.GetFieldValue(20);				//CHAMBER_FIX_TEMP
		pReservData->fChamberFixTemp = (float)data.dblVal;
		data = rs.GetFieldValue(21);				//CHAMBER_PTN_NUM		
		pReservData->uiChamberPatternNum = data.lVal;
		data = rs.GetFieldValue(22);				//CHAMBER_DELTA_TEMP
		pReservData->fChamberDeltaTemp = (float)data.dblVal;
		data = rs.GetFieldValue(23);				//CHAMBER_DELAY_TIME
		pReservData->uiChamberDelayTime = data.lVal;
		data = rs.GetFieldValue(24);				//CHAMBER_DELTA_FIX_TEMP
		pReservData->fChamberDeltaFixTemp = (float)data.dblVal;

		data = rs.GetFieldValue(25);				//ENABLE_STEP_SKIP	
		pReservData->bEnableStepSkip = (data.lVal == 0)? 0:1;
		data = rs.GetFieldValue(26);				//CHAMBER_CONTINUE
		pReservData->bChamberContinue = (data.lVal == 0)? 0:1;
		data = rs.GetFieldValue(27);				//IS_CHAMBER_CONTINUE
		pReservData->bIsChamberContinue = (data.lVal == 0)? 0:1;
		data = rs.GetFieldValue(28);				//CHAMBER_CHECK_TIME
		pReservData->uiChamberCheckTime = data.lVal;
		data = rs.GetFieldValue(29);				//CHAMBER_CHECK_FIX_TIME
		pReservData->uiChamberCheckFixTime = data.lVal;
		data = rs.GetFieldValue(30);				//IS_CAN_MODE_CHECK
		pReservData->bIsCanModeCheck = (data.lVal == 0)? 0:1;
		data = rs.GetFieldValue(31);				//MUX_OPTION	
		pReservData->uiMuxOption = data.lVal; //yulee 20181025
		
		m_ptrReservData.SetAtGrow(nI, pReservData); //Data를 Array에 추가.	
		SetListItem(nI, pReservData);

		CString strTemp;
		strTemp.Format(_T("resv list no:%d state:%d"),pReservData->nIndex,pReservData->nState);
		log_All(_T("resv"),_T("sbc"),strTemp);

		//우선순위를 정리한다.
		strSQL.Format("UPDATE Work_Reservation SET PRIORITY=%d WHERE ID = %d",nI,pReservData->nIndex);
		ExecuteSQL(strSQL);

		nI++;

		rs.MoveNext();
	}

	rs.Close();
	db.Close();

	//// 200313 HKH Memory Leak 수정
	//if (pReservData != NULL)
	//{
	//	delete pReservData;
	//	pReservData = NULL;
	//}

	return TRUE;
}

void CReserveDlg::OnButtonTest() 
{
	//if(MessageBox("실행 완료된 예약 작업은 목록에서 제거 됩니다.","확인",MB_YESNO|MB_ICONINFORMATION) == IDYES)
	if(MessageBox(Fun_FindMsg("OnButtonTest_msg1","IDD_WORK_RESERVE_DLG"),Fun_FindMsg("OnButtonTest_msg2","IDD_WORK_RESERVE_DLG"),MB_YESNO|MB_ICONINFORMATION) == IDYES) //&&
	{
		if(RemoveCompleteWork())
		{
			RequeryReservList();
		
		}		
	}
}


//ksj 20160527 작업예약 정보 DB저장
BOOL CReserveDlg::AddReservData(CScheduleData* pScheduleInfo, int nModelPK, int nTestPK, CDWordArray* pAdwChArray, CStartDlgEx* pStartDlg, CString strSchPath)
{
	//포인터 Parameter 검사
	if(pScheduleInfo == NULL || pAdwChArray == NULL || pStartDlg == NULL)
	{
		//AfxMessageBox("작업 예약 Parameter Error");
		AfxMessageBox("work reservation Parameter Error");
		return FALSE;
	}

	S_WORK_RESERVATION_DATA stData;
	CDaoDatabase  db;
	m_listReserveQueue.DeleteAllItems();	
	
	//DB 오픈
	try
	{
		db.Open(m_strDatabaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	CString strSQL;	
	CString strSQL2;

	//기존 예약 Data 검색.
	strSQL = "SELECT * FROM Work_Reservation ";	
	
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}

	//DB에 저장하기 위한 Data 처리
	while(!rs.IsEOF()) rs.MoveNext(); //전체 레코드 개수 얻기 위한 탐색.	
	int nTotalReservation = rs.GetRecordCount(); //전체 레코드 개수	
	int i;
	CString strAdwChArray;
	CString strReservedTime;
	char szBuff[256];
	CTime t = CTime::GetCurrentTime();
	strAdwChArray.Empty();

	
	for(int i = 0;i<pAdwChArray->GetSize();i++)  //선택 채널 정보를 DB에 저장하기 위해 String으로 바꾼다.
	{
		ltoa(pAdwChArray->GetAt(i),szBuff,10);
		strAdwChArray += szBuff;
		strAdwChArray += ";";
	}
	
	strReservedTime.Format("%d-%02d-%02d %02d:%02d:%02d",
		t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond()); //예약 시간
	

	//DB에 저장할 Data 정리.
	// 200313 HKH Memory Leak 수정
	stData.nPriority = nTotalReservation; //작업 우선 순위 (최하위)
	//stData.strAdwChArray = strAdwChArray;
	//stData.strModelName = pScheduleInfo->GetModelName();
	//stData.strSchName = pScheduleInfo->GetScheduleName();
	//stData.strTestName = pStartDlg->GetFileName();
	//stData.strWorkerName = pStartDlg->m_strWorker;
	//stData.strReservedTime = strReservedTime;
	sprintf_s(stData.szAdwChArray, sizeof(TCHAR) * 256, _T("%s"), strAdwChArray);
	sprintf_s(stData.szModelName, sizeof(TCHAR) * 256, _T("%s"), pScheduleInfo->GetModelName());
	sprintf_s(stData.szSchName, sizeof(TCHAR) * 256, _T("%s"), pScheduleInfo->GetScheduleName());
	sprintf_s(stData.szTestName, sizeof(TCHAR) * 256, _T("%s"), pStartDlg->GetFileName());
	sprintf_s(stData.szWorkerName, sizeof(TCHAR) * 256, _T("%s"), pStartDlg->m_strWorker);
	sprintf_s(stData.szReservedTime, sizeof(TCHAR) * 256, _T("%s"), strReservedTime);

	stData.nScheduleInfoModelPK = nModelPK;
	stData.nScheduleInfoTestPK = nTestPK;
	stData.nStartCycle = pStartDlg->m_nStartCycle;
	stData.nStartStep = pStartDlg->m_nStartStep;
	stData.nStartOptChamber = pStartDlg->m_nStartOptChamber;

	//stData.strLot = pStartDlg->m_strLot;
	//stData.strComment = pStartDlg->m_strComment;
	//stData.strDataPath = pStartDlg->m_strDataPath;
	//stData.strSchPath = strSchPath;
	sprintf_s(stData.szLot, sizeof(TCHAR) * 256, _T("%s"), pStartDlg->m_strLot);
	sprintf_s(stData.szComment, sizeof(TCHAR) * 256, _T("%s"), pStartDlg->m_strComment);
	sprintf_s(stData.szDataPath, sizeof(TCHAR) * 256, _T("%s"), pStartDlg->m_strDataPath);
	sprintf_s(stData.szSchPath, sizeof(TCHAR) * 256, _T("%s"), strSchPath);

	stData.fChamberFixTemp = pStartDlg->m_fChamberFixTemp;
	stData.uiChamberPatternNum = pStartDlg->m_uiChamberPatternNum;	
	stData.fChamberDeltaTemp = pStartDlg->m_fChamberDeltaTemp;
	stData.uiChamberDelayTime = pStartDlg->m_uiChamberDelayTime;
	stData.fChamberDeltaFixTemp = pStartDlg->m_fChamberDeltaFixTemp;

	stData.bEnableStepSkip = pStartDlg->m_bEnableStepSkip;
	stData.bChamberContinue = pStartDlg->m_bChamberContinue;
	stData.bIsChamberContinue = pStartDlg->m_IsChamberContinue;
	stData.uiChamberCheckTime = pStartDlg->m_uiChamberCheckTime;	
	stData.uiChamberCheckFixTime = 	pStartDlg->m_uiChamberCheckFixTime;
	stData.bIsCanModeCheck = pStartDlg->m_bIsCanModeCheck;	 
	stData.uiMuxOption = pStartDlg->m_nCboMuxChoice; //yulee 20181025

 
	//SQL 쿼리문 생성.	// 200313 HKH Memory Leak 수정
	strSQL.Format("INSERT INTO Work_Reservation(PRIORITY,ADWCHARRAY,MODEL_LIST_NAME,SCHEDULE_NAME,WORK_NAME,WORKER_NAME,RESERVED_TIME,SCHEDULEINFO_MODELPK,SCHEDULEINFO_TESTPK,START_CYCLE,START_STEP,START_OPT_CHAMBER,LOT,COMMENT,DATA_PATH,SCH_PATH,CHAMBER_FIX_TEMP,CHAMBER_PTN_NUM,CHAMBER_DELTA_TEMP,CHAMBER_DELAY_TIME,CHAMBER_DELTA_FIX_TEMP,ENABLE_STEP_SKIP,CHAMBER_CONTINUE,IS_CHAMBER_CONTINUE,CHAMBER_CHECK_TIME,CHAMBER_CHECK_FIX_TIME,IS_CAN_MODE_CHECK, MUX_OPTION)"); //yulee 20181025 MUX_OPTION 추가
	strSQL2.Format(" VALUES(%d,'%s','%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,'%s','%s','%s','%s',%f,%d,%f,%d,%f,%d,%d,%d,%d,%d,%d,%d)",
							stData.nPriority,
							//stData.strAdwChArray,
							//stData.strModelName,
							//stData.strSchName,
							//stData.strTestName,
							//stData.strWorkerName,
							//stData.strReservedTime,
							stData.szAdwChArray,
							stData.szModelName,
							stData.szSchName,
							stData.szTestName,
							stData.szWorkerName,
							stData.szReservedTime,
							stData.nScheduleInfoModelPK,
							stData.nScheduleInfoTestPK,
							stData.nStartCycle,
							stData.nStartStep,
							stData.nStartOptChamber,
							//stData.strLot,
							//stData.strComment,
							//stData.strDataPath,
							//stData.strSchPath,
							stData.szLot,
							stData.szComment,
							stData.szDataPath,
							stData.szSchPath,
							stData.fChamberFixTemp,
							stData.uiChamberPatternNum,
							stData.fChamberDeltaTemp,
							stData.uiChamberDelayTime,
							stData.fChamberDeltaFixTemp,

							stData.bEnableStepSkip,
							stData.bChamberContinue,
							stData.bIsChamberContinue,
							stData.uiChamberCheckTime,
							stData.uiChamberCheckFixTime,
							stData.bIsCanModeCheck, 
							stData.uiMuxOption //yulee 20181025
							);
	

	strSQL += strSQL2;

#ifdef _DEBUG
	//AfxMessageBox(strSQL);
#endif

	//쿼리 실행
	try
	{
		db.Execute(strSQL);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();		
	}	

	rs.Close();
	db.Close();

	RequeryReservList(); //리스트 갱신

	return TRUE;
}

//DB 초기화
void CReserveDlg::InitDatabase()
{
	CString strDatabaseName = m_pDoc->GetDataBaseName();
	int nPos = strDatabaseName.ReverseFind('\\');
	CString strPath = strDatabaseName.Left(nPos);
	
	m_strDatabaseName.Format("%s\\%s",strPath,"Work_Reservation.mdb");
}

//ksj 20160527 문자열로 저장했던 채널 정보를 DWord Array로 변환하여 반환한다.
BOOL CReserveDlg::ConvertAdwChArrayFromString(CString strAdwChArray, CDWordArray* pAdwChArray)
{
	
	if(pAdwChArray == NULL)
	{
		AfxMessageBox("NULL parameter error");
		return FALSE;
	}

	int nCount = 0;
	//int bContinue = TRUE;
	CString strExt;
	
	do{
		AfxExtractSubString(strExt, strAdwChArray, nCount++, ';');

		if(!strExt.IsEmpty())
		{
			pAdwChArray->Add(atol(strExt));			
		}
	}while(!strExt.IsEmpty());

	return TRUE;
}

//ksj 20160527 채널 DWord Array를 화면에 표시하기 위한 문자열로 변환한다.
CString CReserveDlg::ConvertAdwChArrayToString(CDWordArray *pAdwChArray)
{
	if(pAdwChArray == NULL)
	{
		AfxMessageBox("NULL parameter error");
		return "";
	}
	CString strResult;
	strResult.Empty();

	int i;
	DWORD dwData;
	int nChannel;
	int nModule;
	CString strTmp;

	for(int i = 0;i<pAdwChArray->GetSize();i++)
	{
		dwData = pAdwChArray->GetAt(i);

		nModule = HIWORD(dwData);
		nChannel = LOWORD(dwData);

		if(i!=0)
		{
			strResult += ", ";
		}
		strTmp.Format("M%02dC%02d",nModule,nChannel+1);
		strResult += strTmp;
	}

	return strResult;
}


//상태 코드를 문자열로 반환
CString CReserveDlg::ConvertStateCodeToString(int nState)
{

	switch(nState)
	{
		case RESERV_STATE_WAITING:
			//return "대기";	
			//return "Wait";
			return Fun_FindMsg("ConvertStateCodeToString_msg1","IDD_WORK_RESERVE_DLG");
		case RESERV_STATE_WORKING:
			//return "실행완료";	
			//return "Complete";
			return Fun_FindMsg("ConvertStateCodeToString_msg2","IDD_WORK_RESERVE_DLG");
		case RESERV_STATE_FAILED:
			//return "실패";
			//return "Failure";
			return Fun_FindMsg("ConvertStateCodeToString_msg3","IDD_WORK_RESERVE_DLG");
		case RESERV_STATE_PAUSED:
				// return "일시정지";
			return Fun_FindMsg("ConvertStateCodeToString_msg5","IDD_WORK_RESERVE_DLG");
		case RESERV_STATE_DOWNRAWDATA://yulee 20190420_1
				//return "SBC Data Download";
			return Fun_FindMsg("ConvertStateCodeToString_msg6","IDD_WORK_RESERVE_DLG");
		
	}

	//return "알 수 없음";
	//return "Unknown";
	return Fun_FindMsg("ConvertStateCodeToString_msg4","IDD_WORK_RESERVE_DLG");
}

//예약 Data Array 지우기.
void CReserveDlg::ResetReservDataArray()
{

	int i;
	LPS_WORK_RESERVATION_DATA pReservData;

	for(int i = 0;i<m_ptrReservData.GetSize();i++)
	{
		pReservData = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(i);
		if(pReservData != NULL)
		{
			delete pReservData;			
		}
	}

	m_ptrReservData.RemoveAll();
	
}

//ksj 20160527 선택한 채널들이 작업 가능한 상태인지 확인.
BOOL CReserveDlg::CheckCanBeStart(CDWordArray *pAdwChArray, int nStartOptChamber)
{

//ksj 20180223 :test
#ifdef _DEBUG
	return TRUE;
#endif
//ksj end

	UINT nModuleID, nChannelIndex;
	int i,j;
	int nChamberOperationType;
	//COvenCtrl	*pCtrlOven;
	//int		nOvenPortLine;
	CDWordArray awOvenArray;

	for(int i = 0;i<pAdwChArray->GetSize();i++)
	{
		nModuleID = HIWORD(pAdwChArray->GetAt(i));
		nChannelIndex = LOWORD(pAdwChArray->GetAt(i));

		CCyclerModule *pMD = m_pParent->GetDocument()->GetModuleInfo(nModuleID);
		if(!pMD) continue;

		if (pMD->GetState() == PS_STATE_LINE_OFF)
			return FALSE;

		CCyclerChannel *pCH = pMD->GetChannelInfo(nChannelIndex);
		if(!pCH) continue;

		//채널이 RUN 명령 전송 가능한 상태인지 확인
		if(!m_pParent->GetDocument()->CheckCmdState(nModuleID, nChannelIndex, SFT_CMD_RUN_QUEUE))
		{		
#ifdef _DEBUG			
			TRACE("M%02dC%02d 작업 시작 불가",nModuleID,nChannelIndex+1);			
#endif
			return FALSE; //전송 불가.
		}
		else
		{
			if(nStartOptChamber != 4) //챔버 사용하는 스케쥴인 경우.
			{
				//챔버를 채널들이 같이 사용하는 경우. 다른 채널도 사용가능해야 작업 가능.
				nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 0);
				if(nChamberOperationType == 0)
				{							
					CCyclerChannel* pCH;
					WORD state;
					//해당 챔버 작업할 채널이 포함되어있는 모듈의 다른 채널 모두 검색
					for(j=0;j<pMD->GetTotalChannel();j++)
					{					
						pCH = pMD->GetChannelInfo(j);
						state = pCH->GetState();
						//채널이 RUN 명령 전송 가능한 상태인지 확인
						if(pCH->GetChamberUsing() && (state == PS_STATE_RUN || state == PS_STATE_PAUSE)) //챔버 연동 작업중인 채널이 있으면 시작하면 안됨.
						{		
#ifdef _DEBUG			
							TRACE("M%02dC%02d 작업 시작 불가",nModuleID,j+1);			
#endif
							return FALSE; //전송 불가.
						}
					}
				}
			}
			
		}
#ifdef _DEBUG			
			TRACE("M%02dC%02d 작업 시작 가능",nModuleID,nChannelIndex+1);			
#endif
	}

	return TRUE; //전송 가능.
}

//ksj 20160527 예약 Data를 작업 시작하기 위해 불러와 복원
BOOL CReserveDlg::RestoreReservedWork(LPVOID pWork, CScheduleData* pScheduleInfo, CDWordArray* pAdwChArray, CStartDlgEx *pStartDlg, int *pModelPK, int *pTestPK)
{
	ASSERT(pWork);
	ASSERT(pStartDlg);

	LPS_WORK_RESERVATION_DATA pReservedWork = (LPS_WORK_RESERVATION_DATA) pWork;
	
	pStartDlg->m_nStartCycle = pReservedWork->nStartCycle;
	pStartDlg->m_nStartOptChamber = pReservedWork->nStartOptChamber;
	pStartDlg->m_nStartStep = pReservedWork->nStartStep;
	// 200313 HKH Memory Leak 수정
	//pStartDlg->m_strLot = pReservedWork->strLot;
	//pStartDlg->m_strComment = pReservedWork->strComment;
	//pStartDlg->m_strDataPath = pReservedWork->strDataPath;
	pStartDlg->m_strLot.Format(_T("%s"), pReservedWork->szLot);
	pStartDlg->m_strComment.Format(_T("%s"), pReservedWork->szComment);
	pStartDlg->m_strDataPath.Format(_T("%s"), pReservedWork->szDataPath);
	
	pStartDlg->m_fChamberFixTemp = pReservedWork->fChamberFixTemp;
	pStartDlg->m_uiChamberPatternNum = pReservedWork->uiChamberPatternNum;
	pStartDlg->m_fChamberDeltaTemp = pReservedWork->fChamberDeltaTemp;
	pStartDlg->m_uiChamberDelayTime = pReservedWork->uiChamberDelayTime;
	pStartDlg->m_fChamberDeltaFixTemp = pReservedWork->fChamberDeltaFixTemp;

	pStartDlg->m_bEnableStepSkip = pReservedWork->bEnableStepSkip;
	pStartDlg->m_bChamberContinue = pReservedWork->bChamberContinue;
	pStartDlg->m_IsChamberContinue = pReservedWork->bIsChamberContinue;
	pStartDlg->m_uiChamberCheckTime = pReservedWork->uiChamberCheckTime;
	pStartDlg->m_uiChamberCheckFixTime = pReservedWork->uiChamberCheckFixTime;
	pStartDlg->m_bIsCanModeCheck = pReservedWork->bIsCanModeCheck;
	pStartDlg->m_nCboMuxChoice = pReservedWork->uiMuxOption;//yulee 20181025
	
	*pModelPK = pReservedWork->nScheduleInfoModelPK;
	*pTestPK = pReservedWork->nScheduleInfoTestPK;	
		
	// 200313 HKH Memory Leak 수정
	//ConvertAdwChArrayFromString(pReservedWork->strAdwChArray,pAdwChArray);
	ConvertAdwChArrayFromString(pReservedWork->szAdwChArray,pAdwChArray);

	//pScheduleInfo->SetSchedule(pReservedWork->strSchPath);
	pScheduleInfo->SetSchedule(pReservedWork->szSchPath);


	//채널 설정 복원

	CCyclerModule* pMD;
	CCyclerChannel* pChannel;
	int i, nModuleID, nChannelIndex;
	int dwData;
	CString strContent;
	BOOL bContinue;
	CFileFind fileFind;
	CString strTmp;

	for(int i = 0;i<pAdwChArray->GetSize();i++)
	{
		dwData = pAdwChArray->GetAt(i);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);

		pMD = m_pDoc->GetModuleInfo(nModuleID);
		pChannel = m_pDoc->GetChannelInfo(nModuleID, nChannelIndex);
		pChannel->ResetData(); //문제 되면 삭제.

		strContent.Format("M%02dCh%02d", nModuleID, nChannelIndex+1, nChannelIndex);		
		//strTmp.Format("%s\\%s\\*.*",pReservedWork->strDataPath,pReservedWork->strTestName);
		strTmp.Format("%s\\%s\\*.*",pReservedWork->szDataPath, pReservedWork->szTestName);		// 200313 HKH Memory Leak 수정
		bContinue = fileFind.FindFile(strTmp);
		while(bContinue)
		{
			bContinue = fileFind.FindNextFile();

			if(fileFind.IsDirectory())
			{
				strTmp = fileFind.GetFileName();
				if(strTmp.Find(strContent,0) != -1)
				{
					//strContent.Format("%s\\%s\\%s",pReservedWork->strDataPath,pReservedWork->strTestName, strTmp);
					strContent.Format("%s\\%s\\%s",pReservedWork->szDataPath, pReservedWork->szTestName, strTmp);	// 200313 HKH Memory Leak 수정
					pChannel->SetFilePath(strContent);
					break;
				}
			}
			strTmp = "";
		}
		if(strTmp == "")
			return FALSE;
		
		pChannel->SetTestSerial(pStartDlg->m_strLot, pStartDlg->m_strWorker, pStartDlg->m_strComment,pChannel->m_strCellDeltaVolt);
		pChannel->LoadSaveItemSet();

		pChannel->WriteCtsFile();

	}

	//Chamber 및 Can 통신 설정 복원	(원본 위치: CStartDlg::OnOK() )
	CString strTemp;

	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Start Chamber Option", pStartDlg->m_nStartOptChamber);
	strTemp.Format("%.1f",pStartDlg->m_fChamberFixTemp);
	AfxGetApp()->WriteProfileString(OVEN_SETTING, "Fix Temp",strTemp);
	strTemp.Format("%.1f",pStartDlg->m_fChamberDeltaFixTemp);
	AfxGetApp()->WriteProfileString(OVEN_SETTING, "Delta Fix Temp",strTemp);
	strTemp.Format("%.1f",pStartDlg->m_fChamberDeltaTemp);
	AfxGetApp()->WriteProfileString(OVEN_SETTING, "Delta Temp",strTemp);
	
	
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Pattern No", pStartDlg->m_uiChamberPatternNum);
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Start Delay Time", pStartDlg->m_uiChamberDelayTime);

	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Fix Check Time", pStartDlg->m_uiChamberCheckFixTime);
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Schedule Check Time", pStartDlg->m_uiChamberCheckTime);

	CString RegName;

	for(int i = 0;i<pAdwChArray->GetSize();i++)
	{
		dwData = pAdwChArray->GetAt(i);
		nModuleID = HIWORD(dwData);
		nChannelIndex = LOWORD(dwData);
		
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		pChannel = m_pDoc->GetChannelInfo(nModuleID, nChannelIndex);

		if (pStartDlg->m_nStartOptChamber == 3) 
			pChannel->SetUseOvenMode(TRUE);
		else
			pChannel->SetUseOvenMode(FALSE);
		
		pChannel->m_nStartOptChamber = pStartDlg->m_nStartOptChamber;
		pChannel->m_fchamberFixTemp = pStartDlg->m_fChamberFixTemp;
		pChannel->m_fChamberDeltaFixTemp = pStartDlg->m_fChamberDeltaFixTemp;
		pChannel->m_fchamberDeltaTemp = pStartDlg->m_fChamberDeltaTemp;
		pChannel->m_uichamberPatternNum = pStartDlg->m_uiChamberPatternNum;
		pChannel->m_uiChamberDelayTime = pStartDlg->m_uiChamberDelayTime;
		pChannel->m_uiChamberCheckTime = pStartDlg->m_uiChamberCheckTime;
		pChannel->m_uiChamberCheckFixTime = pStartDlg->m_uiChamberCheckFixTime;
		
    	//	RegName.Format("UseOvenChamperContinue%d",pChannel->GetChannelIndex() + 1);				
		//	AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, TRUE);
		
		//yulee 20180914
		RegName.Format("OvenContinue_%d",pChannel->GetChannelIndex() + 1);	

		BOOL bContinueValue;  //yulee 20190715
		if(pStartDlg->m_IsChamberContinue == TRUE)
			bContinueValue = TRUE;
		else
			bContinueValue = FALSE;
 		AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, bContinueValue);


		
		if(pStartDlg->m_bChamberContinue){						
			AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, pStartDlg->m_IsChamberContinue);			
			
			if(pStartDlg->m_IsChamberContinue){
				m_pDoc->m_bChamberStand = 1;
			}else{
				m_pDoc->m_bChamberStand = 0;
			}
		}			
		RegName.Format("UseCanModeCheck%d%d",pChannel->GetModuleID(),pChannel->GetChannelIndex() + 1);
		
		//AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, RegName, FALSE);
		
		//ksj 20180223 : 임시 주석
		/*
		int nCanCheckMode =  pStartDlg->m_ctrlCboCanCheck.GetItemData( pStartDlg->m_ctrlCboCanCheck.GetCurSel());
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, RegName, pStartDlg->m_bIsCanModeCheck);*/
	}
	
	return TRUE;
}

void CReserveDlg::CheckReservedWork()
{	
	if(!m_bWorking)
		return;

	SetChResvStopBySbcStoporPause();//yulee 20190707

	//m_StatusBar.SetPaneText(1,"다음 작업 실행 대기중입니다....");
	//m_StatusBar.SetPaneText(1,"Waiting for Run a next operation.....");
	m_StatusBar.SetPaneText(1,Fun_FindMsg("CheckReservedWork_msg1","IDD_WORK_RESERVE_DLG"));

	LPS_WORK_RESERVATION_DATA pTopPriorityWork = NULL;
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	int i;
//	int nCount=0;	

	//m_pTopPriorityWork = NULL;
	m_nProgressCnt = 0;
	//yulee 20190420_2
	//대기 상태인 예약 항목을 가져온다.
	int nRsvCh1=0, nRsvCh2=0, nRsvCh3=0, nRsvCh4=0, nRsvSearchChMax=0;
	BOOL bRetCntWaitingResv;
	bRetCntWaitingResv = GetCntWaitingResv(nRsvCh1, nRsvCh2, nRsvCh3, nRsvCh4);

	if(nRsvCh1*1 != 0) nRsvSearchChMax = 1;
	if(nRsvCh2*1 != 0) nRsvSearchChMax = 2;
	if(nRsvCh3*1 != 0) nRsvSearchChMax = 3;
	if(nRsvCh4*1 != 0) nRsvSearchChMax = 4;

	nRsvCh1 *= (int)(m_bRESRV_Ch01_ON_OFF.GetCheck());//yulee 20190503
	nRsvCh2 *= (int)(m_bRESRV_Ch02_ON_OFF.GetCheck());
	nRsvCh3 *= (int)(m_bRESRV_Ch03_ON_OFF.GetCheck());
	nRsvCh4 *= (int)(m_bRESRV_Ch04_ON_OFF.GetCheck());

	//yulee 20190420_2
	//1. 레지스트리에 저장된 검색 채널 가져오기
	//2. 가져온 채널이 예약이 되어 있는지 확인(nRsvCh*의 값 확인 비교)
	//3. nRsvCh*값이 0이면 다음 채널 검색으로 넘어가면서 레지스트리에 검색 채널 업데이트
	//4. nRsrvCh*값이 0이 아니면 해당 채널에 해당하는 예약 대기 중인 첫번째 작업 찾기

	BOOL bRsvWorkExist, bRet;
	bRsvWorkExist = FALSE;

	bRet = IsChWaitingRsvWork(m_SearchCh, bRsvWorkExist, nRsvSearchChMax, nRsvCh1, nRsvCh2, nRsvCh3, nRsvCh4);

	CString strExt;
	CDWordArray adwChArray;
	int nCount = 0, nSearchedCh;

	if(bRsvWorkExist)
	{
		//해당 채널에 대기 중인 우선 순위 1번 예약작업 검색
	for(int i = 0;i<m_ptrReservData.GetSize();i++)
		{
			pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(i);
			
			CString strTemp;
			strTemp.Format(_T("resv work no:%d state:%d"),pWork->nIndex,pWork->nState);
			log_All(_T("resv"),_T("sbc"),strTemp);
			
			do{
				//AfxExtractSubString(strExt, pWork->strAdwChArray, nCount++, ';');
				AfxExtractSubString(strExt, pWork->szAdwChArray, nCount++, ';');	// 200313 HKH Memory Leak 수정
				
				if(!strExt.IsEmpty())
				{
					adwChArray.Add(atol(strExt));			
				}
			}while(!strExt.IsEmpty());
			nCount = 0;
			nSearchedCh = LOWORD(adwChArray.GetAt(0))+1;//예약 리스트의 i번째의 채널 값
			adwChArray.RemoveAll();
			if(pWork->nState == RESERV_STATE_WAITING) //예약 대기중인 첫번째 작업 찾기. /i번째 값이 Waiting이면
			{	
				if(nSearchedCh == m_SearchCh)
				{
					pTopPriorityWork = pWork;
					m_pTopPriorityWork = pWork;
					break;
				}
				else
				{
				
				}
			}
		}
	}
	else
	{
		m_SearchCh = (m_SearchCh%4)+1;//yulee 20190420_2
	}
/* //yulee 20190420_2 주석 처리
	//m_pTopPriorityWork = NULL;
	m_nProgressCnt = 0;
	
	//대기중인 우선 순위 1번 예약작업 검색
	for(i=0;i<m_ptrReservData.GetSize();i++)
	{
		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(i);
		
		CString strTemp;
		strTemp.Format(_T("resv work no:%d state:%d"),pWork->nIndex,pWork->nState);
		log_All(_T("resv"),_T("sbc"),strTemp);

		if(pWork->nState == RESERV_STATE_WAITING) //예약 대기중인 첫번째 작업 찾기.
		{
			//if(pTopPriorityWork == NULL)
			//{
				//m_pTopPriorityWork = pWork;			
				pTopPriorityWork = pWork;
				break;
			//}
		}
		else
		{
			m_nProgressCnt++;//완료된 작업 갯수
		}
	}
*/
	//if(m_pTopPriorityWork != NULL)
	if(pTopPriorityWork != NULL)
	{
		CDWordArray adwChArray;
		//ConvertAdwChArrayFromString(pTopPriorityWork->strAdwChArray,&adwChArray); //채널 DB string값을 array로 변환
		ConvertAdwChArrayFromString(pTopPriorityWork->szAdwChArray,&adwChArray); //채널 DB string값을 array로 변환	// 200313 HKH Memory Leak 수정
		
		if(CheckCanBeStart(&adwChArray,pTopPriorityWork->nStartOptChamber)) //채널이 사용가능한지 확인.
		{
			//예약된 작업의 채널들이 준비되어있음. 작업시작가능.			

			if(!m_nWorkCount) m_nDelayedStartSec = m_nDelayedStartWaitTime; //첫번째 작업은 대기 없이 바로 시작.
				
			KillTimer(TIMER_CHECK_START);			
			SetTimer(TIMER_DELAYED_START, 1000, NULL); //일정 시간 대기후 다음 예약 작업 실행. 1초간격으로 DelayedStart() 함수 호출. //ksj 20200707 : 코드 원복함, 아래 자동 백업이 정상적으로 되지 않아 아래 코드 동작안하므로, 임시 원복 처리.
			//SetTimer(TIMER_DELAYED_STARTBEFDOWN, 1000, NULL); //일정 시간 대기후 다음 예약 작업 실행. 1초간격으로 DelayedStart() 함수 호출. //ksj 20200707 : 주석처리
		}
		else
		{
//			int nNextSearchCh;//yulee 20190420_2
// 			nNextSearchCh = LOWORD(adwChArray.GetAt(0))%4+1;
// 			pApp->WriteProfileInt(RESERVATION_REGISTRY_KEY, "ReserveSearchCh", nNextSearchCh);
			m_SearchCh = (m_SearchCh%4)+1;//yulee 20190420_2
		}
	}

	m_ctrlProgress.SetRange(0,m_ptrReservData.GetSize());
	m_ctrlProgress.SetPos(m_nProgressCnt);		

	if(m_ptrReservData.GetSize() == m_nProgressCnt)
	{
		//KillTimer(TIMER_CHECK_START);
		//MessageBox("예약된 모든 작업을 실행했습니다.","작업 예약",MB_OK|MB_ICONINFORMATION);				
		OnWorkStartStop(); //작업 중단.
		//m_StatusBar.SetPaneText(1,"예약된 모든 작업을 실행했습니다.");
		//m_StatusBar.SetPaneText(1,"Operation complete all scheduled");
		m_StatusBar.SetPaneText(1,Fun_FindMsg("CheckReservedWork_msg2","IDD_WORK_RESERVE_DLG"));	
	}
}

BOOL CReserveDlg::IsChWaitingRsvWork(int nSchCh, int &bRsvChExist, int nSearchMax, int Ch1Value, int Ch2Value, int Ch3Value, int Ch4Value)//yulee 20190420_2
{
	int i;

	if(nSearchMax != 0)
	{
		for(i=0;i<nSearchMax;i++)
		{
			switch(nSchCh)
			{
			case 1:
				{
					if(Ch1Value > 0)
						bRsvChExist = TRUE;
					break;
				}
			case 2:
				{
					if(Ch2Value > 0)
						bRsvChExist = TRUE;
					break;
				}
			case 3:
				{
					if(Ch3Value > 0)
						bRsvChExist = TRUE;
					break;
				}
			case 4:
				{
					if(Ch4Value > 0)
						bRsvChExist = TRUE;
					break;
				}
			default:
				break;
			}
			break;
		}
	}
	return TRUE;
}

BOOL CReserveDlg::GetCntWaitingResv(int &ChCnt1, int &ChCnt2, int &ChCnt3, int &ChCnt4)//yulee 20190420_2
{
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	UINT numCh;
	//UINT nModuleID;
	CDWordArray adwChArray;
	int nCount = 0;
	DWORD dwData;
	CString strExt;

	int i;
	for(i=0;i<m_ptrReservData.GetSize();i++)
	{
		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(i);
		do{
			//AfxExtractSubString(strExt, pWork->strAdwChArray, nCount++, ';');	
			AfxExtractSubString(strExt, pWork->szAdwChArray, nCount++, ';');		// 200313 HKH Memory Leak 수정

			if(!strExt.IsEmpty())
			{
				adwChArray.Add(atol(strExt));			
			}
		}while(!strExt.IsEmpty());
		nCount = 0;


		for(int j=0;j<adwChArray.GetSize();j++)
		{
			dwData = adwChArray.GetAt(j);
			
			//nModule = HIWORD(dwData);
			numCh = LOWORD(dwData)+1;
		}
		adwChArray.RemoveAll();

		if(pWork->nState == RESERV_STATE_WAITING)
		{
			//dwData = LOWORD(adwChArray.GetAt(1));
			//numCh = LOWORD(dwData)+1;

			switch(numCh)
			{
			case 1:
				{
					ChCnt1++;
					break;
				}
			case 2:
				{
					ChCnt2++;
					break;
				}
			case 3:
				{
					ChCnt3++;
					break;
				}
			case 4:
				{
					ChCnt4++;
					break;
				}
			default:
				break;
			}
		}
		else
			m_nProgressCnt++;//대기가 아닌 항목에 대한 카운트
	}
	return TRUE;
}

int CReserveDlg::ChangeState(int nIndex, int nState,CString strCH)
{
	CTime t = CTime::GetCurrentTime();
	CString strTime;
	CString strSQL;

	strTime.Format("%d-%02d-%02d %02d:%02d:%02d",t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond());

	//cny 181920
	if(nState==RESERV_STATE_PAUSED)
	{//PAUSE
		strSQL.Format("UPDATE Work_Reservation SET STATE=%d, START_TIME='%s' WHERE ADWCHARRAY='%s'", nState, strTime,strCH);
	}
	else
	{
		strSQL.Format("UPDATE Work_Reservation SET STATE=%d, START_TIME='%s' WHERE ID=%d", nState, strTime, nIndex);
	}
	
	return ExecuteSQL(strSQL);
}

void CReserveDlg::OnWorkStartStop() 
{
	// TODO: Add your control notification handler code here
	m_bWorking = !m_bWorking;

	if(m_bWorking)
	{
		#ifdef _DEBUG
			//AfxMessageBox("작업 예약 실행");
		#endif

	BOOL bAllChStart = TRUE;
	bAllChStart = (int)(m_bRESRV_Ch01_ON_OFF.GetCheck());
	bAllChStart = (int)(m_bRESRV_Ch02_ON_OFF.GetCheck());
	bAllChStart = (int)(m_bRESRV_Ch03_ON_OFF.GetCheck());
	bAllChStart = (int)(m_bRESRV_Ch04_ON_OFF.GetCheck());

		//yulee 20190503 모든 채널 예약 작업 할 것인지 묻고 시작
		if(bAllChStart == FALSE)
		{
		if(IDYES == AfxMessageBox(_T("Checked channel will start the reserved works. Will you start?(YES)\n Will you start all channel?(No)"), MB_YESNO))
			{
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH1_ONOFF", (int)(m_bRESRV_Ch01_ON_OFF.GetCheck()));
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH2_ONOFF", (int)(m_bRESRV_Ch02_ON_OFF.GetCheck()));
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH3_ONOFF", (int)(m_bRESRV_Ch03_ON_OFF.GetCheck()));
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH4_ONOFF", (int)(m_bRESRV_Ch04_ON_OFF.GetCheck()));
			}
			else
			{
				m_bRESRV_Ch01_ON_OFF.SetCheck(RESERV_ON_BY_CH);
				m_bRESRV_Ch02_ON_OFF.SetCheck(RESERV_ON_BY_CH);
				m_bRESRV_Ch03_ON_OFF.SetCheck(RESERV_ON_BY_CH);
				m_bRESRV_Ch04_ON_OFF.SetCheck(RESERV_ON_BY_CH);
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH1_ONOFF", (int)(m_bRESRV_Ch01_ON_OFF.GetCheck()));
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH2_ONOFF", (int)(m_bRESRV_Ch02_ON_OFF.GetCheck()));
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH3_ONOFF", (int)(m_bRESRV_Ch03_ON_OFF.GetCheck()));
				AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH4_ONOFF", (int)(m_bRESRV_Ch04_ON_OFF.GetCheck()));
				UpdateData(TRUE);
			}
		}

		
		m_nWorkCount = 0;
		m_nRunFailCnt = 0;
		//m_StatusBar.SetPaneText(0,"예약 작업 실행 중...");
		//m_StatusBar.SetPaneText(0,"Running scheduled operation...");
		m_StatusBar.SetPaneText(0,Fun_FindMsg("OnWorkStartStop_msg1","IDD_WORK_RESERVE_DLG"));

		//GetDlgItem(IDC_WORK_START_STOP)->SetWindowText("예약 작업 중단(&S)");
		//GetDlgItem(IDC_WORK_START_STOP)->SetWindowText("Stop (&S)");
		GetDlgItem(IDC_WORK_START_STOP)->SetWindowText(Fun_FindMsg("OnWorkStartStop_msg2","IDD_WORK_RESERVE_DLG"));

		RequeryReservList();
		CheckReservedWork();
		KillTimer(TIMER_CHECK_START);
		SetTimer(TIMER_CHECK_START,TIMER_CHECK_START_ELAPSE,NULL);

		//yulee 20190420_2 순서 변경 버튼 비 활성화
		GetDlgItem(IDC_BUTTON_UP)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow(FALSE);
	}
	else
	{
#ifdef _DEBUG
		//AfxMessageBox("작업 예약 실행 멈춤");
#endif
		//m_StatusBar.SetPaneText(0,"예약 작업 멈춤");
		//m_StatusBar.SetPaneText(0,"Scheduler was Stopped");
		m_StatusBar.SetPaneText(0,Fun_FindMsg("OnWorkStartStop_msg3","IDD_WORK_RESERVE_DLG"));

		m_StatusBar.SetPaneText(1,"");
		//GetDlgItem(IDC_WORK_START_STOP)->SetWindowText("예약 작업 시작(&S)");
		//GetDlgItem(IDC_WORK_START_STOP)->SetWindowText("Run scheduled Operations(&S)");
		GetDlgItem(IDC_WORK_START_STOP)->SetWindowText(Fun_FindMsg("OnWorkStartStop_msg4","IDD_WORK_RESERVE_DLG"));

		KillTimer(TIMER_CHECK_START);
		KillTimer(TIMER_DELAYED_START);
		m_ctrlProgress.SetRange(0,0);
		m_ctrlProgress.SetPos(0);	

		//yulee 20190420_2 순서 변경 버튼 활성화
		GetDlgItem(IDC_BUTTON_UP)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow(TRUE);
	}
}

void CReserveDlg::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	switch(nIDEvent)
	{
		case TIMER_CHECK_START:
			CheckReservedWork();	//ksj 20160603 예약 작업 체크
		break;
		case TIMER_DELAYED_STARTBEFDOWN:
			DelayedStart(1);		//일정 지연시간 후 작업 시작.
		break;
		case TIMER_DELAYED_START:
			DelayedStart();		//일정 지연시간 후 작업 시작.
		break;
		case TIMER_CHECK_SBCDATADOWN:
			//CheckSBCDataDown();		//일정 지연시간 후 작업 시작. //ksj 20200707 : 주석처리. 이것때문에 작업예약기능이 안된다. 정확히 원인 파악하기전에 일단 제거
			break;
	}
	CDialog::OnTimer(nIDEvent);
}

BOOL CReserveDlg::CheckResvSbcStop(CCyclerChannel *pChannel)
{
	if(!pChannel) return FALSE;
	if(m_bWorking==FALSE) return FALSE;

	int iCH=pChannel->m_iChannelNo;

	CString strTemp;
	strTemp.Format(_T("resv state CH:%d working:%d"),iCH,m_bWorking);
	log_All(_T("resv"),_T("sbc"),strTemp);

	OnWorkStartStop();

	strTemp.Format(_T("resv stop CH:%d working:%d"),iCH,m_bWorking);
	log_All(_T("resv"),_T("sbc"),strTemp);

	int  nChIndex = pChannel->GetChannelIndex();
	int  nModuleID = pChannel->GetModuleID();
	char szBuff[256]={0,};
	ltoa(MAKELONG(nChIndex, nModuleID),szBuff,10);
	CString strCH = szBuff;
	strCH += _T(";");
	
	int nItem=GList::list_GetSameText(&m_listReserveQueue,strCH,LIST_INDEX_CHANNEL);
	if(nItem<0) return TRUE;
	
	CWorkWarnin WorkWarningDlg;
	CString strMsg;
	strMsg.Format(_T("CH:%d Reserved work remained. Will you stop the reserved works ?"),pChannel->m_iChannelNo);
	WorkWarningDlg.Fun_SetMessage(strMsg);

	if (WorkWarningDlg.DoModal() == IDOK)
	{//Pause
		strTemp.Format(_T("resv stop CH:%d %s"),iCH,strCH);
		log_All(_T("resv"),_T("sbc"),strTemp);

		ChangeState(0, RESERV_STATE_PAUSED, strCH);
		RequeryReservList();
	}
	OnWorkStartStop();
	
	strTemp.Format(_T("resv start CH:%d working:%d"),iCH,m_bWorking);
	log_All(_T("resv"),_T("sbc"),strTemp);
	return TRUE;
}

BOOL CReserveDlg::SetChResvStopBySbcStoporPause()//yulee 20190503
{
	CCyclerModule* pMD;
	CCyclerChannel* pChannel;
	int nModuleID = 1, iCH = -1;
	CString strTemp;

	pMD = m_pDoc->GetModuleInfo(nModuleID);//모듈 1개만 지원
	if(!pMD) return FALSE;
	for(int i = 0;i<pMD->GetTotalChannel();i++)
	{
		pChannel = m_pDoc->GetChannelInfo(nModuleID, i);
		if(!pChannel) return FALSE;
		if(m_bWorking==FALSE) return FALSE;
		iCH=pChannel->GetChannelIndex()+1;

		if((pChannel->GetState() == PS_STATE_STOP || pChannel->GetState() == PS_STATE_PAUSE))
		{
			strTemp.Format(_T("resv state ich:%d CH:%d state:%d"),i, iCH,pChannel->GetState());
			log_All(_T("resv"),_T("sbc"),strTemp);


			switch(iCH)
			{
			case 1:
				{
					m_bRESRV_Ch01_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
					AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH1_ONOFF", RESERV_OFF_BY_CH);
					break;
				}
			case 2:
				{
					m_bRESRV_Ch02_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
					AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH2_ONOFF", RESERV_OFF_BY_CH);
					break;
				}
			case 3:
				{
					m_bRESRV_Ch03_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
					AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH3_ONOFF", RESERV_OFF_BY_CH);

					break;
				}
			case 4:
				{
					m_bRESRV_Ch04_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
					AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH4_ONOFF", RESERV_OFF_BY_CH);
					break;
				}
			default:
				break;
			}
		}

		strTemp.Format(_T("resv stop i:%d, CH:%d reserving work:%d"),i ,iCH ,m_bRESRV_Ch01_ON_OFF.GetCheck());
		log_All(_T("resv"),_T("sbc"),strTemp);
	}
	UpdateData(TRUE);
	return TRUE;
}

BOOL CReserveDlg::CheckSBCDataDown()
{
	LPS_WORK_RESERVATION_DATA pTopPriorityWork = NULL;
	pTopPriorityWork = m_pTopPriorityWork;

	if((m_pDoc->m_bWorkingFtpDown == FALSE) && (m_pDoc->m_bSucessFtpDownAllSBCData == TRUE))
	{
		KillTimer(TIMER_CHECK_SBCDATADOWN);
		SetTimer(TIMER_DELAYED_START,1000, NULL);
		return TRUE;
	}
	else if((m_pDoc->m_bWorkingFtpDown == TRUE) && (m_pDoc->m_bSucessFtpDownAllSBCData == FALSE))
	{
		Sleep(1000);
		return TRUE;
	}
	else if((m_pDoc->m_bWorkingFtpDown == FALSE) && (m_pDoc->m_bSucessFtpDownAllSBCData == FALSE))
	{
		KillTimer(TIMER_CHECK_SBCDATADOWN);
		Fun_DataDownFailProcess();
		return FALSE;
	}
	return FALSE;
}

BOOL CReserveDlg::Fun_DataDownFailProcess()
{
	KillTimer(TIMER_CHECK_SBCDATADOWN);
	OnWorkStartStop();
	m_StatusBar.SetPaneText(1,"SBC Data Download fail, Reserving tasks stop.");
	return TRUE;;
}

BOOL CReserveDlg::DelayedStart(int nType) //yulee 20190420_1 nType 추가
{
	CString strTmp;
	int     nChamStratOption;

	if(m_nDelayedStartSec >= m_nDelayedStartWaitTime) 
	{		
		KillTimer(TIMER_DELAYED_START);
		KillTimer(TIMER_DELAYED_STARTBEFDOWN);//yulee 20190420_1
		//예약된 작업중 최우선순위 작업 실행.

		//대기중인 우선 순위 1번 예약작업 검색 //n초 후에 작업을 실행하므로 대기중에 사용자가 우선순위를 바꿀 수 있다. 다시 검색한다.
		LPS_WORK_RESERVATION_DATA pWork = NULL;
		LPS_WORK_RESERVATION_DATA pTopPriorityWork = NULL;
		
		/*int i;
		for(i=0;i<m_ptrReservData.GetSize();i++)
		{
			pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(i);
			
			if(pWork->nState == RESERV_STATE_WAITING) //예약 대기중인 첫번째 작업 찾기.
			{
				//m_pTopPriorityWork = pWork;			
				pTopPriorityWork = pWork;
				m_nID = pWork->nIndex; //20180427 yulee add
				nChamStratOption = pWork->nStartOptChamber;
				break;
			}
		}*/

		m_nID = m_pTopPriorityWork->nIndex; //20180427 yulee add
		nChamStratOption = m_pTopPriorityWork->nStartOptChamber;
		pTopPriorityWork = m_pTopPriorityWork;

		if(pTopPriorityWork)
		{
			CDWordArray adwChArray;
			//ConvertAdwChArrayFromString(pTopPriorityWork->strAdwChArray,&adwChArray); //채널 DB string값을 array로 변환
			ConvertAdwChArrayFromString(pTopPriorityWork->szAdwChArray,&adwChArray); //채널 DB string값을 array로 변환	// 200313 HKH Memory Leak 수정

			if(CheckCanBeStart(&adwChArray,pTopPriorityWork->nStartOptChamber)) //채널이 사용가능한지 확인.
			{
				int nModuleID = HIWORD(adwChArray.GetAt(0));
				int nChannelIndex = LOWORD(adwChArray.GetAt(0));

				BOOL	bDownAllRawDataBefRun = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "DownAllRawDataBefRun", TRUE); //yulee 20190420
	
				if((bDownAllRawDataBefRun)&&(nType == 1))
				{
					int nModuleID, nChannelIndex;
					for(int i=0;i<adwChArray.GetSize();i++)
					{
						nModuleID = HIWORD(adwChArray.GetAt(i));
						nChannelIndex = LOWORD(adwChArray.GetAt(i));

						//ksj 20160728 이전 작업의 SBC StepEndData를 다운로드 한다.
						m_pDoc->m_nFtpDownStepEndModuleID = nModuleID;
						m_pDoc->m_nFtpDownStepEndChannelIndex = nChannelIndex;
						//yulee 20190420 이전 작업의 SBC Raw Data를 다운로드 한다.
						//pDoc->m_bRunFtpDownStepEnd = TRUE;
						m_pDoc->m_bSucessFtpDownAllSBCData = FALSE;
						m_pDoc->m_FTPFileDownRetryCnt = 0;
						m_pDoc->m_bRunFtpDownAllSBCData = TRUE;

						Sleep(1000);
						SetTimer(TIMER_CHECK_SBCDATADOWN,1000, NULL);
						return TRUE;
					}
				}

				//예약된 작업의 채널들이 준비되어있음. 작업시작가능.			
				if(m_pParent->RunProcess(WORK_START_RESERV_RUN, pTopPriorityWork, m_nID) == RUN_OK)
				{
					//작업 실행 후 성공시
					pTopPriorityWork->nState = RESERV_STATE_WORKING; //상태 전환.
					if(ChangeState(pTopPriorityWork->nIndex,pTopPriorityWork->nState))
					{	
						RequeryReservList(); //리스트 갱신
						SetTimer(TIMER_CHECK_START,TIMER_CHECK_START_ELAPSE,NULL); //다음 작업 실행 감시 타이머 시작.	
						
						//yulee 20190420_2
						CWinApp* pApp = AfxGetApp();
						
 						//int nChannelIndex;
 						//nChannelIndex = LOWORD(adwChArray.GetAt(0));
 						//nChannelIndex = nChannelIndex%4+1;
 						//pApp->WriteProfileInt(RESERVATION_REGISTRY_KEY, "ReserveSearchCh", nChannelIndex);
						m_SearchCh = (m_SearchCh%4)+1;//yulee 20190420_2
						
						m_nDelayedStartSec = 0;
						m_StatusBar.SetPaneText(1,"Work will start soon.");
						
						m_nWorkCount++;
						m_nRunFailCnt = 0;
						
						return TRUE;
					}
				}
				else
				{
					m_nRunFailCnt++;				
				}
				/*
				//예약된 작업의 채널들이 준비되어있음. 작업시작가능.			
				if(m_pParent->RunProcess(WORK_START_RESERV_RUN, pTopPriorityWork, m_nID) == RUN_OK)
				{
					//작업 실행 후 성공시
					pTopPriorityWork->nState = RESERV_STATE_WORKING; //상태 전환.
					if(ChangeState(pTopPriorityWork->nIndex,pTopPriorityWork->nState))
					{
						
						RequeryReservList(); //리스트 갱신
						SetTimer(TIMER_CHECK_START,TIMER_CHECK_START_ELAPSE,NULL); //다음 작업 실행 감시 타이머 시작.	
						
						//yulee 20190420_2
						CWinApp* pApp = AfxGetApp();

						int nChannelIndex;
						nChannelIndex = LOWORD(adwChArray.GetAt(0));
						nChannelIndex = nChannelIndex%4+1;
						pApp->WriteProfileInt(RESERVATION_REGISTRY_KEY, "ReserveSearchCh", nChannelIndex);
						

						m_nDelayedStartSec = 0;
						m_StatusBar.SetPaneText(1,"작업을 시작합니다.");
					
						m_nWorkCount++;
						m_nRunFailCnt = 0;

						return TRUE;
					}
				}
				else
				{
					m_nRunFailCnt++;				
				}
				*/
			}
			else//yulee 20190420_2
			{
				SetTimer(TIMER_CHECK_START,TIMER_CHECK_START_ELAPSE,NULL); //다음 작업 실행 감시 타이머 시작.	
				
				//yulee 20190420_2
				CWinApp* pApp = AfxGetApp();
				
// 				int nChannelIndex;
// 				nChannelIndex = LOWORD(adwChArray.GetAt(0));
// 				nChannelIndex = nChannelIndex%4+1;
// 				pApp->WriteProfileInt(RESERVATION_REGISTRY_KEY, "ReserveSearchCh", nChannelIndex);
				m_SearchCh = (m_SearchCh%4)+1;//yulee 20190420_2
				
				
				m_nDelayedStartSec = 0;
						m_StatusBar.SetPaneText(1,"다른 채널을 예약 시작을 확인합니다.");
			}
			
			//작업 실패시			
			strTmp.Format("예약 실행에 실패하였습니다.(%d)",m_nRunFailCnt);
			m_StatusBar.SetPaneText(1,strTmp);

			if(m_nRunFailCnt >=3) //3번의 작업 실행 실패시
			{				
				pTopPriorityWork->nState = RESERV_STATE_FAILED; //상태 전환.
				if(ChangeState(pTopPriorityWork->nIndex,pTopPriorityWork->nState))
				{	
					m_nWorkCount++;
					m_nRunFailCnt = 0;
				}
			}

			RequeryReservList(); //리스트 갱신	
			if(m_bWorking)
				SetTimer(TIMER_CHECK_START,TIMER_CHECK_START_ELAPSE,NULL); //다음 작업 실행 감시 타이머 시작.		
			m_nDelayedStartSec = 0;

			return FALSE;		
		}	
	}
	
	int nCntDnSec = m_nDelayedStartWaitTime-m_nDelayedStartSec; 
	if(nCntDnSec < 60)
		strTmp.Format("Waiting %d Sec. for next work.", nCntDnSec);
	else
	{
		strTmp.Format("In about %d min. next work will start.", nCntDnSec/60+1);
	}
	m_StatusBar.SetPaneText(1,strTmp);

	m_nDelayedStartSec++;

	return FALSE;
}

//완료된 작업 삭제.
int CReserveDlg::RemoveCompleteWork()
{	
	CString strSQL;	
	
	strSQL.Format("DELETE FROM Work_Reservation WHERE STATE<>%d", RESERV_STATE_WAITING);
	return ExecuteSQL(strSQL);
}

void CReserveDlg::OnBegindragListRunQueue(NMHDR* pNMHDR, LRESULT* pResult) 
{
	//NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	m_bDraging = TRUE;
	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	m_nDragItemIndex = m_listReserveQueue.GetNextSelectedItem(pos);

	SetCapture();

	GetCursorPos(&m_ptDragFirstPoint);		
	ScreenToClient(&m_ptDragFirstPoint);

	*pResult = 0;
}

void CReserveDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_bDraging)
	{
		//아래로 내리기
		if(m_ptDragFirstPoint.y - point.y < -10)
		{
			OnButtonDown();
			GetCursorPos(&m_ptDragFirstPoint);		
			ScreenToClient(&m_ptDragFirstPoint);
		}

		//위로 올리기
		if(m_ptDragFirstPoint.y - point.y > 10)
		{
			OnButtonUp();
			GetCursorPos(&m_ptDragFirstPoint);		
			ScreenToClient(&m_ptDragFirstPoint);
		}

		//AfxMessageBox(strTmp);

	}
	
	CDialog::OnMouseMove(nFlags, point);
}
void CReserveDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	//m_bDraging = FALSE;

	CDialog::OnLButtonDown(nFlags, point);
}

void CReserveDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(m_bDraging)
	{
		ReleaseCapture();
		m_bDraging = FALSE;
		
	}

	CDialog::OnLButtonUp(nFlags, point);
}


//선택 작업의 우선순위 올리기.
void CReserveDlg::OnButtonUp() 
{
	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	int nItem;
	CString strTmp;
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	LPS_WORK_RESERVATION_DATA pSwapWork = NULL;
	int nPriority;

	while(pos)
	{
		nItem = m_listReserveQueue.GetNextSelectedItem(pos);

		if(nItem == 0)
			return;

		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem);
		pSwapWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem-1);

		if(pSwapWork->nState != RESERV_STATE_WAITING || pWork->nState != RESERV_STATE_WAITING)
			return;

		nPriority = pWork->nPriority;
		ChangePriority(pWork, pSwapWork->nPriority);
		ChangePriority(pSwapWork, nPriority);

		SetListItem(nItem-1,pWork,FALSE);
		SetListItem(nItem,pSwapWork,FALSE);

		m_ptrReservData.SetAtGrow(nItem-1, pWork);
		m_ptrReservData.SetAtGrow(nItem, pSwapWork);
		m_listReserveQueue.SetItemState((nItem-1),LVIF_STATE|LVIS_SELECTED|LVIR_BOUNDS|LVIF_PARAM,LVIS_SELECTED);
		m_listReserveQueue.SetItemState(nItem,(UINT)(LVIF_STATE|~LVIS_SELECTED|LVIR_BOUNDS|LVIF_PARAM),(UINT)LVIS_SELECTED);                                    
	}
			
	
}

//우선순위 낮추기 
void CReserveDlg::OnButtonDown() 
{
	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	int nItem;
	CString strTmp;
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	LPS_WORK_RESERVATION_DATA pSwapWork = NULL;
	int nPriority;
	int anSelItems[MAX_RESERV_QUEUE];
	int nSelCount = 0;
	int i;
	
	//역순으로 처리하기 위해 선택된 아이템 번호를 우선 저장.
	while(pos)
	{
		nItem = m_listReserveQueue.GetNextSelectedItem(pos);
		anSelItems[nSelCount++] = nItem;
	}

	//리스트의 역순으로 처리
	for(i = nSelCount-1; 0 <= i ; i--)
	{
		nItem = anSelItems[i];
		if(nItem == m_listReserveQueue.GetItemCount()-1)
			return;		
		
		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem);
		pSwapWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem+1);

		if(pSwapWork->nState != RESERV_STATE_WAITING || pWork->nState != RESERV_STATE_WAITING)
			return;
		
		nPriority = pWork->nPriority;
		ChangePriority(pWork, pSwapWork->nPriority);
		ChangePriority(pSwapWork, nPriority);
		
		SetListItem(nItem+1,pWork,FALSE);
		SetListItem(nItem,pSwapWork,FALSE);
		
		m_ptrReservData.SetAtGrow(nItem+1, pWork);
		m_ptrReservData.SetAtGrow(nItem, pSwapWork);
		m_listReserveQueue.SetItemState(nItem+1,LVIF_STATE|LVIS_SELECTED|LVIR_BOUNDS|LVIF_PARAM,LVIS_SELECTED);                                    
		m_listReserveQueue.SetItemState(nItem,(UINT)(LVIF_STATE|~LVIS_SELECTED|LVIR_BOUNDS|LVIF_PARAM),(UINT)LVIS_SELECTED);  
	}
	
    
}

//작업의 우선순위를 바꾼다.
BOOL CReserveDlg::ChangePriority(LPS_WORK_RESERVATION_DATA pWork, int nPriority)
{
	ASSERT(pWork);
	CString strSQL;	

	strSQL.Format("UPDATE Work_Reservation SET PRIORITY=%d WHERE ID=%d", nPriority, pWork->nIndex);
	
	if(ExecuteSQL(strSQL))
	{
		pWork->nPriority = nPriority;
		return TRUE;
	}

	return FALSE;
}

BOOL CReserveDlg::ExecuteSQL(CString strSQL)
{
	CDaoDatabase  db;
	
	try
	{
		db.Open(m_strDatabaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}		
	
	
	try
	{
		db.Execute(strSQL);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	
	
	db.Close();
	
	
	return TRUE;
}

BOOL CReserveDlg::SetListItem(int nI, LPS_WORK_RESERVATION_DATA pReservData, BOOL bOpt)
{
	LVITEM lvItem;
	TCHAR szBuff[256] = {0,};
	CString strTmp;
	
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;

	lvItem.iItem = nI;
	lvItem.iSubItem = LIST_INDEX_PRIORITY;

	if(bOpt) //신규 등록
	{
		sprintf(szBuff,"%d", pReservData->nPriority+1);
		lvItem.pszText = szBuff; 		
		m_listReserveQueue.InsertItem(&lvItem);
	}
	else //수정
	{		
		sprintf(szBuff,"%d", pReservData->nPriority+1);
		lvItem.pszText = szBuff; 		
		m_listReserveQueue.SetItem(&lvItem);
	}	
	
	lvItem.iSubItem = LIST_INDEX_STATE;
	sprintf(szBuff,"%s", ConvertStateCodeToString(pReservData->nState));
	lvItem.pszText = szBuff; 		
	m_listReserveQueue.SetItem(&lvItem);
	
	// 200313 HKH Memory Leak 수정
	CDWordArray adwChArray;
	lvItem.iSubItem = LIST_INDEX_CHANNEL;
	//if(ConvertAdwChArrayFromString(pReservData->strAdwChArray, &adwChArray))
	if(ConvertAdwChArrayFromString(pReservData->szAdwChArray, &adwChArray))	
	{
		sprintf(szBuff,"%s", ConvertAdwChArrayToString(&adwChArray));
		lvItem.pszText = szBuff; 
		m_listReserveQueue.SetItem(&lvItem);
	}			
	
	lvItem.iSubItem = LIST_INDEX_MODEL_NAME;
	//sprintf(szBuff,"%s", pReservData->strModelName);
	sprintf(szBuff,"%s", pReservData->szModelName);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_INDEX_SCH_NAME;
	//sprintf(szBuff,"%s", pReservData->strSchName);
	sprintf(szBuff,"%s", pReservData->szSchName);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_INDEX_WORK_NAME;
	//sprintf(szBuff,"%s", pReservData->strTestName);
	sprintf(szBuff,"%s", pReservData->szTestName);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_INDEX_WORKER_NAME;
	//sprintf(szBuff,"%s", pReservData->strWorkerName);
	sprintf(szBuff,"%s", pReservData->szWorkerName);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_INDEX_RESERVED_TIME;
	//sprintf(szBuff,"%s", pReservData->strReservedTime);
	sprintf(szBuff,"%s", pReservData->szReservedTime);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);
	
	lvItem.iSubItem = LIST_INDEX_START_TIME;
	//sprintf(szBuff,"%s", pReservData->strStartTime);
	sprintf(szBuff,"%s", pReservData->szStartTime);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);

	lvItem.iSubItem = LIST_INDEX_LOT;
	//sprintf(szBuff,"%s", pReservData->strLot);
	sprintf(szBuff,"%s", pReservData->szLot);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);

	lvItem.iSubItem = LIST_INDEX_COMMENT;
	//sprintf(szBuff,"%s", pReservData->strComment);
	sprintf(szBuff,"%s", pReservData->szComment);
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);

	lvItem.iSubItem = LIST_INDEX_CHAMBER;
	switch(pReservData->nStartOptChamber)
	{	
	case 1:
		strTmp.Format("Fix(%.0f℃)",pReservData->fChamberFixTemp);
		sprintf(szBuff,"%s",strTmp);
		break;
	case 2:
		strTmp.Format("Pattern(%d)",pReservData->uiChamberPatternNum);
		sprintf(szBuff,"%s",strTmp);
		break;
	case 3:
		if(pReservData->bIsChamberContinue)
			//sprintf(szBuff,"Step 대기");
			//sprintf(szBuff,"Step Wait");
			sprintf(szBuff,Fun_FindMsg("SetListItem_msg1","IDD_WORK_RESERVE_DLG"));
		else
			//sprintf(szBuff,"Step");
			sprintf(szBuff,Fun_FindMsg("SetListItem_msg2","IDD_WORK_RESERVE_DLG"));
		break;
	case 5: //yulee 20180910
		if(pReservData->bIsChamberContinue)
			//sprintf(szBuff,"챔버 정지");//$1013
			sprintf(szBuff,Fun_FindMsg("ReserveDlg_SetListItem_msg1","IDD_WORK_RESERVE_DLG"));//$1013//&&
		else
			sprintf(szBuff,"Step");
		break;
	default:	
		sprintf(szBuff,"");
		break;
	}

	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);

	//yulee 20181025
	lvItem.iSubItem = LIST_INDEX_MUX;
	switch(pReservData->uiMuxOption)
	{
	case 0:
		{
			sprintf(szBuff,"ALL OFF");
			break;
		}
	case 1:
		{
			sprintf(szBuff,"MUX A");
			break;
		}
	case 2:
		{
			sprintf(szBuff,"MUX B");
			break;
		}
	default:
		{
			sprintf(szBuff,"-");
			break;
		}
	}
	lvItem.pszText = szBuff; 
	m_listReserveQueue.SetItem(&lvItem);


	return TRUE;
}

void CReserveDlg::OnButtonReserveDel() 
{
//	if(MessageBox("선택한 작업을 예약 취소하시겠습니까?","예약 취소",MB_YESNO|MB_ICONWARNING) == IDNO)
	//if(MessageBox("Are you sure you want to cancel the selected task?","cancel task",MB_YESNO|MB_ICONWARNING) == IDNO)
	if(MessageBox(Fun_FindMsg("OnButtonReserveDel_msg1","IDD_WORK_RESERVE_DLG"),Fun_FindMsg("OnButtonReserveDel_msg2","IDD_WORK_RESERVE_DLG"),MB_YESNO|MB_ICONWARNING) == IDNO)
		return;

	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	int nItem;
	CString strTmp;
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	
	
	while(pos)
	{
		pWork = NULL;
		nItem = m_listReserveQueue.GetNextSelectedItem(pos);			
		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem);
		
		if(pWork != NULL)
		{
			strTmp.Format("DELETE * FROM Work_Reservation WHERE ID = %d",pWork->nIndex);
			ExecuteSQL(strTmp);
		}
	}
	RequeryReservList();
}


void CReserveDlg::OnButtonReserveRestart() 
{
	if(MessageBox("Will you restart the works?","Cancel",MB_YESNO|MB_ICONWARNING) == IDNO)
	{
		return;
	}
	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	int nItem;
	CString strTmp;
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	
	while(pos)
	{
		pWork = NULL;
		nItem = m_listReserveQueue.GetNextSelectedItem(pos);			
		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem);
		
		if(pWork != NULL)
		{
			if(pWork->nState==RESERV_STATE_PAUSED)
			{
				strTmp.Format("UPDATE Work_Reservation SET STATE='0' WHERE ID = %d",pWork->nIndex);
				ExecuteSQL(strTmp);
			}
		}
	}
	RequeryReservList();
}

void CReserveDlg::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default


	CDialog::OnRButtonDown(nFlags, point);
}

void CReserveDlg::OnRclickListRunQueue(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();

	if(!pos) return;
	
	CPoint point;
	::GetCursorPos(&point);

	/*if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		GetClientRect(rect);
		ClientToScreen(rect);
		6
		point = rect.TopLeft();
		point.Offset(5, 5);
	}*/
	
	CMenu menu;
	//VERIFY(menu.LoadMenu(IDR_RESERV_POP_MENU));
	VERIFY(menu.LoadMenu(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDR_RESERV_POP_MENU):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDR_RESERV_POP_MENU_ENG):
	IDR_RESERV_POP_MENU));
	
	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	CWnd* pWndPopupOwner = this;
	
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();
	
	
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
	
	*pResult = 0;
}

void CReserveDlg::OnWorkLogView() 
{

	CString strData;
	CString strTemp;
	
	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	int nItem;
	while(pos)
	{
		nItem = m_listReserveQueue.GetNextSelectedItem(pos);
		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem);
		
		CString strContent;
		BOOL bContinue;
		CFileFind fileFind;
		CString strTmp;			
		
		//strTmp.Format("%s\\%s\\*.*",pWork->strDataPath,pWork->strTestName);
		strTmp.Format("%s\\%s\\*.*",pWork->szDataPath,pWork->szTestName);		// 200313 HKH Memory Leak 수정
		bContinue = fileFind.FindFile(strTmp);
		while(bContinue)
		{
			bContinue = fileFind.FindNextFile();
			
			if(fileFind.IsDirectory())
			{
				strTmp = fileFind.GetFileName();			
				if(!strTmp.Left(1).Compare("M")) //폴더명의 첫글자가 M인 폴더만 모두 열기
				{
					//strContent.Format("%s\\%s\\%s\\%s.log",pWork->strDataPath,pWork->strTestName, strTmp,pWork->strTestName);	
					strContent.Format("%s\\%s\\%s\\%s.log",pWork->szDataPath,pWork->szTestName, strTmp,pWork->szTestName);	// 200313 HKH Memory Leak 수정
					if(m_pDoc->ExecuteProgram("notepad.exe", strContent, "", "", TRUE, FALSE) == FALSE)
					{
						//AfxMessageBox(strContent +"를 찾을 수 없습니다.");
						//AfxMessageBox(strContent +" not found");
						AfxMessageBox(strContent + Fun_FindMsg("OnWorkLogView_msg1","IDD_WORK_RESERVE_DLG"));
					}

				}				
			}
		}			
		
	}	
	
}

void CReserveDlg::OnCheckCond() 
{	
	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	
	if(!pos)
	{
		return;
	}
	
	LPS_WORK_RESERVATION_DATA pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(m_listReserveQueue.GetNextSelectedItem(pos));
	ASSERT(pWork);
	CString strSchFileName;

	//strSchFileName = pWork->strSchPath;	
	strSchFileName.Format(_T("%s"), pWork->szSchPath);		// 200313 HKH Memory Leak 수정
	if(strSchFileName.IsEmpty())	return;
	CScheduleData schData;
	if(schData.SetSchedule(strSchFileName)== FALSE)
	{
		//AfxMessageBox("스케줄 정보를 읽을 수 없습니다.");
		//AfxMessageBox("Schedule information can not be read.");
		AfxMessageBox(Fun_FindMsg("OnCheckCond_msg1","IDD_WORK_RESERVE_DLG"));
		return;
	}
	schData.ShowContent();

}

void CReserveDlg::OnDataView() 
{
	RunAnalyzer(3);	
}

void CReserveDlg::OnGraphView() 
{
	RunAnalyzer(4);
}

void CReserveDlg::RunAnalyzer(int nType)
{
	CString strData;
	CString strTemp;
	
	POSITION pos = m_listReserveQueue.GetFirstSelectedItemPosition();
	LPS_WORK_RESERVATION_DATA pWork = NULL;
	int nItem;
	while(pos)
	{
		nItem = m_listReserveQueue.GetNextSelectedItem(pos);
		pWork = (LPS_WORK_RESERVATION_DATA) m_ptrReservData.GetAt(nItem);
		
		CString strContent;
		BOOL bContinue;
		CFileFind fileFind;
		CString strTmp;
		
		
		//strTmp.Format("%s\\%s\\*.*",pWork->strDataPath,pWork->strTestName);	
		strTmp.Format("%s\\%s\\*.*",pWork->szDataPath,pWork->szTestName);	// 200313 HKH Memory Leak 수정
		bContinue = fileFind.FindFile(strTmp);
		while(bContinue)
		{
			bContinue = fileFind.FindNextFile();
			
			if(fileFind.IsDirectory())
			{
				strTmp = fileFind.GetFileName();			
				if(!strTmp.Left(1).Compare("M")) //폴더명의 첫글자가 M인 폴더만 모두 열기
				{
					//strContent.Format("%s\\%s\\%s",pWork->strDataPath,pWork->strTestName, strTmp);
					strContent.Format("%s\\%s\\%s",pWork->szDataPath,pWork->szTestName, strTmp);	// 200313 HKH Memory Leak 수정
					strData = strData + strContent + "\n";
				}				
			}
		}			
		
	}		
	
	if(strData.IsEmpty())
	{
		//AfxMessageBox("확인할 수 있는 Data가 없습니다.");
		//AfxMessageBox("No data available to verify.");
		AfxMessageBox(Fun_FindMsg("RunAnalyzer_msg1","IDD_WORK_RESERVE_DLG"));
		return;
	}
	
	
	CString strProgramName;
	strProgramName.Format("%s\\CTSAnalyzerPro.exe", m_pDoc->m_strCurPath);
	if(m_pDoc->ExecuteProgram(strProgramName, "", PS_DATA_ANAL_PRO_CLASS_NAME, "", FALSE, TRUE))
	{	
		//Message를 전송한다.
		Sleep(300); //Analyzer가 실행되어 메세지 받을 준비가 되기를 잠깐 기다린다.
		CWnd *pWnd = FindWindow(PS_DATA_ANAL_PRO_CLASS_NAME, NULL);
		if(pWnd)
		{
			int nSize = 0;
			if(strData.IsEmpty() == FALSE) //yulee 20190707
			{
				nSize = strData.GetLength()+1;
			}
			if(nSize > 2000) nSize = 2000; //너무 과도한 사이즈는 자른다.
			char *pData = new char[nSize];
			//sprintf(pData, "%s", strData);
			memcpy(pData,strData.GetBuffer(0),nSize);
			pData[nSize-1] = '\0';
			
			COPYDATASTRUCT CpStructData;
			CpStructData.dwData = nType;		//CTSMonPro Program 구별 Index
			CpStructData.cbData = nSize;
			CpStructData.lpData = pData;
			
	//		TRACE("Send File %s\n", pData);
			
			::SendMessage(pWnd->m_hWnd, WM_COPYDATA, 0, (LPARAM)&CpStructData);
			
			delete [] pData;
		}	
	}
}

void CReserveDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	/*switch(m_nMode)
	{
		case RESERVATION_DLG_MODE_QUICK_ADD:
			OnButtonReserveAdd();
			m_nMode = RESERVATION_DLG_MODE_NORMAL;
		break;
	}*/
	
}

void CReserveDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);


	CRect rect, rect2;
	CRect windowRect;
	int nWidth, nHeight;

	GetClientRect(&windowRect);

	if(m_StatusBar.GetSafeHwnd())
	{
		//m_StatusBar.GetWindowRect(&rect);
		//ScreenToClient(&rect);
		rect.left = windowRect.left;
		rect.right = windowRect.right;		
		rect.bottom = windowRect.bottom;
		rect.top = rect.bottom - 20;		

		m_StatusBar.SetPaneInfo(0, ID_RESERV_DLG_INDICATOR1, SBPS_NORMAL, 200);
		m_StatusBar.SetPaneInfo(1, ID_RESERV_DLG_INDICATOR2, SBPS_NORMAL, windowRect.Width()/3*2-200);
//		m_StatusBar.SetPaneInfo(2, ID_RESERV_DLG_INDICATOR3, SBPS_NORMAL, windowRect.Width()/3);

		m_StatusBar.MoveWindow(&rect);
	}

	
	if(m_listReserveQueue.GetSafeHwnd())
	{
		m_listReserveQueue.GetWindowRect(&rect);
		ScreenToClient(&rect);
		rect.left = windowRect.left + 10;
		rect.right = windowRect.right - 10;
		rect.top = windowRect.top + 10;
		rect.bottom = windowRect.bottom - 70;

		m_listReserveQueue.MoveWindow(&rect);
	}

	if(m_ctrlProgress.GetSafeHwnd())
	{
		m_ctrlProgress.GetWindowRect(&rect);
		ScreenToClient(&rect);
		//nHeight = rect.Height();
		rect.left = windowRect.right/3*2 + 17;
		rect.right = windowRect.right - 20;		
		rect.bottom = windowRect.bottom;
		rect.top = rect.bottom - 18;
		
		m_ctrlProgress.MoveWindow(&rect);
	}

	if(GetDlgItem(IDOK)->GetSafeHwnd())
	{
		GetDlgItem(IDOK)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		nWidth = rect.Width();
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		rect.right = windowRect.right - 30;
		rect.left = rect.right - nWidth;		
		GetDlgItem(IDOK)->MoveWindow(&rect);
	}

	if(GetDlgItem(IDC_BUTTON_DOWN)->GetSafeHwnd())
	{
		GetDlgItem(IDC_BUTTON_DOWN)->GetWindowRect(&rect);		
		GetDlgItem(IDOK)->GetWindowRect(&rect2);
		ScreenToClient(&rect);
		ScreenToClient(&rect2);
		nHeight = rect.Height();		
		nWidth = rect.Width();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		rect.right = rect2.left - 10;
		rect.left = rect.right - nWidth;
		
		GetDlgItem(IDC_BUTTON_DOWN)->MoveWindow(&rect);				
	}
	
	if(GetDlgItem(IDC_BUTTON_UP)->GetSafeHwnd())
	{
		GetDlgItem(IDC_BUTTON_UP)->GetWindowRect(&rect);		
		GetDlgItem(IDC_BUTTON_DOWN)->GetWindowRect(&rect2);
		ScreenToClient(&rect);
		ScreenToClient(&rect2);
		nHeight = rect.Height();		
		nWidth = rect.Width();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		rect.right = rect2.left - 5;
		rect.left = rect.right - nWidth;
		
		GetDlgItem(IDC_BUTTON_UP)->MoveWindow(&rect);			
	}

	if(GetDlgItem(IDC_BUTTON_RESERVE_RESTART)->GetSafeHwnd())
	{
		GetDlgItem(IDC_BUTTON_RESERVE_RESTART)->GetWindowRect(&rect);		
		GetDlgItem(IDC_BUTTON_UP)->GetWindowRect(&rect2);
		ScreenToClient(&rect);
		ScreenToClient(&rect2);
		nHeight = rect.Height();		
		nWidth = rect.Width();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		rect.right = rect2.left - 5;
		rect.left = rect.right - nWidth;
		
		GetDlgItem(IDC_BUTTON_RESERVE_RESTART)->MoveWindow(&rect);
	}
	if(GetDlgItem(IDC_BUTTON_RESERVE_DEL)->GetSafeHwnd())
	{
		GetDlgItem(IDC_BUTTON_RESERVE_DEL)->GetWindowRect(&rect);		
		GetDlgItem(IDC_BUTTON_RESERVE_RESTART)->GetWindowRect(&rect2);		
		ScreenToClient(&rect);
		ScreenToClient(&rect2);
		nHeight = rect.Height();	
		nWidth = rect.Width();
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		rect.right = rect2.left - 10;
		rect.left = rect.right - nWidth;
		
		GetDlgItem(IDC_BUTTON_RESERVE_DEL)->MoveWindow(&rect);	
	}

	if(GetDlgItem(IDC_BUTTON_RESERVE_ADD)->GetSafeHwnd())
	{
		GetDlgItem(IDC_BUTTON_RESERVE_ADD)->GetWindowRect(&rect);
		GetDlgItem(IDC_BUTTON_RESERVE_DEL)->GetWindowRect(&rect2);
		ScreenToClient(&rect);
		ScreenToClient(&rect2);
		nHeight = rect.Height();		
		nWidth = rect.Width();
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		rect.right = rect2.left - 10;
		rect.left = rect.right - nWidth;

		GetDlgItem(IDC_BUTTON_RESERVE_ADD)->MoveWindow(&rect);	
	}


	if(GetDlgItem(IDC_BUTTON_TEST)->GetSafeHwnd())
	{
		GetDlgItem(IDC_BUTTON_TEST)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		
		GetDlgItem(IDC_BUTTON_TEST)->MoveWindow(&rect);		
	}

	if(GetDlgItem(IDC_WORK_START_STOP)->GetSafeHwnd())
	{
		GetDlgItem(IDC_WORK_START_STOP)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		
		GetDlgItem(IDC_WORK_START_STOP)->MoveWindow(&rect);		
	}

	if(GetDlgItem(IDC_RESERV_CH_SW_BOX)->GetSafeHwnd())
	{
		GetDlgItem(IDC_RESERV_CH_SW_BOX)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		rect.bottom = windowRect.bottom - 20;
		rect.top = rect.bottom - nHeight;
		
		GetDlgItem(IDC_RESERV_CH_SW_BOX)->MoveWindow(&rect);		
	}

	
	if(GetDlgItem(IDC_CHK_RESERV_CH1)->GetSafeHwnd())
	{
		GetDlgItem(IDC_CHK_RESERV_CH1)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		
		GetDlgItem(IDC_CHK_RESERV_CH1)->MoveWindow(&rect);		
	}

	if(GetDlgItem(IDC_CHK_RESERV_CH2)->GetSafeHwnd())
	{
		GetDlgItem(IDC_CHK_RESERV_CH2)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		
		GetDlgItem(IDC_CHK_RESERV_CH2)->MoveWindow(&rect);		
	}

	if(GetDlgItem(IDC_CHK_RESERV_CH3)->GetSafeHwnd())
	{
		GetDlgItem(IDC_CHK_RESERV_CH3)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		
		GetDlgItem(IDC_CHK_RESERV_CH3)->MoveWindow(&rect);		
	}

	if(GetDlgItem(IDC_CHK_RESERV_CH4)->GetSafeHwnd())
	{
		GetDlgItem(IDC_CHK_RESERV_CH4)->GetWindowRect(&rect);		
		ScreenToClient(&rect);
		nHeight = rect.Height();		
		rect.bottom = windowRect.bottom - 30;
		rect.top = rect.bottom - nHeight;
		
		GetDlgItem(IDC_CHK_RESERV_CH4)->MoveWindow(&rect);		
	}

	Invalidate();	


}

void CReserveDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	// TODO: Add your message handler code here and/or call default
	
	lpMMI->ptMinTrackSize.x = 630;
	lpMMI->ptMinTrackSize.y = 350;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CReserveDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	CRect rect;
	GetWindowRect(&rect);
	CWinApp* pApp = AfxGetApp();

	pApp->WriteProfileInt(RESERVATION_REGISTRY_KEY,"Window Width",rect.Width());
	pApp->WriteProfileInt(RESERVATION_REGISTRY_KEY,"Window Height",rect.Height());
	
	int i;
	int nColumnCnt = m_listReserveQueue.GetHeaderCtrl()->GetItemCount();
	int* pData = new int[nColumnCnt];

	ZeroMemory(pData,sizeof(int)*nColumnCnt);

	for(int i = 0;i<nColumnCnt;i++)
	{
		pData[i] = m_listReserveQueue.GetColumnWidth(i);
	}

	pApp->WriteProfileBinary(RESERVATION_REGISTRY_KEY,"Column Width",(LPBYTE)pData,sizeof(int)*nColumnCnt);

	delete pData;
}

void CReserveDlg::OnReservCancel() 
{
	// TODO: Add your command handler code here
	OnButtonReserveDel();
}

void CReserveDlg::OnClear() 
{
	//if(MessageBox("실행 완료된 모든 예약 작업을 목록에서 제거합니다.","확인",MB_YESNO|MB_ICONINFORMATION) == IDYES)
	//if(MessageBox("Removes all completed scheduled tasks from the list.","Check",MB_YESNO|MB_ICONINFORMATION) == IDYES)
	if(MessageBox(Fun_FindMsg("OnClear_msg1","IDD_WORK_RESERVE_DLG"),Fun_FindMsg("OnClear_msg2","IDD_WORK_RESERVE_DLG"),MB_YESNO|MB_ICONINFORMATION) == IDYES)
	{
		if(RemoveCompleteWork())
		{
			RequeryReservList();
			
		}		
	}	
}

void CReserveDlg::ReserveAdd()
{
	CString strTmp;
	if(m_listReserveQueue.GetItemCount() >= MAX_RESERV_QUEUE)
	{
	//	strTmp.Format("최대 예약 개수(%d) 초과",MAX_RESERV_QUEUE);
	//	strTmp.Format("Maximum number of reservations (%d)",MAX_RESERV_QUEUE);
		strTmp.Format(Fun_FindMsg("ReserveAdd_msg1","IDD_WORK_RESERVE_DLG"),MAX_RESERV_QUEUE);
		AfxMessageBox(strTmp);
		return;
	}
	if(m_pParent->RunProcess(WORK_START_RESERV) == RESERV_OK)
	{
		
	}
}

void CReserveDlg::OnBeginrdragListRunQueue(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	//TRACE("%d\n",pNMListView->iItem);
	*pResult = 0;
}

// void CReserveDlg::OnWorkStartTest() 
// {
// // 	int nModuleID=1;
// // 	int nChIndex=0;
// // 
// // 	char szBuff[256]={0,};
// // 	ltoa(MAKELONG(nChIndex, nModuleID),szBuff,10);
// // 	CString strCH = szBuff;
// // 	strCH += _T(";");
// // 
// // 	ChangeState(0, RESERV_STATE_PAUSED, strCH);
// // 	RequeryReservList();
// }

void CReserveDlg::OnRadReservCh1() //yulee 20190503
{
	// TODO: Add your control notification handler code here
	UpdateData(FALSE);
//	BOOL bValue = (int)(m_bRESRV_Ch01_ON_OFF.GetCheck());
// 	if(bValue == RESERV_ON_BY_CH)
// 	{
// 		m_bRESRV_Ch01_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
// 	}
// 	else
// 	{
// 		m_bRESRV_Ch01_ON_OFF.SetCheck(RESERV_ON_BY_CH);
// 	}
	AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH1_ONOFF", (int)(m_bRESRV_Ch01_ON_OFF.GetCheck()));

	UpdateData(TRUE);
}

void CReserveDlg::OnRadReservCh2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(FALSE);
//	BOOL bValue = (int)(m_bRESRV_Ch02_ON_OFF.GetCheck());
// 	if(bValue == RESERV_ON_BY_CH)
// 	{
// 		m_bRESRV_Ch02_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
// 	}
// 	else
// 	{
// 		m_bRESRV_Ch02_ON_OFF.SetCheck(RESERV_ON_BY_CH);
// 	}
	AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH2_ONOFF", (int)(m_bRESRV_Ch02_ON_OFF.GetCheck()));
	
	UpdateData(TRUE);
}

void CReserveDlg::OnRadReservCh3() 
{
	// TODO: Add your control notification handler code here
	UpdateData(FALSE);
// 	BOOL bValue = (int)(m_bRESRV_Ch03_ON_OFF.GetCheck());
// 	if(bValue == RESERV_ON_BY_CH)
// 	{
// 		m_bRESRV_Ch03_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
// 	}
// 	else
// 	{
// 		m_bRESRV_Ch03_ON_OFF.SetCheck(RESERV_ON_BY_CH);
// 	}
// 	AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH3_ONOFF", (int)(m_bRESRV_Ch03_ON_OFF.GetCheck()));
// 	
	UpdateData(TRUE);
}

void CReserveDlg::OnRadReservCh4() 
{
	// TODO: Add your control notification handler code here
	UpdateData(FALSE);
// 	BOOL bValue = (int)(m_bRESRV_Ch04_ON_OFF.GetCheck());
// 	if(bValue == RESERV_ON_BY_CH)
// 	{
// 		m_bRESRV_Ch04_ON_OFF.SetCheck(RESERV_OFF_BY_CH);
// 	}
// 	else
// 	{
// 		m_bRESRV_Ch04_ON_OFF.SetCheck(RESERV_ON_BY_CH);
// 	}
// 	AfxGetApp()->WriteProfileInt(RESERVATION_REGISTRY_KEY, "RESEV_CH4_ONOFF", (int)(m_bRESRV_Ch04_ON_OFF.GetCheck()));
// 	
	UpdateData(TRUE);
}