// CChamberCtrl.h: interface for the CChillerCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "../OvenCtrl.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct s_OVEN_CONTL 
{
	SFT_CHAMBER_VALUE m_pChamberData;
	int    m_nItem;
	int    m_iError;
	int    m_nCHNO;
	int    m_nChamberONorOff;//1-ON,2-OFF
	int    m_iReqStop;

	s_OVEN_CONTL()
	{
		 m_nItem=-1;
		 m_iError=0;
		 m_nCHNO=0;
		 m_nChamberONorOff=0;
		 m_iReqStop=0;
		 
		 ZeroMemory(&m_pChamberData,sizeof(SFT_CHAMBER_VALUE));
	}
}ST_OVEN_CONTL;

class CChamberCtrl 
{
public:
	CChamberCtrl();
	BOOL CheckOven_Line1(int nOvenID);
	BOOL CheckOven_Line2(int nOvenID);
	int	m_nChamberWriteCount1;		//ljb 20170720 chamber log 남기기 count
	int	m_nChamberWriteCount2;		//ljb 20170720 chamber log 남기기 count
	CString m_strpathIni;
	virtual ~CChamberCtrl();
/*
	COvenCtrl		m_ctrlOven[MAX_CHAMBER_COUNT];
	SFT_CHAMBER_VALUE m_pChamberData;

	int    m_nItem;
	int    m_iError;
	int    m_nCHNO;
	int    m_nChamberONorOff;//1-ON,2-OFF
	int    m_iReqStop;

	HANDLE m_hThread1;
	HANDLE m_hThread2;
	*/
	COvenCtrl m_ctrlOven1;
	COvenCtrl m_ctrlOven2;
	HANDLE m_hThread1;
	HANDLE m_hThread2;
	modbus_t* ctx1;
	modbus_t* ctx2;
	CRITICAL_SECTION* pCS1;
	CRITICAL_SECTION* pCS2;

	ST_OVEN_CONTL mST_OvenContl[MAX_CHAMBER_COUNT];

	void onResetData();
	void onSetInit(int nItem);
	CString onGetLogType(int nType);

	static DWORD WINAPI Chamber1DealThread(LPVOID arg);
	static DWORD WINAPI Chamber2DealThread(LPVOID arg);
protected:

};


