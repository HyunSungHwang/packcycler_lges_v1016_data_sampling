// ChamberCtrl.cpp: implementation of the CChamberCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 


#include "../CTSMonPro.h"
#include "ChamberCtrl.h"

#include "../MainFrm.h"
#include "../CTSMonPro.h"
#include "../CTSMonProDoc.h"
#include "../CTSMonProView.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChamberCtrl::CChamberCtrl()
{	
	m_hThread1=NULL;
	m_hThread2=NULL;
	m_nChamberWriteCount1 = 0;
	m_nChamberWriteCount2 = 0;

// 	m_nItem=-1;
// 	m_iError=0;
// 	m_nCHNO=0;
// 	m_nChamberONorOff=0;
// 	m_iReqStop=0;

	//ZeroMemory(&m_pChamberData,sizeof(SFT_CHAMBER_VALUE));
}

CChamberCtrl::~CChamberCtrl()
{
	GWin::win_ThreadEnd2(m_hThread1);
	GWin::win_ThreadEnd2(m_hThread2);
}

void CChamberCtrl::onResetData()
{	
	for(int i= 0; i< MAX_CHAMBER_COUNT; i++)
	{
		mST_OvenContl[i].m_nItem = -1;
		mST_OvenContl[i].m_iError=0;	
		mST_OvenContl[i].m_nCHNO=0;
		mST_OvenContl[i].m_nChamberONorOff=0;
		mST_OvenContl[i].m_iReqStop=0;
		ZeroMemory(&(mST_OvenContl[i].m_pChamberData),sizeof(SFT_CHAMBER_VALUE));
	}
	ctx1 = NULL;//yulee 20190609
	ctx2 = NULL;
/*
	m_nItem=-1;
	m_iError=0;	
	m_nCHNO=0;
	m_nChamberONorOff=0;
	m_iReqStop=0;

	ZeroMemory(&m_pChamberData,sizeof(SFT_CHAMBER_VALUE));
	*/
}

void CChamberCtrl::onSetInit(int nItem)
{		
	DWORD hThreadId=-1; //yulee 20190611
	if((!(m_hThread1))&&(nItem == 0))
	{
		hThreadId = 0;
		m_hThread1 = CreateThread(NULL, 0, Chamber1DealThread,(LPVOID)this, 0, &hThreadId);	
	}
	else if((!(m_hThread2))&&(nItem == 1))
	{
		hThreadId = 0;
		m_hThread2 = CreateThread(NULL, 0, Chamber2DealThread,(LPVOID)this, 0, &hThreadId);	
	}
}



CString CChamberCtrl::onGetLogType(int nType)
{
	int nCHNO=mST_OvenContl[nType].m_nCHNO;

	CString strLogType;
	strLogType.Format(_T("chamber%d"),nCHNO+1);
	return strLogType;
}

DWORD WINAPI CChamberCtrl::Chamber1DealThread(LPVOID arg)
{//yulee 20190608
	CChamberCtrl *obj=(CChamberCtrl *)arg;
	if(!obj) return 0;

	Sleep(1000);

	//TRACE("====================>>>>>>>>    Chamber1 Thread start Chamber1DealThread \n");
	if(!mainFrm)  return 0;
	if(!mainView) return 0;
	CCTSMonProDoc *pMainDoc=mainView->GetDocument();
	if(!pMainDoc) return 0;

	static int nDevice = 0;
	int  nOvenID;
	long lOvenState;
	int  m_uLogCnt=1;
	int  nItem=obj->mST_OvenContl[0].m_nItem;

	int  nCHNO=obj->mST_OvenContl[0].m_nCHNO;	
	CString strLogType=obj->onGetLogType(0);

	CString strTemp;
	strTemp.Format("CHNO:%d Chamber%d Thread Start(Model %d)",
		nCHNO,nItem,obj->m_ctrlOven1.GetOvenModelType());
	log_All(strLogType,_T("thread"),strTemp);

	while(obj->m_ctrlOven1.m_bUseOvenCtrl)
	{
		while(obj->m_ctrlOven1.m_bPortOpen)
		{
			for (nOvenID=0; nOvenID < obj->m_ctrlOven1.GetOvenCount() ; nOvenID++)
			{
				//Oven 사용 여부
				if (obj->m_ctrlOven1.GetUseOven(nOvenID) == FALSE) continue;

				//Oven 상태 체크
				lOvenState = obj->m_ctrlOven1.GetOvenStatus(nOvenID);
				if (lOvenState != 0)
				{
					//전채널 일시 정지 => Warring Message 표시
					//ljb 임시	Fun_AlarmOvenState(nOvenID, lOvenState);
					//return;
				}
				//------------------------------------------
				if(obj->mST_OvenContl[0].m_iReqStop==1)
				{
					Sleep(1000);
					continue;
				}
				//------------------------------------------
				if(obj->m_ctrlOven1.GetDisconnectCnt(nOvenID) < 10) 
				{
					//현재 Oven 통신 가능
					//현재 Oven의 상태값을 Update한다.	

					if (obj->m_ctrlOven1.RequestCurrentValue(nDevice,nOvenID, (LPVOID)obj) == 0)	
					{//lmh 20111115 2단챔버 연동시 디바이스와 오븐아이디가 바뀌어야함
						obj->m_ctrlOven1.IncreaseDisconnectTime(nOvenID);//통신 두절 count 증가

						if(obj->m_ctrlOven1.m_ctx != NULL)
						{
							modbus_flush(obj->m_ctrlOven1.m_ctx);
						}	

						//modbus_close(obj->m_ctrlOven.m_ctx);
						Sleep(100);
						//modbus_connect(obj->m_ctrlOven.m_ctx);

						m_uLogCnt++;
						if((m_uLogCnt % 90) == 0)	// log 남기기
						{ 
							strTemp.Format("Chamber 1 no response(Oven Model %d, Oven ID %d)",pMainDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenModelType(),nOvenID+1);//&&
							pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_SYSTEM);//$1013
							m_uLogCnt = 0;
						}
					}
					else
					{
						//ksj 20201017 : 정상적으로 RequestCurrentValue 읽혔다면
						//디스커넥 카운트를 리셋해준다.
						//기존 로직은 디스커넥카운트가 누적되면 언젠가는 값을 가져오지 않게 될듯?
						obj->m_ctrlOven1.ResetDisconnectCnt(nOvenID); 
					}
				}
				else
				{
					//현재 Oven 통신 불능
					//10초 이상이면  전송하지 않음
					obj->m_ctrlOven1.SetLineState(nOvenID, FALSE);
					obj->m_ctrlOven1.ClearCurrentData();
					obj->m_ctrlOven1.ResetDisconnectCnt(nOvenID);					
				}

				//Start Dlg가 떠있는동안은 Reset 시킴 지연을 위해 반듯이 필요
				//if(m_bBlockFlag)
				//{
				//	pDoc->m_nRunCmdDelayTime = 0;
				//}
				//else
				//{
				//	pDoc->m_nRunCmdDelayTime += (CHAMBER_TIMER/1000);		//2sec timer
				//}

				//Oven 연동 채널이 모두 Oven연동 Pause 상태 일때 -> 챔버 온도 변경 및 Oven 연동 Continue 전송			
				if(obj->m_ctrlOven1.GetLineState(nOvenID)) 	
				{
			
					obj->CheckOven_Line1(nOvenID);
				}

			}
			Sleep(1000);

		}// end while

		Sleep(1000);
	}
	strTemp.Format("CHNO:%d Chamber%d Thread End",nCHNO,nItem);
	log_All(strLogType,_T("thread"),strTemp);
	return 1;
}

DWORD WINAPI CChamberCtrl::Chamber2DealThread(LPVOID arg)
{//yulee 20190608
	CChamberCtrl *obj=(CChamberCtrl *)arg;
	if(!obj) return 0;

	Sleep(1000);

	//TRACE("====================>>>>>>>>    Chamber2 Thread start Chamber2DealThread \n");
	if(!mainFrm)  return 0;
	if(!mainView) return 0;
	CCTSMonProDoc *pMainDoc=mainView->GetDocument();
	if(!pMainDoc) return 0;

	static int nDevice = 0;
	int  nOvenID;
	long lOvenState;
	int  m_uLogCnt=1;
	int  nItem=obj->mST_OvenContl[1].m_nItem;

	int  nCHNO=obj->mST_OvenContl[1].m_nCHNO;	
	CString strLogType=obj->onGetLogType(1);

	CString strTemp;
	strTemp.Format("CHNO:%d Chamber%d Thread Start(Model %d)",
		nCHNO,nItem,obj->m_ctrlOven2.GetOvenModelType());
	log_All(strLogType,_T("thread"),strTemp);

	while(obj->m_ctrlOven2.m_bUseOvenCtrl)
	{
		//line 연결 상태 확인을 위해 처음에 FALSE 셋팅
		//pOvenCtrl.SetLineState(nOvenID, FALSE);

		while(obj->m_ctrlOven2.m_bPortOpen)
		{
			for (nOvenID=0; nOvenID < obj->m_ctrlOven2.GetOvenCount() ; nOvenID++)
			{
				//Oven 사용 여부
				if (obj->m_ctrlOven2.GetUseOven(nOvenID) == FALSE) continue;

				//Oven 상태 체크
				lOvenState = obj->m_ctrlOven2.GetOvenStatus(nOvenID);
				if (lOvenState != 0)
				{
					//전채널 일시 정지 => Warring Message 표시
					//ljb 임시	Fun_AlarmOvenState(nOvenID, lOvenState);
					//return;
				}
				//------------------------------------------
				if(obj->mST_OvenContl[1].m_iReqStop==1)
				{
					Sleep(1000);
					continue;
				}
				//------------------------------------------
				if(obj->m_ctrlOven2.GetDisconnectCnt(nOvenID) < 10) 
				{
					//현재 Oven 통신 가능
					//현재 Oven의 상태값을 Update한다.	

					if (obj->m_ctrlOven2.RequestCurrentValue(nDevice,nOvenID ,  (LPVOID)obj) == 0)	
					{//lmh 20111115 2단챔버 연동시 디바이스와 오븐아이디가 바뀌어야함
						obj->m_ctrlOven2.IncreaseDisconnectTime(nOvenID);//통신 두절 count 증가

						if(obj->m_ctrlOven1.m_ctx != NULL)
						{
							modbus_flush(obj->m_ctrlOven2.m_ctx);
						}
						//modbus_close(obj->m_ctrlOven.m_ctx);
						Sleep(100);
						//modbus_connect(obj->m_ctrlOven.m_ctx);

						m_uLogCnt++;
						if((m_uLogCnt % 90) == 0)	// log 남기기
						{
							//strTemp.Format("챔버 2 에서 응답이 없습니다.(Oven Model %d, Oven ID %d)",pMainDoc->m_ctrlOven[1].GetOvenModelType(),nOvenID+1);
							strTemp.Format("Chamber 2 no response(Oven Model %d, Oven ID %d)",pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenModelType(),nOvenID+1);//&&
							pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_SYSTEM);
							m_uLogCnt = 0;
						}
					}
					else
					{
						//ksj 20201017 : 정상적으로 RequestCurrentValue 읽혔다면
						//디스커넥 카운트를 리셋해준다.
						//기존 로직은 디스커넥카운트가 누적되면 언젠가는 값을 가져오지 않게 될듯?
						obj->m_ctrlOven2.ResetDisconnectCnt(nOvenID); 
					}
				}
				else
				{
					//현재 Oven 통신 불능
					//10초 이상이면  전송하지 않음
					obj->m_ctrlOven2.SetLineState(nOvenID, FALSE);
					obj->m_ctrlOven2.ClearCurrentData();
					obj->m_ctrlOven2.ResetDisconnectCnt(nOvenID);					
				}

				//Start Dlg가 떠있는동안은 Reset 시킴 지연을 위해 반듯이 필요
				//if(m_bBlockFlag)
				//{
				//	pDoc->m_nRunCmdDelayTime = 0;
				//}
				//else
				//{
				//	pDoc->m_nRunCmdDelayTime += (CHAMBER_TIMER/1000);		//2sec timer
				//}

				//Oven 연동 채널이 모두 Oven연동 Pause 상태 일때 -> 챔버 온도 변경 및 Oven 연동 Continue 전송			
				if(obj->m_ctrlOven2.GetLineState(nOvenID)) 	
				{
					obj->CheckOven_Line2(nOvenID);
				}

			}// end for (nOvenID=0; nOvenID < pOvenCtrl.GetOvenCount() ; nOvenID++)
			Sleep(1000);			

		}// end while(pOvenCtrl.m_bPortOpen)

		Sleep(1000);
	}
	strTemp.Format("CHNO:%d Chamber%d Thread End",nCHNO,nItem);
	log_All(strLogType,_T("thread"),strTemp);
	return 1;
}

//Oven내 step변화를 감지 하고 다음 온도를 전송함
// => Oven 연동 채널에 대해서 모든 채널이 Pause(Oven 연동 대기) 이면 Oven 온도 변경 후 Oven 연동 Continue 전송
//=> 한개의 Oven에서 Oven연동으로 시작 한 채널은 Oven연동으로 시작한 첫번째 채널의 스케쥴을 따라감
// Oven연동 Channel이 모두 Paue(Oven 연동 대기)로 변했을 경우 새로운 온도 명령을 전송한다.

BOOL CChamberCtrl::CheckOven_Line1(int nOvenID)
{
	m_nChamberWriteCount1++;	//3초 타이머

	CCTSMonProDoc *pMainDoc = mainView->GetDocument();
	if(!pMainDoc)  return 0;

	COvenCtrl* pCtrlOven = &(pMainDoc->m_ChamberCtrl.m_ctrlOven1);

	/////////////////////////////////////////////////////////////////////
	//ljb 201012 챔버 데이터 PSERVER.DLL로 전송
	CString strTemp;
	ZeroMemory(&(pMainDoc->m_sChamData_1),sizeof(SFT_CHAMBER_VALUE));

	strTemp.Format("%.1f",pCtrlOven->GetCurTemperature(nOvenID));
	pMainDoc->m_sChamData_1.fCurTemper = atof(strTemp);
	pMainDoc->m_sChamData_1.fRefTemper = pCtrlOven->GetRefTemperature(nOvenID); // 오븐의 설정 온도 데이터를 채널에 저장

	if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 
		|| pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 
		|| pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500
		|| pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
	{
		pMainDoc->m_sChamData_1.fCurHumidity = pCtrlOven->GetCurHumidity(nOvenID); // 오븐의 현재 습도 데이터를 채널에 저장
		pMainDoc->m_sChamData_1.fRefHumidity = pCtrlOven->GetRefHumidity(nOvenID); // 오븐의 설정 습도 데이터를 채널에 저장
	}	
	//TRACE("Oven1 -> CurTemp = %.3f, RefTemp = %.3f",pMainDoc->m_sChamData_1.fCurTemper,pMainDoc->m_sChamData_1.fRefTemper);

	int mdIndex =1;
	//::SFTSetChamberData(mdIndex,0,&pMainDoc->m_sChamData_1);		//챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 1//yulee 20190304 

	if(pMainDoc->m_nChamberOperationType == 0)		//m_nChamberOperationType = 0 이면 한 챔버에 ch이 모두 있음.			
	{	
		//모든 모듈 - 채널에 값을 넣어 줘야 함.
		::SFTSetChamberData(mdIndex,1,&pMainDoc->m_sChamData_1); //챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 2
	}
	//////////////////////////////////////////////////////////////////////////
	pMainDoc->m_iOvenStartOption_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Start Chamber Option", 0);
	pMainDoc->m_uiOvenCheckTime_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Fix Check Time", 0);
	pMainDoc->m_uiOvenCheckFixTime_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Schedule Check Time", 0);

	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Fix Temp","25.0");
	pMainDoc->m_fOvenStartFixTemp_1 = atof(strTemp);

	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Delta Fix Temp","1");
	pMainDoc->m_fOvenDeltaFixTemp_1 = atof(strTemp);

	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Delta Temp","1");
	pMainDoc->m_fOvenDeltaTemp_1 = atof(strTemp);

	pMainDoc->m_uiOvenPatternNum_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Pattern No", 1);

	//m_iOvenContinueType_1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Chamber Continue Type", 0);		//ljb 20180226 add

	if (pMainDoc->m_iOvenStartOption_1 < 1) return FALSE;		//ljb 챔버 연동 안함
	if (pMainDoc->m_iOvenStartOption_1 > 3) return FALSE;		//ljb 챔버 연동 안함

	//ljb Oven 연동
	int nLinkChamberCount=0;		//ljb 챔버 연동 채널 갯수
	int nReadyNextChamberCount=0;	//ljb 챔버 연동 대기 채널 갯수	(nLinkChamberCount 와 같으면 다음 명령 전송)
	CDWordArray adwChArray;			//ljb 챔버 연동 모듈,채널 갯수
	BOOL	bOverTempSpec(FALSE);

	CCyclerModule *pMD; 
	CCyclerChannel *pCh;
	int nChannelIndex=0;	//ljb
	int nMinStepNo = 0x7fffffff;
	int nTotalCh = 0; // = 2;					//기본 1채널만 해당됨


 	int nDevTotalCh = 0;
	nDevTotalCh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC , "Max Parallel Count", 2); //yulee 20190910 	nDevTotalCh = pMD->GetTotalChannel(); //yulee 20190910
	int ch = 0; //yulee 20190708
	if(g_AppInfo.iPType == TRUE) //yulee 20190708
	{
		nTotalCh = 1;
	}
	else
	{
 		if(nDevTotalCh == 2)
 		{ 
 			nTotalCh = 1;
 		}
 		else if(nDevTotalCh == 4)
 		{
			nTotalCh = 2;
		}
	}
	//전체 모듈에 대해 챔버연동 대기 채널 검색
	//for(mdIndex =1; mdIndex <= GetInstallModuleCount(); mdIndex++)
	//{
	pMD = pMainDoc->GetModuleInfo(mdIndex);
	if(pMD)
	{
		//Line Off
		if(pMD->GetState() == PS_STATE_LINE_OFF) return FALSE;

		if(pMainDoc->m_nChamberOperationType == 0)	//nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 1);
		{
			nTotalCh = pMD->GetTotalChannel();	////1챔버에 모든 채널이 있음
		}
		for(int ch =0; ch < nTotalCh; ch++)
		{
			::SFTSetChamberData(mdIndex,ch,&pMainDoc->m_sChamData_1);		//ljb 20151120 add	채널별로 챔버 데이터 전송
			pCh = pMD->GetChannelInfo(ch);
			if(pCh)
			{
				if(nOvenID == pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
				{	
					//Idle인 상태
					if( pCh->GetState() == PS_STATE_IDLE || pCh->GetState() == PS_STATE_STANDBY ||
						pCh->GetState() == PS_STATE_END	|| pCh->GetState() == PS_STATE_READY )	
					{								
						//continue;
					}
					else if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
					{
						//ljb 20120627 병렬모드 일때도 skip
						TRACE("병렬모드 skip \n");
						//continue;
					}
					else if (pCh->GetState() == PS_STATE_MAINTENANCE)
					{
						//continue;
					}

					else if((pCh->GetChamberUsing() == TRUE) && ( pCh->GetStepType()  == PS_STEP_EXT_CAN)) 
					{//20180704 yulee External CAN으로 챔버 제어
						// 챔버 사용 중, External CAN 스텝이면 다음과 같은 동작을 한다.
						// CAN Ddata 중 코드 1700으로 등록된 값(Ref Temp.) 가져온다. 
						// 챔버의 먼저 설정된 값(SP)이 설정 할 온도(RT)와 같으면 Skip
						// 현재 챔버 온도(CT)와 설정 온도(RT)를 비교한다. - 비슷하면 skip 
						// SP와 RT가 다르면 RT가 0이 아닐 경우 챔버 온도 설정(pCtrlOven->SetTemperature)을 한다. 

						//CAN 데이터에서 값 가져오기 
						SFT_CAN_DATA * pCanData = pCh->GetCanData();
						if(pCanData == NULL) return FALSE;

						float fCanTref = 0.0;
						CString strItem = ("");
						for(int j = 0; j < pCh->GetCanCount(); j++)
						{
							if(pCanData[j].function_division == 1700)
							{
								if(pCanData[j].data_type == 0 || pCanData[j].data_type == 1 || pCanData[j].data_type == 2)
								{
									strItem.Format("%.6f", pCanData[j].canVal.fVal[0]);
									if (pCanData[j].function_division > 100 && pCanData[j].function_division < 150)
										strItem.Format("%02x%02x%02x%02x", (BYTE)pCanData[j].canVal.strVal[0], (BYTE)pCanData[j].canVal.strVal[1], (BYTE)pCanData[j].canVal.strVal[2], (BYTE)pCanData[j].canVal.strVal[3]);

									strItem.TrimRight("0");
									strItem.TrimRight(".");
								}
								else if(pCanData[j].data_type == 3){
									strItem.Format("%s", pCanData[j].canVal.strVal);
								}
								else if (pCanData[j].data_type == 4){
									strItem.Format("%x", (UINT)pCanData[j].canVal.fVal[0]);	
								}
								fCanTref = atof(strItem);
								break;
							}
						}

						if(((strItem.IsEmpty() == FALSE) || (strItem != (""))) && (fCanTref != 0.0))
						{ 
							float fChamberSPTemp = 0.0;
							fChamberSPTemp = pCtrlOven->GetRefTemperature(nOvenID);
							if(fChamberSPTemp != fCanTref)
							{
								if ((pMainDoc->m_sChamData_1.fCurTemper < (fCanTref - 0.2)) || (pMainDoc->m_sChamData_1.fCurTemper > (fCanTref + 0.2)))
								{
									if(pCtrlOven->SetTemperature(0,nOvenID, fCanTref))
									{
										//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) \r\n",pCh->GetStepNo(), fCanTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper );
										strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg1","CTSMonPro_DOC"),pCh->GetStepNo(), fCanTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper );//&&
										pCh->WriteLog(strTemp);//$1013
									}
									else
										//{	strTemp.Format("STEP %d 챔버 온도설정 명령 전송실패 \r\n",pCh->GetStepNo());
									{	strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg2","CTSMonPro_DOC"),pCh->GetStepNo());//&&
									pCh->WriteLog(strTemp);//$1013
									}
								}
							}
						}
					}
					else
					{
						//Fix 모드에서 온도 범위 초과 체크
						if (pMainDoc->m_iOvenStartOption_1 == 1 && pMainDoc->m_uiOvenCheckFixTime_1 != 0)
						{
							if (pCtrlOven->GetRun(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetTotTime() > pMainDoc->m_uiOvenCheckFixTime_1)//lmh 201111 chk 챔버 연동 가능하게 변경해야함
							{
								if ((fabs(pMainDoc->m_sChamData_1.fRefTemper - pMainDoc->m_sChamData_1.fCurTemper)) > pMainDoc->m_fOvenDeltaFixTemp_1)
								{
									bOverTempSpec = TRUE;
									strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg1","CTSMonPro_DOC"),pMainDoc->GetModuleName(pMD->GetModuleID()),ch+1,pMainDoc->m_sChamData_1.fRefTemper,pMainDoc->m_sChamData_1.fCurTemper,pMainDoc->m_fOvenDeltaTemp_1);
									//strTemp.Format("모듈(%s) 채널(%d) => Fix Mode 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,m_sChamData.fRefTemper,m_sChamData.fCurTemper,m_fOvenDeltaTemp_1);
									pCh->WriteLog(strTemp);
									pMainDoc->WriteSysEventLog(strTemp);
								}
							}
							if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
							{
								adwChArray.Add(MAKELONG(ch, mdIndex));									
							}
							else
							{
							}
						}

						//스케쥴 연동
						if (pMainDoc->m_iOvenStartOption_1 == 3 && pCh->GetChamberUsing())
						{
							//진행 시간과 온도 편차가 설정 값보다 크면 챔버 정지 및 충방전기 일시 정지, 챔버가 Prog 모드일 경우는?
							if (pMainDoc->m_uiOvenCheckTime_1 != 0)
							{
								if (pCtrlOven->GetRun(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetStepTime() > pMainDoc->m_uiOvenCheckTime_1)
								{
									if ((fabs(pMainDoc->m_sChamData_1.fRefTemper - pMainDoc->m_sChamData_1.fCurTemper)) > pMainDoc->m_fOvenDeltaTemp_1)
									{
										bOverTempSpec = TRUE;
										strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg2","CTSMonPro_DOC"),pMainDoc->GetModuleName(pMD->GetModuleID()),ch+1,pMainDoc->m_sChamData_1.fRefTemper,pMainDoc->m_sChamData_1.fCurTemper,pMainDoc->m_fOvenDeltaTemp_1);//strTemp.Format("모듈(%s) 채널(%d) => 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,m_sChamData.fRefTemper,m_sChamData.fCurTemper,m_fOvenDeltaTemp_1);
										pCh->WriteLog(strTemp);
										pMainDoc->WriteSysEventLog(strTemp);
									}
								}
							}

							if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
							{
								nLinkChamberCount ++;
								adwChArray.Add(MAKELONG(ch, mdIndex));
							}
							else
							{

							}
							if (m_nChamberWriteCount1 > 20)
							{
								strTemp.Format("nLinkChamberCount %d, CellCode %d, Ch State %d ,Step No,%d, Step Type, %d,chamber Temp, PV, %.2f, SV, %.2f,chamber Humi, PV, %.2f, SV, %.2f",
									nLinkChamberCount, pCh->GetCellCode(),pCh->GetState(), pCh->GetStepNo(),pCh->GetStepType(),pMainDoc->m_sChamData_1.fCurTemper,pMainDoc->m_sChamData_1.fRefTemper,pMainDoc->m_sChamData_1.fCurHumidity,pMainDoc->m_sChamData_1.fRefHumidity);
								pCh->WriteChamberLog(strTemp);
								m_nChamberWriteCount1 = 0;
							}
						}

					}

					if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
					{
						//ljb 20120627 병렬모드 일때도 skip
						TRACE("병렬모드 skip \n");
						//continue;
					}
					else if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)	// 87 챔버연동 대기
					{
						nReadyNextChamberCount ++;
						nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호
					}
				}//end if(nOvenID == pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
			}//end if (pCh)
		}//end for (ch)
	}//end if (pMD)
	//}//end for (mdIndex)

	//챔버연동으로 Pause 된 채널이 없으면 리턴
	if (pMainDoc->m_iOvenStartOption_1 == 3 && nLinkChamberCount == 0 ) return FALSE;

	POSITION pos = pMainDoc->GetFirstViewPosition();
	CCTSMonProView *pView = (CCTSMonProView *)pMainDoc->GetNextView(pos);
	//일시 정지 및 챔버 STOP
	if (bOverTempSpec)
	{
		//★★ ljb 2010326  ★★★★★★★★ //챔버 연동된 채널에 Contiue 전송
		pView->SendCmdToModule(adwChArray, SFT_CMD_PAUSE);
		//1. 챔버 STOP
		if (pCtrlOven->SendStopCmd(0,nOvenID) == FALSE)
		{
			strTemp = Fun_FindMsg("CheckOvenTemp_Line1_msg3","CTSMonPro_DOC"); //strTemp = "챔버 동작 중단 명령 실패(REST)!!!";
			pMainDoc->WriteSysLog(strTemp);
		}
		strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg4","CTSMonPro_DOC"),pMainDoc->m_sChamData_1.fRefTemper,pMainDoc->m_sChamData_1.fCurTemper,pMainDoc->m_fOvenDeltaTemp_1);//strTemp.Format("설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",m_sChamData.fRefTemper,m_sChamData.fCurTemper,m_fOvenDeltaTemp_1);
		pMainDoc->WriteSysEventLog(strTemp);
		//AfxMessageBox(strTemp);
	}

	//스케줄 연동이 아니면 리턴 한다.
	if (pMainDoc->m_iOvenStartOption_1 != 3 ) return FALSE;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//챔버 연동 명령어 전송 부분
	//-------------------------------------------------------
	// 	CCyclerModule *pMD;		//상위에 선언되어 있음.
	// 	CCyclerChannel *pCh;	//상위에 선언되어 있음.

	int nUsingChamberCh = 0, nRunStepNo = -1;

	DWORD dwData;
	BOOL nRtn = FALSE;	
	CString RegName;
	//BOOL bChamperContinue(FALSE);	//챔버연동 대기 플래그 값 

	pCh = NULL;
	nUsingChamberCh = adwChArray.GetSize();
	if (nReadyNextChamberCount > 0)
	{
		dwData = adwChArray.GetAt(0);
		pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));

		if(pCh)
		{
			nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호

			CScheduleData *lpSchedule = pCh->GetScheduleData();		//ljb 20180219 메모리로 업로드 하고 읽어 오는 구조로 변경 해야 할듯 함.
			if(lpSchedule)
			{							
				//CStep *lpStep = lpSchedule->GetStepData(nMinStepNo);
				CStep *lpStep = lpSchedule->GetStepData(nMinStepNo -1);		//ljb 20140108 add 현재스텝 가져오기

				//RegName.Format("UseOvenChamberContinue%d",pCh->GetChannelIndex() + 1);	
				//bChamperContinue = AfxGetApp()->GetProfileInt(OVEN_SETTING, RegName, 0);	//챔버대기 옵션 확인.	//ljb 20180212 default 를 0으로 변경
				RegName.Format("OvenContinue_%d",pCh->GetChannelIndex() + 1);	
				BOOL bChamberContinue;
				bChamberContinue = AfxGetApp()->GetProfileInt(OVEN_SETTING, RegName, 0);		

				if(bChamberContinue == FALSE)
				{
					if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
					{
						//strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg3","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
						pCh->WriteLog(strTemp);//$1013
					}

					if(lpStep->m_fTref != 0.0f)
					{ 
						pCtrlOven->SetTemperature(0,nOvenID, lpStep->m_fTref);								
						//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper,bChamberContinue );
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg4","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper,bChamberContinue );//&&
						pCh->WriteLog(strTemp);//$1013
					}

					if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500 ||
						pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							pCtrlOven->SetHumidity(0,nOvenID, lpStep->m_fHref);								
							//strTemp.Format("STEP %d 챔버 설정습도 %.2f (챔버 현재습도: %.2f / 챔버 현재설정습도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fHref, pMainDoc->m_sChamData_1.fCurHumidity, pMainDoc->m_sChamData_1.fRefHumidity,bChamberContinue );
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg5","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fHref, pMainDoc->m_sChamData_1.fCurHumidity, pMainDoc->m_sChamData_1.fRefHumidity,bChamberContinue );//&&
							pCh->WriteLog(strTemp);//$1013
						}
					}

					//strTemp.Format("%.1f",pCtrlOven->GetRefTemperature(nOvenID));		//ljb 20170719 add
					//fCurOvenSetTemp = atof(strTemp);
					//if (lpStep->m_fTref == fCurOvenSetTemp)
					//if ( ((lpStep->m_fTref - 0.2) < fCurOvenSetTemp) && (fCurOvenSetTemp < (lpStep->m_fTref + 0.2)))		//ljb 20180212 수정 float 변환으로 인한 설정값 이상을 의심
					//{
					//	pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	
					//	strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData.fCurTemper, m_sChamData.fRefTemper,bChamperContinue );
					//	lpChannel->WriteLog(strTemp);
					//	return TRUE;
					//}
					//else
					//	return FALSE;

					return TRUE;
				}//end if(m_iOvenContinueType_1 == FALSE)	

				if(lpStep->m_type == PS_STEP_LOOP)
				{			
					pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	//ljb 20130710 add
					//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) LOOP \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper );
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg6","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper );//&&
					pCh->WriteLog(strTemp);//$1013
					return TRUE;
				}

				//ljb 20180219	// 챔버 온도 대기시 온도 비교 하는 부분 (현재 온도가 비슷하면 Continue 한다)
				if ( (pMainDoc->m_sChamData_1.fCurTemper >= (lpStep->m_fTref - 0.2)) && 
					(pMainDoc->m_sChamData_1.fCurTemper <= (lpStep->m_fTref + 0.2)))
				{
					if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || 
						pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || 
						pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500 ||
						pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
					{
						if ( (pMainDoc->m_sChamData_1.fCurHumidity >= (lpStep->m_fHref - 0.2)) && 
							(pMainDoc->m_sChamData_1.fCurHumidity <= (lpStep->m_fHref + 0.2)))
						{
							if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
							{
								//strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg7","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
								pCh->WriteLog(strTemp);//$1013

								//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 1
								RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
								BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 1);
								strTemp.Format(" %s registry %s Write 1", OVEN_SETTING, RegName);
								pCh->WriteLog(strTemp);
								if(bRetWriteProfile != TRUE)
									return FALSE;	
							}
							else
							{
								//strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg8","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
								pCh->WriteLog(strTemp);//$1013

								//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 0
								RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
								BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 0);
								strTemp.Format(" %s registry %s Write 0", OVEN_SETTING, RegName);
								pCh->WriteLog(strTemp);
								if(bRetWriteProfile != TRUE)
									return FALSE;	
								
							}
							//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg9","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper);//&&
							pCh->WriteLog(strTemp);
							return TRUE;
						}
					}
					else
					{
						if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
						{
							//strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg10","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
							pCh->WriteLog(strTemp);//$1013

							//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 1
							RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
							BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 1);
							strTemp.Format(" %s registry %s Write 1", OVEN_SETTING, RegName);
							pCh->WriteLog(strTemp);
							if(bRetWriteProfile != TRUE)
								return FALSE;	
						}
						else
						{
							//strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg11","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
							pCh->WriteLog(strTemp);//$1013

							//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 0
							RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
							BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 0);
							strTemp.Format(" %s registry %s Write 0", OVEN_SETTING, RegName);
							pCh->WriteLog(strTemp);
							if(bRetWriteProfile != TRUE)
								return FALSE;	
						}

						//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg12","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_1.fCurTemper, pMainDoc->m_sChamData_1.fRefTemper);//&&
						pCh->WriteLog(strTemp);//$1013
						return TRUE;
					}
				}//end if ((pMainDoc->m_sChamData_1.fCurTemper >= (lpStep->m_fTref - 0.2)) && (pMainDoc->m_sChamData_1.fCurTemper <= (lpStep->m_fTref + 0.2)))


				if(lpStep == NULL || lpStep->m_type == PS_STEP_END)
				{
					//챔버 STOP
					if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoOvenStop", FALSE))
						pMainDoc->OvenStopStateAndSendStopCmd(1);			//챔버 1라인 
					return TRUE;
				}
				else
				{
					//float fSetOvenTempValue;
					//float fSetOvenHumValue; //ksj 20160310

					//if (lpStep->m_bUseChamberProg){
					//	fSetOvenTempValue = pCtrlOven->GetProgTemperature(nOvenID); // Prog 모드 온도 목표치						
					//	//ksj 20160310 Prog 모드에 습도값 넣어야하는가???
					//	fSetOvenHumValue = 0.0f; //일단 0으로 둠.					
					//}
					//else{				
					//	fSetOvenTempValue = pCtrlOven->GetRefTemperature(nOvenID); // 오븐의 현재 온도
					//	fSetOvenHumValue = pCtrlOven->GetRefHumidity(nOvenID);		//ksj 20160310 오븐 현재 습도.
					//}

					//ljb 설정 온도가 맞는지 비교 하는 부분 (목표 값으로 설정 되어 있으면 리턴 한다. 목표값이 아니면 아래로 내려서 설정값을 변경 한다.)
					if (lpStep->m_fTref == pMainDoc->m_sChamData_1.fRefTemper || lpStep->m_fTref == 0.0f)
					{
						if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500||
							pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
						{
							if (lpStep->m_fHref == pMainDoc->m_sChamData_1.fRefHumidity || lpStep->m_fHref == 0)
							{
								return FALSE;
							}
						}
						else
						{
							return FALSE;	
						}	
					}

					//ljb 20180219 add 온도 및 습도 변경.
					if(lpStep->m_fTref != 0.0f)	pCtrlOven->SetTemperature(0,nOvenID, lpStep->m_fTref);								
					if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500||
						pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							pCtrlOven->SetHumidity(0,nOvenID, lpStep->m_fHref);								
						}
					}

					return TRUE;
				}

				// 여기서 종료------------------------------------------------------------------------------------------
				////////////////////////////////////////////////////////////////////////////////////////////////////////

				//습도 조건 추가. //ksj 20160310
				//if ((lpStep->m_fTref == fSetOvenTempValue && lpStep->m_fHref == fSetOvenHumValue) ||
				//	(lpStep->m_fTref == fSetOvenTempValue && lpStep->m_fHref == 0.0f) ||
				//	(lpStep->m_fTref == 0.0f && lpStep->m_fHref == fSetOvenHumValue) ||
				//	(lpStep->m_fTref == 0.0f && lpStep->m_fHref == 0.0f))
				//{
				//	return FALSE;
				//}

				TRACE("오븐셋팅 (온도: %.2f / 습도:%.2f)\r\n",lpStep->m_fTref, lpStep->m_fHref );
				if (lpStep->m_bUseChamberProg)
				{
					//챔버 Prog 모드로 운영
					//1. 챔버 STOP
					if (pCtrlOven->SendStopCmd(0,nOvenID) == FALSE)
					{
						//strTemp = "챔버 동작 중단 명령 실패(REST)!!!";
						strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg13","CTSMonPro_DOC");//&&
						pMainDoc->WriteSysLog(strTemp);
					}
					//2.현재 챔버 모드 확인
					if (pCtrlOven->GetRunWorkMode(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())))		// FALSE = PROG 모드, TRUE = FIX 모드
					{
						//챔버 Fix 모드 ->	Prog Mode 변환
						if (pCtrlOven->SetFixMode(0,nOvenID,FALSE) == FALSE)
						{
							//strTemp = "챔버 PROG 모드로 변환 실패(REST)!!!";
							strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg14","CTSMonPro_DOC");//&&
							pMainDoc->WriteSysLog(strTemp);
						}
					}
					//3. Pattern 99번에 Segment 1에 설정
					TRACE("STEP END TIME = %f\n",lpStep->m_fEndTime);
					if (pCtrlOven->SetPatternSegNo(0,nOvenID,99,1,lpStep->m_fTref,10.0,lpStep->m_fEndTime))	//시간은 분으로 입력해야 함
					{
						strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg5","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);//strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
					}
					else
					{
						strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg6","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);//strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
					}
					pCh->WriteLog(strTemp);
					pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
					//4. 챔버 Run
					if (pCtrlOven->SendRunCmd(0,nOvenID) == FALSE)
					{
						strTemp = Fun_FindMsg("CheckOvenTemp_Line1_msg7","CTSMonPro_DOC");//strTemp = "챔버 동작 실행 명령 실패(REST)!!!";
						pMainDoc->WriteSysLog(strTemp);
					}
				}
				else
				{
					//챔버 Fix 모드로 운영
					//1.현재 챔버 모드 확인
					if (pCtrlOven->GetRunWorkMode(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) == FALSE)		// FALSE = PROG 모드, TRUE = FIX 모드
					{
						//챔버 Prog 모드
						//1. 챔버 STOP
						if (pCtrlOven->SendStopCmd(0,nOvenID) == FALSE)
						{
							strTemp = Fun_FindMsg("CheckOvenTemp_Line1_msg8","CTSMonPro_DOC");	//strTemp = "Line 1 챔버 동작 중단 명령 실패(REST)!!!";
							pMainDoc->WriteSysLog(strTemp);
						}
						//2.FIX 모드로 전환
						if (pCtrlOven->SetFixMode(0,nOvenID,TRUE) == FALSE)
						{
							strTemp = Fun_FindMsg("CheckOvenTemp_Line1_msg9","CTSMonPro_DOC");//strTemp = "Line 1 챔버 FIX 모드로 변환 실패!!!";
							pMainDoc->WriteSysLog(strTemp);
						}
						//3.챔버 RUN
						if (pCtrlOven->SendRunCmd(0,nOvenID) == FALSE)
						{
							strTemp = Fun_FindMsg("CheckOvenTemp_Line1_msg10","CTSMonPro_DOC");//strTemp = "Line 1 챔버 동작 실행 명령 실패!!!";
							pMainDoc->WriteSysLog(strTemp);
						}
					}

					//FIX 모드 챔버 온도 설정
					if(lpStep->m_fTref != 0.0f)
					{
						if(MAKE2LONG(lpStep->m_fTref*100.0f) != MAKE2LONG(pCtrlOven->GetRefTemperature(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
						{	
							nRtn = TRUE;
							if(pCtrlOven->SetTemperature(0,nOvenID, lpStep->m_fTref))
							{
								strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg11","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));//strTemp.Format("챔버%d 온도 설정 변경 성공 Step %d =>%.2f℃/%.2f℃ 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));
							}
							else
							{
								strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg12","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));//strTemp.Format("챔버%d 온도 설정 변경 실패 !!! Step %d =>%.2f℃/%.2f℃ 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));
							}
							//WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
							pCh->WriteLog(strTemp);
						}

						//Oven 소속된 모든 Channel에 온도 변경 Event 기록  
						strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg13","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1,  lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));//strTemp.Format("챔버%d 온도 설정 변경 Step %d =>%.2f℃/%.2f℃ 변경 완료", nOvenID+1, lpStep->m_StepIndex+1,  lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));
						//WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);//$1013							
						//strTemp.Format("챔버(%d) 현재 SV %.2f℃ 에서 설정 SV %.2f℃ 로 변경 완료 (Step %d 설정 온도는 %.2f℃) ", nOvenID+1, pCtrlOven->GetRefTemperature(nOvenID), lpStep->m_fTref, lpStep->m_StepIndex+1,lpStep->m_fTref);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg15","CTSMonPro_DOC"), nOvenID+1, pCtrlOven->GetRefTemperature(nOvenID), lpStep->m_fTref, lpStep->m_StepIndex+1,lpStep->m_fTref);//&&
						//lpChannel->WriteLog(strTemp);

						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1)// && lpChannel->GetState() == PS_STATE_RUN)// && lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					}

					//온도 변화 Rate 기록 
					if(lpStep->m_fTrate != 0.0f)		//0.0f이면 이전 상태 유지 
					{	
						nRtn = TRUE;
						if(pCtrlOven->SetTSlop(0,nOvenID, lpStep->m_fTrate))
						{
							//strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg16","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);//&&
						}
						else
						{
							//strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg17","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);//&&
						}							
						pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);

						//Oven에 소속된 모든 Channel에 온도 변경 Event 기록  
						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					}

					//습도 Ref 설정 
					if(lpStep->m_fHref != 0.0f) 
					{

						if(MAKE2LONG(lpStep->m_fHref*100.0f) != MAKE2LONG(pCtrlOven->GetRefHumidity(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
						{
							nRtn = TRUE;
							if(pCtrlOven->SetHumidity(0,nOvenID, lpStep->m_fHref))
							{
								//strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pCtrlOven->GetRefHumidity(nOvenID));
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg18","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pCtrlOven->GetRefHumidity(nOvenID));//&&
							}
							else
							{
								//strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pCtrlOven->GetRefHumidity(nOvenID));
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg19","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pCtrlOven->GetRefHumidity(nOvenID));//&&
							}
							pMainDoc->WriteSysLog(strTemp);
						}

						//Ovne에 소속된 모든 Channel에 습도 변경 Event 기록  
						//strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 변경 완료", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pCtrlOven->GetRefHumidity(nOvenID));
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg20","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pCtrlOven->GetRefHumidity(nOvenID));//&&
						pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);	
						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					}

					//습도 Slop 설정 
					if(lpStep->m_fHrate != 0.0f)		//0.0f이면 이전 상태 유지 
					{	
						nRtn = TRUE;
						//((CMainFrame *)AfxGetMainWnd())->SetOvenHSlop(a, lpStep->m_fHrate);	
						if(pCtrlOven->SetHSlop(0,nOvenID, lpStep->m_fHrate))
						{
							//strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg21","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);//&&
						}
						else
						{
							//strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line1_msg22","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);//&&
						}
						pMainDoc->WriteSysLog(strTemp);							

						//Ovne에 소속된 모든 Channel에 온도 변경 Event 기록  
						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					} //end if -> //습도 Slop 설정
				}//end if (lpStep->m_bUseChamberProg) else
				//} //end if -> if(lpStep == NULL ) else
			} //end if -> if(lpSchedule)
			else
			{
				//Schedule == NULL
				strTemp.Format(Fun_FindMsg("CheckOvenTemp_Line1_msg14","CTSMonPro_DOC"), nOvenID+1);//strTemp.Format("챔버%d 연동 lpSchedule Error  !!!", nOvenID+1);
				pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			}
		}//end if (nReadyNextChamberCount > 0)
	}
	return TRUE;
}

BOOL CChamberCtrl::CheckOven_Line2(int nOvenID)
{
	m_nChamberWriteCount2++;	//3초 타이머

	CCTSMonProDoc *pMainDoc = mainView->GetDocument();
	if(!pMainDoc)  return 0;

	COvenCtrl* pCtrlOven = &(pMainDoc->m_ChamberCtrl.m_ctrlOven2);
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	//ljb 201012 챔버 데이터 PSERVER.DLL로 전송
	CString strTemp;
	ZeroMemory(&pMainDoc->m_sChamData_2,sizeof(SFT_CHAMBER_VALUE));

	strTemp.Format("%.1f",pCtrlOven->GetCurTemperature(nOvenID));
	pMainDoc->m_sChamData_2.fCurTemper = atof(strTemp);
	pMainDoc->m_sChamData_2.fRefTemper = pCtrlOven->GetRefTemperature(nOvenID); // 오븐의 설정 온도 데이터를 채널에 저장

	if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 
		|| pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 
		|| pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500
		|| pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
	{
		pMainDoc->m_sChamData_2.fCurHumidity = pCtrlOven->GetCurHumidity(nOvenID); // 오븐의 현재 습도 데이터를 채널에 저장
		pMainDoc->m_sChamData_2.fRefHumidity = pCtrlOven->GetRefHumidity(nOvenID); // 오븐의 설정 습도 데이터를 채널에 저장
	}	
	//TRACE("Oven2 -> CurTemp = %.3f, RefTemp = %.3f", pMainDoc->m_sChamData_2.fCurTemper,pMainDoc->m_sChamData_2.fRefTemper);

	int mdIndex =1;
	//챔버설정에 할당된 채널에 모두 보내야 함. (추 후 수정)
	//::SFTSetChamberData(mdIndex,1,&pMainDoc->m_sChamData_2);		//챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 2 //yulee 20190304


	if(pMainDoc->m_nChamberOperationType == 0)		//m_nChamberOperationType = 0 이면 한 챔버에 ch이 모두 있음.			
	{	
		//모든 모듈 - 채널에 값을 넣어 줘야 함.
		::SFTSetChamberData(mdIndex,1,&pMainDoc->m_sChamData_2); //챔버 연동이 아니어도 온도값 저장을 위해 보냄.  ch 2
	}
	//////////////////////////////////////////////////////////////////////////

	pMainDoc->m_iOvenStartOption_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Start Chamber Option", 0);
	pMainDoc->m_uiOvenCheckTime_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Fix Check Time", 0);
	pMainDoc->m_uiOvenCheckFixTime_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Schedule Check Time", 0);

	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG4, "Fix Temp","25.0");
	pMainDoc->m_fOvenStartFixTemp_2 = atof(strTemp);

	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG4, "Delta Fix Temp","1");
	pMainDoc->m_fOvenDeltaFixTemp_2 = atof(strTemp);

	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG4, "Delta Temp","1");
	pMainDoc->m_fOvenDeltaTemp_2 = atof(strTemp);

	pMainDoc->m_uiOvenPatternNum_2 = AfxGetApp()->GetProfileInt(OVEN_SETTING, "Pattern No", 1);
	//m_iOvenContinueType_2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Chamber Continue Type", 0);		//ljb 20180226 add


	if (pMainDoc->m_iOvenStartOption_2 < 1) return FALSE;		//ljb 챔버 연동 안함
	if (pMainDoc->m_iOvenStartOption_2 > 3) return FALSE;		//ljb 챔버 연동 안함

	//ljb Oven 연동
	int nLinkChamberCount=0;		//ljb 챔버 연동 채널 갯수
	int nReadyNextChamberCount=0;	//ljb 챔버 연동 대기 채널 갯수	(nLinkChamberCount 와 같으면 다음 명령 전송)
	CDWordArray adwChArray;			//ljb 챔버 연동 모듈,채널 갯수
	BOOL	bOverTempSpec(FALSE);

	CCyclerModule *pMD; 
	CCyclerChannel *pCh;
	int nChannelIndex=0;	//ljb
	int nMinStepNo = 0x7fffffff;
	int nTotalCh = 0; //3;					//기본 1채널만 해당됨

 	int nDevTotalCh = 0;
 	nDevTotalCh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC , "Max Parallel Count", 2); //yulee 20190910
	int ch = 0; //yulee 20190708
	if(g_AppInfo.iPType == TRUE) //yulee 20190708 //yulee 20190712_1 edit
	{
		ch = 1;
		nTotalCh = 2;
	}
	else
	{
 		if(nDevTotalCh == 2)
 		{ 
 			ch = 1;
 			nTotalCh = 2;
 		}
 		else if(nDevTotalCh == 4)
 		{
 			ch = 2;
 			nTotalCh = 4;
		}
	}
	//전체 모듈에 대해 챔버연동 대기 채널 검색
	//for(mdIndex =1; mdIndex <= GetInstallModuleCount(); mdIndex++)
	//{
	pMD = pMainDoc->GetModuleInfo(mdIndex);
	if(pMD)
	{
		//Line Off
		if(pMD->GetState() == PS_STATE_LINE_OFF) return FALSE;

		if(pMainDoc->m_nChamberOperationType == 0)	//nChamberOperationType = AfxGetApp()->GetProfileInt(OVEN_SETTING, "ChamberOperationType", 1);
		{
			return FALSE;
			//nTotalCh = pMD->GetTotalChannel();	////1챔버에 모든 채널이 있음
		}



		for(ch; ch <= nTotalCh; ch++)		//2번 채널 하나만 비교 한다.
			/*for(int ch =1; ch <= pMD->GetTotalChannel(); ch++)		//2번 채널 하나만 비교 한다.*/
		{
			::SFTSetChamberData(mdIndex,ch,&(pMainDoc->m_sChamData_2));		//ljb 20151120 add	채널별로 챔버 데이터 전송
			pCh = pMD->GetChannelInfo(ch);
			if(pCh)
			{
				if(nOvenID == pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
				{					
					//Idle인 상태
					if( pCh->GetState() == PS_STATE_IDLE || pCh->GetState() == PS_STATE_STANDBY ||
						pCh->GetState() == PS_STATE_END	|| pCh->GetState() == PS_STATE_READY )	
					{								
						//continue;
					}
					else if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
					{
						//ljb 20120627 병렬모드 일때도 skip
						TRACE("병렬모드 skip \n");
						//continue;
					}
					else if (pCh->GetState() == PS_STATE_MAINTENANCE)
					{
						//continue;
					}

					else if((pCh->GetChamberUsing() == TRUE) && ( pCh->GetStepType()  == PS_STEP_EXT_CAN)) 
					{//20180704 yulee External CAN으로 챔버 제어
						// 챔버 사용 중, External CAN 스텝이면 다음과 같은 동작을 한다.
						// CAN Ddata 중 코드 1700으로 등록된 값(Ref Temp.) 가져온다. 
						// 챔버의 먼저 설정된 값(SP)이 설정 할 온도(RT)와 같으면 Skip
						// 현재 챔버 온도(CT)와 설정 온도(RT)를 비교한다. - 비슷하면 skip 
						// SP와 RT가 다르면 RT가 0이 아닐 경우 챔버 온도 설정(pCtrlOven->SetTemperature)을 한다. 

						//CAN 데이터에서 값 가져오기 
						SFT_CAN_DATA * pCanData = pCh->GetCanData();
						if(pCanData == NULL) return FALSE;

						float fCanTref = 0.0;
						CString strItem = ("");
						for(int j = 0; j < pCh->GetCanCount(); j++)
						{
							if(pCanData[j].function_division == 1700)
							{
								if(pCanData[j].data_type == 0 || pCanData[j].data_type == 1 || pCanData[j].data_type == 2)
								{
									strItem.Format("%.6f", pCanData[j].canVal.fVal[0]);
									if (pCanData[j].function_division > 100 && pCanData[j].function_division < 150)
										strItem.Format("%02x%02x%02x%02x", (BYTE)pCanData[j].canVal.strVal[0], (BYTE)pCanData[j].canVal.strVal[1], (BYTE)pCanData[j].canVal.strVal[2], (BYTE)pCanData[j].canVal.strVal[3]);

									strItem.TrimRight("0");
									strItem.TrimRight(".");
								}
								else if(pCanData[j].data_type == 3){
									strItem.Format("%s", pCanData[j].canVal.strVal);
								}
								else if (pCanData[j].data_type == 4){
									strItem.Format("%x", (UINT)pCanData[j].canVal.fVal[0]);	
								}
								fCanTref = atof(strItem);
								break;
							}
						}

						if(((strItem.IsEmpty() == FALSE) || (strItem != (""))) && (fCanTref != 0.0))
						{ 
							float fChamberSPTemp = 0.0;
							fChamberSPTemp = pCtrlOven->GetRefTemperature(nOvenID);
							if(fChamberSPTemp != fCanTref)
							{
								if ((pMainDoc->m_sChamData_2.fCurTemper < (fCanTref - 0.2)) || (pMainDoc->m_sChamData_2.fCurTemper > (fCanTref + 0.2)))
								{
									if(pCtrlOven->SetTemperature(0,nOvenID, fCanTref))
									{
										//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) \r\n",pCh->GetStepNo(), fCanTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper );
										strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg1","CTSMonPro_DOC"),pCh->GetStepNo(), fCanTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper );//&&
										pCh->WriteLog(strTemp);
									}
									else
										//{	strTemp.Format("STEP %d 챔버 온도설정 명령 전송실패 \r\n",pCh->GetStepNo());
									{	strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg2","CTSMonPro_DOC"),pCh->GetStepNo());//&&
									pCh->WriteLog(strTemp);
									}
								}
							}
						}
					}

					else
					{
						//Fix 모드에서 온도 범위 초과 체크
						if (pMainDoc->m_iOvenStartOption_2 == 1 && pMainDoc->m_uiOvenCheckFixTime_2 != 0)
						{
							if (pCtrlOven->GetRun(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetTotTime() > pMainDoc->m_uiOvenCheckFixTime_2)//lmh 201111 chk 챔버 연동 가능하게 변경해야함
							{
								if ((fabs(pMainDoc->m_sChamData_2.fRefTemper - pMainDoc->m_sChamData_2.fCurTemper)) > pMainDoc->m_fOvenDeltaFixTemp_2)
								{
									bOverTempSpec = TRUE;//$1013
									//strTemp.Format("모듈(%s) 채널(%d) => Fix Mode 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,pMainDoc->m_sChamData_2.fRefTemper,pMainDoc->m_sChamData_2.fCurTemper,m_fOvenDeltaTemp_2);
									strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg3","CTSMonPro_DOC"),pMainDoc->GetModuleName(pMD->GetModuleID()),ch+1,pMainDoc->m_sChamData_2.fRefTemper,pMainDoc->m_sChamData_2.fCurTemper,pMainDoc->m_fOvenDeltaTemp_2);//&&
									pCh->WriteLog(strTemp);
									pMainDoc->WriteSysEventLog(strTemp);
								}
							}
							if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
							{
								adwChArray.Add(MAKELONG(ch, mdIndex));									
							}
							else
							{
							}
						}

						//스케쥴 연동
						if (pMainDoc->m_iOvenStartOption_2 == 3 && pCh->GetChamberUsing())
						{
							//진행 시간과 온도 편차가 설정 값보다 크면 챔버 정지 및 충방전기 일시 정지, 챔버가 Prog 모드일 경우는?
							if (pMainDoc->m_uiOvenCheckTime_2 != 0)
							{
								if (pCtrlOven->GetRun(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) && pCh->GetStepTime() > pMainDoc->m_uiOvenCheckTime_2)
								{
									if ((fabs(pMainDoc->m_sChamData_2.fRefTemper - pMainDoc->m_sChamData_2.fCurTemper)) > pMainDoc->m_fOvenDeltaTemp_2)
									{
										bOverTempSpec = TRUE;//$1013
										//strTemp.Format("모듈(%s) 채널(%d) => 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",GetModuleName(pMD->GetModuleID()),ch+1,pMainDoc->m_sChamData_2.fRefTemper,pMainDoc->m_sChamData_2.fCurTemper,m_fOvenDeltaTemp_2);
										strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg4","CTSMonPro_DOC"),pMainDoc->GetModuleName(pMD->GetModuleID()),ch+1,pMainDoc->m_sChamData_2.fRefTemper,pMainDoc->m_sChamData_2.fCurTemper,pMainDoc->m_fOvenDeltaTemp_2);//&&
										pCh->WriteLog(strTemp);
										pMainDoc->WriteSysEventLog(strTemp);
									}
								}
							}

							if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)
							{
								nLinkChamberCount ++;
								adwChArray.Add(MAKELONG(ch, mdIndex));
							}
							else
							{

							}
							if (m_nChamberWriteCount2 > 20)
							{
								strTemp.Format("nLinkChamberCount %d, CellCode %d, Ch State %d ,Step No,%d, Step Type, %d,chamber Temp, PV, %.2f, SV, %.2f,chamber Humi, PV, %.2f, SV, %.2f",
									nLinkChamberCount, pCh->GetCellCode(),pCh->GetState(), pCh->GetStepNo(),pCh->GetStepType(),pMainDoc->m_sChamData_2.fCurTemper,pMainDoc->m_sChamData_2.fRefTemper,pMainDoc->m_sChamData_2.fCurHumidity,pMainDoc->m_sChamData_2.fRefHumidity);
								pCh->WriteChamberLog(strTemp);
								m_nChamberWriteCount2 = 0;
							}
						}

					}

					if(pCh->IsParallel() && pCh->m_bMaster == FALSE)   
					{
						//ljb 20120627 병렬모드 일때도 skip
						TRACE("병렬모드 skip \n");
						//continue;
					}
					else if (pCh->GetState() == PS_STATE_PAUSE && pCh->GetCellCode() == PS_STATE_PAUSE_CHAMBER_READY)	// 87 챔버연동 대기
					{
						nReadyNextChamberCount ++;
						nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호
					}
				}//end if(nOvenID == pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) //lmh chk
			}//end if (pCh)
		}//end for (ch)
	}//end if (pMD)
	//}//end for (mdIndex)

	//챔버연동으로 Pause 된 채널이 없으면 리턴
	if (pMainDoc->m_iOvenStartOption_2 == 3 && nLinkChamberCount == 0 ) return FALSE;

	POSITION pos = pMainDoc->GetFirstViewPosition();
	CCTSMonProView *pView = (CCTSMonProView *)pMainDoc->GetNextView(pos);
	//일시 정지 및 챔버 STOP
	if (bOverTempSpec)
	{
		//★★ ljb 2010326  ★★★★★★★★ //챔버 연동된 채널에 Contiue 전송
		pView->SendCmdToModule(adwChArray, SFT_CMD_PAUSE);
		//1. 챔버 STOP
		if (pCtrlOven->SendStopCmd(0,nOvenID) == FALSE)
		{
			//strTemp = "챔버 2 챔버 동작 중단 명령 실패(REST)!!!";//$1013
			strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg5","CTSMonPro_DOC");//&&
			pMainDoc->WriteSysLog(strTemp);
		}//$1013
		//strTemp.Format("챔버 2 설정온도(%.1f)와 현재 온도(%.1f)차가 지정한 온도(%.1f)차 보다 큽니다.",pMainDoc->m_sChamData_2.fRefTemper,pMainDoc->m_sChamData_2.fCurTemper,m_fOvenDeltaTemp_2);
		strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg6","CTSMonPro_DOC"),pMainDoc->m_sChamData_2.fRefTemper,pMainDoc->m_sChamData_2.fCurTemper,pMainDoc->m_fOvenDeltaTemp_2);//&&
		pMainDoc->WriteSysEventLog(strTemp);
		//AfxMessageBox(strTemp);
	}

	//스케줄 연동이 아니면 리턴 한다.
	if (pMainDoc->m_iOvenStartOption_2 != 3 ) return FALSE;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//챔버 연동 명령어 전송 부분
	//-------------------------------------------------------
	// 	CCyclerModule *pMD;		//상위에 선언되어 있음.
	// 	CCyclerChannel *pCh;	//상위에 선언되어 있음.

	int nUsingChamberCh = 0, nRunStepNo = -1;

	DWORD dwData;
	BOOL nRtn = FALSE;	
	CString RegName;
	//BOOL bChamperContinue(FALSE);	//챔버연동 대기 플래그 값 

	pCh = NULL;
	nUsingChamberCh = adwChArray.GetSize();
	if (nReadyNextChamberCount > 0)
	{
		dwData = adwChArray.GetAt(0);
		pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));

		if(pCh)
		{
			nMinStepNo = pCh->GetStepNo();	//현재 STEP 번호

			CScheduleData *lpSchedule = pCh->GetScheduleData();		//ljb 20180219 메모리로 업로드 하고 읽어 오는 구조로 변경 해야 할듯 함.
			if(lpSchedule)
			{							
				CStep *lpStep = lpSchedule->GetStepData(nMinStepNo -1);		//ljb 20140108 add 현재스텝 가져오기

				//yulee 20181113
				RegName.Format("OvenContinue_%d",pCh->GetChannelIndex() + 1);	
				BOOL bChamberContinue;
				bChamberContinue = AfxGetApp()->GetProfileInt(OVEN_SETTING, RegName, 0);


				if(bChamberContinue == FALSE)
				{											
					if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
					{//$1013
						//strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg7","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
						pCh->WriteLog(strTemp);
					}

					if(lpStep->m_fTref != 0.0f)
					{ 
						pCtrlOven->SetTemperature(0,nOvenID, lpStep->m_fTref);//$1013						
						//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 현재설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper,bChamberContinue );
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg8","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper,bChamberContinue );//&&
						pCh->WriteLog(strTemp);
					}

					if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500||
						pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							pCtrlOven->SetHumidity(0,nOvenID, lpStep->m_fHref);//$1013								
							//strTemp.Format("STEP %d 챔버 설정습도 %.2f (챔버 현재습도: %.2f / 챔버 현재설정습도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fHref, pMainDoc->m_sChamData_2.fCurHumidity, pMainDoc->m_sChamData_2.fRefHumidity,bChamberContinue );
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg9","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fHref, pMainDoc->m_sChamData_2.fCurHumidity, pMainDoc->m_sChamData_2.fRefHumidity,bChamberContinue );//&&
							pCh->WriteLog(strTemp);
						}
					}

					//strTemp.Format("%.1f",pCtrlOven->GetRefTemperature(nOvenID));		//ljb 20170719 add
					//fCurOvenSetTemp = atof(strTemp);
					//if (lpStep->m_fTref == fCurOvenSetTemp)
					//if ( ((lpStep->m_fTref - 0.2) < fCurOvenSetTemp) && (fCurOvenSetTemp < (lpStep->m_fTref + 0.2)))		//ljb 20180212 수정 float 변환으로 인한 설정값 이상을 의심
					//{
					//	pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	
					//	strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) 챔버연동 대기 %d \r\n",lpStep->m_StepIndex, lpStep->m_fTref, m_sChamData.fCurTemper, m_sChamData.fRefTemper,bChamperContinue );
					//	lpChannel->WriteLog(strTemp);
					//	return TRUE;
					//}
					//else
					//	return FALSE;

					return TRUE;
				}//end if(m_iOvenContinueType_2 == FALSE)

				if(lpStep->m_type == PS_STEP_LOOP)
				{//$1013
					pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE);	//ljb 20130710 add
					//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) LOOP \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper );
					strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg10","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper );//&&
					pCh->WriteLog(strTemp);
					return TRUE;
				}
				
				//ljb 20180219	// 챔버 온도 대기시 온도 비교 하는 부분
				if ((pMainDoc->m_sChamData_2.fCurTemper >= (lpStep->m_fTref - 0.2)) && (pMainDoc->m_sChamData_2.fCurTemper <= (lpStep->m_fTref + 0.2)))
				{
					if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500||
						pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
					{
						if ((pMainDoc->m_sChamData_2.fCurHumidity >= (lpStep->m_fHref - 0.2)) && (pMainDoc->m_sChamData_2.fCurHumidity <= (lpStep->m_fHref + 0.2)))
						{
							if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
							{//$1013
								//strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg11","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
								pCh->WriteLog(strTemp);

								//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 1
								RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
								BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 1);
								strTemp.Format(" %s registry %s Write 1", OVEN_SETTING, RegName);
								pCh->WriteLog(strTemp);
								if(bRetWriteProfile != TRUE)
									return FALSE;	
							}
							else
							{
								//strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg12","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
								pCh->WriteLog(strTemp);

								//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 0
								RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
								BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 0);
								strTemp.Format(" %s registry %s Write 0", OVEN_SETTING, RegName);
								pCh->WriteLog(strTemp);
								if(bRetWriteProfile != TRUE)
									return FALSE;	
							}
							//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg13","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper);//&&
							pCh->WriteLog(strTemp);
							return TRUE;
						}
					}
					else
					{
						if (pView->SendCmdToModule(adwChArray, SFT_CMD_CHAMBER_CONTINUE))
						{//$1013
							//strTemp.Format("STEP %d 챔버 Continue 명령 전송 성공 \r\n",lpStep->m_StepIndex);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg14","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
							pCh->WriteLog(strTemp);

							//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 1
							RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
							BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 1);
							strTemp.Format(" %s registry %s Write 1", OVEN_SETTING, RegName);
							pCh->WriteLog(strTemp);
							if(bRetWriteProfile != TRUE)
								return FALSE;	
						}
						else
						{
							//strTemp.Format("STEP %d 챔버 Continue 명령 전송 실패 \r\n",lpStep->m_StepIndex);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg15","CTSMonPro_DOC"),lpStep->m_StepIndex);//&&
							pCh->WriteLog(strTemp);

							//yulee 20190714 SFT_CMD_CHAMBER_CONTINUE 전송 후 레지스트리 쓰기 0
							RegName.Format("OvenContinueSend_ch%d",pCh->GetChannelIndex() + 1);
							BOOL bRetWriteProfile = AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, 0);
							strTemp.Format(" %s registry %s Write 0", OVEN_SETTING, RegName);
							pCh->WriteLog(strTemp);
							if(bRetWriteProfile != TRUE)
								return FALSE;	
						}

						//strTemp.Format("STEP %d 챔버 설정온도 %.2f (챔버 현재온도: %.2f / 챔버 설정온도:%.2f) \r\n",lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg16","CTSMonPro_DOC"),lpStep->m_StepIndex, lpStep->m_fTref, pMainDoc->m_sChamData_2.fCurTemper, pMainDoc->m_sChamData_2.fRefTemper);//&&
						pCh->WriteLog(strTemp);
						return TRUE;
					}
				}

				if(lpStep == NULL || lpStep->m_type == PS_STEP_END)
				{
					//챔버 STOP
					if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AutoOvenStop", FALSE))
						pMainDoc->OvenStopStateAndSendStopCmd(2);			//챔버 2라인 
					return TRUE;
				}
				else
				{
					//ljb 설정 온도가 맞는지 비교 하는 부분 (목표 값으로 설정 되어 있으면 리턴 한다. 목표값이 아니면 아래로 내려서 설정값을 변경 한다.)
					if (lpStep->m_fTref == pMainDoc->m_sChamData_2.fRefTemper || lpStep->m_fTref == 0.0f)
					{
						if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500||
							pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
						{
							if (lpStep->m_fHref == pMainDoc->m_sChamData_2.fRefHumidity || lpStep->m_fHref == 0)
							{
								return FALSE;
							}
						}
						else
						{
							return FALSE;	
						}

					}


					//ljb 20180219 add 온도 및 습도 변경.
					if(lpStep->m_fTref != 0.0f)	pCtrlOven->SetTemperature(0,nOvenID, lpStep->m_fTref);								
					if (pCtrlOven->GetOvenModelType() == CHAMBER_TEMI880 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2300 || pCtrlOven->GetOvenModelType() == CHAMBER_TEMI2500||
						pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1200)
					{
						if(lpStep->m_fHref != 0.0f)	//ksj 20160310 추가
						{ 
							pCtrlOven->SetHumidity(0,nOvenID, lpStep->m_fHref);								
						}
					}

					return TRUE;
				}
				
				// 여기서 종료------------------------------------------------------------------------------------------
				////////////////////////////////////////////////////////////////////////////////////////////////////////

				TRACE("오븐셋팅 (온도: %.2f / 습도:%.2f)\r\n",lpStep->m_fTref, lpStep->m_fHref );
				if (lpStep->m_bUseChamberProg)
				{
					//챔버 Prog 모드로 운영
					//1. 챔버 STOP
					if (pCtrlOven->SendStopCmd(0,nOvenID) == FALSE)
					{
						//strTemp = "챔버 동작 중단 명령 실패(REST)!!!";//$1013
						strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg17","CTSMonPro_DOC");//$1013
						pMainDoc->WriteSysLog(strTemp);
					}
					//2.현재 챔버 모드 확인
					if (pCtrlOven->GetRunWorkMode(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())))		// FALSE = PROG 모드, TRUE = FIX 모드
					{
						//챔버 Fix 모드 ->	Prog Mode 변환
						if (pCtrlOven->SetFixMode(0,nOvenID,FALSE) == FALSE)
						{
							//strTemp = "챔버 PROG 모드로 변환 실패(REST)!!!";//$1013
							strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg18","CTSMonPro_DOC");//&&
							pMainDoc->WriteSysLog(strTemp);
						}
					}
					//3. Pattern 99번에 Segment 1에 설정
					TRACE("STEP END TIME = %f\n",lpStep->m_fEndTime);
					if (pCtrlOven->SetPatternSegNo(0,nOvenID,99,1,lpStep->m_fTref,10.0,lpStep->m_fEndTime))	//시간은 분으로 입력해야 함
					{//$1013
						//strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg19","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);//&&
					}
					else
					{//$1013
						//strTemp.Format("챔버%d Pattern 설정 변경 Step %d =>목표온도 %.2f℃, Time : %d초 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg20","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, (int)lpStep->m_fEndTime);//&&
					}
					pCh->WriteLog(strTemp);
					pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
					//4. 챔버 Run
					if (pCtrlOven->SendRunCmd(0,nOvenID) == FALSE)
					{
						//strTemp = "Line 2 챔버 동작 실행 명령 실패(REST)!!!";
						strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg21","CTSMonPro_DOC");//&&
						pMainDoc->WriteSysLog(strTemp);
					}
				}
				else
				{
					//챔버 Fix 모드로 운영
					//1.현재 챔버 모드 확인
					if (pCtrlOven->GetRunWorkMode(pCtrlOven->GetOvenIndex(pMD->GetModuleID(),pCh->GetChannelIndex())) == FALSE)		// FALSE = PROG 모드, TRUE = FIX 모드
					{
						//챔버 Prog 모드
						//1. 챔버 STOP
						if (pCtrlOven->SendStopCmd(0,nOvenID) == FALSE)
						{
							//strTemp = "Line 2 챔버 동작 중단 명령 실패(REST)!!!";
							strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg22","CTSMonPro_DOC");//&&
							pMainDoc->WriteSysLog(strTemp);
						}
						//2.FIX 모드로 전환
						if (pCtrlOven->SetFixMode(0,nOvenID,TRUE) == FALSE)
						{
							//strTemp = "Line 2 챔버 FIX 모드로 변환 실패!!!";
							strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg23","CTSMonPro_DOC");//&&
							pMainDoc->WriteSysLog(strTemp);
						}
						//3.챔버 RUN
						if (pCtrlOven->SendRunCmd(0,nOvenID) == FALSE)
						{
							//strTemp = "Line 2 챔버 동작 실행 명령 실패!!!";
							strTemp = Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg24","CTSMonPro_DOC");//&&
							pMainDoc->WriteSysLog(strTemp);
						}
					}

					//FIX 모드 챔버 온도 설정
					if(lpStep->m_fTref != 0.0f)
					{
						if(MAKE2LONG(lpStep->m_fTref*100.0f) != MAKE2LONG(pCtrlOven->GetRefTemperature(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
						{	
							nRtn = TRUE;
							if(pCtrlOven->SetTemperature(0,nOvenID, lpStep->m_fTref))
							{
								//strTemp.Format("Line 2 챔버%d 온도 설정 변경 성공 Step %d =>%.2f℃/%.2f℃ 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg25","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));//&&
							}
							else
							{
								//strTemp.Format("Line 2 챔버%d 온도 설정 변경 실패 !!! Step %d =>%.2f℃/%.2f℃ 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg26","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));//&&
							}
							pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
							//strTemp.Format("Line 2 챔버 온도 설정 변경 %d/%d", MAKE2LONG(lpStep->m_fTref*100.0f), MAKE2LONG(pCtrlOven->GetRefTemperature(a)*100.0f));
						}

						//Oven 소속된 모든 Channel에 온도 변경 Event 기록  
						//strTemp.Format("챔버(%d) 현재  온도 설정 변경 Step %d 설정온도 = %.2f℃ / 현재 설정 온도 %.2f℃ 변경 완료", nOvenID+1, lpStep->m_StepIndex+1,  lpStep->m_fTref, pCtrlOven->GetRefTemperature(nOvenID));
						//WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);		//$1013					
						//strTemp.Format("챔버(%d) 현재 SV %.2f℃ 에서 설정 SV %.2f℃ 로 변경 완료 (Step %d 설정 온도는 %.2f℃) ", nOvenID+1, pCtrlOven->GetRefTemperature(nOvenID), lpStep->m_fTref, lpStep->m_StepIndex+1,lpStep->m_fTref);
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg26","CTSMonPro_DOC"), nOvenID+1, pCtrlOven->GetRefTemperature(nOvenID), lpStep->m_fTref, lpStep->m_StepIndex+1,lpStep->m_fTref);//&&
						//lpChannel->WriteLog(strTemp);

						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1)// && lpChannel->GetState() == PS_STATE_RUN)// && lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					}

					//온도 변화 Rate 기록 
					if(lpStep->m_fTrate != 0.0f)		//0.0f이면 이전 상태 유지 
					{	
						nRtn = TRUE;
						if(pCtrlOven->SetTSlop(0,nOvenID, lpStep->m_fTrate))
						{//$1013
							//strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg27","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);//&&
						}
						else
						{
							//strTemp.Format("챔버%d 온도 변화율 설정 변경 Step %d =>%.2f℃/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg28","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fTrate);//&&
						}							
						pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);

						//Oven에 소속된 모든 Channel에 온도 변경 Event 기록  
						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					}

					//습도 Ref 설정 
					if(lpStep->m_fHref != 0.0f) 
					{

						if(MAKE2LONG(lpStep->m_fHref*100.0f) != MAKE2LONG(pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetRefHumidity(nOvenID)*100.0f))		//0.0f이면 이전 상태 유지 
						{
							nRtn = TRUE;
							if(pMainDoc->m_ChamberCtrl.m_ctrlOven2.SetHumidity(0,nOvenID, lpStep->m_fHref))
							{//$1013
								//strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetRefHumidity(nOvenID));
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg29","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetRefHumidity(nOvenID));//&&
							}
							else
							{
								//strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetRefHumidity(nOvenID));
								strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg30","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetRefHumidity(nOvenID));//&&
							}
							pMainDoc->WriteSysLog(strTemp);
						}

						//Ovne에 소속된 모든 Channel에 습도 변경 Event 기록  
						//strTemp.Format("챔버%d 습도 설정 변경 Step %d =>%.2f%%/%.2f%% 변경 완료", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetRefHumidity(nOvenID));
						strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg31","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHref, pMainDoc->m_ChamberCtrl.m_ctrlOven2.GetRefHumidity(nOvenID));//&&
						pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);	
						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					}

					//습도 Slop 설정 
					if(lpStep->m_fHrate != 0.0f)		//0.0f이면 이전 상태 유지 
					{	
						nRtn = TRUE;
						//((CMainFrame *)AfxGetMainWnd())->SetOvenHSlop(a, lpStep->m_fHrate);	
						if(pMainDoc->m_ChamberCtrl.m_ctrlOven2.SetHSlop(0,nOvenID, lpStep->m_fHrate))
						{//$1013
							//strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 성공", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg32","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);//&&
						}
						else
						{
							//strTemp.Format("챔버%d 습도 변화율 설정 변경 Step %d =>%.2f%%/분 전송 실패!!!", nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);
							strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg33","CTSMonPro_DOC"), nOvenID+1, lpStep->m_StepIndex+1, lpStep->m_fHrate);//&&
						}
						pMainDoc->WriteSysLog(strTemp);							

						//Ovne에 소속된 모든 Channel에 온도 변경 Event 기록  
						for(int b=0; b<adwChArray.GetSize(); b++)
						{
							dwData = adwChArray.GetAt(b);
							pCh = pMainDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
							if(pCh)
							{
								//준 Stop상태(Pause)에 있는 Channel이 있을 수 있으므로 PS_STATE_PAUSE는 제외 한다.
								if(pCh->GetChamberUsing() == 1 && pCh->GetState() == PS_STATE_RUN )//&& lpChannel->GetUseOvenMode())
								{
									pCh->WriteLog(strTemp);
								}
							}
						}
					} //end if -> //습도 Slop 설정
				}//end if (lpStep->m_bUseChamberProg) else
				//} //end if -> if(lpStep == NULL ) else
			} //end if -> if(lpSchedule)
			else
			{
				//Schedule == NULL
				//strTemp.Format("챔버%d 연동 lpSchedule Error  !!!", nOvenID+1);
				strTemp.Format(Fun_FindMsg("CTSMonProDoc_CheckOven_Line2_msg34","CTSMonPro_DOC"), nOvenID+1);//&&
				pMainDoc->WriteSysLog(strTemp, CT_LOG_LEVEL_NORMAL);
			}
		}//end if (nReadyNextChamberCount > 0)
	}
	return TRUE;
}