// AuxDataDlg.cpp : implementation file
//

#include "stdafx.h" 

#include "ctsmonpro.h"
#include "AuxDataDlg.h"

#include "AddAuxDataDlg.h"

#include "LoginManagerDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAuxDataDlg dialog

#define WM_TREE_ITEM_DRAG_DROP			WM_USER+3030		//Message TREE Drag and Drop
#define WM_TREE_ITEM_BEGIN_DRAG			WM_USER+3031		//Message TREE Bigin Drag
CAuxDataDlg::CAuxDataDlg(CCTSMonProDoc * pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CAuxDataDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAuxDataDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAuxDataDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CAuxDataDlg::IDD3):
	(CAuxDataDlg::IDD), pParent )
{
	//{{AFX_DATA_INIT(CAuxDataDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	nCurrentModule = 0;
	IsChanged = FALSE;

	ZeroMemory(&bColor, sizeof(BOOL)*_SFT_MAX_MAPPING_AUX);


}


void CAuxDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAuxDataDlg)
	DDX_Control(pDX, IDC_COMBO_CHLIST, m_CboChList);
	DDX_Control(pDX, IDC_SENSOR_LIST, m_ctrlSensorList);
	DDX_Control(pDX, IDC_AUX_TREE, m_ctrlAuxTree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAuxDataDlg, CDialog)
	//{{AFX_MSG_MAP(CAuxDataDlg)
	ON_BN_CLICKED(IDC_BTN_ADD, OnBtnAdd)
	ON_BN_CLICKED(IDC_BTN_DEL, OnBtnDel)
	ON_NOTIFY(TVN_SELCHANGED, IDC_AUX_TREE, OnSelchangedAuxTree)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, OnButtonApply)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BTN_MODIFY, OnBtnModify)
	ON_NOTIFY ( NM_CUSTOMDRAW, IDC_SENSOR_LIST, OnCustomdrawMyList)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BTN_ALL_APPLY, OnBtnAllApply)
	ON_BN_CLICKED(IDC_BTN_CARBON_APPLY, OnBtnCarbonApply)
	ON_BN_CLICKED(IDC_BTN_LTO_APPLY, OnBtnLtoApply)
	ON_BN_CLICKED(IDC_BTN_ALL_APPLY_THERSENSOR, OnBtnAllApplyTherSensor)
	ON_BN_CLICKED(IDC_BTN_MOVE, OnBtnMove)
	//}}AFX_MSG_MAP
	
	ON_MESSAGE(WM_TREE_ITEM_DRAG_DROP, OnItemDragAndDrop)
	ON_BN_CLICKED(IDC_BTN_INITIALIZE, &CAuxDataDlg::OnBnClickedBtnInitialize)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAuxDataDlg message handlers

void CAuxDataDlg::OnBtnAdd() 
{
	HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();
	CString strAuxName, strData;
	STF_MD_AUX_SET_DATA auxData;
	int nChIndex = 0;

	CString strItemText = m_ctrlAuxTree.GetItemText(hItem);
	
	if(strItemText.Left(2) != "Ch")
	{
		HTREEITEM hTemp = m_ctrlAuxTree.GetParentItem(hItem);
		CString strTemp = m_ctrlAuxTree.GetItemText(hTemp);
		if(strTemp.Left(2) != "Ch")
			return;
		else
		{
			hItem = hTemp;
			strItemText = strTemp;
		}
	}

	nChIndex = atoi(strItemText.Mid(2));


	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex-1);
	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
	{
		AfxMessageBox(Fun_FindMsg("OnBtnAdd_msg","IDD_AUXDATA_DLG"));
		//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		return;
	}

	CAddAuxDataDlg dlg(NULL, pMD);
	dlg.bModify = FALSE;

	dlg.Fun_SetArryAuxDivision(m_uiArryAuxCode);	//ljb 2011222 이재복 //////////
	dlg.Fun_SetArryAuxString(m_strArryAuxName);		//ljb 2011222 이재복 //////////

	dlg.Fun_SetArryAuxThrDivision(m_uiArryAuxThrCode);
	dlg.Fun_SetArryAuxThrString(m_strArryAuxThrName);

	dlg.Fun_SetArryAuxHumiDivision(m_uiArryAuxHumiCode);
	dlg.Fun_SetArryAuxHumiString(m_strArryAuxHumiName);

	if(dlg.DoModal() == IDOK)
	{
		CString strTemp;
//		float fMaxData, fMinData, fEndMax, fEndMin;
		ZeroMemory(&auxData, sizeof(STF_MD_AUX_SET_DATA));
		strAuxName = dlg.m_strSensorName;
		auxData.auxType	= dlg.nSensorType;
		auxData.auxChNo = dlg.nSensorCh;
		sprintf(auxData.szName, strAuxName);

		strTemp.Format("%s %d",GetAuxTypeName(auxData.auxType), auxData.auxChNo);

			float fltTmp;
			fltTmp = 0; //yulee 20181129
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMax, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp = m_pDoc->UnitTrans(dlg.m_fEndMax, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lEndMaxData = atol(strData);
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMin, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp = m_pDoc->UnitTrans(dlg.m_fEndMin, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lEndMinData = atol(strData);
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMAx, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp = m_pDoc->UnitTrans(dlg.m_fSensorMAx, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lMaxData = atol(strData);
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMin, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp = m_pDoc->UnitTrans(dlg.m_fSensorMin, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lMinData = atol(strData);
			/*
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMax, TRUE, auxData.auxType));
			auxData.lEndMaxData = atol(strData);
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMin, TRUE, auxData.auxType));
			auxData.lEndMinData = atol(strData);
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMAx, TRUE, auxData.auxType));
			auxData.lMaxData = atol(strData);
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMin, TRUE, auxData.auxType));
			auxData.lMinData = atol(strData);
			*/
// 		auxData.lMaxData = long(m_pDoc->UnitTrans(dlg.m_fSensorMAx, TRUE, auxData.auxType));
// 		auxData.lMinData = long(m_pDoc->UnitTrans(dlg.m_fSensorMin, TRUE, auxData.auxType));
// 		auxData.lEndMaxData = long(m_pDoc->UnitTrans(dlg.m_fEndMax, TRUE, auxData.auxType));
// 		auxData.lEndMinData = long(m_pDoc->UnitTrans(dlg.m_fEndMin, TRUE, auxData.auxType));

		//ljb 2009-03 add -> AUX Function_Division 추가
		auxData.funtion_division1 = int(dlg.nSensorFunction1);
		//ljb 2010-06 add -> AUX Function_Division2 ,3 추가
		auxData.funtion_division2 = int(dlg.nSensorFunction2);
		auxData.funtion_division3 = int(dlg.nSensorFunction3);
		auxData.auxTempTableType = BYTE(dlg.nSensorAuxthrType);		//ljb 20160504 add

		auxData.vent_use_flag = BYTE(dlg.m_bUseVentLink); //ksj 20200116 : V1016 : vent 연동 여부

		HTREEITEM hAux = m_ctrlAuxTree.InsertItem(strTemp, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM, hItem);
		m_ctrlAuxTree.Expand(hItem, TVE_EXPAND);

		HTREEITEM hUnit = m_ctrlAuxTree.GetParentItem(hItem);
		UpdateRemainAux(nCurrentModule, hUnit, hAux, TRUE);

		CChannelSensor chSensor(&auxData, strAuxName, nChIndex);
		AddList(&chSensor, TRUE);
		UpdateChannelAuxData(nCurrentModule, nChIndex, &chSensor, TRUE);
		IsChanged = TRUE;
		GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);
	
	
	}	
}

void CAuxDataDlg::OnBtnDel() 
{
	STF_MD_AUX_SET_DATA auxData;
/*	HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();

	CString strTemp = m_ctrlAuxTree.GetItemText(hItem);

	//Ch Item 이거나 Child Item이 없는경우(Module Item, Root Item, Child가 없는 Ch Item) Return  
	if(strTemp.Left(2) == "Ch" || m_ctrlAuxTree.ItemHasChildren(hItem))
		return;	
*/
	CCyclerModule * pMD;
	pMD = m_pDoc->GetModuleInfo(nCurrentModule);
	if(pMD == NULL)		return;

	HTREEITEM hDel = m_ctrlAuxTree.GetFirstSelectedItem();
	HTREEITEM hFirst = hDel;
	int nCnt = 0;
	HTREEITEM deleteItem[1024];
	while(hDel)
	{
		CString strTemp = m_ctrlAuxTree.GetItemText(hDel);
		HTREEITEM hParItem = m_ctrlAuxTree.GetParentItem(hDel);
		CString strPar = m_ctrlAuxTree.GetItemText(hParItem);

		if(strPar == Fun_FindMsg("GetAuxTypeName_msg1","IDD_AUXDATA_DLG") ||
			strPar == Fun_FindMsg("GetAuxTypeName_msg2","IDD_AUXDATA_DLG")  ||
			strPar == Fun_FindMsg("GetAuxTypeName_msg3","IDD_AUXDATA_DLG") ||
			strPar == Fun_FindMsg("AuxDataDlg_OnBtnMove_msg7","IDD_AUXDATA_DLG") ||
			strPar == Fun_FindMsg("AuxDataDlg_OnBtnMove_msg8","IDD_AUXDATA_DLG")  ||
			strPar == Fun_FindMsg("AuxDataDlg_OnBtnMove_msg9","IDD_AUXDATA_DLG") )
		{
			return;
		}

		if(strPar.Left(4) == "Unit") return;

		int nChIndex = atoi(strPar.Mid(2));

		CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex-1);
		if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
		{
			AfxMessageBox(Fun_FindMsg("OnBtnDel_msg","IDD_AUXDATA_DLG"));
			//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
			return;
		}
		
		int nIndex = strTemp.Find(" ", 0);		
		CString strType = strTemp.Left(nIndex);

		auxData.auxType = GetAuxType(strType);
		//auxData.auxChNo = atoi(strTemp.Mid(nIndex+1, strTemp.GetLength() - nIndex)); //ksj 20200129 : 주석처리. 한글 이외에는 안통하는 코드

		//ksj 20200129 : 파싱 방법 변경. 영문에서는 파싱 되지 않으므로 개선.
		int nIndex2 = strTemp.ReverseFind(' ');
		auxData.auxChNo = atoi(strTemp.Mid(nIndex2+1, strTemp.GetLength() - nIndex2));
		//ksj end

	/*	if (auxData.auxType == PS_AUX_TYPE_TEMPERATURE_TH)  //ksj 20160627 주석처리함
		{
			auxData.auxChNo =  auxData.auxChNo - pMD->GetMaxVoltage();
			pMD->RemoveAuxData(auxData);
		}
		else
		{*/
			pMD->RemoveAuxData(auxData);
		//}

		HTREEITEM hUnitItem = m_ctrlAuxTree.GetParentItem(hParItem);
		UpdateRemainAux(nCurrentModule, hUnitItem, hDel, FALSE);

		deleteItem[nCnt] = hDel;	

		HTREEITEM hNext = m_ctrlAuxTree.GetNextSelectedItem(hDel);
		//m_ctrlAuxTree.DeleteItem(hDel);
		hDel = hNext;
		IsChanged = TRUE;
		nCnt++;
	}

	/*m_ctrlAuxTree.DeleteItem(hFirst);*/
	while(nCnt>0)
	{
		m_ctrlAuxTree.DeleteItem(deleteItem[nCnt-1]);
		nCnt--;			
	}

	GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);
}

BOOL CAuxDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString strPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Path");
	strSaveDir = strPath + "\\" + "DataBase";

	//ljb 2011221 이재복 //////////
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("OnInitDialog_msg1","IDD_AUXDATA_DLG"));
	//@ if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");
	m_pDoc->Fun_GetArryAuxDivision(m_uiArryAuxCode);		//ljb 2011222 이재복 //////////
	m_pDoc->Fun_GetArryAuxStringName(m_strArryAuxName);		//ljb 2011222 이재복 //////////
	m_pDoc->Fun_GetArryAuxThrDivision(m_uiArryAuxThrCode);		//ljb 20160504 이재복 //////////
	m_pDoc->Fun_GetArryAuxThrStringName(m_strArryAuxThrName);	//ljb 20160504 이재복 //////////
	m_pDoc->Fun_GetArryAuxHumiDivision(m_uiArryAuxHumiCode);		//ksj 20200207
	m_pDoc->Fun_GetArryAuxHumiStringName(m_strArryAuxHumiName);	//ksj 20200207

	//GetAuxInfo(); //ksj 20160624
	
	CRect rect;

	//리스트 초기화////////////////////////////////////////////////////////////////////////////
	m_ctrlSensorList.GetClientRect(&rect);
//	int nColumnWidth = rect.Width() / 4;

	m_ctrlSensorList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES); 

	int n_dummy =  m_ctrlSensorList.InsertColumn(0, "Dummy", LVCFMT_CENTER, 100);	//Dummy Column

	m_ctrlSensorList.InsertColumn(1, _T(Fun_FindMsg("OnInitDialog_msg2","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 80);
	//@ m_ctrlSensorList.InsertColumn(1, _T("센서종류"), LVCFMT_CENTER, 80);
	m_ctrlSensorList.InsertColumn(2, _T(Fun_FindMsg("OnInitDialog_msg3","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 50);
	//@ m_ctrlSensorList.InsertColumn(2, _T("채널"), LVCFMT_CENTER, 50);
	m_ctrlSensorList.InsertColumn(3, _T(Fun_FindMsg("OnInitDialog_msg4","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 100);
	//@ m_ctrlSensorList.InsertColumn(3, _T("센서 이름"), LVCFMT_CENTER, 100);
	m_ctrlSensorList.InsertColumn(4, _T(Fun_FindMsg("OnInitDialog_msg5","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 80);
	//@ m_ctrlSensorList.InsertColumn(4, _T("안전상한값"), LVCFMT_CENTER, 80);
	m_ctrlSensorList.InsertColumn(5, _T(Fun_FindMsg("OnInitDialog_msg6","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 80);
	//@ m_ctrlSensorList.InsertColumn(5, _T("안전하한값"), LVCFMT_CENTER, 80);
	m_ctrlSensorList.InsertColumn(6, _T(Fun_FindMsg("OnInitDialog_msg7","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 80);
	//@ m_ctrlSensorList.InsertColumn(6, _T("종료상한값"), LVCFMT_CENTER, 80);
	m_ctrlSensorList.InsertColumn(7, _T(Fun_FindMsg("OnInitDialog_msg8","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 80);
	//@ m_ctrlSensorList.InsertColumn(7, _T("종료하한값"), LVCFMT_CENTER, 80);
	m_ctrlSensorList.InsertColumn(8, _T(Fun_FindMsg("OnInitDialog_msg9","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 200);
	//@ m_ctrlSensorList.InsertColumn(8, _T("분류 1"), LVCFMT_CENTER, 200);
	m_ctrlSensorList.InsertColumn(9, _T(Fun_FindMsg("OnInitDialog_msg10","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 200);
	//@ m_ctrlSensorList.InsertColumn(9, _T("분류 2"), LVCFMT_CENTER, 200);
	m_ctrlSensorList.InsertColumn(10, _T(Fun_FindMsg("OnInitDialog_msg11","IDD_AUXDATA_DLG")), LVCFMT_CENTER, 200);
	//@ m_ctrlSensorList.InsertColumn(10, _T("분류 3"), LVCFMT_CENTER, 200);
	//m_ctrlSensorList.InsertColumn(11, _T("Thermistor Type"), LVCFMT_CENTER, 200);
	m_ctrlSensorList.InsertColumn(11, _T("Aux Sensor Type"), LVCFMT_CENTER, 200); //ksj 20200211 : 명칭 변경. (써미스터, 습도 모두 포용)
	m_ctrlSensorList.DeleteColumn(n_dummy);		//Remove Dummy Column

	///////////////////////////////////////////////////////////////////////////////////////////////////

	InitTreeList();
	
	//콤보박스 m_CboChList 초기화
	InitAuxDataMovePart();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAuxDataDlg::InitTreeList()
{

	m_ctrlAuxTree.DeleteAllItems();

	m_ModuleImage = new CImageList;

	//상태별 표시할 이미지 로딩
	m_ModuleImage->Create(IDB_VI_STATE_, 20, 2,RGB(255,255,255));
	m_ctrlAuxTree.SetImageList(m_ModuleImage, TVSIL_NORMAL);

	hRoot = m_ctrlAuxTree.InsertItem("System", IMAGE_ROOT_TREE_ITEM , IMAGE_ROOT_TREE_ITEM);

	CCyclerModule * pMD;
	int nModuleID = 0;
	CString strTmp;
	HTREEITEM hLevel1;
	
	for(int i =0; i < m_pDoc->GetInstallModuleCount(); i++)
	{
		nModuleID = m_pDoc->GetModuleID(i);

		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD == NULL)		continue;
#ifdef _DEBUG
		//pMD->MakeAuxData();
#endif // _DEBUG
		strTmp.Format("%s", m_pDoc->GetModuleName(nModuleID));

		if( pMD->GetState() != PS_STATE_LINE_OFF )
		{
			hLevel1 = m_ctrlAuxTree.InsertItem(strTmp, IMAGE_UNIT_LINEON_TREE_ITEM , IMAGE_UNIT_LINEON_TREE_ITEM, hRoot);
		}
		else
		{	
#ifdef _DEBUG
			hLevel1 = m_ctrlAuxTree.InsertItem(strTmp, IMAGE_UNIT_LINEON_TREE_ITEM , IMAGE_UNIT_LINEON_TREE_ITEM, hRoot);
#else
			hLevel1 = m_ctrlAuxTree.InsertItem(strTmp, IMAGE_UNIT_LINEOFF_TREE_ITEM , IMAGE_UNIT_LINEOFF_TREE_ITEM, hRoot);
#endif
			
		}

		UpdateChannelTree(nModuleID, hLevel1);		//채널 정보 추가
		UpdateRemainAllAux(nModuleID, hLevel1);		//남은 Aux 추가

		m_ctrlAuxTree.Expand(hLevel1, TVE_EXPAND);
				
	}
}

void CAuxDataDlg::InitAuxDataMovePart()
{

	CString tmpStr;
	CCyclerModule * pMD;
	int nModuleID;
	nModuleID = nCurrentModule+1;
	pMD = m_pDoc->GetModuleInfo(nModuleID);
	if(pMD == NULL)		
	{
		//$1013 AfxMessageBox(_T("모듈 정보를 가져오지 못함"));
		AfxMessageBox(Fun_FindMsg("AuxDataDlg_InitAuxDataMovePart_msg1","IDD_AUXDATA_DLG")); //&&
		return;
	}
	int nChCount = pMD->GetTotalChannel();
//CboBox 설정
	//$ m_CboChList.AddString(_T("채널 선택"));
	m_CboChList.AddString(Fun_FindMsg("AuxDataDlg_InitAuxDataMovePart_msg2","IDD_AUXDATA_DLG")); //&&
	m_CboChList.SetItemData(0, 0);
	m_CboChList.SetCurSel(0);
	int i=0;
	for(int i = 0; i< nChCount; i++)
	{
		tmpStr.Format(_T("Ch%d"), i+1);
		m_CboChList.AddString(tmpStr);
		m_CboChList.SetItemData(0, i);
	}
//센서 존재 확인 후 
	BOOL bAuxResetChoice;
	bAuxResetChoice = FALSE;
	for(int y = 0 ; y < pMD->GetMaxSensorType(); y++)
	{
		int nCount =0;
		switch(y)
		{
		case 0: 
			{
				nCount = pMD->GetMaxTemperature(); 
				if(nCount>0)
				{
					bAuxResetChoice = TRUE;
				}
				break;
			}
		case 1: 
			{
				nCount = pMD->GetMaxVoltage(); 
				if(nCount>0)
				{
					bAuxResetChoice = TRUE;
				}
				break;
			}
		case 2: 
			{
				nCount = pMD->GetMaxTemperatureTh(); 
				if(nCount>0)
				{
					bAuxResetChoice = TRUE;
				}
				break;
			}
		//ksj 20200116
		case 3: 
			{
				nCount = pMD->GetMaxHumidity(); 
				if(nCount>0)
				{
					bAuxResetChoice = TRUE;
				}
				break;
			}
		default :
			break;
		}	
	}

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark 
		if(g_AppInfo.iPType==1)
		{

		}
//#else
		else
		{
		//yulee 20181213 병렬 mark 
		 	if(bAuxResetChoice == TRUE)
		 	{
			//$ 1013 tmpStr.Format(_T("할당 해제"));
			tmpStr.Format(Fun_FindMsg("AuxDataDlg_InitAuxDataMovePart_msg3","IDD_AUXDATA_DLG")); //&&
			m_CboChList.AddString(tmpStr);
			m_CboChList.SetItemData(0, i-1);
		 	}
		}
//#endif
}

BOOL CAuxDataDlg::DestroyWindow() 
{
	if(m_ModuleImage != NULL)
	{
		delete m_ModuleImage;
		m_ModuleImage = NULL;
	}
	
	return CDialog::DestroyWindow();
}

//Channel 정보를 Tree 에 추가한다. Dialog Init시 호출
void CAuxDataDlg::UpdateChannelTree(int nCurrentModule, HTREEITEM hItem)
{
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
	int nCount = pMD->GetTotalChannel();

//	int nStartCh = GetStartChNo(nCurrentModule);
	int nStartCh =1;
	CString strTemp;


	for(int a = 0; a < nCount; a++ )
	{
		strTemp.Format("Ch %d",nStartCh+a );		
		HTREEITEM hChITem;
		if(pMD->GetState() != PS_STATE_LINE_OFF)
			hChITem = m_ctrlAuxTree.InsertItem(strTemp, 1, 1, hItem);
		else
		{
#ifdef _DEBUG
			hChITem = m_ctrlAuxTree.InsertItem(strTemp, 1, 1, hItem);
#else
			hChITem = m_ctrlAuxTree.InsertItem(strTemp, 12, 12, hItem);
#endif
			
		}

		if(pMD == NULL)		return;
			
		for(int i = 0 ; i < pMD->GetMaxSensorType(); i++)
		{
			int nCount =0;
			switch(i)
			{
			case 0: nCount = pMD->GetMaxTemperature(); break;
			case 1: nCount = pMD->GetMaxVoltage(); break;
			case 2: nCount = pMD->GetMaxTemperatureTh(); break;
			case 3: nCount = pMD->GetMaxHumidity(); break; //ksj 20200116
			}
			for(int j = 0 ; j < nCount; j++)
			{
				//CChannelSensor * pSensor = pMD->GetAuxData(j, i);
				CChannelSensor * pSensor;
				if (i==2) 
					pSensor = pMD->GetAuxData(j+pMD->GetMaxVoltage(), i);
				else if (i==3) //ksj 20200120
					pSensor = pMD->GetAuxData(j+pMD->GetMaxVoltage()+pMD->GetMaxTemperatureTh(), i);
				else
					pSensor = pMD->GetAuxData(j, i);

				if(pSensor == NULL) continue;

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark //yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
{
					//ksj 20171025 : 주석처리
					if (pSensor->GetMasterChannel() == 3)
					{
						pSensor->SetMasterChannel(2);	//ljb 20170630 add
					}
}
//#endif

				//ksj 20210512 : 이상감지 경고 기능 추가 (ATS가 채널 할당을 이상하게 꼬을때가 있음)
				int nMasterCh = pSensor->GetMasterChannel();
				if(nMasterCh > pMD->GetTotalChannel())
				{
					if(i==PS_AUX_TYPE_TEMPERATURE_TH) //ksj 20160624 Thermistor일때는 AuxV 개수 빼고 넘버링.
						strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage());
					else if(i==PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : v1016 습도 추가. 가구현. 확인필요.
						strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage()-pMD->GetMaxTemperatureTh());
					else
						strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());

					CString strMsg;
					//AfxMessageBox("External data setting error has been detected.\nClick the [Initialize] button to reset the settings.");
					//AfxMessageBox("외부데이터 설정 이상이 감지되었습니다.\n초기화 버튼을 눌러 리셋 해주세요.");


					strMsg.Format("외부데이터 설정 이상이 감지되었습니다.\n초기화 버튼을 눌러 외부데이터 설정을 리셋 해주세요.\n(오류: %s(이)가 존재하지 않는 채널%d에 할당 감지됨.)",strTemp,nMasterCh);
					m_pDoc->WriteLog(strMsg);
					MessageBox(strMsg,"error",MB_ICONERROR|MB_OK);
				}
				//ksj end

				if(pSensor->GetInstall() == TRUE && pSensor->GetMasterChannel() == a+1)
				{
					if(i==PS_AUX_TYPE_TEMPERATURE_TH) //ksj 20160624 Thermistor일때는 AuxV 개수 빼고 넘버링.
						strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage());
					else if(i==PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : v1016 습도 추가. 가구현. 확인필요.
						strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage()-pMD->GetMaxTemperatureTh());
					else
						strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());

					/*
					{
						//yulee 20180302 97~104 [17PSCSM039]SMPS8CH - Ch별 12V, 24V 세팅 - 하드 코딩
						//strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
						if (pSensor->GetAuxChannelNumber() > 96 && pSensor->GetAuxChannelNumber() < 105)
						{
						 	if (pSensor->GetAuxChannelNumber() == 97 ) strTemp.Format("%s %d Ch1 12V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()); //ljb 20170925 add
						 	if (pSensor->GetAuxChannelNumber() == 98 ) strTemp.Format("%s %d Ch2 12V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
						 	if (pSensor->GetAuxChannelNumber() == 99 ) strTemp.Format("%s %d Ch3 12V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
						 	if (pSensor->GetAuxChannelNumber() == 100) strTemp.Format("%s %d Ch4 12V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 101) strTemp.Format("%s %d Ch1 24V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 102) strTemp.Format("%s %d Ch2 24V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 103) strTemp.Format("%s %d Ch3 24V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
						 	if (pSensor->GetAuxChannelNumber() == 104) strTemp.Format("%s %d Ch4 24V",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
						}
						else
						{
							strTemp.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
						}
						//yulee 20180302 97~104 [17PSCSM039] 하드코딩 끝
					}
					*/

					if(pMD->GetState() != PS_STATE_LINE_OFF)
						m_ctrlAuxTree.InsertItem(strTemp, IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hChITem);
					else
						m_ctrlAuxTree.InsertItem(strTemp, 14, 14, hChITem);
				}
			}			
		}		
	}
}

int CAuxDataDlg::GetStartChNo(int nModuleID)
{
	CCyclerModule *lpModule;

	int nStartCh = 1;
	//모듈들 채널 번호를 연속하여 표기할 경우 
	if(m_pDoc->m_bChContinueNo)
	{
		int mdID;
		for(int s =0; s<m_pDoc->GetInstallModuleCount(); s++)
		{
			mdID = m_pDoc->GetModuleID(s);
			if(mdID < nModuleID)
			{
				lpModule =  m_pDoc->GetCyclerMD(mdID);
				if(lpModule)
				{
					nStartCh+= lpModule->GetTotalChannel();
				}
			}
		}
	}
	return nStartCh;
}

//해당 채널 클래스에 Aux Data를 추가한다.
void CAuxDataDlg::UpdateChannelAuxData(int nModuleID, int nChIndex, CChannelSensor * chSensorData, BOOL bInstall)
{
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nModuleID);

	if(pMD != NULL)
	{
		CChannelSensor * pSensor = pMD->GetAuxData(chSensorData->GetAuxChannelNumber() - 1, chSensorData->GetAuxType());
		if(pSensor == NULL) return;
		pSensor->SetAuxData(&chSensorData->GetAuxData());
		pSensor->SetAuxName(chSensorData->GetAuxName());
		pSensor->SetInstall(bInstall);
		pSensor->SetMasterChannel(nChIndex);
	}	
}

//Tree Item 변경시마다 ListControl의 내용을 변경시킨다.
void CAuxDataDlg::OnSelchangedAuxTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	HTREEITEM hItem = pNMTreeView->itemNew.hItem;

	if(hItem == hRoot)
		return;
	UpdateAuxTreeItem(hItem);

	*pResult = 0;

	//bool bShift = (::GetKeyState(VK_SHIFT) & 0x8000) != 0;

// 	if(bShift)
// 	{
// 		GetCursorPos(&m_Pt);
// 		CString tmpStr = "";
// 		tmpStr.Format("x: %d, y:%d", m_Pt.x, m_Pt.y);
// 		TRACE0(tmpStr);
// 		KillTimer(TIMER_EXTERNAL_MULTISEL);
// 		SetTimer(TIMER_EXTERNAL_MULTISEL, 100, NULL);
// 	}
}

/*void CAuxDataDlg::Wait_Milli(DWORD dwMillisecond) //yulee 20181026
{
	MSG msg;
	DWORD dwStart;
	dwStart = GetTickCount();

	while(GetTickCount() - dwStart < dwMillisecond)
	{
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return;
}*/

//Module Name 으로 Module ID를 검색한다.
int CAuxDataDlg::GetModuleID(CString strModuleName)
{
	CCyclerModule * pMD;
	int nModuleID = 0;
	CString strTmp;

	for(int i =0; i < m_pDoc->GetInstallModuleCount(); i++)
	{
		nModuleID = m_pDoc->GetModuleID(i);
		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD == NULL)		continue;

		strTmp.Format("%s", m_pDoc->GetModuleName(nModuleID));

		if(strModuleName == strTmp)			//이름이 같으면...
		{
			return nModuleID;
		}
	}
	return -1;								//같은 이름이 없으면 
}

//CString형 Aux Data Type, Aux Channel Number로 검색한다.
CChannelSensor * CAuxDataDlg::GetSensorData(CString strItemName)
{
	int nTemp, nTemp1 = 0;
	int nAuxCh = 0;
	CString strTemp;

	nTemp = strItemName.Find(" ", 0);
	strTemp = strItemName.Mid(nTemp+1, strItemName.GetLength() - nTemp);
	nTemp1 = strTemp.Find(" ", 0);

	if (nTemp1 < 0)
	{
		nAuxCh = atoi(strItemName.Mid(nTemp+1, strItemName.GetLength() - nTemp));
	}
	else
	{
		nAuxCh = atoi(strTemp.Mid(nTemp1+1, strTemp.GetLength() - nTemp1));
	}
	
/*	int nTemp2 = strItemName.Find("Measure", 0);
	
	
	if(nTemp2 == -1)
	{
		nTemp2 = strItemName.Find("Sensor", 0);
		nTemp2 += 6;
	}
	else
	{
		 nTemp2 = strItemName.Find("Measure", 0);
		 nTemp2 += 7;
	}
*/

	CString strAuxType = strItemName.Left(nTemp);

	int auxType = GetAuxType(strAuxType);

	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
	if(pMD == NULL)		return NULL;

	if (auxType == PS_AUX_TYPE_TEMPERATURE_TH) nAuxCh = nAuxCh + pMD->GetMaxVoltage(); //ljb 20160621
	if (auxType == PS_AUX_TYPE_HUMIDITY) nAuxCh = nAuxCh + pMD->GetMaxVoltage() + pMD->GetMaxTemperatureTh(); //ksj 20200116 : v1016 습도 추가. 가구현. 확인필요.

	return pMD->GetAuxData(nAuxCh-1, auxType);
	
}

//닫기
void CAuxDataDlg::OnOK() 
{		
	if(IsChanged)
	{
		if(AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_AUXDATA_DLG"), MB_YESNO) != IDYES)
			//@ if(AfxMessageBox("외부 센서 정보가 변경되었습니다.\r\n작업을 취소하시겠습니까?", MB_YESNO) != IDYES)
			return;

		CCyclerModule * pMD;
		int nModuleID = 0;
		
		for(int i =0; i < m_pDoc->GetInstallModuleCount(); i++)
		{
			nModuleID = m_pDoc->GetModuleID(i);
			pMD = m_pDoc->GetModuleInfo(nModuleID);
			if(pMD == NULL)		continue;
			pMD->CencleAuxData();
		}
	
		
	}
	CDialog::OnOK();
}


void CAuxDataDlg::SendAuxDataModule(HTREEITEM hUnit)
{
	
	CCyclerModule * pMD;
	int nModuleID = nCurrentModule;
	HTREEITEM hChItem;

	while(hUnit)
	{
		//Module 정보를 가져온다.
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(pMD == NULL)		return;

		hChItem = m_ctrlAuxTree.GetChildItem(hUnit);	//Ch Item	
		
		while(hChItem)
		{
			if(m_ctrlAuxTree.ItemHasChildren(hChItem) == TRUE)		//Aux Data가 Child로 있으면 
				SendAuxDataCh(hChItem);
		
			hChItem = m_ctrlAuxTree.GetNextItem(hChItem, TVGN_NEXT);
		}
		hUnit = m_ctrlAuxTree.GetNextItem(hUnit, TVGN_NEXT);
	}

}

void CAuxDataDlg::SendAuxDataCh(HTREEITEM hItem)
{
/*	STF_MD_AUX_SET_DATA mdAuxData[_SFT_MAX_MAPPING_AUX];
	CString strAuxName;
	int nChIndex = 0;
	int nIndex = 0;
	ZeroMemory(&mdAuxData, sizeof(STF_MD_AUX_SET_DATA));
	CString strItemText = m_ctrlAuxTree.GetItemText(hItem);
	
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);

	if(pMD == NULL)
		return;
	
	nChIndex = atoi(strItemText.Mid(2));
	mdAuxData.chNo = nChIndex;

	if(!m_ctrlAuxTree.ItemHasChildren(hItem))
		return;

	HTREEITEM hChildItem = m_ctrlAuxTree.GetChildItem(hItem);	//Ch Item	

	while(hChildItem)
	{
		strItemText = m_ctrlAuxTree.GetItemText(hChildItem);
		
		CChannelSensor * pSensor = GetSensorData(strItemText);

		if(pSensor->GetInstall() == TRUE && pSensor->GetMasterChannel() == nChIndex)
		{
			memcpy(&mdAuxData[nIndex++], &pSensor->GetAuxData(), sizeof(STF_MD_AUX_SET_DATA));
		}

		hChildItem = m_ctrlAuxTree.GetNextItem(hChildItem,TVGN_NEXT);
	}
	m_pDoc->SendAuxDatatoModule(nCurrentModule, mdAuxData);		//SBC 로 Aux Data 설정을 전송한다.*/
}

void CAuxDataDlg::OnButtonApply() 
{
	m_pDoc->WriteLog("DEBUG:::::::::::::::::  aux 적용 버튼 누름"); //ksj 20201118 : TEST
	
	if(SendAllAuxDataToModule() == TRUE)
	{
		m_pDoc->WriteLog("DEBUG:::::::::::::::::  aux 전송 TRUE"); //ksj 20201118 : TEST

		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
		pMD->ApplyAuxData();	//Aux Save 포함
		ZeroMemory(&bColor, sizeof(BOOL)*_SFT_MAX_MAPPING_AUX);
		Invalidate(TRUE);
		IsChanged = FALSE;
		
		Sleep(3000);
		//m_pDoc->SendAuxDataRequest(nCurrentModule);		

		GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);
		
		m_CboChList.SetCurSel(0);
		InitTreeList();	//ljb 20150810 add
	}
	
}

void CAuxDataDlg::OnClose() 
{
	CDialog::OnClose();
}

//전체 모듈에 대하여 모든 Aux 정보를 전송한다.
//BOOL CAuxDataDlg::SendAllAuxDataToModule()
BOOL CAuxDataDlg::SendAllAuxDataToModule(BOOL bReset) //ksj 20210512 : reset 기능 추가.
{

	STF_MD_AUX_SET_DATA_A mdAuxData_A[_SFT_MAX_MAPPING_AUX_A];
	STF_MD_AUX_SET_DATA mdAuxData[_SFT_MAX_MAPPING_AUX];
	STF_MD_AUX_SET_DATA_V1015 mdAuxData_1015[_SFT_MAX_MAPPING_AUX]; //ksj 20201013 : V1015 호환 추가.

	CCyclerModule * pMD;
//	CCyclerChannel *lpChannelInfo;
	int nRtn = 0;
	
	for(int j =0; j < m_pDoc->GetInstallModuleCount(); j++)
	{
		ZeroMemory(&mdAuxData, sizeof(STF_MD_AUX_SET_DATA)*_SFT_MAX_MAPPING_AUX);
		ZeroMemory(&mdAuxData_A, sizeof(STF_MD_AUX_SET_DATA_A)*_SFT_MAX_MAPPING_AUX_A);
		ZeroMemory(&mdAuxData_1015, sizeof(STF_MD_AUX_SET_DATA_V1015)*_SFT_MAX_MAPPING_AUX); //ksj 20201013 : V1015 호환 추가.

		int nModuleID = m_pDoc->GetModuleID(j);

		pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(!pMD) continue; //ksj 20200129
		SFT_MD_SYSTEM_DATA* pSysData = ::SFTGetModuleSysData(nModuleID);		
		if(!pSysData) continue; //ksj 20200129
		if(pMD == NULL) continue;
		if (pMD->GetState() == PS_STATE_LINE_OFF) continue;
				
		int nIndex = 0;

		for(int i = 0 ; i < pMD->GetMaxSensorType(); i++)
		{
			int nCount =0;
			switch(i)
			{
			case 0: nCount = pMD->GetMaxTemperature(); break;
			case 1: nCount = pMD->GetMaxVoltage(); break;
			case 2: nCount = pMD->GetMaxTemperatureTh(); break;
			case 3: nCount = pMD->GetMaxHumidity(); break; //ksj 20200116
			}
			for(int j = 0 ; j < nCount; j++)
			{
				//CChannelSensor * pSensor = pMD->GetAuxData(j, i);
				CChannelSensor * pSensor;
				if (i == PS_AUX_TYPE_TEMPERATURE_TH) 
					pSensor = pMD->GetAuxData(j+pMD->GetMaxVoltage(), i);
				else if (i == PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : V1016 습도 추가, 가구현, 확인필요.
					pSensor = pMD->GetAuxData(j+pMD->GetMaxVoltage()+pMD->GetMaxTemperatureTh(), i);
				else
					pSensor = pMD->GetAuxData(j, i);

				if(pSensor != NULL)
				{		
					if(pSensor->GetInstall() == TRUE)
					{
						//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION)
						if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20200125
 						{
							STF_MD_AUX_SET_DATA temp = pSensor->GetAuxData();
							mdAuxData_A[nIndex].auxChNo = temp.auxChNo;
							mdAuxData_A[nIndex].auxType	= temp.auxType;
							mdAuxData_A[nIndex].chNo	= temp.chNo;
							mdAuxData_A[nIndex].lEndMaxData	= temp.lEndMaxData;
							mdAuxData_A[nIndex].lEndMinData	= temp.lEndMinData;
							mdAuxData_A[nIndex].lMaxData	= temp.lMaxData;
							mdAuxData_A[nIndex].lMinData	= temp.lMinData;
							sprintf(mdAuxData_A[nIndex].szName, temp.szName);

							//ksj 20210512 : Reset 기능 추가
							if(bReset)
							{
								mdAuxData_A[nIndex].chNo	= 0; //할당된 채널을 해제한다.
							}
						}
						else if(pSysData->nProtocolVersion == _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : V1015 호환 기능 추가.
						{
							//v1016 -> v1015 변환
							STF_MD_AUX_SET_DATA temp = pSensor->GetAuxData();
							mdAuxData_1015[nIndex].auxChNo = temp.auxChNo;
							mdAuxData_1015[nIndex].auxType	= temp.auxType;
							mdAuxData_1015[nIndex].chNo	= temp.chNo;
							mdAuxData_1015[nIndex].lEndMaxData	= temp.lEndMaxData;
							mdAuxData_1015[nIndex].lEndMinData	= temp.lEndMinData;
							mdAuxData_1015[nIndex].lMaxData	= temp.lMaxData;
							mdAuxData_1015[nIndex].lMinData	= temp.lMinData;

							mdAuxData_1015[nIndex].auxTempTableType = temp.auxTempTableType;
							mdAuxData_1015[nIndex].funtion_division1 = temp.funtion_division1;
							mdAuxData_1015[nIndex].funtion_division2 = temp.funtion_division2;
							mdAuxData_1015[nIndex].funtion_division3 = temp.funtion_division3;
							mdAuxData_1015[nIndex].reserved2 = temp.reserved2;

							mdAuxData_1015[nIndex].reserved[0] = temp.vent_use_flag;
							mdAuxData_1015[nIndex].reserved[1] = temp.reserved1;

							sprintf(mdAuxData_1015[nIndex].szName, temp.szName);
		
							//ksj 20210512 : Reset 기능 추가
							if(bReset)
							{
								mdAuxData_1015[nIndex].chNo	= 0; //할당된 채널을 해제한다.
							}
						}
						else //v1016
						{
							memcpy(&mdAuxData[nIndex], &pSensor->GetAuxData(), sizeof(STF_MD_AUX_SET_DATA));	

							//ksj 20210512 : Reset 기능 추가
							if(bReset)
							{
								mdAuxData[nIndex].chNo	= 0; //할당된 채널을 해제한다.
							}
						}

						if(g_AppInfo.iPType==1)
						{
							//ksj 20171025 : 주석처리
							if (mdAuxData[nIndex].chNo == 2) 
							{
								mdAuxData[nIndex].chNo = 3;		//ljb 20170630 add
							}
						}

						if (i == PS_AUX_TYPE_TEMPERATURE_TH)
						{
							//mdAuxData[nIndex].auxChNo = mdAuxData[nIndex].auxChNo + pMD->GetMaxVoltage(); 
							mdAuxData[nIndex].auxChNo = j + 1 + pMD->GetMaxVoltage(); // ksj 20160624
						}

						//ksj 20200116 : v1016 습도 추가. 가구현. 확인필요.
						if (i == PS_AUX_TYPE_HUMIDITY)
						{
							//mdAuxData[nIndex].auxChNo = mdAuxData[nIndex].auxChNo + pMD->GetMaxVoltage(); 
							mdAuxData[nIndex].auxChNo = j + 1 + pMD->GetMaxVoltage() + pMD->GetMaxTemperatureTh(); // ksj 20160624
						}

						nIndex++;

					}
				}
			}
		}
	
		//if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION)
		if(pSysData->nProtocolVersion < _SFT_PROTOCOL_VERSION_1015) //ksj 20200121 
		{
			nRtn = m_pDoc->SendAuxDatatoModuleA(nModuleID, mdAuxData_A);
		}
		else if(pSysData->nProtocolVersion == _SFT_PROTOCOL_VERSION_1015) //ksj 20201013 : V1015 호환 추가. 
		{
			nRtn = m_pDoc->SendAuxDatatoModule1015(nModuleID, mdAuxData_1015);
		}
		else
		{
			m_pDoc->WriteLog("DEBUG:::::::::::::::::  SBC에 전송 전 로그"); //ksj 20201118 : TEST
			nRtn = m_pDoc->SendAuxDatatoModule(nModuleID, mdAuxData);
			m_pDoc->WriteLog("DEBUG:::::::::::::::::  SBC에 전송 후 로그"); //ksj 20201118 : TEST
		}
		

		if(nRtn != 0)	//전송에 성공하면 Aux Tree Item의 이미지를 변경한다.
		{
			HTREEITEM hUnitItem = m_ctrlAuxTree.GetChildItem(hRoot);
			while(hUnitItem)
			{			
				if(nModuleID == GetModuleID(m_ctrlAuxTree.GetItemText(hUnitItem)))
				{
					HTREEITEM hChItem = m_ctrlAuxTree.GetChildItem(hUnitItem);
				
					while(hChItem)
					{
						if(m_ctrlAuxTree.ItemHasChildren(hChItem))
						{
							HTREEITEM hAuxItem = m_ctrlAuxTree.GetChildItem(hChItem);
							while(hAuxItem)
							{
								m_ctrlAuxTree.SetItemImage(hAuxItem, 3, 3);
								hAuxItem = m_ctrlAuxTree.GetNextItem(hAuxItem, TVGN_NEXT);
							}
						}
						hChItem = m_ctrlAuxTree.GetNextItem(hChItem, TVGN_NEXT);
					}		
				}
				hUnitItem = m_ctrlAuxTree.GetNextItem(hUnitItem, TVGN_NEXT);				
			}
		}
		else
		{
			AfxMessageBox(Fun_FindMsg("SendAllAuxDataToModule_msg","IDD_AUXDATA_DLG"), MB_ICONWARNING);	
			//@ AfxMessageBox("외부 데이터 전송에 실패했습니다.", MB_ICONWARNING);	
			return FALSE;
		}
	}//End for(int i =0; i < m_pDoc->GetInstallModuleCount(); i++)
	return TRUE;
}

void CAuxDataDlg::OnBtnModify() 
{	
	ModifyAux();	
}

void CAuxDataDlg::OnCustomdrawMyList(NMHDR* pNMHDR, LRESULT* pResult)

{
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );	
	
    *pResult = CDRF_DODEFAULT;
	
	if ( CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage )
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if ( CDDS_ITEMPREPAINT == pLVCD->nmcd.dwDrawStage )
	{ 
		COLORREF crText;
		
		// Store the color back in the NMLVCUSTOMDRAW struct.
		if(bColor[pLVCD->nmcd.dwItemSpec])
			crText = RGB(192,192,192);
		else
			crText = RGB(0,0,0);
		pLVCD->clrText = crText;
		
		// Tell Windows to paint the control itself.
		*pResult = CDRF_DODEFAULT;
	}   
}

//List Control에 데이터를 등록한다.
void CAuxDataDlg::AddSensorListChannelItem(HTREEITEM hChItem)
{
	if(!m_ctrlAuxTree.ItemHasChildren(hChItem))
		return;

	CString strItemText = m_ctrlAuxTree.GetItemText(hChItem);
	
	int nChIndex = atoi(strItemText.Mid(2));
	
	HTREEITEM hChildItem = m_ctrlAuxTree.GetChildItem(hChItem);	//Get Aux Item
	
	while(hChildItem)
	{
		if(m_ctrlAuxTree.GetItemText(hChItem).Left(2) == "Ch")		
			AddSensorListAuxItem(hChildItem, nChIndex);
		hChildItem = m_ctrlAuxTree.GetNextItem(hChildItem,TVGN_NEXT);
	}
}

void CAuxDataDlg::AddSensorListAuxItem(HTREEITEM hAuxItem, int nMasterCh)
{
	CString strItemText = m_ctrlAuxTree.GetItemText(hAuxItem);
	CChannelSensor * pSensor = GetSensorData(strItemText);	


	if(pSensor == NULL)
		return;
					
	if(pSensor->GetInstall() == TRUE && pSensor->GetMasterChannel() == nMasterCh)
	{
		int nImage;
		int nImage2;
		m_ctrlAuxTree.GetItemImage(hAuxItem, nImage, nImage2);
		
		if(nImage == IMAGE_TEMPORARY_AUX_TREE_ITEM)
			AddList(pSensor, TRUE);
		else
			AddList(pSensor, FALSE);						
	}	

	
}

void CAuxDataDlg::AddList(CChannelSensor *pSensor, BOOL bColor)
{
	CString strTemp;
	float fMaxData, fMinData, fEndMax, fEndMin;
	int nCount = m_ctrlSensorList.GetItemCount();
	int iFunctionDivision=0;
	int iauThermistorType=0;

	fMaxData = m_pDoc->UnitTrans(float(pSensor->GetAuxData().lMaxData), FALSE, pSensor->GetAuxType());
	fMinData = m_pDoc->UnitTrans(float(pSensor->GetAuxData().lMinData), FALSE, pSensor->GetAuxType());
	fEndMax = m_pDoc->UnitTrans(float(pSensor->GetAuxData().lEndMaxData), FALSE, pSensor->GetAuxType());
	fEndMin = m_pDoc->UnitTrans(float(pSensor->GetAuxData().lEndMinData), FALSE, pSensor->GetAuxType());
	
	this->bColor[nCount] = bColor;
	m_ctrlSensorList.InsertItem(nCount,GetAuxTypeName(pSensor->GetAuxType()));
	if(pSensor->GetAuxType() == PS_AUX_TYPE_TEMPERATURE_TH){ //ksj 20160627
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
		if(pMD != NULL)
			strTemp.Format("%d", pSensor->GetAuxChannelNumber() - pMD->GetMaxVoltage());
	}
	else if(pSensor->GetAuxType() == PS_AUX_TYPE_HUMIDITY){ //ksj 20200116 : v1016 습도 추가. 가구현. 확인필요.
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
		if(pMD != NULL)
			strTemp.Format("%d", pSensor->GetAuxChannelNumber() - pMD->GetMaxVoltage() - pMD->GetMaxTemperatureTh());
	}
	else
	{
		strTemp.Format("%d", pSensor->GetAuxChannelNumber());
	}

	m_ctrlSensorList.SetItemText(nCount, 1, strTemp);
	
	m_ctrlSensorList.SetItemText(nCount, 2, pSensor->GetAuxName());
	
	strTemp.Format("%.3f", fMaxData);
	m_ctrlSensorList.SetItemText(nCount, 3, strTemp);
	strTemp.Format("%.3f", fMinData);
	m_ctrlSensorList.SetItemText(nCount, 4, strTemp);
	strTemp.Format("%.3f", fEndMax);
	m_ctrlSensorList.SetItemText(nCount, 5, strTemp);
	strTemp.Format("%.3f", fEndMin);
	m_ctrlSensorList.SetItemText(nCount, 6, strTemp);

	//ljb 2009-03 add function_Division
	iFunctionDivision = pSensor->GetAuxData().funtion_division1;
	if (iFunctionDivision == 0)
		strTemp = "None [0]";
	else
		strTemp = Fun_GetAuxCodeName(iFunctionDivision);
	m_ctrlSensorList.SetItemText(nCount, 7, strTemp);

	//ljb 2009-03 add function_Division 2
	iFunctionDivision = pSensor->GetAuxData().funtion_division2;
	if (iFunctionDivision == 0)
		strTemp = "None [0]";
	else
		strTemp = Fun_GetAuxCodeName(iFunctionDivision);
	m_ctrlSensorList.SetItemText(nCount, 8, strTemp);

	//ljb 2009-03 add function_Division 3
	iFunctionDivision = pSensor->GetAuxData().funtion_division3;
	if (iFunctionDivision == 0)
		strTemp = "None [0]";
	else
		strTemp = Fun_GetAuxCodeName(iFunctionDivision);
	m_ctrlSensorList.SetItemText(nCount, 9, strTemp);
	
	//ljb 20160429 add Thermistor Table type
	iauThermistorType = pSensor->GetAuxData().auxTempTableType;
	if (iauThermistorType == 0)
	{
		strTemp = "None [0]";
	}
	else
	{
		//strTemp = Fun_GetAuxThrCodeName(iauThermistorType);

		//ksj 20200211 : 기존 써미스터만 표시에서, 습도 센서 타입이면 습도 타입 표시.
		if(pSensor->GetAuxType() == PS_AUX_TYPE_HUMIDITY)
		{
			strTemp = Fun_GetAuxHumiCodeName(iauThermistorType);
		}
		else
		{
			strTemp = Fun_GetAuxThrCodeName(iauThermistorType);
		}
	}
	m_ctrlSensorList.SetItemText(nCount, 10, strTemp);

	if(m_ctrlSensorList.GetItemCount() > 0)
		m_ctrlSensorList.SetItemState(0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED| LVIS_FOCUSED); 
}

void CAuxDataDlg::UpdateAuxTreeItem(HTREEITEM hItem)
{
	m_ctrlSensorList.DeleteAllItems();
	ZeroMemory(bColor, sizeof(BOOL)*_SFT_MAX_MAPPING_AUX);
	
	GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_ADD)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_DEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_ALL_APPLY)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_ALL_APPLY_THERSENSOR)->EnableWindow(FALSE);
	
	
	int nModuleID = 0;	//선택된 아이템이 속한 모듈을 저장한다.
	CString strModuleName;	//모듈 ID를 찾기위한 모듈 이름
	
	CString strItemText = _T("");
		
	strItemText = m_ctrlAuxTree.GetItemText(hItem);	
	
	if(strItemText.Left(2) == "Ch")				//Ch ITem 선택시 Ch에 속한 Aux Data를 보여준다.
	{
		HTREEITEM hParentITem = m_ctrlAuxTree.GetParentItem(hItem);			//Module 정보를 얻기위해  Ch가 속한 Module 정보를 얻는다.
		strModuleName = m_ctrlAuxTree.GetItemText(hParentITem);	
		
		nModuleID = GetModuleID(strModuleName);
		
		if(nModuleID > 0)
			nCurrentModule = nModuleID;
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
		if(pMD == NULL) return;
	
		if(pMD->GetState() != PS_STATE_LINE_OFF)
		{
			GetDlgItem(IDC_BTN_ADD)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_ALL_APPLY)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_ALL_APPLY_THERSENSOR)->EnableWindow(TRUE);
		}
		
		AddSensorListChannelItem(hItem);			
	}
	else
	{
		if(m_ctrlAuxTree.ItemHasChildren(hItem) == FALSE)	//Ch Item도 아니고 Child Item도 없으면 AuxData다
		{						
			HTREEITEM hParaentItem = m_ctrlAuxTree.GetParentItem(hItem);
			strItemText = m_ctrlAuxTree.GetItemText(hParaentItem);
			
			int nChIndex = atoi(strItemText.Mid(2));
			AddSensorListAuxItem(hItem, nChIndex);
			
			hParaentItem = m_ctrlAuxTree.GetParentItem(hParaentItem);
			strModuleName = m_ctrlAuxTree.GetItemText(hParaentItem);
			
			nModuleID = GetModuleID(strModuleName);
			
			if(nModuleID > 0)
				nCurrentModule = nModuleID;

			CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
			if(pMD == NULL) return;

			if(strItemText.Left(2) == "Ch" && pMD->GetState() != PS_STATE_LINE_OFF)
			{
				GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(TRUE);
				GetDlgItem(IDC_BTN_ADD)->EnableWindow(TRUE);
				GetDlgItem(IDC_BTN_DEL)->EnableWindow(TRUE);	
				GetDlgItem(IDC_BTN_ALL_APPLY)->EnableWindow(TRUE);
				GetDlgItem(IDC_BTN_ALL_APPLY_THERSENSOR)->EnableWindow(TRUE);
			}



			if (strItemText == Fun_FindMsg("AuxDataDlg_OnBtnMove_msg7","IDD_AUXDATA_DLG") ||
				strItemText == Fun_FindMsg("AuxDataDlg_OnBtnMove_msg8","IDD_AUXDATA_DLG") ||
				strItemText == Fun_FindMsg("AuxDataDlg_OnBtnMove_msg9","IDD_AUXDATA_DLG"))
			{
				GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(FALSE);
				GetDlgItem(IDC_BTN_ADD)->EnableWindow(FALSE);
				GetDlgItem(IDC_BTN_DEL)->EnableWindow(FALSE);	
				GetDlgItem(IDC_BTN_ALL_APPLY)->EnableWindow(FALSE);
				GetDlgItem(IDC_BTN_ALL_APPLY_THERSENSOR)->EnableWindow(FALSE);
			}
/*	
#ifdef _DEBUG
			GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_ADD)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_DEL)->EnableWindow(TRUE);	
			GetDlgItem(IDC_BTN_ALL_APPLY)->EnableWindow(TRUE);
			GetDlgItem(IDC_BTN_ALL_APPLY_THERSENSOR)->EnableWindow(TRUE);
#endif // _DEBUG
*/					
		}
		else		//Ch Item 또는 AuxData가 아니면 Module Item이다.
		{
			strModuleName = strItemText;
			nModuleID = GetModuleID(strModuleName);
			
			if(nModuleID > 0)
				nCurrentModule = nModuleID;
			
			HTREEITEM hChItem = m_ctrlAuxTree.GetChildItem(hItem);
			while(hChItem)
			{
				AddSensorListChannelItem(hChItem);
				hChItem = m_ctrlAuxTree.GetNextItem(hChItem, TVGN_NEXT);
			}
		}
	}
	HTREEITEM hItem1 = m_ctrlAuxTree.GetFirstSelectedItem();
	if (hItem1)
	{
		GetDlgItem(IDC_BTN_MODIFY)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_ADD)->EnableWindow(TRUE);
		GetDlgItem(IDC_BTN_DEL)->EnableWindow(TRUE);	
		GetDlgItem(IDC_BTN_ALL_APPLY)->EnableWindow(TRUE);
	}
	
	nModuleID = GetModuleID(strModuleName);
	
	if(nModuleID > 0)
		nCurrentModule = nModuleID;
}

CString CAuxDataDlg::GetAuxTypeName(int nAuxType)
{
	switch(nAuxType)
	{
	case PS_AUX_TYPE_TEMPERATURE:	return Fun_FindMsg("GetAuxTypeName_msg1","IDD_AUXDATA_DLG");
		//@ case PS_AUX_TYPE_TEMPERATURE:	return "온도센서";
	case PS_AUX_TYPE_VOLTAGE:		return Fun_FindMsg("GetAuxTypeName_msg2","IDD_AUXDATA_DLG");
		//@ case PS_AUX_TYPE_VOLTAGE:		return "전압측정";
	//$1013 case PS_AUX_TYPE_TEMPERATURE_TH:		return "Thermistor"; //언어파일작업필요. 
	case PS_AUX_TYPE_TEMPERATURE_TH:		return Fun_FindMsg("GetAuxTypeName_msg3","IDD_AUXDATA_DLG");

	//ksj 20200116 : v1016 습도 추가. 다국어화 필요.
	//case PS_AUX_TYPE_HUMIDITY:		return Fun_FindMsg("GetAuxTypeName_msg3","IDD_AUXDATA_DLG");
	case PS_AUX_TYPE_HUMIDITY:		return "습도센서";
	}
	return "";
}

int CAuxDataDlg::GetAuxType(CString strAuxName)
{
	//if(strAuxName == "온도센서")
	if(strAuxName == Fun_FindMsg("AuxDataDlg_GetAuxType_msg1","IDD_AUXDATA_DLG"))//&&
		return PS_AUX_TYPE_TEMPERATURE;
	//if(strAuxName == "전압측정")
	if(strAuxName == Fun_FindMsg("AuxDataDlg_GetAuxType_msg2","IDD_AUXDATA_DLG"))//&&
		return PS_AUX_TYPE_VOLTAGE;
	//if(strAuxName == "써미스터")
	if(strAuxName == Fun_FindMsg("AuxDataDlg_GetAuxType_msg3","IDD_AUXDATA_DLG"))//&&
		return PS_AUX_TYPE_TEMPERATURE_TH;
	//ksj 20200116 : v1016 습도센서 추가. 다국어화 필요
	//if(strAuxName == Fun_FindMsg("AuxDataDlg_GetAuxType_msg3","IDD_AUXDATA_DLG"))//&&
	if(strAuxName == "습도센서")//&&
		return PS_AUX_TYPE_HUMIDITY;

	return -1;
}

//Drag & Drop 시
LRESULT CAuxDataDlg::OnItemDragAndDrop(WPARAM wParam, LPARAM LPARAM)
{
	HTREEITEM hChItem = (HTREEITEM)wParam;
	HTREEITEM hAuxItem = (HTREEITEM)LPARAM;

	CString strCh = m_ctrlAuxTree.GetItemText(hChItem);
	CString strAuxName = m_ctrlAuxTree.GetItemText(hAuxItem);
	CChannelSensor * pSensor = GetSensorData(strAuxName);
	int nCh = 0;
	if(strCh.Left(2) == "Ch")		//Target이 Ch이라면 실행 아니면(여유 Aux 라면) 실행 안함
	{
		nCh = atoi(strCh.Mid(2));		
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
		int nTotalCount = pMD->GetTotalChannel();	
//		int nStartCh = GetStartChNo(nCurrentModule);
		int nStartCh = 1;
//		nCh = nStartCh = (nCh - nStartCh)+1;

		if(nCh > nTotalCount || nCh < nStartCh) return 0;	
		if(pSensor == NULL) return 0;
		if(pSensor->GetMasterChannel() == nCh) return 0;		//같은 채널 안에서는 이동 불가
		CCyclerChannel * pChInfo1 = pMD->GetChannelInfo(nCh-1);

		int nSourseCh = pSensor->GetMasterChannel();

		if(nSourseCh != 0)	//Aux가 여유분 Aux이면 MasterChannel은 0이다.
		{
			CCyclerChannel * pChInfo2 = pMD->GetChannelInfo(nSourseCh-1);

			if(pChInfo1->GetState() == PS_STATE_RUN || pChInfo1->GetState() == PS_STATE_PAUSE ||
				pChInfo2->GetState() == PS_STATE_RUN || pChInfo2->GetState() == PS_STATE_PAUSE ) 
			{
				
				HTREEITEM hUnitItem = m_ctrlAuxTree.GetParentItem(hChItem);

				HTREEITEM hCh = m_ctrlAuxTree.GetChildItem(hUnitItem);
				while(hCh)
				{
					CString strName = m_ctrlAuxTree.GetItemText(hCh);
					if(atoi(strName.Mid(2)) == pSensor->GetMasterChannel())
					{
						m_ctrlAuxTree.InsertItem(m_ctrlAuxTree.GetItemText(hAuxItem), IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hCh);
						m_ctrlAuxTree.DeleteItem(hAuxItem);
					}
					hCh = m_ctrlAuxTree.GetNextItem(hCh, TVGN_NEXT);
				}
				AfxMessageBox(Fun_FindMsg("OnItemDragAndDrop_msg1","IDD_AUXDATA_DLG"));
				//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
				return 0;
			}
		}
		else
		{
			if(pChInfo1->GetState() == PS_STATE_RUN || pChInfo1->GetState() == PS_STATE_PAUSE) 
			{
				
				HTREEITEM hUnitItem = m_ctrlAuxTree.GetParentItem(hChItem);

				HTREEITEM hCh = m_ctrlAuxTree.GetChildItem(hUnitItem);
				while(hCh)
				{
					CString strName = m_ctrlAuxTree.GetItemText(hCh);
					if(atoi(strName.Mid(2)) == pSensor->GetMasterChannel())
					{
						m_ctrlAuxTree.InsertItem(m_ctrlAuxTree.GetItemText(hAuxItem), IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hCh);
						m_ctrlAuxTree.DeleteItem(hAuxItem);
					}
					hCh = m_ctrlAuxTree.GetNextItem(hCh, TVGN_NEXT);
				}
				AfxMessageBox(Fun_FindMsg("OnItemDragAndDrop_msg2","IDD_AUXDATA_DLG"));
				//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
				return 0;
			}
		}
		//	ModifyAux();
		pSensor->SetAuxDivision3();
	}
	else
	{
		if(strCh.Right(8) != strAuxName.Left(8))	//Target과 Aux Type 다르다면 취소
		{
			HTREEITEM hUnitItem = m_ctrlAuxTree.GetParentItem(hChItem);

			HTREEITEM hCh = m_ctrlAuxTree.GetChildItem(hUnitItem);
			while(hCh)
			{
				CString strName = m_ctrlAuxTree.GetItemText(hCh);
				if(atoi(strName.Mid(2)) == pSensor->GetMasterChannel())
				{
					m_ctrlAuxTree.InsertItem(strAuxName, IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hCh);
					m_ctrlAuxTree.DeleteItem(hAuxItem);
				}
				hCh = m_ctrlAuxTree.GetNextItem(hCh, TVGN_NEXT);
			}
			AfxMessageBox(Fun_FindMsg("OnItemDragAndDrop_msg3","IDD_AUXDATA_DLG"));
			//@ AfxMessageBox("센서 타입이 다릅니다.");
			return 0;
		}
	}
	
	//바뀐 채널로 정보를 바꾼다.
	pSensor->SetMasterChannel(nCh);
	CString str = pSensor->GetAuxName();

	if(pSensor->GetAuxName() == "")		//이름이 없으면 센서타입으로 이름을 입력한다.
	{
		CString strName;
		if(pSensor->GetAuxType()==PS_AUX_TYPE_TEMPERATURE_TH) //ksj 20160624 Thermistor일때는 AuxV 개수 빼고 넘버링.18
		{
			CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
			if(pMD)
				strName.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage());
		}
		else if(pSensor->GetAuxType()==PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : v1016 습도 추가. 가구현. 확인필요.
		{
			CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
			if(pMD)
				strName.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage()-pMD->GetMaxTemperatureTh());
		}
		else
		{
			strName.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
		}
		pSensor->SetAuxName(strName);
	}

	m_ctrlAuxTree.SetItemImage(hAuxItem, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
	
	IsChanged = TRUE;
	GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);

	return 1;

}

void CAuxDataDlg::SaveAuxConfig()
{
	CFile file;
	char szBuff[1000];
	
	CString strBuff;
	CString strFileName;
	CString strTemp;
	HTREEITEM hUnit = m_ctrlAuxTree.GetChildItem(hRoot);	//Unit Item

	while(hUnit)
	{
		HTREEITEM hChItem = m_ctrlAuxTree.GetChildItem(hUnit);	//Ch Item;

		while(hChItem)
		{
			if(m_ctrlAuxTree.ItemHasChildren(hChItem))
			{
				TRY
				{
					ZeroMemory(&szBuff, 1000);
					strFileName.Format("%s\\Config\\M%02dCH%02d.aux", strSaveDir,
						atoi(m_ctrlAuxTree.GetItemText(hUnit).Mid(4)),atoi(m_ctrlAuxTree.GetItemText(hChItem).Mid(2)));

					file.Open(strFileName, CFile::modeCreate|CFile::modeWrite);

					HTREEITEM hAuxItem = m_ctrlAuxTree.GetChildItem(hChItem);
									
					while(hAuxItem)
					{
						CChannelSensor * pSensor = GetSensorData(m_ctrlAuxTree.GetItemText(hAuxItem));
						if(pSensor == NULL)	continue;
						strTemp.Format("%d,%d,", pSensor->GetAuxChannelNumber(), pSensor->GetAuxType());
						strBuff.Insert(strBuff.GetLength(), strTemp);
						hAuxItem = m_ctrlAuxTree.GetNextItem(hAuxItem, TVGN_NEXT);
					}
					sprintf(szBuff, strBuff, strBuff.GetLength()-1);
					int nLength = strBuff.GetLength()-1;
					file.Write(szBuff, sizeof(char)*nLength);
					file.Close();

					strBuff = "";
					strTemp = "";
				}CATCH(CFileException, e)
				{
					e->ReportError();
					return;
				}
				END_CATCH
				
			}
			hChItem = m_ctrlAuxTree.GetNextItem(hChItem, TVGN_NEXT);
		}
		
		hUnit = m_ctrlAuxTree.GetNextItem(hUnit, TVGN_NEXT);
	}
	
}

//남아 있는 전체 Aux를 Tree에 추가한다.
void CAuxDataDlg::UpdateRemainAllAux(int nCurrentModule, HTREEITEM hUnit)
{
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
	if(pMD == NULL) return;

	float fTempLow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxTempLow", 0);			//20150810 ljb
	float fTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxTempHigh", 80);			//20150810 ljb
	float fVoltageLow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxVolLow", 1500);		//20150810 ljb
	float fVoltageHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxVolHigh", 4300);		//20150810 ljb

	//ksj 20200128
	int nSafetyFixTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", 80);
	int nSafetyCellVolHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", 4600);
	int nSafetyCellVolLow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltLow", 0);
	//ksj end

	
	HTREEITEM hAux;
	CString strTempAux;

	for(int i = 0 ; i < pMD->GetMaxSensorType(); i++)
	{
		int nCount =0;
		switch(i)
		{
		case 0: nCount = pMD->GetMaxTemperature(); 	break;
		case 1: nCount = pMD->GetMaxVoltage(); break;
		case 2: nCount = pMD->GetMaxTemperatureTh(); break;
		case 3: nCount = pMD->GetMaxHumidity(); break; //ksj 20200116 : v1016 습도 써미스터 추가
		}

		if(nCount > 0)
		{
			strTempAux.Format(Fun_FindMsg("UpdateRemainAllAux_msg","IDD_AUXDATA_DLG"), GetAuxTypeName(i));
			//@ strTempAux.Format("남은 %s", GetAuxTypeName(i));
			hAux = m_ctrlAuxTree.InsertItem(strTempAux, 1, 1, hUnit);			
		}

		for(int j = 0 ; j < nCount; j++)
		{
			//CChannelSensor * pSensor = pMD->GetAuxData(j, i);
			CChannelSensor * pSensor;
			if (i == PS_AUX_TYPE_TEMPERATURE_TH) 
				pSensor = pMD->GetAuxData(j+pMD->GetMaxVoltage(), i);
			else if (i == PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : V1016 습도 추가. 가구현. 확인 필요
				pSensor = pMD->GetAuxData(j+pMD->GetMaxVoltage()+pMD->GetMaxTemperatureTh(), i);
			else
				pSensor = pMD->GetAuxData(j, i);

			if(pSensor != NULL)
			{		
				if(pSensor->GetInstall() == TRUE && pSensor->GetMasterChannel() == 0)
				{
					
					if(i == PS_AUX_TYPE_TEMPERATURE_TH)
						strTempAux.Format("%s %d", GetAuxTypeName(i), pSensor->GetAuxChannelNumber() - pMD->GetMaxVoltage()); //ksj 20160627
					else if(i == PS_AUX_TYPE_HUMIDITY) //ksj 20200116 : v1016 습도 추가. 가구현. 확인 필요
						strTempAux.Format("%s %d", GetAuxTypeName(i), pSensor->GetAuxChannelNumber() - pMD->GetMaxVoltage() - pMD->GetMaxTemperatureTh());
					else
						strTempAux.Format("%s %d", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());

					/*
					{

						//yulee 20180302 97~104 [17PSCSM039]SMPS8CH - Ch별 12V, 24V 세팅 - 하드 코딩
						//strTempAux.Format("%s %d", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
						if (pSensor->GetAuxChannelNumber() > 96 && pSensor->GetAuxChannelNumber() < 105)
						{
							if (pSensor->GetAuxChannelNumber() == 97) strTempAux.Format("%s %d Ch1 12V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber()); //ljb 20170925 add
							if (pSensor->GetAuxChannelNumber() == 98) strTempAux.Format("%s %d Ch2 12V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 99) strTempAux.Format("%s %d Ch3 12V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 100) strTempAux.Format("%s %d Ch4 12V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 101) strTempAux.Format("%s %d Ch1 24V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 102) strTempAux.Format("%s %d Ch2 24V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 103) strTempAux.Format("%s %d Ch3 24V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == 104) strTempAux.Format("%s %d Ch4 24V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber()); 
						}
						else
						{
							strTempAux.Format("%s %d", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
						}
						//yulee 20180302 97~104 [17PSCSM039] 하드코딩 끝
					}
					*/
					m_ctrlAuxTree.InsertItem(strTempAux, IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hAux);

					//20110319 KHS/////////////////////////////////////////////
					STF_MD_AUX_SET_DATA tempAuxData = pSensor->GetAuxData();
					if(i == PS_AUX_TYPE_TEMPERATURE )
					{
						tempAuxData.lMinData = fTempLow*1000.0f;	//ljb 20150810 add
						tempAuxData.lMaxData = fTempHigh*1000.0f;

						//ksj 20200128
						tempAuxData.vent_lower = 0;
						tempAuxData.vent_upper = nSafetyFixTempHigh*1000;
					}
					else if(i == PS_AUX_TYPE_VOLTAGE)
					{
						tempAuxData.lMinData = fVoltageLow*1000.0f;	//ljb 20150810 add
						tempAuxData.lMaxData = fVoltageHigh*1000.0f;
						//if (pSensor->GetAuxChannelNumber() == 51) strTempAux.Format("%s Ch1 12V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber()); //ljb 20170925 add
						//if (pSensor->GetAuxChannelNumber() == 52) strTempAux.Format("%s Ch2 12V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
						//if (pSensor->GetAuxChannelNumber() == 53) strTempAux.Format("%s Ch1 24V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
						//if (pSensor->GetAuxChannelNumber() == 54) strTempAux.Format("%s Ch2 24V", GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
						/*
						//yulee 20180302 97~104 [17PSCSM039]SMPS8CH - Ch별 12V, 24V 세팅 - 하드 코딩
						int nExtSMPSChNo = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "ExtSMPSChNo", 97); //리니어팩의 경우 11부터 시작.
							
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo)	  strTempAux.Format("%s %d Ch1 12V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber()); //ljb 20170925 add
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo+1) strTempAux.Format("%s %d Ch2 12V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo+2) strTempAux.Format("%s %d Ch3 12V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo+3) strTempAux.Format("%s %d Ch4 12V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo+4) strTempAux.Format("%s %d Ch1 24V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo+5) strTempAux.Format("%s %d Ch2 24V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo+6) strTempAux.Format("%s %d Ch3 24V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber());
							if (pSensor->GetAuxChannelNumber() == nExtSMPSChNo+7) strTempAux.Format("%s %d Ch4 24V",GetAuxTypeName(i), pSensor->GetAuxChannelNumber());

							//yulee 20180302 97~104 [17PSCSM039] 하드코딩 끝
						sprintf(tempAuxData.szName,strTempAux.GetBuffer(0)); //ksj 20160627
						*/

						//ksj 20200128
						tempAuxData.vent_lower = nSafetyCellVolLow*1000;
						tempAuxData.vent_upper = nSafetyCellVolHigh*1000;
					}
					else if(i == PS_AUX_TYPE_TEMPERATURE_TH)
					{

 						tempAuxData.lMinData = fTempLow*1000000.0f;	//ljb 20150810 add
 						tempAuxData.lMaxData = fTempHigh*1000000.0f;
						sprintf(tempAuxData.szName,strTempAux.GetBuffer(0)); //ksj 20160627

						//ksj 20200128
						tempAuxData.vent_lower = 0;
						tempAuxData.vent_upper = nSafetyFixTempHigh*1000000;
					}
					//ksj 20200116 : V1016 습도 추가. 단위 확인 필요.
					else if(i == PS_AUX_TYPE_HUMIDITY)
					{
						tempAuxData.lMinData = fTempLow*1000000.0f;
						tempAuxData.lMaxData = fTempHigh*1000000.0f;
						sprintf(tempAuxData.szName,strTempAux.GetBuffer(0)); 
					}

					pSensor->SetAuxData(&tempAuxData);
					////////////////////////////////////////////////////////////
				}
			}
		}
	}
}

void CAuxDataDlg::ModifyAux()
{
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
	if(pMD == NULL)		return;

	CString strData;
	CAddAuxDataDlg dlg(NULL, pMD);
	//m_fSensorMAx

	HTREEITEM hItem = m_ctrlAuxTree.GetFirstSelectedItem();

 	if(hItem)
	{
		CString strTemp = m_ctrlAuxTree.GetItemText(hItem);
		CChannelSensor * pSensor = GetSensorData(strTemp);
		if(pSensor == NULL) return;

		int nChIndex = pSensor->GetMasterChannel()-1; 
		CCyclerChannel * pChInfo = pMD->GetChannelInfo(nChIndex);
		//if(pChInfo == NULL) return; //lyj 20200227 남은센서들도 수정가능하도록..
		if(pChInfo != NULL)
		{
			if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE)  //lyj 20200227 동작중인채널 수정버튼 클릭 시 수정안되도록 수정
			{
				AfxMessageBox(Fun_FindMsg("OnBtnDel_msg","IDD_AUXDATA_DLG"));
				//@ AfxMessageBox("동작중인 채널은 변경 할 수 없습니다.");
				return;
			}
		}
		
		STF_MD_AUX_SET_DATA auxData = pSensor->GetAuxData();	
		
		dlg.Fun_SetArryAuxDivision(m_uiArryAuxCode);	//ljb 2011222 이재복 //////////
		dlg.Fun_SetArryAuxString(m_strArryAuxName);		//ljb 2011222 이재복 //////////
		m_strArryAuxName.GetData();

		dlg.Fun_SetArryAuxThrDivision(m_uiArryAuxThrCode);	//ljb 20160504 이재복 //////////
		dlg.Fun_SetArryAuxThrString(m_strArryAuxThrName);	//ljb 20160504 이재복 //////////

		dlg.Fun_SetArryAuxHumiDivision(m_uiArryAuxHumiCode);	//ksj 20200207
		dlg.Fun_SetArryAuxHumiString(m_strArryAuxHumiName);	//ksj 20200207

		dlg.bModify = TRUE;
		if(pSensor->GetAuxName() == "")
			dlg.m_strSensorName.Format("%s %d", GetAuxTypeName(auxData.auxType), auxData.auxChNo);
		else
			dlg.m_strSensorName = pSensor->GetAuxName();
		dlg.nSensorType = auxData.auxType;
		dlg.nSensorFunction1	= auxData.funtion_division1;
		dlg.nSensorFunction2	= auxData.funtion_division2;
		dlg.nSensorFunction3	= auxData.funtion_division3;
		dlg.nSensorAuxthrType	= auxData.auxTempTableType;			//ljb 20160504 add
		dlg.m_fEndMax = float(m_pDoc->UnitTrans(auxData.lEndMaxData, FALSE, auxData.auxType));
		dlg.m_fEndMin = float(m_pDoc->UnitTrans(auxData.lEndMinData, FALSE, auxData.auxType));
		dlg.m_fSensorMAx	= float(m_pDoc->UnitTrans(auxData.lMaxData, FALSE, auxData.auxType));
		dlg.m_fSensorMin	= float(m_pDoc->UnitTrans(auxData.lMinData, FALSE, auxData.auxType));
		dlg.m_bUseVentLink = auxData.vent_use_flag; //ksj 20200116 : v1016 vent 안전기능 추가
		
		if(dlg.DoModal() == IDOK)
		{
			float fltTmp;
			fltTmp = 0; //yulee 20181129
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMax, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp	 = m_pDoc->UnitTrans(dlg.m_fEndMax, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lEndMaxData = atol(strData);
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMin, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp = m_pDoc->UnitTrans(dlg.m_fEndMin, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lEndMinData = atol(strData);
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMAx, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp = m_pDoc->UnitTrans(dlg.m_fSensorMAx, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lMaxData = atol(strData);
		//	strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMin, TRUE, auxData.auxType));
			fltTmp = 0;
			fltTmp = m_pDoc->UnitTrans(dlg.m_fSensorMin, TRUE, auxData.auxType);
			strData.Format("%.0f",fltTmp);
			auxData.lMinData = atol(strData);
			/*
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMax, TRUE, auxData.auxType));
			auxData.lEndMaxData = atol(strData);
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fEndMin, TRUE, auxData.auxType));
			auxData.lEndMinData = atol(strData);
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMAx, TRUE, auxData.auxType));
			auxData.lMaxData = atol(strData);
			strData.Format("%.3f",m_pDoc->UnitTrans(dlg.m_fSensorMin, TRUE, auxData.auxType));
			auxData.lMinData = atol(strData);
			*/

			auxData.funtion_division1 = dlg.nSensorFunction1;
			auxData.funtion_division2 = dlg.nSensorFunction2;
			auxData.funtion_division3 = dlg.nSensorFunction3;
			auxData.auxTempTableType = dlg.nSensorAuxthrType;

			auxData.vent_use_flag = BYTE(dlg.m_bUseVentLink); //ksj 20200116 : V1016 : vent 연동 여부

			ZeroMemory(&auxData.szName, sizeof(auxData.szName));
			
			sprintf(auxData.szName, dlg.m_strSensorName);
			pSensor->SetAuxData(&auxData);
			UpdateAuxTreeItem(hItem);
			m_ctrlAuxTree.SetItemImage(hItem, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
			IsChanged = TRUE;
		}
		//hItem = m_ctrlAuxTree.GetNextSelectedItem(hItem);
	}
	GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);	
}

void CAuxDataDlg::UpdateRemainAux(int nMoudle, HTREEITEM hUnit, HTREEITEM hItem, BOOL IsDelete)
{
	HTREEITEM hParent = m_ctrlAuxTree.GetChildItem(hUnit);
	while(hParent)
	{
#ifdef _DEBUG
		TRACE("%s %s\n",m_ctrlAuxTree.GetItemText(hParent).Right(8), m_ctrlAuxTree.GetItemText(hItem).Left(8));
#endif
		//if(m_ctrlAuxTree.GetItemText(hParent).Right(8) == m_ctrlAuxTree.GetItemText(hItem).Left(8))
		if(m_ctrlAuxTree.GetItemText(hParent).Right(8) == m_ctrlAuxTree.GetItemText(hItem).Left(8) || 
			m_ctrlAuxTree.GetItemText(hParent).Find(m_ctrlAuxTree.GetItemText(hItem).Left(8)) > 0 )//ksj 20200121 : 영문 처리 추가.
		{
			if(IsDelete)
			{
				HTREEITEM hDel = m_ctrlAuxTree.GetChildItem(hParent);
				while(hDel)
				{
					if(m_ctrlAuxTree.GetItemText(hDel) == m_ctrlAuxTree.GetItemText(hItem))
					{
						m_ctrlAuxTree.DeleteItem(hDel);
						break;
					}
					hDel = m_ctrlAuxTree.GetNextItem(hDel, TVGN_NEXT);
				}
				
			}
			else
				m_ctrlAuxTree.InsertItem(m_ctrlAuxTree.GetItemText(hItem),IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM, hParent);
		}
		hParent = m_ctrlAuxTree.GetNextItem(hParent, TVGN_NEXT);
	}
	
}


void CAuxDataDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnLButtonDown(nFlags, point);
}

void CAuxDataDlg::OnBtnAllApply() 
{
	if(m_ctrlSensorList.GetSelectedCount() <= 0)
		return;

	CString strTemp, strAuxName;
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);

	int nChIndex = 0;
	int i,j,iStartPos=0;
	int nSelectAuxType=2;
	CChannelSensor * pSensor;
	CChannelSensor * Temp;
	STF_MD_AUX_SET_DATA auxData;
	STF_MD_AUX_SET_DATA tempAuxData;
	for(int i = 0 ; i < m_ctrlSensorList.GetItemCount() ;  i++) 
	{
		UINT uState = m_ctrlSensorList.GetItemState(i, LVIS_SELECTED); 	
		if(uState & LVIS_SELECTED)
		{
			iStartPos = i;
			strTemp = m_ctrlSensorList.GetItemText(i, 0);
			strAuxName = strTemp + " ";
			strTemp = m_ctrlSensorList.GetItemText(i, 1);
			strAuxName += strTemp;
			pSensor = GetSensorData(strAuxName);
			nChIndex = pSensor->GetMasterChannel();
			auxData = pSensor->GetAuxData();
			nSelectAuxType = pSensor->GetAuxType();
			break;
		}
	}

	//lmh 20120524 그리드가 비워져 있어서 수정이 안됨  그래서 이부분이 추가되야함
////////////////////////////////////////////////////////////////////
	HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();
	
	if(m_ctrlAuxTree.ItemHasChildren(hItem) == FALSE)
	{
		hItem = m_ctrlAuxTree.GetParentItem(hItem);
	}
	CString strName = m_ctrlAuxTree.GetItemText(hItem);
	
	if(strName.Left(2) == "Ch")
	{
		HTREEITEM hChild = m_ctrlAuxTree.GetChildItem(hItem);
		while(hChild)
		{
			CString strAuxName2 = m_ctrlAuxTree.GetItemText(hChild);
			
			if(strAuxName != strAuxName2)
			{
				m_ctrlAuxTree.SetItemImage(hChild, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
				
				m_ctrlSensorList.DeleteAllItems();
				AddSensorListChannelItem(hItem);
			}
			hChild = m_ctrlAuxTree.GetNextItem(hChild, TVGN_NEXT);
			
		}
	}
//////////////////////////////////////////////////////////////////////////////////
	for(j = iStartPos ; j < m_ctrlSensorList.GetItemCount() ;  j++) 
	{
// 			int nCount = 0;
// 			switch(pSensor->GetAuxType())
// 			{
// 				case 0: nCount = pMD->GetMaxTemperature(); 	break;
// 				case 1: nCount = pMD->GetMaxVoltage(); break;
// 				case 2: nCount = 0; break;
// 			}
		strTemp = m_ctrlSensorList.GetItemText(j, 0);
		strAuxName = strTemp + " ";
		strTemp = m_ctrlSensorList.GetItemText(j, 1);
		strAuxName += strTemp;
		Temp = GetSensorData(strAuxName);
		tempAuxData = Temp->GetAuxData();
		if (Temp->GetAuxType() != pSensor->GetAuxType()) continue;
		
// 		Temp = pMD->GetAuxData(j, pSensor->GetAuxType());
		if(Temp != NULL)
		{		
			if(Temp->GetInstall() == TRUE && Temp->GetMasterChannel() == pSensor->GetMasterChannel())
			{
// 				STF_MD_AUX_SET_DATA tempAuxData = Temp->GetAuxData();
				tempAuxData.lEndMaxData = auxData.lEndMaxData;
				tempAuxData.lEndMinData	= auxData.lEndMinData;
				tempAuxData.lMaxData	= auxData.lMaxData;
				tempAuxData.lMinData	= auxData.lMinData;
				tempAuxData.funtion_division1 = auxData.funtion_division1;
				tempAuxData.funtion_division2 = auxData.funtion_division2;
				tempAuxData.funtion_division3 = auxData.funtion_division3;

				//ksj 20200824 : 벤트 연동 check 박스도 일괄 적용 시킴 (오창 LGC 요청 사항)
				tempAuxData.vent_use_flag = auxData.vent_use_flag;
				//ksj end

				Temp->SetAuxData(&tempAuxData);
				IsChanged = TRUE;
			}
		}
	}

	if(IsChanged == TRUE)
	{
		HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();

		if(m_ctrlAuxTree.ItemHasChildren(hItem) == FALSE)
		{
			hItem = m_ctrlAuxTree.GetParentItem(hItem);
		}
		CString strName = m_ctrlAuxTree.GetItemText(hItem);

		if(strName.Left(2) == "Ch")
		{
			HTREEITEM hChild = m_ctrlAuxTree.GetChildItem(hItem);
			while(hChild)
			{
				CString strAuxName2 = m_ctrlAuxTree.GetItemText(hChild);
				
				if(strAuxName != strAuxName2)
				{
					m_ctrlAuxTree.SetItemImage(hChild, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
					
					m_ctrlSensorList.DeleteAllItems();
					AddSensorListChannelItem(hItem);
				}
				hChild = m_ctrlAuxTree.GetNextItem(hChild, TVGN_NEXT);
				
			}
		}
	}
	GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);	

	
}

CString CAuxDataDlg::Fun_GetAuxCodeName(int iCode)
{
	CString strTemp;
	for (int i=0; i < m_uiArryAuxCode.GetSize(); i++)
	{
		if (iCode == m_uiArryAuxCode.GetAt(i))
		{
			strTemp.Format("%s [%d]",m_strArryAuxName.GetAt(i), m_uiArryAuxCode.GetAt(i));
			return strTemp;
		}
	}
	strTemp.Format("None [%d]",iCode);
	return strTemp;
}

CString CAuxDataDlg::Fun_GetAuxThrCodeName(int iCode)
{
	CString strTemp;
	for (int i=0; i < m_uiArryAuxThrCode.GetSize(); i++)
	{
		if (iCode == m_uiArryAuxThrCode.GetAt(i))
		{
			strTemp.Format("%s [%d]",m_strArryAuxThrName.GetAt(i), m_uiArryAuxThrCode.GetAt(i));
			return strTemp;
		}
	}
	strTemp.Format("None [%d]",iCode);
	return strTemp;
}

//ksj 20200207
CString CAuxDataDlg::Fun_GetAuxHumiCodeName(int iCode)
{
	CString strTemp;
	for (int i=0; i < m_uiArryAuxHumiCode.GetSize(); i++)
	{
		if (iCode == m_uiArryAuxHumiCode.GetAt(i))
		{
			strTemp.Format("%s [%d]",m_strArryAuxHumiName.GetAt(i), m_uiArryAuxHumiCode.GetAt(i));
			return strTemp;
		}
	}
	strTemp.Format("None [%d]",iCode);
	return strTemp;
}

void CAuxDataDlg::OnBtnCarbonApply() 
{
// 	CLoginManagerDlg LoginDlg;
// 	if(LoginDlg.DoModal() != IDOK) return;

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxTempLow", 0);			//20150810 ljb
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxTempHigh", 80);			//20150810 ljb
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxVolLow", 1500);		//20150810 ljb
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxVolHigh", 4300);		//20150810 ljb

	
	Fun_ApplaySafety(4300000,1500000,80000,0);	//Carbon cell	
	
//	InitTreeList();
}

void CAuxDataDlg::OnBtnLtoApply() 
{
// 	CLoginManagerDlg LoginDlg;
// 	if(LoginDlg.DoModal() != IDOK) return;

// 	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxTempLow", 0);			//20150810 ljb
// 	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxTempHigh", 80);			//20150810 ljb
// 	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxVolLow", 1000);		//20150810 ljb
// 	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "AuxVolHigh", 2900);		//20150810 ljb

	Fun_ApplaySafety(2900000,1000000,80000,0);	//LTO cell	
	
//	InitTreeList();
}

void CAuxDataDlg::Fun_ApplaySafety(long lCellMax, long lCellMin, long lAuxTempMax, long lAuxTempMin)
{

	if(m_ctrlSensorList.GetSelectedCount() <= 0)
		return;

	CString strTemp, strAuxName;
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);

	int nChIndex = 0;
	int i,j,iStartPos=0;
	int nSelectAuxType=2;
	CChannelSensor * pSensor;
	CChannelSensor * Temp;
	STF_MD_AUX_SET_DATA auxData;
	STF_MD_AUX_SET_DATA tempAuxData;
// 	for(int i = 0 ; i < m_ctrlSensorList.GetItemCount() ;  i++) 
// 	{
// 		UINT uState = m_ctrlSensorList.GetItemState(i, LVIS_SELECTED); 	
// 		if(uState & LVIS_SELECTED)
// 		{
// 			iStartPos = i;
// 			strTemp = m_ctrlSensorList.GetItemText(i, 0);
// 			strAuxName = strTemp + " ";
// 			strTemp = m_ctrlSensorList.GetItemText(i, 1);
// 			strAuxName += strTemp;
// 			pSensor = GetSensorData(strAuxName);
// 			nChIndex = pSensor->GetMasterChannel();
// 			auxData = pSensor->GetAuxData();
// 			nSelectAuxType = pSensor->GetAuxType();
// 			break;
// 		}
// 	}

	//lmh 20120524 그리드가 비워져 있어서 수정이 안됨  그래서 이부분이 추가되야함
////////////////////////////////////////////////////////////////////
// 	HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();
// 	
// 	if(m_ctrlAuxTree.ItemHasChildren(hItem) == FALSE)
// 	{
// 		hItem = m_ctrlAuxTree.GetParentItem(hItem);
// 	}
// 	CString strName = m_ctrlAuxTree.GetItemText(hItem);
// 	
// 	if(strName.Left(2) == "Ch")
// 	{
// 		HTREEITEM hChild = m_ctrlAuxTree.GetChildItem(hItem);
// 		while(hChild)
// 		{
// 			CString strAuxName2 = m_ctrlAuxTree.GetItemText(hChild);
// 			
// 			if(strAuxName != strAuxName2)
// 			{
// 				m_ctrlAuxTree.SetItemImage(hChild, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
// 				
// 				m_ctrlSensorList.DeleteAllItems();
// 				AddSensorListChannelItem(hItem);
// 			}
// 			hChild = m_ctrlAuxTree.GetNextItem(hChild, TVGN_NEXT);
// 			
// 		}
// 	}
//////////////////////////////////////////////////////////////////////////////////
	for(j = 0 ; j < m_ctrlSensorList.GetItemCount() ;  j++) 
	{
// 			int nCount = 0;
// 			switch(pSensor->GetAuxType())
// 			{
// 				case 0: nCount = pMD->GetMaxTemperature(); 	break;
// 				case 1: nCount = pMD->GetMaxVoltage(); break;
// 				case 2: nCount = 0; break;
// 			}
		strTemp = m_ctrlSensorList.GetItemText(j, 0);
		strAuxName = strTemp + " ";
		strTemp = m_ctrlSensorList.GetItemText(j, 1);
		strAuxName += strTemp;
		pSensor = GetSensorData(strAuxName);
		nChIndex = pSensor->GetMasterChannel();
		auxData = pSensor->GetAuxData();
		nSelectAuxType = pSensor->GetAuxType();
// 		Temp = GetSensorData(strAuxName);
// 		tempAuxData = Temp->GetAuxData();
//		if (Temp->GetAuxType() != pSensor->GetAuxType()) continue;
		
// 		Temp = pMD->GetAuxData(j, pSensor->GetAuxType());
		if(pSensor != NULL)
		{		
//			if(Temp->GetInstall() == TRUE && Temp->GetMasterChannel() == pSensor->GetMasterChannel())
//			{
// 				STF_MD_AUX_SET_DATA tempAuxData = Temp->GetAuxData();
			//AUX Type (0 : 온도센서, 1:Voltage)
			if (nSelectAuxType ==  0)
			{
// 				auxData.lEndMaxData = auxData.lEndMaxData;
// 				auxData.lEndMinData	= auxData.lEndMinData;
				auxData.lMaxData	= lAuxTempMax;
				auxData.lMinData	= lAuxTempMin;
			}
			else if (nSelectAuxType ==  1)
			{
// 				tempAuxData.lEndMaxData = auxData.lEndMaxData;
// 				tempAuxData.lEndMinData	= auxData.lEndMinData;
				auxData.lMaxData	= lCellMax;
				auxData.lMinData	= lCellMin;
// 				auxData.funtion_division1 = auxData.funtion_division1;
// 				auxData.funtion_division2 = auxData.funtion_division2;
// 				auxData.funtion_division3 = auxData.funtion_division3;

			}
			pSensor->SetAuxData(&auxData);
// 			sprintf(auxData.szName, dlg.m_strSensorName);
// 			pSensor->SetAuxData(&auxData);
// 			UpdateAuxTreeItem(hItem);
// 			m_ctrlAuxTree.SetItemImage(hItem, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
			IsChanged = TRUE;

		}
	}

	if(IsChanged == TRUE)
	{
		HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();

		if(m_ctrlAuxTree.ItemHasChildren(hItem) == FALSE)
		{
			hItem = m_ctrlAuxTree.GetParentItem(hItem);
		}
		CString strName = m_ctrlAuxTree.GetItemText(hItem);

		if(strName.Left(2) == "Ch")
		{
			HTREEITEM hChild = m_ctrlAuxTree.GetChildItem(hItem);
			while(hChild)
			{
				CString strAuxName2 = m_ctrlAuxTree.GetItemText(hChild);
				
//				if(strAuxName != strAuxName2)
				{
					m_ctrlAuxTree.SetItemImage(hChild, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
					
					m_ctrlSensorList.DeleteAllItems();
					AddSensorListChannelItem(hItem);
				}
				hChild = m_ctrlAuxTree.GetNextItem(hChild, TVGN_NEXT);
				
			}
		}
	}
	GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);	
}

//20180305 yulee 첫번째 Thermistor 센서 선택 후 나머지 Thermistor 적용 버튼
void CAuxDataDlg::OnBtnAllApplyTherSensor() 
{
	if(m_ctrlSensorList.GetSelectedCount() <= 0)
	return;

	CString strTemp, strAuxName;
	CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);

	int nChIndex = 0;
	int i,j,iStartPos=0;
	int nSelectAuxType=2;
	int nThermistorType=0;
	CChannelSensor * pSensor;
	CChannelSensor * Temp;
	STF_MD_AUX_SET_DATA auxData;
	STF_MD_AUX_SET_DATA tempAuxData;
	for(int i = 0 ; i < m_ctrlSensorList.GetItemCount() ;  i++) 
	{
		UINT uState = m_ctrlSensorList.GetItemState(i, LVIS_SELECTED); 	
		if(uState & LVIS_SELECTED)
		{
			iStartPos = i;
			strTemp = m_ctrlSensorList.GetItemText(i, 0);
			strAuxName = strTemp + " ";
			strTemp = m_ctrlSensorList.GetItemText(i, 1);
			strAuxName += strTemp;
			pSensor = GetSensorData(strAuxName);
			nChIndex = pSensor->GetMasterChannel();
			auxData = pSensor->GetAuxData();
			nSelectAuxType = pSensor->GetAuxType();
			nThermistorType = pSensor->GetAuxData().auxTempTableType; //20180305 yulee add
			break;
		}
	}
	//if(nSelectAuxType != 2)
	if(nSelectAuxType != PS_AUX_TYPE_TEMPERATURE_TH
		&& nSelectAuxType != PS_AUX_TYPE_HUMIDITY) //ksj 20200211 : 습도 센서일때도 선택 가능하게 허용.
	{
		//AfxMessageBox(_T("Only Thermister type"));
		AfxMessageBox(_T("can apply only Thermister or Humidity type")); //ksj 20200211
		return;
	}

	//lmh 20120524 그리드가 비워져 있어서 수정이 안됨  그래서 이부분이 추가되야함
////////////////////////////////////////////////////////////////////
	HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();
	
	if(m_ctrlAuxTree.ItemHasChildren(hItem) == FALSE)
	{
		hItem = m_ctrlAuxTree.GetParentItem(hItem);
	}
	CString strName = m_ctrlAuxTree.GetItemText(hItem);
	
	if(strName.Left(2) == "Ch")
	{
		HTREEITEM hChild = m_ctrlAuxTree.GetChildItem(hItem);
		while(hChild)
		{
			CString strAuxName2 = m_ctrlAuxTree.GetItemText(hChild);
			
			if(strAuxName != strAuxName2)
			{
				m_ctrlAuxTree.SetItemImage(hChild, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
				
				m_ctrlSensorList.DeleteAllItems();
				AddSensorListChannelItem(hItem);
			}
			hChild = m_ctrlAuxTree.GetNextItem(hChild, TVGN_NEXT);
			
		}
	}
//////////////////////////////////////////////////////////////////////////////////
	for(j = iStartPos ; j < m_ctrlSensorList.GetItemCount() ;  j++) 
	{
// 			int nCount = 0;
// 			switch(pSensor->GetAuxType())
// 			{
// 				case 0: nCount = pMD->GetMaxTemperature(); 	break;
// 				case 1: nCount = pMD->GetMaxVoltage(); break;
// 				case 2: nCount = 0; break;
// 			}
		strTemp = m_ctrlSensorList.GetItemText(j, 0);
		strAuxName = strTemp + " ";
		strTemp = m_ctrlSensorList.GetItemText(j, 1);
		strAuxName += strTemp;
		Temp = GetSensorData(strAuxName);
		tempAuxData = Temp->GetAuxData();
		if (Temp->GetAuxType() != pSensor->GetAuxType()) continue;
		
// 		Temp = pMD->GetAuxData(j, pSensor->GetAuxType());
		if(Temp != NULL)
		{		
			if(Temp->GetInstall() == TRUE && Temp->GetMasterChannel() == pSensor->GetMasterChannel())
			{
// 				tempAuxData.lEndMaxData = auxData.lEndMaxData;
// 				tempAuxData.lEndMinData	= auxData.lEndMinData;
// 				tempAuxData.lMaxData	= auxData.lMaxData;
// 				tempAuxData.lMinData	= auxData.lMinData;
// 				tempAuxData.funtion_division1 = auxData.funtion_division1;
// 				tempAuxData.funtion_division2 = auxData.funtion_division2;
//				tempAuxData.funtion_division3 = auxData.funtion_division3;
				tempAuxData.auxTempTableType = auxData.auxTempTableType;  //20180305 yulee add

				//ksj 20200824 : 벤트 연동 check 박스도 일괄 적용 시킴 (오창 LGC 요청 사항)
				//tempAuxData.vent_use_flag = auxData.vent_use_flag;
				//ksj end

				Temp->SetAuxData(&tempAuxData);
				IsChanged = TRUE;
			}
		}
	}

	if(IsChanged == TRUE)
	{
		HTREEITEM hItem = m_ctrlAuxTree.GetSelectedItem();

		if(m_ctrlAuxTree.ItemHasChildren(hItem) == FALSE)
		{
			hItem = m_ctrlAuxTree.GetParentItem(hItem);
		}
		CString strName = m_ctrlAuxTree.GetItemText(hItem);

		if(strName.Left(2) == "Ch")
		{
			HTREEITEM hChild = m_ctrlAuxTree.GetChildItem(hItem);
			while(hChild)
			{
				CString strAuxName2 = m_ctrlAuxTree.GetItemText(hChild);
				
				if(strAuxName != strAuxName2)
				{
					m_ctrlAuxTree.SetItemImage(hChild, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
					
					m_ctrlSensorList.DeleteAllItems();
					AddSensorListChannelItem(hItem);
				}
				hChild = m_ctrlAuxTree.GetNextItem(hChild, TVGN_NEXT);
				
			}
		}
	}
	GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);	

	
}

void CAuxDataDlg::OnBtnMove() //20180424 yulee add
{
//모듈 1개에 대한 기능
//외부센서에 대한 이동을 드래그 앤 드랍 뿐만 아니라 버튼 클릭으로 되도록 수정(외부센서 많을 때 불편) 
//[17PSCSA039]SK이노베이션 120V400A-4CH_18SET 고객사 요청
// TODO: Add your control notification handler code here


		int FromChNo, ToChNo;
		CString FromChNM, ToChNM, MainToChNM;
		ToChNo = m_CboChList.GetCurSel();
		CString tmpStr;

		//$1013 tmpStr.Format(_T("이동할 채널 : %d"), ToChNo); 
		tmpStr.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg1","IDD_AUXDATA_DLG"), ToChNo); //&&
		if(tmpStr.Find(_T("")))
		{
			tmpStr.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg2","IDD_AUXDATA_DLG")); //&&
			return;
		}
		switch(ToChNo)
		{
			case 0 :
				{
					ToChNM.Format(_T(""));
					MainToChNM.Format(_T(""));
					AfxMessageBox(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg3","IDD_AUXDATA_DLG")); //&&
					break;
				}
			case 1 :
				{
					ToChNM.Format(_T("Ch 1"));
					MainToChNM.Format(_T("Ch 1"));
					break;
				}
			case 2 :
				{
					ToChNM.Format(_T("Ch 2"));
					MainToChNM.Format(_T("Ch 2"));
					break;
			}
			case 3 :
				{
					ToChNM.Format(_T("Ch 3"));
					MainToChNM.Format(_T("Ch 3"));
					break;
				}
			case 4 :
				{
					ToChNM.Format(_T("Ch 4"));
					MainToChNM.Format(_T("Ch 4"));
					break;
				}
			case 5 :
				{
					//ToChNM.Format(_T("할당 해제"));
					ToChNM.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg4","IDD_AUXDATA_DLG"));//&&
					//MainToChNM.Format(_T("할당 해제"));
					MainToChNM.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg5","IDD_AUXDATA_DLG"));//&&
					break;
				}
			default:
				break;
		}
		
		//HTREEITEM hAuxItem = m_ctrlAuxTree.GetSelectedItem();
	
	HTREEITEM hAuxRootHd = m_ctrlAuxTree.GetRootItem();
	HTREEITEM hAuxItem = FindTreNodeStateSelected(&m_ctrlAuxTree, hAuxRootHd);
	while(hAuxItem)
	{
		CString strAuxName = m_ctrlAuxTree.GetItemText(hAuxItem);
		//tmpStr.Format(_T("Selected Item: %s"), strAuxName);
		CChannelSensor * pSensor = GetSensorData(strAuxName);

		//if(MainToChNM.Find(_T("할당 해제"))>-1)
		if(MainToChNM.Find(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg6","IDD_AUXDATA_DLG"))>-1) //&&
		{
			if(pSensor->GetAuxType() == PS_AUX_TYPE_TEMPERATURE)
				//ToChNM.Format(_T("남은 온도센서"));
				ToChNM.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg7","IDD_AUXDATA_DLG"));//&&
			if(pSensor->GetAuxType() == PS_AUX_TYPE_VOLTAGE)
				//ToChNM.Format(_T("남은 전압측정"));
				ToChNM.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg8","IDD_AUXDATA_DLG"));//&&
			if(pSensor->GetAuxType() == PS_AUX_TYPE_TEMPERATURE_TH)
				//ToChNM.Format(_T("남은 써미스터"));
				ToChNM.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg9","IDD_AUXDATA_DLG"));//&&
			//ksj 20200116 : v1016 습도 추가. 다국어화 필요.
			if(pSensor->GetAuxType() == PS_AUX_TYPE_HUMIDITY)
				ToChNM.Format(_T("남은 습도센서"));
				//ToChNM.Format(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg9","IDD_AUXDATA_DLG"));//&&
		}

		//이동할 채널가져오기
		//HTREEITEM hAuxRootHd = m_ctrlAuxTree.GetRootItem();
		HTREEITEM hChItem = FindTreeData(&m_ctrlAuxTree, hAuxRootHd, ToChNM);



		CString strCh = m_ctrlAuxTree.GetItemText(hChItem);
		int nCh = 0;
		if(strCh.Left(2) == "Ch")		//Target이 Ch이라면 실행 아니면(여유 Aux 라면) 실행 안함
		{
			nCh = atoi(strCh.Mid(2));		
			CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
			int nTotalCount = pMD->GetTotalChannel();	
	//		int nStartCh = GetStartChNo(nCurrentModule);
			int nStartCh = 1;
	//		nCh = nStartCh = (nCh - nStartCh)+1;

			if(nCh > nTotalCount || nCh < nStartCh) return;	
			if(pSensor == NULL) return;
			if(pSensor->GetMasterChannel() == nCh) return;		//같은 채널 안에서는 이동 불가
			CCyclerChannel * pChInfo1 = pMD->GetChannelInfo(nCh-1);

			int nSourseCh = pSensor->GetMasterChannel();

			if(nSourseCh != 0)	//Aux가 여유분 Aux이면 MasterChannel은 0이다.
			{
				CCyclerChannel * pChInfo2 = pMD->GetChannelInfo(nSourseCh-1);

				if(pChInfo1->GetState() == PS_STATE_RUN || pChInfo1->GetState() == PS_STATE_PAUSE ||
					pChInfo2->GetState() == PS_STATE_RUN || pChInfo2->GetState() == PS_STATE_PAUSE ) 
				{
					
					HTREEITEM hUnitItem = m_ctrlAuxTree.GetParentItem(hChItem);

					HTREEITEM hCh = m_ctrlAuxTree.GetChildItem(hUnitItem);
					while(hCh)
					{
						CString strName = m_ctrlAuxTree.GetItemText(hCh);
						if(atoi(strName.Mid(2)) == pSensor->GetMasterChannel())
						{
							m_ctrlAuxTree.InsertItem(m_ctrlAuxTree.GetItemText(hAuxItem), IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hCh);
							m_ctrlAuxTree.DeleteItem(hAuxItem);
						}
						hCh = m_ctrlAuxTree.GetNextItem(hCh, TVGN_NEXT);
					}
					//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					AfxMessageBox(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg10","IDD_AUXDATA_DLG"));//&&
					return;
				}
			}
			else
			{
				if(pChInfo1->GetState() == PS_STATE_RUN || pChInfo1->GetState() == PS_STATE_PAUSE) 
				{
					
					HTREEITEM hUnitItem = m_ctrlAuxTree.GetParentItem(hChItem);

					HTREEITEM hCh = m_ctrlAuxTree.GetChildItem(hUnitItem);
					while(hCh)
					{
						CString strName = m_ctrlAuxTree.GetItemText(hCh);
						if(atoi(strName.Mid(2)) == pSensor->GetMasterChannel())
						{
							m_ctrlAuxTree.InsertItem(m_ctrlAuxTree.GetItemText(hAuxItem), IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hCh);
							m_ctrlAuxTree.DeleteItem(hAuxItem);
						}
						hCh = m_ctrlAuxTree.GetNextItem(hCh, TVGN_NEXT);
					}
					//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
					AfxMessageBox(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg11","IDD_AUXDATA_DLG"));//&&
					return;
				}
			}
			//	ModifyAux();
			pSensor->SetAuxDivision3();
		}
		else
		{
			if(strCh.Right(8) != strAuxName.Left(8))	//Target과 Aux Type 다르다면 취소
			{
				HTREEITEM hUnitItem = m_ctrlAuxTree.GetParentItem(hChItem);

				HTREEITEM hCh = m_ctrlAuxTree.GetChildItem(hUnitItem);
				while(hCh)
				{
					CString strName = m_ctrlAuxTree.GetItemText(hCh);
					if(atoi(strName.Mid(2)) == pSensor->GetMasterChannel())
					{
						m_ctrlAuxTree.InsertItem(strAuxName, IMAGE_AUX_TREE_ITEM, IMAGE_AUX_TREE_ITEM, hCh);
						m_ctrlAuxTree.DeleteItem(hAuxItem);
					}
					hCh = m_ctrlAuxTree.GetNextItem(hCh, TVGN_NEXT);
				}
				//AfxMessageBox("센서 타입이 다릅니다.");
				AfxMessageBox(Fun_FindMsg("AuxDataDlg_OnBtnMove_msg12","IDD_AUXDATA_DLG"));//&&
				return;
			}
		}

		//바뀐 채널로 정보를 바꾼다.
		pSensor->SetMasterChannel(nCh);
		if(pSensor->GetAuxName() == "")		//이름이 없으면 센서타입으로 이름을 입력한다.
		{
			CString strName;
			if(pSensor->GetAuxType()==PS_AUX_TYPE_TEMPERATURE_TH) //ksj 20160624 Thermistor일때는 AuxV 개수 빼고 넘버링.
			{
				CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
				if(pMD)
					strName.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage());
			}
			else if(pSensor->GetAuxType()==PS_AUX_TYPE_HUMIDITY) //ksj 20200116 습도일때는 AuxV 개수 빼고 넘버링. 가구현. 확인 필요
			{
				CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
				if(pMD)
					strName.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber()-pMD->GetMaxVoltage()-pMD->GetMaxTemperatureTh());
			}
			else
			{
				strName.Format("%s %d",GetAuxTypeName(pSensor->GetAuxType()), pSensor->GetAuxChannelNumber());
			}

			pSensor->SetAuxName(strName);
		}
		
		m_ctrlAuxTree.SetItemImage(hAuxItem, IMAGE_TEMPORARY_AUX_TREE_ITEM, IMAGE_TEMPORARY_AUX_TREE_ITEM);
		
		IsChanged = TRUE;
		GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);

		hAuxItem = FindTreNodeStateSelected(&m_ctrlAuxTree, hAuxRootHd);
	}
		
}

HTREEITEM CAuxDataDlg::FindTreeData(CTreeCtrl* pTree, HTREEITEM hitem, CString strTagetNM) //20180424 yulee add
{
	HTREEITEM hitemFind, hItemChild, hItemSibling;
	hitemFind = hItemChild = hItemSibling = NULL;

	if(pTree->GetItemText(hitem) == strTagetNM)
	{
		hitemFind = hitem;
	}
	else
	{
		//자식 노드를 찾는다. 
		hItemChild = pTree->GetChildItem(hitem);
		if(hItemChild)
		{
			hitemFind = FindTreeData(pTree, hItemChild,strTagetNM);
		}
		//형제노드를 찾는다.
		hItemSibling = pTree->GetNextSiblingItem(hitem);
		if(hitemFind==NULL && hItemSibling)
		{
			hitemFind = FindTreeData(pTree, hItemSibling, strTagetNM);
		}
	}
	return hitemFind;
}

HTREEITEM CAuxDataDlg::FindTreNodeStateSelected(CTreeCtrl* pTree, HTREEITEM hitem) //20180424 yulee add
{
	HTREEITEM hitemFind, hItemChild, hItemSibling;
	hitemFind = hItemChild = hItemSibling = NULL;
	
	if(pTree->GetItemState(hitem, LVIS_SELECTED)&LVIS_SELECTED)
	{
		hitemFind = hitem;
		pTree->SetItemState(hitem, 0, LVIS_SELECTED);
	}
	else  
	{
		//자식 노드를 찾는다. 
		hItemChild = pTree->GetChildItem(hitem);
		if(hItemChild)
		{
			hitemFind = FindTreNodeStateSelected(pTree, hItemChild);
		}
		//형제노드를 찾는다.
		hItemSibling = pTree->GetNextSiblingItem(hitem);
		if(hitemFind==NULL && hItemSibling)
		{
			hitemFind = FindTreNodeStateSelected(pTree, hItemSibling);
		}
	}
	return hitemFind;
}

//ksj 20210512 : Aux 초기화 기능추가. (ATS 연동때문에 간혹 Aux 설정이 뒤죽박죽 꼬이는 경우가 있으므로, GUI에서 초기화 할 수 있도록 기능 추가
void CAuxDataDlg::OnBnClickedBtnInitialize()
{
	CLoginManagerDlg LoginDlg;
	if(LoginDlg.DoModal() != IDOK) return;
	
	m_pDoc->WriteLog("DEBUG:::::::::::::::::  aux 초기화 버튼 누름"); //ksj 20201118 : TEST

	if(SendAllAuxDataToModule(TRUE) == TRUE)
	{
		m_pDoc->WriteLog("DEBUG:::::::::::::::::  aux 초기화 TRUE"); //ksj 20201118 : TEST

		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nCurrentModule);
		pMD->ApplyAuxData();	//Aux Save 포함
		ZeroMemory(&bColor, sizeof(BOOL)*_SFT_MAX_MAPPING_AUX);
		Invalidate(TRUE);
		IsChanged = FALSE;

		Sleep(3000);
		
		GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(IsChanged);
		
		m_CboChList.SetCurSel(0);
		InitTreeList();	//ljb 20150810 add

		//재시작해야 반영됨
		AfxMessageBox("Initialize success. Please restart the program.");
		exit(0);
	}
}

