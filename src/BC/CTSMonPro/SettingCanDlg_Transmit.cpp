// SettingCanDlg_Transmit.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "SettingCanDlg_Transmit.h"
#include "RealEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSettingCanDlg_Transmit dialog
#define CAN_COL_NO				0
#define CAN_COL_ID				1
#define CAN_COL_TIME			2
#define CAN_COL_NAME			3
#define CAN_COL_STARTBIT		4
#define CAN_COL_BITCOUNT		5
#define CAN_COL_BYTE_ORDER		6
#define CAN_COL_DATATYPE		7
#define CAN_COL_FACTOR			8
#define CAN_COL_OFFSET			9
#define CAN_COL_DEFAULT			10
#define CAN_COL_DIVISION1		11
#define CAN_COL_DIVISION2		12
#define CAN_COL_DIVISION3		13


#define TAB_MASTER		0
#define TAB_SLAVE		1

#define ID_CAN_USER_RUN			301
#define ID_CAN_USER_STOP		302
#define ID_CAN_USER_PAUSE		303
#define ID_CAN_USER_CONTINUE	304
#define ID_CAN_USER_NEXT		305
#define ID_CAN_USER_GOTO		306
#define ID_CAN_USER_STATE		307

typedef struct CONFIGDATA
{
	char canID[8];
	char ExtID;
	char BaudRate;
	long CapVtg;
}ConfigFile;

CSettingCanDlg_Transmit::CSettingCanDlg_Transmit(CCTSMonProDoc * pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CSettingCanDlg_Transmit::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSettingCanDlg_Transmit::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSettingCanDlg_Transmit::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSettingCanDlg_Transmit::IDD3):
	(CSettingCanDlg_Transmit::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSettingCanDlg_Transmit)
	m_ckExtID = FALSE;
	m_strBaudRate = _T("");
	m_strSelectCh = _T("");
	m_strCanID = _T("");
	m_strCanIDSlave = _T("");
	m_ckExtIDSlave = FALSE;
	m_strBaudRateSlave = _T("");
	m_nMasterIDType = -1;
	m_strType = _T("");
	m_nProtocolVer = 0;
	m_strDescript = _T("");
	m_edit_chargeOnVolt = 0.0f;
	m_edit_OnDelayTime = 0;
	m_edit_KeyonVoltMax = 0.0f;
	m_edit_KeyonVoltMin = 0.0f;
	m_edit_BmsRestartTime = 0.0f;
	//}}AFX_DATA_INIT
		m_pDoc = pDoc;
		IsChange = FALSE;
}


void CSettingCanDlg_Transmit::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingCanDlg_Transmit)
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_SJW_SLAVE, m_ctrlBmsSJWSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_SJW, m_ctrlBmsSJW);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_SJW, m_strBmsSJW);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_SJW_SLAVE, m_strBmsSJWSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_TYPE_SLAVE, m_ctrlBmsTypeSlave);
	DDX_Control(pDX, IDC_COMBO_CAN_BMS_TYPE, m_ctrlBmsType);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_TYPE, m_strBmsType);
	DDX_CBString(pDX, IDC_COMBO_CAN_BMS_TYPE_SLAVE, m_strBmsTypeSlave);
	DDX_Control(pDX, IDC_PARAM_TAB, m_ctrlParamTab);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE_SLAVE, m_ctrlBaudRateSlave);
	DDX_Control(pDX, IDC_STATIC_CH_MSG, m_ctrlLabelChMsg);
	DDX_Control(pDX, IDC_STATIC_SEL_CH_CAN, m_ctrlLabelChNo);
	DDX_Control(pDX, IDC_LABEL_MSG, m_ctrlLabelMsg);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE, m_ctrlBaudRate);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE, m_ckExtID);
	DDX_CBString(pDX, IDC_COMBO_CAN_BAUDRATE, m_strBaudRate);
	DDX_Text(pDX, IDC_STATIC_SEL_CH_CAN, m_strSelectCh);
	DDX_Text(pDX, IDC_EDIT_CAN_ID, m_strCanID);
	DDX_Text(pDX, IDC_EDIT_SLAVE_CANID, m_strCanIDSlave);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE_SLAVE, m_ckExtIDSlave);
	DDX_CBString(pDX, IDC_COMBO_CAN_BAUDRATE_SLAVE, m_strBaudRateSlave);
	DDX_Radio(pDX, IDC_RADIO3, m_nMasterIDType);
	DDX_Text(pDX, IDC_EDIT_CHARGE_ON_VOLT, m_edit_chargeOnVolt);
	DDX_Text(pDX, IDC_EDIT_ON_DELAY_TIME, m_edit_OnDelayTime);
	DDX_Text(pDX, IDC_EDIT_KEYON_VOLT_MAX, m_edit_KeyonVoltMax);
	DDX_Text(pDX, IDC_EDIT_KEYON_VOLT_MIN, m_edit_KeyonVoltMin);
	DDX_Text(pDX, IDC_EDIT_BMS_RESTART_TIME, m_edit_BmsRestartTime);
	//}}AFX_DATA_MAP
}



BEGIN_MESSAGE_MAP(CSettingCanDlg_Transmit, CDialog)
	//{{AFX_MSG_MAP(CSettingCanDlg_Transmit)
	ON_BN_CLICKED(IDC_BTN_LOAD_CAN_SETUP, OnBtnLoadCanSetup)
	ON_BN_CLICKED(IDC_BTN_ADD_MASTER, OnBtnAddMaster)
	ON_BN_CLICKED(IDC_BTN_DEL_MASTER, OnBtnDelMaster)
	ON_BN_CLICKED(IDC_BTN_SAVE_CAN_SETUP, OnBtnSaveCanSetup)
	ON_BN_CLICKED(IDC_BTN_ADD_SLAVE, OnBtnAddSlave)
	ON_BN_CLICKED(IDC_BTN_DEL_SLAVE, OnBtnDelSlave)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	ON_NOTIFY(TCN_SELCHANGE, IDC_PARAM_TAB, OnSelchangeParamTab)
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnGridBeginEdit)
	ON_MESSAGE(WM_GRID_PASTE, OnGridPaste)
	ON_BN_CLICKED(IDC_BUT_NOT_USE, OnButNotUse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingCanDlg_Transmit message handlers

BOOL CSettingCanDlg_Transmit::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_nMasterIDType = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CanMaterTransIDType", 0);

	//ljb 2011221 이재복 //////////
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("OnInitDialog_msg1","IDD_CAN_SETTING_TRANSMIT"));
	//@ if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");
	m_pDoc->Fun_GetArryCanDivision(uiArryCanCode);		//ljb 2011222 이재복 //////////
	m_pDoc->Fun_GetArryCanStringName(strArryCanName);	//ljb 2011222 이재복 //////////

	InitParamTab();
	InitGridMaster();
	InitGridSlave();
	InitLabel();
	
	m_strCanID = "FFFFFFFF";
	m_strCanIDSlave = "FFFFFFFF";
//	m_fCapVtg = 0.000;
//	m_fCapVtgSlave = 0.000;
// 	m_fMcuTemp = 0.000;
// 	m_fMcuTempSlave = 0.000;
	m_strSelectCh.Format(Fun_FindMsg("OnInitDialog_msg2","IDD_CAN_SETTING_TRANSMIT"), nSelCh+1);
	//@ m_strSelectCh.Format("%d 번", nSelCh+1);
	UpdateData(FALSE);
	m_ctrlBaudRate.SetCurSel(3);
	m_ctrlBaudRateSlave.SetCurSel(3);
	
	m_ctrlBmsType.SetCurSel(0);
	m_ctrlBmsTypeSlave.SetCurSel(0);

	m_ctrlBmsSJW.SetCurSel(2);
	m_ctrlBmsSJWSlave.SetCurSel(2);

	this->SetWindowText(m_strTitle);
	UpdateCommonData();
	UpdateGridData();
	
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if(pMD == NULL)	return FALSE;
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return FALSE;
	
	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
		//if(pMD->GetState() == PS_STATE_LINE_OFF)
	{
		m_ctrlLabelMsg.SetText(Fun_FindMsg("OnInitDialog_msg4","IDD_CAN_SETTING_TRANSMIT"));
		//@ m_ctrlLabelMsg.SetText("동작중인 채널은 수정할 수 없습니다.");
		EnableAllControl(FALSE);
		//return;
	}	

	//ksj 20210712 : 무슨 기능인지 이력도 없고, 현재 사용에 방해가 되므로 일단 제거함
	//향후 필요한 경우 정확이 어떻게 필요한지 다시 파악하여 재구현 하더라도 일단 제거
	/*if (gst_CanRxSave.bEnable == TRUE)
	{
		//if (gst_CanRxSave.CanType == 1)
		if (gst_CanRxSave.CanType == nLINtoCANSelect)  //ksj 20210712 : 기존 코드 의도를 잘 모르는데 CAN RX 설정 창쪽 코드랑 동일하게 일단 고쳐봄.
		{
			m_ctrlBaudRate.SetCurSel(gst_CanRxSave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->SetCheck(gst_CanRxSave.CanRxExtendedIDMode);
			m_ctrlBmsSJW.SetCurSel(gst_CanRxSave.SJW);
			m_ctrlBmsType.SetCurSel(gst_CanRxSave.BMSType);	
		}
		memcpy(&gst_CanTxSave, &gst_CanRxSave, sizeof(CAN_RX_SAVE));
	}

	if (gst_CanRxSaveSlave.bEnable == TRUE)
	{
		//if (gst_CanRxSaveSlave.CanType == 1)
		if (gst_CanRxSaveSlave.CanType == nLINtoCANSelect)  //ksj 20210712 : 기존 코드 의도를 잘 모르는데 CAN RX 설정 창쪽 코드랑 동일하게 일단 고쳐봄.
		{
			m_ctrlBaudRateSlave.SetCurSel(gst_CanRxSaveSlave.CanRxBaudrate);			
			((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->SetCheck(gst_CanRxSaveSlave.CanRxExtendedIDMode);
			m_ctrlBmsSJWSlave.SetCurSel(gst_CanRxSaveSlave.SJW);
			m_ctrlBmsTypeSlave.SetCurSel(gst_CanRxSaveSlave.BMSType);
		}
		memcpy(&gst_CanTxSaveSlave, &gst_CanRxSaveSlave, sizeof(CAN_RX_SAVE));
	}*/


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
void CSettingCanDlg_Transmit::EnableAllControl(BOOL IsEnable)
{
	//m_wndCanGrid.EnableWindow(IsEnable);
	//m_wndCanGridSlave.EnableWindow(IsEnable);
	m_wndCanGrid.EnableEdit(IsEnable);
		
	GetDlgItem(IDC_EDIT_CAN_ID)->EnableWindow(IsEnable);
	GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_CHECK_EXT_MODE)->EnableWindow(IsEnable);
	GetDlgItem(IDC_EDIT_CAPVTG)->EnableWindow(IsEnable);
	
	GetDlgItem(IDC_BTN_ADD_MASTER)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_DEL_MASTER)->EnableWindow(IsEnable);
	GetDlgItem(IDC_BTN_LOAD_CAN_SETUP)->EnableWindow(IsEnable);
}
void CSettingCanDlg_Transmit::InitLabel()
{
	m_ctrlLabelMsg.SetTextColor(RGB(255, 0, 0))
		.FlashText(TRUE)
		.SetFontAlign(0)
		.SetFontName(Fun_FindMsg("InitLabel_msg1","IDD_CAN_SETTING_TRANSMIT"));
	//@ .SetFontName("HY헤드라인M");
	
	m_ctrlLabelChNo.SetTextColor(RGB(0, 0, 255))
		.SetFontName(Fun_FindMsg("InitLabel_msg2","IDD_CAN_SETTING_TRANSMIT"));
	//@ .SetFontName("HY헤드라인M");
	m_ctrlLabelChMsg.SetTextColor(RGB(0, 0, 0))
		.SetFontName(Fun_FindMsg("InitLabel_msg3","IDD_CAN_SETTING_TRANSMIT"));
	//@ .SetFontName("HY헤드라인M");
	
				
}
void CSettingCanDlg_Transmit::IsChangeFlag(BOOL IsFlag)
{
	IsChange = IsFlag;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);	
/*#ifdef NDEBUG  //ksj 20210115 : 주석처리함
	if(pMD == NULL || pMD->GetState() == PS_STATE_LINE_OFF)	
	{
		return;
	}
	else
#endif NDEBUG
	{
		GetDlgItem(IDOK)->EnableWindow(IsChange);
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}*/

	//ksj 20210115 : SAVE 버튼만 활성화
	if(pMD == NULL || pMD->GetState() == PS_STATE_LINE_OFF)	
	{
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}
	else
	{
		GetDlgItem(IDOK)->EnableWindow(IsChange);
		GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(IsChange);
	}
}
LRESULT CSettingCanDlg_Transmit::OnGridPaste(WPARAM wParam, LPARAM lParam)
{
	IsChangeFlag(TRUE);

	return 1;
}

LRESULT CSettingCanDlg_Transmit::OnGridBeginEdit(WPARAM wParam, LPARAM lParam)
{
	
	
	IsChangeFlag(TRUE);
	return 1;
}
void CSettingCanDlg_Transmit::InitGridMaster()			
{	
	//ljb	2008-09-04 CAN_COL_McuTempFlag 추가

	m_wndCanGrid.SubclassDlgItem(IDC_CAN_GRID, this);
	m_wndCanGrid.Initialize();

	CRect rect;
	m_wndCanGrid.GetParam()->EnableUndo(FALSE);
	
	//m_wndCanGrid.SetRowCount(1);
	m_wndCanGrid.SetRowCount(0); //ksj 20210305
	m_wndCanGrid.SetRowHeight(0,0,50); //lyj 20190704
	m_wndCanGrid.SetColCount(CAN_COL_DIVISION3);		//ljb
	m_wndCanGrid.GetClientRect(&rect);
	m_wndCanGrid.SetColWidth(CAN_COL_NO			, CAN_COL_NO			, 30);
	m_wndCanGrid.SetColWidth(CAN_COL_ID			, CAN_COL_ID			, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_TIME		, CAN_COL_TIME			, 80);
	//m_wndCanGrid.SetColWidth(CAN_COL_NAME		, CAN_COL_NAME			, rect.Width() - 690);
	m_wndCanGrid.SetColWidth(CAN_COL_NAME		, CAN_COL_NAME			, 120);
	m_wndCanGrid.SetColWidth(CAN_COL_STARTBIT	, CAN_COL_STARTBIT		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_BITCOUNT	, CAN_COL_BITCOUNT		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_BYTE_ORDER	, CAN_COL_BYTE_ORDER	, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_DATATYPE	, CAN_COL_DATATYPE		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_FACTOR		, CAN_COL_FACTOR		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_OFFSET		, CAN_COL_OFFSET		, 60);
	m_wndCanGrid.SetColWidth(CAN_COL_DEFAULT	, CAN_COL_DEFAULT		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION1	, CAN_COL_DIVISION1		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION2	, CAN_COL_DIVISION2		, 100);
	m_wndCanGrid.SetColWidth(CAN_COL_DIVISION3	, CAN_COL_DIVISION3		, 100);
	

	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_NO			),	CGXStyle().SetValue("No"));
	if(m_nMasterIDType == 1)
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	else
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));

	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue(Fun_FindMsg("InitGridMaster_msg1","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue("전송속도(ms)"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_NAME			),	CGXStyle().SetValue("Name"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT		),	CGXStyle().SetValue("StartBit"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT		),	CGXStyle().SetValue("BitCount"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER	),	CGXStyle().SetValue("ByteOrder"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE		),	CGXStyle().SetValue("DataType"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_FACTOR		),	CGXStyle().SetValue("Factor"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_OFFSET		),	CGXStyle().SetValue("Offset"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue(Fun_FindMsg("InitGridMaster_msg2","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue("설정값"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue(Fun_FindMsg("InitGridMaster_msg3","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue("분류 1"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue(Fun_FindMsg("InitGridMaster_msg4","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue("분류 2"));
	m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue(Fun_FindMsg("InitGridMaster_msg5","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue("분류 3"));
	
	m_wndCanGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndCanGrid.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(0),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_ID, CAN_COL_TIME),
		CGXStyle()
  			.SetControl(GX_IDS_CTRL_EDIT)
	);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// 		.SetChoiceList(_T("USER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\n"))
// 		.SetValue(_T(""))
// 	);
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_EDIT)
// 		);

	m_wndCanGrid.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndCanGrid, IDS_CTRL_REALEDIT));
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndCanGrid.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	
	char str[12], str2[7], str3[5];
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T(Fun_FindMsg("InitGridMaster_msg6","IDD_CAN_SETTING_TRANSMIT")))
			//@ .SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
			.SetValue(0L)
			
	);

	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(1L)
	);
	//전송 속도 기본값자 자리수 셋팅
	sprintf(str, "%%d", 1);		//정수부
	sprintf(str2, "%d", 5);		
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_TIME),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(10L)
	);

	sprintf(str, "%%.%dlf", 10);	//실수 포멧			ljb 20160825 박진호 요청 6->10
	sprintf(str2, "%d", 6);		    //정수부 크기		ljb 20160825 박진호 요청 6->7
	sprintf(str3, "%d", 10);		//소수부 크기		ljb 20160825 박진호 요청 6->10

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_FACTOR),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1L)
	);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);
	//기본 전송 값, 기본값자 자리수 셋팅
	sprintf(str3, "%d", 6);		//소숫점 이하		// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str, "%%.%dlf", 6);						// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str2, "%d", 8);		//정수부			//ljb 12->8  [8/29/2011 XNOTE]
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);

	//division 전송 값, 기본값자 자리수 셋팅
	sprintf(str, "%%d", 2);		
	sprintf(str2, "%d", 4);		//정수부
	
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("1000"))
		// 		.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
		.SetValue(0L)
		);


// 	sprintf(str3, "%d", 6);		//소숫점 이하
// 	sprintf(str, "%%.%dlf", 5);		
// 	sprintf(str2, "%d", 8);		//정수부
// 
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_FAULT_UPPER, CAN_COL_END_LOWER),
// 		CGXStyle()
// 			.SetControl(IDS_CTRL_REALEDIT)
// 			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
// 			.SetValue(0L)
// 	);

// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_CVFLAG),
// 		CGXStyle()
// 			.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 			.SetValue(0L)
// 	);
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_CapVtgFlag),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 		.SetValue(0L)
// 	);
// 	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_McuTempFlag),			
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_CHECKBOX3D)
// 		.SetValue(0L)
// 		);
	///////////////////////////////////////////////////

	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
				.SetValue(_T("Intel"))
		);
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
				//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\n"))
				.SetValue(_T("Unsigned"))
		);

	//ljb 2011222 이재복 S //////////
// 	CString strComboItem,strTemp;
// 	strComboItem = "None[0]\r\n";
// 	for (int i=0; i < strArryCanName.GetSize() ; i++)
// 	{
// 		strTemp.Format("[%d]",uiArryCanCode.GetAt(i));
// 		strComboItem = strComboItem + strArryCanName.GetAt(i) + "\r\n";		
// 	}
	CString strComboItem,strTemp,strTemp2;
	strComboItem = "None [0]\r\n";
//	int nFind=0;
	for (int i=0; i < strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",strArryCanName.GetAt(i),uiArryCanCode.GetAt(i));
		strTemp2 = strTemp.Left(2);
		strTemp2.MakeUpper();

		if (strTemp2 == "TX") strComboItem = strComboItem + strTemp;
	}
	//////////////////////////////////////////////////////////////////////////
	
	m_wndCanGrid.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		//		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetControl(GX_IDS_CTRL_COMBOBOX)					//쓰기도 가능  //yulee 20190714 check
// 		.SetChoiceList(_T("NONE\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
//		.SetChoiceList(strComboItem)
//		.SetChoiceList(_T("NONE\r\n"))
		.SetChoiceList(strComboItem)
		.SetValue(_T("None [0]"))
		);

}

void CSettingCanDlg_Transmit::OnOK() 
{
	CString strTemp,strCode;
	int		nLength;

	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CanMaterTransIDType", m_nMasterIDType);

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return;
	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END )
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg7","IDD_CAN_SETTING_TRANSMIT"));
		//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		return;
	}
// 	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
// 	{
// 		AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
// 		return;
// 	}
	UpdateData();

	SFT_TRANS_CMD_CHANNEL_CAN_SET canTransSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canTransSetData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_TRANS_DATA commonTransData;
	ZeroMemory(&commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));
	commonTransData.can_baudrate = m_ctrlBaudRate.GetCurSel();
	commonTransData.extended_id	= m_ckExtID;
//	commonTransData.fCapVtg	= m_fCapVtg;
//	commonTransData.fMcuTemp	= m_fMcuTemp;
	commonTransData.controller_canID = strtoul(m_strCanID,(char **)NULL,  16);
	commonTransData.bms_type = m_ctrlBmsType.GetCurSel();
	commonTransData.bms_sjw = m_ctrlBmsSJW.GetCurSel();

	//ljb 20181115 S CAN,LIN select 
	commonTransData.can_lin_select	= nLINtoCANSelect;
	commonTransData.can_fd_flag = 0;  //lyj 20200722 fdflag default 0
	commonTransData.can_datarate = 0; //lyj 20200217 datarate default 0
	commonTransData.terminal_r	= 1;	//임시
	commonTransData.crc_type	= 1;	//임시
	//ljb 20181115 E CAN,LIN select 

	//ljb 201011
	commonTransData.can_function_division[0] = 1801;		//charge on volt
	commonTransData.can_function_division[1] = 1802;		//on delay time
	commonTransData.can_function_division[2] = 1803;		//key on volt min
	commonTransData.can_function_division[3] = 1804;		//key on volt max
	commonTransData.can_function_division[4] = 1805;		//BMS restart time
	commonTransData.can_compare_type[0] = 3;	// 3: ==
	commonTransData.can_compare_type[1] = 3;	
	commonTransData.can_compare_type[2] = 3;	
	commonTransData.can_compare_type[3] = 3;	
	commonTransData.can_compare_type[4] = 3;	

	commonTransData.can_data_type[0] = 2;	// 2:float
	commonTransData.can_data_type[1] = 2;	
	commonTransData.can_data_type[2] = 2;
	commonTransData.can_data_type[3] = 2;
	commonTransData.can_data_type[4] = 2;

	commonTransData.fcan_Value[0] = m_edit_chargeOnVolt;
	commonTransData.fcan_Value[1] = (float)m_edit_OnDelayTime;
	commonTransData.fcan_Value[2] = m_edit_KeyonVoltMin;
	commonTransData.fcan_Value[3] = m_edit_KeyonVoltMax;
	commonTransData.fcan_Value[4] = m_edit_BmsRestartTime;
	
	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_MASTER);
	memcpy(&canTransSetData.canCommonData, &commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));	//ljb 201008

	ZeroMemory(&commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));
	commonTransData.can_baudrate = m_ctrlBaudRateSlave.GetCurSel();
	commonTransData.extended_id	= m_ckExtIDSlave;
//	commonTransData.fCapVtg	= m_fCapVtgSlave;
//	commonTransData.fMcuTemp	= m_fMcuTempSlave;
	commonTransData.controller_canID = strtoul(m_strCanIDSlave,(char **)NULL,  16);
	commonTransData.bms_type = m_ctrlBmsTypeSlave.GetCurSel();
	commonTransData.bms_sjw = m_ctrlBmsSJWSlave.GetCurSel();

	//ljb 20181115 S CAN,LIN select 
	commonTransData.can_lin_select	= nLINtoCANSelect;
	commonTransData.can_fd_flag = 0 ;  //lyj 20200722 fdflag default 0
	commonTransData.can_datarate = 0 ; //lyj 20200217 datarate default 0
	commonTransData.terminal_r	= 1;	//임시
	commonTransData.crc_type	= 1;	//임시
	//ljb 20181115 E CAN,LIN select 

	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_SLAVE);
	memcpy(&canTransSetData.canCommonData[1], &commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));	//ljb 201008

	int nMasterCount = 0;
	SFT_CAN_SET_TRANS_DATA canTransData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canTransData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);

	for(int i=0; i<m_wndCanGrid.GetRowCount(); i++)
	{		
		CString strTemp;
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_ID);
		if(strTemp == "")
		{
			continue;
		}
		if(strTemp == "0")
		{
			continue;
		}
		//canData[i].canID = strtoul(strTemp,(char **)NULL,  16);
		if(m_nMasterIDType == 0)		//16진수로 되어있으면 10진수로 변환
		{
			CString str10;
			
			str10.Format("%d", strtoul(strTemp , NULL, 16));
			strTemp = str10;
		}
		canTransData[i].dlc = 8;	//ljb 20181119 add 임시
		//canTransData[i].canID = atol(strTemp);
		canTransData[i].canID = strtoul(strTemp,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.
		canTransData[i].canType = PS_CAN_TYPE_MASTER;
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_NAME);
		strTemp.TrimLeft();
		sprintf(canTransData[i].name, strTemp);		
		canTransData[i].startBit = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_STARTBIT));
		canTransData[i].bitCount = atoi(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BITCOUNT));
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER);
		if(strTemp == "Motorola")
			canTransData[i].byte_order = 1;
		else
			canTransData[i].byte_order = 0;		
		
		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DATATYPE);
		if(strTemp == "Unsigned")
			canTransData[i].data_type = 0;
		else if(strTemp == "Signed")
			canTransData[i].data_type = 1;
		else if(strTemp == "Float")
			canTransData[i].data_type = 2;
		else if(strTemp == "String")
			canTransData[i].data_type = 3;

		canTransData[i].factor_multiply = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_FACTOR));
		canTransData[i].factor_Offset = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_OFFSET));
		
		canTransData[i].default_fValue = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DEFAULT));


		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1)) //yulee 20181119
		{
			//strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION1);
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[i].function_division = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division = atoi(strTemp);
		}

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[i].function_division2 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division2 = atoi(strTemp);
		}

		strTemp = m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[i].function_division3 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division3 = atoi(strTemp);
		}

		canTransData[i].send_time = atof(m_wndCanGrid.GetValueRowCol(i+1, CAN_COL_TIME));
		
		nMasterCount++;
	}

	int nSlaveCount = 0;
	for(int i=0; i<m_wndCanGridSlave.GetRowCount(); i++)
	{		
		int nIndex = i + nMasterCount;
		CString strTemp;
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_ID);
		if(strTemp == "")
		{
			continue;
		}
		//canData[nIndex].canID = strtoul(strTemp,(char **)NULL,  16);
		if(m_nMasterIDType == 0)		//16진수로 되어있으면 10진수로 변환
		{
			CString str10;
			
			str10.Format("%d", strtoul(strTemp , NULL, 16));
			strTemp = str10;
		}
		canTransData[nIndex].dlc = 8;	//ljb 20181119 add 임시
		//canTransData[nIndex].canID = atol(strTemp);
		canTransData[nIndex].canID = strtoul(strTemp,NULL,10); //ksj 20200323 : unsigned 범위 초과 버그 수정.
		canTransData[nIndex].canType = PS_CAN_TYPE_SLAVE;
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_NAME);
		strTemp.TrimLeft();
		sprintf(canTransData[nIndex].name, strTemp);		
		canTransData[nIndex].startBit = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_STARTBIT));
		canTransData[nIndex].bitCount = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BITCOUNT));
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_BYTE_ORDER);
		if(strTemp == "Motorola")
			canTransData[nIndex].byte_order = 1;
		else
			canTransData[nIndex].byte_order = 0;		
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DATATYPE);
		if(strTemp == "Unsigned")
			canTransData[nIndex].data_type = 0;
		else if(strTemp == "Signed")
			canTransData[nIndex].data_type = 1;
		else if(strTemp == "Float")
			canTransData[nIndex].data_type = 2;		
		else if(strTemp == "String")
			canTransData[nIndex].data_type = 3;
		
		canTransData[nIndex].factor_multiply = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_FACTOR));
		canTransData[nIndex].factor_Offset = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_OFFSET));
		
		canTransData[nIndex].send_time = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_TIME));
		canTransData[nIndex].default_fValue = atof(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DEFAULT));
// 		canTransData[nIndex].function_division = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION1));
// 		canTransData[nIndex].function_division2 = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION2));
// 		canTransData[nIndex].function_division3 = atoi(m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION3));

		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1)) //yulee 20181119
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[nIndex].function_division = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division = atoi(strTemp);
		}
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[nIndex].function_division2 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division2 = atoi(strTemp);
		}
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		if((strTemp.Find('['))>-1 && (strTemp.Find(']')-1))
		{
			nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
			strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
			canTransData[nIndex].function_division3 = atoi(strCode);
		}
		else
		{
			int nStrLength, nCnt = 0;
			nStrLength = strTemp.GetLength();
			for(int j=0; j<nStrLength ; j++)
			{
				if(isdigit(strTemp.GetAt(j)))
					nCnt++;
			}
			if(nStrLength == nCnt) 
				canTransData[i].function_division3 = atoi(strTemp);
		}



/*

		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION1);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[nIndex].function_division = atoi(strCode);
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[nIndex].function_division2 = atoi(strCode);
		
		strTemp = m_wndCanGridSlave.GetValueRowCol(i+1, CAN_COL_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		canTransData[nIndex].function_division3 = atoi(strCode);
		*/
		nSlaveCount++;
		
	}
	
	pMD->SetCANSetTransData(nSelCh, canTransData, nSlaveCount+nMasterCount);
	memcpy(&canTransSetData.canSetData, &canTransData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);	//ljb 201008

// 	SFT_TRANS_CMD_CAN_SET canTransSetData;
// 	ZeroMemory(&canTransSetData, sizeof(SFT_TRANS_CMD_CAN_SET));
	
// 	SFT_MD_CAN_TRANS_INFO_DATA * canInforData = pMD->GetCanTransAllData();
// 	memcpy(&canTransSetData.canCommonData, canInforData->canSetAllData.canCommonData, sizeof(canTransSetData.canCommonData));
// 	memcpy(&canTransSetData.canSetData, canInforData->canSetAllData.canSetData, sizeof(canTransSetData.canSetData));
	
	if(int nRth = m_pDoc->SetCANTransDataToModule(nSelModule, &canTransSetData, nSelCh) == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_CAN_SETTING_TRANSMIT"));
		//@ AfxMessageBox("CAN Trnas 정보 설정에 실패했습니다.");
		return;
	}

// 	SFT_LINCAN_SELECT LinCanSelectData;
// 	ZeroMemory(&LinCanSelectData, sizeof(SFT_LINCAN_SELECT));
// 	
// 	LinCanSelectData.SelectMode	= nLINtoCANSelect;
// 	
// 	BOOL bFlag = m_pDoc->SetLINCANSelectCommandDataToModule(nSelModule, nSelCh, &LinCanSelectData);
// 	if(bFlag == FALSE)
// 	{
// 		AfxMessageBox("LIN/CAN select 정보 설정에 실패했습니다.");
// 		return;
// 	}
	//yulee 20181114

	IsChange = FALSE;
	GetDlgItem(IDOK)->EnableWindow(IsChange);

	pMD->SaveCanTransConfig();	

	gst_CanTxSave.CanType = nLINtoCANSelect;
	gst_CanTxSave.CanRxBaudrate = m_ctrlBaudRate.GetCurSel();
	gst_CanTxSave.CanRxExtendedIDMode = ((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE))->GetCheck();
	gst_CanTxSave.SJW = m_ctrlBmsSJW.GetCurSel();
	gst_CanTxSave.BMSType = m_ctrlBmsType.GetCurSel();
	gst_CanTxSave.bEnable = TRUE;

	gst_CanTxSaveSlave.CanType = nLINtoCANSelect;
	gst_CanTxSaveSlave.CanRxBaudrate = m_ctrlBaudRateSlave.GetCurSel();
	gst_CanTxSaveSlave.CanRxExtendedIDMode = ((CButton*)GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE))->GetCheck();
	gst_CanTxSaveSlave.SJW = m_ctrlBmsSJWSlave.GetCurSel();
	gst_CanTxSaveSlave.BMSType = m_ctrlBmsTypeSlave.GetCurSel();
	gst_CanTxSaveSlave.bEnable = TRUE;
	
//	CDialog::OnOK();
}

//SBC 에서 Master CAN 설정 데이터 가져 오기
void CSettingCanDlg_Transmit::UpdateCommonData()
{
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if(pMD == NULL)	return;
	
// 	if(pMD->GetInstalledCanTransMaster(nSelCh) > 0 )
// 	{
		CString strData;
		SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nSelCh, PS_CAN_TYPE_MASTER);
		SFT_CAN_COMMON_DATA commonRecvData = pMD->GetCANCommonData(nSelCh, PS_CAN_TYPE_MASTER);
		if (commonRecvData.can_lin_select	!= nLINtoCANSelect)
		{
			if(commonRecvData.can_lin_select == 0)
			{
				AfxMessageBox(Fun_FindMsg("SettingCanDlg_DiffCan1","IDD_CAN_SETTING_DLG"));
				//CAN Rx 적용 후 CAN Tx를 다시 설정하여 주세요(현재 CAN Tx : LintoCAN)
			}
			else if (commonRecvData.can_lin_select == 1)
			{
				AfxMessageBox(Fun_FindMsg("SettingCanDlg_DiffCan2","IDD_CAN_SETTING_DLG"));
				//CAN Rx 적용 후 CAN Tx를 다시 설정하여 주세요(현재 CAN Tx : LintoCAN)
			}
			else
			{
				AfxMessageBox(Fun_FindMsg("SettingCanDlg_DiffCan3","IDD_CAN_SETTING_DLG"));
			}
		}
		m_strCanID.Format("%lX", commonData.controller_canID);		//khs 20081118
		m_ckExtID = commonData.extended_id;
//		EnableMaskFilter(0, PS_CAN_TYPE_MASTER);

		m_ctrlBaudRate.GetLBText(commonData.can_baudrate, m_strBaudRate);
		m_ctrlBmsType.GetLBText(commonData.bms_type, m_strBmsType);
		m_ctrlBmsSJW.GetLBText(commonData.bms_sjw, m_strBmsSJW);

		//ljb 201012
		m_edit_chargeOnVolt = commonData.fcan_Value[0];
		m_edit_OnDelayTime = commonData.fcan_Value[1];
		m_edit_KeyonVoltMin = commonData.fcan_Value[2];
		m_edit_KeyonVoltMax = commonData.fcan_Value[3];
		m_edit_BmsRestartTime = commonData.fcan_Value[4];

//		UpdateData(FALSE);
//	}
// 	if(pMD->GetInstalledCanTransSlave(nSelCh) > 0 )
// 	{
//		CString strData;
//		SFT_CAN_COMMON_TRANS_DATA commonData = pMD->GetCANCommonTransData(nSelCh, PS_CAN_TYPE_SLAVE);
		commonData = pMD->GetCANCommonTransData(nSelCh, PS_CAN_TYPE_SLAVE);
//		strData.Format("%lX", commonData.controller_canID);
		m_strCanIDSlave.Format("%lX", commonData.controller_canID);		//khs 20081118
		m_ckExtIDSlave = commonData.extended_id;
//		m_fCapVtgSlave = commonData.fCapVtg;
//		m_fMcuTempSlave = commonData.fMcuTemp;
		m_ctrlBaudRateSlave.GetLBText(commonData.can_baudrate, m_strBaudRateSlave);
		m_ctrlBmsTypeSlave.GetLBText(commonData.bms_type, m_strBmsTypeSlave);
		m_ctrlBmsSJWSlave.GetLBText(commonData.bms_sjw, m_strBmsSJWSlave);
// 	}
		UpdateData(FALSE);
}

//SBC 에서 CAN 데이터 가져 오기
void CSettingCanDlg_Transmit::UpdateGridData()
{
	int nRow= 0;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if(pMD == NULL)	return;
	
	for(int nMaster = 0; nMaster < pMD->GetInstalledCanTransMaster(nSelCh); nMaster++)
	{
		
		CString strTemp;
		nRow = m_wndCanGrid.GetRowCount();
		SFT_CAN_SET_TRANS_DATA canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nMaster);
		//		strTemp.Format("%ld", canData.canID);

		CString strName, strID; //yulee 20190911
		strName.Format("%s", canData.name);
		if(strName.CompareNoCase(_T("$")) == 0)
		{
			break;
		}

		if(m_nMasterIDType == 0)	//Hex Type 이면
			strTemp.Format("%X", canData.canID);	
		else
			strTemp.Format("%ld", canData.canID);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_ID), strTemp);		
		strTemp.Format("%s", canData.name);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_NAME), strTemp);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_TIME),  canData.send_time);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT), canData.startBit);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT), canData.bitCount);
		
		if(canData.byte_order == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Intel");
		else
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Motorola");
		
		if(canData.data_type == 0)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Unsigned");
		else if(canData.data_type == 1)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Signed");
		else if(canData.data_type == 2)
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Float");
		else
			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "String");	
		
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_FACTOR), canData.factor_multiply);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_OFFSET), canData.factor_Offset);
		long lTemp = (long)canData.default_fValue;
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DEFAULT), lTemp);

		strTemp = Fun_FindCanName(canData.function_division); //yulee 20181220
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division2);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division3);
		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), strTemp);

//		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), canData.function_division);
//		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), canData.function_division2);
//		m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), canData.function_division3);
		
// 		lTemp = (long)canData.CapVtgFlag;
// 		if (lTemp == 1)
// 			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_CapVtgFlag), (long)1);
// 		if (lTemp == 2)
// 			m_wndCanGrid.SetValueRange(CGXRange(nRow, CAN_COL_McuTempFlag), (long)1);
		
		if(nMaster+1 < pMD->GetInstalledCanTransMaster(nSelCh))
			m_wndCanGrid.InsertRows(nRow+1, 1);
	}
	
	//2014.08.22 캔 슬레이브를 사용하지않을시 그리드 적용하지않음.
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);
	if(isSlave == FALSE){
		return;
	}

	for(int nSlave = 0; nSlave < pMD->GetInstalledCanTransSlave(nSelCh); nSlave++)
	{
		CString strTemp;
		nRow = m_wndCanGridSlave.GetRowCount();
		SFT_CAN_SET_TRANS_DATA canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSlave);
		//		strTemp.Format("%ld", canData.canID);

		CString strName, strID; //yulee 20190911
		strName.Format("%s", canData.name);
		if(strName.CompareNoCase(_T("$")) == 0)
		{
			break;
		}

		if(m_nMasterIDType == 0)	//Hex Type 이면
			strTemp.Format("%X", canData.canID);	
		else
			strTemp.Format("%ld", canData.canID);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_ID), strTemp);		
		strTemp.Format("%s", canData.name);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_NAME), strTemp);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_TIME),  canData.send_time);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_STARTBIT), canData.startBit);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BITCOUNT), canData.bitCount);
		
		if(canData.byte_order == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Intel");
		else
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_BYTE_ORDER), "Motorola");
		
		if(canData.data_type == 0)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Unsigned");
		else if(canData.data_type == 1)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Signed");
		else if(canData.data_type == 2)
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "Float");
		else
			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DATATYPE), "String");	
		
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_FACTOR), canData.factor_multiply);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_OFFSET), canData.factor_Offset);
		long lTemp = (long)canData.default_fValue;
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DEFAULT), lTemp);

		strTemp = Fun_FindCanName(canData.function_division); //yulee 20181220
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division2);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), strTemp);
		
		strTemp = Fun_FindCanName(canData.function_division3);
		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), strTemp);

//		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION1), canData.function_division);
//		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION2), canData.function_division2);
//		m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_DIVISION3), canData.function_division3);
// 		lTemp = (long)canData.CapVtgFlag;
// 		if (lTemp == 1)
// 			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_CapVtgFlag), (long)1);
// 		if (lTemp == 2)
// 			m_wndCanGridSlave.SetValueRange(CGXRange(nRow, CAN_COL_McuTempFlag), (long)1);
		
		if(nSlave+1 < pMD->GetInstalledCanTransSlave(nSelCh))
			m_wndCanGridSlave.InsertRows(nRow+1, 1);
	}
}

CString CSettingCanDlg_Transmit::Fun_FindCanName(int nDivision) //yulee 20181220
{
	CString strName;
	int nSelectPos, nFindPos;
	for (int i=0; i < uiArryCanCode.GetSize() ; i++)
	{
		if (nDivision == uiArryCanCode.GetAt(i))
		{
			strName.Format("%s [%d]", strArryCanName.GetAt(i), nDivision);
			return strName;
		}
	}
	strName.Format("None [%d]", nDivision);
	return strName;
}

void CSettingCanDlg_Transmit::OnBtnLoadCanSetup() 
{
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");
	
	CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	if(IDOK == pDlg.DoModal())
	{
		if(m_wndCanGrid.GetRowCount() > 0)
			m_wndCanGrid.RemoveRows(1, m_wndCanGrid.GetRowCount());
		if(m_wndCanGridSlave.GetRowCount() > 0)
			m_wndCanGridSlave.RemoveRows(1, m_wndCanGridSlave.GetRowCount());
		if(LoadCANConfig(pDlg.GetPathName()) == TRUE)
		{
			CString strName;
			strName.Format("CAN Transmit - %s",  pDlg.GetFileName());
			this->SetWindowText(strName);
			
			IsChangeFlag(TRUE);
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}	
}

void CSettingCanDlg_Transmit::OnBtnAddMaster() 
{
	CRowColArray ra;
	m_wndCanGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndCanGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, 1);
	
	
	IsChangeFlag(TRUE);
	
}

void CSettingCanDlg_Transmit::OnBtnDelMaster() 
{
	CRowColArray ra;
	m_wndCanGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndCanGrid.RemoveRows(ra[i], ra[i]);
	}
	
	IsChangeFlag(TRUE);	
}

void CSettingCanDlg_Transmit::OnBtnSaveCanSetup() 
{
	UpdateData();
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\DataBase\\Config", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");		//ljb 2009-09-04
	
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if(IDOK == pDlg.DoModal())
	{
		if(SaveCANConfig(pDlg.GetPathName()) == TRUE)
			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
	}	
}
BOOL CSettingCanDlg_Transmit::SaveCANConfig(CString strSavePath)
{
	CFile file;
	CString strData,strSaveData;
	CString strTitle, strTitle1;
	int i = 0;
	strSaveData.Empty();

	//Start Save Master/////////////////////////////////////////////////////////////////////////
	
	strTitle1 = "**Data Info**\r\nTYPE,VERSION,DESCRIPT\r\n";

	strTitle = "**Master Transmit Configuration**\r\nID(Hex),BaudRate,Ext ID,Bms Type\r\n";

	TRY
	{
		if(strSavePath.Find('.') < 0)
			strSavePath += ".csv";

		if(file.Open(strSavePath, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			AfxMessageBox(Fun_FindMsg("SaveCANConfig_msg1","IDD_CAN_SETTING_TRANSMIT"));
			//@ AfxMessageBox("파일을 생성할 수 없습니다.");
			return FALSE;
		}

		file.Write(strTitle1, strTitle1.GetLength());	//Title1
		
//		strData.Format("%s,", m_strType);
		strData.Format("TRANS,");
		file.Write(strData, strData.GetLength()); //Type
		
//		strData.Format("%d,", m_nProtocolVer);
		strData.Format("3,");		//ljb 201008 2->3 분류2,분류3 추가
		file.Write(strData, strData.GetLength()); //ProtocolVer
		m_strDescript = m_strTitle; //lyj 20200730 설정 저장 시 title 값으로 descript 저장

		strData.Format("%s,\r\n\r\n", m_strDescript);
//		strData.Format("PNE CAN CONFIG FILE,\r\n\r\n");
		file.Write(strData, strData.GetLength()); //Descript


		file.Write(strTitle, strTitle.GetLength());	//Title 

		strSaveData.Format("%s,", m_strCanID);
//		file.Write(strData, strData.GetLength()); //ID

		m_ctrlBaudRate.GetWindowText(strData);	//ljb 20180326
// 		if(m_strBaudRate == "125 kbps")
// 			strData.Format("%d,", 125);
// 		else if(m_strBaudRate == "250 kbps")
// 			strData.Format("%d,", 250);
// 		else if(m_strBaudRate == "500 kbps")
// 			strData.Format("%d,", 500);
// 		else if(m_strBaudRate == "1 Mbps")
// 			strData.Format("%d,", 1000);
		//strData.Format("%s,", m_strBaudRate);	

		strSaveData += strData + ",";

		strData.Format("%d,",m_ckExtID);	//Ext ID
		strSaveData += strData;

		strData.Format("%s,",m_strBmsType);	//BMS Type
		strSaveData += strData;

		strData.Format("%s,",m_strBmsSJW);	//BMS SJW
		strSaveData += strData;

		strData.Format("%.3f,",m_edit_chargeOnVolt);	//Charge On Volt
		strSaveData += strData;
		strData.Format("%d,",m_edit_OnDelayTime);	//On Delay Time
		strSaveData += strData;
		strData.Format("%.3f,",m_edit_KeyonVoltMin);	//Key On Volt Min
		strSaveData += strData;
		strData.Format("%.3f,",m_edit_KeyonVoltMax);	//Key On Volt Max
		strSaveData += strData;
		strData.Format("%.3f,\r\n\r\n",m_edit_BmsRestartTime);	//BMS restart time
		strSaveData += strData;

		file.Write(strSaveData, strSaveData.GetLength()); //Bms Type

//		strData.Format("%f,", m_fCapVtg );
//		file.Write(strData, strData.GetLength());	// CapVtg Value

// 		strData.Format("%f,\r\n\r\n", m_fMcuTemp );
// 		file.Write(strData, strData.GetLength());	// Motor Inverter Temperature
		

		if(m_nMasterIDType == 0)		//Hex
		{
			//strTitle = Fun_FindMsg("SaveCANConfig_msg2","IDD_CAN_SETTING_TRANSMIT");
			strTitle = "ID(Hex),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		}
		//@ strTitle = "ID(Hex),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		else							//Dec
		{
			//strTitle = Fun_FindMsg("SaveCANConfig_msg3","IDD_CAN_SETTING_TRANSMIT");
			strTitle = "ID(Dec),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		}
		//@ strTitle = "ID(Dec),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		file.Write(strTitle, strTitle.GetLength());	//Title 
		
		//for(int i = 0; i<=m_wndCanGrid.GetRowCount(); i++)
		for(i=1; i<=m_wndCanGrid.GetRowCount(); i++) //ksj 20170915 : V100F 수정사항 적용.
		{
			for(int j = 1; j <=m_wndCanGrid.GetColCount(); j++)
			{
				strData.Format("%s,",m_wndCanGrid.GetValueRowCol(i, j));
				file.Write(strData, strData.GetLength()); 
			}
			strData.Format("\r\n");
			file.Write(strData, strData.GetLength()); //줄바꿈
		}
		strData.Format("\r\n");
		file.Write(strData, strData.GetLength()); //줄바꿈
	}
	//End Save Master/////////////////////////////////////////////////////////////////////////

	//Start Save Slave/////////////////////////////////////////////////////////////////////////
	strTitle = "**Slave Transmit Configuration**\r\nID(Hex),BaudRate,Ext ID,Bms Type\r\n";
	
	
	file.Write(strTitle, strTitle.GetLength());	//Title 
	
	strSaveData.Format("%s,", m_strCanIDSlave);
	m_ctrlBaudRateSlave.GetWindowText(strData);	//ljb 20180326
// 	if(m_strBaudRateSlave == "125 kbps")
// 		strData.Format("%d,", 125);
// 	else if(m_strBaudRateSlave == "250 kbps")
// 		strData.Format("%d,", 250);
// 	else if(m_strBaudRateSlave == "500 kbps")
// 		strData.Format("%d,", 500);
// 	else if(m_strBaudRateSlave == "1 Mbps")
// 		strData.Format("%d,", 1000);
	//strData.Format("%s,", m_strBaudRate);	
	strSaveData += strData +",";
//	file.Write(strData, strData.GetLength()); //BaudRate
	
	strData.Format("%d,",m_ckExtIDSlave);	
	strSaveData += strData;
//	file.Write(strData, strData.GetLength()); //Ext ID

	strData.Format("%s,",m_strBmsTypeSlave);	//BMS Type
	strSaveData += strData;

	strData.Format("%s,\r\n\r\n",m_strBmsSJWSlave);	//BMS Type
	strSaveData += strData;
	
	file.Write(strSaveData, strSaveData.GetLength()); //Bms Type
	
// 	strData.Format("%f,", m_fCapVtgSlave );
// 	file.Write(strData, strData.GetLength());	// CapVtg Value
	
// 	strData.Format("%f,\r\n\r\n", m_fMcuTempSlave );
// 	file.Write(strData, strData	.GetLength());	// Motor Inverter Temperature 
	
	if(m_nMasterIDType == 0)		//Hex
	{
		strTitle = "ID(Hex),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		//strTitle = Fun_FindMsg("SaveCANConfig_msg2","IDD_CAN_SETTING_TRANSMIT");//&&
	}
	else
	{
		strTitle = "ID(Dec),전송속도(ms),Name,StartBit,BitCount,ByteOrder,DataType,Factor,Offset,설정값,분류 1,분류 2,분류 3\r\n";
		//strTitle = Fun_FindMsg("SaveCANConfig_msg3","IDD_CAN_SETTING_TRANSMIT");//&&
	}
	file.Write(strTitle, strTitle.GetLength());	//Title 
	
	for(i=1; i<=m_wndCanGridSlave.GetRowCount(); i++)
	{
		for(int j = 1; j <= m_wndCanGridSlave.GetColCount(); j++)
		{
			strData.Format("%s,",m_wndCanGridSlave.GetValueRowCol(i, j));
			file.Write(strData, strData.GetLength()); 
		}
		strData.Format("\r\n");
		file.Write(strData, strData.GetLength()); //줄바꿈
	}
	//End Save Slave/////////////////////////////////////////////////////////////////////////		
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
	
	file.Close();
	return TRUE;
}

BOOL CSettingCanDlg_Transmit::LoadCANConfig(CString strLoadPath)
{
	CString strData;
	CStdioFile aFile;
	CFileException e;
	int nIndex, nPos = 0, nTemp;
	CString strTitle = "ID(Hex),BaudRate,Ext ID,BMS Capacitor voltage,BMS Motor Inverter Temperature";
	CString strTitle2 = "ID(Hex),BaudRate,Ext ID";
	CString strTitle3 = "ID(Hex),BaudRate,Ext ID,Bms Type";

	if( !aFile.Open( strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
		#ifdef _DEBUG
		   afxDump << "File could not be opened " << e.m_cause << "\n";
		#endif
		   AfxMessageBox(Fun_FindMsg("LoadCANConfig_msg7","IDD_CAN_SETTING_TRANSMIT"));
		   //@ AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		   return FALSE;
	}

	CStringList		strDataList;
	CStringList		strItemList;
	aFile.ReadString(strData);	//Version Check
	
	if(strData.Find("Data") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data
		
		
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);		//TRANS인지 RECEIVE인지 Check
			m_strType = strTemp;
			
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);				//1: 1006 Ver 추후 버전 업그레이드에 따라 추가
			
			if (strTemp.IsEmpty()) m_nProtocolVer = 0;
			else
				m_nProtocolVer = atoi(strTemp);
// 			if(strTemp == "1" || strTemp == "2")
// 				m_nProtocolVer = atoi(strTemp);
// 			else 
// 				m_nProtocolVer = 0;
			
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strDescript = strTemp;
		}
		
		// Data Info에서 Type에 따라 Load할 Data를 결정	
		if(m_strType != "TRANS")
		{
			AfxMessageBox(Fun_FindMsg("LoadCANConfig_msg8","IDD_CAN_SETTING_TRANSMIT"));
			//@ AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			m_nProtocolVer = 0;
			return FALSE;
		}

		nPos = 0;
		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Master or Slave
	}	
	
	if(strData.Find("Master") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.

		//구버전을 읽어올 경우 TRANS인지 RECEIVE인지 Check
		if (m_nProtocolVer == 2) strTitle = strTitle2;
		if (m_nProtocolVer == 3) strTitle = strTitle3;
		if(strData != strTitle)
		{
			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("LoadCANConfig_msg8","IDD_CAN_SETTING_TRANSMIT"));//&&
			m_nProtocolVer = 0;
			return FALSE;
		}

		aFile.ReadString(strData);	//Data
		
		
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strCanID = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");
			strTemp.MakeUpper();
			if(strTemp == "125" || strTemp == "125K" || strTemp == "125KBPS"){
				m_ctrlBaudRate.SetCurSel(0);}
			else if(strTemp == "250" || strTemp == "250K" || strTemp == "250KBPS"){
				m_ctrlBaudRate.SetCurSel(1);}
			else if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
				m_ctrlBaudRate.SetCurSel(2);}
			else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
				m_ctrlBaudRate.SetCurSel(3);}
			else {
				m_ctrlBaudRate.SetCurSel(0);
			}			
			//m_strBaudRate = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckExtID = atoi(strTemp);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			if (m_nProtocolVer == 1)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
//				m_fCapVtg = atof(strTemp);
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);

				strTemp = strData.Mid(nPos, nIndex - nPos);
//				m_fMcuTemp = atof(strTemp);
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);
			}

			if (m_nProtocolVer == 3)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
				m_strBmsType = strTemp;
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);
				
				if (nIndex > 0)	//BMS SJW
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_strBmsSJW = strTemp;
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Charge On Volt
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_chargeOnVolt = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//On Delay Time
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_OnDelayTime = atoi(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Key On Volt Min
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_KeyonVoltMin = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Key On Volt Max
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_KeyonVoltMax = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
				if (nIndex > 0)	//Key On Volt Max
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_edit_BmsRestartTime = atof(strTemp);
					nPos = nIndex+1;
					nIndex = strData.Find(",", nPos);
				}
			}
		}
		if (m_nProtocolVer < 3) m_strBmsType = "0";

		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		
		nPos = strData.Find("Name", 0);
		if(nPos > 4)	//ID가 Hex또는 Decimal인지 구분.. 이 방법밖에 없나??
		{
			CString strType;
			nPos = strData.Find("(", 0);
			strType = strData.Mid(nPos+1, strData.Find(")", nPos+1)-(nPos+1));
			strType.MakeUpper();
			if(strType == "HEX")
				m_nMasterIDType = 0;
			else
				m_nMasterIDType = 1;
		}
		else		//(HEX), (DEC) 가 없으면 무조건 DEC
			m_nMasterIDType = 1;
	
		UpdateData(FALSE);
		
		nPos = 0;
		while(aFile.ReadString(strData))	//Data
		{
			if(strData == "" || strData.Left(10) == ",,,,,,,,,,")
				break;
			//CString strTemp = strData.Mid(nPos, nIndex - nPos);
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			
			int nCol = 1;

			m_wndCanGrid.InsertRows(m_wndCanGrid.GetRowCount()+1, nCol);
			int nRow = m_wndCanGrid.GetRowCount();
			while(p0 != -1)
			{			
				CString strCanData = strData.Mid(p1, p0 - p1);
				if (strCanData == "")
				{
					p0 = -1;
					break;
				}
				if(m_nProtocolVer == 0)
				{
					if(nCol != CAN_COL_OFFSET)
					{
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else if (m_nProtocolVer == 1)
				{
					if(nCol != CAN_COL_DIVISION1)
					{
//						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
						nTemp = atoi(strCanData);
						if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
						else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
						else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
						else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
						else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
						else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
						else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
						TRACE("%s \n",strCanData);
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
						break;
					}
				}
				else if (m_nProtocolVer == 2)
				{
//					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					nTemp = atoi(strCanData);
					if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
					else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
					else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
					else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
					else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
					else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
					else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
					TRACE("%s \n",strCanData);
					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);

				}
				else if (m_nProtocolVer == 3)
				{
//					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					nTemp = atoi(strCanData);
					if (nTemp  == ID_CAN_USER_RUN) strCanData = "USER RUN";
					else if (nTemp  == ID_CAN_USER_STOP) strCanData = "USER STOP";
					else if (nTemp  == ID_CAN_USER_PAUSE) strCanData = "USER PAUSE";
					else if (nTemp  == ID_CAN_USER_CONTINUE) strCanData = "USER CONTINUE";
					else if (nTemp  == ID_CAN_USER_NEXT) strCanData = "USER NEXT";
					else if (nTemp  == ID_CAN_USER_GOTO) strCanData = "USER GOTO";
					else if (nTemp  == ID_CAN_USER_STATE) strCanData = "USER STATE";
					TRACE("%s \n",strCanData);
					m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);

				}
	
				p1 = p0 +1;
				p0 = strData.Find(",", p1);
			}
		}	

	}

	aFile.ReadString(strData);	//Master or Slave
	
	if(strData.Find("Slave") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data
		
		
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			m_strCanIDSlave = strTemp;
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);
			strTemp.Replace(" ", "");		//CAN Baud
			strTemp.MakeUpper();
			if(strTemp == "125" || strTemp == "125K" || strTemp == "125KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(0);}
			else if(strTemp == "250" || strTemp == "250K" || strTemp == "250KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(1);}
			else if(strTemp == "500" || strTemp == "500K" || strTemp == "500KBPS"){
				m_ctrlBaudRateSlave.SetCurSel(2);}
			else if(strTemp == "1" || strTemp == "1M" || strTemp.CompareNoCase("1MBPS") == 0 || strTemp.CompareNoCase("1 MBPS") == 0 || strTemp == "1000"){
				m_ctrlBaudRateSlave.SetCurSel(3);}
			else {
				m_ctrlBaudRateSlave.SetCurSel(0);
			}						
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_ckExtIDSlave = atoi(strTemp);
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			
			if (m_nProtocolVer == 3)
			{
				strTemp = strData.Mid(nPos, nIndex - nPos);
				m_strBmsTypeSlave = strTemp;
				nPos = nIndex+1;
				nIndex = strData.Find(",", nPos);

				if (nIndex > 0)
				{
					strTemp = strData.Mid(nPos, nIndex - nPos);
					m_strBmsSJWSlave = strTemp;
					//nPos = nIndex+1;
					//nIndex = strData.Find(",", nPos);
				}
			}
		}
		if (m_nProtocolVer < 3) m_strBmsTypeSlave = "0";
		//EnableMaskFilter(m_ckUserFilterSlave, PS_CAN_TYPE_SLAVE);
		UpdateData(FALSE);

		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		
		nPos = 0;

		while(aFile.ReadString(strData))	//Data
		{

			//2014.08.22 캔 전송 슬레이브를 사용하지않을시 그리드 적용하지않음.
			BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);
			if(isSlave == FALSE){
				break;
			}

			if(strData == "" || strData.Left(10) == ",,,,,,,,,,")
				break;
			//CString strTemp = strData.Mid(nPos, nIndex - nPos);
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			
			int nCol = 1;

			m_wndCanGridSlave.InsertRows(m_wndCanGridSlave.GetRowCount()+1, nCol);
			int nRow = m_wndCanGridSlave.GetRowCount();
			while(p0 != -1)
			{			
				CString strCanData = strData.Mid(p1, p0 - p1);
				if (strCanData == "")
				{
					p0 = -1;
					break;
				}
				if(m_nProtocolVer == 0)
				{
					if(nCol != CAN_COL_OFFSET)
					{
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
					}
				}
				else if (m_nProtocolVer == 1)
				{
					if(nCol != CAN_COL_DIVISION1)
					{
						m_wndCanGrid.SetValueRange(CGXRange(nRow, nCol++), strCanData);
					}
					else
					{
						p0 = p1 - 1;
						nCol++;
						break;
					}
				}
				else if (m_nProtocolVer == 2)
				{
					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
				}
				else if (m_nProtocolVer == 3)
				{
					m_wndCanGridSlave.SetValueRange(CGXRange(nRow, nCol++), strCanData);
				}

				p1 = p0 +1;
				p0 = strData.Find(",", p1);
			}				

		}	
	}//END if(strData.Find("Slave") > 0) 

	aFile.Close();
	return TRUE;
	
}

void CSettingCanDlg_Transmit::InitGridSlave()
{
	//ljb	2008-09-04 CAN_COL_McuTempFlag 추가

	m_wndCanGridSlave.SubclassDlgItem(IDC_CAN_GRID_SLAVE, this);
	m_wndCanGridSlave.Initialize();

	CRect rect;
	m_wndCanGridSlave.GetParam()->EnableUndo(FALSE);
	
	//2014.08.21 캔 슬레이브 사용하지않을시 기본1행 삽입 하지않는다.
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);	
	if(isSlave == TRUE){
		//m_wndCanGridSlave.SetRowCount(1);
		m_wndCanGridSlave.SetRowCount(0); //ksj 20210305
	}
	//m_wndCanGridSlave.SetRowCount(1);
	m_wndCanGridSlave.SetRowHeight(0,0,50); //lyj 20190704

	m_wndCanGridSlave.SetColCount(CAN_COL_DIVISION3);		//ljb
	m_wndCanGridSlave.GetClientRect(&rect);
	m_wndCanGridSlave.SetColWidth(CAN_COL_NO			, CAN_COL_NO			, 30);
	m_wndCanGridSlave.SetColWidth(CAN_COL_ID			, CAN_COL_ID			, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_TIME			, CAN_COL_TIME			, 80);
	//m_wndCanGridSlave.SetColWidth(CAN_COL_NAME		, CAN_COL_NAME			, rect.Width() - 690);
	m_wndCanGridSlave.SetColWidth(CAN_COL_NAME			, CAN_COL_NAME			, 120);
	m_wndCanGridSlave.SetColWidth(CAN_COL_STARTBIT		, CAN_COL_STARTBIT		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BITCOUNT		, CAN_COL_BITCOUNT		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_BYTE_ORDER	, CAN_COL_BYTE_ORDER	, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DATATYPE		, CAN_COL_DATATYPE		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_FACTOR		, CAN_COL_FACTOR		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_OFFSET		, CAN_COL_OFFSET		, 60);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DEFAULT		, CAN_COL_DEFAULT		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION1	,	CAN_COL_DIVISION1		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION2	,	CAN_COL_DIVISION2		, 100);
	m_wndCanGridSlave.SetColWidth(CAN_COL_DIVISION3	,	CAN_COL_DIVISION3		, 100);
	

	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_NO			),	CGXStyle().SetValue("No"));
//	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID"));
	if(m_nMasterIDType == 1)
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	else
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue(Fun_FindMsg("InitGridSlave_msg1","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_TIME			),	CGXStyle().SetValue("전송속도(ms)"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_NAME			),	CGXStyle().SetValue("Name"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_STARTBIT		),	CGXStyle().SetValue("StartBit"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BITCOUNT		),	CGXStyle().SetValue("BitCount"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_BYTE_ORDER	),	CGXStyle().SetValue("ByteOrder"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DATATYPE		),	CGXStyle().SetValue("DataType"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_FACTOR		),	CGXStyle().SetValue("Factor"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_OFFSET		),	CGXStyle().SetValue("Offset"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue(Fun_FindMsg("InitGridMaster_msg2","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DEFAULT		),	CGXStyle().SetValue("설정값"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue(Fun_FindMsg("InitGridSlave_msg3","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION1		),	CGXStyle().SetValue("분류 1"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue(Fun_FindMsg("InitGridSlave_msg4","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION2		),	CGXStyle().SetValue("분류 2"));
	m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue(Fun_FindMsg("InitGridSlave_msg5","IDD_CAN_SETTING_TRANSMIT")));
	//@ m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_DIVISION3		),	CGXStyle().SetValue("분류 3"));
	
	m_wndCanGridSlave.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);	//Resize Col Width
	m_wndCanGridSlave.GetParam()->EnableUndo(TRUE);

	//Static Ctrl/////////////////////////////////////
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(0),
		CGXStyle()
  			.SetHorizontalAlignment(DT_CENTER)
	);
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_ID, CAN_COL_TIME),
		CGXStyle()
  			.SetControl(GX_IDS_CTRL_EDIT)
	);

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
// 	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_NAME),
// 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_COMBOBOX)
// 		.SetChoiceList(_T("USER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\n"))
// 		.SetValue(_T(""))
// 	);

	m_wndCanGridSlave.RegisterControl(IDS_CTRL_REALEDIT, new CRealEditControl(&m_wndCanGridSlave, IDS_CTRL_REALEDIT));
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITFORMAT);
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE);
	m_wndCanGridSlave.GetParam()->GetStylesMap()->AddUserAttribute(IDS_UA_REALEDITSIZE2);
	
	char str[12], str2[7], str3[5];
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T(Fun_FindMsg("InitGridSlave_msg6","IDD_CAN_SETTING_TRANSMIT")))
			//@ .SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
			.SetValue(0L)
			
	);

	sprintf(str2, "%d", 8);		//정수부
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BITCOUNT),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(1L)
	);
	//전송 속도 기본값자 자리수 셋팅
	sprintf(str, "%%d", 1);		//정수부
	sprintf(str2, "%d", 5);		
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_TIME),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(10L)
	);

	sprintf(str, "%%.%dlf", 10);	//실수 포멧			ljb 20160825 박진호 요청 6->10
	sprintf(str2, "%d", 6);		    //정수부 크기		
	sprintf(str3, "%d", 10);		//소수부 크기		ljb 20160825 박진호 요청 6->10

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_FACTOR),
		CGXStyle()
  			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1L)
	);
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
	);
	//기본 전송 값, 기본값자 자리수 셋팅
	sprintf(str3, "%d", 6);		//소숫점 이하		// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str, "%%.%dlf", 6);						// ljb 2->6 [8/29/2011 XNOTE]
	sprintf(str2, "%d", 8);		//정수부			//ljb 12->8  [8/29/2011 XNOTE]
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DEFAULT),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0L)
		);

	//division 전송 값, 기본값자 자리수 셋팅
	sprintf(str, "%%d", 2);		
	sprintf(str2, "%d", 4);		//정수부
	
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1, CAN_COL_DIVISION3),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(0L)
		);

	///////////////////////////////////////////////////

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_BYTE_ORDER),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Intel\r\nMotorola\r\n"))
				.SetValue(_T("Intel"))
		);
	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DATATYPE),
			CGXStyle()
				.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
				.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
				//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\n"))
				.SetValue(_T("Unsigned"))
		);

	//ljb 2011222 이재복 S //////////
	CString strComboItem,strTemp;
	strComboItem = "None[0]\r\n";
	for (int i=0; i < strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("[%d]",uiArryCanCode.GetAt(i));
		strComboItem = strComboItem + strArryCanName.GetAt(i) + "\r\n";		
	}
	//////////////////////////////////////////////////////////////////////////

	m_wndCanGridSlave.SetStyleRange(CGXRange().SetCols(CAN_COL_DIVISION1,CAN_COL_DIVISION3),
		CGXStyle()
		//	.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetControl(GX_IDS_CTRL_COMBOBOX)			//쓰기도 가능 //yulee 20190714 check
//		.SetChoiceList(_T("NONE\r\nUSER_RUN\r\nUSER_STOP\r\nUSER_PAUSE\r\nUSER_CONTINUE\r\nUSER_NEXT\r\nUSER_GOTO\r\nUSER_STATE\r\n"))
//		.SetChoiceList(strComboItem)
		.SetChoiceList(_T("NONE\r\n"))
		.SetValue(_T("NONE"))
		);

}

void CSettingCanDlg_Transmit::OnBtnAddSlave() 
{
	// TODO: Add your control notification handler code here
	CRowColArray ra;
	m_wndCanGridSlave.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndCanGridSlave.InsertRows(ra[0] + 1, 1);
	else
		m_wndCanGridSlave.InsertRows(m_wndCanGrid.GetRowCount()+1, 1);
	
	
	IsChangeFlag(TRUE);
	
}

void CSettingCanDlg_Transmit::OnBtnDelSlave() 
{
	CRowColArray ra;
	m_wndCanGridSlave.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndCanGridSlave.RemoveRows(ra[i], ra[i]);
	}
	
	IsChangeFlag(TRUE);		
}
void CSettingCanDlg_Transmit::ReDrawMasterGrid()
{
	UpdateData();
	if(m_nMasterIDType == 1)
	{
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(DEC)"));
	}
	else
	{
		m_wndCanGrid.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
		m_wndCanGridSlave.SetStyleRange(CGXRange(0,CAN_COL_ID			),	CGXStyle().SetValue("ID(HEX)"));
	}
	
	int i = 0;
	CString strID, strTemp;
	for(i = 1; i <= m_wndCanGrid.GetRowCount(); i++)
	{
		strTemp = m_wndCanGrid.GetValueRowCol(i, CAN_COL_ID);
		if(m_nMasterIDType == 0)
		{
			strID.Format("%X", strtoul(strTemp , NULL, 10));
		}
		else
		{
			//strID.Format("%d", strtoul(strTemp , NULL, 16));
			strID.Format("%u", strtoul(strTemp , NULL, 16)); //ksj 20200323
		}
		m_wndCanGrid.SetValueRange(CGXRange(i, CAN_COL_ID), strID);
	}
	
	for(i = 1; i <= m_wndCanGridSlave.GetRowCount(); i++)
	{
		strTemp = m_wndCanGridSlave.GetValueRowCol(i, CAN_COL_ID);
		if(m_nMasterIDType == 0)
		{
			strID.Format("%X", strtoul(strTemp , NULL, 10));
		}
		else
		{
			//strID.Format("%d", strtoul(strTemp , NULL, 16));
			strID.Format("%u", strtoul(strTemp , NULL, 16)); //ksj 20200323
		}
		m_wndCanGridSlave.SetValueRange(CGXRange(i, CAN_COL_ID), strID);
	}
	
}
void CSettingCanDlg_Transmit::OnRadio3() 
{
	// TODO: Add your control notification handler code here
	ReDrawMasterGrid();
	
}

void CSettingCanDlg_Transmit::OnRadio4() 
{
	// TODO: Add your control notification handler code here
	ReDrawMasterGrid();
	
}

void CSettingCanDlg_Transmit::OnCancel() 
{
	if(IsChange)
	{
		if(AfxMessageBox(Fun_FindMsg("OnCancel_msg","IDD_CAN_SETTING_TRANSMIT"), MB_YESNO) != IDYES)
			//@ if(AfxMessageBox("CAN 정보가 변경되었습니다.\r\n작업을 취소하시겠습니까?", MB_YESNO) != IDYES)
			return;
	}
	IsChange = FALSE;
	
	CDialog::OnCancel();
}

void CSettingCanDlg_Transmit::InitParamTab()
{
	m_ctrlParamTab.ModifyStyle(TCS_BOTTOM|TCS_MULTILINE|TCS_VERTICAL|TCS_BUTTONS, TCS_OWNERDRAWFIXED|TCS_FIXEDWIDTH);

	TC_ITEM item;
	item.mask = TCIF_TEXT;
	item.pszText = _T("Master");
	m_ctrlParamTab.InsertItem(TAB_MASTER, &item);

	//2014.07.07 슬레이브 사용유무
	BOOL isSlave = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseSlave", TRUE);;	
	if(isSlave == TRUE){
		item.pszText = "Slave";	
		m_ctrlParamTab.InsertItem(TAB_SLAVE, &item);
	}
	m_ctrlParamTab.SetColor(RGB(0,0,0), RGB(255,255,255), RGB(240,240,240), RGB(240,240,240));	// 글자색, 바깥쪽 라인, 내부 색깔, 이미지쪽 라인
	
	//Master and Slave button size 같게 설정
	Fun_ChangeSizeControl(IDC_BTN_ADD_MASTER,IDC_BTN_ADD_SLAVE);
	Fun_ChangeSizeControl(IDC_BTN_DEL_MASTER,IDC_BTN_DEL_SLAVE);
	Fun_ChangeSizeControl(IDC_CAN_GRID,IDC_CAN_GRID_SLAVE);
	Fun_ChangeSizeControl(IDC_EDIT_CAN_ID,IDC_EDIT_SLAVE_CANID);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BAUDRATE,IDC_COMBO_CAN_BAUDRATE_SLAVE);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BMS_TYPE,IDC_COMBO_CAN_BMS_TYPE_SLAVE);
	Fun_ChangeSizeControl(IDC_COMBO_CAN_BMS_SJW,IDC_COMBO_CAN_BMS_SJW_SLAVE);
	Fun_ChangeSizeControl(IDC_CHECK_EXT_MODE,IDC_CHECK_EXT_MODE_SLAVE);
}
void CSettingCanDlg_Transmit::Fun_ChangeViewControl(BOOL bFlag)
{
	if(bFlag)
	{
		GetDlgItem(IDC_FRAME_SETTING)->SetWindowText(Fun_FindMsg("Fun_ChangeViewControl_msg1","IDD_CAN_SETTING_TRANSMIT"));
		//@ GetDlgItem(IDC_FRAME_SETTING)->SetWindowText("Transmit Master 설정");
		GetDlgItem(IDC_BTN_ADD_MASTER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DEL_MASTER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAN_GRID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_CAN_ID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_EXT_MODE)->ShowWindow(SW_SHOW);
		// 		m_wndGradeGrid.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_ADD_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_DEL_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAN_GRID_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_SLAVE_CANID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_CHARGE_ON_VOLT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_ON_DELAY_TIME)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_KEYON_VOLT_MIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_KEYON_VOLT_MAX)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_BMS_RESTART_TIME)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL10)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL5)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL6)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL7)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL8)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_LABEL9)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_FRAME_SETTING)->SetWindowText(Fun_FindMsg("Fun_ChangeViewControl_msg2","IDD_CAN_SETTING_TRANSMIT"));
		//@ GetDlgItem(IDC_FRAME_SETTING)->SetWindowText("Transmit Slave 설정");
		GetDlgItem(IDC_BTN_ADD_MASTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_DEL_MASTER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CAN_GRID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_CAN_ID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_EXT_MODE)->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_BTN_ADD_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_DEL_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CAN_GRID_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_SLAVE_CANID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BAUDRATE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_TYPE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_COMBO_CAN_BMS_SJW_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_EXT_MODE_SLAVE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_CHARGE_ON_VOLT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_ON_DELAY_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_KEYON_VOLT_MIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_KEYON_VOLT_MAX)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_BMS_RESTART_TIME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL10)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL6)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL7)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL8)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_LABEL9)->ShowWindow(SW_HIDE);
	}
}

void CSettingCanDlg_Transmit::Fun_ChangeSizeControl(UINT uiMaster, UINT uiSlave)
{
	CRect	rect;
	GetDlgItem(uiMaster)->GetWindowRect(rect);
	ScreenToClient(&rect);
	GetDlgItem(uiSlave)->SetWindowPos(NULL,rect.left,rect.top,rect.Width(),rect.Height(),SWP_HIDEWINDOW);
	
}

void CSettingCanDlg_Transmit::OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nTabNum = m_ctrlParamTab.GetCurSel();
	TRACE("Tab %d Selected\n", nTabNum);
	
	switch(nTabNum)
	{
		
	case TAB_MASTER:		//안전 조건 
		Fun_ChangeViewControl(TRUE);
		break;
		
	case TAB_SLAVE:		//Grading조건
		Fun_ChangeViewControl(FALSE);
		break;
		
	default:	//TAB_RECORD_VAL	//Reporting 조건
		Fun_ChangeViewControl(TRUE);
	}	
	
	*pResult = 0;
}

void CSettingCanDlg_Transmit::OnButNotUse() 
{
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return;
	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END )
	{
		AfxMessageBox(Fun_FindMsg("OnButNotUse_msg1","IDD_CAN_SETTING_TRANSMIT"));
		//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		return;
	}

	SFT_TRANS_CMD_CHANNEL_CAN_SET canTransSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canTransSetData, sizeof(SFT_TRANS_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_TRANS_DATA commonTransData;
	ZeroMemory(&commonTransData, sizeof(SFT_CAN_COMMON_TRANS_DATA));
	
	for(int i=0;i<2;i++) //lyj 20200722 can clear 시 0이 아닌 default 값으로 내리기
	{
		canTransSetData.canCommonData[i].can_fd_flag = 0 ;
		canTransSetData.canCommonData[i].can_datarate = 0; 
		canTransSetData.canCommonData[i].terminal_r = 1; 
		canTransSetData.canCommonData[i].crc_type = 1;
	}

	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_MASTER);
	Sleep(200);
	pMD->SetCANCommonTransData(nSelCh, commonTransData, PS_CAN_TYPE_SLAVE);
	Sleep(200);

	SFT_CAN_SET_TRANS_DATA canTransData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canTransData, sizeof(SFT_CAN_SET_TRANS_DATA)*_SFT_MAX_MAPPING_CAN);
	pMD->SetCANSetTransData(nSelCh, canTransData, 0);

	//	AfxMessageBox("All setting is clear. When you open this window, every can data will be cleared");
	
	if(int nRth = m_pDoc->SetCANTransDataToModule(nSelModule, &canTransSetData, nSelCh) == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnButNotUse_msg2","IDD_CAN_SETTING_TRANSMIT"));
		//@ AfxMessageBox("CAN Trnas 정보 설정에 실패했습니다.");
		return;
	}

	pMD->SaveCanTransConfig();	
}