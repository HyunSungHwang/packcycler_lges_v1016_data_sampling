// ModbusCanConvBroker.h: interface for the CModbusCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODBUSCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_)
#define AFX_MODBUSCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include "CTSMonProDoc.h"
#include "CanConvBroker.h"
#include "xlCANFunctions.h"
#include "modbus.h"

typedef enum
{
	MCC_PLAY,
	MCC_PAUSE,
	MCC_STOP,
} MCC_MODE ;

typedef struct tagMCC_INFO
{
	// modbus addr, startbit, bitcounts, name, datatype, canid, startbit, bitcounts
	INT addr ;
	INT mb_startbit ;
	INT mb_bitcounts ;
	CHAR name[256] ;
	INT datatype ;	// Unsign:0, Sign:1,
	INT canid ;
	INT can_startbit ;
	INT can_bitcounts ;
	FLOAT factor ; //OFFSET, SAFEUPPERLIMIT, SAFELOWERLIMIT, TERMUPPERLIMIT, TERMLOWERLIMIT, CAT1, CAT2, CAT3
	FLOAT offset ;
	FLOAT fault_upper ;
	FLOAT fault_lower ;
	FLOAT end_upper ;
	FLOAT end_lower ;
	short int function_division ;
	short int function_division2 ;
	short int function_division3 ;
	CHAR desc[512] ;
} MCC_INFO, *PMCC_INFO ;

typedef struct tagMODBUS_INFO
{
	CHAR comm[16] ;		// 통신방법
	CHAR Parity ;
	CHAR Databit ;
	CHAR Stopbit ;
	INT Baudrate ;

	INT responsetimeout ;
	INT slaveid ;

	INT functioncd ;
	INT scanrate ;

	CHAR IPAddr[MAX_PATH] ;
	INT Port ;

	INT MBAddr ;
	INT MBLen ;


	INT can_baudrate ;
	INT extmode ;
} MODBUS_INFO, *PMODBUS_INFO ;

class CCANFunctions ;
class CCTSMonProDoc ;
class CModbusConvDlg ;
class CModbusCanConvBroker : public CCanConvBroker
{
public:
	CModbusCanConvBroker();
	virtual ~CModbusCanConvBroker();

	// modbus info
	UINT	m_nSlaveID;
	modbus_t* ctx;
	UINT m_wTimerRes;

	CModbusConvDlg* m_pModbusDlg ;
	CString m_strModbusSettingName ;

	INT m_nModbusAddr ;
	INT GetModbusAddr() { ASSERT(this) ; return m_nModbusAddr ; }
	
	INT m_nModbusLen ;
	INT GetModbusLen() { ASSERT(this) ; return m_nModbusLen ; }
	
	CString m_strModbusCommunication ;
	INT m_nModbusDatabit  ;
	INT m_nModbusStopbit  ;
	CHAR m_cModbusParity  ;
	LONG m_lModbusBaudrate ;
	CString m_strModbusIPAddr ;
	INT m_nModbusPort ;
	
	BOOL m_bSaveRxFlag ;
	void SetSaveRxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveRxFlag = b ; }
	
	BOOL m_bSaveTxFlag ;
	void SetSaveTxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveTxFlag = b ; }
	
//	INT m_nChannel ;
//	CString m_strModbusSettingName ;	
	MCC_MODE m_nModbusMode ;
	
	DWORD m_timerID ;


	// CAN info
	CCANFunctions* m_pCAN;
	BOOL m_bSendCANData ;
//	BOOL m_bCanRxThreadRun ;
	XLstatus SendCANData(XLevent& xlEvent);

	void DoNotShowMessage() ;
	void StopModbusCanConvThread();//yulee 20190523 _2010버전 업 중 에러 LNK2019
	void PauseModbusCanConvThread() { ASSERT(this) ; m_nModbusMode = MCC_PAUSE ; } ;
	BOOL BeginModbusCanConvThread(int nDevID, PMODBUS_INFO pDlg, CString strDBName, CArray<PMCC_INFO, PMCC_INFO>& mcc, int nCanChannel = 0, int nCanBaudrate = 0);
	//BOOL BeginModbusCanConvThread(int nDevID, CModbusConvDlg* pDlg, CString strDBName, CArray<PMCC_INFO, PMCC_INFO>& mcc, int nCanChannel = 0, int nCanBaudrate = 0);
    //BOOL BeginModbusCanConvThread(int nDevID, PMODBUS_INFO pMBInfo, CString strDBName, CArray<PMCC_INFO, PMCC_INFO>& mcc);	//yulee 20190523 _2010버전 업 중 에러 LNK2019


	HANDLE m_hModubsSchedulerTerminated ;
	HANDLE m_hModbusRxThreadTerminated ;
//	HANDLE m_hCanRxThreadTerminated ;
	
	// Dlg 의 리스트에 전시여부
//	BOOL m_bShowMessage ;

	CListBox* m_pRxList ;
	CListBox* m_pTxList ;

	CArray<PMCC_INFO, PMCC_INFO> m_mcc_array ;


	INT m_nDevID ;

protected:
	// Modbus-Can Conv Set ID	- PCI 슬롯번호(?) 최대 4개.
//	INT m_nIndex ;

};

DWORD WINAPI ModbusSchedulerThread( PVOID par );
void CALLBACK TimeProc(UINT _timerId, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2) ;

#endif // !defined(AFX_MODBUSCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_)
