/*----------------------------------------------------------------------------
| File        : xlCANFunctions.cpp
| Project     : Vector CAN Example 
|
| Description : Shows the basic CAN functionality for the XL Driver Library
|-----------------------------------------------------------------------------
| $Author: vismra $    $Locker: $   $Revision: 20478 $
| $Header: /VCANDRV/XLAPI/samples/xlCANcontrol/xlCANFunctions.cpp 11    14.11.05 10:59 J?g $
|-----------------------------------------------------------------------------
| Copyright (c) 2012 by Vector Informatik GmbH.  All rights reserved.
|---------------------------------------------------------------------------*/

#include "stdafx.h" 
//#include "xlCANcontrol.h"
#include "CanConvBroker.h"
#include "xlCANFunctions.h"
#include "debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// globals
//////////////////////////////////////////////////////////////////////

// TStruct g_th;
//BOOL    g_bCANThreadRun;
//extern BOOL g_bCanRxThreadRun[4] ;

#define RECEIVE_EVENT_SIZE 1                // DO NOT EDIT! Currently 1 is supported only
#define RX_QUEUE_SIZE      4096             // internal driver queue size in CAN events

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCANFunctions::CCANFunctions()
{
// 	m_xlChannelMask[CHAN01] = 0;
// 	m_xlChannelMask[CHAN02] = 0;

	for (int nIdx = 0; nIdx < MAXPORT; nIdx++)
	{
		//		m_xlChannelMask[MASTER] = m_xlChannelMask[SLAVE] = 0;
		m_xlChannelMask[nIdx] = 0 ;
	}
	
	strcpy(m_AppName, "CTSMonPro") ;            //!< Application name which is displayed in VHWconf
	m_BaudRate = 500000 ;						//!< Default baudrate
	m_bInitDone = FALSE;

	m_pOutput = NULL ;
	m_pHardware = NULL ;


	m_nChannel = -1 ;
//	m_bCanRxThreadRun = FALSE ;
//	m_bShowMessage = FALSE ;
//	ZeroMemory(&m_tstruct, sizeof(m_tstruct)) ;
}

CCANFunctions::~CCANFunctions()
{
	if (m_bInitDone)
	{
//		CloseHandle(m_hThread);
//		CloseHandle(m_hMsgEvent);
		m_bInitDone = FALSE;

		XLstatus xlStatus;	
		if (m_xlPortHandle != XL_INVALID_PORTHANDLE)
		{
			xlStatus = xlClosePort(m_xlPortHandle);
			TRACE("- ClosePort        : PH(0x%x), %s\n", m_xlPortHandle, xlGetErrorString(xlStatus));
		}
		
		m_xlPortHandle = XL_INVALID_PORTHANDLE;
/*
		xlCloseDriver() ;
*/
	}
}

////////////////////////////////////////////////////////////////////////////

//! CANInit

//! Open the driver, get the channelmasks and create the RX thread.
//! 
//!
//////////////////////////////////////////////////////////////////////////// 

/*
XLstatus CCANFunctions::CANInit()
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];
	m_bInitDone = FALSE;

/ *
	// ------------------------------------
	// open the driver
	// ------------------------------------
	xlStatus = xlOpenDriver();

	sprintf(tmp, "xlOpenDriver, stat: %d", xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	if (xlStatus != XL_SUCCESS)
	{
		AfxMessageBox("Error when opening driver!\nMaybe the DLL is too old.");
		return xlStatus;
	}

	// ---------------------------------------
	// Get/Set the application within VHWConf
	// ---------------------------------------
	xlStatus = canGetChannelMask();
	if ((xlStatus) || (m_xlChannelMask[CHAN01] == 0) || (m_xlChannelMask[CHAN02] == 0))
	{
		return XL_ERROR;
	}

	// ---------------------------------------
	// Open ONE port for both channels
	// ---------------------------------------
	xlStatus = canInit();
	if (xlStatus)
	{
		return xlStatus;
	}
* /

	// Modified by 224 (2014/04/07) : xlCanControl 에서는 두개의 채널만 사용
	m_xlChanMaskTx = 0;
	m_xlChanIndex  = 0;

	xlStatus = canInitDriver(&m_xlChanMaskTx, &m_xlChanIndex) ;


	// ---------------------------------------
	// Create ONE thread for both channels
	// ---------------------------------------
	xlStatus = canCreateRxThread();
	if (xlStatus)
	{
		return xlStatus;
	}

	m_bInitDone = TRUE;

	return xlStatus;
}
*/

// Added by 224 (2014/11/04) : CLinCanConvBroker, CModbusCanConvBroker 를 동시에 사용하기 위해 생성
// 기존 CLinCanConvBroker 는 그대로 사요하게 두고, 새로 만듬
// CLinCanConvBroker, CModbusCanConvBroker는 CCanConvBroker에서 상속받음.
XLstatus CCANFunctions::CANInit(CCanConvBroker* pBroker, int nChannel)
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];
	m_bInitDone = FALSE;
	
	
	// 0, 2, 4, 6 채널 중에 하나이다.
//	ASSERT(nChannel % 2 != 0) ;
	
	// ---------------------------------------
	// Open ONE port for both channels master+slave
	// ---------------------------------------
	
	// calculate the channelMask for both channel 
	//	xlChannelMask_both = m_xlChannelMask[MASTER] | m_xlChannelMask[SLAVE];
	m_nChannel = nChannel ;
	XLaccess xlChannelMask_both = m_xlChannelMask[m_nChannel] ;
	// 	XLaccess xlChannelMask_both = 0 ;
	// 	for (int nIdx = 0; nIdx < MAXPORT; nIdx++)
	// 	{
	// 		xlChannelMask_both |= m_xlChannelMask[nIdx] ;
	// 	}
	XLaccess xlPermissionMask = xlChannelMask_both;
	
	sprintf(tmp, "CTSMon CAN Master %d", m_nChannel) ;
	xlStatus = xlOpenPort(&m_xlPortHandle, tmp, xlChannelMask_both, &xlPermissionMask, 4096, XL_INTERFACE_VERSION, XL_BUS_TYPE_CAN); 
	
	sprintf(tmp, "xlOpenPort: PortHandle: %d; Permissionmask: 0x%I64x; Status: %d", m_xlPortHandle, xlPermissionMask, xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	
	ASSERT(xlChannelMask_both == xlPermissionMask) ;
	
	if (xlStatus != XL_SUCCESS)
	{
		if (m_pOutput)
		{
			m_pOutput->InsertString(-1, "xlOpenPort failed");
		}
		return xlStatus ;
	}
	
	if (m_pOutput)
	{
		CString str ;
		str.Format("Channel: %d xlOpenPort done", nChannel) ;
		m_pOutput->InsertString(-1, str);
	}
	
	if (m_xlPortHandle == XL_INVALID_PORTHANDLE)
		return XL_ERROR;
	
	if (xlStatus != XL_SUCCESS)
	{
		m_xlPortHandle = XL_INVALID_PORTHANDLE;
		return xlStatus;
	}
	
	// ---------------------------------------
	// Create ONE thread for both channels
	// ---------------------------------------
	
	//	m_pDoc = pDoc ;
	m_pBroker = pBroker ;
	
	// 	xlStatus = canInitDriver(&m_xlChanMaskTx, &m_xlChanIndex) ;
	
	
	// ---------------------------------------
	// Create ONE thread for both channels
	// ---------------------------------------
	xlStatus = canCreateRxThread();
	if (xlStatus)
	{
		return xlStatus;
	}
	
	m_bInitDone = TRUE;
	
	return xlStatus;
}

XLstatus CCANFunctions::CANInit(CLinCanConvBroker* pBroker, int nChannel)
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];
	m_bInitDone = FALSE;


	// 0, 2, 4, 6 채널 중에 하나이다.
	ASSERT(nChannel % 2 != 0) ;
	
	// ---------------------------------------
	// Open ONE port for both channels master+slave
	// ---------------------------------------
	
	// calculate the channelMask for both channel 
	//	xlChannelMask_both = m_xlChannelMask[MASTER] | m_xlChannelMask[SLAVE];
	m_nChannel = nChannel ;
	XLaccess xlChannelMask_both = m_xlChannelMask[m_nChannel] ;
// 	XLaccess xlChannelMask_both = 0 ;
// 	for (int nIdx = 0; nIdx < MAXPORT; nIdx++)
// 	{
// 		xlChannelMask_both |= m_xlChannelMask[nIdx] ;
// 	}
 	XLaccess xlPermissionMask = xlChannelMask_both;
	
	sprintf(tmp, "CTSMon CAN Master %d", m_nChannel) ;
	xlStatus = xlOpenPort(&m_xlPortHandle, tmp, xlChannelMask_both, &xlPermissionMask, 4096, XL_INTERFACE_VERSION, XL_BUS_TYPE_CAN); 
	
	sprintf(tmp, "xlOpenPort: PortHandle: %d; Permissionmask: 0x%I64x; Status: %d", m_xlPortHandle, xlPermissionMask, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	ASSERT(xlChannelMask_both == xlPermissionMask) ;
	
	if (xlStatus != XL_SUCCESS)
	{
		if (m_pOutput)
		{
			m_pOutput->InsertString(-1, "xlOpenPort failed");
		}
		return xlStatus ;
	}
	
	if (m_pOutput)
	{
		m_pOutput->InsertString(-1, "xlOpenPort done");
	}
	
	if (m_xlPortHandle == XL_INVALID_PORTHANDLE)
		return XL_ERROR;
	
	if (xlStatus != XL_SUCCESS)
	{
		m_xlPortHandle = XL_INVALID_PORTHANDLE;
		return xlStatus;
	}
	
	// ---------------------------------------
	// Create ONE thread for both channels
	// ---------------------------------------
	
	//	m_pDoc = pDoc ;
	m_pBroker = pBroker ;

// 	xlStatus = canInitDriver(&m_xlChanMaskTx, &m_xlChanIndex) ;


	// ---------------------------------------
	// Create ONE thread for both channels
	// ---------------------------------------
	xlStatus = canCreateRxThread();
	if (xlStatus)
	{
		return xlStatus;
	}

	m_bInitDone = TRUE;

	return xlStatus;
}


XLstatus CCANFunctions::CANClose()
{

	ASSERT(m_nChannel != -1) ;
	
	XLstatus        xlStatus = XL_SUCCESS;
//	XLaccess        xlChannelMask_both = m_xlChannelMask[MASTER] | m_xlChannelMask[SLAVE];
//	XLaccess        xlChannelMask_both = m_xlChannelMask[m_nChannel] ;
	char            tmp[100];
	
	// Wait until the thread is done...
	Sleep(100);
	
	if (XL_INVALID_PORTHANDLE == m_xlPortHandle)
	{
		return(xlStatus);
	}

	CANGoOffBus() ;

/*
	if (xlChannelMask_both)
	{
		xlStatus = xlDeactivateChannel(m_xlPortHandle, xlChannelMask_both);
		sprintf(tmp, "xlDeactivateChannel, status: %d\n",  xlStatus);
		DEBUG(DEBUG_ADV, tmp);
		if (xlStatus)
		{
			return xlStatus;
		}
	}
*/
	
	xlStatus = xlClosePort(m_xlPortHandle);
	sprintf(tmp, "xlClosePort, status: %d\n",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	if (xlStatus)
	{
		return xlStatus;
	}
	
	m_xlPortHandle = XL_INVALID_PORTHANDLE;
	
	// 재시작 하면 죽는 문제가 여기 때문인가???
	
/*
	xlStatus = xlCloseDriver();
	sprintf(tmp, "xlCloseDriver, status: %d\n",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	if (xlStatus)
	{
		return xlStatus;
	}
*/
	
	if (m_pOutput)
	{
		m_pOutput->InsertString(-1, "CAN: Close All");
	}
	
	return xlStatus;
}

void CCANFunctions::demoPrintConfig(void)
{
	
	unsigned int i;
	char         str[XL_MAX_LENGTH + 1]="";
	
	TRACE("----------------------------------------------------------\n");
	TRACE("- %02d channels       Hardware Configuration               -\n", m_xlDrvConfig.channelCount);
	TRACE("----------------------------------------------------------\n");
	
	for( int i=0; i < m_xlDrvConfig.channelCount; i++)
	{
		
		TRACE("- Ch:%02d, CM:0x%03I64x,"
			, m_xlDrvConfig.channel[i].channelIndex
			, m_xlDrvConfig.channel[i].channelMask);
		
		strncpy(str, m_xlDrvConfig.channel[i].name, 23);
		TRACE("%23s,", str);
		
		memset(str, 0, sizeof(str));
		
		if (m_xlDrvConfig.channel[i].transceiverType != XL_TRANSCEIVER_TYPE_NONE)
		{
			strncpy( str, m_xlDrvConfig.channel[i].transceiverName, 13);
			TRACE("%13s -\n", str);
		}
		else
		{
			TRACE("    no Cab!   -\n", str);
		}
	}
	
	TRACE("----------------------------------------------------------\n\n");
}

////////////////////////////////////////////////////////////////////////////
//! canGetChannelMask
//! parse the registry to get the channelmask
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CCANFunctions::canGetChannelMask()
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];
	
	XLdriverConfig  xlDrvConfig;
	
	// default values
	unsigned int    hwType;
	unsigned int    hwIndex;
	unsigned int    hwChannel;
	
	//check for hardware:
	xlStatus = xlGetDriverConfig(&xlDrvConfig);
	if (xlStatus)
	{
		DEBUG(DEBUG_ADV,"Error in xlGetDriverConfig...");
		return xlStatus;
	}
	
	for (int appChannel = 0; appChannel < xlDrvConfig.channelCount; appChannel++)
	{
		hwType    = xlDrvConfig.channel[appChannel].hwType ;
		hwIndex   = xlDrvConfig.channel[appChannel].hwIndex ;
		hwChannel = xlDrvConfig.channel[appChannel].hwChannel ;
		
		int channelIndex = xlGetChannelIndex(hwType, hwIndex, hwChannel);
		
		// check if we have a valid LIN cab/piggy
		if (xlDrvConfig.channel[channelIndex].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN)
		{
			DEBUG(DEBUG_ADV,"Found CAN cab/piggy\n");
			// and check the right hardwaretype
			if (xlDrvConfig.channel[channelIndex].hwType == hwType)
			{
				m_xlChannelMask[appChannel] = xlGetChannelMask(hwType, hwIndex, hwChannel);
			}
			
		}
		else
		{
			m_xlChannelMask[appChannel] = 0 ;
			DEBUG(DEBUG_ADV,"No LIN cab/piggy found\n");
		}
		
		sprintf(tmp, "Init CAN hWType: %d; hWIndex: %d; hwChannel: %d; channelMask: 0x%I64x for appChannel: %d\n", 
			hwType, hwIndex, hwChannel, m_xlChannelMask[appChannel], appChannel);
		DEBUG(DEBUG_ADV,tmp);
		
	}
	
	return xlStatus;
}

/*
XLstatus CCANFunctions::canGetChannelMask()
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];

	// default values
	unsigned int  hwType     = 0;
	unsigned int  hwIndex    = 0;
	unsigned int  hwChannel  = 0;
//	unsigned int  appChannel = 0;
	unsigned int  busType    = XL_BUS_TYPE_CAN;   
	unsigned int  i;
	unsigned int  chan1Found = 0;
	unsigned int  chan2Found = 0;

//	XLdriverConfig  xlDrvConfig;

	//check for hardware:
	xlStatus = xlGetDriverConfig(&m_xlDrvConfig);
	if (xlStatus)
	{
		return xlStatus;
	}

	// we check only if there is an application registered or not.
//	xlStatus = xlGetApplConfig(m_AppName, CHAN01, &hwType, &hwIndex, &hwChannel, busType); 


//	for( int i = 0; i < m_xlDrvConfig.channelCount; i++)
	for (int appChannel = 0; appChannel < m_xlDrvConfig.channelCount; appChannel++)
	{

		hwType    = m_xlDrvConfig.channel[appChannel].hwType ;
		hwIndex   = m_xlDrvConfig.channel[appChannel].hwIndex ;
		hwChannel = m_xlDrvConfig.channel[appChannel].hwChannel ;
		
		int channelIndex = xlGetChannelIndex(hwType, hwIndex, hwChannel);
		
		// check if we have a valid LIN cab/piggy
		if (m_xlDrvConfig.channel[channelIndex].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN)
		{
			DEBUG(DEBUG_ADV,"Found LIN cab/piggy\n");
			// and check the right hardwaretype
			if (m_xlDrvConfig.channel[channelIndex].hwType == hwType)
			{
				m_xlChannelMask[appChannel] = xlGetChannelMask(hwType, hwIndex, hwChannel);
			}
			
		}
		else
		{
			m_xlChannelMask[appChannel] = 0 ;
			DEBUG(DEBUG_ADV,"No LIN cab/piggy found\n");
		}
		
		sprintf(tmp, "Init CAN hWType: %d; hWIndex: %d; hwChannel: %d; channelMask: 0x%I64x for appChannel: %d\n", 
			hwType, hwIndex, hwChannel, m_xlChannelMask[appChannel], appChannel);
		DEBUG(DEBUG_ADV,tmp);

		if (m_pHardware)
		{
			m_pHardware->InsertString(-1, m_xlDrvConfig.channel[i].name);
		}

/ *
		sprintf (tmp, "hwType: %d, bustype: %d, hwChannel: %d, cap: 0x%x", 
			m_xlDrvConfig.channel[i].hwType, 
			m_xlDrvConfig.channel[i].connectedBusType,
			m_xlDrvConfig.channel[i].hwChannel,
			m_xlDrvConfig.channel[i].channelBusCapabilities);
		DEBUG(DEBUG_ADV,tmp);

		// we search not the first CAN cabs
		if ((m_xlDrvConfig.channel[i].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN) && (appChannel < 2))
		{
			hwType    = m_xlDrvConfig.channel[i].hwType;
			hwIndex   = m_xlDrvConfig.channel[i].hwIndex;
			hwChannel = m_xlDrvConfig.channel[i].hwChannel;

			xlStatus = xlSetApplConfig( // Registration of Application with default settings
				m_AppName,             // Application Name
				appChannel,                 // Application channel 0 or 1
				hwType,                     // hwType  (CANcardXL...)    
				hwIndex,                    // Index of hardware (slot) (0,1,...)
				hwChannel,                  // Index of channel (connector) (0,1,...)
				busType);                   // the application is for CAN.

			m_xlChannelMask[appChannel] = xlGetChannelMask(hwType, hwIndex, hwChannel);
			sprintf (tmp, "Register CAN hWType: %d, CM: 0x%I64x", hwType, m_xlChannelMask[appChannel]);
			DEBUG(DEBUG_ADV,tmp);

			if (m_pHardware)
			{
				m_pHardware->InsertString(-1, m_xlDrvConfig.channel[i].name);
			}

			appChannel++;
		}
* /

	}

	return xlStatus;
}
*/

/*
XLstatus CCANFunctions::canInitDriver(XLaccess *pxlChannelMaskTx, unsigned char *pxlChannelIndex)
{
	XLstatus          xlStatus;
//	XLaccess          xlChannelMaskTx = 0;
	unsigned int      i;
	
	
	// ------------------------------------
	// open the driver
	// ------------------------------------
	xlStatus = xlOpenDriver ();
	
	// ------------------------------------
	// get/print the hardware configuration
	// ------------------------------------
	if (XL_SUCCESS == xlStatus)
	{
		xlStatus = xlGetDriverConfig(&m_xlDrvConfig);
	}
	
	if (XL_SUCCESS == xlStatus)
	{
#ifdef _DEBUG
		demoPrintConfig();
#endif
//		TRACE("Usage: xlCANdemo <BaudRate> <ApplicationName> <Identifier>\n\n");
		
		// ------------------------------------
		// select the wanted channels
		// ------------------------------------
//		g_xlChannelMask = 0;
		for( int i = 0; i < m_xlDrvConfig.channelCount; i++)
		{
			// we take all hardware we found and
			// check that we have only CAN cabs/piggy's
			// at the moment there is no VNss8910 XLAPI support!
			if (m_xlDrvConfig.channel[i].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN)
			{
				if (!*pxlChannelMaskTx)
				{
					*pxlChannelMaskTx = m_xlDrvConfig.channel[i].channelMask;
					*pxlChannelIndex  = m_xlDrvConfig.channel[i].channelIndex;
				}
				
//				g_xlChannelMask |= m_xlDrvConfig.channel[i].channelMask;

				if (m_pHardware)
				{
					m_pHardware->InsertString(-1, m_xlDrvConfig.channel[i].name);
				}
			}
		}
		
		if (!g_xlChannelMask)
		{
			TRACE("ERROR: no available channels found! (e.g. no CANcabs...)\n\n");
			xlStatus = XL_ERROR;
		}
	}
	
	m_xlPermissionMask = g_xlChannelMask;
	
	// ------------------------------------
	// open ONE port including all channels
	// ------------------------------------
	if (XL_SUCCESS == xlStatus)
	{
		xlStatus = xlOpenPort(&m_xlPortHandle, m_AppName, g_xlChannelMask, &m_xlPermissionMask, RX_QUEUE_SIZE, XL_INTERFACE_VERSION, XL_BUS_TYPE_CAN);
		TRACE("- OpenPort         : CM=0x%I64x, PH=0x%02X, PM=0x%I64x, %s\n"
			, g_xlChannelMask
			, m_xlPortHandle
			, m_xlPermissionMask
			, xlGetErrorString(xlStatus));
	}
	
	if ((XL_SUCCESS == xlStatus) && (XL_INVALID_PORTHANDLE != m_xlPortHandle))
	{
		// ------------------------------------
		// if we have permission we set the
		// bus parameters (baudrate)
		// ------------------------------------
		if (g_xlChannelMask == m_xlPermissionMask)
		{
			xlStatus = xlCanSetChannelBitrate(m_xlPortHandle, g_xlChannelMask, m_BaudRate);
			TRACE("- SetChannelBitrate: baudr.=%u, %s\n", m_BaudRate, xlGetErrorString(xlStatus));
		} 
		else
		{
			printf("-                  : we have NO init access!\n");
		}
		
	}
	else
	{
		
		xlClosePort(m_xlPortHandle);
		m_xlPortHandle = XL_INVALID_PORTHANDLE;
		xlStatus = XL_ERROR;
	}
	
	return xlStatus;
}*/


////////////////////////////////////////////////////////////////////////////

//! canInit

//! xlCANcontrol use ONE port for both channels.
//!
//////////////////////////////////////////////////////////////////////////// 

/*
XLstatus CCANFunctions::canInit()
{
	XLstatus         xlStatus = XL_ERROR;
	XLaccess         xlPermissionMask;
	char             tmp[100];

	// ---------------------------------------
	// Open ONE port for both channels 
	// ---------------------------------------

	// calculate the channelMask for both channel 
//	m_xlChannelMask_both = m_xlChannelMask[CHAN01] + m_xlChannelMask[CHAN02];
	m_xlChannelMask_both = m_xlChannelMask[CHAN01] + m_xlChannelMask[CHAN02];
	xlPermissionMask = m_xlChannelMask_both;

	xlStatus = xlOpenPort(&m_xlPortHandle, m_AppName, m_xlChannelMask_both, &xlPermissionMask, 256, XL_INTERFACE_VERSION, XL_BUS_TYPE_CAN); 
	sprintf(tmp, "xlOpenPort: PortHandle: %d; Permissionmask: 0x%I64x; Status: %d", m_xlPortHandle, xlPermissionMask, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	if (m_xlPortHandle == XL_INVALID_PORTHANDLE)
		return XL_ERROR;
	if (xlStatus == XL_ERR_INVALID_ACCESS)
		return xlStatus;

	return xlStatus;
}
*/

////////////////////////////////////////////////////////////////////////////

//! canCreateRxThread

//! set the notification and creates the thread.
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANFunctions::canCreateRxThread()
{

	XLstatus      xlStatus = XL_ERROR;
	DWORD         ThreadId=0;
	char          tmp[100];

	if (m_xlPortHandle!= XL_INVALID_PORTHANDLE)
	{
		// Send a event for each Msg!!!
		xlStatus = xlSetNotification (m_xlPortHandle, &m_hMsgEvent, 1);
		sprintf(tmp, "SetNotification '%d', xlStatus: %d", m_hMsgEvent, xlStatus);
		DEBUG(DEBUG_ADV, tmp);

		// for the RxThread
// 		m_tstruct.xlPortHandle = m_xlPortHandle;
// 		m_tstruct.hMsgEvent    = m_hMsgEvent; 
// 		m_tstruct.pOutput      = m_pOutput;

//		m_hThread = CreateThread(0, 0x1000, RxThread, (LPVOID) &g_th, 0, &ThreadId);
		m_hThread = CreateThread(0, 0x1000, CanRxThread, (LPVOID) this, 0, &ThreadId);
		sprintf(tmp, "CreateThread %d", m_hThread);
		DEBUG(DEBUG_ADV, tmp);

	}
	return xlStatus;
}


////////////////////////////////////////////////////////////////////////////

//! CANGoOnBus

//! set the selected baudrate and go on bus.
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANFunctions::CANGoOnBus(unsigned long baudrate)
{
	XLstatus      xlStatus = XL_ERROR;
	char          tmp[100];

/*
	xlStatus = xlCanSetChannelBitrate(m_xlPortHandle, m_xlChannelMask_both, baudrate);
*/
	XLaccess xlChannelMask_both = m_xlChannelMask[m_nChannel] ;

	xlStatus = xlCanSetChannelBitrate(m_xlPortHandle, xlChannelMask_both, baudrate);
	sprintf(tmp, "SetBaudrate: %d, stat: %d", baudrate, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	if (xlStatus != XL_SUCCESS)
	{
		return xlStatus ;
	}

/*
	xlStatus = xlActivateChannel(m_xlPortHandle, m_xlChannelMask_both, XL_BUS_TYPE_CAN, XL_ACTIVATE_RESET_CLOCK);
*/
	xlStatus = xlActivateChannel(m_xlPortHandle, xlChannelMask_both, XL_BUS_TYPE_CAN, XL_ACTIVATE_RESET_CLOCK);
	sprintf(tmp, "ActivateChannel, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANGoOffBus

//! Deactivate the channel
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANFunctions::CANGoOffBus()
{
	XLstatus      xlStatus = XL_ERROR;
	char          tmp[100];

/*
	xlStatus = xlDeactivateChannel(m_xlPortHandle, m_xlChannelMask_both);
*/

	XLaccess xlChannelMask_both = m_xlChannelMask[m_nChannel] ;

	xlStatus = xlDeactivateChannel(m_xlPortHandle, xlChannelMask_both);
	sprintf(tmp, "DeactivateChannel, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANSend

//! transmit a CAN message to the selected channel with the give values.
//!
////////////////////////////////////////////////////////////////////////////
/*

XLstatus CCANFunctions::CANSend(XLevent xlEvent, int channel)
{

	XLstatus      xlStatus;
	char          tmp[100];
	unsigned int  messageCount = 1;


	// Added by 224 (2014/07/13) : 선택한 채널로 데이터 보내기 기능 
	// 이전 채널과 다른 경우면, 벼경하는것이 좋기는 한데, 속도차이는 없을 듯 하여
	// 호출 시 마다 Tx 마스크 변경
	if (m_nChannel != channel)
	{
		m_nChannel = channel ;
		m_xlChanMaskTx = m_xlDrvConfig.channel[channel].channelMask;
	}

/ *
	xlStatus = xlCanTransmit(m_xlPortHandle, m_xlChannelMask[channel], &messageCount, &xlEvent);
* /
	xlStatus = xlCanTransmit(m_xlPortHandle, m_xlChanMaskTx, &messageCount, &xlEvent);
	sprintf(tmp, "Transmit, mc: %d, channel: %d, stat: %d",  messageCount, channel, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}
*/

XLstatus CCANFunctions::CANSend(XLevent xlEvent)
{

	XLstatus      xlStatus;
	char          tmp[100];
	unsigned int  messageCount = 1;


	// Added by 224 (2014/07/13) : 선택한 채널로 데이터 보내기 기능 
	// 이전 채널과 다른 경우면, 벼경하는것이 좋기는 한데, 속도차이는 없을 듯 하여
	// 호출 시 마다 Tx 마스크 변경
	XLaccess xlChannelMask_both = m_xlChannelMask[m_nChannel] ;

	ASSERT(m_xlPortHandle != XL_INVALID_PORTHANDLE) ;
TRACE("BF: xlCanTransmit(m_xlPortHandle, xlChannelMask_both, &messageCount, &xlEvent);\n") ;
	xlStatus = xlCanTransmit(m_xlPortHandle, xlChannelMask_both, &messageCount, &xlEvent);
TRACE("AT: xlCanTransmit(m_xlPortHandle, xlChannelMask_both, &messageCount, &xlEvent);\n") ;
	sprintf(tmp, "Transmit, mc: %d, channel: %d, stat: %d",  messageCount, m_nChannel, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANResetFilter

//! Reset the acceptancefilter
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANFunctions::CANResetFilter()
{
	XLstatus      xlStatus;
	char          tmp[100];

/*
	xlStatus = xlCanResetAcceptance(m_xlPortHandle, m_xlChannelMask_both, XL_CAN_STD);
*/


	xlStatus = xlCanResetAcceptance(m_xlPortHandle, m_xlChannelMask_both, XL_CAN_STD);
	sprintf(tmp, "CanResetAcceptance, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANSetFilter

//! Reset the acceptancefilter
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANFunctions::CANSetFilter(unsigned long first_id, unsigned long last_id)
{
	XLstatus      xlStatus;
	char          tmp[100];

	// because there all filters open, we close all.
/*
	xlStatus = xlCanSetChannelAcceptance(m_xlPortHandle, m_xlChannelMask_both, 0xFFF, 0xFFF, XL_CAN_STD);
*/
	XLaccess xlChannelMask_both = m_xlChannelMask[m_nChannel] ;
	xlStatus = xlCanSetChannelAcceptance(m_xlPortHandle, xlChannelMask_both, 0xFFF, 0xFFF, XL_CAN_STD);
	sprintf(tmp, "CanSetChannelAcceptance, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	// and now we can set the acceptance filter range.
/*
	xlStatus = xlCanAddAcceptanceRange(m_xlPortHandle, m_xlChannelMask_both, first_id, last_id);
*/
	xlStatus = xlCanAddAcceptanceRange(m_xlPortHandle, xlChannelMask_both, first_id, last_id);
	sprintf(tmp, "CanAddAcceptanceRange, firstID: %d, lastID: %d, stat: %d",  first_id, last_id, xlStatus);
	DEBUG(DEBUG_ADV, tmp);


	return xlStatus;
}

///////////////////////////////////////////////////////////////////////////

//! ShowLicenses

//! Reads licenses from the selected channels and displays it.
//!
////////////////////////////////////////////////////////////////////////////
/*
XLstatus CCANFunctions::ShowLicenses()
{
	XLstatus xlStatus;
	char licAvail[2048];
	char strtmp[512];

	// Show available licenses
	XLlicenseInfo licenseArray[1024];
	unsigned int licArraySize = 1024;
	xlStatus = xlGetLicenseInfo(m_xlChannelMask[CHAN01] | m_xlChannelMask[CHAN02], licenseArray, licArraySize);
	if (xlStatus == XL_SUCCESS)
	{
		strcpy(licAvail, "Licenses found:\n\n");
		for (unsigned int i = 0; i < licArraySize; i++)
		{
			if (licenseArray[i].bAvailable) {
				sprintf(strtmp, "ID 0x%03x: %s\n", i, licenseArray[i].licName);
				if ((strlen(licAvail) + strlen(strtmp)) < sizeof(licAvail))
				{
					strcat(licAvail, strtmp);
				}
				else
				{
					// Too less memory for printing licenses
					sprintf(licAvail, "Internal Error: String size in CCANFunctions::ShowLicenses() is too small!");
					xlStatus = XL_ERROR;
				}
			}
		}
	}
	else
	{
		sprintf(licAvail, "Error %d when calling xlGetLicenseInfo()!", xlStatus);
	}

	AfxMessageBox(licAvail);
	return xlStatus;
}
*/


///////////////////////////////////////////////////////////////////////////
//!
//! CanRxThread
//! thread to readout the message queue and parse the incoming messages
//!
////////////////////////////////////////////////////////////////////////////

DWORD WINAPI CanRxThread(LPVOID par) 
{

	XLstatus        xlStatus;

	unsigned int    msgsrx = 1;
	XLevent         xlEvent; 
	char            tmp[110];
	CString         str;

	CCANFunctions* pCan = (CCANFunctions*)par ;
//	CLinCanConvBroker* pBroker = pCan->GetConvBroker() ;
	CCanConvBroker* pBroker = pCan->GetConvBroker() ;

	// thread running
	pBroker->m_bCanRxThreadRun = TRUE ;

	HANDLE hMsgEvent = pCan->GetMsgEvent() ;
	XLportHandle portHandle = pCan->GetPortHandle() ;
	sprintf(tmp, "thread: SetNotification '%d'", hMsgEvent);
	DEBUG(DEBUG_ADV, tmp);

 	while (pBroker->m_bCanRxThreadRun)
	{ 
		WaitForSingleObject(hMsgEvent, 1000);
		xlStatus = XL_SUCCESS;

		while (!xlStatus && pBroker->m_bCanRxThreadRun)
		{
			msgsrx = 1;
			xlStatus = xlReceive(portHandle, &msgsrx, &xlEvent);	

			if (pBroker->m_bShowMessage)
			{
				if (xlStatus != XL_ERR_QUEUE_IS_EMPTY)
				{
					sprintf(tmp, "%s", xlGetEventString(&xlEvent));
					DEBUG(DEBUG_ADV, tmp);
					
// 					pBroker->m_pRxList->InsertString(-1, tmp);
// 					pBroker->m_pRxList->SetCurSel(pBroker->m_pRxList->GetCount()-1);
				}
			}

//			if (pBroker->m_bShowMessage)
			{
				pCan->m_bShowCycleDone = TRUE ;
				Sleep(0) ;
			}
		}

	}

	SetEvent(pBroker->m_hCanRxThreadTerminated) ;

	return NO_ERROR; 
}

XLstatus CCANFunctions::CANGetDevice()
{
	XLstatus         xlStatus = XL_ERROR;
	char            tmp[100];

	xlStatus = xlOpenDriver();
	sprintf(tmp, "xlOpenDriver, stat: %d", xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	if (xlStatus != XL_SUCCESS)
	{
		return xlStatus;
	}

	xlStatus = canGetChannelMask();

	// we need minimum one LIN channel for MASTER/SLAVE config
/*
	if (m_xlChannelMask[MASTER] == 0)
	{
		return XL_ERROR;
	}
*/

	return xlStatus;
}
