#if !defined(AFX_LINCONVDLG_H__A9D288BB_82E2_41C4_B773_2DC61B87BE83__INCLUDED_)
#define AFX_LINCONVDLG_H__A9D288BB_82E2_41C4_B773_2DC61B87BE83__INCLUDED_

#include "../MyGridWnd.h"	// Added by ClassView


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LinConvDlg.h : header file
//

#include "../CTSMonProDoc.h"
#include "xlCANStandAloneFunc.h"
#include "xlCANFunctions.h"

#include "../TabCtrlEx.h"

/////////////////////////////////////////////////////////////////////////////
// CLinConvDlg dialog

class CLinConvDlg : public CDialog
{
// Construction
public:

	CLinConvDlg(CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor
 	CLinConvDlg();
	~CLinConvDlg();
	DECLARE_DYNCREATE(CLinConvDlg)

// Attributes
public:
	int nSelCh;
	int nSelModule;
	int m_nDevID ;

	CString m_strConfigFilename ;
//	CString m_strDBName ;	// 현재 설정 DB 값 보관

//	CCANFunctions m_CAN;
	CCANStandAloneFunc m_CAN;
	CCTSMonProDoc* m_pDoc ;
	
	BOOL m_bCanRecvRxTxMsg ;

	// Operations
public:
	void InitParamTab();
	void InitGridRx();
	void InitGridTx();
	void InitGridSchedule();

	void GetLinIDInfo(CArray<PLINID_INFO, PLINID_INFO>& array) ;
	void GetScheduleInfo(CArray<PSCHEDULER_INFO, PSCHEDULER_INFO>& array) ;
	void GetLinConvInfo(CArray<PLCC_INFO, PLCC_INFO>& array);
	BOOL SaveCANConfig(CArray<PLCC_INFO, PLCC_INFO>& array);
//	BOOL m_bSendCANData;
	void SendCANData(XLevent& xlEvent);
//	void Update(CMMTimer &Timer);
//	void SetControlsWithDBName(CString strName) ;
	BOOL CheckMapValues() ;

// Dialog Data
	//{{AFX_DATA(CLinConvDlg)
	enum { IDD = IDD_LIN_CONV , IDD2 = IDD_LIN_CONV_ENG , IDD3 = IDD_LIN_CONV_PL };
	/*CTabCtrl*/	CTabCtrlEx m_ctrlParamTab;
	CComboBox	m_ctrlLinChannel;
	CComboBox	m_ctrlLinSpeed;
	CComboBox	m_Channel;
	CComboBox	m_CAN_Baudrate;
	CListBox	m_ctlCanHardware;
	CListBox	m_ctlRxList;
	CListBox	m_ctlTxList;
	CString	m_eID;
	BOOL	m_bSaveRx;
	BOOL	m_bSaveTx;
	BOOL	m_bCheckExtMode;
	//}}AFX_DATA
//	UINT	m_nLinID;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLinConvDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLinConvDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnEtcSetting();
	afx_msg void OnChangeLinId();
	afx_msg void OnClose();
	afx_msg void OnResetRxContents();
	afx_msg void OnResetTxContents();
	afx_msg void OnOperStart();
	afx_msg void OnOperStop();
	afx_msg void OnOpenConvData();
	afx_msg void OnTransCanData();
	afx_msg void OnSaveConfig();
	afx_msg void OnAddRow();
	afx_msg void OnDelRow();
	afx_msg void OnAddScheRow();
	afx_msg void OnDelScheRow();
	afx_msg void OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	//}}AFX_MSG
	afx_msg LRESULT OnGridBeginEdit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridEndEdit(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	CUIntArray m_uiArryCanCode;
	CStringArray m_strArryCanName;
//	CString m_strConvDBFilename ;

	CMyGridWnd m_wndLinGrid;
	CMyGridWnd m_wndScheduleGrid;
	CMyGridWnd m_wndTxGrid;

	INT m_nTabSelection ;

	// Added by 224 (2014/10/29) : csv 파일로 Load/Save 적용
	BOOL LoadLinConfig(CString strLoadPath);
	BOOL SaveLinConfig(CString strSavePath);

	void Fun_ChangeViewControl(BOOL bFlag);
	CString Fun_FindCanName(int nDivision);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINCONVDLG_H__A9D288BB_82E2_41C4_B773_2DC61B87BE83__INCLUDED_)
