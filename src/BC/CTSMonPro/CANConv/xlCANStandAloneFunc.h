/*----------------------------------------------------------------------------
| File        : xlCANFunctions.h
| Project     : Vector CAN Example 
|
| Description : shows the basic CAN functionality for the XL Driver Library
|-----------------------------------------------------------------------------
| $Author: vismra $    $Locker: $   $Revision: 4693 $
| $Header: /VCANDRV/XLAPI/samples/xlCANcontrol/xlCANFunctions.h 4     15.06.05 15:16 Harald $
|-----------------------------------------------------------------------------
| Copyright (c) 2004 by Vector Informatik GmbH.  All rights reserved.
|---------------------------------------------------------------------------*/

#if !defined(AFX_XLCANSTANDALONEFUNC_H__48DFA4A9_72B2_48FE_80D5_D318A80C4B3A__INCLUDED_)
#define AFX_XLCANSTANDALONEFUNC_H__48DFA4A9_72B2_48FE_80D5_D318A80C4B3A__INCLUDED_

#include "vxlapi.h"

#define CHAN01 0
#define CHAN02 1

typedef struct {
    XLportHandle xlPortHandle; 
    HANDLE       hMsgEvent;
    CListBox    *pOutput;
} TStruct;

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCANStandAloneFunc  
{
public:
	CCANStandAloneFunc();
	virtual ~CCANStandAloneFunc();

	XLstatus CANInit();
	XLstatus CANGoOnBus(unsigned long baudrate);
	XLstatus CANGoOffBus();
	XLstatus CANSend(XLevent xlEvent, int channel);
	XLstatus CANResetFilter();
	XLstatus CANSetFilter(unsigned long first_id, unsigned long last_id);
	XLstatus ShowLicenses();

	CListBox *m_pOutput;
	CListBox *m_pHardware;

private:
	void demoPrintConfig(void) ;
	XLdriverConfig  m_xlDrvConfig;
	XLaccess        m_xlPermissionMask ;
	XLaccess        g_xlChannelMask ;
	unsigned int    m_BaudRate ;                 //!< Default baudrate

	char            m_AppName[XL_MAX_LENGTH+1] ;
	XLstatus		canInitDriver(XLaccess *pxlChannelMaskTx, unsigned char *pxlChannelIndex) ;
	XLaccess        m_xlChanMaskTx ;
	unsigned char   m_xlChanIndex ;
	INT	m_nChannel ;



	XLstatus         canGetChannelMask();
	XLstatus         canInit();
	XLstatus         canCreateRxThread();

	XLaccess         m_xlChannelMask[2];        //!< we support only two channels
	XLportHandle     m_xlPortHandle;            //!< and one port
	XLaccess         m_xlChannelMask_both;

	HANDLE           m_hThread;
	XLhandle         m_hMsgEvent;
	int              m_bInitDone;
};

DWORD     WINAPI RxThread( PVOID par );


#endif // !defined(AFX_XLCANSTANDALONEFUNC_H__48DFA4A9_72B2_48FE_80D5_D318A80C4B3A__INCLUDED_)
