#if !defined(AFX_MODBUSCONVDLG_H__B2D30AE5_3343_4AB2_9C85_DCFBA5EC594C__INCLUDED_)
#define AFX_MODBUSCONVDLG_H__B2D30AE5_3343_4AB2_9C85_DCFBA5EC594C__INCLUDED_

#include "../MyGridWnd.h"	// Added by ClassView

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModbusConvDlg.h : header file
//

#include "../CTSMonProDoc.h"
#include "xlCANStandAloneFunc.h"
#include "ModbusCanConvBroker.h"
#include "../Resource.h"
//#include "xlCANFunctions.h"
//#include "CTSMonProDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CModbusConvDlg dialog
typedef enum
{
	COL_SEQ,
	COL_ADDR,
	COL_INDEX,
	COL_ITEMS,
	COL_STARTBIT,
	COL_BITCOUNTS,
	COL_DATATYPE,
	COL_FACTOR,
	COL_OFFSET,
	COL_FAULT_UPPER,
	COL_FAULT_LOWER,
	COL_END_UPPER,
	COL_END_LOWER,
	COL_FUNCTION_DIVISION,
	COL_FUNCTION_DIVISION2,
	COL_FUNCTION_DIVISION3,
	COL_DESC,
	COL_NUM = COL_DESC,
} COL_TYPE ;

/*
typedef struct tagMCC_INFO
{
	// modbus addr, startbit, bitcounts, name, datatype, canid, startbit, bitcounts
	INT addr ;
	INT mb_startbit ;
	INT mb_bitcounts ;
	CHAR name[256] ;
	INT datatype ;	// Unsign:0, Sign:1,
	INT canid ;
	INT can_startbit ;
	INT can_bitcounts ;
	FLOAT factor ; //OFFSET, SAFEUPPERLIMIT, SAFELOWERLIMIT, TERMUPPERLIMIT, TERMLOWERLIMIT, CAT1, CAT2, CAT3
	FLOAT offset ;
	FLOAT fault_upper ;
	FLOAT fault_lower ;
	FLOAT end_upper ;
	FLOAT end_lower ;
	short int function_division ;
	short int function_division2 ;
	short int function_division3 ;
	CHAR desc[512] ;
} MCC_INFO, *PMCC_INFO ;
*/

class CCTSMonProDoc ;
class CCANStandAloneFunc ;
typedef struct _modbus modbus_t;
class CModbusConvDlg : public CDialog
{
// Construction
public:
	CModbusConvDlg(CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor
	CModbusConvDlg(); 
	DECLARE_DYNCREATE(CModbusConvDlg)

// Dialog Data
	//{{AFX_DATA(CModbusConvDlg)
	enum { IDD = IDD_MODBUS_CONV , IDD2 = IDD_MODBUS_CONV_ENG , IDD3 = IDD_MODBUS_CONV_PL };
	CComboBox	m_ctrlDatabits;
	CComboBox	m_ctrlParitybit;
	CComboBox	m_ctrlStopbit;
	CComboBox	m_ctrlMBBaudrate;
	CButton	m_btnSend;
	CComboBox	m_Channel;
	CComboBox	m_CAN_Baudrate;
	CButton	m_btnOffBus;
	CButton	m_btnOnBus;
	CListBox	m_ctlTxList;
	CComboBox	m_ctlMBFunc;
	CListBox	m_ctlRxList;
	CComboBox	m_ctlDeviceComm;
	CString	m_strAddr;
	UINT	m_nResponseTimeout;
	UINT	m_nPort;
	UINT	m_nMBLen;
	UINT	m_nMBScanRate;
	UINT	m_nSlaveID;
	UINT	m_nMBAddr;
	CString	m_eID;
	BOOL	m_bCheckExtMode;
	BOOL	m_bSaveRx;
	BOOL	m_bSaveTx;
	//}}AFX_DATA


// Attributes
public:
	int nSelCh;
	int nSelModule;
	int m_nDevID ;

	modbus_t* ctx;
//	HANDLE m_hTimer ;
//	HANDLE m_hTimerQueue ;
    UINT m_wTimerRes;
	DWORD m_timerID ;

	CString m_strDBName ;	// 현재 설정 DB 값 보관


// 	static HANDLE m_hEvent ;
//  	static VOID CALLBACK TimerRoutine(PVOID lpParam, BOOLEAN TimerOrWaitFired) ;
// 	static void CALLBACK TimeProc(UINT _timerId, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2) ;

//	CCANFunctions m_CAN;
//	CCANStandAloneFunc m_CAN;
	CCTSMonProDoc* m_pDoc ;

	BOOL m_bCanRecvRxTxMsg ;

//	static BOOL m_bRunning ;

// Operations
public:
	static void GetModbusCommInfo(CString strName, MODBUS_INFO* pInfo, BOOL bShowMessage = TRUE);
	static void GetModbusConvInfo(CString strName, CArray<PMCC_INFO, PMCC_INFO>& array, BOOL bShowMessage = TRUE);
	void GetModbusConvInfo(CArray<PMCC_INFO, PMCC_INFO>& array);
	BOOL SaveCANConfig(CArray<PMCC_INFO, PMCC_INFO>& array);
	BOOL m_bSendCANData;
	void SendCANData(XLevent& xlEvent);
//	void Update(CMMTimer &Timer);
	void SetControlsWithDBName(CString strName) ;
	BOOL CheckMapValues() ;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModbusConvDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CModbusConvDlg)
	virtual BOOL OnInitDialog();
	//virtual void OnCancel();
	//virtual void OnOK();
	afx_msg void OnResetRxContents();
	afx_msg void OnResetTxContents();
	afx_msg void OnOperStart();
	afx_msg void OnOperPause();
	afx_msg void OnOperStop();
	afx_msg void OnAddRow();
	afx_msg void OnDelRow();
	afx_msg void OnOpenConvData();
	afx_msg void OnTransCanData();
	afx_msg void OnSaveConfig();
	afx_msg void OnSelchangeModbusFunction();
	afx_msg void OnSelchangeDeviceCommunication();
	afx_msg void OnClose();
	afx_msg void OnEtcSetting();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnSaveRxPacket();
	afx_msg void OnSaveTxPacket();
	//}}AFX_MSG
	afx_msg LRESULT OnGridBeginEdit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridEndEdit(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
		
private:
	CUIntArray m_uiArryCanCode;
	CStringArray m_strArryCanName;
	
	CMyGridWnd m_wndModGrid;

	CString Fun_FindCanName(int nDivision);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODBUSCONVDLG_H__B2D30AE5_3343_4AB2_9C85_DCFBA5EC594C__INCLUDED_)
