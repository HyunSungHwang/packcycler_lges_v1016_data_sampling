// ModbusCanConvBroker.cpp: implementation of the CModbusCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////
#pragma comment(lib, "vxlapi.lib")

#include "stdafx.h" 
#include "../CTSMonPro.h"

#include "../CyclerModule.h"
#include "../CyclerChannel.h"
#include "ModbusConvDlg.h"
#include "../CTSMonProDoc.h"
#include "ModbusCanConvBroker.h"
//#include <Mmsystem.h>

#include "mmsystem.h"

#define GETBIT(data, index) ((data & (1 << index)) >> index)

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//BOOL g_bCanRxThreadRun[4] ;
//extern BOOL    g_bCANThreadRun;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CModbusCanConvBroker::CModbusCanConvBroker()
{
	// 변수 초기화
//	m_bCanRxThreadRun = FALSE ;
	m_bSendCANData = FALSE ;
	m_nModbusMode = MCC_STOP ;

// 	m_bLinRxThreadRun = FALSE ;
 //	m_bCanRxThreadRun = FALSE ;
//	m_bShowMessage = FALSE ;
	m_pRxList = NULL ;
	m_pTxList = NULL ;

//	m_pLIN = NULL ;
	m_pCAN = NULL ;

	m_bSaveRxFlag = FALSE ;
	m_bSaveTxFlag = FALSE ;

	// 초기화를 해주는 방법 고민 필요
	// 예. Module 에는 반드시 두개의 채널이 있어야한다?
	m_nDevID = -1 ;
//	m_nLinID = -1 ;

	// 종료 이벤트
	m_hModubsSchedulerTerminated = INVALID_HANDLE_VALUE ;
	m_hModbusRxThreadTerminated  = INVALID_HANDLE_VALUE ;
	m_hCanRxThreadTerminated     = INVALID_HANDLE_VALUE ;

	ctx = NULL ;

	m_pModbusDlg = NULL ;
}

CModbusCanConvBroker::~CModbusCanConvBroker()
{
	if (m_pCAN)
	{
		delete m_pCAN ;
	}
}

// Added by 224 (2014/10/09) : LIN-CAN Convert 쓰레드 구동
BOOL CModbusCanConvBroker::BeginModbusCanConvThread(int nDevID
												  , PMODBUS_INFO pMBInfo
												  , CString strDBName
												  , CArray<PMCC_INFO, PMCC_INFO>& mcc
												  , int nCanChannel
												  , int nCanBaudrate
												   )
{
	// Pause 상태에서 재시작 하는 경우 처리
	if (m_nModbusMode == MCC_PAUSE)
	{
		m_nModbusMode = MCC_PLAY ;
		return TRUE ;
	}
	else if (m_nModbusMode == MCC_PLAY)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}

	//------------------------ SBC init ------------------------
	//------------------------ SBC init ------------------------

	//----------------- converting array 설정 ----------------- 
	

	m_nDevID = nDevID ;

	ASSERT(ctx == NULL) ;
	// Open the serial port
	// 1. Check COM Port
	// 	UINT uPort = GetPrivateProfileInt("SENSOR", "PORT", 1, ".\\config.ini");
	// 	
	// 	CString strPort ;
	// 	strPort.Format("COM%d", uPort) ;

	// TCP/IP 또는 시리얼통신 여부를 확인한다.
	m_strModbusCommunication = CString(pMBInfo->comm) ;
	if (m_strModbusCommunication.Left(3) == "COM")
	{	

		m_cModbusParity = pMBInfo->Parity ; 

		m_nModbusDatabit = pMBInfo->Databit ;
		ASSERT(m_nModbusDatabit == 7 || m_nModbusDatabit == 8) ;

		m_nModbusStopbit = pMBInfo->Stopbit ;
		ASSERT(m_nModbusStopbit == 1 || m_nModbusStopbit == 2) ;

		m_lModbusBaudrate = pMBInfo->Baudrate ;

		ctx = modbus_new_rtu(m_strModbusCommunication, m_lModbusBaudrate, m_cModbusParity, m_nModbusDatabit, m_nModbusStopbit);
	}
	else if (m_strModbusCommunication.Left(3) == "TCP")
	{
		m_strModbusIPAddr = CString(pMBInfo->IPAddr) ;
		m_nModbusPort     = pMBInfo->Port ;

		ctx = modbus_new_tcp(m_strModbusIPAddr, m_nModbusPort);
	}
	else
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}

//	ctx = modbus_new_tcp("127.0.0.1", 502);
//	ctx = modbus_new_tcp("17.91.30.246", 502);	// 삼성 SDI (천안) BMW IP
	m_nSlaveID = 1 ;
	
	if (ctx == NULL)
	{
		TRACE("Unable to allocate libmodbus context\n");
//		LOG(__FILE__, __LINE__, "Unable to allocate libmodbus context") ;
// 		if (hwnd)
// 		{
// 			m_pModbusDlg->m_ctlRxList.AddString("Unable to allocate libmodbus context") ;
// 			m_pModbusDlg->m_ctlRxList.SetCurSel(m_pModbusDlg->m_ctlRxList.GetCount()-1);  
// 		}

		ASSERT(FALSE) ;
		return FALSE ;
	}
	
//	modbus_set_debug(ctx, TRUE);
//	modbus_set_error_recovery(ctx, modbus_error_recovery_mode(MODBUS_ERROR_RECOVERY_LINK | MODBUS_ERROR_RECOVERY_PROTOCOL));
	
	if (modbus_connect(ctx) == -1)
	{
		TRACE("Connection failed: %s\n", modbus_strerror(errno));
//		LOG(__FILE__, __LINE__, "libmodbus Connection failed") ;
/*//*
		if (hwnd)
		{
			if (m_strModbusCommunication.Left(3) == "TCP")
			{
				m_strModbusIPAddr = m_pModbusDlg->m_strAddr ;
				m_nModbusPort     = m_pModbusDlg->m_nPort ;

				CString str ;
				str.Format("MODBUS Connection failed. w/ TCP, %s, %d", m_strModbusIPAddr, m_nModbusPort) ;

				m_pModbusDlg->m_ctlRxList.AddString(str) ;
				m_pModbusDlg->m_ctlRxList.SetCurSel(m_pModbusDlg->m_ctlRxList.GetCount()-1);  
			}
			else
			{
				
				m_pModbusDlg->m_ctlRxList.AddString("MODBUS Connection failed") ;
				m_pModbusDlg->m_ctlRxList.SetCurSel(m_pModbusDlg->m_ctlRxList.GetCount()-1);  
			}
		}
*/

		modbus_free(ctx);
		ctx = NULL ;

		ASSERT(FALSE) ;
		return FALSE ;
	}

	// 설정한 모드버스 주소를 저장한 후에 read_register() 호출 시에 사용한다.
	m_nModbusAddr = pMBInfo->MBAddr ;
	m_nModbusLen  = pMBInfo->MBLen ;
	

	//------------------------ modbus init ------------------------

	

	//------------------------ can init ------------------------
	ASSERT(m_pCAN == NULL) ;
	{
		m_pCAN = new CCANFunctions ;
		
		m_pCAN->m_pOutput = m_pRxList ;

		XLstatus xlStatus = m_pCAN->CANGetDevice() ;
		
		if (xlStatus != XL_SUCCESS)
		{
			TRACE("Unable to allocate libmodbus context\n");
			
			ASSERT(m_bShowMessage == TRUE && m_pRxList != NULL) ;
			if (m_bShowMessage)
			{
				m_pRxList->AddString("ERROR: You need CANcabs or CANPiggy's!") ;
				m_pRxList->SetCurSel(m_pRxList->GetCount()-1);  
			}
			
			ASSERT(FALSE) ;
			return FALSE ;
		}
		
		// init the CAN hardware
		if (m_pCAN->CANInit(this, m_nDevID))
		{
			ASSERT(FALSE) ;
			// 		m_ctlCanHardware.ResetContent();
			// 		m_ctlCanHardware.AddString("<ERROR> no HW!");
			// 		m_ctlTxList.AddString("You need two CANcabs/CANpiggy's");
			// 		m_btnOnBus.EnableWindow(FALSE);
			
			ASSERT(m_bShowMessage == TRUE && m_pTxList != NULL) ;
			if (m_bShowMessage)
			{
				m_pTxList->AddString("You need two CANcabs/CANpiggy's") ;
				m_pTxList->SetCurSel(m_pTxList->GetCount()-1);  
			}
			
			delete m_pCAN ;
			m_pCAN = NULL ;
			
			return FALSE ;
		}
		
		// go on bus
		//	int           nIndex=0;
		//	unsigned long bitrate[5] = {125000, 250000, 500000, 1000000};
		
		xlStatus = m_pCAN->CANGoOnBus(pMBInfo->can_baudrate);
		if (!xlStatus)
		{
			m_bSendCANData = TRUE ;
		}
		else
		{
			m_pRxList->AddString("ERROR: CANGoOnBus() !") ;

			ASSERT(FALSE) ;
			m_bSendCANData = FALSE ;
			
			return FALSE ;
		}
	}

	//------------------------ can init ------------------------

	//----------------- converting array 설정 ----------------- 
	//
	// 	m_mcc_array.RemoveAll() ;
	ASSERT(m_mcc_array.GetSize() == 0) ;
	for (INT nLoop = 0; nLoop < mcc.GetSize(); nLoop++)
	{
		MCC_INFO* info = mcc.GetAt(nLoop) ;
		
		m_mcc_array.Add(info) ;
	}

// 	// clearing
// 	for(int nLoop = 0; nLoop < m_mcc_array.GetSize(); nLoop++)
// 	{
// 		MCC_INFO* ptr = m_mcc_array.GetAt(nLoop) ;
// 		delete ptr ;
// 	}
// 	
// 	m_mcc_array.RemoveAll() ;
// 	
// 	m_pModbusDlg->GetModbusConvInfo(m_mcc_array) ;


	//------------------------ timer init -------------------------
	m_nModbusMode = MCC_PLAY ;
	m_strModbusSettingName = strDBName ;

    TIMECAPS tc;
	//    UINT wTimerRes;
	
    // 타이머의 최소/최대 해상도를 얻어옵니다.
    if (timeGetDevCaps (&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR)
	{
		// 에러발생
		//		return -1;
		ASSERT(FALSE) ;
		return FALSE ;
    }
	
#define TARGET_RESOLUTION       1		// 1-millisecond target resolution
	
    m_wTimerRes = min(max(tc.wPeriodMin, TARGET_RESOLUTION), tc.wPeriodMax);
	
	// 타이머의 최소 해상도를 설정합니다.
	if (timeBeginPeriod (m_wTimerRes) != TIMERR_NOERROR)
	{
		// 에러발생
		//		return -1;
		ASSERT(FALSE) ;
		return FALSE ;
	}
	
	// 1ms의 타이머 이벤트를 시작합니다.
	UINT interval = 1000;  // 단위: ms
	m_timerID = timeSetEvent (interval, m_wTimerRes, TimeProc, (DWORD)this, TIME_PERIODIC);
	if (m_timerID == NULL)
	{
		// 에러발생
		ASSERT(FALSE) ;
		//        return -1;
	}
	//------------------------ timer init -------------------------
	

	if (m_hCanRxThreadTerminated == INVALID_HANDLE_VALUE)
	{
		m_hCanRxThreadTerminated = CreateEvent(NULL, FALSE, TRUE, NULL) ;
		//		DWORD dwErr = GetLastError() ;
	}

	// Tx/Rx 메시지 전시 flag 를 설정한다.
	// 자동 실행하는 
//	m_bShowMessage = TRUE ;

	return TRUE ;
}


void CModbusCanConvBroker::StopModbusCanConvThread()
{
	// 1. modbus close
	if (ctx)
	{
		modbus_close(ctx);
		modbus_free(ctx);
		ctx = NULL ;
	}
	
	// timer 프로시저에서 CAN으로 보내는 데이터를 정지하고, 잠시 시간을 준다.
	m_bSendCANData = FALSE ;
	Sleep(2000) ;

	// 2. 타이머를 해제 합니다.
	if (timeKillEvent (m_timerID) != TIMERR_NOERROR)
	{
        // 에러발생
		//        return -1;
	}
	
	// 3. CAN 해제
	
	if (m_pCAN)
	{
		m_bCanRxThreadRun = FALSE ;

		// wait untill thread exiting
		DWORD dw = WaitForSingleObject(m_hCanRxThreadTerminated, INFINITE) ;
		TRACE("Signaled m_hCanRxThreadTerminated\n") ;
		ASSERT(dw == WAIT_OBJECT_0) ;
		
	}
	
	// 3. CAN 해제
	if (m_pCAN)
	{
		m_pCAN->CANClose() ;

		Sleep(500) ;
		
		delete m_pCAN ;
		m_pCAN = NULL ;
	}

	// 3.1. CAN Rx 쓰레드 종료

	// 4. converting array clearing
	for (INT nLoop = 0; nLoop < m_mcc_array.GetSize(); nLoop++)
	{
		MCC_INFO* ptr = m_mcc_array.GetAt(nLoop) ;
		delete ptr ;
	}
	m_mcc_array.RemoveAll() ;

	
	m_nModbusMode = MCC_STOP ;
	m_strModbusSettingName.Empty() ;
	m_bShowMessage = FALSE ;
//	m_strModbusSettingName.Empty() ;	
}

void CModbusCanConvBroker::DoNotShowMessage()
{
	// Commented by 224 (2104/10/15) :
	// Lin dialog 창을 닫을 때
	// 세개의 쓰레드에서 dialog의 컨트롤에 메시지 전시하는
	// 로직이 동기화가 되지 않으면, NULL 포인트 접근을 하여
	// 프로그램의 오류가 발생한다.

	// 현재 visual 이 활성화된 브로커에게만 호출한다.
	ASSERT(m_bShowMessage == TRUE) ;
	// Can Rx, Lin Rx, Lin Scheduler 쓰레드의 메시지 전시가 종료된 후에 리턴한다.

	m_bShowMessage = FALSE ;

	// CAN Rx 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pCAN != NULL) ;
	// 생성 실패시 나타날 수 있음
	// Can Rx 는 메시지 이벤트 발생시에만 작동하므로 기다릴 필요가 없다.
/*
	if (m_pCAN)
	{
		m_pCAN->m_bShowCycleDone = FALSE ;
		Sleep(0) ;
		
		while (!m_pCAN->m_bShowCycleDone)
		{
			Sleep(1) ;
		}
		
	}
*/
	TRACE("CAN Rx 메시지 종료 확인\n") ;

	// Lin Rx 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pLIN != NULL) ;
/*
	if (m_pLIN)
	{
		m_pLIN->m_bShowRxCycleDone = FALSE ;
		Sleep(0) ;
		
// 		while (!m_pLIN->m_bShowRxCycleDone)
// 		{
// 			Sleep(1) ;
// 		}
	}
*/

	TRACE("Lin Rx 메시지 종료 확인\n") ;

	// Lin TX 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pLIN != NULL) ;
/*
	if (m_pLIN)
	{
		m_pLIN->m_bShowTxCycleDone = FALSE ;
		Sleep(0) ;
		
// 		while (!m_pLIN->m_bShowTxCycleDone)
// 		{
// 			Sleep(1) ;
// 		}
	}
*/

	TRACE("Lin Tx 메시지 종료 확인\n") ;

	return ;
}

//void CModbusCanConvBroker::SendCANData(XLevent &xlEvent)
XLstatus CModbusCanConvBroker::SendCANData(XLevent &xlEvent)
{
	//----------------------------------------------------------
	// CAN 으로 전송
	
/*
	XLevent xlEvent;	
	memset(&xlEvent, 0, sizeof(xlEvent));
	
	// 전송
	xlEvent.tag                 = XL_TRANSMIT_MSG;
	xlEvent.tagData.msg.dlc     = xlLinEvent.tagData.linMsgApi.linMsg.dlc ;
	xlEvent.tagData.msg.flags   = 0;
	xlEvent.tagData.msg.id      = xlLinEvent.tagData.linMsgApi.linMsg.id ;
	
	for (int nIdx = 0; nIdx < xlEvent.tagData.msg.dlc; nIdx++)
	{
		xlEvent.tagData.msg.data[nIdx] = xlLinEvent.tagData.linMsgApi.linMsg.data[nIdx] ;
	}
	
	TRACE("xlEvent.tagData.msg.id      = 0x%02x\n", xlEvent.tagData.msg.id) ;
	TRACE("xlEvent.tagData.msg.data[0] = 0x%02x\n", xlEvent.tagData.msg.data[0]) ;
	TRACE("xlEvent.tagData.msg.data[1] = 0x%02x\n", xlEvent.tagData.msg.data[1]) ;
	TRACE("xlEvent.tagData.msg.data[2] = 0x%02x\n", xlEvent.tagData.msg.data[2]) ;
	TRACE("xlEvent.tagData.msg.data[3] = 0x%02x\n", xlEvent.tagData.msg.data[3]) ;
	TRACE("xlEvent.tagData.msg.data[4] = 0x%02x\n", xlEvent.tagData.msg.data[4]) ;
	TRACE("xlEvent.tagData.msg.data[5] = 0x%02x\n", xlEvent.tagData.msg.data[5]) ;
	TRACE("xlEvent.tagData.msg.data[6] = 0x%02x\n", xlEvent.tagData.msg.data[6]) ;
	TRACE("xlEvent.tagData.msg.data[7] = 0x%02x\n", xlEvent.tagData.msg.data[7]) ;
*/

	ASSERT(m_bSendCANData) ;
	XLstatus  xlStatus = XL_SUCCESS ;

	if (m_pCAN != NULL)
	{
		xlStatus = m_pCAN->CANSend(xlEvent);
	}	

	TRACE("------------------------------------ CAN SEND ------------------------------------\n") ;
//	return xlStatus ;

//	EnterCriticalSection(m_pcsShowMessage) ;

	// Added by 224 (2014/11/10) : 동기화 불일치 문제로 메시지 치리 안함.	

	if (m_bShowMessage)
//	if (1 == 0)
	{
		ASSERT(m_pTxList != NULL) ;

		CString strCont ;
		strCont.Format("ID:%02d ", xlEvent.tagData.msg.id) ;
		for (int nIdx = 0; nIdx < xlEvent.tagData.msg.dlc; nIdx++)
		{
			CString str ;
			str.Format("[0x%02X] ", xlEvent.tagData.msg.data[nIdx]) ;
			
			strCont += str ;
		}
		
		// 로그 파일에 저장
		if (m_bSaveTxFlag)
		{
			CHAR buf[1024] ;
			
			strcpy(buf, strCont) ;
			strcat(buf, "\n") ;
			
			CTime ctCur = CTime::GetCurrentTime();
			CString csFilename;
			csFilename.Format(_T("tx_%04d%02d%02d.log"), ctCur.GetYear(), ctCur.GetMonth(), ctCur.GetDay());
			
			FILE* fp = fopen(csFilename, "a+") ;
			//			FILE* fp = fopen(".\\tx.log", "a");
			fwrite(buf, strlen(buf), 1, fp) ;
			fclose(fp) ;
		}	

		// commented by 224 (2014/10/09) :

		int cnt = m_pTxList->GetCount() ;
		while (cnt > 99)
		{
			if (m_pTxList == NULL)
				break ;

			m_pTxList->DeleteString(0) ;

			if (m_pTxList == NULL)
				break ;

			cnt = m_pTxList->GetCount() ;
		}
		
		if (m_pTxList)
		{
			m_pTxList->InsertString(-1, strCont) ;
		}

		if (m_pTxList)
		{
			m_pTxList->SetCurSel(m_pTxList->GetCount()-1);  
		}

	}

/*
	m_pLIN->m_bShowTxCycleDone = TRUE ;
*/

//	LeaveCriticalSection(m_pcsShowMessage) ;
	
	return xlStatus ;
	
}

/*
void CModbusCanConvBroker::linCreateSchedulerThread()
{
//	XLstatus      xlStatus;
	DWORD         ThreadId=0;
	char          tmp[100];
	
	if (m_pLIN->IsValid())
	{
		
		HANDLE hThread ;
		hThread = CreateThread(0, 0x1000, LinSchedulerThread, (LPVOID) this, 0, &ThreadId);
		sprintf(tmp, "CreateThread %d", hThread);
		//		DEBUG(DEBUG_ADV, tmp);
		
	}
	
}
*/

VOID CALLBACK TimeProc(UINT _timerId, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	CModbusCanConvBroker* pBroker = (CModbusCanConvBroker*)dwUser ;

	// 일시정지 인 경우는 바로 리턴한다.
	if (pBroker->m_nModbusMode == MCC_PAUSE)
	{
		return ;
	}

//	CCTSMonProDoc* pDoc = (CCTSMonProDoc*)dwUser ;
	ASSERT(pBroker->m_nModbusMode == MCC_PLAY) ;
	ASSERT(pBroker->m_nSlaveID > 0) ;

	modbus_set_slave(pBroker->ctx, pBroker->m_nSlaveID) ;

	// 0. 레지스터값 보관 변수
	//-----------------------------------------------------------------
	uint16_t* registers ;
	registers = (uint16_t *) malloc(pBroker->GetModbusLen() * sizeof(uint16_t));

	// 1. 
	//-----------------------------------------------------------------
	int rc = modbus_read_input_registers(pBroker->ctx, pBroker->GetModbusAddr()-1, pBroker->GetModbusLen(), registers);
	if (rc != pBroker->GetModbusLen())
	{
		// commented by 224 (2014/06/25) : SelectScheduleDlg.cpp 228 라인에서 Dao 함수 호출 시 네트워크가 끊어지는 현상
		// -> m_pModbusDlg 의 변수를 참조해서 생기는 문제였음.

		//		--------------------- Error ----------------------
		TRACE("FAILED (nb points %d), len = %d\n", rc, pBroker->GetModbusLen());
		TRACE("failed: %s\n", modbus_strerror(errno));

		delete registers ;

		Sleep(1000) ;
		return ;
	}

	CString strCont ;
	for (int nIdx = 0; nIdx < pBroker->GetModbusLen(); nIdx++)
	{
		CString str ;
		str.Format("[0x%02X] ", registers[nIdx]) ;

		strCont += str ;
	}

	// 로그 파일에 저장
	if (pBroker->m_bSaveRxFlag)
	{
		CHAR buf[1024] ;
		
		strcpy(buf, strCont) ;
		strcat(buf, "\n") ;
	
		CTime ctCur = CTime::GetCurrentTime();
		CString csFilename;
		csFilename.Format(_T("rx_%04d%02d%02d.log"), ctCur.GetYear(), ctCur.GetMonth(), ctCur.GetDay());
		
		FILE* fp = fopen(csFilename, "a+") ;
		
//		FILE* fp = fopen(".\\modbus_rx.log", "a");
		fwrite(buf, strlen(buf), 1, fp) ;
		fclose(fp) ;
	}	

	HWND hwnd = FindWindow(NULL, "Modbus Config") ;
/*
	if (hwnd && pBroker->m_pModbusDlg->m_bCanRecvRxTxMsg)
	{
		pBroker->m_pModbusDlg->m_ctlRxList.AddString(strCont) ;
		pBroker->m_pModbusDlg->m_ctlRxList.SetCurSel(pBroker->m_pModbusDlg->m_ctlRxList.GetCount()-1);  

	}
*/
	//-----------------------------------------------------------------

	//-----------------------------------------------------------------
	
	// 2. Send data by CAN
	if (pBroker->m_bSendCANData)
	{

//------------------------------------------
		
		XLevent xlEvent;	
		memset(&xlEvent, 0, sizeof(xlEvent));
		
		INT nCurCANID = -1 ;
		for (INT nLoop = 0; nLoop < pBroker->m_mcc_array.GetSize(); nLoop++)
		{
			MCC_INFO* ptr = pBroker->m_mcc_array.GetAt(nLoop) ;

			if (ptr->canid != nCurCANID && nCurCANID != -1)
			{
				// 기존 설정값들을 전송
				xlEvent.tag                 = XL_TRANSMIT_MSG;
				xlEvent.tagData.msg.dlc     = 8 ;
				xlEvent.tagData.msg.flags   = 0;
				xlEvent.tagData.msg.id      = nCurCANID ;
				
/*
				TRACE("xlEvent.tagData.msg.id      = 0x%02d\n", nCurCANID) ;
				TRACE("xlEvent.tagData.msg.data[0] = 0x%02x\n", xlEvent.tagData.msg.data[0]) ;
				TRACE("xlEvent.tagData.msg.data[1] = 0x%02x\n", xlEvent.tagData.msg.data[1]) ;
				TRACE("xlEvent.tagData.msg.data[2] = 0x%02x\n", xlEvent.tagData.msg.data[2]) ;
				TRACE("xlEvent.tagData.msg.data[3] = 0x%02x\n", xlEvent.tagData.msg.data[3]) ;
				TRACE("xlEvent.tagData.msg.data[4] = 0x%02x\n", xlEvent.tagData.msg.data[4]) ;
				TRACE("xlEvent.tagData.msg.data[5] = 0x%02x\n", xlEvent.tagData.msg.data[5]) ;
				TRACE("xlEvent.tagData.msg.data[6] = 0x%02x\n", xlEvent.tagData.msg.data[6]) ;
				TRACE("xlEvent.tagData.msg.data[7] = 0x%02x\n", xlEvent.tagData.msg.data[7]) ;
*/
				
//				pBroker->SendCANData(xlEvent, pBroker->m_pModbusDlg, hwnd && pBroker->m_pModbusDlg->m_bCanRecvRxTxMsg) ;

				pBroker->SendCANData(xlEvent) ;
			}
			
			nCurCANID = ptr->canid ;
			
			
			ASSERT(ptr->can_bitcounts == 8 || ptr->can_bitcounts == 16) ;
			ASSERT(ptr->can_startbit + ptr->can_bitcounts <= 64) ;
			ASSERT(ptr->mb_bitcounts <= 16) ;
			
			INT nCANIdx = 0 ;	// can id
			INT nMBIdx  = 0 ;	// modbus address
			if (ptr->can_bitcounts == 16)
			{
				nCANIdx = ptr->can_startbit / 8 ;
				
				ASSERT(ptr->mb_bitcounts == 16) ;
				nMBIdx = ptr->addr - 1 ;
				
				xlEvent.tagData.msg.data[nCANIdx]   = (unsigned char) LOBYTE(registers[nMBIdx]) ;
				xlEvent.tagData.msg.data[nCANIdx+1] = (unsigned char) HIBYTE(registers[nMBIdx]) ;
				
				
			}
			else if (ptr->can_bitcounts == 8)
			{
				nCANIdx = ptr->can_startbit / 8 ;
				nMBIdx  = ptr->addr - 1 ;
				
				INT value = 0 ;
				for (int nIdx = ptr->mb_startbit; nIdx < (ptr->mb_startbit + ptr->mb_bitcounts); nIdx++)
				{
					value += (GETBIT(registers[nMBIdx], nIdx) << (nIdx - ptr->mb_startbit)) ;
				}
				
				xlEvent.tagData.msg.data[nCANIdx] = value ;
				
			}
		}

		if (nCurCANID != -1)
		{
			// 기존 설정값들을 전송
			xlEvent.tag                 = XL_TRANSMIT_MSG;
			xlEvent.tagData.msg.dlc     = 8 ;
			xlEvent.tagData.msg.flags   = 0;
			xlEvent.tagData.msg.id      = nCurCANID ;
			
			TRACE("xlEvent.tagData.msg.id      = 0x%02d\n", nCurCANID) ;
			TRACE("xlEvent.tagData.msg.data[0] = 0x%02x\n", xlEvent.tagData.msg.data[0]) ;
			TRACE("xlEvent.tagData.msg.data[1] = 0x%02x\n", xlEvent.tagData.msg.data[1]) ;
			TRACE("xlEvent.tagData.msg.data[2] = 0x%02x\n", xlEvent.tagData.msg.data[2]) ;
			TRACE("xlEvent.tagData.msg.data[3] = 0x%02x\n", xlEvent.tagData.msg.data[3]) ;
			TRACE("xlEvent.tagData.msg.data[4] = 0x%02x\n", xlEvent.tagData.msg.data[4]) ;
			TRACE("xlEvent.tagData.msg.data[5] = 0x%02x\n", xlEvent.tagData.msg.data[5]) ;
			TRACE("xlEvent.tagData.msg.data[6] = 0x%02x\n", xlEvent.tagData.msg.data[6]) ;
			TRACE("xlEvent.tagData.msg.data[7] = 0x%02x\n", xlEvent.tagData.msg.data[7]) ;
			
//			pBroker->SendCANData(xlEvent, pBroker->m_pModbusDlg, hwnd && pBroker->m_pModbusDlg->m_bCanRecvRxTxMsg) ;
			pBroker->SendCANData(xlEvent) ;
		}

//---------------------------------------------
		
	}
	else
	{
		if (hwnd && pBroker->m_pModbusDlg && pBroker->m_pModbusDlg->m_bCanRecvRxTxMsg)
		{
			pBroker->m_pModbusDlg->m_ctlTxList.InsertString(-1, "Error: Not OnBus Status");
		}
	}

	// 3. Clearing
	delete registers ;

}
