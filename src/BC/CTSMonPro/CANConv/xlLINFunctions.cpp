/*----------------------------------------------------------------------------
| File        : LINFunctions.cpp
| Project     : Vector LIN Example - manage the LIN access
|
| Description : 
|-----------------------------------------------------------------------------
| $Author: vismra $    $Locker: $   $Revision: 20478 $
| $Header: /VCANDRV/XLAPI/samples/xlLinExample/xlLINFunctions.cpp 16    14.11.05 10:54 J?g $
|-----------------------------------------------------------------------------
| Copyright (c) 2012 by Vector Informatik GmbH.  All rights reserved.
|---------------------------------------------------------------------------*/

#include "stdafx.h" 
#include "../CTSMonPro.h"
#include "../CTSMonProDoc.h"
#include "xlLINFunctions.h"
#include "debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


// ---------------------------------------------------
// globals
//BOOL      g_bLinThreadRun;
//LINTStruct   g_LinTH;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLINFunctions::CLINFunctions()
{
	m_xlPortHandle = XL_INVALID_PORTHANDLE;
	for (int nIdx = 0; nIdx < MAXPORT; nIdx++)
	{
//		m_xlChannelMask[MASTER] = m_xlChannelMask[SLAVE] = 0;
		m_xlChannelMask[nIdx] = 0 ;
	}

	m_nChannel = -1 ;

//	m_bLinRxThreadRun = FALSE ;
	m_bShowRxCycleDone = FALSE ;
	m_bShowTxCycleDone = FALSE ;
}

CLINFunctions::~CLINFunctions() 
{
}

////////////////////////////////////////////////////////////////////////////

//! LINGetDevice

//! readout the registry to get the hardware information. If there is no 
//! application which is named "xlLINExample" a new one is generated. 
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CLINFunctions::LINGetDevice()
{
	XLstatus         xlStatus = XL_ERROR;
	char            tmp[100];

	xlStatus = xlOpenDriver();
	sprintf(tmp, "xlOpenDriver, stat: %d", xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	if (xlStatus != XL_SUCCESS)
	{
		return xlStatus;
	}

	xlStatus = linGetChannelMask();

	// we need minimum one LIN channel for MASTER/SLAVE config
/*
	if (m_xlChannelMask[MASTER] == 0)
	{
		return XL_ERROR;
	}
*/

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! LINInit

//! LINExample use ONE port for master and slave which is opened. Also a 
//! thread for all incoming messages is created.
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CLINFunctions::LINInit(CLinCanConvBroker* pBroker, int nChannel, CArray<PLINID_INFO, PLINID_INFO>& linid)
{
	XLstatus         xlStatus = XL_ERROR;
	char             tmp[100];

	// 0, 2, 4, 6 채널 중에 하나이다.
	ASSERT(nChannel % 2 == 0) ;

	// ---------------------------------------
	// Open ONE port for both channels master+slave
	// ---------------------------------------

	// calculate the channelMask for both channel 
//	xlChannelMask_both = m_xlChannelMask[MASTER] | m_xlChannelMask[SLAVE];
	m_nChannel = nChannel ;
	XLaccess xlChannelMask_both = m_xlChannelMask[m_nChannel] ;
	XLaccess xlPermissionMask = xlChannelMask_both;

	sprintf(tmp, "CTSMon LIN Master %d", m_nChannel) ;
	xlStatus = xlOpenPort(&m_xlPortHandle, tmp, xlChannelMask_both, &xlPermissionMask, 256, XL_INTERFACE_VERSION, XL_BUS_TYPE_LIN); 

	sprintf(tmp, "xlOpenPort: PortHandle: %d; Permissionmask: 0x%I64x; Status: %d", m_xlPortHandle, xlPermissionMask, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	if (xlStatus != XL_SUCCESS)
	{
		m_pStatusBox->InsertString(-1, "xlOpenPort failed");
		return xlStatus ;
	}

	m_pStatusBox->InsertString(-1, "xlOpenPort done");

	if (m_xlPortHandle == XL_INVALID_PORTHANDLE)
		return XL_ERROR;

	if (xlStatus != XL_SUCCESS)
	{
		m_xlPortHandle = XL_INVALID_PORTHANDLE;
		return xlStatus;
	}

	// ---------------------------------------
	// Create ONE thread for both channels
	// ---------------------------------------

//	m_pDoc = pDoc ;
	m_pBroker = pBroker ;
	
	linCreateRxThread();

	// ---------------------------------------
	// Init each channel (master+slave)
	// ---------------------------------------

	xlStatus = linInitMaster(linid);
	if (xlStatus)
	{
		m_pStatusBox->InsertString(-1,"Init Master failed!");
		return xlStatus;
	}

	for (int nLoop = 0; nLoop < linid.GetSize(); nLoop++)
	{
		LINID_INFO* pInfo = linid.GetAt(nLoop) ;

		int linID = pInfo->id ;

		sprintf(tmp, "Init M/Slave id:0x%02X", linID);
		m_pStatusBox->InsertString(-1,tmp);
	}

/*
	// for the next slave we take the next ID
	linID++;

	// if we have a second channel we setup a LIN slave
	if (m_xlChannelMask[SLAVE])
	{
		xlStatus = linInitSlave(linID);
		if (xlStatus)
		{
			m_pStatusBox->InsertString(-1,"Init Slave failed!");
			return xlStatus;
		}
		sprintf(tmp, "Init Slave id:%d", linID);
		m_pStatusBox->InsertString(-1,tmp);
	}

*/

	return xlStatus;
}

///////////////////////////////////////////////////////////////////////////

//! LINClose()

//! Close the port
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CLINFunctions::LINClose()
{
	// 초기화 안된 상태에서 부른 경우 처리
//	if (m_xlChannelMask[MASTER] )

	ASSERT(m_nChannel != -1) ;

	XLstatus        xlStatus = XL_SUCCESS;
//	XLaccess        xlChannelMask_both = m_xlChannelMask[MASTER] | m_xlChannelMask[SLAVE];
	XLaccess        xlChannelMask_both = m_xlChannelMask[m_nChannel] ;
	char            tmp[100];

//	m_bLinRxThreadRun = FALSE ;
//	g_bLinThreadRun = FALSE;

	// Wait until the thread is done...
	Sleep(1500);

	if(XL_INVALID_PORTHANDLE == m_xlPortHandle)
	{
		return(xlStatus);
	}

	if (xlChannelMask_both)
	{
		xlStatus = xlDeactivateChannel(m_xlPortHandle, xlChannelMask_both);
		sprintf(tmp, "xlDeactivateChannel, status: %d\n",  xlStatus);
		DEBUG(DEBUG_ADV, tmp);
		if (xlStatus)
		{
			return xlStatus;
		}
	}

	xlStatus = xlClosePort(m_xlPortHandle);
	sprintf(tmp, "xlClosePort, status: %d\n",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	if (xlStatus)
	{
		return xlStatus;
	}

	m_xlPortHandle = XL_INVALID_PORTHANDLE;

	// 재시작 하면 죽는 문제가 여기 때문인가???

// 	xlStatus = xlCloseDriver();
// 	sprintf(tmp, "xlCloseDriver, status: %d\n",  xlStatus);
// 	DEBUG(DEBUG_ADV, tmp);
// 	if (xlStatus)
// 	{
// 		return xlStatus;
// 	}

	m_pStatusBox->InsertString(-1, "LIN: Close All");

	return xlStatus;
}

///////////////////////////////////////////////////////////////////////////

//! LINSendMasterReq()

//! Sends a master request to the LIN bus.
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CLINFunctions::LINSendMasterReq(BYTE data, int linID)
{
	XLstatus        xlStatus = XL_ERROR;
	//char            tmp[100];

	ASSERT(m_nChannel != -1) ;

	// send the master request
	xlStatus = xlLinSendRequest(m_xlPortHandle, m_xlChannelMask[m_nChannel], (unsigned char)linID, 0);
//	sprintf(tmp, "SendRq CM: '%I64u', status: %d\n", m_xlChannelMask[m_nChannel], xlStatus);
//	DEBUG(DEBUG_ADV, tmp);

//	m_pStatusBox->InsertString(-1, "Master Request");

	// setup the only slave channel (LIN ID + 1)
//	xlStatus = linSetSlave(linID+1, data);

//	xlStatus = linSetSlave(linID, data);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! linGetChannelMask

//! parse the registry to get the channelmask
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CLINFunctions::linGetChannelMask()
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];

	XLdriverConfig  xlDrvConfig;

	// default values
	unsigned int    hwType;
	unsigned int    hwIndex;
	unsigned int    hwChannel;

	//check for hardware:
	xlStatus = xlGetDriverConfig(&xlDrvConfig);
	if (xlStatus)
	{
		DEBUG(DEBUG_ADV,"Error in xlGetDriverConfig...");
		return xlStatus;
	}

	for (int appChannel = 0; appChannel < xlDrvConfig.channelCount; appChannel++)
	{
		hwType    = xlDrvConfig.channel[appChannel].hwType ;
		hwIndex   = xlDrvConfig.channel[appChannel].hwIndex ;
		hwChannel = xlDrvConfig.channel[appChannel].hwChannel ;

		int channelIndex = xlGetChannelIndex(hwType, hwIndex, hwChannel);

		// check if we have a valid LIN cab/piggy
		if (xlDrvConfig.channel[channelIndex].channelBusCapabilities & XL_BUS_ACTIVE_CAP_LIN)
		{
			DEBUG(DEBUG_ADV,"Found LIN cab/piggy\n");
			// and check the right hardwaretype
			if (xlDrvConfig.channel[channelIndex].hwType == hwType)
			{
				m_xlChannelMask[appChannel] = xlGetChannelMask(hwType, hwIndex, hwChannel);
			}

		}
		else
		{
			m_xlChannelMask[appChannel] = 0 ;
			DEBUG(DEBUG_ADV,"No LIN cab/piggy found\n");
		}

		sprintf(tmp, "Init LIN hWType: %d; hWIndex: %d; hwChannel: %d; channelMask: 0x%I64x for appChannel: %d\n", 
			hwType, hwIndex, hwChannel, m_xlChannelMask[appChannel], appChannel);
		DEBUG(DEBUG_ADV,tmp);

	}

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////
//! linCreateRxThread
//! set the notification and creates the thread.
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CLINFunctions::linCreateRxThread()
{
	XLstatus      xlStatus = XL_ERROR ;
	DWORD         ThreadId=0;
	char          tmp[100];

	ASSERT(m_xlPortHandle != XL_INVALID_PORTHANDLE) ;
	if (m_xlPortHandle != XL_INVALID_PORTHANDLE)
	{

		sprintf(tmp, "Found OpenPort: %d", m_xlPortHandle);
		DEBUG(DEBUG_ADV, tmp);

		// Send a event for each Msg!!!
		xlStatus = xlSetNotification (m_xlPortHandle, &m_hMsgEvent, 1);
		sprintf(tmp, "SetNotification '%d', xlStatus: %d", m_hMsgEvent, xlStatus);
		DEBUG(DEBUG_ADV, tmp);

		// for the RxThread
// 		LINTStruct _LinTH;

		m_LinTH.xlPortHandle = m_xlPortHandle;
		m_LinTH.hMsgEvent    = m_hMsgEvent; 
		m_LinTH.pListRX      = m_pRXBox;
		m_LinTH.pStatusBox   = m_pStatusBox;
//		m_LinTH.pDoc         = m_pDoc ;
		m_LinTH.pLin         = this ;
		m_LinTH.pBroker      = m_pBroker ;

		m_hThread = CreateThread(0, 0x1000, LinRxThread, (LPVOID) &m_LinTH, 0, &ThreadId);
		sprintf(tmp, "CreateThread %d", m_hThread);
		DEBUG(DEBUG_ADV, tmp);

	}

	return xlStatus;
}


////////////////////////////////////////////////////////////////////////////
//! linInitMaster
//! initialize the LIN master, set the master DLC's, opens the 
//! message filter and activate the LIN channel (-> bus on).
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CLINFunctions::linInitMaster(CArray<PLINID_INFO, PLINID_INFO>& linid)
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];

	unsigned char   data[8];
	unsigned char   DLC[64];

	// ---------------------------------------
	// Setup the channel as a MASTER
	// ---------------------------------------

	XLlinStatPar     xlStatPar;

	xlStatPar.LINMode    = XL_LIN_MASTER; 
	xlStatPar.baudrate   = DEFAULT_LIN_BAUDRATE;  // set the baudrate
	xlStatPar.LINVersion = XL_LIN_VERSION_1_3;    // use LIN 1.3

	ASSERT(m_nChannel != -1) ;
	xlStatus = xlLinSetChannelParams(m_xlPortHandle, m_xlChannelMask[m_nChannel], xlStatPar);

	sprintf(tmp, "Init Master PH: '%d', CM: '0x%I64x', status: %d\n", m_xlPortHandle, m_xlChannelMask[m_nChannel], xlStatus );
	DEBUG(DEBUG_ADV, tmp);

	// ---------------------------------------
	// Setup the Master DLC's
	// ---------------------------------------

	// set the DLC for all ID's to DEFAULT_LIN_DLC
	for (int i=0;i<64;i++)
	{
		DLC[i] = DEFAULT_LIN_DLC;
	}

	xlStatus = xlLinSetDLC(m_xlPortHandle, m_xlChannelMask[m_nChannel], DLC);
	sprintf(tmp, "xlLinSetDLC, CM: '0x%I64x', status: %d", m_xlChannelMask[m_nChannel], xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	// ---------------------------------------
	// Setup the channel as a SLAVE also
	// ---------------------------------------

	for (int nLoop = 0; nLoop < linid.GetSize(); nLoop++)
	{
		LINID_INFO* info = linid.GetAt(nLoop) ;

		memset(data, 0, 8);
		
		UCHAR linID = info->id ;
		data[0] = info->data.d0 ;
		data[1] = info->data.d1 ;
		data[2] = info->data.d2 ;
		data[3] = info->data.d3 ;
		data[4] = info->data.d4 ;
		data[5] = info->data.d5 ;
		data[6] = info->data.d6 ;
		data[7] = info->data.d7 ;

		xlStatus = xlLinSetSlave(m_xlPortHandle, m_xlChannelMask[m_nChannel], (unsigned char)linID, data, DEFAULT_LIN_DLC, XL_LIN_CALC_CHECKSUM_ENHANCED);
		sprintf(tmp, "Set Slave id:%d, CM: '0x%I64x', status: %d\n", linID, m_xlChannelMask[m_nChannel], xlStatus);
		DEBUG(DEBUG_ADV, tmp);
	}

	// ---------------------------------------
	// Activate the Master Channel
	// ---------------------------------------

	xlStatus = xlActivateChannel(m_xlPortHandle, m_xlChannelMask[m_nChannel], XL_BUS_TYPE_LIN, XL_ACTIVATE_RESET_CLOCK);
	sprintf(tmp, "Activate Channel, CM: '0x%I64x', status: %d\n", m_xlChannelMask[m_nChannel], xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	if (xlStatus != XL_SUCCESS) return xlStatus;

	xlStatus = xlFlushReceiveQueue(m_xlPortHandle);
	sprintf(tmp, "FlushReceiveQueue stat: %d\n", xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}


////////////////////////////////////////////////////////////////////////////
//! linInitSlave
//! initialize the LIN slave, define the slave (id, dlc, data), opens the 
//! message filter and activate the LIN channel (-> bus on).
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CLINFunctions::linInitSlave(int linID, int nChannel)
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];

	unsigned char   data[8];
	unsigned char   DLC[64];

	// ---------------------------------------
	// Setup the channel as a SLAVE
	// ---------------------------------------

	XLlinStatPar     xlStatPar;

	xlStatPar.LINMode    = XL_LIN_SLAVE;
	xlStatPar.baudrate   = DEFAULT_LIN_BAUDRATE; // set the baudrate
	xlStatPar.LINVersion = XL_LIN_VERSION_1_3;   // use LIN 1.3

	xlStatus = xlLinSetChannelParams(m_xlPortHandle, m_xlChannelMask[nChannel], xlStatPar);

	sprintf(tmp, "Init Slave PH: '%d', CM: '0x%I64x', status: %d\n", m_xlPortHandle, m_xlChannelMask[nChannel], xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	// ---------------------------------------
	// Setup the Slave DLC's
	// ---------------------------------------

	// set the DLC for all ID's to DEFAULT_LIN_DLC
	for (int i=0;i<64;i++)
	{
		DLC[i] = DEFAULT_LIN_DLC;
	}

	xlStatus = xlLinSetDLC(m_xlPortHandle, m_xlChannelMask[nChannel], DLC);
	sprintf(tmp, "xlLinSetDLC, CM: '0x%I64x', status: %d", m_xlChannelMask[nChannel], xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	// ---------------------------------------
	// Setup the SLAVE 
	// ---------------------------------------

	memset(data, 0, 8);

	xlStatus = xlLinSetSlave(m_xlPortHandle, m_xlChannelMask[nChannel], (unsigned char)linID, data, DEFAULT_LIN_DLC, XL_LIN_CALC_CHECKSUM);
	sprintf(tmp, "Set Slave id:%d, CM: '0x%I64x', status: %d\n", linID, m_xlChannelMask[nChannel], xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	// ---------------------------------------
	// Activate the Slave Channel
	// ---------------------------------------

	xlStatus = xlActivateChannel(m_xlPortHandle, m_xlChannelMask[nChannel], XL_BUS_TYPE_LIN, XL_ACTIVATE_RESET_CLOCK);
	sprintf(tmp, "Activate Channel CM: '0x%I64x', status: %d\n", m_xlChannelMask[nChannel], xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	if (xlStatus != XL_SUCCESS) return xlStatus;

	xlStatus = xlFlushReceiveQueue(m_xlPortHandle);
	sprintf(tmp, "FlushReceiveQueue stat: %d\n", xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;

}

////////////////////////////////////////////////////////////////////////////
//! linSetSlave
//! change the slave
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CLINFunctions::linSetSlave(int linID, byte databyte, int MASTERORSLAVE)
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];

	unsigned char            data[8];

	data[0] = databyte;
	data[1] = 0x0F;
	data[2] = 0x00;
	data[3] = 0x00;
	data[4] = 0x00;
	data[5] = 0x00;
	data[6] = 0xFF;
	data[7] = 0x00;

//	xlStatus = xlLinSetSlave(m_xlPortHandle, m_xlChannelMask[SLAVE], (unsigned char)linID, data, DEFAULT_LIN_DLC, XL_LIN_CALC_CHECKSUM);
	xlStatus = xlLinSetSlave(m_xlPortHandle, m_xlChannelMask[MASTERORSLAVE], (unsigned char)linID, data, DEFAULT_LIN_DLC, XL_LIN_CALC_CHECKSUM);
	sprintf(tmp, "Set Slave ID CM: '0x%I64x', status: %d", m_xlChannelMask[MASTERORSLAVE], xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

///////////////////////////////////////////////////////////////////////////

//! LinRxThread

//! thread to readout the message queue and parse the incoming messages
//!
////////////////////////////////////////////////////////////////////////////

DWORD WINAPI LinRxThread(LPVOID par) 
{

	XLstatus        xlStatus;

	//char            tmp[100];
	unsigned int    msgsrx = RECEIVE_EVENT_SIZE;
	XLevent         xlEvent; 
	char            tmp[100];
	CString         str;

//	g_bLinThreadRun = TRUE;

	// 글로벌 변수를 굳이 파라미터로 넘길 필요가 있나???
	LINTStruct* pst_LIN = (LINTStruct*) par;
//	memcpy(&st_LIN, par, sizeof(LINTStruct)) ;  

	sprintf(tmp, "thread: SetNotification '%d'", pst_LIN->hMsgEvent);
	DEBUG(DEBUG_ADV, tmp);

	CLINFunctions* pLin = pst_LIN->pLin ;
	CLinCanConvBroker* pBroker = pLin->GetConvBroker() ;

	pBroker->m_bLinRxThreadRun = TRUE ;

	while (pBroker->m_bLinRxThreadRun)
	{

		WaitForSingleObject(pst_LIN->hMsgEvent, 10);
// 		WaitForSingleObject(st_LIN->hMsgEvent, INFINITE);

		xlStatus = XL_SUCCESS;

		while (!xlStatus)
		{
			msgsrx = RECEIVE_EVENT_SIZE;

			if (!pBroker->m_bLinRxThreadRun)
			{
				break ;
			}

			xlStatus = xlReceive(pLin->GetPortHandle(), &msgsrx, &xlEvent);

			if ( xlStatus != XL_ERR_QUEUE_IS_EMPTY )
			{

				//sprintf(tmp, sizeof(tmp), "thread: ReceiveEx tag: '%d'", vEvent2.tag);
				//DEBUG(DEBUG_ADV, tmp);

				switch (xlEvent.tag)
				{

					// CAN events
				case XL_SYNC_PULSE:
					sprintf(tmp, "SYNC_PULSE: on Ch: '%d'", xlEvent.chanIndex);
					DEBUG(DEBUG_ADV, tmp); 
//					pst_LIN->pListRX->InsertString(-1,tmp);
					break;

				case XL_TRANSCEIVER:
					sprintf(tmp, "TRANSCEIVER: on Ch: '%d'", xlEvent.chanIndex);
					DEBUG(DEBUG_ADV, tmp); 
//					pst_LIN->pListRX->InsertString(-1,tmp);
					break;

					// LIN events

				case XL_LIN_NOANS:
					sprintf(tmp, "LIN NOANS ID: '0x%x' on Ch: '%d', time: %I64u", xlEvent.tagData.linMsgApi.linNoAns.id, xlEvent.chanIndex, xlEvent.timeStamp);
					DEBUG(DEBUG_ADV, tmp); 
//					pst_LIN->pListRX->InsertString(-1,tmp);
					break;

				case XL_LIN_MSG:
					{
						CString         str1, sData;
						str = "RX: ";
						if (xlEvent.tagData.linMsgApi.linMsg.flags & XL_LIN_MSGFLAG_TX)
						{
							str = "TX: ";
						}

						str1 = "";
						for (int i = 0; i < xlEvent.tagData.linMsgApi.linMsg.dlc; i++)
						{
							str1.Format(_T("%02x"), xlEvent.tagData.linMsgApi.linMsg.data[i]);
							sData = sData + str1;
						}

						sprintf(tmp, "ID: 0x%02x, dlc: '%d', Data: 0x%s, time: %I64u, Ch: '%d'", xlEvent.tagData.linMsgApi.linMsg.id, xlEvent.tagData.linMsgApi.linMsg.dlc, sData, xlEvent.timeStamp, xlEvent.chanIndex);
						DEBUG(DEBUG_ADV, tmp); 

//						EnterCriticalSection(pst_LIN->pBroker->m_pcsShowMessage) ;
						pBroker->ProcessLinData(xlEvent) ;
//						LeaveCriticalSection(pst_LIN->pBroker->m_pcsShowMessage) ;

						////////////////////////////////////////////////////////////////
						pBroker->SendCANData(xlEvent) ;
						////////////////////////////////////////////////////////////////

						break;
					}

				case XL_LIN_SLEEP:
					sprintf(tmp
							, "LIN SLEEP flag: 0x%x, time: %I64u, Ch: '%d'"
							, xlEvent.tagData.linMsgApi.linSleep.flag
							, xlEvent.timeStamp
							, xlEvent.chanIndex);
					DEBUG(DEBUG_ADV, tmp); 
//					pst_LIN->pListRX->InsertString(-1,tmp);
					break;

				case XL_LIN_ERRMSG:
					sprintf(tmp, "LIN ERROR, Ch: '%d'", xlEvent.chanIndex);
					DEBUG(DEBUG_ADV, tmp); 
//					pst_LIN->pListRX->InsertString(-1,tmp);
					break;

				case XL_LIN_SYNCERR:
					sprintf(tmp, "LIN SYNCERR on Ch: '%d'", xlEvent.chanIndex);
					DEBUG(DEBUG_ADV, tmp); 
//					pst_LIN->pListRX->InsertString(-1,tmp);
					break;

				case XL_LIN_WAKEUP:
					sprintf(tmp, "LIN WAKEUP flags: 0x%x on Ch: '%d'", xlEvent.tagData.linMsgApi.linWakeUp.flag, xlEvent.chanIndex);
					DEBUG(DEBUG_ADV, tmp); 
//					pst_LIN->pListRX->InsertString(-1,tmp);
					break;
				}

				ResetEvent(pst_LIN->hMsgEvent);

				//int nCount = pmyListBox->GetCount();
				//if (nCount > 0)
/*
				pst_LIN->pListRX->SetCurSel(pst_LIN->pListRX->GetCount()-1);
				pst_LIN->pStatusBox->SetCurSel(pst_LIN->pStatusBox->GetCount()-1);
*/
			}

//			if (pst_LIN->pBroker->m_bShowMessage)
			{
				pLin->m_bShowRxCycleDone = TRUE ;
				Sleep(0) ;
			}
		}

	}

	SetEvent(pst_LIN->pBroker->m_hLinRxThreadTerminated) ;

	TRACE("Exiting LinRxThread loop\n") ;

	return NO_ERROR; 
}


//----------------------------------------------

