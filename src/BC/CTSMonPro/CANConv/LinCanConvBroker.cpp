// LinCanConvBroker.cpp: implementation of the CLinCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "../CTSMonPro.h"

#include "../CyclerModule.h"
#include "../CyclerChannel.h"
#include "LinCanConvBroker.h"
#include "../CTSMonProDoc.h"
#include "xlLINFunctions.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//BOOL g_bCanRxThreadRun[4] ;
//extern BOOL    g_bCANThreadRun;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLinCanConvBroker::CLinCanConvBroker()
{
	// 변수 초기화
//	m_bCanRxThreadRun = FALSE ;
	m_bSendCANData = FALSE ;
	m_nLinMode = LCC_STOP ;

	m_bLinRxThreadRun = FALSE ;
	m_bCanRxThreadRun = FALSE ;
//	m_bShowMessage = FALSE ;
	m_pRxList = NULL ;
	m_pTxList = NULL ;

	m_pLIN = NULL ;
	m_pCAN = NULL ;

	m_bSaveRxFlag = FALSE ;
	m_bSaveTxFlag = FALSE ;

	// 초기화를 해주는 방법 고민 필요
	// 예. Module 에는 반드시 두개의 채널이 있어야한다?
	m_nDevID = -1 ;
//	m_nLinID = -1 ;

	// Lin 스케쥴러 종료 이벤트
	m_hLinSchedulerTerminated = INVALID_HANDLE_VALUE ;
	m_hLinRxThreadTerminated  = INVALID_HANDLE_VALUE ;
//	m_hCanRxThreadTerminated  = INVALID_HANDLE_VALUE ;

}

CLinCanConvBroker::~CLinCanConvBroker()
{
	if (m_pLIN)
	{
		delete m_pLIN ;
	}

	if (m_pCAN)
	{
		delete m_pCAN ;
	}
}

// Added by 224 (2014/10/09) : LIN-CAN Convert 쓰레드 구동
BOOL CLinCanConvBroker::BeginLinCanConvThread(int nDevID
											, CString strSettingName
//											, INT nLinID
											, INT nLinChannel
											, INT nSpeed
											, CArray<PLINID_INFO, PLINID_INFO>& linid
											, CArray<PLCC_INFO, PLCC_INFO>& lcc
											, CArray<PSCHEDULER_INFO, PSCHEDULER_INFO>& sched
											, INT nCanChannel
											, INT nCanBaudrate)
{
//	CString strTemp,strCode;
// 	int		nLength;
// 	int i;

	// Pause 상태에서 재시작 하는 경우 처리
	if (m_nLinMode == LCC_PAUSE)
	{
		m_nLinMode = LCC_PLAY ;
		return TRUE ;
	}
	else if (m_nLinMode == LCC_PLAY)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}

// 	// GUI에 로그 파일 작성하기 위한 모듈
// 	HWND hwnd = FindWindow(NULL, "Lin Config") ;

	//------------------------ SBC init ------------------------

/*
	CCyclerModule* pMD;
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if (pMD == NULL)
	{
		return FALSE ;
	}

	CCyclerChannel* pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}
*/
	//------------------------ SBC init ------------------------

	//----------------- converting array 설정 ----------------- 
	

	m_nDevID = nDevID ;
//	m_nLinID = nLinID ;

	//------------------------ LIN init ------------------------

	ASSERT(m_pLIN == NULL) ;
	{
		CString str ;
		
		if (m_pLIN == NULL)
		{
			m_pLIN = new CLINFunctions ;
		}
		
		m_pLIN->m_pRXBox     = m_pRxList ;
		m_pLIN->m_pStatusBox = m_pRxList ;
		XLstatus xlStatus = m_pLIN->LINGetDevice() ;
		
		if (xlStatus != XL_SUCCESS)
		{
			TRACE("Unable to allocate libmodbus context\n");
			
			ASSERT(m_bShowMessage == TRUE && m_pRxList != NULL) ;
			if (m_bShowMessage)
			{
				m_pRxList->AddString("ERROR: You need LINCabs or LINPiggy's!") ;
				m_pRxList->SetCurSel(m_pRxList->GetCount()-1);  
			}
			
			ASSERT(FALSE) ;
			return FALSE ;
		}

//		ASSERT(m_nLinID != -1) ;
//		xlStatus = m_pLIN->LINInit(this, m_nDevID * 2, m_nLinID);
		xlStatus = m_pLIN->LINInit(this, m_nDevID * 2, linid);
		
		if (xlStatus == XL_SUCCESS)
		{
			// Do nothing !!! fall down...
			
		}
		else if (xlStatus == XL_ERR_INVALID_ACCESS)
		{
			AfxMessageBox("ERROR: You need INIT access!", MB_ICONSTOP);
			
			xlStatus = m_pLIN->LINClose();
			delete m_pLIN ;
			m_pLIN = NULL ;

			// 4. converting array clearing
			INT nLoop = 0 ;

			for(int nLoop = 0; nLoop < linid.GetSize(); nLoop++)
			{
				LINID_INFO* ptr = linid.GetAt(nLoop) ;
				delete ptr ;
			}
			linid.RemoveAll() ;
			

			for(int nLoop = 0; nLoop < lcc.GetSize(); nLoop++)
			{
				LCC_INFO* ptr = lcc.GetAt(nLoop) ;
				delete ptr ;
			}
			lcc.RemoveAll() ;
			
			for(int nLoop = 0; nLoop < sched.GetSize(); nLoop++)
			{
				SCHEDULER_INFO* ptr = sched.GetAt(nLoop) ;
				delete ptr ;
			}
			sched.RemoveAll() ;

			return FALSE ;
		}
		else
		{
			AfxMessageBox("ERROR in LIN INIT !!!", MB_ICONSTOP);
			xlStatus = m_pLIN->LINClose();
			if (xlStatus)
			{
				AfxMessageBox("ERROR in closing port !", MB_ICONSTOP);
			}

			// 4. converting array clearing
			INT nLoop = 0 ;
			
			for(int nLoop = 0; nLoop < linid.GetSize(); nLoop++)
			{
				LINID_INFO* ptr = linid.GetAt(nLoop) ;
				delete ptr ;
			}
			linid.RemoveAll() ;
			
			
			for(int nLoop = 0; nLoop < lcc.GetSize(); nLoop++)
			{
				LCC_INFO* ptr = lcc.GetAt(nLoop) ;
				delete ptr ;
			}
			lcc.RemoveAll() ;

			for(int nLoop = 0; nLoop < sched.GetSize(); nLoop++)
			{
				SCHEDULER_INFO* ptr = sched.GetAt(nLoop) ;
				delete ptr ;
			}
			sched.RemoveAll() ;

			return FALSE ;
		}	
	}

	//------------------------ can init ------------------------
	ASSERT(m_pCAN == NULL) ;
	{
		m_pCAN = new CCANFunctions ;
		
		m_pCAN->m_pOutput = m_pRxList ;

		XLstatus xlStatus = m_pCAN->CANGetDevice() ;
		
		if (xlStatus != XL_SUCCESS)
		{
			TRACE("Unable to allocate libmodbus context\n");
			
			ASSERT(m_bShowMessage == TRUE && m_pRxList != NULL) ;
			if (m_bShowMessage)
			{
				m_pRxList->AddString("ERROR: You need CANcabs or CANPiggy's!") ;
				m_pRxList->SetCurSel(m_pRxList->GetCount()-1);  
			}
			
			ASSERT(FALSE) ;
			return FALSE ;
		}
		
		// init the CAN hardware
		if (m_pCAN->CANInit(this, m_nDevID * 2 + 1))
		{
			ASSERT(FALSE) ;
			// 		m_ctlCanHardware.ResetContent();
			// 		m_ctlCanHardware.AddString("<ERROR> no HW!");
			// 		m_ctlTxList.AddString("You need two CANcabs/CANpiggy's");
			// 		m_btnOnBus.EnableWindow(FALSE);
			
			ASSERT(m_bShowMessage == TRUE && m_pTxList != NULL) ;
			if (m_bShowMessage)
			{
				m_pTxList->AddString("You need two CANcabs/CANpiggy's") ;
				m_pTxList->SetCurSel(m_pTxList->GetCount()-1);  
			}
			
			delete m_pCAN ;
			m_pCAN = NULL ;
			
			return FALSE ;
		}
		
		// go on bus
		//	int           nIndex=0;
		//	unsigned long bitrate[5] = {125000, 250000, 500000, 1000000};
		
		xlStatus = m_pCAN->CANGoOnBus(nCanBaudrate);
		if (!xlStatus)
		{
			m_bSendCANData = TRUE ;
		}
		else
		{
			m_pRxList->AddString("ERROR: CANGoOnBus() !") ;

			ASSERT(FALSE) ;
			m_bSendCANData = FALSE ;
			
			return FALSE ;
		}
	}

	//------------------------ can init ------------------------

	//----------------- converting array 설정 ----------------- 
	//
	// 	m_lcc_array.RemoveAll() ;
	for (INT nLoop = 0; nLoop < linid.GetSize(); nLoop++)
	{
		LINID_INFO* info = linid.GetAt(nLoop) ;
		
		m_linid_array.Add(info) ;
	}


	ASSERT(m_lcc_array.GetSize() == 0) ;
	for(int nLoop = 0; nLoop < lcc.GetSize(); nLoop++)
	{
		LCC_INFO* info = lcc.GetAt(nLoop) ;
		
		m_lcc_array.Add(info) ;
	}
	
	ASSERT(m_schedule_array.GetSize() == 0) ;
	for(int nLoop = 0; nLoop < sched.GetSize(); nLoop++)
	{
		SCHEDULER_INFO* info = sched.GetAt(nLoop) ;
		
		m_schedule_array.Add(info) ;
	}

	//------------------------ LIN 스케쥴러 init -------------------------

	m_bLinOnBus = TRUE ;
	linCreateSchedulerThread() ;

	m_nLinMode = LCC_PLAY ;
	m_strLinSettingName = strSettingName ;


	// 아래의 CreateEvent() 에서 원인모를 오류로 인하여
	// Doc에서 Handle 을 생성하여 전달하도록 코드 수정함
	if (m_hLinSchedulerTerminated == INVALID_HANDLE_VALUE)
	{
		m_hLinSchedulerTerminated = CreateEvent(NULL, FALSE, TRUE, NULL) ;
//		DWORD dwErr = GetLastError() ;
	}

	if (m_hLinRxThreadTerminated== INVALID_HANDLE_VALUE)
	{
		m_hLinRxThreadTerminated = CreateEvent(NULL, FALSE, TRUE, NULL) ;
//		DWORD dwErr = GetLastError() ;
	}

	if (m_hCanRxThreadTerminated == INVALID_HANDLE_VALUE)
	{
		m_hCanRxThreadTerminated = CreateEvent(NULL, FALSE, TRUE, NULL) ;
//		DWORD dwErr = GetLastError() ;
	}

	return TRUE ;
}


void CLinCanConvBroker::StopLinCanConvThread()
{
	m_nLinMode = LCC_STOP ;

	m_bShowMessage = FALSE ;

	if (m_pLIN == NULL)
	{
		ASSERT (m_pCAN == NULL) ;
		
		// 이미 초기화 되어진 상태임.
		return ;
	}

	// 1. Lin 스케쥴러 종료 flag 설정.
	{
		m_bLinOnBus = FALSE ;
		
		// wait untill thread exiting
		//	Sleep(500) ;
		DWORD dw = WaitForSingleObject(m_hLinSchedulerTerminated, INFINITE) ;
		TRACE("Signaled m_hLinSchedulerTerminated\n") ;
		ASSERT(dw == WAIT_OBJECT_0) ;
	}

	// 2. LIN 해제	
	if (m_pLIN)
	{
		m_bLinRxThreadRun = FALSE ;
		
		// wait untill thread exiting
		DWORD dw = WaitForSingleObject(m_hLinRxThreadTerminated, INFINITE) ;
		TRACE("Signaled m_hLinRxThreadTerminated\n") ;
		ASSERT(dw == WAIT_OBJECT_0) ;
		
	}
	
	if (m_pCAN)
	{
		m_bCanRxThreadRun = FALSE ;

		//		g_bCanRxThreadRun[m_nDevID] = FALSE ;
		
		//		m_pCAN->CANGoOffBus() ;
		//		DWORD dw = WaitForSingleObject(m_hCanRxTerminated, 3000) ;
		
		// wait untill thread exiting
		DWORD dw = WaitForSingleObject(m_hCanRxThreadTerminated, INFINITE) ;
		TRACE("Signaled m_hCanRxThreadTerminated\n") ;
		ASSERT(dw == WAIT_OBJECT_0) ;
		
	}
	
	// 3. CAN 해제
	// 3.1. CAN Rx 쓰레드 종료
			
	if (m_pCAN)
	{
		m_pCAN->CANClose() ;
		
		delete m_pCAN ;
		m_pCAN = NULL ;
	}

	if (m_pLIN)
	{
		m_pLIN->LINClose() ;
		
		delete m_pLIN ;
		m_pLIN = NULL ;
	}

	// 4. converting array clearing
	INT nLoop ;
	for(int nLoop = 0; nLoop < m_linid_array.GetSize(); nLoop++)
	{
		LINID_INFO* ptr = m_linid_array.GetAt(nLoop) ;
		delete ptr ;
	}
	m_linid_array.RemoveAll() ;

	for(int nLoop = 0; nLoop < m_lcc_array.GetSize(); nLoop++)
	{
		LCC_INFO* ptr = m_lcc_array.GetAt(nLoop) ;
		delete ptr ;
	}
	m_lcc_array.RemoveAll() ;

	for(int nLoop = 0; nLoop < m_schedule_array.GetSize(); nLoop++)
	{
		SCHEDULER_INFO* ptr = m_schedule_array.GetAt(nLoop) ;
		delete ptr ;
	}
	m_schedule_array.RemoveAll() ;
	
	m_strLinSettingName.Empty() ;	
}

void CLinCanConvBroker::DoNotShowMessage()
{
	// Commented by 224 (2104/10/15) :
	// Lin dialog 창을 닫을 때
	// 세개의 쓰레드에서 dialog의 컨트롤에 메시지 전시하는
	// 로직이 동기화가 되지 않으면, NULL 포인트 접근을 하여
	// 프로그램의 오류가 발생한다.

	// 현재 visual 이 활성화된 브로커에게만 호출한다.
	ASSERT(m_bShowMessage == TRUE) ;
	// Can Rx, Lin Rx, Lin Scheduler 쓰레드의 메시지 전시가 종료된 후에 리턴한다.

	m_bShowMessage = FALSE ;

	// CAN Rx 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pCAN != NULL) ;
	// 생성 실패시 나타날 수 있음
	// Can Rx 는 메시지 이벤트 발생시에만 작동하므로 기다릴 필요가 없다.
/*
	if (m_pCAN)
	{
		m_pCAN->m_bShowCycleDone = FALSE ;
		Sleep(0) ;
		
		while (!m_pCAN->m_bShowCycleDone)
		{
			Sleep(1) ;
		}
		
	}
*/
	TRACE("CAN Rx 메시지 종료 확인\n") ;

	// Lin Rx 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pLIN != NULL) ;
	if (m_pLIN)
	{
		m_pLIN->m_bShowRxCycleDone = FALSE ;
		Sleep(0) ;
		
// 		while (!m_pLIN->m_bShowRxCycleDone)
// 		{
// 			Sleep(1) ;
// 		}
	}

	TRACE("Lin Rx 메시지 종료 확인\n") ;

	// Lin TX 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pLIN != NULL) ;
	if (m_pLIN)
	{
		m_pLIN->m_bShowTxCycleDone = FALSE ;
		Sleep(0) ;
		
// 		while (!m_pLIN->m_bShowTxCycleDone)
// 		{
// 			Sleep(1) ;
// 		}
	}

	TRACE("Lin Tx 메시지 종료 확인\n") ;

	return ;
}

void CLinCanConvBroker::ProcessLinData(XLevent &xlLinEvent)
{
	char tmp[100];
	CString sData ;
	for (int i = 0; i < xlLinEvent.tagData.linMsgApi.linMsg.dlc; i++)
	{
		CString str1 ;
		str1.Format(_T("%02x"), xlLinEvent.tagData.linMsgApi.linMsg.data[i]);
		sData = sData + str1;
	}
	
	sprintf(tmp
		, "ID: 0x%02x, dlc: '%d', Data: 0x%s, time: %I64u, Ch: '%d'"
		, xlLinEvent.tagData.linMsgApi.linMsg.id
		, xlLinEvent.tagData.linMsgApi.linMsg.dlc
		, sData
		, xlLinEvent.timeStamp
		, xlLinEvent.chanIndex);

//	if (hwnd && m_pLinDlg->m_bCanRecvRxTxMsg)
//	EnterCriticalSection(m_pcsShowMessage) ;

//	if (m_bShowMessage)
	if (FALSE)
	{

		ASSERT(m_pRxList != NULL) ;

		int cnt = m_pRxList->GetCount() ;
		while (cnt > 99)
		{
			if (!m_pRxList)
			{
				break ;
			}

			m_pRxList->DeleteString(0) ;

			if (!m_pRxList)
			{
				break ;
			}
			cnt = m_pRxList->GetCount() ;
		}
		
		if (m_pRxList)
		{
			m_pRxList->AddString(tmp) ;
		}

		if (m_pRxList)
		{
			m_pRxList->SetCurSel(m_pRxList->GetCount()-1);
		}		
	}
//	LeaveCriticalSection(m_pcsShowMessage) ;

}

//void CLinCanConvBroker::SendCANData(XLevent &xlEvent)
XLstatus CLinCanConvBroker::SendCANData(XLevent &xlLinEvent)
{
	//----------------------------------------------------------
	// CAN 으로 전송
	
	XLevent xlEvent;	
	memset(&xlEvent, 0, sizeof(xlEvent));
	
	// 전송
	xlEvent.tag                 = XL_TRANSMIT_MSG;
	xlEvent.tagData.msg.dlc     = xlLinEvent.tagData.linMsgApi.linMsg.dlc ;
	xlEvent.tagData.msg.flags   = 0;
	xlEvent.tagData.msg.id      = xlLinEvent.tagData.linMsgApi.linMsg.id ;
	
	for (int nIdx = 0; nIdx < xlEvent.tagData.msg.dlc; nIdx++)
	{
		xlEvent.tagData.msg.data[nIdx] = xlLinEvent.tagData.linMsgApi.linMsg.data[nIdx] ;
	}
	
/*
	TRACE("xlEvent.tagData.msg.id      = 0x%02x\n", xlEvent.tagData.msg.id) ;
	TRACE("xlEvent.tagData.msg.data[0] = 0x%02x\n", xlEvent.tagData.msg.data[0]) ;
	TRACE("xlEvent.tagData.msg.data[1] = 0x%02x\n", xlEvent.tagData.msg.data[1]) ;
	TRACE("xlEvent.tagData.msg.data[2] = 0x%02x\n", xlEvent.tagData.msg.data[2]) ;
	TRACE("xlEvent.tagData.msg.data[3] = 0x%02x\n", xlEvent.tagData.msg.data[3]) ;
	TRACE("xlEvent.tagData.msg.data[4] = 0x%02x\n", xlEvent.tagData.msg.data[4]) ;
	TRACE("xlEvent.tagData.msg.data[5] = 0x%02x\n", xlEvent.tagData.msg.data[5]) ;
	TRACE("xlEvent.tagData.msg.data[6] = 0x%02x\n", xlEvent.tagData.msg.data[6]) ;
	TRACE("xlEvent.tagData.msg.data[7] = 0x%02x\n", xlEvent.tagData.msg.data[7]) ;
*/


	ASSERT(m_bSendCANData) ;
	XLstatus  xlStatus = XL_SUCCESS ;

	if (m_pCAN != NULL)
	{
		xlStatus = m_pCAN->CANSend(xlEvent);
	}	

	TRACE("------------------------------------ CAN SEND ------------------------------------\n") ;
	return xlStatus ;

//	EnterCriticalSection(m_pcsShowMessage) ;
	if (m_bShowMessage)
	{
		ASSERT(m_pTxList != NULL) ;

		CString strCont ;
		strCont.Format("ID:%02d ", xlEvent.tagData.msg.id) ;
		for (int nIdx = 0; nIdx < xlEvent.tagData.msg.dlc; nIdx++)
		{
			CString str ;
			str.Format("[0x%02X] ", xlEvent.tagData.msg.data[nIdx]) ;
			
			strCont += str ;
		}
		
		// 로그 파일에 저장
		if (m_bSaveTxFlag)
		{
			CHAR buf[1024] ;
			
			strcpy(buf, strCont) ;
			strcat(buf, "\n") ;
			
			CTime ctCur = CTime::GetCurrentTime();
			CString csFilename;
			csFilename.Format(_T("tx_%04d%02d%02d.log"), ctCur.GetYear(), ctCur.GetMonth(), ctCur.GetDay());
			
			FILE* fp = fopen(csFilename, "a+") ;
			//			FILE* fp = fopen(".\\tx.log", "a");
			fwrite(buf, strlen(buf), 1, fp) ;
			fclose(fp) ;
		}	

		// commented by 224 (2014/10/09) :

		int cnt = m_pTxList->GetCount() ;
		while (cnt > 99)
		{
			if (m_pTxList == NULL)
				break ;

			m_pTxList->DeleteString(0) ;

			if (m_pTxList == NULL)
				break ;

			cnt = m_pTxList->GetCount() ;
		}
		
		if (m_pTxList)
		{
			m_pTxList->InsertString(-1, strCont) ;
		}

		if (m_pTxList)
		{
			m_pTxList->SetCurSel(m_pTxList->GetCount()-1);  
		}

	}

	m_pLIN->m_bShowTxCycleDone = TRUE ;

//	LeaveCriticalSection(m_pcsShowMessage) ;
	
	return xlStatus ;
	
}

void CLinCanConvBroker::linCreateSchedulerThread()
{
//	XLstatus      xlStatus;
	DWORD         ThreadId=0;
	char          tmp[100];
	
	if (m_pLIN->IsValid())
	{
		
		HANDLE hThread ;
		hThread = CreateThread(0, 0x1000, LinSchedulerThread, (LPVOID) this, 0, &ThreadId);
		sprintf(tmp, "CreateThread %d", hThread);
		//		DEBUG(DEBUG_ADV, tmp);
		
	}
	
}

#define CHECKCONTINUOUS(delay) \
	if ((pBroker->m_bLinOnBus))\
{\
	Sleep(delay) ;\
}\
	else \
{\
	break ;\
}

DWORD WINAPI LinSchedulerThread(PVOID param)
{
	CLinCanConvBroker* pBroker = (CLinCanConvBroker*)param ;
	CLINFunctions* pLin = pBroker->m_pLIN ; 
	//	CLINFunctions* pLin = &pDoc->m_LIN ; 
	

	if (pBroker->m_schedule_array.GetSize() > 0)
	{
		while (pBroker->m_bLinOnBus)
		{
			Sleep(0) ;
			
			for (INT nLoop = 0; nLoop < pBroker->m_schedule_array.GetSize(); nLoop++)
			{
				SCHEDULER_INFO* pInfo = pBroker->m_schedule_array.GetAt(nLoop) ;
				
				pLin->LINSendMasterReq(0, pInfo->id) ;
				CHECKCONTINUOUS(pInfo->delay) ;
				
			}
				
		}

		
/*
		pLin->LINSendMasterReq(0, 0x2b) ;	// module state 1, 43
		CHECKCONTINUOUS(10) ;
		
		pLin->LINSendMasterReq(0, 0x2c) ;	// module state 2, 44
		CHECKCONTINUOUS(10) ;
		
		pLin->LINSendMasterReq(0, 0x2d) ;	// module state 3, 45
		CHECKCONTINUOUS(20) ;
		
		pLin->LINSendMasterReq(0, 0x2e) ;	// module state 4, 46
		CHECKCONTINUOUS(20) ;
		
		pLin->LINSendMasterReq(0, 0x2f) ;	// module state 5, 47
		CHECKCONTINUOUS(20) ;
		
		pLin->LINSendMasterReq(0, 0x30) ;	// module state 6, 48
		CHECKCONTINUOUS(20) ;
		
		pLin->LINSendMasterReq(0, 0x33) ;	// BMS_control, 51
		CHECKCONTINUOUS(10) ;
*/
	}
	
	SetEvent(pBroker->m_hLinSchedulerTerminated) ;
	
	TRACE("Exiting SchedulerThread()\n") ;
	
	return 0 ;
}
