#if !defined(AFX_LINSETTINGDBDIALOG_H__B031B82F_EEB7_4FE7_9544_D2EEE84B31A4__INCLUDED_)
#define AFX_LINSETTINGDBDIALOG_H__B031B82F_EEB7_4FE7_9544_D2EEE84B31A4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModbusSettingDBDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLinSettingDBDialog dialog

class CLinSettingDBDialog : public CDialog
{
// Construction
public:

	CLinSettingDBDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLinSettingDBDialog)
	enum { IDD = IDD_LIN_DB_DIALOG , IDD2 = IDD_LIN_DB_DIALOG_ENG , IDD3 = IDD_LIN_DB_DIALOG_PL };
	CXPButton	m_btnDelRecord;
	CListCtrl	m_list;
	CString	m_strName;
	CString	m_strDesc;
	//}}AFX_DATA

	CString m_strConvDBFilename ;
	BOOL m_bSaveMode ;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLinSettingDBDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLinSettingDBDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnClickDbList(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	afx_msg void OnDelRecord();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINSETTINGDBDIALOG_H__B031B82F_EEB7_4FE7_9544_D2EEE84B31A4__INCLUDED_)
