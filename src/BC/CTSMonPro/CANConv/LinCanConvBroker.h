// LinCanConvBroker.h: interface for the CLinCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_)
#define AFX_LINCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include "CTSMonProDoc.h"
#include "CanConvBroker.h"
//#include "xlCANFunctions.h"
//#include "xlLINFunctions.h"
#include "vxlapi.h"

typedef enum
{
	LCC_PLAY,
	LCC_PAUSE,
	LCC_STOP,
} LCC_MODE ;

typedef struct tagLCC_INFO
{
	CHAR framename[256] ;	// framename
	CHAR item[256] ;	// item
	INT canid ;			// frameid
	INT datatype ;	// Unsign:0, Sign:1,
	INT can_startbit ;
	INT can_bitcounts ;
	FLOAT factor ; //OFFSET, SAFEUPPERLIMIT, SAFELOWERLIMIT, TERMUPPERLIMIT, TERMLOWERLIMIT, CAT1, CAT2, CAT3
	FLOAT offset ;
	FLOAT fault_upper ;
	FLOAT fault_lower ;
	FLOAT end_upper ;
	FLOAT end_lower ;
	short int function_division ;
	short int function_division2 ;
	short int function_division3 ;
	CHAR desc[512] ;
} LCC_INFO, *PLCC_INFO ;

typedef struct tagSCHEDULER_INFO
{
	INT id ;
	CHAR framename[256] ;
	INT delay ;
} SCHEDULER_INFO, *PSCHEDULER_INFO ;

typedef struct tagLINID_INFO
{
	INT id ;
	union {
		struct
		{
			BYTE d0 ;
			BYTE d1 ;
			BYTE d2 ;
			BYTE d3 ;
			BYTE d4 ;
			BYTE d5 ;
			BYTE d6 ;
			BYTE d7 ;
		};
		
		BYTE value[8];
	} data;
	
} LINID_INFO, *PLINID_INFO ;

class CCANFunctions ;
class CLINFunctions ;
class CCTSMonProDoc ;
class CLinCanConvBroker : public CCanConvBroker
{
public:
	CLinCanConvBroker();
	virtual ~CLinCanConvBroker();

	// CAN info
	CCANFunctions* m_pCAN;
	BOOL m_bSendCANData ;
//	BOOL m_bCanRxThreadRun ;
	XLstatus SendCANData(XLevent& xlEvent);

	// LIN info
	CLINFunctions* m_pLIN ;	
	BOOL m_bLinRxThreadRun ;
	BOOL m_bLinOnBus ;			// 스케쥴러 쓰레드에서의 진행여부 플래그
//	INT m_nLinID ;

	void DoNotShowMessage() ;
	void StopLinCanConvThread();
	void PauseLinCanConvThread();
	BOOL BeginLinCanConvThread(int nDevID, CString strDBName/*, INT nLinID*/, INT nLinChannel, INT nSpeed
							 , CArray<PLINID_INFO, PLINID_INFO>& linid
							 , CArray<PLCC_INFO, PLCC_INFO>& lcc, CArray<PSCHEDULER_INFO, PSCHEDULER_INFO>& sched
							 , INT nCanChannel, INT nCanBaudrate) ;
	void linCreateSchedulerThread() ;
	void ProcessLinData(XLevent &xlEvent) ;
	HANDLE m_hLinSchedulerTerminated ;
	HANDLE m_hLinRxThreadTerminated ;
//	HANDLE m_hCanRxThreadTerminated ;
	
	CString m_strLinSettingName ;	
	LCC_MODE m_nLinMode ;

	// CLinConvDlg 의 리스트에 전시여부
	CRITICAL_SECTION* m_pcsShowMessage ;
//	BOOL m_bShowMessage ;
	CListBox* m_pRxList ;
	CListBox* m_pTxList ;

	CArray<PLINID_INFO, PLINID_INFO> m_linid_array ;
	CArray<PLCC_INFO, PLCC_INFO> m_lcc_array ;
	CArray<PSCHEDULER_INFO, PSCHEDULER_INFO> m_schedule_array ;

	BOOL m_bSaveRxFlag ;
	void SetSaveRxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveRxFlag = b ; }
	
	BOOL m_bSaveTxFlag ;
	void SetSaveTxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveTxFlag = b ; }
	void SetIndex(INT idx) { ASSERT(this) ; m_nIndex = idx ; }

	INT m_nDevID ;
protected:
	// Lin-Can Conv Set ID	- PCI 슬롯번호(?) 최대 4개.
	INT m_nIndex ;

};

DWORD WINAPI LinSchedulerThread( PVOID par );

#endif // !defined(AFX_LINCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_)
