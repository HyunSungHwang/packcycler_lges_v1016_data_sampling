// LinConvDlg.cpp : implementation file
//

#include "stdafx.h" 
#include "../CTSMonPro.h"
#include "LinConvDlg.h"
#include "../CTSMonProView.h"
#include "CppSQLite3.h"
#include "LinSettingDBDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define GETBIT(data, index) ((data & (1 << index)) >> index)
#define SETBIT(data, index) (data = data | (1 << index))

unsigned long linspeed[] = { 16500, 19200, };
unsigned long canspeed[] = {125000, 250000, 500000, 1000000};

#define TAB_RX		0
#define TAB_TX		1


int HexStr2Dec(LPCTSTR hexStr)
{
	char ch;
	int  decVal = 0;
	
	sscanf(hexStr, "%s", hexStr);
	for(int  i = 0; hexStr[i]; i++)
	{
		ch = hexStr[i];
		decVal = decVal * 16;   // 자리수가 늘때마다 16을 곱함
		if (ch>='0' && ch<='9') // 0 - 9 사이의 값
		{
			decVal += ch - '0';
		}
		else
		{                  // A - F 사이의 값
			ch = ch & 0xdf; // 소문자를 대문자로 변환
			decVal += ch - 'A' + 10;
		}
	}
	
	return decVal ;
}

char* Dec2HexStr(int dec)
{
	char hexStr[128] ;

	sprintf(hexStr, "%x", dec) ;

	return hexStr ;
}

/////////////////////////////////////////////////////////////////////////////
// CLinConvDlg dialog

typedef enum
{
	LIN_COL_SEQ,
	LIN_COL_FRAMEID,	// COL_ADDR
	LIN_COL_FRAMENAME,	// COL_INDEX
	LIN_COL_ITEMS,
	LIN_COL_STARTBIT,
	LIN_COL_BITCOUNTS,
	LIN_COL_DATATYPE,
	LIN_COL_FACTOR,
	LIN_COL_OFFSET,
	LIN_COL_FAULT_UPPER,
	LIN_COL_FAULT_LOWER,
	LIN_COL_END_UPPER,
	LIN_COL_END_LOWER,
	LIN_COL_FUNCTION_DIVISION,
	LIN_COL_FUNCTION_DIVISION2,
	LIN_COL_FUNCTION_DIVISION3,
	LIN_COL_DESC,
	LIN_COL_NUM = LIN_COL_DESC,
} LIN_COL_TYPE ;

CLinConvDlg::CLinConvDlg()
{
}

CLinConvDlg::~CLinConvDlg()
{
	m_bCanRecvRxTxMsg = FALSE ;
}

CLinConvDlg::CLinConvDlg(CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CLinConvDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CLinConvDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CLinConvDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CLinConvDlg::IDD3):
	(CLinConvDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CLinConvDlg)
	m_eID = _T("");
	m_bSaveRx = FALSE;
	m_bSaveTx = FALSE;
	m_bCheckExtMode = FALSE;
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	m_bCanRecvRxTxMsg = FALSE ;

	m_nDevID = -1 ;
}

void CLinConvDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLinConvDlg)
	DDX_Control(pDX, IDC_PARAM_TAB, m_ctrlParamTab);
	DDX_Control(pDX, IDC_LIN_CHANNEL, m_ctrlLinChannel);
	DDX_Control(pDX, IDC_LIN_SPEED, m_ctrlLinSpeed);
	DDX_Control(pDX, IDC_CAN_CHANNEL, m_Channel);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE, m_CAN_Baudrate);
	DDX_Control(pDX, IDC_CAN_HARDWARE, m_ctlCanHardware);
	DDX_Control(pDX, IDC_RX_LIST, m_ctlRxList);
	DDX_Control(pDX, IDC_TX_LIST, m_ctlTxList);
	DDX_Text(pDX, IDC_CANID, m_eID);
	DDX_Check(pDX, IDC_SAVE_RX_PACKET, m_bSaveRx);
	DDX_Check(pDX, IDC_SAVE_TX_PACKET, m_bSaveTx);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE, m_bCheckExtMode);
	//}}AFX_DATA_MAP
}

IMPLEMENT_DYNCREATE(CLinConvDlg, CDialog)

BEGIN_MESSAGE_MAP(CLinConvDlg, CDialog)
	//{{AFX_MSG_MAP(CLinConvDlg)
	ON_BN_CLICKED(IDC_ETC_SETTING, OnEtcSetting)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_BN_CLICKED(IDC_RESET_RX_CONTENTS, OnResetRxContents)
	ON_BN_CLICKED(IDC_RESET_TX_CONTENTS, OnResetTxContents)
	ON_BN_CLICKED(IDC_OPER_START, OnOperStart)
	ON_BN_CLICKED(IDC_OPER_STOP, OnOperStop)
	ON_BN_CLICKED(IDC_OPEN_CONV_DATA, OnOpenConvData)
	ON_BN_CLICKED(IDC_TRANS_CAN_DATA, OnTransCanData)
	ON_BN_CLICKED(IDC_SAVE_CONFIG, OnSaveConfig)
	ON_BN_CLICKED(IDC_ADD_ROW, OnAddRow)
	ON_BN_CLICKED(IDC_DEL_ROW, OnDelRow)
	ON_BN_CLICKED(IDC_ADD_SCHE_ROW, OnAddScheRow)
	ON_BN_CLICKED(IDC_DEL_SCHE_ROW, OnDelScheRow)
	ON_NOTIFY(TCN_SELCHANGE, IDC_PARAM_TAB, OnSelchangeParamTab)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnGridBeginEdit)
	ON_MESSAGE(WM_GRID_ENDEDIT, OnGridEndEdit)
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()
//ON_EN_CHANGE(IDC_LIN_ID, OnChangeLinId)

/////////////////////////////////////////////////////////////////////////////
// CLinConvDlg message handlers

BOOL CLinConvDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//ljb 2011221 이재복 //////////
	//if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("LinConvDlg_OnInitDialog_msg1","IDD_LIN_CONV"));//&&
	m_pDoc->Fun_GetArryCanDivision(m_uiArryCanCode);		//ljb 2011222 이재복 //////////
	m_pDoc->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////

	// TODO: Add extra initialization here
	CCyclerModule* pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if (pMD == NULL)	return FALSE;
	
	CCyclerChannel* pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL) return FALSE;
	
	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
		//if(pMD->GetState() == PS_STATE_LINE_OFF)
	{
//		m_ctrlLabelMsg.SetText("동작중인 채널은 수정할 수 없습니다.");
//		EnableAllControl(FALSE);
		//return;
	}

	// 각 모듈에는 2개의 채널이 붙어 있다고 가정한다.
	m_nDevID = (nSelModule - 1) * 2 + nSelCh ;


	// . 컨트롤 초기화

	// Lin 속도 설정
	m_ctrlLinSpeed.SetCurSel(1) ;
	m_ctrlLinSpeed.SetItemData(0, linspeed[0]) ;
	m_ctrlLinSpeed.SetItemData(1, linspeed[1]) ;

	// Lin 채널 설정
	m_ctrlLinChannel.SetCurSel(m_nDevID * 2) ;

	// Lin ID 설정
//	m_nLinID = 51 ;

	// Can Initialize --------------------------------
	// we need the listbox pointer within our CAN class
	m_CAN.m_pOutput   = &m_ctlTxList;
	m_CAN.m_pHardware = &m_ctlCanHardware ;
	
	// init the CAN hardware
/*
	if (m_CAN.CANInit())
	{
		m_ctlCanHardware.ResetContent();
		m_ctlCanHardware.AddString("<ERROR> no HW!");
		m_ctlTxList.AddString("You need two CANcabs/CANpiggy's");
		m_btnOnBus.EnableWindow(FALSE);
	}
*/

	// Can Initialize --------------------------------

	// init the default window
	m_CAN_Baudrate.SetCurSel(2);
	m_Channel.SetCurSel(m_nDevID * 2 + 1);
	
// 	m_eFilterFrom = "5";
// 	m_eFilterTo   = "10";
	
	// CAN이 OnBus 여야지 전송할 수 있다.
//	m_bSendCANData = FALSE ;


	// Rx/Tx 로그 사용
	m_bSaveRx = m_pDoc->m_bSaveRxFlag ;
	m_bSaveTx = m_pDoc->m_bSaveTxFlag ;


	// . 마지막 저장값 불러오기
	UpdateData(FALSE) ;
	
	// Added by 224 (2014/04/23) : 모드버스 다이얼로그 핸들링을 위한 변수할당
	m_pDoc->m_pLinDlg = this ;
	m_bCanRecvRxTxMsg = TRUE ;

	LCC_MODE nMode = m_pDoc->GetLinMode(m_nDevID) ;
	switch (nMode)
	{
	case LCC_PLAY:
		// 적용 버튼은 비 활성화 시킨다.
		GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;

		GetDlgItem(IDC_OPER_START)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_OPER_STOP)->EnableWindow(TRUE) ;
		m_Channel.SetCurSel(m_pDoc->GetLinCanChannel(m_nDevID));

		// 채널 변경 비 활성화
		m_Channel.EnableWindow(FALSE) ;

		break ;
	default:
		GetDlgItem(IDC_OPER_START)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_OPER_PAUSE)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_OPER_STOP)->EnableWindow(FALSE) ;
		break ;
	}


//	m_wndTxGrid.ShowScrollBar(SB_VERT, FALSE) ;
	
	// GxGrid 의 첫번채 컬럼을 지우기...
	// GxGrid 는 기본으로 첫 컬럼은 순번으로 채워진다.


	//-------------------------------------------------------------------------------------
	
	InitParamTab();
	InitGridRx();
	InitGridTx();
	InitGridSchedule();

	m_nTabSelection = TAB_RX ;

	//--------------------------------------------------------------------
	// XP 에서 상대경로가 적용 안되는 경우가 있음
/*
	TCHAR szCurDir[MAX_PATH];
	::GetModuleFileName(AfxGetApp()->m_hInstance, szCurDir, MAX_PATH);
	
	char Drive[_MAX_DRIVE];
	char Path[_MAX_PATH];
	
	_splitpath(szCurDir, Drive, Path, NULL,NULL);//Filename, Ext);
	
	m_strConvDBFilename  = Drive ;
	m_strConvDBFilename += Path ;
	m_strConvDBFilename += "MODBUSCONV.db" ;
	
	//	m_strConvDBFilename = "C:\\Program Files\\PNE CTSPack\\MODBUSCONV.db" ;
	//--------------------------------------------------------------------
*/

	// 작동중이거나 일시 정지인 경우 설정된 값으로 컨트롤들을 세트한다.
	if (LCC_PLAY == m_pDoc->m_LinCanConv[m_nDevID].m_nLinMode || LCC_PAUSE == m_pDoc->m_LinCanConv[m_nDevID].m_nLinMode)
	{
//		m_strDBName = m_pDoc->m_LinCanConv[m_nDevID].m_strLinSettingName ;
//		SetControlsWithDBName(m_strDBName) ;

		// Play 중일 때는 빈 Row를 클리어 하고 로드한다
		if (m_wndLinGrid.GetRowCount() > 0)
		{
			m_wndLinGrid.RemoveRows(1, m_wndLinGrid.GetRowCount()) ;
		}

		m_strConfigFilename = m_pDoc->m_LinCanConv[m_nDevID].m_strLinSettingName ;
		if (LoadLinConfig(m_strConfigFilename) == TRUE)
		{
			CHAR filename[MAX_FNAME_LEN] ;
			CHAR ext[MAX_FNAME_LEN] ;
			_splitpath(m_strConfigFilename, NULL, NULL, filename, ext) ;

			CString strName;
			//strName.Format("Lin 설정 - %s.%s",  filename, ext);
			strName.Format(Fun_FindMsg("LinConvDlg_OnInitDialog_msg2","IDD_LIN_CONV"),  filename, ext);//&&
			this->SetWindowText(strName);
		}

//		ASSERT(!m_pDoc->m_strLinSettingName.IsEmpty()) ;
	}

	// 기타설정 문구 초기화
	//SetDlgItemText(IDC_ETC_SETTING, "기타설정 <<") ;
	SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("LinConvDlg_OnInitDialog_msg3","IDD_LIN_CONV")) ;//&&
	OnEtcSetting() ;

	// LIN ID 값 HEX 로 변경
//	OnChangeLinId() ;


	m_pDoc->SetLinShowMessageFlag(m_nDevID, &m_ctlRxList, &m_ctlTxList) ;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLinConvDlg::InitParamTab()
{
	m_ctrlParamTab.ModifyStyle(TCS_BOTTOM|TCS_MULTILINE|TCS_VERTICAL|TCS_BUTTONS, TCS_OWNERDRAWFIXED|TCS_FIXEDWIDTH);

	TC_ITEM item;
	item.mask = TCIF_TEXT;
	item.pszText = _T("LIN Rx");
	m_ctrlParamTab.InsertItem(TAB_RX, &item);

	item.pszText = "LIN Tx";
	m_ctrlParamTab.InsertItem(TAB_TX, &item);

	m_ctrlParamTab.SetColor(RGB(0,0,0), RGB(255,255,255), RGB(240,240,240), RGB(240,240,240));	// 글자색, 바깥쪽 라인, 내부 색깔, 이미지쪽 라인
}

void CLinConvDlg::Fun_ChangeViewControl(BOOL bFlag)
{

	m_wndLinGrid.ShowWindow(bFlag) ;

// Commented by 224 (2015/06/05) : 왜 주석처리가 되어있었지???

	//+ 2015.9.2 USY Mod For Test
// 	GetDlgItem(IDC_ADD_ROW)->ShowWindow(bFlag) ;
// 	GetDlgItem(IDC_DEL_ROW)->ShowWindow(bFlag) ;
// 	GetDlgItem(IDC_ETC_SETTING)->ShowWindow(bFlag) ;
	//-
	m_wndTxGrid.ShowWindow(!bFlag) ;
	GetDlgItem(IDC_STATIC_REQUEST)->ShowWindow(!bFlag) ;
}

void CLinConvDlg::InitGridRx()			
{
	m_wndLinGrid.SubclassDlgItem(IDC_LIN_GRID, this);
	m_wndLinGrid.Initialize();

	CRect rect;
	m_wndLinGrid.GetParam()->EnableUndo(FALSE);
	
	// 기본 행 설정
	m_wndLinGrid.SetRowCount(1);

	// grid 컬럼 사이즈 설정
	m_wndLinGrid.SetColCount(LIN_COL_NUM);
	m_wndLinGrid.GetClientRect(&rect);
	m_wndLinGrid.SetColWidth(LIN_COL_SEQ,       LIN_COL_SEQ,       30);
	m_wndLinGrid.SetColWidth(LIN_COL_FRAMEID,   LIN_COL_FRAMEID,   120);
	m_wndLinGrid.SetColWidth(LIN_COL_FRAMENAME, LIN_COL_FRAMENAME, 120);
	m_wndLinGrid.SetColWidth(LIN_COL_ITEMS,     LIN_COL_ITEMS,     120);
	m_wndLinGrid.SetColWidth(LIN_COL_STARTBIT,  LIN_COL_STARTBIT,  60);
	m_wndLinGrid.SetColWidth(LIN_COL_BITCOUNTS, LIN_COL_BITCOUNTS, 60);
	m_wndLinGrid.SetColWidth(LIN_COL_DATATYPE,  LIN_COL_DATATYPE,  60);
	m_wndLinGrid.SetColWidth(LIN_COL_FACTOR,    LIN_COL_FACTOR,    60);
	m_wndLinGrid.SetColWidth(LIN_COL_OFFSET,    LIN_COL_OFFSET,    60);
	m_wndLinGrid.SetColWidth(LIN_COL_FAULT_UPPER, LIN_COL_FAULT_UPPER, 60); 
	m_wndLinGrid.SetColWidth(LIN_COL_FAULT_LOWER, LIN_COL_FAULT_LOWER, 60);
	m_wndLinGrid.SetColWidth(LIN_COL_END_UPPER, LIN_COL_END_UPPER, 60);
	m_wndLinGrid.SetColWidth(LIN_COL_END_LOWER, LIN_COL_END_LOWER, 60);
// 	m_wndLinGrid.SetColWidth(LIN_COL_FUNCTION_DIVISION, LIN_COL_FUNCTION_DIVISION, 60);
// 	m_wndLinGrid.SetColWidth(LIN_COL_FUNCTION_DIVISION2, LIN_COL_FUNCTION_DIVISION2, 60);
// 	m_wndLinGrid.SetColWidth(LIN_COL_FUNCTION_DIVISION3, LIN_COL_FUNCTION_DIVISION3, 60);
	m_wndLinGrid.SetColWidth(LIN_COL_FUNCTION_DIVISION, LIN_COL_FUNCTION_DIVISION, 100); //ksj 20200330 : size 조절
	m_wndLinGrid.SetColWidth(LIN_COL_FUNCTION_DIVISION2, LIN_COL_FUNCTION_DIVISION2, 100); //ksj 20200330 : size 조절
	m_wndLinGrid.SetColWidth(LIN_COL_FUNCTION_DIVISION3, LIN_COL_FUNCTION_DIVISION3, 100); //ksj 20200330 : size 조절
	m_wndLinGrid.SetColWidth(LIN_COL_DESC, LIN_COL_DESC, 500);

	// column header 설정
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FRAMEID),      CGXStyle().SetValue("Frame ID (HEX)"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FRAMENAME),    CGXStyle().SetValue("Frame Name"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_ITEMS),	   CGXStyle().SetValue("Items"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_STARTBIT),  CGXStyle().SetValue("StartBit"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_BITCOUNTS), CGXStyle().SetValue("BitCount"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_DATATYPE),  CGXStyle().SetValue("DataType"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FACTOR),    CGXStyle().SetValue("Factor"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_OFFSET),    CGXStyle().SetValue("Offset"));
	/* 
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FAULT_UPPER), CGXStyle().SetValue("안전상한"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FAULT_LOWER), CGXStyle().SetValue("안전하한"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_END_UPPER), CGXStyle().SetValue("종료상한"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_END_LOWER), CGXStyle().SetValue("종료하한"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FUNCTION_DIVISION),    CGXStyle().SetValue("분류1"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FUNCTION_DIVISION2),    CGXStyle().SetValue("분류2"));
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FUNCTION_DIVISION3),    CGXStyle().SetValue("분류3"));
	*/
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FAULT_UPPER), CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridRx_msg1","IDD_LIN_CONV")));//&&
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FAULT_LOWER), CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridRx_msg2","IDD_LIN_CONV")));//&&
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_END_UPPER), CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridRx_msg3","IDD_LIN_CONV")));//&&
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_END_LOWER), CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridRx_msg4","IDD_LIN_CONV")));//&&
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FUNCTION_DIVISION),    CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridRx_msg5","IDD_LIN_CONV")));//&&
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FUNCTION_DIVISION2),    CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridRx_msg6","IDD_LIN_CONV")));//&&
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_FUNCTION_DIVISION3),    CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridRx_msg7","IDD_LIN_CONV")));//&&
	
	m_wndLinGrid.SetStyleRange(CGXRange(0, LIN_COL_DESC),      CGXStyle().SetValue("Description").SetHorizontalAlignment(DT_LEFT));

	// Resize Col Width
	m_wndLinGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);
	m_wndLinGrid.GetParam()->EnableUndo(TRUE);

	// 컬럼별 속성 설정
	char str[12], str2[7], str3[5];

	// Frame ID
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FRAMEID),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	// Frame Name
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FRAMENAME),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

	// ITEMS
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_ITEMS),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
	);

/*
	// Frame ID
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FRAMEID),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
	);
*/

	// Start bit
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63"))
			//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("LinConvDlg_InitGridRx_msg8","IDD_LIN_CONV"))//**
			.SetValue(0L)
	);

	// Bit count
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 8);		//정수부
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_BITCOUNTS),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(16L)
	);

	// Datatype
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_DATATYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
			//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
			.SetChoiceList(_T("Unsigned\r\nSigned\r\n"))
			.SetValue(_T("Unsigned"))
	);

	// Factor
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FACTOR),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1.0f)
	);

	// offet (
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 안전상한
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FAULT_UPPER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);


	// 안전하한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FAULT_LOWER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 종료상한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_END_UPPER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 종료하한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_END_LOWER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	//ljb 2011222 이재복 S //////////
	CString strComboItem,strTemp;
	strComboItem = "None [0]\r\n";
	for (int i=0; i < m_strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",m_strArryCanName.GetAt(i),m_uiArryCanCode.GetAt(i));
		strComboItem = strComboItem + strTemp;
	}

	/*m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FUNCTION_DIVISION, LIN_COL_FUNCTION_DIVISION3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)	//선택만 가능
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);*/

	//ksj 20200330 : 분류 값 선택시 값이 선택되지 않고 사라지는 현상 수정.
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_FUNCTION_DIVISION,LIN_COL_FUNCTION_DIVISION3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);


	// Desc
	m_wndLinGrid.SetStyleRange(CGXRange().SetCols(LIN_COL_DESC),
		CGXStyle()
			.SetHorizontalAlignment(DT_LEFT)
	);


}

void CLinConvDlg::InitGridTx()
{
	//-------------------------------------------------------------------------------------
	// Tx  grid 초기화
	m_wndTxGrid.SubclassDlgItem(IDC_TX_GRID, this);
	m_wndTxGrid.Initialize();
	m_wndTxGrid.GetParam()->EnableUndo(FALSE);
	
	// grid 컬럼 사이즈 설정
	m_wndTxGrid.SetColCount(8);
// 	m_wndTxGrid.SetColWidth(0, 0, 0);
	m_wndTxGrid.SetColWidth(0, 8, 80);
	m_wndTxGrid.SetRowHeight(1, 1, 15);
	
	// column header 설정
	m_wndTxGrid.SetStyleRange(CGXRange(0, 1), CGXStyle().SetValue("ID(HEX)"));
	m_wndTxGrid.SetStyleRange(CGXRange(0, 2), CGXStyle().SetValue("Name"));
	m_wndTxGrid.SetStyleRange(CGXRange(0, 3), CGXStyle().SetValue("StartBit"));
	m_wndTxGrid.SetStyleRange(CGXRange(0, 4), CGXStyle().SetValue("BitCount"));
	//m_wndTxGrid.SetStyleRange(CGXRange(0, 5), CGXStyle().SetValue("설정값"));
	m_wndTxGrid.SetStyleRange(CGXRange(0, 5), CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridTx_msg1","IDD_LIN_CONV")));//&&
	m_wndTxGrid.SetStyleRange(CGXRange(0, 6), CGXStyle().SetValue("Factor"));
	m_wndTxGrid.SetStyleRange(CGXRange(0, 7), CGXStyle().SetValue("Offset"));
	//m_wndTxGrid.SetStyleRange(CGXRange(0, 8), CGXStyle().SetValue("연산값"));
	m_wndTxGrid.SetStyleRange(CGXRange(0, 8), CGXStyle().SetValue(Fun_FindMsg("LinConvDlg_InitGridTx_msg2","IDD_LIN_CONV")));//&&

	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetMaxLength(2).SetValueType(GX_VT_NUMERIC));
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetMaxLength(2).SetValueType(GX_VT_NUMERIC));
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(4), CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetMaxLength(2).SetValueType(GX_VT_NUMERIC));
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(5), CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC));
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(6), CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC));
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(7), CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC));
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(8), CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC));

	// 컬럼별 속성 설정
	char str[12], str2[7], str3[5];

	// Start bit
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(3),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63"))
			//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("LinConvDlg_InitGridRx_msg8","IDD_LIN_CONV"))//&&
			.SetValue(0L)
	);

	// Bit count
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 8);		//정수부
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(4),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(8L)
	);

	// 설정값
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(5),
		CGXStyle()
			.SetValue(0L)
	);

	// Factor
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(6),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1.0f)
	);

	// offet
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(7),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 연산값
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndTxGrid.SetStyleRange(CGXRange().SetCols(8),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
		);
	
	// 기본 행 설정
	m_wndTxGrid.SetRowCount(1);
	m_wndTxGrid.SetScrollBarMode(SB_VERT, FALSE);	//Vertical Scroll Bar
}

void CLinConvDlg::InitGridSchedule()
{
	//-------------------------------------------------------------------------------------
	// scheduler grid 초기화
	m_wndScheduleGrid.SubclassDlgItem(IDC_SCHEDULER_GRID, this);
	m_wndScheduleGrid.Initialize();
	m_wndScheduleGrid.GetParam()->EnableUndo(FALSE);
	
	// grid 컬럼 사이즈 설정
	m_wndScheduleGrid.SetColCount(3);
	m_wndScheduleGrid.SetColWidth(1, 3, 120);
	
	// column header 설정
	m_wndScheduleGrid.SetStyleRange(CGXRange(0, 1),      CGXStyle().SetValue("ReqID(hex)"));
	m_wndScheduleGrid.SetStyleRange(CGXRange(0, 2),      CGXStyle().SetValue("Frame name"));
	m_wndScheduleGrid.SetStyleRange(CGXRange(0, 3),      CGXStyle().SetValue("Delay(ms)"));
	m_wndScheduleGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
	m_wndScheduleGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
	m_wndScheduleGrid.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
	
	// 기본 행 설정
	m_wndScheduleGrid.SetRowCount(1);
	
}

void CLinConvDlg::OnEtcSetting() 
{
	CString str ;
	GetDlgItemText(IDC_ETC_SETTING, str) ;
	
	BOOL bMore = FALSE ;
	if (str.Right(2) == ">>")
	{
		bMore = TRUE ;
	}
	
	CRect rcDialog ;
	GetWindowRect(rcDialog) ;
	int nNewWidth = -1 ;
	
	CWnd* pWndMoreOrLess = NULL ;
	CRect rcMoreOrLess ;
	if (bMore)
	{
		pWndMoreOrLess = GetDlgItem(IDC_LARGE) ;
		//SetDlgItemText(IDC_ETC_SETTING, "기타설정 <<") ;
		SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("LinConvDlg_OnEtcSetting_msg1","IDD_LIN_CONV")) ;//&&
	}
	else
	{
		pWndMoreOrLess = GetDlgItem(IDC_SMALL) ;
		//SetDlgItemText(IDC_ETC_SETTING, "기타설정 >>") ;
		SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("LinConvDlg_OnEtcSetting_msg1","IDD_LIN_CONV")) ;//&&
	}
	ASSERT_VALID(pWndMoreOrLess) ;
	
	pWndMoreOrLess->GetWindowRect(rcMoreOrLess) ;
	
	nNewWidth = rcMoreOrLess.left - rcDialog.left ;
	SetWindowPos(NULL, 0, 0, nNewWidth, rcDialog.Height(), SWP_NOMOVE | SWP_NOZORDER) ;
	
}




/*
void CLinConvDlg::OnChangeLinId() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	UpdateData() ;

	CString str ;
	str.Format("0x%02X", m_nLinID) ;

	SetDlgItemText(IDC_STATIC_LINID_HEX, str) ;
}
*/

void CLinConvDlg::OnClose() 
{
	m_bCanRecvRxTxMsg = FALSE ;

	m_pDoc->ClearLinAllShowMessageFlag() ;

	// 쓰레드에서 리스트박스에 쓰는 시간 까지 멈추기
	Sleep(200) ;

	// clearing
/*
	for (INT nLoop = 0; nLoop < m_lcc_array.GetSize(); nLoop++)
	{
		LCC_INFO* ptr = m_lcc_array.GetAt(nLoop) ;
		delete ptr ;
	}	
	m_lcc_array.RemoveAll() ;
*/
	

	CDialog::OnCancel();
	
}

void CLinConvDlg::OnCancel() 
{
	return ;
}


void CLinConvDlg::OnOK() 
{
	return ;
}

void CLinConvDlg::OnResetRxContents() 
{
	m_ctlRxList.ResetContent() ;	
}

void CLinConvDlg::OnResetTxContents() 
{
	m_ctlTxList.ResetContent() ;
}



void CLinConvDlg::OnOperStart() 
{
	UpdateData() ;
	
//	CWnd* pWnd = FindWindow(NULL, "Lin 설정") ;

	m_bCanRecvRxTxMsg = TRUE ;
	
// 	if (m_strDBName.IsEmpty())
// 	{
// 		MessageBox("DB 이름을 설정하고 운영 시작을 하여야 합니다.") ;
// 		return ;
// 	}
	
	if (m_pDoc->BeginLinCanConvThread(nSelCh, nSelModule, m_strConfigFilename))
	{
		GetDlgItem(IDC_OPER_START)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_OPER_STOP)->EnableWindow(TRUE) ;
	}
	
	// 적용 버튼은 비 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;
	
	// 채널 변경 비 활성화
//	m_Channel.EnableWindow(FALSE) ;
}

void CLinConvDlg::OnOperStop() 
{
	// TODO: Add your control notification handler code here
	m_bCanRecvRxTxMsg = FALSE ;
	m_pDoc->StopLinCanConvThread(m_nDevID) ;
	
	GetDlgItem(IDC_OPER_START)->EnableWindow(TRUE) ;
	GetDlgItem(IDC_OPER_STOP)->EnableWindow(FALSE) ;
	
	// 채널 변경 활성화
// 	m_Channel.EnableWindow(TRUE) ;
}

// 호출한 곳에서 array 의 메모리를 제거하여야 한다.
void CLinConvDlg::GetLinIDInfo(CArray<PLINID_INFO, PLINID_INFO>& array)
{
	ASSERT(array.GetSize() == 0) ;
	
	INT nCurrAddr = -1 ;
	INT nPrevAddr = -1 ;
	LINID_INFO* pInfo = NULL ;

	for (int nRow = 1; nRow < m_wndTxGrid.GetRowCount()+1; nRow++)
	{
		nCurrAddr = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 1)) ;
		
		if (nCurrAddr != nPrevAddr)
		{
			// 이전 다 채워진 packet 을 array 에 넣는다. 초기값은 제외
			if (pInfo != NULL)
			{
				array.Add(pInfo) ;
			}

			pInfo = new LINID_INFO ;
			ZeroMemory(pInfo, sizeof(LINID_INFO)) ;

			pInfo->id = nCurrAddr ;
		}
		
		int nStartBit = atoi(m_wndTxGrid.GetValueRowCol(nRow, 3)) ;
		int nBitCounts = atoi(m_wndTxGrid.GetValueRowCol(nRow, 4)) ;
		int nValue = atoi(m_wndTxGrid.GetValueRowCol(nRow, 5)) ;
//		if (nStartBit + nBitCounts > 63 || nStartBit < 0 || nBitCounts <= 0 || nValue >= pow(2, nBitCounts))
		if (nStartBit + nBitCounts > 64 || nStartBit < 0 || nBitCounts <= 0 || nValue >= pow((double)2, (double)nBitCounts))
		{
			// 번지수를 넘어가는 경우는 skip 한다.
			// 실제값의 필요공간이 저장공간보다 큰경우 skip 한다.
			// bitcount 는 2의 자승이다.
			continue ;
		}

		// 비트 연산으로 값을 채운다.
		// 뒷자리부터 설정한다.
		for (int n = 0; n < nBitCounts; n++)
		{
			bool flag = nValue & (1 << n) ;

			if (flag)
			{
				// 예) nStartBit = 20, nBitCounts = 8, nValue = 255
				// idx=2 mod=[4,5,6,7], idx=3 mod=[0,1,2,3]
				int idx = (nStartBit + n) / 8 ;
				int mod = (nStartBit + n) % 8 ;

				// 해당 bit의 flag 값을 세트한다.
				SETBIT(pInfo->data.value[idx], mod) ;
			}
		}


		// Modified by 224 (2014/11/05) : Dec->Hex 로 전시
/*
		info->id = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 1)) ;
		info->d0 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 2)) ;
		info->d1 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 3)) ;
		info->d2 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 4)) ;
		info->d3 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 5)) ;
		info->d4 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 6)) ;
		info->d5 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 7)) ;
		info->d6 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 8)) ;
		info->d7 = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 9)) ;
*/

		nPrevAddr = nCurrAddr ;

	}

	// 마지막 구조체 저장
	array.Add(pInfo) ;


#ifdef _DEBUG	
	for (int nLoop = 0; nLoop < array.GetSize(); nLoop++)
	{
		LINID_INFO* ptr = array.GetAt(nLoop) ;
		//		TRACE("addr:%02x | s: %02d | b: %2d | datatype: %d | canid: %03d | s: %02d | b: %2d | f: %.2f | name: %s \n"
		TRACE("id = %d, d0=0x%02x, d1=0x%02x, d2=0x%02x, d3=0x%02x, d4=0x%02x, d5=0x%02x, d6=0x%02x, d7=0x%02x  \n"
			, ptr->id
			, ptr->data.d0
			, ptr->data.d1
			, ptr->data.d2
			, ptr->data.d3
			, ptr->data.d4
			, ptr->data.d5
			, ptr->data.d6
			, ptr->data.d7) ;
	}
#endif
}

// 호출한 곳에서 array 의 메모리를 제거하여야 한다.
void CLinConvDlg::GetScheduleInfo(CArray<PSCHEDULER_INFO, PSCHEDULER_INFO>& array)
{
	ASSERT(array.GetSize() == 0) ;
	
	INT nCurrAddr = -1 ;
	for (int nRow = 1; nRow < m_wndScheduleGrid.GetRowCount()+1; nRow++)
	{		
		SCHEDULER_INFO* info = new SCHEDULER_INFO ;
		ZeroMemory(info, sizeof(SCHEDULER_INFO)) ;
		

		// Modified by 224 (2014/11/05) : Dec->Hex 로 전시
//		info->id    = atoi(m_wndScheduleGrid.GetValueRowCol(nRow, 1)) ;
		info->id    = HexStr2Dec(m_wndScheduleGrid.GetValueRowCol(nRow, 1)) ;
		strcpy(info->framename, m_wndScheduleGrid.GetValueRowCol(nRow, 2)) ;
		info->delay = atoi(m_wndScheduleGrid.GetValueRowCol(nRow, 3)) ;
		
		array.Add(info) ;
		
	}
	
#ifdef _DEBUG	
	for (int nLoop = 0; nLoop < array.GetSize(); nLoop++)
	{
		SCHEDULER_INFO* ptr = array.GetAt(nLoop) ;
		//		TRACE("addr:%02x | s: %02d | b: %2d | datatype: %d | canid: %03d | s: %02d | b: %2d | f: %.2f | name: %s \n"
		TRACE("id = %d, delay = %d \n"
			, ptr->id
			, ptr->delay) ;
	}
#endif
}

// 호출한 곳에서 array 의 메모리를 제거하여야 한다.
void CLinConvDlg::GetLinConvInfo(CArray<PLCC_INFO, PLCC_INFO>& array)
{
	// 현재 테이블 값 검증. 순차적인가? INDEX, STARTBIT, BITCOUNTS
	
	// CAN Receive 설정 함수 호출
	//	
	// 누적 bit Counts 가 64(8bytes) 가 넘으면 새로운 CANID를 생성한다.
	
	ASSERT(array.GetSize() == 0) ;
	
//	int canid = 600 ;
//	int nAccCounts = 0 ;	// 누적값이 64를 넘으면 안된다.
	
	INT nCurrAddr = -1 ;
	for (int nRow = 1; nRow < m_wndLinGrid.GetRowCount()+1; nRow++)
	{
		// Added by 224 (2014/11/05) : 16 진수 로 수정 
//		nCurrAddr    = atoi(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FRAMEID)) ;
		nCurrAddr    = HexStr2Dec(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FRAMEID));
		ASSERT(nCurrAddr > 0) ;
			
		LCC_INFO* info = new LCC_INFO ;
		ZeroMemory(info, sizeof(LCC_INFO)) ;
		
		strcpy(info->framename, m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FRAMENAME)) ;
		strcpy(info->item, m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_ITEMS)) ;
		info->datatype     = (CString("Unsigned") == m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_DATATYPE)) ? 0 : 1 ;
		strcpy(info->desc, m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_DESC)) ;
		
//		info->canid			= atoi(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FRAMEID)) ;
		info->canid			= HexStr2Dec(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FRAMEID)) ;
		info->can_startbit  = atoi(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_STARTBIT)) ;
		info->can_bitcounts = atoi(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_BITCOUNTS)) ;
		info->factor		= atof(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FACTOR)) ;
		info->offset		= atof(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_OFFSET)) ;
		info->fault_upper	= atof(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FAULT_UPPER)) ;
		info->fault_lower   = atof(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FAULT_LOWER)) ;
		info->end_upper	    = atof(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_END_UPPER)) ;
		info->end_lower	    = atof(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_END_LOWER)) ;
		
		CString strTemp, strCode ;
		INT nLength ;
		
		strTemp = m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FUNCTION_DIVISION);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		info->function_division  = atoi(strCode);
		
		strTemp = m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FUNCTION_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		info->function_division2  = atoi(strCode);
		
		strTemp = m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FUNCTION_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		info->function_division3  = atoi(strCode);
		
//		nAccCounts += bitcounts ;
		
		array.Add(info) ;
		
	}
	
#ifdef _DEBUG	
	for (int nLoop = 0; nLoop < array.GetSize(); nLoop++)
	{
		LCC_INFO* ptr = array.GetAt(nLoop) ;
//		TRACE("addr:%02x | s: %02d | b: %2d | datatype: %d | canid: %03d | s: %02d | b: %2d | f: %.2f | name: %s \n"
		TRACE("canid: %03d | datatype: %d | s: %02d | b: %2d | f: %.2f | name: %s \n"
			, ptr->canid
			, ptr->datatype
			, ptr->can_startbit
			, ptr->can_bitcounts
			, ptr->factor
			, ptr->item) ;
	}
#endif
}


void CLinConvDlg::OnOpenConvData() 
{
#if 1

	// Added by 224 (2014/10/29):
	// 고객 (LG 화학)의 요청으로 인하여, SQLite 사용 안하고,
	// 기존 csv 파일을 이용한 UI 로 변환

	{
		CString strTemp;
		strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
			"csv", "csv");
		
		CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
		pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
		if (IDOK == pDlg.DoModal())
		{
			if (m_wndLinGrid.GetRowCount() > 0)
			{
				m_wndLinGrid.RemoveRows(1, m_wndLinGrid.GetRowCount());
			}

			m_strConfigFilename = pDlg.GetPathName() ;
			if (LoadLinConfig(m_strConfigFilename) == TRUE)
			{
				CString strName;
				//strName.Format("Lin 설정 - %s", pDlg.GetFileName());
				strName.Format(Fun_FindMsg("LinConvDlg_OnOpenConvData_msg1","IDD_LIN_CONV"), pDlg.GetFileName());//&&
				this->SetWindowText(strName);
				
//				IsChangeFlag(TRUE);
//				GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
			}
		}
	}

#else

	// 파일 DB 사용안함.
	{
		char szEntry[_MAX_PATH];
		CString strRecentName, defultName;
		
		//기본 Data 위치
		::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
		CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
		defultName.Format("%s\\DataBase\\Config", szCurDir);
		
		CString strTemp;
		strTemp.Format("db file(*.%s)|*.%s|All Files(*.*)|*.*|", "db", "db");
		
		CFileDialog pDlg(TRUE, "db", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
		pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
		pDlg.m_ofn.lpstrInitialDir = defultName;
		if (IDOK == pDlg.DoModal())
		{
			m_strConvDBFilename = pDlg.GetPathName() ;
		}
	}
	
	// 대화상자에서 레코드를 선택한다.
	CLinSettingDBDialog dlg ; 
	dlg.m_strConvDBFilename = m_strConvDBFilename ;
	dlg.m_bSaveMode = FALSE ;
	if (IDOK != dlg.DoModal())
	{
		return ;
	}

	

	m_strDBName = dlg.m_strName ;
	SetControlsWithDBName(m_strDBName) ;

#endif

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;
}

BOOL CLinConvDlg::LoadLinConfig(CString strLoadPath)
{
	CString strData;
	CStdioFile aFile;
	CFileException e;
	int nIndex, nPos = 0;
	int nTemp;
	INT nProtocolVer ;

	if (!aFile.Open(strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e))
	{
		//AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("LinConvDlg_LoadLinConfig_msg1","IDD_LIN_CONV"));//&&
		return FALSE;
	}

	CStringList		strDataList;
	CStringList		strItemList;
	aFile.ReadString(strData);	//Version Check

	if (strData.Find("Data") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data
		
		CString strType, strDescript;
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);		// Lin 여부 Check
			strType = strTemp;

			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);				//1: 1006 Ver 추후 버전 업그레이드에 따라 추가
			
			if(strTemp.IsEmpty())
				nProtocolVer = 0;
			else
				nProtocolVer = atoi(strTemp);

			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			strDescript = strTemp;
		}

// 		EnableMaskFilter(m_ckUserFilter, PS_CAN_TYPE_MASTER);
			
		// Data Info에서 Type에 따라 Load할 Data를 결정	
		if (strType != "LIN")
		{
			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("LinConvDlg_LoadLinConfig_msg2","IDD_LIN_CONV"));//&&
//			m_nProtocolVer = 0;
			return FALSE;
		}

		nPos = 0;
		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//
	}	

	if (strData.Find("LIN-CAN") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.

		// LIN인지 Check
		CString strTitle = "LIN BaudRate,CAN BaudRate,Ext ID" ;
 		if (strData != strTitle)
 		{
 			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("LinConvDlg_LoadLinConfig_msg2","IDD_LIN_CONV"));//&&
 			return FALSE;
 		}

		aFile.ReadString(strData);	//Data

		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			// Lin baudrate
// 			nPos = nIndex+1;
// 			nIndex = strData.Find(",", nPos);

			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			INT nBaudrate = atoi(strTemp) ;

			for (int i = 0; i < m_ctrlLinSpeed.GetCount(); i++)
			{
				if (nBaudrate == m_ctrlLinSpeed.GetItemData(i))
				{
					m_ctrlLinSpeed.SetCurSel(i) ;
					break ;
				}
			}

			// Can baudrate
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			strTemp = strData.Mid(nPos, nIndex - nPos);
			INT nCanBaud  = atoi(strTemp) ;
			m_CAN_Baudrate.SetCurSel(0) ;
			for (int n = 0; n < sizeof(canspeed) / sizeof(canspeed[0]); n++)
			{
				if (nCanBaud == canspeed[n])
				{
					m_CAN_Baudrate.SetCurSel(n) ;
					break ;
				}
			}

			// Can extended
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_bCheckExtMode = atoi(strTemp) ;

		}
	}

	aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.

	// Lin ID.........................
	// .1. 현재 grid 의 모든 항목 삭제
	if (m_wndTxGrid.GetRowCount() > 0)
	{
		m_wndTxGrid.RemoveRows(1, m_wndTxGrid.GetRowCount()) ;
	}

	aFile.ReadString(strData);
	if (strData.Find("LIN ID") > 0)
	{
		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		//CString strTitle = "LINID(hex),Name,StartBit,BitCount,설정값,Factor,Offset";
		CString strTitle = Fun_FindMsg("LinConvDlg_LoadLinConfig_msg3","IDD_LIN_CONV"); //&&
//		CString strTitle = "LINID(hex),Name,StartBit,BitCount,설정값";
		if (strData != strTitle)
		{
			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("LinConvDlg_LoadLinConfig_msg2","IDD_LIN_CONV"));//&&
			return FALSE;
		}
		
		while (aFile.ReadString(strData))	// TX items
		{
			CString strTemp ;

			if (strData == "")
				break;

			INT nRow = m_wndTxGrid.GetRowCount() + 1;
			m_wndTxGrid.InsertRows(nRow, 1);

			int p0, p1 = 0;

			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 1), strTemp) ;

			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 2), strTemp) ;

			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 3), strTemp) ;

			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 4), strTemp) ;
			
			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 5), strTemp) ;
			
			// Added by 224 (2014/01/08) : Factor, Offset, 연산값 적용
			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 6), strTemp) ;
			
			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 7), strTemp) ;

			INT nValue    = atoi(m_wndTxGrid.GetValueRowCol(nRow, 5)) ;
			FLOAT fFactor = atof(m_wndTxGrid.GetValueRowCol(nRow, 6)) ;
			FLOAT fOffset = atof(m_wndTxGrid.GetValueRowCol(nRow, 7)) ;
			
			strTemp.Format("%.5f", nValue * fFactor + fOffset) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 8), strTemp) ;

			
		}
	}
	aFile.ReadString(strData);	// skip


	// Schedule........................
//	nPos = strData.Find("Schedule", 0);
	// .1. 현재 grid 의 모든 항목 삭제
	if (m_wndScheduleGrid.GetRowCount() > 0)
	{
		m_wndScheduleGrid.RemoveRows(1, m_wndScheduleGrid.GetRowCount()) ;
	}

	aFile.ReadString(strData);	// schedule		
	if (strData.Find("Schedule") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		
		while (aFile.ReadString(strData))	// schedule items
		{
			if (strData == "")
				break;
			
			CString str, strTemp ;

			INT nRow = m_wndScheduleGrid.GetRowCount() + 1;
			m_wndScheduleGrid.InsertRows(nRow, 1);
			
			// ReqID
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndScheduleGrid.SetValueRange(CGXRange(nRow, 1), strTemp) ;
						
			// Frame name
			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndScheduleGrid.SetValueRange(CGXRange(nRow, 2), strTemp) ;
			
			// Delay (ms)
			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndScheduleGrid.SetValueRange(CGXRange(nRow, 3), strTemp) ;
			
			nRow++ ;
		}
		
	}

	aFile.ReadString(strData) ;


	// LIN Map ......................................
	aFile.ReadString(strData) ; // title skipping..

	nPos = 0;
	while (aFile.ReadString(strData))
	{
		CString str, strTemp ;
		if(strData == "")
			break;

		INT nRow = m_wndLinGrid.GetRowCount() + 1;
		m_wndLinGrid.InsertRows(nRow, 1);


		// Frame ID
		int p0, p1 = 0;
		p0 = strData.Find(",", p1);		
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FRAMEID), strTemp) ;

		// Frame Name
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FRAMENAME), strTemp);

		// Items
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_ITEMS), strTemp);

		// StartBit
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_STARTBIT), strTemp) ;

		// BitCount
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_BITCOUNTS), strTemp) ;

		// DataType
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		INT nType = atoi(strTemp) ;
		CString strType = (nType == 0) ? "Unsigned" : "Signed" ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_DATATYPE),  strType) ;

		// Factor
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;

		CString strFloat ;
		strFloat.Format("%.5f", atof(strTemp)) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FACTOR),    strFloat) ;

		
		// Offset
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_OFFSET),    strFloat) ;
		
		// 안전상한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FAULT_UPPER),    strFloat) ;
		
		// 안전하한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FAULT_LOWER),    strFloat) ;
		
		// 종료상한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_END_UPPER),    strFloat) ;
		
		// 종료하한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_END_LOWER),    strFloat) ;
		
		// 분류값 1
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		INT nValue ;
		nValue = (strTemp.IsEmpty()) ? 0 : atoi(strTemp) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FUNCTION_DIVISION),  Fun_FindCanName(nValue)) ;
		
		// 분류값 2
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		nValue = (strTemp.IsEmpty()) ? 0 : atoi(strTemp) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FUNCTION_DIVISION2),  Fun_FindCanName(nValue)) ;
		
		// 분류값 3
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		nValue = (strTemp.IsEmpty()) ? 0 : atoi(strTemp) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_FUNCTION_DIVISION3),  Fun_FindCanName(nValue)) ;
		
		// Descript
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndLinGrid.SetValueRange(CGXRange(nRow, LIN_COL_DESC), strTemp) ;
		
	}	
	

	aFile.Close();

	UpdateData(FALSE);
	
	return TRUE;
	
}

BOOL CLinConvDlg::SaveLinConfig(CString strSavePath)
{
	CFile file;
	CString strData,strSaveData;
	CString strTitle, strTitle2, strTitle3 ;

	int i = 0;
	strSaveData.Empty();

	//Start Save Master/////////////////////////////////////////////////////////////////////////

	TRY
	{
		if (strSavePath.Find('.') < 0)
		{
			strSavePath += ".csv";
		}

		if (file.Open(strSavePath, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			//AfxMessageBox("파일을 생성할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("LinConvDlg_SaveLinConfig_msg1","IDD_LIN_CONV"));//&&
			return FALSE;
		}

		// title 1
		//--------------------------------------------------
		strTitle = "**Data Info**\r\nTYPE,VERSION,DESCRIPT\r\n";
		file.Write(strTitle, strTitle.GetLength());

		strData.Format("LIN,4,PNE LIN CONFIG FILE,\r\n\r\n");
		file.Write(strData, strData.GetLength());

		// title 2
		//--------------------------------------------------
		strTitle = "**LIN-CAN Configuration**\r\nLIN BaudRate,CAN BaudRate,Ext ID\r\n";
		file.Write(strTitle, strTitle.GetLength());

		INT nLinBaud = m_ctrlLinSpeed.GetItemData(m_ctrlLinSpeed.GetCurSel()) ;
		INT nCanBaud = canspeed[m_CAN_Baudrate.GetCurSel()] ;

		// TX 값 가져오기 가져오기		

		strData.Format("%d,%d,%d,\r\n\r\n"
					 , nLinBaud
					 , nCanBaud
					 , m_bCheckExtMode
					 );

		file.Write(strData, strData.GetLength()); //


		// title 3
		//--------------------------------------------------
//		strTitle = "**LIN ID Configuration**\r\nLINID(hex),TXD0,TXD1,TXD2,TXD3,TXD4,TXD5,TXD6,TXD7\r\n";
		strTitle = "**LIN ID Configuration**\r\nLINID(hex),Name,StartBit,BitCount,설정값,Factor,Offset\r\n";
		//strTitle = Fun_FindMsg("LinConvDlg_SaveLinConfig_msg2","IDD_LIN_CONV");//&&
		file.Write(strTitle, strTitle.GetLength());


		// Commented by 224 (2014/11/21) : Tx 값을 사용자 입력으로 수정함 
		for (int nRow = 1; nRow < m_wndTxGrid.GetRowCount()+1; nRow++)
		{
			INT nCurrAddr   = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 1)) ;
			CString strName = m_wndTxGrid.GetValueRowCol(nRow, 2) ;
			int nStartBit   = atoi(m_wndTxGrid.GetValueRowCol(nRow, 3)) ;
			int nBitCounts  = atoi(m_wndTxGrid.GetValueRowCol(nRow, 4)) ;
			int nValue      = atoi(m_wndTxGrid.GetValueRowCol(nRow, 5)) ;
			FLOAT fFactor   = atof(m_wndTxGrid.GetValueRowCol(nRow, 6)) ;
			FLOAT fOffset   = atof(m_wndTxGrid.GetValueRowCol(nRow, 7)) ;

			strData.Format("%02x,%s,%d,%d,%d,%.5f,%.5f,\r\n"
						 , nCurrAddr
						 , strName
						 , nStartBit
						 , nBitCounts
						 , nValue
						 , fFactor
						 , fOffset) ;

			file.Write(strData, strData.GetLength());
		}

		strData = "\r\n\r\n" ;
		file.Write(strData, strData.GetLength());
/*
		CArray<PLINID_INFO, PLINID_INFO> linid_array ;
		GetLinIDInfo(linid_array) ;

		for (int nLoop = 0; nLoop < linid_array.GetSize(); nLoop++)
		{
			LINID_INFO* ptr = linid_array.GetAt(nLoop) ;
// 			TRACE("id:%d | delay: %d\n", ptr->id, ptr->delay) ;
			
			// 현재 전시된 그리드의 항목을 모두 저장한다.			
			strData.Format("%x,%x,%x,%x,%x,%x,%x,%x,%x,\r\n"
				, ptr->id, ptr->data.d0, ptr->data.d1, ptr->data.d2, ptr->data.d3, ptr->data.d4, ptr->data.d5, ptr->data.d6, ptr->data.d7) ;
			
			file.Write(strData, strData.GetLength());
		}
		
		strData = "\r\n\r\n" ;
		file.Write(strData, strData.GetLength());

		// clearing
		for(int nLoop = 0; nLoop < linid_array.GetSize(); nLoop++)
		{
			LINID_INFO* ptr = linid_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		linid_array.RemoveAll() ;
*/

		// title 4
		//--------------------------------------------------
		strTitle = "**Schedule Configuration**\r\nReqID(hex),Frame name,Delay(ms)\r\n";
		file.Write(strTitle, strTitle.GetLength());

		CArray<PSCHEDULER_INFO, PSCHEDULER_INFO> sched_array ;
		GetScheduleInfo(sched_array) ;
		
		for (int nLoop = 0; nLoop < sched_array.GetSize(); nLoop++)
		{
			SCHEDULER_INFO* ptr = sched_array.GetAt(nLoop) ;
			TRACE("id:%d | delay: %d\n", ptr->id, ptr->delay) ;
			
			// 현재 전시된 그리드의 항목을 모두 저장한다.			
			strData.Format("%x,%s,%d,\r\n"
						 , ptr->id
						 , ptr->framename
						 , ptr->delay) ;
			
			file.Write(strData, strData.GetLength());
		}

		strData = "\r\n\r\n" ;
		file.Write(strData, strData.GetLength());

		// clearing
		for(int nLoop = 0; nLoop < sched_array.GetSize(); nLoop++)
		{
			SCHEDULER_INFO* ptr = sched_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		sched_array.RemoveAll() ;

		// title 5
		//--------------------------------------------------
		strTitle = "Frame ID,Frame Name,Items,StartBit,BitCount,DataType,Factor,Offset,안전상한,안전하한,종료상한,종료하한,분류값 1,분류값 2,분류값 3,Desc,\r\n";
		//strTitle = Fun_FindMsg("LinConvDlg_SaveLinConfig_msg3","IDD_LIN_CONV");//&& 
		file.Write(strTitle, strTitle.GetLength());	//Title 

		// Grid 의 설정된 값들을 가져온다.
		CArray<PLCC_INFO, PLCC_INFO> lcc_array ;
		GetLinConvInfo(lcc_array) ;
		
		for(int nLoop = 0; nLoop < lcc_array.GetSize(); nLoop++)
		{
			LCC_INFO* ptr = lcc_array.GetAt(nLoop) ;
			TRACE("name:%s | datatype: %d | canid: %x | s: %02d | b: %2d | f: %.2f | off: %.2f | fup: %.2f | flo: %.2f | eup: %.2f | elo: %.2f \n"
				, ptr->item
				, ptr->datatype
				, ptr->canid
				, ptr->can_startbit
				, ptr->can_bitcounts
				, ptr->factor
				, ptr->offset
				, ptr->fault_upper
				, ptr->fault_lower
				, ptr->end_upper
				, ptr->end_lower) ;

			// 현재 전시된 그리드의 항목을 모두 저장한다.

			// 문자열내에 ', "" 를 교체
			CString name = ptr->item ;
			name.Replace("'", "''") ;
			
			CString desc = ptr->desc ;
			desc.Replace("'", "''") ;

			strData.Format("%x,%s,%s,%d,%d,%d,%f,%f,%f,%f,%f,%f,%d,%d,%d,%s,\r\n"
				  , ptr->canid, ptr->framename, ptr->item, ptr->can_startbit, ptr->can_bitcounts, ptr->datatype, ptr->factor
				  , ptr->offset, ptr->fault_upper, ptr->fault_lower, ptr->end_upper, ptr->end_lower
				  , ptr->function_division, ptr->function_division2, ptr->function_division3, desc) ;

			file.Write(strData, strData.GetLength());
		}


		// clearing
		for(int nLoop = 0; nLoop < lcc_array.GetSize(); nLoop++)
		{
			LCC_INFO* ptr = lcc_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		lcc_array.RemoveAll() ;
		//---------------------------------------------------------------------

	}
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
	
	file.Close();

	return TRUE;
}

void CLinConvDlg::OnTransCanData() 
{
	// Grid 내의 Map 값 Valid 체크
	if (!CheckMapValues())
	{
		return ;
	}
	
	// Grid 의 설정된 값들을 가져온다.
	CArray<PLCC_INFO, PLCC_INFO> lcc_array ;
	GetLinConvInfo(lcc_array) ;

	
	//-----------------------------------------------------------------------
	// CAN Receive 설정 함수 호출
	//-----------------------------------------------------------------------
	
	if (!SaveCANConfig(lcc_array))
	{
		TRACE("SaveCANConfig return FALSE\n") ;
		return ;
	}
	//-----------------------------------------------------------------------
	// Runtime 변환 단위테스트
	//-----------------------------------------------------------------------
	
	
	// clearing
	for (INT nLoop = 0; nLoop < lcc_array.GetSize(); nLoop++)
	{
		LCC_INFO* ptr = lcc_array.GetAt(nLoop) ;
		delete ptr ;
	}
	lcc_array.RemoveAll() ;
	
	POSITION pos = m_pDoc->GetFirstViewPosition();
	CCTSMonProView* pView = (CCTSMonProView *)m_pDoc->GetNextView(pos);
	
	pView->m_wndCanList.DeleteAllItems();	
	
	// 적용 시 버튼은 비활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;
	
	
	// CAN 적용과 동시에 모드버스 종료-시작을 호출한다.
//	PostMessage(WM_COMMAND, IDC_OPER_STOP) ;
	SendMessage(WM_COMMAND, IDC_OPER_STOP) ;
	PostMessage(WM_COMMAND, IDC_OPER_START) ;
}

BOOL CLinConvDlg::CheckMapValues() 
{
	// 체크항목
	// 1. 인덱스의 행이 감소하면 안된다.
	// 2. 인덱스가 0이거나 이하이면 안된다.
	// 3. 동일 인덱스의 누적 bitcount 가 16을 넘을 수 없다.
	// 4. ITEMS 의 이름이 있어야 한다..
	
	INT nLatestAddr = -1 ;
	INT nAccCounts = 0 ;

	// RX ------------------------------------------------------------
	for (int nRow = 1; nRow < m_wndLinGrid.GetRowCount()+1; nRow++)
	{		
//		INT nCurrAddr  = atoi(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FRAMEID)) ;
		INT nCurrAddr    = HexStr2Dec(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_FRAMEID));
//		INT nStartBit  = atoi(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_STARTBIT)) ;
		INT nBitcounts = atoi(m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_BITCOUNTS)) ;
		
		if (nLatestAddr > nCurrAddr)
		{
			// 1. 인덱스의 행이 감소하면 안된다.
			CString str ;
			//str.Format("RX %d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("LinConvDlg_CheckMapValues_msg1","IDD_LIN_CONV"), nRow) ;//&&
			//MessageBox("인덱스값은 감소할 수 없습니다.", str) ;
			MessageBox(Fun_FindMsg("LinConvDlg_CheckMapValues_msg2","IDD_LIN_CONV"), str) ;//&&
			
			return FALSE ;
		}
// 		else if (nCurrAddr > nLatestAddr)
// 		{
// 			nLatestAddr = nCurrAddr ;
// 			nAccCounts = 0 ;
// 		}
// 		if (nLatestAddr == nCurrAddr)
// 		{
// 			// 3. 동일 인덱스의 누적 bitcount 가 16을 넘을 수 없다.
// 			if (nAccCounts + nBitcounts > 16)
// 			{
// 				CString str ;
// 				str.Format("%d 번째행 이상", nRow) ;
// 				MessageBox("동일 인덱스의 누적 bitcount 가 16을 넘을 수 없습니다.", str) ;
// 				
// 				return FALSE ;
// 			}
// 		}
		
		if (nCurrAddr <= 0)
		{
			// 2. 인덱스가 0이거나 이하이면 안된다.
			CString str ;
			//str.Format("RX %d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("LinConvDlg_CheckMapValues_msg3","IDD_LIN_CONV"), nRow) ;//&&
			//MessageBox("인덱스값은 0이거나 이하일 수 없습니다.", str) ;
			MessageBox(Fun_FindMsg("LinConvDlg_CheckMapValues_msg4","IDD_LIN_CONV"), str) ;//&&
			
			return FALSE ;
		}
		
		// . 누적 bitcount 보관
		nAccCounts += nBitcounts ;
		
		CString strItems = m_wndLinGrid.GetValueRowCol(nRow, LIN_COL_ITEMS) ;
		if (strItems.IsEmpty())
		{
			// 4. ITEMS 의 이름이 있어야 한다.
			CString str ;
			//str.Format("RX%d 행의 ITEMS 명 이상", nRow) ;
			str.Format(Fun_FindMsg("LinConvDlg_CheckMapValues_msg5","IDD_LIN_CONV"), nRow) ;//&&
			//MessageBox(str, "ITEMS 의 이름이 있어야 합니다.") ;
			MessageBox(str, Fun_FindMsg("LinConvDlg_CheckMapValues_msg6","IDD_LIN_CONV")) ;//&&
			
			return FALSE ;
			
		}
		
		nLatestAddr = nCurrAddr ;
		//		(CString("Unsign") == m_wndLinGrid.GetValueRowCol(nRow, COL_DATATYPE)) ? 0 : 1 ;
	}


	// TX ------------------------------------------------------------
	nLatestAddr = -1 ;
	nAccCounts = 0 ;
	for (int nRow = 1; nRow < m_wndTxGrid.GetRowCount()+1; nRow++)
	{
		INT nCurrAddr  = HexStr2Dec(m_wndTxGrid.GetValueRowCol(nRow, 1)) ;
		int nStartBit = atoi(m_wndTxGrid.GetValueRowCol(nRow, 3)) ;
		int nBitCounts = atoi(m_wndTxGrid.GetValueRowCol(nRow, 4)) ;
		int nValue = atoi(m_wndTxGrid.GetValueRowCol(nRow, 5)) ;
		
		if (nLatestAddr > nCurrAddr)
		{
			// 1. 인덱스의 행이 감소하면 안된다.
			CString str ;
			//str.Format("TX %d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("LinConvDlg_CheckMapValues_msg7","IDD_LIN_CONV"), nRow) ;//&&
			//MessageBox("인덱스값은 감소할 수 없습니다.", str) ;
			MessageBox(Fun_FindMsg("LinConvDlg_CheckMapValues_msg8","IDD_LIN_CONV"), str) ;//&&
			
			return FALSE ;
		}
		else if (nCurrAddr > nLatestAddr)
		{
			nLatestAddr = nCurrAddr ;
			nAccCounts = 0 ;
		}

		if (nCurrAddr <= 0)
		{
			// 2. 인덱스가 0이거나 이하이면 안된다.
			CString str ;
			//str.Format("TX %d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("LinConvDlg_CheckMapValues_msg9","IDD_LIN_CONV"), nRow) ;//&& 
			//MessageBox("인덱스값은 0이거나 이하일 수 없습니다.", str) ;
			MessageBox(Fun_FindMsg("LinConvDlg_CheckMapValues_msg10","IDD_LIN_CONV"), str) ;//&&
			
			return FALSE ;
		}

		if (nStartBit + nBitCounts > 64 || nStartBit < 0 || nBitCounts <= 0 || nValue >= pow((double)2, (double)nBitCounts))
		{
			// 번지수를 넘어가는 경우는 skip 한다.
			// 실제값의 필요공간이 저장공간보다 큰경우 skip 한다.
			// bitcount 는 2의 자승이다.
			CString str ;
			//str.Format("TX %d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("LinConvDlg_CheckMapValues_msg11","IDD_LIN_CONV"), nRow) ;//&&
			//MessageBox("StartBit, BitCount 값 이상 또는 자릿수보다 큰 설정값 입니다.", str) ;
			MessageBox(Fun_FindMsg("LinConvDlg_CheckMapValues_msg12","IDD_LIN_CONV"), str) ;//&&
			
			return FALSE ;
		}
	}

	return TRUE ;
}

// SBC 에 CAN 설정값을 전달하는 함수
BOOL CLinConvDlg::SaveCANConfig(CArray<PLCC_INFO, PLCC_INFO>& array)
{
	CString strTemp,strCode;
//	int		nLength;
	int i;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if (pMD == NULL)
	{
		return FALSE ;
	}

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		return FALSE ;
	}

	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END	)
	{
		//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("LinConvDlg_SaveCANConfig_msg1","IDD_LIN_CONV"));//&&
		return FALSE ;
	}

// 	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
// 	{
// 		AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
// 		return;
// 	}

	// 설정 변수값을 가져온다.
	UpdateData();

	if ((array.GetSize()) > 256)
	{
		//AfxMessageBox("CAN 설정은 256개를 초과 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("LinConvDlg_SaveCANConfig_msg2","IDD_LIN_CONV"));//&&
		return FALSE ;
	}

	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_DATA commonData;
	ZeroMemory(&commonData, sizeof(SFT_CAN_COMMON_DATA));
	
	commonData.can_baudrate = m_CAN_Baudrate.GetCurSel();
	commonData.extended_id	= m_bCheckExtMode ;
//	commonData.fCell_cv	= m_fCV;
	commonData.controller_canID = strtoul("0", (char **)NULL, 16);
	commonData.bms_type     = 0 ;
	commonData.bms_sjw      = 0 ;

	
	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_MASTER);
	memcpy(&canSetData.canCommonData, &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008
	
//	int nSelectIndex=0;	//ljb 2011222 이재복 //////////

	SFT_CAN_SET_DATA canData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);

	for( int i = 0; i < array.GetSize(); i++)
	{
		LCC_INFO* ptr = array.GetAt(i) ;

		canData[i].canID      = ptr->canid ;
		// Commented by 224 (2014/07/04) : 김재호 과장의 요청에 의하여, 모드버스 포트는 슬레이브로 설정한다.
//		canData[i].canType    = PS_CAN_TYPE_MASTER;
		canData[i].canType    = PS_CAN_TYPE_SLAVE;
		strcpy(canData[i].name, ptr->item);		
		canData[i].startBit   = ptr->can_startbit ;
		canData[i].bitCount   = ptr->can_bitcounts ;
		canData[i].byte_order = 0;		
		
		canData[i].data_type = ptr->datatype ;
		canData[i].factor_multiply = ptr->factor ;
		canData[i].factor_Offset = ptr->offset ;
		
		canData[i].fault_upper    = ptr->fault_upper ;
		canData[i].fault_lower    = ptr->fault_lower ;
		canData[i].end_upper      = ptr->end_upper ;
		canData[i].end_lower      = ptr->end_lower ;
		canData[i].default_fValue = 0.0f ;
		canData[i].sentTime       = 0.0f ;

		canData[i].function_division  = ptr->function_division ;
		canData[i].function_division2 = ptr->function_division2 ;
		canData[i].function_division3 = ptr->function_division3 ;
// 		canData[i].startBit2          = 0 ;
// 		canData[i].bitCount2          = 0 ;
// 		canData[i].byte_order2        = 0 ;		
// 		canData[i].data_type2         = 0 ;
// 		canData[i].default_fValue2    = 0 ;
		//****************************************************************************************

	}

	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_SLAVE);
	memcpy(&canSetData.canCommonData[1], &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008
	
	pMD->SetCANSetData(nSelCh, canData, array.GetSize());
	memcpy(&canSetData.canSetData, &canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);	//ljb 201008

	//SFT_RCV_CMD_CAN_SET canSetData;
	//ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CAN_SET));
// 	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
// 	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));
	
// 	SFT_MD_CAN_INFO_DATA * canInforData = pMD->GetCanAllData();
// 	memcpy(&canSetData.canCommonData, pMD->GetCANCommonData(), sizeof(canSetData.canCommonData));
// 	memcpy(&canSetData.canSetData, canInforData->canSetAllData.canSetData, sizeof(canSetData.canSetData));
	

	TRACE("CAN commdata Size = %d, size data = %d, total size = %d \n",sizeof(canSetData.canCommonData[0]),sizeof(canSetData.canSetData[0]), sizeof(canSetData));
	if(int nRth = m_pDoc->SetCANDataToModule(nSelModule, &canSetData, nSelCh) == FALSE)
	{
		//AfxMessageBox("CAN 정보 설정에 실패했습니다.");
		AfxMessageBox(Fun_FindMsg("LinConvDlg_SaveCANConfig_msg3","IDD_LIN_CONV"));//&&
		return FALSE ;
	}

//	IsChange = FALSE;
//	GetDlgItem(IDOK)->EnableWindow(IsChange);

	pMD->SaveCanConfig();

	
	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;

	return TRUE ;
}

CString CLinConvDlg::Fun_FindCanName(int nDivision)
{
	CString strName;
	int nSelectPos, nFindPos;
	for (int i=0; i < m_uiArryCanCode.GetSize() ; i++)
	{
		if (nDivision == m_uiArryCanCode.GetAt(i))
		{
			strName.Format("%s [%d]", m_strArryCanName.GetAt(i), nDivision);
			return strName;
		}
	}
	strName.Format("None [%d]", nDivision);
	return strName;
}

void CLinConvDlg::OnSaveConfig() 
{
	UpdateData() ;

	// Grid 내의 Map 값 Valid 체크
	if (!CheckMapValues())
	{
		return ;
	}


#if 1
	
	UpdateData();
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\DataBase\\Config", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", "csv", "csv");
		
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if (IDOK == pDlg.DoModal())
	{
		if (SaveLinConfig(pDlg.GetPathName()) == TRUE)
		{
//			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}
	
#else

	CLinSettingDBDialog dlg ; 
	dlg.m_strConvDBFilename = m_strConvDBFilename ;
	dlg.m_bSaveMode = TRUE ;
	dlg.m_strName = m_strDBName ;
	if (IDOK != dlg.DoModal())
	{
		return ;
	}
	
	// modbus config 가져오기
	m_strDBName = dlg.m_strName ;
	CString strDesc = dlg.m_strDesc ;
	m_strDBName.TrimLeft() ; m_strDBName.TrimRight() ;

	// LIN
//	m_nLinID ;
	INT nLinBaud = m_ctrlLinSpeed.GetItemData(m_ctrlLinSpeed.GetCurSel()) ;

	// CAN
	INT nCanBaud = canspeed[m_CAN_Baudrate.GetCurSel()] ;
	m_bCheckExtMode ;
	
	// TX 값 가져오기 가져오기

	INT arrTx[8] ;
	for (int nIdx = 0; nIdx < 8; nIdx++)
	{
		arrTx[nIdx] = atoi(m_wndTxGrid.GetValueRowCol(1, nIdx+1)) ;
	}

    try
    {
		CppSQLite3DB db;
		db.open(m_strConvDBFilename) ;
		

		if (!db.tableExists("TB_LIN_CONV_LIST") || !db.tableExists("TB_LIN_MAP") || !db.tableExists("TB_LIN_SCHEDULE"))
		{
			//MessageBox("잘못된 db 파일입니다") ;
			MessageBox(Fun_FindMsg("LinConvDlg_SaveCANConfig_msg4")) ;//&&
			db.close() ;
			return ;
		}

		//---------------------------------------------------------------------

		// 사용자가 설정한 자료가 기존자료인지 신규자료인지 체크
//		CString strQuery ;
		char csQuery[512*2] ;
		sprintf(csQuery, "SELECT COUNT(*) FROM TB_LIN_CONV_LIST WHERE NAME = '%s'", m_strDBName) ;
		BOOL bUpdate = db.execScalar(csQuery) ;

		// transaction 시작
//		db.execDML("begin transaction") ;

		//---------------------------------------------------------------------

		// 현재 통신설정값을 TB_CONV_LIST 에 저장한다.
		char buf[512*2] ;

		if (bUpdate)
		{
			sprintf(buf
				  , "UPDATE TB_LIN_CONV_LIST   "
				    "   SET LINID       = %d   "
					"     , LINBAUDRATE = %d   "
					"     , CANBAUDRATE = %d   "
					"     , CANEXTENDED = %d   "
					"     , TXD0        = %d   "
					"     , TXD1        = %d   "
					"     , TXD2        = %d   "
					"     , TXD3        = %d   "
					"     , TXD4        = %d   "
					"     , TXD5        = %d   "
					"     , TXD6        = %d   "
					"     , TXD7        = %d   "
					"     , DESC        = '%s' "
					" WHERE NAME = '%s'        "
				  , 0, nLinBaud, nCanBaud , m_bCheckExtMode
				  , arrTx[0], arrTx[1], arrTx[2], arrTx[3], arrTx[4], arrTx[5], arrTx[6], arrTx[7]
				  , strDesc, m_strDBName) ;
		}
		else
		{
			sprintf(buf
				  , "INSERT INTO TB_LIN_CONV_LIST (NAME, LINID, LINBAUDRATE, CANBAUDRATE, CANEXTENDED, TXD0, TXD1, TXD2, TXD3, TXD4, TXD5, TXD6, TXD7, DESC) "
				    "VALUES ('%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s') ;"
				  , m_strDBName, 0, nLinBaud, nCanBaud, m_bCheckExtMode
				  , arrTx[0], arrTx[1], arrTx[2], arrTx[3], arrTx[4], arrTx[5], arrTx[6], arrTx[7]
				  , strDesc) ;
		}
		db.execDML(buf) ;

		// 현재 입력한 이름의 SEQ의 값을 얻는다.
		sprintf(csQuery
			  , "SELECT SEQ FROM TB_LIN_CONV_LIST WHERE NAME = '%s'"
			  , m_strDBName) ;
		CppSQLite3Query q = db.execQuery(csQuery) ;
		ASSERT(!q.eof()) ;

		CString strSEQ = q.fieldValue("SEQ") ;
		q.finalize();

		//---------------------------------------------------------------------

		// 기존에 설정되어 있던 동일한 TB_MODBUS_MAP 의 PARENTCD 항목은 모두 삭제한다.
		sprintf(buf
			  , "DELETE FROM TB_LIN_MAP WHERE PARENTCD = %s"
			  , strSEQ) ;
		db.execDML(buf) ;


		// Grid 의 설정된 값들을 가져온다.
		CArray<PLCC_INFO, PLCC_INFO> lcc_array ;
		GetLinConvInfo(lcc_array) ;

		for (INT nLoop = 0; nLoop < lcc_array.GetSize(); nLoop++)
		{
			LCC_INFO* ptr = lcc_array.GetAt(nLoop) ;
			TRACE("name:%s | datatype: %d | canid: %03d | s: %02d | b: %2d | f: %.2f | off: %.2f | fup: %.2f | flo: %.2f | eup: %.2f | elo: %.2f \n"
				, ptr->item
				, ptr->datatype
				, ptr->canid
				, ptr->can_startbit
				, ptr->can_bitcounts
				, ptr->factor
				, ptr->offset
				, ptr->fault_upper
				, ptr->fault_lower
				, ptr->end_upper
				, ptr->end_lower) ;

			// 현재 전시된 그리드의 항목을 모두 저장한다.

			// 문자열내에 ', "" 를 교체
			CString name = ptr->item ;
			name.Replace("'", "''") ;
			
			CString desc = ptr->desc ;
			desc.Replace("'", "''") ;

			sprintf(buf
				  , "INSERT INTO TB_LIN_MAP (PARENTCD, SEQ, FRAMEID, FRAMENAME, ITEMS, STARTBIT, BITCOUNT, DATATYPE, FACTOR, OFFSET, FAULT_UPPER, FAULT_LOWER, END_UPPER, END_LOWER, FUNC_DIVISION, FUNC_DIVISION2, FUNC_DIVISION3, DESC) "
				    "                   VALUES (%s, %d, %d, '%s', '%s', %d, %d, %d, %f, %f, %f, %f, %f, %f, %d, %d, %d, '%s')  ;"
				  , strSEQ, nLoop, ptr->canid, ptr->framename, ptr->item, ptr->can_startbit, ptr->can_bitcounts, ptr->datatype, ptr->factor
				  , ptr->offset, ptr->fault_upper, ptr->fault_lower, ptr->end_upper, ptr->end_lower
				  , ptr->function_division, ptr->function_division2, ptr->function_division3, desc) ;

//			TRACE("%s\n", buf) ;

			db.execDML(buf) ;
		}


		// clearing
		for(int nLoop = 0; nLoop < lcc_array.GetSize(); nLoop++)
		{
			LCC_INFO* ptr = lcc_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		lcc_array.RemoveAll() ;
		//---------------------------------------------------------------------

		sprintf(buf
			, "DELETE FROM TB_LIN_SCHEDULE WHERE PARENTCD = %s"
			, strSEQ) ;
		db.execDML(buf) ;
		

		CArray<PSCHEDULER_INFO, PSCHEDULER_INFO> sched_array ;
		GetScheduleInfo(sched_array) ;
		
		for(int nLoop = 0; nLoop < sched_array.GetSize(); nLoop++)
		{
			SCHEDULER_INFO* ptr = sched_array.GetAt(nLoop) ;
			TRACE("id:%d | delay: %d\n", ptr->id, ptr->delay) ;
			
			// 현재 전시된 그리드의 항목을 모두 저장한다.			
			sprintf(buf
				  , "INSERT INTO TB_LIN_SCHEDULE (PARENTCD, SEQ, FRAMEID, FRAMENAME, DELAY) "
				    "                   VALUES (%s, %d, %d, '%s', %d)  ;"
				  , strSEQ, nLoop, ptr->id, ptr->framename, ptr->delay) ;

//			TRACE("%s\n", buf) ;

			db.execDML(buf) ;
		}


		// clearing
		for(int nLoop = 0; nLoop < sched_array.GetSize(); nLoop++)
		{
			SCHEDULER_INFO* ptr = sched_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		sched_array.RemoveAll() ;
	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		MessageBox(e.errorMessage()) ;
    }

#endif

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;

}

void CLinConvDlg::OnAddRow() 
{
	CRowColArray ra;

	if (m_nTabSelection == TAB_RX)
	{
		m_wndLinGrid.GetSelectedRows(ra);
		if(ra.GetSize() > 0)
			m_wndLinGrid.InsertRows(ra[0] + 1, 1);
		else
			m_wndLinGrid.InsertRows(m_wndLinGrid.GetRowCount()+1, 1);
		
	}
	else if (m_nTabSelection == TAB_TX)
	{
		m_wndTxGrid.GetSelectedRows(ra) ;
		if(ra.GetSize() > 0)
			m_wndTxGrid.InsertRows(ra[0] + 1, 1);
		else
			m_wndTxGrid.InsertRows(m_wndTxGrid.GetRowCount()+1, 1);

//		m_wndTxGrid.()
	}
	else
	{
		ASSERT(FALSE) ;
	}

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;	
}

void CLinConvDlg::OnDelRow() 
{
	CRowColArray ra;
	if (m_nTabSelection == TAB_RX)
	{
		m_wndLinGrid.GetSelectedRows(ra);
		
		for (int i = ra.GetSize()-1; i >=0 ; i--)
		{
			if(ra[i] == 0) //Header 삭제불가
				continue;
			m_wndLinGrid.RemoveRows(ra[i], ra[i]);
		}
		
	}
	else if (m_nTabSelection == TAB_TX)
	{
		m_wndTxGrid.GetSelectedRows(ra);
		
		for (int i = ra.GetSize()-1; i >=0 ; i--)
		{
			if(ra[i] == 0) //Header 삭제불가
				continue;
			m_wndTxGrid.RemoveRows(ra[i], ra[i]);
		}		
	}
	else
	{
		ASSERT(FALSE) ;
	}

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;
}

void CLinConvDlg::OnAddScheRow() 
{
	CRowColArray ra;
	m_wndScheduleGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndScheduleGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndScheduleGrid.InsertRows(m_wndScheduleGrid.GetRowCount()+1, 1);
	
}

void CLinConvDlg::OnDelScheRow() 
{
	CRowColArray ra;
	m_wndScheduleGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndScheduleGrid.RemoveRows(ra[i], ra[i]);
	}
	
}

void CLinConvDlg::OnSelchangeParamTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nTabNum = m_ctrlParamTab.GetCurSel();
	TRACE("Tab %d Selected\n", nTabNum);
	
	switch(nTabNum)
	{
		
	case TAB_RX:		//안전 조건 
		Fun_ChangeViewControl(TRUE);
		m_nTabSelection = TAB_RX ;
		break;
		
	case TAB_TX:		//Grading조건
		Fun_ChangeViewControl(FALSE);
		m_nTabSelection = TAB_TX ;
		break;
		
	default:	
		Fun_ChangeViewControl(TRUE);
		m_nTabSelection = TAB_RX ;
	}	
	
	*pResult = 0;
}

LRESULT CLinConvDlg::OnGridBeginEdit(WPARAM wParam, LPARAM lParam)
{
	// 적용 버튼은 활성화 시킨다.
	if ((CMyGridWnd*)lParam == &m_wndLinGrid || (CMyGridWnd*)lParam == &m_wndTxGrid)
	{
		GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;
	}
	return 1;
}

// Added by 224 (2015/01/07)
// Tx 그리드에서 설정값,Factor,Offset이 바뀌면, 연산값을 재 설정한다.
// 연산값 = 설정값 * Factor + Offset
LRESULT CLinConvDlg::OnGridEndEdit(WPARAM wParam, LPARAM lParam)
{	
	if ((CMyGridWnd*)lParam != &m_wndTxGrid)
	{
		return 0;
	}

	ASSERT((CMyGridWnd*)lParam == &m_wndTxGrid) ;

	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);	

	switch (nCol)
	{
	case 5:			// 설정값
	case 6:			// Factor
	case 7:			// Offset
		{
			INT nValue    = atoi(m_wndTxGrid.GetValueRowCol(nRow, 5)) ;
			FLOAT fFactor = atof(m_wndTxGrid.GetValueRowCol(nRow, 6)) ;
			FLOAT fOffset = atof(m_wndTxGrid.GetValueRowCol(nRow, 7)) ;
			
			CString str ;
			str.Format("%.5f", nValue * fFactor + fOffset) ;
			m_wndTxGrid.SetValueRange(CGXRange(nRow, 8), str);
		}
		break ;

	default:
		break ;
	}
	return 1;
}

void CLinConvDlg::OnSysCommand(UINT nID, LPARAM lParam) 
{
	// 종료버튼이 눌리면 다이얼로그를 종료한다.
	if((nID & 0xFFF0) == SC_CLOSE)
	{
		m_bCanRecvRxTxMsg = FALSE ;

		m_pDoc->ClearLinAllShowMessageFlag() ;

		// 쓰레드에서 리스트박스에 쓰는 시간 까지 멈추기
		Sleep(200) ;

		EndDialog(IDCANCEL);
		return ;
	}

	CDialog::OnSysCommand(nID, lParam);

	return;
}