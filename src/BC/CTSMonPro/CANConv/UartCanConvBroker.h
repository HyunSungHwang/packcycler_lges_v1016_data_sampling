// UartCanConvBroker.h: interface for the CUartCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UARTCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_)
#define AFX_UARTCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include "CTSMonProDoc.h"
#include "CanConvBroker.h"
//#include "xlCANFunctions.h"
//#include "xlLINFunctions.h"
//#include "UartFunctions.h"
#include "vxlapi.h"


typedef enum
{
	UCC_PLAY,
	UCC_PAUSE,
	UCC_STOP,
} UCC_MODE ;

typedef struct tagUCC_INFO
{
	CHAR cmd ;				// command
	CHAR item[256] ;	// item
	int cmd_startbit ;
	INT cmd_bitcounts ;
	INT canid ;			// frameid
	INT datatype ;	// Unsign:0, Sign:1,
	INT can_startbit ;
	FLOAT factor ; //OFFSET, SAFEUPPERLIMIT, SAFELOWERLIMIT, TERMUPPERLIMIT, TERMLOWERLIMIT, CAT1, CAT2, CAT3
	FLOAT offset ;
	FLOAT fault_upper ;
	FLOAT fault_lower ;
	FLOAT end_upper ;
	FLOAT end_lower ;
	short int function_division ;
	short int function_division2 ;
	short int function_division3 ;
	CHAR desc[512] ;
} UCC_INFO, *PUCC_INFO ;

typedef struct tagUARTSCHEDULER_INFO
{
	INT id ;
	CHAR framename[256] ;
	INT delay ;
} UARTSCHEDULER_INFO, *PUARTSCHEDULER_INFO ;

/*
typedef struct tagUART_INFO
{
	INT id ;
	union {
		struct
		{
			BYTE d0 ;
			BYTE d1 ;
			BYTE d2 ;
			BYTE d3 ;
			BYTE d4 ;
			BYTE d5 ;
			BYTE d6 ;
			BYTE d7 ;
		};
		
		BYTE value[8];
	} data;
	
} UART_INFO, *PUART_INFO ;
*/

#include "UartFunctions.h"
#include "xlCANFunctions.h"

class CCANFunctions ;
class CUartFunctions ;
class CCTSMonProDoc ;
class CUartCanConvBroker : public CCanConvBroker
{
public:
	CUartCanConvBroker();
	virtual ~CUartCanConvBroker();

	// CAN info
	CCANFunctions* m_pCAN;
	BOOL m_bSendCANData ;
//	BOOL m_bCanRxThreadRun ;
	XLstatus SendCANData(XLevent& xlEvent);

	// Uart info
	CUartFunctions* m_pUart ;	
	BOOL m_bUartRxThreadRun ;
	BOOL m_bUartOnBus ;			// 스케쥴러 쓰레드에서의 진행여부 플래그
//	INT m_nUartID ;

	void DoNotShowMessage() ;
	void StopUartCanConvThread();
	void PauseUartCanConvThread();
// 	BOOL BeginUartCanConvThread(int nDevID, CString strDBName/*, INT nUartID*/, INT nUartChannel, INT nSpeed
// 							 , CArray<PUART_INFO, PUART_INFO>& Uartid
// 							 , CArray<PUCC_INFO, PUCC_INFO>& ucc, CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO>& sched
// 							 , INT nCanChannel, INT nCanBaudrate) ;
	BOOL BeginUartCanConvThread(int nDevID, CString strDBName, INT nComPort, INT nBaudrate, INT nDataBit, INT nParity, INT nStopBit
							 , CArray<PUCC_INFO, PUCC_INFO>& ucc, CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO>& sched
							 , INT nCanChannel, INT nCanBaudrate) ;
	void UartCreateSchedulerThread() ;
	void ProcessUartData(CHAR cmd, CHAR rslt, CHAR* data, INT len) ;
	HANDLE m_hUartSchedulerTerminated ;
//	HANDLE m_hUartRxThreadTerminated ;
//	HANDLE m_hCanRxThreadTerminated ;
	
	CString m_strUartSettingName ;	
	UCC_MODE m_nUartMode ;

	// CUartConvDlg 의 리스트에 전시여부
	CRITICAL_SECTION* m_pcsShowMessage ;
//	BOOL m_bShowMessage ;
	CListBox* m_pRxList ;
	CListBox* m_pTxList ;

//	CArray<PUART_INFO, PUART_INFO> m_Uartid_array ;
	CArray<PUCC_INFO, PUCC_INFO> m_ucc_array ;
	CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO> m_schedule_array ;

	BOOL m_bSaveRxFlag ;
	void SetSaveRxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveRxFlag = b ; }
	
	BOOL m_bSaveTxFlag ;
	void SetSaveTxDataFlag(BOOL b) { ASSERT(this) ; m_bSaveTxFlag = b ; }
	void SetIndex(INT idx) { ASSERT(this) ; m_nIndex = idx ; }

	INT m_nDevID ;

protected:
	// Uart-Can Conv Set ID	- PCI 슬롯번호(?) 최대 4개.
	INT m_nIndex ;

};

DWORD WINAPI UartSchedulerThread( PVOID par );

#endif // !defined(AFX_UartCANCONVBROKER_H__5F2A9404_B163_4BB7_A6F1_FF90490846BE__INCLUDED_)
