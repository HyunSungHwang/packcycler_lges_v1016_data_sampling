// UartConvDlg.cpp : implementation file
//

#include "stdafx.h" 
#include "../CTSMonPro.h"
#include "../CTSMonProView.h"
#include "UartConvDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUartConvDlg dialog

unsigned long uartspeed[] = { 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200 };
unsigned long canspeed2[] = {125000, 250000, 500000, 1000000};
// extern unsigned long canspeed[] ;

/*
int HexStr2Dec(LPCTSTR hexStr)
{
	char ch;
	int  decVal = 0;
	
	sscanf(hexStr, "%s", hexStr);
	for(int  i = 0; hexStr[i]; i++)
	{
		ch = hexStr[i];
		decVal = decVal * 16;   // 자리수가 늘때마다 16을 곱함
		if (ch>='0' && ch<='9') // 0 - 9 사이의 값
		{
			decVal += ch - '0';
		}
		else
		{                  // A - F 사이의 값
			ch = ch & 0xdf; // 소문자를 대문자로 변환
			decVal += ch - 'A' + 10;
		}
	}
	
	return decVal ;
}
*/
int HexStr2Dec(LPCTSTR hexStr) ;

typedef enum
{
	UART_COL_SEQ,
	UART_COL_CMD,
	UART_COL_ITEMS,
	UART_COL_CMD_STARTBIT,
	UART_COL_CMD_BITCOUNTS,
//	UART_COL_FRAMEID,	// COL_ADDR
//	UART_COL_FRAME_STARTBIT,
//	UART_COL_FRAME_BITCOUNTS,
	UART_COL_DATATYPE,
	UART_COL_FACTOR,
	UART_COL_OFFSET,
	UART_COL_FAULT_UPPER,
	UART_COL_FAULT_LOWER,
	UART_COL_END_UPPER,
	UART_COL_END_LOWER,
	UART_COL_FUNCTION_DIVISION,
	UART_COL_FUNCTION_DIVISION2,
	UART_COL_FUNCTION_DIVISION3,
	UART_COL_DESC,
	UART_COL_NUM = UART_COL_DESC,
} UART_COL_TYPE ;

CUartConvDlg::CUartConvDlg(CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CUartConvDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CUartConvDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CUartConvDlg::IDD3):
	(CUartConvDlg::IDD), pParent) 
{
	//{{AFX_DATA_INIT(CUartConvDlg)
	m_bCheckExtMode = FALSE;
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	m_bCanRecvRxTxMsg = FALSE ;

	m_nDevID = -1 ;
}

CUartConvDlg::CUartConvDlg()
{
}

CUartConvDlg::~CUartConvDlg()
{
	m_bCanRecvRxTxMsg = FALSE ;
}

void CUartConvDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUartConvDlg)
	DDX_Control(pDX, IDC_CAN_CHANNEL, m_Channel);
	DDX_Control(pDX, IDC_RX_LIST, m_ctlRxList);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE, m_CAN_Baudrate);
	DDX_Control(pDX, IDC_STOPBIT2, m_ctrlStopbit);
	DDX_Control(pDX, IDC_PARITYBIT2, m_ctrlParitybit);
	DDX_Control(pDX, IDC_DATABITS2, m_ctrlDatabits);
	DDX_Control(pDX, IDC_BAUDRATE, m_ctrlBaudrate);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE, m_bCheckExtMode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUartConvDlg, CDialog)
	//{{AFX_MSG_MAP(CUartConvDlg)
	ON_BN_CLICKED(IDC_ADD_SCHE_ROW, OnAddScheRow)
	ON_BN_CLICKED(IDC_DEL_SCHE_ROW, OnDelScheRow)
	ON_BN_CLICKED(IDC_ADD_ROW, OnAddRow)
	ON_BN_CLICKED(IDC_DEL_ROW, OnDelRow)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_BN_CLICKED(IDC_OPEN_CONV_DATA, OnOpenConvData)
	ON_BN_CLICKED(IDC_SAVE_CONFIG, OnSaveConfig)
	ON_BN_CLICKED(IDC_TRANS_CAN_DATA, OnTransCanData)
	ON_BN_CLICKED(IDC_OPER_START, OnOperStart)
	ON_BN_CLICKED(IDC_OPER_STOP, OnOperStop)
	ON_BN_CLICKED(IDC_ETC_SETTING, OnEtcSetting)
	ON_WM_SYSCOMMAND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUartConvDlg message handlers

BOOL CUartConvDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
/*
	//ljb 2011221 이재복 //////////
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)
		AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");

	m_pDoc->Fun_GetArryCanDivision(m_uiArryCanCode);		//ljb 2011222 이재복 //////////
	m_pDoc->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////
	
	// TODO: Add extra initialization here
	CCyclerModule* pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if (pMD == NULL)	return FALSE;
	
	CCyclerChannel* pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL) return FALSE;
	
	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
		//if(pMD->GetState() == PS_STATE_LINE_OFF)
	{
		//		m_ctrlLabelMsg.SetText("동작중인 채널은 수정할 수 없습니다.");
		//		EnableAllControl(FALSE);
		//return;
	}
*/

	
	// 각 모듈에는 2개의 채널이 붙어 있다고 가정한다.
	m_nDevID = (nSelModule - 1) * 2 + nSelCh ;


	// Can Initialize --------------------------------

	// init the default window
	m_CAN_Baudrate.SetCurSel(2);
	m_Channel.SetCurSel(m_nDevID);

	
	// Can Initialize --------------------------------


	CString str ;
	str.Format("UART COM%d", m_nDevID + 4) ;		// DevID 0번이 COM4으로 설정한다.
	SetDlgItemText(IDC_STATIC_COMPORT, str) ;

	// . 컨트롤 초기화
	m_ctrlBaudrate.SetCurSel(7) ;
	for (int i = 0; i < m_ctrlBaudrate.GetCount(); i++)
	{
		m_ctrlBaudrate.SetItemData(i, uartspeed[i]) ;
	}

	m_ctrlDatabits.SetCurSel(1) ;
	m_ctrlParitybit.SetCurSel(0) ;
	m_ctrlStopbit.SetCurSel(0) ;

	// . 마지막 저장값 불러오기
	UpdateData(FALSE) ;

	// Added by 224 (2014/04/23) : 모드버스 다이얼로그 핸들링을 위한 변수할당
	m_pDoc->m_pUartDlg = this ;
	m_bCanRecvRxTxMsg = TRUE ;


	UCC_MODE nMode = m_pDoc->GetUartMode(m_nDevID) ;
	switch (nMode)
	{
	case UCC_PLAY:
		// 적용 버튼은 비 활성화 시킨다.
		GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;
		
		GetDlgItem(IDC_OPER_START)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_OPER_STOP)->EnableWindow(TRUE) ;
		m_Channel.SetCurSel(m_pDoc->GetUartCanChannel(m_nDevID) - 1);
		
		// 채널 변경 비 활성화
		m_Channel.EnableWindow(FALSE) ;
		
		break ;
	default:
		GetDlgItem(IDC_OPER_START)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_OPER_PAUSE)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_OPER_STOP)->EnableWindow(FALSE) ;
		break ;
	}

	InitGridSchedule() ;
	InitGridRx() ;
	
	

	// 작동중이거나 일시 정지인 경우 설정된 값으로 컨트롤들을 세트한다.
	if (UCC_PLAY == m_pDoc->m_UartCanConv[m_nDevID].m_nUartMode || UCC_PAUSE == m_pDoc->m_UartCanConv[m_nDevID].m_nUartMode)
	{
		//		m_strDBName = m_pDoc->m_LinCanConv[m_nDevID].m_strLinSettingName ;
		//		SetControlsWithDBName(m_strDBName) ;
		
		// Play 중일 때는 빈 Row를 클리어 하고 로드한다
		if (m_wndUartGrid.GetRowCount() > 0)
		{
			m_wndUartGrid.RemoveRows(1, m_wndUartGrid.GetRowCount()) ;
		}
		
		m_strConfigFilename = m_pDoc->m_UartCanConv[m_nDevID].m_strUartSettingName ;
		if (LoadUartConfig(m_strConfigFilename) == TRUE)
		{
			CHAR filename[MAX_FNAME_LEN] ;
			CHAR ext[MAX_FNAME_LEN] ;
			_splitpath(m_strConfigFilename, NULL, NULL, filename, ext) ;
			
			CString strName;
			//strName.Format("Uart 설정 - %s.%s",  filename, ext);
			strName.Format(Fun_FindMsg("UartConvDlg_OnInitDialog_msg1","IDD_UART_CONV"),  filename, ext);//&&
			this->SetWindowText(strName);
		}
		
	}
	
	// 기타설정 문구 초기화
	//SetDlgItemText(IDC_ETC_SETTING, "기타설정 <<") ;
	SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("UartConvDlg_OnInitDialog_msg2","IDD_UART_CONV")) ;//&&
	OnEtcSetting() ;
	
	
	m_pDoc->SetUartShowMessageFlag(m_nDevID, &m_ctlRxList, NULL) ;
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CUartConvDlg::InitGridSchedule()
{
	//-------------------------------------------------------------------------------------
	// scheduler grid 초기화
	m_wndScheduleGrid.SubclassDlgItem(IDC_SCHEDULER_GRID, this);
	m_wndScheduleGrid.Initialize();
	m_wndScheduleGrid.GetParam()->EnableUndo(FALSE);
	
	// grid 컬럼 사이즈 설정
	m_wndScheduleGrid.SetColCount(3);
	m_wndScheduleGrid.SetColWidth(1, 3, 120);
	
	// column header 설정
	m_wndScheduleGrid.SetStyleRange(CGXRange(0, 1),      CGXStyle().SetValue("REQ(hex)"));
	m_wndScheduleGrid.SetStyleRange(CGXRange(0, 2),      CGXStyle().SetValue("Frame name"));
	m_wndScheduleGrid.SetStyleRange(CGXRange(0, 3),      CGXStyle().SetValue("Delay(ms)"));
	m_wndScheduleGrid.SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
	m_wndScheduleGrid.SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
	m_wndScheduleGrid.SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetControl(GX_IDS_CTRL_EDIT));
	
	// 기본 행 설정
	m_wndScheduleGrid.SetRowCount(1);
	
}

void CUartConvDlg::InitGridRx()			
{
	m_wndUartGrid.SubclassDlgItem(IDC_UART_GRID, this);
	m_wndUartGrid.Initialize();

//	CRect rect;
	m_wndUartGrid.GetParam()->EnableUndo(FALSE);
	
	// 기본 행 설정
	m_wndUartGrid.SetRowCount(1);

	// grid 컬럼 사이즈 설정
	m_wndUartGrid.SetColCount(UART_COL_NUM);
//	m_wndUartGrid.GetClientRect(&rect);
	m_wndUartGrid.SetColWidth(UART_COL_SEQ,           UART_COL_SEQ,       30);
	m_wndUartGrid.SetColWidth(UART_COL_CMD,           UART_COL_CMD,       120);
	m_wndUartGrid.SetColWidth(UART_COL_ITEMS,         UART_COL_ITEMS,     120);
	m_wndUartGrid.SetColWidth(UART_COL_CMD_STARTBIT,  UART_COL_CMD_STARTBIT,  60);
	m_wndUartGrid.SetColWidth(UART_COL_CMD_BITCOUNTS, UART_COL_CMD_BITCOUNTS, 60);
//	m_wndUartGrid.SetColWidth(UART_COL_FRAMEID,       UART_COL_FRAMEID,   120);
//	m_wndUartGrid.SetColWidth(UART_COL_FRAME_STARTBIT,  UART_COL_FRAME_STARTBIT,  60);
//	m_wndUartGrid.SetColWidth(UART_COL_FRAME_BITCOUNTS, UART_COL_FRAME_BITCOUNTS, 60);
	m_wndUartGrid.SetColWidth(UART_COL_DATATYPE,      UART_COL_DATATYPE,  60);
	m_wndUartGrid.SetColWidth(UART_COL_FACTOR,        UART_COL_FACTOR,    60);
	m_wndUartGrid.SetColWidth(UART_COL_OFFSET,        UART_COL_OFFSET,    60);
	m_wndUartGrid.SetColWidth(UART_COL_FAULT_UPPER,   UART_COL_FAULT_UPPER, 60); 
	m_wndUartGrid.SetColWidth(UART_COL_FAULT_LOWER,   UART_COL_FAULT_LOWER, 60);
	m_wndUartGrid.SetColWidth(UART_COL_END_UPPER,     UART_COL_END_UPPER, 60);
	m_wndUartGrid.SetColWidth(UART_COL_END_LOWER,     UART_COL_END_LOWER, 60);
	m_wndUartGrid.SetColWidth(UART_COL_FUNCTION_DIVISION,  UART_COL_FUNCTION_DIVISION,  60);
	m_wndUartGrid.SetColWidth(UART_COL_FUNCTION_DIVISION2, UART_COL_FUNCTION_DIVISION2, 60);
	m_wndUartGrid.SetColWidth(UART_COL_FUNCTION_DIVISION3, UART_COL_FUNCTION_DIVISION3, 60);
	m_wndUartGrid.SetColWidth(UART_COL_DESC, UART_COL_DESC, 500);

	// column header 설정
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_CMD),      CGXStyle().SetValue("Resp (HEX)"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_ITEMS),	   CGXStyle().SetValue("Items"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_CMD_STARTBIT),      CGXStyle().SetValue("StartBit"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_CMD_BITCOUNTS),      CGXStyle().SetValue("BitCount"));
//	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FRAMEID),      CGXStyle().SetValue("Frame ID (HEX)"));
//	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FRAME_STARTBIT),  CGXStyle().SetValue("StartBit"));
//	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FRAME_BITCOUNTS), CGXStyle().SetValue("BitCount"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_DATATYPE),  CGXStyle().SetValue("DataType"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FACTOR),    CGXStyle().SetValue("Factor"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_OFFSET),    CGXStyle().SetValue("Offset"));
	/*
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FAULT_UPPER), CGXStyle().SetValue("안전상한"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FAULT_LOWER), CGXStyle().SetValue("안전하한"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_END_UPPER), CGXStyle().SetValue("종료상한"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_END_LOWER), CGXStyle().SetValue("종료하한"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FUNCTION_DIVISION),    CGXStyle().SetValue("분류1"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FUNCTION_DIVISION2),    CGXStyle().SetValue("분류2"));
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FUNCTION_DIVISION3),    CGXStyle().SetValue("분류3"));
	*/
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FAULT_UPPER), CGXStyle().SetValue(Fun_FindMsg("UartConvDlg_InitGridRx_msg1","IDD_UART_CONV")));//&&
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FAULT_LOWER), CGXStyle().SetValue(Fun_FindMsg("UartConvDlg_InitGridRx_msg2","IDD_UART_CONV")));//&&
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_END_UPPER), CGXStyle().SetValue(Fun_FindMsg("UartConvDlg_InitGridRx_msg3","IDD_UART_CONV")));//&&
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_END_LOWER), CGXStyle().SetValue(Fun_FindMsg("UartConvDlg_InitGridRx_msg4","IDD_UART_CONV")));//&&
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FUNCTION_DIVISION),    CGXStyle().SetValue(Fun_FindMsg("UartConvDlg_InitGridRx_msg5","IDD_UART_CONV")));//&&
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FUNCTION_DIVISION2),    CGXStyle().SetValue(Fun_FindMsg("UartConvDlg_InitGridRx_msg6","IDD_UART_CONV")));//&&
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_FUNCTION_DIVISION3),    CGXStyle().SetValue(Fun_FindMsg("UartConvDlg_InitGridRx_msg7","IDD_UART_CONV")));//&&
	m_wndUartGrid.SetStyleRange(CGXRange(0, UART_COL_DESC),      CGXStyle().SetValue("Description").SetHorizontalAlignment(DT_LEFT));

	// Resize Col Width
	m_wndUartGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);
	m_wndUartGrid.GetParam()->EnableUndo(TRUE);

	// 컬럼별 속성 설정
	char str[12], str2[7], str3[5];

	// CMD
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_CMD),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);

/*
	// Frame ID
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FRAMEID),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_EDIT)
		);
*/

	// ITEMS
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_ITEMS),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
	);

/*
	// Frame ID
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FRAMEID),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
	);
*/

	// CMD Start bit
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_CMD_STARTBIT),
		CGXStyle()
		.SetHorizontalAlignment(DT_CENTER)
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
		.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("256"))
		//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~256)"))
		.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("UartConvDlg_InitGridRx_msg8","IDD_UART_CONV"))//&&
		.SetValue(0L)
		);

	// Bit count
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 8);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_CMD_BITCOUNTS),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		.SetValue(16L)
		);

/*
	// Frame Start bit
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FRAME_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("63"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~63)"))
			.SetValue(0L)
	);
*/

/*
	// Bit count
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 8);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FRAME_BITCOUNTS),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(16L)
	);
*/

	// Datatype
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_DATATYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
			.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
//			.SetChoiceList(_T("Unsigned\r\nSigned\r\n"))
			.SetValue(_T("Unsigned"))
	);

	// Factor
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FACTOR),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1.0f)
	);

	// offet (
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 안전상한
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FAULT_UPPER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);


	// 안전하한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FAULT_LOWER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 종료상한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_END_UPPER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 종료하한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_END_LOWER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	//ljb 2011222 이재복 S //////////
	CString strComboItem,strTemp;
	strComboItem = "None [0]\r\n";
	for (int i=0; i < m_strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",m_strArryCanName.GetAt(i),m_uiArryCanCode.GetAt(i));
		strComboItem = strComboItem + strTemp;
	}

	/*m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FUNCTION_DIVISION, UART_COL_FUNCTION_DIVISION3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)	//선택만 가능
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);*/

	//ksj 20200330 : 분류 값 선택시 값이 선택되지 않고 사라지는 현상 수정.
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_FUNCTION_DIVISION,UART_COL_FUNCTION_DIVISION3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);


	// Desc
	m_wndUartGrid.SetStyleRange(CGXRange().SetCols(UART_COL_DESC),
		CGXStyle()
			.SetHorizontalAlignment(DT_LEFT)
	);


}

void CUartConvDlg::OnAddScheRow() 
{
	CRowColArray ra;
	m_wndScheduleGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndScheduleGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndScheduleGrid.InsertRows(m_wndScheduleGrid.GetRowCount()+1, 1);
	
}

void CUartConvDlg::OnDelScheRow() 
{
	CRowColArray ra;
	m_wndScheduleGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndScheduleGrid.RemoveRows(ra[i], ra[i]);
	}
	
}

void CUartConvDlg::OnAddRow() 
{
	CRowColArray ra;
	
	m_wndUartGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndUartGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndUartGrid.InsertRows(m_wndUartGrid.GetRowCount()+1, 1);
		
	
	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;	
}

void CUartConvDlg::OnDelRow() 
{
	CRowColArray ra;
	m_wndUartGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndUartGrid.RemoveRows(ra[i], ra[i]);
	}
	
	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;
}

void CUartConvDlg::OnCancel() 
{
	return ;
}


void CUartConvDlg::OnOK() 
{
	return ;
}

void CUartConvDlg::OnClose() 
{
	m_bCanRecvRxTxMsg = FALSE ;

	m_pDoc->ClearUartAllShowMessageFlag() ;

	// 쓰레드에서 리스트박스에 쓰는 시간 까지 멈추기
	Sleep(200) ;


	CDialog::OnCancel();
	
}

void CUartConvDlg::OnOpenConvData() 
{
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", 
		"csv", "csv");
	
	CFileDialog pDlg(TRUE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	if (IDOK == pDlg.DoModal())
	{
		if (m_wndUartGrid.GetRowCount() > 0)
		{
			m_wndUartGrid.RemoveRows(1, m_wndUartGrid.GetRowCount());
		}
		
		m_strConfigFilename = pDlg.GetPathName() ;
		if (LoadUartConfig(m_strConfigFilename) == TRUE)
		{
			CString strName;
			//strName.Format("Uart 설정 - %s", pDlg.GetFileName());
			strName.Format(Fun_FindMsg("UartConvDlg_OnOpenConvData_msg1","IDD_UART_CONV"), pDlg.GetFileName());//&&
			this->SetWindowText(strName);
			
			//				IsChangeFlag(TRUE);
			//				GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}
}

BOOL CUartConvDlg::LoadUartConfig(CString strLoadPath)
{
	CString strData;
	CStdioFile aFile;
	CFileException e;
	int nIndex, nPos = 0;
//	int nTemp;
	INT nProtocolVer ;

	if (!aFile.Open(strLoadPath, CFile::modeRead|CFile::shareDenyWrite, &e))
	{
		//AfxMessageBox("File이 Open되어 있어 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("UartConvDlg_LoadUartConfig_msg1","IDD_UART_CONV"));//&&
		return FALSE;
	}

	CStringList		strDataList;
	CStringList		strItemList;
	aFile.ReadString(strData);	//Version Check

	if (strData.Find("Data") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		aFile.ReadString(strData);	//Data
		
		CString strType, strDescript;
		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			CString strTemp = strData.Mid(nPos, nIndex - nPos);		// UART 여부 Check
			strType = strTemp;

			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);				//1: 1006 Ver 추후 버전 업그레이드에 따라 추가
			
			if(strTemp.IsEmpty())
				nProtocolVer = 0;
			else
				nProtocolVer = atoi(strTemp);

			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);

			strTemp = strData.Mid(nPos, nIndex - nPos);
			strDescript = strTemp;
		}

// 		EnableMaskFilter(m_ckUserFilter, PS_CAN_TYPE_MASTER);
			
		// Data Info에서 Type에 따라 Load할 Data를 결정	
		if (strType != "UART")
		{
			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("UartConvDlg_LoadUartConfig_msg2","IDD_UART_CONV"));//&&
//			m_nProtocolVer = 0;
			return FALSE;
		}

		nPos = 0;
		aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.
		aFile.ReadString(strData);	//
	}	

	if (strData.Find("UART-CAN") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.

		// UART인지 Check
		CString strTitle = "UART BaudRate,Databit,Parity,StopBit,CAN BaudRate,Ext ID" ;
 		if (strData != strTitle)
 		{
 			//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("UartConvDlg_LoadUartConfig_msg3","IDD_UART_CONV"));//&&
 			return FALSE;
 		}

		aFile.ReadString(strData);	//Data

		nIndex = strData.Find(",", nPos);
		if(nIndex != -1)
		{
			// UART baudrate
//			nPos = nIndex+1;
//			nIndex = strData.Find(",", nPos);

			CString strTemp = strData.Mid(nPos, nIndex - nPos);
			INT nBaudrate = atoi(strTemp) ;
			for (int i = 0; i < m_ctrlBaudrate.GetCount(); i++)
			{
				if (nBaudrate == m_ctrlBaudrate.GetItemData(i))
				{
					m_ctrlBaudrate.SetCurSel(i) ;
					break ;
				}
			}

			// data bit
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			strTemp = strData.Mid(nPos, nIndex - nPos);
			INT nDatabit = atoi(strTemp) ; 
			m_ctrlDatabits.SetCurSel(nDatabit == 7 ? 0 : 1) ;

			// Parity
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			strTemp = strData.Mid(nPos, nIndex - nPos);
			INT nParity = atoi(strTemp) ; 
			m_ctrlParitybit.SetCurSel(nParity) ;

			// Stopbit
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			strTemp = strData.Mid(nPos, nIndex - nPos);
			INT nStopbit = atoi(strTemp) ; 
			m_ctrlParitybit.SetCurSel(nStopbit == 1 ? 0 : 1) ;

			// Can baudrate
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			strTemp = strData.Mid(nPos, nIndex - nPos);
			INT nCanBaud  = atoi(strTemp) ;
			m_CAN_Baudrate.SetCurSel(0) ;
			for (int n = 0; n < sizeof(canspeed2) / sizeof(canspeed2[0]); n++)
			{
				if (nCanBaud == canspeed2[n])
				{
					m_CAN_Baudrate.SetCurSel(n) ;
					break ;
				}
			}

			// Can extended
			nPos = nIndex+1;
			nIndex = strData.Find(",", nPos);
			strTemp = strData.Mid(nPos, nIndex - nPos);
			m_bCheckExtMode = atoi(strTemp) ;

		}
	}
	else
	{
		//AfxMessageBox("Type이 맞지 않아 파일을 Load 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("UartConvDlg_LoadUartConfig_msg4","IDD_UART_CONV"));//&&
		return FALSE;
	}

	aFile.ReadString(strData);	//빈줄 -> 그냥 버린다.

	// UART ID.........................
	// .1. 현재 grid 의 모든 항목 삭제
	if (m_wndUartGrid.GetRowCount() > 0)
	{
		m_wndUartGrid.RemoveRows(1, m_wndUartGrid.GetRowCount()) ;
	}

	// Schedule........................
//	nPos = strData.Find("Schedule", 0);
	// .1. 현재 grid 의 모든 항목 삭제
	if (m_wndScheduleGrid.GetRowCount() > 0)
	{
		m_wndScheduleGrid.RemoveRows(1, m_wndScheduleGrid.GetRowCount()) ;
	}

	aFile.ReadString(strData);	// schedule		
	if (strData.Find("Schedule") > 0)
	{
		aFile.ReadString(strData);	//Title -> 그냥 버린다.
		
		while (aFile.ReadString(strData))	// schedule items
		{
			if (strData == "")
				break;
			
			CString str, strTemp ;

			INT nRow = m_wndScheduleGrid.GetRowCount() + 1;
			m_wndScheduleGrid.InsertRows(nRow, 1);
			
			// ReqID
			int p0, p1 = 0;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndScheduleGrid.SetValueRange(CGXRange(nRow, 1), strTemp) ;
						
			// Frame name
			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndScheduleGrid.SetValueRange(CGXRange(nRow, 2), strTemp) ;
			
			// Delay (ms)
			p1 = p0 +1;
			p0 = strData.Find(",", p1);
			strTemp = strData.Mid(p1, p0 - p1) ;
			m_wndScheduleGrid.SetValueRange(CGXRange(nRow, 3), strTemp) ;
			
			nRow++ ;
		}
		
	}

	aFile.ReadString(strData) ;


	// UART Map ......................................
	aFile.ReadString(strData) ; // title skipping..

	nPos = 0;
	while (aFile.ReadString(strData))
	{
		CString str, strTemp ;
		if(strData == "")
			break;

		INT nRow = m_wndUartGrid.GetRowCount() + 1;
		m_wndUartGrid.InsertRows(nRow, 1);


		// Response (HEX)
		int p0, p1 = 0;
		p0 = strData.Find(",", p1);		
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_CMD), strTemp) ;

		// Items
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_ITEMS), strTemp);

		// StartBit
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_CMD_STARTBIT), strTemp) ;

		// BitCount
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_CMD_BITCOUNTS), strTemp) ;


		// frame id, startbit 는 파일에는 쓰되 읽을 때는 참조하지 않는다.
		// frame id
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
//		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FRAMEID), strTemp) ;

		// frame start bit
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
//		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FRAME_STARTBIT), strTemp) ;

		// DataType
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		INT nType = atoi(strTemp) ;
		CString strType = (nType == 0) ? "Unsigned" : (nType == 1) ? "Signed" : (nType == 2) ? "Float" : "String" ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_DATATYPE),  strType) ;

		// Factor
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;

		CString strFloat ;
		strFloat.Format("%.5f", atof(strTemp)) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FACTOR),    strFloat) ;

		
		// Offset
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_OFFSET),    strFloat) ;
		
		// 안전상한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FAULT_UPPER),    strFloat) ;
		
		// 안전하한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FAULT_LOWER),    strFloat) ;
		
		// 종료상한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_END_UPPER),    strFloat) ;
		
		// 종료하한
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		strFloat.Format("%.5f", (strTemp.IsEmpty()) ? 0 : atof(strTemp)) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_END_LOWER),    strFloat) ;
		
		// 분류값 1
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		INT nValue ;
		nValue = (strTemp.IsEmpty()) ? 0 : atoi(strTemp) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FUNCTION_DIVISION),  Fun_FindCanName(nValue)) ;
		
		// 분류값 2
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		nValue = (strTemp.IsEmpty()) ? 0 : atoi(strTemp) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FUNCTION_DIVISION2),  Fun_FindCanName(nValue)) ;
		
		// 분류값 3
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		nValue = (strTemp.IsEmpty()) ? 0 : atoi(strTemp) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_FUNCTION_DIVISION3),  Fun_FindCanName(nValue)) ;
		
		// Descript
		p1 = p0 +1;
		p0 = strData.Find(",", p1);
		strTemp = strData.Mid(p1, p0 - p1) ;
		m_wndUartGrid.SetValueRange(CGXRange(nRow, UART_COL_DESC), strTemp) ;
		
	}	
	

	aFile.Close();

	UpdateData(FALSE);
	
	return TRUE;
	
}


CString CUartConvDlg::Fun_FindCanName(int nDivision)
{
	CString strName;
//	int nSelectPos, nFindPos;
	for (int i=0; i < m_uiArryCanCode.GetSize() ; i++)
	{
		if (nDivision == m_uiArryCanCode.GetAt(i))
		{
			strName.Format("%s [%d]", m_strArryCanName.GetAt(i), nDivision);
			return strName;
		}
	}
	strName.Format("None [%d]", nDivision);
	return strName;
}

void CUartConvDlg::OnSaveConfig() 
{
	UpdateData() ;

	// Grid 내의 Map 값 Valid 체크
	if (!CheckMapValues())
	{
		return ;
	}
	
	UpdateData();
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\DataBase\\Config", szCurDir);
	
	CString strTemp;
	strTemp.Format("csv file(*.%s)|*.%s|All Files(*.*)|*.*|", "csv", "csv");
		
	CFileDialog pDlg(FALSE, "csv", "", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strTemp);
	pDlg.m_ofn.lpstrTitle = "PNE CTSPro file";
	pDlg.m_ofn.lpstrInitialDir = defultName;
	if (IDOK == pDlg.DoModal())
	{
		if (SaveUartConfig(pDlg.GetPathName()) == TRUE)
		{
//			GetDlgItem(IDC_BTN_SAVE_CAN_SETUP)->EnableWindow(FALSE);
		}
	}
	

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;

}

BOOL CUartConvDlg::SaveUartConfig(CString strSavePath)
{
	CFile file;
	CString strData,strSaveData;
	CString strTitle, strTitle2, strTitle3 ;

//	int i = 0;
	strSaveData.Empty();

	//Start Save Master/////////////////////////////////////////////////////////////////////////

	TRY
	{
		if (strSavePath.Find('.') < 0)
		{
			strSavePath += ".csv";
		}

		if (file.Open(strSavePath, CFile::modeCreate|CFile::modeWrite) == FALSE)
		{
			//AfxMessageBox("파일을 생성할 수 없습니다.");
			AfxMessageBox(Fun_FindMsg("UartConvDlg_SaveUartConfig_msg1","IDD_UART_CONV"));//&&
			return FALSE;
		}

		// title 1
		//--------------------------------------------------
		strTitle = "**Data Info**\r\nTYPE,VERSION,DESCRIPT\r\n";
		file.Write(strTitle, strTitle.GetLength());

		strData.Format("UART,4,PNE UART CONFIG FILE,\r\n\r\n");
		file.Write(strData, strData.GetLength());

		// title 2
		//--------------------------------------------------
		strTitle = "**UART-CAN Configuration**\r\nUART BaudRate,Databit,Parity,StopBit,CAN BaudRate,Ext ID\r\n";
		file.Write(strTitle, strTitle.GetLength());

		INT nUartBaud = m_ctrlBaudrate.GetItemData(m_ctrlBaudrate.GetCurSel()) ;
		INT nDatabit = m_ctrlDatabits.GetCurSel() == 0 ? 7 : 8 ;
		INT nParity = m_ctrlParitybit.GetCurSel() ;
		INT nStopbit = m_ctrlStopbit.GetCurSel() == 0 ? 1 : 2 ;
		INT nCanBaud = canspeed2[m_CAN_Baudrate.GetCurSel()] ;

		// TX 값 가져오기 가져오기		

		strData.Format("%d,%d,%d,%d,%d,%d,\r\n\r\n"
					 , nUartBaud
					 , nDatabit
					 , nParity
					 , nStopbit
					 , nCanBaud
					 , m_bCheckExtMode
					 );

		file.Write(strData, strData.GetLength()); //

		// title 4
		//--------------------------------------------------
		strTitle = "**Schedule Configuration**\r\nReqID(hex),Frame name,Delay(ms)\r\n";
		file.Write(strTitle, strTitle.GetLength());

		CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO> sched_array ;
		GetScheduleInfo(sched_array) ;
		
		for (int nLoop = 0; nLoop < sched_array.GetSize(); nLoop++)
		{
			PUARTSCHEDULER_INFO ptr = sched_array.GetAt(nLoop) ;
			TRACE("id:%d | delay: %d\n", ptr->id, ptr->delay) ;
			
			// 현재 전시된 그리드의 항목을 모두 저장한다.			
			strData.Format("%x,%s,%d,\r\n"
						 , ptr->id
						 , ptr->framename
						 , ptr->delay) ;
			
			file.Write(strData, strData.GetLength());
		}

		strData = "\r\n\r\n" ;
		file.Write(strData, strData.GetLength());

		// clearing
		for(int nLoop = 0; nLoop < sched_array.GetSize(); nLoop++)
		{
			PUARTSCHEDULER_INFO ptr = sched_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		sched_array.RemoveAll() ;

		// title 5
		//--------------------------------------------------
		strTitle = "Frame ID,Frame Name,Items,StartBit,BitCount,DataType,Factor,Offset,안전상한,안전하한,종료상한,종료하한,분류값 1,분류값 2,분류값 3,Desc,\r\n";
		//strTitle = Fun_FindMsg("UartConvDlg_SaveUartConfig_msg2","IDD_UART_CONV");//&&
		file.Write(strTitle, strTitle.GetLength());	//Title 

		// Grid 의 설정된 값들을 가져온다.
		CArray<PUCC_INFO, PUCC_INFO> ucc_array ;
		GetUartConvInfo(ucc_array) ;
		
		for(int nLoop = 0; nLoop < ucc_array.GetSize(); nLoop++)
		{
			UCC_INFO* ptr = ucc_array.GetAt(nLoop) ;
			TRACE("cmd 0x%02x | name:%s | s: %02d | b: %2d | datatype: %d | canid: %x | s: %02d | f: %.2f | off: %.2f | fup: %.2f | flo: %.2f | eup: %.2f | elo: %.2f \n"
				, ptr->cmd
				, ptr->item
				, ptr->cmd_startbit
				, ptr->cmd_bitcounts
				, ptr->datatype
				, ptr->canid
				, ptr->can_startbit
				, ptr->factor
				, ptr->offset
				, ptr->fault_upper
				, ptr->fault_lower
				, ptr->end_upper
				, ptr->end_lower) ;

			// 현재 전시된 그리드의 항목을 모두 저장한다.

			// 문자열내에 ', "" 를 교체
			CString name = ptr->item ;
			name.Replace("'", "''") ;
			
			CString desc = ptr->desc ;
			desc.Replace("'", "''") ;

			//43,BMS_T_bat,0,8,43,0,1.000000,-40.000000,0.000000,0.000000,0.000000,0.000000,0,0,0,,
			strData.Format("%x,%s,%d,%d,%x,%d,%d,%f,%f,%f,%f,%f,%f,%d,%d,%d,%s,\r\n"
				  , ptr->cmd, ptr->item, ptr->cmd_startbit, ptr->cmd_bitcounts, ptr->canid, ptr->can_startbit, ptr->datatype, ptr->factor
				  , ptr->offset, ptr->fault_upper, ptr->fault_lower, ptr->end_upper, ptr->end_lower
				  , ptr->function_division, ptr->function_division2, ptr->function_division3, desc) ;

			file.Write(strData, strData.GetLength());
		}

		// clearing
		for(int nLoop = 0; nLoop < ucc_array.GetSize(); nLoop++)
		{
			UCC_INFO* ptr = ucc_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		ucc_array.RemoveAll() ;
		//---------------------------------------------------------------------

	}
	CATCH(CFileException, e)
	{
		e->ReportError();
	}
	END_CATCH
	
	file.Close();

	return TRUE;
}

// 호출한 곳에서 array 의 메모리를 제거하여야 한다.
void CUartConvDlg::GetScheduleInfo(CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO>& array)
{
	ASSERT(array.GetSize() == 0) ;
	
//	INT nCurrAddr = -1 ;
	for (int nRow = 1; nRow < m_wndScheduleGrid.GetRowCount()+1; nRow++)
	{		
		PUARTSCHEDULER_INFO info = new UARTSCHEDULER_INFO ;
		ZeroMemory(info, sizeof(UARTSCHEDULER_INFO)) ;
		
		
		// Modified by 224 (2014/11/05) : Dec->Hex 로 전시
		//		info->id    = atoi(m_wndScheduleGrid.GetValueRowCol(nRow, 1)) ;
		info->id    = HexStr2Dec(m_wndScheduleGrid.GetValueRowCol(nRow, 1)) ;
		strcpy(info->framename, m_wndScheduleGrid.GetValueRowCol(nRow, 2)) ;
		info->delay = atoi(m_wndScheduleGrid.GetValueRowCol(nRow, 3)) ;
		
		array.Add(info) ;
		
	}
	
#ifdef _DEBUG	
	for (int nLoop = 0; nLoop < array.GetSize(); nLoop++)
	{
		PUARTSCHEDULER_INFO ptr = array.GetAt(nLoop) ;
		//		TRACE("addr:%02x | s: %02d | b: %2d | datatype: %d | canid: %03d | s: %02d | b: %2d | f: %.2f | name: %s \n"
		TRACE("id = %d, delay = %d \n"
			, ptr->id
			, ptr->delay) ;
	}
#endif
}

BOOL CUartConvDlg::CheckMapValues() 
{
	// 체크항목
	// 1. 인덱스의 행이 감소하면 안된다.
	// 2. 인덱스가 0이거나 이하이면 안된다.
	// 3. 동일 인덱스의 누적 bitcount 가 16을 넘을 수 없다.
	// 4. ITEMS 의 이름이 있어야 한다..
	
//	INT nLatestAddr = -1 ;
	INT nAccCounts = 0 ;

	// RX ------------------------------------------------------------
	for (int nRow = 1; nRow < m_wndUartGrid.GetRowCount()+1; nRow++)
	{		
		INT nCurrCmd    = HexStr2Dec(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_CMD));
//		INT nStartBit  = atoi(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_CMD_STARTBIT)) ;
		INT nBitcounts = atoi(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_CMD_BITCOUNTS)) ;
		
/*
		if (nLatestAddr > nCurrAddr)
		{
			// 1. 인덱스의 행이 감소하면 안된다.
			CString str ;
			str.Format(" %d 번째행 이상", nRow) ;
			MessageBox("인덱스값은 감소할 수 없습니다.", str) ;
			
			return FALSE ;
		}
*/

		if (nCurrCmd < 0)
		{
			// 2. 인덱스가 0이거나 이하이면 안된다.
			CString str ;
			//str.Format("%d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("UartConvDlg_CheckMapValues_msg1","IDD_UART_CONV"), nRow) ;//&&
			//MessageBox("커맨드값은 0보다 커야 합니다.", str) ;
			MessageBox(Fun_FindMsg("UartConvDlg_CheckMapValues_msg2","IDD_UART_CONV"), str) ;//&&
			
			return FALSE ;
		}

		// . 누적 bitcount 보관
		nAccCounts += nBitcounts ;
		
		CString strItems = m_wndUartGrid.GetValueRowCol(nRow, UART_COL_ITEMS) ;
		if (strItems.IsEmpty())
		{
			// 4. ITEMS 의 이름이 있어야 한다.
			CString str ;
			//str.Format("%d 행의 ITEMS 명 이상", nRow) ;
			str.Format(Fun_FindMsg("UartConvDlg_CheckMapValues_msg3","IDD_UART_CONV"), nRow) ;//&&
			//MessageBox(str, "ITEMS 의 이름이 있어야 합니다.") ;
			MessageBox(str, Fun_FindMsg("UartConvDlg_CheckMapValues_msg4","IDD_UART_CONV")) ;//&&
			
			return FALSE ;			
		}
		
/*
		nLatestAddr = nCurrAddr ;
*/
	}

	return TRUE ;
}

// 호출한 곳에서 array 의 메모리를 제거하여야 한다.
void CUartConvDlg::GetUartConvInfo(CArray<PUCC_INFO, PUCC_INFO>& array)
{
	// 2015/04/01: 
// => CheckMapValues() 함수 새로 구현해야함
// 	Resp(HEX)의 값은 한군데에 모아져 있어야 함.  분포되면 안됨.
// 	하나의 Resp(hex)값 내의 startbit 는 증가되어야 함. ??? 유저에게 맡기나?

	// 현재 테이블 값 검증. 순차적인가? INDEX, STARTBIT, BITCOUNTS
	
	// CAN Receive 설정 함수 호출
	//	
	// 누적 bit Counts 가 64(8bytes) 가 넘으면 새로운 CANID를 생성한다.
	
	ASSERT(array.GetSize() == 0) ;
	
	int canid = 600 ;
	int nAccBitCounts = 0 ;	// 누적값이 64를 넘으면 안된다.
	
	UCC_INFO* pInfo = NULL ;
	INT nPrevCmd = INT_MAX ;
	for (int nRow = 1; nRow < m_wndUartGrid.GetRowCount()+1; nRow++)
	{
		// Added by 224 (2014/11/05) : 16 진수 로 수정 
		INT nCurrCmd    = HexStr2Dec(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_CMD));
		ASSERT(nCurrCmd > 0) ;

		if (nPrevCmd != nCurrCmd)
		{
			canid++ ;
			nAccBitCounts = 0 ;

//			UCC_INFO* info = new UCC_INFO ;
		}
		
		pInfo = new UCC_INFO ;
		ZeroMemory(pInfo, sizeof(UCC_INFO)) ;

		ASSERT(pInfo != NULL) ;
		strcpy(pInfo->item, m_wndUartGrid.GetValueRowCol(nRow, UART_COL_ITEMS)) ;
		CString strType = m_wndUartGrid.GetValueRowCol(nRow, UART_COL_DATATYPE) ;
		pInfo->datatype     = (CString("Unsigned") == strType) ? 0 : (CString("Signed") == strType) ? 1 : (CString("Float") == strType) ? 2 : 3 ;
		strcpy(pInfo->desc, m_wndUartGrid.GetValueRowCol(nRow, UART_COL_DESC)) ;
		
		pInfo->cmd           = HexStr2Dec(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_CMD)) ;
		pInfo->cmd_startbit  = atoi(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_CMD_STARTBIT)) ;
		pInfo->cmd_bitcounts = atoi(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_CMD_BITCOUNTS)) ;

//		pInfo->canid		 = HexStr2Dec(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FRAMEID)) ;
// 		pInfo->can_startbit  = atoi(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FRAME_STARTBIT)) ;

		if (nAccBitCounts + pInfo->cmd_bitcounts > 64)
		{
			canid++ ;
			nAccBitCounts = 0;
		}
		pInfo->canid		 = canid ;
 		pInfo->can_startbit  = nAccBitCounts ;
		nAccBitCounts += pInfo->cmd_bitcounts ;

		pInfo->factor		= atof(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FACTOR)) ;
		pInfo->offset		= atof(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_OFFSET)) ;
		pInfo->fault_upper	= atof(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FAULT_UPPER)) ;
		pInfo->fault_lower   = atof(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FAULT_LOWER)) ;
		pInfo->end_upper	 = atof(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_END_UPPER)) ;
		pInfo->end_lower	 = atof(m_wndUartGrid.GetValueRowCol(nRow, UART_COL_END_LOWER)) ;
		
		CString strTemp, strCode ;
		INT nLength ;
		
		strTemp = m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FUNCTION_DIVISION);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		pInfo->function_division  = atoi(strCode);
		
		strTemp = m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FUNCTION_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		pInfo->function_division2  = atoi(strCode);
		
		strTemp = m_wndUartGrid.GetValueRowCol(nRow, UART_COL_FUNCTION_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		pInfo->function_division3  = atoi(strCode);
		
		//		nAccCounts += bitcounts ;
		
		array.Add(pInfo) ;
	
		nPrevCmd = nCurrCmd ;
	}

#ifdef _DEBUG	
	for (int nLoop = 0; nLoop < array.GetSize(); nLoop++)
	{
		UCC_INFO* ptr = array.GetAt(nLoop) ;
		TRACE("canid: %03d | datatype: %d | s: %02d | f: %.2f | name: %s \n"
			, ptr->canid
			, ptr->datatype
			, ptr->can_startbit
			, ptr->factor
			, ptr->item) ;
	}
#endif
}

void CUartConvDlg::OnTransCanData() 
{
	// Grid 내의 Map 값 Valid 체크
	if (!CheckMapValues())
	{
		return ;
	}
	
	// Grid 의 설정된 값들을 가져온다.
	CArray<PUCC_INFO, PUCC_INFO> ucc_array ;
	GetUartConvInfo(ucc_array) ;
	
	
	//-----------------------------------------------------------------------
	// CAN Receive 설정 함수 호출
	//-----------------------------------------------------------------------
	
	if (!SaveCANConfig(ucc_array))
	{
		TRACE("SaveCANConfig return FALSE\n") ;
		return ;
	}
	//-----------------------------------------------------------------------
	// Runtime 변환 단위테스트
	//-----------------------------------------------------------------------
	
	
	// clearing
	for (INT nLoop = 0; nLoop < ucc_array.GetSize(); nLoop++)
	{
		UCC_INFO* ptr = ucc_array.GetAt(nLoop) ;
		delete ptr ;
	}
	ucc_array.RemoveAll() ;
	
	POSITION pos = m_pDoc->GetFirstViewPosition();
	CCTSMonProView* pView = (CCTSMonProView *)m_pDoc->GetNextView(pos);
	
	pView->m_wndCanList.DeleteAllItems();	
	
	// 적용 시 버튼은 비활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;
	
	
	// CAN 적용과 동시에 모드버스 종료-시작을 호출한다.
	//	PostMessage(WM_COMMAND, IDC_OPER_STOP) ;
	SendMessage(WM_COMMAND, IDC_OPER_STOP) ;

	// 포트 재오픈 시간...
	// sio_close() 후 곧 바로 sio_open() 호출 시 포트 오픈 오류발생함
	Sleep(500) ;
	
	PostMessage(WM_COMMAND, IDC_OPER_START) ;
}

// SBC 에 CAN 설정값을 전달하는 함수
BOOL CUartConvDlg::SaveCANConfig(CArray<PUCC_INFO, PUCC_INFO>& array)
{
	CString strTemp,strCode;
//	int		nLength;
	int i;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if (pMD == NULL)
	{
		return FALSE ;
	}

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		return FALSE ;
	}

	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END	)
	{
		//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("UartConvDlg_SaveCANConfig_msg1","IDD_UART_CONV"));//&&
		return FALSE ;
	}

	// 설정 변수값을 가져온다.
	UpdateData();

	if ((array.GetSize()) > 256)
	{
		//AfxMessageBox("CAN 설정은 256개를 초과 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("UartConvDlg_SaveCANConfig_msg2","IDD_UART_CONV"));//&&
		return FALSE ;
	}

	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_DATA commonData;
	ZeroMemory(&commonData, sizeof(SFT_CAN_COMMON_DATA));
	
	commonData.can_baudrate = m_CAN_Baudrate.GetCurSel();
	commonData.extended_id	= m_bCheckExtMode ;
//	commonData.fCell_cv	= m_fCV;
	commonData.controller_canID = strtoul("0", (char **)NULL, 16);
	commonData.bms_type     = 0 ;
	commonData.bms_sjw      = 0 ;

	
	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_MASTER);
	memcpy(&canSetData.canCommonData, &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008
	
//	int nSelectIndex=0;	//ljb 2011222 이재복 //////////

	SFT_CAN_SET_DATA canData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);

	for( int i = 0; i < array.GetSize(); i++)
	{
		UCC_INFO* ptr = array.GetAt(i) ;

		canData[i].canID      = ptr->canid ;
		// Commented by 224 (2014/07/04) : 김재호 과장의 요청에 의하여, 모드버스 포트는 슬레이브로 설정한다.
//		canData[i].canType    = PS_CAN_TYPE_MASTER;
		canData[i].canType    = PS_CAN_TYPE_SLAVE;
		strcpy(canData[i].name, ptr->item);		
		canData[i].startBit   = ptr->can_startbit ;
		canData[i].bitCount   = ptr->cmd_bitcounts ;

		// 0: Intel, 1:Motolora
		canData[i].byte_order = 0;		
		
		canData[i].data_type = ptr->datatype ;
		canData[i].factor_multiply = ptr->factor ;
		canData[i].factor_Offset = ptr->offset ;
		
		canData[i].fault_upper    = ptr->fault_upper ;
		canData[i].fault_lower    = ptr->fault_lower ;
		canData[i].end_upper      = ptr->end_upper ;
		canData[i].end_lower      = ptr->end_lower ;
		canData[i].default_fValue = 0.0f ;
		canData[i].sentTime       = 0.0f ;

		canData[i].function_division  = ptr->function_division ;
		canData[i].function_division2 = ptr->function_division2 ;
		canData[i].function_division3 = ptr->function_division3 ;
// 		canData[i].startBit2          = 0 ;
// 		canData[i].bitCount2          = 0 ;
// 		canData[i].byte_order2        = 0 ;		
// 		canData[i].data_type2         = 0 ;
// 		canData[i].default_fValue2    = 0 ;
		//****************************************************************************************

	}

	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_SLAVE);
	memcpy(&canSetData.canCommonData[1], &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008
	
	pMD->SetCANSetData(nSelCh, canData, array.GetSize());
	memcpy(&canSetData.canSetData, &canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);	//ljb 201008

	//SFT_RCV_CMD_CAN_SET canSetData;
	//ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CAN_SET));
// 	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
// 	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));
	
// 	SFT_MD_CAN_INFO_DATA * canInforData = pMD->GetCanAllData();
// 	memcpy(&canSetData.canCommonData, pMD->GetCANCommonData(), sizeof(canSetData.canCommonData));
// 	memcpy(&canSetData.canSetData, canInforData->canSetAllData.canSetData, sizeof(canSetData.canSetData));
	

	TRACE("CAN commdata Size = %d, size data = %d, total size = %d \n",sizeof(canSetData.canCommonData[0]),sizeof(canSetData.canSetData[0]), sizeof(canSetData));
	if(int nRth = m_pDoc->SetCANDataToModule(nSelModule, &canSetData, nSelCh) == FALSE)
	{
		//AfxMessageBox("CAN 정보 설정에 실패했습니다.");
		AfxMessageBox(Fun_FindMsg("UartConvDlg_SaveCANConfig_msg3","IDD_UART_CONV"));//&&
		return FALSE ;
	}

//	IsChange = FALSE;
//	GetDlgItem(IDOK)->EnableWindow(IsChange);

	pMD->SaveCanConfig();

	
	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;

	return TRUE ;
}

void CUartConvDlg::OnOperStart() 
{
	UpdateData() ;
	
	m_bCanRecvRxTxMsg = TRUE ;
		
	if (m_pDoc->BeginUartCanConvThread(nSelCh, nSelModule, m_strConfigFilename))
	{
		GetDlgItem(IDC_OPER_START)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_OPER_STOP)->EnableWindow(TRUE) ;
	}
	
	// 적용 버튼은 비 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;
	
	// 채널 변경 비 활성화
	//	m_Channel.EnableWindow(FALSE) ;
}

void CUartConvDlg::OnOperStop() 
{
	// TODO: Add your control notification handler code here
	m_bCanRecvRxTxMsg = FALSE ;
	m_pDoc->StopUartCanConvThread(m_nDevID) ;
	
	GetDlgItem(IDC_OPER_START)->EnableWindow(TRUE) ;
	GetDlgItem(IDC_OPER_STOP)->EnableWindow(FALSE) ;
	
	// 채널 변경 활성화
	// 	m_Channel.EnableWindow(TRUE) ;
}

void CUartConvDlg::OnEtcSetting() 
{
	CString str ;
	GetDlgItemText(IDC_ETC_SETTING, str) ;
	
	BOOL bMore = FALSE ;
	if (str.Right(2) == ">>")
	{
		bMore = TRUE ;
	}
	
	CRect rcDialog ;
	GetWindowRect(rcDialog) ;
	int nNewWidth = -1 ;
	
	CWnd* pWndMoreOrLess = NULL ;
	CRect rcMoreOrLess ;
	if (bMore)
	{
		pWndMoreOrLess = GetDlgItem(IDC_LARGE) ;
		//SetDlgItemText(IDC_ETC_SETTING, "기타설정 <<") ;
		SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("UartConvDlg_OnEtcSetting_msg1","IDD_UART_CONV")) ;//&&
	}
	else
	{
		pWndMoreOrLess = GetDlgItem(IDC_SMALL) ;
		//SetDlgItemText(IDC_ETC_SETTING, "기타설정 >>") ;
		SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("UartConvDlg_OnEtcSetting_msg2","IDD_UART_CONV")) ;//&&
	}
	ASSERT_VALID(pWndMoreOrLess) ;
	
	pWndMoreOrLess->GetWindowRect(rcMoreOrLess) ;
	
	nNewWidth = rcMoreOrLess.left - rcDialog.left ;
	SetWindowPos(NULL, 0, 0, nNewWidth, rcDialog.Height(), SWP_NOMOVE | SWP_NOZORDER) ;
	
}

void CUartConvDlg::OnSysCommand(UINT nID, LPARAM lParam) 
{
	// 종료버튼이 눌리면 다이얼로그를 종료한다.
	if((nID & 0xFFF0) == SC_CLOSE)
	{
		m_bCanRecvRxTxMsg = FALSE ;

		m_pDoc->ClearUartAllShowMessageFlag() ;

		// 쓰레드에서 리스트박스에 쓰는 시간 까지 멈추기
		Sleep(200) ;

		EndDialog(IDCANCEL);
		return ;
	}

	CDialog::OnSysCommand(nID, lParam);

	return;
}