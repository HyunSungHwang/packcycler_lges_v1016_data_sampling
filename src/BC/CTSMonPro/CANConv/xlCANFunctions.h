/*----------------------------------------------------------------------------
| File        : xlCANFunctions.h
| Project     : Vector CAN Example 
|
| Description : shows the basic CAN functionality for the XL Driver Library
|-----------------------------------------------------------------------------
| $Author: vismra $    $Locker: $   $Revision: 4693 $
| $Header: /VCANDRV/XLAPI/samples/xlCANcontrol/xlCANFunctions.h 4     15.06.05 15:16 Harald $
|-----------------------------------------------------------------------------
| Copyright (c) 2004 by Vector Informatik GmbH.  All rights reserved.
|---------------------------------------------------------------------------*/

#if !defined(AFX_XLCANFUNCTIONS_H__48DFA4A9_72B2_48FE_80D5_D318A80C4B3A__INCLUDED_)
#define AFX_XLCANFUNCTIONS_H__48DFA4A9_72B2_48FE_80D5_D318A80C4B3A__INCLUDED_

#include "vxlapi.h"
#include "LinCanConvBroker.h"

#define MAXPORT		8
// #define CHAN01 0
// #define CHAN02 1

// typedef struct {
//     XLportHandle xlPortHandle; 
//     HANDLE       hMsgEvent;
//     CListBox    *pOutput;
// } TStruct;

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCanConvBroker ;
class CLinCanConvBroker ;
class CCANFunctions
{
public:
	CCANFunctions();
	virtual ~CCANFunctions();

//	XLstatus CANInit();
	XLstatus CANInit(CCanConvBroker* pBroker, int nChannel);
	XLstatus CANInit(CLinCanConvBroker* pBroker, int nChannel);
	XLstatus CANGetDevice() ;
	XLstatus CANClose();
	
	XLstatus CANGoOnBus(unsigned long baudrate);
	XLstatus CANGoOffBus();
	XLstatus CANSend(XLevent xlEvent, int channel);
	XLstatus CANSend(XLevent xlEvent);
	XLstatus CANResetFilter();
	XLstatus CANSetFilter(unsigned long first_id, unsigned long last_id);
	XLstatus ShowLicenses();

	CListBox *m_pOutput;
	CListBox *m_pHardware;

	BOOL m_bShowCycleDone ;	// lin dialog 종료 시 메시지 동기화 플래그
//	BOOL m_bCanRxThreadRun ;
//	BOOL m_bShowMessage ;
//	TStruct m_tstruct ;
	XLhandle GetMsgEvent() { ASSERT(this) ; return m_hMsgEvent; }
	XLportHandle  GetPortHandle() { ASSERT(this) ; return m_xlPortHandle; }

//	CLinCanConvBroker* GetConvBroker() { ASSERT(this) ; return m_pBroker ; }
	CCanConvBroker* GetConvBroker() { ASSERT(this) ; return m_pBroker ; }
	INT GetChannelNo() { ASSERT(this) ; return m_nChannel ; }

private:
	void demoPrintConfig(void) ;
	XLdriverConfig  m_xlDrvConfig;
	XLaccess        m_xlPermissionMask ;
//	XLaccess        g_xlChannelMask ;
	unsigned int    m_BaudRate ;                 //!< Default baudrate

	char            m_AppName[XL_MAX_LENGTH+1] ;
	XLstatus		canInitDriver(XLaccess *pxlChannelMaskTx, unsigned char *pxlChannelIndex) ;
//	XLaccess        m_xlChanMaskTx ;

	unsigned char   m_xlChanIndex ;
//	INT	m_nChannel ;


	XLstatus         canGetChannelMask();
	XLstatus         canInit();
	XLstatus         canCreateRxThread();

	XLaccess         m_xlChannelMask[MAXPORT];
//	XLaccess         m_xlChannelMask[2];        //!< we support only two channels
	XLportHandle     m_xlPortHandle;            //!< and one port
	XLaccess         m_xlChannelMask_both;

	HANDLE           m_hThread;
	XLhandle         m_hMsgEvent;
	int              m_bInitDone;

//	CLinCanConvBroker* m_pBroker ;
	CCanConvBroker* m_pBroker ;
	INT m_nChannel ;
};

DWORD     WINAPI CanRxThread( PVOID par );


#endif // !defined(AFX_XLCANFUNCTIONS_H__48DFA4A9_72B2_48FE_80D5_D318A80C4B3A__INCLUDED_)
