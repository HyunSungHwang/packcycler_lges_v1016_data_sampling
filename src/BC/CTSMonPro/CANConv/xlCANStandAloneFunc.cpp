/*----------------------------------------------------------------------------
| File        : xlCANFunctions.cpp
| Project     : Vector CAN Example 
|
| Description : Shows the basic CAN functionality for the XL Driver Library
|-----------------------------------------------------------------------------
| $Author: vismra $    $Locker: $   $Revision: 20478 $
| $Header: /VCANDRV/XLAPI/samples/xlCANcontrol/xlCANFunctions.cpp 11    14.11.05 10:59 J?g $
|-----------------------------------------------------------------------------
| Copyright (c) 2012 by Vector Informatik GmbH.  All rights reserved.
|---------------------------------------------------------------------------*/

#include "stdafx.h" 
//#include "xlCANcontrol.h"
#include "xlCANStandAloneFunc.h"
#include "debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// globals
//////////////////////////////////////////////////////////////////////

TStruct g_th;
BOOL    g_bThreadRun;

#define RECEIVE_EVENT_SIZE 1                // DO NOT EDIT! Currently 1 is supported only
#define RX_QUEUE_SIZE      4096             // internal driver queue size in CAN events

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCANStandAloneFunc::CCANStandAloneFunc()
{
	m_xlChannelMask[CHAN01] = 0;
	m_xlChannelMask[CHAN02] = 0;
	
	strcpy(m_AppName, "CTSMonPro") ;            //!< Application name which is displayed in VHWconf
	m_BaudRate = 500000 ;						//!< Default baudrate
	m_bInitDone = FALSE;

	m_pOutput = NULL ;
	m_pHardware = NULL ;


	m_nChannel = -1 ;
}

CCANStandAloneFunc::~CCANStandAloneFunc()
{
	if (m_bInitDone)
	{
		CloseHandle(m_hThread);
		CloseHandle(m_hMsgEvent);
		m_bInitDone = FALSE;

/*
		XLstatus xlStatus;	
		if (m_xlPortHandle != XL_INVALID_PORTHANDLE)
		{
			xlStatus = xlClosePort(m_xlPortHandle);
			TRACE("- ClosePort        : PH(0x%x), %s\n", m_xlPortHandle, xlGetErrorString(xlStatus));
		}
		
		m_xlPortHandle = XL_INVALID_PORTHANDLE;
		xlCloseDriver() ;
*/
	}
}

////////////////////////////////////////////////////////////////////////////

//! CANInit

//! Open the driver, get the channelmasks and create the RX thread.
//! 
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CCANStandAloneFunc::CANInit()
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];
	m_bInitDone = FALSE;

/*
	// ------------------------------------
	// open the driver
	// ------------------------------------
	xlStatus = xlOpenDriver();

	sprintf(tmp, "xlOpenDriver, stat: %d", xlStatus);
	DEBUG(DEBUG_ADV, tmp);
	if (xlStatus != XL_SUCCESS)
	{
		AfxMessageBox("Error when opening driver!\nMaybe the DLL is too old.");
		return xlStatus;
	}

	// ---------------------------------------
	// Get/Set the application within VHWConf
	// ---------------------------------------
	xlStatus = canGetChannelMask();
	if ((xlStatus) || (m_xlChannelMask[CHAN01] == 0) || (m_xlChannelMask[CHAN02] == 0))
	{
		return XL_ERROR;
	}

	// ---------------------------------------
	// Open ONE port for both channels
	// ---------------------------------------
	xlStatus = canInit();
	if (xlStatus)
	{
		return xlStatus;
	}
*/

	// Modified by 224 (2014/04/07) : xlCanControl 에서는 두개의 채널만 사용
	m_xlChanMaskTx = 0;
	m_xlChanIndex  = 0;

	xlStatus = canInitDriver(&m_xlChanMaskTx, &m_xlChanIndex) ;


	// ---------------------------------------
	// Create ONE thread for both channels
	// ---------------------------------------
	xlStatus = canCreateRxThread();
	if (xlStatus)
	{
		return xlStatus;
	}

	m_bInitDone = TRUE;

	return xlStatus;
}

void CCANStandAloneFunc::demoPrintConfig(void)
{
	
	unsigned int i;
	char         str[XL_MAX_LENGTH + 1]="";
	
	TRACE("----------------------------------------------------------\n");
	TRACE("- %02d channels       Hardware Configuration               -\n", m_xlDrvConfig.channelCount);
	TRACE("----------------------------------------------------------\n");
	
	for( int i=0; i < m_xlDrvConfig.channelCount; i++)
	{
		
		TRACE("- Ch:%02d, CM:0x%03I64x,"
			, m_xlDrvConfig.channel[i].channelIndex
			, m_xlDrvConfig.channel[i].channelMask);
		
		strncpy(str, m_xlDrvConfig.channel[i].name, 23);
		TRACE("%23s,", str);
		
		memset(str, 0, sizeof(str));
		
		if (m_xlDrvConfig.channel[i].transceiverType != XL_TRANSCEIVER_TYPE_NONE)
		{
			strncpy( str, m_xlDrvConfig.channel[i].transceiverName, 13);
			TRACE("%13s -\n", str);
		}
		else
		{
			TRACE("    no Cab!   -\n", str);
		}
	}
	
	TRACE("----------------------------------------------------------\n\n");
}

////////////////////////////////////////////////////////////////////////////
//! canGetChannelMask
//! parse the registry to get the channelmask
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CCANStandAloneFunc::canGetChannelMask()
{
	XLstatus        xlStatus = XL_ERROR;
	char            tmp[100];

	// default values
	unsigned int  hwType     = 0;
	unsigned int  hwIndex    = 0;
	unsigned int  hwChannel  = 0;
	unsigned int  appChannel = 0;
	unsigned int  busType    = XL_BUS_TYPE_CAN;   
	unsigned int  i;
	unsigned int  chan1Found = 0;
	unsigned int  chan2Found = 0;

//	XLdriverConfig  xlDrvConfig;

	//check for hardware:
	xlStatus = xlGetDriverConfig(&m_xlDrvConfig);
	if (xlStatus)
	{
		return xlStatus;
	}

	// we check only if there is an application registered or not.
	xlStatus = xlGetApplConfig(m_AppName, CHAN01, &hwType, &hwIndex, &hwChannel, busType); 

	// Set the params into registry (default values...!)
	if (xlStatus)
	{
		DEBUG(DEBUG_ADV,"set in VHWConf");

		for( int i=0; i < m_xlDrvConfig.channelCount; i++)
		{

			sprintf (tmp, "hwType: %d, bustype: %d, hwChannel: %d, cap: 0x%x", 
				m_xlDrvConfig.channel[i].hwType, 
				m_xlDrvConfig.channel[i].connectedBusType,
				m_xlDrvConfig.channel[i].hwChannel,
				m_xlDrvConfig.channel[i].channelBusCapabilities);
			DEBUG(DEBUG_ADV,tmp);

			// we search not the first CAN cabs
			if ((m_xlDrvConfig.channel[i].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN) && (appChannel < 2))
			{
				hwType    = m_xlDrvConfig.channel[i].hwType;
				hwIndex   = m_xlDrvConfig.channel[i].hwIndex;
				hwChannel = m_xlDrvConfig.channel[i].hwChannel;

				xlStatus = xlSetApplConfig( // Registration of Application with default settings
					m_AppName,             // Application Name
					appChannel,                 // Application channel 0 or 1
					hwType,                     // hwType  (CANcardXL...)    
					hwIndex,                    // Index of hardware (slot) (0,1,...)
					hwChannel,                  // Index of channel (connector) (0,1,...)
					busType);                   // the application is for CAN.

				m_xlChannelMask[appChannel] = xlGetChannelMask(hwType, hwIndex, hwChannel);
				sprintf (tmp, "Register CAN hWType: %d, CM: 0x%I64x", hwType, m_xlChannelMask[appChannel]);
				DEBUG(DEBUG_ADV,tmp);

				if (m_pHardware)
				{
					m_pHardware->InsertString(-1, m_xlDrvConfig.channel[i].name);
				}

				appChannel++;
			}

		}
	}
	// application is registered, get two CAN channels which are assigned
	else
	{

		// get the first channel (check if channel is assigned)
		m_xlChannelMask[CHAN01] = xlGetChannelMask(hwType, hwIndex, hwChannel);
		sprintf (tmp, "Found CAN in VHWConf, hWType: %d, CM: 0x%I64x", hwType, m_xlChannelMask[CHAN01]);
		DEBUG(DEBUG_ADV,tmp);

		for( int i=0; i < m_xlDrvConfig.channelCount; i++)
		{

			if ((m_xlDrvConfig.channel[i].channelMask == m_xlChannelMask[CHAN01]) &&
				(m_xlDrvConfig.channel[i].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN))
			{
					m_pHardware->AddString(m_xlDrvConfig.channel[i].name);
					chan1Found = 1;
					break;
			}
		}

		if (!chan1Found)
		{
			sprintf (tmp, "No assigned CAN in VHWConf for channel 0, hWType: %d, CM: 0x%I64x", hwType, m_xlChannelMask[CHAN01]);
			DEBUG(DEBUG_ADV,tmp);
			return XL_ERROR;
		}

		// get the second channel
		xlStatus = xlGetApplConfig(m_AppName, CHAN02, &hwType, &hwIndex, &hwChannel, busType); 
		if (xlStatus)
			return xlStatus;

		m_xlChannelMask[CHAN02] = xlGetChannelMask(hwType, hwIndex, hwChannel);
		sprintf (tmp, "Found CAN in VHWConf, hWType: %d, CM: 0x%I64x", hwType, m_xlChannelMask[CHAN02]);
		DEBUG(DEBUG_ADV,tmp);

		for( int i=0; i < m_xlDrvConfig.channelCount; i++)
		{
			if ((m_xlDrvConfig.channel[i].channelMask == m_xlChannelMask[CHAN02]) &&
				(m_xlDrvConfig.channel[i].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN))
			{
				m_pHardware->AddString(m_xlDrvConfig.channel[i].name);
				chan2Found = 1;
				break;
			}
		}

		if (!chan2Found)
		{
			sprintf (tmp, "No assigned CAN in VHWConf for channel 1, hWType: %d, CM: 0x%I64x", hwType, m_xlChannelMask[CHAN01]);
			DEBUG(DEBUG_ADV,tmp);
			return XL_ERROR;
		}
	}

	return xlStatus;
}

XLstatus CCANStandAloneFunc::canInitDriver(XLaccess *pxlChannelMaskTx, unsigned char *pxlChannelIndex)
{
	XLstatus          xlStatus;
//	XLaccess          xlChannelMaskTx = 0;
	unsigned int      i;
	
	
	// ------------------------------------
	// open the driver
	// ------------------------------------
	xlStatus = xlOpenDriver ();
	
	// ------------------------------------
	// get/print the hardware configuration
	// ------------------------------------
	if (XL_SUCCESS == xlStatus)
	{
		xlStatus = xlGetDriverConfig(&m_xlDrvConfig);
	}
	
	if (XL_SUCCESS == xlStatus)
	{
#ifdef _DEBUG
		demoPrintConfig();
#endif
//		TRACE("Usage: xlCANdemo <BaudRate> <ApplicationName> <Identifier>\n\n");
		
		// ------------------------------------
		// select the wanted channels
		// ------------------------------------
		g_xlChannelMask = 0;
		for( int i = 0; i < m_xlDrvConfig.channelCount; i++)
		{
			// we take all hardware we found and
			// check that we have only CAN cabs/piggy's
			// at the moment there is no VNss8910 XLAPI support!
			if (m_xlDrvConfig.channel[i].channelBusCapabilities & XL_BUS_ACTIVE_CAP_CAN)
			{
				if (!*pxlChannelMaskTx)
				{
					*pxlChannelMaskTx = m_xlDrvConfig.channel[i].channelMask;
					*pxlChannelIndex  = m_xlDrvConfig.channel[i].channelIndex;
				}
				
				g_xlChannelMask |= m_xlDrvConfig.channel[i].channelMask;

				if (m_pHardware)
				{
					m_pHardware->InsertString(-1, m_xlDrvConfig.channel[i].name);
				}
			}
		}
		
		if (!g_xlChannelMask)
		{
			TRACE("ERROR: no available channels found! (e.g. no CANcabs...)\n\n");
			xlStatus = XL_ERROR;
		}
	}
	
	m_xlPermissionMask = g_xlChannelMask;
	
	// ------------------------------------
	// open ONE port including all channels
	// ------------------------------------
	if (XL_SUCCESS == xlStatus)
	{
		xlStatus = xlOpenPort(&m_xlPortHandle, m_AppName, g_xlChannelMask, &m_xlPermissionMask, RX_QUEUE_SIZE, XL_INTERFACE_VERSION, XL_BUS_TYPE_CAN);
		TRACE("- OpenPort         : CM=0x%I64x, PH=0x%02X, PM=0x%I64x, %s\n"
			, g_xlChannelMask
			, m_xlPortHandle
			, m_xlPermissionMask
			, xlGetErrorString(xlStatus));
	}
	
	if ((XL_SUCCESS == xlStatus) && (XL_INVALID_PORTHANDLE != m_xlPortHandle))
	{
		// ------------------------------------
		// if we have permission we set the
		// bus parameters (baudrate)
		// ------------------------------------
		if (g_xlChannelMask == m_xlPermissionMask)
		{
			xlStatus = xlCanSetChannelBitrate(m_xlPortHandle, g_xlChannelMask, m_BaudRate);
			TRACE("- SetChannelBitrate: baudr.=%u, %s\n", m_BaudRate, xlGetErrorString(xlStatus));
		} 
		else
		{
			printf("-                  : we have NO init access!\n");
		}
		
	}
	else
	{
		
		xlClosePort(m_xlPortHandle);
		m_xlPortHandle = XL_INVALID_PORTHANDLE;
		xlStatus = XL_ERROR;
	}
	
	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! canInit

//! xlCANcontrol use ONE port for both channels.
//!
//////////////////////////////////////////////////////////////////////////// 

XLstatus CCANStandAloneFunc::canInit()
{
	XLstatus         xlStatus = XL_ERROR;
	XLaccess         xlPermissionMask;
	char             tmp[100];

	// ---------------------------------------
	// Open ONE port for both channels 
	// ---------------------------------------

	// calculate the channelMask for both channel 
	m_xlChannelMask_both = m_xlChannelMask[CHAN01] + m_xlChannelMask[CHAN02];
	xlPermissionMask = m_xlChannelMask_both;

	xlStatus = xlOpenPort(&m_xlPortHandle, m_AppName, m_xlChannelMask_both, &xlPermissionMask, 256, XL_INTERFACE_VERSION, XL_BUS_TYPE_CAN); 
	sprintf(tmp, "xlOpenPort: PortHandle: %d; Permissionmask: 0x%I64x; Status: %d", m_xlPortHandle, xlPermissionMask, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	if (m_xlPortHandle == XL_INVALID_PORTHANDLE)
		return XL_ERROR;
	if (xlStatus == XL_ERR_INVALID_ACCESS)
		return xlStatus;

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! canCreateRxThread

//! set the notification and creates the thread.
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANStandAloneFunc::canCreateRxThread()
{

	XLstatus      xlStatus = XL_ERROR;
	DWORD         ThreadId=0;
	char          tmp[100];

	if (m_xlPortHandle!= XL_INVALID_PORTHANDLE)
	{
		// Send a event for each Msg!!!
		xlStatus = xlSetNotification (m_xlPortHandle, &m_hMsgEvent, 1);
		sprintf(tmp, "SetNotification '%d', xlStatus: %d", m_hMsgEvent, xlStatus);
		DEBUG(DEBUG_ADV, tmp);

		// for the RxThread
		g_th.xlPortHandle = m_xlPortHandle;
		g_th.hMsgEvent    = m_hMsgEvent; 
		g_th.pOutput      = m_pOutput;

		m_hThread = CreateThread(0, 0x1000, RxThread, (LPVOID) &g_th, 0, &ThreadId);
		sprintf(tmp, "CreateThread %d", m_hThread);
		DEBUG(DEBUG_ADV, tmp);

	}
	return xlStatus;
}


////////////////////////////////////////////////////////////////////////////

//! CANGoOnBus

//! set the selected baudrate and go on bus.
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANStandAloneFunc::CANGoOnBus(unsigned long baudrate)
{
	XLstatus      xlStatus = XL_ERROR;
	char          tmp[100];

/*
	xlStatus = xlCanSetChannelBitrate(m_xlPortHandle, m_xlChannelMask_both, baudrate);
*/
	xlStatus = xlCanSetChannelBitrate(m_xlPortHandle, g_xlChannelMask, baudrate);
	sprintf(tmp, "SetBaudrate: %d, stat: %d", baudrate, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

/*
	xlStatus = xlActivateChannel(m_xlPortHandle, m_xlChannelMask_both, XL_BUS_TYPE_CAN, XL_ACTIVATE_RESET_CLOCK);
*/
	xlStatus = xlActivateChannel(m_xlPortHandle, g_xlChannelMask, XL_BUS_TYPE_CAN, XL_ACTIVATE_RESET_CLOCK);
	sprintf(tmp, "ActivateChannel, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANGoOffBus

//! Deactivate the channel
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANStandAloneFunc::CANGoOffBus()
{
	XLstatus      xlStatus = XL_ERROR;
	char          tmp[100];

/*
	xlStatus = xlDeactivateChannel(m_xlPortHandle, m_xlChannelMask_both);
*/
	xlStatus = xlDeactivateChannel(m_xlPortHandle, g_xlChannelMask);
	sprintf(tmp, "DeactivateChannel, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANSend

//! transmit a CAN message to the selected channel with the give values.
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANStandAloneFunc::CANSend(XLevent xlEvent, int channel)
{

	XLstatus      xlStatus;
	char          tmp[100];
	unsigned int  messageCount = 1;


	// Added by 224 (2014/07/13) : 선택한 채널로 데이터 보내기 기능 
	// 이전 채널과 다른 경우면, 벼경하는것이 좋기는 한데, 속도차이는 없을 듯 하여
	// 호출 시 마다 Tx 마스크 변경
	if (m_nChannel != channel)
	{
		m_nChannel = channel ;
		m_xlChanMaskTx = m_xlDrvConfig.channel[channel].channelMask;
	}

/*
	xlStatus = xlCanTransmit(m_xlPortHandle, m_xlChannelMask[channel], &messageCount, &xlEvent);
*/
	xlStatus = xlCanTransmit(m_xlPortHandle, m_xlChanMaskTx, &messageCount, &xlEvent);
	sprintf(tmp, "Transmit, mc: %d, channel: %d, stat: %d",  messageCount, channel, xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANResetFilter

//! Reset the acceptancefilter
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANStandAloneFunc::CANResetFilter()
{
	XLstatus      xlStatus;
	char          tmp[100];

/*
	xlStatus = xlCanResetAcceptance(m_xlPortHandle, m_xlChannelMask_both, XL_CAN_STD);
*/
	xlStatus = xlCanResetAcceptance(m_xlPortHandle, g_xlChannelMask, XL_CAN_STD);
	sprintf(tmp, "CanResetAcceptance, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	return xlStatus;
}

////////////////////////////////////////////////////////////////////////////

//! CANSetFilter

//! Reset the acceptancefilter
//!
////////////////////////////////////////////////////////////////////////////

XLstatus CCANStandAloneFunc::CANSetFilter(unsigned long first_id, unsigned long last_id)
{
	XLstatus      xlStatus;
	char          tmp[100];

	// because there all filters open, we close all.
/*
	xlStatus = xlCanSetChannelAcceptance(m_xlPortHandle, m_xlChannelMask_both, 0xFFF, 0xFFF, XL_CAN_STD);
*/
	xlStatus = xlCanSetChannelAcceptance(m_xlPortHandle, g_xlChannelMask, 0xFFF, 0xFFF, XL_CAN_STD);
	sprintf(tmp, "CanSetChannelAcceptance, stat: %d",  xlStatus);
	DEBUG(DEBUG_ADV, tmp);

	// and now we can set the acceptance filter range.
/*
	xlStatus = xlCanAddAcceptanceRange(m_xlPortHandle, m_xlChannelMask_both, first_id, last_id);
*/
	xlStatus = xlCanAddAcceptanceRange(m_xlPortHandle, g_xlChannelMask, first_id, last_id);
	sprintf(tmp, "CanAddAcceptanceRange, firstID: %d, lastID: %d, stat: %d",  first_id, last_id, xlStatus);
	DEBUG(DEBUG_ADV, tmp);


	return xlStatus;
}

///////////////////////////////////////////////////////////////////////////

//! ShowLicenses

//! Reads licenses from the selected channels and displays it.
//!
////////////////////////////////////////////////////////////////////////////
XLstatus CCANStandAloneFunc::ShowLicenses()
{
	XLstatus xlStatus;
	char licAvail[2048];
	char strtmp[512];

	// Show available licenses
	XLlicenseInfo licenseArray[1024];
	unsigned int licArraySize = 1024;
	xlStatus = xlGetLicenseInfo(m_xlChannelMask[CHAN01] | m_xlChannelMask[CHAN02], licenseArray, licArraySize);
	if (xlStatus == XL_SUCCESS)
	{
		strcpy(licAvail, "Licenses found:\n\n");
		for (unsigned int i = 0; i < licArraySize; i++)
		{
			if (licenseArray[i].bAvailable) {
				sprintf(strtmp, "ID 0x%03x: %s\n", i, licenseArray[i].licName);
				if ((strlen(licAvail) + strlen(strtmp)) < sizeof(licAvail))
				{
					strcat(licAvail, strtmp);
				}
				else
				{
					// Too less memory for printing licenses
					sprintf(licAvail, "Internal Error: String size in CCANStandAloneFunc::ShowLicenses() is too small!");
					xlStatus = XL_ERROR;
				}
			}
		}
	}
	else
	{
		sprintf(licAvail, "Error %d when calling xlGetLicenseInfo()!", xlStatus);
	}

	AfxMessageBox(licAvail);
	return xlStatus;
}


///////////////////////////////////////////////////////////////////////////

//! RxThread

//! thread to readout the message queue and parse the incoming messages
//!
////////////////////////////////////////////////////////////////////////////

DWORD WINAPI RxThread(LPVOID par) 
{

	XLstatus        xlStatus;

	unsigned int    msgsrx = 1;
	XLevent         xlEvent; 
	char            tmp[110];
	CString         str;

	g_bThreadRun = TRUE;

	TStruct *g_th;

	g_th = (TStruct*) par;  

	sprintf(tmp, "thread: SetNotification '%d'", g_th->hMsgEvent);
	DEBUG(DEBUG_ADV, tmp);

	while (g_bThreadRun)
	{ 

		WaitForSingleObject(g_th->hMsgEvent,1000);

		xlStatus = XL_SUCCESS;

		while (!xlStatus)
		{

			if (!g_bThreadRun)
			{
				break ;
			}

			msgsrx = 1;
			xlStatus = xlReceive(g_th->xlPortHandle, &msgsrx, &xlEvent);	

			if (xlStatus != XL_ERR_QUEUE_IS_EMPTY && g_th->pOutput)
			{

				sprintf(tmp, "%s", xlGetEventString(&xlEvent));
				DEBUG(DEBUG_ADV, tmp);
				g_th->pOutput->InsertString(-1, tmp);

				g_th->pOutput->SetCurSel(g_th->pOutput->GetCount()-1);
			}

		}

	}

	return NO_ERROR; 
}
