// UartFunctions.cpp : implementation file
//

#include "stdafx.h" 
#include "../CTSMonPro.h"
#include "UartFunctions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CUartFunctions

CUartFunctions::CUartFunctions()
{
	m_bValid = FALSE ;
}

CUartFunctions::~CUartFunctions()
{
	if (m_bValid)
	{
		sio_close(m_nComPort) ;
	}
}


BEGIN_MESSAGE_MAP(CUartFunctions, CWnd)
	//{{AFX_MSG_MAP(CUartFunctions)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CUartFunctions message handlers


BOOL CUartFunctions::UartInit(CUartCanConvBroker* pBroker, INT nComport, INT nBaudrate, INT nDataBit, INT nParity, INT nStopbit)
{
	ASSERT(pBroker != NULL) ;
	m_pBroker   = pBroker ;

	m_nComPort  = nComport ;
	m_nBaudrate = nBaudrate ;
	m_nDataBit  = nDataBit ;
	m_nParity   = nParity ;
	m_nStopBit  = nStopbit ;

	// open port w/ parameter
	if (sio_open(m_nComPort) == SIO_OK)
	{
		sio_ioctl(m_nComPort, B19200, BIT_8);
		sio_SetReadTimeouts(m_nComPort, 0, 50) ;

		CString str ;
		str.Format("Port %d Open", m_nComPort) ;
		
		TRACE("Port %d Open\r\n", m_nComPort);
		m_bValid = TRUE ;
	}
	else
	{
		TRACE("Port %d Failed\r\n", m_nComPort);
		
		CString str ;
		str.Format("Port %d Failed", m_nComPort) ;

		// 호출 하는 곳에서 오류 메시지 박스를 띄운다		
		
		m_bValid = FALSE ;
		return FALSE ;
	}

	// 로드셀 Rx Thread 생성
	DWORD ThreadId = 0;
	m_bThreadRun = TRUE;
	HANDLE hThread = CreateThread(0, 0x1000, RxThread, (LPVOID)this, 0, &ThreadId);

	return TRUE ;
}

void CUartFunctions::SendPacket(CHAR* packet, INT len)
{
	INT ret = sio_write(m_nComPort, packet, len) ;

	ASSERT(ret == len) ;
}

BOOL CUartFunctions::DataParsing(CHAR* buf, INT len)
{
#ifdef _DEBUG
	int nLoop ;
	for(int nLoop = 0; nLoop < len; nLoop++)
	{
		TRACE("[0x%02x] ", (BYTE)buf[nLoop]) ;
	}
	TRACE("\n") ;
#endif 

	ASSERT(len > 4) ;
	ASSERT(buf[0] == SYNC && buf[1] == SYNC && buf[2] == SYNC && buf[3] == SYNC) ;
	ASSERT(buf[4] == STX && buf[len-1] == ETX) ;

	if (buf[4] != STX && buf[len-1] != ETX)
	{
		// invalid packet
		return FALSE ;
	}

	// check LRC
	// cmd
	CHAR cmd = buf[5] ;
	INT dlen = buf[7] << 8 | buf[6] ;
	CHAR rslt = buf[8] ;

// 	if (rslt != 0x00)
// 	{
// 		// BMS error ;
// 		return FALSE ;
// 	}

	CHAR lrc = 0 ;
	for (int idx=5; idx < dlen + 5+3; idx++)
	{
		lrc ^= buf[idx] ;
	}

	ASSERT(lrc == buf[len-2]) ;
	if (lrc != buf[len-2])
	{
		return FALSE ;
	}

	m_pBroker->ProcessUartData(cmd, rslt, &buf[9], dlen-1) ;	

	return TRUE ;
}

DWORD WINAPI CUartFunctions::RxThread(LPVOID par) 
{
	CUartFunctions* pThis = (CUartFunctions*)par ;
	pThis->m_bThreadRun = TRUE;
	
	BYTE read_data[80];
	int read_data_num = 0 ;

	INT idx = 0 ;
	while (pThis->m_bThreadRun)
	{ 
		read_data_num = sio_read(pThis->m_nComPort, (CHAR*)&read_data[0], 80);
		if (read_data_num < 0)
		{
			// 쓰레드 종료한다.
			break ;
		}
		
		for (INT nLoop = 0; nLoop < read_data_num; nLoop++)
		{
			if (read_data[nLoop] == 0x16 && (nLoop + 3 < read_data_num ) && read_data[nLoop+1] == 0x16 && read_data[nLoop+2] == 0x16 && read_data[nLoop+3] == 0x16)
			{
				// STX 바이트인경우,
				idx = 0 ;
			}
			
			ASSERT(idx < MAXBUFFER) ;
			if (idx > MAXBUFFER)
			{
				break ;
			}
			
			pThis->m_Buffer[idx] = read_data[nLoop] ;
			idx++ ;
			
			// packet 중에 ETX 가 포함될 수 있다.
			if (read_data[nLoop] == 0x03)
			{
				if (idx > 8)
				{
					// dlen 까지 값을 읽은 경우, 패킷 사이즈를 계산한다. 
					INT dlen = pThis->m_Buffer[7] << 8 | pThis->m_Buffer[6] ; 
					
					// 패킷보다 작으면 계속 진행한다.
					if (idx < 9 + dlen)
					{
						continue ;
					}
				}
				else
				{
					// 패킷보다 작으면 계속 진행한다.
					continue ;
				}

				pThis->DataParsing(&pThis->m_Buffer[0], idx) ;
				idx = 0 ;
			}
		}
		
		//		DataParsing(read_data, read_data_num, nEulerDataLen);
	}
	
	return 0 ;
}
