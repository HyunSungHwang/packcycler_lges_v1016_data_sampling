// CanConvBroker.cpp: implementation of the CCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "../CTSMonPro.h"
#include "CanConvBroker.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCanConvBroker::CCanConvBroker()
{
	// 변수 초기화
	m_bCanRxThreadRun = FALSE ;
	m_bShowMessage = FALSE ;

	// 종료 이벤트
	m_hCanRxThreadTerminated  = INVALID_HANDLE_VALUE ;
}

CCanConvBroker::~CCanConvBroker()
{

}
