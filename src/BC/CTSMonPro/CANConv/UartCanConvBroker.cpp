// UartCanConvBroker.cpp: implementation of the CUartCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "../CTSMonPro.h"

#include "CanConvBroker.h"
#include "../CyclerModule.h"
#include "../CyclerChannel.h"
#include "UartCanConvBroker.h"
#include "UartFunctions.h"
#include "../CTSMonProDoc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define GETBIT(data, index) ((data & (1 << index)) >> index)
#define SETBIT(data, index) (data = data | (1 << index))

//BOOL g_bCanRxThreadRun[4] ;
//extern BOOL    g_bCANThreadRun;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUartCanConvBroker::CUartCanConvBroker()
{
	// 변수 초기화
//	m_bCanRxThreadRun = FALSE ;
	m_bSendCANData = FALSE ;
	m_nUartMode = UCC_STOP ;

	m_bUartRxThreadRun = FALSE ;
	m_bCanRxThreadRun = FALSE ;
//	m_bShowMessage = FALSE ;
	m_pRxList = NULL ;
	m_pTxList = NULL ;

	m_pUart = NULL ;
	m_pCAN = NULL ;

	m_bSaveRxFlag = FALSE ;
	m_bSaveTxFlag = FALSE ;

	// 초기화를 해주는 방법 고민 필요
	// 예. Module 에는 반드시 두개의 채널이 있어야한다?
	m_nDevID = -1 ;
//	m_nUartID = -1 ;

	// Uart 스케쥴러 종료 이벤트
	m_hUartSchedulerTerminated = INVALID_HANDLE_VALUE ;
//	m_hUartRxThreadTerminated  = INVALID_HANDLE_VALUE ;
//	m_hCanRxThreadTerminated  = INVALID_HANDLE_VALUE ;

}

CUartCanConvBroker::~CUartCanConvBroker()
{
	if (m_pUart)
	{
		delete m_pUart ;
	}
	
	if (m_pCAN)
	{
		delete m_pCAN ;
	}
}

// Added by 224 (2014/10/09) : Uart-CAN Convert 쓰레드 구동
BOOL CUartCanConvBroker::BeginUartCanConvThread(int nDevID
											, CString strSettingName
											, INT nComPort
											, INT nBaudrate
											, INT nDataBit
											, INT nParity
											, INT nStopBit
											, CArray<PUCC_INFO, PUCC_INFO>& ucc
											, CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO>& sched
											, INT nCanChannel
											, INT nCanBaudrate)
{
//	CString strTemp,strCode;
// 	int		nLength;
// 	int i;

	// Pause 상태에서 재시작 하는 경우 처리
	if (m_nUartMode == UCC_PAUSE)
	{
		m_nUartMode = UCC_PLAY ;
		return TRUE ;
	}
	else if (m_nUartMode == UCC_PLAY)
	{
		ASSERT(FALSE) ;
		return FALSE ;
	}


	//----------------- converting array 설정 ----------------- 
	

	m_nDevID = nDevID ;
//	m_nUartID = nUartID ;

	//------------------------ Uart init ------------------------

	ASSERT(m_pUart == NULL) ;
	{
		CString str ;
		
		if (m_pUart == NULL)
		{
			m_pUart = new CUartFunctions ;
		}
		
		m_pUart->m_pRXBox     = m_pRxList ;
		m_pUart->m_pStatusBox = m_pRxList ;
//		XLstatus xlStatus = m_pUart->UartGetDevice() ;
		
//		ASSERT(m_nUartID != -1) ;
//		xlStatus = m_pUart->UartInit(this, m_nDevID * 2, Uartid);
		BOOL bInit = m_pUart->UartInit(this, nComPort, nBaudrate, nDataBit, nParity, nStopBit);
		if (bInit == FALSE)
		{
			//str.Format("ERROR: COM%d 포트를 열 수가 없습니다.", nComPort) ;
			str.Format(Fun_FindMsg("ReserveDlg_SetListItem_msg1","UARTCANCONVBROKER"), nComPort) ;//&&
			AfxMessageBox(str, MB_ICONSTOP) ;
			
// 			xlStatus = m_pUart->UartClose();
			delete m_pUart ;
			m_pUart = NULL ;

			// 4. converting array clearing
			INT nLoop = 0 ;
			for(int nLoop = 0; nLoop < ucc.GetSize(); nLoop++)
			{
				UCC_INFO* ptr = ucc.GetAt(nLoop) ;
				delete ptr ;
			}
			ucc.RemoveAll() ;
			
			for(int nLoop = 0; nLoop < sched.GetSize(); nLoop++)
			{
				UARTSCHEDULER_INFO* ptr = sched.GetAt(nLoop) ;
				delete ptr ;
			}
			sched.RemoveAll() ;

			return FALSE ;
		}
	}

	//------------------------ can init ------------------------
	ASSERT(m_pCAN == NULL) ;
	{
		m_pCAN = new CCANFunctions ;
		
		m_pCAN->m_pOutput = m_pRxList ;

		XLstatus xlStatus = m_pCAN->CANGetDevice() ;
		
		if (xlStatus != XL_SUCCESS)
		{
			TRACE("Unable to allocate xxx context\n");
			
			ASSERT(m_bShowMessage == TRUE && m_pRxList != NULL) ;
			if (m_bShowMessage)
			{
				m_pRxList->AddString("ERROR: You need CANcabs or CANPiggy's!") ;
				m_pRxList->SetCurSel(m_pRxList->GetCount()-1);  
			}
			
			ASSERT(FALSE) ;
			return FALSE ;
		}
		
		// init the CAN hardware
		if (m_pCAN->CANInit(this, m_nDevID))
		{
			ASSERT(FALSE) ;
			// 		m_ctlCanHardware.ResetContent();
			// 		m_ctlCanHardware.AddString("<ERROR> no HW!");
			// 		m_ctlTxList.AddString("You need two CANcabs/CANpiggy's");
			// 		m_btnOnBus.EnableWindow(FALSE);
			
			ASSERT(m_bShowMessage == TRUE && m_pTxList != NULL) ;
			if (m_bShowMessage)
			{
				m_pTxList->AddString("You need two CANcabs/CANpiggy's") ;
				m_pTxList->SetCurSel(m_pTxList->GetCount()-1);  
			}
			
			delete m_pCAN ;
			m_pCAN = NULL ;
			
			return FALSE ;
		}
		
		// go on bus
		//	int           nIndex=0;
		//	unsigned long bitrate[5] = {125000, 250000, 500000, 1000000};
		
		xlStatus = m_pCAN->CANGoOnBus(nCanBaudrate);
		if (!xlStatus)
		{
			m_bSendCANData = TRUE ;
		}
		else
		{
			m_pRxList->AddString("ERROR: CANGoOnBus() !") ;

			ASSERT(FALSE) ;
			m_bSendCANData = FALSE ;
			
			return FALSE ;
		}
	}

	//------------------------ can init ------------------------

	//----------------- converting array 설정 ----------------- 
	//
	// 	m_ucc_array.RemoveAll() ;
/*
	for (INT nLoop = 0; nLoop < Uartid.GetSize(); nLoop++)
	{
		UART_INFO* info = Uartid.GetAt(nLoop) ;
		
		m_Uartid_array.Add(info) ;
	}
*/

	INT nLoop = 0 ;
	ASSERT(m_ucc_array.GetSize() == 0) ;
	for(int nLoop = 0; nLoop < ucc.GetSize(); nLoop++)
	{
		UCC_INFO* info = ucc.GetAt(nLoop) ;
		
		m_ucc_array.Add(info) ;
	}
	
	ASSERT(m_schedule_array.GetSize() == 0) ;
	for(int nLoop = 0; nLoop < sched.GetSize(); nLoop++)
	{
		UARTSCHEDULER_INFO* info = sched.GetAt(nLoop) ;
		
		m_schedule_array.Add(info) ;
	}

	//------------------------ Uart 스케쥴러 init -------------------------

	m_bUartOnBus = TRUE ;
	UartCreateSchedulerThread() ;

	m_nUartMode = UCC_PLAY ;
	m_strUartSettingName = strSettingName ;


	// 아래의 CreateEvent() 에서 원인모를 오류로 인하여
	// Doc에서 Handle 을 생성하여 전달하도록 코드 수정함
	if (m_hUartSchedulerTerminated == INVALID_HANDLE_VALUE)
	{
		m_hUartSchedulerTerminated = CreateEvent(NULL, FALSE, TRUE, NULL) ;
//		DWORD dwErr = GetLastError() ;
	}

// 	if (m_hUartRxThreadTerminated== INVALID_HANDLE_VALUE)
// 	{
// 		m_hUartRxThreadTerminated = CreateEvent(NULL, FALSE, TRUE, NULL) ;
// //		DWORD dwErr = GetLastError() ;
// 	}

	if (m_hCanRxThreadTerminated == INVALID_HANDLE_VALUE)
	{
		m_hCanRxThreadTerminated = CreateEvent(NULL, FALSE, TRUE, NULL) ;
//		DWORD dwErr = GetLastError() ;
	}

	return TRUE ;
}


void CUartCanConvBroker::StopUartCanConvThread()
{
	m_nUartMode = UCC_STOP ;

	m_bShowMessage = FALSE ;

	if (m_pUart == NULL)
	{
		ASSERT (m_pCAN == NULL) ;
		
		// 이미 초기화 되어진 상태임.
		return ;
	}

	// 1. Uart 스케쥴러 종료 flag 설정.
	{
		m_bUartOnBus = FALSE ;
		
		// wait untill thread exiting
		//	Sleep(500) ;
		DWORD dw = WaitForSingleObject(m_hUartSchedulerTerminated, INFINITE) ;
		TRACE("Signaled m_hUartSchedulerTerminated\n") ;
		ASSERT(dw == WAIT_OBJECT_0) ;
	}

/*
	// 2. Uart 해제	
	if (m_pUart)
	{
		m_bUartRxThreadRun = FALSE ;
		
		// wait untill thread exiting
		DWORD dw = WaitForSingleObject(m_hUartRxThreadTerminated, INFINITE) ;
		TRACE("Signaled m_hUartRxThreadTerminated\n") ;
		ASSERT(dw == WAIT_OBJECT_0) ;
		
	}
*/
	
	if (m_pCAN)
	{
		m_bCanRxThreadRun = FALSE ;

		//		g_bCanRxThreadRun[m_nDevID] = FALSE ;
		
		//		m_pCAN->CANGoOffBus() ;
		//		DWORD dw = WaitForSingleObject(m_hCanRxTerminated, 3000) ;
		
		// wait untill thread exiting
		DWORD dw = WaitForSingleObject(m_hCanRxThreadTerminated, INFINITE) ;
		TRACE("Signaled m_hCanRxThreadTerminated\n") ;
		ASSERT(dw == WAIT_OBJECT_0) ;
		
	}
	
	// 3. CAN 해제
	// 3.1. CAN Rx 쓰레드 종료
			
	if (m_pCAN)
	{
		m_pCAN->CANClose() ;
		
		delete m_pCAN ;
		m_pCAN = NULL ;
	}

	if (m_pUart)
	{
		m_pUart->UartClose() ;
		
		delete m_pUart ;
		m_pUart = NULL ;
	}

	// 4. converting array clearing
	INT nLoop ;
// 	for(int nLoop = 0; nLoop < m_Uartid_array.GetSize(); nLoop++)
// 	{
// 		UART_INFO* ptr = m_Uartid_array.GetAt(nLoop) ;
// 		delete ptr ;
// 	}
// 	m_Uartid_array.RemoveAll() ;

	for(int nLoop = 0; nLoop < m_ucc_array.GetSize(); nLoop++)
	{
		UCC_INFO* ptr = m_ucc_array.GetAt(nLoop) ;
		delete ptr ;
	}
	m_ucc_array.RemoveAll() ;

	for(int nLoop = 0; nLoop < m_schedule_array.GetSize(); nLoop++)
	{
		UARTSCHEDULER_INFO* ptr = m_schedule_array.GetAt(nLoop) ;
		delete ptr ;
	}
	m_schedule_array.RemoveAll() ;
	
	m_strUartSettingName.Empty() ;	
}

void CUartCanConvBroker::DoNotShowMessage()
{
	// Commented by 224 (2104/10/15) :
	// Uart dialog 창을 닫을 때
	// 세개의 쓰레드에서 dialog의 컨트롤에 메시지 전시하는
	// 로직이 동기화가 되지 않으면, NULL 포인트 접근을 하여
	// 프로그램의 오류가 발생한다.

	// 현재 visual 이 활성화된 브로커에게만 호출한다.
	ASSERT(m_bShowMessage == TRUE) ;
	// Can Rx, Uart Rx, Uart Scheduler 쓰레드의 메시지 전시가 종료된 후에 리턴한다.

	m_bShowMessage = FALSE ;

	// CAN Rx 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pCAN != NULL) ;
	// 생성 실패시 나타날 수 있음
	// Can Rx 는 메시지 이벤트 발생시에만 작동하므로 기다릴 필요가 없다.
/*
	if (m_pCAN)
	{
		m_pCAN->m_bShowCycleDone = FALSE ;
		Sleep(0) ;
		
		while (!m_pCAN->m_bShowCycleDone)
		{
			Sleep(1) ;
		}
		
	}
*/
	TRACE("CAN Rx 메시지 종료 확인\n") ;

	// Uart Rx 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pUart != NULL) ;
	if (m_pUart)
	{
//		m_pUart->m_bShowRxCycleDone = FALSE ;
		Sleep(0) ;
		
// 		while (!m_pUart->m_bShowRxCycleDone)
// 		{
// 			Sleep(1) ;
// 		}
	}

	TRACE("Uart Rx 메시지 종료 확인\n") ;

	// Uart TX 메시지 종료 확인
	//--------------------------------------------
//	ASSERT(m_pUart != NULL) ;
	if (m_pUart)
	{
//		m_pUart->m_bShowTxCycleDone = FALSE ;
		Sleep(0) ;
		
// 		while (!m_pUart->m_bShowTxCycleDone)
// 		{
// 			Sleep(1) ;
// 		}
	}

	TRACE("Uart Tx 메시지 종료 확인\n") ;

	return ;
}

union {
	float f;
	unsigned char b[4];
} f;

void CUartCanConvBroker::ProcessUartData(CHAR cmd, CHAR rslt, CHAR* data, INT len)
{

	// Uart Convert To CAN 자료구조를 이용하여 CAN 에 값을 보낸다.
	if (rslt != 0x00)
	{
		if (m_bShowMessage)
		{
			CString str ;
			str.Format("CMD=0x%0x: FAIL ERRCODE=", cmd, data[0]) ;
			while (m_pRxList->GetCount() > 99)
			{
				m_pRxList->DeleteString(0) ;
			}
			INT idx = m_pRxList->AddString(str) ;
			m_pRxList->SetTopIndex(idx) ;
//			m_pRxList->SetCurSel(m_pRxList->GetCount()-1);  
		}
	}


	{
		CString str ;
		if (len == 1)
		{
			str.Format("CMD:0x%02X rslt:%d len=%d data[0]=0x%02X\n", cmd, rslt, len, (BYTE)data[0]) ;
		}
		else if (len == 2)
		{
			str.Format("CMD:0x%02X rslt:%d len=%d data[0]=0x%02X data[1]=0x%02X\n", cmd, rslt, len, (BYTE)data[0], (BYTE)data[1]) ;
		}
		else
		{
			str.Format("CMD:0x%02X rslt:%d len=%d data[0]=0x%02X data[1]=0x%02X data[2]=0x%02X data[3]=0x%02X\n"
				, cmd, rslt, len
				, (BYTE)data[0], (BYTE)data[1], (BYTE)data[2], (BYTE)data[3]) ;
		}
		OutputDebugString(str) ;
	}

	// 패킷 파싱...

	// cmd 와 UCC_INFO:cmd 와 동일한 것을 모두 찾는다.
	// UCC_INFO:canid 는 1개 이상이다.


	// CAN 전송은 map 으로 관리한다.
	CMapWordToPtr canmap ;

	// .1. 저장된 UCC_INFO의 수만큼 루프를 돈다.
	for (INT nLoop = 0; nLoop < m_ucc_array.GetSize(); nLoop++)
	{
		// .1.1. 인덱스의 값을 가져온다.
		UCC_INFO* ptr = m_ucc_array.GetAt(nLoop) ;

		// .1.2. 동일한 command 를 찾는다.
		if (cmd == ptr->cmd)
		{

			INT canid = ptr->canid ;

			// canid 오 map에서 찾는다 없으면 새로 생성한다.
			XLevent* pEvent = NULL ;
			if (FALSE == canmap.Lookup(canid, (void*&)pEvent))
			{
				pEvent = new XLevent ;
				memset(pEvent, 0, sizeof(XLevent));			

				// 기존 값 설정
				pEvent->tag                 = XL_TRANSMIT_MSG;
				pEvent->tagData.msg.dlc     = 8 ;
				pEvent->tagData.msg.flags   = 0;
				pEvent->tagData.msg.id      = canid ;

				canmap.SetAt(canid, pEvent) ;
			}

			if (ptr->cmd_startbit/ 8 + ptr->cmd_bitcounts/8 > len)
			{
				if (m_bShowMessage)
				{
					CString str ;
					str.Format("CMD=0x%0x: INVALID startbit, bitcounts", cmd) ;
					while (m_pRxList->GetCount() > 99)
					{
						m_pRxList->DeleteString(0) ;
					}
					INT idx = m_pRxList->AddString(str) ;
					m_pRxList->SetTopIndex(idx) ;
				}

				// 요청한 사이즈가 주어진 패킷의 범위를 초과한 경우
				continue ;
			}

			if (ptr->can_startbit + ptr->cmd_bitcounts > 64)
			{
				if (m_bShowMessage)
				{
					CString str ;
					str.Format("CMD=0x%0x: INVALID CAN size", cmd) ;
					while (m_pRxList->GetCount() > 99)
					{
						m_pRxList->DeleteString(0) ;
					}
					INT idx = m_pRxList->AddString(str) ;
					m_pRxList->SetTopIndex(idx) ;
				}

				// CAN 전송 최대 용량(8Bytes) 를 초과한 경우
				continue ;
			}

// 			ptr->cmd_startbit ;
// 			ptr->cmd_bitcounts ;
// 
// 			ptr->canid ;
// 			ptr->can_startbit ;
// 			ptr->cmd_bitcounts ;

			CHAR value[1024] ;
			if (ptr->datatype != 2)	// float 가 아닌경우
			{
				
				INT i = 0 ;
				for (int nIdx = ptr->cmd_startbit; nIdx < (ptr->cmd_startbit + ptr->cmd_bitcounts); nIdx++)
				{
					// ex. startbit = 11, bitcounts = 8 인경우
					// data[1]:4번째 비트부터 ~ data[2]:4 번째 비트까지 
					// 값을 가져온다.
					
					BOOL flag = GETBIT(data[nIdx/8], nIdx % 8) ; ;				
					if (flag)
					{
						// 예) nStartBit = 20, nBitCounts = 8, nValue = 255
						// idx=2 mod=[4,5,6,7], idx=3 mod=[0,1,2,3]
						int cix = (ptr->can_startbit + i) / 8 ;
						int mod = (ptr->can_startbit + i) % 8 ;
						
						// 해당 bit의 flag 값을 세트한다.
						SETBIT(pEvent->tagData.msg.data[cix], mod) ;
					}
					
					i++ ;
				}
			}
			else
			{
				// float 인 경우처리
				INT i = 0 ;
				CHAR buf[4] ;
				buf[0] = buf[1] = buf[2] = buf[3] = 0x00 ;
				for (int nIdx = ptr->cmd_startbit; nIdx < (ptr->cmd_startbit + ptr->cmd_bitcounts); nIdx++)
				{
					BOOL flag = GETBIT(data[nIdx/8], nIdx % 8) ; ;				
					if (flag)
					{
						// 예) nStartBit = 20, nBitCounts = 8, nValue = 255
						// idx=2 mod=[4,5,6,7], idx=3 mod=[0,1,2,3]
						int cix = i / 8 ;
						int mod = i % 8 ;

						SETBIT(buf[cix], mod) ;
					}
					
					i++ ;
				}
				
				INT n = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24) ;
				f.f = n ;

				{
					CString str ;
					INT idx = ptr->cmd_startbit / 8 ;
					str.Format("D[0]=0x%02X D[1]=0x%02X D[2]=0x%02X D[3]=0x%03X S32=%d FLOAT=%f"
						, data[idx], data[idx+1], data[idx+2], data[idx+3], n, f.f) ;
					OutputDebugString(str) ;
				}

				int cix = ptr->can_startbit / 8 ;
				pEvent->tagData.msg.data[cix]   = f.b[0] ;
				pEvent->tagData.msg.data[cix+1] = f.b[1] ;
				pEvent->tagData.msg.data[cix+2] = f.b[2] ;
				pEvent->tagData.msg.data[cix+3] = f.b[3] ;

			}
		}
	}

	// .2. 위 루프에서 저장한 XLEvent을 값들을 전송한다.
	for (INT nIdx = 0; nIdx < canmap.GetCount(); nIdx++)
	{
		POSITION pos = canmap.GetStartPosition() ;

		while (pos != NULL)
		{

			WORD canid ;
			XLevent* pEvent ;

			canmap.GetNextAssoc(pos, canid, (void*&)pEvent) ;
			ASSERT(pEvent != NULL) ;

			TRACE("xlEvent.tagData.msg.id      = 0x%02d\n", canid) ;
			TRACE("xlEvent.tagData.msg.data[0] = 0x%02x\n", pEvent->tagData.msg.data[0]) ;
			TRACE("xlEvent.tagData.msg.data[1] = 0x%02x\n", pEvent->tagData.msg.data[1]) ;
			TRACE("xlEvent.tagData.msg.data[2] = 0x%02x\n", pEvent->tagData.msg.data[2]) ;
			TRACE("xlEvent.tagData.msg.data[3] = 0x%02x\n", pEvent->tagData.msg.data[3]) ;
			TRACE("xlEvent.tagData.msg.data[4] = 0x%02x\n", pEvent->tagData.msg.data[4]) ;
			TRACE("xlEvent.tagData.msg.data[5] = 0x%02x\n", pEvent->tagData.msg.data[5]) ;
			TRACE("xlEvent.tagData.msg.data[6] = 0x%02x\n", pEvent->tagData.msg.data[6]) ;
			TRACE("xlEvent.tagData.msg.data[7] = 0x%02x\n", pEvent->tagData.msg.data[7]) ;


			SendCANData(*pEvent) ;

			delete pEvent ;
		}

	}

	// 3. 클리어
	canmap.RemoveAll() ;

}

//void CUartCanConvBroker::SendCANData(XLevent &xlEvent)
XLstatus CUartCanConvBroker::SendCANData(XLevent &xlEvent)
{
	//----------------------------------------------------------
	// CAN 으로 전송
	
/*
	XLevent xlEvent;	
	memset(&xlEvent, 0, sizeof(xlEvent));
	
	// 전송
	xlEvent.tag                 = XL_TRANSMIT_MSG;
	xlEvent.tagData.msg.dlc     = xlUartEvent.tagData.UartMsgApi.UartMsg.dlc ;
	xlEvent.tagData.msg.flags   = 0;
	xlEvent.tagData.msg.id      = xlUartEvent.tagData.UartMsgApi.UartMsg.id ;
	
	for (int nIdx = 0; nIdx < xlEvent.tagData.msg.dlc; nIdx++)
	{
		xlEvent.tagData.msg.data[nIdx] = xlUartEvent.tagData.UartMsgApi.UartMsg.data[nIdx] ;
	}
*/
	
/*
	TRACE("xlEvent.tagData.msg.id      = 0x%02x\n", xlEvent.tagData.msg.id) ;
	TRACE("xlEvent.tagData.msg.data[0] = 0x%02x\n", xlEvent.tagData.msg.data[0]) ;
	TRACE("xlEvent.tagData.msg.data[1] = 0x%02x\n", xlEvent.tagData.msg.data[1]) ;
	TRACE("xlEvent.tagData.msg.data[2] = 0x%02x\n", xlEvent.tagData.msg.data[2]) ;
	TRACE("xlEvent.tagData.msg.data[3] = 0x%02x\n", xlEvent.tagData.msg.data[3]) ;
	TRACE("xlEvent.tagData.msg.data[4] = 0x%02x\n", xlEvent.tagData.msg.data[4]) ;
	TRACE("xlEvent.tagData.msg.data[5] = 0x%02x\n", xlEvent.tagData.msg.data[5]) ;
	TRACE("xlEvent.tagData.msg.data[6] = 0x%02x\n", xlEvent.tagData.msg.data[6]) ;
	TRACE("xlEvent.tagData.msg.data[7] = 0x%02x\n", xlEvent.tagData.msg.data[7]) ;
*/


	ASSERT(m_bSendCANData) ;
	XLstatus  xlStatus = XL_SUCCESS ;

	if (m_pCAN != NULL)
	{
		xlStatus = m_pCAN->CANSend(xlEvent);
	}	

	TRACE("------------------------------------ CAN SEND ------------------------------------\n") ;
	return xlStatus ;

//	EnterCriticalSection(m_pcsShowMessage) ;
	if (m_bShowMessage)
	{
		ASSERT(m_pTxList != NULL) ;

		CString strCont ;
		strCont.Format("ID:%02d ", xlEvent.tagData.msg.id) ;
		for (int nIdx = 0; nIdx < xlEvent.tagData.msg.dlc; nIdx++)
		{
			CString str ;
			str.Format("[0x%02X] ", xlEvent.tagData.msg.data[nIdx]) ;
			
			strCont += str ;
		}
		
		// 로그 파일에 저장
		if (m_bSaveTxFlag)
		{
			CHAR buf[1024] ;
			
			strcpy(buf, strCont) ;
			strcat(buf, "\n") ;
			
			CTime ctCur = CTime::GetCurrentTime();
			CString csFilename;
			csFilename.Format(_T("tx_%04d%02d%02d.log"), ctCur.GetYear(), ctCur.GetMonth(), ctCur.GetDay());
			
			FILE* fp = fopen(csFilename, "a+") ;
			//			FILE* fp = fopen(".\\tx.log", "a");
			fwrite(buf, strlen(buf), 1, fp) ;
			fclose(fp) ;
		}	

		// commented by 224 (2014/10/09) :

		int cnt = m_pTxList->GetCount() ;
		while (cnt > 99)
		{
			if (m_pTxList == NULL)
				break ;

			m_pTxList->DeleteString(0) ;

			if (m_pTxList == NULL)
				break ;

			cnt = m_pTxList->GetCount() ;
		}
		
		if (m_pTxList)
		{
			m_pTxList->InsertString(-1, strCont) ;
		}

		if (m_pTxList)
		{
			m_pTxList->SetCurSel(m_pTxList->GetCount()-1);  
		}

	}

//	m_pUart->m_bShowTxCycleDone = TRUE ;

//	LeaveCriticalSection(m_pcsShowMessage) ;
	
	return xlStatus ;
	
}

void CUartCanConvBroker::UartCreateSchedulerThread()
{
//	XLstatus      xlStatus;
	DWORD         ThreadId=0;
	char          tmp[100];
	
	if (m_pUart->IsValid())
	{
		
		HANDLE hThread ;
		hThread = CreateThread(0, 0x1000, UartSchedulerThread, (LPVOID) this, 0, &ThreadId);
		sprintf(tmp, "CreateThread %d", hThread);
		//		DEBUG(DEBUG_ADV, tmp);
		
	}
	
}

#define CHECKCONTINUOUS(delay) \
	if ((pBroker->m_bUartOnBus))\
{\
	Sleep(delay) ;\
}\
	else \
{\
	break ;\
}

DWORD WINAPI UartSchedulerThread(PVOID param)
{
	CUartCanConvBroker* pBroker = (CUartCanConvBroker*)param ;
	CUartFunctions* pUart = pBroker->m_pUart ; 
	//	CUartFunctions* pUart = &pDoc->m_Uart ; 
	

//	CHAR packet[MAXBUFFER] = { 0x16, 0x16, 0x16, 0x16, 0x02, 0x43, 0x00, 0x00, 0x43, 0x03 } ;
	CHAR packet[MAXBUFFER] ;

	if (pBroker->m_schedule_array.GetSize() > 0)
	{
		while (pBroker->m_bUartOnBus)
		{
			
			
			for (INT nLoop = 0; nLoop < pBroker->m_schedule_array.GetSize(); nLoop++)
			{
				UARTSCHEDULER_INFO* pInfo = pBroker->m_schedule_array.GetAt(nLoop) ;
				
//				pUart->SendCommand(0, pInfo->id) ;

				packet[0] = packet[1] = packet[2] = packet[3] = SYNC ;
				packet[4] = STX ;
				packet[5] = pInfo->id ;			// cmd
				packet[6] = packet[7] = 0x00 ;	// dlen
				packet[8] = pInfo->id ;			// lrc
				packet[9] = ETX ;

				pUart->SendPacket(packet, 10) ;

				
				CHECKCONTINUOUS(pInfo->delay) ;
				
			}

			Sleep(0) ;
				
		}

		
	}
	
	SetEvent(pBroker->m_hUartSchedulerTerminated) ;
	
	TRACE("Exiting SchedulerThread()\n") ;
	
	return 0 ;
}
