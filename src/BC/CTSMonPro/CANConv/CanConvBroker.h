// CanConvBroker.h: interface for the CCanConvBroker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CANCONVBROKER_H__BBBB8176_DF8E_4907_B8B4_E83F809739CF__INCLUDED_)
#define AFX_CANCONVBROKER_H__BBBB8176_DF8E_4907_B8B4_E83F809739CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCanConvBroker  
{
public:
	CCanConvBroker();
	virtual ~CCanConvBroker();

	BOOL m_bCanRxThreadRun ;
	BOOL m_bShowMessage ;
	HANDLE m_hCanRxThreadTerminated ;

};

#endif // !defined(AFX_CANCONVBROKER_H__BBBB8176_DF8E_4907_B8B4_E83F809739CF__INCLUDED_)
