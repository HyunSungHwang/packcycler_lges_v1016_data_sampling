// LINFunctions.h: interface for the CLINFunctions class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINFUNCTIONS_H__CFE0F1C7_2CCA_4700_8EA9_F8BE82F0BDA6__INCLUDED_)
#define AFX_LINFUNCTIONS_H__CFE0F1C7_2CCA_4700_8EA9_F8BE82F0BDA6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "vxlapi.h"
#include "LinCanConvBroker.h"

#define MAXPORT 8
#define RECEIVE_EVENT_SIZE       1     // DO NOT EDIT! Currently 1 is supported only
// #define MASTER                   1     //!< channel is a master
// #define SLAVE                    2     //!< channel is a slave
#define DEFAULT_LIN_DLC          8     //!< default DLC for master/slave
//#define DEFAULT_LIN_BAUDRATE 16500     //!< default LIN baudrate
#define DEFAULT_LIN_BAUDRATE 19200     //!< default LIN baudrate

class CLinCanConvBroker ;
class CLINFunctions;
typedef struct {
    XLportHandle xlPortHandle; 
    HANDLE      hMsgEvent;
    CListBox	  *pStatusBox;
    CListBox    *pListRX;
	//	CCTSMonProDoc* pDoc ;
	CLINFunctions* pLin ;
	CLinCanConvBroker* pBroker ;
} LINTStruct;

class CLinCanConvBroker ;
class CLINFunctions  
{
public:
	CLINFunctions();
	virtual ~CLINFunctions();
	
	XLstatus LINGetDevice();
//	XLstatus LINInit(CCTSMonProDoc* pDoc, int linID);
//	XLstatus LINInit(CLinCanConvBroker* pBroker, int nChannel, int linID);
	XLstatus LINInit(CLinCanConvBroker* pBroker, int nChannel, CArray<PLINID_INFO, PLINID_INFO>& linid);

	XLstatus LINSendMasterReq(BYTE data, int linID);

	XLstatus LINClose();
	
	CListBox        *m_pRXBox;
	CListBox        *m_pStatusBox;
	
//	BOOL m_bLinRxThreadRun ;	
	BOOL IsValid() { ASSERT(this) ; return (m_xlPortHandle != XL_INVALID_PORTHANDLE) ; }

	BOOL m_bShowRxCycleDone ;	// lin dialog 종료 시 메시지 동기화 플래그
	BOOL m_bShowTxCycleDone ;	// lin dialog 종료 시 메시지 동기화 플래그

	CLinCanConvBroker* GetConvBroker() { ASSERT(this) ; return m_pBroker ; }
	XLportHandle  GetPortHandle() { ASSERT(this) ; return m_xlPortHandle; }

private:
	XLstatus         linGetChannelMask();
	XLstatus         linInitMaster(CArray<PLINID_INFO, PLINID_INFO>& linid);
	XLstatus         linInitSlave(int linID, int nChannel);
	XLstatus         linCreateRxThread(); 
	void linCreateSchedulerThread();
	XLstatus         linSetSlave(int linID, byte databyte, int MASTERORSLAVE = 1);
	
	XLaccess         m_xlChannelMask[MAXPORT];
	XLportHandle     m_xlPortHandle;
	
	XLhandle         m_hMsgEvent;
	HANDLE           m_hThread;

//	CCTSMonProDoc* m_pDoc ;
	CLinCanConvBroker* m_pBroker ;
	INT m_nChannel ;
	LINTStruct m_LinTH;

};

DWORD     WINAPI LinRxThread( PVOID par );

#endif // !defined(AFX_LINFUNCTIONS_H__CFE0F1C7_2CCA_4700_8EA9_F8BE82F0BDA6__INCLUDED_)
