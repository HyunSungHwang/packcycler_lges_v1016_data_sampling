// ModbusSettingDBDialog.cpp : implementation file
//

#include "stdafx.h" 
#include "../resource.h"
#include "ModbusSettingDBDialog.h"
#include "CppSQLite3.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModbusSettingDBDialog dialog


CModbusSettingDBDialog::CModbusSettingDBDialog(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CModbusSettingDBDialog::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CModbusSettingDBDialog::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CModbusSettingDBDialog::IDD3):
	CModbusSettingDBDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModbusSettingDBDialog)
	m_strName = _T("");
	m_strDesc = _T("");
	//}}AFX_DATA_INIT

	m_bSaveMode = FALSE ;
}


void CModbusSettingDBDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModbusSettingDBDialog)
	DDX_Control(pDX, IDC_DEL_RECORD, m_btnDelRecord);
	DDX_Control(pDX, IDC_DB_LIST, m_list);
	DDX_Text(pDX, IDC_RECORD_NAME, m_strName);
	DDX_Text(pDX, IDC_DESC, m_strDesc);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModbusSettingDBDialog, CDialog)
	//{{AFX_MSG_MAP(CModbusSettingDBDialog)
	ON_NOTIFY(NM_CLICK, IDC_DB_LIST, OnClickDbList)
	ON_BN_CLICKED(IDC_DEL_RECORD, OnDelRecord)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModbusSettingDBDialog message handlers

extern const char* gszConvDBFile ;

BOOL CModbusSettingDBDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (m_bSaveMode)
	{
		//SetWindowText("저장") ;
		SetWindowText(Fun_FindMsg("ModbusSettingDBDialog_OnInitDialog_msg1","IDD_MODBUS_DB_DIALOG")) ; //&&

	}
	else
	{
		//SetWindowText("불러오기") ;
		SetWindowText(Fun_FindMsg("ModbusSettingDBDialog_OnInitDialog_msg2","IDD_MODBUS_DB_DIALOG")) ; //&&

		CRect rcDialog ;
		GetWindowRect(rcDialog) ;
		int nNewHeight = -1 ;
		
		CWnd* pWndSmall = GetDlgItem(IDC_SMALL) ;
		ASSERT_VALID(pWndSmall) ;

		CRect rcSmall ;
		pWndSmall->GetWindowRect(rcSmall) ;

		nNewHeight = rcSmall.top-rcDialog.top ;
		SetWindowPos(NULL, 0, 0, rcDialog.Width(), nNewHeight, SWP_NOMOVE | SWP_NOZORDER) ;

		CEdit* pEdit = (CEdit*)GetDlgItem(IDC_RECORD_NAME) ;
		pEdit->SetReadOnly(TRUE) ;
	}

	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT) ;

	CRect rect ;
	m_list.GetWindowRect(&rect) ;
	//m_list.InsertColumn(0, "이름", LVCFMT_LEFT, 200) ;
	m_list.InsertColumn(0, Fun_FindMsg("ModbusSettingDBDialog_OnInitDialog_msg3","IDD_MODBUS_DB_DIALOG"), LVCFMT_LEFT, 200) ;//&&
	//m_list.InsertColumn(1, "설명", LVCFMT_LEFT, rect.Width()-200-4) ;
	m_list.InsertColumn(1, Fun_FindMsg("ModbusSettingDBDialog_OnInitDialog_msg4","IDD_MODBUS_DB_DIALOG"), LVCFMT_LEFT, rect.Width()-200-4) ;//&&

    try
    {
		CppSQLite3DB db;
		db.open(gszConvDBFile) ;
		

		if (!db.tableExists("TB_CONV_LIST") || !db.tableExists("TB_MODBUS_MAP"))
		{
			//MessageBox("잘못된 db 파일입니다") ;
			MessageBox(Fun_FindMsg("ModbusSettingDBDialog_OnInitDialog_msg5","IDD_MODBUS_DB_DIALOG")) ;//&&
			db.close() ;
			return FALSE ;
		}

		CppSQLite3Query q = db.execQuery("SELECT NAME, DESC FROM TB_CONV_LIST ORDER BY SEQ ;");
		INT nIdx = 0 ;
		while (!q.eof())
		{
			CString str ;

			str = q.fieldValue("NAME");
			m_list.InsertItem(nIdx, str) ;

			str = q.fieldValue("DESC") ;
			m_list.SetItemText(nIdx, 1, str) ;

			// 요청에 의해 최근 저장된 레코드가 맨 위로 나오게 한다.
//			nIdx++ ;
			q.nextRow() ;
		}
		q.finalize();

	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		MessageBox(e.errorMessage()) ;
    }
	
	// 저장인 경우, 현재 설정된 이름이 있으면 기본으로 선택하게 한다.
	if (m_bSaveMode && !m_strName.IsEmpty())
	{
		INT cnt = m_list.GetItemCount()	;

		for (int nLoop = 0; nLoop < cnt; nLoop++)
		{
			CString str = m_list.GetItemText(nLoop, 0) ;
			if (m_strName == str)
			{
				m_list.SetItemState(nLoop, LVIS_SELECTED, LVIS_SELECTED);
				break ;
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModbusSettingDBDialog::OnClickDbList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	NM_LISTVIEW* pView = (NM_LISTVIEW*) pNMHDR;

    *pResult = 0;
	
//     if (m_list.GetItemCount() <= 0 )		
// 	{
//         return;
// 	}
	
    if (pView->iItem < 0)
	{
		TRACE("No Selection... \n") ;
		return;
	}
	
	int nSelectedItem = m_list.GetNextItem( -1, LVNI_SELECTED );
    m_strName = m_list.GetItemText(nSelectedItem, 0) ;
	m_strDesc = m_list.GetItemText(nSelectedItem, 1) ;

	UpdateData(FALSE) ;
}

void CModbusSettingDBDialog::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData() ;

	if (m_strName.IsEmpty())
	{
		//MessageBox("선택한 데이터가 없습니다.") ;
		MessageBox(Fun_FindMsg("ModbusSettingDBDialog_OnOK_msg1","IDD_MODBUS_DB_DIALOG")) ;//&&
		return ;
	}
	
	CDialog::OnOK();
}

void CModbusSettingDBDialog::OnDelRecord() 
{
	// TODO: Add your control notification handler code here

	int nSelectedItem = m_list.GetNextItem( -1, LVNI_SELECTED );
	if (nSelectedItem < 0)
		return ;

	CString strName = m_list.GetItemText(nSelectedItem, 0) ;

	//if (IDNO == MessageBox("선택한 항목을 삭제하겠습니까?", NULL, MB_YESNO))
	if (IDNO == MessageBox(Fun_FindMsg("ModbusSettingDBDialog_OnDelRecord_msg1","IDD_MODBUS_DB_DIALOG"), NULL, MB_YESNO))//&&
	{
		return ;
	}

    try
    {
		CppSQLite3DB db;
		db.open(gszConvDBFile) ;
		
		if (!db.tableExists("TB_CONV_LIST") || !db.tableExists("TB_MODBUS_MAP"))
		{
			//MessageBox("잘못된 db 파일입니다") ;
			MessageBox(Fun_FindMsg("ModbusSettingDBDialog_OnDelRecord_msg2","IDD_MODBUS_DB_DIALOG")) ;//&&
			db.close() ;
			return ;
		}

		char buf[512] ;

		// 현재 입력한 이름의 SEQ의 값을 얻는다.
		CString strQuery ;
		strQuery.Format("SELECT SEQ FROM TB_CONV_LIST WHERE NAME = '%s'", strName) ;
		CppSQLite3Query q = db.execQuery(strQuery) ;
		ASSERT(!q.eof()) ;
		
		CString strSEQ = q.fieldValue("SEQ") ;
		q.finalize();
		
		// 기존에 설정되어 있던 동일한 TB_MODBUS_MAP 의 PARENTCD 항목은 모두 삭제한다.
		sprintf(buf
			, "DELETE FROM TB_MODBUS_MAP WHERE PARENTCD = %s"
			, strSEQ) ;
		db.execDML(buf) ;

		sprintf(buf
			, "DELETE FROM TB_CONV_LIST WHERE SEQ = %s"
			, strSEQ) ;
		db.execDML(buf) ;

		m_list.DeleteItem(nSelectedItem) ;
	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		MessageBox(e.errorMessage()) ;
    }


}