#if !defined(AFX_UARTFUNCTIONS_H__06DBF86F_81E6_4EA7_90C2_C648B8B360C0__INCLUDED_)
#define AFX_UARTFUNCTIONS_H__06DBF86F_81E6_4EA7_90C2_C648B8B360C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UartFunctions.h : header file
//

#include "Pcomm.h"
#include "UartCanConvBroker.h"

/////////////////////////////////////////////////////////////////////////////
// CUartFunctions window
#define STX		0x02
#define ETX		0x03
#define SYNC	0x16

#define MAXBUFFER	(256)

class CUartCanConvBroker ;
class CUartFunctions : public CWnd
{
// Construction
public:
	CUartFunctions();

// Attributes
public:
	CListBox        *m_pRXBox;
	CListBox        *m_pStatusBox;

	INT m_nComPort ;
	INT m_nBaudrate ;
	INT m_nDataBit ;
	INT m_nParity ;
	INT m_nStopBit ;

	CUartCanConvBroker* m_pBroker ;

// Operations
public:
	void UartClose() { ASSERT(this) ; sio_close(m_nComPort) ; m_bValid = FALSE ; }
//	BOOL UartInit(CUartCanConvBroker*, INT, CArray<PUART_INFO, PUART_INFO>& ) { ASSERT(this) ; return FALSE ; }
	BOOL UartInit(CUartCanConvBroker*, INT nComport, INT nBaudrate, INT nDataBit, INT nParity, INT nStopbit) ;

	BOOL IsValid() { ASSERT(this) ; return m_bValid ; }
	BOOL m_bValid ;

	void SendPacket(CHAR* packet, INT len) ;
	BOOL DataParsing(CHAR* buf, INT len) ;

	BOOL m_bThreadRun ;
	CHAR m_Buffer[MAXBUFFER] ;
	static DWORD WINAPI RxThread(LPVOID par) ; 

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUartFunctions)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CUartFunctions();

	// Generated message map functions
protected:
	//{{AFX_MSG(CUartFunctions)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UARTFUNCTIONS_H__06DBF86F_81E6_4EA7_90C2_C648B8B360C0__INCLUDED_)
