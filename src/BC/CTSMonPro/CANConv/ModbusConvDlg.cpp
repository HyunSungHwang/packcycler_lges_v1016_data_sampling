// ModbusConvDlg.cpp : implementation file
//


#include "stdafx.h" 
#include "../resource.h"

#include "ModbusConvDlg.h"
#include "modbus.h"
#include "CppSQLite3.h"

// #include <WinResrc.h>
// #include <WinBase.h>
#include <Mmsystem.h>
#include "../CyclerModule.h"
#include "ModbusSettingDBDialog.h"
#include "../CTSMonProView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

unsigned long bitrate[] = { 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200 };
unsigned long databit[] = { 7, 8 };
unsigned char parity[]  = { 'N', 'O', 'E' } ;
unsigned long stopbit[] = { 1, 2 };
#define GETBIT(data, index) ((data & (1 << index)) >> index)

const char* gszConvDBFile = ".\\MODBUSCONV2.db";
// const char* gszConvDBFile = "C:\\MODBUSCONV2.db";
			
/////////////////////////////////////////////////////////////////////////////
// CModbusConvDlg dialog
//HANDLE CModbusConvDlg::m_hEvent   = NULL ;
//BOOL   CModbusConvDlg::m_bRunning = FALSE ;

CModbusConvDlg::CModbusConvDlg()
{

}

CModbusConvDlg::CModbusConvDlg(CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CModbusConvDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CModbusConvDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CModbusConvDlg::IDD3):
	(CModbusConvDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CModbusConvDlg)
	m_strAddr = _T("");
	m_nResponseTimeout = 0;
	m_nPort = 0;
	m_nMBLen = 0;
	m_nMBScanRate = 0;
	m_nSlaveID = 0;
	m_nMBAddr = 0;
	m_eID = _T("");
	m_bCheckExtMode = FALSE;
	m_bSaveRx = FALSE;
	m_bSaveTx = FALSE;
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	m_bCanRecvRxTxMsg = FALSE ;
	ctx = NULL ;
}

void CModbusConvDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModbusConvDlg)
	DDX_Control(pDX, IDC_DATABITS, m_ctrlDatabits);
	DDX_Control(pDX, IDC_PARITYBIT, m_ctrlParitybit);
	DDX_Control(pDX, IDC_STOPBIT, m_ctrlStopbit);
	DDX_Control(pDX, IDC_MB_BAUDRATE, m_ctrlMBBaudrate);
	DDX_Control(pDX, IDC_CAN_CHANNEL, m_Channel);
	DDX_Control(pDX, IDC_COMBO_CAN_BAUDRATE, m_CAN_Baudrate);
	DDX_Control(pDX, IDC_TX_LIST, m_ctlTxList);
	DDX_Control(pDX, IDC_MODBUS_FUNCTION, m_ctlMBFunc);
	DDX_Control(pDX, IDC_RX_LIST, m_ctlRxList);
	DDX_Control(pDX, IDC_DEVICE_COMMUNICATION, m_ctlDeviceComm);
	DDX_Text(pDX, IDC_IP_ADDR, m_strAddr);
	DDX_Text(pDX, IDC_RESPONSE_TIMEOUT, m_nResponseTimeout);
	DDX_Text(pDX, IDC_IP_PORT, m_nPort);
	DDV_MinMaxUInt(pDX, m_nPort, 1, 9999);
	DDX_Text(pDX, IDC_MODBUS_LEN, m_nMBLen);
	DDX_Text(pDX, IDC_MODBUS_SCANRATE, m_nMBScanRate);
	DDX_Text(pDX, IDC_SLAVE_ID, m_nSlaveID);
	DDV_MinMaxUInt(pDX, m_nSlaveID, 1, 255);
	DDX_Text(pDX, IDC_MODBUS_ADDR, m_nMBAddr);
	DDX_Text(pDX, IDC_CANID, m_eID);
	DDX_Check(pDX, IDC_CHECK_EXT_MODE, m_bCheckExtMode);
	DDX_Check(pDX, IDC_SAVE_RX_PACKET, m_bSaveRx);
	DDX_Check(pDX, IDC_SAVE_TX_PACKET, m_bSaveTx);
	//}}AFX_DATA_MAP
}

IMPLEMENT_DYNCREATE(CModbusConvDlg, CDialog)

BEGIN_MESSAGE_MAP(CModbusConvDlg, CDialog)
	//{{AFX_MSG_MAP(CModbusConvDlg)
	ON_BN_CLICKED(IDC_RESET_RX_CONTENTS, OnResetRxContents)
	ON_BN_CLICKED(IDC_RESET_TX_CONTENTS, OnResetTxContents)
	ON_BN_CLICKED(IDC_OPER_START, OnOperStart)
	ON_BN_CLICKED(IDC_OPER_PAUSE, OnOperPause)
	ON_BN_CLICKED(IDC_OPER_STOP, OnOperStop)
	ON_BN_CLICKED(IDC_ADD_ROW, OnAddRow)
	ON_BN_CLICKED(IDC_DEL_ROW, OnDelRow)
	ON_BN_CLICKED(IDC_OPEN_CONV_DATA, OnOpenConvData)
	ON_BN_CLICKED(IDC_TRANS_CAN_DATA, OnTransCanData)
	ON_BN_CLICKED(IDC_SAVE_CONFIG, OnSaveConfig)
	ON_CBN_SELCHANGE(IDC_MODBUS_FUNCTION, OnSelchangeModbusFunction)
	ON_CBN_SELCHANGE(IDC_DEVICE_COMMUNICATION, OnSelchangeDeviceCommunication)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_BN_CLICKED(IDC_ETC_SETTING, OnEtcSetting)
	ON_WM_SYSCOMMAND()
	ON_BN_CLICKED(IDC_SAVE_RX_PACKET, OnSaveRxPacket)
	ON_BN_CLICKED(IDC_SAVE_TX_PACKET, OnSaveTxPacket)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_BEGINEDIT, OnGridBeginEdit)
	ON_MESSAGE(WM_GRID_ENDEDIT, OnGridEndEdit)
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModbusConvDlg message handlers
//#define _WIN32_WINNT 0x0501
BOOL CModbusConvDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//ljb 2011221 이재복 //////////
	//if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox("외부데이터와 CAN CODE 가져 오기 실패");
	if (m_pDoc->Fun_ReloadDivisionCode() == FALSE)	AfxMessageBox(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg1","IDD_MODBUS_CONV")); //&&
	m_pDoc->Fun_GetArryCanDivision(m_uiArryCanCode);		//ljb 2011222 이재복 //////////
	m_pDoc->Fun_GetArryCanStringName(m_strArryCanName);	//ljb 2011222 이재복 //////////

	// TODO: Add extra initialization here
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	
	if(pMD == NULL)	return FALSE;
	
	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return FALSE;
	
	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
		//if(pMD->GetState() == PS_STATE_LINE_OFF)
	{
//		m_ctrlLabelMsg.SetText("동작중인 채널은 수정할 수 없습니다.");
//		EnableAllControl(FALSE);
		//return;
	}

	// 각 모듈에는 2개의 채널이 붙어 있다고 가정한다.
	m_nDevID = (nSelModule - 1) * 2 + nSelCh ;

	// . 컨트롤 초기화

	// Connection
	m_ctlDeviceComm.SetCurSel(0) ;
	m_strAddr = "127.0.0.1" ;
	m_nPort   = 502 ;
	m_nResponseTimeout = 1000 ;

	// Poll Definition
	m_nSlaveID = 1 ;
	m_ctlMBFunc.SetCurSel(3) ;
	m_nMBAddr = 1 ;
	m_nMBLen  = 26 ;
	m_nMBScanRate = 1000 ;

	OnSelchangeDeviceCommunication() ;

	// RTU 설정
	m_ctrlMBBaudrate.SetCurSel(11) ;
	m_ctrlDatabits.SetCurSel(1) ;
	m_ctrlParitybit.SetCurSel(0) ;
	m_ctrlStopbit.SetCurSel(0) ;

	// Can Initialize --------------------------------
	// we need the listbox pointer within our CAN class
/*
	m_CAN.m_pOutput = &m_ctlTxList;
	m_CAN.m_pHardware = &m_ctlCanHardware ;
	
	// init the CAN hardware
	if (m_CAN.CANInit())
	{
		m_ctlCanHardware.ResetContent();
		m_ctlCanHardware.AddString("<ERROR> no HW!");
		m_ctlTxList.AddString("You need two CANcabs/CANpiggy's");
		m_btnOnBus.EnableWindow(FALSE);
	}
*/

	// Can Initialize --------------------------------


	// init the default window
	m_eID  = "600";
	
	m_CAN_Baudrate.SetCurSel(2);
	m_Channel.SetCurSel(m_nDevID);
	
// 	m_eFilterFrom = "5";
// 	m_eFilterTo   = "10";
	
	// CAN이 OnBus 여야지 전송할 수 있다.
	m_bSendCANData = FALSE ;


	// Rx/Tx 로그 사용
	m_bSaveRx = m_pDoc->m_bSaveRxFlag ;
	m_bSaveTx = m_pDoc->m_bSaveTxFlag ;

	// . 마지막 저장값 불러오기
	UpdateData(FALSE) ;
	
	// Added by 224 (2014/04/23) : 모드버스 다이얼로그 핸들링을 위한 변수할당
	m_pDoc->m_pModbusDlg = this ;
	m_bCanRecvRxTxMsg = TRUE ;

	MCC_MODE nMode = m_pDoc->GetModbusMode(m_nDevID) ;
	switch (nMode)
	{
	case MCC_PLAY:
		// 적용 버튼은 비 활성화 시킨다.
		GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;

		GetDlgItem(IDC_OPER_START)->EnableWindow(FALSE) ;
		m_Channel.SetCurSel(m_pDoc->GetModbusCanChannel(m_nDevID));

		// 채널 변경 비 활성화
		m_Channel.EnableWindow(FALSE) ;

		break ;
	default:
		GetDlgItem(IDC_OPER_START)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_OPER_PAUSE)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_OPER_STOP)->EnableWindow(TRUE) ;
		break ;
	}

	// modbus grid 초기화
	m_wndModGrid.SubclassDlgItem(IDC_MODBUS_GRID, this);
	m_wndModGrid.Initialize();

	CRect rect;
	m_wndModGrid.GetParam()->EnableUndo(FALSE);
	
	// 기본 행 설정
	m_wndModGrid.SetRowCount(1);

	// grid 컬럼 사이즈 설정
	m_wndModGrid.SetColCount(COL_NUM);
	m_wndModGrid.GetClientRect(&rect);
	m_wndModGrid.SetColWidth(COL_SEQ,       COL_SEQ,       30);
	m_wndModGrid.SetColWidth(COL_ADDR,      COL_ADDR,      60);
	m_wndModGrid.SetColWidth(COL_INDEX,     COL_INDEX,     60);
	m_wndModGrid.SetColWidth(COL_ITEMS,     COL_ITEMS,     120);
	m_wndModGrid.SetColWidth(COL_STARTBIT,  COL_STARTBIT,  60);
	m_wndModGrid.SetColWidth(COL_BITCOUNTS, COL_BITCOUNTS, 60);
	m_wndModGrid.SetColWidth(COL_DATATYPE,  COL_DATATYPE,  60);
	m_wndModGrid.SetColWidth(COL_FACTOR,    COL_FACTOR,    60);
	m_wndModGrid.SetColWidth(COL_OFFSET,    COL_OFFSET,    60);
	m_wndModGrid.SetColWidth(COL_FAULT_UPPER, COL_FAULT_UPPER, 60); 
	m_wndModGrid.SetColWidth(COL_FAULT_LOWER, COL_FAULT_LOWER, 60);
	m_wndModGrid.SetColWidth(COL_END_UPPER, COL_END_UPPER, 60);
	m_wndModGrid.SetColWidth(COL_END_LOWER, COL_END_LOWER, 60);
	m_wndModGrid.SetColWidth(COL_FUNCTION_DIVISION, COL_FUNCTION_DIVISION, 60);
	m_wndModGrid.SetColWidth(COL_FUNCTION_DIVISION2, COL_FUNCTION_DIVISION2, 60);
	m_wndModGrid.SetColWidth(COL_FUNCTION_DIVISION3, COL_FUNCTION_DIVISION3, 60);
	m_wndModGrid.SetColWidth(COL_DESC, COL_DESC, 500);

	// column header 설정
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_ADDR),      CGXStyle().SetValue("Address"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_INDEX),     CGXStyle().SetValue("Index"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_ITEMS),	   CGXStyle().SetValue("Items"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_STARTBIT),  CGXStyle().SetValue("StartBit"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_BITCOUNTS), CGXStyle().SetValue("BitCount"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_DATATYPE),  CGXStyle().SetValue("DataType"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_FACTOR),    CGXStyle().SetValue("Factor"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_OFFSET),    CGXStyle().SetValue("Offset"));
	//m_wndModGrid.SetStyleRange(CGXRange(0, COL_FAULT_UPPER), CGXStyle().SetValue("안전상한"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_FAULT_UPPER), CGXStyle().SetValue(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg2","IDD_MODBUS_CONV"))); //&&
	//m_wndModGrid.SetStyleRange(CGXRange(0, COL_FAULT_LOWER), CGXStyle().SetValue("안전하한"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_FAULT_LOWER), CGXStyle().SetValue(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg3","IDD_MODBUS_CONV"))); //&&
	//m_wndModGrid.SetStyleRange(CGXRange(0, COL_END_UPPER), CGXStyle().SetValue("종료상한"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_END_UPPER), CGXStyle().SetValue(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg4","IDD_MODBUS_CONV"))); //&&
	//m_wndModGrid.SetStyleRange(CGXRange(0, COL_END_LOWER), CGXStyle().SetValue("종료하한"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_END_LOWER), CGXStyle().SetValue(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg5","IDD_MODBUS_CONV"))); //&&
	//m_wndModGrid.SetStyleRange(CGXRange(0, COL_FUNCTION_DIVISION),    CGXStyle().SetValue("분류1"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_FUNCTION_DIVISION),    CGXStyle().SetValue(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg6","IDD_MODBUS_CONV"))); //&&
	//m_wndModGrid.SetStyleRange(CGXRange(0, COL_FUNCTION_DIVISION2),    CGXStyle().SetValue("분류2"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_FUNCTION_DIVISION2),    CGXStyle().SetValue(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg7","IDD_MODBUS_CONV"))); //&&
	//m_wndModGrid.SetStyleRange(CGXRange(0, COL_FUNCTION_DIVISION3),    CGXStyle().SetValue("분류3"));
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_FUNCTION_DIVISION3),    CGXStyle().SetValue(Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg8","IDD_MODBUS_CONV"))); //&&
	m_wndModGrid.SetStyleRange(CGXRange(0, COL_DESC),      CGXStyle().SetValue("Description").SetHorizontalAlignment(DT_LEFT));

	// Resize Col Width
	m_wndModGrid.GetParam()->EnableTrackColWidth(GX_TRACK_INDIVIDUAL | GX_TRACK_EXTHITTEST);
	m_wndModGrid.GetParam()->EnableUndo(TRUE);

	// 컬럼별 속성 설정
	char str[12], str2[7], str3[5];

	// ITEMS
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_ITEMS),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_EDIT)
	);

	// Index
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_INDEX),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
	);

	// Start bit
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 2);		//정수부	
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_STARTBIT),
		CGXStyle()
			.SetHorizontalAlignment(DT_CENTER)
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetUserAttribute(GX_IDS_UA_VALID_MIN, _T("0"))
			.SetUserAttribute(GX_IDS_UA_VALID_MAX, _T("15"))
			//.SetUserAttribute(GX_IDS_UA_VALID_MSG, _T("입력 범위에서 벗어났습니다.(0~15)"))
			.SetUserAttribute(GX_IDS_UA_VALID_MSG, Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg10","IDD_MODBUS_CONV")) //&&
			.SetValue(0L)
	);

	// Bit count
	sprintf(str, "%%d", 1);		
	sprintf(str2, "%d", 8);		//정수부
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_BITCOUNTS),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
			.SetValue(16L)
	);

	// Datatype
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_DATATYPE),
		CGXStyle()
			.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
			//.SetChoiceList(_T("Unsigned\r\nSigned\r\nFloat\r\nString\r\n"))
			.SetChoiceList(_T("Unsigned\r\nSigned\r\n"))
			.SetValue(_T("Unsigned"))
	);

	// Factor
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_FACTOR),
		CGXStyle()
			.SetControl(IDS_CTRL_REALEDIT)
//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
			.SetValue(1.0f)
	);

	// offet (
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_OFFSET),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 안전상한
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_FAULT_UPPER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);


	// 안전하한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_FAULT_LOWER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 종료상한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_END_UPPER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	// 종료하한,
	sprintf(str3, "%d", 5);		//소숫점 이하
	sprintf(str, "%%.%dlf", 5);		
	sprintf(str2, "%d", 5);		//정수부
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_END_LOWER),
		CGXStyle()
		.SetControl(IDS_CTRL_REALEDIT)
		//			.SetUserAttribute(IDS_UA_REALEDITFORMAT, str)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE, str2)
		// 			.SetUserAttribute(IDS_UA_REALEDITSIZE2, str3)
		.SetValue(0.0f)
	);

	//ljb 2011222 이재복 S //////////
	CString strComboItem,strTemp;
	strComboItem = "None [0]\r\n";
	for (int i=0; i < m_strArryCanName.GetSize() ; i++)
	{
		strTemp.Format("%s [%d]\r\n",m_strArryCanName.GetAt(i),m_uiArryCanCode.GetAt(i));
		strComboItem = strComboItem + strTemp;
	}

	/*m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_FUNCTION_DIVISION, COL_FUNCTION_DIVISION3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)	//선택만 가능
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);*/

	//ksj 20200330 : 분류 값 선택시 값이 선택되지 않고 사라지는 현상 수정.
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_FUNCTION_DIVISION,COL_FUNCTION_DIVISION3),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)	//선택만 가능
		.SetChoiceList(strComboItem)
		.SetValue(_T("None[0]"))
		);


	// Desc
	m_wndModGrid.SetStyleRange(CGXRange().SetCols(COL_DESC),
		CGXStyle()
			.SetHorizontalAlignment(DT_LEFT)
	);


	// 작동중이거나 일시 정지인 경우 설정된 값으로 컨트롤들을 세트한다.
//	MCC_MODE nMode = m_pDoc->GetModbusMode(m_nDevID) ;
	if (MCC_PLAY == nMode || MCC_PAUSE == nMode)
	{
		m_strDBName = m_pDoc->m_ModbusCanConv[m_nDevID].m_strModbusSettingName ;

		ASSERT(!m_strDBName.IsEmpty()) ;
		SetControlsWithDBName(m_strDBName) ;
	}

	// 기타설정 문구 초기화
	//SetDlgItemText(IDC_ETC_SETTING, "기타설정 <<") ;
	SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("ModbusConvDlg_OnInitDialog_msg11","IDD_MODBUS_CONV")) ; //&&
	OnEtcSetting() ;


	m_pDoc->SetModbusShowMessageFlag(m_nDevID, &m_ctlRxList, &m_ctlTxList) ;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/*
void CModbusConvDlg::OnCancel() 
{
	return ;

	// TODO: Add extra cleanup here
	// 1. modbus close
	if (ctx)
	{
		modbus_close(ctx);
		modbus_free(ctx);
	}
	
	// 2. 타이머를 해제 합니다.
	if (timeKillEvent (m_timerID) != TIMERR_NOERROR)
	{
        // 에러발생
//        return -1;
	}

	// 3. 
	if (timeEndPeriod (m_wTimerRes) != TIMERR_NOERROR)
	{
		// 에러발생
//		return -1;
	}


	// 4. CAN close
	// m_CAN 소멸자에서 실행

	// 5. 더이상 RxTx 받지 않게 한다.
	m_bCanRecvRxTxMsg = FALSE ;

	CDialog::OnCancel();
}

void CModbusConvDlg::OnOK() 
{
	// 아래코드들은 doc 에서 작동하게 함
	return ;

//	CDialog::OnOK();
}
*/
void CModbusConvDlg::SendCANData(XLevent& xlEvent)
{
	ASSERT(m_bSendCANData) ;

	CString strCont ;
	strCont.Format("ID:%02d ", xlEvent.tagData.msg.id) ;
	for (int nIdx = 0; nIdx < xlEvent.tagData.msg.dlc; nIdx++)
	{
		CString str ;
		str.Format("[0x%02X] ", xlEvent.tagData.msg.data[nIdx]) ;
		
		strCont += str ;
	}
	
	int cnt = m_ctlTxList.GetCount() ;
	while (cnt > 99)
	{
		m_ctlTxList.DeleteString(0) ;
		cnt = m_ctlTxList.GetCount() ;
	}
	
	m_ctlTxList.InsertString(-1, strCont) ;
	m_ctlTxList.SetCurSel(m_ctlTxList.GetCount()-1);  

	if (m_bCheckExtMode)
	{
		xlEvent.tagData.msg.id |= XL_CAN_EXT_MSG_ID;
	}
	
/*
	m_CAN.CANSend(xlEvent, m_Channel.GetCurSel() );
*/

}

void CModbusConvDlg::OnResetRxContents() 
{
	m_ctlRxList.ResetContent() ;	
}

void CModbusConvDlg::OnResetTxContents() 
{
	m_ctlTxList.ResetContent() ;
}

// modbus-can convert thread 시작
void CModbusConvDlg::OnOperStart() 
{
	UpdateData() ;

	CWaitCursor wc ;

	m_bCanRecvRxTxMsg = TRUE ;

	if (m_strDBName.IsEmpty())
	{
		//MessageBox("DB 이름을 설정하고 운영 시작을 하여야 합니다.") ;
		MessageBox(Fun_FindMsg("ModbusConvDlg_OnOperStart_msg1","IDD_MODBUS_CONV")) ; //&&
		return ;
	}

	if (m_pDoc->BeginMCCThread(nSelCh, nSelModule, m_strDBName))
	{
		GetDlgItem(IDC_OPER_START)->EnableWindow(FALSE) ;

		
		
		// Added by 224 (2014/12/05) :
		// 비정상 종료 후 재시작시 최종공정 자동 실행하게 구성
		// 	MODxCHx [ MOD0CH1, MOD0CH2, MOD1CH1, MOD1CH2]
		// 		name:
		// 		type: [0:CAN, 1:Modbus, 2:LIN, ...]
		// 		typename: [CAN|Modbus|LIN]
		// 		StartTime:
		// 		EndTime:
		// 		EndFlag: [0:No, 1:Yes]
		
		CString strSectionName ;
		strSectionName.Format("MOD%dCH%d", m_nDevID / 2, m_nDevID % 2 + 1) ;
		AfxGetApp()->WriteProfileString(strSectionName, "Name", m_strDBName) ;
		AfxGetApp()->WriteProfileInt(strSectionName,    "DEVID", m_nDevID) ;
		AfxGetApp()->WriteProfileInt(strSectionName,    "Type", 1) ;
		AfxGetApp()->WriteProfileString(strSectionName, "TypeName", "MODBUS") ;
		AfxGetApp()->WriteProfileString(strSectionName, "StratTime", CTime::GetCurrentTime().Format(_T("%Y-%m-%d %H:%M:%S"))) ;
		AfxGetApp()->WriteProfileString(strSectionName, "EndTime", "") ;
		AfxGetApp()->WriteProfileInt(strSectionName,    "EndFlag", 0) ;

		// 적용 버튼은 비 활성화 시킨다.
		GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;
		
		// 채널 변경 비 활성화
		m_Channel.EnableWindow(FALSE) ;
	}
}

// modbus-can convert thread 일시정지
void CModbusConvDlg::OnOperPause() 
{
	// TODO: Add your control notification handler code here
	m_bCanRecvRxTxMsg = FALSE ;
	m_pDoc->PauseMCCThread(m_nDevID) ;
	
	GetDlgItem(IDC_OPER_START)->EnableWindow(TRUE) ;
}

// modbus-can convert thread 종료
void CModbusConvDlg::OnOperStop() 
{
	CWaitCursor wc ;

	// TODO: Add your control notification handler code here
	m_bCanRecvRxTxMsg = FALSE ;
	m_pDoc->StopMCCThread(m_nDevID) ;
	
	GetDlgItem(IDC_OPER_START)->EnableWindow(TRUE) ;

	// 채널 변경 활성화
	m_Channel.EnableWindow(FALSE) ;


	// Added by 224 (2014/12/05) :
	// 비정상 종료 후 재시작시 최종공정 자동 실행하게 구성

	CString strSectionName ;
	strSectionName.Format("MOD%dCH%d", m_nDevID / 2, m_nDevID % 2 + 1) ;
	AfxGetApp()->WriteProfileString(strSectionName, "EndTime", CTime::GetCurrentTime().Format(_T("%Y-%m-%d %H:%M:%S"))) ;
	AfxGetApp()->WriteProfileInt(strSectionName, "EndFlag", 1) ;

}

void CModbusConvDlg::OnAddRow() 
{
	CRowColArray ra;
	m_wndModGrid.GetSelectedRows(ra);
	if(ra.GetSize() > 0)
		m_wndModGrid.InsertRows(ra[0] + 1, 1);
	else
		m_wndModGrid.InsertRows(m_wndModGrid.GetRowCount()+1, 1);
	
	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;	
}

void CModbusConvDlg::OnDelRow() 
{
	CRowColArray ra;
	m_wndModGrid.GetSelectedRows(ra);
	
	for (int i = ra.GetSize()-1; i >=0 ; i--)
	{
		if(ra[i] == 0) //Header 삭제불가
			continue;
		m_wndModGrid.RemoveRows(ra[i], ra[i]);
	}

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;
}

void CModbusConvDlg::SetControlsWithDBName(CString strName)
{
	CString strQuery ;

	// MODBUS Function 별 베이스 Address
	INT nBaseModbusAddr = 0 ;


	// 현재 grid 의 모든 항목 삭제
	if (m_wndModGrid.GetRowCount() > 0)
	{
		m_wndModGrid.RemoveRows(1, m_wndModGrid.GetRowCount()) ;
	}


	// modbus, can 설정값을 채운다.
    try
    {
		CppSQLite3DB db;
		db.open(gszConvDBFile) ;

		// 현재 입력한 이름의 SEQ의 값을 얻는다.
		strQuery.Format("SELECT * FROM TB_CONV_LIST WHERE NAME = '%s'", strName) ;
		CppSQLite3Query q = db.execQuery(strQuery) ;
		ASSERT(!q.eof()) ;
	
		if (!q.eof())
		{
			CString str = q.fieldValue("CONNECTION") ;
			if (str.Left(3) == "TCP")
			{
				m_ctlDeviceComm.SetCurSel(0) ;
			}
			else
			{
				INT nIdx = atoi(str.Right(1)) ;
				m_ctlDeviceComm.SetCurSel(nIdx) ;

			}
			OnSelchangeDeviceCommunication() ;

			m_strAddr = q.fieldValue("IPADDR") ;
			m_nPort   = atoi(q.fieldValue("PORT")) ;

			// baudrate
			INT nBaudrate = atoi(q.fieldValue("BAUDRATE")) ;
			for (int nSelect = 0; nSelect < sizeof(bitrate) / sizeof(bitrate[0]); nSelect++)
			{
				if (nBaudrate == bitrate[nSelect])
				{
					m_ctrlMBBaudrate.SetCurSel(nSelect) ;
					break ;
				}
			}

			// databit 
			INT nDatabit = atoi(q.fieldValue("DATABIT")) ;
			for(int nSelect = 0; nSelect < sizeof(databit) / sizeof(databit[0]); nSelect++)
			{
				if (nDatabit == databit[nSelect])
				{
					m_ctrlDatabits.SetCurSel(nSelect) ;
					break ;
				}
			}
			
			// parity
			str = q.fieldValue("PARITY") ;
			for(int nSelect = 0; nSelect < sizeof(parity) / sizeof(parity[0]); nSelect++)
			{
				if (str[0] == parity[nSelect])
				{
					m_ctrlParitybit.SetCurSel(nSelect) ;
					break ;
				}
			}

			// stopbit
			INT nStopbit  = atoi(q.fieldValue("STOPBIT")) ;
			for(int nSelect = 0; nSelect < sizeof(stopbit) / sizeof(parity[0]); nSelect++)
			{
				if (nStopbit == parity[nSelect])
				{
					m_ctrlStopbit.SetCurSel(nSelect) ;
					break ;
				}
			}

			m_nResponseTimeout = atoi(q.fieldValue("RESPONSETIMEOUT")) ;
			m_nSlaveID = atoi(q.fieldValue("SLAVEID")) ;

			INT nFunctionCD = atoi(q.fieldValue("FUNCTIONCD")) ;
			switch (nFunctionCD)
			{
			case 0: nBaseModbusAddr = 0 ;     break ;
			case 1: nBaseModbusAddr = 10000 ; break ;
			case 2: nBaseModbusAddr = 40000 ; break ;
			case 3: nBaseModbusAddr = 30000 ; break ;
			default: ASSERT(FALSE) ;          break ;
			}
			m_ctlMBFunc.SetCurSel(nFunctionCD) ;

			m_nMBAddr       = atoi(q.fieldValue("MAPFROM")) ;
			m_nMBLen        = atoi(q.fieldValue("MAPLENGTH")) ;
			m_nMBScanRate   = atoi(q.fieldValue("SCANRATE")) ;

			CString strCANBaud = q.fieldValue("CANBAUDRATE") ;
			INT n = m_CAN_Baudrate.FindStringExact(0, strCANBaud) ;
			m_CAN_Baudrate.SetCurSel(n) ;

			m_bCheckExtMode = atoi(q.fieldValue("CANEXTENDED")) ;
//			q.fieldValue("DESC") ;

		}
		
		UpdateData(FALSE) ;
	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		MessageBox(e.errorMessage()) ;
		return ;
    }


	// modbus map 값을 설정한다.
    try
    {
		CppSQLite3DB db;
		db.open(gszConvDBFile) ;
				
		if (!db.tableExists("TB_CONV_LIST") || !db.tableExists("TB_MODBUS_MAP"))
		{
			//MessageBox("잘못된 db 파일입니다") ;
			MessageBox(Fun_FindMsg("ModbusConvDlg_SetControlsWithDBName_msg1","IDD_MODBUS_CONV")) ;//&&
			db.close() ;
			return ;
		}

		// 현재 입력한 이름의 SEQ의 값을 얻는다.
		strQuery.Format("SELECT SEQ FROM TB_CONV_LIST WHERE NAME = '%s'", strName) ;
		CppSQLite3Query q = db.execQuery(strQuery) ;
		ASSERT(!q.eof()) ;

		CString strSEQ = q.fieldValue("SEQ") ;
		q.finalize();

		strQuery.Format("SELECT IDX, ITEMS, STARTBIT, BITCOUNT, DATATYPE, FACTOR, OFFSET, FAULT_UPPER, FAULT_LOWER, END_UPPER, END_LOWER, FUNC_DIVISION, FUNC_DIVISION2, FUNC_DIVISION3, DESC "
						"  FROM TB_MODBUS_MAP "
						" WHERE PARENTCD = %s "
						" ORDER BY IDX, STARTBIT "
					  , strSEQ) ;
		
		INT nRow ;
		q = db.execQuery(strQuery) ;
		while (!q.eof())
		{
			CString strIdx ;
			strIdx.Format("%d", nBaseModbusAddr + atoi(q.fieldValue("IDX"))) ;

			nRow = m_wndModGrid.GetRowCount() + 1;
			m_wndModGrid.InsertRows(nRow, 1);
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_ADDR),      strIdx);
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_INDEX),     q.fieldValue("IDX"));
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_ITEMS),     q.fieldValue("ITEMS"));
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_STARTBIT),  q.fieldValue("STARTBIT")) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_BITCOUNTS), q.fieldValue("BITCOUNT")) ;

			INT nType = atoi(q.fieldValue("DATATYPE")) ;
			CString strType = (nType == 0) ? "Unsigned" : "Signed" ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_DATATYPE),  strType) ;

			CString strFloat ;
			strFloat.Format("%.5f", atof(q.fieldValue("FACTOR"))) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_FACTOR),    strFloat) ;

			
			strFloat.Format("%.5f", (q.fieldValue("OFFSET") == NULL) ? 0 : atof(q.fieldValue("OFFSET"))) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_OFFSET),    strFloat) ;

			strFloat.Format("%.5f", (q.fieldValue("FAULT_UPPER") == NULL) ? 0 : atof(q.fieldValue("FAULT_UPPER"))) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_FAULT_UPPER),    strFloat) ;

			strFloat.Format("%.5f", (q.fieldValue("FAULT_LOWER") == NULL) ? 0 : atof(q.fieldValue("FAULT_LOWER"))) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_FAULT_LOWER),    strFloat) ;
			
			strFloat.Format("%.5f", (q.fieldValue("END_UPPER") == NULL) ? 0 : atof(q.fieldValue("END_UPPER"))) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_END_UPPER),    strFloat) ;
			
			strFloat.Format("%.5f", (q.fieldValue("END_LOWER") == NULL) ? 0 : atof(q.fieldValue("END_LOWER"))) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_END_LOWER),    strFloat) ;
			
			INT nValue = 0 ;
			nValue = (q.fieldIsNull("FUNC_DIVISION")) ? 0 : atoi(q.fieldValue("FUNC_DIVISION")) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_FUNCTION_DIVISION),  Fun_FindCanName(nValue)) ;

			nValue = (q.fieldIsNull("FUNC_DIVISION2")) ? 0 : atoi(q.fieldValue("FUNC_DIVISION2")) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_FUNCTION_DIVISION2),  Fun_FindCanName(nValue)) ;

			nValue = (q.fieldIsNull("FUNC_DIVISION3")) ? 0 : atoi(q.fieldValue("FUNC_DIVISION3")) ;
			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_FUNCTION_DIVISION3),  Fun_FindCanName(nValue)) ;

			m_wndModGrid.SetValueRange(CGXRange(nRow, COL_DESC),      q.fieldValue("DESC")) ;

			q.nextRow() ;
		}

		q.finalize();

	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		MessageBox(e.errorMessage()) ;
    }

	// 헤더값을 불러온다.

	// modbus 행값을 채운다.

	
	return ;
} 


void CModbusConvDlg::OnOpenConvData() 
{
#if 1
	// 대화상자에서 레코드를 선택한다.
	CModbusSettingDBDialog dlg ; 
	dlg.m_bSaveMode = FALSE ;
	if (IDOK != dlg.DoModal())
	{
		return ;
	}

	m_strDBName = dlg.m_strName ;
	SetControlsWithDBName(m_strDBName) ;

#endif

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;
}

void CModbusConvDlg::GetModbusCommInfo(CString strName, MODBUS_INFO* pInfo, BOOL bShowMessage)
{
	CString str ;
	CString strQuery ;
    try
    {
		CppSQLite3DB db;
		db.open(gszConvDBFile) ;

		// 현재 입력한 이름의 SEQ의 값을 얻는다.
		strQuery.Format("SELECT * FROM TB_CONV_LIST WHERE NAME = '%s'", strName) ;
		CppSQLite3Query q = db.execQuery(strQuery) ;
		ASSERT(!q.eof()) ;
	
		if (!q.eof())
		{
			strcpy(pInfo->comm, q.fieldValue("CONNECTION")) ;

			strcpy(pInfo->IPAddr, q.fieldIsNull("IPADDR") ? "" : q.fieldValue("IPADDR")) ;
			pInfo->Port = atoi(q.fieldValue("PORT")) ;

			// baudrate
			pInfo->Baudrate = atoi(q.fieldValue("BAUDRATE")) ;

			// databit 
			pInfo->Databit = atoi(q.fieldValue("DATABIT")) ;
			
			// parity
			str = q.fieldValue("PARITY") ;
			pInfo->Parity = str[0] ;

			// stopbit
			pInfo->Stopbit  = atoi(q.fieldValue("STOPBIT")) ;

			pInfo->responsetimeout = atoi(q.fieldValue("RESPONSETIMEOUT")) ;
			pInfo->slaveid = atoi(q.fieldValue("SLAVEID")) ;

			pInfo->functioncd = atoi(q.fieldValue("FUNCTIONCD")) ;

			pInfo->MBAddr   = atoi(q.fieldValue("MAPFROM")) ;
			pInfo->MBLen    = atoi(q.fieldValue("MAPLENGTH")) ;
			pInfo->scanrate = atoi(q.fieldValue("SCANRATE")) ;

			// Quick & Dirty. 
			CString strCANBaud = q.fieldValue("CANBAUDRATE") ;
			if (strCANBaud == CString("125 kbps"))
			{
				pInfo->can_baudrate = 125000 ;
			}
			else if (strCANBaud == CString("250 kbps"))
			{
				pInfo->can_baudrate = 250000 ;
			}
			else if (strCANBaud == CString("500 kbps"))
			{
				pInfo->can_baudrate = 500000 ;
			}
			else if (strCANBaud == CString("1 Mbps"))
			{
				pInfo->can_baudrate = 1000000 ;
			}
			else
			{
				ASSERT(FALSE) ;
				pInfo->can_baudrate = 500000 ;
			}


			pInfo->extmode = atoi(q.fieldValue("CANEXTENDED")) ;
//			q.fieldValue("DESC") ;

		}
		
	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		if (bShowMessage)
		{
			AfxMessageBox(e.errorMessage()) ;
		}

		return ;
    }

}

void CModbusConvDlg::GetModbusConvInfo(CString strName, CArray<PMCC_INFO, PMCC_INFO>& array, BOOL bShowMessage)
{
	CString strQuery ;
	int canid = 600 ;
	int nAccCounts = 0 ;	// 누적값이 64를 넘으면 안된다.

	// modbus map 값을 설정한다.
    try
    {
		CppSQLite3DB db;
		db.open(gszConvDBFile) ;
		
		if (!db.tableExists("TB_CONV_LIST") || !db.tableExists("TB_MODBUS_MAP"))
		{
			if (bShowMessage)
				{
				AfxMessageBox("잘못된 db 파일입니다") ; //yulee 20190405 check
			}
			db.close() ;
			return ;
		}
		
		// 현재 입력한 이름의 SEQ의 값을 얻는다.
		strQuery.Format("SELECT SEQ FROM TB_CONV_LIST WHERE NAME = '%s'", strName) ;
		CppSQLite3Query q = db.execQuery(strQuery) ;
		ASSERT(!q.eof()) ;
		
		CString strSEQ = q.fieldValue("SEQ") ;

		// MODBUS Function 별 베이스 Address
// 		INT nBaseModbusAddr = 0 ;
// 		INT nFunctionCD = atoi(q.fieldValue("FUNCTIONCD")) ;
// 		switch (nFunctionCD)
// 		{
// 		case 0: nBaseModbusAddr = 0 ;     break ;
// 		case 1: nBaseModbusAddr = 10000 ; break ;
// 		case 2: nBaseModbusAddr = 40000 ; break ;
// 		case 3: nBaseModbusAddr = 30000 ; break ;
// 		default: ASSERT(FALSE) ;          break ;
// 		}
		q.finalize();
		
		strQuery.Format("SELECT IDX, ITEMS, STARTBIT, BITCOUNT, DATATYPE, FACTOR, OFFSET, FAULT_UPPER, FAULT_LOWER, END_UPPER, END_LOWER, FUNC_DIVISION, FUNC_DIVISION2, FUNC_DIVISION3, DESC "
						"  FROM TB_MODBUS_MAP "
						" WHERE PARENTCD = %s "
						" ORDER BY IDX, STARTBIT "
						, strSEQ) ;
		
		INT nRow ;
		q = db.execQuery(strQuery) ;
		while (!q.eof())
		{
			MCC_INFO* info = new MCC_INFO ;
			ZeroMemory(info, sizeof(MCC_INFO)) ;
			
			info->addr = atoi(q.fieldValue("IDX")) ;
			ASSERT(info->addr > 0) ;

			info->mb_startbit  = atoi(q.fieldValue("STARTBIT")) ;
			info->mb_bitcounts = atoi(q.fieldValue("BITCOUNT")) ;
			strcpy(info->name, q.fieldValue("ITEMS")) ;
			info->datatype     = atoi(q.fieldValue("DATATYPE")) ;
			strcpy(info->desc, q.fieldValue("DESC")) ;
			
			ASSERT(info->mb_bitcounts <= 16) ;
			
			INT bitcounts = max(info->mb_bitcounts, 8) ;
			bitcounts = min(bitcounts, 16) ;
			if (nAccCounts + bitcounts > 64)
			{
				canid++ ;
				nAccCounts = 0 ;
			}
			
			info->canid				= canid ;
			info->can_startbit		= nAccCounts ;
			info->can_bitcounts		= bitcounts ;
			info->factor			= atof(q.fieldIsNull("FACTOR") ? 0 : q.fieldValue("FACTOR")) ;
			info->offset			= (q.fieldIsNull("OFFSET")) ? 0 : atof(q.fieldValue("OFFSET")) ;
			info->fault_upper	    = (q.fieldIsNull("FAULT_UPPER")) ? 0 : atof(q.fieldValue("FAULT_UPPER")) ;
			info->fault_lower       = (q.fieldIsNull("FAULT_LOWER")) ? 0 : atof(q.fieldValue("FAULT_LOWER")) ;
			info->end_upper	        = (q.fieldIsNull("END_UPPER")) ? 0 : atof(q.fieldValue("END_UPPER")) ;
			info->end_lower	        = (q.fieldIsNull("END_LOWER")) ? 0 : atof(q.fieldValue("END_LOWER")) ;
			
			CString strTemp, strCode ;
			INT nLength ;
			
			info->function_division  = (q.fieldIsNull("FUNC_DIVISION")) ?  0 : atoi(q.fieldValue("FUNC_DIVISION")) ;
			info->function_division2 = (q.fieldIsNull("FUNC_DIVISION2")) ? 0 : atoi(q.fieldValue("FUNC_DIVISION2")) ;
			info->function_division3 = (q.fieldIsNull("FUNC_DIVISION3")) ? 0 : atoi(q.fieldValue("FUNC_DIVISION3")) ;
			
			nAccCounts += bitcounts ;
			
			array.Add(info) ;
			
			q.nextRow() ;
		}
		
		q.finalize();
		
	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		if (bShowMessage)
		{
			AfxMessageBox(e.errorMessage()) ;
		}
    }
	
}

void CModbusConvDlg::GetModbusConvInfo(CArray<PMCC_INFO, PMCC_INFO>& array)
{
	// 현재 테이블 값 검증. 순차적인가? INDEX, STARTBIT, BITCOUNTS
	
	// CAN Receive 설정 함수 호출
	//	
	// 누적 bit Counts 가 64(8bytes) 가 넘으면 새로운 CANID를 생성한다.
	
	
	int canid = 600 ;
	int nAccCounts = 0 ;	// 누적값이 64를 넘으면 안된다.
	
	INT nCurrAddr = -1 ;
	for (int nRow = 1; nRow < m_wndModGrid.GetRowCount()+1; nRow++)
	{		
		nCurrAddr    = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_INDEX)) ;
//		TRACE("current addr = %0d\n", nCurrAddr) ;
		
		ASSERT(nCurrAddr > 0) ;
		
		MCC_INFO* info = new MCC_INFO ;
		ZeroMemory(info, sizeof(MCC_INFO)) ;
		
		info->addr = nCurrAddr ;
		info->mb_startbit  = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_STARTBIT)) ;
		info->mb_bitcounts = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_BITCOUNTS)) ;
		strcpy(info->name, m_wndModGrid.GetValueRowCol(nRow, COL_ITEMS)) ;
		info->datatype     = (CString("Unsigned") == m_wndModGrid.GetValueRowCol(nRow, COL_DATATYPE)) ? 0 : 1 ;
		strcpy(info->desc, m_wndModGrid.GetValueRowCol(nRow, COL_DESC)) ;
		
		ASSERT(info->mb_bitcounts <= 16) ;
		
		INT bitcounts = max(info->mb_bitcounts, 8) ;
		bitcounts = min(bitcounts, 16) ;
		if (nAccCounts + bitcounts > 64)
		{
			canid++ ;
			nAccCounts = 0 ;
		}
		
		info->canid				= canid ;
		info->can_startbit		= nAccCounts ;
		info->can_bitcounts		= bitcounts ;
		info->factor			= atof(m_wndModGrid.GetValueRowCol(nRow, COL_FACTOR)) ;
		info->offset			= atof(m_wndModGrid.GetValueRowCol(nRow, COL_OFFSET)) ;
		info->fault_upper	    = atof(m_wndModGrid.GetValueRowCol(nRow, COL_FAULT_UPPER)) ;
		info->fault_lower       = atof(m_wndModGrid.GetValueRowCol(nRow, COL_FAULT_LOWER)) ;
		info->end_upper	        = atof(m_wndModGrid.GetValueRowCol(nRow, COL_END_UPPER)) ;
		info->end_lower	        = atof(m_wndModGrid.GetValueRowCol(nRow, COL_END_LOWER)) ;

		CString strTemp, strCode ;
		INT nLength ;

		strTemp = m_wndModGrid.GetValueRowCol(nRow, COL_FUNCTION_DIVISION);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		info->function_division  = atoi(strCode);

		strTemp = m_wndModGrid.GetValueRowCol(nRow, COL_FUNCTION_DIVISION2);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		info->function_division2  = atoi(strCode);

		strTemp = m_wndModGrid.GetValueRowCol(nRow, COL_FUNCTION_DIVISION3);
		nLength = strTemp.ReverseFind(']') - ((strTemp.ReverseFind('[')+1));
		strCode = strTemp.Mid(strTemp.ReverseFind('[')+1,nLength);
		info->function_division3  = atoi(strCode);
		
		nAccCounts += bitcounts ;
		
		array.Add(info) ;
		
	}

#ifdef _DEBUG	
	for (int nLoop = 0; nLoop < array.GetSize(); nLoop++)
	{
		MCC_INFO* ptr = array.GetAt(nLoop) ;
		TRACE("addr:%02x | s: %02d | b: %2d | datatype: %d | canid: %03d | s: %02d | b: %2d | f: %.2f | name: %s \n"
			, ptr->addr
			, ptr->mb_startbit
			, ptr->mb_bitcounts
			, ptr->datatype
			, ptr->canid
			, ptr->can_startbit
			, ptr->can_bitcounts
			, ptr->factor
			, ptr->name) ;
	}
#endif
}

BOOL CModbusConvDlg::CheckMapValues() 
{
	// 체크항목
	// 1. 인덱스의 행이 감소하면 안된다.
	// 2. 인덱스가 0이거나 이하이면 안된다.
	// 3. 동일 인덱스의 누적 bitcount 가 16을 넘을 수 없다.
	// 4. ITEMS 의 이름이 있어야 한다..

	INT nLatestAddr = -1 ;
	INT nAccCounts = 0 ;
	for (int nRow = 1; nRow < m_wndModGrid.GetRowCount()+1; nRow++)
	{		
		INT nCurrAddr  = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_INDEX)) ;
		INT nStartBit  = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_STARTBIT)) ;
		INT nBitcounts = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_BITCOUNTS)) ;
		
		if (nLatestAddr > nCurrAddr)
		{
			// 1. 인덱스의 행이 감소하면 안된다.
			CString str ;
			//str.Format("%d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg1","IDD_MODBUS_CONV"), nRow) ; //&&
			//MessageBox("인덱스값은 감소할 수 없습니다.", str) ;
			MessageBox(Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg2","IDD_MODBUS_CONV"), str) ; //&&

			return FALSE ;
		}
		else if (nCurrAddr > nLatestAddr)
		{
			nLatestAddr = nCurrAddr ;
			nAccCounts = 0 ;
		}
		if (nLatestAddr == nCurrAddr)
		{
			// 3. 동일 인덱스의 누적 bitcount 가 16을 넘을 수 없다.
			if (nAccCounts + nBitcounts > 16)
			{
				CString str ;
				//str.Format("%d 번째행 이상", nRow) ;
				str.Format(Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg3","IDD_MODBUS_CONV"), nRow) ; //&&
				//MessageBox("동일 인덱스의 누적 bitcount 가 16을 넘을 수 없습니다.", str) ;
				MessageBox(Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg4","IDD_MODBUS_CONV"), str) ; //&&
				
				return FALSE ;
			}
		}

		if (nCurrAddr <= 0)
		{
			// 2. 인덱스가 0이거나 이하이면 안된다.
			CString str ;
			//str.Format("%d 번째행 이상", nRow) ;
			str.Format(Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg5","IDD_MODBUS_CONV"), nRow) ;//&&
			//MessageBox("인덱스값은 0이거나 이하일 수 없습니다.", str) ;
			MessageBox(Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg6","IDD_MODBUS_CONV"), str) ;//&&

			return FALSE ;
		}

		// . 누적 bitcount 보관
		nAccCounts += nBitcounts ;

		CString strItems = m_wndModGrid.GetValueRowCol(nRow, COL_ITEMS) ;
		if (strItems.IsEmpty())
		{
			// 4. ITEMS 의 이름이 있어야 한다.
			CString str ;
			//str.Format("%d 행의 ITEMS 명 이상", nRow) ;
			str.Format(Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg7","IDD_MODBUS_CONV"), nRow) ;//&&
			//MessageBox(str, "ITEMS 의 이름이 있어야 합니다.") ;
			MessageBox(str, Fun_FindMsg("ModbusConvDlg_CheckMapValues_msg8","IDD_MODBUS_CONV")) ;//&&
			
			return FALSE ;

		}

//		(CString("Unsign") == m_wndModGrid.GetValueRowCol(nRow, COL_DATATYPE)) ? 0 : 1 ;
	}

	return TRUE ;
}

void CModbusConvDlg::OnTransCanData() 
{
	// Grid 내의 Map 값 Valid 체크
	if (!CheckMapValues())
	{
		return ;
	}

	// Grid 의 설정된 값들을 가져온다.
	CArray<PMCC_INFO, PMCC_INFO> mcc_array ;
	GetModbusConvInfo(mcc_array) ;

	//-----------------------------------------------------------------------
	// CAN Receive 설정 함수 호출
	//-----------------------------------------------------------------------

	if (!SaveCANConfig(mcc_array))
	{
		TRACE("SaveCANConfig return FALSE\n") ;
		return ;
	}
//-----------------------------------------------------------------------
// Runtime 변환 단위테스트
//-----------------------------------------------------------------------


	// clearing
	for (INT nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
	{
		MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
		delete ptr ;
	}
	
	mcc_array.RemoveAll() ;

	POSITION pos = m_pDoc->GetFirstViewPosition();
	CCTSMonProView* pView = (CCTSMonProView *)m_pDoc->GetNextView(pos);

	pView->m_wndCanList.DeleteAllItems();	

	// 적용 시 버튼은 비활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(FALSE) ;


	// CAN 적용과 동시에 모드버스 종료-시작을 호출한다.
	PostMessage(WM_COMMAND, IDC_OPER_STOP) ;
	PostMessage(WM_COMMAND, IDC_OPER_START) ;
}

// SBC 에 CAN 설정값을 전달하는 함수
BOOL CModbusConvDlg::SaveCANConfig(CArray<PMCC_INFO, PMCC_INFO>& array)
{
	CString strTemp,strCode;
	int		nLength;
	int i;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);

	if (pMD == NULL)
	{
		return FALSE ;
	}

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if (pChInfo == NULL)
	{
		return FALSE ;
	}

	if(	pChInfo->GetState() != PS_STATE_IDLE && 
		pChInfo->GetState() != PS_STATE_STANDBY &&
		pChInfo->GetState() != PS_STATE_READY &&
		pChInfo->GetState() != PS_STATE_END	)
	{
		//AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("ModbusConvDlg_SaveCANConfig_msg1","IDD_MODBUS_CONV"));//&&
		return FALSE ;
	}

// 	if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
// 	{
// 		AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
// 		return;
// 	}

	// 설정 변수값을 가져온다.
	UpdateData();

	if ((array.GetSize()) > 256)
	{
		//AfxMessageBox("CAN 설정은 256개를 초과 할 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("ModbusConvDlg_SaveCANConfig_msg2","IDD_MODBUS_CONV"));//&&
		return FALSE ;
	}

	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));

	SFT_CAN_COMMON_DATA commonData;
	ZeroMemory(&commonData, sizeof(SFT_CAN_COMMON_DATA));
	
	commonData.can_baudrate = m_CAN_Baudrate.GetCurSel();
	commonData.extended_id	= m_bCheckExtMode ;
//	commonData.fCell_cv	= m_fCV;
	commonData.controller_canID = strtoul("0", (char **)NULL, 16);
	commonData.bms_type     = 0 ;
	commonData.bms_sjw      = 0 ;

	
	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_MASTER);
	memcpy(&canSetData.canCommonData, &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008
	
//	int nSelectIndex=0;	//ljb 2011222 이재복 //////////

	SFT_CAN_SET_DATA canData[_SFT_MAX_MAPPING_CAN];
	ZeroMemory(&canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);

	for( int i = 0; i < array.GetSize(); i++)
	{
		MCC_INFO* ptr = array.GetAt(i) ;

		canData[i].canID      = ptr->canid ;
		// Commented by 224 (2014/07/04) : 김재호 과장의 요청에 의하여, 모드버스 포트는 슬레이브로 설정한다.
//		canData[i].canType    = PS_CAN_TYPE_MASTER;
		canData[i].canType    = PS_CAN_TYPE_SLAVE;
		strcpy(canData[i].name, ptr->name);		
		canData[i].startBit   = ptr->can_startbit ;
		canData[i].bitCount   = ptr->can_bitcounts ;
		canData[i].byte_order = 0;		
		
		canData[i].data_type = ptr->datatype ;
		canData[i].factor_multiply = ptr->factor ;
		canData[i].factor_Offset = 0.0f ;
		
		canData[i].fault_upper    = ptr->fault_upper ;
		canData[i].fault_lower    = ptr->fault_lower ;
		canData[i].end_upper      = ptr->end_upper ;
		canData[i].end_lower      = ptr->end_lower ;
		canData[i].default_fValue = 0.0f ;
		canData[i].sentTime       = 0.0f ;

		canData[i].function_division  = ptr->function_division ;
		canData[i].function_division2 = ptr->function_division2 ;
		canData[i].function_division3 = ptr->function_division3 ;
//		canData[i].startBit2          = 0 ;
//		canData[i].bitCount2          = 0 ;
//		canData[i].byte_order2        = 0 ;		
//		canData[i].data_type2         = 0 ;
//		canData[i].default_fValue2    = 0 ;
		//****************************************************************************************

	}

	pMD->SetCANCommonData(nSelCh, commonData, PS_CAN_TYPE_SLAVE);
	memcpy(&canSetData.canCommonData[1], &commonData, sizeof(SFT_CAN_COMMON_DATA));	//ljb 201008
	
	pMD->SetCANSetData(nSelCh, canData, array.GetSize());
	memcpy(&canSetData.canSetData, &canData, sizeof(SFT_CAN_SET_DATA)*_SFT_MAX_MAPPING_CAN);	//ljb 201008

	//SFT_RCV_CMD_CAN_SET canSetData;
	//ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CAN_SET));
// 	SFT_RCV_CMD_CHANNEL_CAN_SET canSetData;	//ljb 201008 CAN 설정값 
// 	ZeroMemory(&canSetData, sizeof(SFT_RCV_CMD_CHANNEL_CAN_SET));
	
// 	SFT_MD_CAN_INFO_DATA * canInforData = pMD->GetCanAllData();
// 	memcpy(&canSetData.canCommonData, pMD->GetCANCommonData(), sizeof(canSetData.canCommonData));
// 	memcpy(&canSetData.canSetData, canInforData->canSetAllData.canSetData, sizeof(canSetData.canSetData));
	

	TRACE("CAN commdata Size = %d, size data = %d, total size = %d \n",sizeof(canSetData.canCommonData[0]),sizeof(canSetData.canSetData[0]), sizeof(canSetData));
	if(int nRth = m_pDoc->SetCANDataToModule(nSelModule, &canSetData, nSelCh) == FALSE)
	{
		//AfxMessageBox("CAN 정보 설정에 실패했습니다.");
		AfxMessageBox(Fun_FindMsg("ModbusConvDlg_SaveCANConfig_msg3","IDD_MODBUS_CONV"));//&&
		return FALSE ;
	}

//	IsChange = FALSE;
//	GetDlgItem(IDOK)->EnableWindow(IsChange);

	pMD->SaveCanConfig();

	
	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;

	return TRUE ;
	//CDialog::OnOK();
}

void CModbusConvDlg::OnSaveConfig() 
{
	UpdateData() ;

	// Grid 내의 Map 값 Valid 체크
	if (!CheckMapValues())
	{
		return ;
	}


	CModbusSettingDBDialog dlg ; 
	dlg.m_bSaveMode = TRUE ;
	dlg.m_strName = m_strDBName ;
	if (IDOK != dlg.DoModal())
	{
		return ;
	}
	
	// modbus config 가져오기
	m_strDBName = dlg.m_strName ;
	CString strDesc = dlg.m_strDesc ;
	m_strDBName.TrimLeft() ; m_strDBName.TrimRight() ;
	CString strConnection ; m_ctlDeviceComm.GetWindowText(strConnection);

	// MODBUS TCP/IP
	m_strAddr ;
	m_nPort ;

	// MODBUS RS232
	INT sel ;
	sel = m_ctrlMBBaudrate.GetCurSel() ;
	INT nBaudrate = bitrate[sel] ;

	sel = m_ctrlDatabits.GetCurSel() ;
	INT nDatabit  = databit[sel] ;
	
	sel = m_ctrlParitybit.GetCurSel() ;
	CHAR cParity   = parity[sel] ;

	sel = m_ctrlStopbit.GetCurSel() ;
	INT nStopbit  = stopbit[sel] ;

	
	m_nResponseTimeout ; 
	m_nSlaveID ;
	INT nFunctioncd = m_ctlMBFunc.GetCurSel() ;
	m_nMBAddr ;
	m_nMBLen ;
	m_nMBScanRate;
	CString strCanbaudrate ; m_CAN_Baudrate.GetWindowText(strCanbaudrate) ;
	m_bCheckExtMode ;
	
	// can config 가져오기

    try
    {
		CppSQLite3DB db;
		db.open(gszConvDBFile) ;
		

		if (!db.tableExists("TB_CONV_LIST") || !db.tableExists("TB_MODBUS_MAP"))
		{
			//MessageBox("잘못된 db 파일입니다") ;
			MessageBox(Fun_FindMsg("ModbusConvDlg_OnSaveConfig_msg1","IDD_MODBUS_CONV")) ;//&&
			db.close() ;
			return ;
		}

		// 사용자가 설정한 자료가 기존자료인지 신규자료인지 체크
//		CString strQuery ;
		char csQuery[512*2] ;
//		strQuery.Format("SELECT COUNT(*) FROM TB_CONV_LIST WHERE NAME = '%s'", m_strDBName) ;
//		BOOL bUpdate = db.execScalar(strQuery) ;
		sprintf(csQuery, "SELECT COUNT(*) FROM TB_CONV_LIST WHERE NAME = '%s'", m_strDBName) ;
		BOOL bUpdate = db.execScalar(csQuery) ;

		// transaction 시작
//		db.execDML("begin transaction") ;

		// 현재 통신설정값을 TB_CONV_LIST 에 저장한다.
		char buf[512*2] ;

		if (bUpdate)
		{
			sprintf(buf
				  , "UPDATE TB_CONV_LIST       "
				    "   SET CONNECTION  = '%s' "
					"     , IPADDR      = '%s' "
					"     , PORT        = %d   "
					"     , BAUDRATE    = %d   "
					"     , DATABIT     = %d   "
					"     , PARITY      = '%c' "
					"     , STOPBIT     = %d   "
					"     , RESPONSETIMEOUT = %d "
					"     , SLAVEID     = %d   "
					"     , FUNCTIONCD  = %d   "
					"     , MAPFROM     = %d   "
					"     , MAPLENGTH   = %d   "
					"     , SCANRATE    = %d   "
					"     , CANBAUDRATE = '%s' "
					"     , CANEXTENDED = %d   "
					"     , DESC        = '%s' "
					" WHERE NAME = '%s'        "
				  , strConnection, m_strAddr, m_nPort, nBaudrate, nDatabit, cParity, nStopbit
				  , m_nResponseTimeout, m_nSlaveID, nFunctioncd, m_nMBAddr, m_nMBLen, m_nMBScanRate
				  , strCanbaudrate, m_bCheckExtMode, strDesc, m_strDBName) ;
		}
		else
		{
			sprintf(buf
				  , "INSERT INTO TB_CONV_LIST (NAME, CONNECTION, IPADDR, PORT, BAUDRATE, DATABIT, PARITY, STOPBIT, RESPONSETIMEOUT, SLAVEID, FUNCTIONCD, MAPFROM, MAPLENGTH, SCANRATE, CANBAUDRATE, CANEXTENDED, DESC) "
				    "VALUES ('%s', '%s', '%s', %d, %d, %d, '%c', %d, %d, %d, %d, %d, %d, %d, '%s', %d, '%s') ;"
				  , m_strDBName, strConnection, m_strAddr, m_nPort, nBaudrate, nDatabit, cParity, nStopbit, m_nResponseTimeout, m_nSlaveID, nFunctioncd, m_nMBAddr, m_nMBLen, m_nMBScanRate, strCanbaudrate, m_bCheckExtMode, strDesc) ;
		}
		db.execDML(buf) ;

		// 현재 입력한 이름의 SEQ의 값을 얻는다.
//		strQuery.Format("SELECT SEQ FROM TB_CONV_LIST WHERE NAME = '%s'", m_strDBName) ;
		sprintf(csQuery
			  , "SELECT SEQ FROM TB_CONV_LIST WHERE NAME = '%s'"
			  , m_strDBName) ;
//		CppSQLite3Query q = db.execQuery(strQuery) ;
		CppSQLite3Query q = db.execQuery(csQuery) ;
		ASSERT(!q.eof()) ;

		CString strSEQ = q.fieldValue("SEQ") ;
		q.finalize();

		// 기존에 설정되어 있던 동일한 TB_MODBUS_MAP 의 PARENTCD 항목은 모두 삭제한다.
		sprintf(buf
			  , "DELETE FROM TB_MODBUS_MAP WHERE PARENTCD = %s"
			  , strSEQ) ;
		db.execDML(buf) ;

		// Grid 의 설정된 값들을 가져온다.
		CArray<PMCC_INFO, PMCC_INFO> mcc_array ;
		GetModbusConvInfo(mcc_array) ;

		for (INT nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
		{
			MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
			TRACE("addr:%02x | s: %02d | b: %2d | datatype: %d | canid: %03d | s: %02d | b: %2d | f: %.2f | off: %.2f | sul: %.2f | sll: %.2f | tul: %.2f | tll: %.2f | name: %s \n"
				, ptr->addr
				, ptr->mb_startbit
				, ptr->mb_bitcounts
				, ptr->datatype
				, ptr->canid
				, ptr->can_startbit
				, ptr->can_bitcounts
				, ptr->factor
				, ptr->offset
				, ptr->fault_upper
				, ptr->fault_lower
				, ptr->end_upper
				, ptr->end_lower
				, ptr->name) ;

			// 현재 전시된 그리드의 항목을 모두 저장한다.

			// 문자열내에 ', "" 를 교체
			CString name = ptr->name ;
			name.Replace("'", "''") ;
			
			CString desc = ptr->desc ;
			desc.Replace("'", "''") ;

			sprintf(buf
				  , "INSERT INTO TB_MODBUS_MAP (PARENTCD, IDX, ITEMS, STARTBIT, BITCOUNT, DATATYPE, FACTOR, OFFSET, FAULT_UPPER, FAULT_LOWER, END_UPPER, END_LOWER, FUNC_DIVISION, FUNC_DIVISION2, FUNC_DIVISION3, DESC) "
				    "                   VALUES (%s, %d, '%s', %d, %d, %d, %f, %f, %f, %f, %f, %f, %d, %d, %d, '%s')  ;"
				  , strSEQ, ptr->addr, name, ptr->mb_startbit, ptr->mb_bitcounts, ptr->datatype, ptr->factor
				  , ptr->offset, ptr->fault_upper, ptr->fault_lower, ptr->end_upper, ptr->end_lower
				  , ptr->function_division, ptr->function_division2, ptr->function_division3, desc) ;

//			TRACE("%s\n", buf) ;

			db.execDML(buf) ;
		}


		// clearing
		for(int nLoop = 0; nLoop < mcc_array.GetSize(); nLoop++)
		{
			MCC_INFO* ptr = mcc_array.GetAt(nLoop) ;
			delete ptr ;
		}		
		mcc_array.RemoveAll() ;

		// 모드버스맵 행을 TB_MODBUS_MAP에 insert 한다...
/*
		CppSQLite3Query q = db.execQuery("select data from bindata where desc = 'testing';");
		
		if (!q.eof())
		{
			//			TRACE()
			// 		blob.setEncoded((unsigned char*)q.fieldValue("data"));
			// 		cout << "Retrieved binary Length: " << blob.getBinaryLength() << endl;
		}
		q.finalize();
*/
//		db.execDML("commit transaction") ;

	}
    catch (CppSQLite3Exception& e)
    {
		TRACE("%d : %s\n", e.errorCode(), e.errorMessage()) ;
		MessageBox(e.errorMessage()) ;
    }

	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;

}

void CModbusConvDlg::OnSelchangeModbusFunction() 
{
	// TODO: Add your control notification handler code here
	INT nFunctionCD = m_ctlMBFunc.GetCurSel() ;
	
	INT nBaseModbusAddr = 0 ;
	switch (nFunctionCD)
	{
	case 0: nBaseModbusAddr = 0 ;     break ;
	case 1: nBaseModbusAddr = 10000 ; break ;
	case 2: nBaseModbusAddr = 40000 ; break ;
	case 3: nBaseModbusAddr = 30000 ; break ;
	default: ASSERT(FALSE) ;          break ;
	}
	
	INT nCnt = m_wndModGrid.GetRowCount() ;
	
	for (int nRow = 1; nRow < nCnt+1; nRow++)
	{
		INT nOffset = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_INDEX)) ;
		
		CString str ;
		str.Format("%d", nBaseModbusAddr + nOffset) ;
		m_wndModGrid.SetValueRange(CGXRange(nRow, COL_ADDR), str);
	}
	
}

void CModbusConvDlg::OnSelchangeDeviceCommunication() 
{
	CString str ;
	GetDlgItemText(IDC_DEVICE_COMMUNICATION, str) ;


	if (str.Left(3) == "TCP")
	{
		GetDlgItem(IDC_IP_ADDR)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_IP_PORT)->EnableWindow(TRUE) ;

		GetDlgItem(IDC_MB_BAUDRATE)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_DATABITS)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_PARITYBIT)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_STOPBIT)->EnableWindow(FALSE) ;
	}
	else
	{
		GetDlgItem(IDC_IP_ADDR)->EnableWindow(FALSE) ;
		GetDlgItem(IDC_IP_PORT)->EnableWindow(FALSE) ;
		
		GetDlgItem(IDC_MB_BAUDRATE)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_DATABITS)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_PARITYBIT)->EnableWindow(TRUE) ;
		GetDlgItem(IDC_STOPBIT)->EnableWindow(TRUE) ;
	}
}

void CModbusConvDlg::OnClose() 
{
	m_bCanRecvRxTxMsg = FALSE ;
	
	m_pDoc->ClearModbusAllShowMessageFlag() ;
	
	// 쓰레드에서 리스트박스에 쓰는 시간 까지 멈추기
	Sleep(200) ;
	
	// Added by 224 (2014/12/05) : 변수 클리어...
	m_pDoc->m_pModbusDlg = NULL ;

	CDialog::OnCancel();
}

void CModbusConvDlg::OnEtcSetting() 
{
	CString str ;
	GetDlgItemText(IDC_ETC_SETTING, str) ;

	BOOL bMore = FALSE ;
	if (str.Right(2) == ">>")
	{
		bMore = TRUE ;
	}

	CRect rcDialog ;
	GetWindowRect(rcDialog) ;
	int nNewWidth = -1 ;

	CWnd* pWndMoreOrLess = NULL ;
	CRect rcMoreOrLess ;
	if (bMore)
	{
		pWndMoreOrLess = GetDlgItem(IDC_LARGE) ;
		//SetDlgItemText(IDC_ETC_SETTING, "기타설정 <<") ;
		SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("ModbusConvDlg_OnEtcSetting_msg1","IDD_MODBUS_CONV")) ;  //&&
	}
	else
	{
		pWndMoreOrLess = GetDlgItem(IDC_SMALL) ;
		//SetDlgItemText(IDC_ETC_SETTING, "기타설정 >>") ;
		SetDlgItemText(IDC_ETC_SETTING, Fun_FindMsg("ModbusConvDlg_OnEtcSetting_msg2","IDD_MODBUS_CONV")) ;//&&
	}
	ASSERT_VALID(pWndMoreOrLess) ;
	
	pWndMoreOrLess->GetWindowRect(rcMoreOrLess) ;

	nNewWidth = rcMoreOrLess.left - rcDialog.left ;
	SetWindowPos(NULL, 0, 0, nNewWidth, rcDialog.Height(), SWP_NOMOVE | SWP_NOZORDER) ;
	
}

LRESULT CModbusConvDlg::OnGridBeginEdit(WPARAM wParam, LPARAM lParam)
{
	// 적용 버튼은 활성화 시킨다.
	GetDlgItem(IDC_TRANS_CAN_DATA)->EnableWindow(TRUE) ;	
	return 1;
}

// 인덱스 수정 시에 Addr를 자동 바꾸게 한다.
// 베이스 어드레스 가져오는 코드가 상위 소스에서 중복되어 사용
LRESULT CModbusConvDlg::OnGridEndEdit(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);  
	nRow = HIWORD(wParam);	

	if (nCol == COL_INDEX)
	{
		INT nFunctionCD = m_ctlMBFunc.GetCurSel() ;
		
		INT nBaseModbusAddr = 0 ;
		switch (nFunctionCD)
		{
		case 0: nBaseModbusAddr = 0 ;     break ;
		case 1: nBaseModbusAddr = 10000 ; break ;
		case 2: nBaseModbusAddr = 40000 ; break ;
		case 3: nBaseModbusAddr = 30000 ; break ;
		default: ASSERT(FALSE) ;          break ;
		}

		INT nOffset = atoi(m_wndModGrid.GetValueRowCol(nRow, COL_INDEX)) ;
		
		CString str ;
		str.Format("%d", nBaseModbusAddr + nOffset) ;
		m_wndModGrid.SetValueRange(CGXRange(nRow, COL_ADDR), str);
	}
	return 1;
}

void CModbusConvDlg::OnSysCommand(UINT nID, LPARAM lParam) 
{
	// 종료버튼이 눌리면 다이얼로그를 종료한다.
	if((nID & 0xFFF0) == SC_CLOSE)
	{
		EndDialog(IDCANCEL);
		return ;
	}
	
	CDialog::OnSysCommand(nID, lParam);

	return;
}

void CModbusConvDlg::OnSaveRxPacket() 
{
	// TODO: Add your control notification handler code here
	UpdateData() ;
	
	m_pDoc->SetSaveRxDataFlag(m_bSaveRx) ;
}

void CModbusConvDlg::OnSaveTxPacket() 
{
	// TODO: Add your control notification handler code here
	UpdateData() ;
	
	m_pDoc->SetSaveTxDataFlag(m_bSaveTx) ;
}

CString CModbusConvDlg::Fun_FindCanName(int nDivision)
{
	CString strName;
	int nSelectPos, nFindPos;
	for (int i=0; i < m_uiArryCanCode.GetSize() ; i++)
	{
		if (nDivision == m_uiArryCanCode.GetAt(i))
		{
			strName.Format("%s [%d]", m_strArryCanName.GetAt(i), nDivision);
			return strName;
		}
	}
	strName.Format("None [%d]", nDivision);
	return strName;
}
