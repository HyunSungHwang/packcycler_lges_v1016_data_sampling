#if !defined(AFX_UARTCONVDLG_H__C1431538_6093_41DF_A5B5_265F0863DD93__INCLUDED_)
#define AFX_UARTCONVDLG_H__C1431538_6093_41DF_A5B5_265F0863DD93__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UartConvDlg.h : header file
//

#include "../CTSMonProDoc.h"
#include "xlCANStandAloneFunc.h"
#include "xlCANFunctions.h"
#include "UartCanConvBroker.h"
#include "../MyGridWnd.h"	// Added by ClassView

/////////////////////////////////////////////////////////////////////////////
// CUartConvDlg dialog

class CUartConvDlg : public CDialog
{
// Construction
public:

	CUartConvDlg(CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor
	CUartConvDlg();
	~CUartConvDlg();

// Dialog Data
	//{{AFX_DATA(CUartConvDlg)
	enum { IDD = IDD_UART_CONV , IDD2 = IDD_UART_CONV_ENG , IDD3 = IDD_UART_CONV_PL };
	CComboBox	m_Channel;
	CListBox	m_ctlRxList;
	CComboBox	m_CAN_Baudrate;
	CComboBox	m_ctrlStopbit;
	CComboBox	m_ctrlParitybit;
	CComboBox	m_ctrlDatabits;
	CComboBox	m_ctrlBaudrate;
	BOOL	m_bCheckExtMode;
	//}}AFX_DATA

	// Attributes
public:
	int nSelCh;
	int nSelModule;
	int m_nDevID ;
	
	CString m_strConfigFilename ;
	//	CString m_strDBName ;	// 현재 설정 DB 값 보관
	
	//	CCANFunctions m_CAN;
//	CCANStandAloneFunc m_CAN;
	CCTSMonProDoc* m_pDoc ;
	
	BOOL m_bCanRecvRxTxMsg ;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUartConvDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Operations
public:
// 	void InitParamTab();
	void InitGridRx();
// 	void InitGridTx();
	void InitGridSchedule();

	BOOL CheckMapValues() ;
	BOOL SaveCANConfig(CArray<PUCC_INFO, PUCC_INFO>& array);
	void SendCANData(XLevent& xlEvent);

private:
	CUIntArray m_uiArryCanCode;
	CStringArray m_strArryCanName;
	CMyGridWnd m_wndScheduleGrid;
	CMyGridWnd m_wndUartGrid;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUartConvDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddScheRow();
	afx_msg void OnDelScheRow();
	afx_msg void OnAddRow();
	afx_msg void OnDelRow();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void OnOpenConvData();
	afx_msg void OnSaveConfig();
	afx_msg void OnTransCanData();
	afx_msg void OnOperStart();
	afx_msg void OnOperStop();
	afx_msg void OnEtcSetting();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()



public:
	
	// Added by 224 (2015/03/25) : csv 파일로 Load/Save 적용
	BOOL LoadUartConfig(CString strLoadPath);
	BOOL SaveUartConfig(CString strSavePath);
	void GetScheduleInfo(CArray<PUARTSCHEDULER_INFO, PUARTSCHEDULER_INFO>& array) ;
	void GetUartConvInfo(CArray<PUCC_INFO, PUCC_INFO>& array);

	void Fun_ChangeViewControl(BOOL bFlag);
	CString Fun_FindCanName(int nDivision);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UARTCONVDLG_H__C1431538_6093_41DF_A5B5_265F0863DD93__INCLUDED_)
