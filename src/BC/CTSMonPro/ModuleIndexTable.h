// ModuleIndexTable.h: interface for the CModuleIndexTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODULEINDEXTABLE_H__48846CD9_79B0_4A1E_8BC4_97F8343961C1__INCLUDED_)
#define AFX_MODULEINDEXTABLE_H__48846CD9_79B0_4A1E_8BC4_97F8343961C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// typedef struct s_Module_Index_Info
// {
// 	long lFileSeqNo;
// 	long lStartNo;
// 	long lEndNo;
// 	char szStartDate[32];
// } CT_MD_INDEX_INFO, *LPCT_MD_INDEX_INFO;

//ljb 201007
typedef struct s_Module_Index_Info
{
	long lFileSeqNo;
	long lStartNo;
	long lEndNo;
	long lLastFileIndex;
	char szStartDate[32];
} CT_MD_INDEX_INFO, *LPCT_MD_INDEX_INFO;

class CModuleIndexTable  
{
public:
	long GetRawDataSize(int nSequenceNo, int nStartNo = 0);
	long FindFirstMachFileNo(long nStartNo);
	int GetLastCount();
	BOOL LoadTable();
	void RemoveModuleIndexList();
	CModuleIndexTable();
	CModuleIndexTable(CString strStartFile, CString strEndFile)	{	m_strStartFile = strStartFile;  m_strEndFile = strEndFile;	}
	virtual ~CModuleIndexTable();
	void SetFileName(CString strStartFile, CString strEndFile) {	m_strStartFile = strStartFile;  m_strEndFile = strEndFile;	}

protected:
	CString m_strStartFile;
	CString m_strEndFile;

	CPtrList		m_ptrListModuleIndex;	//모듈에서 download한 index파일의 list

};

#endif // !defined(AFX_MODULEINDEXTABLE_H__48846CD9_79B0_4A1E_8BC4_97F8343961C1__INCLUDED_)
