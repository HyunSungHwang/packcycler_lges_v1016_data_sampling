// CTSMonPro.h : main header file for the CTSMonPro application
//

#if !defined(AFX_CTSMonPro_H__224D9B67_53A2_4CAE_8FE6_98FF57933E0A__INCLUDED_)
#define AFX_CTSMonPro_H__224D9B67_53A2_4CAE_8FE6_98FF57933E0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCTSMonProApp:
// See CTSMonPro.cpp for the implementation of this class
//




class CCTSMonProApp : public CWinApp
{
public:
	void LoadChannelColTitle();
	//int m_nUnitNo;
	CString m_pstrChannelColTitle[PS_MAX_ITEM_NUM];		//32
	int m_nMaxChannelColNum;
	
	CString GetColName(int nIndex) { return m_pstrChannelColTitle[nIndex]; }
	int     GetColNum()	{return m_nMaxChannelColNum;}
	void    SetColName(int nIndex, CString strData);
	
	BOOL Fun_ReceiveStepEnd_Data(CCyclerChannel *pChannel,PS_STEP_END_RECORD *tStepEnd);
	void Fun_CycSendCommand(CCyclerChannel *pChannel,int nCmd);
	void Fun_SbcWorkEnd(CCyclerChannel *pChannel);
	void Fun_SbcWorkPause(CCyclerChannel *pChannel); //ksj 20200601
	
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CCTSMonProApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSMonProApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCTSMonProApp)
	afx_msg void OnAppAbout();
	afx_msg void OnAppFolder(); //lyj 20210108

	afx_msg void OnMenuLangEng();
	afx_msg void OnMenuLangKor();
	afx_msg void OnUpdateLangEng(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLangKor(CCmdUI* pCmdUI);
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
extern CCTSMonProApp theApp;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSMonPro_H__224D9B67_53A2_4CAE_8FE6_98FF57933E0A__INCLUDED_)
