#include "stdafx.h"


#include "ctsmonpro.h"
#include "SendToFTP_v1014_v1_SCH.h"


CSendToFTP_v1014_v1_SCH::CSendToFTP_v1014_v1_SCH(void)
{
}

CSendToFTP_v1014_v1_SCH::~CSendToFTP_v1014_v1_SCH(void)
{
}

BOOL CSendToFTP_v1014_v1_SCH::SendSchFTP_v1014_v1_SCH(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, int nOption,int ReserveID, CCTSMonProDoc* pDoc)
{
	//pProgressWnd->SetText(_T("스케줄 가공중 "));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg1","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(30);

	CStringArray	arryStrPatternFile;
	CString	strTemp,strMsg,strFileName,strFileName2,strDsc;
	short int	nWaitTimeGotoCnt = 0;	//ljb 20160427 add

	strFileName2.Empty();

	CCyclerModule *pMD;
	CCyclerChannel *pChInfo;
	CStep *pStepData;

	int nRtn = 0;
	int ch = 0;
	int nPatternCount = 0; //스케쥴에 있는 패턴 STEP 갯수
	arryStrPatternFile.RemoveAll();		//SBC용 스케쥴 파일에 저장할 Pattern 파일 이름

	UINT k;
	CUIntArray arryTimeType;
	CUIntArray arryModeType;
	CUIntArray arryPlusMode;
	CUIntArray arryMaxValue;
	
	CUIntArray arryFileSize;
	CUIntArray arryCheckSum;
	
	long lTimeType, lModeType, lPulsMode,lMaxValue;
	ULONG lFileSize=0, lCheckSum=0;
	ULONG lFileSize2=0, lCheckSum2=0;
	float fMaxValue=0;
	int nPos = 0;
	
	pMD		=	pDoc->GetModuleInfo(nModuleID);

	//현재 시간 구함 
	CString strTimeCheck;
	CTime tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 전송 시작 (SendScheduleToModuleFTP :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg2","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);

	//0. FTP 접속
	CString strIP = pMD->GetIPAddress();
	if(strIP.IsEmpty())		{
		//AfxMessageBox("모듈 IP 이상");//$1013
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg3","CTSMonPro_DOC"));//&&
		return FALSE;
	}
	if (pDoc->Fun_FtpConnect(strIP) == FALSE)		{
		if (pDoc->Fun_FtpConnect(strIP) == FALSE)		{
			//AfxMessageBox("FTP 접속 실패 !!!");//$1013
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg4","CTSMonPro_DOC"));//&&
			return FALSE;
		}
	}

	//pProgressWnd->SetText(_T("접속 완료"));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg5","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(40);

	//strMsg.Format(_T("접속 완료."));//$1013
	strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg6","CTSMonPro_DOC"));//&&

	SFT_STEP_START_INFO stepInfo;
	ZeroMemory(&stepInfo, sizeof(SFT_STEP_START_INFO));	

	//+2015.9.23 USY Add For PatternCV
	UINT nParallelCnt = 0;
	//-

	//1. FTP 폴더에 run.txt 파일 있는지 체크
	//2. FTP 폴더내 모든 파일 삭제, 없으면 생성
	CString strChangeDir;
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = pDoc->GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pChInfo)
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
			if(g_AppInfo.iPType==1)
			{
				//ksj 20180223 : 주석처리
				if (pChInfo->GetChannelIndex() < 1)       
				{
					strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
				}
				else if (pChInfo->GetChannelIndex() == 1)
				{
					strChangeDir.Format("/START_INFO/CH%03d",3);	//ljb 201706 add
				}
			}
//#else
			else
			{
				//ksj 20180223 : 기존 코드 원복
				strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
			}//-----------------------------------------------------------------
//#endif
			if (pDoc->Fun_FtpChangeDir(strChangeDir) == TRUE)
			{
				if (pDoc->Fun_FtpFileFind("run.txt") == TRUE)
				{//$1013
					//strMsg.Format("%s SBC에서 작업 중인 채널 입니다.(ch : %d)",pDoc->GetModuleName(nModuleID), pChInfo->GetChannelIndex()+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg7","CTSMonPro_DOC"),pDoc->GetModuleName(nModuleID), pChInfo->GetChannelIndex()+1);//&&
					pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
					AfxMessageBox(strMsg);
					return FALSE;			
				}
				else
				{
					if (pDoc->Fun_FtpFileDeleteAll(strChangeDir) == FALSE)
					{//$1013
						//strMsg.Format("%s 에서 파일 삭제 에러.(%s)",pDoc->GetModuleName(nModuleID),strChangeDir);
						strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg8","CTSMonPro_DOC"),pDoc->GetModuleName(nModuleID),strChangeDir);//&&
						pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
						AfxMessageBox(strMsg);
					}
				}
			}
			//+2015.9.23 USY Add For PatternCV
			if(pChInfo->IsParallel()) nParallelCnt++;
			//-
		}
		else
		{//$1013
			//strMsg.Format("sbc에 Run.txt 파일 이상.(ch : %d)",pChInfo->GetChannelIndex()+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg9","CTSMonPro_DOC"),pChInfo->GetChannelIndex()+1);//&&
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
			AfxMessageBox(strMsg);
			return FALSE;			
		}
	}
	//pProgressWnd->SetText(_T("패턴의 유무를 확인중입니다."));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg10","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(60);

	//3. 전송할 스케쥴 확인 (Pattern 확인)
	if(pSchData->GetStepSize() <= 0)
	{//$1013
		//strMsg.Format("%s 에 전송할 스케줄이 없습니다.!!!", pDoc->GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg11","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID));//&&
		pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}
	//+2015.9.23 USY Add For PatternCV
	float fMinVoltage;
	if(pDoc->m_bOverChargerSet) //yulee 20180227 
		fMinVoltage= -30000.0f;
	else
		fMinVoltage= 0.0f;
	float fMaxVoltage = 0.0f;
	float fMaxCurrent = 0.0f;

	
	CString rng, str;
	fMaxVoltage = pMD->GetVoltageSpec()/1000.0f;
	for(int r=0; r<pMD->GetCurrentRangeCount(); r++)
	{
		if((pMD->GetCurrentSpec(r)/1000.0f) > fMaxCurrent)
		{
			fMaxCurrent = pMD->GetCurrentSpec(r)/1000.0f;
		}
	}
	//-

	for(k =0; k<pSchData->GetStepSize(); k++)
	{
		pStepData = pSchData->GetStepData(k);
		if(pStepData == NULL)
		{//$1013
			//strMsg.Format("%s 에 전송할 스케줄 STEP %d 가 NULL 입니다.!!!", pDoc->GetModuleName(nModuleID),k+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg12","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID),k+1);//&&
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		
		////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 시작 ////////////////
		lTimeType = lModeType = lPulsMode = lMaxValue = 0;
		lFileSize = lCheckSum = 0;
		strFileName ="";

		//WaitTimeGotoStep count 확인
		if (pStepData->m_nWaitTimeInit > 0 || pStepData->m_nWaitTimeDay > 0 
			|| pStepData->m_nWaitTimeHour > 0 || pStepData->m_nWaitTimeMin > 0  || pStepData->m_nWaitTimeSec > 0)
		{
			nWaitTimeGotoCnt++;
		}
		//2014.09.11 PS_STEP_USER_MAP 추가. 
		//20180427 yulee 예약시작 시 패턴 path 가져오기에 대한 수정
		//예약시작 시 패턴 데이터를 가져오는 pStaepData는 주기적으로 초기화가 이루어지므로 
		//pStepData->m_strPatternFileName은 비어 있음. 
		//따라서 예약 DB에 저장된 스케쥴 path를 통하여 다시 해당 스텝에 대한 패턴 path를 가져와 적용 시켜야함.
		
		if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
		{
			//			2013-06-12			bung			:			wait_FTP_endTime_plusPtrn
			pDoc->m_wait_FTP_endTime_plusPtrn += 10;
			
			if(nOption == 2) //WORK_START_RESERV_RUN 2로 예약기능에서 사용 중
			{//패턴 해당 path를 sch에서 가져온다
				bool bRet = -1;
				bRet = pDoc->GetPattPathFromDB(ReserveID, k, strFileName);
				if(bRet == TRUE)
				{
					pStepData->m_strPatternFileName = strFileName;
				}
			}
			else //기존과 동일 
				strFileName = pStepData->m_strPatternFileName;
			nPatternCount++;
			
			//+2015.9.23 USY Add For PatternCV	
			
			if(pDoc->CheckPatternData(strFileName, fMaxVoltage, fMaxCurrent, nParallelCnt) == FALSE)
			{//$1013
				//strMsg.Format("%s Pattern File 오류", strFileName);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg13","CTSMonPro_DOC"), strFileName);//&&
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			//-
			
			if (pStepData->m_fValueMax != 0) lMaxValue = pStepData->m_fValueMax / 1000;		//ljb 20130509 add
			else lMaxValue = pStepData->m_fValueMax;										//ljb 20130509 add
			
			//ljb 20130514 메모리로 읽고 CheckSum 후 strDsc로 파일 복사 생성 한다.
			if(pStepData->m_type == PS_STEP_PATTERN){
				strDsc.Format("%s\\Pattern\\sbc_pattern_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}else if(pStepData->m_type == PS_STEP_USER_MAP){ //2014.09.11 PS_STEP_USER_MAP 추가.
				strDsc.Format("%s\\Pattern\\sbc_usermap_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}
			
			
			if(pDoc->Fun_MemoryCopyFileAndCheckSum(strFileName, strDsc, lFileSize, lCheckSum) == FALSE)
			{//$1013
				//strMsg.Format("%s 에 Simulation File Size, checksum 실패!!! %s (File size = %d, CheckSum = %d", pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg14","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);//&&
				AfxMessageBox(strMsg);
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			Sleep(100);	//201305 슬립이 있어야 CSV 파일이 안 깨짐
			//strMsg.Format("%s 에 Simulation File Size, checksum 성공 !!! %s (File size = %d, CheckSum = %d", pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg15","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);//&&
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
		
			//TRACE("STEP %d (Pattern Data check 완료)\r\n", pStepData->m_StepIndex);	//$1013			
			TRACE(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg55","CTSMonPro_DOC"), pStepData->m_StepIndex);	//&&
			////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 끝 ////////////////
			arryStrPatternFile.Add(strDsc);
		}
		else
		{
			arryStrPatternFile.Add(strFileName);
		}
		/*
		//2014.09.11 PS_STEP_USER_MAP 추가. 
		if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
		{
//			2013-06-12			bung			:			wait_FTP_endTime_plusPtrn
			m_wait_FTP_endTime_plusPtrn += 10;
	
			strFileName = pStepData->m_strPatternFileName;
			nPatternCount++;

			//+2015.9.23 USY Add For PatternCV	

			if(CheckPatternData(strFileName, fMaxVoltage, fMaxCurrent, nParallelCnt) == FALSE)
			{
				strMsg.Format("%s Pattern File 오류", strFileName);
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			//-

			if (pStepData->m_fValueMax != 0) lMaxValue = pStepData->m_fValueMax / 1000;		//ljb 20130509 add
			else lMaxValue = pStepData->m_fValueMax;										//ljb 20130509 add

			//ljb 20130514 메모리로 읽고 CheckSum 후 strDsc로 파일 복사 생성 한다.
			if(pStepData->m_type == PS_STEP_PATTERN){
				strDsc.Format("%s\\Pattern\\sbc_pattern_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}else if(pStepData->m_type == PS_STEP_USER_MAP){ //2014.09.11 PS_STEP_USER_MAP 추가.
				strDsc.Format("%s\\Pattern\\sbc_usermap_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}


			if(Fun_MemoryCopyFileAndCheckSum(strFileName, strDsc, lFileSize, lCheckSum) == FALSE)
			{
				strMsg.Format("%s 에 Simulation File Size, checksum 실패!!! %s (File size = %d, CheckSum = %d", pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);
				AfxMessageBox(strMsg);
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			Sleep(100);	//201305 슬립이 있어야 CSV 파일이 안 깨짐
			strMsg.Format("%s 에 Simulation File Size, checksum 성공 !!! %s (File size = %d, CheckSum = %d", pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);

			TRACE("STEP %d (Pattern Data check 완료)\r\n", pStepData->m_StepIndex);				
		////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 끝 ////////////////
			arryStrPatternFile.Add(strDsc);
		}
		else
		{
			arryStrPatternFile.Add(strFileName);
		}
		*/
		arryTimeType.Add(lTimeType);
		arryModeType.Add(lModeType);
		arryPlusMode.Add(lPulsMode);
		arryMaxValue.Add(lMaxValue);

		arryFileSize.Add(lFileSize);
		arryCheckSum.Add(lCheckSum);
	}

	stepInfo.byTotalStep = pSchData->GetStepSize();
	stepInfo.nPatternStepCount = nPatternCount;
	stepInfo.bWaitTimeGotoCount = nWaitTimeGotoCnt;		//ljb 20160427 add

	//pProgressWnd->SetText(_T("스케줄 전송 준비 완료"));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg16","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(70);

	strMsg.Format(" SFT_CMD_SCHEDULE_START :: (1020) before sendcmmand", pDoc->GetModuleName(nModuleID), nRtn);
	pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
 //			2013-11-28			bung			:			SelChArray Dump S
	CWordArray *pDump_SelChArray;
	pDump_SelChArray = pSelChArray;

	// 스케쥴 시작
	if((nRtn = pMD->SendCommand(SFT_CMD_SCHEDULE_START, pSelChArray, &stepInfo, sizeof(SFT_STEP_START_INFO))) != SFT_ACK)
	{//$1013
		//strMsg.Format("%s 에 스케쥴 전송 준비 실패!!!(Code %d)", pDoc->GetModuleName(nModuleID), nRtn);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg17","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), nRtn);//&&
		pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}
	else
	{//$1013
		//strMsg.Format("%s 에 스케쥴 전송 준비 성공", pDoc->GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg18","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID));//&&
		pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
	}

	strMsg.Format(" SFT_CMD_SCHEDULE_START :: (1020) after sendcmmand", pDoc->GetModuleName(nModuleID), nRtn);
	pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);

	SFT_STEP_CONDITION_V2 stepCon; //lyj 20200214 LG v1015 미만
	SFT_STEP_CONDITION_WAIT_TIME stepConWaitTime;	//ljb 20160427 add


	BOOL bUseChiller = FALSE;
	//4. FTP 전송용 스케쥴 파일 생성
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = pDoc->GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
 		if(pChInfo)
		{
			strFileName = pChInfo->GetScheduleFileForSbc();
						
			//2014.12.10 간이 충방전일경우 저장시간을 설정안할경우. 임시폴더를 생성하고 임시 스케줄 파일을 만들어 공정을 돌린다.
			if(pDoc->m_bSimpleTest){
				CString sPath;
				sPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC,"Data Path","C:\\");
				//폴더를 생성한다.				
				strFileName.Format("%s\\simpleTest\\M%02dCh%02d[%03d]\\StepStart",sPath,nModuleID,pChInfo->GetChannelIndex() + 1,1);		//ljb 20150508 add
				pDoc->ForceDirectory(strFileName);
				strFileName.Format("%s\\simpleTest\\M%02dCh%02d[%03d]",sPath,nModuleID,pChInfo->GetChannelIndex() + 1,1);
				pChInfo->SetFilePath(strFileName);
				strFileName.Format("%s\\StepStart\\sbc_schedule_info.sch",strFileName);
				DeleteFile(strFileName);
			}
			
			//strMsg.Format("%s 파일 생성 시작", strFileName);//$1013
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg19","CTSMonPro_DOC"), strFileName);//&&
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.21
			//if(FALSE == pSchData->SaveToFile(strFileName, FALSE))
			if(FALSE == pSchData->SaveToFile_ForFTP(strFileName, FALSE))		//sbc로 전송할 sbc_schedule.sch file 생성 (0k byte) 	
			{//$1013
				//strMsg.Format("%s 에 %d 채널 스케쥴 파일 생성 실패.!!!", pDoc->GetModuleName(nModuleID), ch+1);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg20","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			// -
			if (nWaitTimeGotoCnt > 0)
			{
				strFileName2 = pChInfo->GetScheduleFileForSbc2();					//20160427 sbc_schedule_info_wait_goto.sch
				if(FALSE == pSchData->SaveToFile_ForFTP(strFileName2, FALSE))		//20160427 sbc로 전송할 sbc_schedule_info_wait_goto.sch file 생성 (0k byte) 	
				{
					//strMsg.Format("%s 에 %d 채널 WaitGoto 스케쥴 파일 생성 실패.!!!", pDoc->GetModuleName(nModuleID), ch+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg21","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
					pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
					return FALSE;
				}
			}
			//////////////////////////////////////////////////////////////////////////


			/*UINT nProtocolVer = pChInfo->GetProtocolVersion();
			if (nProtocolVer == _SFT_PROTOCOL_VERSION)
			{
			}*/

			
///////////////////////////////////////스케줄 시작 정보 저장 시작 ////////////////////////////////////////////////////////
			//file에 추가
			CFile rltData;
			CFileException e;
			long fsize = 0;
			
			CFile rltData2;
			CFileException e2;
			long fsize2 = 0;
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.21
			//if(!rltData.Open(strFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			if(!rltData.Open(strFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			// - 
			//////////////////////////////////////////////////////////////////////////
			{//$1013
				//strMsg.Format("%s 에 %d 채널 스케쥴 파일 추가 실패.!!!", pDoc->GetModuleName(nModuleID), ch+1);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg22","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
				pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			fsize = rltData.SeekToEnd();
			rltData.Seek(0, CFile::end);

			if(rltData.m_hFile != NULL)
			{
				rltData.Write(&stepInfo, sizeof(SFT_STEP_START_INFO));
				rltData.Flush();
			}

			//ljb 20160427 waitTimeGoto write
			if (nWaitTimeGotoCnt > 0)
			{
				if(!rltData2.Open(strFileName2, CFile::modeReadWrite|CFile::shareDenyWrite, &e2))
					// - 
					//////////////////////////////////////////////////////////////////////////
				{
					//strMsg.Format("%s 에 %d 채널 스케쥴 파일 추가 실패.!!!", pDoc->GetModuleName(nModuleID), ch+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg23","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
					pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
					return FALSE;
				}
				fsize2 = rltData2.SeekToEnd();
				rltData2.Seek(0, CFile::end);
				
				if(rltData2.m_hFile != NULL)
				{
					rltData2.Write(&stepInfo, sizeof(SFT_STEP_START_INFO));
					rltData2.Flush();
				}
			}
			///////////////////////////////////////안전조건 저장 시작 ////////////////////////////////////////////////////////
			SFT_TEST_SAFETY_SET_V1 safetyInfo;
			ZeroMemory(&safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1));
			
			CELL_CHECK_DATA* pCheckData = pSchData->GetCellCheckParam();
			//Voltage Limit
			safetyInfo.lVtgHigh	=	FLOAT2LONG(pCheckData->fMaxV);
			safetyInfo.lVtgLow	=	FLOAT2LONG(pCheckData->fMinV);
			safetyInfo.lVtgCellDelta	= FLOAT2LONG(pCheckData->fCellDeltaV);		//ljb 20150730
			
			//yulee 20181114 Protocol 1013
			float fltCellVolt = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", 4600);
			float fltFixTempHigh = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", 80);
			safetyInfo.lVtgCellHigh = FLOAT2LONG(fltCellVolt);
			safetyInfo.lTempHigh	= FLOAT2LONG(fltFixTempHigh);

			//Current Limit
			safetyInfo.lCrtHigh	= FLOAT2LONG(pCheckData->fMaxI);
			
			//Temperature limit
			//safetyInfo.lTempHigh = pCheckData->fMaxT;
			safetyInfo.lTempLow = pCheckData->fMinT;
			
			//Capacity Limit
			safetyInfo.lCapHigh	= FLOAT2LONG(pCheckData->fMaxC);			//Vref
			safetyInfo.lWattHigh	= pCheckData->lMaxW;			//Vref
			safetyInfo.lWattHourHigh = pCheckData->lMaxWh;			//Vref
			
			//ljb 20100820
			for (int i=0; i < _SFT_MAX_CAN_AUX_COMPARE_STEP; i++)
			{
				safetyInfo.can_function_division[i] = pCheckData->ican_function_division[i];
				safetyInfo.can_compare_type[i] = pCheckData->cCan_compare_type[i];
				safetyInfo.can_data_type[i] = pCheckData->cCan_data_type[i];
				safetyInfo.fcan_Value[i] = pCheckData->fcan_Value[i];
				safetyInfo.aux_function_division[i] = pCheckData->iaux_function_division[i];
				safetyInfo.aux_compare_type[i] = pCheckData->cAux_compare_type[i];
				safetyInfo.aux_data_type[i] = pCheckData->cAux_data_type[i];
				safetyInfo.faux_Value[i] = pCheckData->faux_Value[i];
			}

			if(rltData.m_hFile != NULL)
			{
				rltData.Write(&safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1));
				rltData.Flush();
			}

			///////////////////////////////////////스텝 데이터 저장 시작 ////////////////////////////////////////////////////////
			CStep *pStepData;

			//2. Schedule Step Data Send command
			for(k =0; k<pSchData->GetStepSize(); k++)
			{
				pStepData = pSchData->GetStepData(k);
				if(pStepData == NULL)	break;
			
				if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)	{
					//+2015.9.22 USY Mod For PatternCV(김재호K)
					//if((pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge <= 0)&& (pStepData->m_mode !=PS_MODE_CV))
					if((pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge < fMinVoltage)&& (pStepData->m_mode !=PS_MODE_CV)) //ksj 20160703 조건 수정.
					{//$1013
						//pDoc->m_strLastErrorString.Format("Step %d의 충전CV전압이나 방전CV전압이 설정되지 않았습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg24","CTSMonPro_DOC"), k+1); //&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
// 					if(pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge <= 0)		{
// 						pDoc->m_strLastErrorString.Format("Step %d의 충전CV전압이나 방전CV전압이 설정되지 않았습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 						AfxMessageBox(pDoc->m_strLastErrorString);
// 						return FALSE;
// 					}
					//-
					else	{//$1013
						//if(pStepData->m_fEndV_H > fMinVoltage && pStepData->m_fVref_Charge <= pStepData->m_fEndV_H)	{
						if(pStepData->m_fEndV_H > fMinVoltage && pStepData->m_fVref_Charge <= pStepData->m_fEndV_H && pStepData->m_fEndV_H != 0)	{ //ksj 20160711 조건 수정
							//pDoc->m_strLastErrorString.Format("Step %d의 충전전압 설정값이 종료전압H보다 낮습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg25","CTSMonPro_DOC"), k+1);//&&
							AfxMessageBox(pDoc->m_strLastErrorString);
							return FALSE;
						}//$1013
						//if(pStepData->m_fEndV_L > fMinVoltage && pStepData->m_fVref_DisCharge >= pStepData->m_fEndV_L)
						if(pStepData->m_fEndV_L > fMinVoltage && pStepData->m_fVref_DisCharge >= pStepData->m_fEndV_L && pStepData->m_fEndV_L != 0) //ksj 20160711 조건 수정
						{
							//pDoc->m_strLastErrorString.Format("Step %d의 방전전압 설정값이 종료전압L보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg26","CTSMonPro_DOC"), k+1);//&&
							AfxMessageBox(pDoc->m_strLastErrorString);
							return FALSE;
						}
					}			
				}

				if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)	
				{
					if(strlen(pStepData->m_strPatternFileName) <= 0)	
					{//$1013
						//pDoc->m_strLastErrorString.Format("Step %d의 Simulation data file이 지정되지 않았습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg27","CTSMonPro_DOC"), k+1);//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}

					if( pStepData->m_fEndDI <= 0 && pStepData->m_fEndDI <= 0 && pStepData->m_fEndW <= 0 &&
						pStepData->m_fEndDV <=0 && pStepData->m_fEndI <=0 && pStepData->m_fEndW <= 0 &&
						pStepData->m_fEndV_H <=fMinVoltage && pStepData->m_fEndV_L <=fMinVoltage && pStepData->m_fEndTime == 0)			
					{
						//pDoc->m_strLastErrorString.Format("Step %d의 종료 조건이 설정되어 있지 않았습니다.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg28","CTSMonPro_DOC"), k+1);			//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
				}

				if(pStepData->m_type == PS_STEP_CHARGE)		{//$1013
					if(pStepData->m_fVref_Charge <= fMinVoltage)		{
						//pDoc->m_strLastErrorString.Format("Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.	(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg29","CTSMonPro_DOC"), k+1);		//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
					if(pStepData->m_fVref_Charge <= pStepData->m_fEndDV)		{//$1013
						//pDoc->m_strLastErrorString.Format("Step %d의 전압변화 설정값이 너무 큽니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg30","CTSMonPro_DOC"), k+1);			//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;				
					}
					
					if(pStepData->m_fVref_Charge <= pStepData->m_fEndV_H)			{//$1013
						//pDoc->m_strLastErrorString.Format("Step %d의 종료 전압이 설정 전압보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg31","CTSMonPro_DOC"), k+1);			//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
				} //charge
					
				if(pStepData->m_type == PS_STEP_DISCHARGE)			{//$1013
					if(pStepData->m_fVref_DisCharge < fMinVoltage && pStepData->m_fEndV_L < fMinVoltage)		{
							//pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 종료 전압보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							//pDoc->m_strLastErrorString.Format("Voltage step(%d) setting higher than the end of the voltage. Check on the program schedule a file editor.", k+1);
							pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg56","CTSMonPro_DOC"), k+1);//&&
							AfxMessageBox(pDoc->m_strLastErrorString);
							return FALSE;
						}
				//	}
				} //Discharge
//$1013
				if(pStepData->m_fTref == 0.0f && (pStepData->m_fStartT !=0.0f || pStepData->m_fEndT != 0.0f))		{
					//pDoc->m_strLastErrorString.Format("Step %d의 온도 설정값을 입력하지 않고 시작온도나 종료온도가 설정되었습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg32","CTSMonPro_DOC"), k+1);			//&&
					AfxMessageBox(pDoc->m_strLastErrorString);
					return FALSE;
				}
					
				//+2015.9.22 USY Mod For PatternCV(김재호K)
				if((pStepData->m_mode != PS_MODE_CV) && (pStepData->m_type != PS_STEP_PATTERN))
				{//$1013
					if(  pStepData->m_fVref_Charge < fMinVoltage)		{
						//pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg33","CTSMonPro_DOC"), k+1);//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}//$1013
					if(  pStepData->m_fVref_DisCharge < fMinVoltage)		{
						//pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg34","CTSMonPro_DOC"), k+1);//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
				}
				//전압 설정값 입력 범위 검사
// 				if(  pStepData->m_fVref_Charge < 0.0f)		{
// 					pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 					AfxMessageBox(pDoc->m_strLastErrorString);
// 					return FALSE;
// 				}
// 				if(  pStepData->m_fVref_DisCharge < 0.0f)		{
// 					pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 					AfxMessageBox(pDoc->m_strLastErrorString);
// 					return FALSE;
// 				}
				//-
				//전압 종료값 입력 범위 검사
				if(pStepData->m_fEndV_H < fMinVoltage)					{//$1013
					//pDoc->m_strLastErrorString.Format("Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg35","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);
					return FALSE;
				}
				//전압 종료값 입력 범위 검사
				if(pStepData->m_fEndV_L < fMinVoltage)					{//$1013
					//pDoc->m_strLastErrorString.Format("Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg36","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);
					return FALSE;
				}
				//전류 종료값 입력 범위 검사
				if(pStepData->m_fEndI < 0.0f)					{//$1013
					//pDoc->m_strLastErrorString.Format("Step %d의 종료 전류값이 입력 범위를 벗어났습니다.(범위:0mA) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg37","CTSMonPro_DOC"), k+1);//&&	
					AfxMessageBox(pDoc->m_strLastErrorString);
					return FALSE;
				}

				//전압 제한값 입력 범위 검사
				if(pStepData->m_fLowLimitV < 0)				{//$1013
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 전압 상하한값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg38","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);
					return FALSE;
				}
				
				//전압 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitV > 0.0f && pStepData->m_fLowLimitV > 0.0f && pStepData->m_fHighLimitV < pStepData->m_fLowLimitV)			{
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 전압 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg39","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}

				//전류 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitI > 0 && pStepData->m_fLowLimitI > 0 && pStepData->m_fHighLimitI < pStepData->m_fLowLimitI)				{
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 전류 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg40","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}

				//온도 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitTemp > 0 && pStepData->m_fLowLimitTemp > 0 && pStepData->m_fHighLimitTemp < pStepData->m_fLowLimitTemp)						{
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 온도 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg41","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				stepCon = pDoc->GetNetworkFormatStep(pStepData);

				//stepCon_v7 = GetNetworkFormatStep_v7(pStepData);
				//2014.09.11 PS_STEP_USER_MAP 추가.
				if (stepCon.stepHeader.nStepTypeID == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
				{
					stepCon.bPatternTime = arryTimeType.GetAt(k);
					//stepCon.bPatternType = arryModeType.GetAt(k);
					//stepCon.bPatternMode = arryPlusMode.GetAt(k);
					lMaxValue = arryMaxValue.GetAt(k);

					stepCon.fPatternMaxValue = lMaxValue;
					stepCon.lPatternFileSize = arryFileSize.GetAt(k);		//v1007
					stepCon.lPatternChecksum = arryCheckSum.GetAt(k);		//v1007
				}
				
				stepCon.bCanRxEndNoCheck	= pStepData->m_nNoCheckMode;	//20150825 CAN RX 안전조건, 종료조건 무시	(2014.12.08 이민규 대리 추가)
				stepCon.bCanTxOffMode		= pStepData->m_nCanTxOffMode;	//20150825 CAN TX 안함						(2014.12.08 이민규 대리 추가)
				stepCon.bStepCanCheckMode	= pStepData->m_nCanCheckMode;	//ljb 20150825 add

				//yulee 20190114
				//ljb 20190114 add s 셀밸런스 값이 있으면 SBC로 내리는  Flag를 1로 변경 한다. 
				if (pStepData->m_nCellBal_CircuitRes > 0)
				{
					stepCon.bUseCellBalancing   = TRUE;
				}
				//ljb 20190114 add e 셀밸런스 값이 있으면 SBC로 내리는  Flag를 1로 변경 한다. 

				if(pStepData->m_nChiller_Cmd == 1) //yulee 20190114
				{
					bUseChiller = TRUE;
				}

				//SFT_STEP_CONDITION stepCon;	저장	
				fsize = rltData.SeekToEnd();
				
				if(rltData.m_hFile != NULL)
				{
					rltData.Write(&stepCon, sizeof(SFT_STEP_CONDITION_V2)); //lyj 20200214 LG v1015 미만
					rltData.Flush();
				}

				//ljb 20160427 waitTimeGoto write
				if (nWaitTimeGotoCnt > 0)
				{
// 					if (pStepData->m_nWaitTimeInit > 0 || pStepData->m_nWaitTimeDay > 0 
// 						|| pStepData->m_nWaitTimeHour > 0 || pStepData->m_nWaitTimeMin > 0  || pStepData->m_nWaitTimeSec > 0)
// 					{
						//SFT_STEP_CONDITION stepCon;	저장
						ZeroMemory(&stepConWaitTime, sizeof(SFT_STEP_CONDITION_WAIT_TIME));
						stepConWaitTime.nStepNo		= pStepData->m_StepIndex+1;
						stepConWaitTime.byTimieInit = pStepData->m_nWaitTimeInit;
						stepConWaitTime.nWaitDay	= pStepData->m_nWaitTimeDay;
						stepConWaitTime.nWaitHour	= pStepData->m_nWaitTimeHour;
						stepConWaitTime.nWaitMin	= pStepData->m_nWaitTimeMin;
						stepConWaitTime.nWaitSec	= pStepData->m_nWaitTimeSec;

						fsize2 = rltData2.SeekToEnd();
						
						if(rltData2.m_hFile != NULL)
						{
							rltData2.Write(&stepConWaitTime, sizeof(SFT_STEP_CONDITION_WAIT_TIME));
							rltData2.Flush();
						}
//					}
				}

			}

			rltData.Flush();
			rltData.Close();
			
			if (nWaitTimeGotoCnt > 0)
			{
				rltData2.Flush();
				rltData2.Close();
			}
			
			//strMsg.Format("%s 파일 생성 완료", strFileName);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg42","CTSMonPro_DOC"), strFileName);//&&
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
///////////////////////////////////////스케줄 시작 정보 저장 종료////////////////////////////////////////////////////////
		}
	}

	if(bUseChiller == TRUE) //yulee 20190114
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 1);
	}
	else
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 0);
	}

	//5. sch 용량, checksum
	if(pDoc->Fun_MemoryCopyFileAndCheckSum(strFileName, "", lFileSize, lCheckSum) == FALSE)
	{//$1013
		//strMsg.Format("sbc로 전송하는 schedule File Size(%d), checksum(%d) 실패!!!(%s)",lFileSize, lCheckSum, strFileName);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg43","CTSMonPro_DOC"),lFileSize, lCheckSum, strFileName);//&&
		AfxMessageBox(strMsg);
		pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}//$1013
	//strMsg.Format("sbc로 전송하는 schedule File Size(%d), checksum(%d) 성공!!!\n(%s)",lFileSize, lCheckSum, strFileName);
	strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg44","CTSMonPro_DOC"),lFileSize, lCheckSum, strFileName);//&&
	pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);

	//ljb 20160427 wait time goto sch 용량, checksum
	if (nWaitTimeGotoCnt > 0)
	{
		if(pDoc->Fun_MemoryCopyFileAndCheckSum(strFileName2, "", lFileSize2, lCheckSum2) == FALSE)
		{
			//strMsg.Format("sbc로 전송하는 wait time goto schedule File Size(%d), checksum(%d) 실패!!!(%s)",lFileSize2, lCheckSum2, strFileName2);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg45","CTSMonPro_DOC"),lFileSize2, lCheckSum2, strFileName2);//&&
			AfxMessageBox(strMsg);
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		//strMsg.Format("sbc로 전송하는 wait time goto schedule File Size(%d), checksum(%d) 성공!!!\n(%s)",lFileSize2, lCheckSum2, strFileName2);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg46","CTSMonPro_DOC"),lFileSize2, lCheckSum2, strFileName2);//&&
		pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_NORMAL);
	}

	//Fun_SaveFileSBCbinary(strFileName);	//ljb 20130509 임시로 binary 저장

	SFT_STEP_END_BODY steEnd;
	ZeroMemory(&steEnd, sizeof(SFT_STEP_END_BODY));
	steEnd.lTestCond_File_Size = lFileSize;
	steEnd.lTestCond_File_CheckSum = lCheckSum;
	steEnd.lWaitTimeGoto_File_Size = lFileSize2;	//ljb 20160427 add
	steEnd.lWaitTimeGoto_CheckSum = lCheckSum2;		//ljb 20160427 add


	//pProgressWnd->SetText(_T("스케줄을 전송중입니다."));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg47","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(80);


	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 FTP로 전송 시작 (Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg48","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);
	//6. FTP로 SBC용 스케쥴 파일 전송 (채널별)
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = pDoc->GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pChInfo)
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
			{
				//ksj 20171027 : 주석처리
				if (pChInfo->GetChannelIndex() < 1)
				{
					strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
				}
				else if (pChInfo->GetChannelIndex() == 1)
				{
					strChangeDir.Format("/START_INFO/CH%03d",3);	//ljb 20170629 add
				}
			}
//#else
else
			{
						//ksj 20171027 : 원복
				strChangeDir.Format("/START_INFO/CH%03d",pChInfo->GetChannelIndex()+1);
			}
//#endif
			if (pDoc->Fun_FtpChangeDir(strChangeDir) == TRUE)
			{
				strFileName = pChInfo->GetScheduleFileForSbc();
			
				//delete and write Directory point Fun_FtpPutFile() 
				if (pDoc->Fun_FtpPutFile(strFileName) == TRUE)
				{
					//7. FTP로 SBC용 Pattern File 업로드 (채널별) /////////////////////////////////////////////
					for (k=0; k<arryStrPatternFile.GetSize(); k++)		{
						strFileName = arryStrPatternFile.GetAt(k);
						if (strFileName.IsEmpty()) continue;
						if (pDoc->Fun_FtpPutFile(strFileName) == TRUE)	{

						}else	{
							strMsg.Format("Pattern File Upload fail (%s)",strFileName);
							pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
							AfxMessageBox(strMsg);
							return FALSE;
						}
					}
					////////////////////////////////////////////////////////////////////////////////////////////
				}else		{
					//strMsg.Format("sch 파일 업로드 실패.(%s)",strChangeDir);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg49","CTSMonPro_DOC"),strChangeDir);//&&
					pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
					AfxMessageBox(strMsg);
					return FALSE;
				}

				//ljb 20160427 waitTimeGoto write
				if (nWaitTimeGotoCnt > 0)
				{
					strFileName2 = pChInfo->GetScheduleFileForSbc2();	//ljb 20160427 add wait time goto sch upload
					if (pDoc->Fun_FtpPutFile(strFileName2) == TRUE)
					{//$1013
						//strTimeCheck.Format("Wait Time goto 스케쥴 FTP로 전송 시작 (Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
						strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg50","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
						pDoc->WriteSysLog(strTimeCheck);
					}
					else
					{
						strMsg.Format("wait time goto sch File Upload fail (%s)",strFileName2);
						pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
						AfxMessageBox(strMsg);
						return FALSE;
					}
				}
			}
		}
		else
		{//$1013
			//strMsg.Format("sch 업로드 pChInfo 이상.(ch : %d)",pChInfo->GetChannelIndex()+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg51","CTSMonPro_DOC"),pChInfo->GetChannelIndex()+1);//&&
			pDoc->WriteSysLog(strMsg, CT_LOG_LEVEL_DETAIL);
			AfxMessageBox(strMsg);
			return FALSE;			
		}

		nPos = 100 - (100/(ch+1));
		//pProgressWnd->SetText(_T("전송 중입니다."));//$1013
		pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg52","CTSMonPro_DOC"));//&&
		pProgressWnd->SetPos(nPos);
	}
	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 FTP로 전송 완료 (Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg53","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);

//			2013-09-13			bung			:			resend
 	pDoc->resteEnd		= steEnd;
 	pDoc->renModuleID		= nModuleID;

//			2013-11-28			bung			:			startChInfo != endChInfo   
	if(pDump_SelChArray != pSelChArray)	{
		pSelChArray = pDump_SelChArray;
	} 
//			2013-10-21			bung			:			Send_CMD_SCHEDULE_END 

	
	long lWaitTime = 2000; //[20191112] BY SKH 
	if(nPatternCount > 0)
		lWaitTime = nPatternCount*2000;

	strTimeCheck.Format("패턴 파일 개수 : %d, Wait Time : %d", nPatternCount, lWaitTime);
	pDoc->WriteSysLog(strTimeCheck);

	
 	pDoc->Send_CMD_SCHEDULE_END(pMD,pSelChArray,steEnd,nModuleID, lWaitTime);

	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 전송 종료 (SendScheduleToModuleFTP :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP_msg54","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);

	return TRUE;
}

BOOL CSendToFTP_v1014_v1_SCH::SendSchFTP2_v1014_v1_SCH(UINT nMaxModuleCnt, UINT nModuleID, CWordArray *pSelChArray, CScheduleData *pSchData, CProgressWnd *pProgressWnd, CCTSMonProDoc* pDoc)
{
	//pProgressWnd->SetText(_T("스케줄 가공중 "));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg1","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(30);

	CStringArray	arryStrPatternFile;
	CString	strTemp,strMsg,strFileName,strFileName2,strDsc;
	short int	nWaitTimeGotoCnt = 0;	//ljb 20160427 add

	strFileName2.Empty();

	CCyclerModule *pMD;
	CCyclerChannel *pChInfo;
	CStep *pStepData;

	int nRtn = 0;
	int ch = 0;
	int nPatternCount = 0; //스케쥴에 있는 패턴 STEP 갯수
	arryStrPatternFile.RemoveAll();		//SBC용 스케쥴 파일에 저장할 Pattern 파일 이름

	UINT k;
	CUIntArray arryTimeType;
	CUIntArray arryModeType;
	CUIntArray arryPlusMode;
	CUIntArray arryMaxValue;
	
	CUIntArray arryFileSize;
	CUIntArray arryCheckSum;
	
	long lTimeType, lModeType, lPulsMode,lMaxValue;
	ULONG lFileSize=0, lCheckSum=0;
	ULONG lFileSize2=0, lCheckSum2=0;
	float fMaxValue=0;
	int nPos = 0;
	
	pMD		=	pDoc->GetModuleInfo(nModuleID);

	//현재 시간 구함 
	CString strTimeCheck;
	CTime tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 전송 시작 (SendScheduleToModuleFTP :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg2","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);

	//0. FTP 접속
	CString strIP = pMD->GetIPAddress();
	if(strIP.IsEmpty())		{
		//AfxMessageBox("모듈 IP 이상");//$1013
		AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg3","CTSMonPro_DOC"));//&&
		return FALSE;
	}
	if (pDoc->Fun_FtpConnect(strIP) == FALSE)		{
		if (pDoc->Fun_FtpConnect(strIP) == FALSE)		{
			//AfxMessageBox("FTP 접속 실패 !!!");//$1013
			AfxMessageBox(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg4","CTSMonPro_DOC"));//&&
			return FALSE;
		}
	}

	//pProgressWnd->SetText(_T("접속 완료"));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg5","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(40);

	//strMsg.Format(_T("접속 완료."));//$1013
	strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg6","CTSMonPro_DOC"));//&&

	SFT_STEP_START_INFO stepInfo;
	ZeroMemory(&stepInfo, sizeof(SFT_STEP_START_INFO));	

	//+2015.9.23 USY Add For PatternCV
	UINT nParallelCnt = 0;
	//-

	//1. FTP 폴더에 run.txt 파일 있는지 체크
	//2. FTP 폴더내 모든 파일 삭제, 없으면 생성
	CString strChangeDir;
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = pDoc->GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pChInfo)
		{
			/*if (pChInfo->GetChannelIndex() == 0)
				strChangeDir.Format("/START_INFO/UPDATE/CH%03d",pChInfo->GetChannelIndex()+1);
			else
				strChangeDir.Format("/START_INFO/UPDATE/CH%03d",3);	//ljb 201706 add*/

//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
			{
				if((pChInfo->GetChannelIndex()+1) == 2)
				{
					strChangeDir.Format("/START_INFO/CH%03d/UPDATE", 3);
				}
				else
				{
					strChangeDir.Format("/START_INFO/CH%03d/UPDATE",pChInfo->GetChannelIndex()+1);
				}

			}
//#else
else
			{
						//ksj 20171027 : 원복				
				strChangeDir.Format("/START_INFO/CH%03d/UPDATE",pChInfo->GetChannelIndex()+1);
			}
//#endif
			if (pDoc->Fun_FtpChangeDir(strChangeDir) == TRUE)
			{
// 				if (pDoc->Fun_FtpFileFind("run.txt") == TRUE)
// 				{
// 					strMsg.Format("%s SBC에서 작업 중인 채널 입니다.(ch : %d)",GetModuleName(nModuleID), pChInfo->GetChannelIndex()+1);
// 					pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
// 					AfxMessageBox(strMsg);
// 					return FALSE;			
// 				}
// 				else
// 				{
					if (pDoc->Fun_FtpFileDeleteAll(strChangeDir) == FALSE)
					{//$1013
						//strMsg.Format("%s 에서 파일 삭제 에러.(%s)",GetModuleName(nModuleID),strChangeDir);
						strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg7","CTSMonPro_DOC"),pDoc->GetModuleName(nModuleID),strChangeDir);//&&
						pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
						AfxMessageBox(strMsg);
					}
//				}
			}
			//+2015.9.23 USY Add For PatternCV
			if(pChInfo->IsParallel()) nParallelCnt++;
			//-
		}
		else
		{
			//strMsg.Format("pChInfo 이상.(ch : %d)",pChInfo->GetChannelIndex()+1);//$1013
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg8","CTSMonPro_DOC"),pChInfo->GetChannelIndex()+1);//&&
			pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			AfxMessageBox(strMsg);
			return FALSE;			
		}
	}
	//pProgressWnd->SetText(_T("패턴의 유무를 확인중입니다."));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg9","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(60);

	//3. 전송할 스케쥴 확인 (Pattern 확인)
	if(pSchData->GetStepSize() <= 0)
	{
		//strMsg.Format("%s 에 전송할 스케줄이 없습니다.!!!", GetModuleName(nModuleID));//$1013
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg10","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID));//&&
		pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}
	//+2015.9.23 USY Add For PatternCV
	float fMaxVoltage = 0.0f;
	float fMaxCurrent = 0.0f;

	
	CString rng, str;
	fMaxVoltage = pMD->GetVoltageSpec()/1000.0f;
	for(int r=0; r<pMD->GetCurrentRangeCount(); r++)
	{
		if((pMD->GetCurrentSpec(r)/1000.0f) > fMaxCurrent)
		{
			fMaxCurrent = pMD->GetCurrentSpec(r)/1000.0f;
		}
	}
	//-

	for(k =0; k<pSchData->GetStepSize(); k++)
	{
		pStepData = pSchData->GetStepData(k);
		if(pStepData == NULL)
		{//$1013
			//strMsg.Format("%s 에 전송할 스케줄 STEP %d 가 NULL 입니다.!!!", GetModuleName(nModuleID),k+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg11","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID),k+1);//&&
			pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		
		////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 시작 ////////////////
		lTimeType = lModeType = lPulsMode = lMaxValue = 0;
		lFileSize = lCheckSum = 0;
		strFileName ="";

		//WaitTimeGotoStep count 확인
		if (pStepData->m_nWaitTimeInit > 0 || pStepData->m_nWaitTimeDay > 0 
			|| pStepData->m_nWaitTimeHour > 0 || pStepData->m_nWaitTimeMin > 0  || pStepData->m_nWaitTimeSec > 0)
		{
			nWaitTimeGotoCnt++;
		}

		//2014.09.11 PS_STEP_USER_MAP 추가.
		if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
		{
//			2013-06-12			bung			:			wait_FTP_endTime_plusPtrn
			pDoc->m_wait_FTP_endTime_plusPtrn += 10;
	
			strFileName = pStepData->m_strPatternFileName;
			nPatternCount++;

			//+2015.9.23 USY Add For PatternCV	

			if(pDoc->CheckPatternData(strFileName, fMaxVoltage, fMaxCurrent, nParallelCnt) == FALSE)
			{
				//strMsg.Format("%s Pattern File 오류", strFileName);//$1013
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg12","CTSMonPro_DOC"), strFileName);//&&
				pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			//-

			if (pStepData->m_fValueMax != 0) lMaxValue = pStepData->m_fValueMax / 1000;		//ljb 20130509 add
			else lMaxValue = pStepData->m_fValueMax;										//ljb 20130509 add

			//ljb 20130514 메모리로 읽고 CheckSum 후 strDsc로 파일 복사 생성 한다.
			if(pStepData->m_type == PS_STEP_PATTERN){
				strDsc.Format("%s\\Pattern\\sbc_pattern_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}else if(pStepData->m_type == PS_STEP_USER_MAP){ //2014.09.11 PS_STEP_USER_MAP 추가.
				strDsc.Format("%s\\Pattern\\sbc_usermap_data_step_%05d.csv", pChInfo->GetTestPath(),k+1);
			}


			if(pDoc->Fun_MemoryCopyFileAndCheckSum(strFileName, strDsc, lFileSize, lCheckSum) == FALSE)
			{
				//strMsg.Format("%s 에 Simulation File Size, checksum 실패!!! %s (File size = %d, CheckSum = %d", GetModuleName(nModuleID), m_strLastErrorString,lFileSize,lCheckSum);
				strMsg.Format(Fun_FindMsg("strMsgCTSMonProDoc_SendScheduleToModuleFTP2_msg13","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);//&&
				AfxMessageBox(strMsg);
				pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			Sleep(100);	//201305 슬립이 있어야 CSV 파일이 안 깨짐
			//strMsg.Format("%s 에 Simulation File Size, checksum 성공 !!! %s (File size = %d, CheckSum = %d", GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg14","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), pDoc->m_strLastErrorString,lFileSize,lCheckSum);//&&
			pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);

			//TRACE("STEP %d (Pattern Data check 완료)\r\n", pStepData->m_StepIndex);	//$1013			
			TRACE(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg53","CTSMonPro_DOC"), pStepData->m_StepIndex);	//$1013			//&&
		////////////////////////////////////////////// 패턴 데이터 확인 및 시간타입, 전류-파워 체크, MAX 값 가져 오기 끝 ////////////////
			arryStrPatternFile.Add(strDsc);
		}
		else
		{
			arryStrPatternFile.Add(strFileName);
		}
		arryTimeType.Add(lTimeType);
		arryModeType.Add(lModeType);
		arryPlusMode.Add(lPulsMode);
		arryMaxValue.Add(lMaxValue);

		arryFileSize.Add(lFileSize);
		arryCheckSum.Add(lCheckSum);
	}

	stepInfo.byTotalStep = pSchData->GetStepSize();
	stepInfo.nPatternStepCount = nPatternCount;
	stepInfo.bWaitTimeGotoCount = nWaitTimeGotoCnt;		//ljb 20160427 add

	//pProgressWnd->SetText(_T("스케줄 전송 준비 완료"));//$1013
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg15","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(70);

	strMsg.Format(" SFT_CMD_SCHEDULE_START :: (1020) before sendcmmand", pDoc->GetModuleName(nModuleID), nRtn);
	pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
 	CWordArray *pDump_SelChArray;
	pDump_SelChArray = pSelChArray;

	// 스케쥴 시작
	if((nRtn = pMD->SendCommand(SFT_CMD_TESTCOND_UPDATE_START, pSelChArray, &stepInfo, sizeof(SFT_STEP_START_INFO))) != SFT_ACK)	//ljb 20170729 add
	{//$1013
		//strMsg.Format("%s 에 스케쥴 업데이트 전송 준비 실패!!!(Code %d)", GetModuleName(nModuleID), nRtn);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg16","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), nRtn);//&&
		pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}
	else
	{//$1013
		//strMsg.Format("%s 에 스케쥴 업데이트 전송 준비 성공", GetModuleName(nModuleID));
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg17","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID));//&&
		pDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	}

	strMsg.Format(" SFT_CMD_TESTCOND_UPDATE_START :: (1020) after sendcmmand", pDoc->GetModuleName(nModuleID), nRtn);
	pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);

	SFT_STEP_CONDITION_V2 stepCon; //lyj 20200214 LG v1015 미만
	SFT_STEP_CONDITION_WAIT_TIME stepConWaitTime;	//ljb 20160427 add
	
	BOOL bUseChiller = FALSE;
	//4. FTP 전송용 스케쥴 파일 생성
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = pDoc->GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
 		if(pChInfo)
		{
			strFileName = pChInfo->GetScheduleFileForSbc();
			::DeleteFile(strFileName);		//ljb 20170801 임시 삭제
						
			//2014.12.10 간이 충방전일경우 저장시간을 설정안할경우. 임시폴더를 생성하고 임시 스케줄 파일을 만들어 공정을 돌린다.
			if(pDoc->m_bSimpleTest){
				CString sPath;
				sPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC,"Data Path","C:\\");
				//폴더를 생성한다.				
				strFileName.Format("%s\\simpleTest\\M%02dCh%02d[%03d]\\StepStart",sPath,nModuleID,pChInfo->GetChannelIndex() + 1,1);		//ljb 20150508 add
				pDoc->ForceDirectory(strFileName);
				strFileName.Format("%s\\simpleTest\\M%02dCh%02d[%03d]",sPath,nModuleID,pChInfo->GetChannelIndex() + 1,1);
				pChInfo->SetFilePath(strFileName);
				strFileName.Format("%s\\StepStart\\sbc_schedule_info_update.sch",strFileName);
				DeleteFile(strFileName);
			}
			
			//strMsg.Format("%s 파일 생성 시작", strFileName);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg54","CTSMonPro_DOC"), strFileName);//&&
			pDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.21
			//if(FALSE == pSchData->SaveToFile(strFileName, FALSE))
			if(FALSE == pSchData->SaveToFile_ForFTP(strFileName, FALSE))		//sbc로 전송할 sbc_schedule.sch file 생성 (0k byte) 	
			{//$1013
				//strMsg.Format("%s 에 %d 채널 스케쥴 파일 생성 실패.!!!", GetModuleName(nModuleID), ch+1);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg18","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
				pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			// -
			if (nWaitTimeGotoCnt > 0)
			{
				strFileName2 = pChInfo->GetScheduleFileForSbc2();					//20160427 sbc_schedule_info_wait_goto.sch
				::DeleteFile(strFileName2);		//ljb 20170801 임시 삭제
				if(FALSE == pSchData->SaveToFile_ForFTP(strFileName2, FALSE))		//20160427 sbc로 전송할 sbc_schedule_info_wait_goto.sch file 생성 (0k byte) 	
				{
					//strMsg.Format("%s 에 %d 채널 WaitGoto 스케쥴 파일 생성 실패.!!!", GetModuleName(nModuleID), ch+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg19","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
					pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					return FALSE;
				}
			}
			//////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////스케줄 시작 정보 저장 시작 ////////////////////////////////////////////////////////
			//file에 추가
			CFile rltData;
			CFileException e;
			long fsize = 0;
			
			CFile rltData2;
			CFileException e2;
			long fsize2 = 0;
			//////////////////////////////////////////////////////////////////////////
			// + BW KIM 2014.02.21
			//if(!rltData.Open(strFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			if(!rltData.Open(strFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &e))
			// - 
			//////////////////////////////////////////////////////////////////////////
			{//$1013
				//strMsg.Format("%s 에 %d 채널 스케쥴 파일 추가 실패.!!!", GetModuleName(nModuleID), ch+1);
				strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg20","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
				pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
				return FALSE;
			}
			fsize = rltData.SeekToEnd();
			rltData.Seek(0, CFile::end);

			if(rltData.m_hFile != NULL)
			{
				rltData.Write(&stepInfo, sizeof(SFT_STEP_START_INFO));
				rltData.Flush();
			}

			//ljb 20160427 waitTimeGoto write
			if (nWaitTimeGotoCnt > 0)
			{
				if(!rltData2.Open(strFileName2, CFile::modeReadWrite|CFile::shareDenyWrite, &e2))
					// - 
					//////////////////////////////////////////////////////////////////////////
				{//$1013
					//strMsg.Format("%s 에 %d 채널 스케쥴 파일 추가 실패.!!!", GetModuleName(nModuleID), ch+1);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg21","CTSMonPro_DOC"), pDoc->GetModuleName(nModuleID), ch+1);//&&
					pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
					return FALSE;
				}
				fsize2 = rltData2.SeekToEnd();
				rltData2.Seek(0, CFile::end);
				
				if(rltData2.m_hFile != NULL)
				{
					rltData2.Write(&stepInfo, sizeof(SFT_STEP_START_INFO));
					rltData2.Flush();
				}
			}
			///////////////////////////////////////안전조건 저장 시작 ////////////////////////////////////////////////////////
			SFT_TEST_SAFETY_SET_V1 safetyInfo;
			ZeroMemory(&safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1));
			
			CELL_CHECK_DATA* pCheckData = pSchData->GetCellCheckParam();
			//Voltage Limit
			safetyInfo.lVtgHigh	=	FLOAT2LONG(pCheckData->fMaxV);
			safetyInfo.lVtgLow	=	FLOAT2LONG(pCheckData->fMinV);
			safetyInfo.lVtgCellDelta	= FLOAT2LONG(pCheckData->fCellDeltaV);		//ljb 20150730
			
			//yulee 20181114 Protocol 1013
			float fltCellVolt = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHigh", 4600);
			float fltFixTempHigh = (float)AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHigh", 80);
			safetyInfo.lVtgCellHigh = FLOAT2LONG(fltCellVolt);
			safetyInfo.lTempHigh	= FLOAT2LONG(fltFixTempHigh);

			//Current Limit
			safetyInfo.lCrtHigh	= FLOAT2LONG(pCheckData->fMaxI);
			
			//Temperature limit
			//safetyInfo.lTempHigh = pCheckData->fMaxT;
			safetyInfo.lTempLow = pCheckData->fMinT;
			
			//Capacity Limit
			safetyInfo.lCapHigh	= FLOAT2LONG(pCheckData->fMaxC);			//Vref
			safetyInfo.lWattHigh	= pCheckData->lMaxW;			//Vref
			safetyInfo.lWattHourHigh = pCheckData->lMaxWh;			//Vref
			
			//ljb 20100820
			for (int i=0; i < _SFT_MAX_CAN_AUX_COMPARE_STEP; i++)
			{
				safetyInfo.can_function_division[i] = pCheckData->ican_function_division[i];
				safetyInfo.can_compare_type[i] = pCheckData->cCan_compare_type[i];
				safetyInfo.can_data_type[i] = pCheckData->cCan_data_type[i];
				safetyInfo.fcan_Value[i] = pCheckData->fcan_Value[i];
				safetyInfo.aux_function_division[i] = pCheckData->iaux_function_division[i];
				safetyInfo.aux_compare_type[i] = pCheckData->cAux_compare_type[i];
				safetyInfo.aux_data_type[i] = pCheckData->cAux_data_type[i];
				safetyInfo.faux_Value[i] = pCheckData->faux_Value[i];
			}

			if(rltData.m_hFile != NULL)
			{
				rltData.Write(&safetyInfo, sizeof(SFT_TEST_SAFETY_SET_V1));
				rltData.Flush();
			}

			///////////////////////////////////////스텝 데이터 저장 시작 ////////////////////////////////////////////////////////
			CStep *pStepData;

			//2. Schedule Step Data Send command
			for(k =0; k<pSchData->GetStepSize(); k++)
			{
				pStepData = pSchData->GetStepData(k);
				if(pStepData == NULL)	break;
			
				if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)	{
					//+2015.9.22 USY Mod For PatternCV(김재호K)
					if((pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge <= 0)&& (pStepData->m_mode !=PS_MODE_CV))
					{
						//pDoc->m_strLastErrorString.Format("Step %d의 충전CV전압이나 방전CV전압이 설정되지 않았습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg22","CTSMonPro_DOC"), k+1);//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
// 					if(pStepData->m_fVref_Charge <= 0 || pStepData->m_fVref_DisCharge <= 0)		{
// 						pDoc->m_strLastErrorString.Format("Step %d의 충전CV전압이나 방전CV전압이 설정되지 않았습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 						AfxMessageBox(pDoc->m_strLastErrorString);
// 						return FALSE;
// 					}
					//-
					else	{
						if(pStepData->m_fEndV_H > 0.0f && pStepData->m_fVref_Charge <= pStepData->m_fEndV_H)	{
							//pDoc->m_strLastErrorString.Format("Step %d의 충전전압 설정값이 종료전압H보다 낮습니다.  스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg23","CTSMonPro_DOC"), k+1);//&&
							AfxMessageBox(pDoc->m_strLastErrorString);
							return FALSE;
						}
						if(pStepData->m_fEndV_L > 0.0f && pStepData->m_fVref_DisCharge >= pStepData->m_fEndV_L)
						{//$1013
							//pDoc->m_strLastErrorString.Format("Step %d의 방전전압 설정값이 종료전압L보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg24","CTSMonPro_DOC"), k+1);//&&
							AfxMessageBox(pDoc->m_strLastErrorString);
							return FALSE;
						}
					}			
				}

				if(pStepData->m_type == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)	
				{
					if(strlen(pStepData->m_strPatternFileName) <= 0)	
					{
						//pDoc->m_strLastErrorString.Format("Step %d의 Simulation data file이 지정되지 않았습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg25","CTSMonPro_DOC"), k+1);//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}

					if( pStepData->m_fEndDI <= 0 && pStepData->m_fEndDI <= 0 && pStepData->m_fEndW <= 0 &&
						pStepData->m_fEndDV <=0 && pStepData->m_fEndI <=0 && pStepData->m_fEndW <= 0 &&
						pStepData->m_fEndV_H <=0 && pStepData->m_fEndV_L <=0 && pStepData->m_fEndTime == 0)			
					{//$1013
						//pDoc->m_strLastErrorString.Format("Step %d의 종료 조건이 설정되어 있지 않았습니다.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg26","CTSMonPro_DOC"), k+1);			//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
				}

				if(pStepData->m_type == PS_STEP_CHARGE)		{
					if(pStepData->m_fVref_Charge <= 0.0f)		{//$1013
						//pDoc->m_strLastErrorString.Format("Step %d의 전압값이 설정 되지 않았거나 범위를 벗어났습니다.	(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg27","CTSMonPro_DOC"), k+1);			//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}//$1013
					if(pStepData->m_fVref_Charge <= pStepData->m_fEndDV)		{
						//pDoc->m_strLastErrorString.Format("Step %d의 전압변화 설정값이 너무 큽니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg28","CTSMonPro_DOC"), k+1);			//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;				
					}
					
					if(pStepData->m_fVref_Charge <= pStepData->m_fEndV_H)			{//$1013
						//pDoc->m_strLastErrorString.Format("Step %d의 종료 전압이 설정 전압보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg29","CTSMonPro_DOC"), k+1);			//&&
						AfxMessageBox(pDoc->m_strLastErrorString);
						return FALSE;
					}
				} //charge
					
				if(pStepData->m_type == PS_STEP_DISCHARGE)			{
					if(pStepData->m_fVref_DisCharge > 0 && pStepData->m_fEndV_L > 0)		{
						if(pStepData->m_fVref_DisCharge >= pStepData->m_fEndV_L)				{//$1013
							//pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 종료 전압보다 높습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
							pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg30","CTSMonPro_DOC"), k+1);//&&
							AfxMessageBox(pDoc->m_strLastErrorString);
							return FALSE;
						}
					}
				} //Discharge

				if(pStepData->m_fTref == 0.0f && (pStepData->m_fStartT !=0.0f || pStepData->m_fEndT != 0.0f))		{
					//pDoc->m_strLastErrorString.Format("Step %d의 온도 설정값을 입력하지 않고 시작온도나 종료온도가 설정되었습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);			
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg30","CTSMonPro_DOC"), k+1);			//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}
					
				//+2015.9.22 USY Mod For PatternCV(김재호K)
				if((pStepData->m_mode != PS_MODE_CV) && (pStepData->m_type != PS_STEP_PATTERN))
				{
					if(  pStepData->m_fVref_Charge < 0.0f)		{
						//pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg31","CTSMonPro_DOC"), k+1);//&&
						AfxMessageBox(pDoc->m_strLastErrorString);//$1013
						return FALSE;
					}
					if(  pStepData->m_fVref_DisCharge < 0.0f)		{
						//pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
						pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg32","CTSMonPro_DOC"), k+1);//&&
						AfxMessageBox(pDoc->m_strLastErrorString);//$1013
						return FALSE;
					}
				}
				//전압 설정값 입력 범위 검사
// 				if(  pStepData->m_fVref_Charge < 0.0f)		{
// 					pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
// 					return FALSE;
// 				}
// 				if(  pStepData->m_fVref_DisCharge < 0.0f)		{
// 					pDoc->m_strLastErrorString.Format("Step %d의 전압 설정값이 범위를 벗어 났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
// 					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
// 					return FALSE;
// 				}
				//-
				//전압 종료값 입력 범위 검사
				if(pStepData->m_fEndV_H < 0.0f)					{
					//pDoc->m_strLastErrorString.Format("Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg33","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}
				//전압 종료값 입력 범위 검사
				if(pStepData->m_fEndV_L < 0.0f)					{
					//pDoc->m_strLastErrorString.Format("Step %d의 종료 전압값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg34","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}
				//전류 종료값 입력 범위 검사
				if(pStepData->m_fEndI < 0.0f)					{
					//pDoc->m_strLastErrorString.Format("Step %d의 종료 전류값이 입력 범위를 벗어났습니다.(범위:0mA) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg35","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}

				//전압 제한값 입력 범위 검사
				if(pStepData->m_fLowLimitV < 0)				{
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 전압 상하한값이 입력 범위를 벗어났습니다.(범위:0mV) 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg36","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}
				
				//전압 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitV > 0.0f && pStepData->m_fLowLimitV > 0.0f && pStepData->m_fHighLimitV < pStepData->m_fLowLimitV)			{
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 전압 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg37","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}

				//전류 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitI > 0 && pStepData->m_fLowLimitI > 0 && pStepData->m_fHighLimitI < pStepData->m_fLowLimitI)				{
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 전류 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg38","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}

				//온도 상한 값이 하한값보다 커야 한다.
				if(pStepData->m_fHighLimitTemp > 0 && pStepData->m_fLowLimitTemp > 0 && pStepData->m_fHighLimitTemp < pStepData->m_fLowLimitTemp)						{
					//pDoc->m_strLastErrorString.Format("Step %d의 안전 온도 상한값이 하한값보다 작습니다. 스케줄 파일을 에디터 프로그램에서 확인하세요.", k+1);
					pDoc->m_strLastErrorString.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg39","CTSMonPro_DOC"), k+1);//&&
					AfxMessageBox(pDoc->m_strLastErrorString);//$1013
					return FALSE;
				}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				

				stepCon = pDoc->GetNetworkFormatStep(pStepData);

				//stepCon_v7 = GetNetworkFormatStep_v7(pStepData);
				//2014.09.11 PS_STEP_USER_MAP 추가.
				if (stepCon.stepHeader.nStepTypeID == PS_STEP_PATTERN || pStepData->m_type == PS_STEP_USER_MAP)
				{
					stepCon.bPatternTime = arryTimeType.GetAt(k);
					//stepCon.bPatternType = arryModeType.GetAt(k);
					//stepCon.bPatternMode = arryPlusMode.GetAt(k);
					lMaxValue = arryMaxValue.GetAt(k);

					stepCon.fPatternMaxValue = lMaxValue;
					stepCon.lPatternFileSize = arryFileSize.GetAt(k);		//v1007
					stepCon.lPatternChecksum = arryCheckSum.GetAt(k);		//v1007
				}
				
				stepCon.bCanRxEndNoCheck	= pStepData->m_nNoCheckMode;	//20150825 CAN RX 안전조건, 종료조건 무시	(2014.12.08 이민규 대리 추가)
				stepCon.bCanTxOffMode		= pStepData->m_nCanTxOffMode;	//20150825 CAN TX 안함						(2014.12.08 이민규 대리 추가)
				stepCon.bStepCanCheckMode	= pStepData->m_nCanCheckMode;	//ljb 20150825 add

				//yulee 20190114
				//ljb 20190114 add s 셀밸런스 값이 있으면 SBC로 내리는  Flag를 1로 변경 한다. 
				if (pStepData->m_nCellBal_CircuitRes > 0)
				{
					stepCon.bUseCellBalancing   = TRUE;
				}
				//ljb 20190114 add e 셀밸런스 값이 있으면 SBC로 내리는  Flag를 1로 변경 한다. 

				if(pStepData->m_nChiller_Cmd == 1) //yulee 20190114
				{
					bUseChiller = TRUE;
				}

				//SFT_STEP_CONDITION stepCon;	저장	
				fsize = rltData.SeekToEnd();
				
				if(rltData.m_hFile != NULL)
				{
					rltData.Write(&stepCon, sizeof(SFT_STEP_CONDITION_V2)); //lyj 20200214 LG v1015 미만
					rltData.Flush();
				}

				//ljb 20160427 waitTimeGoto write
				if (nWaitTimeGotoCnt > 0)
				{
// 					if (pStepData->m_nWaitTimeInit > 0 || pStepData->m_nWaitTimeDay > 0 
// 						|| pStepData->m_nWaitTimeHour > 0 || pStepData->m_nWaitTimeMin > 0  || pStepData->m_nWaitTimeSec > 0)
// 					{
						//SFT_STEP_CONDITION stepCon;	저장
						ZeroMemory(&stepConWaitTime, sizeof(SFT_STEP_CONDITION_WAIT_TIME));
						stepConWaitTime.nStepNo		= pStepData->m_StepIndex+1;
						stepConWaitTime.byTimieInit = pStepData->m_nWaitTimeInit;
						stepConWaitTime.nWaitDay	= pStepData->m_nWaitTimeDay;
						stepConWaitTime.nWaitHour	= pStepData->m_nWaitTimeHour;
						stepConWaitTime.nWaitMin	= pStepData->m_nWaitTimeMin;
						stepConWaitTime.nWaitSec	= pStepData->m_nWaitTimeSec;

						fsize2 = rltData2.SeekToEnd();
						
						if(rltData2.m_hFile != NULL)
						{
							rltData2.Write(&stepConWaitTime, sizeof(SFT_STEP_CONDITION_WAIT_TIME));
							rltData2.Flush();
						}
//					}
				}

			}

			rltData.Flush();
			rltData.Close();
			
			if (nWaitTimeGotoCnt > 0)
			{
				rltData2.Flush();
				rltData2.Close();
			}
			
			//strMsg.Format("%s 파일 생성 완료", strFileName);//$1013
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg40","CTSMonPro_DOC"), strFileName);//&&
			pDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
		}
	}

	if(bUseChiller == TRUE) //yulee 20190114
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 1);
	}
	else
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Chiller", 0);
	}

	//5. sch 용량, checksum
	if(pDoc->Fun_MemoryCopyFileAndCheckSum(strFileName, "", lFileSize, lCheckSum) == FALSE)
	{//$1013
		//strMsg.Format("sbc로 전송하는 schedule File Size(%d), checksum(%d) 실패!!!(%s)",lFileSize, lCheckSum, strFileName);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg41","CTSMonPro_DOC"),lFileSize, lCheckSum, strFileName);//&&
		AfxMessageBox(strMsg);
		pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		return FALSE;
	}//$1013
	//strMsg.Format("sbc로 전송하는 schedule File Size(%d), checksum(%d) 성공!!!\n(%s)",lFileSize, lCheckSum, strFileName);
	strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg42","CTSMonPro_DOC"),lFileSize, lCheckSum, strFileName);//&&
	pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);

	//ljb 20160427 wait time goto sch 용량, checksum
	if (nWaitTimeGotoCnt > 0)
	{
		if(pDoc->Fun_MemoryCopyFileAndCheckSum(strFileName2, "", lFileSize2, lCheckSum2) == FALSE)
		{
			//strMsg.Format("sbc로 전송하는 wait time goto schedule File Size(%d), checksum(%d) 실패!!!(%s)",lFileSize2, lCheckSum2, strFileName2);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg43","CTSMonPro_DOC"),lFileSize2, lCheckSum2, strFileName2);//&&
			AfxMessageBox(strMsg);
			pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
			return FALSE;
		}
		//strMsg.Format("sbc로 전송하는 wait time goto schedule File Size(%d), checksum(%d) 성공!!!\n(%s)",lFileSize2, lCheckSum2, strFileName2);
		strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg44","CTSMonPro_DOC"),lFileSize2, lCheckSum2, strFileName2);//&&
		pDoc->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
	}

	//pDoc->Fun_SaveFileSBCbinary(strFileName);	//ljb 20130509 임시로 binary 저장

	SFT_STEP_END_BODY steEnd;
	ZeroMemory(&steEnd, sizeof(SFT_STEP_END_BODY));
	steEnd.lTestCond_File_Size = lFileSize;
	steEnd.lTestCond_File_CheckSum = lCheckSum;
	steEnd.lWaitTimeGoto_File_Size = lFileSize2;	//ljb 20160427 add
	steEnd.lWaitTimeGoto_CheckSum = lCheckSum2;		//ljb 20160427 add


	//pProgressWnd->SetText(_T("스케줄을 전송중입니다."));
	pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg45","CTSMonPro_DOC"));//&&
	pProgressWnd->SetPos(80);


	tm = CTime::GetCurrentTime();
	//strTimeCheck.Format("스케쥴 FTP로 전송 시작 (pDoc->Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg46","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);
	//6. FTP로 SBC용 스케쥴 파일 전송 (채널별)
	for(ch =0; ch < pSelChArray->GetSize(); ch++)
	{
		pChInfo = pDoc->GetChannelInfo(nModuleID, pSelChArray->GetAt(ch));
		if(pChInfo)
		{
//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
if(g_AppInfo.iPType==1)
			{
				//ksj 20171027 : 주석처리
				if((pChInfo->GetChannelIndex()+1) == 2)
				{
					strChangeDir.Format("/START_INFO/CH%03d/UPDATE", 3);
				}
				else
				{
					strChangeDir.Format("/START_INFO/CH%03d/UPDATE",pChInfo->GetChannelIndex()+1);
				}
			}
//#else
else
			{
				//ksj 20171027 : 원복
				strChangeDir.Format("/START_INFO/CH%03d/UPDATE",pChInfo->GetChannelIndex()+1);
			}//------------------------------------------------------------------------
//#endif			
			pDoc->Fun_FtpFileDeleteAll(strChangeDir);		//ljb 20170801 기존 스케쥴 파일 삭제
			
			if (pDoc->Fun_FtpChangeDir(strChangeDir) == TRUE)
			{
				strFileName = pChInfo->GetScheduleFileForSbc();
			
				//delete and write Directory point pDoc->Fun_FtpPutFile() 
				if (pDoc->Fun_FtpPutFile(strFileName) == TRUE)
				{
					//7. FTP로 SBC용 Pattern File 업로드 (채널별) /////////////////////////////////////////////
					for (k=0; k<arryStrPatternFile.GetSize(); k++)		{
						strFileName = arryStrPatternFile.GetAt(k);
						if (strFileName.IsEmpty()) continue;
						if (pDoc->Fun_FtpPutFile(strFileName) == TRUE)	{

						}else	{
							strMsg.Format("Pattern File Upload fail (%s)",strFileName);
							pDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
							AfxMessageBox(strMsg);
							return FALSE;
						}
					}
					////////////////////////////////////////////////////////////////////////////////////////////
				}else		{//$1013
					//strMsg.Format("sch 파일 업로드 실패.(%s)",strChangeDir);
					strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg47","CTSMonPro_DOC"),strChangeDir);//&&
					pDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
					AfxMessageBox(strMsg);
					return FALSE;
				}

				//ljb 20160427 waitTimeGoto write
				if (nWaitTimeGotoCnt > 0)
				{
					strFileName2 = pChInfo->GetScheduleFileForSbc2();	//ljb 20160427 add wait time goto sch upload
					if (pDoc->Fun_FtpPutFile(strFileName2) == TRUE)
					{//$1013
						//strTimeCheck.Format("Wait Time goto 스케쥴 FTP로 전송 시작 (pDoc->Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
						strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg48","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
						pDoc->WriteSysLog(strTimeCheck);
					}
					else
					{
						strMsg.Format("wait time goto sch File Upload fail (%s)",strFileName2);
						pDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
						AfxMessageBox(strMsg);
						return FALSE;
					}
				}
			}
		}
		else
		{//$1013
			//strMsg.Format("sch 업로드 pChInfo 이상.(ch : %d)",pChInfo->GetChannelIndex()+1);
			strMsg.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg49","CTSMonPro_DOC"),pChInfo->GetChannelIndex()+1);//&&
			pDoc->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
			AfxMessageBox(strMsg);
			return FALSE;			
		}

		nPos = 100 - (100/(ch+1));
		//pProgressWnd->SetText(_T("전송 중입니다."));//$1013
		pProgressWnd->SetText(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg50","CTSMonPro_DOC"));//&&
		pProgressWnd->SetPos(nPos);
	}
	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 FTP로 전송 완료 (pDoc->Fun_FtpPutFile :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg51","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);

 	pDoc->resteEnd		= steEnd;
 	pDoc->renModuleID		= nModuleID;

	if(pDump_SelChArray != pSelChArray)	{
		pSelChArray = pDump_SelChArray;
	} 
 	pDoc->Send_CMD_SCHEDULE_END2(pMD,pSelChArray,steEnd,nModuleID);

	tm = CTime::GetCurrentTime();//$1013
	//strTimeCheck.Format("스케쥴 업데이트 전송 종료 (SendScheduleToModuleFTP :: %s)", tm.Format("%Y/%m/%d %H:%M:%S"));
	strTimeCheck.Format(Fun_FindMsg("CTSMonProDoc_SendScheduleToModuleFTP2_msg52","CTSMonPro_DOC"), tm.Format("%Y/%m/%d %H:%M:%S"));//&&
	pDoc->WriteSysLog(strTimeCheck);
	return TRUE;
}