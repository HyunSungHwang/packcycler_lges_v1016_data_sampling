#if !defined(AFX_DLG_CELLBAL_H__1BBA29C8_A5F1_4532_9382_D40293C30CF5__INCLUDED_)
#define AFX_DLG_CELLBAL_H__1BBA29C8_A5F1_4532_9382_D40293C30CF5__INCLUDED_

#include "Dlg_CellBALMain.h"
#include "Dlg_CellBALOpt.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_CellBAL.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBAL dialog

class Dlg_CellBAL : public CDialog
{
// Construction
public:
	Dlg_CellBAL(CWnd* pParent = NULL);   // standard constructor
	virtual ~Dlg_CellBAL();				// 200313 HKH Memory Leak ����

	Dlg_CellBALMain *m_DlgCellBalMain;
	Dlg_CellBALOpt  *m_DlgCellBalOpt;
	
	void onDlgHide();
	LRESULT onDlgCellBalOpt();
	void onDlgCellBalMain();

// Dialog Data
	//{{AFX_DATA(Dlg_CellBAL)
	enum { IDD = IDD_DIALOG_CELLBAL , IDD2 = IDD_DIALOG_CELLBAL_ENG };
	CStatic	m_StaticArea;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dlg_CellBAL)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Dlg_CellBAL)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnButCellbalOpt();
	afx_msg void OnButCellbalList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern Dlg_CellBAL *mainDlgCellBal;
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_CELLBAL_H__1BBA29C8_A5F1_4532_9382_D40293C30CF5__INCLUDED_)
