#if !defined(AFX_DETAILOVENDLG_H__CF3910DB_21D4_480C_9464_B09C1818BBDB__INCLUDED_)
#define AFX_DETAILOVENDLG_H__CF3910DB_21D4_480C_9464_B09C1818BBDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DetailOvenDlg.h : header file
//
#include "CTSMonProDoc.h"
#include "MyGridWnd.h"	// Added by ClassView
#include "LedButton.h"
#include "WarningDlg.h"

#define ID_OVEN_TIMER_SIZE 2000

/////////////////////////////////////////////////////////////////////////////
// CDetailOvenDlg dialog

class CDetailOvenDlg : public CDialog
{
	// Construction
public:
	LRESULT OnGridLClicked(WPARAM wParam, LPARAM lParam);
	LRESULT OnGridLDoubleClicked(WPARAM wParam, LPARAM lParam);
	// -------------------------------
	// Select chamber id
	int m_SelectChamberId;
	CMyGridWnd m_wndOvenGrid1;
	CMyGridWnd m_wndOvenGrid2;
	
	// =================================================
	//  DetailOvenDlg 변경 변수 false:오븐1, true:오븐2
	BOOL m_bDetailOvenDlgChange;
	char buf[20];
	BOOL m_bUseDoorOpenSensor;
	BOOL m_bSendDoorFlag;	
	CString m_OvenLoc;
	bool m_bChiller_DESChiller; //lyj 20201207


	void InitOvenGrid();		// 그리드 초기화
	void DrawOvenGrid1();		// 1번 그리드
	void DrawOvenGrid2();		// 2번 그리드
	void InsertDataOvenGrid1();	// 1번 그리드 데이타 입력
	void InsertDataOvenGrid2();	// 2번 그리드 데이타 입력
	void DisplayOvenGrid();		// DrawGrid
	
	void SetListFontSize(int nSize);
	void UpdateList(BOOL bDeleteAllItem = FALSE);
	void SetWinPos (CRect rect);
	void InitList();
	void InitLedBtn();
	void ResetLedBtn();			// LedBtn 초기화
	void DrawOvenStatus();		// Oven 상태 표시
	CString onGetDeviceName();
	
	//cny
	void InitControl();
	void onPumpDraw();
	void onSizeSave(int iType);
	BOOL onChkSbcRunState();
	BOOL onChkSerialIsPort();

	CDetailOvenDlg(CCTSMonProDoc * pDoc, CWnd* pParent = NULL);   // standard constructor
	// Dialog Data
	//{{AFX_DATA(CDetailOvenDlg)
	enum { IDD = IDD_DETAIL_OVEN_DLG , IDD2 = IDD_DETAIL_OVEN_DLG_ENG , IDD3 = IDD_DETAIL_OVEN_DLG_PL };
	CLabel	m_StaticPumpMode;
	CLabel	m_StaticPumpSP;
	CLabel	m_StaticPumpPV;
	CLabel	m_StaticPumpComm;
	CStatic	m_CtrlLabLine;
	CLedButton	m_CtrlUVCheck;
	CLedButton	m_CtrlDoorCheck;
	CLedButton	m_CtrlOtherCheck;
	CLedButton	m_CtrlSmokeCheck;
	CStatic		m_static_OvenNum;
	CButton		m_buttonOvenSelect;
	CListCtrl	m_ctrlOvenList;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDetailOvenDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	CWarningDlg	  * m_pWarringDlg;
	CCTSMonProDoc * m_pDoc;
	COvenCtrl		*m_pCtrlOven;			//
	int				m_nPortLine;			//ljb 챔버 라인
	enum {_COL_GRID2_NO = 0, _COL_GRID2_TEMPER_PV_ =1, _COL_GRID2_TEMPER_SV_ = 2, 
		_COL_GRID2_HUMID_PV = 3, _COL_GRID2_HUMID_SV = 4, _ROW_GRID2_ITEM_START_ = 2};
	
	// Generated message map functions
	//{{AFX_MSG(CDetailOvenDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBtnAdd();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBtnOvenSetup();
	virtual void OnCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnButton2();
	afx_msg void OnButOvenTest();
	afx_msg void OnButPumpRun();
	afx_msg void OnButPumpStop();
	afx_msg void OnButPumpAuto();
	afx_msg void OnButPumpManual();
	afx_msg void OnButPumpSetsp1();
	afx_msg void OnWindowPosChanged(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnButOvenTest2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	DWORD ConvertRGBStrToCOLORREF(CString strColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DETAILOVENDLG_H__CF3910DB_21D4_480C_9464_B09C1818BBDB__INCLUDED_)
