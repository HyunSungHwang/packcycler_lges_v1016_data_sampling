
#include <atlbase.h>

#define UM_LOGMESSAGE		      WM_USER+101          //LOG Message
#define UM_LOGINSTAT		      WM_USER+102          //HSMS login

#define ID_CHILLER_AUX_DIVISION2  15000
#define ID_CHILLER_CAN_DIVISION2  15000

#define MAX_CHAMBER_COUNT		  2
#define MAX_CLIENT_COUNT	      4
#define MAX_POWER_COUNT	          2
#define MAX_POWER_CH_COUNT	      2
#define MAX_CHILLER_COUNT	      4
#define MAX_PUMP_COUNT   	      4

#define ID_PUMP_CMD_READSTATE     1
#define ID_PUMP_CMD_RUN           2
#define ID_PUMP_CMD_STOP          3
#define ID_PUMP_CMD_AUTO          4
#define ID_PUMP_CMD_MANUAL        5
#define ID_PUMP_CMD_SETSPNO       6
#define ID_PUMP_CMD_SETSP1        7
#define ID_PUMP_CMD_SETSP2        8
#define ID_PUMP_CMD_SETSP3        9
#define ID_PUMP_CMD_SETSP4        10

#define ID_CHILLER_PUMP_CMD_READSTATE     1
#define ID_CHILLER_PUMP_CMD_RUN           2
#define ID_CHILLER_PUMP_CMD_STOP          3
#define ID_CHILLER_PUMP_CMD_FIX			  4
#define ID_CHILLER_PUMP_CMD_PROG	      5
#define ID_CHILLER_PUMP_CMD_SETSPNO       6
#define ID_CHILLER_PUMP_CMD_SETSP1        7
#define ID_CHILLER_PUMP_CMD_SETSP2        8
#define ID_CHILLER_PUMP_CMD_SETSP3        9
#define ID_CHILLER_PUMP_CMD_SETSP4        10

#define COMMON_CELLBAL_CFG_FILE   _T("CELLBAL_CONFIG.ini")
#define PROGRAM_LANGUAGE_SET	_T("LANGUAGE_SET") //yulee 20190715

#define ID_COLOR_RED    RGB(255, 0, 0)
#define ID_COLOR_BLACK  RGB(0, 0, 0)
#define ID_COLOR_WHITE  RGB(255, 255, 255)
#define ID_COLOR_GREEN  RGB(0, 255, 0)
#define ID_COLOR_ORANGE RGB(255, 153, 0)
#define ID_COLOR_DKGRAY RGB(128, 128, 128)

// const COLORREF CLOUDBLUE = RGB(128, 184, 223);
// const COLORREF WHITE = RGB(255, 255, 255);
// const COLORREF BLACK = RGB(1, 1, 1);
// const COLORREF DKGRAY = RGB(128, 128, 128);
// const COLORREF LLTGRAY = RGB(220, 220, 220);
// const COLORREF LTGRAY = RGB(192, 192, 192);
// const COLORREF YELLOW = RGB(255, 255, 0);
// const COLORREF DKYELLOW = RGB(128, 128, 0);
// const COLORREF RED = RGB(255, 0, 0);
// const COLORREF DKRED = RGB(128, 0, 0);
// const COLORREF BLUE = RGB(0, 0, 255);
// const COLORREF LBLUE = RGB(192, 192, 255);
// const COLORREF DKBLUE = RGB(0, 0, 128);
// const COLORREF CYAN = RGB(0, 255, 255);
// const COLORREF DKCYAN = RGB(0, 128, 128);
// const COLORREF GREEN = RGB(0, 255, 0);
// const COLORREF DKGREEN = RGB(0, 128, 0);
// const COLORREF MAGENTA = RGB(255, 0, 255);
// const COLORREF DKMAGENTA = RGB(128, 0, 128);
// const COLORREF ORANGE = RGB(255, 153, 0);

#pragma warning(disable:4514)
#pragma warning(disable:4510)
#pragma warning(disable:4520)
#pragma warning(disable:4146)//unary minus operator applied to unsigned type

#include <Winnetwk.h>
#pragma comment(lib,"Mpr.lib")

#include "./Global/Mesg.h"
#include "./Global/GStr.h"
#include "./Global/GMath.h"
#include "./Global/GReg.h"
#include "./Global/GWin.h"
#include "./Global/GFile.h"
#include "./Global/GShell.h"
#include "./Global/GTime.h"
#include "./Global/GGdc.h"
#include "./Global/GList.h"
#include "./Global/GNet.h"

#include "./Global/GLog.h"
#include "./Global/IniParse/IniParser.h"

typedef struct CHINFO	
{	
	int nModuleID;
	int nChannelIndex;	
	
	CString nomonitor;
	CString nmwork;
	CString nmmodule;
	CString testserial;
    CString Eqstate;
	CString Eqstr;
	CString nmsched;
	CString starttime;
	CString endtime;
	
	float fCapacitySum;
	float fMeter;
	int   isParallel;
	int   bMaster;
	
	PS_STEP_END_RECORD pRecord;
	
} CHINFO, *LPCHINFO;

typedef struct CHBACKTIME	
{	
	int nModuleID;
	int nChannelIndex;	
	
    int nState;
	
} CHBACKTIME, *LPCHBACKTIME;
typedef struct
{	
	CString AppPath;
	int     iPType; //yulee 20190705 碍力捍纺 可记 贸府 Mark
	BOOL           bChBackTimeStart;
	CTypedPtrList<CPtrList, CHBACKTIME*> ChBackTimeList;
	CString strDbip;
	CString strDbport;
	CString strDbname;
	CString strDbid;
	CString strDbpwd;
	CString strDbauto;
	CString strDbstate;
    
	CString strDeviceid;
	CString strBlockid;
	int     iState;

	CTypedPtrList<CPtrList, CHINFO*> ChList;
	
	//cny--------------------
	int     iEnd;
	int     iCellBalShow;
	int     iCellBalLog;
	int		iCEllBalLogRun;
	int     iCellBalAuxDiv3;
	CString strCellBal_CfgFile;

	int     iPwrSupply_CmdWaitTime;

	//yulee 20181114
	int		iRTTableShow;
	CString strSetLanuageFile;

} APPINFO;//config