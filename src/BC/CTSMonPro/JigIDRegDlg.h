#if !defined(AFX_JIGIDREGDLG_H__6FD05E24_8CF6_48D0_8BBA_8CB621BC2C1E__INCLUDED_)
#define AFX_JIGIDREGDLG_H__6FD05E24_8CF6_48D0_8BBA_8CB621BC2C1E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JigIDRegDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CJigIDRegDlg dialog
#include "CTSMonProDoc.h"

class CJigIDRegDlg : public CDialog
{
// Construction
public:
	int LoadingData();
	CString SearchLocation(int nModuleID, int nJigID);
	BOOL SearchLocation(CString strID, int &nModuleID, int &nJigID);
	BOOL DeleteJigIDInfo(CString strJigID);
	BOOL SaveJigInfoToDB(CString strID, int nModuleID, int nJigNo);
	CJigIDRegDlg(CWnd* pParent = NULL);   // standard constructor
	CCTSMonProDoc *m_pDoc;

// Dialog Data
	//{{AFX_DATA(CJigIDRegDlg)
	enum { IDD = IDD_JIG_LOCATION_DLG , IDD2 = IDD_JIG_LOCATION_DLG_ENG , IDD3 = IDD_JIG_LOCATION_DLG_PL };
	CComboBox	m_ctrlJig;
	CComboBox	m_ctrlModule;
	CListCtrl	m_wndList;
	CString	m_strTagID;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJigIDRegDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

//	CGXGridWnd	m_RegGrid;
	CWordArray m_aModuleList;

	// Generated message map functions
	//{{AFX_MSG(CJigIDRegDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeCombo1();
	//}}AFX_MSG
	afx_msg LRESULT OnBcrscaned(WPARAM wParam,LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JIGIDREGDLG_H__6FD05E24_8CF6_48D0_8BBA_8CB621BC2C1E__INCLUDED_)
