#if !defined(AFX_RESTOREDLG_H__7F4F4493_2A03_44B7_A54D_9C2A01E8F0B8__INCLUDED_)
#define AFX_RESTOREDLG_H__7F4F4493_2A03_44B7_A54D_9C2A01E8F0B8__INCLUDED_

#include "CTSMonProDoc.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RestoreDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRestoreDlg dialog
class CCTSMonProDoc;
#include "XPButton.h"

class CRestoreDlg : public CDialog
{
// Construction
public:
	
	int str_Count(CString tmpstr,CString searchstr);

	BOOL Fun_RemoveDir(CString strPath);
	BOOL AutoRepair(int nModuleID, int nChIndex);		//ljb
	void LossDataAddModuleAndChannel(int nModuleID, int nChIndex);	//ljb
	int FindLossDataRange(int nModuleID, int nChIndex);
	CRestoreDlg(CCTSMonProDoc *pDoc, CWnd* pParent = NULL);   // standard constructor

	//ljb -------------
	int m_iModuleList[255];
	int m_iChannelList[255];
	int m_iModuleListCount;
	int m_iChannelListCount;
	//////////////////////////////////////////////////////////////////////////

// Dialog Data
	//{{AFX_DATA(CRestoreDlg)
	enum { IDD = IDD_DATA_RESTORE_DLG , IDD2 = IDD_DATA_RESTORE_DLG_ENG ,IDD3 = IDD_DATA_RESTORE_DLG_PL };
	CComboBox	m_ctrlChannel;
	CComboBox	m_ctrlModule;
	CXPButton	m_btnRestore;
	CXPButton	m_btnOK;
	CXPButton	m_btnUpdate;
	CListCtrl	m_ctrlRangeList;
	CListCtrl	m_ctrlChList;
//	CLabel	m_CautionText;
	CStatic	m_CautionText;
	BOOL	m_bLineOffInclude;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRestoreDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCTSMonProDoc *m_pDoc;
	void InitRangeList();
	void InitChList();

	// Generated message map functions
	//{{AFX_MSG(CRestoreDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRestoreSelChButton();
	afx_msg void OnItemchangedDataLossChList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSearchUpdateButton();
	afx_msg void OnSelchangeComboMd();
	afx_msg void OnRestoreLocalFolderButton();
	afx_msg void OnRestoreLocalFolderButton2();
	afx_msg void OnBtnRestoreCtsFile();
	afx_msg void OnBtnRestoreCycCheck();
	afx_msg void OnBntDownRestoreData();
	afx_msg void OnButRestore2();
	afx_msg void OnButFileDiv();
	afx_msg void OnButDiv2();
	afx_msg void OnBtnRestoreCtsFile2();
	afx_msg void OnRestoreLocalFolderButton3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESTOREDLG_H__7F4F4493_2A03_44B7_A54D_9C2A01E8F0B8__INCLUDED_)
