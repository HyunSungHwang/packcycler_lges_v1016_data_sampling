// AccuracySetDlg.cpp : implementation file
//

#include "stdafx.h" 
#define _AFXDLL
#include "afxwin.h"
#include "CTSMonPro.h"
#include "AccuracySetDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAccuracySetDlg dialog


// CAccuracySetDlg::CAccuracySetDlg(CCaliPoint* pCalPoint, CWnd* pParent /*=NULL*/)
// 	: CDialog(CAccuracySetDlg::IDD, pParent)

	CAccuracySetDlg::CAccuracySetDlg(CCaliPoint* pCalPoint, CWnd* pParent /*=NULL*/)
	:CDialog( 
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAccuracySetDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAccuracySetDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CAccuracySetDlg::IDD3):
	(CAccuracySetDlg::IDD), pParent
		)


// 	CAccuracySetDlg::CAccuracySetDlg(CCaliPoint* pCalPoint, CWnd* pParent /*=NULL*/)
// 	:CDialog(	
// 	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAccuracySetDlg::IDD, pParent):
// (AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAccuracySetDlg::IDD2, pParent):
// (CAccuracySetDlg::IDD, pParent)
// 	)



{
	//{{AFX_DATA_INIT(CAccuracySetDlg)
	m_fIRange = 0.0f;
	m_fVRange = 0.0f;
	m_fIRange1 = 0.0f;
	m_fVRange1 = 0.0f;
	//}}AFX_DATA_INIT

	m_pCalPoint = pCalPoint;
	ASSERT(pCalPoint);
}


void CAccuracySetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAccuracySetDlg)
	DDX_Control(pDX, IDC_I_RANGE_LIST, m_IRangeList);
	DDX_Text(pDX, IDC_I_RANGE_EDIT, m_fIRange);
	DDX_Text(pDX, IDC_V_RANGE_EDIT, m_fVRange);
	DDX_Control(pDX, IDC_V_RANGE_LIST, m_VRangeList);
	DDX_Text(pDX, IDC_I_RANGE_EDIT2, m_fIRange1);
	DDX_Text(pDX, IDC_V_RANGE_EDIT2, m_fVRange1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAccuracySetDlg, CDialog)
	//{{AFX_MSG_MAP(CAccuracySetDlg)
	ON_LBN_SELCHANGE(IDC_I_RANGE_LIST, OnSelchangeIRangeList)
	ON_LBN_SELCHANGE(IDC_V_RANGE_LIST, OnSelchangeVRangeList)
	ON_EN_CHANGE(IDC_V_RANGE_EDIT, OnChangeVRangeEdit)
	ON_EN_CHANGE(IDC_V_RANGE_EDIT2, OnChangeVRangeEdit2)
	ON_EN_CHANGE(IDC_I_RANGE_EDIT, OnChangeIRangeEdit)
	ON_EN_CHANGE(IDC_I_RANGE_EDIT2, OnChangeIRangeEdit2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAccuracySetDlg message handlers

BOOL CAccuracySetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString strMsg;
	for(int  i =0; i<SFT_MAX_VOLTAGE_RANGE; i++)
	{
		strMsg.Format("Range #%d", i+1);
		m_VRangeList.AddString(strMsg);
	}
	for(int i = 0; i<SFT_MAX_CURRENT_RANGE; i++)
	{
		strMsg.Format("Range #%d", i+1);
		m_IRangeList.AddString(strMsg);
	}
	m_VRangeList.SetCurSel(0);
	OnSelchangeVRangeList();
	
	m_IRangeList.SetCurSel(0);
	OnSelchangeIRangeList();

	if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Crt Unit Mode", 0))
	{
		GetDlgItem(IDC_CRT_UNIT_STATIC1)->SetWindowText("uA");
		GetDlgItem(IDC_CRT_UNIT_STATIC2)->SetWindowText("uA");
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAccuracySetDlg::OnSelchangeIRangeList() 
{
	// TODO: Add your control notification handler code here

	
	int a = m_IRangeList.GetCurSel();
	if(a >=0 && a<SFT_MAX_CURRENT_RANGE)
	{
		m_fIRange = m_pCalPoint->m_dIDAAccuracy[a];
		m_fIRange1 = m_pCalPoint->m_dIADAccuracy[a];
		
		CString str;
		m_IRangeList.GetText(a, str);
		GetDlgItem(IDC_I_RANGE_STATIC)->SetWindowText("Current "+str);
	}
	UpdateData(FALSE);
}

void CAccuracySetDlg::OnSelchangeVRangeList() 
{
	// TODO: Add your control notification handler code here
	int a = m_VRangeList.GetCurSel();
	if(a >=0 && a<SFT_MAX_VOLTAGE_RANGE)
	{
		m_fVRange = m_pCalPoint->m_dVDAAccuracy[a];
		m_fVRange1 = m_pCalPoint->m_dVADAccuracy[a];
	
		CString str;
		m_IRangeList.GetText(a, str);
		GetDlgItem(IDC_V_RANGE_STATIC)->SetWindowText("voltage "+str);
	}
	UpdateData(FALSE);	
}

void CAccuracySetDlg::OnChangeVRangeEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_VRangeList.GetCurSel();
	if(a >=0 && a<SFT_MAX_VOLTAGE_RANGE)
	{
		m_pCalPoint->m_dVDAAccuracy[a]	= m_fVRange;
	}
}

void CAccuracySetDlg::OnChangeVRangeEdit2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_VRangeList.GetCurSel();
	if(a >=0 && a<SFT_MAX_VOLTAGE_RANGE)
	{
		m_pCalPoint->m_dVADAccuracy[a] = m_fVRange1;
	}	
}

void CAccuracySetDlg::OnChangeIRangeEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_IRangeList.GetCurSel();
	if(a >=0 && a<SFT_MAX_CURRENT_RANGE)
	{
		m_pCalPoint->m_dIDAAccuracy[a] = m_fIRange;
	}
	
}

void CAccuracySetDlg::OnChangeIRangeEdit2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int a = m_IRangeList.GetCurSel();
	if(a >=0 && a<SFT_MAX_CURRENT_RANGE)
	{
		m_pCalPoint->m_dIADAccuracy[a] = m_fIRange1;
	}
}

void CAccuracySetDlg::OnOK() 
{
	// TODO: Add extra validation here
	m_pCalPoint->SavePointData();

	CDialog::OnOK();
}