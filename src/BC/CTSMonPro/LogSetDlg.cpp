// LogSetDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "LogSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogSetDlg dialog


CLogSetDlg::CLogSetDlg(CWnd* pParent /*=NULL*/)
//	: CDialog(CLogSetDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CLogSetDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CLogSetDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CLogSetDlg::IDD3):
	(CLogSetDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CLogSetDlg)
	m_bUseAutoHide = TRUE;
	m_nHideDelay = 5;
	m_nLogLevel = 1;
	m_bAutoShow = TRUE;
	//}}AFX_DATA_INIT
}


void CLogSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogSetDlg)
	DDX_Check(pDX, IDC_USE_AUTO_HIDE_CHECK, m_bUseAutoHide);
	DDX_Text(pDX, IDC_HIDE_DELAY_EDIT, m_nHideDelay);
	DDV_MinMaxUInt(pDX, m_nHideDelay, 1, 3600);
	DDX_CBIndex(pDX, IDC_LOG_LEVEL_COMBO, m_nLogLevel);
	DDX_Check(pDX, IDC_AUTO_SHOW_CHECK, m_bAutoShow);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogSetDlg, CDialog)
	//{{AFX_MSG_MAP(CLogSetDlg)
	ON_BN_CLICKED(IDC_USE_AUTO_HIDE_CHECK, OnUseAutoHideCheck)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogSetDlg message handlers

void CLogSetDlg::OnUseAutoHideCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	if(m_bUseAutoHide)
	{
		GetDlgItem(IDC_HIDE_DELAY_EDIT)->EnableWindow();
		GetDlgItem(IDC_HIDE_STATIC)->EnableWindow();
	}
	else
	{
		GetDlgItem(IDC_HIDE_DELAY_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_HIDE_STATIC)->EnableWindow(FALSE);
	}
}

BOOL CLogSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if(m_bUseAutoHide)
	{
		GetDlgItem(IDC_HIDE_DELAY_EDIT)->EnableWindow();
		GetDlgItem(IDC_HIDE_STATIC)->EnableWindow();
	}
	else
	{
		GetDlgItem(IDC_HIDE_DELAY_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_HIDE_STATIC)->EnableWindow(FALSE);
	}
	UpdateData(FALSE);
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLogSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	
	CDialog::OnOK();
}