// FileSaveSetDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "FileSaveSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileSaveSetDlg dialog


CFileSaveSetDlg::CFileSaveSetDlg(CWnd* pParent /*=NULL*/)
//	: CDialog(CFileSaveSetDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CFileSaveSetDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CFileSaveSetDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CFileSaveSetDlg::IDD3):
	(CFileSaveSetDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CFileSaveSetDlg)
	m_bTime = TRUE;
	m_bVoltage = TRUE;
	m_bCurrent = TRUE;
	m_bCapacity = FALSE;
	m_bWattHour = FALSE;
	m_bAvgCurrent = TRUE;
	m_bAvgVoltage = TRUE;
	m_bOvenTemperature = FALSE;
	m_bMeterValue = FALSE;
	m_bCVTime = FALSE;
	m_bSyncTime = FALSE;
	m_bOvenHumidity = FALSE;
	m_bVoltageInput = TRUE;
	m_bVoltagePower = TRUE;
	m_bVoltageBus = TRUE;
	m_bOutputState = FALSE;
	m_bInputState = FALSE;
	m_bLoaderVolt = FALSE;
	m_bLoaderCurr = FALSE;
	//}}AFX_DATA_INIT
}


void CFileSaveSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileSaveSetDlg)
	DDX_Check(pDX, IDC_CHECK8, m_bTime);
	DDX_Check(pDX, IDC_CHECK1, m_bVoltage);
	DDX_Check(pDX, IDC_CHECK2, m_bCurrent);
	DDX_Check(pDX, IDC_CHECK3, m_bCapacity);
	DDX_Check(pDX, IDC_CHECK4, m_bWattHour);
	DDX_Check(pDX, IDC_CHECK5, m_bAvgCurrent);
	DDX_Check(pDX, IDC_CHECK6, m_bAvgVoltage);
	DDX_Check(pDX, IDC_CHECK7, m_bOvenTemperature);
	DDX_Check(pDX, IDC_CHECK9, m_bMeterValue);
	DDX_Check(pDX, IDC_CHECK10, m_bCVTime);
	DDX_Check(pDX, IDC_CHECK11, m_bSyncTime);
	DDX_Check(pDX, IDC_CHECK12, m_bOvenHumidity);
	DDX_Check(pDX, IDC_CHECK13, m_bVoltageInput);
	DDX_Check(pDX, IDC_CHECK14, m_bVoltagePower);
	DDX_Check(pDX, IDC_CHECK15, m_bVoltageBus);
	DDX_Check(pDX, IDC_CHECK16, m_bOutputState);
	DDX_Check(pDX, IDC_CHECK17, m_bInputState);
	DDX_Check(pDX, IDC_CHECK18, m_bLoaderVolt);
	DDX_Check(pDX, IDC_CHECK19, m_bLoaderCurr);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFileSaveSetDlg, CDialog)
	//{{AFX_MSG_MAP(CFileSaveSetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileSaveSetDlg message handlers

BOOL CFileSaveSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdateData();
	m_bTime = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Time", TRUE);
	m_bVoltage = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage", TRUE);
	m_bCurrent = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Current", TRUE);
	m_bCapacity = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Capacity", TRUE);
	m_bWattHour = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "WattHour", TRUE);
	m_bAvgCurrent = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgCurrent", TRUE);
	m_bAvgVoltage = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgVoltage", TRUE);
	m_bOvenTemperature = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenTemperature", TRUE);
	m_bOvenHumidity = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenHumidity", TRUE);

	m_bMeterValue = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Meter", FALSE);

	m_bCVTime = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "CVTime", TRUE);
	m_bSyncTime = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "SyncTime", TRUE);

	m_bVoltageInput = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Input", TRUE);
	m_bVoltagePower = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Power", TRUE);
	m_bVoltageBus = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Bus", TRUE);
	
	m_bOutputState = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Output State", TRUE);
	m_bInputState = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "Input State", TRUE);

	m_bLoaderVolt = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderVoltage", TRUE);
	m_bLoaderCurr = AfxGetApp()->GetProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderCurrent", TRUE);

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CFileSaveSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Time", m_bTime);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage", m_bVoltage);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Current", m_bCurrent);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Capacity", m_bCapacity);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "WattHour", m_bWattHour);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgCurrent", m_bAvgCurrent);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "AvgVoltage", m_bAvgVoltage);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenTemperature", m_bOvenTemperature);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "OvenHumidity", m_bOvenHumidity);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Meter", m_bMeterValue);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "CVTime", m_bCVTime);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "SyncTime", m_bSyncTime);

	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Input", m_bVoltageInput);	//ljb 201011
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Power", m_bVoltagePower);	//ljb 201011
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Voltage Bus", m_bVoltageBus);		//ljb 201011

	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Output State", m_bOutputState);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "Input State", m_bInputState);

	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderVoltage", m_bLoaderVolt);
	AfxGetApp()->WriteProfileInt(FILE_SAVE_ITEM_REG_SET, "LoaderCurrent", m_bLoaderCurr);
	CDialog::OnOK();
}