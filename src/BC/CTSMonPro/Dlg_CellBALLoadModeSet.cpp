// Dlg_CellBALLoadModeSet.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "Dlg_CellBALLoadModeSet.h"

#include "Global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALLoadModeSet dialog


Dlg_CellBALLoadModeSet::Dlg_CellBALLoadModeSet(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_CellBALLoadModeSet::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_CellBALLoadModeSet::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_CellBALLoadModeSet::IDD3):
	Dlg_CellBALLoadModeSet::IDD, pParent)
{
	//{{AFX_DATA_INIT(Dlg_CellBALLoadModeSet)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_iState_Conn=0;
	m_cmdBodyLoadModeSet=NULL;
}


void Dlg_CellBALLoadModeSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_CellBALLoadModeSet)
	DDX_Control(pDX, IDC_EDIT_SIGNAL_SENS_TIME, m_EditSignalSensTime);
	DDX_Control(pDX, IDC_EDIT_AVAILABILITY_VOLTAGE_LOWER, m_EditChCondEndVoltage);
	DDX_Control(pDX, IDC_EDIT_CH_COND_END_TIME, m_EditChCondEndTime);
	DDX_Control(pDX, IDC_EDIT_CH_COND_END_CURRENT, m_EditChCondEndCurrent);
	DDX_Control(pDX, IDC_EDIT_CH_COND_DETECTION, m_EditChCondDetection);
	DDX_Control(pDX, IDC_EDIT_CH_COND_AUTO_STOP_TIME, m_EditChCondAutoStopTime);
	DDX_Control(pDX, IDC_EDIT_UNDER_VOLTAGE, m_EditUnderVoltage);
	DDX_Control(pDX, IDC_EDIT_RESERVED2, m_EditReserved2);
	DDX_Control(pDX, IDC_EDIT_RESERVED1, m_EditReserved1);
	DDX_Control(pDX, IDC_EDIT_AVAILABILITY_VOLTAGE_UPPER, m_EditOverVoltage);
	DDX_Control(pDX, IDC_EDIT_OVER_TEMP, m_EditOverTemp);
	DDX_Control(pDX, IDC_EDIT_CH_COND_RELEASE, m_EditChCondRelease);
	DDX_Control(pDX, IDC_EDIT_OVER_CURRENT, m_EditOverCurrent);
	DDX_Control(pDX, IDC_EDIT_LOAD_MODE_VALUE, m_EditLoadModeValue);
	DDX_Control(pDX, IDC_EDIT_LOAD_MODE_SET, m_EditLoadModeSet);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_CellBALLoadModeSet, CDialog)
	//{{AFX_MSG_MAP(Dlg_CellBALLoadModeSet)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALLoadModeSet message handlers

BOOL Dlg_CellBALLoadModeSet::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_EditLoadModeSet.LimitText(2);
	m_EditReserved1.LimitText(2);
	m_EditLoadModeValue.LimitText(10);
	m_EditSignalSensTime.LimitText(2);
	m_EditReserved2.LimitText(8);
	m_EditOverVoltage.LimitText(10);
	m_EditUnderVoltage.LimitText(10);
	m_EditOverCurrent.LimitText(10);
	m_EditOverTemp.LimitText(10);
	m_EditChCondEndVoltage.LimitText(10);
	m_EditChCondEndCurrent.LimitText(10);
	m_EditChCondEndTime.LimitText(10);
	m_EditChCondDetection.LimitText(10);
	m_EditChCondRelease.LimitText(10);
	m_EditChCondAutoStopTime.LimitText(10);

	m_EditLoadModeSet.SetWindowText(_T("CR"));
	m_EditReserved1.SetWindowText(_T("00"));
	m_EditLoadModeValue.SetWindowText(_T("+0010000mR"));
	m_EditSignalSensTime.SetWindowText(_T("15"));
	m_EditReserved2.SetWindowText(_T("00000000"));
	m_EditOverVoltage.SetWindowText(_T("+4300000uV"));
	m_EditUnderVoltage.SetWindowText(_T("+2400000uV"));
	m_EditOverCurrent.SetWindowText(_T("-3000000uA"));
	m_EditOverTemp.SetWindowText(_T("+0050.00dC"));
	m_EditChCondEndVoltage.SetWindowText(_T("+3500000uV"));
	m_EditChCondEndCurrent.SetWindowText(_T("+0000000uA"));
	m_EditChCondEndTime.SetWindowText(_T("0000000sec"));
	m_EditChCondDetection.SetWindowText(_T("+0030000uV"));
	m_EditChCondRelease.SetWindowText(_T("+0020000uV"));
	m_EditChCondAutoStopTime.SetWindowText(_T("0000000sec"));

	if( m_iState_Conn>ID_SOCKET_STAT_NONE &&
		m_cmdBodyLoadModeSet)
	{
		CString strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->load_mode_set,2,0,_T(""));
		m_EditLoadModeSet.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->reserved1,2,0,_T(""));
		m_EditReserved1.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->load_mode_value,10,0,_T(""));
		m_EditLoadModeValue.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->signal_sens_time,2,0,_T(""));
		m_EditSignalSensTime.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->reserved2,8,0,_T(""));
		m_EditReserved2.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->availability_voltage_upper,10,0,_T(""));
		m_EditOverVoltage.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->under_voltage,10,0,_T(""));
		m_EditUnderVoltage.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->over_current,10,0,_T(""));
		m_EditOverCurrent.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->over_temp,10,0,_T(""));
		m_EditOverTemp.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->availability_voltage_lower,10,0,_T(""));
		m_EditChCondEndVoltage.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->ch_cond_end_current,10,0,_T(""));
		m_EditChCondEndCurrent.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->ch_cond_end_time,10,0,_T(""));
		if(atoi(strTemp) > 600) //yulee 20190117
		{
			int TmpiVal;
			TmpiVal = atoi(strTemp);
			TmpiVal -= 600;
			strTemp.Format(_T("%07d"), TmpiVal);
		}
		m_EditChCondEndTime.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->ch_cond_detection,10,0,_T(""));
		m_EditChCondDetection.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->ch_cond_release,10,0,_T(""));
		m_EditChCondRelease.SetWindowText(strTemp);

		strTemp=mainGlobal.onGetCHAR(m_cmdBodyLoadModeSet->ch_cond_auto_stop_time,10,0,_T(""));
		m_EditChCondAutoStopTime.SetWindowText(strTemp);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_CellBALLoadModeSet::OnCancel() 
{
	CDialog::OnCancel();
}

void Dlg_CellBALLoadModeSet::OnClose() 
{
	CDialog::OnClose();
}

void Dlg_CellBALLoadModeSet::OnOK() 
{
	onCellBALMakeLoadModeSet_User();

	CDialog::OnOK();
}

void Dlg_CellBALLoadModeSet::onCellBALMakeLoadModeSet_User()
{
	if(!m_cmdBodyLoadModeSet)
	{
		MSGWARN(this->m_hWnd,_T("load mode set failed"),TITLE_WARN);
		return;
	}
	CString strTemp;
	int nSize=0;

	strTemp=GWin::win_GetText(&m_EditLoadModeSet);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->load_mode_set,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditReserved1);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->reserved1,strTemp,nSize);
	
	strTemp=GWin::win_GetText(&m_EditLoadModeValue);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->load_mode_value,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditSignalSensTime);
	nSize=strTemp.GetLength();
	if(nSize == 1)
	{
		strTemp.Format("%02d", atoi(strTemp));
		nSize = 2;
	}
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->signal_sens_time,strTemp,nSize);

	strTemp=GWin::win_GetText(&m_EditReserved2);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->reserved2,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditOverVoltage);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->availability_voltage_upper,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditUnderVoltage);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->under_voltage,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditOverCurrent);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->over_current,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditOverTemp);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->over_temp,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditChCondEndVoltage);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->availability_voltage_lower,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditChCondEndCurrent);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->ch_cond_end_current,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditChCondEndTime);
	nSize=strTemp.GetLength();

	if(atoi(strTemp) > 0) //yulee 20190117
	{
		int TmpiVal;
		TmpiVal = atoi(strTemp);
		TmpiVal += 600;
		strTemp.Format(_T("%07d"), TmpiVal);
	}
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->ch_cond_end_time,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditChCondDetection);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->ch_cond_detection,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditChCondRelease);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->ch_cond_release,strTemp,nSize);
		
	strTemp=GWin::win_GetText(&m_EditChCondAutoStopTime);
	nSize=strTemp.GetLength();
	mainGlobal.onCopyChar(m_cmdBodyLoadModeSet->ch_cond_auto_stop_time,strTemp,nSize);
}

BOOL Dlg_CellBALLoadModeSet::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE)
		{					  
			return FALSE;	
		}
		if(pMsg->wParam == VK_RETURN)
		{		
			OnOK();
			return FALSE;	
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
