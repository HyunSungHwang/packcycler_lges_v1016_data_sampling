// ItemOnDisplay.h: interface for the CItemOnDisplay class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITEMONDISPLAY_H__FDFEEA56_F758_4E26_9CDD_7B024D9A78D8__INCLUDED_)
#define AFX_ITEMONDISPLAY_H__FDFEEA56_F758_4E26_9CDD_7B024D9A78D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct s_ListColumn {
	CString strTitle;
	UINT	nBlockWidth;
	BOOL	bDisplay;
};

class CItemOnDisplay  
{
public:
	///////////////////////////////////////////////////////
	// Constructor & Destructor
	CItemOnDisplay();
	virtual ~CItemOnDisplay();

	///////////////////////////////////////////////////////
	// Static Member Variables
	static long m_lNumOfItems;
	static CString REG_SECTION_NAME;

	///////////////////////////////////////////////////////
	// Static Member Functions
	static CItemOnDisplay* CreateItemsOnDisplay();			////Registry의 [DisplayItem]에서 표기할 Column을 읽는다.
	static int FindItemIndexOfTitle(CItemOnDisplay *pItemOnDisplay,WORD nITem);
	static void DeleteAllItemOnDisplay(CItemOnDisplay *pItemOnDisplay);
	static int GetItemNum() { return m_lNumOfItems; }
//	static void	LoadImageIndex(LVITEM& lvItem, int nChannelState);

	///////////////////////////////////////////////////////
	// Dynamic Member Variables
	CString m_strItemTitle;
	int		m_nBlockWidth;
	int		m_nItemNo;
	

	///////////////////////////////////////////////////////
	// Dynamic Member Functions
	int		GetItemNo()	{	return m_nItemNo;	}
	CString	GetTitle() { return m_strItemTitle; }
	int		GetWidth() { return m_nBlockWidth; }
	void	SetWidth(int nWidth) { m_nBlockWidth = nWidth; }

protected:
	struct s_ListColumn	*m_pItemList;
};

#endif // !defined(AFX_ITEMONDISPLAY_H__FDFEEA56_F758_4E26_9CDD_7B024D9A78D8__INCLUDED_)
