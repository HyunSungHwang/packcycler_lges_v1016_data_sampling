// SelectScheduleDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "SelectScheduleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectScheduleDlg dialog


CSelectScheduleDlg::CSelectScheduleDlg(CCTSMonProDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSelectScheduleDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSelectScheduleDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSelectScheduleDlg::IDD3):
	(CSelectScheduleDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSelectScheduleDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	ASSERT(pDoc);

	m_nSelModelID = 0;
	m_nSelScheduleID = 0;
}


void CSelectScheduleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectScheduleDlg)
	DDX_Control(pDX, IDC_EDIT_BUTTON, m_btnEdit);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_SCHEDULE_LIST, m_ctrlScheduleList);
	DDX_Control(pDX, IDC_MODEL_LIST, m_ctrlModelList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectScheduleDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectScheduleDlg)
	ON_BN_CLICKED(IDC_EDIT_BUTTON, OnEditButton)
	ON_NOTIFY(NM_CLICK, IDC_MODEL_LIST, OnClickModelList)
	ON_NOTIFY(NM_CLICK, IDC_SCHEDULE_LIST, OnClickScheduleList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectScheduleDlg message handlers

void CSelectScheduleDlg::InitList()
{
//	m_pChStsSmallImage = new CImageList;

	//상태별 표시할 이미지 로딩
//	m_pChStsSmallImage->Create(IDB_CELL_STATE_ICON,19,7,RGB(255,255,255));
//	m_wndChannelList.SetImageList(m_pChStsSmallImage, LVSIL_SMALL);

	//Style
	m_ctrlModelList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);

	// Column 삽입
	m_ctrlModelList.InsertColumn(0, "No",  LVCFMT_CENTER,  30,  0);
	m_ctrlModelList.InsertColumn(1, Fun_FindMsg("InitList_msg1","IDD_SCHEDULE_LIST_DLG"),  LVCFMT_CENTER,  100,  1);
	//@ m_ctrlModelList.InsertColumn(1, "이 름",  LVCFMT_CENTER,  100,  1);
	m_ctrlModelList.InsertColumn(2, Fun_FindMsg("InitList_msg2","IDD_SCHEDULE_LIST_DLG"),  LVCFMT_CENTER,  150,  2);
	//@ m_ctrlModelList.InsertColumn(2, "설명",  LVCFMT_CENTER,  150,  2);
	m_ctrlModelList.InsertColumn(3, Fun_FindMsg("InitList_msg3","IDD_SCHEDULE_LIST_DLG"),  LVCFMT_CENTER,  150,  3);
	//@ m_ctrlModelList.InsertColumn(3, "등록일",  LVCFMT_CENTER,  150,  3);

	m_ctrlScheduleList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_TRACKSELECT);

	// Column 삽입
	m_ctrlScheduleList.InsertColumn(0, "No",  LVCFMT_CENTER,  30,  0);
	m_ctrlScheduleList.InsertColumn(1, Fun_FindMsg("InitList_msg4","IDD_SCHEDULE_LIST_DLG"),  LVCFMT_CENTER,  100,  1);
	//@ m_ctrlScheduleList.InsertColumn(1, "이름",  LVCFMT_CENTER,  100,  1);
	m_ctrlScheduleList.InsertColumn(2, Fun_FindMsg("InitList_msg5","IDD_SCHEDULE_LIST_DLG"),  LVCFMT_CENTER,  150,  2);
	//@ m_ctrlScheduleList.InsertColumn(2, "설명",  LVCFMT_CENTER,  150,  2);
	m_ctrlScheduleList.InsertColumn(3, Fun_FindMsg("InitList_msg6","IDD_SCHEDULE_LIST_DLG"),  LVCFMT_CENTER,  150,  3);
	//@ m_ctrlScheduleList.InsertColumn(3, "등록일",  LVCFMT_CENTER,  150,  3);

//	CString strTitle;
//	LVITEM lvItem;
//	ZeroMemory(&lvItem, sizeof(LVITEM));
//	lvItem.mask = LVIF_TEXT|LVIF_IMAGE;
//
//	for(int nI = 0; nI < nCount; nI++ )
//	{
//		lvItem.iItem = nI;
//		lvItem.iSubItem = 0;
//		lvItem.iImage = I_IMAGECALLBACK;
////		strTitle.Format("");
////		lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
//		lvItem.lParam = nI;			//Channel Index 저장 
//		m_wndChannelList.InsertItem(&lvItem);
//	}
	
}

BOOL CSelectScheduleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	ASSERT(m_pDoc);
	// TODO: Add extra initialization here

	InitList();
	// TODO: Add extra initialization here

	RequeryModelList();

	for(int i=0; i<m_ctrlModelList.GetItemCount(); i++)
	{
		if(m_ctrlModelList.GetItemData(i)== m_nSelModelID)
		{
			m_ctrlModelList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
			CString strName = m_ctrlModelList.GetItemText(i, 1);
			GetDlgItem(IDC_SELECTED_MODEL_STATIC)->SetWindowText(strName);
			break;
		}
	}

	if(m_nSelModelID > 0)
	{
		RequeryScheduleList(m_nSelModelID);
	
		for(int i=0; i<m_ctrlScheduleList.GetItemCount(); i++)
		{
			if(m_ctrlScheduleList.GetItemData(i)== m_nSelScheduleID)
			{
				m_ctrlScheduleList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
				CString strName = m_ctrlScheduleList.GetItemText(i, 1);
				GetDlgItem(IDC_SELECTED_SCHEDULE_STATIC)->SetWindowText(strName);
				m_ctrlScheduleList.SetFocus();
				break;
			}
		}
	}

	UpdateSelection();

	//20130121 add start /////////////////////////////////////////////
// 	UpdateData(TRUE);
// 	if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseOverChargeMode", FALSE) == TRUE)
// 	{
// 		GetDlgItem(IDC_CHECK_SKIP_OVER_CHARGE)->ShowWindow(SW_SHOW);
// 		m_chkSkipOverCharge = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CheckOverChargeMode", FALSE);
// 		
// 	}
// 	else
// 	{
// 		GetDlgItem(IDC_CHECK_SKIP_OVER_CHARGE)->ShowWindow(SW_HIDE);
// 		m_chkSkipOverCharge = FALSE;
// 		
// 	}
	//20130121 add end /////////////////////////////////////////////
	
	UpdateData(FALSE);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectScheduleDlg::OnOK() 
{
	// TODO: Add extra validation here
	POSITION pos = m_ctrlModelList.GetFirstSelectedItemPosition();
	
	POSITION pos1 = m_ctrlScheduleList.GetFirstSelectedItemPosition();

	if(pos == NULL || pos1 == NULL)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_SCHEDULE_LIST_DLG"));
		//@ AfxMessageBox("선택된 목록이 없습니다.");
		return;
	}

	int a = m_ctrlModelList.GetNextSelectedItem(pos);
	m_nSelModelID = m_ctrlModelList.GetItemData(a);
	
	int b = m_ctrlScheduleList.GetNextSelectedItem(pos1);
	m_nSelScheduleID = m_ctrlScheduleList.GetItemData(b);

	
	CDialog::OnOK();
}

void CSelectScheduleDlg::OnEditButton() 
{
	// TODO: Add your control notification handler code here

	POSITION pos = m_ctrlModelList.GetFirstSelectedItemPosition();
	POSITION pos1 = m_ctrlScheduleList.GetFirstSelectedItemPosition();

	if(pos == NULL || pos1 == NULL)
	{
		AfxMessageBox(Fun_FindMsg("OnEditButton_msg","IDD_SCHEDULE_LIST_DLG"));
		//@ AfxMessageBox("선택된 목록이 없습니다.");
		return;
	}

	int a = m_ctrlModelList.GetNextSelectedItem(pos);
	int b = m_ctrlScheduleList.GetNextSelectedItem(pos1);

	m_pDoc->ExecuteEditor(m_ctrlModelList.GetItemText(a, 1), m_ctrlScheduleList.GetItemText(b, 1));
}

BOOL CSelectScheduleDlg::RequeryModelList()
{
	CDaoDatabase  db;

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	

	CString strSQL;
	//strSQL = "SELECT ModelID, No, ModelName, Description, CreatedTime FROM BatteryModel ORDER BY No DESC";
	strSQL = "SELECT ModelID, No, ModelName, Description, CreatedTime FROM BatteryModel";
	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

//	CString strTitle;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;//|LVIF_IMAGE;
//
//	for(int nI = 0; nI < nCount; nI++ )
//	{
//		lvItem.iItem = nI;
//		lvItem.iSubItem = 0;
//		lvItem.iImage = I_IMAGECALLBACK;
////		strTitle.Format("");
////		lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
//		lvItem.lParam = nI;			//Channel Index 저장 
//		m_wndChannelList.InsertItem(&lvItem);
//	}

	int nI = 0;
	char szBuff[128];
	if(rs.IsBOF())
	{
		rs.Close();
		return TRUE;
	}

	int nPK = 0;
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);		//PK
		nPK = data.lVal;
		data = rs.GetFieldValue(1);		//No

		lvItem.iItem = nI++;
		lvItem.iSubItem = 0;
		lvItem.pszText = data.pcVal; 
		sprintf(szBuff,"%d", nI);
		lvItem.pszText = szBuff; 
		m_ctrlModelList.InsertItem(&lvItem);
		m_ctrlModelList.SetItemData(lvItem.iItem, nPK);

		data = rs.GetFieldValue(2);		//Name
		sprintf(szBuff,"%d", data.lVal);
		lvItem.iSubItem = 1;
		lvItem.pszText = data.pcVal; 
		m_ctrlModelList.SetItem(&lvItem);
		
		data = rs.GetFieldValue(3);		//Description
		lvItem.iSubItem = 2;
		lvItem.pszText = data.pcVal; 
		m_ctrlModelList.SetItem(&lvItem);

		data = rs.GetFieldValue(4);		//DateTime

		COleDateTime  date = data;
		lvItem.iSubItem = 3;
		sprintf(szBuff, "%s", date.Format());
		lvItem.pszText = szBuff; 
		m_ctrlModelList.SetItem(&lvItem);

		rs.MoveNext();
	}

	rs.Close();
	db.Close();

	return TRUE;
}

BOOL CSelectScheduleDlg::RequeryScheduleList(UINT nModelID)
{
	CDaoDatabase  db;


	int nCount = m_ctrlScheduleList.GetItemCount();

	// Delete all of the items from the list view control.
	for (int i=0;i < nCount;i++)
	{
	   m_ctrlScheduleList.DeleteItem(0);
	}

	GetDlgItem(IDC_SELECTED_SCHEDULE_STATIC)->SetWindowText(Fun_FindMsg("RequeryScheduleList_msg","IDD_SCHEDULE_LIST_DLG"));
	//@ GetDlgItem(IDC_SELECTED_SCHEDULE_STATIC)->SetWindowText("선택된 스케쥴이 없습니다.");

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	
	

	CString strSQL, strWhere; 
	strSQL = "SELECT TestID, TestNo, TestName, Description, Creator, ModifiedTime, ProcTypeID FROM TestName ";
	strWhere.Format("WHERE ModelID = %d ORDER BY TestNo", nModelID);

	
	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL+strWhere, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

//	CString strTitle;
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_TEXT;//|LVIF_IMAGE;
//
//	for(int nI = 0; nI < nCount; nI++ )
//	{
//		lvItem.iItem = nI;
//		lvItem.iSubItem = 0;
//		lvItem.iImage = I_IMAGECALLBACK;
////		strTitle.Format("");
////		lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
//		lvItem.lParam = nI;			//Channel Index 저장 
//		m_wndChannelList.InsertItem(&lvItem);
//	}

	int nI = 0;
	char szBuff[128];
	if(rs.IsBOF())
	{
		rs.Close();
		return TRUE;
	}


	int nPK = 0;
	while(!rs.IsEOF())
	{
		data = rs.GetFieldValue(0);		//PK
		nPK = data.lVal;
		data = rs.GetFieldValue(1);		//No
		data = rs.GetFieldValue(2);		//Name

		lvItem.iItem = nI++;
		lvItem.iSubItem = 0;
		sprintf(szBuff,"%d", nI);
		lvItem.pszText = szBuff; 
		m_ctrlScheduleList.InsertItem(&lvItem);
		m_ctrlScheduleList.SetItemData(lvItem.iItem, nPK);

		lvItem.iSubItem = 1;
		lvItem.pszText = data.pcVal; 
		m_ctrlScheduleList.SetItem(&lvItem);
		
		data = rs.GetFieldValue(3);		//Description
		lvItem.iSubItem = 2;
		lvItem.pszText =data.pcVal; 
		m_ctrlScheduleList.SetItem(&lvItem);

		
		data = rs.GetFieldValue(3);		//Creator
		data = rs.GetFieldValue(5);		//DateTime

		COleDateTime  date = data;
		lvItem.iSubItem = 3;
		sprintf(szBuff, "%s", date.Format());
		lvItem.pszText = szBuff; 
		m_ctrlScheduleList.SetItem(&lvItem);

		rs.MoveNext();
	}

	rs.Close();
	db.Close();

	return TRUE;
}

void CSelectScheduleDlg::OnClickModelList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
//	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	
//    long index = pDispInfo->item.iItem;
//    long nModelPK = pDispInfo->item.lParam;

	int nItem = 0;
	POSITION pos = m_ctrlModelList.GetFirstSelectedItemPosition();
	if(pos)
	{
		nItem = m_ctrlModelList.GetNextSelectedItem(pos);
		m_nSelModelID = m_ctrlModelList.GetItemData(nItem);
	}
	else
	{
		return;
	}

	CString strName = m_ctrlModelList.GetItemText(nItem, 1);
	GetDlgItem(IDC_SELECTED_MODEL_STATIC)->SetWindowText(strName);
	
	RequeryScheduleList(m_nSelModelID);

	UpdateSelection();

	*pResult = 0;
}

void CSelectScheduleDlg::OnClickScheduleList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	int nItem = 0;
	POSITION pos = m_ctrlScheduleList.GetFirstSelectedItemPosition();
	if(pos)
	{
		nItem = m_ctrlScheduleList.GetNextSelectedItem(pos);
		m_nSelScheduleID = m_ctrlScheduleList.GetItemData(nItem);
	}
	else
	{
		return;
	}

	CString strName = m_ctrlScheduleList.GetItemText(nItem, 1);
	GetDlgItem(IDC_SELECTED_SCHEDULE_STATIC)->SetWindowText(strName);
	

	UpdateSelection();
	*pResult = 0;
}

void CSelectScheduleDlg::UpdateSelection()
{
	if(m_nSelScheduleID != 0 && m_nSelModelID != 0)
	{
		GetDlgItem(IDOK)->EnableWindow();
	}
	else
	{
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	}
}