// header (.h) file
 
class CRealEditControl: public CGXEditControl
{
	GRID_DECLARE_CONTROL(CRealEditControl);
 
public:
 	// Constructor & Destructor
 	CRealEditControl(CGXGridCore* pGrid, UINT nID);
 
protected:
 	// Overrides
 	BOOL GetControlText(CString& strResult, ROWCOL nRow, ROWCOL nCol, LPCTSTR pszRawValue, const CGXStyle& style);
 	BOOL ValidateString(const CString& sEdit);
 
 	// Generated message map functions
protected:
 	//{{AFX_MSG(CRealEditControl)
 	//}}AFX_MSG
 	DECLARE_MESSAGE_MAP()
};
 