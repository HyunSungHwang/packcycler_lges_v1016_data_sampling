// DlgUpdateReserve.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMonPro.h"
#include "DlgUpdateReserve.h"
#include "afxdialogex.h"



// CDlgUpdateReserve 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlgUpdateReserve, CDialog)

CDlgUpdateReserve::CDlgUpdateReserve(CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgUpdateReserve::IDD, pParent)
{
	m_pDoc = pDoc;
	m_bUpdateReserve = TRUE;
	m_bThreadFinish = FALSE;
}

CDlgUpdateReserve::~CDlgUpdateReserve()
{
}

void CDlgUpdateReserve::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_UPDATE_RESERVE, m_sUpdateReserve);
	DDX_Control(pDX, IDC_STATIC_RESERVE_STATE, m_sReserveState);
}


BEGIN_MESSAGE_MAP(CDlgUpdateReserve, CDialog)
	ON_BN_CLICKED(IDC_BTN_UPDATE_CANCEL, &CDlgUpdateReserve::OnBnClickedBtnUpdateCancel)
	ON_MESSAGE(WM_UPDATE_RESERVE_CNT, &CDlgUpdateReserve::OnUpdateReserveCnt)
	ON_MESSAGE(WM_UPDATE_RESERVE_FINISH, &CDlgUpdateReserve::OnUpdateReserveFinish)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CDlgUpdateReserve 메시지 처리기입니다.

BOOL CDlgUpdateReserve::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect crRect;

	GetWindowRect(&crRect);

	//::SetWindowPos(AfxGetMainWnd()->m_hWnd, HWND_TOPMOST, crRect.left, crRect.top, crRect.Width(), crRect.Height(), SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);

	//m_sReserveState.Format("시험 상태 확인중");
	m_sUpdateReserve.SetFontSize(28);
	m_sUpdateReserve.SetBkColor(RGB(255, 128, 0));
	m_sUpdateReserve.SetFontBold(TRUE);
	m_sUpdateReserve.SetText("자동 업데이트 예약중");
	m_sReserveState.SetFontSize(16);
	m_sReserveState.SetBkColor(RGB(255, 128, 0));
	m_sReserveState.SetText("시험 상태 확인중");

	AfxBeginThread(ThreadAutoUpdateReservation, this);

	UpdateData(FALSE);

	return TRUE; 
}

void CDlgUpdateReserve::OnBnClickedBtnUpdateCancel()
{
	SendMessage(WM_UPDATE_RESERVE_FINISH, NULL, NULL);


	//m_bUpdateReserve = FALSE;

	//Sleep(2000);

	//if (m_bUpdateReserve)
	//{
	//	GetDlgItem(IDC_BTN_UPDATE_CANCEL)->SetWindowText("종료");

	//	UpdateData(FALSE);
	//}
	//else
	//{
	//	//DestroyWindow();
	//	OnCancel();
	//}
}

void CDlgUpdateReserve::Finish()
{
	::SendMessage(this->m_hWnd, WM_CLOSE, NULL, NULL);
}

LRESULT CDlgUpdateReserve::OnUpdateReserveFinish(WPARAM wParam, LPARAM lParam)
{
	
	for (int i=0; i<5; i++)
	{
		Sleep(1000);
		m_bUpdateReserve = FALSE;
		if (m_bThreadFinish)	break;
	}

	::SendMessage(this->m_hWnd, WM_CLOSE, NULL, NULL);

	EndDialog(0); //lyj 20210722
	return 0;
}

BOOL CDlgUpdateReserve::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);
	
	pDC->FillSolidRect(rect, RGB(255, 128, 0));
	
	return TRUE;
}

// 210603 HKH 자동 업데이트 예약기능 
UINT CDlgUpdateReserve::ThreadAutoUpdateReservation(LPVOID pParam)
{
	CDlgUpdateReserve *pDlg = (CDlgUpdateReserve *)pParam;
	CCTSMonProDoc *pDoc = pDlg->m_pDoc;

	CCyclerModule *pMD = NULL;
	CCyclerChannel *pCH = NULL;
	CUIntArray arrModuleReady;

	BOOL bUpdateReady = FALSE;
	int nChRunCnt = 0;
	CString sMsg;

	sMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약 기능 실행");
	pDoc->WriteLog(sMsg);

	/*
	while (pDlg->m_bUpdateReserve)
	{
// 		for (int i=0; i<10; i++)
// 		{
// 			Sleep(1000);
// 
// 			if (pDlg->m_bUpdateReserve == FALSE)	break;
// 		}

		if (pDlg->m_bUpdateReserve == FALSE)
		{
			sMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약을 취소하였습니다.");
			pDoc->WriteLog(sMsg);
			
			pDlg->m_bThreadFinish = TRUE;

			return 0;
		}


		arrModuleReady.RemoveAll();

		//for (int nMdIndex = 0; nMdIndex < pDoc->GetInstallModuleCount(); nMdIndex++)
		for (int nMdIndex = 1; nMdIndex <= pDoc->GetInstallModuleCount(); nMdIndex++) //lyj 20210722
		{
			pMD = pDoc->GetModuleInfo(nMdIndex);

			nChRunCnt = 0;

			if (pMD)
			{
				for (int nChIndex = 0; nChIndex < pMD->GetTotalChannel(); nChIndex++)
				{
					pCH = pMD->GetChannelInfo(nChIndex); 

					if (pCH)
					{
						if (pCH->GetState() == PS_STATE_RUN)
						{
							nChRunCnt++;
						}
					}
				}
			}

			if (nChRunCnt > 0)	arrModuleReady.Add(nMdIndex);
		}

		if (arrModuleReady.GetSize() == 0)
		{
			bUpdateReady = TRUE;

			break;
		}
	}


	if (pDlg->m_bUpdateReserve == FALSE)
	{
		sMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약을 취소하였습니다.");
		pDoc->WriteLog(sMsg);

		pDlg->m_bThreadFinish = TRUE;

		return 0;
	}
	*/
	
	sMsg.Format("[AutoUpdaetReserve] 채널 동작 상태 확인 완료");
	pDoc->WriteLog(sMsg);

	for (int i=10; i>=0; i--)
	{
		if (pDlg->m_bUpdateReserve)
		{
			Sleep(1000);

			pDlg->SendMessage(WM_UPDATE_RESERVE_CNT, NULL, i);
		}
	}

	if (pDlg->m_bUpdateReserve == FALSE)
	{
		sMsg.Format("[AutoUpdaetReserve] 자동 업데이트 예약을 취소하였습니다.");
		pDoc->WriteLog(sMsg);

		pDlg->m_bThreadFinish = TRUE;

		return 0;
	}

	sMsg.Format("[AutoUpdaetReserve] 업데이트 시작");
	pDoc->WriteLog(sMsg);

	::ShellExecute(NULL, "open", "C:\\Program Files (x86)\\PNE CTSPack\\GuiUpdater.exe", NULL, NULL, SW_SHOW);

	pDlg->m_bThreadFinish = TRUE;

	pDlg->Finish();
	
	return 0;
}

LRESULT CDlgUpdateReserve::OnUpdateReserveCnt(WPARAM wParam, LPARAM lParam)
{
	//m_sReserveState.Format("%d초 뒤에 업데이트 예정", (int)lParam);
	CString sTemp;

	sTemp.Format("%d초 뒤에 업데이트 예정", (int)lParam);
	m_sReserveState.SetText(sTemp);

	UpdateData(FALSE);

	return 0;
}
