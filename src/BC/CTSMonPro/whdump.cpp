#include "stdafx.h"

#include "CTSMonPro.h"
#include "MainFrm.h"
#include "whdump.h"
#include <atlimage.h>

LPCTSTR MiniDumper::m_szAppName;

MiniDumper::MiniDumper( LPCTSTR szAppName )
{
    //m_szAppName = szAppName ? wcsdup(szAppName) : _T("Application");
	m_szAppName = szAppName ? strdup(szAppName) : _T("Application");

    ::SetUnhandledExceptionFilter( TopLevelFilter );
}

LONG MiniDumper::TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo )
{
    LONG retval = EXCEPTION_CONTINUE_SEARCH;
    HWND hParent = NULL;      // find a better value for your app

    // firstly see if dbghelp.dll is around and has the function we need
    // look next to the EXE first, as the one in System32 might be old
    // (e.g. Windows 2000)
    HMODULE hDll = NULL;
    //WCHAR szDbgHelpPath[_MAX_PATH];
	TCHAR szDbgHelpPath[_MAX_PATH];

    if (GetModuleFileName( NULL, szDbgHelpPath, _MAX_PATH ))
    {
        //WCHAR *pSlash = _tcsrchr( szDbgHelpPath, '\\' );
		TCHAR *pSlash = _tcsrchr( szDbgHelpPath, '\\' );
        if (pSlash)
        {
            _tcscpy( pSlash+1, _T("DBGHELP.DLL") );
            hDll = ::LoadLibrary( szDbgHelpPath );
        }
    }

    if (hDll==NULL)
    {
        // load any version we can
        hDll = ::LoadLibrary( _T("DBGHELP.DLL") );
    }

    LPCTSTR szResult = NULL;

    if (hDll)
    {
        MINIDUMPWRITEDUMP pDump = (MINIDUMPWRITEDUMP)::GetProcAddress( hDll, "MiniDumpWriteDump" );
        if (pDump)
        {
			TCHAR szDumpPath[_MAX_PATH];
			TCHAR szScratch [10000];


            // work out a good place for the dump file
            //if (!GetTempPath( _MAX_PATH, szDumpPath ))
            //    _tcscpy( szDumpPath, _T("c:\\temp\\") );

			CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd();						
			CCTSMonProDoc *pDoc = NULL;
			if(pMainFrame)
			{
				pDoc = (CCTSMonProDoc *)pMainFrame->GetActiveDocument();
			}		
		
			CString strTemp, strCrashDumpsRootPath, strCrashDumpsDatePath, strCrashDumpsPath, strPDBPath, strPDBCopyPath, strSysLogPath;
			CString strAppVer;
			CTime t = CTime::GetCurrentTime();
			TCHAR szTime[256];

			//((CCTSMonProApp*)AfxGetApp())->m_pMainWnd->GetWindowText(strAppVer);
			//ksj 20190306 : 프로그램 버전 정보 가져오기.
 			if(pMainFrame) strAppVer = pMainFrame->m_strVersion;
 			else strAppVer.Format("NONE");

			strTemp = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC ,_T("Path"));
			
			strSysLogPath.Format("%s\\log",strTemp); //프로그램 로그 경로			
			strCrashDumpsRootPath.Format(_T("C:\\PNE_Dumps"), strTemp); //덤프 폴더
			strCrashDumpsDatePath.Format(_T("%s\\%d-%02d-%02d"),  //덤프 하위 폴더 (날짜)
				strCrashDumpsRootPath, t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond());

			strCrashDumpsPath.Format(_T("%s\\%d-%02d-%02d_%02d%02d%02d_%s\\"), //덤프 하위 폴더 (프로그램명_시간)
				strCrashDumpsDatePath, t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond(),m_szAppName); 
			strPDBPath.Format(_T("%s\\PDB"), strTemp); //PDB 원본 위치

			strPDBCopyPath.Format("%s\\PDB" //PDB 복사 위치
				,strCrashDumpsDatePath, t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond());
			

			sprintf(szDumpPath, strCrashDumpsPath); //dump 위치
			
			if(pDoc)
			{
				//폴더 생성
				pDoc->ForceDirectory(strCrashDumpsRootPath);
				pDoc->ForceDirectory(strCrashDumpsDatePath);
				pDoc->ForceDirectory(strCrashDumpsPath);
			}
			
							

			//swprintf(szTime,_T("_minidump_%d%02d%02d_%02d%02d%02d"),t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond());
			sprintf(szTime,_T("_minidump_%d%02d%02d_%02d%02d%02d"),t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond());
		
            _tcscat( szDumpPath, m_szAppName );
			_tcscat( szDumpPath, szTime );
            _tcscat( szDumpPath, _T(".dmp") );

            // ask the user if they want to save a dump file
            //if (::MessageBox( NULL, _T("Something bad happened in your program, would you like to save a diagnostic file?"), m_szAppName, MB_YESNO )==IDYES) //ksj 20171031 : 묻지 않고 덤프하도록 주석처리.
            {
                // create the file
                HANDLE hFile = ::CreateFile( szDumpPath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS,
                    FILE_ATTRIBUTE_NORMAL, NULL );

                if (hFile!=INVALID_HANDLE_VALUE)
                {
                    _MINIDUMP_EXCEPTION_INFORMATION ExInfo;

                    ExInfo.ThreadId = ::GetCurrentThreadId();
                    ExInfo.ExceptionPointers = pExceptionInfo;
                    ExInfo.ClientPointers = NULL;

                    // write the dump
                    BOOL bOK = pDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL );
                    if (bOK)
                    {
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
sprintf( szScratch, _T("CTS 프로그램에 잠시 알 수 없는 오류가 있었습니다.\n\
발생 빈도에 따라 프로그램 또는 PC 점검이 필요할 수 있습니다.\n\
An unknown error has occurred in The CTS program.\n\
If it occurs frequently, a program or PC check may be required.\n\n\
Please send this information and the following files to PNE to help you analyze the problem.\n\n\
================ INFORMATION ===============\n\
Event Time : %d-%02d-%02d %02d:%02d:%02d\n\
Program Version : %s\n\n\
## Error Log Folder (Please send this folder) ##\n\
\"%s\"\n\n\n\
#Details\n\
Program Database(PDB) : \"%s\"\n\
CrashDumps : \"%s\"\n\
Saved dump file to \"%s\"\n\
============================================")

, t.GetYear(), t.GetMonth(), t.GetDay(), t.GetHour(), t.GetMinute(), t.GetSecond()
, strAppVer
, strCrashDumpsDatePath
, strPDBCopyPath
, strCrashDumpsPath
, szDumpPath 
);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        szResult = szScratch;
                        retval = EXCEPTION_EXECUTE_HANDLER;

						if(pDoc) //ksj 20190306 : 로그 남기기.
						{
							pDoc->WriteSysLog(szScratch);
						}
						
						//ksj 20171031 : 별도의 정보 파일 남기기.
						CString strInfoFile;
						strInfoFile.Format(_T("%s.txt"),szDumpPath);
						FILE* fp = fopen(CStringA(strInfoFile),"wt");
						//AfxMessageBox(strInfoFile);

						if(fp)
						{
							fprintf(fp,CStringA(szScratch));
							fclose(fp);

							BOOL bShowError = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Show Crash Error Txt", FALSE);
							if(bShowError)
							{
								//ksj 20190306 : 프로그램 크래쉬난 경우 메모장으로 정보 로그를 띄운다.
								ShellExecute(NULL, "open", "notepad", strInfoFile, NULL, SW_SHOW);
							}							
						}
						//ksj end
                    }
                    else
                    {
                        //swprintf( szScratch, _T("Failed to save dump file to '%s' (error %d)"), szDumpPath, GetLastError() );
						sprintf( szScratch, _T("Failed to save dump file to '%s' (error %d)"), szDumpPath, GetLastError() );
                        szResult = szScratch;
                    }
                    ::CloseHandle(hFile);
                }
                else
                {
                    //swprintf( szScratch, _T("Failed to create dump file '%s' (error %d)"), szDumpPath, GetLastError() );
					sprintf( szScratch, _T("Failed to create dump file '%s' (error %d)"), szDumpPath, GetLastError() );
                    szResult = szScratch;
                }
            }


			//ksj 20210728 : 프로그램 종료시 각종 추가 정보 복사 및 생성.
			if(pDoc)
			{	
				//덤프 폴더에 로그등 추가 정보 파일 복사.
				//최종 로그 데이터 복사
				CString strSrcFile, strDestFile;
				strSrcFile.Format("%s\\%d%02d%02d.log",strSysLogPath, t.GetYear(),t.GetMonth(),t.GetDay());
				strDestFile.Format("%s%d%02d%02d.log",strCrashDumpsPath, t.GetYear(),t.GetMonth(),t.GetDay());
				CopyFile(strSrcFile,strDestFile, FALSE);

				//로그 날짜 폴더 복사
				strSrcFile.Format("%s\\%d-%02d-%02d",strSysLogPath, t.GetYear(),t.GetMonth(),t.GetDay());
				strDestFile.Format("%s%d-%02d-%02d",strCrashDumpsPath, t.GetYear(),t.GetMonth(),t.GetDay());
				pDoc->CopyDirectory(strSrcFile,strDestFile);
			
				//최종 PDB 복사
				pDoc->CopyDirectory(strPDBPath,strPDBCopyPath);

				//향후에 가능하면 SBC 로그도 다운로드?
				//프로그램 죽는것은 SBC랑 인과관계가 낮아.. 굳이.. 필요하진 않음.

				//ksj 20210728 : 현재 화면 캡쳐 저장
				strDestFile.Format(_T("%s.png"),szDumpPath);
				HDC h_dc = ::GetWindowDC(NULL);
				CImage img;
				int cx = ::GetSystemMetrics(SM_CXSCREEN);
				int cy = ::GetSystemMetrics(SM_CYSCREEN);
				int color_depth = ::GetDeviceCaps(h_dc, BITSPIXEL);
				img.Create(cx, cy, color_depth, 0);

				::BitBlt(img.GetDC(), 0, 0, cx, cy, h_dc, 0, 0, SRCCOPY);
				img.Save(strDestFile, Gdiplus::ImageFormatPNG);

				::ReleaseDC(NULL, h_dc);
				img.ReleaseDC();				
			}

			//ksj 20210728 : 프로그램 죽기전에 다시 CTSMonitorPro.exe 를 실행시키는 프로세스를 실행시켜서
			//프로그램이 다시 자동 실행 될 수 있도록 한다.
			BOOL bRelaunch = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use relauncher in crash", TRUE);
			if(bRelaunch)
			{
				ShellExecute(NULL, "open", "CTSRelauncher.exe", "", NULL, SW_SHOW);
			}
        }
        else
        {
            szResult = _T("DBGHELP.DLL too old");
        }
    }
    else
    {
        szResult = _T("DBGHELP.DLL not found");
    }

	//팝업 띄우지 않고 프로그램 종료
//     if (szResult)
//         ::MessageBox( NULL, szResult, m_szAppName, MB_ICONWARNING|MB_OK );

    return retval;
}