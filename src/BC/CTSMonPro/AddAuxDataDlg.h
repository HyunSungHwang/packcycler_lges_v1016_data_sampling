#if !defined(AFX_ADDAUXDATADLG_H__CAFBF078_4DFA_4AEF_9954_2BD40F605575__INCLUDED_)
#define AFX_ADDAUXDATADLG_H__CAFBF078_4DFA_4AEF_9954_2BD40F605575__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddAuxDataDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAddAuxDataDlg dialog



class CAddAuxDataDlg : public CDialog
{
// Construction
public:
	float m_fTempLow;// = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxTempLow", 0);			//20150811 ljb
	float m_fTempHigh;// = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxTempHigh", 80);			//20150811 ljb
	float m_fVoltageLow;// = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxVolLow", 1500);		//20150811 ljb
	float m_fVoltageHigh;// = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "AuxVolHigh", 4300);		//20150811 ljb
	float m_fMaxValue;
	float m_fMinValue;
	BOOL	m_bLogin;		//Check permission
	

	int Fun_GetFunctionAuxPos(int nCode);
	int Fun_GetFunctionAuxThrPos(int nCode);
	void Fun_SetArryAuxDivision(CUIntArray & uiArryAuxCode);	//ljb 2011222 ljb //////////
	void Fun_SetArryAuxString(CStringArray & strArryAuxName);	//ljb 2011222 ljb //////////

	void Fun_SetArryAuxThrDivision(CUIntArray & uiArryAuxCode);	//ljb 20160504 ljb //////////
	void Fun_SetArryAuxThrString(CStringArray & strArryAuxName);//ljb 20160504 ljb //////////

	void Fun_SetArryAuxHumiDivision(CUIntArray & uiArryAuxHumiCode); //ksj 20200207
	void Fun_SetArryAuxHumiString(CStringArray & strArryAuxHumiName); //ksj 20200207

	void InitFunCombo();
	void InitList();
	BOOL bModify;
	void UpdateList();
	int nSensorType;
	int nSensorCh;
	int nSensorFunction1;
	int nSensorFunction2;
	int nSensorFunction3;
	int nSensorAuxthrType;		//ljb 20160504 add
	CAddAuxDataDlg(CWnd * pParent = NULL, CCyclerModule * pMD = NULL);   // standard constructor
	CCyclerModule * pMD;

	CUIntArray m_uiArryAuxCode;		//ljb 2011222 ljb //////////
	CStringArray m_strArryAuxName;	//ljb 2011222 ljb //////////
	CUIntArray m_uiArryAuxThrCode;		//ljb 20160504 ljb //////////
	CStringArray m_strArryAuxThrName;	//ljb 20160504 ljb //////////

	CUIntArray m_uiArryAuxHumiCode;		//ksj 20200207
	CStringArray m_strArryAuxHumiName;	//ksj 20200207



// Dialog Data
	//{{AFX_DATA(CAddAuxDataDlg)
	enum { IDD = IDD_ADD_AUXDATA , IDD2 = IDD_ADD_AUXDATA_ENG , IDD3 = IDD_ADD_AUXDATA_PL };
	CComboBox	m_ctrlSensorAuxThrType;
	CComboBox	m_ctrlSensorFUNC3;
	CComboBox	m_ctrlSensorFUNC2;
	CEdit	m_ctrlSensName;
	CListCtrl	m_ctrlSensorList;
	CComboBox	m_ctrlSensorCB;
	CComboBox	m_ctrlSensorFUNC1;
	CString	m_strSensorName;
	CString	m_strEndReq;
	float	m_fSensorMin;
	float	m_fSensorMAx;
	float	m_fEndMin;
	float	m_fEndMax;
	BOOL	m_bUseVentSafety;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddAuxDataDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAddAuxDataDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeComboSensorType();
	afx_msg void OnEditchangeComboSensorFunction();
	afx_msg void OnSetfocusEditMaxValue();
	afx_msg void OnSetfocusEditMinValue();
	afx_msg void OnKillfocusEditMaxValue();
	afx_msg void OnKillfocusEditMinValue();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnCancel();
	afx_msg void OnKillfocusEditEndMaxValue();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	// ksj 20200116 : V1016, Vent 연동 기능 추가
	BOOL m_bUseVentLink;
	void InitRtTableCombo(void);
	int InitSensorSelectCombo(void);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDAUXDATADLG_H__CAFBF078_4DFA_4AEF_9954_2BD40F605575__INCLUDED_)
