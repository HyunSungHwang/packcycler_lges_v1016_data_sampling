// CalDataExportDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CalDataExportDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalDataExportDlg dialog


CCalDataExportDlg::CCalDataExportDlg(CWnd* pParent /*=NULL*/)
//	: CDialog(CCalDataExportDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCalDataExportDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCalDataExportDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CCalDataExportDlg::IDD3):
	(CCalDataExportDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CCalDataExportDlg)
	m_bVertial = FALSE;
	m_bHorizental = TRUE;
	//}}AFX_DATA_INIT
}


void CCalDataExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCalDataExportDlg)
	DDX_Check(pDX, IDC_VERTIAL_CHECK, m_bVertial);
	DDX_Check(pDX, IDC_HORIZENTAL_CHECK, m_bHorizental);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCalDataExportDlg, CDialog)
	//{{AFX_MSG_MAP(CCalDataExportDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCalDataExportDlg message handlers

void CCalDataExportDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	UpdateData(TRUE);

	if(m_bVertial == FALSE && m_bHorizental == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg","IDD_CAL_REPORT_DIALOG"));
		//@ AfxMessageBox("저장 형태를 선택 하십시요");
		return;
	}

	CDialog::OnOK();
}