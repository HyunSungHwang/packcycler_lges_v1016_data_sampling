// SettingCanChangeDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "SettingCanChangeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSettingCanChangeDlg dialog


CSettingCanChangeDlg::CSettingCanChangeDlg(CCTSMonProDoc * pDoc, CWnd* pParent /*=NULL*/)
//	: CDialog(CSettingCanChangeDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSettingCanChangeDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSettingCanChangeDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSettingCanChangeDlg::IDD3):
	(CSettingCanChangeDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSettingCanChangeDlg)
	m_fCanValue_1 = 0.0f;
	m_fCanValue_2 = 0.0f;
	m_fCanValue_3 = 0.0f;
	m_fCanValue_4 = 0.0f;
	m_fCanValue_5 = 0.0f;
	m_fCanValue_6 = 0.0f;
	m_fCanValue_7 = 0.0f;
	m_fCanValue_8 = 0.0f;
	m_fCanValue_9 = 0.0f;
	m_fCanValue_10 = 0.0f;
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	nLastPos=0;

}


void CSettingCanChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingCanChangeDlg)
	DDX_Control(pDX, IDC_CBO_CAN_NAME_1, m_ctrlCanName1);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_2, m_ctrlCanName2);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_3, m_ctrlCanName3);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_4, m_ctrlCanName4);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_5, m_ctrlCanName5);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_6, m_ctrlCanName6);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_7, m_ctrlCanName7);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_8, m_ctrlCanName8);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_9, m_ctrlCanName9);
	DDX_Control(pDX, IDC_CBO_CAN_NAME_10,m_ctrlCanName10);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_1, m_fCanValue_1);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_2, m_fCanValue_2);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_3, m_fCanValue_3);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_4, m_fCanValue_4);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_5, m_fCanValue_5);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_6, m_fCanValue_6);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_7, m_fCanValue_7);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_8, m_fCanValue_8);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_9, m_fCanValue_9);
	DDX_Text(pDX, IDC_EDIT_CAN_VALUE_10, m_fCanValue_10);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSettingCanChangeDlg, CDialog)
	//{{AFX_MSG_MAP(CSettingCanChangeDlg)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_1, OnSelchangeCboCanName1)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_2, OnSelchangeCboCanName2)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_3, OnSelchangeCboCanName3)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_4, OnSelchangeCboCanName4)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_5, OnSelchangeCboCanName5)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_6, OnSelchangeCboCanName6)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_7, OnSelchangeCboCanName7)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_8, OnSelchangeCboCanName8)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_9, OnSelchangeCboCanName9)
	ON_CBN_SELCHANGE(IDC_CBO_CAN_NAME_10, OnSelchangeCboCanName10)
	ON_BN_CLICKED(IDC_BUT_SEND, OnButSend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingCanChangeDlg message handlers

BOOL CSettingCanChangeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdateGridData();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSettingCanChangeDlg::UpdateGridData()
{
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;

	CString strTemp;
	
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaster, nSlave;
	int nLoopCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if (nLoopCnt < 1)
	{
		AfxMessageBox(Fun_FindMsg("UpdateGridData_msg1","IDD_CAN_COMMAND_CHANGE"));
		//@ AfxMessageBox("CAN 설정값이 없습니다.");
		return;
	}

	for( nMaster = 0; nMaster < nLoopCnt ; nMaster++)
	{
//		ZeroMemory(canData,sizeof(SFT_CAN_SET_TRANS_DATA));
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nMaster );
		strTemp.Format("%s", canData.name);
		if (strTemp == "$")
		{
			break;
			nMaster--;
		}
		m_ctrlCanName1.AddString(strTemp);
		m_ctrlCanName1.SetItemData(nMaster,nMaster);
		m_ctrlCanName2.AddString(strTemp);
		m_ctrlCanName2.SetItemData(nMaster,nMaster);
		m_ctrlCanName3.AddString(strTemp);
		m_ctrlCanName3.SetItemData(nMaster,nMaster);
		m_ctrlCanName4.AddString(strTemp);
		m_ctrlCanName4.SetItemData(nMaster,nMaster);
		m_ctrlCanName5.AddString(strTemp);
		m_ctrlCanName5.SetItemData(nMaster,nMaster);
		m_ctrlCanName6.AddString(strTemp);
		m_ctrlCanName6.SetItemData(nMaster,nMaster);
		m_ctrlCanName7.AddString(strTemp);
		m_ctrlCanName7.SetItemData(nMaster,nMaster);
		m_ctrlCanName8.AddString(strTemp);
		m_ctrlCanName8.SetItemData(nMaster,nMaster);
		m_ctrlCanName9.AddString(strTemp);
		m_ctrlCanName9.SetItemData(nMaster,nMaster);
		m_ctrlCanName10.AddString(strTemp);
		m_ctrlCanName10.SetItemData(nMaster,nMaster);

// 		strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
// 		GetDlgItem(IDC_LAB_DIVISION_1)->SetWindowText(strTemp);
// 		strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
// 		GetDlgItem(IDC_LAB_DIVISION_1)->SetWindowText(strTemp);
	}
	
	nLoopCnt = pMD->GetInstalledCanTransSlave(nSelCh);
	for( nSlave = 0; nSlave < nLoopCnt ; nSlave++)
	{
//		ZeroMemory(canData,sizeof(SFT_CAN_SET_TRANS_DATA));
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSlave);
		strTemp.Format("%s", canData.name);
		if (strTemp == "$")
		{
			break;
			nSlave--;
		}
		m_ctrlCanName1.AddString(strTemp);
		m_ctrlCanName1.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName2.AddString(strTemp);
		m_ctrlCanName2.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName3.AddString(strTemp);
		m_ctrlCanName3.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName4.AddString(strTemp);
		m_ctrlCanName4.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName5.AddString(strTemp);
		m_ctrlCanName5.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName6.AddString(strTemp);
		m_ctrlCanName6.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName7.AddString(strTemp);
		m_ctrlCanName7.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName8.AddString(strTemp);
		m_ctrlCanName8.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName9.AddString(strTemp);
		m_ctrlCanName9.SetItemData(nMaster+nSlave,nMaster+nSlave);
		m_ctrlCanName10.AddString(strTemp);
		m_ctrlCanName10.SetItemData(nMaster+nSlave,nMaster+nSlave);
	}

	nLastPos = nMaster+nSlave;
	strTemp = Fun_FindMsg("UpdateGridData_msg2","IDD_CAN_COMMAND_CHANGE");
	//@ strTemp = "=== 선택 ===";
	m_ctrlCanName1.AddString(strTemp);
	m_ctrlCanName2.AddString(strTemp);
	m_ctrlCanName3.AddString(strTemp);
	m_ctrlCanName4.AddString(strTemp);
	m_ctrlCanName5.AddString(strTemp);
	m_ctrlCanName6.AddString(strTemp);
	m_ctrlCanName7.AddString(strTemp);
	m_ctrlCanName8.AddString(strTemp);
	m_ctrlCanName9.AddString(strTemp);
	m_ctrlCanName10.AddString(strTemp);
	m_ctrlCanName1.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName2.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName3.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName4.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName5.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName6.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName7.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName8.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName9.SetItemData(nLastPos,nLastPos);
	m_ctrlCanName10.SetItemData(nLastPos,nLastPos);
	
	m_ctrlCanName1.SetCurSel(nLastPos);
	m_ctrlCanName2.SetCurSel(nLastPos);
	m_ctrlCanName3.SetCurSel(nLastPos);
	m_ctrlCanName4.SetCurSel(nLastPos);
	m_ctrlCanName5.SetCurSel(nLastPos);
	m_ctrlCanName6.SetCurSel(nLastPos);
	m_ctrlCanName7.SetCurSel(nLastPos);
	m_ctrlCanName8.SetCurSel(nLastPos);
	m_ctrlCanName9.SetCurSel(nLastPos);
	m_ctrlCanName10.SetCurSel(nLastPos);

//	canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, 0);
//	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	strTemp = " 000 000 000";
	GetDlgItem(IDC_LAB_DIVISION_1)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_2)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_3)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_4)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_5)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_6)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_7)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_8)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_9)->SetWindowText(strTemp);
	GetDlgItem(IDC_LAB_DIVISION_10)->SetWindowText(strTemp);
	
// 	m_fCanValue_1 = canData.default_fValue;
// 	m_fCanValue_2 = canData.default_fValue;
// 	m_fCanValue_3 = canData.default_fValue;
// 	m_fCanValue_4 = canData.default_fValue;
// 	m_fCanValue_5 = canData.default_fValue;
// 	m_fCanValue_6 = canData.default_fValue;
// 	m_fCanValue_7 = canData.default_fValue;
// 	m_fCanValue_8 = canData.default_fValue;
// 	m_fCanValue_9 = canData.default_fValue;
// 	m_fCanValue_10 = canData.default_fValue;
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName1() 
{
	int nSelCombo;
	if(m_ctrlCanName1.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName1.GetItemData(m_ctrlCanName1.GetCurSel());
	}
	else
		return;

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_1 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_1)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName2() 
{
	int nSelCombo;
	if(m_ctrlCanName2.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName2.GetItemData(m_ctrlCanName2.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_2 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_2)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName3() 
{
	int nSelCombo;
	if(m_ctrlCanName3.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName3.GetItemData(m_ctrlCanName3.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_3 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_3)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName4() 
{
	int nSelCombo;
	if(m_ctrlCanName4.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName4.GetItemData(m_ctrlCanName4.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_4 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_4)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName5() 
{
	int nSelCombo;
	if(m_ctrlCanName5.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName5.GetItemData(m_ctrlCanName5.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_5 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_5)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName6() 
{
	int nSelCombo;
	if(m_ctrlCanName6.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName6.GetItemData(m_ctrlCanName6.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_6 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_6)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName7() 
{
	int nSelCombo;
	if(m_ctrlCanName7.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName7.GetItemData(m_ctrlCanName7.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_7 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_7)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName8() 
{
	int nSelCombo;
	if(m_ctrlCanName8.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName8.GetItemData(m_ctrlCanName8.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_8 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_8)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName9() 
{
	int nSelCombo;
	if(m_ctrlCanName9.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName9.GetItemData(m_ctrlCanName9.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_9 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_9)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnSelchangeCboCanName10() 
{
	int nSelCombo;
	if(m_ctrlCanName10.GetCurSel() != LB_ERR)
	{
		nSelCombo = m_ctrlCanName10.GetItemData(m_ctrlCanName10.GetCurSel());
	}
	else
		return;
	
	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);	
	if(pMD == NULL)	return;
	
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);
	if ( nSelCombo <= nMaxSetCnt)
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
	}
	else
	{
		canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
	}
	m_fCanValue_10 = 0;
	UpdateData();
	strTemp.Format("%03d %03d %03d",canData.function_division, canData.function_division2, canData.function_division3);
	GetDlgItem(IDC_LAB_DIVISION_10)->SetWindowText(strTemp);
	UpdateData(FALSE);
}

void CSettingCanChangeDlg::OnButSend() 
{

	CCyclerModule * pMD;		
	pMD = m_pDoc->GetModuleInfo(nSelModule);
	if(pMD == NULL)	return;

	CCyclerChannel * pChInfo = pMD->GetChannelInfo(nSelCh);
	if(pChInfo == NULL) return;
	
	SFT_SEND_CMD_CAN_CHANGE_SET canChangeSetData;	//ljb 201009 CAN 변경 설정값 
	ZeroMemory(&canChangeSetData, sizeof(SFT_SEND_CMD_CAN_CHANGE_SET));

	int nPos=0;
	int nSelCombo;
	CString strTemp;
	SFT_CAN_SET_TRANS_DATA canData;
	int nMaxSetCnt = pMD->GetInstalledCanTransMaster(nSelCh);

	UpdateData();
	if(m_ctrlCanName1.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName1.GetItemData(m_ctrlCanName1.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_1;
		nPos++;
	}
	if(m_ctrlCanName2.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName2.GetItemData(m_ctrlCanName2.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_2;
		nPos++;
	}
	if(m_ctrlCanName3.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName3.GetItemData(m_ctrlCanName3.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_3;
		nPos++;
	}
	if(m_ctrlCanName4.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName4.GetItemData(m_ctrlCanName4.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_4;
		nPos++;
	}
	if(m_ctrlCanName5.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName5.GetItemData(m_ctrlCanName5.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_5;
		nPos++;
	}
	if(m_ctrlCanName6.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName6.GetItemData(m_ctrlCanName6.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_6;
		nPos++;
	}
	if(m_ctrlCanName7.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName7.GetItemData(m_ctrlCanName7.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_7;
		nPos++;
	}
	if(m_ctrlCanName8.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName8.GetItemData(m_ctrlCanName8.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_8;
		nPos++;
	}
	if(m_ctrlCanName9.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName9.GetItemData(m_ctrlCanName9.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_9;
		nPos++;
	}
	if(m_ctrlCanName10.GetCurSel() != LB_ERR) 
		nSelCombo = m_ctrlCanName10.GetItemData(m_ctrlCanName10.GetCurSel());
	else 
		return;
	if ( nSelCombo < nLastPos )
	{
		if ( nSelCombo <= nMaxSetCnt)
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_MASTER, nSelCombo);
		else
			canData = pMD->GetCANSetTransData(nSelCh, PS_CAN_TYPE_SLAVE, nSelCombo);
		canChangeSetData.canChangeData[nPos].canID = canData.canID;
		canChangeSetData.canChangeData[nPos].function_division = canData.function_division;
		canChangeSetData.canChangeData[nPos].function_division2 = canData.function_division2;
		canChangeSetData.canChangeData[nPos].function_division3 = canData.function_division3;
		canChangeSetData.canChangeData[nPos].canType = canData.canType;
		canChangeSetData.canChangeData[nPos].startBit = canData.startBit;
		canChangeSetData.canChangeData[nPos].default_fValue = m_fCanValue_10;
	}

	if(int nRth = m_pDoc->SetCANChangeDataToModule(nSelModule, &canChangeSetData, nSelCh) == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("OnButSend_msg","IDD_CAN_COMMAND_CHANGE"));
		//@ AfxMessageBox("CAN Trnas Change 실패.");
	}	
}