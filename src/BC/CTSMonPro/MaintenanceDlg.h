#if !defined(AFX_MAINTENANCEDLG_H__99724BD8_3885_448D_8438_F4811F0911A3__INCLUDED_)
#define AFX_MAINTENANCEDLG_H__99724BD8_3885_448D_8438_F4811F0911A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MaintenanceDlg.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CMaintenanceDlg dialog
#include "CTSMonProDoc.h"
#include "XPButton.h"
 
class CMaintenanceDlg : public CDialog
{
// Construction
public:
	BOOL m_bChanged;
	void UpdateModuleList();
	CMaintenanceDlg(CCTSMonProDoc *pDoc, bool bMaintenanceMode = true, CWnd* pParent = NULL);   // standard constructor
	~CMaintenanceDlg();

// Dialog Data
	//{{AFX_DATA(CMaintenanceDlg)
	enum { IDD = IDD_MAINTENANCE_DLG , IDD2 = IDD_MAINTENANCE_DLG_ENG , IDD3 = IDD_MAINTENANCE_DLG_PL };
	CXPButton	m_btnCancel;
	CXPButton	m_btnOK;
	CXPButton	m_btnDelete;
	CXPButton	m_btnAdd;
	CListCtrl		m_wndChannelList;
	CListCtrl		m_wndModuleList;
	CIPAddressCtrl	m_ctrlServerAddress;
	int				m_nRackID;
	BOOL	m_bUseAutoRestarting;
	int		m_nWaitTimeForAutoRestart;
	int		m_nInstalledModuleNum;
	//}}AFX_DATA
	int*			m_pChannelNum;
	int				m_nSetDPItemNum;
	int				m_nChannelDPItemNum;
	CImageList*		m_pChStsSmallImage;
	BYTE*			m_pSelectedItem;
	int				m_nCurSelIndex;
	bool			m_bMaintenanceMode;
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaintenanceDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	void OnUpdateChannelColString(BOOL bUpdate, CString strData = _T(""));
// Implementation
protected:
	BOOL AddModule(int nModuleID);
	BOOL DeleteModule(int nModuleID);
	CCTSMonProDoc  *m_pDoc;

	// Generated message map functions
	//{{AFX_MSG(CMaintenanceDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBtnAdd();
	afx_msg void OnBtnDelete();
	afx_msg void OnRclickListChannelDpItem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkListChannelDpItem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnAutorestart();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINTENANCEDLG_H__99724BD8_3885_448D_8438_F4811F0911A3__INCLUDED_)
