#if !defined(AFX_CALIBRATORDLG_H__C028F11C_5329_4D8D_8DFD_8E8DD8AA3CBB__INCLUDED_)
#define AFX_CALIBRATORDLG_H__C028F11C_5329_4D8D_8DFD_8E8DD8AA3CBB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalibratorDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCalibratorDlg dialog

typedef struct
{
	UINT	nModuleID;
	BYTE	byChSelIndex;		//선택된 채널 Array 변수에서 현재 Index
	BYTE	byTotalPoint;
	SFT_CALI_START_INFO	aCaliSet;
} S_BATCH_CALI_DATA;

#define		CAL_MODE_CALIBRATION	0
#define		CAL_MODE_CAL_CHECK		1

#define		CAL_VOLTAGE		0
#define		CAL_CURRENT		1
#define		CAL_CHARGE		0//lmh 20120505 add
#define		CAL_DISCHARGE	4//lmh 20120505 add

#define		CAL_SINGLE_RANGE	0
#define		CAL_ALL_RANGE	1
#define		CAL_ALL_ITEM_RANGE	2

#define		CAL_EXT_SHUNT	0
#define		CAL_METER_SHUNT	1

#define		CAL_TIMER_INTERVAL	200
#define		CAL_TIME_OUT_COUNT	150	 //150*200msec=30sec time out

//class CCTSMonProView;
class CCTSMonProDoc;
#include "XPButton.h"
#include "AccuracySetDlg.h"

class CCalibratorDlg : public CDialog
{
// Construction
public:
	BOOL m_bCaliFirstComm;	//ljb 20151222 add //ksj 20200819 : 자동교정 기능 이식
	BOOL	Fun_CalibrationStart(int nNewModuleID); //ksj 20200819 : 자동교정 기능 이식
	BOOL	Fun_CalibrationEnd(int nNewModuleID); //ksj 20200819 : 자동교정 기능 이식

	BOOL FindNextRangeInfo(int nCurType, WORD wCurRnage, int  &nNextType, WORD &wNextRange);
	BOOL SendNextCalInfo();
	BOOL ExportVerticalReport(CString strFileName);
	BOOL IsEditedCalData(int nModuleID);
//	BOOL UpdateBatchData(int nType, WORD wRange, int nMode = CAL_SINGLE_RANGE);
	BOOL SendCalStart(UINT nChIndex);
	void InitCalSelChList(int nNewModuleID);
	void InitTree();
	void UpdateCalDataList();
	CCalibratorDlg(int nModuleID, CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor
	~CCalibratorDlg();
// Dialog Data
	//{{AFX_DATA(CCalibratorDlg)
	enum { IDD = IDD_CALIBRATION_DLG , IDD2 = IDD_CALIBRATION_DLG_ENG , IDD3 = IDD_CALIBRATION_DLG_PL };
	CXPButton	m_btnAccurayBtm;
	CXPButton	m_btnSaveFile;
	CXPButton	m_btnSelect;
	CXPButton	m_btnUpdateAll;
	CXPButton	m_btnStop;
	CXPButton	m_btnAllItem;
	CXPButton	m_btnRange1;
	CXPButton	m_btnVoltage;
	CXPButton	m_btnConnect;
	CXPButton	m_btnSetPoint;
	CXPButton	m_btnOK;
	CComboBox	m_ctrlSelDataTypeCombo;
	CProgressCtrl	m_ctrlProgress;
	CListCtrl	m_CalChSelList;
	CComboBox	m_ModuleCombo;
	CComboBox	m_wndTotalPoint;
	CListCtrl	m_wndTotal;
	CListCtrl	m_wndCheck;
	int		m_nTotalMode;
	CImageList* m_pCaliResultImage;
	//}}AFX_DATA

	CCTSMonProDoc * m_pDoc;
	CWordArray	m_aCaliSelChArray;
	CImageList	m_stateImage;
//	CImageList	m_VIImage;

	bool m_bCalStatus;
	int m_nChNoFirst;
	int m_nChNoLast;
//	int m_nModuleIndex;
	int m_nModuleID;
	int m_nSockModuleID;
	int m_nSelectedChNum;
	int m_nTimerArrow;

	DWORD	m_dwElapsedTime;		//1채널 교정하는데 걸리는 평균 Timer 횟수(200ms 횟수) 
//	DWORD	m_dwRemainTime;

	bool m_bDirection;
	
//	CAL_POINT* m_pCalPoint;
//	CAL_POINT* m_pCheckPoint;

	CPtrArray m_aCalResultData1;
	CPtrArray m_aCalResultData2;
	CPtrArray m_aCalResultData3;

	int m_nPresNo;

//	LP_CHANNEL_CAL_DATA m_pChannelData;

	S_BATCH_CALI_DATA m_aBatchCaliData;

	BOOL SendUpdateCmd(int nModuleID, CWordArray *pSelChArray);
	void SetPointData();
//	void ManualCalibration(BYTE byRange, int nChannelIndex);

	void SetReceiveResultData(LPSFT_CALI_END_DATA pReceivedCalData);

//	void SetConfigurationData(CAL_POINT* pCalPoint, 
//							  CAL_POINT* pCheckPoint, 
//							  int nModuleIndex, 
//							  BYTE* pPossibleChannelList,
//							  int nPossibleChannelNum);
	void OnCommConnected();
	void OnCommDisconnected();
	void OnCalDataModified();

	void BeginWaitState();
	void EndWaitState();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalibratorDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	int m_nStateWait;
	BOOL StartCalibration(int nModuleID, int nType, WORD wRange, int nMode);
	int m_nCalMode;
	int m_nChargeDisChargeMode;//lmh 20120505
	int m_nSelectedRange;//lmh 20120504
	BOOL UpdateBatchCalPointData(int nType, WORD wRange);
	BOOL ExportHorizentalReport(CString strFileName);

//	float m_fVtgDAErrorSet;
//	float m_fVtgADErrorSet;
//	float m_fCrtDAErrorSet;
//	float m_fCrtADErrorSet;

	CMenu m_Rangemenu; 
	CMenu m_SubMenu;//lmh 20120504 add
	int m_nAVGOnePointCalTime;
	int m_nTimeOutCount;
	BOOL m_bStopFlag;
	BOOL UpdateChCaliData(LPSFT_CALI_END_DATA pReceivedCalData);
	void UpdateMDStateList(int nChIndex = -1);
	void SetReceivedCheckData(int nModuleID, int nChIndex);

	// Generated message map functions
	//{{AFX_MSG(CCalibratorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnBtnRange1();
	afx_msg void OnBtnSetPoint();
	afx_msg void OnBtnConnect();
	afx_msg void OnBtnUpdateSel();
	afx_msg void OnBtnUpdateAll();
	afx_msg void OnSelchangeComboDpMode();
	afx_msg void OnTimer(UINT_PTR  nIDEvent);
	afx_msg void OnRclickListTotal(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCaliRange1();
	afx_msg void OnCaliRange2();
	afx_msg void OnCaliRange3();
	virtual void OnOK();
	afx_msg void OnBtnVoltage();
	afx_msg void OnBtnSaveFile();
	afx_msg void OnSelchangeModuleSelCombo();
	afx_msg void OnItemchangedCalChList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnStopButton();
	afx_msg void OnGetdispinfoCalChList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCaliRangeAll();
	afx_msg void OnUpdateCaliRangeAll(CCmdUI* pCmdUI);
	afx_msg void OnAllItemButton();
	afx_msg void OnCaliRange4();
	afx_msg void OnCaliRange5();
	afx_msg void OnSelchangeCombo3();
	afx_msg void OnAccuracyButton();
	afx_msg void OnChargeRange();
	afx_msg void OnDisChargeRange();
	afx_msg void OnChargeRange2();
	afx_msg void OnDisChargeRange2();
	afx_msg void OnChargeRange3();
	afx_msg void OnDisChargeRange3();
	afx_msg void OnChargeRange4();
	afx_msg void OnDisChargeRange4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	WORD m_nCalibrationItem;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALIBRATORDLG_H__C028F11C_5329_4D8D_8DFD_8E8DD8AA3CBB__INCLUDED_)
