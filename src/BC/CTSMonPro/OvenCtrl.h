// OvenCtrl.h: interface for the COvenCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OVENCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
#define AFX_OVENCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SerialPort.h"
#include "../CANConv/modbus.h" //yulee 20190605

#define MAX_OVEN_COUNT	4
#define MAX_FILO_SIZE 3

#define MODBUS_LOCK_DELAY_TIME_MSEC 500 //ksj 20201022

union com_Data
{
	WORD nData;
	BYTE bData[2];
};

union com_TEMI880Data
{
	long lData;
	BYTE bData[4];
};
union com_WISECUBEData
{
	long lData;
	BYTE bData[5];
};

//20090113 KHS TEMP880 추가
union com_TEMP880Data
{
	long lDatal;
	BYTE bData[4];
};

//ljb 20180219 TEMP2520 add
union com_TEMP2520Data
{
	long lData;
	BYTE bData[4];
};


typedef struct S_PumpState 
{
	int nPV;//Current	
	int nSP;//Reference

	float fPV;//Current	//ksj 20200706 : 소수점 처리 추가
	float fSP;//Reference //ksj 20200706 : 소수점 처리 추가.
	
	int nPlay;//0-Run,1-Stop
	int nMode;//0-Auto,1-Manual

	int nSPNo;

	int nSP1;
	int nSP2;
	int nSP3;
	int nSP4;

	int nError;

} PUMP_STATE;

class COven
{
public:
	ULONG m_nDisconnectCnt;
	BOOL m_bLineState;
	COven()
	{
		ResetData();
	}
	virtual ~COven()	
	{
		ResetData();
	}

	//  [5/28/2009 kky ]
	// for OvenStatus 추가
	void	SetOvenStatus(long lOvenStatus)		{	m_lOvenStatus = lOvenStatus;	}
	long	GetOvenStatus()						{	return m_lOvenStatus;		}

	int		GetRunStepNo()						{	return m_nRunMinStepNo;		}
	void	SetRunStepNo(int nStepNo)			{	m_nRunMinStepNo = nStepNo;	}
	
	void	SetRunningTime(ULONG ulRunTime)		{	m_ulRunnTime = ulRunTime;	}
	ULONG	GetRunningTime()					{	return m_ulRunnTime;		}

	void    SetProgTemperature(float	fSetData)	{	m_fProgTemper = fSetData;	}
	float	GetProgTemperature()					{	return m_fProgTemper;		}
	
	void	SetCurTemperature(float	fSetData)	{	m_fCurTemper = fSetData;	}
	float	GetCurTemperature()					{	return m_fCurTemper;		}
	void	SetRefTemperature(float	fSetData)	{	m_fSetTemper = fSetData;	}
	float	GetRefTemperature()					{	return m_fSetTemper;		}
//*****************************************************************************************************************//
//Ch2 Chamber 대응*************************************************************************************************//
	void	SetCurTemperatureCh2(float	fSetData)	{	m_fCurTemper2 = fSetData;	}
	float	GetCurTemperatureCh2()					{	return m_fCurTemper2;		}
	void	SetRefTemperatureCh2(float	fSetData)	{	m_fSetTemper2 = fSetData;	}//yulee 20190919 temp2520 대응
	float	GetRefTemperatureCh2()					{	return m_fSetTemper2;		}//yulee 20190919 temp2520 대응
//*****************************************************************************************************************//
	void	SetCurHumidity(float	fSetData)		{	m_fCurHumidity = fSetData;	}
	float	GetCurHumidity()					{	return m_fCurHumidity;		}
	void	SetRefHumidity(float	fSetData)	{	m_fSetHumidity = fSetData;	}
	float	GetRefHumidity()					{	return m_fSetHumidity;		}

	CString	GetVersion()				{	return	m_strVer;	}
	void	SetVersion(CString str)		{	m_strVer = str;}

 	//long	GetRunMode()				{	return m_RunMode;	}
 	//void	SetRunMode(long lMode)		{	m_RunMode = lMode;	}

	WORD	GetRunState()				{ return m_state; }
	void	SetRunState(WORD state)		{ m_state = state;}	

	long	GetRunWorkMode()				{	return m_RunWorkMode;	}
	void	SetRunWorkMode(long lMode)		{	m_RunWorkMode = lMode;	}

//Ch2 Chamber 대응*************************************************************************************************//
	WORD	GetRunState2()					{ return m_state2; }
	void	SetRunState2(WORD state)		{ m_state2 = state;}	

	long	GetRunWorkMode2()				{	return m_RunWorkMode2;	}
	void	SetRunWorkMode2(long lMode)		{	m_RunWorkMode2 = lMode;	}
//*****************************************************************************************************************//

	long	GetPatternSaveDone()				{	return m_PatternSave;	}
	void	SetPatternSaveDone(long lMode)		{	m_PatternSave = lMode;	}

	long	GetRunTime()				{	return m_lRunTime;	}
	void	SetRunTime(ULONG lRunTime)	{	m_lRunTime = lRunTime;	}
	
	BOOL	GetUseOven()				{ return m_bUse; } 
	void	SetUseOven(BOOL bUse)		{ m_bUse = bUse; }

	CDWordArray m_awChMapArray;			//설치된 Channel List
	void	ResetData()
	{
		m_lOvenStatus =  0;		//★★ ljb 2010325  ★★★★★★★★
		m_fCurTemper = 0.0f;
		m_fSetTemper = 0.0f;
		m_fProgTemper = 0.0f;


		m_fCurHumidity = 0.0f;
		m_fSetHumidity = 0.0f;

		m_RunMode = 0;
		m_RunWorkMode = 0;
		m_RunWorkMode2 = 0;
		
		m_state = 0;
		m_state2 = 0;
		m_lRunTime = 0;
		m_nRunMinStepNo = 0;
		m_bLineState = FALSE;
		m_bUse = FALSE;
		m_nDisconnectCnt = 0;
		m_ulRunnTime = 0;			//Oven Run Time

		m_awChMapArray.RemoveAll();
	}

protected:	
	ULONG m_ulRunnTime;
	long  m_lOvenStatus;

	float m_fProgTemper;	//ljb 프로그램 모드 세그먼트 목표 온도값
	float m_fCurTemper;
	float m_fSetTemper;

	float m_fCurTemper2;
	float m_fSetTemper2;

	float m_fCurHumidity;
	float m_fSetHumidity;

	long m_RunMode;				
	long m_RunWorkMode;			// FIX, PROG
	long m_RunWorkMode2;		// FIX, PROG
	long m_PatternSave;			//Pattern save done

	CString	m_strVer;
	ULONG	m_lRunTime;
	int		m_nRunMinStepNo;
	WORD	m_state;			// RUN, STOP
	WORD	m_state2;			// RUN, STOP

	BOOL	m_bUse;




};

class COvenCtrl  
{
public:
	SERIAL_CONFIG	m_SerialConfig;
	BOOL ReConnect(int nIndex, int nDevice); //ksj 20201017 : SK 소스에서 발췌
	void ResetDisconnectCnt(int nOvenIndex);
	ULONG GetDisconnectCnt(int nOvenIndex);
	void IncreaseDisconnectTime(int nOvenIndex);
	int RequestCurrentValue(int nIndex, int nDevice, LPVOID pParam = NULL);    // (int nIndex, int nDevice, LPVOID pParam= NULL);//yulee 20190607
	BOOL SetFixMode(int nIndex, int nDevice, BOOL bSet = TRUE);
	BOOL SetFixPumpMode(int nIndex, int nDevice, BOOL bSet = TRUE);
	BOOL SetRunPatternNo(int nIndex, int nDevice, WORD nNumber);	//ljb for samsung QC
	BOOL SetStartProgMode(int nIndex, int nDevice, WORD wPatternNo);
	BOOL SetPatternSegNo(int nIndex, int nDevice, WORD wPatternNo, WORD wSegNo,float fSP,float fTSP, float fStime);	//ljb for samsung QC
	BOOL SendStopCmd(int nIndex, int nDevice);
	BOOL SendRunCmd(int nIndex, int nDevice);
	BOOL SetTSlop(int nIndex, int nDevice, float fDataT);
	BOOL SetHSlop(int nIndex, int nDevice, float fDataH);
	BOOL SetHumidity(int nIndex, int nDevice, float fDataH);
	BOOL SetTemperature(int nIndex, int nDevice, float fDataT, int nChillerPump = 0);
	long StringToHex32(CString strHex);
	int GetReqOvenHumidity(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen);
	int GetReqOvenSetHSlop(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen);
	int GetReqOvenSetTSlop(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen);
	int GetReqOvenFixMode(int nDeviceNumber, float fDataT, char * szCmd, int & nResLen);
	BOOL SetRefData(int nIndex, char szBuffData[1024]);
	void SetCurrentTempSPDataWiseCube(int nIndex, char szBuffData[]);
	int GetReqOvenReadSPTemperatureCmd(int nDeviceNumber,  char *szCmd, int &nResLen);
 	//void LoadSerilaConfig();
 	//void SaveSerilaConfig();
	BOOL InitSerialPort(int nOvenPortNum);	
	BOOL InitSerialPort_Modbus(int nOvenPortNum, int nChamberCtrlModel);//yulee 20190609
	BOOL CloseSerialPort(int nOvenPortNum);
	void DeleteAllOven();
	int GetReqStopCmd(int nDeviceNumber, char *szCmd, int &nResLen);
	int GetReqPumpStopCmd(int nDeviceNumber, char *szCmd, int &nResLen); //yulee 20191022
	int GetReqRunCmd(int nDeviceNumber, char * szCmd, int & nResLen);
	int GetReqPumpRunCmd(int nDeviceNumber, char *szCmd, int &nResLen); //yulee 20191022
	void SetCurrentDataWiseCube(int nIndex, char szBuffData[]);
	
	CSerialPort *m_pSerial[MAX_OVEN_COUNT];	
	//CPtrArray	m_apSerial;
	//CSerialPort m_Serial[MAX_OVEN_COUNT];
	//SERIAL_CONFIG m_SerialConfig[MAX_OVEN_COUNT];

	void InsertRxChar(char szRxChar, int nOvenIndex = -1);
	void SetCurrentDataTEMI880(int nIndex, char szBuffData[]);
	void SetCurrentDataTEMP880(int nIndex, char szBuffData[]);		//20090113 KHS TEMP880 추가
	void SetCurrentDataTemp2300(int nIndex, char szBuffData[]);		//ljb 20140429 add Temp2300
	void SetCurrentDataTemi2300(int nIndex, char szBuffData[]);		//ljb 20140429 add Temp2300
	void SetCurrentDataTemp2500(int nIndex, char szBuffData[]);		//lmh 20120524 Temp2500 add
	void SetCurrentDataTemi2500(int nIndex, char szBuffData[]);		//lmh 20120524 Temp2500 add
	void SetCurrentDataChillerTEMP2520(int nIndex, char szBuffData[]);
	void SetCurrentDataChillerST590(int nIndex, char szBuffData[]); //ksj 20200401


	void SetCurrentDataNEX(int nIndex, float CurTempCh1, float SetTempCh1, int OPStateCh1, int OPModeCh1, 
		 float CurTempCh2, float SetTempCh2, int OPStateCh2, int OPModeCh2,
		float CurTempCh3, float SetTempCh3, int OPStateCh3, int OPModeCh3,
		float CurHumi, float SetHumi,  //yulee 20190614_*
		float CurPump, float SetPump, int OPStatePump, int OPModePump);		//yulee 20190605

	int GetReqStrOvenVersionCmd(int nDeviceNumber, char * szCmd, int & nResLen);
	int GetOvenModelType();
	void SetOvenModelType(int nType);
	void SetOvenOndoPSFactor(float fFactorValue); //20170525 YULEE 추가 
	void SetOvenOndoSPFactor(float fFactorValue); //20170907 YULEE SPFactor의 옵션처리 차 추가 
	long GetRunTime(int nIndex = 0);
	void SetRunStepNo(int nIndex, int nStepNo);
	int GetRunStepNo(int nIndex);
	void ResetData(int nIndex = 0);
	BOOL GetRun(int nindex);
	BOOL GetPumpRun(int nindex);		//yulee 20191025
	long GetRunWorkMode(int nindex);
	long GetRunPumpWorkMode(int nindex); //yulee 20191025
	BOOL GetPatternSaveDon(int nindex);
	BOOL GetOvenIndexList(CDWordArray &adwChArray, CDWordArray &adwOvenIndex);
	int GetOvenIndex(int nModuleID, int nChIndex);
	void SetVersionInfo(int nIndex, CString strVer);
	BOOL SetCurrentData(int nIndex, char szBuffData[1024]);
	BOOL SetCurrentDataByModbus(int nIndex, float CurTempCh1, float SetTempCh1, int OPStateCh1, int OPModeCh1,
		float CurTempCh2, float SetTempCh2, int OPStateCh2, int OPModeCh2,
		float CurTempCh3, float SetTempCh3, int OPStateCh3, int OPModeCh3
		, float CurHumi, float SetHumi, float CurPump, FLOAT SetPump, int OPStatePump, int OPModePump); //yulee 20190605
	void ClearCurrentData();				//★★ ljb 2010513  ★★★★★★★★
	COvenCtrl();
	virtual ~COvenCtrl();
	
	//  [5/28/2009 kky ]
	// for OvenStatus
	long	GetOvenStatus(int nOvenIndex = 0)						{	return m_OvenData[nOvenIndex].GetOvenStatus();	}	//★★ ljb 2010325  ★★★★★★★★
	float	GetCurTemperature(int nOvenIndex = 0)					{	return m_OvenData[nOvenIndex].GetCurTemperature();	}
	float	GetRefTemperature(int nOvenIndex = 0)					{	return m_OvenData[nOvenIndex].GetRefTemperature();	}
	float	GetProgTemperature(int nOvenIndex = 0)					{	return m_OvenData[nOvenIndex].GetProgTemperature();	}	//ljb

	float	GetCurHumidity(int nOvenIndex = 0)					{	return m_OvenData[nOvenIndex].GetCurHumidity();	}
	float	GetRefHumidity(int nOvenIndex = 0)					{	return m_OvenData[nOvenIndex].GetRefHumidity();	}

	void	SetOvenCount(int nCnt)	{	m_nInstallCount = nCnt;		}
	int		GetOvenCount()			{	return m_nInstallCount;		}

	void	GetChannelMapArray(int nOvenIndex, CDWordArray &awChannel)	
	{	
		awChannel.Copy(m_OvenData[nOvenIndex].m_awChMapArray);	
	}
	void	SetChannelMapArray(int nOvenIndex, CDWordArray &awChannel)		
	{	
		m_OvenData[nOvenIndex].m_awChMapArray.Copy(awChannel);
	}
	void	SetLineState(int nOvenIndex, BOOL bLineOn = TRUE)	{	m_OvenData[nOvenIndex].m_bLineState = bLineOn;	}
	BOOL	GetLineState(int nOvenIndex)						{	return m_OvenData[nOvenIndex].m_bLineState;	}

	BOOL	GetUseOven(int nOvenIndex)							{	return m_OvenData[nOvenIndex].GetUseOven();	}
	void	SetUseOven(int nOvenIndex, BOOL bUse = TRUE)		{	m_OvenData[nOvenIndex].SetUseOven(bUse);	}
	BOOL	m_bUseOvenCtrl;
	BOOL	m_bPortOpen;			//ljb 20180219 add
	int		m_nOvenModelType;		//20090120 KHS protected에서 public로 이동 //lmh 20120524 new chamber Temp2500 add
	float   m_fOvenOndoPVFactor;	//20170525 YULEE 추가
	float   m_fOvenOndoSPFactor;	//20170907 YULEE SPFactor의 옵션처리 차 추가

	//yulee 20190609 modbus 추가
	CRITICAL_SECTION* m_pCS;
	modbus_t* m_ctx;
	BOOL	m_bPortOpen_Modbus;			//ljb 20180219 add

	//cny--------------------------------
	PUMP_STATE   m_PumpState;
	CSerialPort  m_pPumpSerial;
	int          m_iPumpDeviceID;
	CStringArray m_PumpCmdList;
	long         m_iPumpEmptyCount;
	int          m_iPumpCHNO;//1start
	int          m_iPumpReqStop;
	BOOL onPumpInitSerialPort(int nOvenPortNum, int nCommType = 0); //yulee 20190614_*
	int  onPumpCmd(int nCmd,int nVal=0,BOOL bLog=FALSE, int *Value = NULL); //yulee 20190614_*
	void onPumpRecvDeal(int nCmd,char *szRxChar, int nRxLen);
	int  onPumpGetCmd(int nCmd,int nVal,char *szCmd, int & nResLen);
	int  onPumpCmdDeal();
	void onPumpGetState();
	void onPumpCmdAdd(int nCmd,int nVal=0,int nLog=0);
	void onPumpSetCHNO(int nCHNO);
	CString onPumpGetLogType();
	//------------------------------------------------------
	HANDLE m_hThreadPump;
	static DWORD WINAPI PumpDealThread(LPVOID arg);
	int m_nCurTempValueCnt; //yulee 20190715
	float m_fltCurTemp[MAX_FILO_SIZE];//yulee 20190715
	//------------------------------------------------------

	bool m_bChiller_DESChiller; //lyj 20201102 DES칠러 

protected:

	int m_nOvenPortNum; //lyj 20200226
	int m_nRxBuffCnt;
	CCriticalSection m_cs; //ksj 20200723 : 크리티컬 섹션 추가. 다중 쓰레드에서 동시 접속 못하도록 잠금
	int GetReqOvenTemperatureCmd(int nDeviceNumber, float fDataT, char * szCmd, int & nResLen);	
	int GetReqChillerPumpCmd(int nDeviceNumber, float fDataT, char *szCmd, int &nResLen); //yulee 20191022
	int GetReqCurrentValueCmd(int nDeviceNumber, char * szCmd, int & nResLen);
	int GetReqPatternDoneCmd(int nDeviceNumber, char * szCmd, int & nResLen);
	char m_szRxBuff[200];	
	void SetCurrentDataTH500(int nIndex, char szBuffData[]);
	void SetCurrentDataTD500(int nIndex, char szBuffData[]);
	//enum {MAX_OVEN_COUNT = 8};
	COven m_OvenData[MAX_OVEN_COUNT];
	
	int   m_nInstallCount;				//ljb 20111117 RS485 통신으로 아이디만 바꿔서 연결된 갯수
	//int m_nOvenModelType;

	float m_PV_temp1; //yulee 20190715
	float m_PV_temp2;
	float m_PV_temp3;	
	float m_SP_temp1;
	float m_SP_temp2;
	float m_SP_temp3;
	int	m_OPState1;
	int	m_OPState2;
	int	m_OPState3;	
	int m_OPMode1;
	int	m_OPMode2;
	int	m_OPMode3;

	float m_PV_humi;
	float m_SP_humi;
	float m_PV_Pump;
	float m_SP_Pump;
	int	  m_OPState_Pump;
	int	  m_OPMode_Pump;//	

	int m_nErrModbusCnt;
	
private:
	// //ksj 20201022 : 연동 쓰레드에서 명령 전송하는 동안 RequestCurrentValue 요청 쓰레드에서 주기적인 상태확인 명령 호출하지않도록 플래그 추가
	BOOL m_bRequestLock;
public:
	void RequestSetLock(bool bLock, int nWaitMiliSec = 0);
	BOOL RequestGetLock();
};

#endif // !defined(AFX_OVENCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
