#include "stdafx.h"
#include "UpdateDatabase.h"
#include "CTSMonProDoc.h"

CUpdateDatabase::CUpdateDatabase(CCTSMonProDoc* pDoc)
{
	m_pDoc = pDoc;
}


CUpdateDatabase::~CUpdateDatabase(void)
{
}

//ksj 20200120 : 기본 계정 자동 생성.
#define UPDATE_USER_CNT 4
int CUpdateDatabase::UpdateDefaultUser(void)
{
	CString strTemp;
	CString strTableName;
	char szUserID[UPDATE_USER_CNT][256] = {
		"lgc", //lg 관리자 계정
		"lguser", //lg 일반 계정
		"su", //관리자 계정
		"user" //일반 계정
	}; 

	char szPassword[UPDATE_USER_CNT][256] = {
		"lgchem!",
		"lguser",
		"su!",
		"user"
	};

	char szName[UPDATE_USER_CNT][256] = {
		"lgc",
		"lguser",
		"su",
		"user"
	};

	int nAuthority[UPDATE_USER_CNT] = {
		PMS_SUPERVISOR, //admin,	
		PS_USER_OPERATOR, //작업자
		PMS_SUPERVISOR, //admin,	
		PS_USER_OPERATOR //작업자
	};



	CDaoDatabase  db;	
	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		m_pDoc->WriteLog(strTemp);
		e->Delete();
		return 0;
	}

	int i = 0;
	for(i=0;i<UPDATE_USER_CNT;i++)
	{
		try
		{
			if(GetMDBUserIDCount(db, szUserID[i]) == 0) //기존에 등록된 User가 없으면 insert
			{
				strTemp.Format("INSERT INTO User(UserID,Password,Name,Authority) VALUES ('%s', '%s', '%s',%d)",szUserID[i],szPassword[i],szName[i],nAuthority[i]);
				db.Execute(strTemp);
			}
			else //있으면 업데이트
			{

			}
		}
		catch (CDaoException* e)
		{
			TRACE("%s\n",e->m_pErrorInfo->m_strDescription);		
#ifdef _DEBUG
			strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
#else  //릴리즈 버전에서는 에러로 표시 하지 않는다.
			if(e->m_pErrorInfo->m_lErrorCode == 3380)
			{
				strTemp.Format("DB Update Result Code(%d) : 이미 DB업데이트 완료됨.",e->m_pErrorInfo->m_lErrorCode);
			}
			else
			{
				strTemp.Format("DB Update Result Code(%d)",e->m_pErrorInfo->m_lErrorCode);
			}		
#endif
			m_pDoc->WriteLog(strTemp);

			e->Delete();
		}
	}	

	db.Close();		

	Sleep(300);


	return 1;
}

//ksj 20200120 : v1016 신규 채널 코드 SQL 쿼리 생성
#define UPDATE_CHANNEL_CODE_V1016_CNT 9
int CUpdateDatabase::UpdateChannelcode_v1016(void)
{
	int nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	CString strTemp;
	CString strTableName;
	char szMsg[UPDATE_CHANNEL_CODE_V1016_CNT][256];
	char szDesc[UPDATE_CHANNEL_CODE_V1016_CNT][256];
	int naCode[UPDATE_CHANNEL_CODE_V1016_CNT]; 

	strTableName = m_pDoc->GetChannelCodeTableLangName(); //ksj 20201012

	//ksj 20200804 : 언어별 업데이트 문자 선택
	switch(nSelLanguage)
	{
	case 1 : //kor
		{
			naCode[0] = 0; //code
			strcpy(szMsg[0],"정상"); //msg
			strcpy(szDesc[0],"정상 Cell(Code 20 ~ 79)"); //desc
			
			naCode[1] = 246; //code
			strcpy(szMsg[1],"외부데이터 고정 상한"); //msg
			strcpy(szDesc[1],"비공정 및 공정 상태에서 외부데이터 고정 상한 검출"); //desc

			naCode[2] = 247; //code
			strcpy(szMsg[2],"외부데이터 고정 하한"); //msg
			strcpy(szDesc[2],"비공정 및 공정 상태에서 외부데이터 고정 하한 검출"); //desc

			naCode[3] = 248; //code
			strcpy(szMsg[3],"VENT 수동 오픈"); //msg
			strcpy(szDesc[3],"UI 화면의 VENT OPEN 버튼을 눌러 VENT를 수동 오픈"); //desc

			naCode[4] = 249; //code
			strcpy(szMsg[4],"외부 습도상한"); //msg
			strcpy(szDesc[4],"외부데이터 설정의 습도상한 조건 감지"); //desc

			naCode[5] = 250; //code
			strcpy(szMsg[5],"외부 습도하한"); //msg
			strcpy(szDesc[5],"외부데이터 설정의 습도하한 조건 감지"); //desc

			naCode[6] = 68; //code
			strcpy(szMsg[6],"외부 습도상한 종료"); //msg
			strcpy(szDesc[6],"외부데이터 설정의 습도상한 조건에 의해 종료됨"); //desc

			naCode[7] = 69; //code
			strcpy(szMsg[7],"외부 습도하한 종료"); //msg
			strcpy(szDesc[7],"외부데이터 설정의 습도상한 조건에 의해 종료됨"); //desc	

			//ksj 20201012 : 챔버 연동 대기 채널코드가 없음. 자동 추가되도록 함.
			naCode[8] = 87; //code
			strcpy(szMsg[8],"챔버연동 대기"); //msg
			strcpy(szDesc[8],"챔버연동 대기"); //desc		

		}
		break;
	default : //기타 등등
		{
			naCode[0] = 0; //code
			strcpy(szMsg[0],"OK"); //msg
			strcpy(szDesc[0],"Normal Cell (Code 20 ~ 79)"); //desc

			naCode[1] = 246; //code
			strcpy(szMsg[1],"External data fixed upper limit"); //msg
			strcpy(szDesc[1],"External fixed upper limit detection in non-process and process conditions"); //desc

			naCode[2] = 247; //code
			strcpy(szMsg[2],"External data fixed lower limit"); //msg
			strcpy(szDesc[2],"Detecting lower limit of external data in non-process and process conditions"); //desc

			naCode[3] = 248; //code
			strcpy(szMsg[3],"VENT manual open"); //msg
			strcpy(szDesc[3],"Press the VENT OPEN button on the UI screen to manually open the VENT"); //desc

			naCode[4] = 249; //code
			strcpy(szMsg[4],"External humidity upper limit"); //msg
			strcpy(szDesc[4],"Detecting upper humidity condition of external data setting"); //desc

			naCode[5] = 250; //code
			strcpy(szMsg[5],"Low external humidity"); //msg
			strcpy(szDesc[5],"Detecting the lower humidity condition of external data setting"); //desc

			naCode[6] = 68; //code
			strcpy(szMsg[6],"External humidity upper limit"); //msg
			strcpy(szDesc[6],"Exit due to external data setting humidity upper limit condition"); //desc

			naCode[7] = 69; //code
			strcpy(szMsg[7],"External low humidity end"); //msg
			strcpy(szDesc[7],"Exit due to external data setting upper humidity limit condition"); //desc	

			//ksj 20201012 : 챔버 연동 대기 채널코드가 없음. 자동 추가되도록 함.
			naCode[8] = 87; //code
			strcpy(szMsg[8],"Chamber interlock standby"); //msg
			strcpy(szDesc[8],"Chamber interlock standby"); //desc	
		}
	}



	CDaoDatabase  db;	
	try
	{
		db.Open(m_pDoc->GetCodeDataBaseName());
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}

	int i = 0;
	for(i=0;i<UPDATE_CHANNEL_CODE_V1016_CNT;i++)
	{
		try
		{
			if(GetMDBChannelCodeCount(db, strTableName, naCode[i]) == 0) //기존에 등록된 코드가 없으면 insert
			{
				strTemp.Format("INSERT INTO %s(Code,Message,Description) VALUES (%d, '%s', '%s')",strTableName,naCode[i],szMsg[i],szDesc[i]);
				db.Execute(strTemp);
			}			
		}
		catch (CDaoException* e)
		{
			TRACE("%s\n",e->m_pErrorInfo->m_strDescription);		
#ifdef _DEBUG
			strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
#else  //릴리즈 버전에서는 에러로 표시 하지 않는다.
			if(e->m_pErrorInfo->m_lErrorCode == 3380)
			{
				//strTemp.Format("DB Update Result Code(%d) : 이미 DB업데이트 완료됨.",e->m_pErrorInfo->m_lErrorCode);
				strTemp.Format("DB Update Result Code(%d) : DB update is already completed.",e->m_pErrorInfo->m_lErrorCode); //lyj 20200716
			}
			else
			{
				strTemp.Format("DB Update Result Code(%d)",e->m_pErrorInfo->m_lErrorCode);
			}		
#endif
			m_pDoc->WriteLog(strTemp);

			e->Delete();
		}
	}	

	db.Close();		

	Sleep(300);

	return 1;
}

// ksj 20200211 : MDB에 자동으로 습도센서 AuxH를 추가한다
int CUpdateDatabase::UpdateMDBAuxH(void)
{
	//DB 오픈
	CString strTemp;
	CDaoDatabase  db;

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		m_pDoc->WriteLog(strTemp);
		e->Delete();
		db.Close();
		return 0;
	}

	//property에 AuxH가 있는지 스캔
	CString strSQL;

	strSQL.Format("SELECT Index, Name FROM Property WHERE Name='Humidity'");

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	int nCount = 0;
	int nPropertyIndex = 0;
	CStringArray strArray;
	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//없으면 추가하고 인덱스중 가장 큰값 +1로 설정

	//가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM Property");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nPropertyIndex = data.lVal+1;

		strArray.RemoveAll();
		strSQL.Format("INSERT INTO Property(Index, Name, UnitNotation, UnitTransfer) VALUES (%d, '%s', '%s', %f)"
			, nPropertyIndex, "Humidity", "%%", 1.0f);

		strArray.Add(strSQL);			
		//ExecuteQueryDB(strArray); //Property 에 Humidity 추가.
		ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //Property 에 Humidity 추가. //ksj 20201012
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//AxisInfo에 AuxH가 있는지 스캔 
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='AuxH'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	//있으면 중단
	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//없으면 추가하고 인덱스 저장
	//가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM AxisInfo");

	int nAxisIndex = 0;
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nAxisIndex = data.lVal+1;

		strArray.RemoveAll();
		strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
			, nAxisIndex, "AuxH", "AuxH", nPropertyIndex);

		strArray.Add(strSQL);			
		//ExecuteQueryDB(strArray); //AxisInfo 에 AuxH 추가.
		ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //Property 에 AuxH 추가. //ksj 20201012
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();
	//Time축 뒤에 인덱스 추가.
	strSQL.Format("SELECT YAxisIndexList FROM AxisInfo WHERE Index=0");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	//있으면 업데이트
	if(!rs.IsBOF() && !rs.IsEOF()) 
	{	
		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)
		{
			strTemp.Format("%s%d,", data.pbVal,nAxisIndex);

			strArray.RemoveAll();
			strSQL.Format("UPDATE AxisInfo SET YAxisIndexList='%s' WHERE Index=0",strTemp);
			strArray.Add(strSQL);
			//ExecuteQueryDB(strArray);
			ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //ksj 20201012
		}		
	}	


	rs.Close();
	db.Close();

	return 0;
}

//lyj 20200520 : MDB에 자동으로 AxisInfo에 Sequence를 추가한다
int CUpdateDatabase::UpdateMDBSequence(void)
{
	//DB 오픈
	CString strTemp;
	CDaoDatabase  db;

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		m_pDoc->WriteLog(strTemp);
		e->Delete();
		db.Close();
		return 0;
	}

	int nCount = 0;
	int nPropertyIndex = 0;
	int nAxisInfo = 0;
	CString strSQL, strYAxisTime;
	CStringArray strArray;

	//Property 테이블에 index 찾기
	strSQL.Format("SELECT Index, Name FROM Property WHERE Name='Index'");

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	rs.Close();

	//Property 가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM Property");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nPropertyIndex = data.lVal+1;

		strArray.RemoveAll();
		strSQL.Format("INSERT INTO Property(Index, Name, UnitNotation, UnitTransfer) VALUES (%d, '%s', '%s', %d)"
			, nPropertyIndex, "Index", "Index", 1);
		strArray.Add(strSQL);			
		//ExecuteQueryDB(strArray); //Property 에 Index 추가.
		ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //ksj 20201012
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//AxisInfo에 Sequence가 있는지 스캔

	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='Sequence'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();

	//없으면 추가하고 인덱스중 가장 큰값 +1로 설정

	//AxisInfo 가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM AxisInfo");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{
		data = rs.GetFieldValue(0);
		nAxisInfo = data.lVal+1;

		rs.Close();

		//Time의 YAxisIndexList 찾기
		strSQL.Empty();
		strSQL.Format("SELECT YAxisIndexList FROM AxisInfo WHERE Name='Time' ");

		try
		{
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			db.Close();
			return 0;
		}

		if(!rs.IsBOF() && !rs.IsEOF())
		{	
			data = rs.GetFieldValue(0);
			strYAxisTime.Format("%d,%s",0,data.bstrVal);

			strSQL.Empty();
			strArray.RemoveAll();
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, IsUsedForXAxis, YAxisIndexList, PropertyIndex) VALUES (%d, '%s', '%s',%d,'%s', %d)"
				, nAxisInfo, "Sequence", "Sequence", 1, strYAxisTime , nPropertyIndex);

			strArray.Add(strSQL);			
			//ExecuteQueryDB(strArray); 
			ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //ksj 20201012
		}
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}
	rs.Close();




	rs.Close();
	db.Close();

	return 0;
}

int CUpdateDatabase::UpdateMDBFlicker(void)  //lyj 20201005
{
	//DB 오픈
	CString strTemp , strSQL;
	CStringArray strArray;
	BOOL bresult = 0;


	// 	int ilanguage = AfxGetApp()->GetProfileInt("Config","Language",1);
	// 	switch (ilanguage)
	// 	{
	// 		case 1: strTemp.Format("%s","ChannelCode_ko");break; 
	// 		case 2: strTemp.Format("%s","ChannelCode_en");break;
	// 		case 3: strTemp.Format("%s","ChannelCode_pl");break;
	// 		default : strTemp.Format("%s","ChannelCode_ko");break;
	// 	}
	strTemp = m_pDoc->GetChannelCodeTableLangName(); //ksj 20201012

	strArray.RemoveAll();
	strSQL.Format("ALTER TABLE %s ADD Color int ",strTemp);
	strArray.Add(strSQL);			
	strSQL.Format("ALTER TABLE %s ADD Priority int ",strTemp);
	strArray.Add(strSQL);			
	//strSQL.Format("ALTER TABLE %s ADD Use int ",strTemp);
	strSQL.Format("ALTER TABLE %s ADD ShowColorTable int ",strTemp);
	strArray.Add(strSQL);			
	strSQL.Format("ALTER TABLE %s ADD UseAutoBuzzerStop int ",strTemp);
	strArray.Add(strSQL);
	//bresult = ExecuteQueryDB(strArray);
	//strSQL.Format(	"UPDATE SystemConfig SET IP_Address = '%s', VSpec = '%s', ISpec = '%s', InstallChCount = %d, Data4 = '%s' WHERE ModuleID = %d", 
	strSQL.Format("UPDATE %s SET Color=%d, Priority=1, ShowColorTable=1, UseAutoBuzzerStop=0",strTemp,RGB(255,0,0)); //ksj 20201012 : 기본 값 지정
	strArray.Add(strSQL);	

	strSQL.Format("UPDATE %s SET Color=%d, Priority=2, ShowColorTable=1, UseAutoBuzzerStop=1 WHERE Code=27 or Code=87 or Code=185",strTemp,RGB(255,255,0)); //ksj 20201012 : 기본 값 지정. 사용자 일시정지, 챔버 연동 대기는 별도로 기본값 지정
	strArray.Add(strSQL);	

	bresult = ExecuteQueryDB(strArray, m_pDoc->GetCodeDataBaseName(), FALSE); //ksj 20201012

// 	if(!bresult)
// 	{
// 		m_pDoc->LoadFlickerTable();
// 	}

	return 0;
}

//ksj 20191104 : 쿼리 연속 실행 함수 추가.
//ksj 20201012 : 쿼리 실패시 이후 쿼리 계속 진행 여부 옵션 추가 
//bFailContinue TRUE인 경우 계속 실행, FALSE 인 경우 스탑.
//int CCTSMonProDoc::ExecuteQueryDB(CStringArray& saSQL, CString strDatabaseName)
int CUpdateDatabase::ExecuteQueryDB(CStringArray& saSQL, CString strDatabaseName, BOOL bFailContinue)
{
	CString strTemp;
	BOOL breturn = TRUE;
	CDaoDatabase  db;	
	try
	{
		//db.Open(GetDataBaseName());
		db.Open(strDatabaseName);
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		m_pDoc->WriteLog(strTemp);
		e->Delete();
		return 0;
	}

	int i = 0;
	for(i=0;i<saSQL.GetCount();i++)
	{
		strTemp = saSQL.GetAt(i);

		try
		{
			db.Execute(strTemp);
		}
		catch (CDaoException* e)
		{
			TRACE("%s\n",e->m_pErrorInfo->m_strDescription);		
#ifdef _DEBUG
			strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
#else  //릴리즈 버전에서는 에러로 표시 하지 않는다.
			if(e->m_pErrorInfo->m_lErrorCode == 3380)
			{
				strTemp.Format("DB Update Result Code(%d) : 이미 DB업데이트 완료됨.",e->m_pErrorInfo->m_lErrorCode);
			}
			else
			{
				strTemp.Format("DB Update Result Code(%d)",e->m_pErrorInfo->m_lErrorCode);
			}		
#endif
			m_pDoc->WriteLog(strTemp);

			e->Delete();
			breturn = FALSE;
			if(!bFailContinue) break; //ksj 20201012 : bFailContinue FALSE 인 경우 다음 쿼리 실행 중단.
		}
	}	

	db.Close();		

	Sleep(300);

	return breturn;
}

//ksj 20200122 : Select 쿼리로 찾은 아이템 개수 리턴
int CUpdateDatabase::GetMDBChannelCodeCount(CDaoDatabase& db, CString strTable, int nChannelCode)
{
	CString strSQL;
	CString strTemp;

	strSQL.Format("SELECT Code, Message, Description FROM %s WHERE Code=%d ORDER BY Code",strTable,nChannelCode);

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	

	//쿼리에서 직접 카운트 값 얻을 수 있으나 시간 관계상 일반 select 후 개수 따로 카운팅.
	int nCount = 0;
	if(!rs.IsBOF() && !rs.IsEOF())
	{	
		while(!rs.IsEOF())
		{
			nCount++;
			rs.MoveNext();
		}
	}

	return nCount;
}

//ksj 20200122 : Select 쿼리로 찾은 UserID 개수 리턴
int CUpdateDatabase::GetMDBUserIDCount(CDaoDatabase& db, CString strUserID)
{
	CString strSQL;
	CString strTemp;

	strSQL.Format("SELECT * FROM User WHERE UserID='%s' ORDER BY index",strUserID);


	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return 0;
	}	

	//쿼리에서 직접 카운트 값 얻을 수 있으나 시간 관계상 일반 select 후 개수 따로 카운팅.
	int nCount = 0;
	if(!rs.IsBOF() && !rs.IsEOF())
	{	
		while(!rs.IsEOF())
		{
			nCount++;
			rs.MoveNext();
		}
	}

	return nCount;
}



//ksj 20200910 : MDB에 chiller 그래프 항목을 자동 추가한다.
int CUpdateDatabase::UpdateMDBChiller(void)
{
	//DB 오픈
	CString strTemp;
	CDaoDatabase  db;

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		m_pDoc->WriteLog(strTemp);
		e->Delete();
		db.Close();
		return 0;
	}

	//property에 LPM(유량)이 있는지 스캔
	CString strSQL;
	strSQL.Format("SELECT Index, Name FROM Property WHERE Name='LPM'");

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	int nCount = 0;
	int nPropertyIndex = 0;
	CStringArray strArray;
	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 리턴
	{	
		// 		rs.Close();
		// 		db.Close();
		// 		return 0;	
		TRACE("LPM 항목 존재");

		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)
		{
			nPropertyIndex = data.lVal;
		}
		rs.Close();
	}
	else //없으면 추가.
	{
		rs.Close();

		//없으면 추가하고 인덱스중 가장 큰값 +1로 설정

		//가장 큰 index 찾기
		strSQL.Format("SELECT MAX(Index) FROM Property");

		try
		{
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			db.Close();
			return 0;
		}	

		if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
		{	

			data = rs.GetFieldValue(0);
			nPropertyIndex = data.lVal+1;

			strArray.RemoveAll();
			strSQL.Format("INSERT INTO Property(Index, Name, UnitNotation, UnitTransfer) VALUES (%d, '%s', '%s', %f)"
				, nPropertyIndex, "LPM", "%%", 1.0f);

			strArray.Add(strSQL);			
			ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //Property 에 LPM 추가.
		}
		else //없으면 리턴
		{
			rs.Close();
			db.Close();
			return 0;	
		}

		rs.Close();
	}


	//AxisInfo에 칠러 항목 있는지 스캔 
	BOOL bChillerRefT = FALSE;
	BOOL bChillerCurT = FALSE;
	BOOL bChillerRefF = FALSE;
	BOOL bChillerCurF = FALSE;
	//////////////////////////////////////////////////////////////////////////////////////////
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='ChillerRefT'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 TRUE
	{	
		bChillerRefT = TRUE;
	}
	rs.Close();
	//////////////////////////////////////////////////////////////////////////////////////////
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='ChillerCurT'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 TRUE
	{	
		bChillerCurT = TRUE;
	}
	rs.Close();
	//////////////////////////////////////////////////////////////////////////////////////////
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='ChillerRefF'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	


	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 TRUE
	{	
		bChillerRefF = TRUE;
	}
	rs.Close();
	//////////////////////////////////////////////////////////////////////////////////////////
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='ChillerCurF'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	


	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 TRUE
	{	
		bChillerCurF = TRUE;
	}
	rs.Close();
	//////////////////////////////////////////////////////////////////////////////////////////


	//없으면 추가하고 인덱스 저장
	//가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM AxisInfo");

	int nAxisIndex = 0;
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	CString strIndexList;
	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nAxisIndex = data.lVal+1;

		if(!bChillerRefF)
		{
			strTemp.Format("%d,",nAxisIndex);
			strIndexList += strTemp;
			strArray.RemoveAll();
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
				, nAxisIndex++, "ChillerRefF", "ChillerRefF", nPropertyIndex);

			strArray.Add(strSQL);	
		}


		if(!bChillerCurF)
		{
			strTemp.Format("%d,",nAxisIndex);
			strIndexList += strTemp;
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
				, nAxisIndex++, "ChillerCurF", "ChillerCurF", nPropertyIndex);

			strArray.Add(strSQL);		
		}

		if(!bChillerRefT)
		{
			strTemp.Format("%d,",nAxisIndex);
			strIndexList += strTemp;
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
				, nAxisIndex++, "ChillerRefT", "ChillerRefT", nPropertyIndex);

			strArray.Add(strSQL);	
		}

		if(!bChillerCurT)
		{
			strTemp.Format("%d,",nAxisIndex);
			strIndexList += strTemp;
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
				, nAxisIndex++, "ChillerCurT", "ChillerCurT", nPropertyIndex);

			strArray.Add(strSQL);	
		}			

		ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //AxisInfo 에 Chiller 추가.
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();
	//Time축 뒤에 인덱스 추가.
	strSQL.Format("SELECT YAxisIndexList FROM AxisInfo WHERE Index=0");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	//있으면 업데이트
	if(!rs.IsBOF() && !rs.IsEOF()) 
	{	
		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)
		{
			if(!strIndexList.IsEmpty())
			{
				strTemp.Format("%s%s", data.pbVal,strIndexList);

				strArray.RemoveAll();
				strSQL.Format("UPDATE AxisInfo SET YAxisIndexList='%s' WHERE Index=0",strTemp);
				strArray.Add(strSQL);
				ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName());
			}
		}		
	}	


	rs.Close();
	db.Close();

	return 0;
}


//ksj 20200910 : MDB에 파워서플라이 그래프 항목을 자동 추가한다.
int CUpdateDatabase::UpdateMDBPowerSupply(void)
{
	//DB 오픈
	CString strTemp;
	CDaoDatabase  db;

	try
	{
		db.Open(m_pDoc->GetDataBaseName());
	}
	catch (CDaoException* e)
	{
		strTemp.Format("DB Update Error(%d) : %s",e->m_pErrorInfo->m_lErrorCode,e->m_pErrorInfo->m_strDescription);		
		m_pDoc->WriteLog(strTemp);
		e->Delete();
		db.Close();
		return 0;
	}

	//property에 전압이 있는지 스캔
	CString strSQL;
	strSQL.Format("SELECT Index, Name FROM Property WHERE Name='Voltage'"); //쿼리 안먹힘
	strSQL.Format("SELECT Index, Name FROM Property WHERE Name like 'Voltage'"); //쿼리 안먹힘

	COleVariant data;
	CDaoRecordset rs(&db);
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	int nCount = 0;
	int nPropertyIndex = 0;
	CStringArray strArray;
	if(!rs.IsBOF() && !rs.IsEOF())
	{	
		TRACE("전압 항목 존재");

		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)
		{
			nPropertyIndex = data.lVal;
		}
		rs.Close();
	}
	else //전압은 없으면 안됨. 리턴.
	{	
		// 		rs.Close();
		// 		db.Close();
		// 		return 0;			

		rs.Close();
		nPropertyIndex = 2; //보통 전압은 인덱스가 2이므로 2로 설정
	}


	//AxisInfo에 칠러 항목 있는지 스캔 
	BOOL bPSSetV = FALSE;
	BOOL bPSCurV = FALSE;
	//////////////////////////////////////////////////////////////////////////////////////////
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='P/SSetV'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 TRUE
	{	
		bPSSetV = TRUE;
	}
	rs.Close();
	//////////////////////////////////////////////////////////////////////////////////////////
	strSQL.Format("SELECT Index, Name FROM AxisInfo WHERE Name='P/SCurV'");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	if(!rs.IsBOF() && !rs.IsEOF()) //있으면 TRUE
	{	
		bPSCurV = TRUE;
	}
	rs.Close();
	//////////////////////////////////////////////////////////////////////////////////////////



	//없으면 추가하고 인덱스 저장
	//가장 큰 index 찾기
	strSQL.Format("SELECT MAX(Index) FROM AxisInfo");

	int nAxisIndex = 0;
	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	CString strIndexList;
	if(!rs.IsBOF() && !rs.IsEOF()) //가장큰 index 찾음
	{	

		data = rs.GetFieldValue(0);
		nAxisIndex = data.lVal+1;

		if(!bPSSetV)
		{
			strTemp.Format("%d,",nAxisIndex);
			strIndexList += strTemp;
			strArray.RemoveAll();
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
				, nAxisIndex++, "P/SSetV", "P/SSetV", nPropertyIndex);

			strArray.Add(strSQL);	
		}


		if(!bPSCurV)
		{
			strTemp.Format("%d,",nAxisIndex);
			strIndexList += strTemp;
			strSQL.Format("INSERT INTO AxisInfo(Index, Name, TransducerName, PropertyIndex) VALUES (%d, '%s', '%s', %d)"
				, nAxisIndex++, "P/SCurV", "P/SCurV", nPropertyIndex);

			strArray.Add(strSQL);		
		}


		ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName()); //AxisInfo 에 Chiller 추가.
	}
	else //없으면 리턴
	{
		rs.Close();
		db.Close();
		return 0;	
	}

	rs.Close();
	//Time축 뒤에 인덱스 추가.
	strSQL.Format("SELECT YAxisIndexList FROM AxisInfo WHERE Index=0");

	try
	{
		rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return 0;
	}	

	//있으면 업데이트
	if(!rs.IsBOF() && !rs.IsEOF()) 
	{	
		data = rs.GetFieldValue(0);
		if(VT_NULL != data.vt)
		{
			if(!strIndexList.IsEmpty())
			{
				strTemp.Format("%s%s", data.pbVal,strIndexList);

				strArray.RemoveAll();
				strSQL.Format("UPDATE AxisInfo SET YAxisIndexList='%s' WHERE Index=0",strTemp);
				strArray.Add(strSQL);
				ExecuteQueryDB(strArray, m_pDoc->GetDataBaseName());
			}		
		}		
	}	


	rs.Close();
	db.Close();

	return 0;
}

//데이터 베이스 초기화 일괄 호출
int CUpdateDatabase::UpdateDatabase(void)
{
	UpdateDefaultUser();
	UpdateChannelcode_v1016();
	UpdateMDBAuxH(); //ksj 20200211 : AuxH 관련 항목 자동 업데이트
	UpdateMDBSequence(); //lyj 20200520 : Sequence 관련 항목 자동 업데이트
	UpdateMDBFlicker(); //lyj 20201005
	UpdateMDBChiller();
	UpdateMDBPowerSupply();

	return 1;
}
