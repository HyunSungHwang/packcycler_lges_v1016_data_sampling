// SimpleTestDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "SimpleTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSimpleTestDlg dialog


CSimpleTestDlg::CSimpleTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSimpleTestDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSimpleTestDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSimpleTestDlg::IDD3):
	(CSimpleTestDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSimpleTestDlg)
	m_ckSimpleSave = FALSE;
	m_strIRef = _T("");
	m_strEndI = _T("");
	m_strVref = _T("");
	m_strEndV = _T("");
	//}}AFX_DATA_INIT
}


void CSimpleTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSimpleTestDlg)
	DDX_Control(pDX, IDC_DATETIMEPICKER2, m_ctrlSaveTime);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_ctrlEndTime);
	DDX_Check(pDX, IDC_CHECK_SAVE, m_ckSimpleSave);
	DDX_Text(pDX, IDC_EDIT_CURRENT, m_strIRef);
	DDX_Text(pDX, IDC_EDIT_CURRENT2, m_strEndI);
	DDX_Text(pDX, IDC_EDIT_VOLTAGE, m_strVref);
	DDX_Text(pDX, IDC_EDIT_VOLTAGE2, m_strEndV);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSimpleTestDlg, CDialog)
	//{{AFX_MSG_MAP(CSimpleTestDlg)
	ON_NOTIFY(NM_OUTOFMEMORY, IDC_SPIN1, OnOutofmemorySpin1)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN1, OnDeltaposSpin1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSimpleTestDlg message handlers

BOOL CSimpleTestDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	((CButton *)GetDlgItem(IDC_RADIO_CHARGE))->SetCheck(TRUE);
	
	m_ctrlEndTime.SetTime(DATE(0));
	m_ctrlEndTime.SetFormat("HH:mm:ss");

	m_ctrlSaveTime.SetTime(DATE(0));
	m_ctrlSaveTime.SetFormat("HH:mm:ss");
	GetDlgItem(IDC_EDIT_MS)->SetWindowText("000");

	CString strUnit;
	char szBuff[64];
	char szDecimalPoint[32], szUnit[32];
	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_VUNIT)->SetWindowText(szUnit);
	GetDlgItem(IDC_STATIC_VUNIT2)->SetWindowText(szUnit);

	sprintf(szBuff, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	GetDlgItem(IDC_STATIC_IUNIT)->SetWindowText(szUnit);
	GetDlgItem(IDC_STATIC_IUNIT2)->SetWindowText(szUnit);

	m_strEndI ="0";
	m_strEndV ="0";
	m_strIRef ="0";
	m_strVref ="0";
	UpdateData(FALSE);
 //yulee 20191017 OverChargeDischarger Mark
	m_bOverChargerSet = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerSet", FALSE);
	float OverChargerMinVolt = 0;
	if(m_bOverChargerSet) //yulee 20180810
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSimpleTestDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	float fMinVoltage;
	if(m_bOverChargerSet) //yulee 20191017 OverChargeDischarger Mark
	{
		float OverChargerMinVolt = 0;
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);
		fMinVoltage = -OverChargerMinVolt;//yulee 20180810
	}
	else
		fMinVoltage = 0.0f;

	m_ScheduleInfo.ResetData();
	
	WORD nType = PS_STEP_CHARGE;
	if(((CButton *)GetDlgItem(IDC_RADIO_CHARGE))->GetCheck() == FALSE)
	{
		nType = PS_STEP_DISCHARGE;
	}

	//Make 4Step Schedule data;
	//FILE_STEP_PARAM_V100D_LOAD stepData;
	//FILE_STEP_PARAM_V100F stepData;

	FILE_STEP_PARAM_V1013_v2_SCH stepData; //ksj 20180528
	//Add step1(Advance step Step)
	//ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100F));
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V1013_v2_SCH)); //ksj 20180528
	
	stepData.chType = PS_STEP_ADV_CYCLE;
	stepData.chStepNo = 0;
	//m_ScheduleInfo.AddStep_vF(&stepData);
	m_ScheduleInfo.AddStep_v1013_v2_SCH(&stepData); //ksj 20180528

	//Add step2
	//ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100F));
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V1013_v2_SCH)); //ksj 20180528
	
	stepData.chType = (BYTE)nType;
	stepData.chMode = PS_MODE_CCCV;

	CString strUnit;
	GetDlgItem(IDC_STATIC_VUNIT)->GetWindowText(strUnit);
	float fData1, fData2;
	fData1 = atof(m_strVref);
	fData2 = atof(m_strEndV);
	if(strUnit == "V")
	{
		fData1 *= 1000.0f;
		fData2 *= 1000.0f;
	}
	else if(strUnit == "uV")
	{
		fData1 /= 1000.0f;
		fData2 /= 1000.0f;
	}
//	stepData.fVref = fData1;
	if (nType == PS_STEP_CHARGE)
	{
		stepData.fVref_Charge = fData1;
		stepData.fVref_DisCharge = 0;
		stepData.fEndV = fData2;		//충전일때
		stepData.fEndV_L = 0;
	}
	else
	{
		stepData.fVref_Charge = 0;
		stepData.fVref_DisCharge = fData1;
		stepData.fEndV = 0;
		stepData.fEndV_L = fData2;		//방전일때
	}
	if(stepData.chType == PS_STEP_CHARGE && stepData.fVref_Charge <= fMinVoltage)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_SIMPLE_TEST_DLG"));
		//@ AfxMessageBox("전압이 설정되어 있지 않습니다.");
		return;
	}

	GetDlgItem(IDC_STATIC_IUNIT)->GetWindowText(strUnit);
	fData1 = atof(m_strIRef);
	fData2 = atof(m_strEndI);
	if(strUnit == "A")
	{
		fData1 *= 1000.0f;
		fData2 *= 1000.0f;
	}
	else if(strUnit == "uA")
	{
		fData1 /= 1000.0f;
		fData2 /= 1000.0f;
	}
	else if(strUnit == "nA")
	{
		fData1 /= 1000000.0f;
		fData2 /= 1000000.0f;
	}
	stepData.fIref = fData1;
	stepData.fEndI = fData2;
	if(stepData.fIref <= 0.0f)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_SIMPLE_TEST_DLG"));
		//@ AfxMessageBox("전류가 설정되어 있지 않습니다.");
		return;
	}

	//ljb 20130813 add start
// 	long lUserMaxPower = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC , "UserLimitPower", 9000000);	//ljb 20130722 add
// 	CString strTemp2;
// 
// 	if (nType == PS_STEP_CHARGE)
// 	{
// 		if (lUserMaxPower < (stepData.fIref / 1000) * (stepData.fVref_Charge / 1000))
// 		{
// 			strTemp2.Format("제한 출력값(%d W)을 초과 했습니다.",lUserMaxPower);
// 			AfxMessageBox(strTemp2);
// 			return;
// 		}
// 	}
// 	else
// 	{
// 		if (lUserMaxPower < (stepData.fIref / 1000) * (stepData.fVref_DisCharge / 1000))
// 		{
// 			strTemp2.Format("제한 출력값(%d W)을 초과 했습니다.",lUserMaxPower);
// 			AfxMessageBox(strTemp2);
// 			return;
// 		}
// 
// 	}
	//ljb 20130813 add end

	COleDateTime endTime;
	m_ctrlEndTime.GetTime(endTime);
	stepData.fEndTime = endTime.GetHour()*3600+endTime.GetMinute()*60+endTime.GetSecond();		

	COleDateTime saveTime;
	m_ctrlSaveTime.GetTime(saveTime);
	stepData.fReportTime = float(saveTime.GetHour()*3600+saveTime.GetMinute()*60+saveTime.GetSecond());
	CString strTemp;
	GetDlgItem(IDC_EDIT_MS)->GetWindowText(strTemp);
	stepData.fReportTime += atof(strTemp)/1000;

	TRACE("EndTime : %f, SaveTime : %f\r\n", stepData.fEndTime, stepData.fReportTime);

	stepData.chStepNo = 1;
	//m_ScheduleInfo.AddStep_vF(&stepData);
	m_ScheduleInfo.AddStep_v1013_v2_SCH(&stepData);

	if(m_bOverChargerSet)//yulee 20180810 //yulee 20191017 OverChargeDischarger Mark
	{
		float OverChargerMinVolt = 0;
		OverChargerMinVolt = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OverChargerMinVolt", FALSE);

		if(stepData.fEndTime <= 0.0f && stepData.fEndV <= ((-OverChargerMinVolt)/1000.0) && stepData.fEndV_L <=((-OverChargerMinVolt)/1000.0)  && stepData.fEndI <= 0.0f)	//ljb 20150707 add  && stepData.fEndV_L <=0.0f  
		{
			//AfxMessageBox("종료 조건이 설정되어 있지 않습니다.");
			AfxMessageBox(Fun_FindMsg("SimpleTestDlg_OnOK_msg1","IDD_SIMPLE_TEST_DLG"));//&&
			return;
		}
	}
	else
	{
		
		if(stepData.fEndTime <= 0.0f && stepData.fEndV <=0.0f && stepData.fEndV_L <=0.0f  && stepData.fEndI <= 0.0f)	//ljb 20150707 add  && stepData.fEndV_L <=0.0f  
		{
		AfxMessageBox(Fun_FindMsg("OnOK_msg3","IDD_SIMPLE_TEST_DLG"));
		//@ AfxMessageBox("종료 조건이 설정되어 있지 않습니다.");
			return;
		}
	}

	

	//Add step3(Loop Step)
	//ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100F));
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V1013_v2_SCH)); //ksj 20180528
	stepData.chType = PS_STEP_LOOP;
	stepData.nLoopInfoCycle = 1;
	stepData.nLoopInfoGotoStep = 4;
	stepData.chStepNo = 2;
	//m_ScheduleInfo.AddStep_vF(&stepData);
	m_ScheduleInfo.AddStep_v1013_v2_SCH(&stepData); //ksj 20180528

	//Add step4(END Step)
	//ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V100F));
	ZeroMemory(&stepData, sizeof(FILE_STEP_PARAM_V1013_v2_SCH)); //ksj 20180528
	stepData.chType = PS_STEP_END;
	stepData.chStepNo = 3;
	//m_ScheduleInfo.AddStep_vF(&stepData);
	m_ScheduleInfo.AddStep_v1013_v2_SCH(&stepData); //ksj 20180528

	CDialog::OnOK();
}

void CSimpleTestDlg::OnOutofmemorySpin1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CSimpleTestDlg::OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CString strData;
	GetDlgItem(IDC_EDIT_MS)->GetWindowText(strData);

	int mtime = atol(strData);

/*	
	//end time은 500msec 이하 설정은 하지 못함 
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= MIN_TIME_INTERVAL;
		if(mtime == (500-MIN_TIME_INTERVAL))	mtime = 0;
		if(mtime < 0)		mtime = (1000-MIN_TIME_INTERVAL);
	}
	else
	{
		mtime += MIN_TIME_INTERVAL;
		if( mtime == MIN_TIME_INTERVAL)	mtime = 500;
		if(mtime >= 1000)	mtime = 0;
	}
*/
	//end time은 0msec 부터 설정 가능
	if(pNMUpDown->iDelta > 0)
	{
		mtime -= 100;
		if(mtime < 0)		mtime = (1000-100);
	}
	else
	{
		mtime += 100;
		if(mtime >= 1000)	mtime = 0;
	}

	strData.Format("%03d", mtime);
	GetDlgItem(IDC_EDIT_MS)->SetWindowText(strData);	
	
	*pResult = 0;
}