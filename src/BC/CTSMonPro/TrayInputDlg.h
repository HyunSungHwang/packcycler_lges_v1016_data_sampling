#if !defined(AFX_TRAYINPUTDLG_H__78E39440_5187_436C_A1B9_14C0CC45C87A__INCLUDED_)
#define AFX_TRAYINPUTDLG_H__78E39440_5187_436C_A1B9_14C0CC45C87A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TrayInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTrayInputDlg dialog
#include "CTSMonProDoc.h"

class CTrayInputDlg : public CDialog
{
// Construction
public:
	
	CCTSMonProDoc *m_pDoc;
	int m_nSelJigNo;
	int m_nSelModuleID;
	void SetUnitInput(BOOL bUnit = TRUE);
	void SetTitle(CString str);
	BOOL SearchTrayID(CString strID);
	BOOL SearchLocation(CString strID, int &nModuleID, int &nJigID);
	void SendTrayNoReadMsg(CString strTray, int nModuleID, int nJigNo = 1);
	UINT m_nTimer;
	void SetHideDelayTime(int nMiliSec);
	BOOL ReadUnitNo(CString strData);
	BOOL ReadTrayNo(CString strData);
	CTrayInputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTrayInputDlg)
	enum { IDD = IDD_TRAYNO_DLG , IDD2 = IDD_TRAYNO_DLG_ENG , IDD3 = IDD_TRAYNO_DLG_PL };
	CComboBox	m_ctrlJigListCombo;
	CLabel	m_ctrlTitle;
	CString	m_strTrayNo;
	CString	m_strUnitNo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrayInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strTitleText;
	BOOL m_bModeLocation;
	CWnd *m_ParentWnd;

	// Generated message map functions
	//{{AFX_MSG(CTrayInputDlg)
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual void OnOK();
	afx_msg void OnSelchangeJigListCombo();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEdit1();
	virtual void OnCancel();
	//}}AFX_MSG
	afx_msg LRESULT OnBcrscaned(WPARAM wParam,LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAYINPUTDLG_H__78E39440_5187_436C_A1B9_14C0CC45C87A__INCLUDED_)
