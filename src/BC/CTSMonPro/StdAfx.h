// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__5D395948_DE2D_409E_9EEE_DEB746DE7612__INCLUDED_)
#define AFX_STDAFX_H__5D395948_DE2D_409E_9EEE_DEB746DE7612__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4244)	//possible loss of data
#pragma warning(disable : 4100)	//unreferenced formal parameter
#pragma warning(disable : 4018) //signed/unsigned mismatch


#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
//#define POINTER_64 //yulee 20190523

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT
//ksj 20190128 : vs2010 ??? ??
#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT
//ksj end

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxsock.h>		// MFC socket extensions
#include <afxtempl.h>

#include <grid/gxall.h>


#ifndef __GDI_PLUS__
#define __GDI_PLUS__
#pragma comment(lib, "gdiplus.lib")
#include <gdiplus.h>
using namespace Gdiplus;
#endif


#include "TabGlobal.h"

//#include "MyDefine.h"
#include "mmsystem.h"
#include "ftp.h"

#include "../../BCLib/Include/PSCommonAll.h"
#include "../../BCLib/Include/PSServerAll.h"
//#include "../../../DataBase/DataBaseAll.h" //ksj 20200203

#ifdef _DEBUG
#pragma comment(lib, "../../../lib/PSServerD.lib ")
#pragma message("Automatically linking with PSServerD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../lib/PSServer.lib")
#pragma message("Automatically linking with PSServer.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../lib/PSCommonD.lib ")
#pragma message("Automatically linking with PSCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../lib/PSCommon.lib")
#pragma message("Automatically linking with PSCommon.lib By K.B.H")

#endif	//_DEBUG


#define FILE_SAVE_ITEM_REG_SET	"FileSave"

#define REG_SERIAL_CONFIG1	"Serial1"
#define REG_SERIAL_CONFIG2	"Serial2"
#define REG_SERIAL_CONFIG3	"SerialOven1"
#define REG_SERIAL_CONFIG4	"SerialOven2"
#define REG_SERIAL_CONFIG5	"SerialLoad1"
#define REG_SERIAL_CONFIG6	"SerialLoad2"

#define REG_SERIAL_POWER1	 "SerialPower1"
#define REG_SERIAL_POWER2	 "SerialPower2"

#define REG_SERIAL_CHILLER1	 "SerialChiller1"
#define REG_SERIAL_CHILLER2	 "SerialChiller2"
#define REG_SERIAL_CHILLER3	 "SerialChiller3"
#define REG_SERIAL_CHILLER4	 "SerialChiller4"

#define REG_SERIAL_PUMP1	 "SerialPump1"
#define REG_SERIAL_PUMP2	 "SerialPump2"
#define REG_SERIAL_PUMP3	 "SerialPump3"
#define REG_SERIAL_PUMP4	 "SerialPump4"

#define SerialPortUsedNum		1
//#define OVEN_SERIAL_CONFIG	"OvenConfig"
#define OVEN_SETTING		"OvenSetting"
#define SAFETY_OPERATE		"SefeyOperate"

//lmh 20120524 챔버 항목 관리
#define CHAMBR_WiseCube		0
#define CHAMBER_TD500		1
#define CHAMBER_TH500		2
#define CHAMBER_TEMP880		3
#define CHAMBER_TEMI880		4
#define CHAMBER_TEMP2300	5
#define CHAMBER_TEMI2300	6
#define CHAMBER_TEMP2500	7
#define CHAMBER_TEMI2500	8
#define CHAMBER_TEMP2520	9
#define CHILLER_TEMP2520	59
#define CHILLER_ST590		10 //ksj 20200401 : 대전LGC 칠러온도 컨트롤러 ST590, 유량 ST560


#define CHAMBER_MODBUS_NEX_START	100 //yulee 20190605
#define CHAMBER_MODBUS_NEX1000		101 //yulee 20190605
#define CHAMBER_MODBUS_NEX1100		102 //yulee 20190605
#define CHAMBER_MODBUS_NEX1200		103 //yulee 20190605

enum {//yulee 20190605 //커맨드 종류_온도/습도/상태/모드_읽기/쓰기_채널_해당모델

	//Common
	//미사용(컨트롤러에 적용X)	OP_ST_CHAMBER_VERSION		= 1,		//Chamber 정보

	//NEX1000, 1100
	PV_T_R_CH1_NEX1000_1100		= 9,		// 0x09 Ch1 현재온도 또는 칠러 온도의 현재 값
	PV_T_R_CH2_NEX1100			= 10,		// 0x10 Ch2 현재온도 또는 칠러 펌프의 유량 현재 값(NEX1100) 
	SP_T_R_CH1_NEX1000_1100		= 11,		// 0x0A Ch1 설정온도 또는 칠러 온도의 설정 값
	SP_T_R_CH2_NEX1100			= 12,		// 0x0B Ch2 설정온도 또는 칠러 펌프의 유량 설정 값(NEX1100) 

	OP_ST_R_CH1_NEX1000_1100	= 4,		//4 0x04 CH1 운전상태 또는 칠러 온도의 동작 상태
	OP_ST_R_CH2_NEX1100			= 6,		// 0x04 CH2 운전상태 또는 칠러 펌프의 유량 동작 상태(NEX1100) 
	// 운전 관련 상태정보	
	//0 PROGRAM RUN(프로그램 운전)
	//1 FIX RUN 정치 운전  
	//2 HOLD 일시정지  
	//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
	//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
	//7 RESET 정지상태 

	OP_CMD_RUN_W_NEX1000_1100		=101,		//0x65 CH1 운전 설정
	OP_CMD_RUN_W_NEX1100			=102,		//0x66 CH2 운전 설정 또는 칠러 펌프의 유량 동작(NEX1100) 


	OP_CMD_MODESET_RW_CH1_NEX1000_1100	= 110,		// 0x6E CH1 운전 MODE 설정 (0:패턴, 1:정치)
	OP_CMD_MODESET_RW_CH2_NEX1100		= 117,		// 0x75 CH2 운전 MODE 설정 (0:패턴, 1:정치)(NEX1100) 

	SP_T_SET_W_CH1_NEX1000_1100	= 103,		// 0x67 CH1 SP 값 보내기 CH1 FIX 운전시의 온도 SP 설정
	SP_T_SET_W_CH2_NEX1100		= 104,		// 0x68 CH2 SP 값 보내기 CH2 FIX 운전시의 온도 SP 설정(NEX1100) 


	//NEX1200
	PV_T_R_NEX1200			= 0,		// 0x00 현재온도 
	PV_H_R_NEX1200			= 2,		// 0x01 현재습도 
	SP_T_R_NEX1200			= 3,		// 0x03 현재 SP 온도 
	SP_H_R_NEX1200			= 4,		// 0x04 현재 SP 습도 
	OP_ST_RW_NEX1200		= 67,		// 0x43 운전상태	
	// 운전 관련 상태정보	
	//0 PROGRAM RUN(프로그램 운전)
	//1 FIX RUN 정치 운전  
	//2 HOLD 일시정지  
	//4 PT_AUTO TUNING 프로그램 운전에서의 오토 튜닝  
	//5 FIX_AUTO TUNING 정치 운전에서의 오토 튜닝  
	//7 RESET 정지상태 

	OP_CMD_MODESET_RW_NEX1200			= 99,       // 0x63 운전 MODE 설정 (0:패턴, 1:정치)	

	OP_CMD_RUN_W_NEX1200			= 1302,		//0x516 운전 설정

	SP_T_SET_RW_NEX1200			= 1299,		// 0x513 온도 SP 값 보내기 FIX 운전시의 온도 SP 설정
	SP_H_SET_RW_NEX1200			= 1300,		// 0x514 습도 SP 값 보내기 FIX 운전시의 습도 SP 설정
};

enum{
	enum_RUN  = 1,
	enum_STOP = 4
};

enum{
	enum_OP_FixMode = 1,
	enum_OP_PattMode = 0
};

// bung log define
#define LOG_OPTION_CHAMBER			1
#define LOG_OPTION_STATE_STEPTYPE	2
#define LOG_OPTION_RECEIVE			3
#define LOG_OPTION_SEND				4


#define SFTWM_BCR_SCANED	WM_USER+3100
#define SFTWM_TAGID_READED	WM_USER+3101	

#define		WM_MY_MESSAGE_CTS_START				(WM_USER + 5000)
#define		WM_MY_MESSAGE_CTS_START_OK			(WM_USER + 5001)
#define		WM_MY_MESSAGE_CTS_START_FAIL		(WM_USER + 5002)
#define		WM_MY_MESSAGE_CTS_WORK_PAUSE		(WM_USER + 5003)
#define		WM_MY_MESSAGE_CTS_CLOSE				(WM_USER + 5004)
#define		WM_MY_MESSAGE_CTS_STEP_END_DATA		(WM_USER + 5010)
#define		WM_MY_MESSAGE_CTS_STOP					(WM_USER + 5016)

#define		WM_MY_MESSAGE_CTS_BMA_CH_INFO			(WM_USER + 5017) // 2010-01-13

#define		WM_MY_MESSAGE_CTS_ISOLATION				(WM_USER + 5018)
#define		WM_MY_MESSAGE_CTS_CELL_ERROR			(WM_USER + 5019)

typedef struct tag_Barcode_Info	{
	char szTagID[64];
	char szReserved[64];
} BAR_TAG_INFOMATION;

typedef struct tag_SerialPortConfigData {
	UINT	nPortNum; 
	UINT	nPortBaudRate;
	char	portParity;
	UINT	nPortDataBits;
	UINT	nPortStopBits;
	int		nPortEvents;
	UINT	nPortBuffer;
} SERIAL_CONFIG;

typedef enum {
	/* 0 CC MODE*/ SL_MODE_CC,
	/* 1 CV MODE*/ SL_MODE_CV,
	/* 2 CR MODE*/ SL_MODE_CR,
	/* 3 CP MODE*/ SL_MODE_CP

}sl200ModeType;

#define		 ID_LOAD_NONE		0
#define		 ID_LOAD_ON			1
#define		 ID_LOAD_OFF		2
#define		 ID_LOAD_CP			3
#define		 ID_LOAD_CC			4
#define		 ID_LOAD_CV			5
#define		 ID_LOAD_CR			6

#include "NameVer.h"
#include "NameInc.h"
#include "NameRes.h"
#include "NameHeader.h"

extern CString g_AppPath;
extern APPINFO g_AppInfo;
extern CString g_AppIni;
extern CString g_AppTempPath;
//extern int m_FlagBackgroundColor; //lyj 20200916 Flicker

typedef struct tag_CanRxSave {
	int CanType;
	int CanRxBaudrate;
	BOOL CanRxExtendedIDMode;
	int SJW;
	int BMSType;
	BOOL bEnable;
} CAN_RX_SAVE;

typedef struct tag_CanTxSave {
	int CanType;
	int CanRxBaudrate;
	BOOL CanRxExtendedIDMode;
	int SJW;
	int BMSType;
	BOOL bEnable;
} CAN_TX_SAVE;

extern CAN_RX_SAVE gst_CanRxSave;
extern CAN_TX_SAVE gst_CanTxSave;

extern CAN_RX_SAVE gst_CanRxSaveSlave;
extern CAN_TX_SAVE gst_CanTxSaveSlave;


extern CString g_strpathIni;
extern int gi_Language;
void init_Language();
CString Fun_FindMsg(CString strMsg, CString strFindMsg);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__5D395948_DE2D_409E_9EEE_DEB746DE7612__INCLUDED_)
