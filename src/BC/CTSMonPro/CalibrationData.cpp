// CalibrationData.cpp: implementation of the CCalibrationData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CalibrationData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCalibrationData::CCalibrationData()
{
	m_nState = CAL_DATA_EMPTY;
}

CCalibrationData::~CCalibrationData()
{

}

//Load Calibration Data from file
BOOL CCalibrationData::LoadDataFromFile(CString strFileName)
{

	if(strFileName.IsEmpty())
	{
		TRACE("교정 파일을 발견 할 수 없습니다.\n");//yulee 20190405 check
		return FALSE;
	}
	
	FILE *fp = fopen(strFileName, "rb");
	if(fp == NULL)
	{
		TRACE("교정 파일을 발견할 수 없습니다.\n");//yulee 20190405 check
		return FALSE;
	}

	PS_FILE_ID_HEADER fileHeader;
	if(fread(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}
	//file Header 검사
	if(fileHeader.nFileID != CAL_FILE_ID)
	{
		fclose(fp);
		return FALSE;
	}

//	//Calibration 포인트 정보 Read
	if(m_PointData.ReadDataFromFile(fp) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}
	//Calibraton값 Load
	if(m_ChCaliData.ReadDataFromFile(fp) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}
	fclose(fp);

	m_nState = CAL_DATA_SAVED;
	m_strCalTime.Format("%s", fileHeader.szCreateDateTime);

	return TRUE;
}

BOOL CCalibrationData::SaveDataToFile(CString strFileName)
{

	if(strFileName.IsEmpty())
	{
		TRACE("교정 파일이름을 발견할 수 없습니다.\n");//yulee 20190405 check
		return FALSE;
	}
	
	FILE *fp = fopen(strFileName, "wb");
	if(fp == NULL)
	{
		TRACE("교정 파일을 발견 할 수 없습니다.\n");//yulee 20190405 check
		return FALSE;
	}

	PS_FILE_ID_HEADER fileHeader;
	COleDateTime curTime = COleDateTime::GetCurrentTime();
	ZeroMemory(&fileHeader, sizeof(PS_FILE_ID_HEADER));
	fileHeader.nFileID = CAL_FILE_ID;
	fileHeader.nFileVersion = 0x1000;
	sprintf(fileHeader.szCreateDateTime, "%s", curTime.Format());
	strcpy(fileHeader.szDescrition, "PNE Calibraion File");

	if(fwrite(&fileHeader, sizeof(PS_FILE_ID_HEADER), 1, fp) < 1)
	{
		fclose(fp);
		return FALSE;
	}

//	//Calibration 포인트 정보 Read
	if(m_PointData.WriteDataToFile(fp) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}
	//Calibraton값 Load
	if(m_ChCaliData.WriteDataToFile(fp) == FALSE)
	{
		fclose(fp);
		return FALSE;
	}
	fclose(fp);

	m_nState = CAL_DATA_SAVED;
	
	m_strCalTime = curTime.Format();

	return TRUE;
}


CString CCalibrationData::GetUpdateTime()
{
	return m_strCalTime;
}

int CCalibrationData::GetState()
{
	return m_nState;
}

void CCalibrationData::SetEdited()
{
	m_nState = CAL_DATA_MODIFY;
}

BOOL CCalibrationData::IsLoadedData()
{
	if(	m_nState == CAL_DATA_EMPTY)		return FALSE;

	return TRUE;
}

