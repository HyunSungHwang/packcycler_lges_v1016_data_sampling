// ChillerCtrl.cpp: implementation of the CChillerCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
 

#include "../CTSMonPro.h"
#include "ChillerCtrl.h"

#include "../MainFrm.h"
#include "../CTSMonPro.h"
#include "../CTSMonProDoc.h"
#include "../CTSMonProView.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChillerCtrl::CChillerCtrl()
{	
	m_hThread=NULL;

	m_nItem=-1;
	m_iError=0;
	m_nCHNO=0;
	m_nChillerPlay=0;
	m_iReqStop=0;

	ZeroMemory(&m_pChillerData,sizeof(SFT_CHAMBER_VALUE));
}

CChillerCtrl::~CChillerCtrl()
{
	GWin::win_ThreadEnd2(m_hThread);
}

void CChillerCtrl::onResetData()
{	
	m_nItem=-1;
	m_iError=0;	
	m_nCHNO=0;
	m_nChillerPlay=0;
	m_iReqStop=0;

	ZeroMemory(&m_pChillerData,sizeof(SFT_CHAMBER_VALUE));
}

void CChillerCtrl::onSetInit(int nItem)
{	
	m_nItem=nItem;//0 start	
		
	if(!m_hThread)
	{
		DWORD hThreadId=0;
		m_hThread = CreateThread(NULL, 0, ChillerDealThread,
		                     (LPVOID)this, 0, &hThreadId);	
	}	
}

CString CChillerCtrl::onGetLogType()
{
	int nCHNO=m_nCHNO;
	
	CString strLogType;
	strLogType.Format(_T("chiller_%d"),nCHNO);
    return strLogType;
}

DWORD WINAPI CChillerCtrl::ChillerDealThread(LPVOID arg)
{//cny 201809
	CChillerCtrl *obj=(CChillerCtrl *)arg;
	if(!obj) return 0;
	
	Sleep(1000);

	if(!mainFrm)  return 0;
	if(!mainView) return 0;
	CCTSMonProDoc *pMainDoc=mainView->GetDocument();
	if(!pMainDoc) return 0;
	
	static int nDevice = 0;
	int  nOvenID;
	long lOvenState;
	int  m_uLogCnt=1;
	int  nItem=obj->m_nItem;

	int  nCHNO=obj->m_nCHNO;	
	CString strLogType=obj->onGetLogType();

	CString strTemp;
	strTemp.Format("CHNO:%d Chiller%d Thread Start(Model %d)",
		           nCHNO,nItem,obj->m_ctrlOven.GetOvenModelType());
	log_All(strLogType,_T("thread"),strTemp);

	BOOL bUseChillerRequestCtrl = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseChillerRequestCtrl", 1); //ksj 20201117 : 다른 명령 전송중에 request 명령 보내지 않도록 잠금 기능 활성화 여부

	while(obj->m_ctrlOven.m_bUseOvenCtrl)
	{
		while(obj->m_ctrlOven.m_bPortOpen)
		{
			//ksj 20201022 //////////////////////
			//현재 다른 쓰레드에서 Modbus 명령 전송중인 경우 Request하지 않도록 잠겨있는지 확인한다.
			//크리티컬 섹션과는 조금 다른 개념임.
			//우선 Modbus에만 제한적으로 적용함. 향후 일반 Temp계열에도 필요한 경우 확장 적용 필요
			//if(obj->m_ctrlOven.GetOvenModelType() > CHAMBER_MODBUS_NEX_START) 
			if(obj->m_ctrlOven.GetOvenModelType() > CHAMBER_MODBUS_NEX_START || bUseChillerRequestCtrl) //ksj 20201117: nex가 아니여도, 레지스트리 옵션에 따라 요청 잠금 기능 활성화.
			{
				if(obj->m_ctrlOven.RequestGetLock())
				{
					Sleep(1000);
					continue;
				}
			}	
			//ksj end ////////////////////////////

			for (nOvenID=0; nOvenID < obj->m_ctrlOven.GetOvenCount() ; nOvenID++)
			{
				//Oven 사용 여부
				if (obj->m_ctrlOven.GetUseOven(nOvenID) == FALSE) continue;
				
				//Oven 상태 체크
				lOvenState = obj->m_ctrlOven.GetOvenStatus(nOvenID);
				if (lOvenState != 0)
				{
					//전채널 일시 정지 => Warring Message 표시
					//ljb 임시	Fun_AlarmOvenState(nOvenID, lOvenState);
					//return;
				}
				//------------------------------------------
				if(obj->m_iReqStop==1)
				{
					Sleep(1000);
					continue;
				}
				//------------------------------------------
				if(obj->m_ctrlOven.GetDisconnectCnt(nOvenID) < 10) //yulee 20190614_*
				{
					//현재 Oven 통신 가능
					//현재 Oven의 상태값을 Update한다.	
					//if (obj->m_ctrlOven.RequestCurrentValue(nDevice,nOvenID ) == 0)	
					if (obj->m_ctrlOven.RequestCurrentValue(nDevice,nOvenID ) != 1)	 //ksj 20201117 : 값이 1일때만 성공이다.
					{//lmh 20111115 2단챔버 연동시 디바이스와 오븐아이디가 바뀌어야함
						obj->m_ctrlOven.IncreaseDisconnectTime(nOvenID);//통신 두절 count 증가
						
						if(obj->m_ctrlOven.m_ctx != NULL)
						{
							modbus_flush(obj->m_ctrlOven.m_ctx);
						}
//						modbus_close(obj->m_ctrlOven.m_ctx);
// 						Sleep(500);
// 						modbus_connect(obj->m_ctrlOven.m_ctx);


						m_uLogCnt++;
						if((m_uLogCnt % 90) == 0)	
						{// log 남기기
							strTemp.Format("Chiller %d No Response.(Model %d, ID %d)",
								          nItem,obj->m_ctrlOven.GetOvenModelType(),nOvenID+1);
							log_All(_T("chiller"),_T("state"),strTemp);
							m_uLogCnt = 0;
						}
					}
					else
					{
						//ksj 20201017 : 정상적으로 RequestCurrentValue 읽혔다면
						//디스커넥 카운트를 리셋해준다.
						//기존 로직은 디스커넥카운트가 누적되면 언젠가는 값을 가져오지 않게 될듯?
						obj->m_ctrlOven.ResetDisconnectCnt(nOvenID); 
					}
				}
				/*else //ksj 20201117 : 주석처리
				{
					//현재 Oven 통신 불능
					//10초 이상이면  전송하지 않음
					obj->m_ctrlOven.SetLineState(nOvenID, FALSE);
					obj->m_ctrlOven.ClearCurrentData();
					obj->m_ctrlOven.ResetDisconnectCnt(nOvenID);					
				}*/


				//ksj 20201117 : 칠러 재접속 추가
// 				if(obj->m_ctrlOven.GetLineState(nOvenID) == FALSE)
// 				{
// 					obj->m_ctrlOven.IncreaseDisconnectTime(nOvenID);
// 				}

				if(obj->m_ctrlOven.GetDisconnectCnt(nOvenID) >= 5)
				{
					obj->m_ctrlOven.ClearCurrentData();
					obj->m_ctrlOven.ResetDisconnectCnt(nOvenID);

					// 201029 HKH Chamber Reconnect 관련 기능 개선
					//pDoc->WriteSysLog("챔버에서 응답이 없습니다. 재연결 시도..", CT_LOG_LEVEL_SYSTEM);
					strTemp.Format("Chiller %d no response... try reconnect....(Model %d, ID %d)",
						nItem,obj->m_ctrlOven.GetOvenModelType(),nOvenID+1);
					log_All(_T("chiller"),_T("state"),strTemp);

					BOOL bRetVal = obj->m_ctrlOven.ReConnect(nOvenID, nDevice);

					if (bRetVal == TRUE)//	pDoc->WriteSysLog("챔버 재연결 성공", CT_LOG_LEVEL_SYSTEM);
					{
						strTemp.Format("Chiller %d Reconnect success.(Model %d, ID %d)",
							nItem,obj->m_ctrlOven.GetOvenModelType(),nOvenID+1);
						log_All(_T("chiller"),_T("state"),strTemp);
					}
					else//					pDoc->WriteSysLog("챔버 재연결 실패", CT_LOG_LEVEL_SYSTEM);			
					{
						strTemp.Format("Chiller %d Reconnect failure.(Model %d, ID %d)",
							nItem,obj->m_ctrlOven.GetOvenModelType(),nOvenID+1);
						log_All(_T("chiller"),_T("state"),strTemp);
					}
				}
				//ksj end
				
				//Start Dlg가 떠있는동안은 Reset 시킴 지연을 위해 반듯이 필요
				//if(m_bBlockFlag)
				//{
				//	pDoc->m_nRunCmdDelayTime = 0;
				//}
				//else
				//{
				//	pDoc->m_nRunCmdDelayTime += (CHAMBER_TIMER/1000);		//2sec timer
				//}
				
				//Oven 연동 채널이 모두 Oven연동 Pause 상태 일때 -> 챔버 온도 변경 및 Oven 연동 Continue 전송			
				if(obj->m_ctrlOven.GetLineState(nOvenID)) 	
				{
					//pMainDoc->CheckOven_Line1(nOvenID);
				}

			}// end for (nOvenID=0; nOvenID < pMainDoc->m_ctrlOven1.GetOvenCount() ; nOvenID++)
			Sleep(1000);
			
		}// end while
		
		Sleep(1000);
	}
	strTemp.Format("CHNO:%d Chiller%d Thread End",nCHNO,nItem);
	log_All(strLogType,_T("thread"),strTemp);
	return 1;
}
