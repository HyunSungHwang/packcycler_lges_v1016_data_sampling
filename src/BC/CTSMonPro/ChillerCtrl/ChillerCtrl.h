// ChillerCtrl.h: interface for the CChillerCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILLERCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
#define AFX_CHILLERCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_

#include "../OvenCtrl.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CChillerCtrl  
{
public:
	CChillerCtrl();
	virtual ~CChillerCtrl();
	
	COvenCtrl         m_ctrlOven;
	SFT_CHAMBER_VALUE m_pChillerData;
	
	int    m_nItem;
	int    m_iError;
	int    m_nCHNO;
    int    m_nChillerPlay;//1-ON,2-OFF
	int    m_iReqStop;

	HANDLE m_hThread;
	
	void onResetData();
	void onSetInit(int nItem);
	CString onGetLogType();

	static DWORD WINAPI ChillerDealThread(LPVOID arg);

protected:

};

#endif // !defined(AFX_CHILLERCTRL_H__F23127BE_F0C9_49BB_8C4C_3D2C17C0F71D__INCLUDED_)
