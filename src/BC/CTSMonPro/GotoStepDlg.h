#if !defined(AFX_GOTOSTEPDLG_H__5C57430A_C83F_43F0_937B_2A0C5DC868D8__INCLUDED_)
#define AFX_GOTOSTEPDLG_H__5C57430A_C83F_43F0_937B_2A0C5DC868D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GotoStepDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGotoStepDlg dialog
#include "CTSMonProDoc.h"
//#include "XPButton.h"

class CGotoStepDlg : public CDialog
{
// Construction
public:

	void Fun_initList();
	BOOL Fun_CheckMovableStep(int nStepNo);
	CGotoStepDlg(CCTSMonProDoc *pDoc,CWnd* pParent = NULL);   // standard constructor
	CCTSMonProDoc *m_pDoc;
	int		m_nModuleID;
	int		m_nChannelID;
	int		m_nNowStepNo;

	CImageList m_imgList;

// Dialog Data
	//{{AFX_DATA(CGotoStepDlg)
	enum { IDD = IDD_GOTO_STEP_DLG , IDD2 = IDD_GOTO_STEP_DLG_ENG , IDD3 = IDD_GOTO_STEP_DLG_PL };
	CListCtrl	m_ctrlScheduleList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGotoStepDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGotoStepDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GOTOSTEPDLG_H__5C57430A_C83F_43F0_937B_2A0C5DC868D8__INCLUDED_)
