#pragma once

#include "CTSMonProDoc.h"
#include "CTSMonProView.h"

#define WM_UPDATE_RESERVE_CNT		WM_USER + 2222
#define WM_UPDATE_RESERVE_FINISH	WM_USER + 2223

// CDlgUpdateReserve 대화 상자입니다.

class CDlgUpdateReserve : public CDialog
{
	DECLARE_DYNAMIC(CDlgUpdateReserve)

public:
	CDlgUpdateReserve(CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlgUpdateReserve();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_UPDATE_RESERVE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:
	static UINT ThreadAutoUpdateReservation(LPVOID pParam);

public:
	afx_msg void OnBnClickedBtnUpdateCancel();
	afx_msg LRESULT OnUpdateReserveCnt(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateReserveFinish(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	void Finish();


public:
	CCTSMonProDoc *m_pDoc;

	BOOL m_bUpdateReserve;
	BOOL m_bThreadFinish;

	CLabel m_sUpdateReserve;
	CLabel m_sReserveState;
};
