#if !defined(AFX_STARTDLGEX_H__8FA871EB_975E_403C_B4F9_2B781D883D96__INCLUDED_)
#define AFX_STARTDLGEX_H__8FA871EB_975E_403C_B4F9_2B781D883D96__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StartDlg.h : header file
// 20160615 ksj
// CStartDlg에서 작업 예약 기능 추가 -> CStartDlgEx

/////////////////////////////////////////////////////////////////////////////
// CStartDlgEx dialog
#include  "CTSMonProDoc.h"
#include "XPButton.h"

#define MAX_RECENT_TEST_NAME	10
#define RECENT_NAME_REG_SECTION		"Recent Test"
#define RESERVATION_REGISTRY_KEY	"Reservation"

class CStartDlgEx : public CDialog
{
// Construction
public:	
	
	void SetReserveMode();
	BOOL m_bEnableStepSkip;
	BOOL m_bReserveMode;
	BOOL m_nReturn;
	UINT	m_nStartStep;
	UINT    m_nStartOptChamber;			//챔버 동작 모드
	UINT	m_uiChamberCheckTime;		//챔버 온도 비교 시작 시간
	UINT	m_uiChamberCheckFixTime;		//챔버 온도 비교 시작 시간
	void Fun_CheckUseOven();
	void UpdateOvenMode();
	void InitGrid();

	BOOL m_bChamberContinue; //2014.11.21 챔버 대기모드 사용유무 추가
	BOOL m_IsChamberContinue;//2014.11.21 챔버 대기모드 사용유무 추가	
	BOOL	m_bIsCanModeCheck;
	UINT m_nCboMuxChoice;//yulee 20181025 
	CComboBox m_CboMuxChoiceRsv; //yulee 20181025
//	BOOL m_bModuleCmd;
	void SetInfoData(CScheduleData *pSchedule, CPtrArray *aPtrCh);

	CString GetTestPath();
	BOOL DeletePath(CString strPath, CProgressWnd *pPrgressWnd = NULL);
	CString GetFileName();
	int CheckFileName();
	CStartDlgEx(CCTSMonProDoc *pDoc, CWnd* pParent = NULL);   // standard constructor
	//CString m_strMuxChoice;//yulee 20181025

// Dialog Data
	//{{AFX_DATA(CStartDlgEx)
	enum { IDD = IDD_START_DLG , IDD2 = IDD_START_DLG_ENG ,IDD3 = IDD_START_DLG_PL };
	CButton	m_ChkMuxLink;
	CComboBox	m_ctrlCboCanCheck;
	CDateTimeCtrl	m_ctrlCheckFixTime;
	CDateTimeCtrl	m_ctrlCheckTime;
	CComboBox	m_StepCombo;
	CComboBox	m_CycleCombo;
	CXPButton	m_btnCancel;
	CXPButton	m_btnOK;
	CXPButton	m_btnPrev;
	CLabel	m_strDestination;
	CComboBox	m_ctrlTestNameCombo;
	CString	m_strWorker;
	CString	m_strLot;
	CString	m_strComment;
	CString	m_strDataPath;
	UINT	m_nStartCycle;
	float	m_fChamberFixTemp;
	UINT	m_uiChamberPatternNum;
	float	m_fChamberDeltaTemp;
	UINT	m_uiChamberDelayTime;
	float	m_fChamberDeltaFixTemp;
	BOOL	m_chkLoadLink;
	BOOL	m_bIsMuxLink;
	BOOL	m_chkChamberContinue;
	BOOL	m_chkPwrSplyOffWorkEnd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStartDlgEx)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CGridCtrl m_Grid;
	CPtrArray		*m_paPtrCh;
	void AddStepInCycle(int nCycle = 0);
	CCTSMonProDoc *m_pDoc;
	CStringList m_strRecentNameList;
	void UpdateState();
	CString m_strTestName;


	CScheduleData *m_pSchedule;
//	CWordArray		*m_pSelChArray;

	// Generated message map functions
	//{{AFX_MSG(CStartDlgEx)
	afx_msg void OnEditchangeTestNameCombo();
	virtual BOOL OnInitDialog();
	afx_msg void OnEditupdateTestNameCombo();
	afx_msg void OnSelchangeTestNameCombo();
	virtual void OnOK();
	afx_msg void OnPrevButton();
	virtual void OnCancel();
	afx_msg void OnSchDetailButton();
	afx_msg void OnDataFolderButton();
	afx_msg void OnSelchangeCycleCombo();
	afx_msg void OnChkChamberFix();
	afx_msg void OnChkChamberProg();
	afx_msg void OnChkChamberStep();
	afx_msg void OnChkChamberNo();
	afx_msg void OnChkChamberNoAndStop();
	afx_msg void OnSelchangeCboMuxSta();
	afx_msg void OnChkMuxLink();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARTDLGEX_H__8FA871EB_975E_403C_B4F9_2B781D883D96__INCLUDED_)