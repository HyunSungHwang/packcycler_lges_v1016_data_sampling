// StartDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "StartDlg.h"
#include "WorkWarnin.h"

#include "FolderDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStartDlg dialog


CStartDlg::CStartDlg(CCTSMonProDoc *pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CStartDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CStartDlg::IDD2):	
	(CStartDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CStartDlg)
	m_strWorker = _T("");
	m_strLot = _T("");
	m_strComment = _T("");
	m_strDataPath = _T("");
	m_nStartCycle = 1;
	m_fChamberFixTemp = 0.0f;
	m_uiChamberPatternNum = 0;
	m_fChamberDeltaTemp = 0.0f;
	m_uiChamberDelayTime = 0;
	m_fChamberDeltaFixTemp = 0.0f;
	m_bIsCanModeCheck = FALSE;
	m_chkLoadLink = FALSE;
	m_bIsMuxLink = FALSE;
	m_nStartStep = 0;
	m_chkChamberContinue = FALSE;
	m_chkPwrSplyOffWorkEnd = FALSE; //yulee 20190615_2
	//}}AFX_DATA_INIT

	m_nReturn = IDCANCEL;
	m_pSchedule = NULL;
	m_paPtrCh = NULL;//yulee 20190627
	//2014.11.19 챔버 대기모드 동작 
	m_IsChamberContinue = TRUE;
	
	ASSERT(pDoc);
	m_pDoc = pDoc;

	m_bEnableStepSkip = FALSE;

}


void CStartDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStartDlg)
	DDX_Control(pDX, IDC_CBO_MUX_STA, m_CboMuxChoiceRsv);
	DDX_Control(pDX, IDC_CBO_CAN_CHECK, m_ctrlCboCanCheck);
	DDX_Control(pDX, IDC_DATETIMEPICKER3, m_ctrlCheckFixTime);
	DDX_Control(pDX, IDC_DATETIMEPICKER2, m_ctrlCheckTime);
	DDX_Control(pDX, IDC_STEP_COMBO, m_StepCombo);
	DDX_Control(pDX, IDC_CYCLE_COMBO, m_CycleCombo);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_PREV_BUTTON, m_btnPrev);
	DDX_Control(pDX, IDC_SEL_CH_STATIC, m_strDestination);
	DDX_Control(pDX, IDC_TEST_NAME_COMBO, m_ctrlTestNameCombo);
	DDX_Text(pDX, IDC_WORKER_NAME_EDIT, m_strWorker);
	DDX_Text(pDX, IDC_LOT_NO_EDIT, m_strLot);
	DDX_Text(pDX, IDC_COMMENT_EDIT, m_strComment);
	DDX_Text(pDX, IDC_EDIT1, m_strDataPath);
	DDX_Text(pDX, IDC_CYCLE_EDIT, m_nStartCycle);
	DDX_Text(pDX, IDC_EDIT_CHAMBER_FIX_TEMP, m_fChamberFixTemp);
	DDX_Text(pDX, IDC_EDIT_CHAMBER_PATTERN_NUM, m_uiChamberPatternNum);
	DDX_Text(pDX, IDC_EDIT_CHAMBER_DELTA_TEMP, m_fChamberDeltaTemp);
	DDX_Text(pDX, IDC_EDIT_CHAMBER_PATTERN_DELAY_TIME, m_uiChamberDelayTime);
	DDX_Text(pDX, IDC_EDIT_CHAMBER_DELTA_FIX_TEMP, m_fChamberDeltaFixTemp);
	DDX_Check(pDX, IDC_CHK_CAN_MODE, m_bIsCanModeCheck);
	DDX_Check(pDX, IDC_CHK_LOAD_LINK, m_chkLoadLink);
	DDX_Check(pDX, IDC_CHK_MUX_LINK, m_bIsMuxLink);
	DDX_Control(pDX, IDC_CUSTOM_INPUT, m_Grid); 
	DDX_Check(pDX, IDC_CHK_CHAMBER_CONTINUE, m_chkChamberContinue);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHK_PWR_OFF_WORKEND, m_chkPwrSplyOffWorkEnd);
}


BEGIN_MESSAGE_MAP(CStartDlg, CDialog)
	//{{AFX_MSG_MAP(CStartDlg)
	ON_CBN_EDITCHANGE(IDC_TEST_NAME_COMBO, OnEditchangeTestNameCombo)
	ON_CBN_EDITUPDATE(IDC_TEST_NAME_COMBO, OnEditupdateTestNameCombo)
	ON_CBN_SELCHANGE(IDC_TEST_NAME_COMBO, OnSelchangeTestNameCombo)
	ON_BN_CLICKED(IDC_PREV_BUTTON, OnPrevButton)
	ON_BN_CLICKED(IDC_SCH_DETAIL_BUTTON, OnSchDetailButton)
	ON_BN_CLICKED(IDC_DATA_FOLDER_BUTTON, OnDataFolderButton)
	ON_CBN_SELCHANGE(IDC_CYCLE_COMBO, OnSelchangeCycleCombo)
	ON_BN_CLICKED(IDC_CHK_CHAMBER_FIX, OnChkChamberFix)
	ON_BN_CLICKED(IDC_CHK_CHAMBER_PROG, OnChkChamberProg)
	ON_BN_CLICKED(IDC_CHK_CHAMBER_STEP, OnChkChamberStep)
	ON_BN_CLICKED(IDC_CHK_CHAMBER_NO, OnChkChamberNo)
	ON_BN_CLICKED(IDC_CHK_CHAMBER_NO_AND_STOP, OnChkChamberNoAndStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStartDlg message handlers

void CStartDlg::OnEditchangeTestNameCombo() 
{
	// TODO: Add your control notification handler code here
//	CString str = GetDlgItem(IDC_TEST_NAME_COMBO)->GetWindowText();
//	CString strTestName;
//	m_ctrlTestNameCombo.GetWindowText(strTestName);
//	TRACE("%s\n", strTestName);
}

BOOL CStartDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here

	//최근 입력한 시험명 10개를 표시한다.
	char szEntry[_MAX_PATH];
	CString strRecentName, defultName;
	
	//기본 Data 위치
	::GetModuleFileName(AfxGetApp()->m_hInstance, szEntry, _MAX_PATH);
	CString szCurDir = CString(szEntry).Mid(0, CString(szEntry).ReverseFind('\\'));
	defultName.Format("%s\\Data", szCurDir);
	
	//Data Path를 구함
	m_strDataPath = AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "Data Path", defultName);	

	//2014.12.22 기본 설정 숨김
	BOOL UseCanModeCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC,"UseCanModeCheck",0);
	if(!UseCanModeCheck){
		GetDlgItem(IDC_CBO_CAN_CHECK)->EnableWindow(FALSE);
		//m_bIsCanModeCheck =FALSE;
	}
	else
	{
		GetDlgItem(IDC_CBO_CAN_CHECK)->EnableWindow(TRUE);
		//m_bIsCanModeCheck =TRUE;
	}

	BOOL IsMuxModeCheck = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC,"UseMuxLink",0);
	m_bIsMuxLink =FALSE; 
	if(!IsMuxModeCheck){
		GetDlgItem(IDC_CHK_MUX_LINK)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_MUX_T)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_MUX_T2)->ShowWindow(FALSE);
		GetDlgItem(IDC_CHK_MUX_LINK)->ShowWindow(FALSE);
		GetDlgItem(IDC_CBO_MUX_STA)->ShowWindow(FALSE); //yulee 20181026
	}
	else
	{
		GetDlgItem(IDC_STATIC_MUX_T)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_MUX_T2)->ShowWindow(TRUE);
		GetDlgItem(IDC_CHK_MUX_LINK)->ShowWindow(TRUE);
	}

	BOOL bUseLoader = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseLoader", FALSE);		//ljb 20151218 add
	if (bUseLoader)
	{
		GetDlgItem(IDC_CHK_LOAD_LINK)->ShowWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_CHK_LOAD_LINK)->ShowWindow(FALSE);
	}


	BOOL bFirst = TRUE;
	defultName = "Test";
	
	for(int i =0; i<MAX_RECENT_TEST_NAME; i++)
	{
		//Name0 ~ Name9 => 최근 이름 10개 
		sprintf(szEntry, "Name%d", i);
		strRecentName = AfxGetApp()->GetProfileString(RECENT_NAME_REG_SECTION, szEntry);
		if(strRecentName.IsEmpty() == FALSE)
		{
			m_strRecentNameList.AddTail(strRecentName);
			if(!bFirst)		//default로 이름으로 표시 하므로 가장 처음것음 리스트에 포함 안함 
			{
				m_ctrlTestNameCombo.AddString(strRecentName);
			}
			else
			{
				bFirst = FALSE;
				defultName= strRecentName;
			}
		}
	}

	//가장 최근에 입력한 이름을 default 이름으로 표시
	m_ctrlTestNameCombo.SetWindowText(defultName);

	//나머지는 표시하지 않음 
	//2006/4/27
//	m_strLot = AfxGetApp()->GetProfileString(RECENT_NAME_REG_SECTION, "LotNo");
//	m_strWorker = AfxGetApp()->GetProfileString(RECENT_NAME_REG_SECTION, "Worker");
//	m_strComment = AfxGetApp()->GetProfileString(RECENT_NAME_REG_SECTION, "Descript");

	if(m_pSchedule )
	{
		GetDlgItem(IDC_SCH_NAME_EDIT)->SetWindowText(m_pSchedule->GetScheduleName());
	}
	else
	{
		GetDlgItem(IDC_SCH_DETAIL_BUTTON)->EnableWindow(FALSE);
	}

	GetDlgItem(IDC_CBO_MUX_STA)->ShowWindow(FALSE);//yulee 20181025
	
	/*if(m_pSelChArray)
	{
		strRecentName.Empty();
	
		//모듈에 전송하는 명령
		if(m_bModuleCmd)
		{
			strRecentName = "MD ";
			for(int i=0; i< m_pSelChArray->GetSize(); i++)
			{
				defultName.Format("%d,", m_pSelChArray->GetAt(i));
				strRecentName += defultName;
			}
			strRecentName += "의 전송가능한 모든 채널";
//			GetDlgItem(IDC_SEL_CH_EDIT)->SetWindowText(strRecentName);
//			m_strDestination.SetText(strRecentName);
			defultName.Format("총 %d 모듈",m_pSelChArray->GetSize());
			GetDlgItem(IDC_TOTAL_CH_STATIC)->SetWindowText(defultName);
//			m_strDestination.SetTextColor(RGB(255, 0, 0));
//			m_strDestination.FlashText(TRUE);
			//m_strDestination.FlashBackground(TRUE);
		}
		else
		{
			strRecentName = "CH ";
			for(int i=0; i<m_pSelChArray->GetSize(); i++)
			{
				defultName.Format(" %d,", m_pSelChArray->GetAt(i)+1);

				strRecentName += defultName;
			}
//			GetDlgItem(IDC_SEL_CH_EDIT)->SetWindowText(strRecentName);
//			m_strDestination.SetText(strRecentName);
			defultName.Format("총 %d CH",m_pSelChArray->GetSize());
			GetDlgItem(IDC_TOTAL_CH_STATIC)->SetWindowText(defultName);
		}
	}*/

	if(m_paPtrCh)
	{
		strRecentName.Empty();
	
		for(int i=0; i< m_paPtrCh->GetSize(); i++)
		{
			CCyclerChannel *pCh = (CCyclerChannel *)m_paPtrCh->GetAt(i);
			defultName.Format("M%02dC%02d, ", pCh->GetModuleID(), pCh->GetChannelIndex()+1);
			strRecentName += defultName;
		}
//		strRecentName += "의 전송가능한 모든 채널";
		m_strDestination.SetText(strRecentName);
		m_strDestination.SetTextColor(RGB(255, 0, 0));
		m_strDestination.FlashText(TRUE);
		
		defultName.Format(Fun_FindMsg("OnInitDialog_msg1","IDD_START_DLG"),m_paPtrCh->GetSize());
		//@ defultName.Format("총 %d CH",m_paPtrCh->GetSize());
		GetDlgItem(IDC_TOTAL_CH_STATIC)->SetWindowText(defultName);
	}

	GetDlgItem(IDC_TEST_NAME_COMBO)->SetFocus();
	GetDlgItem(IDC_CYCLE_EDIT)->EnableWindow(m_bEnableStepSkip);
	m_CycleCombo.EnableWindow(m_bEnableStepSkip);
	m_StepCombo.EnableWindow(m_bEnableStepSkip);


	CStep *pStep;
	m_CycleCombo.AddString(Fun_FindMsg("OnInitDialog_msg2","IDD_START_DLG"));
	//@ m_CycleCombo.AddString("처음부터...");
	int nCount = 0;
	for(int i =0; i<m_pSchedule->GetStepSize(); i++)
	{
		pStep = m_pSchedule->GetStepData(i);
		if(pStep)
		{
			if(pStep->m_type == PS_STEP_ADV_CYCLE)
			{
				nCount++;
				defultName.Format("Cyc #%d", nCount);
				m_CycleCombo.AddString(defultName);
			}
		}
	}
	m_CycleCombo.SetCurSel(0);
	AddStepInCycle();
	
	UpdateOvenMode();

	InitGrid();
	//ljb 챔버연동 안함 선택
	CButton* pOption = (CButton*)GetDlgItem(IDC_CHK_CHAMBER_NO);
	pOption->SetCheck(1);
	m_nStartOptChamber = 4;

	//yulee 20180814 챔버연동 안함(챔버 중지)
	CButton* pOption2;
	if(pOption == NULL)
	{
		pOption2 = (CButton*)GetDlgItem(IDC_CHK_CHAMBER_NO_AND_STOP);
		pOption2->SetCheck(1);
		pOption->SetCheck(0);
		m_nStartOptChamber = 5;
	}


	//ljb 20110112
	CString strTemp;
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Fix Temp","25.0");
	m_fChamberFixTemp = atof(strTemp);

	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Delta Fix Temp","1");
	m_fChamberDeltaFixTemp = atof(strTemp);
	
	strTemp = AfxGetApp()->GetProfileString(REG_SERIAL_CONFIG3, "Delta Temp","1");
	m_fChamberDeltaTemp = atof(strTemp);

	m_uiChamberPatternNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Pattern No", 1);
	m_uiChamberDelayTime = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Start Delay Time", 4);

// 	m_fChamberFixTemp = 0;
// 	m_uiChamberPatternNum = 1;
	if(m_uiChamberDelayTime < 3) //yulee 20190715 챔버 대기 시간을 3초 이상으로 한다. 
		m_uiChamberDelayTime = 3;
// 	m_uiChamberDelayTime = 3;
// 	m_fChamberDeltaTemp = 1;
	m_ctrlCheckFixTime.SetTime(DATE(0));
	m_ctrlCheckFixTime.SetFormat("HH:mm:ss");

	m_ctrlCheckTime.SetTime(DATE(0));
	m_ctrlCheckTime.SetFormat("HH:mm:ss");




//1. 채널이 할당된 챔버의 번호를 가져온다. 20180515 yulee 
//2. 그 챔버가 동작 중인지를 확인한다. 
//3. 동작 중이면 챔버 관련 컨트롤러를 비활성화 시킨다. 

	int nModuleID = -1, nChIndex = -1, nOvenID = -1;
	//int nOvenIndex;
	for(int i=0; i< m_paPtrCh->GetSize(); i++)
	{
		CCyclerChannel *pCh = (CCyclerChannel *)m_paPtrCh->GetAt(i);
		nModuleID = pCh->GetModuleID();
		nChIndex = pCh->GetChannelIndex();
	}
	
	CDWordArray adwSelChList, adwOvenList;
	//m_pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndexList(adwSelChList , adwOvenList);

	//COvenCtrl *pCtrlOven;
	COvenCtrl *pCtrlOven = NULL; //ksj 20200729
	BOOL bUseOven1or2 = FALSE; 
	nOvenID = m_pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndex(nModuleID, nChIndex);
	if(nOvenID != -1)
	{
		nOvenID = m_pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndex(nModuleID, nChIndex);
		pCtrlOven = &(m_pDoc->m_ChamberCtrl.m_ctrlOven1);
		bUseOven1or2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE); //yulee 20190702
	}
	else
	{
		nOvenID = m_pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndex(nModuleID, nChIndex);
		if(nOvenID != -1)
		{
			nOvenID = m_pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndex(nModuleID, nChIndex);
			pCtrlOven = &(m_pDoc->m_ChamberCtrl.m_ctrlOven2);
			bUseOven1or2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven", FALSE); //yulee 20190702
		}
	}


	BOOL bOvenRun = FALSE;
	//BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	BOOL bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven", FALSE); //yulee 20180821
	
	GetDlgItem(IDC_CHK_PWR_OFF_WORKEND)->ShowWindow(FALSE); //lyj 20210615 //lyj 20210730 위치이동


	if((bUseOven == TRUE) && (bUseOven1or2 == TRUE))
	{
		if(pCtrlOven) //ksj 20200729 : null pointer 체크 추가.
		{
			if(pCtrlOven->GetRun(nOvenID))	//챔버 동작 중
			{
				CCyclerChannel *pChInfo;
				for(int i = 0 ; i < pCtrlOven->GetOvenCount(); i++)
				{
					CDWordArray adwCh;
					pCtrlOven->GetChannelMapArray(i, adwCh);			
					for(int index =0; index < adwCh.GetSize(); index++)
					{
						DWORD dwData = adwCh.GetAt(index);
					
						CWordArray OneChArray;
						OneChArray.Add(dwData);
					
						pChInfo = m_pDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
						if(pChInfo->GetChannelIndex() != nChIndex)
						{
							if((pChInfo->GetState() == PS_STATE_RUN) || 
								(pChInfo->GetState() == PS_STATE_PAUSE) ||
								(pChInfo->GetState() == PS_STATE_PAUSE_CHAMBER_LINK) ||
								(pChInfo->GetState() == PS_STATE_PAUSE_CHAMBER_READY))
							{
								//TRACE(_T("할당된 해당 챔버가 동작중 "));
								TRACE(Fun_FindMsg("StartDlg_OnInitDialog_msg1","IDD_START_DLG")); //&&
								bOvenRun = TRUE;
								//GetDlgItem(IDC_CHK_CHAMBER_NO_CHAM_RUN)->SetWindowText(" - 다른 채널에서 챔버 동작 중"); //yulee 2018120
								GetDlgItem(IDC_CHK_CHAMBER_NO_CHAM_RUN)->SetWindowText(Fun_FindMsg("StartDlg_OnInitDialog_msg2","IDD_START_DLG")); //yulee 2018120//&&
							}
							else
							{
								GetDlgItem(IDC_CHK_CHAMBER_NO_CHAM_RUN)->SetWindowText(" "); //yulee 2018120
							}
						}	
						
					}
				}
			}
			else
			{
				//TRACE(_T("할당된 해당 챔버가 동작중 아님 "));
				TRACE(Fun_FindMsg("StartDlg_OnInitDialog_msg3","IDD_START_DLG"));//&&
				bOvenRun = FALSE;
				//GetDlgItem(IDC_CHK_CHAMBER_NO)->SetWindowText("챔버 연동 안함");
				GetDlgItem(IDC_CHK_CHAMBER_NO)->SetWindowText(Fun_FindMsg("StartDlg_OnInitDialog_msg4","IDD_START_DLG"));//&&
			}
		}
		else
		{
			TRACE(_T("할당된 해당 챔버가 동작중 아님 "));
			bOvenRun = FALSE;
			GetDlgItem(IDC_CHK_CHAMBER_NO)->SetWindowText("챔버 연동 안함");
		}

	}
	else if((bUseOven == TRUE) && (bUseOven1or2 == FALSE)) //yulee 20190702
	{	
		//StartDlg_OnInitDialog_msg5	=	챔버1 또는 2의 연결이 되어 있지 않습니다. "Serial 통신 설정"의 챔버 설정을 확인해 주세요.
		AfxMessageBox(Fun_FindMsg("StartDlg_OnInitDialog_msg5","IDD_START_DLG"));
		return FALSE;
	}
	
	//bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	bUseOven = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven", FALSE); //yulee 20180821;
	if (bUseOven && (!bOvenRun))
	{
		GetDlgItem(IDC_CHK_CHAMBER_FIX)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_CHAMBER_FIX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_DATETIMEPICKER3)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_CHAMBER_DELTA_FIX_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_CHAMBER_PROG)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_CHAMBER_STEP)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_CHAMBER_PATTERN_NUM)->EnableWindow(TRUE);
		GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_CHAMBER_DELTA_TEMP)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_CHAMBER_PATTERN_DELAY_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_1)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_2)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_3)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_4)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_5)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_6)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_7)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_8)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_9)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_10)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_11)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_START_12)->EnableWindow(TRUE);	
		
		//2014.11.19 챔버 대기 모드 설정 레지스트리
		m_bChamberContinue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseChamberContinue",FALSE);
		
		//2014.11.19 챔버 대기 모드 설정 체크 박스 표시
		if(m_bChamberContinue)
		{
			GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->ShowWindow(TRUE);
//			CheckDlgButton(IDC_CHK_CHAMBER_CONTINUE,TRUE);    
			m_chkChamberContinue = FALSE;//yulee 20190715
			GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->EnableWindow(FALSE);
		}

	}
	else
	{
		GetDlgItem(IDC_CHK_CHAMBER_FIX)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_CHAMBER_FIX_TEMP)->EnableWindow(FALSE);
		GetDlgItem(IDC_DATETIMEPICKER3)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_CHAMBER_DELTA_FIX_TEMP)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_CHAMBER_PROG)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_CHAMBER_STEP)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_CHAMBER_PATTERN_NUM)->EnableWindow(FALSE);
		GetDlgItem(IDC_DATETIMEPICKER2)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_CHAMBER_DELTA_TEMP)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_CHAMBER_PATTERN_DELAY_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_1)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_2)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_3)->EnableWindow(FALSE); //yulee 20190625
		GetDlgItem(IDC_STATIC_START_4)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_5)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_6)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_7)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_8)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_9)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_10)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_11)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_START_12)->EnableWindow(FALSE);

		//2014.11.19 챔버 대기 모드 설정 레지스트리 //yulee 20190715
		m_bChamberContinue = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseChamberContinue",FALSE);

		//2014.11.19 챔버 대기 모드 설정 체크 박스 표시
		if(m_bChamberContinue)
		{
			GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->ShowWindow(TRUE);
			//			CheckDlgButton(IDC_CHK_CHAMBER_CONTINUE,TRUE);    
			m_chkChamberContinue = FALSE;//yulee 20190715
			GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->EnableWindow(FALSE);
		}
	}

	//GetDlgItem(IDC_CHK_PWR_OFF_WORKEND)->ShowWindow(FALSE); //lyj 20210615 //lyj 20210730 위치이동
	/*lyj 20210615  주석처리(파워서플라이 다이얼로그에서 설정하는것과 중복)
	////////////////////////////////////////////////////////////
	//20190615_2 파워서플라이 작업종료시 OFF 기능 여부 확인을 위한 기능 활성화 레지스트리
	BOOL m_bPwrSplyOffShow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UsePwrSplyOffWorkEndActivate",FALSE);

	if(m_bPwrSplyOffShow == TRUE)
	{
		GetDlgItem(IDC_CHK_PWR_OFF_WORKEND)->EnableWindow(TRUE);
		m_chkPwrSplyOffWorkEnd = TRUE;
	}
	else
	{
		m_chkPwrSplyOffWorkEnd = FALSE;
	}
	*/


	////////////////////////////////////////////////////////////

	UpdateData(FALSE);
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CStartDlg::AddStepInCycle(int nCycle)
{
	m_StepCombo.ResetContent();

	if(nCycle == 0)
	{
		m_StepCombo.AddString(Fun_FindMsg("AddStepInCycle_msg1","IDD_START_DLG"));
		//@ m_StepCombo.AddString("처음부터...");
		m_StepCombo.SetCurSel(0);
		m_StepCombo.SetItemData(0, 0);
		GetDlgItem(IDC_MAX_CYCLE_STATIC)->SetWindowText("...");
		GetDlgItem(IDC_CYCLE_EDIT)->EnableWindow(FALSE);
		m_nStartCycle = 0;
	}
	else
	{
		GetDlgItem(IDC_CYCLE_EDIT)->EnableWindow(TRUE);
		m_nStartCycle = 1;

		int nItemCount = 0;

		CString strTemp;
		CStep *pStep;
		CUnitTrans unit;
		int nCount = 0;
		for(int i =0; i<m_pSchedule->GetStepSize(); i++)
		{
			pStep = m_pSchedule->GetStepData(i);
			if(pStep)
			{
				if(pStep->m_type == PS_STEP_ADV_CYCLE)
				{
					nCount++;
				}

				if(nCount == nCycle &&  pStep->m_type != PS_STEP_END)
				{
					if(pStep->m_type == PS_STEP_ADV_CYCLE)
					{
						m_StepCombo.AddString(Fun_FindMsg("AddStepInCycle_msg2","IDD_START_DLG"));
						//@ m_StepCombo.AddString("처음부터...");
						m_StepCombo.SetItemData(nItemCount++, pStep->m_StepIndex);	//처음부터에 이전 step 할당 

					}
					else if(pStep->m_type == PS_STEP_LOOP)
					{
						strTemp.Format(Fun_FindMsg("AddStepInCycle_msg3","IDD_START_DLG"), pStep->m_nLoopInfoCycle);
						//@ strTemp.Format("(최대 %d회)", pStep->m_nLoopInfoCycle);
						GetDlgItem(IDC_MAX_CYCLE_STATIC)->SetWindowText(strTemp);
					}
					else
					{
						strTemp.Format("Step %d :: %s (I=%s)", 
										pStep->m_StepIndex+1, 
										::PSGetTypeMsg(pStep->m_type),
										unit.ValueString(pStep->m_fIref, PS_CURRENT, TRUE)
										);
						m_StepCombo.AddString(strTemp);
						m_StepCombo.SetItemData(nItemCount++, pStep->m_StepIndex);
					}
				}
			}
		}

	}
	
	m_StepCombo.SetCurSel(0);
	UpdateData(FALSE);
}


void CStartDlg::OnEditupdateTestNameCombo() 
{
	// TODO: Add your control notification handler code here
	m_ctrlTestNameCombo.GetWindowText(m_strTestName);
	UpdateState();
	
}

void CStartDlg::OnSelchangeTestNameCombo() 
{
	// TODO: Add your control notification handler code here
	m_ctrlTestNameCombo.GetWindowText(m_strTestName);
	UpdateState();
}

void CStartDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData();
	
	m_ctrlTestNameCombo.GetWindowText(m_strTestName);
	CString strTemp,strMsg;

	if(m_strTestName.IsEmpty())
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_START_DLG"));
		//@ AfxMessageBox("작업명이 입력되어 있지 않습니다.");
		GetDlgItem(IDC_TEST_NAME_COMBO)->SetFocus();
		return;
	}

	m_strTestName.TrimLeft(" ");
	m_strTestName.TrimRight(" ");
	
	//, :*/＼?"<>. -> 사용 못하는 첨자
	if (m_strTestName.Find(',') >=0 || m_strTestName.Find(':') >=0 || m_strTestName.Find('*') >=0 || m_strTestName.Find('/') >=0 || m_strTestName.Find('.') >=0 || 
	    m_strTestName.Find('\\') >=0 || m_strTestName.Find('"') >=0 || m_strTestName.Find('"') >=0 || m_strTestName.Find('<') >=0 ||  m_strTestName.Find('>') >=0) // hhs 20210714 '.' 추가
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_START_DLG"));
		//@ AfxMessageBox("작업명은 , :*/＼""?<>. 문자를 포함할 수 없습니다.");
		GetDlgItem(IDC_TEST_NAME_COMBO)->SetFocus();
		return;
	}

	//CSV 파일에 저장하므로 ,(comma)를 입력하지 못하도록 함
	if(m_strTestName.Find(',') >=0 )
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg3","IDD_START_DLG"));
		//@ AfxMessageBox("작업명은 \",\" 문자를 포함할 수 없습니다.");
		GetDlgItem(IDC_TEST_NAME_COMBO)->SetFocus();
		return;
	}
	if(m_strWorker.Find(',') >=0 )
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg4","IDD_START_DLG"));
		//@ AfxMessageBox("작업자명은 \",\" 문자를 포함할 수 없습니다.");
		GetDlgItem(IDC_WORKER_NAME_EDIT)->SetFocus();
		return;
	}
/*	if(m_strLot.Find(',') >=0 )
	{
		AfxMessageBox("작업번호는 \",\" 문자를 포함할 수 없습니다.");
		GetDlgItem(IDC_LOT_NO_EDIT)->SetFocus();
		return;
	}*/

	//lyj 20211026  s 작업시작 시 파일길이 확인==========
	strTemp.Format("%s\\%s\\%s\\%s.%s", m_strDataPath,"M00Ch00[000]",m_strTestName,m_strTestName,PS_RESULT_FILE_NAME_EXT);
	if (strTemp.GetLength()  > MAX_PATH)
	{
		strMsg.Format("%s \n Path is Too Long, Please Check PathLength \n(*TotalPathLength <= %d)",strTemp,MAX_PATH);
		AfxMessageBox(strMsg);
		return;
	}
	//lyj 20211026  e ==========

	BOOL bShowTray = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "ShowTrayInfo", FALSE);
	BOOL bShowCellNo = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "ShowCellInfo", FALSE);
	
	CString strTrayNo, strSerial;
	CCyclerChannel *pChannel;
	for(int row=1; row < m_Grid.GetRowCount(); row++)
	{
		strTrayNo = m_Grid.GetItemText(row, 1);
		strTemp = m_Grid.GetItemText(row, 0);
		if(strTrayNo.Find(',') >=0 )
		{
			AfxMessageBox(strTemp + Fun_FindMsg("OnOK_msg5","IDD_START_DLG"));
			//@ AfxMessageBox(strTemp + " Tray 번호는 \",\" 문자를 포함할 수 없습니다.");
			m_Grid.SetFocusCell(row, 1);
			return;
		}
		
		if(strTrayNo.IsEmpty() && bShowTray)
		{
			AfxMessageBox(strTemp + Fun_FindMsg("OnOK_msg6","IDD_START_DLG"));
			//@ AfxMessageBox(strTemp + " Tray 번호를 입력하십시요.");
			m_Grid.SetFocusCell(row, 1);
			return;
		}

		strSerial = m_Grid.GetItemText(row, 2);
		if(strSerial.Find(',') >=0 )
		{
			AfxMessageBox(strTemp + Fun_FindMsg("OnOK_msg7","IDD_START_DLG"));
			//@ AfxMessageBox(strTemp + " CellSerial은 \",\" 문자를 포함할 수 없습니다.");
			m_Grid.SetFocusCell(row, 1);
			return;
		}
		if(strSerial.IsEmpty() && bShowCellNo)
		{
			AfxMessageBox(strTemp + Fun_FindMsg("OnOK_msg8","IDD_START_DLG"));
			//@ AfxMessageBox(strTemp + " Cell Serial 번호를 입력하십시요.");
			m_Grid.SetFocusCell(row, 2);
			return;
		}
		
		if(row <= m_paPtrCh->GetSize())
		{
			pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(row-1);
			if(pChannel)
			{
				pChannel->ResetData();
				pChannel->SetTestSerial(strSerial, m_strWorker, m_strComment);

				//Oven하고 연동하여 사용 할지 여부
				if (m_nStartOptChamber == 3) 
					pChannel->SetUseOvenMode(TRUE);
				else
					pChannel->SetUseOvenMode(FALSE);

			}
		}
	}
	
	if(m_strComment.Find(',') >=0 )
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg9","IDD_START_DLG"));
		//@ AfxMessageBox("작업설명은 \",\" 문자를 포함할 수 없습니다.");
		GetDlgItem(IDC_COMMENT_EDIT)->SetFocus();
		return;
	}
	
	int nStepSel = m_StepCombo.GetCurSel();
	int nCycleSel = m_CycleCombo.GetCurSel();
	if(nCycleSel < 1 && nStepSel < 1)
	{
		m_nStartCycle = 0;
		m_nStartStep = 0;
	}
	else
	{
		m_nStartStep = m_StepCombo.GetItemData(nStepSel)+1;				
	}

	if(m_nStartCycle > 0 && m_nStartStep < 1)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg10","IDD_START_DLG"));
		//@ AfxMessageBox("반복횟수가 지정되었으나 시작 Step번호가 지정되지 않았습니다.");
		GetDlgItem(IDC_STEP_EDIT)->SetFocus();	
		return;
	}

	if(m_nStartCycle < 1 && m_nStartStep > 0)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg11","IDD_START_DLG"));
		//@ AfxMessageBox("시작 시작 Step번호가 지정되었으나 반복횟수가 지정되지 않았습니다.");
		GetDlgItem(IDC_CYCLE_EDIT)->SetFocus();	
		return;
	}

	if(m_nStartStep >= m_pSchedule->GetStepSize())
	{
		AfxMessageBox(Fun_FindMsg("OnOk_msg12","IDD_START_DLG"));
		//@ AfxMessageBox("시작 Step 번호가 지정 스케쥴에 없거나 지정할 수 있는 Step입니다.\n선택 스케쥴 자세히 보기에서 확인 하십시요.");
		GetDlgItem(IDC_STEP_EDIT)->SetFocus();	
		return;
	}

	if(m_nStartStep > 0)
	{
		int nCycleNo = m_pSchedule->GetCurrentStepCycle(m_nStartStep-1);
		if(nCycleNo < 1)	//Error
		{
			//편집 Error
			AfxMessageBox(Fun_FindMsg("OnOK_msg13","IDD_START_DLG"));
			//@ AfxMessageBox("조건 편집 이상");
		}
		if(m_nStartCycle > nCycleNo)
		{
			strTemp.Format(Fun_FindMsg("OnOK_msg14","IDD_START_DLG"),
				//@ strTemp.Format("Step %d는 %d회 반복하도록 설정되어 있습니다. %d 이하값을 입력하십시요.\n선택 스케쥴 자세히 보기에서 확인 하십시요.",
							m_nStartStep, nCycleNo, nCycleNo);
			AfxMessageBox(strTemp);
			GetDlgItem(IDC_CYCLE_EDIT)->SetFocus();	
			return;
		}

	}

	//파일 존재 여부 및 이름 검사 
	int nRtn =  CheckFileName();
	if(0 == nRtn)
	{
		GetDlgItem(IDC_TEST_NAME_COMBO)->SetFocus();
		return;
	}
	else if(nRtn == 1)		//새롭게 입력되었으면 List에 추가(중복된 이름을 OverWirte 했을 경우는 최근 목록에 추가 안함)
	{
		//최근 시험명 리스트 저장 
		char szEntry[64];
		CString strRecentName;

		//지금 입력한 이름 저장
		AfxGetApp()->WriteProfileString(RECENT_NAME_REG_SECTION, "Name0", m_strTestName);

		//나머지 9개 저장 
		POSITION pos = m_strRecentNameList.GetHeadPosition();
		int nCount = 1;
		while(pos != NULL && nCount < MAX_RECENT_TEST_NAME)
		{
			sprintf(szEntry, "Name%d", nCount++);
			strRecentName = m_strRecentNameList.GetNext(pos);
			AfxGetApp()->WriteProfileString(RECENT_NAME_REG_SECTION, szEntry, strRecentName);
		}
		
	}
		
	GetDlgItem(IDC_LOT_NO_EDIT)->GetWindowText(strTemp);
	AfxGetApp()->WriteProfileString(RECENT_NAME_REG_SECTION, "LotNo", strTemp);
	GetDlgItem(IDC_WORKER_NAME_EDIT)->GetWindowText(strTemp);
	AfxGetApp()->WriteProfileString(RECENT_NAME_REG_SECTION, "Worker", strTemp);
	GetDlgItem(IDC_COMMENT_EDIT)->GetWindowText(strTemp);
	AfxGetApp()->WriteProfileString(RECENT_NAME_REG_SECTION, "Descript", strTemp);
	
	//ljb 2011 시작 챔버 정보
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Start Chamber Option", m_nStartOptChamber);
	strTemp.Format("%.1f",m_fChamberFixTemp);
	AfxGetApp()->WriteProfileString(OVEN_SETTING, "Fix Temp",strTemp);
	strTemp.Format("%.1f",m_fChamberDeltaFixTemp);
	AfxGetApp()->WriteProfileString(OVEN_SETTING, "Delta Fix Temp",strTemp);
	strTemp.Format("%.1f",m_fChamberDeltaTemp);
	AfxGetApp()->WriteProfileString(OVEN_SETTING, "Delta Temp",strTemp);

	
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Pattern No", m_uiChamberPatternNum);
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Start Delay Time", m_uiChamberDelayTime);
	//////////////////////////////////////////////////////////////////////////

	//Oven 사용 조건
	//=> 한개의 Oven은 Oven연동으로 check한 Channel들은 모두 같은 시험조건을 수행야하 함
	//=> 즉 1개의 Oven에서 있는 Channel group은 1개의 시험조건만 시행가능함(단 Oven 연동 Flag를 Set 하지 않은 Channel은 다른 시험조건을 시행할 수 있음)

	if((m_nStartOptChamber != 4) && (m_nStartOptChamber != 5)) //yulee 20180820 
	{
		//Warning message for Samsung QC Cycler
		CWorkWarnin WorkWarningDlg;
		strTemp = Fun_FindMsg("OnOK_msg15","IDD_START_DLG");
		//@ strTemp = "챔버 연동을 선택 하셨습니다. 진행 하시겠습니까?";
		WorkWarningDlg.Fun_SetMessage(strTemp);
		if (WorkWarningDlg.DoModal() == IDCANCEL) return;
	}

	int nModuleID,nChIndex;
	if(m_nStartOptChamber == 3) //테스트
	{
		//현재 Channel이 속한 Oven에 Oven과 연동중인 Channel이 존재 하는가? => Oven을 사용 못함 
		for(int ch = 0; ch<m_paPtrCh->GetSize(); ch++)
		{
			//선택 Channel을 1개씩 Scan 함 
			pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(ch);
			nModuleID = pChannel->GetModuleID();
			nChIndex = pChannel->GetChannelIndex();			
									
			if((pChannel->GetState() == PS_STATE_RUN || pChannel->GetState() == PS_STATE_PAUSE)&& pChannel->GetUseOvenMode() == TRUE)
			{
				//strTemp.Format("Oven %d는 %s의 CH %d와 연동 운영중입니다. Oven %d에 설치된 나머지 Channel은 Oven 연동으로 사용할 수 없습니다.", 
				strTemp.Format(Fun_FindMsg("OnOK_msg16","IDD_START_DLG"), 
				//@ strTemp.Format("%s의 CH %d는 챔버 연동 운영중입니다. ", 
					//nOvenIndex+1, m_pDoc->GetModuleName(HIWORD(dwData)), LOWORD(dwData)+1, pChannel->m_nChannelIndex+1);
					m_pDoc->GetModuleName(nModuleID), nChIndex+1);
				AfxMessageBox(strTemp, MB_ICONSTOP|MB_OK);
				return;
			}
			
		}
	}

	//ljb 20180226 챔버 연동 조건 레지스트리에 기록
	COleDateTime checkTime;
	m_ctrlCheckFixTime.GetTime(checkTime);
	m_uiChamberCheckFixTime = (checkTime.GetHour()*3600+checkTime.GetMinute()*60+checkTime.GetSecond())*100;		
	
	m_ctrlCheckTime.GetTime(checkTime);
	m_uiChamberCheckTime = (checkTime.GetHour()*3600+checkTime.GetMinute()*60+checkTime.GetSecond())*100;		
	
	if (m_pDoc->m_nChamberOperationType == 0)	//ljb 20180226 edit 챔버 시작 정보 저장
	{
		if (m_nStartOptChamber == 3)	//ljb 20180430 add 챔버 연동 일때만 기록
		{
			//1개 챔버 사용
			//ljb 2011 시작 챔버 정보
			AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Start Chamber Option", m_nStartOptChamber);
			strTemp.Format("%.1f",m_fChamberFixTemp);
			AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG3, "Fix Temp",strTemp);
			strTemp.Format("%.1f",m_fChamberDeltaFixTemp);
			AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG3, "Delta Fix Temp",strTemp);
			strTemp.Format("%.1f",m_fChamberDeltaTemp);
			AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG3, "Delta Temp",strTemp);
			
			AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Pattern No", m_uiChamberPatternNum);
			AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Start Delay Time", m_uiChamberDelayTime);
			
			AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Fix Check Time", m_uiChamberCheckFixTime);
			AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Schedule Check Time", m_uiChamberCheckTime);
			
			AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Chamber Continue Type", m_chkChamberContinue);	//ljb 20180226 add
			//////////////////////////////////////////////////////////////////////////
			
		}
	}
	else
	{
		//2개 이상 챔버 사용 (작업 시작 채널에 따라서 챔버 옵션 입력 한다.)
		
		//현재 Channel이 속한 Oven에 Oven과 연동중인 Channel이 존재 하는가? => Oven을 사용 못함 
		for(int ch = 0; ch<m_paPtrCh->GetSize(); ch++)
		{
			//선택 Channel을 1개씩 Scan 함 
			pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(ch);
			nModuleID = pChannel->GetModuleID();
			nChIndex = pChannel->GetChannelIndex();			
			
			if(m_nStartOptChamber == 3)	//ljb 20180430 add 챔버 연동 일때만 기록
			{
				if (nChIndex == 0) //1번 채널
				{
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Start Chamber Option", m_nStartOptChamber);
					strTemp.Format("%.1f",m_fChamberFixTemp);
					AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG3, "Fix Temp",strTemp);
					strTemp.Format("%.1f",m_fChamberDeltaFixTemp);
					AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG3, "Delta Fix Temp",strTemp);
					strTemp.Format("%.1f",m_fChamberDeltaTemp);
					AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG3, "Delta Temp",strTemp);
					
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Pattern No", m_uiChamberPatternNum);
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Start Delay Time", m_uiChamberDelayTime);
					
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Fix Check Time", m_uiChamberCheckFixTime);
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Schedule Check Time", m_uiChamberCheckTime);
					
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Chamber Continue Type", m_chkChamberContinue);	//ljb 20180226 add
				}
				else
				{
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Start Chamber Option", m_nStartOptChamber);
					strTemp.Format("%.1f",m_fChamberFixTemp);
					AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG4, "Fix Temp",strTemp);
					strTemp.Format("%.1f",m_fChamberDeltaFixTemp);
					AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG4, "Delta Fix Temp",strTemp);
					strTemp.Format("%.1f",m_fChamberDeltaTemp);
					AfxGetApp()->WriteProfileString(REG_SERIAL_CONFIG4, "Delta Temp",strTemp);
					
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Pattern No", m_uiChamberPatternNum);
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Start Delay Time", m_uiChamberDelayTime);
					
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Fix Check Time", m_uiChamberCheckFixTime);
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Schedule Check Time", m_uiChamberCheckTime);
					
					AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Chamber Continue Type", m_chkChamberContinue);	//ljb 20180226 add
				}
			}
		}
	}
	


	//ljb 20151111 Mux 연동 여부
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Mux Link Option", m_bIsMuxLink);

	m_ctrlCheckFixTime.GetTime(checkTime);
	m_uiChamberCheckFixTime = (checkTime.GetHour()*3600+checkTime.GetMinute()*60+checkTime.GetSecond())*100;		

	m_ctrlCheckTime.GetTime(checkTime);
	m_uiChamberCheckTime = (checkTime.GetHour()*3600+checkTime.GetMinute()*60+checkTime.GetSecond())*100;		
	

	m_nReturn = IDOK;

	//////////////////////////////////////////////////////////////////////////
	//ljb 2011 
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Fix Check Time", m_uiChamberCheckFixTime);
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "Schedule Check Time", m_uiChamberCheckTime);
	//////////////////////////////////////////////////////////////////////////
	//if (AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Load 2", FALSE))
	CString strUseLoaderLink;
	UpdateData();
	for(int ch = 0; ch < m_paPtrCh->GetSize(); ch++) //lmh 20111213 챔버 연동
	{	
		pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(ch);		
		pChannel->m_nStartOptChamber = m_nStartOptChamber;
		pChannel->m_fchamberFixTemp = m_fChamberFixTemp;
		pChannel->m_fChamberDeltaFixTemp = m_fChamberDeltaFixTemp;
		pChannel->m_fchamberDeltaTemp = m_fChamberDeltaTemp;
		pChannel->m_uichamberPatternNum = m_uiChamberPatternNum;
		pChannel->m_uiChamberDelayTime = m_uiChamberDelayTime;
		pChannel->m_uiChamberCheckTime = m_uiChamberCheckTime;
		pChannel->m_uiChamberCheckFixTime = m_uiChamberCheckFixTime;

		strUseLoaderLink.Format("USE LINK LOADER %d", pChannel->GetChannelIndex() + 1);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, strUseLoaderLink, m_chkLoadLink);

		//2014.11.21 챔버 연동 대기 모드는 기본 1(1 대기, 0 계속)
		CString RegName;
		RegName.Format("OvenContinue_%d",pChannel->GetChannelIndex() + 1);		

		BOOL bContinueValue;  //yulee 20190715
		bContinueValue =  IsDlgButtonChecked(IDC_CHK_CHAMBER_CONTINUE);	


		AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, bContinueValue);
		
		//2014.11.19 챔버 대기 동작 설정 여부 레지
		if(m_bChamberContinue){						
			//m_IsChamberContinue = IsDlgButtonChecked(IDC_CHK_CHAMBER_CONTINUE);	
			//m_IsChamberContinue = m_chkChamberContinue;

			
			//AfxGetApp()->WriteProfileInt(OVEN_SETTING, RegName, m_IsChamberContinue);	
			

			//챔버 대기 모스 상태값 저장.
			if(bContinueValue){
				m_pDoc->m_bChamberStand = 1;
			}else{
				m_pDoc->m_bChamberStand = 0;
			}

// 			//챔버 대기 모스 상태값 저장.
// 			if(m_IsChamberContinue){
// 				m_pDoc->m_bChamberStand = 1;
// 			}else{
// 				m_pDoc->m_bChamberStand = 0;
// 			}
		}		

		//2014.12.03 캔 통신 모드 체크 모드
		RegName.Format("UseCanModeCheck%d%d",pChannel->GetModuleID(),pChannel->GetChannelIndex() + 1);
		//AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, RegName, FALSE);
		//m_bIsCanModeCheck = IsDlgButtonChecked(IDC_CHK_CAN_MODE);			

		int nCanCheckMode = m_ctrlCboCanCheck.GetItemData(m_ctrlCboCanCheck.GetCurSel());
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, RegName, nCanCheckMode);	
	}

	/*lyj 20210615  주석처리(파워서플라이 다이얼로그에서 설정하는것과 중복) =================
	//yulee 20190615_2 Power supply off 기능, 작업 완료 시 - 채널별 저장
	for(int ch = 0; ch<m_paPtrCh->GetSize(); ch++)
	{
		//선택 Channel을 1개씩 Scan 함 
		pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(ch);
		nModuleID = pChannel->GetModuleID();
		nChIndex = pChannel->GetChannelIndex();			

		CString strTempRegName;
		strTempRegName.Format(_T("")); 

		if(nChIndex == 0) //1번 채널
		{
			strTempRegName.Format("PwrSplyCh%dOffWhenWorkEnd", nChIndex+1);
			AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER1, strTempRegName , m_chkPwrSplyOffWorkEnd);	//yulee 20190615_2
		}
		else if(nChIndex == 1)
		{
			strTempRegName.Format("PwrSplyCh%dOffWhenWorkEnd", nChIndex+1);
			AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER1, strTempRegName , m_chkPwrSplyOffWorkEnd);	//yulee 20190615_2
		}
		else if(nChIndex == 2)
		{
			strTempRegName.Format("PwrSplyCh%dOffWhenWorkEnd", nChIndex-1);
			AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER2, strTempRegName , m_chkPwrSplyOffWorkEnd);	//yulee 20190615_2
		}
		else if(nChIndex == 3)
		{
			strTempRegName.Format("PwrSplyCh%dOffWhenWorkEnd", nChIndex-1);
			AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER2, strTempRegName , m_chkPwrSplyOffWorkEnd);	//yulee 20190615_2
		}//m_chkPwrSplyOffWorkEnd
	}
	*/

	CDialog::OnOK();
}

//OK Button 상태 Update
void CStartDlg::UpdateState()
{
	if(m_strTestName.IsEmpty())
	{
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDOK)->EnableWindow();
	}
}

//return 0: 생성 실패
//return 1: 새로 생성 
//return -1:중복을 overwrite
//현재 사용중인 시험명인지 검사 
int CStartDlg::CheckFileName()
{

	CString fullName, strTemp;
	fullName = m_strDataPath +"\\"+m_strTestName;

	//현재 다른 모듈에서 사용중인 이름인지 확인
	CCyclerModule *pMD; 
	CCyclerChannel *pCh;

	//전체 모듈에 대해 검색
	for(int mdIndex =0; mdIndex < m_pDoc->GetInstallModuleCount(); mdIndex++)
	{
		pMD = m_pDoc->GetModuleInfo(m_pDoc->GetModuleID(mdIndex));
		if(pMD)
		{
			//Line Off이면 검색 안함 
			if(pMD->GetState() == PS_STATE_LINE_OFF)	continue;

			//모든 채널에 대해 검색
			for(int ch =0; ch < pMD->GetTotalChannel(); ch++)
			{
				pCh = pMD->GetChannelInfo(ch);
				if(pCh)
				{
					//Idle인 채널은 검색 안함 
					if( pCh->GetState() == PS_STATE_IDLE || 
						pCh->GetState() == PS_STATE_STANDBY ||
						pCh->GetState() == PS_STATE_END	||
						pCh->GetState() == PS_STATE_READY
					)	
					{
						continue;
					}
					if(fullName == pCh->GetTestPath())
					{
						strTemp.Format(Fun_FindMsg("CheckFileName_msg1","IDD_START_DLG"), m_strTestName, m_pDoc->GetModuleName(m_pDoc->GetModuleID(mdIndex)), ch+1);
						//@ strTemp.Format("%s는 %s Ch %d에서 사용중인 이름입니다. 다른 이름을 입력해 주십시요", m_strTestName, m_pDoc->GetModuleName(m_pDoc->GetModuleID(mdIndex)), ch+1);
						MessageBox(strTemp, Fun_FindMsg("CheckFileName_msg2","IDD_START_DLG"), MB_OK|MB_ICONSTOP);
						//@ MessageBox(strTemp, "이름 사용중", MB_OK|MB_ICONSTOP);
						return 0;
					}
				}
			}
		}
	}


	//폴더 생성 실패
	if(FALSE == ::CreateDirectory(fullName, NULL))
	{
		DWORD dwCode = ::GetLastError();
		if(dwCode == ERROR_ALREADY_EXISTS)	//이미 존재하는 폴더
		{
			if(AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "NoOverWrite", FALSE))
			{	
				//over write 불가 
				MessageBox(m_strTestName+Fun_FindMsg("CheckFileName_msg3","IDD_START_DLG"), Fun_FindMsg("CheckFileName_msg4","IDD_START_DLG"), MB_OK|MB_ICONSTOP);
				//@ MessageBox(m_strTestName+"은 이미 존재합니다. 다른 이름을 입력하거나 이전 결과를 삭제 후 시작하십시요.", "이름 중복", MB_OK|MB_ICONSTOP);
				return 0;
			}
			else	//Over write 가능
			{
				//다른 완료된 시험과 중복 여부 검사.//$
				if(IDYES != MessageBox(m_strTestName+Fun_FindMsg("CheckFileName_msg5","IDD_START_DLG"), Fun_FindMsg("CheckFileName_msg6","IDD_START_DLG"), MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2))
				//@ if(IDYES != MessageBox(m_strTestName+"은 이미 존재합니다. 덮어 쓰시겠습니까? (포함된 모든 파일이 삭제됩니다.)", "이름 중복", MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2))
				{
					return 0;
				}
				//기존 폴더 삭제
				//삭제되면 손실된 Data가 있는 채널도 복구할 수 없게 됨
				//data량에 따라 지연됨 
				
				CProgressWnd *pProgressWnd = new CProgressWnd;
				pProgressWnd->Create(AfxGetMainWnd(), Fun_FindMsg("CheckFileName_msg7","IDD_START_DLG"), TRUE);
				//@ pProgressWnd->Create(AfxGetMainWnd(), "삭제중...", TRUE);
				pProgressWnd->Show();
				if(DeletePath(fullName, pProgressWnd) == FALSE)
				{
					MessageBox(m_strTestName+Fun_FindMsg("CheckFileName_msg8","IDD_START_DLG"), Fun_FindMsg("CheckFileName_msg9","IDD_START_DLG"), MB_OK|MB_ICONSTOP);				
					//@ MessageBox(m_strTestName+"를 삭제 할 수 없습니다.(폴더가 열려 있거나 파일이 사용중 입니다.)", "생성실패", MB_OK|MB_ICONSTOP);				
					return 0;
				}
				pProgressWnd->Hide();
				delete pProgressWnd;
				
				//재 생성
				if(FALSE == ::CreateDirectory(fullName, NULL))
				{
					MessageBox(m_strTestName+Fun_FindMsg("CheckFileName_msg10","IDD_START_DLG"), Fun_FindMsg("CheckFileName_msg11","IDD_START_DLG"), MB_OK|MB_ICONSTOP);
					//@ MessageBox(m_strTestName+"을 생성할 수 없습니다.", "생성실패", MB_OK|MB_ICONSTOP);
					return 1;
				}
				return -1;
			}
		}
		else if(dwCode == ERROR_PATH_NOT_FOUND)		//존재 하지 않는 폴더
		{
			//cf. Create all intermediate directories on the path
//			int SHCreateDirectoryEx	(	HWND hwnd,
//										LPCTSTR pszPath,
//										SECURITY_ATTRIBUTES *psa
//									);
			//Direcotry 및 파일명 검사.
			MessageBox(m_strDataPath+Fun_FindMsg("CheckFileName_msg12","IDD_START_DLG"),  Fun_FindMsg("CheckFileName_msg13","IDD_START_DLG"), MB_OK|MB_ICONSTOP);
			//@ MessageBox(m_strDataPath+"가 존재하지 않습니다. 먼저 저장위치를 생성하십시요.",  "생성실패", MB_OK|MB_ICONSTOP);
		}
		else
		{
			MessageBox(m_strTestName+Fun_FindMsg("CheckFileName_msg14","IDD_START_DLG"), Fun_FindMsg("CheckFileName_msg15","IDD_START_DLG"), MB_OK|MB_ICONSTOP);
			//@ MessageBox(m_strTestName+"은 잘못된 시험명입니다.(파일이름으로 사용가능한 이름을 입력하십시요.)", "생성실패", MB_OK|MB_ICONSTOP);
		}
		return 0;
	}
	return 1;
}

CString CStartDlg::GetFileName()
{
	return m_strTestName;
}

//파일이 많거나 크기가 크면 삭제 시간이 오래 걸림 

BOOL CStartDlg::DeletePath(CString strPath, CProgressWnd *pPrgressWnd )
{
	CWaitCursor aWait;
    
	if(strPath.IsEmpty())	return TRUE;
    
	CFileFind finder;
    BOOL bContinue = TRUE;

	CString strTemp(strPath);
    if(strTemp.Right(1) != _T("\\"))
        strTemp += _T("\\");

    strTemp += _T("*.*");
    bContinue = finder.FindFile(strTemp);

    while(bContinue)
    {
        bContinue = finder.FindNextFile();
        if(finder.IsDots()) // Ignore this item.
        {
            continue;
        }
        else if(finder.IsDirectory()) // Delete all sub item.
        {
            if(DeletePath(finder.GetFilePath()) == FALSE)	return FALSE;
        }
        else // Delete file.
        {
			strTemp = finder.GetFilePath();
			if(pPrgressWnd)
			{
				pPrgressWnd->SetText(strTemp+Fun_FindMsg("DeletePath_msg","IDD_START_DLG"));
				//@ pPrgressWnd->SetText(strTemp+" 삭제중...");
			}

            if(::DeleteFile((LPCTSTR)strTemp) == 0)
			{
				return FALSE;
			}
        }
		
		if(pPrgressWnd)
		{
			if(pPrgressWnd->PeekAndPump()== FALSE)	return FALSE;
		}

    }
    finder.Close();

    if(::RemoveDirectory((LPCTSTR)strPath) == 0)
		return FALSE;
	
	return TRUE;
}

CString CStartDlg::GetTestPath()
{
	return 	(m_strDataPath +"\\"+m_strTestName);
}

void CStartDlg::OnPrevButton() 
{
	// TODO: Add your control notification handler code here
	
	m_nReturn = IDRETRY;
	CDialog::OnCancel();
}

void CStartDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	m_nReturn = IDCANCEL;
	CDialog::OnCancel();
}

void CStartDlg::SetInfoData(CScheduleData *pSchedule, CPtrArray *aPtrCh)
{
	ASSERT(pSchedule);
	ASSERT(aPtrCh);

	m_pSchedule = pSchedule;
	m_paPtrCh = aPtrCh;
}

void CStartDlg::OnSchDetailButton() 
{
	// TODO: Add your control notification handler code here
	if(m_pSchedule)
		m_pSchedule->ShowContent();
}

void CStartDlg::OnDataFolderButton() 
{
	// TODO: Add your control notification handler code here
	CFolderDialog dlg(m_strDataPath);
	if( dlg.DoModal() == IDOK)
	{
		m_strDataPath = dlg.GetPathName();
		//Data Path를 저장
		AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "Data Path", m_strDataPath);
		UpdateData(FALSE);
	}	
}


void CStartDlg::OnSelchangeCycleCombo() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_CycleCombo.GetCurSel();
	if(nSel != CB_ERR)
	{
		AddStepInCycle(nSel);
	}
}

void CStartDlg::InitGrid()
{
	CString str;

	m_Grid.EnableDragAndDrop(FALSE);
 	m_Grid.SetEditable(TRUE);
	m_Grid.SetVirtualMode(FALSE);
	
	//Cell의 색상 
	m_Grid.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
   
	//여러 Row와 Column을 동시에 선택 가능하도록 한다.
	m_Grid.SetSingleRowSelection(FALSE);
	m_Grid.SetSingleColSelection(FALSE);
	m_Grid.SetHeaderSort(FALSE);	//Sort기능 사용안함 
	
	//크기 조정을 사용자가 못하도록 한다.
	m_Grid.SetColumnResize(FALSE);
	m_Grid.SetRowResize(FALSE);
	
	//Header 부분을 각각 1칸씩 사용한다.
	m_Grid.SetFixedRowCount(1); 
    m_Grid.SetFixedColumnCount(1); 

	m_Grid.SetColumnCount(4);
	m_Grid.SetRowCount(1);

	m_Grid.SetItemText(0, 0, "CH");
	m_Grid.SetItemFormat(0, 0, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	m_Grid.SetItemText(0, 1, AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "TrayName", "Tray"));
	m_Grid.SetItemFormat(0, 1, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	m_Grid.SetItemText(0, 2, "Cell Serial");
	m_Grid.SetItemFormat(0, 2, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	m_Grid.SetItemText(0, 3, Fun_FindMsg("InitGrid_msg1","IDD_START_DLG"));
	//@ m_Grid.SetItemText(0, 3, "온도연동");
	m_Grid.SetItemFormat(0, 3, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
	

	m_Grid.SetColumnWidth(0, 100);

// 	if(m_bUseOven)
// 	{
// 		m_Grid.SetColumnWidth(1, 150);
// 		m_Grid.SetColumnWidth(2, 180);
// 		m_Grid.SetColumnWidth(3, 100);
// 	}
// 	else
// 	{
		m_Grid.SetColumnWidth(1, 200);
		m_Grid.SetColumnWidth(2, 230);
		m_Grid.SetColumnWidth(3, 0);
//	}

	CCyclerChannel *pChannel;
	int nRow = 0;
	for(int c=0; c<m_paPtrCh->GetSize(); c++)
	{
		 pChannel = (CCyclerChannel *)m_paPtrCh->GetAt(c);
		 if(pChannel)
		 {
				str.Format("M%02d CH%03d", pChannel->GetModuleID(), pChannel->GetChannelIndex()+1);
				m_Grid.InsertRow(str);
				nRow++;

				m_Grid.SetItemText(nRow, 0, str);
				m_Grid.SetItemFormat(nRow, 0, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
						
//				m_Grid.SetItemText(nRow, 1, m_strTrayNo);
//				m_Grid.SetItemFormat(nRow, 1, DT_CENTER|DT_VCENTER|DT_SINGLELINE );

				str = pChannel->GetTestSerial();
				
/*				str.Format("%s%d", m_strLot, c+1);
				
				//마지막이 숫자이면 숫자를 증가시켜서 표시 
				int nIndex = -1;
				for(int n = m_strLot.GetLength()-1; n>=0; n--)
				{
					if(isdigit(m_strLot[n]))
					{
						nIndex = n;
					}
					else
					{
						break;
					}
				}
				if(nIndex >= 0)
				{
					str.Format("%s%d", m_strLot.Left(nIndex), atol(m_strLot.Mid(nIndex))+c+1);
				}
*/
				m_Grid.SetItemText(nRow, 2, str);
				m_Grid.SetItemFormat(nRow, 2, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
				
				if(pChannel->GetUseOvenMode())
				{
					str = Fun_FindMsg("InitGrid_msg2","IDD_START_DLG");
					//@ str = "온도연동";
				}
				else
				{
					str.Empty();
				}
				m_Grid.SetItemText(nRow, 3, str);
				m_Grid.SetItemFormat(nRow, 3, DT_CENTER|DT_VCENTER|DT_SINGLELINE );

		 }
	}

	int nCount = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Count", 4);			//ljb 20170720 add
	int nSelOption = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "CAN Comm Option Select", 1);	//ljb 20170720 add
	BOOL bUseOnlyMasterStrDlg = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "UseCanCheckModeOnlyMaster",  FALSE);	//20150825 캔 통신체크 모드 //20180518 yulee master만 사용 시
	
	if(bUseOnlyMasterStrDlg == FALSE)
	{
		//ljb 20170328 add
		m_ctrlCboCanCheck.ResetContent();
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg2","IDD_START_DLG")); //CAN 통신체크 해제
		m_ctrlCboCanCheck.SetItemData(0,0);
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg1","IDD_START_DLG")); //CAN 통신 체크 Master + Slave
		m_ctrlCboCanCheck.SetItemData(1,1);

		if (nCount == 4)
		{
			m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg3","IDD_START_DLG"));	//CAN 통신체크 Master
			m_ctrlCboCanCheck.SetItemData(2,2);
			m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg4","IDD_START_DLG"));	//CAN 통신체크 Slave
			m_ctrlCboCanCheck.SetItemData(3,3);
		}
	}
	else
	{
		//ljb 20170328 add
		m_ctrlCboCanCheck.ResetContent();
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg2","IDD_START_DLG"));	//CAN 통신체크 해제
		m_ctrlCboCanCheck.SetItemData(0,0);
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg3","IDD_START_DLG"));  //CAN 통신체크 Master
		m_ctrlCboCanCheck.SetItemData(1,1);
		if(nSelOption == 2)
			nSelOption--;
	}
#if 0
	if(bUseOnlyMasterStrDlg == FALSE)
	{
	//ljb 20170328 add
		m_ctrlCboCanCheck.ResetContent();
	/*m_ctrlCboCanCheck.AddString("CAN Communication Check Release");	
	m_ctrlCboCanCheck.SetItemData(0,0);
	m_ctrlCboCanCheck.AddString("CAN Communication Check Master + Slave");	
		m_ctrlCboCanCheck.SetItemData(1,1);

		if (nCount == 4)
		{
		m_ctrlCboCanCheck.AddString("CAN Communication Check Master");	
		m_ctrlCboCanCheck.SetItemData(2,2);
		m_ctrlCboCanCheck.AddString("CAN Communication Check Slave");	
		m_ctrlCboCanCheck.SetItemData(3,3);
		}
	}*/
		CString STR = Fun_FindMsg("StartDlg_Can_msg1","IDD_START_DLG");
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg1","IDD_START_DLG"));	
		m_ctrlCboCanCheck.SetItemData(0,0);
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg2","IDD_START_DLG"));	
		m_ctrlCboCanCheck.SetItemData(1,1);

		if (nCount == 4)
		{
			m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg3","IDD_START_DLG"));	
			m_ctrlCboCanCheck.SetItemData(2,2);
			m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_Can_msg4","IDD_START_DLG"));	
			m_ctrlCboCanCheck.SetItemData(3,3);
		}
	}
	else
	{
		//ljb 20170328 add
		m_ctrlCboCanCheck.ResetContent();
		//m_ctrlCboCanCheck.AddString("CAN 통신체크 해제");	
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_InitGrid_msg1","IDD_START_DLG"));	//&&
		m_ctrlCboCanCheck.SetItemData(0,0);
		//m_ctrlCboCanCheck.AddString("CAN 통신체크 Master");
		m_ctrlCboCanCheck.AddString(Fun_FindMsg("StartDlg_InitGrid_msg2","IDD_START_DLG"));//&&
		m_ctrlCboCanCheck.SetItemData(1,1);
		if(nSelOption == 2)
			nSelOption--;
	}
#endif
	//m_ctrlCboCanCheck.SetCurSel(1);
	
	m_ctrlCboCanCheck.SetCurSel(nSelOption);


/*	int nSize = m_pSelChArray->GetSize();
	if(nSize > 0)
	{
		if(m_bModuleCmd)
		{
			int nRow = 0;
			CCyclerModule *pModule;
			for(int i=0; i<nSize; i++)
			{
				pModule = m_pDoc->GetCyclerMD(m_pSelChArray->GetAt(i));
				if(pModule)
				{
					for(int c=0; c<pModule->GetTotalChannel(); c++)
					{
						str.Format("M%02d CH%03d", pModule->GetModuleID(), c+1);
						m_Grid.InsertRow(str);
						nRow++;

						m_Grid.SetItemText(nRow, 0, str);
						m_Grid.SetItemFormat(nRow, 0, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
						
						m_Grid.SetItemText(nRow, 1, m_strTrayNo);
						m_Grid.SetItemFormat(nRow, 1, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
						
						str.Format("%s_%d", m_strLot, nRow);
						m_Grid.SetItemText(nRow, 2, str);
						m_Grid.SetItemFormat(nRow, 2, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
					}
				}
			}
		}
		else
		{
			m_Grid.SetRowCount(nSize+1);
			for(int i=0; i<nSize; i++)
			{
				str.Format("CH%03d", m_pSelChArray->GetAt(i)+1);
				m_Grid.SetItemText(i+1, 0, str);
				m_Grid.SetItemFormat(i+1, 0, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
				
				m_Grid.SetItemText(i+1, 1, m_strTrayNo);
				m_Grid.SetItemFormat(i+1, 1, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
				
				str.Format("%s_%d", m_strLot, m_pSelChArray->GetAt(i)+1);
				m_Grid.SetItemText(i+1, 2, str);
				m_Grid.SetItemFormat(i+1, 2, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
			}
		}
	}
*/
}

void CStartDlg::Fun_CheckUseOven() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	UpdateOvenMode();

	CCyclerChannel *pChannel;
	int nRow = 0;
	CString str;
	for(int c=0; c<m_paPtrCh->GetSize(); c++)
	{
		 pChannel = (CCyclerChannel *)m_paPtrCh->GetAt(c);
		 if(pChannel)
		 {
			nRow++;
			if(pChannel->GetUseOvenMode())
			{
				str = Fun_FindMsg("Fun_CheckUseOven_msg","IDD_START_DLG");
				//@ str = "연동채널";
			}
			else
			{
				str.Empty();
			}
			m_Grid.SetItemText(nRow, 3, str);
		}
	}
	
}

void CStartDlg::UpdateOvenMode()
{
	//Oven 연동할 Channel을 찾는다.
	CDWordArray adwSelChList, adwOvenList;
	WORD nModuleID,  nChIndex;
	CCyclerChannel *pChannel;
	for(int ch = 0; ch<m_paPtrCh->GetSize(); ch++)
	{
		//선택 Channel을 1개씩 Scan 함 
		pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(ch);
		nModuleID = pChannel->GetModuleID();
		nChIndex = pChannel->GetChannelIndex();	
		adwSelChList.Add(MAKELONG(nChIndex, nModuleID));
		pChannel->SetUseOvenMode(FALSE);
	}

	if(m_nStartOptChamber == 3)
	{
		m_pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndexList(adwSelChList , adwOvenList);
		int a;
		int nOvenIndex;
		for( a = 0; a<adwOvenList.GetSize(); a++)
		{
			for(int ch = 0; ch<m_paPtrCh->GetSize(); ch++)
			{
				pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(ch);
				nModuleID = pChannel->GetModuleID();
				nChIndex = pChannel->GetChannelIndex();	
				nOvenIndex = m_pDoc->m_ChamberCtrl.m_ctrlOven1.GetOvenIndex(nModuleID, nChIndex);
				if(adwOvenList.GetAt(a) == nOvenIndex)
				{
					pChannel->SetUseOvenMode(TRUE);
					break;
				}
			}
		}

		m_pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndexList(adwSelChList , adwOvenList);
		for( a = 0; a<adwOvenList.GetSize(); a++)
		{
			for(int ch = 0; ch<m_paPtrCh->GetSize(); ch++)
			{
				pChannel =(CCyclerChannel *) m_paPtrCh->GetAt(ch);
				nModuleID = pChannel->GetModuleID();
				nChIndex = pChannel->GetChannelIndex();	
				nOvenIndex = m_pDoc->m_ChamberCtrl.m_ctrlOven2.GetOvenIndex(nModuleID, nChIndex);
				if(adwOvenList.GetAt(a) == nOvenIndex)
				{
					pChannel->SetUseOvenMode(TRUE);
					break;
				}
			}
		}
	}
}

void CStartDlg::OnChkChamberFix() 
{
	m_nStartOptChamber = 1;	
//	CheckDlgButton(IDC_CHK_CHAMBER_CONTINUE,TRUE);  
	m_chkChamberContinue = FALSE;//yulee 20190715
	GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->EnableWindow(FALSE);
}

void CStartDlg::OnChkChamberProg() 
{
	m_nStartOptChamber = 2;	
//	CheckDlgButton(IDC_CHK_CHAMBER_CONTINUE,TRUE);  
	m_chkChamberContinue = TRUE;//yulee 20190715
	GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->EnableWindow(FALSE);
}

void CStartDlg::OnChkChamberStep() 
{
	m_nStartOptChamber = 3;	
	Fun_CheckUseOven();
	GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->EnableWindow(TRUE);
}

void CStartDlg::OnChkChamberNo() 
{
	m_nStartOptChamber = 4;	
//	CheckDlgButton(IDC_CHK_CHAMBER_CONTINUE,TRUE);  
	m_chkChamberContinue = FALSE; //yulee 20190715
	GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->EnableWindow(FALSE);
}

void CStartDlg::OnChkChamberNoAndStop() 
{
	m_nStartOptChamber = 5;	
	//	CheckDlgButton(IDC_CHK_CHAMBER_CONTINUE,TRUE);  
	m_chkChamberContinue = FALSE;
	GetDlgItem(IDC_CHK_CHAMBER_CONTINUE)->EnableWindow(FALSE);
}