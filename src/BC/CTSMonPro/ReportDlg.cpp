// ReportDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "ReportDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReportDlg dialog


CReportDlg::CReportDlg(CWnd* pParent /*=NULL*/)
: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CReportDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CReportDlg::IDD2):
	(CReportDlg::IDD),pParent
{
	//{{AFX_DATA_INIT(CReportDlg)
	//}}AFX_DATA_INIT
}


void CReportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportDlg)
	DDX_Control(pDX, IDC_LIST_ENABLE, m_ctrlEnableList);
	DDX_Control(pDX, IDC_LIST_DISABLE, m_ctrlDisableList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportDlg, CDialog)
	//{{AFX_MSG_MAP(CReportDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReportDlg message handlers

BOOL CReportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	Initialize();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CReportDlg::Initialize()
{
	CString strMsg;
	bool bRefCh = false;

	for( int i = 0; i < MAX_MODULE_NUM; i++ )
	{
		for ( int j = 0; j < MAX_CHBOARD_NUM; j++ )
		{
			if ( i == 3 && j > 3 )
				break;

			strMsg.Format("%d Module %03d Channel", i+1, j+1);
			bRefCh = m_bRefCh[i * MAX_CHBOARD_NUM + j];
			if ( bRefCh && m_bChannel[i][j] && (m_nModule[i] == MODULE_STATE_CONN) )
			{
				m_ctrlEnableList.AddString(strMsg);
				m_ctrlEnableList.SetTopIndex(m_ctrlEnableList.GetCount()-1);
			}
			else if ( bRefCh && m_bChannel[i][j] && (m_nModule[i] == MODULE_STATE_DISCONN) )
			{
				m_ctrlDisableList.AddString(strMsg);
				m_ctrlDisableList.SetTopIndex(m_ctrlDisableList.GetCount()-1);
			}
			else if ( bRefCh && !m_bChannel[i][j] )
			{
				m_ctrlDisableList.AddString(strMsg);
				m_ctrlDisableList.SetTopIndex(m_ctrlDisableList.GetCount()-1);
			}
		}
	}
}

void CReportDlg::SetChannelData(bool bChannel[][MAX_CHBOARD_NUM], bool bRefCh[], int nModule[])
{
	memcpy(m_nModule, nModule, sizeof(m_nModule));
	memcpy(m_bRefCh, bRefCh, sizeof(m_bRefCh));
	memcpy(m_bChannel, bChannel, sizeof(m_bChannel));	
}
