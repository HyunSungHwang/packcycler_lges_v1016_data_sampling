// Dlg_CellBALOpt.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "Dlg_CellBALOpt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALOpt dialog


Dlg_CellBALOpt::Dlg_CellBALOpt(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_CellBALOpt::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_CellBALOpt::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_CellBALOpt::IDD3):
	(Dlg_CellBALOpt::IDD), pParent)
{
//	{{AFX_DATA_INIT(Dlg_CellBALOpt)
		// NOTE: the ClassWizard will add member initialization here
//}}AFX_DATA_INIT
}


void Dlg_CellBALOpt::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_CellBALOpt)
	//DDX_Control(pDX, IDC_CHK_CELLBALLOG_RESULT, m_ChkCellBalLogRunInResult);
	DDX_Control(pDX, IDC_EDIT_CELLBAL_AUXDIV3, m_EditCellBalAuxDiv3);
	DDX_Control(pDX, IDC_CHK_CELLBALLOG, m_ChkCellBalLog);
	DDX_Control(pDX, IDC_CHK_CELLBALSHOW, m_ChkCellBalShow);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_CellBALOpt, CDialog)
	//{{AFX_MSG_MAP(Dlg_CellBALOpt)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_CHK_CELLBALSHOW, OnChkCellbalshow)
	ON_BN_CLICKED(IDC_CHK_CELLBALLOG, OnChkCellballog)
	ON_EN_CHANGE(IDC_EDIT_CELLBAL_AUXDIV3, OnChangeEditCellbalAuxdiv3)
	//ON_BN_CLICKED(IDC_CHK_CELLBALLOG_RESULT, OnChkCellballogResult)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_CellBALOpt message handlers

BOOL Dlg_CellBALOpt::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	InitObject();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_CellBALOpt::OnOK() 
{
	//CDialog::OnOK();
}

void Dlg_CellBALOpt::OnCancel() 
{
	//CDialog::OnCancel();
}

void Dlg_CellBALOpt::OnClose() 
{
	//CDialog::OnClose();
}

BOOL Dlg_CellBALOpt::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP)
	{
		if(pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_RETURN)
		{					  
			return FALSE;	
		}					
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_CellBALOpt::InitObject() 
{
	
	
}

void Dlg_CellBALOpt::SetValue() 
{
	m_ChkCellBalShow.SetCheck(g_AppInfo.iCellBalShow);
	m_ChkCellBalLog.SetCheck(g_AppInfo.iCellBalLog);
	//m_ChkCellBalLogRunInResult.SetCheck(g_AppInfo.iCEllBalLogRun);
	m_EditCellBalAuxDiv3.SetWindowText(GStr::str_IntToStr(g_AppInfo.iCellBalAuxDiv3));
}

void Dlg_CellBALOpt::OnChkCellbalshow() 
{
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkCellBalShow.GetCheck();
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_SHOW", bflag);
	g_AppInfo.iCellBalShow=bflag;
	
	
	UpdateData(FALSE);	
}

void Dlg_CellBALOpt::OnChkCellballog() 
{
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkCellBalLog.GetCheck();
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_LOG", bflag);
	g_AppInfo.iCellBalLog=bflag;
	
	UpdateData(FALSE);	
}

void Dlg_CellBALOpt::OnChangeEditCellbalAuxdiv3() 
{
	UpdateData(TRUE);
	
	CString strValue=GWin::win_GetText(&m_EditCellBalAuxDiv3);
	int iValue=atoi(strValue);
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_AUXDIV3", iValue);
	g_AppInfo.iCellBalAuxDiv3=iValue;
	
	UpdateData(FALSE);	
}

/*void Dlg_CellBALOpt::OnChkCellballogResult() 
{
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkCellBalLogRunInResult.GetCheck();
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "CELLBAL_LOG_RUNINRESULT", bflag);
	g_AppInfo.iCEllBalLogRun=bflag;
	
	UpdateData(FALSE);	
}*/
