// Dlg_PwrSupplyStat.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "Dlg_PwrSupplyStat.h"

#include "Global.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_PwrSupplyStat dialog


Dlg_PwrSupplyStat::Dlg_PwrSupplyStat(CCTSMonProDoc * pDoc,CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_PwrSupplyStat::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_PwrSupplyStat::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_PwrSupplyStat::IDD3):
	(Dlg_PwrSupplyStat::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_PwrSupplyStat)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pDoc=pDoc;
	
	m_nItem=-1;
}


void Dlg_PwrSupplyStat::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_PwrSupplyStat)
	DDX_Control(pDX, IDC_STATIC_PWRSUPPLY_VOLT2, m_StaticPwrSupplyVOLT2);
	DDX_Control(pDX, IDC_STATIC_PWRSUPPLY_VOLT1, m_StaticPwrSupplyVOLT1);
	DDX_Control(pDX, IDC_EDIT_PWRSUPPLY_VOLT2, m_EditPwrSupplyVOLT2);
	DDX_Control(pDX, IDC_EDIT_PWRSUPPLY_VOLT1, m_EditPwrSupplyVOLT1);
	DDX_Control(pDX, IDC_STATIC_PWRSUPPLY_COMM, m_StaticPwrSupplyComm);
	DDX_Control(pDX, IDC_STATIC_PWRSUPPLY_OUTP, m_StaticPwrSupplyOUTP);
	DDX_Control(pDX, IDC_STATIC_PWRSUPPLY_INST, m_StaticPwrSupplyINST);
	DDX_Control(pDX, IDC_STATIC_PWRSUPPLY_ID, m_StaticPwrSupplyID);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_STATIC_AUTOSTOPSET1, m_StaticPwrAutoStopSetCh1);
	DDX_Control(pDX, IDC_STATIC_AUTOSTOPSET2, m_StaticPwrAutoStopSetCh2);
	DDX_Control(pDX, IDC_STATIC_AUTOSTOPSET_PAUSE1, m_StaticPwrAutoStopAtPauseSetCh1);
	DDX_Control(pDX, IDC_STATIC_AUTOSTOPSET_PAUSE2, m_StaticPwrAutoStopAtPauseSetCh2);
}


BEGIN_MESSAGE_MAP(Dlg_PwrSupplyStat, CDialog)
	//{{AFX_MSG_MAP(Dlg_PwrSupplyStat)
	ON_WM_CLOSE()
	ON_WM_LBUTTONDOWN()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUT_PWRSUPPLY_OUTP_ON, OnButPwrsupplyOutpOn)
	ON_BN_CLICKED(IDC_BUT_PWRSUPPLY_OUTP_OFF, OnButPwrsupplyOutpOff)
	ON_BN_CLICKED(IDC_BUT_PWRSUPPLY_VOLT1, OnButPwrsupplyVolt1)
	ON_BN_CLICKED(IDC_BUT_PWRSUPPLY_VOLT2, OnButPwrsupplyVolt2)
	ON_BN_CLICKED(IDC_BUT_PWRSUPPLY_INST1, OnButPwrsupplyInst1)
	ON_BN_CLICKED(IDC_BUT_PWRSUPPLY_INST2, OnButPwrsupplyInst2)
	ON_WM_WINDOWPOSCHANGED()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUT_AUTOSTOP1, &Dlg_PwrSupplyStat::OnBnClickedButAutostop1)
	ON_BN_CLICKED(IDC_BUT_AUTOSTOP2, &Dlg_PwrSupplyStat::OnBnClickedButAutostop2)
	ON_BN_CLICKED(IDC_BUT_AUTOSTOP_PAUSE1, &Dlg_PwrSupplyStat::OnBnClickedButAutostopPause1)
	ON_BN_CLICKED(IDC_BUT_AUTOSTOP_PAUSE2, &Dlg_PwrSupplyStat::OnBnClickedButAutostopPause2)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_PwrSupplyStat message handlers

BOOL Dlg_PwrSupplyStat::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	InitControl();

	SetTimer(100,500,NULL);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void Dlg_PwrSupplyStat::OnClose() 
{
	ShowWindow(SW_HIDE);
	//CDialog::OnClose();
}

void Dlg_PwrSupplyStat::OnCancel() 
{
	ShowWindow(SW_HIDE);
	//CDialog::OnCancel();
}

void Dlg_PwrSupplyStat::OnOK() 
{
	ShowWindow(SW_HIDE);
	//CDialog::OnOK();
}

BOOL Dlg_PwrSupplyStat::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP)
	{
		if(pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_RETURN)
		{					  
			return FALSE;	
		}					
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_PwrSupplyStat::InitControl()
{
	m_StaticPwrSupplyComm.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_DKGRAY)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);

	m_StaticPwrSupplyID.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);

	m_StaticPwrSupplyINST.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);

	m_StaticPwrSupplyOUTP.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);

	m_StaticPwrSupplyVOLT1.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(FALSE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);

	m_StaticPwrSupplyVOLT2.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(FALSE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);

	m_StaticPwrAutoStopSetCh1.SetFontSize(12) //yulee 20190702
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(FALSE)		
		.SetFontName("HY헤드라인M")
		.SetSunken(TRUE);

	m_StaticPwrAutoStopSetCh2.SetFontSize(12) //yulee 20190702
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(FALSE)		
		.SetFontName("HY헤드라인M")
		.SetSunken(TRUE);


	//ksj 20200601
	m_StaticPwrAutoStopAtPauseSetCh1.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(FALSE)		
		.SetFontName("HY헤드라인M") //폰트...;;;
		.SetSunken(TRUE);

	m_StaticPwrAutoStopAtPauseSetCh2.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(FALSE)		
		.SetFontName("HY헤드라인M") //폰트...;;;
		.SetSunken(TRUE);
	//ksj end
}

void Dlg_PwrSupplyStat::OnLButtonDown(UINT nFlags, CPoint point) 
{
	PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);

	CDialog::OnLButtonDown(nFlags, point);
}

void Dlg_PwrSupplyStat::OnTimer(UINT_PTR nIDEvent) 
{
	if(nIDEvent==100)
	{
		onSetState();
	}
	
	CDialog::OnTimer(nIDEvent);
}

void Dlg_PwrSupplyStat::onSetState() 
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;

	CString strTemp;
	
	//-------------------------------------
	int iComm=mainFrm->m_PwrSupplyCtrl[m_nItem].m_SerialPort.IsPortOpen();
	int iRecv=mainFrm->m_PwrSupplyCtrl[m_nItem].m_SerialPort.m_bRecv;
	int iError=mainFrm->m_PwrSupplyCtrl[m_nItem].m_iError;

	strTemp=_T("Disconnect");
	if(iComm==1)
	{
		strTemp=_T("Ready");
		if(iRecv==1)
		{
			strTemp=_T("Connect");			
		}
		if(iError>0)
		{
			//strTemp=TITLE_DISCONN;			
			strTemp=_T("Disconnect"); //ksj 20200129
		}
	}
	if(m_StaticPwrSupplyComm.GetText()!=strTemp)
	{
		m_StaticPwrSupplyComm.SetText(strTemp);		
	}	
	//-------------------------------------
	COLORREF nColor=ID_COLOR_DKGRAY;
	if(iComm==1) nColor=ID_COLOR_ORANGE;
	if(iRecv==1) nColor=ID_COLOR_GREEN;
	if(iError>0) nColor=ID_COLOR_RED;
	
	if(m_StaticPwrSupplyComm.GetBkColor()!=nColor)
	{
		m_StaticPwrSupplyComm.SetBkColor(nColor);
		m_StaticPwrSupplyComm.Invalidate(TRUE);
	}
	//-------------------------------------
	strTemp.Format(_T("%d, P/S No.%d"),m_nItem+1, m_nItem+1);
	if(m_StaticPwrSupplyID.GetText()!=strTemp)
	{
		m_StaticPwrSupplyID.SetText(strTemp);
	}
	
	int iInst=mainFrm->m_PwrSupplyCtrl[m_nItem].m_iState_Inst;
	strTemp.Format(_T("%d"),iInst);
	if(m_StaticPwrSupplyINST.GetText()!=strTemp)
	{
		m_StaticPwrSupplyINST.SetText(strTemp);
	}
	
	int iOutp=mainFrm->m_PwrSupplyCtrl[m_nItem].m_iState_Outp;
	strTemp.Format(_T("%s"),((iOutp==1)?_T("ON"):_T("OFF")));
	if(m_StaticPwrSupplyOUTP.GetText()!=strTemp)
	{
		m_StaticPwrSupplyOUTP.SetText(strTemp);
	}

	strTemp.Format(_T("%.2f"),mainFrm->m_PwrSupplyCtrl[m_nItem].m_fState_Volt[0]);
	if(m_StaticPwrSupplyVOLT1.GetText()!=strTemp)
	{
		m_StaticPwrSupplyVOLT1.SetText(strTemp);
	}
	
	strTemp.Format(_T("%.2f"),mainFrm->m_PwrSupplyCtrl[m_nItem].m_fState_Volt[1]);
	if(m_StaticPwrSupplyVOLT2.GetText()!=strTemp)
	{
		m_StaticPwrSupplyVOLT2.SetText(strTemp);
	}

	int nCHA=0;//yulee 20190702
	int nCHB=0;
	mainFrm->onGetSerialPowerGetSbcCh(m_nItem,nCHA,nCHB);

	BOOL bAutoStopChk1 = FALSE, bAutoStopChk2 = FALSE;
	CString strRegName;

	CCyclerChannel *pChannel=NULL;

	if(m_nItem == 0)
	{
		strRegName = REG_SERIAL_POWER1;
	}
	else
	{
		strRegName = REG_SERIAL_POWER2;
	}

	//-------------------------------------------------
	if(nCHA>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHA);
		if(pChannel)
		{
			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh1OffWhenWorkEnd" , FALSE);
			if(bAutoStopChk1 == TRUE)
			{
				strTemp.Format(_T("ON"));
			}
			else
			{
				strTemp.Format(_T("OFF"));
			}
			m_StaticPwrAutoStopSetCh1.SetText(strTemp);

			//ksj 20200601
			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh1OffWhenPause" , FALSE);
			if(bAutoStopChk1 == TRUE)
			{
				strTemp.Format(_T("ON"));
			}
			else
			{
				strTemp.Format(_T("OFF"));
			}
			m_StaticPwrAutoStopAtPauseSetCh1.SetText(strTemp);
			//ksj end
		}
	}
	//-------------------------------------------------
	if(nCHB>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHB);
		if(pChannel)
		{
			bAutoStopChk2 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh2OffWhenWorkEnd" , FALSE);
			if(bAutoStopChk2 == TRUE)
			{
				strTemp.Format(_T("ON"));
			}
			else
			{
				strTemp.Format(_T("OFF"));
			}
			m_StaticPwrAutoStopSetCh2.SetText(strTemp);

			//ksj 20200601
			bAutoStopChk2 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh2OffWhenPause" , FALSE);
			if(bAutoStopChk2 == TRUE)
			{
				strTemp.Format(_T("ON"));
			}
			else
			{
				strTemp.Format(_T("OFF"));
			}
			m_StaticPwrAutoStopAtPauseSetCh2.SetText(strTemp);
			//ksj end
		}
	}


	//ksj 20200228 : 폴란드 LGC 요청으로 파워서플라이 채널명 변경 //ksj 20200818 : 빈강 LGC 요청으로 v1016에도 추가
	//2번 파워 서플라이의 1번 2번 채널을 각각 3번 4번 채널로 표시
	if(m_nItem > 0)
	{
		if(GetDlgItem(IDC_STATIC_GROUP_CH1)->GetSafeHwnd())
		{
			strTemp.Format("CH%d",(m_nItem*2)+1);
			GetDlgItem(IDC_STATIC_GROUP_CH1)->SetWindowText(strTemp);
		}

		if(GetDlgItem(IDC_STATIC_GROUP_CH2)->GetSafeHwnd())
		{
			strTemp.Format("CH%d",(m_nItem*2)+2);	
			GetDlgItem(IDC_STATIC_GROUP_CH2)->SetWindowText(strTemp);
		}
	}	
	//ksj end
}

BOOL Dlg_PwrSupplyStat::onChkSbcRunState()
{
	int nCHA=0;
	int nCHB=0;
	mainFrm->onGetSerialPowerGetSbcCh(m_nItem,nCHA,nCHB);
	
	CString strTemp;
	CCyclerChannel *pChannel=NULL;
	
	//-------------------------------------------------
	if(nCHA>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHA);
		if(pChannel)
		{
			if( pChannel->GetState()==PS_STATE_RUN)
			{
				strTemp.Format(_T("CH:%d Working"),nCHA);
				AfxMessageBox(strTemp);
				return TRUE;
			}
		}
	}
	//-------------------------------------------------
	if(nCHB>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHB);
		if(pChannel)
		{
			if( pChannel->GetState()==PS_STATE_RUN)
			{
				strTemp.Format(_T("CH:%d Working"),nCHB);
				AfxMessageBox(strTemp);
				return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL Dlg_PwrSupplyStat::onChkSerialIsPort()
{
	if(m_nItem<0) return FALSE;
	if(!mainFrm)  return FALSE;

	CString strTemp;
	int iComm=mainFrm->m_PwrSupplyCtrl[m_nItem].m_SerialPort.IsPortOpen();
	if(iComm==FALSE)
	{
		strTemp.Format(_T("Port Disconnected1"));	
		AfxMessageBox(strTemp);
		return FALSE;
	}
	if(mainFrm->m_PwrSupplyCtrl[m_nItem].m_iError>0)
	{
		strTemp.Format(_T("Port Disconnected2"));	
		AfxMessageBox(strTemp);
		return FALSE;
	}
	return TRUE;
}

void Dlg_PwrSupplyStat::OnButPwrsupplyInst1() 
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;
	
	//-------------------------------------
	if(onChkSbcRunState()==TRUE) return;//RUN
	if(onChkSerialIsPort()==FALSE) return;//NO OPEN
	//-------------------------------------
	CString strTemp;
	strTemp.Format(_T("CH_0,INST OUTP1,INST?,"));
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);
}

void Dlg_PwrSupplyStat::OnButPwrsupplyInst2() 
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;
	
	//-------------------------------------
	if(onChkSbcRunState()==TRUE) return;//RUN
	if(onChkSerialIsPort()==FALSE) return;//NO OPEN
	//-------------------------------------
	CString strTemp;
	strTemp.Format(_T("CH_0,INST OUTP2,INST?,"));
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);
}


void Dlg_PwrSupplyStat::OnButPwrsupplyOutpOn() 
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;

	//-------------------------------------
	if(onChkSbcRunState()==TRUE) return;//RUN
	if(onChkSerialIsPort()==FALSE) return;//NO OPEN
	//-------------------------------------
	CString strTemp;
	strTemp.Format(_T("CH_0,OUTP ON,OUTP?,"));
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);
}

void Dlg_PwrSupplyStat::OnButPwrsupplyOutpOff() 
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;
	
	//-------------------------------------
	if(onChkSbcRunState()==TRUE) return;//RUN
	if(onChkSerialIsPort()==FALSE) return;//NO OPEN
	//-------------------------------------
	CString strTemp;
	strTemp.Format(_T("CH_0,OUTP OFF,OUTP?,"));
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);
}

void Dlg_PwrSupplyStat::OnButPwrsupplyVolt1() 
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;
	
	//-------------------------------------
	//if(onChkSbcRunState()==TRUE) return;//RUN //yulee 20190514 _ 담당자 해제 요청
	if(onChkSerialIsPort()==FALSE) return;//NO OPEN
	if(m_EditPwrSupplyVOLT1.IsWindowEnabled()==FALSE) return;
	//-------------------------------------
	CString strTemp;
	strTemp.Format(_T("CH_0,INST OUTP1,INST?,"));
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);

	CString strVolt=GWin::win_GetText(&m_EditPwrSupplyVOLT1);
	strTemp.Format(_T("CH_0,VOLT %s,VOLT?,"),strVolt);
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);
}

void Dlg_PwrSupplyStat::OnButPwrsupplyVolt2() 
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;
	
	//-------------------------------------
	//if(onChkSbcRunState()==TRUE) return;//RUN //yulee 20190514 _ 담당자 해제 요청
	if(onChkSerialIsPort()==FALSE) return;//NO OPEN
	if(m_EditPwrSupplyVOLT2.IsWindowEnabled()==FALSE) return;
	//-------------------------------------
	CString strTemp;
	strTemp.Format(_T("CH_0,INST OUTP2,INST?,"));
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);

	CString strVolt=GWin::win_GetText(&m_EditPwrSupplyVOLT2);
	strTemp.Format(_T("CH_0,VOLT %s,VOLT?,"),strVolt);
	mainFrm->m_PwrSupplyCtrl[m_nItem].onSerialPortWriteBuffer(strTemp);
}


void Dlg_PwrSupplyStat::OnWindowPosChanged(WINDOWPOS FAR* lpwndpos) 
{
	CDialog::OnWindowPosChanged(lpwndpos);
	
	onSizeSave(1);
}

void Dlg_PwrSupplyStat::onSizeSave(int iType)
{
	KillTimer(ID_PWRSUPPLY_TIMER_SIZE);
	if(iType==1)
	{
		struct ST_TIMER_SIZESAVE{
		static void CALLBACK TimeProcSizeSave(HWND hwnd,UINT iMsg,UINT_PTR wParam,DWORD lParam)
		{
			::KillTimer(hwnd,wParam);
			Dlg_PwrSupplyStat *dlg = (Dlg_PwrSupplyStat*)CWnd::FromHandle(hwnd);
			if(!dlg) return;
			
			dlg->onSizeSave(2);			
		}};
		SetTimer( ID_PWRSUPPLY_TIMER_SIZE, 100, ST_TIMER_SIZESAVE::TimeProcSizeSave);
	}
	else if(iType==2)
	{
		CRect rect;
		GetWindowRect(rect);

		CString strTemp;
		strTemp.Format(_T("%d,%d,%d,%d,"),rect.left,rect.top,rect.Width(),rect.Height());

		CString strKey;
		if(m_nItem==0)      strKey=REG_SERIAL_POWER1;
		else if(m_nItem==1) strKey=REG_SERIAL_POWER2;
		else return;

		AfxGetApp()->WriteProfileString(strKey, "Win_Size",strTemp);		
	}
	else if(iType==3)
	{		
		CString strKey;
		if(m_nItem==0)      strKey=REG_SERIAL_POWER1;
		else if(m_nItem==1) strKey=REG_SERIAL_POWER2;
		else return;
		
		CString strWinSize=AfxGetApp()->GetProfileString(strKey, "Win_Size");	
		if(strWinSize.IsEmpty() || strWinSize==_T("")) return;

		CString strLeft=GStr::str_GetSplit(strWinSize,0,',');
		CString strTop=GStr::str_GetSplit(strWinSize,1,',');
		CString strWidth=GStr::str_GetSplit(strWinSize,2,',');
		CString strHeight=GStr::str_GetSplit(strWinSize,3,',');

		CRect rect;
		rect.left=atoi(strLeft);
		rect.top=atoi(strTop);
		rect.right=rect.left+atoi(strWidth);
		rect.bottom=rect.top+atoi(strHeight);

		SIZE s;
		ZeroMemory(&s, sizeof(SIZE));
		s.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
		s.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);

		if((rect.left > s.cx )||(rect.top > s.cy))
		{
			rect.left=0;
			rect.top=0;
			rect.right=rect.left+atoi(strWidth);
			rect.bottom=rect.top+atoi(strHeight);
		}

		::SetWindowPos(this->m_hWnd, 
			           HWND_NOTOPMOST, 
					   rect.left, 
					   rect.top, 
					   rect.right, 
					   rect.bottom, 
					   SWP_NOSIZE |SWP_SHOWWINDOW);
	}
		
}


void Dlg_PwrSupplyStat::OnBnClickedButAutostop1() //yulee 20190702
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;


	int nCHA=0;//yulee 20190702
	int nCHB=0;
	mainFrm->onGetSerialPowerGetSbcCh(m_nItem,nCHA,nCHB);

	BOOL bAutoStopChk1 = FALSE, bAutoStopChk2 = FALSE;
	CString strRegName;

	CCyclerChannel *pChannel=NULL;

	if(m_nItem == 0)
	{
		strRegName = REG_SERIAL_POWER1;
	}
	else
	{
		strRegName = REG_SERIAL_POWER2;
	}

	//-------------------------------------------------
	if(nCHA>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHA);
		if(pChannel)
		{
			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh1OffWhenWorkEnd" , FALSE);
			AfxGetApp()->WriteProfileInt(strRegName, "PwrSplyCh1OffWhenWorkEnd" , (!bAutoStopChk1));
		}
	}
	//-------------------------------------------------
// 	if(nCHB>0)
// 	{
// 		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHB);
// 		if(pChannel)
// 		{
// 			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh2OffWhenWorkEnd" , FALSE);
// 			AfxGetApp()->WriteProfileInt(strRegName, "PwrSplyCh2OffWhenWorkEnd" , (!bAutoStopChk1));
// 		}
// 	}
}


void Dlg_PwrSupplyStat::OnBnClickedButAutostop2() //yulee 20190702
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;


	int nCHA=0;//yulee 20190702
	int nCHB=0;
	mainFrm->onGetSerialPowerGetSbcCh(m_nItem,nCHA,nCHB);

	BOOL bAutoStopChk1 = FALSE, bAutoStopChk2 = FALSE;
	CString strRegName;

	CCyclerChannel *pChannel=NULL;

	if(m_nItem == 0)
	{
		strRegName = REG_SERIAL_POWER1;
	}
	else
	{
		strRegName = REG_SERIAL_POWER2;
	}

	//-------------------------------------------------
// 	if(nCHA>0)
// 	{
// 		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHA);
// 		if(pChannel)
// 		{
// 			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh1OffWhenWorkEnd" , FALSE);
// 			AfxGetApp()->WriteProfileInt(strRegName, "PwrSplyCh1OffWhenWorkEnd" , (!bAutoStopChk1));
// 		}
// 	}
	//-------------------------------------------------
	if(nCHB>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHB);
		if(pChannel)
		{
			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh2OffWhenWorkEnd" , FALSE);
			AfxGetApp()->WriteProfileInt(strRegName, "PwrSplyCh2OffWhenWorkEnd" , (!bAutoStopChk1));
		}
	}
}

//ksj 20200601 : 채널 PAUSE시 파워 서플라이 정지.
void Dlg_PwrSupplyStat::OnBnClickedButAutostopPause1()
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;


	int nCHA=0;
	int nCHB=0;
	mainFrm->onGetSerialPowerGetSbcCh(m_nItem,nCHA,nCHB);

	BOOL bAutoStopChk1 = FALSE, bAutoStopChk2 = FALSE;
	CString strRegName;

	CCyclerChannel *pChannel=NULL;

	if(m_nItem == 0)
	{
		strRegName = REG_SERIAL_POWER1;
	}
	else
	{
		strRegName = REG_SERIAL_POWER2;
	}

	//-------------------------------------------------
	if(nCHA>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHA);
		if(pChannel)
		{
			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh1OffWhenPause" , FALSE);
			AfxGetApp()->WriteProfileInt(strRegName, "PwrSplyCh1OffWhenPause" , (!bAutoStopChk1));
		}
	}
}

//ksj 20200601 : 채널 PAUSE시 파워 서플라이 정지.
void Dlg_PwrSupplyStat::OnBnClickedButAutostopPause2()
{
	if(m_nItem<0) return;
	if(!mainFrm)  return;


	int nCHA=0;
	int nCHB=0;
	mainFrm->onGetSerialPowerGetSbcCh(m_nItem,nCHA,nCHB);

	BOOL bAutoStopChk1 = FALSE, bAutoStopChk2 = FALSE;
	CString strRegName;

	CCyclerChannel *pChannel=NULL;

	if(m_nItem == 0)
	{
		strRegName = REG_SERIAL_POWER1;
	}
	else
	{
		strRegName = REG_SERIAL_POWER2;
	}


	if(nCHB>0)
	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHB);
		if(pChannel)
		{
			bAutoStopChk1 = AfxGetApp()->GetProfileInt(strRegName, "PwrSplyCh2OffWhenPause" , FALSE);
			AfxGetApp()->WriteProfileInt(strRegName, "PwrSplyCh2OffWhenPause" , (!bAutoStopChk1));
		}
	}
}
