#if !defined(AFX_VOLTAGECHECKDLG_H__0BD3A76F_41AA_416E_8B84_E0532F31F597__INCLUDED_)
#define AFX_VOLTAGECHECKDLG_H__0BD3A76F_41AA_416E_8B84_E0532F31F597__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VoltageCheckDlg.h : header file
// ksj 20160726 안전조건 강화 (작업 시작전 배터리 전압 확인)

#include "CTSMonProDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CVoltageCheckDlg dialog

class CVoltageCheckDlg : public CDialog
{
// Construction
public:

	BOOL m_bCtrlCreated;
	CRect m_nLastCtrlRect;
	void InitCtrl();
	CCTSMonProDoc* m_pDoc;
	CDWordArray m_adwTargetChArray;
	CVoltageCheckDlg(CDWordArray& adwTargetChArray, CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // standard constructor

	CEdit* m_pEdit;
	CStatic* m_pStaticCh;
	CStatic* m_pStaticV;
// Dialog Data
	//{{AFX_DATA(CVoltageCheckDlg)
	enum { IDD = IDD_VOLTAGE_CHECK_DLG , IDD2 = IDD_VOLTAGE_CHECK_DLG_ENG , IDD3 = IDD_VOLTAGE_CHECK_DLG_PL };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVoltageCheckDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CVoltageCheckDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VOLTAGECHECKDLG_H__0BD3A76F_41AA_416E_8B84_E0532F31F597__INCLUDED_)
