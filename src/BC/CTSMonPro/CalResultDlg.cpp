// CalResultDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "CalResultDlg.h"
#include "CaliSetDlg.h"
#include "CTSMonProView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalResultDlg dialog

CCalResultDlg::CCalResultDlg(CCTSMonProView* pView, CWnd* pParent /*=NULL*/)
//	: CDialog(CCalResultDlg::IDD, pParent)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CCalResultDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CCalResultDlg::IDD2):
	(CCalResultDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CCalResultDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pView = pView;
	m_pCalResultData = NULL;
}

void CCalResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCalResultDlg)
	DDX_Control(pDX, IDC_LIST_CHECK_RESULT, m_wndCheckResult);
	DDX_Control(pDX, IDC_LIST_RESULT_H, m_wndResult_H);
	DDX_Control(pDX, IDC_LIST_RESULT_L, m_wndResult_L);
	DDX_Control(pDX, IDC_LIST_RESULT_M, m_wndResult_M);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCalResultDlg, CDialog)
	//{{AFX_MSG_MAP(CCalResultDlg)
	ON_BN_CLICKED(IDC_BTN_SET_POINT, OnBtnSetPoint)
	ON_BN_CLICKED(IDC_BTN_CONNECT, OnBtnConnect)
	ON_BN_CLICKED(IDC_BTN_DISCONNECT, OnBtnDisconnect)
	ON_BN_CLICKED(IDC_BTN_CALI_CRT_H, OnBtnCaliCrtH)
	ON_BN_CLICKED(IDC_BTN_CALI_CRT_L, OnBtnCaliCrtL)
	ON_BN_CLICKED(IDC_BTN_CALI_CRT_M, OnBtnCaliCrtM)
	ON_BN_CLICKED(IDC_BTN_UPDATE, OnBtnUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCalResultDlg message handlers

BOOL CCalResultDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	{ //List Property

		SetPointData();
		
		char szHeader[][20] = 
			{"Point", "AD", "DVM"};
		int	cx[] = 
			{ 60, 65, 65};

		CListCtrl* pWndResult = NULL;
		for( int nI = 0; nI < 3; nI++ )
		{
			switch( nI )
			{
			case 0:	pWndResult = &m_wndResult_H; break;
			case 1:	pWndResult = &m_wndResult_L; break;
			case 2:	pWndResult = &m_wndResult_M; break;
			}
			for ( int i = 0; i < 3; i++ )
			{
				pWndResult->InsertColumn( i, szHeader[i], LVCFMT_CENTER, cx[i] );
			}
			DWORD dwExStyle = pWndResult->GetExtendedStyle();
			dwExStyle |= LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
			pWndResult->SetExtendedStyle( dwExStyle );

			MakeList(nI);
		}
	}

	GetDlgItem(IDC_BTN_DISCONNECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_V)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_CRT_H)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_CRT_L)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_CRT_M)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE)->EnableWindow(FALSE);

	char szBuff[64];
	sprintf(szBuff, "Calibration ▒▒ Module %d Channel %d", m_nModuleID, m_nChannelID);
	SetWindowText(szBuff);
	return TRUE;
}

//void CCalResultDlg::SetConfigurationData(CAL_POINT* pCalPoint, 
//										 CAL_POINT* pCheckPoint, 
//										 int nModuleIndex, int nChannelIndex)
//{
//	m_pCalPoint = pCalPoint;
//	m_pCheckPoint = pCheckPoint;
//	m_nModuleID = nModuleIndex+1;
//	m_nChannelID = nChannelIndex+1;
//}

void CCalResultDlg::SetData(int nMode, long lCmdID, long lCmdPoint, long lADVal, long lDVMVal)
{
	CListCtrl* pWndResult = NULL;
	switch( nMode )
	{
	case 0:	pWndResult = &m_wndResult_H;	break;
	case 1:	pWndResult = &m_wndResult_L;	break;
	case 2:	pWndResult = &m_wndResult_M;	break;
	default:	return;
	}

	CString strTemp;
	LV_ITEM aItem;
	aItem.mask = LVIF_TEXT;
	if( lCmdID < m_pCalPoint[nMode].byValidPointNum )
	{
		aItem.iItem = lCmdID;		
		strTemp.Format("%.2f", (float)lADVal/DIVIDE_LONG_2_FLOAT);
		aItem.iSubItem = 1;	
		aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		pWndResult->SetItem(&aItem);

		strTemp.Format("%.2f", (float)lDVMVal/DIVIDE_LONG_2_FLOAT);
		aItem.iSubItem = 2;	
		aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		pWndResult->SetItem(&aItem);
	}

/*	CListCtrl* pWndResult = NULL;
	CPtrArray* pResultData = NULL;
	switch( nMode )
	{
	case 0:	pWndResult = &m_wndResult_H; pResultData = &m_aCalResultData_H; break;
	case 1:	pWndResult = &m_wndResult_L; pResultData = &m_aCalResultData_L; break;
	case 2:	pWndResult = &m_wndResult_M; pResultData = &m_aCalResultData_M; break;
	}

	CString strTemp;
	LP_CAL_RESULT_DATA pData = NULL;	
	LV_ITEM aItem;
	ZeroMemory(&aItem, sizeof(aItem));
	aItem.mask = LVIF_TEXT;

	for( int nI = 0; nI < pResultData->GetSize(); nI++ )
	{
		pData = (LP_CAL_RESULT_DATA)pResultData->GetAt(nI);
		if( pData->lCmdID == lCmdID )
		{
			pData->lCmdPoint	= lCmdPoint;
			pData->lADVal		= lADVal;
			pData->lDVMVal		= lDVMVal;

			aItem.iItem = nI;		
			strTemp.Format("%.2f", (float)lADVal/10000.0f);
			aItem.iSubItem = 1;	
			aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
			pWndResult->SetItem(&aItem);

			strTemp.Format("%.2f", (float)lDVMVal/10000.0f);
			aItem.iSubItem = 2;	
			aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
			pWndResult->SetItem(&aItem);
			break;
		}
	}
*/
}

void CCalResultDlg::ClearList(int nMode)
{
	CListCtrl* pWndResult = NULL;
	CPtrArray* pResultData = NULL;
	switch( nMode )
	{
	case 0:	pWndResult = &m_wndResult_H; pResultData = &m_aCalResultData_H; break;
	case 1:	pWndResult = &m_wndResult_L; pResultData = &m_aCalResultData_L; break;
	case 2:	pWndResult = &m_wndResult_M; pResultData = &m_aCalResultData_M; break;
	}

	pWndResult->DeleteAllItems();

	LP_CAL_RESULT_DATA pData = NULL;
	for( int nI = 0; nI < pResultData->GetSize(); nI++ )
	{
		pData = (LP_CAL_RESULT_DATA)pResultData->GetAt(nI);
		ASSERT(pData);
		delete pData;
	}
	pResultData->RemoveAll();
}

void CCalResultDlg::MakeList(int nMode)
{
	CListCtrl* pWndResult = NULL;
	CPtrArray* pResultData = NULL;
	switch( nMode )
	{
	case 0:	pWndResult = &m_wndResult_H; pResultData = &m_aCalResultData_H; break;
	case 1:	pWndResult = &m_wndResult_L; pResultData = &m_aCalResultData_L; break;
	case 2:	pWndResult = &m_wndResult_M; pResultData = &m_aCalResultData_M; break;
	}

	pWndResult->DeleteAllItems();

	LP_CAL_RESULT_DATA pData = NULL;
	CString strTemp;
	LVITEM aItem;
	ZeroMemory(&aItem, sizeof(aItem));
	aItem.mask = LVIF_TEXT;

	long lCmdPoint, lADVal, lDVMVal;	
	for( int nI = 0; nI < pResultData->GetSize(); nI++ )
	{
		aItem.iItem = nI;		

		pData = (LP_CAL_RESULT_DATA)pResultData->GetAt(nI);
		lCmdPoint	= pData->lCmdPoint;
		lADVal		= pData->lADVal;
		lDVMVal		= pData->lDVMVal;

		strTemp.Format("%.2f", (float)lCmdPoint/DIVIDE_LONG_2_FLOAT);
		aItem.iSubItem = 0;	
		aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		pWndResult->InsertItem(&aItem);

		strTemp.Format("%.2f", (float)lADVal/DIVIDE_LONG_2_FLOAT);
		aItem.iSubItem = 1;	
		aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		pWndResult->SetItem(&aItem);

		strTemp.Format("%.2f", (float)lDVMVal/DIVIDE_LONG_2_FLOAT);
		aItem.iSubItem = 2;	
		aItem.pszText = (LPSTR)(LPCTSTR)strTemp;
		pWndResult->SetItem(&aItem);
	}
}

void CCalResultDlg::OnBtnSetPoint() 
{
//	CCaliSetDlg dlg(m_pCalPoint, m_pCheckPoint, 1);
//	dlg.SetCellType(m_pView->GetCellType());
//
//	if( dlg.DoModal() == IDCANCEL )
//		return;
//
//	SetPointData();
//	for( int nI = 0; nI < 3; nI++ )
//		MakeList(nI);
}

void CCalResultDlg::SetPointData()
{
	CPtrArray* pResultData = NULL;
	LP_CAL_RESULT_DATA pData = NULL;
	for( int nI = 0; nI < 3; nI++ )
	{
		ClearList(nI);	
		switch( m_pCalPoint[nI].byType )
		{
		case 0:	pResultData = &m_aCalResultData_H;	break;
		case 1:	pResultData = &m_aCalResultData_L;	break;
		case 2:	pResultData = &m_aCalResultData_M;	break;
		}
		for( int nX = 0; nX < m_pCalPoint[nI].byValidPointNum; nX++ )
		{
			pData = new S_CAL_RESULT_DATA;
			ZeroMemory(pData, sizeof(S_CAL_RESULT_DATA));
			pData->lCmdID = nX;
			pData->lCmdPoint = m_pCalPoint[nI].lPoint[nX];
			pResultData->Add(pData);
		}
	}
}

void CCalResultDlg::OnCommConnected()
{
//	GetDlgItem(IDC_BTN_DISCONNECT)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_CALI_V)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_CRT_H)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_CALI_CRT_L)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_CALI_CRT_M)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_UPDATE)->EnableWindow(FALSE);
}

void CCalResultDlg::OnCommDisconnected()
{
	GetDlgItem(IDC_BTN_DISCONNECT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_V)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_CRT_H)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_CRT_L)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_CALI_CRT_M)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_UPDATE)->EnableWindow(FALSE);
}

void CCalResultDlg::OnCalDataModified()
{
	GetDlgItem(IDC_BTN_UPDATE)->EnableWindow(TRUE);
}

void CCalResultDlg::OnBtnConnect() 
{
	DWORD dwChannel[2];	
	ZeroMemory(dwChannel, sizeof(dwChannel));

	if( m_nChannelID < sizeof(DWORD)*8 )
		dwChannel[0] |= 0x00000001 << (m_nChannelID-1);
	else
		dwChannel[1] |= 0x00000001 << (m_nChannelID-1-(sizeof(DWORD)*8));
	
	if( ::SFTSendCommand( m_nModuleID, CMD_COMM_CONNECT, (LPSFT_CH_SEL_DATA)dwChannel, NULL, 0) )
	{
		OnCommConnected();
		
// 		CString strMsg;
// 		strMsg.Format("Module %02d :: HP34401 Connected", m_nModuleID);
//		m_pView->WriteLog(strMsg, CT_LOG_LEVEL_DETAIL);
	}
	else
	{
		OnCommDisconnected();

		CString strMsg;
		strMsg.Format("Module %d Channel %d::HP34401과의 통신설정에 실패했습니다.\n%s",
			m_nModuleID, m_nChannelID, 
			CResponseCmdID::GetErrorMsg(::SFTGetLastError()));
//		m_pView->WriteLog(strMsg, CT_LOG_LEVEL_NORMAL);
		MessageBox(strMsg, "Calibration 실패", MB_ICONEXCLAMATION);
	}
}

void CCalResultDlg::OnBtnDisconnect() 
{
	DWORD dwChannel[2];	
	ZeroMemory(dwChannel, sizeof(dwChannel));

	if( m_nChannelID < sizeof(DWORD)*8 )
		dwChannel[0] |= 0x00000001 << (m_nChannelID-1);
	else
		dwChannel[1] |= 0x00000001 << (m_nChannelID-1-(sizeof(DWORD)*8));
	
	::SFTSendCommand(m_nModuleID,  CMD_COMM_DISCONNECT, (LPSFT_CH_SEL_DATA)dwChannel,NULL, 0);
	OnCommDisconnected();
}

void CCalResultDlg::OnBtnCaliCrtH() 
{
	DWORD dwChannel[2];	
	ZeroMemory(dwChannel, sizeof(dwChannel));

	if( m_nChannelID < sizeof(DWORD)*8 )
		dwChannel[0] |= 0x00000001 << (m_nChannelID-1);
	else
		dwChannel[1] |= 0x00000001 << (m_nChannelID-1-(sizeof(DWORD)*8));
	
	S_CALI_SET aCaliSet;
	ZeroMemory(&aCaliSet, sizeof(S_CALI_SET));
	CopyMemory(aCaliSet.lCaliPoint, m_pCalPoint[0].lPoint, sizeof(aCaliSet.lCaliPoint));
	CopyMemory(aCaliSet.lCheckPoint, m_pCheckPoint[0].lPoint, sizeof(aCaliSet.lCheckPoint));
	aCaliSet.byRange = CALI_RANGE_HIGH;
	aCaliSet.byValidCalPointNum = m_pCalPoint[0].byValidPointNum;
	aCaliSet.byValidCheckPointNum = m_pCheckPoint[0].byValidPointNum;
	
	if( ::SFTSendCommand(m_nModuleID,  CMD_CALI_C_H, (LPSFT_CH_SEL_DATA)dwChannel,
					(BYTE*)&aCaliSet, sizeof(aCaliSet)) == TRUE )
	{
		OnCalDataModified();

		CString strMsg;
		//strMsg.Format("M%02dCh%03d :: Range-1 Calibration 지령전송 성공", m_nModuleID, m_nChannelID);
		strMsg.Format("Module %02d :: Channel %02d Range-1 Calibration 지령전송 성공", m_nModuleID, m_nChannelID);
//		m_pView->WriteLog(strMsg, (BYTE)EP_LIST_COLOR_BLACK);
	}
	else
	{
		CString strMsg;
		strMsg.Format("Module %d Channel %d::Range-1의 Calibration 지령 전송에 실패했습니다."
			"\n%s",
			m_nModuleID, m_nChannelID, 
			CResponseCmdID::GetErrorMsg(::SFTGetLastError()));
		MessageBox(strMsg, "Calibration 실패", MB_ICONEXCLAMATION);
	}
}

void CCalResultDlg::OnBtnCaliCrtL() 
{
	DWORD dwChannel[2];	
	ZeroMemory(dwChannel, sizeof(dwChannel));

	if( m_nChannelID < sizeof(DWORD)*8 )
		dwChannel[0] |= 0x00000001 << (m_nChannelID-1);
	else
		dwChannel[1] |= 0x00000001 << (m_nChannelID-1-(sizeof(DWORD)*8));
	
	S_CALI_SET aCaliSet;
	ZeroMemory(&aCaliSet, sizeof(S_CALI_SET));
	CopyMemory(aCaliSet.lCaliPoint, m_pCalPoint[1].lPoint, sizeof(aCaliSet.lCaliPoint));
	CopyMemory(aCaliSet.lCheckPoint, m_pCheckPoint[1].lPoint, sizeof(aCaliSet.lCheckPoint));
	aCaliSet.byRange = CALI_RANGE_LOW;
	aCaliSet.byValidCalPointNum = m_pCalPoint[1].byValidPointNum;
	aCaliSet.byValidCheckPointNum = m_pCheckPoint[1].byValidPointNum;

	if( ::SFTSendCommand( m_nModuleID, CMD_CALI_C_L, (LPSFT_CH_SEL_DATA)dwChannel, 
					(BYTE*)&aCaliSet, sizeof(aCaliSet)) == TRUE )
	{
		OnCalDataModified();
		
		CString strMsg;
		//strMsg.Format("M%02dCh%03d :: Range-2 Calibration 지령전송 성공", m_nModuleID, m_nChannelID);
		strMsg.Format("Module %02d :: Channel %02d Range-2 Calibration 지령전송 성공", m_nModuleID, m_nChannelID);
//		m_pView->WriteLog(strMsg, (BYTE)EP_LIST_COLOR_BLACK);
	}
	else
	{
		CString strMsg;
		strMsg.Format("Module %d Channel %d::Range-2의 Calibration 지령 전송에 실패했습니다."
			"\n%s",
			m_nModuleID, m_nChannelID, 
			CResponseCmdID::GetErrorMsg(::SFTGetLastError()));
		MessageBox(strMsg, "Calibration 실패", MB_ICONEXCLAMATION);
	}
}

void CCalResultDlg::OnBtnCaliCrtM() 
{
	DWORD dwChannel[2];	
	ZeroMemory(dwChannel, sizeof(dwChannel));

	if( m_nChannelID < sizeof(DWORD)*8 )
		dwChannel[0] |= 0x00000001 << (m_nChannelID-1);
	else
		dwChannel[1] |= 0x00000001 << (m_nChannelID-1-(sizeof(DWORD)*8));
	
	S_CALI_SET aCaliSet;
	ZeroMemory(&aCaliSet, sizeof(S_CALI_SET));
	CopyMemory(aCaliSet.lCaliPoint, m_pCalPoint[2].lPoint, sizeof(aCaliSet.lCaliPoint));
	CopyMemory(aCaliSet.lCheckPoint, m_pCheckPoint[2].lPoint, sizeof(aCaliSet.lCheckPoint));
	aCaliSet.byRange = CALI_RANGE_MICRO;
	aCaliSet.byValidCalPointNum = m_pCalPoint[2].byValidPointNum;
	aCaliSet.byValidCheckPointNum = m_pCheckPoint[2].byValidPointNum;

	if( ::SFTSendCommand( m_nModuleID,  CMD_CALI_C_M, (LPSFT_CH_SEL_DATA)dwChannel,
					(BYTE*)&aCaliSet, sizeof(aCaliSet)) == TRUE )	
	{
		OnCalDataModified();
		
		CString strMsg;
		//strMsg.Format("M%02dCh%03d :: Range-3 Calibration 지령전송 성공", m_nModuleID, m_nChannelID);
		strMsg.Format("Module %02d :: Channel %02d Range-3 Calibration 지령전송 성공", m_nModuleID, m_nChannelID);
//		m_pView->WriteLog(strMsg, (BYTE)EP_LIST_COLOR_BLACK);
	}
	else
	{
		CString strMsg;
		strMsg.Format("Module %d Channel %d::Range-3의 Calibration 지령 전송에 실패했습니다."
			"\n%s",
			m_nModuleID, m_nChannelID, 
			CResponseCmdID::GetErrorMsg(::SFTGetLastError()));
		MessageBox(strMsg, "Calibration 실패", MB_ICONEXCLAMATION);
	}
}

void CCalResultDlg::OnBtnUpdate() 
{
	CWordArray aChannel;
	DWORD dwChannel[2];	
	ZeroMemory(dwChannel, sizeof(dwChannel));

	if( m_nChannelID < sizeof(DWORD)*8 )
		dwChannel[0] |= 0x00000001 << (m_nChannelID-1);
	else
		dwChannel[1] |= 0x00000001 << (m_nChannelID-1-(sizeof(DWORD)*8));
	
	long lChannelData = m_nChannelID;
	if( ::SFTSendCommand(m_nModuleID, 
						 CMD_CALI_UPDATE, (LPSFT_CH_SEL_DATA)dwChannel,
						(BYTE *)&lChannelData, 
						sizeof(long)) == false )
	{
		CString strMsg;
		strMsg.Format("Module %d Channel %d::Update 지령 전송에 실패했습니다."
			"\n%s",
			m_nModuleID, m_nChannelID, 
			CResponseCmdID::GetErrorMsg(::SFTGetLastError()));
		MessageBox(strMsg, "Calibration 실패", MB_ICONEXCLAMATION);
	}
	else
	{		
		CString strMsg;
		//strMsg.Format("M%02dCh%03d :: Apply Calibration Result 지령전송 성공", m_nModuleID, m_nChannelID);
		strMsg.Format("Module %02d :: Channel %02d Apply Calibration Result 지령전송 성공", m_nModuleID, m_nChannelID);
//		m_pView->WriteLog(strMsg, (BYTE)EP_LIST_COLOR_BLACK);
	}

	GetDlgItem(IDC_BTN_UPDATE)->EnableWindow(FALSE);
}

BOOL CCalResultDlg::DestroyWindow() 
{
	for( int nI = 0; nI < 3; nI++ )
		ClearList(nI);
	
	return CDialog::DestroyWindow();
}

void CCalResultDlg::SetReceiveResultData(LPSFT_CALI_END_DATA pReceivedCalData)
{
//	int nI;
//	CString strMsg;
//	BYTE byRange = pReceivedCalData->byRange;
//
//	if( byRange >= 3 || byRange < 0 )
//		goto EXIT_FUNC;
//
//	if( pReceivedCalData->byValidCalPointNum > m_pCalPoint[byRange].byValidPointNum ||
//		pReceivedCalData->byValidCalPointNum < 0 )
//		goto EXIT_FUNC;
//
//	if( pReceivedCalData->byValidCheckPointNum > m_pCheckPoint[byRange].byValidPointNum ||
//		pReceivedCalData->byValidCheckPointNum < 0 )
//		goto EXIT_FUNC;
//
//	for(int nI = 0; nI < pReceivedCalData->byValidCalPointNum; nI++ )
//	{
//		SetData(pReceivedCalData->byRange, 
//				nI, 
//				m_pCalPoint[pReceivedCalData->byRange].lPoint[nI],
//				pReceivedCalData->lCaliAD[nI], 
//				pReceivedCalData->lCaliDVM[nI]);
//	}
//
//	strMsg.Format("Range : %d▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒", pReceivedCalData->byRange+1);
//	m_wndCheckResult.AddString("");
//	m_wndCheckResult.AddString(strMsg, RGB(255,0,0));
//	m_wndCheckResult.SetTopIndex(m_wndCheckResult.GetCount()-1);
//	for(int nI = 0; nI < pReceivedCalData->byValidCheckPointNum; nI++ )
//	{
//		strMsg.Format("Point[%10s]%12s%12s", 
//			CConvertData::LongToFloat(m_pCheckPoint[pReceivedCalData->byRange].lPoint[nI], MULTI_LONG_2_FLOAT),
//			CConvertData::LongToFloat(pReceivedCalData->lCheckAD[nI], MULTI_LONG_2_FLOAT),
//			CConvertData::LongToFloat(pReceivedCalData->lCheckDVM[nI], MULTI_LONG_2_FLOAT));
//		m_wndCheckResult.AddString(strMsg);
//		m_wndCheckResult.SetTopIndex(m_wndCheckResult.GetCount()-1);
//	}
//
//EXIT_FUNC:
	//////////////////////////////
	// 반드시 삭제시켜야 한다.	//
//	delete pReceivedCalData;	//
	//////////////////////////////
}
