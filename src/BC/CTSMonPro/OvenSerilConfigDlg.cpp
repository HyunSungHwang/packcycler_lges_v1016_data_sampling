// OvenSerilConfigDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "OvenSerilConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COvenSerilConfigDlg dialog


COvenSerilConfigDlg::COvenSerilConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COvenSerilConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COvenSerilConfigDlg)
	m_nPortNum = -1;
	m_nBaudRate = -1;
	m_nDataBits = -1;
	m_nStopBits = -1;
	m_nParity = -1;
	//}}AFX_DATA_INIT
	m_ParityStr.Format("NOEMS");
}


void COvenSerilConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COvenSerilConfigDlg)
	DDX_Control(pDX, IDC_COMBO5, m_ctrlStopBits);
	DDX_Control(pDX, IDC_COMBO4, m_ctrlDataBits);
	DDX_Control(pDX, IDC_COMBO2, m_ctrlBaud);
	DDX_Control(pDX, IDC_COMBO1, m_ctrlPort);
	DDX_Control(pDX, IDC_COMBO3, m_ctrlParity);
	DDX_CBIndex(pDX, IDC_COMBO1, m_nPortNum);
	DDX_CBIndex(pDX, IDC_COMBO2, m_nBaudRate);
	DDX_CBIndex(pDX, IDC_COMBO4, m_nDataBits);
	DDX_CBIndex(pDX, IDC_COMBO5, m_nStopBits);
	DDX_CBIndex(pDX, IDC_COMBO3, m_nParity);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COvenSerilConfigDlg, CDialog)
	//{{AFX_MSG_MAP(COvenSerilConfigDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COvenSerilConfigDlg message handlers

void COvenSerilConfigDlg::OnOK() 
{
	UpdateData();
	CString strTemp;
	m_ctrlPort.GetLBText(m_ctrlPort.GetCurSel(), strTemp);
	m_SerialConfig.nPortNum			= atol(strTemp);

	m_ctrlBaud.GetLBText(m_ctrlBaud.GetCurSel(), strTemp);
	m_SerialConfig.nPortBaudRate	= atol(strTemp);

	m_ctrlDataBits.GetLBText(m_ctrlDataBits.GetCurSel(), strTemp);
	m_SerialConfig.nPortDataBits	= atol(strTemp);

	m_ctrlStopBits.GetLBText(m_ctrlStopBits.GetCurSel(), strTemp);
	m_SerialConfig.nPortStopBits	= atol(strTemp);

	
	m_SerialConfig.portParity		= m_ParityStr[m_nParity];
	m_SerialConfig.nPortBuffer		= 512;
	m_SerialConfig.nPortEvents		= EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
	
	SaveConfig();
	CDialog::OnOK();
}


BOOL COvenSerilConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	LoadConfig();

	m_nPortNum	=	m_SerialConfig.nPortNum - 1;
	switch(m_SerialConfig.nPortBaudRate)
	{
	case 115200:	m_nBaudRate= 0; break;
	case 57600:		m_nBaudRate= 1; break;
	case 38400:		m_nBaudRate= 2; break;
	case 19200:		m_nBaudRate= 3; break;
	case 9600:		m_nBaudRate= 4; break;
	case 4800:		m_nBaudRate= 5; break;
	default:		m_nBaudRate= 0; break;
	}

	m_nParity	=	m_ParityStr.Find(m_SerialConfig.portParity);
	
	switch(m_SerialConfig.nPortDataBits)
	{
	case 8:			m_nDataBits= 0; break;
	case 7:			m_nDataBits= 1; break;
	case 6:			m_nDataBits= 2; break;
	default:		m_nDataBits= 0; break;
	}

	switch(m_SerialConfig.nPortStopBits)
	{
	case 1:			m_nStopBits= 0; break;
	case 2:			m_nStopBits= 1; break;
	default:		m_nStopBits= 0; break;
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COvenSerilConfigDlg::LoadConfig()
{
	m_SerialConfig.nPortNum = AfxGetApp()->GetProfileInt(OVEN_SERIAL_CONFIG, "PortNo", 2);
	m_SerialConfig.nPortBaudRate = AfxGetApp()->GetProfileInt(OVEN_SERIAL_CONFIG, "BaudRate", 9600);
	m_SerialConfig.nPortDataBits = AfxGetApp()->GetProfileInt(OVEN_SERIAL_CONFIG, "DataBit", 8);
	m_SerialConfig.nPortStopBits = AfxGetApp()->GetProfileInt(OVEN_SERIAL_CONFIG, "StopBit", 1);
	m_SerialConfig.nPortBuffer = AfxGetApp()->GetProfileInt(OVEN_SERIAL_CONFIG, "Buffer", 512);
	m_SerialConfig.portParity = AfxGetApp()->GetProfileInt(OVEN_SERIAL_CONFIG, "Parity", 'N');
	m_SerialConfig.nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
}

void COvenSerilConfigDlg::SaveConfig()
{
	AfxGetApp()->WriteProfileInt(OVEN_SERIAL_CONFIG, "PortNo", m_SerialConfig.nPortNum);
	AfxGetApp()->WriteProfileInt(OVEN_SERIAL_CONFIG, "BaudRate", m_SerialConfig.nPortBaudRate);
	AfxGetApp()->WriteProfileInt(OVEN_SERIAL_CONFIG, "DataBit", m_SerialConfig.nPortDataBits);
	AfxGetApp()->WriteProfileInt(OVEN_SERIAL_CONFIG, "StopBit", m_SerialConfig.nPortStopBits);
	AfxGetApp()->WriteProfileInt(OVEN_SERIAL_CONFIG, "Buffer", m_SerialConfig.nPortBuffer);
	AfxGetApp()->WriteProfileInt(OVEN_SERIAL_CONFIG, "Parity", m_SerialConfig.portParity);
}
