// DlgConnectionSequence.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMonPro.h"
#include "DlgConnectionSequence.h"
#include "afxdialogex.h"


// CDlgConnectionSequence 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlgConnectionSequence, CDialogEx)

CDlgConnectionSequence::CDlgConnectionSequence(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlgConnectionSequence::IDD, pParent)
	, m_nTranspCount(0)
{

}

CDlgConnectionSequence::~CDlgConnectionSequence()
{
}

void CDlgConnectionSequence::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TEXT, m_labText);
	DDX_Control(pDX, IDC_LIST_LOG, m_listLog);
	DDX_Control(pDX, IDC_PROGRESS, m_ctrlProgress);
}


BEGIN_MESSAGE_MAP(CDlgConnectionSequence, CDialogEx)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDlgConnectionSequence 메시지 처리기입니다.


BOOL CDlgConnectionSequence::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	// Here we import the function from USER32.DLL
	HMODULE hUser32 = GetModuleHandle(_T("USER32.DLL"));
	m_pSetLayeredWindowAttributes = 
		(lpfnSetLayeredWindowAttributes)GetProcAddress(hUser32, 
		"SetLayeredWindowAttributes");


	// If the import did not succeed, make sure your app can handle it!
	if (NULL == m_pSetLayeredWindowAttributes)
		return FALSE; //Bail out!!!

	//If the function was imported correctly we must set the dialog we want to make transparent into "transparent-mode". 
	//E.G. Set the style for the dialog so that it can be transparent, and that is done with the flag WS_EX_LAYERED defined earlier.
	// Check the current state of the dialog, and then add the 
	// WS_EX_LAYERED attribute
	SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd, GWL_EXSTYLE)|WS_EX_LAYERED);


	SetBackgroundColor(RGB(255,255,255));
	m_labText.SetBkColor(RGB(255,255,255));	
	m_labText.SetFontSize(20);
	//m_labText.SetFontBold(TRUE);
	//m_labText.SetFontName("맑은 고딕");
	m_labText.SetFontName("Malgun Gothic");	
	m_ctrlProgress.SetRange(0,100);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

//ksj 20201209 : 상단 텍스트 메세지 변경
int CDlgConnectionSequence::SetText(CString strText)
{
	ResetTransparent();

	m_labText.SetText(strText);
	AddLog(strText);

	CenterWindow();

	return 0;
}

//ksj 20201209 : 하단 로그 리스트에 텍스트 추가
int CDlgConnectionSequence::AddLog(CString strLog)
{
	ResetTransparent();

	if ( m_listLog.GetCount() > 200 )
	{
		m_listLog.DeleteString(0);
	}

	CTime t = CTime::GetCurrentTime();
	CString strTemp;

	strTemp.Format("[%d-%02d-%02d %02d:%02d:%02d] %s",
		t.GetYear(),
		t.GetMonth(),
		t.GetDay(),
		t.GetHour(),
		t.GetMinute(),
		t.GetSecond(),
		strLog);

	m_listLog.AddString(strTemp);
	m_listLog.SetCurSel(m_listLog.GetCount()-1);

	CenterWindow();

	return 0;
}

//ksj 20201209 : 텍스트 깜빡임 (0: 안깜빡 1: 깜빡)
int CDlgConnectionSequence::SetFlashText(BOOL bActivate)
{
	m_labText.FlashText(bActivate);

	return 0;
}

//ksj 20201209 : 진행상태 프로그래스바 설정 (0~100%)
int CDlgConnectionSequence::SetPos(int nPos)
{
	ResetTransparent();
	m_ctrlProgress.SetPos(nPos);
	CenterWindow();

	if(nPos >= 100) //진행도가 100이되면 서서히 창 숨긴다.
	{
		m_nTranspCount = 100;
		SetTimer(100,100,NULL);
	}

	return 0;
}


void CDlgConnectionSequence::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	m_pSetLayeredWindowAttributes(m_hWnd, 0, 255*m_nTranspCount/100, LWA_ALPHA);

	if(m_nTranspCount <= 0)
	{
		KillTimer(100);
	}

	m_nTranspCount-=4;

	CDialogEx::OnTimer(nIDEvent);
}


void CDlgConnectionSequence::ResetTransparent()
{
	KillTimer(100);

	//투명도를 없앤다.
	if(m_pSetLayeredWindowAttributes)
		m_pSetLayeredWindowAttributes(m_hWnd, 0, 255, LWA_ALPHA);
}