// DetailOvenDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "DetailOvenDlg.h"
#include "MainFrm.h"
#include "WarningDlg.h"  // KHM

#include "Global.h"
#include "SetupOven.h"
//#include "SetupOven_V2.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDetailOvenDlg dialog


CDetailOvenDlg::CDetailOvenDlg(CCTSMonProDoc * pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CDetailOvenDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CDetailOvenDlg::IDD2):
	(CDetailOvenDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CDetailOvenDlg)
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	m_pWarringDlg = NULL;
    m_pCtrlOven=NULL;
	m_bChiller_DESChiller=FALSE; //lyj 20201207

	//---------------------------------
	m_nPortLine = pDoc->m_nOvenPortLine;
	pDoc->m_nOvenPortLine=0;
	//---------------------------------
}


void CDetailOvenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDetailOvenDlg)
	DDX_Control(pDX, IDC_STATIC_PUMP_MODE, m_StaticPumpMode);
	DDX_Control(pDX, IDC_STATIC_PUMP_SP, m_StaticPumpSP);
	DDX_Control(pDX, IDC_STATIC_PUMP_PV, m_StaticPumpPV);
	DDX_Control(pDX, IDC_STATIC_PUMP_COMM, m_StaticPumpComm);
	DDX_Control(pDX, IDC_LAB_LINE, m_CtrlLabLine);
	DDX_Control(pDX, IDC_UV_CHECK, m_CtrlUVCheck);
	DDX_Control(pDX, IDC_DOOR_OPEN, m_CtrlDoorCheck);
	DDX_Control(pDX, IDC_OTHER_ERROR, m_CtrlOtherCheck);
	DDX_Control(pDX, IDC_SMOKE, m_CtrlSmokeCheck);
	DDX_Control(pDX, IDC_STATIC_ChamberNumber, m_static_OvenNum);
	DDX_Control(pDX, IDC_BUTTON2, m_buttonOvenSelect);
	DDX_Control(pDX, IDC_LIST_OVEN, m_ctrlOvenList);
	//}}AFX_DATA_MAP
}

/************************************************************************/
/* Message_Map Begin                                                    */
/************************************************************************/

BEGIN_MESSAGE_MAP(CDetailOvenDlg, CDialog)
	//{{AFX_MSG_MAP(CDetailOvenDlg)
	ON_BN_CLICKED(IDC_BTN_ADD, OnBtnAdd)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BTN_OVEN_SETUP, OnBtnOvenSetup)
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUT_OVEN_TEST, OnButOvenTest)
	ON_BN_CLICKED(IDC_BUT_PUMP_RUN, OnButPumpRun)
	ON_BN_CLICKED(IDC_BUT_PUMP_STOP, OnButPumpStop)
	ON_BN_CLICKED(IDC_BUT_PUMP_AUTO, OnButPumpAuto)
	ON_BN_CLICKED(IDC_BUT_PUMP_MANUAL, OnButPumpManual)
	ON_BN_CLICKED(IDC_BUT_PUMP_SETSP1, OnButPumpSetsp1)
	ON_WM_WINDOWPOSCHANGED()
	ON_BN_CLICKED(IDC_BUT_OVEN_TEST2, OnButOvenTest2)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridLDoubleClicked)
	ON_MESSAGE(WM_GRID_CLICK, OnGridLClicked)
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDetailOvenDlg message handlers
/************************************************************************/
/* OnInitDialog()                                                       */
/************************************************************************/
BOOL CDetailOvenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	InitControl();

	CRect dlgRect;
	dlgRect.left = AfxGetApp()->GetProfileInt(OVEN_SETTING, "left", 500);
	dlgRect.top = AfxGetApp()->GetProfileInt(OVEN_SETTING, "top", 500);
	CRect winRect;
	this->GetClientRect(&winRect);

	dlgRect.right = dlgRect.left + winRect.Width();	
	//dlgRect.bottom = dlgRect.top + 220;	

	//yulee 20190807
	int nLanguage;
	nLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	if(nLanguage == 1)
	{
		dlgRect.bottom = dlgRect.top + 180;
	}
	else if(nLanguage == 2)
	{
		dlgRect.bottom = dlgRect.top + 180;
	}
#if _DEBUG //yulee 20191025
	if(nLanguage == 1)
	{
		dlgRect.bottom = dlgRect.top + 300;
	}
	else if(nLanguage == 2)
	{
		dlgRect.bottom = dlgRect.top + 300;
	}
#endif
	
	// ChamberId = 0 으로 초기화 (RS485 통신할때 챔버 ID로 활용)
	m_SelectChamberId = 0;				
	m_bSendDoorFlag = FALSE;
	
	if (m_nPortLine == 1) 
	{
		dlgRect.bottom += 150;		
		SetWinPos(dlgRect);
		m_pCtrlOven = &m_pDoc->m_ChamberCtrl.m_ctrlOven1;
	}
	else if (m_nPortLine == 2)
	{
		dlgRect.bottom += 150;		
		SetWinPos(dlgRect);
		m_pCtrlOven = &m_pDoc->m_ChamberCtrl.m_ctrlOven2;
	}
	else if (m_nPortLine == 7 ||
			 m_nPortLine == 8 ||
			 m_nPortLine == 11 ||
			 m_nPortLine == 12 ||
			 m_nPortLine == 13 ||
			 m_nPortLine == 14) 
	{
// #ifdef _DEBUG  //yulee 20190613
// 		dlgRect.bottom += 180;
// #else
// 		dlgRect.bottom += 130;
// #endif

// #ifdef _DEBUG  //yulee 20190613
// 		dlgRect.bottom += 280;
// #else
		dlgRect.bottom += 150;
/*#endif*/


		
		SetWinPos(dlgRect);

		int nItem=m_nPortLine-11;
		m_pCtrlOven = &m_pDoc->m_chillerCtrl[nItem].m_ctrlOven;
		GetDlgItem(IDC_BTN_OVEN_SETUP)->EnableWindow(FALSE);
	}
	//-------------------------------------------------------
	if(!m_pCtrlOven)
	{
		AfxMessageBox(_T("Check Oven Port Use"));
		return FALSE;
	}
	InitLedBtn();	
	InitOvenGrid();
	DrawOvenGrid1();
	DrawOvenGrid2();
	
	DisplayOvenGrid();
	
	// 챔버 DoorOpenSensor 사용유무 사용하면 챔버 Door OPEN 하면 충방전기 일시정지 실행
	m_bUseDoorOpenSensor = AfxGetApp()->GetProfileInt(OVEN_SETTING, "UseDoorOpenSensor", 0);
	
	SetTimer(542, 2000, NULL);	
	SetTimer(543, 1000, NULL);	

	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100 ||
		m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520) //yulee 20190613_1 //yulee 20190614_* //yulee 20191022
	{
		GetDlgItem(IDC_BUT_PUMP_MANUAL)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_PUMP_AUTO)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_PUMP_STOP)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_PUMP_RUN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_PUMP_SETSP1)->EnableWindow(FALSE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/************************************************************************/
/* OnLButtonDown()                                                      */
/************************************************************************/

void CDetailOvenDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);

	// CDialog::OnLButtonDown(nFlags, point);
}

void CDetailOvenDlg::InitLedBtn()
{
	m_CtrlUVCheck.SetImage(IDB_RED, 15);
	m_CtrlDoorCheck.SetImage(IDB_RED, 15);
	m_CtrlOtherCheck.SetImage(IDB_RED, 15);
	m_CtrlSmokeCheck.SetImage(IDB_RED, 15);

	m_CtrlUVCheck.Depress(FALSE);
	m_CtrlDoorCheck.Depress(FALSE);
	m_CtrlOtherCheck.Depress(FALSE);
	m_CtrlSmokeCheck.Depress(FALSE);	
}

/************************************************************************/
/* OnBtnOvenSetup()                                                     */
/************************************************************************/
void CDetailOvenDlg::OnBtnOvenSetup() 
{	
	//if (m_nPortLine == 1) m_pDoc->m_nOvenPortLine = 1;
	//else if (m_nPortLine == 2) m_pDoc->m_nOvenPortLine = 2;

	CSetupOven dlg(m_pDoc, &m_nPortLine);

	if(dlg.DoModal() == IDOK)
	{	
		//m_pDoc->CheckUsePort();		
		ResetLedBtn();
		//20090120 KHS
		if( m_bDetailOvenDlgChange )
		{		
			DrawOvenGrid1();
		}
		else
		{
			DrawOvenGrid2();
		}
		m_bUseDoorOpenSensor = dlg.m_bUseDoorOpenSensor;		// 챔버 DoorOpenSensor 사용유무
		DisplayOvenGrid();
		//UpdateList(TRUE);
	}	
}

/************************************************************************/
/* SetWinPos()                                                          */
/************************************************************************/

void CDetailOvenDlg::SetWinPos(CRect rect)
{
	::SetWindowPos(m_hWnd, HWND_NOTOPMOST, rect.left, rect.top, rect.Width(), rect.Height(), SWP_SHOWWINDOW);
}

/************************************************************************/
/* OnCancel()                                                           */
/************************************************************************/

void CDetailOvenDlg::OnCancel() 
{
	CRect dlgRect;
	this->GetClientRect(&dlgRect);
	ClientToScreen(&dlgRect);
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "left", dlgRect.left);
	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "top", dlgRect.top);

//	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
	
	CDialog::OnCancel();
}

/************************************************************************/
/* OnTimer()                                                            */
/************************************************************************/

void CDetailOvenDlg::OnTimer(UINT_PTR nIDEvent) 
{
	if(nIDEvent == 542)
	{
		DisplayOvenGrid();		
	}
	if(nIDEvent == 543)
	{
		onPumpDraw();
	}

	CDialog::OnTimer(nIDEvent);
}

/************************************************************************/
/* OnShowWindow()                                                       */
/************************************************************************/

void CDetailOvenDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{			
//	AfxGetApp()->WriteProfileInt("OvenSetting","ShowOvenDlg" , bShow);
	CDialog::OnShowWindow(bShow, nStatus);	
	
	/*BOOL bTimer = FALSE;
	for(int i = 0 ; m_pDoc->m_ctrlOven.GetOvenCount(); i++)
	{
		if(m_pDoc->m_ctrlOven.GetUseOven(i) == TRUE)		//Oven이 한개라도 사용중이면 Timer를 동작시킨다.
		{
			bTimer = TRUE;
			break;
		}
	}
	if(bTimer)		SetTimer(100, 2000, NULL);	*/
}

/************************************************************************/
/* InitOvenGrid()                                                       */
/************************************************************************/

void CDetailOvenDlg::InitOvenGrid()
{
	memset(buf, 0, 20 );	
	// ---------------------------------------------------
	// Grid 초기화
	m_wndOvenGrid1.SubclassDlgItem(IDC_OVEN_GRID, this);
	m_wndOvenGrid1.Initialize();

	m_wndOvenGrid2.SubclassDlgItem(IDC_OVEN_GRID2, this);
	m_wndOvenGrid2.Initialize();	
	// ------------------------------

	//sprintf(buf, "Line(%d) ChamberID : %d",m_nPortLine, m_SelectChamberId );
	sprintf(buf, "Line(%d)-%s",m_nPortLine, m_OvenLoc);
	m_CtrlLabLine.SetWindowText(buf);

	// ---------------------------
	// 오븐의 갯수를 가져와 초기화

   	if( m_pCtrlOven->GetOvenCount() < 2)
	{
		m_bDetailOvenDlgChange = TRUE;
		m_static_OvenNum.ShowWindow(TRUE);
		m_static_OvenNum.SetWindowText(buf);
		m_wndOvenGrid1.ShowWindow(SW_SHOW);
		m_wndOvenGrid2.ShowWindow(SW_HIDE);
		m_buttonOvenSelect.SetWindowText(Fun_FindMsg("InitOvenGrid_msg1","IDD_DETAIL_OVEN_DLG"));		
		//@ m_buttonOvenSelect.SetWindowText("상위");		
	}
	else
	{
		m_bDetailOvenDlgChange = FALSE;
		m_static_OvenNum.ShowWindow(FALSE);
		m_wndOvenGrid1.ShowWindow(SW_HIDE);
		m_wndOvenGrid2.ShowWindow(SW_SHOW);
		m_buttonOvenSelect.SetWindowText(Fun_FindMsg("InitOvenGrid_msg2","IDD_DETAIL_OVEN_DLG"));		
		//@ m_buttonOvenSelect.SetWindowText("선택");		
	}	
	m_wndOvenGrid1.Redraw();
	m_wndOvenGrid2.Redraw();
}

/************************************************************************/
/* DrawOvenGrid1()                                                      */
/************************************************************************/
void CDetailOvenDlg::DrawOvenGrid1()
{
	// ---------------------------------
	// Grid1 setting 첫번째 그리드 셋팅
	// ---------------------------------
	CRect rect;
	m_wndOvenGrid1.GetParam()->EnableUndo(FALSE);

	//20090120 KHS TEMP880일 경우 습도 삭제
	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP880  //yulee 20190614_*
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP2500 
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1000
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_ST590)
	{
		m_wndOvenGrid1.SetRowCount(2);
		m_wndOvenGrid1.SetColCount(2);
		m_wndOvenGrid1.GetClientRect(&rect);
		m_wndOvenGrid1.SetColWidth(0, 0, 0);
		m_wndOvenGrid1.SetColWidth(1, 1, 143);
		m_wndOvenGrid1.SetColWidth(2, 2, 75);
		m_wndOvenGrid1.SetRowHeight(0,0,0);
		m_wndOvenGrid1.SetRowHeight(1,2,37);
		m_wndOvenGrid1.SetRowHeight(2,2,37);
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	else if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TD500)
	{
		m_wndOvenGrid1.SetRowCount(2);
		m_wndOvenGrid1.SetColCount(2);
		m_wndOvenGrid1.GetClientRect(&rect);
		m_wndOvenGrid1.SetColWidth(0, 0, 0);
		m_wndOvenGrid1.SetColWidth(1, 1, 143);
		m_wndOvenGrid1.SetColWidth(2, 2, 75);
		m_wndOvenGrid1.SetRowHeight(0,0,0);
		m_wndOvenGrid1.SetRowHeight(1,2,35);
		m_wndOvenGrid1.SetRowHeight(2,2,35);
	}
	// -
	//////////////////////////////////////////////////////////////////////////
	else
	{
		m_wndOvenGrid1.SetRowCount(4);
		m_wndOvenGrid1.SetColCount(2);
		m_wndOvenGrid1.GetClientRect(&rect);
		m_wndOvenGrid1.SetColWidth(0, 0, 0);
		m_wndOvenGrid1.SetColWidth(1, 1, 143);
		m_wndOvenGrid1.SetColWidth(2, 2, 75);
		m_wndOvenGrid1.SetRowHeight(0,0,0);
		m_wndOvenGrid1.SetRowHeight(1,2,19);
		m_wndOvenGrid1.SetRowHeight(2,2,19);
	}
	
	m_wndOvenGrid1.GetParam()->EnableTrackColWidth(GX_TRACK_DISABLE);	//Resize Col Width
	m_wndOvenGrid1.GetParam()->EnableSelection(GX_SELNONE);
	m_wndOvenGrid1.GetParam()->EnableUndo(TRUE);
	
	//Static Ctrl/////////////////////////////////////
	m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(0), CGXStyle().SetHorizontalAlignment(DT_CENTER) );
	///////////////////////////////////////////////////

	//Edit Ctrl////////////////////////////////////////
	//20090120 KHS TEMP880일 경우 습도 삭제
	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP880  //yulee 20190614_*
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP2500 
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1000
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_ST590)
	{
		m_wndOvenGrid1.SetCoveredCellsRowCol(1,1,2,1);
		//m_wndOvenGrid1.SetValueRange(CGXRange(1,2), Fun_FindMsg("DrawOvenGrid_msg1"));
		m_wndOvenGrid1.SetValueRange(CGXRange(1,2), Fun_FindMsg("DrawOvenGrid1_msg1","IDD_DETAIL_OVEN_DLG")); //ksj 20180718 : 오타수정.
		//@ m_wndOvenGrid1.SetValueRange(CGXRange(1,2), "온도SP");

		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 2),
			CGXStyle().SetInterior(RGB(98,255,98)));
		m_wndOvenGrid1.SetStyleRange(CGXRange(2, 2, 2, 2),
			CGXStyle().SetInterior(RGB(255,255,255)));
		
		m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1, 2), CGXStyle().SetControl(GX_IDS_CTRL_STATIC) );
		m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1),CGXStyle().SetFont(CGXFont().SetSize(23).SetBold(TRUE)));
		//	m_wndOvenGrid1.SetStyleRange(CGXRange().SetRows(3,1),	CGXStyle().SetFont(CGXFont().SetSize(15)));
		m_wndOvenGrid1.SetCurrentCell(0,0);
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26 
	else if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TD500)
	{
		m_wndOvenGrid1.SetCoveredCellsRowCol(1,1,2,1);

		m_wndOvenGrid1.SetValueRange(CGXRange(1,2), Fun_FindMsg("DrawOvenGrid1_msg2","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid1.SetValueRange(CGXRange(1,2), "온도SP");

		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 2),
			CGXStyle().SetInterior(RGB(98,255,98)));
		m_wndOvenGrid1.SetStyleRange(CGXRange(2, 2, 2, 2),
			CGXStyle().SetInterior(RGB(255,255,255)));
		
		m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1, 2), CGXStyle().SetControl(GX_IDS_CTRL_STATIC) );
		m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1),CGXStyle().SetFont(CGXFont().SetSize(25).SetBold(TRUE)));
		//	m_wndOvenGrid1.SetStyleRange(CGXRange().SetRows(3,1),	CGXStyle().SetFont(CGXFont().SetSize(15)));
		m_wndOvenGrid1.SetCurrentCell(0,0);
	}
	// - 
	//////////////////////////////////////////////////////////////////////////
	else
	{
		m_wndOvenGrid1.SetCoveredCellsRowCol(1,1,2,1);
		m_wndOvenGrid1.SetCoveredCellsRowCol(3,1,4,1);	

		m_wndOvenGrid1.SetValueRange(CGXRange(1,2), Fun_FindMsg("DrawOvenGrid1_msg3","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid1.SetValueRange(CGXRange(1,2), "온도SP");
		m_wndOvenGrid1.SetValueRange(CGXRange(3,2), Fun_FindMsg("DrawOvenGrid1_msg4","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid1.SetValueRange(CGXRange(3,2), "습도SP");

		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 2),
						CGXStyle().SetInterior(RGB(98,255,98)));
		m_wndOvenGrid1.SetStyleRange(CGXRange(2, 2, 2, 2),
						CGXStyle().SetInterior(RGB(255,255,255)));
		m_wndOvenGrid1.SetStyleRange(CGXRange(3, 1, 4, 2),
						CGXStyle().SetInterior(RGB(255,255,98)));
		m_wndOvenGrid1.SetStyleRange(CGXRange(4, 2, 4, 2),
						CGXStyle().SetInterior(RGB(255,255,255)));

		m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1, 2), CGXStyle().SetControl(GX_IDS_CTRL_STATIC) );
		m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1),CGXStyle().SetFont(CGXFont().SetSize(15).SetBold(TRUE)));
//		m_wndOvenGrid1.SetStyleRange(CGXRange().SetRows(3,1),	CGXStyle().SetFont(CGXFont().SetSize(15)));
		m_wndOvenGrid1.SetCurrentCell(0,0);
	}

	m_wndOvenGrid1.SetScrollBarMode(SB_BOTH, gxnDisabled);
	m_wndOvenGrid1.SetAutoScroll(FALSE);
}

void CDetailOvenDlg::DrawOvenGrid2()
{
	// --------------------------------
	// Grid2 setting 두번째 그리드 셋팅
	// --------------------------------
	int		roop_i=0;
	memset(buf, 0, 20 );	
	CRect	rect;

	int		TotalOvenCount = m_pCtrlOven->GetOvenCount();		// OvenCount setting

	m_wndOvenGrid2.GetParam()->EnableUndo(TRUE);	
	m_wndOvenGrid2.GetClientRect(&rect);

	//20090120 KHS TEMP880일 경우 습도 삭제
	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP880 //yulee 20190614_*
		||m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP2500
		||m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1000
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_ST590)
	{
		m_wndOvenGrid2.SetRowCount(1 + TotalOvenCount);		// Menu:2 + OvenCount
		m_wndOvenGrid2.SetColCount(3);						// Menu:1 + OvenCount	

		// ---------------------------
		// 가로
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_NO, _COL_GRID2_NO, 25);
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_TEMPER_PV_, _COL_GRID2_TEMPER_SV_, 46);
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_HUMID_PV, _COL_GRID2_HUMID_PV, 92);
//		m_wndOvenGrid2.SetRowHeight(0,0,0);			// default
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	else if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TD500)
	{
		m_wndOvenGrid2.SetRowCount(1 + TotalOvenCount);		// Menu:2 + OvenCount
		m_wndOvenGrid2.SetColCount(3);						// Menu:1 + OvenCount	
		
		// ---------------------------
		// 가로
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_NO, _COL_GRID2_NO, 25);
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_TEMPER_PV_, _COL_GRID2_TEMPER_SV_, 46);
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_HUMID_PV, _COL_GRID2_HUMID_PV, 92);
	}
	// -
	//////////////////////////////////////////////////////////////////////////
	else
	{
		m_wndOvenGrid2.SetRowCount(1 + TotalOvenCount);		// Menu:2 + OvenCount
		m_wndOvenGrid2.SetColCount(4);						// Menu:1 + OvenCount	
		
		// ---------------------------
		// 가로
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_NO, _COL_GRID2_NO, 25);
		m_wndOvenGrid2.SetColWidth(_COL_GRID2_TEMPER_PV_, _COL_GRID2_HUMID_SV, 46);
//		m_wndOvenGrid2.SetRowHeight(0,0,0);			// default
	}
	
	m_wndOvenGrid2.GetParam()->EnableTrackColWidth(GX_TRACK_DISABLE);	//Resize Col Width
	m_wndOvenGrid2.GetParam()->EnableTrackRowHeight(GX_TRACK_DISABLE);
	m_wndOvenGrid2.GetParam()->EnableSelection(GX_SELNONE);
	m_wndOvenGrid2.GetParam()->EnableUndo(TRUE);


	//Static Ctrl/////////////////////////////////////
	m_wndOvenGrid2.SetStyleRange(CGXRange().SetCols(0), CGXStyle().SetHorizontalAlignment(DT_CENTER) );
	///////////////////////////////////////////////////
	
	//Edit Ctrl////////////////////////////////////////	
	//20090120 KHS TEMP880일 경우 습도 삭제

	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP880 //yulee 20190614_*
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP2500
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1000
		|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_ST590)
	{
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_NO,1,_COL_GRID2_NO);
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_TEMPER_PV_,0,_COL_GRID2_TEMPER_SV_);
		m_wndOvenGrid2.SetCoveredCellsRowCol(0, 3, 1, 3);
		
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_NO), "NO");
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_TEMPER_PV_), Fun_FindMsg("DrawOvenGrid2_msg1","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_TEMPER_PV_), "온도");
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_HUMID_PV), Fun_FindMsg("DrawOvenGrid2_msg2","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_HUMID_PV), "통신상태");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_TEMPER_PV_), "PV");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_TEMPER_SV_), "SP");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_HUMID_PV), Fun_FindMsg("DrawOvenGrid2_msg3","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_HUMID_PV), "통신상태");
	}
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	else if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TD500)
	{
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_NO,1,_COL_GRID2_NO);
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_TEMPER_PV_,0,_COL_GRID2_TEMPER_SV_);
		m_wndOvenGrid2.SetCoveredCellsRowCol(0, 3, 1, 3);
		
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_NO), "NO");
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_TEMPER_PV_), Fun_FindMsg("DrawOvenGrid2_msg4","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_TEMPER_PV_), "온도");
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_HUMID_PV), Fun_FindMsg("DrawOvenGrid2_msg5","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_HUMID_PV), "통신상태");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_TEMPER_PV_), "PV");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_TEMPER_SV_), "SP");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_HUMID_PV), Fun_FindMsg("DrawOvenGrid2_msg6","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_HUMID_PV), "통신상태");
	}
	// -
	//////////////////////////////////////////////////////////////////////////
	else
	{
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_NO,1,_COL_GRID2_NO);
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_TEMPER_PV_,0,_COL_GRID2_TEMPER_SV_);
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_HUMID_PV,0,_COL_GRID2_HUMID_PV);
		m_wndOvenGrid2.SetCoveredCellsRowCol(0,_COL_GRID2_HUMID_PV,0,_COL_GRID2_HUMID_SV);
		
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_NO), "NO");
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_TEMPER_PV_), Fun_FindMsg("DrawOvenGrid2_msg7","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_TEMPER_PV_), "온도");
		m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_HUMID_PV), Fun_FindMsg("DrawOvenGrid2_msg8","IDD_DETAIL_OVEN_DLG"));
		//@ m_wndOvenGrid2.SetValueRange(CGXRange(0,_COL_GRID2_HUMID_PV), "습도");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_TEMPER_PV_), "PV");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_TEMPER_SV_), "SP");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_HUMID_PV), "PV");
		m_wndOvenGrid2.SetValueRange(CGXRange(1,_COL_GRID2_HUMID_SV), "SP");
	}

	// ----------------------------
	// Cell 	
	for( roop_i=1; roop_i < TotalOvenCount+1; roop_i++ )
	{
		sprintf(buf, "%d", roop_i );
		m_wndOvenGrid2.SetValueRange(CGXRange(roop_i+1, 0), buf);
	}
	// -----------------------------
	// Cell color setting
	// first cell
//	m_wndOvenGrid2.SetStyleRange(CGXRange(0, _COL_GRID2_NO, 1+TotalOvenCount, _COL_GRID2_NO), CGXStyle().SetInterior(RGB(200,200,200)));
	// second cell
//	m_wndOvenGrid2.SetStyleRange(CGXRange(0, _COL_GRID2_TEMPER_PV_, 1+TotalOvenCount, _COL_GRID2_TEMPER_SV_), CGXStyle().SetInterior(RGB(98,255,98)));		
	// third cell
//	m_wndOvenGrid2.SetStyleRange(CGXRange(0, _COL_GRID2_HUMID_PV, 1+TotalOvenCount, _COL_GRID2_HUMID_SV), CGXStyle().SetInterior(RGB(255,255,98)));

	// m_wndOvenGrid2.SetStyleRange(CGXRange().SetRows(1,4), CGXStyle().SetControl(GX_IDS_CTRL_STATIC));	
//	m_wndOvenGrid2.SetStyleRange(CGXRange().SetCols(1),CGXStyle().SetFont(CGXFont().SetSize(13).SetBold(TRUE)));
	m_wndOvenGrid2.SetCurrentCell(0,0);	
	m_wndOvenGrid2.SetScrollBarMode(SB_BOTH, gxnDisabled);
	m_wndOvenGrid2.SetAutoScroll(FALSE);	
}

/************************************************************************/
/* InsertDataOvenGrid1()                                                */
// 선택된 하나의 그리드를 출력
/************************************************************************/
void CDetailOvenDlg::InsertDataOvenGrid1()
{
	CString strMode;		//ljb 20101227
	CString strOvenItem;	
	float fSetValue,fCurValue, fSetValueHumi, fCurValueHumi;
	fSetValue = m_pCtrlOven->GetRefTemperature( m_SelectChamberId );
	fCurValue = m_pCtrlOven->GetCurTemperature( m_SelectChamberId );
	fSetValueHumi = m_pCtrlOven->GetRefHumidity( m_SelectChamberId );//yulee 20190620
	fCurValueHumi = m_pCtrlOven->GetCurHumidity( m_SelectChamberId );//yulee 20190620


	if(m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520)
	{
		if (m_pCtrlOven->GetRun(0))
		{
			GetDlgItem(IDC_STATIC_RUN_STATE)->SetWindowText("RUN");
		}
		else
		{
			GetDlgItem(IDC_STATIC_RUN_STATE)->SetWindowText("STOP");
		}
	}
	else
	{
		if (m_pCtrlOven->GetRun(0))
		{
			GetDlgItem(IDC_STATIC_RUN_STATE)->SetWindowText("RUN");
		}
		else
		{
			GetDlgItem(IDC_STATIC_RUN_STATE)->SetWindowText("STOP");
		}
	}



	if(m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520)
	{
		int nTempMode, nPumpMode; //0: Fix mode, 1: Prog Mode
		nTempMode = 0;
		nPumpMode = 0;

		nTempMode = m_pCtrlOven->GetRunWorkMode(0);
		nPumpMode = m_pCtrlOven->GetRunPumpWorkMode(0);

		if((nTempMode == 1)&&(nPumpMode)==1)
		{
			GetDlgItem(IDC_STATIC_MODE)->SetWindowText("Temp(Fix), Pump(Fix)");
		}
		else if((nTempMode == 1)&&(nPumpMode)==0)
		{
			GetDlgItem(IDC_STATIC_MODE)->SetWindowText("Temp(Fix), Pump(Prog)");
		}
		else if((nTempMode == 0)&&(nPumpMode)==1)
		{
			GetDlgItem(IDC_STATIC_MODE)->SetWindowText("Temp(Prog), Pump(Fix)");
		}
		else if((nTempMode == 0)&&(nPumpMode)==0)
		{
			GetDlgItem(IDC_STATIC_MODE)->SetWindowText("Temp(Fix), Pump(Fix)");
		}
	}
	else
	{
		if (m_pCtrlOven->GetRunWorkMode(0))		// FALSE = PROG 모드, TRUE = FIX 모드
		{
			GetDlgItem(IDC_STATIC_MODE)->SetWindowText("Fix Mode");
		}
		else
		{
			GetDlgItem(IDC_STATIC_MODE)->SetWindowText("Prog Mode");
		}
	}


	//20090120 KHS 현재온도에 따른 Grid 색상 변화////////////////////////////////
	/*if(fCurValue >= 30.0f)		//Red
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(RGB(255,0,0)));
	}
	else if(fCurValue < 30.0f && fCurValue >= 15.0f)		//Orange
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(RGB(255,128,0)));
	}
	else if(fCurValue < 15.0f && fCurValue >= 0.0f)		//Bluesky
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(RGB(128,255,255)));
	}
	else if(fCurValue < 0.0f)		//Blue
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(RGB(0,0,255)));
	}*/

	//ksj 20200703 : 조건 및 색상 옵션 처리
	float fTempRange1 = (float)AfxGetApp()->GetProfileInt(OVEN_SETTING, "TempRange1", 30);
	float fTempRange2 = (float)AfxGetApp()->GetProfileInt(OVEN_SETTING, "TempRange2", 15);
	float fTempRange3 = (float)AfxGetApp()->GetProfileInt(OVEN_SETTING, "TempRange3", 0);

	CString strTempColor1 = AfxGetApp()->GetProfileString(OVEN_SETTING, "TempColor1", "255,0,0"); //기본값 red //lg는 초록색 요청됨. (0,255,0)
	CString strTempColor1_2 = AfxGetApp()->GetProfileString(OVEN_SETTING, "TempColor1-2", "255,128,0"); //기본 값 orange //lg는 연두색 요청됨 (187,255,187)
	CString strTempColor2_3 = AfxGetApp()->GetProfileString(OVEN_SETTING, "TempColor2-3", "128,255,255"); //기본 값 bluesky
	CString strTempColor3 = AfxGetApp()->GetProfileString(OVEN_SETTING, "TempColor3", "0,0,255"); //기본 값 blue
		
	COLORREF dwTempColor1 = ConvertRGBStrToCOLORREF(strTempColor1);
	COLORREF dwTempColor1_2 = ConvertRGBStrToCOLORREF(strTempColor1_2);
	COLORREF dwTempColor2_3 = ConvertRGBStrToCOLORREF(strTempColor2_3);
	COLORREF dwTempColor3 = ConvertRGBStrToCOLORREF(strTempColor3);


	if(fCurValue >= fTempRange1)		//Red
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(dwTempColor1));
	}
	else if(fCurValue < fTempRange1 && fCurValue >= fTempRange2)		//Orange
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(dwTempColor1_2));
	}
	else if(fCurValue < fTempRange2 && fCurValue >= fTempRange3)		//Bluesky
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(dwTempColor2_3));
	}
	else if(fCurValue < fTempRange3)		//Blue
	{
		m_wndOvenGrid1.SetStyleRange(CGXRange(1, 1, 2, 1),
			CGXStyle().SetInterior(dwTempColor3));
	}
	/////////////////////////////////////////////////////////////////////////////

	strOvenItem.Format("%.1f", fCurValue);	//strOvenItem.Format("%.1f℃", fCurValue);
	m_wndOvenGrid1.SetValueRange(CGXRange(1,1), strOvenItem);

	strOvenItem.Format("%.1f", fSetValue);	//strOvenItem.Format("%.1f℃", fSetValue);
	m_wndOvenGrid1.SetValueRange(CGXRange(2,2), strOvenItem);
	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMI880||
		m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1200)
	{
		strOvenItem.Format("%.1f％", fCurValueHumi);
		m_wndOvenGrid1.SetValueRange(CGXRange(3,1), strOvenItem);

		strOvenItem.Format("%.1f％", fSetValueHumi);
		m_wndOvenGrid1.SetValueRange(CGXRange(4,2), strOvenItem);
	}


	
	//20090120 KHS TEMP880일 경우 습도 삭제
	//////////////////////////////////////////////////////////////////////////
	// + BW KIM 2014.02.26
	//if(m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP880 && m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP2500)
	if(m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP880 
		&& m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP2500 
		&& m_pCtrlOven->m_nOvenModelType != CHAMBER_TD500
		&& m_pCtrlOven->m_nOvenModelType != CHAMBER_MODBUS_NEX1000
		&& m_pCtrlOven->m_nOvenModelType != CHAMBER_MODBUS_NEX1100
		&& m_pCtrlOven->m_nOvenModelType != CHILLER_TEMP2520
		&& m_pCtrlOven->m_nOvenModelType != CHILLER_ST590)
	// -
	//////////////////////////////////////////////////////////////////////////
	{
		fSetValue = m_pCtrlOven->GetRefHumidity( m_SelectChamberId );
		fCurValue = m_pCtrlOven->GetCurHumidity( m_SelectChamberId );		
		
		if(fCurValue > 105.0f)
			strOvenItem.Format("--.--", fCurValue);	//strOvenItem.Format("--.-％", fCurValue);
		else
			strOvenItem.Format("%.1f", fCurValue);	//strOvenItem.Format("%.1f％", fCurValue);
		m_wndOvenGrid1.SetValueRange(CGXRange(3,1), strOvenItem);
		
		strOvenItem.Format("%.1f", fSetValue);	//strOvenItem.Format("%.1f％", fSetValue);
		m_wndOvenGrid1.SetValueRange(CGXRange(4,2), strOvenItem);
	}

	if(m_pCtrlOven->GetLineState( m_SelectChamberId ))
	{
		if(m_pCtrlOven->GetRun( m_SelectChamberId ))	{
			strOvenItem.Format(Fun_FindMsg("InsertDataOvenGrid1_msg1","IDD_DETAIL_OVEN_DLG"));
			//@ strOvenItem.Format("동작중");
		}
		else {
			strOvenItem.Format(Fun_FindMsg("InsertDataOvenGrid1_msg2","IDD_DETAIL_OVEN_DLG"));
			//m_wndOvenGrid1.SetValueRange(CGXRange(1,1), strOvenItem);
			//@ strOvenItem.Format("대기");
		}
		m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1),CGXStyle().SetFont(CGXFont().SetSize(23).SetBold(TRUE)));
	}
	else
	{
		strOvenItem = Fun_FindMsg("InsertDataOvenGrid1_msg3","IDD_DETAIL_OVEN_DLG");
		//@ strOvenItem = "통신두절";
		m_wndOvenGrid1.SetValueRange(CGXRange(1,1), strOvenItem);
		if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMI880 
			|| m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP2500
			|| m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP2500 
			|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1000
			|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100
			|| m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1200
			|| m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520)
		{
			strOvenItem.Format(_T("%.1f(%s)"), fCurValue,Fun_FindMsg("InsertDataOvenGrid1_msg3","IDD_DETAIL_OVEN_DLG"));
			m_wndOvenGrid1.SetValueRange(CGXRange(1,1), strOvenItem);
			m_wndOvenGrid1.SetStyleRange(CGXRange().SetCols(1),CGXStyle().SetFont(CGXFont().SetSize(13).SetBold(TRUE)));
		}
	}
}

/************************************************************************/
/* InsertDataOvenGrid2()                                                */
/************************************************************************/

void CDetailOvenDlg::InsertDataOvenGrid2()
{	
	CString strOvenItem2;

	// --------------------------------
	// 오븐의 갯수만큼 그리드를 늘린다.	
	for( int i=0; i<m_pCtrlOven->GetOvenCount(); i++ )
	{
		if (m_pCtrlOven->GetUseOven(i) == FALSE) continue;
		float fSetValue2 = m_pCtrlOven->GetRefTemperature( i );
		float fCurValue2 = m_pCtrlOven->GetCurTemperature( i );		
		
		strOvenItem2.Format("%.1f", fCurValue2);	//strOvenItem2.Format("%.1f℃", fCurValue2);
		m_wndOvenGrid2.SetValueRange(CGXRange(_ROW_GRID2_ITEM_START_+i,_COL_GRID2_TEMPER_PV_), strOvenItem2);
		
		strOvenItem2.Format("%.1f", fSetValue2);	//strOvenItem2.Format("%.1f℃", fSetValue2);
		m_wndOvenGrid2.SetValueRange(CGXRange(_ROW_GRID2_ITEM_START_+i,_COL_GRID2_TEMPER_SV_), strOvenItem2);
		
		//20090120 KHS TEMP880일 경우 습도 삭제
		//////////////////////////////////////////////////////////////////////////
		// + BW KIM 2014.02.26
		//if(m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP880 && m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP2500)
		if(m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP880 
			&& m_pCtrlOven->m_nOvenModelType != CHAMBER_TEMP2500 
			&& m_pCtrlOven->m_nOvenModelType != CHAMBER_TD500
			&& m_pCtrlOven->m_nOvenModelType != CHAMBER_MODBUS_NEX1000
			&& m_pCtrlOven->m_nOvenModelType != CHAMBER_MODBUS_NEX1100
			&& m_pCtrlOven->m_nOvenModelType != CHILLER_TEMP2520
			&& m_pCtrlOven->m_nOvenModelType != CHILLER_ST590)
		// -
		//////////////////////////////////////////////////////////////////////////
		{
			fSetValue2 = m_pCtrlOven->GetRefHumidity(i);
			fCurValue2 = m_pCtrlOven->GetCurHumidity(i);		
			
			if(fCurValue2 > 105.0f)
				strOvenItem2.Format("--.--", fCurValue2);	//strOvenItem2.Format("--.-％", fCurValue2);
			else
				strOvenItem2.Format("%.1f", fCurValue2);	//strOvenItem2.Format("%.1f％", fCurValue2);
			m_wndOvenGrid2.SetValueRange(CGXRange(_ROW_GRID2_ITEM_START_+i,_COL_GRID2_HUMID_PV), strOvenItem2);
			
			strOvenItem2.Format("%.1f", fSetValue2);	//strOvenItem2.Format("%.1f％", fSetValue2);
			m_wndOvenGrid2.SetValueRange(CGXRange(_ROW_GRID2_ITEM_START_+i,_COL_GRID2_HUMID_SV), strOvenItem2);
		}
		
		if(m_pCtrlOven->GetLineState(i))
		{
			if(m_pCtrlOven->GetRun(i))
			{
				strOvenItem2.Format(Fun_FindMsg("InsertDataOvenGrid2_msg1","IDD_DETAIL_OVEN_DLG"));
				//@ strOvenItem2.Format("동작중");
			}
			else
			{
				strOvenItem2.Format(Fun_FindMsg("InsertDataOvenGrid2_msg2","IDD_DETAIL_OVEN_DLG"));
				//@ strOvenItem2.Format("대기");
			}
		}
		else
		{
			strOvenItem2 = Fun_FindMsg("InsertDataOvenGrid2_msg3","IDD_DETAIL_OVEN_DLG");
			//@ strOvenItem2 = "통신두절";
		}
		//20090120 KHS TEMP880일 경우 통신상태 표시
		if(m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP880 || m_pCtrlOven->m_nOvenModelType == CHAMBER_TEMP2500)
		{
			//m_wndOvenGrid2.SetValueRange(CGXRange(_ROW_GRID2_ITEM_START_+i,_COL_GRID2_HUMID_PV), strOvenItem2);	//ljb 20120627 주석처리
		}
	}	
}

/************************************************************************/
/* DisplayOvenGrid()                                                    */
/************************************************************************/

CString CDetailOvenDlg::onGetDeviceName()
{
	CString strTemp;

	if( m_nPortLine==11 ||
		m_nPortLine==12 ||
		m_nPortLine==13 ||
		m_nPortLine==14)
	{
		strTemp.Format(_T("Chiller : %d"),m_SelectChamberId +1);
	}
	else
	{
		strTemp.Format(_T("ChamberID : %d"),m_SelectChamberId +1);
	}
	return strTemp;
}

void CDetailOvenDlg::DisplayOvenGrid()
{
	memset(buf, 0, 20 );		
	// ------------------------------------------------------------------------
	// m_SelectChamberId 의 값에 따라 0,1이면 단일, 2이상 이면 복수 그리드 표시
	// ------------------------------------------------------------------------	
	if( m_bDetailOvenDlgChange )
	{
		m_wndOvenGrid2.ShowWindow(FALSE);
		m_wndOvenGrid1.ShowWindow(TRUE);	

		m_static_OvenNum.ShowWindow(TRUE);
		m_static_OvenNum.SetWindowText(onGetDeviceName());

		m_buttonOvenSelect.ShowWindow(TRUE);
		m_buttonOvenSelect.SetWindowText(Fun_FindMsg("DisplayOvenGrid_msg2","IDD_DETAIL_OVEN_DLG"));
		//@ m_buttonOvenSelect.SetWindowText("상위");
		
		InsertDataOvenGrid1();
	}
	else
	{
		m_wndOvenGrid1.ShowWindow(FALSE);
		m_wndOvenGrid2.ShowWindow(TRUE);
		
		m_static_OvenNum.ShowWindow(FALSE);
		m_buttonOvenSelect.ShowWindow(TRUE);
		m_buttonOvenSelect.SetWindowText(Fun_FindMsg("DisplayOvenGrid_msg1","IDD_DETAIL_OVEN_DLG"));
		//@ m_buttonOvenSelect.SetWindowText("선택");
		
		InsertDataOvenGrid2();
	}

	//DrawOvenStatus();

	m_wndOvenGrid2.Redraw();
}

void CDetailOvenDlg::DrawOvenStatus()
{
	// for DrawOvenStatus
	CString strMessage,strTemp;
	long OvenStatus;	//★★ ljb 2010325  ★★★★★★★★
	for( int iCnt=0; iCnt<m_pCtrlOven->GetOvenCount(); iCnt++ )
	{
		strMessage.Empty();
		OvenStatus =  m_pCtrlOven->GetOvenStatus( iCnt );
		if (m_pCtrlOven->GetUseOven( iCnt ) == FALSE) continue;

		for(int i=0; i<8; i++ )
		{
			if (i == 0 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage="";}
			else m_CtrlOtherCheck.Depress(FALSE);

			if (i == 1 && ((OvenStatus>>i) & 0x00000001))
			{
				strMessage += " Door Open.";
				m_CtrlDoorCheck.Depress(TRUE);			// OvenDoor, Open, Set 일시 채널 멈춤 실행
				if( m_bUseDoorOpenSensor == TRUE )
				{						
					if( m_bSendDoorFlag == FALSE )
					{
						CCyclerChannel *pChInfo;
						for(int i = 0 ; i < m_pCtrlOven->GetOvenCount(); i++)
						{
							CDWordArray adwCh;
							m_pCtrlOven->GetChannelMapArray(i, adwCh);			
							for(int index =0; index < adwCh.GetSize(); index++)
							{
								DWORD dwData = adwCh.GetAt(index);
								
								CWordArray OneChArray;
								OneChArray.Add(dwData);
								
								pChInfo = m_pDoc->GetChannelInfo(HIWORD(dwData), LOWORD(dwData));
								if(pChInfo)
								{
									m_pDoc->SendCommand(HIWORD(dwData), &OneChArray, SFT_CMD_PAUSE);	// kky for 일시정지의 경우 Cycle, Step 값이 0으로
									Sleep(100);		// 모듈 상태 전이를 위한 값
								}
							}
						}							
						m_bSendDoorFlag = TRUE;
					}							
				}
			}
			else
			{
				m_CtrlDoorCheck.Depress(FALSE);
				m_bSendDoorFlag = FALSE;
			}

			if (i == 2 && ((OvenStatus>>i) & 0x00000001))
			{
				strMessage += Fun_FindMsg("DrawOvenStatus_msg1","IDD_DETAIL_OVEN_DLG");
				//@ strMessage += " 불꽃 연기 감지.";
				m_CtrlUVCheck.Depress(TRUE);
				m_CtrlSmokeCheck.Depress(TRUE);
			}
			else
			{
				m_CtrlUVCheck.Depress(FALSE);
				m_CtrlSmokeCheck.Depress(FALSE);
			}

// 			if (i == 3 && ((OvenStatus>>i) & 0x00000001)) m_CtrlOtherCheck.Depress(TRUE);
// 			else m_CtrlOtherCheck.Depress(FALSE);
			
			if (i == 4 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += Fun_FindMsg("DrawOvenStatus_msg2","IDD_DETAIL_OVEN_DLG");}
			//@ if (i == 4 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += " 냉동기 이상.";}
			else m_CtrlOtherCheck.Depress(FALSE);
			
			if (i == 5 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += Fun_FindMsg("DrawOvenStatus_msg3","IDD_DETAIL_OVEN_DLG");}
			//@ if (i == 5 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += " FAN 이상.";}
			else m_CtrlOtherCheck.Depress(FALSE);

			if (i == 6 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += Fun_FindMsg("DrawOvenStatus_msg4","IDD_DETAIL_OVEN_DLG");}
			//@ if (i == 6 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += " HEATER 이상.";}
			else m_CtrlOtherCheck.Depress(FALSE);

			if (i == 7 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += Fun_FindMsg("DrawOvenStatus_msg5","IDD_DETAIL_OVEN_DLG");}
			//@ if (i == 7 && ((OvenStatus>>i) & 0x00000001)){ m_CtrlOtherCheck.Depress(TRUE); strMessage += " 공압 이상.";}
			else m_CtrlOtherCheck.Depress(FALSE);
			//////////////////////////////////////////////////////////////////////////
		}

// 		if (OvenStatus != 0)
// 		{
// 			if (m_pWarringDlg != NULL)
// 			{
// 				delete m_pWarringDlg;
// 			}
// 			m_pWarringDlg = new CWarningDlg;
// 			m_pWarringDlg->m_strWarringMessage = strMessage;
// 			m_pWarringDlg->Create(IDD_WARNING,this);
// 
// 			m_pWarringDlg->ShowWindow(SW_SHOW);
// 		}
	}	
}

void CDetailOvenDlg::ResetLedBtn()
{
	m_CtrlOtherCheck.Depress(FALSE);
	m_CtrlDoorCheck.Depress(FALSE);
	m_CtrlUVCheck.Depress(FALSE);
	m_CtrlSmokeCheck.Depress(FALSE);
}

/************************************************************************/
/* OnButton2()                                                          */
// 상위, 확인 버튼
/************************************************************************/
void CDetailOvenDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	m_bDetailOvenDlgChange = !m_bDetailOvenDlgChange;	
	DisplayOvenGrid();

//	((CMainFrame*)AfxGetMainWnd())->SendOvenStopCmd(0);
}

/************************************************************************/
/* OnGridDoubleClicked()                                                */
/************************************************************************/

LRESULT CDetailOvenDlg::OnGridLDoubleClicked(WPARAM wParam, LPARAM lParam)
{
	if( m_bDetailOvenDlgChange == FALSE )
	{
		//CMyGridWnd *pGrid = (CMyGridWnd*) lParam;

		ROWCOL nRow, nCol;							
	
		nCol = LOWORD( wParam );
		nRow = HIWORD( wParam );
	
		if( nRow >= _ROW_GRID2_ITEM_START_ )
		{
			m_SelectChamberId = nRow - 2;		// 클릭한 부분의 챔버 아이디를 가지고 온다.
			m_bDetailOvenDlgChange = !m_bDetailOvenDlgChange;	
			DisplayOvenGrid();
		}
	}	
	return 1;
}

/************************************************************************/
/* OnGridClicked()                                                      */
/************************************************************************/

LRESULT CDetailOvenDlg::OnGridLClicked(WPARAM wParam, LPARAM lParam)
{
	if( m_bDetailOvenDlgChange == FALSE )
	{
		//CMyGridWnd *pGrid = (CMyGridWnd*) lParam;

		ROWCOL nRow, nCol;
		nCol = LOWORD( wParam );
		nRow = HIWORD( wParam );
		
		if( nRow >= _ROW_GRID2_ITEM_START_ )
		{
			m_SelectChamberId = nRow - 1;		// 클릭한 부분의 챔버 아이디를 가지고 온다.
		}
	}	
	return 1;
}

/************************************************************************/
//
//							참고 소스                                                            
// 
/************************************************************************/
void CDetailOvenDlg::OnBtnAdd() 
{
	/*
	int nCount = m_ctrlOvenList.GetItemCount();
	CString strResource;
	strResource.Format("챔버%d", nCount+1);
	m_ctrlOvenList.InsertItem(LVIF_TEXT|LVIF_STATE, nCount, 
			strResource, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0, 0);	
	*/
}

void CDetailOvenDlg::InitList()
{ /*
	CRect rect;
	m_ctrlOvenList.GetClientRect(&rect);
	int n_dummy =  m_ctrlOvenList.InsertColumn(0, "Dummy", LVCFMT_CENTER, 100);	//Dummy Column

	m_ctrlOvenList.InsertColumn(1, "챔버", LVCFMT_CENTER, rect.Width() - 220);
	m_ctrlOvenList.InsertColumn(2, "현재온도", LVCFMT_CENTER, 80);
	m_ctrlOvenList.InsertColumn(3, "설정온도", LVCFMT_CENTER, 80);
	m_ctrlOvenList.InsertColumn(4, "상태", LVCFMT_CENTER, 60);

	m_ctrlOvenList.DeleteColumn(n_dummy);		//Remove Dummy Column

	m_ctrlOvenList.SetExtendedStyle(LVS_EX_FULLROWSELECT);	
	*/
} 

void CDetailOvenDlg::UpdateList(BOOL bDeleteAllItem)
{
	CString strOvenItem;

	if(bDeleteAllItem)
		m_ctrlOvenList.DeleteAllItems();
	
	for(int i = 0 ; i < m_pCtrlOven->GetOvenCount(); i++)
	{
	//	if(m_pDoc->m_ctrlOven.GetUseOven(i) == FALSE) continue;	//Oven을 사용하지 않으면 표시하지 않음
		int nIndex = i;
		if(bDeleteAllItem)
		{
			strOvenItem.Format(Fun_FindMsg("UpdateList_msg1","IDD_DETAIL_OVEN_DLG"), i+1);
			//@ strOvenItem.Format("챔버 %d", i+1);
			m_ctrlOvenList.InsertItem(nIndex, strOvenItem);
		}		

		float fSetValue = m_pCtrlOven->GetRefTemperature(i);
		float fCurValue = m_pCtrlOven->GetCurTemperature(i);
		

		strOvenItem.Format("%.1f", fCurValue);	//strOvenItem.Format("%.1f℃", fCurValue);
		m_ctrlOvenList.SetItemText(nIndex, 1, strOvenItem);

		strOvenItem.Format("%.1f", fSetValue);	//strOvenItem.Format("%.1f℃", fSetValue);
		m_ctrlOvenList.SetItemText(nIndex, 2, strOvenItem);

		if(m_pCtrlOven->GetLineState(i))
		{
			if(m_pCtrlOven->GetRun(i))	strOvenItem.Format(Fun_FindMsg("UpdateList_msg2","IDD_DETAIL_OVEN_DLG"));
			//@ if(m_pCtrlOven->GetRun(i))	strOvenItem.Format("동작중");
			else								strOvenItem.Format(Fun_FindMsg("UpdateList_msg3","IDD_DETAIL_OVEN_DLG"));
			//@ else								strOvenItem.Format("대기");
		}
		else
		{
			strOvenItem = Fun_FindMsg("UpdateList_msg4","IDD_DETAIL_OVEN_DLG");
			//@ strOvenItem = "통신두절";
		}		
		m_ctrlOvenList.SetItemText(nIndex, 3, strOvenItem);
	}
}

void CDetailOvenDlg::SetListFontSize(int nSize)
{
	CFont font;
	font.CreatePointFont(nSize, Fun_FindMsg("SetListFontSize_msg","IDD_DETAIL_OVEN_DLG"));	
	//@ font.CreatePointFont(nSize, "굴림");	
	m_ctrlOvenList.SetFont(&font);
}

	//if( nCount > 0 && nRow == 0 )
	//{
	//	if( nCol == COL_STEP_REF_I )
	//	{
	
	//	}
	//}
/*
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	
	
	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col
	
	int nCount = m_wndStepGrid.GetRowCount();
	if(nCount > 0 && nRow == 0  )
	{
		if(nCol == COL_STEP_REF_I)
		{
			CCommonInputDlg dlg;
			dlg.m_strTitleString = "적용할 전류 설정값을 입력하십시요";
			if(dlg.DoModal() == IDOK)
			{
				STEP *pStep;
				for(int step = 0; step < GetDocument()->GetTotalStepNum(); step++)
				{
					pStep = GetDocument()->GetStepData(step+1);
					if(pStep == NULL)	break;
					
					if(dlg.m_nMode == 1)	//All Step
					{
						pStep->fIref = dlg.m_fValue;
						m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
					}
					else if(dlg.m_nMode == 2)	//Charge step
					{
						if(pStep->chType == PS_STEP_CHARGE)
						{
							pStep->fIref = dlg.m_fValue;
							if(pStep->chMode == PS_MODE_CP)		//ljb
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_POWER));
							else
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
						}
					}
					else if(dlg.m_nMode == 3)	//DisCharge step
					{
						if(pStep->chType == PS_STEP_DISCHARGE)
						{
							pStep->fIref = dlg.m_fValue;
							if(pStep->chMode == PS_MODE_CP)		//ljb
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_POWER));
							else
								m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
							//							m_wndStepGrid.SelectRange(CGXRange(step+1, COL_STEP_REF_I));
						}
					}
				}
				//Default 현재 선택된 Step으로 
				DisplayStepGrid();
			}
		}
		else if(nCol == COL_STEP_END)
		{
			//			CEndConditionDlg dlg;
			//			dlg.DoModal();
		}
		return 0;
	}
	
	if(nRow < 1)	return 0L;
	
	if(pGrid == (CMyGridWnd *)&m_wndTestListGrid)			//Test List Grid
	{
		OnLoadTest() ;
		//		OnTestEdit() ;
	}
	else if(pGrid == m_pWndCurModelGrid)
	{
		//		OnModelEdit() ;
	}
	return 0;
*/

void CDetailOvenDlg::OnButOvenTest() 
{
   CString strTemp;

   if( m_pCtrlOven && 
	   m_pCtrlOven->m_bPortOpen)
   {
	   //m_pCtrlOven->SetTemperature(m_SelectChamberId,0,20);

	   int iState=m_pCtrlOven->GetRun(0);
	   strTemp.Format(_T("state switch:%s(%d)"),
					 ((iState==1)?_T("RUN"):_T("STOP")),iState);
	   AfxMessageBox(strTemp);

	   if(m_pCtrlOven->GetRun(0)==0)
	   {
			m_pCtrlOven->SendRunCmd(m_SelectChamberId, 0);
			AfxMessageBox("run");
	   }
	   else
	   {
			m_pCtrlOven->SendStopCmd(m_SelectChamberId, 0);
			AfxMessageBox("stop");
	   }
   }
}

void CDetailOvenDlg::InitControl()
{
	m_StaticPumpComm.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_DKGRAY)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);
	
	m_StaticPumpPV.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);
	
	m_StaticPumpSP.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);	

	m_StaticPumpMode.SetFontSize(12)
		.SetTextColor(ID_COLOR_BLACK)
		.SetBkColor(ID_COLOR_WHITE)		
		.SetFontBold(TRUE)		
		.SetFontName("HY헤드라인M")//yulee 20190405 check
		.SetSunken(TRUE);	
}

void CDetailOvenDlg::onPumpDraw()
{	
	if(!m_pCtrlOven) return;

	float fltFactorChillPump; //yulee 20190121
	fltFactorChillPump = atof(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "ChillPumpFactor", "1"));

	CString strTemp;
	int iComm = -1; //yulee 20190614_*
	int iRecv = -1;

	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100 ||
		m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520 ) //yulee 20190613
	{
		int nItem=m_nPortLine-1;//-11;   // 20191223 dhkim 확인 필요 port가 oven은 11 이하라 에러 발생
		if(!m_pDoc) return;
		strTemp=_T("Disconnect");
		if(m_pDoc->m_chillerCtrl[nItem].m_ctrlOven.m_bPortOpen)
		{
#ifdef _DEBUG
			strTemp=_T("Ready_DEBUG");			
#else
			strTemp=_T("Ready");			
#endif

			iComm = 1;
		}
		if((m_pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetDisconnectCnt(0) != 0) && ((m_pDoc->m_chillerCtrl[nItem].m_ctrlOven.GetDisconnectCnt(0))%90 != 0))
		{
#ifdef _DEBUG
			strTemp=_T("Connect_DEBUG");			
#else
			strTemp=_T("Connect");			
#endif

			iRecv =1;
		}
		else
		{
#ifdef _DEBUG
			strTemp=_T("Connect_DEBUG");			
#else
			strTemp=_T("Connect");			
#endif

			iRecv =1;
		}
	}
	else
	{
		//-------------------------------------
		int iComm=m_pCtrlOven->m_pPumpSerial.IsPortOpen();
		int iRecv=m_pCtrlOven->m_pPumpSerial.m_bRecv;

		strTemp=_T("Disconnect");
		if(iComm==1) 
			strTemp=_T("Ready");	
		if(iRecv==1)
		{
			strTemp=_T("Connect");
			if(m_pCtrlOven->m_PumpState.nError==1)
			{
				strTemp+=_T("(Error)");
			}
		}
	}

	if(m_StaticPumpComm.GetText()!=strTemp)
	{
		m_StaticPumpComm.SetText(strTemp);
	}
	//-------------------------------------
	COLORREF nColor=ID_COLOR_DKGRAY;
	if(iComm==1) nColor=ID_COLOR_ORANGE;
	if(iRecv==1)
	{
		nColor=ID_COLOR_GREEN;
		if(m_pCtrlOven->m_PumpState.nError==1)
		{
			nColor=ID_COLOR_RED;
		}
	}	
	if(m_StaticPumpComm.GetBkColor()!=nColor)
	{
		m_StaticPumpComm.SetBkColor(nColor);
		m_StaticPumpComm.Invalidate(TRUE);
	}	
	//-------------------------------------

	if(m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520) //ksj 20200706 : TEMP2520 예외처리 추가
	{
		strTemp.Format(_T("%.1f"),m_pCtrlOven->m_PumpState.fPV);//CUR
		if(m_StaticPumpPV.GetText()!=strTemp)
		{		
			FLOAT FltTmp = 0.0;
			FltTmp = (atof(strTemp))*fltFactorChillPump;
			CString StrTmp;
			StrTmp.Format(_T("%.1f"), FltTmp);
			m_StaticPumpPV.SetText(StrTmp);
		}
		//-------------------------------------
		strTemp.Format(_T("STOP"));

		strTemp.Format(_T("%.1f"), m_pCtrlOven->m_PumpState.fSP);//REF
		if(m_StaticPumpSP.GetText()!=strTemp)
		{
			FLOAT FltTmp = 0.0;
			FltTmp = (atof(strTemp))*fltFactorChillPump;
			CString StrTmp;
			StrTmp.Format(_T("%.1f"), FltTmp);
			m_StaticPumpSP.SetText(StrTmp);
		}
	}
	else //기존 코드
	{
		//strTemp.Format(_T("%d"),m_pCtrlOven->m_PumpState.nPV);//CUR
		strTemp.Format(_T("%.1f"),m_pCtrlOven->m_PumpState.fPV);//lyj 20201105 NEX1100칠러 소숫점
		if(m_StaticPumpPV.GetText()!=strTemp)
		{		
			FLOAT FltTmp = 0.0; //yulee 20181226
			FltTmp = (atof(strTemp))*fltFactorChillPump; //yulee 20190121
	
			CString StrTmp;
			StrTmp.Format(_T("%.3f"), FltTmp);
			m_StaticPumpPV.SetText(StrTmp);
		}
		//-------------------------------------
		strTemp.Format(_T("STOP"));
		//	if(m_pCtrlOven->m_PumpState.nPlay==0)//yulee 20190129//20190129 RUN아닐 때도 표시
		//	{//RUN
		//strTemp.Format(_T("%d"), m_pCtrlOven->m_PumpState.nSP);//REF
		strTemp.Format(_T("%.1f"), m_pCtrlOven->m_PumpState.fSP);//lyj 20201105 NEX1100칠러 소숫점
		//	}
		if(m_StaticPumpSP.GetText()!=strTemp)
		{
			FLOAT FltTmp = 0.0; //yulee 20181226
			FltTmp = (atof(strTemp))*fltFactorChillPump; //yulee 20190121
			
			CString StrTmp;
			StrTmp.Format(_T("%.3f"), FltTmp);
			m_StaticPumpSP.SetText(StrTmp);
		}
	}	

	if(m_pCtrlOven->m_nOvenModelType == CHAMBER_MODBUS_NEX1100 
		|| m_pCtrlOven->m_nOvenModelType == CHILLER_TEMP2520) //yulee 20190613
	{
		//-------------------------------------
		strTemp.Format(_T(""));
		// 		if(m_pCtrlOven->m_PumpState.nMode==0)
		// 		{//AUTO
		strTemp.Format(_T("AUTO"));
		// 		}
		// 		else 
		// 		{//MANUAL
		// 			strTemp.Format(_T("MANUAL"));
		// 		}
		if(m_StaticPumpMode.GetText()!=strTemp)
		{
			m_StaticPumpMode.SetText(strTemp);
		}	
	}
	else
	{
		//-------------------------------------
		strTemp.Format(_T(""));
		if(m_pCtrlOven->m_PumpState.nMode==0)
		{//AUTO
			strTemp.Format(_T("AUTO"));
		}
		else 
		{//MANUAL
			strTemp.Format(_T("MANUAL"));
		}
		if(m_StaticPumpMode.GetText()!=strTemp)
		{
			m_StaticPumpMode.SetText(strTemp);
		}	
	}
}

BOOL CDetailOvenDlg::onChkSbcRunState()
{
	if(!m_pDoc) return FALSE;
	if(!m_pCtrlOven) return FALSE;

	int nItem=m_nPortLine-11;
	if(nItem<0) return FALSE;

	int nCHNO=m_pDoc->m_chillerCtrl[nItem].m_nCHNO;

	CString strTemp;
	CCyclerChannel *pChannel=NULL;
	
	//-------------------------------------------------
	if(nCHNO>0)
 	{
		pChannel=mainGlobal.onSbcCh_GetCHInfo(1,nCHNO);
		if(pChannel)
		{
			if( pChannel->GetState()==PS_STATE_RUN)
			{
				strTemp.Format(_T("CH:%d Working"),nCHNO);
				AfxMessageBox(strTemp);
				return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL CDetailOvenDlg::onChkSerialIsPort()
{
	if(!m_pCtrlOven) return FALSE;

	CString strTemp;

	if(m_pCtrlOven->m_pPumpSerial.IsPortOpen()==FALSE)
	{
		strTemp.Format(_T("Port Disconnected1"));	
 		AfxMessageBox(strTemp);
		return FALSE;
	}
	return TRUE;
}

void CDetailOvenDlg::OnButPumpRun() 
{
	if(m_pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1100
		|| m_pCtrlOven->GetOvenModelType() == CHILLER_TEMP2520)
	{
		int nItem=m_nPortLine-11; 
		m_pCtrlOven = &m_pDoc->m_chillerCtrl[nItem].m_ctrlOven;
		if(	m_pCtrlOven->m_bPortOpen == FALSE) return;
		
	}
	else
	{	
		int nItem=m_nPortLine-11;
		m_pCtrlOven = &m_pDoc->m_chillerCtrl[nItem].m_ctrlOven;
		//----------------------------------
		if(onChkSerialIsPort()==FALSE) return;
		if(onChkSbcRunState()==TRUE) return;
		//----------------------------------
	}
	
	m_pCtrlOven->onPumpCmdAdd(ID_PUMP_CMD_RUN);
// 	int nItem=m_nPortLine-11;
// 		m_pCtrlOven = &m_pDoc->m_chillerCtrl[nItem].m_ctrlOven;
// 	if(m_pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1100)
// 	{
// 		if(	m_pCtrlOven->m_bPortOpen == FALSE) return;
// 
// 	}
// 	else
// 	{	
// 		//----------------------------------
// 		if(onChkSerialIsPort()==FALSE) return;
// 		if(onChkSbcRunState()==TRUE) return;
// 		//----------------------------------
// 	}
// 
// 	m_pCtrlOven->onPumpCmdAdd(ID_PUMP_CMD_RUN);
}

void CDetailOvenDlg::OnButPumpStop() 
{
	int nItem=m_nPortLine-11;
		m_pCtrlOven = &m_pDoc->m_chillerCtrl[nItem].m_ctrlOven;
	if(m_pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1100
		|| m_pCtrlOven->GetOvenModelType() == CHILLER_TEMP2520)
	{
		if(	m_pCtrlOven->m_bPortOpen == FALSE) return;

	}
	else
	{	
		//----------------------------------
		if(onChkSerialIsPort()==FALSE) return;
		if(onChkSbcRunState()==TRUE) return;
		//----------------------------------
	}

	m_pCtrlOven->onPumpCmdAdd(ID_PUMP_CMD_STOP);
}

void CDetailOvenDlg::OnButPumpAuto() 
{
	//----------------------------------
    if(onChkSerialIsPort()==FALSE) return;
	if(onChkSbcRunState()==TRUE) return;
	//----------------------------------
	m_pCtrlOven->onPumpCmdAdd(ID_PUMP_CMD_AUTO);
}

void CDetailOvenDlg::OnButPumpManual() 
{
	//----------------------------------
    if(onChkSerialIsPort()==FALSE) return;
	if(onChkSbcRunState()==TRUE) return;
	//----------------------------------
	m_pCtrlOven->onPumpCmdAdd(ID_PUMP_CMD_MANUAL);
}

void CDetailOvenDlg::OnButPumpSetsp1() 
{
	int nItem=m_nPortLine-11;
		m_pCtrlOven = &m_pDoc->m_chillerCtrl[nItem].m_ctrlOven;
	if(m_pCtrlOven->GetOvenModelType() == CHAMBER_MODBUS_NEX1100
		|| m_pCtrlOven->GetOvenModelType() == CHILLER_TEMP2520)
	{
		if(	m_pCtrlOven->m_bPortOpen == FALSE) return;

	}
	else
	{	
		//----------------------------------
		if(onChkSerialIsPort()==FALSE) return;
		if(onChkSbcRunState()==TRUE) return;
		//----------------------------------
	}
	m_pCtrlOven->onPumpCmdAdd(ID_PUMP_CMD_SETSP1,20);
}

void CDetailOvenDlg::OnWindowPosChanged(WINDOWPOS FAR* lpwndpos) 
{
	CDialog::OnWindowPosChanged(lpwndpos);
	
	onSizeSave(1);
}

void CDetailOvenDlg::onSizeSave(int iType)
{
	KillTimer(ID_OVEN_TIMER_SIZE);
	if(iType==1)
	{
		struct ST_TIMER_OVENSIZESAVE{
		static void CALLBACK TimeProcOvenSizeSave(HWND hwnd,UINT iMsg,UINT_PTR wParam,DWORD lParam)
		{
			::KillTimer(hwnd,wParam);
			CDetailOvenDlg *dlg = (CDetailOvenDlg*)CWnd::FromHandle(hwnd);
			if(!dlg) return;
				
			dlg->onSizeSave(2);			
		}};
		SetTimer( ID_OVEN_TIMER_SIZE, 100, ST_TIMER_OVENSIZESAVE::TimeProcOvenSizeSave);
	}
	else if(iType==2)
	{
		CRect rect;
		GetWindowRect(rect);
		
		CString strTemp;
		strTemp.Format(_T("%d,%d,%d,%d,"),rect.left,rect.top,rect.Width(),rect.Height());
		
		CString strKey;
		if(m_nPortLine==1)       strKey=REG_SERIAL_CONFIG3;
		else if(m_nPortLine==2)  strKey=REG_SERIAL_CONFIG4;
		else if(m_nPortLine==11) strKey=REG_SERIAL_CHILLER1;
		else if(m_nPortLine==12) strKey=REG_SERIAL_CHILLER2;
		else if(m_nPortLine==13) strKey=REG_SERIAL_CHILLER3;
		else if(m_nPortLine==14) strKey=REG_SERIAL_CHILLER4;			 
		else return;
		
		AfxGetApp()->WriteProfileString(strKey, "Win_Size",strTemp);
	}
	else if(iType==3)
	{		
		CString strKey;
		if(m_nPortLine==1)       strKey=REG_SERIAL_CONFIG3;
		else if(m_nPortLine==2)  strKey=REG_SERIAL_CONFIG4;
		else if(m_nPortLine==11) strKey=REG_SERIAL_CHILLER1;
		else if(m_nPortLine==12) strKey=REG_SERIAL_CHILLER2;
		else if(m_nPortLine==13) strKey=REG_SERIAL_CHILLER3;
		else if(m_nPortLine==14) strKey=REG_SERIAL_CHILLER4;			 
		else return;
		
		CString strWinSize=AfxGetApp()->GetProfileString(strKey, "Win_Size");	
		if(strWinSize.IsEmpty() || strWinSize==_T("")) return;
		
		CString strLeft=GStr::str_GetSplit(strWinSize,0,',');
		CString strTop=GStr::str_GetSplit(strWinSize,1,',');
		CString strWidth=GStr::str_GetSplit(strWinSize,2,',');
		CString strHeight=GStr::str_GetSplit(strWinSize,3,',');
		
		CRect rect;
		rect.left=atoi(strLeft);
		rect.top=atoi(strTop);
		rect.right=rect.left+atoi(strWidth);
		rect.bottom=rect.top+atoi(strHeight);

		SIZE s;
		ZeroMemory(&s, sizeof(SIZE));
		s.cx = (LONG)::GetSystemMetrics(SM_CXFULLSCREEN);
		s.cy = (LONG)::GetSystemMetrics(SM_CYFULLSCREEN);

		if((rect.left > s.cx )||(rect.top > s.cy))
		{
			rect.left=0;
			rect.top=0;
			rect.right=rect.left+atoi(strWidth);
			rect.bottom=rect.top+atoi(strHeight);
		}
			

		/*::SetWindowPos( this->m_hWnd, 
					    HWND_NOTOPMOST, 
						rect.left, 
						rect.top, 
						rect.right, 
						rect.bottom, 
						SWP_NOSIZE |SWP_SHOWWINDOW);*/


		//ksj 20200130 : 수정.
		//챔버창을 자유롭게 사이즈 변경할 수 있게 했으므로
		//기억한 마지막 사이즈를 다시 복원.
		int nWidth = atoi(strWidth);
		int nHeight = atoi(strHeight);

		::SetWindowPos( this->m_hWnd, 
			HWND_NOTOPMOST, 
			rect.left, 
			rect.top, 
			nWidth, 
			nHeight, 
			SWP_SHOWWINDOW);
	}	
}

void CDetailOvenDlg::OnButOvenTest2() 
{
	// TODO: Add your control notification handler code here
/*	GetRunWorkMode*/
}

//ksj 20200130 : 창크기 제한
void CDetailOvenDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 200;
	lpMMI->ptMinTrackSize.y = 200;

	lpMMI->ptMaxTrackSize.x = 350;
	lpMMI->ptMaxTrackSize.y = 500;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

//ksj 20200706 : 255,255,255, 와 같은 형식의 문자열 RGB 값을 COLORREF로 변경
COLORREF CDetailOvenDlg::ConvertRGBStrToCOLORREF(CString strColor)
{
	//RED,GREEN,BLUE 분리
	CString strRed, strGreen, strBlue;
	AfxExtractSubString(strRed,strColor,0,',');
	AfxExtractSubString(strGreen,strColor,1,',');
	AfxExtractSubString(strBlue,strColor,2,',');	
	BYTE bRed = atoi(strRed);
	BYTE bGreen = atoi(strGreen);
	BYTE bBlue = atoi(strBlue);

	return RGB(bRed,bGreen,bBlue);
}
