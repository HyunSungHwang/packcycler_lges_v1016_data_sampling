// FlickerConfigDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMonPro.h"
#include "FlickerConfigDlg.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"
#include "LoginManagerDlg.h" //ksj 20201013

HHOOK g_hMessageBoxHook;

// FlickerConfigDlg 대화 상자입니다.
IMPLEMENT_DYNAMIC(CFlickerConfigDlg, CDialog)
CFlickerConfigDlg::CFlickerConfigDlg(CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CFlickerConfigDlg::IDD, pParent)
{
	m_iPrirority = 1;
	m_iRow =1;
	m_iStartRow = 0;
	m_iEndRow = 0;
	m_iUse = 0;
	m_pDoc = pDoc; //ksj 20201012
	m_strCodeDatabaseName = pDoc->m_strCodeDataBaseName; //ksj 20201017
}

CFlickerConfigDlg::~CFlickerConfigDlg()
{
}

void CFlickerConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFlickerConfigDlg, CDialog)
	ON_MESSAGE(WM_GRID_DOUBLECLICK, OnGridDbClicked)
	ON_MESSAGE(WM_GRID_RIGHT_CLICK, OnRButtonClickedRowCol)
	ON_BN_CLICKED(IDC_COLOR_UPDATE_ALL, &CFlickerConfigDlg::OnColorUpdateALL)
	ON_BN_CLICKED(IDC_USE_SETTING, &CFlickerConfigDlg::OnColorUseSetting)
	ON_COMMAND(ID_CHANGE_COLOR, OnChangeColor)
	ON_COMMAND(ID_CHANGE_PRIORITY, OnChangePriority)
	ON_COMMAND(ID_CHANGE_ALL, OnChangeALL)
	ON_COMMAND(ID_USE_SETTING, OnChangeUse)
	//ON_UPDATE_COMMAND_UI(ID_USE_SETTING, OnShowChangeUse)
	ON_BN_CLICKED(IDOK, &CFlickerConfigDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// FlickerConfigDlg 메시지 처리기입니다.



BOOL CFlickerConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bUseAdmin = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC,"Administrator_Flicker",0);

	if(m_bUseAdmin)
	{
		GetDlgItem(IDC_COLOR_UPDATE_ALL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_USE_SETTING)->ShowWindow(SW_SHOW);
	}

	InitFlickerGrid();
	
	return 0;	

}

BOOL CFlickerConfigDlg::InitFlickerGrid()
{
	CString strDBName;
	CString strType, strMsg ,strTemp,strTemp_ ,strTok1 , strTok2;
	
	m_wndFlickerGrid.SubclassDlgItem(IDC_FLICKERLIST_GRID, this);
	m_wndFlickerGrid.Initialize();
	m_wndFlickerGrid.SetColCount(4);
	m_wndFlickerGrid.SetRowHeight(0,0,40); //ksj 20201012

	CRect rect;
	m_wndFlickerGrid.GetClientRect(&rect);

	m_wndFlickerGrid.SetColWidth(COL_FLICKER_CODE, COL_FLICKER_CODE, rect.Width()*0.10f);
	m_wndFlickerGrid.SetColWidth(COL_FLICKER_MESSAGE, COL_FLICKER_MESSAGE, rect.Width()*0.4f);
	m_wndFlickerGrid.SetColWidth(COL_FLICKER_COLOR, COL_FLICKER_COLOR, rect.Width()*0.2f);
	m_wndFlickerGrid.SetColWidth(COL_FLICKER_PRIORITY, COL_FLICKER_PRIORITY, rect.Width()*0.15f);
	m_wndFlickerGrid.SetColWidth(COL_FLICKER_PRIORITY, COL_FLICKER_BUZZERSTOP, rect.Width()*0.15f);

	m_wndFlickerGrid.SetStyleRange(CGXRange(0, COL_FLICKER_CODE),		CGXStyle().SetValue("Code"));
	m_wndFlickerGrid.SetStyleRange(CGXRange(0, COL_FLICKER_MESSAGE),	CGXStyle().SetValue("Message"));
	m_wndFlickerGrid.SetStyleRange(CGXRange(0, COL_FLICKER_COLOR),		CGXStyle().SetValue("Flicking Color\n(Pause)"));
	m_wndFlickerGrid.SetStyleRange(CGXRange(0, COL_FLICKER_PRIORITY),	CGXStyle().SetValue("Priority\nLevel"));
	m_wndFlickerGrid.SetStyleRange(CGXRange(0, COL_FLICKER_BUZZERSTOP),	CGXStyle().SetValue("Buzzer\nStop"));

 	m_wndFlickerGrid.SetStyleRange(CGXRange().SetCols(COL_FLICKER_PRIORITY),
 		CGXStyle()
// 		.SetControl(GX_IDS_CTRL_EDIT)
		.SetAllowEnter(TRUE)
 		);

	m_pPrioritySelCombo = new CGXComboBox(&m_wndFlickerGrid,
		GX_IDS_CTRL_ZEROBASED_EX1,
		GX_IDS_CTRL_ZEROBASED_EX1,
		GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
		);
	m_wndFlickerGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX1, m_pPrioritySelCombo);
	m_wndFlickerGrid.SetStyleRange(CGXRange().SetCols(COL_FLICKER_PRIORITY),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX1)
		.SetHorizontalAlignment(DT_CENTER)
		.SetValue("1")
		.SetChoiceList("0\n1\n2\n3")
		);	

	m_pBuzzerStopSelCombo = new CGXComboBox(&m_wndFlickerGrid,
		GX_IDS_CTRL_ZEROBASED_EX2,
		GX_IDS_CTRL_ZEROBASED_EX2,
		GXCOMBO_ZEROBASED|GXCOMBO_DISPLAYCHOICE
		);
	m_wndFlickerGrid.RegisterControl(GX_IDS_CTRL_ZEROBASED_EX2, m_pBuzzerStopSelCombo);
	m_wndFlickerGrid.SetStyleRange(CGXRange().SetCols(COL_FLICKER_BUZZERSTOP),
		CGXStyle()
		.SetControl(GX_IDS_CTRL_ZEROBASED_EX2)
		.SetHorizontalAlignment(DT_CENTER)
		.SetValue("0")
		.SetChoiceList("Disable\nEnable")
		);	
	
	CString strTableName = m_pDoc->GetChannelCodeTableLangName(); //ksj 20201012
	strDBName = m_pDoc->GetCodeDataBaseName(); //ksj 20201013

 	int nRow = 1;
 	if(!strDBName.IsEmpty())
 	{
 		CDaoDatabase  db;
 		COleVariant data;
		COLORREF strtemp;
 
 		try
 		{
 			db.Open(strDBName);
 			CDaoRecordset rs(&db);

			//CString strSQL("SELECT Code,Message,Color,Priority FROM ChannelCode_ko WHERE Use = 1 ORDER BY Code ");
			CString strSQL;
			strSQL.Format("SELECT Code,Message,Color,Priority,UseAutoBuzzerStop FROM %s WHERE ShowColorTable = 1 ORDER BY Code", strTableName);
 			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);

 			if(!rs.IsBOF()&&!rs.IsEOF())
 			{
 				strType.Empty();
 				while(!rs.IsEOF())
 				{
					COLORREF color;
 					m_wndFlickerGrid.InsertRows(nRow,1);
 					data = rs.GetFieldValue(0);
 					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_CODE), data.iVal);
 					data = rs.GetFieldValue(1);
 					strMsg = data.pbVal;
 					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_MESSAGE), strMsg);	
 					data = rs.GetFieldValue(2);
	 				//m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_COLOR), data.ulVal);
					m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(data.ulVal)));
					//m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR).SetCols(COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(RGB(255, 153, 0))));	
 					data = rs.GetFieldValue(3);
 					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_PRIORITY), data.ulVal);
					//ksj 20201013 : 자동 부저 중단 기능 추가
					data = rs.GetFieldValue(4);
					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_BUZZERSTOP), data.ulVal);				
					//ksj end

					rs.MoveNext();
 					nRow++;
 				}
 			}
 			rs.Close();
 			db.Close();
 		}
 		catch (CDaoException* e)
 		{
 			e->Delete();
 		}
	m_iRow = nRow;
 	}
 
 	m_wndFlickerGrid.GetParam()->EnableUndo(TRUE);

	return TRUE;
}


LONG CFlickerConfigDlg::OnGridDbClicked(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;				//Get Grid	

	ROWCOL nRow, nCol;			
	nCol = LOWORD(wParam);		
	nRow = HIWORD(wParam);	//Get Current Row Col

	int nCount = m_wndFlickerGrid.GetRowCount();
	//if(nCount > 0 && nRow == 0  )
	if(nCount > 0  )
	{
		if(nRow>0 && nCol == COL_FLICKER_COLOR)
		{
			m_iStartRow = nRow;
			m_iEndRow = m_iStartRow+1;

			CGXStyle style;  //ksj 20201012 : 초기 색상 값 지정.
			m_wndFlickerGrid.GetStyleRowCol(nRow,COL_FLICKER_COLOR,style);  //ksj 20201012 : 초기 색상 값 지정.			
			CGXBrush brush = style.GetInterior();  //ksj 20201012 : 초기 색상 값 지정.
			COLORREF color = brush.GetColor();

			CColorDialog ins_dlg(color,CC_FULLOPEN|CC_RGBINIT,this);
			CString strDBName , strSQL;
			CString strCode ;
			
			if(ins_dlg.DoModal() == IDCANCEL)
			{
				return 1;
			}
			m_cColor = ins_dlg.m_cc.rgbResult;
			m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(m_cColor)));
		}
	}

	return 0;
}

LRESULT CFlickerConfigDlg::OnRButtonClickedRowCol(WPARAM wParam, LPARAM lParam)
{
	ROWCOL nRow, nCol;
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;
	nCol = wParam & 0x0000ffff;
	nRow = wParam >> 16;
	
	ASSERT(pGrid);
	if(nCol<0 || nRow < 1 || &m_wndFlickerGrid != pGrid)	return 0;
	
	CPoint point;
	::GetCursorPos(&point);
	
	if (point.x == -1 && point.y == -1)
	{
		//keystroke invocation
		CRect rect;
		GetClientRect(rect);
		ClientToScreen(rect);

		point = rect.TopLeft();
		point.Offset(5, 5);
	}

 	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_POP_MENU));
 
 	CMenu* pPopup = menu.GetSubMenu(4);
 	ASSERT(pPopup != NULL);
 	CWnd* pWndPopupOwner = this;
	
 	while (pWndPopupOwner->GetStyle() & WS_CHILD)
 		pWndPopupOwner = pWndPopupOwner->GetParent();
 
	if(!m_bUseAdmin)
	{
		menu.DeleteMenu(ID_CHANGE_ALL,1);
		menu.DeleteMenu(ID_USE_SETTING,1);
	}

 	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);

	return TRUE;
}

BOOL CFlickerConfigDlg::ExecuteSQL(CString strSQL)
{
	CDaoDatabase  db;

	try
	{
		db.Open(m_strCodeDatabaseName);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}		


	try
	{
		db.Execute(strSQL);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		db.Close();
		return FALSE;
	}	

	db.Close();


	return TRUE;
}

void CFlickerConfigDlg::OnColorUpdateALL()
{
	CColorDialog ins_dlg;
	CString strSQL;
	int iRowCount;

	ins_dlg.DoModal();

	CString strTableName = m_pDoc->GetChannelCodeTableLangName(); //ksj 20201012
	
	//strSQL.Format("UPDATE ChannelCode_ko SET Color=%u ",ins_dlg.m_cc.rgbResult);
	strSQL.Format("UPDATE %s SET Color=%u ",strTableName, ins_dlg.m_cc.rgbResult); //ksj 20201012

	ExecuteSQL(strSQL);

	//int nRow = 1;
	if(!m_strCodeDatabaseName.IsEmpty())
	{
		CDaoDatabase  db;
		COleVariant data;
		COLORREF strtemp;
		CString strMsg;
		int nRow = 1;

		//strSQL.Format("SELECT Color FROM ChannelCode_ko WHERE Use = 1 ORDER BY Code ");
		strSQL.Format("SELECT Color FROM %s WHERE ShowColorTable = 1 ORDER BY Code ",strTableName); //ksj 20201012

		try
		{
			db.Open(m_strCodeDatabaseName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				while(!rs.IsEOF())
				{
					data = rs.GetFieldValue(0);
					m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(ins_dlg.m_cc.rgbResult)));
					rs.MoveNext();
					nRow++;
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			e->Delete();
		}
	}
}

void CFlickerConfigDlg::OnColorUseSetting()
{
	CString strSQL;
	int iRowCount;
	CString strTableName = m_pDoc->GetChannelCodeTableLangName(); //ksj 20201012

	//strSQL.Format("UPDATE ChannelCode_ko SET Use=%d ",1);
	strSQL.Format("UPDATE %s SET ShowColorTable=%d ",strTableName,1); //ksj 20201012
	ExecuteSQL(strSQL);


	//int nRow = 1;
	if(!m_strCodeDatabaseName.IsEmpty())
	{
		CDaoDatabase  db;
		COleVariant data;
		COLORREF strtemp;
		CString strMsg;
		int nRow = 1;

		//CString strSQL("SELECT Code,Message,Color,Priority FROM ChannelCode_ko WHERE Use = 1 ORDER BY Code ");
		CString strSQL;
		strSQL.Format("SELECT Code,Message,Color,Priority FROM %s WHERE ShowColorTable = 1 ORDER BY Code ",strTableName); //ksj 20201012

		try
		{
			db.Open(m_strCodeDatabaseName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				while(!rs.IsEOF())
				{
					COLORREF color;
					m_wndFlickerGrid.InsertRows(nRow,1);
					data = rs.GetFieldValue(0);
					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_CODE), data.iVal);
					data = rs.GetFieldValue(1);
					strMsg = data.pbVal;
					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_MESSAGE), strMsg);	
					data = rs.GetFieldValue(2);
					//m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_COLOR), data.ulVal);
					m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(data.ulVal)));
					//m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR).SetCols(COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(RGB(255, 153, 0))));	
					data = rs.GetFieldValue(3);
					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_PRIORITY), data.ulVal);
					rs.MoveNext();
					nRow++;
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			e->Delete();
		}

		int totalraw = m_wndFlickerGrid.GetRowCount();

		m_wndFlickerGrid.RemoveRows(nRow,totalraw);

		//AfxMessageBox("Use Setting = 1");
	}
}

LRESULT CFlickerConfigDlg::OnSelDragRowsDrop(WPARAM wParam, LPARAM lParam)
{
	CMyGridWnd *pGrid = (CMyGridWnd *) lParam;

	if(pGrid == &m_wndFlickerGrid)
	{
		GX_DROP_TARGET *pTarget = (GX_DROP_TARGET *)wParam;
	}
	return TRUE;
}

void CFlickerConfigDlg::OnChangeALL()
{
	OnChangeColor();
	OnChangePriority();
}

void CFlickerConfigDlg::OnChangePriority()
{
	CRowColArray	awRows;
	ROWCOL nRow, nCol;

	if(m_wndFlickerGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	m_wndFlickerGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();
	if(nSelNo < 1)		return;

	nRow = awRows[0];
	m_iStartRow = nRow;
	m_iEndRow = m_iStartRow+nSelNo;

	if(m_iStartRow>0 )
	{
		CString strCode ;
		int iresult;

		iresult = MyMessageBox("우선순위설정\n(High > Middle > Low)", MB_ABORTRETRYIGNORE | MB_ICONINFORMATION);

		if(iresult == IDABORT)
		{
			m_iPrirority =1;
		}
		else if(iresult == IDRETRY)
		{
			m_iPrirority =2;
		}
		else if(iresult == IDIGNORE)
		{
			m_iPrirority =3;
		}
		else{
			return;
		}

		int i = m_iStartRow;
		for(i;i<m_iEndRow;i++	)
		{
			m_wndFlickerGrid.SetValueRange(CGXRange(i, COL_FLICKER_PRIORITY), (ULONG)m_iPrirority);
		}
	}
}

void CFlickerConfigDlg::OnChangeUse()
{
	CRowColArray	awRows;
	ROWCOL nRow, nCol;
	CString strTableName = m_pDoc->GetChannelCodeTableLangName(); //ksj 20201012
	if(m_wndFlickerGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	m_wndFlickerGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();
	if(nSelNo < 1)		return;

	nRow = awRows[0];
	m_iStartRow = nRow;
	m_iEndRow = m_iStartRow+nSelNo;

	if(m_iStartRow>0 )
	{
		CString strCode ;
		int iresult;

		iresult = MyMessageBox("USE SETTING (NotUse:0,Use:1)", MB_OKCANCEL| MB_ICONINFORMATION);

		if(iresult == IDOK)
		{
			m_iUse =0;
		}
		else if(iresult == IDCANCEL)
		{
			m_iUse =0;
		}
		else
		{
			return;
		}

		int i = m_iStartRow;
		CString strSQL;
		for(i;i<m_iEndRow;i++	)
		{
			//			strCode= m_wndFlickerGrid.GetValueRowCol(i,0) ;
			//m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_PRIORITY), data.uVlal);
			 strCode= m_wndFlickerGrid.GetValueRowCol(i,0) ;
			 //strSQL.Format("UPDATE ChannelCode_ko SET Use=%d WHERE Code = %s",m_iUse,strCode);
			 strSQL.Format("UPDATE %s SET ShowColorTable=%d WHERE Code = %s",strTableName,m_iUse,strCode); //ksj 20201012
			 ExecuteSQL(strSQL);
		}
		CString strtemp;
		strtemp.Format("Use %d Setting",m_iUse);
		AfxMessageBox(strtemp);
	}
}

void CFlickerConfigDlg::OnChangeColor()
{
	CRowColArray	awRows;
	ROWCOL nRow, nCol;
	CString strDBName , strSQL, strCode;
	COLORREF cColor;

	if(m_wndFlickerGrid.GetCurrentCell(nRow, nCol)== FALSE)	return;

	m_wndFlickerGrid.GetSelectedRows(awRows);
	int nSelNo = awRows.GetSize();
	if(nSelNo < 1)		return;

	m_iStartRow = awRows[0];
	m_iEndRow = m_iStartRow+nSelNo;

	if(m_iStartRow>0 )
	{
		CColorDialog ins_dlg;
		ins_dlg.DoModal();
		m_cColor = ins_dlg.m_cc.rgbResult;

		int i = m_iStartRow;
		for(i;i<m_iEndRow;i++)
		{
			strCode= m_wndFlickerGrid.GetValueRowCol(i,0) ;
			m_wndFlickerGrid.SetStyleRange(CGXRange(i, COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(m_cColor)));
		}
	}
}

int CALLBACK SetHook(INT nCode, WPARAM wParam, LPARAM lParam)
{
	HWND    hChildWnd;

	if (nCode == HCBT_ACTIVATE)
	{
		hChildWnd  = (HWND)wParam;
		if (::GetDlgItem(hChildWnd,IDABORT) != NULL)
			::SetDlgItemText(hChildWnd, IDABORT,  "High(1)");
		if (::GetDlgItem(hChildWnd,IDRETRY) != NULL)
			::SetDlgItemText(hChildWnd, IDRETRY, "Middle(2)");
		if (::GetDlgItem(hChildWnd,IDIGNORE) != NULL)
			::SetDlgItemText(hChildWnd, IDIGNORE,  "Low(3)");
		if (::GetDlgItem(hChildWnd,IDOK) != NULL)
			::SetDlgItemText(hChildWnd, IDOK,  "Use 0");
		if (::GetDlgItem(hChildWnd,IDCANCEL) != NULL)
			::SetDlgItemText(hChildWnd, IDCANCEL,  "Use 1");
		// 훅 해제
		UnhookWindowsHookEx(g_hMessageBoxHook);
	}
	else
		CallNextHookEx(g_hMessageBoxHook, nCode, wParam, lParam);

	return 0;
}

UINT CFlickerConfigDlg::MyMessageBox(char* szText, UINT nType)
{
	// 훅설정
	//g_hMessageBoxHook = SetWindowsHookEx(WH_CBT,(int(__stdcall *))&HookWndProc,0, GetCurrentThreadId());
	
	g_hMessageBoxHook = SetWindowsHookEx (WH_CBT,
		(HOOKPROC)SetHook,
		NULL,
		GetCurrentThreadId ());


	// 메시지 박스 호출
	return MessageBox(szText, "알림", nType);

}

void CFlickerConfigDlg::OnBnClickedOk()
{
	CLoginManagerDlg LoginDlg(PMS_SUPERVISOR); //ksj 20201013 : 권한 설정. 관리자 이상만 접근 가능
	if(LoginDlg.DoModal() != IDOK) return;

	CString strTableName = m_pDoc->GetChannelCodeTableLangName(); //ksj 20201012 : 수정

	//ksj 20201013 : 업데이트 쿼리 수정. 화면의 리스트에 있는 모든 값을 재갱신한다.
	CString strCode, strSQL;	
	COLORREF dwColor;
	CString strBuzzerStop;
	CString strPriority;
	CGXStyle style;
	CGXBrush brush;
	COLORREF color = RGB(255,0,0);
	int i=0;

	for(i=1;i<=m_wndFlickerGrid.GetRowCount();i++)
	{
		strCode= m_wndFlickerGrid.GetValueRowCol(i,COL_FLICKER_CODE);		
		strPriority = m_wndFlickerGrid.GetValueRowCol(i,COL_FLICKER_PRIORITY);
		strBuzzerStop = m_wndFlickerGrid.GetValueRowCol(i,COL_FLICKER_BUZZERSTOP);

		if(m_wndFlickerGrid.GetStyleRowCol(i,COL_FLICKER_COLOR,style))
		{
			brush = style.GetInterior();
			color = brush.GetColor();
		}

		strSQL.Format("UPDATE %s SET Color=%u, Priority=%s, UseAutoBuzzerStop=%s WHERE Code=%s",strTableName,color,strPriority,strBuzzerStop,strCode);
		ExecuteSQL(strSQL);
	}
	//ksj end


	m_wndFlickerGrid.ResetCurrentCell(1);
		
	if(!m_strCodeDatabaseName.IsEmpty())
	{
		CDaoDatabase  db;
		COleVariant data;
		COLORREF strtemp;
		int nRow = 1;

		//CString strSQL("SELECT Code,Message,Color,Priority FROM ChannelCode_ko WHERE Use = 1 ORDER BY Code ");
		CString strSQL;
		strSQL.Format("SELECT Code,Message,Color,Priority FROM %s WHERE ShowColorTable = 1 ORDER BY Code ",strTableName);
		
		CString strMsg;

		try
		{
			db.Open(m_strCodeDatabaseName);
			CDaoRecordset rs(&db);
			rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);	
			if(!rs.IsBOF()&&!rs.IsEOF())
			{
				while(!rs.IsEOF())
				{
					//m_wndFlickerGrid.InsertRows(nRow,1);
					data = rs.GetFieldValue(0);
					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_CODE), data.iVal);
					data = rs.GetFieldValue(1);
					strMsg = data.pbVal;
					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_MESSAGE), strMsg);	
					data = rs.GetFieldValue(2);
					//m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_COLOR), data.ulVal);
					m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(data.ulVal)));
					//m_wndFlickerGrid.SetStyleRange(CGXRange(nRow, COL_FLICKER_COLOR).SetCols(COL_FLICKER_COLOR),CGXStyle().SetInterior( CGXBrush().SetColor(RGB(255, 153, 0))));	
					data = rs.GetFieldValue(3);
					m_wndFlickerGrid.SetValueRange(CGXRange(nRow, COL_FLICKER_PRIORITY), data.ulVal);
					rs.MoveNext();
					nRow++;
				}
			}
			rs.Close();
			db.Close();
		}
		catch (CDaoException* e)
		{
			e->Delete();
		}
	}
		
	CDialog::OnOK();
}


void CFlickerConfigDlg::OnShowChangeUse(CCmdUI* pCmdUI) 
{
	if(m_bUseAdmin == 0) 
	{
		if (pCmdUI->m_pMenu!=NULL)
		{
			pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
			return;
		}
	}
}

