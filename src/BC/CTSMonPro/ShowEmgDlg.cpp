// ShowEmgDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "ShowEmgDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShowEmgDlg dialog

//2014.09.26 생성자 변경 CCTSMonProView 추가.
CShowEmgDlg::CShowEmgDlg(CCTSMonProView* pView,CWnd* pParent /*=NULL*/ )
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CShowEmgDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CShowEmgDlg::IDD2):	
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CShowEmgDlg::IDD3):	
	(CShowEmgDlg::IDD), pParent)
{		
	m_pView = pView;
	//{{AFX_DATA_INIT(CShowEmgDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CShowEmgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShowEmgDlg)
	DDX_Control(pDX, IDC_STATIC_MSG2, m_ctrlEmgValue);
	DDX_Control(pDX, IDC_STATIC_MSG, m_ctrlMsgLabel);
	DDX_Control(pDX, IDC_STATIC_EMG, m_ctrlEmgLabel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CShowEmgDlg, CDialog)
	//{{AFX_MSG_MAP(CShowEmgDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShowEmgDlg message handlers

BOOL CShowEmgDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	//m_ctrlMsgLabel.SetWindowText(strMsg);
	//m_ctrlEmgValue.SetWindowText(strEmgValue);
	m_ctrlEmgLabel.SetWindowText(strEmgValue);

	m_ctrlEmgLabel.SetFontSize(30)
					 .SetTextColor(RGB(50, 100, 200))
					 .SetBkColor(RGB(234,28,120))
					 .SetGradientColor(RGB(255,255,255))
					 .SetFontBold(TRUE)
					 .SetGradient(TRUE)
					 .SetFontName(Fun_FindMsg("OnInitDialog_msg","IDD_SHOW_EMG_DLG"))
					 //@ .SetFontName("HY헤드라인M")
					 .FlashBackground(TRUE)
					 .SetSunken(TRUE);
	
	/*m_ctrlMsgLabel.SetFontSize(20)
					 .SetTextColor(RGB(50, 100, 200))
					 .SetBkColor(RGB(234,28,120))
					 .SetGradientColor(RGB(255,255,255))
					 .SetFontBold(TRUE)
					 .SetGradient(TRUE)
					 .SetFontName("HY헤드라인M")
					 .FlashBackground(FALSE)
					 .SetSunken(TRUE);

	m_ctrlEmgValue.SetFontSize(20)
					 .SetTextColor(RGB(50, 100, 200))
					 .SetBkColor(RGB(234,28,120))
					 .SetGradientColor(RGB(255,255,255))
					 .SetFontBold(TRUE)
					 .SetGradient(TRUE)
					 .SetFontName("HY헤드라인M")
					 .FlashBackground(FALSE)
					 .SetSunken(TRUE);*/
	this->SetWindowText(strMsg);	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CShowEmgDlg::SetEmgData(CString strModuleName, CString strCodeMsg, long lValue, CString strCanName)
{
	if (strCanName.IsEmpty())
	{
		strMsg.Format(Fun_FindMsg("SetEmgData_msg1","IDD_SHOW_EMG_DLG"), strModuleName);
		//@ strMsg.Format("%s에서 비상 상태가 발생하였습니다.", strModuleName);
		strEmgValue.Format("%s(Value : %d)", strCodeMsg, lValue);	
	}
	else
	{
		//strMsg.Format(Fun_FindMsg("SetEmgData_msg2","IDD_SHOW_EMG_DLG"), strModuleName,lValue);
		strMsg.Format(Fun_FindMsg("SetEmgData_msg2","IDD_SHOW_EMG_DLG"), strModuleName,lValue,strCanName); //ksj 20201123 : formatstring 에러 수정.
		//@ strMsg.Format("%s에서 채널(%d)에 %s 메시지 이상이 발생하였습니다.", strModuleName,lValue);
		strEmgValue.Format("%s(Value : %d)", strCodeMsg, lValue);	
	}
}

void CShowEmgDlg::SetWarnData(CString strTitle,CString strMessage)
{
	strMsg=strTitle;
	strEmgValue=strMessage;
}

/*
---------------------------------------------------------
---------
-- Filename		:	ShowEmgDlg.cpp 
-- Description	:
-- Author		:	이민규
----------------------------------------------------------
---------
-- Revision History
-- Date			Name		Description
-- 
----------------------------------------------------------
*/
BOOL CShowEmgDlg::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	//2014.09.26 종료시 Emg알람창 상태 변경.
	m_pView->bShowEmgDlg = FALSE;

	return CDialog::DestroyWindow();
}