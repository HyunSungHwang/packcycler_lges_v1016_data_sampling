#pragma once


// CAuxVentSafetyDlg 대화 상자입니다.
class CCTSMonProDoc;
class CAuxVentSafetyDlg : public CDialog
{
	DECLARE_DYNAMIC(CAuxVentSafetyDlg)

public:
	CAuxVentSafetyDlg(CCTSMonProDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAuxVentSafetyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AUX_VENT_SAFETY_DLG , IDD2 = IDD_AUX_VENT_SAFETY_DLG_ENG , IDD3 = IDD_AUX_VENT_SAFETY_DLG_PL}; //lyj 20200716 

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CString m_strAuxTempLimitH;	
	CString m_strAuxVoltLimitH;
	CString m_strAuxVoltLimitL;	
	CCTSMonProDoc *m_pDoc;
	BOOL CheckRunState(void);
	BOOL UpdateAuxData(void);
};
