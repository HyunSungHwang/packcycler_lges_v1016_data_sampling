// SerialConfigDlg.cpp : implementation file
//

#include "stdafx.h" 
 

#include "CTSMonPro.h"
#include "SerialConfigDlg.h"

#include "MainFrm.h"
#include "CTSMonProDoc.h"
#include "CTSMonProView.h"

#include "Global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialConfigDlg dialog


CSerialConfigDlg::CSerialConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSerialConfigDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSerialConfigDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSerialConfigDlg::IDD3):
	(CSerialConfigDlg::IDD), pParent)
{

	//{{AFX_DATA_INIT(CSerialConfigDlg)
	m_bUsePort1 = FALSE;
	m_bUsePort2 = FALSE;
	m_bUsePort3 = FALSE;
	m_bUsePort4 = FALSE;
	m_bUsePort5 = FALSE;
	m_bUsePort6 = FALSE;
	m_fOndoFactor1 = 0.0f;
	m_fOndoFactor2 = 0.0f;
	m_fOndoSPFactor1 = 10.0f;	//20170907 YULEE SPFactor? ???? ? ?? //??? 
	m_fOndoSPFactor2 = 10.0f;	//20170907 YULEE SPFactor? ???? ? ?? //???
	m_fChillerPumpFactor = 1.0f;
	//}}AFX_DATA_INIT
	m_ParityStr.Format("NOEMS");
	//m_strTitle = "외부 기기 통신 설정";
	m_strTitle = Fun_FindMsg("CSerialConfigDlg_msg","IDD_SERIAL_DLG");//&&
}

void CSerialConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CSerialConfigDlg)
	DDX_Control(pDX, IDC_CBO_CHILLER4_PUMP_COM, m_CboChiller4PumpCom);
	DDX_Control(pDX, IDC_CBO_CHILLER3_PUMP_COM, m_CboChiller3PumpCom);
	DDX_Control(pDX, IDC_CBO_CHILLER2_PUMP_COM, m_CboChiller2PumpCom);
	DDX_Control(pDX, IDC_CBO_CHILLER1_PUMP_COM, m_CboChiller1PumpCom);
	DDX_Control(pDX, IDC_CBO_CHILLER4_CHNO, m_CboChiller4Chno);
	DDX_Control(pDX, IDC_CBO_CHILLER3_CHNO, m_CboChiller3Chno);
	DDX_Control(pDX, IDC_CBO_CHILLER1_CHNO, m_CboChiller1Chno);
	DDX_Control(pDX, IDC_CBO_CHILLER2_CHNO, m_CboChiller2Chno);
	DDX_Control(pDX, IDC_CBO_CHILLER4_MODEL, m_CboChiller4Model);
	DDX_Control(pDX, IDC_CBO_CHILLER3_MODEL, m_CboChiller3Model);
	DDX_Control(pDX, IDC_CBO_CHILLER2_MODEL, m_CboChiller2Model);
	DDX_Control(pDX, IDC_CBO_CHILLER1_MODEL, m_CboChiller1Model);
	DDX_Control(pDX, IDC_CBO_CHILLER4_COM, m_CboChiller4Com);
	DDX_Control(pDX, IDC_CBO_CHILLER3_COM, m_CboChiller3Com);
	DDX_Control(pDX, IDC_CBO_CHILLER2_COM, m_CboChiller2Com);
	DDX_Control(pDX, IDC_CBO_CHILLER1_COM, m_CboChiller1Com);
	DDX_Control(pDX, IDC_CHK_CHILLER4_USE, m_ChkChiller4Use);
	DDX_Control(pDX, IDC_CHK_CHILLER3_USE, m_ChkChiller3Use);
	DDX_Control(pDX, IDC_CHK_CHILLER2_USE, m_ChkChiller2Use);
	DDX_Control(pDX, IDC_CHK_CHILLER1_USE, m_ChkChiller1Use);
	DDX_Control(pDX, IDC_CHK_PWR1_USE, m_ChkPwr1Use);
	DDX_Control(pDX, IDC_CHK_PWR2_USE, m_ChkPwr2Use);
	DDX_Control(pDX, IDC_CBO_PWR1_COM, m_CboPwr1Com);
	DDX_Control(pDX, IDC_CBO_PWR2_COM, m_CboPwr2Com);	
	DDX_Control(pDX, IDC_CBO_PWR1A_CHNO, m_CboPwr1AChno);
	DDX_Control(pDX, IDC_CBO_PWR1B_CHNO, m_CboPwr1BChno);
	DDX_Control(pDX, IDC_CBO_PWR2A_CHNO, m_CboPwr2AChno);
	DDX_Control(pDX, IDC_CBO_PWR2B_CHNO, m_CboPwr2BChno);
	DDX_CBString(pDX, IDC_COMBO25, m_ctrlDcLoader_2);
	DDX_CBString(pDX, IDC_COMBO19, m_ctrlDcLoader_1);
	DDX_Control(pDX, IDC_CBO_OVEN2_MAX, m_ctrlOvenMax2);
	DDX_Control(pDX, IDC_CBO_OVEN1_MAX, m_ctrlOvenMax1);
	DDX_Control(pDX, IDC_COMBO_OVEN_MODEL_2, m_ctrlOvenModel_2);
	DDX_Control(pDX, IDC_COMBO_OVEN_MODEL_1, m_ctrlOvenModel_1);
	DDX_CBString(pDX, IDC_SERIAL_PORTNUMCOMBO, m_strPortNum);
	DDX_CBString(pDX, IDC_SERIAL_BAUDRATECOMBO, m_strBaudRate);
	DDX_CBString(pDX, IDC_SERIAL_DATABITSCOMBO, m_strDataBits);
	DDX_CBIndex(pDX, IDC_SERIAL_PARITYCOMBO, m_nParity);
	DDX_CBString(pDX, IDC_SERIAL_STOPBITSCOMBO, m_strStopBits);
	DDX_CBString(pDX, IDC_SERIAL_SENDBUFFERCOMBO, m_strSendBuffer);
	DDX_CBString(pDX, IDC_COMBO1, m_strPortNum1);
	DDX_CBString(pDX, IDC_COMBO2, m_strBaudRate1);
	DDX_CBString(pDX, IDC_COMBO4, m_strDataBits1);
	DDX_CBIndex(pDX, IDC_COMBO3, m_nParity1);
	DDX_CBString(pDX, IDC_COMBO5, m_strStopBits1);
	DDX_CBString(pDX, IDC_COMBO6, m_strSendBuffer1);
	DDX_CBString(pDX, IDC_COMBO7, m_strPortNum2);
	DDX_CBString(pDX, IDC_COMBO8, m_strBaudRate2);
	DDX_CBString(pDX, IDC_COMBO10, m_strDataBits2);
	DDX_CBIndex(pDX, IDC_COMBO9, m_nParity2);
	DDX_CBString(pDX, IDC_COMBO11, m_strStopBits2);
	DDX_CBString(pDX, IDC_COMBO12, m_strSendBuffer2);
	DDX_CBString(pDX, IDC_COMBO13, m_strPortNum3);
	DDX_CBString(pDX, IDC_COMBO19, m_strPortNum_Load1);
	DDX_CBString(pDX, IDC_COMBO25, m_strPortNum_Load2);
	DDX_CBString(pDX, IDC_COMBO14, m_strBaudRate3);
	DDX_CBString(pDX, IDC_COMBO16, m_strDataBits3);
	DDX_CBIndex(pDX, IDC_COMBO15, m_nParity3);
	DDX_CBString(pDX, IDC_COMBO17, m_strStopBits3);
	DDX_CBString(pDX, IDC_COMBO18, m_strSendBuffer3);
	DDX_Check(pDX, IDC_CHECK1, m_bUsePort1);
	DDX_Check(pDX, IDC_CHECK2, m_bUsePort2);
	DDX_Check(pDX, IDC_CHECK3, m_bUsePort3);
	DDX_Check(pDX, IDC_CHECK4, m_bUsePort4);
	DDX_Check(pDX, IDC_CHECK5, m_bUsePort5);
	DDX_Check(pDX, IDC_CHECK6, m_bUsePort6);
	DDX_Text(pDX, IDC_EDIT_ONDOFACTOR1, m_fOndoFactor1);
	DDX_Text(pDX, IDC_EDIT_ONDOFACTOR2, m_fOndoFactor2);
	DDX_Text(pDX, IDC_EDIT_ONDOSPFACTOR1, m_fOndoSPFactor1);	//20170907 YULEE SPFactor? ???? ? ?? //Serial port 1
	DDX_Text(pDX, IDC_EDIT_ONDOSPFACTOR2, m_fOndoSPFactor2);	//20170907 YULEE SPFactor? ???? ? ?? //Serial port 2
	DDX_Text(pDX, IDC_EDIT_CHILLPUMPFACTOR, m_fChillerPumpFactor);//yulee 20190121
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_CHK_CHILLER1_AUTOSTOP, m_ChkChiller1AutoStop);
	DDX_Control(pDX, IDC_CHK_CHILLER2_AUTOSTOP, m_ChkChiller2AutoStop);
	DDX_Control(pDX, IDC_CHK_CHILLER3_AUTOSTOP, m_ChkChiller3AutoStop);
	DDX_Control(pDX, IDC_CHK_CHILLER4_AUTOSTOP, m_ChkChiller4AutoStop);
}


BEGIN_MESSAGE_MAP(CSerialConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CSerialConfigDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_CHK_PWR1_USE, OnChkPwr1Use)
	ON_BN_CLICKED(IDC_CHK_PWR2_USE, OnChkPwr2Use)
	ON_BN_CLICKED(IDC_BUT_PWR1_ON, OnButPwr1On)
	ON_BN_CLICKED(IDC_BUT_PWR1_OFF, OnButPwr1Off)
	ON_BN_CLICKED(IDC_BUT_PWR2_ON, OnButPwr2On)
	ON_BN_CLICKED(IDC_BUT_PWR2_OFF, OnButPwr2Off)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_BN_CLICKED(IDC_CHECK2, OnCheck2)
	ON_BN_CLICKED(IDC_CHECK3, OnCheck3)
	ON_BN_CLICKED(IDC_CHECK4, OnCheck4)
	ON_BN_CLICKED(IDC_CHECK5, OnCheck5)
	ON_BN_CLICKED(IDC_CHECK6, OnCheck6)
	ON_BN_CLICKED(IDC_CHK_CHILLER1_USE, OnChkChiller1Use)
	ON_BN_CLICKED(IDC_CHK_CHILLER2_USE, OnChkChiller2Use)
	ON_BN_CLICKED(IDC_CHK_CHILLER3_USE, OnChkChiller3Use)
	ON_BN_CLICKED(IDC_CHK_CHILLER4_USE, OnChkChiller4Use)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CHK_CHILLER1_AUTOSTOP, &CSerialConfigDlg::OnBnClickedChkChiller1Autostop)
	ON_BN_CLICKED(IDC_CHK_CHILLER2_AUTOSTOP, &CSerialConfigDlg::OnBnClickedChkChiller2Autostop)
	ON_BN_CLICKED(IDC_CHK_CHILLER3_AUTOSTOP, &CSerialConfigDlg::OnBnClickedChkChiller3Autostop)
	ON_BN_CLICKED(IDC_CHK_CHILLER4_AUTOSTOP, &CSerialConfigDlg::OnBnClickedChkChiller4Autostop)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialConfigDlg message handlers

BOOL CSerialConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//cny 201809
	mainGlobal.onSbcCh_GetList(&m_CboPwr1AChno);
	mainGlobal.onSbcCh_GetList(&m_CboPwr1BChno);
	mainGlobal.onSbcCh_GetList(&m_CboPwr2AChno);
	mainGlobal.onSbcCh_GetList(&m_CboPwr2BChno);
	mainGlobal.onSbcCh_GetList(&m_CboChiller1Chno);
	mainGlobal.onSbcCh_GetList(&m_CboChiller2Chno);
	mainGlobal.onSbcCh_GetList(&m_CboChiller3Chno);
	mainGlobal.onSbcCh_GetList(&m_CboChiller4Chno);

	GetDlgItem(IDC_STATIC_TITLE)->SetWindowText(m_strTitle);
	onLoadConfig(1);		
	
	UpdateData(FALSE);
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSerialConfigDlg::OnOk() 
{	
	onSaveConfig(0);

	CDialog::OnOK();	
}

void CSerialConfigDlg::onSaveConfig(int iType) 
{
	UpdateData(TRUE);
	int nGetData;
	
	//----------------------------------------------------------------------------------------	
	if(iType==0 || iType==ID_SERIAL_BCR1)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use BarCode Reader 1", m_bUsePort1);
		
		m_SerialConfig[0].nPortNum		= atol(m_strPortNum);
		m_SerialConfig[0].nPortBaudRate	= atol(m_strBaudRate);
		m_SerialConfig[0].portParity	= m_ParityStr[m_nParity];
		m_SerialConfig[0].nPortDataBits	= atol(m_strDataBits);
		m_SerialConfig[0].nPortStopBits	= atol(m_strStopBits);
		m_SerialConfig[0].nPortBuffer	= atol(m_strSendBuffer);
		
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG1, "PortNo", m_SerialConfig[0].nPortNum);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG1, "BaudRate", m_SerialConfig[0].nPortBaudRate);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG1, "DataBit", m_SerialConfig[0].nPortDataBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG1, "StopBit", m_SerialConfig[0].nPortStopBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG1, "Buffer", m_SerialConfig[0].nPortBuffer);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG1, "Parity", m_SerialConfig[0].portParity);
	}
	
	//----------------------------------------------------------------------------------------
	if(iType==0 || iType==ID_SERIAL_BCR2)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use BarCode Reader 2", m_bUsePort2);

		m_SerialConfig[1].nPortNum		= atol(m_strPortNum1);
		m_SerialConfig[1].nPortBaudRate	= atol(m_strBaudRate1);
		m_SerialConfig[1].portParity	= m_ParityStr[m_nParity1];
		m_SerialConfig[1].nPortDataBits	= atol(m_strDataBits1);
		m_SerialConfig[1].nPortStopBits	= atol(m_strStopBits1);
		m_SerialConfig[1].nPortBuffer	= atol(m_strSendBuffer1);

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG2, "PortNo", m_SerialConfig[1].nPortNum);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG2, "BaudRate", m_SerialConfig[1].nPortBaudRate);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG2, "DataBit", m_SerialConfig[1].nPortDataBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG2, "StopBit", m_SerialConfig[1].nPortStopBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG2, "Buffer", m_SerialConfig[1].nPortBuffer);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG2, "Parity", m_SerialConfig[1].portParity);
	}

	//----------------------------------------------------------------------------------------	
	if(iType==0 || iType==ID_SERIAL_OVEN1)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", m_bUsePort3);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "OvenOndoFactor1", m_fOndoFactor1);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "OvenOndoSPFactor1", m_fOndoSPFactor1);  //20170907 YULEE SPFactor? 
		
		nGetData = m_ctrlOvenModel_1.GetCurSel();
		//yulee 20190611_1 //yulee 20190614_*
		CString tmpStr;
		m_ctrlOvenModel_1.GetLBText(m_ctrlOvenModel_1.GetCurSel(), tmpStr);
		if(tmpStr.Find(_T("MOD")) > -1)//yulee 20190611_1
		{
			int nModelPlusValue;
			nGetData = CHAMBER_MODBUS_NEX_START+(nGetData%10+1);
		}
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "ChamberModel", nGetData);
		nGetData = m_ctrlOvenMax1.GetCurSel();
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "ChamberMaxCount", nGetData+1);
		
		m_SerialConfig[2].nPortNum		= atol(m_strPortNum2);
		m_SerialConfig[2].nPortBaudRate	= atol(m_strBaudRate2);
		m_SerialConfig[2].portParity	= m_ParityStr[m_nParity2];
		m_SerialConfig[2].nPortDataBits	= atol(m_strDataBits2);
		m_SerialConfig[2].nPortStopBits	= atol(m_strStopBits2);
		m_SerialConfig[2].nPortBuffer	= atol(m_strSendBuffer2);

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "PortNo", m_SerialConfig[2].nPortNum);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "BaudRate", m_SerialConfig[2].nPortBaudRate);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "DataBit", m_SerialConfig[2].nPortDataBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "StopBit", m_SerialConfig[2].nPortStopBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Buffer", m_SerialConfig[2].nPortBuffer);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG3, "Parity", m_SerialConfig[2].portParity);
	}
	
	//----------------------------------------------------------------------------------------
	if(iType==0 || iType==ID_SERIAL_OVEN2)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", m_bUsePort4);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "OvenOndoFactor2", m_fOndoFactor2);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "OvenOndoSPFactor2", m_fOndoSPFactor2);  //20170907 YULEE SPFactor? ???? ? ?? //?????? ? ?? 

		nGetData = m_ctrlOvenModel_2.GetCurSel();
		//yulee 20190611_1 //yulee 20190614_*
		CString tmpStr;
		m_ctrlOvenModel_2.GetLBText(m_ctrlOvenModel_2.GetCurSel(), tmpStr);
		if(tmpStr.Find(_T("MOD")) > -1)//yulee 20190611_1
		{
			int nModelPlusValue;
			nGetData = CHAMBER_MODBUS_NEX_START+(nGetData%10+1);
		}
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "ChamberModel", nGetData);
		nGetData = m_ctrlOvenMax2.GetCurSel();
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "ChamberMaxCount", nGetData+1);

		m_SerialConfig[3].nPortNum		= atol(m_strPortNum3);
		m_SerialConfig[3].nPortBaudRate	= atol(m_strBaudRate3);
		m_SerialConfig[3].portParity	= m_ParityStr[m_nParity3];
		m_SerialConfig[3].nPortDataBits	= atol(m_strDataBits3);
		m_SerialConfig[3].nPortStopBits	= atol(m_strStopBits3);
		m_SerialConfig[3].nPortBuffer	= atol(m_strSendBuffer3);

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "PortNo", m_SerialConfig[3].nPortNum);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "BaudRate", m_SerialConfig[3].nPortBaudRate);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "DataBit", m_SerialConfig[3].nPortDataBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "StopBit", m_SerialConfig[3].nPortStopBits);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Buffer", m_SerialConfig[3].nPortBuffer);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG4, "Parity", m_SerialConfig[3].portParity);

	}
	
	//----------------------------------------------------------------------------------------	
	if(iType==0 || iType==ID_SERIAL_LOAD1)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Load 1", m_bUsePort5);

		m_SerialConfig[4].nPortNum		= atol(m_strPortNum_Load1);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG5, "PortNo", m_SerialConfig[4].nPortNum);
	}
	if(iType==0 || iType==ID_SERIAL_LOAD2)
	{
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Load 2", m_bUsePort6);	

		m_SerialConfig[5].nPortNum		= atol(m_strPortNum_Load2);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CONFIG6, "PortNo", m_SerialConfig[5].nPortNum);
	}
	
	//Power Supply----------------------------------------------------------------------------------------	
	//cny 201809
	if(iType==0 || iType==ID_SERIAL_POWER1)
	{	
		m_Serial_Power[0].nPortNum=atoi(GWin::win_CboGetString(&m_CboPwr1Com));
		
		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER1 , "Use"   , m_ChkPwr1Use.GetCheck());		
		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER1 , "PortNo", m_Serial_Power[0].nPortNum);		
		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER1 , "ChNoA"  , atoi(GWin::win_CboGetString(&m_CboPwr1AChno)));
		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER1 , "ChNoB"  , atoi(GWin::win_CboGetString(&m_CboPwr1BChno)));		
	}
	if(iType==0 || iType==ID_SERIAL_POWER2)
	{
		m_Serial_Power[1].nPortNum=atoi(GWin::win_CboGetString(&m_CboPwr2Com));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER2 , "Use"   , m_ChkPwr2Use.GetCheck());
		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER2 , "PortNo", m_Serial_Power[1].nPortNum);	
		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER2 , "ChNoA"  , atoi(GWin::win_CboGetString(&m_CboPwr2AChno)));
		AfxGetApp()->WriteProfileInt(REG_SERIAL_POWER2 , "ChNoB"  , atoi(GWin::win_CboGetString(&m_CboPwr2BChno)));
	}
	
	//chiller----------------------------------------------------------------------------------------	
	//cny 201809
	if(iType==0 || iType==ID_SERIAL_CHILLER1)
	{
		m_Serial_Chiller[0].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller1Com));
		m_Serial_Pump[0].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller1PumpCom));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER1 , "Use"           , m_ChkChiller1Use.GetCheck());
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER1 , "PortNo"        , m_Serial_Chiller[0].nPortNum);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER1 , "ChillerModel"  , GWin::win_CboGetDataItemVal(&m_CboChiller1Model));
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER1 , "ChNo"          , atoi(GWin::win_CboGetString(&m_CboChiller1Chno)));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_PUMP1    , "PortNo"        , m_Serial_Pump[0].nPortNum);

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER1    , "ChillerOffWhenWorkEnd" , m_ChkChiller1AutoStop.GetCheck()); //ksj 20200825
	}

	if(iType==0 || iType==ID_SERIAL_CHILLER2)
	{
		m_Serial_Chiller[1].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller2Com));
		m_Serial_Pump[1].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller2PumpCom));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER2 , "Use"           , m_ChkChiller2Use.GetCheck());
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER2 , "PortNo"        , m_Serial_Chiller[1].nPortNum);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER2 , "ChillerModel"  , GWin::win_CboGetDataItemVal(&m_CboChiller2Model));
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER2 , "ChNo"          , atoi(GWin::win_CboGetString(&m_CboChiller2Chno)));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_PUMP2    , "PortNo"        , m_Serial_Pump[1].nPortNum);	

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER2    , "ChillerOffWhenWorkEnd" , m_ChkChiller2AutoStop.GetCheck()); //ksj 20200825
	}
	if(iType==0 || iType==ID_SERIAL_CHILLER3)
	{
		m_Serial_Chiller[2].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller3Com));
		m_Serial_Pump[2].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller3PumpCom));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER3 , "Use"            , m_ChkChiller3Use.GetCheck());
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER3 , "PortNo"         , m_Serial_Chiller[2].nPortNum);
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER3 , "ChillerModel"  , GWin::win_CboGetDataItemVal(&m_CboChiller3Model));
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER3 , "ChNo"          , atoi(GWin::win_CboGetString(&m_CboChiller3Chno)));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_PUMP3    , "PortNo"        , m_Serial_Pump[2].nPortNum);

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER3    , "ChillerOffWhenWorkEnd" , m_ChkChiller3AutoStop.GetCheck()); //ksj 20200825
	
	}
	if(iType==0 || iType==ID_SERIAL_CHILLER4)
	{
		m_Serial_Chiller[3].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller4Com));
		m_Serial_Pump[3].nPortNum=atoi(GWin::win_CboGetString(&m_CboChiller4PumpCom));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER4 , "Use"           , m_ChkChiller4Use.GetCheck());
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER4 , "PortNo"        , m_Serial_Chiller[3].nPortNum);	
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER4 , "ChillerModel"  , GWin::win_CboGetDataItemVal(&m_CboChiller4Model));
		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER4 , "ChNo"          , atoi(GWin::win_CboGetString(&m_CboChiller4Chno)));

		AfxGetApp()->WriteProfileInt(REG_SERIAL_PUMP4    , "PortNo"        , m_Serial_Pump[3].nPortNum);

		AfxGetApp()->WriteProfileInt(REG_SERIAL_CHILLER4    , "ChillerOffWhenWorkEnd" , m_ChkChiller4AutoStop.GetCheck()); //ksj 20200825
	}

	CString StrTmp; 
	StrTmp.Format(_T("%.1f"), m_fChillerPumpFactor);
	AfxGetApp()->WriteProfileString(CT_CONFIG_REG_SEC, "ChillPumpFactor", StrTmp);  //yulee 20190121 
		
}

BOOL CSerialConfigDlg::onLoadConfig(int iType)
{
	//----------------------------------------------------------------------------------------			
	//m_bUseDoorOpenSensor = AfxGetApp()->GetProfileInt(OVEN_SETTING, "UseDoorOpenSensor", 0);
	//ljb end [11/17/2011 XNOTE]	
	
	int nGetData1,nGetData2;
	//----------------------------------------------------------------------------------------	
	m_SerialConfig[0].nPortNum      = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG1, "PortNo", 1);
	m_SerialConfig[0].nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG1, "BaudRate", 9600);
	m_SerialConfig[0].nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG1, "DataBit", 8);
	m_SerialConfig[0].nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG1, "StopBit", 1);
	m_SerialConfig[0].nPortBuffer   = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG1, "Buffer", 512);
	m_SerialConfig[0].portParity    = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG1, "Parity", 'N');
	m_SerialConfig[0].nPortEvents   = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

	if(iType==1)
	{
		m_bUsePort1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use BarCode Reader 1", FALSE);
		m_strPortNum.Format("%d", m_SerialConfig[0].nPortNum);
		m_strBaudRate.Format("%d", m_SerialConfig[0].nPortBaudRate);
		m_nParity = m_ParityStr.Find(m_SerialConfig[0].portParity);
		m_strDataBits.Format("%d", m_SerialConfig[0].nPortDataBits);
		m_strStopBits.Format("%d", m_SerialConfig[0].nPortStopBits);
		m_strSendBuffer.Format("%d", m_SerialConfig[0].nPortBuffer);
	}
	
	//----------------------------------------------------------------------------------------	
	m_SerialConfig[1].nPortNum      = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG2, "PortNo", 2);
	m_SerialConfig[1].nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG2, "BaudRate", 9600);
	m_SerialConfig[1].nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG2, "DataBit", 8);
	m_SerialConfig[1].nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG2, "StopBit", 1);
	m_SerialConfig[1].nPortBuffer   = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG2, "Buffer", 512);
	m_SerialConfig[1].portParity    = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG2, "Parity", 'N');
	m_SerialConfig[1].nPortEvents   = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

	if(iType==1)
	{
		m_bUsePort2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use BarCode Reader 2", FALSE);	
		m_strPortNum1.Format("%d", m_SerialConfig[1].nPortNum);
		m_strBaudRate1.Format("%d", m_SerialConfig[1].nPortBaudRate);
		m_nParity1 = m_ParityStr.Find(m_SerialConfig[1].portParity);
		m_strDataBits1.Format("%d", m_SerialConfig[1].nPortDataBits);
		m_strStopBits1.Format("%d", m_SerialConfig[1].nPortStopBits);
		m_strSendBuffer1.Format("%d", m_SerialConfig[1].nPortBuffer);
	}

	//----------------------------------------------------------------------------------------	
	m_SerialConfig[2].nPortNum      = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "PortNo", 2);
	m_SerialConfig[2].nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "BaudRate", 9600);
	m_SerialConfig[2].nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "DataBit", 8);
	m_SerialConfig[2].nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "StopBit", 1);
	m_SerialConfig[2].nPortBuffer   = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Buffer", 512);
	m_SerialConfig[2].portParity    = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "Parity", 'N');
	m_SerialConfig[2].nPortEvents   = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

	if(iType==1)
	{
		m_bUsePort3 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 1", FALSE);
		m_fOndoFactor1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoFactor1", FALSE);	
		m_fOndoSPFactor1 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoSPFactor1", 10); //lyj 20200218
		nGetData1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "ChamberModel",0);
		if(nGetData1 > CHAMBER_MODBUS_NEX_START) //yulee 20190611_1 //yulee 20190614_*
			nGetData1 = (nGetData1-CHAMBER_MODBUS_NEX_START) +9;
		m_ctrlOvenModel_1.SetCurSel(nGetData1);
		nGetData1 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG3, "ChamberMaxCount",1);
		m_ctrlOvenMax1.SetCurSel(nGetData1-1);

		m_strPortNum2.Format("%d", m_SerialConfig[2].nPortNum);
		m_strBaudRate2.Format("%d", m_SerialConfig[2].nPortBaudRate);
		m_nParity2 = m_ParityStr.Find(m_SerialConfig[2].portParity);
		m_strDataBits2.Format("%d", m_SerialConfig[2].nPortDataBits);
		m_strStopBits2.Format("%d", m_SerialConfig[2].nPortStopBits);
		m_strSendBuffer2.Format("%d", m_SerialConfig[2].nPortBuffer);
	}

	//----------------------------------------------------------------------------------------	
	m_SerialConfig[3].nPortNum      = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "PortNo", 2);
	m_SerialConfig[3].nPortBaudRate = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "BaudRate", 9600);
	m_SerialConfig[3].nPortDataBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "DataBit", 8);
	m_SerialConfig[3].nPortStopBits = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "StopBit", 1);
	m_SerialConfig[3].nPortBuffer   = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Buffer", 512);
	m_SerialConfig[3].portParity    = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "Parity", 'N');
	m_SerialConfig[3].nPortEvents   = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;

	if(iType==1)
	{
		m_bUsePort4 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Oven 2", FALSE);
		m_fOndoFactor2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoFactor2", FALSE);
		m_fOndoSPFactor2 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "OvenOndoSPFactor2", 10); //lyj 20200218
		nGetData2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "ChamberModel",0);	
		if(nGetData2 > CHAMBER_MODBUS_NEX_START) 
			nGetData2 = (nGetData2-CHAMBER_MODBUS_NEX_START) +9;
		m_ctrlOvenModel_2.SetCurSel(nGetData2);
		nGetData2 = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG4, "ChamberMaxCount",1);	
		m_ctrlOvenMax2.SetCurSel(nGetData2-1);

		m_strPortNum3.Format("%d", m_SerialConfig[3].nPortNum);
		m_strBaudRate3.Format("%d", m_SerialConfig[3].nPortBaudRate);
		m_nParity3 = m_ParityStr.Find(m_SerialConfig[3].portParity);
		m_strDataBits3.Format("%d", m_SerialConfig[3].nPortDataBits);
		m_strStopBits3.Format("%d", m_SerialConfig[3].nPortStopBits);
		m_strSendBuffer3.Format("%d", m_SerialConfig[3].nPortBuffer);
	}

	//----------------------------------------------------------------------------------------		
	m_SerialConfig[4].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG5, "PortNo", 2);	
	m_SerialConfig[5].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CONFIG6, "PortNo", 3);

	if(iType==1)
	{
		m_bUsePort5 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Load 1", FALSE);
		m_bUsePort6 = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Load 2", FALSE);
	
		m_strPortNum_Load1.Format("%d", m_SerialConfig[4].nPortNum);
		m_strPortNum_Load2.Format("%d", m_SerialConfig[5].nPortNum);
	}
	
	//power supply----------------------------------------------------------------------------------------	
	//cny 201809
	m_Serial_Power[0].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1  ,"PortNo", 4);
	m_Serial_Power[1].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2  ,"PortNo", 5);	
	for(int iA=0;iA<MAX_POWER_COUNT;iA++)
	{
		m_Serial_Power[iA].nPortBaudRate=9600;
		m_Serial_Power[iA].nPortDataBits=8;
		m_Serial_Power[iA].nPortStopBits=1;
		m_Serial_Power[iA].nPortBuffer=512;
		m_Serial_Power[iA].portParity='N';
		m_Serial_Power[iA].nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
	}
	
	if(iType==1)
	{
		m_ChkPwr1Use.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1 , "Use", 0));
		m_ChkPwr2Use.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2 , "Use", 0));
		GWin::win_CboGetFind(&m_CboPwr1Com,GStr::str_IntToStr(m_Serial_Power[0].nPortNum));
		GWin::win_CboGetFind(&m_CboPwr2Com,GStr::str_IntToStr(m_Serial_Power[1].nPortNum));
		GWin::win_CboGetFind(&m_CboPwr1AChno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1 , "ChNoA", 0)));
		GWin::win_CboGetFind(&m_CboPwr1BChno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_POWER1 , "ChNoB", 0)));
		GWin::win_CboGetFind(&m_CboPwr2AChno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2 , "ChNoA", 0)));
		GWin::win_CboGetFind(&m_CboPwr2BChno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_POWER2 , "ChNoB", 0)));
	}
	
	//chiller----------------------------------------------------------------------------------------	
	//cny 201809
	m_Serial_Chiller[0].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1  ,"PortNo", 6);
	m_Serial_Chiller[1].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2  ,"PortNo", 7);	
	m_Serial_Chiller[2].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3  ,"PortNo", 8);
	m_Serial_Chiller[3].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4  ,"PortNo", 9);	

	m_Serial_Pump[0].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_PUMP1  ,"PortNo", 10);
	m_Serial_Pump[1].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_PUMP2  ,"PortNo", 11);	
	m_Serial_Pump[2].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_PUMP3  ,"PortNo", 12);
	m_Serial_Pump[3].nPortNum = AfxGetApp()->GetProfileInt(REG_SERIAL_PUMP4  ,"PortNo", 13);	

	m_fChillerPumpFactor = atof(AfxGetApp()->GetProfileString(CT_CONFIG_REG_SEC, "ChillPumpFactor", "1"));  //yulee 20190121 
	
	for(int iB=0;iB<MAX_CHILLER_COUNT;iB++)
	{
		m_Serial_Chiller[iB].nPortBaudRate=9600;
		m_Serial_Chiller[iB].nPortDataBits=8;
		m_Serial_Chiller[iB].nPortStopBits=1;
		m_Serial_Chiller[iB].nPortBuffer=512;
		m_Serial_Chiller[iB].portParity='N';
		m_Serial_Chiller[iB].nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
	}
	
	for(int iC=0;iC<MAX_PUMP_COUNT;iC++)
	{
		m_Serial_Pump[iC].nPortBaudRate=9600;
		m_Serial_Pump[iC].nPortDataBits=8;
		m_Serial_Pump[iC].nPortStopBits=1;
		m_Serial_Pump[iC].nPortBuffer=512;
		m_Serial_Pump[iC].portParity='N';
		m_Serial_Pump[iC].nPortEvents = EV_CTS | EV_BREAK | EV_ERR | EV_RXCHAR | EV_RXFLAG;
	}
	if(iType==1)
	{
		m_ChkChiller1Use.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1 , "Use", 0));
		m_ChkChiller2Use.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2 , "Use", 0));
		m_ChkChiller3Use.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3 , "Use", 0));
		m_ChkChiller4Use.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4 , "Use", 0));

		GWin::win_CboGetFind(&m_CboChiller1Com,GStr::str_IntToStr(m_Serial_Chiller[0].nPortNum));
		GWin::win_CboGetFind(&m_CboChiller2Com,GStr::str_IntToStr(m_Serial_Chiller[1].nPortNum));
		GWin::win_CboGetFind(&m_CboChiller3Com,GStr::str_IntToStr(m_Serial_Chiller[2].nPortNum));
		GWin::win_CboGetFind(&m_CboChiller4Com,GStr::str_IntToStr(m_Serial_Chiller[3].nPortNum));

		GWin::win_CboGetFind(&m_CboChiller1PumpCom,GStr::str_IntToStr(m_Serial_Pump[0].nPortNum));
		GWin::win_CboGetFind(&m_CboChiller2PumpCom,GStr::str_IntToStr(m_Serial_Pump[1].nPortNum));
		GWin::win_CboGetFind(&m_CboChiller3PumpCom,GStr::str_IntToStr(m_Serial_Pump[2].nPortNum));
		GWin::win_CboGetFind(&m_CboChiller4PumpCom,GStr::str_IntToStr(m_Serial_Pump[3].nPortNum));

		m_CboChiller1Model.AddString(_T("TEMP880"));
		m_CboChiller1Model.AddString(_T("MOD_NEX1100")); //yulee 20190611_1 //yulee 20190614_*
		m_CboChiller1Model.AddString(_T("TEMP2520"));
		m_CboChiller1Model.AddString(_T("ST590")); //ksj 20200401
		m_CboChiller1Model.AddString(_T("TEMP2500")); //lyj 20200901 HLGP
		m_CboChiller2Model.AddString(_T("TEMP880"));
		m_CboChiller2Model.AddString(_T("MOD_NEX1100")); //yulee 20190614_*
		m_CboChiller2Model.AddString(_T("TEMP2520"));
		m_CboChiller2Model.AddString(_T("ST590")); //ksj 20200401
		m_CboChiller2Model.AddString(_T("TEMP2500")); //lyj 20200901 HLGP
		m_CboChiller3Model.AddString(_T("TEMP880"));
		m_CboChiller3Model.AddString(_T("MOD_NEX1100")); //yulee 20190614_*
		m_CboChiller3Model.AddString(_T("TEMP2520"));
		m_CboChiller3Model.AddString(_T("ST590")); //ksj 20200401
		m_CboChiller3Model.AddString(_T("TEMP2500")); //lyj 20200901 HLGP
		m_CboChiller4Model.AddString(_T("TEMP880"));
		m_CboChiller4Model.AddString(_T("MOD_NEX1100")); //ksj 20200616
		m_CboChiller4Model.AddString(_T("TEMP2520")); //ksj 20200616
		m_CboChiller4Model.AddString(_T("ST590")); //ksj 20200401
		m_CboChiller4Model.AddString(_T("TEMP2500")); //lyj 20200901 HLGP
		m_CboChiller1Model.SetItemData(0,CHAMBER_TEMP880);
		m_CboChiller1Model.SetItemData(1,CHAMBER_MODBUS_NEX1100); //yulee 20190614_*
		m_CboChiller1Model.SetItemData(2,CHILLER_TEMP2520); //yulee 20190614_*
		m_CboChiller1Model.SetItemData(3,CHILLER_ST590); //ksj 20200401
		m_CboChiller1Model.SetItemData(4,CHAMBER_TEMP2500); //lyj 20200901 HLGP
		m_CboChiller2Model.SetItemData(0,CHAMBER_TEMP880);
		m_CboChiller2Model.SetItemData(1,CHAMBER_MODBUS_NEX1100); //yulee 20190614_*
		m_CboChiller2Model.SetItemData(2,CHILLER_TEMP2520); //yulee 20190614_*
		m_CboChiller2Model.SetItemData(3,CHILLER_ST590); //ksj 20200401
		m_CboChiller2Model.SetItemData(4,CHAMBER_TEMP2500); //lyj 20200901 HLGP
		m_CboChiller3Model.SetItemData(0,CHAMBER_TEMP880);
		m_CboChiller3Model.SetItemData(1,CHAMBER_MODBUS_NEX1100); //yulee 20190614_*
		m_CboChiller3Model.SetItemData(2,CHILLER_TEMP2520); //yulee 20190614_*
		m_CboChiller3Model.SetItemData(3,CHILLER_ST590); //ksj 20200401
		m_CboChiller3Model.SetItemData(4,CHAMBER_TEMP2500); //lyj 20200901 HLGP
		m_CboChiller4Model.SetItemData(0,CHAMBER_TEMP880);
		m_CboChiller4Model.SetItemData(1,CHAMBER_MODBUS_NEX1100); //ksj 20200616
		m_CboChiller4Model.SetItemData(2,CHILLER_TEMP2520); //ksj 20200616
		m_CboChiller4Model.SetItemData(3,CHILLER_ST590); //ksj 20200401
		m_CboChiller4Model.SetItemData(4,CHAMBER_TEMP2500); //lyj 20200901 HLGP

		GWin::win_CboGetDataItemFind(&m_CboChiller1Model,AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1  , "ChillerModel", CHILLER_TEMP2520),TRUE); //yulee 20190614_*
		GWin::win_CboGetDataItemFind(&m_CboChiller2Model,AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2  , "ChillerModel", CHILLER_TEMP2520),TRUE); //yulee 20190614_*
		GWin::win_CboGetDataItemFind(&m_CboChiller3Model,AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3  , "ChillerModel", CHILLER_TEMP2520),TRUE); //yulee 20190614_*
		GWin::win_CboGetDataItemFind(&m_CboChiller4Model,AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4  , "ChillerModel", CHILLER_TEMP2520),TRUE); //yulee 20190614_*

		GWin::win_CboGetFind(&m_CboChiller1Chno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1 , "ChNo", 0)));
		GWin::win_CboGetFind(&m_CboChiller2Chno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2 , "ChNo", 0)));
		GWin::win_CboGetFind(&m_CboChiller3Chno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3 , "ChNo", 0)));
		GWin::win_CboGetFind(&m_CboChiller4Chno,GStr::str_IntToStr(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4 , "ChNo", 0)));
	
		//ksj 20200825 : 작업 완료시 칠러 자동 중단 여부.
		m_ChkChiller1AutoStop.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER1 , "ChillerOffWhenWorkEnd", TRUE));
		m_ChkChiller2AutoStop.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER2 , "ChillerOffWhenWorkEnd", TRUE));
		m_ChkChiller3AutoStop.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER3 , "ChillerOffWhenWorkEnd", TRUE));
		m_ChkChiller4AutoStop.SetCheck(AfxGetApp()->GetProfileInt(REG_SERIAL_CHILLER4 , "ChillerOffWhenWorkEnd", TRUE));
		//ksj end
	}
	return TRUE;
}


void CSerialConfigDlg::onPowerDeal(int nCmd,int nItem,int nVal)
{
	if(!mainFrm) return;
		
	if(nCmd==1)
	{
		int nPort=0;
		if(nItem==0) nPort=atoi(GWin::win_CboGetString(&m_CboPwr1Com));
		if(nItem==1) nPort=atoi(GWin::win_CboGetString(&m_CboPwr2Com));
		if(nPort<1) return;
		
		m_SerialConfig[nItem].nPortNum=nPort;
		mainFrm->m_PwrSupplyCtrl[nItem].m_SerialPort.DisConnect();//OPEN
		
		if(nVal==1)
		{//CLOSE
			mainFrm->m_PwrSupplyCtrl[nItem].onInitSerialPort(m_SerialConfig[nItem]);
		}
	}
	else if(nCmd==11)
	{
		mainFrm->m_PwrSupplyCtrl[nItem].m_SerialPort.m_bMessage=TRUE;
		mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(_T("CHNO_0,OUTP ON,OUTP?,"));		
	}
	else if(nCmd==10)
	{
		mainFrm->m_PwrSupplyCtrl[nItem].m_SerialPort.m_bMessage=TRUE;
		mainFrm->m_PwrSupplyCtrl[nItem].onSerialPortWriteBuffer(_T("CHNO_0,OUTP OFF,OUTP?,"));
	}
}

void CSerialConfigDlg::OnChkPwr1Use() 
{//Power1
	UpdateData(TRUE);
	
	onSaveConfig(ID_SERIAL_POWER1);	

	if(m_ChkPwr1Use.GetCheck()==TRUE)
	{
		onPowerDeal(1,0,1);//PORT CLOSE
	}
	else
	{
		onPowerDeal(1,0,0);//PORT OPEN
	}
	UpdateData(FALSE);
}

void CSerialConfigDlg::OnChkPwr2Use() 
{//Power2
	UpdateData(TRUE);

	onSaveConfig(ID_SERIAL_POWER2);	

	if(m_ChkPwr2Use.GetCheck()==TRUE)
	{
		onPowerDeal(1,1,1);//PORT CLOSE
	}
	else
	{
		onPowerDeal(1,1,0);//PORT OPEN
	}
	UpdateData(FALSE);
}

void CSerialConfigDlg::OnButPwr1On() 
{//Power1
	onPowerDeal(11,0);//OUTP ON
}

void CSerialConfigDlg::OnButPwr1Off() 
{//Power1
	onPowerDeal(10,0);//OUTP OFF
}

void CSerialConfigDlg::OnButPwr2On() 
{//Power2
	onPowerDeal(11,1);//OUTP ON
}

void CSerialConfigDlg::OnButPwr2Off() 
{//Power2
	onPowerDeal(10,1);//OUTP OFF
}

void CSerialConfigDlg::OnCheck1() 
{//BarCode1
	UpdateData(TRUE);

	onSaveConfig(ID_SERIAL_BCR1);	
	mainFrm->onOpenPort_Bcr1(m_bUsePort1);
	
	UpdateData(FALSE);
}

void CSerialConfigDlg::OnCheck2() 
{//BarCode2
	UpdateData(TRUE);
	
	onSaveConfig(ID_SERIAL_BCR2);	
	mainFrm->onOpenPort_Bcr2(m_bUsePort2);
	
	UpdateData(FALSE);
	
}

void CSerialConfigDlg::OnCheck3() 
{//OVEN1
	UpdateData(TRUE);

	BOOL bONOFF;

	if(m_bUsePort3 == TRUE)
	{
		bONOFF = TRUE;
	}
	else if(m_bUsePort3 == FALSE)
	{
		bONOFF = FALSE;
	}

	onSaveConfig(ID_SERIAL_OVEN1);	
	mainFrm->onOpenPort_Chamber(0, bONOFF);
	
	UpdateData(FALSE);
}

void CSerialConfigDlg::OnCheck4() 
{//OVEN2
	UpdateData(TRUE);

	BOOL bONOFF;

	if(m_bUsePort4 == TRUE)
	{
		bONOFF = TRUE;
	}
	else if(m_bUsePort4 == FALSE)
	{
		bONOFF = FALSE;
	}
	
	onSaveConfig(ID_SERIAL_OVEN2);	
	mainFrm->onOpenPort_Chamber(1, bONOFF);
	
	UpdateData(FALSE);
}

void CSerialConfigDlg::OnCheck5() 
{//LOAD1
	UpdateData(TRUE);
	
	onSaveConfig(ID_SERIAL_LOAD1);	
	mainFrm->onOpenPort_Load1(m_bUsePort5);
	
	UpdateData(FALSE);
}

void CSerialConfigDlg::OnCheck6() 
{//LOAD2
	UpdateData(TRUE);
	
	onSaveConfig(ID_SERIAL_LOAD2);	
	mainFrm->onOpenPort_Load2(m_bUsePort6);
	
	UpdateData(FALSE);
}

void CSerialConfigDlg::OnChkChiller1Use() 
{//Chiller1
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkChiller1Use.GetCheck();

	onSaveConfig(ID_SERIAL_CHILLER1);	
	mainFrm->onOpenPort_Chiller(1,bflag);
	
	UpdateData(FALSE);	
}

void CSerialConfigDlg::OnChkChiller2Use() 
{//Chiller2
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkChiller2Use.GetCheck();
	
	onSaveConfig(ID_SERIAL_CHILLER2);	
	mainFrm->onOpenPort_Chiller(2,bflag);
	
	UpdateData(FALSE);	
}

void CSerialConfigDlg::OnChkChiller3Use() 
{//Chiller3
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkChiller3Use.GetCheck();
	
	onSaveConfig(ID_SERIAL_CHILLER3);	
	mainFrm->onOpenPort_Chiller(3,bflag);
	
	UpdateData(FALSE);	
}

void CSerialConfigDlg::OnChkChiller4Use() 
{//Chiller4
	UpdateData(TRUE);
	
	BOOL bflag=m_ChkChiller4Use.GetCheck();
	
	onSaveConfig(ID_SERIAL_CHILLER4);	
	mainFrm->onOpenPort_Chiller(4,bflag);
	
	UpdateData(FALSE);	
}

void CSerialConfigDlg::OnBnClickedChkChiller1Autostop()
{
//	onSaveConfig(ID_SERIAL_CHILLER1);
}


void CSerialConfigDlg::OnBnClickedChkChiller2Autostop()
{
//	onSaveConfig(ID_SERIAL_CHILLER2);
}


void CSerialConfigDlg::OnBnClickedChkChiller3Autostop()
{
//	onSaveConfig(ID_SERIAL_CHILLER3);
}


void CSerialConfigDlg::OnBnClickedChkChiller4Autostop()
{
//	onSaveConfig(ID_SERIAL_CHILLER4);
}
