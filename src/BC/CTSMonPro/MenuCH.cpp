#include "stdafx.h" 
 

#include "MenuCH.h"

int strcfind(LPCSTR string,char ch,int nStart)
{
	int i=nStart;
   
	while( i < lstrlen(string) )
	{
		if( *(string+i) == ch )
			return i;
		i++;
	}
	return -1;
}
CMenuCH::CMenuCH()
{
	m_Width		  = 25;				// default menu width
	m_Height	  = 32;				// defautl menu height
	m_nType		  = MIT_ICON;		// default menu type (Icon Menu)
}
CMenuCH::~CMenuCH()
{
	while( m_MenuList.GetCount() )	// release all menu item
	{
		CMenuItem* pItem = m_MenuList.GetHead();
		if( pItem->m_pszHotkey != NULL )
			delete pItem->m_pszHotkey;
		delete pItem->m_pBitmap;	// the menu include bitmap
		delete pItem;
		m_MenuList.RemoveHead();
	}
}
BOOL CMenuCH::AppendMenu(UINT nFlags,UINT nIDNewItem,LPCSTR lpszNewItem,
						 UINT nIDBitmap,HICON hIcon)
{
	CBitmap* pBitmap;

	if( nIDBitmap != NULL )				 // the menu has bitmap
	{
		pBitmap = new CBitmap;			 
		pBitmap->LoadBitmap(nIDBitmap);  // load bitmap from application resource
	}
	else
		pBitmap = NULL;					 // not has bitmap 

	// create a new menu item that associated bitmap and icon (if has)
	CMenuItem* pItem = new CMenuItem(lpszNewItem,pBitmap,hIcon);
	m_MenuList.AddTail(pItem);

	pItem->m_nHeight = m_Height;	// set current menu height
	pItem->m_nWidth  = m_Width;		// set current menu width

	//Commected by KBH
//	if( !(nFlags & MF_POPUP) )		// not handled popup menu
	nFlags |= MF_OWNERDRAW;
	

	lstrcpy(pItem->m_szText,"");
	pItem->m_pszHotkey = NULL;
	int n;
	if( (n=strcfind(lpszNewItem,'\t',0)) != -1 )	// get item string and hotkey string
	{
		strncpy(pItem->m_szText,lpszNewItem,n);
		pItem->m_szText[n] = '\0';

		pItem->m_pszHotkey = new char[lpszNewItem+n+1,lstrlen(lpszNewItem)-(n+1)+1+1];
		strncpy(pItem->m_pszHotkey,lpszNewItem+n+1,lstrlen(lpszNewItem)-(n+1)+1);
		pItem->m_pszHotkey[lstrlen(lpszNewItem)-(n+1)+1] = '\0';
	}	
	else
		lstrcpy(pItem->m_szText,lpszNewItem);
	return CMenu::AppendMenu(nFlags,nIDNewItem,(LPCSTR)pItem);
}
void CMenuCH::SetMenuWidth(DWORD width)
{
	m_Width = width;
}
void CMenuCH::SetMenuHeight(DWORD height)
{
	m_Height = height;
}
void CMenuCH::SetMenuType(UINT nType)
{
	m_nType = nType;
}
void CMenuCH::MeasureItem(LPMEASUREITEMSTRUCT lpMIS)
{
	UINT state = GetMenuState(lpMIS->itemID,MF_BYCOMMAND);

	CMenuItem* pItem = reinterpret_cast<CMenuItem *>(lpMIS->itemData);
	if( state & MF_SEPARATOR )		// item state is separator
	{
		lpMIS->itemWidth =  pItem->m_nWidth;
		lpMIS->itemHeight = 6;
	}
	else							// other item state
	{
		lpMIS->itemWidth =  pItem->m_nWidth;
		lpMIS->itemHeight = pItem->m_nHeight;
	}
}
void CMenuCH::DrawColorMenu(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);

	CMenuItem* pItem = reinterpret_cast<CMenuItem *>(lpDIS->itemData);
	CRect rect(&lpDIS->rcItem); 

	if (lpDIS->itemAction & ODA_DRAWENTIRE)
	{
		// paint the brush and color item in requested
		pDC->FrameRect(rect,&CBrush(GetSysColor(COLOR_3DFACE)));
		rect.DeflateRect(3,3,3,3);
		pDC->FrameRect(rect,&CBrush(RGB(128,128,128)));

		rect.DeflateRect(1,1,1,1);
		// draw a rectangle palette
		pDC->FillSolidRect(rect,(COLORREF) atol(pItem->m_szText));
	}

	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// item has been selected - raised frame
		pDC->DrawEdge(rect, EDGE_RAISED, BF_RECT);
		m_SelColor = (COLORREF) atol(pItem->m_szText);
		m_curSel = lpDIS->itemID;
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & ODA_SELECT))
	{
		// item has been de-selected -- remove frame
		CBrush br(GetSysColor(COLOR_3DFACE));
		pDC->FrameRect(rect, &br);
		rect.DeflateRect(1,1,1,1);
		pDC->FrameRect(rect, &br);
	}
}
void CMenuCH::DrawIconMenu(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);

	CMenuItem* pItem = reinterpret_cast<CMenuItem *>(lpDIS->itemData);
	CRect rect(&lpDIS->rcItem); 
	
	UINT state = GetMenuState(lpDIS->itemID,MF_BYCOMMAND);
	if (lpDIS->itemAction & ODA_DRAWENTIRE)
	{
		// paint icon item in requested
		pDC->DrawIcon(rect.left+2,rect.top,pItem->m_hIcon);
		// the menu state is separator - draw a color title
		if( state & MF_SEPARATOR )
		{
			for(int i=0; i<rect.Width(); i++)
				pDC->FillRect(rect,&CBrush(RGB(216-i,216-i,235)));
		}
	}

	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// item has been selected - draw hilite frame
		CBrush br(RGB(64,64,128));
		pDC->FrameRect(rect,&br);
		rect.DeflateRect(1,1,1,1);
		pDC->FrameRect(rect,&br);
		m_curSel = lpDIS->itemID;	// get current selected 
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & ODA_SELECT))
	{
		// item has been de-selected - remove frame
		CBrush br(::GetSysColor(COLOR_3DFACE));
		pDC->FrameRect(rect,&br);
		rect.DeflateRect(1,1,1,1);
		pDC->FrameRect(rect,&br);
	}
}
void CMenuCH::DrawXPMenu(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);

	CMenuItem* pItem = reinterpret_cast<CMenuItem *>(lpDIS->itemData);
	CRect rectFull(&lpDIS->rcItem); 

	// get icon region and text region
	CRect rectIcon(rectFull.left,rectFull.top,rectFull.left+20,rectFull.top+20);
	CRect rectText(rectIcon.right,rectFull.top,rectFull.right,rectFull.bottom);

	UINT state = GetMenuState(lpDIS->itemID,MF_BYCOMMAND);

	if (lpDIS->itemAction & ODA_DRAWENTIRE)
	{
		// paint the brush and icon item in requested
		CRect rect;
		// paint item background 
		pDC->FillRect(rectFull,&CBrush(RGB(255,255,240)));
		pDC->FillRect(rectIcon,&CBrush(GetSysColor(COLOR_3DFACE)));

		if( state & MF_SEPARATOR )	// item is separator
		{	// draw a etched edge
			rect.CopyRect(rectText);
			rect.OffsetRect(5,3);
			pDC->DrawEdge(rect,EDGE_ETCHED,BF_TOP);
		}
			
		if( state & MFS_CHECKED ) // item has been checked
		{
			// paint the frame and brush,then draw a check mark
			rect.CopyRect(rectIcon);
			rect.DeflateRect(1,1,1,1);
			pDC->FrameRect(rect,&CBrush(RGB(64,64,128)));

			rect.DeflateRect(1,1,1,1);
			pDC->FillRect(rect,&CBrush(RGB(216,216,235)));
			DrawCheckMark(pDC,rect.left+5,rect.top+5,RGB(64,64,128));
		}
		
		if( pItem->m_pBitmap )	// item has bitmap
		{	// draw a bitmap
			CDC memDC;
			memDC.CreateCompatibleDC(pDC);
			CBitmap* pOldBitmap = memDC.SelectObject(pItem->m_pBitmap);
			pDC->BitBlt(rectIcon.left+2,rectIcon.top+2,16,16,&memDC,0,0,SRCCOPY);
			memDC.SelectObject(pOldBitmap);
		}


		// draw display text
		pDC->SetBkMode(TRANSPARENT);
		rect.CopyRect(rectText);
		rect.DeflateRect(2,2,2,2);
		//rect.OffsetRect(6,2);		
		//ksj 20170811: Sub 팝업 메뉴 글자 하단 잘리는 현상 수정.		
		rect.OffsetRect(6,0);
		if( state & MF_POPUP)
		{
			rect.OffsetRect(0,-2);
			rect.InflateRect(0,0,0,6);
		}
		//ksj end

		if( state & MFS_DISABLED )	// item has been disabled
			pDC->SetTextColor(GetSysColor(COLOR_3DFACE));
		
		pDC->DrawText(pItem->m_szText,lstrlen(pItem->m_szText),rect,DT_LEFT|DT_SINGLELINE);	
		if( pItem->m_pszHotkey != NULL )
		pDC->DrawText(pItem->m_pszHotkey,lstrlen(pItem->m_pszHotkey),
					  CRect(rect.right-60,rect.top,rect.right,rect.bottom),DT_LEFT|DT_SINGLELINE);	
	}

	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// item has been selected
		if( state & MFS_DISABLED )	
			return;					// item has been disabled  
		CRect  rect;

		// item has bitmap - draw a raised edge
		if( !(state & MFS_CHECKED ) && (pItem->m_pBitmap != NULL) )	
			pDC->DrawEdge(rectIcon,EDGE_RAISED,BF_RECT);

		// draw hilite frame
		rect.SetRect(rectText.left+2,rectText.top,rectText.right,rectText.bottom);
		pDC->FrameRect(rect,&CBrush(RGB(64,64,128)));
		// draw hilite brush
		rect.DeflateRect(1,1,1,1);
		pDC->FillRect(rect,&CBrush(RGB(216,216,235)));
		// draw display text
		rect.CopyRect(rectText);
		rect.DeflateRect(2,2,2,2);
		//rect.OffsetRect(6,2);		
		//ksj 20170811: Sub 팝업 메뉴 글자 하단 잘리는 현상 수정.		
		rect.OffsetRect(6,0);		
		if( state & MF_POPUP)
		{
			rect.OffsetRect(0,-2);
			rect.InflateRect(0,0,0,6);
		}
		//ksj end

		pDC->SetBkMode(TRANSPARENT);
		pDC->DrawText(pItem->m_szText,lstrlen(pItem->m_szText),rect,DT_LEFT|DT_SINGLELINE);	
		if( pItem->m_pszHotkey != NULL )
		pDC->DrawText(pItem->m_pszHotkey,lstrlen(pItem->m_pszHotkey),
					  CRect(rect.right-60,rect.top,rect.right,rect.bottom),DT_LEFT|DT_SINGLELINE);	
		m_curSel = lpDIS->itemID;	// get current selected
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & ODA_SELECT))
	{
		// item has been de-selected
		CRect rect;
		rect.CopyRect(rectIcon);

		if( pItem->m_pBitmap )
		{	// the item has bitmap - repaint icon region and bitmap
			pDC->FillRect(rect,&CBrush(::GetSysColor(COLOR_3DFACE)));
			CDC memDC;
			memDC.CreateCompatibleDC(pDC);
			CBitmap* pOldBitmap = memDC.SelectObject(pItem->m_pBitmap);
			pDC->BitBlt(rectIcon.left+2,rectIcon.top+2,16,16,&memDC,0,0,SRCCOPY);

			memDC.SelectObject(pOldBitmap);
		}

		// draw display text
		pDC->FillRect(rectText,&CBrush(RGB(255,255,240)));
		rect.CopyRect(rectText);
		rect.DeflateRect(2,2,2,2);
		//rect.OffsetRect(6,2);		
		//ksj 20170811: Sub 팝업 메뉴 글자 하단 잘리는 현상 수정.		
		rect.OffsetRect(6,0);		
		if( state & MF_POPUP)
		{
			rect.OffsetRect(0,-2);
			rect.InflateRect(0,0,0,6);
		}
		//ksj end

		pDC->SetBkMode(TRANSPARENT);
		if( state & MFS_DISABLED ) // item has been disabled
			pDC->SetTextColor(GetSysColor(COLOR_3DFACE));

		pDC->DrawText(pItem->m_szText,lstrlen(pItem->m_szText),rect,DT_LEFT|DT_SINGLELINE);	
		if( pItem->m_pszHotkey != NULL )
		pDC->DrawText(pItem->m_pszHotkey,lstrlen(pItem->m_pszHotkey),
					  CRect(rect.right-60,rect.top,rect.right,rect.bottom),DT_LEFT|DT_SINGLELINE);	
	}
}
void CMenuCH::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	switch(m_nType)
	{
	case MIT_XP:
		DrawXPMenu(lpDIS);
		break;
	case MIT_ICON:
		DrawIconMenu(lpDIS);
		break;
	case MIT_COLOR:
		DrawColorMenu(lpDIS);
		break;
	}
}

void CMenuCH::DrawCheckMark(CDC* pDC,int x,int y,COLORREF color)
{
	CPen Pen;

	Pen.CreatePen(PS_SOLID,0,color);
	CPen *pOldPen = pDC->SelectObject(&Pen);
	
	pDC->MoveTo(x,y+2);
	pDC->LineTo(x,y+5);
	
	pDC->MoveTo(x+1,y+3);
	pDC->LineTo(x+1,y+6);
	
	pDC->MoveTo(x+2,y+4);
	pDC->LineTo(x+2,y+7);
	
	pDC->MoveTo(x+3,y+3);
	pDC->LineTo(x+3,y+6);
	
	pDC->MoveTo(x+4,y+2);
	pDC->LineTo(x+4,y+5);
	
	pDC->MoveTo(x+5,y+1);
	pDC->LineTo(x+5,y+4);
	
	pDC->MoveTo(x+6,y);
	pDC->LineTo(x+6,y+3);
	
	pDC->SelectObject(pOldPen);
	Pen.DeleteObject();
}

