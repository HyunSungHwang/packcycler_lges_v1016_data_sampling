// Dlg_Dblogin.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "Dlg_Dblogin.h"

#include "./Global/Mysql/PneApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dlg_Dblogin dialog


Dlg_Dblogin::Dlg_Dblogin(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(Dlg_Dblogin::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(Dlg_Dblogin::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(Dlg_Dblogin::IDD3):
	(Dlg_Dblogin::IDD), pParent)
{
	//{{AFX_DATA_INIT(Dlg_Dblogin)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void Dlg_Dblogin::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Dlg_Dblogin)
	DDX_Control(pDX, IDC_EDIT_DBNAME, m_EditDbname);
	DDX_Control(pDX, IDC_EDIT_DBIP, m_EditDbip);
	DDX_Control(pDX, IDC_EDIT_DBPORT, m_EditDbport);
	DDX_Control(pDX, IDC_EDIT_DBID, m_EditDbid);
	DDX_Control(pDX, IDC_EDIT_DBPWD, m_EditDbpwd);
	DDX_Control(pDX, IDC_CHK_AUTO, m_ChkAuto);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Dlg_Dblogin, CDialog)
	//{{AFX_MSG_MAP(Dlg_Dblogin)
	ON_BN_CLICKED(IDC_BUT_DBTEST, OnButDbtest)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dlg_Dblogin message handlers

BOOL Dlg_Dblogin::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	InitObject();
		
	return FALSE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void Dlg_Dblogin::OnOK() 
{
	if(chkDBTest(FALSE)==FALSE) return;

	CString strIP=GWin::win_GetText(&m_EditDbip);
	CString strPort=GWin::win_GetText(&m_EditDbport);
	CString strName=GWin::win_GetText(&m_EditDbname);

	CString strID=GWin::win_GetText(&m_EditDbid);
	CString strPWD=GWin::win_GetText(&m_EditDbpwd);

	BOOL bAuto=m_ChkAuto.GetCheck();

	g_AppInfo.strDbip=strIP;
	g_AppInfo.strDbport=strPort;
	g_AppInfo.strDbname=strName;
	g_AppInfo.strDbid=strID;
	g_AppInfo.strDbpwd=strPWD;
	g_AppInfo.strDbauto=GStr::str_IntToStr(bAuto);

	GIni::ini_SetFile(g_AppIni,_T("DB"),_T("DBIP"),strIP);
	GIni::ini_SetFile(g_AppIni,_T("DB"),_T("DBPORT"),strPort);
	GIni::ini_SetFile(g_AppIni,_T("DB"),_T("DBNAME"),strName);
	GIni::ini_SetFile(g_AppIni,_T("DB"),_T("DBID"),strID);	

	strPWD=GStr::str_Encrypt2(strPWD);
	if(bAuto==TRUE) GIni::ini_SetFile(g_AppIni,_T("DB"),_T("DBPWD"),strPWD);
	
	GIni::ini_SetFile(g_AppIni,_T("DB"),_T("AUTO"),GStr::str_IntToStr(bAuto));

	CDialog::OnOK();
}

void Dlg_Dblogin::OnCancel() 
{
	CDialog::OnCancel();
}

void Dlg_Dblogin::OnClose() 
{
	CDialog::OnClose();
}


void Dlg_Dblogin::InitObject()
{
	m_EditDbip.SetWindowText(g_AppInfo.strDbip);
	m_EditDbport.SetWindowText(g_AppInfo.strDbport);
	m_EditDbname.SetWindowText(g_AppInfo.strDbname);

	m_EditDbid.SetWindowText(g_AppInfo.strDbid);
	if(g_AppInfo.strDbauto==_T("1"))
	{
		m_EditDbpwd.SetWindowText(g_AppInfo.strDbpwd);
		m_ChkAuto.SetCheck(TRUE);
	}	
	m_EditDbip.SetFocus();
}

BOOL Dlg_Dblogin::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE) 
		{				
			OnClose();
			return TRUE;	
		}
		else if(pMsg->wParam == VK_RETURN)
		{
			OnOK();
			return TRUE;
		}
	} 
	return CDialog::PreTranslateMessage(pMsg);
}

void Dlg_Dblogin::OnButDbtest() 
{
	chkDBTest(TRUE);
}


BOOL Dlg_Dblogin::chkDBTest(BOOL bCreate)
{
	CString strIP=GWin::win_GetText(&m_EditDbip);
	CString strPort=GWin::win_GetText(&m_EditDbport);
	CString strName=GWin::win_GetText(&m_EditDbname);

	CString strID=GWin::win_GetText(&m_EditDbid);
	CString strPWD=GWin::win_GetText(&m_EditDbpwd);

	if(strIP.IsEmpty() || strIP==_T(""))
	{
		MSGWARN(this->m_hWnd,MSG_EMPTYIP,TITLE_WARN);
		return FALSE;
	}
	if(strPort.IsEmpty() || strPort==_T(""))
	{
		MSGWARN(this->m_hWnd,MSG_EMPTYPORT,TITLE_WARN);
		return FALSE;
	}
	if(strName.IsEmpty() || strName==_T(""))
	{
		MSGWARN(this->m_hWnd,MSG_EMPTYDATABASE,TITLE_WARN);
		return FALSE;
	}

	if(strID.IsEmpty() || strID==_T(""))
	{
		MSGWARN(this->m_hWnd,MSG_EMPTYID,TITLE_WARN);
		return FALSE;
	}
	if(strPWD.IsEmpty() || strPWD==_T(""))
	{
		MSGWARN(this->m_hWnd,MSG_EMPTYPWD,TITLE_WARN);
		return FALSE;
	}
	
	BOOL bflag=mainPneApp.m_MySql.DBConn(strIP,strID,strPWD,strName,_ttoi(strPort),FALSE,TRUE);	
	if(bflag==FALSE)
	{
		MSGWARN(this->m_hWnd,MSG_DBCONNFAILED,TITLE_WARN);
		return FALSE;
	}

	mainPneApp.CreateTbMonitor();

	if(bCreate==TRUE)
	{
		MSGWARN(this->m_hWnd,MSG_DBCONNOK,TITLE_SUCS);
	}
	return TRUE;
}