// SetFixSafetyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSMonPro.h"
#include "AuxVentSafetyDlg.h"
#include "afxdialogex.h"
#include "LoginManagerDlg.h"
#include "CTSMonProDoc.h"

// CAuxVentSafetyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAuxVentSafetyDlg, CDialog)

CAuxVentSafetyDlg::CAuxVentSafetyDlg(CCTSMonProDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CAuxVentSafetyDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CAuxVentSafetyDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CAuxVentSafetyDlg::IDD3):
	(CAuxVentSafetyDlg::IDD),pParent
	)
	, m_strAuxTempLimitH(_T(""))
	, m_strAuxVoltLimitH(_T(""))
	, m_strAuxVoltLimitL(_T(""))
{
	m_pDoc = pDoc;
}

CAuxVentSafetyDlg::~CAuxVentSafetyDlg()
{
}

void CAuxVentSafetyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_AUX_TEMP_LIMIT_H, m_strAuxTempLimitH);
	DDX_Text(pDX, IDC_EDIT_AUX_VOLT_LIMIT_H, m_strAuxVoltLimitH);
	DDX_Text(pDX, IDC_EDIT_AUX_VOLT_LIMIT_L, m_strAuxVoltLimitL);
}


BEGIN_MESSAGE_MAP(CAuxVentSafetyDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAuxVentSafetyDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CAuxVentSafetyDlg 메시지 처리기입니다.




BOOL CAuxVentSafetyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.	
	int nSafetyFixTempHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", 80);
	int nSafetyCellVolHigh = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", 4600);
	int nSafetyCellVolLow = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltLow", 0);
	

	m_strAuxTempLimitH.Format("%d",nSafetyFixTempHigh);
	m_strAuxVoltLimitH.Format("%d",nSafetyCellVolHigh);
	m_strAuxVoltLimitL.Format("%d",nSafetyCellVolLow);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


//ksj 20200116 : 고정 안전 설정 저장
//저장시 로그인 창을 띄워 한번 더 체크하도록 한다.
//변경한 사용자 ID를 로그에 남긴다.
//변경한 값은 변경 전 후 값을 로그에 남긴다.
//SBC에 AuxSetData 로 설정 값 전송해야함.
void CAuxVentSafetyDlg::OnBnClickedOk()
{
	ASSERT(m_pDoc);

	CString strLog;

	//채널들이 사용중인지 확인. 모든 채널이 비가동중이어야 변경가능
	if(!CheckRunState()) return;

	//로그인창 띄워 한번 더 확인
	//CLoginManagerDlg LoginDlg;
	CLoginManagerDlg LoginDlg(PMS_SUPERVISOR); //ksj 20200203 : 관리자 이상 설정 변경 가능
	if(LoginDlg.DoModal() != IDOK) return;	

	//변경전 값 백없.
	CString strAuxTempLimitH_OLD = m_strAuxTempLimitH;
	CString strAuxVoltLimitH_OLD = m_strAuxVoltLimitH;
	CString strAuxVoltLimitL_OLD = m_strAuxVoltLimitL;

	//갱신
	UpdateData();
	
	//저장 시작
	
	strLog.Format("================고정안전기능설정===========================");
	m_pDoc->WriteLog(strLog);
	strLog.Format("ID : %s 사용자가 안전조건을 변경하였습니다.", LoginDlg.m_strUserID);
	m_pDoc->WriteLog(strLog);
	strLog.Format("====================================================");
	m_pDoc->WriteLog(strLog);

	//레지스트리에 기록
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "SafetyFixTempHIgh", atoi(m_strAuxTempLimitH));
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltHIgh", atoi(m_strAuxVoltLimitH));	
	AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "SafetyCellVoltLow", atoi(m_strAuxVoltLimitL));

	//변경 내역 로깅. (레지스트리 값 기준 로깅)
	strLog.Format("외부온도상한 변경 %s'C -> %s'C", strAuxTempLimitH_OLD, m_strAuxTempLimitH);
	m_pDoc->WriteLog(strLog);
	strLog.Format("외부전압상한 변경 %smV -> %smV", strAuxVoltLimitH_OLD, m_strAuxVoltLimitH);
	m_pDoc->WriteLog(strLog);
	strLog.Format("외부온도하한 변경 %smV -> %smV", strAuxVoltLimitL_OLD, m_strAuxVoltLimitL);
	m_pDoc->WriteLog(strLog);

	//온도 aux센서 기준 로깅
	strLog.Format("====================================================");
	m_pDoc->WriteLog(strLog);


	if(!UpdateAuxData())
	{

		strLog.Format("====================================================");
		m_pDoc->WriteLog(strLog);

		return;
	}

	strLog.Format("====================================================");
	m_pDoc->WriteLog(strLog);

	MessageBox("설정이 적용되었습니다.","안전설정",MB_ICONINFORMATION);

	CDialog::OnOK();
}


//ksj 20200116 : 가동중인 채널이 있는지 확인
BOOL CAuxVentSafetyDlg::CheckRunState(void)
{	
	//채널들이 사용중인지 확인. 모든 채널이 비가동중이어야 변경가능
	CString strLog;
	int nModuleCnt = m_pDoc->GetInstallModuleCount();

	for(int nModuleIdx=0; nModuleIdx < nModuleCnt; nModuleIdx++)
	{
		int nModuleID = m_pDoc->GetModuleID(nModuleIdx);
		CCyclerModule *pMD = m_pDoc->GetModuleInfo(nModuleID);
		if(!pMD) continue;

		int nTotalChCnt = pMD->GetTotalChannel();

		for(int nChIdx=0; nChIdx < nTotalChCnt; nChIdx++)
		{
			CCyclerChannel *pCH = pMD->GetChannelInfo(nChIdx);
			if(!pCH) continue;

			//가동중인 채널 있으면 리턴
			WORD wState = pCH->GetState();
			if(wState == PS_STATE_RUN
				|| wState == PS_STATE_PAUSE
				//|| wState == PS_STATE_LINE_OFF //임시 테스트
				)
			{
				strLog.Format("Module %d의 채널 %d가 사용중이므로 설정을 변경할 수 없습니다.",nModuleID, nChIdx+1);
				MessageBox(strLog,"안전설정",MB_ICONERROR);
				return FALSE;
			}
		}

	}

	return TRUE;
}

//ksj 20200116 : 설정 값을 전체 SBC에 전송한다.
BOOL CAuxVentSafetyDlg::UpdateAuxData(void)
{	

	STF_MD_AUX_SET_DATA mdAuxData[_SFT_MAX_MAPPING_AUX];
	CCyclerModule * pMD;
	int nRtn = 0;
	CString strLog;

	for(int nModuleIdx =0; nModuleIdx < m_pDoc->GetInstallModuleCount(); nModuleIdx++)
	{
		ZeroMemory(&mdAuxData, sizeof(STF_MD_AUX_SET_DATA)*_SFT_MAX_MAPPING_AUX);
		int nModuleID = m_pDoc->GetModuleID(nModuleIdx);

		pMD = m_pDoc->GetModuleInfo(nModuleID);

		//SFT_MD_SYSTEM_DATA* pSysData = ::SFTGetModuleSysData(nModuleID);		

		if(pMD == NULL) continue;
		if (pMD->GetState() == PS_STATE_LINE_OFF) continue;

		int nIndex = 0;

		for(int iSensorType = 0 ; iSensorType < pMD->GetMaxSensorType(); iSensorType++)
		{
			int nCount =0;
			switch(iSensorType)
			{
			case 0: nCount = pMD->GetMaxTemperature(); break;
			case 1: nCount = pMD->GetMaxVoltage(); break;
			case 2: nCount = pMD->GetMaxTemperatureTh(); break;
			}
			for(int iSensorIdx = 0 ; iSensorIdx < nCount; iSensorIdx++)
			{
				CChannelSensor * pSensor;
				if (iSensorType == PS_AUX_TYPE_TEMPERATURE_TH) 
					pSensor = pMD->GetAuxData(iSensorIdx+pMD->GetMaxVoltage(), iSensorType);
				else
					pSensor = pMD->GetAuxData(iSensorIdx, iSensorType);

				if(pSensor != NULL)
				{		
					if(pSensor->GetInstall() == TRUE)
					{
						STF_MD_AUX_SET_DATA auxData = pSensor->GetAuxData();
						switch(iSensorType)
						{
						case PS_AUX_TYPE_TEMPERATURE:
							if(auxData.auxChNo == 1)
							{
								strLog.Format("변경전 온도상한 %d, 온도하한 %d",auxData.vent_upper,auxData.vent_lower);
								m_pDoc->WriteLog(strLog);
							}
							auxData.vent_upper = atoi(m_strAuxTempLimitH)*1000;
							auxData.vent_lower = 0;

							if(auxData.auxChNo == 1)
							{
								strLog.Format("변경후 온도상한 %d, 온도하한 %d",auxData.vent_upper,auxData.vent_lower);
								m_pDoc->WriteLog(strLog);
							}
							break;		
						case PS_AUX_TYPE_VOLTAGE:
							if(auxData.auxChNo == 1)
							{
								strLog.Format("변경전 전압상한 %d, 전압하한 %d",auxData.vent_upper,auxData.vent_lower);
								m_pDoc->WriteLog(strLog);
							}

							auxData.vent_upper = atoi(m_strAuxVoltLimitH)*1000;
							auxData.vent_lower = atoi(m_strAuxVoltLimitL)*1000;

							if(auxData.auxChNo == 1)
							{
								strLog.Format("변경후 전압상한 %d, 전압하한 %d",auxData.vent_upper,auxData.vent_lower);
								m_pDoc->WriteLog(strLog);
							}
							break;
						case PS_AUX_TYPE_TEMPERATURE_TH:	
							if(auxData.auxChNo == 1)
							{
								strLog.Format("변경전 온도TH상한 %d, 온도TH하한 %d",auxData.vent_upper,auxData.vent_lower);
								m_pDoc->WriteLog(strLog);
							}
							auxData.vent_upper = atoi(m_strAuxTempLimitH)*1000000;
							auxData.vent_lower = 0;	
							if(auxData.auxChNo == 1)
							{
								strLog.Format("변경후 온도TH상한 %d, 온도TH하한 %d",auxData.vent_upper,auxData.vent_lower);
								m_pDoc->WriteLog(strLog);
							}
							break;
						}
					
						memcpy(&mdAuxData[nIndex], &auxData, sizeof(STF_MD_AUX_SET_DATA));

						//#ifdef _PARALLEL_FORCE 전처리기 주석처리//yulee 20190705 강제병렬 옵션 처리 Mark
						if(g_AppInfo.iPType==1)
						{
							//ksj 20171025 : 주석처리
							if (mdAuxData[nIndex].chNo == 2) 
							{
								mdAuxData[nIndex].chNo = 3;		//ljb 20170630 add
							}
						}
						//#endif

						if (iSensorType == PS_AUX_TYPE_TEMPERATURE_TH)
						{
							//mdAuxData[nIndex].auxChNo = mdAuxData[nIndex].auxChNo + pMD->GetMaxVoltage(); 
							mdAuxData[nIndex].auxChNo = iSensorIdx + 1 + pMD->GetMaxVoltage(); // ksj 20160624
						}

						nIndex++;
					
					}
				}
			}
		}

		if(!m_pDoc->SendAuxDatatoModule(nModuleID,mdAuxData)) //SBC에 전송.
		{
			AfxMessageBox("전송실패");
			return FALSE;
		}

		//SBC에 전송 성공했으면 GUI 메모리상에도 센서값 적용.
		for(int iAuxIdx=0;iAuxIdx<_SFT_MAX_MAPPING_AUX;iAuxIdx++)
		{
			int nAuxSensorIndex = mdAuxData[iAuxIdx].auxChNo-1;
			int nAuxSensorType = mdAuxData[iAuxIdx].auxType;

			//GUI 메모리에도 복사. GUI 화면에서 SBC에서 실시간 값 가져오는게 아니라 GUI 자체에 저장하고 있는 값을 표시함.
			//그러므로 지금 SBC에 전송하는 값을 GUI메모리에도 써줘야됨.
			CChannelSensor* pSensor = pMD->GetAuxData(nAuxSensorIndex,nAuxSensorType);
			if(pSensor) pSensor->SetAuxData(&mdAuxData[iAuxIdx]);
		}
		pMD->ApplyAuxData();
	}

	return TRUE;
}
