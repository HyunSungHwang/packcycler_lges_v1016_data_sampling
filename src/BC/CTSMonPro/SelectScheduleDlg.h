#if !defined(AFX_SELECTSCHEDULEDLG_H__0FFE8FEA_86B8_4D8F_9157_980942385349__INCLUDED_)
#define AFX_SELECTSCHEDULEDLG_H__0FFE8FEA_86B8_4D8F_9157_980942385349__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectScheduleDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectScheduleDlg dialog

#include "CTSMonProDoc.h"
#include "XPButton.h"

class CSelectScheduleDlg : public CDialog
{
// Construction
public:
	
	CSelectScheduleDlg(CCTSMonProDoc *pDoc, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectScheduleDlg)
	enum { IDD = IDD_SCHEDULE_LIST_DLG , IDD2 = IDD_SCHEDULE_LIST_DLG_ENG , IDD3 = IDD_SCHEDULE_LIST_DLG_PL };
	CXPButton	m_btnEdit;
	CXPButton	m_btnCancel;
	CXPButton	m_btnOK;
	CListCtrl	m_ctrlScheduleList;
	CListCtrl	m_ctrlModelList;
	//}}AFX_DATA

	CCTSMonProDoc *m_pDoc;
	long m_nSelModelID;
	long m_nSelScheduleID;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectScheduleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateSelection();
	BOOL RequeryScheduleList(UINT nModelID);
	BOOL RequeryModelList();
	void InitList();

	// Generated message map functions
	//{{AFX_MSG(CSelectScheduleDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnEditButton();
	afx_msg void OnClickModelList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickScheduleList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTSCHEDULEDLG_H__0FFE8FEA_86B8_4D8F_9157_980942385349__INCLUDED_)
