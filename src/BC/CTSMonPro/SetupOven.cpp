// SetupOven.cpp : implementation file
//

#include "stdafx.h" 
 

#include "ctsmonpro.h"
#include "SetupOven.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetupOven dialog


CSetupOven::CSetupOven(CCTSMonProDoc * pDoc, int * nPortLine, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSetupOven::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSetupOven::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSetupOven::IDD3):
	(CSetupOven::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSetupOven)
	m_bUseDoorOpenSensor = FALSE;
	//}}AFX_DATA_INIT

	m_pDoc = pDoc;
	m_nPortLine = *nPortLine;
}


void CSetupOven::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetupOven)
	DDX_Control(pDX, IDC_LIST_OVENCH_MAP, m_ctrlOvenChMapList);
	DDX_Control(pDX, IDC_LIST_CHANNEL, m_ctrlChList);
	DDX_Control(pDX, IDC_COMBO_OVEN, m_ctrlOvenList);
	DDX_Check(pDX, IDC_CHK_DOOROPENSENSOR, m_bUseDoorOpenSensor);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetupOven, CDialog)
	//{{AFX_MSG_MAP(CSetupOven)
	ON_BN_CLICKED(IDC_BTN_POP_OVEN, OnBtnPopOven)
	ON_BN_CLICKED(IDC_BTN_PUSH_OVEN, OnBtnPushOven)
	ON_CBN_SELCHANGE(IDC_COMBO_OVEN, OnSelchangeComboOven)
	ON_NOTIFY(NM_SETFOCUS, IDC_LIST_CHANNEL, OnSetfocusListChannel)
	ON_NOTIFY(NM_SETFOCUS, IDC_LIST_OVENCH_MAP, OnSetfocusListOvenchMap)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetupOven message handlers

BOOL CSetupOven::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//////////////////////////////////////////////////////////////////////////////////////////////
	m_pCtrlOven = NULL;
	if (m_nPortLine == 1 ) m_pCtrlOven = &m_pDoc->m_ChamberCtrl.m_ctrlOven1; 
	else if (m_nPortLine == 2 ) m_pCtrlOven = &m_pDoc->m_ChamberCtrl.m_ctrlOven2; 

	InitList();
	LoadOvenData();
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSetupOven::OnOK() 
{	
	SaveOvenData();
	
	CDialog::OnOK();
}

void CSetupOven::OnBtnPopOven() 
{	
}

void CSetupOven::OnBtnPushOven() 
{

	CString strErrorMsg;
	if(m_ctrlOvenList.GetCount() == 0)
	{
		strErrorMsg.Format(Fun_FindMsg("OnBtnPushOven_msg1","IDD_SETUP_OVEN_DLG"));
		//@ strErrorMsg.Format("등록된 Oven이 없습니다.");
		AfxMessageBox(strErrorMsg);
		return;
	}
	CString strArrow;
	GetDlgItem(IDC_BTN_PUSH_OVEN)->GetWindowText(strArrow);

	if(strArrow == ">>")
	{
		if(m_ctrlChList.GetSelectedCount() <= 0)
		{		
			strErrorMsg.Format(Fun_FindMsg("OnBtnPushOven_msg2","IDD_SETUP_OVEN_DLG"));
			//@ strErrorMsg.Format("채널을 선택해야합니다.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		//동작중인 채널... 동작중인 Oven은 변경이 불가능하도록 해야한다.

		for(int i = 0 ; i < m_ctrlChList.GetItemCount() ;  i++) 
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		

			CString strModule = m_ctrlChList.GetItemText(i, 0);

			int nModuleID = atoi(strModule.Mid(1));
			CString strCh = m_ctrlChList.GetItemText(i, 1);
			int chNum		= atoi(strCh.Mid(2));
			CCyclerModule * pMD = m_pDoc->GetModuleInfo(nModuleID);
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
//20090115 KHS/////////////////////////////
#ifdef _DEBUG
				if(pChInfo == NULL) return;
#else
				if(pChInfo == NULL) continue;
#endif
///////////////////////////////////////////
			if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
			{
				AfxMessageBox(Fun_FindMsg("OnBtnPushOven_msg3","IDD_SETUP_OVEN_DLG"));
				//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
				return;
			}
					
		
			if(uState & LVIS_SELECTED)
			{
				int nCount = m_ctrlOvenChMapList.GetItemCount();
			   m_ctrlOvenChMapList.InsertItem(nCount, strModule);
			   m_ctrlOvenChMapList.SetItemText(nCount, 1, strCh);
			}
		}

		for(int i = m_ctrlChList.GetItemCount() ; i >=0  ;  i--) 
		{
			UINT uState = m_ctrlChList.GetItemState(i, LVIS_SELECTED); 		
		
			if(uState & LVIS_SELECTED)
			{
				m_ctrlChList.DeleteItem(i);
			}
		}
	}
	else
	{
		if(m_ctrlOvenChMapList.GetSelectedCount() <= 0)
		{		
			CString strErrorMsg;
			strErrorMsg.Format(Fun_FindMsg("OnBtnPushOven_msg4","IDD_SETUP_OVEN_DLG"));
			//@ strErrorMsg.Format("채널을 선택해야합니다.");
			AfxMessageBox(strErrorMsg);
			return;
		}

		//동작중인 채널... 동작중인 Oven은 변경이 불가능하도록 해야한다.
		for(int i = 0 ; i < m_ctrlOvenChMapList.GetItemCount() ;  i++) 
		{
			UINT uState = m_ctrlOvenChMapList.GetItemState(i, LVIS_SELECTED); 		

			CString strModule = m_ctrlOvenChMapList.GetItemText(i, 0);

			int nModuleID = atoi(strModule.Mid(1));
			CString strCh = m_ctrlOvenChMapList.GetItemText(i, 1);
			int chNum		= atoi(strCh.Mid(2));
			CCyclerModule * pMD = m_pDoc->GetModuleInfo(nModuleID);
			CCyclerChannel * pChInfo = pMD->GetChannelInfo(chNum-1);
//20090115 KHS/////////////////////////////
#ifdef _DEBUG
			if(pChInfo == NULL) return;
#else
			if(pChInfo == NULL) continue;
#endif
///////////////////////////////////////////
			if(pChInfo->GetState() == PS_STATE_RUN || pChInfo->GetState() == PS_STATE_PAUSE) 
			{
				AfxMessageBox(Fun_FindMsg("OnBtnPushOven_msg5","IDD_SETUP_OVEN_DLG"));
				//@ AfxMessageBox("동작중인 채널은 변경할 수 없습니다.");
				return;
			}
					
		
			if(uState & LVIS_SELECTED)
			{
				int nCount = m_ctrlChList.GetItemCount();
			   m_ctrlChList.InsertItem(nCount, strModule);
			   m_ctrlChList.SetItemText(nCount, 1, strCh);
			}
		}

		for(int i = m_ctrlOvenChMapList.GetItemCount() ; i >= 0 ;  i--) 
		{
			UINT uState = m_ctrlOvenChMapList.GetItemState(i, LVIS_SELECTED); 		
		
			if(uState & LVIS_SELECTED)
			{
				m_ctrlOvenChMapList.DeleteItem(i);
			}
		}

	}

	//m_ctrlOvenChMapList 에 속한 채널을 OvenCtrl에 등록한다.
	CDWordArray awCh;
	DWORD dwData;
	int nOven = m_ctrlOvenList.GetCurSel();
	for(int i = 0 ; i < m_ctrlOvenChMapList.GetItemCount() ;  i++) 
	{
		CString strModule = m_ctrlOvenChMapList.GetItemText(i, 0);

		int nModuleID = atoi(strModule.Mid(1));
		CString strCh = m_ctrlOvenChMapList.GetItemText(i, 1);
		int chNum		= atoi(strCh.Mid(2));
		dwData = MAKELONG(chNum-1, nModuleID);
		awCh.Add(dwData);		
	}	
	m_OvenList.SetChannelMapArray(nOven, awCh);		
	
}

void CSetupOven::InitList()
{
	CRect rect;
	m_ctrlChList.GetClientRect(&rect);
	int nWidth = rect.Width() / 2;

	int n_dummy =  m_ctrlChList.InsertColumn(0, "Dummy", LVCFMT_CENTER, 100);	//Dummy Column
	m_ctrlChList.InsertColumn(1, "Unit", LVCFMT_CENTER, nWidth);
	m_ctrlChList.InsertColumn(2, Fun_FindMsg("InitList_msg1","IDD_SETUP_OVEN_DLG"), LVCFMT_CENTER, rect.Width() - nWidth);
    //@ m_ctrlChList.InsertColumn(2, "채널", LVCFMT_CENTER, rect.Width() - nWidth);
	m_ctrlChList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_ctrlChList.DeleteColumn(n_dummy);		//Remove Dummy Column
	
	m_ctrlOvenChMapList.GetClientRect(&rect);
	nWidth = rect.Width() / 2;

	n_dummy =  m_ctrlOvenChMapList.InsertColumn(0, "Dummy", LVCFMT_CENTER, 100);	//Dummy Column
	
	m_ctrlOvenChMapList.InsertColumn(1, "Unit", LVCFMT_CENTER, nWidth);
	m_ctrlOvenChMapList.InsertColumn(2, Fun_FindMsg("InitList_msg2","IDD_SETUP_OVEN_DLG"), LVCFMT_CENTER, rect.Width() - nWidth);
	//@ m_ctrlOvenChMapList.InsertColumn(2, "채널", LVCFMT_CENTER, rect.Width() - nWidth);
	m_ctrlOvenChMapList.DeleteColumn(n_dummy);		//Remove Dummy Column

	m_ctrlOvenChMapList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
}

void CSetupOven::LoadOvenData()
{
	int i = 0;
	for(i = 0 ; i < m_pCtrlOven->GetOvenCount(); i++)
	{
		CDWordArray adwCh; 

		m_pCtrlOven->GetChannelMapArray(i, adwCh);
		m_OvenList.SetChannelMapArray(i,adwCh);
		m_OvenList.SetUseOven(i, m_pCtrlOven->GetUseOven(i));			

//		memcpy(m_OvenList.m_SerialConfig, m_pDoc->m_ctrlOven.m_SerialConfig, sizeof(SERIAL_CONFIG)*MAX_OVEN_COUNT);
//		memcpy(m_OvenList.m_SerialConfig, m_pDoc->m_ctrlOven.m_SerialConfig, sizeof(SERIAL_CONFIG));
	}
	
	m_OvenList.SetOvenCount(i);

// 	int nSelModel = AfxGetApp()->GetProfileInt(OVEN_SETTING, "SelectModel",0);
// 	m_bUseDoorOpenSensor = AfxGetApp()->GetProfileInt(OVEN_SETTING, "UseDoorOpenSensor", 0);
	
//	m_ctrlOvenModel.SetCurSel(nSelModel);
	UpdateOvenList();
	UpdateOvenChMapList();
	UpdateRemainChList();
}

void CSetupOven::UpdateOvenChMapList()
{
	CDWordArray adwCh;
	CString strTemp;
	
	if(m_ctrlOvenList.GetCurSel() >= 0)
	{
		m_ctrlOvenChMapList.DeleteAllItems();
		m_OvenList.GetChannelMapArray(m_ctrlOvenList.GetCurSel(), adwCh);

		for(int a = 0; a<adwCh.GetSize(); a++)
		{
			DWORD dwData = adwCh.GetAt(a);
			
			strTemp.Format("M%02d", HIWORD(dwData));
			m_ctrlOvenChMapList.InsertItem(a, strTemp);

			strTemp.Format("CH%02d", LOWORD(dwData)+1);
			m_ctrlOvenChMapList.SetItemText(a, 1, strTemp);
		}
	}
}

void CSetupOven::UpdateOvenList()
{
	CString strTemp;
	for(int i = 0 ; i < m_OvenList.GetOvenCount(); i++)
	{
		strTemp.Format("Oven %d", i+1);
		m_ctrlOvenList.AddString(strTemp);
	}

	if(m_ctrlOvenList.GetCount() > 0)
	{
		m_ctrlOvenList.SetCurSel(0);
//		EnableOvelCtrl(m_OvenList.GetUseOven(0));
		GetDlgItem(IDC_BTN_PUSH_OVEN)->EnableWindow(TRUE);
	}
}

void CSetupOven::OnSelchangeComboOven() 
{
	//선택된 Oven이 동작중이면 변경할수 없다는 표시를 해야한다.
	UpdateOvenChMapList();

// 	if(m_ctrlOvenList.GetCurSel() >= 0)
// 		EnableOvelCtrl(m_OvenList.GetUseOven(m_ctrlOvenList.GetCurSel()));	
	
}

void CSetupOven::UpdateRemainChList()
{
	//Oven에 포함된 모든 Ch 정보를 구한다.
	CDWordArray adwCh;
	for(int a = 0 ; a < m_OvenList.GetOvenCount(); a++)
	{
		CDWordArray adwTemp;
		m_OvenList.GetChannelMapArray(a, adwTemp);

		for(int b = 0; b<adwTemp.GetSize(); b++)
		{
			DWORD dwData = adwTemp.GetAt(b);			
			adwCh.Add(dwData);
		}
	}

	//Oven에 포함된 Ch를 제외한 모든 Ch 정보를 리스트에 입력한다.
	CDWordArray adwUpdateCh;
	
	for(int i = 0 ; i < m_pDoc->GetInstallModuleCount(); i++)
	{

		int nModuleID = m_pDoc->GetModuleID(i);
		CCyclerModule * pMD = m_pDoc->GetModuleInfo(nModuleID);
		for(int nCh = 0 ; nCh < pMD->GetTotalChannel(); nCh++)
		{
			BOOL bFind = FALSE;
			DWORD dwData;
			for(int j = 0 ; j < adwCh.GetSize(); j++)
			{
				dwData = adwCh.GetAt(j);		
				int nCkModuleID = HIWORD(dwData);
				int nCkChNo = LOWORD(dwData);
				if(i+1 == nCkModuleID && nCh == nCkChNo)
				{
					bFind = TRUE;
					break;
				}
			}

			if(bFind == FALSE)
			{
				dwData = MAKELONG(nCh, i+1);
				adwUpdateCh.Add(dwData);
			}			
		}
	}

	CString strTemp;
	for(int i = 0 ; i < adwUpdateCh.GetSize(); i++)
	{
		DWORD dwData = adwUpdateCh.GetAt(i);
			
		strTemp.Format("M%02d", HIWORD(dwData));
		m_ctrlChList.InsertItem(i, strTemp);

		strTemp.Format("CH%02d", LOWORD(dwData)+1);
		m_ctrlChList.SetItemText(i, 1, strTemp);
	}
}

void CSetupOven::OnSetfocusListChannel(NMHDR* pNMHDR, LRESULT* pResult) 
{
	GetDlgItem(IDC_BTN_PUSH_OVEN)->SetWindowText(">>");
	
	*pResult = 0;
}

void CSetupOven::OnSetfocusListOvenchMap(NMHDR* pNMHDR, LRESULT* pResult) 
{
	GetDlgItem(IDC_BTN_PUSH_OVEN)->SetWindowText("<<");
	
	*pResult = 0;
}

void CSetupOven::SaveOvenData()
{
	UpdateData(TRUE);
	DWORD dwItem[MAX_OVEN_COUNT][64];
	memset(dwItem, 0, sizeof(dwItem));	//존재하지 않는 숫자로 Setting한다.
	long	lUseOven = 0;

	int i = 0;

//	m_pDoc->m_ctrlOven.DeleteAllOven();
	for(int i = 0 ; i < m_OvenList.GetOvenCount(); i++)
	{
		CDWordArray adwCh; 

		m_OvenList.GetChannelMapArray(i, adwCh);
		m_pCtrlOven->SetChannelMapArray(i,adwCh);

		for(int a = 0 ; a < adwCh.GetSize(); a++)
		{
			dwItem[i][a] = adwCh.GetAt(a);
		}

//		m_pDoc->m_ctrlOven.SetUseOven(i, m_OvenList.GetUseOven(i));

//		lUseOven = lUseOven << 1;		//항상 0
//		lUseOven |= m_OvenList.GetUseOven(i);

		//20081103 Oven 사용 여부를 Bit Flag를 이용하여 저장 
// 		if(m_OvenList.GetUseOven(i))
// 		{
// 			lUseOven |= (0x01 << i);
// 		}	
	}
//	int nSelModel = m_ctrlOvenModel.GetCurSel();
//	m_pDoc->m_ctrlOven.SetOvenModelType(nSelModel);

//	m_pDoc->m_ctrlOven.SetOvenCount(m_OvenList.GetOvenCount());	

//	memcpy(&m_OvenList.m_SerialConfig, &m_pDoc->m_ctrlOven.m_SerialConfig, sizeof(SERIAL_CONFIG)*MAX_OVEN_COUNT);
//	memcpy(&m_pDoc->m_ctrlOven.m_SerialConfig, &m_OvenList.m_SerialConfig, sizeof(SERIAL_CONFIG)*MAX_OVEN_COUNT);

//	m_pDoc->m_ctrlOven.LoadSerilaConfig();		// 2009_03_02 kky 시리얼 환경 로드

//	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "UseOvenCh", lUseOven);

	if (m_nPortLine == 1) AfxGetApp()->WriteProfileBinary(REG_SERIAL_CONFIG3, "OvenChMap", (LPBYTE)dwItem, sizeof(dwItem));
	if (m_nPortLine == 2) AfxGetApp()->WriteProfileBinary(REG_SERIAL_CONFIG4, "OvenChMap", (LPBYTE)dwItem, sizeof(dwItem));
//	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "SelectModel", nSelModel);
//	AfxGetApp()->WriteProfileInt(OVEN_SETTING, "UseDoorOpenSensor", m_bUseDoorOpenSensor);
}