#if !defined(AFX_READYRUNDLG_H__90AF6257_F198_43FA_8D37_93EE2AD28C10__INCLUDED_)
#define AFX_READYRUNDLG_H__90AF6257_F198_43FA_8D37_93EE2AD28C10__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReadyRunDlg.h : header file
//

#include  "CTSMonProDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CReadyRunDlg dialog

class CReadyRunDlg : public CDialog
{
// Construction
public:

	CReadyRunDlg(CCTSMonProDoc *pDoc, CWnd* pParent = NULL);   // standard constructor
//	CReadyRunDlg(CWnd* pParent = NULL);   // standard constructor
	CCTSMonProDoc *m_pDoc;
	int m_nReayTime;
	int m_nProgPos;


// Dialog Data
	//{{AFX_DATA(CReadyRunDlg)
	enum { IDD = IDD_WORK_READY_DLG , IDD2 = IDD_WORK_READY_DLG_ENG ,IDD3 = IDD_WORK_READY_DLG_PL };
	CProgressCtrl	m_CtrlProgress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReadyRunDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CReadyRunDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_READYRUNDLG_H__90AF6257_F198_43FA_8D37_93EE2AD28C10__INCLUDED_)
