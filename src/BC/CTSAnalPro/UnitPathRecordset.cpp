// UnitPathRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "UnitPathRecordset.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnitPathRecordset

IMPLEMENT_DYNAMIC(CUnitPathRecordset, CDaoRecordset)

CUnitPathRecordset::CUnitPathRecordset(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CUnitPathRecordset)
	m_No = 0;
	m_Path = _T("");
	m_Comm = FALSE;
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CUnitPathRecordset::GetDefaultDBName()
{
	return _T(m_strPath);
}

CString CUnitPathRecordset::GetDefaultSQL()
{
	return _T("[UnitInfo]");
}

void CUnitPathRecordset::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CUnitPathRecordset)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Byte(pFX, _T("[No]"), m_No);
	DFX_Text(pFX, _T("[Path]"), m_Path);
	DFX_Bool(pFX, _T("[Comm]"), m_Comm);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CUnitPathRecordset diagnostics

#ifdef _DEBUG
void CUnitPathRecordset::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CUnitPathRecordset::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG

void CUnitPathRecordset::SetDefaultPath(CString strPath)
{
	m_strPath = strPath + "\\sysinfo.mdb";
}
