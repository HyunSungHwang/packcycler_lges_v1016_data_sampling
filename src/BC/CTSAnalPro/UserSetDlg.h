#if !defined(AFX_USERSETDLG_H__91ACB43A_7F64_4A4D_A31B_E5B885189D1C__INCLUDED_)
#define AFX_USERSETDLG_H__91ACB43A_7F64_4A4D_A31B_E5B885189D1C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserSetDlg dialog

class CUserSetDlg : public CDialog
{
// Construction
public:	
	CUserSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUserSetDlg)
	enum { IDD = IDD_USER_SET_DLG , IDD2 = IDD_USER_SET_DLG_ENG };
	CComboBox	m_ctrlCapaDecimal;
	CComboBox	m_ctrlCapaUnit;
	CCheckListBox	m_itemList;
	CComboBox	m_ctrlWhDecimal;
	CComboBox	m_ctrlWDecimal;
	CComboBox	m_ctrlWhUnit;
	CComboBox	m_ctrlWUnit;
	CLabel	m_ctrFontResult;
	CComboBox	m_ctrlTimeUnit;
	CComboBox	m_ctrlCDecimal;
	CComboBox	m_ctrlIDecimal;
	CComboBox	m_ctrlVDecimal;
	CComboBox	m_ctrlCUnit;
	CComboBox	m_ctrlIUnit;
	CComboBox	m_ctrlVUnit;
	int		m_nTrayColSize;
	BOOL	m_bDispCapSum;
	int		m_fTime1;
	float	m_fTime2;
	BOOL	m_bUserImpTime;
	int		m_nExcelOp;
	int		m_nExcelSize;
	BOOL	bAuxListAllFlag; //yulee 20180729
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	LOGFONT		m_afont;
	// Generated message map functions
	//{{AFX_MSG(CUserSetDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnFontSetBtn();
	afx_msg void OnUpButton();
	afx_msg void OnDownButton();
	afx_msg void OnCheck1();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnButExportCheckAll();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERSETDLG_H__91ACB43A_7F64_4A4D_A31B_E5B885189D1C__INCLUDED_)
