/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: CTSAnalPro.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CCTSAnalProApp class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// CTSAnalPro.h : main header file for the CTSAnalPro application
//

#if !defined(AFX_CTSAnalPro_H__13FE5EA4_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CTSAnalPro_H__13FE5EA4_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

// #include

#include "resource.h"       // main symbols
#include "WorkSTSignAnalDlg.h"

// #define

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProApp:
// See CTSAnalPro.cpp for the implementation of this class
//

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CCTSAnalProApp
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CCTSAnalProApp : public CWinApp
{
public:
	CMultiDocTemplate* m_pDataTemplate;
	CMultiDocTemplate* m_pGraphTemplate;
	CCTSAnalProApp();

	BOOL file_Finder(CString path);
	BOOL file_AddWrite(CString filename,CString data);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSAnalProApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
	CWorkSTSignAnalDlg * m_pWorkSTSignAnalDlg; //yulee 20181002
	//{{AFX_MSG(CCTSAnalProApp)
	afx_msg void OnAppAbout();
	afx_msg void OnShowMonitor();
	afx_msg void OnShowData();
	afx_msg void OnShowGraph();
	afx_msg void OnShowHelp();
	afx_msg void OnWorkSTShow();
	afx_msg void OnUpdateShowHelp(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowMonitor(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowGraph(CCmdUI* pCmdUI);

	afx_msg void OnMenuLangEng();
	afx_msg void OnMenuLangKor();
	afx_msg void OnUpdateLangEng(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLangKor(CCmdUI* pCmdUI);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
private:
	CDWordArray m_dwaSelTableArray;				//lmh 20120207
	CMultiDocTemplate* m_pMonitorTemplate;
	CMultiDocTemplate* m_pHelpTemplate;
	BOOL               m_bInitial;

// Operations
	public:
		BOOL IsInitialState() { return m_bInitial;};
		void ConfirmInitialOperation() { m_bInitial=FALSE;};

// Static
public:
	void SetSelTableIndexArray(CDWordArray &dwSelArray);		//lmh 20120207
	void CreateNewGraphFrame(CString strPath);					//lmh 20120207


	BOOL ExecuteExcel(CString strFileName);
	CString GetExcelPath();
	void SetResultDataPath(CString strPath);
	char m_szCurrentDirectory [512];	//ljb 2011517 128 -> 512
	void InitDBPath();
	static CString m_strOriginFolderPath;
	static void SetOriginFolderPath(LPCTSTR str) { m_strOriginFolderPath=str;};
};

extern CCTSAnalProApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSAnalPro_H__13FE5EA4_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
