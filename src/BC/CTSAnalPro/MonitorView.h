/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: MonitorView.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CMonitorView class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_MONITORVIEW_H__3AAEBDE1_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_MONITORVIEW_H__3AAEBDE1_F61A_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MonitorView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMonitorView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

// #include

//#include "../PSModule/ItemOnDisplay.h"
//#include "ProgressWnd.h"

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CMonitorView
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------

#define	WM_ERROR_REPORT				WM_USER + 120

class CMonitorView : public CFormView
{
protected:
	CMonitorView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CMonitorView)
public:
	CMonitorDoc* GetDocument();

// Form Data
public:
	//{{AFX_DATA(CMonitorView)
	enum { IDD = IDD_MONITOR_FORM };
	CListCtrl	m_ChListCtrl;
	//}}AFX_DATA

// Attributes
public:
//	CProgressWnd	m_wndProg;
	CImageList*      m_pChStsLargeImage;
	CImageList*      m_pChStsSmallImage;
//	CItemOnDisplay*  m_pItem;

	CString			m_strLogFileName;
	CStdioFile		m_LogFile;

// Operations
public:
	BOOL SaveFinalView();
	void GetSelItemList(CList<int, int&>* pList);
	BOOL IsThisCommandPossible(BYTE command);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMonitorView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:	
	BOOL ListUpChannel();
	void InitListCtrl();
	void WriteLog(CString strLogMsg, bool bAutoDisplayTime = true);
	void ResetCommandProgress();
	void SetCommandProgress(int nUnitNo);
	virtual ~CMonitorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	// Generated message map functions
protected:
	//{{AFX_MSG(CMonitorView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTestStart();
	afx_msg void OnCommandPause();
	afx_msg void OnCommandRetry();
	afx_msg void OnCommandShowPulse();
	afx_msg void OnCommandStopAfterCurrentCycle();
	afx_msg void OnCommandStopAfterCurrentMode();
	afx_msg void OnCommandStopAfterCurrentStep();
	afx_msg void OnCommandStopNow();
	afx_msg void OnManagePattern();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnUpdateTestStart(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCommandPause(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCommandRetry(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCommandShowPulse(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCommandStopAfterCurrentCycle(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCommandStopAfterCurrentMode(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCommandStopAfterCurrentStep(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCommandStopNow(CCmdUI* pCmdUI);
	afx_msg void OnManageNetwork();
	afx_msg void OnUpdateManageNetwork(CCmdUI* pCmdUI);
	afx_msg void OnManageData();
	afx_msg void OnUpdateCommandStop(CCmdUI* pCmdUI);
	afx_msg void OnManageItemtobedisplayed();
	afx_msg void OnManagePacktype();
	afx_msg void OnUpdateManagePacktype(CCmdUI* pCmdUI);
	afx_msg void OnPathManage();
	afx_msg void OnNetstateView();
	afx_msg void OnDblclkChList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	afx_msg LRESULT OnErrorReportMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in aMonitorView.cpp
inline CMonitorDoc* CMonitorView::GetDocument()
   { return (CMonitorDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MONITORVIEW_H__3AAEBDE1_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
