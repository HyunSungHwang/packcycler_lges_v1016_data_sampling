#if !defined(__TREEBAR_H__)
#define __TREEBAR_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// Treebar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTreeBar window

#ifndef baseCMyBar
#define baseCMyBar CSizingControlBarG
#endif

#include "AdpTreeCtrl.h"

class CTreeBar : public baseCMyBar
{
// Construction
public:
	CTreeBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	CWnd * GetClientWnd();
	virtual ~CTreeBar();

protected:
	CAdpTreeCtrl	m_wndTree;
	CFont	m_font;

	// Generated message map functions
protected:
	//{{AFX_MSG(CTreeBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(__TREEBAR_H__)
