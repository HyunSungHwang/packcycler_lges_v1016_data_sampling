#if !defined(AFX_DATASELECTDLG_H__83409306_A921_414E_83B9_24E910DB2177__INCLUDED_)
#define AFX_DATASELECTDLG_H__83409306_A921_414E_83B9_24E910DB2177__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataSelectDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataSelectDlg dialog

class CDataSelectDlg : public CDialog
{
// Construction
public:
	CDataSelectDlg(CWnd* pParent = NULL);   // standard constructor
	long m_lMax;

// Dialog Data
	//{{AFX_DATA(CDataSelectDlg)
	enum { IDD = IDD_DATA_SELECT_DLG , IDD2 = IDD_DATA_SELECT_DLG_ENG , IDD3 = IDD_DATA_SELECT_DLG_PL };
	UINT	m_lFrom;
	long	m_lTo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataSelectDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATASELECTDLG_H__83409306_A921_414E_83B9_24E910DB2177__INCLUDED_)
