// Ba_Tester.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CTSAnalPro.h"

#include "MainFrm.h"

#include "ChildFrm.h"
#include "CTSAnalProDoc.h"
#include "CTSAnalProView.h"

#include "MonitorFrame.h"
#include "MonitorDoc.h"
#include "MonitorView.h"

#include "HelpFrame.h"
#include "HelpDoc.h"
#include "HelpView.h"

#include "DataFrame.h"
#include "DataDoc.h"
#include "DataView.h"

#include "Splash.h"
#include "Win32Process.h"

#include "UnitPathRecordset.h"
#include "WorkSTSignAnalDlg.h"

#define STRING_UNIT1			"Z:\\"
#define STRING_UNIT2			"Y:\\"
//#define STRING_UNIT1			"C:\\Unit1\\Share"
//#define STRING_UNIT2			"C:\\Unit2\\Share"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProApp

BEGIN_MESSAGE_MAP(CCTSAnalProApp, CWinApp)
	//{{AFX_MSG_MAP(CCTSAnalProApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_SHOW_MONITOR, OnShowMonitor)
	ON_COMMAND(ID_SHOW_DATA, OnShowData)
	ON_COMMAND(ID_SHOW_GRAPH, OnShowGraph)
	ON_UPDATE_COMMAND_UI(ID_SHOW_MONITOR, OnUpdateShowMonitor)
	ON_UPDATE_COMMAND_UI(ID_SHOW_GRAPH, OnUpdateShowGraph)
	ON_COMMAND(ID_WORKST_WND, OnWorkSTShow)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	ON_COMMAND(ID_MENU_LANG_KOR, OnMenuLangKor)
	ON_COMMAND(ID_MENU_LANG_ENG, OnMenuLangEng)
	ON_UPDATE_COMMAND_UI(ID_MENU_LANG_KOR, OnUpdateLangKor)
	ON_UPDATE_COMMAND_UI(ID_MENU_LANG_ENG, OnUpdateLangEng)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProApp construction

CCTSAnalProApp::CCTSAnalProApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_pMonitorTemplate = NULL;
	m_pHelpTemplate    = NULL;
	m_bInitial         = TRUE;
	m_pWorkSTSignAnalDlg = NULL;
}

CString CCTSAnalProApp::m_strOriginFolderPath = "";

/////////////////////////////////////////////////////////////////////////////
// The one and only CCTSAnalProApp object

CCTSAnalProApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProApp initialization

BOOL CCTSAnalProApp::InitInstance()
{
	AfxGetModuleState()->m_dwVersion = 0x0601;
	AfxSocketInit();
	AfxOleInit();		//for clipboard ftn
	GXInit();

	// 동일한 Program이 이미 실행되고 있는지 확인 
/*	CWin32Process win32proc;

	if(!win32proc.Init()) {
		AfxMessageBox(win32proc.GetLastError());
		return FALSE;
	}

	if(win32proc.IsWinNT() ) {
		//AfxMessageBox("This program cannot run on WinNT");
		//return FALSE;
	}

	if (!win32proc.EnumAllProcesses())
	{
		AfxMessageBox(win32proc.GetLastError());
		return FALSE;
	}


	int nCount = 0;
	CStringArray* pArray =win32proc.GetAllProcessesNames();
	for (int i=0;i<pArray->GetSize();i++){
		if(pArray->GetAt(i).CompareNoCase(m_pszAppName)==0) nCount++;
	}

	if(nCount>1){
		MessageBox(NULL, "이미 실행중입니다.", "Error", MB_ICONEXCLAMATION);
		//AfxMessageBox("This program is already running!");
		return FALSE;
	}
*/
	// CG: The following block was added by the Splash Screen component.
\
	{
\
		CCommandLineInfo cmdInfo;
\
		ParseCommandLine(cmdInfo);
\

\
//		CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);
\
	}

	//
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(m_hInstance,szBuff,_MAX_PATH);
	CString path(szBuff);
	SetOriginFolderPath(path.Left(path.ReverseFind('\\')));

	//
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	
	SetRegistryKey("PNE CTSPack");

	char szCurrentDirectory[_MAX_PATH];
	GetCurrentDirectory(_MAX_PATH, szCurrentDirectory);
	
	CString strCurrentPath = GetProfileString("Control", "ResultDataPath", szCurrentDirectory);
	if ( strCurrentPath.Compare(szCurrentDirectory) == 0 )
	{
		strcat(szCurrentDirectory, "\\Data");
		WriteProfileString("Control", "ResultDataPath", szCurrentDirectory);
	}
	strcpy(m_szCurrentDirectory, strCurrentPath);

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

//	CMultiDocTemplate* pDocTemplate;
	m_pDataTemplate = new CMultiDocTemplate(
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDR_DATA):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDR_DATA_ENG):
		IDR_DATA,
		RUNTIME_CLASS(CDataDoc),
		RUNTIME_CLASS(CDataFrame), // custom MDI child frame
		RUNTIME_CLASS(CDataView));
	AddDocTemplate(m_pDataTemplate);

	m_pMonitorTemplate = new CMultiDocTemplate(
		IDR_MONITOR,
		RUNTIME_CLASS(CMonitorDoc),
		RUNTIME_CLASS(CMonitorFrame), // custom MDI child frame
		RUNTIME_CLASS(CMonitorView));
	AddDocTemplate(m_pMonitorTemplate);


	m_pGraphTemplate = new CMultiDocTemplate(
		//IDR_BA_TESTYPE,
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDR_GRAPH): //ksj 20200130 : 그래프 그릴시 메뉴가 언어 설정에 관계없이 한글로 나오는 현상 수정
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDR_GRAPH_ENG): //ksj 20200130 : 그래프 그릴시 메뉴가 언어 설정에 관계없이 한글로 나오는 현상 수정
		IDR_GRAPH, //ksj 20200130 : 그래프 그릴시 메뉴가 언어 설정에 관계없이 한글로 나오는 현상 수정
		RUNTIME_CLASS(CCTSAnalProDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CCTSAnalProView));
	AddDocTemplate(m_pGraphTemplate);

	m_pHelpTemplate = new CMultiDocTemplate(
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDR_HELP):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDR_HELP_ENG):
		IDR_HELP,
		RUNTIME_CLASS(CHelpDoc),
		RUNTIME_CLASS(CHelpFrame), // custom MDI child frame
		RUNTIME_CLASS(CHelpView));
	AddDocTemplate(m_pHelpTemplate);



	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(IDR_MAINFRAME):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(IDR_MAINFRAME_ENG):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(IDR_MAINFRAME_PL):
		(IDR_MAINFRAME)))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

//	// Dispatch commands specified on the command line
//	if (!ProcessShellCommand(cmdInfo))
//		return FALSE;

//	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();

	CString strArgu((char *)m_lpCmdLine);
	//strArgu
	//c:\data 저장위치\test명\M01Ch01\test명.rpt
	//								 \test명.cyc

	if(!strArgu.IsEmpty())
	{
		strArgu.TrimLeft("\"");
		strArgu.TrimRight("\"");
		CString strExt;
		int p1 = strArgu.ReverseFind('.');
		if(p1 > 0)								//파일 확장자 검색 
		{
			strExt =  strArgu.Mid(p1+1);
			strExt.MakeLower();
			if(strExt == PS_RAW_FILE_NAME_EXT || strExt == "rpt" || strExt == PS_RESULT_FILE_NAME_EXT)
			{
				p1 = strArgu.ReverseFind('\\');
				if(p1 >= 0)									//채널 폴더명 분리  
				{
					CString strChPathName, strTestName;
					strChPathName = strArgu.Left(p1);
					//AfxMessageBox(strTemp);
					
					p1 = strChPathName.ReverseFind('\\');	 
					if(p1 > 0)
					{
						CString strTemp;
						strTemp = strChPathName.Left(p1);		//test명 폴더 분리
						//AfxMessageBox(strChPathName);
					
						p1 = strTemp.ReverseFind('\\');
						if(p1 > 0)
						{
							strTestName = strTemp.Mid(p1+1);		//test명 분리 
							//AfxMessageBox(strTestName);
							pMainFrame->m_strCopyMsgString = strChPathName+"\n";
							m_pDataTemplate->OpenDocumentFile(strTestName);
						}
					}
				}
			}
		}
	}


	// Main Frame이 Drag되는 Files를 받아들이게 한다.
	pMainFrame->DragAcceptFiles(TRUE);

//	InitDBPath();
	
	//yulee 20181002 화면에 작업 장소 표시 
	BOOL bUsePlacePlate;
	
	bUsePlacePlate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", FALSE);
	
	if(bUsePlacePlate)
	{
		if(m_pWorkSTSignAnalDlg == NULL)
		{
			m_pWorkSTSignAnalDlg = new CWorkSTSignAnalDlg();
			m_pWorkSTSignAnalDlg->Create(IDD_WORKSTSIGNANAL_DLG);
		}
		m_pWorkSTSignAnalDlg->ShowWindow(SW_SHOW);
		CRect rectTemp;
		m_pWorkSTSignAnalDlg->GetWindowRect(rectTemp);
		::SetWindowPos(m_pWorkSTSignAnalDlg->m_hWnd, HWND_NOTOPMOST, 0, 0, rectTemp.Width(), rectTemp.Height(), SWP_SHOWWINDOW);
		m_pWorkSTSignAnalDlg->ShowWindow(SW_SHOWMINIMIZED); //yulee 20181005
	}

	OnWorkSTShow(); //yulee 20181005
	//
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CAboutDlg
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX , IDD2 = IDD_ABOUTBOX_ENG , IDD3 = IDD_ABOUTBOX_PL};
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CStatic info;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC_ABOUT_INFO, info);

	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	info.SetWindowTextA(PROGRAM_DATE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

// App command to run the dialog
void CCTSAnalProApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProApp message handlers


int CCTSAnalProApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class

//	if(m_pDataTemplate != NULL)		delete m_pDataTemplate; 

//	if(m_pMonitorTemplate!=NULL) delete m_pMonitorTemplate;


//	if(m_pHelpTemplate!=NULL)    delete m_pHelpTemplate;

	//
	return CWinApp::ExitInstance();
}

BOOL CCTSAnalProApp::PreTranslateMessage(MSG* pMsg)
{
	// CG: The following lines were added by the Splash Screen component.
//	if (CSplashWnd::PreTranslateAppMessage(pMsg))
//		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}

//Monitor Frame은 1개만 생성하도록 한다.
void CCTSAnalProApp::OnShowMonitor() 
{
	POSITION pos = m_pMonitorTemplate->GetFirstDocPosition();
	//
	if(pos==NULL) m_pMonitorTemplate->OpenDocumentFile(NULL);	
	else
	{
		CDocument* pDoc = m_pMonitorTemplate->GetNextDoc(pos);
		POSITION pos = pDoc->GetFirstViewPosition();
		pDoc->GetNextView(pos)->GetParentFrame()->ActivateFrame(SW_SHOW);
	}
}

void CCTSAnalProApp::OnShowGraph()
{
// TODO: Add your command handler code here
//	POSITION pos = GetFirstDocTemplatePosition();
//	CMultiDocTemplate *pTempate;
//	pTempate = (CMultiDocTemplate *)GetNextDocTemplate(pos);
//	pTempate->OpenDocumentFile(NULL);

	m_pGraphTemplate->OpenDocumentFile(NULL);

}

void CCTSAnalProApp::OnShowData()
{
	m_pDataTemplate->OpenDocumentFile(NULL);
}


// HELP Frame은 1개만 생성하도록 한다.
void CCTSAnalProApp::OnShowHelp() 
{
	POSITION pos = m_pHelpTemplate->GetFirstDocPosition();
	//
	if(pos==NULL) m_pHelpTemplate->OpenDocumentFile(NULL);	
	else
	{
		CDocument* pDoc = m_pHelpTemplate->GetNextDoc(pos);
		POSITION pos = pDoc->GetFirstViewPosition();
		pDoc->GetNextView(pos)->GetParentFrame()->ActivateFrame(SW_SHOW);
	}
}

//void CCTSAnalProApp::OnFileNew() 
//{
//	if(!m_bInitial) CWinApp::OnFileNew();
//}

void CCTSAnalProApp::InitDBPath()
{
	CUnitPathRecordset rs;
	rs.SetDefaultPath(m_strOriginFolderPath);

	if( rs.IsOpen() ) rs.Close();
	try{ rs.Open(); } catch(...) { return; }

	if( rs.IsBOF() || rs.IsEOF() )
	{
		for ( int nI = 1; nI <= 2; nI++ )
		{
			rs.AddNew();
			rs.m_No = nI;
			rs.m_Path = (nI == 1) ? _T(STRING_UNIT1) : _T(STRING_UNIT2);
			rs.m_Comm = TRUE;
			rs.Update();
		}
	}
	else
	{
		int nI = 1;
		rs.MoveFirst();

		CString strPath;
		while( !rs.IsEOF() )
		{
			strPath = (nI == 1) ? _T(STRING_UNIT1) : _T(STRING_UNIT2);
			if ( rs.m_No == nI )
			{
				if( !rs.m_Path.Compare(strPath) )
				{
					if ( rs.m_Comm == FALSE )
					{
						rs.Edit();
						rs.m_Comm = TRUE;
						rs.Update();
					}
				}
				else
				{
					rs.Edit();
					rs.m_Path = strPath;
					rs.m_Comm = TRUE;
					rs.Update();
				}
			}
			else
			{
				rs.Edit();
				rs.m_No = nI;
				rs.m_Path = strPath;
				rs.m_Comm = TRUE;
				rs.Update();
			}
			rs.MoveNext();
			nI++;
		}
	}
	rs.Close();
/*
	CString strUnitNo, strUnitPath;
	BOOL bCommState;

	CDaoDatabase aDB;
	aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\sysinfo.mdb");
		CDaoRecordset aRec(&aDB);
		aRec.Open(dbOpenTable, "UnitInfo");
		
		if( aRec.IsBOF() || aRec.IsEOF() )
		{
		}
		else
		{
			aRec.MoveFirst();
			int nI = 0;
			while( !aRec.IsEOF() )
			{
				COleVariant varValue;
				//
				aRec.GetFieldValue("No",varValue);
				if(varValue.vt!=VT_NULL) 
					//strUnitNo = (LPCTSTR)(LPTSTR)varValue.bstrVal;
					int nUnitNo = varValue.intVal;
				varValue.Clear();
				//
				aRec.GetFieldValue("Path",varValue);
				if(varValue.vt!=VT_NULL) 
					strUnitPath = (LPCTSTR)(LPTSTR)varValue.bstrVal;
				varValue.Clear();
				//
				aRec.GetFieldValue("Comm",varValue);
				bCommState=varValue.boolVal;
				//
				if ( atoi(strUnitNo) != nI + 1 )
				{
					CString strTmp;
					strTmp.Format("%d", nI + 1);
					varValue.Clear();
					varValue.intVal = nI + 1;
					aRec.SetFieldValue("No", varValue);
					varValue.SetString((nI == 0) ?_T("Z:\\") : _T("Y:\\"), VT_BSTRT);
					aRec.SetFieldValue("Path", varValue);
					varValue.boolVal = TRUE;
					aRec.SetFieldValue("Comm", varValue);
				}
				else 
				{
					if ( ! strUnitPath.Compare ( (nI == 0) ?_T("Z:\\") : _T("Y:\\") ) )
					{
						varValue.boolVal = TRUE;
						aRec.SetFieldValue("Comm", varValue);
					}
					else
					{
						varValue.SetString((nI == 0) ?_T("Z:\\") : _T("Y:\\"), VT_BSTRT);
						aRec.SetFieldValue("Path", varValue);
						//aRec.SetFieldValue("Path", (nI == 0) ?_T("Z:\\") : _T("Y:\\"));
						varValue.boolVal = TRUE;
						aRec.SetFieldValue("Comm", varValue);
					}
				}

				nI++;
				aRec.MoveNext();
			}
		}
		aRec.Close();
	*/
}

void CCTSAnalProApp::SetResultDataPath(CString strPath)
{
	strcpy(m_szCurrentDirectory, strPath);

	WriteProfileString("Control", "ResultDataPath", strPath);
}

void CCTSAnalProApp::OnUpdateShowHelp(CCmdUI* pCmdUI) 
{
	CString strHelpFilePath = m_strOriginFolderPath + "\\Manual.htm";
	CFileFind aFinder;
	
	if(aFinder.FindFile(strHelpFilePath))
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);

	aFinder.Close();
}



void CCTSAnalProApp::OnUpdateShowMonitor(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
}

void CCTSAnalProApp::OnUpdateShowGraph(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
//	pCmdUI->Enable(FALSE);	
}

CString CCTSAnalProApp::GetExcelPath()
{
	CString strExcelPath = GetProfileString(REG_CONFIG_SECTION,"Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		AfxMessageBox(Fun_FindMsg("GetExcelPath_msg1","CCTSAnalProApp"));
		//@ AfxMessageBox("Excel 위치가 설정되어 있지 않습니다. 위치를 지정하여 주십시요.");
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		pDlg.m_ofn.lpstrTitle = Fun_FindMsg("GetExcelPath_msg2","CCTSAnalProApp");
		//@ pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		WriteProfileString(REG_CONFIG_SECTION,"Excel Path", strExcelPath);
	}
	return strExcelPath;
}

BOOL CCTSAnalProApp::ExecuteExcel(CString strFileName)
{
	STARTUPINFO	stStartUpInfo;
	PROCESS_INFORMATION	ProcessInfo;
	ZeroMemory(&stStartUpInfo, sizeof(STARTUPINFO));
	ZeroMemory(&ProcessInfo, sizeof(PROCESS_INFORMATION));
		
	stStartUpInfo.cb = sizeof(STARTUPINFO);
	stStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	stStartUpInfo.wShowWindow = SW_NORMAL;

	CString strTemp;
	strTemp = GetExcelPath();
	if(strTemp.IsEmpty())	return FALSE;

	char buff1[_MAX_PATH];
	char buff2[_MAX_PATH];
	ZeroMemory(buff1, _MAX_PATH);
	ZeroMemory(buff2, _MAX_PATH);

	int aa =0;
	do {
		//존재 여부 확인
		aa = GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
		if(aa <= 0)
		{
			if(AfxMessageBox(Fun_FindMsg("ExecuteExcel_msg1","CCTSAnalProApp"), MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			//@ if(AfxMessageBox("Excel 실행파일 경로가 잘못되었습니다. 경로를 설정 하십시요.", MB_ICONSTOP|MB_OKCANCEL) == IDCANCEL)
			{
				return FALSE;

			}
			strTemp = GetExcelPath();
			if(strTemp.IsEmpty())	return FALSE;
		}
		
	} while( aa <= 0 );	

	GetShortPathName((LPSTR)(LPCTSTR)strTemp, buff2, _MAX_PATH);
	GetShortPathName((LPSTR)(LPCTSTR)strFileName, buff1, _MAX_PATH);
	
	strTemp.Format("%s %s", buff2, buff1);
	
	//BOOL bFlag = ::CreateProcess(NULL, (LPSTR)(LPCTSTR)strTemp, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &stStartUpInfo, &ProcessInfo);
		
	//20180315 yulee office2016 호환 
	BOOL bFlag;
	SHELLEXECUTEINFO lpExecInfo;
	memset(&lpExecInfo,0, sizeof(SHELLEXECUTEINFO));
	lpExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strFileName;
	
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST | SEE_MASK_NOCLOSEPROCESS;
	lpExecInfo.hwnd = NULL;
	lpExecInfo.lpVerb = _T("open");
	lpExecInfo.lpDirectory=NULL;
	lpExecInfo.nShow = SW_SHOW;
	lpExecInfo.hInstApp = (HINSTANCE)SE_ERR_DDEFAIL;
	
    bFlag = ShellExecuteEx(&lpExecInfo);

	if(bFlag == FALSE)
	{
		strTemp.Format(Fun_FindMsg("ExecuteExcel_msg2","CCTSAnalProApp"), strFileName);
		//@ strTemp.Format("%s를 Open할 수 없습니다.", strFileName);
		AfxMessageBox(strTemp, MB_OK|MB_ICONSTOP);
	}
	return TRUE;
}


//lmh 20120207
void CCTSAnalProApp::CreateNewGraphFrame(CString strPath)
{
	CCTSAnalProDoc *pDoc = (CCTSAnalProDoc *)m_pGraphTemplate->CreateNewDocument();	//new CCTSAnalProDoc;
	//	CChildFrame *pFrame = new CChildFrame;
	
	CFrameWnd *pFrmWnd = m_pGraphTemplate->CreateNewFrame(pDoc, NULL);
	m_pGraphTemplate->InitialUpdateFrame(pFrmWnd, pDoc);
	((CCTSAnalProView *)(pFrmWnd->GetActiveView()))->DataIndexQuery(strPath, &m_dwaSelTableArray);
}

//lmh 20120207
//CreateNewGraphFrame() 호출전 반드시 호출 해야 함
void CCTSAnalProApp::SetSelTableIndexArray(CDWordArray &dwSelArray)
{
	m_dwaSelTableArray.RemoveAll();
	for(int i = 0; i<dwSelArray.GetSize(); i++)
	{
		m_dwaSelTableArray.Add(dwSelArray[i]);
	}
}

void CCTSAnalProApp::OnWorkSTShow() 
{
	BOOL bUsePlacePlate;
	
	bUsePlacePlate = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", FALSE);
	// yulee 20181002
	// yulee 20181002
	if(m_pWorkSTSignAnalDlg == NULL)
	{
		if(bUsePlacePlate)
		{
		m_pWorkSTSignAnalDlg = new CWorkSTSignAnalDlg();
		m_pWorkSTSignAnalDlg->Create(IDD_WORKSTSIGNANAL_DLG);
		m_pWorkSTSignAnalDlg->ShowWindow(SW_SHOW);
		AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Use Place Plate", TRUE);
		}
		else
		{
		//	AfxMessageBox("관리자에게 문의하세요. : Registry 설정 Off");
		}
	}
	else
	{
		m_pWorkSTSignAnalDlg->DestroyWindow();
		delete m_pWorkSTSignAnalDlg;
		m_pWorkSTSignAnalDlg = NULL;

		m_pWorkSTSignAnalDlg = new CWorkSTSignAnalDlg();
		m_pWorkSTSignAnalDlg->Create(IDD_WORKSTSIGNANAL_DLG);
		m_pWorkSTSignAnalDlg->ShowWindow(SW_SHOW);
	}
	
}
BOOL CCTSAnalProApp::file_Finder(CString path)
{//file find	
	if(path.IsEmpty() || path==_T("")) return FALSE;
	
	BOOL bWork;
	CFileFind finder;
	bWork=finder.FindFile(path);		
	finder.Close();		
	
	return bWork;
}

BOOL CCTSAnalProApp::file_AddWrite(CString filename,CString data)
{//파일에 문자열 추가		
	FILE *fWrite;
	
	#ifdef _UNICODE			   			
		if(file_Finder(filename)==FALSE)
		{				
			fWrite = _wfopen(filename,_T("wb+"));
			if(!fWrite) return FALSE;
			
			//한글utf-8
			//WORD mark = 0xFEFF;
			//fwrite(&mark, sizeof(WORD), 1, fWrite);
		} 
		else 
		{
			fWrite = _wfopen(filename,_T("rb+"));
			if(!fWrite) return FALSE;
			
			fseek(fWrite, 0, SEEK_END);
		}
		fwprintf(fWrite, data);		
	#else
		if(file_Finder(filename)==FALSE)
		{				
			fWrite=fopen(filename,_T("w+"));	
			if(!fWrite) return FALSE;
		} 
		else 
		{
				  fWrite=fopen(filename,_T("r+"));	
				  if(!fWrite) return FALSE;
				  
				  fseek(fWrite, 0, SEEK_END);
		}
		fwrite(data,sizeof(char),data.GetLength(),fWrite);	
	#endif	
	
	fclose(fWrite);	
	return TRUE;
}

void CCTSAnalProApp::OnMenuLangKor() 
{
	CString tmpStr;
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 1);
	if(bSelectedLanguage == 1)
	{
		tmpStr.Format("한국어가 적용되어 있습니다.");
		AfxMessageBox(tmpStr);//&&	
		return;
	}
	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 1);

	if(bChangeToEng == TRUE)
	{
		tmpStr.Format("프로그램 재 시작 시 한국어가 적용됩니다.");
		AfxMessageBox(tmpStr);//&&
	}
}

void CCTSAnalProApp::OnMenuLangEng()//2-English
{
	CString tmpStr;
	BOOL bSelectedLanguage = AfxGetApp()->GetProfileInt(CT_CONFIG_REG_SEC, "Language", 2);
	if(bSelectedLanguage == 2)
	{
		tmpStr.Format("The language is already applied.");
		AfxMessageBox(tmpStr);//&&	
		return;
	}
	BOOL bChangeToEng = AfxGetApp()->WriteProfileInt(CT_CONFIG_REG_SEC, "Language", 2);

	if(bChangeToEng == TRUE)
	{
		tmpStr.Format("English will be applied after restart of this program.");
		AfxMessageBox(tmpStr);//&&
	}
}

void CCTSAnalProApp::OnUpdateLangKor(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CCTSAnalProApp::OnUpdateLangEng(CCmdUI* pCmdUI) 
{
	//pCmdUI->Enable(FALSE);
	pCmdUI->Enable(TRUE); //lyj 20200909 english 버튼 활성화
}
