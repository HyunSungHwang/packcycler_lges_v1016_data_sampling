// ORIGIONAL FILE INFO:
//   IniFile.h: interface for the CIniFile class.
//   Written by: Adam Clauss
//   Email: pandcc3@comwerx.net
//       You may use this class/code as you wish in your programs.  Feel free to distribute it, and
//     email suggested changes to me.
//
// ADAPTED FILE INFO:
//   LangIni.cpp: interface for the CLangIni class.
//   Written by: OMOSHIMA
//   Email: omoshima@hotmail.com
//       CODE ADAPTED FROM: Adam Clauss
//
// NOTES:
//    I removed some functions and changed some functions to more
//  precicely fit the needs of a Language .INI File..... Hope everyone
//  understands it. I also changed it to conform with VC++.NET specs.
//////////////////////////////////////////////////////////////////////


#ifndef _LANGINI_H_
#define _LANGINI_H_

#include <iostream>
using namespace std;

#include <afxtempl.h>

class CLangIni
{
	public:
		CLangIni(void);
		~CLangIni(void);
	private:
		//all keys are of this type
		struct key
		{
			//list of values in key
			CArray<CString, CString> values;

			//list of value names
			CArray<CString, CString> names;
		};
		
		//list of keys in ini
		CArray<key, key> keys;
		
		//corresponding list of keynames
		CArray<CString, CString> names;
		
		// Path to <language_name>.ini file
		CString m_path;
		
		// Returns index of specified key, or -1 if not found
		int FindKey(CString keyname);
		
		// Returns index of specified value in the specified key, or -1 if not found
		int FindValue(int keynum, CString valuename);
		
		// overloaded from original getline to take CString
		istream& getline(istream &is, CString &str);
		
	public:
		// Contains error message (if one occurs)
		CString m_errorMessage;
		
		// Reads ini file specified using CIniFile::SetPath()
		// Returns true if successful, false otherwise
		bool LoadFile(void);
		
		// Reads ini file specified by a string parameter
		// Returns true if successful, false otherwise
		// Sets m_path to the specified string
		bool CLangIni::LoadFile(CString path);
		
		// Sets value of:
		//     [keyname]
		//     valuename=_____
		//
		// NOTE: Specify the optional paramter as false if you do not want it to create
		// the key if it doesn't exist. Returns true if data entered, otherwise false
		bool SetValue(CString keyname, CString valuename, CString value, bool create=true);
		
		// Gets value of:
		//     [keyname]
		//     valuename=_____
		CString GetValue(CString keyname, CString valuename, CString sdefault=_T(""));
		
		// Set path to ini file
		void SetPath(CString path) { m_path = path; }
		
		// Return path of current ini file
		CString GetPath(void) { return m_path; }

		//get valuename seq
		void GetMuldata(CString keyname,CString valuename,CStringArray& Data);

		void SetFile(CString keyname,CString valuename,CString value);
		void SetEmpty(CString keyname);

};

#endif

