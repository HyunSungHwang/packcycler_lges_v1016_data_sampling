#if !defined(AFX_NETSTATEMONITORINGDLG_H__BFFF5D44_CD4E_48D5_8058_973409450A41__INCLUDED_)
#define AFX_NETSTATEMONITORINGDLG_H__BFFF5D44_CD4E_48D5_8058_973409450A41__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NetStateMonitoringDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNetStateMonitoringDlg dialog
class CMonitorDoc;
class CNetStateMonitoringDlg : public CDialog
{
// Construction
public:
	CNetStateMonitoringDlg(CMonitorDoc* pDoc, CWnd* pParent = NULL);   // standard constructor
	virtual ~CNetStateMonitoringDlg();

	void SetUnitState(BYTE byUnitIndex);
// Dialog Data
	//{{AFX_DATA(CNetStateMonitoringDlg)
	enum { IDD = IDD_NETSTATE_DLG };
	CListCtrl	m_wndList;
	//}}AFX_DATA
	CImageList* m_pNetStateImage;
	CMonitorDoc* m_pDoc;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetStateMonitoringDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNetStateMonitoringDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETSTATEMONITORINGDLG_H__BFFF5D44_CD4E_48D5_8058_973409450A41__INCLUDED_)
