/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: SetNetworkDlg.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CSetNetworkDialog class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_SETNETWORKDLG_H__B8D30FDA_F0C4_4CF5_9E10_474D620A894A__INCLUDED_)
#define AFX_SETNETWORKDLG_H__B8D30FDA_F0C4_4CF5_9E10_474D620A894A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// SetNetworkDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetNetworkDialog dialog

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CSetNetworkDialog
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CSetNetworkDialog : public CDialog
{
// Construction
public:
	CSetNetworkDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetNetworkDialog)
	enum { IDD = IDD_SET_NETWORK_DIALOG , IDD2 = IDD_SET_NETWORK_DIALOG_ENG , IDD3 = IDD_SET_NETWORK_DIALOG_PL };
	CCheckListBox	m_ModuleListBox;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetNetworkDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSetNetworkDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnSetNetwork();
	afx_msg void OnSelchangeModuleList();
	virtual void OnOK();
	afx_msg void OnConnectBtn();
	afx_msg void OnDisconnectBtn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
public:
	int  m_iUnitIndex;
	BOOL m_bValidPath;

// Operations
private:
	void DisplayPathInfo();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETNETWORKDLG_H__B8D30FDA_F0C4_4CF5_9E10_474D620A894A__INCLUDED_)
