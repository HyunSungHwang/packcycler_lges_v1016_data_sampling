// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CTSAnalPro.h"

#include "MainFrm.h"
#include "Splash.h"
#include "LogDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_DROPFILES()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	//}}AFX_MSG_MAP
	ON_MESSAGE(UM_SHOW_LOG_DIALOG, OnShowLogDialog)
	ON_NOTIFY(TBN_DROPDOWN, AFX_IDW_TOOLBAR, OnToolbarDropDown)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	m_sTestName = "";
	m_bIsProgressGraph = FALSE;	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

//	ShowLoginDlg();
//	CLogDialog aDlg;
//	if(aDlg.DoModal()!=IDOK)	return -1;

	
	if (!m_wndMainToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) || 
		!m_wndMainToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar5\n");
		return -1;      // fail to create
	}

	if (!m_wndHelpToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) || 
		!m_wndHelpToolBar.LoadToolBar(IDR_HELP))
	{
		TRACE0("Failed to create toolbar4\n");
		return -1;      // fail to create
	}

	if (!m_wndMonitorToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndMonitorToolBar.LoadToolBar(IDR_MONITOR))
	{
		TRACE0("Failed to create toolbar3\n");
		return -1;      // fail to create
	}
//	m_wndMonitorToolBar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);

		// ===========================================================
	// True color Bitmap 을 툴바의 버튼에 그리는 방법
//    m_bmToolbarHi.LoadBitmap(IDB_MONITOR);
//    m_wndMonitorToolBar.SetBitmap((HBITMAP)m_bmToolbarHi);
	// ===========================================================
	
	//DWORD dwStyle = m_wndMonitorToolBar.GetButtonStyle(m_wndMonitorToolBar.CommandToIndex(IDM_COMMAND_STOP));
	//dwStyle |= TBSTYLE_DROPDOWN;
	//m_wndMonitorToolBar.SetButtonStyle(m_wndMonitorToolBar.CommandToIndex(IDM_COMMAND_STOP), dwStyle);

	if (!m_wndDataToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndDataToolBar.LoadToolBar(IDR_DATA))
	{
		TRACE0("Failed to create toolbar1\n");
		return -1;      // fail to create
	}
	if (!m_wndGraphToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndGraphToolBar.LoadToolBar(IDR_GRAPH_ENG))
	{
		TRACE0("Failed to create toolbar2\n");
		return -1;      // fail to create
	}

	

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	EnableDocking(CBRS_ALIGN_ANY);

	//
	LoadToolBar(0);

	// CG: The following line was added by the Splash Screen component.
//	CSplashWnd::ShowSplashScreen(this);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
/*	cs.style &= ~FWS_ADDTOTITLE;
	cs.lpszName="PowerBTS-Control System";
	cs.x=0;
	cs.y=0;
	cs.cx=1024;
	cs.cy=768;*/
//	return TRUE;

	cs.style &= ~FWS_ADDTOTITLE;
	cs.lpszName=PROGRAM_VER;
	
//	int x=GetSystemMetrics(SM_CXMAXTRACK);
//	int y=GetSystemMetrics(SM_CYMAXTRACK);

//	cs.x=(int)(x-800)/2;
//	cs.y=(int)(y-600)/2;
//	cs.cx=800;
//	cs.cy=600;

	WNDCLASS wc;
	::GetClassInfo( AfxGetInstanceHandle(), cs.lpszClass, &wc );
    wc.lpszClassName = PS_DATA_ANAL_PRO_CLASS_NAME; 
	cs.lpszClass = PS_DATA_ANAL_PRO_CLASS_NAME;    
	wc.hIcon = ::LoadIcon(AfxGetInstanceHandle(), 
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?("IDR_MAINFRAME"):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?("IDR_MAINFRAME_ENG"):
		(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?("IDR_MAINFRAME_PL"):
		"IDR_MAINFRAME");
	::RegisterClass( &wc );

	return TRUE; 
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

LRESULT CMainFrame::OnShowLogDialog(WPARAM wParam, LPARAM lParam)
{
	ShowLoginDlg();
	return 0UL;
}


void CMainFrame::LoadToolBar(int no)
{
	ShowControlBar(&m_wndMainToolBar, FALSE, TRUE);
	ShowControlBar(&m_wndHelpToolBar, FALSE, TRUE);
	ShowControlBar(&m_wndMonitorToolBar, FALSE, TRUE);
	ShowControlBar(&m_wndDataToolBar, FALSE, TRUE);
	ShowControlBar(&m_wndGraphToolBar, FALSE, TRUE);

	switch(no)
	{
	case MAIN_TOOLBAR:
		ShowControlBar(&m_wndMainToolBar, TRUE, FALSE);
		break;
	case MONITOR_TOOLBAR:
		ShowControlBar(&m_wndMonitorToolBar, TRUE, FALSE);
		break;
	case DATA_TOOLBAR:
		ShowControlBar(&m_wndDataToolBar, TRUE, FALSE);
		break;	
	case GRAPH_TOOLBAR:
		ShowControlBar(&m_wndGraphToolBar, TRUE, FALSE);
		break;
	case HELP_TOOLBAR:
		ShowControlBar(&m_wndHelpToolBar, TRUE, FALSE);
		break;
	}
}



void CMainFrame::OnDropFiles(HDROP hDropInfo) 
{
	CString Path;
	UINT nFiles = ::DragQueryFile(hDropInfo, (UINT)-1, NULL, 0);
	
	CString strChPathName, strTestName;
	m_strCopyMsgString.Empty();
	for (UINT iFile = 0; iFile < nFiles; iFile++)
	{
		TCHAR szFileName[_MAX_PATH];
		ZeroMemory(szFileName, _MAX_PATH);
		::DragQueryFile(hDropInfo, iFile, szFileName, _MAX_PATH);
		
		CString strArgu(szFileName);
		
		/*CFileFind afinder;
		afinder.FindFile(strPath);
		afinder.FindNextFile();
		if(!afinder.IsDots()&&afinder.IsDirectory()) Path+=strPath+"\n";
		*/
		
		if(!strArgu.IsEmpty())
		{
			strArgu.TrimLeft("\"");
			strArgu.TrimRight("\"");
			CString strExt;
			int p1 = strArgu.ReverseFind('.');
			if(p1 > 0)								//파일 확장자 검색 
			{
				strExt =  strArgu.Mid(p1+1);
				strExt.MakeLower();
				if(strExt == PS_RAW_FILE_NAME_EXT || strExt == "rpt" || strExt == PS_RESULT_FILE_NAME_EXT)
				{
					p1 = strArgu.ReverseFind('\\');
					if(p1 >= 0)									//채널 폴더명 분리  
					{
						strChPathName = strArgu.Left(p1);
						//AfxMessageBox(strTemp);
						
						p1 = strChPathName.ReverseFind('\\');	 
						if(p1 > 0)
						{
							CString strTemp;
							strTemp = strChPathName.Left(p1);		//test명 폴더 분리
							//AfxMessageBox(strChPathName);
						
							p1 = strTemp.ReverseFind('\\');
							if(p1 > 0)
							{
								strTestName = strTemp.Mid(p1+1);		//test명 분리 
								//AfxMessageBox(strTestName);
								m_strCopyMsgString += (strChPathName+"\n");
							}
						}
					}
				}
			}
		}
	}
	::DragFinish(hDropInfo);
	
	if(m_strCopyMsgString.GetLength() > 0)
	{
		((CCTSAnalProApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile(strTestName);
	}
}

void CMainFrame::OnClose() 
{
	while(TRUE)
	{
		CFrameWnd* pFrm = GetActiveFrame();
		if(pFrm==this) break;
		pFrm->SendMessage(WM_CLOSE);
	}

	//
	CMDIFrameWnd::OnClose();
}

void CMainFrame::OnToolbarDropDown(NMHDR* pnmtb, LRESULT *plr)
{
	CWnd *pWnd;
	UINT nID;

	// Switch on button command id's.
	switch (pnmtb->idFrom)
	{
	case IDM_COMMAND_STOP:
		pWnd = &m_wndMonitorToolBar;
		nID  = IDR_CONTEXT_MENU;
		break;
	default:
		return;
	}
	
	// load and display popup menu
	CMenu menu;
	menu.LoadMenu(nID);
	CMenu* pPopup = menu.GetSubMenu(3);
	ASSERT(pPopup);
	
	CRect rc;
	pWnd->SendMessage(TB_GETRECT, pnmtb->idFrom, (LPARAM)&rc);
	pWnd->ClientToScreen(&rc);
	
	pPopup->TrackPopupMenu( TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_VERTICAL,
		rc.left, rc.bottom, this, &rc);
}

BOOL CMainFrame::ShowLoginDlg()
{
	CLogDialog aDlg;
	if(aDlg.DoModal()!=IDOK) AfxGetMainWnd()->SendMessage(WM_CLOSE);
	else                     AfxGetMainWnd()->SendMessage(WM_COMMAND, ID_SHOW_MONITOR);

	return TRUE;
}

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	// TODO: Add your message handler code here and/or call default

	//CTSMonPro에서 전달된 Message
	if(pCopyDataStruct->dwData == 3)
	{
		if(pCopyDataStruct->cbData > 0)	//"\n"
		{
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
//			CString strData(pData);
			m_strCopyMsgString = pData;
			delete [] pData;	
			if(!m_strCopyMsgString.IsEmpty())
			{
				CString strTestName("Data1");
				int p1 = m_strCopyMsgString.Find('\n');
				if(p1 >= 0)	
				{
					CString strTemp;
					strTemp = m_strCopyMsgString.Left(p1);
					p1 = strTemp.ReverseFind('\\');
					if(p1 >= 0)
					{
						CString strTemp1;
						strTemp1 = strTemp.Left(p1);
						p1 = strTemp1.ReverseFind('\\');
						if(p1 >=0)
						{
							strTestName = strTemp1.Mid(p1+1);
						}
					}
				}

				//document인자는 overflow 발생하므로 인자로 안넘기고 변수로 처리 
				//((CCTSAnalProApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile("aa");
			//	((CCTSAnalProApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile(strTestName);
				((CCTSAnalProApp *)AfxGetApp())->m_pDataTemplate->OpenDocumentFile(strTestName, FALSE, TRUE);
			}
		}
	}
	else if(pCopyDataStruct->dwData == 4)	//Graph View
	{
		if(pCopyDataStruct->cbData > 0)	//"\n"
		{
			char *pData = new char[pCopyDataStruct->cbData];
			memcpy(pData, pCopyDataStruct->lpData, pCopyDataStruct->cbData);
//			CString strData(pData);		
			m_strCopyMsgString = pData;
			delete [] pData;
			if(!m_strCopyMsgString.IsEmpty())
			{
				CString strTestName("Graph1");
				int p1 = m_strCopyMsgString.Find('\n');
				if(p1 >= 0)	
				{
					CString strTemp;
					strTemp = m_strCopyMsgString.Left(p1);
					p1 = strTemp.ReverseFind('\\');
					if(p1 >= 0)
					{
						CString strTemp1;
						strTemp1 = strTemp.Left(p1);
						p1 = strTemp1.ReverseFind('\\');
						if(p1 >=0)
						{
							strTestName = strTemp1.Mid(p1+1);
						}
					}
				}

				// 210805 HKH Reload 관련 추가
				m_sTestName = strTestName;
				m_bIsProgressGraph = TRUE;

				//document인자는 overflow 발생하므로 인자로 안넘기고 변수로 처리 
				//((CCTSAnalProApp *)AfxGetApp())->m_pGraphTemplate->OpenDocumentFile("aa");
				//((CCTSAnalProApp *)AfxGetApp())->m_pGraphTemplate->OpenDocumentFile(strTestName);
				((CCTSAnalProApp *)AfxGetApp())->m_pGraphTemplate->OpenDocumentFile(strTestName, FALSE, TRUE);
			}
		}
	}
	return CMDIFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}


