#pragma once
#include "afxcmn.h"


// CGeneralConfigDlg 대화 상자입니다.

class CGeneralConfigDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CGeneralConfigDlg)

public:
	CGeneralConfigDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CGeneralConfigDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_GENERAL_CFG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	int LoadProperty(void);
	CListCtrl m_ctrlPropertyList;
	afx_msg void OnBnClickedOk();
	int InitListCtrl(void);
	virtual BOOL OnInitDialog();
	float m_fUnitTransfer;
	afx_msg void OnEnChangeEditUnitTransfer();
	afx_msg void OnLvnItemchangedListProperty(NMHDR *pNMHDR, LRESULT *pResult);
	int UpdateProperty(void);
};
