#if !defined(AFX_UPDATEDSCHLISTDLG_H__3516F871_C91B_4754_8F1D_126A832F7A80__INCLUDED_)
#define AFX_UPDATEDSCHLISTDLG_H__3516F871_C91B_4754_8F1D_126A832F7A80__INCLUDED_
//$1013
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UpdatedSchListDlg.h : header file
//
// ksj 20170810 : Update Sch 목록 출력 및 파일 열기 대화상자.

/////////////////////////////////////////////////////////////////////////////
// CUpdatedSchListDlg dialog

class CUpdatedSchListDlg : public CDialog
{
// Construction
public:
	void InitList();
	CUpdatedSchListDlg(CString strPath, CWnd* pParent = NULL);   // standard constructor
	CString m_strPath;

// Dialog Data
	//{{AFX_DATA(CUpdatedSchListDlg)
	enum { IDD = IDD_UPDATED_SCH_LIST , IDD2 = IDD_UPDATED_SCH_LIST_ENG, IDD3 = IDD_UPDATED_SCH_LIST_PL };
	CListCtrl	m_ctrlUpdateSchList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUpdatedSchListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUpdatedSchListDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnItemdblclickUpdateSchList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkUpdateSchList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void UpdateList(CString strPath);
	void OpenSch(CString strFileName);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UPDATEDSCHLISTDLG_H__3516F871_C91B_4754_8F1D_126A832F7A80__INCLUDED_)
