// UserSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "UserSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserSetDlg dialog


CUserSetDlg::CUserSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CUserSetDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CUserSetDlg::IDD2):
	(CUserSetDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CUserSetDlg)
	m_nTrayColSize = 0;
	m_bDispCapSum = FALSE;
	m_fTime1 = 0;
	m_fTime2 = 0.0f;
	m_bUserImpTime = FALSE;
	m_nExcelOp = 0;
	m_nExcelSize = 0;
	bAuxListAllFlag = FALSE; //yulee 20180729
	//}}AFX_DATA_INIT
}


void CUserSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserSetDlg)
	DDX_Control(pDX, IDC_CAPA_PRESI_COMBO, m_ctrlCapaDecimal);
	DDX_Control(pDX, IDC_CAPA_UNIT_COMBO, m_ctrlCapaUnit);
	DDX_Control(pDX, IDC_ITEM_LIST, m_itemList);
	DDX_Control(pDX, IDC_WH_PRESI_COMBO, m_ctrlWhDecimal);
	DDX_Control(pDX, IDC_W_PRESI_COMBO, m_ctrlWDecimal);
	DDX_Control(pDX, IDC_WH_UNIT_COMBO, m_ctrlWhUnit);
	DDX_Control(pDX, IDC_W_UNIT_COMBO, m_ctrlWUnit);
	DDX_Control(pDX, IDC_FONT_NAME, m_ctrFontResult);
	DDX_Control(pDX, IDC_TIME_UNIT_COMBO, m_ctrlTimeUnit);
	DDX_Control(pDX, IDC_C_PRESI_COMBO, m_ctrlCDecimal);
	DDX_Control(pDX, IDC_I_PRESI_COMBO, m_ctrlIDecimal);
	DDX_Control(pDX, IDC_V_PRESI_COMBO, m_ctrlVDecimal);
	DDX_Control(pDX, IDC_C_UNIT_COMBO, m_ctrlCUnit);
	DDX_Control(pDX, IDC_I_UNIT_COMBO, m_ctrlIUnit);
	DDX_Control(pDX, IDC_V_UNIT_COMBO, m_ctrlVUnit);
	DDX_Text(pDX, 1078, m_nTrayColSize);
	DDX_Check(pDX, IDC_CAP_SUM_CHECK, m_bDispCapSum);
	DDX_Text(pDX, IDC_EDIT_TIME1, m_fTime1);
	DDX_Text(pDX, IDC_EDIT_TIME2, m_fTime2);
	DDX_Check(pDX, IDC_CHECK1, m_bUserImpTime);
	DDX_Radio(pDX, IDC_RADIO1, m_nExcelOp);
	DDX_Text(pDX, IDC_EDIT_EXCEL_SIZE, m_nExcelSize);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserSetDlg, CDialog)
	//{{AFX_MSG_MAP(CUserSetDlg)
	ON_BN_CLICKED(IDC_FONT_SET_BTN, OnFontSetBtn)
	ON_BN_CLICKED(IDC_UP_BUTTON, OnUpButton)
	ON_BN_CLICKED(IDC_DOWN_BUTTON, OnDownButton)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_ButExportCheckAll, OnButExportCheckAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserSetDlg message handlers

BOOL CUserSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//////////////////////////////////////////////////////////////////////////

	m_nTrayColSize = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "TrayColSize", 8);
	m_nExcelOp = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "ExcelSaveOption", 0);
	m_nExcelSize = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "ExcelSaveSize", 65536);

	if(m_nExcelOp)
	{
		GetDlgItem(IDC_STATIC_EXCEL_SIZE)->SetWindowText("KB");
	}
	else
	{
		GetDlgItem(IDC_STATIC_EXCEL_SIZE)->SetWindowText("Row");
	}

	UINT nSize;
	LPVOID* pData= NULL;
	BOOL nRtn = AfxGetApp()->GetProfileBinary(REG_CONFIG_SECTION, "GridFont", (LPBYTE *)&pData, &nSize);
	if(nSize > 0 && nRtn == TRUE)	
	{
		memcpy(&m_afont, pData, nSize);
	}
	else
	{
		CFont *pFont = GetFont();
		pFont->GetLogFont(&m_afont);
	}
	if(pData) delete [] pData;

// #ifdef _EDLC_TEST_SYSTEM
// 	m_ctrlCUnit.ResetContent();
// 	m_ctrlCUnit.AddString("F");
// 	m_ctrlCUnit.AddString("mF");
// 	m_ctrlCUnit.AddString("uF");
// #endif


	char szBuff[32], szUnit[16], szDecimalPoint[16];
	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "V Unit", "V 3"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlVUnit.SelectString(0, szUnit);
	m_ctrlVDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "I Unit", "mA 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlIUnit.SelectString(0,szUnit);
	m_ctrlIDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "C Unit", "mAh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlCUnit.SelectString(0,szUnit);
	m_ctrlCDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "Capa Unit", "mF 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlCapaUnit.SelectString(0,szUnit);
	m_ctrlCapaDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "W Unit", "mW 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlWUnit.SelectString(0,szUnit);
	m_ctrlWDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "Wh Unit", "mWh 1"));
	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
	m_ctrlWhUnit.SelectString(0,szUnit);
	m_ctrlWhDecimal.SelectString(0,szDecimalPoint);

	//ljb  [9/30/2011 XNOTE]
// 	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "F Unit", "F 3"));
// 	sscanf(szBuff, "%s %s", szUnit, szDecimalPoint);
// 	m_ctrlFUnit.SelectString(0,szUnit);
// 	m_ctrlFDecimal.SelectString(0,szDecimalPoint);

	sprintf(szBuff, AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "Time Unit", "0"));
	m_ctrlTimeUnit.SetCurSel(atol(szBuff));
	//////////////////////////////////////////////////////////////////////////
	
	m_bDispCapSum = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "Capacity Sum", FALSE);

	m_fTime1 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime1", "100"));
	m_fTime2 = atof(AfxGetApp()->GetProfileString(REG_CONFIG_SECTION, "ImpTime2", "0.01"));
	m_bUserImpTime = AfxGetApp()->GetProfileInt(REG_CONFIG_SECTION, "UserImp", FALSE);
	GetDlgItem(IDC_EDIT_TIME1)->EnableWindow(m_bUserImpTime);
	GetDlgItem(IDC_EDIT_TIME2)->EnableWindow(m_bUserImpTime);

#ifdef _EDLC_CELL_
	GetDlgItem(IDC_STATIC_CAP)->ShowWindow(TRUE);
	GetDlgItem(IDC_CAPA_UNIT_COMBO)->ShowWindow(TRUE);
	GetDlgItem(IDC_CAPA_PRESI_COMBO)->ShowWindow(TRUE);
	GetDlgItem(IDC_STATIC_NORMAL)->ShowWindow(FALSE);
	GetDlgItem(IDC_C_UNIT_COMBO)->ShowWindow(FALSE);
	GetDlgItem(IDC_C_PRESI_COMBO)->ShowWindow(FALSE);
#else
	GetDlgItem(IDC_STATIC_CAP)->ShowWindow(FALSE);
	GetDlgItem(IDC_CAPA_UNIT_COMBO)->ShowWindow(FALSE);
	GetDlgItem(IDC_CAPA_PRESI_COMBO)->ShowWindow(FALSE);
	GetDlgItem(IDC_STATIC_NORMAL)->ShowWindow(TRUE);
	GetDlgItem(IDC_C_UNIT_COMBO)->ShowWindow(TRUE);
	GetDlgItem(IDC_C_PRESI_COMBO)->ShowWindow(TRUE);
#endif

	//Save Item
	CString strData;
	DWORD dwItem[PS_MAX_ITEM_NUM] = {0xffff,};

	pData = NULL;
	//nRtn = AfxGetApp()->GetProfileBinary(REG_CONFIG_SECTION, "SaveItem_v100F", (LPBYTE *)&pData, &nSize);
	nRtn = AfxGetApp()->GetProfileBinary(REG_CONFIG_SECTION, "SaveItem_v1016", (LPBYTE *)&pData, &nSize); //ksj 20200210
	if(nSize > 0 && nRtn == TRUE)	
	{
		memcpy(dwItem, pData, nSize);
	}
	else
	{
		//default list
		for( int nI = 0; nI < PS_MAX_ITEM_NUM; nI++ )
		{
			strData = ::PSGetItemName(nI);
			TRACE("nI=%d, %s \n", nI, strData);
			if (strData == "") continue;	//ljb 20150604 add
			if(strData.IsEmpty())
			{
				dwItem[nI] = MAKELONG(nI , FALSE);
			}
			else
			{
				dwItem[nI] = MAKELONG(nI , TRUE);
			}
		}
	}


	if(pData) delete [] pData;

	//Display selected data
	int nCnt=0;
	for( int nI = 0; nI < PS_MAX_ITEM_NUM; nI++ )
	{
		//if (nI > 40 && dwItem[nI] == 0) continue;	//ljb 20150604 add
		strData = ::PSGetItemName(LOWORD(dwItem[nI]));	
		if (strData == "") continue;	//ljb 20150604 add
		if (nI > 40 && strData == "State") continue;	//ljb 20150604 add


		TRACE("%s \n", strData);

		if(!strData.IsEmpty())
		{
			m_itemList.AddString(strData);
			
 			//m_itemList.SetCheck(nI, HIWORD(dwItem[nI]));
 			//m_itemList.SetItemData(nI, LOWORD(dwItem[nI]));

			m_itemList.SetCheck(nCnt, HIWORD(dwItem[nI]));
			m_itemList.SetItemData(nCnt, LOWORD(dwItem[nI]));
			TRACE("ni=%d, %x \n",nCnt, LOWORD(dwItem[nI]));
			nCnt++;
		}
	}		

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUserSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "ExcelSaveOption", m_nExcelOp);
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "ExcelSaveSize", m_nExcelSize);

	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "TrayColSize", m_nTrayColSize);

	AfxGetApp()->WriteProfileBinary(REG_CONFIG_SECTION, "GridFont",(LPBYTE)&m_afont, sizeof(LOGFONT));
	
	char szBuff[32], szUnit[16], szDecimalPoint[16];
	m_ctrlVUnit.GetLBText(m_ctrlVUnit.GetCurSel(), szUnit);
	m_ctrlVDecimal.GetLBText(m_ctrlVDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "V Unit", szBuff);

	m_ctrlIUnit.GetLBText(m_ctrlIUnit.GetCurSel(), szUnit);
	m_ctrlIDecimal.GetLBText(m_ctrlIDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "I Unit", szBuff);

	m_ctrlCUnit.GetLBText(m_ctrlCUnit.GetCurSel(), szUnit); //
	m_ctrlCDecimal.GetLBText(m_ctrlCDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "C Unit", szBuff);

	m_ctrlCapaUnit.GetLBText(m_ctrlCapaUnit.GetCurSel(), szUnit);
	m_ctrlCapaDecimal.GetLBText(m_ctrlCapaDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "CAPA Unit", szBuff);

	m_ctrlWUnit.GetLBText(m_ctrlWUnit.GetCurSel(), szUnit);
	m_ctrlWDecimal.GetLBText(m_ctrlWDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "W Unit", szBuff);

	m_ctrlWhUnit.GetLBText(m_ctrlWhUnit.GetCurSel(), szUnit);
	m_ctrlWhDecimal.GetLBText(m_ctrlWhDecimal.GetCurSel(), szDecimalPoint);
	sprintf(szBuff, "%s %s", szUnit, szDecimalPoint);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "Wh Unit", szBuff);

	sprintf(szBuff, "%d", m_ctrlTimeUnit.GetCurSel());
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "Time Unit", szBuff);
	
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "Capacity Sum", m_bDispCapSum);

	//Save item 
	int nTot = m_itemList.GetCount();
	DWORD dwItem[PS_MAX_ITEM_NUM];
	memset(dwItem, PS_MAX_ITEM_NUM+1, sizeof(dwItem));	//존재하지 않는 숫자로 Setting한다.

	//for(int a=0; a<nTot && a<PS_MAX_ITEM_NUM; a++)
	for(int a=0; a<PS_MAX_ITEM_NUM; a++)
	{
		if (a<nTot)
		{
			dwItem[a] = MAKELONG(m_itemList.GetItemData(a), m_itemList.GetCheck(a));
		}
		else
		{
			dwItem[a] = MAKELONG(0, 0);
		}
	}

	//AfxGetApp()->WriteProfileBinary(REG_CONFIG_SECTION, "SaveItem_v100F",(LPBYTE)dwItem, sizeof(dwItem));
	AfxGetApp()->WriteProfileBinary(REG_CONFIG_SECTION, "SaveItem_v1016",(LPBYTE)dwItem, sizeof(dwItem)); //ksj 20200210

	sprintf(szBuff, "%d", m_fTime1);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "ImpTime1", szBuff);
	sprintf(szBuff, "%f", m_fTime2);
	AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION, "ImpTime2", szBuff);
	AfxGetApp()->WriteProfileInt(REG_CONFIG_SECTION, "UserImp", m_bUserImpTime);

	CDialog::OnOK();
}

void CUserSetDlg::OnFontSetBtn() 
{
	// TODO: Add your control notification handler code here
	CFontDialog aDlg(&m_afont);
	if(aDlg.DoModal()==IDOK)
	{
		aDlg.GetCurrentFont(&m_afont);
		m_ctrFontResult.SetUserFont(&m_afont);
	}	
}

void CUserSetDlg::OnUpButton() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_itemList.GetCurSel();
	if(nSel > 0)
	{
		DWORD itemData = m_itemList.GetItemData(nSel);
		BOOL bCheck = m_itemList.GetCheck(nSel);
		CString strItem;
		m_itemList.GetText(nSel, strItem);

		m_itemList.InsertString(nSel-1, strItem);
		m_itemList.SetItemData(nSel-1, itemData);
		m_itemList.SetCheck(nSel-1, bCheck);
		
		m_itemList.DeleteString(nSel+1);
		m_itemList.SetCurSel(nSel-1);
	}
}

void CUserSetDlg::OnDownButton() 
{
	// TODO: Add your control notification handler code here
	int nSel = m_itemList.GetCurSel();
	if(nSel >= 0 && nSel < m_itemList.GetCount()-1)
	{
		DWORD itemData = m_itemList.GetItemData(nSel);
		BOOL bCheck = m_itemList.GetCheck(nSel);
		CString strItem;
		m_itemList.GetText(nSel, strItem);

		m_itemList.InsertString(nSel+2, strItem);
		m_itemList.SetItemData(nSel+2, itemData);
		m_itemList.SetCheck(nSel+2, bCheck);
		
		m_itemList.DeleteString(nSel);
		m_itemList.SetCurSel(nSel+1);
	}	
}

void CUserSetDlg::OnCheck1() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	GetDlgItem(IDC_EDIT_TIME1)->EnableWindow(m_bUserImpTime);
	GetDlgItem(IDC_EDIT_TIME2)->EnableWindow(m_bUserImpTime);
}

void CUserSetDlg::OnRadio1() 
{
	GetDlgItem(IDC_STATIC_EXCEL_SIZE)->SetWindowText("Row");
	
}

void CUserSetDlg::OnRadio2() 
{
	GetDlgItem(IDC_STATIC_EXCEL_SIZE)->SetWindowText("KB");
	
}

void CUserSetDlg::OnButExportCheckAll() //yulee 20180729
{
	// TODO: Add your control notification handler code here
	for(int i = 0 ; i < m_itemList.GetCount(); i++)
		m_itemList.SetCheck(i, !bAuxListAllFlag);
	
	if(bAuxListAllFlag)
		//GetDlgItem(IDC_ButExportCheckAll)->SetWindowText("전체선택"); //$1013
		GetDlgItem(IDC_ButExportCheckAll)->SetWindowText(Fun_FindMsg("UserSetDlg_OnButExportCheckAll_msg1","IDD_USER_SET_DLG")); //&&
	else
		//GetDlgItem(IDC_ButExportCheckAll)->SetWindowText("전체해제"); //$1013
		GetDlgItem(IDC_ButExportCheckAll)->SetWindowText(Fun_FindMsg("UserSetDlg_OnButExportCheckAll_msg2","IDD_USER_SET_DLG")); //&&
	
	bAuxListAllFlag  = !bAuxListAllFlag;	
}
