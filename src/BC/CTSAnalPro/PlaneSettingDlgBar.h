/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: PlaneSettingDlgBar.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CPlaneSettingDialogBar class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_PLANESETTINGDLGBAR_H__23618E80_E1A0_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_PLANESETTINGDLGBAR_H__23618E80_E1A0_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PlaneSettingDlgBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPlaneSettingDialogBar window

// #include

#include "LineListBox.h"
class    CPlane;

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CPlaneSettingDialogBar
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CPlaneSettingDialogBar : public CDialogBar
{
// Construction
public:
	CPlaneSettingDialogBar();

// Attributes
public:
	int m_iCurSelPlaneIndex;

// Dialog Data
	//{{AFX_DATA(CPlaneSettingDialogBar)
	enum { IDD = IDD_PLANE_SETTING_DIALOGBAR , IDD2 = IDD_PLANE_SETTING_DIALOGBAR_ENG, IDD3 = IDD_PLANE_SETTING_DIALOGBAR_PL };
	CLineListBox	m_LineListBox;
	CComboBox	m_PlaneCombo;
	float	m_fltXAxisGrid;
	float	m_fltXAxisMax;
	float	m_fltXAxisMin;
	float	m_fltYAxisGrid;
	float	m_fltYAxisMax;
	float	m_fltYAxisMin;
	CString	m_strTestName;
	//}}AFX_DATA

// Operations
public:
	void AddPlaneIntoComboBox();
	void StandLinesInALine(CPlane* pPlane);
	void SetLineWidth(int width);
	void SetLineType(int type);
	void SetLineColor();
	int  GetSelLineCount();
	void TrackLine();
	void UpdateAxesRange();
	void ShowPattern();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPlaneSettingDialogBar)
	public:
	virtual BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID );
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

// Implementation
public:	
	CString GetTestName();
	void SetTestName(CString strTestName);
	virtual ~CPlaneSettingDialogBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPlaneSettingDialogBar)
	afx_msg void OnSelchangePlaneCombo();
	afx_msg void OnXaxisFontBtn();
	afx_msg void OnYaxisFontBtn();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnUpdateXAxisFont(CCmdUI* pCmdUI);
	afx_msg void OnUpdateYAxisFont(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PLANESETTINGDLGBAR_H__23618E80_E1A0_11D4_88DF_006008CEDA07__INCLUDED_)
