#if !defined(AFX_SAVEEXCELOPTIONDLG_H__A3EEC7EC_4E8D_4C22_8078_A60BD0AEC783__INCLUDED_)
#define AFX_SAVEEXCELOPTIONDLG_H__A3EEC7EC_4E8D_4C22_8078_A60BD0AEC783__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SaveExcelOptionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSaveExcelOptionDlg dialog

class CSaveExcelOptionDlg : public CDialog
{
// Construction
public:
	CSaveExcelOptionDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSaveExcelOptionDlg)
	enum { IDD = IDD_SAVE_EXCEL_OPTION_DLG , IDD2 = IDD_SAVE_EXCEL_OPTION_DLG_ENG , IDD3 = IDD_SAVE_EXCEL_OPTION_DLG_PL  };
	int		m_nExcelOp;
	int		m_nFileSize;
	int		m_nRowCount;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveExcelOptionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSaveExcelOptionDlg)
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVEEXCELOPTIONDLG_H__A3EEC7EC_4E8D_4C22_8078_A60BD0AEC783__INCLUDED_)
