// PointsetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanalpro.h"
#include "PointsetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPointsetDlg dialog


CPointsetDlg::CPointsetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CPointsetDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CPointsetDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CPointsetDlg::IDD3):
	(CPointsetDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CPointsetDlg)
	m_fPoint1 = 0.0f;
	m_fPoint2 = 0.0f;
	m_fPoint3 = 0.0f;
	m_fPoint4 = 0.0f;
	m_fPoint5 = 0.0f;
	m_strStep = _T("");
	m_bChk1 = FALSE;
	m_bChk2 = FALSE;
	m_bChk3 = FALSE;
	m_bChk4 = FALSE;
	m_bChk5 = FALSE;
	//}}AFX_DATA_INIT
}


void CPointsetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPointsetDlg)
	DDX_Text(pDX, IDC_EDIT_POINT1, m_fPoint1);
	DDV_MinMaxFloat(pDX, m_fPoint1, 0.f, 1000.f);
	DDX_Text(pDX, IDC_EDIT_POINT2, m_fPoint2);
	DDV_MinMaxFloat(pDX, m_fPoint2, 0.f, 1000.f);
	DDX_Text(pDX, IDC_EDIT_POINT3, m_fPoint3);
	DDV_MinMaxFloat(pDX, m_fPoint3, 0.f, 1000.f);
	DDX_Text(pDX, IDC_EDIT_POINT4, m_fPoint4);
	DDV_MinMaxFloat(pDX, m_fPoint4, 0.f, 1000.f);
	DDX_Text(pDX, IDC_EDIT_POINT5, m_fPoint5);
	DDV_MinMaxFloat(pDX, m_fPoint5, 0.f, 1000.f);
	DDX_Text(pDX, IDC_EDIT_STEP, m_strStep);
	DDX_Check(pDX, IDC_CHECK1, m_bChk1);
	DDX_Check(pDX, IDC_CHECK2, m_bChk2);
	DDX_Check(pDX, IDC_CHECK3, m_bChk3);
	DDX_Check(pDX, IDC_CHECK4, m_bChk4);
	DDX_Check(pDX, IDC_CHECK5, m_bChk5);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPointsetDlg, CDialog)
	//{{AFX_MSG_MAP(CPointsetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPointsetDlg message handlers

void CPointsetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	AfxGetApp()->WriteProfileInt(REG_POINT_SECTION, "USE Point 1", m_bChk1);
	AfxGetApp()->WriteProfileInt(REG_POINT_SECTION, "USE Point 2", m_bChk2);
	AfxGetApp()->WriteProfileInt(REG_POINT_SECTION, "USE Point 3", m_bChk3);
	AfxGetApp()->WriteProfileInt(REG_POINT_SECTION, "USE Point 4", m_bChk4);
	AfxGetApp()->WriteProfileInt(REG_POINT_SECTION, "USE Point 5", m_bChk5);

	CString strTemp;
	strTemp.Format("%.2f",m_fPoint1);	
	AfxGetApp()->WriteProfileString(REG_POINT_SECTION, "Point Data 1", strTemp);
	strTemp.Format("%.2f",m_fPoint2);	
	AfxGetApp()->WriteProfileString(REG_POINT_SECTION, "Point Data 2", strTemp);
	strTemp.Format("%.2f",m_fPoint3);	
	AfxGetApp()->WriteProfileString(REG_POINT_SECTION, "Point Data 3", strTemp);
	strTemp.Format("%.2f",m_fPoint4);	
	AfxGetApp()->WriteProfileString(REG_POINT_SECTION, "Point Data 4", strTemp);
	strTemp.Format("%.2f",m_fPoint5);	
	AfxGetApp()->WriteProfileString(REG_POINT_SECTION, "Point Data 5", strTemp);

	AfxGetApp()->WriteProfileString(REG_POINT_SECTION, "Point Step", m_strStep);

	UpdateData(FALSE);
	CDialog::OnOK();
}

BOOL CPointsetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdateData(TRUE);
	m_bChk1 = AfxGetApp()->GetProfileInt(REG_POINT_SECTION, "USE Point 1", 0);
	m_bChk2 = AfxGetApp()->GetProfileInt(REG_POINT_SECTION, "USE Point 2", 0);
	m_bChk3 = AfxGetApp()->GetProfileInt(REG_POINT_SECTION, "USE Point 3", 0);
	m_bChk4 = AfxGetApp()->GetProfileInt(REG_POINT_SECTION, "USE Point 4", 0);
	m_bChk5 = AfxGetApp()->GetProfileInt(REG_POINT_SECTION, "USE Point 5", 0);

	m_fPoint1 = atof(AfxGetApp()->GetProfileString(REG_POINT_SECTION, "Point Data 1", "0.0"));
	m_fPoint2 = atof(AfxGetApp()->GetProfileString(REG_POINT_SECTION, "Point Data 2", "0.0"));
	m_fPoint3 = atof(AfxGetApp()->GetProfileString(REG_POINT_SECTION, "Point Data 3", "0.0"));
	m_fPoint4 = atof(AfxGetApp()->GetProfileString(REG_POINT_SECTION, "Point Data 4", "0.0"));
	m_fPoint5 = atof(AfxGetApp()->GetProfileString(REG_POINT_SECTION, "Point Data 5", "0.0"));

	m_strStep = AfxGetApp()->GetProfileString(REG_POINT_SECTION, "Point Step", "");
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}