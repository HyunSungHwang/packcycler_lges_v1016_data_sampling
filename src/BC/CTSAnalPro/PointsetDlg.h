#if !defined(AFX_POINTSETDLG_H__A8B1F9D4_5E26_4A14_B078_6B5E52ED81ED__INCLUDED_)
#define AFX_POINTSETDLG_H__A8B1F9D4_5E26_4A14_B078_6B5E52ED81ED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PointsetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPointsetDlg dialog

class CPointsetDlg : public CDialog
{
	// Construction
public:
	CPointsetDlg(CWnd* pParent = NULL);   // standard constructor
	
	// Dialog Data
	//{{AFX_DATA(CPointsetDlg)
	enum { IDD = IDD_POINT_SET_DLG , IDD2 = IDD_POINT_SET_DLG_ENG , IDD3 = IDD_POINT_SET_DLG_PL };
	float	m_fPoint1;
	float	m_fPoint2;
	float	m_fPoint3;
	float	m_fPoint4;
	float	m_fPoint5;
	CString	m_strStep;
	BOOL	m_bChk1;
	BOOL	m_bChk2;
	BOOL	m_bChk3;
	BOOL	m_bChk4;
	BOOL	m_bChk5;
	//}}AFX_DATA
	
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPointsetDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CPointsetDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POINTSETDLG_H__A8B1F9D4_5E26_4A14_B078_6B5E52ED81ED__INCLUDED_)
