// PasswordDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "PasswordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPasswordDialog dialog


CPasswordDialog::CPasswordDialog(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CPasswordDialog::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CPasswordDialog::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CPasswordDialog::IDD3):
	(CPasswordDialog::IDD), pParent)
{
	//{{AFX_DATA_INIT(CPasswordDialog)
	m_strID = _T("");
	m_strPassword1 = _T("");
	m_strPassword2 = _T("");
	//}}AFX_DATA_INIT
}


void CPasswordDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPasswordDialog)
	DDX_Text(pDX, IDC_EDIT_ID, m_strID);
	DDX_Text(pDX, IDC_EDIT_PASSWORD1, m_strPassword1);
	DDX_Text(pDX, IDC_EDIT_PASSWORD2, m_strPassword2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPasswordDialog, CDialog)
	//{{AFX_MSG_MAP(CPasswordDialog)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPasswordDialog message handlers