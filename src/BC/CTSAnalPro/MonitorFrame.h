/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: MonitorFrame.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CMonitorFrame class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_MONITORFRAME_H__3AAEBDE2_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_MONITORFRAME_H__3AAEBDE2_F61A_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MonitorFrame.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMonitorFrame frame


// #include

#include "SelMdlDlgBar.h"

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CMonitorFrame
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CMonitorFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CMonitorFrame)
public:
	CMonitorFrame();           // protected constructor used by dynamic creation

// Attributes
public:
	CSelModuleDialogBar m_wndDialogBar;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMonitorFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMonitorFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	//{{AFX_MSG(CMonitorFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	//}}AFX_MSG
	afx_msg LRESULT OnCheckBackUpFile(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompleteOneCycle(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MONITORFRAME_H__3AAEBDE2_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
