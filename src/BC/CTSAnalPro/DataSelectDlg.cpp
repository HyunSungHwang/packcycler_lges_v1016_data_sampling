// DataSelectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanalpro.h"
#include "DataSelectDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataSelectDlg dialog


CDataSelectDlg::CDataSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CDataSelectDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CDataSelectDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CDataSelectDlg::IDD3):
	(CDataSelectDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CDataSelectDlg)
	m_lFrom = 0;
	m_lTo = 0;
	//}}AFX_DATA_INIT
	m_lMax = 0;
}


void CDataSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataSelectDlg)
	DDX_Text(pDX, IDC_EDIT_FROM, m_lFrom);
	DDX_Text(pDX, IDC_EDIT_TO, m_lTo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDataSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CDataSelectDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataSelectDlg message handlers

void CDataSelectDlg::OnOK() 
{
	// TODO: Add extra validation here

	UpdateData(TRUE);
	CString strTemp;
	if(m_lTo > m_lMax)
	{
		strTemp.Format(Fun_FindMsg("DataSelectDlg_OnOK_msg1","IDD_DATA_SELECT_DLG"),m_lMax); //&&
		AfxMessageBox(strTemp);
		return;
	}

	if(m_lFrom > m_lTo)
	{
		strTemp.Format(Fun_FindMsg("DataSelectDlg_OnOK_msg2","IDD_DATA_SELECT_DLG"),m_lMax); //&&
		AfxMessageBox(strTemp);
		return;
	}

	if(m_lFrom < 1)
	{
		strTemp.Format(Fun_FindMsg("DataSelectDlg_OnOK_msg3","IDD_DATA_SELECT_DLG"),m_lMax); //&&
		//$ strTemp.Format("시작 지점이 1보다 작을 수 없습니다.");
		AfxMessageBox(strTemp);
	}
	CDialog::OnOK();
}

BOOL CDataSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_lFrom = 1;
	m_lTo = m_lMax;
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}