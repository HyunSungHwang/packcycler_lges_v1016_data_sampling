#if !defined(AFX_SERVERSOCKET_H__313C2670_42F3_4469_93BD_88352603F5AA__INCLUDED_)
#define AFX_SERVERSOCKET_H__313C2670_42F3_4469_93BD_88352603F5AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServerSocket.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CServerSocket command target
#include "MonitorDoc.h"

class CServerSocket : public CSocket
{
// Attributes
public:
	CMonitorDoc* m_pDoc;
// Operations
public:
	CServerSocket(CMonitorDoc* pDoc);
	virtual ~CServerSocket();

// Overrides
public:
	BOOL CreateSocket();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServerSocket)
	public:
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CServerSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVERSOCKET_H__313C2670_42F3_4469_93BD_88352603F5AA__INCLUDED_)
