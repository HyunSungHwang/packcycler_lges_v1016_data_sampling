#if !defined(AFX_LOGDLG_H__3AAEBDE3_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_LOGDLG_H__3AAEBDE3_F61A_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogDialog dialog

// #include
//#include "BitmapDialog.h"

// #define

class CLogDialog : public CDialog
{
// Construction
public:
	CLogDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLogDialog)
	enum { IDD = IDD_LOG_DLG , IDD2 = IDD_LOG_DLG_ENG , IDD3 = IDD_LOG_DLG_PL };
//	CBitmapButton	m_EnterBtn;
//	CBitmapButton	m_CancelBtn;
//	CBitmapButton	m_ChangeSecurBtn;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLogDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnCancelMode();
	afx_msg void OnChangeBtn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
private:
	CString m_strID;
	CString m_strPassword;
	BOOL    m_bSkip;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGDLG_H__3AAEBDE3_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
