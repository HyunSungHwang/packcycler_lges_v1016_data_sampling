// RangeSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanalpro.h"
#include "RangeSelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRangeSelDlg dialog


CRangeSelDlg::CRangeSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CRangeSelDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CRangeSelDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CRangeSelDlg::IDD3):
	(CRangeSelDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CRangeSelDlg)
	m_lFrom = 1;
	//}}AFX_DATA_INIT
	m_lTo = MAX_RAW_DATA_COUNT;
	m_lMax = 0;
}


void CRangeSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRangeSelDlg)
	DDX_Text(pDX, IDC_FROM_EDIT, m_lFrom);
	DDX_Text(pDX, IDC_TO_EDIT, m_lTo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRangeSelDlg, CDialog)
	//{{AFX_MSG_MAP(CRangeSelDlg)
	ON_EN_CHANGE(IDC_FROM_EDIT, OnChangeFromEdit)
	ON_EN_CHANGE(IDC_TO_EDIT, OnChangeToEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRangeSelDlg message handlers

BOOL CRangeSelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString str;
	str.Format(Fun_FindMsg("OnInitDialog_msg1","IDD_RANGE_SEL_DIALOG"), m_lMax);
	//@ str.Format("입력 구간 : 1 ~ %d", m_lMax);
	GetDlgItem(IDC_MAX_STATIC)->SetWindowText(str);

	str.Format(Fun_FindMsg("OnInitDialog_msg2","IDD_RANGE_SEL_DIALOG"), MAX_RAW_DATA_COUNT);
	//@ str.Format("(구간은 최대 %d개 이하로 설정하기를 권장합니다.  시스템 사양에 따라  표시하는데 시간이 오래 걸리수 있습니다.)", MAX_RAW_DATA_COUNT);
	GetDlgItem(IDC_WARING_STATIC)->SetWindowText(str);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRangeSelDlg::OnChangeFromEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString str;
	str.Format(Fun_FindMsg("OnChangeFromEdit_msg","IDD_RANGE_SEL_DIALOG"), m_lTo-m_lFrom+1);
	//@ str.Format("구간 : %d", m_lTo-m_lFrom+1);
	GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(str);
}

void CRangeSelDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	if(m_lFrom > m_lTo)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg1","IDD_RANGE_SEL_DIALOG"), MB_OK|MB_ICONSTOP);
		//@ AfxMessageBox("구간 설정이 잘못되었습니다.", MB_OK|MB_ICONSTOP);//$
		return;
	}

	if(m_lFrom > m_lMax)
	{
		AfxMessageBox(Fun_FindMsg("OnOK_msg2","IDD_RANGE_SEL_DIALOG"), MB_OK|MB_ICONSTOP);
		//@ AfxMessageBox("시작 구간이 범위를 벗어난 구간입니다.", MB_OK|MB_ICONSTOP);
		return;
	}

	if(m_lTo-m_lFrom+1 > MAX_RAW_DATA_COUNT)
	{
		if(AfxMessageBox(Fun_FindMsg("OnOK_msg3","IDD_RANGE_SEL_DIALOG"),
			//@ if(AfxMessageBox("선택된 구간이 너무 큽니다. 표시하는데 시간이 걸리수 있습니다.\n\n진행 하시겠습니까?",
						MB_ICONQUESTION|MB_YESNO) == IDNO)
						return;
	}

	CDialog::OnOK();
}

void CRangeSelDlg::OnChangeToEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CString str;
	str.Format(Fun_FindMsg("OnChangeToEdit_msg","IDD_RANGE_SEL_DIALOG"), m_lTo-m_lFrom+1);
	//@ str.Format("구간 : %d", m_lTo-m_lFrom+1);
	GetDlgItem(IDC_COUNT_STATIC)->SetWindowText(str);	
}