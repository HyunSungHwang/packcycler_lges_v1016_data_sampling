// UpdatedSchListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanalpro.h"
#include "UpdatedSchListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUpdatedSchListDlg dialog


CUpdatedSchListDlg::CUpdatedSchListDlg(CString strPath, CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CUpdatedSchListDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CUpdatedSchListDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CUpdatedSchListDlg::IDD3):
	(CUpdatedSchListDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CUpdatedSchListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_strPath = strPath + "\\Update";
}


void CUpdatedSchListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUpdatedSchListDlg)
	DDX_Control(pDX, IDC_UPDATE_SCH_LIST, m_ctrlUpdateSchList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUpdatedSchListDlg, CDialog)
	//{{AFX_MSG_MAP(CUpdatedSchListDlg)
	ON_NOTIFY(HDN_ITEMDBLCLICK, IDC_UPDATE_SCH_LIST, OnItemdblclickUpdateSchList)
	ON_NOTIFY(NM_DBLCLK, IDC_UPDATE_SCH_LIST, OnDblclkUpdateSchList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUpdatedSchListDlg message handlers

BOOL CUpdatedSchListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	InitList();
	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUpdatedSchListDlg::OpenSch(CString strFileName)
{

	CFileFind afinder;
	
	if(!afinder.FindFile(strFileName))
	{
		//AfxMessageBox(("Find"))
		//AfxMessageBox("sch 파일을 찾을 수 없습니다.");
		AfxMessageBox(Fun_FindMsg("UpdatedSchListDlg_OpenSch_msg1","UPDATEDSCHLIST_DLG"));//&&
		return;	
	}
	
	CScheduleData *pScheduleData = new CScheduleData;
	if(!pScheduleData->SetSchedule(strFileName))
	{
		delete pScheduleData;
		pScheduleData = NULL;
		return;
	}
	
	pScheduleData->ShowContent();
	
	if(pScheduleData != NULL)
	{
		delete pScheduleData;
		pScheduleData = NULL;
	}
}

void CUpdatedSchListDlg::InitList()
{
	m_ctrlUpdateSchList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_ctrlUpdateSchList.InsertColumn(0, _T("No"), LVCFMT_CENTER, 30);
	//m_ctrlUpdateSchList.InsertColumn(1, _T("업데이트 시간"), LVCFMT_LEFT, 170);
	m_ctrlUpdateSchList.InsertColumn(1, Fun_FindMsg("UpdatedSchListDlg_InitList_msg1","UPDATEDSCHLIST_DLG"), LVCFMT_LEFT, 170);//&&
	m_ctrlUpdateSchList.InsertColumn(2, _T("Model"), LVCFMT_LEFT, 230);
	m_ctrlUpdateSchList.InsertColumn(3, _T("Schedule"), LVCFMT_LEFT, 230);
	//m_ctrlUpdateSchList.InsertColumn(4, _T("스케쥴 수정 시간"), LVCFMT_LEFT, 180);
	m_ctrlUpdateSchList.InsertColumn(4, Fun_FindMsg("UpdatedSchListDlg_InitList_msg2","UPDATEDSCHLIST_DLG"), LVCFMT_LEFT, 180); //&&
	m_ctrlUpdateSchList.InsertColumn(5, _T("path"), LVCFMT_LEFT, 0);

	m_ctrlUpdateSchList.DeleteAllItems();

	UpdateList(m_strPath);
	
}

void CUpdatedSchListDlg::OnOK() 
{
	// TODO: Add extra validation here

	POSITION pos = m_ctrlUpdateSchList.GetFirstSelectedItemPosition();
	int nIndex  = m_ctrlUpdateSchList.GetNextSelectedItem(pos);

	if(nIndex >= 0) OpenSch(m_ctrlUpdateSchList.GetItemText(nIndex, 5));
	
	//CDialog::OnOK();
}

void CUpdatedSchListDlg::UpdateList(CString strPath)
{
	if(strPath.IsEmpty())	return;

	CScheduleData stScheduleData;
	CFileFind afinder;
	CString strName;
	CString strTemp;
	LVITEM lvItem;
	BOOL bWorking = afinder.FindFile(strPath+"\\*.*");
	int nItemIndex = 0;
	CTime time;
	COleDateTime date;
	
	while(bWorking)
	{
		bWorking = afinder.FindNextFile();
		strName = afinder.GetFilePath();

		if(afinder.IsDirectory() && afinder.GetFileName() != "." && afinder.GetFileName() != "..")
		{
			UpdateList(strName); //폴더면 재귀호출.
			continue;
		}		
		
		if(stScheduleData.SetSchedule(strName))
		{
			nItemIndex = m_ctrlUpdateSchList.GetItemCount();
			//번호
			::ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.mask = LVIF_TEXT;
			lvItem.iItem = nItemIndex;
			lvItem.iSubItem = 0;
			strTemp.Format(_T("%d"),nItemIndex+1);
			lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
			
			m_ctrlUpdateSchList.InsertItem(&lvItem);
			
			//업데이트 날짜
			::ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.mask = LVIF_TEXT;
			lvItem.iItem = nItemIndex;
			lvItem.iSubItem = 1;
			afinder.GetLastWriteTime(time);
			//strTemp.Format(_T("%d-%02d-%02d %02d:%02d:%02d"),time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
			date.SetDateTime(time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
			strTemp.Format(_T("%s"),date.Format());			
			
	
			lvItem.pszText = (LPSTR)(LPCTSTR)strTemp;
			
			m_ctrlUpdateSchList.SetItem(&lvItem);
			//모델
			::ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.mask = LVIF_TEXT;
			lvItem.iItem = nItemIndex;
			lvItem.iSubItem = 2;
			lvItem.pszText = (LPSTR)(LPCTSTR)stScheduleData.GetModelName();
			
			m_ctrlUpdateSchList.SetItem(&lvItem);
			//스케쥴
			::ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.mask = LVIF_TEXT;
			lvItem.iItem = nItemIndex;
			lvItem.iSubItem = 3;
			lvItem.pszText = (LPSTR)(LPCTSTR)stScheduleData.GetScheduleName();
			
			m_ctrlUpdateSchList.SetItem(&lvItem);
			//스케쥴 수정 날짜
			::ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.mask = LVIF_TEXT;
			lvItem.iItem = nItemIndex;
			lvItem.iSubItem = 4;
			lvItem.pszText = (LPSTR)(LPCTSTR)stScheduleData.GetScheduleEditTime();
			
			m_ctrlUpdateSchList.SetItem(&lvItem);
			//sch 경로
			::ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.mask = LVIF_TEXT;
			lvItem.iItem = nItemIndex;
			lvItem.iSubItem = 5;
			lvItem.pszText = (LPSTR)(LPCTSTR)strName;
			
			m_ctrlUpdateSchList.SetItem(&lvItem);
		}
		
	}
}

void CUpdatedSchListDlg::OnItemdblclickUpdateSchList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;
	// TODO: Add your control notification handler code here

	OnOK();
	
	*pResult = 0;
}

void CUpdatedSchListDlg::OnDblclkUpdateSchList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnOK();
	*pResult = 0;
}