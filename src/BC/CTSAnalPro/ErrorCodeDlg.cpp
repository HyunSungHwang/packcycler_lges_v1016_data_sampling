// ErrorCodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "ErrorCodeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CErrorCodeDlg dialog


CErrorCodeDlg::CErrorCodeDlg(int nModuleID, int nErrorCode, CWnd* pParent /*=NULL*/)
	: CDialog(CErrorCodeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CErrorCodeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_nUnitID = nModuleID / 10;
	m_nModuleID = nModuleID % (m_nUnitID * 10);
	m_nErrorCode = nErrorCode;
}


void CErrorCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CErrorCodeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CErrorCodeDlg, CDialog)
	//{{AFX_MSG_MAP(CErrorCodeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CErrorCodeDlg message handlers

BOOL CErrorCodeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	Initialize();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CErrorCodeDlg::Initialize()
{
	SetDlgItemText(IDC_EDIT_TIME, CTime::GetCurrentTime().Format("%Y/%m/%d %H:%M:%S"));

	CString strMsg;
	strMsg.Format(Fun_FindMsg("Initialize_msg1","IDD_ERROR_CODE_DLG")
		//@ strMsg.Format("Unit %d - Module %d 에 아래의 현상이 발생했습니다. \n"
		+ Fun_FindMsg("Initialize_msg2","IDD_ERROR_CODE_DLG"), 
		//@ "아래의 Code와 원인을 확인하시고 \n조치하시기 바랍니다.", 
		m_nUnitID, m_nModuleID);
	SetDlgItemText(IDC_EDIT_MODULE, strMsg);
	
	strMsg.Format("0x%02X", (BYTE)m_nErrorCode);
	SetDlgItemText(IDC_EDIT_CODE, strMsg);

	switch( m_nErrorCode )
	{
		case 0x00 :	strMsg = _T(Fun_FindMsg("Initialize_msg3","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x00 :	strMsg = _T("이상무");	break;
		case 0x01 :	strMsg = _T(Fun_FindMsg("Initialize_msg4","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x01 :	strMsg = _T("입력 전원 정전");	break;
		case 0x02 :	strMsg = _T(Fun_FindMsg("Initialize_msg5","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x02 :	strMsg = _T("UPS Battery 이상");	break;
		case 0x03 :	strMsg = _T(Fun_FindMsg("Initialize_msg6","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x03 :	strMsg = _T("Emergency Switch 눌렸음");	break;
		case 0x04 :	strMsg = _T(Fun_FindMsg("Initialize_msg7","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x04 :	strMsg = _T("LAN 통신 이상");	break;
		case 0x05 :	strMsg = _T(Fun_FindMsg("Initialize_msg8","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x05 :	strMsg = _T("직접 Module Shutdonw 시켰음");	break;
		case 0x06 :	strMsg = _T(Fun_FindMsg("Initialize_msg9","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x06 :	strMsg = _T("Controller 오동작 감지");	break;
		case 0x07 :	strMsg = _T(Fun_FindMsg("Initialize_msg10","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x07 :	strMsg = _T("DVM 통신 이상");	break;
		case 0x08 :	strMsg = _T(Fun_FindMsg("Initialize_msg11","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x08 :	strMsg = _T("Calibrator 통신 이상");	break;
		case 0x09 : strMsg = _T(Fun_FindMsg("Initialize_msg12","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x09 : strMsg = _T("Network로 Module Shutdown 시켰음");	break;
		case 0x0A : strMsg = _T(Fun_FindMsg("Initialize_msg13","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x0A : strMsg = _T("Terminal로 Module Shutdown 시켰음");	break;
		case 0x0B : strMsg = _T(Fun_FindMsg("Initialize_msg14","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x0B : strMsg = _T("Terminal로 Program 종료 시켰음");	break;
		case 0x20 :	strMsg = _T(Fun_FindMsg("Initialize_msg15","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x20 :	strMsg = _T("장비 내부 과열");	break;
		case 0x21 :	strMsg = _T(Fun_FindMsg("Initialize_msg16","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x21 :	strMsg = _T("장비 내부 AD Convertor 고장");	break;
		case 0x22 : strMsg = _T(Fun_FindMsg("Initialize_msg17","IDD_ERROR_CODE_DLG")); break;
			//@ case 0x22 : strMsg = _T("측정전압 범위가 허용 Range를 벗어남. 전압 Calibration 요망"); break;
		case 0x40 :	strMsg = _T(Fun_FindMsg("Initialize_msg18","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x40 :	strMsg = _T("Jig 온도 이상");	break;
		case 0x41 :	strMsg = _T(Fun_FindMsg("Initialize_msg19","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x41 :	strMsg = _T("Jig 연기 감지");	break;
		case 0x42 :	strMsg = _T(Fun_FindMsg("Initialize_msg20","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x42 :	strMsg = _T("Jig GAS 감지");	break;
		case 0x43 :	strMsg = _T(Fun_FindMsg("Initialize_msg21","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x43 :	strMsg = _T("Jig Door Open");	break;
		default :	strMsg = _T(Fun_FindMsg("Initialize_msg22","IDD_ERROR_CODE_DLG"));	break;
			//@ default :	strMsg = _T("Unknown Error");	break;
	}	
	SetDlgItemText(IDC_EDIT_MESSAGE, strMsg);
}

CString CErrorCodeDlg::GetErrorMessage()
{
	CString strMsg;
	switch( m_nErrorCode )
	{
		case 0x00 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg1","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x00 :	strMsg = _T("이상무");	break;
		case 0x01 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg2","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x01 :	strMsg = _T("입력 전원 정전");	break;
		case 0x02 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg3","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x02 :	strMsg = _T("UPS Battery 이상");	break;
		case 0x03 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg4","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x03 :	strMsg = _T("Emergency Switch 눌렸음");	break;
		case 0x04 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg5","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x04 :	strMsg = _T("LAN 통신 이상");	break;
		case 0x05 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg6","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x05 :	strMsg = _T("직접 Module Shutdonw 시켰음");	break;
		case 0x06 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg7","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x06 :	strMsg = _T("Controller 오동작 감지");	break;
		case 0x07 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg8","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x07 :	strMsg = _T("DVM 통신 이상");	break;
		case 0x08 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg9","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x08 :	strMsg = _T("Calibrator 통신 이상");	break;
		case 0x09 : strMsg = _T(Fun_FindMsg("GetErrorMessage_msg10","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x09 : strMsg = _T("Network로 Module Shutdown 시켰음");	break;
		case 0x0A : strMsg = _T(Fun_FindMsg("GetErrorMessage_msg11","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x0A : strMsg = _T("Terminal로 Module Shutdown 시켰음");	break;
		case 0x0B : strMsg = _T(Fun_FindMsg("GetErrorMessage_msg12","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x0B : strMsg = _T("Terminal로 Program 종료 시켰음");	break;
		case 0x20 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg13","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x20 :	strMsg = _T("장비 내부 과열");	break;
		case 0x21 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg14","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x21 :	strMsg = _T("장비 내부 AD Convertor 고장");	break;
		case 0x22 : strMsg = _T(Fun_FindMsg("GetErrorMessage_msg15","IDD_ERROR_CODE_DLG")); break;
			//@ case 0x22 : strMsg = _T("측정전압 범위가 허용 Range를 벗어남. 전압 Calibration 요망"); break;
		case 0x40 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg16","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x40 :	strMsg = _T("Jig 온도 이상");	break;
		case 0x41 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg17","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x41 :	strMsg = _T("Jig 연기 감지");	break;
		case 0x42 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg18","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x42 :	strMsg = _T("Jig GAS 감지");	break;
		case 0x43 :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg19","IDD_ERROR_CODE_DLG"));	break;
			//@ case 0x43 :	strMsg = _T("Jig Door Open");	break;
		default :	strMsg = _T(Fun_FindMsg("GetErrorMessage_msg20","IDD_ERROR_CODE_DLG"));	break;
			//@ default :	strMsg = _T("Unknown Error");	break;
	}	

	CString strRet;
	strRet.Format(Fun_FindMsg("GetErrorMessage_msg21","IDD_ERROR_CODE_DLG"),
		//@ strRet.Format("Unit %d - Module %d : [Error Code : 0x%02X] [원인 : %s]",
		m_nUnitID, m_nModuleID, (BYTE)m_nErrorCode, strMsg);
	return strRet;
}