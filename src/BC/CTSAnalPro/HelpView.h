/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: HelpView.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CHelpView class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_HELPVIEW_H__D4FEE121_8CE4_4FBF_BC25_23848E2188C8__INCLUDED_)
#define AFX_HELPVIEW_H__D4FEE121_8CE4_4FBF_BC25_23848E2188C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HelpView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHelpView html view

// #include
#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
#include <afxhtml.h>

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CHelpView
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CHelpView : public CHtmlView
{
protected:
	CHelpView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CHelpView)

// html Data
public:
	//{{AFX_DATA(CHelpView)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:
	CHelpDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHelpView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHelpView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CHelpView)
	afx_msg void OnMoveNext();
	afx_msg void OnMoveHone();
	afx_msg void OnMovePrev();
	afx_msg void OnMoveRefresh();
	afx_msg void OnMoveStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in htmltestView.cpp
inline CHelpDoc* CHelpView::GetDocument()
   { return (CHelpDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HELPVIEW_H__D4FEE121_8CE4_4FBF_BC25_23848E2188C8__INCLUDED_)
