#if !defined(AFX_ERRORCODEDLG_H__2D08A8B1_99E3_4121_80A6_583A89B77F48__INCLUDED_)
#define AFX_ERRORCODEDLG_H__2D08A8B1_99E3_4121_80A6_583A89B77F48__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ErrorCodeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CErrorCodeDlg dialog

class CErrorCodeDlg : public CDialog
{
// Construction
public:	
	CString GetErrorMessage();
	int m_nUnitID;
	int m_nModuleID;
	int m_nErrorCode;

	void Initialize();
	CErrorCodeDlg(int nModuleID, int nErrorCode, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CErrorCodeDlg)
	enum { IDD = IDD_ERROR_CODE_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CErrorCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CErrorCodeDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ERRORCODEDLG_H__2D08A8B1_99E3_4121_80A6_583A89B77F48__INCLUDED_)
