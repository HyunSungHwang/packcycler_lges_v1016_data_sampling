// CTSAnalProDoc.cpp : implementation of the CCTSAnalProDoc class
//

#include "stdafx.h"
#include "CTSAnalPro.h"

#include "CTSAnalProDoc.h"
#include "ChildFrm.h"
#include "PlaneSettingDlgBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProDoc

IMPLEMENT_DYNCREATE(CCTSAnalProDoc, CDocument)

BEGIN_MESSAGE_MAP(CCTSAnalProDoc, CDocument)
	//{{AFX_MSG_MAP(CCTSAnalProDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProDoc construction/destruction

CCTSAnalProDoc::CCTSAnalProDoc()
{
	// TODO: add one-time construction code here
	m_iCurSelPlaneIndex = 0;
}

CCTSAnalProDoc::~CCTSAnalProDoc()
{
}

#include "MainFrm.h"
#include "MonitorFrame.h"
#include "MonitorDoc.h"

BOOL CCTSAnalProDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

//	POSITION pos = GetFirstDocTemplatePosition();
//	CMultiDocTemplate *pTempate;
//	pTempate = (CMultiDocTemplate *)GetNextDocTemplate(pos);

//	CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
//	if(pMainFrm == NULL)	return FALSE;
//	CMonitorFrame *pMonFrm = (CMonitorFrame *)pMainFrm->GetActiveFrame();
//	if(pMonFrm == NULL)	return FALSE;
//	CMonitorDoc *pMonDoc = (CMonitorDoc *)pMonFrm->GetActiveDocument();
//	if(pMonDoc == NULL)	return FALSE;

//	CString strName, strTemp;
//	CStringList *pList = pMonDoc->GetSelFileList();
//	POSITION pos = pList->GetHeadPosition();
//	
//	while(pos)
//	{
//		strTemp.Format("%s\\%s?", pMonDoc->GetDataPath(), pList->GetNext(pos));
//		strName += strTemp;
	//	}

	init_Language();

	if( !m_Data.QueryData(NULL))				
		return FALSE;
	
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProDoc serialization

void CCTSAnalProDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProDoc diagnostics

#ifdef _DEBUG
void CCTSAnalProDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCTSAnalProDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCTSAnalProDoc commands

BOOL CCTSAnalProDoc::RequeryDataQuery() 
{
	if(m_Data.QueryData(NULL))
	{
//		POSITION pos = GetFirstViewPosition();
//		((CChildFrame*)(GetNextView(pos)->GetParentFrame()))->m_wndDialogBar.AddPlaneIntoComboBox();
		UpdateAllViews(NULL);
		return TRUE;
	}
	return FALSE;
}

void CCTSAnalProDoc::DrawView(CDC* pDC, CRect ClientRect)
{
	m_Data.DrawPlaneOnView(pDC, ClientRect);
}

BOOL CCTSAnalProDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	return m_Data.QueryData(((CMainFrame *)AfxGetMainWnd())->m_strCopyMsgString);
}

void CCTSAnalProDoc::ReloadData()
{
	m_Data.ReLoadData();
	UpdateAllViews(NULL);
	return;
}

//lmh 20120207
BOOL CCTSAnalProDoc::RequeryIndexDataQuery(CString strPath, CDWordArray *apIndexArray)
{
	// 	ASSERT(apIndexArray);
	// 	for(int i=0; i<apIndexArray->GetSize(); i++)
	// 	{
	// 		TRACE("Sel Index %d\n", apIndexArray->GetAt(i));
	// 	}
	//	m_Data.SetQueryMode(QUERY_MODE_INDEX, apIndexArray);
	m_Data.QueryData(strPath, FALSE, apIndexArray, 1);
	return TRUE;
}