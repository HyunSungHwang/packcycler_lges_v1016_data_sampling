#if !defined(AFX_GRAPHSETDLG_H__B87D0D25_1BA4_46EB_B752_951FB24EAD7E__INCLUDED_)
#define AFX_GRAPHSETDLG_H__B87D0D25_1BA4_46EB_B752_951FB24EAD7E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GraphSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGraphSetDlg dialog

#include "LineListBox.h"

class    CPlane;
class    CCTSAnalProDoc;


class CGraphSetDlg : public CDialog
{
// Construction
public:	
	BOOL UpdateParam();
	void SetTestName(CString strTestName);
	void StandLinesInALine(CPlane* pPlane);
	void AddPlaneIntoComboBox();
	CGraphSetDlg(CCTSAnalProDoc *pDoc, CWnd* pParent = NULL);   // standard constructor

	int m_iCurSelPlaneIndex;
	CCTSAnalProDoc *m_pDoc;

// Dialog Data
	//{{AFX_DATA(CGraphSetDlg)
	enum { IDD = IDD_GRAPH_SET_DLG ,IDD2 = IDD_GRAPH_SET_DLG_ENG , IDD3 = IDD_GRAPH_SET_DLG_PL };
	CListBox	m_YItemList;
	CLineListBox	m_LineListBox;
	CComboBox	m_PlaneCombo;
	float	m_fltXAxisGrid;
	float	m_fltXAxisMax;
	float	m_fltXAxisMin;
	float	m_fltYAxisGrid;
	float	m_fltYAxisMax;
	float	m_fltYAxisMin;
	CString	m_strTestName;
	BOOL	m_bXGrid;
	BOOL	m_bYGrid;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGraphSetDlg)

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGraphSetDlg)
	afx_msg void OnSelchangePlaneCombo();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnXaxisFontBtn();
	afx_msg void OnYaxisFontBtn();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnApply();
	afx_msg void OnSelchangeYItemList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHSETDLG_H__B87D0D25_1BA4_46EB_B752_951FB24EAD7E__INCLUDED_)
