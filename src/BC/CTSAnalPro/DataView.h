#if !defined(AFX_DATAVIEW_H__ADD07C71_4F4E_4F11_A9EB_918693785425__INCLUDED_)
#define AFX_DATAVIEW_H__ADD07C71_4F4E_4F11_A9EB_918693785425__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDataView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
#include <vector>
#include "NewGridWnd.h"
#include "CheckComboBox.h"
#include "TreeCtrlEx.h"

//#define MAX_EXCEL_ROW	20
#include "RangeSelDlg.h"

typedef union s_uniCANVALUE{
	long	lVal[2];
	float	fVal[2];
	char	strVal[8];
}SFT_CAN_VALUE;

class CDataView : public CFormView
{
protected:
	CDataView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDataView)

// Form Data
public:
	//{{AFX_DATA(CDataView)
	enum { IDD = IDD_DATA_FORM , IDD2 = IDD_DATA_FORM_ENG , IDD3 = IDD_DATA_FORM_PL };
	CCheckComboBox	m_StepSelCombo2;
	CLabel	m_ctrlCurStep;
	CComboBox	m_StepSelCombo;
	CListCtrl	m_ctrlTestInfoList;
	CTreeCtrlEx	m_TestTree; 
	CString	m_strRangeString;
	//}}AFX_DATA

// Attributes
public:
	char * m_pData;
	CString m_strPath;//lmh 20120612 add
	CStringList m_strStepList;
	bool	m_bChNodeSel;
	CString m_strStepEndVolt; //yulee 20180419
	CString m_strStepEndVoltToExcel; //yulee 20180419
	CString m_strPauseToExcel; //yulee 20180419
	BOOL m_bAccCycleOffSet;	//lyj 20201013 세인 ENG 누적사이클 화면표시 +1
	BOOL m_bSein;
	BOOL m_bProgressing ;

// Operations
public:
	int m_nMaxExcelFileSize;
	int m_nExcelOp;
	ULONG m_nWrittenSize;
	BOOL ExcelStepData(BOOL bSelStep = FALSE);
//	void InitStepEndGrid();
	BOOL DisplayData(HTREEITEM hItem);
	int SaveToExcelFile(CString strFilter, CString strSaveFile);
	int SaveToExcelFileFromView(CString strFilter, CString strSaveFile);
	BOOL AddStepStartData();
	CString GetUnitString(int nItem);
	CString ValueString(double dData, int item, BOOL bUnit = FALSE, double dData2 = 0.0);
//	void UpdateUnitSetting();
//	BOOL m_bShowChList;
//	BOOL UpdateRangeList();
	BOOL UpdateRangeList(CString strRangeData);

	void DisplayCurSelCh();
//	void LoadSelChData();	// Commented by 224 (2014/02/13) 
	CString GetDefaultExcelFileName(CString strTestPath, CString strChName);
//	void InitChListGrid();
//	BOOL ExecuteExcel(CString strFileName);
	void UpdateTestInfoList(CString strChName);
	void InitTestInfoList();
//	void InitRawDataGrid();
	void UpdateStepRawData(LONG	lTableIndex, BOOL bShowStepStartData = FALSE);
	void CopyDataToClipBoard(CListCtrl *pListCtrl);
	BOOL UpdateStepData(CString strChPath);
	void UpdateStepData2(CString strChPath);
	BOOL FildStepPointData(LONG lTableIndex,float **ppPoint);
	int  CtsFileCheckNSetRecordStructure(CString strChPath);//yulee 20190521 스케쥴 파일 확인 후
	bool CtsFileMapping(CString strChPath, BOOL nType = TRUE);//lmh 20120612 add  //20180309 yulee
	bool CtsFileMappingNewRecordST(CString strChPath, BOOL nType = TRUE);//yulee 20190521
	bool CtsFileMappingNewRecordST_v1013_CWA(CString strChPath, BOOL nType = TRUE);//yulee 20190521
	bool CycFileMapping(CString strChPath, int iRow);//lmh 20120613
	bool CycFileMapping2(CString strChPath, int iRow);	//2013/12/22 (224) : 빅파일 처리
	bool CycFileMapping3(CString strChPath, int iRow);	//2013/12/26 (224) : 분할파일처리
	bool CycFileMapping4(CString strChPath, int nStependRow, int nStartRec, int nShowRecs);	//2014/06/28 (224) : Pagination 처리
	INT m_nSelStependRow ;
	INT m_nRecordsPerPage ;
	INT m_nBaseSection ;	// 현재 전시된 1~10까지의 Pagination 의 누적 페이지
	INT m_nTotRecords ; // 현재 선택된 Stepend 의 총 Raw 레코드 수 
	INT m_nSizeOfResultFileHeader; //yulee 20191029 
	INT m_nResultSTSel;
	void OnDivideFile(CString strChPath, int iRow);	// 파일 분할 테스트
	
//	bool CycFileMapping3(CString strChPath, int iRow);//lmh 20120613
//	bool CycFileMappingWrite(CString strChPath, int iRow);//ljb 20131128 -> raw data file write
	bool Fun_MakCanAndAuxStepEnd(CString strChPath);//ljb Step End File 다시 만들기
	
	void SetItemData(PS_STEP_END_RECORD &chData, WORD wItem, float fData);//lmh 20120613
	void LoadAuxTitle(CString strAuxConfigPath, PS_AUX_DISP_INFO* pAuxDispInfo);//lmh 21020614
	void LoadCanTitle(CString strCanConfigPath, PS_CAN_DISP_INFO* pCanDispInfo);//lmh 20120614
	bool SaveToExcelFileFromCts(CString strFilter, CString strSaveFile);//lmh 20120615 add
	bool SaveToExcelFileFromCts_v1013_CWA(CString strFilter,CString strSaveFile);
//	bool SaveToExcelFileFromCyc(BOOL bSelStep, HTREEITEM hItem, BOOL bSelCh = false);//lmh 20120618 add

//	void UpdateStepDataA(CString strChPath);
	void ListUpTree();
	void OnGridCopy();
	BOOL UpdateDataList();
	BOOL ExecutePreAnal(CString strResPath, int nVersion); //yulee 20190521_1
	BOOL ExecuteProgram (	CString strProgram, 
		CString strArgument = "", 
		CString strClassName = "", 
		CString strWindowName = "", 
		BOOL bNewExe = FALSE,
		BOOL bWait = FALSE
						);//yulee 20190521_1
	BOOL m_bPreAnalOpenFlag;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	int		m_nSetAnalStepTo;
	int		m_nSetAnalStepFrom;
	BOOL	m_bSetAnalStepSize;
	unsigned long m_lstartindex; //lyj 20200504

// Implementation
protected:
	float	**ppPointData;

	HTREEITEM hOldSelectItem;			//ljb 20110125 같은 아이템 클릭이면 리턴 처리		
	int		m_nDisplyMode;		//★★ ljb 201012  ★★★★★★★★
	int		m_nDisplyOption;	//★★ ljb 201012  ★★★★★★★★

	BOOL m_bInitedTooltip;
	void InitTooltip();
	CToolTipCtrl m_tooltip;
	BOOL MakeNextFile(FILE *fp,  int &nFileCount, CString strFile);
//	BOOL ExcelAllStepData(BOOL bSelStep = FALSE);
#if (1 == 0)
	BOOL ExcelAllStepData(BOOL bSelStep, HTREEITEM hItem, BOOL bSelCh = false);
#endif
	
	// Added by 224 (2014/01/30) : 액샐변경 방식 저장
	BOOL ExcelAllStepData2(BOOL bSelStep, HTREEITEM hItem, BOOL bSelCh = false);

	// Added by 224 (2014/07/28) : 액샐변경 방식 저장
	BOOL ExcelAllStepData3(BOOL bSelStep, HTREEITEM hItem, BOOL bSelCh = false);

	// Added by 224 (2014/07/25) : 채널 2개의 컬림이 다른경우 처리
	//BOOL GetColumnHeader(CString strPath, CString& strHeaderString, INT& nAuxTempCount, INT& nAuxVoltCount,INT& nAuxTempTHCount, INT& nCanCOunt) ;

	 //ksj 20200116 : v1016 습도 센서 추가
	BOOL GetColumnHeader(CString strPath, CString& strHeaderString, INT& nAuxTempCount, INT& nAuxVoltCount,INT& nAuxTempTHCount, INT& nAuxHumiCount, INT& nCanCOunt);

	CUnitTrans m_UnitTrans;
	BOOL m_bReturn;
//	int	m_nRawGridWidth;

	int m_nCurrentUnitMode;
	
/*	int m_nTimeUnit;
	CString m_strVUnit;
	int m_nVDecimal ;
	CString m_strIUnit;
	int m_nIDecimal;
	CString m_strCUnit;
	int m_nCDecimal;
	CString m_strWUnit;
	int m_nWDecimal;
	CString m_strWhUnit;
	int m_nWhDecimal;
*/
	CList<LONG, LONG&> m_RangeList;
//	void UpdateChListData(CString strPath);
//	CGridCtrl	m_wndChListGrid;
	CGridCtrl	m_wndStepGrid;
//	CGridCtrl	m_wndRawDataGrid;

	CMyGridWnd	m_wndStepEndGrid;

	// Added by 224 (2014/06/28) : Pagination 추가
	BOOL m_bSelectedPage[10] ;


	CImageList	*m_pImagelist;
	void InitStepListCtrl();
	virtual ~CDataView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CDataView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangedTestTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridLButtonDblClk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGridRButtonUp(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTestSelButton();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnSelectAll();
	afx_msg void OnFilePrint();
	afx_msg void OnRclickTestTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnEditClear();
	afx_msg void OnUpdateEditClear(CCmdUI* pCmdUI);
	afx_msg void OnSchButton();
	afx_msg void OnExcelStepEnd();
	afx_msg void OnExcelSelStep();
	afx_msg void OnUpdateExcelSelStep(CCmdUI* pCmdUI);
	afx_msg void OnExcelAllStepData();
	afx_msg void OnUpdateExcelAllStepData(CCmdUI* pCmdUI);
	afx_msg void OnGraphView();
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnApplyButton();
	afx_msg void OnStepStartDetail();
	afx_msg void OnUpdateStepStartDetail(CCmdUI* pCmdUI);
	afx_msg void OnDetailStepStartButton();
	afx_msg void OnUserOption();
	afx_msg void OnExcelDetailButton();
	afx_msg void OnWorkLogView();
	afx_msg void OnManagePattern();
	afx_msg void OnUpdateManagePattern(CCmdUI* pCmdUI);
	afx_msg void OnUpdateWorkLogView(CCmdUI* pCmdUI);
	afx_msg void OnLgoButton();
	afx_msg void OnFolderButton(); //lyj 20210108
	afx_msg void OnDblclkTestTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnApply2Button();
	afx_msg void OnButViewData();
	afx_msg void OnButAllView();
	afx_msg void OnButSetPoint();
	afx_msg void OnSelAllStepData();
	afx_msg void OnButton2();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnPrevPage();
	afx_msg void OnNextPage();
	afx_msg void OnFirstPage();
	afx_msg void OnLastPage();
	afx_msg void OnSelchangeRecordsPerPage();
	afx_msg void OnSchButtonTmp();
	afx_msg void OnUpdateLogButton();
	afx_msg void OnUpdateSchButton();
	afx_msg void OnButton3();
	//}}AFX_MSG
	afx_msg void OnGridSelectChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPageClicked(UINT id);

	DECLARE_MESSAGE_MAP()
public:
	bool bIsActive;
	int nDivisibleCnt;
	afx_msg void OnExcelSelStepDataSampling();
	afx_msg void OnUpdateExcelSelStepDataSampling(CCmdUI *pCmdUI);
	afx_msg void OnExcelAllStepDataSampling();
	afx_msg void OnUpdateExcelAllStepDataSampling(CCmdUI *pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAVIEW_H__ADD07C71_4F4E_4F11_A9EB_918693785425__INCLUDED_)
