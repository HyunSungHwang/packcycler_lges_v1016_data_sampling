// NetStateMonitoringDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "MonitorDoc.h"
#include "NetStateMonitoringDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNetStateMonitoringDlg dialog


CNetStateMonitoringDlg::CNetStateMonitoringDlg(CMonitorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CNetStateMonitoringDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNetStateMonitoringDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
}

CNetStateMonitoringDlg::~CNetStateMonitoringDlg()
{
	if( m_pNetStateImage )
		delete m_pNetStateImage;
}

void CNetStateMonitoringDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetStateMonitoringDlg)
	DDX_Control(pDX, IDC_LIST1, m_wndList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNetStateMonitoringDlg, CDialog)
	//{{AFX_MSG_MAP(CNetStateMonitoringDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetStateMonitoringDlg message handlers

BOOL CNetStateMonitoringDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_pNetStateImage = new CImageList;
	m_pNetStateImage->Create(IDB_NETSTATE,22,2,RGB(255,255,255));
	m_wndList.SetImageList(m_pNetStateImage,LVSIL_SMALL);

	//
	m_wndList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_TRACKSELECT);
	m_wndList.InsertColumn(1, "State", LVCFMT_CENTER, 80);
	
//	int nUnitNum = CUnit::m_byNumOfUnit;
//	CString strTitle;
//	LVITEM lvItem; ::ZeroMemory(&lvItem, sizeof(LVITEM));
//	for( int nI = 0; nI < nUnitNum; nI++ )
//	{
//		lvItem.iItem = nI;
//		lvItem.iSubItem = 0;
//		lvItem.mask = LVIF_IMAGE|LVIF_TEXT;
//		lvItem.iImage = m_pDoc->m_pUnit[nI].GetNetworkState();
//		strTitle.Format("Unit %d", m_pDoc->m_pUnit[nI].GetNo());
//		lvItem.pszText = (LPSTR)(LPCTSTR)strTitle;
//		m_wndList.InsertItem(&lvItem);
//	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNetStateMonitoringDlg::SetUnitState(BYTE byUnitIndex)
{
	LVITEM lvItem; ::ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.iItem = byUnitIndex;
	lvItem.iSubItem = 0;
	lvItem.mask = LVIF_IMAGE;
//	lvItem.iImage = m_pDoc->m_pUnit[byUnitIndex].GetNetworkState();
	m_wndList.SetItem(&lvItem);
}