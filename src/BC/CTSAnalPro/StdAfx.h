// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__13FE5EA6_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_STDAFX_H__13FE5EA6_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#include <afxtempl.h>
//#include <afxdao.h>
//#include <secall.h>
#include <afxsock.h>
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "../../BCLib/Include/PSCommonAll.h"
#include "../../BCLib/Include/PSDataAll.h"

#ifdef _DEBUG
#pragma comment(lib, "../../../lib/PSDataD.lib ")
#pragma message("Automatically linking with PSDataD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../lib/PSData.lib ")
#pragma message("Automatically linking with PSData.lib By K.B.H")
#endif	//_DEBUG

#ifdef _DEBUG
#pragma comment(lib, "../../../lib/PSCommonD.lib ")
#pragma message("Automatically linking with PSCommonD.lib By K.B.H ")
#else
#pragma comment(lib, "../../../lib/PSCommon.lib")
#pragma message("Automatically linking with PSCommon.lib By K.B.H")
#endif	//_DEBUG

#include "./Global/IniParse/IniParser.h"

#define REG_CONFIG_SECTION	"Config"
#define REG_POINT_SECTION	"Point"

#include <grid/gxall.h>

#define _SCB_REPLACE_MINIFRAME
#include "sizecbar.h"
#include "scbarg.h"
#include "scbarcf.h"
#define baseCMyBar CSizingControlBarCF

#pragma warning(disable:4244)

#include "NameVer.h"
#include <afxcontrolbars.h>

extern CString g_strpathIni;
extern int gi_Language;
CString Fun_FindMsg(CString strMsg, CString strFindMsg);
void init_Language();

//#include "../Include/EPMacro.h"
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#ifdef TRACE
#undef TRACE
#define TRACE
#endif

#endif // !defined(AFX_STDAFX_H__13FE5EA6_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)



