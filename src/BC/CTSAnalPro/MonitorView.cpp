// MonitorView.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"

#include "MonitorDoc.h"
#include "MonitorView.h"
#include "MonitorFrame.h"

#include "SetNetworkDlg.h"

#define INITIAL_TIMER					500
#define	MAX_WAITSEC_FOR_CMD_SENDING		10
#define MAX_WAITSEC_FOR_DISPLAY			10
#define WAIT_PROGRESS_FOR_CHANNEL_GRID	100

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMonitorView

IMPLEMENT_DYNCREATE(CMonitorView, CFormView)

CMonitorView::CMonitorView()
	: CFormView(CMonitorView::IDD)
{
	//{{AFX_DATA_INIT(CMonitorView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pChStsLargeImage=NULL;
	m_pChStsSmallImage=NULL;
//	m_pItem           =NULL;
}

CMonitorView::~CMonitorView()
{
	if(m_pChStsLargeImage!=NULL)    delete m_pChStsLargeImage;
	if(m_pChStsSmallImage!=NULL)    delete m_pChStsSmallImage;
}

void CMonitorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMonitorView)
	DDX_Control(pDX, IDC_CH_LIST, m_ChListCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMonitorView, CFormView)
	//{{AFX_MSG_MAP(CMonitorView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_COMMAND(IDM_TEST_START, OnTestStart)
	ON_COMMAND(IDM_COMMAND_PAUSE, OnCommandPause)
	ON_COMMAND(IDM_COMMAND_RETRY, OnCommandRetry)
	ON_COMMAND(IDM_COMMAND_SHOW_PULSE, OnCommandShowPulse)
	ON_COMMAND(IDM_COMMAND_STOP_AFTER_CURRENT_CYCLE, OnCommandStopAfterCurrentCycle)
	ON_COMMAND(IDM_COMMAND_STOP_AFTER_CURRENT_MODE, OnCommandStopAfterCurrentMode)
	ON_COMMAND(IDM_COMMAND_STOP_AFTER_CURRENT_STEP, OnCommandStopAfterCurrentStep)
	ON_COMMAND(IDM_COMMAND_STOP_NOW, OnCommandStopNow)
	ON_COMMAND(IDM_MANAGE_PATTERN, OnManagePattern)
	ON_WM_CONTEXTMENU()
	ON_UPDATE_COMMAND_UI(IDM_TEST_START, OnUpdateTestStart)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_PAUSE, OnUpdateCommandPause)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_RETRY, OnUpdateCommandRetry)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_SHOW_PULSE, OnUpdateCommandShowPulse)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_STOP_AFTER_CURRENT_CYCLE, OnUpdateCommandStopAfterCurrentCycle)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_STOP_AFTER_CURRENT_MODE, OnUpdateCommandStopAfterCurrentMode)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_STOP_AFTER_CURRENT_STEP, OnUpdateCommandStopAfterCurrentStep)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_STOP_NOW, OnUpdateCommandStopNow)
	ON_COMMAND(IDM_MANAGE_NETWORK, OnManageNetwork)
	ON_UPDATE_COMMAND_UI(IDM_MANAGE_NETWORK, OnUpdateManageNetwork)
	ON_COMMAND(IDM_MANAGE_DATA, OnManageData)
	ON_UPDATE_COMMAND_UI(IDM_COMMAND_STOP, OnUpdateCommandStop)
	ON_COMMAND(IDM_MANAGE_ITEMTOBEDISPLAYED, OnManageItemtobedisplayed)
	ON_COMMAND(IDM_MANAGE_PACKTYPE, OnManagePacktype)
	ON_UPDATE_COMMAND_UI(IDM_MANAGE_PACKTYPE, OnUpdateManagePacktype)
	ON_COMMAND(IDM_PATH_MANAGE, OnPathManage)
	ON_COMMAND(IDM_NETSTATE_VIEW, OnNetstateView)
	ON_NOTIFY(NM_DBLCLK, IDC_CH_LIST, OnDblclkChList)
	ON_COMMAND(IDM_MANAGER_NETWORK, OnManageNetwork)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_ERROR_REPORT, OnErrorReportMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMonitorView diagnostics

#ifdef _DEBUG
void CMonitorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CMonitorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CMonitorDoc* CMonitorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMonitorDoc)));
	return (CMonitorDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMonitorView message handlers

void CMonitorView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	if(m_ChListCtrl.GetSafeHwnd())
	{
		CRect	rect;
		m_ChListCtrl.GetWindowRect(rect);
		ScreenToClient(&rect);
		m_ChListCtrl.MoveWindow(rect.left, rect.top,cx-rect.left,cy-rect.top);
	}
	
}

#include "MainFrm.h"
#include "MonitorFrame.h"


void CMonitorView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

	InitListCtrl();

//	ListUpChannel();


	//
	CRect rect;
	GetClientRect(rect);
	OnSize(SIZE_RESTORED,rect.right,rect.bottom);
	//
//	SetTimer(INITIAL_TIMER, 500, NULL);
//	m_wndProg.Create(this, "Command를 전송하고 있습니다.");
//	m_wndProg.Hide();


//	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
//	CMonitorFrame *pMonFrm = (CMonitorFrame *)pMain->GetActiveFrame();
//
//	CString strName;
//	CStringList *pList = GetDocument()->GetSelFileList();
//	POSITION pos = pList->GetHeadPosition();
//	while(pos)
//	{	
//		strName = pList->GetNext(pos);
//	}
//	pMonFrm->SetWindowText(strName);
//


	WriteLog("◀==============================================================▶", false);
	WriteLog(Fun_FindMsg("OnInitialUpdate_msg","IDD_MONITOR_FORM"));
	//@ WriteLog("System 시작");
}

void CMonitorView::OnTimer(UINT nIDEvent) 
{
	static int nPos = 0;
	// 초기화를 위한 Timer동작
	if(nIDEvent==INITIAL_TIMER)
	{
		// 초기화 Timer는 1회 실행하도록 한다.
		KillTimer(nIDEvent);
		// 프로그램을 시작하는 Document의 함수를 실행한다.
		if(!GetDocument()->StartMonitor()) GetParentFrame()->PostMessage(WM_CLOSE);
		// 
	}

	//////////////////////////////////////////////////////////////
	//	Timer ID : 100 + Unit ID								//
	//	Elapsed Time : 1000 msec								//
	//////////////////////////////////////////////////////////////
	else 
	{
		int nUnitIndex = nIDEvent - 100 - 1;

		CMonitorDoc* pDoc = (CMonitorDoc *)GetDocument();
		TRACE("\t**Unit %d [%d]\n", nUnitIndex+1, pDoc->m_nElapsedTime[nUnitIndex]);

		bool bReceiveAck = false, bReceiveNack = false;
		if( pDoc->m_nElapsedTime[nUnitIndex] < MAX_WAITSEC_FOR_CMD_SENDING )
		{
			//////////////////////////////////////////////
			// 1. 아직 ACK를 수신하지 못함..			//
			//////////////////////////////////////////////
			if ( pDoc->m_nReceiveAck[nUnitIndex] == 0x00 )
			{
				CString strText;
				pDoc->m_nElapsedTime[nUnitIndex] ++;		
				strText.Format(Fun_FindMsg("OnTimer_msg1","IDD_MONITOR_FORM"),
					//@ strText.Format("Command 전송 Timeout (%d/%d)",
					MAX_WAITSEC_FOR_CMD_SENDING - pDoc->m_nElapsedTime[nUnitIndex],
					MAX_WAITSEC_FOR_CMD_SENDING);
				//strText.Format("%d 초 이내에 응답이 없으면 Command 전송이 실패합니다.", 
				//	MAX_WAITSEC_FOR_CMD_SENDING - pDoc->m_nElapsedTime[nUnitIndex]);
//				m_wndProg.SetPos(pDoc->m_nElapsedTime[nUnitIndex] * 1000);
//				m_wndProg.PeekAndPump(FALSE);
//				m_wndProg.SetText(strText);
				return;
			}
			//////////////////////////////////////////////
			// 2. ACK를 수신함..						//
			//////////////////////////////////////////////
			else
			{
//				WORD byCompBit = 1;
				CString strMsg;

				/////////////////////////////////////////////////////
				// Command를 전송한 Unit에 속한 모든 Module에 대하여
				for( int nI = 0; nI < 4; nI++ )
				{
					// Receive ACK
					if( pDoc->m_wdModuleID[nUnitIndex][nI] == 0x01 )
					{
						bReceiveAck = true;
					}
					
					// Receive NACK
					else if( pDoc->m_wdModuleID[nUnitIndex][nI] == 0x02 )
					{
						bReceiveNack = true;

						if( pDoc->m_strModuleTestName[nUnitIndex].IsEmpty() == FALSE )
						{
							CString strPath;
							strPath.Format("%s\\M%02d*", 
								pDoc->m_strModuleTestName[nUnitIndex], 
								(nUnitIndex*4)+nI+1);
							TRACE("Dest Name : %s\n", strPath);
							
							SHFILEOPSTRUCT FileOp = {0}; 
							char szTemp[MAX_PATH]; 

							strcpy(szTemp, strPath); 
							szTemp[strPath.GetLength() + 1] = NULL; // NULL문자가 두개 들어가야 한다. 
							FileOp.hwnd = NULL; 
							FileOp.wFunc = FO_DELETE; 
							FileOp.pFrom = NULL; 
							FileOp.pTo = NULL; 
							FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI; // 확인메시지가 안뜨도록 설정 
							FileOp.fAnyOperationsAborted = false; 
							FileOp.hNameMappings = NULL; 
							FileOp.lpszProgressTitle = NULL; 

							FileOp.pFrom = szTemp; 
							SHFileOperation(&FileOp); 
						}
						CString strTemp;						
						strTemp.Format("Module %d,\n", nI+1);
						strMsg += strTemp;
					}
				}

				KillTimer(nIDEvent);
				ResetCommandProgress();

				pDoc->m_nReceiveAck[nUnitIndex] = 0x00;
				pDoc->m_nElapsedTime[nUnitIndex] = 0x00;
				
				// 전체 테스트 폴더를 삭제한다.
				if( bReceiveAck == false )
				{
					if( pDoc->m_strModuleTestName[nUnitIndex].IsEmpty() == FALSE )
					{
						CString strPath = pDoc->m_strModuleTestName[nUnitIndex];
						TRACE("Dest Name : %s\n", strPath);
						
						SHFILEOPSTRUCT FileOp = {0}; 
						char szTemp[MAX_PATH]; 

						strcpy(szTemp, strPath); 
						szTemp[strPath.GetLength() + 1] = NULL; // NULL문자가 두개 들어가야 한다. 
						FileOp.hwnd = NULL; 
						FileOp.wFunc = FO_DELETE; 
						FileOp.pFrom = NULL; 
						FileOp.pTo = NULL; 
						FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI; // 확인메시지가 안뜨도록 설정 
						FileOp.fAnyOperationsAborted = false; 
						FileOp.hNameMappings = NULL; 
						FileOp.lpszProgressTitle = NULL; 

						FileOp.pFrom = szTemp; 
						SHFileOperation(&FileOp); 
					}
				}
				if( bReceiveNack == true )
				{
					CString strText, strErrorMsg;
					strText.Format(Fun_FindMsg("OnTimer_msg2","IDD_MONITOR_FORM"));
					//@ strText.Format("Command 전송 실패");
//					m_wndProg.SetText(strText);
					
					strMsg += _T(Fun_FindMsg("OnTimer_msg3","IDD_MONITOR_FORM"));
					//@ strMsg += _T("Command를 전송할 수 있는 상태가 아닙니다.");
					strErrorMsg.Format(Fun_FindMsg("OnTimer_msg4","IDD_MONITOR_FORM")
						//@ strErrorMsg.Format("Unit %d 에 전송한 Command가 수락되지 않았습니다.\n\n[원인]\n%s"
						+ Fun_FindMsg("OnTimer_msg5","IDD_MONITOR_FORM"), nUnitIndex+1, strMsg);
					//@ "\n\n다시 시도해 주십시오.", nUnitIndex+1, strMsg);

					MessageBox(strErrorMsg, strText, MB_ICONEXCLAMATION);
				}
			}
		}

		//////////////////////////////////////////////
		// Timeout									//
		//////////////////////////////////////////////
		else
		{
			KillTimer(nIDEvent);
			ResetCommandProgress();

			pDoc->m_nReceiveAck[nUnitIndex] = 0x00;
			pDoc->m_nElapsedTime[nUnitIndex] = 0x00;

			CString strText, strErrorMsg;
			strText.Format(Fun_FindMsg("OnTimer_msg6","IDD_MONITOR_FORM"));
			//@ strText.Format("Command 전송 실패");
//			m_wndProg.SetText(strText);

			strErrorMsg.Format(Fun_FindMsg("OnTimer_msg7","IDD_MONITOR_FORM")
				//@ strErrorMsg.Format("Unit %d 에 전송한 Command가 수락되지 않았습니다.\n\n[원인 : %s]"
				+ Fun_FindMsg("OnTimer_msg8","IDD_MONITOR_FORM"), 
				//@ "\n\n다시 시도해 주십시오.",  
				nUnitIndex+1,
				_T(Fun_FindMsg("OnTimer_msg9","IDD_MONITOR_FORM")));
			//@ _T("Module에서 Command를 수락하지 않았습니다."));

			MessageBox(strErrorMsg, strText, MB_ICONEXCLAMATION);

			if( pDoc->m_strModuleTestName[nUnitIndex].IsEmpty() == FALSE )
			{
				CString strPath = pDoc->m_strModuleTestName[nUnitIndex];
				TRACE("Dest Name : %s\n", strPath);
				
				SHFILEOPSTRUCT FileOp = {0}; 
				char szTemp[MAX_PATH]; 

				strcpy(szTemp, strPath); 
				szTemp[strPath.GetLength() + 1] = NULL; // NULL문자가 두개 들어가야 한다. 
				FileOp.hwnd = NULL; 
				FileOp.wFunc = FO_DELETE; 
				FileOp.pFrom = NULL; 
				FileOp.pTo = NULL; 
				FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI; // 확인메시지가 안뜨도록 설정 
				FileOp.fAnyOperationsAborted = false; 
				FileOp.hNameMappings = NULL; 
				FileOp.lpszProgressTitle = NULL; 

				FileOp.pFrom = szTemp; 
				SHFileOperation(&FileOp); 
			}
		}
	}
	CFormView::OnTimer(nIDEvent);
}

void CMonitorView::OnTestStart() 
{
	//
//	CString strPaths;
//	for(BYTE i=0; i<CUnit::m_byNumOfUnit; i++)
//	{
//		if(GetDocument()->m_pUnit[i].IsValidPath())
//			strPaths += GetDocument()->m_pUnit[i].GetPath()+"\n";
//	}
//
//	//
//	CString strContent = CPattern::ShowPatternList(CCTSAnalProApp::m_strOriginFolderPath+"\\pattern",
//		                                           strPaths);
//
//	//
//	if(strContent.IsEmpty()) return;
//
//	CString strTestName = strContent.Left(strContent.Find('\n'));
//	CString strPtnName  = strContent.Mid(strContent.Find('\n')+1);
//
//	//
//	CList<int, int&> IndexList;
//	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList,
//		                       CChannel::USER_COMMAND_START_TEST,
//							   strTestName,
//							   strPtnName);
}

void CMonitorView::OnCommandPause() 
{
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList, CChannel::USER_COMMAND_PAUSE);
}

void CMonitorView::OnCommandRetry() 
{
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList, CChannel::USER_COMMAND_RETRY);
}


void CMonitorView::OnCommandStopNow() 
{
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList, CChannel::USER_COMMAND_STOP_NOW);
}
void CMonitorView::OnCommandStopAfterCurrentMode() 
{
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList, CChannel::USER_COMMAND_STOP_AFTER_CURRENT_MODE);
}
void CMonitorView::OnCommandStopAfterCurrentStep() 
{
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList, CChannel::USER_COMMAND_STOP_AFTER_CURRENT_STEP);
}
void CMonitorView::OnCommandStopAfterCurrentCycle() 
{
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList, CChannel::USER_COMMAND_STOP_AFTER_CURRENT_CYCLE);
}

void CMonitorView::OnCommandShowPulse() 
{
	//
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
//	GetDocument()->SendCommand(&IndexList, CChannel::USER_COMMAND_SHOW_PULSE_2);
}

void CMonitorView::OnManagePattern() 
{
//	CPattern::ShowPatternList(CCTSAnalProApp::m_strOriginFolderPath+"\\pattern",
//		                      NULL);
}

BOOL CMonitorView::SaveFinalView()
{
	//
//	for(int i=0; i<CItemOnDisplay::m_lNumOfItems; i++)
//	{
//		LVCOLUMN col;
//		col.mask = LVCF_WIDTH;
//		if (m_ChListCtrl.GetColumn(i, &col)) m_pItem[i].SetWidth(col.cx);
//	}
//
//	//
//	CItemOnDisplay::DeleteItemsOnDisplay(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb",m_pItem);
//	m_pItem = NULL;

	//
	return TRUE;
}

void CMonitorView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	CMenu aMenu;
	aMenu.LoadMenu(IDR_CONTEXT_MENU);
	aMenu.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,AfxGetMainWnd());
}

void CMonitorView::GetSelItemList(CList<int, int&>* pList)
{
	POSITION pos = m_ChListCtrl.GetFirstSelectedItemPosition();
	while(pos)
	{
		int nItem = m_ChListCtrl.GetNextSelectedItem(pos);
		pList->AddTail(nItem);
	}
}

BOOL CMonitorView::IsThisCommandPossible(BYTE command)
{
	CList<int, int&> IndexList;
	GetSelItemList(&IndexList);
	return GetDocument()->IsThisCommandPossible(&IndexList, command);
}

void CMonitorView::OnUpdateTestStart(CCmdUI* pCmdUI) 
{ 
//	pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_START_TEST)); 
}
void CMonitorView::OnUpdateCommandPause(CCmdUI* pCmdUI) 
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_PAUSE)); 
}
void CMonitorView::OnUpdateCommandRetry(CCmdUI* pCmdUI) 
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_RETRY)); 
}
void CMonitorView::OnUpdateCommandShowPulse(CCmdUI* pCmdUI) 
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_SHOW_PULSE_2)&&(m_ChListCtrl.GetSelectedCount()==1)); 
}
void CMonitorView::OnUpdateCommandStopAfterCurrentCycle(CCmdUI* pCmdUI) 
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_STOP_AFTER_CURRENT_CYCLE)); 
}
void CMonitorView::OnUpdateCommandStopAfterCurrentMode(CCmdUI* pCmdUI)
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_STOP_AFTER_CURRENT_MODE)); 
}
void CMonitorView::OnUpdateCommandStopAfterCurrentStep(CCmdUI* pCmdUI)
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_STOP_AFTER_CURRENT_STEP)); 
}
void CMonitorView::OnUpdateCommandStopNow(CCmdUI* pCmdUI)
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_STOP_NOW)); 
}
void CMonitorView::OnUpdateCommandStop(CCmdUI* pCmdUI) 
{ 
	//pCmdUI->Enable(IsThisCommandPossible(CChannel::USER_COMMAND_STOP_NOW)); 
}

void CMonitorView::OnManageNetwork() 
{
	CSetNetworkDialog aDlg(this);
//	aDlg.m_iUnitIndex = ((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_iCurSelUnit-1;

	if( aDlg.m_iUnitIndex < 0 )
		return;
	if(IDOK==aDlg.DoModal())
	{
		((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.OnSelchangeModuleCombo();
	}
}

void CMonitorView::OnUpdateManageNetwork(CCmdUI* pCmdUI) 
{
 	pCmdUI->Enable(((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_iCurSelUnit>0);	
}

void CMonitorView::OnManageData() 
{
	GetDocument()->BringTestDataIntoDataPC(FALSE);
}

void CMonitorView::OnManageItemtobedisplayed() 
{
//	int N = CItemOnDisplay::m_lNumOfItems;
//	m_pItem = CItemOnDisplay::CreateItemsOnDisplay(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb",m_pItem);
//	for(int i=0; i<N; i++) m_ChListCtrl.DeleteColumn(0);
//	for(int i = 0; i<CItemOnDisplay::m_lNumOfItems; i++)
//	{
//		m_ChListCtrl.InsertColumn(i+1,m_pItem[i].GetTitle(), LVCFMT_CENTER, m_pItem[i].GetWidth(), i);
//	}
}

void CMonitorView::OnManagePacktype() 
{
//	int i = ((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_iCurSelUnit;
//	int j = ((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_iCurSelModule;
//	CString str;
//	BYTE u=0,m=0;
//	((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_UnitCombo.GetLBText(i,str);
//	u = (BYTE)atoi(str);
//	((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_ModuleCombo.GetLBText(j,str);
//	m = (BYTE)atoi(str);
//
//	WORD result   = GetDocument()->m_pModule[CModule::GetModuleIndex(m)].SetPackType();
//	BYTE Serial   = LOBYTE(result);
//	BYTE Parallel = HIBYTE(result);
//	if(Serial>0&&Parallel>0)
//	{
//		CStdioFile afile;
//		CString strPath;
//		strPath.Format("%s\\command\\modules.cmd", GetDocument()->m_pUnit[CUnit::GetUnitIndex(u)].GetPath());
//		if(afile.Open(strPath,CFile::modeCreate|CFile::modeWrite|CFile::typeText))
//		{
//			CString str;
//			str.Format("Command=%d\n",CModule::USER_COMMAND_CHANGE_PACK_TYPE);
//			afile.WriteString(str);
//			str.Format("%d,%d*%d\n", m, Serial, Parallel);
//			afile.WriteString(str);
//			afile.Close();
//		}
//	}
//	//
//	return;
}

void CMonitorView::OnUpdateManagePacktype(CCmdUI* pCmdUI) 
{
//	pCmdUI->Enable(  (((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_iCurSelUnit>0)
//		           &&(((CMonitorFrame*)GetParentFrame())->m_wndDialogBar.m_iCurSelModule>0));
}

//#include "ProgressWnd.h"
void CMonitorView::SetCommandProgress(int nUnitNo)
{
//	m_wndProg.SetText("Sending Command.......");
//	m_wndProg.SetRange(0, MAX_WAITSEC_FOR_CMD_SENDING * 1000);
//	m_wndProg.SetPos(0);
//	m_wndProg.PeekAndPump(FALSE);
//	m_wndProg.Show();
//	m_wndProg.BeginWaitCursor();

	SetTimer(nUnitNo + 100, 1000, NULL);
}

void CMonitorView::ResetCommandProgress()
{
//	m_wndProg.Hide();
//	m_wndProg.EndWaitCursor();
}

#include "ErrorCodeDlg.h"
LRESULT CMonitorView::OnErrorReportMessage(WPARAM wParam, LPARAM lParam)
{
	int nUnitModuleNo = (int)wParam;
	int nErrorCode = (int)lParam;
	CErrorCodeDlg dlg (nUnitModuleNo, nErrorCode);
	WriteLog(dlg.GetErrorMessage());
//	dlg.DoModal();
	return 0;
}

void CMonitorView::WriteLog(CString strLogMsg, bool bAutoDisplayTime)
{
	char szCurrentDirectory[128];
	GetCurrentDirectory(128, szCurrentDirectory);
	CString strCurrentTime ;
	CTime tm = CTime::GetCurrentTime();
	strCurrentTime.Format("%04d-%02d-%02d", tm.GetYear(), tm.GetMonth(), tm.GetDay());
	m_strLogFileName.Format("%s\\Log\\%s.txt", szCurrentDirectory, strCurrentTime);
//	TRACE("%s\n", m_strLogFileName);

	CFileFind aFinder;
	if( aFinder.FindFile(m_strLogFileName) == TRUE )
	{
		if ( m_LogFile.Open(m_strLogFileName, CFile::modeWrite | CFile::shareDenyNone) )
		{
			m_LogFile.SeekToEnd();
			if ( bAutoDisplayTime )
				m_LogFile.WriteString(strCurrentTime + " : " + strLogMsg + "\n");
			else
				m_LogFile.WriteString(strLogMsg + "\n");
			m_LogFile.Close();
		}
	}
	else
	{
		if ( m_LogFile.Open(m_strLogFileName, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyNone) )
		{
			if ( bAutoDisplayTime )
				m_LogFile.WriteString(strCurrentTime + " : " + strLogMsg + "\n");
			else
				m_LogFile.WriteString(strLogMsg + "\n");
			m_LogFile.Close();
		}
	}
}

void CMonitorView::OnPathManage() 
{
	char buffer[_MAX_PATH] = {'\0'};
	BROWSEINFO browseInfo;
	browseInfo.hwndOwner = m_hWnd;
	browseInfo.pidlRoot  = NULL;
	browseInfo.lpszTitle = Fun_FindMsg("OnPathManage_msg1","IDD_MONITOR_FORM");
	//@ browseInfo.lpszTitle = "시험 결과 데이터를 저장할 Path를 설정해 주십시오";//$
	browseInfo.pszDisplayName = buffer;
	browseInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	browseInfo.lpfn=NULL;
	browseInfo.lParam=0;
	LPITEMIDLIST pDirList = SHBrowseForFolder(&browseInfo);
	if(pDirList!=NULL)
	{
		SHGetPathFromIDList(pDirList, buffer);		
		CString strPath(buffer);
		LPMALLOC lpMalloc;
		VERIFY(::SHGetMalloc(&lpMalloc) == NOERROR);
		lpMalloc->Free(pDirList);
		lpMalloc->Release();
		//
		strPath.MakeLower();
		
		CString strMsg ;
		strMsg.Format(Fun_FindMsg("OnPathManage_msg2","IDD_MONITOR_FORM")
			//@ strMsg.Format("[ %s ] 을 새로운 데이터 저장 경로로 지정 하시겠습니까?\n\n"
			+ Fun_FindMsg("OnPathManage_msg3","IDD_MONITOR_FORM")
			//@ "※ 주의 : 현재 진행되고 있는 시험은 시험이 종료된 후에 윈도우 탐색기에서\n\n"
			+ Fun_FindMsg("OnPathManage_msg4","IDD_MONITOR_FORM"), buffer);
		    //@ "   새로 지정된 경로로 데이터를 이동시켜야 올바른 데이터 조회를 할 수 있습니다.", buffer);
			
		if( MessageBox(strMsg, "새로운 저장 경로 설정", MB_ICONQUESTION | MB_YESNO ) == IDYES )
		{
			CCTSAnalProApp* pApp = (CCTSAnalProApp*)AfxGetApp();
			pApp->SetResultDataPath(strPath);
		}
		else
		{
			return ;
		}
	}
}

void CMonitorView::OnNetstateView() 
{
	CMonitorDoc* pDoc = (CMonitorDoc*)GetDocument();
	if(pDoc->m_pNetStateMonitoringDlg)
		pDoc->m_pNetStateMonitoringDlg->ShowWindow(SW_SHOW);
}

void CMonitorView::InitListCtrl()
{
	// Image 등록
	m_pChStsLargeImage    = new CImageList;
	m_pChStsSmallImage    = new CImageList;
	m_pChStsLargeImage->Create(IDB_CH_STS_BIG_SIGN,32,5,RGB(255,255,255));
	m_pChStsSmallImage->Create(IDB_CH_STS_SMALL_SIGN,16,6,RGB(255,255,255));
	m_ChListCtrl.SetImageList(m_pChStsLargeImage,LVSIL_NORMAL);
	m_ChListCtrl.SetImageList(m_pChStsSmallImage,LVSIL_SMALL);

	//
	m_ChListCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_TRACKSELECT);
	m_ChListCtrl.SetHoverTime(10000);

	// Column 삽입

//	m_pItem = CItemOnDisplay::CreateItemsOnDisplay(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb");
//	if(m_pItem==NULL) return;
//
//	for(int i=0; i<CItemOnDisplay::m_lNumOfItems; i++)
//	{
//		m_ChListCtrl.InsertColumn(i+1,m_pItem[i].GetTitle(), LVCFMT_CENTER, m_pItem[i].GetWidth(), i);
//	}
}

//BOOL CMonitorView::ListUpChannel()
//{
//	CMonitorDoc *pDoc = (CMonitorDoc *)GetDocument();
//	m_ChListCtrl.DeleteAllItems();
//	ASSERT(m_ChListCtrl.GetItemCount() == 0);
//
//	LVITEM lvItem; 
//	::ZeroMemory(&lvItem, sizeof(LVITEM));
//	lvItem.mask = LVIF_IMAGE|LVIF_TEXT;
//
//	CTime createTime;
//	SYSTEMTIME timeDest;
//	COleDateTime oleTime;
//	
//
//	CString strName;
//	CFileFind *pFinder;
//	pFinder = new CFileFind();
//	int nCount = 0;
//	BOOL bWorking = FALSE;
//
//	CStringList *pList = pDoc->GetSelFileList();
//	POSITION pos = pList->GetHeadPosition();
//	while(pos)
//	{	
//		strName.Format("%s\\%s\\*.*", pDoc->GetDataPath(), pList->GetNext(pos));
//		
//		bWorking = pFinder->FindFile(strName);
//		while (bWorking)
//		{
//			bWorking = pFinder->FindNextFile();
//
//			if(pFinder->IsDots())	continue;
//
//			if (pFinder->IsDirectory())
//			{
//
//				//No
//				lvItem.iItem = nCount++;
//				lvItem.iSubItem = 0;
//		//		lvItem.iImage = m_pDoc->m_pUnit[nI].GetNetworkState();
//				strName.Format("%d", nCount);
//				lvItem.pszText = (LPSTR)(LPCTSTR)strName;
//				m_ChListCtrl.InsertItem(&lvItem);
//				m_ChListCtrl.SetItemData(lvItem.iItem, lvItem.iItem);
//
//				
//				//Name
//				m_ChListCtrl.SetItemText(lvItem.iItem, 1, pFinder->GetFileName());
//
//				pFinder->GetCreationTime(createTime);
//
//				createTime.GetAsSystemTime(timeDest);
//	//			m_ctrlTestList.SetItemText(lvItem.iItem, 2, createTime.Format("%Y/%M/%d %H:%M:%S"));
//				m_ChListCtrl.SetItemText(lvItem.iItem, 2, COleDateTime(timeDest).Format());
//
//		  }
//		}
//	}
//
//	delete pFinder;
//
//	return TRUE;
//}


void CMonitorView::OnDblclkChList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
//	((CCTSAnalProApp *)AfxGetApp())->OpenDataWnd();	
	
	*pResult = 0;
}

