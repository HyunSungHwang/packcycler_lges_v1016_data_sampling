// SaveExcelOptionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ctsanalpro.h"
#include "SaveExcelOptionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSaveExcelOptionDlg dialog


CSaveExcelOptionDlg::CSaveExcelOptionDlg(CWnd* pParent /*=NULL*/)
: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSaveExcelOptionDlg::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSaveExcelOptionDlg::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSaveExcelOptionDlg::IDD3):
	(CSaveExcelOptionDlg::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSaveExcelOptionDlg)
	m_nExcelOp = 0;
	m_nFileSize = 0;
	m_nRowCount = 0;
	//}}AFX_DATA_INIT
}


void CSaveExcelOptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSaveExcelOptionDlg)
	DDX_Radio(pDX, IDC_RADIO1, m_nExcelOp);
	DDX_Text(pDX, IDC_EDIT1, m_nFileSize);
	DDV_MinMaxInt(pDX, m_nFileSize, 0, 999999);
	DDX_Text(pDX, IDC_EDIT_ROW_COUNT, m_nRowCount);
	DDV_MinMaxInt(pDX, m_nRowCount, 0, 65536);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSaveExcelOptionDlg, CDialog)
	//{{AFX_MSG_MAP(CSaveExcelOptionDlg)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaveExcelOptionDlg message handlers

void CSaveExcelOptionDlg::OnRadio1() 
{
	GetDlgItem(IDC_EDIT1)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDIT2)->EnableWindow(FALSE);
	
}

void CSaveExcelOptionDlg::OnRadio2() 
{
	GetDlgItem(IDC_EDIT1)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT2)->EnableWindow(TRUE);
	
}

void CSaveExcelOptionDlg::OnOK() 
{
	UpdateData(FALSE);

	if(m_nExcelOp == 0 && m_nRowCount == 0)
		m_nRowCount = 65536;
	if(m_nExcelOp == 1 && m_nFileSize == 0)
		m_nFileSize = 10;
	
	CDialog::OnOK();
}