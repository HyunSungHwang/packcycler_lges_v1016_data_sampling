// MonitorDoc.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "MonitorFrame.h"
//#include "CommandSocket.h"
//#include "ServerSocket.h"
#include "NetStateMonitoringDlg.h"
#include "MonitorDoc.h"
#include "MonitorView.h"
//#include "../PSModule/PulseDataDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMonitorDoc

IMPLEMENT_DYNCREATE(CMonitorDoc, CDocument)

CMonitorDoc::CMonitorDoc()
{
//	m_pModule     = NULL;
//	m_pUnit       = NULL;

	m_bThread     = FALSE;
	m_pThread     = NULL;

	memset(m_nReceiveAck, 0, sizeof(m_nReceiveAck));
	memset(m_nElapsedTime, 0, sizeof(m_nElapsedTime));
	memset(m_wdModuleID, 0, sizeof(m_wdModuleID));

	m_pNetStateMonitoringDlg = NULL;
}

BOOL CMonitorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

//	m_strDataPath = AfxGetApp()->GetProfileString("Control", "ResultDataPath", NULL);


	//결과 파일을 선택한다.
	//한종류의 결과 파일만 선택 가능하다.
//	if(SelResultFile() == FALSE)	return FALSE;


//	m_pServerSock = new CServerSocket(this);
//	if( m_pServerSock->CreateSocket() == FALSE )
//	{
//		MessageBox(NULL, "Program을 중복실행시켰습니다.", "BTS", MB_ICONINFORMATION);
//		return FALSE;
//	}
	
	return TRUE;
}

CMonitorDoc::~CMonitorDoc()
{
//	if( m_pUnit )
//	{
//		CUnit::DeleteUnitObjects(m_pUnit);
//	}
//

	DeleteSocket();
}


BEGIN_MESSAGE_MAP(CMonitorDoc, CDocument)
	//{{AFX_MSG_MAP(CMonitorDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMonitorDoc diagnostics

#ifdef _DEBUG
void CMonitorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMonitorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMonitorDoc serialization

void CMonitorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
	}
	else
	{
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMonitorDoc commands
UINT CMonitorDoc::MonitorThreadProc(LPVOID pParam)
{
//	CMonitorDoc* pDoc = (CMonitorDoc*)pParam;
//
//	//
//	DWORD BackUpFileCheckTime = 0L;
//
//	while(pDoc->ThreadContinue())
//	{
//		// Keep this thread's priority as normal.
//		if(pDoc->GetThreadPoint()->GetThreadPriority()!=THREAD_PRIORITY_NORMAL)
//		{
//			pDoc->GetThreadPoint()->SetThreadPriority(THREAD_PRIORITY_NORMAL);
//		}
//		// 일정시간간격으로 Backup file을 Check 한다.
//		DWORD ElapsedCount=0UL, CurTickCount = GetTickCount();
//		if(CurTickCount>=BackUpFileCheckTime) ElapsedCount =  CurTickCount - BackUpFileCheckTime;
//		else ElapsedCount = 0xFFFFFFFF - (BackUpFileCheckTime - CurTickCount);
//		//
//		if(ElapsedCount>=BACKUP_FILE_CHECK_PERIOD)
//		{
//			::SendMessage(CModule::m_hMainWnd, UM_CHECK_BACKUP_FILE, 0, 0);
//			BackUpFileCheckTime = GetTickCount();
//		}
//		//
//		Sleep(1000);
//	}
	return 0UL;
}

BOOL CMonitorDoc::StartMonitor()
{
	// 초기화과정동안 커서를 모래시계로...
//	CWaitCursor acursor;
//
//	//
//	// Step 1. Create Module Objects
//	//
//
//	TRY{
//
//		// Set as Monitor Program Mode
//		CModule::SetMode(CModule::MODE_TESTER);
//
//		// SysInfo.mdb file을 통하여 Module Object들을 생성한다.
//		m_pUnit   = CUnit::CreateUnitObjects(
//			            CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb"
//					);
//
//		m_pModule = CModule::CreateModuleObjects(
//						CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb"
//					);
//
//		// Monitor Frame의 핸들을 CModule의 static member variable에 넣어둔다.
//		POSITION pos = GetFirstViewPosition();
//		CView* pView = GetNextView(pos);
//		CModule::SetMainWndHandle(pView->GetParentFrame()->m_hWnd);
//
//		if( m_pNetStateMonitoringDlg == NULL )
//			m_pNetStateMonitoringDlg = new CNetStateMonitoringDlg(this);
//		m_pNetStateMonitoringDlg->Create(IDD_NETSTATE_DLG, NULL);
//		
//	}
//	CATCH(CDaoException,e){
//		e->ReportError();
//		return FALSE;
//	}
//	AND_CATCH(CMemoryException,e){
//		e->ReportError();
//		return FALSE;
//	}
//	AND_CATCH(CException,e){
//		e->ReportError();
//		return FALSE;
//	}
//	END_CATCH
//
//	//
//	if(((CCTSAnalProApp*)AfxGetApp())->IsInitialState())
//	{
//		//
//		for(BYTE i=0; i<CUnit::m_byNumOfUnit; i++)
//		{
//			if(m_pUnit[i].IsValidPath())
//			{
//				// Unit와의 Network 연결에 이상이 있을 때에는,
//				if(!m_pUnit[i].CheckWhetherPathIsValid())
//				{
//					m_pUnit[i].SetWhetherPathIsValid(FALSE);
//					m_pUnit[i].SaveWhetherPathIsValid(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb");
//				}
//			}
//		}
//		//
//		BringTestDataIntoDataPC(TRUE);
//		((CCTSAnalProApp*)AfxGetApp())->ConfirmInitialOperation();
//	}
//
//	//
//	// Frame의 DialogBar 초기Setting -> View초기화
//	//
//
//	POSITION pos = GetFirstViewPosition();
//	CMonitorView* pView = (CMonitorView*) GetNextView(pos);
////	((CMonitorFrame*)pView->GetParentFrame())->m_wndDialogBar.InitialSet(this);
//
//	//
//	// Thread구동
//	//
//
//	if(!StartMonitorThread()) return FALSE;

	//
	return TRUE;
}

BOOL CMonitorDoc::EndMonitor()
{
	//
//	if(!EndMonitorThread()) return FALSE;

	//
//	POSITION pos = GetFirstViewPosition();
//	CMonitorView* pView = (CMonitorView*)GetNextView(pos);
//	pView->SaveFinalView();
//
//	// Module Object들을 Delete한다.
//	CModule::DeleteModuleObjects(m_pModule);
//
//	if( m_pNetStateMonitoringDlg )
//	{
//		delete m_pNetStateMonitoringDlg;
//		m_pNetStateMonitoringDlg = NULL;
//	}
//	//
	return TRUE;
}

BOOL CMonitorDoc::EndMonitorThread()
{
	DWORD startTime = GetTickCount();
	// Thread가 nonsignaled에서 signaled로 바뀔 때까지 다음을 계속 시도하여
	// Thread를 종료시킨다.
	m_bThread=FALSE;
	while(::WaitForSingleObject(m_pThread->m_hThread,0)!=WAIT_OBJECT_0)
	{
		// 함수를 시작한 시간에서 5초가 지나도 
		// Thread가 종료하지 않으면 return FALSE;
		DWORD curTime=GetTickCount();
		DWORD elapsedTime = 0UL;
		if(startTime<=curTime) elapsedTime = curTime - startTime;
		else elapsedTime = 0xFFFFFFFF - (startTime - curTime);
		if(elapsedTime>5000)
		{
			m_bThread=TRUE;
			return FALSE;
		}
		// Thread함수가 무한루프를 나오게 한다.
		m_bThread=FALSE;
		Sleep(0);
	}
	// Thread가 종료되었으면 Object를 Delete한다.
	delete m_pThread;
	m_pThread=NULL;
	//
	return TRUE;
}

BOOL CMonitorDoc::StartMonitorThread()
{
	m_pThread = AfxBeginThread(MonitorThreadProc,
		                       this,
							   THREAD_PRIORITY_NORMAL,
							   0,
							   CREATE_SUSPENDED,
							   NULL);
	// Thread 생성에 실패한 경우 return FALSE.
	if(m_pThread==NULL) return FALSE;
	// Thread 종료 후, Object가 자동으로 Delete되지 않게 함.
	m_pThread->m_bAutoDelete = FALSE;
	// Thread 동작을 시작시킨다.
	m_bThread = TRUE;
	if(m_pThread->ResumeThread()==0xFFFFFFFF)
	{   // Thread가 Suspended mode에서 다시 동작하지 않을 때,
		delete m_pThread; // Thread object를 없애고 return FALSE.
		m_pThread = NULL;
		m_bThread=FALSE;
		return FALSE;
	}
	//
	return TRUE;
}

void CMonitorDoc::MonitorModules()
{
//	POSITION pos = GetFirstViewPosition();
//	CMonitorView* pView = (CMonitorView*) GetNextView(pos);
//	//
//	// 각 Unit의 Network Path를 점검한다.
//	//
//
//	for(BYTE i=0; i<CUnit::m_byNumOfUnit; i++)
//	{
//		if(m_pUnit[i].IsValidPath())
//		{
//			// Unit와의 Network 연결에 이상이 있을 때에는,
//			if(!m_pUnit[i].CheckWhetherPathIsValid())
//			{
//				// Network가 연결되거나 끊어졌을 때
//				m_pUnit[i].SetWhetherPathIsValid(FALSE);
//				m_pUnit[i].SaveWhetherPathIsValid(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb");
////				((CMonitorFrame*)(pView->GetParentFrame()))->m_wndDialogBar.OnSelchangeModuleCombo();
//			}
//		}
//	}

	//
	// 각 Module의 Backup file을 Loading한다.
	//

	// ListCtrl에 Display되고 있는 Module의 Index List를 구한다.
//	CList<int, int&> ModuleIndexList;
//	GetDisplayedModuleIndexList(&ModuleIndexList);
//	// 각 Module에 대하여
//	pos = ModuleIndexList.GetHeadPosition();
//	BYTE byStateCheck = CSelModuleDialogBar::STATE_CHECK_NONE;
//	while(pos)
//	{
//		int  module_index = ModuleIndexList.GetNext(pos);
//		// Module이 속한 Unit와의 통신이 Normal인 경우
//		BYTE unit_index = CUnit::GetUnitIndex(m_pModule[module_index].GetUnitNo());
//
////		if(m_pUnit[unit_index].IsValidPath() && 
////		   m_pUnit[unit_index].GetNetworkState()==CUnit::NETWORK_STATE_LINE_ON)
//		if(m_pUnit[unit_index].IsValidPath())
//		{
//			CString strPath;
//			strPath.Format("%s\\status\\module%02d.bak",
//				m_pUnit[unit_index].GetPath(),
//				m_pModule[module_index].GetModuleNo());
//
//			// 그 Backup file의 내용을 Loading한다.
//			if(m_pModule[module_index].LoadBackFile(strPath)!=0x01)
//			{
//				 UpdateChOnView(TRUE,  module_index, byStateCheck);
//			}
//			else UpdateChOnView(FALSE, module_index, byStateCheck);
//		}
//		else     UpdateChOnView(FALSE, module_index, byStateCheck);
//	}
	//
//	((CMonitorFrame*)pView->GetParentFrame())->m_wndDialogBar.SetCheckState(byStateCheck);
}	

void CMonitorDoc::UpdateChOnView(BOOL bNormal, int module_index, BYTE& byStateCheck)
{
//	POSITION pos = GetFirstViewPosition();
//	CMonitorView* pView = (CMonitorView*)GetNextView(pos);
//	//
//	pos = m_listIndexFromListToCh.GetHeadPosition();
//	int iItem = 0;
//	while(pos)
//	{
//		//
//		POINT pt = m_listIndexFromListToCh.GetNext(pos);
//		int  index = pt.x;
//		BYTE ch    = (BYTE) pt.y;
//
//		if(index!=module_index){ iItem++; continue; }
//
//		//
//		UINT uOpSts = m_pModule[index].GetOperationStatusOfCh(ch);
//		BYTE byDisplayFlag = 0x00;
//
//		//
//		switch(uOpSts)
//		{
//		case CChannel::CH_OPERATION_STATUS_INITIAL:
//		case CChannel::CH_OPERATION_STATUS_CALIBRATION:
//			byDisplayFlag = 0x01;
//			break;
//		case CChannel::CH_OPERATION_STATUS_STAND_BY:
//		case CChannel::CH_OPERATION_STATUS_CHARGE:
//		case CChannel::CH_OPERATION_STATUS_CHARGE_BREAK:
//		case CChannel::CH_OPERATION_STATUS_DCIR_IN_CHARGE_BREAK:
//		case CChannel::CH_OPERATION_STATUS_DISCHARGE:
//		case CChannel::CH_OPERATION_STATUS_DISCHARGE_BREAK:
//		case CChannel::CH_OPERATION_STATUS_DCIR_IN_DISCHARGE_BREAK:
//			byDisplayFlag = 0x02;
//			break;
//		case CChannel::CH_OPERATION_STATUS_ERROR:
//		case CChannel::CH_OPERATION_STATUS_PAUSE:
//			byDisplayFlag = 0x04;
//			break;
//		case CChannel::CH_OPERATION_STATUS_USER_STOP:
//		case CChannel::CH_OPERATION_STATUS_NORMAL_END:
//			byDisplayFlag = 0x08;
//			break;
//		}
//		/*
//		|--------------------------------------------
//		|                | 0x01 | 0x02 | 0x04 | 0x08 |
//		|---------------------------------------------
//		| 채널번호       |  o   |  o   |  o   |  o   |
//		| 채널상태       |  o   |  o   |  o   |  o   |
//		| Test 이름      |  x   |  o   |  o   |  o   |
//		| Test 패턴      |  x   |  o   |  o   |  x   |
//		| 현재 스텝      |  x   |  o   |  o   |  x   |
//		| 스텝 내 Cycle  |  x   |  o   |  o   |  x   |
//		| 총시간         |  x   |  o   |  o   |  x   |
//		| 총 Cycle       |  x   |  o   |  o   |  o   |
//		| 시간           |  x   |  o   |  o   |  o   |
//		| 전압           |  o   |  o   |  o   |  o   |
//		| 전류           |  x   |  o   |  x   |  x   |
//		| 전력           |  x   |  o   |  x   |  x   |
//		| 용량           |  x   |  o   |  o   |  o   |
//		| 전력량         |  x   |  o   |  x   |  x   |
//		| 온도           |  o   |  o   |  o   |  o   |
//		| 압력           |  x   |  o   |  x   |  x   |
//		---------------------------------------------|
//		*/
//
//		//
//		for(int iSubItem=0; iSubItem<CItemOnDisplay::m_lNumOfItems; iSubItem++)
//		{
//			LV_ITEM lvitem;
//			lvitem.iItem    = iItem;
//			lvitem.iSubItem = iSubItem;
//			CString str;
//			float fltVal = 0.0f;
//
//			switch(pView->m_pItem[iSubItem].GetIndex())
//			{
//			// 채널번호 //////////////////////////
//			case CItemOnDisplay::INDEX_CHANNEL_NO:
//			//////////////////////////////////////
//
//				lvitem.mask     = LVIF_TEXT|LVIF_IMAGE;
//
//				if(bNormal)
//				{
//					switch(uOpSts)
//					{
//					case CChannel::CH_OPERATION_STATUS_INITIAL:
//						lvitem.iImage = 0;
//						byStateCheck|=CSelModuleDialogBar::STATE_CHECK_INITIAL;
//						break;
//					case CChannel::CH_OPERATION_STATUS_STAND_BY:
//					case CChannel::CH_OPERATION_STATUS_CHARGE:
//					case CChannel::CH_OPERATION_STATUS_CHARGE_BREAK:
//					case CChannel::CH_OPERATION_STATUS_DCIR_IN_CHARGE_BREAK:
//					case CChannel::CH_OPERATION_STATUS_DISCHARGE:
//					case CChannel::CH_OPERATION_STATUS_DISCHARGE_BREAK:
//					case CChannel::CH_OPERATION_STATUS_DCIR_IN_DISCHARGE_BREAK:
//						lvitem.iImage = 1;
//						byStateCheck|=CSelModuleDialogBar::STATE_CHECK_RUN;
//						break;
//					case CChannel::CH_OPERATION_STATUS_ERROR:
//						lvitem.iImage = 2;
//						byStateCheck|=CSelModuleDialogBar::STATE_CHECK_ERROR;
//						break;
//					case CChannel::CH_OPERATION_STATUS_PAUSE:
//						lvitem.iImage = 3;
//						byStateCheck|=CSelModuleDialogBar::STATE_CHECK_PAUSE;
//						break;
//					case CChannel::CH_OPERATION_STATUS_USER_STOP:
//					case CChannel::CH_OPERATION_STATUS_NORMAL_END:
//						lvitem.iImage = 4;
//						byStateCheck|=CSelModuleDialogBar::STATE_CHECK_FINISH;
//						break;
//					case CChannel::CH_OPERATION_STATUS_CALIBRATION:
//						lvitem.iImage = 5;
//						break;
//					}
//				}
//				else lvitem.iImage = 6;
//
//				//
//				str.Format("U%02dM%02dCH%02d",
//					       m_pModule[index].GetUnitNo(),
//						   m_pModule[index].GetModuleNo(),
//						   ch+1);
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//				pView->m_ChListCtrl.SetItem(&lvitem);
//
//				break;
//
//			// 상태 //////////////////////////////////
//			case CItemOnDisplay::INDEX_CHANNEL_STATUS:
//			//////////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				switch(uOpSts)
//				{
//				case CChannel::CH_OPERATION_STATUS_INITIAL:
//					str = "초기";
//					break;
//				case CChannel::CH_OPERATION_STATUS_STAND_BY:
//					str = "대기";
//					break;
//				case CChannel::CH_OPERATION_STATUS_CHARGE:
//					str = "충전";
//					break;
//				case CChannel::CH_OPERATION_STATUS_CHARGE_BREAK:
//					str = "충휴";
//					break;
//				case CChannel::CH_OPERATION_STATUS_DCIR_IN_CHARGE_BREAK:
//					str = "충IR";
//					break;
//				case CChannel::CH_OPERATION_STATUS_DISCHARGE:
//					str = "방전";
//					break;
//				case CChannel::CH_OPERATION_STATUS_DISCHARGE_BREAK:
//					str = "방휴";
//					break;
//				case CChannel::CH_OPERATION_STATUS_DCIR_IN_DISCHARGE_BREAK:
//					str = "방IR";
//					break;
//				case CChannel::CH_OPERATION_STATUS_ERROR:
//					str = "이상";
//					break;
//				case CChannel::CH_OPERATION_STATUS_PAUSE:
//					str = "Pause";
//					break;
//				case CChannel::CH_OPERATION_STATUS_USER_STOP:
//					str = "종료";
//					break;
//				case CChannel::CH_OPERATION_STATUS_NORMAL_END:
//					str = "종료";
//					break;
//				}
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// Test이름 /////////////////////////
//			case CItemOnDisplay::INDEX_TEST_NAME:
//			/////////////////////////////////////
//				lvitem.mask = LVIF_TEXT;
////				if(byDisplayFlag&0x01) str = "";
////				else                   
//				str = m_pModule[index].GetTestNameOfCh(ch);
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//				
//				break;
//
//
//			// 패턴이름 ////////////////////////////
//			case CItemOnDisplay::INDEX_TEST_PATTERN:
//			////////////////////////////////////////
//				lvitem.mask = LVIF_TEXT;
//				if(byDisplayFlag&(0x02|0x04)) 
//				{
//					str = m_pModule[index].GetPatternNameOfCh(ch);
//				}
//				else                          
//				{
//					str = "";
//				}
////				str = m_pModule[index].GetPatternNameOfCh(ch);
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 현재스텝 /////////////////////////
//			case CItemOnDisplay::INDEX_TEST_STEP:
//			/////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				if(byDisplayFlag&(0x02|0x04)) str.Format("%d", m_pModule[index].GetCurStepOfCh(ch));
//				else                          str = "";
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 스텝 내 Cycle /////////////////////////////
//			case CItemOnDisplay::INDEX_TEST_CYCLE_IN_STEP:
//			//////////////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				if(byDisplayFlag&(0x02|0x04)) str.Format("%d", m_pModule[index].GetCycleInStepOfCh(ch)+1);
//				else                          str = "";
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 총Cycle /////////////////////////////////
//			case CItemOnDisplay::INDEX_TEST_TOTAL_CYCLE:
//			////////////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				if(byDisplayFlag&0x01) str = "";
//				else                   str.Format("%d",m_pModule[index].GetNumOfTotalCycleOfCh(ch)+1);
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 총경과시간 /////////////////////////////
//			case CItemOnDisplay::INDEX_TEST_TOTAL_TIME:
//			///////////////////////////////////////////
//			{
//
//				if(byDisplayFlag == 0x00)				//
//				{										//
//					lvitem.mask = LVIF_TEXT;			//
//					lvitem.pszText = "";				//
//					break;								//
//				}										//
////////////////////////////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				fltVal = m_pModule[index].GetTotalElapsedSecOfTestInCh(ch);
//
//				int nDay, nHour, nMin, nSec;
//				nDay = (int)fltVal / (3600 * 24);	fltVal -= (nDay * (3600 * 24));
//				nHour = (int)fltVal / 3600;			fltVal -= (nHour * 3600);
//				nMin = (int)fltVal / 60;			fltVal -= (nMin * 60);
//				nSec = (int)fltVal;
//				CString strJSH;
//				strJSH.Format("%02d/%02d:%02d:%02d", nDay, nHour, nMin, nSec);
/////////////////////////////////////////////////////////////////////////////////////////////////////
//				if(byDisplayFlag&(0x01|0x08)) str = "";
//				else {
//					str = strJSH;
//				}
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//			}
//			// 모드경과시간 ////////////////
//			case CItemOnDisplay::INDEX_TIME:
//			////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				fltVal = m_pModule[index].GetTransducerCurValueOfCh(ch, CCondition::TRANSDUCER_INDEX_TIME);
//				if(byDisplayFlag&0x01) str="";
//				else
//				{
//					str.Format("%3d:%02d:%02d", ((int)fltVal)/3600L,
//						                      (((int)fltVal)%3600L)/60L,
//											  (((int)fltVal)%3600L)%60L);
//				}
//
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 전압 ///////////////////////////
//			case CItemOnDisplay::INDEX_VOLTAGE:
//			///////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				//str.Format("%.4f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_VOLTAGE));
//				str.Format("%1.4f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_VOLTAGE));
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 전류 ///////////////////////////
//			case CItemOnDisplay::INDEX_CURRENT:
//			///////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				//if(byDisplayFlag&0x02) str.Format("%.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_CURRENT));
//				if(byDisplayFlag&0x02) str.Format("%4.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_CURRENT));
//				else                   str="";
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// Power ////////////////////////
//			case CItemOnDisplay::INDEX_POWER:
//			/////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				//if(byDisplayFlag&0x02) str.Format("%.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_POWER));
//				if(byDisplayFlag&0x02) str.Format("%4.f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_POWER));
//				else                   str="";
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 용량 ////////////////////////////
//			case CItemOnDisplay::INDEX_CAPACITY:
//			////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				if(byDisplayFlag&0x01) str="";
//				//else                   str.Format("%.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_CAPACITY)/3600.0f);
//				else                   str.Format("%.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_CAPACITY));
//
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 전력량 /////////////////////////
//			case CItemOnDisplay::INDEX_WATTAGE:
//			///////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				//if(byDisplayFlag&0x02) str.Format("%.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_WATTAGE));
//				if(byDisplayFlag&0x02) str.Format("%4.f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_WATTAGE));
//				else                   str="";
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 온도 ///////////////////////////////
//			case CItemOnDisplay::INDEX_TEMPERATURE:
//			///////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				str.Format("%.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_TEMPERATURE));
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 압력 ////////////////////////////
//			case CItemOnDisplay::INDEX_PRESSURE:
//			////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				if(byDisplayFlag&0x02) str.Format("%.1f", m_pModule[index].GetTransducerCurValueOfCh(ch,CCondition::TRANSDUCER_INDEX_PRESSURE));
//				else                   str="";
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//
//			// 결과코드 ///////////////////////////
//			case CItemOnDisplay::INDEX_RESULTCODE:
//			///////////////////////////////////////
//
//				lvitem.mask = LVIF_TEXT;
//				str.Format("%s", m_pModule[index].GetResultCodeOfCh(ch));
//				if( str.IsEmpty() )
//				{
//					str = pView->m_ChListCtrl.GetItemText(iItem, iSubItem);
//				}
//				lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//
//				break;
//			}
//			//
//			if(bNormal)
//			{
//				if(pView->m_ChListCtrl.GetItemText(iItem,iSubItem).Compare(str)!=0) pView->m_ChListCtrl.SetItem(&lvitem);
//			}
//			else
//			{
//				if(pView->m_pItem[iSubItem].GetIndex()!=CItemOnDisplay::INDEX_CHANNEL_NO)
//				{
//					lvitem.mask    = LVIF_TEXT;
//					lvitem.pszText = "";
//					pView->m_ChListCtrl.SetItem(&lvitem);
//				}
//			}
//		}
//
//		iItem++;
//	}
}

void CMonitorDoc::UpdateListView(BYTE UnitNo, BYTE ModuleNo, BYTE SelStatus)
{
	// View의 Point 구하기
//	POSITION pos = GetFirstViewPosition();
//	CMonitorView* pView = (CMonitorView*) GetNextView(pos);
//
//	// 일단 ListCtrl을 지운다.
//	pView->m_ChListCtrl.DeleteAllItems();
//	m_listIndexFromListToCh.RemoveAll();
//
//	// 모든 Module을 돌면서 다음의 조건을 만족하면 ListCtrl에 넣어준다.
//	int iItem = 0;
//	for(BYTE i=0;i<CModule::m_byNumOfModules;i++)
//	{
//		// 선택된 Unit가 All이거나 Module이 속한 Unit가 선택된 경우,
//		if(m_pModule[i].GetUnitNo()==UnitNo||UnitNo==0)
//		{
//			// 선택된 Module이 All이거나 선택된 Module일때
//			if(m_pModule[i].GetModuleNo()==ModuleNo||ModuleNo==0)
//			{
//				// Module이 속한 Unit의 Network가 정상적이고 Module이 사용자잠금 상태가 아닐 때,
//				if((m_pModule[i].GetOperationFlag()&CModule::OPERATION_FLAG_USER)&&m_pUnit[CUnit::GetUnitIndex(m_pModule[i].GetUnitNo())].IsValidPath())
//				{
//					// Module내 모든 Channel에 대하여
//					for(BYTE j=0; j<m_pModule[i].GetNumOfChannels(); j++)
//					{
//						BOOL bCheck = FALSE;
//						UINT uOpSts = m_pModule[i].GetOperationStatusOfCh(j);
//
//						// 선택된 상태에 해당하는 채널들을 Check해서
//						if(uOpSts&CChannel::CH_OPERATION_STATUS_INITIAL)
//						{
//							if(SelStatus&CSelModuleDialogBar::STATE_CHECK_INITIAL) bCheck=TRUE;
//						}
//						else if(uOpSts&(CChannel::CH_OPERATION_STATUS_STAND_BY|
//								        CChannel::CH_OPERATION_STATUS_CHARGE|
//									    CChannel::CH_OPERATION_STATUS_CHARGE_BREAK|
//									    CChannel::CH_OPERATION_STATUS_DCIR_IN_CHARGE_BREAK|
//									    CChannel::CH_OPERATION_STATUS_DISCHARGE|
//									    CChannel::CH_OPERATION_STATUS_DISCHARGE_BREAK|
//									    CChannel::CH_OPERATION_STATUS_DCIR_IN_DISCHARGE_BREAK
//								       )
//							   )
//						{
//							if(SelStatus&CSelModuleDialogBar::STATE_CHECK_RUN) bCheck=TRUE;
//						}
//						else if(uOpSts&CChannel::CH_OPERATION_STATUS_ERROR)
//						{
//							if(SelStatus&CSelModuleDialogBar::STATE_CHECK_ERROR) bCheck=TRUE;
//						}
//						else if(uOpSts&CChannel::CH_OPERATION_STATUS_PAUSE)
//						{
//							if(SelStatus&CSelModuleDialogBar::STATE_CHECK_PAUSE) bCheck=TRUE;
//						}
//						else if(uOpSts&(CChannel::CH_OPERATION_STATUS_USER_STOP|
//									    CChannel::CH_OPERATION_STATUS_NORMAL_END
//								        )
//							   )
//						{
//							if(SelStatus&CSelModuleDialogBar::STATE_CHECK_FINISH) bCheck=TRUE;
//						}
//
//						// ListCtrl에 넣어준다.
//						if(bCheck)
//						{
//							LV_ITEM lvitem;
//							lvitem.iItem    = iItem;
//							lvitem.iSubItem = 0;
//							lvitem.mask     = LVIF_TEXT|LVIF_IMAGE;
//
//							switch(m_pModule[i].GetOperationStatusOfCh(j))
//							{
//							case CChannel::CH_OPERATION_STATUS_INITIAL:
//								lvitem.iImage = 0;
//								break;
//							case CChannel::CH_OPERATION_STATUS_STAND_BY:
//							case CChannel::CH_OPERATION_STATUS_CHARGE:
//							case CChannel::CH_OPERATION_STATUS_CHARGE_BREAK:
//							case CChannel::CH_OPERATION_STATUS_DCIR_IN_CHARGE_BREAK:
//							case CChannel::CH_OPERATION_STATUS_DISCHARGE:
//							case CChannel::CH_OPERATION_STATUS_DISCHARGE_BREAK:
//							case CChannel::CH_OPERATION_STATUS_DCIR_IN_DISCHARGE_BREAK:
//								lvitem.iImage = 1;
//								break;
//							case CChannel::CH_OPERATION_STATUS_ERROR:
//								lvitem.iImage = 2;
//								break;
//							case CChannel::CH_OPERATION_STATUS_PAUSE:
//								lvitem.iImage = 3;
//								break;
//							case CChannel::CH_OPERATION_STATUS_USER_STOP:
//							case CChannel::CH_OPERATION_STATUS_NORMAL_END:
//								lvitem.iImage = 4;
//								break;
//							}
//							CString str;
//							str.Format("U%02dM%02dCH%02d",
//								       m_pModule[i].GetUnitNo(),
//									   m_pModule[i].GetModuleNo(),
//									   j+1);
//							lvitem.pszText = (LPTSTR)(LPCTSTR) str;
//							pView->m_ChListCtrl.InsertItem(&lvitem);
//							//
//							iItem++;
//							POINT pt = {i,j};
//							m_listIndexFromListToCh.AddTail(pt);
//						}
//					}
//				}
//			}
//		}
//	}
	//
	return;
}

void CMonitorDoc::SendCommand(CList<int, int&>* pList, BYTE command, LPCTSTR strTestName, LPCTSTR strPtnName)
{
/////////////////////////////////////////////////////////////////////////////////
	/*
	 *
	 * 사용자가 선택한 Unit의 번호를 알아내야 한다.
	 */
//	{
//		CFileFind aFinder;
//		CString strCommandFileName;
//		POSITION pos;
//		CList<BYTE, BYTE&> UnitList;
//		pos = pList->GetHeadPosition();
//		while(pos)
//		{
//			POINT pt = m_listIndexFromListToCh.GetAt(
//				m_listIndexFromListToCh.FindIndex(pList->GetNext(pos)));
//			BYTE  no = m_pModule[pt.x].GetUnitNo();
//			if( UnitList.Find(no) == NULL ) 
//				UnitList.AddTail(no);
//		}
//
///*		pos = UnitList.GetHeadPosition();
//		while(pos)
//		{
//			//
//			BYTE UnitNo = UnitList.GetNext(pos);
//			gUnitNo = UnitNo;
//			int N = 0;
//			strCommandFileName.Format("%s\\command\\processing.cmd", m_pUnit[CUnit::GetUnitIndex(UnitNo)].GetPath());
//			if ( aFinder.FindFile(strCommandFileName) )
//			{
//				MessageBox(NULL, "다른 Command가 아직 처리중입니다. 잠시만 기다려주십시오", "Command 중복 전송", MB_ICONEXCLAMATION);
//				return ;
//			}
//		}
//*/
//	}
//
///////////////////////////////////////////////////////////////////////////////////
//
//	ASSERT(pList->GetCount()>0);
//
//	if(IDYES!=MessageBox(NULL,"Command를 실행하시겠습니까?", "Command 전송 확인", MB_YESNO|MB_ICONQUESTION)) return;
//
//	CString strFileContents;
//	//
//	// Test 시작인 경우, 
//	//
//	if(command==CChannel::USER_COMMAND_START_TEST)
//	{
//		//
//		// 선택된 Pattern이 각 Channel에 적합한지를 판단한다.
//		//
//		CString strMsg;
//		POSITION pos = pList->GetHeadPosition();
//		POINT pt;
//		while(pos)
//		{
//			POSITION PrePos = pos;
//			pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(pos)));
//			CString str = m_pModule[pt.x].IsItPossibleToApplyThisPatternToCh(
//												(BYTE)pt.y,
//												CCTSAnalProApp::m_strOriginFolderPath+"\\pattern\\"+strPtnName+".pat");
//			if(!str.IsEmpty())
//			{
//				pList->RemoveAt(PrePos);
//				//
//				CString strTemp;
//				strTemp.Format("M%02dCH%02d: %s\r\n", m_pModule[pt.x].GetModuleNo(), pt.y+1, str);
//				strMsg+=strTemp;
//			}
//		}
//		//
//		if(!strMsg.IsEmpty())
//		{
//			if(!m_pModule[pt.x].LongMessageBox(strMsg,0x01)) return;
//		}
//		
//		//
//		if(pList->IsEmpty())
//		{
//			MessageBox(NULL, "Command 전송에 실패했습니다.\n\n[선택한 Pattern이 적용될 수 없습니다.]", "Error", MB_ICONEXCLAMATION);
//			return;
//		}
//
//		//
//		// 전압이 0.0V인 채널이 있는지 확인한다.
//		//
///*		strMsg.Empty();
//		pos = pList->GetHeadPosition();
//		while(pos)
//		{
//			pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(pos)));
//			float fltV = m_pModule[pt.x].GetTransducerCurValueOfCh((BYTE)pt.y,CCondition::TRANSDUCER_INDEX_VOLTAGE);
//			if(fltV==0.0f)
//			{
//				CString strTemp;
//				strTemp.Format("M%02dCH%02d\r\n", m_pModule[pt.x].GetModuleNo(), pt.y+1);
//				strMsg+=strTemp;
//			}
//		}
//		//
//		if(!strMsg.IsEmpty())
//		{
//		}
//*/
//		//
//		// Test 시작인 경우, 필요한 사전작업
//		//
//		CList<BYTE, BYTE&> UnitList;
//		pos = pList->GetHeadPosition();
//		while(pos)
//		{
//			POINT pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(pos)));
//			BYTE  no = m_pModule[pt.x].GetUnitNo();
//			if(UnitList.Find(no)==NULL) UnitList.AddTail(no);
//		}
//
//		pos = UnitList.GetHeadPosition();
//		while(pos)
//		{
//			//
//			BYTE UnitNo = UnitList.GetNext(pos);
//			int N = 0;
//			POSITION ChPos = pList->GetHeadPosition();
//			while(ChPos)
//			{
//				POINT pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(ChPos)));
//				if(m_pModule[pt.x].GetUnitNo()==UnitNo) N++;
//			}
//			//
//			CString strDestinationFolder;
//			strDestinationFolder.Format("%s\\data\\%s",
//										m_pUnit[CUnit::GetUnitIndex(UnitNo)].GetPath(),
//										strTestName);
//			CString strSourcePtnFilePath = CCTSAnalProApp::m_strOriginFolderPath+"\\pattern\\"+strPtnName+".pat";
//	
//			strFileContents += strDestinationFolder + "\n";
//			//
//			if(!CreateDirectory(strDestinationFolder,NULL)) 
//			{
//				continue;
//			}
//	
//			//
//			char* pFrom = new char[(strSourcePtnFilePath.GetLength()+1)*N+1];
//			char* pTo   = new char[(strDestinationFolder.GetLength()+9+CString(strPtnName).GetLength()+4+1)*N+1];
//			int n = 0;
//			memset(pFrom, 0, (strSourcePtnFilePath.GetLength()+1)*N+1);
//			memset(pTo, 0, (strDestinationFolder.GetLength()+9+CString(strPtnName).GetLength()+4+1)*N+1);
//
//			ChPos = pList->GetHeadPosition();
//			while(ChPos)
//			{
//				POINT pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(ChPos)));
//				if(m_pModule[pt.x].GetUnitNo()==UnitNo)
//				{
//					CString strTmp = strPtnName;				//
//					strTmp = strTmp.Mid(strTmp.Find("\\")+1);	//
//
//					CString strDBFile;
//					strDBFile.Format("%s\\M%02dCh%02d\\%s.pat",
//						             strDestinationFolder,
//									 m_pModule[pt.x].GetModuleNo(),
//									 pt.y+1,
//									 strTmp);//strPtnName);
//					//
//					CreateDirectory(strDBFile.Left(strDBFile.ReverseFind('\\')), NULL);
//					//
//					for(int j=0;j<strSourcePtnFilePath.GetLength();j++) pFrom[(strSourcePtnFilePath.GetLength()+1)*n+j] = strSourcePtnFilePath.GetAt(j);
//					pFrom[strSourcePtnFilePath.GetLength()*(n+1)+n] = '\0';
//					for(    j=0;j<strDBFile.GetLength();           j++) pTo  [(strDBFile.GetLength()+           1)*n+j] = strDBFile.GetAt(j);
//					pTo  [strDBFile.GetLength()*           (n+1)+n] = '\0';
//					//
//					n++;
//				}
//			}
//			//
//			pFrom[(strSourcePtnFilePath.GetLength()+1)*N] = '\0';
//			pTo  [(strDestinationFolder.GetLength()+9+CString(strPtnName).GetLength()+4+1)*N] = '\0';
//
//			SHFILEOPSTRUCT sto;
//			memset(&sto,0,sizeof(sto));
//			sto.wFunc=FO_COPY;
//			sto.fFlags = FOF_MULTIDESTFILES|FOF_SILENT;
//			sto.pFrom = pFrom;
//			sto.pTo   = pTo;
//			int result = ::SHFileOperation(&sto);
//
//			delete [] pFrom;
//			delete [] pTo;
//	
//			//
//			if(result!=0)
//			{
//				sto.wFunc = FO_DELETE;
//				pFrom=new char[strDestinationFolder.GetLength()+1];
//				for(int i=0; i<strDestinationFolder.GetLength(); i++) pFrom[i]=strDestinationFolder.GetAt(i);
//				pFrom[strDestinationFolder.GetLength()] = '\0';
//				sto.pFrom = pFrom;
//				::SHFileOperation(&sto);
//				delete [] pFrom;
//			}
//		}
//	}
//	
//	//
//	// Command File 전송.
//	//
//
//	// 선택한 채널에 포함된 Module번호 List를 얻는다.
//	CList<BYTE, BYTE&> ModuleList;
//	POSITION pos = pList->GetHeadPosition();
//	while(pos)
//	{
//		POINT pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(pos)));
//		BYTE  no = m_pModule[pt.x].GetModuleNo();
//		if(ModuleList.Find(no)==NULL) 
//			ModuleList.AddTail(no);
//	}
//
//	///////////////////////////////////////////////////////
//	// Version 2.0 (2003-03-08)
//	int nModuleIndex;
//	BYTE byCmd; 
//	BYTE byChannelNum;
//	DWORD dwChannel[2];
//	{ 
//		switch ( command )
//		{
//		case CChannel::USER_COMMAND_START_TEST :
//			byCmd = EP_CMD_RUN;			break;
//		case CChannel::USER_COMMAND_STOP_NOW :
//			byCmd = EP_CMD_STOP;		break;
//		case CChannel::USER_COMMAND_RETRY :
//			byCmd = EP_CMD_CONTINUE;	break;
//		case CChannel::USER_COMMAND_PAUSE :
//			byCmd = EP_CMD_PAUSE;		break;
//		}
//	} ///////////////////////////////////////////////////////////////////
//
//	// 각 Module별로 Command file을 기록한다.
//	pos = ModuleList.GetHeadPosition();
//
//	CPtrArray aSendCmd;
//	LPEP_CMD_PACKET pCmd = NULL;
//	BYTE byPrevUnit = 0;
//	while(pos)
//	{
//		BYTE    ModuleNo = ModuleList.GetNext(pos);
//		nModuleIndex = CModule::GetModuleIndex(ModuleNo);
//
//		BYTE    UnitNo   = m_pModule[nModuleIndex].GetUnitNo();
//		if( byPrevUnit != UnitNo )
//		{
//			byPrevUnit = UnitNo;
//			pCmd = new EP_CMD_PACKET;
//			ZeroMemory(pCmd, sizeof(EP_CMD_PACKET));
//
//			pCmd->byUnitID = UnitNo;
//			pCmd->byCmd = byCmd;
//
//			aSendCmd.Add(pCmd);
//		}
//
//		if( pCmd == NULL )
//			continue;
//
//		// 각 모듈별로 채널선택정보 초기화
//		ZeroMemory(dwChannel, sizeof(dwChannel));
//		DWORD lBit = 1;
//		byChannelNum = 0;
//		
//		POSITION ChPos = pList->GetHeadPosition();
//		while(ChPos)
//		{
//			POINT pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(ChPos)));
//			BYTE  no = m_pModule[pt.x].GetModuleNo();
//			lBit = 1;
//			if(ModuleNo==no)
//			{
//				TRACE("Module %d Channel %d\n", pt.x, pt.y);
//
//				// 선택된 채널
//				if( pt.y < sizeof(DWORD)*8 )
//					dwChannel[0] += lBit << pt.y;
//				else
//					dwChannel[1] += lBit << (pt.y-sizeof(DWORD)*8);
//				byChannelNum++;
//			}
//		}
//		int nTmpIndex = CModule::GetModuleIndex(ModuleNo) % EP_MAX_MODULE_NUM;
//		WORD wdBit = 0x0001;
//		pCmd->wdChNum += byChannelNum;
//		pCmd->dwChannelLow[nTmpIndex] = dwChannel[0];
//		pCmd->dwChannelHigh[nTmpIndex] = dwChannel[1];
//		pCmd->wdModuleID += (wdBit << nTmpIndex);
//
//		TRACE("%d\n", CModule::GetModuleIndex(ModuleNo));
//
//		if( strTestName )
//			pCmd->wdDataSize = strlen(strTestName);
//		else
//			pCmd->wdDataSize = 0;
//	}
//	
//	for( int nI = 0; nI < aSendCmd.GetSize(); nI++ )
//	{
//		pCmd = (LPEP_CMD_PACKET)aSendCmd.GetAt(nI);
//		BYTE UnitNo = pCmd->byUnitID;
//		CCommandSocket* pSock = GetValidSocket(UnitNo);
//		if( pSock )
//		{
//			if( command==CChannel::USER_COMMAND_START_TEST )
//			{
//				m_strModuleTestName[nI].Format("%s\\data\\%s",
//						m_pUnit[CUnit::GetUnitIndex(UnitNo)].GetPath(),
//						strTestName);
//			}
//			else
//			{
//				m_strModuleTestName[nI] = _T("");
//			}
//			pSock->SendCommand(byCmd, pCmd, (LPSTR)strTestName);
//			
//			m_nElapsedTime[nI] = 0;
//			m_nReceiveAck[nI] = 0x00;
//				
//			POSITION vPos = GetFirstViewPosition();
//			CMonitorView* pView = (CMonitorView *)GetNextView(vPos);
//			pView->SetCommandProgress(UnitNo);
//			
//			delete pCmd;
//			Sleep(100);
//		}
//	}
//
//	aSendCmd.RemoveAll();
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///* //2003-03-08
//		CString strPath;
//		strPath.Format("%s\\command\\module%02d.cmd",
//			           m_pUnit[CUnit::GetUnitIndex(UnitNo)].GetPath(),
//					   ModuleNo);
//		CStdioFile afile;
//		if(!afile.Open(strPath,CFile::modeCreate|CFile::modeWrite|CFile::typeText|CFile::shareDenyNone)) return;
//			//
//			CString strContent;
//			if(command==CChannel::USER_COMMAND_START_TEST) strContent.Format("Test=%s\n",strTestName);
//			else                                           strContent.Format("Command=%d\n",command);
//			afile.WriteString(strContent);
//			//
//			POSITION ChPos = pList->GetHeadPosition();
//			while(ChPos)
//			{
//				POINT pt = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetNext(ChPos)));
//				BYTE  no = m_pModule[pt.x].GetModuleNo();
//				if(ModuleNo==no)
//				{
//					strContent.Format("%d\n", pt.y+1);
//					afile.WriteString(strContent);
//				}
//			}
//		afile.Close();
//	}
//*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	//
//	if(command == CChannel::USER_COMMAND_SHOW_PULSE_2)
//	{
//		POINT pt     = m_listIndexFromListToCh.GetAt(m_listIndexFromListToCh.FindIndex(pList->GetHead()));
//		BYTE  UnitNo = m_pModule[pt.x].GetUnitNo();
//		CString strPath;
//		strPath.Format("%s\\status\\pulse_data_ok.dat",
//			           m_pUnit[CUnit::GetUnitIndex(UnitNo)].GetPath());
//		CFileFind afinder;
//		COleDateTime start = COleDateTime::GetCurrentTime();
//		BOOL bFail = FALSE;
//		while(!bFail)
//		{
//			if(afinder.FindFile(strPath)) break;
//			COleDateTimeSpan sp = COleDateTime::GetCurrentTime() - start;
//			if(sp.GetTotalSeconds()>5.0) bFail = TRUE;
//		}
//		if(!bFail)
//		{
//			DeleteFile(strPath);
//
//			CPulseDataDialog aDlg;
//			aDlg.m_strTitle.Format("Pulse Data: Module%02d, CH%02d",
//				                   m_pModule[pt.x].GetModuleNo(),
//								   pt.y+1);
//			strPath.Format("%s\\status\\pulse_data.dat",
//				           m_pUnit[CUnit::GetUnitIndex(UnitNo)].GetPath());
//			CStdioFile afile;
//			afile.Open(strPath, CFile::modeRead|CFile::typeText|CFile::shareDenyNone);
//				// 내용을 읽어옴
//				CString strTemp;
//				while(afile.ReadString(strTemp))
//				{
//					if(!strTemp.IsEmpty())
//					{
//						float crt = (float)atof(strTemp);
//						aDlg.m_fltlistData.AddTail(crt);
//					}
//				}
//			afile.Close();
//			DeleteFile(strPath);
//			aDlg.DoModal();
//		}
//	}
}

BOOL CMonitorDoc::IsThisCommandPossible(CList<int, int&>* pList, BYTE command)
{
//	if(pList == NULL)	return FALSE;
//	//
//	POSITION pos = pList->GetHeadPosition();
//	if(pos ==NULL) return FALSE;
//
//	//
//	POSITION pos2;
//	while(pos)
//	{	
//		pos2 = m_listIndexFromListToCh.FindIndex(pList->GetNext(pos));
//		if(pos2 == NULL)	continue;
//		POINT pt = m_listIndexFromListToCh.GetAt(pos2);
//		if(!m_pModule[pt.x].IsThisCommandPossibleForCh((BYTE)pt.y, command)) return FALSE;
//	}
//
//	//
	return TRUE;
}

void CMonitorDoc::GetDisplayedModuleIndexList(CList<int, int&>* pList)
{
	if(pList == NULL)	return;
	POSITION pos = m_listIndexFromListToCh.GetHeadPosition();
	while(pos)
	{
		int index = m_listIndexFromListToCh.GetNext(pos).x;
		if(pList->Find(index)==NULL) pList->AddTail(index);
	}
}

BOOL CMonitorDoc::GetRunningTestList(CStringList* pList)
{
	// Network에 이상이 있는 Unit가 있는지 Check한다.
//	{
//		for(BYTE i=0; i<CUnit::m_byNumOfUnit; i++)
//		{
//			if(!m_pUnit[i].IsValidPath()) return FALSE;
//		}
//	}
//
//	// 각 Module의 back-up file을 읽어서
//	{
//		for(BYTE module_index=0; module_index<CModule::m_byNumOfModules; module_index++)
//		{
//			BYTE unit_index = CUnit::GetUnitIndex(m_pModule[module_index].GetUnitNo());
//			if(unit_index==0xFF)
//				continue;
//			CString strBakFilePath;
//			strBakFilePath.Format("%s\\status\\module%02d.bak",
//				                  m_pUnit[unit_index].GetPath(),
//								  m_pModule[module_index].GetModuleNo());
//			CFileFind afinder;
//			if(afinder.FindFile(strBakFilePath))
//			{
//				if(m_pModule[module_index].LoadBackFile(strBakFilePath)==0x00)
//				{
//					//
//					for(BYTE ch=0; ch<m_pModule[module_index].GetNumOfChannels(); ch++)
//					{
//						if(m_pModule[module_index].IsThisChRunningTest(ch)||m_pModule[module_index].IsThisChHaltingTest(ch))
//						{
//							CString str = m_pModule[module_index].GetTestNameOfCh(ch);
//							str.MakeLower();
//							if(pList->Find(str)==NULL) pList->AddTail(str);
//						}
//					}
//				}
//				else return FALSE;
//			}
//			else return FALSE;
//		}
//	}

	//
	return TRUE;
}

BOOL CMonitorDoc::BringTestDataIntoDataPC(BOOL bAuto)
{
//	OSVERSIONINFO os;
//	os.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
//	::GetVersionEx(&os);
//	if(os.dwPlatformId==VER_PLATFORM_WIN32_WINDOWS&&os.dwMinorVersion==0){
//		AfxMessageBox("Data file transfer is protected in Windows 95");
//		return FALSE;
//	}
//
//	//
//	for(BYTE i=0; i<CUnit::m_byNumOfUnit; i++)
//	{
//		if(m_pUnit[i].IsValidPath())
//		{
//			// Unit와의 Network 연결에 이상이 있을 때에는,
//			if(!m_pUnit[i].CheckWhetherPathIsValid())
//			{
//				m_pUnit[i].SaveWhetherPathIsValid(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb");
//				return FALSE;
//			}
//		}
//		else return FALSE;
//	}
//
//	// 먼저 Data-View MDI Child Frame을 모두 종료시킨다.
//	{
//		POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
//		CDocTemplate* pTempl = AfxGetApp()->GetNextDocTemplate(pos);
//		pTempl->CloseAllDocuments(TRUE);
//	}
//
//	// 현재 Running 중인 모든 Test의 이름의 List를 얻는다.
//	CStringList RunningTestList;
//	if(!GetRunningTestList(&RunningTestList))
//		return FALSE;
//
//	//////////////////////
//	// 자동 수행인 경우 //
//	//////////////////////
//	if(bAuto) CData::AutoLoad(&RunningTestList);
//	///////////////////////
//	// 수동 수행인 경우, //
//	///////////////////////
//	else      CData::ManageData(&RunningTestList);
//
//	//
	return TRUE;
}

UINT ThreadFuncBringCycleData(LPVOID lpData)
{
//	S_RESULT_BAK_P pData = (S_RESULT_BAK_P)lpData ;
//
//	CString strSourcePath ;
//	CString strDestPath ;
//	char szCurrentDir[128];
////	::GetCurrentDirectory(128, szCurrentDir);
//
//	CCTSAnalProApp* pApp = (CCTSAnalProApp*)AfxGetApp();
//	strcpy(szCurrentDir, pApp->m_szCurrentDirectory);
//	// X:\Data\Test\M01Ch01\
//	
//	if ( !strcmp(pData->szTestName, "") )
//	{
//		delete pData;
////		TRACE("pData->szTestName == \"\"\n");
//		return 0;
//	}
//	strSourcePath.Format("%s\\Data\\%s\\M%02dCh%02d\\", 
//		pData->szSourcePath, 
//		pData->szTestName,
//		pData->nModuleID,
//		pData->nChannelID);
////	TRACE("Source Path : %s\n", strSourcePath);
//	
//	// C:\Share\Data\Test
//	strDestPath.Format("%s\\%s", szCurrentDir, pData->szTestName);
//	CFileFind aFinder;
//	if( aFinder.FindFile(strDestPath) == FALSE )
//	{
//		if( CreateDirectory(strDestPath, NULL) )
//			TRACE("Test Directory 생성 성공 : %s\n", strDestPath);
//		else
//		{
//			TRACE("Test Directory 생성 실패 : %s\n", strDestPath);
//		}
//	}
//
//	CString strDestChFile;
//
//	// C:\Share\Data\Test\M01Ch01
//	strDestChFile.Format("%s\\M%02dCh%02d", strDestPath, pData->nModuleID, pData->nChannelID);
//
//	if ( aFinder.FindFile(strDestChFile) == FALSE )
//	{
//		if ( CreateDirectory(strDestChFile, NULL) )
//		{
////			TRACE("Channel Directory 생성 성공 : %s\n", strDestChFile);
//
//			// X:\Data\Test\M01Ch01\*.pat
//			CString strTmp = strSourcePath + "*.pat";
////			TRACE("Pattern File name : %s\n", strTmp);
//			if ( aFinder.FindFile(strTmp) == TRUE )
//			{
//				aFinder.FindNextFile();
//				strTmp = aFinder.GetFileName();
//
//				// C:\Share\Data\Test\M01Ch01\abcd.pat
//				CString strDestTmp = strDestChFile + "\\" + strTmp;
//					//strTmp.Mid(strTmp.ReverseFind('\\')+1);
//
//				// X:\Data\Test\M01Ch01\abcd.pat, C:\Share\Data\Test\M01Ch01\abcd.pat
//				if( CopyFile(strSourcePath + "\\" + strTmp, strDestTmp, FALSE) == TRUE )
////					TRACE("Pattern File Copy 성공\n");
//					;
//				else
////					TRACE("Pattern File Copy 실패 : %s - %s\n", strTmp, strDestTmp);
//					;
//			}
//		}
//		else
//		{
////			TRACE("Channel Directory 생성 실패 : %s\n", strDestChFile);
//		}
//	}
//
//	CString strDestFile;
//	CString strSourceFile;
//
//	char pFrom [128];
//	char pTo [128];
//
//	memset(pFrom, 0, 128);
//	memset(pTo, 0, 128);
//
//	int nDestOffset = 0;
//	int nSourceOffset = 0;
///*
//	for ( int nX = 0; nX <= pData->nCycleNum; nX++ )
//	{
//		for( int nI = 1; nI <= 4; nI++ )
//		{
//			strSourceFile.Format("%s%06d", 
//				strSourcePath, 
//				nX * 4 + nI
//				//pData->nCycleNum * 4 + nI
//			);
//			if ( aFinder.FindFile(strSourceFile) == TRUE )
//			{
//				aFinder.Close();
//				strDestFile.Format("%s\\M%02dCh%02d\\%06d", 
//					strDestPath, 
//					pData->nModuleID, 
//					pData->nChannelID,
//					nX * 4 + nI
//					//pData->nCycleNum * 4 + nI
//				);	
//				
//				for(int j=0; j<strSourceFile.GetLength(); j++) pFrom[j]=strSourceFile.GetAt(j);
//				pFrom[strSourceFile.GetLength()]='\0'; pFrom[strSourceFile.GetLength()+1]='\0';
//
//				for(j=0; j<strDestFile.GetLength(); j++) pTo[j]=strDestFile.GetAt(j);
//				pTo[strDestFile.GetLength()]='\0'; pTo[strDestFile.GetLength()+1]='\0';
//
//				// Move Window Shell Operation..
//				{
//					SHFILEOPSTRUCT FileOp;
//					memset(&FileOp,0,sizeof(FileOp));
//					FileOp.wFunc=FO_MOVE;
//					FileOp.fFlags = FOF_NOCONFIRMATION | FOF_SILENT | FOF_NOERRORUI;
//					FileOp.pFrom=pFrom;
//					FileOp.pTo=pTo;
//					int nResult = SHFileOperation(&FileOp) ;
//					TRACE("Source File : %s <Result Code : %d>\n", strSourceFile, nResult);
//				}
//			}
//		}
//	}
//*/
//	strDestFile.Format("%s\\M%02dCh%02d", 
//		strDestPath, 
//		pData->nModuleID, 
//		pData->nChannelID);
//	aFinder.Close();
//
//	BOOL bFound = aFinder.FindFile(strSourcePath + "*.*");
//			
//	CString strDest = strDestFile;
//	CString strFileName;
//	int nMaxBoundaryNo = (pData->nCycleNum + 1) * 4;
//	while(bFound)
//	{
//		bFound = aFinder.FindNextFile();
//		if( !aFinder.IsDots() && !aFinder.IsDirectory() )
//		{
//			strFileName = aFinder.GetFileName();
//			strFileName.MakeLower();
//			if ( strFileName.Compare("tablelist") && strFileName.Find(".pat") < 0 )
//			{
//				int nFileNum = atoi(strFileName);
//				if ( nMaxBoundaryNo >= nFileNum )
//				{
//					strSourceFile.Format("%s%06d", 
//						strSourcePath, 
//						atoi(strFileName)
//					);
//					strDestFile.Format("%s\\%s", strDest, strFileName);
//					//strDestFile += "\\" + strFileName;
//
//					for(int j=0; j<strSourceFile.GetLength(); j++) pFrom[j]=strSourceFile.GetAt(j);
//					pFrom[strSourceFile.GetLength()]='\0'; pFrom[strSourceFile.GetLength()+1]='\0';
//
//					for(j=0; j<strDestFile.GetLength(); j++) pTo[j]=strDestFile.GetAt(j);
//					pTo[strDestFile.GetLength()]='\0'; pTo[strDestFile.GetLength()+1]='\0';
//
//					// Move Window Shell Operation..
//					{
//						SHFILEOPSTRUCT FileOp;
//						memset(&FileOp,0,sizeof(FileOp));
//						FileOp.wFunc=FO_MOVE;
//						FileOp.fFlags = FOF_NOCONFIRMATION | FOF_SILENT | FOF_NOERRORUI;
//						FileOp.pFrom=pFrom;
//						FileOp.pTo=pTo;
//						int nResult = SHFileOperation(&FileOp) ;
//					}
//				}
//			}
//			else if ( !strFileName.Compare("tablelist") )
//			{
//				CString strTmp = strSourcePath + "TableList";
//				TRACE("TableList File name : %s\n", strTmp);
//				if ( aFinder.FindFile(strTmp) == TRUE )
//				{
//					///////////////////////////////////////////////////////////
//					// C:\Share\Data\Test\M01Ch01\TableList
//					CString strDestTmp = strDestChFile + "\\TableList";
//
//					///////////////////////////////////////////////////////////
//					// X:\Data\Test\M01Ch01\TableList, C:\Share\Data\Test\M01Ch01\TableList
//					if( CopyFile(strTmp, strDestTmp, FALSE) == FALSE )
//						TRACE("TableList File Copy 실패 : %s - %s\n", strTmp, strDestTmp);
//				}
//			}
//		}
//	}
//	// TableList File Copying
//		
//	delete pData;
//
	return 0;
}

/* 
 *	마지막 모든 데이터를 Downloading 하고 기존의 Directory를 삭제한다.
 */
UINT ThreadFuncBringLastData(LPVOID lpData)
{
//	S_RESULT_BAK_P pData = (S_RESULT_BAK_P)lpData ;
//
//	CString strSourcePath ;
//	CString strDestPath ;
//	char szCurrentDir[128];
//
//	CCTSAnalProApp* pApp = (CCTSAnalProApp*)AfxGetApp();
//	strcpy(szCurrentDir, pApp->m_szCurrentDirectory);
//	
//	// X:\Data\Test\M01Ch01\\..
//	if ( !strcmp(pData->szTestName, "") )
//	{
//		delete pData;
////		TRACE("pData->szTestName == \"\"\n");
//		return 0;
//	}
//	strSourcePath.Format("%s\\Data\\%s\\M%02dCh%02d", 
//		pData->szSourcePath, 
//		pData->szTestName,
//		pData->nModuleID,
//		pData->nChannelID);
////	TRACE("Source Path : %s\n", strSourcePath);
//	
//	// C:\Share\Data\Test
//	strDestPath.Format("%s\\%s", szCurrentDir, pData->szTestName);
//	CFileFind aFinder;
//	if( aFinder.FindFile(strDestPath) == FALSE )
//	{
//		if( CreateDirectory(strDestPath, NULL) )
//			TRACE("Test Directory 생성 성공 : %s\n", strDestPath);
//		else
//		{
//			TRACE("Test Directory 생성 실패 : %s\n", strDestPath);
//		}
//	}
//
//	CString strDestFile;
//
//	// C:\Share\Data\Test\M01Ch01
//	//strDestFile.Format("%s\\M%02dCh%02d", strDestPath, pData->nModuleID, pData->nChannelID);
//	strDestFile = strDestPath;
//
//	// 파일을 모두 옮긴다.
//	if ( aFinder.FindFile(strSourcePath) == TRUE )
//	{
//		char pFrom [128];
//		char pTo [128];
//
//		memset(pFrom, 0, 128);
//		memset(pTo, 0, 128);
//
//		for(int j=0; j<strSourcePath.GetLength(); j++) pFrom[j]=strSourcePath.GetAt(j);
//			pFrom[strSourcePath.GetLength()]='\0'; pFrom[strSourcePath.GetLength()+1]='\0';
//
//		for(j=0; j<strDestFile.GetLength(); j++) pTo[j]=strDestFile.GetAt(j);
//			pTo[strDestFile.GetLength()]='\0'; pTo[strDestFile.GetLength()+1]='\0';
//
//		// Move Window Shell Operation..
//		{
//			SHFILEOPSTRUCT FileOp;
//			memset(&FileOp,0,sizeof(FileOp));
//			FileOp.wFunc=FO_MOVE;
//			FileOp.fFlags = FOF_NOCONFIRMATION | FOF_SILENT | FOF_NOERRORUI;
//			FileOp.pFrom=pFrom;
//			FileOp.pTo=pTo;
//			int nResult = SHFileOperation(&FileOp) ;
////			TRACE("Source File : %s <Result Code : %d>\n", strSourcePath, nResult);
//		}
//		aFinder.Close();
//	}
//
//	CString strDeletePath;
//	strDeletePath.Format("%s\\Data\\%s", 
//		pData->szSourcePath, 
//		pData->szTestName);
//
//	if ( aFinder.FindFile(strDeletePath + "\\*") == TRUE )
//	{
//		BOOL bFound = TRUE ;
//		CString strFoundFile;
//		bool bExistAnotherFile = false;
//		while( bFound )
//		{
//			bFound = aFinder.FindNextFile();
//			strFoundFile = aFinder.GetFileName();
//			if ( strFoundFile.Compare(_T(".")) && strFoundFile.Compare(_T("..")) )
//			{
//				bExistAnotherFile = true;
//				break;
//			}
//		}
//		aFinder.Close();
//
//		if ( bExistAnotherFile == false )
//		{			
//			char pFrom[128];
//			strcpy(pFrom, strDeletePath);
//			pFrom[strDeletePath.GetLength()+1] = 0;
//			
//			SHFILEOPSTRUCT FileOp;
//			memset(&FileOp,0,sizeof(FileOp));
//			FileOp.wFunc=FO_DELETE;
//			FileOp.fFlags = FOF_NOCONFIRMATION | FOF_SILENT | FOF_NOERRORUI;
//			FileOp.pFrom=pFrom;
//			FileOp.pTo=NULL;
//			int nResult = SHFileOperation(&FileOp) ;
//
//			TRACE("Result Code : %d\n", nResult);
//		}
//	}
//
//	delete pData;

	return 0;
}

//void CMonitorDoc::BringCycleDataIntoDataPC(bool bIsFirstCycle, S_RESULT_BAK_P pData)
//{
//	sprintf(pData->szSourcePath, m_pUnit[pData->nUnitID - 1].GetPath());
////	TRACE("In BringCycleDataIntoDataPC::Ch %03d\n", pData->nChannelID);
//
//	// If byRetVal is 0x02 or 0x03
//	if ( bIsFirstCycle == true )
//		AfxBeginThread(ThreadFuncBringCycleData, (LPVOID)pData);
//	// If byRetVal is 0x04 ( Run ==> Init )
//	else
//		AfxBeginThread(ThreadFuncBringLastData, (LPVOID)pData);
//}

void CMonitorDoc::DeleteSocket(BYTE byUnitNo)
{
//	BYTE byUnitIndex;
//	if( byUnitNo == 0 )
//	{
//		CCommandSocket* pSocket = NULL;
//		for( int nI = 0; nI < m_aCmdSocket.GetSize(); nI++ )
//		{
//			pSocket = (CCommandSocket *)m_aCmdSocket.GetAt(nI);
//			byUnitIndex = pSocket->m_byUnitNo-1;
//			m_pUnit[byUnitIndex].SetNetworkState(CUnit::NETWORK_STATE_LINE_OFF);
//			
//			if( m_pNetStateMonitoringDlg )
//				m_pNetStateMonitoringDlg->SetUnitState(byUnitIndex);
//
//			pSocket->Close();
//			delete pSocket;
//		}
//
//		m_aCmdSocket.RemoveAll();
//
//		if( m_pServerSock )
//			delete m_pServerSock;
//	}
//	else
//	{
//		CCommandSocket* pSocket = NULL;
//		for( int nI = 0; nI < m_aCmdSocket.GetSize(); nI++ )
//		{
//			pSocket = (CCommandSocket *)m_aCmdSocket.GetAt(nI);
//			if( pSocket->m_byUnitNo == byUnitNo )
//			{
//				m_aCmdSocket.RemoveAt(nI);
//				pSocket->Close();
//				delete pSocket;
//				byUnitIndex = byUnitNo-1;
//				m_pUnit[byUnitIndex].SetNetworkState(CUnit::NETWORK_STATE_LINE_OFF);
//
//				if( m_pNetStateMonitoringDlg )
//					m_pNetStateMonitoringDlg->SetUnitState(byUnitIndex);
//
//				TRACE("***Unit %d Disconnected***\n", byUnitNo);
//				break;
//			}
//		}
//	}
}

//CCommandSocket* CMonitorDoc::GetValidSocket(BYTE byUnitNo)
//{
//	CCommandSocket* pSocket = NULL;
//	for( int nI = 0; nI < m_aCmdSocket.GetSize(); nI++ )
//	{
//		pSocket = (CCommandSocket *)m_aCmdSocket.GetAt(nI);
//		if( pSocket->m_byUnitNo == byUnitNo )
//			return pSocket;
//	}
//	return NULL;
//}

void CMonitorDoc::ReceiveErrorReport(BYTE byUnitNo, BYTE byModuleNo, int nErrorCode)
{
	POSITION pos = GetFirstViewPosition();
	CMonitorView* pView = (CMonitorView*) GetNextView(pos);

	pView->PostMessage(WM_ERROR_REPORT, 
		(WPARAM)(byUnitNo*10 + byModuleNo), 
		(LPARAM)nErrorCode);
	pView->WriteLog("Receive Fault Message From Module");
}

void CMonitorDoc::ConnectedUnit(BYTE byUnitNo)
{
//	if( byUnitNo <= 0 || byUnitNo > CUnit::m_byNumOfUnit )
//		return;
//
//	BYTE byUnitIndex = byUnitNo-1;
//	m_pUnit[byUnitIndex].SetNetworkState(CUnit::NETWORK_STATE_LINE_ON);
//
//	if(m_pNetStateMonitoringDlg)
//	{
//		m_pNetStateMonitoringDlg->SetUnitState(byUnitIndex);
//	}
}

void CMonitorDoc::OnCloseDocument() 
{	
	CDocument::OnCloseDocument();
}

BOOL CMonitorDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here

	return TRUE;
}


BOOL CMonitorDoc::SelResultFile()
{

//	CResultTestSelDlg	dlg;
//	CString strFileName;
//	if( dlg.DoModal() == IDCANCEL)		return FALSE;
//
//	m_strDataPath = dlg.m_strFileFolder;		
//	m_strFileList.RemoveAll();
//
//	CStringList *pList = dlg.GetSelList();
//	POSITION pos = pList->GetHeadPosition();
//	while(pos)
//	{	
//		strFileName = pList->GetNext(pos);
//		m_strFileList.AddTail(strFileName);
//	}
	return TRUE;
}

