/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: HelpDoc.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CHelpDoc class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_HELPDOC_H__FA89CAA8_D9B5_446F_B393_9CE4549B9B09__INCLUDED_)
#define AFX_HELPDOC_H__FA89CAA8_D9B5_446F_B393_9CE4549B9B09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HelpDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHelpDoc document

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CHelpDoc
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CHelpDoc : public CDocument
{
protected:
	CHelpDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CHelpDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHelpDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHelpDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CHelpDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HELPDOC_H__FA89CAA8_D9B5_446F_B393_9CE4549B9B09__INCLUDED_)
