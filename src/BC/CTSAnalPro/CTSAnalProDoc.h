/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: CTSAnalProDoc.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CCTSAnalProDoc class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// CTSAnalProDoc.h : interface of the CCTSAnalProDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSAnalProDOC_H__13FE5EAC_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CTSAnalProDOC_H__13FE5EAC_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CCTSAnalProDoc
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CCTSAnalProDoc : public CDocument
{
protected: // create from serialization only
	CCTSAnalProDoc();
	DECLARE_DYNCREATE(CCTSAnalProDoc)

// Attributes
public:
	CData        m_Data;

// Operations
public:
	void  DrawView(CDC* pDC, CRect ClientRect);
	BOOL RequeryDataQuery();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSAnalProDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL RequeryIndexDataQuery(CString strPath, CDWordArray *apIndexArray);	//lmh 20120207
	BOOL m_bLoadedSheet;
	void ReloadData();
	int m_iCurSelPlaneIndex;
	virtual ~CCTSAnalProDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCTSAnalProDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSAnalProDOC_H__13FE5EAC_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
