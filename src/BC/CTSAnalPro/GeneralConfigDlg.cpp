// GraphConfigDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "GeneralConfigDlg.h"
#include "afxdialogex.h"


// CGeneralConfigDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CGeneralConfigDlg, CDialogEx)

CGeneralConfigDlg::CGeneralConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGeneralConfigDlg::IDD, pParent)
	, m_fUnitTransfer(0)
{

}

CGeneralConfigDlg::~CGeneralConfigDlg()
{
}

void CGeneralConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PROPERTY, m_ctrlPropertyList);
	DDX_Text(pDX, IDC_EDIT_UNIT_TRANSFER, m_fUnitTransfer);
}


BEGIN_MESSAGE_MAP(CGeneralConfigDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CGeneralConfigDlg::OnBnClickedOk)
	ON_EN_CHANGE(IDC_EDIT_UNIT_TRANSFER, &CGeneralConfigDlg::OnEnChangeEditUnitTransfer)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_PROPERTY, &CGeneralConfigDlg::OnLvnItemchangedListProperty)
END_MESSAGE_MAP()


// CGeneralConfigDlg 메시지 처리기입니다.


//ksj 20200210 : DB에서 그래프 단위 읽어온다. PROPERTY 테이블
int CGeneralConfigDlg::LoadProperty(void)
{
	CString strTempMDB;
	char path[MAX_PATH];
	GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	CString strTemp(path);
	strTempMDB.Format("%s\\DataBase\\Pack_Schedule_2000.mdb", strTemp.Left(strTemp.ReverseFind('\\')));
	
	CDaoDatabase  db;
	try
	{
		db.Open(strTempMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}	

	CString strSQL,strInsertSQL;

	CString strName,strNotation;
	int nIndex;
	float fUnitTransfer;

	strSQL.Format("SELECT * FROM Property order by Index");
	COleVariant data;
	CDaoRecordset rs(&db);
	rs.Open(dbOpenSnapshot, strSQL, dbReadOnly);

	CRowColArray ra;
	int nRow = 0;
	m_ctrlPropertyList.DeleteAllItems();

	if(!rs.IsBOF() && !rs.IsEOF())
	{
		COleVariant data;
		while(!rs.IsEOF())
		{
			data = rs.GetFieldValue("Index");
			nIndex = data.lVal;
			strTemp.Format("%d",nIndex);
			m_ctrlPropertyList.InsertItem(nRow,strTemp);

			data = rs.GetFieldValue("Name");
			if(VT_NULL != data.vt) strName.Format("%s", data.pbVal);
			else strName.Empty();
			m_ctrlPropertyList.SetItemText(nRow,1,strName);

			data = rs.GetFieldValue("UnitNotation");
			if(VT_NULL != data.vt) strNotation.Format("%s", data.pbVal);
			else strNotation.Empty();
			m_ctrlPropertyList.SetItemText(nRow,2,strNotation);

 			data = rs.GetFieldValue("UnitTransfer");
 			fUnitTransfer = data.fltVal;
 			strTemp.Format("%.3f",fUnitTransfer);
 			m_ctrlPropertyList.SetItemText(nRow,3,strTemp);

			rs.MoveNext();	
			nRow++;
		}	
	}
	rs.Close();
	db.Close();
	return TRUE;
}


void CGeneralConfigDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(UpdateProperty() == FALSE)
	{	
		AfxMessageBox("Failure");
		return;
	}

	CDialogEx::OnOK();
}


int CGeneralConfigDlg::InitListCtrl(void)
{
	DWORD style = 	m_ctrlPropertyList.GetExtendedStyle();
	style |= LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES;
	m_ctrlPropertyList.SetExtendedStyle(style );

	m_ctrlPropertyList.InsertColumn(0, "Index",  LVCFMT_CENTER,  50,  0);
	m_ctrlPropertyList.InsertColumn(1, "Name",  LVCFMT_CENTER,  100,  1);
	m_ctrlPropertyList.InsertColumn(2, "UnitNotation",  LVCFMT_CENTER,  100,  2);
	m_ctrlPropertyList.InsertColumn(3, "UnitTransfer",  LVCFMT_CENTER,  150,  3);

	return 0;
}


BOOL CGeneralConfigDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	InitListCtrl();
	LoadProperty();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CGeneralConfigDlg::OnEnChangeEditUnitTransfer()
{
	UpdateData();
	TRACE("%.6f\n",m_fUnitTransfer);

	POSITION pos = m_ctrlPropertyList.GetFirstSelectedItemPosition();
	int nSel = m_ctrlPropertyList.GetNextSelectedItem(pos);
	if(nSel < 0) return;

	CString strTemp;
	strTemp.Format("%.3f\n",m_fUnitTransfer);	
	m_ctrlPropertyList.SetItemText(nSel,3,strTemp);
}


void CGeneralConfigDlg::OnLvnItemchangedListProperty(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	POSITION pos = m_ctrlPropertyList.GetFirstSelectedItemPosition();
	int nSel = m_ctrlPropertyList.GetNextSelectedItem(pos);
	if(nSel < 0) return;

	m_fUnitTransfer = atof(m_ctrlPropertyList.GetItemText(nSel,3));

	UpdateData(FALSE);

	*pResult = 0;
}


int CGeneralConfigDlg::UpdateProperty(void)
{

	CString strTempMDB;
	char path[MAX_PATH];
	GetModuleFileName(::GetModuleHandle(NULL), path, MAX_PATH);
	CString strTemp(path);
	strTempMDB.Format("%s\\DataBase\\Pack_Schedule_2000.mdb", strTemp.Left(strTemp.ReverseFind('\\')));
	CDaoDatabase  db;

	try
	{
		db.Open(strTempMDB);
	}
	catch (CDaoException* e)
	{
		AfxMessageBox(e->m_pErrorInfo->m_strDescription);
		e->Delete();
		return FALSE;
	}		



	for(int i=0;i<m_ctrlPropertyList.GetItemCount();i++)
	{
		CString strSQL;
		CString strIndex = m_ctrlPropertyList.GetItemText(i,0);
		CString strName = m_ctrlPropertyList.GetItemText(i,1);
		CString strUnitNotation = m_ctrlPropertyList.GetItemText(i,2);		
		CString strUnitTransfer = m_ctrlPropertyList.GetItemText(i,3);		

		strSQL.Format("UPDATE Property SET UnitTransfer=%f WHERE Index=%s AND Name like \"%s*\" AND UnitNotation like \"%s*\"",atof(strUnitTransfer), strIndex, strName, strUnitNotation); //MDB에 text 255로 선언되어있어, 문자열 뒤에 null 값이 있어 * 와일드카드 없이는 찾을 수 없음

		try
		{
			db.Execute(strSQL);
		}
		catch (CDaoException* e)
		{
			AfxMessageBox(e->m_pErrorInfo->m_strDescription);
			e->Delete();
			db.Close();
			return FALSE;
		}	
	}

	

	db.Close();

	return TRUE;
}
