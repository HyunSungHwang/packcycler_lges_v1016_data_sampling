#if !defined(AFX_MONITORDOC_H__3AAEBDE0_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_MONITORDOC_H__3AAEBDE0_F61A_11D4_88DF_006008CEDA07__INCLUDED_

#define BACKUP_FILE_CHECK_PERIOD 3000 // (mSec)
#define UM_CHECK_BACKUP_FILE     (WM_USER+500)

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// MonitorDoc.h : header file
//
#define WM_COMPLETE_CYCLE			WM_USER + 130
typedef struct RESULT_BAK
{
	int nUnitID;
	int nModuleID;
	int nChannelID;
	int nCycleNum;
	char szTestName[128];
	char szSourcePath[128];
} S_RESULT_BAK, *S_RESULT_BAK_P;


/////////////////////////////////////////////////////////////////////////////
// CMonitorDoc document

// #include

//#include "../PSModule/Module.h"
//#include "../PSModule/Unit.h"
#include "NetStateMonitoringDlg.h"



//class CCommandSocket;
//class CServerSocket;
// #define

#define MAX_UNIT_NUM					7 // <--2
#define MAX_MODULE_PER_UNIT_NUM			4

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CMonitorDoc
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CMonitorDoc : public CDocument
{
protected:
	CMonitorDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CMonitorDoc)

// Attributes
public:

	int			m_nElapsedTime[MAX_UNIT_NUM];
//	CUnit*      m_pUnit;
//	CModule*    m_pModule;
	CPtrArray	m_aCmdSocket;
//	CServerSocket* m_pServerSock;
	CString		m_strModuleTestName[MAX_UNIT_NUM];
	BYTE		m_nReceiveAck[MAX_UNIT_NUM];
	DWORD		m_wdModuleID[MAX_UNIT_NUM][MAX_MODULE_PER_UNIT_NUM];
	CNetStateMonitoringDlg* m_pNetStateMonitoringDlg;
private:
	CWinThread* m_pThread;
	BOOL        m_bThread;

	CList<POINT, POINT&> m_listIndexFromListToCh;

// Operations
public:
	BOOL StartMonitor();
	BOOL EndMonitor();
	BOOL StartMonitorThread();
	BOOL EndMonitorThread();
	BOOL ThreadContinue() { return m_bThread; };
	CWinThread* GetThreadPoint() { return m_pThread; };
	void MonitorModules();
	void UpdateChOnView(BOOL bNormal, int module_index, BYTE& byStateCheck);
	void UpdateListView(BYTE UnitNo, BYTE ModuleNo, BYTE SelStatus);
	void SendCommand(CList<int, int&>* pList, BYTE command, LPCTSTR strTestName=NULL, LPCTSTR strPtnName=NULL);
	BOOL IsThisCommandPossible(CList<int, int&>* pList, BYTE command);
	void GetDisplayedModuleIndexList(CList<int, int&>* pList);
	BOOL GetRunningTestList(CStringList* pList);
	BOOL BringTestDataIntoDataPC(BOOL bAuto);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMonitorDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnNewDocument();
	virtual void OnCloseDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL SelResultFile();
	void ConnectedUnit(BYTE byUnitNo);
	void ReceiveErrorReport(BYTE byUnitNo, BYTE byModuleNo, int nErrorCode);
//	CCommandSocket* GetValidSocket(BYTE byUnitNo);
	void DeleteSocket(BYTE byUnitNo = 0);
//	void BringCycleDataIntoDataPC(bool bIsFirstCycle, S_RESULT_BAK_P pData);
	virtual ~CMonitorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CMonitorDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Statics
private:
	static UINT MonitorThreadProc(LPVOID pParam);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MONITORDOC_H__3AAEBDE0_F61A_11D4_88DF_006008CEDA07__INCLUDED_)
