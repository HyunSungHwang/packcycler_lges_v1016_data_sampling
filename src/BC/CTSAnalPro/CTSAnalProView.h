/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: CTSAnalProView.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CCTSAnalProView class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

// CTSAnalProView.h : interface of the CCTSAnalProView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTSAnalProVIEW_H__13FE5EAE_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
#define AFX_CTSAnalProVIEW_H__13FE5EAE_AB5E_11D4_88DF_006008CEDA07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CCTSAnalProView
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------

#define ZOOM_MIN_SIZE	10
#include "CTSAnalProDoc.h"
#include "WorkSTSignAnalDlg.h"
#define IsCTRLPressed() (0x8000 == (GetKeyState(VK_CONTROL)& 0x8000))

class CCTSAnalProView : public CView
{
protected: // create from serialization only
	CCTSAnalProView();
	DECLARE_DYNCREATE(CCTSAnalProView)

// Attributes
private:

	BYTE m_byToolFlag;
	CRect    m_ZoomRect;
//	CStatic* m_pMouseTrackWnd;
	CLabel*  m_pMouseTrackChannelWnd;
	CEdit*	 m_pMouseTrackWnd; //20180710 yulee
	POINT    m_TrackingLineIndex;
	int      m_PointPosInTrackingLine;
	POINT	 m_ptCurrentMousePosition;
	CFont	 g_editFont;
	fltPoint m_fltXPointFromTrackWnd;
	CString  m_strYaxisData;
	CRect	 m_ZoomRectTemp;	// 210804 HKH 그래프 새로고침시 줌 비율 유지

	enum {
		TOOL_NONE					= 0x00,
		TOOL_ZOOM					= 0x01,
		TOOL_LINE_TRACKING			= 0x02,
		TOOL_DISPLAY_COORDINATE		= 0x04,
		TOOL_DISPLAY_CHANNEL_NUMBER = 0x08
	};

// Operations
public:

	CCTSAnalProDoc* GetDocument();
	void    OnToolsLinetrack(int nPlane, int nLine);
	void    OnToolsShowLinePattern(int nPlane, int nLine);
	BOOL    DrawTrackingLine();
	void	OnToolsScreencapture();
	void	SetLineWidth(int width);
	void	SetLineType(int type);
	BOOL	SetClipBoardText(CString strSource);
	void	GraphZoomDisplay();		// 210804 HKH 그래프 새로고침시 줌 비율 유지

virtual BOOL PreTranslateMessage(MSG* pMsg);	//20180710 YULEE 

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTSAnalProView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

// Implementation
public:
	void DataIndexQuery(CString strPath, CDWordArray *apIndexArray);		//lmh 20120207
	void DataQuery();														//lmh 20120207
	BOOL m_nShowLineToopTip;						//20090911 KBH
	BOOL m_bAutoReDraw;
	void DestoryMouseTraceWnd();
	void DestroyMouseTraceWndSet(); //20180710 yulee
	BOOL AddLineList();
	virtual ~CCTSAnalProView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
//	CLineListBox m_LineListBox;
	CPoint m_ZoomLeftTopPoint;
	BOOL m_bCapturedPoint;

// Generated message map functions
protected:
	//{{AFX_MSG(CCTSAnalProView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnToolZoomIn();
	afx_msg void OnUpdateToolZoomIn(CCmdUI* pCmdUI);
	afx_msg void OnToolZoomOut();
	afx_msg void OnUpdateToolZoomOut(CCmdUI* pCmdUI);
	afx_msg void OnToolZoomOff();
	afx_msg void OnUpdateToolZoomOff(CCmdUI* pCmdUI);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDataSave();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBtnGetChannel();
	afx_msg void OnUpdateBtnGetChannel(CCmdUI* pCmdUI);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnFilePrint();
	afx_msg void OnLineTracking();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDataQuery();
	afx_msg void OnLineListSelectChanged();
	afx_msg void OnLinePattern();
	afx_msg void OnLineColor();
	afx_msg void OnLineWidth1();
	afx_msg void OnLineWidth2();
	afx_msg void OnLineWidth3();
	afx_msg void OnLineWidth4();
	afx_msg void OnLineWidth5();
	afx_msg void OnLineTypeSolid();
	afx_msg void OnLineTypeDot();
	afx_msg void OnLineTypeDashdotdot();
	afx_msg void OnLineTypeDashdot();
	afx_msg void OnLineTypeDash();
	afx_msg void OnUpdateLineWidth1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineWidth2(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineWidth3(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineWidth4(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineWidth5(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineTypeSolid(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineTypeDot(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineTypeDashdotdot(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineTypeDashdot(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineTypeDash(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineColor(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLinePattern(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineTracking(CCmdUI* pCmdUI);
	afx_msg void OnReload();
	afx_msg void OnAutoRedraw();
	afx_msg void OnUpdateAutoRedraw(CCmdUI* pCmdUI);
	afx_msg void OnDataDetailView();
	afx_msg void OnMarkDataPoint();
	afx_msg void OnMarkDataPointNone();
	afx_msg void OnLineNone();
	afx_msg void OnUpdateLineNone(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMarkDataPointNone(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMarkDataPoint(CCmdUI* pCmdUI);
	afx_msg void OnAxisSetting();
	afx_msg void OnLogView();
	afx_msg void OnUpdateLogView(CCmdUI* pCmdUI);
	//lmh 20120207
	afx_msg void OnLineTooltip();
	afx_msg void OnUpdateLineTooltip(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnGeneralConfig();
};

#ifndef _DEBUG  // debug version in CTSAnalProView.cpp
inline CCTSAnalProDoc* CCTSAnalProView::GetDocument()
   { return (CCTSAnalProDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTSAnalProVIEW_H__13FE5EAE_AB5E_11D4_88DF_006008CEDA07__INCLUDED_)
