// SetNetworkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "SetNetworkDlg.h"
#include "MonitorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetNetworkDialog dialog


CSetNetworkDialog::CSetNetworkDialog(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CSetNetworkDialog::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CSetNetworkDialog::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CSetNetworkDialog::IDD3):
	(CSetNetworkDialog::IDD), pParent)
{
	//{{AFX_DATA_INIT(CSetNetworkDialog)
	//}}AFX_DATA_INIT
}


void CSetNetworkDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetNetworkDialog)
	DDX_Control(pDX, IDC_MODULE_LIST, m_ModuleListBox);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate)
	{
//		CMonitorDoc* pDoc = (CMonitorDoc*)(((CMDIChildWnd*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetActiveFrame()))->GetActiveDocument());

		// Network 경로가 바뀌었을 때
		CString strPath;
		GetDlgItem(IDC_STATIC_PATH)->GetWindowText(strPath);
		strPath = strPath.Mid(6);
//		if(pDoc->m_pUnit[m_iUnitIndex].GetPath().CompareNoCase(strPath)!=0)
//		{
//			pDoc->m_pUnit[m_iUnitIndex].SetPath(strPath);
//			pDoc->m_pUnit[m_iUnitIndex].SavePath(CCTSAnalProApp::m_strOriginFolderPath+"\\sysinfo.mdb");
//		}
//
//		// Network가 연결되거나 끊어졌을 때
//		if(   (!pDoc->m_pUnit[m_iUnitIndex].IsValidPath()&& m_bValidPath)
//			||( pDoc->m_pUnit[m_iUnitIndex].IsValidPath()&&!m_bValidPath))
//		{
//			pDoc->m_pUnit[m_iUnitIndex].SetWhetherPathIsValid(m_bValidPath);
//			pDoc->m_pUnit[m_iUnitIndex].SaveWhetherPathIsValid(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb");
//		}

		// 각 Module의 사용자 선택/잠금 mode선택에 대하여
//		BYTE UnitNo = pDoc->m_pUnit[m_iUnitIndex].GetNo();
//		CStringList ModuleList;
//		int iItem = 0;
//		for(BYTE i=0; i<CModule::m_byNumOfModules; i++)
//		{
//			if(pDoc->m_pModule[i].GetUnitNo()==UnitNo)
//			{
//				// 원래 User선택 Mode인 Module을 Disable시킨 경우,
//				if(pDoc->m_pModule[i].GetOperationFlag()&CModule::OPERATION_FLAG_USER)
//				{
//					if(m_ModuleListBox.GetCheck(iItem)!=1)
//					{
//						CString str;
//						str.Format("%d,D\n", pDoc->m_pModule[i].GetModuleNo());
//						ModuleList.AddTail(str);
//						//
//						pDoc->m_pModule[i].SetOperationFlag(CModule::OPERATION_FLAG_USER,FALSE);
//						pDoc->m_pModule[i].SaveUserSelection(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb");
//					}
//				}
//				// 원래 User잠금 Mode인 Module을 Enable시킨 경우,
//				else
//				{
//					if(m_ModuleListBox.GetCheck(iItem)==1)
//					{
//						CString str;
//						str.Format("%d,E\n", pDoc->m_pModule[i].GetModuleNo());
//						ModuleList.AddTail(str);
//						//
//						pDoc->m_pModule[i].SetOperationFlag(CModule::OPERATION_FLAG_USER,TRUE);
//						pDoc->m_pModule[i].SaveUserSelection(CCTSAnalProApp::m_strOriginFolderPath+"\\SysInfo.mdb");
//					}
//				}
//				//
//				iItem++;
//			}
//		}
//		//
//		strPath.Format("%s\\command\\modules.cmd",
//			           pDoc->m_pUnit[m_iUnitIndex].GetPath());

/*		CStdioFile afile;
		if(ModuleList.GetCount()>0)
		{
			if(afile.Open(strPath,CFile::modeCreate|CFile::modeWrite|CFile::typeText))
			{
				CString str;
				str.Format("Command=%d\n",CModule::USER_COMMAND_EN_DISABLE_MODULE);
				afile.WriteString(str);
				POSITION pos = ModuleList.GetHeadPosition();
				while(pos) afile.WriteString(ModuleList.GetNext(pos));
				afile.Close();
			}
		}
*/	}
}


BEGIN_MESSAGE_MAP(CSetNetworkDialog, CDialog)
	//{{AFX_MSG_MAP(CSetNetworkDialog)
	ON_BN_CLICKED(IDC_SET_NETWORK, OnSetNetwork)
	ON_LBN_SELCHANGE(IDC_MODULE_LIST, OnSelchangeModuleList)
	ON_BN_CLICKED(IDC_CONNECT_BTN, OnConnectBtn)
	ON_BN_CLICKED(IDC_DISCONNECT_BTN, OnDisconnectBtn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetNetworkDialog message handlers

BOOL CSetNetworkDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
//	CMonitorDoc* pDoc = (CMonitorDoc*)(((CMDIChildWnd*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetActiveFrame()))->GetActiveDocument());

	// Title
//	BYTE UnitNo = pDoc->m_pUnit[m_iUnitIndex].GetNo();
	CString str;
//	str.Format("Unit%02d", UnitNo);
	SetWindowText(str);

	// Path
//	GetDlgItem(IDC_STATIC_PATH)->SetWindowText("경로: "+pDoc->m_pUnit[m_iUnitIndex].GetPath());
//	m_bValidPath=pDoc->m_pUnit[m_iUnitIndex].IsValidPath();
	DisplayPathInfo();

	// Module ListBox
//	int iItem = 0;
//	for(BYTE i=0; i<CModule::m_byNumOfModules; i++)
//	{
//		if(pDoc->m_pModule[i].GetUnitNo()==UnitNo)
//		{
//			CString str;
//			str.Format("Module%02d", pDoc->m_pModule[i].GetModuleNo());
//			m_ModuleListBox.AddString(str);
//			if(pDoc->m_pModule[i].GetOperationFlag()&CModule::OPERATION_FLAG_USER) m_ModuleListBox.SetCheck(iItem,1);
//			iItem++;
//		}
//	}
//	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSetNetworkDialog::OnSetNetwork() 
{
	char buffer[_MAX_PATH] = {'\0'};
	BROWSEINFO browseInfo;
	browseInfo.hwndOwner = m_hWnd;
	browseInfo.pidlRoot  = NULL;
	browseInfo.lpszTitle = "Select a path of the unit.";
	browseInfo.pszDisplayName = buffer;
	browseInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	browseInfo.lpfn=NULL;
	browseInfo.lParam=0;
	LPITEMIDLIST pDirList = SHBrowseForFolder(&browseInfo);
	if(pDirList!=NULL)
	{
		SHGetPathFromIDList(pDirList, buffer);
		CString strPath(buffer);
		LPMALLOC lpMalloc;
		VERIFY(::SHGetMalloc(&lpMalloc) == NOERROR);
		lpMalloc->Free(pDirList);
		lpMalloc->Release();
		//
		strPath.MakeLower();
		if(strPath.GetAt(strPath.GetLength()-1)=='\\') strPath = strPath.Left(strPath.GetLength()-1);
		CFileFind afinder;
		BOOL bCheck = TRUE;
		if     (!afinder.FindFile(strPath+"\\command")) bCheck = FALSE;
		else if(!afinder.FindFile(strPath+"\\data"))    bCheck = FALSE;
		else if(!afinder.FindFile(strPath+"\\status"))  bCheck = FALSE;
		if(bCheck)
		{
			GetDlgItem(IDC_STATIC_PATH)->SetWindowText(Fun_FindMsg("OnSetNetwork_msg","IDD_SET_NETWORK_DIALOG")+strPath);
			//@ GetDlgItem(IDC_STATIC_PATH)->SetWindowText("경로: "+strPath);
			m_bValidPath = TRUE;
			DisplayPathInfo();
		}
		else MessageBox("You select wrong path.", "BTS", MB_ICONINFORMATION);
	}
}

void CSetNetworkDialog::OnSelchangeModuleList() 
{
//	CMonitorDoc* pDoc = (CMonitorDoc*)(((CMDIChildWnd*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetActiveFrame()))->GetActiveDocument());
//	BYTE UnitNo = pDoc->m_pUnit[m_iUnitIndex].GetNo();
//	//
//	int iItem = 0;
//	for(BYTE i=0; i<CModule::m_byNumOfModules; i++)
//	{
//		if(pDoc->m_pModule[i].GetUnitNo()==UnitNo)
//		{
//			// 원래 User선택 Mode인 Module을
//			if(pDoc->m_pModule[i].GetOperationFlag()&CModule::OPERATION_FLAG_USER)
//			{
//				// Disable 시킨 경우,
//				if(m_ModuleListBox.GetCheck(iItem)!=1)
//				{
//					if(pDoc->m_pModule[i].IsThereAnyChOnTest())
//					{
//						MessageBox("Can't disable this module", "BTS", MB_ICONINFORMATION);
//						m_ModuleListBox.SetCheck(iItem, 1);
//						break;
//					}
//				}
//			}
//			//
//			iItem++;
//			Sleep(100);
//		}
//	}
}

void CSetNetworkDialog::OnOK() 
{

	CDialog::OnOK();
}

void CSetNetworkDialog::OnConnectBtn() 
{
	//
	if(!m_bValidPath)
	{
//		CMonitorDoc* pDoc = (CMonitorDoc*)(((CMDIChildWnd*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetActiveFrame()))->GetActiveDocument());
//		CFileFind afinder;
//		m_bValidPath = afinder.FindFile(pDoc->m_pUnit[m_iUnitIndex].GetPath()+"\\*.*");
		DisplayPathInfo();
	}
}

void CSetNetworkDialog::OnDisconnectBtn() 
{
	//
	if(m_bValidPath)
	{
		m_bValidPath= FALSE;
		DisplayPathInfo();
	}
}

void CSetNetworkDialog::DisplayPathInfo() 
{
	// Disk free space
	if(m_bValidPath)
	{
		ULARGE_INTEGER A;
		ULARGE_INTEGER B;
		ULARGE_INTEGER C;
		//
		CString strPath;
		GetDlgItem(IDC_STATIC_PATH)->GetWindowText(strPath);
		strPath = strPath.Mid(6);
		GetDiskFreeSpaceEx(strPath,&A,&B,&C); // 유니트 경로의 남은 디스크용량
		//
		CString str;
		str.Format(Fun_FindMsg("DisplayPathInfo_msg1","IDD_SET_NETWORK_DIALOG"),C.QuadPart/1024L/1024L);
		//@ str.Format("디스크 크기: %d MB",C.QuadPart/1024L/1024L);
		GetDlgItem(IDC_STATIC_DISK)->SetWindowText(str);
	}
	else GetDlgItem(IDC_STATIC_DISK)->SetWindowText("Not available");

	// Network state
	if(m_bValidPath) GetDlgItem(IDC_STATIC_NET_STATE)->SetWindowText(Fun_FindMsg("DisplayPathInfo_msg2","IDD_SET_NETWORK_DIALOG"));
	//@ if(m_bValidPath) GetDlgItem(IDC_STATIC_NET_STATE)->SetWindowText("네트워크 상태: Connected");
	else             GetDlgItem(IDC_STATIC_NET_STATE)->SetWindowText(Fun_FindMsg("DisplayPathInfo_msg3","IDD_SET_NETWORK_DIALOG"));
	//@ else             GetDlgItem(IDC_STATIC_NET_STATE)->SetWindowText("네트워크 상태: Not Connected");
}