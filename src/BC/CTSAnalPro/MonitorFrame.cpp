// MonitorFrame.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "MonitorFrame.h"
#include "MainFrm.h"
#include "MonitorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMonitorFrame

IMPLEMENT_DYNCREATE(CMonitorFrame, CMDIChildWnd)

CMonitorFrame::CMonitorFrame()
{
}

CMonitorFrame::~CMonitorFrame()
{
}


BEGIN_MESSAGE_MAP(CMonitorFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMonitorFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(UM_CHECK_BACKUP_FILE, OnCheckBackUpFile)
	ON_MESSAGE(WM_COMPLETE_CYCLE, OnCompleteOneCycle)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CMonitorFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMonitorFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG
/////////////////////////////////////////////////////////////////////////////
// CMonitorFrame message handlers

BOOL CMonitorFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;
	cs.style &= ~FWS_ADDTOTITLE;
//	cs.lpszName = "Channel Monitoring";
	cs.cx = 790;
	cs.cy = 500;
	
	return TRUE;
}

void CMonitorFrame::OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt)
{
	if(bActive)             ((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::MONITOR_TOOLBAR);
	if(pActivateWnd==NULL) 	((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::MAIN_TOOLBAR);

	CMDIChildWnd::OnUpdateFrameMenu(bActive, pActivateWnd, hMenuAlt);
}

int CMonitorFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	int nSelLanguage, nIDSel;//&&
	nSelLanguage = AfxGetApp()->GetProfileInt("Config","Language",1);

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	//if (!m_wndDialogBar.Create(this,
	//	                       IDD_SEL_MODULE_DIALOGBAR,
	//	                       CBRS_TOP,
	//						   IDD_SEL_MODULE_DIALOGBAR))

	if (!m_wndDialogBar.Create(this, //&&
		nSelLanguage == 1?(IDD_SEL_MODULE_DIALOGBAR):
		nSelLanguage == 2?(IDD_SEL_MODULE_DIALOGBAR_ENG):
		nSelLanguage == 3?(IDD_SEL_MODULE_DIALOGBAR_PL):
								   IDD_SEL_MODULE_DIALOGBAR,
		                       CBRS_TOP,
		nSelLanguage == 1?(IDD_SEL_MODULE_DIALOGBAR):
		nSelLanguage == 2?(IDD_SEL_MODULE_DIALOGBAR_ENG):
		nSelLanguage == 3?(IDD_SEL_MODULE_DIALOGBAR_PL):
							   IDD_SEL_MODULE_DIALOGBAR))
	{
		TRACE0("Failed to create dialog bar m_wndDialogBar\n");
		return -1;		// fail to create
	}

	m_wndDialogBar.EnableDocking(CBRS_ALIGN_TOP);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndDialogBar);
	
	return 0;
}

void CMonitorFrame::OnClose() 
{
	((CMonitorDoc*)GetActiveDocument())->EndMonitor();
	CMDIChildWnd::OnClose();
}

LRESULT CMonitorFrame::OnCheckBackUpFile(WPARAM wParam, LPARAM lParam)
{
	((CMonitorDoc*)GetActiveDocument())->MonitorModules();
	//
	return 0UL;
}

LRESULT CMonitorFrame::OnCompleteOneCycle(WPARAM wParam, LPARAM lParam)
{
//	BYTE byMode = (BYTE)wParam;
//	bool bMode = (byMode == 0x04) ? false : true;
//	S_RESULT_BAK_P pResultData = (S_RESULT_BAK_P)lParam;
//	((CMonitorDoc*)GetActiveDocument())->BringCycleDataIntoDataPC(bMode, pResultData);
	return 0;
}
