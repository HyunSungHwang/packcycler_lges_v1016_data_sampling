// LogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "LogDlg.h"
#include "PasswordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogDialog dialog


CLogDialog::CLogDialog(CWnd* pParent /*=NULL*/)
	: CDialog(
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 1?(CLogDialog::IDD):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 2?(CLogDialog::IDD2):
	(AfxGetApp()->GetProfileInt("Config","Language",1)) == 3?(CLogDialog::IDD3):
	(CLogDialog::IDD), pParent)
{
	//{{AFX_DATA_INIT(CLogDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bSkip = FALSE;
}


void CLogDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogDialog)
//	DDX_Control(pDX, IDOK, m_EnterBtn);
//	DDX_Control(pDX, IDCANCEL, m_CancelBtn);
//	DDX_Control(pDX, IDC_CHANGE_BTN, m_ChangeSecurBtn);
	//}}AFX_DATA_MAP
	if(pDX->m_bSaveAndValidate)
	{
		//
		CString str;
		GetDlgItem(IDC_EDIT_ID)->GetWindowText(str);
		if(str.Compare(m_strID)!=0)
		{
// MessageBox의 Comment 변경
			MessageBox(Fun_FindMsg("DoDataExchange_msg1","IDD_LOG_DLG"), "Error", MB_ICONEXCLAMATION);
			//@ MessageBox("ID가 맞지 않습니다.", "Error", MB_ICONEXCLAMATION);
			//AfxMessageBox("Wrong User ID !");
			pDX->Fail();
		}
		GetDlgItem(IDC_EDIT_PASSWORD)->GetWindowText(str);
		if(str.Compare(m_strPassword)!=0)
		{
// MessageBox의 Comment 변경
			MessageBox(Fun_FindMsg("DoDataExchange_msg2","IDD_LOG_DLG"), "Error", MB_ICONEXCLAMATION);
			//@ MessageBox("Password가 맞지 않습니다.", "Error", MB_ICONEXCLAMATION);
			//AfxMessageBox("Wrong Password !");
			pDX->Fail();
		}

		// User가 "Save password" Check를 다르게 한 경우...
		if(   m_bSkip&&((CButton*)GetDlgItem(IDC_CHECK_SKIP))->GetCheck()!=1
		   ||!m_bSkip&&((CButton*)GetDlgItem(IDC_CHECK_SKIP))->GetCheck()==1)
		{
			m_bSkip = !m_bSkip;
			//
			CDaoDatabase aDB;
			
// 			switch(nLanguage) 20190701
// 			{
// 			case 1: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME); break;
// 			case 2: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN); break;
// 			case 3: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_PL); break;
// 			default : aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN); break;
// 			}

			aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME);
				CDaoRecordset aRec(&aDB);
				aRec.Open(dbOpenTable, "User");
					COleVariant varValue;
					varValue.vt=VT_BOOL;
					varValue.boolVal = m_bSkip;
					aRec.Edit();
					aRec.SetFieldValue("AutoLogOut",varValue);
					aRec.Update();
				aRec.Close();
			aDB.Close();
		}
	}
}


BEGIN_MESSAGE_MAP(CLogDialog, CDialog)
	//{{AFX_MSG_MAP(CLogDialog)
	ON_WM_CANCELMODE()
	ON_BN_CLICKED(IDC_CHANGE_BTN, OnChangeBtn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogDialog message handlers

BOOL CLogDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//
//	m_ChangeSecurBtn.LoadBitmaps(IDB_SECUR_UP,IDB_SECUR_DOWN,IDB_SECUR_FOCUS);
//	m_CancelBtn.LoadBitmaps(IDB_CANCEL_UP,IDB_CANCEL_DOWN,IDB_CANCEL_FOCUS);
//	m_EnterBtn.LoadBitmaps(IDB_ENTER_UP,IDB_ENTER_DOWN,IDB_ENTER_FOCUS);
	//m_EnterBtn.LoadBitmaps(IDB_ENTER_UP,IDB_ENTER_DOWN,IDB_ENTER_FOCUS);
	
	// TODO: Add extra initialization here
//	SetBitmap(IDB_LOG,CBitmapDialog::BITMAP_STRETCH);
	
	//
	CDaoDatabase aDB;

// 	switch(nLanguage) 20190701
// 	{
// 	case 1: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME); break;
// 	case 2: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN); break;
// 	case 3: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_PL); break;
// 	default : aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN); break;
// 	}
	
	aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME);
		CDaoRecordset aRec(&aDB);
		aRec.Open(dbOpenTable, "User");
			COleVariant varValue;
			//
			aRec.GetFieldValue("UserID",varValue);
			if(varValue.vt!=VT_NULL) m_strID = (LPCTSTR)(LPTSTR)varValue.bstrVal;
			varValue.Clear();
			//
			aRec.GetFieldValue("Password",varValue);
			if(varValue.vt!=VT_NULL) m_strPassword = (LPCTSTR)(LPTSTR)varValue.bstrVal;
			varValue.Clear();
			//
			aRec.GetFieldValue("AutoLogOut",varValue);
			m_bSkip=varValue.boolVal;
			//
			GetDlgItem(IDC_EDIT_ID)->SetWindowText(m_strID);
			if(m_bSkip)
			{
				GetDlgItem(IDC_EDIT_PASSWORD)->SetWindowText(m_strPassword);
				((CButton*)GetDlgItem(IDC_CHECK_SKIP))->SetCheck(1);
			}
		aRec.Close();
	aDB.Close();
	//
	((CEdit*)GetDlgItem(IDC_EDIT_ID))->SetSel(0,0);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLogDialog::OnCancelMode() 
{
	CDialog::OnCancelMode();
}

void CLogDialog::OnChangeBtn() 
{
		// 기존의 ID와 Password 입력이 잘 되었는지 Check
		CString str;
		GetDlgItem(IDC_EDIT_ID)->GetWindowText(str);
		if(str.Compare(m_strID)!=0)
		{
			MessageBox(Fun_FindMsg("OnChangeBtn_msg1","IDD_LOG_DLG"), "CTS", MB_ICONINFORMATION);
			//@ MessageBox("ID가 맞지 않습니다.", "CTS", MB_ICONINFORMATION);
			return;
		}
		GetDlgItem(IDC_EDIT_PASSWORD)->GetWindowText(str);
		if(str.Compare(m_strPassword)!=0)
		{
			MessageBox(Fun_FindMsg("OnChangeBtn_msg2","IDD_LOG_DLG"), "CTS", MB_ICONINFORMATION);
			//@ MessageBox("Password가 맞지 않습니다.", "CTS", MB_ICONINFORMATION);
			return;
		}

		//
		CPasswordDialog aDlg;
		aDlg.m_strID = m_strID;
		if(IDOK==aDlg.DoModal())
		{
			//
			m_strID       = aDlg.m_strID;
			m_strPassword = aDlg.m_strPassword1;
			//
			GetDlgItem(IDC_EDIT_ID)->SetWindowText(m_strID);
			GetDlgItem(IDC_EDIT_PASSWORD)->SetWindowText(m_strPassword);
			//
			CDaoDatabase aDB;
			
// 			switch(nLanguage) 20190701
// 			{
// 			case 1: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME); break;
// 			case 2: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN); break;
// 			case 3: aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_PL); break;
// 			default : aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME_EN); break;
// 			}

			aDB.Open(CCTSAnalProApp::m_strOriginFolderPath+"\\DataBase\\"+PS_SCHEDULE_DATABASE_NAME);
				CDaoRecordset aRec(&aDB);
				aRec.Open(dbOpenTable, "User");
				aRec.Edit();
					//
					COleVariant varValue;
					varValue.SetString(m_strID, VT_BSTRT);
					aRec.SetFieldValue("UserID",varValue);
					//
					varValue.Clear();
					varValue.SetString(m_strPassword, VT_BSTRT);
					aRec.SetFieldValue("Password",varValue);
				aRec.Update();
				aRec.Close();
			aDB.Close();
		}
}
