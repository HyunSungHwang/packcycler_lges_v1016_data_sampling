#if !defined(AFX_WORKSTSIGNANALDLG_H__3E0709D4_10A8_458C_AFBA_A1BB23BA0A07__INCLUDED_)
#define AFX_WORKSTSIGNANALDLG_H__3E0709D4_10A8_458C_AFBA_A1BB23BA0A07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WorkSTSignAnalDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWorkSTSignAnalDlg dialog
#include "resource.h"
class CWorkSTSignAnalDlg : public CDialog
{
// Construction
public:
	CWorkSTSignAnalDlg(CWnd* pParent = NULL);   // standard constructor
	CLabel	m_CntrlPlaceSign;
// Dialog Data
	//{{AFX_DATA(CWorkSTSignAnalDlg)
	enum { IDD = IDD_WORKSTSIGNANAL_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	void SetWinPos (CRect rect);
	void CWorkSTSignAnalDlg::InitControlLbl();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkSTSignAnalDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
  	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWorkSTSignAnalDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKSTSIGNANALDLG_H__3E0709D4_10A8_458C_AFBA_A1BB23BA0A07__INCLUDED_)
