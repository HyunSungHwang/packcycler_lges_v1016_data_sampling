// HelpFrame.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "HelpFrame.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHelpFrame

IMPLEMENT_DYNCREATE(CHelpFrame, CMDIChildWnd)

CHelpFrame::CHelpFrame()
{
}

CHelpFrame::~CHelpFrame()
{
}


BEGIN_MESSAGE_MAP(CHelpFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CHelpFrame)
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CHelpFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CHelpFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}
#endif //_DEBUG
/////////////////////////////////////////////////////////////////////////////
// CHelpFrame message handlers
BOOL CHelpFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;
	cs.style &= ~FWS_ADDTOTITLE;
	//cs.lpszName = "온라인 메뉴얼";
	cs.lpszName = Fun_FindMsg("HelpFrame_PreCreateWindow_msg1","HELPFRAME");//&&
	cs.cx = 760;
	cs.cy=500;
	return TRUE;
}

void CHelpFrame::OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt)
{
	if(bActive)             ((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::HELP_TOOLBAR);
	if(pActivateWnd==NULL) 	((CMainFrame*)GetParentFrame())->LoadToolBar(CMainFrame::MAIN_TOOLBAR);

	CMDIChildWnd::OnUpdateFrameMenu(bActive, pActivateWnd, hMenuAlt);
}