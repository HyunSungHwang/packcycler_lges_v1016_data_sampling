/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: SelMdlDlgBar.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CSelModuleDialogBar class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_SELMDLDLGBAR_H__15B4FE5B_5993_4E07_ADB2_F6254535B2BA__INCLUDED_)
#define AFX_SELMDLDLGBAR_H__15B4FE5B_5993_4E07_ADB2_F6254535B2BA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelMdlDlgBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelModuleDialogBar window

// #include

class CMonitorDoc;

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CSelModuleDialogBar
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CSelModuleDialogBar : public CDialogBar
{
// Construction
public:
	CSelModuleDialogBar();

// Attributes
public:
	CMonitorDoc* m_pDocument;
	BYTE         m_byCheckState;
	int          m_iCurSelUnit;
	int          m_iCurSelModule;

// Dialog Data
	//{{AFX_DATA(CSelModuleDialogBar)
	enum { IDD = IDD_SEL_MODULE_DIALOGBAR , IDD2 = IDD_SEL_MODULE_DIALOGBAR_ENG, IDD3 = IDD_SEL_MODULE_DIALOGBAR_PL  };
	CComboBox	m_UnitCombo;
	CComboBox	m_ModuleCombo;
	//}}AFX_DATA

// Operations
public:
	void InitialSet(CMonitorDoc* pDoc);
	void UpdateListView();
	void SetCheckState(BYTE byCheckState);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelModuleDialogBar)
	public:
	virtual BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID );
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSelModuleDialogBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSelModuleDialogBar)
	afx_msg void OnStatusFinishCheck();
	afx_msg void OnStatusErrorCheck();
	afx_msg void OnStatusInitialCheck();
	afx_msg void OnStatusPauseCheck();
	afx_msg void OnStatusRunCheck();
	afx_msg void OnSelchangeUnitCombo();
	afx_msg void OnSelStatusAll();
public:
	afx_msg void OnSelchangeModuleCombo();
	//}}AFX_MSG
	afx_msg void OnUpdateSelStatusAll(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

//
public:
	void SetStatusMsg(CString strMsg, BYTE nUnitNo);
	void SetProgressMsg(CString strMsg);
	enum{ STATE_CHECK_NONE     = 0x00,
		  STATE_CHECK_INITIAL  = 0x01,
		  STATE_CHECK_RUN      = 0x02,
		  STATE_CHECK_ERROR    = 0x04,
		  STATE_CHECK_PAUSE    = 0x08,
		  STATE_CHECK_FINISH   = 0x10
		};
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELMDLDLGBAR_H__15B4FE5B_5993_4E07_ADB2_F6254535B2BA__INCLUDED_)
