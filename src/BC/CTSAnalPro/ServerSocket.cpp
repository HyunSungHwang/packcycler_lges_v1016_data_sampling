// ServerSocket.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "ServerSocket.h"
#include "CommandSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServerSocket

CServerSocket::CServerSocket(CMonitorDoc* pDoc)
: m_pDoc(pDoc)
{
}

CServerSocket::~CServerSocket()
{
//	Close();
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CServerSocket, CSocket)
	//{{AFX_MSG_MAP(CServerSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CServerSocket member functions

void CServerSocket::OnAccept(int nErrorCode) 
{
	CCommandSocket* pNewClient = new CCommandSocket;
	pNewClient->m_pDoc = m_pDoc;

	Accept(*pNewClient);
	
	{// Verifying connect from Same Address
		CString strIPAddress, strPrevIPAddress;UINT nPort;
		pNewClient->GetPeerName(strIPAddress, nPort);
		CCommandSocket* pPrevClient = NULL;
		for( int nI = 0; nI < m_pDoc->m_aCmdSocket.GetSize(); nI++ )
		{
			pPrevClient = (CCommandSocket*)m_pDoc->m_aCmdSocket.GetAt(nI);
			pPrevClient->GetPeerName(strPrevIPAddress, nPort);
			if( strIPAddress.Compare(strPrevIPAddress) == 0 )
			{
				m_pDoc->m_aCmdSocket.RemoveAt(nI);
				delete pPrevClient;
				break;
			}
		}
	}

	m_pDoc->m_aCmdSocket.Add(pNewClient);
	
	CSocket::OnAccept(nErrorCode);
}

BOOL CServerSocket::CreateSocket()
{
	if( Create(MONITORING_BASIC_PORT) == FALSE )
		return FALSE;
	
	Listen();
	return TRUE;
}
