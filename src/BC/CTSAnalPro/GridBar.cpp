// gridbar.cpp : implementation file
//

#include "stdafx.h"
#include "gridbar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGridBar

CGridBar::CGridBar()
{
}

CGridBar::~CGridBar()
{
}


BEGIN_MESSAGE_MAP(CGridBar, baseCMyBar)
	//{{AFX_MSG_MAP(CGridBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CGridBar message handlers

int CGridBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (baseCMyBar::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetSCBStyle(GetSCBStyle() | SCBS_SIZECHILD);

	if (!m_GridWnd.Create(WS_CHILD|WS_VISIBLE|ES_AUTOVSCROLL,
		CRect(0,0,0,0), this, 123))
		return -1;

	m_GridWnd.ModifyStyleEx(0, WS_EX_CLIENTEDGE);
	m_GridWnd.SetScrollBarMode(SB_BOTH, gxnEnabled);

	m_GridWnd.Initialize();

	// older versions of Windows* (NT 3.51 for instance)
	// fail with DEFAULT_GUI_FONT
	if (!m_font.CreateStockObject(DEFAULT_GUI_FONT))
		if (!m_font.CreatePointFont(80, "MS Sans Serif"))
			return -1;

	m_GridWnd.SetFont(&m_font);
	m_GridWnd.GetParam()->EnableUndo(FALSE);
	return 0;
}

CWnd * CGridBar::GetClientWnd()
{
	return &m_GridWnd;
}

void CGridBar::SelectDataRange(int nStartIndex, int nEndIndex, int nColumn)
{
	int nRowCount = m_GridWnd.GetRowCount();

	if(nStartIndex < 0)			nStartIndex	 = 0;
	if(nEndIndex >= nRowCount)	nEndIndex = nRowCount-1;

	m_GridWnd.SelectRange(CGXRange().SetTable(), FALSE);
	if(nStartIndex < nEndIndex)
		m_GridWnd.SelectRange(CGXRange().SetCells(nStartIndex+1, 1, nEndIndex+1, m_GridWnd.GetColCount()));
	else 
		return;

	//Movie Current Cell in view
	m_GridWnd.ScrollCellInView(nStartIndex+1, 1);
}

void CGridBar::DisplaySheetData(CData *pData)
{

	ASSERT(pData);

	int pcount = pData->GetPlaneCount();
	if (pcount < 1)
	{
		return;
	}
	
	CPlane *pPlane;
	CLine *pLine, *pMaxLine = NULL;
	fltPoint fltData;
	int nTotRow = 0;
	int nLineCnt = 0;
	BOOL bXAixsCycle = FALSE;

	CStringList listChName;
	//모든 Pane에 라인수가 같은지 확인 
	for (int i = 0; i < pcount; i++)
	{
		pPlane = pData->GetPlaneAt(i);
		if (pPlane)
		{
			nLineCnt = pPlane->GetLineCount();
			POSITION linePos = pPlane->GetLineHead();
			while (linePos)
			{
				//라인중에서 최대 data수를 구한다.
				pLine = pPlane->GetNextLine(linePos);
				if (pLine->GetDataCount() > nTotRow)
				{
					nTotRow = pLine->GetDataCount();
					pMaxLine = pLine;
				}
				
				//그래프된 CH의 List를 구한다.
#ifdef _DEBUG
	TRACE("pLine->GetName() = %s\n", pLine->GetName()) ;
#endif
				// #define FILE_FIND_FORMAT	"M??Ch??*"

				CString strListName = pLine->GetName() ;
				strListName = strListName.Left(12) ;
				if (listChName.Find(strListName) == NULL)
				{
					listChName.AddTail(strListName);
				}

// 				if (listChName.Find(pLine->GetName()) == NULL)
// 				{
// 					listChName.AddTail(pLine->GetName());
// 				}
			}
		}
	}

	// 같으면, Main Frame의 상태바에 Progress바를 생성하여 데이터 loading상황을 보여줄 준비를 한다.
	CStatusBar* pStatusBar = (CStatusBar*)(((CMDIFrameWnd*)AfxGetMainWnd())->GetDlgItem(AFX_IDW_STATUS_BAR));
	CProgressCtrl ProgressCtrl;
	CRect rect;
	pStatusBar->GetItemRect(0,rect);
	rect.left = rect.Width()/2;
	ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,pStatusBar,777);
	ProgressCtrl.SetRange(0, 100);
	int nPos = 0;
	int nProgressCnt = 0;
	nProgressCnt = pcount*nLineCnt*listChName.GetCount();
	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("sheet loading...");

//	DWORD dwStart = GetTickCount();
	// 2014.03.20 224
/*
	//default grid setting
	if (nTotRow >= 0x10000)
	{
		AfxMessageBox("Sheet에 Data수가 많아 모두 표시하지 못하였습니다.");
		nTotRow = 0x10000-1;		//Max row수 제한 
	}
*/
	m_GridWnd.SetRowCount(nTotRow);
	m_GridWnd.SetColCount(0);
	m_GridWnd.SetRowHeight(0, 0, 30);			//20091024 KHS

	CString strTemp, strPath;
	CAxis *pXAxis, *pYAxis;
	POSITION dataPos;
	int nRow = 1;
	int nCol = 1;
	int nXColCnt = 0;
	char szYBuff[512];	//속도 향상을 위해 CString안쓰고 배열 사용 
	char szXBuff[256];	//속도 향상을 위해 CString안쓰고 배열 사용 

	//Ch 순서로 Short함 
	POSITION chPos = listChName.GetHeadPosition();
	BOOL bFistColumnData = TRUE;
	nCol = m_GridWnd.GetColCount()+1;
	CString strAxisName;
	strAxisName.Empty();

	while(chPos)
	{
		CString strCh = listChName.GetNext(chPos);
		int nCount = strCh.GetLength();
//		TRACE("%d",nCount);


//		if (nCount > 20 )
		if (nCount > 40 )
		{
			ASSERT(FALSE) ;
			//break;
			continue;	//20130215 add
		}
		
		
		bFistColumnData = TRUE;

		//Plan 검색
		INT nBeginCol = -1 ;
		for (int i = 0; i < pcount; i++)
		{
			pPlane = pData->GetPlaneAt(i);
			if (strAxisName != pPlane->GetYAxis()->GetTitle()) 
			{
				strAxisName = pPlane->GetYAxis()->GetTitle();
			}
			else
			{
				continue;
			}

			if (pPlane)
			{
		 		pXAxis = pPlane->GetXAxis();
		 		pYAxis = pPlane->GetYAxis();

				//각 Plane의 Line 검색 
				POSITION linePos = pPlane->GetLineHead();
				INT nCnt = pPlane->GetLineCount() ;

				// 영점시작인 경우 x축 data 가 모두 표시 안되는 문제 해결. (224)
				//while (linePos && nCol <255)
				while (linePos && nCol <1024) //ljb 20180921 255 -> 1024 Aux데이터 표시시 253개 이상 표시 하기 위해 변경 //DAQ512 수정 포인트
				{
					pLine = pPlane->GetNextLine(linePos);
					TRACE("Y Axis name = %s \n",pPlane->GetYAxis()->GetTitle());
					if (pLine->GetName().Left(12) == strCh.Left(12) || pLine->GetName() == strCh.Left(12))		//20100618 KHS
					{
// 						if (strAxisName == pLine->GetAxisName())
// 							continue;

						//해당 ch을 처음 Dsiplay시 X축 Data를 Display한다.
						if (bFistColumnData && bXAixsCycle == FALSE)
						{
							pMaxLine = pLine;
							bFistColumnData = FALSE;
							sprintf(szXBuff, "%s_%s(%s)", pLine->GetName(), pXAxis->GetPropertyTitle().Left(4), pXAxis->GetUnitNotation()); 
						
							//x축이 cycle이면 제일 앞에 1개의 x축만 Diaplay(최대 record line의 X Data)
							if (pXAxis->GetPropertyTitle() == "Cycle")
							{
								bXAixsCycle = TRUE;
								sprintf(szXBuff, "%s(%s)", pXAxis->GetPropertyTitle().Left(3), pXAxis->GetUnitNotation()); 
							}

							//x축 Data Display
							nBeginCol = nCol ;	// save begining column
							m_GridWnd.InsertCols(nCol, 1);	
							m_GridWnd.SetValueRange(CGXRange(0 , nCol), szXBuff);
							m_GridWnd.SetStyleRange(CGXRange().SetCols(nCol), CGXStyle().SetInterior(RGB(192, 192, 255)));
							
							//display Line data
		 					dataPos = pMaxLine->GetDataHead();
							nRow = 1;
		 					while (dataPos)
		 					{
		 						fltData = pLine->GetNextData(dataPos);
		 						m_GridWnd.SetValueRange(CGXRange(nRow++ ,	nCol), pXAxis->ConvertUnit(fltData.x));
		 					}

							nCol++;
							nXColCnt++;
						}
						
						//Set y data column
						m_GridWnd.InsertCols(nCol, 1);
						if(pYAxis->GetPropertyTitle().Left(3).Find("CAN")>-1) //yulee 20180729
						{//yulee 20180831
							sprintf(szYBuff, "%s", pLine->GetName());//yulee 20180831-2
							//sprintf(szYBuff, "%s_%s(%s)", pLine->GetName(), pYAxis->GetPropertyTitle().Left(3), pYAxis->GetPropertyTitle().Right(2));
						}
						else
							sprintf(szYBuff, "%s_%s(%s)", pLine->GetName(), pYAxis->GetPropertyTitle().Left(3), pYAxis->GetUnitNotation()); 
						
						m_GridWnd.SetValueRange(CGXRange(0 , nCol), szYBuff);
						CString sHeadName;					//2014.09.18 컬럼 해더값
						sHeadName.Format("%s",szYBuff);		//2014.09.18 컬럼 해더값
						//TRACE("HeadName = %s \n",szYBuff);
						//display Line data
						dataPos = pLine->GetDataHead();
						nRow = 1;
						//////////////////////////////////////////////////////////////////////////
						// + 224 2014.03.20
						//while (dataPos && nRow < 0x10000)
						while(dataPos)
						// -
						//////////////////////////////////////////////////////////////////////////
						{
							fltData = pLine->GetNextData(dataPos);
							
							// Added by 224 (2014/02/13) : x 축 값 설정 - 최대값이 마지막에 설정된다.
							ASSERT(nBeginCol > 0) ;

							//2014.09.18 컬럼 해더값이 AxuT 경우에 값 입력을 ConvertUnit 하지않고 그냥 출력.
						//	if(sHeadName.Find("AxuT")){
							if(sHeadName.Find("AuxT") > -1){ //yulee 20180729 버그 수정
								m_GridWnd.SetValueRange(CGXRange(nRow, nBeginCol), fltData.x); //2014.09.18 온도값 소수점으로 표시되는부분 수정 
								m_GridWnd.SetValueRange(CGXRange(nRow++, nCol), fltData.y);	   //2014.09.18 온도값 소수점으로 표시되는부분 수정 								
							}else{
								m_GridWnd.SetValueRange(CGXRange(nRow, nBeginCol), pXAxis->ConvertUnit(fltData.x));
								m_GridWnd.SetValueRange(CGXRange(nRow++, nCol), pYAxis->ConvertUnit(fltData.y));
							}																												
						}
						nCol++;
					}
					
					ProgressCtrl.SetPos(int(nPos++/(float)(nProgressCnt)*100.0f));
				}
			}
		}	
	}
	
	SelectDataRange(0, nTotRow-1);
	((CMDIFrameWnd*)AfxGetMainWnd())->SetMessageText("");
}

void CGridBar::OnSize(UINT nType, int cx, int cy) 
{
	baseCMyBar::OnSize(nType, cx, cy);
}
