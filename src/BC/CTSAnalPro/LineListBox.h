/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: LineListBox.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CLineListBox class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_LINELISTBOX_H__90BB9AC3_1411_4284_B4BD_6DF2237A8DAB__INCLUDED_)
#define AFX_LINELISTBOX_H__90BB9AC3_1411_4284_B4BD_6DF2237A8DAB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// LineListBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLineListBox window

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CLineListBox
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CLineListBox : public CListBox
{
	DECLARE_DYNAMIC(CLineListBox)

// Construction
public:
	CLineListBox();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLineListBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLineListBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(CLineListBox)
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINELISTBOX_H__90BB9AC3_1411_4284_B4BD_6DF2237A8DAB__INCLUDED_)
