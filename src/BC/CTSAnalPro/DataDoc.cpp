// DataDoc.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "DataDoc.h"

#include "MainFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataDoc

IMPLEMENT_DYNCREATE(CDataDoc, CDocument)

CDataDoc::CDataDoc()
{
//	m_strDataPath = AfxGetApp()->GetProfileString("Control", "ResultDataPath", NULL);
}

BOOL CDataDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

//	m_strDataPath = AfxGetApp()->GetProfileString("Control", "ResultDataPath", NULL);
//	m_Data.QueryData(m_strDataPath+"?");

	if (SelectTestDlg() == FALSE)
	{
		return FALSE;
	}

	return TRUE;
}

CDataDoc::~CDataDoc()
{
}


BEGIN_MESSAGE_MAP(CDataDoc, CDocument)
	//{{AFX_MSG_MAP(CDataDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataDoc diagnostics

#ifdef _DEBUG
void CDataDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDataDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDataDoc serialization

void CDataDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDataDoc commands

CStringList * CDataDoc::GetSelFileList()
{
	return &m_strFileList;
}


void CDataDoc::SetTestList(CStringList *pstrList)
{
	ASSERT(pstrList);

	m_strFileList.RemoveAll();

	CString strFileName;
	POSITION pos = pstrList->GetHeadPosition();
	while(pos)
	{	
		strFileName = pstrList->GetNext(pos);
		m_strFileList.AddTail(strFileName);
	}
}

BOOL CDataDoc::GetCellLastState(CString strChPath)
{
	return m_ChData.IsOKCell();
}

//선택 파일을 Load한다.
BOOL CDataDoc::SetChPath(CString strChPath)
{
	m_ChData.SetPath(strChPath);
	// Commented by 224 (2014/02/13) :
	// m_ChData가 초기화가 안된 상태에서 로딩 (DataDoc의 멤버변수)으로 인해
	// From, To 구간 설정 안됨.
	return TRUE ;
	return m_ChData.Create();
}

CChData * CDataDoc::GetChData()
{
	return &m_ChData;
}

BOOL CDataDoc::SelectTestDlg()
{
	CResultTestSelDlg	dlg;

	dlg.SetTestList(GetSelFileList());
	if( dlg.DoModal() == IDCANCEL)		return FALSE;
	
//	SetDataPath(dlg.m_strFileFolder);	
	SetTestList(dlg.GetSelList());

	return TRUE;
}
BOOL CDataDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
//	if (!CDocument::OnOpenDocument(lpszPathName))
//		return FALSE;
	
	// TODO: Add your specialized creation code here
	CStringList selDataFolerList;
	CString str;
	str = ((CMainFrame *)AfxGetMainWnd())->m_strCopyMsgString;

	
	if(!str.IsEmpty())
	{
		//여러 폴더명 분리
		CStringList selFileList;
		CString strPath;
		int p1=0,p2=0;
		while(TRUE)
		{
			// '\n'로 분리된 각 folder의 Path를 얻어서
			p2 = str.Find('\n',p1);
			if(p2 < 0) break;
			strPath = str.Mid(p1,p2-p1);
			p1=p2+1;

			selFileList.AddTail(strPath);
		}

		//CString strChName;
		BOOL bSameFlag = FALSE;
		POSITION pos, pos2;
		//각 이름을 Foler와 Channel 번호로 분리한다.
		for(int i =0; i<selFileList.GetCount(); i++)
		{
			pos = selFileList.GetHeadPosition();
			while(pos)
			{
				strPath = selFileList.GetNext(pos);
				//foler와 채널을 분리
				{
					//strPath => Full Path Name\\M01C02 형태
					str = strPath.Left(strPath.ReverseFind('\\'));				//Full Path Name
					//strChName = strPath.Mid(strPath.ReverseFind('\\')+1);		//M01C02
					
					//선택 파일의 폴더 리스트에서 동일 폴더가 이미 있는지 확인
					pos2 = selDataFolerList.GetHeadPosition();
					bSameFlag = FALSE;
					while(pos2)
					{
						if(str == selDataFolerList.GetNext(pos2))
						{
							 bSameFlag = TRUE;
							//동일 폴더가 있을 경우 채널 번호만 추가 
							 break;
						}
					}
					//동일 폴더가 있지 않으면 새롭게 추가한다.
					if( bSameFlag == FALSE)
					{
						selDataFolerList.AddTail(str);			//폴더 추가
					}
				}
			}
		}

		SetTestList(&selDataFolerList);
	}
	//lpszPathName Null로 올 수 없음 
/*	if(lpszPathName == NULL || selDataFolerList.GetCount() == 0 )
	{
		if(SelectTestDlg()== FALSE)		return FALSE;
	}
*/

	return TRUE;
}

BOOL CDataDoc::DeleteFolder(CString strFolder)
{
	if(strFolder.IsEmpty())		return FALSE;

	BOOL	bReturn = TRUE;
	CFileFind finder;
	char szFrom[_MAX_PATH];
	ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate

	//pFrom에 Full Path 제공하고 
	//pTo에 Path명을 제공하지 않고 
	//fFlags Option에 FOF_ALLOWUNDO 부여하고 
	//Delete 동작 하면 휴지통으로 삭제 된다.
	LPSHFILEOPSTRUCT lpFileOp = new SHFILEOPSTRUCT;
	ZeroMemory(lpFileOp, sizeof(SHFILEOPSTRUCT));
	lpFileOp->hwnd = NULL; 
	lpFileOp->wFunc = FO_DELETE ; 
	lpFileOp->pFrom = szFrom; 
	lpFileOp->pTo = NULL; 
	lpFileOp->fFlags = FOF_FILESONLY|FOF_NOCONFIRMATION|FOF_ALLOWUNDO ;//FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY ; 
	lpFileOp->fAnyOperationsAborted = FALSE; 
	lpFileOp->hNameMappings = NULL; 
	lpFileOp->lpszProgressTitle = NULL; 

	sprintf(szFrom, "%s", strFolder);

	//삭제 파일을 휴지통으로 보냄 			
	BeginWaitCursor();
	if(SHFileOperation(lpFileOp) != 0)		//Move File
	{
		//AfxMessageBox(strFolder+"를 삭제할 수 없습니다.");
		AfxMessageBox(strFolder+Fun_FindMsg("DataDoc_DeleteFolder_msg1","DATADOC"));//&&
		bReturn = FALSE;
	}
	EndWaitCursor();

	//파일 삭제시 
//	// build a string with wildcards
//	CString strWildcard(strFolder);
//	strWildcard += _T("\\*.*");
//
//	// start working for files
//	BOOL bWorking = finder.FindFile(strWildcard);
//
//	BeginWaitCursor();
//	while (bWorking)
//	{
//		bWorking = finder.FindNextFile();
//
//		// skip . and .. files; otherwise, we'd
//		// recur infinitely!
//
//		if (finder.IsDots())
//		   continue;
//
//		// if it's a directory, recursively search it
//
//		if (finder.IsDirectory())
//		{
//			if(DeleteFolder(finder.GetFilePath()) == FALSE)
//			{
//				EndWaitCursor();
//				delete lpFileOp;
//				lpFileOp = NULL;
//				finder.Close();
//				return FALSE;
//			}
//		}
//		else
//		{
//			  CString strFileName = finder.GetFilePath();
//			//바로 삭제 
///*			if(_unlink((LPSTR)(LPCTSTR)strTemp) != 0)
//			{
//				strTemp =  strTemp+" 파일을 삭제 할 수 없습니다."; 
//				AfxMessageBox(strTemp);
//				continue;
//			}
//*/			
//			ZeroMemory(szFrom, _MAX_PATH);	//Double NULL Terminate
//			sprintf(szFrom, "%s", strFileName);
//
//			//삭제 파일을 휴지통으로 보냄 			
//			if(SHFileOperation(lpFileOp) != 0)		//Move File
//			{
//				EndWaitCursor();
//				delete lpFileOp;
//				lpFileOp = NULL;
//				finder.Close();
//				AfxMessageBox(strFileName+"를 삭제할 수 없습니다.\n");
//				return FALSE;
//			}
//		}
//	}
//	EndWaitCursor();
//
//	//자기 자신 폴더 삭제 
//	{
//		
//	}
//
//	delete lpFileOp;
//	lpFileOp = NULL;
//	
//	finder.Close();
	return bReturn;
}
/*
CString CDataDoc::GetExcelPath()
{
	CString strExcelPath = AfxGetApp()->GetProfileString(REG_CONFIG_SECTION,"Excel Path");
	CFileFind finder;
	if(finder.FindFile(strExcelPath) == FALSE)
	{
		AfxMessageBox("Excel 위치가 설정되어 있지 않습니다. 위치를 지정하여 주십시요.");
		CFileDialog pDlg(TRUE, "exe", strExcelPath, OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, "exe file(*.exe)|*.exe|All Files(*.*)|*.*|");
		pDlg.m_ofn.lpstrTitle = "Excel 파일 위치";
		if(IDOK != pDlg.DoModal())
		{
			return	"";
		}
		strExcelPath = pDlg.GetPathName();
		AfxGetApp()->WriteProfileString(REG_CONFIG_SECTION,"Excel Path", strExcelPath);
	}
	return strExcelPath;
}
*/

float CDataDoc::Fun_CalImpedance(CString &strName, int fTime1, float fTime2)
{
	double fVData, fIData;
	double fVData1, fIData1;
	double fVData2, fIData2;
//	double fTime2 = 0.0f;
	double fSigmaXY = 0.0f, fSigmaX = 0.0f, fSigmaY = 0.0f, fSigmaXX = 0.0f,fSigmaCurrent=0.0f;

	int iCount=0;
	float   fTime= 0;
	CString strTime, strTemp, str;
	CStdioFile aFile;
	CFileException e;

	//m_strFileName = strFileName;
	if( !aFile.Open( strName, CFile::modeRead|CFile::shareDenyWrite, &e ) )
	{
	#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
		return FALSE;
	}

	int iLineNum=0;	//ljb 201167 처음 5줄 버림

	CStringList		strDataList,strArryTitleList;
	while(aFile.ReadString(strTemp))
	{
		iLineNum++;
		if(!strTemp.IsEmpty())	
		{
			if (iLineNum > 1)
			{
				strTime = strTemp.Left(strTemp.Find(','));
				fTime = atof(strTime);
			}
			
			strDataList.AddTail(strTemp);
		}
	}
	aFile.Close();

	int nRecordCount = strDataList.GetCount()-1;
	if(nRecordCount < 0)		return FALSE;

	int p1=0, s=0;
	POSITION pos = strDataList.GetHeadPosition();

	strTemp = strDataList.GetNext(pos);		//Title
	while(p1!=-1)
	{
		p1 = strTemp.Find(',', s);
		if(p1!=-1)
		{
			str = strTemp.Mid(s, p1-s);
			strArryTitleList.AddTail(str);
			s  = p1+1;
		}
	}
	str = strTemp.Mid(s);
	str.TrimLeft(' '); str.TrimRight(' ');
	if(str.IsEmpty() == FALSE)	strArryTitleList.AddTail(str);

	int nColumnSize = strArryTitleList.GetCount();

//	m_ppData = new float*[m_nColumnSize];
// 	for(int i =0; i<nColumnSize; i++)
// 	{
// 		m_ppData[i] = new float[m_nRecordCount];
// 	}

	float a = 0, b=0;
	CStringList strColList;
	int nRecord = 0;
	while(pos)
	{
		strTemp = strDataList.GetNext(pos);
		//////////////////////////////////////////////////////////////////////////
		p1=0, s=0;
		strColList.RemoveAll();
		while(p1!=-1)
		{
			p1 = strTemp.Find(',', s);
			if(p1!=-1)
			{
				str = strTemp.Mid(s, p1-s);
				strColList.AddTail(str);
				s  = p1+1;
			}
		}
		str = strTemp.Mid(s);
		str.TrimLeft(' '); str.TrimRight(' ');
		if(str.IsEmpty() == FALSE)	strColList.AddTail(str);
		
		//////////////////////////////////////////////////////////////////////////
		POSITION pos1 = strColList.GetHeadPosition();
		int nC = 0;
		while(pos1 && nC < nColumnSize)
		{
			str = strColList.GetNext(pos1);
			//m_ppData[nC][nRecord] = atof(str) * 1000.0f;		//ljb * 1000 추가함  [10/6/2011 XNOTE]
			
			//시간 값
			if (nC == 0) fTime = atof(str);
						
			//전압 값
			if (nC == 1)  fVData = atof(str);
			//전류값
			if (nC == 2)  fIData = atof(str);
			nC++;
		}
		nRecord++;
		if (nRecord == 1)
		{
			fVData1 = fVData * 1000;
			fIData1 = fIData * 1000;
		}
		if (fTime2 <= fTime)
		{
			fTime = fTime * 1000;
			fVData2 = fVData * 1000;
			fIData2 = fIData * 1000;
			//1. sigma(x*y)
			fSigmaXY += (fTime*fVData2);
			//2. sigma(x)
			fSigmaX += fTime;
			//3. sigma(y)
			fSigmaY += fVData2;
			//4. sigma(x*x)
			fSigmaXX += fTime*fTime;
			//fTime2 = fTime;
			//5. sigma(current)
			fSigmaCurrent += fIData2;
			TRACE("%f\n", fVData2);
					
			iCount++;
			if (iCount >= fTime1)
			{
				break;
			}

		}
	}
	a = (fTime1*fSigmaXY-fSigmaX*fSigmaY)/(fTime1*fSigmaXX-fSigmaX*fSigmaX);
	b = (fSigmaY-a*fSigmaX)/fTime1;
	fIData2 = fSigmaCurrent / fTime1;		//평균 전류

	fVData2 = b;

	double fResist = 0.0f;
	double fDeltaI, fDeltaV;
	fDeltaI = fIData1-fIData2;
	fDeltaV = fVData1-fVData2;
	if(fDeltaI != 0.0f)
	{
		fResist = fDeltaV/fDeltaI*1000.0f;
	}
	
// 	CString str, str1;
// 	str.Format("%.3f", fVData1/1000);
// 	GetDlgItem(IDC_EDIT_V1)->SetWindowText(str);
// 	str.Format("%.3f", fVData2/1000);
// 	GetDlgItem(IDC_EDIT_V2)->SetWindowText(str);
// 	str.Format("%.3f", fIData1/1000);
// 	GetDlgItem(IDC_EDIT_I1)->SetWindowText(str);
// 	str.Format("%.3f", fIData2/1000);
// 	GetDlgItem(IDC_EDIT_I2)->SetWindowText(str);
// 	str.Format("%.6f", fResist);
// 	GetDlgItem(IDC_EDIT_R)->SetWindowText(str);


	strName.Format("DCR(%.6f) v= %.3f",fResist,fVData2);

	return fResist;
 
}

float CDataDoc::CalImpedance(CString &strName, int fTime1, float fTime2)
{
	//ljb fTime1 은 Time2 다음에 추세선 이용할 데이터 개수임 (100)
	float fVData1, fIData1;
	float fVData2, fIData2;
	//double fVtg1 = 0.0f, fVtg2 = 0.0f, fCrt1 = 0.0f, fCrt2 = 0.0f;
	double fVoltageSum, fCurrentSum, fCapacity, fWattHour;
	fVoltageSum=0.0f;
	fCurrentSum=0.0f;

	//ljb 추세선 적용 해야 함
	FILE *fp = fopen(strName, "rt");
	if(fp)
	{
		char szBuff[64];
		long lIndex1 = 0, lIndex2 = 0;
		float fPrevTime = -1.0f,fStepTime;

		if(fscanf(fp, "%s,%s,%s,%s,%s", szBuff, szBuff, szBuff, szBuff, szBuff) > 0)	//Skip Header
		{
			if(fscanf(fp, "%d,%d,%d,%s,%s",  &fStepTime, &fVData1, &fIData1, szBuff, szBuff) > 0)	//Load First Data
			{
				while(fscanf(fp, "%f,%f,%f,%f,%f", &fStepTime, &fVData2, &fIData2, &fCapacity, &fWattHour) > 0)
				{
// 					if(fPrevTime < fStepTime && fStepTime <= fTime1)
// 					{
// 						lIndex1++;
// 						fVtg1 = fVoltage;
// 						fCrt1 = fCurrent;
// 					}
					
					//if(fPrevTime < fStepTime && fStepTime <= fTime2)
					if(fStepTime >= fTime2)
					{
						lIndex2++;
						fVoltageSum += fVData2;
						fCurrentSum += fIData2;
						if (lIndex2 > fTime1) break;
					}
				}

			}
		}
		fclose(fp);
// 		if(lIndex1 < lIndex2 && fCrt2 != fCrt1)
// 		{
// 			strName.Format("(%d, %d):R=(%.3f-%.3f)/(%.3f-%.3f)=%fOhm", lIndex1, lIndex2, fVtg2, fVtg1, fCrt2, fCrt1, (fVtg2-fVtg1)/(fCrt2-fCrt1));
// 			return  (fVtg2-fVtg1)/(fCrt2-fCrt1)*1000.0f;
// 		}
	}
	return -1.0f;
}