/*************************************************************
	Copyrighted by LG Production engineering Research Center

	File Name: HelpFrame.h

	Written out by 
	Evaluation Technology Group
	Kim, Sung-Hoon

	Comment: CHelpFrame class is defined

	Revision History:
	Version   Name            Date        Reason
	    1.0   Kim, Sung-Hoon  2001.06.01  Create
*************************************************************/

#if !defined(AFX_HELPFRAME_H__672B958C_217D_45A3_A24D_45DDEA0A9210__INCLUDED_)
#define AFX_HELPFRAME_H__672B958C_217D_45A3_A24D_45DDEA0A9210__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HelpFrame.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHelpFrame frame

// #include

// #define

// Class definition

//---------------------------------------------------------------------------
//	Class Name: CHelpFrame
//
//	Comment:
//
//	Revision History:
//	Version   Name            Date        Reason
//	    1.0   Kim, Sung-Hoon  2001.06.01  Create
//---------------------------------------------------------------------------
class CHelpFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CHelpFrame)
public:
	CHelpFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHelpFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnUpdateFrameMenu(BOOL bActive, CWnd* pActivateWnd, HMENU hMenuAlt);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHelpFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CHelpFrame)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HELPFRAME_H__672B958C_217D_45A3_A24D_45DDEA0A9210__INCLUDED_)
