// SelMdlDlgBar.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"
#include "SelMdlDlgBar.h"
#include "MonitorDoc.h"
//#include "../PSModule/Module.h"
//#include "../PSModule/Unit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelModuleDialogBar

CSelModuleDialogBar::CSelModuleDialogBar()
{
	//{{AFX_DATA_INIT(CSelModuleDialogBar)
	//}}AFX_DATA_INIT

	m_pDocument     = NULL;
	m_byCheckState  = STATE_CHECK_NONE;
	m_iCurSelUnit   = 0;
	m_iCurSelModule = 0;
}

CSelModuleDialogBar::~CSelModuleDialogBar()
{
}


BEGIN_MESSAGE_MAP(CSelModuleDialogBar, CDialogBar)
	//{{AFX_MSG_MAP(CSelModuleDialogBar)
	ON_BN_CLICKED(IDC_STATUS_FINISH_CHECK,	OnStatusFinishCheck)
	ON_BN_CLICKED(IDC_STATUS_ERROR_CHECK,	OnStatusErrorCheck)
	ON_BN_CLICKED(IDC_STATUS_INITIAL_CHECK, OnStatusInitialCheck)
	ON_BN_CLICKED(IDC_STATUS_PAUSE_CHECK,	OnStatusPauseCheck)
	ON_BN_CLICKED(IDC_STATUS_RUN_CHECK,		OnStatusRunCheck)
	ON_CBN_SELCHANGE(IDC_UNIT_COMBO,		OnSelchangeUnitCombo)
	ON_CBN_SELCHANGE(IDC_MODULE_COMBO,		OnSelchangeModuleCombo)
	ON_BN_CLICKED(IDC_SEL_STATUS_ALL,		OnSelStatusAll)
	ON_UPDATE_COMMAND_UI(IDC_SEL_STATUS_ALL,OnUpdateSelStatusAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSelModuleDialogBar message handlers

void CSelModuleDialogBar::DoDataExchange(CDataExchange* pDX) 
{
	CDialogBar::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CSelModuleDialogBar)
	DDX_Control(pDX, IDC_UNIT_COMBO, m_UnitCombo);
	DDX_Control(pDX, IDC_MODULE_COMBO, m_ModuleCombo);
	//}}AFX_DATA_MAP
}

BOOL CSelModuleDialogBar::Create(CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID )
{
	// TODO: Add your specialized code here and/or call the base class
	
	if(!CDialogBar::Create(pParentWnd, nIDTemplate, nStyle, nID)) return FALSE;

	//
	UpdateData(FALSE);

	//
	return TRUE;
}

void CSelModuleDialogBar::OnStatusFinishCheck() 
{
	int iCheck = ((CButton*)GetDlgItem(IDC_STATUS_FINISH_CHECK))->GetCheck();
	if(iCheck==1) m_byCheckState |=  STATE_CHECK_FINISH;
	else          m_byCheckState &= ~STATE_CHECK_FINISH;
	UpdateListView();
}

void CSelModuleDialogBar::OnStatusErrorCheck() 
{
	int iCheck = ((CButton*)GetDlgItem(IDC_STATUS_ERROR_CHECK))->GetCheck();
	if(iCheck==1) m_byCheckState |=  STATE_CHECK_ERROR;
	else          m_byCheckState &= ~STATE_CHECK_ERROR;
	UpdateListView();
}

void CSelModuleDialogBar::OnStatusInitialCheck() 
{
	int iCheck = ((CButton*)GetDlgItem(IDC_STATUS_INITIAL_CHECK))->GetCheck();
	if(iCheck==1) m_byCheckState |=  STATE_CHECK_INITIAL;
	else          m_byCheckState &= ~STATE_CHECK_INITIAL;
	UpdateListView();
}

void CSelModuleDialogBar::OnStatusPauseCheck() 
{
	int iCheck = ((CButton*)GetDlgItem(IDC_STATUS_PAUSE_CHECK))->GetCheck();
	if(iCheck==1) m_byCheckState |=  STATE_CHECK_PAUSE;
	else          m_byCheckState &= ~STATE_CHECK_PAUSE;
	UpdateListView();
}

void CSelModuleDialogBar::OnStatusRunCheck() 
{
	int iCheck = ((CButton*)GetDlgItem(IDC_STATUS_RUN_CHECK))->GetCheck();
	if(iCheck==1) m_byCheckState |=  STATE_CHECK_RUN;
	else          m_byCheckState &= ~STATE_CHECK_RUN;
	UpdateListView();
}

void CSelModuleDialogBar::InitialSet(CMonitorDoc* pDoc)
{
	//
	m_pDocument = pDoc;
	m_UnitCombo.AddString("ALL");
//	for(BYTE i=0; i<CUnit::m_byNumOfUnit; i++)
//	{
//		CString str;
//		str.Format("%02d", pDoc->m_pUnit[i].GetNo());
//		m_UnitCombo.AddString(str);
//	}
	m_UnitCombo.SetCurSel(m_iCurSelUnit);

	//
	((CButton*)GetDlgItem(IDC_STATUS_INITIAL_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_RUN_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_ERROR_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_PAUSE_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_FINISH_CHECK))->SetCheck(1);
	m_byCheckState = STATE_CHECK_INITIAL|STATE_CHECK_RUN|STATE_CHECK_ERROR|STATE_CHECK_PAUSE|STATE_CHECK_FINISH;
	
	//
	OnSelchangeUnitCombo();
}

void CSelModuleDialogBar::OnSelchangeUnitCombo() 
{
	m_iCurSelUnit = m_UnitCombo.GetCurSel();
	if(m_iCurSelUnit!=CB_ERR)
	{
		//
		m_ModuleCombo.ResetContent();
		m_ModuleCombo.AddString("ALL");
	
		//

		if(m_iCurSelUnit==0)
		{
//			for(BYTE i=0; i<CModule::m_byNumOfModules; i++)
//			{
//				//if( i >= 4 )	continue;
//				CString str;
//				str.Format("%02d", m_pDocument->m_pModule[i].GetModuleNo());
//				m_ModuleCombo.AddString(str);
//			}
		}
		
		//
		else
		{
//			for(BYTE i=0; i<CModule::m_byNumOfModules; i++)
//			{
//				if(m_pDocument->m_pUnit[m_iCurSelUnit-1].GetNo()==m_pDocument->m_pModule[i].GetUnitNo())
//				//if(m_pDocument->m_pUnit[m_iCurSelUnit].GetNo()==m_pDocument->m_pModule[i].GetUnitNo())
//				{
//					CString str;
//					str.Format("%02d", m_pDocument->m_pModule[i].GetModuleNo());
//					m_ModuleCombo.AddString(str);
//				}
//			}
		}
		
		//
		m_ModuleCombo.SetCurSel(0);
		//
	}
	//
	OnSelchangeModuleCombo();
	AfxGetMainWnd()->SetFocus();
}

void CSelModuleDialogBar::UpdateListView()
{
	if(m_iCurSelUnit!=CB_ERR&&m_iCurSelModule!=CB_ERR)
	{
		BYTE UnitNo=0, ModuleNo=0;
		if(m_iCurSelUnit!=0)   
//			UnitNo   = m_pDocument->m_pUnit[m_iCurSelUnit-1].GetNo();
		if(m_iCurSelModule!=0)
		{
			CString str;
			m_ModuleCombo.GetLBText(m_iCurSelModule,str);
			ModuleNo = (BYTE) atoi(str);
		}

		m_pDocument->UpdateListView(UnitNo, ModuleNo, m_byCheckState);
	}
}

void CSelModuleDialogBar::OnSelchangeModuleCombo() 
{
	m_iCurSelModule = m_ModuleCombo.GetCurSel();
	if(m_iCurSelModule!=CB_ERR)
	{
		AfxGetMainWnd()->SetFocus();
		OnSelStatusAll();
	}
}

void CSelModuleDialogBar::OnSelStatusAll() 
{
	((CButton*)GetDlgItem(IDC_STATUS_INITIAL_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_RUN_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_ERROR_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_PAUSE_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_STATUS_FINISH_CHECK))->SetCheck(1);
	m_byCheckState = STATE_CHECK_INITIAL|STATE_CHECK_RUN|STATE_CHECK_ERROR|STATE_CHECK_PAUSE|STATE_CHECK_FINISH;
	UpdateListView();
}

void CSelModuleDialogBar::OnUpdateSelStatusAll(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_byCheckState!=(STATE_CHECK_INITIAL|STATE_CHECK_RUN|STATE_CHECK_ERROR|STATE_CHECK_PAUSE|STATE_CHECK_FINISH));
}

void CSelModuleDialogBar::SetCheckState(BYTE byCheckState)
{
	m_byCheckState = byCheckState;
	//
	if(m_byCheckState&STATE_CHECK_INITIAL) ((CButton*)GetDlgItem(IDC_STATUS_INITIAL_CHECK))->SetCheck(1);
	else                                   ((CButton*)GetDlgItem(IDC_STATUS_INITIAL_CHECK))->SetCheck(0);
	//
	if(m_byCheckState&STATE_CHECK_RUN    ) ((CButton*)GetDlgItem(IDC_STATUS_RUN_CHECK))->SetCheck(1);
	else                                   ((CButton*)GetDlgItem(IDC_STATUS_RUN_CHECK))->SetCheck(0);
	//
	if(m_byCheckState&STATE_CHECK_ERROR  ) ((CButton*)GetDlgItem(IDC_STATUS_ERROR_CHECK))->SetCheck(1);
	else                                   ((CButton*)GetDlgItem(IDC_STATUS_ERROR_CHECK))->SetCheck(0);
	//
	if(m_byCheckState&STATE_CHECK_PAUSE  ) ((CButton*)GetDlgItem(IDC_STATUS_PAUSE_CHECK))->SetCheck(1);
	else                                   ((CButton*)GetDlgItem(IDC_STATUS_PAUSE_CHECK))->SetCheck(0);
	//
	if(m_byCheckState&STATE_CHECK_FINISH ) ((CButton*)GetDlgItem(IDC_STATUS_FINISH_CHECK))->SetCheck(1);
	else                                   ((CButton*)GetDlgItem(IDC_STATUS_FINISH_CHECK))->SetCheck(0);
}

void CSelModuleDialogBar::SetProgressMsg(CString strMsg)
{
}

void CSelModuleDialogBar::SetStatusMsg(CString strMsg, BYTE nUnitNo)
{
}