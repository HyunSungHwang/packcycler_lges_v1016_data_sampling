// HelpView.cpp : implementation file
//

#include "stdafx.h"
#include "CTSAnalPro.h"

#include "HelpDoc.h"
#include "HelpView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHelpView

IMPLEMENT_DYNCREATE(CHelpView, CHtmlView)

BEGIN_MESSAGE_MAP(CHelpView, CHtmlView)
	//{{AFX_MSG_MAP(CHelpView)
//	ON_COMMAND(IDM_MOVE_NEXT, OnMoveNext)
	ON_COMMAND(IDM_MOVE_HONE, OnMoveHone)
//	ON_COMMAND(IDM_MOVE_PREV, OnMovePrev)
	ON_COMMAND(IDM_MOVE_REFRESH, OnMoveRefresh)
	ON_COMMAND(IDM_MOVE_STOP, OnMoveStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CHelpView::CHelpView()
{
}

CHelpView::~CHelpView()
{
}

BOOL CHelpView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CHtmlView::PreCreateWindow(cs);
}

void CHelpView::OnDraw(CDC* pDC)
{
	CHelpDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

void CHelpView::OnInitialUpdate()
{
	CHtmlView::OnInitialUpdate();

	// TODO: This code navigates to a popular spot on the web.
	//  change the code to go where you'd like.
	Navigate2(_T(CCTSAnalProApp::m_strOriginFolderPath+"\\Manual.htm"),NULL,NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CHelpView diagnostics

#ifdef _DEBUG
void CHelpView::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CHelpView::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}

CHelpDoc* CHelpView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CHelpDoc)));
	return (CHelpDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CHelpView message handlers

void CHelpView::OnMoveNext() 
{
	GoForward();	
}

void CHelpView::OnMoveHone() 
{
	Navigate2(_T(CCTSAnalProApp::m_strOriginFolderPath+"\\Manual.htm"),NULL,NULL);
}

void CHelpView::OnMovePrev() 
{
	GoBack();
}

void CHelpView::OnMoveRefresh() 
{
	Refresh();	
}

void CHelpView::OnMoveStop() 
{
	Stop();	
}
