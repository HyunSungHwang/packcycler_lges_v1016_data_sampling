//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GuiUpdater.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GUIUPDATER_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_STATIC_1                    1000
#define IDC_EDIT_FILE_PATH              1001
#define IDC_BTN_FIND_PATH               1002
#define IDC_BTN_UPDATE                  1003
#define IDC_PROGRESS_UPDATE_STATE       1004
#define IDC_STATIC_STATE                1005
#define IDC_CHECK_AUTO_BACKUP           1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
