
// GuiUpdaterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "GuiUpdater.h"
#include "GuiUpdaterDlg.h"
#include "afxdialogex.h"
#include <TlHelp32.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CGuiUpdaterDlg::CGuiUpdaterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGuiUpdaterDlg::IDD, pParent)
	, m_sUpdateFilePath(_T(""))
	, m_sUpdateState(_T("Ready"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGuiUpdaterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_FILE_PATH, m_sUpdateFilePath);
	DDX_Control(pDX, IDC_PROGRESS_UPDATE_STATE, m_progressUpdateState);
	DDX_Text(pDX, IDC_STATIC_STATE, m_sUpdateState);
}

BEGIN_MESSAGE_MAP(CGuiUpdaterDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_FIND_PATH, &CGuiUpdaterDlg::OnBnClickedBtnFindPath)
	ON_BN_CLICKED(IDC_BTN_UPDATE, &CGuiUpdaterDlg::OnBnClickedBtnUpdate)
	ON_MESSAGE(_WM_UPDATE_STATE_, &CGuiUpdaterDlg::OnUpdateState)
END_MESSAGE_MAP()


BOOL CGuiUpdaterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	CString strPath;

	// Get Current Directory Path
	TCHAR szBuff[_MAX_PATH];
	::GetModuleFileName(NULL,szBuff,_MAX_PATH);

	//m_sBasePath = _T("C:\\Program Files (x86)\\PNE CTSPack");
	m_sBasePath = CString(szBuff).Mid(0, CString(szBuff).ReverseFind('\\')); //lyj 20210722
	m_sUpdateFilePath = m_sBasePath + _T("\\Update");

	m_progressUpdateState.SetRange(0, 100);

	AfxBeginThread(ThreadUpdate, this);

	UpdateData(FALSE);

	return TRUE;
}

void CGuiUpdaterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
// 	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
// 	{
// 		CAboutDlg dlgAbout;
// 		dlgAbout.DoModal();
// 	}
// 	else
// 	{
 		CDialogEx::OnSysCommand(nID, lParam);
//	}
}

void CGuiUpdaterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

HCURSOR CGuiUpdaterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CGuiUpdaterDlg::OnBnClickedBtnFindPath()
{
	BROWSEINFO BrInfo;
	TCHAR szBuffer[512] = {NULL, };

	ZeroMemory(&BrInfo, sizeof(BROWSEINFO));

	BrInfo.hwndOwner = GetSafeHwnd();
	BrInfo.lpszTitle = _T("Select Update Folder Path");
	BrInfo.ulFlags = BIF_NEWDIALOGSTYLE | BIF_EDITBOX | BIF_RETURNONLYFSDIRS;

	LPITEMIDLIST pItemIdList = ::SHBrowseForFolder(&BrInfo);

	::SHGetPathFromIDList(pItemIdList, szBuffer);               

	m_sUpdateFilePath.Format(_T("%s"), szBuffer);
	
	UpdateData(FALSE);
}


void CGuiUpdaterDlg::OnBnClickedBtnUpdate()
{
	UpdateData();

	AfxBeginThread(ThreadUpdate, this);
	
}

BOOL CGuiUpdaterDlg::UpdateGuiProgram(CString sSrc_, CString sDest_)
{
	CFileFind cFindSrc;
	CString sFindPath;
	BOOL bRes;
	CString sCopySrc, sCopyDest;
	CString sFindFolderName;

	sFindPath.Format(_T("%s\\*.*"), sSrc_);

	bRes = cFindSrc.FindFile(sFindPath);

	while(bRes)
	{
		bRes = cFindSrc.FindNextFile();

		if (cFindSrc.IsDots())
		{
			continue;
		}
		else if (cFindSrc.IsDirectory())
		{
			sFindFolderName = cFindSrc.GetFileTitle();
			sCopySrc = cFindSrc.GetFilePath();
			sCopyDest.Format(_T("%s\\%s"), sDest_, sFindFolderName);

			CopyFolder(sCopySrc, sCopyDest);
		}
		else
		{
			sCopySrc = cFindSrc.GetFilePath();
			sCopyDest.Format(_T("%s\\%s"), sDest_, cFindSrc.GetFileName());

			::CopyFile(sCopySrc, sCopyDest, NULL);
		}
	}


	return TRUE;
}

LRESULT CGuiUpdaterDlg::OnUpdateState(WPARAM wParam, LPARAM lParam)
{
	int nCmd = (int)lParam;
	int nProgressPos = 0;

	switch(nCmd)
	{
	case 0:
		m_sUpdateState = _T("Update Start...");
		m_progressUpdateState.SetPos(0);
		break;
	case 1:
		m_sUpdateState = _T("Check Update File Path Complete...");
		m_progressUpdateState.SetPos(25);
		break;
	case 2:
		m_sUpdateState = _T("Check Acvice GUI Process Complete...");
		m_progressUpdateState.SetPos(50);
		break;
	case 3:
		m_sUpdateState = _T("Program Backup Complete...");
		m_progressUpdateState.SetPos(75);
		break;
	case 4:
		m_sUpdateState = _T("Update Complete.");
		m_progressUpdateState.SetPos(100);
		break;
	default:
		m_sUpdateState = _T("Update Fail.");
		m_progressUpdateState.SetPos(0);
		break;
	}

	Invalidate(FALSE);

	return 0;
}

BOOL CGuiUpdaterDlg::CheckActiveGuiProcess()
{
	CString sGuiProcess[3] = {_T("CTSMonitorPro.exe"), _T("CTSEditorPro.exe"), _T("CTSAnalyzerPro.exe")};
	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 processEntry32;

	if(hProcessSnap == INVALID_HANDLE_VALUE)
	{
		exit(EXIT_FAILURE);
	}

	processEntry32.dwSize = sizeof(PROCESSENTRY32);

	if( !Process32First(hProcessSnap, &processEntry32) )
	{
		CloseHandle(hProcessSnap);
		exit(EXIT_FAILURE); 
	}

	while(Process32Next(hProcessSnap, &processEntry32))
	{
		if (sGuiProcess[0].Compare(processEntry32.szExeFile) == 0)
		{
			//AfxMessageBox(_T("CTSMonitorPro가 동작중입니다."));
			return FALSE;
		}
		else if (sGuiProcess[1].Compare(processEntry32.szExeFile) == 0)
		{
			//AfxMessageBox(_T("CTSEditorPro가 동작중입니다."));
			return FALSE;
		}
		else if (sGuiProcess[1].Compare(processEntry32.szExeFile) == 0)
		{
			//AfxMessageBox(_T("CTSAnalyzerPro가 동작중입니다."));
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CGuiUpdaterDlg::BackupOldFile()
{
	CString sBackupBasePath, sBackupPath;
	CString sBackupSrc, sBackupDest;
	CString sCmdMsg;
	CString sTemp;

	CTime ctToday = CTime::GetCurrentTime();

	if (PathIsDirectory(m_sBasePath) == FALSE)
	{
		m_sBasePath = _T("C:\\Program Files\\PNE CTSPack");

		if (PathIsDirectory(m_sBasePath) == FALSE)
		{
			//AfxMessageBox(_T("GUI Program 설치 경로를 찾을 수 없습니다."));
			return FALSE;
		}
	}

	sBackupBasePath.Format(_T("%s\\Backup"), m_sBasePath);

	if (PathIsDirectory(sBackupBasePath) == FALSE)		::CreateDirectory(sBackupBasePath, NULL);

	sBackupPath.Format(_T("%s\\%.2d%.2d%.2d"), sBackupBasePath, ctToday.GetYear(), ctToday.GetMonth(), ctToday.GetDay());

	if (PathIsDirectory(sBackupPath) == FALSE)	::CreateDirectory(sBackupPath, NULL);

	// 1. DataBase
	sBackupSrc.Format(_T("%s\\DataBase"), m_sBasePath);
	sBackupDest.Format(_T("%s\\DataBase"), sBackupPath);

	if (PathIsDirectory(sBackupSrc))
	{
		if (PathIsDirectory(sBackupDest) == FALSE)	::CreateDirectory(sBackupDest, NULL);

		//CopyFolder(sBackupSrc, sBackupDest);
		CString sTempDB1, sTempDB2;

		sTempDB1.Format(_T("%s\\Pack_Schedule_2000.mdb"), sBackupSrc);
		sTempDB2.Format(_T("%s\\Pack_Schedule_2000.mdb"), sBackupDest);

		::CopyFile(sTempDB1, sTempDB2, NULL);

		sTempDB1.Format(_T("%s\\Pack_ChannelCode.mdb"), sBackupSrc);
		sTempDB2.Format(_T("%s\\Pack_ChannelCode.mdb"), sBackupDest);

		::CopyFile(sTempDB1, sTempDB2, NULL);

		sTempDB1.Format(_T("%s\\Pack_EmgCode.mdb"), sBackupSrc);
		sTempDB2.Format(_T("%s\\Pack_EmgCode.mdb"), sBackupDest);

		::CopyFile(sTempDB1, sTempDB2, NULL);
		
		//lyj 20210722 s =
		sTempDB1.Format(_T("%s\\Pack_Code_2000.mdb"), sBackupSrc);
		sTempDB2.Format(_T("%s\\Pack_Code_2000.mdb"), sBackupDest);

		::CopyFile(sTempDB1, sTempDB2, NULL);
		//lyj 20210722 e =
	}

	// 2. Language_PackCycler
	sBackupSrc.Format(_T("%s\\Language_PackCycler"), m_sBasePath);
	sBackupDest.Format(_T("%s\\Language_PackCycler"), sBackupPath);

	if (PathIsDirectory(sBackupSrc))
	{
		if (PathIsDirectory(sBackupDest) == FALSE)	::CreateDirectory(sBackupDest, NULL);

		CopyFolder(sBackupSrc, sBackupDest);
	}
	
	// 3. CTSMonitorPro.exe
	sBackupSrc.Format(_T("%s\\CTSMonitorPro.exe"), m_sBasePath);
	sBackupDest.Format(_T("%s\\CTSMonitorPro.exe"), sBackupPath);
	if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);

	// 4. CTSEditorPro.exe
	sBackupSrc.Format(_T("%s\\CTSEditorPro.exe"), m_sBasePath);
	sBackupDest.Format(_T("%s\\CTSEditorPro.exe"), sBackupPath);
	if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);

	// 5. CTSAnalyzerPro.exe
	sBackupSrc.Format(_T("%s\\CTSAnalyzerPro.exe"), m_sBasePath);
	sBackupDest.Format(_T("%s\\CTSAnalyzerPro.exe"), sBackupPath);
	if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);

	// 6. PSCommon.dll
	sBackupSrc.Format(_T("%s\\PSCommon.dll"), m_sBasePath);
	sBackupDest.Format(_T("%s\\PSCommon.dll"), sBackupPath);
	if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);

	// 7. PSData.dll
	sBackupSrc.Format(_T("%s\\PSData.dll"), m_sBasePath);
	sBackupDest.Format(_T("%s\\PSData.dll"), sBackupPath);
	if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);

	// 8. PSServer.dll
	sBackupSrc.Format(_T("%s\\PSServer.dll"), m_sBasePath);
	sBackupDest.Format(_T("%s\\PSServer.dll"), sBackupPath);
	if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);

	//lyj 20210722 s =
	// CTSFlowPress.exe
	sBackupSrc.Format(_T("%s\\CTSFlowPress.exe"), m_sBasePath);
	sBackupDest.Format(_T("%s\\CTSFlowPress.exe"), sBackupPath);
	if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);

	// PDB
	sBackupSrc.Format(_T("%s\\PDB"), m_sBasePath);
	sBackupDest.Format(_T("%s\\PDB"), sBackupPath);
	//if (PathFileExists(sBackupSrc))	::CopyFile(sBackupSrc, sBackupDest, NULL);
	if (PathFileExists(sBackupSrc))	CopyFolder(sBackupSrc, sBackupDest);
	//lyj 20210722 e =

	// 9. Registry
	sTemp.Format(_T("C:\\BackupReg.reg"));	 // C:\\Program Files(x86)경로에 바로 레지스트리 내보내기 안되므로 우회할 경로 설정
	sBackupDest.Format(_T("%s\\BackupReg.reg"), sBackupPath);
	sCmdMsg.Format(_T("/C reg export \"HKCU\\Software\\PNE CTSPack\" %s"), sTemp);	

	::ShellExecute(NULL, _T("runas"), _T("cmd.exe"), sCmdMsg, NULL, SW_HIDE);

	Sleep (1000); // Registry 파일 저장할 시간 대기

	::CopyFile(sTemp, sBackupDest, NULL);
	::DeleteFile(sTemp);


	return TRUE;
}

int CGuiUpdaterDlg::CopyFolder(CString sSrc_, CString sDest_)
{
	CFileFind cFind;
	CString sFindPath;
	BOOL bRes;
	CString sFrom, sTo;
	CString sFindFolderName;

	sFindPath.Format(_T("%s\\*.*"), sSrc_);

	bRes = cFind.FindFile(sFindPath);

	if (PathIsDirectory(sDest_) == FALSE)	::CreateDirectory(sDest_, NULL);
	
	while(bRes)
	{
		bRes = cFind.FindNextFile();

		if (cFind.IsDots())
		{
			continue;
		}
		else if (cFind.IsDirectory())
		{
			sFindFolderName = cFind.GetFileTitle();
			sFrom = cFind.GetFilePath();
			sTo.Format(_T("%s\\%s"), sDest_, sFindFolderName);

			CopyFolder(sFrom, sTo);
		}
		else
		{
			sFrom = cFind.GetFilePath();
			sTo.Format(_T("%s\\%s"), sDest_, cFind.GetFileName());

			::CopyFile(sFrom, sTo, NULL);
		}

		Sleep(1);
	}

	return 1;
}

UINT CGuiUpdaterDlg::ThreadUpdate(LPVOID pParam)
{
	CGuiUpdaterDlg* pThis = (CGuiUpdaterDlg *)pParam;
	
	BOOL bComplete = FALSE;
	BOOL bUpdatePathChk = FALSE, bRunningProcChk = FALSE, bBackupChk = FALSE;
	int nTryCnt = 0;

	pThis->SendMessage(_WM_UPDATE_STATE_, NULL, 0);

	while (!bComplete)
	{
		Sleep(1000);

		if (nTryCnt++ > 100)	break;

		// 1. Update File Path 검사
		if (!bUpdatePathChk)
		{
// 			if (pThis->CheckUpdateFolder(pThis->m_sUpdateFilePath) == FALSE)
// 			{
// 				//pThis->SendMessage(_WM_UPDATE_STATE_, NULL, -1);
// 				//AfxMessageBox(_T("유효하지 않은 Update File Path 입니다."));
// 				continue;
// 			}
			CStdioFile cfile;
			CArray<CString ,CString&> atParam;

			CString strProcessName , strData;
			CString strPath_INI = pThis->m_sBasePath + _T("\\GuiUpdater.ini");

			if(cfile.Open(strPath_INI,CFile::modeRead ))
			{
				while(cfile.ReadString(strData))
				{
					atParam.Add(strData);
				}
			}
			else
			{
				AfxMessageBox(_T("GuiUpdater.ini CAN NOT OPEN !"));
				return FALSE;
			}

			for(int i = 0; i < atParam.GetSize(); i++)
			{
				ProcessKill(atParam.GetAt(i));
			}

			bUpdatePathChk = TRUE;

			pThis->SendMessage(_WM_UPDATE_STATE_, NULL, 1);
		}		
		
		// 2. Update 전 현재 동작중인  GUI 프로그렘이 있는지 확인
		if (!bRunningProcChk)
		{
			if (pThis->CheckActiveGuiProcess() == FALSE)
			{
				//pThis->SendMessage(_WM_UPDATE_STATE_, NULL, -1);
				//return 0;
				continue;
			}

			bRunningProcChk = TRUE;

			pThis->SendMessage(_WM_UPDATE_STATE_, NULL, 2);
		}

		if (!bBackupChk)
		{
			// 3. 기존 파일 및 폴더, 레지스트리 백업
			if (pThis->BackupOldFile() == FALSE)
			{
				//pThis->SendMessage(_WM_UPDATE_STATE_, NULL, -1);
				//return 0;
				continue;
			}

			bBackupChk = TRUE;

			pThis->SendMessage(_WM_UPDATE_STATE_, NULL, 3);
		}

		// 4. Update(File Copy)
		pThis->UpdateGuiProgram(pThis->m_sUpdateFilePath, pThis->m_sBasePath);

		pThis->SendMessage(_WM_UPDATE_STATE_, NULL, 4);

		bComplete = TRUE;
	}

	::ShellExecute(NULL, _T("open"), _T("C:\\Program Files (x86)\\PNE CTSPack\\CTSMonitorPro.exe"), NULL, NULL, SW_SHOW);

	if (!bComplete)	AfxMessageBox(_T("Update Fail"));

	::SendMessage(pThis->m_hWnd, WM_CLOSE, NULL, NULL);

	return 1;
}

BOOL CGuiUpdaterDlg::CheckUpdateFolder(CString sUpdateFolder_)
{
	if (PathIsDirectory(sUpdateFolder_) == FALSE)	return FALSE;

	CFileFind cFind;
	CString sFindPath;

	sFindPath = sUpdateFolder_ + _T("\\*.*");

	if (cFind.FindFile(sFindPath) == FALSE)	return FALSE;

	return TRUE;
}

BOOL CGuiUpdaterDlg::ProcessKill(CString strProcessName)
{
	HANDLE         hProcessSnap = NULL;
	BOOL           bRet      = FALSE;
	PROCESSENTRY32 pe32      = {0};

	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (hProcessSnap == (HANDLE)-1)
		return false;



	pe32.dwSize = sizeof(PROCESSENTRY32);

	//프로세스가 메모리상에 있으면 첫번째 프로세스를 얻는다
	if (Process32First(hProcessSnap, &pe32))
	{
		BOOL          bCurrent = FALSE;
		MODULEENTRY32 me32       = {0};

		do
		{
			// 			// 모듈을 검색.. 실행파일이 길면 pe32.szExeFile 이걸로 확인불가??
			bCurrent = GetProcessModule(pe32.th32ProcessID,strProcessName);

			if(bCurrent)
			{
				HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS,

					FALSE, pe32.th32ProcessID);

				if(hProcess)
				{
					if(TerminateProcess(hProcess, 0))
					{
						unsigned long nCode; //프로세스 종료 상태
						GetExitCodeProcess(hProcess, &nCode);
					}
					CloseHandle(hProcess);
				}
			}
		}
		while (Process32Next(hProcessSnap, &pe32));

		//다음 프로세스의 정보를 구하여 있으면 루프를 돈다.
	}

	CloseHandle (hProcessSnap);

	return true;
}

BOOL CGuiUpdaterDlg::GetProcessModule(DWORD dwPID,CString sProcessName)
{
	HANDLE        hModuleSnap = NULL;
	MODULEENTRY32 me32        = {0};

	hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID);
	if (hModuleSnap == (HANDLE)-1)
		return (FALSE);

	me32.dwSize = sizeof(MODULEENTRY32);

	//해당 프로세스의 모듈리스트를 루프로 돌려서 프로세스이름과 동일하면
	//true를 리턴한다.
	if(Module32First(hModuleSnap, &me32))
	{
		do
		{
			if(me32.szModule == sProcessName)
			{
				CloseHandle (hModuleSnap);
				return true;
			}
		}
		while(Module32Next(hModuleSnap, &me32));
	}

	CloseHandle (hModuleSnap);

	return false;
}
