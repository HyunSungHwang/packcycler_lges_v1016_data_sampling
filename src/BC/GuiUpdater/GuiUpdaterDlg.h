
// GuiUpdaterDlg.h : 헤더 파일
//

#pragma once
#include "afxcmn.h"


// CGuiUpdaterDlg 대화 상자
class CGuiUpdaterDlg : public CDialogEx
{
// 생성입니다.
public:
	CGuiUpdaterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_GUIUPDATER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnFindPath();
	afx_msg void OnBnClickedBtnUpdate();
	afx_msg LRESULT OnUpdateState(WPARAM wParam, LPARAM lParam);

	BOOL CheckActiveGuiProcess();
	BOOL CheckUpdateFolder(CString sUpdateFolder_);
	BOOL BackupOldFile();
	int CopyFolder(CString sSrc_, CString sDest_);
	BOOL UpdateGuiProgram(CString sSrc_, CString sDest_);

	CString m_sUpdateFilePath;
	CProgressCtrl m_progressUpdateState;
	CString m_sUpdateState;

	CString m_sBasePath;

	static UINT ThreadUpdate(LPVOID pParam);

	static BOOL ProcessKill(CString strProcessName);
	static BOOL GetProcessModule(DWORD dwPID,CString sProcessName);
};
