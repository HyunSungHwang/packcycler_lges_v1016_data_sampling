CTS2005 V 1.1.0	CTSEditor 1.0.5	CTSAnal 2.0.1
1. Pattern 파일 필요시만 Loading 하도록 수정 
Loading 부분을 CChData::Create() => CChData::GetPattern() 으로 이동

2. 아직 완료되지 않은 step은 data를 표기 하지 않도록 수정
void CDataView::UpdateChListData(CString strPath)

3. 해당 data가 없을시 Error 표시를 위해 수정
LONG CChData::GetTableIndex(int nNo)
LONG CChData::GetTableNo(int nIndex)


CTS2005 V 1.1.1	CTSEditor 1.0.6	CTSAnal 2.0.1  (모두 이전 버전과 호환됨)
1. SFT_STEP_CONDITION 에 SOC 종료 조건을 위한 Member 변수 할당
2. Editor에서 SOC Rate 종료 입력기능 추가
3. Grid에 사용자 ToolTip 기능 추가
4. 전압/전류 단위를 PC에서 별도로 설정한값만을 사용하도록 수정
5. Grid Font, Size, Column Size 수정가능, 상세보기 Column 설정가능, Timer 설정 가능(User Option Dialog 항목 추가)
6. Raw data 저장 Column에 Data Index 추가
7. SFT_STEP_CONDITION CCTS2005Doc::GetNetworkFormatStep(CStep *pStep)에서 용량값에 1000을 곱해서 하달


CTSEditor 1.1.0
1. Loop Step에서 이동 Step 편집방법 개선
  -다음 cycle로 이동시는 Step번호를 지정하지 않고 Next를 선택해 주십시요.
  -Step번호를 지정하여도 상관없지만 해당 Step을 복사하여 붙여 넣으면 재수정 하여야 합니다.
2. 붙여넣기(Ctrl+V)시 덮어쓰기 기능
 - 기존에 붙여넣기시에 Step이 Insert 되었는데 덮어쓰기로 수정
 - Menu에 복사Step삽입(Ctrl+E)을 선택하면 복사된 Step이 삽입됨
3. Step을 삭제하거나 복사하게 되면 이후 Step이나 이전Step의 이동 Step번호가 자동 수정됨
 - 이동 Step을 Next를 지정하지 않은 Loop Step은 이전Step이나 이후 step이 삭제되거나 삽입되면 자동 증가/감소됨
4. 취소(Ctrl+Z)횟수 증가
 - 기존 취소(Ctrl+Z)가 최근 1회만 제공하였으나 최대 10회까지 제공
 - 주의: 취소기능은 Step의 추가/삭제에 대해 취소됩니다. Step의 data를 수정한 후 취소 기능을 사용하게 되면 모든 수정된 Data는 수정 이전 Data로 복구됩니다. 
5. 스케쥴 목록에서 해당 목록 Double click시 자동 Laoding
 - 스케쥴 목록을 double click하게되면 해당 schedule을 Loading합니다.
6. Bug 수정
 - 안전조건을 수정하고 다른 곳을 Click하기 전에는 저장 버튼이 Enable되지 않던 버그 수정 

PSServer.dll
1. SFT_CH_DATA의 Cycle Number 정의를 unsinged short int =>unsinged long으로 변경하고 Protocol version을 1.001로 변경

CTSEditor 1.1.1
1. Test 조건을 아무 Step이 없는 곳으로 복사 할 경우 내용이 복사되지 않던 Bug 수정
  void CCTSEditorView::OnEditPaste() 

CTS2005 1.5.0 (LS전선 20060118 적용)
1. Data 복구 기능 추가
 - 통신 두절이나 Host down시 손실된 data 자동 감지 및 복구 기능 제공 [도구]=>[Data 복구] 메뉴

CTS2005 1.5.1
1. Top Grid의 Tooltip에 표기되는 Channel 번호 잘못 표기되는 Bug 수정
2. 채널번호를 연속으로 표시하는 Option 추가 (Registry : ChNumberMode)
3. 결과 저장 폴더 생성 실패시 Message표시

CTS2005 1.5.2 LG전선과 호환안됨
1. Project 명을 CTS2005 => CTSMonPro로 변경(Registry)
2. DataBase 시간 단위를 sec에서 10msec 단위로 변경
3. DataBase code table에서 EMGCode, ChannelCode를 Cycler에서 사용, EMGCode1, ChannelCode1 => Formation에서 사용

CTSEditor 1.1.2 (LS 전선과 호환안됨)
1. 모든 시간 변수를 10msec 단위로 저장
2. Database의 Cell Check time datatype을 datetime에서 long으로 변경
