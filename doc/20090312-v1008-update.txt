--------2009-08-14-----------------------------
1. Anal
	1) 선택 Step 모든 Data 저장 오류 수정
	2) Option Menu Excel 저장 크기 설정
	    단위 오류 수정
	3) Acc Cycle Num 표시 오류 수정

2. Editor
	1) DC Impedance Step 선택시 전압/전류 설정 Field Enable 안되는 현상 수정

3. Monitoring
	1) Channel Info List 표시 항목 수정 (충전용량, 방전 용량 중복 정의 삭제)
	2) rp$ 파일 저장 필드 추가

--------2009-02-02-----------------------------
1. Monitoring		2009-02-02
	1) CAN TRANSMIT GUI 및 Sending 구조 변경
	2) CAN RECEIVE GUI 및 Sending 구조 변경
	
--------2009-03-12-----------------------------
1. MDB -> STEP에서 Value9을 TEXT(255)로 필드 수정
   저장 데이터는 Aux,BMS 에 의한 Goto 부분 저장 (Aux 전압 H,Aux 전압 L,Aux 온도 H,Aux 온도 L,BMS 전압 H,BMS 전압 L,BMS 온도 H,BMS 온도 L,BMS Flt,
                           Aux 전압 H 값,Aux 전압 L 값,Aux 온도 H 값,Aux 온도 L 값,BMS 전압 H 값,BMS 전압 L 값,BMS 온도 H 값,BMS 온도 L 값,BMS Flt 값)
2. Aux 설정창 분류 추가 (0:사용안함 1:HIGH	2:LOW	3: HIGH or LOW )